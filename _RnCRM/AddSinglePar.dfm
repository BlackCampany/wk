object fmAddSinglePar: TfmAddSinglePar
  Left = 0
  Top = 0
  BorderIcons = [biMinimize, biMaximize]
  BorderStyle = bsDialog
  ClientHeight = 122
  ClientWidth = 602
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 24
    Top = 25
    Width = 48
    Height = 13
    Caption = #1053#1072#1079#1074#1072#1085#1080#1077
  end
  object Panel1: TPanel
    Left = 417
    Top = 0
    Width = 185
    Height = 122
    Align = alRight
    BevelInner = bvLowered
    Color = 16765864
    ParentBackground = False
    TabOrder = 0
    ExplicitLeft = 248
    ExplicitTop = 8
    ExplicitHeight = 41
    object cxButton1: TcxButton
      Left = 24
      Top = 16
      Width = 137
      Height = 33
      Caption = 'Ok'
      Default = True
      ModalResult = 1
      TabOrder = 0
    end
    object cxButton2: TcxButton
      Left = 24
      Top = 63
      Width = 137
      Height = 33
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
    end
  end
  object cxTextEdit1: TcxTextEdit
    Left = 24
    Top = 51
    TabOrder = 1
    Width = 369
  end
end
