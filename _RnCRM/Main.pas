unit Main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  dxRibbonForm, IniFiles, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, dxRibbon, dxBar, cxClasses, Vcl.PlatformDefaultStyleActnCtrls, System.Actions, Vcl.ActnList, Vcl.ActnMan,
  dxRibbonBackstageView, Vcl.ExtCtrls, dxStatusBar, dxRibbonStatusBar, Vcl.Menus, cxContainer, cxEdit, cxSplitter, cxTextEdit, cxMemo,
  Shared_MDITaskBar,
  Vcl.StdCtrls, cxButtons, JvFormPlacement, JvComponentBase, JvAppStorage, JvAppIniStorage, cxDBLookupComboBox, cxBarEditItem, cxCheckBox, Vcl.ImgList;

type
  TfmMain = class(TdxRibbonForm)
    ActionManager: TActionManager;
    dxBarManager: TdxBarManager;
    dxBarGroup1: TdxBarGroup;
    dxRibbon: TdxRibbon;
    rtMainTab: TdxRibbonTab;
    acClose: TAction;
    brbtnClose: TdxBarLargeButton;
    dxRibbonBackstageView: TdxRibbonBackstageView;
    brQuickMenu: TdxBar;
    StatusBar1: TdxRibbonStatusBar;
    pnlMDITaskBar: TPanel;
    PanelLog: TPanel;
    PanelCloseLog: TPanel;
    btnCloseLog: TcxButton;
    MemoLogGlobal: TcxMemo;
    SplitterBottom: TcxSplitter;
    dxBarLargeButton1: TdxBarLargeButton;
    MainAppIniFileStorage: TJvAppIniFileStorage;
    JvFormStorage: TJvFormStorage;
    dxBarManagerBar1: TdxBar;
    dxBarManagerBar2: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    acClients: TAction;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarEdit1: TdxBarEdit;
    beiPointMF: TcxBarEditItem;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton9: TdxBarLargeButton;
    dxBarLargeButton10: TdxBarLargeButton;
    dxBarLargeButton11: TdxBarLargeButton;
    dxBarLargeButton12: TdxBarLargeButton;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    acOrders: TAction;
    Sprav: TdxRibbonTab;
    dxBarManagerBar3: TdxBar;
    acCliTypes: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    acCardsType: TAction;
    dxBarLargeButton16: TdxBarLargeButton;
    procedure acCloseExecute(Sender: TObject);
    procedure dxBarManagerMerge(Sender, ChildBarManager: TdxBarManager; AddItems: Boolean);
    procedure dxRibbonTabChanged(Sender: TdxCustomRibbon);
    procedure FormCreate(Sender: TObject);
    procedure acClientsExecute(Sender: TObject);
    procedure acOrdersExecute(Sender: TObject);
    procedure acCliTypesExecute(Sender: TObject);
    procedure acCardsTypeExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure acAddDocExecute(Sender: TObject);
    procedure acEditDocExecute(Sender: TObject);
    procedure acViewDocExecute(Sender: TObject);
    procedure acDelDocExecute(Sender: TObject);
    procedure acSetStatusExecute(Sender: TObject);
  private
    { Private declarations }
    MDITaskBar: TMDITaskBar;
    FTabSelect: Integer;
  public
    { Public declarations }
  end;

var
  fmMain: TfmMain;
  sVer: String;    //������ ����� �� Project/Options.../Version Info/ProgramVersion
  constMainRibbonTabsCount: Integer;  //��������� ���������� Ribbon ������� �� Merge

Const CurIni :String = 'Profiles.ini';
      GridIni:String = 'ProfilesGr.ini';
      FormIni:String = 'ProfilesFr.ini';

procedure ShowMessageLog(text:string);
procedure ClearMessageLog;


implementation

uses
  Un2, dbs, SprCliTypes, SprCardsType, Clients, DocZ_HD;


{$R *.dfm}

procedure ClearMessageLog;
begin
  fmMain.MemoLogGlobal.Clear;
end;

procedure ShowMessageLog(text:string);
begin
  if Assigned(fmMain)=false then Exit;

  if fmMain.PanelLog.Visible=false then begin
    fmMain.PanelLog.Visible:=true;
    fmMain.SplitterBottom.Visible:=true;
    fmMain.SplitterBottom.Top:=0;
    if fmMain.PanelLog.Height=0 then fmMain.PanelLog.Height:=100;
    fmMain.PanelLog.Refresh;
    Application.ProcessMessages;
  end;
  fmMain.MemoLogGlobal.Lines.Append(TimeToStr(now)+'> '+text);
end;


procedure TfmMain.acAddDocExecute(Sender: TObject);
begin
//
end;

procedure TfmMain.acCardsTypeExecute(Sender: TObject);
begin
  // ���� �������
  ShowFormSprCaTypes;
end;

procedure TfmMain.acClientsExecute(Sender: TObject);
begin
  ShowFormCli;
end;

procedure TfmMain.acCliTypesExecute(Sender: TObject);
begin
  //���� �����������
  ShowFormSprCliTypes;
end;

procedure TfmMain.acCloseExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmMain.acDelDocExecute(Sender: TObject);
begin
//
end;

procedure TfmMain.acEditDocExecute(Sender: TObject);
begin
//
end;

procedure TfmMain.acOrdersExecute(Sender: TObject);
begin
  //������
  ShowFormDocZHD;
end;

procedure TfmMain.acSetStatusExecute(Sender: TObject);
begin
//
end;

procedure TfmMain.acViewDocExecute(Sender: TObject);
begin
//
end;

procedure TfmMain.dxBarManagerMerge(Sender, ChildBarManager: TdxBarManager; AddItems: Boolean);
var i: Integer;
  procedure DisableRefreshChildForms;
  //var i: integer;
  begin
    SendMessage(fmMain.ClientHandle, WM_SETREDRAW, WPARAM(False), 0);  //��������� ��������� �������� ����
    //for i := 0 to MDIChildCount-1 do begin
    //  SendMessage(MDIChildren[i].Handle, WM_SETREDRAW, WPARAM(False), 0);  //��������� ��������� ����
    //end;
  end;
  procedure EnableRefreshChildForms;
  //var i: integer;
  begin
    SendMessage(fmMain.ClientHandle, WM_SETREDRAW, WPARAM(True), 0);  //�������� ��������� �������� ����
    RedrawWindow(fmMain.ClientHandle, nil, 0, RDW_FRAME or RDW_INVALIDATE or RDW_ALLCHILDREN or RDW_NOINTERNALPAINT);
    //fmMain.Refresh;
    //for i := 0 to MDIChildCount-1 do begin
    //  SendMessage(MDIChildren[i].Handle, WM_SETREDRAW, WPARAM(True), 0);  //�������� ��������� ����
    //  RedrawWindow(MDIChildren[i].Handle, nil, 0, RDW_FRAME or RDW_INVALIDATE or RDW_ALLCHILDREN or RDW_NOINTERNALPAINT);
    //end;
  end;
begin
  i:=TdxRibbon(self.RibbonControl).Tabs.Count;

  TdxRibbon(self.RibbonControl).BeginUpdate;
  DisableRefreshChildForms;
  try
    //Merge - Unmerge
    if AddItems then begin
      //Merge
      //ShowMessageLog('Merge');
      if Assigned(ChildBarManager) then begin
        try
          if TdxRibbon(self.RibbonControl).Tabs.Count=constMainRibbonTabsCount then  //���� ������ �� Merged, �� Merge
            Sender.Merge(ChildBarManager,True);
        except
          //���� ��� �������� ������ ��� ��� Merged
        end;
        //AfterMerge
        if(TdxRibbon(self.RibbonControl).Tabs.Count>i) then
        begin
          try
            if TdxRibbon(self.RibbonControl).Tabs[i].Active=False then     //���� �� �������, �� ��������
              TdxRibbon(self.RibbonControl).Tabs[i].Active:=true;
          except
            //ShowMessageLog('Cannot activate Tab');
          end;
        end;
      end;
    end else begin
      //Unmerge
      //ShowMessageLog('Unmerge');
      if Assigned(ChildBarManager) then begin
        //i := FTabSelect;  //��������������, �� ������������ �� ��������� �������
        i := 0;             //������������ ������ �� ������� �������
        TdxRibbon(self.RibbonControl).Tabs[i].Active:=true;
        Sender.Unmerge(ChildBarManager);
      end;
    end;

    //<--������ ������� �������, ���� ���� ������������
//    if TdxRibbon(self.RibbonControl).Tabs.Count<=constMainRibbonTabsCount then
//      rtMainTab.Visible:=True
//    else
//      rtMainTab.Visible:=False;
    //-->
  finally
    TdxRibbon(self.RibbonControl).EndUpdate;
    EnableRefreshChildForms;
  end;
end;

procedure TfmMain.dxRibbonTabChanged(Sender: TdxCustomRibbon);
begin
  if dxRibbon.ActiveTab.Index < constMainRibbonTabsCount then
    FTabSelect := dxRibbon.ActiveTab.Index;
end;

procedure TfmMain.FormCreate(Sender: TObject);
begin
  CurDir := ExtractFilePath(ParamStr(0));
  ReadIni;

  //<--��������� ���������� Ribbon ������� �� Merge
  constMainRibbonTabsCount:=dxRibbon.TabCount;

  MainAppIniFileStorage.FileName:=CurDir+FormIni;

  //-->
  //<--������������ ��������� �� �������
  LoadKeyboardLayout('00000419', KLF_ACTIVATE);

  MDITaskBar := TMDITaskBar.Create(Self);
  with MDITaskBar do begin
    Parent := pnlMDITaskBar;
    BevelOuter:=bvNone;
    Align := alClient;
    //Height := 27;
    AutoCreateButtons := True;
    AlwaysMaximized := False;
    ButtonWidth := 140;
    Flat := False;
    HideMinimizedMDI := True;
    GroupSimilar := False;
    SyncCaptions := True;
    DragDropReorder := False;
    ShowHint := True;
    Font.Style := [fsBold];
    ScrollType := TScrollType.stNone;
  end;
end;

procedure TfmMain.FormShow(Sender: TObject);
begin
  ClearMessageLog;
  if dmS.DBConnectUDL then ShowMessageLog('����� � ����� SQL ��.')
  else ShowMessageLog('������ �������� ���� SQL');
  fmMain.MainAppIniFileStorage.FileName:=GetFormIniFile;
end;

end.
