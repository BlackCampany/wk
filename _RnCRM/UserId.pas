unit UserId;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus, cxControls, cxContainer, cxEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, Vcl.StdCtrls, cxButtons, Vcl.ExtCtrls, Vcl.ComCtrls, Data.DB, FIBDataSet, pFIBDataSet;

type
  TfmUserId = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Button1: TcxButton;
    Button2: TcxButton;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    ComboBox1: TcxLookupComboBox;
    Edit1: TcxTextEdit;
    quPer: TpFIBDataSet;
    quPerID: TFIBIntegerField;
    quPerID_PARENT: TFIBIntegerField;
    quPerNAME: TFIBStringField;
    quPerUVOLNEN: TFIBBooleanField;
    quPerCheck: TFIBStringField;
    quPerMODUL1: TFIBBooleanField;
    quPerMODUL2: TFIBBooleanField;
    quPerMODUL3: TFIBBooleanField;
    quPerMODUL4: TFIBBooleanField;
    quPerMODUL5: TFIBBooleanField;
    quPerMODUL6: TFIBBooleanField;
    dsPer: TDataSource;
    procedure Button1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    CountEnter:Integer;
    iLastP:Integer;
    procedure Init;
  public
    { Public declarations }
  end;


function GetLoginForm: boolean;

// var fmUserId: TfmUserId;

implementation

uses dbfb, Un2;

{$R *.dfm}

function GetLoginForm: boolean;
var F: TfmUserId;
begin
  F:=TfmUserId.Create(Application);
  try
    F.Init;
    if F.ShowModal=mrOk then
    begin

      Result:=True;
    end else begin
      Result:=False;
    end;
  finally
    F.Free;
  end;
end;

procedure TfmUserId.Button1Click(Sender: TObject);
Var StrWk:String;
begin
  strwk:=Edit1.Text;
  with dmFB do
  begin
    quPer.Locate('ID',ComboBox1.EditValue,[]);
    if quPerCheck.AsString=Edit1.Text then
    begin
      Person.Id:=quPerId.AsInteger;
      Person.Name:=quPerName.AsString;
      quPer.Active:=False;
      ModalResult:=1;
    end
    else
    begin
      inc(CountEnter);
      if CountEnter>2 then close;
      showmessage('������ ������������. �������� '+IntToStr(3-CountEnter)+' �������.');
      Edit1.Text:='';
      Edit1.SetFocus;
    end;
  end;
end;

procedure TfmUserId.FormCreate(Sender: TObject);
begin
  Left:=1;
  Top:=1;
  CountEnter:=0;
end;

procedure TfmUserId.FormShow(Sender: TObject);
begin
  Edit1.Text:='******';
  Edit1.SetFocus;
end;

procedure TfmUserId.Init;
begin
  if dmFb.CasherRnDb.Connected then
  begin
    quPer.Active:=False;
    quPer.SelectSQL.Clear;
    quPer.SelectSQL.Add('select * from rpersonal');
    quPer.SelectSQL.Add('where uvolnen=1 and modul6=0');
    quPer.SelectSQL.Add('order by Name');
    quPer.Active:=True;

    ComboBox1.EditValue:=Person.Id;
  end;
end;


end.
