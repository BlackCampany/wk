unit CustomChildForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  dxRibbonForm, dxRibbonSkins, dxBar, dxRibbon, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Vcl.Menus, cxControls, cxContainer, cxEdit, JvComponentBase, JvFormPlacement, cxSplitter, cxTextEdit, cxMemo, Vcl.StdCtrls, cxButtons,
  Vcl.ExtCtrls;

type
  TfmCustomChildForm = class(TdxRibbonForm)
    PanelLog: TPanel;
    PanelCloseLog: TPanel;
    btnCloseLog: TcxButton;
    MemoLogLocal: TcxMemo;
    SplitterBottom: TcxSplitter;
    JvFormStorageCustom: TJvFormStorage;
    procedure btnCloseLogClick(Sender: TObject);
  private
    { Private declarations }
    FNewFormStyle: TFormStyle;
    function GetCaption: String;
    procedure SetCaption(const Value: String);
  protected
    { Protected declarations }
    procedure CreateParams(var Params: TCreateParams); override;
  published
    { Published declarations }
    property Caption: String read GetCaption write SetCaption;
  public
    { Public declarations }
//    constructor Create(AOwner: TComponent); reintroduce;
    constructor Create(AOwner: TComponent; AModal: TFormStyle = fsMDIChild); reintroduce;  //fsNormal
    destructor Destroy; override;
    procedure ClearMessageLogLocal;
    procedure ShowMessageLogLocal(text:string);
  end;

//var
//  fmCustomChildForm: TfmCustomChildForm;

implementation

uses
  Main;

{$R *.dfm}

//constructor TfmCustomChildForm.Create(AOwner: TComponent);
constructor TfmCustomChildForm.Create(AOwner: TComponent; AModal: TFormStyle);
var tempdxRibbon: TdxRibbon;
begin
  FNewFormStyle := AModal;
  inherited Create(AOwner);
  //����� inherited Create(AOwner), �.�. �������� ���� ����� ��������� ������ ���

  //<--��������������� ��������� �����
  JvFormStorageCustom.Active:=False;  //����� ������ ���������� ����������� � ���������������
  JvFormStorageCustom.AppStoragePath:=Self.Name;
  JvFormStorageCustom.RestoreFormPlacement;
  if Top<0 then Top:=0;  //�������������, ���� ����� ������� �����
  //-->

  //<--���� ��������� TdxRibbon �� ����� � �������� � ���� ��������� �����
  tempdxRibbon:=TdxRibbon(self.RibbonControl);
  if Assigned(tempdxRibbon) then begin
    tempdxRibbon.ShowTabHeaders:=False;
    tempdxRibbon.Style:=TdxRibbonStyle.rs2010;
    tempdxRibbon.SupportNonClientDrawing:=True;
  end;
  //-->

  Application.ProcessMessages;  //������������ �����
end;



procedure TfmCustomChildForm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams( Params );

  case FNewFormStyle of
    fsNormal:    begin
                   Visible := false;
                   FormStyle := FNewFormStyle;
                   //���� ����� ���������
                   if fsModal in FormState then begin
                     BorderIcons:=BorderIcons-[biMinimize,biMaximize];
                     //BorderStyle:=bsSingle;
                   end else begin
                     BorderIcons:=BorderIcons+[biMinimize,biMaximize];
                     BorderStyle:=bsSizeable;
                   end;
                 end;
    fsStayOnTop: begin
                   Visible := false;
                   FormStyle := FNewFormStyle;
                   BorderIcons:=BorderIcons-[biMinimize,biMaximize];
                   //BorderStyle:=bsSizeToolWin;
                 end;
  end;

  //������ ����� ������ �������� ���� ��������� ��� ���� ��������� ������ � ������ ����� Windows
  with Params do
  begin
//    ExStyle := ExStyle or WS_EX_APPWINDOW;
 //   WndParent := GetDesktopwindow;  //��� ������������ �������� ���� ����� �� ������������� ��������
  end;
end;


destructor TfmCustomChildForm.Destroy;
begin
  //<--��������� ��������� ����� ������ ���� ��� �� ��������������
  if not(fsModal in FormState) then
    Height:=Height+22;  //�����-�� ���, ��� ���������� � �������������� �����, � ������ ����������� �� 22 �������, ��� � ������������ ���
  if WindowState<>wsMinimized then
    JvFormStorageCustom.SaveFormPlacement;
  //-->

  inherited Destroy;
end;

procedure TfmCustomChildForm.btnCloseLogClick(Sender: TObject);
begin
  PanelLog.Visible:=false;
  SplitterBottom.Visible:=false;
  MemoLogLocal.Clear;
end;

procedure TfmCustomChildForm.ClearMessageLogLocal;
begin
  MemoLogLocal.Clear;
end;

procedure TfmCustomChildForm.ShowMessageLogLocal(text:string);
const constMaxCountLinesInMemoLog = 1000;        //������������ ���-�� ����� � ������� MemoLog
var i, countToDel: integer;
begin
  MemoLogLocal.Lines.BeginUpdate;
  try
    //<--��������� ����� ����� � Memo ���� �� ������ constMaxCountLinesInMemoLog
    if MemoLogLocal.Lines.Count>constMaxCountLinesInMemoLog then begin
      countToDel:=MemoLogLocal.Lines.Count-constMaxCountLinesInMemoLog;
      //������� ������ ���-�� ������� �����
      for i := 0 to countToDel do
        MemoLogLocal.Lines.Delete(0);
    end;
    //-->

    //<--���� ������ ������, �� ���������� �
    if PanelLog.Visible=false then begin
      SplitterBottom.Top:=5000;  //����� ��� � ����� ����
      SplitterBottom.Visible:=true;
      PanelLog.Top:=5111;        //����� ��� � ����� ���� � ���� SplitterBottom
      PanelLog.Visible:=true;
      if PanelLog.Height=0 then PanelLog.Height:=100;
      PanelLog.Refresh;
      Application.ProcessMessages;
    end;
    //-->
  finally
    MemoLogLocal.Lines.EndUpdate;
  end;
  //��������� ��������� � ����
  MemoLogLocal.Lines.Append(TimeToStr(now)+'> '+text);
end;

function TfmCustomChildForm.GetCaption: String;
begin
  Result := inherited Caption;
end;

procedure TfmCustomChildForm.SetCaption(const Value: String);
var tempdxRibbon: TdxRibbon;
begin
  if value <> Caption then begin
    //<--���� ��������� TdxRibbon �� �����
    tempdxRibbon:=TdxRibbon(self.RibbonControl);
    if Assigned(tempdxRibbon) then begin
      //������ ��������� ������ ������� �������
      if tempdxRibbon.TabCount>0 then
        tempdxRibbon.Tabs.Items[0].Caption := Value;
      //������ ��������� ������������ ������� ������� �� ������� �����
      if fmMain.dxRibbon.TabCount>constMainRibbonTabsCount then
        //��������� ����� �� ������������ ������� ������� ����� ���� ����, �.�. ����� ������ ���
        if fmMain.dxRibbon.Tabs[constMainRibbonTabsCount].Caption=Caption then
          fmMain.dxRibbon.Tabs[constMainRibbonTabsCount].Caption := Value;
    end;
    //-->
    inherited Caption := Value;
    Invalidate;
  end;
end;

end.
