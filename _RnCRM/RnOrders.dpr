// JCL_DEBUG_EXPERT_DELETEMAPFILE ON
program RnOrders;

uses
  Vcl.Forms,
  Main in 'Main.pas' {fmMain},
  CustomChildForm in 'CustomChildForm.pas' {fmCustomChildForm},
  Shared_MDITaskBar in '..\SharedUnits\Shared_MDITaskBar.pas',
  UserId in 'UserId.pas' {fmUserId},
  dbfb in 'dbfb.pas' {dmFb: TDataModule},
  dbs in 'dbs.pas' {dmS: TDataModule},
  Un2 in 'Un2.pas',
  SprCliTypes in 'SprCliTypes.pas' {fmSprCliTypes},
  AddSinglePar in 'AddSinglePar.pas' {fmAddSinglePar},
  Clients in 'Clients.pas' {fmClients},
  SprCardsType in 'SprCardsType.pas' {fmSprCaTypes},
  AddCliOrd in 'AddCliOrd.pas' {fmAddCli},
  DocZ_HD in 'DocZ_HD.pas' {fmDocZ_HD},
  DocZ_SP in 'DocZ_SP.pas' {fmDocZ_SP};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfmMain, fmMain);
  Application.CreateForm(TdmFb, dmFb);
  Application.CreateForm(TdmS, dmS);
  if GetLoginForm=False then
  begin
    //������ �� �����, ��������� ���������
    Application.ShowMainForm:=False;
    Application.Terminate;
  end else
  begin
    WriteIni;
  end;

  Application.Run;
end.
