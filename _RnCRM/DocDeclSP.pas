unit DocDeclSP;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  CustomChildForm, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer,
  cxTextEdit, cxCalendar, cxCheckBox, cxDBLookupComboBox, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error,
  FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Vcl.Menus, cxClasses, FireDAC.Comp.Client,
  FireDAC.Comp.DataSet, System.Actions, Vcl.ActnList, dxBar, cxBarEditItem, cxMemo, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridCustomView, cxGrid, dxRibbon, cxDropDownEdit, cxCalc, dxBarExtItems;

type
  TfmDocSpecDecl = class(TfmCustomChildForm)
    dxBarManager1: TdxBarManager;
    dxBarGroup1: TdxBarGroup;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    dxBarManager1Bar1: TdxBar;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    ActionList1: TActionList;
    acClose: TAction;
    acSave: TAction;
    quSpecOb: TFDQuery;
    UpdSQL1: TFDUpdateSQL;
    dsquSpecOb: TDataSource;
    ViewSpecDecl: TcxGridDBTableView;
    LevelSpecDecl: TcxGridLevel;
    GridSpecDecl: TcxGrid;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    PopupMenu1: TPopupMenu;
    cxStyle2: TcxStyle;
    quE: TFDQuery;
    acTestSpec: TAction;
    acDelPos: TAction;
    N1: TMenuItem;
    dxBarManager1Bar2: TdxBar;
    acGetInfoSel: TAction;
    N2: TMenuItem;
    N3: TMenuItem;
    acAddPos: TAction;
    N4: TMenuItem;
    LookupComboBox1: TcxBarEditItem;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    LevelSpecDecl1: TcxGridLevel;
    ViewSpecDecl1: TcxGridDBTableView;
    quS: TFDQuery;
    SpinEdit1: TdxBarSpinEdit;
    Combo2: TdxBarCombo;
    DateEdit1: TdxBarDateCombo;
    DateEdit2: TdxBarDateCombo;
    Combo1: TdxBarCombo;
    quOrg: TFDQuery;
    quOrgRARID: TStringField;
    quOrgISHOP: TIntegerField;
    quOrgIPUTM: TStringField;
    quOrgIPDB: TStringField;
    quOrgDBNAME: TStringField;
    quOrgPASSW: TStringField;
    quOrgINN: TStringField;
    quOrgKPP: TStringField;
    quOrgSHORTNAME: TStringField;
    quOrgFULLNAME: TStringField;
    quOrgCOUNTRY: TStringField;
    quOrgDESCR: TStringField;
    quOrgNAMESHOP: TStringField;
    quOrgISS: TSmallintField;
    quOrgISPRODUCTION: TSmallintField;
    quOrgSDEP: TStringField;
    quOrgIACTIVE: TSmallintField;
    quOrgWRITELOG: TSmallintField;
    dsquOrg: TDataSource;
    quSpecObIDHEAD: TLargeintField;
    quSpecObID: TLargeintField;
    quSpecObAVID: TIntegerField;
    quSpecObNAMEV: TStringField;
    quSpecObPRODID: TStringField;
    quSpecObNAME: TMemoField;
    quSpecObPRODINN: TStringField;
    quSpecObPRODKPP: TStringField;
    quSpecObREMNB: TSingleField;
    quSpecObQIN1: TSingleField;
    quSpecObQIN2: TSingleField;
    quSpecObQIN3: TSingleField;
    quSpecObQUANT_IN: TSingleField;
    quSpecObQIN4: TSingleField;
    quSpecObQIN5: TSingleField;
    quSpecObQIN6: TSingleField;
    quSpecObQUANT_IN_ITOG: TSingleField;
    quSpecObQOUT1: TSingleField;
    quSpecObQOUT2: TSingleField;
    quSpecObQOUT3: TSingleField;
    quSpecObQOUT4: TSingleField;
    quSpecObQUANT_OUT_ITOG: TSingleField;
    quSpecObREMNE: TSingleField;
    ViewSpecDeclAVID: TcxGridDBColumn;
    ViewSpecDeclNAMEV: TcxGridDBColumn;
    ViewSpecDeclNAME: TcxGridDBColumn;
    ViewSpecDeclPRODINN: TcxGridDBColumn;
    ViewSpecDeclPRODKPP: TcxGridDBColumn;
    ViewSpecDeclREMNB: TcxGridDBColumn;
    ViewSpecDeclQIN1: TcxGridDBColumn;
    ViewSpecDeclQIN2: TcxGridDBColumn;
    ViewSpecDeclQIN3: TcxGridDBColumn;
    ViewSpecDeclQUANT_IN: TcxGridDBColumn;
    ViewSpecDeclQIN4: TcxGridDBColumn;
    ViewSpecDeclQIN5: TcxGridDBColumn;
    ViewSpecDeclQIN6: TcxGridDBColumn;
    ViewSpecDeclQUANT_IN_ITOG: TcxGridDBColumn;
    ViewSpecDeclQOUT1: TcxGridDBColumn;
    ViewSpecDeclQOUT2: TcxGridDBColumn;
    ViewSpecDeclQOUT3: TcxGridDBColumn;
    ViewSpecDeclQOUT4: TcxGridDBColumn;
    ViewSpecDeclQUANT_OUT_ITOG: TcxGridDBColumn;
    ViewSpecDeclREMNE: TcxGridDBColumn;
    ViewSpecDeclPRODID: TcxGridDBColumn;
    quSpecIn: TFDQuery;
    dsquSpecIn: TDataSource;
    UpdSQL2: TFDUpdateSQL;
    quSpecInIDHEAD: TLargeintField;
    quSpecInID: TLargeintField;
    quSpecInAVID: TIntegerField;
    quSpecInNAMEV: TStringField;
    quSpecInPRODID: TStringField;
    quSpecInNAMEP: TMemoField;
    quSpecInPRODINN: TStringField;
    quSpecInPRODKPP: TStringField;
    quSpecInCLIENTID: TStringField;
    quSpecInNAMECLI: TMemoField;
    quSpecInCLIENTINN: TStringField;
    quSpecInCLIENTKPP: TStringField;
    quSpecInLICCODE: TStringField;
    quSpecInLICSERNUM: TStringField;
    quSpecInLICDATEB: TStringField;
    quSpecInLICDATEE: TStringField;
    quSpecInLICORGAN: TStringField;
    quSpecInDOCDATE: TSQLTimeStampField;
    quSpecInDOCNUM: TStringField;
    quSpecInGTD: TStringField;
    quSpecInQIN: TSingleField;
    ViewSpecDecl1AVID: TcxGridDBColumn;
    ViewSpecDecl1NAMEV: TcxGridDBColumn;
    ViewSpecDecl1PRODID: TcxGridDBColumn;
    ViewSpecDecl1NAMEP: TcxGridDBColumn;
    ViewSpecDecl1PRODINN: TcxGridDBColumn;
    ViewSpecDecl1PRODKPP: TcxGridDBColumn;
    ViewSpecDecl1CLIENTID: TcxGridDBColumn;
    ViewSpecDecl1NAMECLI: TcxGridDBColumn;
    ViewSpecDecl1CLIENTINN: TcxGridDBColumn;
    ViewSpecDecl1CLIENTKPP: TcxGridDBColumn;
    ViewSpecDecl1LICCODE: TcxGridDBColumn;
    ViewSpecDecl1LICSERNUM: TcxGridDBColumn;
    ViewSpecDecl1LICDATEB: TcxGridDBColumn;
    ViewSpecDecl1LICDATEE: TcxGridDBColumn;
    ViewSpecDecl1LICORGAN: TcxGridDBColumn;
    ViewSpecDecl1DOCDATE: TcxGridDBColumn;
    ViewSpecDecl1DOCNUM: TcxGridDBColumn;
    ViewSpecDecl1GTD: TcxGridDBColumn;
    ViewSpecDecl1QIN: TcxGridDBColumn;
    procedure acCloseExecute(Sender: TObject);
    procedure acSaveExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acDelPosExecute(Sender: TObject);
    procedure acAddPosExecute(Sender: TObject);
  private
    { Private declarations }
    FRARID:string;
    FIDATE: Integer;
    FSID:string;
    FIEDIT: Integer;
    FNUMBER:string;
    FDESCR:string;
    FIADD:Integer;
    FIDH:Integer;

    procedure Init;

  public
    { Public declarations }
  end;

// var
//  fmDocSpec: TfmDocSpec;

procedure ShowSpecViewADecl(RARID:string;IDH,IEDIT,IADD:Integer);

implementation

{$R *.dfm}

uses ViewRar, dmRar, EgaisDecode, ViewDocCorr, AddPosCorrSpec, ViewCards;

procedure ShowSpecViewADecl(RARID:string;IDH,IEDIT,IADD:Integer);
var F:TfmDocSpecDecl;
begin
  F:=TfmDocSpecDecl.Create(Application);
// ShowMessage(Xmlstr);
  F.FRARID:=RARID;
//  F.FIDATE:=IDATE;
//  F.FSID:=SID;
  F.FIEDIT:=IEDIT;
//  F.FDESCR:=FDESCR;
  F.FIADD:=IADD;
  F.FIDH:=IDH;
//  F.FNUMBER:=FNUMBER;

  F.Init;
  F.Show;
end;

procedure TfmDocSpecDecl.acAddPosExecute(Sender: TObject);
begin
  //�������� �������
  //�������� �������
  if FIEDIT=1 then
  begin
    {
    //�������� �������
    if Assigned(fmACard)=False then
    begin //����� �� ����������, � ����� �������
      fmACard:=TfmACard.Create(Application);
      fmACard.Init;
      fmACard.FRARID:=FRARID;
      fmACard.FIDATE:=FIDATE;
      fmACard.FSID:=FSID;
      fmACard.STYPE:='CORR';
      fmACard.quSpec:=quSpecCorr;
      fmACard.Show;
    end else
    begin
      fmACard.FRARID:=FRARID;
      fmACard.FIDATE:=FIDATE;
      fmACard.FSID:=FSID;
      fmACard.STYPE:='CORR';
      fmACard.quSpec:=quSpecCorr;
      fmACard.Show;
    end;
    }
    {
    if not Assigned(fmAddPosCorr) then fmAddPosCorr:=TfmAddPosCorr.Create(Application,TFormStyle.fsNormal);

    fmAddPosCorr.ShowModal;
    if fmAddPosCorr.ModalResult=mrOk then
    begin //�������� �������
      with fmAddPosCorr do
      with dmR do
      begin
        quSelCard.Active:=False;
        quSelCard.ParamByName('CODE').AsString:=fmAddPosCorr.cxButtonEdit1.Text;
        quSelCard.Active:=True;

        if quSelCard.RecordCount=1 then
        begin
        end;

        quSelCard.Active:=False;
      end;
    end;}
  end;

end;

procedure TfmDocSpecDecl.acCloseExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmDocSpecDecl.acDelPosExecute(Sender: TObject);
begin
  if FIEDIT=1 then
  begin
//    quSpecCorr.Delete;
  end;
end;

procedure TfmDocSpecDecl.acSaveExecute(Sender: TObject);
var iErrors:Integer;
begin
  //��������� ������������
  if FIADD=1 then
  begin
    {
    try
      with dmR do
      begin
        quS.Active:=False;
        quS.SQL.Clear;
        quS.SQL.Add('');
        quS.SQL.Add('  declare @FSRARID VARCHAR(50) = '''+CommonSet.FSRAR_ID+'''');
        quS.SQL.Add('  DECLARE @IDATEVN INT = '+its(FIDATE));
        quS.SQL.Add('  DECLARE @SID VARCHAR(50) = '''+FSID+'''');
        quS.SQL.Add('  DECLARE @SNUM VARCHAR(50) = '''+FNUMBER+'''');
        quS.SQL.Add('  DECLARE @STYPE VARCHAR(10) = ''---''');
        quS.SQL.Add('  INSERT INTO [dbo].[ADOCSCORR_HD] ([FSRARID],[IDATE],[SID],[NUMBER],[STYPE],[IACTIVE])');
        quS.SQL.Add('  VALUES (@FSRARID,@IDATEVN,@SID,@SNUM,@STYPE,0)');
        quS.ExecSQL;

        FIADD:=0;
      end;
    except
    end;}
  end;

  if FIADD=0 then
  begin
    {
    quSpecCorr.UpdateTransaction.StartTransaction;
    iErrors := quSpecCorr.ApplyUpdates;
    if iErrors = 0 then begin
      try
        quSpecCorr.CommitUpdates;
        quSpecCorr.UpdateTransaction.Commit;

        quE.Active:=False;
        quE.SQL.Clear;
        quE.SQL.Add('UPDATE [dbo].[ADOCSCORR_HD]');
        quE.SQL.Add('Set');
        quE.SQL.Add('[DESCR]='''+Trim(cxBarEditItem1.EditValue)+'''');
        quE.SQL.Add('WHERE');
        quE.SQL.Add('[FSRARID] = '''+FRARID+'''');
        quE.SQL.Add('and [IDATE] ='+its(FIDATE));
        quE.SQL.Add('and [SID] ='''+FSID+'''');
        quE.ExecSQL;

        if Assigned(fmDocCorr) then
          if fmDocCorr.Showing then fmDocCorr.Init;
      except
        ShowMessage('������ ����������..');
      end;
    end else
    begin
      ShowMessage('������ ����������.');
      quSpecCorr.UpdateTransaction.Rollback;
    end;
    }
  end;
end;

procedure TfmDocSpecDecl.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  quOrg.Active:=False;
  Action:=caFree;
end;

procedure TfmDocSpecDecl.Init;
begin
  quOrg.Active:=False;
  quOrg.ParamByName('RARID').AsString:=FRARID;
  quOrg.Active:=True;
  quOrg.First;

  LookupComboBox1.EditValue:=quOrgRARID.AsString;

  if FIADD=1 then
  begin
    Caption:='���������� ����������� ����������.';

    DateEdit1.Date:=Date;
    DateEdit2.Date:=Date;
    Combo1.ItemIndex:=0;
    Combo2.ItemIndex:=0;
    SpinEdit1.Value:=0;
  end;

  if FIADD=0 then
  begin
    Caption:='������������ ��������� ����������� ���������� ('+its(FIDH)+')';

    quS.Active:=False;
    quS.SQL.Clear;
    quS.SQL.Add('');
    quS.SQL.Add('SELECT TOP 1 [FSRARID],[ID],[IDATEB],[IDATEE],[DDATEB],[DDATEE],[IACTIVE],[IT1],[IT2],[INUMCORR] FROM [RAR].[dbo].[ADECL_HD]');
    quS.SQL.Add('where [FSRARID]='''+FRARID+'''');
    quS.SQL.Add('and [ID]='+its(FIDH));
    quS.Active:=True;

    DateEdit1.Date:=quS.FieldByName('DDATEB').AsDateTime;
    DateEdit2.Date:=quS.FieldByName('DDATEE').AsDateTime;
  //  ComboBox1.Properties
    quS.Active:=False;
  end;


  ViewSpecDecl.BeginUpdate;
  quSpecOb.Active:=False;
  quSpecOb.ParamByName('IDH').AsInteger:=FIDH;
  quSpecOb.Active:=True;
  ViewSpecDecl.EndUpdate;

  ViewSpecDecl1.BeginUpdate;
  quSpecIn.Active:=False;
  quSpecIn.ParamByName('IDH').AsInteger:=FIDH;
  quSpecIn.Active:=True;
  ViewSpecDecl1.EndUpdate;

  if FIEDIT=1 then
  begin
    acSave.Enabled:=True;
//    ViewSpecCorrQUANT.Options.Editing:=True;
//    cxBarEditItem1.Properties.ReadOnly:=False;
  end else
  begin
    acSave.Enabled:=False;


//    ViewSpecCorrQUANT.Options.Editing:=False;
//    cxBarEditItem1.Properties.ReadOnly:=True;
  end;
end;

end.
