unit AddCliOrd;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus, Vcl.StdCtrls, cxButtons, Vcl.ExtCtrls, cxControls, cxContainer, cxEdit,
  Vcl.ComCtrls, dxCore, cxDateUtils, cxCheckBox, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxMaskEdit, cxCalendar, cxLabel, cxTextEdit, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client;

type
  TfmAddCli = class(TForm)
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxTextEdit1: TcxTextEdit;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    cxTextEdit2: TcxTextEdit;
    cxLabel3: TcxLabel;
    cxDateEdit1: TcxDateEdit;
    cxLabel4: TcxLabel;
    cxTextEdit3: TcxTextEdit;
    cxLabel5: TcxLabel;
    cxLookupComboBox1: TcxLookupComboBox;
    cxLabel6: TcxLabel;
    cxLookupComboBox2: TcxLookupComboBox;
    cxLabel7: TcxLabel;
    cxLabel8: TcxLabel;
    cxTextEdit4: TcxTextEdit;
    cxCheckBox1: TcxCheckBox;
    cxCheckBox2: TcxCheckBox;
    quCliTypes: TFDQuery;
    quCliTypesID: TIntegerField;
    quCliTypesNAME: TStringField;
    dsquCliTypes: TDataSource;
    quCardsTypes: TFDQuery;
    quCardsTypesID: TIntegerField;
    quCardsTypesNAME: TStringField;
    dsquCardsTypes: TDataSource;
    cxLookupComboBox3: TcxLookupComboBox;
    quPeriods: TFDQuery;
    IntegerField1: TIntegerField;
    StringField1: TStringField;
    dsquPeriods: TDataSource;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAddCli: TfmAddCli;

implementation

uses
  dbs;

{$R *.dfm}

procedure TfmAddCli.FormShow(Sender: TObject);
begin
  cxTextEdit1.SetFocus;
end;

end.
