unit SprCliTypes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  CustomChildForm,
  Vcl.Controls, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus,
  cxControls, cxContainer, cxEdit, System.Actions, Vcl.ActnList, Data.DB,
  Data.Win.ADODB, cxTextEdit, cxMemo, Vcl.StdCtrls, Vcl.ExtCtrls, dxRibbonSkins,
  dxRibbonCustomizationForm, dxBar, dxBarExtItems, cxClasses, dxRibbon,
  cxCalendar, cxBarEditItem, dxStatusBar, dxRibbonStatusBar, cxDBLookupComboBox,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator,
  cxDBData, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridCustomView, cxGrid,vcl.Forms, cxLabel, cxCheckBox,
  JvComponentBase, JvFormPlacement, Dialogs, cxImageComboBox, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TfmSprCliTypes = class(TfmCustomChildForm)
    ActionList1: TActionList;
    acRefresh: TAction;
    dxRibbon1: TdxRibbon;
    rtScales: TdxRibbonTab;
    dxBarManager1: TdxBarManager;
    bmAction: TdxBar;
    bmClose: TdxBar;
    bUpdate: TdxBarLargeButton;
    bClose: TdxBarLargeButton;
    acClose: TAction;
    sbSprScale: TdxRibbonStatusBar;
    GrCT: TcxGrid;
    ViewCT: TcxGridDBTableView;
    LevelCT: TcxGridLevel;
    JvFormStorage: TJvFormStorage;
    btnAdd: TdxBarLargeButton;
    btnDel: TdxBarLargeButton;
    btnEdit: TdxBarLargeButton;
    acAdd: TAction;
    acEdit: TAction;
    acDel: TAction;
    quCliTypes: TFDQuery;
    dsquCliTypes: TDataSource;
    quCliTypesID: TIntegerField;
    quCliTypesNAME: TStringField;
    ViewCTID: TcxGridDBColumn;
    ViewCTNAME: TcxGridDBColumn;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acCloseExecute(Sender: TObject);
    procedure acRefreshExecute(Sender: TObject);
    procedure acAddExecute(Sender: TObject);
    procedure acEditExecute(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ViewCTCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Init;
  end;
  procedure ShowFormSprCliTypes;

var
  fmSprCliTypes: TfmSprCliTypes;

implementation

uses
  Un2, dbs, AddSinglePar;

{$R *.dfm}

//��������� �������� �����
procedure ShowFormSprCliTypes;
begin
  if Assigned(fmSprCliTypes)=False then begin //����� �� ����������, � ����� �������
    fmSprCliTypes:=TfmSprCliTypes.Create(Application);
  end;
  fmSprCliTypes.Init;
  fmSprCliTypes.Show;
end;

procedure TfmSprCliTypes.acAddExecute(Sender: TObject);
var sRet:string;
    iNum:Integer;
begin
  if ShowFormAddStrPar(1,'',sRet) then
  begin
//    ShowMessage('�������� '+sRet);
    iNum:=fGetId(101);
    quCliTypes.Append;
    quCliTypesID.AsInteger:=iNum;
    quCliTypesNAME.AsString:=sRet;
    quCliTypes.Post;
  end;
end;

procedure TfmSprCliTypes.acCloseExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmSprCliTypes.acDelExecute(Sender: TObject);
var iErrors:integer;
begin
  // ��������
  if quCliTypes.RecordCount>0 then
  begin
    if MessageDlg('������� ��� '+quCliTypesNAME.AsString+' ?',mtConfirmation,[mbYes,mbNo],0) = mrYes then
    begin
      with dmS do
      begin
        quS.Active:=False;
        quS.SQL.Clear;
        quS.SQL.Add('');
        quS.SQL.Add('update [dbo].[ORDERSCLI]');
        quS.SQL.Add('Set [CLITYPE]=0');
        quS.SQL.Add('where [CLITYPE]='+its(quCliTypesID.AsInteger));
        quS.ExecSQL;

        quCliTypes.Delete;
      end;
    end;
  end;
end;

procedure TfmSprCliTypes.acEditExecute(Sender: TObject);
var sRet:string;
begin
  if quCliTypes.RecordCount>0 then
  begin
    if ShowFormAddStrPar(0,quCliTypesNAME.AsString,sRet) then
    begin
      quCliTypes.Edit;
      quCliTypesNAME.AsString:=sRet;
      quCliTypes.Post;
    end;
  end;
end;

procedure TfmSprCliTypes.acRefreshExecute(Sender: TObject);
begin
  Init;
end;

procedure TfmSprCliTypes.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
  if not(fsModal in FormState) then
    fmSprCliTypes:=Nil;
  ViewCT.StoreToIniFile(GetGridIniFile,False,[gsoUseSummary,gsoUseFilter]);
end;

procedure TfmSprCliTypes.FormCreate(Sender: TObject);
begin
  ViewCT.RestoreFromIniFile(GetGridIniFile);
end;

procedure TfmSprCliTypes.Init;
begin
  ViewCT.BeginUpdate;
  quCliTypes.Active:=False;
  quCliTypes.Active:=True;
  ViewCT.EndUpdate;
end;

procedure TfmSprCliTypes.ViewCTCustomDrawCell(Sender: TcxCustomGridTableView; ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var Value:string;
begin
  {
  Value:=AViewInfo.GridRecord.DisplayTexts[ViewScISTATUS.Index];
  if Value='����������' then ACanvas.Canvas.Brush.Color := $00B3B3FF;
  }
end;

end.

