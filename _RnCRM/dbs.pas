unit dbs;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool,
  FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.MSSQL, FireDAC.Phys.MSSQLDef, FireDAC.VCLUI.Wait, FireDAC.Comp.UI, FireDAC.Phys.ODBCBase, FireDAC.Comp.Client, Data.DB,
  cxGrid,ComCtrls,cxGridExportLink,

  Vcl.ImgList, Vcl.Controls, cxGraphics, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt, FireDAC.Comp.DataSet, cxStyles, cxClasses, Vcl.Dialogs;



// Forms, Windows, Messages, SysUtils, cxGridExportLink, cxGrid, DB,
//     IniFiles, ADODB, Variants, IdGlobal, ComCtrls, dxmdaset, ActiveX, Classes, dmMain;


type
  TdmS = class(TDataModule)
    FDConnection: TFDConnection;
    FDTrans: TFDTransaction;
    FDTransUpdate: TFDTransaction;
    FDPhysMSSQLDriverLink: TFDPhysMSSQLDriverLink;
    FDGUIxWaitCursor: TFDGUIxWaitCursor;
    SmallImage: TcxImageList;
    LargeImage: TcxImageList;
    prGetID: TFDStoredProc;
    prGetIDNUM: TIntegerField;
    quS: TFDQuery;
    stlRepColors: TcxStyleRepository;
    stlLightBlue: TcxStyle;
    stlLightYellow: TcxStyle;
    stRedSegoe: TcxStyle;
    stClientsTCK: TcxStyle;
    stZakazTCK: TcxStyle;
    stFileTCK: TcxStyle;
    stRepDocs: TcxStyle;
    stInvTO: TcxStyle;
    stBold: TcxStyle;
    SaveDialog: TSaveDialog;
    OpenDialog: TOpenDialog;
  private
    { Private declarations }
  public
    { Public declarations }
    function DBConnectUDL: boolean;
  end;

type
  TSupportedExportType = (exHTML, exXML, exExcel97, exExcel, exPDF, exText);


function fGetId(iT:Integer):Integer;
procedure ExportGridToFile(afilename: string; cxGrid: TcxGrid);


var
  dmS: TdmS;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure ExportGridToFile(afilename: string; cxGrid: TcxGrid);
var AExportType: TSupportedExportType;
    Ext:String;
begin
  AExportType := exExcel97;
  dmS.SaveDialog.FileName := afilename;
  if dmS.SaveDialog.Execute then begin
    case dmS.SaveDialog.FilterIndex of
      1: begin AExportType := exExcel97; Ext := 'xls'; end;
      2: begin AExportType := exText; Ext := 'csv'; end;
    end;

    if UpperCase(Copy(dmS.SaveDialog.FileName,Length(dmS.SaveDialog.FileName)-2,3)) <> UpperCase(Ext) then
      dmS.SaveDialog.FileName := dmS.SaveDialog.FileName + '.' + Ext;

    case AExportType of
      exHTML:
        ExportGridToHTML(dmS.SaveDialog.FileName, cxGrid);
      exXML:
        ExportGridToXML(dmS.SaveDialog.FileName, cxGrid);
      exExcel97:
        ExportGridToExcel(dmS.SaveDialog.FileName, cxGrid);
      exExcel:
        ExportGridToXLSX(dmS.SaveDialog.FileName, cxGrid);
      exText:
        ExportGridToText(dmS.SaveDialog.FileName, cxGrid);
    end;
  end;
end;


function fGetId(iT:Integer):Integer;
begin
 with dmS do
 begin
  Result:=0;
  try
    prGetId.Params.ParamByName('@ITYPE').Value:=iT;
    prGetID.Active:=true;
    Result:=prGetIDNUM.asInteger;
    prGetID.Active:=false;
  except
//    MessageDlg('�������� ������ ��� ���������� ���� (fDeborGetId)',mtError,[mbOk],0);
    Result:=0;
  end;
 end;
end;


function  TdmS.DBConnectUDL: boolean;
var UDLData: TStrings;
    Params: TStrings;
    CurDir : string;
    Database, Server, User_Name, Password: string;
begin
  Result:=False;
  //����� �� ��������� ����� ���� � ����������� ���������� MSSQL � FireDAC
  //c������ ������ ������������ ����� udl �� ADO
  CurDir := ExtractFilePath(ParamStr(0));
  if FileExists(CurDir+'ord.udl')=False then Exit;

  UDLData := TStringList.Create;
  Params := TStringList.Create;
  try
    UDLData.LoadFromFile(CurDir+'ord.udl');

    //<--������ ��������� ����������� ;
    Params.Delimiter:=';';
    Params.StrictDelimiter := True;
    Params.DelimitedText:=UDLData.Text;
    Server:=trim(Params.Values['Data Source']);
    Database:=trim(Params.Values['Initial Catalog']);
    User_Name:=trim(Params.Values['User ID']);
    Password:=trim(Params.Values['Password']);
    //-->

    //<--��������� ��������� ����������
    FDConnection.Close;
    FDConnection.Params.Values['SERVER']:=Server;
    FDConnection.Params.Values['DATABASE']:=Database;
    FDConnection.Params.Values['User_Name']:=User_Name;
    FDConnection.Params.Values['Password']:=Password;
    FDConnection.Params.Values['ApplicationName']:='rn_orders';
    FDConnection.Params.Values['Workstation']:='';
    //FDConnection.Params.Values['MARS']:='yes';
    FDConnection.Params.Values['DriverID']:='MSSQL';
    FDConnection.Connected:=True;
    //-->

  finally
    UDLData.Free;
    Params.Free;
  end;

  Sleep(100);
  Result:=FDConnection.Connected;
end;


end.
