object fmAddCli: TfmAddCli
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  ClientHeight = 376
  ClientWidth = 469
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 308
    Width = 469
    Height = 68
    Align = alBottom
    BevelInner = bvLowered
    Color = 16765864
    ParentBackground = False
    TabOrder = 0
    ExplicitTop = 360
    ExplicitWidth = 459
    object cxButton1: TcxButton
      Left = 96
      Top = 16
      Width = 89
      Height = 41
      Caption = 'Ok'
      Default = True
      ModalResult = 1
      TabOrder = 0
    end
    object cxButton2: TcxButton
      Left = 264
      Top = 16
      Width = 89
      Height = 41
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
    end
  end
  object cxTextEdit1: TcxTextEdit
    Left = 128
    Top = 15
    TabOrder = 1
    Text = 'cxTextEdit1'
    Width = 297
  end
  object cxLabel1: TcxLabel
    Left = 24
    Top = 16
    Caption = #1060#1048#1054' '#1087#1086#1082#1091#1087#1072#1090#1077#1083#1103
    Transparent = True
  end
  object cxLabel2: TcxLabel
    Left = 24
    Top = 43
    Caption = #1058#1077#1083#1077#1092#1086#1085
    Transparent = True
  end
  object cxTextEdit2: TcxTextEdit
    Left = 128
    Top = 42
    TabOrder = 4
    Text = 'cxTextEdit2'
    Width = 297
  end
  object cxLabel3: TcxLabel
    Left = 24
    Top = 80
    Caption = #1044#1072#1090#1072' '#1088#1086#1078#1076#1077#1085#1080#1103
    Transparent = True
  end
  object cxDateEdit1: TcxDateEdit
    Left = 128
    Top = 78
    TabOrder = 6
    Width = 137
  end
  object cxLabel4: TcxLabel
    Left = 24
    Top = 107
    Caption = #1050#1086#1084#1087#1072#1085#1080#1103
    Transparent = True
  end
  object cxTextEdit3: TcxTextEdit
    Left = 128
    Top = 105
    TabOrder = 8
    Text = 'cxTextEdit3'
    Width = 297
  end
  object cxLabel5: TcxLabel
    Left = 24
    Top = 134
    Caption = #1058#1080#1087' '#1087#1086#1082#1091#1087#1072#1090#1077#1083#1103
    Transparent = True
  end
  object cxLookupComboBox1: TcxLookupComboBox
    Left = 128
    Top = 132
    Properties.KeyFieldNames = 'ID'
    Properties.ListColumns = <
      item
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        FieldName = 'NAME'
      end>
    Properties.ListSource = dsquCliTypes
    TabOrder = 10
    Width = 265
  end
  object cxLabel6: TcxLabel
    Left = 24
    Top = 161
    Caption = #1058#1080#1087' '#1087#1088#1086#1076#1091#1082#1090#1072
    Transparent = True
  end
  object cxLookupComboBox2: TcxLookupComboBox
    Left = 128
    Top = 159
    Properties.KeyFieldNames = 'ID'
    Properties.ListColumns = <
      item
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        FieldName = 'NAME'
      end>
    Properties.ListSource = dsquCardsTypes
    TabOrder = 12
    Width = 265
  end
  object cxLabel7: TcxLabel
    Left = 24
    Top = 202
    Caption = #1055#1077#1088#1080#1086#1076#1080#1095#1085#1086#1089#1090#1100' '#1080#1085#1092#1086#1088#1084#1080#1088#1086#1074#1072#1085#1080#1103
    Transparent = True
  end
  object cxLabel8: TcxLabel
    Left = 24
    Top = 228
    Caption = #1040#1076#1088#1077#1089' EMAIL'
    Transparent = True
  end
  object cxTextEdit4: TcxTextEdit
    Left = 128
    Top = 227
    TabOrder = 15
    Text = 'cxTextEdit4'
    Width = 265
  end
  object cxCheckBox1: TcxCheckBox
    Left = 32
    Top = 264
    Caption = #1040#1082#1090#1080#1074#1085#1099#1081
    Properties.ValueChecked = 1
    Properties.ValueUnchecked = 0
    TabOrder = 16
  end
  object cxCheckBox2: TcxCheckBox
    Left = 184
    Top = 264
    Caption = #1043#1086#1088#1103#1095#1080#1081
    Properties.ValueChecked = 1
    Properties.ValueUnchecked = 0
    TabOrder = 17
  end
  object cxLookupComboBox3: TcxLookupComboBox
    Left = 202
    Top = 200
    Properties.KeyFieldNames = 'ID'
    Properties.ListColumns = <
      item
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        FieldName = 'NAME'
      end>
    Properties.ListSource = dsquPeriods
    TabOrder = 18
    Width = 191
  end
  object quCliTypes: TFDQuery
    Connection = dmS.FDConnection
    Transaction = dmS.FDTrans
    UpdateTransaction = dmS.FDTransUpdate
    SQL.Strings = (
      'SELECT [ID],[NAME] FROM [dbo].[CLITYPES]')
    Left = 400
    Top = 208
    object quCliTypesID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quCliTypesNAME: TStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      Size = 150
    end
  end
  object dsquCliTypes: TDataSource
    DataSet = quCliTypes
    Left = 400
    Top = 256
  end
  object quCardsTypes: TFDQuery
    Connection = dmS.FDConnection
    SQL.Strings = (
      'SELECT [ID],[NAME] FROM [dbo].[CARDSCATS]')
    Left = 296
    Top = 208
    object quCardsTypesID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quCardsTypesNAME: TStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      Size = 150
    end
  end
  object dsquCardsTypes: TDataSource
    DataSet = quCardsTypes
    Left = 296
    Top = 256
  end
  object quPeriods: TFDQuery
    Connection = dmS.FDConnection
    Transaction = dmS.FDTrans
    UpdateTransaction = dmS.FDTransUpdate
    SQL.Strings = (
      'SELECT [ID],[NAME] FROM [dbo].[PERIODS]')
    Left = 136
    Top = 216
    object IntegerField1: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object StringField1: TStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      Size = 150
    end
  end
  object dsquPeriods: TDataSource
    DataSet = quPeriods
    Left = 136
    Top = 264
  end
end
