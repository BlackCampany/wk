object fmUserId: TfmUserId
  Left = 0
  Top = 0
  Caption = #1048#1076#1077#1085#1090#1080#1092#1080#1082#1072#1094#1080#1103
  ClientHeight = 170
  ClientWidth = 307
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 151
    Width = 307
    Height = 19
    Panels = <
      item
        Width = 300
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 98
    Width = 307
    Height = 53
    Align = alBottom
    BevelInner = bvLowered
    Color = clWhite
    ParentBackground = False
    TabOrder = 1
    ExplicitTop = 96
    ExplicitWidth = 298
    object Button1: TcxButton
      Left = 40
      Top = 11
      Width = 90
      Height = 30
      Caption = #1054#1082
      Colors.Default = 16776176
      Colors.Normal = 16776176
      Default = True
      LookAndFeel.Kind = lfUltraFlat
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button2: TcxButton
      Left = 160
      Top = 11
      Width = 90
      Height = 30
      Caption = #1054#1090#1084#1077#1085#1072
      Colors.Default = 16776176
      Colors.Normal = 16776176
      LookAndFeel.Kind = lfUltraFlat
      ModalResult = 2
      TabOrder = 1
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 307
    Height = 98
    Align = alClient
    Caption = #1042#1099#1073#1077#1088#1080#1090#1077' '#1087#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1103' '#1080' '#1074#1074#1077#1076#1080#1090#1077' '#1087#1072#1088#1086#1083#1100
    Color = 16765864
    ParentBackground = False
    ParentColor = False
    TabOrder = 2
    ExplicitLeft = 8
    ExplicitTop = 1
    ExplicitWidth = 298
    ExplicitHeight = 96
    object Label1: TLabel
      Left = 40
      Top = 59
      Width = 37
      Height = 13
      Caption = #1055#1072#1088#1086#1083#1100
    end
    object ComboBox1: TcxLookupComboBox
      Left = 56
      Top = 29
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Caption = #1048#1084#1103
          Width = 100
          FieldName = 'NAME'
        end>
      Properties.ListSource = dsPer
      Style.BorderColor = clMenuHighlight
      Style.Color = 16776176
      Style.LookAndFeel.Kind = lfUltraFlat
      Style.Shadow = True
      StyleDisabled.LookAndFeel.Kind = lfUltraFlat
      StyleFocused.LookAndFeel.Kind = lfUltraFlat
      StyleHot.LookAndFeel.Kind = lfUltraFlat
      TabOrder = 0
      Width = 217
    end
    object Edit1: TcxTextEdit
      Left = 96
      Top = 56
      Properties.EchoMode = eemPassword
      Style.Color = 16776176
      Style.Shadow = True
      TabOrder = 1
      Text = '111111111111'
      Width = 177
    end
  end
  object quPer: TpFIBDataSet
    SelectSQL.Strings = (
      'select * from rpersonal'
      'where uvolnen=1 and modul1=0'
      'order by Name')
    Transaction = dmFb.trSelect
    Database = dmFb.CasherRnDb
    UpdateTransaction = dmFb.trUpdate
    Left = 24
    Top = 8
    object quPerID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quPerID_PARENT: TFIBIntegerField
      FieldName = 'ID_PARENT'
    end
    object quPerNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object quPerUVOLNEN: TFIBBooleanField
      FieldName = 'UVOLNEN'
    end
    object quPerCheck: TFIBStringField
      FieldName = 'PASSW'
      Transliterate = False
      EmptyStrToNull = True
    end
    object quPerMODUL1: TFIBBooleanField
      FieldName = 'MODUL1'
    end
    object quPerMODUL2: TFIBBooleanField
      FieldName = 'MODUL2'
    end
    object quPerMODUL3: TFIBBooleanField
      FieldName = 'MODUL3'
    end
    object quPerMODUL4: TFIBBooleanField
      FieldName = 'MODUL4'
    end
    object quPerMODUL5: TFIBBooleanField
      FieldName = 'MODUL5'
    end
    object quPerMODUL6: TFIBBooleanField
      FieldName = 'MODUL6'
    end
  end
  object dsPer: TDataSource
    DataSet = quPer
    Left = 24
    Top = 64
  end
end
