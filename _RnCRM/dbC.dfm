object dmC: TdmC
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 486
  Width = 835
  object CasherRnDb: TpFIBDatabase
    DBName = 'localhost:D:\_CasherRn\DBCONK\CASHERRN.gdb'
    DBParams.Strings = (
      'user_name=SYSDBA'
      'lc_ctype=WIN1251'
      'password=masterkey'
      'sql_role_name=SYSDBA')
    DefaultTransaction = trSelect
    DefaultUpdateTransaction = trUpdate
    SQLDialect = 3
    Timeout = 0
    SynchronizeTime = False
    DesignDBOptions = []
    AliasName = 'CasherRnDb1'
    WaitForRestoreConnect = 20
    Left = 24
    Top = 16
  end
  object trSelect: TpFIBTransaction
    DefaultDatabase = CasherRnDb
    Left = 24
    Top = 72
  end
  object trUpdate: TpFIBTransaction
    DefaultDatabase = CasherRnDb
    Left = 24
    Top = 128
  end
end
