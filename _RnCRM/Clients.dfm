object fmClients: TfmClients
  Left = 0
  Top = 0
  Hint = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082' '#1074#1077#1089#1086#1074
  Caption = #1055#1086#1082#1091#1087#1072#1090#1077#1083#1080
  ClientHeight = 445
  ClientWidth = 1279
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1279
    Height = 122
    ApplicationButton.Visible = False
    BarManager = dxBarManager1
    Style = rs2016
    ColorSchemeAccent = rcsaBlue
    ColorSchemeName = 'Colorful'
    ShowMinimizeButton = False
    SupportNonClientDrawing = True
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object rtScales: TdxRibbonTab
      Active = True
      Caption = #1055#1086#1082#1091#1087#1072#1090#1077#1083#1080
      Groups = <
        item
          ToolbarName = 'bmAction'
        end
        item
          ToolbarName = 'bmClose'
        end>
      Index = 0
    end
  end
  object sbSprScale: TdxRibbonStatusBar
    Left = 0
    Top = 422
    Width = 1279
    Height = 23
    Panels = <>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ExplicitWidth = 1230
  end
  object GrClients: TcxGrid
    Left = 0
    Top = 122
    Width = 1279
    Height = 300
    Align = alClient
    TabOrder = 6
    ExplicitTop = 91
    ExplicitHeight = 325
    object ViewClients: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.PriorPage.Visible = True
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.InfoPanel.Visible = True
      Navigator.Visible = True
      OnCustomDrawCell = ViewClientsCustomDrawCell
      DataController.DataSource = dsquOrdClients
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
        end
        item
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      object ViewClientsIDCLI: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1087#1086#1082#1091#1087#1072#1090#1077#1083#1103
        DataBinding.FieldName = 'IDCLI'
        Width = 49
      end
      object ViewClientsCLITYPE: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1090#1080#1087#1072
        DataBinding.FieldName = 'CLITYPE'
        Visible = False
      end
      object ViewClientsCLITYPEN: TcxGridDBColumn
        Caption = #1058#1080#1087' '#1087#1086#1082#1091#1087#1072#1090#1077#1083#1103
        DataBinding.FieldName = 'CLITYPEN'
        Width = 94
      end
      object ViewClientsNAME: TcxGridDBColumn
        Caption = #1060#1048#1054
        DataBinding.FieldName = 'NAME'
        Width = 180
      end
      object ViewClientsPHONE: TcxGridDBColumn
        Caption = #1058#1077#1083#1077#1092#1086#1085' '#8470' '
        DataBinding.FieldName = 'PHONE'
        Width = 154
      end
      object ViewClientsDBURN: TcxGridDBColumn
        Caption = #1044#1072#1090#1072' '#1088#1086#1078#1076#1077#1085#1080#1103
        DataBinding.FieldName = 'DBURN'
        Width = 127
      end
      object ViewClientsCOMPANY: TcxGridDBColumn
        Caption = #1050#1086#1084#1087#1072#1085#1080#1103
        DataBinding.FieldName = 'COMPANY'
        Width = 117
      end
      object ViewClientsCARDSCAT: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1090#1080#1087#1072' '#1087#1088#1086#1076#1091#1082#1090#1072
        DataBinding.FieldName = 'CARDSCAT'
        Visible = False
      end
      object ViewClientsCARDSCATN: TcxGridDBColumn
        Caption = #1058#1080#1087' '#1087#1088#1086#1076#1091#1082#1090#1072
        DataBinding.FieldName = 'CARDSCATN'
        Width = 169
      end
      object ViewClientsPERIODRING: TcxGridDBColumn
        Caption = #1055#1077#1088#1080#1086#1076#1080#1095#1085#1086#1089#1090#1100' '#1080#1085#1092#1086#1088#1084#1080#1088#1086#1074#1072#1085#1080#1103
        DataBinding.FieldName = 'PERIODRING'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Items = <
          item
            Description = #1053#1080#1082#1086#1075#1076#1072
            ImageIndex = 0
            Value = 0
          end
          item
            Description = #1056#1072#1079' '#1074' '#1085#1077#1076#1077#1083#1102
            Value = 1
          end
          item
            Description = #1056#1072#1079' '#1074' '#1084#1077#1089#1103#1094
            Value = 2
          end
          item
            Description = #1056#1072#1079' '#1074' '#1082#1074#1072#1088#1090#1072#1083
            Value = 3
          end
          item
            Description = #1056#1072#1079' '#1074' '#1075#1086#1076
            Value = 4
          end>
        Width = 103
      end
      object ViewClientsSEMAIL: TcxGridDBColumn
        Caption = 'EMAIL '#1072#1076#1088#1077#1089
        DataBinding.FieldName = 'SEMAIL'
        Width = 118
      end
      object ViewClientsIACTIVE: TcxGridDBColumn
        Caption = #1057#1090#1072#1090#1091#1089
        DataBinding.FieldName = 'IACTIVE'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Images = dmS.SmallImage
        Properties.Items = <
          item
            Description = #1040#1082#1090#1080#1074#1085#1099#1081
            ImageIndex = 81
            Value = 1
          end
          item
            Description = #1085#1077#1072#1082#1090#1080#1074#1085#1099#1081
            ImageIndex = 82
            Value = 0
          end>
        Properties.LargeImages = dmS.LargeImage
      end
      object ViewClientsHOTCLI: TcxGridDBColumn
        Caption = #1043#1086#1088#1103#1095#1080#1081
        DataBinding.FieldName = 'HOTCLI'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Images = dmS.SmallImage
        Properties.Items = <
          item
            Description = '  '
            ImageIndex = 81
            Value = 1
          end
          item
            Description = '     '
            ImageIndex = 80
            Value = 0
          end>
        Properties.LargeImages = dmS.LargeImage
      end
    end
    object LevelClients: TcxGridLevel
      GridView = ViewClients
    end
  end
  object ActionList1: TActionList
    Left = 420
    Top = 140
    object acRefresh: TAction
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100' (F5)'
      Checked = True
      Hint = #1054#1073#1085#1086#1074#1080#1090#1100' (F5)'
      ImageIndex = 5
      ShortCut = 116
      OnExecute = acRefreshExecute
    end
    object acClose: TAction
      Caption = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 100
      OnExecute = acCloseExecute
    end
    object acAdd: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ImageIndex = 75
      OnExecute = acAddExecute
    end
    object acEdit: TAction
      Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100
      ImageIndex = 73
      OnExecute = acEditExecute
    end
    object acDel: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ImageIndex = 74
      OnExecute = acDelExecute
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      #1044#1077#1081#1089#1090#1074#1080#1103)
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmS.SmallImage
    ImageOptions.LargeImages = dmS.LargeImage
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 344
    Top = 172
    DockControlHeights = (
      0
      0
      0
      0)
    object bmAction: TdxBar
      Caption = #1044#1077#1081#1089#1090#1074#1080#1103
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 778
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'bUpdate'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'btnAdd'
        end
        item
          Visible = True
          ItemName = 'btnEdit'
        end
        item
          Visible = True
          ItemName = 'btnDel'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object bmClose: TdxBar
      Caption = #1047#1072#1082#1088#1099#1090#1100
      CaptionButtons = <>
      DockedLeft = 311
      DockedTop = 0
      FloatLeft = 778
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'bClose'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object bUpdate: TdxBarLargeButton
      Action = acRefresh
      Category = 0
      SyncImageIndex = False
      ImageIndex = 5
    end
    object bClose: TdxBarLargeButton
      Action = acClose
      Category = 0
    end
    object btnAdd: TdxBarLargeButton
      Action = acAdd
      Category = 0
    end
    object btnDel: TdxBarLargeButton
      Action = acDel
      Category = 0
    end
    object btnEdit: TdxBarLargeButton
      Action = acEdit
      Category = 0
    end
  end
  object JvFormStorage: TJvFormStorage
    AppStorage = fmMain.MainAppIniFileStorage
    AppStoragePath = 'SprScales\'
    Options = [fpSize, fpLocation]
    StoredValues = <>
    Left = 412
    Top = 236
  end
  object quOrdClients: TFDQuery
    Connection = dmS.FDConnection
    Transaction = dmS.FDTrans
    UpdateTransaction = dmS.FDTransUpdate
    SQL.Strings = (
      'SELECT cli.[IDCLI]'
      '      ,cli.[CLITYPE]'
      #9'  ,clit.[NAME] as CLITYPEN'
      '      ,cli.[NAME]'
      '      ,cli.[PHONE]'
      '      ,cli.[DBURN]'
      '      ,cli.[COMPANY]'
      '      ,cli.[CARDSCAT]'
      #9'  ,cat.[NAME] as CARDSCATN'
      #9'  ,cli.[PERIODRING]'
      '      ,cli.[SEMAIL]'
      '      ,cli.[IACTIVE]'
      '      ,cli.[HOTCLI]'
      '      ,cli.[DEL]'
      '  FROM [dbo].[ORDERSCLI] cli'
      '  left join [dbo].[CLITYPES] clit on clit.ID=cli.[CLITYPE]'
      '  left join [dbo].[CARDSCATS] cat on cat.ID=cli.[CARDSCAT]'
      'where cli.[DEL]=0')
    Left = 48
    Top = 176
    object quOrdClientsIDCLI: TLargeintField
      FieldName = 'IDCLI'
      Origin = 'IDCLI'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quOrdClientsCLITYPE: TIntegerField
      FieldName = 'CLITYPE'
      Origin = 'CLITYPE'
    end
    object quOrdClientsCLITYPEN: TStringField
      FieldName = 'CLITYPEN'
      Origin = 'CLITYPEN'
      Size = 150
    end
    object quOrdClientsNAME: TStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      Size = 200
    end
    object quOrdClientsPHONE: TStringField
      FieldName = 'PHONE'
      Origin = 'PHONE'
      Size = 200
    end
    object quOrdClientsDBURN: TSQLTimeStampField
      FieldName = 'DBURN'
      Origin = 'DBURN'
    end
    object quOrdClientsCOMPANY: TStringField
      FieldName = 'COMPANY'
      Origin = 'COMPANY'
      Size = 300
    end
    object quOrdClientsCARDSCAT: TIntegerField
      FieldName = 'CARDSCAT'
      Origin = 'CARDSCAT'
    end
    object quOrdClientsCARDSCATN: TStringField
      FieldName = 'CARDSCATN'
      Origin = 'CARDSCATN'
      Size = 150
    end
    object quOrdClientsPERIODRING: TSmallintField
      FieldName = 'PERIODRING'
      Origin = 'PERIODRING'
    end
    object quOrdClientsSEMAIL: TStringField
      FieldName = 'SEMAIL'
      Origin = 'SEMAIL'
      Size = 150
    end
    object quOrdClientsIACTIVE: TSmallintField
      FieldName = 'IACTIVE'
      Origin = 'IACTIVE'
    end
    object quOrdClientsHOTCLI: TSmallintField
      FieldName = 'HOTCLI'
      Origin = 'HOTCLI'
    end
    object quOrdClientsDEL: TSmallintField
      FieldName = 'DEL'
      Origin = 'DEL'
    end
  end
  object dsquOrdClients: TDataSource
    DataSet = quOrdClients
    Left = 48
    Top = 224
  end
end
