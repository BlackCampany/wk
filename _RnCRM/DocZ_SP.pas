unit DocZ_SP;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  CustomChildForm, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer,
  cxTextEdit, cxCalendar, cxCheckBox, cxDBLookupComboBox, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error,
  FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Vcl.Menus, cxClasses, FireDAC.Comp.Client,
  FireDAC.Comp.DataSet, System.Actions, Vcl.ActnList, dxBar, cxBarEditItem, cxMemo, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridCustomView, cxGrid, dxRibbon, cxDropDownEdit, cxCalc, dxBarExtItems, Vcl.ComCtrls, dxCore, cxDateUtils, cxCurrencyEdit, cxSpinEdit, cxTimeEdit,
  cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxLabel, Vcl.StdCtrls, cxButtons, Vcl.ExtCtrls;

type
  TfmDocZ_SP = class(TfmCustomChildForm)
    ActionList1: TActionList;
    acClose: TAction;
    acSave: TAction;
    quSpecZ: TFDQuery;
    UpdSQL1: TFDUpdateSQL;
    dsquSpecZ: TDataSource;
    ViewSpecZ: TcxGridDBTableView;
    LevelSpecZ: TcxGridLevel;
    GridSpecZ: TcxGrid;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    PopupMenu1: TPopupMenu;
    cxStyle2: TcxStyle;
    quE: TFDQuery;
    acTestSpec: TAction;
    acDelPos: TAction;
    N1: TMenuItem;
    acGetInfoSel: TAction;
    N2: TMenuItem;
    N3: TMenuItem;
    acAddPos: TAction;
    N4: TMenuItem;
    quS: TFDQuery;
    quCli: TFDQuery;
    dsquCli: TDataSource;
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxLabel1: TcxLabel;
    cxLookupComboBox1: TcxLookupComboBox;
    cxLabel2: TcxLabel;
    cxDateEdit1: TcxDateEdit;
    cxTimeEdit1: TcxTimeEdit;
    cxLabel3: TcxLabel;
    cxDateEdit2: TcxDateEdit;
    cxTimeEdit2: TcxTimeEdit;
    cxButton3: TcxButton;
    cxLabel4: TcxLabel;
    cxCurrencyEdit1: TcxCurrencyEdit;
    cxCurrencyEdit2: TcxCurrencyEdit;
    cxLabel5: TcxLabel;
    cxLabel6: TcxLabel;
    cxTextEdit1: TcxTextEdit;
    cxLabel7: TcxLabel;
    cxTextEdit2: TcxTextEdit;
    cxLabel8: TcxLabel;
    cxLabel9: TcxLabel;
    cxLabel10: TcxLabel;
    cxTextEdit4: TcxTextEdit;
    quSpecZIDH: TLargeintField;
    quSpecZID: TLargeintField;
    quSpecZNUM: TIntegerField;
    quSpecZICODE: TIntegerField;
    quSpecZNAMECARD: TStringField;
    quSpecZVESZ: TSingleField;
    quSpecZQUANTZ: TIntegerField;
    quSpecZVESPRO: TSingleField;
    quSpecZQUANTPRO: TIntegerField;
    quSpecZVESSEND: TSingleField;
    quSpecZQUANTSEND: TIntegerField;
    quSpecZPRICE: TSingleField;
    quSpecZRSUM: TSingleField;
    ViewSpecZNUM: TcxGridDBColumn;
    ViewSpecZICODE: TcxGridDBColumn;
    ViewSpecZNAMECARD: TcxGridDBColumn;
    ViewSpecZVESZ: TcxGridDBColumn;
    ViewSpecZQUANTZ: TcxGridDBColumn;
    ViewSpecZVESPRO: TcxGridDBColumn;
    ViewSpecZQUANTPRO: TcxGridDBColumn;
    ViewSpecZVESSEND: TcxGridDBColumn;
    ViewSpecZQUANTSEND: TcxGridDBColumn;
    ViewSpecZPRICE: TcxGridDBColumn;
    ViewSpecZRSUM: TcxGridDBColumn;
    quCliIDCLI: TLargeintField;
    quCliCLITYPE: TIntegerField;
    quCliCLITYPEN: TStringField;
    quCliNAME: TStringField;
    quCliPHONE: TStringField;
    quCliDBURN: TSQLTimeStampField;
    quCliCOMPANY: TStringField;
    quCliCARDSCAT: TIntegerField;
    quCliCARDSCATN: TStringField;
    quCliPERIODRING: TSmallintField;
    quCliSEMAIL: TStringField;
    quCliIACTIVE: TSmallintField;
    quCliHOTCLI: TSmallintField;
    quCliDEL: TSmallintField;
    cxLookupComboBox2: TcxLookupComboBox;
    cxLookupComboBox3: TcxLookupComboBox;
    quSourceInfo: TFDQuery;
    dsquSourceInfo: TDataSource;
    quPovod: TFDQuery;
    dsquPovod: TDataSource;
    quSourceInfoID: TIntegerField;
    quSourceInfoNAME: TStringField;
    quPovodID: TIntegerField;
    quPovodNAME: TStringField;
    cxButton4: TcxButton;
    cxButton5: TcxButton;
    procedure acCloseExecute(Sender: TObject);
    procedure acSaveExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acDelPosExecute(Sender: TObject);
    procedure acAddPosExecute(Sender: TObject);
  private
    { Private declarations }
    FRARID:string;
    FIDATE: Integer;
    FSID:string;
    FIEDIT: Integer;
    FNUMBER:string;
    FDESCR:string;
    FIADD:Integer;
    FIDH:Integer;

    procedure Init;

  public
    { Public declarations }
  end;

// var
//  fmDocSpec: TfmDocSpec;

procedure ShowSpecZ(IDH,IEDIT,IADD:Integer);

implementation

{$R *.dfm}

uses dbs, Un2, DocZ_HD;

procedure ShowSpecZ(IDH,IEDIT,IADD:Integer);
var F:TfmDocZ_SP;
begin
  F:=TfmDocZ_SP.Create(Application);
  F.FIEDIT:=IEDIT;
  F.FIADD:=IADD;
  F.FIDH:=IDH;

  F.Init;
  F.Show;
end;

procedure TfmDocZ_SP.acAddPosExecute(Sender: TObject);
begin
  //�������� �������
  if FIEDIT=1 then
  begin
    {
    //�������� �������
    if Assigned(fmACard)=False then
    begin //����� �� ����������, � ����� �������
      fmACard:=TfmACard.Create(Application);
      fmACard.Init;
      fmACard.FRARID:=FRARID;
      fmACard.FIDATE:=FIDATE;
      fmACard.FSID:=FSID;
      fmACard.STYPE:='CORR';
      fmACard.quSpec:=quSpecCorr;
      fmACard.Show;
    end else
    begin
      fmACard.FRARID:=FRARID;
      fmACard.FIDATE:=FIDATE;
      fmACard.FSID:=FSID;
      fmACard.STYPE:='CORR';
      fmACard.quSpec:=quSpecCorr;
      fmACard.Show;
    end;
    }
    {
    if not Assigned(fmAddPosCorr) then fmAddPosCorr:=TfmAddPosCorr.Create(Application,TFormStyle.fsNormal);

    fmAddPosCorr.ShowModal;
    if fmAddPosCorr.ModalResult=mrOk then
    begin //�������� �������
      with fmAddPosCorr do
      with dmR do
      begin
        quSelCard.Active:=False;
        quSelCard.ParamByName('CODE').AsString:=fmAddPosCorr.cxButtonEdit1.Text;
        quSelCard.Active:=True;

        if quSelCard.RecordCount=1 then
        begin
        end;

        quSelCard.Active:=False;
      end;
    end;}
  end;

end;

procedure TfmDocZ_SP.acCloseExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmDocZ_SP.acDelPosExecute(Sender: TObject);
begin
  if FIEDIT=1 then
  begin
//    quSpecCorr.Delete;
  end;
end;

procedure TfmDocZ_SP.acSaveExecute(Sender: TObject);
var iErrors:Integer;
begin
  //��������� ������������
  if FIADD=1 then
  begin
    try
      with dmS do
      begin
        quS.Active:=False;
        quS.SQL.Clear;
        quS.SQL.Add('');

        quS.SQL.Add('declare @IDATE int = '+its(Trunc(date)));
        quS.SQL.Add('declare @ID bigint = '+its(FIDH));
        quS.SQL.Add('declare @TIMEPRO datetime = '''+dssqltime(Trunc(cxDateEdit1.Date)+Frac(cxTimeEdit1.Time))+'''');
        quS.SQL.Add('declare @TIMEDOST datetime = '''+dssqltime(Trunc(cxDateEdit2.Date)+Frac(cxTimeEdit2.Time))+'''');
        quS.SQL.Add('declare @IDCLI int = '+its(cxLookupComboBox1.EditValue));
        quS.SQL.Add('declare @PHONE varchar(100) = '''+cxTextEdit1.Text+'''');
        quS.SQL.Add('declare @ADDRES varchar(200) = '''+cxTextEdit2.Text+'''');
        quS.SQL.Add('declare @IDFROM int = '+its(cxLookupComboBox2.EditValue));
        quS.SQL.Add('declare @POVOD int = '+its(cxLookupComboBox3.EditValue));
        quS.SQL.Add('declare @ISTATUS int = 0');
        quS.SQL.Add('declare @COMMENT varchar(200) = '''+cxTextEdit4.Text+'''');
        quS.SQL.Add('declare @SUMDOSTAV decimal(18,2) = '+fts(cxCurrencyEdit1.Value));

        quS.SQL.Add('INSERT INTO [dbo].[DOCZ_HD]');
        quS.SQL.Add('           ([IDATE],[ID],[TIMEPRO],[TIMEDOST],[IDCLI],[PHONE],[ADDRES],[IDFROM],[POVOD],[ISTATUS],[COMMENT],[SUMDOSTAV])');
        quS.SQL.Add('   VALUES  (@IDATE,@ID,@TIMEPRO,@TIMEDOST,@IDCLI,@PHONE,@ADDRES,@IDFROM,@POVOD,@ISTATUS,@COMMENT,@SUMDOSTAV)');

        quS.ExecSQL;

        FIADD:=0;
      end;
    except
    end;
  end;

  if FIADD=0 then
  begin
    quSpecZ.UpdateTransaction.StartTransaction;
    iErrors := quSpecZ.ApplyUpdates;
    if iErrors = 0 then begin
      try
        quSpecZ.CommitUpdates;
        quSpecZ.UpdateTransaction.Commit;

        quE.Active:=False;
        quE.SQL.Clear;

        quE.SQL.Add('UPDATE [dbo].[DOCZ_HD]');
        quE.SQL.Add('   SET [TIMEPRO] = '''+dssqltime(Trunc(cxDateEdit1.Date)+Frac(cxTimeEdit1.Time))+'''');
        quE.SQL.Add('      ,[TIMEDOST] = '''+dssqltime(Trunc(cxDateEdit2.Date)+Frac(cxTimeEdit2.Time))+'''');
        quE.SQL.Add('      ,[IDCLI] = '+its(cxLookupComboBox1.EditValue));
        quE.SQL.Add('      ,[PHONE] = '''+cxTextEdit1.Text+'''');
        quE.SQL.Add('      ,[ADDRES] = '''+cxTextEdit2.Text+'''');
        quE.SQL.Add('      ,[IDFROM] = '+its(cxLookupComboBox2.EditValue));
        quE.SQL.Add('      ,[POVOD] = '+its(cxLookupComboBox3.EditValue));
        quE.SQL.Add('      ,[COMMENT] = '''+cxTextEdit4.Text+'''');
        quE.SQL.Add('      ,[SUMDOSTAV] = '+fts(cxCurrencyEdit1.Value));
        quE.SQL.Add(' WHERE ID='+its(FIDH));

        quE.ExecSQL;

        if Assigned(fmDocZ_HD) then
          if fmDocZ_HD.Showing then fmDocZ_HD.Init;
      except
        ShowMessage('������ ����������..');
      end;
    end else
    begin
      ShowMessage('������ ����������.');
      quSpecZ.UpdateTransaction.Rollback;
    end;

  end;
end;

procedure TfmDocZ_SP.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  quCli.Active:=False;
  quPovod.Active:=False;
  quSourceInfo.Active:=False;
  Action:=caFree;
end;

procedure TfmDocZ_SP.Init;
begin

  quCli.Active:=False;
  quCli.Active:=True;
  quCli.First;

  quSourceInfo.Active:=False;
  quSourceInfo.Active:=True;
  quSourceInfo.First;

  quPovod.Active:=False;
  quPovod.Active:=True;
  quPovod.First;

//  cxLookupComboBox3.EditValue:=quPovodID.AsInteger;

  if FIADD=1 then
  begin
    Caption:='���������� ������.';

    cxDateEdit1.Date:=Date;
    cxDateEdit2.Date:=Date;
    cxTimeEdit1.Time :=Time;
    cxTimeEdit2.Time:=Time;

    cxTextEdit1.Text:='';
    cxTextEdit2.Text:='';
    cxTextEdit4.Text:='';

    cxCurrencyEdit1.Value:=0;
    cxCurrencyEdit2.Value:=0;
  end;

  if FIADD=0 then
  begin
    Caption:='������������ ������ ('+its(FIDH)+')';

    if FIDH>0 then
    begin
      quS.Active:=False;
      quS.SQL.Clear;
      quS.SQL.Add('');
      quS.SQL.Add('SELECT TOP 1 [IDATE],[ID],[TIMEPRO],[TIMEDOST],[IDCLI],[PHONE],[ADDRES],[IDFROM],[POVOD],[ISTATUS],[COMMENT],[SUMDOSTAV]');
      quS.SQL.Add('  FROM [RNORDERS].[dbo].[DOCZ_HD]');
      quS.SQL.Add('  where ID='+its(FIDH));
      quS.Active:=True;

      cxDateEdit1.Date:=Trunc(quS.FieldByName('TIMEPRO').AsDateTime);
      cxDateEdit2.Date:=Trunc(quS.FieldByName('TIMEDOST').AsDateTime);
      cxTimeEdit1.Time:=Frac(quS.FieldByName('TIMEPRO').AsDateTime);
      cxTimeEdit2.Time:=Frac(quS.FieldByName('TIMEDOST').AsDateTime);

      cxTextEdit1.Text:=quS.FieldByName('PHONE').AsString;
      cxTextEdit2.Text:=quS.FieldByName('ADDRES').AsString;
      cxTextEdit4.Text:=quS.FieldByName('COMMENT').AsString;

      cxLookupComboBox1.EditValue:=quS.FieldByName('IDCLI').AsInteger;
      cxLookupComboBox2.EditValue:=quS.FieldByName('IDFROM').AsInteger;
      cxLookupComboBox3.EditValue:=quS.FieldByName('POVOD').AsInteger;

      cxCurrencyEdit1.Value:=quS.FieldByName('SUMDOSTAV').AsFloat;
      cxCurrencyEdit2.Value:=0;

      quS.Active:=False;
    end;
  end;

  ViewSpecZ.BeginUpdate;
  quSpecZ.Active:=False;
  quSpecZ.ParamByName('IDH').AsInteger:=FIDH;
  quSpecZ.Active:=True;
  ViewSpecZ.EndUpdate;

  if FIEDIT=1 then
  begin
    acSave.Enabled:=True;
  end else
  begin
    acSave.Enabled:=False;
  end;
end;

end.
