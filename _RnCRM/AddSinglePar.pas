unit AddSinglePar;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus, cxControls, cxContainer, cxEdit, Vcl.StdCtrls, cxTextEdit, cxButtons,
  Vcl.ExtCtrls;

type
  TfmAddSinglePar = class(TForm)
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxTextEdit1: TcxTextEdit;
    Label1: TLabel;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAddSinglePar: TfmAddSinglePar;

function ShowFormAddStrPar(iAdd:Integer;sVal:string;var sRet:String):Boolean;

implementation

{$R *.dfm}

function ShowFormAddStrPar(iAdd:Integer;sVal:string;var sRet:String):Boolean;
begin
  sRet:='';

  if Assigned(fmAddSinglePar)=False then begin //����� �� ����������, � ����� �������
    fmAddSinglePar:=TfmAddSinglePar.Create(Application);
  end;
  if iAdd=1 then fmAddSinglePar.cxTextEdit1.Text:=''
  else fmAddSinglePar.cxTextEdit1.Text:=sVal;

  fmAddSinglePar.ShowModal;
  if fmAddSinglePar.ModalResult=mrOk then
  begin
    sRet:=fmAddSinglePar.cxTextEdit1.Text;
    Result:=True;
  end else  Result:=False;
end;

procedure TfmAddSinglePar.FormShow(Sender: TObject);
begin
  cxTextEdit1.SetFocus;
  cxTextEdit1.SelectAll;
end;

end.
