﻿unit CLITYPES;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxRibbonSkins, dxRibbonCustomizationForm, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB,
  cxDBData, cxImageComboBox, Vcl.Menus, frxClass, frxDBSet, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, System.Actions, Vcl.ActnList, dxBar, dxBarExtItems,
  uCustomChildForm, dxRibbon, cxMRUEdit, cxBarEditItem,
  cxDBExtLookupComboBox, cxShellComboBox, cxCurrencyEdit, cxDropDownEdit,
  cxBlobEdit, dxRibbonGallery, dxSkinChooserGallery, cxCalendar, dmDocs,
  Shared_Functions, UnitFunction, cxCheckBox, cxGridCustomPopupMenu,
  cxGridPopupMenu, AddDocIn, dxStatusBar, dxRibbonStatusBar, dxRibbonRadialMenu,
  cxRadioGroup, dmMain, Shared_Types, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.Client,
  FireDAC.Comp.DataSet, cxTextEdit, Main, cxLabel, cxMaskEdit, cxContainer,
  System.Win.TaskbarCore, Vcl.Taskbar, cxDBLookupComboBox;

type
  TfmDocsIn = class(TCustomChildForm)
    dxRibbon1: TdxRibbon;
    rtDocsIn: TdxRibbonTab;
    dxBarManager1: TdxBarManager;
    bmAction: TdxBar;
    bmApplyDate: TdxBar;
    bmClose: TdxBar;
    bDone: TdxBarLargeButton;
    bClose: TdxBarLargeButton;
    ActionList1: TActionList;
    acClose: TAction;
    acSelectDate: TAction;
    bmManage: TdxBar;
    acEdit: TAction;
    acView: TAction;
    acDelete: TAction;
    acExpExcel: TAction;
    acOnDoc: TAction;
    acOffDoc: TAction;
    acViewForm: TAction;
    bViewForm: TdxBarLargeButton;
    acRegPrice: TAction;
    acInfList: TAction;
    acActRevPrice: TAction;
    chbView: TcxBarEditItem;
    cxGridPopupMenu1: TcxGridPopupMenu;
    bEditDoc: TdxBarLargeButton;
    bViewDoc: TdxBarLargeButton;
    bDelDoc: TdxBarLargeButton;
    bOnDoc: TdxBarLargeButton;
    bOffDoc: TdxBarLargeButton;
    bmPrint: TdxBar;
    dxBarSubItem1: TdxBarSubItem;
    acAddDoc: TAction;
    bAddDoc: TdxBarLargeButton;
    sbDocsIn: TdxRibbonStatusBar;
    quDocIn: TFDQuery;
    quDocInIDSKL: TIntegerField;
    quDocInDATEDOC: TSQLTimeStampField;
    quDocInNUMDOC: TStringField;
    quDocInNUMSF: TStringField;
    quDocInIDCLI: TIntegerField;
    quDocInIACTIVE: TSmallintField;
    quDocInDATESF: TSQLTimeStampField;
    quDocInID: TLargeintField;
    quDocInORDERN: TLargeintField;
    quDocInSUMIN: TFloatField;
    quDocInSUMR: TFloatField;
    quDocInSUMNAC: TFloatField;
    quDocInSUMNDS: TFloatField;
    quDocInNAMEPOST: TStringField;
    quDocInNAMESKL: TStringField;
    quDocInINNPOST: TStringField;
    quDocInIDATEDOC: TIntegerField;
    quDocInIDCLI_TO: TIntegerField;
    quDocInISHOP: TIntegerField;
    quDocInOPRIZN: TSmallintField;
    quDocInIDDOC: TStringField;
    dsDocIn: TDataSource;
    quDocInCards: TFDQuery;
    quDocInCardsIDSKL: TIntegerField;
    quDocInCardsDATEDOC: TSQLTimeStampField;
    quDocInCardsNUM: TIntegerField;
    quDocInCardsIDCARD: TIntegerField;
    quDocInCardsIDM: TIntegerField;
    quDocInCardsSBAR: TStringField;
    quDocInCardsNDSPROC: TSingleField;
    quDocInCardsSUMNDS: TSingleField;
    quDocInCardsQUANT: TSingleField;
    quDocInCardsQUANT_OTH: TSingleField;
    quDocInCardsPROC_OTH: TSingleField;
    quDocInCardsCardType: TIntegerField;
    quDocInCardsPRICEIN: TSingleField;
    quDocInCardsPRICEIN0: TSingleField;
    quDocInCardsPRICER: TSingleField;
    quDocInCardsSUMIN: TSingleField;
    quDocInCardsSUMR: TSingleField;
    quDocInCardsSUMIN0: TSingleField;
    quDocInCardsNUMSF: TStringField;
    quDocInCardsIDCLI: TIntegerField;
    quDocInCardsIACTIVE: TSmallintField;
    quDocInCardsDATESF: TSQLTimeStampField;
    quDocInCardsID: TLargeintField;
    quDocInCardsORDERN: TLargeintField;
    quDocInCardsNAMEPOST: TStringField;
    quDocInCardsNAMEDEP: TStringField;
    quDocInCardsINNPOST: TStringField;
    quDocInCardsNAMECARD: TStringField;
    quDocInCardsFULLNAME: TStringField;
    quDocInCardsSSNameGr: TStringField;
    quDocInCardsSNameGr: TStringField;
    quDocInCardsNameGr: TStringField;
    dsDocInCards: TDataSource;
    PopupMenu1: TPopupMenu;
    Excel1: TMenuItem;
    N2: TMenuItem;
    N16: TMenuItem;
    acPrintSCFK: TAction;
    acExitUsers: TAction;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    acActIn: TAction;
    dxBarLargeButton4: TdxBarLargeButton;
    frxRepDIN: TfrxReport;
    frxtaSp: TfrxDBDataset;
    frxtaSpTara: TfrxDBDataset;
    acPrintNACL: TAction;
    quDocInSp: TFDQuery;
    quDocInSpIDHEAD: TLargeintField;
    quDocInSpNUM: TIntegerField;
    quDocInSpIDCARD: TIntegerField;
    quDocInSpIDM: TIntegerField;
    quDocInSpSBAR: TStringField;
    quDocInSpNDSPROC: TSingleField;
    quDocInSpSUMNDS: TSingleField;
    quDocInSpQUANT: TSingleField;
    quDocInSpNAMEPOST: TStringField;
    quDocInSpISHOP: TIntegerField;
    quDocInSpQUANT_OTH: TSingleField;
    quDocInSpPROC_OTH: TSingleField;
    quDocInSpCardType: TIntegerField;
    quDocInSpPRICEIN: TSingleField;
    quDocInSpPRICEIN0: TSingleField;
    quDocInSpPRICER: TSingleField;
    quDocInSpSUMIN: TSingleField;
    quDocInSpSUMR: TSingleField;
    quDocInSpSUMIN0: TSingleField;
    quDocInSpNAMEDEP: TStringField;
    quDocInSpINNPOST: TStringField;
    quDocInSpNAMECARD: TStringField;
    quDocInSpFULLNAME: TStringField;
    quDocInSpID: TLargeintField;
    quDocInSpGTD: TStringField;
    quDocInSpQUANT_POST: TSingleField;
    quDocInSpPRICEIN_ZAK: TSingleField;
    quDocInSpPRICEIN0_ZAK: TSingleField;
    quDocInSpQUANT_ZAK: TSingleField;
    dsDocInSp: TDataSource;
    quDocInSpTara: TFDQuery;
    LargeintField1: TLargeintField;
    IntegerField1: TIntegerField;
    IntegerField2: TIntegerField;
    IntegerField3: TIntegerField;
    StringField1: TStringField;
    SingleField1: TSingleField;
    SingleField2: TSingleField;
    SingleField3: TSingleField;
    StringField2: TStringField;
    IntegerField4: TIntegerField;
    SingleField4: TSingleField;
    SingleField5: TSingleField;
    IntegerField5: TIntegerField;
    SingleField6: TSingleField;
    SingleField7: TSingleField;
    SingleField8: TSingleField;
    SingleField9: TSingleField;
    SingleField10: TSingleField;
    SingleField11: TSingleField;
    StringField3: TStringField;
    StringField4: TStringField;
    StringField5: TStringField;
    StringField6: TStringField;
    LargeintField2: TLargeintField;
    StringField10: TStringField;
    IntegerField6: TIntegerField;
    SingleField12: TSingleField;
    SingleField13: TSingleField;
    SingleField14: TSingleField;
    SingleField15: TSingleField;
    dsDocInSpTara: TDataSource;
    GridDocsIn: TcxGrid;
    ViewDocsIn: TcxGridDBTableView;
    ViewDocsInNAMESKL: TcxGridDBColumn;
    ViewDocsInIDSKL: TcxGridDBColumn;
    ViewDocsInNAMEPOST: TcxGridDBColumn;
    ViewDocsInDATEDOC: TcxGridDBColumn;
    ViewDocsInNUMDOC: TcxGridDBColumn;
    ViewDocsInNUMSF: TcxGridDBColumn;
    ViewDocsInDATESF: TcxGridDBColumn;
    ViewDocsInSUMIN: TcxGridDBColumn;
    ViewDocsInSUMR: TcxGridDBColumn;
    ViewDocsInSUMNDS: TcxGridDBColumn;
    ViewDocsInSUMNAC: TcxGridDBColumn;
    ViewDocsInIACTIVE: TcxGridDBColumn;
    ViewDocsInIDCLI_TO: TcxGridDBColumn;
    ViewDocsInID: TcxGridDBColumn;
    ViewDocsInORDERN: TcxGridDBColumn;
    ViewCardsIn: TcxGridDBTableView;
    ViewCardsInIDSKL: TcxGridDBColumn;
    ViewCardsInNAMEDEP: TcxGridDBColumn;
    ViewCardsInDATEDOC: TcxGridDBColumn;
    ViewCardsInITYPED: TcxGridDBColumn;
    ViewCardsInNUM: TcxGridDBColumn;
    ViewCardsInNUMSF: TcxGridDBColumn;
    ViewCardsInIDCARD: TcxGridDBColumn;
    ViewCardsInNAMECARD: TcxGridDBColumn;
    ViewCardsInFULLNAME: TcxGridDBColumn;
    ViewCardsInCardType: TcxGridDBColumn;
    ViewCardsInIDM: TcxGridDBColumn;
    ViewCardsInSBAR: TcxGridDBColumn;
    ViewCardsInNDSPROC: TcxGridDBColumn;
    ViewCardsInSUMNDS: TcxGridDBColumn;
    ViewCardsInQUANT: TcxGridDBColumn;
    ViewCardsInPRICEIN: TcxGridDBColumn;
    ViewCardsInPRICEIN0: TcxGridDBColumn;
    ViewCardsInPRICER: TcxGridDBColumn;
    ViewCardsInSUMIN: TcxGridDBColumn;
    ViewCardsInSUMR: TcxGridDBColumn;
    ViewCardsInIDCLI: TcxGridDBColumn;
    ViewCardsInIACTIVE: TcxGridDBColumn;
    ViewCardsInDATESF: TcxGridDBColumn;
    ViewCardsInID: TcxGridDBColumn;
    ViewCardsInNAMEPOST: TcxGridDBColumn;
    ViewCardsInINNPOST: TcxGridDBColumn;
    ViewCardsInSSNameGr: TcxGridDBColumn;
    ViewCardsInSNameGr: TcxGridDBColumn;
    ViewCardsInNameGr: TcxGridDBColumn;
    LevelDocsIn: TcxGridLevel;
    LevelCards: TcxGridLevel;
    dxBarSeparator1: TdxBarSeparator;
    bSCHF: TdxBarLargeButton;
    bNacl: TdxBarLargeButton;
    bExpExcel: TdxBarLargeButton;
    dxBarSeparator2: TdxBarSeparator;
    seCopy: TdxBarSpinEdit;
    quDocInSpTaraCountry: TIntegerField;
    quDocInSpTaraCountryName: TStringField;
    quDocInSpCountry: TIntegerField;
    quDocInSpCountryName: TStringField;
    UpDocIn: TFDUpdateSQL;
    quDocInSUMNDS1: TFloatField;
    quDocInSUMNDS2: TFloatField;
    ViewDocsInSUMIN_T: TcxGridDBColumn;
    quDocInSUMIN_T: TFloatField;
    deBegin: TcxBarEditItem;
    deEnd: TcxBarEditItem;
    quDocInNAMEPER: TStringField;
    ViewDocsInNAMEPER: TcxGridDBColumn;
    acAddPack: TAction;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarSubItem2: TdxBarSubItem;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acCloseExecute(Sender: TObject);
    procedure acSelectDateExecute(Sender: TObject);
    procedure acViewFormExecute(Sender: TObject);
    procedure acExpExcelExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure acEditExecute(Sender: TObject);
    procedure acViewExecute(Sender: TObject);
    procedure acDeleteExecute(Sender: TObject);
    procedure acAddDocExecute(Sender: TObject);
    procedure ViewCardsInCellDblClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure ViewDocsInCellDblClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure acOnDocExecute(Sender: TObject);
    procedure acOffDocExecute(Sender: TObject);
    procedure acActInExecute(Sender: TObject);
    procedure acUpdate(Sender: TObject);
    procedure acInfListExecute(Sender: TObject);
    procedure acRegPriceExecute(Sender: TObject);
    procedure acActRevPriceExecute(Sender: TObject);
    procedure acPrintSCFKExecute(Sender: TObject);
    procedure acPrintNACLExecute(Sender: TObject);
    procedure acExitUsersExecute(Sender: TObject);
    procedure ViewDocsInCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure acAddPackExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Init;
    procedure ViewForm;
  end;

var
  fmDocsIn: TfmDocsIn;
  procedure ShowFormDocsIn;

implementation

{$R *.dfm}

uses dmStyleCtrl, UserId;

//Процедура открытия формы
procedure ShowFormDocsIn;
begin
  if Assigned(fmDocsIn)=False then //Форма не существует, её нужно создать
    fmDocsIn:=TfmDocsIn.Create(Application);
  fmDocsIn.deBegin.EditValue:=Date-3;
  fmDocsIn.deEnd.EditValue:=Date;
//  fmDocsIn.dxRibbon1.Style:=fmMain.dxRibbon.Style;
//  fmDocsIn.dxRibbon1.ColorSchemeName:=fmMain.dxRibbon.ColorSchemeName;
  fmDocsIn.Show;
  fmDocsIn.Init;
  fmDocsIn.ViewForm;
end;

procedure TfmDocsIn.Init;
//var iCounterPerSec: TLargeInteger;
//    C1, C2: TLargeInteger;
begin
  //QueryPerformanceFrequency(iCounterPerSec);
  //QueryPerformanceCounter(C1);
  if LevelDocsIn.Visible then begin
    quDocIn.Active:=false;
    quDocIn.ParamByName('DATEB').Value:=trunc(deBegin.EditValue);
    quDocIn.ParamByName('DATEE').Value:=trunc(deEnd.EditValue);
    quDocIn.ParamByName('ISHOP').Value:=fShop;
    quDocIn.Active:=true;
  end else begin
    quDocInCards.Active:=false;
    quDocInCards.ParamByName('DATEB').Value:=trunc(deBegin.EditValue);
    quDocInCards.ParamByName('DATEE').Value:=trunc(deEnd.EditValue);
    quDocInCards.ParamByName('ISHOP').Value:=fShop;
    quDocInCards.Active:=true;
  end;
  //QueryPerformanceCounter(C2);
  //fmMain.sbPanel.Panels[1].Text:=(FormatFloat('0.0000', (C2 - C1) / iCounterPerSec) + ' сек.');
  //SetUserRights; - во ViewForm запускается
end;

procedure TfmDocsIn.ViewCardsInCellDblClick(Sender: TcxCustomGridTableView; ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
begin
  if quDocIn.RecordCount>0 then ShowFormAddDocIn(sView, quDocInOPRIZN.AsInteger);
end;

procedure TfmDocsIn.ViewDocsInCellDblClick(Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  if quDocIn.RecordCount>0 then
    if quDocInIACTIVE.AsInteger <> 1 then ShowFormAddDocIn(sEdit, quDocInOPRIZN.AsInteger)
    else ShowFormAddDocIn(sView, quDocInOPRIZN.AsInteger);
end;

procedure TfmDocsIn.ViewDocsInCustomDrawCell(Sender: TcxCustomGridTableView; ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var t:string;
begin
  t:=AViewInfo.GridRecord.DisplayTexts[ViewDocsInIACTIVE.Index];

  if Trim(t)='Редактируется' then begin
    ACanvas.Canvas.Brush.Color := $00BCF5FC;
    ACanvas.Canvas.Font.Color := clBlack;
  end;
end;

procedure TfmDocsIn.ViewForm;
begin
  if LevelCards.Visible then begin
    Caption:='Приход по товарам за период с '+FormatDateTiMe('dd.mm.yyyy',deBegin.EditValue)+' по '+FormatDateTiMe('dd.mm.yyyy',deEnd.EditValue);
    acView.Enabled:=False;

    acAddDoc.Enabled:=False;
    acEdit.Enabled:=False;
    acDelete.Enabled:=False;
    acOnDoc.Enabled:=False;
    acOffDoc.Enabled:=False;
  end else begin
    Caption:='Документы на приход за период с '+FormatDateTiMe('dd.mm.yyyy',deBegin.EditValue)+' по '+FormatDateTiMe('dd.mm.yyyy',deEnd.EditValue);
    acView.Enabled:=True;
    SetUserRights(Self);
  end;
  //Init;
  //acUpdate(LevelCards);   //не нужен    /Иванченко//
end;

procedure TfmDocsIn.acViewFormExecute(Sender: TObject);
begin //вид
  if LevelCards.Visible then begin
    LevelDocsIn.Visible:=True;
    LevelCards.Visible:=False;
    bViewForm.Down:=False;
  end else begin
    LevelDocsIn.Visible:=False;
    LevelCards.Visible:=True;
    bViewForm.Down:=True;
  end;
  acSelectDateExecute(Sender);
end;

procedure TfmDocsIn.acUpdate(Sender: TObject);
begin
  {if quDocInIACTIVE.AsBoolean then begin
    acEdit.Enabled := False;
    acDelete.Enabled := False;
    acOnDoc.Enabled := False;
    acOffDoc.Enabled := True;
  end else begin
    acEdit.Enabled := True;
    acDelete.Enabled := True;
    acOnDoc.Enabled := True;
    acOffDoc.Enabled := False;
  end;}
end;

procedure TfmDocsIn.acActInExecute(Sender: TObject);
var FIO: string;
    Stream: TMemoryStream;
begin
  dmM.quS.Active:=false;
  dmM.quS.SQL.Clear;
  dmM.quS.SQL.Add('SELECT Surname+'' ''+Name+'' ''+MiddleName AS FIO FROM EcrystalR.dbo.Personal WHERE Id_Personal = (SELECT TOP 1 PERSON FROM ELOGMC.dbo.History_HD');
  dmM.quS.SQL.Add('WHERE IDH = '+ its(quDocInID.AsInteger) +' AND IDOCTYPE = 1 ORDER BY DATEOPER DESC)');
  dmM.quS.Active:=True;

  if dmM.quS.RecordCount>0 then
  begin
    FIO:=dmM.quS.fieldByName('FIO').AsString;
    ShowMessage(FIO);
    dmM.quS.Active:=false;
  end;

  if quDocIn.RecordCount>0 then
    if quDocInIACTIVE.AsInteger=1 then
    begin
//      reportfile:=CommonSet.PathReports + 'ActPriemki.fr3';
//      if FileExists(reportfile)=False then begin
//        ShowMessage('Файл отчета не найден '+reportfile);
//        Exit;
//      end;
//      frxRepDIN.LoadFromFile(reportfile);

      dmM.quBlobRep.Active := False;
      dmM.quBlobRep.ParamByName('ID').Value:=1;
      dmM.quBlobRep.Active := True;
      Stream := TMemoryStream.Create;
      try
        dmM.quBlobRepBLOB.SaveToStream(Stream);
        Stream.Position:=0;
        frxRepDIN.LoadFromStream(Stream);
      finally
        Stream.Free;
      end;

      frxRepDIN.Variables['xActNum']:=''''+quDocInIDDOC.AsString+'''';
      frxRepDIN.Variables['xActDate']:=''''+FormatDateTime('dd.mm.yyyy',Date())+'''';
      frxRepDIN.Variables['xPoluchNameOpr']:=''''+FIO+'''';

      frxRepDIN.Variables['xZakazNum']:=''''+quDocInORDERN.AsString+'''';      //номер заявки
      if strtointdef(quDocInORDERN.AsString,0)>0 then
        frxRepDIN.Variables['xZakazDate']:=''''+FormatDateTime('dd.mm.yyyy',quDocInDATEDOC.AsDateTime)+'''';  //дата заявки

      frxRepDIN.Variables['xDocNum']:=''''+quDocInNUMDOC.AsString+'''';        //номер прихода
      frxRepDIN.Variables['xDocDate']:=''''+FormatDateTime('dd.mm.yyyy',quDocInDATEDOC.AsDateTime)+''''; //дата прихода

      dmD.quDepPars.Active:=False;
      dmD.quDepPars.ParamByName('ID').AsInteger:=quDocInIDSKL.AsInteger;
      dmD.quDepPars.Active:=True;

      if dmD.quDepPars.RecordCount>0 then
      begin
        frxRepDIN.Variables['xPoluch']:=''''+dmD.quDepParsFullName.AsString+'''';
        frxRepDIN.Variables['xPoluchINN']:=''''+dmD.quDepParsINN.AsString+'''';
        frxRepDIN.Variables['xPoluchKPP']:=''''+dmD.quDepParsKPP.AsString+'''';
        frxRepDIN.Variables['xPoluchAdr']:=''''+dmD.quDepParsAddres.AsString+'''';
      end else
        frxRepDIN.Variables['xCli1Name']:=''''+quDocInNAMESKL.AsString+'''';

      dmD.quFindCli.Active:=False;
      dmD.quFindCli.ParamByName('ID').AsInteger:=quDocInIDCLI.AsInteger;
      dmD.quFindCli.Active:=True;

      if dmD.quFindCli.RecordCount>0 then
      begin
        frxRepDIN.Variables['xOtpr']:=''''+dmD.quFindCliFullName.AsString+'''';
        frxRepDIN.Variables['xOtprAdr']:=''''+dmD.quFindCliADROTPR.AsString+'''';
        frxRepDIN.Variables['xOtprINN']:=''''+dmD.quFindCliINN.AsString+'''';
        frxRepDIN.Variables['xOtprKPP']:=''''+dmD.quFindCliKPP.AsString+'''';
      end;

      quDocInSp.Active := False;
      quDocInSp.ParamByName('ID').Value:=quDocInID.AsInteger;
      quDocInSp.ParamByName('TYPE').Value:=0;
      quDocInSp.Active := True;

      quDocInSpTara.Active := False;
      quDocInSpTara.ParamByName('ID').Value:=quDocInID.AsInteger;
      quDocInSpTara.Active := True;

      frxRepDIN.PrepareReport;
      frxRepDIN.ShowPreparedReport;
    end else ShowMessage('Неверный статус документа.');
end;

procedure TfmDocsIn.acInfListExecute(Sender: TObject);
var Stream: TMemoryStream;
begin
  if quDocIn.RecordCount>0 then
  begin
    dmM.quBlobRep.Active := False;
    dmM.quBlobRep.ParamByName('ID').Value:=2;
    dmM.quBlobRep.Active := True;
    Stream := TMemoryStream.Create;
    try
      dmM.quBlobRepBLOB.SaveToStream(Stream);
      Stream.Position:=0;
      frxRepDIN.LoadFromStream(Stream);
    finally
      Stream.Free;
    end;

    frxRepDIN.Variables['DocNum']:=''''+quDocInNUMDOC.AsString+'''';        //номер прихода
    frxRepDIN.Variables['DocDate']:=''''+FormatDateTime('dd.mm.yyyy',quDocInDATEDOC.AsDateTime)+''''; //дата прихода
    frxRepDIN.Variables['CliFrom']:=''''+quDocInNAMEPOST.AsString+'''';
    frxRepDIN.Variables['CliTo']:=''''+quDocInNAMESKL.AsString+'''';

    quDocInSp.Active := False;
    quDocInSp.ParamByName('ID').Value:=quDocInID.AsInteger;
    quDocInSp.ParamByName('TYPE').Value:=0;
    quDocInSp.Active := True;

    frxRepDIN.PrepareReport;
    if chbView.EditValue then frxRepDIN.ShowPreparedReport
    else begin
      frxRepDIN.PrintOptions.Copies:=1;
      frxRepDIN.PrintOptions.Collate:=False;
      frxRepDIN.PrintOptions.PrintPages:=ppAll;
      frxRepDIN.Print;
    end;
  end;
end;

procedure TfmDocsIn.acRegPriceExecute(Sender: TObject);
var Stream: TMemoryStream;
begin
  if quDocIn.RecordCount>0 then
  begin
    dmD.quDepPars.Active:=False;
    dmD.quDepPars.ParamByName('ID').AsInteger:=quDocInIDSKL.AsInteger;
    dmD.quDepPars.Active:=True;

    dmM.quBlobRep.Active := False;
    if dmD.quDepParsPayNDS.AsBoolean then dmM.quBlobRep.ParamByName('ID').Value:=3
    else dmM.quBlobRep.ParamByName('ID').Value:=4;
    dmM.quBlobRep.Active := True;
    Stream := TMemoryStream.Create;
    try
      dmM.quBlobRepBLOB.SaveToStream(Stream);
      Stream.Position:=0;
      frxRepDIN.LoadFromStream(Stream);
    finally
      Stream.Free;
    end;

    frxRepDIN.Variables['DocNum']:=''''+quDocInNUMDOC.AsString+'''';        //номер прихода
    frxRepDIN.Variables['DocDate']:=''''+FormatDateTime('dd.mm.yyyy',quDocInDATEDOC.AsDateTime)+''''; //дата прихода
    frxRepDIN.Variables['CliFrom']:=''''+quDocInNAMEPOST.AsString+'''';
    frxRepDIN.Variables['CliTo']:=''''+quDocInNAMESKL.AsString+'''';

    frxRepDIN.Variables['SSumNac']:=''''+MoneyToString(quDocInSUMR.AsFloat-quDocInSUMIN.AsFloat,True,False)+'''';
    frxRepDIN.Variables['SSumNDS10']:=''''+MoneyToString(quDocInSUMNDS1.AsFloat,True,False)+'''';
    frxRepDIN.Variables['SSumNDS20']:=''''+MoneyToString(quDocInSUMNDS2.AsFloat,True,False)+'''';
    frxRepDIN.Variables['SSumNDS']:=''''+MoneyToString(quDocInSUMNDS.AsFloat,True,False)+'''';
    frxRepDIN.Variables['SSumIn']:=''''+MoneyToString(quDocInSUMIN.AsFloat,True,False)+'''';
    frxRepDIN.Variables['SSumOP']:=''''+MoneyToString(0*quDocInSUMIN.AsFloat,True,False)+'''';
    frxRepDIN.Variables['SSumOM']:=''''+MoneyToString(0*quDocInSUMR.AsFloat,True,False)+'''';

    quDocInSp.Active := False;
    quDocInSp.ParamByName('ID').Value:=quDocInID.AsInteger;
    quDocInSp.ParamByName('TYPE').Value:=1;
    quDocInSp.Active := True;

    frxRepDIN.PrepareReport;
    if chbView.EditValue then frxRepDIN.ShowPreparedReport
    else begin
      frxRepDIN.PrintOptions.Copies:=1;
      frxRepDIN.PrintOptions.Collate:=False;
      frxRepDIN.PrintOptions.PrintPages:=ppAll;
      frxRepDIN.Print;
    end;
  end;
end;

procedure TfmDocsIn.acActRevPriceExecute(Sender: TObject);
var Stream: TMemoryStream;
begin
  if quDocIn.RecordCount>0 then
  begin
    dmM.quBlobRep.Active := False;
    dmM.quBlobRep.ParamByName('ID').Value:=5;
    dmM.quBlobRep.Active := True;
    Stream := TMemoryStream.Create;
    try
      dmM.quBlobRepBLOB.SaveToStream(Stream);
      Stream.Position:=0;
      frxRepDIN.LoadFromStream(Stream);
    finally
      Stream.Free;
    end;

    frxRepDIN.Variables['DocNum']:=''''+quDocInNUMDOC.AsString+'''';        //номер прихода
    frxRepDIN.Variables['DocDate']:=''''+FormatDateTime('dd.mm.yyyy',quDocInDATEDOC.AsDateTime)+''''; //дата прихода
    frxRepDIN.Variables['CliFrom']:=''''+quDocInNAMEPOST.AsString+'''';
    frxRepDIN.Variables['CliTo']:=''''+quDocInNAMESKL.AsString+'''';

    quDocInSp.Active := False;
    quDocInSp.ParamByName('ID').Value:=quDocInID.AsInteger;
    quDocInSp.ParamByName('TYPE').Value:=1;
    quDocInSp.Active := True;

    frxRepDIN.PrepareReport;
    if chbView.EditValue then frxRepDIN.ShowPreparedReport
    else begin
      frxRepDIN.PrintOptions.Copies:=1;
      frxRepDIN.PrintOptions.Collate:=False;
      frxRepDIN.PrintOptions.PrintPages:=ppAll;
      frxRepDIN.Print;
    end;
  end;
end;

procedure TfmDocsIn.acPrintNACLExecute(Sender: TObject);
var sStr: string;
    Stream: TMemoryStream;
begin
  if quDocIn.RecordCount>0 then
    if quDocInIACTIVE.AsInteger=1 then
    begin
      dmM.quBlobRep.Active := False;
      dmM.quBlobRep.ParamByName('ID').Value:=7;
      dmM.quBlobRep.Active := True;
      Stream := TMemoryStream.Create;
      try
        dmM.quBlobRepBLOB.SaveToStream(Stream);
        Stream.Position:=0;
        frxRepDIN.LoadFromStream(Stream);
      finally
        Stream.Free;
      end;

      frxRepDIN.Variables['DocNum']:=''''+FormatDateTime('dd',quDocInDATEDOC.AsDateTime)+quDocInNUMDOC.AsString+'''';        //номер прихода
      frxRepDIN.Variables['DocDate']:=''''+FormatDateTime('dd.mm.yyyy',quDocInDATEDOC.AsDateTime)+''''; //дата прихода
      frxRepDIN.Variables['SSum']:=''''+MoneyToString(quDocInSUMIN.AsFloat,True,False)+'''';
      frxRepDIN.Variables['TypeP']:=1;

      dmD.quDepPars.Active:=False;
      dmD.quDepPars.ParamByName('ID').AsInteger:=quDocInIDSKL.AsInteger;
      dmD.quDepPars.Active:=True;

      if dmD.quDepPars.RecordCount>0 then
      begin
        frxRepDIN.Variables['Cli2Name']:=''''+dmD.quDepParsNAMEOTP.AsString+'''';
        frxRepDIN.Variables['Cli2Adr']:=''''+dmD.quDepParsADROTPR.AsString+'''';
        frxRepDIN.Variables['Cli2Inn']:=''''+dmD.quDepParsINN.AsString+' '+dmD.quDepParsKPP.AsString+'''';
        frxRepDIN.Variables['Poluch']:=''''+dmD.quDepParsFullName.AsString+'''';
        frxRepDIN.Variables['PoluchAdr']:=''''+dmD.quDepParsAddres.AsString+'''';
        frxRepDIN.Variables['Cli2RSch']:=''''+dmD.quDepParsRSch.AsString+'''';
        frxRepDIN.Variables['Cli2Bank']:=''''+dmD.quDepParsBank.AsString+'''';
        frxRepDIN.Variables['Cli2KSch']:=''''+dmD.quDepParsKSch.AsString+'''';
        frxRepDIN.Variables['Cli2Bik']:=''''+dmD.quDepParsBik.AsString+'''';
      end;

      dmD.quFindCli.Active:=False;
      dmD.quFindCli.ParamByName('ID').AsInteger:=quDocInIDCLI.AsInteger;
      dmD.quFindCli.Active:=True;

      if dmD.quFindCli.RecordCount>0 then
      begin
        frxRepDIN.Variables['Cli1Name']:=''''+dmD.quFindCliFullName.AsString+'''';
        frxRepDIN.Variables['Cli1Adr']:=''''+dmD.quFindCliADROTPR.AsString+'''';
        frxRepDIN.Variables['Cli1Inn']:=''''+dmD.quFindCliINN.AsString+' '+dmD.quFindCliKPP.AsString+'''';
        frxRepDIN.Variables['Cli1RSch']:=''''+dmD.quFindCliRSch.AsString+'''';
        frxRepDIN.Variables['Cli1Bank']:=''''+dmD.quFindCliBank.AsString+'''';
        frxRepDIN.Variables['Cli1KSch']:=''''+dmD.quFindCliKSch.AsString+'''';
        frxRepDIN.Variables['Cli1Bik']:=''''+dmD.quFindCliBik.AsString+'''';

        if ((Trim(dmD.quFindCliNAMEOTP.AsString)='') OR (dmD.quFindCliNAMEOTP.AsString='.')) then
        begin
          frxRepDIN.Variables['Otpr']:=frxRepDIN.Variables['Cli1Name'];
          frxRepDIN.Variables['OtprAdr']:=frxRepDIN.Variables['Cli1Adr'];
        end else begin
          frxRepDIN.Variables['Otpr']:=''''+dmD.quFindCliNAMEOTP.AsString+'''';
          frxRepDIN.Variables['OtprAdr']:=''''+dmD.quFindCliADROTPR.AsString+'''';
        end;
      end;

      //frxRepDIN.Variables['GlBuh']:=''''+cxTextEdit2.Text+'''';

      quDocInSp.Active := False;
      quDocInSp.ParamByName('ID').Value:=quDocInID.AsInteger;
      quDocInSp.ParamByName('TYPE').Value:=1;
      quDocInSp.Active := True;

      sStr:=MoneyToString(quDocInSp.RecordCount,True,False);
      if pos('руб',sStr)>0 then SStr:=Copy(sStr,1,pos('руб',sStr)-1);
      frxRepDIN.Variables['SQStr']:=''''+' ('+sStr+')'+'''';
      frxRepDIN.Variables['Osnovanie']:=''''+'Основной договор'+'''';

      frxRepDIN.PrepareReport;
      if chbView.EditValue then frxRepDIN.ShowPreparedReport
      else begin
        frxRepDIN.PrintOptions.Copies:=Trunc(seCopy.Value);
        frxRepDIN.PrintOptions.Collate:=False;
        frxRepDIN.PrintOptions.PrintPages:=ppAll;
        frxRepDIN.Print;
      end;
    end else ShowMessage('Неверный статус документа.');
end;

procedure TfmDocsIn.acPrintSCFKExecute(Sender: TObject);
var Stream: TMemoryStream;
begin
  if quDocIn.RecordCount>0 then
    if quDocInIACTIVE.AsInteger=1 then
    begin
      dmM.quBlobRep.Active := False;
      dmM.quBlobRep.ParamByName('ID').Value:=6;
      dmM.quBlobRep.Active := True;
      Stream := TMemoryStream.Create;
      try
        dmM.quBlobRepBLOB.SaveToStream(Stream);
        Stream.Position:=0;
        frxRepDIN.LoadFromStream(Stream);
      finally
        Stream.Free;
      end;

      frxRepDIN.Variables['DocNum']:=''''+FormatDateTime('dd',quDocInDATEDOC.AsDateTime)+quDocInNUMDOC.AsString+'''';        //номер прихода
      frxRepDIN.Variables['DocDate']:=''''+FormatDateTime('dd.mm.yyyy',quDocInDATEDOC.AsDateTime)+''''; //дата прихода

      dmD.quDepPars.Active:=False;
      dmD.quDepPars.ParamByName('ID').AsInteger:=quDocInIDSKL.AsInteger;
      dmD.quDepPars.Active:=True;

      if dmD.quDepPars.RecordCount>0 then
      begin
        frxRepDIN.Variables['Cli2Name']:=''''+dmD.quDepParsFullName.AsString+'''';
        frxRepDIN.Variables['Cli2Inn']:=''''+dmD.quDepParsINN.AsString+' '+dmD.quDepParsKPP.AsString+'''';
        frxRepDIN.Variables['Cli2Adr']:=''''+dmD.quDepParsAddres.AsString+'''';
        frxRepDIN.Variables['Poluch']:=''''+dmD.quDepParsNAMEOTP.AsString+'''';
        frxRepDIN.Variables['PoluchAdr']:=''''+dmD.quDepParsADROTPR.AsString+'''';
      end;

      dmD.quFindCli.Active:=False;
      dmD.quFindCli.ParamByName('ID').AsInteger:=quDocInIDCLI.AsInteger;
      dmD.quFindCli.Active:=True;

      if dmD.quFindCli.RecordCount>0 then
      begin
        frxRepDIN.Variables['Cli1Name']:=''''+dmD.quFindCliFullName.AsString+'''';
        frxRepDIN.Variables['Cli1Adr']:=''''+dmD.quFindCliADROTPR.AsString+'''';
        frxRepDIN.Variables['Cli1Inn']:=''''+dmD.quFindCliINN.AsString+' '+dmD.quFindCliKPP.AsString+'''';

        if ((Trim(dmD.quFindCliNAMEOTP.AsString)='') OR (dmD.quFindCliNAMEOTP.AsString='.')) then
        begin
          frxRepDIN.Variables['Otpr']:=frxRepDIN.Variables['Cli1Name'];
          frxRepDIN.Variables['OtprAdr']:=frxRepDIN.Variables['Cli1Adr'];
        end else begin
          frxRepDIN.Variables['Otpr']:=''''+dmD.quFindCliNAMEOTP.AsString+'''';
          frxRepDIN.Variables['OtprAdr']:=''''+dmD.quFindCliADROTPR.AsString+'''';
        end;
      end;

    //  frxRepDIN.Variables['Rukovod']:=''''+cxTextEdit1.Text+'''';
    //  frxRepDIN.Variables['GlBuh']:=''''+cxTextEdit2.Text+'''';

      quDocInSp.Active := False;
      quDocInSp.ParamByName('ID').Value:=quDocInID.AsInteger;
      quDocInSp.ParamByName('TYPE').Value:=1;
      quDocInSp.Active := True;

      frxRepDIN.PrepareReport;
      if chbView.EditValue then frxRepDIN.ShowPreparedReport
      else begin
        frxRepDIN.PrintOptions.Copies:=Trunc(seCopy.Value);
        frxRepDIN.PrintOptions.Collate:=False;
        frxRepDIN.PrintOptions.PrintPages:=ppAll;
        frxRepDIN.Print;
      end;
    end else ShowMessage('Неверный статус документа.');
end;

procedure TfmDocsIn.acAddDocExecute(Sender: TObject);
begin
  ShowFormAddDocIn(sAdd, 1);
end;

procedure TfmDocsIn.acAddPackExecute(Sender: TObject);
begin
  ShowFormAddDocIn(sAdd, 99);
end;

procedure TfmDocsIn.acCloseExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmDocsIn.acDeleteExecute(Sender: TObject);
var iErrors:Integer;
    aIShop, aITypeDoc, aIdHead: Integer;
    LockDocReturn: TLockDocReturn;
begin
  if quDocIn.RecordCount>0 then
    if quDocInIACTIVE.AsInteger <> 1 then
    begin
      if MessageDlg('Удалить выбранный документ ('+quDocInNUMDOC.AsString+' от '+quDocInNAMEPOST.AsString+') ?',mtConfirmation, [mbYes, mbNo], 0, mbYes) = mrYes then
      begin
        //<--Проверяем и захватываем документ на редактирование
        aIShop := quDocInISHOP.AsInteger;  //Выбранный магазин
        aITypeDoc := 1;                                //Тип документа 1-Приходы
        aIdHead := quDocInID.AsInteger;       //Выбранный документ
        if LockDoc(aIShop, aITypeDoc, aIdHead, LockDocReturn)=False then begin    //Блокируем документ
          ShowMsg('Данный документ удалить нельзя, т.к. он редактируется пользователем '+LockDocReturn.NamePersonal+'!');
          Exit;   //Документ занят, выходим
        end; //-->

        quDocIn.Edit;
        quDocInIACTIVE.AsInteger:=-1;
        quDocIn.Post;

        UnLockDoc(aIShop, aITypeDoc, aIdHead);    //Разблокируем документ
        quDocIn.Refresh;
      end;
    end else
      ShowMessage('Удаление, оприходованного, документа ЗАПРЕЩЕНО!');
end;

procedure TfmDocsIn.acEditExecute(Sender: TObject);
begin
  if quDocIn.RecordCount>0 then
    if quDocInIACTIVE.AsInteger <> 1 then
      ShowFormAddDocIn(sEdit, quDocInOPRIZN.AsInteger)
    else
      ShowMessage('Редактирование, оприходованного, документа ЗАПРЕЩЕНО!');
end;

procedure TfmDocsIn.acExitUsersExecute(Sender: TObject);
begin
  SetLockDoc(quDocInISHOP.AsInteger, 1, quDocInID.AsInteger, 1);
end;

procedure TfmDocsIn.acExpExcelExecute(Sender: TObject);
begin
  ExportGridToFile(Caption+'.xls', GridDocsIn);
end;

procedure TfmDocsIn.acOffDocExecute(Sender: TObject);
var iUser: Integer;
begin
  //Откатить
  if quDocIn.RecordCount>0 then
    if (quDocInIACTIVE.AsInteger = 1) then
      if MessageDlg('Откатить выбранный документ ( '+quDocInNUMDOC.AsString+' от '+quDocInNAMEPOST.AsString+' ) ?',mtConfirmation, [mbYes, mbNo], 0, mbYes) = mrYes then
      begin
        try
          //<-- запрашиваем конкретного пользователя
          if GetIdUser('Для выполнения отката необходимо подписать документ.', iUser) then
            if prSetStatusDoc(1,0,quDocInISHOP.AsInteger,quDocInID.AsInteger,iUser) then
              ShowMessage('Откат выполнен.');
          //-->
          quDocIn.Refresh;
        except on ER:Exception do
          ShowMsg(StringReplace(ER.Message, FireDAC_ERR, '',[rfReplaceAll, rfIgnoreCase]),'Внимание!!!',MB_OK + MB_ICONERROR)
        end;
      end;
end;

procedure TfmDocsIn.acOnDocExecute(Sender: TObject);
var aIShop, aITypeDoc, aIdHead: Integer;
    LockDocReturn: TLockDocReturn;
    iUser: Integer;
begin
  //Оприходовать
  if quDocIn.RecordCount>0 then
    if quDocInIACTIVE.AsInteger = 0 then
      if MessageDlg('Оприходовать выбранный документ ( '+quDocInNUMDOC.AsString+' от '+quDocInNAMEPOST.AsString+' ) ?',mtConfirmation, [mbYes, mbNo], 0, mbYes) = mrYes then
      begin
        try
          //<--Проверяем и захватываем документ на редактирование
          aIShop := quDocInISHOP.AsInteger;  //Выбранный магазин
          aITypeDoc := 1;                                //Тип документа 1-Приходы
          aIdHead := quDocInID.AsInteger;       //Выбранный документ
          if LockDoc(aIShop, aITypeDoc, aIdHead, LockDocReturn)=False then begin    //Блокируем документ
            ShowMsg('Данный документ оприходовать нельзя, т.к. он редактируется пользователем '+LockDocReturn.NamePersonal+'!');
            Exit;   //Документ занят, выходим
          end;
          //-->
          //<-- запрашиваем конкретного пользователя
          if GetIdUser('Для выполнения оприходования необходимо подписать документ.',iUser) then
            if prSetStatusDoc(aITypeDoc,1,quDocInISHOP.AsInteger,quDocInID.AsInteger,iUser) then
              ShowMessage('Оприходование выполнено.');
          //-->

          UnLockDoc(aIShop, aITypeDoc, aIdHead);    //Разблокируем документ
          quDocIn.Refresh;
        except on ER:Exception do
          ShowMsg(StringReplace(ER.Message, FireDAC_ERR, '',[rfReplaceAll, rfIgnoreCase]),'Внимание!!!',MB_OK + MB_ICONERROR)
        end;
      end;
end;

procedure TfmDocsIn.acSelectDateExecute(Sender: TObject);
begin
  Init;
  ViewForm;
end;

procedure TfmDocsIn.acViewExecute(Sender: TObject);
begin
  if quDocIn.RecordCount>0 then  ShowFormAddDocIn(sView, quDocInOPRIZN.AsInteger);
end;

procedure TfmDocsIn.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
  ViewDocsIn.StoreToIniFile(GetGridIniFile,False,[gsoUseSummary,gsoUseFilter]);
  ViewCardsIn.StoreToIniFile(GetGridIniFile,False,[gsoUseSummary,gsoUseFilter]);
  if Assigned(fmAddDocIn)=True then fmAddDocIn.Close;
  if not(fsModal in FormState) then fmDocsIn:=Nil;
end;

procedure TfmDocsIn.FormCreate(Sender: TObject);
begin
  ViewDocsIn.RestoreFromIniFile(GetGridIniFile);
  ViewCardsIn.RestoreFromIniFile(GetGridIniFile);
end;

end.
