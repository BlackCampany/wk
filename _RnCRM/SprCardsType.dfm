object fmSprCaTypes: TfmSprCaTypes
  Left = 0
  Top = 0
  Hint = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082' '#1074#1077#1089#1086#1074
  Caption = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082' '#1090#1080#1087#1086#1074' '#1090#1086#1074#1072#1088#1086#1074
  ClientHeight = 445
  ClientWidth = 537
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 537
    Height = 122
    ApplicationButton.Visible = False
    BarManager = dxBarManager1
    Style = rs2016
    ColorSchemeAccent = rcsaBlue
    ColorSchemeName = 'Colorful'
    ShowMinimizeButton = False
    SupportNonClientDrawing = True
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object rtScales: TdxRibbonTab
      Active = True
      Caption = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082' '#1090#1080#1087#1086#1074' '#1090#1086#1074#1072#1088#1086#1074
      Groups = <
        item
          ToolbarName = 'bmAction'
        end
        item
          ToolbarName = 'bmClose'
        end>
      Index = 0
    end
  end
  object sbSprScale: TdxRibbonStatusBar
    Left = 0
    Top = 422
    Width = 537
    Height = 23
    Panels = <>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object GrCaT: TcxGrid
    Left = 0
    Top = 122
    Width = 537
    Height = 300
    Align = alClient
    TabOrder = 6
    ExplicitTop = 97
    ExplicitHeight = 325
    object ViewCaT: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.PriorPage.Visible = True
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.InfoPanel.Visible = True
      Navigator.Visible = True
      OnCustomDrawCell = ViewCaTCustomDrawCell
      DataController.DataSource = dsquCardsTypes
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
        end
        item
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      object ViewCaTID: TcxGridDBColumn
        Caption = #1050#1086#1076
        DataBinding.FieldName = 'ID'
      end
      object ViewCaTNAME: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'NAME'
        Width = 414
      end
    end
    object LevelCaT: TcxGridLevel
      GridView = ViewCaT
    end
  end
  object ActionList1: TActionList
    Left = 420
    Top = 140
    object acRefresh: TAction
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100' (F5)'
      Checked = True
      Hint = #1054#1073#1085#1086#1074#1080#1090#1100' (F5)'
      ImageIndex = 5
      ShortCut = 116
      OnExecute = acRefreshExecute
    end
    object acClose: TAction
      Caption = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 100
      OnExecute = acCloseExecute
    end
    object acAdd: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ImageIndex = 75
      OnExecute = acAddExecute
    end
    object acEdit: TAction
      Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100
      ImageIndex = 73
      OnExecute = acEditExecute
    end
    object acDel: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ImageIndex = 74
      OnExecute = acDelExecute
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      #1044#1077#1081#1089#1090#1074#1080#1103)
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmS.SmallImage
    ImageOptions.LargeImages = dmS.LargeImage
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 344
    Top = 172
    DockControlHeights = (
      0
      0
      0
      0)
    object bmAction: TdxBar
      Caption = #1044#1077#1081#1089#1090#1074#1080#1103
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 778
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'bUpdate'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'btnAdd'
        end
        item
          Visible = True
          ItemName = 'btnEdit'
        end
        item
          Visible = True
          ItemName = 'btnDel'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object bmClose: TdxBar
      Caption = #1047#1072#1082#1088#1099#1090#1100
      CaptionButtons = <>
      DockedLeft = 311
      DockedTop = 0
      FloatLeft = 778
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'bClose'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object bUpdate: TdxBarLargeButton
      Action = acRefresh
      Category = 0
      SyncImageIndex = False
      ImageIndex = 5
    end
    object bClose: TdxBarLargeButton
      Action = acClose
      Category = 0
    end
    object btnAdd: TdxBarLargeButton
      Action = acAdd
      Category = 0
    end
    object btnDel: TdxBarLargeButton
      Action = acDel
      Category = 0
    end
    object btnEdit: TdxBarLargeButton
      Action = acEdit
      Category = 0
    end
  end
  object JvFormStorage: TJvFormStorage
    AppStorage = fmMain.MainAppIniFileStorage
    AppStoragePath = 'SprScales\'
    Options = [fpSize, fpLocation]
    StoredValues = <>
    Left = 412
    Top = 236
  end
  object quCardsTypes: TFDQuery
    Connection = dmS.FDConnection
    Transaction = dmS.FDTrans
    UpdateTransaction = dmS.FDTransUpdate
    SQL.Strings = (
      'SELECT [ID],[NAME] FROM [dbo].[CARDSCATS]')
    Left = 48
    Top = 176
    object quCardsTypesID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quCardsTypesNAME: TStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      Size = 150
    end
  end
  object dsquCardsTypes: TDataSource
    DataSet = quCardsTypes
    Left = 48
    Top = 224
  end
end
