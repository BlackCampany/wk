unit ViewDocVn;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  CustomChildForm, System.UITypes, ShellApi, httpsend, synautil, nativexml, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxRibbonSkins, dxRibbonCustomizationForm, cxContainer, cxEdit, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator,
  Data.DB, cxDBData, cxImageComboBox, cxTextEdit, cxDBLookupComboBox, cxCheckBox, cxCalendar, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.Menus, System.Actions, Vcl.ActnList, dxBar, cxBarEditItem, dxBarExtItems, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid, cxMemo, Vcl.ExtCtrls, dxRibbon;

type
  TfmDocVn = class(TfmCustomChildForm)
    dxRibbon1: TdxRibbon;
    dxRibbon1Tab1: TdxRibbonTab;
    dxBarManager1: TdxBarManager;
    bmApplyDate: TdxBar;
    bmClose: TdxBar;
    btnDone: TdxBarLargeButton;
    btnClose: TdxBarLargeButton;
    deDateBeg: TdxBarDateCombo;
    deDateEnd: TdxBarDateCombo;
    dxBarGroup1: TdxBarGroup;
    ActionList1: TActionList;
    acRefresh: TAction;
    acClose: TAction;
    acSelectDate: TAction;
    GridDocVn: TcxGrid;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    beiPoint: TcxBarEditItem;
    acExpExcel: TAction;
    dxBarLargeButton9: TdxBarLargeButton;
    dxBarLargeButton10: TdxBarLargeButton;
    beiGetTTN: TcxBarEditItem;
    dxBarLargeButton14: TdxBarLargeButton;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    LevelDocVn: TcxGridLevel;
    ViewDocVn: TcxGridDBTableView;
    pmDocVn: TPopupMenu;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton11: TdxBarLargeButton;
    ViewDocVnFSRARID: TcxGridDBColumn;
    ViewDocVnIDATE: TcxGridDBColumn;
    ViewDocVnSID: TcxGridDBColumn;
    ViewDocVnNUMBER: TcxGridDBColumn;
    ViewDocVnSDATE: TcxGridDBColumn;
    ViewDocVnSTYPE: TcxGridDBColumn;
    ViewDocVnIACTIVE: TcxGridDBColumn;
    ViewDocVnREADYSEND: TcxGridDBColumn;
    ViewDocVnTICK1: TcxGridDBColumn;
    ViewDocVnTICK2: TcxGridDBColumn;
    ViewDocVnSIDIN: TcxGridDBColumn;
    ViewDocVnSENDXML: TcxGridDBColumn;
    acSendDocVnToRar: TAction;
    N1: TMenuItem;
    dxBarLargeButton12: TdxBarLargeButton;
    N2: TMenuItem;
    Excel1: TMenuItem;
    acDelDocVn: TAction;
    N3: TMenuItem;
    procedure acRefreshExecute(Sender: TObject);
    procedure acCloseExecute(Sender: TObject);
    procedure acSelectDateExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure deDateBegChange(Sender: TObject);
    procedure acExpExcelExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ViewDocVnDblClick(Sender: TObject);
    procedure acSendDocVnToRarExecute(Sender: TObject);
    procedure acDelDocVnExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }

    procedure Init;
  end;

var
  fmDocVn: TfmDocVn;

procedure ShowFormDocVn;

implementation

{$R *.dfm}

uses ViewXML, EgaisDecode, ViewCards, dmRar, UTMExch, ViewSpecOut, MCrystDocs, ProDocs, History, UTMArh, UTMCheck, DocHeaderCB,
  ViewSpec, Main, ViewSpecVn, dbs;

procedure ShowFormDocVn;
begin
  if Assigned(fmDocVn)=False then //����� �� ����������, � ����� �������
    fmDocVn:=TfmDocVn.Create(Application);
  if fmDocVn.WindowState=wsMinimized then //���� ���� ��������, �� ������������� ���
    fmDocVn.WindowState:=wsNormal;

  fmDocVn.deDateBeg.Date:=Date-7;
  fmDocVn.deDateEnd.Date:=Date;
  fmDocVn.Init;
  fmDocVn.Show;
end;

procedure TfmDocVn.Init;
var
  FocusedRow, TopRow: Integer;
  flag: boolean;
begin
  with dmR do
  begin
    try
      //<--Refresh � ��������������� ������� ������� � �������
      flag:=quDocsVnHD.Active;  //���������� ���� �� ������� �������, ��� �������������� ������� �������
      TopRow := ViewDocVn.Controller.TopRowIndex;
      FocusedRow := ViewDocVn.DataController.FocusedRowIndex;
      //-->
      ViewDocVn.BeginUpdate;

      quDocsVnHD.Active:=False;
      quDocsVnHD.ParamByName('IDATEB').AsInteger:=Trunc(deDateBeg.Date);
      quDocsVnHD.ParamByName('IDATEE').AsInteger:=Trunc(deDateEnd.Date);
      quDocsVnHD.ParamByName('RARID').AsString:=CommonSet.FSRAR_ID;
      quDocsVnHD.Active:=True;
      quDocsVnHD.First;
      //-->
    finally
      ViewDocVn.EndUpdate;
      //<--��������������� �������
      if flag then begin
        ViewDocVn.DataController.FocusedRowIndex := FocusedRow;
        ViewDocVn.Controller.TopRowIndex := TopRow;
      end;
      //-->
    end;
  end;

//  FSRAR_ID:=CommonSet.FSRAR_ID;
end;


procedure TfmDocVn.ViewDocVnDblClick(Sender: TObject);
begin
  // ������������
  with dmR do
  begin
    if quDocsVnHD.RecordCount>0 then
    begin
      if (ViewDocVn.Controller.FocusedColumn.Name='ViewDocVnSENDXML') then
      begin
        quSelDocVn1.Active:=False;
        quSelDocVn1.ParamByName('RARID').AsString:=quDocsVnHDFSRARID.AsString;
        quSelDocVn1.ParamByName('IDATE').AsInteger:=quDocsVnHDIDATE.AsInteger;
        quSelDocVn1.ParamByName('SID').AsString:=quDocsVnHDSID.AsString;
        quSelDocVn1.Active:=True;
        if quSelDocVn1.RecordCount>0 then
        begin
          ShowXMLView(StrToIntDef(quSelDocVn1NUMBER.AsString,0),quSelDocVn1SENDXML.AsString);
        end;
        Exit;
      end;

      if (ViewDocVn.Controller.FocusedColumn.Name='ViewDocVnTICK1') then
      begin
        if (Abs(quDocsVnHDTICK1.AsInteger)>0) then
        begin
          quReplyRec.Active:=False;
          quReplyRec.SQL.Clear;
          quReplyRec.SQL.Add('SELECT * FROM dbo.REPLYLIST');
          quReplyRec.SQL.Add('  WHERE ID='+its(Abs(quDocsVnHDTICK1.AsInteger)));
          quReplyRec.SQL.Add('  and FSRARID='''+quDocsVnHDFSRARID.AsString+'''');
          quReplyRec.Active:=True;

          if quReplyRec.RecordCount>0 then
            ShowXMLView(quDocsVnHDTICK1.AsInteger,quReplyRecReplyFile.AsString);
        end;
        Exit;
      end;

      if (ViewDocVn.Controller.FocusedColumn.Name='ViewDocVnTICK2') then
      begin
        if Abs(quDocsVnHDTICK2.AsInteger)>0 then
        begin
          quReplyRec.Active:=False;
          quReplyRec.SQL.Clear;
          quReplyRec.SQL.Add('SELECT * FROM dbo.REPLYLIST');
          quReplyRec.SQL.Add('  WHERE ID='+its(Abs(quDocsVnHDTICK2.AsInteger)));
          quReplyRec.SQL.Add('  and FSRARID='''+quDocsVnHDFSRARID.AsString+'''');
          quReplyRec.Active:=True;

          if quReplyRec.RecordCount>0 then
            ShowXMLView(quDocsVnHDTICK2.AsInteger,quReplyRecReplyFile.AsString);
        end;
        Exit;
      end;

      if quDocsVnHDIACTIVE.AsInteger=0 then
        ShowSpecViewVn(quDocsVnHDFSRARID.AsString,quDocsVnHDSID.AsString,quDocsVnHDNUMBER.AsString,quDocsVnHDIDATE.AsInteger,1,quDocsVnHDSIDIN.AsString)
      else
        ShowSpecViewVn(quDocsVnHDFSRARID.AsString,quDocsVnHDSID.AsString,quDocsVnHDNUMBER.AsString,quDocsVnHDIDATE.AsInteger,0,quDocsVnHDSIDIN.AsString);

    end;
  end;
end;

procedure TfmDocVn.acCloseExecute(Sender: TObject);
begin
  //�������
  Close;
//  fmMain.Close;
end;

procedure TfmDocVn.acDelDocVnExecute(Sender: TObject);
begin
  //������� �������� �������
  with dmR do
  begin
    if quDocsVnHD.RecordCount>0 then
    begin
      if MessageDlg('������� �������� ����������� '+quDocsVnHDNUMBER.AsString+' �� '+ds1(quDocsVnHDIDATE.AsInteger)+'?',mtConfirmation, [mbYes, mbNo], 0, mbYes) = mrYes then
      begin
        ClearMessageLogLocal;
        ShowMessageLogLocal('�����.. ���� �������� ���������');

        if quDocsVnHDIACTIVE.AsInteger=0 then
        begin
          try
            quS.Active:=False;
            quS.SQL.Clear;
            quS.SQL.Add('');

            quS.SQL.Add('delete from dbo.ADOCSVN_HD');
            quS.SQL.Add('WHERE');
            quS.SQL.Add('  FSRARID = '''+quDocsVnHDFSRARID.AsString+'''');
            quS.SQL.Add('  AND IDATE = '+its(quDocsVnHDIDATE.AsInteger));
            quS.SQL.Add('  AND SID = '''+quDocsVnHDSID.AsString+'''');
            quS.ExecSQL;

            Init;
          except
          end;
        end else ShowMessageLogLocal('  �������� ������ ���������. �������� ����������.');


        ShowMessageLogLocal('������� ��������.');
      end;
    end;
  end;
end;

procedure TfmDocVn.acExpExcelExecute(Sender: TObject);
begin
  ExportGridToFile(formatdatetime('ddmmyyyy',deDateBeg.Date)+'_'+formatdatetime('ddmmyyyy',deDateEnd.Date)+'_'+formatdatetime('ddmmyyyy_hh_nn_ss',now)+'.xls', GridDocVn);
end;

procedure TfmDocVn.acRefreshExecute(Sender: TObject);
begin
  //������
  Init;
end;

procedure TfmDocVn.acSelectDateExecute(Sender: TObject);
begin
  //��������
  Init;
end;

procedure TfmDocVn.acSendDocVnToRarExecute(Sender: TObject);
var Rec:TcxCustomGridRecord;
    i,j,iC:Integer;
    FSRARID,SID,STYPE:string;
    IDATE,IACTIVE:Integer;
begin
  //�������� ��������� � ���

  if ViewDocVn.Controller.SelectedRecordCount>0 then
  begin
    ClearMessageLogLocal;
    ShowMessageLogLocal('����� ... ���� ��������� ����������.');
    iC:=0;

    FSRARID:='';
    SID:='';
    IDATE:=0;
    STYPE:='';

    for i:=0 to ViewDocVn.Controller.SelectedRecordCount-1 do
    begin
      Rec:=ViewDocVn.Controller.SelectedRecords[i];

      for j:=0 to Rec.ValueCount-1 do
      begin
        if ViewDocVn.Columns[j].Name='ViewDocVnFSRARID' then begin FSRARID:=Rec.Values[j]; end;
        if ViewDocVn.Columns[j].Name='ViewDocVnIDATE' then begin IDATE:=Rec.Values[j]; end;
        if ViewDocVn.Columns[j].Name='ViewDocVnSID' then begin SID:=Rec.Values[j]; end;
        if ViewDocVn.Columns[j].Name='ViewDocVnIACTIVE' then begin IACTIVE:=Rec.Values[j]; end;
        if ViewDocVn.Columns[j].Name='ViewDocVnSTYPE' then begin STYPE:=Rec.Values[j]; end;

      end;
      if (FSRARID>'')and(IDATE>0)and(SID>'')and(IACTIVE=0)  then  //��� ����������� �� 2-�� �������
      begin
        with dmR do
        begin
          ShowMessageLogLocal('    ���. '+FSRARID+','+ds1(iDate)+','+SID);
          if STYPE='ToShop' then
          begin
            if prSendDocVnPrivate(FSRARID,SID,IDATE,MemoLogLocal,dmR.FDConnection,CommonSet.IPUTM) then
            begin
              quS.Active:=False;
              quS.SQL.Clear;
              quS.SQL.Add('');

              quS.SQL.Add('UPDATE dbo.ADOCSVN_HD');
              quS.SQL.Add('SET IACTIVE = 1');
              quS.SQL.Add(',SDATE = GETDATE()');
              quS.SQL.Add('WHERE');
              quS.SQL.Add('  FSRARID = '''+FSRARID+'''');
              quS.SQL.Add('  AND IDATE = '+its(IDATE));
              quS.SQL.Add('  AND SID = '''+SID+'''');
              quS.ExecSQL;
            end;
            inc(iC);
          end;
          if STYPE='FromShop' then
          begin
            if prSendDocVn1Private(FSRARID,SID,IDATE,MemoLogLocal,dmR.FDConnection,CommonSet.IPUTM) then
            begin
              quS.Active:=False;
              quS.SQL.Clear;
              quS.SQL.Add('');

              quS.SQL.Add('UPDATE dbo.ADOCSVN_HD');
              quS.SQL.Add('SET IACTIVE = 1');
              quS.SQL.Add(',SDATE = GETDATE()');
              quS.SQL.Add('WHERE');
              quS.SQL.Add('  FSRARID = '''+FSRARID+'''');
              quS.SQL.Add('  AND IDATE = '+its(IDATE));
              quS.SQL.Add('  AND SID = '''+SID+'''');
              quS.ExecSQL;
            end;
            inc(iC);
          end;
        end;
      end;
    end;
    init;
    ShowMessageLogLocal('���������� '+its(iC)+' ����������.');
    ShowMessageLogLocal('������� ��������.');
  end;

end;

procedure TfmDocVn.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
  fmDocVn:=Nil;
end;

procedure TfmDocVn.FormCreate(Sender: TObject);
begin
  ClearMessageLogLocal;
end;

procedure TfmDocVn.deDateBegChange(Sender: TObject);
begin
  if dmR.FDConnection.Connected then Init;
end;

end.
