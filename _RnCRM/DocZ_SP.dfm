object fmDocZ_SP: TfmDocZ_SP
  Left = 0
  Top = 0
  Caption = #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1103' '#1076#1086#1082#1091#1084#1077#1085#1090#1072' '#1072#1083#1082#1086#1075#1086#1083#1100#1085#1086#1081' '#1076#1077#1082#1083#1072#1088#1072#1094#1080#1080
  ClientHeight = 656
  ClientWidth = 1093
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object GridSpecZ: TcxGrid
    Left = 0
    Top = 145
    Width = 1093
    Height = 511
    Align = alClient
    TabOrder = 0
    LookAndFeel.Kind = lfOffice11
    LookAndFeel.NativeStyle = False
    RootLevelOptions.DetailTabsPosition = dtpTop
    RootLevelOptions.TabsForEmptyDetails = False
    object ViewSpecZ: TcxGridDBTableView
      PopupMenu = PopupMenu1
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.InfoPanel.Visible = True
      Navigator.Visible = True
      DataController.DataSource = dsquSpecZ
      DataController.Summary.DefaultGroupSummaryItems = <
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'RSUM'
          Column = ViewSpecZRSUM
        end>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'RSUM'
          Column = ViewSpecZRSUM
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Inserting = False
      OptionsSelection.MultiSelect = True
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      OptionsView.HeaderHeight = 50
      object ViewSpecZNUM: TcxGridDBColumn
        Caption = #8470' '#1087#1087
        DataBinding.FieldName = 'NUM'
      end
      object ViewSpecZICODE: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1073#1083#1102#1076#1072
        DataBinding.FieldName = 'ICODE'
      end
      object ViewSpecZNAMECARD: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'NAMECARD'
        Width = 322
      end
      object ViewSpecZVESZ: TcxGridDBColumn
        Caption = #1042#1077#1089' '#1087#1086' '#1079#1072#1082#1072#1079#1091
        DataBinding.FieldName = 'VESZ'
      end
      object ViewSpecZQUANTZ: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086' '#1087#1086' '#1079#1072#1082#1072#1079#1091' '
        DataBinding.FieldName = 'QUANTZ'
      end
      object ViewSpecZVESPRO: TcxGridDBColumn
        Caption = #1042#1077#1089' '#1080#1079#1075#1086#1090#1086#1074#1083#1077#1085#1085#1086#1075#1086
        DataBinding.FieldName = 'VESPRO'
      end
      object ViewSpecZQUANTPRO: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086' '#1080#1079#1075#1086#1090#1086#1074#1083#1077#1085#1085#1086#1075#1086' '
        DataBinding.FieldName = 'QUANTPRO'
      end
      object ViewSpecZVESSEND: TcxGridDBColumn
        Caption = #1042#1077#1089' '#1086#1090#1075#1088#1091#1078#1077#1085#1085#1086#1075#1086'  '
        DataBinding.FieldName = 'VESSEND'
      end
      object ViewSpecZQUANTSEND: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086' '#1086#1090#1075#1088#1091#1078#1077#1085#1085#1086#1075#1086' '
        DataBinding.FieldName = 'QUANTSEND'
      end
      object ViewSpecZPRICE: TcxGridDBColumn
        Caption = #1062#1077#1085#1072' '#1079#1072' '#1077#1076
        DataBinding.FieldName = 'PRICE'
      end
      object ViewSpecZRSUM: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072
        DataBinding.FieldName = 'RSUM'
      end
    end
    object LevelSpecZ: TcxGridLevel
      Caption = #1054#1073#1086#1088#1086#1090#1099
      GridView = ViewSpecZ
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1093
    Height = 145
    Align = alTop
    BevelInner = bvLowered
    Color = 16765864
    ParentBackground = False
    TabOrder = 1
    object cxButton1: TcxButton
      Left = 882
      Top = 16
      Width = 143
      Height = 50
      Action = acSave
      TabOrder = 0
    end
    object cxButton2: TcxButton
      Left = 882
      Top = 72
      Width = 143
      Height = 49
      Action = acClose
      TabOrder = 1
    end
    object cxLabel1: TcxLabel
      Left = 16
      Top = 16
      Caption = #1047#1072#1082#1072#1079#1095#1080#1082
      Transparent = True
    end
    object cxLookupComboBox1: TcxLookupComboBox
      Left = 81
      Top = 15
      Properties.KeyFieldNames = 'IDCLI'
      Properties.ListColumns = <
        item
          Caption = #1053#1072#1079#1074#1072#1085#1080#1077
          FieldName = 'NAME'
        end
        item
          Caption = #1058#1077#1083#1077#1092#1086#1085
          FieldName = 'PHONE'
        end
        item
          Caption = #1044#1072#1090#1072' '#1088#1086#1078#1076#1077#1085#1080#1103
          FieldName = 'DBURN'
        end
        item
          Caption = #1058#1080#1087' '#1087#1086#1082#1091#1087#1072#1090#1077#1083#1103
          FieldName = 'CLITYPEN'
        end>
      Properties.ListSource = dsquCli
      TabOrder = 3
      Width = 264
    end
    object cxLabel2: TcxLabel
      Left = 16
      Top = 58
      Caption = #1044#1072#1090#1072' '#1080#1079#1075#1086#1090#1086#1074#1083#1077#1085#1080#1103
      Transparent = True
    end
    object cxDateEdit1: TcxDateEdit
      Left = 141
      Top = 56
      TabOrder = 5
      Width = 116
    end
    object cxTimeEdit1: TcxTimeEdit
      Left = 272
      Top = 56
      EditValue = 0d
      TabOrder = 6
      Width = 96
    end
    object cxLabel3: TcxLabel
      Left = 16
      Top = 85
      Caption = #1044#1072#1090#1072' '#1076#1086#1089#1090#1072#1074#1082#1080
      Transparent = True
    end
    object cxDateEdit2: TcxDateEdit
      Left = 141
      Top = 83
      TabOrder = 8
      Width = 116
    end
    object cxTimeEdit2: TcxTimeEdit
      Left = 273
      Top = 82
      EditValue = 0d
      TabOrder = 9
      Width = 96
    end
    object cxButton3: TcxButton
      Left = 343
      Top = 13
      Width = 25
      Height = 25
      Caption = '+'
      TabOrder = 10
    end
    object cxLabel4: TcxLabel
      Left = 392
      Top = 57
      Caption = #1057#1090#1086#1080#1084#1086#1089#1090#1100' '#1076#1086#1089#1090#1072#1074#1082#1080
      Transparent = True
    end
    object cxCurrencyEdit1: TcxCurrencyEdit
      Left = 515
      Top = 56
      EditValue = 0.000000000000000000
      Properties.Alignment.Horz = taRightJustify
      TabOrder = 12
      Width = 121
    end
    object cxCurrencyEdit2: TcxCurrencyEdit
      Left = 741
      Top = 56
      EditValue = 0.000000000000000000
      ParentFont = False
      Properties.Alignment.Horz = taRightJustify
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsBold]
      Style.IsFontAssigned = True
      TabOrder = 13
      Width = 121
    end
    object cxLabel5: TcxLabel
      Left = 664
      Top = 57
      Caption = #1057#1091#1084#1084#1072' '#1042#1057#1045#1043#1054
      Transparent = True
    end
    object cxLabel6: TcxLabel
      Left = 416
      Top = 16
      Caption = #1058#1077#1083'.'
      Transparent = True
    end
    object cxTextEdit1: TcxTextEdit
      Left = 456
      Top = 15
      TabOrder = 16
      Text = 'cxTextEdit1'
      Width = 137
    end
    object cxLabel7: TcxLabel
      Left = 392
      Top = 85
      Caption = #1040#1076#1088#1077#1089' '#1076#1086#1089#1090#1072#1074#1082#1080
      Transparent = True
    end
    object cxTextEdit2: TcxTextEdit
      Left = 515
      Top = 83
      TabOrder = 18
      Text = 'cxTextEdit2'
      Width = 347
    end
    object cxLabel8: TcxLabel
      Left = 16
      Top = 112
      Caption = #1050#1072#1082' '#1091#1079#1085#1072#1083#1080
      Transparent = True
    end
    object cxLabel9: TcxLabel
      Left = 613
      Top = 17
      Caption = #1055#1086#1074#1086#1076
      Transparent = True
    end
    object cxLabel10: TcxLabel
      Left = 392
      Top = 112
      Caption = #1050#1086#1075#1086' '#1088#1077#1082#1086#1084#1077#1085#1076#1086#1074#1072#1083
      Transparent = True
    end
    object cxTextEdit4: TcxTextEdit
      Left = 515
      Top = 110
      TabOrder = 22
      Text = 'cxTextEdit2'
      Width = 347
    end
    object cxLookupComboBox2: TcxLookupComboBox
      Left = 112
      Top = 110
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Caption = #1053#1072#1079#1074#1072#1085#1080#1077
          FieldName = 'NAME'
        end>
      Properties.ListSource = dsquSourceInfo
      TabOrder = 23
      Width = 233
    end
    object cxLookupComboBox3: TcxLookupComboBox
      Left = 656
      Top = 16
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Caption = #1053#1072#1079#1074#1072#1085#1080#1077
          FieldName = 'NAME'
        end>
      Properties.ListSource = dsquPovod
      TabOrder = 24
      Width = 185
    end
    object cxButton4: TcxButton
      Left = 343
      Top = 108
      Width = 25
      Height = 25
      Caption = '+'
      TabOrder = 25
    end
    object cxButton5: TcxButton
      Left = 837
      Top = 14
      Width = 25
      Height = 25
      Caption = '+'
      TabOrder = 26
    end
  end
  object ActionList1: TActionList
    Images = dmS.SmallImage
    Left = 712
    Top = 272
    object acClose: TAction
      Caption = #1047#1072#1082#1088#1099#1090#1100
      ImageIndex = 100
      OnExecute = acCloseExecute
    end
    object acSave: TAction
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' (Ctrl + S)'
      ImageIndex = 7
      ShortCut = 16467
      OnExecute = acSaveExecute
    end
    object acTestSpec: TAction
      Caption = #1055#1088#1086#1074#1077#1088#1082#1072' '#1089#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1080
      ImageIndex = 38
    end
    object acDelPos: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1087#1086#1079#1080#1094#1080#1102
      ImageIndex = 4
      OnExecute = acDelPosExecute
    end
    object acGetInfoSel: TAction
      Caption = #1047#1072#1087#1088#1086#1089#1080#1090#1100' '#1080#1085#1092#1086#1088#1084#1072#1094#1080#1102' '#1087#1086' '#1074#1099#1076#1077#1083#1077#1085#1085#1099#1084
      ImageIndex = 50
    end
    object acAddPos: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1087#1086#1079#1080#1094#1080#1102
      ImageIndex = 3
      OnExecute = acAddPosExecute
    end
  end
  object quSpecZ: TFDQuery
    CachedUpdates = True
    Connection = dmS.FDConnection
    Transaction = dmS.FDTrans
    UpdateTransaction = dmS.FDTransUpdate
    UpdateObject = UpdSQL1
    SQL.Strings = (
      'SELECT [IDH]'
      '      ,[ID]'
      '      ,[NUM]'
      '      ,[ICODE]'
      '      ,[NAMECARD]'
      '      ,[VESZ]'
      '      ,[QUANTZ]'
      '      ,[VESPRO]'
      '      ,[QUANTPRO]'
      '      ,[VESSEND]'
      '      ,[QUANTSEND]'
      '      ,[PRICE]'
      '      ,[RSUM]'
      '  FROM [dbo].[DOCZ_SP]'
      '  where IDH=:IDH')
    Left = 88
    Top = 360
    ParamData = <
      item
        Name = 'IDH'
        DataType = ftInteger
        ParamType = ptInput
        Value = 4
      end>
    object quSpecZIDH: TLargeintField
      FieldName = 'IDH'
      Origin = 'IDH'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quSpecZID: TLargeintField
      AutoGenerateValue = arAutoInc
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object quSpecZNUM: TIntegerField
      FieldName = 'NUM'
      Origin = 'NUM'
    end
    object quSpecZICODE: TIntegerField
      FieldName = 'ICODE'
      Origin = 'ICODE'
    end
    object quSpecZNAMECARD: TStringField
      FieldName = 'NAMECARD'
      Origin = 'NAMECARD'
      Size = 200
    end
    object quSpecZVESZ: TSingleField
      FieldName = 'VESZ'
      Origin = 'VESZ'
      DisplayFormat = '0.000'
    end
    object quSpecZQUANTZ: TIntegerField
      FieldName = 'QUANTZ'
      Origin = 'QUANTZ'
      DisplayFormat = '0.000'
    end
    object quSpecZVESPRO: TSingleField
      FieldName = 'VESPRO'
      Origin = 'VESPRO'
      DisplayFormat = '0.000'
    end
    object quSpecZQUANTPRO: TIntegerField
      FieldName = 'QUANTPRO'
      Origin = 'QUANTPRO'
      DisplayFormat = '0.000'
    end
    object quSpecZVESSEND: TSingleField
      FieldName = 'VESSEND'
      Origin = 'VESSEND'
      DisplayFormat = '0.000'
    end
    object quSpecZQUANTSEND: TIntegerField
      FieldName = 'QUANTSEND'
      Origin = 'QUANTSEND'
      DisplayFormat = '0.000'
    end
    object quSpecZPRICE: TSingleField
      FieldName = 'PRICE'
      Origin = 'PRICE'
      DisplayFormat = '0.00'
    end
    object quSpecZRSUM: TSingleField
      FieldName = 'RSUM'
      Origin = 'RSUM'
      DisplayFormat = '0.00'
    end
  end
  object UpdSQL1: TFDUpdateSQL
    Connection = dmS.FDConnection
    InsertSQL.Strings = (
      'INSERT INTO RNORDERS.dbo.DOCZ_SP'
      '(NUM, ICODE, NAMECARD, VESZ, QUANTZ, '
      '  VESPRO, QUANTPRO, VESSEND, QUANTSEND, '
      '  PRICE, RSUM)'
      
        'VALUES (:NEW_NUM, :NEW_ICODE, :NEW_NAMECARD, :NEW_VESZ, :NEW_QUA' +
        'NTZ, '
      '  :NEW_VESPRO, :NEW_QUANTPRO, :NEW_VESSEND, :NEW_QUANTSEND, '
      '  :NEW_PRICE, :NEW_RSUM);'
      'SELECT SCOPE_IDENTITY() AS ID')
    ModifySQL.Strings = (
      'UPDATE RNORDERS.dbo.DOCZ_SP'
      
        'SET NUM = :NEW_NUM, ICODE = :NEW_ICODE, NAMECARD = :NEW_NAMECARD' +
        ', '
      '  VESZ = :NEW_VESZ, QUANTZ = :NEW_QUANTZ, VESPRO = :NEW_VESPRO, '
      
        '  QUANTPRO = :NEW_QUANTPRO, VESSEND = :NEW_VESSEND, QUANTSEND = ' +
        ':NEW_QUANTSEND, '
      '  PRICE = :NEW_PRICE, RSUM = :NEW_RSUM'
      'WHERE IDH = :OLD_IDH AND ID = :OLD_ID;'
      'SELECT ID'
      'FROM RNORDERS.dbo.DOCZ_SP'
      'WHERE IDH = :NEW_IDH AND ID = :NEW_ID')
    DeleteSQL.Strings = (
      'DELETE FROM RNORDERS.dbo.DOCZ_SP'
      'WHERE IDH = :OLD_IDH AND ID = :OLD_ID')
    FetchRowSQL.Strings = (
      
        'SELECT IDH, SCOPE_IDENTITY() AS ID, NUM, ICODE, NAMECARD, VESZ, ' +
        'QUANTZ, '
      '  VESPRO, QUANTPRO, VESSEND, QUANTSEND, PRICE, RSUM'
      'FROM RNORDERS.dbo.DOCZ_SP'
      'WHERE IDH = :IDH AND ID = :ID')
    Left = 176
    Top = 360
  end
  object dsquSpecZ: TDataSource
    DataSet = quSpecZ
    Left = 88
    Top = 424
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 224
    Top = 256
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svFont]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
    end
    object cxStyle2: TcxStyle
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 608
    Top = 272
    object N4: TMenuItem
      Action = acAddPos
    end
    object N1: TMenuItem
      Action = acDelPos
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object N3: TMenuItem
      Action = acGetInfoSel
    end
  end
  object quE: TFDQuery
    Connection = dmS.FDConnection
    Left = 80
    Top = 280
  end
  object quS: TFDQuery
    Connection = dmS.FDConnection
    Left = 160
    Top = 280
  end
  object quCli: TFDQuery
    Connection = dmS.FDConnection
    SQL.Strings = (
      'SELECT cli.[IDCLI]'
      '      ,cli.[CLITYPE]'
      #9'  ,clit.[NAME] as CLITYPEN'
      '      ,cli.[NAME]'
      '      ,cli.[PHONE]'
      '      ,cli.[DBURN]'
      '      ,cli.[COMPANY]'
      '      ,cli.[CARDSCAT]'
      #9'  ,cat.[NAME] as CARDSCATN'
      #9'  ,cli.[PERIODRING]'
      '      ,cli.[SEMAIL]'
      '      ,cli.[IACTIVE]'
      '      ,cli.[HOTCLI]'
      '      ,cli.[DEL]'
      '  FROM [dbo].[ORDERSCLI] cli'
      '  left join [dbo].[CLITYPES] clit on clit.ID=cli.[CLITYPE]'
      '  left join [dbo].[CARDSCATS] cat on cat.ID=cli.[CARDSCAT]'
      'where cli.[DEL]=0')
    Left = 328
    Top = 280
    object quCliIDCLI: TLargeintField
      FieldName = 'IDCLI'
      Origin = 'IDCLI'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quCliCLITYPE: TIntegerField
      FieldName = 'CLITYPE'
      Origin = 'CLITYPE'
    end
    object quCliCLITYPEN: TStringField
      FieldName = 'CLITYPEN'
      Origin = 'CLITYPEN'
      Size = 150
    end
    object quCliNAME: TStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      Size = 200
    end
    object quCliPHONE: TStringField
      FieldName = 'PHONE'
      Origin = 'PHONE'
      Size = 200
    end
    object quCliDBURN: TSQLTimeStampField
      FieldName = 'DBURN'
      Origin = 'DBURN'
    end
    object quCliCOMPANY: TStringField
      FieldName = 'COMPANY'
      Origin = 'COMPANY'
      Size = 300
    end
    object quCliCARDSCAT: TIntegerField
      FieldName = 'CARDSCAT'
      Origin = 'CARDSCAT'
    end
    object quCliCARDSCATN: TStringField
      FieldName = 'CARDSCATN'
      Origin = 'CARDSCATN'
      Size = 150
    end
    object quCliPERIODRING: TSmallintField
      FieldName = 'PERIODRING'
      Origin = 'PERIODRING'
    end
    object quCliSEMAIL: TStringField
      FieldName = 'SEMAIL'
      Origin = 'SEMAIL'
      Size = 150
    end
    object quCliIACTIVE: TSmallintField
      FieldName = 'IACTIVE'
      Origin = 'IACTIVE'
    end
    object quCliHOTCLI: TSmallintField
      FieldName = 'HOTCLI'
      Origin = 'HOTCLI'
    end
    object quCliDEL: TSmallintField
      FieldName = 'DEL'
      Origin = 'DEL'
    end
  end
  object dsquCli: TDataSource
    DataSet = quCli
    Left = 328
    Top = 336
  end
  object quSourceInfo: TFDQuery
    Connection = dmS.FDConnection
    SQL.Strings = (
      'SELECT [ID],[NAME] FROM [RNORDERS].[dbo].[SOURCEINFO]')
    Left = 400
    Top = 280
    object quSourceInfoID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quSourceInfoNAME: TStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      Size = 300
    end
  end
  object dsquSourceInfo: TDataSource
    DataSet = quSourceInfo
    Left = 400
    Top = 336
  end
  object quPovod: TFDQuery
    Connection = dmS.FDConnection
    SQL.Strings = (
      'SELECT [ID],[NAME] FROM [RNORDERS].[dbo].[POVOD]')
    Left = 480
    Top = 280
    object quPovodID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quPovodNAME: TStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      Size = 300
    end
  end
  object dsquPovod: TDataSource
    DataSet = quPovod
    Left = 480
    Top = 336
  end
end
