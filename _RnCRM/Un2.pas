unit Un2;

interface

uses
  IniFiles,System.SysUtils;


type TPerson = record
     Id:Integer;
     Name,sCli:String;
     WinName:String;
     end;

Procedure ReadIni;
Procedure WriteIni;
function GetGridIniFile: string;
function GetFormIniFile: string;

Function its(i:Integer):String;
function dstime(d:TDateTime):String;
function dssqltime(d:TDateTime):String;
Function fts(rSum:Real):String;

Const CurIni:String = 'Profiles.ini';
      GridIni:String = 'ProfilesGr.ini';
      FormIni:String = 'ProfilesFr.ini';

Var Person:TPerson;
    CurDir:String;
    DBName:String;

implementation


Function fts(rSum:Real):String;
Var s:String;
begin
  s:=FloatToStr(rSum);
  while pos(',',s)>0 do s[pos(',',s)]:='.';
  Result:=s;
end;

function dssqltime(d:TDateTime):String;
begin
  result:=FormatDateTime('yyyymmdd hh:nn:ss',d);
end;

function dstime(d:TDateTime):String;
begin
  result:=FormatDateTime('dd.mm.yyyy hh:nn',d);
end;


Function its(i:Integer):String;
begin
  result:=IntToStr(i);
end;

function GetFormIniFile: string;
Var Str1:String;
begin
  if Person.Name<>'' then
  begin
    if not DirectoryExists(CurDir+'\'+Person.Name) then CreateDir(CurDir+'\'+Person.Name);
    Result:=CurDir+'\'+Person.Name+'\'+FormIni;
  end else
    Result:=CurDir+'\'+FormIni;
end;


function GetGridIniFile: string;
begin
  if not DirectoryExists(CurDir{+DelP(CommonSet.Ip)}+'\'+Person.Name) then CreateDir(CurDir{+DelP(CommonSet.Ip)}+'\'+Person.Name);

  Result:=CurDir{+DelP(CommonSet.Ip)}+'\'+Person.Name+'\'+GridIni;
end;

Procedure ReadIni;
Var f:TIniFile;
begin
  try
    f:=TIniFile.create(CurDir+CurIni);

    DBName:=f.ReadString('Config_','DBName','localhost:D:\_CasherRn\DBCONK\CASHERRN.gdb');
    f.WriteString('Config_','DBName',DBName);

    Person.Id:=f.ReadInteger('Config_','PersinId',0);
    f.WriteInteger('Config_','PersinId',Person.Id);

  finally
    f.Free;
  end;
end;

Procedure WriteIni;
Var f:TIniFile;
begin
  try
    f:=TIniFile.create(CurDir+CurIni);
    f.WriteInteger('Config_','PersinId',Person.Id);
  finally
    f.Free;
  end;
end;


end.
