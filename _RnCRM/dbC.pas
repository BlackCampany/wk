unit dbC;

interface

uses
  System.SysUtils, System.Classes, FIBDatabase, pFIBDatabase;

type
  TdmC = class(TDataModule)
    CasherRnDb: TpFIBDatabase;
    trSelect: TpFIBTransaction;
    trUpdate: TpFIBTransaction;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmC: TdmC;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure TdmC.DataModuleCreate(Sender: TObject);
begin
  CasherRnDb.Connected:=False;
//  CasherRnDb.DBName:=DBName;
  try
    CasherRnDb.Open;
  except
  end;
end;

end.
