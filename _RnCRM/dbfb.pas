unit dbfb;

interface

uses
  System.SysUtils, System.Classes, FIBDatabase, pFIBDatabase;

type
  TdmFb = class(TDataModule)
    CasherRnDb: TpFIBDatabase;
    trSelect: TpFIBTransaction;
    trUpdate: TpFIBTransaction;
    trDel: TpFIBTransaction;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmFb: TdmFb;

implementation

uses
  Un2;

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure TdmFb.DataModuleCreate(Sender: TObject);
begin
  CasherRnDb.Connected:=False;
  CasherRnDb.DBName:=DBName;
  try
    CasherRnDb.Open;
  except
  end;
end;

procedure TdmFb.DataModuleDestroy(Sender: TObject);
begin
  try
    CasherRnDb.Close;
  except
  end;
end;

end.
