unit Clients;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  CustomChildForm,
  Vcl.Controls, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus,
  cxControls, cxContainer, cxEdit, System.Actions, Vcl.ActnList, Data.DB,
  Data.Win.ADODB, cxTextEdit, cxMemo, Vcl.StdCtrls, Vcl.ExtCtrls, dxRibbonSkins,
  dxRibbonCustomizationForm, dxBar, dxBarExtItems, cxClasses, dxRibbon,
  cxCalendar, cxBarEditItem, dxStatusBar, dxRibbonStatusBar, cxDBLookupComboBox,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator,
  cxDBData, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridCustomView, cxGrid,vcl.Forms, cxLabel, cxCheckBox,
  JvComponentBase, JvFormPlacement, Dialogs, cxImageComboBox, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TfmClients = class(TfmCustomChildForm)
    ActionList1: TActionList;
    acRefresh: TAction;
    dxRibbon1: TdxRibbon;
    rtScales: TdxRibbonTab;
    dxBarManager1: TdxBarManager;
    bmAction: TdxBar;
    bmClose: TdxBar;
    bUpdate: TdxBarLargeButton;
    bClose: TdxBarLargeButton;
    acClose: TAction;
    sbSprScale: TdxRibbonStatusBar;
    GrClients: TcxGrid;
    ViewClients: TcxGridDBTableView;
    LevelClients: TcxGridLevel;
    JvFormStorage: TJvFormStorage;
    btnAdd: TdxBarLargeButton;
    btnDel: TdxBarLargeButton;
    btnEdit: TdxBarLargeButton;
    acAdd: TAction;
    acEdit: TAction;
    acDel: TAction;
    quOrdClients: TFDQuery;
    dsquOrdClients: TDataSource;
    quOrdClientsIDCLI: TLargeintField;
    quOrdClientsCLITYPE: TIntegerField;
    quOrdClientsCLITYPEN: TStringField;
    quOrdClientsNAME: TStringField;
    quOrdClientsPHONE: TStringField;
    quOrdClientsDBURN: TSQLTimeStampField;
    quOrdClientsCOMPANY: TStringField;
    quOrdClientsCARDSCAT: TIntegerField;
    quOrdClientsCARDSCATN: TStringField;
    quOrdClientsPERIODRING: TSmallintField;
    quOrdClientsSEMAIL: TStringField;
    quOrdClientsIACTIVE: TSmallintField;
    quOrdClientsHOTCLI: TSmallintField;
    ViewClientsIDCLI: TcxGridDBColumn;
    ViewClientsCLITYPE: TcxGridDBColumn;
    ViewClientsCLITYPEN: TcxGridDBColumn;
    ViewClientsNAME: TcxGridDBColumn;
    ViewClientsPHONE: TcxGridDBColumn;
    ViewClientsDBURN: TcxGridDBColumn;
    ViewClientsCOMPANY: TcxGridDBColumn;
    ViewClientsCARDSCAT: TcxGridDBColumn;
    ViewClientsCARDSCATN: TcxGridDBColumn;
    ViewClientsPERIODRING: TcxGridDBColumn;
    ViewClientsSEMAIL: TcxGridDBColumn;
    ViewClientsIACTIVE: TcxGridDBColumn;
    ViewClientsHOTCLI: TcxGridDBColumn;
    quOrdClientsDEL: TSmallintField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acCloseExecute(Sender: TObject);
    procedure acRefreshExecute(Sender: TObject);
    procedure acAddExecute(Sender: TObject);
    procedure acEditExecute(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ViewClientsCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Init;
  end;
  procedure ShowFormCli;

var
  fmClients: TfmClients;

implementation

uses
  Un2, dbs, AddSinglePar, AddCliOrd;

{$R *.dfm}

//��������� �������� �����
procedure ShowFormCli;
begin
  if Assigned(fmClients)=False then begin //����� �� ����������, � ����� �������
    fmClients:=TfmClients.Create(Application);
  end;
  fmClients.Init;
  fmClients.Show;
end;

procedure TfmClients.acAddExecute(Sender: TObject);
var sRet:string;
    iNum:Integer;
begin
  //���������� ����������
  if Assigned(fmAddCli)=False then begin //����� �� ����������, � ����� �������
    fmAddCli:=TfmAddCli.Create(Application);
  end;
  with fmAddCli do
  begin
    quCardsTypes.Active:=True;
    quCliTypes.Active:=True;
    quPeriods.Active:=True;

    cxTextEdit1.Text:='';
    cxTextEdit2.Text:='';
    cxDateEdit1.Date:=Date;
    cxTextEdit3.Text:='';
    cxLookupComboBox1.EditValue:=1;
    cxLookupComboBox2.EditValue:=1;
    cxLookupComboBox3.EditValue:=0;
    cxTextEdit4.Text:='';
    cxCheckBox1.Checked:=True;
    cxCheckBox2.Checked:=False;
  end;
  fmAddCli.ShowModal;
  if fmAddCli.ModalResult=mrOk then
  begin
   // ShowMessage('�������� '+fmAddCli.cxTextEdit1.Text);
    with fmAddCli do
    begin
      quOrdClients.Append;
      quOrdClientsIDCLI.AsInteger:=fGetId(1);
      quOrdClientsCLITYPE.AsInteger:=cxLookupComboBox1.EditValue;
      quOrdClientsNAME.AsString:=cxTextEdit1.Text;
      quOrdClientsPHONE.AsString:=cxTextEdit2.Text;
      quOrdClientsDBURN.AsDateTime:=cxDateEdit1.Date;
      quOrdClientsCOMPANY.AsString:=cxTextEdit3.Text;
      quOrdClientsCARDSCAT.AsInteger:=cxLookupComboBox2.EditValue;
      quOrdClientsPERIODRING.AsInteger:=cxLookupComboBox3.EditValue;
      quOrdClientsSEMAIL.AsString:=cxTextEdit4.Text;
      quOrdClientsIACTIVE.AsInteger:=cxCheckBox1.EditValue;
      quOrdClientsHOTCLI.AsInteger:=cxCheckBox2.EditValue;
      quOrdClientsDEL.AsInteger:=0;
      quOrdClients.Post;
    end;

    Init;
  end;

  fmAddCli.quCardsTypes.Active:=False;
  fmAddCli.quCliTypes.Active:=False;
  fmAddCli.quPeriods.Active:=False;

  fmAddCli.Release;
  fmAddCli:=nil;
end;

procedure TfmClients.acCloseExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmClients.acDelExecute(Sender: TObject);
var iErrors:integer;
begin
  // �������� -  ������� ����������
  if quOrdClients.RecordCount>0 then
  begin
    if MessageDlg('������� '+quOrdClientsNAME.AsString+' ?',mtConfirmation,[mbYes,mbNo],0) = mrYes then
    begin
      with dmS do
      begin
        quS.Active:=False;
        quS.SQL.Clear;
        quS.SQL.Add('');
        quS.SQL.Add('update [dbo].[ORDERSCLI]');
        quS.SQL.Add('Set [DEL]=1');
        quS.SQL.Add('where [IDCLI]='+its(quOrdClientsIDCLI.AsInteger));
        quS.ExecSQL;

        Init;
      end;
    end;
  end;
end;

procedure TfmClients.acEditExecute(Sender: TObject);
var sRet:string;
begin
  //�������������� ����������
  if quOrdClients.RecordCount>0 then
  begin
    if Assigned(fmAddCli)=False then begin //����� �� ����������, � ����� �������
      fmAddCli:=TfmAddCli.Create(Application);
    end;
    with fmAddCli do
    begin
      quCardsTypes.Active:=True;
      quCliTypes.Active:=True;
      quPeriods.Active:=True;

      cxTextEdit1.Text:=quOrdClientsNAME.AsString;
      cxTextEdit2.Text:=quOrdClientsPHONE.AsString;
      cxDateEdit1.Date:=quOrdClientsDBURN.AsDateTime;
      cxTextEdit3.Text:=quOrdClientsCOMPANY.AsString;
      cxLookupComboBox1.EditValue:=quOrdClientsCLITYPE.AsInteger;
      cxLookupComboBox2.EditValue:=quOrdClientsCARDSCAT.AsInteger;
      cxLookupComboBox3.EditValue:=quOrdClientsPERIODRING.AsInteger;
      cxTextEdit4.Text:=quOrdClientsSEMAIL.AsString;
      cxCheckBox1.EditValue:=quOrdClientsIACTIVE.AsInteger;
      cxCheckBox2.EditValue:=quOrdClientsHOTCLI.AsInteger;
    end;
    fmAddCli.ShowModal;
    if fmAddCli.ModalResult=mrOk then
    begin
     // ShowMessage('���������� '+fmAddCli.cxTextEdit1.Text);
      with fmAddCli do
      begin
        quOrdClients.Edit;
        quOrdClientsCLITYPE.AsInteger:=cxLookupComboBox1.EditValue;
        quOrdClientsNAME.AsString:=cxTextEdit1.Text;
        quOrdClientsPHONE.AsString:=cxTextEdit2.Text;
        quOrdClientsDBURN.AsDateTime:=cxDateEdit1.Date;
        quOrdClientsCOMPANY.AsString:=cxTextEdit3.Text;
        quOrdClientsCARDSCAT.AsInteger:=cxLookupComboBox2.EditValue;
        quOrdClientsPERIODRING.AsInteger:=cxLookupComboBox3.EditValue;
        quOrdClientsSEMAIL.AsString:=cxTextEdit4.Text;
        quOrdClientsIACTIVE.AsInteger:=cxCheckBox1.EditValue;
        quOrdClientsHOTCLI.AsInteger:=cxCheckBox2.EditValue;

        quOrdClients.Post;
      end;

      Init;
    end;

    fmAddCli.quCardsTypes.Active:=False;
    fmAddCli.quCliTypes.Active:=False;
    fmAddCli.quPeriods.Active:=False;

    fmAddCli.Release;
    fmAddCli:=nil;
  end;
end;

procedure TfmClients.acRefreshExecute(Sender: TObject);
begin
  Init;
end;

procedure TfmClients.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
  if not(fsModal in FormState) then
    fmClients:=Nil;
  ViewClients.StoreToIniFile(GetGridIniFile,False,[gsoUseSummary,gsoUseFilter]);
end;

procedure TfmClients.FormCreate(Sender: TObject);
begin
  ViewClients.RestoreFromIniFile(GetGridIniFile);
end;

procedure TfmClients.Init;
var
  FocusedRow, TopRow: Integer;
  flag: boolean;
begin
  try
    //<--Refresh � ��������������� ������� ������� � �������
    flag:=quOrdClients.Active;  //���������� ���� �� ������� �������, ��� �������������� ������� �������
    TopRow := ViewClients.Controller.TopRowIndex;
    FocusedRow := ViewClients.DataController.FocusedRowIndex;

    ViewClients.BeginUpdate;
    quOrdClients.Active:=False;
    quOrdClients.Active:=True;
  finally
    ViewClients.EndUpdate;
    //<--��������������� �������
    if flag then begin
      ViewClients.DataController.FocusedRowIndex := FocusedRow;
      ViewClients.Controller.TopRowIndex := TopRow;
    end;
  end;
end;

procedure TfmClients.ViewClientsCustomDrawCell(Sender: TcxCustomGridTableView; ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var Value:string;
begin
  {
  Value:=AViewInfo.GridRecord.DisplayTexts[ViewScISTATUS.Index];
  if Value='����������' then ACanvas.Canvas.Brush.Color := $00B3B3FF;
  }
end;

end.

