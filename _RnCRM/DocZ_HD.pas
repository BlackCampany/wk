unit DocZ_HD;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  CustomChildForm, System.UITypes, ShellApi, httpsend, synautil, nativexml, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxRibbonSkins, dxRibbonCustomizationForm, cxContainer, cxEdit, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator,
  Data.DB, cxDBData, cxImageComboBox, cxTextEdit, cxDBLookupComboBox, cxCheckBox, cxCalendar, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.Menus, System.Actions, Vcl.ActnList, dxBar, cxBarEditItem, dxBarExtItems, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid, cxMemo, Vcl.ExtCtrls, dxRibbon, cxTimeEdit;

type
  TfmDocZ_HD = class(TfmCustomChildForm)
    dxRibbon1: TdxRibbon;
    dxRibbon1Tab1: TdxRibbonTab;
    dxBarManager1: TdxBarManager;
    bmApplyDate: TdxBar;
    bmClose: TdxBar;
    btnDone: TdxBarLargeButton;
    btnClose: TdxBarLargeButton;
    deDateBeg: TdxBarDateCombo;
    deDateEnd: TdxBarDateCombo;
    dxBarGroup1: TdxBarGroup;
    ActionList1: TActionList;
    acRefresh: TAction;
    acClose: TAction;
    acSelectDate: TAction;
    GridDocZ: TcxGrid;
    acExpExcel: TAction;
    LevelDocZ: TcxGridLevel;
    ViewDocZ: TcxGridDBTableView;
    pmDocVn: TPopupMenu;
    N1: TMenuItem;
    dxBarLargeButton12: TdxBarLargeButton;
    N2: TMenuItem;
    Excel1: TMenuItem;
    N3: TMenuItem;
    quDocZHD: TFDQuery;
    dsquDocZHD: TDataSource;
    quDocZHDIDATE: TIntegerField;
    quDocZHDID: TLargeintField;
    quDocZHDTIMEPRO: TSQLTimeStampField;
    quDocZHDTIMEDOST: TSQLTimeStampField;
    quDocZHDIDCLI: TIntegerField;
    quDocZHDPHONE: TStringField;
    quDocZHDADDRES: TStringField;
    quDocZHDIDFROM: TIntegerField;
    quDocZHDPOVOD: TIntegerField;
    quDocZHDISTATUS: TIntegerField;
    quDocZHDCOMMENT: TStringField;
    quDocZHDSUMDOSTAV: TBCDField;
    quDocZHDCLINAME: TStringField;
    quDocZHDNAMEINFO: TStringField;
    quDocZHDSPOVOD: TStringField;
    quDocZHDSUMR: TSingleField;
    quDocZHDSUMITOG: TSingleField;
    ViewDocZIDATE: TcxGridDBColumn;
    ViewDocZID: TcxGridDBColumn;
    ViewDocZTIMEPRO: TcxGridDBColumn;
    ViewDocZTIMEDOST: TcxGridDBColumn;
    ViewDocZIDCLI: TcxGridDBColumn;
    ViewDocZPHONE: TcxGridDBColumn;
    ViewDocZADDRES: TcxGridDBColumn;
    ViewDocZISTATUS: TcxGridDBColumn;
    ViewDocZCOMMENT: TcxGridDBColumn;
    ViewDocZSUMDOSTAV: TcxGridDBColumn;
    ViewDocZCLINAME: TcxGridDBColumn;
    ViewDocZNAMEINFO: TcxGridDBColumn;
    ViewDocZSPOVOD: TcxGridDBColumn;
    ViewDocZSUMR: TcxGridDBColumn;
    ViewDocZSUMITOG: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    acAddDoc: TAction;
    acEditDoc: TAction;
    acViewDoc: TAction;
    acDelDoc: TAction;
    acSetStatus: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    dxBarLargeButton18: TdxBarLargeButton;
    dxBarLargeButton19: TdxBarLargeButton;
    dxBarManager1Bar1: TdxBar;
    procedure acRefreshExecute(Sender: TObject);
    procedure acCloseExecute(Sender: TObject);
    procedure acSelectDateExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure deDateBegChange(Sender: TObject);
    procedure acExpExcelExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ViewDocZDblClick(Sender: TObject);
    procedure acAddDocExecute(Sender: TObject);
    procedure acEditDocExecute(Sender: TObject);
    procedure acViewDocExecute(Sender: TObject);
    procedure acDelDocExecute(Sender: TObject);
    procedure acSetStatusExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }

    procedure Init;
  end;

var
  fmDocZ_HD: TfmDocZ_HD;

procedure ShowFormDocZHD;

implementation

{$R *.dfm}

uses Main, dbs, Un2, DocZ_SP;

procedure ShowFormDocZHD;
begin
  if Assigned(fmDocZ_HD)=False then //����� �� ����������, � ����� �������
    fmDocZ_HD:=TfmDocZ_HD.Create(Application);
  if fmDocZ_HD.WindowState=wsMinimized then //���� ���� ��������, �� ������������� ���
    fmDocZ_HD.WindowState:=wsNormal;

  fmDocZ_HD.deDateBeg.Date:=Date-7;
  fmDocZ_HD.deDateEnd.Date:=Date;
  fmDocZ_HD.Init;
  fmDocZ_HD.Show;
end;

procedure TfmDocZ_HD.Init;
var
  FocusedRow, TopRow: Integer;
  flag: boolean;
begin
  with dmS   do
  begin
    try
      //<--Refresh � ��������������� ������� ������� � �������
      flag:=quDocZHD.Active;  //���������� ���� �� ������� �������, ��� �������������� ������� �������
      TopRow := ViewDocZ.Controller.TopRowIndex;
      FocusedRow := ViewDocZ.DataController.FocusedRowIndex;
      //-->
      ViewDocZ.BeginUpdate;

      quDocZHD.Active:=False;
      quDocZHD.ParamByName('IDATEB').AsInteger:=Trunc(deDateBeg.Date);
      quDocZHD.ParamByName('IDATEE').AsInteger:=Trunc(deDateEnd.Date);
      quDocZHD.Active:=True;
      quDocZHD.First;
      //-->
    finally
      ViewDocZ.EndUpdate;
      //<--��������������� �������
      if flag then begin
        ViewDocZ.DataController.FocusedRowIndex := FocusedRow;
        ViewDocZ.Controller.TopRowIndex := TopRow;
      end;
      //-->
    end;
  end;

//  FSRAR_ID:=CommonSet.FSRAR_ID;
end;


procedure TfmDocZ_HD.ViewDocZDblClick(Sender: TObject);
begin
  if quDocZHD.RecordCount>0 then
  begin
    if quDocZHDISTATUS.AsInteger=0 then ShowSpecZ(quDocZHDID.AsInteger,1,0)
    else ShowSpecZ(quDocZHDID.AsInteger,0,0);
  end;
end;

procedure TfmDocZ_HD.acAddDocExecute(Sender: TObject);
var IDH:Integer;
begin
  //�������� ��������
  IDH:=fGetId(2);
  ShowSpecZ(IDH,1,1);
end;

procedure TfmDocZ_HD.acCloseExecute(Sender: TObject);
begin
  //�������
  Close;
//  fmMain.Close;
end;

procedure TfmDocZ_HD.acDelDocExecute(Sender: TObject);
begin
  if quDocZHD.RecordCount>0 then
  begin
    if MessageDlg('������� �������� ������ '+quDocZHDCLINAME.AsString+' �� '+dstime(quDocZHDTIMEPRO.AsDateTime)+'?',mtConfirmation, [mbYes, mbNo], 0, mbYes) = mrYes then
    begin
      ClearMessageLogLocal;
      ShowMessageLogLocal('�����.. ���� �������� ���������');

      if quDocZHDISTATUS.AsInteger=0 then
      begin
        with dmS do
        begin
          try
            quS.Active:=False;
            quS.SQL.Clear;
            quS.SQL.Add('');
            quS.SQL.Add('DELETE FROM [dbo].[DOCZ_HD]');
            quS.SQL.Add('WHERE  ID = '+its(quDocZHDID.AsInteger));
            quS.ExecSQL;

            Init;
          except
          end;
        end;
      end else ShowMessageLogLocal('  �������� ������ ���������. �������� ����������.');

      ShowMessageLogLocal('������� ��������.');
    end;
  end;
end;

procedure TfmDocZ_HD.acEditDocExecute(Sender: TObject);
begin
  if quDocZHD.RecordCount>0 then
  begin
    if quDocZHDISTATUS.AsInteger=0 then ShowSpecZ(quDocZHDID.AsInteger,1,0)
    else
    begin
      ClearMessageLogLocal;
      ShowMessageLogLocal('  �������� ������ ���������. �������������� ����������.');
    end;
  end;
end;

procedure TfmDocZ_HD.acExpExcelExecute(Sender: TObject);
begin
  ExportGridToFile(formatdatetime('ddmmyyyy',deDateBeg.Date)+'_'+formatdatetime('ddmmyyyy',deDateEnd.Date)+'_'+formatdatetime('ddmmyyyy_hh_nn_ss',now)+'.xls', GridDocZ);
end;

procedure TfmDocZ_HD.acRefreshExecute(Sender: TObject);
begin
  //������
  Init;
end;

procedure TfmDocZ_HD.acSelectDateExecute(Sender: TObject);
begin
  //��������
  Init;
end;

procedure TfmDocZ_HD.acSetStatusExecute(Sender: TObject);
begin
//
end;

procedure TfmDocZ_HD.acViewDocExecute(Sender: TObject);
begin
  if quDocZHD.RecordCount>0 then
  begin
    ShowSpecZ(quDocZHDID.AsInteger,0,0);
  end;
end;

procedure TfmDocZ_HD.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
  fmDocZ_HD:=Nil;
  ViewDocZ.StoreToIniFile(GetGridIniFile,False,[gsoUseSummary,gsoUseFilter]);
end;

procedure TfmDocZ_HD.FormCreate(Sender: TObject);
begin
  ClearMessageLogLocal;
  ViewDocZ.RestoreFromIniFile(GetGridIniFile);
end;

procedure TfmDocZ_HD.deDateBegChange(Sender: TObject);
begin
  if dmS.FDConnection.Connected then Init;
end;


end.
