object fmDocZ_HD: TfmDocZ_HD
  Left = 0
  Top = 0
  Caption = #1047#1072#1082#1072#1079#1099' '#1085#1072' '#1087#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1086
  ClientHeight = 547
  ClientWidth = 1517
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1517
    Height = 127
    ApplicationButton.Visible = False
    BarManager = dxBarManager1
    Style = rs2010
    ColorSchemeAccent = rcsaOrange
    ColorSchemeName = 'Blue'
    SupportNonClientDrawing = True
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1047#1072#1082#1072#1079#1099' '#1085#1072' '#1087#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1086
      Groups = <
        item
          ToolbarName = 'bmApplyDate'
        end
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'bmClose'
        end>
      Index = 0
    end
  end
  object GridDocZ: TcxGrid
    Left = 0
    Top = 127
    Width = 1517
    Height = 420
    Align = alClient
    TabOrder = 5
    LevelTabs.Style = 8
    LookAndFeel.Kind = lfOffice11
    LookAndFeel.NativeStyle = False
    object ViewDocZ: TcxGridDBTableView
      PopupMenu = pmDocVn
      OnDblClick = ViewDocZDblClick
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.First.Visible = False
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.InfoPanel.Visible = True
      Navigator.Visible = True
      DataController.DataSource = dsquDocZHD
      DataController.Summary.DefaultGroupSummaryItems = <
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'SUMDOSTAV'
          Column = ViewDocZSUMDOSTAV
        end
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'SUMITOG'
          Column = ViewDocZSUMITOG
        end
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'SUMR'
          Column = ViewDocZSUMR
        end>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SUMDOSTAV'
          Column = ViewDocZSUMDOSTAV
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SUMITOG'
          Column = ViewDocZSUMITOG
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SUMR'
          Column = ViewDocZSUMR
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.MultiSelect = True
      OptionsView.Footer = True
      object ViewDocZIDATE: TcxGridDBColumn
        Caption = #1044#1072#1090#1072' '#1080#1079#1075#1086#1090#1086#1074#1083#1077#1085#1080#1103
        DataBinding.FieldName = 'IDATE'
        PropertiesClassName = 'TcxDateEditProperties'
      end
      object ViewDocZID: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1079#1072#1082#1072#1079#1072
        DataBinding.FieldName = 'ID'
        Width = 79
      end
      object ViewDocZTIMEPRO: TcxGridDBColumn
        Caption = #1042#1088#1077#1084#1103' '#1080#1079#1075#1086#1090#1086#1074#1083#1077#1085#1080#1103
        DataBinding.FieldName = 'TIMEPRO'
        PropertiesClassName = 'TcxTimeEditProperties'
        Width = 73
      end
      object ViewDocZTIMEDOST: TcxGridDBColumn
        Caption = #1044#1072#1090#1072', '#1074#1088#1077#1084#1103' '#1087#1086#1089#1090#1072#1074#1082#1080
        DataBinding.FieldName = 'TIMEDOST'
        Width = 134
      end
      object ViewDocZIDCLI: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1079#1072#1082#1072#1079#1095#1080#1082#1072
        DataBinding.FieldName = 'IDCLI'
        Visible = False
      end
      object ViewDocZCLINAME: TcxGridDBColumn
        Caption = #1047#1072#1082#1072#1079#1095#1080#1082
        DataBinding.FieldName = 'CLINAME'
        Width = 169
      end
      object ViewDocZSUMDOSTAV: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1076#1086#1089#1090#1072#1074#1082#1080
        DataBinding.FieldName = 'SUMDOSTAV'
        Width = 97
      end
      object ViewDocZSUMR: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1079#1072#1082#1072#1079#1072
        DataBinding.FieldName = 'SUMR'
        Width = 89
      end
      object ViewDocZSUMITOG: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1074#1089#1077#1075#1086
        DataBinding.FieldName = 'SUMITOG'
        Styles.Content = dmS.stBold
        Width = 100
      end
      object ViewDocZISTATUS: TcxGridDBColumn
        Caption = #1057#1090#1072#1090#1091#1089
        DataBinding.FieldName = 'ISTATUS'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Images = dmS.SmallImage
        Properties.Items = <
          item
            Description = #1053#1086#1074#1099#1081' '#1079#1072#1082#1072#1079
            ImageIndex = 79
            Value = 1
          end
          item
            Description = #1047#1072#1082#1072#1079' '#1087#1088#1080#1085#1103#1090
            ImageIndex = 81
            Value = 2
          end
          item
            Description = #1086#1090#1087#1088#1072#1074#1083#1077#1085' '#1074' '#1086#1090#1076#1077#1083' '#1051#1086#1075#1080#1089#1090#1080#1082#1080
            ImageIndex = 61
            Value = 3
          end
          item
            Description = #1054#1090#1087#1088#1072#1074#1083#1077#1085' '#1074' '#1094#1077#1093
            ImageIndex = 38
            Value = 4
          end
          item
            Description = #1048#1079#1075#1086#1090#1086#1074#1083#1077#1085
            ImageIndex = 1
            Value = 5
          end
          item
            Description = #1047#1072#1082#1072#1079' '#1086#1090#1075#1088#1091#1078#1077#1085
            ImageIndex = 72
            Value = 6
          end>
      end
      object ViewDocZPHONE: TcxGridDBColumn
        Caption = #1050#1086#1085#1090#1072#1082#1090#1085#1099#1081' '#1090#1077#1083#1077#1092#1086#1085
        DataBinding.FieldName = 'PHONE'
        Width = 120
      end
      object ViewDocZADDRES: TcxGridDBColumn
        Caption = #1040#1076#1088#1077#1089' '#1087#1086#1089#1090#1072#1074#1082#1080
        DataBinding.FieldName = 'ADDRES'
        Width = 188
      end
      object ViewDocZNAMEINFO: TcxGridDBColumn
        Caption = #1050#1072#1082' '#1091#1079#1085#1072#1083#1080' '
        DataBinding.FieldName = 'NAMEINFO'
        Width = 170
      end
      object ViewDocZSPOVOD: TcxGridDBColumn
        Caption = #1055#1086#1074#1086#1076' '#1076#1083#1103' '#1079#1072#1082#1072#1079#1072' '
        DataBinding.FieldName = 'SPOVOD'
        Width = 178
      end
      object ViewDocZCOMMENT: TcxGridDBColumn
        Caption = #1050#1086#1084#1084#1077#1085#1090#1072#1088#1080#1081
        DataBinding.FieldName = 'COMMENT'
        Width = 218
      end
    end
    object LevelDocZ: TcxGridLevel
      Caption = #1053#1077#1086#1073#1088#1072#1073#1086#1090#1072#1085#1085#1099#1077' '#1058#1058#1053
      GridView = ViewDocZ
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      #1044#1077#1081#1089#1090#1074#1080#1103)
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmS.SmallImage
    ImageOptions.LargeImages = dmS.LargeImage
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 708
    Top = 204
    DockControlHeights = (
      0
      0
      0
      0)
    object bmApplyDate: TdxBar
      Caption = #1044#1077#1081#1089#1090#1074#1080#1103
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 778
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          UserDefine = [udWidth]
          UserWidth = 75
          Visible = True
          ItemName = 'deDateBeg'
        end
        item
          UserDefine = [udWidth]
          UserWidth = 74
          Visible = True
          ItemName = 'deDateEnd'
        end
        item
          Visible = True
          ItemName = 'btnDone'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton17'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton18'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object bmClose: TdxBar
      Caption = #1047#1072#1082#1088#1099#1090#1100
      CaptionButtons = <>
      DockedLeft = 592
      DockedTop = 0
      FloatLeft = 778
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'btnClose'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar1: TdxBar
      Caption = #1044#1086#1087'.'#1092#1091#1085#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 503
      DockedTop = 0
      FloatLeft = 1551
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton19'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object deDateBeg: TdxBarDateCombo
      Caption = 'C  '
      Category = 0
      Hint = 'C  '
      Visible = ivAlways
      OnChange = deDateBegChange
      ImageIndex = 25
      ShowDayText = False
    end
    object deDateEnd: TdxBarDateCombo
      Caption = #1087#1086
      Category = 0
      Hint = #1087#1086
      Visible = ivAlways
      OnChange = deDateBegChange
      ImageIndex = 25
      ShowDayText = False
    end
    object btnDone: TdxBarLargeButton
      Action = acRefresh
      Category = 0
    end
    object btnClose: TdxBarLargeButton
      Action = acClose
      Category = 0
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = acAddDoc
      Category = 0
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = acEditDoc
      Category = 0
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Action = acViewDoc
      Category = 0
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Action = acDelDoc
      Category = 0
    end
    object dxBarLargeButton19: TdxBarLargeButton
      Action = acSetStatus
      Category = 0
    end
    object dxBarGroup1: TdxBarGroup
      Items = ()
    end
  end
  object ActionList1: TActionList
    Images = dmS.SmallImage
    Left = 560
    Top = 228
    object acRefresh: TAction
      Category = #1060#1086#1088#1084#1072
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100' (F5)'
      Hint = #1054#1073#1085#1086#1074#1080#1090#1100' (F5)'
      ImageIndex = 5
      ShortCut = 116
      OnExecute = acRefreshExecute
    end
    object acClose: TAction
      Category = #1060#1086#1088#1084#1072
      Caption = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 100
      OnExecute = acCloseExecute
    end
    object acSelectDate: TAction
      Category = #1060#1086#1088#1084#1072
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      Hint = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      ImageIndex = 1
      OnExecute = acSelectDateExecute
    end
    object acExpExcel: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1069#1082#1089#1087#1086#1088#1090' '#1074' Excel'
      ImageIndex = 76
      OnExecute = acExpExcelExecute
    end
    object acAddDoc: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ImageIndex = 75
      OnExecute = acAddDocExecute
    end
    object acEditDoc: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100
      ImageIndex = 73
      OnExecute = acEditDocExecute
    end
    object acViewDoc: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1055#1088#1086#1089#1084#1086#1090#1088
      ImageIndex = 39
      OnExecute = acViewDocExecute
    end
    object acDelDoc: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ImageIndex = 74
      OnExecute = acDelDocExecute
    end
    object acSetStatus: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1089#1090#1072#1090#1091#1089
      ImageIndex = 81
      OnExecute = acSetStatusExecute
    end
  end
  object pmDocVn: TPopupMenu
    Images = dmS.SmallImage
    Left = 488
    Top = 304
    object N1: TMenuItem
      Caption = #1055#1077#1088#1077#1076#1072#1090#1100' '#1074' '#1045#1043#1040#1048#1057' '#1074#1099#1076'.'
      ImageIndex = 49
    end
    object N3: TMenuItem
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1076#1086#1082#1091#1084#1077#1085#1090
      ImageIndex = 100
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Excel1: TMenuItem
      Action = acExpExcel
    end
  end
  object quDocZHD: TFDQuery
    Connection = dmS.FDConnection
    Transaction = dmS.FDTrans
    UpdateTransaction = dmS.FDTransUpdate
    SQL.Strings = (
      'declare @IDATEB int = :IDATEB'
      'declare @IDATEE int = :IDATEE'
      ''
      'SELECT hd.[IDATE]'
      '      ,hd.[ID]'
      '      ,hd.[TIMEPRO]'
      '      ,hd.[TIMEDOST]'
      '      ,hd.[IDCLI]'
      '      ,hd.[PHONE]'
      '      ,hd.[ADDRES]'
      '      ,hd.[IDFROM]'
      '      ,hd.[POVOD]'
      '      ,hd.[ISTATUS]'
      '      ,hd.[COMMENT]'
      '      ,hd.[SUMDOSTAV]'
      #9'  ,cli.[NAME] as CLINAME'
      #9'  ,si.NAME as NAMEINFO'
      #9'  ,pov.NAME as SPOVOD'
      
        #9'  ,isnull((Select [RSUM] from [dbo].[DOCZ_SP] where IDH=hd.[ID]' +
        '),0) as SUMR'
      
        #9'  ,isnull((Select [RSUM] from [dbo].[DOCZ_SP] where IDH=hd.[ID]' +
        '),0)+hd.[SUMDOSTAV] as SUMITOG'
      '  FROM [dbo].[DOCZ_HD] hd'
      '  left join [dbo].[ORDERSCLI] cli on cli.[IDCLI]=hd.[IDCLI]'
      '  left join [dbo].[SOURCEINFO] si on si.ID=hd.[IDFROM]'
      '  left join [dbo].[POVOD] pov on pov.ID=hd.[POVOD]'
      '  where hd.IDATE>=@IDATEB'
      '  and hd.IDATE<=@IDATEE')
    Left = 72
    Top = 208
    ParamData = <
      item
        Name = 'IDATEB'
        DataType = ftInteger
        ParamType = ptInput
        Value = 42000
      end
      item
        Name = 'IDATEE'
        DataType = ftInteger
        ParamType = ptInput
        Value = 43000
      end>
    object quDocZHDIDATE: TIntegerField
      FieldName = 'IDATE'
      Origin = 'IDATE'
      Required = True
    end
    object quDocZHDID: TLargeintField
      FieldName = 'ID'
      Origin = 'ID'
      Required = True
    end
    object quDocZHDTIMEPRO: TSQLTimeStampField
      FieldName = 'TIMEPRO'
      Origin = 'TIMEPRO'
    end
    object quDocZHDTIMEDOST: TSQLTimeStampField
      FieldName = 'TIMEDOST'
      Origin = 'TIMEDOST'
    end
    object quDocZHDIDCLI: TIntegerField
      FieldName = 'IDCLI'
      Origin = 'IDCLI'
    end
    object quDocZHDPHONE: TStringField
      FieldName = 'PHONE'
      Origin = 'PHONE'
      Size = 100
    end
    object quDocZHDADDRES: TStringField
      FieldName = 'ADDRES'
      Origin = 'ADDRES'
      Size = 200
    end
    object quDocZHDIDFROM: TIntegerField
      FieldName = 'IDFROM'
      Origin = 'IDFROM'
    end
    object quDocZHDPOVOD: TIntegerField
      FieldName = 'POVOD'
      Origin = 'POVOD'
    end
    object quDocZHDISTATUS: TIntegerField
      FieldName = 'ISTATUS'
      Origin = 'ISTATUS'
    end
    object quDocZHDCOMMENT: TStringField
      FieldName = 'COMMENT'
      Origin = 'COMMENT'
      Size = 200
    end
    object quDocZHDSUMDOSTAV: TBCDField
      FieldName = 'SUMDOSTAV'
      Origin = 'SUMDOSTAV'
      DisplayFormat = '0.00'
      Precision = 18
      Size = 2
    end
    object quDocZHDCLINAME: TStringField
      FieldName = 'CLINAME'
      Origin = 'CLINAME'
      Size = 200
    end
    object quDocZHDNAMEINFO: TStringField
      FieldName = 'NAMEINFO'
      Origin = 'NAMEINFO'
      Size = 300
    end
    object quDocZHDSPOVOD: TStringField
      FieldName = 'SPOVOD'
      Origin = 'SPOVOD'
      Size = 300
    end
    object quDocZHDSUMR: TSingleField
      FieldName = 'SUMR'
      Origin = 'SUMR'
      ReadOnly = True
      Required = True
      DisplayFormat = '0.00'
    end
    object quDocZHDSUMITOG: TSingleField
      FieldName = 'SUMITOG'
      Origin = 'SUMITOG'
      ReadOnly = True
      DisplayFormat = '0.00'
    end
  end
  object dsquDocZHD: TDataSource
    DataSet = quDocZHD
    Left = 72
    Top = 256
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 248
    Top = 224
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
    end
  end
end
