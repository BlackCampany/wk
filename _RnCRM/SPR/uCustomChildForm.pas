unit uCustomChildForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  dxRibbonForm, dxBar, DB, dxRibbon, cxGrid, cxGridChartView, cxGridDBChartView, cxGridTableView, cxGridDBTableView, cxGridDBBandedTableView,
  cxGridCustomView, System.Generics.Collections, StdCtrls, Buttons, ExtCtrls, dxStatusBar, dxRibbonStatusBar, dxRibbonSkins, dxGDIPlusClasses,
  JvExExtCtrls, JvImage, cxPC, dxDockPanel, dxDockControl, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer,
  cxEdit, cxImage, Vcl.Menus, cxTextEdit, cxMemo, cxButtons, JvComponentBase, JvFormPlacement, cxSplitter,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TCustomChildForm = class(TdxRibbonForm)
    imgFON: TcxImage;
    imgTEXT: TcxImage;
    SplitterBottom: TcxSplitter;
    JvFormStorageCustom: TJvFormStorage;
    PanelLog: TPanel;
    PanelCloseLog: TPanel;
    btnCloseLog: TcxButton;
    MemoLogLocal: TcxMemo;
    TimerEditLock: TTimer;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormResize(Sender: TObject);
    procedure btnCloseLogClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure TimerEditLockTimer(Sender: TObject);
  private
    { Private declarations }
    FPreviousWindowState: TWindowState;
    FActiveGrid: TcxGrid;
    //FDefaultViews : TObjectDictionary<string, TStream>;
    FIDH_Number: Integer; //������ ����� ��������� ���������, ��� ���������� ��������� �� ����� ������ ����� �������� ����
    FNewFormStyle: TFormStyle;
    procedure SetActiveGrid(Value: TcxGrid);
    function GetActiveGridView: TcxGridTableView;
    procedure WMSysCommand(var msg: TWMSysCommand); message WM_SYSCOMMAND;  protected
    procedure ShowMessageLogLocal(text:string);
    procedure SearchActiveGrid;                      //���� � ������������� ActiveGrid
    procedure ClearMessageLogLocal;
    function GetCaption: String;
    procedure SetCaption(const Value: String);
  protected
    { Protected declarations }
    procedure CreateParams(var Params: TCreateParams); override;
  published
    { Published declarations }
    property ActiveGrid: TcxGrid read FActiveGrid write SetActiveGrid;
    property ActiveGridView: TcxGridTableView read GetActiveGridView;
    property SetWindowIDH: Integer write FIDH_Number;
    property Caption: String read GetCaption write SetCaption;
  public
    { Public declarations }
    FIdHead: Integer;
    FIdShop: Integer;
    FIdType: Integer;
    constructor Create(AOwner: TComponent; AModal: TFormStyle = fsMDIChild); reintroduce;
    destructor Destroy; override;
    procedure StoreTableView(aView: TcxGridTableView);   //���������� ���� �������
    procedure RestoreTableView(aView: TcxGridTableView); //�������������� ���� �������
    procedure PleaseWait(ON_OFF: Boolean); //����������, ���������
  end;

var
  CustomChildForm: TCustomChildForm;

function FindWindowByIDH(FormClass: TClass; IDH: Integer): TCustomForm;

implementation


{$R *.dfm}

uses Main, Un2;


{ TCustomChildForm }

function FindWindowByIDH(FormClass: TClass; IDH: Integer): TCustomForm;
var i: integer;
    FormType: string;
begin
  Result:=nil;
  FormType:=FormClass.ClassName;
  for i := fmMain.MDIChildCount-1 downto 0 do begin
    if fmMain.MDIChildren[i].ClassName=FormType then begin
      if TCustomChildForm(fmMain.MDIChildren[i]).FIDH_Number=IDH then begin
        Result:=fmMain.MDIChildren[i];
        Exit;
      end;
    end;
  end;
end;


constructor TCustomChildForm.Create(AOwner: TComponent; AModal: TFormStyle);
var  tempdxRibbon: TdxRibbon;
begin
  FNewFormStyle := AModal;
  //FDefaultViews := TObjectDictionary<string, TStream>.Create([doOwnsValues]);
  inherited Create(AOwner);


  //<--��������������� ��������� �����
  JvFormStorageCustom.Active:=False;  //����� ������ ���������� ����������� � ���������������
  JvFormStorageCustom.AppStoragePath:=Self.Name;
  JvFormStorageCustom.RestoreFormPlacement;
  if Top<0 then
    Top:=0;
  //-->

  //<--���� � ������������� ActiveGrid ��� ������ �����
  SearchActiveGrid;
  //-->

  //<--���� ��������� TdxRibbon �� ����� � �������� � ���� ��������� �����
  tempdxRibbon:=TdxRibbon(self.RibbonControl);
  if Assigned(tempdxRibbon) then begin
    tempdxRibbon.ShowTabHeaders:=False;
    tempdxRibbon.SupportNonClientDrawing:=True;
    tempdxRibbon.Style:=fmMain.dxRibbon.Style;
    tempdxRibbon.ColorSchemeName:=fmMain.dxRibbon.ColorSchemeName;
  end;
  //-->
end;

destructor TCustomChildForm.Destroy;
begin
  //FreeAndNil(FDefaultViews);
  //<--��������� ��������� ����� ������ ���� ��� �� ��������������
//  if not(fsModal in FormState) then
//    Height:=Height+22;  //�����-�� ���, ��� ���������� � �������������� �����, � ������ ����������� �� 22 �������, ��� � ������������ ���
  if WindowState<>wsMinimized then
    JvFormStorageCustom.SaveFormPlacement;
  //-->
  inherited Destroy;
end;

procedure TCustomChildForm.PleaseWait(ON_OFF: Boolean);
begin
  //<--�������� �� �� ���� � �������� "����������, ���������"
  if ON_OFF then
  begin
    imgFON.Visible:=True;
    imgTEXT.Visible:=True;
    imgFON.Height:=Height+4;
    imgFON.Width:=Width+4;
    imgFON.Left:=-2;
    imgFON.Top:=-2;
    imgTEXT.Height:=Height+4;
    imgTEXT.Width:=Width+4;
    imgTEXT.Left:=-2;
    imgTEXT.Top:=-2;
    imgFON.BringToFront;
    imgTEXT.BringToFront;
  end else begin
    imgFON.Visible:=False;
    imgTEXT.Visible:=False;
  end;
  //-->
end;

procedure TCustomChildForm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams( Params );

  case FNewFormStyle of
    fsNormal:    begin
                   Visible := false;
                   FormStyle := FNewFormStyle;
                   //���� ����� ���������
                   if fsModal in FormState then begin
                     BorderIcons:=BorderIcons-[biMinimize,biMaximize];
                     //BorderStyle:=bsSingle;
                   end else begin
                     BorderIcons:=BorderIcons+[biMinimize,biMaximize];
                     BorderStyle:=bsSizeable;
                   end;
                 end;
    fsStayOnTop: begin
                   Visible := false;
                   FormStyle := FNewFormStyle;
                   BorderIcons:=BorderIcons-[biMinimize,biMaximize];
                   //BorderStyle:=bsSizeToolWin;
                 end;
  end;

  //������ ����� ������ �������� ���� ��������� ��� ���� ��������� ������ � ������ ����� Windows
  with Params do
  begin
//    ExStyle := ExStyle or WS_EX_APPWINDOW;
//    WndParent := GetDesktopwindow;  //��� ������������ �������� ���� ����� �� ������������� ��������
  end;
end;

procedure TCustomChildForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TCustomChildForm.btnCloseLogClick(Sender: TObject);
begin
  PanelLog.Visible:=false;
  SplitterBottom.Visible:=false;
  MemoLogLocal.Clear;
end;

procedure TCustomChildForm.FormResize(Sender: TObject);
var i: Integer;
    dxRibbonTab: TdxRibbonTab;
begin
  //�������� �������� � ����� ��� ������������ ����
  if FPreviousWindowState=WindowState then Exit;

  FPreviousWindowState:=WindowState;
  for i := ComponentCount-1 downto 0 do
  begin
    if Components[i] is TdxRibbonTab then begin
      dxRibbonTab:=Components[i] as TdxRibbonTab;
      if WindowState= TWindowState.wsMaximized then begin
        if fmMain.dxRibbon.Contexts.Count>0 then
          dxRibbonTab.Context:=fmMain.dxRibbon.Contexts.Items[0];  //�������� �������� � ������� �����
      end else
        dxRibbonTab.Context:=nil;
    end;
  end;
end;

procedure TCustomChildForm.FormShow(Sender: TObject);
var LastBuildVersion: string;
begin
end;

//���������� ���� �������
procedure TCustomChildForm.StoreTableView(aView: TcxGridTableView);
begin
  try
    aView.StoreToIniFile(GetGridIniFile,False,[gsoUseSummary,gsoUseFilter]);
  except
//    on E: Exception do
//    begin
//      if E is EInOutError then
//        raise Exception.Create('EInOutError: ('+IntToStr((E as EInOutError).ErrorCode)+') '+E.Message)
//      else
//        raise;
//    end;
  end;
end;

procedure TCustomChildForm.TimerEditLockTimer(Sender: TObject);
begin
end;

//�������������� ���� �������
procedure TCustomChildForm.RestoreTableView(aView: TcxGridTableView);
begin
  //��������������� ���������������� ��� �� ��� �����
  aView.RestoreFromIniFile(GetGridIniFile);
end;

procedure TCustomChildForm.SearchActiveGrid;
var i: Integer;
begin
  for i := 0 to ComponentCount - 1 do begin
    if (Components[i] is TcxGrid) then begin
      if not Assigned(ActiveGrid) then begin  //���� ������������� ��� �� �������, �� �������� �
        if (TcxGrid(Components[i]).FocusedView is TcxGridTableView) then begin
          ActiveGrid:=TcxGrid(Components[i]);
          Exit;
        end;
      end;
    end;
  end;
end;

procedure TCustomChildForm.WMSysCommand(var msg: TWmSysCommand);
var tempdxBarManager: TdxBarManager;
  function SearchTdxBarManager:TdxBarManager;
  var i: Integer;
  begin
    Result:=nil;
    for i := 0 to ComponentCount - 1 do begin
      //ShowMessageLogLocal('   '+Components[i].ClassName);
      if (Components[i] is TdxBarManager) then begin
        //ShowMessageLogLocal(Components[i].ClassName);
        Result:=Components[i] as TdxBarManager;
      end;
    end;
    //ShowMessageLogLocal('---------------------------------');
  end;
begin
  try
    //���� ����� �� ���������, �� ������������ �������
    if not(fsModal in FormState) then begin
      if msg.CmdType = SC_MINIMIZE then
      begin
        // here is the code that should be executed
        //ShowMessageLogLocal('Minimize');
        tempdxBarManager:=SearchTdxBarManager;
        if Assigned(tempdxBarManager) then
          fmMain.dxBarManagerMerge(fmMain.dxBarManager, tempdxBarManager, False); //Unmerge
        //fmMainCard.Focused:=True;
        Exit ;
      end;
      if msg.CmdType = SC_MAXIMIZE then
      begin
        // here is the code that should be executed
        //ShowMessageLogLocal('Maximize');
        tempdxBarManager:=SearchTdxBarManager;
        if Assigned(tempdxBarManager) then
          fmMain.dxBarManagerMerge(fmMain.dxBarManager, tempdxBarManager, True);  //Merge
        Exit ;
      end;
    end;
  finally
    inherited;
  end;
end;

procedure TCustomChildForm.ShowMessageLogLocal(text:string);
const constMaxCountLinesInMemoLog = 1000;        //������������ ���-�� ����� � ������� MemoLog
var i, countToDel: integer;
begin
  MemoLogLocal.Lines.BeginUpdate;
  try
    //<--��������� ����� ����� � Memo ���� �� ������ constMaxCountLinesInMemoLog
    if MemoLogLocal.Lines.Count>constMaxCountLinesInMemoLog then begin
      countToDel:=MemoLogLocal.Lines.Count-constMaxCountLinesInMemoLog;
      //������� ������ ���-�� ������� �����
      for i := 0 to countToDel do
        MemoLogLocal.Lines.Delete(0);
    end;
    //-->

    //<--���� ������ ������, �� ���������� �
    if PanelLog.Visible=false then begin
      SplitterBottom.Top:=5000;  //����� ��� � ����� ����
      SplitterBottom.Visible:=true;
      PanelLog.Top:=5111;        //����� ��� � ����� ���� � ���� SplitterBottom
      PanelLog.Visible:=true;
      if PanelLog.Height=0 then PanelLog.Height:=100;
      PanelLog.Refresh;
      Application.ProcessMessages;
    end;
    //-->
  finally
    MemoLogLocal.Lines.EndUpdate;
  end;
  //��������� ��������� � ����
  MemoLogLocal.Lines.Append(TimeToStr(now)+'> '+text);
end;

procedure TCustomChildForm.ClearMessageLogLocal;
begin
  MemoLogLocal.Clear;
end;

function TCustomChildForm.GetCaption: String;
begin
  Result := inherited Caption;
end;

procedure TCustomChildForm.SetActiveGrid(Value: TcxGrid);
begin
  FActiveGrid:=Value;
end;

function TCustomChildForm.GetActiveGridView: TcxGridTableView;
begin
  Result:=nil;
  if Assigned(FActiveGrid) then
    if Assigned(FActiveGrid.ActiveView) then begin
      if (FActiveGrid.ActiveView is TcxGridTableView) then       //�������
        Result:=TcxGridTableView(FActiveGrid.ActiveView);
      if (FActiveGrid.ActiveView is TcxGridDBChartView) then     //������
        Result:=nil;
    end;
end;

procedure TCustomChildForm.SetCaption(const Value: String);
var tempdxRibbon: TdxRibbon;
begin
  if value <> Caption then begin
    //<--���� ��������� TdxRibbon �� �����
    tempdxRibbon:=TdxRibbon(self.RibbonControl);
    if Assigned(tempdxRibbon) then begin
      //������ ��������� ������ ������� �������
      if tempdxRibbon.TabCount>0 then
        tempdxRibbon.Tabs.Items[0].Caption := Value;
      //������ ��������� ������������ ������� ������� �� ������� �����
      if fmMain.dxRibbon.TabCount>constMainRibbonTabsCount then
        //��������� ����� �� ������������ ������� ������� ����� ���� ����, �.�. ����� ������ ���
        if fmMain.dxRibbon.Tabs[constMainRibbonTabsCount].Caption=Caption then
          fmMain.dxRibbon.Tabs[constMainRibbonTabsCount].Caption := Value;
    end;
    //-->
    inherited Caption := Value;
    Invalidate;
  end;
end;

end.
