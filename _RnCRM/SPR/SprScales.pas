unit SprScales;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  uCustomChildForm,
  Vcl.Controls, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus,
  cxControls, cxContainer, cxEdit, System.Actions, Vcl.ActnList, Data.DB,
  Data.Win.ADODB, cxTextEdit, cxMemo, Vcl.StdCtrls, Vcl.ExtCtrls, dxRibbonSkins,
  dxRibbonCustomizationForm, dxBar, dxBarExtItems, cxClasses, dxRibbon,
  cxCalendar, cxBarEditItem, dxStatusBar, dxRibbonStatusBar, cxDBLookupComboBox,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator,
  cxDBData, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridCustomView, cxGrid,vcl.Forms, cxLabel, cxCheckBox,
  JvComponentBase, JvFormPlacement, Dialogs, cxImageComboBox;

type
  TfmSprScales = class(TCustomChildForm)
    ActionList1: TActionList;
    acRefresh: TAction;
    dxRibbon1: TdxRibbon;
    rtScales: TdxRibbonTab;
    dxBarManager1: TdxBarManager;
    bmAction: TdxBar;
    bmClose: TdxBar;
    bUpdate: TdxBarLargeButton;
    bClose: TdxBarLargeButton;
    acClose: TAction;
    sbSprScale: TdxRibbonStatusBar;
    GrSc: TcxGrid;
    ViewSc: TcxGridDBTableView;
    ViewScId: TcxGridDBColumn;
    ViewScNumber: TcxGridDBColumn;
    ViewScModel: TcxGridDBColumn;
    ViewScName: TcxGridDBColumn;
    ViewScIp1: TcxGridDBColumn;
    ViewScIp2: TcxGridDBColumn;
    ViewScIp3: TcxGridDBColumn;
    ViewScIp4: TcxGridDBColumn;
    ViewScIp5: TcxGridDBColumn;
    LevelSc: TcxGridLevel;
    ViewScIp6: TcxGridDBColumn;
    ViewScIp7: TcxGridDBColumn;
    JvFormStorage: TJvFormStorage;
    btnAdd: TdxBarLargeButton;
    btnDel: TdxBarLargeButton;
    btnEdit: TdxBarLargeButton;
    acAdd: TAction;
    acEdit: TAction;
    acDel: TAction;
    ViewScISTATUS: TcxGridDBColumn;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acCloseExecute(Sender: TObject);
    procedure acRefreshExecute(Sender: TObject);
    procedure acAddExecute(Sender: TObject);
    procedure acEditExecute(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ViewScCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Init;
  end;
  procedure ShowFormSprScales;
var
  fmSprScales: TfmSprScales;

implementation

{$R *.dfm}

uses dmMain, dmStyleCtrl, dmDocs, Main, AddScales,Shared_Types,UnitFunction;

//��������� �������� �����
procedure ShowFormSprScales;
begin
  if Assigned(fmSprScales)=False then begin //����� �� ����������, � ����� �������
    fmSprScales:=TfmSprScales.Create(Application);
  end;
  fmSprScales.Init;
  fmSprScales.Show;
end;

procedure TfmSprScales.acAddExecute(Sender: TObject);
begin
  ShowFormAddScales(sAdd);
end;

procedure TfmSprScales.acCloseExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmSprScales.acDelExecute(Sender: TObject);
var iErrors:integer;
begin
  // ��������
  with dmD do
  begin
    if MessageDlg('�� ������������� ������ ������� ���� �'+quSprScaleID.AsString+',"'+quSprScaleName.AsString+'",'+quSprScaleSCALETYPE.AsString+' ?',mtConfirmation,[mbYes,mbNo],0) = mrYes then
    begin
      quSprScale.Delete;

      iErrors := qusprScale.ApplyUpdates;
      if iErrors = 0 then
        qusprScale.CommitUpdates;

      quSprScale.Refresh;
    end;
  end;
  end;

procedure TfmSprScales.acEditExecute(Sender: TObject);
begin
  ShowFormAddScales(sEdit);
end;

procedure TfmSprScales.acRefreshExecute(Sender: TObject);
begin
  dmD.quSprScale.Active:=False;
  dmD.quSprScale.Params.ParamByName('DEPART').Value:=fskl;
  dmD.quSprScale.Params.ParamByName('ISHOP').Value:=fShop;
  dmD.quSprScale.Active:=True;
end;

procedure TfmSprScales.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
  if not(fsModal in FormState) then
    fmSprScales:=Nil;
  ViewSc.StoreToIniFile(GetGridIniFile,False,[gsoUseSummary,gsoUseFilter]);
end;

procedure TfmSprScales.FormCreate(Sender: TObject);
begin
  ViewSc.RestoreFromIniFile(GetGridIniFile);
end;

procedure TfmSprScales.Init;
begin
  dmD.quSprScale.Active:=false;
  dmD.quSprScale.Params.ParamByName('DEPART').Value:=fskl;
  dmD.quSprScale.Params.ParamByName('ISHOP').Value:=fShop;
  dmD.quSprScale.Active:=true;
end;

procedure TfmSprScales.ViewScCustomDrawCell(Sender: TcxCustomGridTableView; ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var Value:string;
begin
  Value:=AViewInfo.GridRecord.DisplayTexts[ViewScISTATUS.Index];
  if Value='����������' then ACanvas.Canvas.Brush.Color := $00B3B3FF;
end;

end.

