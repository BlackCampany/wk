unit SprCardsType;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  CustomChildForm,
  Vcl.Controls, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus,
  cxControls, cxContainer, cxEdit, System.Actions, Vcl.ActnList, Data.DB,
  Data.Win.ADODB, cxTextEdit, cxMemo, Vcl.StdCtrls, Vcl.ExtCtrls, dxRibbonSkins,
  dxRibbonCustomizationForm, dxBar, dxBarExtItems, cxClasses, dxRibbon,
  cxCalendar, cxBarEditItem, dxStatusBar, dxRibbonStatusBar, cxDBLookupComboBox,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator,
  cxDBData, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridCustomView, cxGrid,vcl.Forms, cxLabel, cxCheckBox,
  JvComponentBase, JvFormPlacement, Dialogs, cxImageComboBox, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TfmSprCaTypes = class(TfmCustomChildForm)
    ActionList1: TActionList;
    acRefresh: TAction;
    dxRibbon1: TdxRibbon;
    rtScales: TdxRibbonTab;
    dxBarManager1: TdxBarManager;
    bmAction: TdxBar;
    bmClose: TdxBar;
    bUpdate: TdxBarLargeButton;
    bClose: TdxBarLargeButton;
    acClose: TAction;
    sbSprScale: TdxRibbonStatusBar;
    GrCaT: TcxGrid;
    ViewCaT: TcxGridDBTableView;
    LevelCaT: TcxGridLevel;
    JvFormStorage: TJvFormStorage;
    btnAdd: TdxBarLargeButton;
    btnDel: TdxBarLargeButton;
    btnEdit: TdxBarLargeButton;
    acAdd: TAction;
    acEdit: TAction;
    acDel: TAction;
    quCardsTypes: TFDQuery;
    dsquCardsTypes: TDataSource;
    ViewCaTID: TcxGridDBColumn;
    ViewCaTNAME: TcxGridDBColumn;
    quCardsTypesID: TIntegerField;
    quCardsTypesNAME: TStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acCloseExecute(Sender: TObject);
    procedure acRefreshExecute(Sender: TObject);
    procedure acAddExecute(Sender: TObject);
    procedure acEditExecute(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ViewCaTCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Init;
  end;
  procedure ShowFormSprCaTypes;

var
  fmSprCaTypes: TfmSprCaTypes;

implementation

uses
  Un2, dbs, AddSinglePar;

{$R *.dfm}

//��������� �������� �����
procedure ShowFormSprCaTypes;
begin
  if Assigned(fmSprCaTypes)=False then begin //����� �� ����������, � ����� �������
    fmSprCaTypes:=TfmSprCaTypes.Create(Application);
  end;
  fmSprCaTypes.Init;
  fmSprCaTypes.Show;
end;

procedure TfmSprCaTypes.acAddExecute(Sender: TObject);
var sRet:string;
    iNum:Integer;
begin
  if ShowFormAddStrPar(1,'',sRet) then
  begin
//    ShowMessage('�������� '+sRet);
    iNum:=fGetId(102);
    quCardsTypes.Append;
    quCardsTypesID.AsInteger:=iNum;
    quCardsTypesNAME.AsString:=sRet;
    quCardsTypes.Post;
  end;
end;

procedure TfmSprCaTypes.acCloseExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmSprCaTypes.acDelExecute(Sender: TObject);
var iErrors:integer;
begin
  // ��������
  if quCardsTypes.RecordCount>0 then
  begin
    if MessageDlg('������� ��� '+quCardsTypesNAME.AsString+' ?',mtConfirmation,[mbYes,mbNo],0) = mrYes then
    begin
      with dmS do
      begin
        quS.Active:=False;
        quS.SQL.Clear;
        quS.SQL.Add('');
        quS.SQL.Add('update [dbo].[ORDERSCLI]');
        quS.SQL.Add('Set [CLITYPE]=0');
        quS.SQL.Add('where [CLITYPE]='+its(quCardsTypesID.AsInteger));
        quS.ExecSQL;

        quCardsTypes.Delete;
      end;
    end;
  end;
end;

procedure TfmSprCaTypes.acEditExecute(Sender: TObject);
var sRet:string;
begin
  if quCardsTypes.RecordCount>0 then
  begin
    if ShowFormAddStrPar(0,quCardsTypesNAME.AsString,sRet) then
    begin
      quCardsTypes.Edit;
      quCardsTypesNAME.AsString:=sRet;
      quCardsTypes.Post;
    end;
  end;
end;

procedure TfmSprCaTypes.acRefreshExecute(Sender: TObject);
begin
  Init;
end;

procedure TfmSprCaTypes.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
  if not(fsModal in FormState) then
    fmSprCaTypes:=Nil;
  ViewCaT.StoreToIniFile(GetGridIniFile,False,[gsoUseSummary,gsoUseFilter]);
end;

procedure TfmSprCaTypes.FormCreate(Sender: TObject);
begin
  ViewCaT.RestoreFromIniFile(GetGridIniFile);
end;

procedure TfmSprCaTypes.Init;
begin
  ViewCaT.BeginUpdate;
  quCardsTypes.Active:=False;
  quCardsTypes.Active:=True;
  ViewCaT.EndUpdate;
end;

procedure TfmSprCaTypes.ViewCaTCustomDrawCell(Sender: TcxCustomGridTableView; ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var Value:string;
begin
  {
  Value:=AViewInfo.GridRecord.DisplayTexts[ViewScISTATUS.Index];
  if Value='����������' then ACanvas.Canvas.Brush.Color := $00B3B3FF;
  }
end;

end.

