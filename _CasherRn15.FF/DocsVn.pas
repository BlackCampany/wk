unit DocsVn;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SpeedBar, ExtCtrls, ComCtrls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Placemnt, cxImageComboBox, XPStyleActnCtrls, ActnList, ActnMan, Menus,
  FR_DSet, FR_DBSet, FR_Class, cxContainer, cxTextEdit, cxMemo;

type
  TfmDocsVn = class(TForm)
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    Timer1: TTimer;
    FormPlacement1: TFormPlacement;
    GridDocsVn: TcxGrid;
    ViewDocsVn: TcxGridDBTableView;
    LevelDocsVn: TcxGridLevel;
    ViewDocsVnID: TcxGridDBColumn;
    ViewDocsVnDATEDOC: TcxGridDBColumn;
    ViewDocsVnNUMDOC: TcxGridDBColumn;
    ViewDocsVnNAMEMH1: TcxGridDBColumn;
    ViewDocsVnIDSKL_FROM: TcxGridDBColumn;
    ViewDocsVnIDSKL_TO: TcxGridDBColumn;
    ViewDocsVnSUMIN: TcxGridDBColumn;
    ViewDocsVnSUMUCH: TcxGridDBColumn;
    ViewDocsVnSUMTAR: TcxGridDBColumn;
    ViewDocsVnSUMUCH1: TcxGridDBColumn;
    ViewDocsVnPROCNAC: TcxGridDBColumn;
    ViewDocsVnNAMEMH: TcxGridDBColumn;
    ViewDocsVnIACTIVE: TcxGridDBColumn;
    amDocsVn: TActionManager;
    acPeriod: TAction;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    SpeedItem6: TSpeedItem;
    acAddDoc1: TAction;
    acEditDoc1: TAction;
    acViewDoc1: TAction;
    acDelDoc1: TAction;
    acOnDoc1: TAction;
    acOffDoc1: TAction;
    SpeedItem7: TSpeedItem;
    SpeedItem8: TSpeedItem;
    acVid: TAction;
    SpeedItem9: TSpeedItem;
    LevelCardsVn: TcxGridLevel;
    ViewCardsVn: TcxGridDBTableView;
    ViewCardsVnNAME: TcxGridDBColumn;
    ViewCardsVnNAMESHORT: TcxGridDBColumn;
    ViewCardsVnIDCARD: TcxGridDBColumn;
    ViewCardsVnQUANT: TcxGridDBColumn;
    ViewCardsVnPRICEIN: TcxGridDBColumn;
    ViewCardsVnSUMIN: TcxGridDBColumn;
    ViewCardsVnPRICEUCH: TcxGridDBColumn;
    ViewCardsVnSUMUCH: TcxGridDBColumn;
    ViewCardsVnIDNDS: TcxGridDBColumn;
    ViewCardsVnSUMNDS: TcxGridDBColumn;
    ViewCardsVnDATEDOC: TcxGridDBColumn;
    ViewCardsVnNUMDOC: TcxGridDBColumn;
    ViewCardsVnNAMECL: TcxGridDBColumn;
    ViewCardsVnNAMEMH: TcxGridDBColumn;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    acPrint1: TAction;
    frRepDocsVn: TfrReport;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    acCopy: TAction;
    acInsertD: TAction;
    frquSpecVnSel: TfrDBDataSet;
    Memo1: TcxMemo;
    Excel1: TMenuItem;
    procedure SpeedItem1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acPeriodExecute(Sender: TObject);
    procedure acAddDoc1Execute(Sender: TObject);
    procedure acEditDoc1Execute(Sender: TObject);
    procedure acViewDoc1Execute(Sender: TObject);
    procedure ViewDocsVnDblClick(Sender: TObject);
    procedure acDelDoc1Execute(Sender: TObject);
    procedure acOnDoc1Execute(Sender: TObject);
    procedure acOffDoc1Execute(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure acVidExecute(Sender: TObject);
    procedure SpeedItem1Click0(Sender: TObject);
    procedure acPrint1Execute(Sender: TObject);
    procedure acCopyExecute(Sender: TObject);
    procedure acInsertDExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Excel1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure prButtonSet(bSet:Boolean);
    procedure prPart(IDH,IdSkl_From,IdSkl_To,iDate:INteger;Var rSumO,rSumT:Real);
  end;

var
  fmDocsVn: TfmDocsVn;
  bClearDocIn:Boolean = false;

implementation

uses Un1, dmOffice, PeriodUni, AddDoc3, DMOReps, TBuff, MainRnOffice;

{$R *.dfm}

procedure TfmDocsVn.prPart(IDH,IdSkl_From,IdSkl_To,iDate:INteger;Var rSumO,rSumT:Real);
Var PriceSp,PriceUch,rSumIn,rSumUch,rQs,rQ,rQp,rMessure:Real;
begin
  with dmO do
  with dmORep do
  begin
    quSpecVnSel.Active:=False;
    quSpecVnSel.ParamByName('IDHD').AsInteger:=IDH;
    quSpecVnSel.Active:=True;

    rSumO:=0;

    quSpecVnSel.First;
    while not quSpecVnSel.Eof do
    begin

      PriceSp:=0;
      PriceUch:=0;
      rSumIn:=0;
      rSumUch:=0;
      rQs:=quSpecVnSelQUANT.AsFloat*quSpecVnSelKM.AsFloat; //�������� � ��������
      prSelPartIn(quSpecVnSelIDCARD.AsInteger,IdSkl_From,0,0);

      quSelPartIn.First;
      if rQs>0 then
      begin
        while (not quSelPartIn.Eof) and (rQs>0) do
        begin
         //���� �� ���� ������� ���� �����, ��������� �������� ���
          rQp:=quSelPartInQREMN.AsFloat;
          if rQs<=rQp then  rQ:=rQs//��������� ������ ������ ���������
                      else  rQ:=rQp;
          rQs:=rQs-rQ;

          PriceSp:=quSelPartInPRICEIN.AsFloat;
          PriceUch:=quSelPartInPRICEOUT.AsFloat;
          rSumIn:=rSumIn+PriceSp*rQ;
          rSumUch:=rSumUch+PriceUch*rQ;
          rSumO:=rSumO+PriceSp*rQ; //�������� ������� �� ���������

          prAddPartOut.ParamByName('ARTICUL').AsInteger:=quSpecVnSelIDCARD.AsInteger;
          prAddPartOut.ParamByName('IDDATE').AsInteger:=iDate;
          prAddPartOut.ParamByName('IDSTORE').AsInteger:=IdSkl_From;
          prAddPartOut.ParamByName('IDPARTIN').AsInteger:=quSelPartInID.AsInteger;
          prAddPartOut.ParamByName('IDDOC').AsInteger:=IdH;
          prAddPartOut.ParamByName('IDCLI').AsInteger:=quSelPartInIDCLI.AsInteger;
          prAddPartOut.ParamByName('DTYPE').AsInteger:=4;
          prAddPartOut.ParamByName('QUANT').AsFloat:=rQ;
          prAddPartOut.ParamByName('PRICEIN').AsFloat:=PriceSp;
          prAddPartOut.ParamByName('SUMOUT').AsFloat:=rSumUch;
          prAddPartout.ExecProc;

          //��� �� �������� ��������� ������� ������
          // ���� �� ����� -  ���������� �������� � ����������

          prAddPartIn1.ParamByName('IDSKL').AsInteger:=IdSkl_To;
          prAddPartIn1.ParamByName('IDDOC').AsInteger:=IDH;
          prAddPartIn1.ParamByName('DTYPE').AsInteger:=4;
          prAddPartIn1.ParamByName('IDATE').AsInteger:=iDate;
          prAddPartIn1.ParamByName('IDCARD').AsInteger:=quSpecVnSelIDCARD.AsInteger;
          prAddPartIn1.ParamByName('IDCLI').AsInteger:=quSelPartInIDCLI.AsInteger;
          prAddPartIn1.ParamByName('QUANT').AsFloat:=rQ;
          prAddPartIn1.ParamByName('PRICEIN').AsFloat:=PriceSp;
          prAddPartIn1.ParamByName('PRICEUCH').AsFloat:=PriceUch;
          prAddPartIn1.ExecProc;


          quSelPartIn.Next;
        end;

        if rQs>0 then //�������� ������������� ������, �������� � ������, �� � ������� ��������� ������ ���, ��� � �������������
        begin
          if PriceSp=0 then
          begin //��� ���� ���������� ������� � ���������� ����������
            prCalcLastPrice1.ParamByName('IDGOOD').AsInteger:=quSpecVnSelIDCARD.AsInteger;
            prCalcLastPrice1.ExecProc;
            PriceSp:=prCalcLastPrice1.ParamByName('PRICEIN').AsFloat;
            rMessure:=prCalcLastPrice1.ParamByName('KOEF').AsFloat;
            if (rMessure<>0)and(rMessure<>1) then PriceSp:=PriceSp/rMessure;
          end;

          prAddPartOut.ParamByName('ARTICUL').AsInteger:=quSpecVnSelIDCARD.AsInteger;
          prAddPartOut.ParamByName('IDDATE').AsInteger:=iDate;
          prAddPartOut.ParamByName('IDSTORE').AsInteger:=IdSkl_From;
          prAddPartOut.ParamByName('IDPARTIN').AsInteger:=-1;
          prAddPartOut.ParamByName('IDDOC').AsInteger:=IdH;
          prAddPartOut.ParamByName('IDCLI').AsInteger:=0;
          prAddPartOut.ParamByName('DTYPE').AsInteger:=4;
          prAddPartOut.ParamByName('QUANT').AsFloat:=rQs;
          prAddPartOut.ParamByName('PRICEIN').AsFloat:=PriceSp;
          prAddPartOut.ParamByName('SUMOUT').AsFloat:=PriceUch*rQs;
          prAddPartout.ExecProc;

          //��� �� �������� ��������� ������� ������
          // ���� �� ����� -  ���������� �������� � ����������

          prAddPartIn1.ParamByName('IDSKL').AsInteger:=IdSkl_To;
          prAddPartIn1.ParamByName('IDDOC').AsInteger:=IdH;
          prAddPartIn1.ParamByName('DTYPE').AsInteger:=4;
          prAddPartIn1.ParamByName('IDATE').AsInteger:=iDate;
          prAddPartIn1.ParamByName('IDCARD').AsInteger:=quSpecVnSelIDCARD.AsInteger;
          prAddPartIn1.ParamByName('IDCLI').AsInteger:=0;
          prAddPartIn1.ParamByName('QUANT').AsFloat:=rQs;
          prAddPartIn1.ParamByName('PRICEIN').AsFloat:=PriceSp;
          prAddPartIn1.ParamByName('PRICEUCH').AsFloat:=PriceUch;
          prAddPartIn1.ExecProc;

          rSumIn:=rSumIn+PriceSp*rQs;
          rSumO:=rSumO+PriceSp*rQs;
        end;
      end;
      quSelPartIn.Active:=False;

            //�������� ���������
      quSpecVnSel.Edit;
      if quSpecVnSelQUANT.AsFloat<>0 then
      begin
        quSpecVnSelSUMIN.AsFloat:=RoundVal(rSumIn);
        quSpecVnSelSUMUCH.AsFloat:=RoundVal(rSumUch);
        quSpecVnSelPRICEIN.AsFloat:=rSumIn/quSpecVnSelQUANT.AsFloat;
        quSpecVnSelPRICEUCH.AsFloat:=rSumUch/quSpecVnSelQUANT.AsFloat;
      end else
      begin
        quSpecVnSelSUMIN.AsFloat:=0;
        quSpecVnSelSUMUCH.AsFloat:=0;
        quSpecVnSelPRICEIN.AsFloat:=0;
        quSpecVnSelPRICEUCH.AsFloat:=0;
      end;
      quSpecVnSel.Post;

      quSpecVnSel.Next;
      delay(10);
    end;

    quSpecVnSel.Active:=False;

     //�� ����

    quTaraVn.Active:=False;
    quTaraVn.ParamByName('IDH').AsInteger:=IDH;
    quTaraVn.Active:=True;

    rSumT:=0;

    quTaraVn.First;
    while not quTaraVn.Eof do
    begin

      PriceSp:=0;
      PriceUch:=0;
      rSumIn:=0;
      rSumUch:=0;
      rQs:=quTaraVnQUANT.AsFloat*quTaraVnKM.AsFloat; //�������� � ��������
      prSelPartInT(quTaraVnIDCARD.AsInteger,IdSkl_From,0);

      quSelPartIn.First;
      if rQs>0 then
      begin
        while (not quSelPartIn.Eof) and (rQs>0) do
        begin
         //���� �� ���� ������� ���� �����, ��������� �������� ���
          rQp:=quSelPartInQREMN.AsFloat;
          if rQs<=rQp then  rQ:=rQs//��������� ������ ������ ���������
                      else  rQ:=rQp;
          rQs:=rQs-rQ;

          PriceSp:=quSelPartInPRICEIN.AsFloat;
          PriceUch:=quSelPartInPRICEOUT.AsFloat;
          rSumIn:=rSumIn+PriceSp*rQ;
          rSumUch:=rSumUch+PriceUch*rQ;
          rSumT:=rSumT+PriceSp*rQ;

          prAddPartOutT.ParamByName('ARTICUL').AsInteger:=quTaraVnIDCARD.AsInteger;
          prAddPartOutT.ParamByName('IDDATE').AsInteger:=iDate;
          prAddPartOutT.ParamByName('IDSTORE').AsInteger:=IdSkl_From;
          prAddPartOutT.ParamByName('IDPARTIN').AsInteger:=quSelPartInID.AsInteger;
          prAddPartOutT.ParamByName('IDDOC').AsInteger:=IdH;
          prAddPartOutT.ParamByName('IDCLI').AsInteger:=quSelPartInIDCLI.AsInteger;
          prAddPartOutT.ParamByName('DTYPE').AsInteger:=4;
          prAddPartOutT.ParamByName('QUANT').AsFloat:=rQ;
          prAddPartOutT.ParamByName('PRICEIN').AsFloat:=PriceSp;
          prAddPartOutT.ParamByName('SUMOUT').AsFloat:=rSumUch;
          prAddPartoutT.ExecProc;

           //��� �� �������� ��������� ������� ������
           // ���� �� ����� -  ���������� �������� � ����������

          prAddPartIn1T.ParamByName('IDSKL').AsInteger:=IdSkl_To;
          prAddPartIn1T.ParamByName('IDDOC').AsInteger:=IdH;
          prAddPartIn1T.ParamByName('DTYPE').AsInteger:=4;
          prAddPartIn1T.ParamByName('IDATE').AsInteger:=iDate;
          prAddPartIn1T.ParamByName('IDCARD').AsInteger:=quTaraVnIDCARD.AsInteger;
          prAddPartIn1T.ParamByName('IDCLI').AsInteger:=quSelPartInIDCLI.AsInteger;
          prAddPartIn1T.ParamByName('QUANT').AsFloat:=rQ;
          prAddPartIn1T.ParamByName('PRICEIN').AsFloat:=PriceSp;
          prAddPartIn1T.ParamByName('PRICEUCH').AsFloat:=PriceUch;
          prAddPartIn1T.ExecProc;


          quSelPartIn.Next;
        end;

        if rQs>0 then //�������� ������������� ������, �������� � ������, �� � ������� ��������� ������ ���, ��� � �������������
        begin
          if PriceSp=0 then
          begin //��� ���� ���������� ������� � ���������� ����������
            prCalcLastPrice1.ParamByName('IDGOOD').AsInteger:=quTaraVnIDCARD.AsInteger;
            prCalcLastPrice1.ExecProc;
            PriceSp:=prCalcLastPrice1.ParamByName('PRICEIN').AsFloat;
            rMessure:=prCalcLastPrice1.ParamByName('KOEF').AsFloat;
            if (rMessure<>0)and(rMessure<>1) then PriceSp:=PriceSp/rMessure;

            prAddPartOutT.ParamByName('ARTICUL').AsInteger:=quTaraVnIDCARD.AsInteger;
            prAddPartOutT.ParamByName('IDDATE').AsInteger:=iDate;
            prAddPartOutT.ParamByName('IDSTORE').AsInteger:=quDocsVnSelIDSKL_FROM.AsInteger;
            prAddPartOutT.ParamByName('IDPARTIN').AsInteger:=-1;
            prAddPartOutT.ParamByName('IDDOC').AsInteger:=IdH;
            prAddPartOutT.ParamByName('IDCLI').AsInteger:=0;
            prAddPartOutT.ParamByName('DTYPE').AsInteger:=4;
            prAddPartOutT.ParamByName('QUANT').AsFloat:=rQs;
            prAddPartOutT.ParamByName('PRICEIN').AsFloat:=PriceSp;
            prAddPartOutT.ParamByName('SUMOUT').AsFloat:=PriceUch*rQs;
            prAddPartoutT.ExecProc;

            //��� �� �������� ��������� ������� ������
            // ���� �� ����� -  ���������� �������� � ����������

            prAddPartIn1T.ParamByName('IDSKL').AsInteger:=IdSkl_To;
            prAddPartIn1T.ParamByName('IDDOC').AsInteger:=IdH;
            prAddPartIn1T.ParamByName('DTYPE').AsInteger:=4;
            prAddPartIn1T.ParamByName('IDATE').AsInteger:=iDate;
            prAddPartIn1T.ParamByName('IDCARD').AsInteger:=quTaraVnIDCARD.AsInteger;
            prAddPartIn1T.ParamByName('IDCLI').AsInteger:=0;
            prAddPartIn1T.ParamByName('QUANT').AsFloat:=rQs;
            prAddPartIn1T.ParamByName('PRICEIN').AsFloat:=PriceSp;
            prAddPartIn1T.ParamByName('PRICEUCH').AsFloat:=PriceUch;
            prAddPartIn1T.ExecProc;

          end;
          rSumIn:=rSumIn+PriceSp*rQs;
          rSumT:=rSumT+PriceSp*rQs;
        end;
      end;
      quSelPartIn.Active:=False;

      //�������� ���������
      quTaraVn.Edit;
      if quTaraVnQUANT.AsFloat<>0 then
      begin
        quTaraVnSUMIN.AsFloat:=RoundVal(rSumIn);
        quTaraVnSUMUCH.AsFloat:=RoundVal(rSumUch);
        quTaraVnPRICEIN.AsFloat:=rSumIn/quTaraVnQUANT.AsFloat;
        quTaraVnPRICEUCH.AsFloat:=rSumUch/quTaraVnQUANT.AsFloat;
      end else
      begin
        quTaraVnSUMIN.AsFloat:=0;
        quTaraVnSUMUCH.AsFloat:=0;
        quTaraVnPRICEIN.AsFloat:=0;
        quTaraVnPRICEUCH.AsFloat:=0;
      end;
      quTaraVn.Post;

      quTaraVn.Next;
      delay(10);
    end;

    quTaraVn.Active:=False;

  end;
end;

procedure TfmDocsVn.prButtonSet(bSet:Boolean);
begin
  SpeedItem1.Enabled:=bSet;
  SpeedItem2.Enabled:=bSet;
  SpeedItem3.Enabled:=bSet;
  SpeedItem4.Enabled:=bSet;
  SpeedItem5.Enabled:=bSet;
  SpeedItem6.Enabled:=bSet;
  SpeedItem7.Enabled:=bSet;
  SpeedItem8.Enabled:=bSet;
  SpeedItem9.Enabled:=bSet;
  delay(100);
end;


procedure TfmDocsVn.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmDocsVn.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  Timer1.Enabled:=True;
  GridDocsVn.Align:=AlClient;
  ViewDocsVn.RestoreFromIniFile(CurDir+GridIni);
  ViewCardsVn.RestoreFromIniFile(CurDir+GridIni);
  StatusBar1.Color:=$00FFCACA;
end;

procedure TfmDocsVn.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewDocsVn.StoreToIniFile(CurDir+GridIni,False);
  ViewCardsVn.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmDocsVn.acPeriodExecute(Sender: TObject);
begin
//  ������
  fmPeriodUni.DateTimePicker1.Date:=CommonSet.DateFrom;
//  fmPeriodUni.DateTimePicker2.Date:=CommonSet.DateTo-1;
  fmPeriodUni.ShowModal;
  if fmPeriodUni.ModalResult=mrOk then
  begin
    CommonSet.DateFrom:=Trunc(fmPeriodUni.DateTimePicker1.Date);
    CommonSet.DateTo:=Trunc(fmPeriodUni.DateTimePicker2.Date)+1;

    with dmO do
    with dmORep do
    begin
      if LevelDocsVn.Visible then
      begin
        if CommonSet.DateTo>=iMaxDate then fmDocsVn.Caption:='���������� ��������� � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
        else fmDocsVn.Caption:='���������� ��������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);

        ViewDocsVn.BeginUpdate;
        quDocsVnSel.Active:=False;
        quDocsVnSel.ParamByName('DATEB').AsDate:=CommonSet.DateFrom;
        quDocsVnSel.ParamByName('DATEE').AsDate:=CommonSet.DateTo;
        quDocsVnSel.Active:=True;
        ViewDocsVn.EndUpdate;
      end else
      begin
        if CommonSet.DateTo>=iMaxDate then fmDocsVn.Caption:='����������� �� ������� � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
        else fmDocsVn.Caption:='����������� �� ������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);

        ViewCardsVn.BeginUpdate;
        quDocsInCard.Active:=False;
        quDocsInCard.ParamByName('DATEB').AsDate:=CommonSet.DateFrom;
        quDocsInCard.ParamByName('DATEE').AsDate:=CommonSet.DateTo;
        quDocsInCard.Active:=True;
        ViewCardsVn.EndUpdate;
      end;
    end;
  end;
end;

procedure TfmDocsVn.acAddDoc1Execute(Sender: TObject);
//Var IDH:INteger;
//    rSum1,rSum2:Real;
begin
  //�������� ��������

  if not CanDo('prAddDocVn') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmO do
  begin
    fmAddDoc3.Caption:='���������: ����� ��������.';
    fmAddDoc3.cxTextEdit1.Text:=prGetNum(4,0);
    fmAddDoc3.cxTextEdit1.Tag:=0;
    fmAddDoc3.cxDateEdit1.Tag:=0;
    fmAddDoc3.cxTextEdit1.Properties.ReadOnly:=False;
    fmAddDoc3.cxDateEdit1.Date:=Date;
    fmAddDoc3.cxDateEdit1.Properties.ReadOnly:=False;

    if quMHAll.Active=False then quMHAll.Active:=True;
    quMHAll.FullRefresh;
    //�� ����
    fmAddDoc3.cxLookupComboBox2.EditValue:=0;
    fmAddDoc3.cxLookupComboBox2.Text:='';
    fmAddDoc3.cxLookupComboBox2.Properties.ReadOnly:=False;
    //����
    fmAddDoc3.cxLookupComboBox1.EditValue:=0;
    fmAddDoc3.cxLookupComboBox1.Text:='';
    fmAddDoc3.cxLookupComboBox1.Properties.ReadOnly:=False;

    if CurVal.IdMH<>0 then
    begin
      //����
      fmAddDoc3.cxLookupComboBox1.EditValue:=CurVal.IdMH;
      fmAddDoc3.cxLookupComboBox1.Text:=CurVal.NAMEMH;
      //�� ����
      fmAddDoc3.cxLookupComboBox2.EditValue:=CurVal.IdMH;
      fmAddDoc3.cxLookupComboBox2.Text:=CurVal.NAMEMH;
    end else
    begin
       quMHAll.First;
       if not quMHAll.Eof then
       begin
         CurVal.IdMH:=quMHAllID.AsInteger;
         CurVal.NAMEMH:=quMHAllNAMEMH.AsString;
         //������
         fmAddDoc3.cxLookupComboBox2.EditValue:=CurVal.IdMH;
         fmAddDoc3.cxLookupComboBox2.Text:=CurVal.NAMEMH;
         //����
         fmAddDoc3.cxLookupComboBox1.EditValue:=CurVal.IdMH;
         fmAddDoc3.cxLookupComboBox1.Text:=CurVal.NAMEMH;
       end;
    end;
    if quMHAll.Locate('ID',CurVal.IdMH,[]) then
    begin
      //�� ����
      fmAddDoc3.Label2.Caption:='��. ����: '+quMHAllNAMEPRICE.AsString;
      fmAddDoc3.Label2.Tag:=quMHAllDEFPRICE.AsInteger;
      //����
      fmAddDoc3.Label15.Caption:='��. ����: '+quMHAllNAMEPRICE.AsString;
      fmAddDoc3.Label15.Tag:=quMHAllDEFPRICE.AsInteger;
    end else
    begin
      //�� ����
      fmAddDoc3.Label2.Caption:='��. ����: ';
      fmAddDoc3.Label2.Tag:=0;
      //����
      fmAddDoc3.Label15.Caption:='��. ����: ';
      fmAddDoc3.Label15.Tag:=0;
    end;

    fmAddDoc3.cxLabel1.Enabled:=True;
    fmAddDoc3.cxLabel2.Enabled:=True;
    fmAddDoc3.cxLabel3.Enabled:=True;
    fmAddDoc3.cxLabel4.Enabled:=True;
    fmAddDoc3.cxLabel5.Enabled:=True;
    fmAddDoc3.cxLabel6.Enabled:=True;
    fmAddDoc3.N1.Enabled:=True;
    fmAddDoc3.cxLookupComboBox2.Properties.ReadOnly:=False;
    fmAddDoc3.cxLookupComboBox1.Properties.ReadOnly:=False;
    fmAddDoc3.cxButton1.Enabled:=True;
    fmAddDoc3.cxButton1.Visible:=True;
    

    fmAddDoc3.ViewDoc3.OptionsData.Editing:=True;
    fmAddDoc3.ViewDoc3.OptionsData.Deleting:=True;
    fmAddDoc3.ViewTara.OptionsData.Editing:=True;
    fmAddDoc3.ViewTara.OptionsData.Deleting:=True;

    CloseTa(fmAddDoc3.taTara);
    CloseTa(fmAddDoc3.taSpec);

    fmAddDoc3.ShowModal;
  end;
end;

procedure TfmDocsVn.acEditDoc1Execute(Sender: TObject);
Var IDH:INteger;
//    rSum1,rSum2:Real;
begin
  //�������������
  if not CanDo('prEditDocVn') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmORep do
  with dmO do
  begin
    if quDocsVnSel.RecordCount>0 then //���� ��� �������������
    begin
      if quDocsVnSelIACTIVE.AsInteger=0 then
      begin
        prAllViewOff;

        fmAddDoc3.Caption:='���������: ��������������.';
        fmAddDoc3.cxTextEdit1.Text:=quDocsVnSelNUMDOC.AsString;
        fmAddDoc3.cxTextEdit1.Properties.ReadOnly:=False;
        fmAddDoc3.cxTextEdit1.Tag:=quDocsVnSelID.AsInteger;
        fmAddDoc3.cxDateEdit1.Date:=quDocsVnSelDATEDOC.AsDateTime;
        fmAddDoc3.cxDateEdit1.Properties.ReadOnly:=False;
        fmAddDoc3.cxDateEdit1.Tag:=0;

        if quMHAll.Active=False then quMHAll.Active:=True;
        quMHAll.FullRefresh;

        //�� ����
        fmAddDoc3.cxLookupComboBox2.EditValue:=quDocsVnSelIDSKL_FROM.AsInteger;
        fmAddDoc3.cxLookupComboBox2.Text:=quDocsVnSelNAMEMH.AsString;
        //����
        fmAddDoc3.cxLookupComboBox1.EditValue:=quDocsVnSelIDSKL_TO.AsInteger;
        fmAddDoc3.cxLookupComboBox1.Text:=quDocsVnSelNAMEMH1.AsString;

        if quMHAll.Locate('ID',quDocsVnSelIDSKL_FROM.AsInteger,[]) then
        begin
          //�� ����
          fmAddDoc3.Label2.Caption:='��. ����: '+quMHAllNAMEPRICE.AsString;
          fmAddDoc3.Label2.Tag:=quMHAllDEFPRICE.AsInteger;
        end;
        if quMHAll.Locate('ID',quDocsVnSelIDSKL_TO.AsInteger,[]) then
        begin
          //����
          fmAddDoc3.Label15.Caption:='��. ����: '+quMHAllNAMEPRICE.AsString;
          fmAddDoc3.Label15.Tag:=quMHAllDEFPRICE.AsInteger;
        end;

        fmAddDoc3.cxLabel1.Enabled:=True;
        fmAddDoc3.cxLabel2.Enabled:=True;
        fmAddDoc3.cxLabel3.Enabled:=True;
        fmAddDoc3.cxLabel4.Enabled:=True;
        fmAddDoc3.cxLabel5.Enabled:=True;
        fmAddDoc3.cxLabel6.Enabled:=True;
        fmAddDoc3.N1.Enabled:=True;
        fmAddDoc3.cxLookupComboBox2.Properties.ReadOnly:=False;
        fmAddDoc3.cxLookupComboBox1.Properties.ReadOnly:=False;
        fmAddDoc3.cxButton1.Enabled:=True;
        fmAddDoc3.cxButton1.Visible:=True;

        fmAddDoc3.ViewDoc3.OptionsData.Editing:=True;
        fmAddDoc3.ViewDoc3.OptionsData.Deleting:=True;
        fmAddDoc3.ViewTara.OptionsData.Editing:=True;
        fmAddDoc3.ViewTara.OptionsData.Deleting:=True;

        CloseTa(fmAddDoc3.taTara);
        CloseTa(fmAddDoc3.taSpec);

        IDH:=quDocsVnSelID.AsInteger;

        quSpecVnSel.Active:=False;
        quSpecVnSel.ParamByName('IDHD').AsInteger:=IDH;
        quSpecVnSel.Active:=True;

        quSpecVnSel.First;
        while not quSpecVnSel.Eof do
        begin
          with fmAddDoc3 do
          begin
            taSpec.Append;
            taSpecNum.AsInteger:=quSpecVnSelNUM.AsInteger;
            taSpecIdGoods.AsInteger:=quSpecVnSelIDCARD.AsInteger;
            taSpecNameG.AsString:=quSpecVnSelNAMEC.AsString;
            taSpecIM.AsInteger:=quSpecVnSelIDM.AsInteger;
            taSpecSM.AsString:=quSpecVnSelSM.AsString;
            taSpecQuant.AsFloat:=quSpecVnSelQUANT.AsFloat;
            taSpecPrice1.AsFloat:=quSpecVnSelPRICEIN.AsFloat;
            taSpecSum1.AsFloat:=quSpecVnSelSUMIN.AsFloat;
            taSpecPrice2.AsFloat:=quSpecVnSelPRICEUCH.AsFloat;
            taSpecSum2.AsFloat:=quSpecVnSelSUMUCH.AsFloat;
            taSpecPrice3.AsFloat:=quSpecVnSelPRICEUCH1.AsFloat;
            taSpecSum3.AsFloat:=quSpecVnSelSUMUCH1.AsFloat;
            taSpecSumNac.AsFloat:=0;
            taSpecProcNac.AsFloat:=0;
            taSpecKM.AsFloat:=quSpecVnSelKM.AsFloat;
            taSpec.Post;
          end;
          quSpecVnSel.Next;
        end;

        quTaraVn.Active:=False;
        quTaraVn.ParamByName('IDH').AsInteger:=IDH;
        quTaraVn.Active:=True;

        quTaraVn.First;
        while not quTaraVn.Eof do
        begin
          with fmAddDoc3 do
          begin
            taTara.Append;
            taTaraNum.AsInteger:=quTaraVnNUM.AsInteger;
            taTaraIdCard.AsInteger:=quTaraVnIDCARD.AsInteger;
            taTaraNameG.AsString:=quTaraVnNAMEC.AsString;
            taTaraQuant.AsFloat:=quTaraVnQUANT.AsFloat;
            taTaraPriceIn.AsFloat:=quTaraVnPRICEIN.AsFloat;
            taTaraSumIn.AsFloat:=quTaraVnSUMIN.AsFloat;
            taTaraPriceUch.AsFloat:=quTaraVnPRICEUCH.AsFloat;
            taTaraSumUch.AsFloat:=quTaraVnSUMUCH.AsFloat;
            taTaraIM.AsInteger:=quTaraVnIDM.AsInteger;
            taTaraKM.AsFloat:=quTaraVnKM.asfloat;
            taTaraSM.AsString:=quTaraVnSM.asString;
            taTaraSumNac.AsFloat:=quTaraVnSUMUCH.AsFloat-quTaraVnSUMIN.AsFloat;
            if abs(quTaraVnSUMIN.AsFloat)>0.01 then
            begin
              taTaraProcNac.AsFloat:=(quTaraVnSUMUCH.AsFloat-quTaraVnSUMIN.AsFloat)/quTaraVnSUMIN.AsFloat*100;
            end;

            taTara.Post;
          end;
          quTaraVn.Next;
        end;

        quTaraVn.Active:=False;
        quSpecVnSel.Active:=False;

        prAllViewOn;

        fmAddDoc3.ShowModal;
      end else
      begin
        showmessage('������������� �������������� �������� ������.');
      end;
    end else
    begin
      showmessage('�������� �������� ��� ��������������.');
    end;
  end;
end;

procedure TfmDocsVn.acViewDoc1Execute(Sender: TObject);
Var IDH:INteger;
begin
  //��������
  if not CanDo('prViewDocVn') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmORep do
  with dmO do
  begin
    if quDocsVnSel.RecordCount>0 then //���� ��� �������������
    begin
      fmAddDoc3.Caption:='���������: ��������.';

      prAllViewOff;

      fmAddDoc3.cxTextEdit1.Text:=quDocsVnSelNUMDOC.AsString;
      fmAddDoc3.cxTextEdit1.Properties.ReadOnly:=True;
      fmAddDoc3.cxTextEdit1.Tag:=quDocsVnSelID.AsInteger;
      fmAddDoc3.cxDateEdit1.Date:=quDocsVnSelDATEDOC.AsDateTime;
      fmAddDoc3.cxDateEdit1.Properties.ReadOnly:=True;
      fmAddDoc3.cxDateEdit1.Tag:=1;

      if quMHAll.Active=False then quMHAll.Active:=True;
      quMHAll.FullRefresh;

      //�� ����
      fmAddDoc3.cxLookupComboBox2.EditValue:=quDocsVnSelIDSKL_FROM.AsInteger;
      fmAddDoc3.cxLookupComboBox2.Text:=quDocsVnSelNAMEMH.AsString;
        //����
      fmAddDoc3.cxLookupComboBox1.EditValue:=quDocsVnSelIDSKL_TO.AsInteger;
      fmAddDoc3.cxLookupComboBox1.Text:=quDocsVnSelNAMEMH1.AsString;

      if quMHAll.Locate('ID',quDocsVnSelIDSKL_FROM.AsInteger,[]) then
      begin
        //�� ����
        fmAddDoc3.Label2.Caption:='��. ����: '+quMHAllNAMEPRICE.AsString;
        fmAddDoc3.Label2.Tag:=quMHAllDEFPRICE.AsInteger;
      end;
      if quMHAll.Locate('ID',quDocsVnSelIDSKL_TO.AsInteger,[]) then
      begin
          //����
        fmAddDoc3.Label15.Caption:='��. ����: '+quMHAllNAMEPRICE.AsString;
        fmAddDoc3.Label15.Tag:=quMHAllDEFPRICE.AsInteger;
      end;

      fmAddDoc3.cxLabel1.Enabled:=False;
      fmAddDoc3.cxLabel2.Enabled:=False;
      fmAddDoc3.cxLabel3.Enabled:=False;
      fmAddDoc3.cxLabel4.Enabled:=False;
      fmAddDoc3.cxLabel5.Enabled:=False;
      fmAddDoc3.cxLabel6.Enabled:=False;
      fmAddDoc3.N1.Enabled:=False;
      fmAddDoc3.cxLookupComboBox2.Properties.ReadOnly:=True;
      fmAddDoc3.cxLookupComboBox1.Properties.ReadOnly:=True;
      fmAddDoc3.cxButton1.Enabled:=False;
      fmAddDoc3.cxButton1.Visible:=False;

      fmAddDoc3.ViewDoc3.OptionsData.Editing:=False;
      fmAddDoc3.ViewDoc3.OptionsData.Deleting:=False;
      fmAddDoc3.ViewTara.OptionsData.Editing:=False;
      fmAddDoc3.ViewTara.OptionsData.Deleting:=False;

      CloseTa(fmAddDoc3.taSpec);
      CloseTa(fmAddDoc3.taTara);

      IDH:=quDocsVnSelID.AsInteger;

      quSpecVnSel.Active:=False;
      quSpecVnSel.ParamByName('IDHD').AsInteger:=IDH;
      quSpecVnSel.Active:=True;

      quSpecVnSel.First;
      while not quSpecVnSel.Eof do
      begin
        with fmAddDoc3 do
        begin
          taSpec.Append;
          taSpecNum.AsInteger:=quSpecVnSelNUM.AsInteger;
          taSpecIdGoods.AsInteger:=quSpecVnSelIDCARD.AsInteger;
          taSpecNameG.AsString:=quSpecVnSelNAMEC.AsString;
          taSpecIM.AsInteger:=quSpecVnSelIDM.AsInteger;
          taSpecSM.AsString:=quSpecVnSelSM.AsString;
          taSpecQuant.AsFloat:=quSpecVnSelQUANT.AsFloat;
          taSpecPrice1.AsFloat:=quSpecVnSelPRICEIN.AsFloat;
          taSpecSum1.AsFloat:=quSpecVnSelSUMIN.AsFloat;
          taSpecPrice2.AsFloat:=quSpecVnSelPRICEUCH.AsFloat;
          taSpecSum2.AsFloat:=quSpecVnSelSUMUCH.AsFloat;
          taSpecPrice3.AsFloat:=quSpecVnSelPRICEUCH1.AsFloat;
          taSpecSum3.AsFloat:=quSpecVnSelSUMUCH1.AsFloat;
          taSpecSumNac.AsFloat:=0;
          taSpecProcNac.AsFloat:=0;
          taSpecKM.AsFloat:=quSpecVnSelKM.AsFloat;
          taSpec.Post;
        end;
        quSpecVnSel.Next;
      end;

      quTaraVn.Active:=False;
      quTaraVn.ParamByName('IDH').AsInteger:=IDH;
      quTaraVn.Active:=True;

      quTaraVn.First;
      while not quTaraVn.Eof do
      begin
        with fmAddDoc3 do
        begin
          taTara.Append;
          taTaraNum.AsInteger:=quTaraVnNUM.AsInteger;
          taTaraIdCard.AsInteger:=quTaraVnIDCARD.AsInteger;
          taTaraNameG.AsString:=quTaraVnNAMEC.AsString;
          taTaraQuant.AsFloat:=quTaraVnQUANT.AsFloat;
          taTaraPriceIn.AsFloat:=quTaraVnPRICEIN.AsFloat;
          taTaraSumIn.AsFloat:=quTaraVnSUMIN.AsFloat;
          taTaraPriceUch.AsFloat:=quTaraVnPRICEUCH.AsFloat;
          taTaraSumUch.AsFloat:=quTaraVnSUMUCH.AsFloat;
          taTaraIM.AsInteger:=quTaraVnIDM.AsInteger;
          taTaraKM.AsFloat:=quTaraVnKM.asfloat;
          taTaraSM.AsString:=quTaraVnSM.asString;
          taTaraSumNac.AsFloat:=quTaraVnSUMUCH.AsFloat-quTaraVnSUMIN.AsFloat;
          if abs(quTaraVnSUMIN.AsFloat)>0.01 then
          begin
            taTaraProcNac.AsFloat:=(quTaraVnSUMUCH.AsFloat-quTaraVnSUMIN.AsFloat)/quTaraVnSUMIN.AsFloat*100;
          end;

          taTara.Post;
        end;
        quTaraVn.Next;
      end;

      quTaraVn.Active:=False;
      quSpecVnSel.Active:=False;

      prAllViewOn;

      fmAddDoc3.ShowModal;
    end else
    begin
      showmessage('�������� �������� ��� ���������.');
    end;
  end;
end;

procedure TfmDocsVn.ViewDocsVnDblClick(Sender: TObject);
begin
  //������� �������
  with dmORep do
  begin
    if quDocsVnSelIACTIVE.AsInteger=0 then acEditDoc1.Execute //��������������
    else acViewDoc1.Execute; //��������
  end;
end;

procedure TfmDocsVn.acDelDoc1Execute(Sender: TObject);
begin
  //������� ��������
  if not CanDo('prDelDocVn') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmO do
  with dmORep do
  begin
    if quDocsVnSel.RecordCount>0 then //���� ��� �������������
    begin
      if quDocsVnSelIACTIVE.AsInteger=0 then
      begin
        if MessageDlg('�� ������������� ������ ������� ��������� �'+quDocsVnSelNUMDOC.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          quDocsVnSel.Delete;
        end;
      end else
      begin
        showmessage('������� �������������� �������� ������.');
      end;
    end else
    begin
      showmessage('�������� �������� ��� ��������.');
    end;
  end;
end;

procedure TfmDocsVn.acOnDoc1Execute(Sender: TObject);
Var IdH:INteger;
    rSumO,rSumT:Real;
begin
//������������
  with dmORep do
  with dmO do
  begin
    if not CanDo('prOnDocVn') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
    if not CanEdit(Trunc(quDocsVnSelDATEDOC.AsDateTime)) then begin StatusBar1.Panels[0].Text:='������ ������.'; exit; end;

    prButtonSet(False);

    if quDocsVnSel.RecordCount>0 then //���� ��� ������������
    begin
      if quDocsVnSelIACTIVE.AsInteger=0 then
      begin
        if MessageDlg('������������ �������� �'+quDocsVnSelNUMDOC.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          if (prTOFind(Trunc(quDocsVnSelDATEDOC.AsDateTime),quDocsVnSelIDSKL_FROM.AsInteger)=1)or
           (prTOFind(Trunc(quDocsVnSelDATEDOC.AsDateTime),quDocsVnSelIDSKL_TO.AsInteger)=1) then
          begin //�� ����
            if MessageDlg('������� ��� �������� ������ �� �� '+quDocsVnSelNAMEMH.AsString+','+quDocsVnSelNAMEMH1.AsString+'  � '+FormatDateTime('dd.mm.yyyy',quDocsVnSelDATEDOC.AsDateTime)+'?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
            begin
              prTODel(Trunc(quDocsVnSelDATEDOC.AsDateTime),quDocsVnSelIDSKL_FROM.AsInteger);
              prTODel(Trunc(quDocsVnSelDATEDOC.AsDateTime),quDocsVnSelIDSKL_TO.AsInteger);
            end
            else
            begin
              showmessage('��������� ������� ��������� ����������...');
              prButtonSet(True);
              exit;
            end;
          end;

          //������� ��� ������ ���� ���� �� ������ ������ �� ������� ���������

          IDH:=quDocsVnSelID.AsInteger;
          prDelPart.ParamByName('IDDOC').AsInteger:=IDH;
          prDelPart.ParamByName('DTYPE').AsInteger:=4;
          prDelPart.ExecProc;

          //����������� ��� ������
          prPart(IDH,quDocsVnSelIDSKL_FROM.AsInteger,quDocsVnSelIDSKL_TO.AsInteger,trunc(quDocsVnSelDATEDOC.AsDateTime),rSumO,rSumT);

          //�������� ������  ���� ��� � ����� �����������
          quDocsVnSel.Edit;
          quDocsVnSelIACTIVE.AsInteger:=1;
          quDocsVnSelSUMIN.AsFloat:=RoundVal(rSumO);
          quDocsVnSelSUMTAR.AsFloat:=RoundVal(rSumT);
          quDocsVnSel.Post;
          quDocsVnSel.Refresh;
        end;
      end;
    end;
  end;
  prButtonSet(True);
end;

procedure TfmDocsVn.acOffDoc1Execute(Sender: TObject);
Var iCountPartOut:Integer;
    bStart:Boolean;
begin
//��������
  with dmO do
  with dmORep do
  begin
    if not CanDo('prOffDocVn') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
    if not CanEdit(Trunc(quDocsVnSelDATEDOC.AsDateTime)) then begin StatusBar1.Panels[0].Text:='������ ������.'; exit; end;

    prButtonSet(False);

    if quDocsVnSel.RecordCount>0 then //���� ��� ������������
    begin
      if quDocsVnSelIACTIVE.AsInteger=1 then
      begin
        if MessageDlg('�������� �������� �'+quDocsVnSelNUMDOC.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin

          if (prTOFind(Trunc(quDocsVnSelDATEDOC.AsDateTime),quDocsVnSelIDSKL_FROM.AsInteger)=1)or
           (prTOFind(Trunc(quDocsVnSelDATEDOC.AsDateTime),quDocsVnSelIDSKL_TO.AsInteger)=1) then
          begin //�� ����
            if MessageDlg('������� ��� �������� ������ �� �� '+quDocsVnSelNAMEMH.AsString+','+quDocsVnSelNAMEMH1.AsString+'  � '+FormatDateTime('dd.mm.yyyy',quDocsVnSelDATEDOC.AsDateTime)+'?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
            begin
              prTODel(Trunc(quDocsVnSelDATEDOC.AsDateTime),quDocsVnSelIDSKL_FROM.AsInteger);
              prTODel(Trunc(quDocsVnSelDATEDOC.AsDateTime),quDocsVnSelIDSKL_TO.AsInteger);
            end
            else
            begin
              showmessage('��������� ������� ��������� ����������...');
              prButtonSet(True);
              exit;
            end;
          end;

         // 1 - ��������� ���� �� �������� � ��������� ������� ������� ���������, �� ������� �  ���������� �����
         // ���� ������ ��
          prFindPartOut.ParamByName('IDDOC').AsInteger:=quDocsVnSelID.AsInteger;
          prFindPartOut.ParamByName('DTYPE').AsInteger:=4;
          prFindPartOut.ExecProc;
          iCountPartOut:=prFindPartOut.ParamByName('RESULT').Value;
          if iCountPartOut>0 then
          begin
            if MessageDlg('� ������� ��������� ��������� '+IntToStr(iCountPartOut)+' ��������� ������. ����������?.',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
            begin
               bStart:=True;
//              showmessage('�� ������� ����������� ������������� � '+FormatDateTime('dd.mm.yyyy',quDocsVnSelDATEDOC.AsDateTime)+' �����.');
//              bStart:=False;
            end else
            begin
              bStart:=False;
            end;
          end else bStart:=True;
          if bStart then
          begin
            // 1 - �������� ��������� ������
            // 2 - ������� ��������� ��������� ������ �� ��������� c ���������� ��������������
            prPartInDel.ParamByName('IDDOC').AsInteger:=quDocsVnSelID.AsInteger;
            prPartInDel.ParamByName('DTYPE').AsInteger:=4;
            prPartInDel.ParamByName('IDATEINV').AsInteger:=Trunc(quDocsVnSelDATEDOC.AsDateTime);
            prPartInDel.ExecProc;

            //������� ��������� ������ �� ��������� � ���������� ��������������
            prDelPartOut.ParamByName('IDDOC').AsInteger:=quDocsVnSelID.AsInteger;
            prDelPartOut.ParamByName('DTYPE').AsInteger:=4;
            prDelPartOut.ExecProc;

            // 4 - �������� ������
            quDocsVnSel.Edit;
            quDocsVnSelIACTIVE.AsInteger:=0;
            quDocsVnSel.Post;
            quDocsVnSel.Refresh;
          end;
        end;
      end;
    end;
  end;
  prButtonSet(True);
end;

procedure TfmDocsVn.Timer1Timer(Sender: TObject);
begin
  if bClearDocIn=True then begin StatusBar1.Panels[0].Text:=''; bClearDocIn:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearDocIn:=True;
end;

procedure TfmDocsVn.acVidExecute(Sender: TObject);
begin
  //���
  with dmO do
  with dmORep do
  begin
    if LevelDocsVn.Visible then
    begin
      if CommonSet.DateTo>=iMaxDate then fmDocsVn.Caption:='����������� �� ������� � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
      else fmDocsVn.Caption:='����������� �� ������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);

      LevelDocsVn.Visible:=False;
      LevelCardsVn.Visible:=True;

      ViewCardsVn.BeginUpdate;
      quDocsInCard.Active:=False;
      quDocsInCard.ParamByName('DATEB').AsDate:=CommonSet.DateFrom;
      quDocsInCard.ParamByName('DATEE').AsDate:=CommonSet.DateTo;
      quDocsInCard.Active:=True;
      ViewCardsVn.EndUpdate;

      SpeedItem3.Visible:=False;
      SpeedItem4.Visible:=False;
      SpeedItem5.Visible:=False;
      SpeedItem6.Visible:=False;
      SpeedItem7.Visible:=False;
      SpeedItem8.Visible:=False;

    end else
    begin
      if CommonSet.DateTo>=iMaxDate then fmDocsVn.Caption:='���������� ��������� � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
      else fmDocsVn.Caption:='���������� ��������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);

      LevelDocsVn.Visible:=True;
      LevelCardsVn.Visible:=False;

      ViewDocsVn.BeginUpdate;
      quDocsVnSel.Active:=False;
      quDocsVnSel.ParamByName('DATEB').AsDate:=CommonSet.DateFrom;
      quDocsVnSel.ParamByName('DATEE').AsDate:=CommonSet.DateTo;
      quDocsVnSel.Active:=True;
      ViewDocsVn.EndUpdate;

      SpeedItem3.Visible:=True;
      SpeedItem4.Visible:=True;
      SpeedItem5.Visible:=True;
      SpeedItem6.Visible:=True;
      SpeedItem7.Visible:=True;
      SpeedItem8.Visible:=True;

    end;
  end;
end;

procedure TfmDocsVn.SpeedItem1Click0(Sender: TObject);
begin
  Close;
end;

procedure TfmDocsVn.acPrint1Execute(Sender: TObject);
begin
//������ �������
  if LevelDocsVn.Visible=False then exit;
  with dmORep do
  begin
    if quDocsVnSel.RecordCount>0 then //���� ��� �������������
    begin
      quSpecVnSel.Active:=False;
      quSpecVnSel.ParamByName('IDHD').AsInteger:=quDocsVnSelID.AsInteger;
      quSpecVnSel.Active:=True;

      frRepDocsVn.LoadFromFile(CurDir + 'ttn13.frf');

      frVariables.Variable['Num']:=quDocsVnSelNUMDOC.AsString;
      frVariables.Variable['sDate']:=FormatDateTime('dd.mm.yyyy',quDocsVnSelDATEDOC.AsDateTime);
      frVariables.Variable['FromMH']:=quDocsVnSelNAMEMH.AsString;
      frVariables.Variable['ToMH']:=quDocsVnSelNAMEMH1.AsString;
      frVariables.Variable['Depart']:=CommonSet.DepartName;

      frRepDocsVn.ReportName:='��������� �� �����������.';
      frRepDocsVn.PrepareReport;
      frRepDocsVn.ShowPreparedReport;

      quSpecVnSel.Active:=False;
    end else
    begin
      showmessage('�������� �������� ��� ������.');
    end;
  end;
end;

procedure TfmDocsVn.acCopyExecute(Sender: TObject);
var Par:Variant;
begin
  //����������
  with dmO do
  with dmORep do
  begin
    if quDocsVnSel.RecordCount=0 then exit;

    taHeadDoc.Active:=False;
    taHeadDoc.FileName:=CurDir+'HeadDoc.cds';
    if FileExists(CurDir+'HeadDoc.cds') then taHeadDoc.Active:=True
    else taHeadDoc.CreateDataSet;

    taSpecDoc.Active:=False;
    taSpecDoc.FileName:=CurDir+'SpecDoc.cds';
    if FileExists(CurDir+'SpecDoc.cds') then taSpecDoc.Active:=True
    else taSpecDoc.CreateDataSet;

    par := VarArrayCreate([0,1], varInteger);
    par[0]:=4;
    par[1]:=quDocsVnSelID.AsInteger;
    if taHeadDoc.Locate('IType;Id',par,[])=False then
    begin
      taHeadDoc.Append;
      taHeadDocIType.AsInteger:=4;
      taHeadDocId.AsInteger:=quDocsVnSelID.AsInteger;
      taHeadDocDateDoc.AsInteger:=Trunc(quDocsVnSelDATEDOC.AsDateTime);
      taHeadDocNumDoc.AsString:=quDocsVnSelNUMDOC.AsString;
      taHeadDocIdCli.AsInteger:=quDocsVnSelIDSKL_FROM.AsInteger;
      taHeadDocNameCli.AsString:=Copy(quDocsVnSelNAMEMH.AsString,1,70);
      taHeadDocIdSkl.AsInteger:=quDocsVnSelIDSKL_TO.AsInteger;
      taHeadDocNameSkl.AsString:=Copy(quDocsVnSelNAMEMH1.AsString,1,70);
      taHeadDocSumIN.AsFloat:=quDocsVnSelSUMIN.AsFloat;
      taHeadDocSumUch.AsFloat:=quDocsVnSelSUMUCH.AsFloat;
      taHeadDoc.Post;

      quSpecVnSel.Active:=False;
      quSpecVnSel.ParamByName('IDHD').AsInteger:=quDocsVnSelID.AsInteger;
      quSpecVnSel.Active:=True;

      quSpecVnSel.First;
      while not quSpecVnSel.Eof do
      begin
        taSpecDoc.Append;
        taSpecDocIType.AsInteger:=4;
        taSpecDocIdHead.AsInteger:=quSpecVnSelIDHEAD.AsInteger;
        taSpecDocNum.AsInteger:=quSpecVnSelNUM.AsInteger;
        taSpecDocIdCard.AsInteger:=quSpecVnSelIDCARD.AsInteger;
        taSpecDocQuant.AsFloat:=quSpecVnSelQUANT.AsFloat;
        taSpecDocPriceIn.AsFloat:=quSpecVnSelPRICEIN.AsFloat;
        taSpecDocSumIn.AsFloat:=quSpecVnSelSUMIN.AsFloat;
        taSpecDocPriceUch.AsFloat:=quSpecVnSelPRICEUCH.AsFloat;
        taSpecDocSumUch.AsFloat:=quSpecVnSelSUMUCH.AsFloat;
        taSpecDocIdNds.AsInteger:=0;
        taSpecDocSumNds.AsFloat:=0;
        taSpecDocNameC.AsString:=Copy(quSpecVnSelNAMEC.AsString,1,30);
        taSpecDocSm.AsString:=quSpecVnSelSM.AsString;
        taSpecDocIdM.AsInteger:=quSpecVnSelIDM.AsInteger;
        taSpecDocKm.AsFloat:=quSpecVnSelKM.AsFloat;
        taSpecDocPriceUch1.AsFloat:=quSpecVnSelPRICEUCH1.AsFloat;
        taSpecDocSumUch1.AsFloat:=quSpecVnSelSUMUCH1.AsFloat;
        taSpecDoc.Post;

        quSpecVnSel.Next;
      end;
    end else
    begin
      showmessage('�������� ��� ���� � ������.');
    end;
    taHeadDoc.Active:=False;
    taSpecDoc.Active:=False;
  end;
end;

procedure TfmDocsVn.acInsertDExecute(Sender: TObject);
begin
  // ��������
  with dmO do
  with dmORep do
  begin
    taHeadDoc.Active:=False;
    taHeadDoc.FileName:=CurDir+'HeadDoc.cds';
    if FileExists(CurDir+'HeadDoc.cds') then taHeadDoc.Active:=True
    else taHeadDoc.CreateDataSet;

    taSpecDoc.Active:=False;
    taSpecDoc.FileName:=CurDir+'SpecDoc.cds';
    if FileExists(CurDir+'SpecDoc.cds') then taSpecDoc.Active:=True
    else taSpecDoc.CreateDataSet;

    fmTBuff:=TfmTBuff.Create(Application);

    fmTBuff.LevelTH.Visible:=False;
    fmTBuff.LevelTS.Visible:=False;
    fmTBuff.LevelD.Visible:=True;
    fmTBuff.LevelDSpec.Visible:=True;

    fmTBuff.ShowModal;
    if fmTBuff.ModalResult=mrOk then
    begin //���������
      fmTBuff.Release;
      if taHeadDoc.RecordCount>0 then
      begin
        if CanDo('prAddDocVn') then
        begin
          prAllViewOff;

          fmAddDoc3.Caption:='���������: ����� ��������.';
          fmAddDoc3.cxTextEdit1.Text:=prGetNum(4,0);
          fmAddDoc3.cxTextEdit1.Tag:=0;
          fmAddDoc3.cxTextEdit1.Properties.ReadOnly:=False;
          fmAddDoc3.cxDateEdit1.Date:=Date;
          fmAddDoc3.cxDateEdit1.Properties.ReadOnly:=False;

          if quMHAll.Active=False then quMHAll.Active:=True;
          quMHAll.FullRefresh;
          //�� ����
          if  taHeadDocIType.AsInteger=4 then
          begin
            fmAddDoc3.cxLookupComboBox2.EditValue:=taHeadDocIdCli.AsInteger;
            fmAddDoc3.cxLookupComboBox2.Text:=taHeadDocNameCli.AsString;
            fmAddDoc3.cxLookupComboBox2.Properties.ReadOnly:=False;
          end
          else
          begin
            fmAddDoc3.cxLookupComboBox2.EditValue:=taHeadDocIdSkl.AsInteger;
            fmAddDoc3.cxLookupComboBox2.Text:=taHeadDocNameSkl.AsString;
            fmAddDoc3.cxLookupComboBox2.Properties.ReadOnly:=False;
          end;

          //����
          fmAddDoc3.cxLookupComboBox1.EditValue:=taHeadDocIdSkl.AsInteger;
          fmAddDoc3.cxLookupComboBox1.Text:=taHeadDocNameSkl.AsString;
          fmAddDoc3.cxLookupComboBox1.Properties.ReadOnly:=False;

          if (quMHAll.Locate('ID',taHeadDocIdCli.AsInteger,[]))and(taHeadDocIType.AsInteger=4) then
          begin
            //�� ����
            fmAddDoc3.Label2.Caption:='��. ����: '+quMHAllNAMEPRICE.AsString;
            fmAddDoc3.Label2.Tag:=quMHAllDEFPRICE.AsInteger;
          end else
          begin
           //�� ����
            fmAddDoc3.Label2.Caption:='��. ����: ';
            fmAddDoc3.Label2.Tag:=0;
          end;

          if quMHAll.Locate('ID',taHeadDocIdSkl.AsInteger,[]) then
          begin
           //����
            fmAddDoc3.Label15.Caption:='��. ����: '+quMHAllNAMEPRICE.AsString;
            fmAddDoc3.Label15.Tag:=quMHAllDEFPRICE.AsInteger;
          end
          else
          begin
            //����
            fmAddDoc3.Label15.Caption:='��. ����: ';
            fmAddDoc3.Label15.Tag:=0;
          end;

          fmAddDoc3.cxLabel1.Enabled:=True;
          fmAddDoc3.cxLabel2.Enabled:=True;
          fmAddDoc3.cxLabel3.Enabled:=True;
          fmAddDoc3.cxLabel4.Enabled:=True;
          fmAddDoc3.cxLabel5.Enabled:=True;
          fmAddDoc3.cxLabel6.Enabled:=True;
          fmAddDoc3.N1.Enabled:=True;
          fmAddDoc3.cxLookupComboBox2.Properties.ReadOnly:=False;
          fmAddDoc3.cxLookupComboBox1.Properties.ReadOnly:=False;
          fmAddDoc3.cxButton1.Enabled:=True;
          fmAddDoc3.cxButton1.Visible:=True;

          fmAddDoc3.ViewDoc3.OptionsData.Editing:=True;
          fmAddDoc3.ViewDoc3.OptionsData.Deleting:=True;
          CloseTa(fmAddDoc3.taSpec);

          fmAddDoc3.ViewTara.OptionsData.Editing:=True;
          fmAddDoc3.ViewTara.OptionsData.Deleting:=True;
          CloseTa(fmAddDoc3.taTara);

          taSpecDoc.First;

          while not taSpecDoc.Eof do
          begin
            if (taSpecDocIType.AsInteger=taHeadDocIType.AsInteger)and(taSpecDocIdHead.AsInteger=taHeadDocId.AsInteger) then
            begin
              with fmAddDoc3 do
              begin
                iCol:=0;

                taSpec.Append;
                taSpecNum.AsInteger:=taSpecDocNum.AsInteger;
                taSpecIdGoods.AsInteger:=taSpecDocIdCard.AsInteger;
                taSpecNameG.AsString:=taSpecDocNameC.AsString;
                taSpecIM.AsInteger:=taSpecDocIdM.AsInteger;
                taSpecSM.AsString:=taSpecDocSm.AsString;
                taSpecKm.AsFloat:=taSpecDocKm.AsFloat;
                taSpecQuant.AsFloat:=RoundEx(taSpecDocQuant.AsFloat*1000)/1000;
                taSpecPrice1.AsFloat:=taSpecDocPriceIn.AsFloat;
                taSpecSum1.AsFloat:=taSpecDocSumIn.AsFloat;
                taSpecPrice2.AsFloat:=taSpecDocPriceUch.AsFloat;
                taSpecSum2.AsFloat:=taSpecDocSumUch.AsFloat;
                taSpecPrice3.AsFloat:=taSpecDocPriceUch1.AsFloat;
                taSpecSum3.AsFloat:=taSpecDocSumUch1.AsFloat;
                taSpecSumNac.AsFloat:=taSpecDocSumUch1.AsFloat-taSpecDocSumUch.AsFloat;
                taSpecProcNac.AsFloat:=0;
                if taSpecDocSumUch.AsFloat<>0 then taSpecProcNac.AsFloat:=RoundEx((taSpecDocSumUch1.AsFloat-taSpecDocSumUch.AsFloat)/taSpecDocSumUch.AsFloat*10000)/100;
                taSpecRemn.AsFloat:=0;

           {    taSpecIdGoods.AsInteger:=1;
                taSpecNameG.AsString:='vbvbvb';
                taSpecIM.AsInteger:=1;
                taSpecSM.AsString:='ru';
                taSpecKm.AsFloat:=1;
                taSpecQuant.AsFloat:=1;
                taSpecPrice1.AsFloat:=1;
                taSpecSum1.AsFloat:=1;
                taSpecPrice2.AsFloat:=1;
                taSpecSum2.AsFloat:=1;
                taSpecPrice3.AsFloat:=1;
                taSpecSum3.AsFloat:=1;
                taSpecSumNac.AsFloat:=1;
                taSpecProcNac.AsFloat:=0;
                taSpecProcNac.AsFloat:=1;
            }
                taSpec.Post;
              end;
            end;
            taSpecDoc.Next;
          end;

          taHeadDoc.Active:=False;
          taSpecDoc.Active:=False;

          prAllViewOn;

          fmAddDoc3.ShowModal;
        end else showmessage('��� ����.');
      end;
    end else
    begin
      fmTBuff.Release;
      taHeadDoc.Active:=False;
      taSpecDoc.Active:=False;
    end;
  end;
end;

procedure TfmDocsVn.FormShow(Sender: TObject);
begin
  Memo1.Clear;
end;

procedure TfmDocsVn.Excel1Click(Sender: TObject);
begin
  prNExportExel4(ViewDocsVn);
end;

end.
