unit Calc1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxControls, cxContainer, cxEdit, cxTextEdit, cxMaskEdit,
  cxDropDownEdit, cxCalc, ExtCtrls, cxLookAndFeelPainters, StdCtrls,
  cxButtons, ActnList, XPStyleActnCtrls, ActnMan, Menus;

type
  TfmCalc1 = class(TForm)
    Panel1: TPanel;
    CalcEdit1: TcxCalcEdit;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    cxButton5: TcxButton;
    cxButton6: TcxButton;
    cxButton7: TcxButton;
    cxButton8: TcxButton;
    cxButton9: TcxButton;
    cxButton10: TcxButton;
    cxButton11: TcxButton;
    cxButton12: TcxButton;
    cxButton13: TcxButton;
    cxButton14: TcxButton;
    cxButton15: TcxButton;
    amCalc: TActionManager;
    Action1: TAction;
    Action2: TAction;
    Action3: TAction;
    Action4: TAction;
    Action5: TAction;
    Action6: TAction;
    Action7: TAction;
    Action8: TAction;
    Action9: TAction;
    Action10: TAction;
    Action11: TAction;
    Action12: TAction;
    Action13: TAction;
    Action14: TAction;
    Action15: TAction;
    acExit: TAction;
    cxButton16: TcxButton;
    acAddToSpec: TAction;
    cxButton17: TcxButton;
    Label1: TLabel;
    cxButton18: TcxButton;
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
    procedure cxButton7Click(Sender: TObject);
    procedure cxButton8Click(Sender: TObject);
    procedure cxButton9Click(Sender: TObject);
    procedure cxButton10Click(Sender: TObject);
    procedure cxButton11Click(Sender: TObject);
    procedure Action1Execute(Sender: TObject);
    procedure Action2Execute(Sender: TObject);
    procedure Action3Execute(Sender: TObject);
    procedure Action4Execute(Sender: TObject);
    procedure Action5Execute(Sender: TObject);
    procedure Action6Execute(Sender: TObject);
    procedure Action7Execute(Sender: TObject);
    procedure Action8Execute(Sender: TObject);
    procedure Action9Execute(Sender: TObject);
    procedure Action10Execute(Sender: TObject);
    procedure Action11Execute(Sender: TObject);
    procedure Action12Execute(Sender: TObject);
    procedure Action13Execute(Sender: TObject);
    procedure Action14Execute(Sender: TObject);
    procedure Action15Execute(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxButton18Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmCalc1: TfmCalc1;
  bFirst:Boolean;

implementation

uses Un1;

{$R *.dfm}

procedure TfmCalc1.cxButton1Click(Sender: TObject);
begin
  Action1.Execute;
end;

procedure TfmCalc1.cxButton2Click(Sender: TObject);
begin
  Action2.Execute;
end;

procedure TfmCalc1.cxButton3Click(Sender: TObject);
begin
  Action3.Execute;
end;

procedure TfmCalc1.cxButton4Click(Sender: TObject);
begin
  Action4.Execute;
end;

procedure TfmCalc1.cxButton5Click(Sender: TObject);
begin
  Action5.Execute;
end;

procedure TfmCalc1.cxButton6Click(Sender: TObject);
begin
  Action6.Execute;
end;

procedure TfmCalc1.cxButton7Click(Sender: TObject);
begin
  Action7.Execute;
end;

procedure TfmCalc1.cxButton8Click(Sender: TObject);
begin
  Action8.Execute;
end;

procedure TfmCalc1.cxButton9Click(Sender: TObject);
begin
  Action9.Execute;
end;

procedure TfmCalc1.cxButton10Click(Sender: TObject);
begin
  Action10.Execute;
end;

procedure TfmCalc1.cxButton11Click(Sender: TObject);
begin
  Action11.Execute;
end;

procedure TfmCalc1.Action1Execute(Sender: TObject);
begin
  if pos(',',CalcEdit1.Text)=0 then CalcEdit1.EditValue:=CalcEdit1.EditValue*10+1
  else CalcEdit1.Text:=CalcEdit1.Text+'1';
  if bFirst then
  begin
    CalcEdit1.EditValue:=1;
    bFirst:=False;
  end;
end;

procedure TfmCalc1.Action2Execute(Sender: TObject);
begin
  if pos(',',CalcEdit1.Text)=0 then CalcEdit1.EditValue:=CalcEdit1.EditValue*10+2
  else CalcEdit1.Text:=CalcEdit1.Text+'2';
  if bFirst then
  begin
    CalcEdit1.EditValue:=2;
    bFirst:=False;
  end;
end;

procedure TfmCalc1.Action3Execute(Sender: TObject);
begin
  if pos(',',CalcEdit1.Text)=0 then CalcEdit1.EditValue:=CalcEdit1.EditValue*10+3
  else CalcEdit1.Text:=CalcEdit1.Text+'3';
  if bFirst then
  begin
    CalcEdit1.EditValue:=3;
    bFirst:=False;
  end;
end;

procedure TfmCalc1.Action4Execute(Sender: TObject);
begin
  if pos(',',CalcEdit1.Text)=0 then CalcEdit1.EditValue:=CalcEdit1.EditValue*10+4
  else CalcEdit1.Text:=CalcEdit1.Text+'4';
  if bFirst then
  begin
    CalcEdit1.EditValue:=4;
    bFirst:=False;
  end;
end;

procedure TfmCalc1.Action5Execute(Sender: TObject);
begin
  if pos(',',CalcEdit1.Text)=0 then CalcEdit1.EditValue:=CalcEdit1.EditValue*10+5
  else CalcEdit1.Text:=CalcEdit1.Text+'5';
  if bFirst then
  begin
    CalcEdit1.EditValue:=5;
    bFirst:=False;
  end;
end;

procedure TfmCalc1.Action6Execute(Sender: TObject);
begin
  if pos(',',CalcEdit1.Text)=0 then CalcEdit1.EditValue:=CalcEdit1.EditValue*10+6
  else CalcEdit1.Text:=CalcEdit1.Text+'6';
  if bFirst then
  begin
    CalcEdit1.EditValue:=6;
    bFirst:=False;
  end;
end;

procedure TfmCalc1.Action7Execute(Sender: TObject);
begin
  if pos(',',CalcEdit1.Text)=0 then CalcEdit1.EditValue:=CalcEdit1.EditValue*10+7
  else CalcEdit1.Text:=CalcEdit1.Text+'7';
  if bFirst then
  begin
    CalcEdit1.EditValue:=7;
    bFirst:=False;
  end;
end;

procedure TfmCalc1.Action8Execute(Sender: TObject);
begin
  if pos(',',CalcEdit1.Text)=0 then CalcEdit1.EditValue:=CalcEdit1.EditValue*10+8
  else CalcEdit1.Text:=CalcEdit1.Text+'8';
  if bFirst then
  begin
    CalcEdit1.EditValue:=8;
    bFirst:=False;
  end;
end;

procedure TfmCalc1.Action9Execute(Sender: TObject);
begin
  if pos(',',CalcEdit1.Text)=0 then CalcEdit1.EditValue:=CalcEdit1.EditValue*10+9
  else CalcEdit1.Text:=CalcEdit1.Text+'9';
  if bFirst then
  begin
    CalcEdit1.EditValue:=9;
    bFirst:=False;
  end;
end;

procedure TfmCalc1.Action10Execute(Sender: TObject);
begin
  if pos(',',CalcEdit1.Text)=0 then CalcEdit1.EditValue:=CalcEdit1.EditValue*10
  else CalcEdit1.Text:=CalcEdit1.Text+'0';
  if bFirst then
  begin
    CalcEdit1.EditValue:=0;
    bFirst:=False;
  end;
end;

procedure TfmCalc1.Action11Execute(Sender: TObject);
begin
  if pos(',',CalcEdit1.Text)=0 then CalcEdit1.Text:=CalcEdit1.Text+',';
  if bFirst then
  begin
    bFirst:=False;
  end;
end;

procedure TfmCalc1.Action12Execute(Sender: TObject);
begin
  CalcEdit1.Value:=0;
  if bFirst then
  begin
    CalcEdit1.EditValue:=0;
    bFirst:=False;
  end;
end;

procedure TfmCalc1.Action13Execute(Sender: TObject);
begin
  CalcEdit1.EditValue:=CalcEdit1.EditValue+1;
  if bFirst then
  begin
    bFirst:=False;
  end;
end;

procedure TfmCalc1.Action14Execute(Sender: TObject);
Var rQ:Real;
begin
  rQ:=CalcEdit1.EditValue-1;
  if rQ >=0 then  CalcEdit1.EditValue:=rQ;
  if bFirst then
  begin
    bFirst:=False;
  end;
end;

procedure TfmCalc1.Action15Execute(Sender: TObject);
begin
  CalcEdit1.EditValue:=0;
  if bFirst then
  begin
    bFirst:=False;
  end;
end;

procedure TfmCalc1.acExitExecute(Sender: TObject);
begin
  ModalResult:=mrCancel;
end;

procedure TfmCalc1.FormShow(Sender: TObject);
begin
  bFirst:=True;
  if bAddPosFromMenu or bAddPosFromSpec then cxButton16.Visible:=True
  else cxButton16.Visible:=False;

  if bScale then Label1.Caption:='���� ��' else Label1.Caption:='����� ���';
 { with dmC1 do
  begin
    if bScale then //���� ������� ����
    begin
      TimerVes.Enabled:=True;
    end;
  end;}
end;

{

Var n: Integer;
    Buff:array[1..3] of char;
    rVes:Real;
    S1,S2:String;
begin
  sScanEv:='';
  iReadCount:=0;
  n:=0;

  Buff[1]:=#$05;
  devCas.WriteBuf(Buff,1);
  delay(5);
  Buff[1]:=#$12;
  devCas.WriteBuf(Buff,1);

  while (iReadCount<38)and(n<10) do
  begin
    delay(50);
    inc(n);
  end;
  Memo1.Lines.Add('['+sScanEv+']'+'   '+IntToStr(iReadCount)+'  '+IntToStr(n));
  rVes:=0;
  if pos('kg',sScanEv)>0 then
  begin
    S1:=Copy(sScanEv,pos('kg',sScanEv)-8,8);
    S2:=S1[1];
    delete(S1,1,1);
    while pos(' ',S1)>0 do delete(S1,pos(' ',S1),1);
    while pos('.',S1)>0 do S1[pos('.',S1)]:=',';
    rVes:=StrToFloatDef(S1,0);
  end;
  cxCalcEdit1.Value:=rVes;
  Label1.Caption:=S2;
end;

}


procedure TfmCalc1.cxButton18Click(Sender: TObject);
begin
  CalcEdit1.Value:=cxButton18.Tag/1000;
end;

end.
