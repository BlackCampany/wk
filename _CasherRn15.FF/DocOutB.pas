unit DocOutB;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SpeedBar, ExtCtrls, ComCtrls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Placemnt, cxImageComboBox, XPStyleActnCtrls, ActnList, ActnMan,
  cxCurrencyEdit, cxContainer, cxTextEdit, cxMemo, FR_Class, FR_DSet,
  FR_DBSet;

type
  TfmDocsOutB = class(TForm)
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    Timer1: TTimer;
    FormPlacement1: TFormPlacement;
    GridDocsOutB: TcxGrid;
    ViewDocsOutB: TcxGridDBTableView;
    LevelDocsOutB: TcxGridLevel;
    ViewDocsOutBID: TcxGridDBColumn;
    ViewDocsOutBDATEDOC: TcxGridDBColumn;
    ViewDocsOutBNUMDOC: TcxGridDBColumn;
    ViewDocsOutBIDSKL: TcxGridDBColumn;
    ViewDocsOutBSUMIN: TcxGridDBColumn;
    ViewDocsOutBSUMUCH: TcxGridDBColumn;
    ViewDocsOutBNAMEMH: TcxGridDBColumn;
    ViewDocsOutBIACTIVE: TcxGridDBColumn;
    amDocsIn: TActionManager;
    acPeriod: TAction;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    SpeedItem6: TSpeedItem;
    acAddDoc1: TAction;
    acEditDoc1: TAction;
    acViewDoc1: TAction;
    acDelDoc1: TAction;
    acOnDoc1: TAction;
    acOffDoc1: TAction;
    SpeedItem7: TSpeedItem;
    SpeedItem8: TSpeedItem;
    ViewDocsOutBOPER: TcxGridDBColumn;
    acAddDocManual: TAction;
    acSebBl: TAction;
    SpeedItem9: TSpeedItem;
    SpeedItem10: TSpeedItem;
    Memo1: TcxMemo;
    acPrintMin: TAction;
    RepDocOutB: TfrReport;
    frquDocOutBPrint: TfrDBDataSet;
    SpeedItem11: TSpeedItem;
    procedure SpeedItem1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acPeriodExecute(Sender: TObject);
    procedure acAddDoc1Execute(Sender: TObject);
    procedure acEditDoc1Execute(Sender: TObject);
    procedure acViewDoc1Execute(Sender: TObject);
    procedure ViewDocsOutBDblClick(Sender: TObject);
    procedure acDelDoc1Execute(Sender: TObject);
    procedure acOnDoc1Execute(Sender: TObject);
    procedure acOffDoc1Execute(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure acAddDocManualExecute(Sender: TObject);
    procedure acSebBlExecute(Sender: TObject);
    procedure acAddDocBExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure acPrintMinExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

procedure prSetDOBSpecPar(sOper:String;iType:Integer);

var
  fmDocsOutB: TfmDocsOutB;
  bClearDocOutB:Boolean = false;
  bStartOpen:Boolean = false;

implementation

uses Un1, dmOffice, PeriodUni, AddDoc1, Period3, DOBSpec, SelPartIn1,
  DMOReps;

{$R *.dfm}

procedure prSetDOBSpecPar(sOper:String;iType:Integer);
begin
  with dmO do
  with dmORep do
  with fmDobSpec do
  begin
    cxTextEdit1.Text:=quDocsOutBNUMDOC.AsString;
    cxTextEdit2.Text:=quDocsOutBOPER.AsString;


    if quMHALL.Active=False then quMHALL.Active:=True;

    CurVal.IdMH:=quDocsOutBIDSKL.AsInteger;
    CurVal.NAMEMH:=quDocsOutBNAMEMH.AsString;

    if CurVal.IdMH<>0 then
    begin
      cxLookupComboBox1.EditValue:=CurVal.IdMH;
      cxLookupComboBox1.Text:=CurVal.NAMEMH;
    end else
    begin
       quMHAll.First;
       if not quMHAll.Eof then
       begin
         CurVal.IdMH:=quMHAllID.AsInteger;
         CurVal.NAMEMH:=quMHAllNAMEMH.AsString;
         cxLookupComboBox1.EditValue:=CurVal.IdMH;
         cxLookupComboBox1.Text:=CurVal.NAMEMH;
       end;
    end;


    cxDateEdit1.Date:=quDocsOutBDATEDOC.AsDateTime;

    if iType=1 then //��������������
    begin
      cxButton2.Visible:=True;
      cxButton2.Enabled:=True;

      if (sOper ='Del')
         or (sOper ='Sale')
         or (sOper ='SaleBank')
         or (sOper ='SalePC')
         or (sOper ='Ret')
         or (sOper ='RetBank') then //���������������� ���������
      begin
        cxTextEdit1.Properties.ReadOnly:=False;
        cxTextEdit2.Properties.ReadOnly:=True;
        cxLookupComboBox1.Properties.ReadOnly:=False;
        cxDateEdit1.Properties.ReadOnly:=False;

        ViewBSIFR.Editing:=False;
        ViewBNAMEB.Editing:=False;
        ViewBCODEB.Editing:=True; ViewBCODEB.Properties.ReadOnly:=False;
        ViewBKB.Editing:=True;
        ViewBQUANT.Editing:=False;
        ViewBPRICER.Editing:=False;
        ViewBDSUM.Editing:=False;
        ViewBRSUM.Editing:=False;
        ViewBIDCARD.Editing:=True; ViewBIDCARD.Properties.ReadOnly:=False;
        ViewBNAME.Editing:=True; ViewBNAME.Properties.ReadOnly:=False;
        ViewBNAMESHORT.Editing:=False;
        ViewBTCARD.Editing:=False;
        ViewBKM.Editing:=False;

        image1.Visible:=False;
        image2.Visible:=False;
        Label11.Visible:=False;
        Label12.Visible:=False;
      end else
      begin
        cxTextEdit1.Properties.ReadOnly:=False;
        cxTextEdit2.Properties.ReadOnly:=False;
        cxLookupComboBox1.Properties.ReadOnly:=False;
        cxDateEdit1.Properties.ReadOnly:=False;

        ViewBSIFR.Editing:=False;
        ViewBNAMEB.Editing:=False;
        ViewBCODEB.Editing:=True; ViewBCODEB.Properties.ReadOnly:=False;
        ViewBKB.Editing:=True;
        ViewBQUANT.Editing:=True;
        ViewBPRICER.Editing:=True;
        ViewBDSUM.Editing:=True;
        ViewBRSUM.Editing:=True;
        ViewBIDCARD.Editing:=True; ViewBIDCARD.Properties.ReadOnly:=False;
        ViewBNAME.Editing:=True; ViewBNAME.Properties.ReadOnly:=False;
        ViewBNAMESHORT.Editing:=False;
        ViewBTCARD.Editing:=False;
        ViewBKM.Editing:=False;

        image1.Visible:=True;
        image2.Visible:=True;
        Label11.Visible:=True;
        Label12.Visible:=True;
      end;
    end;
    if iType=2 then //��������
    begin
      cxTextEdit1.Properties.ReadOnly:=True;
      cxTextEdit2.Properties.ReadOnly:=True;
      cxLookupComboBox1.Properties.ReadOnly:=True;
      cxDateEdit1.Properties.ReadOnly:=True;

      ViewBSIFR.Editing:=False;
      ViewBNAMEB.Editing:=False;
      ViewBCODEB.Editing:=False; ViewBCODEB.Properties.ReadOnly:=True;
      ViewBKB.Editing:=False;
      ViewBQUANT.Editing:=False;
      ViewBPRICER.Editing:=False;
      ViewBDSUM.Editing:=False;
      ViewBRSUM.Editing:=False;
      ViewBIDCARD.Editing:=False; ViewBIDCARD.Properties.ReadOnly:=True;
      ViewBNAME.Editing:=False; ViewBNAME.Properties.ReadOnly:=True;
      ViewBNAMESHORT.Editing:=False;
      ViewBTCARD.Editing:=False;
      ViewBKM.Editing:=False;

      cxButton2.Visible:=False;
      cxButton2.Enabled:=False;

      image1.Visible:=False;
      image2.Visible:=False;
      Label11.Visible:=False;
      Label12.Visible:=False;
    end;
  end;
end;


procedure TfmDocsOutB.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmDocsOutB.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  Timer1.Enabled:=True;
  GridDocsOutB.Align:=AlClient;
  StatusBar1.Color:=$00FFCACA;
  ViewDocsOutB.RestoreFromIniFile(CurDir+GridIni);
  Memo1.Clear;
end;

procedure TfmDocsOutB.FormClose(Sender: TObject; var Action: TCloseAction);
begin
   ViewDocsOutB.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmDocsOutB.acPeriodExecute(Sender: TObject);
begin
//  ������
  fmPeriodUni.DateTimePicker1.Date:=CommonSet.DateFrom;
  fmPeriodUni.DateTimePicker2.Date:=CommonSet.DateTo-1;
  fmPeriodUni.ShowModal;
  if fmPeriodUni.ModalResult=mrOk then
  begin
    CommonSet.DateFrom:=Trunc(fmPeriodUni.DateTimePicker1.Date);
    CommonSet.DateTo:=Trunc(fmPeriodUni.DateTimePicker2.Date)+1;

    if CommonSet.DateTo>=iMaxDate then fmDocsOutB.Caption:='���������� � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
    else fmDocsOutB.Caption:='���������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);
    with dmO do
    begin
      ViewDocsOutB.BeginUpdate;
      quDocsOutB.Active:=False;
      quDocsOutB.ParamByName('DATEB').AsDate:=CommonSet.DateFrom;
      quDocsOutB.ParamByName('DATEE').AsDate:=CommonSet.DateTo;
      quDocsOutB.Active:=True;
      ViewDocsOutB.EndUpdate;
    end;
  end;
end;

procedure TfmDocsOutB.acAddDoc1Execute(Sender: TObject);
begin
  //�������� ��������
  if not CanDo('prTakeDocOutB') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;

  fmSelPerSkl1:=tfmSelPerSkl1.Create(Application);
  with fmSelPerSkl1 do
  begin
    if CommonSet.DateTo>=iMaxDate-1 then CommonSet.DateTo:=Date;
    cxDateEdit1.Date:=CommonSet.DateFrom;
    cxDateEdit2.Date:=CommonSet.DateTo;
    cxTimeEdit1.Time:=StrToTimeDef(CommonSet.ZTimeShift,0);
    cxTimeEdit2.Time:=StrToTimeDef(CommonSet.ZTimeShift,0);
    quMHAll1.Active:=False;
    quMHAll1.Active:=True;
    if CommonSet.IdStore>0 then quMHAll1.Locate('ID',CommonSet.IdStore,[])
                           else quMHAll1.First;
    cxLookupComboBox1.EditValue:=quMHAll1ID.AsInteger;
    Label4.Caption:='';
  end;
  fmSelPerSkl1.ShowModal;
  if fmSelPerSkl1.ModalResult=mrOk then
  begin
    ViewDocsOutB.BeginUpdate;
    dmO.quDocsOutB.Active:=False;
    dmO.quDocsOutB.Active:=True;
    ViewDocsOutB.EndUpdate;
  end;
  fmSelPerSkl1.quMHAll1.Active:=False;
  fmSelPerSkl1.Release;
end;

procedure TfmDocsOutB.acEditDoc1Execute(Sender: TObject);
begin
  //�������������
  if bStartOpen then
  begin
    Memo1.Lines.Add('  ������� ��� �������, ���������.');Delay(10);
    exit; //�� �������� �������
  end;
  if not CanDo('prEditDocOutB') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  with dmO do
  with dmORep do
  begin
    if quDocsOutB.RecordCount>0 then
    begin
      if quDocsOutBIACTIVE.AsInteger=0 then
      begin
        bStartOpen:=True;
        prSetDOBSpecPar(quDocsOutBOPER.AsString,1); //�������������� 1

        Memo1.Clear;
        Memo1.Lines.Add('�����, ���� �������� ���������.');Delay(10);
        Memo1.Lines.Add('   ������ ����.');Delay(10);
        taDobSpec.Active:=False;
        taDobSpec.ParamByName('IDH').AsInteger:=quDocsOutBID.AsInteger;
        taDobSpec.Active:=True;

        Memo1.Lines.Add('   ������ �������.');Delay(10);
        taCalc.Active:=False;
        taCalc.CreateDataSet;

        taCalcB.Active:=False;
        taCalcB.CreateDataSet;

        taDobSpec1.Active:=False;
        taDobSpec1.ParamByName('IDH').AsInteger:=quDocsOutBID.AsInteger;
        taDobSpec1.Active:=True;
        taDobSpec1.First;
        while not taDobSpec1.Eof do
        begin
          taCalc.Append;
          taCalcArticul.AsInteger:=taDobSpec1ARTICUL.AsInteger;
          taCalcName.AsString:=taDobSpec1NAME.AsString;
          taCalcIdm.AsInteger:=taDobSpec1IDM.AsInteger;
          taCalcsM.AsString:=taDobSpec1SM.AsString;
          taCalcKm.AsFloat:=taDobSpec1KM.AsFloat;
          taCalcQuant.AsFloat:=taDobSpec1QUANT.AsFloat;
          taCalcQuantFact.AsFloat:=0;
          taCalcQuantDiff.AsFloat:=0;
          taCalcSumIn.AsFloat:=taDobSpec1SUMIN.AsFloat;
          taCalcSumUch.AsFloat:=0;

          taCalc.Post;

          taDobSpec1.Next;
        end;
        taDobSpec1.Active:=False;

        Memo1.Lines.Add('   ����������� ����.');Delay(10);

        taDobSpec2.Active:=False;
        taDobSpec2.ParamByName('IDH').AsInteger:=quDocsOutBID.AsInteger;
        taDobSpec2.Active:=True;
        taDobSpec2.First;
        while not taDobSpec2.Eof do
        begin
          taCalcB.Append;
          taCalcBID.AsInteger:=taDobSpec2IDB.AsInteger;
          taCalcBCODEB.AsInteger:=taDobSpec2CODEB.AsInteger;
          taCalcBNAMEB.AsString:=taDobSpec2NAMEB.AsString;
          taCalcBQUANT.AsFloat:=taDobSpec2QUANT.AsFloat;
          taCalcBPRICEOUT.AsFloat:=taDobSpec2PRICEOUT.AsFloat;
          taCalcBSUMOUT.AsFloat:=taDobSpec2SUMOUT.AsFloat;
          taCalcBIDCARD.AsInteger:=taDobSpec2IDCARD.AsInteger;
          taCalcBNAMEC.AsString:=taDobSpec2NAMEC.AsString;
          taCalcBQUANTC.AsFloat:=taDobSpec2QUANTC.AsFloat;
          taCalcBPRICEIN.AsFloat:=taDobSpec2PRICEIN.AsFloat;
          taCalcBSUMIN.AsFloat:=taDobSpec2SUMIN.AsFloat;
          taCalcBIM.AsInteger:=taDobSpec2IM.AsInteger;
          taCalcBSM.AsString:=taDobSpec2SM.AsString;
          taCalcBSB.AsString:=taDobSpec2SB.AsString;
          taCalcB.Post;

          taDobSpec2.Next;
        end;
        taDobSpec1.Active:=False;
        taDobSpec2.Active:=False;
        Memo1.Lines.Add('������.');Delay(10);

        bStartOpen:=False;
        fmDobSpec.ShowModal;

        taCalc.Active:=False;
        taCalcB.Active:=False;
      end else showmessage('������������� �������������� �������� ������.');
    end;
  end;
end;

procedure TfmDocsOutB.acViewDoc1Execute(Sender: TObject);
begin
  //��������
  if bStartOpen then
  begin
    Memo1.Lines.Add('  ������� ��� �������, ���������.');Delay(10);
    exit; //�� �������� �������
  end;
  if not CanDo('prViewDocOutB') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  with dmO do
  with dmORep do
  begin
    if quDocsOutB.RecordCount>0 then
    begin
      bStartOpen:=True;
      prSetDOBSpecPar(quDocsOutBOPER.AsString,2); //2 - ��������

      Memo1.Clear;
      Memo1.Lines.Add('�����, ���� �������� ���������.');Delay(10);
      Memo1.Lines.Add('   ������ ����.');Delay(10);
      taDobSpec.Active:=False;
      taDobSpec.ParamByName('IDH').AsInteger:=quDocsOutBID.AsInteger;
      taDobSpec.Active:=True;

      Memo1.Lines.Add('   ������ �������.');Delay(10);
      taCalc.Active:=False;
      taCalc.CreateDataSet;

      taCalcB.Active:=False;
      taCalcB.CreateDataSet;

      taDobSpec1.Active:=False;
      taDobSpec1.ParamByName('IDH').AsInteger:=quDocsOutBID.AsInteger;
      taDobSpec1.Active:=True;
      taDobSpec1.First;
      while not taDobSpec1.Eof do
      begin
        taCalc.Append;
        taCalcArticul.AsInteger:=taDobSpec1ARTICUL.AsInteger;
        taCalcName.AsString:=taDobSpec1NAME.AsString;
        taCalcIdm.AsInteger:=taDobSpec1IDM.AsInteger;
        taCalcsM.AsString:=taDobSpec1SM.AsString;
        taCalcKm.AsFloat:=taDobSpec1KM.AsFloat;
        taCalcQuant.AsFloat:=taDobSpec1QUANT.AsFloat;
        taCalcQuantFact.AsFloat:=0;
        taCalcQuantDiff.AsFloat:=0;
        taCalcSumIn.AsFloat:=taDobSpec1SUMIN.AsFloat;
        taCalcSumUch.AsFloat:=0;

        taCalc.Post;

        taDobSpec1.Next;
      end;
      taDobSpec1.Active:=False;

      Memo1.Lines.Add('   ����������� ����.');Delay(10);

      taDobSpec2.Active:=False;
      taDobSpec2.ParamByName('IDH').AsInteger:=quDocsOutBID.AsInteger;
      taDobSpec2.Active:=True;
      taDobSpec2.First;
      while not taDobSpec2.Eof do
      begin
        taCalcB.Append;
        taCalcBID.AsInteger:=taDobSpec2IDB.AsInteger;
        taCalcBCODEB.AsInteger:=taDobSpec2CODEB.AsInteger;
        taCalcBNAMEB.AsString:=taDobSpec2NAMEB.AsString;
        taCalcBQUANT.AsFloat:=taDobSpec2QUANT.AsFloat;
        taCalcBPRICEOUT.AsFloat:=taDobSpec2PRICEOUT.AsFloat;
        taCalcBSUMOUT.AsFloat:=taDobSpec2SUMOUT.AsFloat;
        taCalcBIDCARD.AsInteger:=taDobSpec2IDCARD.AsInteger;
        taCalcBNAMEC.AsString:=taDobSpec2NAMEC.AsString;
        taCalcBQUANTC.AsFloat:=taDobSpec2QUANTC.AsFloat;
        taCalcBPRICEIN.AsFloat:=taDobSpec2PRICEIN.AsFloat;
        taCalcBSUMIN.AsFloat:=taDobSpec2SUMIN.AsFloat;
        taCalcBIM.AsInteger:=taDobSpec2IM.AsInteger;
        taCalcBSM.AsString:=taDobSpec2SM.AsString;
        taCalcBSB.AsString:=taDobSpec2SB.AsString;
        taCalcB.Post;

        taDobSpec2.Next;
      end;
      taDobSpec1.Active:=False;
      taDobSpec2.Active:=False;
      Memo1.Lines.Add('������.');Delay(10);

      bStartOpen:=False;
      fmDobSpec.ShowModal;

      taCalc.Active:=False;
      taCalcB.Active:=False;
    end;
  end;
end;

procedure TfmDocsOutB.ViewDocsOutBDblClick(Sender: TObject);
begin
  //������� �������
  with dmO do
  begin
    if quDocsOutBIACTIVE.AsInteger=0 then acEditDoc1.Execute //��������������
    else acViewDoc1.Execute; //��������}
  end;
end;

procedure TfmDocsOutB.acDelDoc1Execute(Sender: TObject);
begin
  //������� ��������
  if not CanDo('prDelDocOutB') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  with dmO do
  begin
    if quDocsOutB.RecordCount>0 then //���� ��� �������������
    begin
      if quDocsOutBIACTIVE.AsInteger=0 then
      begin
        if MessageDlg('�� ������������� ������ ������� �������� �'+quDocsOutBNUMDOC.AsString+' �� '+FormatDateTime('dd.mm.yyyy',quDocsOutBDATEDOC.AsDateTime),mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          quDocsOutB.Delete;
        end;
      end else
      begin
        showmessage('������� �������������� �������� ������.');
      end;
    end else
    begin
      showmessage('�������� �������� ��� ��������.');
    end;
  end;
end;

procedure TfmDocsOutB.acOnDoc1Execute(Sender: TObject);
Var IdH,IdCli:INteger;
    rQP,rQs,PriceSp,rQ,rSum,rSumP,rMessure:Real;
begin
//������������
  if not CanDo('prOnDocOutB') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmO do
  with dmORep do
  begin
    if quDocsOutB.RecordCount>0 then //���� ��� ������������
    begin
      if quDocsOutBIACTIVE.AsInteger=0 then
      begin
        if MessageDlg('������������ �������� �'+quDocsOutBNUMDOC.AsString+' �� '+FormatDateTime('dd.mm.yyyy',quDocsOutBDATEDOC.AsDateTime),mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          if prTOFind(Trunc(quDocsOutBDATEDOC.AsDateTime),quDocsOutBIDSKL.AsInteger)=1 then
          begin //�� ����
            if MessageDlg('������� ��� �������� ������ �� �� '+quDocsOutBNAMEMH.AsString+' � '+quDocsOutBNUMDOC.AsString+'?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
              prTODel(Trunc(quDocsOutBDATEDOC.AsDateTime),quDocsOutBIDSKL.AsInteger)
            else
            begin
              showmessage('��������� ������� ��������� ����������...');
              exit;
            end;
          end;

         // 1 - ������� ��������� ������
         // 2 - �������� ��������������
         // 3 - �������� ������

          IDH:=quDocsOutBID.AsInteger;
          if quDocsOutBIDSKL.AsInteger>0 then
          begin

            taDobSpec1.Active:=False;
            taDobSpec1.ParamByName('IDH').AsInteger:=IDH;
            taDobSpec1.Active:=True;

            if taDobSpec1.RecordCount=0 then
            begin
              Memo1.Clear;
              Memo1.Lines.Add('������������ ���������� !!!');
              Memo1.Lines.Add('  �� ���������� "������ ���������� ��� ��������".');

              taDobSpec1.Active:=False;
              exit;
            end;

            taDobSpec2.Active:=False;
            taDobSpec2.ParamByName('IDH').AsInteger:=IDH;
            taDobSpec2.Active:=True;

            if taDobSpec2.RecordCount=0 then
            begin
              Memo1.Clear;
              Memo1.Lines.Add('������������ ���������� !!!');
              Memo1.Lines.Add('  �� ���������� "������ ������������� ����".');
              taDobSpec1.Active:=False;
              taDobSpec2.Active:=False;
              exit;
            end;

            taPartTest.Active:=False;
            taPartTest.CreateDataSet;
            rSum:=0;

            taDobSpec1.First;
            while not taDobSpec1.Eof do
            begin
              PriceSp:=0;
              IdCli:=0;
              rSumP:=0;

              rQs:=taDobSpec1QUANT.AsFloat; //taDobSpec1 - � �������� �������� ���������
              prSelPartIn(taDobSpec1ARTICUL.AsInteger,quDocsOutBIDSKL.AsInteger,0);

              quSelPartIn.First;
              if rQs>0 then
              begin
                while (not quSelPartIn.Eof) and (rQs>0) do
                begin
                //���� �� ���� ������� ���� �����, ��������� �������� ���
                  rQp:=quSelPartInQREMN.AsFloat;
                  if rQs<=rQp then  rQ:=rQs//��������� ������ ������ ���������
                              else  rQ:=rQp;
                  rQs:=rQs-rQ;
                //��������� ��������� ������
                //����������� ��������� ������
//?ARTICUL, ?IDDATE, ?IDSTORE, ?IDPARTIN, ?IDDOC, ?IDCLI, ?DTYPE, ?QUANT, ?PRICEIN, ?SUMOUT)
                  prAddPartOut.ParamByName('ARTICUL').AsInteger:=taDobSpec1ARTICUL.AsInteger;
                  prAddPartOut.ParamByName('IDDATE').AsInteger:=Trunc(quDocsOutBDATEDOC.AsDateTime);
                  prAddPartOut.ParamByName('IDSTORE').AsInteger:=quDocsOutBIDSKL.AsInteger;
                  prAddPartOut.ParamByName('IDPARTIN').AsInteger:=quSelPartInID.AsInteger;
                  prAddPartOut.ParamByName('IDDOC').AsInteger:=taDobSpec1IDHEAD.AsInteger;
                  prAddPartOut.ParamByName('IDCLI').AsInteger:=quSelPartInIDCLI.AsInteger;
                  prAddPartOut.ParamByName('DTYPE').AsInteger:=2;
                  prAddPartOut.ParamByName('QUANT').AsFloat:=rQ;
                  prAddPartOut.ParamByName('PRICEIN').AsFloat:=quSelPartInPRICEIN.AsFloat;
                  prAddPartOut.ParamByName('SUMOUT').AsFloat:=quSelPartInPRICEOUT.AsFloat*rQ;
                  prAddPartout.ExecProc;

                  PriceSp:=quSelPartInPRICEIN.AsFloat;
                  IdCli:=quSelPartInIDCLI.AsInteger;
                  rSum:=rSum+PriceSp*rQ;
                  rSumP:=rSumP+PriceSp*rQ;
//                WriteTestLog('N;'+taDobSpec1ARTICUL.AsString+';'+taDobSpec1NAME.AsString+';'+FloatToStr(PriceSp)+';'+FloatToStr(rQ)+';'+FloatToStr(PriceSp*rQ));

                  quSelPartIn.Next;
                end;

                quSelPartIn.Active:=False;

              //��������� �������� � ������������� ������
                if rQs>0 then //�������� ������������� ������, �������� � ������
                begin
                  if PriceSp=0 then
                  begin //��� ���� ���������� ������� � ���������� ����������
                    prCalcLastPrice1.ParamByName('IDGOOD').AsInteger:=taDobSpec1ARTICUL.AsInteger;
                    prCalcLastPrice1.ExecProc;
                    PriceSp:=prCalcLastPrice1.ParamByName('PRICEIN').AsCurrency;
                    rMessure:=prCalcLastPrice1.ParamByName('KOEF').AsFloat;
                    IdCli:=prCalcLastPrice1.ParamByName('IDCLI').AsInteger;
                    if (rMessure<>0)and(rMessure<>1) then PriceSp:=PriceSp/rMessure;
                  end;

                //��������� ��� ����� ���������
                  prAddPartOut.ParamByName('ARTICUL').AsInteger:=taDobSpec1ARTICUL.AsInteger;
                  prAddPartOut.ParamByName('IDDATE').AsInteger:=Trunc(quDocsOutBDATEDOC.AsDateTime);
                  prAddPartOut.ParamByName('IDSTORE').AsInteger:=quDocsOutBIDSKL.AsInteger;
                  prAddPartOut.ParamByName('IDPARTIN').AsInteger:=-1;
                  prAddPartOut.ParamByName('IDDOC').AsInteger:=taDobSpec1IDHEAD.AsInteger;
                  prAddPartOut.ParamByName('IDCLI').AsInteger:=IdCli;
                  prAddPartOut.ParamByName('DTYPE').AsInteger:=2;
                  prAddPartOut.ParamByName('QUANT').AsFloat:=rQs;
                  prAddPartOut.ParamByName('PRICEIN').AsFloat:=PriceSp;
                  prAddPartOut.ParamByName('SUMOUT').AsFloat:=PriceSp*rQs;
                  prAddPartout.ExecProc;

                  rSum:=rSum+PriceSp*rQs;
                  rSumP:=rSumP+PriceSp*rQs;
//                WriteTestLog('B;'+taDobSpec1ARTICUL.AsString+';'+taDobSpec1NAME.AsString+';'+FloatToStr(PriceSp)+';'+FloatToStr(rQs)+';'+FloatToStr(PriceSp*rQs));

                  taPartTest.Append;
                  taPartTestNum.AsInteger:=0;
                  taPartTestIdGoods.AsInteger:=taDobSpec1ARTICUL.AsInteger;
                  taPartTestNameG.AsString:=taDobSpec1NAME.AsString;
                  taPartTestIM.AsInteger:=taDobSpec1IDM.AsInteger;
                  taPartTestSM.AsString:=taDobSpec1SM.AsString;
                  taPartTestQuant.AsFloat:=taDobSpec1QUANT.AsFloat;
                  taPartTestPrice1.AsCurrency:=0;
                  taPartTestiRes.AsInteger:=0;
                  taPartTest.Post;
                end else
                begin
                  taPartTest.Append;
                  taPartTestNum.AsInteger:=0;
                  taPartTestIdGoods.AsInteger:=taDobSpec1ARTICUL.AsInteger;
                  taPartTestNameG.AsString:=taDobSpec1NAME.AsString;
                  taPartTestIM.AsInteger:=taDobSpec1IDM.AsInteger;
                  taPartTestSM.AsString:=taDobSpec1SM.AsString;
                  taPartTestQuant.AsFloat:=taDobSpec1QUANT.AsFloat;
                  taPartTestPrice1.AsCurrency:=0;
                  taPartTestiRes.AsInteger:=1;
                  taPartTest.Post;
                end;
              end;

              if rQs<0 then //�������� ��� ������
              begin

//              IDSTORE=:IDSKL and ARTICUL=:IDCARD and IDATE>=:IDATE

                quSelPartIn1.Active:=False;
                quSelPartIn1.ParamByName('IDATE').AsInteger:=Trunc(quDocsOutBDATEDOC.AsDateTime);
                quSelPartIn1.ParamByName('IDSKL').AsInteger:=quDocsOutBIDSKL.AsInteger;
                quSelPartIn1.ParamByName('IDCARD').AsInteger:=taDobSpec1ARTICUL.AsInteger;
                quSelPartIn1.Active:=True;

                quSelPartIn1.First;
                if quSelPartIn1.RecordCount>0 then
                begin
                  PriceSp:=quSelPartIn1PRICEIN.AsFloat;
                  rSumP:=PriceSp*rQs;
                  rSum:=rSum+PriceSp*rQs;
                  //����������� ��������� ������
                  rQ:=(-1)*rQs+quSelPartIn1QREMN.AsFloat;
                  quSelPartIn1.Edit;
                  quSelPartIn1QREMN.AsFloat:=rQ;
                  quSelPartIn1.Post;

                  //�������� ��������������
//EXECUTE PROCEDURE PR_GDSMOVE (?IART, ?IDATE, ?IDSTORE, ?RPOSTIN, ?RPOSTOUT, ?RVNIN, ?RVNOUT, ?RINV, ?RQREAL)
                  prGdsMove.ParamByName('IART').AsInteger:=taDobSpec1ARTICUL.AsInteger;
                  prGdsMove.ParamByName('IDATE').AsInteger:=Trunc(quDocsOutBDATEDOC.AsDateTime);
                  prGdsMove.ParamByName('IDSTORE').AsInteger:=quDocsOutBIDSKL.AsInteger;
                  prGdsMove.ParamByName('RPOSTIN').AsFloat:=(-1)*rQs;
                  prGdsMove.ParamByName('RPOSTOUT').AsFloat:=0;
                  prGdsMove.ParamByName('RVNIN').AsFloat:=0;
                  prGdsMove.ParamByName('RVNOUT').AsFloat:=0;
                  prGdsMove.ParamByName('RINV').AsFloat:=0;
                  prGdsMove.ParamByName('RQREAL').AsFloat:=0;
                  prGdsMove.ExecProc;

                end;
                quSelPartIn1.Active:=False;
              end;


              taDobSpec1.Edit;
              taDobSpec1SUMIN.AsFloat:=rSumP;
              taDobSpec1.Post;

              taDobSpec1.Next;
            end;

            taDobSpec2.First;
            while not taDobSpec2.Eof do
            begin
              if taDobSpec1.Locate('ARTICUL',taDobSpec2IDCARD.AsInteger,[]) then
              if taDobSpec1QUANT.AsFloat<>0 then
              begin
                taDobSpec2.Edit;
                taDobSpec2PRICEIN.AsFloat:=taDobSpec1SUMIN.AsFloat/taDobSpec1QUANT.AsFloat;
                taDobSpec2SUMIN.AsFloat:=taDobSpec1SUMIN.AsFloat/taDobSpec1QUANT.AsFloat*taDobSpec2QUANTC.AsFloat;
                taDobSpec2.Post;
              end else
              begin
                taDobSpec2.Edit;
                taDobSpec2PRICEIN.AsFloat:=0;
                taDobSpec2SUMIN.AsFloat:=0;
                taDobSpec2.Post;
              end;
              taDobSpec2.Next;
            end;

            taDobSpec1.Active:=False;
            taDobSpec2.Active:=False;

            // 3 - �������� ������
            quDocsOutB.Edit;
            quDocsOutBIACTIVE.AsInteger:=1;
            quDocsOutBSUMIN.AsFloat:=rSum;
            quDocsOutB.Post;
            quDocsOutB.Refresh;

            fmPartIn1:=tfmPartIn1.Create(Application);
            fmPartIn1.Label5.Caption:=quDocsOutBNAMEMH.AsString;
            fmPartIn1.Label6.Caption:='�� ����.';
            fmPartIn1.ViewPartInPrice1.Visible:=False;
            fmPartIn1.ViewPartInNum.Visible:=False;

            fmPartIn1.ShowModal;
            fmPartIn1.Release;
            taPartTest.Active:=False;
          end else showmessage('���������� ����� �������� ���������.');
        end;
      end;
    end;
  end;
end;


procedure TfmDocsOutB.acOffDoc1Execute(Sender: TObject);
Var IdH:INteger;
    rQs,rQ:Real;
begin
//��������
  if not CanDo('prOffDocOutB') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  with dmO do
  begin
    if quDocsOutB.RecordCount>0 then //���� ��� ����������
    begin
      if quDocsOutBIACTIVE.AsInteger=1 then
      begin
        if MessageDlg('�������� �������� �'+quDocsOutBNUMDOC.AsString+' �� '+FormatDateTime('dd.mm.yyyy',quDocsOutBDATEDOC.AsDateTime),mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          if prTOFind(Trunc(quDocsOutBDATEDOC.AsDateTime),quDocsOutBIDSKL.AsInteger)=1 then
          begin //�� ����
            if MessageDlg('������� ��� �������� ������ �� �� '+quDocsOutBNAMEMH.AsString+' � '+quDocsOutBNUMDOC.AsString+'?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
              prTODel(Trunc(quDocsOutBDATEDOC.AsDateTime),quDocsOutBIDSKL.AsInteger)
            else
            begin
              showmessage('��������� ������� ��������� ����������...');
              exit;
            end;
          end;
          //������� ��� �������� ������
//          ?IDDOC, ?DTYPE
          if pos('Ret',quDocsOutBOPER.AsString)=0 then
          begin
            prDelPartOut.ParamByName('IDDOC').AsInteger:=quDocsOutBID.AsInteger;
            prDelPartOut.ParamByName('DTYPE').AsInteger:=2;
            prDelPartOut.ExecProc;
          end
          else //��� ����� ��������
          begin
            IDH:=quDocsOutBID.AsInteger;

            taDobSpec1.Active:=False;
            taDobSpec1.ParamByName('IDH').AsInteger:=IDH;
            taDobSpec1.Active:=True;

            taDobSpec1.First;
            while not taDobSpec1.Eof do
            begin
              rQs:=taDobSpec1QUANT.AsFloat; //taDobSpec1 - � �������� �������� ���������
              //rQs < 0

              quSelPartIn1.Active:=False;
              quSelPartIn1.ParamByName('IDATE').AsInteger:=Trunc(quDocsOutBDATEDOC.AsDateTime);
              quSelPartIn1.ParamByName('IDSKL').AsInteger:=quDocsOutBIDSKL.AsInteger;
              quSelPartIn1.ParamByName('IDCARD').AsInteger:=taDobSpec1ARTICUL.AsInteger;
              quSelPartIn1.Active:=True;

              quSelPartIn1.First;
              if quSelPartIn1.RecordCount>0 then
              begin
                  //��������� ������� ��������� ������
                rQ:=rQs+quSelPartIn1QREMN.AsFloat;
                quSelPartIn1.Edit;
                quSelPartIn1QREMN.AsFloat:=rQ;
                quSelPartIn1.Post;

                  //�������� ��������������
//EXECUTE PROCEDURE PR_GDSMOVE (?IART, ?IDATE, ?IDSTORE, ?RPOSTIN, ?RPOSTOUT, ?RVNIN, ?RVNOUT, ?RINV, ?RQREAL)
                prGdsMove.ParamByName('IART').AsInteger:=taDobSpec1ARTICUL.AsInteger;
                prGdsMove.ParamByName('IDATE').AsInteger:=Trunc(quDocsOutBDATEDOC.AsDateTime);
                prGdsMove.ParamByName('IDSTORE').AsInteger:=quDocsOutBIDSKL.AsInteger;
                prGdsMove.ParamByName('RPOSTIN').AsFloat:=rQs;
                prGdsMove.ParamByName('RPOSTOUT').AsFloat:=0;
                prGdsMove.ParamByName('RVNIN').AsFloat:=0;
                prGdsMove.ParamByName('RVNOUT').AsFloat:=0;
                prGdsMove.ParamByName('RINV').AsFloat:=0;
                prGdsMove.ParamByName('RQREAL').AsFloat:=0;
                prGdsMove.ExecProc;

              end;
              quSelPartIn1.Active:=False;

              taDobSpec1.Next;
            end;
            taDobSpec1.Active:=False;
          end;

          quDocsOutB.Edit;
          quDocsOutBIACTIVE.AsInteger:=0;
          quDocsOutB.Post;
          quDocsOutB.Refresh;
        end;
      end;
    end;
  end;
end;

procedure TfmDocsOutB.Timer1Timer(Sender: TObject);
begin
  if bClearDocOutB=True then begin StatusBar1.Panels[0].Text:=''; bClearDocOutB:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearDocOutB:=True;
end;

procedure TfmDocsOutB.acAddDocManualExecute(Sender: TObject);
Var IDH:INteger;
    dCurDate:TDateTime;
begin
//  �������� �������� �������
  if not CanDo('prAddDocOutB') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  with dmO do
  with dmORep do
  begin
    if (Date>=CommonSet.DateFrom) and (Date<=CommonSet.DateTo-1) then dCurDate:=Trunc(Date)
    else dCurDate:=Trunc(CommonSet.DateTo-1);

    IDH:=GetId('DocOutB');

    quDOBHEAD.Active:=False;
    quDOBHEAD.ParamByName('IDH').AsInteger:=IDH;
    quDOBHEAD.Active:=True;

    quDOBHEAD.Append;
    quDOBHEADID.AsInteger:=IDH;
    quDOBHEADDATEDOC.AsDateTime:=dCurDate;
    quDOBHEADNUMDOC.AsString:=IntToStr(IDH);
    quDOBHEADDATESF.AsDateTime:=dCurDate;
    quDOBHEADNUMSF.AsString:=IntToStr(IDH);
    quDOBHEADIDCLI.AsInteger:=0;
    quDOBHEADIDSKL.AsInteger:=CommonSet.IdStore;
    quDOBHEADSUMIN.AsCurrency:=0;
    quDOBHEADSUMUCH.AsCurrency:=0;
    quDOBHEADSUMTAR.AsCurrency:=0;
    quDOBHEADSUMNDS0.AsCurrency:=0;
    quDOBHEADSUMNDS1.AsCurrency:=0;
    quDOBHEADSUMNDS2.AsCurrency:=0;
    quDOBHEADPROCNAC.AsFloat:=0;
    quDOBHEADIACTIVE.AsInteger:=0;
    quDOBHEADOPER.AsString:='';
    quDOBHEAD.Post;

    quDocsOutB.Active:=False;
    quDocsOutB.ParamByName('DATEB').AsDate:=CommonSet.DateFrom;
    quDocsOutB.ParamByName('DATEE').AsDate:=CommonSet.DateTo;
    quDocsOutB.Active:=True;

    if quDocsOutB.Locate('ID',IDH,[]) then
    begin
      prSetDOBSpecPar('',1); //�������������� 1

      taDOBSpec.Active:=False;
      taDobSpec.ParamByName('IDH').AsInteger:=IDH;
      taDobSpec.Active:=True;

      taCalc.Active:=False;
      taCalc.CreateDataSet;

      taCalcB.Active:=False;
      taCalcB.CreateDataSet;

      fmDobSpec.ShowModal;

      taCalc.Active:=False;
      taCalcB.Active:=False;
    end;
  end;
end;

procedure TfmDocsOutB.acSebBlExecute(Sender: TObject);
begin
 //�������������
  if not CanDo('prDocOutBSeb') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  with dmO do
  with dmORep do
  begin
    begin
      Memo1.Lines.Add('�����, ���� ������.'); delay(10);

      taCalcB.Active:=False;
      taCalcB.CreateDataSet;

      taDobSpec2.Active:=False;
      taDobSpec2.ParamByName('IDH').AsInteger:=quDocsOutBID.AsInteger;
      taDobSpec2.Active:=True;
      taDobSpec2.First;
      while not taDobSpec2.Eof do
      begin
        taCalcB.Append;
        taCalcBID.AsInteger:=taDobSpec2IDB.AsInteger;
        taCalcBCODEB.AsInteger:=taDobSpec2CODEB.AsInteger;
        taCalcBNAMEB.AsString:=taDobSpec2NAMEB.AsString;
        taCalcBQUANT.AsFloat:=taDobSpec2QUANT.AsFloat;
        taCalcBPRICEOUT.AsFloat:=taDobSpec2PRICEOUT.AsFloat;
        taCalcBSUMOUT.AsFloat:=taDobSpec2SUMOUT.AsFloat;
        taCalcBIDCARD.AsInteger:=taDobSpec2IDCARD.AsInteger;
        taCalcBNAMEC.AsString:=taDobSpec2NAMEC.AsString;
        taCalcBQUANTC.AsFloat:=taDobSpec2QUANTC.AsFloat;
        taCalcBPRICEIN.AsFloat:=taDobSpec2PRICEIN.AsFloat;
        taCalcBSUMIN.AsFloat:=taDobSpec2SUMIN.AsFloat;
        taCalcBIM.AsInteger:=taDobSpec2IM.AsInteger;
        taCalcBSM.AsString:=taDobSpec2SM.AsString;
        taCalcBSB.AsString:=taDobSpec2SB.AsString;
        taCalcB.Post;

        taDobSpec2.Next;
      end;
      taDobSpec2.Active:=False;

      Memo1.Lines.Add('������ ��������.'); delay(10);
      with dmORep do
      begin
        frRep1.LoadFromFile(CurDir + 'BludaSeb.frf');

        frVariables.Variable['DocNum']:=quDocsOutBNUMDOC.AsString;
        frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',quDocsOutBDATEDOC.AsDateTime);
        frVariables.Variable['SumOut']:=quDocsOutBSUMUCH.AsFloat;
        frVariables.Variable['DocOper']:=quDocsOutBOPER.AsString;

        frRep1.ReportName:='������������� ����.';
        frRep1.PrepareReport;
        frRep1.ShowPreparedReport;
      end;
    end;
  end;
end;

procedure TfmDocsOutB.acAddDocBExecute(Sender: TObject);
begin
//��������

end;

procedure TfmDocsOutB.FormShow(Sender: TObject);
begin
  bStartOpen:=False;
end;

procedure TfmDocsOutB.acPrintMinExecute(Sender: TObject);
Var IdPar:Integer;
begin
  //������������� ������
  if not CanDo('prDocOutBSeb') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  with dmO do
  with dmORep do
  begin
    if quDocsOutB.RecordCount>0 then
    begin
      if quDocsOutBIACTIVE.AsInteger=1 then
      begin
        idPar:=GetId('Pars');
        taParams.Active:=False;
        taParams.Active:=True;
        taParams.Append;
        taParamsID.AsInteger:=idPar;
        taParamsIDATEB.AsInteger:=Trunc(Date);
        taParamsIDATEE.AsInteger:=Trunc(Date);
        taParamsIDSTORE.AsInteger:=quDocsOutBID.AsInteger;
        taParams.Post;
        taParams.Active:=False;

        quDocOutBPrint.Active:=False;
        quDocOutBPrint.ParamByName('IDH').AsInteger:=quDocsOutBID.AsInteger;
        quDocOutBPrint.Active:=True;

        RepDocOutB.LoadFromFile(CurDir + 'DocOutBSeb.frf');

        frVariables.Variable['DocNum']:=quDocsOutBNUMDOC.AsString;
        frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',quDocsOutBDATEDOC.AsDateTime);
        frVariables.Variable['DocStore']:=quDocsOutBNAMEMH.AsString;
        frVariables.Variable['Depart']:=CommonSet.DepartName;

        RepDocOutB.ReportName:='�������� ����.';
        RepDocOutB.PrepareReport;
        RepDocOutB.ShowPreparedReport;

        quDocOutBPrint.Active:=False;
      end else showmessage('������ ����������, �������� ������ ���� �����������.');
    end else
    begin
      showmessage('�������� �������� ��� ������.');
    end;
  end;
end;

end.
