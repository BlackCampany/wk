unit Tabs;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  ComCtrls, ExtCtrls, SpeedBar, Placemnt, ActnList, XPStyleActnCtrls,
  ActnMan, cxDataStorage, cxCurrencyEdit, cxImageComboBox,
  ComObj, ActiveX, Excel2000, OleServer, ExcelXP;

type
  TfmTabs = class(TForm)
    StatusBar1: TStatusBar;
    ViewTabs: TcxGridDBTableView;
    LevelTabs: TcxGridLevel;
    GridTabs: TcxGrid;
    ViewTabsID: TcxGridDBColumn;
    ViewTabsID_PERSONAL: TcxGridDBColumn;
    ViewTabsNUMTABLE: TcxGridDBColumn;
    ViewTabsQUESTS: TcxGridDBColumn;
    ViewTabsTABSUM: TcxGridDBColumn;
    ViewTabsBEGTIME: TcxGridDBColumn;
    ViewTabsENDTIME: TcxGridDBColumn;
    ViewTabsDISCONT: TcxGridDBColumn;
    ViewTabsOPERTYPE: TcxGridDBColumn;
    ViewTabsCHECKNUM: TcxGridDBColumn;
    ViewTabsSKLAD: TcxGridDBColumn;
    ViewTabsNAME: TcxGridDBColumn;
    SpeedBar1: TSpeedBar;
    FormPlacement1: TFormPlacement;
    ViewTabsCASHNUM: TcxGridDBColumn;
    ViewTabsZNUM: TcxGridDBColumn;
    ViewTabsCHECKNUM1: TcxGridDBColumn;
    ViewTabsTABSUM1: TcxGridDBColumn;
    ViewTabsCLIENTSUM: TcxGridDBColumn;
    ViewTabsCHDATE: TcxGridDBColumn;
    amTabs: TActionManager;
    acPeriod: TAction;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    acSpecSel: TAction;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    LevelSpec: TcxGridLevel;
    ViewSpec: TcxGridDBTableView;
    ViewSpecID_TAB: TcxGridDBColumn;
    ViewSpecID: TcxGridDBColumn;
    ViewSpecID_PERSONAL: TcxGridDBColumn;
    ViewSpecNUMTABLE: TcxGridDBColumn;
    ViewSpecSIFR: TcxGridDBColumn;
    ViewSpecPRICE: TcxGridDBColumn;
    ViewSpecQUANTITY: TcxGridDBColumn;
    ViewSpecDISCOUNTPROC: TcxGridDBColumn;
    ViewSpecDISCOUNTSUM: TcxGridDBColumn;
    ViewSpecSUMMA: TcxGridDBColumn;
    ViewSpecITYPE: TcxGridDBColumn;
    ViewSpecNAMEMM: TcxGridDBColumn;
    ViewSpecNAMEMD: TcxGridDBColumn;
    ViewSpecNAMEPERS: TcxGridDBColumn;
    ViewSpecQUESTS: TcxGridDBColumn;
    ViewSpecTABSUM: TcxGridDBColumn;
    ViewSpecBEGTIME: TcxGridDBColumn;
    ViewSpecENDTIME: TcxGridDBColumn;
    ViewSpecDISCONT: TcxGridDBColumn;
    ViewSpecOPERTYPE: TcxGridDBColumn;
    ViewSpecCHECKNUM: TcxGridDBColumn;
    ViewSpecSKLAD: TcxGridDBColumn;
    ViewSpecCASHNUM: TcxGridDBColumn;
    ViewSpecZNUM: TcxGridDBColumn;
    ViewSpecCLIENTSUM: TcxGridDBColumn;
    ViewSpecCHDATE: TcxGridDBColumn;
    SpeedItem4: TSpeedItem;
    ViewSpecName: TcxGridDBColumn;
    aPrintCheck: TAction;
    ViewSpecSTREAM: TcxGridDBColumn;
    ViewSpecNAMESTREAM: TcxGridDBColumn;
    SpeedItem5: TSpeedItem;
    acExportEx: TAction;
    ViewTabsSTATION: TcxGridDBColumn;
    ViewSpecSTATION: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acPeriodExecute(Sender: TObject);
    procedure acSpecSelExecute(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
    procedure SpeedItem4Click(Sender: TObject);
    procedure aPrintCheckExecute(Sender: TObject);
    procedure acExportExExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmTabs: TfmTabs;

implementation

uses Dm, Un1, PeriodUni, SpecSel, UnCash, Period;

{$R *.dfm}
{$I dll.inc}

procedure TfmTabs.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  ViewTabs.RestoreFromIniFile(CurDir+GridIni);
  ViewSpec.RestoreFromIniFile(CurDir+GridIni);
end;

procedure TfmTabs.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  dmC.taTabsAllSel.Active:=False;
  ViewTabs.StoreToIniFile(CurDir+GridIni,False);
  ViewSpec.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmTabs.acPeriodExecute(Sender: TObject);
begin
  try
    fmPeriod:=TfmPeriod.Create(Application); //��� ���� ������������

    fmPeriod.ShowModal;
    if fmPeriod.ModalResult=mrOk then
    begin
      with dmC do
      begin
        taTabsAllSel.Active:=False;
        taTabsAllSel.ParamByName('DBEG').AsDateTime:=TrebSel.DateFrom;
        taTabsAllSel.ParamByName('DEND').AsDateTime:=TrebSel.DateTo;
        taTabsAllSel.Active:=True;
        taTabsAllSel.Last;

        quSelSpecAll.Active:=False;
        quSelSpecAll.ParamByName('DBEG').AsDateTime:=TrebSel.DateFrom;
        quSelSpecAll.ParamByName('DEND').AsDateTime:=TrebSel.DateTo;
        quSelSpecAll.Active:=True;
      end;

      fmTabs.Caption:='����������� ������ �� ������ � '+FormatDateTime('dd.mm.yyyy hh:mm',TrebSel.DateFrom)+' �� '+FormatDateTime('dd.mm.yyyy hh:mm',TrebSel.DateTo);
    end;
  finally
    fmPeriod.Release;
  end;  
end;

procedure TfmTabs.acSpecSelExecute(Sender: TObject);
begin
  with dmC do
  begin
    taSpecAllSel.Active:=False;
    taSpecAllSel.ParamByName('IDHEAD').AsInteger:=taTabsAllSelID.AsInteger;
    taSpecAllSel.Active:=True;

    fmSpecSel.ShowModal;

  end;
end;

procedure TfmTabs.SpeedItem3Click(Sender: TObject);
begin
  close;
end;

procedure TfmTabs.SpeedItem4Click(Sender: TObject);
begin
  //��������� ����
  if LevelTabs.Visible=True then
  begin
    LevelTabs.Visible:=False;
    SpeedItem2.Enabled:=False;
    LevelSpec.Visible:=True;
  end else
  begin
    LevelTabs.Visible:=True;
    SpeedItem2.Enabled:=True;
    LevelSpec.Visible:=False;
  end;
end;

procedure TfmTabs.aPrintCheckExecute(Sender: TObject);
Var iCo:Integer;
    rSum:Real;
    Rec:TcxCustomGridRecord;
    i,j,iSum: Integer;

    {
Var sGr:String;
    iGr:Integer;
    iCo:Integer;
    iNum: Integer;
}

begin
  //������ �����
  iCo:=ViewTabs.Controller.SelectedRecordCount;
  if iCo>0 then
  begin
    if MessageDlg('���������� ���� ('+IntToStr(iCo)+')?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      OpenDll('������','AERF',PChar('COM1'),0);
      with dmC do
      begin
        //ViewTabsTABSUM
        for i:=0 to ViewTabs.Controller.SelectedRecordCount-1 do
        begin
          Rec:=ViewTabs.Controller.SelectedRecords[i];

          for j:=0 to Rec.ValueCount-1 do
          begin
            if ViewTabs.Columns[j].Name='ViewTabsTABSUM' then break;
          end;

          rSum:=Rec.Values[j];

          if rSum>0 then
          begin
            iSum:=RoundEx(rSum*100);
            StartReceipt(0,1,'','','');
            ItemReceiptPlus('����� �� ����� ','12345','��.','','',iSum,1000,1);
            TotalReceipt;
            TenderReceipt(0,iSum,'');
            CloseReceipt;
          end;
        end;
      end;
      XReport;
      delay(1000);
      CloseDll;
    end;
  end;

{

iCo:=ViewMenuCr.Controller.SelectedRecordCount;
    if iCo>0 then
    begin
      if MessageDlg('�� ������������� ������ ����������� ��������� ������� ('+IntToStr(iCo)+' ��.) � ������ "'+sGr+'"?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        with dmC do
        begin
          taMenu.Active:=False;
          taMenu.Active:=True;

          for i:=0 to ViewMenuCr.Controller.SelectedRecordCount-1 do
          begin
            Rec:=ViewMenuCr.Controller.SelectedRecords[i];

            for j:=0 to Rec.ValueCount-1 do
            begin
              if ViewMenuCr.Columns[j].Name='ViewMenuCrSIFR' then break;
            end;

            iNum:=Rec.Values[j];
          //��� ��� - ����������

            if taMenu.Locate('SIFR',iNum,[]) then
            begin
              trUpd2.StartTransaction;
              taMenu.Edit;
              taMenuPARENT.AsInteger:=iGr;
              taMenu.Post;
              trUpd2.Commit;
            end;
          end;
          taMenu.Active:=False;
          quMenuSel.FullRefresh;
        end;
      end;
    end;
}


end;

procedure TfmTabs.acExportExExecute(Sender: TObject);
Var ExcelApp, Workbook, Range, Cell1, Cell2, ArrayData  : Variant;
    iCol,iRow,i:Integer;
    NameF:String;
begin
//������� � ������
  with dmC do
  begin
    StatusBar1.Panels[0].Text:='����� ���� ������������.'; delay(10);
    if LevelTabs.Visible=True then
    begin //ViewTabs
      StatusBar1.Panels[0].Text:='����� ���� ������������.'; delay(10);
      ViewTabs.BeginUpdate;

      dsTabsAllSel.DataSet:=nil;

      taTabsAllSel.Filter:=ViewTabs.DataController.Filter.FilterText;
      taTabsAllSel.Filtered:=True;

      //����� ������ ������
      if not IsOLEObjectInstalled('Excel.Application') then exit;
      ExcelApp := CreateOleObject('Excel.Application');
      ExcelApp.Application.EnableEvents := false;
    //� ������� �����
      Workbook := ExcelApp.WorkBooks.Add;

      iCol:=ViewTabs.VisibleColumnCount;
      iRow:=taTabsAllSel.RecordCount+1;

      ArrayData := VarArrayCreate([1,iRow, 1, iCol], varVariant);
      for i:=1 to iCol do
      begin
        ArrayData[1,i] := ViewTabs.VisibleColumns[i-1].Caption;
      end;

      taTabsAllSel.First;
      iRow:=2;
      While not taTabsAllSel.eof do
      begin
        for i:=1 to iCol do
        begin
          NameF:=ViewTabs.VisibleColumns[i-1].Name;
          delete(NameF,1,8); //������ �� �������� ������� ViewTabs
          ArrayData[iRow,i] := taTabsAllSel.Fields.FieldByName(NameF).Value;
        end;
        taTabsAllSel.Next; //Delay(10);
        inc(iRow);
      end;

      StatusBar1.Panels[0].Text:='���� ������� ������.'; delay(10);

      Cell1 := WorkBook.WorkSheets[1].Cells[1,1];
      Cell2 := WorkBook.WorkSheets[1].Cells[1+taTabsAllSel.RecordCount,iCol];
      Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
      Range.Value := ArrayData;

      taTabsAllSel.Filter:='';
      taTabsAllSel.Filtered:=False;

      taTabsAllSel.First;

      dsTabsAllSel.DataSet:=taTabsAllSel;

      ViewTabs.EndUpdate;
      StatusBar1.Panels[0].Text:='������������ ��.';

      ExcelApp.Visible := true;
    end else //levelspec
    begin
      StatusBar1.Panels[0].Text:='����� ���� ������������.'; delay(10);
      ViewSpec.BeginUpdate;

      dsSelSpecAll.DataSet:=nil;

      quSelSpecAll.Filter:=ViewSpec.DataController.Filter.FilterText;
      quSelSpecAll.Filtered:=True;

      //����� ������ ������
      if not IsOLEObjectInstalled('Excel.Application') then exit;
      ExcelApp := CreateOleObject('Excel.Application');
      ExcelApp.Application.EnableEvents := false;
    //� ������� �����
      Workbook := ExcelApp.WorkBooks.Add;

      iCol:=ViewSpec.VisibleColumnCount;
      iRow:=quSelSpecAll.RecordCount+1;

      ArrayData := VarArrayCreate([1,iRow, 1, iCol], varVariant);
      for i:=1 to iCol do
      begin
        ArrayData[1,i] := ViewSpec.VisibleColumns[i-1].Caption;
      end;

      quSelSpecAll.First;
      iRow:=2;
      While not quSelSpecAll.eof do
      begin
        for i:=1 to iCol do
        begin
          NameF:=ViewSpec.VisibleColumns[i-1].Name;
          delete(NameF,1,8); //������ �� �������� ������� ViewSpec
          ArrayData[iRow,i] := quSelSpecAll.Fields.FieldByName(NameF).Value;
        end;
        quSelSpecAll.Next; //Delay(10);
        inc(iRow);
      end;

      StatusBar1.Panels[0].Text:='���� ������� ������.'; delay(10);

      Cell1 := WorkBook.WorkSheets[1].Cells[1,1];
      Cell2 := WorkBook.WorkSheets[1].Cells[1+quSelSpecAll.RecordCount,iCol];
      Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
      Range.Value := ArrayData;

      quSelSpecAll.Filter:='';
      quSelSpecAll.Filtered:=False;

      quSelSpecAll.First;

      dsSelSpecAll.DataSet:=quSelSpecAll;

      ViewSpec.EndUpdate;
      StatusBar1.Panels[0].Text:='������������ ��.';

      ExcelApp.Visible := true;
    end;
  end;
end;

end.
