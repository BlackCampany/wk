unit RepObSa;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxTextEdit, cxImageComboBox,
  DBClient, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxControls, cxGridCustomView, cxGrid,
  cxContainer, cxMemo, ExtCtrls, SpeedBar, ComCtrls, Placemnt, FR_DSet,
  FR_DBSet, FR_Class, cxGridBandedTableView, cxGridDBBandedTableView;

type
  TfmRepOb = class(TForm)
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    Panel1: TPanel;
    Memo1: TcxMemo;
    GridObVed: TcxGrid;
    LevelObVed: TcxGridLevel;
    taObVed: TClientDataSet;
    dsObVed: TDataSource;
    FormPlacement1: TFormPlacement;
    frRepOb: TfrReport;
    frtaObVed: TfrDBDataSet;
    SpeedItem4: TSpeedItem;
    taObVedIdCode: TIntegerField;
    taObVedNameC: TStringField;
    taObVediM: TIntegerField;
    taObVedSm: TStringField;
    taObVedIdGroup: TIntegerField;
    taObVedNameClass: TStringField;
    taObVedQBeg: TFloatField;
    taObVedSBeg: TCurrencyField;
    taObVedQIn: TFloatField;
    taObVedSIn: TCurrencyField;
    taObVedQOut: TFloatField;
    taObVedSOut: TCurrencyField;
    taObVedQRes: TFloatField;
    taObVedSRes: TCurrencyField;
    taObVedPrice: TFloatField;
    ViewObVed: TcxGridDBBandedTableView;
    ViewObVedIdCode: TcxGridDBBandedColumn;
    ViewObVedNameC: TcxGridDBBandedColumn;
    ViewObVediM: TcxGridDBBandedColumn;
    ViewObVedSm: TcxGridDBBandedColumn;
    ViewObVedIdGroup: TcxGridDBBandedColumn;
    ViewObVedNameClass: TcxGridDBBandedColumn;
    ViewObVedQBeg: TcxGridDBBandedColumn;
    ViewObVedSBeg: TcxGridDBBandedColumn;
    ViewObVedQIn: TcxGridDBBandedColumn;
    ViewObVedSIn: TcxGridDBBandedColumn;
    ViewObVedQOut: TcxGridDBBandedColumn;
    ViewObVedSOut: TcxGridDBBandedColumn;
    ViewObVedQRes: TcxGridDBBandedColumn;
    ViewObVedSRes: TcxGridDBBandedColumn;
    ViewObVedPrice: TcxGridDBBandedColumn;
    taObVedKm: TFloatField;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
    procedure SpeedItem4Click(Sender: TObject);
    procedure taObVedCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmRepOb: TfmRepOb;

implementation

uses Un1, SelPerSkl, dmOffice, DMOReps;

{$R *.dfm}

procedure TfmRepOb.FormCreate(Sender: TObject);
begin
  GridObVed.Align:=AlClient;
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  ViewObVed.RestoreFromIniFile(CurDir+GridIni);
end;

procedure TfmRepOb.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewObVed.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmRepOb.FormShow(Sender: TObject);
begin
  Memo1.Clear;
end;

procedure TfmRepOb.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmRepOb.SpeedItem2Click(Sender: TObject);
Var  IdPar:Integer;
     bAdd:Boolean;
     rQMain,rSum,rKm:Real;
begin
  fmSelPerSkl:=tfmSelPerSkl.Create(Application);
  with fmSelPerSkl do
  begin
    cxDateEdit1.Date:=CommonSet.DateFrom;
    quMHAll1.Active:=False;
    quMHAll1.Active:=True;
    quMHAll1.First;
    cxLookupComboBox1.EditValue:=quMHAll1ID.AsInteger;
  end;
  fmSelPerSkl.ShowModal;
  if fmSelPerSkl.ModalResult=mrOk then
  begin
    CommonSet.DateFrom:=Trunc(fmSelPerSkl.cxDateEdit1.Date);
    CommonSet.DateTo:=Trunc(fmSelPerSkl.cxDateEdit2.Date)+1;
    CommonSet.IdStore:=fmSelPerSkl.cxLookupComboBox1.EditValue;
    CommonSet.NameStore:=fmSelPerSkl.cxLookupComboBox1.Text;

    if CommonSet.DateTo>=iMaxDate then fmRepOb.Caption:='��������� ��������� �� �� '+CommonSet.NameStore+' � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
    else fmRepOb.Caption:='��������� ��������� �� �� '+CommonSet.NameStore+' �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);
    fmSelPerSkl.quMHAll1.Active:=False;
    fmSelPerSkl.Release;

    with dmO do
    with dmORep do
    begin
      Memo1.Lines.Add('����� .. ���� ������������ ������.'); delay(10);
      Memo1.Lines.Add('  ������� �� ������.'); delay(10);

      idPar:=GetId('Pars');
      taParams.Active:=False;
      taParams.Active:=True;
      taParams.Append;
      taParamsID.AsInteger:=idPar;
      taParamsIDATEB.AsInteger:=Trunc(CommonSet.DateFrom-1); //-1 �.�. ���� ������
      taParamsIDATEE.AsInteger:=Trunc(CommonSet.DateFrom-1);
      taParamsIDSTORE.AsInteger:=CommonSet.IdStore;
      taParams.Post;
      taParams.Active:=False;

      quRemnDate.Active:=False;
      quRemnDate.Active:=True;

      Memo1.Lines.Add('  ��������� ��������.'); delay(10);

      ViewObVed.BeginUpdate;
      taObVed.Active:=False;
      taObVed.CreateDataSet;

      quRemnDate.First;
      while not quRemnDate.Eof do
      begin
          bAdd:=False;
          if quRemnDateTCARD.AsInteger=1 then  //��� �����
          begin
            if abs(quRemnDateREMN.AsFloat)>0.000001 then bAdd:=True; //�� ������� ���� !!!
          end
          else bAdd:=True;

          if bAdd then
          begin
            //��� ��� ������� ����� ������� - ����� ���� �� ������ ���������
            //Function prCalcRemnSum(iCode,iDate,iSkl:Integer;rQ:Real):Real;
            //MOV.REMN/ME.KOEF
            rQMain:=quRemnDateREMN.AsFloat*quRemnDateKOEF.AsFloat;
            rSum:=prCalcRemnSum(quRemnDateID.AsInteger,Trunc(CommonSet.DateFrom-1),CommonSet.IdStore,rQMain);

            taObVed.Append;
            taObVedIdCode.AsInteger:=quRemnDateID.AsInteger;
            taObVedNameC.AsString:=quRemnDateNAME.AsString;
            taObVediM.AsInteger:=quRemnDateIMESSURE.AsInteger;
            taObVedSm.AsString:=quRemnDateNAMEMESSURE.AsString;
            taObVedIdGroup.AsInteger:=quRemnDatePARENT.AsInteger;
            taObVedNameClass.AsString:=quRemnDateNAMECL.AsString;
            taObVedQBeg.AsFloat:=quRemnDateREMN.AsFloat;
            taObVedSBeg.AsFloat:=rSum;
            taObVedQIn.AsFloat:=0;
            taObVedSIn.AsFloat:=0;
            taObVedQOut.AsFloat:=0;
            taObVedSOut.AsFloat:=0;
            taObVedPrice.AsFloat:=0;
            taObVedKm.AsFloat:=quRemnDateKOEF.AsFloat;
            taObVed.Post;
          end;
        quRemnDate.Next; delay(10);
      end;

      quRemnDate.Active:=False;

      fmRepOb.Memo1.Lines.Add('  ��������� ��������.'); delay(10);

      quSelInSum.Active:=False;
      quSelInSum.ParamByName('DATEB').AsInteger:=Trunc(CommonSet.DateFrom);
      quSelInSum.ParamByName('DATEE').AsInteger:=Trunc(CommonSet.DateTo);
      quSelInSum.ParamByName('IDSKL').AsInteger:=CommonSet.IdStore;
      quSelInSum.Active:=True;

      quSelInSum.First;
      while not quSelInSum.Eof do
      begin
          if taObVed.Locate('IdCode',quSelInSumARTICUL.AsInteger,[]) then
          begin
            rKm:=taObVedKm.AsFloat;
            taObVed.Edit;
            if rKm<>0 then taObVedQIn.AsFloat:=quSelInSumQSUM.AsFloat/rKm
            else taObVedQIn.AsFloat:=0;
            taObVedSIn.AsFloat:=quSelInSumSUMIN.AsFloat;
            taObVed.Post;
          end else
          begin  //���� ���������
            quSelNamePos.Active:=False;
            quSelNamePos.ParamByName('IDCARD').AsInteger:=quSelInSumARTICUL.AsInteger;
            quSelNamePos.Active:=True;

            quSelNamePos.First;
            if quSelNamePos.RecordCount>0 then
            begin
              rKm:=quSelNamePosKOEF.AsFloat;
              taObVed.Append;
              taObVedIdCode.AsInteger:=quSelInSumARTICUL.AsInteger;
              taObVedNameC.AsString:=quSelNamePosNAME.AsString;
              taObVediM.AsInteger:=quSelNamePosIMESSURE.AsInteger;
              taObVedSm.AsString:=quSelNamePosSM.AsString;
              taObVedIdGroup.AsInteger:=quSelNamePosPARENT.AsInteger;
              taObVedNameClass.AsString:=quSelNamePosNAMECL.AsString;
              taObVedQBeg.AsFloat:=0;
              taObVedSBeg.AsFloat:=0;
              taObVedQIn.AsFloat:=0;

              if rKm<>0 then taObVedQIn.AsFloat:=quSelInSumQSUM.AsFloat/rKm;
              taObVedSIn.AsFloat:=quSelInSumSUMIN.AsFloat;

              taObVedQOut.AsFloat:=0;
              taObVedSOut.AsFloat:=0;
              taObVedPrice.AsFloat:=0;
              taObVedKm.AsFloat:=quSelNamePosKOEF.AsFloat;
              taObVed.Post;
            end;
            quSelNamePos.Active:=False;
          end;
        quSelInSum.Next;
      end;
      quSelInSum.Active:=False;

      fmRepOb.Memo1.Lines.Add('  ��������� ��������.'); delay(10);

      quSelOutSum.Active:=False;
      quSelOutSum.ParamByName('DATEB').AsInteger:=Trunc(CommonSet.DateFrom);
      quSelOutSum.ParamByName('DATEE').AsInteger:=Trunc(CommonSet.DateTo);
      quSelOutSum.ParamByName('IDSKL').AsInteger:=CommonSet.IdStore;
      quSelOutSum.Active:=True;

      quSelOutSum.First;
      while not quSelOutSum.Eof do
      begin
          if taObVed.Locate('IdCode',quSelOutSumARTICUL.AsInteger,[]) then
          begin
            rKm:=taObVedKm.AsFloat;
            taObVed.Edit;
            if rKm<>0 then taObVedQOut.AsFloat:=quSelOutSumQSUM.AsFloat/rKm
            else taObVedQOut.AsFloat:=0;
            taObVedSOut.AsFloat:=quSelOutSumSUMOUT.AsFloat;
            taObVed.Post;
          end else
          begin  //���� ���������
            quSelNamePos.Active:=False;
            quSelNamePos.ParamByName('IDCARD').AsInteger:=quSelOutSumARTICUL.AsInteger;
            quSelNamePos.Active:=True;

            quSelNamePos.First;
            if quSelNamePos.RecordCount>0 then
            begin
              rKm:=quSelNamePosKOEF.AsFloat;
              taObVed.Append;
              taObVedIdCode.AsInteger:=quSelOutSumARTICUL.AsInteger;
              taObVedNameC.AsString:=quSelNamePosNAME.AsString;
              taObVediM.AsInteger:=quSelNamePosIMESSURE.AsInteger;
              taObVedSm.AsString:=quSelNamePosSM.AsString;
              taObVedIdGroup.AsInteger:=quSelNamePosPARENT.AsInteger;
              taObVedNameClass.AsString:=quSelNamePosNAMECL.AsString;
              taObVedQBeg.AsFloat:=0;
              taObVedSBeg.AsFloat:=0;
              taObVedQIn.AsFloat:=0;
              taObVedSIn.AsFloat:=0;

              taObVedQOut.AsFloat:=0;
              if rKm<>0 then taObVedQOut.AsFloat:=quSelOutSumQSUM.AsFloat/rKm;
              taObVedSOut.AsFloat:=quSelOutSumSUMOut.AsFloat;

              taObVedPrice.AsFloat:=0;
              taObVedKm.AsFloat:=quSelNamePosKOEF.AsFloat;
              taObVed.Post;
            end;
            quSelNamePos.Active:=False;
          end;
        quSelOutSum.Next;
      end;
      quSelOutSum.Active:=False;

      ViewObVed.EndUpdate;

      Memo1.Lines.Add('������������ ��.'); delay(10);
    end;
  end else
  begin
    fmSelPerSkl.quMHAll1.Active:=False;
    fmSelPerSkl.Release;
  end;
end;

procedure TfmRepOb.SpeedItem3Click(Sender: TObject);
begin
  prExportExel2(ViewObVed,dsObVed,taObVed);
end;

procedure TfmRepOb.SpeedItem4Click(Sender: TObject);
Var StrWk:String;
    i:Integer;
begin
//������
  ViewObVed.BeginUpdate;
  taObVed.Filter:=ViewObVed.DataController.Filter.FilterText;
  taObVed.Filtered:=True;

  frRepOb.LoadFromFile(CurDir + 'ObVed.frf');

  if CommonSet.DateTo>=iMaxDate then frVariables.Variable['sPeriod']:=' � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
  else frVariables.Variable['sPeriod']:=' �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);

  frVariables.Variable['DocStore']:=CommonSet.NameStore;

  StrWk:=ViewObVed.DataController.Filter.FilterCaption;
  while Pos('AND',StrWk)>0 do
  begin
    i:=Pos('AND',StrWk);
    StrWk[i]:=' ';
    StrWk[i+1]:='�';
    StrWk[i+2]:=' ';
  end;
  while Pos('and',StrWk)>0 do
  begin
    i:=Pos('and',StrWk);
    StrWk[i]:=' ';
    StrWk[i+1]:='�';
    StrWk[i+2]:=' ';
  end;

  frVariables.Variable['DocPars']:=StrWk;

  frRepOb.ReportName:='��������� ���������.';
  frRepOb.PrepareReport;
  frRepOb.ShowPreparedReport;

  taObVed.Filter:='';
  taObVed.Filtered:=False;
  ViewObVed.EndUpdate;
end;

procedure TfmRepOb.taObVedCalcFields(DataSet: TDataSet);
begin
  taObVedQRes.AsFloat:=taObVedQBeg.AsFloat+taObVedQIn.AsFloat-taObVedQOut.AsFloat;
  taObVedSRes.AsCurrency:=taObVedSBeg.AsCurrency+taObVedSIn.AsCurrency-taObVedSOut.AsCurrency;
end;

end.
