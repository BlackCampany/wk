unit RepPrib;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxTextEdit, cxImageComboBox,
  DBClient, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxControls, cxGridCustomView, cxGrid,
  cxContainer, cxMemo, ExtCtrls, SpeedBar, ComCtrls, Placemnt, FR_DSet,
  FR_DBSet, FR_Class;

type
  TfmRepPrib = class(TForm)
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    Panel1: TPanel;
    Memo1: TcxMemo;
    GridPrib: TcxGrid;
    ViewPrib: TcxGridDBTableView;
    LevelPrib: TcxGridLevel;
    taPrib: TClientDataSet;
    dsPrib: TDataSource;
    taPribCodeB: TIntegerField;
    taPribName: TStringField;
    taPribParent: TIntegerField;
    taPribNameClass: TStringField;
    taPribQuant: TFloatField;
    taPribSumOut: TFloatField;
    taPribSumIn: TFloatField;
    taPribPriceIn: TFloatField;
    taPribPriceOut: TFloatField;
    taPribPr1: TFloatField;
    taPribPr2: TFloatField;
    taPribPr3: TFloatField;
    taPribDiff: TFloatField;
    taPribDiffSum: TFloatField;
    ViewPribCodeB: TcxGridDBColumn;
    ViewPribName: TcxGridDBColumn;
    ViewPribParent: TcxGridDBColumn;
    ViewPribNameClass: TcxGridDBColumn;
    ViewPribQuant: TcxGridDBColumn;
    ViewPribSumOut: TcxGridDBColumn;
    ViewPribSumIn: TcxGridDBColumn;
    ViewPribPriceIn: TcxGridDBColumn;
    ViewPribPriceOut: TcxGridDBColumn;
    ViewPribPr1: TcxGridDBColumn;
    ViewPribPr2: TcxGridDBColumn;
    ViewPribPr3: TcxGridDBColumn;
    ViewPribDiff: TcxGridDBColumn;
    ViewPribDiffSum: TcxGridDBColumn;
    FormPlacement1: TFormPlacement;
    frRepPrib: TfrReport;
    frtaPrib: TfrDBDataSet;
    SpeedItem4: TSpeedItem;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
    procedure SpeedItem4Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmRepPrib: TfmRepPrib;

implementation

uses Un1, SelPerSkl, dmOffice, DMOReps;

{$R *.dfm}

procedure TfmRepPrib.FormCreate(Sender: TObject);
begin
  GridPrib.Align:=AlClient;
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  ViewPrib.RestoreFromIniFile(CurDir+GridIni);
end;

procedure TfmRepPrib.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewPrib.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmRepPrib.FormShow(Sender: TObject);
begin
  Memo1.Clear;
end;

procedure TfmRepPrib.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmRepPrib.SpeedItem2Click(Sender: TObject);
Var rSumOut,rSumMarg:Real;
    iGr:Integer;
begin
  //����� �� �������
  fmSelPerSkl:=tfmSelPerSkl.Create(Application);
  with fmSelPerSkl do
  begin
    cxDateEdit1.Date:=CommonSet.DateFrom;
    quMHAll1.Active:=False;
    quMHAll1.Active:=True;
    quMHAll1.First;
    cxLookupComboBox1.EditValue:=quMHAll1ID.AsInteger;
  end;
  fmSelPerSkl.ShowModal;
  if fmSelPerSkl.ModalResult=mrOk then
  begin
    CommonSet.DateFrom:=Trunc(fmSelPerSkl.cxDateEdit1.Date);
    CommonSet.DateTo:=Trunc(fmSelPerSkl.cxDateEdit2.Date)+1;
    CommonSet.IdStore:=fmSelPerSkl.cxLookupComboBox1.EditValue;
    CommonSet.NameStore:=fmSelPerSkl.cxLookupComboBox1.Text;

    if CommonSet.DateTo>=iMaxDate then fmRepPrib.Caption:='����� �� ������� �� �� '+CommonSet.NameStore+' � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
    else fmRepPrib.Caption:='����� �� ������� �� �� '+CommonSet.NameStore+' �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);
    fmSelPerSkl.quMHAll1.Active:=False;
    fmSelPerSkl.Release;
    with dmO do
    with dmORep do
    begin
       Memo1.Clear;
       Memo1.Lines.Add('����� .. ���� ������������ ������.'); delay(10);
       Memo1.Lines.Add('  ���������� �� ������.'); delay(10);

       quSelPrib.Active:=False;
       quSelPrib.ParamByName('DATEB').AsDateTime:=CommonSet.DateFrom;
       quSelPrib.ParamByName('DATEE').AsDateTime:=CommonSet.DateTo;
       quSelPrib.ParamByName('IDSTORE').AsInteger:=CommonSet.IdStore;
       quSelPrib.Active:=True;

       Memo1.Lines.Add('  ���������� �� �������.'); delay(10);

       quSelPribGr.Active:=False;
       quSelPribGr.ParamByName('DATEB').AsDateTime:=CommonSet.DateFrom;
       quSelPribGr.ParamByName('DATEE').AsDateTime:=CommonSet.DateTo;
       quSelPribGr.ParamByName('IDSTORE').AsInteger:=CommonSet.IdStore;
       quSelPribGr.Active:=True;

       Memo1.Lines.Add('  ��������� ������.'); delay(10);

       ViewPrib.BeginUpdate;
       taPrib.Active:=False;
       taPrib.CreateDataSet;

       quSelPrib.First;
       while not quSelPrib.Eof do
       begin
         taPrib.Append;
         taPribCodeB.AsInteger:=quSelPribCODEB.AsInteger;
         taPribName.AsString:=quSelPribNAME.AsString;
         taPribParent.AsInteger:=quSelPribPARENT.AsInteger;
         taPribNameClass.AsString:=quSelPribNAMECL.AsString;
         taPribQuant.AsFloat:=quSelPribQUANT.AsFloat;
         taPribSumOut.AsFloat:=quSelPribSUMOUT.AsFloat;
         taPribSumIn.AsFloat:=quSelPribSUMIN.AsFloat;
         taPribPriceIn.AsFloat:=0;
         taPribPriceOut.AsFloat:=0;
         taPribPr1.AsFloat:=0;
         taPribPr2.AsFloat:=0;
         taPribPr3.AsFloat:=0;
         taPribDiff.AsFloat:=0;
         taPribDiffSum.AsFloat:=quSelPribSUMOUT.AsFloat-quSelPribSUMIN.AsFloat;

         if abs(quSelPribQUANT.AsFloat)>0 then
         begin
           taPribPriceIn.AsFloat:=RoundEx(quSelPribSUMIN.AsFloat/quSelPribQUANT.AsFloat*100)/100;
           taPribPriceOut.AsFloat:=RoundEx(quSelPribSUMOUT.AsFloat/quSelPribQUANT.AsFloat*100)/100;
           taPribDiff.AsFloat:=RoundEx(quSelPribSUMOUT.AsFloat/quSelPribQUANT.AsFloat*100)/100-RoundEx(quSelPribSUMIN.AsFloat/quSelPribQUANT.AsFloat*100)/100;
         end;
         if abs(quSelPribSUMOUT.AsFloat)>0 then
         begin
           taPribPr2.AsFloat:=RoundEx(quSelPribSUMIN.AsFloat/quSelPribSUMOUT.AsFloat*1000)/10;
         end;

         iGr:=quSelPribPARENT.AsInteger;
         if quSelPribGr.Locate('PARENT',iGr,[]) then
         begin
           rSumOut:=quSelPribGrSUMOUT.AsFloat;
           rSumMarg:=quSelPribGrSUMOUT.AsFloat-quSelPribGrSUMIN.AsFloat;
           if rSumOut<>0 then
           begin
             taPribPr1.AsFloat:=RoundEx(quSelPribSUMOUT.AsFloat/rSumOut*1000)/10;
           end;
           if rSumMarg<>0 then
           begin
             taPribPr3.AsFloat:=RoundEx((quSelPribSUMOUT.AsFloat-quSelPribSUMIN.AsFloat)/rSumMarg*1000)/10;
           end;
         end;
         taPrib.Post;

         quSelPrib.Next;
       end;
       ViewPrib.EndUpdate;

       Memo1.Lines.Add('������������ ��.'); delay(10);
       quSelPrib.Active:=False;
       quSelPribGr.Active:=False;
    end;
  end else
  begin
    fmSelPerSkl.quMHAll1.Active:=False;
    fmSelPerSkl.Release;
  end;
end;

procedure TfmRepPrib.SpeedItem3Click(Sender: TObject);
begin
  prExportExel1(ViewPrib,dsPrib,taPrib);
end;

procedure TfmRepPrib.SpeedItem4Click(Sender: TObject);
Var StrWk:String;
    i:Integer;
begin
//������
  ViewPrib.BeginUpdate;
  taPrib.Filter:=ViewPrib.DataController.Filter.FilterText;
  taPrib.Filtered:=True;

  frRepPrib.LoadFromFile(CurDir + 'Prib.frf');

  if CommonSet.DateTo>=iMaxDate then frVariables.Variable['sPeriod']:=' � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
  else frVariables.Variable['sPeriod']:=' �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);

  frVariables.Variable['DocStore']:=CommonSet.NameStore;

  StrWk:=ViewPrib.DataController.Filter.FilterCaption;
  while Pos('AND',StrWk)>0 do
  begin
    i:=Pos('AND',StrWk);
    StrWk[i]:=' ';
    StrWk[i+1]:='�';
    StrWk[i+2]:=' ';
  end;
  while Pos('and',StrWk)>0 do
  begin
    i:=Pos('and',StrWk);
    StrWk[i]:=' ';
    StrWk[i+1]:='�';
    StrWk[i+2]:=' ';
  end;

  frVariables.Variable['DocPars']:=StrWk;

  frRepPrib.ReportName:='����� � �������.';
  frRepPrib.PrepareReport;
  frRepPrib.ShowPreparedReport;

  taPrib.Filter:='';
  taPrib.Filtered:=False;
  ViewPrib.EndUpdate;
end;

end.
