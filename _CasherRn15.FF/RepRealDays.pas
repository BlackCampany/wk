unit RepRealDays;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ComCtrls, cxCurrencyEdit, cxTextEdit,
  ExtCtrls, SpeedBar, Placemnt, Grids, DBGrids,
  ComObj, ActiveX, Excel2000, OleServer, ExcelXP;

type
  TfmRealDays = class(TForm)
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    FormPlacement1: TFormPlacement;
    ViewRealD: TcxGridDBTableView;
    LevelRealD: TcxGridLevel;
    GridRealD: TcxGrid;
    ViewRealDDATER: TcxGridDBColumn;
    ViewRealDSIFR: TcxGridDBColumn;
    ViewRealDPRICE: TcxGridDBColumn;
    ViewRealDNAME: TcxGridDBColumn;
    ViewRealDNAMEGROUP: TcxGridDBColumn;
    ViewRealDNAMECAT: TcxGridDBColumn;
    ViewRealDQSUM: TcxGridDBColumn;
    ViewRealDSSUM: TcxGridDBColumn;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    procedure FormCreate(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SpeedItem2Click(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function IsOLEObjectInstalled(Name: String): boolean;

var
  fmRealDays: TfmRealDays;

implementation

uses Un1, Dm, Period2;

{$R *.dfm}

function IsOLEObjectInstalled(Name: String): boolean;
var
  ClassID: TCLSID;
  Rez : HRESULT;
begin

// ���� CLSID OLE-�������
  Rez := CLSIDFromProgID(PWideChar(WideString(Name)), ClassID);
  if Rez = S_OK then
 // ������ ������
    Result := true
  else
    Result := false;
end;


procedure TfmRealDays.FormCreate(Sender: TObject);
begin
//  GridRealDays.Align:=AlClient;
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  ViewRealD.RestoreFromIniFile(CurDir+GridIni);
end;

procedure TfmRealDays.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmRealDays.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  dmC.quRep9.Active:=False;
  ViewRealD.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmRealDays.SpeedItem2Click(Sender: TObject);
Var DateBeg,DateEnd:TDateTime;
    bSt:Boolean;
begin
  //��������� ������
  fmPeriod2:=TfmPeriod2.Create(Application);
  fmPeriod2.DateTimePicker1.Date:=Trunc(TrebSel.DateFrom);
  fmPeriod2.DateTimePicker2.Date:=Trunc(TrebSel.DateTo);
  fmPeriod2.DateTimePicker3.Visible:=False;
  fmPeriod2.DateTimePicker4.Visible:=False;
  fmPeriod2.Label3.Visible:=False;
  fmPeriod2.Label4.Visible:=False;

  bSt:=False;
  fmPeriod2.ShowModal;
  if fmPeriod2.ModalResult=mrOk then  bSt:=True;
  fmPeriod2.Release;
  if bSt then
  begin
    ViewRealD.BeginUpdate;
    Datebeg:=Trunc(fmPeriod2.DateTimePicker1.Date);
    DateEnd:=Trunc(fmPeriod2.DateTimePicker2.Date);
    fmRealDays.Caption:='���������� �� ���� �� ������ c '+FormatDateTime('dd.mm.yyyy',DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy',DateEnd);

    with dmC do
    begin
      quRep9.Active:=False;
      quRep9.ParamByName('DATEB').AsDate:=Datebeg;
      quRep9.ParamByName('DATEE').AsDate:=DateEnd; //��� ������ <, ��  TrebSel.DateTo ��� +1
      quRep9.Active:=True;

      fmRealDays.Show;
    end;
    ViewRealD.EndUpdate;
  end;
end;

procedure TfmRealDays.SpeedItem3Click(Sender: TObject);
Var ExcelApp, Workbook, Range, Cell1, Cell2, ArrayData  : Variant;
    iCol,iRow:Integer;
begin
  //������� � ������
  with dmC do
  begin
    StatusBar1.Panels[0].Text:='����� ���� ������������.'; delay(10);
    ViewRealD.BeginUpdate;

    dsRep9.DataSet:=nil;
    quRep9.Filter:=ViewRealD.DataController.Filter.FilterText;
    quRep9.Filtered:=True;

    //����� ������ ������
    if not IsOLEObjectInstalled('Excel.Application') then exit;
    ExcelApp := CreateOleObject('Excel.Application');
    ExcelApp.Application.EnableEvents := false;
    //� ������� �����
    Workbook := ExcelApp.WorkBooks.Add;

    iCol:=8;
    iRow:=quRep9.RecordCount+1;
    ArrayData := VarArrayCreate([1,iRow, 1, iCol], varVariant);

    ArrayData[1,1] := '����';
    ArrayData[1,2] := '���';
    ArrayData[1,3] := '��������';
    ArrayData[1,4] := '������';
    ArrayData[1,5] := '���������';
    ArrayData[1,6] := '����';
    ArrayData[1,7] := '���-��';
    ArrayData[1,8] := '�����';

    StatusBar1.Panels[0].Text:='���� ���������� ������.'; delay(10);

    quRep9.First;
    iRow:=2;
    While not quRep9.eof do
    begin

      ArrayData[iRow,1] := quRep9DATER.AsString;
      ArrayData[iRow,2] := quRep9SIFR.AsInteger;
      ArrayData[iRow,3] := quRep9NAME.AsString;
      ArrayData[iRow,4] := quRep9NAMEGROUP.AsString;
      ArrayData[iRow,5] := quRep9NAMECAT.AsString;
      ArrayData[iRow,6] := quRep9PRICE.AsFloat;
      ArrayData[iRow,7] := quRep9QSUM.AsFloat;
      ArrayData[iRow,8] := quRep9SSUM.AsFloat;

      quRep9.Next; //Delay(10);
      inc(iRow);
    end;

    StatusBar1.Panels[0].Text:='���� ������� ������.'; delay(10);

    Cell1 := WorkBook.WorkSheets[1].Cells[1,1];
    Cell2 := WorkBook.WorkSheets[1].Cells[1+quRep9.RecordCount,iCol];
    Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
    Range.Value := ArrayData;

//    Range.Borders.LineStyle := xlDouble;
//    Range.Borders.Color := RGB(0,0,0);

    quRep9.Filter:='';
    quRep9.Filtered:=False;

    quRep9.First;
    dsRep9.DataSet:=quRep9;

    ViewRealD.EndUpdate;
    StatusBar1.Panels[0].Text:='������������ ��.';

    ExcelApp.Visible := true;
  end;
end;

end.
