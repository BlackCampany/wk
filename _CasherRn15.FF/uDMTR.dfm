object dmTR: TdmTR
  OldCreateOrder = False
  Left = 408
  Top = 252
  Height = 242
  Width = 347
  object TRDb: TpFIBDatabase
    DBName = 'localhost:C:\_CasherRn\DB2\CASHERRN.GDB'
    DBParams.Strings = (
      'user_name=SYSDBA'
      'lc_ctype=WIN1251'
      'password=masterkey'
      'sql_role_name=SYSDBA')
    DefaultTransaction = trSelTR
    DefaultUpdateTransaction = trUpdTR
    SQLDialect = 3
    Timeout = 0
    SynchronizeTime = False
    DesignDBOptions = []
    AliasName = 'TRDb'
    WaitForRestoreConnect = 0
    Left = 20
    Top = 15
  end
  object trSelTR: TpFIBTransaction
    DefaultDatabase = TRDb
    TimeoutAction = TARollback
    Left = 20
    Top = 68
  end
  object trUpdTR: TpFIBTransaction
    DefaultDatabase = TRDb
    TimeoutAction = TARollback
    Left = 20
    Top = 120
  end
  object quTr: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT tr.SIFR,'
      '       me.NAME,'
      '       me.PRICE,'
      '       me.CODE,'
      '       me.TREETYPE,'
      '       me.LIMITPRICE,'
      '       me.CATEG,'
      '       me.PARENT,'
      '       me.LINK,'
      '       me.STREAM,'
      '       me.LACK,'
      '       me.DESIGNSIFR,'
      '       me.ALTNAME,'
      '       me.NALOG,'
      '       me.BARCODE,'
      '       me.IMAGE,'
      '       me.CONSUMMA,'
      '       me.MINREST,'
      '       me.PRNREST,'
      '       me.COOKTIME,'
      '       me.DISPENSER,'
      '       me.DISPKOEF,'
      '       me.ACCESS,'
      '       me.FLAGS,'
      '       me.TARA,'
      '       me.CNTPRICE,'
      '       me.BACKBGR,'
      '       me.FONTBGR,'
      '       me.IACTIVE,'
      '       me.IEDIT,'
      '       me.DATEB,'
      '       me.DATEE,'
      '       me.DAYWEEK,'
      '       me.TIMEB,'
      '       me.TIMEE,'
      '       me.ALLTIME'
      'FROM TRANS tr'
      'left join MENU me on me.SIFR=tr.SIFR'
      'where tr.STATION=:ST'
      'and tr.IACTIVE=2'
      'and tr.ITYPE=1'
      ''
      '')
    Transaction = trSelTR
    Database = TRDb
    UpdateTransaction = trUpdTR
    AutoCommit = True
    Left = 92
    Top = 16
    object quTrSIFR: TFIBIntegerField
      FieldName = 'SIFR'
    end
    object quTrNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 100
      EmptyStrToNull = True
    end
    object quTrPRICE: TFIBFloatField
      FieldName = 'PRICE'
    end
    object quTrCODE: TFIBStringField
      FieldName = 'CODE'
      Size = 10
      EmptyStrToNull = True
    end
    object quTrTREETYPE: TFIBStringField
      FieldName = 'TREETYPE'
      Size = 1
      EmptyStrToNull = True
    end
    object quTrLIMITPRICE: TFIBFloatField
      FieldName = 'LIMITPRICE'
    end
    object quTrCATEG: TFIBSmallIntField
      FieldName = 'CATEG'
    end
    object quTrPARENT: TFIBSmallIntField
      FieldName = 'PARENT'
    end
    object quTrLINK: TFIBSmallIntField
      FieldName = 'LINK'
    end
    object quTrSTREAM: TFIBSmallIntField
      FieldName = 'STREAM'
    end
    object quTrLACK: TFIBSmallIntField
      FieldName = 'LACK'
    end
    object quTrDESIGNSIFR: TFIBSmallIntField
      FieldName = 'DESIGNSIFR'
    end
    object quTrALTNAME: TFIBStringField
      FieldName = 'ALTNAME'
      Size = 100
      EmptyStrToNull = True
    end
    object quTrNALOG: TFIBFloatField
      FieldName = 'NALOG'
    end
    object quTrBARCODE: TFIBStringField
      FieldName = 'BARCODE'
      EmptyStrToNull = True
    end
    object quTrIMAGE: TFIBSmallIntField
      FieldName = 'IMAGE'
    end
    object quTrCONSUMMA: TFIBFloatField
      FieldName = 'CONSUMMA'
    end
    object quTrMINREST: TFIBSmallIntField
      FieldName = 'MINREST'
    end
    object quTrPRNREST: TFIBSmallIntField
      FieldName = 'PRNREST'
    end
    object quTrCOOKTIME: TFIBSmallIntField
      FieldName = 'COOKTIME'
    end
    object quTrDISPENSER: TFIBSmallIntField
      FieldName = 'DISPENSER'
    end
    object quTrDISPKOEF: TFIBSmallIntField
      FieldName = 'DISPKOEF'
    end
    object quTrACCESS: TFIBSmallIntField
      FieldName = 'ACCESS'
    end
    object quTrFLAGS: TFIBSmallIntField
      FieldName = 'FLAGS'
    end
    object quTrTARA: TFIBSmallIntField
      FieldName = 'TARA'
    end
    object quTrCNTPRICE: TFIBSmallIntField
      FieldName = 'CNTPRICE'
    end
    object quTrBACKBGR: TFIBFloatField
      FieldName = 'BACKBGR'
    end
    object quTrFONTBGR: TFIBFloatField
      FieldName = 'FONTBGR'
    end
    object quTrIACTIVE: TFIBSmallIntField
      FieldName = 'IACTIVE'
    end
    object quTrIEDIT: TFIBSmallIntField
      FieldName = 'IEDIT'
    end
    object quTrDATEB: TFIBIntegerField
      FieldName = 'DATEB'
    end
    object quTrDATEE: TFIBIntegerField
      FieldName = 'DATEE'
    end
    object quTrDAYWEEK: TFIBStringField
      FieldName = 'DAYWEEK'
      Size = 10
      EmptyStrToNull = True
    end
    object quTrTIMEB: TFIBTimeField
      FieldName = 'TIMEB'
    end
    object quTrTIMEE: TFIBTimeField
      FieldName = 'TIMEE'
    end
    object quTrALLTIME: TFIBSmallIntField
      FieldName = 'ALLTIME'
    end
  end
  object quUpd1: TpFIBQuery
    Transaction = trUpdTR
    Database = TRDb
    SQL.Strings = (
      'Update TRANS Set IACTIVE=2 '
      'where IACTIVE=1'
      '    ')
    Left = 148
    Top = 16
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object quUpd2: TpFIBQuery
    Transaction = trUpdTR
    Database = TRDb
    SQL.Strings = (
      'Update TRANS Set IACTIVE=3 '
      'where IACTIVE=2'
      '    ')
    Left = 208
    Top = 16
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object prTRADDCASHSAIL: TpFIBStoredProc
    Transaction = trUpdTR
    Database = TRDb
    SQL.Strings = (
      
        'EXECUTE PROCEDURE TRADDCASHSAIL (?CASHNUM, ?ZNUM, ?CHECKNUM, ?TA' +
        'B_ID, ?TABSUM, ?CLIENTSUM, ?CASHERID, ?WAITERID, ?CHDATE, ?PAYTY' +
        'PE, ?PAYID, ?PAYBAR, ?IACTIVE)')
    StoredProcName = 'TRADDCASHSAIL'
    Left = 92
    Top = 80
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object prTRADDTABLES: TpFIBStoredProc
    Transaction = trUpdTR
    Database = TRDb
    SQL.Strings = (
      
        'EXECUTE PROCEDURE TRADDTABLES (?ID_PERSONAL, ?NUMTABLE, ?QUESTS,' +
        ' ?TABSUM, ?BEGTIME, ?ENDTIME, ?DISCONT, ?OPERTYPE, ?CHECKNUM, ?S' +
        'KLAD, ?SPBAR, ?STATION, ?NUMZ, ?SALET)')
    StoredProcName = 'TRADDTABLES'
    Left = 196
    Top = 80
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object prTRADDSPECT: TpFIBStoredProc
    Transaction = trUpdTR
    Database = TRDb
    SQL.Strings = (
      
        'EXECUTE PROCEDURE TRADDSPECT (?ID, ?ID_TAB, ?ID_PERSONAL, ?NUMTA' +
        'BLE, ?SIFR, ?PRICE, ?QUANTITY, ?DISCOUNTPROC, ?DISCOUNTSUM, ?SUM' +
        'MA, ?ISTATUS, ?ITYPE, ?QUANTITY1, ?INEED, ?IPRINT, ?STREAM)')
    StoredProcName = 'TRADDSPECT'
    Left = 196
    Top = 136
    qoAutoCommit = True
    qoStartTransaction = True
  end
end
