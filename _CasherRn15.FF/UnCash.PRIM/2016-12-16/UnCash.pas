unit UnCash;

interface
uses
//  ������� ����-08

  Windows, SysUtils, Classes, Controls, Forms;


Function InspectSt(Var sMess:String):Integer;
Function GetSt(Var sMess:String):INteger;
Function OpenZ:Boolean;  //�������
Function GetSerial:Boolean;
Function GetNums:Boolean;
Function GetSums(Var rSum1,rSum2,rSum3:Real):Boolean;
Function GetRes:Boolean;

Function CashOpen(sPort:PChar):Boolean;
Function CashClose:Boolean;
Function CashDate(var sDate:String):Boolean;
Function GetX:Boolean;
Function ZOpen:Boolean; //������ �����
Function ZClose:Boolean;
Function CheckStart:Boolean;
Function CheckRetStart:Boolean;
Function CheckAddPos(Var iSum:Integer):Boolean;
Function CheckTotal(Var iTotal:Integer):Boolean;
Function CheckDiscount(rDiscount:Real;Var iDisc,iItog:Integer):Boolean;
Function CheckRasch(iPayType,iClient:Integer; Comment:String; Var iDopl:Integer):Boolean;
Function CheckClose:Boolean;
Procedure CheckCancel;
Procedure InCass(rSum:Real);//����������
Function GetCashReg(Var rSum:Real):Boolean;//���������� � �����

Function CashDriver:Boolean;
Procedure TestPosCh;

Function OpenNFDoc:Boolean;
Function CloseNFDoc:Boolean;
Function PrintNFStr(StrPr:String):Boolean;
Function SelectF(iNum:Integer):Boolean;
Function CutDoc:Boolean;
Function CutDoc1:Boolean;

procedure WriteHistoryFR(Strwk_: string);
Function TestStatus(StrOp:String; Var SMess:String):Boolean;
function More24H(Var iSt:INteger):Boolean;
Function CheckOpen:Boolean;
Function SetDP(Str1,Str2:String):Boolean;

procedure prBnPrint;
Procedure TestFp(StrOp:String);

Type
     TStatusFR = record
     St1:String[2];
     St2:String[4];
     St3:String[4];
     St4:String[10];
     end;

     TNums = record
     bOpen:Boolean;
     ZNum:Integer;
     SerNum:String[11];
     RegNum:string[10];
     CheckNum:string[4];
     iCheckNum:Integer;
     iRet:INteger;
     sRet:String;
     sDate:String;
     ZYet:Integer;
     sDateTimeBeg:String;
     end;

     TCurPos = record
     Articul:String;
     Bar:String;
     Name:String;
     EdIzm:String;
     Quant:Real;
     Price:Currency;
     DProc:Real;
     DSum:Currency;
     Depart:SmallInt;
     end;

     TPos = record
     Name:String;
     Code:String;
     AddName:String;
     Price:Integer;
     Count:Integer;
     Stream: Integer;
     Sum:Real;
     end;

var
  CommandsArray : array [1..32000] of Char;
  HeapStatus    : THeapStatus;
  PressCount    : Byte;
  StatusFr      : TStatusFr;
  Nums          : TNums;
//  CurPos        :TCurPos;
//  SelPos        :TCurPos;
  sMessage      :String;
  PosCh         : TPos;



implementation

uses Un1, Attention;

{$I dll.inc}


Procedure TestFp(StrOp:String);
begin
  while TestStatus(StrOp,sMessage)=False do
  begin
    fmAttention.Label1.Caption:=sMessage;
    fmAttention.ShowModal;
    Nums.iRet:=InspectSt(Nums.sRet);
    prWriteLog('~~AttentionShow;'+sMessage+';'+'InspectSt(Nums.sRet)='+IntToStr(Nums.iRet));
  end;
end;


procedure prBnPrint;
begin
//����� ����� ������ ������ ����� ���������� �� ����������
  try
//      with fmMainCasher do
//      begin
          if  BnStr>'' then
          begin
            if OpenNFDoc=false then
            begin
              TestFp('PrintNFStr');
              delay(100);
              OpenNFDoc;
            end;

            while BnStr>'' do
            begin
              if (ord(BnStr[1])=$0D)or(ord(BnStr[1])=$0A) then Delete(BnStr,1,1)
              else
              begin
                if ord(BnStr[1])=$01 then
                begin
                  try
                    if CloseNFDoc=False then
                    begin
                      TestFp('PrintNFStr');
                      CloseNFDoc;
                    end;

                    if (CommonSet.TypeFis='prim')or(CommonSet.TypeFis='shtrih') then
                    begin
                      PrintNFStr(' ');
                      PrintNFStr(' ');
                      PrintNFStr(' ');
                      PrintNFStr(' ');
                      PrintNFStr(' ');
                      PrintNFStr(' ');
                      CutDoc;
                    end;

                    delay(33);

                    if OpenNFDoc=false then
                    begin
                      TestFp('PrintNFStr');
                      OpenNFDoc;
                    end;
                  finally
                    Delete(BnStr,1,1);
                  end;
                end
                else
                begin
                  if Pos(Chr($0D),BnStr)>0 then
                  begin
                    StrWk:=Copy(BnStr,1,Pos(Chr($0D),BnStr)-1);
                    Delete(BnStr,1,Pos(Chr($0D),BnStr));
                  end
                  else
                  begin
                    if Pos(Chr($01),BnStr)>0 then
                    begin
                      StrWk:=Copy(BnStr,1,Pos(Chr($01),BnStr)-1);
                      Delete(BnStr,1,Pos(Chr($01),BnStr)-1);
                    end
                    else
                    begin
                      StrWk:=BnStr;
                      BnStr:='';
                    end;
                  end;
                  if pos('�����',StrWk)>0 then PrintNFStr('')
                  else
                  if PrintNFStr(StrWk)=False then
                  begin
                     TestFp('PrintNFStr');
                     PrintNFStr(StrWk);
                  end;
                end;
              end;
            end;

            if (CommonSet.TypeFis='prim')or(CommonSet.TypeFis='shtrih') then
            begin
              PrintNFStr(' '); PrintNFStr(' '); PrintNFStr(' '); // PrintNFStr(' '); PrintNFStr(' '); PrintNFStr(' ');
              CutDoc;
              delay(33);
            end;
            if CloseNFDoc=False then
            begin
              TestFp('PrintNFStr');
              CloseNFDoc;
            end;
       //   end;
      end;
  except
    prWriteLog('ErrPrint');
  end;
end;


Function SetDP(Str1,Str2:String):Boolean;
begin
  Result:=False;
  if (Commonset.CashNum<=0) then begin Result:=True; exit; end;
{  iRfr:=Drv.EEJShowClientDisplayText(Str1,Str2);
  if iRfr=0 then Result:=True
  else Result:=False;}
end;


Function GetCashReg(Var rSum:Real):Boolean;//���������� � �����
//Var iSt:Integer;
begin
  result:=True;  //���� ��������
  rSum:=0;
//  if Commonset.CashNum<=0 then begin Result:=True; exit; end;
end;

Procedure InCass(rSum:Real);
begin
//  if Commonset.CashNum<=0 then begin exit; end;
end;


Function GetSums(Var rSum1,rSum2,rSum3:Real):Boolean;
Var StrP:PChar;
    iRet:Word;
    StrWk:String;
begin
  result:=False;
  if Commonset.CashNum<=0 then begin Result:=True; exit; end;
  //��������
  StrP:=StrAlloc(255);
  iRet:=GetEReport($37,0);
  if iRet=0 then
  begin
    //����� � �����
    GetFldStr(19,StrP);
    StrWk:=String(StrP);
    while pos('.',StrWk)>0 do StrWk[pos('.',StrWk)]:=',';
    rSum1:=StrToFloatDef(StrWk,0);
    //�������
    GetFldStr(20,StrP); StrWk:=String(StrP);
    while pos('.',StrWk)>0 do StrWk[pos('.',StrWk)]:=',';
    rSum2:=StrToFloatDef(StrWk,0);
    //��������
    GetFldStr(21,StrP); StrWk:=String(StrP);
    while pos('.',StrWk)>0 do StrWk[pos('.',StrWk)]:=',';
    rSum3:=StrToFloatDef(StrWk,0);

    Result:=True;
  end;

  StrDispose(StrP);
end;

Function CheckOpen:Boolean;
Var StrP:PChar;
    iRet:Word;
    iB,iR:Integer;
begin
  result:=False;
  if Commonset.CashNum<=0 then begin Result:=False; exit; end;
  StrP:=StrAlloc(255);

  iRet:=StartSeans;
  if iRet<>0 then Result:=True
  else
  begin
    GetFldStr(1,StrP); StatusFr.St1:=copy(String(StrP),1,2);
    GetFldStr(2,StrP); StatusFr.St2:=copy(String(StrP),3,2)+copy(String(StrP),1,2);
    GetFldStr(3,StrP); StatusFr.St3:=copy(String(StrP),3,2)+copy(String(StrP),1,2);
    GetFldStr(4,StrP); StatusFr.St4:=copy(String(StrP),1,10);

    //�������� ������
    //2
    if StatusFr.St2='' then StatusFr.St2:='0';
    iB:=StrToIntDef('$'+StatusFr.St2,0);
    iR:=iB and $0007;
    if iR<>0 then Result:=True;

  end;
  StrDispose(StrP);
end;

function More24H(Var iSt:INteger):Boolean;
Var StrP:PChar;
    iRet:Word;
    iB,iR:Integer;
begin
  result:=False;
  if Commonset.CashNum<=0 then begin Result:=False; exit; end;
  if (Commonset.CashNum>0)and(CommonSet.SpecChar=1) then begin Result:=False; exit; end; //���� ������������ �����
  
  StrP:=StrAlloc(255);

  iRet:=StartSeans;
  if iRet<>0 then Result:=True
  else
  begin
    GetFldStr(1,StrP); StatusFr.St1:=copy(String(StrP),1,2);
    GetFldStr(2,StrP); StatusFr.St2:=copy(String(StrP),3,2)+copy(String(StrP),1,2);
    GetFldStr(3,StrP); StatusFr.St3:=copy(String(StrP),3,2)+copy(String(StrP),1,2);
    GetFldStr(4,StrP); StatusFr.St4:=copy(String(StrP),1,10);

    //�������� ������
    //2
    if StatusFr.St2='' then StatusFr.St2:='0';
    iB:=StrToIntDef('$'+StatusFr.St2,0);
    iR:=iB and $0010;
    if iR<>0 then Result:=True;
  end;
  StrDispose(StrP);
end;



{
procedure TForm1.acGetStatusExecute(Sender: TObject);
Var iRet,iNum,i:Integer;
    StrWk:String;
    dTime:TDateTime;
begin
//
  Memo1.Lines.Add('�������� ������ (������).');

  dTime:=Now;
  iRet:=GetStatusPlus;
  dTime:=dTime-Now;
  Memo1.Lines.Add('������ ���� '+FormatDateTime('nn:ss:zzz',dTime));


//  strwk:=String(GetCommand(PChar(@CommandsArray)));

  Memo1.Lines.Add('����� - '+IntToStr(iRet));
  Memo1.Lines.Add(StrWk);

//  strwk:=String(GetAnswer(PChar(@CommandsArray)));
//  Memo1.Lines.Add(StrWk);

  if iRet=0 then
  begin
    StrWk:='����� ������� - ';
    for i:=1 to 6 do
    begin
      iNum:=GetStatusNum(i);
      strwk:=Strwk+chr(iNum)+':('+INtToStr(iNum)+') ';
    end;
    Memo1.Lines.Add(StrWk);
  end;
  Memo1.Lines.Add('');
end;
}

Function GetSt(Var sMess:String):Integer;
Var iB,iR:Integer;
begin
  if Commonset.CashNum<=0 then begin Result:=0; exit; end;

  result:=0;
  sMess:='';

  GetStatusPlus;

  //�������� ������
  //1 ����
  iB:=GetStatusNum(1);

  iR:=iB and $04;
  if iR=0 then
  begin
    sMess:=sMess+' ������� �� ����������. ';
    Result:=Result+1;
  end;

  iR:=iB and $08;
  if iR=0 then
  begin
    sMess:=sMess+' ��� ������ �� ��������� �������. ';
    Result:=Result+10;
  end;

  iR:=iB and $20;
  if iR=0 then
  begin
    sMess:=sMess+' ������ ����������� ����������. ';
    Result:=Result+100;
  end;
end;



Function TestStatus(StrOp:String; Var SMess:String):Boolean;
Var bWr:Boolean;
    iB,iR:Integer;
    StrWk:String;

begin
  Result:=True;
  sMess:=StrOp;

  if Commonset.CashNum<=0 then exit;

  bWr:=False;

  WriteHistoryFr(StrOp+';'+StatusFr.St1+';'+StatusFr.St2+';'+StatusFr.St3+';'+StatusFr.St4+';'+sMess);

  if (StatusFr.St1<>'88')and(StatusFr.St1<>'80') then
  begin
    StrOp:=StrOp+'_1';
    bWr:=True;
    Result:=False;

    //�������� ������
    //1
    if StatusFr.St1='' then StatusFr.St1:='0';
    iB:=StrToIntDef('$'+StatusFr.St1,0);
    iR:=iB and $35;
    if iR<>0 then
    begin
      sMess:='������ 1: '+StatusFr.St1;
      if iR=$20 then sMess:='�� ���������.';
      if iR=$10 then sMess:='�� ������ � �����.';
      if iR=$04 then sMess:='���� ��.';
      if iR=$01 then sMess:='���������� ������.';
    end;
  end;

  StatusFr.St2[1]:='0'; //������ ������ �������� � 0 -� �� ��������� ��-�� ���� ����

{  if (StatusFr.St2<>'0100')and(StatusFr.St2<>'0101')and(StatusFr.St2<>'0900')and(StatusFr.St2<>'0901')and(StatusFr.St2<>'0902')and(StatusFr.St2<>'0903')and(StatusFr.St2<>'0904')and(StatusFr.St2<>'0905')and(StatusFr.St2<>'0906')and(StatusFr.St2<>'0907') then
  begin
    StrOp:=StrOp+'_2 (St2='+StatusFr.St2+')';
    bWr:=True;
    Result:=False;

    //2
    if StatusFr.St2='' then StatusFr.St2:='0';
    iB:=StrToIntDef('$'+StatusFr.St2,0);
//    iR:=iB and $0007;
//    if iR<>0 then  sMess:='��� �� ������.';
    iR:=iB and $0010;
    if iR<>0 then  sMess:='���������� ������� �����. (St2='+StatusFr.St2+')';
    iR:=iB and $0020;
    if iR<>0 then sMess:='���������� ������� ������������.(St2='+StatusFr.St2+')';
    iR:=iB and $0040;
    if iR<>0 then sMess:='���������� ������� �����������.(St2='+StatusFr.St2+')';
    iR:=iB and $0050;
    if iR<>0 then sMess:='���������� ������� �����..(St2='+StatusFr.St2+')';
    iR:=iB and $0400;
    if iR<>0 then sMess:='����� ��������� ������ � �����.(St2='+StatusFr.St2+')';
 //   iR:=iB and $0800;
  //  if iR=0 then sMess:=' ����� �������.';
    //
  end;}
  
  if StatusFr.St3<>'0000' then
  begin
    StrOp:=StrOp+'_3';
    bWr:=True;
    Result:=False;
    //3
    sMess:='������ 3: '+StatusFr.St3;

    if StatusFr.St3='0015' then sMess:='���������� ������� �����...';
  end;

  StatusFr.St4:=Copy(StatusFr.St4,1,8); //��������� 5-�� ���� �� ���� - �� ������������
  if (StatusFr.St4<>'16121212')and(StatusFr.St4<>'12121212') then   //��������
  begin
    StrOp:=StrOp+'_4';
    bWr:=True;
    Result:=False;

    //4
    StrWk:=Copy(StatusFr.St4,1,2); //���� 1
    if StrWk='' then StrWk:='0';
    iB:=StrToIntDef('$'+StrWk,0);
    iR:=iB and $08;
    if iR<>0 then  sMess:='��� ����� � ��.';

    StrWk:=Copy(StatusFr.St4,3,2); //���� 2
    if StrWk='' then StrWk:='0';
    iB:=StrToIntDef('$'+StrWk,0);
    iR:=iB and $04;
    if iR<>0 then sMess:='������� ������ �������� (��).';
    iR:=iB and $20;
    if iR<>0 then sMess:='����������� ������ ...';
    iR:=iB and $40;
    if iR<>0 then sMess:='������ �������� (��).';

    StrWk:=Copy(StatusFr.St4,5,2); //���� 3
    if StrWk='' then StrWk:='0';
    iB:=StrToIntDef('$'+StrWk,0);
    iR:=iB and $04;
    if iR<>0 then sMess:='����������� �������� (��).';
    iR:=iB and $08;
    if iR<>0 then sMess:='����������� ���� (��).';
    iR:=iB and $20;
    if iR<>0 then sMess:='��������������� ������.';

    StrWk:=Copy(StatusFr.St4,7,2); //���� 4
    if StrWk='' then StrWk:='0';
    iB:=StrToIntDef('$'+StrWk,0);
    iR:=iB and $40;
    if iR<>0 then sMess:='����������� ������ ..';
    iR:=iB and $08;
    if iR<>0 then sMess:='����������� ������ .';

  end;

  if bWr then WriteHistoryFr(StrOp+';'+StatusFr.St1+';'+StatusFr.St2+';'+StatusFr.St3+';'+StatusFr.St4+';'+sMess);

end;

procedure WriteHistoryFR(Strwk_: string);
var F: TextFile;
    Strwk1:String;
    FileN:String;
begin
  try
    if not DirectoryExists(CommonSet.PathHistory) then
    begin
//      ShowMessage('������: ����������� ����������� - "'+CommonSet.PathHistory+'"');
      exit;
    end;

    strwk1:='fr'+FormatDateTime('yyyy_mm',Date);
    Strwk1:=StrWk1+'.txt';
    Application.ProcessMessages;

    FileN:=CommonSet.PathHistory+strwk1;

    AssignFile(F, FileN);
    if Not FileExists(FileN) then Rewrite(F)
    else                           Append(F);
    Write(F, FormatDateTime('DD/MM/YYYY  HH:NN:SS ', Now));
    WriteLn(F, Strwk_);
    Flush(F);
  finally
    CloseFile(F);
  end;
end;


Function CutDoc:Boolean;
begin
  if Commonset.CashNum<=0 then begin Result:=True; exit; end;
  Result:=False;
  if FreeDocCutPlus(5)=0 then Result:=True;
//  if FreeDocCut=0 then Result:=True;
end;

Function CutDoc1:Boolean;
begin
  if Commonset.CashNum<=0 then begin Result:=True; exit; end;
  Result:=False;
  if FreeDocCutPlus(5)=0 then Result:=True;
end;

Function SelectF(iNum:Integer):Boolean;
begin
  Result:=False;
  if Commonset.CashNum<=0 then begin Result:=True; exit; end;
  Case iNum of
  0: begin if FontSelectFDoc($01)=0 then Result:=True; end;  //�������
  1: begin if FontSelectFDoc($21)=0 then Result:=True; end;  //������� ������
  2: begin if FontSelectFDoc($11)=0 then Result:=True; end;  //������� ������
  3: begin if FontSelectFDoc($81)=0 then Result:=True; end;  //�������������
  4: begin if FontSelectFDoc($31)=0 then Result:=True; end;  //������� ��� � ���

  5: begin if FontSelectFDoc($09)=0 then Result:=True; end;  //�������
  6: begin if FontSelectFDoc($29)=0 then Result:=True; end;  //������� ������
  7: begin if FontSelectFDoc($19)=0 then Result:=True; end;  //������� ������
  8: begin if FontSelectFDoc($89)=0 then Result:=True; end;  //�������������
  9: begin if FontSelectFDoc($39)=0 then Result:=True; end;  //������� ��� � ���

  10: begin if FontSelectFDoc($00)=0 then Result:=True; end;  //�������
  11: begin if FontSelectFDoc($20)=0 then Result:=True; end;  //������� ������
  12: begin if FontSelectFDoc($10)=0 then Result:=True; end;  //������� ������
  13: begin if FontSelectFDoc($80)=0 then Result:=True; end;  //�������������
  14: begin if FontSelectFDoc($30)=0 then Result:=True; end;  //������� ��� � ���

  15: begin if FontSelectFDoc($08)=0 then Result:=True; end;  //�������
  16: begin if FontSelectFDoc($28)=0 then Result:=True; end;  //������� ������
  17: begin if FontSelectFDoc($18)=0 then Result:=True; end;  //������� ������
  18: begin if FontSelectFDoc($88)=0 then Result:=True; end;  //�������������
  19: begin if FontSelectFDoc($38)=0 then Result:=True; end;  //������� ��� � ���

  end;
end;


Function PrintNFStr(StrPr:String):Boolean;
Var iLen:DWord;
    StrP:PChar;
begin
  if Commonset.CashNum<=0 then begin Result:=True; exit; end;
  iLen:=Length(StrPr)+1;
  Result:=False;
  if PrintOEMCRLFDoc(PChar(StrPr),iLen)=0 then Result:=True;

  StrP:=StrAlloc(255);

  GetFldStr(1,StrP); StatusFr.St1:=copy(String(StrP),1,2);
  GetFldStr(2,StrP); StatusFr.St2:=copy(String(StrP),3,2)+copy(String(StrP),1,2);
  GetFldStr(3,StrP); StatusFr.St3:=copy(String(StrP),3,2)+copy(String(StrP),1,2);
  GetFldStr(4,StrP); StatusFr.St4:=copy(String(StrP),1,10);

  StrDispose(StrP);

  if not TestStatus('PrintNFStr',sMessage) then Result:=False;

end;


Function OpenNFDoc:Boolean;
begin
  if Commonset.CashNum<=0 then begin Result:=True; exit; end;
  Result:=False;
  if OpenFDoc=0 then Result:=True;
end;

Function CloseNFDoc:Boolean;
begin
  if Commonset.CashNum<=0 then begin Result:=True; exit; end;
  Result:=False;
  if CloseFDoc=0 then Result:=True;
end;


Function CashDriver:Boolean;
begin
  if Commonset.CashNum<=0 then begin Result:=True; exit; end;
  Result:=True;
//  CashDriverEnable(1);
  CashDriverOpen;
end;


Function CheckClose:Boolean;
Var StrP:PChar;
begin
  if Commonset.CashNum<=0 then begin Result:=True; exit; end;
  Result:=False;
  if CloseReceipt=0 then Result:=True;

  StrP:=StrAlloc(255);

  GetFldStr(1,StrP); StatusFr.St1:=copy(String(StrP),1,2);
  GetFldStr(2,StrP); StatusFr.St2:=copy(String(StrP),3,2)+copy(String(StrP),1,2);
  GetFldStr(3,StrP); StatusFr.St3:=copy(String(StrP),3,2)+copy(String(StrP),1,2);
  GetFldStr(4,StrP); StatusFr.St4:=copy(String(StrP),1,10);

  StrDispose(StrP);

//  if not TestStatus('CheckClose',sMessage) then Result:=False;
end;

Function CheckRasch(iPayType,iClient:Integer; Comment:String; Var iDopl:Integer):Boolean;
Var StrP:PChar;
    iRet:Word;
    StrBN:PChar;
    sDopl:String;
//    StrWk:String;
begin
  iDopl:=1;
  if Commonset.CashNum<=0 then begin Result:=True; exit; end;

  StrP:=StrAlloc(255);
  StrBN:=StrAlloc(255);

  try
    StrBN:=StrPCopy(StrBN,Comment);

    if iPayType=0 then iRet:=TenderReceipt(0,iClient,'') //���
    else //iRet:=TenderReceipt(2,iClient,'���������� �����'); //������
      iRet:=TenderReceiptPlus(2,iClient,'���������� �����',StrBN); //iType : 0-���,1-������,2-��.�����


    GetFldStr(1,StrP); StatusFr.St1:=copy(String(StrP),1,2);
    GetFldStr(2,StrP); StatusFr.St2:=copy(String(StrP),3,2)+copy(String(StrP),1,2);
    GetFldStr(3,StrP); StatusFr.St3:=copy(String(StrP),3,2)+copy(String(StrP),1,2);
    GetFldStr(4,StrP); StatusFr.St4:=copy(String(StrP),1,10);

    //5-� ���� ����� ������� - ��� ��������� ���� ������ ���� >=0
    GetFldStr(5,StrP); sDopl:=copy(String(StrP),1,14);
    while pos('.',sDopl)>0 do delete(sDopl,pos('.',sDopl),1);
    while pos(',',sDopl)>0 do delete(sDopl,pos(',',sDopl),1);
    while pos(' ',sDopl)>0 do delete(sDopl,pos(' ',sDopl),1);
    iDopl:=StrToIntDef(sDopl,0);

  finally
    StrDispose(StrP);
    StrDispose(StrBN);
  end;

//  if not TestStatus('CheckRasch',sMessage) then Result:=False;

  if iRet<>0 then
  begin
    Result:=False;
    Exit;
  end
  else
  begin
    Result:=True;
  end;
end;

{
Function CheckRasch(iType,iClient:Integer;ValName,Comment:String;Var iDopl:Integer):Boolean;
Var StrP:PChar;
    iRet:Word;
    StrVal:PChar;
    StrBN:PChar;
    sDopl:String;
//    StrWk:String;
begin
  iDopl:=0;
  if Commonset.CashNum=0 then begin Result:=True; exit; end;

  StrP:=StrAlloc(255);
  StrVal:=StrAlloc(Length(ValName)+1);
  StrBN:=StrAlloc(255);


  try
    StrVal:=StrPCopy(StrVal,ValName);
    StrBN:=StrPCopy(StrBN,Comment);


    iRet:=TenderReceiptPlus(iType,iClient,StrVal,StrBN); //iType : 0-���,1-������,2-��.�����

    GetFldStr(1,StrP); StatusFr.St1:=copy(String(StrP),1,2);
    GetFldStr(2,StrP); StatusFr.St2:=copy(String(StrP),3,2)+copy(String(StrP),1,2);
    GetFldStr(3,StrP); StatusFr.St3:=copy(String(StrP),3,2)+copy(String(StrP),1,2);
    GetFldStr(4,StrP); StatusFr.St4:=copy(String(StrP),1,10);
    //5-� ���� ����� ������� - ��� ��������� ���� ������ ���� >=0
    GetFldStr(5,StrP); sDopl:=copy(String(StrP),1,14);
    while pos('.',sDopl)>0 do delete(sDopl,pos('.',sDopl),1);
    while pos(',',sDopl)>0 do delete(sDopl,pos(',',sDopl),1);
    while pos(' ',sDopl)>0 do delete(sDopl,pos(' ',sDopl),1);
    iDopl:=StrToIntDef(sDopl,0);
  finally
    StrDispose(StrP);
    StrDispose(StrVal);
    StrDispose(StrBN);
  end;

  if iRet<>0 then
  begin
    Result:=False;
    Exit;
  end
  else
  begin
    Result:=True;
  end;
end;
}



Function CheckDiscount(rDiscount:Real;Var iDisc,iItog:Integer):Boolean;
Var iDiscount:Integer;
    StrP:PChar;
    iRet:Word;
    StrWk,StrWk1:String;
begin
  StrP:=StrAlloc(255);
  iDiscount:=RoundEx(rDiscount*100);
  iDisc:=0;
  iItog:=0;
  if Commonset.CashNum<=0 then begin Result:=True; exit; end;
  if iDiscount>0 then
  begin //������
    iRet:=ComissionReceipt(1,0,iDiscount);

    GetFldStr(1,StrP); StatusFr.St1:=copy(String(StrP),1,2);
    GetFldStr(2,StrP); StatusFr.St2:=copy(String(StrP),3,2)+copy(String(StrP),1,2);
    GetFldStr(3,StrP); StatusFr.St3:=copy(String(StrP),3,2)+copy(String(StrP),1,2);
    GetFldStr(4,StrP); StatusFr.St4:=copy(String(StrP),1,10);

//    if not TestStatus('CheckDiscount',sMessage) then iRet:=100;  //������� �� ����� <>0

    if iRet<>0 then
    begin
      StrDispose(StrP);
      Result:=False;
      iItog:=0;
      iDisc:=0;
      Exit;
    end
    else
    begin
      Result:=True;
      GetFldStr(6,StrP);
      StrWk:=String(StrP);
      StrWk:=DelSp(StrWk);
      while pos('.',strwk)>0 do delete(StrWk,pos('.',strwk),1);
      while strwk[1]='0' do delete(StrWk,1,1);
      iDisc:=StrToIntDef(StrWk,0);

      GetFldStr(7,StrP);
      StrWk1:=String(StrP);
      StrWk1:=DelSp(StrWk1);
      while pos('.',strwk1)>0 do delete(StrWk1,pos('.',strwk1),1);
      while strwk1[1]='0' do delete(StrWk1,1,1);
      iItog:=StrToIntDef(StrWk1,0);
    end;
  end
  else //�������
  begin
    iRet:=ComissionReceipt(0,0,iDiscount);
    if iRet<>0 then
    begin
      StrDispose(StrP);
      Result:=False;
      iItog:=0;
      iDisc:=0;
      Exit;
    end
    else
    begin
      Result:=True;
      GetFldStr(6,StrP);
      StrWk:=String(StrP);
      StrWk:=DelSp(StrWk);
      while pos('.',strwk)>0 do delete(StrWk,pos('.',strwk),1);
      while strwk[1]='0' do delete(StrWk,1,1);
      iDisc:=StrToIntDef(StrWk,0);

      GetFldStr(7,StrP);
      StrWk1:=String(StrP);
      StrWk1:=DelSp(StrWk1);
      while pos('.',strwk1)>0 do delete(StrWk1,pos('.',strwk1),1);
      while strwk1[1]='0' do delete(StrWk1,1,1);
      iItog:=StrToIntDef(StrWk1,0);
    end;
//    if ComissionReceipt(0,0,iDiscount)=0 then Result:=True;
  end;
  StrDispose(StrP);
end;

Function CheckTotal(Var iTotal:Integer):Boolean;
Var StrP:PChar;
    iRet:Word;
    StrWk:String;
begin
  iTotal:=0;
  if Commonset.CashNum<=0 then begin Result:=True; exit; end;
  StrP:=StrAlloc(255);
  iRet:=TotalReceipt;

  GetFldStr(1,StrP); StatusFr.St1:=copy(String(StrP),1,2);
  GetFldStr(2,StrP); StatusFr.St2:=copy(String(StrP),3,2)+copy(String(StrP),1,2);
  GetFldStr(3,StrP); StatusFr.St3:=copy(String(StrP),3,2)+copy(String(StrP),1,2);
  GetFldStr(4,StrP); StatusFr.St4:=copy(String(StrP),1,10);

//  if not TestStatus('CheckTotal',sMessage) then iRet:=100;  //������� �� ����� <>0

  if iRet<>0 then
  begin
    StrDispose(StrP);
    Result:=False;
    iTotal:=0;
    Exit;
  end
  else
  begin
    GetFldStr(5,StrP);
    StrWk:=String(StrP);
    StrWk:=DelSp(StrWk);
    while pos('.',strwk)>0 do delete(StrWk,pos('.',strwk),1);
    while strwk[1]='0' do delete(StrWk,1,1);
    iTotal:=StrToIntDef(StrWk,0);
    Result:=True;
  end;
  StrDispose(StrP);
end;


Procedure CheckCancel;
begin
  if Commonset.CashNum<=0 then begin exit; end;
  CancelReceipt;
end;

Procedure TestPosCh;
begin
  if Length(PosCh.Name)>39 then PosCh.Name:=Copy(PosCh.Name,1,39);
  if Length(PosCh.Code)>19 then PosCh.Code:=Copy(PosCh.Code,1,19);
  if Length(PosCh.AddName)>250 then PosCh.AddName:=Copy(PosCh.AddName,1,250);
end;


Function CheckAddPos(Var iSum:Integer):Boolean;
Var StrFont:String;
    StrP:PChar;
    StrWk:String;
begin
  iSum:=0;
  if Commonset.CashNum<=0 then begin Result:=True; exit; end;
  Result:=False;
  TestPosCh;
  StrFont:='~09';

  StrP:=StrAlloc(255);
//  if ItemReceiptPlus(PChar(StrFont+CurPos.Articul+' '+CurPos.Name),'','   ','','',RoundEx(CurPos.Price*100),RoundEx(CurPos.Quant*1000),1)=0 then Result:=True;
//  if (PosCh.Price>=1)and(PosCh.Count>=1) then
//  begin
    if ItemReceiptPlus(PChar(PosCh.Name),PChar(PosCh.Code),'��.','',PChar(PosCh.AddName),PosCh.Price,PosCh.Count,1)=0 then Result:=True;

    GetFldStr(1,StrP); StatusFr.St1:=copy(String(StrP),1,2);
    GetFldStr(2,StrP); StatusFr.St2:=copy(String(StrP),3,2)+copy(String(StrP),1,2);
    GetFldStr(3,StrP); StatusFr.St3:=copy(String(StrP),3,2)+copy(String(StrP),1,2);
    GetFldStr(4,StrP); StatusFr.St4:=copy(String(StrP),1,10);

//  if not TestStatus('CheckAddPos',sMessage) then Result:=False;

    GetFldStr(5,StrP);
    StrWk:=String(StrP);
    StrWk:=DelSp(StrWk);
    while pos('.',strwk)>0 do delete(StrWk,pos('.',strwk),1);
    while strwk[1]='0' do delete(StrWk,1,1);
    iSum:=StrToIntDef(StrWk,0);
//  end
//  else iSum:=0;

  StrDispose(StrP);
end;


Function CheckStart:Boolean;
Var StrP:PChar;
begin
  if Commonset.CashNum<=0 then begin Result:=True; exit; end;
  Result:=False;
//  Result:=True;
  if StartReceipt(0,1,'','','')=0 then Result:=True;

  StrP:=StrAlloc(255);
  GetFldStr(1,StrP); StatusFr.St1:=copy(String(StrP),1,2);
  GetFldStr(2,StrP); StatusFr.St2:=copy(String(StrP),3,2)+copy(String(StrP),1,2);
  GetFldStr(3,StrP); StatusFr.St3:=copy(String(StrP),3,2)+copy(String(StrP),1,2);
  GetFldStr(4,StrP); StatusFr.St4:=copy(String(StrP),1,10);
  StrDispose(StrP);

//  if not TestStatus('CheckStart',sMessage) then Result:=False;

end;

Function CheckRetStart:Boolean;
Var StrP:PChar;
begin
  if Commonset.CashNum<=0 then begin Result:=True; exit; end;
  Result:=False;
//  Result:=True;
  if StartReceipt(2,1,'','','')=0 then Result:=True;

  StrP:=StrAlloc(255);
  GetFldStr(1,StrP); StatusFr.St1:=copy(String(StrP),1,2);
  GetFldStr(2,StrP); StatusFr.St2:=copy(String(StrP),3,2)+copy(String(StrP),1,2);
  GetFldStr(3,StrP); StatusFr.St3:=copy(String(StrP),3,2)+copy(String(StrP),1,2);
  GetFldStr(4,StrP); StatusFr.St4:=copy(String(StrP),1,10);
  StrDispose(StrP);

//  if not TestStatus('CheckRetStart',sMessage) then Result:=False;

end;


Function ZClose:Boolean;
begin
  if Commonset.CashNum<=0 then begin Result:=True; exit; end;
  Result:=False;
  if ShiftClose=0 then Result:=True;
end;


Function ZOpen:Boolean;
begin
  if Commonset.CashNum<=0 then begin Result:=True; exit; end;
  Result:=False;
  if ShiftOpen('')=0 then Result:=True;
end;

Function GetX:Boolean;
begin
  if Commonset.CashNum<=0 then begin Result:=True; exit; end;
  Result:=False;
  if XReport=0 then Result:=True;
end;

Function CashDate(var sDate:String):Boolean;
Var iRet:Integer;
    StrP:PChar;
begin
//
  if Commonset.CashNum<=0 then begin Result:=True; exit; end;
  Result:=False;
  sDate:='';
  iRet:=GetDate;
  StrP:=StrAlloc(255);
  if iRet<>0 then
  begin
    StrDispose(StrP);
    Exit;
  end
  else
  begin
    GetFldStr(5,StrP);
    sDate:=Copy(String(StrP),1,2)+'.'+Copy(String(StrP),3,2)+'.'+Copy(String(StrP),5,2);

//    GetFldStr(6,StrP);
//    StrWk:=StrWk+'   '+Copy(String(StrP),1,2)+':'+Copy(String(StrP),3,2);

    StrDispose(StrP);
    Result:=true;
  end;
end;


Function CashClose:Boolean;
Var iRet:Integer;
begin
  if Commonset.CashNum<=0 then begin Result:=True; exit; end;
  Result:=False;
  iRet:=CloseDll;
  if iRet=0 then result:=True;
end;

Function CashOpen(sPort:PChar):Boolean;
Var iRet:Integer;
begin
  if Commonset.CashNum<=0 then begin Result:=True; exit; end;

  CommonSet.TypeFis:='prim';

  Result:=False;
  iRet:=OpenDll('������','AERF',sPort,0);
  if iRet=0 then result:=True;
end;

Function GetNums:Boolean;
Var StrP:PChar;
    iRet:Word;
begin
  if (Commonset.CashNum<=0)or(Commonset.SpecChar=1) then begin inc(Nums.iCheckNum); inc(CommonSet.CashChNum);  Result:=True; exit; end;
  result:=False;
  StrP:=StrAlloc(255);
  iRet:=GetFiscalNums;
  if iRet<>0 then
  begin
    StrDispose(StrP);
    Exit;
  end
  else
  begin
    GetFldStr(8,StrP); Nums.RegNum:=copy(String(StrP),1,10);
  end;

  iRet:=GetNumbers;
  if iRet<>0 then
  begin
    StrDispose(StrP);
    Exit;
  end
  else
  begin
    try
      GetFldStr(6,StrP);
      Nums.CheckNum:=copy(String(StrP),3,2)+copy(String(StrP),1,2);
      if Nums.CheckNum='' then Nums.CheckNum:='0';
      Nums.iCheckNum:=StrToIntDef('$'+Nums.CheckNum,0);
      Nums.CheckNum:=IntToStr(Nums.iCheckNum);
    except
    end;
  end;
  StrDispose(StrP);
  CommonSet.CashChNum:=Nums.iCheckNum;
  Result:=True;
end;

Function GetRes:Boolean;
Var StrP:PChar;
    iRet:Word;
    StrWk:String;
begin
  if Commonset.CashNum<=0 then begin Result:=True; exit; end;
  Result:=False;
  StrP:=StrAlloc(255);
  iRet:=GetResource;
  if iRet<>0 then
  begin
    StrDispose(StrP);
    Exit;
  end
  else
  begin
//    Memo1.Lines.Add('�������.');
//    Memo1.Lines.Add('5'); ����� ���������� ���������������
//    GetFldStr(5,StrP); StrWk:=String(StrP); Memo1.Lines.Add(StrWk);
    //�������� ����
    GetFldStr(6,StrP); StrWk:=String(StrP);
    if StrWk='' then StrWk:='0';
    Nums.ZYet :=StrToIntDef('$'+ copy(StrWk,3,2)+copy(StrWk,1,2),0);
    //����� Z
    GetFldStr(7,StrP); StrWk:=String(StrP);
    if StrWk='' then StrWk:='0';
    Nums.ZNum :=StrToIntDef('$'+ copy(StrWk,3,2)+copy(StrWk,1,2),0)+1; //��������� �������� +1
    //����
    GetFldStr(8,StrP); StrWk:=String(StrP);
    Nums.sDateTimeBeg:=Copy(StrWk,1,2)+'.'+Copy(StrWk,3,2)+'.20'+Copy(StrWk,5,2);
    //�����
    GetFldStr(9,StrP); StrWk:=String(StrP);
    Nums.sDateTimeBeg:=Nums.sDateTimeBeg+' '+Copy(StrWk,1,2)+':'+Copy(StrWk,3,2);
  end;

  StrDispose(StrP);
  Result:=True;
end;

Function GetSerial:Boolean;
Var StrP:PChar;
    iRet:Word;
begin
  if Commonset.CashNum<=0 then begin Result:=True; exit; end;
  result:=False;
  StrP:=StrAlloc(255);
  iRet:=GetSerialNum;
  if iRet<>0 then
  begin
    StrDispose(StrP);
    Exit;
  end
  else
  begin
    GetFldStr(5,StrP);
    Nums.SerNum:=copy(String(StrP),1,11);
    while pos(' ',Nums.SerNum)>0 do delete(Nums.SerNum,pos(' ',Nums.SerNum),1);
    StrDispose(StrP);
    Result:=True;
  end;
end;


Function OpenZ:Boolean;
Var StrP:PChar;
    iRet:Word;
    iB,iR:Integer;
begin
  if Commonset.CashNum<=0 then begin Result:=True; exit; end;
  result:=True;
  StrP:=StrAlloc(255);
  iRet:=StartSeans;
  if iRet<>0 then
  begin
    StrDispose(StrP);
    Exit;
  end
  else
  begin
    GetFldStr(1,StrP); StatusFr.St1:=copy(String(StrP),1,2);
    GetFldStr(2,StrP); StatusFr.St2:=copy(String(StrP),3,2)+copy(String(StrP),1,2);
    GetFldStr(3,StrP); StatusFr.St3:=copy(String(StrP),3,2)+copy(String(StrP),1,2);
    GetFldStr(4,StrP); StatusFr.St4:=copy(String(StrP),1,10);
    StrDispose(StrP);

    //�������� ������
    //2
    if StatusFr.St2='' then StatusFr.St2:='FFFF';
    iB:=StrToIntDef('$'+StatusFr.St2,$0800);
    iR:=iB and $0800;
//    if iR=0 then  Result:=False;  //������ �������

    WriteHistoryFr('XRep;'+StatusFr.St1+';'+StatusFr.St2+';'+StatusFr.St3+';'+StatusFr.St4+';');
  end;
end;



Function InspectSt(Var sMess:String):Integer;
Var StrP:PChar;
    iRet:Word;
    iB,iR:Integer;
    StrWk:String;
begin
  if Commonset.CashNum<=0 then begin Result:=0; exit; end;

  result:=0;
  sMess:='��� ��.';
  StrP:=StrAlloc(255);

  iRet:=StartSeans;
//  iRet:=GetStatus;
  if iRet<>0 then
  begin
    StrP:=GetErrorMessage(StrP);
    sMess:='������ ��������: '+String(StrP)+' ���������� ������� �����.';
    Result:=1;
    StrDispose(StrP);
    Exit;
  end
  else
  begin
    GetFldStr(1,StrP); StatusFr.St1:=copy(String(StrP),1,2);
    GetFldStr(2,StrP); StatusFr.St2:=copy(String(StrP),3,2)+copy(String(StrP),1,2);
    GetFldStr(3,StrP); StatusFr.St3:=copy(String(StrP),3,2)+copy(String(StrP),1,2);
    GetFldStr(4,StrP); StatusFr.St4:=copy(String(StrP),1,10);

    StrDispose(StrP);


    //�������� ������
    //1
    if StatusFr.St1='' then StatusFr.St1:='0';
    iB:=StrToIntDef('$'+StatusFr.St1,0);
    iR:=iB and $35;
    if iR<>0 then
    begin
      sMess:='���������� ������: '+StatusFr.St1;
      Result:=10;
      if iR=$20 then begin sMess:='���������� ������: '+StatusFr.St1+' ���������� ������ ���������.'; end;
      if iR=$10 then begin sMess:='���������� ������: '+StatusFr.St1+' ���������� ������ ������ � �����.'; end;
      if iR=$04 then begin sMess:='���������� ������: '+StatusFr.St1+' ���� ���������� ������.'; end;
      if iR=$01 then begin sMess:='���������� ������: '+StatusFr.St1+' ���������� ������.'; end;
      exit;
    end;

    //2
    if StatusFr.St2='' then StatusFr.St2:='0';
    iB:=StrToIntDef('$'+StatusFr.St2,0);
{    iR:=iB and $0007;
    if iR<>0 then
    begin
      sMess:='������� ������: '+StatusFr.St2+' ��� �� ������.';
      Result:=21;
      exit;
    end;}
    iR:=iB and $0010;
    if iR<>0 then
    begin
      sMess:='������� ������: '+StatusFr.St2+' ���������� ������� �����.';
      Result:=22;
      exit;
    end;
    iR:=iB and $0020;
    if iR<>0 then
    begin
      sMess:='������� ������: '+StatusFr.St2+' ���������� ������� ������������.';
      Result:=23;
      exit;
    end;
    iR:=iB and $0040;
    if iR<>0 then
    begin
      sMess:='������� ������: '+StatusFr.St2+' ���������� ������� �����������.';
      Result:=24;
      exit;
    end;
    iR:=iB and $0400;
    if iR<>0 then
    begin
      sMess:='������� ������: '+StatusFr.St2+' ����� ��������� ������ � �����.';
      Result:=25;
      exit;
    end;
{    iR:=iB and $0800;
    if iR=0 then
    begin
      sMess:='������� ������: '+StatusFr.St2+' ����� �������.';
      Result:=26;
      exit;
    end;}

    //3
    if StatusFr.St3<>'0000' then
    begin
      sMess:='������ : '+StatusFr.St3;
      Result:=3;
      exit;
    end;

    //4
    StrWk:=Copy(StatusFr.St4,1,2); //����
    if StrWk='' then StrWk:='0';
    iB:=StrToIntDef('$'+StrWk,0);
    iR:=iB and $08;
    if iR<>0 then
    begin
      sMess:='���������� ����������: '+StatusFr.St2+' ����� �� �����������.';
      Result:=41;
//       exit;
    end;

    StrWk:=Copy(StatusFr.St4,3,2); //����
    if StrWk='' then StrWk:='0';
    iB:=StrToIntDef('$'+StrWk,0);
    iR:=iB and $04;
    if iR<>0 then
    begin
      sMess:='���������� ����������: '+StatusFr.St2+' ������ ��� �������.';
      Result:=42;
      exit;
    end;
    iR:=iB and $20;
    if iR<>0 then
    begin
      sMess:='���������� ����������: '+StatusFr.St2+' ����� ������.';
      Result:=43;
      exit;
    end;
    iR:=iB and $40;
    if iR<>0 then
    begin
      sMess:='���������� ����������: '+StatusFr.St2+' ������.';
      Result:=44;
      exit;
    end;

    StrWk:=Copy(StatusFr.St4,5,2); //����
    if StrWk='' then StrWk:='0';
    iB:=StrToIntDef('$'+StrWk,0);
    iR:=iB and $04;
    if iR<>0 then
    begin
      sMess:='���������� ����������: '+StatusFr.St2+' ������������ �����������.';
      Result:=45;
      exit;
    end;
    iR:=iB and $08;
    if iR<>0 then
    begin
      sMess:='���������� ����������: '+StatusFr.St2+' ����������� ����.';
      Result:=46;
      exit;
    end;
    iR:=iB and $20;
    if iR<>0 then
    begin
      sMess:='���������� ����������: '+StatusFr.St2+' ��������������� ������.';
      Result:=47;
      exit;
    end;

    StrWk:=Copy(StatusFr.St4,7,2); //����
    if StrWk='' then StrWk:='0';
    iB:=StrToIntDef('$'+StrWk,0);
    iR:=iB and $40;
    if iR<>0 then
    begin
      sMess:='���������� ����������: '+StatusFr.St2+' ����������� ������� �����.';
      Result:=48;
      exit;
    end;
  end;
end;


end.
