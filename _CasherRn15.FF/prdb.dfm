object dmPC: TdmPC
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 422
  Top = 135
  Height = 225
  Width = 332
  object PCDb: TpFIBDatabase
    DBName = 'localhost:C:\_CasherRn\DB2\CASHERRN.GDB'
    DBParams.Strings = (
      'user_name=SYSDBA'
      'lc_ctype=WIN1251'
      'password=masterkey'
      'sql_role_name=SYSDBA')
    SQLDialect = 3
    Timeout = 0
    SynchronizeTime = False
    DesignDBOptions = []
    AliasName = 'PCDb'
    WaitForRestoreConnect = 0
    Left = 20
    Top = 15
  end
  object trSelPC: TpFIBTransaction
    DefaultDatabase = PCDb
    TimeoutAction = TARollback
    Left = 20
    Top = 68
  end
  object trUpdPC: TpFIBTransaction
    DefaultDatabase = PCDb
    TimeoutAction = TARollback
    Left = 20
    Top = 120
  end
  object prGetLim: TpFIBStoredProc
    Transaction = trSelPC
    Database = PCDb
    SQL.Strings = (
      'EXECUTE PROCEDURE PR_GETBNLIM (?PCBAR)')
    StoredProcName = 'PR_GETBNLIM'
    Left = 80
    Top = 16
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object prWritePC: TpFIBStoredProc
    Transaction = trUpdPC
    Database = PCDb
    SQL.Strings = (
      
        'EXECUTE PROCEDURE PRSAVEPC (?PCBAR, ?PCSUM, ?STATION, ?IDPERSON,' +
        ' ?PERSNAME, ?ID_TAB)')
    StoredProcName = 'PRSAVEPC'
    Left = 140
    Top = 16
    qoAutoCommit = True
    qoStartTransaction = True
  end
end
