unit CalcSeb;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  StdCtrls, cxButtons, ExtCtrls, DBClient, cxContainer, cxTextEdit,
  cxMaskEdit, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox, cxCalendar, FIBDataSet, pFIBDataSet;

type
  TfmCalcSeb = class(TForm)
    Panel2: TPanel;
    GrSeb: TcxGrid;
    ViewSeb: TcxGridDBTableView;
    LevelSeb: TcxGridLevel;
    Label2: TLabel;
    Label3: TLabel;
    cxButton2: TcxButton;
    cxButton1: TcxButton;
    taSeb: TClientDataSet;
    dsSeb: TDataSource;
    taSebIDCARD: TIntegerField;
    taSebCURMESSURE: TIntegerField;
    taSebNETTO: TFloatField;
    taSebBRUTTO: TFloatField;
    taSebNAME: TStringField;
    taSebNAMESHORT: TStringField;
    taSebKOEF: TFloatField;
    taSebKNB: TFloatField;
    taSebPRICE1: TCurrencyField;
    taSebSUM1: TCurrencyField;
    taSebPRICE2: TCurrencyField;
    taSebSUM2: TCurrencyField;
    ViewSebIDCARD: TcxGridDBColumn;
    ViewSebCURMESSURE: TcxGridDBColumn;
    ViewSebNETTO: TcxGridDBColumn;
    ViewSebBRUTTO: TcxGridDBColumn;
    ViewSebNAME: TcxGridDBColumn;
    ViewSebNAMESHORT: TcxGridDBColumn;
    ViewSebKOEF: TcxGridDBColumn;
    ViewSebKNB: TcxGridDBColumn;
    ViewSebPRICE1: TcxGridDBColumn;
    ViewSebSUM1: TcxGridDBColumn;
    ViewSebPRICE2: TcxGridDBColumn;
    ViewSebSUM2: TcxGridDBColumn;
    Label4: TLabel;
    quMHAll: TpFIBDataSet;
    quMHAllID: TFIBIntegerField;
    quMHAllPARENT: TFIBIntegerField;
    quMHAllITYPE: TFIBIntegerField;
    quMHAllNAMEMH: TFIBStringField;
    quMHAllDEFPRICE: TFIBIntegerField;
    quMHAllNAMEPRICE: TFIBStringField;
    dsMHAll: TDataSource;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmCalcSeb: TfmCalcSeb;

implementation

uses dmOffice, TCard;

{$R *.dfm}

procedure TfmCalcSeb.FormCreate(Sender: TObject);
begin
  GrSeb.Align:=AlClient;
end;

procedure TfmCalcSeb.cxButton2Click(Sender: TObject);
begin
  close;
end;

procedure TfmCalcSeb.cxButton1Click(Sender: TObject);
Var rPrice,rMessure,k:Real;
    bBludo:Boolean;
begin
  taSeb.Active:=False;
  taSeb.CreateDataSet;
  with dmO do
  begin
    dsTSpec.DataSet:=Nil;
    quTSpec.First;
    while not quTSpec.Eof do
    begin
      //��������� � �� ����� �� ���???
      bBludo:=False;
      quFindCard.Active:=False;
      quFindCard.ParamByName('IDCARD').AsInteger:=quTSpecIDCARD.AsInteger;
      quFindCard.Active:=True;
      if quFindCardTCARD.AsInteger=1 then bBludo:=True;
      quFindCard.Active:=False;

      if bBludo=False then
      begin
        prCalcLastPrice1.ParamByName('IDGOOD').AsInteger:=quTSpecIDCARD.AsInteger;
        prCalcLastPrice1.ExecProc;
        rPrice:=prCalcLastPrice1.ParamByName('PRICEIN').AsCurrency;
        rMessure:=prCalcLastPrice1.ParamByName('KOEF').AsFloat;

        if quTSpecKOEF.AsFloat<>rMessure then
        begin
          k:=quTSpecKOEF.AsFloat/rMessure;
        end else k:=1;
      end else
      begin //��� ����� - ���� �������� ������������� �����
        k:=1;
        rPrice:=0;
//        prCalcSebBludo(quTSpecIDCARD.AsInteger)
      end;

      taSeb.Append;
      taSebIDCARD.AsInteger:=quTSpecIDCARD.AsInteger;
      taSebCURMESSURE.AsInteger:=quTSpecCURMESSURE.AsInteger;
      taSebNETTO.AsFloat:=quTSpecNETTO.AsFloat;
      taSebBRUTTO.AsFloat:=quTSpecBRUTTO.AsFloat;
      taSebNAME.AsString:=quTSpecNAME.AsString;
      taSebNAMESHORT.AsString:=quTSpecNAMESHORT.AsString;
      taSebKOEF.AsFloat:=quTSpecKOEF.AsFloat;
      taSebKNB.AsFloat:=quTSpecKNB.AsFloat;
      taSebPRICE1.AsCurrency:=rPrice;
      taSebSUM1.AsCurrency:=Round(rPrice*k*taSebBRUTTO.AsFloat*100)/100;
      taSebPRICE2.AsCurrency:=0;
      taSebSUM2.AsCurrency:=0;
      taSeb.Post;

      quTSpec.Next;
    end;
    dsTSpec.DataSet:=quTSpec;
  end;
end;

end.
