unit prdb;

interface

uses
  SysUtils, Classes, FIBDatabase, pFIBDatabase, FIBQuery, pFIBQuery,
  pFIBStoredProc;

type
  TdmPC = class(TDataModule)
    PCDb: TpFIBDatabase;
    trSelPC: TpFIBTransaction;
    trUpdPC: TpFIBTransaction;
    prGetLim: TpFIBStoredProc;
    prWritePC: TpFIBStoredProc;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

Procedure prFindPCardBalans(sBar:String; Var rBalans,rBalansDay,DLimit:Real; Var CliName,CliType:String );
Procedure prWritePCSum(sBar:String;PCSUM:Real;IDT:INteger);

var
  dmPC: TdmPC;

implementation

uses Un1;

{$R *.dfm}

Procedure prWritePCSum(sBar:String;PCSUM:Real;IDT:INteger);
begin
  with dmPC do
  begin
    prWritePC.ParamByName('PCBAR').AsString:=sBar;
    prWritePC.ParamByName('PCSUM').AsFloat:=PCSUM;
    prWritePC.ParamByName('STATION').AsINteger:=CommonSet.Station;
    prWritePC.ParamByName('IDPERSON').AsINteger:=Person.Id;
    prWritePC.ParamByName('PERSNAME').AsString:=Person.Name;
    prWritePC.ParamByName('ID_TAB').AsINteger:=IDT;

    //ECUTE PROCEDURE PRSAVEPC (?PCBAR, ?PCSUM, ?STATION, ?IDPERSON, ?PERSNAME, ID_TAB)
    prWritePC.ExecProc;
  end;
end;


Procedure prFindPCardBalans(sBar:String; Var rBalans,rBalansDay,DLimit:Real; Var CliName,CliType:String );
begin
  with dmPC do
  begin
    prGetLim.ParamByName('PCBAR').AsString:=sBar;
    prGetLim.ExecProc;
    delay(33);
    rBalans:=prGetLim.ParamByName('BALANS').Value;
    rBalansDay:=prGetLim.ParamByName('BALANSDAY').Value;
    DLimit:=prGetLim.ParamByName('DLIMIT').Value;
    CliName:=prGetLim.ParamByName('CLINAME').Value;
    CliType:=prGetLim.ParamByName('CLITYPE').Value;
  end;
end;


procedure TdmPC.DataModuleCreate(Sender: TObject);
begin
{  PCDb.Connected:=False;
  PCDb.DBName:=DBNamePC;
  try
    PCDb.Open;
  except
  end;}
end;

procedure TdmPC.DataModuleDestroy(Sender: TObject);
begin
  PCDb.Close;
end;

end.
