unit fmDiscountShape;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, dxfShapedForm, Menus, cxLookAndFeelPainters, StdCtrls,
  cxButtons, cxControls, cxContainer, cxEdit, cxTextEdit, dxfCheckBox;

type
  TfmDiscount_Shape = class(TForm)
    dxfShapedForm1: TdxfShapedForm;
    Panel2: TPanel;
    Panel1: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    cxTextEdit1: TcxTextEdit;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Label1: TLabel;
    cxButton10: TcxButton;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    cxButton5: TcxButton;
    cxButton6: TcxButton;
    cxButton7: TcxButton;
    cxButton8: TcxButton;
    cxButton9: TcxButton;
    cxButton11: TcxButton;
    cxButton12: TcxButton;
    cxButton14: TcxButton;
    cxButton13: TcxButton;
    cxCheckBox1: TdxfCheckBox;
    procedure cxButton10Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
    procedure cxButton7Click(Sender: TObject);
    procedure cxButton8Click(Sender: TObject);
    procedure cxButton9Click(Sender: TObject);
    procedure cxButton11Click(Sender: TObject);
    procedure cxButton12Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cxButton13Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmDiscount_Shape: TfmDiscount_Shape;

implementation

uses PCardList, Dm;

{$R *.dfm}

procedure TfmDiscount_Shape.cxButton10Click(Sender: TObject);
begin
  cxTextEdit1.Text:=cxTextEdit1.Text+'0';
end;

procedure TfmDiscount_Shape.cxButton3Click(Sender: TObject);
begin
  cxTextEdit1.Text:=cxTextEdit1.Text+'1';
end;

procedure TfmDiscount_Shape.cxButton4Click(Sender: TObject);
begin
  cxTextEdit1.Text:=cxTextEdit1.Text+'2';
end;

procedure TfmDiscount_Shape.cxButton5Click(Sender: TObject);
begin
  cxTextEdit1.Text:=cxTextEdit1.Text+'3';
end;

procedure TfmDiscount_Shape.cxButton6Click(Sender: TObject);
begin
  cxTextEdit1.Text:=cxTextEdit1.Text+'4';
end;

procedure TfmDiscount_Shape.cxButton7Click(Sender: TObject);
begin
  cxTextEdit1.Text:=cxTextEdit1.Text+'5';
end;

procedure TfmDiscount_Shape.cxButton8Click(Sender: TObject);
begin
  cxTextEdit1.Text:=cxTextEdit1.Text+'6';
end;

procedure TfmDiscount_Shape.cxButton9Click(Sender: TObject);
begin
  cxTextEdit1.Text:=cxTextEdit1.Text+'7';
end;

procedure TfmDiscount_Shape.cxButton11Click(Sender: TObject);
begin
  cxTextEdit1.Text:=cxTextEdit1.Text+'8';
end;

procedure TfmDiscount_Shape.cxButton12Click(Sender: TObject);
begin
  cxTextEdit1.Text:=cxTextEdit1.Text+'9';
end;

procedure TfmDiscount_Shape.FormShow(Sender: TObject);
begin
  cxTextEdit1.SetFocus;
  cxTextEdit1.SelectAll;
  cxButton1.ModalResult:=mrOk;
end;

procedure TfmDiscount_Shape.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  cxButton13.Visible:=False;
  cxCheckBox1.Visible:=False;
end;

procedure TfmDiscount_Shape.cxButton13Click(Sender: TObject);
begin
  if not CanDo('prPrintPCCheck') then exit;
  with dmC do
  begin
    fmPCardList.ViewPCL.BeginUpdate;

    quPCardsList.Active:=False;
    quPCardsList.ParamByName('DATEB').AsDate:=Date;
    quPCardsList.ParamByName('DATEE').AsDate:=Date;
    quPCardsList.Active:=True;

    fmPCardList.ViewPCL.EndUpdate;


    fmPCardList.ShowModal;
    if fmPCardList.ModalResult=mrOk then
    begin
      if quPCardsList.RecordCount>0 then  cxTextEdit1.Text:=quPCardsListBARCODE.AsString;
    end;

    quPCardsList.Active:=False;
  end;
end;

end.
