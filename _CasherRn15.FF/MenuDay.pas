unit MenuDay;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Placemnt, ComCtrls, SpeedBar, ExtCtrls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  RXSplit, cxCurrencyEdit, cxImageComboBox, cxTextEdit, XPStyleActnCtrls,
  ActnList, ActnMan, Menus, ToolWin, ActnCtrls, ActnMenus, StdCtrls,
  FR_Class, FR_DSet, FR_DBSet, DBClient, FR_BarC, cxContainer, cxTreeView,
  FR_Desgn;

type
  TfmMenuDay = class(TForm)
    FormPlacement1: TFormPlacement;
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    RxSplitter1: TRxSplitter;
    ViewMenuDay: TcxGridDBTableView;
    LevelMenuDay: TcxGridLevel;
    GridMenuDay: TcxGrid;
    am3: TActionManager;
    acEditMGr: TAction;
    acAddMGr: TAction;
    acExit: TAction;
    PopupMenu1: TPopupMenu;
    acAddMGr1: TMenuItem;
    N1: TMenuItem;
    acAddMSubGr: TAction;
    Timer1: TTimer;
    acAddMSubGr1: TMenuItem;
    N2: TMenuItem;
    N6: TMenuItem;
    acDelMGr: TAction;
    ActionMainMenuBar1: TActionMainMenuBar;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    SpeedItem6: TSpeedItem;
    SpeedItem7: TSpeedItem;
    SpeedItem8: TSpeedItem;
    MenuTree: TcxTreeView;
    ViewMenuDaySIFR: TcxGridDBColumn;
    ViewMenuDayNAME: TcxGridDBColumn;
    ViewMenuDayPRICE: TcxGridDBColumn;
    acPrint: TAction;
    SpeedItem9: TSpeedItem;
    frRepMD: TfrReport;
    frquMDRep: TfrDBDataSet;
    ViewMenuDayALTNAME: TcxGridDBColumn;
    ViewMenuDayPRI: TcxGridDBColumn;
    frDesigner1: TfrDesigner;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure MenuTreeChange(Sender: TObject; Node: TTreeNode);
    procedure acExitExecute(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure acAddMGrExecute(Sender: TObject);
    procedure acAddMSubGrExecute(Sender: TObject);
    procedure acDelMGrExecute(Sender: TObject);
    procedure acEditMGrExecute(Sender: TObject);
    procedure MenuTreeDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure MenuTreeExpanding(Sender: TObject; Node: TTreeNode;
      var AllowExpansion: Boolean);
    procedure FormShow(Sender: TObject);
    procedure ViewMenuDayDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure ViewMenuDayDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure acPrintExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

Procedure FillFm1;

var
  fmMenuDay: TfmMenuDay;
  bDr:Boolean=False;

implementation

uses Un1, dmRnEdit, AddCateg, AddM, AddMDG, MenuCr;

{$R *.dfm}

Procedure FillFm1;
begin
  with dmC do
  begin
    with fmAddM do
    begin
      cxTextEdit1.Text:='';
      cxCalcEdit1.Value:=0;
      cxCalcEdit3.Value:=1;
      cxCurrencyEdit1.Value:=0;

      cxTextEdit2.Text:='';
      cxLookupComboBox3.EditValue:=Null;
      cxLookupComboBox1.EditValue:=Null;
      cxLookupComboBox2.EditValue:=Null;
      cxLookupComboBox4.EditValue:=Null;
      cxSpinEdit1.EditValue:=0;
      cxCheckBox1.Checked:=False;

      cxTextEdit1.Text:=quMenuSelName.AsString;
      cxCalcEdit1.Value:=StrToINtDef(quMenuSelCODE.AsString,0);
      cxCalcEdit3.Value:=quMenuSelCONSUMMA.AsFloat;
      cxCurrencyEdit1.Value:=quMenuSelPRICE.AsCurrency;
      cxTextEdit2.Text:=quMenuSelBARCODE.AsString;

      cxLookupComboBox3.EditValue:=Round(quMenuSelNALOG.AsFloat);
      cxLookupComboBox1.EditValue:=quMenuSelCATEG.AsInteger;
      cxLookupComboBox2.EditValue:=quMenuSelLINK.AsInteger;
      cxLookupComboBox4.EditValue:=quMenuSelSTREAM.AsInteger;
      cxSpinEdit1.EditValue:=Round(quMenuSelLIMITPRICE.AsFloat);
      if quMenuSelDESIGNSIFR.AsInteger>0 then cxCheckBox1.Checked:=True;

    end;
  end;
end;

procedure TfmMenuDay.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  GridMenuDay.Align:=AlClient;
  ViewMenuDay.RestoreFromIniFile(CurDir+GridIni);
end;

procedure TfmMenuDay.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewMenuDay.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmMenuDay.MenuTreeChange(Sender: TObject; Node: TTreeNode);
begin
  if bRefresh then
  begin
    with dmC do
    begin
      ViewMenuDay.BeginUpdate;
      quMDS.Active:=False;
      quMDS.ParamByName('IDH').AsInteger:=MenuTree.Tag;
      quMDS.ParamByName('ID').AsInteger:=Integer(MenuTree.Selected.Data);
      quMDS.Active:=True;
      ViewMenuDay.EndUpdate;
    end;
  end;
end;

procedure TfmMenuDay.acExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmMenuDay.acAddMGrExecute(Sender: TObject);
Var Id,i:Integer;
begin
  //�������� ������
  with dmC do
  begin
    fmMDG:=tfmMDG.Create(Application);
    fmMDG.Caption:='������ �������� ���� - ����������.';
    fmMDG.cxTextEdit1.Text:='����� ������';
    fmMDG.cxSpinEdit1.Value:=1;
    fmMDG.ShowModal;
    if fmMDG.ModalResult=mrOk then
    begin
      quMaxMDG.Active:=False;
      quMaxMDG.ParamByName('IDH').AsInteger:=MenuTree.Tag;
      quMaxMDG.Active:=True;

      Id:=1;
      if quMaxMDG.RecordCount>0 then
      begin
        Id:=quMaxMDGID.AsInteger+1;
      end;
      quMaxMDG.Active:=False;

      quMDGID.Active:=False;
      quMDGID.ParamByName('IDH').AsInteger:=MenuTree.Tag;
      quMDGID.ParamByName('ID').AsInteger:=Id;
      quMDGID.Active:=True;

      while not quMDGID.Eof do quMDGID.Delete;

      quMDGID.Append;
      quMDGIDIDH.AsInteger:=MenuTree.Tag;
      quMDGIDID.AsInteger:=Id;
      quMDGIDNAMEGR.AsString:=fmMDG.cxTextEdit1.Text;
      quMDGIDPRIOR.AsInteger:=fmMDG.cxSpinEdit1.Value;
      quMDGIDPARENT.AsInteger:=0;
      quMDGID.Post;

      quMDGID.Active:=False;

      bRefresh:=False;

      MenuExpandLevel(nil,MenuTree,dmC.quMenuDay,MenuTree.Tag);

      for i:=0 to MenuTree.Items.Count-1 do
      if Integer(MenuTree.Items.Item[i].Data) = Id then
      begin
        MenuTree.Items[i].Selected:=True;
        MenuTree.Repaint;
      end;

      bRefresh:=True;

      ViewMenuDay.BeginUpdate;
      quMDS.Active:=False;
      quMDS.ParamByName('IDH').AsInteger:=MenuTree.Tag;
      quMDS.ParamByName('ID').AsInteger:=Integer(MenuTree.Selected.Data);
      quMDS.Active:=True;
      ViewMenuDay.EndUpdate;

    end;

    fmMDG.Release;
  end;
end;

procedure TfmMenuDay.Timer1Timer(Sender: TObject);
begin
  if bClear3=True then begin StatusBar1.Panels[0].Text:=''; bClear3:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClear3:=True;
end;


procedure TfmMenuDay.acAddMSubGrExecute(Sender: TObject);
Var Id,IdGr:Integer;
    TreeNode : TTreeNode;
begin
// �������� ���������
  if not CanDo('prAddMenuSubGr') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  if (MenuTree.Items.Count=0)  then
  begin
    showmessage('�������� ������.');
    exit;
  end;
  if (MenuTree.Selected=nil)  then
  begin
    showmessage('�������� ������,���������.');
    exit;
  end;

  with dmC do
  begin
    IdGr:=Integer(MenuTree.Selected.data);

    fmMDG:=TfmMDG.create(Application);
    fmMDG.Caption:='���������� � ������ "'+MenuTree.Selected.Text+'" ����� ���������.';
    fmMDG.cxTextEdit1.Text:='����� ���������';
    fmMDG.cxSpinEdit1.Value:=1;
    fmMDG.ShowModal;
    if fmMDG.ModalResult=mrOk then
    begin
      quMaxMDG.Active:=False;
      quMaxMDG.ParamByName('IDH').AsInteger:=MenuTree.Tag;
      quMaxMDG.Active:=True;

      Id:=1;
      if quMaxMDG.RecordCount>0 then
      begin
        Id:=quMaxMDGID.AsInteger+1;
      end;
      quMaxMDG.Active:=False;

      quMDGID.Active:=False;
      quMDGID.ParamByName('IDH').AsInteger:=MenuTree.Tag;
      quMDGID.ParamByName('ID').AsInteger:=Id;
      quMDGID.Active:=True;

      while not quMDGID.Eof do quMDGID.Delete;

      quMDGID.Append;
      quMDGIDIDH.AsInteger:=MenuTree.Tag;
      quMDGIDID.AsInteger:=Id;
      quMDGIDNAMEGR.AsString:=fmMDG.cxTextEdit1.Text;
      quMDGIDPRIOR.AsInteger:=fmMDG.cxSpinEdit1.Value;
      quMDGIDPARENT.AsInteger:=IdGr;
      quMDGID.Post;

      quMDGID.Active:=False;

      bRefresh:=False;
      TreeNode:=MenuTree.Items.AddChildObject(MenuTree.Selected,fmMDG.cxTextEdit1.Text, Pointer(Id));
      TreeNode.ImageIndex:=8;
      TreeNode.SelectedIndex:=7;
      bRefresh:=True;


    end;
    fmMDG.Release;
  end;
end;

procedure TfmMenuDay.acDelMGrExecute(Sender: TObject);
begin
  //������� ������
  if MenuTree.Focused then
  begin
    if (MenuTree.Items.Count=0)  then
    begin
      exit;
    end;
    if (MenuTree.Selected=nil)  then
    begin
      showmessage('�������� ������,���������.');
      exit;
    end;

    with dmC do
    begin
      if MessageDlg('�� ������������� ������ ������� ������ - "'+MenuTree.Selected.Text+'"?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        if quMDS.RecordCount>0 then
        begin
          showmessage('������ �� �����, �������� ����������!!!');
          exit;
        end;

        quMDFindParent.Active:=False;
        quMDFindParent.ParamByName('PARENTID').AsInteger:=Integer(MenuTree.Selected.Data);
        quMDFindParent.Active:=True;

        if quMDFindParentCOUNTREC.AsInteger>0 then
        begin
          showmessage('������ �� �����, �������� ����������!!!');
          quMDFindParent.Active:=False;
          exit;
        end;
        quMDFindParent.Active:=False;

        //���� ����� �� ���� �� �������� ��������

        //������

        quMDGID.Active:=False;
        quMDGID.ParamByName('IDH').AsInteger:=MenuTree.Tag;
        quMDGID.ParamByName('ID').AsInteger:=Integer(MenuTree.Selected.Data);
        quMDGID.Active:=True;

        quMDGID.First;
        while not quMDGID.Eof  do quMDGID.Delete;

        //������ � ������
        MenuTree.Selected.Delete;

        //��� ����� ���������� ������ �������� - ����� ������� ������� �� ����
        ViewMenuDay.BeginUpdate;
        quMDS.Active:=False;
        quMDS.ParamByName('IDH').AsInteger:=MenuTree.Tag;
        quMDS.ParamByName('ID').AsInteger:=Integer(MenuTree.Selected.Data);
        quMDS.Active:=True;
        ViewMenuDay.EndUpdate;

      end;
    end;
  end;
  if ViewMenuDay.Focused then
  begin
    dmC.quMDS.Delete;
  end;
end;

procedure TfmMenuDay.acEditMGrExecute(Sender: TObject);
Var Id,iPrior,i:Integer;
begin
  //�������������� ������
  if (MenuTree.Items.Count=0)  then
  begin
   // showmessage('�������� ������.');
    exit;
  end;
  if (MenuTree.Selected=nil)  then
  begin
    showmessage('�������� ������,���������.');
    exit;
  end;
  with dmC do
  begin
    Id:=Integer(MenuTree.Selected.Data);

    quMDGID.Active:=False;
    quMDGID.ParamByName('IDH').AsInteger:=MenuTree.Tag;
    quMDGID.ParamByName('ID').AsInteger:=Id;
    quMDGID.Active:=True;

    iPrior:=quMDGIDPRIOR.AsInteger;
    
    fmMDG:=TfmMDG.create(Application);
    fmMDG.Caption:='������ �������� ���� - ���������.';
    fmMDG.cxTextEdit1.Text:=quMDGIDNAMEGR.AsString;
    fmMDG.cxSpinEdit1.Value:=quMDGIDPRIOR.AsInteger;
    fmMDG.ShowModal;
    if fmMDG.ModalResult=mrOk then
    begin
      try
        quMDGID.Edit;
        quMDGIDNAMEGR.AsString:=fmMDG.cxTextEdit1.Text;
        quMDGIDPRIOR.AsInteger:=fmMDG.cxSpinEdit1.Value;
        quMDGID.Post;
      except
      end;

      if iPrior<>fmMDG.cxSpinEdit1.Value then
      begin

        bRefresh:=False;

        MenuExpandLevel(nil,MenuTree,dmC.quMenuDay,MenuTree.Tag);

        for i:=0 to MenuTree.Items.Count-1 do
        if Integer(MenuTree.Items.Item[i].Data) = Id then
        begin
          MenuTree.Items[i].Selected:=True;
          MenuTree.Repaint;
        end;

        bRefresh:=True;

        ViewMenuDay.BeginUpdate;
        quMDS.Active:=False;
        quMDS.ParamByName('IDH').AsInteger:=MenuTree.Tag;
        quMDS.ParamByName('ID').AsInteger:=Integer(MenuTree.Selected.Data);
        quMDS.Active:=True;
        ViewMenuDay.EndUpdate;


      end else MenuTree.Selected.Text:=fmMDG.cxTextEdit1.Text;

    end;

    quMDGID.Active:=False;

    fmMDG.Release;
  end;
end;

procedure TfmMenuDay.MenuTreeDragDrop(Sender, Source: TObject; X,
  Y: Integer);
Var sGr:String;
    iGr:Integer;
    iCo:Integer;
    i,j: Integer;
    iNum: Integer;
    Rec:TcxCustomGridRecord;

begin
  if bDr then
  begin
    bDr:=False;
    sGr:=MenuTree.DropTarget.Text;
    iGr:=Integer(MenuTree.DropTarget.data);
    iCo:=ViewMenuDay.Controller.SelectedRecordCount;
    if iCo>0 then
    begin
      if MessageDlg('�� ������������� ������ ����������� ��������� ������� ('+IntToStr(iCo)+' ��.) � ������ "'+sGr+'"?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        with dmC do
        begin
          taMenu.Active:=False;
          taMenu.Active:=True;

          for i:=0 to ViewMenuDay.Controller.SelectedRecordCount-1 do
          begin
            Rec:=ViewMenuDay.Controller.SelectedRecords[i];

            for j:=0 to Rec.ValueCount-1 do
            begin
              if ViewMenuDay.Columns[j].Name='ViewMenuDaySIFR' then break;
            end;

            iNum:=Rec.Values[j];
          //��� ��� - ����������

            if taMenu.Locate('SIFR',iNum,[]) then
            begin
              trUpd2.StartTransaction;
              taMenu.Edit;
              taMenuPARENT.AsInteger:=iGr;
              taMenu.Post;
              trUpd2.Commit;
            end;
          end;
          taMenu.Active:=False;
          quMenuSel.FullRefresh;
        end;
      end;
    end;
  end;

{
      if CardsView.Controller.SelectedRecordCount>0 then
      begin
        for i:=0 to CardsView.Controller.SelectedRecordCount-1 do
        begin
          Rec:=CardsView.Controller.SelectedRecords[i];

          for j:=0 to Rec.ValueCount-1 do
          begin
            if CardsView.Columns[j].Name='CardsViewId' then break;
          end;

          iNum:=Rec.Values[j];
          //��� ��� - ����������

          if taGoods.Locate('Id',iNum,[]) then
          begin
            taGoods.Edit;
            taGoodsId_Group.AsInteger:=PosP.IdGr;
            taGoodsId_SubGroup.AsInteger:=PosP.Id-10000;
            taGoods.Post;
          end;
        end;
        quCardSel.Active:=False;
        quCardSel.Active:=True;
      end;

}

      //        quMenuSel.Refresh;

end;

procedure TfmMenuDay.MenuTreeExpanding(Sender: TObject; Node: TTreeNode;
  var AllowExpansion: Boolean);
begin
  if Node.getFirstChild.Data = nil then
  begin
    Node.DeleteChildren;
    MenuExpandLevel(Node,MenuTree,dmC.quMenuDay,MenuTree.Tag);
  end;
end;

procedure TfmMenuDay.FormShow(Sender: TObject);
begin
  with dmC do
  begin
    if MenuTree.Selected<>nil then
    begin
      ViewMenuDay.BeginUpdate;
      quMDS.Active:=False;
      quMDS.ParamByName('IDH').AsInteger:=MenuTree.Tag;
      quMDS.ParamByName('ID').AsInteger:=Integer(MenuTree.Selected.Data);
      quMDS.Active:=True;
      ViewMenuDay.EndUpdate;
    end;
  end;
end;

procedure TfmMenuDay.ViewMenuDayDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept:=False;
  if bDrMenuDay then  Accept:=True;
end;

procedure TfmMenuDay.ViewMenuDayDragDrop(Sender, Source: TObject; X,
  Y: Integer);
Var iCo:Integer;
    i,j: Integer;
    iNum: Integer;
    Rec:TcxCustomGridRecord;
    iMax:Integer;
begin
//
  if bDrMenuDay then
  begin
    bDrMenuDay:=False;
    iCo:=fmMenuCr.ViewMenuCr.Controller.SelectedRecordCount;
    if iCo>0 then
    begin
      if MenuTree.Selected=nil then showmessage('�������� ������.')
      else
      begin
        with dmC do
        begin
          if MessageDlg('�� ������������� ������ ��������� ��������� ������� ('+IntToStr(iCo)+' ��.) � ������ "'+MenuTree.Selected.Text+'"?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
          begin
            ViewMenuDay.BeginUpdate;
{            quMDS.Active:=False;
            quMDS.ParamByName('IDH').AsInteger:=MenuTree.Tag;
            quMDS.ParamByName('ID').AsInteger:=Integer(MenuTree.Selected.Data);
            quMDS.Active:=True;
}
            quMDS.FullRefresh;
            
            iMax:=1;
            if quMDS.RecordCount>0 then
            begin
              quMDS.Last;
              iMax:=quMDSPRI.AsInteger+1;
            end;


            for i:=0 to fmMenuCr.ViewMenuCr.Controller.SelectedRecordCount-1 do
            begin
              Rec:=fmMenuCr.ViewMenuCr.Controller.SelectedRecords[i];

              for j:=0 to Rec.ValueCount-1 do
              begin
                if fmMenuCr.ViewMenuCr.Columns[j].Name='ViewMenuCrSIFR' then break;
              end;

              iNum:=Rec.Values[j];
          //��� ��� - ����������

              if quMDS.Locate('SIFR',iNum,[])=False then
              begin
                quMDS.Append;
                quMDSIDH.AsInteger:=MenuTree.Tag;
                quMDSIDGR.AsInteger:=Integer(MenuTree.Selected.Data);
                quMDSSIFR.AsInteger:=iNum;
                quMDSPRI.AsInteger:=iMax;
                quMDS.Post;

                inc(iMax);
                quMDS.Refresh;
              end;
            end;

            ViewMenuDay.EndUpdate;
          end;
        end;
      end;
    end;
  end;

end;

procedure TfmMenuDay.acPrintExecute(Sender: TObject);
Var StrWk:String;
begin
//������
  with dmC do
  begin
    quMDRep.Active:=False;
    quMDRep.ParamByName('IDH').AsInteger:=MenuTree.Tag;
    quMDRep.Active:=True;

    strwk:=FormatDateTime('mm',GridMenuDay.Tag);
    if StrWk='01' then StrWk:=' ������ ';
    if StrWk='02' then StrWk:=' ������� ';
    if StrWk='03' then StrWk:=' ����� ';
    if StrWk='04' then StrWk:=' ������ ';
    if StrWk='05' then StrWk:=' ��� ';
    if StrWk='06' then StrWk:=' ���� ';
    if StrWk='07' then StrWk:=' ���� ';
    if StrWk='08' then StrWk:=' ������� ';
    if StrWk='09' then StrWk:=' �������� ';
    if StrWk='10' then StrWk:=' ������� ';
    if StrWk='11' then StrWk:=' ������ ';
    if StrWk='12' then StrWk:=' ������� ';

    frRepMD.LoadFromFile(CurDir + 'MenuDay.frf');
    frRepMD.ReportName:='����.';
    frVariables.Variable['MDate']:=FormatDateTime('�� dd'+StrWk+'yyyy �',GridMenuDay.Tag);

    frRepMD.PrepareReport;
    frRepMD.ShowPreparedReport;
  end;
end;

end.
