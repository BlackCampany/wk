unit Modif;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Placemnt, ExtCtrls, cxStyles, cxCustomData, cxGraphics,
  cxFilter, cxData, cxEdit, DB, cxDBData, cxGridLevel, cxClasses,
  cxControls, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxLookAndFeelPainters, StdCtrls, cxButtons,
  ActnList, XPStyleActnCtrls, ActnMan, Menus, cxDataStorage;

type
  TfmModif = class(TForm)
    Panel1: TPanel;
    FormPlacement1: TFormPlacement;
    ViewMo: TcxGridDBTableView;
    LevelMo: TcxGridLevel;
    GridMo: TcxGrid;
    Button3: TcxButton;
    cxButton1: TcxButton;
    ViewMoSIFR: TcxGridDBColumn;
    ViewMoNAME: TcxGridDBColumn;
    ViewMoPARENT: TcxGridDBColumn;
    ViewMoPRICE: TcxGridDBColumn;
    ViewMoREALPRICE: TcxGridDBColumn;
    ViewMoIACTIVE: TcxGridDBColumn;
    ViewMoIEDIT: TcxGridDBColumn;
    Panel2: TPanel;
    Label1: TLabel;
    amMod: TActionManager;
    acSelMod: TAction;
    acExitMod: TAction;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ViewMoCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure cxButton1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure acExitModExecute(Sender: TObject);
    procedure acSelModExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmModif: TfmModif;

implementation

uses Dm, Un1, Spec;

{$R *.dfm}

procedure TfmModif.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  ViewMo.RestoreFromIniFile(CurDir+GridIni,False);
end;

procedure TfmModif.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewMo.StoreToIniFile(CurDir+GridIni);
end;

procedure TfmModif.ViewMoCustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);
var
  ARec: TRect;
  ATextToDraw: string;
begin
  if (AViewInfo is TcxGridTableDataCellViewInfo) then
    ATextToDraw := AViewInfo.GridRecord.DisplayTexts[AViewInfo.Item.Index]
  else
    ATextToDraw := VarAsType(AViewInfo.Item.Caption, varString);

  ARec := AViewInfo.Bounds;
  ACanvas.Canvas.Brush.Bitmap := ABitmap;

  ACanvas.Canvas.FillRect(ARec);
  SetBkMode(ACanvas.Canvas.Handle, TRANSPARENT);
  ACanvas.DrawText(ATextToDraw, AViewInfo.Bounds, 0, True);
  ADone := True; // }
end;

procedure TfmModif.cxButton1Click(Sender: TObject);
begin
  acSelMod.Execute;
end;

procedure TfmModif.Button3Click(Sender: TObject);
begin
  acExitMod.Execute;
end;

procedure TfmModif.FormShow(Sender: TObject);
begin
  GridMo.SetFocus;
end;

procedure TfmModif.acExitModExecute(Sender: TObject);
begin
  close;
end;

procedure TfmModif.acSelModExecute(Sender: TObject);
begin
  if (MaxMod-CountMod)>0 then
  begin
    with fmSpec do
    begin
      ViewMod.BeginUpdate;
      with dmC do
      begin
        quCurMod.Append;
        quCurModSTATION.AsInteger:=CommonSet.Station;
        quCurModID_TAB.AsInteger:=Tab.Id;
        quCurModId_Pos.AsInteger:=quCurSpecID.AsInteger;
        quCurModId.AsInteger:=Check.Max+1;
        quCurModSifr.AsInteger:=quModifSIFR.AsInteger;
        quCurModName.AsString:=quModifName.AsString;
        quCurModQuantity.AsFloat:=quCurSpecQUANTITY.AsFloat;
        quCurMod.Post;

        inc(Check.Max);
        ViewMod.EndUpdate;
      end;
      inc(CountMod);
    end;
  end;
  Label1.Caption:='�������� � ���������� '+INtToStr(MaxMod-CountMod)+' ������������.';
  GridMo.SetFocus;
  if (MaxMod-CountMod)=0 then close;
end;

{

Var StrWk:String;
begin
  with dmC do
  begin
    if quCurSpec.RecordCount>0 then
    begin
      fmDiscount.cxTextEdit1.Text:='';
      fmDiscount.Caption:='��������� ���������� �����';
      fmDiscount.ShowModal;
      if fmDiscount.ModalResult=mrOk then
      begin
        //������� ������� ������
//      showmessage('');

        if fmDiscount.cxTextEdit1.Text>'' then
        begin
          DiscountBar:=fmDiscount.cxTextEdit1.Text;
          WriteHistory('��:"'+DiscountBar+'"');

          If FindDiscount(DiscountBar) then
          begin
            // ��������� ������
            Str(Tab.DPercent:5:2,StrWk);
            Label7.Caption:='������������ ������ - '+ Tab.DName+' ('+StrWk+'%)';
            //������ ������ �� ��� �������
            ViewSpec.BeginUpdate;
            quCurSpec.First;
            While not quCurSpec.Eof do
            begin
              CalcDiscontN(quCurSpecSIFR.AsInteger,quCurSpecID.AsInteger,-1,quCurSpecSTREAM.AsInteger,quCurSpecPrice.AsFloat,quCurSpecQuantity.AsFloat,quCurSpecQuantity.AsFloat*quCurSpecPrice.AsFloat,Tab.DBar,Now,Check.DProc,Check.DSum);
              if Check.DSum<>0 then
              begin
                quCurSpec.Edit;
                quCurSpecSumma.AsFloat:=RoundEx(quCurSpecPrice.AsFloat*quCurSpecQuantity.AsFloat*100)/100-Check.DSum;
                quCurSpecDProc.AsFloat:=Check.DProc;
                quCurSpecDSum.AsFloat:=Check.DSum;
                quCurSpec.Post;

                ViewSpecDPROC.Visible:=True;
                ViewSpecDSum.Visible:=True;
              end;
              quCurSpec.Next;
            end;
            quCurSpec.First;
            RecalcSum;
            ViewSpec.EndUpdate;
          end;
        end else //������ �������� - ��� ������ ������
        begin
            //������ ������ �� ��� �������
          DiscountBar:='';
          Check.DProc:=0;
          Check.DSum:=0;
          Tab.DBar:='';
          Tab.DPercent:=0;
          Tab.DName:='';

          ViewSpec.BeginUpdate;
          quCurSpec.First;
          While not quCurSpec.Eof do
          begin
            quCurSpec.Edit;
            quCurSpecSumma.AsFloat:=RoundEx(quCurSpecPrice.AsFloat*quCurSpecQuantity.AsFloat*100)/100;
            quCurSpecDProc.AsFloat:=0;
            quCurSpecDSum.AsFloat:=0;
            quCurSpec.Post;

            quCurSpec.Next;
          end;
          //�������

          Label7.Caption:='';
          ViewSpecDPROC.Visible:=False;
          ViewSpecDSum.Visible:=False;

          quCurSpec.First;
          RecalcSum;
          ViewSpec.EndUpdate;
        end;
      end;
    end;
  end;
end;

}

end.
