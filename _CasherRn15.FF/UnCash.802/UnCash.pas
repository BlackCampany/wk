unit UnCash;

interface

uses
//  ������� ��� ��-101 ���

  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ComObj, ActiveX, OleServer, StdCtrls;

function InspectSt(var sMess: string): Integer;

function GetSt(var sMess: string): INteger;

function OpenZ: Boolean;  //��������

function GetSerial: Boolean;

function GetNums: Boolean;

function GetRes: Boolean;

function GetSums(var rSum1, rSum2, rSum3: Real): Boolean;

function CashOpen(sPort: PChar): Boolean;

function CashClose: Boolean;

function CashDate(var sDate: string): Boolean;

function GetX: Boolean;

function ZOpen: Boolean; //������ �����

function ZClose: Boolean;

function CheckStart: Boolean;

function CheckRetStart: Boolean;

function CheckAddPos(var iSum: Integer): Boolean;

function CheckTotal(var iTotal: Integer): Boolean;

function CheckDiscount(rDiscount: Real; var iDisc, iItog: Integer): Boolean;

function CheckRasch(iPayType, iClient: Integer; Comment: string; var iDopl: Integer): Boolean;

function CheckClose: Boolean;

procedure CheckCancel;

function InCass(rSum: Real): Boolean;//����������

function GetCashReg(var rSum: Real): Boolean;//���������� � �����

function CashDriver: Boolean;

procedure TestPosCh;

function OpenNFDoc: Boolean;

function CloseNFDoc: Boolean;

function PrintNFStr(StrPr: string): Boolean;

function SelectF(iNum: Integer): Boolean;

function CutDoc: Boolean;

function CutDoc1: Boolean;

procedure WriteHistoryFR(Strwk_: string);

function TestStatus(StrOp: string; var SMess: string): Boolean;

function IsOLEObjectInstalled(Name: string): boolean;

function More24H(var iSt: INteger): Boolean;

function CheckOpenFis: Boolean;

function GetNumCh: Boolean;

function GetSumDisc(var rSum1, rSum2, rSum3, rSum4: Real): Boolean;

function SetDP(Str1, Str2: string): Boolean;

function CheckOpen: Boolean;

procedure prBnPrint;

procedure TestFp(StrOp: string);

procedure prClearBuf;

type
  TStatusFR = record
    St1: string[2];
    St2: string[4];
    St3: string[4];
    St4: string[10];
  end;

  TNums = record
    bOpen: Boolean;
    ZNum: Integer;
    SerNum: string[11];
    RegNum: string[10];
    CheckNum: string[4];
    iCheckNum: Integer;
    iRet: INteger;
    sRet: string;
    sDate: string;
    ZYet: Integer;
    sDateTimeBeg: string;
  end;

  TCurPos = record
    Articul: string;
    Bar: string;
    Name: string;
    EdIzm: string;
    TypeIzm: INteger;
    Quant: Real;
    Price: Real;
    DProc: Real;
    DSum: Real;
    Depart: SmallInt;
  end;

  TPos = record
    Name: string;
    Code: string;
    AddName: string;
    Price: Integer;
    Count: Integer;
    Stream: Integer;
    Sum: Real;
    Bar: string;
  end;

var
  CommandsArray: array[1..32000] of Char;
  HeapStatus: THeapStatus;
  PressCount: Byte;
  StatusFr: TStatusFr;
  Nums: TNums;
  CurPos: TCurPos;
  SelPos: TCurPos;
  sMessage: string;
  PosCh: TPos;
  iRfr: SmallInt;
  rSumR, rSumD: Real;
  sBuffer: string;
  BufferCount: Integer;
  sNFDoc:string;

const
  iPassw: Integer = 30;

implementation

uses
  Un1, Dm, MainFF, Attention;

procedure prClearBuf;
begin
  //Fillchar(Buffer, Sizeof(Buffer), 0);
  sBuffer := '';
  BufferCount := 0;
end;

procedure TestFp(StrOp: string);
begin
  while TestStatus(StrOp, sMessage) = False do
  begin
    fmAttention.Label1.Caption := sMessage;
    fmAttention.ShowModal;
    Nums.iRet := InspectSt(Nums.sRet);
    prWriteLog('~~AttentionShow;' + sMessage + ';' + 'InspectSt(Nums.sRet)=' + IntToStr(Nums.iRet));
  end;
end;

procedure prBnPrint;
begin
//����� ����� ������ ������ ����� ���������� �� ����������
  try
//      with fmMainCasher do
//      begin
    if BnStr > '' then
    begin
      if OpenNFDoc = false then
      begin
        TestFp('PrintNFStr');
        delay(100);
        OpenNFDoc;
      end;

      while BnStr > '' do
      begin
        if (ord(BnStr[1]) = $0D) or (ord(BnStr[1]) = $0A) then
          Delete(BnStr, 1, 1)
        else
        begin
          if ord(BnStr[1]) = $01 then
          begin
            try
              if CloseNFDoc = False then
              begin
                TestFp('PrintNFStr');
//                CloseNFDoc;
              end;

              if (CommonSet.TypeFis = 'prim') or (CommonSet.TypeFis = 'shtrih') then
              begin
                PrintNFStr(' ');
                PrintNFStr(' ');
                PrintNFStr(' ');
                PrintNFStr(' ');
                PrintNFStr(' ');
                PrintNFStr(' ');
                CutDoc;
              end;

              delay(33);

              if OpenNFDoc = false then
              begin
                TestFp('PrintNFStr');
                OpenNFDoc;
              end;
            finally
              Delete(BnStr, 1, 1);
            end;
          end
          else
          begin
            if Pos(Chr($0D), BnStr) > 0 then
            begin
              StrWk := Copy(BnStr, 1, Pos(Chr($0D), BnStr) - 1);
              Delete(BnStr, 1, Pos(Chr($0D), BnStr));
            end
            else
            begin
              if Pos(Chr($01), BnStr) > 0 then
              begin
                StrWk := Copy(BnStr, 1, Pos(Chr($01), BnStr) - 1);
                Delete(BnStr, 1, Pos(Chr($01), BnStr) - 1);
              end
              else
              begin
                StrWk := BnStr;
                BnStr := '';
              end;
            end;
            if pos('�����', StrWk) > 0 then
              PrintNFStr('')
            else if PrintNFStr(StrWk) = False then
            begin
              TestFp('PrintNFStr');
              PrintNFStr(StrWk);
            end;
          end;
        end;
      end;
      if CloseNFDoc = False then
      begin
        TestFp('PrintNFStr');
//        CloseNFDoc;
      end;
       //   end;
    end;
  except
    prWriteLog('ErrPrint');
  end;
end;

function CheckOpen: Boolean;
//Var iSt:INteger;
begin
  if Commonset.CashNum <= 0 then
  begin
    Result := False;
    exit;
  end;
  result := False;
end;

function SetDP(Str1, Str2: string): Boolean;
begin
  if (Commonset.CashNum <= 0) then
  begin
    Result := True;
    exit;
  end;
  Result := True
end;

function GetSumDisc(var rSum1, rSum2, rSum3, rSum4: Real): Boolean;
begin
  rSum1 := 0;
  rSum2 := 0;
  rSum3 := 0;
  rSum4 := 0;
  if (Commonset.CashNum <= 0) then
  begin
    inc(Nums.iCheckNum);
    Result := True;
    exit;
  end;

  Result := True;
end;

function GetNumCh: Boolean;
begin
  if (Commonset.CashNum <= 0) then
  begin
    inc(Nums.iCheckNum);
    Result := True;
    exit;
  end;

  inc(Nums.iCheckNum);
  Nums.CheckNum := IntToStr(Nums.iCheckNum);

  Result := True;
end;

function GetCashReg(var rSum: Real): Boolean;//���������� � �����
var
  StrSend: string;
  StrSendU: UTF8String;
  sRes, Sid: string;
  n: Integer;
  a: TGUID;
  sSum:string;
begin
  rSum := -1;
  if (Commonset.CashNum <= 0) then
  begin
    inc(Nums.iCheckNum);
    Result := True;
    exit;
  end;

  //�������� X �����
  with fmMainFF do
  begin
    result := False;
    try
      prClearBuf;

      CreateGUID(a);
      Sid := GUIDTostring(a);

      StrSend := '';
      StrSend := StrSend + Trim('<?xml version="1.0" encoding="UTF-8"?>');
      StrSend := StrSend + Trim('<ArmRequest>');
      StrSend := StrSend + Trim('    <RequestBody>');
      StrSend := StrSend + Trim('        <ProtocolLabel>OFDFNARMUKM</ProtocolLabel>');
      StrSend := StrSend + Trim('        <ProtocolVersion>10.1</ProtocolVersion>');
      StrSend := StrSend + Trim('        <RequestId>' + Sid + '</RequestId>');
      StrSend := StrSend + Trim('        <DateTime>' + curdt + '</DateTime>');
      StrSend := StrSend + Trim('        <Command>40</Command>');
      StrSend := StrSend + Trim('    </RequestBody>');
      StrSend := StrSend + Trim('    <RequestData><![CDATA[]]>');
      StrSend := StrSend + Trim('</RequestData>');
      StrSend := StrSend + Trim('</ArmRequest>');

      StrSendU := AnsiToUtf8(StrSend);

      if ClientSock.Active = False then
      begin
        ClientSock.Host := '';
        ClientSock.Address := CommonSet.CashTCP;
        ClientSock.Port := 6667;
        ClientSock.Open;
        Delay(1000);
      end;

//<CashDrawerSumCash>14853.37</CashDrawerSumCash>

      if ClientSock.Active then
      begin
        ClientSock.Socket.SendText(StrSendU);

        for n := 1 to 600 do
        begin
          delay(50);
          if Length(sBuffer) > 0 then
            break; //�� 50 �� ������ �� ����������...
        end;
        if sBuffer > '' then
        begin
          sRes := sBuffer;
          if Pos(Sid, sRes) > 0 then
          begin
            if Pos('<Result>0</Result>', sRes) > 0 then
            begin
              sMessErrorFis:='';
              if Pos('<CashDrawerSumCash>', sRes) > 0 then
                if Pos('</CashDrawerSumCash>', sRes) > 0 then
                begin
                  sSum:=Copy(sRes,Pos('<CashDrawerSumCash>', sRes)+19,Pos('</CashDrawerSumCash>', sRes)-(Pos('<CashDrawerSumCash>', sRes)+19));
                  if sSum>'' then
                  begin
                    while Pos('.',sSum)>0 do sSum[Pos('.',sSum)]:=',';
                    rSum:=StrToFloatDef(sSum,-1);
                    result := True;
                  end;
                end;
            end else
            begin
              //<ErrorDescription>����������������� ����� ��������� 24 ����</ErrorDescription>
              if Pos('<ErrorDescription>', sRes)>0 then
                if Pos('</ErrorDescription>', sRes)>0 then
                begin
                  sMessErrorFis:=Utf8ToAnsi(Copy(sRes,Pos('<ErrorDescription>', sRes)+18,Pos('</ErrorDescription>', sRes)-(Pos('<ErrorDescription>', sRes)+18)));
                end;
            end;
          end;
        end;
      end;

      writefislog(StrSendU, sBuffer);
    except
      result := False;
    end;
  end;

end;

function InCass(rSum: Real): Boolean;
var
  StrSend: string;
  StrSendU: UTF8String;
  sRes, Sid: string;
  n: Integer;
  a: TGUID;
  sOp:string;
begin
  Result := True;
  if (Commonset.CashNum <= 0) then
  begin
    exit;
  end;

  //�������� - �������
  with fmMainFF do
  begin
    result := False;
    try
      prClearBuf;

      if rSum>0 then sOp:='1' else sOp:='2';

      CreateGUID(a);
      Sid := GUIDTostring(a);

      StrSend := '';
      StrSend := StrSend + Trim('<?xml version="1.0" encoding="UTF-8"?>');
      StrSend := StrSend + Trim('<ArmRequest>');
      StrSend := StrSend + Trim('    <RequestBody>');
      StrSend := StrSend + Trim('        <ProtocolLabel>OFDFNARMUKM</ProtocolLabel>');
      StrSend := StrSend + Trim('        <ProtocolVersion>10.1</ProtocolVersion>');
      StrSend := StrSend + Trim('        <RequestId>' + Sid + '</RequestId>');
      StrSend := StrSend + Trim('        <DateTime>' + curdt + '</DateTime>');
      StrSend := StrSend + Trim('        <Command>38</Command>');
      StrSend := StrSend + Trim('    </RequestBody>');
      StrSend := StrSend + Trim('    <RequestData><![CDATA[');
      StrSend := StrSend + Trim('        <ArmMoneyOperation>');
      StrSend := StrSend + Trim('             <UserLoginName>' + Person.Name + '</UserLoginName>');
      StrSend := StrSend + Trim('             <DateTime>' + curdt + '</DateTime>');
      StrSend := StrSend + Trim('             <MoneyOperationType>'+sOp+'</MoneyOperationType>');
      StrSend := StrSend + Trim('             <HeaderText> ����� '+its(CommonSet.CashNum)+'</HeaderText>');
      StrSend := StrSend + Trim('             <CashSumm>'+fts(Abs(rSum))+'</CashSumm>');
      StrSend := StrSend + Trim('             <Footer></Footer>');
      StrSend := StrSend + Trim('        </ArmMoneyOperation>]]>');
      StrSend := StrSend + Trim('    </RequestData>');
      StrSend := StrSend + Trim('</ArmRequest>');

      StrSendU := AnsiToUtf8(StrSend);

      if ClientSock.Active = False then
      begin
        ClientSock.Host := '';
        ClientSock.Address := CommonSet.CashTCP;
        ClientSock.Port := 6667;
        ClientSock.Open;
        Delay(1000);
      end;

      if ClientSock.Active then
      begin
        ClientSock.Socket.SendText(StrSendU);

        for n := 1 to 600 do
        begin
          delay(50);
          if Length(sBuffer) > 0 then
            break; //�� 50 �� ������ �� ����������...
        end;
        if sBuffer > '' then
        begin
          sRes := sBuffer;
          if Pos(Sid, sRes) > 0 then
          begin
            if Pos('<Result>0</Result>', sRes) > 0 then
            begin
              sMessErrorFis:='';
              result := True;
            end else
            begin
              //<ErrorDescription>����������������� ����� ��������� 24 ����</ErrorDescription>
              if Pos('<ErrorDescription>', sRes)>0 then
                if Pos('</ErrorDescription>', sRes)>0 then
                begin
                  sMessErrorFis:=Utf8ToAnsi(Copy(sRes,Pos('<ErrorDescription>', sRes)+18,Pos('</ErrorDescription>', sRes)-(Pos('<ErrorDescription>', sRes)+18)));
                end;
            end;
          end;
        end;
      end;

      writefislog(StrSendU, sBuffer);
    except
      result := False;
    end;
  end;
end;

function GetSums(var rSum1, rSum2, rSum3: Real): Boolean;
begin
  result := True;
  if (Commonset.CashNum <= 0) then
  begin
    Result := True;
    exit;
  end;
  rSum1 := 0;
  rSum2 := 0;
  rSum3 := 0;
end;

function More24H(var iSt: INteger): Boolean;
begin
  result := False;
  if (Commonset.CashNum <= 0) then
  begin
    Result := False;
    exit;
  end;
end;

function IsOLEObjectInstalled(Name: string): boolean;
var
  ClassID: TCLSID;
  Rez: HRESULT;
begin

// ���� CLSID OLE-�������
  Rez := CLSIDFromProgID(PWideChar(WideString(Name)), ClassID);
  if Rez = S_OK then
 // ������ ������
    Result := true
  else
    Result := false;
end;

function GetSt(var sMess: string): Integer;
begin
  if (Commonset.CashNum <= 0) then
  begin
    Result := 0;
    exit;
  end;

  result := 0;
  sMess := '';
end;

function TestStatus(StrOp: string; var SMess: string): Boolean;
var
  St1, St2, St3: ShortINt;
begin
  Result := True;
  SMess := '';
  if (Commonset.CashNum <= 0) then  exit;

  iRfr := 0;
  St1 := 0;
  St2 := 0;
  St3 := 0;

  if sMessErrorFis>'' then
  begin
    SMess:=sMessErrorFis;
    iRfr:=1;
  end;

  if (iRfr <> 0) or (St1 <> 0) or ((St2 <> 2) and (St2 <> 0)) or (St3 <> 0) then
  begin
    prWriteLog('--' + StrOp + ';' + its(iRfr) + ';' + its(St1) + ';' + its(St2) + ';' + its(St3) + ';' + SMess);
    Result := False; //���� ���������� �.�. ����� ����������� ������� �������, �������� ������� ����
  end;
end;

procedure WriteHistoryFR(Strwk_: string);
var
  F: TextFile;
  Strwk1: string;
  FileN: string;
begin
  try
    if not DirectoryExists(CommonSet.PathHistory) then
    begin
//      ShowMessage('������: ����������� ����������� - "'+CommonSet.PathHistory+'"');
      exit;
    end;

    Strwk1 := 'fr' + FormatDateTime('yyyy_mm', Date);
    Strwk1 := Strwk1 + '.txt';
    Application.ProcessMessages;

    FileN := CommonSet.PathHistory + Strwk1;

    AssignFile(F, FileN);
    if not FileExists(FileN) then
      Rewrite(F)
    else
      Append(F);
    Write(F, FormatDateTime('DD/MM/YYYY  HH:NN:SS ', Now));
    WriteLn(F, Strwk_);
    Flush(F);
  finally
    CloseFile(F);
  end;
end;

function CutDoc: Boolean;
begin
  if (Commonset.CashNum <= 0) then
  begin

    dmC.prCutDoc(CommonSet.PortCash, 0);

    Result := True;
    exit;
  end;

  Result := True;
end;

function CutDoc1: Boolean;
begin
  if (Commonset.CashNum <= 0) then
  begin
    Result := True;
    exit;
  end;
  Result := True;
end;

function SelectF(iNum: Integer): Boolean;
begin
  Result := True;
  if (Commonset.CashNum <= 0) then
  begin
    Result := True;
    exit;
  end;
end;

function PrintNFStr(StrPr: string): Boolean;
begin
  sNFDoc:=sNFDoc+StrPr+#$0D;
  Result := True;
end;

function OpenNFDoc: Boolean;
begin
  if (Commonset.CashNum <= 0) then
  begin
    dmC.prDevOpen(CommonSet.PortCash, 0);

    Result := True;
    exit;
  end;
  sNFDoc:='';
  Result := True;
end;

function CloseNFDoc: Boolean;
var
  StrSend: string;
  StrSendU: UTF8String;
  sRes, Sid: string;
  n: Integer;
  a: TGUID;
begin
  result := False;
  if (Commonset.CashNum <= 0) then
  begin
    dmC.prCutDoc(CommonSet.PortCash, 0);
    dmC.prDevClose(CommonSet.PortCash, 0);
    Result := True;
    exit;
  end;
  if sNFDoc>'' then
  begin
    //������ ������������ ��������
    with fmMainFF do
    begin
      try
        prClearBuf;

        CreateGUID(a);
        Sid := GUIDTostring(a);

        StrSend := '';
        StrSend:=StrSend+Trim('<?xml version="1.0" encoding="UTF-8"?>');
        StrSend:=StrSend+Trim('<ArmRequest>');
        StrSend:=StrSend+Trim('    <RequestBody>');
        StrSend:=StrSend+Trim('        <ProtocolLabel>OFDFNARMUKM</ProtocolLabel>');
        StrSend:=StrSend+Trim('        <ProtocolVersion>10.1</ProtocolVersion>');
        StrSend:=StrSend+Trim('        <RequestId>' + Sid + '</RequestId>');
        StrSend:=StrSend+Trim('        <DateTime>' + curdt + '</DateTime>');
        StrSend:=StrSend+Trim('        <Command>30</Command>');
        StrSend:=StrSend+Trim('    </RequestBody>');
        StrSend:=StrSend+Trim('    <RequestData><![CDATA[');
        StrSend:=StrSend+Trim('    <ArmNonFiscalDoc>');
        StrSend:=StrSend+Trim('        <NonFiscalText>'+sNFDoc);
        StrSend:=StrSend+Trim('        </NonFiscalText>');
        StrSend:=StrSend+Trim('    </ArmNonFiscalDoc>');
        StrSend:=StrSend+Trim(']]>');
        StrSend:=StrSend+Trim('    </RequestData>');
        StrSend:=StrSend+Trim('</ArmRequest>');

        StrSendU := AnsiToUtf8(StrSend);

        if ClientSock.Active = False then
        begin
          ClientSock.Host := '';
          ClientSock.Address := CommonSet.CashTCP;
          ClientSock.Port := 6667;
          ClientSock.Open;
          Delay(1000);
        end;

        if ClientSock.Active then
        begin
          ClientSock.Socket.SendText(StrSendU);

          for n := 1 to 600 do
          begin
            delay(50);
            if Length(sBuffer) > 0 then
              break; //�� 50 �� ������ �� ����������...
          end;
          if sBuffer > '' then
          begin
            sRes := sBuffer;
            if Pos(Sid, sRes) > 0 then
            begin
              if Pos('<Result>0</Result>', sRes) > 0 then
              begin
                sMessErrorFis:='';
                result := True;
              end else
              begin
                //<ErrorDescription>����������������� ����� ��������� 24 ����</ErrorDescription>
                if Pos('<ErrorDescription>', sRes)>0 then
                  if Pos('</ErrorDescription>', sRes)>0 then
                  begin
                    sMessErrorFis:=Utf8ToAnsi(Copy(sRes,Pos('<ErrorDescription>', sRes)+18,Pos('</ErrorDescription>', sRes)-(Pos('<ErrorDescription>', sRes)+18)));
                  end;
              end;
            end;
          end;
        end;

        writefislog(StrSendU, sBuffer);
      except
        result := False;
      end;
    end;
  end
end;

function CashDriver: Boolean;
var
  StrSend: string;
  StrSendU: UTF8String;
  sRes, Sid: string;
  n: Integer;
  a: TGUID;
begin
  if (Commonset.CashNum <= 0) then
  begin
    Result := True;
    exit;
  end;

  //������� ��

  with fmMainFF do
  begin
    result := False;
    try
      prClearBuf;

      CreateGUID(a);
      Sid := GUIDTostring(a);

      StrSend := '';
      StrSend := StrSend + Trim('<?xml version="1.0" encoding="UTF-8"?>');
      StrSend := StrSend + Trim('<ArmRequest>');
      StrSend := StrSend + Trim('    <RequestBody>');
      StrSend := StrSend + Trim('        <ProtocolLabel>OFDFNARMUKM</ProtocolLabel>');
      StrSend := StrSend + Trim('        <ProtocolVersion>10.1</ProtocolVersion>');
      StrSend := StrSend + Trim('        <RequestId>' + Sid + '</RequestId>');
      StrSend := StrSend + Trim('        <DateTime>' + curdt + '</DateTime>');
      StrSend := StrSend + Trim('        <Command>36</Command>');
      StrSend := StrSend + Trim('    </RequestBody>');
      StrSend := StrSend + Trim('    <RequestData><![CDATA[]]>');
      StrSend := StrSend + Trim('</RequestData>');
      StrSend := StrSend + Trim('</ArmRequest>');

      StrSendU := AnsiToUtf8(StrSend);

      if ClientSock.Active = False then
      begin
        ClientSock.Host := '';
        ClientSock.Address := CommonSet.CashTCP;
        ClientSock.Port := 6667;
        ClientSock.Open;
        Delay(1000);
      end;

      if ClientSock.Active then
      begin
        ClientSock.Socket.SendText(StrSendU);

        for n := 1 to 600 do
        begin
          delay(50);
          if Length(sBuffer) > 0 then
            break; //�� 50 �� ������ �� ����������...
        end;
        if sBuffer > '' then
        begin
          sRes := sBuffer;
          if Pos(Sid, sRes) > 0 then
          begin
            if Pos('<Result>0</Result>', sRes) > 0 then
            begin
              sMessErrorFis:='';
              result := True;
            end else
            begin
              //<ErrorDescription>����������������� ����� ��������� 24 ����</ErrorDescription>
              if Pos('<ErrorDescription>', sRes)>0 then
                if Pos('</ErrorDescription>', sRes)>0 then
                begin
                  sMessErrorFis:=Utf8ToAnsi(Copy(sRes,Pos('<ErrorDescription>', sRes)+18,Pos('</ErrorDescription>', sRes)-(Pos('<ErrorDescription>', sRes)+18)));
                end;
            end;
          end;
        end;
      end;

      writefislog(StrSendU, sBuffer);
    except
      result := False;
    end;
  end;
end;

function CheckClose: Boolean;
begin
  if (Commonset.CashNum <= 0) then
  begin
    Result := True;
    exit;
  end;
  Result := True;
end;

function CheckRasch(iPayType, iClient: Integer; Comment: string; var iDopl: Integer): Boolean;
var
  ValName: string;
  StrSend: string;
  StrSendU: UTF8String;
  sRes, Sid, sFoot, sSd: string;
  n: Integer;
  a: TGUID;
  rSumCh,rSumSd:Real;
begin
  iDopl := 0;
  ValName := '';
  if (Commonset.CashNum <= 0) then
  begin
    Result := True;
    exit;
  end;
  with fmMainFF do
  begin
    with dmC do
    begin
//      result := False;
      result := True;
      try
        inc(CommonSet.CashChNum);    // ������ ��� ������� ��������
        Nums.iCheckNum:=CommonSet.CashChNum;
        Nums.CheckNum:=its(CommonSet.CashChNum);
        WriteCheckNum;

        prClearBuf;

        CreateGUID(a);
        Sid := GUIDTostring(a);

        StrSend := '';
        StrSend := StrSend + Trim('<?xml version="1.0" encoding="UTF-8"?>');
        StrSend := StrSend + Trim('<ArmRequest> ');
        StrSend := StrSend + Trim('    <RequestBody>');
        StrSend := StrSend + Trim('        <ProtocolLabel>OFDFNARMUKM</ProtocolLabel>');
        StrSend := StrSend + Trim('        <ProtocolVersion>10.1</ProtocolVersion>   ');
        StrSend := StrSend + Trim('        <RequestId>' + Sid + '</RequestId>  ');
        StrSend := StrSend + Trim('        <DateTime>' + curdt + '</DateTime>');
        StrSend := StrSend + Trim('        <Command>8</Command>                    ');
        StrSend := StrSend + Trim('    </RequestBody>                              ');
        StrSend := StrSend + Trim('    <RequestData>                              ');
        StrSend := StrSend + Trim('        <![CDATA[<ArmReceipt>                   ');
        StrSend := StrSend + Trim('        <UserLoginName>' + Person.Name + '</UserLoginName>   ');
        StrSend := StrSend + Trim('        <ReceiptType>' + its(Operation + 1) + '</ReceiptType>  ');  //0 �������  2 �������
        StrSend := StrSend + Trim('        <DateTime>' + curdt + '</DateTime> ');
        StrSend := StrSend + Trim('        <TaxSystem>' + its(CommonSet.TaxSystem) + '</TaxSystem>            ');
        StrSend := StrSend + Trim('        <HeaderText>');
        StrSend := StrSend + Trim('        </HeaderText>');
        StrSend := StrSend + Trim('        <Items>                                  ');

        ViewSpec.BeginUpdate;
        try
          rSumCh:=0;
          quCurSpec.First;
          while not quCurSpec.Eof do
          begin      //��������� �������
            {
            PosCh.Name:=quCurSpecNAME.AsString;
            PosCh.Code:=quCurSpecSIFR.AsString;
            PosCh.AddName:='';
            PosCh.Price:=RoundEx(quCurSpecPRICE.AsFloat*100);
            PosCh.Count:=RoundEx(quCurSpecQUANTITY.AsFloat*1000);

            rDiscont:=rDiscont+quCurSpecDSUM.AsFloat;
            rSumWithDisc:=rSumWithDisc+PosCh.Price*PosCh.Count;
            }
            rSumCh:=rSumCh+quCurSpecSUMMA.AsFloat;

            StrSend := StrSend + Trim('            <ArmReceiptItem>                     ');
            StrSend := StrSend + Trim('                <ItemArticle>'+quCurSpecSIFR.AsString+'</ItemArticle> ');
            StrSend := StrSend + Trim('                <ItemName>'+quCurSpecNAME.AsString+'</ItemName> ');
            StrSend := StrSend + Trim('                <Price>'+fts(quCurSpecPRICE.AsFloat)+'</Price>            ');
            StrSend := StrSend + Trim('                <Quantity>'+fts(quCurSpecQUANTITY.AsFloat)+'</Quantity>       ');
            StrSend := StrSend + Trim('                <ItemTotal>0.00</ItemTotal>     ');
            StrSend := StrSend + Trim('                <TaxType>4</TaxType>            ');
            StrSend := StrSend + Trim('                <Tax>0.00</Tax>                 ');
            StrSend := StrSend + Trim('                <ItemDiscounts>                 ');
            StrSend := StrSend + Trim('                    <ArmDiscount>               ');
            StrSend := StrSend + Trim('                        <Name>*MALUS*</Name>    ');
            StrSend := StrSend + Trim('                        <Value>'+fts(quCurSpecDSUM.AsFloat)+'</Value>     ');
            StrSend := StrSend + Trim('                    </ArmDiscount>              ');
            StrSend := StrSend + Trim('                </ItemDiscounts>                ');
            StrSend := StrSend + Trim('                <ItemText>                      ');
            StrSend := StrSend + Trim('                </ItemText>                     ');
            StrSend := StrSend + Trim('            </ArmReceiptItem>                   ');

            quCurSpec.Next;
          end;
        finally
          ViewSpec.EndUpdate;
        end;
        StrSend := StrSend + Trim('        </Items>                                ');
        StrSend := StrSend + Trim('        <CustomerAddress>                       ');
        StrSend := StrSend + Trim('        </CustomerAddress>                      ');
        StrSend := StrSend + Trim('        <Total>0.00</Total>                     ');

        if iPayType = 0 then
        begin
          StrSend := StrSend + Trim('        <CashSumm>'+fts(rSumCh)+'</CashSumm>              ');
          StrSend := StrSend + Trim('        <NonCashSumm>0.00</NonCashSumm>         ');
        end else
        begin
          StrSend := StrSend + Trim('        <CashSumm>0</CashSumm>              ');
          StrSend := StrSend + Trim('        <NonCashSumm>'+fts(rSumCh)+'</NonCashSumm>         ');
        end;

        StrSend := StrSend + Trim('        <PosNum>'+its(CommonSet.CashNum)+'</PosNum>              ');
        StrSend := StrSend + Trim('        <PosShiftNum>0</PosShiftNum>   ');
        StrSend := StrSend + Trim('        <PosReceiptNum>0</PosReceiptNum>');
        StrSend := StrSend + Trim('        <ArmReceiptTax>                         ');
        StrSend := StrSend + Trim('            <nds18>0.00</nds18>                 ');
        StrSend := StrSend + Trim('            <nds10>0.00</nds10>                 ');
        StrSend := StrSend + Trim('            <nds00>0.00</nds00>                 ');
        StrSend := StrSend + Trim('            <ndsNo>0.00</ndsNo>                 ');
        StrSend := StrSend + Trim('            <nds18_118>0.00</nds18_118>         ');
        StrSend := StrSend + Trim('            <nds10_110>0.00</nds10_110>         ');
        StrSend := StrSend + Trim('        </ArmReceiptTax>                        ');

        sFoot:='     ������� �� �������      ';
        rSumSd:=iClient/100-rSumCh;
        if rSumSd>=0.01 then
        begin
          Str(rSumSd:8:2,sSd);
          sFoot:='�����:.......................'+sSd+#$0D+sFoot;
        end;

        StrSend := StrSend + Trim('        <Footer>'+sFoot+'</Footer>');
        StrSend := StrSend + Trim('        <FiscalDocNumber>0</FiscalDocNumber> ');
        StrSend := StrSend + Trim('        <FiscalSign>0</FiscalSign>        ');
        StrSend := StrSend + Trim('        <ShiftNumber>0</ShiftNumber>          ');
        StrSend := StrSend + Trim('        <ReceiptNumber>0</ReceiptNumber> ');
        StrSend := StrSend + Trim('    </ArmReceipt>]]>');
        StrSend := StrSend + Trim('</RequestData>  ');
        StrSend := StrSend + Trim('</ArmRequest>  ');

        StrSendU := AnsiToUtf8(StrSend);

        if ClientSock.Active = False then
        begin
          ClientSock.Host := '';
          ClientSock.Address := CommonSet.CashTCP;
          ClientSock.Port := 6667;
          ClientSock.Open;
          Delay(1000);
        end;

        if ClientSock.Active then
        begin
          ClientSock.Socket.SendText(StrSendU);

          for n := 1 to 200 do  //10 ���
          begin
            delay(50);
            if Length(sBuffer) > 0 then break; //�� 50 �� ������ �� ����������...
          end;
          if sBuffer > '' then
          begin
            sRes := sBuffer;
            if Pos(Sid, sRes) > 0 then
            begin
              if Pos('<Result>0</Result>', sRes) > 0 then
              begin
                sMessErrorFis:='';
                result := True;
              end else
              begin
                //<ErrorDescription>����������������� ����� ��������� 24 ����</ErrorDescription>
                if Pos('<ErrorDescription>', sRes)>0 then
                  if Pos('</ErrorDescription>', sRes)>0 then
                  begin
                    sMessErrorFis:=Utf8ToAnsi(Copy(sRes,Pos('<ErrorDescription>', sRes)+18,Pos('</ErrorDescription>', sRes)-(Pos('<ErrorDescription>', sRes)+18)));
                  end;
              end;
            end;
          end;
        end;

        writefislog(StrSendU, sBuffer);
      except
        result := False;
      end;

      iDopl :=iClient-RoundEx(rSumCh*100);
    end;
  end;
end;

function CheckDiscount(rDiscount: Real; var iDisc, iItog: Integer): Boolean;
var
  iDiscount: Integer;
  sDisc: string;
begin
  sDisc := '������';
  iDiscount := RoundEx(rDiscount * 100);
  iDisc := iDiscount;
  iItog := 0;
  if (Commonset.CashNum <= 0) then
  begin
    Result := True;
    exit;
  end;

  Result := True;
  delay(100);
end;

function CheckTotal(var iTotal: Integer): Boolean;
begin
  iTotal := 0;
  if (Commonset.CashNum <= 0) then
  begin
    Result := True;
    exit;
  end;

  Result := True;
  delay(100);
end;

procedure CheckCancel;
begin
  if (Commonset.CashNum <= 0) then
  begin
    exit;
  end;
end;

procedure TestPosCh;
begin
//  if Length(PosCh.Name)>36 then PosCh.Name:=Copy(PosCh.Name,1,36);
//  if Length(PosCh.Code)>19 then PosCh.Code:=Copy(PosCh.Code,1,19);
//  if Length(PosCh.AddName)>250 then PosCh.AddName:=Copy(PosCh.AddName,1,250);

  if Length(PosCh.Name) > 32 then
    PosCh.Name := Copy(PosCh.Name, 1, 32) + ' ';
  if Length(PosCh.Code) > 5 then
    PosCh.Code := Copy(PosCh.Code, 1, 5);
end;

function CheckAddPos(var iSum: Integer): Boolean;
begin
  iSum := RoundEx(rv(PosCh.Price) * RoundEx(PosCh.Count * 1000) / 1000 * 100);
  TestPosCh;

  if (Commonset.CashNum <= 0) then
  begin
    Result := True;
    exit;
  end;

  Result := True;
  delay(100);
end;

function CheckStart: Boolean;
//Var StrWk:String;
begin
  if (Commonset.CashNum <= 0) then
  begin
    Result := True;
    exit;
  end;
  Result := True;
end;

function CheckRetStart: Boolean;
begin
  if (Commonset.CashNum <= 0) then
  begin
    Result := True;
    exit;
  end;
  Result := True;
end;

function ZClose: Boolean;
var
  StrSend: string;
  StrSendU: UTF8String;
  sRes, Sid: string;
  n: Integer;
  a: TGUID;
begin
  if (Commonset.CashNum <= 0) then
  begin
    Result := True;
    exit;
  end;

  //������� �����
  with fmMainFF do
  begin
    result := False;
    try
      prClearBuf;

      CreateGUID(a);
      Sid := GUIDTostring(a);

      StrSend := '';

      StrSend := StrSend + Trim('<?xml version="1.0" encoding="UTF-8"?>');
      StrSend := StrSend + Trim('<ArmRequest>');
      StrSend := StrSend + Trim('    <RequestBody>');
      StrSend := StrSend + Trim('        <ProtocolLabel>OFDFNARMUKM</ProtocolLabel> ');
      StrSend := StrSend + Trim('        <ProtocolVersion>10.1</ProtocolVersion> ');
      StrSend := StrSend + Trim('        <RequestId>' + Sid + '</RequestId> ');
      StrSend := StrSend + Trim('        <DateTime>' + curdt + '</DateTime> ');
      StrSend := StrSend + Trim('        <Command>42</Command> ');
      StrSend := StrSend + Trim('    </RequestBody>');
      StrSend := StrSend + Trim('    <RequestData> ');
      StrSend := StrSend + Trim('        <![CDATA[<ArmShiftClose> ');
      StrSend := StrSend + Trim('        <FiscalDocNumber>0</FiscalDocNumber> ');
      StrSend := StrSend + Trim('        <FiscalSign>0</FiscalSign> ');
      StrSend := StrSend + Trim('        <ShiftNumber>0</ShiftNumber> ');
      StrSend := StrSend + Trim('        <OFDQueueLegth>0</OFDQueueLegth>');
      StrSend := StrSend + Trim('        <OFDfirstQueueDocDateTime> ');
      StrSend := StrSend + Trim('        </OFDfirstQueueDocDateTime> ');
      StrSend := StrSend + Trim('        <ReceiptsCount>0</ReceiptsCount> ');
      StrSend := StrSend + Trim('        <FiscalDocCount>0</FiscalDocCount>');
      StrSend := StrSend + Trim('        <OFDLongWait>0</OFDLongWait> ');
      StrSend := StrSend + Trim('        <FNNeedChange>1</FNNeedChange> ');
      StrSend := StrSend + Trim('        <FNOwerflow>1</FNOwerflow>');
      StrSend := StrSend + Trim('        <FNExhausted>1</FNExhausted> ');
      StrSend := StrSend + Trim('        <UserLoginName>' + Person.Name + '</UserLoginName>');
      StrSend := StrSend + Trim('        <DateTime>' + curdt + '</DateTime> ');
      StrSend := StrSend + Trim('        <HeaderReport>');
      StrSend := StrSend + Trim('            ����� � ' + its(CommonSet.CashNum));
      StrSend := StrSend + Trim('        </HeaderReport>');
      StrSend := StrSend + Trim('        <FooterReport></FooterReport>');
      StrSend := StrSend + Trim('    </ArmShiftClose>]]> ');
      StrSend := StrSend + Trim('</RequestData> ');
      StrSend := StrSend + Trim('</ArmRequest>');

      StrSendU := AnsiToUtf8(StrSend);

      if ClientSock.Active = False then
      begin
        ClientSock.Host := '';
        ClientSock.Address := CommonSet.CashTCP;
        ClientSock.Port := 6667;
        ClientSock.Open;
        Delay(1000);
      end;

      if ClientSock.Active then
      begin
        ClientSock.Socket.SendText(StrSendU);

        for n := 1 to 600 do
        begin
          delay(50);
          if Length(sBuffer) > 0 then
            break; //�� 50 �� ������ �� ����������...
        end;
        if sBuffer > '' then
        begin
          sRes := sBuffer;
          if Pos(Sid, sRes) > 0 then
          begin
            if Pos('<Result>0</Result>', sRes) > 0 then
            begin
              sMessErrorFis:='';
              result := True;
            end else
            begin
              //<ErrorDescription>����������������� ����� ��������� 24 ����</ErrorDescription>
              if Pos('<ErrorDescription>', sRes)>0 then
                if Pos('</ErrorDescription>', sRes)>0 then
                begin
                  sMessErrorFis:=Utf8ToAnsi(Copy(sRes,Pos('<ErrorDescription>', sRes)+18,Pos('</ErrorDescription>', sRes)-(Pos('<ErrorDescription>', sRes)+18)));
                end;
            end;
          end;
        end;
      end;

      writefislog(StrSendU, sBuffer);
    except
      result := False;
    end;
  end;
end;

function ZOpen: Boolean;
var
  StrSend: string;
  StrSendU: UTF8String;
  sRes, Sid: string;
  n: Integer;
  a: TGUID;
  sZ:string;
begin
  if (Commonset.CashNum <= 0) then
  begin
    Result := True;
    exit;
  end;

  //������� �����

  with fmMainFF do
  begin
    result := False;
    try
      prClearBuf;

      CreateGUID(a);
      Sid := GUIDTostring(a);

      StrSend := '';
      StrSend := StrSend + Trim('<?xml version="1.0" encoding="UTF-8"?>');
      StrSend := StrSend + Trim('<ArmRequest>');
      StrSend := StrSend + Trim('    <RequestBody>');
      StrSend := StrSend + Trim('        <ProtocolLabel>OFDFNARMUKM</ProtocolLabel>');
      StrSend := StrSend + Trim('        <ProtocolVersion>10.1</ProtocolVersion>');
      StrSend := StrSend + Trim('        <RequestId>' + Sid + '</RequestId>');
      StrSend := StrSend + Trim('        <DateTime>' + curdt + '</DateTime>');
      StrSend := StrSend + Trim('        <Command>4</Command>');
      StrSend := StrSend + Trim('    </RequestBody>');
      StrSend := StrSend + Trim('    <RequestData><![CDATA[');
      StrSend := StrSend + Trim('        <ArmShiftOpen>');
      StrSend := StrSend + Trim('        <FiscalDocNumber>100011</FiscalDocNumber>');
      StrSend := StrSend + Trim('        <FiscalSign></FiscalSign>');
      StrSend := StrSend + Trim('        <ShiftNumber>5</ShiftNumber>');
      StrSend := StrSend + Trim('        <UserLoginName>' + Person.Name + '</UserLoginName>');
      StrSend := StrSend + Trim('        <DateTime>' + curdt + '</DateTime>');
      StrSend := StrSend + Trim('    </ArmShiftOpen>]]>');
      StrSend := StrSend + Trim('</RequestData>');
      StrSend := StrSend + Trim('</ArmRequest>');

      StrSendU := AnsiToUtf8(StrSend);

      if ClientSock.Active = False then
      begin
        ClientSock.Host := '';
        ClientSock.Address := CommonSet.CashTCP;
        ClientSock.Port := 6667;
        ClientSock.Open;
        Delay(1000);
      end;

      if ClientSock.Active then
      begin
        ClientSock.Socket.SendText(StrSendU);

        for n := 1 to 600 do
        begin
          delay(50);
          if Length(sBuffer) > 0 then
            break; //�� 50 �� ������ �� ����������...
        end;
        if sBuffer > '' then
        begin
          sRes := sBuffer;
          if Pos(Sid, sRes) > 0 then
          begin
            if Pos('<Result>0</Result>', sRes) > 0 then
            begin
              sMessErrorFis:='';
              result := True;

              //<ShiftNumber>12</ShiftNumber>
              if Pos('<ShiftNumber>', sRes)>0 then
                if Pos('</ShiftNumber>', sRes)>0 then
                begin
                  sZ:=Utf8ToAnsi(Copy(sRes,Pos('<ShiftNumber>', sRes)+13,Pos('</ShiftNumber>', sRes)-(Pos('<ShiftNumber>', sRes)+13)));
                  Nums.ZNum:=StrToIntDef(sZ,CommonSet.CashZ);
                  if Nums.ZNum<>CommonSet.CashZ then CommonSet.CashZ:=Nums.ZNum;

                  CommonSet.CashChNum:=0;
                  Nums.iCheckNum:=0;
                  CommonSet.PreCheckNum:=1;
                  if CommonSet.IncNumTab>0 then CommonSet.IncNumTab:=1;//��������� ������� ������� ������
                  WriteCheckNum;
                  Label4.Caption:='����� �'+IntToStr(CommonSet.CashZ)+'  ��� � '+IntToStr(CommonSet.CashChNum);
                end;
            end else
            begin
              //<ErrorDescription>����������������� ����� ��������� 24 ����</ErrorDescription>
              if Pos('<ErrorDescription>', sRes)>0 then
                if Pos('</ErrorDescription>', sRes)>0 then
                begin
                  sMessErrorFis:=Utf8ToAnsi(Copy(sRes,Pos('<ErrorDescription>', sRes)+18,Pos('</ErrorDescription>', sRes)-(Pos('<ErrorDescription>', sRes)+18)));
                end;
            end;
          end;
        end;
      end;

      writefislog(StrSendU, sBuffer);
    except
      result := False;
    end;
  end;
end;

function GetX: Boolean;
var
  StrSend: string;
  StrSendU: UTF8String;
  sRes, Sid: string;
  n: Integer;
  a: TGUID;
begin
  if (Commonset.CashNum <= 0) then
  begin
    Result := True;
    exit;
  end;

  //�������� X �����
  with fmMainFF do
  begin
    result := False;
    try
      prClearBuf;

      CreateGUID(a);
      Sid := GUIDTostring(a);

      StrSend := '';
      StrSend := StrSend + Trim('<?xml version="1.0" encoding="UTF-8"?>');
      StrSend := StrSend + Trim('<ArmRequest>');
      StrSend := StrSend + Trim('    <RequestBody>');
      StrSend := StrSend + Trim('        <ProtocolLabel>OFDFNARMUKM</ProtocolLabel>');
      StrSend := StrSend + Trim('        <ProtocolVersion>10.1</ProtocolVersion>');
      StrSend := StrSend + Trim('        <RequestId>' + Sid + '</RequestId>');
      StrSend := StrSend + Trim('        <DateTime>' + curdt + '</DateTime>');
      StrSend := StrSend + Trim('        <Command>46</Command>');
      StrSend := StrSend + Trim('    </RequestBody>');
      StrSend := StrSend + Trim('    <RequestData><![CDATA[');
      StrSend := StrSend + Trim('        <ArmCXReport>');
      StrSend := StrSend + Trim('        <UserLoginName>' + Person.Name + '</UserLoginName>');
      StrSend := StrSend + Trim('        <DateTime>' + curdt + '</DateTime>');
      StrSend := StrSend + Trim('        <HeaderReport>');
      StrSend := StrSend + Trim('            ����� � ' + its(CommonSet.CashNum));
      StrSend := StrSend + Trim('        </HeaderReport>');
      StrSend := StrSend + Trim('        <FooterReport></FooterReport>');
      StrSend := StrSend + Trim('    </ArmCXReport>]]>');
      StrSend := StrSend + Trim('</RequestData>');
      StrSend := StrSend + Trim('</ArmRequest>');

      StrSendU := AnsiToUtf8(StrSend);

      if ClientSock.Active = False then
      begin
        ClientSock.Host := '';
        ClientSock.Address := CommonSet.CashTCP;
        ClientSock.Port := 6667;
        ClientSock.Open;
        Delay(1000);
      end;

      if ClientSock.Active then
      begin
        ClientSock.Socket.SendText(StrSendU);

        for n := 1 to 600 do
        begin
          delay(50);
          if Length(sBuffer) > 0 then
            break; //�� 50 �� ������ �� ����������...
        end;
        if sBuffer > '' then
        begin
          sRes := sBuffer;
          if Pos(Sid, sRes) > 0 then
          begin
            if Pos('<Result>0</Result>', sRes) > 0 then
            begin
              sMessErrorFis:='';
              result := True;
            end else
            begin
              //<ErrorDescription>����������������� ����� ��������� 24 ����</ErrorDescription>
              if Pos('<ErrorDescription>', sRes)>0 then
                if Pos('</ErrorDescription>', sRes)>0 then
                begin
                  sMessErrorFis:=Utf8ToAnsi(Copy(sRes,Pos('<ErrorDescription>', sRes)+18,Pos('</ErrorDescription>', sRes)-(Pos('<ErrorDescription>', sRes)+18)));
                end;
            end;
          end;
        end;
      end;

      writefislog(StrSendU, sBuffer);
    except
      result := False;
    end;
  end;
end;

function CashDate(var sDate: string): Boolean;
begin
//
  if (Commonset.CashNum <= 0) then
  begin
    Result := True;
    exit;
  end;
  sDate:=FormatDateTime('dd.mm.yyyy',date);
  Result := true;
end;

function CashClose: Boolean;
begin
  if (Commonset.CashNum <= 0) then
  begin
    Result := True;
    exit;
  end;
  result := True;
end;

function CashOpen(sPort: PChar): Boolean;
var
  StrSend: string;
  StrSendU: UTF8String;
  sRes, Sid: string;
  n: Integer;
  a: TGUID;
begin
  if (Commonset.CashNum <= 0) then
  begin
    Result := True;
    exit;
  end;

  CommonSet.TypeFis := 'sp802';

  //��������� ����� �� �����
  with fmMainFF do
  begin
    result := False;
    try
      prClearBuf;

      CreateGUID(a);
      Sid := GUIDTostring(a);

      StrSend := '';
      StrSend := StrSend + Trim('<?xml version="1.0" encoding="UTF-8"?>');
      StrSend := StrSend + Trim('<ArmRequest>');
      StrSend := StrSend + Trim('    <RequestBody>');
      StrSend := StrSend + Trim('        <ProtocolLabel>OFDFNARMUKM</ProtocolLabel>');
      StrSend := StrSend + Trim('        <ProtocolVersion>1</ProtocolVersion>');
      StrSend := StrSend + Trim('        <RequestId>' + Sid + '</RequestId>');
      StrSend := StrSend + Trim('        <DateTime>' + curdt + '</DateTime>');
      StrSend := StrSend + Trim('        <Command>2</Command>');
      StrSend := StrSend + Trim('    </RequestBody>');
      StrSend := StrSend + Trim('    <RequestData><![CDATA[]]>');
      StrSend := StrSend + Trim('    </RequestData>');
      StrSend := StrSend + Trim('</ArmRequest>');

      StrSendU := AnsiToUtf8(StrSend);

      if ClientSock.Active = False then
      begin
        ClientSock.Host := '';
        ClientSock.Address := CommonSet.CashTCP;
        ClientSock.Port := 6667;
        ClientSock.Open;
        Delay(1000);
      end;

      if ClientSock.Active then
      begin
        ClientSock.Socket.SendText(StrSendU);

        for n := 1 to 600 do
        begin
          delay(50);
          if Length(sBuffer) > 0 then
            break; //�� 50 �� ������ �� ����������...
        end;
        if sBuffer > '' then
        begin
          sRes := sBuffer;
          if Pos(Sid, sRes) > 0 then
          begin
            if Pos('<Result>0</Result>', sRes) > 0 then
            begin
              sMessErrorFis:='';
              result := True;
            end else
            begin
              //<ErrorDescription>����������������� ����� ��������� 24 ����</ErrorDescription>
              if Pos('<ErrorDescription>', sRes)>0 then
                if Pos('</ErrorDescription>', sRes)>0 then
                begin
                  sMessErrorFis:=Utf8ToAnsi(Copy(sRes,Pos('<ErrorDescription>', sRes)+18,Pos('</ErrorDescription>', sRes)-(Pos('<ErrorDescription>', sRes)+18)));
                end;
            end;
          end;
        end;
      end;

      writefislog(StrSend, sBuffer);
    except
      result := False;
    end;
  end;
end;

function GetNums: Boolean;
begin
  if (Commonset.CashNum <= 0) then
  begin
    Result := True;
    exit;
  end;
  Result := True;
end;

function GetRes: Boolean;
begin
  Result := True;
end;

function GetSerial: Boolean;
begin
  if (Commonset.CashNum <= 0) then
  begin
    Result := True;
    exit;
  end;
  result := True;
end;

function OpenZ: Boolean;
begin
  if (Commonset.CashNum <= 0) then
  begin
    Result := True;
    exit;
  end;
  result := True;
end;

function InspectSt(var sMess: string): Integer;
begin
  sMess := '��� ��.';
  if (Commonset.CashNum <= 0) then
  begin
    Result := 0;
    exit;
  end;
  iRfr := 0;
  sMessErrorFis:='';

  result := iRfr;
end;

function CheckOpenFis: Boolean;
//Var iSt:INteger;
begin
  if (Commonset.CashNum <= 0) then
  begin
    Result := True;
    exit;
  end;
  result := True;
end;

end.

