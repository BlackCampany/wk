unit messagecash;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, Placemnt, cxStyles, cxCustomData, cxGraphics,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, ActnList,
  XPStyleActnCtrls, ActnMan, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxControls,
  cxGridCustomView, cxGrid, SpeedBar, ExtCtrls;

type
  TfmMessage = class(TForm)
    StatusBar1: TStatusBar;
    FormPlacement1: TFormPlacement;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    GridMessage: TcxGrid;
    ViewMessage: TcxGridDBTableView;
    LevelMessage: TcxGridLevel;
    am3: TActionManager;
    acAdd: TAction;
    acEdit: TAction;
    acDel: TAction;
    acExit: TAction;
    Timer1: TTimer;
    ViewMessageID: TcxGridDBColumn;
    ViewMessageMESSAG: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure acAddExecute(Sender: TObject);
    procedure acEditExecute(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmMessage: TfmMessage;
  bClear3: Boolean;

implementation

uses Un1, dmRnEdit, AddCateg;

{$R *.dfm}

procedure TfmMessage.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  GridMessage.Align:=AlClient;
  ViewMessage.RestoreFromIniFile(CurDir+GridIni);
  delay(10);
end;

procedure TfmMessage.acExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmMessage.acAddExecute(Sender: TObject);
Var Id:Integer;
begin
  //���������� ���������
  if not CanDo('prAddMessage') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  with dmC do
  begin
    fmAddCateg:=TFmAddCateg.create(Application);
    fmAddCateg.Caption:='���������� ���������.';
    fmAddCateg.cxTextEdit1.Text:='';
    fmAddCateg.ShowModal;
    if fmAddCateg.ModalResult=mrOk then
    begin
      try
        Id:=1;
        if taMessage.RecordCount>0 then
        begin
          taMessage.Last;
          Id:=taMESSAGEID.AsInteger+1;
        end;

        taMESSAGE.Append;
        taMESSAGEID.AsInteger:=Id;
        taMESSAGEMESSAG.AsString:=Copy(fmAddCateg.cxTextEdit1.Text,1,50);
        taMESSAGE.Post;

      except
      end;
    end;
    fmAddCateg.Release;
  end;
end;

procedure TfmMessage.acEditExecute(Sender: TObject);
begin
  //�������������� ���������
  if not CanDo('prEditMessage') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  with dmC do
  begin
    if taMessage.RecordCount>0 then
    begin
      fmAddCateg:=TFmAddCateg.create(Application);
      fmAddCateg.Caption:='�������������� ���������.';
      fmAddCateg.cxTextEdit1.Text:=taMESSAGEMESSAG.AsString;
      fmAddCateg.ShowModal;
      if fmAddCateg.ModalResult=mrOk then
      begin
        try
          taMESSAGE.Edit;
          taMESSAGEMESSAG.AsString:=Copy(fmAddCateg.cxTextEdit1.Text,1,50);
          taMESSAGE.Post;
        except
        end;
      end;
      fmAddCateg.Release;
    end;
  end;
end;

procedure TfmMessage.Timer1Timer(Sender: TObject);
begin
  if bClear3=True then begin StatusBar1.Panels[0].Text:=''; bClear3:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClear3:=True;
end;

procedure TfmMessage.acDelExecute(Sender: TObject);
begin
  //������� ���������
  if not CanDo('prDelMessage') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  with dmC do
  begin
    if taMessage.RecordCount>0 then
    begin
      if MessageDlg('�� ������������� ������ ������� ��������� - '+taMESSAGEMESSAG.AsString+' ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        try
          taMessage.Delete;
        except
        end;
      end;  
    end;
  end;
end;

procedure TfmMessage.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewMessage.StoreToIniFile(CurDir+GridIni,False);
end;

end.
