unit MainRnConv;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, RXShell, Menus, ImgList, ExtCtrls, DB, DBTables, FileUtil;

type
  TfmMainConvRn = class(TForm)
    RxTray: TRxTrayIcon;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Timer1: TTimer;
    N2: TMenuItem;
    N3: TMenuItem;
    procedure N3Click(Sender: TObject);
    procedure RxTrayClick(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure N1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure prTransp;
  end;

procedure WrMess(StrOut:String);
procedure FindSortFiles(FList:TStrings);

var
  fmMainConvRn: TfmMainConvRn;

implementation

uses HistoryTr, Un1, Dm, u2fdk;

{$R *.dfm}

procedure FindSortFiles(FList:TStrings);
Var SR:TSearchRec; // ��������� ����������
    FindRes:Integer; // ���������� ��� ������ ���������� ������
begin
  FList.Clear; // ������� ���������� ListBox1 ����� ���������� � ���� ������ ������
  FindRes:=FindFirst(CommonSet.PathImport+'*.off',32 ,SR);  //32 faArhive
  While FindRes=0 do // ���� �� ������� ����� (��������), �� ��������� ����
  begin
    FList.Add(SR.Name); // ���������� � ������ �������� ���������� ��������
    FindRes:=FindNext(SR); // ����������� ������ �� �������� ��������
  end;
  FindClose(SR); // ��������� �����
    //���������

end;


Function ImportFile(fName:String;Var iOk,iNon:Integer):Boolean;
Var f:TextFile;
    StrWk,StrWk1:String;
    Par:Variant;
begin
  with dmC do
  begin
    result:=True;
    iOk:=0;
    iNon:=0;

    CasherRnDb.Connected:=False;
    CasherRnDb.DBName:=DBName;
    try
      CasherRnDb.Connected:=True;
    except
      result:=False;
      exit;
    end;

    taClassif.Active:=False;
    taCateg.Active:=False;
    taMenu.Active:=False;
    taModify.Active:=False;

    taClassif.Active:=True;
    taCateg.Active:=True;
    taMenu.Active:=True;
    taModify.Active:=True;

//    trUpdate.StartTransaction;

    try
      assignfile(F,CommonSet.PathImport+fName);
      reset(F);
      while not EOF(F) do
      begin
        ReadLn(F,StrWk);
        if StrWk>'' then
        begin
          StrWk1:=Copy(StrWk,1,Pos(r,StrWk)-1);
          if StrWk1 = 'Classif' then
          begin
            if StrToClassif(StrWk,ClassifRec) then
            begin
              inc(iOk);
              trUpDate.StartTransaction;
              par := VarArrayCreate([0,1], varInteger);
              par[0]:=ClassifRec.TYPE_CLASSIF;
              par[1]:=ClassifRec.ID;
              if taClassif.Locate('TYPE_CLASSIF;ID',par,[]) then
                taClassif.Edit
              else
                taClassif.Append;

              taClassifTYPE_CLASSIF.AsInteger:=ClassifRec.TYPE_CLASSIF;
              taClassifIACTIVE.AsInteger:=1;
              taClassifID.AsInteger:=ClassifRec.ID;
              taClassifID_PARENT.AsInteger:=ClassifRec.Id_Parent;
              taClassifNAME.AsString:=OemToAnsiConvert(ClassifRec.Name);
              taClassifIEDIT.AsInteger:=1;
              taClassif.Post;
              trUpDate.Commit;
              delay(10);

            end else inc(iNon);
          end;
          if StrWk1 = 'Category' then
          begin
            if StrToCateg(StrWk,CategRec) then
            begin
              inc(iOk);
              trUpDate.StartTransaction;
              if taCateg.Locate('SIFR',CategRec.Id,[]) then
              begin
                taCateg.Edit;
              end else taCateg.Append;
              taCategSIFR.AsInteger:=CategRec.Id;
              taCategName.AsString:=OemToAnsiConvert(CategRec.Name);
              taCategTAX_GROUP.AsInteger:=CategRec.Tax_Group;
              taCategIACTIVE.AsInteger:=1;
              taCategIEDIT.AsInteger:=1;
              taCateg.Post;
              trUpDate.Commit;
              delay(10);
            end else inc(iNon);
          end;
          if StrWk1 = 'Menu' then
          begin
            if StrToMenu(StrWk,MenuRec) then
            begin
              inc(iOk);
              trUpDate.StartTransaction;
              if taMenu.locate('SIFR',MenuRec.Sifr,[]) then taMenu.Edit
              else taMenu.Append;
              taMenuSIFR.AsInteger:=MenuRec.Sifr;
              taMenuNAME.AsString:=OemToAnsiConvert(MenuRec.Name);
              taMenuPRICE.AsFloat:=MenuRec.Price;
//              while MenuRec.Code[1]='0' do delete(MenuRec.Code,1,1);
              taMenuCODE.AsString:=MenuRec.Code;
              taMenuTREETYPE.AsString:=MenuRec.TreeType;
              taMenuLIMITPRICE.AsFloat:=MenuRec.LimitPrice;
              taMenuCATEG.AsInteger:=MenuRec.Categ;
              taMenuPARENT.AsInteger:=MenuRec.Parent;
              taMenuLINK.AsInteger:=MenuRec.Link;
              taMenuSTREAM.AsInteger:=MenuRec.Stream;
              taMenuLACK.AsInteger:=Menurec.Lack;
              taMenuDESIGNSIFR.AsInteger:=MenuRec.DesignSifr;
              taMenuALTNAME.AsString:=OemToAnsiConvert(MenuRec.AltName);
              taMenuNALOG.AsFloat:=MenuRec.Nalog;
              taMenuBARCODE.AsString:=MenuRec.Barcode;
              taMenuIMAGE.AsInteger:=MenuRec.Image;
              taMenuCONSUMMA.AsFloat:=MenuRec.Consumma;
              taMenuMINREST.AsInteger:=MenuRec.MinRest;
              taMenuPRNREST.AsInteger:=MenuRec.PrnRest;
              taMenuCOOKTIME.AsInteger:=MenuRec.CookTime;
              taMenuDISPENSER.AsInteger:=MenuRec.Dispenser;
              taMenuDISPKOEF.AsInteger:=MenuRec.DispKoef;
              taMenuACCESS.AsInteger:=MenuRec.Access;
              taMenuFLAGS.AsInteger:=MenuRec.Flags;
              taMenuTARA.AsInteger:=MenuRec.Tara;
              taMenuCNTPRICE.AsInteger:=MenuRec.CntPrice;
              taMenuBACKBGR.AsFloat:=MenuRec.BackBGR;
              taMenuFONTBGR.AsFloat:=MenuRec.FontBGR;
              taMenuIACTIVE.AsInteger:=1;
              taMenuIEDIT.AsInteger:=1;
              taMenu.Post;
              trUpDate.Commit;
              delay(10);
              if MenuRec.TreeType='T' then
              begin  //��� ����� ����
                trUpDate.StartTransaction;
                par := VarArrayCreate([0,1], varInteger);
                par[0]:=2;
                par[1]:=MenuRec.Sifr+kClassif;
                if taClassif.Locate('TYPE_CLASSIF;ID',par,[]) then taClassif.Edit
                else taClassif.Append;

                taClassifTYPE_CLASSIF.AsInteger:=2;
                taClassifIACTIVE.AsInteger:=1;
                taClassifID.AsInteger:=MenuRec.Sifr+kClassif;
                if MenuRec.Parent >0 then taClassifID_PARENT.AsInteger:=MenuRec.Parent+kClassif
                else taClassifID_PARENT.AsInteger:=0;
                taClassifNAME.AsString:=OemToAnsiConvert(MenuRec.Name);
                taClassifIEDIT.AsInteger:=1;
                taClassif.Post;
                trUpDate.Commit;
                delay(10);
              end;
            end else inc(iNon);
          end;
          if StrWk1 = 'Modify' then
          begin
            if StrToMod(StrWk,ModRec) then
            begin
              inc(iOk);
              trUpDate.StartTransaction;

              if taModify.locate('SIFR',ModRec.Id,[]) then taModify.Edit
              else taModify.Append;
              taModifySIFR.AsInteger:=ModRec.Id;
              taModifyNAME.AsString:=OemToAnsiConvert(ModRec.Name);
              taModifyPARENT.AsInteger:=Modrec.Parent;
              taModifyPRICE.AsFloat:=ModRec.Price;
              taModifyREALPRICE.AsInteger:=ModRec.RealPrice;
              taModifyIACTIVE.AsInteger:=1;
              taModifyIEDIT.AsInteger:=1;
              taModify.Post;
              trUpDate.Commit;
              delay(10);
              if ModRec.Parent=0 then
              begin  //��� ����� ����
                trUpDate.StartTransaction;
                par := VarArrayCreate([0,1], varInteger);
                par[0]:=3;
                par[1]:=ModRec.Id+kClassif*2;
                if taClassif.Locate('TYPE_CLASSIF;ID',par,[]) then taClassif.Edit
                else taClassif.Append;

                taClassifTYPE_CLASSIF.AsInteger:=3;
                taClassifIACTIVE.AsInteger:=1;
                taClassifID.AsInteger:=ModRec.Id+kClassif*2;
                taClassifID_PARENT.AsInteger:=0;
                taClassifNAME.AsString:=OemToAnsiConvert(ModRec.Name);
                taClassifIEDIT.AsInteger:=1;
                taClassif.Post;
                trUpDate.Commit;
                delay(10);
              end;
            end else inc(iNon);
          end;
        end;
      end;
      CloseFile(F);
      delay(10);
      if FileExists(CommonSet.PathArh+fName) then DeleteFiles(CommonSet.PathArh+fName);
      delay(10);
      MoveFile(CommonSet.PathImport+fName, CommonSet.PathArh+fName);
    except
      Result:=False;

      CloseFile(F);

      taClassif.Active:=False;
      taCateg.Active:=False;
      taMenu.Active:=False;
      taModify.Active:=False;

      CasherRnDb.Connected:=False;
    end;

    if result=True then
    begin
      prAfterImportRk.ExecProc;
    end;
    taClassif.Active:=False;
    taCateg.Active:=False;
    taMenu.Active:=False;
    taModify.Active:=False;
    CasherRnDb.Connected:=False;
  end;
end;

procedure TfmMainConvRn.prTransp;
Var StrWk, StrF:String;
    FList:TStrings;
    i,iOk,iNon:Integer;
begin
  if Fileexists(CommonSet.PathImport+FileInd) then
  begin //���� start ��������� - ������ ������������
    FList := TStringList.Create;
    try
      FindSortFiles(FList);  //�������� ��������������� ������ ������ (����� ������������)
      for i:=0 to FList.Count-1 do
      begin
        strwk:=FList.Strings[i];
        WrMess('  ��������� ������: "'+StrWk+'"');
        if ImportFile(StrWk,iOk,iNon) then
        begin
          WrMess('    ������� ������� - '+IntToStr(iOk)+' �����.');
          WrMess('    �� ������� - '+IntToStr(iNon)+' �����.');
          WrMess('  ������ ��.');
        end
        else WrMess('  ������ ��� ������ ������.');
        delay(10);
      end;
      fList.Free;
    except
      fList.Free;
      WrMess('������ �������������� ����� - '+StrF);
    end;
  end;
end;

procedure WrMess(StrOut:String);
Var StrWk:String;
begin
  with fmHistoryTr do
  begin
    if Memo1.Lines.Count > 500 then Memo1.Clear;
//  WriteHistory(S);
    if StrOut>'' then StrWk:=FormatDateTime('dd.mm hh:nn:ss',now)+'  '+StrOut
    else StrWk:=StrOut;
    Memo1.Lines.Add(StrWk);
  end;
end;

procedure TfmMainConvRn.N3Click(Sender: TObject);
begin
  close;
end;

procedure TfmMainConvRn.RxTrayClick(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
//  PopUpMenu1.Popup(Screen.DesktopRect.Right-100,Screen.DesktopRect.Bottom-80);
  fmHistoryTr.Show;
end;

procedure TfmMainConvRn.N1Click(Sender: TObject);
begin
  fmHistoryTr.Show;
end;

procedure TfmMainConvRn.FormCreate(Sender: TObject);
begin
  CurDir := ExtractFilePath(ParamStr(0));
  ReadIni;
  TestDir;
end;

procedure TfmMainConvRn.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled:=False;
  prTransp;
  Timer1.Enabled:=True;
end;

end.
