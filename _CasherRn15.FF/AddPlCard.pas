unit AddPlCard;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons, ExtCtrls,
  ComCtrls, dxfBackGround, cxMaskEdit, cxDropDownEdit, cxCalc, cxCheckBox,
  cxControls, cxContainer, cxEdit, cxTextEdit, cxGraphics, cxLookupEdit,
  cxDBLookupEdit, cxDBLookupComboBox;

type
  TfmAddPlCard = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Label1: TLabel;
    cxTextEdit1: TcxTextEdit;
    Label2: TLabel;
    cxTextEdit2: TcxTextEdit;
    CheckBox1: TcxCheckBox;
    Label3: TLabel;
    dxfBackGround1: TdxfBackGround;
    cxLookupComboBox1: TcxLookupComboBox;
    procedure cxButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAddPlCard: TfmAddPlCard;

implementation

uses DmRnDisc;

{$R *.dfm}

procedure TfmAddPlCard.cxButton2Click(Sender: TObject);
begin
  Close;
end;

end.
