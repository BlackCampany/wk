object fmShutd: TfmShutd
  Left = 495
  Top = 251
  Width = 296
  Height = 345
  Caption = #1042#1099#1082#1083#1102#1095#1077#1085#1080#1077' ( '#1055#1077#1088#1077#1079#1072#1075#1088#1091#1079#1082#1072' )'
  Color = 8080936
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 8
    Top = 4
    Width = 277
    Height = 301
    BevelInner = bvLowered
    Color = 6832674
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 16
      Top = 16
      Width = 245
      Height = 77
      Caption = #1042#1099#1082#1083#1102#1095#1080#1090#1100
      Default = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -21
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ModalResult = 1
      ParentFont = False
      TabOrder = 0
      TabStop = False
      Colors.Default = 14930628
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      Layout = blGlyphTop
      LookAndFeel.Kind = lfFlat
    end
    object cxButton2: TcxButton
      Left = 16
      Top = 108
      Width = 245
      Height = 77
      Caption = #1055#1077#1088#1077#1079#1072#1075#1088#1091#1079#1080#1090#1100
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -21
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ModalResult = 4
      ParentFont = False
      TabOrder = 1
      TabStop = False
      Colors.Default = 14930628
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      Layout = blGlyphTop
      LookAndFeel.Kind = lfFlat
    end
    object cxButton3: TcxButton
      Left = 16
      Top = 200
      Width = 245
      Height = 77
      Caption = #1054#1090#1084#1077#1085#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -21
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ModalResult = 2
      ParentFont = False
      TabOrder = 2
      TabStop = False
      Colors.Default = 14930628
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      Layout = blGlyphTop
      LookAndFeel.Kind = lfFlat
    end
  end
end
