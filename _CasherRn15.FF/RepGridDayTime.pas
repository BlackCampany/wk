unit RepGridDayTime;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ComCtrls, dxfProgressBar, SpeedBar, Placemnt,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, FR_Class;

type
  TfmRDTime = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    ProgressBar1: TdxfProgressBar;
    FormPlacement1: TFormPlacement;
    SpeedBar1: TSpeedBar;
    ViewRDT: TcxGridDBTableView;
    LevelRDT: TcxGridLevel;
    GridRDT: TcxGrid;
    ViewRDTSifr: TcxGridDBColumn;
    ViewRDTSUMQUANT: TcxGridDBColumn;
    ViewRDTSUMSUM: TcxGridDBColumn;
    ViewRDTSUMDISC: TcxGridDBColumn;
    ViewRDTNAME: TcxGridDBColumn;
    ViewRDTPARENT: TcxGridDBColumn;
    ViewRDTNAMEGR: TcxGridDBColumn;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    frRepRDT: TfrReport;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SpeedItem3Click(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmRDTime: TfmRDTime;

implementation

uses Un1, MainRep, Dm, Period2;

{$R *.dfm}

procedure TfmRDTime.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  ViewRDT.RestoreFromIniFile(CurDir+GridIni);
end;

procedure TfmRDTime.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewRDT.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmRDTime.SpeedItem3Click(Sender: TObject);
begin
  Close;
end;

procedure TfmRDTime.SpeedItem2Click(Sender: TObject);
Var DateBeg,DateEnd:TDateTime;
    iDateb,iDateE,iDateCur:Integer;
    rProc,rQ,rS,rD:Real;
begin
  fmPeriod2:=TfmPeriod2.Create(Application);
  fmPeriod2.DateTimePicker1.Date:=TrebSel.DateFrom;
  fmPeriod2.DateTimePicker2.Date:=TrebSel.DateTo;
//  fmPeriod2.DateTimePicker3.Visible:=True;
//  fmPeriod2.DateTimePicker4.Visible:=True;
  fmPeriod2.DateTimePicker3.Time:=TrebSel.TimeFrom;
  fmPeriod2.DateTimePicker4.Time:=TrebSel.TimeTo;

  fmPeriod2.ShowModal;
  if fmPeriod2.ModalResult=mrOk then
  begin
    Datebeg:=Trunc(fmPeriod2.DateTimePicker1.Date);
    DateEnd:=Trunc(fmPeriod2.DateTimePicker2.Date);

    with dmC do
    begin
      fmMainRep.taRDT.Active:=False;
      fmMainRep.taRDT.CreateDataSet;
      Caption:='���������� �� ������ c '+FormatDateTime('dd.mm.yyyy',DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy',DateEnd)+'   c '+FormatDateTime('hh:nn',TrebSel.TimeFrom)+' �� '+FormatDateTime('hh:nn',TrebSel.TimeTo);
      ProgressBar1.Visible:=True;
      ProgressBar1.Position:=0;
      delay(10);

      ViewRDT.BeginUpdate;
      iDateB:=Trunc(DateBeg);
      iDateE:=Trunc(DateEnd);
      rProc:=100/(iDateE-iDateB);
      for iDateCur:=iDateB to iDateE do
      begin
        quRep1.Active:=False;
        quRep1.ParamByName('DateB').AsDateTime:=iDateCur+Frac(TrebSel.TimeFrom);
        quRep1.ParamByName('DateE').AsDateTime:=iDateCur+Frac(TrebSel.TimeTo);
        quRep1.Active:=True;
        qurep1.First;
        while not quRep1.Eof do
        begin
          if fmMainRep.taRDT.Locate('Sifr',quRep1SIFR.AsInteger,[]) then
          begin //����� ����� ��� ����
            rQ:=fmMainRep.taRDTSUMQUANT.AsFloat;
            rS:=fmMainRep.taRDTSUMSUM.AsFloat;
            rD:=fmMainRep.taRDTSUMDISC.AsFloat;

            fmMainRep.taRDT.Edit;
            fmMainRep.taRDTSUMQUANT.AsFloat:=rQ+quRep1SUMQUANT.AsFloat;
            fmMainRep.taRDTSUMSUM.AsFloat:=rS+quRep1SUMSUM.AsFloat;
            fmMainRep.taRDTSUMDISC.AsFloat:=rD+quRep1SUMDISC.AsFloat;
            fmMainRep.taRDT.Post;
          end else
          begin  // ������ ��� ���.
            fmMainRep.taRDT.Append;
            fmMainRep.taRDTSifr.AsInteger:=quRep1SIFR.AsInteger;
            fmMainRep.taRDTNAME.AsString:=quRep1NAME.AsString;
            fmMainRep.taRDTPARENT.AsInteger:=quRep1PARENT.AsInteger;
            fmMainRep.taRDTNAMEGR.AsString:=quRep1NAMEGR.AsString;
            fmMainRep.taRDTSUMQUANT.AsFloat:=quRep1SUMQUANT.AsFloat;
            fmMainRep.taRDTSUMSUM.AsFloat:=quRep1SUMSUM.AsFloat;
            fmMainRep.taRDTSUMDISC.AsFloat:=quRep1SUMDISC.AsFloat;
            fmMainRep.taRDT.Post;
          end;
          quRep1.Next;
        end;

        ProgressBar1.Position:=Round((iDateCur-iDateB)*rProc);
        delay(100);
      end;

      quRep1.Active:=False;
      ViewRDT.EndUpdate;
      ProgressBar1.Position:=100;
      delay(200);
      ProgressBar1.Visible:=False;
    end;
  end;
  fmPeriod2.Release;
end;

procedure TfmRDTime.SpeedItem1Click(Sender: TObject);
Var StrWk:String;
    i:Integer;
begin
  ViewRDT.BeginUpdate;
  with fmMainRep do
  begin
    taRDT.Filter:=ViewRDT.DataController.Filter.FilterText;
    taRDT.Filtered:=True;

    frRepRDT.LoadFromFile(CurDir + 'RepRDT.frf');

    frVariables.Variable['sPeriod']:='c '+FormatDateTime('dd.mm.yyyy',TrebSel.DateFrom)+' �� '+FormatDateTime('dd.mm.yyyy',TrebSel.DateTo);
    frVariables.Variable['sDayPer']:='c '+FormatDateTime('hh:nn',TrebSel.TimeFrom)+' �� '+FormatDateTime('hh:nn',TrebSel.TimeTo);

    StrWk:=ViewRDT.DataController.Filter.FilterCaption;
    while Pos('AND',StrWk)>0 do
    begin
      i:=Pos('AND',StrWk);
      StrWk[i]:=' ';
      StrWk[i+1]:='�';
      StrWk[i+2]:=' ';
    end;
    while Pos('and',StrWk)>0 do
    begin
      i:=Pos('and',StrWk);
      StrWk[i]:=' ';
      StrWk[i+1]:='�';
      StrWk[i+2]:=' ';
    end;

    frVariables.Variable['sDop']:=StrWk;

    frRepRDT.ReportName:='���������� �� ������ ���.';
    frRepRDT.PrepareReport;
    frRepRDT.ShowPreparedReport;

    taRDT.Filter:='';
    taRDT.Filtered:=False;
  end;
  ViewRDT.EndUpdate;
end;

end.
