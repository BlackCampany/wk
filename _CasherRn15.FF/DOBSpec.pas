unit DOBSpec;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ComCtrls, Menus, cxLookAndFeelPainters, StdCtrls,
  cxButtons, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, Placemnt, cxImageComboBox, cxSplitter,
  cxContainer, cxTextEdit, cxMemo, DBClient, pFIBDataSet, ActnList,
  XPStyleActnCtrls, ActnMan, cxMaskEdit, cxDropDownEdit, cxCalendar,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxButtonEdit, cxCalc, FR_Class;

type
  TfmDOBSpec = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Panel2: TPanel;
    cxButton1: TcxButton;
    Timer1: TTimer;
    FormPlacement1: TFormPlacement;
    Panel3: TPanel;
    Panel4: TPanel;
    cxSplitter1: TcxSplitter;
    Label1: TLabel;
    Memo1: TcxMemo;
    Label2: TLabel;
    cxButton2: TcxButton;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    amDOB: TActionManager;
    acCalcB: TAction;
    Panel5: TPanel;
    Label7: TLabel;
    cxTextEdit1: TcxTextEdit;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    cxDateEdit1: TcxDateEdit;
    cxLookupComboBox1: TcxLookupComboBox;
    cxTextEdit2: TcxTextEdit;
    Label11: TLabel;
    Label12: TLabel;
    Image1: TImage;
    Image2: TImage;
    acCalcBB: TAction;
    Label15: TLabel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    GridB: TcxGrid;
    ViewB: TcxGridDBTableView;
    ViewBSIFR: TcxGridDBColumn;
    ViewBNAMEB: TcxGridDBColumn;
    ViewBCODEB: TcxGridDBColumn;
    ViewBKB: TcxGridDBColumn;
    ViewBQUANT: TcxGridDBColumn;
    ViewBPRICER: TcxGridDBColumn;
    ViewBDSUM: TcxGridDBColumn;
    ViewBRSUM: TcxGridDBColumn;
    ViewBIDCARD: TcxGridDBColumn;
    ViewBNAME: TcxGridDBColumn;
    ViewBNAMESHORT: TcxGridDBColumn;
    ViewBTCARD: TcxGridDBColumn;
    ViewBKM: TcxGridDBColumn;
    LevelB: TcxGridLevel;
    GridC: TcxGrid;
    ViewC: TcxGridDBTableView;
    ViewCArticul: TcxGridDBColumn;
    ViewCName: TcxGridDBColumn;
    ViewCsM: TcxGridDBColumn;
    ViewCQuant: TcxGridDBColumn;
    ViewCQuantFact: TcxGridDBColumn;
    ViewCQuantDiff: TcxGridDBColumn;
    ViewCSumIn: TcxGridDBColumn;
    LevelC: TcxGridLevel;
    GridBC: TcxGrid;
    ViewBC: TcxGridDBTableView;
    ViewBCID: TcxGridDBColumn;
    ViewBCCODEB: TcxGridDBColumn;
    ViewBCNAMEB: TcxGridDBColumn;
    ViewBCQUANT: TcxGridDBColumn;
    ViewBCPRICEOUT: TcxGridDBColumn;
    ViewBCSUMOUT: TcxGridDBColumn;
    ViewBCIDCARD: TcxGridDBColumn;
    ViewBCNAMEC: TcxGridDBColumn;
    ViewBCSB: TcxGridDBColumn;
    ViewBCQUANTC: TcxGridDBColumn;
    ViewBCPRICEIN: TcxGridDBColumn;
    ViewBCSUMIN: TcxGridDBColumn;
    ViewBCIM: TcxGridDBColumn;
    ViewBCSM: TcxGridDBColumn;
    LevelBC: TcxGridLevel;
    PopupMenu1: TPopupMenu;
    Excel1: TMenuItem;
    acExpExcel1: TAction;
    acTest1: TAction;
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Label1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Label2Click(Sender: TObject);
    procedure Memo1DblClick(Sender: TObject);
    procedure Label4Click(Sender: TObject);
    procedure Label5Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure Label6Click(Sender: TObject);
    procedure acCalcBExecute(Sender: TObject);
    procedure ViewBCODEBPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure Label11Click(Sender: TObject);
    procedure acCalcBBExecute(Sender: TObject);
    procedure Label15Click(Sender: TObject);
    procedure ViewBCCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure acExpExcel1Execute(Sender: TObject);
    procedure acTest1Execute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmDOBSpec: TfmDOBSpec;
  bClearDoB:Boolean = False;

implementation

uses Un1, dmOffice, Message, SelPartIn1, DMOReps, Goods;

{$R *.dfm}

procedure TfmDOBSpec.FormCreate(Sender: TObject);
begin
  GridB.Align:=AlClient;
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  ViewB.RestoreFromIniFile(CurDir+GridIni);
  ViewC.RestoreFromIniFile(CurDir+GridIni);
  ViewBC.RestoreFromIniFile(CurDir+GridIni);
end;

procedure TfmDOBSpec.Timer1Timer(Sender: TObject);
begin
  if bClearDoB=True then begin StatusBar1.Panels[0].Text:=''; bClearDoB:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearDoB:=True;
end;

procedure TfmDOBSpec.cxButton1Click(Sender: TObject);
begin
  close;
end;

procedure TfmDOBSpec.FormClose(Sender: TObject; var Action: TCloseAction);
begin
   ViewB.StoreToIniFile(CurDir+GridIni,False);
   ViewC.StoreToIniFile(CurDir+GridIni,False);
   ViewBC.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmDOBSpec.Label1Click(Sender: TObject);
Var iErr:INteger;
begin
  with dmO do
  begin
    iErr:=0;
    Memo1.Clear;
    ViewB.BeginUpdate;
    taDobSpec.First;
    while not taDobSpec.Eof do
    begin
      if taDobSpecIDCARD.AsInteger=0 then
      begin
        Memo1.Lines.Add('������: ��� ������������ �� �����:  '+taDobSpecNAMEB.AsString+' ('+taDobSpecSIFR.AsString+')');
        inc(iErr);
      end;
      taDobSpec.Next;
    end;
    taDobSpec.First;
    ViewB.EndUpdate;
  end;
  if iErr>0 then Memo1.Lines.Add('�����:'+IntToStr(iErr)+' ������.')
            else Memo1.Lines.Add('��� ��. ������ ������������ ������������.');
end;

procedure TfmDOBSpec.FormShow(Sender: TObject);
begin
  Memo1.Clear;
  PageControl1.ActivePageIndex:=0;
end;

procedure TfmDOBSpec.Label2Click(Sender: TObject);
Var rQ:Real;
    IdPos:INteger;

//Procedure prSaveCalc();

begin
  //������ ���-�� ��� ��������
  Memo1.Clear;
  Memo1.Lines.Add('�������� ������.');
  iErr:=0; IdPos:=1;
  with dmO do
  with dmORep do
  begin
    taCalc.Active:=False;
    taCalc.CreateDataSet;

    taCalcB.Active:=False;
    taCalcB.CreateDataSet;

    taDobSpec.First;
    while not taDobSpec.Eof do
    begin
      rQ:=taDobSpecQUANT.AsFloat*taDobSpecKB.AsFloat;
      if taDobSpecIDCARD.AsInteger>0 then prCalcBl('    ',taDobSpecNAME.AsString,IdPos,taDobSpecIDCARD.AsInteger,Trunc(quDocsOutBDATEDOC.AsDateTime),taDobSpecTCARD.AsInteger,rQ,taDobSpecIDM.AsInteger,taCalc,taCalcB,Memo1);

      taDobSpec.Next;
      inc(IdPos);
    end;
    taDobSpec.First;
  end;
  Memo1.Lines.Add('������ ��������.');
  Memo1.Lines.Add('');
  Memo1.Lines.Add('������ �����: '+INtToStr(iErr));
end;

procedure TfmDOBSpec.Memo1DblClick(Sender: TObject);
begin
  fmMessage:=tfmMessage.Create(Application);
  fmMessage.Memo1.Lines:=Memo1.Lines;
  fmMessage.ShowModal;
  fmMessage.Release;
end;

procedure TfmDOBSpec.Label4Click(Sender: TObject);
Var rQ,rQ1:Real;
begin
  iErr:=0;
  Memo1.Clear;
  Memo1.Lines.Add('������ ��������.');
  with dmORep do
  begin
    if taCalc.Active then
    begin
      taCalc.First;
      while not taCalc.Eof do
      begin
        with dmO do  rQ:=prCalcRemn(taCalcArticul.AsInteger,Trunc(quDocsOutBDATEDOC.AsDateTime),quDocsOutBIDSKL.AsInteger);
        rQ1:=rQ-taCalcQuant.AsFloat;
        if (taCalcQuant.AsFloat-rQ)>0.001 then
        begin
          Memo1.Lines.Add('    ������: �� ������� ���-�� '+taCalcName.AsString+' ('+taCalcArticul.AsString+')  '+ FloatToStr(RoundEx((taCalcQuant.AsFloat-rQ)*1000)/1000)+' '+taCalcsM.AsString);
          inc(iErr);
        end;
        taCalc.Edit;
        taCalcQuantFact.AsFloat:=rQ;
        taCalcQuantDiff.AsFloat:=rQ1;
        taCalc.Post;

        taCalc.Next;
      end;
    end else taCalc.CreateDataSet;
  end;
  Memo1.Lines.Add('������ �������� ��������.');
  Memo1.Lines.Add('');
  Memo1.Lines.Add('������ �����: '+INtToStr(iErr));
end;

procedure TfmDOBSpec.Label5Click(Sender: TObject);
Var iYes,iNum:Integer;
    rQP,rQs:Real;
begin
//�������� ������
  fmPartIn1:=tfmPartIn1.Create(Application);
  with dmO do
  with dmORep do
  begin
    if quDocsOutBIDSKL.AsInteger>0 then
    begin
      iNum:=1;
      taPartTest.Active:=False;
      taPartTest.CreateDataSet;

      if taCalc.Active=False then taCalc.CreateDataSet;

      taCalc.First;
      while not taCalc.Eof do
      begin
        //��� ��� ������
    //    k:=prFindKM(taCalcIM.AsInteger);
    //    PriceSp:=taCalcPrice1.AsCurrency/k;

        rQs:=taCalcQuant.AsFloat;
        rQp:=0;
        prSelPartIn(taCalcArticul.AsInteger,quDocsOutBIDSKL.AsInteger,0);
        quSelPartIn.First;
        while not quSelPartIn.Eof do
        begin
          //���� �� ���� ������� ���� �����, ��������� �������� ���
          rQp:=rQp+quSelPartInQREMN.AsFloat;
          quSelPartIn.Next;
        end;
        quSelPartIn.Active:=False;

        if rQp>=rQs then iYes:=1 else iYes:=0;

        taPartTest.Append;
        taPartTestNum.AsInteger:=iNum;
        taPartTestIdGoods.AsInteger:=taCalcArticul.AsInteger;
        taPartTestNameG.AsString:=taCalcName.AsString;
        taPartTestIM.AsInteger:=taCalcIdm.AsInteger;
        taPartTestSM.AsString:=taCalcsM.AsString;
        taPartTestQuant.AsFloat:=taCalcQuant.AsFloat;
        taPartTestPrice1.AsCurrency:=0;
        taPartTestiRes.AsInteger:=iYes;
        taPartTest.Post;

        inc(iNum);
        taCalc.Next;
      end;
      fmPartIn1.ViewPartInPrice1.Visible:=False;
      fmPartIn1.Label5.Caption:=quDocsOutBNAMEMH.AsString;

      fmPartIn1.Label6.Caption:='�� ����.';
      fmPartIn1.ShowModal;

      taPartTest.Active:=False;
      fmPartIn1.Release;
    end else showmessage('����� �������� �� ����������.');
  end;
end;

procedure TfmDOBSpec.cxButton2Click(Sender: TObject);
Var Idh,Ids:Integer;
    rSum:Real;
begin
  //��������� ������
  with dmO do
  with dmORep do
  begin
    Memo1.Lines.Add('����� ���� ����������.');
    IdH:=quDocsOutBID.AsInteger;

    ViewB.BeginUpdate;
    rSum:=0;
    taDobSpec.First;
    while not taDobSpec.Eof do
    begin
      rSum:=rSum+taDobSpecRSUM.AsFloat;
      taDobSpec.Next;
    end;
    ViewB.EndUpdate;


    quDocsOutB.Edit;
    quDocsOutBDATEDOC.AsDateTime:=Trunc(cxDateEdit1.Date);
    quDocsOutBOPER.AsString:=cxTextEdit2.Text;
    quDocsOutBNUMDOC.AsString:=cxTextEdit1.Text;
    quDocsOutBIDSKL.AsInteger:=cxLookupComboBox1.EditValue;
    quDocsOutBSUMUCH.AsFloat:=rSum;
    quDocsOutB.Post;
    quDocsOutB.Refresh;

    taDobSpec1.Active:=False;
    taDobSpec1.ParamByName('IDH').AsInteger:=IdH;
    taDobSpec1.Active:=True;

    while not taDobSpec1.Eof do taDobSpec1.Delete;

    taCalc.First;
    while not taCalc.Eof do
    begin
      Ids:=GetId('SpecOutC');

      taDobSpec1.Append;
      taDobSpec1IDHEAD.AsInteger:=Idh;
      taDobSpec1ID.AsInteger:=Ids;
      taDobSpec1ARTICUL.AsInteger:=taCalcArticul.AsInteger;
      taDobSpec1IDM.AsInteger:=taCalcIdm.AsInteger;
      taDobSpec1SM.AsString:=taCalcsM.AsString;
      taDobSpec1KM.AsFloat:=taCalcKm.AsFloat;
      taDobSpec1QUANT.AsFloat:=taCalcQuant.AsFloat;
      taDobSpec1SUMIN.AsFloat:=taCalcSumIn.AsFloat;
      taDobSpec1.Post;

      taCalc.Next;
    end;

    taDobSpec2.Active:=False;
    taDobSpec2.ParamByName('IDH').AsInteger:=IdH;
    taDobSpec2.Active:=True;

    while not taDobSpec2.Eof do taDobSpec2.Delete;

    IdS:=1;
    taCalcB.First;
    while not taCalcB.Eof do
    begin
      taDobSpec2.Append;
      taDobSpec2IDHEAD.AsInteger:=Idh;
      taDobSpec2IDB.AsInteger:=taCalcBID.AsInteger;
      taDobSpec2ID.AsInteger:=Ids;
      taDobSpec2CODEB.AsInteger:=taCalcBCODEB.AsInteger;
      taDobSpec2NAMEB.AsString:=taCalcBNAMEB.AsString;
      taDobSpec2QUANT.AsFloat:=taCalcBQUANT.AsFloat;
      taDobSpec2PRICEOUT.AsFloat:=taCalcBPRICEOUT.AsFloat;
      taDobSpec2SUMOUT.AsFloat:=taCalcBSUMOUT.AsFloat;
      taDobSpec2IDCARD.AsInteger:=taCalcBIDCARD.AsInteger;
      taDobSpec2NAMEC.AsString:=taCalcBNAMEC.AsString;
      taDobSpec2QUANTC.AsFloat:=taCalcBQUANTC.AsFloat;
      taDobSpec2PRICEIN.AsFloat:=taCalcBPRICEIN.AsFloat;
      taDobSpec2SUMIN.AsFloat:=taCalcBSUMIN.AsFloat;
      taDobSpec2IM.AsInteger:=taCalcBIM.AsInteger;
      taDobSpec2SM.AsString:=taCalcBSM.AsString;
      taDobSpec2SB.AsString:=taCalcBSB.AsString;
      taDobSpec2.Post;

      inc(Ids);
      taCalcB.Next;
    end;

    taDobSpec1.Active:=False;
    taDobSpec2.Active:=False;
    Memo1.Lines.Add('���������� ���������.');
  end;
  close;
end;

procedure TfmDOBSpec.Label6Click(Sender: TObject);
Var PriceSp,rSumIn,rSumUch,rQ,rQs,rQp,PriceUch,rMessure:Real;
begin
  Memo1.Lines.Add('�����, ���� ������.'); delay(10);
  with dmO do
  with dmORep do
  begin
    if taCalc.Active=False then
    begin
      showmessage('���������� ������� ��������� �������.');
      exit;
    end;

    ViewB.BeginUpdate;
    ViewC.BeginUpdate;
    ViewBC.BeginUpdate;

    taCalc.First;
    while not taCalc.Eof do
    begin
      PriceSp:=0;
      rSumIn:=0;
      rSumUch:=0;
      rQs:=taCalcQUANT.AsFloat; //taCalc - � �������� �������� ���������
      prSelPartIn(taCalcARTICUL.AsInteger,quDocsOutBIDSKL.AsInteger,0);

      quSelPartIn.First;
      if rQs>0 then
      begin
        while (not quSelPartIn.Eof) and (rQs>0) do
        begin
          //���� �� ���� ������� ���� �����, ��������� �������� ���
          rQp:=quSelPartInQREMN.AsFloat;
          if rQs<=rQp then  rQ:=rQs//��������� ������ ������ ���������
                            else  rQ:=rQp;
          rQs:=rQs-rQ;

          PriceSp:=quSelPartInPRICEIN.AsFloat;
          PriceUch:=quSelPartInPRICEOUT.AsFloat;
          rSumIn:=rSumIn+PriceSp*rQ;
          rSumUch:=rSumUch+PriceUch*rQ;
          quSelPartIn.Next;
        end;

        if rQs>0 then //�������� ������������� ������, �������� � ������, �� � ������� ��������� ������ ���, ��� � �������������
        begin
          if PriceSp=0 then
          begin //��� ���� ���������� ������� � ���������� ����������
            prCalcLastPrice1.ParamByName('IDGOOD').AsInteger:=taCalcARTICUL.AsInteger;
            prCalcLastPrice1.ExecProc;
            PriceSp:=prCalcLastPrice1.ParamByName('PRICEIN').AsCurrency;
            rMessure:=prCalcLastPrice1.ParamByName('KOEF').AsFloat;
            if (rMessure<>0)and(rMessure<>1) then PriceSp:=PriceSp/rMessure;
          end;
          rSumIn:=rSumIn+PriceSp*rQs;
        end;
      end;
      if rQs<0 then //���� �������� , ���� ������
      begin
        if quSelPartIn.RecordCount>0 then
        begin
          PriceSp:=quSelPartInPRICEIN.AsFloat;
          PriceUch:=quSelPartInPRICEOUT.AsFloat;
          rSumIn:=PriceSp*rQs;
          rSumUch:=PriceUch*rQs;
        end else
        begin //��������� ��������� ��� - ���� ���������� �������
          prCalcLastPrice1.ParamByName('IDGOOD').AsInteger:=taCalcARTICUL.AsInteger;
          prCalcLastPrice1.ExecProc;
          PriceSp:=prCalcLastPrice1.ParamByName('PRICEIN').AsCurrency;
          rMessure:=prCalcLastPrice1.ParamByName('KOEF').AsFloat;
          if (rMessure<>0)and(rMessure<>1) then PriceSp:=PriceSp/rMessure;

          rSumIn:=PriceSp*rQs;
        end;
      end;

      quSelPartIn.Active:=False;

      //�������� ���������
      taCalc.Edit;
      taCalcSumUch.AsFloat:=rSumUch;
      taCalcSumIn.AsFloat:=rSumIn;
      taCalc.Post;

      taCalc.Next;
    end;


    taCalcB.First;
    while not taCalcB.Eof do
    begin
      if taCalc.Locate('Articul',taCalcBIDCARD.AsInteger,[]) then
      if taCalcQuant.AsFloat<>0 then
      begin
        taCalcB.Edit;
        taCalcBPRICEIN.AsFloat:=taCalcSumIn.AsFloat/taCalcQuant.AsFloat;
        taCalcBSUMIN.AsFloat:=taCalcSumIn.AsFloat/taCalcQuant.AsFloat*taCalcBQUANTC.AsFloat;
        taCalcB.Post;
      end;
      taCalcB.Next;
    end;

    ViewB.EndUpdate;
    ViewC.EndUpdate;
    ViewBC.EndUpdate;
  end;

//  acCalcB.Execute;
  Memo1.Lines.Add('������ ��������.'); delay(10);
end;

procedure TfmDOBSpec.acCalcBExecute(Sender: TObject);
  //������ ������������� ����
Var // rQP,rQs,PriceSp,PriceUch,rQ,rSumIn,rSumUch:Real;
    IdCard,iDC:Integer;
    kM:Real;
    sM:String;
    iM,iTc,IPCount,iMain,iDate:Integer;
    QT1:TpFIBDataSet;
    rQ,rSumIn,rQ1,kBrutto,MassaB:Real;

procedure prSaveB(iCode:INteger;rQ,rQ1:Real);
begin
  with dmO do
  with dmORep do
  begin
    if taCalc.Locate('Articul',iCode,[]) then
    if taCalcQuant.AsFloat<>0 then
    begin
      taCalcB.Append;
//      taCalcBID.AsInteger:=taDobSpecID.AsInteger;
      taCalcBID.AsInteger:=iId;
      taCalcBCODEB.AsInteger:=taDobSpecIDCARD.AsInteger;
      taCalcBNAMEB.AsString:=taDobSpecNAME.AsString;
      taCalcBQUANT.AsFloat:=rQ;
      if rQ<>0 then
      begin
        taCalcBPRICEOUT.AsFloat:=taDobSpecRSUM.AsFloat/rQ;
      end else
      begin
        taCalcBPRICEOUT.AsFloat:=0;
      end;
      taCalcBSUMOUT.AsFloat:=taDobSpecRSUM.AsFloat;
      taCalcBIDCARD.AsInteger:=taCalcArticul.AsInteger;
      taCalcBNAMEC.AsString:=taCalcName.AsString;
      taCalcBQUANTC.AsFloat:=rQ1;
      if taCalcQuant.AsFloat<>0 then
      begin
        taCalcBPRICEIN.AsFloat:=taCalcSumIn.AsFloat/taCalcQuant.AsFloat;
        taCalcBSUMIN.AsFloat:=taCalcSumIn.AsFloat/taCalcQuant.AsFloat*rQ1;
      end else
      begin
        taCalcBPRICEIN.AsFloat:=0;
        taCalcBSUMIN.AsFloat:=0;
      end;
      taCalcBIM.AsInteger:=taCalcidM.AsInteger;
      taCalcBSM.AsString:=taCalcSm.AsString;
      taCalcBSB.AsString:='';
      taCalcB.Post;
    end;
  end;
end;


begin
  //������ ������������� ����
  // 1. ���������� ������� ��������� - ������� ��� ��� �������
  // 2. ���������� ������������� ��������
  // 3. ���������� ������������� ���� �� 2-��� ������

  iId:=1;
  // 2. ���������� ������������� ��������
  with dmO do
  with dmORep do
  begin
    if taCalc.Active=False then
    begin
      showmessage('���������� ������� ��������� �������.');
      exit;
    end;

    iDate:=Trunc(quDocsOutBDATEDOC.AsDateTime);

    taCalcB.Active:=False;
    taCalcB.CreateDataSet;


    ViewB.BeginUpdate;
    taDobSpec.First;
    while not taDobSpec.Eof do
    begin
      if (taDobSpecIDCARD.AsInteger>0)and(taDobSpecNAME.AsString>'') then
      begin //����� � �������� ����������
        IdCard:=taDobSpecIDCARD.AsInteger;
        rQ:=taDobSpecQUANT.AsFloat*taDobSpecKB.AsFloat;
//       rSumIn:=0;

        if taDobSpecTCARD.AsInteger=1 then
        begin //���� �� - ������ ������� ���� ������������
          iTC:=0; iPCount:=0; MassaB:=1;
          //��� ��� ��
          quFindTCard.Active:=False;
          quFindTCard.ParamByName('IDCARD').AsInteger:=IdCard;
          quFindTCard.ParamByName('IDATE').AsDateTime:=iDate;
          quFindTCard.Active:=True;
          if quFindTCard.RecordCount>0 then
          begin
            iTC:=quFindTCardID.AsInteger;
            iPCount:=quFindTCardPCOUNT.AsInteger;
            MassaB:=quFindTCardPVES.AsFloat/1000;
            if prFindMT(taDobSpecIDM.AsInteger)=1 then MassaB:=1;
          end;
          quFindTCard.Active:=False;
          if iTC>0 then
          begin  //��� �����������
            QT1:=TpFIBDataSet.Create(Owner);
            QT1.Active:=False;
            QT1.Database:=OfficeRnDb;
            QT1.Transaction:=trSel;
            QT1.SelectSQL.Clear;
            QT1.SelectSQL.Add('SELECT CS.ID,CS.IDCARD,CS.CURMESSURE,CS.NETTO,CS.BRUTTO,CS.KNB,');
            QT1.SelectSQL.Add('CD.NAME,CD.TCARD');
            QT1.SelectSQL.Add('FROM OF_CARDSTSPEC CS');
            QT1.SelectSQL.Add('left join of_cards cd on cd.ID=CS.IDCARD');
            QT1.SelectSQL.Add('where IDC='+IntToStr(IdCard)+' and IDT='+IntToStr(iTC));
            QT1.SelectSQL.Add('ORDER BY CS.ID');
            QT1.Active:=True;

            QT1.First;
            while not QT1.Eof do
            begin
        //����� �����
              rQ1:=QT1.FieldByName('NETTO').AsFloat;

             //��������� � �������� �������
              kM:=prFindKM(QT1.FieldByName('CURMESSURE').AsInteger);
              prFindSM(QT1.FieldByName('CURMESSURE').AsInteger,sM,iMain); //��� �������� �������� ������� ���������
              rQ1:=rQ1*kM;

              //���� ������ �� ������ ����
              IdC:=QT1.FieldByName('IDCARD').AsInteger;
              kBrutto:=prFindBrutto(IdC,iDate);
              rQ1:=rQ1*(100+kBrutto)/100; //��� ����� � ������
              rQ1:=rQ1/iPCount; //��� ����� �� 1-� ������ ���� �������� �� �������
              rQ1:=rQ1*rQ/MassaB; //��� ����� �� ��� ���-�� ������
              rQ1:=rQ1*rQ; //��� ����� �� ��� ���-�� ������

              if QT1.FieldByName('TCARD').AsInteger=1 then
              begin  //���� ��
                prCalcBlPrice(QT1.FieldByName('IDCARD').AsInteger,iDate,rQ1,iMain,taCalc,rSumIn);

                taCalcB.Append;
                taCalcBID.AsInteger:=iId;
                taCalcBCODEB.AsInteger:=taDobSpecIDCARD.AsInteger;
                taCalcBNAMEB.AsString:=taDobSpecNAME.AsString;
                taCalcBQUANT.AsFloat:=rQ;
                if rQ<>0 then
                begin
                  taCalcBPRICEOUT.AsFloat:=taDobSpecRSUM.AsFloat/rQ;
                  taCalcBPRICEIN.AsFloat:=rSumIn/rQ;
                  taCalcBSUMIN.AsFloat:=rSumIn;
                end else
                begin
                  taCalcBPRICEOUT.AsFloat:=0;
                  taCalcBPRICEIN.AsFloat:=0;
                  taCalcBSUMIN.AsFloat:=0;
                end;
                taCalcBSUMOUT.AsFloat:=taDobSpecRSUM.AsFloat;
                taCalcBIDCARD.AsInteger:=QT1.FieldByName('IDCARD').AsInteger;
                taCalcBNAMEC.AsString:=QT1.FieldByName('NAME').AsString;
                taCalcBQUANTC.AsFloat:=rQ1;
                taCalcBIM.AsInteger:=iMain;
                taCalcBSM.AsString:=sM;
                taCalcBSB.AsString:='�����';
                taCalcB.Post;

              end else prSaveB(QT1.FieldByName('IDCARD').AsInteger,rQ,rQ1); //��� ��
              QT1.Next;
            end;
            QT1.Active:=False;
            QT1.Free;
          end;
        end else
        begin //��� ��
          //�������� � �������� �����
          kM:=prFindKM(taDobSpecIDM.AsInteger);
          prFindSM(taDobSpecIDM.AsInteger,sM,iM); //��� �������� �������� ������� ���������
          rQ:=rQ*kM;

          prSaveB(IdCard,rQ,rQ);
        end;
      end;
      inc(iId);
      taDobSpec.Next;
    end;
    taDobSpec.First;
    ViewB.EndUpdate;

    taCalcB.First;
  end;
end;

procedure TfmDOBSpec.ViewBCODEBPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  bAddSpecB1:=True;
  if fmGoods.Visible then fmGoods.Close; delay(10);
  fmGoods.Show;
end;

procedure TfmDOBSpec.Label11Click(Sender: TObject);
begin
  bAddSpecB:=True;
  if fmGoods.Visible then fmGoods.Close; delay(10);
  fmGoods.Show;
end;

procedure TfmDOBSpec.acCalcBBExecute(Sender: TObject);
Var StrWk:String;
    rSum:Real;
    rQ,rQ1:Real;
begin
  //������ ����� � �����
  with dmO do
  with dmORep do
  begin
    if taCalcB.Active then
    if taCalcBSB.AsString>'' then
    begin //��� �����
      Memo1.Clear;
      rQ:=taCalcBQUANT.AsFloat;
      rQ1:=0; if rQ<>0 then rQ1:=taCalcBQUANTC.AsFloat/rQ;
      StrWk:=taCalcBNAMEC.AsString;
      While Length(StrWk)<20 do StrWk:=StrWk+' ';
      Memo1.Lines.Add('    '+StrWk+' ('+taCalcBIDCARD.AsString+'). ���-��: '+FloatToStr(RoundEx(taCalcBQUANTC.AsFloat*1000)/1000)+'  ������: '+FloatToStr(RoundEx(rQ)*1000/1000)+' �� 1-� ����.: '+FloatToStr(RoundEx(rQ1*1000)/1000));
      prCalcBlPrice1('    ',taCalcBIDCARD.AsInteger,Trunc(quDocsOutBDATEDOC.AsDateTime),taCalcBQUANTC.AsFloat,taCalcBIM.AsInteger,taCalc,Memo1,rSum);
      Memo1.Lines.Add('');
      if taCalcBQUANTC.AsFloat<>0 then
        Memo1.Lines.Add('    ����� �� '+IntToStr(RoundEx(taCalcBQUANT.AsFloat))+' ������ :     '+FloatToStr(RoundEx(rSum*100)/100)+' ���.  ���� ������: '+FloatToStr(RoundEx(rSum/taCalcBQUANT.AsFloat*100)/100));
    end;
  end;
end;

procedure TfmDOBSpec.Label15Click(Sender: TObject);
begin
  with dmO do
  with dmORep do
  begin
    frRep1.LoadFromFile(CurDir + 'BludaSeb.frf');

    frVariables.Variable['DocNum']:=quDocsOutBNUMDOC.AsString;
    frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',quDocsOutBDATEDOC.AsDateTime);
    frVariables.Variable['SumOut']:=quDocsOutBSUMUCH.AsFloat;
    frVariables.Variable['DocOper']:=quDocsOutBOPER.AsString;

    frRep1.ReportName:='������������� ����.';
    frRep1.PrepareReport;
    frRep1.ShowPreparedReport;
  end;
end;

procedure TfmDOBSpec.ViewBCCustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);

Var i:Integer;
    sType:String;

begin
//����������
  sType:='';
  for i:=0 to ViewBC.ColumnCount-1 do
  begin
    if ViewBC.Columns[i].Name='ViewBCSB' then
    begin
      try
        sType:=AViewInfo.GridRecord.DisplayTexts[i];
        break;
      except
      end;
    end;
  end;

  if sType>''  then
  begin
    ACanvas.Canvas.Brush.Color := $00B3FF66;
    ACanvas.Canvas.Font.Color := clBlack;
  end;
end;

procedure TfmDOBSpec.acExpExcel1Execute(Sender: TObject);
begin
  prExportExel1(ViewC,dmORep.dsCalc,dmORep.taCalc);
end;

procedure TfmDOBSpec.acTest1Execute(Sender: TObject);
Var rQ:Real;
begin
  if MessageDlg('����� ��������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
    Memo1.Clear;
    Memo1.Lines.Add('������'); Delay(10);
    with dmORep do
    begin
      taCalcB.First;
      while not taCalcB.Eof do
      begin
        if taCalcBSB.AsString='' then
        begin
          if taCalc.Locate('Articul',taCalcBIDCARD.AsInteger,[]) then
          begin
            rQ:=taCalcQuant.AsFloat-taCalcBQUANTC.AsFloat;
            taCalc.Edit;
            taCalcQuant.AsFloat:=rQ;
            taCalc.Post;
            taCalcB.Delete;
          end else taCalcB.Next;
        end else taCalcB.Next;
      end;
    end;
    Memo1.Lines.Add('���������.');
  end;
end;

end.
