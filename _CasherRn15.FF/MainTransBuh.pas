unit MainTransBuh;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, cxLookAndFeelPainters, cxButtons, ExtCtrls,
  FIBDatabase, pFIBDatabase, DBTables, DB, FIBDataSet, pFIBDataSet,
  ComCtrls, dxfProgressBar, dxfBackGround, dxfColorButton, dxfDesigner,
  dxfCheckBox, cxControls, cxContainer, cxEdit, cxRadioGroup, ActnList,
  XPStyleActnCtrls, ActnMan;

type
  TfmMainAppend = class(TForm)
    Memo1: TMemo;
    Timer1: TTimer;
    dmC: TpFIBDatabase;
    trSelect: TpFIBTransaction;
    trUpdate: TpFIBTransaction;
    taCashB: TpFIBDataSet;
    Timer2: TTimer;
    ProgressBar1: TdxfProgressBar;
    dxfBackGround1: TdxfBackGround;
    Button1: TdxfColorButton;
    Button2: TdxfColorButton;
    CheckBox1: TdxfCheckBox;
    CheckBox2: TdxfCheckBox;
    Label1: TLabel;
    DateTimePicker1: TDateTimePicker;
    Label2: TLabel;
    DateTimePicker2: TDateTimePicker;
    am1: TActionManager;
    aCheck1: TAction;
    aCheck2: TAction;
    CheckBox3: TdxfCheckBox;
    taFindBound: TpFIBDataSet;
    taFindBoundMINID: TFIBIntegerField;
    taFindBoundMAXID: TFIBIntegerField;
    DateTimePicker3: TDateTimePicker;
    DateTimePicker4: TDateTimePicker;
    taCashBCASHNUM: TFIBIntegerField;
    taCashBZNUM: TFIBIntegerField;
    taCashBITYPE: TFIBSmallIntField;
    taCashBSIFR: TFIBIntegerField;
    taCashBPRICE: TFIBFloatField;
    taCashBSUMQ: TFIBFloatField;
    taCashBSUMD: TFIBFloatField;
    taCashBSUMR: TFIBFloatField;
    taCashBMIN: TFIBDateTimeField;
    taFindLast: TpFIBDataSet;
    taFindLastMAXID: TFIBIntegerField;
    taCashBCODE: TFIBStringField;
    taSpecDel: TpFIBDataSet;
    taSpecDelITYPE: TFIBSmallIntField;
    taSpecDelSIFR: TFIBIntegerField;
    taSpecDelCODE: TFIBStringField;
    taSpecDelPRICE: TFIBFloatField;
    taSpecDelSUMQ: TFIBFloatField;
    taSpecDelSUMD: TFIBFloatField;
    taSpecDelSUMR: TFIBFloatField;
    taFindBDel: TpFIBDataSet;
    taFindBDelMINID: TFIBIntegerField;
    taFindBDelMAXID: TFIBIntegerField;
    trSel1: TpFIBTransaction;
    trUpd1: TpFIBTransaction;
    taFindLDel: TpFIBDataSet;
    taFindLDelMAXID: TFIBIntegerField;
    taSpecDelENDTIME: TFIBDateTimeField;
    taCashZ: TpFIBDataSet;
    taCashZCASHNUM: TFIBIntegerField;
    taCashZZNUM: TFIBIntegerField;
    taCashZDATEZ: TFIBDateTimeField;
    taCashBNAME: TFIBStringField;
    taCashBSTREAM: TFIBSmallIntField;
    taCashBPARENT: TFIBSmallIntField;
    taCashBDISCONT: TFIBStringField;
    taCashBOPERTYPE: TFIBStringField;
    taSpecDelNAME: TFIBStringField;
    taSpecDelSTREAM: TFIBSmallIntField;
    taSpecDelPARENT: TFIBSmallIntField;
    taSpecDelDISCONT: TFIBStringField;
    taSpecDelOPERTYPE: TFIBStringField;
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure aCheck1Execute(Sender: TObject);
    procedure aCheck2Execute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    Procedure prExportCash;
  end;

var
  fmMainAppend: TfmMainAppend;
  iProc:Integer;
  iCurR:Integer;

implementation

uses Un1, u2fdk;

{$R *.dfm}

Procedure TfmMainAppend.prExportCash;
Var StrWk:String;
    ExpN:String;
    f:TextFile;
    iBeg,iEnd:INteger;
    par:Variant;
    tD:TDateTime;
    TimeShift:TDateTime;
begin
  Memo1.Lines.Add('�������� ���� FB.');
  try
    dmC.Connected:=False;
    dmC.DatabaseName:=DBName;
    dmC.Connected:=True;
    if dmC.Connected=True then Memo1.Lines.Add('���������� ��.');
    //���������

    Memo1.Lines.Add('�����. ���� �������� ������.');

    if CommonSet.ExportFile>'' then ExpN:=CommonSet.ExportFile
    else ExpN:='Exp'+FormatDateTime('yyyy_mm_dd',date)+'.txt';

    if CheckBox1.Checked then
    begin
      ReadStartIni;

      taCashB.Active:=False;
      taCashB.ParamByName('IdBeg').AsInteger:=StartId.IdBeg;
      taCashB.ParamByName('IdEnd').AsInteger:=100000000;
      taCashB.Active:=True;

      taCashZ.Active:=False;
      taCashZ.ParamByName('IdBeg').AsInteger:=StartId.IdBeg;
      taCashZ.ParamByName('IdEnd').AsInteger:=100000000;
      taCashZ.Active:=True;

      taSpecDel.Active:=False;
      taSpecDel.ParamByName('IdBeg').AsInteger:=StartId.IdBegT;
      taSpecDel.ParamByName('IdEnd').AsInteger:=100000000;
      taSpecDel.Active:=True;



      taFindLast.Active:=False;
      taFindLast.Active:=True;

      StartId.IdBeg:=taFindLastMAXID.AsInteger+1;

      taFindLDel.Active:=False;
      taFindLDel.Active:=True;

      StartId.IdBegT:=taFindLDelMAXID.AsInteger+1;
    end
    else
    begin
      taFindBound.Active:=False;
      taFindBound.ParamByName('DateB').AsString:=FormatDateTime('dd.mm.yyyy',DateTimePicker1.Date)+FormatDateTime(' hh:nn:ss',DateTimePicker3.Date);
      taFindBound.ParamByName('DateE').AsString:=FormatDateTime('dd.mm.yyyy',DateTimePicker2.Date)+FormatDateTime(' hh:nn:ss',DateTimePicker4.Date);
      taFindBound.Active:=True;
      IBeg:= taFindBoundMINID.AsInteger;
      IEnd:= taFindBoundMAXID.AsInteger;
      taFindBound.Active:=False;

      taCashB.Active:=False;
      taCashB.ParamByName('IdBeg').AsInteger:=IBeg;
      taCashB.ParamByName('IdEnd').AsInteger:=IEnd;
      taCashB.Active:=True;

      taCashZ.Active:=False;
      taCashZ.ParamByName('IdBeg').AsInteger:=IBeg;
      taCashZ.ParamByName('IdEnd').AsInteger:=IEnd;
      taCashZ.Active:=True;

      StartId.IdBeg:= IEnd+1;

      taFindBDel.Active:=False;
      taFindBDel.ParamByName('DateB').AsString:=FormatDateTime('dd.mm.yyyy',DateTimePicker1.Date)+FormatDateTime(' hh:mm:ss',DateTimePicker3.Date);
      taFindBDel.ParamByName('DateE').AsString:=FormatDateTime('dd.mm.yyyy',DateTimePicker2.Date)+FormatDateTime(' hh:mm:ss',DateTimePicker4.Date);
      taFindBDel.Active:=True;
      IBeg:= taFindBDelMINID.AsInteger;
      IEnd:= taFindBDelMAXID.AsInteger;
      taFindBDel.Active:=False;

      taSpecDel.Active:=False;
      taSpecDel.ParamByName('IdBeg').AsInteger:=IBeg;
      taSpecDel.ParamByName('IdEnd').AsInteger:=IEnd;
      taSpecDel.Active:=True;

      StartId.IdBegT:= IEnd+1;
    end;


    taCashB.First;
    taSpecDel.First;
    ProgressBar1.Position:=0;
    ProgressBar1.Visible:=True;
    Timer2.Enabled:=True;
    iProc:=taCashB.RecordCount+taSpecDel.RecordCount;
    iCurR:=1;

    //������� ���� ��� ������
    try
      assignfile(f,CommonSet.PathExport+ExpN);
      if CheckBox3.Checked then
      begin
        if FileExists(CommonSet.PathExport+ExpN) then Append(f)
        else Rewrite(f);
      end
      else Rewrite(f);

      par := VarArrayCreate([0,1], varInteger);

      Memo1.Lines.Add('  �������� �����.'); delay(10);
      while not taCashB.Eof do
      begin
        //��� �� ������ ������
        StrWk:=FormatDateTime('dd.mm.yyyy',taCashBMIN.AsDateTime);
//        StrWk:=FormatDateTime('dd.mm.yyyy',Date);

        //��� �������� ����������� ����
        par[0]:=taCashBCASHNUM.AsInteger;
        par[1]:=taCashBZNUM.AsInteger;
        if taCashZ.Locate('CASHNUM;ZNUM',par,[]) then StrWk:= FormatDateTime('dd.mm.yyyy',taCashZDATEZ.AsDateTime);

        StrWk:=IntToStr(taCashBCASHNUM.AsInteger)+';'+IntToStr(taCashBZNUM.AsInteger)+';'+IntToStr(taCashBITYPE.AsInteger)+';'+taCashBCODE.AsString+';'+FloatToStr(taCashBPRICE.AsCurrency)+';'+FloatToStr(taCashBSUMQ.AsFloat)+';'+FloatToStr(RoundEx(taCashBSUMD.AsCurrency*100)/100)+';'+FloatToStr(RoundEx(taCashBSUMR.AsCurrency*100)/100)+';'+StrWk+';'+taCashBSIFR.AsString+';R;'+AnsiToOemConvert(taCashBNAME.AsString)+';'+taCashBSTREAM.AsString+';'+taCashBPARENT.AsString+';"'+taCashBDISCONT.AsString+'";'+taCashBOPERTYPE.AsString;
        WriteLn(f,StrWk);
        inc(iCurR);
        taCashB.Next;
      end;

      Memo1.Lines.Add('  �������� ���������.'); delay(10);
      while not taSpecDel.Eof do
      begin
        TimeShift:=StrToTimeDef(CommonSet.ZTimeShift,0.25);
        tD:=taSpecDelENDTIME.AsDateTime;
        if frac(tD)<=TimeShift then//������ 6 ����� ���� �� �������� �� ���������� �����
        begin
          tD:=tD-TimeShift;
        end;

//        StrWk:=FormatDateTime('dd.mm.yyyy',taSpecDelENDTIME.AsDateTime);
        StrWk:=FormatDateTime('dd.mm.yyyy',tD);

        StrWk:='0;0;'+IntToStr(taSpecDelITYPE.AsInteger)+';'+taSpecDelCODE.AsString+';'+FloatToStr(taSpecDelPRICE.AsCurrency)+';'+FloatToStr(taSpecDelSUMQ.AsFloat)+';'+FloatToStr(RoundEx(taSpecDelSUMD.AsCurrency*100)/100)+';'+FloatToStr(RoundEx(taSpecDelSUMR.AsCurrency*100)/100)+';'+StrWk+';'+taSpecDelSIFR.AsString+';D;'+AnsiToOemConvert(taSpecDelNAME.AsString)+';'+taSpecDelSTREAM.AsString+';'+taSpecDelPARENT.AsString+';"'+taSpecDelDISCONT.AsString+'";'+taSpecDelOPERTYPE.AsString;
        WriteLn(f,StrWk);
        inc(iCurR);
        taSpecDel.Next;
      end;

    finally
      CloseFile(f);
      taSpecDel.Active:=False;
      taCashB.Active:=False;
      taCashZ.Active:=False;
    end;

    WriteStartIni;

    Timer2.Enabled:=False;
    Memo1.Lines.Add('�������� ��������� ��.');
    ProgressBar1.Visible:=False;
  finally
    Button1.Enabled:=True;
//    Memo1.Lines.Add('�������� ���� RK.');

    Memo1.Lines.Add('�������� ���� FB.');
    dmC.Connected:=False;

    StrWk := ParamStr(1);
    if (StrWk='As')or(StrWk='as')or(StrWk='AS') then
    begin
      Delay(3000);
      Close;
    end;
  end;
end;

procedure TfmMainAppend.FormCreate(Sender: TObject);
Var StrWk:String;
    TimeShift:TDateTime;
begin
  CurDir := ExtractFilePath(ParamStr(0));
  ReadIni;
  Memo1.Clear;
  DateTimePicker1.DateTime:=Trunc(Date-1);
  DateTimePicker2.DateTime:=Trunc(Date);
  TimeShift:=StrToTimeDef(CommonSet.ZTimeShift,0.25);
  DateTimePicker3.Time:=frac(TimeShift);
  DateTimePicker4.Time:=frac(TimeShift);
  StrWk :=  ParamStr(1);
  if (StrWk='As')or(StrWk='as')or(StrWk='AS') then
  begin
    Button1.Enabled:=False;
    Timer1.Enabled:=True;
  end;
end;

procedure TfmMainAppend.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled:=False;
  Button1.Enabled:=False;
  prExportCash;
end;

procedure TfmMainAppend.Timer2Timer(Sender: TObject);
begin
  ProgressBar1.Position:=round(iCurR/iProc*100);
  delay(10);
end;

procedure TfmMainAppend.Button2Click(Sender: TObject);
begin
  Close;
end;

procedure TfmMainAppend.Button1Click(Sender: TObject);
begin
  Button1.Enabled:=False;
  Timer1.Enabled:=True;
end;

procedure TfmMainAppend.aCheck1Execute(Sender: TObject);
begin
  if CheckBox1.Checked then
  begin
    CheckBox2.Checked:=False;
    Label1.Visible:=False;
    Label2.Visible:=False;
    DateTimePicker1.Visible:=False;
    DateTimePicker2.Visible:=False;
    DateTimePicker3.Visible:=False;
    DateTimePicker4.Visible:=False;
  end
  else
  begin
    CheckBox1.Checked:=False;
    Label1.Visible:=True;
    Label2.Visible:=True;
    DateTimePicker1.Visible:=True;
    DateTimePicker2.Visible:=True;
    DateTimePicker3.Visible:=True;
    DateTimePicker4.Visible:=True;
  end;
end;

procedure TfmMainAppend.aCheck2Execute(Sender: TObject);
begin
  if CheckBox2.Checked then
  begin
    CheckBox1.Checked:=False;
    Label1.Visible:=True;
    Label2.Visible:=True;
    DateTimePicker1.Visible:=True;
    DateTimePicker2.Visible:=True;
    DateTimePicker3.Visible:=True;
    DateTimePicker4.Visible:=True;
  end
  else
  begin
    CheckBox1.Checked:=True;
    Label1.Visible:=False;
    Label2.Visible:=False;
    DateTimePicker1.Visible:=False;
    DateTimePicker2.Visible:=False;
    DateTimePicker3.Visible:=False;
    DateTimePicker4.Visible:=False;
  end;
end;

end.
