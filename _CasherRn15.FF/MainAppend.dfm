object fmMainAppend: TfmMainAppend
  Left = 303
  Top = 110
  BorderStyle = bsDialog
  Caption = 'Appender'
  ClientHeight = 412
  ClientWidth = 324
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TMemo
    Left = 0
    Top = 0
    Width = 324
    Height = 353
    Align = alTop
    Lines.Strings = (
      'Memo1')
    TabOrder = 0
  end
  object cxButton1: TcxButton
    Left = 208
    Top = 368
    Width = 91
    Height = 25
    Caption = #1042#1099#1093#1086#1076
    TabOrder = 1
    OnClick = cxButton1Click
    Glyph.Data = {
      5E040000424D5E04000000000000360000002800000012000000130000000100
      18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
      CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
      8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
      0000CED3D6848684848684848684848684848684848684848684848684848684
      848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
      7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
      FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
      00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
      75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
      0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
      FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
      CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
      007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
      00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
      75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
      0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
      FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
      CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
      00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
      494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
      0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
      00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
      CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
      0000}
    LookAndFeel.Kind = lfFlat
    UseSystemPaint = False
  end
  object ProgressBar1: TProgressBar
    Left = 16
    Top = 376
    Width = 150
    Height = 16
    Position = 5
    TabOrder = 2
    Visible = False
  end
  object Timer1: TTimer
    Enabled = False
    OnTimer = Timer1Timer
    Left = 256
    Top = 16
  end
  object dmC: TpFIBDatabase
    DBName = 'C:\Projects\Casher\Rn\DB\CasherRn.GDB'
    DBParams.Strings = (
      'lc_ctype=WIN1251'
      'password=masterkey'
      'sql_role_name=SYSDBA'
      'user_name=SYSDBA')
    DefaultTransaction = trSelect
    DefaultUpdateTransaction = trUpdate
    SQLDialect = 3
    Timeout = 0
    DesignDBOptions = [ddoIsDefaultDatabase]
    WaitForRestoreConnect = 0
    Left = 32
    Top = 16
  end
  object trSelect: TpFIBTransaction
    DefaultDatabase = dmC
    TimeoutAction = TARollback
    Left = 88
    Top = 16
  end
  object trUpdate: TpFIBTransaction
    DefaultDatabase = dmC
    TimeoutAction = TARollback
    Left = 144
    Top = 16
  end
  object taCheck: TTable
    DatabaseName = 'C:\Projects\Casher\Rn\BIN\RkData\'
    SessionName = 'Session1_1'
    TableName = 'ACheck.DB'
    Left = 104
    Top = 88
    object taCheckUNI: TIntegerField
      FieldName = 'UNI'
    end
    object taCheckSys_Num: TIntegerField
      FieldName = 'Sys_Num'
    end
    object taCheckCnum: TSmallintField
      FieldName = 'Cnum'
    end
    object taCheckLogicDate: TDateField
      FieldName = 'LogicDate'
    end
    object taCheckRealDate: TDateField
      FieldName = 'RealDate'
    end
    object taCheckOpenTime: TStringField
      FieldName = 'OpenTime'
      Size = 5
    end
    object taCheckCloseTime: TStringField
      FieldName = 'CloseTime'
      Size = 5
    end
    object taCheckCover: TSmallintField
      FieldName = 'Cover'
    end
    object taCheckCashier: TSmallintField
      FieldName = 'Cashier'
    end
    object taCheckWaiter: TSmallintField
      FieldName = 'Waiter'
    end
    object taCheckUnit: TStringField
      FieldName = 'Unit'
      Size = 2
    end
    object taCheckDepart: TStringField
      FieldName = 'Depart'
      Size = 2
    end
    object taCheckTotal: TFloatField
      FieldName = 'Total'
    end
    object taCheckBaseKurs: TFloatField
      FieldName = 'BaseKurs'
    end
    object taCheckDeleted: TSmallintField
      FieldName = 'Deleted'
    end
    object taCheckManager: TSmallintField
      FieldName = 'Manager'
    end
    object taCheckCharge: TFloatField
      FieldName = 'Charge'
    end
    object taCheckTable: TStringField
      FieldName = 'Table'
      Size = 4
    end
    object taCheckOpenDate: TDateField
      FieldName = 'OpenDate'
    end
    object taCheckCount: TSmallintField
      FieldName = 'Count'
    end
    object taCheckNacKurs: TFloatField
      FieldName = 'NacKurs'
    end
    object taCheckTotalR: TFloatField
      FieldName = 'TotalR'
    end
    object taCheckCoverR: TSmallintField
      FieldName = 'CoverR'
    end
    object taCheckTaxSum: TFloatField
      FieldName = 'TaxSum'
    end
    object taCheckTaxSumR: TFloatField
      FieldName = 'TaxSumR'
    end
    object taCheckTaxRate: TFloatField
      FieldName = 'TaxRate'
    end
    object taCheckTaxRateR: TFloatField
      FieldName = 'TaxRateR'
    end
    object taCheckBonus: TFloatField
      FieldName = 'Bonus'
    end
    object taCheckBonusCard: TFloatField
      FieldName = 'BonusCard'
    end
  end
  object Session1: TSession
    Active = True
    AutoSessionName = True
    NetFileDir = 'C:\'
    Left = 32
    Top = 88
  end
  object taRCheck: TTable
    DatabaseName = 'C:\Projects\Casher\Rn\BIN\RkData\'
    SessionName = 'Session1_1'
    TableName = 'ARcheck.DB'
    Left = 168
    Top = 88
    object taRCheckUNI: TIntegerField
      FieldName = 'UNI'
    end
    object taRCheckSys_Num: TIntegerField
      FieldName = 'Sys_Num'
    end
    object taRCheckCnum: TSmallintField
      FieldName = 'Cnum'
    end
    object taRCheckSifr: TSmallintField
      FieldName = 'Sifr'
    end
    object taRCheckQnt: TFloatField
      FieldName = 'Qnt'
    end
    object taRCheckPrice: TFloatField
      FieldName = 'Price'
    end
    object taRCheckComp: TStringField
      FieldName = 'Comp'
      Size = 1
    end
    object taRCheckRealPrice: TFloatField
      FieldName = 'RealPrice'
    end
    object taRCheckQntR: TFloatField
      FieldName = 'QntR'
    end
    object taRCheckNalog: TFloatField
      FieldName = 'Nalog'
    end
  end
  object taDCheck: TTable
    DatabaseName = 'C:\Projects\Casher\Rn\BIN\RkData\'
    SessionName = 'Session1_1'
    TableName = 'ADCheck.DB'
    Left = 240
    Top = 88
    object taDCheckUNI: TIntegerField
      FieldName = 'UNI'
    end
    object taDCheckSys_Num: TIntegerField
      FieldName = 'Sys_Num'
    end
    object taDCheckCnum: TSmallintField
      FieldName = 'Cnum'
    end
    object taDCheckSifr: TSmallintField
      FieldName = 'Sifr'
    end
    object taDCheckSum: TFloatField
      FieldName = 'Sum'
    end
    object taDCheckCardCod: TFloatField
      FieldName = 'CardCod'
    end
    object taDCheckPerson: TSmallintField
      FieldName = 'Person'
    end
    object taDCheckSumR: TFloatField
      FieldName = 'SumR'
    end
  end
  object taPCheck: TTable
    DatabaseName = 'C:\Projects\Casher\Rn\BIN\RkData\'
    SessionName = 'Session1_1'
    TableName = 'APcheck.DB'
    Left = 104
    Top = 152
  end
  object taVCheck: TTable
    DatabaseName = 'C:\Projects\Casher\Rn\BIN\RkData\'
    SessionName = 'Session1_1'
    TableName = 'AVCheck.DB'
    Left = 168
    Top = 152
    object taVCheckUNI: TAutoIncField
      FieldName = 'UNI'
      ReadOnly = True
    end
    object taVCheckLogicDate: TDateField
      FieldName = 'LogicDate'
    end
    object taVCheckRealDate: TDateField
      FieldName = 'RealDate'
    end
    object taVCheckTime: TStringField
      FieldName = 'Time'
      Size = 5
    end
    object taVCheckSifr: TSmallintField
      FieldName = 'Sifr'
    end
    object taVCheckComp: TSmallintField
      FieldName = 'Comp'
    end
    object taVCheckQnt: TFloatField
      FieldName = 'Qnt'
    end
    object taVCheckPrice: TFloatField
      FieldName = 'Price'
    end
    object taVCheckReason: TSmallintField
      FieldName = 'Reason'
    end
    object taVCheckManager: TSmallintField
      FieldName = 'Manager'
    end
    object taVCheckWaiter: TSmallintField
      FieldName = 'Waiter'
    end
    object taVCheckTable: TStringField
      FieldName = 'Table'
      Size = 4
    end
    object taVCheckUnit: TStringField
      FieldName = 'Unit'
      Size = 2
    end
    object taVCheckDepart: TStringField
      FieldName = 'Depart'
      Size = 2
    end
    object taVCheckTabNum: TFloatField
      FieldName = 'TabNum'
    end
  end
  object taCashSail: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT'
      
        '    C.ID,C.CASHNUM,C.ZNUM,C.CHECKNUM,C.TAB_ID,C.TABSUM,C.CASHERI' +
        'D,C.WAITERID,C.CHDATE,'
      '    T.BEGTIME,T.NUMTABLE,T.QUESTS,T.DISCONT'
      'FROM'
      '    CASHSAIL C'
      ''
      'LEFT JOIN TABLES_ALL T ON T.ID=C.TAB_ID'
      ''
      'WHERE C.ID>=:IDBEG'
      ''
      'ORDER BY C.ID')
    Transaction = trSelect
    Database = dmC
    UpdateTransaction = trUpdate
    Left = 32
    Top = 224
    poAskRecordCount = True
    object taCashSailCASHNUM: TFIBIntegerField
      FieldName = 'CASHNUM'
    end
    object taCashSailZNUM: TFIBIntegerField
      FieldName = 'ZNUM'
    end
    object taCashSailCHECKNUM: TFIBIntegerField
      FieldName = 'CHECKNUM'
    end
    object taCashSailTAB_ID: TFIBIntegerField
      FieldName = 'TAB_ID'
    end
    object taCashSailTABSUM: TFIBFloatField
      FieldName = 'TABSUM'
    end
    object taCashSailCASHERID: TFIBIntegerField
      FieldName = 'CASHERID'
    end
    object taCashSailWAITERID: TFIBIntegerField
      FieldName = 'WAITERID'
    end
    object taCashSailCHDATE: TFIBDateTimeField
      FieldName = 'CHDATE'
    end
    object taCashSailBEGTIME: TFIBDateTimeField
      FieldName = 'BEGTIME'
    end
    object taCashSailNUMTABLE: TFIBStringField
      FieldName = 'NUMTABLE'
      EmptyStrToNull = True
    end
    object taCashSailQUESTS: TFIBIntegerField
      FieldName = 'QUESTS'
    end
    object taCashSailID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taCashSailDISCONT: TFIBStringField
      FieldName = 'DISCONT'
      EmptyStrToNull = True
    end
  end
  object Timer2: TTimer
    Enabled = False
    Interval = 500
    OnTimer = Timer2Timer
    Left = 256
    Top = 160
  end
  object taSpec: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT'
      '    ID_TAB,'
      '    ID,'
      '    ID_PERSONAL,'
      '    NUMTABLE,'
      '    SIFR,'
      '    PRICE,'
      '    QUANTITY,'
      '    DISCOUNTPROC,'
      '    DISCOUNTSUM,'
      '    SUMMA,'
      '    ISTATUS,'
      '    ITYPE'
      'FROM'
      '    SPEC_ALL '
      'WHERE'
      '    ID_TAB=:IDHEAD'
      ''
      'ORDER BY ID')
    Transaction = trSelect
    Database = dmC
    UpdateTransaction = trUpdate
    Left = 104
    Top = 224
    object taSpecID_TAB: TFIBIntegerField
      FieldName = 'ID_TAB'
    end
    object taSpecID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taSpecID_PERSONAL: TFIBIntegerField
      FieldName = 'ID_PERSONAL'
    end
    object taSpecNUMTABLE: TFIBStringField
      FieldName = 'NUMTABLE'
      EmptyStrToNull = True
    end
    object taSpecSIFR: TFIBIntegerField
      FieldName = 'SIFR'
    end
    object taSpecPRICE: TFIBFloatField
      FieldName = 'PRICE'
    end
    object taSpecQUANTITY: TFIBFloatField
      FieldName = 'QUANTITY'
    end
    object taSpecDISCOUNTPROC: TFIBFloatField
      FieldName = 'DISCOUNTPROC'
    end
    object taSpecDISCOUNTSUM: TFIBFloatField
      FieldName = 'DISCOUNTSUM'
    end
    object taSpecSUMMA: TFIBFloatField
      FieldName = 'SUMMA'
    end
    object taSpecISTATUS: TFIBIntegerField
      FieldName = 'ISTATUS'
    end
    object taSpecITYPE: TFIBSmallIntField
      FieldName = 'ITYPE'
    end
  end
  object taDel: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    ID_PERSONAL,'
      '    NUMTABLE,'
      '    QUESTS,'
      '    TABSUM,'
      '    BEGTIME,'
      '    ENDTIME,'
      '    DISCONT,'
      '    OPERTYPE,'
      '    CHECKNUM,'
      '    SKLAD'
      'FROM'
      '    TABLES_ALL '
      'where ID>=:IDBEG'
      'and OPERTYPE='#39'Del'#39
      ''
      'order by ID')
    Transaction = trSelect
    Database = dmC
    UpdateTransaction = trUpdate
    Left = 176
    Top = 224
    object taDelID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taDelID_PERSONAL: TFIBIntegerField
      FieldName = 'ID_PERSONAL'
    end
    object taDelNUMTABLE: TFIBStringField
      FieldName = 'NUMTABLE'
      EmptyStrToNull = True
    end
    object taDelQUESTS: TFIBIntegerField
      FieldName = 'QUESTS'
    end
    object taDelTABSUM: TFIBFloatField
      FieldName = 'TABSUM'
    end
    object taDelBEGTIME: TFIBDateTimeField
      FieldName = 'BEGTIME'
    end
    object taDelENDTIME: TFIBDateTimeField
      FieldName = 'ENDTIME'
    end
    object taDelDISCONT: TFIBStringField
      FieldName = 'DISCONT'
      EmptyStrToNull = True
    end
    object taDelOPERTYPE: TFIBStringField
      FieldName = 'OPERTYPE'
      Size = 10
      EmptyStrToNull = True
    end
    object taDelCHECKNUM: TFIBIntegerField
      FieldName = 'CHECKNUM'
    end
    object taDelSKLAD: TFIBSmallIntField
      FieldName = 'SKLAD'
    end
  end
end
