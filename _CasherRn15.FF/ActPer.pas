unit ActPer;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SpeedBar, ExtCtrls, ComCtrls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Placemnt, cxImageComboBox, XPStyleActnCtrls, ActnList, ActnMan, Menus,
  FR_DSet, FR_DBSet, FR_Class, cxContainer, cxTextEdit, cxMemo;

type
  TfmDocsActs = class(TForm)
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    Timer1: TTimer;
    FormPlacement1: TFormPlacement;
    GridActs: TcxGrid;
    ViewActs: TcxGridDBTableView;
    LevelActs: TcxGridLevel;
    ViewActsID: TcxGridDBColumn;
    ViewActsDATEDOC: TcxGridDBColumn;
    ViewActsNUMDOC: TcxGridDBColumn;
    ViewActsIDSKL: TcxGridDBColumn;
    ViewActsSUMIN: TcxGridDBColumn;
    ViewActsSUMUCH: TcxGridDBColumn;
    ViewActsNAMEMH: TcxGridDBColumn;
    ViewActsIACTIVE: TcxGridDBColumn;
    amDocsIn: TActionManager;
    acPeriod: TAction;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    SpeedItem6: TSpeedItem;
    acAddDoc1: TAction;
    acEditAct: TAction;
    acViewAct: TAction;
    acDelDoc1: TAction;
    acOnDoc1: TAction;
    acOffDoc1: TAction;
    SpeedItem7: TSpeedItem;
    SpeedItem8: TSpeedItem;
    acVid: TAction;
    SpeedItem9: TSpeedItem;
    LevelCardsActs: TcxGridLevel;
    ViewCardsActs: TcxGridDBTableView;
    ViewCardsActsNAME: TcxGridDBColumn;
    ViewCardsActsNAMESHORT: TcxGridDBColumn;
    ViewCardsActsIDCARD: TcxGridDBColumn;
    ViewCardsActsQUANT: TcxGridDBColumn;
    ViewCardsActsPRICEIN: TcxGridDBColumn;
    ViewCardsActsSUMIN: TcxGridDBColumn;
    ViewCardsActsPRICEUCH: TcxGridDBColumn;
    ViewCardsActsSUMUCH: TcxGridDBColumn;
    ViewCardsActsIDNDS: TcxGridDBColumn;
    ViewCardsActsSUMNDS: TcxGridDBColumn;
    ViewCardsActsDATEDOC: TcxGridDBColumn;
    ViewCardsActsNUMDOC: TcxGridDBColumn;
    ViewCardsActsNAMECL: TcxGridDBColumn;
    ViewCardsActsNAMEMH: TcxGridDBColumn;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    acCopy: TAction;
    acInsertD: TAction;
    N5: TMenuItem;
    N6: TMenuItem;
    acPrint: TAction;
    frRepA: TfrReport;
    frquSpecAO: TfrDBDataSet;
    frquSpecAI: TfrDBDataSet;
    ViewActsCOMMENT: TcxGridDBColumn;
    Memo1: TcxMemo;
    Excel1: TMenuItem;
    procedure SpeedItem1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acPeriodExecute(Sender: TObject);
    procedure acAddDoc1Execute(Sender: TObject);
    procedure acEditActExecute(Sender: TObject);
    procedure acViewActExecute(Sender: TObject);
    procedure ViewActsDblClick(Sender: TObject);
    procedure acDelDoc1Execute(Sender: TObject);
    procedure acOnDoc1Execute(Sender: TObject);
    procedure acOffDoc1Execute(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure acVidExecute(Sender: TObject);
    procedure SpeedItem1Click0(Sender: TObject);
    procedure acCopyExecute(Sender: TObject);
    procedure acInsertDExecute(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Excel1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure prPriceIn(Idh:INteger);
    procedure prPriceOut(Idh,IdSkl,iDate:INteger);
    procedure prPartIn(Idh,IdSkl,iDate:INteger;Var rSumO:Real);
  end;

var
  fmDocsActs: TfmDocsActs;
  bClearActs:Boolean = false;

implementation

uses Un1, dmOffice, PeriodUni, AddDoc1, DMOReps, TBuff, AddAct,
  MainRnOffice;

{$R *.dfm}
procedure TfmDocsActs.prPartIn(Idh,IdSkl,iDate:INteger;Var rSumO:Real);
Var PriceSp,PriceUch,rQs:Real;
begin
  with dmO do
  with dmORep do
  begin
    quSpecAI.Active:=False;
    quSpecAI.ParamByName('IDH').AsInteger:=IDH;
    quSpecAI.Active:=True;

    rSumO:=0;
    //������
    quSpecAI.First;
    while not quSpecAI.Eof do
    begin
      rQs:=quSpecAIQUANT.AsFloat*quSpecAIKM.AsFloat; //�������� � ��������

      prAddPartIn1.ParamByName('IDSKL').AsInteger:=IdSkl;
      prAddPartIn1.ParamByName('IDDOC').AsInteger:=IdH;
      prAddPartIn1.ParamByName('DTYPE').AsInteger:=5;
      prAddPartIn1.ParamByName('IDATE').AsInteger:=iDate;
      prAddPartIn1.ParamByName('IDCARD').AsInteger:=quSpecAIIDCARD.AsInteger;
      prAddPartIn1.ParamByName('IDCLI').AsInteger:=(-1)*IdSkl;
      prAddPartIn1.ParamByName('QUANT').AsFloat:=rQs;
      PriceSp:=0;
      PriceUch:=0;
      if quSpecAIKM.AsFloat<>0 then
      begin
        PriceSp:=quSpecAIPRICEIN.AsFloat/quSpecAIKM.AsFloat;   //���� � ������� ��
        PriceUch:=quSpecAIPRICEINUCH.AsFloat/quSpecAIKM.AsFloat; //���� � ������� ��
      end;

      prAddPartIn1.ParamByName('PRICEIN').AsFloat:=PriceSp;
      prAddPartIn1.ParamByName('PRICEUCH').AsFloat:=PriceUch;
      prAddPartIn1.ExecProc;

      rSumO:=rSumO+quSpecAISUMIN.AsFloat;
      quSpecAI.Next;
    end;
    quSpecAI.Active:=False;
  end;
end;

procedure TfmDocsActs.prPriceOut(Idh,IdSkl,iDate:INteger);
Var PriceSp,PriceUch,rSumIn,rSumUch,rQs,rQ,rQp,rMessure:Real;
begin
  with dmO do
  with dmORep do
  begin
    quSpecAO.Active:=False;
    quSpecAO.ParamByName('IDH').AsInteger:=IDH;
    quSpecAO.Active:=True;

    quSpecAO.First;
    while not quSpecAO.Eof do
    begin

      PriceSp:=0;
      PriceUch:=0;
      rSumIn:=0;
      rSumUch:=0;

      rQs:=quSpecAOQUANT.AsFloat*quSpecAOKM.AsFloat; //�������� � ��������
      prSelPartIn(quSpecAOIDCARD.AsInteger,IdSkl,0,0);
      quSelPartIn.First;
      if rQs>0 then
      begin
        while (not quSelPartIn.Eof) and (rQs>0) do
        begin
         //���� �� ���� ������� ���� �����, ��������� �������� ���
          rQp:=quSelPartInQREMN.AsFloat;
          if rQs<=rQp then  rQ:=rQs//��������� ������ ������ ���������
                      else  rQ:=rQp;
          rQs:=rQs-rQ;
          PriceSp:=quSelPartInPRICEIN.AsFloat;
          PriceUch:=quSelPartInPRICEOUT.AsFloat;

          rSumIn:=rSumIn+PriceSp*rQ;
          rSumUch:=rSumUch+PriceUch*rQ;

          prAddPartOut.ParamByName('ARTICUL').AsInteger:=quSpecAOIDCARD.AsInteger;
          prAddPartOut.ParamByName('IDDATE').AsInteger:=iDate;
          prAddPartOut.ParamByName('IDSTORE').AsInteger:=IdSkl;
          prAddPartOut.ParamByName('IDPARTIN').AsInteger:=quSelPartInID.AsInteger;
          prAddPartOut.ParamByName('IDDOC').AsInteger:=IDH;
          prAddPartOut.ParamByName('IDCLI').AsInteger:=quSelPartInIDCLI.AsInteger;
          prAddPartOut.ParamByName('DTYPE').AsInteger:=5;
          prAddPartOut.ParamByName('QUANT').AsFloat:=rQ;
          prAddPartOut.ParamByName('PRICEIN').AsFloat:=PriceSp;
          prAddPartOut.ParamByName('SUMOUT').AsFloat:=PriceUch*rQ;
          prAddPartout.ExecProc;

          quSelPartIn.Next;
        end;

        if rQs>0 then //�������� ������������� ������, �������� � ������, �� � ������� ��������� ������ ���, ��� � �������������
        begin
          if PriceSp=0 then
          begin //��� ���� ���������� ������� � ���������� ����������
            prCalcLastPrice1.ParamByName('IDGOOD').AsInteger:=quSpecAOIDCARD.AsInteger;
            prCalcLastPrice1.ExecProc;
            PriceSp:=prCalcLastPrice1.ParamByName('PRICEIN').AsFloat;
            rMessure:=prCalcLastPrice1.ParamByName('KOEF').AsFloat;
            if (rMessure<>0)and(rMessure<>1) then PriceSp:=PriceSp/rMessure;
          end;

          rSumIn:=rSumIn+PriceSp*rQs;
          rSumUch:=rSumUch+PriceUch*rQs;

          prAddPartOut.ParamByName('ARTICUL').AsInteger:=quSpecAOIDCARD.AsInteger;
          prAddPartOut.ParamByName('IDDATE').AsInteger:=iDate;
          prAddPartOut.ParamByName('IDSTORE').AsInteger:=IdSkl;
          prAddPartOut.ParamByName('IDPARTIN').AsInteger:=-1;
          prAddPartOut.ParamByName('IDDOC').AsInteger:=IdH;
          prAddPartOut.ParamByName('IDCLI').AsInteger:=0;
          prAddPartOut.ParamByName('DTYPE').AsInteger:=5;
          prAddPartOut.ParamByName('QUANT').AsFloat:=rQs;
          prAddPartOut.ParamByName('PRICEIN').AsFloat:=PriceSp;
          prAddPartOut.ParamByName('SUMOUT').AsFloat:=PriceUch*rQs;
          prAddPartout.ExecProc;
        end;
      end;
      quSelPartIn.Active:=False;

      //�������� ���������
      quSpecAO.Edit;
      if quSpecAOQuant.AsFloat<>0 then
      begin
        quSpecAOSUMIN.AsFloat:=rSumIn;
        quSpecAOSUMINUCH.AsFloat:=rSumUch;
        quSpecAOPRICEIN.AsFloat:=rSumIn/quSpecAOQuant.AsFloat;
        quSpecAOPRICEINUCH.AsFloat:=rSumUch/quSpecAOQuant.AsFloat;
      end else
      begin
        quSpecAOSUMIN.AsFloat:=0;
        quSpecAOSUMINUCH.AsFloat:=0;
        quSpecAOPRICEIN.AsFloat:=0;
        quSpecAOPRICEINUCH.AsFloat:=0;
      end;

      quSpecAO.Post;

      quSpecAO.Next; delay(10);
    end;
    quSpecAO.Active:=False;
  end;
end;


procedure TfmDocsActs.prPriceIn(Idh:INteger);
Var rSumO,rSumI:Real;
    rSumIn,rSumUch:Real;
    rQs,FullProc,rP,rSumRemn,rQ:Real;
begin
  with dmO do
  with dmORep do
  begin
   //1.������ ����� �������
    rSumO:=0; rSumI:=0;

    quSpecAO.Active:=False;
    quSpecAO.ParamByName('IDH').AsInteger:=IDH;
    quSpecAO.Active:=True;

    quSpecAO.First;
    while not quSpecAO.Eof do
    begin
      rSumO:=rSumO+quSpecAOSumIn.AsFloat;
      quSpecAO.Next;
    end;

    quSpecAI.Active:=False;
    quSpecAI.ParamByName('IDH').AsInteger:=IDH;
    quSpecAI.Active:=True;

    quSpecAI.First;
    while not quSpecAI.Eof do
    begin
      rSumI:=rSumI+quSpecAISumIn.AsFloat;
      quSpecAI.Next;
    end;

    if rSumI<>rSumO then
    begin
      rSumIn:=0;
      rSumUch:=0;

      quSpecAO.First;
      while not quSpecAO.Eof do
      begin
        rSumIn:=rSumIn+quSpecAOSumIn.AsFloat;
        rSumUch:=rSumUch+quSpecAOSumInUch.AsFloat;
        quSpecAO.Next;
      end;

      rQs:=0; FullProc:=0;
      rSumRemn:=rSumIn; //��� ����� , ��� �������� ���������� 0-��
      //2. ������ ��� ���-�� ������� � ������ ������� ����
      quSpecAI.First;
      while not quSpecAI.Eof do
      begin
        FullProc:=FullProc+quSpecAIProcPrice.AsFloat;
        rQs:=rQs+quSpecAIQuant.AsFloat*quSpecAIKm.AsFloat;
        quSpecAI.Next;
      end;

      if FullProc=0 then
      begin   //������������� �� ���-��
        quSpecAI.First;
        while not quSpecAI.Eof do
        begin
          rQ:=quSpecAIQuant.AsFloat*quSpecAIKm.AsFloat;
          quSpecAI.Edit;
          if (rQs<>0)and(quSpecAIKm.AsFloat<>0) then
          begin
            quSpecAISumIn.AsFloat:=RoundEx(rQ/rQs*rSumIn*100)/100;
            quSpecAISumInUch.AsFloat:=RoundEx(rQ/rQs*rSumUch*100)/100;
            quSpecAIPriceIn.AsFloat:=(RoundEx(rQ/rQs*rSumIn*100)/100)/(rQ/quSpecAIKm.AsFloat);
            quSpecAIPriceInUch.AsFloat:=(RoundEx(rQ/rQs*rSumUch*100)/100)/(rQ/quSpecAIKm.AsFloat);

            rSumRemn:=rSumRemn-RoundEx(rQ/rQs*rSumIn*100)/100;
          end else
          begin
            quSpecAISumIn.AsFloat:=0;
            quSpecAISumInUch.AsFloat:=0;
            quSpecAIPriceIn.AsFloat:=0;
            quSpecAIPriceInUch.AsFloat:=0;
          end;
          quSpecAI.Post;

          quSpecAI.Next;
        end;
      end else //������������� �� ��������
      begin
        quSpecAI.First;
        while not quSpecAI.Eof do
        begin
          rP:=quSpecAIProcPrice.AsFloat;
          rQ:=quSpecAIQuant.AsFloat*quSpecAIKm.AsFloat;
          quSpecAI.Edit;
          if (FullProc<>0)and(rQ<>0)and(quSpecAIKm.AsFloat<>0)  then
          begin
            quSpecAISumIn.AsFloat:=RoundEx(rP/FullProc*rSumIn*100)/100;
            quSpecAISumInUch.AsFloat:=RoundEx(rP/FullProc*rSumUch*100)/100;
            quSpecAIPriceIn.AsFloat:=(RoundEx(rP/FullProc*rSumIn*100)/100)/(rQ/quSpecAIKm.AsFloat);
            quSpecAIPriceInUch.AsFloat:=(RoundEx(rP/FullProc*rSumUch*100)/100)/(rQ/quSpecAIKm.AsFloat);

            rSumRemn:=rSumRemn-RoundEx(rP/FullProc*rSumIn*100)/100;
          end else
          begin
            quSpecAISumIn.AsFloat:=0;
            quSpecAISumInUch.AsFloat:=0;
            quSpecAIPriceIn.AsFloat:=0;
            quSpecAIPriceInUch.AsFloat:=0;
          end;
          quSpecAI.Post;

          quSpecAI.Next;
        end;
      end;

      if rSumRemn<>0 then
      begin //�������� ���������������� �����-�� �����
        quSpecAI.Last;
        while (quSpecAI.Bof=False)and(rSumRemn<>0) do
        begin
          if quSpecAIQuant.AsFloat<>0 then
          begin
            rSumRemn:=rSumRemn+quSpecAISumIn.AsFloat;
            rQ:=quSpecAIQuant.AsFloat;
            quSpecAI.Edit;
            quSpecAISumIn.AsFloat:=rSumRemn;
            quSpecAIPriceIn.AsFloat:=rSumRemn/rQ;
            quSpecAI.Post;
            rSumRemn:=0;
          end;
          quSpecAI.Prior;
        end;
      end;
    end;
    quSpecAO.Active:=False;
    quSpecAI.Active:=False;
  end;
end;



procedure TfmDocsActs.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmDocsActs.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  Timer1.Enabled:=True;
  GridActs.Align:=AlClient;
  ViewActs.RestoreFromIniFile(CurDir+GridIni);
  ViewCardsActs.RestoreFromIniFile(CurDir+GridIni);
  StatusBar1.Color:=$00FFCACA;
end;

procedure TfmDocsActs.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewActs.StoreToIniFile(CurDir+GridIni,False);
  ViewCardsActs.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmDocsActs.acPeriodExecute(Sender: TObject);
begin
//  ������
  fmPeriodUni.DateTimePicker1.Date:=CommonSet.DateFrom;
//  fmPeriodUni.DateTimePicker2.Date:=CommonSet.DateTo-1;
  fmPeriodUni.ShowModal;
  if fmPeriodUni.ModalResult=mrOk then
  begin
    CommonSet.DateFrom:=Trunc(fmPeriodUni.DateTimePicker1.Date);
    CommonSet.DateTo:=Trunc(fmPeriodUni.DateTimePicker2.Date)+1;

    with dmO do
    with dmORep do
    begin
      if LevelActs.Visible then
      begin
        if CommonSet.DateTo>=iMaxDate then fmDocsActs.Caption:='���� ����������� � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
        else fmDocsActs.Caption:='���� ����������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);

        ViewActs.BeginUpdate;
        quDocsActs.Active:=False;
        quDocsActs.ParamByName('DATEB').AsDate:=CommonSet.DateFrom;
        quDocsActs.ParamByName('DATEE').AsDate:=CommonSet.DateTo;
        quDocsActs.Active:=True;
        ViewActs.EndUpdate;
      end else
      begin
        if CommonSet.DateTo>=iMaxDate then fmDocsActs.Caption:='���� ����������� �� ������� � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
        else fmDocsActs.Caption:='���� ����������� �� ������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);

        ViewCardsActs.BeginUpdate;
{        quDocsInCard.Active:=False;
        quDocsInCard.ParamByName('DATEB').AsDate:=CommonSet.DateFrom;
        quDocsInCard.ParamByName('DATEE').AsDate:=CommonSet.DateTo;
        quDocsInCard.Active:=True;}
        ViewCardsActs.EndUpdate;
      end;
    end;
  end;
end;

procedure TfmDocsActs.acAddDoc1Execute(Sender: TObject);
begin
  //�������� ��������
  if not CanDo('prAddAct') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmO do
  with dmORep do
  begin
    fmAddAct.Caption:='���� �����������: ����� ��������.';
    fmAddAct.cxTextEdit1.Text:=prGetNum(5,0);
    fmAddAct.cxTextEdit1.Properties.ReadOnly:=False;
    fmAddAct.cxTextEdit1.Tag:=0; //������� ���������� ���������
    fmAddAct.cxDateEdit1.Date:=Date;
    fmAddAct.cxDateEdit1.Properties.ReadOnly:=False;
    fmAddAct.cxTextEdit2.Text:='';
    fmAddAct.cxTextEdit2.Properties.ReadOnly:=False;

    if quMHAll.Active=False then quMHAll.Active:=True;
    quMHAll.FullRefresh;

    fmAddAct.cxLookupComboBox1.EditValue:=0;
    fmAddAct.cxLookupComboBox1.Text:='';
    fmAddAct.cxLookupComboBox1.Properties.ReadOnly:=False;

    if CurVal.IdMH<>0 then
    begin
      fmAddAct.cxLookupComboBox1.EditValue:=CurVal.IdMH;
      fmAddAct.cxLookupComboBox1.Text:=CurVal.NAMEMH;
    end else
    begin
       quMHAll.First;
       if not quMHAll.Eof then
       begin
         CurVal.IdMH:=quMHAllID.AsInteger;
         CurVal.NAMEMH:=quMHAllNAMEMH.AsString;
         fmAddAct.cxLookupComboBox1.EditValue:=CurVal.IdMH;
         fmAddAct.cxLookupComboBox1.Text:=CurVal.NAMEMH;
       end;
    end;
    if quMHAll.Locate('ID',CurVal.IdMH,[]) then
    begin
      fmAddAct.Label15.Caption:='��. ����: '+quMHAllNAMEPRICE.AsString;
      fmAddAct.Label15.Tag:=quMHAllDEFPRICE.AsInteger;
    end else
    begin
      fmAddAct.Label15.Caption:='��. ����: ';
      fmAddAct.Label15.Tag:=0;
    end;

    fmAddAct.cxLabel1.Enabled:=True;
    fmAddAct.cxLabel2.Enabled:=True;
    fmAddAct.cxLabel7.Enabled:=True;
    fmAddAct.cxLabel9.Enabled:=True;
    fmAddAct.Label3.Enabled:=True;

    fmAddAct.cxButton1.Enabled:=True;
    CloseTa(fmAddAct.taSpecO);
    CloseTa(fmAddAct.taSpecI);

    fmAddAct.ViewAO.OptionsData.Editing:=True;
    fmAddAct.ViewAI.OptionsData.Editing:=True;

    fmAddAct.Show;
  end;
end;

procedure TfmDocsActs.acEditActExecute(Sender: TObject);
Var IDH:INteger;
//    rSum1,rSum2:Real;
begin
  //�������������
  if not CanDo('prEditAct') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmO do
  with dmORep do
  begin
    if quDocsActs.RecordCount>0 then //���� ��� �������������
    begin
      if quDocsActsIACTIVE.AsInteger=0 then
      begin
        prAllViewOff;

        fmAddAct.Caption:='���� �����������: ��������������.';
        fmAddAct.cxTextEdit1.Text:=quDocsActsNUMDOC.AsString;
        fmAddAct.cxTextEdit1.Properties.ReadOnly:=False;
        fmAddAct.cxTextEdit1.Tag:=quDocsActsID2.AsInteger; //������� ���������� ��������� ���� 0

        fmAddAct.cxTextEdit2.Text:=quDocsActsCOMMENT.AsString;
        fmAddAct.cxTextEdit2.Properties.ReadOnly:=False;

        fmAddAct.cxDateEdit1.Date:=quDocsActsDATEDOC.AsDateTime;
        fmAddAct.cxDateEdit1.Properties.ReadOnly:=False;

        if quMHAll.Active=False then quMHAll.Active:=True;
        quMHAll.FullRefresh;

        fmAddAct.cxLookupComboBox1.EditValue:=quDocsActsIDSKL.asinteger;
        fmAddAct.cxLookupComboBox1.Text:=quDocsActsNAMEMH.AsString;
        fmAddAct.cxLookupComboBox1.Properties.ReadOnly:=False;

        CurVal.IdMH:=quDocsActsIDSKL.asinteger;
        CurVal.NAMEMH:=quDocsActsNAMEMH.AsString;

        fmAddAct.cxLabel1.Enabled:=True;
        fmAddAct.cxLabel2.Enabled:=True;
        fmAddAct.cxLabel7.Enabled:=True;
        fmAddAct.cxLabel9.Enabled:=True;
        fmAddAct.Label3.Enabled:=True;

        fmAddAct.cxButton1.Enabled:=True;

        CloseTa(fmAddAct.taSpecO);
        CloseTa(fmAddAct.taSpecI);

        IDH:=quDocsActsID2.AsInteger;

        quSpecAO.Active:=False;
        quSpecAO.ParamByName('IDH').AsInteger:=IDH;
        quSpecAO.Active:=True;

        quSpecAO.First;
        while not quSpecAO.Eof do
        begin
          with fmAddAct do
          begin
            taSpecO.Append;
            taSpecONum.AsInteger:=quSpecAOID.AsInteger;
            taSpecOIdGoods.AsInteger:=quSpecAOIDCARD.AsInteger;
            taSpecONameG.AsString:=quSpecAONAME.AsString;
            taSpecOIM.AsInteger:=quSpecAOIDM.AsInteger;
            taSpecOSM.AsString:=quSpecAONAMESHORT.AsString;
            taSpecOQuant.AsFloat:=quSpecAOQUANT.AsFloat;
            taSpecOPriceIn.AsFloat:=quSpecAOPRICEIN.AsFloat;
            taSpecOSumIn.AsFloat:=quSpecAOSUMIN.AsFloat;
            taSpecOPriceUch.AsFloat:=quSpecAOPRICEINUCH.AsFloat;
            taSpecOSumUch.AsFloat:=quSpecAOSUMINUCH.AsFloat;
            taSpecOKm.AsFloat:=quSpecAOKM.AsFloat;
            taSpecO.Post;
          end;
          quSpecAO.Next;
        end;

        quSpecAO.Active:=False;

        quSpecAI.Active:=False;
        quSpecAI.ParamByName('IDH').AsInteger:=IDH;
        quSpecAI.Active:=True;

        quSpecAI.First;
        while not quSpecAI.Eof do
        begin
          with fmAddAct do
          begin
            taSpecI.Append;
            taSpecINum.AsInteger:=quSpecAIID.AsInteger;
            taSpecIIdGoods.AsInteger:=quSpecAIIDCARD.AsInteger;
            taSpecINameG.AsString:=quSpecAINAME.AsString;
            taSpecIIM.AsInteger:=quSpecAIIDM.AsInteger;
            taSpecISM.AsString:=quSpecAINAMESHORT.AsString;
            taSpecIQuant.AsFloat:=quSpecAIQUANT.AsFloat;
            taSpecIPriceIn.AsFloat:=quSpecAIPRICEIN.AsFloat;
            taSpecISumIn.AsFloat:=quSpecAISUMIN.AsFloat;
            taSpecIPriceUch.AsFloat:=quSpecAIPRICEINUCH.AsFloat;
            taSpecISumUch.AsFloat:=quSpecAISUMINUCH.AsFloat;
            taSpecIKm.AsFloat:=quSpecAIKM.AsFloat;
            taSpecIProcPrice.AsFloat:=quSpecAIProcPrice.AsFloat;
            taSpecI.Post;
          end;
          quSpecAI.Next;
        end;

        quSpecAI.Active:=False;

        fmAddAct.ViewAO.OptionsData.Editing:=True;
        fmAddAct.ViewAI.OptionsData.Editing:=True;

        prAllViewOn;

        fmAddAct.ShowModal;

      end else
      begin
        showmessage('������������� �������������� �������� ������.');
      end;
    end else
    begin
      showmessage('�������� �������� ��� ��������������.');
    end;
  end;
end;

procedure TfmDocsActs.acViewActExecute(Sender: TObject);
Var IDH:INteger;
//    rSum1,rSum2:Real;
begin
  //�������������
  if not CanDo('prViewAct') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmO do
  with dmORep do
  begin
    if quDocsActs.RecordCount>0 then //���� ��� ��������
    begin
      prAllViewOff;

      fmAddAct.Caption:='���� �����������: ��������.';
      fmAddAct.cxTextEdit1.Text:=quDocsActsNUMDOC.AsString;
      fmAddAct.cxTextEdit1.Properties.ReadOnly:=True;
      fmAddAct.cxTextEdit1.Tag:=quDocsActsID2.AsInteger; //������� ���������� ��������� ���� 0

      fmAddAct.cxDateEdit1.Date:=quDocsActsDATEDOC.AsDateTime;
      fmAddAct.cxDateEdit1.Properties.ReadOnly:=True;

      fmAddAct.cxTextEdit2.Text:=quDocsActsCOMMENT.AsString;
      fmAddAct.cxTextEdit2.Properties.ReadOnly:=True;

      if quMHAll.Active=False then quMHAll.Active:=True;
      quMHAll.FullRefresh;

      fmAddAct.cxLookupComboBox1.EditValue:=quDocsActsIDSKL.asinteger;
      fmAddAct.cxLookupComboBox1.Text:=quDocsActsNAMEMH.AsString;
      fmAddAct.cxLookupComboBox1.Properties.ReadOnly:=True;

      CurVal.IdMH:=quDocsActsIDSKL.asinteger;
      CurVal.NAMEMH:=quDocsActsNAMEMH.AsString;

      fmAddAct.cxLabel1.Enabled:=False;
      fmAddAct.cxLabel2.Enabled:=False;
      fmAddAct.cxLabel7.Enabled:=False;
      fmAddAct.cxLabel9.Enabled:=False;
      fmAddAct.Label3.Enabled:=False;

      fmAddAct.cxButton1.Enabled:=False;

      CloseTa(fmAddAct.taSpecO);
      CloseTa(fmAddAct.taSpecI);

      IDH:=quDocsActsID2.AsInteger;

      quSpecAO.Active:=False;
      quSpecAO.ParamByName('IDH').AsInteger:=IDH;
      quSpecAO.Active:=True;

      quSpecAO.First;
      while not quSpecAO.Eof do
      begin
        with fmAddAct do
        begin
          taSpecO.Append;
          taSpecONum.AsInteger:=quSpecAOID.AsInteger;
          taSpecOIdGoods.AsInteger:=quSpecAOIDCARD.AsInteger;
          taSpecONameG.AsString:=quSpecAONAME.AsString;
          taSpecOIM.AsInteger:=quSpecAOIDM.AsInteger;
          taSpecOSM.AsString:=quSpecAONAMESHORT.AsString;
          taSpecOQuant.AsFloat:=quSpecAOQUANT.AsFloat;
          taSpecOPriceIn.AsFloat:=quSpecAOPRICEIN.AsFloat;
          taSpecOSumIn.AsFloat:=quSpecAOSUMIN.AsFloat;
          taSpecOPriceUch.AsFloat:=quSpecAOPRICEINUCH.AsFloat;
          taSpecOSumUch.AsFloat:=quSpecAOSUMINUCH.AsFloat;
          taSpecOKm.AsFloat:=quSpecAOKM.AsFloat;
          taSpecO.Post;
        end;
        quSpecAO.Next;
      end;

      quSpecAO.Active:=False;

      quSpecAI.Active:=False;
      quSpecAI.ParamByName('IDH').AsInteger:=IDH;
      quSpecAI.Active:=True;

      quSpecAI.First;
      while not quSpecAI.Eof do
      begin
        with fmAddAct do
        begin
          taSpecI.Append;
          taSpecINum.AsInteger:=quSpecAIID.AsInteger;
          taSpecIIdGoods.AsInteger:=quSpecAIIDCARD.AsInteger;
          taSpecINameG.AsString:=quSpecAINAME.AsString;
          taSpecIIM.AsInteger:=quSpecAIIDM.AsInteger;
          taSpecISM.AsString:=quSpecAINAMESHORT.AsString;
          taSpecIQuant.AsFloat:=quSpecAIQUANT.AsFloat;
          taSpecIPriceIn.AsFloat:=quSpecAIPRICEIN.AsFloat;
          taSpecISumIn.AsFloat:=quSpecAISUMIN.AsFloat;
          taSpecIPriceUch.AsFloat:=quSpecAIPRICEINUCH.AsFloat;
          taSpecISumUch.AsFloat:=quSpecAISUMINUCH.AsFloat;
          taSpecIKm.AsFloat:=quSpecAIKM.AsFloat;
          taSpecIProcPrice.AsFloat:=quSpecAIProcPrice.AsFloat;
          taSpecI.Post;
        end;
        quSpecAI.Next;
      end;

      quSpecAI.Active:=False;

      fmAddAct.ViewAO.OptionsData.Editing:=False;
      fmAddAct.ViewAI.OptionsData.Editing:=False;

      prAllViewOn;

      fmAddAct.ShowModal;
    end else
    begin
      showmessage('�������� �������� ��� ���������.');
    end;
  end;
end;

procedure TfmDocsActs.ViewActsDblClick(Sender: TObject);
begin
  //������� �������
  with dmORep do
  begin
    if quDocsActsIACTIVE.AsInteger=0 then acEditAct.Execute //��������������
    else acViewAct.Execute; //��������
  end;
end;

procedure TfmDocsActs.acDelDoc1Execute(Sender: TObject);
begin
  //������� ��������
  if not CanDo('prDelAct') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmO do
  with dmORep do
  begin
    if quDocsActs.RecordCount>0 then //���� ��� �������������
    begin
      if quDocsActsIACTIVE.AsInteger=0 then
      begin
        if MessageDlg('�� ������������� ������ ������� �������� �'+quDocsActsNUMDOC.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          quDocsActs.Delete;
        end;
      end else
      begin
        showmessage('������� �������������� �������� ������.');
      end;
    end else
    begin
      showmessage('�������� �������� ��� ��������.');
    end;
  end;
end;

procedure TfmDocsActs.acOnDoc1Execute(Sender: TObject);
Var IdH:INteger;
    rSumO:Real;
begin
//������������
  with dmO do
  with dmORep do
  begin
    if not CanDo('prOnAct') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
    if not CanEdit(Trunc(quDocsActsDATEDOC.AsDateTime)) then begin StatusBar1.Panels[0].Text:='������ ������.'; exit; end;
  //��� ���������
    if quDocsActs.RecordCount>0 then //���� ��� ��������
    begin
      if MessageDlg('������������ �������� �'+quDocsActsNUMDOC.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        IDH:=quDocsActsID2.AsInteger;
        rSumO:=0;

        //������
        if prTOFind(Trunc(quDocsActsDATEDOC.AsDateTime),quDocsActsIDSKL.AsInteger)=1
        then
        begin //�� ����
          if MessageDlg('������� ��� �������� ������ �� �� '+quDocsActsNAMEMH.AsString+'  � '+FormatDateTime('dd.mm.yyyy',quDocsActsDATEDOC.AsDateTime)+'?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
            prTODel(Trunc(quDocsActsDATEDOC.AsDateTime),quDocsActsIDSKL.AsInteger)
          else
          begin
            showmessage('��������� ������� ��������� ����������...');
            exit;
          end;
        end;

        //������� ������ - ����� ������
        prPriceOut(Idh,quDocsActsIDSKL.AsInteger,trunc(quDocsActsDATEDOC.AsDateTime)); //�������� ������ � ������������ ���� ������
        prPriceIn(IDH);//�������� ���� ������� � �������
        prPartIn(IDH,quDocsActsIDSKL.AsInteger,trunc(quDocsActsDATEDOC.AsDateTime),rSumO); // ������������ ��������� ������

        //�������� ������ � �����
        quDocsActs.Edit;
        quDocsActsIACTIVE.AsInteger:=1;
        quDocsActsSUMIN.AsFloat:=rSumO;
        quDocsActs.Post;
        quDocsActs.Refresh;

        quSpecAO.Active:=False;
        quSpecAI.Active:=False;
      end;
    end;
  end;
end;

procedure TfmDocsActs.acOffDoc1Execute(Sender: TObject);
Var iCountPartOut:Integer;
    bStart:Boolean;
begin
//��������
  //��� ���������
  with dmO do
  with dmORep do
  begin
    if not CanDo('prOffAct') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
    if not CanEdit(Trunc(quDocsActsDATEDOC.AsDateTime)) then begin StatusBar1.Panels[0].Text:='������ ������.'; exit; end;

    if quDocsActs.RecordCount>0 then //���� ��� ������������
    begin
      if quDocsActsIACTIVE.AsInteger=1 then
      begin
        if MessageDlg('�������� �������� �'+quDocsActsNUMDOC.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin

          if prTOFind(Trunc(quDocsActsDATEDOC.AsDateTime),quDocsActsIDSKL.AsInteger)=1 then
          begin //�� ����
            if MessageDlg('������� ��� �������� ������ �� �� '+quDocsActsNAMEMH.AsString+'  � '+FormatDateTime('dd.mm.yyyy',quDocsActsDATEDOC.AsDateTime)+'?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
              prTODel(Trunc(quDocsActsDATEDOC.AsDateTime),quDocsActsIDSKL.AsInteger)
            else
            begin
              showmessage('��������� ������� ��������� ����������...');
              exit;
            end;
          end;

         // 1 - ��������� ���� �� �������� � ��������� ������� ������� ���������, �� ������� �  ���������� �����
         // ���� ������ ��
          prFindPartOut.ParamByName('IDDOC').AsInteger:=quDocsActsID2.AsInteger;
          prFindPartOut.ParamByName('DTYPE').AsInteger:=5;
          prFindPartOut.ExecProc;
          iCountPartOut:=prFindPartOut.ParamByName('RESULT').Value;
          if iCountPartOut>0 then
          begin
            if MessageDlg('� ������� ��������� ��������� '+IntToStr(iCountPartOut)+' ��������� ������. ����������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
            begin
              bStart:=True;
//              showmessage('�� ������� ����������� ������������� � '+FormatDateTime('dd.mm.yyyy',quDocsActsDATEDOC.AsDateTime)+' �����.');
//              bStart:=False;
            end else
            begin
              bStart:=False;
            end;
          end else bStart:=True;
          if bStart then
          begin
            // 1 - �������� ��������� ������
            // 2 - ������� ��������� ��������� ������ �� ��������� c ���������� ��������������
            prPartInDel.ParamByName('IDDOC').AsInteger:=quDocsActsID2.AsInteger;
            prPartInDel.ParamByName('DTYPE').AsInteger:=5;
            prPartInDel.ParamByName('IDATEINV').AsInteger:=Trunc(quDocsActsDATEDOC.AsDateTime);
            prPartInDel.ExecProc;

            //������� ��������� ������ �� ��������� � ���������� ��������������
            prDelPartOut.ParamByName('IDDOC').AsInteger:=quDocsActsID2.AsInteger;
            prDelPartOut.ParamByName('DTYPE').AsInteger:=5;
            prDelPartOut.ExecProc;

            // 4 - �������� ������
            quDocsActs.Edit;
            quDocsActsIACTIVE.AsInteger:=0;
            quDocsActs.Post;
            quDocsActs.Refresh;
          end;
        end;
      end;
    end;
  end;
end;

procedure TfmDocsActs.Timer1Timer(Sender: TObject);
begin
  if bClearActs=True then begin StatusBar1.Panels[0].Text:=''; bClearActs:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearActs:=True;
end;

procedure TfmDocsActs.acVidExecute(Sender: TObject);
begin
  //���
  with dmO do
  with dmORep do
  begin
{    if LevelDocsIn.Visible then
    begin
      if CommonSet.DateTo>=iMaxDate then fmDocsIn.Caption:='������� �� ������� � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
      else fmDocsIn.Caption:='������� �� ������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);

      LevelDocsIn.Visible:=False;
      LevelCards.Visible:=True;

      ViewCardsActs.BeginUpdate;
      quDocsInCard.Active:=False;
      quDocsInCard.ParamByName('DATEB').AsDate:=CommonSet.DateFrom;
      quDocsInCard.ParamByName('DATEE').AsDate:=CommonSet.DateTo;
      quDocsInCard.Active:=True;
      ViewCardsActs.EndUpdate;

      SpeedItem3.Visible:=False;
      SpeedItem4.Visible:=False;
      SpeedItem5.Visible:=False;
      SpeedItem6.Visible:=False;
      SpeedItem7.Visible:=False;
      SpeedItem8.Visible:=False;

    end else
    begin
      if CommonSet.DateTo>=iMaxDate then fmDocsIn.Caption:='���� ����������� � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
      else fmDocsIn.Caption:='���� ����������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);

      LevelDocsIn.Visible:=True;
      LevelCards.Visible:=False;

      ViewActs.BeginUpdate;
      quDocsActs.Active:=False;
      quDocsActs.ParamByName('DATEB').AsDate:=CommonSet.DateFrom;
      quDocsActs.ParamByName('DATEE').AsDate:=CommonSet.DateTo;
      quDocsActs.Active:=True;
      ViewActs.EndUpdate;

      SpeedItem3.Visible:=True;
      SpeedItem4.Visible:=True;
      SpeedItem5.Visible:=True;
      SpeedItem6.Visible:=True;
      SpeedItem7.Visible:=True;
      SpeedItem8.Visible:=True;

    end;}
  end;
end;

procedure TfmDocsActs.SpeedItem1Click0(Sender: TObject);
begin
  Close;
end;

procedure TfmDocsActs.acCopyExecute(Sender: TObject);
var Par:Variant;
    IDH:INteger;
begin
  //����������
  with dmO do
  with dmORep do
  begin
    if quDocsActs.RecordCount=0 then exit;

    taHeadDoc.Active:=False;
    taHeadDoc.FileName:=CurDir+'HeadDoc.cds';
    if FileExists(CurDir+'HeadDoc.cds') then taHeadDoc.Active:=True
    else taHeadDoc.CreateDataSet;

    taSpecDoc.Active:=False;
    taSpecDoc.FileName:=CurDir+'SpecDoc.cds';
    if FileExists(CurDir+'SpecDoc.cds') then taSpecDoc.Active:=True
    else taSpecDoc.CreateDataSet;

    taSpecDoc1.Active:=False;
    taSpecDoc1.FileName:=CurDir+'SpecDoc1.cds';
    if FileExists(CurDir+'SpecDoc1.cds') then taSpecDoc1.Active:=True
    else taSpecDoc1.CreateDataSet;


    par := VarArrayCreate([0,1], varInteger);
    par[0]:=5; //��� �����������
    par[1]:=quDocsActsID2.AsInteger;
    if taHeadDoc.Locate('IType;Id',par,[])=False then
    begin
      taHeadDoc.Append;
      taHeadDocIType.AsInteger:=5;
      taHeadDocId.AsInteger:=quDocsActsID2.AsInteger;
      taHeadDocDateDoc.AsInteger:=Trunc(quDocsActsDATEDOC.AsDateTime);
      taHeadDocNumDoc.AsString:=quDocsActsNUMDOC.AsString;
      taHeadDocIdCli.AsInteger:=0;
      taHeadDocNameCli.AsString:='';
      taHeadDocIdSkl.AsInteger:=quDocsActsIDSKL.AsInteger;
      taHeadDocNameSkl.AsString:=Copy(quDocsActsNAMEMH.AsString,1,70);
      taHeadDocSumIN.AsFloat:=quDocsActsSUMIN.AsFloat;
      taHeadDocSumUch.AsFloat:=quDocsActsSUMUCH.AsFloat;
      taHeadDoc.Post;

      IDH:=quDocsActsID2.AsInteger;

      quSpecAO.Active:=False;
      quSpecAO.ParamByName('IDH').AsInteger:=IDH;
      quSpecAO.Active:=True;

      quSpecAO.First;
      while not quSpecAO.Eof do
      begin
        taSpecDoc1.Append;
        taSpecDoc1IType.AsInteger:=5;
        taSpecDoc1IdHead.AsInteger:=IDH;
        taSpecDoc1Num.AsInteger:=quSpecAOID.AsInteger;
        taSpecDoc1IdCard.AsInteger:=quSpecAOIDCARD.AsInteger;
        taSpecDoc1Quant.AsFloat:=quSpecAOQUANT.AsFloat;
        taSpecDoc1PriceIn.AsFloat:=quSpecAOPRICEIN.AsFloat;
        taSpecDoc1SumIn.AsFloat:=quSpecAOSUMIN.AsFloat;
        taSpecDoc1PriceUch.AsFloat:=quSpecAOPRICEINUCH.AsFloat;
        taSpecDoc1SumUch.AsFloat:=quSpecAOSUMINUCH.AsFloat;
        taSpecDoc1IdNds.AsInteger:=0;
        taSpecDoc1SumNds.AsFloat:=0;
        taSpecDoc1NameC.AsString:=Copy(quSpecAONAME.AsString,1,30);
        taSpecDoc1Sm.AsString:=quSpecAONAMESHORT.AsString;
        taSpecDoc1IdM.AsInteger:=quSpecAOIDM.AsInteger;
        taSpecDoc1Km.AsFloat:=quSpecAOKM.AsFloat;
        taSpecDoc1.Post;

        quSpecAO.Next;
      end;

      quSpecAO.Active:=False;

      quSpecAI.Active:=False;
      quSpecAI.ParamByName('IDH').AsInteger:=IDH;
      quSpecAI.Active:=True;

      quSpecAI.First;
      while not quSpecAI.Eof do
      begin
        taSpecDoc.Append;
        taSpecDocIType.AsInteger:=5;
        taSpecDocIdHead.AsInteger:=IDH;
        taSpecDocNum.AsInteger:=quSpecAIID.AsInteger;
        taSpecDocIdCard.AsInteger:=quSpecAIIDCARD.AsInteger;
        taSpecDocQuant.AsFloat:=quSpecAIQUANT.AsFloat;
        taSpecDocPriceIn.AsFloat:=quSpecAIPRICEIN.AsFloat;
        taSpecDocSumIn.AsFloat:=quSpecAISUMIN.AsFloat;
        taSpecDocPriceUch.AsFloat:=quSpecAIPRICEINUCH.AsFloat;
        taSpecDocSumUch.AsFloat:=quSpecAISUMINUCH.AsFloat;
        taSpecDocIdNds.AsInteger:=0;
        taSpecDocSumNds.AsFloat:=0;
        taSpecDocNameC.AsString:=Copy(quSpecAINAME.AsString,1,30);
        taSpecDocSm.AsString:=quSpecAINAMESHORT.AsString;
        taSpecDocIdM.AsInteger:=quSpecAIIDM.AsInteger;
        taSpecDocKm.AsFloat:=quSpecAIKM.AsFloat;
        taSpecDocProcPrice.AsFloat:=quSpecAIPROCPRICE.AsFloat;
        taSpecDoc.Post;

        quSpecAI.Next;
      end;
      quSpecAI.Active:=False;

    end else
    begin
      showmessage('�������� ��� ���� � ������.');
    end;
    taHeadDoc.Active:=False;
    taSpecDoc.Active:=False;
    taSpecDoc1.Active:=False;
  end;
end;

procedure TfmDocsActs.acInsertDExecute(Sender: TObject);
begin
  // ��������
  with dmO do
  with dmORep do
  begin
    taHeadDoc.Active:=False;
    taHeadDoc.FileName:=CurDir+'HeadDoc.cds';
    if FileExists(CurDir+'HeadDoc.cds') then taHeadDoc.Active:=True
    else taHeadDoc.CreateDataSet;

    taSpecDoc.Active:=False;
    taSpecDoc.FileName:=CurDir+'SpecDoc.cds';
    if FileExists(CurDir+'SpecDoc.cds') then taSpecDoc.Active:=True
    else taSpecDoc.CreateDataSet;

    taSpecDoc1.Active:=False;
    taSpecDoc1.FileName:=CurDir+'SpecDoc1.cds';
    if FileExists(CurDir+'SpecDoc1.cds') then taSpecDoc1.Active:=True
    else taSpecDoc1.CreateDataSet;

    fmTBuff:=TfmTBuff.Create(Application);

    fmTBuff.LevelTH.Visible:=False;
    fmTBuff.LevelTS.Visible:=False;
    fmTBuff.LevelD.Visible:=True;
    fmTBuff.LevelDSpec.Visible:=True;

    fmTBuff.ShowModal;
    if fmTBuff.ModalResult=mrOk then
    begin //���������
      fmTBuff.Release;
      if taHeadDoc.RecordCount>0 then
      begin
        if CanDo('prAddAct') then
        begin
          prAllViewOff;

          fmAddAct.Caption:='���� �����������: ����� ��������.';
          fmAddAct.cxTextEdit1.Text:=prGetNum(5,0);
          fmAddAct.cxTextEdit1.Properties.ReadOnly:=False;
          fmAddAct.cxTextEdit1.Tag:=0; //������� ���������� ���������
          fmAddAct.cxDateEdit1.Date:=Date;
          fmAddAct.cxDateEdit1.Properties.ReadOnly:=False;
          fmAddAct.cxTextEdit2.Text:='';
          fmAddAct.cxTextEdit2.Properties.ReadOnly:=False;

          if quMHAll.Active=False then quMHAll.Active:=True;
          quMHAll.FullRefresh;

          fmAddAct.cxLookupComboBox1.EditValue:=taHeadDocIdSkl.AsInteger;
          fmAddAct.cxLookupComboBox1.Text:=taHeadDocNameSkl.AsString;
          fmAddAct.cxLookupComboBox1.Properties.ReadOnly:=False;

          if quMHAll.Locate('ID',taHeadDocIdSkl.AsInteger,[]) then
          begin
            fmAddAct.Label15.Caption:='��. ����: '+quMHAllNAMEPRICE.AsString;
            fmAddAct.Label15.Tag:=quMHAllDEFPRICE.AsInteger;
          end else
          begin
            fmAddAct.Label15.Caption:='��. ����: ';
            fmAddAct.Label15.Tag:=0;
          end;

          fmAddAct.cxLabel1.Enabled:=True;
          fmAddAct.cxLabel2.Enabled:=True;
          fmAddAct.cxLabel7.Enabled:=True;
          fmAddAct.cxLabel9.Enabled:=True;
          fmAddAct.Label3.Enabled:=True;

          fmAddAct.cxButton1.Enabled:=True;
          CloseTa(fmAddAct.taSpecO);
          CloseTa(fmAddAct.taSpecI);

          fmAddAct.ViewAO.OptionsData.Editing:=True;
          fmAddAct.ViewAI.OptionsData.Editing:=True;

          taSpecDoc.First;
          while not taSpecDoc.Eof do
          begin
            if (taSpecDocIType.AsInteger=taHeadDocIType.AsInteger)and(taSpecDocIdHead.AsInteger=taHeadDocId.AsInteger) then
            begin
              with fmAddAct do
              begin
                taSpecI.Append;
                taSpecINum.AsInteger:=taSpecDocNum.AsInteger;
                taSpecIIdGoods.AsInteger:=taSpecDocIdCard.AsInteger;
                taSpecINameG.AsString:=taSpecDocNameC.AsString;
                taSpecIIM.AsInteger:=taSpecDocIdM.AsInteger;
                taSpecISM.AsString:=taSpecDocSm.AsString;
                taSpecIQuant.AsFloat:=RoundEx(taSpecDocQuant.AsFloat*1000)/1000;
                taSpecIPriceIn.AsFloat:=taSpecDocPriceIn.AsFloat;
                taSpecISumIn.AsFloat:=taSpecDocSumIn.AsFloat;
                taSpecIPriceUch.AsFloat:=taSpecDocPriceUch.AsFloat;
                taSpecISumUch.AsFloat:=taSpecDocSumUch.AsFloat;
                taSpecIKm.AsFloat:=taSpecDocKm.AsFloat;
                taSpecIProcPrice.AsFloat:=taSpecDocProcPrice.AsFloat;
                taSpecI.Post;
              end;
            end;
            taSpecDoc.Next;
          end;

          taSpecDoc1.First;
          while not taSpecDoc1.Eof do
          begin
            if (taSpecDoc1IType.AsInteger=taHeadDocIType.AsInteger)and(taSpecDoc1IdHead.AsInteger=taHeadDocId.AsInteger) then
            begin
              with fmAddAct do
              begin
                taSpecO.Append;
                taSpecONum.AsInteger:=taSpecDoc1Num.AsInteger;
                taSpecOIdGoods.AsInteger:=taSpecDoc1IdCard.AsInteger;
                taSpecONameG.AsString:=taSpecDoc1NameC.AsString;
                taSpecOIM.AsInteger:=taSpecDoc1IdM.AsInteger;
                taSpecOSM.AsString:=taSpecDoc1Sm.AsString;
                taSpecOQuant.AsFloat:=RoundEx(taSpecDoc1Quant.AsFloat*1000)/1000;
                taSpecOPriceIn.AsFloat:=taSpecDoc1PriceIn.AsFloat;
                taSpecOSumIn.AsFloat:=taSpecDoc1SumIn.AsFloat;
                taSpecOPriceUch.AsFloat:=taSpecDoc1PriceUch.AsFloat;
                taSpecOSumUch.AsFloat:=taSpecDoc1SumUch.AsFloat;
                taSpecOKm.AsFloat:=taSpecDoc1Km.AsFloat;
                taSpecO.Post;
              end;
            end;
            taSpecDoc1.Next;
          end;

          taHeadDoc.Active:=False;
          taSpecDoc.Active:=False;
          taSpecDoc1.Active:=False;

          prAllViewOn;

          fmAddAct.ShowModal;

        end else showmessage('��� ����.');
      end;
    end else
    begin
      fmTBuff.Release;
      taHeadDoc.Active:=False;
      taSpecDoc.Active:=False;
      taSpecDoc1.Active:=False;
    end;
  end;
end;

procedure TfmDocsActs.acPrintExecute(Sender: TObject);
Var IDH:INteger;
    rQO,rQI,rQR,rPr:Real;
begin
  //������
  if not CanDo('prPrintAct') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmO do
  with dmORep do
  begin
    if quDocsActs.RecordCount>0 then //���� ��� ��������
    begin
      IDH:=quDocsActsID2.AsInteger;

      rQO:=0;
      rQI:=0;

      quSpecAO.Active:=False;
      quSpecAO.ParamByName('IDH').AsInteger:=IDH;
      quSpecAO.Active:=True;
      quSpecAO.First;
      while not quSpecAO.Eof do
      begin
        rQO:=rQO+quSpecAOQUANT.AsFloat*quSpecAOKM.AsFloat;
        quSpecAO.Next;
      end;


      quSpecAI.Active:=False;
      quSpecAI.ParamByName('IDH').AsInteger:=IDH;
      quSpecAI.Active:=True;
      quSpecAI.First;
      while not quSpecAI.Eof do
      begin
        rQI:=rQI+quSpecAIQUANT.AsFloat*quSpecAIKM.AsFloat;
        quSpecAI.Next;
      end;

      rQr:=rQO-rQI;
      rPr:=0;
      if rQO<>0 then
      begin
        rPr:=RoundEx(rQr/rQO*10000)/100;
      end;

      frRepA.LoadFromFile(CurDir + 'ttnAktP.frf');

      frVariables.Variable['Num']:=quDocsActsNUMDOC.AsString;
      frVariables.Variable['sDate']:=FormatDateTime('dd.mm.yyyy',quDocsActsDATEDOC.AsDateTime);
      frVariables.Variable['DocStore']:=quDocsActsNAMEMH.AsString;
      frVariables.Variable['Depart']:=CommonSet.DepartName;
      frVariables.Variable['QuantO']:=rQO;
      frVariables.Variable['QuantI']:=rQI;
      frVariables.Variable['QuantR']:=rQr;
      frVariables.Variable['Proc']:=rPr;

      frRepA.ReportName:='��� �����������.';
      frRepA.PrepareReport;
      frRepA.ShowPreparedReport;


      quSpecAI.Active:=False;
      quSpecAO.Active:=False;
    end else
    begin
      showmessage('�������� �������� ��� ������.');
    end;
  end;
end;

procedure TfmDocsActs.FormShow(Sender: TObject);
begin
  Memo1.Clear;
end;

procedure TfmDocsActs.Excel1Click(Sender: TObject);
begin
//������� � ������
  prNExportExel4(ViewActs);
end;

end.
