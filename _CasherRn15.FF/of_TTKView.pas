unit of_TTKView;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxButtonEdit, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ExtCtrls, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxControls, cxGridCustomView, cxGrid, ComCtrls, cxDropDownEdit, cxCalc,
  cxMaskEdit, cxSpinEdit, cxContainer, cxTextEdit, DBClient, Placemnt;

type
  TfmTTKView = class(TForm)
    StatusBar1: TStatusBar;
    GTSpecV: TcxGrid;
    ViewTSpecV: TcxGridDBTableView;
    ViewTSpecVNum: TcxGridDBColumn;
    ViewTSpecVIDCARD: TcxGridDBColumn;
    ViewTSpecVNAME: TcxGridDBColumn;
    ViewTSpecVIdM: TcxGridDBColumn;
    ViewTSpecVSM: TcxGridDBColumn;
    ViewTSpecVNETTO: TcxGridDBColumn;
    ViewTSpecVBRUTTO: TcxGridDBColumn;
    ViewTSpecVTCard: TcxGridDBColumn;
    LTSpecV: TcxGridLevel;
    Panel1: TPanel;
    cxButton1: TcxButton;
    Label2: TLabel;
    Label3: TLabel;
    cxTextEdit3: TcxTextEdit;
    cxTextEdit4: TcxTextEdit;
    Label4: TLabel;
    Label5: TLabel;
    cxSpinEdit1: TcxSpinEdit;
    cxCalcEdit1: TcxCalcEdit;
    FormPlacement1: TFormPlacement;
    taTSpecV: TClientDataSet;
    taTSpecVNum: TIntegerField;
    taTSpecVIdCard: TIntegerField;
    taTSpecVIdM: TIntegerField;
    taTSpecVSM: TStringField;
    taTSpecVKm: TFloatField;
    taTSpecVName: TStringField;
    taTSpecVNetto: TFloatField;
    taTSpecVBrutto: TFloatField;
    taTSpecVKnb: TFloatField;
    taTSpecVTCard: TSmallintField;
    dstaTSpecV: TDataSource;
    procedure FormCreate(Sender: TObject);
    procedure ViewTSpecVCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmTTKView: TfmTTKView;

implementation

uses dmOffice, Un1;

{$R *.dfm}

procedure TfmTTKView.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
end;

procedure TfmTTKView.ViewTSpecVCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
Var iType,i:Integer;
begin
//  if AViewInfo.GridRecord.Selected then exit;

  iType:=0;
  for i:=0 to ViewTSpecV.ColumnCount-1 do
  begin
    if ViewTSpecV.Columns[i].Name='ViewTSpecVTCard' then
    begin
      iType:=VarAsType(AViewInfo.GridRecord.DisplayTexts[i], varInteger);
      break;
    end;
  end;

  if iType=1  then
  begin
    ACanvas.Canvas.Font.Color := clHotLight;
    ACanvas.Canvas.Font.Style :=[fsBold];
  end;
end;

end.
