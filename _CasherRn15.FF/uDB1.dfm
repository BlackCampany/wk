object dmC1: TdmC1
  OldCreateOrder = False
  Left = 344
  Top = 365
  Height = 523
  Width = 791
  object CasherRnDb1: TpFIBDatabase
    DBName = 'localhost:C:\_CasherRn\DB\CASHERRN.GDB'
    DBParams.Strings = (
      'user_name=SYSDBA'
      'lc_ctype=WIN1251'
      'password=masterkey'
      'sql_role_name=SYSDBA')
    DefaultTransaction = trPrintSel
    DefaultUpdateTransaction = trPrintUpd
    SQLDialect = 3
    Timeout = 0
    SynchronizeTime = False
    DesignDBOptions = []
    AliasName = 'CasherRnDb1'
    WaitForRestoreConnect = 0
    Left = 40
    Top = 15
  end
  object quPrintQu: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE PRINTQUERY'
      'SET '
      '    STREAM = :STREAM,'
      '    PTYPE = :PTYPE,'
      '    FTYPE = :FTYPE,'
      '    STR = :STR,'
      '    PAGECODE = :PAGECODE,'
      '    CTYPE = :CTYPE,'
      '    BRING = :BRING'
      'WHERE'
      '    IDQUERY = :OLD_IDQUERY'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    PRINTQUERY'
      'WHERE'
      '        IDQUERY = :OLD_IDQUERY'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO PRINTQUERY('
      '    IDQUERY,'
      '    ID,'
      '    STREAM,'
      '    PTYPE,'
      '    FTYPE,'
      '    STR,'
      '    PAGECODE,'
      '    CTYPE,'
      '    BRING'
      ')'
      'VALUES('
      '    :IDQUERY,'
      '    :ID,'
      '    :STREAM,'
      '    :PTYPE,'
      '    :FTYPE,'
      '    :STR,'
      '    :PAGECODE,'
      '    :CTYPE,'
      '    :BRING'
      ')')
    RefreshSQL.Strings = (
      'SELECT IDQUERY,ID,STREAM,PTYPE,FTYPE,STR,PAGECODE,CTYPE,BRING'
      'FROM PRINTQUERY'
      'where(  STREAM=:ISTREAM'
      '     ) and (     PRINTQUERY.IDQUERY = :OLD_IDQUERY'
      '    and PRINTQUERY.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT IDQUERY,ID,STREAM,PTYPE,FTYPE,STR,PAGECODE,CTYPE,BRING'
      'FROM PRINTQUERY'
      'where IDQUERY=:IDQ'
      'Order by IDQUERY,ID')
    Transaction = trQuPrint
    Database = CasherRnDb1
    UpdateTransaction = trPrintUpd
    AutoCommit = True
    Left = 248
    Top = 16
    poAskRecordCount = True
    object quPrintQuIDQUERY: TFIBIntegerField
      FieldName = 'IDQUERY'
    end
    object quPrintQuID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quPrintQuSTREAM: TFIBIntegerField
      FieldName = 'STREAM'
    end
    object quPrintQuPTYPE: TFIBStringField
      FieldName = 'PTYPE'
      Size = 10
      EmptyStrToNull = True
    end
    object quPrintQuFTYPE: TFIBSmallIntField
      FieldName = 'FTYPE'
    end
    object quPrintQuSTR: TFIBStringField
      FieldName = 'STR'
      Size = 60
      EmptyStrToNull = True
    end
    object quPrintQuPAGECODE: TFIBSmallIntField
      FieldName = 'PAGECODE'
    end
    object quPrintQuCTYPE: TFIBSmallIntField
      FieldName = 'CTYPE'
    end
    object quPrintQuBRING: TFIBSmallIntField
      FieldName = 'BRING'
    end
  end
  object quDelQu: TpFIBQuery
    Transaction = trDelQu
    Database = CasherRnDb1
    SQL.Strings = (
      'DELETE from PRINTQUERY '
      'where IDQUERY=:IDQ'
      '    ')
    Left = 304
    Top = 16
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object trQuPrint: TpFIBTransaction
    DefaultDatabase = CasherRnDb1
    TimeoutAction = TARollback
    Left = 248
    Top = 72
  end
  object trDelQu: TpFIBTransaction
    DefaultDatabase = CasherRnDb1
    TimeoutAction = TARollback
    Left = 304
    Top = 72
  end
  object trPrintSel: TpFIBTransaction
    DefaultDatabase = CasherRnDb1
    TimeoutAction = TARollback
    Left = 120
    Top = 16
  end
  object trPrintUpd: TpFIBTransaction
    DefaultDatabase = CasherRnDb1
    TimeoutAction = TARollback
    Left = 120
    Top = 72
  end
  object prGetId1: TpFIBStoredProc
    Transaction = trSelGetId
    Database = CasherRnDb1
    SQL.Strings = (
      'EXECUTE PROCEDURE PR_GETID (?ITYPE)')
    StoredProcName = 'PR_GETID'
    Left = 96
    Top = 136
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object trSelGetId: TpFIBTransaction
    DefaultDatabase = CasherRnDb1
    TimeoutAction = TARollback
    Left = 32
    Top = 136
  end
  object prSetRefresh: TpFIBStoredProc
    Transaction = trRefresh
    Database = dmC.CasherRnDb
    SQL.Strings = (
      'EXECUTE PROCEDURE PR_SETREFRESH (?STATION)')
    StoredProcName = 'PR_SETREFRESH'
    Left = 568
    Top = 16
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object trRefresh: TpFIBTransaction
    DefaultDatabase = dmC.CasherRnDb
    TimeoutAction = TARollback
    Left = 496
    Top = 16
  end
  object quRefresh: TpFIBDataSet
    Transaction = dmC.trSelect
    Database = dmC.CasherRnDb
    UpdateTransaction = dmC.trUpdate
    Left = 576
    Top = 72
    poAskRecordCount = True
  end
  object taNumZ: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE ZAKAZNUM'
      'SET '
      '    CURDATE = :CURDATE,'
      '    NUM = :NUM'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    ZAKAZNUM'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO ZAKAZNUM('
      '    ID,'
      '    CURDATE,'
      '    NUM'
      ')'
      'VALUES('
      '    :ID,'
      '    :CURDATE,'
      '    :NUM'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    CURDATE,'
      '    NUM'
      'FROM'
      '    ZAKAZNUM '
      ''
      ' WHERE '
      '        ZAKAZNUM.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    CURDATE,'
      '    NUM'
      'FROM'
      '    ZAKAZNUM ')
    Transaction = trSelNumZ
    Database = dmC.CasherRnDb
    UpdateTransaction = trUpdNumZ
    AutoCommit = True
    Left = 24
    Top = 208
    poAskRecordCount = True
    object taNumZID: TFIBSmallIntField
      FieldName = 'ID'
    end
    object taNumZCURDATE: TFIBDateTimeField
      FieldName = 'CURDATE'
    end
    object taNumZNUM: TFIBIntegerField
      FieldName = 'NUM'
    end
  end
  object trSelNumZ: TpFIBTransaction
    DefaultDatabase = dmC.CasherRnDb
    TimeoutAction = TARollback
    Left = 80
    Top = 208
  end
  object trUpdNumZ: TpFIBTransaction
    DefaultDatabase = dmC.CasherRnDb
    TimeoutAction = TARollback
    Left = 80
    Top = 264
  end
  object quRSum: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE PRINTQUERY'
      'SET '
      '    STREAM = :STREAM,'
      '    PTYPE = :PTYPE,'
      '    FTYPE = :FTYPE,'
      '    STR = :STR,'
      '    PAGECODE = :PAGECODE,'
      '    CTYPE = :CTYPE,'
      '    BRING = :BRING'
      'WHERE'
      '    IDQUERY = :OLD_IDQUERY'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    PRINTQUERY'
      'WHERE'
      '        IDQUERY = :OLD_IDQUERY'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO PRINTQUERY('
      '    IDQUERY,'
      '    ID,'
      '    STREAM,'
      '    PTYPE,'
      '    FTYPE,'
      '    STR,'
      '    PAGECODE,'
      '    CTYPE,'
      '    BRING'
      ')'
      'VALUES('
      '    :IDQUERY,'
      '    :ID,'
      '    :STREAM,'
      '    :PTYPE,'
      '    :FTYPE,'
      '    :STR,'
      '    :PAGECODE,'
      '    :CTYPE,'
      '    :BRING'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    IDQUERY,'
      '    ID,'
      '    STREAM,'
      '    PTYPE,'
      '    FTYPE,'
      '    STR,'
      '    PAGECODE,'
      '    CTYPE,'
      '    BRING'
      'FROM'
      '    PRINTQUERY '
      ''
      ' WHERE '
      '        PRINTQUERY.IDQUERY = :OLD_IDQUERY'
      '    and PRINTQUERY.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'SELECT PAYTYPE,SUM(TABSUM)as SUMR'
      'FROM CASHSAIL'
      'where CASHNUM=:ICASHNUM'
      'and ZNUM=:IZNUM'
      'and TABSUM>0'
      'Group by PAYTYPE')
    Transaction = trS1
    Database = dmC.CasherRnDb
    AutoCommit = True
    Left = 256
    Top = 144
    poAskRecordCount = True
    object quRSumPAYTYPE: TFIBSmallIntField
      FieldName = 'PAYTYPE'
    end
    object quRSumSUMR: TFIBFloatField
      FieldName = 'SUMR'
    end
  end
  object trS1: TpFIBTransaction
    DefaultDatabase = dmC.CasherRnDb
    TimeoutAction = TARollback
    Left = 200
    Top = 144
  end
  object quRSumRet: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE PRINTQUERY'
      'SET '
      '    STREAM = :STREAM,'
      '    PTYPE = :PTYPE,'
      '    FTYPE = :FTYPE,'
      '    STR = :STR,'
      '    PAGECODE = :PAGECODE,'
      '    CTYPE = :CTYPE,'
      '    BRING = :BRING'
      'WHERE'
      '    IDQUERY = :OLD_IDQUERY'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    PRINTQUERY'
      'WHERE'
      '        IDQUERY = :OLD_IDQUERY'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO PRINTQUERY('
      '    IDQUERY,'
      '    ID,'
      '    STREAM,'
      '    PTYPE,'
      '    FTYPE,'
      '    STR,'
      '    PAGECODE,'
      '    CTYPE,'
      '    BRING'
      ')'
      'VALUES('
      '    :IDQUERY,'
      '    :ID,'
      '    :STREAM,'
      '    :PTYPE,'
      '    :FTYPE,'
      '    :STR,'
      '    :PAGECODE,'
      '    :CTYPE,'
      '    :BRING'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    IDQUERY,'
      '    ID,'
      '    STREAM,'
      '    PTYPE,'
      '    FTYPE,'
      '    STR,'
      '    PAGECODE,'
      '    CTYPE,'
      '    BRING'
      'FROM'
      '    PRINTQUERY '
      ''
      ' WHERE '
      '        PRINTQUERY.IDQUERY = :OLD_IDQUERY'
      '    and PRINTQUERY.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'SELECT PAYTYPE,SUM(TABSUM)as SUMR'
      'FROM CASHSAIL'
      'where CASHNUM=:ICASHNUM'
      'and ZNUM=:IZNUM'
      'and TABSUM<0'
      'Group by PAYTYPE')
    Transaction = trS1
    Database = dmC.CasherRnDb
    AutoCommit = True
    Left = 256
    Top = 200
    poAskRecordCount = True
    object quRSumRetPAYTYPE: TFIBSmallIntField
      FieldName = 'PAYTYPE'
    end
    object quRSumRetSUMR: TFIBFloatField
      FieldName = 'SUMR'
    end
  end
  object quMaxMin: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE PRINTQUERY'
      'SET '
      '    STREAM = :STREAM,'
      '    PTYPE = :PTYPE,'
      '    FTYPE = :FTYPE,'
      '    STR = :STR,'
      '    PAGECODE = :PAGECODE,'
      '    CTYPE = :CTYPE,'
      '    BRING = :BRING'
      'WHERE'
      '    IDQUERY = :OLD_IDQUERY'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    PRINTQUERY'
      'WHERE'
      '        IDQUERY = :OLD_IDQUERY'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO PRINTQUERY('
      '    IDQUERY,'
      '    ID,'
      '    STREAM,'
      '    PTYPE,'
      '    FTYPE,'
      '    STR,'
      '    PAGECODE,'
      '    CTYPE,'
      '    BRING'
      ')'
      'VALUES('
      '    :IDQUERY,'
      '    :ID,'
      '    :STREAM,'
      '    :PTYPE,'
      '    :FTYPE,'
      '    :STR,'
      '    :PAGECODE,'
      '    :CTYPE,'
      '    :BRING'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    IDQUERY,'
      '    ID,'
      '    STREAM,'
      '    PTYPE,'
      '    FTYPE,'
      '    STR,'
      '    PAGECODE,'
      '    CTYPE,'
      '    BRING'
      'FROM'
      '    PRINTQUERY '
      ''
      ' WHERE '
      '        PRINTQUERY.IDQUERY = :OLD_IDQUERY'
      '    and PRINTQUERY.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'SELECT Min(TAB_ID) as IBEG,Max(TAB_ID) as IEND'
      'FROM CASHSAIL'
      'where CASHNUM=:ICASHNUM'
      'and ZNUM=:IZNUM')
    Transaction = trS1
    Database = dmC.CasherRnDb
    AutoCommit = True
    Left = 256
    Top = 256
    poAskRecordCount = True
    object quMaxMinIBEG: TFIBIntegerField
      FieldName = 'IBEG'
    end
    object quMaxMinIEND: TFIBIntegerField
      FieldName = 'IEND'
    end
  end
  object quDSum: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE PRINTQUERY'
      'SET '
      '    STREAM = :STREAM,'
      '    PTYPE = :PTYPE,'
      '    FTYPE = :FTYPE,'
      '    STR = :STR,'
      '    PAGECODE = :PAGECODE,'
      '    CTYPE = :CTYPE,'
      '    BRING = :BRING'
      'WHERE'
      '    IDQUERY = :OLD_IDQUERY'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    PRINTQUERY'
      'WHERE'
      '        IDQUERY = :OLD_IDQUERY'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO PRINTQUERY('
      '    IDQUERY,'
      '    ID,'
      '    STREAM,'
      '    PTYPE,'
      '    FTYPE,'
      '    STR,'
      '    PAGECODE,'
      '    CTYPE,'
      '    BRING'
      ')'
      'VALUES('
      '    :IDQUERY,'
      '    :ID,'
      '    :STREAM,'
      '    :PTYPE,'
      '    :FTYPE,'
      '    :STR,'
      '    :PAGECODE,'
      '    :CTYPE,'
      '    :BRING'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    IDQUERY,'
      '    ID,'
      '    STREAM,'
      '    PTYPE,'
      '    FTYPE,'
      '    STR,'
      '    PAGECODE,'
      '    CTYPE,'
      '    BRING'
      'FROM'
      '    PRINTQUERY '
      ''
      ' WHERE '
      '        PRINTQUERY.IDQUERY = :OLD_IDQUERY'
      '    and PRINTQUERY.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'SELECT SUM(sp.DISCOUNTSUM) as DSUM'
      'FROM SPEC_ALL sp'
      'left join TABLES_ALL ta on ta.ID=sp.ID_TAB'
      'where sp.ID_TAB>=:IMIN'
      'and sp.ID_TAB<=:IMAX'
      'and ta.STATION=:ISTATION')
    Transaction = trS1
    Database = dmC.CasherRnDb
    AutoCommit = True
    Left = 320
    Top = 144
    poAskRecordCount = True
    object quDSumDSUM: TFIBFloatField
      FieldName = 'DSUM'
    end
  end
  object quSums: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE CURREST'
      'SET '
      '    SUM1 = :SUM1,'
      '    SUM2 = :SUM2,'
      '    SUM3 = :SUM3'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    CURREST'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO CURREST('
      '    ID,'
      '    SUM1,'
      '    SUM2,'
      '    SUM3'
      ')'
      'VALUES('
      '    :ID,'
      '    :SUM1,'
      '    :SUM2,'
      '    :SUM3'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    SUM1,'
      '    SUM2,'
      '    SUM3'
      'FROM'
      '    CURREST '
      ''
      ' WHERE '
      '        CURREST.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    SUM1,'
      '    SUM2,'
      '    SUM3'
      'FROM'
      '    CURREST '
      'ORDER BY ID')
    Transaction = trS1
    Database = dmC.CasherRnDb
    UpdateTransaction = taU1
    AutoCommit = True
    Left = 320
    Top = 200
    poAskRecordCount = True
    object quSumsID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quSumsSUM1: TFIBFloatField
      FieldName = 'SUM1'
    end
    object quSumsSUM2: TFIBFloatField
      FieldName = 'SUM2'
    end
    object quSumsSUM3: TFIBFloatField
      FieldName = 'SUM3'
    end
  end
  object taU1: TpFIBTransaction
    DefaultDatabase = dmC.CasherRnDb
    TimeoutAction = TARollback
    Left = 200
    Top = 200
  end
  object quTabFind: TpFIBDataSet
    SelectSQL.Strings = (
      'Select first 1 ID from tables'
      'where ID=:ID'
      '')
    Transaction = trSelGetId
    Database = CasherRnDb1
    UpdateTransaction = taU1
    Left = 416
    Top = 152
    poAskRecordCount = True
    object quTabFindID: TFIBIntegerField
      FieldName = 'ID'
    end
  end
  object quWaitersSum: TpFIBDataSet
    SelectSQL.Strings = (
      
        'SELECT first 3 ta.ID_PERSONAL,pe.NAME,SUM(ta.QUESTS) as SUMQU, S' +
        'UM(ta.TABSUM) as SUMR'
      'FROM TABLES_ALL ta'
      'left join RPERSONAL pe on pe.ID=ta.ID_PERSONAL'
      'where ta.ENDTIME>=:DATEB and ta.ENDTIME<:DATEE'
      'and ta.OPERTYPE<>'#39'Del'#39' '
      'Group by ta.ID_PERSONAL,pe.NAME'
      'order by SUM(ta.TABSUM) DESC')
    Transaction = trS1
    Database = dmC.CasherRnDb
    UpdateTransaction = taU1
    Left = 528
    Top = 160
    poAskRecordCount = True
    poEmptyStrToNull = False
    object quWaitersSumID_PERSONAL: TFIBIntegerField
      FieldName = 'ID_PERSONAL'
    end
    object quWaitersSumNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 200
      EmptyStrToNull = False
    end
    object quWaitersSumSUMQU: TFIBBCDField
      FieldName = 'SUMQU'
      Size = 0
      RoundByScale = True
    end
    object quWaitersSumSUMR: TFIBFloatField
      FieldName = 'SUMR'
    end
  end
  object quPrint: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE PRINTQUERY'
      'SET '
      '    STREAM = :STREAM,'
      '    PTYPE = :PTYPE,'
      '    FTYPE = :FTYPE,'
      '    STR = :STR,'
      '    PAGECODE = :PAGECODE,'
      '    CTYPE = :CTYPE,'
      '    BRING = :BRING'
      'WHERE'
      '    IDQUERY = :OLD_IDQUERY'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    PRINTQUERY'
      'WHERE'
      '        IDQUERY = :OLD_IDQUERY'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO PRINTQUERY('
      '    IDQUERY,'
      '    ID,'
      '    STREAM,'
      '    PTYPE,'
      '    FTYPE,'
      '    STR,'
      '    PAGECODE,'
      '    CTYPE,'
      '    BRING'
      ')'
      'VALUES('
      '    :IDQUERY,'
      '    :ID,'
      '    :STREAM,'
      '    :PTYPE,'
      '    :FTYPE,'
      '    :STR,'
      '    :PAGECODE,'
      '    :CTYPE,'
      '    :BRING'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    IDQUERY,'
      '    ID,'
      '    STREAM,'
      '    PTYPE,'
      '    FTYPE,'
      '    STR,'
      '    PAGECODE,'
      '    CTYPE,'
      '    BRING'
      'FROM'
      '    PRINTQUERY '
      ''
      ' WHERE '
      '        PRINTQUERY.IDQUERY = :OLD_IDQUERY'
      '    and PRINTQUERY.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    IDQUERY,'
      '    ID,'
      '    STREAM,'
      '    PTYPE,'
      '    FTYPE,'
      '    STR,'
      '    PAGECODE,'
      '    CTYPE,'
      '    BRING'
      'FROM'
      '    PRINTQUERY ')
    Transaction = trPrintSel
    Database = CasherRnDb1
    UpdateTransaction = trPrintUpd
    AutoCommit = True
    Left = 184
    Top = 16
    object quPrintIDQUERY: TFIBIntegerField
      FieldName = 'IDQUERY'
    end
    object quPrintID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quPrintSTREAM: TFIBIntegerField
      FieldName = 'STREAM'
    end
    object quPrintPTYPE: TFIBStringField
      FieldName = 'PTYPE'
      Size = 10
      EmptyStrToNull = True
    end
    object quPrintFTYPE: TFIBSmallIntField
      FieldName = 'FTYPE'
    end
    object quPrintSTR: TFIBStringField
      FieldName = 'STR'
      Size = 40
      EmptyStrToNull = True
    end
    object quPrintPAGECODE: TFIBSmallIntField
      FieldName = 'PAGECODE'
    end
    object quPrintCTYPE: TFIBSmallIntField
      FieldName = 'CTYPE'
    end
    object quPrintBRING: TFIBSmallIntField
      FieldName = 'BRING'
    end
  end
  object quPrintQH: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE PRINTQH'
      'SET '
      '    COMMENT = :COMMENT'
      'WHERE'
      '    STREAM = :OLD_STREAM'
      '    and IDH = :OLD_IDH'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    PRINTQH'
      'WHERE'
      '        STREAM = :OLD_STREAM'
      '    and IDH = :OLD_IDH'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO PRINTQH('
      '    STREAM,'
      '    IDH,'
      '    COMMENT'
      ')'
      'VALUES('
      '    :STREAM,'
      '    :IDH,'
      '    :COMMENT'
      ')')
    RefreshSQL.Strings = (
      'SELECT first 3 STREAM,IDH,COMMENT'
      'FROM PRINTQH '
      ''
      ' WHERE '
      '        PRINTQH.STREAM = :OLD_STREAM'
      '    and PRINTQH.IDH = :OLD_IDH'
      '    ')
    SelectSQL.Strings = (
      'SELECT first 3 STREAM,IDH,COMMENT'
      'FROM PRINTQH '
      'where STREAM=:ISTREAM')
    Transaction = trQuPrint
    Database = CasherRnDb1
    UpdateTransaction = trPrintUpd
    AutoCommit = True
    Left = 432
    Top = 16
    poAskRecordCount = True
    object quPrintQHSTREAM: TFIBIntegerField
      FieldName = 'STREAM'
    end
    object quPrintQHIDH: TFIBIntegerField
      FieldName = 'IDH'
    end
    object quPrintQHCOMMENT: TFIBStringField
      FieldName = 'COMMENT'
      Size = 50
      EmptyStrToNull = True
    end
  end
  object quAddPQH: TpFIBQuery
    Transaction = trPrintUpd
    Database = CasherRnDb1
    SQL.Strings = (
      'insert into PRINTQH values(1,1,'#39'Test'#39')')
    Left = 368
    Top = 16
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object quSumTalons: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE PRINTQUERY'
      'SET '
      '    STREAM = :STREAM,'
      '    PTYPE = :PTYPE,'
      '    FTYPE = :FTYPE,'
      '    STR = :STR,'
      '    PAGECODE = :PAGECODE,'
      '    CTYPE = :CTYPE,'
      '    BRING = :BRING'
      'WHERE'
      '    IDQUERY = :OLD_IDQUERY'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    PRINTQUERY'
      'WHERE'
      '        IDQUERY = :OLD_IDQUERY'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO PRINTQUERY('
      '    IDQUERY,'
      '    ID,'
      '    STREAM,'
      '    PTYPE,'
      '    FTYPE,'
      '    STR,'
      '    PAGECODE,'
      '    CTYPE,'
      '    BRING'
      ')'
      'VALUES('
      '    :IDQUERY,'
      '    :ID,'
      '    :STREAM,'
      '    :PTYPE,'
      '    :FTYPE,'
      '    :STR,'
      '    :PAGECODE,'
      '    :CTYPE,'
      '    :BRING'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    IDQUERY,'
      '    ID,'
      '    STREAM,'
      '    PTYPE,'
      '    FTYPE,'
      '    STR,'
      '    PAGECODE,'
      '    CTYPE,'
      '    BRING'
      'FROM'
      '    PRINTQUERY '
      ''
      ' WHERE '
      '        PRINTQUERY.IDQUERY = :OLD_IDQUERY'
      '    and PRINTQUERY.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT sp.SIFR,me.NAME,sp.PRICE,SUM(sp.QUANTITY) as QUANT,SUM(sp' +
        '.SUMMA) as RSUM'
      'FROM SPEC_ALL sp'
      'left join MENU me on me.SIFR=sp.SIFR'
      'left join TABLES_ALL ta on ta.ID=sp.ID_TAB'
      'where sp.ID_TAB>=:IMIN'
      'and sp.ID_TAB<=:IMAX'
      'and ta.STATION=:ISTATION'
      'and sp.SUMMA<0'
      'group by sp.SIFR,me.NAME,sp.PRICE')
    Transaction = trS1
    Database = dmC.CasherRnDb
    AutoCommit = True
    Left = 324
    Top = 256
    poAskRecordCount = True
    object quSumTalonsSIFR: TFIBIntegerField
      FieldName = 'SIFR'
    end
    object quSumTalonsNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 100
      EmptyStrToNull = True
    end
    object quSumTalonsPRICE: TFIBFloatField
      FieldName = 'PRICE'
    end
    object quSumTalonsQUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quSumTalonsRSUM: TFIBFloatField
      FieldName = 'RSUM'
    end
  end
  object quFBar: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE MENU'
      'SET '
      '    NAME = :NAME,'
      '    PRICE = :PRICE,'
      '    CODE = :CODE,'
      '    TREETYPE = :TREETYPE,'
      '    LIMITPRICE = :LIMITPRICE,'
      '    CATEG = :CATEG,'
      '    PARENT = :PARENT,'
      '    LINK = :LINK,'
      '    STREAM = :STREAM,'
      '    LACK = :LACK,'
      '    DESIGNSIFR = :DESIGNSIFR,'
      '    ALTNAME = :ALTNAME,'
      '    NALOG = :NALOG,'
      '    BARCODE = :BARCODE,'
      '    IMAGE = :IMAGE,'
      '    CONSUMMA = :CONSUMMA,'
      '    MINREST = :MINREST,'
      '    PRNREST = :PRNREST,'
      '    COOKTIME = :COOKTIME,'
      '    DISPENSER = :DISPENSER,'
      '    DISPKOEF = :DISPKOEF,'
      '    ACCESS = :ACCESS,'
      '    FLAGS = :FLAGS,'
      '    TARA = :TARA,'
      '    CNTPRICE = :CNTPRICE,'
      '    BACKBGR = :BACKBGR,'
      '    FONTBGR = :FONTBGR,'
      '    IACTIVE = :IACTIVE,'
      '    IEDIT = :IEDIT,'
      '    DATEB = :DATEB,'
      '    DATEE = :DATEE,'
      '    DAYWEEK = :DAYWEEK,'
      '    TIMEB = :TIMEB,'
      '    TIMEE = :TIMEE,'
      '    ALLTIME = :ALLTIME'
      'WHERE'
      '    SIFR = :OLD_SIFR'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    MENU'
      'WHERE'
      '        SIFR = :OLD_SIFR'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO MENU('
      '    SIFR,'
      '    NAME,'
      '    PRICE,'
      '    CODE,'
      '    TREETYPE,'
      '    LIMITPRICE,'
      '    CATEG,'
      '    PARENT,'
      '    LINK,'
      '    STREAM,'
      '    LACK,'
      '    DESIGNSIFR,'
      '    ALTNAME,'
      '    NALOG,'
      '    BARCODE,'
      '    IMAGE,'
      '    CONSUMMA,'
      '    MINREST,'
      '    PRNREST,'
      '    COOKTIME,'
      '    DISPENSER,'
      '    DISPKOEF,'
      '    ACCESS,'
      '    FLAGS,'
      '    TARA,'
      '    CNTPRICE,'
      '    BACKBGR,'
      '    FONTBGR,'
      '    IACTIVE,'
      '    IEDIT,'
      '    DATEB,'
      '    DATEE,'
      '    DAYWEEK,'
      '    TIMEB,'
      '    TIMEE,'
      '    ALLTIME'
      ')'
      'VALUES('
      '    :SIFR,'
      '    :NAME,'
      '    :PRICE,'
      '    :CODE,'
      '    :TREETYPE,'
      '    :LIMITPRICE,'
      '    :CATEG,'
      '    :PARENT,'
      '    :LINK,'
      '    :STREAM,'
      '    :LACK,'
      '    :DESIGNSIFR,'
      '    :ALTNAME,'
      '    :NALOG,'
      '    :BARCODE,'
      '    :IMAGE,'
      '    :CONSUMMA,'
      '    :MINREST,'
      '    :PRNREST,'
      '    :COOKTIME,'
      '    :DISPENSER,'
      '    :DISPKOEF,'
      '    :ACCESS,'
      '    :FLAGS,'
      '    :TARA,'
      '    :CNTPRICE,'
      '    :BACKBGR,'
      '    :FONTBGR,'
      '    :IACTIVE,'
      '    :IEDIT,'
      '    :DATEB,'
      '    :DATEE,'
      '    :DAYWEEK,'
      '    :TIMEB,'
      '    :TIMEE,'
      '    :ALLTIME'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      
        '    mm.SIFR,mm.NAME,mm.PRICE,mm.CODE,mm.TREETYPE,mm.LIMITPRICE,m' +
        'm.CATEG,'
      
        '    mm.PARENT,mm.LINK,mm.STREAM,mm.LACK,mm.DESIGNSIFR,mm.ALTNAME' +
        ',mm.NALOG,'
      
        '    mm.BARCODE,mm.IMAGE,mm.CONSUMMA,mm.MINREST,mm.PRNREST,mm.COO' +
        'KTIME,'
      
        '    mm.DISPENSER,mm.DISPKOEF,mm.ACCESS,mm.FLAGS,mm.TARA,mm.CNTPR' +
        'ICE,'
      
        '    mm.BACKBGR,mm.FONTBGR,mm.IACTIVE,mm.IEDIT,mm.DATEB,mm.DATEE,' +
        'mm.DAYWEEK,mm.TIMEB,mm.TIMEE,mm.ALLTIME,'
      '    ca.NAME as NAMECA,mo.NAME as NAMEMO,s.NAMESTREAM'
      '    '
      'FROM MENU mm'
      'Left join CATEGORY ca on ca.SIFR=mm.CATEG'
      'Left join modify mo on mo.SIFR=mm.LINK'
      'Left join STREAMS s on s.ID=mm.STREAM'
      '    '
      'WHERE(  mm.PARENT=:PARENTID'
      'and mm.TREETYPE='#39'F'#39
      '     ) and (     MM.SIFR = :OLD_SIFR'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    BARCODE,'
      '    SIFR,'
      '    QUANT,'
      '    PRICE'
      'FROM'
      '    BARCODE'
      'where BARCODE=:SBAR ')
    Transaction = dmC.trSelect
    Database = dmC.CasherRnDb
    UpdateTransaction = dmC.trUpdate
    Left = 416
    Top = 212
    poAskRecordCount = True
    object quFBarBARCODE: TFIBStringField
      FieldName = 'BARCODE'
      EmptyStrToNull = True
    end
    object quFBarSIFR: TFIBIntegerField
      FieldName = 'SIFR'
    end
    object quFBarQUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quFBarPRICE: TFIBFloatField
      FieldName = 'PRICE'
    end
  end
  object devCas: TVaComm
    Baudrate = br9600
    FlowControl.OutCtsFlow = False
    FlowControl.OutDsrFlow = False
    FlowControl.ControlDtr = dtrDisabled
    FlowControl.ControlRts = rtsDisabled
    FlowControl.XonXoffOut = False
    FlowControl.XonXoffIn = False
    FlowControl.DsrSensitivity = False
    FlowControl.TxContinueOnXoff = False
    DeviceName = 'COM1'
    OnRxBuf = devCasRxBuf
    Left = 40
    Top = 339
  end
  object VaWaitMessageCas: TVaWaitMessage
    Comm = devCas
    Strings.Strings = (
      #1052#1077#1089#1089#1072#1075#1072)
    Active = True
    Left = 116
    Top = 340
  end
end
