object fmTabsFF: TfmTabsFF
  Left = 383
  Top = 103
  BorderStyle = bsDialog
  Caption = #1055#1077#1088#1077#1085#1077#1089#1090#1080
  ClientHeight = 551
  ClientWidth = 478
  Color = 16449787
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 8
    Top = 480
    Width = 461
    Height = 9
  end
  object Label1: TLabel
    Left = 16
    Top = 8
    Width = 219
    Height = 20
    Caption = #1057#1087#1080#1089#1086#1082' '#1086#1090#1082#1088#1099#1090#1099#1093' '#1079#1072#1082#1072#1079#1086#1074
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object cxButton3: TcxButton
    Left = 277
    Top = 496
    Width = 192
    Height = 45
    Caption = #1054#1090#1084#1077#1085#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ModalResult = 2
    ParentFont = False
    TabOrder = 0
    Colors.Default = 16759807
    Colors.Normal = 16759807
    Colors.Pressed = 11403438
    Glyph.Data = {
      5E040000424D5E04000000000000360000002800000012000000130000000100
      18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
      CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
      8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
      0000CED3D6848684848684848684848684848684848684848684848684848684
      848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
      7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
      FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
      00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
      75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
      0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
      FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
      CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
      007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
      00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
      75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
      0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
      FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
      CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
      00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
      494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
      0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
      00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
      CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
      0000}
    LookAndFeel.Kind = lfFlat
  end
  object cxButton1: TcxButton
    Left = 9
    Top = 496
    Width = 260
    Height = 45
    Caption = #1054#1090#1082#1088#1099#1090#1100
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ModalResult = 1
    ParentFont = False
    TabOrder = 1
    Colors.Default = 16759807
    Colors.Normal = 16759807
    Colors.Pressed = 11403438
    LookAndFeel.Kind = lfFlat
  end
  object Panel1: TPanel
    Left = 16
    Top = 192
    Width = 321
    Height = 41
    BevelInner = bvLowered
    Color = 16449787
    TabOrder = 2
    object Label4: TLabel
      Left = 24
      Top = 16
      Width = 78
      Height = 13
      Caption = #1053#1086#1084#1077#1088' '#1089#1090#1086#1083#1072
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Transparent = True
    end
    object TextEdit1: TcxTextEdit
      Left = 144
      Top = 8
      ParentFont = False
      Properties.Alignment.Horz = taRightJustify
      Style.BorderColor = 8454143
      Style.BorderStyle = ebsUltraFlat
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clNavy
      Style.Font.Height = -13
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsBold]
      Style.LookAndFeel.Kind = lfUltraFlat
      Style.IsFontAssigned = True
      StyleDisabled.LookAndFeel.Kind = lfUltraFlat
      StyleFocused.LookAndFeel.Kind = lfUltraFlat
      StyleHot.LookAndFeel.Kind = lfUltraFlat
      TabOrder = 0
      Text = '0'
      Width = 124
    end
  end
  object Panel2: TPanel
    Left = 8
    Top = 32
    Width = 461
    Height = 445
    BevelInner = bvLowered
    Color = 16449787
    TabOrder = 3
    Visible = False
    object Grid: TcxGrid
      Left = 2
      Top = 2
      Width = 457
      Height = 441
      Align = alClient
      TabOrder = 0
      object ViewMove: TcxGridDBCardView
        NavigatorButtons.ConfirmDelete = False
        DataController.DataSource = dmC.dsTabsPers
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        LayoutDirection = ldVertical
        OptionsBehavior.ImmediateEditor = False
        OptionsCustomize.CardSizing = False
        OptionsCustomize.RowFiltering = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsSelection.UnselectFocusedRecordOnExit = False
        OptionsView.CardWidth = 93
        OptionsView.SeparatorColor = 16713471
        OptionsView.SeparatorWidth = 3
        Styles.Background = dmC.cxStyle20
        Styles.CardBorder = dmC.cxStyle19
        object ViewMoveNUMZ: TcxGridDBCardViewRow
          Caption = #1047#1072#1082#1072#1079' '#8470
          DataBinding.FieldName = 'NUMZ'
          Styles.Content = dmC.cxStyle5
        end
        object ViewMoveNUMTABLE: TcxGridDBCardViewRow
          Caption = #1057#1090#1086#1083' '#8470
          DataBinding.FieldName = 'NUMTABLE'
          Styles.Content = dmC.cxStyle13
        end
        object ViewMoveTABSUM: TcxGridDBCardViewRow
          Caption = #1057#1091#1084#1084#1072
          DataBinding.FieldName = 'TABSUM'
          Styles.Content = dmC.cxStyle12
        end
      end
      object Level: TcxGridLevel
        GridView = ViewMove
      end
    end
  end
end
