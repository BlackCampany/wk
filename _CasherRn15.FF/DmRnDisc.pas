unit DmRnDisc;

interface

uses
  SysUtils, Classes, DB, FIBDataSet, pFIBDataSet, FIBDatabase, pFIBDatabase,
  cxStyles, ImgList, Controls, FIBQuery, pFIBQuery, pFIBStoredProc,
  frexpimg, frRtfExp, frTXTExp, frXMLExl, frOLEExl, FR_E_HTML2, FR_E_HTM,
  FR_E_CSV, FR_E_RTF, FR_Class, FR_E_TXT;

type
  TdmCDisc = class(TDataModule)
    CasherRnDb: TpFIBDatabase;
    trSelect: TpFIBTransaction;
    trUpdate: TpFIBTransaction;
    trDel: TpFIBTransaction;
    quPer: TpFIBDataSet;
    quPerID: TFIBIntegerField;
    quPerID_PARENT: TFIBIntegerField;
    quPerNAME: TFIBStringField;
    quPerUVOLNEN: TFIBBooleanField;
    quPerCheck: TFIBStringField;
    quPerMODUL1: TFIBBooleanField;
    quPerMODUL2: TFIBBooleanField;
    quPerMODUL3: TFIBBooleanField;
    quPerMODUL4: TFIBBooleanField;
    quPerMODUL5: TFIBBooleanField;
    quPerMODUL6: TFIBBooleanField;
    dsPer: TDataSource;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    cxStyle14: TcxStyle;
    cxStyle15: TcxStyle;
    cxStyle16: TcxStyle;
    cxStyle17: TcxStyle;
    cxStyle18: TcxStyle;
    cxStyle19: TcxStyle;
    cxStyle20: TcxStyle;
    cxStyle21: TcxStyle;
    cxStyle22: TcxStyle;
    cxStyle23: TcxStyle;
    imState: TImageList;
    quCanDo: TpFIBDataSet;
    quCanDoID_PERSONAL: TFIBIntegerField;
    quCanDoNAME: TFIBStringField;
    quCanDoPREXEC: TFIBBooleanField;
    trCanDo: TpFIBTransaction;
    prAddRClassif: TpFIBStoredProc;
    prDelRClassif: TpFIBStoredProc;
    prEditRClassif: TpFIBStoredProc;
    cxStyle24: TcxStyle;
    cxStyle25: TcxStyle;
    prGetId: TpFIBStoredProc;
    frTextExport1: TfrTextExport;
    frRTFExport1: TfrRTFExport;
    frCSVExport1: TfrCSVExport;
    frHTMExport1: TfrHTMExport;
    frHTML2Export1: TfrHTML2Export;
    frOLEExcelExport1: TfrOLEExcelExport;
    frXMLExcelExport1: TfrXMLExcelExport;
    frTextAdvExport1: TfrTextAdvExport;
    frRtfAdvExport1: TfrRtfAdvExport;
    frJPEGExport1: TfrJPEGExport;
    quClassif: TpFIBDataSet;
    quDiscSel: TpFIBDataSet;
    quDiscSelBARCODE: TFIBStringField;
    quDiscSelNAME: TFIBStringField;
    quDiscSelPERCENT: TFIBFloatField;
    quDiscSelCLIENTINDEX: TFIBIntegerField;
    quDiscSelIACTIVE: TFIBSmallIntField;
    dsDiscSel: TDataSource;
    quMaxIdM: TpFIBDataSet;
    quMaxIdMMAXID: TFIBIntegerField;
    taClassif: TpFIBDataSet;
    taClassifTYPE_CLASSIF: TFIBIntegerField;
    taClassifID: TFIBIntegerField;
    taClassifIACTIVE: TFIBSmallIntField;
    taClassifID_PARENT: TFIBIntegerField;
    taClassifNAME: TFIBStringField;
    taClassifIEDIT: TFIBSmallIntField;
    taClassEdit: TpFIBDataSet;
    taClassEditTYPE_CLASSIF: TFIBIntegerField;
    taClassEditID: TFIBIntegerField;
    taClassEditIACTIVE: TFIBSmallIntField;
    taClassEditID_PARENT: TFIBIntegerField;
    taClassEditNAME: TFIBStringField;
    taClassEditIEDIT: TFIBSmallIntField;
    quFindChild: TpFIBDataSet;
    quFindChildCOUNTREC: TFIBIntegerField;
    quFindDC: TpFIBDataSet;
    quFindDCBARCODE: TFIBStringField;
    taTypePl: TpFIBDataSet;
    taTypePlID: TFIBIntegerField;
    taTypePlNAME: TFIBStringField;
    taTypePlDATEFROM: TFIBDateField;
    taTypePlDATETO: TFIBDateField;
    taTypePlCOMMENT: TFIBStringField;
    dsTypePl: TDataSource;
    quMaxIdTPL: TpFIBDataSet;
    quMaxIdTPLMAXID: TFIBIntegerField;
    quFindTPL: TpFIBDataSet;
    quFindTPLCOUNTREC: TFIBIntegerField;
    dsTypePl1: TDataSource;
    taPCard: TpFIBDataSet;
    dsPCard: TDataSource;
    taPCardBARCODE: TFIBStringField;
    taPCardPLATTYPE: TFIBIntegerField;
    taPCardIACTIVE: TFIBSmallIntField;
    taPCardCLINAME: TFIBStringField;
    taPCardNAME: TFIBStringField;
    taPCardDATEFROM: TFIBDateField;
    taPCardDATETO: TFIBDateField;
    taPCardCOMMENT: TFIBStringField;
    quRep1: TpFIBDataSet;
    quRep1DISCONT: TFIBStringField;
    quRep1NAME: TFIBStringField;
    quRep1PERCENT: TFIBFloatField;
    quRep1SSUM: TFIBFloatField;
    quDiscDetail: TpFIBDataSet;
    quDiscDetailID_PERSONAL: TFIBIntegerField;
    quDiscDetailNUMTABLE: TFIBStringField;
    quDiscDetailQUESTS: TFIBIntegerField;
    quDiscDetailENDTIME: TFIBDateTimeField;
    quDiscDetailDISCONT: TFIBStringField;
    quDiscDetailNAME: TFIBStringField;
    quDiscDetailPERCENT: TFIBFloatField;
    quDiscDetailTABSUM: TFIBFloatField;
    quDiscDetailNAME1: TFIBStringField;
    dsDiscDetail: TDataSource;
    quPCDet: TpFIBDataSet;
    quPCDetID: TFIBIntegerField;
    quPCDetID_PERSONAL: TFIBIntegerField;
    quPCDetNUMTABLE: TFIBStringField;
    quPCDetQUESTS: TFIBIntegerField;
    quPCDetENDTIME: TFIBDateTimeField;
    quPCDetDISCONT: TFIBStringField;
    quPCDetCLINAME: TFIBStringField;
    quPCDetTABSUM: TFIBFloatField;
    quPCDetNAME: TFIBStringField;
    quPCDetSIFR: TFIBIntegerField;
    quPCDetPRICE: TFIBFloatField;
    quPCDetQUANTITY: TFIBFloatField;
    quPCDetDISCOUNTPROC: TFIBFloatField;
    quPCDetDISCOUNTSUM: TFIBFloatField;
    quPCDetSUMMA: TFIBFloatField;
    quPCDetNAME1: TFIBStringField;
    dsPCDet: TDataSource;
    quPCDetNAMETYPE: TFIBStringField;
    quPCDetCOMMENT: TFIBStringField;
    quPCDetDATETO: TFIBDateField;
    quDiscSelPHONE: TFIBStringField;
    quDiscSelBERTHDAY: TFIBDateField;
    quDiscSelCOMMENT: TFIBStringField;
    quPCDetDISCONT1: TFIBStringField;
    quPCDetDCNAME: TFIBStringField;
    quDiscDetailID: TFIBIntegerField;
    quPCCli: TpFIBDataSet;
    quPCCliDISCONT: TFIBStringField;
    quPCCliDCNAME: TFIBStringField;
    quPCCliSUM: TFIBFloatField;
    quPCCliSUM1: TFIBFloatField;
    quPCCliSITOG: TFIBFloatField;
    dsPCCli: TDataSource;
    dsRep1: TDataSource;
    quRep1SUMPOS: TFIBFloatField;
    quRep1ITSUM: TFIBFloatField;
    quDiscDetailITSUM: TFIBFloatField;
    quDiscDetailSUMPOS: TFIBFloatField;
    taTabs: TpFIBDataSet;
    taTabsID: TFIBIntegerField;
    taTabsID_PERSONAL: TFIBIntegerField;
    taTabsNUMTABLE: TFIBStringField;
    taTabsQUESTS: TFIBIntegerField;
    taTabsTABSUM: TFIBFloatField;
    taTabsBEGTIME: TFIBDateTimeField;
    taTabsENDTIME: TFIBDateTimeField;
    taTabsDISCONT: TFIBStringField;
    taTabsOPERTYPE: TFIBStringField;
    taTabsCHECKNUM: TFIBIntegerField;
    taTabsSKLAD: TFIBSmallIntField;
    taTabsDISCONT1: TFIBStringField;
    taTabsSTATION: TFIBIntegerField;
    taTabsNUMZ: TFIBIntegerField;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

Function CanDo(Name_F:String):Boolean;

var
  dmCDisc: TdmCDisc;

implementation

uses Un1;

{$R *.dfm}

procedure TdmCDisc.DataModuleCreate(Sender: TObject);
begin
  CasherRnDb.Connected:=False;
  CasherRnDb.DBName:=DBName;
  try
    CasherRnDb.Open;
  except
  end;
end;

procedure TdmCDisc.DataModuleDestroy(Sender: TObject);
begin
  try
    CasherRnDb.Close;
  except
  end;
end;

Function CanDo(Name_F:String):Boolean;
begin
  result:=True;
  with dmCDisc do
  begin
    quCanDo.Active:=False;
    quCanDo.ParamByName('Person_id').Value:=Person.Id;
    quCanDo.ParamByName('Name_F').Value:=Name_F;
    quCanDo.Active:=True;
    if quCanDoprExec.AsBoolean=True then Result:=False;
  end;
end;


end.
