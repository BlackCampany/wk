unit AddDoc4;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, cxGraphics, cxCurrencyEdit, cxDropDownEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxButtonEdit,
  cxMaskEdit, cxCalendar, cxControls, cxContainer, cxEdit, cxTextEdit,
  StdCtrls, ExtCtrls, Menus, cxLookAndFeelPainters, cxButtons, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, DB, cxDBData, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxLabel, dxfLabel, Placemnt, FIBDataSet,
  pFIBDataSet, DBClient, FIBQuery, pFIBQuery, pFIBStoredProc,
  XPStyleActnCtrls, ActnList, ActnMan, FIBDatabase, pFIBDatabase,
  cxImageComboBox, cxCalc;

type
  TfmAddDoc4 = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Label1: TLabel;
    cxTextEdit1: TcxTextEdit;
    cxDateEdit1: TcxDateEdit;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label12: TLabel;
    cxTextEdit2: TcxTextEdit;
    cxDateEdit2: TcxDateEdit;
    cxButtonEdit1: TcxButtonEdit;
    cxLookupComboBox1: TcxLookupComboBox;
    Panel2: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Panel3: TPanel;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    cxLabel3: TcxLabel;
    Label15: TLabel;
    FormPlacement1: TFormPlacement;
    taSpec: TClientDataSet;
    dsSpec: TDataSource;
    taSpecNum: TIntegerField;
    taSpecIdGoods: TIntegerField;
    taSpecNameG: TStringField;
    taSpecIM: TIntegerField;
    taSpecSM: TStringField;
    taSpecQuant: TFloatField;
    taSpecPriceIn: TCurrencyField;
    taSpecSumIn: TCurrencyField;
    taSpecPriceR: TCurrencyField;
    taSpecSumR: TCurrencyField;
    taSpecINds: TIntegerField;
    taSpecSNds: TStringField;
    taSpecRNds: TCurrencyField;
    taSpecSumNac: TCurrencyField;
    taSpecProcNac: TFloatField;
    cxLabel4: TcxLabel;
    cxLabel5: TcxLabel;
    cxLabel6: TcxLabel;
    taSpecKm: TFloatField;
    amDocR: TActionManager;
    acAddPos: TAction;
    acDelPos: TAction;
    acAddList: TAction;
    acDelAll: TAction;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    acSaveDoc: TAction;
    acExitDoc: TAction;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    GridDoc4: TcxGrid;
    ViewDoc4: TcxGridDBTableView;
    ViewDoc4Num: TcxGridDBColumn;
    ViewDoc4IdGoods: TcxGridDBColumn;
    ViewDoc4NameG: TcxGridDBColumn;
    ViewDoc4IM: TcxGridDBColumn;
    ViewDoc4SM: TcxGridDBColumn;
    ViewDoc4Quant: TcxGridDBColumn;
    ViewDoc4PriceIn: TcxGridDBColumn;
    ViewDoc4SumIn: TcxGridDBColumn;
    ViewDoc4PriceR: TcxGridDBColumn;
    ViewDoc4SumR: TcxGridDBColumn;
    ViewDoc4INds: TcxGridDBColumn;
    ViewDoc4SNds: TcxGridDBColumn;
    ViewDoc4RNds: TcxGridDBColumn;
    ViewDoc4SumNac: TcxGridDBColumn;
    ViewDoc4ProcNac: TcxGridDBColumn;
    LevelDoc4: TcxGridLevel;
    taTara: TClientDataSet;
    dstaTara: TDataSource;
    taTaraNum: TIntegerField;
    taTaraIdCard: TIntegerField;
    taTaraNameG: TStringField;
    taTaraIM: TIntegerField;
    taTaraKM: TFloatField;
    taTaraSM: TStringField;
    taTaraQuant: TFloatField;
    taTaraPriceIn: TFloatField;
    taTaraSumIn: TFloatField;
    taTaraPriceUch: TFloatField;
    taTaraSumUch: TFloatField;
    taTaraSumNac: TFloatField;
    taTaraProcNac: TFloatField;
    GrTara: TcxGrid;
    ViewTara: TcxGridDBTableView;
    LevelTara: TcxGridLevel;
    ViewTaraNum: TcxGridDBColumn;
    ViewTaraIdCard: TcxGridDBColumn;
    ViewTaraNameG: TcxGridDBColumn;
    ViewTaraIM: TcxGridDBColumn;
    ViewTaraKM: TcxGridDBColumn;
    ViewTaraSM: TcxGridDBColumn;
    ViewTaraQuant: TcxGridDBColumn;
    ViewTaraPriceIn: TcxGridDBColumn;
    ViewTaraSumIn: TcxGridDBColumn;
    ViewTaraPriceUch: TcxGridDBColumn;
    ViewTaraSumUch: TcxGridDBColumn;
    ViewTaraSumNac: TcxGridDBColumn;
    ViewTaraProcNac: TcxGridDBColumn;
    ViewDoc4Km: TcxGridDBColumn;
    CasherRnDb1: TpFIBDatabase;
    trSelCash: TpFIBTransaction;
    quFindPrice: TpFIBDataSet;
    quFindPricePRICE: TFIBFloatField;
    quFindPriceCODE: TFIBStringField;
    quFindPriceCONSUMMA: TFIBFloatField;
    quFindPriceIACTIVE: TFIBSmallIntField;
    taSpecTCard: TIntegerField;
    ViewDoc4TCard: TcxGridDBColumn;
    cxCalcEdit1: TcxCalcEdit;
    Label6: TLabel;
    taSpecOper: TSmallintField;
    ViewDoc4Oper: TcxGridDBColumn;
    N2: TMenuItem;
    Excel1: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure cxLookupComboBox1PropertiesChange(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxLabel1Click(Sender: TObject);
    procedure ViewDoc4DragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure ViewDoc4DragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure taSpecQuantChange(Sender: TField);
    procedure taSpecPriceInChange(Sender: TField);
    procedure taSpecSumInChange(Sender: TField);
    procedure taSpecPriceRChange(Sender: TField);
    procedure taSpecSumRChange(Sender: TField);
    procedure cxLabel3Click(Sender: TObject);
    procedure cxLabel2Click(Sender: TObject);
    procedure ViewDoc4Editing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cxLabel5Click(Sender: TObject);
    procedure ViewDoc4EditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure ViewDoc4EditKeyPress(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
    procedure acAddPosExecute(Sender: TObject);
    procedure acDelPosExecute(Sender: TObject);
    procedure acAddListExecute(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure acDelAllExecute(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure cxLabel6Click(Sender: TObject);
    procedure cxDateEdit1PropertiesChange(Sender: TObject);
    procedure ViewDoc4SMPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxButton2Click(Sender: TObject);
    procedure acSaveDocExecute(Sender: TObject);
    procedure acExitDocExecute(Sender: TObject);
    procedure ViewTaraDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure ViewTaraDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure ViewTaraEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure ViewTaraEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure ViewTaraEditKeyPress(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
    procedure taTaraQuantChange(Sender: TField);
    procedure taTaraPriceInChange(Sender: TField);
    procedure taTaraSumInChange(Sender: TField);
    procedure taTaraPriceUchChange(Sender: TField);
    procedure taTaraSumUchChange(Sender: TField);
    procedure ViewTaraInSMPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxButtonEdit1KeyPress(Sender: TObject; var Key: Char);
    procedure Excel1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAddDoc4: TfmAddDoc4;
  iCol:Integer = 0;
  iColT:Integer = 0;
  bAdd:Boolean = False;

implementation

uses Un1, dmOffice, Clients, Goods, FCards, DocsIn, CurMessure, OMessure,
  DMOReps, DocOutR, FindSebPr;

{$R *.dfm}

procedure TfmAddDoc4.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  PageControl1.Align:=alClient;
  PageControl1.ActivePageIndex:=0;
  ViewDoc4.RestoreFromIniFile(CurDir+GridIni);
  ViewTara.RestoreFromIniFile(CurDir+GridIni);
end;

procedure TfmAddDoc4.cxLookupComboBox1PropertiesChange(Sender: TObject);
begin
  with dmO do
  begin
    CurVal.IdMH:=cxLookupComboBox1.EditValue;
    CurVal.NAMEMH:=cxLookupComboBox1.Text;
    if quMHAll.Locate('ID',CurVal.IdMH,[]) then
    begin
      Label15.Caption:='��. ����: '+quMHAllNAMEPRICE.AsString;
      Label15.Tag:=quMHAllDEFPRICE.AsInteger;
    end else
    begin
      Label15.Caption:='';
      Label15.Tag:=0;
    end;  
  end;
end;

procedure TfmAddDoc4.cxButtonEdit1PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  if dmO.taClients.Active=False then dmO.taClients.Active:=True;
  if fmClients.Visible then fmClients.Close;
  bSelCli:=True;
  if cxButtonEdit1.Tag>0 then dmO.taClients.Locate('ID',cxButtonEdit1.Tag,[]);
  fmClients.ShowModal;
  if fmClients.ModalResult=mrOk then
  begin
    cxButtonEdit1.Tag:=dmO.taClientsID.AsInteger;
    cxButtonEdit1.Text:=dmO.taClientsNAMECL.AsString;
  end else
  begin
    cxButtonEdit1.Tag:=0;
    cxButtonEdit1.Text:='';
  end;
  bSelCli:=False;
end;

procedure TfmAddDoc4.cxLabel1Click(Sender: TObject);
begin
  acAddList.Execute;
end;

procedure TfmAddDoc4.ViewDoc4DragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept:=False;
  if bDrR then  Accept:=True;
end;

procedure TfmAddDoc4.ViewDoc4DragDrop(Sender, Source: TObject; X,
  Y: Integer);
Var iCo:Integer;
    i,j: Integer;
    iNum: Integer;
    Rec:TcxCustomGridRecord;
    iMax:Integer;
begin
//
  if bDrR then
  begin
    ResetAddVars;
    iCo:=fmGoods.ViewGoods.Controller.SelectedRecordCount;
    if iCo>0 then
    begin
      if MessageDlg('�� ������������� ������ ����������� ��������� ������� ('+IntToStr(iCo)+' ��.) � ������������ ���������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        iMax:=1;
        fmAddDoc4.taSpec.First;
        if not fmAddDoc4.taSpec.Eof then
        begin
          fmAddDoc4.taSpec.Last;
          iMax:=fmAddDoc4.taSpecNum.AsInteger+1;
        end;

        with dmO do
        begin
          ViewDoc4.BeginUpdate;
          for i:=0 to fmGoods.ViewGoods.Controller.SelectedRecordCount-1 do
          begin
            Rec:=fmGoods.ViewGoods.Controller.SelectedRecords[i];

            for j:=0 to Rec.ValueCount-1 do
            begin
              if fmGoods.ViewGoods.Columns[j].Name='ViewGoodsID' then break;
            end;

            iNum:=Rec.Values[j];
          //��� ��� - ����������

            if quCardsSel.Locate('ID',iNum,[]) then
            begin
              with fmAddDoc4 do
              begin
                taSpec.Append;
                taSpecNum.AsInteger:=iMax;
                taSpecIdGoods.AsInteger:=quCardsSelID.AsInteger;
                taSpecNameG.AsString:=quCardsSelNAME.AsString;
                taSpecIM.AsInteger:=quCardsSelIMESSURE.AsInteger;
                taSpecSM.AsString:=quCardsSelNAMESHORT.AsString;
                taSpecKm.AsFloat:=quCardsselKOEF.AsFloat;
                taSpecQuant.AsFloat:=1;
                taSpecPriceIn.AsFloat:=0;
                taSpecSumIn.AsFloat:=0;
                taSpecPriceR.AsFloat:=quCardsSelLASTPRICEOUT.AsFloat;
                taSpecSumR.AsFloat:=quCardsSelLASTPRICEOUT.AsFloat;
                taSpecINds.AsInteger:=quCardsSelINDS.AsInteger;
                taSpecSNds.AsString:=quCardsSelNAMENDS.AsString;
                taSpecRNds.AsFloat:=0;
                taSpecSumNac.AsFloat:=0;
                taSpecProcNac.AsFloat:=0;
                taSpecTCard.AsInteger:=quCardsSelTCard.AsInteger;
                taSpecOper.AsInteger:=0;
                taSpec.Post;
                inc(iMax);
              end;
            end;
          end;
          ViewDoc4.EndUpdate;
        end;
      end;
    end;
  end;
end;

procedure TfmAddDoc4.taSpecQuantChange(Sender: TField);
begin
  //���������� �����
  if iCol=1 then
  begin
    taSpecSumIn.AsFloat:=taSpecPriceIn.AsFloat*taSpecQuant.AsFloat;
    taSpecSumR.AsFloat:=taSpecPriceR.AsFloat*taSpecQuant.AsFloat;
    taSpecSumNac.AsFloat:=taSpecSumR.AsFloat-taSpecSumIn.AsFloat;
    if taSpecSumIn.AsFloat>0.01 then
    begin
      taSpecProcNac.AsFloat:=(taSpecSumR.AsFloat-taSpecSumIn.AsFloat)/taSpecSumIn.AsFloat*100;
    end;
    taSpecRNds.AsFloat:=RoundEx(taSpecSumIn.AsFloat*vNds[taSpecINds.AsInteger]/(100+vNds[taSpecINds.AsInteger])*100)/100;
  end;
end;

procedure TfmAddDoc4.taSpecPriceInChange(Sender: TField);
begin
  //���������� ����
  if iCol=2 then
  begin
    taSpecSumIn.AsFloat:=taSpecPriceIn.AsFloat*taSpecQuant.AsFloat;
//  taSpecSumR.AsFloat:=taSpecPriceR.AsFloat*taSpecQuant.AsFloat;
    taSpecSumNac.AsFloat:=taSpecSumR.AsFloat-taSpecSumIn.AsFloat;
    if taSpecSumIn.AsFloat>0.01 then
    begin
      taSpecProcNac.AsFloat:=(taSpecSumR.AsFloat-taSpecSumIn.AsFloat)/taSpecSumIn.AsFloat*100;
    end;
    taSpecRNds.AsFloat:=RoundEx(taSpecSumIn.AsFloat*vNds[taSpecINds.AsInteger]/(100+vNds[taSpecINds.AsInteger])*100)/100;
  end;
end;

procedure TfmAddDoc4.taSpecSumInChange(Sender: TField);
begin
  if iCol=3 then
  begin
    if abs(taSpecQuant.AsFloat)>0 then taSpecPriceIn.AsFloat:=RoundEx(taSpecSumIn.AsFloat/taSpecQuant.AsFloat*100)/100;
    taSpecSumNac.AsFloat:=taSpecSumR.AsFloat-taSpecSumIn.AsFloat;
    if taSpecSumIn.AsFloat>0.01 then
    begin
      taSpecProcNac.AsFloat:=(taSpecSumR.AsFloat-taSpecSumIn.AsFloat)/taSpecSumIn.AsFloat*100;
    end;
    taSpecRNds.AsFloat:=RoundEx(taSpecSumIn.AsFloat*vNds[taSpecINds.AsInteger]/(100+vNds[taSpecINds.AsInteger])*100)/100;
  end;
end;

procedure TfmAddDoc4.taSpecPriceRChange(Sender: TField);
begin
  //���������� ����
  if iCol=4 then
  begin
//  taSpecSumIn.AsFloat:=taSpecPriceIn.AsFloat*taSpecQuant.AsFloat;
    taSpecSumR.AsFloat:=taSpecPriceR.AsFloat*taSpecQuant.AsFloat;
    taSpecSumNac.AsFloat:=taSpecSumR.AsFloat-taSpecSumIn.AsFloat;
    if taSpecSumIn.AsFloat>0.01 then
    begin
      taSpecProcNac.AsFloat:=(taSpecSumR.AsFloat-taSpecSumIn.AsFloat)/taSpecSumIn.AsFloat*100;
    end;  
  end;
end;

procedure TfmAddDoc4.taSpecSumRChange(Sender: TField);
begin
  if iCol=5 then
  begin
    if abs(taSpecQuant.AsFloat)>0 then taSpecPriceR.AsFloat:=RoundEx(taSpecSumR.AsFloat/taSpecQuant.AsFloat*100)/100;
    taSpecSumNac.AsFloat:=taSpecSumR.AsFloat-taSpecSumIn.AsFloat;
    if taSpecSumIn.AsFloat>0.01 then
    begin
      taSpecProcNac.AsFloat:=(taSpecSumR.AsFloat-taSpecSumIn.AsFloat)/taSpecSumIn.AsFloat*100;
    end;
  end;
end;

procedure TfmAddDoc4.cxLabel3Click(Sender: TObject);
Var PriceSp,rPriceIn,rPriceUch:Real;
    rQs,rQ,rQp:Real;
    rMessure,rSumIn,rPrice,rDisc:Real;
begin
  //����������� ����
  if PageControl1.ActivePageIndex=0 then
  begin
//    ViewDoc4.BeginUpdate;
    GridDoc4.SetFocus;
    taSpec.First;
    while not taSpec.Eof do //��� ���� ������ �� ������� ���������
    begin
      if CommonSet.PriceFromCash=1 then //���� ������� � �����
      begin
        try
          CasherRnDb1.Connected:=False;
          CasherRnDb1.DatabaseName:=DBName;
          CasherRnDb1.Open;
          delay(500);
          if CasherRnDb1.Connected then
          begin
            rDisc:=RoundVal(cxCalcEdit1.EditValue);
            while not taSpec.Eof do
            begin
              quFindPrice.Active:=False;
              quFindPrice.ParamByName('IDCARD').AsString:=INtToStr(taSpecIdGoods.AsInteger);
              quFindPrice.Active:=True;
              if quFindPrice.RecordCount>0 then
              begin
                quFindPrice.First;

                rPrice:=RoundVal(quFindPricePRICE.AsFloat*(100-rDisc)/100);

                taSpec.Edit;
                taSpecPriceR.AsFloat:=rPrice;
                taSpecSumR.AsFloat:=RoundVal(taSpecQUANT.AsFloat*rPrice);
                taSpec.Post;
              end else
              begin
                if taSpecPriceR.AsFloat<>0 then
                begin
                  rPrice:=RoundVal(taSpecPriceR.AsFloat*(100-rDisc)/100);

                  taSpec.Edit;
                  taSpecPriceR.AsFloat:=rPrice;
                  taSpecSumR.AsFloat:=RoundVal(taSpecQUANT.AsFloat*rPrice);
                  taSpec.Post;
                end;
              end;
              quFindPrice.Active:=False;

              taSpec.Next;
            end;
            CasherRnDb1.Connected:=False;
          end;
        except
          CasherRnDb1.Connected:=False;
        end;
      end else //��������� ���� �� ��������
      begin
        rDisc:=RoundVal(cxCalcEdit1.EditValue);
        taSpec.First;
        while not taSpec.Eof do
        begin
          with dmO do
          begin
            quFCard.Active:=False;
            quFCard.SelectSQL.Clear;
            quFCard.SelectSQL.Add('SELECT ID,NAME,IMESSURE,PARENT,TCARD,INDS,LASTPRICEOUT');
            quFCard.SelectSQL.Add('FROM OF_CARDS');
            quFCard.SelectSQL.Add('where ID='+IntToStr(taSpecIdGoods.AsInteger));
            quFCard.Active:=True;

            if quFCard.RecordCount=1 then
            begin
              rPrice:=RoundVal(quFCardLASTPRICEOUT.AsFloat*(100-rDisc)/100);

              if rPrice>0.001 then
              begin
                taSpec.Edit;
                taSpecPriceR.AsFloat:=rPrice;
                taSpecSumR.AsFloat:=RoundVal(rPrice*taSpecQuant.AsFloat);
                taSpec.Post;
              end;
            end;

            quFCard.Active:=False;
          end;  

          taSpec.Next;
        end;
      end;

      taSpec.First;
      while not taSpec.Eof do
      begin
        PriceSp:=0;
        rSumIn:=0;
        rQs:=taSpecQuant.AsFloat*taSpecKm.AsFloat; //�������� � ��������
        prSelPartIn(taSpecIdGoods.AsInteger,cxLookupComboBox1.EditValue,0,0);

        with dmO do
        begin
          quSelPartIn.First;
          if rQs>0 then
          begin
            while (not quSelPartIn.Eof) and (rQs>0) do
            begin
              //���� �� ���� ������� ���� �����, ��������� �������� ���
              rQp:=quSelPartInQREMN.AsFloat;
              if rQs<=rQp then  rQ:=rQs//��������� ������ ������ ���������
                                else  rQ:=rQp;
              rQs:=rQs-rQ;

              PriceSp:=quSelPartInPRICEIN.AsFloat;
              rSumIn:=rSumIn+PriceSp*rQ;
              quSelPartIn.Next;
            end;

            if rQs>0 then //�������� ������������� ������, �������� � ������, �� � ������� ��������� ������ ���, ��� � �������������
            begin
              if PriceSp=0 then
              begin //��� ���� ���������� ������� � ���������� ����������
                prCalcLastPrice1.ParamByName('IDGOOD').AsInteger:=taSpecIdGoods.AsInteger;
                prCalcLastPrice1.ExecProc;
                PriceSp:=prCalcLastPrice1.ParamByName('PRICEIN').AsFloat;
                rMessure:=prCalcLastPrice1.ParamByName('KOEF').AsFloat;
                if (rMessure<>0)and(rMessure<>1) then PriceSp:=PriceSp/rMessure;
              end;
              rSumIn:=rSumIn+PriceSp*rQs;
            end;
          end;
          quSelPartIn.Active:=False;
        end;

        //�������� ���������
        taSpec.Edit;
        if taSpecQuant.AsFloat<>0 then
        begin
          taSpecSumIn.AsFloat:=rSumIn;
          taSpecPriceIn.AsFloat:=rSumIn/taSpecQuant.AsFloat;
        end else
        begin
          taSpecSumIn.AsFloat:=0;
          taSpecPriceIn.AsFloat:=0;
        end;
        taSpec.Post;

        taSpec.Next;
      end;
      delay(10);
    end;
    taSpec.First;
//  ViewDoc4.EndUpdate;
  end;
  if PageControl1.ActivePageIndex=1 then
  begin
    GrTara.SetFocus;
    taTara.First;
    while not taTara.Eof do
    begin
      rPriceIn:=taTaraPriceIn.AsFloat;
      if abs(rPriceIn)<0.01 then
      begin
        with dmORep do
        begin
          quLastPriceT.Active:=False;
          quLastPriceT.ParamByName('IDGOOD').AsInteger:=taTaraIdCard.AsInteger;
          quLastPriceT.Active:=True;

          if quLastPriceT.RecordCount>0 then
          begin
            rPriceIn:=quLastPriceTPRICEIN.AsFloat*taTaraKm.AsFloat;
            rPriceUch:=quLastPriceTPRICEOUT.AsFloat*taTaraKm.AsFloat;

            taTara.Edit;
            taTaraPriceIn.AsFloat:=rPriceIn;
            taTaraSumIn.AsFloat:=RoundVal(taTaraQuant.AsFloat*rPriceIn);
            taTaraPriceUch.AsFloat:=rPriceUch;
            taTaraSumUch.AsFloat:=RoundVal(taTaraQuant.AsFloat*rPriceUch);
            taTaraSumNac.AsFloat:=taTaraQuant.AsFloat*rPriceUch-taTaraQuant.AsFloat*rPriceIn;
            if abs(taTaraQuant.AsFloat*rPriceIn)>0.01 then
              taTaraProcNac.AsFloat:=(taTaraQuant.AsFloat*rPriceUch-taTaraQuant.AsFloat*rPriceIn)/(taTaraQuant.AsFloat*rPriceIn)*100;
            taTara.Post;  
          end;
          quLastPriceT.Active:=False;
        end;
      end;

      taTara.Next;
      delay(10);
    end;
    taTara.First;
  end;
end;

procedure TfmAddDoc4.cxLabel2Click(Sender: TObject);
begin
  acDelPos.Execute;
end;

procedure TfmAddDoc4.ViewDoc4Editing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  iCol:=0;
  if ViewDoc4.Controller.FocusedColumn.Name='ViewDoc4Quant' then iCol:=1;
  if ViewDoc4.Controller.FocusedColumn.Name='ViewDoc4PriceIn' then
  begin
    iCol:=2;
    if taSpec.RecordCount>0 then
    if (taSpecQuant.AsFloat<0)and(cxButton1.Enabled=True)and(taSpecIdGoods.AsInteger>0) then
    begin
      with dmORep do
      with dmO do
      begin
        quFindSebReal.Active:=False;
        quFindSebReal.ParamByName('IDCARD').AsInteger:=taSpecIdGoods.AsInteger;
        quFindSebReal.Active:=True;
        if quFindSebReal.RecordCount>0 then
        begin
          fmFindSebPr:=TfmFindSebPr.Create(Application);
          fmFindSebPr.ShowModal;
          if fmFindSebPr.ModalResult=mrOk then
          begin
            taSpec.Edit;
            taSpecPriceIn.AsFloat:=quFindSebRealPRICEIN.AsFloat;
            taSpecSumIn.AsFloat:=RoundVal(taSpecQuant.AsFloat*quFindSebRealPRICEIN.AsFloat);
            taSpec.Post;
          end;
          fmFindSebPr.Release;
        end;  
        quFindSebReal.Active:=False;
      end;
    end;
  end;
  if ViewDoc4.Controller.FocusedColumn.Name='ViewDoc4SumIn' then iCol:=3;
  if ViewDoc4.Controller.FocusedColumn.Name='ViewDoc4PriceR' then iCol:=4;
  if ViewDoc4.Controller.FocusedColumn.Name='ViewDoc4SumR' then iCol:=5;

end;

procedure TfmAddDoc4.FormShow(Sender: TObject);
begin
  iCol:=0;
  iColT:=0;
  PageControl1.ActivePageIndex:=0;

  if cxTextEdit1.Text='' then
  begin
    cxTextEdit1.SetFocus;
    cxTextEdit1.SelectAll;
  end else GridDoc4.SetFocus;

  ViewDoc4NameG.Options.Editing:=False;
  ViewTaraNameG.Options.Editing:=False;
end;

procedure TfmAddDoc4.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewDoc4.StoreToIniFile(CurDir+GridIni,False);
  ViewTara.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmAddDoc4.cxLabel5Click(Sender: TObject);
begin
  acAddPos.Execute;
end;

procedure TfmAddDoc4.ViewDoc4EditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
Var iCode:Integer;
    Km:Real;
    sName:String;
    //���� ������ ��� ������ ����� �� ������
    rK:Real;
    iM:Integer;
    Sm:String;
    //--
begin
  with dmO do
  begin
    if (Key=$0D) then
    begin
      if ViewDoc4.Controller.FocusedColumn.Name='ViewDoc4IdGoods' then
      begin
        iCode:=VarAsType(AEdit.EditingValue, varInteger);

        quFCard.Active:=False;
        quFCard.SelectSQL.Clear;
        quFCard.SelectSQL.Add('SELECT ID,NAME,IMESSURE,PARENT,TCARD,INDS,LASTPRICEOUT');
        quFCard.SelectSQL.Add('FROM OF_CARDS');
        quFCard.SelectSQL.Add('where ID='+IntToStr(iCode));
        quFCard.Active:=True;

        if quFCard.RecordCount=1 then
        begin
          ViewDoc4.BeginUpdate;
          taSpec.Edit;
          taSpecIdGoods.AsInteger:=iCode;
          taSpecNameG.AsString:=quFCardNAME.AsString;
          taSpecIM.AsInteger:=quFCardIMESSURE.AsInteger;
          taSpecSM.AsString:=prFindKNM(quFCardIMESSURE.AsInteger,Km);
          taSpecKm.AsFloat:=Km;
          taSpecQuant.AsFloat:=1;
          taSpecPriceIn.AsFloat:=0;
          taSpecSumIn.AsFloat:=0;
          taSpecPriceR.AsFloat:=quFCardLASTPRICEOUT.AsFloat;
          taSpecSumR.AsFloat:=quFCardLASTPRICEOUT.AsFloat;
          taSpecINds.AsInteger:=0;
          taSpecSNds.AsString:='';
          if taNDS.Active=False then taNDS.Active:=True;
          if taNds.Locate('ID',quFCardINDS.AsInteger,[]) then
          begin
            taSpecINds.AsInteger:=quFCardINDS.AsInteger;
            taSpecSNds.AsString:=taNDSNAMENDS.AsString;
          end;

          taSpecRNds.AsFloat:=0;
          taSpecSumNac.AsFloat:=0;
          taSpecProcNac.AsFloat:=0;
          taSpecTCard.AsInteger:=quFCardTCard.AsInteger;
          taSpec.Post;

          ViewDoc4.EndUpdate;
        end;
      end;
      if ViewDoc4.Controller.FocusedColumn.Name='ViewDoc4NameG' then
      begin
        sName:=VarAsType(AEdit.EditingValue, varString);

        quFCard.Active:=False;
        quFCard.SelectSQL.Clear;
        quFCard.SelectSQL.Add('SELECT ID,NAME,IMESSURE,PARENT,TCARD,INDS,LASTPRICEOUT');
        quFCard.SelectSQL.Add('FROM OF_CARDS');
        quFCard.SelectSQL.Add('where UPPER(NAME) like ''%'+AnsiUpperCase(sName)+'%''');
        quFCard.SelectSQL.Add('Order by NAME');

        quFCard.Active:=True;

        bAdd:=True;
        //�������� ����� ������ � ����� ������
        fmFCards.ShowModal;
        if fmFCards.ModalResult=mrOk then
        begin
          if quFCard.RecordCount>0 then
          begin
            ViewDoc4.BeginUpdate;
            taSpec.Edit;
            taSpecIdGoods.AsInteger:=quFCardID.AsInteger;
            taSpecNameG.AsString:=quFCardNAME.AsString;
            taSpecIM.AsInteger:=quFCardIMESSURE.AsInteger;
            taSpecSM.AsString:=prFindKNM(quFCardIMESSURE.AsInteger,Km);
            taSpecKm.AsFloat:=Km;
            taSpecQuant.AsFloat:=1;
            taSpecPriceIn.AsFloat:=0;
            taSpecSumIn.AsFloat:=0;
            taSpecPriceR.AsFloat:=quFCardLASTPRICEOUT.AsFloat;
            taSpecSumR.AsFloat:=quFCardLASTPRICEOUT.AsFloat;
            taSpecINds.AsInteger:=0;
            taSpecSNds.AsString:='';
            taSpecRNds.AsFloat:=0;
            if taNDS.Active=False then taNDS.Active:=True;
            if taNds.Locate('ID',quFCardINDS.AsInteger,[]) then
            begin
              taSpecINds.AsInteger:=quFCardINDS.AsInteger;
              taSpecSNds.AsString:=taNDSNAMENDS.AsString;
            end;
            taSpecSumNac.AsFloat:=0;
            taSpecProcNac.AsFloat:=0;
            taSpecTCard.AsInteger:=quFCardTCard.AsInteger;
            taSpec.Post;
            ViewDoc4.EndUpdate;
            AEdit.SelectAll;
          end;
        end;
        bAdd:=False;
      end;
      if ViewDoc4.Controller.FocusedColumn.Name='ViewDoc4SM' then
      begin
        if taSpecIm.AsInteger>0 then
        begin
          bAddSpec:=True;
          fmMessure.ShowModal;
          if fmMessure.ModalResult=mrOk then
          begin
            iM:=iMSel; iMSel:=0;
            Sm:=prFindKNM(iM,rK); //�������� ����� �������� � ����� �����

            taSpec.Edit;
            taSpecIM.AsInteger:=iM;
            taSpecKm.AsFloat:=rK;
            taSpecSM.AsString:=Sm;
            taSpec.Post;
          end;
        end;
      end;
    end else
      if ViewDoc4.Controller.FocusedColumn.Name='ViewDoc4IdGoods' then
        if fTestKey(Key)=False then
          if taSpec.State in [dsEdit,dsInsert] then taSpec.Cancel;
  end;
end;

procedure TfmAddDoc4.ViewDoc4EditKeyPress(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
Var Km:Real;
    sName:String;
begin
  if bAdd then exit;
  with dmO do
  begin
    if (ViewDoc4.Controller.FocusedColumn.Name='ViewDoc4NameG') then
    begin
      sName:=VarAsType(AEdit.EditingValue ,varString)+Key;
//      Label2.Caption:=sName;
      if sName>'' then
      begin
        quFCard.Active:=False;
        quFCard.SelectSQL.Clear;
        quFCard.SelectSQL.Add('SELECT  first 2 ID,NAME,IMESSURE,PARENT,TCARD,INDS,LASTPRICEOUT');
        quFCard.SelectSQL.Add('FROM OF_CARDS');
        quFCard.SelectSQL.Add('where UPPER(NAME) like ''%'+AnsiUpperCase(sName)+'%''');
        quFCard.SelectSQL.Add('Order by NAME');
        quFCard.Active:=True;

        if quFCard.RecordCount=1 then
        begin
          ViewDoc4.BeginUpdate;
          taSpec.Edit;
          taSpecIdGoods.AsInteger:=quFCardID.AsInteger;
          taSpecNameG.AsString:=quFCardNAME.AsString;
          taSpecIM.AsInteger:=quFCardIMESSURE.AsInteger;
          taSpecSM.AsString:=prFindKNM(quFCardIMESSURE.AsInteger,Km);
          taSpecKm.AsFloat:=Km;
          taSpecQuant.AsFloat:=0;
          taSpecPriceIn.AsFloat:=0;
          taSpecSumIn.AsFloat:=0;
          taSpecPriceR.AsFloat:=0;
          taSpecSumR.AsFloat:=0;
          taSpecINds.AsInteger:=0;
          taSpecSNds.AsString:='';
          if taNDS.Active=False then taNDS.Active:=True;
          if taNds.Locate('ID',quFCardINDS.AsInteger,[]) then
          begin
            taSpecINds.AsInteger:=quFCardINDS.AsInteger;
            taSpecSNds.AsString:=taNDSNAMENDS.AsString;
          end;
          taSpecRNds.AsFloat:=0;
          taSpecSumNac.AsFloat:=0;
          taSpecProcNac.AsFloat:=0;
          taSpecTCard.AsInteger:=quFCardTCard.AsInteger;
          taSpec.Post;
          ViewDoc4.EndUpdate;
          AEdit.SelectAll;
          ViewDoc4NameG.Options.Editing:=False;
          ViewDoc4NameG.Focused:=True;
          Key:=#0;
        end;
      end;
    end;
  end;//}
end;

procedure TfmAddDoc4.acAddPosExecute(Sender: TObject);
Var iMax:Integer;
begin
  //�������� �������
  if PageControl1.ActivePageIndex=0 then
  begin
    iMax:=1;
    ViewDoc4.BeginUpdate;

    taSpec.First;
    if not taSpec.Eof then
    begin
      taSpec.Last;
      iMax:=taSpecNum.AsInteger+1;
    end;

    taSpec.Append;
    taSpecNum.AsInteger:=iMax;
    taSpecIdGoods.AsInteger:=0;
    taSpecNameG.AsString:='';
    taSpecIM.AsInteger:=0;
    taSpecSM.AsString:='';
    taSpecKm.AsFloat:=0;
    taSpecQuant.AsFloat:=0;
    taSpecPriceIn.AsFloat:=0;
    taSpecSumIn.AsFloat:=0;
    taSpecPriceR.AsFloat:=0;
    taSpecSumR.AsFloat:=0;
    taSpecINds.AsInteger:=0;
    taSpecSNds.AsString:='';
    taSpecRNds.AsFloat:=0;
    taSpecSumNac.AsFloat:=0;
    taSpecProcNac.AsFloat:=0;
    taSpecOper.AsInteger:=0;
    taSpec.Post;
    ViewDoc4.EndUpdate;
    GridDoc4.SetFocus;

    ViewDoc4NameG.Options.Editing:=True;
    ViewDoc4NameG.Focused:=True;
  end;
  if PageControl1.ActivePageIndex=1 then
  begin
    iMax:=1;
    ViewTara.BeginUpdate;

    taTara.First;
    if not taTara.Eof then
    begin
      taTara.Last;
      iMax:=taTaraNum.AsInteger+1;
    end;

    taTara.Append;
    taTaraNum.AsInteger:=iMax;
    taTaraIdCard.AsInteger:=0;
    taTaraNameG.AsString:='';
    taTaraIM.AsInteger:=0;
    taTaraSM.AsString:='';
    taTaraKm.AsFloat:=0;
    taTaraQuant.AsFloat:=0;
    taTaraPriceIn.AsFloat:=0;
    taTaraSumIn.AsFloat:=0;
    taTaraPriceUch.AsFloat:=0;
    taTaraSumUch.AsFloat:=0;
    taTaraSumNac.AsFloat:=0;
    taTaraProcNac.AsFloat:=0;
    taTara.Post;

    ViewTara.EndUpdate;
    GrTara.SetFocus;

    ViewTaraNameG.Options.Editing:=True;
    ViewTaraNameG.Focused:=True;
  end;
end;

procedure TfmAddDoc4.acDelPosExecute(Sender: TObject);
begin
  //������� �������
  if PageControl1.ActivePageIndex=0 then
  begin
    if taSpec.RecordCount>0 then
    begin
      taSpec.Delete;
    end;
  end;
  if PageControl1.ActivePageIndex=1 then
  begin
    if taTara.RecordCount>0 then
    begin
      taTara.Delete;
    end;
  end;
end;

procedure TfmAddDoc4.acAddListExecute(Sender: TObject);
begin
  bAddSpecR:=True;
  fmGoods.Show;
end;

procedure TfmAddDoc4.cxButton1Click(Sender: TObject);
Var IdH:Integer;
    rSum1,rSum2,rSumT:Real;
begin
  //��������
  with dmO do
  with dmORep do
  begin
    IdH:=cxTextEdit1.Tag;
    if taSpec.State in [dsEdit,dsInsert] then taSpec.Post;
    if taTara.State in [dsEdit,dsInsert] then taTara.Post;
    if cxDateEdit1.Date<3000 then  cxDateEdit1.Date:=Date;
    if cxDateEdit2.Date<3000 then  cxDateEdit2.Date:=Date;

    //������� ������ ������
    ViewDoc4.BeginUpdate;
    ViewTara.BeginUpdate;

    //�������� �� ������� ����
    taSpec.First;
    while not taSpec.Eof do
    begin
      if taSpecQuant.AsFloat<0 then
      begin
        if abs(taSpecSumIn.AsFloat)<0.01 then
        begin
          showmessage('������� - "'+taSpecNameG.AsString+'" ��������� ���� �������.');
          ViewDoc4.EndUpdate;
          ViewTara.EndUpdate;
          exit;
        end;
      end;
      taSpec.Next;
    end;

    if cxTextEdit1.Tag=0 then
    begin
      IDH:=GetId('DocR');
      cxTextEdit1.Tag:=IDH;
      if cxTextEdit1.Text=prGetNum(8,0) then prGetNum(8,1); //��������
    end;

    quDocsRId.Active:=False;
    quDocsRId.ParamByName('IDH').AsInteger:=IDH;
    quDocsRId.Active:=True;

    quDocsRId.First;
    if quDocsRId.RecordCount=0 then quDocsRId.Append else quDocsRId.Edit;

    quDocsRIdID.AsInteger:=IDH;
    quDocsRIdDATEDOC.AsDateTime:=Trunc(cxDateEdit1.Date);
    quDocsRIdNUMDOC.AsString:=cxTextEdit1.Text;
    quDocsRIdDATESF.AsDateTime:=Trunc(cxDateEdit2.Date);
    quDocsRIdNUMSF.AsString:=cxTextEdit2.Text;
    quDocsRIdIDCLI.AsInteger:=cxButtonEdit1.Tag;
    quDocsRIdIDSKL.AsInteger:=cxLookupComboBox1.EditValue;
    quDocsRIdSUMIN.AsFloat:=0;
    quDocsRIdSUMUCH.AsFloat:=0;
    quDocsRIdSUMTAR.AsFloat:=0;
    quDocsRIdSUMNDS0.AsFloat:=sNDS[1];
    quDocsRIdSUMNDS1.AsFloat:=sNDS[2];
    quDocsRIdSUMNDS2.AsFloat:=sNDS[3];
    quDocsRIdPROCNAC.AsFloat:=RoundVal(cxCalcEdit1.EditValue);
    quDocsRIdIACTIVE.AsInteger:=0;
    quDocsRId.Post;

    //�������� ������������
    quSpecRSel.Active:=False;
    quSpecRSel.ParamByName('IDHD').AsInteger:=IDH;
    quSpecRSel.Active:=True;

    quSpecRSel.First; //������� ������
    while not quSpecRSel.Eof do quSpecRSel.Delete;
    quSpecRSel.FullRefresh;

    sNDS[1]:=0;sNDS[2]:=0;sNDS[3]:=0;
    rSum1:=0; rSum2:=0;
    taSpec.First;
    while not taSpec.Eof do
    begin
      if quSpecRSel.Locate('IDCARD',taSpecIdGoods.AsInteger,[])=False then
      begin
        quSpecRSel.Append;
        quSpecRSelIDHEAD.AsInteger:=IDH;
        quSpecRSelID.AsInteger:=taSpecNum.AsInteger;
        quSpecRSelIDCARD.AsInteger:=taSpecIdGoods.AsInteger;
        quSpecRSelQUANT.AsFloat:=taSpecQuant.AsFloat;
        quSpecRSelPRICEIN.AsFloat:=taSpecPriceIn.AsFloat;
        quSpecRSelSUMIN.AsFloat:=RoundVal(taSpecSumIn.AsFloat);
        quSpecRSelPRICER.AsFloat:=taSpecPriceR.AsFloat;
        quSpecRSelSUMR.AsFloat:=RoundVal(taSpecSumR.AsFloat);
        quSpecRSelIDNDS.AsInteger:=taSpecINds.AsInteger;
        quSpecRSelSUMNDS.AsFloat:=RoundVal(taSpecRNds.AsFloat);
        quSpecRSelIDM.AsInteger:=taSpecIM.AsInteger;
        quSpecRSelKM.AsFloat:=taSpecKM.AsFloat;
        quSpecRSelTCARD.AsInteger:=taSpecTCARD.AsInteger;
        quSpecRSelOPER.AsInteger:=taSpecOper.AsInteger;
        quSpecRSel.Post;

        sNDS[taSpecINds.AsInteger]:=sNDS[taSpecINds.AsInteger]+RoundVal(taSpecRNds.AsFloat);
        rSum1:=rSum1+RoundVal(taSpecSumIn.AsFloat); rSum2:=rSum2+RoundVal(taSpecSumR.AsFloat);
      end else
      begin
        showmessage('���������� ������� ���������. ( ��� '+taSpecNum.AsString+', ��� '+taSpecIdGoods.AsString+')');
      end;

      taSpec.Next;
    end;

    quSpecRSel.Active:=False;

    //�������� ����
    quTaraR.Active:=False;
    quTaraR.ParamByName('IDH').AsInteger:=IDH;
    quTaraR.Active:=True;

    quTaraR.First; //������� ������
    while not quTaraR.Eof do quTaraR.Delete;
    quTaraR.FullRefresh;

    rSumT:=0;

    taTara.First;
    while not taTara.Eof do
    begin
      if quTaraR.Locate('IDCARD',taTaraIdCard.AsInteger,[])=False then
      begin
        quTaraR.Append;
        quTaraRIDHEAD.AsInteger:=IDH;
        quTaraRNUM.AsInteger:=taTaraNum.AsInteger;
        quTaraRIDCARD.AsInteger:=taTaraIdCard.AsInteger;
        quTaraRQUANT.AsFloat:=taTaraQuant.AsFloat;
        quTaraRPRICEIN.AsFloat:=taTaraPriceIn.AsFloat;
        quTaraRSUMIN.AsFloat:=RoundVal(taTaraSumIn.AsFloat);
        quTaraRPRICEUCH.AsFloat:=taTaraPriceUch.AsFloat;
        quTaraRSUMUCH.AsFloat:=taTaraSumUch.AsFloat;
        quTaraRIDM.AsInteger:=taTaraIM.AsInteger;
        quTaraRKM.AsFloat:=taTaraKM.asfloat;
        quTaraR.Post;
        rSumT:=rSumT+RoundVal(taTaraSumIn.AsFloat);
      end;
      
      taTara.Next;
    end;

    quTaraR.Active:=False;

    quDocsRId.Edit;
    quDocsRIdSUMIN.AsFloat:=rSum1; //��� ������ ���� ��� ����������� �� ��������
    quDocsRIdSUMUCH.AsFloat:=rSum2;
    quDocsRIdSUMTAR.AsFloat:=rSumT;
    quDocsRIdSUMNDS0.AsFloat:=sNDS[1];
    quDocsRIdSUMNDS1.AsFloat:=sNDS[2];
    quDocsRIdSUMNDS2.AsFloat:=sNDS[3];
    quDocsRIdPROCNAC.AsFloat:=cxCalcEdit1.EditValue;
    quDocsRIdIACTIVE.AsInteger:=0;
    quDocsRId.Post;

    quDocsRId.Active:=False;

    fmDocsReal.ViewDocsR.BeginUpdate;
    quDocsRSel.FullRefresh;
    quDocsRSel.Locate('ID',IDH,[]);
    fmDocsReal.ViewDocsR.EndUpdate;

    ViewDoc4.EndUpdate;
    ViewTara.EndUpdate;

  end;
end;

procedure TfmAddDoc4.acDelAllExecute(Sender: TObject);
begin
  if PageControl1.ActivePageIndex=0 then
  begin
    if taSpec.RecordCount>0 then
    begin
      if MessageDlg('�� ������������� ������ �������� ������������ �������?',
       mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        taSpec.First; while not taSpec.Eof do taSpec.Delete;
      end;
    end;
  end;  
  if PageControl1.ActivePageIndex=1 then
  begin
    if taTara.RecordCount>0 then
    begin
      if MessageDlg('�� ������������� ������ �������� ������������ ����?',
       mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        taTara.First; while not taTara.Eof do taTara.Delete;
      end;
    end;
  end;
end;

procedure TfmAddDoc4.N1Click(Sender: TObject);
Type tRecSpec = record
     Num:INteger;
     IdGoods:Integer;
     NameG:String;
     IM:Integer;
     SM:String;
     Quant,Price1,Price2,Sum1,Sum2:Real;
     iNds:INteger;
     sNds:String;
     rNds,SumNac,ProcNac,Km:Real;
     end;
Var RecSpec:TRecSpec;
    iMax:Integer;
begin
  //���������� �������
  if not taSpec.Eof then
  begin
    RecSpec.Num:=taSpecNum.AsInteger;
    RecSpec.IdGoods:=taSpecIdGoods.AsInteger;
    RecSpec.Quant:=taSpecQuant.AsFloat;
    RecSpec.Price1:=taSpecPriceIn.AsFloat;
    RecSpec.Sum1:=taSpecSumIn.AsFloat;
    RecSpec.Price2:=taSpecPriceR.AsFloat;
    RecSpec.Sum2:=taSpecSumR.AsFloat;
    RecSpec.iNds:=taSpecINds.AsInteger;
    RecSpec.rNds:=taSpecRNds.AsFloat;
    RecSpec.IM:=taSpecIM.AsInteger;
    RecSpec.NameG:=taSpecNameG.AsString;
    RecSpec.SM:=taSpecSM.AsString;
    RecSpec.sNds:=taSpecsNds.AsString;
    RecSpec.SumNac:=taSpecSumNac.AsFloat;
    RecSpec.ProcNac:=taSpecProcNac.AsFloat;
    RecSpec.Km:=taSpecKm.AsFloat;

    iMax:=1;
    ViewDoc4.BeginUpdate;

    taSpec.First;
    if not taSpec.Eof then
    begin
      taSpec.Last;
      iMax:=taSpecNum.AsInteger+1;
    end;

    taSpec.Append;
    taSpecNum.AsInteger:=iMax;
    taSpecIdGoods.AsInteger:=RecSpec.IdGoods;
    taSpecNameG.AsString:=RecSpec.NameG;
    taSpecIM.AsInteger:=RecSpec.IM;
    taSpecSM.AsString:=RecSpec.SM;
    taSpecKm.AsFloat:=RecSpec.Km;
    taSpecQuant.AsFloat:=RecSpec.Quant;
    taSpecPriceIn.AsFloat:=RecSpec.Price1;
    taSpecSumIn.AsFloat:=RecSpec.Sum1;
    taSpecPriceR.AsFloat:=RecSpec.Price2;
    taSpecSumR.AsFloat:=RecSpec.Sum2;
    taSpecINds.AsInteger:=RecSpec.iNds;
    taSpecSNds.AsString:=RecSpec.sNds;
    taSpecRNds.AsFloat:=RecSpec.rNds;
    taSpecSumNac.AsFloat:=RecSpec.SumNac;
    taSpecProcNac.AsFloat:=RecSpec.ProcNac;
    taSpecOper.AsInteger:=0;
    taSpec.Post;
    ViewDoc4.EndUpdate;
    GridDoc4.SetFocus;
  end;
end;

procedure TfmAddDoc4.cxLabel6Click(Sender: TObject);
begin
  acDelall.Execute;
end;

procedure TfmAddDoc4.cxDateEdit1PropertiesChange(Sender: TObject);
begin
  cxDateEdit2.EditValue:=cxDateEdit1.EditValue;
end;

procedure TfmAddDoc4.ViewDoc4SMPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
Var rK:Real;
    iM:Integer;
    Sm:String;
begin
  with dmO do
  begin
    if taSpecIm.AsInteger>0 then
    begin
      bAddSpec:=True;
      fmMessure.ShowModal;
      if fmMessure.ModalResult=mrOk then
      begin
        iM:=iMSel; iMSel:=0;
        Sm:=prFindKNM(iM,rK); //�������� ����� �������� � ����� �����

        taSpec.Edit;
        taSpecIM.AsInteger:=iM;
        taSpecKm.AsFloat:=rK;
        taSpecSM.AsString:=Sm;
        taSpec.Post;
      end;
    end;
  end;
end;

procedure TfmAddDoc4.cxButton2Click(Sender: TObject);
begin
  Close;
end;

procedure TfmAddDoc4.acSaveDocExecute(Sender: TObject);
begin
  cxButton1.Click;
end;

procedure TfmAddDoc4.acExitDocExecute(Sender: TObject);
begin
  cxButton2.Click;
end;

procedure TfmAddDoc4.ViewTaraDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept:=False;
  if bDrR then  Accept:=True;
end;

procedure TfmAddDoc4.ViewTaraDragDrop(Sender, Source: TObject; X,
  Y: Integer);
Var iCo:Integer;
    i,j: Integer;
    iNum: Integer;
    Rec:TcxCustomGridRecord;
    iMax:Integer;
begin
//
  if bDrR then
  begin
    ResetAddVars;
    iCo:=fmGoods.ViewGoods.Controller.SelectedRecordCount;
    if iCo>0 then
    begin
      if MessageDlg('�� ������������� ������ ����������� ��������� ������� ('+IntToStr(iCo)+' ��.) � ������������ ���������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        iMax:=1;
        fmAddDoc4.taTara.First;
        if not fmAddDoc4.taTara.Eof then
        begin
          fmAddDoc4.taTara.Last;
          iMax:=fmAddDoc4.taTaraNum.AsInteger+1;
        end;

        with dmO do
        begin
          ViewTara.BeginUpdate;
          for i:=0 to fmGoods.ViewGoods.Controller.SelectedRecordCount-1 do
          begin
            Rec:=fmGoods.ViewGoods.Controller.SelectedRecords[i];

            for j:=0 to Rec.ValueCount-1 do
            begin
              if fmGoods.ViewGoods.Columns[j].Name='ViewGoodsID' then break;
            end;

            iNum:=Rec.Values[j];
          //��� ��� - ����������

            if quCardsSel.Locate('ID',iNum,[]) then
            begin
              taTara.Append;
              taTaraNum.AsInteger:=iMax;
              taTaraIdCard.AsInteger:=quCardsSelID.AsInteger;
              taTaraNameG.AsString:=quCardsSelNAME.AsString;
              taTaraIM.AsInteger:=quCardsSelIMESSURE.AsInteger;
              taTaraSM.AsString:=quCardsSelNAMESHORT.AsString;
              taTaraKM.AsFloat:=quCardsSelKOEF.AsFloat;
              taTaraQuant.AsFloat:=1;
              taTaraPriceIn.AsFloat:=0;
              taTaraSumIn.AsFloat:=0;
              taTaraPriceUch.AsFloat:=0;
              taTaraSumUch.AsFloat:=0;
              taTaraSumNac.AsFloat:=0;
              taTaraProcNac.AsFloat:=0;
              taTara.Post;
              inc(iMax);
            end;
          end;
          ViewTara.EndUpdate;
        end;
      end;
    end;
  end;
end;

procedure TfmAddDoc4.ViewTaraEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  iCol:=0;
  if ViewTara.Controller.FocusedColumn.Name='ViewTaraQuant' then iCol:=1;
  if ViewTara.Controller.FocusedColumn.Name='ViewTaraPriceIn' then iCol:=2;
  if ViewTara.Controller.FocusedColumn.Name='ViewTaraSumIn' then iCol:=3;
  if ViewTara.Controller.FocusedColumn.Name='ViewTaraPriceUch' then iCol:=4;
  if ViewTara.Controller.FocusedColumn.Name='ViewTaraSumUch' then iCol:=5;
end;

procedure TfmAddDoc4.ViewTaraEditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
Var iCode:Integer;
    Km:Real;
    sName:String;
    //���� ������ ��� ������ ����� �� ������
    rK:Real;
    iM:Integer;
    Sm:String;
    //---
begin
  with dmO do
  begin
    if (Key=$0D) then
    begin
      if ViewTara.Controller.FocusedColumn.Name='ViewTaraIdCard' then
      begin
        iCode:=VarAsType(AEdit.EditingValue, varInteger);

        quFCard.Active:=False;
        quFCard.SelectSQL.Clear;
        quFCard.SelectSQL.Add('SELECT ID,NAME,IMESSURE,PARENT,TCARD,INDS,LASTPRICEOUT');
        quFCard.SelectSQL.Add('FROM OF_CARDS');
        quFCard.SelectSQL.Add('where ID='+IntToStr(iCode));
        quFCard.Active:=True;

        if quFCard.RecordCount=1 then
        begin
          ViewTara.BeginUpdate;
          taTara.Edit;
          taTaraNameG.AsString:=quFCardNAME.AsString;
          taTaraIM.AsInteger:=quFCardIMESSURE.AsInteger;
          taTaraSM.AsString:=prFindKNM(quFCardIMESSURE.AsInteger,Km);
          taTaraKm.AsFloat:=Km;
          taTaraIdCard.AsInteger:=iCode;
          taTaraQuant.AsFloat:=1;
          taTaraPriceIn.AsFloat:=0;
          taTaraSumIn.AsFloat:=0;
          taTaraPriceUch.AsFloat:=0;
          taTaraSumUch.AsFloat:=0;
          taTaraSumNac.AsFloat:=0;
          taTaraProcNac.AsFloat:=0;
          taTara.Post;

          ViewTara.EndUpdate;
        end;
      end;
      if ViewTara.Controller.FocusedColumn.Name='ViewTaraNameG' then
      begin
        sName:=VarAsType(AEdit.EditingValue, varString);

        quFCard.Active:=False;
        quFCard.SelectSQL.Clear;
        quFCard.SelectSQL.Add('SELECT ID,NAME,IMESSURE,PARENT,TCARD,INDS,LASTPRICEOUT');
        quFCard.SelectSQL.Add('FROM OF_CARDS');
        quFCard.SelectSQL.Add('where UPPER(NAME) like ''%'+AnsiUpperCase(sName)+'%''');
        quFCard.SelectSQL.Add('Order by NAME');

        quFCard.Active:=True;

        bAdd:=True;
        //�������� ����� ������ � ����� ������
        fmFCards.ShowModal;
        if fmFCards.ModalResult=mrOk then
        begin
          if quFCard.RecordCount>0 then
          begin
            ViewTara.BeginUpdate;
            taTara.Edit;
            taTaraIdCard.AsInteger:=quFCardID.AsInteger;
            taTaraNameG.AsString:=quFCardNAME.AsString;
            taTaraIM.AsInteger:=quFCardIMESSURE.AsInteger;
            taTaraSM.AsString:=prFindKNM(quFCardIMESSURE.AsInteger,Km);
            taTaraKm.AsFloat:=Km;
            taTaraQuant.AsFloat:=1;
            taTaraPriceIn.AsFloat:=0;
            taTaraSumIn.AsFloat:=0;
            taTaraPriceUch.AsFloat:=0;
            taTaraSumUch.AsFloat:=0;
            taTaraSumNac.AsFloat:=0;
            taTaraProcNac.AsFloat:=0;
            taTara.Post;
            ViewTara.EndUpdate;
            AEdit.SelectAll;
          end;
        end;
        bAdd:=False;
      end;
      if ViewTara.Controller.FocusedColumn.Name='ViewTaraSM' then
      begin
        if taTaraIm.AsInteger>0 then
        begin
          bAddSpec:=True;
          fmMessure.ShowModal;
          if fmMessure.ModalResult=mrOk then
          begin
            iM:=iMSel; iMSel:=0;
            Sm:=prFindKNM(iM,rK); //�������� ����� �������� � ����� �����

            taTara.Edit;
            taTaraIM.AsInteger:=iM;
            taTaraKm.AsFloat:=rK;
            taTaraSM.AsString:=Sm;
            taTara.Post;
          end;
        end;
      end;
    end else
      if ViewTara.Controller.FocusedColumn.Name='ViewTaraIdGoods' then
        if fTestKey(Key)=False then
          if taTara.State in [dsEdit,dsInsert] then taTara.Cancel;
  end;
end;

procedure TfmAddDoc4.ViewTaraEditKeyPress(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
Var Km:Real;
    sName:String;
begin
  if bAdd then exit;
  with dmO do
  begin
    if (ViewTara.Controller.FocusedColumn.Name='ViewTaraNameG') then
    begin
      sName:=VarAsType(AEdit.EditingValue ,varString)+Key;
//      Label2.Caption:=sName;
      if sName>'' then
      begin
        quFCard.Active:=False;
        quFCard.SelectSQL.Clear;
        quFCard.SelectSQL.Add('SELECT  first 2 ID,NAME,IMESSURE,PARENT,TCARD,INDS,LASTPRICEOUT');
        quFCard.SelectSQL.Add('FROM OF_CARDS');
        quFCard.SelectSQL.Add('where UPPER(NAME) like ''%'+AnsiUpperCase(sName)+'%''');
        quFCard.SelectSQL.Add('Order by NAME');
        quFCard.Active:=True;

        if quFCard.RecordCount=1 then
        begin
          ViewTara.BeginUpdate;
          taTara.Edit;
          taTaraIdCard.AsInteger:=quFCardID.AsInteger;
          taTaraNameG.AsString:=quFCardNAME.AsString;
          taTaraIM.AsInteger:=quFCardIMESSURE.AsInteger;
          taTaraSM.AsString:=prFindKNM(quFCardIMESSURE.AsInteger,Km);
          taTaraKm.AsFloat:=Km;
          taTaraQuant.AsFloat:=0;
          taTaraPriceIn.AsFloat:=0;
          taTaraSumIn.AsFloat:=0;
          taTaraPriceUch.AsFloat:=0;
          taTaraSumUch.AsFloat:=0;
          taTaraSumNac.AsFloat:=0;
          taTaraProcNac.AsFloat:=0;
          taTara.Post;
          ViewTara.EndUpdate;
          AEdit.SelectAll;
          ViewTaraNameG.Options.Editing:=False;
          ViewTaraNameG.Focused:=True;
          Key:=#0;
        end;
      end;
    end;
  end;//}
end;

procedure TfmAddDoc4.taTaraQuantChange(Sender: TField);
begin
  //���������� �����
  if iCol=1 then
  begin
    taTaraSumIn.AsFloat:=taTaraPriceIn.AsFloat*taTaraQuant.AsFloat;
    taTaraSumUch.AsFloat:=taTaraPriceUch.AsFloat*taTaraQuant.AsFloat;
    taTaraSumNac.AsFloat:=taTaraSumUch.AsFloat-taTaraSumIn.AsFloat;
    if taTaraSumIn.AsFloat>0.01 then
    begin
      taTaraProcNac.AsFloat:=(taTaraSumUch.AsFloat-taTaraSumIn.AsFloat)/taTaraSumIn.AsFloat*100;
    end;
  end;
end;

procedure TfmAddDoc4.taTaraPriceInChange(Sender: TField);
begin
  //���������� ����
  if iCol=2 then
  begin
    taTaraSumIn.AsFloat:=taTaraPriceIn.AsFloat*taTaraQuant.AsFloat;
//  taTaraSumUch.AsFloat:=taTaraPriceUch.AsFloat*taTaraQuant.AsFloat;
    taTaraSumNac.AsFloat:=taTaraSumUch.AsFloat-taTaraSumIn.AsFloat;
    if taTaraSumIn.AsFloat>0.01 then
    begin
      taTaraProcNac.AsFloat:=(taTaraSumUch.AsFloat-taTaraSumIn.AsFloat)/taTaraSumIn.AsFloat*100;
    end;
  end;

end;

procedure TfmAddDoc4.taTaraSumInChange(Sender: TField);
begin
  if iCol=3 then //���������� �����
  begin
    if abs(taTaraQuant.AsFloat)>0 then taTaraPriceIn.AsFloat:=RoundEx(taTaraSumIn.AsFloat/taTaraQuant.AsFloat*100)/100;
    taTaraSumNac.AsFloat:=taTaraSumUch.AsFloat-taTaraSumIn.AsFloat;
    if taTaraSumIn.AsFloat>0.01 then
    begin
      taTaraProcNac.AsFloat:=(taTaraSumUch.AsFloat-taTaraSumIn.AsFloat)/taTaraSumIn.AsFloat*100;
    end;
  end;
end;

procedure TfmAddDoc4.taTaraPriceUchChange(Sender: TField);
begin
  //���������� ����
  if iCol=4 then
  begin
//  taTaraSumIn.AsFloat:=taTaraPriceIn.AsFloat*taTaraQuant.AsFloat;
    taTaraSumUch.AsFloat:=taTaraPriceUch.AsFloat*taTaraQuant.AsFloat;
    taTaraSumNac.AsFloat:=taTaraSumUch.AsFloat-taTaraSumIn.AsFloat;
    if taTaraSumIn.AsFloat>0.01 then
    begin
      taTaraProcNac.AsFloat:=(taTaraSumUch.AsFloat-taTaraSumIn.AsFloat)/taTaraSumIn.AsFloat*100;
    end;  
  end;
end;

procedure TfmAddDoc4.taTaraSumUchChange(Sender: TField);
begin
  if iCol=5 then
  begin
    if abs(taTaraQuant.AsFloat)>0 then taTaraPriceUch.AsFloat:=RoundEx(taTaraSumUch.AsFloat/taTaraQuant.AsFloat*100)/100;
    taTaraSumNac.AsFloat:=taTaraSumUch.AsFloat-taTaraSumIn.AsFloat;
    if taTaraSumIn.AsFloat>0.01 then
    begin
      taTaraProcNac.AsFloat:=(taTaraSumUch.AsFloat-taTaraSumIn.AsFloat)/taTaraSumIn.AsFloat*100;
    end;
  end;
end;

procedure TfmAddDoc4.ViewTaraInSMPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
Var rK:Real;
    iM:Integer;
    Sm:String;
begin
  with dmO do
  begin
    if taTaraIm.AsInteger>0 then
    begin
      bAddSpec:=True;
      fmMessure.ShowModal;
      if fmMessure.ModalResult=mrOk then
      begin
        iM:=iMSel; iMSel:=0;
        Sm:=prFindKNM(iM,rK); //�������� ����� �������� � ����� �����

        taTara.Edit;
        taTaraIM.AsInteger:=iM;
        taTaraKm.AsFloat:=rK;
        taTaraSM.AsString:=Sm;
        taTara.Post;
      end;
    end;
  end;
end;

procedure TfmAddDoc4.cxButtonEdit1KeyPress(Sender: TObject; var Key: Char);
Var sName:String;
begin
  sName:=cxButtonEdit1.Text+Key;
  if Length(sName)>1 then
  begin
    with dmORep do
    begin
      quFindCli.Close;
      quFindCli.SelectSQL.Clear;
      quFindCli.SelectSQL.Add('SELECT first 2 ID,NAMECL');
      quFindCli.SelectSQL.Add('FROM OF_CLIENTS');
      quFindCli.SelectSQL.Add('where UPPER(NAMECL) like ''%'+AnsiUpperCase(sName)+'%''');
      quFindCli.Open;
      if quFindCli.RecordCount=1 then
      begin
        cxButtonEdit1.Text:=quFindCliNAMECL.AsString;
        cxButtonEdit1.Tag:=quFindCliID.AsInteger;
        Key:=#0;
      end;
      quFindCli.Close;
    end;
  end;
end;

procedure TfmAddDoc4.Excel1Click(Sender: TObject);
begin
  prNExportExel4(ViewDoc4);
end;

end.
