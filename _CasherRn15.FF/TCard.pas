unit TCard;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxImageComboBox, Menus,
  cxLookAndFeelPainters, cxMaskEdit, cxSpinEdit, StdCtrls, cxContainer,
  cxTextEdit, cxButtons, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxControls,
  cxGridCustomView, cxGrid, ExtCtrls, cxDropDownEdit, cxCalc, cxLabel,
  Placemnt, cxDBLookupComboBox, FIBDataSet, pFIBDataSet, FIBDatabase,
  pFIBDatabase, cxButtonEdit, cxCalendar, DBClient;

type
  TTK = Record
  Add,Edit:Boolean
  end;

  TfmTCard = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    GTCards: TcxGrid;
    ViewTCards: TcxGridDBTableView;
    LTCards: TcxGridLevel;
    cxButton4: TcxButton;
    cxButton5: TcxButton;
    Panel2: TPanel;
    Panel3: TPanel;
    cxTextEdit1: TcxTextEdit;
    cxTextEdit2: TcxTextEdit;
    Label1: TLabel;
    Label2: TLabel;
    cxTextEdit3: TcxTextEdit;
    Label3: TLabel;
    cxTextEdit4: TcxTextEdit;
    Label4: TLabel;
    cxSpinEdit1: TcxSpinEdit;
    Label5: TLabel;
    cxCalcEdit1: TcxCalcEdit;
    Label6: TLabel;
    cxButton2: TcxButton;
    Image1: TImage;
    cxLabel1: TcxLabel;
    Image2: TImage;
    cxLabel2: TcxLabel;
    GTSpec: TcxGrid;
    ViewTSpec: TcxGridDBTableView;
    LTSpec: TcxGridLevel;
    ViewTCardsIDCARD: TcxGridDBColumn;
    ViewTCardsID: TcxGridDBColumn;
    ViewTCardsDATEB: TcxGridDBColumn;
    ViewTCardsDATEE: TcxGridDBColumn;
    ViewTCardsSHORTNAME: TcxGridDBColumn;
    ViewTCardsRECEIPTNUM: TcxGridDBColumn;
    ViewTCardsPOUTPUT: TcxGridDBColumn;
    ViewTCardsPCOUNT: TcxGridDBColumn;
    ViewTCardsPVES: TcxGridDBColumn;
    ViewTSpecIDCARD: TcxGridDBColumn;
    ViewTSpecNETTO: TcxGridDBColumn;
    ViewTSpecBRUTTO: TcxGridDBColumn;
    ViewTSpecNAME: TcxGridDBColumn;
    ViewTSpecNAMESHORT: TcxGridDBColumn;
    cxButton6: TcxButton;
    Timer1: TTimer;
    cxButton3: TcxButton;
    cxButton1: TcxButton;
    FormPlacement1: TFormPlacement;
    ViewTSpecCURMESSURE: TcxGridDBColumn;
    ViewTCardsSDATEE: TcxGridDBColumn;
    Label7: TLabel;
    cxDateEdit1: TcxDateEdit;
    Image3: TImage;
    cxLabel3: TcxLabel;
    cxButton7: TcxButton;
    Label8: TLabel;
    PopupMenuTCard: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    TCS: TClientDataSet;
    TCH: TClientDataSet;
    TCHIDCARD: TIntegerField;
    TCHID: TIntegerField;
    TCHDATEB: TDateField;
    TCHDATEE: TDateField;
    TCHSHORTNAME: TStringField;
    TCHRECEIPTNUM: TStringField;
    TCHPOUTPUT: TStringField;
    TCHPCOUNT: TIntegerField;
    TCHPVES: TFloatField;
    TCSIDC: TIntegerField;
    TCSIDT: TIntegerField;
    TCSID: TIntegerField;
    TCSIDCARD: TIntegerField;
    TCSCURMESSURE: TIntegerField;
    TCSNETTO: TFloatField;
    TCSBRUTTO: TFloatField;
    TCSKNB: TFloatField;
    dsTCH: TDataSource;
    dsTCS: TDataSource;
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxTextEdit2PropertiesChange(Sender: TObject);
    procedure cxTextEdit3PropertiesChange(Sender: TObject);
    procedure cxTextEdit4PropertiesChange(Sender: TObject);
    procedure cxSpinEdit1PropertiesChange(Sender: TObject);
    procedure cxCalcEdit1PropertiesChange(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
    procedure ViewTCardsFocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure cxButton5Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxLabel1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ViewTSpecNAMESHORTPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxLabel2Click(Sender: TObject);
    procedure Label7Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton7Click(Sender: TObject);
    procedure cxLabel3Click(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure  prSetVal;
    procedure  prSetOn;


  end;

var
  fmTCard: TfmTCard;
  TK: TTK;
  bClearTK:Boolean = False;

implementation

uses dmOffice, Un1, PeriodUni, Goods, FindResult, CurMessure, GoodsSel,
  CalcSeb, TBuff;

{$R *.dfm}
procedure  TfmTCard.prSetOn;
begin
  cxButton1.Enabled:=True;
end;

procedure  TfmTCard.prSetVal;
var kBrutto:Real;
    iDate:Integer;
begin
  with dmO do
  begin
    cxTextEdit2.Text:=quTCardsSHORTNAME.AsString;
    cxTextEdit3.Text:=quTCardsRECEIPTNUM.AsString;
    cxTextEdit4.Text:=quTCardsPOUTPUT.AsString;
    cxSpinEdit1.EditValue:=quTCardsPCOUNT.AsInteger;
    cxCalcEdit1.EditValue:=quTCardsPVES.AsFloat;

    ViewTSpec.BeginUpdate;
    quTSpec.Active:=False;
    quTSpec.ParamByName('IDC').AsInteger:=quTCardsIDCARD.AsInteger;
    quTSpec.ParamByName('IDTC').AsInteger:=quTCardsID.AsInteger;
    quTSpec.Active:=True;

    ViewTSpecBRUTTO.Caption:='������ �� '+FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date);
    iDate:=prDateToI(cxDateEdit1.Date);

    quTSpec.First;
    while not quTSpec.Eof do
    begin
      //���������� ������
      kBrutto:=0;
      quFindEU.Active:=False;
      quFindEU.ParamByName('GOODSID').AsInteger:=quTSpecIDCARD.AsInteger;
      quFindEU.ParamByName('DATEB').AsInteger:=iDate;
      quFindEU.ParamByName('DATEE').AsInteger:=iDate;
      quFindEU.Active:=True;

      if quFindEU.RecordCount>0 then
      begin
        quFindEU.First;
        kBrutto:=quFindEUTO100GRAMM.AsFloat;
      end;

      quFindEU.Active:=False;

      quTSpec.Edit;
      quTSpecBRUTTO.AsFloat:=quTSpecNETTO.AsFloat*(100+kBrutto)/100;
      quTSpecKNB.AsFloat:=kBrutto;
      quTSpec.Post;

      quTSpec.Next;
    end;

    ViewTSpec.EndUpdate;
    cxButton1.Enabled:=False;

    ViewTSpec.OptionsData.Editing:=False;
    cxLabel1.Enabled:=False;
    cxLabel2.Enabled:=False;
    Image1.Enabled:=False;
    Image2.Enabled:=False;

  end;
end;

procedure TfmTCard.cxButton2Click(Sender: TObject);
begin
  close;
end;

procedure TfmTCard.Timer1Timer(Sender: TObject);
begin
  if bClearTK=True then begin StatusBar1.Panels[0].Text:='';bClearTK:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearTK:=True;
end;

procedure TfmTCard.cxButton4Click(Sender: TObject);
Var IId:Integer;
    DateB,DateE:TDateTime;
    iDate,iC:Integer;
begin
  //���������� ��
  if not CanDo('prAddTK') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  with dmO do
  begin


    fmPeriodUni.DateTimePicker1.Date:=Date;
    fmPeriodUni.DateTimePicker2.Date:=iMaxDate;
    fmPeriodUni.ShowModal;
    if fmPeriodUni.ModalResult=mrOk then
    begin
      DateB:=Trunc(fmPeriodUni.DateTimePicker1.Date);
      DateE:=Trunc(fmPeriodUni.DateTimePicker2.Date)+1;

      //�������� �� �������������

      prTestUseTC(cxTextEdit1.Tag,iDate,iC);
      if Trunc(DateB)<=iDate then
      begin //� ������ �������� ���� ������������� - �������� ����������
        Showmessage('�� ���� � �������� (���� - '+FormatDateTime('dd.mm.yyyy',iDate)+', ��� ����� - '+IntToStr(iC)+'). ������� ������ ������ �������� !!!');
        exit;
      end;

      if quTCards.RecordCount>0 then
      begin //�������� ���� ��������� �������� ���� ���������� �� ��������� �� �������
        quTCards.Last;
        prSetVal;
        if quTCardsDATEB.AsDateTime>=DateB then
        begin
          showmessage('������ ������ ��� ���������, ���������� ����������.');
          exit;
        end;
        if quTCardsDATEE.AsDateTime>=iMaxDate then
        begin
          quTCards.Edit;
          quTCardsDATEE.AsDateTime:=Trunc(DateB-1);
          quTCards.Post;
        end;
      end;

      iId:=GetId('TCards');

      quTCards.Append;
      quTCardsIDCARD.AsInteger:=cxTextEdit1.Tag;
      quTCardsID.AsInteger:=IId;
      quTCardsDATEB.AsDateTime:=Trunc(DateB);
      quTCardsDATEE.AsDateTime:=Trunc(DateE-1); //��� ������������
      quTCardsSHORTNAME.AsString:=cxTextEdit1.Text;
      quTCardsRECEIPTNUM.AsString:='';
      quTCardsPOUTPUT.AsString:='';
      quTCardsPCOUNT.AsInteger:=1;
      quTCardsPVES.AsFloat:=0;
      quTCards.Post;

      if quCardsSelTCARD.AsInteger=0 then
      begin
        quCardsSel.Edit;
        quCardsSelTCARD.AsInteger:=1;
        quCardsSel.Post;
        quCardsSel.Refresh;
      end;

      if Panel3.Visible=False then Panel3.Visible:=True;
      prSetVal;
//      prSetOn;

    end;
  end;
end;

procedure TfmTCard.cxButton1Click(Sender: TObject);
begin
  cxButton1.Enabled:=False;
  with dmO do
  begin
    quTCards.Edit;
    quTCardsSHORTNAME.AsString:=cxTextEdit2.Text;
    quTCardsRECEIPTNUM.AsString:=cxTextEdit3.Text;
    quTCardsPOUTPUT.AsString:=cxTextEdit4.Text;
    quTCardsPCOUNT.AsInteger:=cxSpinEdit1.EditValue;
    quTCardsPVES.AsFloat:=cxCalcEdit1.EditValue;
    quTCards.Post;
  end;
end;

procedure TfmTCard.cxTextEdit2PropertiesChange(Sender: TObject);
begin
  prSetOn;

end;

procedure TfmTCard.cxTextEdit3PropertiesChange(Sender: TObject);
begin
  prSetOn;

end;

procedure TfmTCard.cxTextEdit4PropertiesChange(Sender: TObject);
begin
  prSetOn;

end;

procedure TfmTCard.cxSpinEdit1PropertiesChange(Sender: TObject);
begin
  prSetOn;
end;

procedure TfmTCard.cxCalcEdit1PropertiesChange(Sender: TObject);
begin
  prSetOn;
end;

procedure TfmTCard.cxButton6Click(Sender: TObject);
begin
  // ������������� ������
  if not CanDo('prEditTKPeriod') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  with dmO do
  begin
    CommonSet.DateFrom:=Trunc(quTCardsDATEB.AsDateTime);
    CommonSet.DateTo:=Trunc(quTCardsDATEE.AsDateTime)+1;

    fmPeriodUni.DateTimePicker1.Date:=CommonSet.DateFrom;
    fmPeriodUni.DateTimePicker2.Date:=CommonSet.DateTo-1;
    fmPeriodUni.ShowModal;
    if fmPeriodUni.ModalResult=mrOk then
    begin
      CommonSet.DateFrom:=Trunc(fmPeriodUni.DateTimePicker1.Date);
      CommonSet.DateTo:=Trunc(fmPeriodUni.DateTimePicker2.Date)+1;

      quTCards.Edit;
      quTCardsDATEB.AsDateTime:=Trunc(CommonSet.DateFrom);
      quTCardsDATEE.AsDateTime:=Trunc(CommonSet.DateTo-1); //��� ������������
      quTCards.Post;
    end;
  end;
end;

procedure TfmTCard.ViewTCardsFocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord;
  ANewItemRecordFocusingChanged: Boolean);
begin
  if Visible then prSetVal;
end;

procedure TfmTCard.cxButton5Click(Sender: TObject);
Var i,iDate,iC:Integer;
begin
  //������� ��
  if not CanDo('prDelTK') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  with dmO do
  begin
    if quTCards.RecordCount>0 then
    begin
          //�������� �� ��������� ��������
      prTestUseTC(quTCardsIDCARD.AsInteger,iDate,iC);
      if Trunc(quTCardsDATEB.AsDateTime)<=iDate then
      begin //� ������ �������� ���� ������������� - �������� ����������
        Showmessage('�� ���� � �������� (���� - '+FormatDateTime('dd.mm.yyyy',iDate)+', ��� ����� - '+IntToStr(iC)+'). �������� ���������� !!!');
        exit;
      end;

    if MessageDlg('�� ������������� ������ ������� �� ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      quTCards.Delete;
      if quTCards.RecordCount=0 then
      begin
        Panel3.Visible:=False;

        //����� ������� ���� ����� �������� � ��������� ��
        if quCardsSel.locate('ID',cxTextEdit1.Tag,[]) = False then
        begin
          quFind.Active:=False;
          quFind.SelectSQL.Clear;
          quFind.SelectSQL.Add('SELECT ID,PARENT,NAME');
          quFind.SelectSQL.Add('FROM OF_CARDS');
          quFind.SelectSQL.Add('where ID ='+IntToStr(cxTextEdit1.Tag));
          quFind.Active:=True;

          if quFind.RecordCount>0 then
          begin
            for i:=0 to fmGoods.ClassTree.Items.Count-1 Do
            if Integer(fmGoods.ClassTree.Items[i].Data) = quFindPARENT.AsInteger Then
            begin
              fmGoods.ClassTree.Items[i].Expand(False);
              fmGoods.ClassTree.Repaint;
              fmGoods.ClassTree.Items[i].Selected:=True;
              Break;
            End;
            delay(10);
            quCardsSel.First;
            if quCardsSel.locate('ID',cxTextEdit1.Tag,[]) then
            begin
              quCardsSel.Edit;
              quCardsSelTCARD.AsInteger:=0;
              quCardsSel.Post;
              quCardsSel.Refresh;
            end;
          end;

        end else
        begin
          quCardsSel.Edit;
          quCardsSelTCARD.AsInteger:=0;
          quCardsSel.Post;
          quCardsSel.Refresh;
        end;
      end;
    end;
    end;
  end;
end;

procedure TfmTCard.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  ViewTSpec.RestoreFromIniFile(CurDir+GridIni);

end;

procedure TfmTCard.cxLabel1Click(Sender: TObject);
begin
  //�������� �������
  with dmO do
  begin
    if quTCards.RecordCount>0 then
    begin
      bAddTSpec:=True;
      quCardsSel1.Active:=False;
      quCardsSel1.Active:=True;
      fmGoodsSel.Show;
    end else showmessage('�������� ������� ��.');
  end;
end;

procedure TfmTCard.FormShow(Sender: TObject);
begin
  cxDateEdit1.Date:=Date;
end;

procedure TfmTCard.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewTSpec.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmTCard.ViewTSpecNAMESHORTPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
Var rN,rB,rK,rKOld:Real;
    Id:Integer;
begin
  with dmO do
  begin
    if quTSpec.RecordCount>0 then
    begin
      quMessureSel.Active:=False;
      quMessureSel.ParamByName('IDM').AsInteger:=quTSpecCURMESSURE.AsInteger;
      quMessureSel.Active:=True;

      quMessureSel.Locate('ID',quTSpecCURMESSURE.AsInteger,[]);

      fmCurMessure.ShowModal;
      if fmCurMessure.ModalResult=mrOk then
      begin
        rN:=quTSpecNETTO.AsFloat;
        rB:=quTSpecBRUTTO.AsFloat;
        rK:=quMessureSelKOEF.AsFloat;
        rKOld:=quTSpecKOEF.AsFloat;
        Id:=quTSpecID.AsInteger;

        quTSpec.Edit;
        quTSpecCURMESSURE.AsInteger:=quMessureSelID.AsInteger;
        if (rK=0) or (rN=0) then
        begin
          quTSpecNETTO.AsFloat:=0;
          quTSpecBRUTTO.AsFloat:=0;
        end else
        begin
          quTSpecNETTO.AsFloat:=RoundEx(rN*rKOld/rK*1000)/1000;
          quTSpecBRUTTO.AsFloat:=(rB/rN)*RoundEx(rN*rKOld/rK*1000)/1000;
        end;
//        quTSpecNAMESHORT.AsString:=quMessureSelNAMESHORT.AsString;
        quTSpec.Post;
        quTSpec.FullRefresh;
        quTSpec.Locate('ID',ID,[]);
      end;
      quMessureSel.Active:=False;
    end;
  end;
end;

procedure TfmTCard.cxLabel2Click(Sender: TObject);
begin
  with dmO do
  begin
    if quTSpec.RecordCount>0 then
    begin
      if MessageDlg('�� ������������� ������ ������� "'+quTSpecNAME.AsString+'" ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        quTSpec.Delete;
      end;
    end;
  end;
end;

procedure TfmTCard.Label7Click(Sender: TObject);
begin
  prSetVal;
end;

procedure TfmTCard.cxButton3Click(Sender: TObject);
begin
//�������������
  fmCalcSeb:=tfmCalcSeb.Create(Application);
  fmCalcSeb.Label4.Caption:=cxTextEdit1.Text;
  fmCalcSeb.Label1.Caption:=FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date);
  fmCalcSeb.ShowModal;
  fmCalcSeb.quMHAll.Active:=False;
  fmCalcSeb.taSeb.Active:=False;

  fmCalcSeb.Release;
end;

procedure TfmTCard.cxButton7Click(Sender: TObject);
Var iDate,iC:Integer;
begin
  //�������� ���������
  prTestUseTC(dmO.quTCardsIDCARD.AsInteger,iDate,iC);
  Label8.Caption:=IntToStr(iDate)+'   '+IntToStr(iC);
end;

procedure TfmTCard.cxLabel3Click(Sender: TObject);
Var iDate,iC:Integer;
begin
  //�������� �� ����������� ��������������
  with dmO do
  begin
    prTestUseTC(quTCardsIDCARD.AsInteger,iDate,iC);
    if Trunc(quTCardsDATEB.AsDateTime)<=iDate then
    begin //� ������ �������� ���� ������������� - �������� ����������
      Showmessage('�� ���� � �������� (���� - '+FormatDateTime('dd.mm.yyyy',iDate)+', ��� ����� - '+IntToStr(iC)+'). �������������� ���������� !!!');
      exit;
    end
    else
    begin
      fmTCard.ViewTSpec.OptionsData.Editing:=True;
      cxLabel1.Enabled:=True;
      cxLabel2.Enabled:=True;
      Image1.Enabled:=True;
      Image2.Enabled:=True;
    end;
  end;
end;

procedure TfmTCard.N1Click(Sender: TObject);
begin
  //���������� ��
  with dmO do
  begin
    if quTCards.RecordCount=0 then exit;

    TCH.Active:=False;
    TCH.FileName:=CurDir+'TCH.cds';
    if FileExists(CurDir+'TCH.cds') then TCH.Active:=True
    else TCH.CreateDataSet;

    TCS.Active:=False;
    TCS.FileName:=CurDir+'TCS.cds';
    if FileExists(CurDir+'TCS.cds') then TCS.Active:=True
    else TCS.CreateDataSet;

    if TCH.Locate('ID',quTCardsID.AsInteger,[])=False
    then
    begin
      TCH.Append;
      TCHIDCARD.AsInteger:=quTCardsIDCARD.AsInteger;
      TCHID.AsInteger:=quTCardsID.AsInteger;
      TCHDATEB.AsDateTime:=quTCardsDATEB.AsDateTime;
      TCHDATEE.AsDateTime:=quTCardsDATEE.AsDateTime;
      TCHSHORTNAME.AsString:=quTCardsSHORTNAME.AsString;
      TCHRECEIPTNUM.AsString:=quTCardsRECEIPTNUM.AsString;
      TCHPOUTPUT.AsString:=quTCardsPOUTPUT.AsString;
      TCHPCOUNT.AsInteger:=quTCardsPCOUNT.AsInteger;
      TCHPVES.AsFloat:=quTCardsPVES.AsFloat;
      TCH.Post;

      quTSpec.First;
      while not quTSpec.Eof do
      begin
        TCS.Append;
        TCSIDC.AsInteger:=quTSpecIDC.AsInteger;
        TCSIDT.AsInteger:=quTSpecIDT.AsInteger;
        TCSID.AsInteger:=quTSpecID.AsInteger;
        TCSIDCARD.AsInteger:=quTSpecIDCARD.AsInteger;
        TCSCURMESSURE.AsInteger:=quTSpecCURMESSURE.AsInteger;
        TCSNETTO.AsFloat:=quTSpecNETTO.AsFloat;
        TCSBRUTTO.AsFloat:=quTSpecBRUTTO.AsFloat;
        TCSKNB.AsFloat:=quTSpecKNB.AsFloat;
        TCS.Post;

        quTSpec.Next;
      end;

    end else
    begin
      showmessage('�� "'+quTCardsSHORTNAME.AsString+'"  ��� ���� � ������.');
    end;
  end;

  TCH.Active:=False;
  TCS.Active:=False;
end;

procedure TfmTCard.N2Click(Sender: TObject);
Var IId:Integer;
    DateB,DateE:TDateTime;
    iDate,iC,iCurDate:Integer;
    bAdd:Boolean;
    kBrutto:Real;
begin
  // ��������
  TCH.Active:=False;
  TCH.FileName:=CurDir+'TCH.cds';
  if FileExists(CurDir+'TCH.cds') then TCH.Active:=True
  else TCH.CreateDataSet;

  TCS.Active:=False;
  TCS.FileName:=CurDir+'TCS.cds';
  if FileExists(CurDir+'TCS.cds') then TCS.Active:=True
  else TCS.CreateDataSet;

  fmTBuff:=TfmTBuff.Create(Application);

  fmTBuff.ShowModal;
  if fmTBuff.ModalResult=mrOk then
  begin //���������
    if TCH.RecordCount>0 then
    begin
      if CanDo('prAddTK') then
      begin
        with dmO do
        begin
          fmPeriodUni.DateTimePicker1.Date:=TCHDATEB.AsDateTime;
          fmPeriodUni.DateTimePicker2.Date:=iMaxDate;
          fmPeriodUni.ShowModal;
          if fmPeriodUni.ModalResult=mrOk then
          begin
            DateB:=Trunc(fmPeriodUni.DateTimePicker1.Date);
            DateE:=Trunc(fmPeriodUni.DateTimePicker2.Date)+1;

           //�������� �� �������������

            prTestUseTC(cxTextEdit1.Tag,iDate,iC);
            if Trunc(DateB)>iDate then
            begin //� ������ �������� �� ���� ������������� - �������� ����������

              bAdd:=True;
              if quTCards.RecordCount>0 then
              begin //�������� ���� ��������� �������� ���� ���������� �� ��������� �� �������
                quTCards.Last;
                prSetVal;
                if quTCardsDATEB.AsDateTime<DateB then
                begin
                  if quTCardsDATEE.AsDateTime>=iMaxDate then
                  begin
                    quTCards.Edit;
                    quTCardsDATEE.AsDateTime:=Trunc(DateB-1);
                    quTCards.Post;
                  end else
                  begin
                    if quTCardsDATEE.AsDateTime>=DateB then
                    begin
                      showmessage('������ ������ ��� ���������, ���������� ����������.');
                      bAdd:=False;
                    end;
                  end;
                end else
                begin
                  showmessage('������ ������ ��� ���������, ���������� ����������.');
                  bAdd:=False;
                end;
              end;

              if bAdd then
              begin
                iId:=GetId('TCards');

                quTCards.Append;
                quTCardsIDCARD.AsInteger:=cxTextEdit1.Tag;
                quTCardsID.AsInteger:=IId;
                quTCardsDATEB.AsDateTime:=Trunc(DateB);
                quTCardsDATEE.AsDateTime:=Trunc(DateE-1); //��� ������������
                quTCardsSHORTNAME.AsString:=cxTextEdit1.Text;
                quTCardsRECEIPTNUM.AsString:='';
                quTCardsPOUTPUT.AsString:='';
                quTCardsPCOUNT.AsInteger:=TCHPCOUNT.AsInteger;
                quTCardsPVES.AsFloat:=TCHPVES.AsFloat;
                quTCards.Post;

                if quCardsSelTCARD.AsInteger=0 then
                begin
                  quCardsSel.Edit;
                  quCardsSelTCARD.AsInteger:=1;
                  quCardsSel.Post;
                  quCardsSel.Refresh;
                end;

                if Panel3.Visible=False then Panel3.Visible:=True;
                prSetVal;

                //�������� ��� ������������  �� TCS
                ViewTSpec.BeginUpdate;
                TCS.First;
                while not TCS.Eof do
                begin
                  if TCSIDT.AsInteger=TCHID.AsInteger then
                  begin

                  //��� ������� �� ���� �� ������� ��� ����� ������
                 //���� ������� ��������� ���� � ��������� ������ � ��� �� ������ ���������
                    if prCanSel(cxTextEdit1.Tag,TCSIDCARD.asInteger) then
                    begin
                      kBrutto:=0;
                      iCurDate:=prDateToI(cxDateEdit1.Date);
                      quFindEU.Active:=False;
                      quFindEU.ParamByName('GOODSID').AsInteger:=TCSIDCARD.asInteger;
                      quFindEU.ParamByName('DATEB').AsInteger:=iCurDate;
                      quFindEU.ParamByName('DATEE').AsInteger:=iCurDate;
                      quFindEU.Active:=True;

                      if quFindEU.RecordCount>0 then
                      begin
                        quFindEU.First;
                        kBrutto:=quFindEUTO100GRAMM.AsFloat;
                      end;

                      quFindEU.Active:=False;

                      quTSpec.Append;
                      quTSpecIDC.AsInteger:=quTCardsIDCARD.AsInteger;
                      quTSpecIDT.AsInteger:=quTCardsID.AsInteger;
                      quTSpecIDCARD.AsInteger:=TCSIDCARD.asInteger;
                      quTSpecCURMESSURE.AsInteger:=TCSCURMESSURE.AsInteger;
                      quTSpecNETTO.AsFloat:=TCSNETTO.AsFloat;
                      quTSpecBRUTTO.AsFloat:=TCSNETTO.AsFloat*(100+kBrutto)/100;
                      quTSpecKNB.AsFloat:=kBrutto;
                      quTSpec.Post;
                    end;
{                    else  //������ �� ��������  ��� ��������������
                    begin
                      showmessage('���������� ����������: ����������� ������.');
                    end; }

                  end;
                  TCS.Next;
                end;
                quTSpec.FullRefresh;
                quTSpec.First;
                ViewTSpec.EndUpdate;
              end;
            end else
            begin //� ������ �������� ���� ������������� - �������� ����������
              Showmessage('�� ���� � �������� (���� - '+FormatDateTime('dd.mm.yyyy',iDate)+', ��� ����� - '+IntToStr(iC)+'). ������� ������ ������ �������� !!!');
              exit;
            end;
          end;
        end;
      end else showmessage('��� ����.');
    end;
  end;
  fmTBuff.Release;

  TCH.Active:=False;
  TCS.Active:=False;
end;

end.
