������� ��� ����������� ����������� �����.


CREATE PROCEDURE PR_RECALCPARTREMN 
RETURNS (
    RESULT INTEGER)
AS
DECLARE VARIABLE IDP INTEGER;
DECLARE VARIABLE IDATE INTEGER;
DECLARE VARIABLE QUANTP DOUBLE PRECISION;
DECLARE VARIABLE QUANTR DOUBLE PRECISION;
DECLARE VARIABLE QUANTRP DOUBLE PRECISION;
DECLARE VARIABLE QUANTOUT DOUBLE PRECISION;

BEGIN
  RESULT=0;
  for SELECT ID,QPART,QREMN,IDATE FROM OF_PARTIN INTO :IDP,:QUANTP,:QUANTRP,:IDATE do
  begin
    update OF_PARTOUT set IDPARTIN=(-1)*:IDP where IDPARTIN=:IDP and IDDATE<:IDATE;

    SELECT SUM(QUANT) FROM OF_PARTOUT
    where IDPARTIN=:IDP and IDDATE>=:IDATE INTO :QUANTOUT;

    if (QUANTOUT is NULL) then QUANTOUT=0;
    QUANTR=:QUANTP-:QUANTOUT;

    if (:QUANTRP<>:QUANTR) then
    begin
      Update OF_Partin set QREMN=:QUANTR where ID=:IDP;
      RESULT=:RESULT+1;
    end
  end
  SUSPEND;
END


--------------------------------------------------
--------------------------------------------------

����� ���� � ��������


������


/* Table: OF_BGUGR */

CREATE TABLE OF_BGUGR (
    ID INTEGER NOT NULL,
    NAMEGR VARCHAR (50) character set WIN1251 collate WIN1251);



/* Primary keys definition */

ALTER TABLE OF_BGUGR ADD CONSTRAINT PK_OF_BGUGR PRIMARY KEY (ID);


/* Indices definition */

CREATE UNIQUE INDEX PK_OF_BGUGR ON OF_BGUGR (ID);



������


/* Table: OF_BGU */

CREATE TABLE OF_BGU (
    ID INTEGER NOT NULL,
    PARENT INTEGER,
    NAMEBGU VARCHAR (100) character set WIN1251 collate WIN1251,
    BB DOUBLE PRECISION,
    GG DOUBLE PRECISION,
    U1 DOUBLE PRECISION,
    U2 DOUBLE PRECISION,
    EE DOUBLE PRECISION);



/* Primary keys definition */

ALTER TABLE OF_BGU ADD CONSTRAINT PK_OF_BGU PRIMARY KEY (ID);


/* Indices definition */

CREATE INDEX IDX_OF_BGU ON OF_BGU (PARENT);
CREATE UNIQUE INDEX PK_OF_BGU ON OF_BGU (ID);



�������� ���� ������ �� ��������������� ������ xls



ALTER TABLE OF_CARDS
ADD BB DOUBLE PRECISION


ALTER TABLE OF_CARDS
ADD GG DOUBLE PRECISION


ALTER TABLE OF_CARDS
ADD U1 DOUBLE PRECISION

ALTER TABLE OF_CARDS
ADD U2 DOUBLE PRECISION


ALTER TABLE OF_CARDS
ADD EE DOUBLE PRECISION


update OF_CARDS
set BB=0,GG=0,U1=0,U2=0,EE=0



----------------------------------------

������


CREATE PROCEDURE PR_NORMPARTIN (
    IDSKL INTEGER,
    IDATE INTEGER,
    IDCARD INTEGER,
    QUANT DOUBLE PRECISION)
AS
DECLARE VARIABLE IDP INTEGER;
DECLARE VARIABLE QP INTEGER;
DECLARE VARIABLE QR DOUBLE PRECISION;
DECLARE VARIABLE IDD INTEGER;
DECLARE VARIABLE QREMN DOUBLE PRECISION;

BEGIN
  if (:QUANT<=0) then
  begin
    update OF_PARTIN SET QREMN=0 where IDSTORE=:IDSKL and ARTICUL=:IDCARD and IDATE<=:IDATE;
  end else
  begin
    QREMN=:QUANT;
    for Select first 50 ID,QPART,QREMN,IDATE from OF_PARTIN where IDSTORE=:IDSKL and ARTICUL=:IDCARD and IDATE<=:IDATE
        order by IDATE DESC
        into :IDP,:QP,:QR,:IDD do
    begin
      if (QREMN=0) then
      begin
        update OF_PARTIN SET QREMN=0 where ID=:IDP;
      end
      else
      begin
        if (QP<=:QREMN) then
        begin
          update OF_PARTIN SET QREMN=:QP where ID=:IDP;
          QREMN=:QREMN-:QP;
        end else
        begin
          if (QREMN>0) then
          begin
            update OF_PARTIN SET QREMN=:QREMN where ID=:IDP;
          end
          QREMN=0;
        end
      end
    end
  end

  SUSPEND;
END




--------------------------------------------

��������� ���

ALTER TABLE OF_CARDSTSPEC
ADD NETTO1 DOUBLE PRECISION


ALTER TABLE OF_CARDSTSPEC
ADD COMMENT VARCHAR(50) CHARACTER SET WIN1251 
COLLATE WIN1251 


ALTER TABLE OF_CARDSTSPEC
ADD IOBR INTEGER


Update OF_CARDSTSPEC
Set NETTO1=NETTO




������� �������� ������� TCH.cds � TCS.cds



---------------------------------------------

�������� 


CREATE VIEW OF_VREALB (
    IDHEAD,
    IDB,
    CODEB,
    QUANT,
    SUMOUT,
    DATEDOC,
    IDSKL,
    SUMIN)
AS

SELECT dsb.IDHEAD,dsb.ID,dsb.IDCARD,dsb.QUANT,dsb.RSUM,
dh.DATEDOC,dh.IDSKL,
cast(SUM(dsbc.SUMIN) as double precision) as SUMIN
FROM OF_DOCSPECOUTB dsb
left join of_docheadoutb dh on dh.ID=dsb.IDHEAD
left join OF_DOCSPECOUTBC dsbc on ((dsbc.IDHEAD=dsb.IDHEAD) and (dsbc.IDB=dsb.ID))
where dh.IACTIVE=1 and dh.OPER like '%Sale%'
group by dsb.IDHEAD,dsb.ID,dsb.IDCARD,dsb.QUANT,dsb.RSUM,
dh.DATEDOC,dh.IDSKL
;



CREATE VIEW OF_VREALB2 (
    IDHEAD,
    IDB,
    CODEB,
    QUANT,
    SUMOUT,
    DATEDOC,
    IDSKL,
    SUMIN)
AS
SELECT dsb.IDHEAD,dsb.ID,dsb.IDCARD,dsb.QUANT,dsb.RSUM,
dh.DATEDOC,dh.IDSKL,
cast(SUM(dsbc.SUMIN) as double precision) as SUMIN
FROM OF_DOCSPECOUTB dsb
left join of_docheadoutb dh on dh.ID=dsb.IDHEAD
left join OF_DOCSPECOUTBC dsbc on ((dsbc.IDHEAD=dsb.IDHEAD) and (dsbc.IDB=dsb.ID))
left join of_oper op on dh.OPER=op.ABR
where dh.IACTIVE=1 and op.TPRIB=2
GROUP BY dsb.IDHEAD,dsb.ID,dsb.IDCARD,dsb.QUANT,dsb.RSUM,
dh.DATEDOC,dh.IDSKL
;



CREATE VIEW OF_VREALB3 (
    IDHEAD,
    IDB,
    CODEB,
    QUANT,
    SUMOUT,
    DATEDOC,
    IDSKL,
    SUMIN)
AS
SELECT dsb.IDHEAD,dsb.ID,dsb.IDCARD,dsb.QUANT,dsb.RSUM,
dh.DATEDOC,dh.IDSKL,
cast(SUM(dsbc.SUMIN) as double precision) as SUMIN
FROM OF_DOCSPECOUTB dsb
left join of_docheadoutb dh on dh.ID=dsb.IDHEAD
left join OF_DOCSPECOUTBC dsbc on ((dsbc.IDHEAD=dsb.IDHEAD) and (dsbc.IDB=dsb.ID))
left join of_oper op on dh.OPER=op.ABR
where dh.IACTIVE=1 and op.TPRIB=3
GROUP BY dsb.IDHEAD,dsb.ID,dsb.IDCARD,dsb.QUANT,dsb.RSUM,
dh.DATEDOC,dh.IDSKL
;



CREATE VIEW OF_VREALB4 (
    IDHEAD,
    IDB,
    CODEB,
    QUANT,
    SUMOUT,
    DATEDOC,
    IDSKL,
    SUMIN)
AS
SELECT dsb.IDHEAD,dsb.ID,dsb.IDCARD,dsb.QUANT,dsb.RSUM,
dh.DATEDOC,dh.IDSKL,
cast(SUM(dsbc.SUMIN) as double precision) as SUMIN
FROM OF_DOCSPECOUTB dsb
left join of_docheadoutb dh on dh.ID=dsb.IDHEAD
left join OF_DOCSPECOUTBC dsbc on ((dsbc.IDHEAD=dsb.IDHEAD) and (dsbc.IDB=dsb.ID))
left join of_oper op on dh.OPER=op.ABR
where dh.IACTIVE=1 and op.TPRIB=4
GROUP BY dsb.IDHEAD,dsb.ID,dsb.IDCARD,dsb.QUANT,dsb.RSUM,
dh.DATEDOC,dh.IDSKL
;



�������� ����

ALTER TABLE OF_CLIENTS
ADD SROKPLAT INTEGER



Update OF_CLIENTS
Set SROKPLAT=0





