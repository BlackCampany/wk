unit TabsFF;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxLookAndFeelPainters, StdCtrls, ExtCtrls, cxButtons,
  cxRadioGroup, cxControls, cxContainer, cxEdit, cxTextEdit, cxMaskEdit,
  cxDropDownEdit, cxCalc, cxStyles, cxCustomData, cxGraphics, cxFilter,
  cxData, DB, cxDBData, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  cxGridCardView, cxGridDBCardView, Menus, cxDataStorage, cxGroupBox;

type
  TfmTabsFF = class(TForm)
    cxButton3: TcxButton;
    Bevel1: TBevel;
    cxButton1: TcxButton;
    Panel1: TPanel;
    TextEdit1: TcxTextEdit;
    Label4: TLabel;
    Panel2: TPanel;
    Level: TcxGridLevel;
    Grid: TcxGrid;
    ViewMove: TcxGridDBCardView;
    ViewMoveNUMTABLE: TcxGridDBCardViewRow;
    ViewMoveTABSUM: TcxGridDBCardViewRow;
    Label1: TLabel;
    ViewMoveNUMZ: TcxGridDBCardViewRow;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmTabsFF: TfmTabsFF;

implementation

uses Calc, Dm, Un1;

{$R *.dfm}

end.
