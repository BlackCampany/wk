unit AddDoc2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, cxGraphics, cxCurrencyEdit, cxDropDownEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxButtonEdit,
  cxMaskEdit, cxCalendar, cxControls, cxContainer, cxEdit, cxTextEdit,
  StdCtrls, ExtCtrls, Menus, cxLookAndFeelPainters, cxButtons, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, DB, cxDBData, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxLabel, dxfLabel, Placemnt, FIBDataSet,
  pFIBDataSet, DBClient, FIBQuery, pFIBQuery, pFIBStoredProc;

type
  TfmAddDoc2 = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Label1: TLabel;
    cxTextEdit1: TcxTextEdit;
    cxDateEdit1: TcxDateEdit;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    cxTextEdit2: TcxTextEdit;
    cxDateEdit2: TcxDateEdit;
    cxButtonEdit1: TcxButtonEdit;
    cxLookupComboBox1: TcxLookupComboBox;
    cxCurrencyEdit1: TcxCurrencyEdit;
    cxCurrencyEdit2: TcxCurrencyEdit;
    Label13: TLabel;
    Label14: TLabel;
    Panel2: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Panel3: TPanel;
    ViewDoc2: TcxGridDBTableView;
    LevelDoc2: TcxGridLevel;
    GridDoc2: TcxGrid;
    cxLabel1: TcxLabel;
    Image1: TImage;
    Image2: TImage;
    cxLabel2: TcxLabel;
    Image3: TImage;
    cxLabel3: TcxLabel;
    Label15: TLabel;
    FormPlacement1: TFormPlacement;
    taSpecOut: TClientDataSet;
    dsSpec: TDataSource;
    taSpecOutNum: TIntegerField;
    taSpecOutIdGoods: TIntegerField;
    taSpecOutNameG: TStringField;
    taSpecOutIM: TIntegerField;
    taSpecOutSM: TStringField;
    taSpecOutQuant: TFloatField;
    taSpecOutPrice1: TCurrencyField;
    taSpecOutSum1: TCurrencyField;
    taSpecOutPrice2: TCurrencyField;
    taSpecOutSum2: TCurrencyField;
    taSpecOutINds: TIntegerField;
    taSpecOutSNds: TStringField;
    taSpecOutRNds: TCurrencyField;
    ViewDoc2Num: TcxGridDBColumn;
    ViewDoc2IdGoods: TcxGridDBColumn;
    ViewDoc2NameG: TcxGridDBColumn;
    ViewDoc2IM: TcxGridDBColumn;
    ViewDoc2SM: TcxGridDBColumn;
    ViewDoc2Quant: TcxGridDBColumn;
    ViewDoc2Price1: TcxGridDBColumn;
    ViewDoc2Sum1: TcxGridDBColumn;
    ViewDoc2Price2: TcxGridDBColumn;
    ViewDoc2Sum2: TcxGridDBColumn;
    ViewDoc2INds: TcxGridDBColumn;
    ViewDoc2SNds: TcxGridDBColumn;
    ViewDoc2RNds: TcxGridDBColumn;
    taSpecOutSumNac: TCurrencyField;
    taSpecOutProcNac: TFloatField;
    ViewDoc2SumNac: TcxGridDBColumn;
    ViewDoc2ProcNac: TcxGridDBColumn;
    prCalcPrice: TpFIBStoredProc;
    Image4: TImage;
    cxLabel4: TcxLabel;
    Image5: TImage;
    cxLabel5: TcxLabel;
    Image6: TImage;
    cxLabel6: TcxLabel;
    Image7: TImage;
    procedure FormCreate(Sender: TObject);
    procedure cxLookupComboBox1PropertiesChange(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxLabel1Click(Sender: TObject);
    procedure ViewDoc2DragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure ViewDoc2DragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure taSpecOutQuantChange(Sender: TField);
    procedure taSpecOutPrice1Change(Sender: TField);
    procedure taSpecOutSum1Change(Sender: TField);
    procedure taSpecOutPrice2Change(Sender: TField);
    procedure taSpecOutSum2Change(Sender: TField);
    procedure cxLabel3Click(Sender: TObject);
    procedure cxLabel2Click(Sender: TObject);
    procedure cxLabel4Click(Sender: TObject);
    procedure ViewDoc2Editing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure FormShow(Sender: TObject);
    procedure cxLabel5Click(Sender: TObject);
    procedure cxLabel6Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAddDoc2: TfmAddDoc2;
  iCol:Integer = 0;

implementation

uses Un1, dmOffice, Clients, Goods, SelPartIn, SelPartIn1, DMOReps;

{$R *.dfm}

procedure TfmAddDoc2.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  GridDoc2.Align:=alClient;
  ViewDoc2.RestoreFromIniFile(CurDir+GridIni);
end;

procedure TfmAddDoc2.cxLookupComboBox1PropertiesChange(Sender: TObject);
begin
  with dmO do
  begin
    CurVal.IdMH:=cxLookupComboBox1.EditValue;
    CurVal.NAMEMH:=cxLookupComboBox1.Text;
    if quMHAll.Locate('ID',CurVal.IdMH,[]) then
    begin
      Label15.Caption:='��. ����: '+quMHAllNAMEPRICE.AsString;
      Label15.Tag:=quMHAllDEFPRICE.AsInteger;
    end else
    begin
      Label15.Caption:='';
      Label15.Tag:=0;
    end;  
  end;
end;

procedure TfmAddDoc2.cxButtonEdit1PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  if dmO.taClients.Active=False then dmO.taClients.Active:=True;
  if fmClients.Visible then fmClients.Close;
  fmClients.ShowModal;
  if fmClients.ModalResult=mrOk then
  begin
    cxButtonEdit1.Tag:=dmO.taClientsID.AsInteger;
    cxButtonEdit1.Text:=dmO.taClientsNAMECL.AsString;

  end;
end;

procedure TfmAddDoc2.cxLabel1Click(Sender: TObject);
begin
  //�������� �������
  bAddSpec:=True;
  if fmGoods.Visible then fmGoods.Close;
  fmGoods.Show;
end;

procedure TfmAddDoc2.ViewDoc2DragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept:=False;
  if bDr then  Accept:=True;
end;

procedure TfmAddDoc2.ViewDoc2DragDrop(Sender, Source: TObject; X,
  Y: Integer);
Var iCo:Integer;
    i,j: Integer;
    iNum: Integer;
    Rec:TcxCustomGridRecord;
    iMax:Integer;
begin
//
  if bDr then
  begin
    bDr:=False;
    bDM:=False;
    bDInv:=False;
    iCo:=fmGoods.ViewGoods.Controller.SelectedRecordCount;
    if iCo>0 then
    begin
      if MessageDlg('�� ������������� ������ ����������� ��������� ������� ('+IntToStr(iCo)+' ��.) � ������������ ���������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        iMax:=1;
        fmAddDoc2.taSpecOut.First;
        if not fmAddDoc2.taSpecOut.Eof then
        begin
          fmAddDoc2.taSpecOut.Last;
          iMax:=fmAddDoc2.taSpecOutNum.AsInteger+1;
        end;

        with dmO do
        begin
          ViewDoc2.BeginUpdate;
          for i:=0 to fmGoods.ViewGoods.Controller.SelectedRecordCount-1 do
          begin
            Rec:=fmGoods.ViewGoods.Controller.SelectedRecords[i];

            for j:=0 to Rec.ValueCount-1 do
            begin
              if fmGoods.ViewGoods.Columns[j].Name='ViewGoodsID' then break;
            end;

            iNum:=Rec.Values[j];
          //��� ��� - ����������

            if quCardsSel.Locate('ID',iNum,[]) then
            begin
              with fmAddDoc2 do
              begin
                taSpecOut.Append;
                taSpecOutNum.AsInteger:=iMax;
                taSpecOutIdGoods.AsInteger:=quCardsSelID.AsInteger;
                taSpecOutNameG.AsString:=quCardsSelNAME.AsString;
                taSpecOutIM.AsInteger:=quCardsSelIMESSURE.AsInteger;
                taSpecOutSM.AsString:=quCardsSelNAMESHORT.AsString;
                taSpecOutQuant.AsFloat:=1;
                taSpecOutPrice1.AsCurrency:=0;
                taSpecOutSum1.AsCurrency:=0;
                taSpecOutPrice2.AsCurrency:=0;
                taSpecOutSum2.AsCurrency:=0;
                taSpecOutINds.AsInteger:=quCardsSelINDS.AsInteger;
                taSpecOutSNds.AsString:=quCardsSelNAMENDS.AsString;
                taSpecOutRNds.AsCurrency:=0;
                taSpecOutSumNac.AsCurrency:=0;
                taSpecOutProcNac.AsFloat:=0;
                taSpecOut.Post;
                inc(iMax);
              end;
            end;
          end;
          ViewDoc2.EndUpdate;
        end;
      end;
    end;
  end;
end;

procedure TfmAddDoc2.taSpecOutQuantChange(Sender: TField);
begin
  //���������� �����
  if iCol=1 then
  begin
    taSpecOutSum1.AsCurrency:=taSpecOutPrice1.AsCurrency*taSpecOutQuant.AsFloat;
    taSpecOutSum2.AsCurrency:=taSpecOutPrice2.AsCurrency*taSpecOutQuant.AsFloat;
    taSpecOutSumNac.AsCurrency:=taSpecOutSum2.AsCurrency-taSpecOutSum1.AsCurrency;
    if taSpecOutSum1.AsCurrency>0.01 then
    begin
      taSpecOutProcNac.AsFloat:=(taSpecOutSum2.AsCurrency-taSpecOutSum1.AsCurrency)/taSpecOutSum1.AsCurrency*100;
    end;
    taSpecOutRNds.AsCurrency:=RoundEx(taSpecOutSum1.AsCurrency*vNds[taSpecOutINds.AsInteger]/(100+vNds[taSpecOutINds.AsInteger])*100)/100;
  end;
end;

procedure TfmAddDoc2.taSpecOutPrice1Change(Sender: TField);
begin
  //���������� ����
  if iCol=2 then
  begin
    taSpecOutSum1.AsCurrency:=taSpecOutPrice1.AsCurrency*taSpecOutQuant.AsFloat;
//  taSpecOutSum2.AsCurrency:=taSpecOutPrice2.AsCurrency*taSpecOutQuant.AsFloat;
    taSpecOutSumNac.AsCurrency:=taSpecOutSum2.AsCurrency-taSpecOutSum1.AsCurrency;
    if taSpecOutSum1.AsCurrency>0.01 then
    begin
      taSpecOutProcNac.AsFloat:=(taSpecOutSum2.AsCurrency-taSpecOutSum1.AsCurrency)/taSpecOutSum1.AsCurrency*100;
    end else taSpecOutProcNac.AsFloat:=0;
    taSpecOutRNds.AsCurrency:=RoundEx(taSpecOutSum1.AsCurrency*vNds[taSpecOutINds.AsInteger]/(100+vNds[taSpecOutINds.AsInteger])*100)/100;
  end;
end;

procedure TfmAddDoc2.taSpecOutSum1Change(Sender: TField);
begin
  if iCol=3 then
  begin
    if abs(taSpecOutQuant.AsFloat)>0 then taSpecOutPrice1.AsCurrency:=RoundEx(taSpecOutSum1.AsCurrency/taSpecOutQuant.AsFloat*100)/100;
//  taSpecOutPrice1.AsCurrency:=RoundEx(taSpecOutSum1.AsCurrency/taSpecOutQuant.AsFloat*100)/100;
    taSpecOutSumNac.AsCurrency:=taSpecOutSum2.AsCurrency-taSpecOutSum1.AsCurrency;
    if abs(taSpecOutSum1.AsCurrency)>0.01 then
    begin
      taSpecOutProcNac.AsFloat:=(taSpecOutSum2.AsCurrency-taSpecOutSum1.AsCurrency)/taSpecOutSum1.AsCurrency*100;
    end else taSpecOutProcNac.AsFloat:=0;
    taSpecOutRNds.AsCurrency:=RoundEx(taSpecOutSum1.AsCurrency*vNds[taSpecOutINds.AsInteger]/(100+vNds[taSpecOutINds.AsInteger])*100)/100;
  end;
end;

procedure TfmAddDoc2.taSpecOutPrice2Change(Sender: TField);
begin
  //���������� ����
  if iCol=4 then
  begin

//  taSpecSum1.AsCurrency:=taSpecOutPrice1.AsCurrency*taSpecOutQuant.AsFloat;
    taSpecOutSum2.AsCurrency:=taSpecOutPrice2.AsCurrency*taSpecOutQuant.AsFloat;
    taSpecOutSumNac.AsCurrency:=taSpecOutSum2.AsCurrency-taSpecOutSum1.AsCurrency;
    if taSpecOutSum1.AsCurrency>0.01 then
    begin
      taSpecOutProcNac.AsFloat:=(taSpecOutSum2.AsCurrency-taSpecOutSum1.AsCurrency)/taSpecOutSum1.AsCurrency*100;
    end else taSpecOutProcNac.AsFloat:=0;
  end;
end;

procedure TfmAddDoc2.taSpecOutSum2Change(Sender: TField);
begin
  if iCol=5 then
  begin
    if abs(taSpecOutQuant.AsFloat)>0 then taSpecOutPrice2.AsCurrency:=RoundEx(taSpecOutSum2.AsCurrency/taSpecOutQuant.AsFloat*100)/100;
    taSpecOutSumNac.AsCurrency:=taSpecOutSum2.AsCurrency-taSpecOutSum1.AsCurrency;
    if taSpecOutSum1.AsCurrency>0.01 then
    begin
      taSpecOutProcNac.AsFloat:=(taSpecOutSum2.AsCurrency-taSpecOutSum1.AsCurrency)/taSpecOutSum1.AsCurrency*100;
    end else taSpecOutProcNac.AsFloat:=0;
  end;
end;

procedure TfmAddDoc2.cxLabel3Click(Sender: TObject);
Var rPrice,rPrice1:Currency;
    rMessure,kSp,k:Real;
begin
  //����������� ����  � ����� ���������� �������
  if cxButtonEdit1.Tag=0 then
  begin
    showmessage('�������� ������� �����������.');
    exit;
  end else
  begin
    iCol:=0;
    taSpecOut.First;
    while not taSpecOut.Eof do
    begin
      with dmO do
      begin
        prCalcLastPrice.ParamByName('IDGOOD').AsInteger:=taSpecOutIdGoods.AsInteger;
        prCalcLastPrice.ParamByName('IDCLI').AsCurrency:=cxButtonEdit1.Tag;
        prCalcLastPrice.ExecProc;

        rPrice:=prCalcLastPrice.ParamByName('PRICEIN').AsCurrency;
        rPrice1:=prCalcLastPrice.ParamByName('PRICEUCH').AsCurrency;
        rMessure:=prCalcLastPrice.ParamByName('KOEF').AsFloat;

        quM.Active:=False;
        quM.ParamByName('IDM').AsInteger:=taSpecOutIM.AsInteger;
        quM.Active:=True;
        kSp:=quMKOEF.AsFloat;
        quM.Active:=False;

        if kSp<>rMessure then
        begin
          k:=rMessure/kSp;
        end else k:=1;

        taSpecOut.Edit;
        taSpecOutPrice1.AsCurrency:=rPrice*k;
        taSpecOutPrice2.AsCurrency:=rPrice1*k;
        taSpecOutSum1.AsCurrency:=rPrice*k*taSpecOutQuant.AsFloat;
        taSpecOutSum2.AsCurrency:=rPrice1*k*taSpecOutQuant.AsFloat;
        taSpecOutSumNac.AsCurrency:=taSpecOutSum2.AsCurrency-taSpecOutSum1.AsCurrency;
        if taSpecOutSum1.AsCurrency>0.01 then
        begin
          taSpecOutProcNac.AsFloat:=(taSpecOutSum2.AsCurrency-taSpecOutSum1.AsCurrency)/taSpecOutSum1.AsCurrency*100;
        end else taSpecOutProcNac.AsFloat:=0;
        taSpecOutRNds.AsCurrency:=RoundEx(taSpecOutSum1.AsCurrency*vNds[taSpecOutINds.AsInteger]/(100+vNds[taSpecOutINds.AsInteger])*100)/100;
        taSpecOut.Post;

        taSpecOut.Next;
      end;
    end;
    taSpecOut.First;
  end;
end;

procedure TfmAddDoc2.cxLabel2Click(Sender: TObject);
begin
  //������� �������
  if MessageDlg('�� ������������� ������ ������� �������  � '+taSpecOutNum.AsString+'  '+taSpecOutNameG.AsString+' ?',
     mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
    taSpecOut.Delete;
  end;
end;

procedure TfmAddDoc2.cxLabel4Click(Sender: TObject);
begin
  //�������� ����
  ViewDoc2.BeginUpdate;
  taSpecOut.First;
  while not taSpecOut.Eof do
  begin
    taSpecOut.Edit;
    taSpecOutPrice2.AsCurrency:=taSpecOutPrice1.AsCurrency;
    taSpecOutSum2.AsCurrency:=taSpecOutSum1.AsCurrency;
    taSpecOutSumNac.AsCurrency:=0;
    taSpecOutProcNac.AsFloat:=0;
    taSpecOut.Post;

    taSpecOut.Next;
    delay(10);
  end;
  taSpecOut.First;
  ViewDoc2.EndUpdate;
end;

procedure TfmAddDoc2.ViewDoc2Editing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  iCol:=0;
  if ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2Quant' then iCol:=1;
  if ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2Price1' then iCol:=2;
  if ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2Sum1' then iCol:=3;
  if ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2Price2' then iCol:=4;
  if ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2Sum2' then iCol:=5;
//  Label16.Caption:=IntToStr(iCol); Delay(10);
end;

procedure TfmAddDoc2.FormShow(Sender: TObject);
begin
  iCol:=0;
end;

procedure TfmAddDoc2.cxLabel5Click(Sender: TObject);
var sMessure:String;
    iM:Integer;
    rPrice,rPrice1,k:Real;
begin
  //������ ������
  fmPartIn:=tfmPartIn.Create(Application);
  if cxLookupComboBox1.EditValue>0 then
  begin
    if not taSpecOut.Eof then prSelPartIn(taSpecOutIdGoods.AsInteger,cxLookupComboBox1.EditValue,cxButtonEdit1.Tag);
    with dmO do
    begin
      quM.Active:=false;
      quM.ParamByName('IDM').AsInteger:=taSpecOutIM.AsInteger;
      quM.Active:=True;
      if quMID_PARENT.AsInteger=0 then sMessure:=quMNAMESHORT.AsString
      else
      begin
        iM:=quMID_PARENT.AsInteger;
        quM.Active:=false;
        quM.ParamByName('IDM').AsInteger:=iM;
        quM.Active:=True;
        sMessure:=quMNAMESHORT.AsString;
      end;
      quM.Active:=false;
    end;
    fmPartIn.Label4.Caption:=taSpecOutNameG.AsString+' ('+sMessure+')';
    fmPartIn.Label5.Caption:=cxLookupComboBox1.Text;
    if cxButtonEdit1.Tag=0 then fmPartIn.Label6.Caption:='�� ���������'
                           else fmPartIn.Label6.Caption:=cxButtonEdit1.Text;
    fmPartIn.ShowModal;
    if fmPartIn.ModalResult=mrOk then
    begin //������������ ����
      rPrice:=dmO.quSelPartInPRICEIN.AsCurrency;
      rPrice1:=dmO.quSelPartInPRICEOUT.AsCurrency;
      k:=1/prFindKM(taSpecOutIM.AsInteger);

      taSpecOut.Edit;
      taSpecOutPrice1.AsCurrency:=rPrice*k;
      taSpecOutPrice2.AsCurrency:=rPrice1*k;
      taSpecOutSum1.AsCurrency:=rPrice*k*taSpecOutQuant.AsFloat;
      taSpecOutSum2.AsCurrency:=rPrice1*k*taSpecOutQuant.AsFloat;
      taSpecOutSumNac.AsCurrency:=taSpecOutSum2.AsCurrency-taSpecOutSum1.AsCurrency;
      if taSpecOutSum1.AsCurrency>0.01 then
      begin
        taSpecOutProcNac.AsFloat:=(taSpecOutSum2.AsCurrency-taSpecOutSum1.AsCurrency)/taSpecOutSum1.AsCurrency*100;
      end else taSpecOutProcNac.AsFloat:=0;
      taSpecOutRNds.AsCurrency:=RoundEx(taSpecOutSum1.AsCurrency*vNds[taSpecOutINds.AsInteger]/(100+vNds[taSpecOutINds.AsInteger])*100)/100;
      taSpecOut.Post;
    end;
  end else showmessage('�������� ����� ��������.');
  fmPartIn.Release;
end;

procedure TfmAddDoc2.cxLabel6Click(Sender: TObject);
Var iYes:Integer;
    rQP,rQs,PriceSp,k:Real;
begin
//�������� ������
  fmPartIn1:=tfmPartIn1.Create(Application);
  if cxLookupComboBox1.EditValue>0 then
  begin
    with dmO do
    with dmORep do
    begin
      iCol:=0;
      taPartTest.Active:=False;
      taPartTest.CreateDataSet;
      taSpecOut.First;
      while not taSpecOut.Eof do
      begin
        //��� ��� ������
        rQP:=0;
        k:=prFindKM(taSpecOutIM.AsInteger);
        rQs:=taSpecOutQuant.AsFloat*k; //20*0,5=10
        PriceSp:=taSpecOutPrice1.AsCurrency/k;

        prSelPartIn(taSpecOutIdGoods.AsInteger,cxLookupComboBox1.EditValue,cxButtonEdit1.Tag);
        with dmO do
        begin
          quSelPartIn.First;
          while not quSelPartIn.Eof do
          begin
            //���� �� ���� ������� ���� �����, ��������� �������� ���
            if (RoundEx(quSelPartInPRICEIN.AsFloat*100)/100)=PriceSp then rQp:=rQp+quSelPartInQREMN.AsFloat;
            quSelPartIn.Next;
          end;
          quSelPartIn.Active:=False;
        end;

        if rQp>=rQs then iYes:=1 else iYes:=0;

        taPartTest.Append;
        taPartTestNum.AsInteger:=taSpecOutNum.AsInteger;
        taPartTestIdGoods.AsInteger:=taSpecOutIdGoods.AsInteger;
        taPartTestNameG.AsString:=taSpecOutNameG.AsString;
        taPartTestIM.AsInteger:=taSpecOutIM.AsInteger;
        taPartTestSM.AsString:=taSpecOutSM.AsString;
        taPartTestQuant.AsFloat:=taSpecOutQuant.AsFloat;
        taPartTestPrice1.AsCurrency:=taSpecOutPrice1.AsCurrency;
        taPartTestiRes.AsInteger:=iYes;
        taPartTest.Post;

        taSpecOut.Next;
      end;

      fmPartIn1.Label5.Caption:=cxLookupComboBox1.Text;
      if cxButtonEdit1.Tag=0 then fmPartIn1.Label6.Caption:='�� ���������'
                             else fmPartIn1.Label6.Caption:=cxButtonEdit1.Text;
      fmPartIn1.ShowModal;

      taPartTest.Active:=False;
    end;
  end else showmessage('�������� ����� ��������.');
  fmPartIn1.Release;
end;

procedure TfmAddDoc2.FormClose(Sender: TObject; var Action: TCloseAction);
begin
//  taSpecOut.Active:=False;
  ViewDoc2.StoreToIniFile(CurDir+GridIni,False);
  iCol:=0;
end;

end.
