unit uDMTR;

interface

uses
  SysUtils, Classes, FIBDatabase, pFIBDatabase, FIBQuery, pFIBQuery,
  pFIBStoredProc, DB, FIBDataSet, pFIBDataSet;

type
  TdmTR = class(TDataModule)
    TRDb: TpFIBDatabase;
    trSelTR: TpFIBTransaction;
    trUpdTR: TpFIBTransaction;
    quTr: TpFIBDataSet;
    quUpd1: TpFIBQuery;
    quTrSIFR: TFIBIntegerField;
    quTrNAME: TFIBStringField;
    quTrPRICE: TFIBFloatField;
    quTrCODE: TFIBStringField;
    quTrTREETYPE: TFIBStringField;
    quTrLIMITPRICE: TFIBFloatField;
    quTrCATEG: TFIBSmallIntField;
    quTrPARENT: TFIBSmallIntField;
    quTrLINK: TFIBSmallIntField;
    quTrSTREAM: TFIBSmallIntField;
    quTrLACK: TFIBSmallIntField;
    quTrDESIGNSIFR: TFIBSmallIntField;
    quTrALTNAME: TFIBStringField;
    quTrNALOG: TFIBFloatField;
    quTrBARCODE: TFIBStringField;
    quTrIMAGE: TFIBSmallIntField;
    quTrCONSUMMA: TFIBFloatField;
    quTrMINREST: TFIBSmallIntField;
    quTrPRNREST: TFIBSmallIntField;
    quTrCOOKTIME: TFIBSmallIntField;
    quTrDISPENSER: TFIBSmallIntField;
    quTrDISPKOEF: TFIBSmallIntField;
    quTrACCESS: TFIBSmallIntField;
    quTrFLAGS: TFIBSmallIntField;
    quTrTARA: TFIBSmallIntField;
    quTrCNTPRICE: TFIBSmallIntField;
    quTrBACKBGR: TFIBFloatField;
    quTrFONTBGR: TFIBFloatField;
    quTrIACTIVE: TFIBSmallIntField;
    quTrIEDIT: TFIBSmallIntField;
    quTrDATEB: TFIBIntegerField;
    quTrDATEE: TFIBIntegerField;
    quTrDAYWEEK: TFIBStringField;
    quTrTIMEB: TFIBTimeField;
    quTrTIMEE: TFIBTimeField;
    quTrALLTIME: TFIBSmallIntField;
    quUpd2: TpFIBQuery;
    prTRADDCASHSAIL: TpFIBStoredProc;
    prTRADDTABLES: TpFIBStoredProc;
    prTRADDSPECT: TpFIBStoredProc;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmTR: TdmTR;

implementation

uses Un1;

{$R *.dfm}


end.
