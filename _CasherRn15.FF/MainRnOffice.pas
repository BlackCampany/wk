unit MainRnOffice;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ToolWin, ActnMan, ActnCtrls, ActnMenus,
  ActnList, ExtCtrls, SpeedBar, XPStyleActnCtrls, StdCtrls;

type
  TfmMainRnOffice = class(TForm)
    StatusBar1: TStatusBar;
    am1: TActionManager;
    ActionMainMenuBar1: TActionMainMenuBar;
    acExit: TAction;
    acGoods: TAction;
    acModify: TAction;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    acCateg: TAction;
    SpeedItem5: TSpeedItem;
    acMessure: TAction;
    acPriceType: TAction;
    acMHP: TAction;
    acClients: TAction;
    SpeedItem2: TSpeedItem;
    acDocIn: TAction;
    SpeedItem3: TSpeedItem;
    acMove: TAction;
    acOut: TAction;
    acRemn: TAction;
    acInv: TAction;
    acMenu: TAction;
    acMoveDate: TAction;
    acOutB: TAction;
    SpeedItem4: TSpeedItem;
    SpeedItem6: TSpeedItem;
    acExportBuh: TAction;
    acTovRep: TAction;
    SpeedItem7: TSpeedItem;
    acRepPrib: TAction;
    acObVed: TAction;
    procedure FormCanResize(Sender: TObject; var NewWidth,
      NewHeight: Integer; var Resize: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure acMessureExecute(Sender: TObject);
    procedure acGoodsExecute(Sender: TObject);
    procedure acPriceTypeExecute(Sender: TObject);
    procedure acMHPExecute(Sender: TObject);
    procedure acClientsExecute(Sender: TObject);
    procedure acDocInExecute(Sender: TObject);
    procedure acMoveExecute(Sender: TObject);
    procedure acOutExecute(Sender: TObject);
    procedure acRemnExecute(Sender: TObject);
    procedure acInvExecute(Sender: TObject);
    procedure acMenuExecute(Sender: TObject);
    procedure acMoveDateExecute(Sender: TObject);
    procedure acOutBExecute(Sender: TObject);
    procedure acExportBuhExecute(Sender: TObject);
    procedure acTovRepExecute(Sender: TObject);
    procedure acRepPribExecute(Sender: TObject);
    procedure acObVedExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmMainRnOffice: TfmMainRnOffice;

implementation

uses Un1, dmOffice, OMessure, Goods, PriceType, MH, PerA_Office, Clients,
  DocsIn, GoodsSel, TransMenuBack, SelPerSkl, MoveSel, DocsOut, DocOutB,
  ExportFromOf, TOSel, DocInv, DMOReps, RepPrib, RepObSa;

{$R *.dfm}

procedure TfmMainRnOffice.FormCanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
  NewHeight:=125
end;

procedure TfmMainRnOffice.FormCreate(Sender: TObject);
begin
  CurDir := ExtractFilePath(ParamStr(0));
  ReadIni;
  CommonSet.DateFrom:=Trunc(Date-20);
  CommonSet.DateTo:=Trunc(Date+1);
  CurVal.IdMH:=0;
  CurVal.NAMEMH:='';
end;

procedure TfmMainRnOffice.FormResize(Sender: TObject);
begin
  StatusBar1.Width:=Width;
end;

procedure TfmMainRnOffice.FormShow(Sender: TObject);
begin
  Left:=0;
  Top:=0;
//  Width:=1024;

  with dmO do
  begin
    quPer.Active:=False;
    quPer.SelectSQL.Clear;
    quPer.SelectSQL.Add('select * from rpersonal');
    quPer.SelectSQL.Add('where uvolnen=1 and modul2=0');
    quPer.SelectSQL.Add('order by Name');
//    quPer.Active:=True; ����������� �� show fmPerA
  end;
  fmPerA_Office:=TfmPerA_Office.Create(Application);
  fmPerA_Office.ShowModal;
  if fmPerA_Office.ModalResult=mrOk then
  begin
    Caption:=Caption+'   : '+Person.Name;
    WriteIni; //�������� �������� �������
    fmPerA_Office.Release;

    bMenuList:=False;
    bMenuListMo:=False;

    fmGoods:=tfmGoods.Create(Application);
    fmGoodsSel:=tfmGoodsSel.Create(Application);
//    fmModCr:=tfmModCr.Create(Application);
//    fmMenuCr.Show;
    bMenuList:=True;
    bMenuListMo:=True;

  end
  else
  begin
    fmPerA_Office.Release;
    delay(100);
    close;
    delay(100);
  end;
end;

procedure TfmMainRnOffice.acExitExecute(Sender: TObject);
begin
  close;
end;

procedure TfmMainRnOffice.acMessureExecute(Sender: TObject);
begin
  //������� ���������
  //������� , ��������  - ����� ����� �������� ��� ������� ����� ����
  fmMessure.Show;  
  
end;

procedure TfmMainRnOffice.acGoodsExecute(Sender: TObject);
begin
  bAddSpec:=False;
  bAddTSpec:=False;
  fmGoods.Show;
end;

procedure TfmMainRnOffice.acPriceTypeExecute(Sender: TObject);
begin
  //���� ���
  with dmO do
  begin
    taPriceT.Active:=False;
    taPriceT.Active:=True;

    fmPriceType.ShowModal;
    taPriceT.Active:=False;
  end;
end;

procedure TfmMainRnOffice.acMHPExecute(Sender: TObject);
begin
  fmMH.TreeMH.Items.Clear;
  ClassifEx(nil,fmMH.TreeMH,dmO.quMHTree,0,'NAMEMH');
  fmMH.ShowModal;
end;

procedure TfmMainRnOffice.acClientsExecute(Sender: TObject);
begin
//�����������
  with dmO do
  begin
    if taClients.Active=False then taClients.Active:=True;
    fmClients.Show;
  end;
end;

procedure TfmMainRnOffice.acDocInExecute(Sender: TObject);
begin
  //��������� ���������
  if CommonSet.DateTo>=iMaxDate then fmDocsIn.Caption:='��������� ��������� � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
  else fmDocsIn.Caption:='��������� ��������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);
  with dmO do
  begin
    quDocsInSel.Active:=False;
    quDocsInSel.ParamByName('DATEB').AsDate:=CommonSet.DateFrom;
    quDocsInSel.ParamByName('DATEE').AsDate:=CommonSet.DateTo;
    quDocsInSel.Active:=True;
  end;
  fmDocsIn.LevelDocsIn.Visible:=True;
  fmDocsIn.LevelCards.Visible:=False;
  fmDocsIn.SpeedItem3.Visible:=True;
  fmDocsIn.SpeedItem4.Visible:=True;
  fmDocsIn.SpeedItem5.Visible:=True;
  fmDocsIn.SpeedItem6.Visible:=True;
  fmDocsIn.SpeedItem7.Visible:=True;
  fmDocsIn.SpeedItem8.Visible:=True;

  fmDocsIn.Show;
end;

procedure TfmMainRnOffice.acMoveExecute(Sender: TObject);
begin
//
end;

procedure TfmMainRnOffice.acOutExecute(Sender: TObject);
begin
  if CommonSet.DateTo>=iMaxDate then fmDocsOut.Caption:='��������� ��������� � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
  else fmDocsOut.Caption:='��������� ��������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);
  with dmO do
  begin
    quDocsOutSel.Active:=False;
    quDocsOutSel.ParamByName('DATEB').AsDate:=CommonSet.DateFrom;
    quDocsOutSel.ParamByName('DATEE').AsDate:=CommonSet.DateTo;
    quDocsOutSel.Active:=True;
  end;
  fmDocsOut.Show;
end;

procedure TfmMainRnOffice.acRemnExecute(Sender: TObject);
// �������
Var IdPar:INteger;
    iDate:Integer;
begin
  fmSelPerSkl:=tfmSelPerSkl.Create(Application);
  with fmSelPerSkl do
  begin
    if CommonSet.DateTo>=iMaxDate then CommonSet.DateTo:=Date;
    cxDateEdit1.Date:=CommonSet.DateTo;
    quMHAll1.Active:=False;
    quMHAll1.Active:=True;
    quMHAll1.First;
    cxLookupComboBox1.EditValue:=quMHAll1ID.AsInteger;
    cxCheckBox1.Visible:=False;
    Label1.Caption:='�� �����';
  end;
  fmSelPerSkl.ShowModal;
  if fmSelPerSkl.ModalResult=mrOk then
  begin
//    CommonSet.DateFrom:=Trunc(fmSelPerSkl.cxDateEdit1.Date);
    CommonSet.DateTo:=Trunc(fmSelPerSkl.cxDateEdit1.Date)+1;
    iDate:=Trunc(fmSelPerSkl.cxDateEdit1.Date);
    CommonSet.IdStore:=fmSelPerSkl.cxLookupComboBox1.EditValue;
    CommonSet.NameStore:=fmSelPerSkl.cxLookupComboBox1.Text;

    fmMoveSel.Caption:='������� �� �� '+CommonSet.NameStore+' �� ����� '+FormatDateTiMe('dd.mm.yyyy',iDate);
    fmSelPerSkl.quMHAll1.Active:=False;
    fmSelPerSkl.Release;
    with dmO do
    begin
      idPar:=GetId('Pars');
      taParams.Active:=False;
      taParams.Active:=True;
      taParams.Append;
      taParamsID.AsInteger:=idPar;
      taParamsIDATEB.AsInteger:=iDate;
      taParamsIDATEE.AsInteger:=iDate;
      taParamsIDSTORE.AsInteger:=CommonSet.IdStore;
      taParams.Post;
      taParams.Active:=False;

      quRemnDate.Active:=False;
      quRemnDate.Active:=True;

    end;
    fmMoveSel.LevelRemn.Visible:=True;
    fmMoveSel.LevelMoveSel.Visible:=False;

    Cursor:=crDefault; Delay(10);
    fmMoveSel.Show;

  end else
  begin
    fmSelPerSkl.quMHAll1.Active:=False;
    fmSelPerSkl.Release;
  end;
end;

procedure TfmMainRnOffice.acInvExecute(Sender: TObject);
begin
  //��������������
  if CommonSet.DateTo>=iMaxDate then fmDocsInv.Caption:='�������������� � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
  else fmDocsInv.Caption:='�������������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);
  with dmO do
  with dmORep do
  begin
    quDocsInvSel.Active:=False;
    quDocsInvSel.ParamByName('DATEB').AsDate:=CommonSet.DateFrom;
    quDocsInvSel.ParamByName('DATEE').AsDate:=CommonSet.DateTo;
    quDocsInvSel.Active:=True;
  end;
  fmDocsInv.LevelDocsInv.Visible:=True;
  fmDocsInv.LevelCardsInv.Visible:=False;
  fmDocsInv.SpeedItem3.Visible:=True;
  fmDocsInv.SpeedItem4.Visible:=True;
  fmDocsInv.SpeedItem5.Visible:=True;
  fmDocsInv.SpeedItem6.Visible:=True;
  fmDocsInv.SpeedItem7.Visible:=True;
  fmDocsInv.SpeedItem8.Visible:=True;

  fmDocsInv.Show;
end;

procedure TfmMainRnOffice.acMenuExecute(Sender: TObject);
begin
  //���� ���������

  fmMenuCr:=tfmMenuCr.Create(Application);

  fmMenuCr.Show;
end;

procedure TfmMainRnOffice.acMoveDateExecute(Sender: TObject);
Var IdPar:INteger;
begin
//�������� �� ������ �� ������
  fmSelPerSkl:=tfmSelPerSkl.Create(Application);
  with fmSelPerSkl do
  begin
    cxDateEdit1.Date:=CommonSet.DateFrom;
    quMHAll1.Active:=False;
    quMHAll1.Active:=True;
    quMHAll1.First;
    cxLookupComboBox1.EditValue:=quMHAll1ID.AsInteger;
  end;
  fmSelPerSkl.ShowModal;
  if fmSelPerSkl.ModalResult=mrOk then
  begin
    CommonSet.DateFrom:=Trunc(fmSelPerSkl.cxDateEdit1.Date);
    CommonSet.DateTo:=Trunc(fmSelPerSkl.cxDateEdit2.Date)+1;
    CommonSet.IdStore:=fmSelPerSkl.cxLookupComboBox1.EditValue;
    CommonSet.NameStore:=fmSelPerSkl.cxLookupComboBox1.Text;

    if CommonSet.DateTo>=iMaxDate then fmMoveSel.Caption:='�������� �� �� '+CommonSet.NameStore+' � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
    else fmMoveSel.Caption:='�������� �� �� '+CommonSet.NameStore+' �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);
    fmSelPerSkl.quMHAll1.Active:=False;
    fmSelPerSkl.Release;
    with dmO do
    begin
      idPar:=GetId('Pars');
      taParams.Active:=False;
      taParams.Active:=True;
      taParams.Append;
      taParamsID.AsInteger:=idPar;
      taParamsIDATEB.AsInteger:=Trunc(CommonSet.DateFrom);
      taParamsIDATEE.AsInteger:=Trunc(CommonSet.DateTo-1);
      taParamsIDSTORE.AsInteger:=CommonSet.IdStore;
      taParams.Post;
      taParams.Active:=False;

      quMoveSel.Active:=False;
      quMoveSel.Active:=True;

    end;
    fmMoveSel.LevelRemn.Visible:=False;
    fmMoveSel.LevelMoveSel.Visible:=True;

    Cursor:=crDefault; Delay(10);
    fmMoveSel.Show;

  end else
  begin
    fmSelPerSkl.quMHAll1.Active:=False;
    fmSelPerSkl.Release;
  end;
end;

procedure TfmMainRnOffice.acOutBExecute(Sender: TObject);
begin
  //������ ����
  if CommonSet.DateTo>=iMaxDate then fmDocsOutB.Caption:='���������� � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
  else fmDocsOutB.Caption:='���������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);
  with dmO do
  begin
    quDocsOutB.Active:=False;
    quDocsOutB.ParamByName('DATEB').AsDate:=CommonSet.DateFrom;
    quDocsOutB.ParamByName('DATEE').AsDate:=CommonSet.DateTo;
    quDocsOutB.Active:=True;
  end;
  fmDocsOutB.Show;

end;

procedure TfmMainRnOffice.acExportBuhExecute(Sender: TObject);
begin
//������� � �����������
  fmExport:=TfmExport.Create(Application);
  with fmExport do
  begin
    cxDateEdit1.Date:=CommonSet.DateFrom;
    cxDateEdit2.Date:=Date;
    ProgressBar1.Visible:=False;
    ProgressBar1.Position:=0;
  end;
  fmExport.ShowModal;
  fmExport.Release;
end;

procedure TfmMainRnOffice.acTovRepExecute(Sender: TObject);
begin
 //�������� �����
  fmSelPerSkl:=tfmSelPerSkl.Create(Application);
  with fmSelPerSkl do
  begin
    cxDateEdit1.Date:=CommonSet.DateFrom;
    quMHAll1.Active:=False;
    quMHAll1.Active:=True;
    quMHAll1.First;
    cxLookupComboBox1.EditValue:=quMHAll1ID.AsInteger;
    if CommonSet.IdStore>0 then  cxLookupComboBox1.EditValue:=CommonSet.IdStore;
  end;
  fmSelPerSkl.ShowModal;
  if fmSelPerSkl.ModalResult=mrOk then
  begin
    CommonSet.DateFrom:=Trunc(fmSelPerSkl.cxDateEdit1.Date);
    CommonSet.DateTo:=Trunc(fmSelPerSkl.cxDateEdit2.Date)+1;
    CommonSet.IdStore:=fmSelPerSkl.cxLookupComboBox1.EditValue;
    CommonSet.NameStore:=fmSelPerSkl.cxLookupComboBox1.Text;

    if CommonSet.DateTo>=iMaxDate then fmTO.Caption:='�������� ������ �� �� '+CommonSet.NameStore+' � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
    else fmTO.Caption:='�������� ������ �� �� '+CommonSet.NameStore+' �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);
    fmSelPerSkl.quMHAll1.Active:=False;
    fmSelPerSkl.Release;
    with dmO do
    begin
      quTO.Active:=False;
      quTO.ParamByName('DATEB').AsInteger:=Trunc(CommonSet.DateFrom);
      quTO.ParamByName('DATEE').AsInteger:=Trunc(CommonSet.DateTo);
      quTO.ParamByName('IDSKL').AsInteger:=CommonSet.IdStore;
      quTO.Active:=True;

      fmTO.ShowModal;

      quTO.Active:=False;
    end;
  end else
  begin
    fmSelPerSkl.quMHAll1.Active:=False;
    fmSelPerSkl.Release;
  end;
end;

procedure TfmMainRnOffice.acRepPribExecute(Sender: TObject);
Var rSumOut,rSumMarg:Real;
    iGr:Integer;
begin
  //����� �� �������
  if not CanDo('prRepPrib') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;

  fmSelPerSkl:=tfmSelPerSkl.Create(Application);
  with fmSelPerSkl do
  begin
    cxDateEdit1.Date:=CommonSet.DateFrom;
    quMHAll1.Active:=False;
    quMHAll1.Active:=True;
    quMHAll1.First;
    cxLookupComboBox1.EditValue:=quMHAll1ID.AsInteger;
  end;
  fmSelPerSkl.ShowModal;
  if fmSelPerSkl.ModalResult=mrOk then
  begin
    CommonSet.DateFrom:=Trunc(fmSelPerSkl.cxDateEdit1.Date);
    CommonSet.DateTo:=Trunc(fmSelPerSkl.cxDateEdit2.Date)+1;
    CommonSet.IdStore:=fmSelPerSkl.cxLookupComboBox1.EditValue;
    CommonSet.NameStore:=fmSelPerSkl.cxLookupComboBox1.Text;

    if CommonSet.DateTo>=iMaxDate then fmRepPrib.Caption:='����� �� ������� �� �� '+CommonSet.NameStore+' � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
    else fmRepPrib.Caption:='����� �� ������� �� �� '+CommonSet.NameStore+' �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);
    fmSelPerSkl.quMHAll1.Active:=False;
    fmSelPerSkl.Release;
    with dmO do
    with dmORep do
    begin
       fmRepPrib.Show;  delay(10);//}
       fmRepPrib.Memo1.Lines.Add('����� .. ���� ������������ ������.'); delay(10);
       fmRepPrib.Memo1.Lines.Add('  ���������� �� ������.'); delay(10);

       quSelPrib.Active:=False;
       quSelPrib.ParamByName('DATEB').AsDateTime:=CommonSet.DateFrom;
       quSelPrib.ParamByName('DATEE').AsDateTime:=CommonSet.DateTo;
       quSelPrib.ParamByName('IDSTORE').AsInteger:=CommonSet.IdStore;
       quSelPrib.Active:=True;

       fmRepPrib.Memo1.Lines.Add('  ���������� �� �������.'); delay(10);

       quSelPribGr.Active:=False;
       quSelPribGr.ParamByName('DATEB').AsDateTime:=CommonSet.DateFrom;
       quSelPribGr.ParamByName('DATEE').AsDateTime:=CommonSet.DateTo;
       quSelPribGr.ParamByName('IDSTORE').AsInteger:=CommonSet.IdStore;
       quSelPribGr.Active:=True;

       fmRepPrib.Memo1.Lines.Add('  ��������� ������.'); delay(10);
       with fmRepPrib do
       begin
         ViewPrib.BeginUpdate;
         taPrib.Active:=False;
         taPrib.CreateDataSet;

         quSelPrib.First;
         while not quSelPrib.Eof do
         begin
           taPrib.Append;
           taPribCodeB.AsInteger:=quSelPribCODEB.AsInteger;
           taPribName.AsString:=quSelPribNAME.AsString;
           taPribParent.AsInteger:=quSelPribPARENT.AsInteger;
           taPribNameClass.AsString:=quSelPribNAMECL.AsString;
           taPribQuant.AsFloat:=quSelPribQUANT.AsFloat;
           taPribSumOut.AsFloat:=quSelPribSUMOUT.AsFloat;
           taPribSumIn.AsFloat:=quSelPribSUMIN.AsFloat;

           taPribPriceIn.AsFloat:=0;
           taPribPriceOut.AsFloat:=0;
           taPribPr1.AsFloat:=0;
           taPribPr2.AsFloat:=0;
           taPribPr3.AsFloat:=0;
           taPribDiff.AsFloat:=0;
           taPribDiffSum.AsFloat:=quSelPribSUMOUT.AsFloat-quSelPribSUMIN.AsFloat;

           if abs(quSelPribQUANT.AsFloat)>0 then
           begin
             taPribPriceIn.AsFloat:=RoundEx(quSelPribSUMIN.AsFloat/quSelPribQUANT.AsFloat*100)/100;
             taPribPriceOut.AsFloat:=RoundEx(quSelPribSUMOUT.AsFloat/quSelPribQUANT.AsFloat*100)/100;
             taPribDiff.AsFloat:=RoundEx(quSelPribSUMOUT.AsFloat/quSelPribQUANT.AsFloat*100)/100-RoundEx(quSelPribSUMIN.AsFloat/quSelPribQUANT.AsFloat*100)/100;
           end;
           if abs(quSelPribSUMOUT.AsFloat)>0 then
           begin
             taPribPr2.AsFloat:=RoundEx(quSelPribSUMIN.AsFloat/quSelPribSUMOUT.AsFloat*1000)/10;
           end;

           iGr:=quSelPribPARENT.AsInteger;
           if quSelPribGr.Locate('PARENT',iGr,[]) then
           begin
             rSumOut:=quSelPribGrSUMOUT.AsFloat;
             rSumMarg:=quSelPribGrSUMOUT.AsFloat-quSelPribGrSUMIN.AsFloat;
             if rSumOut<>0 then
             begin
               taPribPr1.AsFloat:=RoundEx(quSelPribSUMOUT.AsFloat/rSumOut*1000)/10;
             end;
             if rSumMarg<>0 then
             begin
               taPribPr3.AsFloat:=RoundEx((quSelPribSUMOUT.AsFloat-quSelPribSUMIN.AsFloat)/rSumMarg*1000)/10;
             end;
           end;
           taPrib.Post;

           quSelPrib.Next;
         end;
         ViewPrib.EndUpdate;
       end;

       fmRepPrib.Memo1.Lines.Add('������������ ��.'); delay(10);
       quSelPrib.Active:=False;
       quSelPribGr.Active:=False;
    end;
  end else
  begin
    fmSelPerSkl.quMHAll1.Active:=False;
    fmSelPerSkl.Release;
  end;
end;

procedure TfmMainRnOffice.acObVedExecute(Sender: TObject);
Var  IdPar:Integer;
     bAdd:Boolean;
     rQMain,rSum,rKm:Real;
begin
//��������� ���������
  if not CanDo('prRepObVed') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;

  fmSelPerSkl:=tfmSelPerSkl.Create(Application);
  with fmSelPerSkl do
  begin
    cxDateEdit1.Date:=CommonSet.DateFrom;
    quMHAll1.Active:=False;
    quMHAll1.Active:=True;
    quMHAll1.First;
    cxLookupComboBox1.EditValue:=quMHAll1ID.AsInteger;
  end;
  fmSelPerSkl.ShowModal;
  if fmSelPerSkl.ModalResult=mrOk then
  begin
    CommonSet.DateFrom:=Trunc(fmSelPerSkl.cxDateEdit1.Date);
    CommonSet.DateTo:=Trunc(fmSelPerSkl.cxDateEdit2.Date)+1;
    CommonSet.IdStore:=fmSelPerSkl.cxLookupComboBox1.EditValue;
    CommonSet.NameStore:=fmSelPerSkl.cxLookupComboBox1.Text;

    if CommonSet.DateTo>=iMaxDate then fmRepOb.Caption:='��������� ��������� �� �� '+CommonSet.NameStore+' � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
    else fmRepOb.Caption:='��������� ��������� �� �� '+CommonSet.NameStore+' �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);
    fmSelPerSkl.quMHAll1.Active:=False;
    fmSelPerSkl.Release;

    with dmO do
    with dmORep do
    begin
      fmRepOb.Show;  delay(10);//}
      fmRepOb.Memo1.Lines.Add('����� .. ���� ������������ ������.'); delay(10);
      fmRepOb.Memo1.Lines.Add('  ������� �� ������.'); delay(10);

      idPar:=GetId('Pars');
      taParams.Active:=False;
      taParams.Active:=True;
      taParams.Append;
      taParamsID.AsInteger:=idPar;
      taParamsIDATEB.AsInteger:=Trunc(CommonSet.DateFrom-1); //-1 �.�. ���� ������
      taParamsIDATEE.AsInteger:=Trunc(CommonSet.DateFrom-1);
      taParamsIDSTORE.AsInteger:=CommonSet.IdStore;
      taParams.Post;
      taParams.Active:=False;

      quRemnDate.Active:=False;
      quRemnDate.Active:=True;

      fmRepOb.Memo1.Lines.Add('  ��������� ��������.'); delay(10);

      fmRepOb.ViewObVed.BeginUpdate;
      fmRepOb.taObVed.Active:=False;
      fmRepOb.taObVed.CreateDataSet;

      quRemnDate.First;
      while not quRemnDate.Eof do
      begin
        with fmRepOb do
        begin
          bAdd:=False;
          if quRemnDateTCARD.AsInteger=1 then  //��� �����
          begin
            if abs(quRemnDateREMN.AsFloat)>0.000001 then bAdd:=True; //�� ������� ���� !!!
          end
          else bAdd:=True;

          if bAdd then
          begin
            //��� ��� ������� ����� ������� - ����� ���� �� ������ ���������
            //Function prCalcRemnSum(iCode,iDate,iSkl:Integer;rQ:Real):Real;
            //MOV.REMN/ME.KOEF
            rQMain:=quRemnDateREMN.AsFloat*quRemnDateKOEF.AsFloat;
            rSum:=prCalcRemnSum(quRemnDateID.AsInteger,Trunc(CommonSet.DateFrom-1),CommonSet.IdStore,rQMain);

            taObVed.Append;
            taObVedIdCode.AsInteger:=quRemnDateID.AsInteger;
            taObVedNameC.AsString:=quRemnDateNAME.AsString;
            taObVediM.AsInteger:=quRemnDateIMESSURE.AsInteger;
            taObVedSm.AsString:=quRemnDateNAMEMESSURE.AsString;
            taObVedIdGroup.AsInteger:=quRemnDatePARENT.AsInteger;
            taObVedNameClass.AsString:=quRemnDateNAMECL.AsString;
            taObVedQBeg.AsFloat:=quRemnDateREMN.AsFloat;
            taObVedSBeg.AsFloat:=rSum;
            taObVedQIn.AsFloat:=0;
            taObVedSIn.AsFloat:=0;
            taObVedQOut.AsFloat:=0;
            taObVedSOut.AsFloat:=0;
            taObVedPrice.AsFloat:=0;
            taObVedKm.AsFloat:=quRemnDateKOEF.AsFloat;
            taObVed.Post;
          end;
        end;
        quRemnDate.Next; delay(10);
      end;

      quRemnDate.Active:=False;

      fmRepOb.Memo1.Lines.Add('  ��������� ��������.'); delay(10);

      quSelInSum.Active:=False;
      quSelInSum.ParamByName('DATEB').AsInteger:=Trunc(CommonSet.DateFrom);
      quSelInSum.ParamByName('DATEE').AsInteger:=Trunc(CommonSet.DateTo);
      quSelInSum.ParamByName('IDSKL').AsInteger:=CommonSet.IdStore;
      quSelInSum.Active:=True;

      quSelInSum.First;
      while not quSelInSum.Eof do
      begin
        with fmRepOb do
        begin
          if taObVed.Locate('IdCode',quSelInSumARTICUL.AsInteger,[]) then
          begin
            rKm:=taObVedKm.AsFloat;
            taObVed.Edit;
            if rKm<>0 then taObVedQIn.AsFloat:=quSelInSumQSUM.AsFloat/rKm
            else taObVedQIn.AsFloat:=0;
            taObVedSIn.AsFloat:=quSelInSumSUMIN.AsFloat;
            taObVed.Post;
          end else
          begin  //���� ���������
            quSelNamePos.Active:=False;
            quSelNamePos.ParamByName('IDCARD').AsInteger:=quSelInSumARTICUL.AsInteger;
            quSelNamePos.Active:=True;

            quSelNamePos.First;
            if quSelNamePos.RecordCount>0 then
            begin
              rKm:=quSelNamePosKOEF.AsFloat;
              taObVed.Append;
              taObVedIdCode.AsInteger:=quSelInSumARTICUL.AsInteger;
              taObVedNameC.AsString:=quSelNamePosNAME.AsString;
              taObVediM.AsInteger:=quSelNamePosIMESSURE.AsInteger;
              taObVedSm.AsString:=quSelNamePosSM.AsString;
              taObVedIdGroup.AsInteger:=quSelNamePosPARENT.AsInteger;
              taObVedNameClass.AsString:=quSelNamePosNAMECL.AsString;
              taObVedQBeg.AsFloat:=0;
              taObVedSBeg.AsFloat:=0;
              taObVedQIn.AsFloat:=0;

              if rKm<>0 then taObVedQIn.AsFloat:=quSelInSumQSUM.AsFloat/rKm;
              taObVedSIn.AsFloat:=quSelInSumSUMIN.AsFloat;

              taObVedQOut.AsFloat:=0;
              taObVedSOut.AsFloat:=0;
              taObVedPrice.AsFloat:=0;
              taObVedKm.AsFloat:=quSelNamePosKOEF.AsFloat;
              taObVed.Post;
            end;
            quSelNamePos.Active:=False;
          end;
        end;
        quSelInSum.Next;
      end;
      quSelInSum.Active:=False;

      fmRepOb.Memo1.Lines.Add('  ��������� ��������.'); delay(10);

      quSelOutSum.Active:=False;
      quSelOutSum.ParamByName('DATEB').AsInteger:=Trunc(CommonSet.DateFrom);
      quSelOutSum.ParamByName('DATEE').AsInteger:=Trunc(CommonSet.DateTo);
      quSelOutSum.ParamByName('IDSKL').AsInteger:=CommonSet.IdStore;
      quSelOutSum.Active:=True;

      quSelOutSum.First;
      while not quSelOutSum.Eof do
      begin
        with fmRepOb do
        begin
          if taObVed.Locate('IdCode',quSelOutSumARTICUL.AsInteger,[]) then
          begin
            rKm:=taObVedKm.AsFloat;
            taObVed.Edit;
            if rKm<>0 then taObVedQOut.AsFloat:=quSelOutSumQSUM.AsFloat/rKm
            else taObVedQOut.AsFloat:=0;
            taObVedSOut.AsFloat:=quSelOutSumSUMOUT.AsFloat;
            taObVed.Post;
          end else
          begin  //���� ���������
            quSelNamePos.Active:=False;
            quSelNamePos.ParamByName('IDCARD').AsInteger:=quSelOutSumARTICUL.AsInteger;
            quSelNamePos.Active:=True;

            quSelNamePos.First;
            if quSelNamePos.RecordCount>0 then
            begin
              rKm:=quSelNamePosKOEF.AsFloat;
              taObVed.Append;
              taObVedIdCode.AsInteger:=quSelOutSumARTICUL.AsInteger;
              taObVedNameC.AsString:=quSelNamePosNAME.AsString;
              taObVediM.AsInteger:=quSelNamePosIMESSURE.AsInteger;
              taObVedSm.AsString:=quSelNamePosSM.AsString;
              taObVedIdGroup.AsInteger:=quSelNamePosPARENT.AsInteger;
              taObVedNameClass.AsString:=quSelNamePosNAMECL.AsString;
              taObVedQBeg.AsFloat:=0;
              taObVedSBeg.AsFloat:=0;
              taObVedQIn.AsFloat:=0;
              taObVedSIn.AsFloat:=0;

              taObVedQOut.AsFloat:=0;
              if rKm<>0 then taObVedQOut.AsFloat:=quSelOutSumQSUM.AsFloat/rKm;
              taObVedSOut.AsFloat:=quSelOutSumSUMOut.AsFloat;

              taObVedPrice.AsFloat:=0;
              taObVedKm.AsFloat:=quSelNamePosKOEF.AsFloat;
              taObVed.Post;
            end;
            quSelNamePos.Active:=False;
          end;
        end;
        quSelOutSum.Next;
      end;
      quSelOutSum.Active:=False;

      fmRepOb.ViewObVed.EndUpdate;

      fmRepOb.Memo1.Lines.Add('������������ ��.'); delay(10);
    end;
  end else
  begin
    fmSelPerSkl.quMHAll1.Active:=False;
    fmSelPerSkl.Release;
  end;
end;


end.
