unit MainFF;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, cxStyles, cxCustomData, cxGraphics,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxCurrencyEdit,
  cxCalc, cxImageComboBox, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridBandedTableView, cxGridDBBandedTableView,
  cxClasses, cxControls, cxGridCustomView, cxGrid, dxfBackGround, RXClock,
  Menus, cxLookAndFeelPainters, cxButtons, cxTextEdit, cxGridCardView,
  cxGridDBCardView, ActnList, XPStyleActnCtrls, ActnMan, cxContainer,
  cxGridDBTableView, DBClient, ComObj, VaClasses, VaComm, cxMaskEdit,
  cxSpinEdit, ScktComp;

type
  TfmMainFF = class(TForm)
    Timer1: TTimer;
    Panel1: TPanel;
    Label8: TLabel;
    Label9: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    GridSpec: TcxGrid;
    ViewSpec: TcxGridDBBandedTableView;
//    ViewSpecSIFR: TcxGridDBBandedColumn;
    ViewSpecNAME: TcxGridDBBandedColumn;
    ViewSpecPRICE: TcxGridDBBandedColumn;
    ViewSpecQUANTITY: TcxGridDBBandedColumn;
    ViewSpecSUMMA: TcxGridDBBandedColumn;
    ViewSpecDPROC: TcxGridDBBandedColumn;
    ViewSpecDSUM: TcxGridDBBandedColumn;
    ViewSpecISTATUS: TcxGridDBBandedColumn;
    LevelSpec: TcxGridLevel;
    dxfBackGround1: TdxfBackGround;
    RxClock1: TRxClock;
    Panel2: TPanel;
    Button1: TcxButton;
    Button2: TcxButton;
    GridM: TcxGrid;
    ViewM: TcxGridDBCardView;
    ViewMINFO: TcxGridDBCardViewRow;
    ViewMTREETYPE: TcxGridDBCardViewRow;
    ViewMSPRICE: TcxGridDBCardViewRow;
    LevelM: TcxGridLevel;
    amMenu: TActionManager;
    acUpUp: TAction;
    acUp: TAction;
    acExit: TAction;
    acOpenSelMenu: TAction;
    acSelOk: TAction;
    Button5: TcxButton;
    cEdit1: TcxCurrencyEdit;
    LevelModif: TcxGridLevel;
    ViewModif: TcxGridDBTableView;
    ViewModifID_TAB: TcxGridDBColumn;
    ViewModifID_POS: TcxGridDBColumn;
    ViewModifID: TcxGridDBColumn;
    ViewModifSIFR: TcxGridDBColumn;
    ViewModifNAME: TcxGridDBColumn;
    ViewModifQUANTITY: TcxGridDBColumn;
    acModify: TAction;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    Panel3: TPanel;
    Label1: TLabel;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    cxButton5: TcxButton;
    cxButton6: TcxButton;
    acDiscount: TAction;
    Label7: TLabel;
    Panel4: TPanel;
    Label10: TLabel;
    Panel5: TPanel;
    cxButton7: TcxButton;
    cxButton8: TcxButton;
    cxButton9: TcxButton;
    cxButton10: TcxButton;
    acCashPCard: TAction;
    taServP: TClientDataSet;
    taServPName: TStringField;
    taServPCode: TStringField;
    taServPQuant: TFloatField;
    taServPStream: TIntegerField;
    taServPiType: TSmallintField;
    cxButton11: TcxButton;
    cxButton12: TcxButton;
    cxButton13: TcxButton;
    acTabs: TAction;
    TimerDelayPrintQ: TTimer;
    TimerPrintQ: TTimer;
    cxButton14: TcxButton;
    ViewSpecSIFR: TcxGridDBBandedColumn;
    cxButton15: TcxButton;
    TimerTr: TTimer;
    Label11: TLabel;
    cxButton16: TcxButton;
    cxButton17: TcxButton;
    Edit1: TEdit;
    acBarC: TAction;
    devCustD: TVaComm;
    cxButton18: TcxButton;
    cxButton19: TcxButton;
    Label12: TLabel;
    cxButton20: TcxButton;
    Label13: TLabel;
    cxButton21: TcxButton;
    Panel6: TPanel;
    cxSpinEdit1: TcxSpinEdit;
    cxButton22: TcxButton;
    cxButton23: TcxButton;
    tiP: TTimer;
    acOpenP: TAction;
    cxButton26: TcxButton;
    TimerSdTo0: TTimer;
    Panel7: TPanel;
    GridHK: TcxGrid;
    ViewHK: TcxGridTableView;
    LevelHK: TcxGridLevel;
    Panel8: TPanel;
    GridP: TcxGrid;
    ViewP: TcxGridDBCardView;
    ViewPID_TAB: TcxGridDBCardViewRow;
    ViewPSUMP: TcxGridDBCardViewRow;
    LevelP: TcxGridLevel;
    cxButton24: TcxButton;
    cxButton25: TcxButton;
    Label14: TLabel;
    ClientSock: TClientSocket;
    cxButton27: TcxButton;
    procedure FormCanResize(Sender: TObject; var NewWidth,
      NewHeight: Integer; var Resize: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure ViewMCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure acOpenSelMenuExecute(Sender: TObject);
    procedure acUpUpExecute(Sender: TObject);
    procedure acUpExecute(Sender: TObject);
    procedure ViewMCellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure acModifyExecute(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure Panel3Click(Sender: TObject);
    procedure RxClock1Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure ViewMDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure ViewHKDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure ViewMMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ViewHKDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure ViewHKCellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure acDiscountExecute(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure Panel4Click(Sender: TObject);
    procedure cxButton7Click(Sender: TObject);
    procedure cxButton8Click(Sender: TObject);
    procedure cxButton9Click(Sender: TObject);
    procedure acCashPCardExecute(Sender: TObject);
    procedure cxButton11Click(Sender: TObject);
    procedure cxButton12Click(Sender: TObject);
    procedure acTabsExecute(Sender: TObject);
    procedure TimerDelayPrintQTimer(Sender: TObject);
    procedure TimerPrintQTimer(Sender: TObject);
    procedure cxButton14Click(Sender: TObject);
    procedure cxButton15Click(Sender: TObject);
    procedure TimerTrTimer(Sender: TObject);
    procedure cxButton16Click(Sender: TObject);
    procedure cxButton17Click(Sender: TObject);
    procedure acBarCExecute(Sender: TObject);
    procedure Edit1KeyPress(Sender: TObject; var Key: Char);
    procedure cxButton18Click(Sender: TObject);
    procedure cxButton19Click(Sender: TObject);
    procedure cxButton20Click(Sender: TObject);
    procedure cxButton21Click(Sender: TObject);
    procedure cxButton22Click(Sender: TObject);
    procedure cxButton23Click(Sender: TObject);
    procedure tiPTimer(Sender: TObject);
    procedure ViewPCellDblClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure acOpenPExecute(Sender: TObject);
    procedure cxButton25Click(Sender: TObject);
    procedure cxButton26Click(Sender: TObject);
    procedure TimerSdTo0Timer(Sender: TObject);
    procedure FormClick(Sender: TObject);
    procedure ClientSockRead(Sender: TObject; Socket: TCustomWinSocket);
    procedure cxButton27Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    Procedure RecalcSum;
    Procedure CreateHotList;
    Procedure DestrViewPers;
    Procedure CreateViewPers;
    Function prSaveSpec(rSumCh:Real):Integer;
    procedure prGetMenu;
    procedure prWriteReal;
    function prOpenTr:Boolean;
    procedure prCloseTr;
  end;

Procedure WriteStatus;
Procedure ToCustomDisp(Str1,Str2:String);
Procedure TestFp(StrOp:String);

var
  fmMainFF: TfmMainFF;
  TableImage:TBitmap;
  FGridBrush:TBrush;
  bDr:Boolean;
  Sifr:INteger;

  MaxCol:Integer;
  MaxLine:Integer;
  ColWidth:Integer;

  hToken: THandle;
  tkp: TTokenPrivileges;
  tkpo: TTokenPrivileges;
  zero: DWORD;

const sVer:String = 'v 17.03';


implementation

uses Dm, Un1, UnCash, PreFF, ModifFF, CashEnd, Calc, Discont, Attention,
  UnitBN, CredCards, PreFF1, fmDiscountShape, UnDP2300, BnSber, TabsFF,
  uDB1, prdb, uDMTR, Shutd, u2fdk, UnInOutM, CopyCh, UnPrizma, Vtb24un,
  AddPodnos;

{$R *.dfm}

Procedure ToCustomDisp(Str1,Str2:String);
Var Buff:array[1..44] of byte;
    i:INteger;
begin
  if CommonSet.TypeDP=4 then
  begin
    if (CommonSet.TypeFis='sp101') then
    begin
      SetDP(Str1,Str2);
    end;
  end else
  begin

  if pos('COM',CommonSet.PortDP)=0 then exit;
  with fmMainFF do
  begin
    try
      if devCustD.Active=False then
      begin
        DevCustD.DeviceName:=CommonSet.PortDP;
        DevCustD.Baudrate:=br9600;
        DEvCustD.Parity:=paNone;
        DEvCustD.Stopbits:=sb1;
        DEvCustD.Databits:=db8;
        DevCustD.Open;
      end;
    except
    end;
    if devCustD.Active then
    begin
      if Length(Str1)>20 then Str1:=Copy(Str1,1,20);
      if Length(Str2)>20 then Str2:=Copy(Str2,1,20);

      Str1:=AnsiToOemConvert(Str1);
      Str2:=AnsiToOemConvert(Str2);

      if CommonSet.TypeDP=1 then
      begin
        Buff[1]:=$0C;
        devCustD.WriteBuf(Buff,1); //T���
        delay(50);
        for i:=1 to 44 do Buff[i]:=$20; //����� ���������
        Buff[1]:=$1B; Buff[2]:=$51; Buff[3]:=$41;
        for i:=1 to Length(Str1) do Buff[i+3]:=ord(Str1[i]);
        Buff[44]:=$0D;
        devCustD.WriteBuf(Buff,44); //1 ������
//      devCustD.WriteBuf(Buff,43); //1 ������
        delay(50);


        for i:=1 to 44 do Buff[i]:=$20; //����� ���������
        Buff[1]:=$1B; Buff[2]:=$51; Buff[3]:=$42;
        for i:=1 to Length(Str2) do Buff[i+3]:=ord(Str2[i]);
        Buff[44]:=$0D;
        devCustD.WriteBuf(Buff,44); //2 ������
      end;
      if CommonSet.TypeDP=2 then
      begin
        Buff[1]:=$0C;
        devCustD.WriteBuf(Buff,1); //T���
        delay(50);

        for i:=1 to 23 do Buff[i]:=$20; //����� ���������
        Buff[1]:=$1B; Buff[2]:=$51; Buff[3]:=$41;
        for i:=1 to Length(Str1) do Buff[i+3]:=ord(Str1[i]);
        devCustD.WriteBuf(Buff,23);
        delay(50);

        for i:=1 to 23 do Buff[i]:=$20;
        Buff[1]:=$1B; Buff[2]:=$51; Buff[3]:=$42;
        for i:=1 to Length(Str2) do Buff[i+3]:=ord(Str2[i]);
        devCustD.WriteBuf(Buff,23);
      end;
      if CommonSet.TypeDP=3 then
      begin
        Buff[1]:=$0C;
        devCustD.WriteBuf(Buff,1);
        delay(50);

        Buff[1]:=$1B; Buff[2]:=$52; Buff[3]:=$0c;
        devCustD.WriteBuf(Buff,3);
        delay(50);

        Buff[1]:=$1B; Buff[2]:=$74; Buff[3]:=$06;
        devCustD.WriteBuf(Buff,3);
        delay(50);

        Buff[1]:=$0B;
        for i:=2 to 41 do Buff[i]:=$20;
        for i:=1 to Length(Str1) do Buff[i+1]:=ord(Str1[i]);
        for i:=1 to Length(Str2) do Buff[i+21]:=ord(Str2[i]);

        devCustD.WriteBuf(Buff,41);
      end;

      delay(50);
    end;
  end;
  end;
end;



Function TFmMainFF.prSaveSpec(rSumCh:Real):Integer;
Var IdH,iNum:INteger;
    rSum,rSumD:Real;
    rSumDiscDop,rProcD:Real;
begin
  with dmC do
  begin
    Tab.Quests:=cxSpinEdit1.Value;

    if CommonSet.UseQuest=1 then cxSpinEdit1.Value:=0; //�������� ������������� ������� ������

    IdH:=GetId('TH'); //���������� ����� ��������� ������ (���� ������)
    Result:=IdH;

    ViewSpec.BeginUpdate;

    rSum:=0;  rSumD:=0; rProcD:=0;
    quCurSpec.First;
    while not quCurSpec.Eof do
    begin      //��������� ������� ������ ���, ������ ��� ��� ����
      rSum:=rSum+quCurSpecSUMMA.AsFloat;
      rSumD:=rSumD+quCurSpecDSUM.AsFloat;
      if rProcD=0 then rProcD:=quCurSpecDPROC.AsFloat;
      quCurSpec.Next;
    end;

    rSumDiscDop:=rSum-rSumCh;

    quCurSpec.First;
    while (quCurSpec.Eof=False)and(rSumDiscDop>0) do
    begin      //��������� ������� ������ ���, ������ ��� ��� ����
      if quCurSpecSUMMA.AsFloat>rSumDiscDop then
      begin
        quCurSpec.Edit;
        quCurSpecSUMMA.AsFloat:=quCurSpecSUMMA.AsFloat-rSumDiscDop;
        quCurSpec.Post;
        Break;
      end;
      quCurSpec.Next;
    end;

    if Prizma then
    begin
      if operation<>1 then
      begin
        prWriteLog('!!CheckEnd');
        Event_RegEx(5,Nums.iCheckNum,0,0,'','',0,0,0);
      end else
      begin
        prWriteLog('!!CheckRetEnd');
        Event_RegEx(47,Nums.iCheckNum,0,0,'','',0,0,0);
      end;
    end;

    taTabAll.Active:=False;
    taTabAll.ParamByName('TID').AsInteger:=IdH;
    taTabAll.Active:=True;

    taTabAll.Append;
    taTabAllID.AsInteger:=IdH;
    taTabAllID_PERSONAL.AsInteger:=Tab.Id_Personal;
    taTabAllNUMTABLE.AsString:=Tab.NumTable;
    taTabAllQUESTS.AsInteger:=Tab.Quests;
    taTabAllTABSUM.AsFloat:=rSumCh;
    taTabAllBEGTIME.AsDateTime:=Tab.OpenTime;
    taTabAllENDTIME.AsDateTime:=now;
    taTabAllDISCONT.AsString:=Tab.DBar;
    taTabAllDISCONT1.AsString:=Tab.PBar;

    Case Operation of
    0: begin
         if iCashType=0 then taTabAllOPERTYPE.AsString:='Sale'
         else taTabAllOPERTYPE.AsString:='SaleBank';
       end;
    1: begin
         if iCashType=0 then taTabAllOPERTYPE.AsString:='Ret'
         else taTabAllOPERTYPE.AsString:='RetBank';
       end;
    2: begin
         taTabAllOPERTYPE.AsString:='SalePC';
       end;
    end;

    if CommonSet.CashNum<0 then taTabAllCHECKNUM.AsInteger:=CommonSet.CashChNum
    else taTabAllCHECKNUM.AsInteger:=Nums.iCheckNum;

    taTabAllSKLAD.AsInteger:=0;
    taTabAllSTATION.AsInteger:=CommonSet.Station;
    taTabAllNUMZ.AsInteger:=CommonSet.CashZ; //������������� �.�. �� ����������
    taTabAllDELT.AsInteger:=0;
    taTabAllSALET.AsInteger:=quSaleTID.AsInteger; //��� ������
    taTabAllID_PERSONALCLOSE.AsInteger:=Person.Id;
    taTabAll.Post;

    taTabAll.Active:=False;

    taCurMod.Active:=False;   //��������� �������� ���� �� ���� �� ������������
    taCurMod.ParamByName('IDT').AsInteger:=Tab.Id;
    taCurMod.ParamByName('STATION').AsInteger:=CommonSet.Station;
    taCurMod.Active:=True;

    taSpecAll.Active:=False;
    taSpecAll.ParamByName('TID').AsInteger:=IdH;
    taSpecAll.Active:=True;

    iNum:=1;

    taServP.Active:=False; //�������� ��� ������ �����
    taServP.CreateDataSet;

    quCurSpec.First;
    while not quCurSpec.Eof do
    begin      //��������� ������� ������ ���, ������ ��� ��� ����
      //�������� ��� ������ �����
      if quCurSpecIStatus.AsInteger=0 then
      begin
        taServP.Append;
        taServPName.AsString:=quCurSpecName.AsString;
        taServPCode.AsString:=quCurSpecSifr.AsString;
        taServPQuant.AsFloat:=quCurSpecQuantity.AsFloat;
        taServPStream.AsInteger:=quCurSpecStream.AsInteger;
        taServPiType.AsInteger:=0; //�����
        taServP.Post;
      end;

      taSpecAll.Append;
      taSpecAllID_TAB.AsInteger:=IdH;
      taSpecAllID.AsInteger:=iNum;
      taSpecAllID_PERSONAL.AsInteger:=Tab.Id_Personal;
      taSpecAllNUMTABLE.AsString:=Tab.NumTable;
      taSpecAllSIFR.AsInteger:=quCurSpecSifr.AsInteger;
      taSpecAllPRICE.AsFloat:=quCurSpecPrice.AsFloat;

      Case Operation of
    0,2: begin
           taSpecAllQUANTITY.AsFloat:=quCurSpecQuantity.AsFloat;
           taSpecAllSUMMA.AsFloat:=quCurSpecSumma.AsFloat;
         end;
      1: begin
           taSpecAllQUANTITY.AsFloat:=quCurSpecQuantity.AsFloat*(-1);
           taSpecAllSUMMA.AsFloat:=quCurSpecSumma.AsFloat*(-1);
         end;
      end;


      taSpecAllDISCOUNTPROC.AsFloat:=quCurSpecDProc.AsFloat;
      taSpecAllDISCOUNTSUM.AsFloat:=quCurSpecDSum.AsFloat;
      taSpecAllISTATUS.AsInteger:=1;
      taSpecAllITYPE.AsInteger:=0;
      taSpecAll.Post;

      inc(iNum);

      taCurMod.First;
      while not taCurMod.Eof do
      begin
        if taCurModID_POS.AsInteger=quCurSpecID.AsInteger then
        begin //��� �������

          if quCurSpecIStatus.AsInteger=0 then
          begin
            taServP.Append;
            taServPName.AsString:=quCurModAllNAME.AsString;
            taServPCode.AsString:='';
            taServPQuant.AsFloat:=0;
            taServPStream.AsInteger:=quCurSpecStream.AsInteger;
            taServPiType.AsInteger:=1; //�����������
            taServP.Post;
          end;

          taSpecAll.Append;
          taSpecAllID_TAB.AsInteger:=IdH;
          taSpecAllID.AsInteger:=iNum;
          taSpecAllID_PERSONAL.AsInteger:=Tab.Id_Personal;
          taSpecAllNUMTABLE.AsString:=Tab.NumTable;
          taSpecAllSIFR.AsInteger:=taCurModSifr.AsInteger;
          taSpecAllPRICE.AsFloat:=0;

          Case Operation of
        0,2: begin
               taSpecAllQUANTITY.AsFloat:=quCurSpecQuantity.AsFloat;
             end;
          1: begin
               taSpecAllQUANTITY.AsFloat:=quCurSpecQuantity.AsFloat*(-1);
             end;
          end;

          taSpecAllDISCOUNTPROC.AsFloat:=0;
          taSpecAllDISCOUNTSUM.AsFloat:=0;
          taSpecAllSUMMA.AsFloat:=0;
          taSpecAllISTATUS.AsInteger:=1;
          taSpecAllITYPE.AsInteger:=1;//�����������
          taSpecAll.Post;
          inc(iNum);
        end;
        taCurMod.Next;
      end;

      quCurSpec.Next;
    end;

    taCurMod.Active:=False;

    ViewSpec.EndUpdate;

    if Operation in [0,2] then PrintServCh('�����');// ���� ������� (1) - �� ������� ������ �� ����
    taServP.Active:=False;

  end;
end;

Procedure TFmMainFF.CreateViewPers;
begin
  GridSpec.Visible:=True;
  GridHK.Visible:=True;
  GridM.Visible:=True;
  cEdit1.Visible:=True;
  Panel2.Visible:=True;
  Panel5.Visible:=False; //������ �������
  Button5.Visible:=True;
  cxButton1.Visible:=True;
  cxButton2.Visible:=True;
  cxButton3.Visible:=True;
  cxButton4.Visible:=True;
  cxButton10.Visible:=True; //������ �/�
//  cxButton5.Visible:=True;  //���� �������� - ������� ��� ������� ������������ �������
  cxButton6.Visible:=True;
  cxButton11.Visible:=True;
  Label1.Caption:=Person.Name;
  Label7.Visible:=True;
  Button5.Enabled:=True;
  Button5.SetFocus;
  cxButton15.Visible:=True;  //���� ������
  cxButton21.Visible:=True; //  1/2

  Label11.Visible:=True;
  Edit1.Visible:=True;
  Label12.Visible:=True;
  Label13.Visible:=True;

  Label14.Visible:=True;
  Panel6.Visible:=True;
  Panel7.Visible:=True;

  //����������� ���������
  Event_RegEx(3,Nums.iCheckNum,0,0,'','',0,0,0);

  GridP.Visible:=True;
  cxButton24.Visible:=True;
  cxButton25.Visible:=True;
end;

Procedure TFmMainFF.DestrViewPers;
begin
  GridSpec.Visible:=False;
  GridHK.Visible:=False;
  GridM.Visible:=False;
  cEdit1.Visible:=False;
  Panel2.Visible:=False;
  Panel5.Visible:=False; //������ �������
  Button5.Visible:=False;
  cxButton1.Visible:=False;
  cxButton2.Visible:=False;
  cxButton3.Visible:=False;
  cxButton4.Visible:=False;
  cxButton5.Visible:=False;
  cxButton6.Visible:=False;
  cxButton11.Visible:=False;
  cxButton10.Visible:=False; //������ �/�
  Label7.Visible:=False;
  cxButton15.Visible:=False;  //���� ������
  cxButton21.Visible:=False;
  Label11.Visible:=False;
  Label12.Visible:=False;
  Edit1.Visible:=False;
  Label13.Visible:=False;

  Label14.Visible:=False;
  Panel6.Visible:=False;
  Panel7.Visible:=False;

  GridP.Visible:=False;
  cxButton24.Visible:=False;
  cxButton25.Visible:=False;

  Person.Id:=0;
  Person.Name:='';
end;

Procedure TFmMainFF.CreateHotList;
Var AColumn:TcxGridColumn;
    i,j:INteger;
    Str1:String;
begin
  try
    try
      for I := 0 to MaxCol-1 do
      begin
        AColumn := ViewHK.CreateColumn;
        AColumn.Caption := IntToStr(i);
        AColumn.Name := 'k'+IntToStr(i);
        AColumn.DataBinding.ValueType := 'String';
        AColumn.Styles.Content:=dmC.cxStyle24;
        AColumn.Styles.Content.Color:=$00AED2AE;
        AColumn.Width:=ColWidth; //���� 100
        AColumn.Styles.Content.Font.Size:=CommonSet.GridHKFontSize;
        if CommonSet.GridHKFontStyle=1 then AColumn.Styles.Content.Font.Style := [fsBold] else  AColumn.Styles.Content.Font.Style := [];
      end;
    except
    end;

    ViewHK.DataController.RecordCount := MaxLine;

    for I := 0 to MaxLine-1 do    //������
      for J := 0 to MaxCol-1 do  //�������
        ViewHK.DataController.SetValue(I, J, '');
    with dmC do
    begin
      if CommonSet.UseDayMenu=0 then
      begin
        try
          quMenuHK.Active:=False;
          quMenuHK.SelectSQL.Clear;
          quMenuHK.SelectSQL.Add('SELECT SIFR,NAME,PRICE,CODE,TREETYPE,LIMITPRICE,CATEG,');
          quMenuHK.SelectSQL.Add('PARENT,LINK,STREAM,LACK,DESIGNSIFR,ALTNAME,NALOG,BARCODE,');
          quMenuHK.SelectSQL.Add('IMAGE,CONSUMMA,MINREST,PRNREST,COOKTIME,DISPENSER,');
          quMenuHK.SelectSQL.Add('DISPKOEF,ACCESS,FLAGS,TARA,CNTPRICE,BACKBGR,');
          quMenuHK.SelectSQL.Add('    FONTBGR,IACTIVE,IEDIT');
          quMenuHK.SelectSQL.Add('FROM MENU');
          quMenuHK.SelectSQL.Add('WHERE IACTIVE=1 and SIFR=:ISIFR');

          quHotKey.Active:=False;
          quHotKey.ParamByName('CASHNUM').AsInteger:=CommonSet.Station;
          quHotKey.Active:=True;
          quHotKey.First;
          while not quHotKey.Eof do
          begin
            try
              Str(quHotKeyPRICE.AsFloat:6:2,StrWk);
              Str1:=quHotKeyNAME.AsString; while pos(#13,Str1)>0 do delete(Str1,pos(#13,Str1),1);

              StrWk:=Copy(Str1,1,CommonSet.GridHKLen)+#13+Copy(Str1,1+CommonSet.GridHKLen,CommonSet.GridHKLen)+#13+'����:'+StrWk+'�.'+#13+IntToStr(quHotKeySIFR.AsInteger);

              if (quHotKeyIROW.AsInteger<MaxLine) and (quHotKeyICOL.AsInteger<MaxCol) then
                ViewHK.DataController.SetValue(quHotKeyIROW.AsInteger,quHotKeyICOL.AsInteger,StrWk);
            except
            end;
            quHotKey.Next;
          end;


        finally
          quHotKey.Active:=False;
        end;
      end;
      if CommonSet.UseDayMenu=1 then
      begin
        try
          quMenuHK.Active:=False;
          quMenuHK.SelectSQL.Clear;

          quMenuHK.SelectSQL.Add('SELECT me.SIFR,me.NAME,me.CODE,md.PRICE,me.TREETYPE,me.LIMITPRICE,me.CATEG,');
          quMenuHK.SelectSQL.Add('    me.PARENT,me.LINK,me.STREAM,me.LACK,me.DESIGNSIFR,me.ALTNAME,me.NALOG,me.BARCODE,');
          quMenuHK.SelectSQL.Add('    me.IMAGE,me.CONSUMMA,me.MINREST,me.PRNREST,me.COOKTIME,me.DISPENSER,');
          quMenuHK.SelectSQL.Add('    me.DISPKOEF,me.ACCESS,me.FLAGS,me.TARA,me.CNTPRICE,me.BACKBGR,');
          quMenuHK.SelectSQL.Add('    me.FONTBGR,me.IACTIVE,me.IEDIT');
          quMenuHK.SelectSQL.Add('FROM MENU me');
          quMenuHK.SelectSQL.Add('left join MENUD md on md.SIFR=me.SIFR');
          quMenuHK.SelectSQL.Add('WHERE me.IACTIVE=1 and me.SIFR=:ISIFR');

          quHotKey1.Active:=False;
          quHotKey1.ParamByName('CASHNUM').AsInteger:=CommonSet.Station;
          quHotKey1.Active:=True;
          quHotKey1.First;
          while not quHotKey1.Eof do
          begin
            try
              Str(quHotKey1PRICE.AsFloat:6:2,StrWk);
              Str1:=quHotKey1NAME.AsString; while pos(#13,Str1)>0 do delete(Str1,pos(#13,Str1),1);

              StrWk:=Copy(Str1,1,CommonSet.GridHKLen)+#13+Copy(Str1,1+CommonSet.GridHKLen,CommonSet.GridHKLen)+#13+'����:'+StrWk+'�.'+#13+IntToStr(quHotKey1SIFR.AsInteger);

              if (quHotKey1IROW.AsInteger<MaxLine) and (quHotKey1ICOL.AsInteger<MaxCol) then
                ViewHK.DataController.SetValue(quHotKey1IROW.AsInteger,quHotKey1ICOL.AsInteger,StrWk);
            except
            end;
            quHotKey1.Next;
          end;
        finally
          quHotKey1.Active:=False;
        end;
      end;
    end;
  except
  end;
end;


Procedure TfmMainFF.RecalcSum;
Var rSum:Real;
    Str1,Str2:String;
begin
  with dmC do
  begin
    //��������� ����� �����
{    ViewSpec.BeginUpdate;
    rSum:=0;
    quCurSpec.First;
    while not quCurSpec.Eof do
    begin
      rSum:=rSum+quCurSpecSUMMA.AsFloat;
      quCurSpec.Next;
    end;
    try
      quCurSpec.Prior;
      quCurSpec.Next;
    except
    end;
    ViewSpec.EndUpdate;
    cEdit1.EditValue:=rSum;}

    if CommonSet.CashNum<0 then Tab.Id:=CommonSet.CashNum else Tab.Id:=CommonSet.CashNum*(-1);

    quCurSum.Active:=False;
    quCurSum.ParamByName('IDT').AsInteger:=Tab.Id;
    quCurSum.ParamByName('STATION').AsInteger:=CommonSet.Station;
    quCurSum.Active:=True;

    rSum:=quCurSumRSUM.AsFloat;
    cEdit1.EditValue:=rSum;

    //��
    if rSum<>0 then
    begin
      Str(quCurSpecSUMMA.AsFloat:7:2,Str1);
      StrWk:=Copy(quCurSpecNAME.AsString,1,12);
      While Length(StrWk)<12 do StrWk:=StrWk+' ';
      Str1:=StrWk+' '+Str1;
      Str(rSum:9:2,Str2); While Length(Str2)<15 do Str2:=' '+Str2;
      Str2:='�����'+Str2;
//      WriteDP(Str1,Str2);
      ToCustomDisp(Str1,Str2);
    end;

  end;
end;

Procedure WriteStatus;
begin
  with fmMainFF do
  begin

    label8.caption:='����� � '+IntToStr(CommonSet.CashNum);
    Label2.Caption:='����: '+Nums.sDate;
    Label4.Caption:='����� �'+IntToStr(Nums.ZNum)+'  ��� � '+IntToStr(Nums.iCheckNum);
    Label3.Caption:='������ ���: ������ ('+IntToStr(Nums.iRet)+')';
    Label9.Caption:='���.�����  '+Nums.SerNum;
    Label6.Caption:='�������� (����) '+IntToStr(Nums.ZYet);

    Case Operation of
    0,2: begin
         Label5.Caption:='�������� - ������� ��������.';
       end;
    1: begin
         Label5.Caption:='�������� - �������.';
       end;
    end;


    if Nums.iRet=0 then Label3.Caption:='������ ���: '+Nums.sRet;
    if (Nums.iRet=2)or(Nums.iRet=22) then Label3.Caption:='������ ���: ���������� ������� �����';


    if CommonSet.CashNum=0 then label8.caption:='������� ���������.'
    else
    begin
      if CommonSet.CashNum>0 then label8.caption:='����� � '+intToStr(CommonSet.CashNum);
      if CommonSet.CashNum<0 then
      begin
        label8.caption:='������� �������.';
        Label2.Caption:='����: '+FormatDateTime('dd.mm.yyyy',date);
        Label4.Caption:='����� �'+IntToStr(CommonSet.CashZ)+'  ��� � '+IntToStr(CommonSet.CashChNum);
      end;
    end;
  end;
end;


procedure TfmMainFF.FormCanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
  if fmMainFF.Tag=17 then NewHeight:=1024;
  if fmMainFF.Tag=15 then NewHeight:=768;
  if fmMainFF.Tag=10 then NewHeight:=600;
  Left:=0;
  Top:=0;
end;

procedure TfmMainFF.FormCreate(Sender: TObject);
begin
  CurDir := ExtractFilePath(ParamStr(0));
  Label8.Caption:='';
  Label9.Caption:='';
  Label2.Caption:='';
  Label3.Caption:='';
  Label4.Caption:='';
  Label7.Caption:=''; //������ ������
  cEdit1.Tag:=0; //������� �������� ������
  Label11.Caption:='';
  Label12.Caption:=sVer;


  Operation:=0; //�������� �������
  Label5.Caption:='�������� - ������� ��������.';

  prClearBuf;
  ReadIni;

//��������� �������� Nums

  CommonSet.PortCash:='COM'+its(CommonSet.CashPort);

  //���� �� ��������

  Nums.ZNum:=CommonSet.CashZ;
  Nums.SerNum:='0';
  Nums.RegNum:='0';
  Nums.CheckNum:=INtToStr(CommonSet.CashChNum);
  Nums.iCheckNum:=CommonSet.CashChNum;
  Nums.iRet:=0;
  Nums.sRet:='���';
  Nums.sDate:=FormatDateTime(sFormatDate,Date);

  bFF:=True; //��� ���� ���

  TimerDelayPrintQ.Interval:=CommonSet.iStartDelaySec;
  TimerDelayPrintQ.Enabled:=True;

  if CommonSet.UseRazd=0 then Panel8.Visible:=False else Panel8.Visible:=True;
  Panel7.Width:=CommonSet.PanelHKW;
  Panel7.Height:=CommonSet.PanelHKH;

  DestrViewPers;

  if (CommonSet.CashNum>0)and(CommonSet.SpecChar=1) then
  begin
    Nums.ZNum:=CommonSet.CashZ;
    Nums.CheckNum:=INtToStr(CommonSet.CashChNum);
    Nums.iCheckNum:=CommonSet.CashChNum;
    Nums.sRet:='���';
    Nums.sDate:=FormatDateTime(sFormatDate,Date);
  end;


  WriteStatus;

  StrPre:='';
  Person.Id:=0;
  Person.Name:='';

  TableImage:=TBitMap.Create;
  TableImage.LoadFromFile(CurDir+'Pict1.bmp');
  FGridBrush := TBrush.Create;

  MaxCol:=CommonSet.GridHKColQuant; //15-������
  MaxLine:=CommonSet.GridHKRowQuant;  //15-������
  ColWidth:=CommonSet.GridHKColW;
  ViewHK.OptionsView.DataRowHeight:=CommonSet.GridHKRowH;

{
  if CommonSet.GridHKType=1 then
  begin
    MaxCol:= 12; //17-������
    MaxLine:= 8;  //17-������
  end;

  if CommonSet.GridHKType=2 then
  begin
    MaxCol:= 10; //17-������
    MaxLine:= 8;  //17-������
  end;
}


  Prizma:=False;
  if CommonSet.Prizma=1 then
    if OpenDrv then
    begin
      Label13.Caption:=' ������ ��.';
      Prizma:=True;
    end else
    begin
      Label13.Caption:=' ������  ���.';
      Prizma:=False;
    end;

end;

procedure TfmMainFF.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  SetWindowPos(FindWindow('Shell_TrayWnd', nil), 0, 0, Screen.Height-24, Screen.Width, 24, SWP_SHOWWINDOW);

  if CommonSet.CashNum>0 then CashClose;

  TableImage.Free;
  FGridBrush.Free;
  dmC.quPers.Active:=False;
  dmC.CasherRnDb.Connected:=False;

  dmC1.devCas.Close;
end;

procedure TfmMainFF.FormShow(Sender: TObject);
begin
//  Left:=0; Top:=0;
//  Width:=1024;
//  Height:=768;
//  Windowstate:=wsMaximized;

  NumPacket:=33;

  with dmC do
  begin
    try
      prWriteLog('-- ��������� ���� ������ - '+DBName);
      prWriteLog('-- 1');
      CasherRnDb.Connected:=False;
      CasherRnDb.DatabaseName:=DBName;
      CasherRnDb.Connected:=True;

      prWriteLog('-- 2');

      taPersonal.Active:=True;
      taRClassif.Active:=True;
      quFuncList.Active:=True;
      taFuncList.Active:=True;

      prUpd;
      prWriteLog('-- 3');

    //�������� ����� ����� ��� ����������� �������
      if CommonSet.CashNum>0 then
      begin
        try
          if CommonSet.CashPort>0 then
          begin
            StrWk:='COM'+IntToStr(CommonSet.CashPort);
            if CashOpen(PChar(StrWk)) then   //
            begin
              //
            end
            else
            begin
              CommonSet.CashNum:=0;
              showmessage('������ �������� �����.');
            end;
          end
          else
          begin
            CommonSet.CashNum:=0;
          end;
        except
          CommonSet.CashNum:=0;
        end;
      end;

      if CommonSet.TypeFis<>'sp802' then //��� 802 ����� �� ���������
        if (CommonSet.CashNum>0)and(CommonSet.SpecChar=0) then //��������� ���� ������
        begin
      //    Label9.Caption:='���.�����  '+Nums.SerNum;

          Nums.iRet:=InspectSt(Nums.sRet);
          if (Nums.iRet=0) or (Nums.iRet=2)or(Nums.iRet=22)or(Nums.iRet=21) then Nums.bOpen:=True;
          if Nums.iRet=21 then
          begin
            CheckCancel;
            Nums.iRet:=InspectSt(Nums.sRet);
          end;
          GetSerial;
          CashDate(Nums.sDate);
          GetNums; //������
          GetRes; //�������
        end;

      WriteStatus;

      if CommonSet.UseDayMenu=1 then //������� ������� ���� ��� ������ �������
      begin
        prCreateDMenu.ParamByName('ISTATION').AsInteger:=CommonSet.Station;
        prCreateDMenu.ExecProc;
      end;

      ViewM.BeginUpdate;
      dsMenu.DataSet:=Nil;

      prFormMenu(0);

      dsMenu.DataSet:=quMenu;
      ViewM.EndUpdate;

      Person.Id:=1001;
      Person.Name:='�������������';

      Tab.Id_Personal:=Person.Id;     //���� �������������� ��������
      Tab.Name:='';
      Tab.OpenTime:=Now;
      Tab.NumTable:='FF';
      Tab.Quests:=1;
      Tab.iStatus:=0; //������� �� �����
      Tab.DBar:='';
      if CommonSet.CashNum<0 then Tab.Id:=CommonSet.CashNum else Tab.Id:=CommonSet.CashNum*(-1);
      Tab.Summa:=0;
      Tab.DPercent:=0;
      Check.Max:=0;

      Label1.Caption:=Person.Name;

      prWriteLog('-- 5');

      prClearCur.ParamByName('ID_TAB').AsInteger:=Tab.Id;
      prClearCur.ParamByName('ID_STATION').AsInteger:=CommonSet.Station;
      prClearCur.ExecProc;

      prWriteLog('-- 6');

      quCurSpecMod.Active:=False;
      quCurSpecMod.ParamByName('IDT').AsInteger:=Tab.Id;
      quCurSpecMod.ParamByName('STATION').AsInteger:=CommonSet.Station;
      quCurSpecMod.Active:=True;

      prWriteLog('-- 7');

      quCurSpec.Active:=False;
      quCurSpec.ParamByName('IDT').AsInteger:=Tab.Id;
      quCurSpec.ParamByName('STATION').AsInteger:=CommonSet.Station;
      quCurSpec.Active:=True;
      quCurSpec.Last;

      prWriteLog('-- 8');

      while not quCurSpec.Bof do
      begin
        ViewSpec.Controller.FocusedRow.Expand(True);
        quCurSpec.Prior;
        delay(10);
      end;

      prWriteLog('-- 9');

      if (CommonSet.CashNum<0)or(CommonSet.SpecChar=1) then cxButton14.Visible:=True;

      prWriteLog('-- 10');

      CreateHotList;

      prWriteLog('-- 11');

      //�� ������ �����������
//      fmSber:=tFmSber.Create(Application);

//      prWriteLog('-- 12');
      if CommonSet.BNManual=2 then
      begin //��� ����
        if IsOLEObjectInstalled('SBRFSRV.Server') then
        begin
          Drv := CreateOleObject('SBRFSRV.Server');
          Drv.Clear;
        end;
      end;

      prWriteLog('-- 13');

      quSaleT.Active:=True;
      quSaleT.First;
      if quSaleT.RecordCount>0 then cxButton15.Caption:=Copy(quSaleTNAMECS.AsString,1,15);

      prWriteLog('-- 14');

    //��
//      WriteDP(' ������� �� �������.','                    ');
      ToCustomDisp(' ������� �� �������.','                    ');

      prWriteLog('-- 15');

      if Prizma then
      begin
        prWriteLog('!!StartPRGM;');
        Event_RegEx(121,0,0,0,'','',0,0,0);
      end;

      prWriteLog('-- ��� ��.');
      prWriteLog('');

    except
      ShowMessage('������ �������� ���� - '+DBName);
      Close;
    end;
  end;
  with dmC1 do
  begin
    try
      CasherRnDb1.Connected:=False;
      CasherRnDb1.DatabaseName:=DBName;
      CasherRnDb1.Connected:=True;

    except
      ShowMessage('������ �������� ���� 2 - '+DBName);
      Close;
    end;

    bScale:=False;

    if pos('COM',CommonSet.PortVesCAS_ER)=1 then
    begin //��������� �������� � ����� ����
      try
        devCas.DeviceName:=CommonSet.PortVesCAS_ER;
        devCas.Open;
        bScale:=True;
      except
        bScale:=False;
      end;

    end;
  end;
  with dmPC do
  begin
    try
      PCDb.Connected:=False;
      PCDb.DatabaseName:=DBNamePC;
      PCDb.Connected:=True;

    except
      ShowMessage('������ �������� ���� 3');
      Close;
    end;
  end;

  Timer1.Enabled:=True;
end;

procedure TfmMainFF.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled:=False;
  Person.Id:=0;
  Person.Name:='';
  if CommonSet.MReader>0 then
  begin
    fmPreFF.ShowModal;
  end else
  begin
    fmPre_Shape.ShowModal;
  end;
  TimerTr.Interval:=CommonSet.TransDelSec;
  TimerTr.Enabled:=True;
end;

procedure TfmMainFF.ViewMCustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);
var
  ARec: TRect;
  ATextToDraw: string;
  sType:String;
begin
  if (AViewInfo is TcxGridCardRowDataViewInfo) then
    ATextToDraw := AViewInfo.GridRecord.DisplayTexts[AViewInfo.Item.Index]
  else
    ATextToDraw := VarAsType(AViewInfo.Item.Caption, varString);

  ARec := AViewInfo.Bounds;

  sType := VarAsType(AViewInfo.GridRecord.DisplayTexts[1], varString);
  if sType='T'  then
  begin
    ACanvas.Canvas.Brush.Color:=$00CDAD98;
  end
  else
  begin
//    ACanvas.Canvas.Brush.Bitmap := ABitmap;
  end;
  ACanvas.Canvas.FillRect(ARec);
  SetBkMode(ACanvas.Canvas.Handle, TRANSPARENT);
  ACanvas.DrawText(ATextToDraw, AViewInfo.Bounds, 0, True);
  ADone := True; // }
end;

procedure TfmMainFF.acOpenSelMenuExecute(Sender: TObject);
Var iParent:Integer;
    rQ:Real;
    cSum,cSumD:Real;
    iC,iR:INteger;
    Str1:String;
begin
  with dmC do
  begin
    if quMenuTREETYPE.AsString='T' then
    begin
      iParent:=quMenuSIFR.AsInteger;
      ViewM.BeginUpdate;
      dsMenu.DataSet:=Nil;

      prFormMenu(iParent);

      dsMenu.DataSet:=quMenu;
      ViewM.EndUpdate;
    end
    else //��������� ������� � �����
    begin
      if GridHK.Tag>0 then //��������� ������� � ������� �������
      begin
        iC:=ViewHK.Controller.FocusedColumnIndex;
        iR:=ViewHK.Controller.FocusedRowIndex;
        if (iC>=0) and (iR>=0) then
        begin
          if Sifr>0 then
          begin
            quMenu.Locate('SIFR',sifr,[]);

            try
              quHotKey.Active:=False;
              quHotKey.ParamByName('CASHNUM').AsInteger:=CommonSet.Station;
              quHotKey.Active:=True;

              quHotKey.First;
              while not quHotKey.Eof do
              begin
                if (quHotKeyCASHNUM.AsInteger=CommonSet.Station)and
                   (quHotKeyIROW.AsInteger=iR)and
                   (quHotKeyICOL.AsInteger=iC) then
                begin
                  quHotKey.Delete; break;
                end else quHotKey.Next;
              end;
              quHotKey.Append;
              quHotKeyCASHNUM.AsInteger:=CommonSet.Station;
              quHotKeyIROW.AsInteger:=iR;
              quHotKeyICOL.AsInteger:=iC;
              quHotKeySIFR.AsInteger:=quMenuSIFR.AsInteger;
              quHotKey.Post;

              Str(quMenuPRICE.AsFloat:6:2,StrWk);
              Str1:=quMenuNAME.AsString; while pos(#13,Str1)>0 do Str1[pos(#13,Str1)]:=' ';

              if MaxCol>=10 then //17
                StrWk:=Copy(Str1,1,14)+#13+Copy(Str1,15,14)+#13+'����:'+StrWk+'�.'+#13+IntToStr(quHotKeySIFR.AsInteger)
              else //15 ���� ��������������������
                StrWk:=Copy(Str1,1,14)+#13+Copy(Str1,15,14)+#13+'����:'+StrWk+'�.'+#13+IntToStr(quHotKeySIFR.AsInteger);

              quHotKey.Active:=False;

              ViewHK.DataController.SetValue(iR,iC,StrWk);
            except
            end;
          end;
        end;
      end else //� �����
      begin
        quCurSpec.Last;
        if Sifr>0 then quMenu.Locate('SIFR',sifr,[]);
        if quMenuSIFR.AsInteger<>Sifr then exit;
        if (quCurSpecSifr.AsInteger<>quMenuSIFR.AsInteger)or(quMenuDESIGNSIFR.AsInteger>0) then //����� ������� ��� ����������� ���-��
        begin
          rQ:=1;
          if quMenuDESIGNSIFR.AsInteger>0 then
          begin
            fmCalc.CalcEdit1.EditValue:=1;
            fmCalc.ShowModal;
            if (fmCalc.ModalResult=mrOk)and(fmCalc.CalcEdit1.EditValue>0.00001)  then
            begin
              rQ:=RoundEx(fmCalc.CalcEdit1.EditValue*1000)/1000;
            end;
          end;

          CalcDiscontN(quMenuSIFR.AsInteger,Check.Max+1,quMenuPARENT.AsInteger,quMenuSTREAM.AsInteger,quMenuPRICE.AsFloat,rQ,rQ*quMenuPRICE.AsFloat,Tab.DBar,Now,Check.DProc,Check.DSum);

          cSum:=RoundVal(quMenuPRICE.AsFloat*rQ-Check.DSum);
          cSumD:=RoundVal(quMenuPRICE.AsFloat*rQ)-cSum;

          if CommonSet.CashNum<0 then Tab.Id:=CommonSet.CashNum else Tab.Id:=CommonSet.CashNum*(-1);

          quCurSpec.Append;
          quCurSpecSTATION.AsInteger:=CommonSet.Station;
          quCurSpecID_TAB.AsInteger:=Tab.Id;
          quCurSpecId_Personal.AsInteger:=Person.Id;
          quCurSpecNumTable.AsString:=IntToStr(Tab.Id);
          quCurSpecId.AsInteger:=Check.Max+1;
          quCurSpecSifr.AsInteger:=quMenuSIFR.AsInteger;
          quCurSpecPrice.AsFloat:=quMenuPRICE.AsFloat;
          quCurSpecQuantity.AsFloat:=rQ;
          quCurSpecSumma.AsFloat:=cSum;
          quCurSpecDProc.AsFloat:=Check.DProc;
          quCurSpecDSum.AsFloat:=cSumD;
          quCurSpecIStatus.AsInteger:=0;
          quCurSpecName.AsString:=quMenuNAME.AsString;
          quCurSpecCode.AsString:=quMenuCODE.AsString;
          quCurSpecLinkM.AsInteger:=quMenuLINK.AsInteger;
          if quMenuLIMITPRICE.AsFloat>0 then
          quCurSpecLimitM.AsInteger:=RoundEx(quMenuLIMITPRICE.AsFloat)
          else quCurSpecLimitM.AsInteger:=99;
          quCurSpecStream.AsInteger:=quMenuSTREAM.AsInteger;
          quCurSpec.Post;
          inc(Check.Max);

          if Prizma then
          begin
            prWriteLog('!!AddPos;'+its(Nums.iCheckNum)+';'+its(Check.Max)+';'+its(quCurSpecSIFR.AsInteger)+';'+quCurSpecNAME.AsString+';'+fts(quCurSpecQUANTITY.AsFloat)+';'+fts(quCurSpecSUMMA.AsFloat)+';'+fts(quCurSpecDSUM.AsFloat));
            Event_RegEx(6,Nums.iCheckNum,Check.Max,quCurSpecSIFR.AsInteger,'',quCurSpecNAME.AsString,quCurSpecPRICE.AsFloat,quCurSpecQUANTITY.AsFloat,quCurSpecSUMMA.AsFloat);
          end;


          if quMenuLINK.AsInteger>0 then
          begin //������������
            acModify.Execute;
          end;
        end else
        begin
          rQ:=quCurSpecQuantity.AsFloat;

          CalcDiscontN(quMenuSIFR.AsInteger,Check.Max+1,quMenuPARENT.AsInteger,quMenuSTREAM.AsInteger,quMenuPRICE.AsFloat,rQ+1,(rQ+1)*quMenuPRICE.AsFloat,Tab.DBar,Now,Check.DProc,Check.DSum);

          cSum:=RoundVal(quMenuPRICE.AsFloat*(rQ+1)-Check.DSum);
          cSumD:=RoundVal(quMenuPRICE.AsFloat*(rQ+1))-cSum;

          quCurSpec.Edit;
          quCurSpecQuantity.AsFloat:=rQ+1;
          quCurSpecSumma.AsFloat:=cSum;
          quCurSpecDProc.AsFloat:=Check.DProc;
          quCurSpecDSum.AsFloat:=cSumD;
          quCurSpec.Post;
        end;
        RecalcSum;
      end;
    end;
  end;
end;

procedure TfmMainFF.acUpUpExecute(Sender: TObject);
begin
  with dmC do
  begin
    ViewM.BeginUpdate;
    dsMenu.DataSet:=Nil;

    prFormMenu(0);

    dsMenu.DataSet:=quMenu;
    ViewM.EndUpdate;

    GridM.SetFocus;
  end;
end;  

procedure TfmMainFF.acUpExecute(Sender: TObject);
Var iParent:INteger;
begin
  with dmC do
  begin
    ViewM.BeginUpdate;
    if CommonSet.UseDayMenu=0 then
    begin
      taMenu.Active:=True;
      if taMenu.Locate('SIFR',quMenuPARENT.AsInteger,[]) and (quMenuPARENT.AsInteger<>0) then
      begin
        ViewM.BeginUpdate;
        dsMenu.DataSet:=Nil;

        prFormMenu(taMenuPARENT.AsInteger);

        dsMenu.DataSet:=quMenu;
        ViewM.EndUpdate;

        quMenu.First;
      end;
      taMenu.Active:=False;
    end;
    if CommonSet.UseDayMenu=1 then
    begin
      quMenuDRec.Active:=False;
      quMenuDRec.ParamByName('IGR').AsInteger:=quMenuPARENT.AsInteger;
      quMenuDRec.Active:=True;
      quMenuDRec.First;
      if quMenuDRec.RecordCount>0 then  iParent:=quMenuDRecPARENT.AsInteger else iParent:=0;

      ViewM.BeginUpdate;
      dsMenu.DataSet:=Nil;

      prFormMenu(iParent);

      dsMenu.DataSet:=quMenu;
      ViewM.EndUpdate;

      quMenuDRec.Active:=False;
    end;
    ViewM.EndUpdate;
    GridM.SetFocus;
  end;
end;

procedure TfmMainFF.ViewMCellClick(Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
Var StrWk:String;
begin
  TimerSdTo0.Enabled:=False;
  if Button5.Enabled=False then exit;
  AHandled:=True;
  if bPrintCheck then exit;
  if cEdit1.Tag>0 then exit;
  bAddPosFromMenu:=True; //��� ���� ����� ������ � ��������� ����� ������������
  bAddPosFromSpec:=False;
  Sifr:=-1;
  StrWk:= ACellViewInfo.Value;
  if Pos('|',StrWk)>0 then
  begin
    Delete(StrWk,1,Pos('|',StrWk));
    Sifr:=StrToIntDef(StrWk,-1);
  end;
  acOpenSelMenu.Execute;
end;

procedure TfmMainFF.acModifyExecute(Sender: TObject);
begin
  //������������
  with dmC do
  begin
    quModif.Active:=False;
    quModif.ParamByName('PARENT').AsInteger:=quCurSpecLINKM.AsInteger;
    quModif.Active:=True;
    fmModifFF:=TFmModifFF.Create(Application);
    //������� �����
    MaxMod:=quCurSpecLimitM.AsInteger;
    CountMod:=0;  //���� ��� ������� ��� ���������

    fmModifFF.Label1.Caption:='�������� � ���������� '+INtToStr(MaxMod-CountMod)+' ������������.';

    fmModifFF.ShowModal;
    fmModifFF.Release;
    quModif.Active:=False;
  end;
end;

procedure TfmMainFF.Button5Click(Sender: TObject);
Var
    strWk,StrWk1,StrWk2,Str1,Str2:String;
    iRet,IdH,IdC,i:Integer;
    bCheckOk:Boolean;
    rDiscont,rDProc:Real;
    iSumPos,iSumTotal,iSumDisc,iSumIt:INteger;
    TabCh:TTab;
    BnStr:String;
    bNotErr:Boolean;
    rSumWithDisc:Real;
    iAvans,iSumA:INteger;
    iCurCheck:Integer;
    StrP:String;
    rSum:Real;
    kDelay:Integer;
    iPC,iOp:Integer;

 procedure prClearCheck;
 begin
   fmAttention.Label1.Caption:='���� ��������� ���� � ��. ��������� ������������ ����!';
   fmAttention.Label2.Caption:='';
   fmAttention.Label3.Caption:='';

   fmAttention.ShowModal;
   Nums.iRet:=InspectSt(Nums.sRet);
   prWriteLog('~~AttentionShow;'+'���� ��������� ���� � ��. ��������� ������������ ����!');

   fmAttention.Label2.Caption:='����� ���������� ������ ������� "�����"';
   fmAttention.Label3.Caption:='��������� ������������ ��������� ��������.';

   prWriteLog('~~CheckCancel;');
   CheckCancel;
    //��������� �����
   bCheckOk:=False;
 end;

begin
  if CommonSet.UseQuest=1 then
  begin
    if cxSpinEdit1.Value=0 then
    begin
      showmessage('������� ���-�� ������.');
      exit;
    end;
  end;

  Button5.Enabled:=False;
  delay(10);
  if not CanDo('prPrintCheck') then begin Button5.Enabled:=True; exit; end;

  TestPrint:=1;
  While PrintQu and (TestPrint<=MaxDelayPrint) do
  begin
//        showmessage('������� �����, ��������� �������.');
    prWriteLog('������ ���� �� ����� �������.');
    delay(1000);
    inc(TestPrint);
  end;

  with dmC do
  begin
    quCurSpec.First;
    if quCurSpec.RecordCount=0 then begin Button5.Enabled:=True; exit; end;

    if More24H(iRet) then
    begin
      fmAttention.Label1.Caption:='������ ����� 24 �����. ���������� ������� �����.';
      fmAttention.ShowModal;
      Button5.Enabled:=True;
      Exit;   //������� , �� ���� �� ������ �� ���������� ������ �� ����
    end;

    TabCh.Id_Personal:=Person.Id;
    TabCh.Name:=Person.Name;
    TabCh.DBar:=Tab.DBar;
    TabCh.Summa:=0;
    TabCh.Id:=Tab.Id;

    //���� ��������� �������� �� ������ �������� iAvans
    // 1 ����� ������� ������ ����� ����  ---- ������������ ������
    // 2 ����� ������ = ����� ����  ----- TabCh.Summa=0
    // 3 ����� ������ < ����� ����  ----- �� ���������� ����� ���� �������� ���

    quCheck.Active:=False;
    quCheck.ParamByName('IDTAB').Value:=TabCh.Id;
    quCheck.Active:=True;

    iSumA:=0;
    iAvans:=0;

    ViewSpec.BeginUpdate;
    quCurSpec.First;
    while not quCurSpec.Eof do
    begin
      TabCh.Summa:=TabCh.Summa+quCurSpecSUMMA.AsFloat;
      if quCurSpecPRICE.AsFloat<0 then iSumA:=RoundEx(quCurSpecSUMMA.AsFloat*100);
      quCurSpec.Next;
    end;
    quCurSpec.First;
    ViewSpec.EndUpdate;

    if Prizma then
    begin
      prWriteLog('!!Itog');
      Event_RegEx(26,Nums.iCheckNum,quCurSpecID.AsInteger,quCurSpecSIFR.AsInteger,'',quCurSpecNAME.AsString,quCurSpecPRICE.AsFloat,quCurSpecQUANTITY.AsFloat,quCurSpecSUMMA.AsFloat);
    end;

    if iSumA<0 then //������ �� ���� � ������
    begin
      if TabCh.Summa>0 then iAvans:=3; // 3 ����� ������ < ����� ����  ----- �� ���������� ����� ���� �������� ���
      if TabCh.Summa=0 then iAvans:=2; // 2 ����� ������ = ����� ����
      if TabCh.Summa<0 then iAvans:=1; // 1 ����� ������� ������ ����� ����  ---- ������������ ������
      if not CanDo('prAvansCheck') then
      begin
        fmAttention.Label1.Caption:='��� ���� ��� ������ � �������� !!!.';
        prWriteLog('~~AttentionShow;'+'��� ���� ��� ������ � �������� !!!.');
        fmAttention.ShowModal;
        Button5.Enabled:=True; //������ ������� ��������
        exit;
      end;
    end;

    if (Operation=1) and (iAvans>1) then
    begin
      fmAttention.Label1.Caption:='������ �������� ��� �������� (������)!!!.';
      prWriteLog('~~AttentionShow;'+'������ �������� ��� �������� (������)!!!.');
      fmAttention.ShowModal;
      Button5.Enabled:=True; //������ ������� ��������
      Exit;   //������� , �� ���� �� ������ �� ���������� ������ �� ����
    end;


    if TabCh.Summa<0.01 then
    begin
      if iAvans<2 then  //��������, ��� �������� ������
      begin
        fmAttention.Label1.Caption:='��� � ������� � ������������� ������ �������� ������ !!!.';
        prWriteLog('~~AttentionShow;'+'��� � ������� � ������������� ������ �������� ������ !!!.');
        fmAttention.ShowModal;
        Button5.Enabled:=True; //������ ������� ��������
        Exit;   //������� , �� ���� �� ������ �� ���������� ������ �� ����
      end;
    end;


    if iAvans=2 then // 2 ����� ������ = ����� ����  ������� ������ ������������ ��������
    begin
      PosCh.Name:='';
      PosCh.Code:='';
      PosCh.Price:=0; //� ��������
      PosCh.Count:=0; //� �������
      PosCh.Sum:=0;

      try
        if CommonSet.PrePrintPort='fisprim' then
        begin
          OpenNFDoc;

          SelectF(13);PrintNFStr(' '+CommonSet.DepartName); PrintNFStr('');
          SelectF(14); PrintNFStr('       ������');
          SelectF(13); PrintNFStr('');
          PrintNFStr('��������: '+TabCh.Name);
          PrintNFStr('����: '+TabCh.NumTable);
          SelectF(13);PrintNFStr('������: '+IntToStr(TabCh.Quests));
          PrintNFStr('������: '+FormatDateTime('dd.mm.yyyy hh:nn:ss',TabCh.OpenTime));
          SelectF(13);StrWk:='                                          ';
          PrintNFStr(StrWk);
          SelectF(15);StrWk:=' ��������          ���-��   ����   ����� ';
          PrintNFStr(StrWk);
          SelectF(13);StrWk:='                                          ';
          PrintNFStr(StrWk);
          SelectF(15); PrintNFStr(' ');PrintNFStr(' ');

          iSumA:=iSumA*(-1);
          Str((TabCh.Summa+iSumA/100):10:2,StrWk);
          StrWk:='����� ����� �����:    '+StrWk+' ���';
          PrintNFStr(StrWk); PrintNFStr(' ');PrintNFStr(' ');

          Str((iSumA/100):10:2,StrWk);
          StrWk:='������ ����� �� �����:'+StrWk+' ���';
          PrintNFStr(StrWk); PrintNFStr(' ');PrintNFStr(' ');

          StrWk:='����� �� �����       :      0,00 ���';
          PrintNFStr(StrWk); PrintNFStr(' ');

          SelectF(13);StrWk:='                                          ';
          PrintNFStr(StrWk);
          SelectF(11);
          PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');
          CloseNFDoc;
          CutDoc;
        end;

      finally

       //�������� � ������ ����� ��� ���������� ��������� - ��� �� �������

        inc(CommonSet.CashChNum); //����� �������� 1-�� - ������������ �������� - �����
        WriteCheckNum;

        prSaveSpec(TabCh.Summa);  // ������������ ��������, ������ cashsail

        // quCashSail �� ���� �.�. �����=0

        //������� �� ������� ���� ����� ������

        if cEdit1.Tag>0 then
        begin
          //������ �����
//       ������ ����
          if abs(TabCh.Summa-Tab.Summa)<0.01 then
          begin
            quDelTab.Active:=False;
            quDelTab.ParamByName('Id').AsInteger:=cEdit1.Tag;

            trDel.StartTransaction;
            quDelTab.Active:=True;
            trDel.Commit;
          end;
          cEdit1.Tag:=0;

          cxButton3.Enabled:=True;
          cxButton4.Enabled:=True;
          cxButton6.Enabled:=True;
          cxButton11.Enabled:=True;
          cxButton21.Enabled:=True;


          Tab.Id_Personal:=Person.Id;
          Tab.Name:='';
          Tab.NumTable:='FF';
          Tab.DBar:='';
          Tab.DBar1:='';
          Tab.Id:=0;
          Tab.Summa:=0;
          Tab.Quests:=1;
        end;

        //������ ������� ��� � ������������
        prClearCur.ParamByName('ID_TAB').AsInteger:=Tab.Id;
        prClearCur.ParamByName('ID_STATION').AsInteger:=CommonSet.Station;
        prClearCur.ExecProc;

        //��������� �� ������
        quCurSpecMod.Active:=False;
        quCurSpecMod.ParamByName('IDT').AsInteger:=Tab.Id;
        quCurSpecMod.ParamByName('STATION').AsInteger:=CommonSet.Station;
        quCurSpecMod.Active:=True;

        quCurSpec.Active:=False;
        quCurSpec.ParamByName('IDT').AsInteger:=Tab.Id;
        quCurSpec.ParamByName('STATION').AsInteger:=CommonSet.Station;
        quCurSpec.Active:=True;
        quCurSpec.Last;

        //�������������� �� ���������
        ViewSpecDPROC.Visible:=False;
        ViewSpecDSum.Visible:=False;
        DiscountBar:='';
        Label7.Caption:='';
        Tab.DBar:='';
        Tab.DPercent:=0;
        Tab.DName:='';
        Operation:=0;

        WriteStatus;
        bPrintCheck:=False; //������� ������ ����
      end;

      Button5.Enabled:=True; //������ ������� ��������
      ToCustomDisp(' ������� �� �������.','                    ');
      exit;
    end;

    //������ �������� ������  iAvans:=3; ����� ������ < ����� ����

    with fmCash do
    begin
      TabCh.Summa:=RoundSpec(TabCh.Summa,TabCh.rDiscDop);

      Caption:='������';
      Label5.Caption:='���. '+TabCh.Name;
      Label6.Caption:='����� - '+Floattostr(TabCh.Summa)+' �.';
      cEdit1.EditValue:=TabCh.Summa;
      cEdit2.EditValue:=TabCh.Summa;
      cEdit3.EditValue:=0;
      cEdit2.SelectAll;

      if (CommonSet.CashNum>0)and(CommonSet.SpecChar=0) then
      begin
        iRet:=InspectSt(StrWk);
        if iRet<>0 then
        begin
          Label7.Caption:='������ ���: ������ ('+IntToStr(iRet)+') '+StrWk;
          fmMainFF.Label3.Caption:='������ ���: ������ ('+IntToStr(iRet)+') '+StrWk;
          while TestStatus('InspectSt',sMessage)=False do
          begin
            fmAttention.Label1.Caption:=sMessage;
            fmAttention.ShowModal;
            Nums.iRet:=InspectSt(Nums.sRet);
            prWriteLog('~~AttentionShow;'+sMessage+';'+'InspectSt(Nums.sRet)='+IntToStr(Nums.iRet));
          end;
        end;
        //���� ����� �� ���� ��� � �����

        Label7.Caption:='������ ���: ��� ��.';
        fmMainFF.Label3.Caption:='������ ���: ��� ��.';
      end;
    end;
    iCashType:=0; //��� �� ���������
    prClearAnsw;
    fmCash.ShowModal;
    if fmCash.ModalResult=mrOk then
    begin
      prWriteLog('!!CashOk;Sum-'+fts(fmCash.cEdit1.Value)+';Bn-'+fts(fmCash.cEdit5.Value)+';Dt-'+fts(fmCash.cEdit6.Value)+';N-'+fts(fmCash.cEdit2.Value)+';Sd-'+fts(fmCash.cEdit3.Value)+';');

      //����� ������������� ������
      fmMainFF.cEdit1.EditValue:=fmCash.CEdit3.EditValue; //����� � �������� ����

      bCheckOk:=False;
      bPrintCheck:=False;
      bPrintCheck:=True; //������� ������ ����
      PrintFCheck:=True; //��� ��������
      delay(10);

      iPC:=0; //�� ��������� ��������� ���� ���

      //��������� ��������� �����
      if fmCash.CEdit6.value>0 then
      begin
        prFindPCardBalans(SalePC.BarCode,SalePC.rBalans,SalePC.rBalansDay,SalePC.DLimit,SalePC.CliName,SalePC.CliType);
        if SalePC.rBalansDay>=fmCash.CEdit6.value then
        begin
          if fmCash.CEdit6.value=fmCash.CEdit1.value then iPc:=2 else iPc:=1;  //���� ������ ������������, ���� ���������� , �� ��������
         // iPc:=2  ������������ �������
          prWriteLog('!!CashOk;Dt type-'+its(iPc)+';');
        end;
      end;

      //��
      Str(fmCash.CEdit1.EditValue:9:2,Str1); While Length(Str1)<15 do Str1:=' '+Str1;
      Str1:='�����'+Str1;
      Str(fmCash.CEdit3.EditValue:9:2,Str2); While Length(Str2)<15 do Str2:=' '+Str2;
      Str2:='�����'+Str2;
      ToCustomDisp(Str1,Str2);

      //-----------------------------

      //������� ���
//      if CommonSet.CashNum>0 then  //���������� �����
      if (CommonSet.CashNum>0)and(CommonSet.SpecChar=0)and(iPC<2) then
      begin
        bCheckOk:=True;

       //�������� ������� �������� �����
        CashDriver;

        //����� ������� ����� �������������� ����� �������� ����. ����� ���� ���� ����� Nums.iCheckNum=0 ��� ��� �����
        //����� ������������ �� ������ ����������� ����� �� 1-��

        GetNums;

        Case Operation of
        0: begin
             prWriteLog('!!CheckStart;'+IntToStr((Nums.iCheckNum+1))+';'+IntToStr(Person.Id)+';'+Person.Name+';');
             CheckStart;
             while TestStatus('CheckStart',sMessage)=False do
             begin
               fmAttention.Label1.Caption:=sMessage;
               fmAttention.ShowModal;
               Nums.iRet:=InspectSt(Nums.sRet);
               prWriteLog('~~AttentionShow;'+sMessage+';'+'InspectSt(Nums.sRet)='+IntToStr(Nums.iRet));
             end;
           end;
        1: begin
             prWriteLog('!!CheckRetStart;'+IntToStr((Nums.iCheckNum+1))+';'+IntToStr(Person.Id)+';'+Person.Name+';');
             CheckRetStart;
             while TestStatus('CheckRetStart',sMessage)=False do
             begin
               fmAttention.Label1.Caption:=sMessage;
               fmAttention.ShowModal;
               Nums.iRet:=InspectSt(Nums.sRet);
               prWriteLog('~~AttentionShow;'+sMessage+';'+'InspectSt(Nums.sRet)='+IntToStr(Nums.iRet));
             end;
           end;
        2: begin
           end;
        end;

        //�������

        rDiscont:=0;
        rSumWithDisc:=0;

        if (iAvans=0)and(iPC=0) then //���� ����������� ������ ��� ������ � ��������� ����
        begin
          ViewSpec.BeginUpdate;

          taCurMod.Active:=False;   //��������� �������� ���� �� ���� �� ������������
          taCurMod.ParamByName('IDT').AsInteger:=Tab.Id;
          taCurMod.ParamByName('STATION').AsInteger:=CommonSet.Station;
          taCurMod.Active:=True;

          quCurSpec.First;
          while not quCurSpec.Eof do
          begin      //��������� �������
            PosCh.Name:=quCurSpecNAME.AsString;
            PosCh.Code:=quCurSpecSIFR.AsString;
            PosCh.AddName:='';
            PosCh.Price:=RoundEx(quCurSpecPRICE.AsFloat*100);
            PosCh.Count:=RoundEx(quCurSpecQUANTITY.AsFloat*1000);

            rDiscont:=rDiscont+quCurSpecDSUM.AsFloat;
            rSumWithDisc:=rSumWithDisc+PosCh.Price*PosCh.Count;
          //������������

            taCurMod.First;
            while not taCurMod.Eof do
            begin
              if taCurModID_POS.AsInteger=quCurSpecID.AsInteger then
              begin //��� �������
                PosCh.AddName:=PosCh.AddName+'|   '+taCurModNAME.AsString;
              end;
              taCurMod.Next;
            end;
            if PosCh.AddName>'' then  delete(PosCh.AddName,1,1);//������ ������ '|

            prWriteLog('~!CheckAddPos;'+IntToStr((Nums.iCheckNum+1))+';'+PosCh.Code+';'+FloatToStr(quCurSpecQUANTITY.AsFloat)+';'+FloatToStr(quCurSpecPRICE.AsFloat)+';'+FloatToStr(quCurSpecDSUM.AsFloat)+';');

            if (PosCh.Price*PosCh.Count/1000)>=1 then CheckAddPos(iSumPos); //��������� ������� ���� ������ >1 ���
            while TestStatus('CheckAddPos',sMessage)=False do
            begin
              fmAttention.Label1.Caption:=sMessage;
              fmAttention.ShowModal;
              Nums.iRet:=InspectSt(Nums.sRet);
              prWriteLog('~~AttentionShow;'+sMessage+';'+'InspectSt(Nums.sRet)='+IntToStr(Nums.iRet));
            end;

            quCurSpec.Next;
          end;

          taCurMod.Active:=False;
          ViewSpec.EndUpdate;

        end else //���� ���� ������ ��� ��������� ����� �� ��� ����� �������
        begin
          if iAvans<>0 then
          begin
            rDiscont:=0;
            rSumWithDisc:=RoundEx(TabCh.Summa*100000);

            PosCh.Name:='� ������ �� �����';
            PosCh.Code:='';
            iSumA:=iSumA*(-1);
            Str((TabCh.Summa+iSumA/100):10:2,StrWk);
            PosCh.AddName:='|����� ����� �����:    '+StrWk+'|������ ����� �� �����:';
            Str((iSumA/100):10:2,StrWk);
            PosCh.AddName:=PosCh.AddName+StrWk+'| ';
            PosCh.Price:=RoundEx(TabCh.Summa*100); //� ��������
            PosCh.Count:=1000; //� �������
            PosCh.Sum:=RoundEx(TabCh.Summa*100)/100;

            prWriteLog('~!CheckAddPos;'+IntToStr((Nums.iCheckNum+1))+';'+PosCh.Code+';'+FloatToStr(PosCh.Count)+';'+FloatToStr(PosCh.Price)+';;');
            CheckAddPos(iSumPos);
            while TestStatus('CheckAddPos',sMessage)=False do
            begin
              fmAttention.Label1.Caption:=sMessage;
              prWriteLog('~~AttentionShow;'+sMessage);
              fmAttention.ShowModal;
              Nums.iRet:=InspectSt(Nums.sRet);
            end;
          end;

          if iPC<>0 then  //�������� ��������� ������
          begin

            PosCh.Name:='� ������ �� �����';
            PosCh.Code:='';

            Str(rv(fmCash.cEdit1.value):10:2,StrWk);
            PosCh.AddName:='|����� ����� �����:    '+StrWk+'|';
            Str(rv(fmCash.cEdit6.value):10:2,StrWk);
            PosCh.AddName:=PosCh.AddName+'�������� '+Copy(SalePC.CliName,1,20)+':'+StrWk+'| ';
            Str(rv(SalePC.rBalans-fmCash.cEdit6.value):10:2,StrWk);
            PosCh.AddName:=PosCh.AddName+'������ �� �����:'+ StrWk+'| ';
            Str(rv(SalePC.rBalansDay-fmCash.cEdit6.value):10:2,StrWk);
            PosCh.AddName:=PosCh.AddName+'������� �������� ������:'+ StrWk+'| ';

            PosCh.Price:=RoundEx(rv(fmCash.cEdit1.value-fmCash.cEdit6.value)*100); //� ��������
            PosCh.Count:=1000; //� �������
            PosCh.Sum:=RoundEx(rv(fmCash.cEdit1.value-fmCash.cEdit6.value)*100)/100; //�� ������������ � CheckAddPos(iSumPos);


            prWriteLog('~!CheckAddPos;'+IntToStr((Nums.iCheckNum+1))+';'+PosCh.Code+';'+FloatToStr(PosCh.Count)+';'+FloatToStr(PosCh.Price)+';;'+PosCh.AddName);
            CheckAddPos(iSumPos);
            while TestStatus('CheckAddPos',sMessage)=False do
            begin
              fmAttention.Label1.Caption:=sMessage;
              prWriteLog('~~AttentionShow;'+sMessage);
              fmAttention.ShowModal;
              Nums.iRet:=InspectSt(Nums.sRet);
            end;

            rSum:=PosCh.Sum;
            rDiscont:=0;
            rSumWithDisc:=PosCh.Sum*100000;
          end;
        end;


        //������������ ���������� - ������ ����, ������, ������;
        rSumWithDisc:=rSumWithDisc/100000; //��� ���� �.�. ��������� ��� � integer  �.�. ��������� � ����� � �� ������ (��� �����)

        CheckTotal(iSumTotal);

        if iSumTotal=0 then iSumTotal:=RoundEx(rSumWithDisc*100);

        prWriteLog('!!AfterCheckTotal;'+IntToStr((Nums.iCheckNum+1))+';'+Inttostr(iSumTotal)+';'+FloatToStr(rSumWithDisc));

        while TestStatus('CheckTotal',sMessage)=False do
        begin
          fmAttention.Label1.Caption:=sMessage;
          fmAttention.ShowModal;
          Nums.iRet:=InspectSt(Nums.sRet);
          prWriteLog('~~AttentionShow;'+sMessage+';'+'InspectSt(Nums.sRet)='+IntToStr(Nums.iRet));
        end;

        //�������� iSumTotal
        if (abs(rSumWithDisc-(iSumTotal/100))>0.01)and(bCheckOk=True) then
        begin //����������� �������� ����� �� ������������ � � ����������� - ����
          //��������� ��� ��������� -> �������� ������
          prClearCheck;
        end;

        if (abs(rDiscont)>0.001)and(bCheckOk=True)  then
        begin
          iSumDisc:=RoundEx(rDiscont*100);
          CheckDiscount('������',rDiscont,iSumIt);
          iSumTotal:=iSumIt;
          prWriteLog('!!AfterCheckDisct;'+IntToStr((Nums.iCheckNum+1))+';'+FloatToStr(rDiscont)+';'+IntToStr(iSumDisc)+';'+IntToStr(iSumIt)+';');
          while TestStatus('CheckDiscount',sMessage)=False do
          begin
            fmAttention.Label1.Caption:=sMessage;
            fmAttention.ShowModal;
            Nums.iRet:=InspectSt(Nums.sRet);
            prWriteLog('~~AttentionShow;'+sMessage+';'+'InspectSt(Nums.sRet)='+IntToStr(Nums.iRet));
          end;
        end;

        if TabCh.rDiscDop>0.001 then
        begin
          iSumDisc:=RoundEx(TabCh.rDiscDop*100);
          CheckDiscount('���.������',TabCh.rDiscDop,iSumIt);
          iSumTotal:=iSumIt;
          prWriteLog('!!AfterCheckDopDisc;'+IntToStr((Nums.iCheckNum+1))+';'+FloatToStr(TabCh.rDiscDop)+';'+IntToStr(iSumDisc)+';'+IntToStr(iSumIt)+';');
          while TestStatus('CheckDiscount',sMessage)=False do
          begin
            fmAttention.Label1.Caption:=sMessage;
            fmAttention.ShowModal;
            Nums.iRet:=InspectSt(Nums.sRet);
            prWriteLog('~~AttentionShow;'+sMessage+';'+'InspectSt(Nums.sRet)='+IntToStr(Nums.iRet));
          end;
        end;

        if CommonSet.TypeFis='sp101' then
        begin
          CheckTotal(iSumTotal);
          prWriteLog('!!AfterCheckTotal2;'+IntToStr((Nums.iCheckNum+1))+';'+Inttostr(iSumTotal)+';'+FloatToStr(rSumWithDisc-rDiscont-TabCh.rDiscDop));
        end;

        //��������� ������� � ������������ �������.
        BnStr:='';
        if (iCashType=1)and(bCheckOk=True) then
        begin
          if CommonSet.BNManual=0 then
          begin
            BnStr:='|';
            //���� � �����
            BnStr:=BnStr+Copy(Answ.sDate,1,2)+'/'+Copy(Answ.sDate,3,2)+'/'+Copy(Answ.sDate,5,2)+'  ';
            BnStr:=BnStr+Copy(Answ.sTime,1,2)+':'+Copy(Answ.sTime,3,2)+'||';
          //��������
            if Operation=0 then BnStr:=BnStr+'��������: �������||'
            else BnStr:=BnStr+'��������: �������||';
            //�����
            StrWk1:=Copy(bnBar,pos('=',bnBar)+1,4); //YYMM
            if Answ.bnBar[1]='4' then BnStr:=BnStr+'Visa';
            if Answ.bnBar[1]='5' then BnStr:=BnStr+'Master Card';
            if Answ.bnBar[1]='6' then BnStr:=BnStr+'Union';

            StrWk2:=Copy(Answ.bnBar,1,16); //�������� �����
            if length(StrWk2)>=12 then for i:=5 to 12 do StrWk2[i]:='X';

             BnStr:=BnStr+' '+StrWk2+' '+Copy(StrWk1,3,2)+'/'+Copy(StrWk1,1,2)+'||';
            //��� �����������
            BnStr:=BnStr+'��� ����������� '+Answ.sAuthCode+'||';
            //�����
            Str((iSumTotal/100):9:2,StrWk1);
            BnStr:=BnStr+'�����: '+StrWk1+' ���.||';
            BnStr:=BnStr+'������� _______________________________||';
          end;
        end;

        if bCheckOk=True then
        begin
          if fmCash.cEdit5.EditValue>0 then
          begin
            if CheckRasch(1,RoundEx(fmCash.cEdit5.EditValue*100),'',BnStr,iSumIt)=False then bCheckOk:=False; //������
          end;
          if fmCash.cEdit2.EditValue>0 then
          begin
            if CheckRasch(0,RoundEx(fmCash.cEdit2.EditValue*100),'',BnStr,iSumIt)=False then bCheckOk:=False; //���
          end;

          prWriteLog('!!AfterCheckRasch;'+IntToStr((Nums.iCheckNum+1))+';'+FloatToStr(fmCash.cEdit2.EditValue)+';'+INtToStr(iSumIt)+';');
          while TestStatus('CheckRasch',sMessage)=False do
          begin
            fmAttention.Label1.Caption:=sMessage;
            fmAttention.ShowModal;
            Nums.iRet:=InspectSt(Nums.sRet);
            prWriteLog('~~AttentionShow;'+sMessage+';'+'InspectSt(Nums.sRet)='+IntToStr(Nums.iRet));
          end;
        end;

        if bCheckOk=True then
        begin
          prWriteLog('!!CheckClose;'+IntToStr((Nums.iCheckNum+1))+';');
          CheckClose;
//          CutDoc;
        end;

        if (BnStr>'')and(bCheckOk=True) then //���� ��� �������� �� �������
        begin
          bNotErr:=True;
          try
            OpenNFDoc;
            SelectF(10);
            if bNotErr then bNotErr:=PrintNFStr('����� ��������. ��� �'+IntToStr(Nums.iCheckNum+1));
            StrWk:=CommonSet.DepartName;
            while length(strwk)<35 do StrWk:=' '+StrWk;
            if bNotErr then bNotErr:=PrintNFStr('�����'+StrWk);
            if bNotErr then bNotErr:=PrintNFStr('      ');
            if bNotErr then bNotErr:=PrintNFStr('�����: '+INtToStr(Nums.ZNum)+'            ������: '+IntToStr(Person.Id));
            if bNotErr then bNotErr:=PrintNFStr('');

            i:=0;
            Delete(BnStr,1,1); //������� |
            while (length(BnBar)>0)and(bNotErr=True)and(i<7) do
            begin
              StrWk:=Copy(BnStr,1,Pos('||',BnStr)-1);
              if pos('�������',StrWk)>0 then if bNotErr then bNotErr:=PrintNFStr('');
              if bNotErr then bNotErr:=PrintNFStr(StrWk);
              Delete(BnStr,1,Pos('||',BnStr)+1);
              inc(i); //�� ������ ������, �������� ����� �� �����
              if pos('�������',StrWk)>0 then Break; //��� ���� �����, ���-�� � ������� �� ��.
            end;
//            if bNotErr then PrintNFStr(BnStr); //������ �������� ������
          finally
            CloseNFDoc;
            CutDoc1;
          end;
        end;

        //�������� ������ ����� ������
        iRet:=InspectSt(StrWk);
        iCurCheck:=Nums.iCheckNum+1;  //� ������ ������ �  ���������� ���� Nums.iCheckNum=0  iCurCheck=1

        if (iRet=0)and(bCheckOk=True) then
        begin
          Label3.Caption:='������ ���: ��� ��.';
          if CashDate(StrWk) then Label2.Caption:='�������� ����: '+StrWk;

          if GetNums then Label4.Caption:='��� � '+Nums.CheckNum;
          //��� ����������� �������� CommonSet.CashChNum - ����� �������� ��� � ���
          WriteCheckNum;
        end
        else
        begin
          Label3.Caption:='������ ���: ������ ('+IntToStr(iRet)+')';
        end;

        if CommonSet.ControlNums=1 then
        begin
          if (Nums.iCheckNum <> iCurCheck) then
          begin //���������� ������������������ �����
                  //������� ���������� ������������
            prWriteLog('!!ErrorNumCheck;new-'+IntToStr(Nums.iCheckNum+1)+';old-'+IntToStr(iCurCheck)+';');
            prClearCheck;
          end;
        end;

      end else
      begin
        if (CommonSet.CashNum<0)or(CommonSet.SpecChar=1)or(iPC=2) then //������ ������������ �����  ��� ������������ ���
        begin
          prWriteLog('!!CashOk;Dt type-'+its(iPc)+';');
          if pos('fisshtrih',CommonSet.PrePrintPort)>0 then
          begin
            TestPrint:=1;
            While PrintQu and (TestPrint<=MaxDelayPrint) do
            begin
//                showmessage('������� �����, ��������� �������.');
              prWriteLog('������������� ������ ���� � �������.');
              delay(1000);
              inc(TestPrint);
            end;
            try
              PrintFCheck:=True; //��� ���� � ������� ������� ��������� ������

              CashDriver;

              PrintNFStr(' '+CommonSet.DepartName);

              if CommonSet.PreLine1>'' then
              begin
                if fDifStr(1,CommonSet.PreLine1)>'' then PrintNFStr(fDifStr(1,CommonSet.PreLine1));
                if fDifStr(2,CommonSet.PreLine1)>'' then PrintNFStr(fDifStr(2,CommonSet.PreLine1));
              end;

              PrintNFStr(' ');
              PrintNFStr('       ��� �'+IntToStr(CommonSet.CashChNum));
              PrintNFStr(' ');
//              PrintNFStr('��������: '+TabCh.Name);
//              PrintNFStr('����: '+TabCh.NumTable);
              PrintNFStr('������: '+FormatDateTime('dd.mm.yyyy hh:nn:ss',now));
              StrWk:='-----------------------------------';
              PrintNFStr(StrWk);
              StrWk:=' ��������     ���-��  ����   ����� ';
              PrintNFStr(StrWk);
              StrWk:='-----------------------------------';
              PrintNFStr(StrWk);

              rDiscont:=0;

              ViewSpec.BeginUpdate;

              taCurMod.Active:=False;   //��������� �������� ���� �� ���� �� ������������
              taCurMod.ParamByName('IDT').AsInteger:=Tab.Id;
              taCurMod.ParamByName('STATION').AsInteger:=CommonSet.Station;
              taCurMod.Active:=True;

              quCurSpec.First;
              while not quCurSpec.Eof do
              begin      //��������� �������

                PosCh.Name:=quCurSpecNAME.AsString;
                PosCh.Code:=quCurSpecSIFR.AsString;
                PosCh.AddName:='';
                PosCh.Price:=RoundEx(quCurSpecPRICE.AsFloat*100);
                PosCh.Count:=RoundEx(quCurSpecQUANTITY.AsFloat*1000);

                StrWk:= Copy(PosCh.Name,1,36);
                while Length(StrWk)<36 do StrWk:=StrWk+' ';
                PrintNFStr(StrWk); //������� ����� �������� - �������� �������

                rDiscont:=rDiscont+quCurSpecDSUM.AsFloat;
//                rSumWithDisc:=rSumWithDisc+PosCh.Price*PosCh.Count;

                Str(quCurSpecQUANTITY.AsFloat:7:3,StrWk1);
                StrWk:='          '+StrWk1;
                Str(quCurSpecPRICE.AsFloat:7:2,StrWk1);
                StrWk:=StrWk+' '+StrWk1;
                Str(quCurSpecSUMMA.AsFloat:8:2,StrWk1);
                StrWk:=StrWk+' '+StrWk1+'���';
                PrintNFStr(StrWk);

                taCurMod.First;
                while not taCurMod.Eof do
                begin
                  if taCurModID_POS.AsInteger=quCurSpecID.AsInteger then
                  begin //��� �������
                    PrintNFStr('   '+taCurModNAME.AsString);
                  end;
                  taCurMod.Next;
                end;

                quCurSpec.Next;

              end;

              ViewSpec.EndUpdate;

{              quCheck.First;
              while not quCheck.Eof do
              begin      //��������� �������

                PosCh.Name:=quCheckNAME.AsString;
                PosCh.Code:=quCheckCODE.AsString;
                PosCh.AddName:='';

                StrWk:= Copy(PosCh.Name,1,36);
                while Length(StrWk)<36 do StrWk:=StrWk+' ';
                PrintNFStr(StrWk); //������� ����� �������� - �������� �������

                rDiscont:=rDiscont+quCheckDISCOUNTSUM.AsFloat;

                Str(quCheckQUANTITY.AsFloat:5:1,StrWk1);
                StrWk:='          '+StrWk1;
                Str(quCheckPRICE.AsFloat:7:2,StrWk1);
                StrWk:=StrWk+' '+StrWk1;
                Str(quCheckSUMMA.AsFloat:8:2,StrWk1);
                StrWk:=StrWk+' '+StrWk1+'���';
                PrintNFStr(StrWk);

                quCheck.Next;
                if not quCheck.Eof then
                begin
                  while quCheckITYPE.AsInteger=1 do
                  begin  //������������
                    if quCheck.Eof then break;
                    if quCheckSifr.AsInteger>0 then
                    if taModif.Locate('SIFR',quCheckSifr.AsInteger,[]) then
                    begin
                      StrWk:='   '+Copy(taModifNAME.AsString,1,29);
                      PrintNFStr(StrWk);
                    end;
                    quCheck.Next;
                  end;
                end;
              end;
              }


              StrWk:='-----------------------------------';
              PrintNFStr(StrWk);
              Str(TabCh.Summa:8:2,StrWk1);
              StrWk:=' �����                '+StrWk1+' ���';
              PrintNFStr(StrWk);


              Case Operation of
              0:begin
                  PrintNFStr('�������                                 ');
                  if fmCash.cEdit5.EditValue>0 then
                  begin
                    PrintNFStr(prDefFormatStr(32,'��������� �����:',fmCash.cEdit5.EditValue));
                  end;
                  if fmCash.cEdit6.EditValue>0 then
                  begin
                    PrintNFStr(prDefFormatStr(32,'��������� �����:',fmCash.cEdit6.EditValue));
                  end;
                  if fmCash.cEdit2.EditValue>0 then
                  begin
                    PrintNFStr(prDefFormatStr(32,'��������:',fmCash.cEdit2.EditValue));
                  end;
                  if (fmCash.cEdit5.EditValue+fmCash.cEdit2.EditValue)>TabCh.Summa then
                  begin
                    PrintNFStr(prDefFormatStr(32,'�����:',(fmCash.cEdit5.EditValue+fmCash.cEdit6.EditValue+fmCash.cEdit2.EditValue)-TabCh.Summa));
                  end;
                end;
              1:begin
                  PrintNFStr('�������                                 ');
                  if fmCash.cEdit5.EditValue>0 then
                  begin
                    PrintNFStr(prDefFormatStr(32,'��������� �����:',fmCash.cEdit5.EditValue));
                  end;
                  if fmCash.cEdit6.EditValue>0 then
                  begin
                    PrintNFStr(prDefFormatStr(32,'��������� �����:',fmCash.cEdit6.EditValue));
                  end;
                  if (rSum-fmCash.cEdit5.EditValue)>0 then
                  begin
                    PrintNFStr(prDefFormatStr(32,'��������:',(rSum-fmCash.cEdit5.EditValue-fmCash.cEdit6.EditValue)));
                  end;
                end;
              end;

              if rDiscont>0.02 then
              begin
                PrintNFStr('');
                rDProc:=RoundEx((rDiscont/(TabCh.Summa+rDiscont)*100)*100)/100;
                Str(rDProc:5:2,StrWk1);
                Str(rDiscont:8:2,StrWk);
                StrWk:=' � �.�. ������ - '+StrWk1+'%  '+StrWk+'�.';
                PrintNFStr(StrWk);
              end;

              if iPC=2 then
              begin
                PrintNFStr(' ');
                Str(rv(fmCash.cEdit6.value):10:2,StrWk);
                PrintNFStr('�������� '+Copy(SalePC.CliName,1,20)+':'+StrWk);
                Str(rv(SalePC.rBalans-fmCash.cEdit6.value):10:2,StrWk);
                PrintNFStr('������ �� �����:'+ StrWk);
                Str(rv(SalePC.rBalansDay-fmCash.cEdit6.value):10:2,StrWk);
                PrintNFStr('������� �������� ������:'+ StrWk);
                PrintNFStr(' ');
              end;

              PrintNFStr('������: '+Person.Name);
              PrintNFStr('�������: _____________________');


              if CommonSet.LastLine1>'' then
              begin
                PrintNFStr('');
                if fDifStr(1,CommonSet.LastLine1)>'' then PrintNFStr(fDifStr(1,CommonSet.LastLine1));
                if fDifStr(2,CommonSet.LastLine1)>'' then PrintNFStr(fDifStr(2,CommonSet.LastLine1));
              end;

              PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');
              CutDoc;
            finally
              bCheckOk:=True;
              PrintFCheck:=False; //��� ���� � ������� ������� ��������� ������
            end;
          end;
          if pos('fisprim',CommonSet.PrePrintPort)>0 then
          begin
            TestPrint:=1;
            While PrintQu and (TestPrint<=MaxDelayPrint) do
            begin
//                showmessage('������� �����, ��������� �������.');
              prWriteLog('������������� ������ ���� � �������.');
              delay(1000);
              inc(TestPrint);
            end;
            try
              PrintFCheck:=True; //��� ���� � ������� ������� ��������� ������

              CashDriver;

              OpenNFDoc;

              PrintNFStr(' '+CommonSet.DepartName);

              if CommonSet.PreLine1>'' then
              begin
                if fDifStr(1,CommonSet.PreLine1)>'' then PrintNFStr(fDifStr(1,CommonSet.PreLine1));
                if fDifStr(2,CommonSet.PreLine1)>'' then PrintNFStr(fDifStr(2,CommonSet.PreLine1));
              end;

              PrintNFStr('����� � '+IntToStr(CommonSet.CashNum));
              PrintNFStr(' ');
              SelectF(14);
              PrintNFStr('   �������� ��� �'+IntToStr(CommonSet.CashChNum));
              SelectF(10);
              PrintNFStr(' ');
//              PrintNFStr('��������: '+TabCh.Name);
//              PrintNFStr('����: '+TabCh.NumTable);
              PrintNFStr('������: '+FormatDateTime('dd.mm.yyyy hh:nn:ss',now));
              StrWk:='----------------------------------------';
              PrintNFStr(StrWk);
              StrWk:=' ��������     ���-��   ����    �����  ';
              PrintNFStr(StrWk);
              StrWk:='----------------------------------------';
              PrintNFStr(StrWk);

              rDiscont:=0;

              ViewSpec.BeginUpdate;

              taCurMod.Active:=False;   //��������� �������� ���� �� ���� �� ������������
              taCurMod.ParamByName('IDT').AsInteger:=Tab.Id;
              taCurMod.ParamByName('STATION').AsInteger:=CommonSet.Station;
              taCurMod.Active:=True;

              quCurSpec.First;
              while not quCurSpec.Eof do
              begin      //��������� �������

                PosCh.Name:=quCurSpecNAME.AsString;
                PosCh.Code:=quCurSpecSIFR.AsString;
                PosCh.AddName:='';
                PosCh.Price:=RoundEx(quCurSpecPRICE.AsFloat*100);
                PosCh.Count:=RoundEx(quCurSpecQUANTITY.AsFloat*1000);

                StrWk:= Copy(PosCh.Name,1,36);
                while Length(StrWk)<36 do StrWk:=StrWk+' ';
                PrintNFStr(StrWk); //������� ����� �������� - �������� �������

                rDiscont:=rDiscont+quCurSpecDSUM.AsFloat;
//                rSumWithDisc:=rSumWithDisc+PosCh.Price*PosCh.Count;

                Str(quCurSpecQUANTITY.AsFloat:7:3,StrWk1);
                StrWk:='          '+StrWk1;
                Str(quCurSpecPRICE.AsFloat:7:2,StrWk1);
                StrWk:=StrWk+' '+StrWk1;
                Str(quCurSpecSUMMA.AsFloat:8:2,StrWk1);
                StrWk:=StrWk+' '+StrWk1+'���';
                PrintNFStr(StrWk);

                taCurMod.First;
                while not taCurMod.Eof do
                begin
                  if taCurModID_POS.AsInteger=quCurSpecID.AsInteger then
                  begin //��� �������
                    PrintNFStr('   '+taCurModNAME.AsString);
                  end;
                  taCurMod.Next;
                end;

                quCurSpec.Next;

              end;

              ViewSpec.EndUpdate;

              StrWk:='----------------------------------------';
              PrintNFStr(StrWk);
              Str(TabCh.Summa:8:2,StrWk1);
              StrWk:=' �����                '+StrWk1+' ���';
              PrintNFStr(StrWk);

              Case Operation of
              0:begin
                  PrintNFStr('�������                                 ');
                  if fmCash.cEdit5.EditValue>0 then
                  begin
                    PrintNFStr(prDefFormatStr(32,'��������� �����:',fmCash.cEdit5.EditValue));
                  end;
                  if fmCash.cEdit6.EditValue>0 then
                  begin
                    PrintNFStr(prDefFormatStr(32,'��������� �����:',fmCash.cEdit6.EditValue));
                  end;
                  if fmCash.cEdit2.EditValue>0 then
                  begin
                    PrintNFStr(prDefFormatStr(32,'��������:',fmCash.cEdit2.EditValue));
                  end;
                  if (fmCash.cEdit5.EditValue+fmCash.cEdit2.EditValue)>TabCh.Summa then
                  begin
                    PrintNFStr(prDefFormatStr(32,'�����:',(fmCash.cEdit5.EditValue+fmCash.cEdit6.EditValue+fmCash.cEdit2.EditValue)-TabCh.Summa));
                  end;
                end;
              1:begin
                  PrintNFStr('�������                                 ');
                  if fmCash.cEdit5.EditValue>0 then
                  begin
                    PrintNFStr(prDefFormatStr(32,'��������� �����:',fmCash.cEdit5.EditValue));
                  end;
                  if fmCash.cEdit6.EditValue>0 then
                  begin
                    PrintNFStr(prDefFormatStr(32,'��������� �����:',fmCash.cEdit6.EditValue));
                  end;
                  if (rSum-fmCash.cEdit5.EditValue)>0 then
                  begin
                    PrintNFStr(prDefFormatStr(32,'��������:',(rSum-fmCash.cEdit5.EditValue-fmCash.cEdit6.EditValue)));
                  end;
                end;
              end;

              if rDiscont>0.02 then
              begin
                PrintNFStr('');
                rDProc:=RoundEx((rDiscont/(TabCh.Summa+rDiscont)*100)*100)/100;
                Str(rDProc:5:2,StrWk1);
                Str(rDiscont:8:2,StrWk);
                StrWk:=' � �.�. ������ - '+StrWk1+'%  '+StrWk+'�.';
                PrintNFStr(StrWk);
              end;

              if iPC=2 then
              begin
                PrintNFStr(' ');
                Str(rv(fmCash.cEdit6.value):10:2,StrWk);
                PrintNFStr('�������� '+Copy(SalePC.CliName,1,20)+':'+StrWk);
                Str(rv(SalePC.rBalans-fmCash.cEdit6.value):10:2,StrWk);
                PrintNFStr('������ �� �����:'+ StrWk);
                Str(rv(SalePC.rBalansDay-fmCash.cEdit6.value):10:2,StrWk);
                PrintNFStr('������� �������� ������:'+ StrWk);
                PrintNFStr(' ');
              end;

              PrintNFStr('������: '+Person.Name);
              PrintNFStr('�������: _____________________');

              if CommonSet.LastLine1>'' then
              begin
                PrintNFStr('');
                if fDifStr(1,CommonSet.LastLine1)>'' then PrintNFStr(fDifStr(1,CommonSet.LastLine1));
                if fDifStr(2,CommonSet.LastLine1)>'' then PrintNFStr(fDifStr(2,CommonSet.LastLine1));
              end;

              PrintNFStr(' '); PrintNFStr(' '); PrintNFStr(' ');  PrintNFStr(' ');PrintNFStr(' '); PrintNFStr(' ');PrintNFStr(' ');

              CloseNFDoc;
              CutDoc;
            finally
              bCheckOk:=True;
              PrintFCheck:=False; //��� ���� � ������� ������� ��������� ������
            end;
          end;

          if (pos('COM',CommonSet.PrePrintPort)>0)or(pos('LPT',CommonSet.PrePrintPort)>0) then //������ ������������ ����� �� ��������� �������
          begin
            try
              OpenMoneyBox;

              prDevOpen(CommonSet.PrePrintPort,0);

              BufPr.iC:=0;
              StrP:=CommonSet.PrePrintPort;
              SelFont(10);

//              prSetFont(StrP,13,0);
              PrintStrDev(StrP,' '+CommonSet.DepartName); PrintStrDev(StrP,'');

//              PrintStrDev(StrP,' '+CommonSet.DepartName);

              if CommonSet.PreLine1>'' then
              begin
                PrintStrDev(StrP,' ');
                if fDifStr(1,CommonSet.PreLine1)>'' then
                begin
                  PrintStrDev(StrP,fDifStr(1,CommonSet.PreLine1));
                  PrintStrDev(StrP,' ');
                end;
                if fDifStr(2,CommonSet.PreLine1)>'' then
                begin
                  PrintStrDev(StrP,fDifStr(2,CommonSet.PreLine1));
                  PrintStrDev(StrP,' ');
                end;
              end;

              PrintStrDev(StrP,'����� � '+IntToStr(CommonSet.CashNum));
              PrintStrDev(StrP,' ');
//              prSetFont(StrP,14,0); //������ �������
//              prSetFont(StrP,10,0);
              PrintStrDev(StrP,'   �������� ��� �'+IntToStr(CommonSet.CashChNum));
//              prSetFont(StrP,10,0);
              PrintStrDev(StrP,' ');
//              PrintStrDev(StrP,'��������: '+TabCh.Name);
//              PrintStrDev(StrP,'����: '+TabCh.NumTable);
              PrintStrDev(StrP,'������: '+FormatDateTime('dd.mm.yyyy hh:nn:ss',now));
              StrWk:='----------------------------------------';
              PrintStrDev(StrP,StrWk);
              StrWk:=' ��������     ���-��   ����    �����  ';
              PrintStrDev(StrP,StrWk);
              StrWk:='----------------------------------------';
              PrintStrDev(StrP,StrWk);

              rDiscont:=0;

              ViewSpec.BeginUpdate;

              taCurMod.Active:=False;   //��������� �������� ���� �� ���� �� ������������
              taCurMod.ParamByName('IDT').AsInteger:=Tab.Id;
              taCurMod.ParamByName('STATION').AsInteger:=CommonSet.Station;
              taCurMod.Active:=True;

              quCurSpec.First;
              while not quCurSpec.Eof do
              begin      //��������� �������

                PosCh.Name:=quCurSpecNAME.AsString;
                PosCh.Code:=quCurSpecSIFR.AsString;
                PosCh.AddName:='';
                PosCh.Price:=RoundEx(quCurSpecPRICE.AsFloat*100);
                PosCh.Count:=RoundEx(quCurSpecQUANTITY.AsFloat*1000);

                StrWk:= Copy(PosCh.Name,1,36);
                while Length(StrWk)<36 do StrWk:=StrWk+' ';
                PrintStrDev(StrP,StrWk); //������� ����� �������� - �������� �������

                rDiscont:=rDiscont+quCurSpecDSUM.AsFloat;
//                rSumWithDisc:=rSumWithDisc+PosCh.Price*PosCh.Count;

                Str(quCurSpecQUANTITY.AsFloat:7:3,StrWk1);
                StrWk:='          '+StrWk1;
                Str(quCurSpecPRICE.AsFloat:7:2,StrWk1);
                StrWk:=StrWk+' '+StrWk1;
                Str(quCurSpecSUMMA.AsFloat:8:2,StrWk1);
                StrWk:=StrWk+' '+StrWk1+'���';
                PrintStrDev(StrP,StrWk);

                taCurMod.First;
                while not taCurMod.Eof do
                begin
                  if taCurModID_POS.AsInteger=quCurSpecID.AsInteger then
                  begin //��� �������
                    PrintStrDev(StrP,'   '+taCurModNAME.AsString);
                  end;
                  taCurMod.Next;
                end;

                quCurSpec.Next;
              end;

              ViewSpec.EndUpdate;

              StrWk:='----------------------------------------';
              PrintStrDev(StrP,StrWk);
              Str(TabCh.Summa:8:2,StrWk1);
              StrWk:=' �����                '+StrWk1+' ���';
              PrintStrDev(StrP,StrWk);

              Case Operation of
              0:begin
                  PrintStrDev(StrP,'�������                                 ');
                  if fmCash.cEdit5.EditValue>0 then
                  begin
                    PrintStrDev(StrP,prDefFormatStr(32,'��������� �����:',fmCash.cEdit5.EditValue));
                  end;
                  if fmCash.cEdit6.EditValue>0 then
                  begin
                    PrintStrDev(StrP,prDefFormatStr(32,'��������� �����:',fmCash.cEdit6.EditValue));
                  end;
                  if fmCash.cEdit2.EditValue>0 then
                  begin
                    PrintStrDev(StrP,prDefFormatStr(32,'��������:',fmCash.cEdit2.EditValue));
                  end;
                  if (fmCash.cEdit5.EditValue+fmCash.cEdit2.EditValue)>TabCh.Summa then
                  begin
                    PrintStrDev(StrP,prDefFormatStr(32,'�����:',(fmCash.cEdit5.EditValue+fmCash.cEdit6.EditValue+fmCash.cEdit2.EditValue)-fmCash.cEdit1.EditValue));
                  end;
                end;
              1:begin
                  PrintStrDev(StrP,'�������                                 ');
                  if fmCash.cEdit5.EditValue>0 then
                  begin
                    PrintStrDev(StrP,prDefFormatStr(32,'��������� �����:',fmCash.cEdit5.EditValue));
                  end;
                  if fmCash.cEdit6.EditValue>0 then
                  begin
                    PrintStrDev(StrP,prDefFormatStr(32,'��������� �����:',fmCash.cEdit6.EditValue));
                  end;
                  if (rSum-fmCash.cEdit5.EditValue)>0 then
                  begin
                    PrintStrDev(StrP,prDefFormatStr(32,'��������:',(rSum-fmCash.cEdit5.EditValue-fmCash.cEdit6.EditValue)));
                  end;
                end;
              end;


              if rDiscont>0.02 then
              begin
                PrintStrDev(StrP,'');
                rDProc:=RoundEx((rDiscont/(TabCh.Summa+rDiscont)*100)*100)/100;
                Str(rDProc:5:1,StrWk1);
                Str(rDiscont:8:2,StrWk);
                StrWk:=' � �.�. ������ - '+StrWk1+'%  '+StrWk+'�.';
                PrintStrDev(StrP,StrWk);
              end;

              if iPC=2 then
              begin
                PrintStrDev(StrP,' ');
                Str(rv(fmCash.cEdit6.value):10:2,StrWk);
                PrintStrDev(StrP,'�������� '+Copy(SalePC.CliName,1,20)+':'+StrWk);
                Str(rv(SalePC.rBalans-fmCash.cEdit6.value):10:2,StrWk);
                PrintStrDev(StrP,'������ �� �����:'+ StrWk);
                Str(rv(SalePC.rBalansDay-fmCash.cEdit6.value):10:2,StrWk);
                PrintStrDev(StrP,'������� �������� ������:'+ StrWk);
                PrintStrDev(StrP,' ');
              end;

              PrintStrDev(StrP,'������: '+Person.Name);
              PrintStrDev(StrP,'�������: _____________________');

              if CommonSet.LastLine1>'' then
              begin
                PrintStrDev(StrP,'');
                if fDifStr(1,CommonSet.LastLine1)>'' then PrintStrDev(StrP,fDifStr(1,CommonSet.LastLine1));
                if fDifStr(2,CommonSet.LastLine1)>'' then PrintStrDev(StrP,fDifStr(2,CommonSet.LastLine1));
              end;

              PrintStrDev(StrP,' ');

              kDelay:=(BufPr.iC div 40)-10;
              if kDelay<0 then kDelay:=0;

              prWrBuf(StrP);
//              delay(500);
              prCutDoc(StrP,0);   //}

              delay(CommonSet.ComDelay*5+trunc(kDelay*CommonSet.ComDelay/2));
            finally
              prDevClose(StrP,0);
            end;
          end;
        end;

        TimerSdTo0.Enabled:=True; //�������� ������ �� ��������� �����
      end;

     //��� ���������� - ���� ��������� � ����

      if CommonSet.CashNum<0 then bCheckOk:=True;

      if bCheckOk then
      begin
        prWriteLog('!!bCheckOk;Dt type-'+its(iPc)+';');

        if CommonSet.TypeFis<>'sp802' then
        begin
          inc(CommonSet.CashChNum); // ����� ����������� ���� ��� ���� ����� 802  � ���� ��� �������
          WriteCheckNum;
        end;

        iOp:=Operation;
        if iPC>0 then Operation:=2; //������ SalePC

        prWriteLog('!!prSaveSpec;Dt type-'+its(iPc)+';Oper-'+its(Operation)+';');

        IdH:=prSaveSpec(TabCh.Summa);  // ������������ ��������, ������ cashsail
        Operation:=iOp;

        //�������� �����
        if (Tab.DType=3) then
        begin
          Case Operation of    //��� ���� �� ������� ������ � ����� � ��������� ������� ��
          0: prWritePCSum(Tab.DBar,rv(CalcBonus(Tab.Id,Tab.DPercent)),IdH); //��� �������
          1: prWritePCSum(Tab.DBar,rv(CalcBonus(Tab.Id,Tab.DPercent)*(-1)),IdH);      //��� ��������
          end;
        end;

       // ��������� �������� �� ��������� �����
        Case Operation of    //��� ���� �� ������� ������ � ����� � ��������� ������� ��
        0: if iPC>0 then prWritePCSum(SalePC.BarCode,rv(fmCash.CEdit6.Value*(-1)),IdH); //- ��� �������
        1: if iPC>0 then prWritePCSum(SalePC.BarCode,rv(fmCash.CEdit6.Value),IdH);      //+ ��� ��������
        end;


        //���� ���
        quCashSail.Active:=False;
        idC:=GetId('CS');
        quCashSail.ParamByName('IDC').AsInteger:=IdC;
        quCashSail.Active:=True;

        quCashSail.Append;
        quCashSailID.AsInteger:=IdC;
        quCashSailCASHNUM.AsInteger:=CommonSet.CashNum;
        if (CommonSet.CashNum<0)or(CommonSet.SpecChar=1) then
        begin
          quCashSailZNUM.AsInteger:=CommonSet.CashZ;
          quCashSailCHECKNUM.AsInteger:=CommonSet.CashChNum;
        end
        else
        begin
          quCashSailZNUM.AsInteger:=Nums.ZNum;
          quCashSailCHECKNUM.AsInteger:=Nums.iCheckNum;
        end;

        quCashSailTAB_ID.AsInteger:=IdH;

        Case Operation of
        0: quCashSailTABSUM.AsFloat:=fmCash.cEdit1.EditValue;
        1: quCashSailTABSUM.AsFloat:=(-1)*fmCash.cEdit1.EditValue;
        2: quCashSailTABSUM.AsFloat:=0; //������� �� ��������� �����, ����� �������, ��� �� ������ ������
        end;

        quCashSailCLIENTSUM.AsFloat:=fmCash.cEdit2.EditValue;
        quCashSailCHDATE.AsDateTime:=now;
        quCashSailCASHERID.AsInteger:=Person.Id;
        quCashSailWAITERID.AsInteger:=Person.Id;
        quCashSailPAYTYPE.AsInteger:=iCashType;

        quCashSail.Post;

        quCashSail.Active:=False;

        //������� �� ������� ���� ����� ������

        if cEdit1.Tag>0 then
        begin
          //������ �����
//       ������ ����
          if abs(TabCh.Summa-Tab.Summa)<0.01 then
          begin
            quDelTab.Active:=False;
            quDelTab.ParamByName('Id').AsInteger:=cEdit1.Tag;

            trDel.StartTransaction;
            quDelTab.Active:=True;
            trDel.Commit;
          end;
          cEdit1.Tag:=0;

          Tab.Id_Personal:=Person.Id;
          Tab.Name:='';
          Tab.NumTable:='FF';
          Tab.DBar:='';
          Tab.DBar1:='';
          Tab.Id:=0;
          Tab.Summa:=0;
          Tab.Quests:=1;

          cxButton3.Enabled:=True;
          cxButton4.Enabled:=True;
          cxButton6.Enabled:=True;
          cxButton11.Enabled:=True;
          cxButton21.Enabled:=True;
        end;
      end;
     //-----------------------------

      //������ ������� ��� � ������������
      prClearCur.ParamByName('ID_TAB').AsInteger:=Tab.Id;
      prClearCur.ParamByName('ID_STATION').AsInteger:=CommonSet.Station;
      prClearCur.ExecProc;

      //��������� �� ������
      quCurSpecMod.Active:=False;
      quCurSpecMod.ParamByName('IDT').AsInteger:=Tab.Id;
      quCurSpecMod.ParamByName('STATION').AsInteger:=CommonSet.Station;
      quCurSpecMod.Active:=True;

      quCurSpec.Active:=False;
      quCurSpec.ParamByName('IDT').AsInteger:=Tab.Id;
      quCurSpec.ParamByName('STATION').AsInteger:=CommonSet.Station;
      quCurSpec.Active:=True;
      quCurSpec.Last;

      //�������������� �� ���������
      ViewSpecDPROC.Visible:=False;
      ViewSpecDSum.Visible:=False;
      DiscountBar:='';
      Label7.Caption:='';
      Tab.DBar:='';
      Tab.DPercent:=0;
      Tab.DName:='';
      Operation:=0;

      with dmC1 do
      begin
   //     cxButton15.Caption:='��� ������';
        quSaleT.First;
        if quSaleT.RecordCount>0 then cxButton15.Caption:=Copy(quSaleTNAMECS.AsString,1,15);
      end;

      WriteStatus;
      bPrintCheck:=False; //������� ������ ����
      PrintFCheck:=False; //��� ��������
    end;
  end;
  //��
  ToCustomDisp(' ������� �� �������.','                    ');
  delay(100);
  Button5.Enabled:=True;
end;

procedure TfmMainFF.cxButton1Click(Sender: TObject);
begin
  with dmC do
  begin
    if not quCurSpec.Bof then quCurSpec.Prior;
  end;
end;

procedure TfmMainFF.cxButton2Click(Sender: TObject);
begin
  with dmC do
  begin
    if not quCurSpec.Eof then
    begin
      quCurSpec.Next;
      if quCurSpec.Eof then
      begin
        quCurSpec.Prior;
        quCurSpec.Next;
      end;
    end;
  end;
end;

procedure TfmMainFF.Panel3Click(Sender: TObject);
begin
{
  if bPrintCheck then
  begin
    bExitPers:=True;
    exit;
  end;
  DestrViewPers;
  fmPre.ShowModal;
}
  DestrViewPers;
  if CommonSet.MReader=1 then fmPreFF.ShowModal
  else fmPre_Shape.ShowModal;
end;

procedure TfmMainFF.RxClock1Click(Sender: TObject);
begin
  if CanDo('prExit') then
    if MessageDlg('�� ������������� ������ �������� ���������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      FormLog('Close',IntToStr(Tab.Id_Personal));
      delay(100);
      Close;
    end;
end;

procedure TfmMainFF.cxButton3Click(Sender: TObject);
Var Id,J,I:INteger;
begin
  cEdit1.Tag:=0;
  Button5.Enabled:=False;
//  cxButton11.Enabled:=False;
//  cxButton3.Enabled:=False;
  with dmC do
  begin
    if GridHK.Tag>0 then //������� ������� � ������� �������
    begin
      //�������� ������
      J:=StrToINtDef(ViewHK.Controller.FocusedColumn.Caption,-1);
      I:=ViewHK.Controller.FocusedRowIndex;
      // ShowMessage(IntToStr(I)+' '+IntToStr(J));
      try
        quHotKey.Active:=False;
        quHotKey.ParamByName('CASHNUM').AsInteger:=CommonSet.Station;
        quHotKey.Active:=True;
        quHotKey.First;
        while not quHotKey.Eof do
        begin
          if (quHotKeyCASHNUM.AsInteger=CommonSet.Station)and
          (quHotKeyIROW.AsInteger=I)and
          (quHotKeyICOL.AsInteger=J) then quHotKey.Delete
          else quHotKey.Next;
        end;
        quHotKey.Active:=False;
        ViewHK.DataController.SetValue(I,J,'');
      except
      end;
    end else
    begin
      if quCurSpec.Eof then quCurSpec.First;
      if not quCurSpec.Eof then
      begin
        if not CanDo('prDelPosFF') then begin showmessage('��� ����.'); exit; end;
        if MessageDlg('�� ������������� ������ ������� "'+quCurSpecNAME.AsString+'"', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          FormLog('DelPos3',IntToStr(Person.Id)+' '+quCurSpecSifr.AsString+' '+fts(quCurSpecQUANTITY.AsFloat)+' '+fts(quCurSpecSUMMA.AsFloat)+' '+quCurSpecName.AsString);
          if quCurSpecIStatus.AsInteger=1 then
          begin
            taServP.Active:=False; //�������� ��� ������ �����
            taServP.CreateDataSet;

            taServP.Append;
            taServPName.AsString:=quCurSpecName.AsString;
            taServPCode.AsString:=quCurSpecSifr.AsString;
            taServPQuant.AsFloat:=quCurSpecQuantity.AsFloat;
            taServPStream.AsInteger:=quCurSpecStream.AsInteger;
            taServPiType.AsInteger:=0; //�����
            taServP.Post;

            PrintServCh('������');
          end;

          Id:=quCurSpecID.AsInteger;
          ViewModif.BeginUpdate;
          quCurSpecMod.First;
          while not quCurSpecMod.Eof do
          begin
            if quCurSpecModID_POS.AsInteger=Id then quCurSpecMod.Delete
            else quCurSpecMod.Next;
          end;
          ViewModif.EndUpdate;

          quCurSpec.Locate('ID',Id,[]);

          if Prizma then
          begin
            prWriteLog('!!AddPos;'+its(Nums.iCheckNum)+';'+its(quCurSpecID.AsInteger)+';'+its(quCurSpecSIFR.AsInteger)+';'+quCurSpecNAME.AsString+';'+fts(quCurSpecQUANTITY.AsFloat)+';'+fts(quCurSpecSUMMA.AsFloat)+';'+fts(quCurSpecDSUM.AsFloat));
            Event_RegEx(18,Nums.iCheckNum,quCurSpecID.AsInteger,quCurSpecSIFR.AsInteger,'',quCurSpecNAME.AsString,quCurSpecPRICE.AsFloat,quCurSpecQUANTITY.AsFloat,quCurSpecSUMMA.AsFloat);
          end;

          quCurSpec.Delete;
        end;
        RecalcSum;
      end;
    end;
  end;
  Button5.Enabled:=True;
//  cxButton11.Enabled:=True;
//  cxButton3.Enabled:=True;
end;

procedure TfmMainFF.cxButton4Click(Sender: TObject);
Var rQ:Real;
    cSum,cSumD:Real;
begin
  cEdit1.Tag:=0;
  with dmC do
  begin
    bAddPosFromMenu:=False; //��� ���� ����� ������ � ��������� ����� ������������
    bAddPosFromSpec:=True;

    if quCurSpec.Eof then exit;
    fmCalc.CalcEdit1.EditValue:=quCurSpecQuantity.AsFloat;
    fmCalc.ShowModal;
    if fmCalc.ModalResult=mrOk then
    begin
      delay(10);
      rQ:=RoundEx(fmCalc.CalcEdit1.EditValue*1000)/1000;

      CalcDiscontN(quCurSpecSIFR.AsInteger,quCurSpecID.AsInteger,-1,quCurSpecSTREAM.AsInteger,quCurSpecPrice.AsFloat,rQ,rQ*quCurSpecPrice.AsFloat,Tab.DBar,Now,Check.DProc,Check.DSum);

      cSum:=RoundVal(quCurSpecPrice.AsFloat*rQ-Check.DSum);
      cSumD:=RoundVal(quCurSpecPrice.AsFloat*rQ)-cSum;

      quCurSpec.Edit;
      quCurSpecQuantity.AsFloat:=rQ;
      quCurSpecDProc.AsFloat:=Check.DProc;
      quCurSpecDSum.AsFloat:=cSumD;
      quCurSpecSumma.AsFloat:=cSum;
      quCurSpec.Post;

      if Prizma then
      begin
        prWriteLog('!!QuantPos;'+its(Nums.iCheckNum)+';'+its(quCurSpecID.AsInteger)+';'+its(quCurSpecSIFR.AsInteger)+';'+quCurSpecNAME.AsString+';'+fts(quCurSpecQUANTITY.AsFloat)+';'+fts(quCurSpecSUMMA.AsFloat)+';'+fts(quCurSpecDSUM.AsFloat));
        Event_RegEx(12,Nums.iCheckNum,quCurSpecID.AsInteger,quCurSpecSIFR.AsInteger,'',quCurSpecNAME.AsString,quCurSpecPRICE.AsFloat,quCurSpecQUANTITY.AsFloat,quCurSpecSUMMA.AsFloat);
      end;

      RecalcSum;
    end
  end;
end;

procedure TfmMainFF.N1Click(Sender: TObject);
Var I,J:Integer;
begin
  //�������� ������
  J:=StrToINtDef(ViewHK.Controller.FocusedColumn.Caption,-1);
  I:=ViewHK.Controller.FocusedRowIndex;
// ShowMessage(IntToStr(I)+' '+IntToStr(J));
  with dmC do
  begin
    try
      quHotKey.Active:=False;
      quHotKey.ParamByName('CASHNUM').AsInteger:=CommonSet.Station;
      quHotKey.Active:=True;
      quHotKey.First;
      while not quHotKey.Eof do
      begin
        if (quHotKeyCASHNUM.AsInteger=CommonSet.Station)and
           (quHotKeyIROW.AsInteger=I)and
           (quHotKeyICOL.AsInteger=J) then quHotKey.Delete
        else quHotKey.Next;
      end;
      quHotKey.Active:=False;
      ViewHK.DataController.SetValue(I,J,'');
    except
    end;
  end;

end;

procedure TfmMainFF.ViewMDragOver(Sender, Source: TObject; X, Y: Integer;
  State: TDragState; var Accept: Boolean);
begin
//
end;

procedure TfmMainFF.ViewHKDragOver(Sender, Source: TObject; X, Y: Integer;
  State: TDragState; var Accept: Boolean);
begin
 Accept:=False;
  if bDr then
  begin
    Accept:=True;
  end;
end;

procedure TfmMainFF.ViewMMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
//  if Button = mbRight then
  if Button = mbRight then
  begin
    bDr:=True;
    GridM.BeginDrag(True,3);
  end;
end;

procedure TfmMainFF.ViewHKDragDrop(Sender, Source: TObject; X, Y: Integer);
Var StrWk,Str1:String;
    I,J:INteger;
begin
//���������
  with dmC do
  begin
    if quMenuTREETYPE.AsString='F' then
    begin
      I:=Y div 45;
      J:=X div Colwidth;
//      StrWk:=INtToStr(quMenuSIFR.AsInteger)+' '+INtToStr(X)+' '+INtToStr(Y)+' '+INtToStr(I)+' '+INtToStr(J);
//      ShowMessage(StrWk);
      try
        quHotKey.Active:=False;
        quHotKey.ParamByName('CASHNUM').AsInteger:=CommonSet.Station;
        quHotKey.Active:=True;

        quHotKey.First;
        while not quHotKey.Eof do
        begin
          if (quHotKeyCASHNUM.AsInteger=CommonSet.Station)and
             (quHotKeyIROW.AsInteger=I)and
             (quHotKeyICOL.AsInteger=J) then
          begin
            quHotKey.Delete; break;
          end else quHotKey.Next;
        end;
        quHotKey.Append;
        quHotKeyCASHNUM.AsInteger:=CommonSet.Station;
        quHotKeyIROW.AsInteger:=I;
        quHotKeyICOL.AsInteger:=J;
        quHotKeySIFR.AsInteger:=quMenuSIFR.AsInteger;
        quHotKey.Post;

        Str(quMenuPRICE.AsFloat:6:2,StrWk);
        Str1:=quMenuNAME.AsString; while pos(#13,Str1)>0 do Str1[pos(#13,Str1)]:=' ';

        if MaxCol>=10 then //17
          StrWk:=Copy(Str1,1,14)+#13+Copy(Str1,15,14)+#13+'����:'+StrWk+'�.'+#13+IntToStr(quHotKeySIFR.AsInteger)
        else //15 ���� ��������������������
          StrWk:=Copy(Str1,1,14)+#13+Copy(Str1,15,14)+#13+'����:'+StrWk+'�.'+#13+IntToStr(quHotKeySIFR.AsInteger);

        quHotKey.Active:=False;

        ViewHK.DataController.SetValue(I,J,StrWk);
      except
      end;
    end;
  end;
end;

procedure TfmMainFF.ViewHKCellClick(Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
Var StrWk:String;
    iCode:Integer;
    rQ:Real;
    cSum,cSumD:Real;
begin
//  StrWk:=IntToStr(ACellViewInfo.Bounds.TopLeft.X)+' '+IntToStr(ACellViewInfo.Bounds.TopLeft.Y);
//  Label1.Caption:=StrWk; //���������

  TimerSdTo0.Enabled:=False;

  if Button5.Enabled=False then exit;
  if bPrintCheck then exit;
  if AButton=mbRight then exit;
  if cEdit1.Tag>0 then exit;
  if GridHK.Tag>0 then
  begin
{    iC:=ACellViewInfo.Item.FocusedCellViewInfo.Bounds.Left+10;
    iL:=ACellViewInfo.Item.FocusedCellViewInfo.Bounds.Top+10;

    I:=iL div 45;
    J:=iC div Colwidth;

    Label11.Caption:=its(i)+' '+its(j);
}
    exit;
  end;

  bAddPosFromMenu:=False; //��� ���� ����� ������ � ��������� ����� ������������
  bAddPosFromSpec:=False;

  StrWk:=ACellViewInfo.Text;
//  Label2.Caption:=StrWk; // ������ ������� � ����������
  delete(StrWk,1,POS(#13,StrWk)); //�������� ������ ������
  delete(StrWk,1,POS(#13,StrWk)); //�������� ������ ������
  delete(StrWk,1,POS(#13,StrWk)); //����
//  Label3.Caption:=StrWk;  //��� ������   4-�� ������
//  ShowMessage(StrWk);
  iCode:=StrToIntDef(StrWk,0);
  if iCode>0 then
  begin
    //��� ������, ������� 
    with dmC do
    begin
      quMenuHK.Active:=False;
      quMenuHK.ParamByName('ISIFR').AsInteger:=iCode;
      qumenuHK.Active:=True;
      if quMenuHK.RecordCount>0 then
      begin
        quMenuHK.First;

        quCurSpec.Last;
        if (quCurSpecSifr.AsInteger<>quMenuHKSIFR.AsInteger)or(quMenuHKDESIGNSIFR.AsInteger>0) then
        begin
          rQ:=1;

          if quMenuHKDESIGNSIFR.AsInteger>0 then
          begin
            fmCalc.CalcEdit1.EditValue:=1;
            fmCalc.ShowModal;
            if (fmCalc.ModalResult=mrOk)and(fmCalc.CalcEdit1.EditValue>0.00001)  then
            begin
              rQ:=RoundEx(fmCalc.CalcEdit1.EditValue*1000)/1000;
            end;
          end;

          CalcDiscontN(quMenuHKSIFR.AsInteger,Check.Max+1,-1,quMenuHKSTREAM.AsInteger,quMenuHKPRICE.AsFloat,rQ,rQ*quMenuHKPRICE.AsFloat,Tab.DBar,Now,Check.DProc,Check.DSum);

          cSum:=RoundVal(quMenuHKPRICE.AsFloat*rQ-Check.DSum);
          cSumD:=RoundVal(quMenuHKPRICE.AsFloat*rQ)-cSum;

          if CommonSet.CashNum<0 then Tab.Id:=CommonSet.CashNum else Tab.Id:=CommonSet.CashNum*(-1);

          quCurSpec.Append;
          quCurSpecSTATION.AsInteger:=CommonSet.Station;
          quCurSpecID_TAB.AsInteger:=Tab.Id;
          quCurSpecId_Personal.AsInteger:=Person.Id;
          quCurSpecNumTable.AsString:=INtToStr(Tab.Id);
          quCurSpecId.AsInteger:=Check.Max+1;
          quCurSpecSifr.AsInteger:=quMenuHKSIFR.AsInteger;
          quCurSpecPrice.AsFloat:=quMenuHKPRICE.AsFloat;
          quCurSpecQuantity.AsFloat:=rQ;
          quCurSpecSumma.AsFloat:=cSum;
          quCurSpecDProc.AsFloat:=Check.DProc;
          quCurSpecDSum.AsFloat:=cSumD;
          quCurSpecIStatus.AsInteger:=0;
          quCurSpecName.AsString:=quMenuHKNAME.AsString;
          quCurSpecCode.AsString:=quMenuHKCODE.AsString;
          quCurSpecLinkM.AsInteger:=quMenuHKLINK.AsInteger;
          if quMenuHKLIMITPRICE.AsFloat>0 then
          quCurSpecLimitM.AsInteger:=RoundEx(quMenuHKLIMITPRICE.AsFloat)
          else quCurSpecLimitM.AsInteger:=99;
          quCurSpecStream.AsInteger:=quMenuHKSTREAM.AsInteger;
          quCurSpec.Post;
          inc(Check.Max);

          if Prizma then
          begin
            prWriteLog('!!AddPos;'+its(Nums.iCheckNum)+';'+its(Check.Max)+';'+its(quCurSpecSIFR.AsInteger)+';'+quCurSpecNAME.AsString+';'+fts(quCurSpecQUANTITY.AsFloat)+';'+fts(quCurSpecSUMMA.AsFloat)+';'+fts(quCurSpecDSUM.AsFloat));
            Event_RegEx(6,Nums.iCheckNum,Check.Max,quCurSpecSIFR.AsInteger,'',quCurSpecNAME.AsString,quCurSpecPRICE.AsFloat,quCurSpecQUANTITY.AsFloat,quCurSpecSUMMA.AsFloat);
          end;

          if quMenuHKLINK.AsInteger>0 then
          begin //������������
            acModify.Execute;
          end;
        end else
        begin
          rQ:=quCurSpecQuantity.AsFloat;

          CalcDiscontN(quMenuHKSIFR.AsInteger,Check.Max+1,-1,quMenuHKSTREAM.AsInteger,quMenuHKPRICE.AsFloat,rQ+1,(rQ+1)*quMenuHKPRICE.AsFloat,Tab.DBar,Now,Check.DProc,Check.DSum);

          cSum:=RoundVal(quMenuHKPRICE.AsFloat*(rQ+1)-Check.DSum);
          cSumD:=RoundVal(quMenuHKPRICE.AsFloat*(rQ+1))-cSum;

          quCurSpec.Edit;
          quCurSpecQuantity.AsFloat:=rQ+1;
          quCurSpecSumma.AsFloat:=cSum;
          quCurSpecDProc.AsFloat:=Check.DProc;
          quCurSpecDSum.AsFloat:=cSumD;
          quCurSpec.Post;
        end;
        RecalcSum;
      end;
      quMenuHK.Active:=False;
    end;
  end;
end;

procedure TfmMainFF.acDiscountExecute(Sender: TObject);
Var StrWk:String;
    cSum,cSumD:Real;
begin
  if Button5.Enabled=False then exit;

  cEdit1.Tag:=0;
  with dmC do
  begin
    if quCurSpec.RecordCount>0 then
    begin
      fmDiscount_Shape.cxTextEdit1.Text:='';
      fmDiscount_Shape.Caption:='��������� ���������� �����';
      fmDiscount_Shape.Label1.Caption:='��������� ���������� �����';
      fmDiscount_Shape.ShowModal;
      if fmDiscount_Shape.ModalResult=mrOk then
      begin
        //������� ������� ������
//      showmessage('');


        if fmDiscount_Shape.cxTextEdit1.Text>'' then
        begin
          DiscountBar:=sOnlyDigit(fmDiscount_Shape.cxTextEdit1.Text);
          WriteHistory('��:"'+DiscountBar+'"');

          If FindDiscount(DiscountBar) then
          begin
            // ��������� ������
            Str(Tab.DPercent:5:2,StrWk);
            Label7.Caption:='������������ ������ - '+ Tab.DName+' ('+StrWk+'%)';
            //������ ������ �� ��� �������
            ViewSpec.BeginUpdate;
            quCurSpec.First;
            While not quCurSpec.Eof do
            begin
              CalcDiscontN(quCurSpecSIFR.AsInteger,quCurSpecID.AsInteger,-1,quCurSpecSTREAM.AsInteger,quCurSpecPrice.AsFloat,quCurSpecQuantity.AsFloat,quCurSpecQuantity.AsFloat*quCurSpecPrice.AsFloat,Tab.DBar,Now,Check.DProc,Check.DSum);
              if Check.DSum<>0 then
              begin

                cSum:=RoundVal(quCurSpecPrice.AsFloat*quCurSpecQuantity.AsFloat-Check.DSum);
                cSumD:=RoundVal(quCurSpecPrice.AsFloat*quCurSpecQuantity.AsFloat)-cSum;

                quCurSpec.Edit;
                quCurSpecSumma.AsFloat:=cSum;
                quCurSpecDProc.AsFloat:=Check.DProc;
                quCurSpecDSum.AsFloat:=cSumD;
                quCurSpec.Post;

                ViewSpecDPROC.Visible:=True;
                ViewSpecDSum.Visible:=True;

{                if Prizma then
                begin
                  prWriteLog('!!prDiscont;'+its(Nums.iCheckNum)+';'+Tab.DName+';'+fts(Check.DProc));
                  Event_RegEx(35,Nums.iCheckNum,0,0,'','',0,0,0,Check.DProc,cSumD);
                end;}

              end;
              quCurSpec.Next;
            end;
            quCurSpec.First;

            RecalcSum;
            ViewSpec.EndUpdate;
          end;
        end else //������ �������� - ��� ������ ������
        begin
            //������ ������ �� ��� �������
          PDiscountBar:='';
          DiscountBar:='';
          Check.DProc:=0;
          Check.DSum:=0;
          Tab.DBar:='';
          Tab.DPercent:=0;
          Tab.DName:='';

          ViewSpec.BeginUpdate;
          quCurSpec.First;
          While not quCurSpec.Eof do
          begin
            quCurSpec.Edit;
            quCurSpecSumma.AsFloat:=RoundEx(quCurSpecPrice.AsFloat*quCurSpecQuantity.AsFloat*100)/100;
            quCurSpecDProc.AsFloat:=0;
            quCurSpecDSum.AsFloat:=0;
            quCurSpec.Post;

            quCurSpec.Next;
          end;
          //�������

          Label7.Caption:='';
          ViewSpecDPROC.Visible:=False;
          ViewSpecDSum.Visible:=False;

          quCurSpec.First;
          RecalcSum;
          ViewSpec.EndUpdate;
        end;

        if Prizma then
        begin
          prWriteLog('!!prDiscont;'+its(Nums.iCheckNum)+';'+Tab.DName+';'+fts(Check.DProc));
          Event_RegEx(35,Nums.iCheckNum,0,0,'','',0,0,0);
        end;

      end;
    end;
  end;
end;

procedure TfmMainFF.cxButton5Click(Sender: TObject);
begin
  with dmC do
  begin
    if quCurSpecLINKM.AsInteger>0 then  acModify.Execute;
  end;
end;

procedure TfmMainFF.Panel4Click(Sender: TObject);
begin
  if Panel5.Visible then Panel5.Visible:=False
  else begin
    Panel5.Visible:=True;
    if CanDo('prRetCheck') then cxButton9.Visible:=True else cxButton9.Visible:=False;
    if CanDo('prCashRep') then
    begin
      cxButton7.Visible:=True;
      cxButton8.Visible:=True;
      cxButton27.Visible:=True;
    end else
    begin
      cxButton7.Visible:=False;
      cxButton8.Visible:=False;
      cxButton27.Visible:=False;
    end;
  end;
end;

procedure TfmMainFF.cxButton7Click(Sender: TObject);
Var StrWk,StrWk1:String;
//    iRet:Integer;
    iNumSm,iCountDoc,iMin,iMax:Integer;
    rSumNal,rSumRet,rSumBNal,rSumBRet,rSumDisc:Real;
    l:ShortInt;
    StrP:String;
begin
  if not CanDo('prXRep') then begin Showmessage('��� ����.'); exit; end;
  prWriteLog('!!XRep;'+IntToStr(Person.Id)+';'+Person.Name+';');

  if (CommonSet.CashNum>0)and(CommonSet.SpecChar=0) then //����������� ����������
  begin
    //����������� ���������� X
    if GetX=False then ShowMessage('������ ��������� X - ������.');
  end else
  begin //��� ����� ������������ ������ �����?

    //������ ������� ��� ���� ���������
    with dmC do
    with dmC1 do
    begin
      iNumSm:=Nums.ZNum;
      iCountDoc:=Nums.iCheckNum;

      //������� ���, ������
      rSumNal:=0;
      rSumBNal:=0;
      quRSum.Active:=False;
      quRSum.ParamByName('ICASHNUM').AsInteger:=CommonSet.CashNum;
      quRSum.ParamByName('IZNUM').AsInteger:=iNumSm;
      quRSum.Active:=True;
      quRSum.First;
      while not quRSum.Eof do
      begin
        if quRSumPAYTYPE.AsInteger=0 then rSumNal:=quRSumSUMR.AsFloat;
        if quRSumPAYTYPE.AsInteger>0 then rSumBNal:=rSumBNal+quRSumSUMR.AsFloat;
        quRSum.Next;
      end;
      quRSum.Active:=False;

      //�������� ���, ������
      rSumRet:=0;
      rSumBRet:=0;
      quRSumRet.Active:=False;
      quRSumRet.ParamByName('ICASHNUM').AsInteger:=CommonSet.CashNum;
      quRSumRet.ParamByName('IZNUM').AsInteger:=iNumSm;
      quRSumRet.Active:=True;
      quRSumRet.First;
      while not quRSumRet.Eof do
      begin
        if quRSumRetPAYTYPE.AsInteger=0 then rSumRet:=quRSumRetSUMR.AsFloat;
        if quRSumRetPAYTYPE.AsInteger>0 then rSumBRet:=rSumBRet+quRSumRetSUMR.AsFloat;
        quRSumRet.Next;
      end;
      quRSumRet.Active:=False;

      rSumRet:=(-1)*rSumRet;
      rSumBRet:=(-1)*rSumBRet;

      iMax:=0;
      iMin:=0;
      quMaxMin.Active:=False;
      quMaxMin.ParamByName('ICASHNUM').AsInteger:=CommonSet.CashNum;
      quMaxMin.ParamByName('IZNUM').AsInteger:=iNumSm;
      quMaxMin.Active:=True;
      if quMaxMin.RecordCount>0 then
      begin
        quMaxMin.First;
        iMin:=quMaxMinIBEG.AsInteger;
        iMax:=quMaxMinIEND.AsInteger;
      end;
      quMaxMin.Active:=False;

      rSumDisc:=0;
      if iMax>0 then
      begin
        rSumDisc:=0;
      end else
      begin
        quDSum.Active:=False;
        quDSum.ParamByName('IMIN').AsInteger:=iMin;
        quDSum.ParamByName('IMAX').AsInteger:=iMax;
        quDSum.ParamByName('ISTATION').AsInteger:=CommonSet.Station;
        quDSum.Active:=True;
        if quDSum.RecordCount>0 then
        begin
          quDSum.First;
          rSumDisc:=quDSumDSUM.AsFloat;
        end;
      end;

          //������� ��������
      if (Pos('COM',CommonSet.SpecBoxX)>0)or(Pos('LPT',CommonSet.SpecBoxX)>0) then
      begin
        try
          l:=40;
          prDevOpen(CommonSet.SpecBoxX,0);
          StrP:=CommonSet.SpecBoxX;
          prSetFont(StrP,10,0);
          prPrintStr(StrP,SpecVal.SPH1);
          prPrintStr(StrP,SpecVal.SPH2);
          prPrintStr(StrP,SpecVal.SPH3);
          prPrintStr(StrP,SpecVal.SPH4);
          prPrintStr(StrP,SpecVal.SPH5);
          StrWk:=IntToStr(iMax+1); //����� ���������
          while Length(StrWk)<5 do StrWk:='0'+StrWk;
          if CommonSet.SpecChar=1 then
            prPrintStr(StrP,'�������� N: '+CommonSet.CashSer+'         N ���: '+StrWk);
          prPrintStr(StrP,FormatDateTime('dd-mm-yyyy                         hh:nn',now));
          prPrintStr(StrP,' ');
          StrWk:='            X-����� N '+IntToStr(iNumSm);
          prPrintStr(StrP,StrWk);
          prPrintStr(StrP,' ');
          prPrintStr(StrP,'��������:');
          prPrintStr(StrP,prDefFormatStr1(l,'�������',rSumNal));
          prPrintStr(StrP,prDefFormatStr1(l,'�����. �������',0));
          prPrintStr(StrP,prDefFormatStr1(l,'�������',rSumRet));
          prPrintStr(StrP,' ');
          prPrintStr(StrP,'������:');
          prPrintStr(StrP,prDefFormatStr1(l,'�������',0));
          prPrintStr(StrP,prDefFormatStr1(l,'�����. �������',0));
          prPrintStr(StrP,' ');
          prPrintStr(StrP,'����. �����:');
          prPrintStr(StrP,prDefFormatStr1(l,'�������',rSumBNal));
          prPrintStr(StrP,prDefFormatStr1(l,'�����. �������',rSumBRet));
          prPrintStr(StrP,' ');
          prPrintStr(StrP,'����. �����2:');
          prPrintStr(StrP,prDefFormatStr1(l,'�������',0));
          prPrintStr(StrP,prDefFormatStr1(l,'�����. �������',0));
          prPrintStr(StrP,' ');
          prPrintStr(StrP,'����. �����3:');
          prPrintStr(StrP,prDefFormatStr1(l,'�������',0));
          prPrintStr(StrP,prDefFormatStr1(l,'�����. �������',0));
          prPrintStr(StrP,' ');
          prPrintStr(StrP,'����. �����4:');
          prPrintStr(StrP,prDefFormatStr1(l,'�������',0));
          prPrintStr(StrP,prDefFormatStr1(l,'�����. �������',0));
          prPrintStr(StrP,' ');

          //���� ������
          quSumTalons.Active:=False;
          quSumTalons.ParamByName('IMIN').AsInteger:=iMin;
          quSumTalons.ParamByName('IMAX').AsInteger:=iMax;
          quSumTalons.ParamByName('ISTATION').AsInteger:=CommonSet.Station;
          quSumTalons.Active:=True;
          if quSumTalons.RecordCount>0 then
          begin
            quSumTalons.First;
            while not quSumTalons.Eof do
            begin
              StrWk:=Copy(quSumTalonsNAME.AsString,1,15);
              while Length(StrWk)<15 do StrWk:=StrWk+' ';
              StrWk1:=its(Trunc(RoundEx(quSumTalonsQUANT.AsFloat)));
              while Length(StrWk1)<4 do StrWk1:=' '+StrWk1;
              StrWk:=StrWk+' x'+StrWk1;

              prPrintStr(StrP,prDefFormatStr1(l,StrWk,quSumTalonsRSUM.AsFloat));
              quSumTalons.Next;
            end;
          end;
          quSumTalons.Active:=False;
          prPrintStr(StrP,' ');
          
          prPrintStr(StrP,'================= ����� ================');
          prPrintStr(StrP,prDefFormatStr1(l,'�������',rSumNal+rSumBNal));
          prPrintStr(StrP,prDefFormatStr1(l,'� �.�. ����:',0));
          prPrintStr(StrP,prDefFormatStr1(l,' ������',rSumDisc));
          prPrintStr(StrP,prDefFormatStr1(l,'�����. �������',0));
          prPrintStr(StrP,prDefFormatStr1(l,'� �.�. ����:',0));
          prPrintStr(StrP,prDefFormatStr1(l,'�������',rSumRet+rSumBRet));
          prPrintStr(StrP,prDefFormatStr1(l,'� �.�. ����:',0));
          prPrintStr(StrP,prDefFormatStr1(l,'������������:',0));
          prPrintStr(StrP,prDefFormatStr1(l,'����������:',0));
          prPrintStr(StrP,' ');
          prPrintStr(StrP,prDefFormatStr2(l,'��������� ����������:',iCountDoc+4));
          prPrintStr(StrP,prDefFormatStr2(l,'� �.�. �����:',iCountDoc));
          prPrintStr(StrP,prDefFormatStr2(l,'� �.�. �����. �����:',0));
          prPrintStr(StrP,prDefFormatStr2(l,'� �.�. ������. ���. ���. �����:',0));
          prPrintStr(StrP,' ');
          prPrintStr(StrP,prDefFormatStr2(l,'�������:',iCountDoc));
          prPrintStr(StrP,prDefFormatStr2(l,'�����. �������:',0));
          prPrintStr(StrP,prDefFormatStr2(l,'�������:',0));
          prPrintStr(StrP,prDefFormatStr2(l,'������������:',0));
          prPrintStr(StrP,prDefFormatStr2(l,'����������:',0));

          if CommonSet.SpecChar=1 then CutDocPrSpec
          else
          begin
             prCutDoc(StrP,0);
             delay(CommonSet.ComDelay*5);
          end;
        finally
          prDevClose(StrP,0);
        end;
      end; //if Pos('COM',CommonSet.SpecBoxX)>0 then
      if Pos('fis',CommonSet.SpecBoxX)>0 then
      begin
        TestPrint:=1;
        While PrintQu and (TestPrint<=MaxDelayPrint) do
        begin
//        showmessage('������� �����, ��������� �������.');
          prWriteLog('������������� ������ ����� � �������.');
          delay(1000);
          inc(TestPrint);
        end;
        try
          PrintFCheck:=True; //��� ���� � ������� ������� ��������� ������

          l:=40;
          if CommonSet.SpecBoxX='fisshtrih' then l:=32;

          OpenNFDoc;

          PrintNFStr(SpecVal.SPH1);
          PrintNFStr(SpecVal.SPH2);
          PrintNFStr(SpecVal.SPH3);
          PrintNFStr(SpecVal.SPH4);
          PrintNFStr(SpecVal.SPH5);
          StrWk:=IntToStr(iMax+1); //����� ���������
          while Length(StrWk)<5 do StrWk:='0'+StrWk;
          PrintNFStr('�������� N: '+CommonSet.CashSer+' N ���: '+StrWk);
          PrintNFStr('����� � '+IntToStr(CommonSet.CashNum));
          PrintNFStr(FormatDateTime('dd-mm-yyyy                 hh:nn',now));
          PrintNFStr(' ');
          StrWk:='            X-����� N '+IntToStr(iNumSm);
          PrintNFStr(StrWk);
          PrintNFStr(' ');
          PrintNFStr('��������:');
          PrintNFStr(prDefFormatStr1(l,'�������',rSumNal));
          PrintNFStr(prDefFormatStr1(l,'�����. �������',0));
          PrintNFStr(prDefFormatStr1(l,'�������',rSumRet));
          PrintNFStr(' ');
          PrintNFStr('������:');
          PrintNFStr(prDefFormatStr1(l,'�������',0));
          PrintNFStr(prDefFormatStr1(l,'�����. �������',0));
          PrintNFStr(' ');
          PrintNFStr('����. �����:');
          PrintNFStr(prDefFormatStr1(l,'�������',rSumBNal));
          PrintNFStr(prDefFormatStr1(l,'�����. �������',rSumBRet));
          PrintNFStr(' ');
          PrintNFStr('����. �����2:');
          PrintNFStr(prDefFormatStr1(l,'�������',0));
          PrintNFStr(prDefFormatStr1(l,'�����. �������',0));
          PrintNFStr(' ');
          PrintNFStr('����. �����3:');
          PrintNFStr(prDefFormatStr1(l,'�������',0));
          PrintNFStr(prDefFormatStr1(l,'�����. �������',0));
          PrintNFStr(' ');
          PrintNFStr('����. �����4:');
          PrintNFStr(prDefFormatStr1(l,'�������',0));
          PrintNFStr(prDefFormatStr1(l,'�����. �������',0));
          PrintNFStr(' ');
          PrintNFStr('================= ����� ================');
          PrintNFStr(prDefFormatStr1(l,'�������',rSumNal+rSumBNal));
          PrintNFStr(prDefFormatStr1(l,'� �.�. ����:',0));
          PrintNFStr(prDefFormatStr1(l,' ������',rSumDisc));
          PrintNFStr(prDefFormatStr1(l,'�����. �������',0));
          PrintNFStr(prDefFormatStr1(l,'� �.�. ����:',0));
          PrintNFStr(prDefFormatStr1(l,'�������',rSumRet+rSumBRet));
          PrintNFStr(prDefFormatStr1(l,'� �.�. ����:',0));
          PrintNFStr(prDefFormatStr1(l,'������������:',0));
          PrintNFStr(prDefFormatStr1(l,'����������:',0));
          PrintNFStr(' ');
          PrintNFStr(prDefFormatStr2(l,'��������� ����������:',iCountDoc+4));
          PrintNFStr(prDefFormatStr2(l,'� �.�. �����:',iCountDoc));
          PrintNFStr(prDefFormatStr2(l,'� �.�. �����. �����:',0));
          PrintNFStr(prDefFormatStr2(l,'� �.�. ������. ���. ���. �����:',0));
          PrintNFStr(' ');
          PrintNFStr(prDefFormatStr2(l,'�������:',iCountDoc));
          PrintNFStr(prDefFormatStr2(l,'�����. �������:',0));
          PrintNFStr(prDefFormatStr2(l,'�������:',0));
          PrintNFStr(prDefFormatStr2(l,'������������:',0));
          PrintNFStr(prDefFormatStr2(l,'����������:',0));

          PrintNFStr(' ');//PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');

          if Pos('fisshtrih',CommonSet.SpecBoxX)>0 then
          begin
            PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');
          end;

          CloseNFDoc;
          CutDoc;
        finally
          PrintFCheck:=False; //��� ���� � ������� ������� ��������� ������
        end;
      end; //if Pos('fis',CommonSet.SpecBoxX)>0 then

    end;
  end;

  if Prizma then
  begin
    prWriteLog('!!GetX');
    Event_RegEx(209,0,0,0,'','',0,0,0);
  end;

end;


procedure TfmMainFF.cxButton8Click(Sender: TObject);
Var iRet:INteger;
    StrWk,StrWk1:String;
    iNumSm,iCountDoc,iMin,iMax:Integer;
    rSumNal,rSumRet,rSumBNal,rSumBRet,rSumDisc:Real;
    l:ShortInt;
    StrP:String;
begin
  //�������� �����
  if not CanDo('prZRep') then begin Showmessage('��� ����.'); exit; end;

  prWriteLog('!!ZRep;'+IntToStr(Person.Id)+';'+Person.Name+';');

  if (CommonSet.CashNum>0)and(CommonSet.SpecChar=0) then //����������� ����������
  begin
    if OpenZ then //��� �������� ����� � �������� ������� �� �����
    begin
      //�������� ��� �������
      ZClose;
      iRet:=InspectSt(StrWk);
      if iRet=0 then
      begin
        Label3.Caption:='������ ���: ��� ��.';
        if CashDate(StrWk) then Label2.Caption:='�������� ����: '+StrWk;
        inc(CommonSet.CashZ);

        GetSerial;
        CashDate(Nums.sDate);
        GetNums;
        GetRes; //�������

        WriteStatus;
        if CommonSet.IncNumTab>0 then CommonSet.IncNumTab:=1;//��������� ������� ������� ������
        WriteCheckNum;

//        acRecalcCount.Execute; //����� currest
      end
      else
      begin
        Label3.Caption:='������ ���: ������ ('+IntToStr(iRet)+')';
        WriteHistoryFR('������ ���: ������ ('+IntToStr(iRet)+')');
      end;
    end
    else
    begin
      Showmessage('����� ��� �������.');
    end;
  end;
  {
  If (CommonSet.CashNum<0)and(CommonSet.SpecChar=1) then    //������ ����������
  begin
    try
      CommonSet.CashNum:=(-1)*CommonSet.CashNum; //������ � ���������� �����
      fmMessTxt:=tfmMessTxt.Create(Application);
      fmMessTxt.Button1.Enabled:=False;
      fmMessTxt.Button2.Enabled:=False;
      fmMessTxt.Show;
      with fmMessTxt do
      begin
        Memo1.Lines.Add('������������� ��.');
        if CommonSet.CashPort>0 then
        begin
          StrWk:='COM'+IntToStr(CommonSet.CashPort);
          if CashOpen(PChar(StrWk)) then   //dll �������
          begin
            Memo1.Lines.Add('���������� - Ok');

            Nums.bOpen:=False;
            if CheckOpen then CheckCancel;
            Nums.iRet:=InspectSt(Nums.sRet);
            Memo1.Lines.Add(Nums.sRet);

            if Nums.iRet=0 then Nums.bOpen:=True
            else
            begin
              if MessageDlg('��� ���������� ������ ���������� ������� �����. �������� ��������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
              begin
                if OpenZ then
                begin
                  ZClose;//��� �������� ����� � �������� ������� �� �����
                  acRecalcCount.Execute; //����� currest
                end;
              end;
            end;

//             Nums.bOpen:=True;  //��������
            if Nums.bOpen then
            begin
              Memo1.Lines.Add('����� ... ���� ��������� ������ c ��.');
              CashDate(Nums.sDate);
              GetNums; //������ �����
              GetRes; //������� � ����� �����
              WriteStatus;

              Memo1.Lines.Add('�� - Ok');
              Memo1.Lines.Add('  ������������ ������.');
              with dmC do
              begin
                taFis.Active:=False;
                taFis.ParamByName('IST').AsInteger:=CommonSet.Station;
                taFis.Active:=True;
                Memo1.Lines.Add('  ������������ ��.');
                Memo1.Lines.Add('    ����� ... ���� ������.');

                taFis.First;
                while not taFis.Eof do
                begin
                  if abs(taFisRSUM.AsFloat)>=0.005 then
                  begin
                      //��������� ���
                    if Pos('Sale',taFisOPERTYPE.AsString)>0 then CheckStart;
                    if Pos('Ret',taFisOPERTYPE.AsString)>0 then CheckRetStart;

                    while TestStatus('CheckStart',sMessage)=False do
                    begin
                      fmAttention.Label1.Caption:=sMessage;
                      prWriteLog('~~AttentionShow;'+sMessage);
                      fmAttention.ShowModal;
                      Nums.iRet:=InspectSt(Nums.sRet);
                    end;

                    PosCh.Name:='����� 1';
                    PosCh.Code:=taFisID_TAB.AsString;
                    PosCh.AddName:='';
                    PosCh.Price:=RoundEx(abs(taFisRSUM.AsFloat)*100);
                    PosCh.Count:=1000;
                    PosCh.Sum:=abs(taFisRSUM.AsFloat)*100;

                    CheckAddPos(iSum);

                    while TestStatus('CheckAddPos',sMessage)=False do
                    begin
                      fmAttention.Label1.Caption:=sMessage;
                      prWriteLog('~~AttentionShow;'+sMessage);
                      fmAttention.ShowModal;
                      Nums.iRet:=InspectSt(Nums.sRet);
                    end;

                       //������������ ���������� - ������ ����, ������, ������;
                    CheckTotal(iSum);

                    while TestStatus('CheckTotal',sMessage)=False do
                    begin
                      fmAttention.Label1.Caption:=sMessage;
                      prWriteLog('~~AttentionShow;'+sMessage);
                      fmAttention.ShowModal;
                      Nums.iRet:=InspectSt(Nums.sRet);
                    end;

                    if pos('Bank',taFisOPERTYPE.AsString)>0 then
                    begin
                      iSumBN:=0;
                      iSumR:=RoundEx(abs(taFisRSUM.AsFloat)*100);
                      taFisBN.Active:=False;
                      taFisBN.ParamByName('IDH').AsInteger:=taFisID_TAB.AsInteger;
                      taFisBN.Active:=True;

                      if taFisBN.RecordCount>0 then iSumBN:=RoundEx(abs(taFisBNTABSUM.AsFloat)*100);
                      taFisBN.Active:=False;

                      if iSumBN>iSumR then iSumBN:=iSumR;
                      if iSumBN>0 then CheckRasch(1,iSumBN,'',iSum); //������
                      if (iSumR-iSumBN)>0 then CheckRasch(0,(iSumR-iSumBN),'',iSum); //������
                    end
                    else CheckRasch(0,RoundEx(abs(taFisRSUM.AsFloat)*100),'',iSum); //���

                    while TestStatus('CheckRasch',sMessage)=False do
                    begin
                      fmAttention.Label1.Caption:=sMessage;
                      prWriteLog('~~AttentionShow;'+sMessage);
                      fmAttention.ShowModal;
                      Nums.iRet:=InspectSt(Nums.sRet);
                    end;

                    CheckClose;
                    while TestStatus('CheckClose',sMessage)=False do
                    begin
                      fmAttention.Label1.Caption:=sMessage;
                      prWriteLog('~~AttentionShow;'+sMessage);
                      fmAttention.ShowModal;
                      Nums.iRet:=InspectSt(Nums.sRet);
                    end;
                  end;

                  quSetPrint.ParamByName('IDH').AsInteger:=taFisID_TAB.AsInteger;
                  quSetPrint.ExecQuery;

                  taFis.Next;
                end;
                taFis.Active:=False;

              end;
              Memo1.Lines.Add('    ������ ���������.');
              Memo1.Lines.Add('  �������� �����.');

              if OpenZ then
              begin
                ZClose;//��� �������� ����� � �������� ������� �� �����
                acRecalcCount.Execute; //����� currest
              end;
              Memo1.Lines.Add('  ������������ ��.');

            end else
            begin
              Memo1.Lines.Add('������ ��.');
            end;
          end else Memo1.Lines.Add('�� - ������ ������������� ����������.');
        end;
      end;

      Delay(1000);
      fmMessTxt.Close;
    finally
      CommonSet.CashNum:=(-1)*CommonSet.CashNum;
      fmMessTxt.Release;
    end;
  end;
  }
  If ((CommonSet.CashNum>0)and(CommonSet.SpecChar=1))or((CommonSet.CashNum<0)and(CommonSet.SpecChar=0)) then //������������
  begin  //��� ������������ ������ �������
    with dmC do
    with dmC1 do
    begin
      iNumSm:=Nums.ZNum;
      iCountDoc:=Nums.iCheckNum;

      //������� ���, ������
      rSumNal:=0;
      rSumBNal:=0;

      quRSum.Active:=False;
      quRSum.ParamByName('ICASHNUM').AsInteger:=CommonSet.CashNum;
      quRSum.ParamByName('IZNUM').AsInteger:=iNumSm;
      quRSum.Active:=True;
      quRSum.First;
      while not quRSum.Eof do
      begin
        if quRSumPAYTYPE.AsInteger=0 then rSumNal:=quRSumSUMR.AsFloat;
        if quRSumPAYTYPE.AsInteger>0 then rSumBNal:=rSumBNal+quRSumSUMR.AsFloat;
        quRSum.Next;
      end;
      quRSum.Active:=False;

      //�������� ���, ������
      rSumRet:=0;
      rSumBRet:=0;
      quRSumRet.Active:=False;
      quRSumRet.ParamByName('ICASHNUM').AsInteger:=CommonSet.CashNum;
      quRSumRet.ParamByName('IZNUM').AsInteger:=iNumSm;
      quRSumRet.Active:=True;
      quRSumRet.First;
      while not quRSumRet.Eof do
      begin
        if quRSumRetPAYTYPE.AsInteger=0 then rSumRet:=quRSumRetSUMR.AsFloat;
        if quRSumRetPAYTYPE.AsInteger>0 then rSumBRet:=rSumBRet+quRSumRetSUMR.AsFloat;
        quRSumRet.Next;
      end;
      quRSumRet.Active:=False;

      rSumRet:=(-1)*rSumRet;
      rSumBRet:=(-1)*rSumBRet;

      iMax:=0;
      iMin:=0;
      quMaxMin.Active:=False;
      quMaxMin.ParamByName('ICASHNUM').AsInteger:=CommonSet.CashNum;
      quMaxMin.ParamByName('IZNUM').AsInteger:=iNumSm;
      quMaxMin.Active:=True;
      if quMaxMin.RecordCount>0 then
      begin
        quMaxMin.First;
        iMin:=quMaxMinIBEG.AsInteger;
        iMax:=quMaxMinIEND.AsInteger;
      end;
      quMaxMin.Active:=False;

      rSumDisc:=0;
      if iMax>0 then
      begin
        rSumDisc:=0;
      end else
      begin
        quDSum.Active:=False;
        quDSum.ParamByName('IMIN').AsInteger:=iMin;
        quDSum.ParamByName('IMAX').AsInteger:=iMax;
        quDSum.ParamByName('ISTATION').AsInteger:=CommonSet.Station;
        quDSum.Active:=True;
        if quDSum.RecordCount>0 then
        begin
          quDSum.First;
          rSumDisc:=quDSumDSUM.AsFloat;
        end;
      end;

          //������� ��������
      if (Pos('COM',CommonSet.SpecBoxX)>0)or(Pos('LPT',CommonSet.SpecBoxX)>0) then
      begin
        try
          l:=40;
          prDevOpen(CommonSet.SpecBoxX,0);
          StrP:=CommonSet.SpecBoxX;
          prSetFont(StrP,10,0);
          prPrintStr(StrP,SpecVal.SPH1);
          prPrintStr(StrP,SpecVal.SPH2);
          prPrintStr(StrP,SpecVal.SPH3);
          prPrintStr(StrP,SpecVal.SPH4);
          prPrintStr(StrP,SpecVal.SPH5);
          StrWk:=IntToStr(iMax+1); //����� ���������
          while Length(StrWk)<5 do StrWk:='0'+StrWk;
//          prPrintStr(StrP,'�������� N: '+CommonSet.CashSer+'         N ���: '+StrWk);   ������� ������������ - ��������� �� ����
          prPrintStr(StrP,FormatDateTime('dd-mm-yyyy                         hh:nn',now));
          prPrintStr(StrP,' ');
          StrWk:='            Z-����� N '+IntToStr(iNumSm);
          prPrintStr(StrP,StrWk);
          prPrintStr(StrP,' ');
          prPrintStr(StrP,'��������:');
          prPrintStr(StrP,prDefFormatStr1(l,'�������',rSumNal));
          prPrintStr(StrP,prDefFormatStr1(l,'�����. �������',0));
          prPrintStr(StrP,prDefFormatStr1(l,'�������',rSumRet));
          prPrintStr(StrP,' ');
          prPrintStr(StrP,'������:');
          prPrintStr(StrP,prDefFormatStr1(l,'�������',0));
          prPrintStr(StrP,prDefFormatStr1(l,'�����. �������',0));
          prPrintStr(StrP,' ');
          prPrintStr(StrP,'����. �����:');
          prPrintStr(StrP,prDefFormatStr1(l,'�������',rSumBNal));
          prPrintStr(StrP,prDefFormatStr1(l,'�����. �������',rSumBRet));
          prPrintStr(StrP,' ');
          prPrintStr(StrP,'����. �����2:');
          prPrintStr(StrP,prDefFormatStr1(l,'�������',0));
          prPrintStr(StrP,prDefFormatStr1(l,'�����. �������',0));
          prPrintStr(StrP,' ');
          prPrintStr(StrP,'����. �����3:');
          prPrintStr(StrP,prDefFormatStr1(l,'�������',0));
          prPrintStr(StrP,prDefFormatStr1(l,'�����. �������',0));
          prPrintStr(StrP,' ');
          prPrintStr(StrP,'����. �����4:');
          prPrintStr(StrP,prDefFormatStr1(l,'�������',0));
          prPrintStr(StrP,prDefFormatStr1(l,'�����. �������',0));
          prPrintStr(StrP,' ');

          //���� ������
          quSumTalons.Active:=False;
          quSumTalons.ParamByName('IMIN').AsInteger:=iMin;
          quSumTalons.ParamByName('IMAX').AsInteger:=iMax;
          quSumTalons.ParamByName('ISTATION').AsInteger:=CommonSet.Station;
          quSumTalons.Active:=True;
          if quSumTalons.RecordCount>0 then
          begin
            quSumTalons.First;
            while not quSumTalons.Eof do
            begin
              StrWk:=Copy(quSumTalonsNAME.AsString,1,15);
              while Length(StrWk)<15 do StrWk:=StrWk+' ';
              StrWk1:=its(Trunc(RoundEx(quSumTalonsQUANT.AsFloat)));
              while Length(StrWk1)<4 do StrWk1:=' '+StrWk1;
              StrWk:=StrWk+' x'+StrWk1;

              prPrintStr(StrP,prDefFormatStr1(l,StrWk,quSumTalonsRSUM.AsFloat));
              quSumTalons.Next;
            end;
          end;
          quSumTalons.Active:=False;
          prPrintStr(StrP,' ');

          prPrintStr(StrP,'================= ����� ================');
          prPrintStr(StrP,prDefFormatStr1(l,'�������',rSumNal+rSumBNal));
          prPrintStr(StrP,prDefFormatStr1(l,'� �.�. ����:',0));
          prPrintStr(StrP,prDefFormatStr1(l,' ������',rSumDisc));
          prPrintStr(StrP,prDefFormatStr1(l,'�����. �������',0));
          prPrintStr(StrP,prDefFormatStr1(l,'� �.�. ����:',0));
          prPrintStr(StrP,prDefFormatStr1(l,'�������',rSumRet+rSumBRet));
          prPrintStr(StrP,prDefFormatStr1(l,'� �.�. ����:',0));
          prPrintStr(StrP,prDefFormatStr1(l,'������������:',0));
          prPrintStr(StrP,prDefFormatStr1(l,'����������:',0));
          prPrintStr(StrP,' ');
          prPrintStr(StrP,prDefFormatStr2(l,'��������� ����������:',iCountDoc+4));
          prPrintStr(StrP,prDefFormatStr2(l,'� �.�. �����:',iCountDoc));
          prPrintStr(StrP,prDefFormatStr2(l,'� �.�. �����. �����:',0));
          prPrintStr(StrP,prDefFormatStr2(l,'� �.�. ������. ���. ���. �����:',0));
          prPrintStr(StrP,' ');
          prPrintStr(StrP,prDefFormatStr2(l,'�������:',iCountDoc));
          prPrintStr(StrP,prDefFormatStr2(l,'�����. �������:',0));
          prPrintStr(StrP,prDefFormatStr2(l,'�������:',0));
          prPrintStr(StrP,prDefFormatStr2(l,'������������:',0));
          prPrintStr(StrP,prDefFormatStr2(l,'����������:',0));

          prCutDoc(StrP,0);
          delay(CommonSet.ComDelay*5);
        finally
          prDevClose(StrP,0);
        end;
      end; //if Pos('COM',CommonSet.SpecBoxX)>0 then
      if Pos('fis',CommonSet.SpecBoxX)>0 then
      begin
        TestPrint:=1;
        While PrintQu and (TestPrint<=MaxDelayPrint) do
        begin
//        showmessage('������� �����, ��������� �������.');
          prWriteLog('������������� ������ ����� � �������.');
          delay(1000);
          inc(TestPrint);
        end;
        try
          PrintFCheck:=True; //��� ���� � ������� ������� ��������� ������

          l:=40;
          if CommonSet.SpecBoxX='fisshtrih' then l:=32;

          OpenNFDoc;

          PrintNFStr(SpecVal.SPH1);
          PrintNFStr(SpecVal.SPH2);
          PrintNFStr(SpecVal.SPH3);
          PrintNFStr(SpecVal.SPH4);
          PrintNFStr(SpecVal.SPH5);
          StrWk:=IntToStr(iMax+1); //����� ���������
          while Length(StrWk)<5 do StrWk:='0'+StrWk;
//          PrintNFStr('�������� N: '+CommonSet.CashSer+' N ���: '+StrWk);  ������� ������������ - ��������� �� ����
          PrintNFStr(FormatDateTime('dd-mm-yyyy                 hh:nn',now));
          PrintNFStr('����� � '+IntToStr(CommonSet.CashNum));
          PrintNFStr(' ');
          StrWk:='            Z-����� N '+IntToStr(iNumSm);
          PrintNFStr(StrWk);
          PrintNFStr(' ');
          PrintNFStr('��������:');
          PrintNFStr(prDefFormatStr1(l,'�������',rSumNal));
          PrintNFStr(prDefFormatStr1(l,'�����. �������',0));
          PrintNFStr(prDefFormatStr1(l,'�������',rSumRet));
          PrintNFStr(' ');
          PrintNFStr('������:');
          PrintNFStr(prDefFormatStr1(l,'�������',0));
          PrintNFStr(prDefFormatStr1(l,'�����. �������',0));
          PrintNFStr(' ');
          PrintNFStr('����. �����:');
          PrintNFStr(prDefFormatStr1(l,'�������',rSumBNal));
          PrintNFStr(prDefFormatStr1(l,'�����. �������',rSumBRet));
          PrintNFStr(' ');
          PrintNFStr('����. �����2:');
          PrintNFStr(prDefFormatStr1(l,'�������',0));
          PrintNFStr(prDefFormatStr1(l,'�����. �������',0));
          PrintNFStr(' ');
          PrintNFStr('����. �����3:');
          PrintNFStr(prDefFormatStr1(l,'�������',0));
          PrintNFStr(prDefFormatStr1(l,'�����. �������',0));
          PrintNFStr(' ');
          PrintNFStr('����. �����4:');
          PrintNFStr(prDefFormatStr1(l,'�������',0));
          PrintNFStr(prDefFormatStr1(l,'�����. �������',0));
          PrintNFStr(' ');
          PrintNFStr('================= ����� ================');
          PrintNFStr(prDefFormatStr1(l,'�������',rSumNal+rSumBNal));
          PrintNFStr(prDefFormatStr1(l,'� �.�. ����:',0));
          PrintNFStr(prDefFormatStr1(l,' ������',rSumDisc));
          PrintNFStr(prDefFormatStr1(l,'�����. �������',0));
          PrintNFStr(prDefFormatStr1(l,'� �.�. ����:',0));
          PrintNFStr(prDefFormatStr1(l,'�������',rSumRet+rSumBRet));
          PrintNFStr(prDefFormatStr1(l,'� �.�. ����:',0));
          PrintNFStr(prDefFormatStr1(l,'������������:',0));
          PrintNFStr(prDefFormatStr1(l,'����������:',0));
          PrintNFStr(' ');
          PrintNFStr(prDefFormatStr2(l,'��������� ����������:',iCountDoc+4));
          PrintNFStr(prDefFormatStr2(l,'� �.�. �����:',iCountDoc));
          PrintNFStr(prDefFormatStr2(l,'� �.�. �����. �����:',0));
          PrintNFStr(prDefFormatStr2(l,'� �.�. ������. ���. ���. �����:',0));
          PrintNFStr(' ');
          PrintNFStr(prDefFormatStr2(l,'�������:',iCountDoc));
          PrintNFStr(prDefFormatStr2(l,'�����. �������:',0));
          PrintNFStr(prDefFormatStr2(l,'�������:',0));
          PrintNFStr(prDefFormatStr2(l,'������������:',0));
          PrintNFStr(prDefFormatStr2(l,'����������:',0));

          PrintNFStr(' '); PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' '); //  PrintNFStr(' ');  PrintNFStr(' ');

          CloseNFDoc;

          CutDoc;
        finally
          PrintFCheck:=False; //��� ���� � ������� ������� ��������� ������
        end;
      end; //if Pos('fis',CommonSet.SpecBoxX)>0 then
    end;
  end;

  If (CommonSet.CashNum<0)or(CommonSet.SpecChar=1)or(CommonSet.TypeFis='sp802') then
  begin
    inc(CommonSet.CashZ);
    inc(Nums.ZNum);
    CommonSet.CashChNum:=0;
    Nums.iCheckNum:=0;
    CommonSet.PreCheckNum:=1;
    if CommonSet.IncNumTab>0 then CommonSet.IncNumTab:=1;//��������� ������� ������� ������
    WriteCheckNum;
    Label4.Caption:='����� �'+IntToStr(CommonSet.CashZ)+'  ��� � '+IntToStr(CommonSet.CashChNum);
  end;

  if Prizma then
  begin
    prWriteLog('!!GetZ');
    Event_RegEx(210,0,0,0,'','',0,0,0);
  end;

end;

procedure TfmMainFF.cxButton9Click(Sender: TObject);
begin
  if not CanDo('prRetCheck') then exit;

  if Operation =0 then Operation:=1
  else Operation := 0;

  Case Operation of
  0,2: begin
       Label5.Caption:='�������� - ������� ��������.';
     end;
  1: begin
       Label5.Caption:='�������� - �������.';
     end;
  end;
end;

procedure TfmMainFF.acCashPCardExecute(Sender: TObject);
//Var StrWk:String;
Var rSum,rSumD:Real;
    StrWk,StrWk1,StrP:String;
    kDelay:Integer;
    rBalans,rBalansDay,DLimit:Real;
    CliName,CliType:String;
begin
  if not CanDo('prPrintBNCheck') then exit;
  with dmC do
  begin
    if quCurSpec.RecordCount>0 then
    begin
      fmDiscount_Shape.cxTextEdit1.Text:='';
      fmDiscount_Shape.Caption:='��������� ��������� �����';
      fmDiscount_Shape.Label1.Caption:='��������� ��������� �����';

      fmDiscount_Shape.cxButton13.Visible:=True;
      fmDiscount_Shape.cxCheckBox1.Checked:=False;
      fmDiscount_Shape.cxCheckBox1.Enabled:=True;
      fmDiscount_Shape.cxCheckBox1.Visible:=True;

      if not CanDo('prPrintPCCheck') then
      begin
        fmDiscount_Shape.cxButton13.Visible:=False;
        fmDiscount_Shape.cxCheckBox1.Checked:=True;
        fmDiscount_Shape.cxCheckBox1.Enabled:=False;
      end;

      fmDiscount_Shape.ShowModal;
      if fmDiscount_Shape.ModalResult=mrOk then
      begin

        //������� ��������� ������ - ��������� ����� - �.�. �� ������
//      showmessage('');
        if fmDiscount_Shape.cxCheckBox1.Checked=False then
        begin

        if fmDiscount_Shape.cxTextEdit1.Text>'' then
        begin
          DiscountBar:=SOnlyDigit(fmDiscount_Shape.cxTextEdit1.Text);
          If FindPCard(DiscountBar) then
          begin
            delay(20);
            bPrintCheck:=True; //������� ������ ����

            FormLog('SalePC',IntToStr(Person.Id)+' '+quTabsID_Personal.AsString+' '+quTabsNUMTABLE.AsString);

            if quCurSpec.RecordCount=0 then exit;

            rSum:=0;
            rSumD:=0;

            ViewSpec.BeginUpdate;
            quCurSpec.First;
            while not quCurSpec.Eof do
            begin
              rSum:=rSum+quCurSpecSUMMA.AsFloat;
              rSumD:=rSumD+quCurSpecDSUM.AsFloat;
              quCurSpec.Next;
            end;
            quCurSpec.First;
//------------------

//        Tab.PBar:=sBar;
//        Tab.PName:=quPCardCLINAME.AsString;

            //������ �������� �� ��
            if CommonSet.PrePrintPort='fisprim' then
            begin
              TestPrint:=1;
              While PrintQu and (TestPrint<=MaxDelayPrint) do
              begin
//        showmessage('������� �����, ��������� �������.');
                prWriteLog('������������� ������ ����� � �������.');
                delay(1000);
                inc(TestPrint);
              end;
              try
                PrintFCheck:=True; //��� ���� � ������� ������� ��������� ������
                OpenNFDoc;
                SelectF(13); PrintNFStr(' '+CommonSet.DepartName);

                if CommonSet.PreLine>'' then
                begin
                  if fDifStr(1,CommonSet.PreLine)>'' then PrintNFStr(fDifStr(1,CommonSet.PreLine));
                  if fDifStr(2,CommonSet.PreLine)>'' then PrintNFStr(fDifStr(2,CommonSet.PreLine));
                end;

                PrintNFStr('');

                SelectF(14); PrintNFStr('    ������ �'+IntToStr(Tab.iNumZ));
                SelectF(13); PrintNFStr(''); PrintNFStr('');
//                PrintNFStr('��������: '+Tab.Name); PrintNFStr('����: '+Tab.NumTable);
//                SelectF(3); PrintNFStr('������: '+IntToStr(Tab.Quests));

                if Tab.DBar>'' then
                begin
                  Str(Tab.DPercent:5:2,StrWk);
                  PrintNFStr('�����: '+Tab.DName+' ('+StrWk+'%)');
                end;
//                PrintNFStr('������: '+IntToStr(Tab.Quests));
                PrintNFStr('������: '+FormatDateTime('dd.mm.yyyy hh:nn:ss',Tab.OpenTime));

                SelectF(3); StrWk:='                                                       ';
                PrintNFStr(StrWk);
                SelectF(15); StrWk:=' ��������          ���-��   ����   ����� ';
                PrintNFStr(StrWk);
                SelectF(3); StrWk:='                                                       ';
                PrintNFStr(StrWk);

                quCurModAll.Active:=False;
                quCurModAll.ParamByName('IDT').AsInteger:=Tab.Id;
                quCurModAll.Active:=True;

                quCurSpec.First;
                while not quCurSpec.Eof do
                begin
                  StrWk:= Copy(quCurSpecName.AsString,1,29);
                  while Length(StrWk)<29 do StrWk:=StrWk+' ';
                  Str(quCurSpecQuantity.AsFloat:5:1,StrWk1);
                  StrWk:=StrWk+' '+StrWk1;
                  Str(quCurSpecPrice.AsFloat:7:2,StrWk1);
                  StrWk:=StrWk+' '+StrWk1;
                  Str(quCurSpecSumma.AsFloat:8:2,StrWk1);
                  StrWk:=StrWk+' '+StrWk1+'���';
                  SelectF(13);
                  PrintNFStr(StrWk);

                  while (quCurModAllID_POS.AsInteger<quCurSpecID.AsInteger) and (quCurModAll.Eof=False) do quCurModAll.Next;
                  while (quCurModAllID_POS.AsInteger=quCurSpecID.AsInteger) and (quCurModAll.Eof=False) do
                  begin
                    if quCurModSIFR.AsInteger>0 then   //� ��������� ������ 0
                    begin
                      StrWk:=Copy(quCurModAllNAME.AsString,1,29);
                      StrWk:='   '+StrWk;
                      SelectF(13);
                      PrintNFStr(StrWk);
                    end;
                    quCurModAll.Next;
                  end;

                  quCurSpec.Next;
                end;

                SelectF(3); StrWk:='                                                       ';
                PrintNFStr(StrWk);
                SelectF(10); PrintNFStr(' ');
                Str(rSum:8:2,StrWk1);
                StrWk:=' �����                      '+StrWk1+' ���';
                SelectF(15); PrintNFStr(StrWk);

                if rSumD>0.02 then
                begin
                  SelectF(3);
                  StrWk:='                                                       ';
                  PrintNFStr(StrWk);

                  Str(rSumD:8:2,StrWk);
                  rSumD:=RoundEx((rSumD/(rSum+rSumD)*100)*100)/100;
                  Str(rSumD:5:2,StrWk1);
                  StrWk:=' � �.�. ������ - '+StrWk1+'%  '+StrWk+'�.';

                  SelectF(10); PrintNFStr(StrWk);
                end;

                StrWk:='�������� ��.������ - '+Copy(Tab.PName,1,19);
                SelectF(15); PrintNFStr(StrWk);

                if CommonSet.LastLine>'' then
                begin
                  PrintNFStr(' ');
                  if fDifStr(1,CommonSet.LastLine)>'' then PrintNFStr(fDifStr(1,CommonSet.LastLine));
                  if fDifStr(2,CommonSet.LastLine)>'' then PrintNFStr(fDifStr(2,CommonSet.LastLine));
                end;

                PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');
                CloseNFDoc;
                CutDoc;
              finally
                PrintFCheck:=False; //��� ���� � ������� ������� ��������� ������
              end;
            end;

            if (pos('COM',CommonSet.PrePrintPort)>0)or(pos('LPT',CommonSet.PrePrintPort)>0) then //������ ������������ ����� �� ��������� �������
            begin
              TestPrint:=1;
              While PrintQu and (TestPrint<=MaxDelayPrint) do
              begin
//        showmessage('������� �����, ��������� �������.');
                prWriteLog('������������� ������ ����� � �������.');
                delay(1000);
                inc(TestPrint);
              end;
              try
                PrintFCheck:=True; //��� ���� � ������� ������� ��������� ������

                BufPr.iC:=0;

                StrP:=CommonSet.PrePrintPort;
                prDevOpen(CommonSet.PrePrintPort,0);
                PrintStrDev(StrP,' '+CommonSet.DepartName);

                SelFont(0);

                if CommonSet.PreLine>'' then
                begin
                  if fDifStr(1,CommonSet.PreLine)>'' then PrintStrDev(StrP,fDifStr(1,CommonSet.PreLine));
                  if fDifStr(2,CommonSet.PreLine)>'' then PrintStrDev(StrP,fDifStr(2,CommonSet.PreLine));
                end;

                PrintStrDev(StrP,'');

                PrintStrDev(StrP,'    ������ �'+IntToStr(CommonSet.CashChNum));
                PrintStrDev(StrP,''); PrintStrDev(StrP,'');

                if Tab.DBar>'' then
                begin
                  Str(Tab.DPercent:5:2,StrWk);
                  PrintStrDev(StrP,'�����: '+Tab.DName+' ('+StrWk+'%)');
                end;
//                PrintStrDev(StrP,'������: '+IntToStr(Tab.Quests));
                PrintStrDev(StrP,'������: '+FormatDateTime('dd.mm.yyyy hh:nn:ss',Tab.OpenTime));

                StrWk:='-------------------------------------------------------';
                PrintStrDev(StrP,StrWk);
                StrWk:=' ��������          ���-��   ����   ����� ';
                PrintStrDev(StrP,StrWk);
                StrWk:='-------------------------------------------------------';
                PrintStrDev(StrP,StrWk);

                quCurModAll.Active:=False;
                quCurModAll.ParamByName('IDT').AsInteger:=Tab.Id;
                quCurModAll.Active:=True;

                quCurSpec.First;
                while not quCurSpec.Eof do
                begin
                  StrWk:= Copy(quCurSpecName.AsString,1,29);
                  while Length(StrWk)<29 do StrWk:=StrWk+' ';
                  Str(quCurSpecQuantity.AsFloat:5:1,StrWk1);
                  StrWk:=StrWk+' '+StrWk1;
                  Str(quCurSpecPrice.AsFloat:7:2,StrWk1);
                  StrWk:=StrWk+' '+StrWk1;
                  Str(quCurSpecSumma.AsFloat:8:2,StrWk1);
                  StrWk:=StrWk+' '+StrWk1+'���';
                  PrintStrDev(StrP,StrWk);

                  while (quCurModAllID_POS.AsInteger<quCurSpecID.AsInteger) and (quCurModAll.Eof=False) do quCurModAll.Next;
                  while (quCurModAllID_POS.AsInteger=quCurSpecID.AsInteger) and (quCurModAll.Eof=False) do
                  begin
                    if quCurModSIFR.AsInteger>0 then   //� ��������� ������ 0
                    begin
                      StrWk:=Copy(quCurModAllNAME.AsString,1,29);
                      StrWk:='   '+StrWk;
                      PrintStrDev(StrP,StrWk);
                    end;
                    quCurModAll.Next;
                  end;

                  quCurSpec.Next;
                end;

                StrWk:='-------------------------------------------------------';
                PrintStrDev(StrP,StrWk);
                PrintStrDev(StrP,' ');
                Str(rSum:8:2,StrWk1);
                StrWk:=' �����                      '+StrWk1+' ���';
                PrintStrDev(StrP,StrWk);

                if rSumD>0.02 then
                begin
                  StrWk:='-------------------------------------------------------';
                  PrintStrDev(StrP,StrWk);

                  Str(rSumD:8:2,StrWk);
                  rSumD:=RoundEx((rSumD/(rSum+rSumD)*100)*100)/100;
                  Str(rSumD:5:2,StrWk1);
                  StrWk:=' � �.�. ������ - '+StrWk1+'%  '+StrWk+'�.';

                  PrintStrDev(StrP,StrWk);
                end;

                StrWk:='�������� ��.������ - '+Copy(Tab.PName,1,19);
                PrintStrDev(StrP,StrWk);

                if CommonSet.LastLine>'' then
                begin
                  PrintStrDev(StrP,' ');
                  if fDifStr(1,CommonSet.LastLine)>'' then PrintStrDev(StrP,fDifStr(1,CommonSet.LastLine));
                  if fDifStr(2,CommonSet.LastLine)>'' then PrintStrDev(StrP,fDifStr(2,CommonSet.LastLine));
                end;

                kDelay:=(BufPr.iC div 40)-10;
                if kDelay<0 then kDelay:=0;

                prWrBuf(StrP);
                delay(CommonSet.ComDelay*5+trunc(kDelay*CommonSet.ComDelay/2));
                prCutDoc(StrP,0);   //}

              finally
                PrintFCheck:=False; //��� ���� � ������� ������� ��������� ������
                prDevClose(StrP,0);
//                inc(CommonSet.CashChNum);
//                WriteCheckNum;
              end;
            end;



//------------------

            ViewSpec.EndUpdate;

            inc(CommonSet.CashChNum); //����� �������� 1-�� � ����
            WriteCheckNum;

            Operation:=2;    // SalePC  ������� �� ��������� �����
            prSaveSpec(rSum);   // ��������� ������������ � ������� ������ �����

            //������ ������� ��� � ������������
            prClearCur.ParamByName('ID_TAB').AsInteger:=Tab.Id;
            prClearCur.ParamByName('ID_STATION').AsInteger:=CommonSet.Station;
            prClearCur.ExecProc;

            //��������� �� ������
            quCurSpecMod.Active:=False;
            quCurSpecMod.ParamByName('IDT').AsInteger:=Tab.Id;
            quCurSpecMod.ParamByName('STATION').AsInteger:=CommonSet.Station;
            quCurSpecMod.Active:=True;

            quCurSpec.Active:=False;
            quCurSpec.ParamByName('IDT').AsInteger:=Tab.Id;
            quCurSpec.ParamByName('STATION').AsInteger:=CommonSet.Station;
            quCurSpec.Active:=True;
            quCurSpec.Last;

            //�������������� �� ���������
            ViewSpecDPROC.Visible:=False;
            ViewSpecDSum.Visible:=False;
            DiscountBar:='';
            PDiscountBar:='';
            Label7.Caption:='';
            Tab.DBar:='';
            Tab.DPercent:=0;
            Tab.DName:='';
            Operation:=0;
            cEdit1.Value:=0;

            WriteStatus;

            if cEdit1.Tag>0 then
            begin
//       ������ ����
              if abs(rSum-Tab.Summa)<0.01 then
              begin
                quDelTab.Active:=False;
                quDelTab.ParamByName('Id').AsInteger:=cEdit1.Tag;

                trDel.StartTransaction;
                quDelTab.Active:=True;
                trDel.Commit;
              end;
              cEdit1.Tag:=0;

              Tab.Id_Personal:=Person.Id;
              Tab.Name:='';
              Tab.NumTable:='FF';
              Tab.DBar:='';
              Tab.DBar1:='';
              Tab.Id:=0;
              Tab.Summa:=0;
              Tab.Quests:=1;
            end;

            bPrintCheck:=False; //������� ������ ����

          end else
          begin
            //��������� ������� ��������� ������
            FormLog('BadPC',IntToStr(Person.Id)+' '+quTabsID_Personal.AsString+' '+quTabsNUMTABLE.AsString);
          end;
        end;
        end else //�������� ������
        begin
          if fmDiscount_Shape.cxTextEdit1.Text>'' then
          begin
            DiscountBar:=SOnlyDigit(fmDiscount_Shape.cxTextEdit1.Text);
            prFindPCardBalans(DiscountBar,rBalans,rBalansDay,DLimit,CliName,CliType);

            Showmessage('     ��������  '+CliName+#$0D+'     ���  '+CliType+#$0D+'     ������  '+fts(rv(rBalans))+#$0D+'     ������ �������  '+fts(rv(rBalansDay))+#$0D+'     ������� �����  '+fts(rv((-1)*DLimit)));
          end;
        end;
      end;
    end;
  end;
end;

procedure TfmMainFF.cxButton11Click(Sender: TObject);
begin
// cxButton11.Enabled:=False;
// cxButton3.Enabled:=False;
  Button5.Enabled:=False;
  ViewSpec.BeginUpdate;
  with dmC do
  begin
    taServP.Active:=False; //�������� ��� ������ �����
    taServP.CreateDataSet;

    taCurMod.Active:=False;   //��������� �������� ���� �� ���� �� ������������
    taCurMod.ParamByName('IDT').AsInteger:=Tab.Id;
    taCurMod.ParamByName('STATION').AsInteger:=CommonSet.Station;
    taCurMod.Active:=True;

    quCurSpec.First;
    while not quCurSpec.Eof do
    begin      //��������� ������� ������ ���, ������ ��� ��� ����
      //�������� ��� ������ �����
      if quCurSpecIStatus.AsInteger=0 then
      begin
        taServP.Append;
        taServPName.AsString:=quCurSpecName.AsString;
        taServPCode.AsString:=quCurSpecSifr.AsString;
        taServPQuant.AsFloat:=quCurSpecQuantity.AsFloat;
        taServPStream.AsInteger:=quCurSpecStream.AsInteger;
        taServPiType.AsInteger:=0; //�����
        taServP.Post;

        taCurMod.First;
        while not taCurMod.Eof do
        begin
          if taCurModID_POS.AsInteger=quCurSpecID.AsInteger then
          begin //��� �������
            taServP.Append;
            taServPName.AsString:=quCurModAllNAME.AsString;
            taServPCode.AsString:='';
            taServPQuant.AsFloat:=0;
            taServPStream.AsInteger:=quCurSpecStream.AsInteger;
            taServPiType.AsInteger:=1; //�����������
            taServP.Post;
          end;
          taCurMod.Next;
        end;
      end;

      quCurSpec.Edit;
      quCurSpecIStatus.AsInteger:=1; //���� �� ���� �������� ��� ��� ����������.
      quCurSpec.Post;

      quCurSpec.Next;
    end;

    if Operation in [0,2] then PrintServCh('�����');// ���� ������� (1) - �� ������� ������ �� ����

    taCurMod.Active:=False;   //��������� �������� ���� �� ���� �� ������������
  end;
  ViewSpec.EndUpdate;
  Button5.Enabled:=True;
//  cxButton11.Enabled:=True;
//  cxButton3.Enabled:=True;
end;

procedure TfmMainFF.cxButton12Click(Sender: TObject);
begin
  if not CanDo('prSberRep') then begin Showmessage('��� ����.'); exit; end;
  if CommonSet.BNManual=2 then
  begin //��� ����
    try
      iMoneyBn:=0; //� ��������
      fmSber.Label1.Visible:=False;
      fmSber.cxButton9.Enabled:=True;
      fmSber.cxButton10.Enabled:=True;
      fmSber.cxButton11.Enabled:=True;
      fmSber.cxButton6.Enabled:=True;

      fmSber.cxButton2.Enabled:=False;
      fmSber.cxButton3.Enabled:=False;
      fmSber.cxButton4.Enabled:=False;
      fmSber.cxButton5.Enabled:=False;
      fmSber.cxButton7.Enabled:=False;
      fmSber.ShowModal;
    finally
//      fmSber.Release;
    end;
  end;
  if CommonSet.BNManual=3 then //���
  begin //��� ����
    try
      SberTabStop:=3;

      iMoneyBn:=0; //� ��������
      fmVTB.Label1.Visible:=False;

      fmVTB.cxButton9.Enabled:=True;
      fmVTB.cxButton9.TabStop:=True;
      fmVTB.cxButton9.TabOrder:=1;
      fmVTB.cxButton9.Default:=True;
      fmVTB.cxButton9.Default:=True;

      fmVTB.cxButton6.Enabled:=True;
      fmVTB.cxButton6.TabOrder:=4;
      fmVTB.cxButton6.TabStop:=True;

      fmVTB.cxButton2.Enabled:=False;
      fmVTB.cxButton2.TabStop:=False;

      fmVTB.cxButton8.Enabled:=False;
      fmVTB.cxButton8.TabStop:=False;

      fmVTB.cxButton13.Enabled:=True;
      fmVTB.cxButton13.TabOrder:=5;
      fmVTB.cxButton13.TabStop:=True;

      fmVTB.cxButton1.TabOrder:=6;

      BnStr:='';

      fmVTB.ShowModal;
    finally

    end;
  end;
end;

procedure TfmMainFF.acTabsExecute(Sender: TObject);
Var DSUM:Real;
begin
// ������
  if Button5.Enabled=False then exit;

  with fmTabsFF do
  begin
    Panel1.Visible:=False;
    Panel2.Visible:=True;
    with dmC do
    begin
      ViewMove.BeginUpdate;
      quTabspers.Active:=False;
      quTabsPers.SelectSQL.Clear;
      quTabsPers.SelectSQL.Add('SELECT ID, ID_PERSONAL, NUMTABLE, QUESTS, TABSUM, DISCONT, BEGTIME, ISTATUS, NUMZ');
      quTabsPers.SelectSQL.Add('FROM TABLES');
//      quTabsPers.SelectSQL.Add('Where Id_personal='+IntToStr(Tab.Id_Personal));
//      quTabsPers.SelectSQL.Add('and ISTATUS=0'); //������ ������c��� � ������ �� ��� ��������� ���� - �������
//      quTabsPers.SelectSQL.Add('and ID<>'+IntToStr(Tab.Id)); //������ ������c��� � ����
      quTabsPers.SelectSQL.Add('ORDER BY ID_PERSONAL, NUMTABLE');
      quTabspers.Active:=True;
      ViewMove.EndUpdate;
    end;
  end;
  fmTabsFF.ShowModal;
  if fmTabsFF.ModalResult=mrOk then
  begin
    cEdit1.Tag:=0;
    with dmC do
    begin
      if quTabsPers.RecordCount>0 then
      begin

        Tab.Id_Personal:=quTabsPersID_PERSONAL.AsInteger;

        quFindPers.Active:=False;
        quFindPers.ParamByName('IDP').AsInteger:=Tab.Id_Personal;
        quFindPers.Active:=True;
        Tab.Name:=quFindPersNAME.AsString;
        quFindPers.Active:=False;

        Tab.OpenTime:=quTabsPersBEGTIME.AsDateTime;
        Tab.NumTable:=quTabsPersNUMTABLE.AsString;
        Tab.Quests:=quTabsPersQUESTS.AsInteger;
        Tab.iStatus:=quTabsPersISTATUS.AsInteger;
        Tab.DBar:=quTabsPersDISCONT.AsString;
        Tab.DBar1:=quTabsPersDISCONT.AsString;
        Tab.Id:=quTabsPersID.AsInteger;
        Tab.Summa:=quTabsPersTABSUM.AsFloat;

        prWriteLog('--OpenTab;'+IntToStr(Tab.Id)+';'+Tab.NumTable+';'+Person.Name+';'+IntToStr(Tab.Id_Personal)+';');

        ViewSpec.BeginUpdate;

        prClearCur.ParamByName('ID_TAB').AsInteger:=Tab.Id;
        prClearCur.ParamByName('ID_STATION').AsInteger:=CommonSet.Station;
        prClearCur.ExecProc;

        prOpenTab.ParamByName('ID_TAB').AsInteger:=Tab.Id;
        prOpenTab.ParamByName('ID_STATION').AsInteger:=CommonSet.Station;
        prOpenTab.ExecProc;

        DSUM:=RoundEx(prOpenTab.ParamByName('DSUMALL').AsDouble*100)/100;

        quCurSpecMod.Active:=False;
        quCurSpecMod.ParamByName('IDT').AsInteger:=Tab.Id;
        quCurSpecMod.ParamByName('STATION').AsInteger:=CommonSet.Station;
        quCurSpecMod.Active:=True;

        quCurSpec.Active:=False;
        quCurSpec.ParamByName('IDT').AsInteger:=Tab.Id;
        quCurSpec.ParamByName('STATION').AsInteger:=CommonSet.Station;
        quCurSpec.Active:=True;
        quCurSpec.Last;

        ViewSpec.EndUpdate;

        while not quCurSpec.Bof do
        begin
          ViewSpec.Controller.FocusedRow.Expand(True);
          quCurSpec.Prior;
//          delay(10);
        end;

        prWriteLog('--SelSpec;'+IntToStr(Tab.Id)+';'+Tab.NumTable+';'+Person.Name+';'+IntToStr(Tab.Id_Personal)+';'+IntToStr(Check.Max)+';');

//        rSum:=0;
        quCurSpec.Last;
        
{        quCurSpec.First;
        while not quCurSpec.Eof do
        begin
          rSum:=rSum+quCurSpecSUMMA.AsFloat;
          prWriteLog('-----pos;'+IntToStr(quCurSpecID_TAB.AsInteger)+';'+IntToStr(quCurSpecID.AsInteger)+';'+IntTostr(quCurSpecID_PERSONAL.AsInteger)+';'+quCurSpecNAME.AsString+';'+IntToStr(quCurSpecSIFR.AsInteger)+';'+FloatToStr(quCurSpecSUMMA.AsFloat)+';');
          quCurSpec.Next;
        end;
        prWriteLog('--EndSpec;'+IntToStr(Tab.Id)+';'+Tab.NumTable+';'+Person.Name+';'+IntToStr(Tab.Id_Personal)+';'+IntToStr(Check.Max)+';');
}
        if DSum<>0 then
        begin
          ViewSpecDPROC.Visible:=True;
          ViewSpecDSum.Visible:=True;
        end
        else
        begin
          ViewSpecDPROC.Visible:=False;
          ViewSpecDSum.Visible:=False;
        end;
//        ViewSpec.EndUpdate;

        cEdit1.Value:=Tab.Summa;
        cEdit1.Tag:=Tab.Id;

//        cxButton3.Enabled:=False;
//        cxButton4.Enabled:=False;
//        cxButton6.Enabled:=False;
//        cxButton11.Enabled:=False;
//        cxButton21.Enabled:=False;
      end;
    end;
  end;
end;

procedure TfmMainFF.TimerDelayPrintQTimer(Sender: TObject);
begin
  TimerDelayPrintQ.Enabled:=False;
  prWriteLogPS('������ ������� ������ ��������.('+INtToStr(CommonSet.iDelay div 1000)+'�)');
  TimerPrintQ.Interval:=CommonSet.iDelay;
  TimerPrintQ.Enabled:=True;
end;

procedure TfmMainFF.TimerPrintQTimer(Sender: TObject);
begin
  TimerPrintQ.Enabled:=False;
  try
    if PrintFCheck then prWriteLogPS('----- stop')
    else prWriteLogPS('----- go');
    if not PrintFCheck then
    begin
      PrintQu:=True;
      try
        dmC.prPrint;
      finally
        PrintQu:=False;
      end;
    end;
  finally
    TimerPrintQ.Enabled:=True;
  end;
end;

procedure TfmMainFF.cxButton14Click(Sender: TObject);
Var StrWk,StrWk1:String;
    iNumSm,iCountDoc,iMin,iMax:Integer;
    rSumNal,rSumRet,rSumBNal,rSumBRet,rSumDisc:Real;
    l:ShortInt;
    StrP:String;
begin
  if not CanDo('prZRep') then begin Showmessage('��� ����.'); exit; end;

  prWriteLog('!!ZRepCopy;'+IntToStr(Person.Id)+';'+Person.Name+';');

  with dmC do
  with dmC1 do
  begin
    fmCalc.CalcEdit1.EditValue:=Nums.ZNum-1;
    fmCalc.ShowModal;
    if fmCalc.ModalResult=mrOk then
    begin
      delay(10);
      iNumSm:=RoundEx(fmCalc.CalcEdit1.EditValue);


      If ((CommonSet.CashNum>0)and(CommonSet.SpecChar=1))or((CommonSet.CashNum<0)and(CommonSet.SpecChar=0)) then //������������
      begin  //��� ������������ ������ �������

        iCountDoc:=1000; //�������� ����

        //������� ���, ������
        rSumNal:=0;
        rSumBNal:=0;

        quRSum.Active:=False;
        quRSum.ParamByName('ICASHNUM').AsInteger:=CommonSet.CashNum;
        quRSum.ParamByName('IZNUM').AsInteger:=iNumSm;
        quRSum.Active:=True;
        quRSum.First;
        while not quRSum.Eof do
        begin
          if quRSumPAYTYPE.AsInteger=0 then rSumNal:=quRSumSUMR.AsFloat;
          if quRSumPAYTYPE.AsInteger>0 then rSumBNal:=rSumBNal+quRSumSUMR.AsFloat;
          quRSum.Next;
        end;
        quRSum.Active:=False;

        //�������� ���, ������
        rSumRet:=0;
        rSumBRet:=0;
        quRSumRet.Active:=False;
        quRSumRet.ParamByName('ICASHNUM').AsInteger:=CommonSet.CashNum;
        quRSumRet.ParamByName('IZNUM').AsInteger:=iNumSm;
        quRSumRet.Active:=True;
        quRSumRet.First;
        while not quRSumRet.Eof do
        begin
          if quRSumRetPAYTYPE.AsInteger=0 then rSumRet:=quRSumRetSUMR.AsFloat;
          if quRSumRetPAYTYPE.AsInteger>0 then rSumBRet:=rSumBRet+quRSumRetSUMR.AsFloat;
          quRSumRet.Next;
        end;
        quRSumRet.Active:=False;

        rSumRet:=(-1)*rSumRet;
        rSumBRet:=(-1)*rSumBRet;

        iMax:=0;
        iMin:=0;
        quMaxMin.Active:=False;
        quMaxMin.ParamByName('ICASHNUM').AsInteger:=CommonSet.CashNum;
        quMaxMin.ParamByName('IZNUM').AsInteger:=iNumSm;
        quMaxMin.Active:=True;
        if quMaxMin.RecordCount>0 then
        begin
          quMaxMin.First;
          iMin:=quMaxMinIBEG.AsInteger;
          iMax:=quMaxMinIEND.AsInteger;
        end;
        quMaxMin.Active:=False;

        rSumDisc:=0;
        if iMax>0 then
        begin
          rSumDisc:=0;
        end else
        begin
          quDSum.Active:=False;
          quDSum.ParamByName('IMIN').AsInteger:=iMin;
          quDSum.ParamByName('IMAX').AsInteger:=iMax;
          quDSum.ParamByName('ISTATION').AsInteger:=CommonSet.Station;
          quDSum.Active:=True;
          if quDSum.RecordCount>0 then
          begin
            quDSum.First;
            rSumDisc:=quDSumDSUM.AsFloat;
          end;
        end;

        if Pos('fis',CommonSet.SpecBoxX)>0 then
        begin
          TestPrint:=1;
          While PrintQu and (TestPrint<=MaxDelayPrint) do
          begin
//          showmessage('������� �����, ��������� �������.');
            prWriteLog('������������� ������ ����� � �������.');
            delay(1000);
            inc(TestPrint);
          end;
          try
            PrintFCheck:=True; //��� ���� � ������� ������� ��������� ������

            l:=40;
            if CommonSet.SpecBoxX='fisshtrih' then l:=32;

            OpenNFDoc;

            PrintNFStr(SpecVal.SPH1);
            PrintNFStr(SpecVal.SPH2);
            PrintNFStr(SpecVal.SPH3);
            PrintNFStr(SpecVal.SPH4);
            PrintNFStr(SpecVal.SPH5);
            StrWk:=IntToStr(iMax+1); //����� ���������
            while Length(StrWk)<5 do StrWk:='0'+StrWk;
//            PrintNFStr('�������� N: '+CommonSet.CashSer+' N ���: '+StrWk);  ������� ������������ - ��������� �� ����
            PrintNFStr(FormatDateTime('dd-mm-yyyy                 hh:nn',now));
            PrintNFStr('����� � '+IntToStr(CommonSet.CashNum));
            PrintNFStr(' ');
            StrWk:='            Z-����� N '+IntToStr(iNumSm);
            PrintNFStr(StrWk);
            PrintNFStr(' ');
            PrintNFStr('��������:');
            PrintNFStr(prDefFormatStr1(l,'�������',rSumNal));
            PrintNFStr(prDefFormatStr1(l,'�����. �������',0));
            PrintNFStr(prDefFormatStr1(l,'�������',rSumRet));
            PrintNFStr(' ');
            PrintNFStr('������:');
            PrintNFStr(prDefFormatStr1(l,'�������',0));
            PrintNFStr(prDefFormatStr1(l,'�����. �������',0));
            PrintNFStr(' ');
            PrintNFStr('����. �����:');
            PrintNFStr(prDefFormatStr1(l,'�������',rSumBNal));
            PrintNFStr(prDefFormatStr1(l,'�����. �������',rSumBRet));
            PrintNFStr(' ');
            PrintNFStr('����. �����2:');
            PrintNFStr(prDefFormatStr1(l,'�������',0));
            PrintNFStr(prDefFormatStr1(l,'�����. �������',0));
            PrintNFStr(' ');
            PrintNFStr('����. �����3:');
            PrintNFStr(prDefFormatStr1(l,'�������',0));
            PrintNFStr(prDefFormatStr1(l,'�����. �������',0));
            PrintNFStr(' ');
            PrintNFStr('����. �����4:');
            PrintNFStr(prDefFormatStr1(l,'�������',0));
            PrintNFStr(prDefFormatStr1(l,'�����. �������',0));
            PrintNFStr(' ');
            PrintNFStr('================= ����� ================');
            PrintNFStr(prDefFormatStr1(l,'�������',rSumNal+rSumBNal));
            PrintNFStr(prDefFormatStr1(l,'� �.�. ����:',0));
            PrintNFStr(prDefFormatStr1(l,' ������',rSumDisc));
            PrintNFStr(prDefFormatStr1(l,'�����. �������',0));
            PrintNFStr(prDefFormatStr1(l,'� �.�. ����:',0));
            PrintNFStr(prDefFormatStr1(l,'�������',rSumRet+rSumBRet));
            PrintNFStr(prDefFormatStr1(l,'� �.�. ����:',0));
            PrintNFStr(prDefFormatStr1(l,'������������:',0));
            PrintNFStr(prDefFormatStr1(l,'����������:',0));
            PrintNFStr(' ');
            PrintNFStr(prDefFormatStr2(l,'��������� ����������:',iCountDoc+4));
            PrintNFStr(prDefFormatStr2(l,'� �.�. �����:',iCountDoc));
            PrintNFStr(prDefFormatStr2(l,'� �.�. �����. �����:',0));
            PrintNFStr(prDefFormatStr2(l,'� �.�. ������. ���. ���. �����:',0));
            PrintNFStr(' ');
            PrintNFStr(prDefFormatStr2(l,'�������:',iCountDoc));
            PrintNFStr(prDefFormatStr2(l,'�����. �������:',0));
            PrintNFStr(prDefFormatStr2(l,'�������:',0));
            PrintNFStr(prDefFormatStr2(l,'������������:',0));
            PrintNFStr(prDefFormatStr2(l,'����������:',0));

            PrintNFStr(' '); PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');// PrintNFStr(' ');

            CloseNFDoc;

            CutDoc;
          finally
            PrintFCheck:=False; //��� ���� � ������� ������� ��������� ������
          end;
        end; //if Pos('fis',CommonSet.SpecBoxX)>0 then

            //������� ��������
      if (Pos('COM',CommonSet.SpecBoxX)>0)or(Pos('LPT',CommonSet.SpecBoxX)>0) then
      begin
        try
          l:=40;
          prDevOpen(CommonSet.SpecBoxX,0);
          StrP:=CommonSet.SpecBoxX;
          prSetFont(StrP,10,0);
          prPrintStr(StrP,SpecVal.SPH1);
          prPrintStr(StrP,SpecVal.SPH2);
          prPrintStr(StrP,SpecVal.SPH3);
          prPrintStr(StrP,SpecVal.SPH4);
          prPrintStr(StrP,SpecVal.SPH5);
          StrWk:=IntToStr(iMax+1); //����� ���������
          while Length(StrWk)<5 do StrWk:='0'+StrWk;
//          prPrintStr(StrP,'�������� N: '+CommonSet.CashSer+'         N ���: '+StrWk);   ������� ������������ - ��������� �� ����
          prPrintStr(StrP,FormatDateTime('dd-mm-yyyy                         hh:nn',now));
          prPrintStr(StrP,'����� � '+IntToStr(CommonSet.CashNum));
          prPrintStr(StrP,' ');
          StrWk:='            Z-����� N '+IntToStr(iNumSm);
          prPrintStr(StrP,StrWk);
          prPrintStr(StrP,' ');
          prPrintStr(StrP,'��������:');
          prPrintStr(StrP,prDefFormatStr1(l,'�������',rSumNal));
          prPrintStr(StrP,prDefFormatStr1(l,'�����. �������',0));
          prPrintStr(StrP,prDefFormatStr1(l,'�������',rSumRet));
          prPrintStr(StrP,' ');
          prPrintStr(StrP,'������:');
          prPrintStr(StrP,prDefFormatStr1(l,'�������',0));
          prPrintStr(StrP,prDefFormatStr1(l,'�����. �������',0));
          prPrintStr(StrP,' ');
          prPrintStr(StrP,'����. �����:');
          prPrintStr(StrP,prDefFormatStr1(l,'�������',rSumBNal));
          prPrintStr(StrP,prDefFormatStr1(l,'�����. �������',rSumBRet));
          prPrintStr(StrP,' ');
          prPrintStr(StrP,'����. �����2:');
          prPrintStr(StrP,prDefFormatStr1(l,'�������',0));
          prPrintStr(StrP,prDefFormatStr1(l,'�����. �������',0));
          prPrintStr(StrP,' ');
          prPrintStr(StrP,'����. �����3:');
          prPrintStr(StrP,prDefFormatStr1(l,'�������',0));
          prPrintStr(StrP,prDefFormatStr1(l,'�����. �������',0));
          prPrintStr(StrP,' ');
          prPrintStr(StrP,'����. �����4:');
          prPrintStr(StrP,prDefFormatStr1(l,'�������',0));
          prPrintStr(StrP,prDefFormatStr1(l,'�����. �������',0));
          prPrintStr(StrP,' ');

          //���� ������
          quSumTalons.Active:=False;
          quSumTalons.ParamByName('IMIN').AsInteger:=iMin;
          quSumTalons.ParamByName('IMAX').AsInteger:=iMax;
          quSumTalons.ParamByName('ISTATION').AsInteger:=CommonSet.Station;
          quSumTalons.Active:=True;
          if quSumTalons.RecordCount>0 then
          begin
            quSumTalons.First;
            while not quSumTalons.Eof do
            begin
              StrWk:=Copy(quSumTalonsNAME.AsString,1,15);
              while Length(StrWk)<15 do StrWk:=StrWk+' ';
              StrWk1:=its(Trunc(RoundEx(quSumTalonsQUANT.AsFloat)));
              while Length(StrWk1)<4 do StrWk1:=' '+StrWk1;
              StrWk:=StrWk+' x'+StrWk1;

              prPrintStr(StrP,prDefFormatStr1(l,StrWk,quSumTalonsRSUM.AsFloat));
              quSumTalons.Next;
            end;
          end;
          quSumTalons.Active:=False;
          prPrintStr(StrP,' ');

          prPrintStr(StrP,'================= ����� ================');
          prPrintStr(StrP,prDefFormatStr1(l,'�������',rSumNal+rSumBNal));
          prPrintStr(StrP,prDefFormatStr1(l,'� �.�. ����:',0));
          prPrintStr(StrP,prDefFormatStr1(l,' ������',rSumDisc));
          prPrintStr(StrP,prDefFormatStr1(l,'�����. �������',0));
          prPrintStr(StrP,prDefFormatStr1(l,'� �.�. ����:',0));
          prPrintStr(StrP,prDefFormatStr1(l,'�������',rSumRet+rSumBRet));
          prPrintStr(StrP,prDefFormatStr1(l,'� �.�. ����:',0));
          prPrintStr(StrP,prDefFormatStr1(l,'������������:',0));
          prPrintStr(StrP,prDefFormatStr1(l,'����������:',0));
          prPrintStr(StrP,' ');
          prPrintStr(StrP,prDefFormatStr2(l,'��������� ����������:',iCountDoc+4));
          prPrintStr(StrP,prDefFormatStr2(l,'� �.�. �����:',iCountDoc));
          prPrintStr(StrP,prDefFormatStr2(l,'� �.�. �����. �����:',0));
          prPrintStr(StrP,prDefFormatStr2(l,'� �.�. ������. ���. ���. �����:',0));
          prPrintStr(StrP,' ');
          prPrintStr(StrP,prDefFormatStr2(l,'�������:',iCountDoc));
          prPrintStr(StrP,prDefFormatStr2(l,'�����. �������:',0));
          prPrintStr(StrP,prDefFormatStr2(l,'�������:',0));
          prPrintStr(StrP,prDefFormatStr2(l,'������������:',0));
          prPrintStr(StrP,prDefFormatStr2(l,'����������:',0));

          prCutDoc(StrP,0);
          delay(CommonSet.ComDelay*5);
        finally
          prDevClose(StrP,0);
        end;
      end; //if Pos('COM',CommonSet.SpecBoxX)>0 then


      end;
    end;
  end;
end;

procedure TfmMainFF.cxButton15Click(Sender: TObject);
begin
  with dmC do
  begin
//    if not quSaleT.Eof then quSaleT.Next else quSaleT.First;
//    cxButton15.Caption:='��� ������';
    quSaleT.Next;
    if quSaleT.Eof then quSaleT.First;
    cxButton15.Caption:=Copy(quSaleTNAMECS.AsString,1,15);
  end;
end;

procedure TfmMainFF.TimerTrTimer(Sender: TObject);
begin
  TimerTr.Enabled:=False;
{  with dmTr do
  begin
    if prOpenTr then
    begin
      Label11.Caption:='����� �� ('+formatDateTime('hh:nn:sss',now)+')';
      prGetMenu;
      prWriteReal;
      prCloseTr;
    end else
      Label11.Caption:='����� ��� ('+formatDateTime('hh:nn:sss',now)+')';

  end;}
  
  Label11.Caption:='����� �� ('+formatDateTime('hh:nn:sss',now)+')';
  TimerTr.Enabled:=True;
end;

procedure TfmMainFF.prGetMenu;
begin
  with dmTr do
  with dmC do
  begin
    try
    quUpd1.ExecQuery;
    delay(33);
    quTr.Active:=False;
    quTr.ParamByName('ST').AsInteger:=CommonSet.Station;
    quTr.Active:=True;
    quTr.First;
    while not quTr.Eof do
    begin
      quMenuId.Active:=False;
      quMenuId.ParamByName('SIFR').AsInteger:=quTrSIFR.AsInteger;
      quMenuId.Active:=True;

      quMenuId.First;

      if quMenuId.RecordCount=1 then
      begin
        quMenuId.Edit;
      end else
      begin
        quMenuId.Append;
        quMenuIdSIFR.AsInteger:=quTrSIFR.AsInteger;
      end;

      quMenuIdNAME.AsString:=quTrNAME.AsString;
      quMenuIdPRICE.AsFloat:=quTrPRICE.AsFloat;
      quMenuIdCODE.AsString:=quTrCODE.AsString;
      quMenuIdTREETYPE.AsString:=quTrTREETYPE.AsString;
      quMenuIdLIMITPRICE.AsFloat:=quTrLIMITPRICE.AsFloat;
      quMenuIdCATEG.AsInteger:=quTrCATEG.AsInteger;
      quMenuIdPARENT.AsInteger:=quTrPARENT.AsInteger;
      quMenuIdLINK.AsInteger:=quTrLINK.AsInteger;
      quMenuIdSTREAM.AsInteger:=quTrSTREAM.AsInteger;
      quMenuIdLACK.AsInteger:=quTrLACK.AsInteger;
      quMenuIdDESIGNSIFR.AsInteger:=quTrDESIGNSIFR.AsInteger;
      quMenuIdALTNAME.AsString:=quTrALTNAME.AsString;
      quMenuIdNALOG.AsFloat:=quTrNALOG.AsFloat;
      quMenuIdBARCODE.AsString:=quTrBARCODE.AsString;
      quMenuIdIMAGE.AsInteger:=quTrIMAGE.AsInteger;
      quMenuIdCONSUMMA.AsFloat:=quTrCONSUMMA.AsFloat;
      quMenuIdMINREST.AsInteger:=quTrMINREST.AsInteger;
      quMenuIdPRNREST.AsInteger:=quTrPRNREST.AsInteger;
      quMenuIdCOOKTIME.AsInteger:=quTrCOOKTIME.AsInteger;
      quMenuIdDISPENSER.AsInteger:=quTrDISPENSER.AsInteger;
      quMenuIdDISPKOEF.AsInteger:=quTrDISPKOEF.AsInteger;
      quMenuIdACCESS.AsInteger:=quTrACCESS.AsInteger;
      quMenuIdFLAGS.AsInteger:=quTrFLAGS.AsInteger;
      quMenuIdTARA.AsInteger:=quTrTARA.AsInteger;
      quMenuIdCNTPRICE.AsInteger:=quTrCNTPRICE.AsInteger;
      quMenuIdBACKBGR.AsFloat:=quTrBACKBGR.AsFloat;
      quMenuIdFONTBGR.AsFloat:=quTrFONTBGR.AsFloat;
      quMenuIdIACTIVE.AsInteger:=quTrIACTIVE.AsInteger;
      quMenuIdIEDIT.AsInteger:=quTrIEDIT.AsInteger;
      quMenuIdDATEB.AsInteger:=quTrDATEB.AsInteger;
      quMenuIdDATEE.AsInteger:=quTrDATEE.AsInteger;
      quMenuIdDAYWEEK.AsString:=quTrDAYWEEK.AsString;
      quMenuIdTIMEB.AsDateTime:=quTrTIMEB.AsDateTime;
      quMenuIdTIMEE.AsDateTime:=quTrTIMEE.AsDateTime;
      quMenuIdALLTIME.AsInteger:=quTrALLTIME.AsInteger;
      quMenuId.Post;

      quMenuId.Active:=False;

      quTr.Next;
    end;

    quUpd2.ExecQuery;

    delay(33);

    quTr.Active:=False;
    except
    end;
  end;

end;

procedure TfmMainFF.prWriteReal;
Var IDH:INteger;
begin
  with dmTr do
  with dmC do
  begin
    try
    quUpd1T.ExecQuery;    //cashsail
    delay(33);

    quTA.Active:=False;
    quTA.Active:=True;
    quTA.First;
    while not quTA.Eof do
    begin
      prTRADDTABLES.ParamByName('ID_PERSONAL').AsInteger:=quTAID_PERSONAL.AsInteger;
      prTRADDTABLES.ParamByName('NUMTABLE').AsString:=quTANUMTABLE.AsString;
      prTRADDTABLES.ParamByName('QUESTS').AsInteger:=quTAQUESTS.AsInteger;
      prTRADDTABLES.ParamByName('TABSUM').AsFloat:=quTATABSUM.AsFloat;
      prTRADDTABLES.ParamByName('BEGTIME').AsDateTime:=quTABEGTIME.AsDateTime;
      prTRADDTABLES.ParamByName('ENDTIME').AsDateTime:=quTAENDTIME.AsDateTime;
      prTRADDTABLES.ParamByName('DISCONT').AsString:=quTADISCONT.AsString;
      prTRADDTABLES.ParamByName('OPERTYPE').AsString:=quTAOPERTYPE.AsString;
      prTRADDTABLES.ParamByName('CHECKNUM').AsInteger:=quTACHECKNUM.AsInteger;
      prTRADDTABLES.ParamByName('SKLAD').AsInteger:=quTASKLAD.AsInteger;
      prTRADDTABLES.ParamByName('SPBAR').AsString:=quTADISCONT1.AsString;
      prTRADDTABLES.ParamByName('STATION').AsInteger:=CommonSet.Station;
      prTRADDTABLES.ParamByName('NUMZ').AsInteger:=quTANUMZ.AsInteger;
      prTRADDTABLES.ParamByName('SALET').AsInteger:=quTASALET.AsInteger;
      prTRADDTABLES.ExecProc;
      delay(33);
      IDH:=prTRADDTABLES.ParamByName('ID_H').AsInteger;
      if IDH>0 then
      begin
        quTAS.Active:=False;
        quTAS.ParamByName('IDH').AsInteger:=quTAID.AsInteger;
        quTAS.Active:=True;
        quTAS.First;
        while not quTAS.Eof do
        begin
          prTRADDSPECT.ParamByName('ID').AsInteger:=quTASID.AsInteger;
          prTRADDSPECT.ParamByName('ID_TAB').AsInteger:=IDH;
          prTRADDSPECT.ParamByName('ID_PERSONAL').AsInteger:=quTASID_PERSONAL.AsInteger;
          prTRADDSPECT.ParamByName('NUMTABLE').AsString:=quTASNUMTABLE.AsString;
          prTRADDSPECT.ParamByName('SIFR').AsInteger:=quTASSIFR.AsInteger;
          prTRADDSPECT.ParamByName('PRICE').AsFloat:=quTASPRICE.AsFloat;
          prTRADDSPECT.ParamByName('QUANTITY').AsFloat:=quTASQUANTITY.AsFloat;
          prTRADDSPECT.ParamByName('DISCOUNTPROC').AsFloat:=quTASDISCOUNTPROC.AsFloat;
          prTRADDSPECT.ParamByName('DISCOUNTSUM').AsFloat:=quTASDISCOUNTSUM.AsFloat;
          prTRADDSPECT.ParamByName('SUMMA').AsFloat:=quTASSUMMA.AsFloat;
          prTRADDSPECT.ParamByName('ISTATUS').AsInteger:=quTASISTATUS.AsInteger;
          prTRADDSPECT.ParamByName('ITYPE').AsInteger:=quTASITYPE.AsInteger;
          prTRADDSPECT.ParamByName('QUANTITY1').AsFloat:=quTASQUANTITY1.AsFloat;
          prTRADDSPECT.ParamByName('INEED').AsInteger:=quTASINEED.AsInteger;
          prTRADDSPECT.ParamByName('IPRINT').AsInteger:=quTASIPRINT.AsInteger;
          prTRADDSPECT.ParamByName('STREAM').AsInteger:=quTASSTREAM.AsInteger;
          prTRADDSPECT.ExecProc;

          quTAS.Next;  delay(33);
        end;

        quTAS.Active:=False;

        delay(33);
        quCS.Active:=False;
        quCS.ParamByName('IDH').AsInteger:=quTAID.AsInteger;
        quCS.Active:=True;
        quCS.First;
        while not quCS.Eof do
        begin
          prTRADDCASHSAIL.ParamByName('CASHNUM').AsInteger:=CommonSet.Station;
          prTRADDCASHSAIL.ParamByName('ZNUM').AsInteger:=quCSZNUM.AsInteger;
          prTRADDCASHSAIL.ParamByName('CHECKNUM').AsInteger:=quCSCHECKNUM.AsInteger;
          prTRADDCASHSAIL.ParamByName('TAB_ID').AsInteger:=IDH;
          prTRADDCASHSAIL.ParamByName('TABSUM').AsFloat:=quCSTABSUM.AsFloat;
          prTRADDCASHSAIL.ParamByName('CLIENTSUM').AsFloat:=quCSCLIENTSUM.AsFloat;
          prTRADDCASHSAIL.ParamByName('CASHERID').AsInteger:=quCSCASHERID.AsInteger;
          prTRADDCASHSAIL.ParamByName('WAITERID').AsInteger:=quCSWAITERID.AsInteger;
          prTRADDCASHSAIL.ParamByName('CHDATE').AsDateTime:=quCSCHDATE.AsDateTime;
          prTRADDCASHSAIL.ParamByName('PAYTYPE').AsInteger:=quCSPAYTYPE.AsInteger;
          prTRADDCASHSAIL.ParamByName('PAYID').AsInteger:=quCSPAYID.AsInteger;
          prTRADDCASHSAIL.ParamByName('PAYBAR').AsString:=quCSPAYBAR.AsString;
          prTRADDCASHSAIL.ParamByName('IACTIVE').AsInteger:=1;
          prTRADDCASHSAIL.ExecProc;

          quCS.Next; delay(33);
        end;

        quCS.Active:=False;
      end;
      quTA.Edit;
      quTAIACTIVE.AsInteger:=3;
      quTA.Post;

      quTA.Next;
    end;
    except
    end;
  end;
end;

function TfmMainFF.prOpenTr:Boolean;
begin
  Result:=False;
  with dmTr do
  begin
    try
      TRDb.Connected:=False;
      TrDB.DBName:=DBNameCB;
      TrDB.Connected:=True;
      Result:=True;
    except
    end;
  end;
end;

procedure TfmMainFF.prCloseTr;
begin
  with dmTr do
  begin
    try
      TRDb.Connected:=False;
    except
    end;
  end;
end;


procedure TfmMainFF.cxButton16Click(Sender: TObject);
Var i:INteger;
begin
  if GridHK.Tag=0 then
  begin
    showmessage('�������� ������� ����� ��������� ������ ������.');
    GridHK.Tag:=1;
//    for i:=0 to ViewHK.ColumnCount-1 do ViewHK.Columns[i].Styles.Content:=dmC.cxStyle28;

    for i:=0 to ViewHK.ColumnCount-1 do ViewHK.Columns[i].Styles.Content.Color:=$00CCCCFF;

  end else
  begin
    showmessage('����� ��������� ������ ������ ��������.');
    GridHK.Tag:=0;

    for i:=0 to ViewHK.ColumnCount-1 do ViewHK.Columns[i].Styles.Content.Color:=$00AED2AE;
    
  end;
end;

procedure TfmMainFF.cxButton17Click(Sender: TObject);
begin
  if not CanDo('prCashOff') then begin ShowMessage('��� ���� �� ��������.');  exit;end;
  if dmC.quCurSpec.RecordCount>0 then
  begin
    ShowMessage('��������� ���.');
  end
  else
  begin
    fmShutd.Showmodal;
    if fmShutd.ModalResult=mrOk then
    begin //���������� �����
      CashClose;
      try
        FormLog('ShutdWin','CashNum '+INtToStr(CommonSet.CashNum)+' '+Person.Name);
        zero:=0;
        if not OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES or TOKEN_QUERY, hToken)
        then
        begin
          Exit;
        end; // if not OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES or TOKEN_QUERY, hToken)
        if not OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES or TOKEN_QUERY, hToken)
        then
        begin
          Exit;
        end; // if not OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES or TOKEN_QUERY, hToken)
         // SE_SHUTDOWN_NAME
        if not LookupPrivilegeValue(nil, 'SeShutdownPrivilege' , tkp.Privileges[ 0 ].Luid)
        then
        begin
          Exit;
        end; // if not LookupPrivilegeValue(nil, 'SeShutdownPrivilege' , tkp.Privileges[0].Luid )

        tkp.PrivilegeCount:=1;
        tkp.Privileges[0].Attributes:=SE_PRIVILEGE_ENABLED;
        AdjustTokenPrivileges(hToken, False, tkp, SizeOf(TTokenPrivileges ), tkpo, zero);
        if Boolean(GetLastError()) then
        begin
          Exit;
        end // if Boolean(GetLastError())
        else
        begin
          ExitWindowsEx(EWX_Force or EWX_SHUTDOWN, 0);
          close;
        end;
      except
      end;
    end;
    if fmShutd.ModalResult=mrRetry then
    begin //������������ �����
      CashClose;
      try
        FormLog('RestartWin','CashNum '+INtToStr(CommonSet.CashNum)+' '+Person.Name);
        zero:=0;
        zero:=0;
        if not OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES or TOKEN_QUERY, hToken)
        then
        begin
          Exit;
        end; // if not OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES or TOKEN_QUERY, hToken)
        if not OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES or TOKEN_QUERY, hToken)
        then
        begin
          Exit;
        end; // if not OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES or TOKEN_QUERY, hToken)
         // SE_SHUTDOWN_NAME
        if not LookupPrivilegeValue(nil, 'SeShutdownPrivilege' , tkp.Privileges[ 0 ].Luid)
        then
        begin
          Exit;
        end; // if not LookupPrivilegeValue(nil, 'SeShutdownPrivilege' , tkp.Privileges[0].Luid )

        tkp.PrivilegeCount:=1;
        tkp.Privileges[0].Attributes:=SE_PRIVILEGE_ENABLED;
        AdjustTokenPrivileges(hToken, False, tkp, SizeOf(TTokenPrivileges ), tkpo, zero);
        if Boolean(GetLastError()) then
        begin
          Exit;
        end // if Boolean(GetLastError())
        else
        begin
          ExitWindowsEx(EWX_Force or EWX_REBOOT, 0);
          close;
        end;
      except
      end;
    end;
  end;
end;

procedure TfmMainFF.acBarCExecute(Sender: TObject);
begin
  Edit1.Clear;
  Edit1.SetFocus;
end;

procedure TfmMainFF.Edit1KeyPress(Sender: TObject; var Key: Char);
Var sBar:String;
    bCh:Byte;
    iCode:INteger;
    rQ,rQBar:Real;
    cSum,cSumD:Real;

begin
  bCh:=ord(Key);
  if bCh = 13 then
  begin //���� ����������

    sBar:=SOnlyDigit(Edit1.Text);
    trim(sBar);
    if sBar>'' then
    begin
      iCode:=prFindBarQ(sBar,rQ);
      if iCode>0 then
      begin
        //��� ������, �������
        with dmC1 do
        with dmC do
        begin
          quMenuHK.Active:=False;
          quMenuHK.ParamByName('ISIFR').AsInteger:=iCode;
          qumenuHK.Active:=True;
          if quMenuHK.RecordCount>0 then
          begin
            quMenuHK.First;

            quCurSpec.Last;
            if quCurSpecSifr.AsInteger<>quMenuHKSIFR.AsInteger then
            begin
//              rQ:=1;
              if quMenuHKDESIGNSIFR.AsInteger>0 then   //����������� ���-��
              begin
                fmCalc.CalcEdit1.EditValue:=rQ;
                fmCalc.ShowModal;
                if (fmCalc.ModalResult=mrOk)and(fmCalc.CalcEdit1.EditValue>0.00001)  then
                begin
                  rQ:=RoundEx(fmCalc.CalcEdit1.EditValue*1000)/1000;
                end;
              end;

              CalcDiscontN(quMenuHKSIFR.AsInteger,Check.Max+1,-1,quMenuHKSTREAM.AsInteger,quMenuHKPRICE.AsFloat,rQ,rQ*quMenuHKPRICE.AsFloat,Tab.DBar,Now,Check.DProc,Check.DSum);

              cSum:=RoundVal(quMenuHKPRICE.AsFloat*rQ-Check.DSum);
              cSumD:=RoundVal(quMenuHKPRICE.AsFloat*rQ)-cSum;

              if CommonSet.CashNum<0 then Tab.Id:=CommonSet.CashNum else Tab.Id:=CommonSet.CashNum*(-1);

              quCurSpec.Append;
              quCurSpecSTATION.AsInteger:=CommonSet.Station;
              quCurSpecID_TAB.AsInteger:=Tab.Id;
              quCurSpecId_Personal.AsInteger:=Person.Id;
              quCurSpecNumTable.AsString:=INtToStr(Tab.Id);
              quCurSpecId.AsInteger:=Check.Max+1;
              quCurSpecSifr.AsInteger:=quMenuHKSIFR.AsInteger;
              quCurSpecPrice.AsFloat:=quMenuHKPRICE.AsFloat;
              quCurSpecQuantity.AsFloat:=rQ;
              quCurSpecSumma.AsFloat:=cSum;
              quCurSpecDProc.AsFloat:=Check.DProc;
              quCurSpecDSum.AsFloat:=cSumD;
              quCurSpecIStatus.AsInteger:=0;
              quCurSpecName.AsString:=quMenuHKNAME.AsString;
              quCurSpecCode.AsString:=quMenuHKCODE.AsString;
              quCurSpecLinkM.AsInteger:=quMenuHKLINK.AsInteger;
              if quMenuHKLIMITPRICE.AsFloat>0 then
              quCurSpecLimitM.AsInteger:=RoundEx(quMenuHKLIMITPRICE.AsFloat)
              else quCurSpecLimitM.AsInteger:=99;
              quCurSpecStream.AsInteger:=quMenuHKSTREAM.AsInteger;
              quCurSpec.Post;
              inc(Check.Max);

              if Prizma then
              begin
                prWriteLog('!!AddPos;'+its(Nums.iCheckNum)+';'+its(Check.Max)+';'+its(quCurSpecSIFR.AsInteger)+';'+quCurSpecNAME.AsString+';'+fts(quCurSpecQUANTITY.AsFloat)+';'+fts(quCurSpecSUMMA.AsFloat)+';'+fts(quCurSpecDSUM.AsFloat));
                Event_RegEx(6,Nums.iCheckNum,Check.Max,quCurSpecSIFR.AsInteger,'',quCurSpecNAME.AsString,quCurSpecPRICE.AsFloat,quCurSpecQUANTITY.AsFloat,quCurSpecSUMMA.AsFloat);
              end;

              if quMenuHKLINK.AsInteger>0 then
              begin //������������
                acModify.Execute;
              end;
            end else
            begin
              rQBar:=rQ; //�������� ���-�� �� ���������

              rQ:=quCurSpecQuantity.AsFloat;

              CalcDiscontN(quMenuHKSIFR.AsInteger,Check.Max+1,-1,quMenuHKSTREAM.AsInteger,quMenuHKPRICE.AsFloat,rQ+rQBar,(rQ+rQBar)*quMenuHKPRICE.AsFloat,Tab.DBar,Now,Check.DProc,Check.DSum);

              cSum:=RoundVal(quMenuHKPRICE.AsFloat*(rQ+rQBar)-Check.DSum);
              cSumD:=RoundVal(quMenuHKPRICE.AsFloat*(rQ+rQBar))-cSum;

              quCurSpec.Edit;
              quCurSpecQuantity.AsFloat:=rQ+rQBar;
              quCurSpecSumma.AsFloat:=cSum;
              quCurSpecDProc.AsFloat:=Check.DProc;
              quCurSpecDSum.AsFloat:=cSumD;
              quCurSpec.Post;
            end;
            RecalcSum;
          end;
          quMenuHK.Active:=False;
        end;
      end;
    end;
  end;
end;

procedure TfmMainFF.cxButton18Click(Sender: TObject);
Var StrWk:String;
    rSum,rSumR:Real;
begin
  with dmC do
  begin
    prWriteLog('!!InPutMoney;');

    if Prizma then
    begin
      prWriteLog('!!InPutMoney');
      Event_RegEx(206,0,0,0,'','',0,0,0);
    end;

    GetCashReg(rSumR);
    if rSumR<0 then              //������������ ����� ���� �� �������� - ������� ����� ������
    begin
      fmInOutM.Label8.Caption:='';
    end else
    begin
      Str(rSumR:12:2,StrWk);
      fmInOutM.Label8.Caption:=StrWk;
    end;

    fmInOutM.Caption:='�������� ����� � �����.';

    fmInOutM.ShowModal; // bMoneyCalc:=False - ��� ��������
    if fmInOutM.ModalResult=mrOk then
    begin
      //���� � ���� �������� �����
      rSum:=fmInOutM.CurrencyEdit1.EditValue;
      if rSum<>0 then InCass(rSum);

      try
        OpenNFDoc;
        SelectF(13);
        PrintNFStr(' ');
        PrintNFStr(FormatDateTime('dd.mm.yyyy            hh:nn:ss',Now));
        PrintNFStr(' ');
//        PrintNFStr('������� �����          ����� '+INtToStr(CommonSet.CashNum));
        PrintNFStr('�������� �����        ����� '+INtToStr(CommonSet.CashNum));
        PrintNFStr('���������: '+Person.Name);
        PrintNFStr(' ');
        Str(rSumR:8:2,StrWk);
        PrintNFStr('����� � �����     ���:'+StrWk);
        PrintNFStr(' ');
        Str(rSum:8:2,StrWk);
        PrintNFStr('������� �����     ���:'+StrWk);
        PrintNFStr(' ');
        Str((rSumR+rSum):8:2,StrWk);
        PrintNFStr('�������           ���:'+StrWk);
        PrintNFStr(' ');
        PrintNFStr(' ');
        PrintNFStr('���� _______________________');
        PrintNFStr(' ');
        PrintNFStr(' ');
        PrintNFStr('������ _____________________');

        PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');
        CloseNFDoc;
        CutDoc;
      finally
      end;
    end;
  end;
end;

procedure TfmMainFF.cxButton19Click(Sender: TObject);
Var StrWk:String;
    rSum,rSumR:Real;
begin
  with dmC do
  begin
    prWriteLog('!!OutPutMoney;');

    if Prizma then
    begin
      prWriteLog('!!Inkass');
      Event_RegEx(206,0,0,0,'','',0,0,0);
    end;

    GetCashReg(rSumR);
    if rSumR<0 then              //������������ ����� ���� �� �������� - ������� ����� ������
    begin
      fmInOutM.Label8.Caption:='';
    end else
    begin
      Str(rSumR:12:2,StrWk);
      fmInOutM.Label8.Caption:=StrWk;
    end;

    fmInOutM.Caption:='������� ����� � �����.';

    fmInOutM.ShowModal; // bMoneyCalc:=False - ��� ��������
    if fmInOutM.ModalResult=mrOk then
    begin
      //���� � ���� �������� �����
      rSum:=fmInOutM.CurrencyEdit1.EditValue;
      if rSum<>0 then InCass(rSum*(-1)); //������ ���� ������������� ��� �������

      try
        OpenNFDoc;
        SelectF(13);
        PrintNFStr(' ');
        PrintNFStr(FormatDateTime('dd.mm.yyyy            hh:nn:ss',Now));
        PrintNFStr(' ');
        PrintNFStr('������� �����        ����� '+INtToStr(CommonSet.CashNum));
        PrintNFStr('���������: '+Person.Name);
        PrintNFStr(' ');
        Str(rSumR:8:2,StrWk);
        PrintNFStr('����� � �����     ���:'+StrWk);
        PrintNFStr(' ');
        Str(rSum:8:2,StrWk);
        PrintNFStr('������ �����     ���:'+StrWk);
        PrintNFStr(' ');
        Str((rSumR-rSum):8:2,StrWk);
        PrintNFStr('�������           ���:'+StrWk);
        PrintNFStr(' ');
        PrintNFStr(' ');
        PrintNFStr('���� _______________________');
        PrintNFStr(' ');
        PrintNFStr(' ');
        PrintNFStr('������ _____________________');

        PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');
        CloseNFDoc;
        CutDoc;
      finally
      end;
    end;
  end;
end;

procedure TfmMainFF.cxButton20Click(Sender: TObject);
begin
  fmCopyCh.cxTextEdit1.Text:=its(Nums.ZNum)+','+its(Nums.iCheckNum);
  fmCopyCh.ShowModal;
end;

Procedure TestFp(StrOp:String);
begin
  while TestStatus(StrOp,sMessage)=False do
  begin
    fmAttention.Label1.Caption:=sMessage;
    fmAttention.ShowModal;
    Nums.iRet:=InspectSt(Nums.sRet);
    prWriteLog('~~AttentionShow;'+sMessage+';'+'InspectSt(Nums.sRet)='+IntToStr(Nums.iRet));
  end;
end;


procedure TfmMainFF.cxButton21Click(Sender: TObject);
Var rQ:Real;
    cSum,cSumD:Real;
begin
  if Button5.Enabled=False then exit;

  with dmC do
  begin
    if quCurSpec.RecordCount=0 then exit;

//    rQ:=RoundEx(quCurSpecQuantity.AsFloat/2*1000)/1000;
    rQ:=0.5;

    CalcDiscontN(quCurSpecSIFR.AsInteger,quCurSpecID.AsInteger,-1,quCurSpecSTREAM.AsInteger,quCurSpecPrice.AsFloat,rQ,rQ*quCurSpecPrice.AsFloat,Tab.DBar,Now,Check.DProc,Check.DSum);

    cSum:=RoundVal(quCurSpecPrice.AsFloat*rQ-Check.DSum);
    cSumD:=RoundVal(quCurSpecPrice.AsFloat*rQ)-cSum;

    quCurSpec.Edit;
    quCurSpecQuantity.AsFloat:=rQ;
    quCurSpecDProc.AsFloat:=Check.DProc;
    quCurSpecDSum.AsFloat:=cSumD;
    quCurSpecSumma.AsFloat:=cSum;
    quCurSpec.Post;

    if Prizma then
    begin
      prWriteLog('!!QuantPos;'+its(Nums.iCheckNum)+';'+its(quCurSpecID.AsInteger)+';'+its(quCurSpecSIFR.AsInteger)+';'+quCurSpecNAME.AsString+';'+fts(quCurSpecQUANTITY.AsFloat)+';'+fts(quCurSpecSUMMA.AsFloat)+';'+fts(quCurSpecDSUM.AsFloat));
      Event_RegEx(12,Nums.iCheckNum,quCurSpecID.AsInteger,quCurSpecSIFR.AsInteger,'',quCurSpecNAME.AsString,quCurSpecPRICE.AsFloat,quCurSpecQUANTITY.AsFloat,quCurSpecSUMMA.AsFloat);
    end;

    RecalcSum;
  end;
end;

procedure TfmMainFF.cxButton22Click(Sender: TObject);
begin
  cxSpinEdit1.Value:=cxSpinEdit1.Value+1;
end;

procedure TfmMainFF.cxButton23Click(Sender: TObject);
begin
  if cxSpinEdit1.Value>0 then cxSpinEdit1.Value:=cxSpinEdit1.Value-1;
end;

procedure TfmMainFF.tiPTimer(Sender: TObject);
Var iNumP:INteger;
begin
  tiP.Enabled:=False;
  with dmC do
  begin
    if CasherRnDb.Connected then
    begin
      dsquTabP.DataSet:=nil;
      if quTabP.Active then
      begin
        if quTabP.RecordCount>0 then iNumP:=quTabPID_TAB.AsInteger else iNumP:=0;
        quTabP.FullRefresh;
        if iNumP>0 then quTabP.Locate('ID_TAB',iNumP,[]);
      end else quTabP.Active:=True;

      dsquTabP.DataSet:=quTabP;
    end;
  end;
  tiP.Enabled:=True;
end;

procedure TfmMainFF.ViewPCellDblClick(Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  acOpenP.Execute;
end;

procedure TfmMainFF.acOpenPExecute(Sender: TObject);
Var iNumP:INteger;
    rQ,cSum,cSumD:Real;
begin
  with dmC do
  begin
    if quTabP.RecordCount>0 then
    begin
      iNumP:=quTabPID_TAB.AsInteger;

      with fmAddP do
      begin
        closeTe(fmAddP.teSp);

        fmAddP.Caption:='�������� ������ � '+its(iNumP)+' � ������� ���?';

        ViewSpecP.BeginUpdate;
        try
          quSpecP.Active:=False;
          quSpecP.ParamByName('IDH').AsInteger:=iNumP;
          quSpecP.Active:=True;
          quSpecP.First;
          while not quSpecP.Eof do
          begin
            if quSpecPITYPE.AsInteger=0 then
            begin
              teSp.Append;
              teSpSIFR.AsInteger:=quSpecPSIFR.AsInteger;
              teSpNAME.AsString:=quSpecPNAME.AsString;
              teSpPRICE.AsFloat:=quSpecPPRICE.AsFloat;
              teSpQUANTITY.AsFloat:=quSpecPQUANTITY.AsFloat;
              teSpSUMMA.AsFloat:=quSpecPSUMMA.AsFloat;
              teSp.Post;
            end;
            quSpecP.Next;
          end;
          quSpecP.Active:=False;
        finally
          ViewSpecP.EndUpdate;
        end;
      end;
      fmAddP.ShowModal;
//      if MessageDlg('�������� ������ � '+its(iNumP)+' � ������� ���?',  mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      if fmAddP.ModalResult=mrOk then
      begin
        TimerSdTo0.Enabled:=False;

        quCurSpec.Last;

        quSpecP.Active:=False;
        quSpecP.ParamByName('IDH').AsInteger:=iNumP;
        quSpecP.Active:=True;
        quSpecP.First;
        while not quSpecP.Eof do
        begin
          if quSpecPITYPE.AsInteger=0 then  //�����
          begin
            rQ:=quSpecPQUANTITY.AsFloat;
            CalcDiscontN(quSpecPSIFR.AsInteger,Check.Max+1,-1,quSpecPSTREAM.AsInteger,quSpecPPRICE.AsFloat,rQ,rQ*quSpecPPRICE.AsFloat,Tab.DBar,Now,Check.DProc,Check.DSum);

            cSum:=RoundVal(quSpecPPRICE.AsFloat*rQ-Check.DSum);
            cSumD:=RoundVal(quSpecPPRICE.AsFloat*rQ)-cSum;

            if CommonSet.CashNum<0 then Tab.Id:=CommonSet.CashNum else Tab.Id:=CommonSet.CashNum*(-1);

            quCurSpec.Append;
            quCurSpecSTATION.AsInteger:=CommonSet.Station;
            quCurSpecID_TAB.AsInteger:=Tab.Id;
            quCurSpecId_Personal.AsInteger:=Person.Id;
            quCurSpecNumTable.AsString:=INtToStr(Tab.Id);
            quCurSpecId.AsInteger:=Check.Max+1;
            quCurSpecSifr.AsInteger:=quSpecPSIFR.AsInteger;
            quCurSpecPrice.AsFloat:=quSpecPPRICE.AsFloat;
            quCurSpecQuantity.AsFloat:=rQ;
            quCurSpecSumma.AsFloat:=cSum;
            quCurSpecDProc.AsFloat:=Check.DProc;
            quCurSpecDSum.AsFloat:=cSumD;
            quCurSpecIStatus.AsInteger:=0;
            quCurSpecName.AsString:=quSpecPNAME.AsString;
            quCurSpecCode.AsString:=quSpecPCODE.AsString;
            quCurSpecLinkM.AsInteger:=quSpecPLINKM.AsInteger;
            if quSpecPLIMITM.AsInteger>0 then  quCurSpecLimitM.AsInteger:=quSpecPLIMITM.AsInteger
            else quCurSpecLimitM.AsInteger:=99;
            quCurSpecStream.AsInteger:=quSpecPSTREAM.AsInteger;
            quCurSpec.Post;
            inc(Check.Max);

            if Prizma then
            begin
              prWriteLog('!!AddPos;'+its(Nums.iCheckNum)+';'+its(Check.Max)+';'+its(quCurSpecSIFR.AsInteger)+';'+quCurSpecNAME.AsString+';'+fts(quCurSpecQUANTITY.AsFloat)+';'+fts(quCurSpecSUMMA.AsFloat)+';'+fts(quCurSpecDSUM.AsFloat));
              Event_RegEx(6,Nums.iCheckNum,Check.Max,quCurSpecSIFR.AsInteger,'',quCurSpecNAME.AsString,quCurSpecPRICE.AsFloat,quCurSpecQUANTITY.AsFloat,quCurSpecSUMMA.AsFloat);
            end;

          end else //�����������
          begin
            if quCurSpecMod.Active then
            begin
              quCurSpecMod.Append;
              quCurSpecModSTATION.AsInteger:=CommonSet.Station;
              quCurSpecModID_TAB.AsInteger:=Tab.Id;
              quCurSpecModId_Pos.AsInteger:=quCurSpecID.AsInteger;
              quCurSpecModId.AsInteger:=Check.Max+1;
              quCurSpecModSifr.AsInteger:=quSpecPSIFR.AsInteger;
              quCurSpecModName.AsString:=quSpecPNAME.AsString;
              quCurSpecModQuantity.AsFloat:=quCurSpecQUANTITY.AsFloat;
              quCurSpecMod.Post;

              inc(Check.Max);
            end;
          end;
          quSpecP.Next;
        end;

        quSpecP.First;
        while not quSpecP.Eof do quSpecP.Delete; //���� ��� , ���������, �� ������ ��������

        quTabP.FullRefresh;
        
        RecalcSum;
      end;
    end;
  end;
end;

procedure TfmMainFF.cxButton25Click(Sender: TObject);
Var iNumP:INteger;
begin
  with dmC do
  begin
    if quTabP.RecordCount>0 then
    begin
      iNumP:=quTabPID_TAB.AsInteger;
      if MessageDlg('������� ������ � '+its(iNumP)+'?',  mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        quSpecP.Active:=False;
        quSpecP.ParamByName('IDH').AsInteger:=iNumP;
        quSpecP.Active:=True;
        quSpecP.First;
        while not quSpecP.Eof do quSpecP.Delete; //���� ��� , ���������, �� ������ ��������
        quSpecP.Active:=False;

        quTabP.FullRefresh;
      end;
    end;
  end;
end;

procedure TfmMainFF.cxButton26Click(Sender: TObject);
begin
  if CommonSet.CashNum>0 then CashDriver
  else
  begin
    if CommonSet.SpecChar=1 then
    begin
      if pos('fis',CommonSet.SpecBox)=0 then dmC.OpenMoneyBox
      else
      begin
        try
          StrWk:='COM'+IntToStr(CommonSet.CashPort);
          CommonSet.CashNum:=CommonSet.CashNum*(-1);
          CashOpen(PChar(StrWk));
          CashDriver;
          CashClose;
          CommonSet.CashNum:=CommonSet.CashNum*(-1);
        except
        end;
      end;
    end else  dmC.OpenMoneyBox;

    if Prizma then
    begin
      prWriteLog('!!OpenDriver');
      Event_RegEx(40,Nums.iCheckNum,0,0,'','',0,0,0);
    end;
  end;
end;

procedure TfmMainFF.TimerSdTo0Timer(Sender: TObject);
begin
  TimerSdTo0.Enabled:=False; //10 ������ � ����� ������������
  with dmC do
  begin
    if quCurSpec.RecordCount=0 then
      if cEdit1.Value>0.001 then cEdit1.Value:=0;
  end;
end;

procedure TfmMainFF.FormClick(Sender: TObject);
begin
  if GridSpec.Visible=False then
  begin
    if CommonSet.MReader=1 then fmPreFF.ShowModal
    else fmPre_Shape.ShowModal;
  end;
end;

procedure TfmMainFF.ClientSockRead(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  sBuffer:=sBuffer+Socket.ReceiveText;
end;

procedure TfmMainFF.cxButton27Click(Sender: TObject);
begin
  //������� �����
  if CommonSet.TypeFis='sp802' then ZOpen;
  while TestStatus('CheckRasch',sMessage)=False do
  begin
    fmAttention.Label1.Caption:=sMessage;
    fmAttention.ShowModal;
    Nums.iRet:=InspectSt(Nums.sRet);
    prWriteLog('~~AttentionShow;'+sMessage+';'+'InspectSt(Nums.sRet)='+IntToStr(Nums.iRet));
  end;
end;

end.

