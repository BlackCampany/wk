object fmTTKView: TfmTTKView
  Left = 373
  Top = 330
  BorderStyle = bsDialog
  Caption = #1058#1058#1050' '#1087#1088#1086#1089#1084#1086#1090#1088
  ClientHeight = 239
  ClientWidth = 575
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 220
    Width = 575
    Height = 19
    Panels = <
      item
        Width = 300
      end
      item
        Width = 50
      end>
  end
  object GTSpecV: TcxGrid
    Left = 0
    Top = 0
    Width = 364
    Height = 220
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -8
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    LookAndFeel.Kind = lfOffice11
    object ViewTSpecV: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      OnCustomDrawCell = ViewTSpecVCustomDrawCell
      DataController.DataSource = dstaTSpecV
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'BRUTTO'
          Column = ViewTSpecVBRUTTO
        end
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'NETTO'
          Column = ViewTSpecVNETTO
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnFiltering = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      Styles.Header = dmO.cxStyle27
      object ViewTSpecVNum: TcxGridDBColumn
        Caption = #8470
        DataBinding.FieldName = 'Num'
        Styles.Content = dmO.cxStyle27
        Styles.Footer = dmO.cxStyle27
        Styles.Header = dmO.cxStyle27
        Width = 29
      end
      object ViewTSpecVIDCARD: TcxGridDBColumn
        Caption = #1050#1086#1076' '
        DataBinding.FieldName = 'IdCard'
        Styles.Content = dmO.cxStyle27
        Styles.Footer = dmO.cxStyle27
        Styles.Header = dmO.cxStyle27
        Width = 34
      end
      object ViewTSpecVNAME: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'Name'
        Styles.Content = dmO.cxStyle27
        Styles.Footer = dmO.cxStyle27
        Styles.Header = dmO.cxStyle27
        Width = 99
      end
      object ViewTSpecVIdM: TcxGridDBColumn
        Caption = #1045#1076'.'#1080#1079#1084'.'
        DataBinding.FieldName = 'IdM'
        PropertiesClassName = 'TcxButtonEditProperties'
        Properties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Visible = False
        Styles.Content = dmO.cxStyle27
        Styles.Footer = dmO.cxStyle27
        Styles.Header = dmO.cxStyle27
        Width = 50
      end
      object ViewTSpecVSM: TcxGridDBColumn
        Caption = #1045#1076'.'#1080#1079#1084'.'
        DataBinding.FieldName = 'SM'
        PropertiesClassName = 'TcxButtonEditProperties'
        Properties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Styles.Content = dmO.cxStyle27
        Styles.Footer = dmO.cxStyle27
        Styles.Header = dmO.cxStyle27
        Width = 50
      end
      object ViewTSpecVNETTO: TcxGridDBColumn
        Caption = #1053#1077#1090#1090#1086
        DataBinding.FieldName = 'Netto'
        Styles.Content = dmO.cxStyle27
        Styles.Footer = dmO.cxStyle27
        Styles.Header = dmO.cxStyle27
        Width = 55
      end
      object ViewTSpecVBRUTTO: TcxGridDBColumn
        Caption = #1041#1088#1091#1090#1090#1086
        DataBinding.FieldName = 'Brutto'
        Styles.Content = dmO.cxStyle27
        Styles.Footer = dmO.cxStyle27
        Styles.Header = dmO.cxStyle27
        Width = 63
      end
      object ViewTSpecVTCard: TcxGridDBColumn
        Caption = #1058#1058#1050
        DataBinding.FieldName = 'TCard'
        Visible = False
        Styles.Content = dmO.cxStyle27
        Styles.Footer = dmO.cxStyle27
        Styles.Header = dmO.cxStyle27
      end
    end
    object LTSpecV: TcxGridLevel
      GridView = ViewTSpecV
    end
  end
  object Panel1: TPanel
    Left = 364
    Top = 0
    Width = 211
    Height = 220
    Align = alRight
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 2
    object Label2: TLabel
      Left = 8
      Top = 16
      Width = 91
      Height = 13
      Caption = #1053#1086#1084#1077#1088' '#1088#1077#1094#1077#1087#1090#1091#1088#1099
    end
    object Label3: TLabel
      Left = 8
      Top = 40
      Width = 74
      Height = 13
      Caption = #1042#1099#1093#1086#1076' 1 '#1087#1086#1088#1094'. '
    end
    object Label4: TLabel
      Left = 8
      Top = 80
      Width = 65
      Height = 13
      Caption = #1063#1080#1089#1083#1086' '#1087#1086#1088#1094'. '
    end
    object Label5: TLabel
      Left = 8
      Top = 104
      Width = 95
      Height = 13
      Caption = #1052#1072#1089#1089#1072' 1 '#1087#1086#1088#1094#1080#1080'  '#1075'.'
    end
    object cxButton1: TcxButton
      Left = 64
      Top = 184
      Width = 105
      Height = 25
      Caption = 'Ok'
      ModalResult = 1
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
    end
    object cxTextEdit3: TcxTextEdit
      Left = 109
      Top = 12
      TabStop = False
      Style.Shadow = True
      TabOrder = 1
      Text = 'cxTextEdit3'
      Width = 89
    end
    object cxTextEdit4: TcxTextEdit
      Left = 109
      Top = 36
      TabStop = False
      Style.Shadow = True
      TabOrder = 2
      Text = 'cxTextEdit4'
      Width = 89
    end
    object cxSpinEdit1: TcxSpinEdit
      Left = 109
      Top = 76
      TabStop = False
      Style.LookAndFeel.Kind = lfOffice11
      Style.Shadow = True
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 3
      Value = 1
      Width = 89
    end
    object cxCalcEdit1: TcxCalcEdit
      Left = 109
      Top = 100
      TabStop = False
      EditValue = 0.000000000000000000
      Style.LookAndFeel.Kind = lfOffice11
      Style.Shadow = True
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 4
      Width = 89
    end
  end
  object FormPlacement1: TFormPlacement
    Active = False
    Left = 264
    Top = 56
  end
  object taTSpecV: TClientDataSet
    Aggregates = <>
    FileName = 'ttkspecv.cds'
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'taTSpecIndex1'
        Fields = 'Num'
      end>
    IndexName = 'taTSpecIndex1'
    Params = <>
    StoreDefs = True
    Left = 144
    Top = 33
    object taTSpecVNum: TIntegerField
      FieldName = 'Num'
    end
    object taTSpecVIdCard: TIntegerField
      FieldName = 'IdCard'
    end
    object taTSpecVIdM: TIntegerField
      FieldName = 'IdM'
    end
    object taTSpecVSM: TStringField
      FieldName = 'SM'
    end
    object taTSpecVKm: TFloatField
      FieldName = 'Km'
    end
    object taTSpecVName: TStringField
      FieldName = 'Name'
      Size = 200
    end
    object taTSpecVNetto: TFloatField
      FieldName = 'Netto'
      DisplayFormat = '0.000'
    end
    object taTSpecVBrutto: TFloatField
      FieldName = 'Brutto'
      DisplayFormat = '0.000'
    end
    object taTSpecVKnb: TFloatField
      FieldName = 'Knb'
    end
    object taTSpecVTCard: TSmallintField
      FieldName = 'TCard'
    end
  end
  object dstaTSpecV: TDataSource
    DataSet = taTSpecV
    Left = 144
    Top = 89
  end
end
