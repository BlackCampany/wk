unit TBuff;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, Menus, cxLookAndFeelPainters, StdCtrls,
  cxButtons, cxCalendar;

type
  TfmTBuff = class(TForm)
    ViewTH: TcxGridDBTableView;
    LevelTH: TcxGridLevel;
    GridTH: TcxGrid;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    GridTHLevelTS: TcxGridLevel;
    ViewTS: TcxGridDBTableView;
    ViewTHDATEB: TcxGridDBColumn;
    ViewTHDATEE: TcxGridDBColumn;
    ViewTHSHORTNAME: TcxGridDBColumn;
    ViewTHRECEIPTNUM: TcxGridDBColumn;
    ViewTHPOUTPUT: TcxGridDBColumn;
    ViewTHPCOUNT: TcxGridDBColumn;
    ViewTHPVES: TcxGridDBColumn;
    ViewTSIDC: TcxGridDBColumn;
    ViewTSIDT: TcxGridDBColumn;
    ViewTSID: TcxGridDBColumn;
    ViewTSIDCARD: TcxGridDBColumn;
    ViewTSCURMESSURE: TcxGridDBColumn;
    ViewTSNETTO: TcxGridDBColumn;
    ViewTSBRUTTO: TcxGridDBColumn;
    ViewTSKNB: TcxGridDBColumn;
    procedure cxButton4Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmTBuff: TfmTBuff;

implementation

uses TCard;

{$R *.dfm}

procedure TfmTBuff.cxButton4Click(Sender: TObject);
begin
  //�������
  with fmTCard do
  begin
    TCS.First;
    while not TCS.Eof do
    begin
      if TCSIDT.AsInteger=TCHID.AsInteger then TCS.Delete
      else TCS.Next;
    end;
    if not TCH.Eof then TCH.Delete;
  end;
end;

procedure TfmTBuff.cxButton3Click(Sender: TObject);
begin
//��������
  with fmTCard do
  begin
    // ��������
    TCH.First;
    TCS.First;
    while not TCS.Eof do TCS.Delete;
    while not TCH.Eof do TCH.Delete;
  end;
end;

end.
