unit DocCompl;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SpeedBar, ExtCtrls, ComCtrls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Placemnt, cxImageComboBox, XPStyleActnCtrls, ActnList, ActnMan,
  cxCurrencyEdit, cxContainer, cxTextEdit, cxMemo, FR_Class, FR_DSet,
  FR_DBSet, Menus, FIBDataSet, pFIBDataSet;

type
  TfmDocsCompl = class(TForm)
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    Timer1: TTimer;
    FormPlacement1: TFormPlacement;
    GridComplB: TcxGrid;
    ViewComplB: TcxGridDBTableView;
    LevelComplB: TcxGridLevel;
    ViewComplBID: TcxGridDBColumn;
    ViewComplBDATEDOC: TcxGridDBColumn;
    ViewComplBNUMDOC: TcxGridDBColumn;
    ViewComplBIDSKL: TcxGridDBColumn;
    ViewComplBSUMIN: TcxGridDBColumn;
    ViewComplBSUMUCH: TcxGridDBColumn;
    ViewComplBNAMEMH: TcxGridDBColumn;
    ViewComplBIACTIVE: TcxGridDBColumn;
    amDocsCompl: TActionManager;
    acPeriod: TAction;
    SpeedItem2: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    SpeedItem6: TSpeedItem;
    acEditDoc1: TAction;
    acViewDoc1: TAction;
    acDelDoc1: TAction;
    acOnDoc1: TAction;
    acOffDoc1: TAction;
    SpeedItem7: TSpeedItem;
    SpeedItem8: TSpeedItem;
    acAddDocManual: TAction;
    SpeedItem9: TSpeedItem;
    Memo1: TcxMemo;
    RepCompl: TfrReport;
    PopupMenu1: TPopupMenu;
    acCopy: TAction;
    acPast: TAction;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    Excel1: TMenuItem;
    N4: TMenuItem;
    acPrintSeb: TAction;
    quRepSeb: TpFIBDataSet;
    quRepSebID: TFIBIntegerField;
    quRepSebIDCARD: TFIBIntegerField;
    quRepSebQUANT: TFIBFloatField;
    quRepSebIDM: TFIBIntegerField;
    quRepSebKM: TFIBFloatField;
    quRepSebPRICEIN: TFIBFloatField;
    quRepSebSUMIN: TFIBFloatField;
    quRepSebNAME: TFIBStringField;
    quRepSebIMESSURE: TFIBIntegerField;
    quRepSebLASTPRICEOUT: TFIBFloatField;
    frquRepSeb: TfrDBDataSet;
    procedure SpeedItem1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acPeriodExecute(Sender: TObject);
    procedure acEditDoc1Execute(Sender: TObject);
    procedure acViewDoc1Execute(Sender: TObject);
    procedure ViewComplBDblClick(Sender: TObject);
    procedure acDelDoc1Execute(Sender: TObject);
    procedure acOnDoc1Execute(Sender: TObject);
    procedure acOffDoc1Execute(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure acAddDocManualExecute(Sender: TObject);
    procedure acAddDocBExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure acCopyExecute(Sender: TObject);
    procedure acPastExecute(Sender: TObject);
    procedure Excel1Click(Sender: TObject);
    procedure acPrintSebExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure prOpenSpec(IDH:INteger);
    procedure prOpenSpecC(IDH:INteger);
    procedure prOpenSpecB(IDH:INteger);
    procedure prOn(IDH,IdSkl,iDate:INteger;Var rSum1,rSum2:Real);
  end;

procedure prSetDOBSpecPar(sOper:String;iType:Integer);

var
  fmDocsCompl: TfmDocsCompl;
  bClearComplB:Boolean = false;
  bStartOpen:Boolean = false;

implementation

uses Un1, dmOffice, PeriodUni, AddDoc1, Period3, DOBSpec, SelPartIn1,
  DMOReps, AddCompl, TBuff, MainRnOffice;

{$R *.dfm}

procedure TfmDocsCompl.prOn(IDH,IdSkl,iDate:INteger;Var rSum1,rSum2:Real);
Var rQP,rQs,PriceSp,rQ,rMessure,rQRemn:Real;
    IdCli:Integer;
    rSumP:Real;
begin
  with dmO do
  with dmORep do
  begin
    CloseTa(taPartTest);

    quSpecComplC.Active:=False;
    quSpecComplC.ParamByName('IDH').AsInteger:=IDH;
    quSpecComplC.Active:=True;

    rSum1:=0;
    rSum2:=0;

    quSpecComplC.First;
    while not quSpecComplC.Eof do
    begin
      PriceSp:=0;
      IdCli:=0;
      rSumP:=0;

      rQs:=quSpecComplCQUANT.AsFloat; //quSpecComplC - � �������� �������� ���������
              //��� ����� �� ������ �.�. ������������ ����������� �� ���, � ��� ��� � �������� ���������

      prSelPartIn(quSpecComplCIDCARD.AsInteger,IdSkl,0,0);

      quSelPartIn.First;
      if rQs>0 then
      begin
        while (not quSelPartIn.Eof) and (rQs>0) do
        begin
          //���� �� ���� ������� ���� �����, ��������� �������� ���
          rQp:=quSelPartInQREMN.AsFloat;
          if rQs<=rQp then  rQ:=rQs//��������� ������ ������ ���������
                      else  rQ:=rQp;
          rQs:=rQs-rQ;
        //��������� ��������� ������
        //����������� ��������� ������
//?ARTICUL, ?IDDATE, ?IDSTORE, ?IDPARTIN, ?IDDOC, ?IDCLI, ?DTYPE, ?QUANT, ?PRICEIN, ?SUMOUT)
          prAddPartOut.ParamByName('ARTICUL').AsInteger:=quSpecComplCIDCARD.AsInteger;
          prAddPartOut.ParamByName('IDDATE').AsInteger:=iDate;
          prAddPartOut.ParamByName('IDSTORE').AsInteger:=IdSkl;
          prAddPartOut.ParamByName('IDPARTIN').AsInteger:=quSelPartInID.AsInteger;
          prAddPartOut.ParamByName('IDDOC').AsInteger:=quSpecComplCIDHEAD.AsInteger;
          prAddPartOut.ParamByName('IDCLI').AsInteger:=quSelPartInIDCLI.AsInteger;
          prAddPartOut.ParamByName('DTYPE').AsInteger:=6; //������������
          prAddPartOut.ParamByName('QUANT').AsFloat:=rQ;
          prAddPartOut.ParamByName('PRICEIN').AsFloat:=quSelPartInPRICEIN.AsFloat;
          prAddPartOut.ParamByName('SUMOUT').AsFloat:=quSelPartInPRICEOUT.AsFloat*rQ;
          prAddPartout.ExecProc;

          PriceSp:=quSelPartInPRICEIN.AsFloat;
          IdCli:=quSelPartInIDCLI.AsInteger;
          rSum1:=rSum1+PriceSp*rQ;
          rSumP:=rSumP+PriceSp*rQ;

          quSelPartIn.Next;
        end;

        quSelPartIn.Active:=False;

       //��������� �������� � ������������� ������
        if rQs>0 then //�������� ������������� ������
        begin
          if PriceSp=0 then
          begin //��� ���� ���������� ������� � ���������� ����������
            prCalcLastPrice1.ParamByName('IDGOOD').AsInteger:=quSpecComplCIDCARD.AsInteger;
            prCalcLastPrice1.ExecProc;
            PriceSp:=prCalcLastPrice1.ParamByName('PRICEIN').AsFloat;
            rMessure:=prCalcLastPrice1.ParamByName('KOEF').AsFloat;
            IdCli:=prCalcLastPrice1.ParamByName('IDCLI').AsInteger;
            if (rMessure<>0)and(rMessure<>1) then PriceSp:=PriceSp/rMessure;
          end;

        //��������� ��� ����� ���������
          prAddPartOut.ParamByName('ARTICUL').AsInteger:=quSpecComplCIDCARD.AsInteger;
          prAddPartOut.ParamByName('IDDATE').AsInteger:=iDate;
          prAddPartOut.ParamByName('IDSTORE').AsInteger:=IdSkl;
          prAddPartOut.ParamByName('IDPARTIN').AsInteger:=-1;
          prAddPartOut.ParamByName('IDDOC').AsInteger:=quSpecComplCIDHEAD.AsInteger;
          prAddPartOut.ParamByName('IDCLI').AsInteger:=IdCli;
          prAddPartOut.ParamByName('DTYPE').AsInteger:=6;
          prAddPartOut.ParamByName('QUANT').AsFloat:=rQs;
          prAddPartOut.ParamByName('PRICEIN').AsFloat:=PriceSp;
          prAddPartOut.ParamByName('SUMOUT').AsFloat:=PriceSp*rQs;
          prAddPartout.ExecProc;

          rSum1:=rSum1+PriceSp*rQs;
          rSumP:=rSumP+PriceSp*rQs;

          taPartTest.Append;
          taPartTestNum.AsInteger:=0;
          taPartTestIdGoods.AsInteger:=quSpecComplCIDCARD.AsInteger;
          taPartTestNameG.AsString:=quSpecComplCNAME.AsString;
          taPartTestIM.AsInteger:=quSpecComplCIDM.AsInteger;
          taPartTestSM.AsString:=quSpecComplCNAMESHORT.AsString;
          taPartTestQuant.AsFloat:=quSpecComplCQUANT.AsFloat;
          taPartTestPrice1.AsFloat:=0;
          taPartTestiRes.AsInteger:=0;
          taPartTest.Post;
        end else
        begin
          taPartTest.Append;
          taPartTestNum.AsInteger:=0;
          taPartTestIdGoods.AsInteger:=quSpecComplCIDCARD.AsInteger;
          taPartTestNameG.AsString:=quSpecComplCNAME.AsString;
          taPartTestIM.AsInteger:=quSpecComplCIDM.AsInteger;
          taPartTestSM.AsString:=quSpecComplCNAMESHORT.AsString;
          taPartTestQuant.AsFloat:=quSpecComplCQUANT.AsFloat;
          taPartTestPrice1.AsFloat:=0;
          taPartTestiRes.AsInteger:=1;
          taPartTest.Post;
        end;
      end;

      rQs:=quSpecComplCQUANT.AsFloat; //quSpecComplC - � �������� �������� ���������
      quSpecComplC.Edit;
      if rQs<>0 then quSpecComplCPRICEIN.AsFloat:=rSumP/rQs
      else quSpecComplCPRICEIN.AsFloat:=0;
      quSpecComplCSUMIN.AsFloat:=rSumP;
      quSpecComplC.Post;

      quSpecComplC.Next;
    end;

 //������� ������������� ����
    quSpecComplCB.Active:=False;
    quSpecComplCB.ParamByName('IDH').AsInteger:=IdH;
    quSpecComplCB.Active:=True;

    //������������� ���� � �����������
    quSpecComplCB.First;
    while not quSpecComplCB.Eof do
    begin
      if quSpecComplC.Locate('IDCARD',quSpecComplCBIDCARD.AsInteger,[]) then
      begin
        quSpecComplCB.Edit;
        quSpecComplCBPRICEIN.AsFloat:=quSpecComplCPRICEIN.AsFloat;
        quSpecComplCBSUMIN.AsFloat:=quSpecComplCBQUANTC.AsFloat*quSpecComplCPRICEIN.AsFloat;
        quSpecComplCB.Post;
      end;
      quSpecComplCB.Next;
    end;

    //������������� ����
    quSpecCompl.Active:=False;
    quSpecCompl.ParamByName('IDH').AsInteger:=IdH;
    quSpecCompl.Active:=True;

    quSpecCompl.First;
    while not quSpecCompl.Eof do
    begin
      rSumP:=0;

      quSpecComplCB.First;
      while not quSpecComplCB.Eof do
      begin
        if quSpecComplCBIDB.AsInteger=quSpecComplID.AsInteger then rSumP:=rSumP+quSpecComplCBSUMIN.AsFloat;
        quSpecComplCB.Next;
      end;

      rQs:=quSpecComplQUANT.AsFloat;

      quSpecCompl.Edit;
      quSpecComplSumIn.AsFloat:=rSumP;
      if rQs<>0 then quSpecComplPRICEIN.AsFloat:=rSumP/rQs
      else  quSpecComplPRICEIN.AsFloat:=0;
      quSpecCompl.Post;

      //�������� �������� �������� �� ����. ���� ������� =0 �� ������� ��� ���������� ������
      rQRemn:=prCalcRemn(quSpecComplIDCARD.AsInteger,iDate-1,IdSkl);
      if rQRemn<=0.001 then //������� ��� ������ �� ������� ������
      begin
        quClosePartIn.ParamByName('IDCARD').AsInteger:=quSpecComplIDCARD.AsInteger;
        quClosePartIn.ParamByName('IDSKL').AsInteger:=IdSkl;
        quClosePartIn.ParamByName('IDATE').AsInteger:=iDate-1;
        quClosePartIn.ExecQuery;
      end;

      //�������� ��������� ������ ����������� �����

      prAddPartIn1.ParamByName('IDSKL').AsInteger:=IdSkl;
      prAddPartIn1.ParamByName('IDDOC').AsInteger:=IdH;
      prAddPartIn1.ParamByName('DTYPE').AsInteger:=6;
      prAddPartIn1.ParamByName('IDATE').AsInteger:=iDate;
      prAddPartIn1.ParamByName('IDCARD').AsInteger:=quSpecComplIDCARD.AsInteger;
      prAddPartIn1.ParamByName('IDCLI').AsInteger:=-3;
      prAddPartIn1.ParamByName('QUANT').AsFloat:=quSpecComplQUANT.AsFloat*quSpecComplKM.AsFloat; //� ��������
      try
        prAddPartIn1.ParamByName('PRICEIN').AsFloat:=rSumP/(quSpecComplQUANT.AsFloat*quSpecComplKM.AsFloat);
        prAddPartIn1.ParamByName('PRICEUCH').AsFloat:=quSpecComplPRICEINUCH.AsFloat/quSpecComplKM.AsFloat;
      except
        prAddPartIn1.ParamByName('PRICEIN').AsFloat:=0;
        prAddPartIn1.ParamByName('PRICEUCH').AsFloat:=0;
      end;
      prAddPartIn1.ExecProc;

      quSpecCompl.Next;
    end;

    quSpecCompl.Active:=False;
    quSpecComplCB.Active:=False;
    quSpecComplC.Active:=False;

    rSum2:=rSum1;

  end;
end;

procedure TfmDocsCompl.prOpenSpec(IDH:INteger);
begin
  with dmO do
  with dmORep do
  begin
    prAllViewOff;

    CloseTa(fmAddCompl.taSpec);

    quSpecCompl.Active:=False;
    quSpecCompl.ParamByName('IDH').AsInteger:=IDH;
    quSpecCompl.Active:=True;

    quSpecCompl.First;
    while not quSpecCompl.Eof do
    begin
      with fmAddCompl do
      begin
        taSpec.Append;
        taSpecNum.AsInteger:=quSpecComplID.AsInteger;
        taSpecIdGoods.AsInteger:=quSpecComplIDCARD.AsInteger;
        taSpecNameG.AsString:=quSpecComplNAME.AsString;
        taSpecIM.AsInteger:=quSpecComplIDM.AsInteger;
        taSpecSM.AsString:=quSpecComplNAMESHORT.AsString;
        taSpecQuantFact.AsFloat:=quSpecComplQUANT.AsFloat;
        taSpecPriceIn.AsFloat:=quSpecComplPRICEIN.AsFloat;
        taSpecSumIn.AsFloat:=quSpecComplSUMIN.AsFloat;
        taSpecPriceUch.AsFloat:=quSpecComplPRICEINUCH.AsFloat;
        taSpecSumUch.AsFloat:=quSpecComplSUMINUCH.AsFloat;
        taSpecKm.AsFloat:=quSpecComplKM.AsFloat;
        taSpecTCard.AsInteger:=quSpecComplTCARD.AsInteger;
        taSpec.Post;
      end;
      quSpecCompl.Next;
    end;
    quSpecCompl.Active:=False;
    prAllViewOn;
  end;
end;

procedure TfmDocsCompl.prOpenSpecC(IDH:INteger);
begin
  with dmO do
  with dmORep do
  begin
    fmAddCompl.ViewComC.BeginUpdate;
    CloseTa(fmAddCompl.taSpecC);

    quSpecComplC.Active:=False;
    quSpecComplC.ParamByName('IDH').AsInteger:=IDH;
    quSpecComplC.Active:=True;

    quSpecComplC.First;
    while not quSpecComplC.Eof do
    begin
      with fmAddCompl do
      begin
        taSpecC.Append;
        taSpecCNum.AsInteger:=quSpecComplCID.AsInteger;
        taSpecCIdGoods.AsInteger:=quSpecComplCIDCARD.AsInteger;
        taSpecCNameG.AsString:=quSpecComplCNAME.AsString;
        taSpecCIM.AsInteger:=quSpecComplCIDM.AsInteger;
        taSpecCSM.AsString:=quSpecComplCNAMESHORT.AsString;
        taSpecCQuantFact.AsFloat:=quSpecComplCQUANT.AsFloat;
        taSpecCKm.AsFloat:=quSpecComplCKM.AsFloat;
        taSpecCPriceIn.AsFloat:=quSpecComplCPRICEIN.AsFloat;
        taSpecCSumIn.AsFloat:=quSpecComplCSUMIN.AsFloat;
        taSpecCPriceUch.AsFloat:=quSpecComplCPRICEINUCH.AsFloat;
        taSpecCSumUch.AsFloat:=quSpecComplCSUMINUCH.AsFloat;
        taSpecC.Post;
      end;
      quSpecComplC.Next;
    end;
    quSpecComplC.Active:=False;
  end;
  fmAddCompl.ViewComC.EndUpdate;
end;

procedure TfmDocsCompl.prOpenSpecB(IDH:INteger);
begin
  with dmO do
  with dmORep do
  begin
    fmAddCompl.ViewComBC.BeginUpdate;
    CloseTa(taCalcB);

    quSpecComplCB.Active:=False;
    quSpecComplCB.ParamByName('IDH').AsInteger:=IDH;
    quSpecComplCB.Active:=True;

    quSpecComplCB.First;
    while not quSpecComplCB.Eof do
    begin
      taCalcB.Append;
      taCalcBID.AsInteger:=quSpecComplCBIDB.AsInteger;
      taCalcBCODEB.AsInteger:=quSpecComplCBCODEB.AsInteger;
      taCalcBNAMEB.AsString:=quSpecComplCBNAMEB.AsString;
      taCalcBQUANT.AsFloat:=quSpecComplCBQUANT.AsFloat;
      taCalcBIDCARD.AsInteger:=quSpecComplCBIDCARD.AsInteger;
      taCalcBNAMEC.AsString:=quSpecComplCBNAMEC.AsString;
      taCalcBQUANTC.AsFloat:=quSpecComplCBQUANTC.AsFloat;
      taCalcBPRICEIN.AsFloat:=quSpecComplCBPRICEIN.AsFloat;
      taCalcBSUMIN.AsFloat:=quSpecComplCBSUMIN.AsFloat;
      taCalcBIM.AsInteger:=quSpecComplCBIM.AsInteger;
      taCalcBSM.AsString:=quSpecComplCBSM.AsString;
      taCalcBSB.AsString:=quSpecComplCBSB.AsString;
      taCalcB.Post;

      quSpecComplCB.Next;
    end;
    quSpecComplCB.Active:=False;
  end;
  fmAddCompl.ViewComBC.EndUpdate;
end;



procedure prSetDOBSpecPar(sOper:String;iType:Integer);
begin
{  with dmO do
  with dmORep do
  with fmDobSpec do
  begin
    cxTextEdit1.Text:=quDocsOutBNUMDOC.AsString;
    cxTextEdit2.Text:=quDocsOutBOPER.AsString;


    if quMHALL.Active=False then quMHALL.Active:=True;
    quMHAll.FullRefresh;

    CurVal.IdMH:=quDocsOutBIDSKL.AsInteger;
    CurVal.NAMEMH:=quDocsOutBNAMEMH.AsString;

    if CurVal.IdMH<>0 then
    begin
      cxLookupComboBox1.EditValue:=CurVal.IdMH;
      cxLookupComboBox1.Text:=CurVal.NAMEMH;
    end else
    begin
       quMHAll.First;
       if not quMHAll.Eof then
       begin
         CurVal.IdMH:=quMHAllID.AsInteger;
         CurVal.NAMEMH:=quMHAllNAMEMH.AsString;
         cxLookupComboBox1.EditValue:=CurVal.IdMH;
         cxLookupComboBox1.Text:=CurVal.NAMEMH;
       end;
    end;


    cxDateEdit1.Date:=quDocsOutBDATEDOC.AsDateTime;

    if iType=1 then //��������������
    begin
      cxButton2.Visible:=True;
      cxButton2.Enabled:=True;

      if (sOper ='Del')
         or (sOper ='Sale')
         or (sOper ='SaleBank')
         or (sOper ='SalePC')
         or (sOper ='Ret')
         or (sOper ='RetBank') then //���������������� ���������
      begin
        cxTextEdit1.Properties.ReadOnly:=False;
        cxTextEdit2.Properties.ReadOnly:=True;
        cxLookupComboBox1.Properties.ReadOnly:=False;
        cxDateEdit1.Properties.ReadOnly:=False;

        ViewBSIFR.Editing:=False;
        ViewBNAMEB.Editing:=False;
        ViewBCODEB.Editing:=True; ViewBCODEB.Properties.ReadOnly:=False;
        ViewBKB.Editing:=True;
        ViewBQUANT.Editing:=False;
        ViewBPRICER.Editing:=False;
        ViewBDSUM.Editing:=False;
        ViewBRSUM.Editing:=False;
        ViewBIDCARD.Editing:=True; ViewBIDCARD.Properties.ReadOnly:=False;
        ViewBNAME.Editing:=True; ViewBNAME.Properties.ReadOnly:=False;
        ViewBNAMESHORT.Editing:=False;
        ViewBTCARD.Editing:=False;
        ViewBKM.Editing:=False;

        image1.Visible:=False;
        image2.Visible:=False;
        Label11.Visible:=False;
        Label12.Visible:=False;
      end else
      begin
        cxTextEdit1.Properties.ReadOnly:=False;
        cxTextEdit2.Properties.ReadOnly:=False;
        cxLookupComboBox1.Properties.ReadOnly:=False;
        cxDateEdit1.Properties.ReadOnly:=False;

        ViewBSIFR.Editing:=False;
        ViewBNAMEB.Editing:=False;
        ViewBCODEB.Editing:=True; ViewBCODEB.Properties.ReadOnly:=False;
        ViewBKB.Editing:=True;
        ViewBQUANT.Editing:=True;
        ViewBPRICER.Editing:=True;
        ViewBDSUM.Editing:=True;
        ViewBRSUM.Editing:=True;
        ViewBIDCARD.Editing:=True; ViewBIDCARD.Properties.ReadOnly:=False;
        ViewBNAME.Editing:=True; ViewBNAME.Properties.ReadOnly:=False;
        ViewBNAMESHORT.Editing:=False;
        ViewBTCARD.Editing:=False;
        ViewBKM.Editing:=False;

        image1.Visible:=True;
        image2.Visible:=True;
        Label11.Visible:=True;
        Label12.Visible:=True;
      end;
    end;
    if iType=2 then //��������
    begin
      cxTextEdit1.Properties.ReadOnly:=True;
      cxTextEdit2.Properties.ReadOnly:=True;
      cxLookupComboBox1.Properties.ReadOnly:=True;
      cxDateEdit1.Properties.ReadOnly:=True;

      ViewBSIFR.Editing:=False;
      ViewBNAMEB.Editing:=False;
      ViewBCODEB.Editing:=False; ViewBCODEB.Properties.ReadOnly:=True;
      ViewBKB.Editing:=False;
      ViewBQUANT.Editing:=False;
      ViewBPRICER.Editing:=False;
      ViewBDSUM.Editing:=False;
      ViewBRSUM.Editing:=False;
      ViewBIDCARD.Editing:=False; ViewBIDCARD.Properties.ReadOnly:=True;
      ViewBNAME.Editing:=False; ViewBNAME.Properties.ReadOnly:=True;
      ViewBNAMESHORT.Editing:=False;
      ViewBTCARD.Editing:=False;
      ViewBKM.Editing:=False;

      cxButton2.Visible:=False;
      cxButton2.Enabled:=False;

      image1.Visible:=False;
      image2.Visible:=False;
      Label11.Visible:=False;
      Label12.Visible:=False;
    end;
  end;}
end;


procedure TfmDocsCompl.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmDocsCompl.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  Timer1.Enabled:=True;
  GridComplB.Align:=AlClient;
  StatusBar1.Color:=$00FFCACA;
  ViewComplB.RestoreFromIniFile(CurDir+GridIni);
  Memo1.Clear;
end;

procedure TfmDocsCompl.FormClose(Sender: TObject; var Action: TCloseAction);
begin
   ViewComplB.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmDocsCompl.acPeriodExecute(Sender: TObject);
begin
//  ������
  fmPeriodUni.DateTimePicker1.Date:=CommonSet.DateFrom;
  fmPeriodUni.DateTimePicker2.Date:=CommonSet.DateTo-1;
  fmPeriodUni.ShowModal;
  if fmPeriodUni.ModalResult=mrOk then
  begin
    CommonSet.DateFrom:=Trunc(fmPeriodUni.DateTimePicker1.Date);
    CommonSet.DateTo:=Trunc(fmPeriodUni.DateTimePicker2.Date)+1;

    if CommonSet.DateTo>=iMaxDate then fmDocsCompl.Caption:='������������ ���� (���� ����) � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
    else fmDocsCompl.Caption:='������������ ���� (���� ����) �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);
    with dmORep do
    begin
      ViewComplB.BeginUpdate;
      quDocsCompl.Active:=False;
      quDocsCompl.ParamByName('DATEB').AsDate:=CommonSet.DateFrom;
      quDocsCompl.ParamByName('DATEE').AsDate:=CommonSet.DateTo;
      quDocsCompl.Active:=True;
      ViewComplB.EndUpdate;
    end;
  end;
end;

procedure TfmDocsCompl.acEditDoc1Execute(Sender: TObject);
Var IDH:INteger;
begin
  //�������������
  if bStartOpen then
  begin
    Memo1.Lines.Add('  ������� ��� �������, ���������.');Delay(10);
    exit; //�� �������� �������
  end;
  if not CanDo('prEditCompl') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  with dmO do
  with dmORep do
  begin
    if quDocsCompl.RecordCount>0 then
    begin
      if quDocsComplIACTIVE.AsInteger=0 then
      begin
        bStartOpen:=True;
        Memo1.Clear;
        Memo1.Lines.Add('�����, ���� �������� ���������.');Delay(10);

        prAllViewOff;

        fmAddCompl.Caption:='������������ (����-����): ��������������.';
        fmAddCompl.cxTextEdit1.Text:=quDocsComplNUMDOC.AsString;
        fmAddCompl.cxTextEdit1.Properties.ReadOnly:=False;
        fmAddCompl.cxTextEdit1.Tag:=quDocsComplId.AsInteger;
        fmAddCompl.cxDateEdit1.Date:=quDocsComplDATEDOC.AsDateTime;
        fmAddCompl.cxDateEdit1.Properties.ReadOnly:=False;

        if quMHAll.Active=False then quMHAll.Active:=True;
        quMHAll.FullRefresh;

        fmAddCompl.cxLookupComboBox1.EditValue:=quDocsComplIDSKL.AsInteger;
        fmAddCompl.cxLookupComboBox1.Text:=quDocsComplNAMEMH.AsString;
        fmAddCompl.cxLookupComboBox1.Properties.ReadOnly:=False;

        CurVal.IdMH:=quDocsComplIDSKL.AsInteger;
        CurVal.NAMEMH:=quDocsComplNAMEMH.AsString;

        if quMHAll.Locate('ID',CurVal.IdMH,[]) then
        begin
          fmAddCompl.Label15.Caption:='��. ����: '+quMHAllNAMEPRICE.AsString;
          fmAddCompl.Label15.Tag:=quMHAllDEFPRICE.AsInteger;
        end else
        begin
          fmAddCompl.Label15.Caption:='��. ����: ';
          fmAddCompl.Label15.Tag:=0;
        end;

        fmAddCompl.cxLabel1.Enabled:=True;
        fmAddCompl.cxLabel2.Enabled:=True;
        fmAddCompl.cxLabel7.Enabled:=True;
        fmAddCompl.cxLabel8.Enabled:=True;
        fmAddCompl.cxLabel9.Enabled:=True;
        fmAddCompl.cxButton1.Enabled:=True;

        fmAddCompl.ViewCom.OptionsData.Editing:=True;

        IDH:=quDocsComplID.AsInteger;

        Memo1.Lines.Add('   �����.'); Delay(10);
        fmDocsCompl.prOpenSpec(IDH);
        Memo1.Lines.Add('   ������.'); Delay(10);
        fmDocsCompl.prOpenSpecC(IDH);
        Memo1.Lines.Add('   �����������.'); Delay(10);
        fmDocsCompl.prOpenSpecB(IDH);

        Memo1.Lines.Add('   �������� ��.'); Delay(10);
        bStartOpen:=False;
        prAllViewOn;

        fmAddCompl.Show;
      end else showmessage('������������� �������������� �������� ������.');
    end;
  end;
end;

procedure TfmDocsCompl.acViewDoc1Execute(Sender: TObject);
Var IDH:INteger;
begin
  //��������
  if bStartOpen then
  begin
    Memo1.Lines.Add('  ������� ��� �������, ���������.');Delay(10);
    exit; //�� �������� �������
  end;
  if not CanDo('prViewCompl') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  with dmO do
  with dmORep do
  begin
    if quDocsCompl.RecordCount>0 then
    begin
        bStartOpen:=True;
        Memo1.Clear;
        Memo1.Lines.Add('�����, ���� �������� ���������.');Delay(10);

        prAllViewOff;

        fmAddCompl.Caption:='������������ (����-����): ��������������.';
        fmAddCompl.cxTextEdit1.Text:=quDocsComplNUMDOC.AsString;
        fmAddCompl.cxTextEdit1.Properties.ReadOnly:=True;
        fmAddCompl.cxTextEdit1.Tag:=quDocsComplId.AsInteger;
        fmAddCompl.cxDateEdit1.Date:=quDocsComplDATEDOC.AsDateTime;
        fmAddCompl.cxDateEdit1.Properties.ReadOnly:=True;

        if quMHAll.Active=False then quMHAll.Active:=True;
        quMHAll.FullRefresh;

        fmAddCompl.cxLookupComboBox1.EditValue:=quDocsComplIDSKL.AsInteger;
        fmAddCompl.cxLookupComboBox1.Text:=quDocsComplNAMEMH.AsString;
        fmAddCompl.cxLookupComboBox1.Properties.ReadOnly:=True;

        CurVal.IdMH:=quDocsComplIDSKL.AsInteger;
        CurVal.NAMEMH:=quDocsComplNAMEMH.AsString;

        if quMHAll.Locate('ID',CurVal.IdMH,[]) then
        begin
          fmAddCompl.Label15.Caption:='��. ����: '+quMHAllNAMEPRICE.AsString;
          fmAddCompl.Label15.Tag:=quMHAllDEFPRICE.AsInteger;
        end else
        begin
          fmAddCompl.Label15.Caption:='��. ����: ';
          fmAddCompl.Label15.Tag:=0;
        end;

        fmAddCompl.cxLabel1.Enabled:=False;
        fmAddCompl.cxLabel2.Enabled:=False;
        fmAddCompl.cxLabel7.Enabled:=False;
        fmAddCompl.cxLabel8.Enabled:=False;
        fmAddCompl.cxLabel9.Enabled:=False;
        fmAddCompl.cxButton1.Enabled:=False;

        fmAddCompl.ViewCom.OptionsData.Editing:=False;

        IDH:=quDocsComplID.AsInteger;

        Memo1.Lines.Add('   �����.'); Delay(10);
        fmDocsCompl.prOpenSpec(IDH);
        Memo1.Lines.Add('   ������.'); Delay(10);
        fmDocsCompl.prOpenSpecC(IDH);
        Memo1.Lines.Add('   �����������.'); Delay(10);
        fmDocsCompl.prOpenSpecB(IDH);

        Memo1.Lines.Add('   �������� ��.'); Delay(10);
        bStartOpen:=False;

        prAllViewOn;

        fmAddCompl.Show;
    end;
  end;
end;

procedure TfmDocsCompl.ViewComplBDblClick(Sender: TObject);
begin
  //������� �������
  with dmORep do
  begin
    if quDocsComplIACTIVE.AsInteger=0 then acEditDoc1.Execute //��������������
    else acViewDoc1.Execute; //��������}
  end;
end;

procedure TfmDocsCompl.acDelDoc1Execute(Sender: TObject);
begin
  //������� ��������
  if not CanDo('prDelCompl') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  with dmORep do
  begin
    if quDocsCompl.RecordCount>0 then //���� ��� �������������
    begin
      if quDocsComplIACTIVE.AsInteger=0 then
      begin
        if MessageDlg('�� ������������� ������ ������� �������� �'+quDocsComplNUMDOC.AsString+' �� '+FormatDateTime('dd.mm.yyyy',quDocsComplDATEDOC.AsDateTime),mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          quDocsCompl.Delete;
        end;
      end else
      begin
        showmessage('������� �������������� �������� ������.');
      end;
    end else
    begin
      showmessage('�������� �������� ��� ��������.');
    end;
  end;
end;

procedure TfmDocsCompl.acOnDoc1Execute(Sender: TObject);
Var IdH:INteger;
    rSum:Real;
begin
//������������
  //��� ���������
  with dmO do
  with dmORep do
  begin

    if not CanDo('prOnCompl') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
    if not CanEdit(Trunc(quDocsComplDATEDOC.AsDateTime)) then begin StatusBar1.Panels[0].Text:='������ ������.'; exit; end;

    if quDocsCompl.RecordCount>0 then //���� ��� ������������
    begin
      if quDocsComplIACTIVE.AsInteger=0 then
      begin
        if MessageDlg('������������ �������� �'+quDocsComplNUMDOC.AsString+' �� '+FormatDateTime('dd.mm.yyyy',quDocsComplDATEDOC.AsDateTime),mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          if prTOFind(Trunc(quDocsComplDATEDOC.AsDateTime),quDocsComplIDSKL.AsInteger)=1 then
          begin //�� ����
            if MessageDlg('������� ��� �������� ������ �� �� '+quDocsComplNAMEMH.AsString+' � '+quDocsComplNUMDOC.AsString+'?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
              prTODel(Trunc(quDocsComplDATEDOC.AsDateTime),quDocsComplIDSKL.AsInteger)
            else
            begin
              showmessage('��������� ������� ��������� ����������...');
              exit;
            end;
          end;

         // 1 - ������� ��������� ������
         // 2 - �������� ��������������
         // 3 - �������� ������

          IDH:=quDocsComplID.AsInteger;
          if quDocsComplIDSKL.AsInteger>0 then
          begin
            Memo1.Clear; delay(10);
            Memo1.Lines.Add('�����...  ���� ������������� ���������.'); delay(10);

            prOn(IDH,quDocsComplIDSKL.AsInteger,trunc(quDocsComplDATEDOC.AsDateTime),rSum,rSum);

            quDocsCompl.Edit;
            quDocsComplSUMIN.AsFloat:=rSum;
            quDocsComplSUMUCH.AsFloat:=rSum;//����
            quDocsComplIACTIVE.AsInteger:=1;
            quDocsCompl.Post;
            quDocsCompl.Refresh;

            Memo1.Lines.Add('������������� ��'); delay(10);

            fmPartIn1:=tfmPartIn1.Create(Application);
            fmPartIn1.Label5.Caption:=quDocsOutBNAMEMH.AsString;
            fmPartIn1.Label6.Caption:='�� ����.';
            fmPartIn1.ViewPartInPrice1.Visible:=False;
            fmPartIn1.ViewPartInNum.Visible:=False;
            fmPartIn1.ShowModal;
            fmPartIn1.Release;

            taPartTest.Active:=False;


          end else showmessage('���������� ����� �������� ���������.');
        end;
      end;
    end;
  end;
end;


procedure TfmDocsCompl.acOffDoc1Execute(Sender: TObject);
Var iCountPartOut:INteger;
    bStart:Boolean;
begin
//��������
  //��� ���������
  with dmO do
  with dmORep do
  begin
    if not CanDo('prOffCompl') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
    if not CanEdit(Trunc(quDocsComplDATEDOC.AsDateTime)) then begin StatusBar1.Panels[0].Text:='������ ������.'; exit; end;

    if quDocsCompl.RecordCount>0 then //���� ��� ����������
    begin
      if quDocsComplIACTIVE.AsInteger=1 then
      begin
        if MessageDlg('�������� �������� �'+quDocsComplNUMDOC.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          if prTOFind(Trunc(quDocsComplDATEDOC.AsDateTime),quDocsComplIDSKL.AsInteger)=1 then
          begin //�� ����
            if MessageDlg('������� ��� �������� ������ �� �� '+quDocsComplNAMEMH.AsString+' � '+quDocsComplNUMDOC.AsString+'?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
              prTODel(Trunc(quDocsComplDATEDOC.AsDateTime),quDocsComplIDSKL.AsInteger)
            else
            begin
              showmessage('��������� ������� ��������� ����������...');
              exit;
            end;
          end;

         // 1 - ��������� ���� �� �������� � ��������� ������� ������� ���������, �� ������� �  ���������� �����
         // ���� ������ ��
          prFindPartOut.ParamByName('IDDOC').AsInteger:=quDocsComplID.AsInteger;
          prFindPartOut.ParamByName('DTYPE').AsInteger:=6;
          prFindPartOut.ExecProc;
          iCountPartOut:=prFindPartOut.ParamByName('RESULT').Value;
          if iCountPartOut>0 then
          begin
            if MessageDlg('� ������� ��������� ��������� '+IntToStr(iCountPartOut)+' ��������� ������. ����������?.',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
            begin
               bStart:=True;
//              showmessage('�� ������� ����������� ������������� � '+FormatDateTime('dd.mm.yyyy',quDocsComplDATEDOC.AsDateTime)+' �����.');
//              bStart:=False;
            end else
            begin
              bStart:=False;
            end;
          end else bStart:=True;
          if bStart then
          begin
            // 1 - �������� ��������� ������
            // 2 - ������� ��������� ��������� ������ �� ��������� c ���������� ��������������
            prPartInDel.ParamByName('IDDOC').AsInteger:=quDocsComplID.AsInteger;
            prPartInDel.ParamByName('DTYPE').AsInteger:=6;
            prPartInDel.ParamByName('IDATEINV').AsInteger:=Trunc(quDocsComplDATEDOC.AsDateTime);
            prPartInDel.ExecProc;

            //������� ��������� ������ �� ��������� � ���������� ��������������
            prDelPartOut.ParamByName('IDDOC').AsInteger:=quDocsComplID.AsInteger;
            prDelPartOut.ParamByName('DTYPE').AsInteger:=6;
            prDelPartOut.ExecProc;

            // 4 - �������� ������
            quDocsCompl.Edit;
            quDocsComplIACTIVE.AsInteger:=0;
            quDocsCompl.Post;
            quDocsCompl.Refresh;
          end;
        end;
      end;
    end;
  end;
end;

procedure TfmDocsCompl.Timer1Timer(Sender: TObject);
begin
  if bClearComplB=True then begin StatusBar1.Panels[0].Text:=''; bClearComplB:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearComplB:=True;
end;

procedure TfmDocsCompl.acAddDocManualExecute(Sender: TObject);
begin
//  �������� �������� �������
  if not CanDo('prAddCompl') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmO do
  with dmORep do
  begin
    fmAddCompl.Caption:='������������ (���� ����): ����� ��������.';
    fmAddCompl.cxTextEdit1.Text:=prGetNum(6,0);
    fmAddCompl.cxTextEdit1.Properties.ReadOnly:=False;
    fmAddCompl.cxTextEdit1.Tag:=0; //������� ���������� ��������� 
    fmAddCompl.cxDateEdit1.Date:=Date;
    fmAddCompl.cxDateEdit1.Properties.ReadOnly:=False;

    if quMHAll.Active=False then quMHAll.Active:=True;
    quMHAll.FullRefresh;

    fmAddCompl.cxLookupComboBox1.EditValue:=0;
    fmAddCompl.cxLookupComboBox1.Text:='';
    fmAddCompl.cxLookupComboBox1.Properties.ReadOnly:=False;

    if CurVal.IdMH<>0 then
    begin
      fmAddCompl.cxLookupComboBox1.EditValue:=CurVal.IdMH;
      fmAddCompl.cxLookupComboBox1.Text:=CurVal.NAMEMH;
    end else
    begin
       quMHAll.First;
       if not quMHAll.Eof then
       begin
         CurVal.IdMH:=quMHAllID.AsInteger;
         CurVal.NAMEMH:=quMHAllNAMEMH.AsString;
         fmAddCompl.cxLookupComboBox1.EditValue:=CurVal.IdMH;
         fmAddCompl.cxLookupComboBox1.Text:=CurVal.NAMEMH;
       end;
    end;
    if quMHAll.Locate('ID',CurVal.IdMH,[]) then
    begin
      fmAddCompl.Label15.Caption:='��. ����: '+quMHAllNAMEPRICE.AsString;
      fmAddCompl.Label15.Tag:=quMHAllDEFPRICE.AsInteger;
    end else
    begin
      fmAddCompl.Label15.Caption:='��. ����: ';
      fmAddCompl.Label15.Tag:=0;
    end;

    fmAddCompl.cxLabel1.Enabled:=True;
    fmAddCompl.cxLabel2.Enabled:=True;
    fmAddCompl.cxLabel7.Enabled:=True;
    fmAddCompl.cxLabel8.Enabled:=True;
    fmAddCompl.cxLabel9.Enabled:=True;

    fmAddCompl.cxButton1.Enabled:=True;

    fmAddCompl.ViewCom.OptionsData.Editing:=True;

    CloseTa(fmAddCompl.taSpec);
    CloseTa(fmAddCompl.taSpecC);
    CloseTa(taCalcB);

    fmAddCompl.ShowModal;
  end;
end;

procedure TfmDocsCompl.acAddDocBExecute(Sender: TObject);
begin
//��������

end;

procedure TfmDocsCompl.FormShow(Sender: TObject);
begin
  bStartOpen:=False;
end;

procedure TfmDocsCompl.acCopyExecute(Sender: TObject);
var Par:Variant;
    IDH:INteger;
    iNum:INteger;
begin
  //����������
  with dmO do
  with dmORep do
  begin
    if quDocsCompl.RecordCount=0 then exit;

    taHeadDoc.Active:=False;
    taHeadDoc.FileName:=CurDir+'HeadDoc.cds';
    if FileExists(CurDir+'HeadDoc.cds') then taHeadDoc.Active:=True
    else taHeadDoc.CreateDataSet;

    taSpecDoc.Active:=False;
    taSpecDoc.FileName:=CurDir+'SpecDoc.cds';
    if FileExists(CurDir+'SpecDoc.cds') then taSpecDoc.Active:=True
    else taSpecDoc.CreateDataSet;

    par := VarArrayCreate([0,1], varInteger);
    par[0]:=6; //������������
    par[1]:=quDocsComplID.AsInteger;
    if taHeadDoc.Locate('IType;Id',par,[])=False then
    begin
      taHeadDoc.Append;
      taHeadDocIType.AsInteger:=6;
      taHeadDocId.AsInteger:=quDocsComplID.AsInteger;
      taHeadDocDateDoc.AsInteger:=Trunc(quDocsComplDATEDOC.AsDateTime);
      taHeadDocNumDoc.AsString:=quDocsComplNUMDOC.AsString;
      taHeadDocIdCli.AsInteger:=0;
      taHeadDocNameCli.AsString:='';
      taHeadDocIdSkl.AsInteger:=quDocsComplIDSKL.AsInteger;
      taHeadDocNameSkl.AsString:=Copy(quDocsComplNAMEMH.AsString,1,70);
      taHeadDocSumIN.AsFloat:=quDocsComplSUMIN.AsFloat;
      taHeadDocSumUch.AsFloat:=quDocsComplSUMUCH.AsFloat;
      taHeadDoc.Post;

      IDH:=quDocsComplID.AsInteger;


      quSpecCompl.Active:=False;
      quSpecCompl.ParamByName('IDH').AsInteger:=IDH;
      quSpecCompl.Active:=True;

      quSpecCompl.First;  iNum:=1;     //����� ������ ����� �.�. ��������� �������������
      while not quSpecCompl.Eof do
      begin
        taSpecDoc.Append;
        taSpecDocIType.AsInteger:=6;
        taSpecDocIdHead.AsInteger:=IDH;
        taSpecDocNum.AsInteger:=iNum;
        taSpecDocIdCard.AsInteger:=quSpecComplIDCARD.AsInteger;
        taSpecDocQuant.AsFloat:=quSpecComplQUANT.AsFloat;
        taSpecDocPriceIn.AsFloat:=quSpecComplPRICEIN.AsFloat;
        taSpecDocSumIn.AsFloat:=quSpecComplSUMIN.AsFloat;
        taSpecDocPriceUch.AsFloat:=quSpecComplPRICEINUCH.AsFloat;
        taSpecDocSumUch.AsFloat:=quSpecComplSUMINUCH.AsFloat;
        taSpecDocIdNds.AsInteger:=0;
        taSpecDocSumNds.AsFloat:=0;
        taSpecDocNameC.AsString:=Copy(quSpecComplNAME.AsString,1,30);
        taSpecDocSm.AsString:=quSpecComplNAMESHORT.AsString;
        taSpecDocIdM.AsInteger:=quSpecComplIDM.AsInteger;
        taSpecDocKm.AsFloat:=quSpecComplKM.AsFloat;
        taSpecDocProcPrice.AsFloat:=0;
        taSpecDoc.Post;

        inc(iNum);

        quSpecCompl.Next;
      end;
      quSpecCompl.Active:=False;

    end else
    begin
      showmessage('�������� ��� ���� � ������.');
    end;
    taHeadDoc.Active:=False;
    taSpecDoc.Active:=False;
    taSpecDoc1.Active:=False;
  end;
end;

procedure TfmDocsCompl.acPastExecute(Sender: TObject);
Var iNum:Integer;
begin
//��������
  with dmO do
  with dmORep do
  begin
    taHeadDoc.Active:=False;
    taHeadDoc.FileName:=CurDir+'HeadDoc.cds';
    if FileExists(CurDir+'HeadDoc.cds') then taHeadDoc.Active:=True
    else taHeadDoc.CreateDataSet;

    taSpecDoc.Active:=False;
    taSpecDoc.FileName:=CurDir+'SpecDoc.cds';
    if FileExists(CurDir+'SpecDoc.cds') then taSpecDoc.Active:=True
    else taSpecDoc.CreateDataSet;

//    taSpecDoc1.Active:=False;  //��� �������
//    taSpecDoc1.FileName:=CurDir+'SpecDoc1.cds';
//    if FileExists(CurDir+'SpecDoc1.cds') then taSpecDoc1.Active:=True
//    else taSpecDoc1.CreateDataSet;

    fmTBuff:=TfmTBuff.Create(Application);

    fmTBuff.LevelTH.Visible:=False;
    fmTBuff.LevelTS.Visible:=False;
    fmTBuff.LevelD.Visible:=True;
    fmTBuff.LevelDSpec.Visible:=True;

    fmTBuff.ShowModal;
    if fmTBuff.ModalResult=mrOk then
    begin //���������
      fmTBuff.Release;
      if taHeadDoc.RecordCount>0 then
      begin
        if CanDo('prAddCompl') then
        begin
          prAllViewOff;

          fmAddCompl.Caption:='������������ (���� ����): ����� ��������.';
          fmAddCompl.cxTextEdit1.Text:=prGetNum(6,0);
          fmAddCompl.cxTextEdit1.Properties.ReadOnly:=False;
          fmAddCompl.cxTextEdit1.Tag:=0; //������� ���������� ���������
          fmAddCompl.cxDateEdit1.Date:=Date;
          fmAddCompl.cxDateEdit1.Properties.ReadOnly:=False;

          if quMHAll.Active=False then quMHAll.Active:=True;
          quMHAll.FullRefresh;

          fmAddCompl.cxLookupComboBox1.EditValue:=taHeadDocIdSkl.AsInteger;
          fmAddCompl.cxLookupComboBox1.Text:=taHeadDocNameSkl.AsString;
          fmAddCompl.cxLookupComboBox1.Properties.ReadOnly:=False;


          if quMHAll.Locate('ID',taHeadDocIdSkl.AsInteger,[]) then
          begin
            fmAddCompl.Label15.Caption:='��. ����: '+quMHAllNAMEPRICE.AsString;
            fmAddCompl.Label15.Tag:=quMHAllDEFPRICE.AsInteger;
          end else
          begin
            fmAddCompl.Label15.Caption:='��. ����: ';
            fmAddCompl.Label15.Tag:=0;
          end;

          fmAddCompl.cxLabel1.Enabled:=True;
          fmAddCompl.cxLabel2.Enabled:=True;
          fmAddCompl.cxLabel7.Enabled:=True;
          fmAddCompl.cxLabel8.Enabled:=True;
          fmAddCompl.cxLabel9.Enabled:=True;

          fmAddCompl.cxButton1.Enabled:=True;

          fmAddCompl.ViewCom.OptionsData.Editing:=True;

          CloseTa(fmAddCompl.taSpec);
          CloseTa(fmAddCompl.taSpecC);
          CloseTa(taCalcB);

          taSpecDoc.First; iNum:=1;
          while not taSpecDoc.Eof do
          begin
            if (taSpecDocIType.AsInteger=taHeadDocIType.AsInteger)and(taSpecDocIdHead.AsInteger=taHeadDocId.AsInteger) then
            begin

              with fmAddCompl do
              begin
                taSpec.Append;
                taSpecNum.AsInteger:=iNum;
                taSpecIdGoods.AsInteger:=taSpecDocIdCard.AsInteger;
                taSpecNameG.AsString:=taSpecDocNameC.AsString;
                taSpecIM.AsInteger:=taSpecDocIdM.AsInteger;
                taSpecSM.AsString:=taSpecDocSm.AsString;
                taSpecQuantFact.AsFloat:=RoundEx(taSpecDocQuant.AsFloat*1000)/1000;
                taSpecPriceIn.AsFloat:=taSpecDocPriceIn.AsFloat;
                taSpecSumIn.AsFloat:=taSpecDocSumIn.AsFloat;
                taSpecPriceUch.AsFloat:=taSpecDocPriceUch.AsFloat;
                taSpecSumUch.AsFloat:=taSpecDocSumUch.AsFloat;
                taSpecKm.AsFloat:=taSpecDocKm.AsFloat;
                taSpecTCard.AsInteger:=prTypeTC(taSpecDocIdCard.AsInteger);
                taSpec.Post;
                inc(iNum);
              end;
            end;
            taSpecDoc.Next;
          end;

          taHeadDoc.Active:=False;
          taSpecDoc.Active:=False;

          prAllViewOn;

          fmAddCompl.ShowModal;

        end else showmessage('��� ����.');
      end;
    end else
    begin
      fmTBuff.Release;
      taHeadDoc.Active:=False;
      taSpecDoc.Active:=False;
    end;
  end;
end;

procedure TfmDocsCompl.Excel1Click(Sender: TObject);
begin
  prNExportExel4(ViewComplB);
end;

procedure TfmDocsCompl.acPrintSebExecute(Sender: TObject);
begin
  //������������� ������
  if not CanDo('prComplSeb') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  with dmO do
  with dmORep do
  begin
    if quDocsCompl.RecordCount>0 then
    begin
      if quDocsComplIACTIVE.AsInteger=1 then
      begin
        quRepSeb.Active:=False;
        quRepSeb.ParamByName('IDHD').AsInteger:=quDocsComplID.AsInteger;
        quRepSeb.Active:=True;

        RepCompl.LoadFromFile(CurDir + 'ComplSeb.frf');

        frVariables.Variable['DocNum']:=quDocsComplNUMDOC.AsString;
        frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',quDocsComplDATEDOC.AsDateTime);
        frVariables.Variable['DocStore']:=quDocsComplNAMEMH.AsString;
        frVariables.Variable['Depart']:=CommonSet.DepartName;
        frVariables.Variable['SumIn']:=quDocsComplSUMIN.AsFloat;
//        frVariables.Variable['Comment']:=quDocsComplCOMMENT.AsString;

        RepCompl.ReportName:='�������� ����.';
        RepCompl.PrepareReport;
        RepCompl.ShowPreparedReport;

        quRepSeb.Active:=False;
      end else showmessage('������ ����������, �������� ������ ���� �����������.');
    end else
    begin
      showmessage('�������� �������� ��� ������.');
    end;
  end;
end;

end.
