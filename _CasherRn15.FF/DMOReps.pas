unit DMOReps;

interface

uses
  SysUtils, Classes, DB, DBClient, frexpimg, frRtfExp, frTXTExp, frXMLExl,
  frOLEExl, FR_E_HTML2, FR_E_HTM, FR_E_CSV, FR_E_RTF, FR_Class, FR_E_TXT,
  FR_DSet, FR_DBSet, FIBDataSet, pFIBDataSet, FIBDatabase, pFIBDatabase;

type
  TdmORep = class(TDataModule)
    taPartTest: TClientDataSet;
    taPartTestNum: TIntegerField;
    taPartTestIdGoods: TIntegerField;
    taPartTestNameG: TStringField;
    taPartTestIM: TIntegerField;
    taPartTestSM: TStringField;
    taPartTestQuant: TFloatField;
    taPartTestPrice1: TCurrencyField;
    taPartTestiRes: TSmallintField;
    dsPartTest: TDataSource;
    taCalc: TClientDataSet;
    taCalcArticul: TIntegerField;
    taCalcName: TStringField;
    taCalcIdm: TIntegerField;
    taCalcsM: TStringField;
    taCalcKm: TFloatField;
    taCalcQuant: TFloatField;
    taCalcQuantFact: TFloatField;
    taCalcQuantDiff: TFloatField;
    dsCalc: TDataSource;
    frRep1: TfrReport;
    frTextExport1: TfrTextExport;
    frRTFExport1: TfrRTFExport;
    frCSVExport1: TfrCSVExport;
    frHTMExport1: TfrHTMExport;
    frHTML2Export1: TfrHTML2Export;
    frOLEExcelExport1: TfrOLEExcelExport;
    frXMLExcelExport1: TfrXMLExcelExport;
    frTextAdvExport1: TfrTextAdvExport;
    frRtfAdvExport1: TfrRtfAdvExport;
    frBMPExport1: TfrBMPExport;
    frJPEGExport1: TfrJPEGExport;
    frTIFFExport1: TfrTIFFExport;
    taCalcSumUch: TFloatField;
    taCalcSumIn: TFloatField;
    taCalcB: TClientDataSet;
    taCalcBCODEB: TIntegerField;
    taCalcBNAMEB: TStringField;
    taCalcBQUANT: TFloatField;
    taCalcBPRICEOUT: TFloatField;
    taCalcBSUMOUT: TFloatField;
    taCalcBID: TIntegerField;
    taCalcBIDCARD: TIntegerField;
    taCalcBNAMEC: TStringField;
    taCalcBQUANTC: TFloatField;
    taCalcBPRICEIN: TFloatField;
    taCalcBSUMIN: TFloatField;
    taCalcBIM: TIntegerField;
    taCalcBSM: TStringField;
    dsCalcB: TDataSource;
    frdsCalcB: TfrDBDataSet;
    taCalcBSB: TStringField;
    taTORep: TClientDataSet;
    dsTORep: TDataSource;
    taTORepsType: TStringField;
    taTORepsDocType: TStringField;
    taTORepsCliName: TStringField;
    taTORepsDate: TStringField;
    taTORepsDocNum: TStringField;
    taTOReprSum: TCurrencyField;
    taTOReprSum1: TCurrencyField;
    frdsTORep: TfrDBDataSet;
    taTOReprSumO: TCurrencyField;
    taTOReprSumO1: TCurrencyField;
    taCMove: TClientDataSet;
    dstaCMove: TDataSource;
    taCMoveARTICUL: TIntegerField;
    taCMoveIDATE: TIntegerField;
    taCMoveIDSTORE: TIntegerField;
    taCMoveNAMEMH: TStringField;
    taCMoveRB: TFloatField;
    taCMovePOSTIN: TFloatField;
    taCMovePOSTOUT: TFloatField;
    taCMoveVNIN: TFloatField;
    taCMoveVNOUT: TFloatField;
    taCMoveINV: TFloatField;
    taCMoveQREAL: TFloatField;
    taCMoveRE: TFloatField;
    quDocsInvSel: TpFIBDataSet;
    quDocsInvSelID: TFIBIntegerField;
    quDocsInvSelDATEDOC: TFIBDateField;
    quDocsInvSelNUMDOC: TFIBStringField;
    quDocsInvSelIDSKL: TFIBIntegerField;
    quDocsInvSelNAMEMH: TFIBStringField;
    quDocsInvSelIACTIVE: TFIBIntegerField;
    quDocsInvSelITYPE: TFIBIntegerField;
    quDocsInvSelSUM1: TFIBFloatField;
    quDocsInvSelSUM11: TFIBFloatField;
    quDocsInvSelSUM2: TFIBFloatField;
    quDocsInvSelSUM21: TFIBFloatField;
    quDocsInvSelSUMDIFIN: TFIBFloatField;
    quDocsInvSelSUMDIFUCH: TFIBFloatField;
    dsquDocsInvSel: TDataSource;
    quDocsInvId: TpFIBDataSet;
    quDocsInvIdID: TFIBIntegerField;
    quDocsInvIdDATEDOC: TFIBDateField;
    quDocsInvIdNUMDOC: TFIBStringField;
    quDocsInvIdIDSKL: TFIBIntegerField;
    quDocsInvIdIACTIVE: TFIBIntegerField;
    quDocsInvIdITYPE: TFIBIntegerField;
    quDocsInvIdSUM1: TFIBFloatField;
    quDocsInvIdSUM11: TFIBFloatField;
    quDocsInvIdSUM2: TFIBFloatField;
    quDocsInvIdSUM21: TFIBFloatField;
    quSpecInv: TpFIBDataSet;
    quSpecInvIDHEAD: TFIBIntegerField;
    quSpecInvID: TFIBIntegerField;
    quSpecInvNUM: TFIBIntegerField;
    quSpecInvIDCARD: TFIBIntegerField;
    quSpecInvQUANT: TFIBFloatField;
    quSpecInvSUMIN: TFIBFloatField;
    quSpecInvSUMUCH: TFIBFloatField;
    quSpecInvIDMESSURE: TFIBIntegerField;
    quSpecInvQUANTFACT: TFIBFloatField;
    quSpecInvSUMINFACT: TFIBFloatField;
    quSpecInvSUMUCHFACT: TFIBFloatField;
    quSpecInvKM: TFIBFloatField;
    quSpecInvTCARD: TFIBSmallIntField;
    quSpecInvIDGROUP: TFIBIntegerField;
    quSpecInvNAMESHORT: TFIBStringField;
    quSpecInvNAMECL: TFIBStringField;
    quSpecInvNAME: TFIBStringField;
    quSpecInvC: TpFIBDataSet;
    quSpecInvCIDHEAD: TFIBIntegerField;
    quSpecInvCID: TFIBIntegerField;
    quSpecInvCNUM: TFIBIntegerField;
    quSpecInvCIDCARD: TFIBIntegerField;
    quSpecInvCQUANT: TFIBFloatField;
    quSpecInvCSUMIN: TFIBFloatField;
    quSpecInvCSUMUCH: TFIBFloatField;
    quSpecInvCIDMESSURE: TFIBIntegerField;
    quSpecInvCQUANTFACT: TFIBFloatField;
    quSpecInvCSUMINFACT: TFIBFloatField;
    quSpecInvCSUMUCHFACT: TFIBFloatField;
    quSpecInvCKM: TFIBFloatField;
    quSpecInvCTCARD: TFIBSmallIntField;
    quSpecInvCIDGROUP: TFIBIntegerField;
    quSpecInvCNAMESHORT: TFIBStringField;
    quSpecInvCNAMECL: TFIBStringField;
    quSpecInvCNAME: TFIBStringField;
    quSelPrib: TpFIBDataSet;
    quSelPribGr: TpFIBDataSet;
    trSelRep: TpFIBTransaction;
    quSelPribNAMECL: TFIBStringField;
    quSelPribPARENT: TFIBIntegerField;
    quSelPribCODEB: TFIBIntegerField;
    quSelPribNAME: TFIBStringField;
    quSelPribQUANT: TFIBFloatField;
    quSelPribSUMOUT: TFIBFloatField;
    quSelPribSUMIN: TFIBFloatField;
    quSelPribGrNAMECL: TFIBStringField;
    quSelPribGrPARENT: TFIBIntegerField;
    quSelPribGrQUANT: TFIBFloatField;
    quSelPribGrSUMOUT: TFIBFloatField;
    quSelPribGrSUMIN: TFIBFloatField;
    quDocOutBPrint: TpFIBDataSet;
    quDocOutBPrintIDB: TFIBIntegerField;
    quDocOutBPrintCODEB: TFIBIntegerField;
    quDocOutBPrintQUANT: TFIBFloatField;
    quDocOutBPrintSUMOUT: TFIBFloatField;
    quDocOutBPrintIDSKL: TFIBIntegerField;
    quDocOutBPrintSUMIN: TFIBFloatField;
    quDocOutBPrintNAMEB: TFIBStringField;
    quDocOutBPrintPRICER: TFIBFloatField;
    quSelInSum: TpFIBDataSet;
    quSelInSumARTICUL: TFIBIntegerField;
    quSelInSumSUMIN: TFIBFloatField;
    quSelInSumQSUM: TFIBFloatField;
    quSelNamePos: TpFIBDataSet;
    quSelNamePosNAMECL: TFIBStringField;
    quSelNamePosPARENT: TFIBIntegerField;
    quSelNamePosNAME: TFIBStringField;
    quSelNamePosSM: TFIBStringField;
    quSelNamePosKOEF: TFIBFloatField;
    quSelNamePosIMESSURE: TFIBIntegerField;
    quSelOutSum: TpFIBDataSet;
    quSelOutSumARTICUL: TFIBIntegerField;
    quSelOutSumSUMOUT: TFIBFloatField;
    quSelOutSumQSUM: TFIBFloatField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmORep: TdmORep;

implementation

uses dmOffice;

{$R *.dfm}

end.
