object fmAddClients: TfmAddClients
  Left = 328
  Top = 183
  BorderStyle = bsDialog
  Caption = #1044#1086#1073#1072#1074#1083#1077#1085#1080#1077' '#1082#1086#1085#1090#1088#1072#1075#1077#1085#1090#1072
  ClientHeight = 384
  ClientWidth = 583
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 32
    Top = 24
    Width = 50
    Height = 13
    Caption = #1053#1072#1079#1074#1072#1085#1080#1077
    Transparent = True
  end
  object Label2: TLabel
    Left = 8
    Top = 56
    Width = 89
    Height = 13
    Caption = #1055#1086#1083#1085#1086#1077' '#1085#1072#1079#1074#1072#1085#1080#1077
    Transparent = True
  end
  object Label3: TLabel
    Left = 40
    Top = 88
    Width = 24
    Height = 13
    Caption = #1048#1053#1053
    Transparent = True
  end
  object Label4: TLabel
    Left = 32
    Top = 168
    Width = 45
    Height = 13
    Caption = #1058#1077#1083#1077#1092#1086#1085
    Transparent = True
  end
  object Label5: TLabel
    Left = 288
    Top = 168
    Width = 49
    Height = 13
    Caption = #1054#1090#1074'. '#1083#1080#1094#1086
    Transparent = True
  end
  object Label6: TLabel
    Left = 24
    Top = 200
    Width = 70
    Height = 13
    Caption = #1050#1086#1084#1084#1077#1085#1090#1072#1088#1080#1081
    Transparent = True
  end
  object Label7: TLabel
    Left = 272
    Top = 88
    Width = 23
    Height = 13
    Caption = #1050#1055#1055
    Transparent = True
  end
  object Label8: TLabel
    Left = 40
    Top = 128
    Width = 31
    Height = 13
    Caption = #1040#1076#1088#1077#1089
    Transparent = True
  end
  object Panel1: TPanel
    Left = 0
    Top = 331
    Width = 583
    Height = 53
    Align = alBottom
    BevelInner = bvLowered
    Color = 16777173
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 128
      Top = 16
      Width = 89
      Height = 25
      Caption = 'Ok'
      Default = True
      ModalResult = 1
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 360
      Top = 16
      Width = 89
      Height = 25
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      Glyph.Data = {
        5E040000424D5E04000000000000360000002800000012000000130000000100
        18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
        8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
        0000CED3D6848684848684848684848684848684848684848684848684848684
        848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
        7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
        FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
        FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
        007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
        FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
        00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
        494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
        00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000}
      LookAndFeel.Kind = lfOffice11
    end
  end
  object cxTextEdit1: TcxTextEdit
    Left = 104
    Top = 20
    Properties.MaxLength = 100
    Style.Shadow = True
    TabOrder = 1
    Text = 'cxTextEdit1'
    Width = 257
  end
  object cxTextEdit2: TcxTextEdit
    Left = 104
    Top = 52
    Properties.MaxLength = 300
    Style.Shadow = True
    TabOrder = 2
    Text = 'cxTextEdit2'
    Width = 449
  end
  object cxTextEdit3: TcxTextEdit
    Left = 104
    Top = 84
    Properties.MaxLength = 15
    Style.Shadow = True
    TabOrder = 3
    Text = 'cxTextEdit3'
    Width = 129
  end
  object cxTextEdit4: TcxTextEdit
    Left = 104
    Top = 164
    Properties.MaxLength = 50
    Style.Shadow = True
    TabOrder = 6
    Text = 'cxTextEdit4'
    Width = 129
  end
  object cxTextEdit5: TcxTextEdit
    Left = 344
    Top = 164
    Properties.MaxLength = 100
    Style.Shadow = True
    TabOrder = 7
    Text = 'cxTextEdit5'
    Width = 209
  end
  object cxMemo1: TcxMemo
    Left = 128
    Top = 200
    Lines.Strings = (
      'cxMemo1')
    Style.Shadow = True
    TabOrder = 8
    Height = 65
    Width = 425
  end
  object cxCheckBox1: TcxCheckBox
    Left = 32
    Top = 280
    Caption = #1055#1083#1072#1090#1077#1083#1100#1097#1080#1082' '#1053#1044#1057
    Properties.ValueChecked = 1
    Properties.ValueUnchecked = 0
    State = cbsChecked
    TabOrder = 9
    Width = 121
  end
  object cxTextEdit6: TcxTextEdit
    Left = 336
    Top = 84
    Properties.MaxLength = 15
    Style.Shadow = True
    TabOrder = 4
    Text = 'cxTextEdit6'
    Width = 129
  end
  object cxTextEdit7: TcxTextEdit
    Left = 104
    Top = 124
    Properties.MaxLength = 50
    Style.Shadow = True
    TabOrder = 5
    Text = 'cxTextEdit7'
    Width = 449
  end
end
