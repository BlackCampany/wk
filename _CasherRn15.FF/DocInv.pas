unit DocInv;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SpeedBar, ExtCtrls, ComCtrls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Placemnt, cxImageComboBox, XPStyleActnCtrls, ActnList, ActnMan,
  cxCalendar, cxContainer, cxTextEdit, cxMemo;

type
  TfmDocsInv = class(TForm)
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    Timer1: TTimer;
    FormPlacement1: TFormPlacement;
    GridDocsInv: TcxGrid;
    ViewDocsInv: TcxGridDBTableView;
    LevelDocsInv: TcxGridLevel;
    amDocsInv: TActionManager;
    acPeriod: TAction;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    SpeedItem6: TSpeedItem;
    acAddDoc1: TAction;
    acEditDoc1: TAction;
    acViewDoc1: TAction;
    acDelDoc1: TAction;
    acOnDoc1: TAction;
    acOffDoc1: TAction;
    SpeedItem7: TSpeedItem;
    SpeedItem8: TSpeedItem;
    acVid: TAction;
    SpeedItem9: TSpeedItem;
    LevelCardsInv: TcxGridLevel;
    ViewCardsInv: TcxGridDBTableView;
    ViewCardsInvNAME: TcxGridDBColumn;
    ViewCardsInvNAMESHORT: TcxGridDBColumn;
    ViewCardsInvIDCARD: TcxGridDBColumn;
    ViewCardsInvQUANT: TcxGridDBColumn;
    ViewCardsInvPRICEIN: TcxGridDBColumn;
    ViewCardsInvSUMIN: TcxGridDBColumn;
    ViewCardsInvPRICEUCH: TcxGridDBColumn;
    ViewCardsInvSUMUCH: TcxGridDBColumn;
    ViewCardsInvIDNDS: TcxGridDBColumn;
    ViewCardsInvSUMNDS: TcxGridDBColumn;
    ViewCardsInvDATEDOC: TcxGridDBColumn;
    ViewCardsInvNUMDOC: TcxGridDBColumn;
    ViewCardsInvNAMECL: TcxGridDBColumn;
    ViewCardsInvNAMEMH: TcxGridDBColumn;
    ViewDocsInvID: TcxGridDBColumn;
    ViewDocsInvDATEDOC: TcxGridDBColumn;
    ViewDocsInvNUMDOC: TcxGridDBColumn;
    ViewDocsInvIDSKL: TcxGridDBColumn;
    ViewDocsInvNAMEMH: TcxGridDBColumn;
    ViewDocsInvIACTIVE: TcxGridDBColumn;
    ViewDocsInvITYPE: TcxGridDBColumn;
    ViewDocsInvSUM1: TcxGridDBColumn;
    ViewDocsInvSUM11: TcxGridDBColumn;
    ViewDocsInvSUM2: TcxGridDBColumn;
    ViewDocsInvSUM21: TcxGridDBColumn;
    ViewDocsInvSUMDIFIN: TcxGridDBColumn;
    ViewDocsInvSUMDIFUCH: TcxGridDBColumn;
    Panel1: TPanel;
    Memo1: TcxMemo;
    procedure SpeedItem1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acPeriodExecute(Sender: TObject);
    procedure acAddDoc1Execute(Sender: TObject);
    procedure acEditDoc1Execute(Sender: TObject);
    procedure acViewDoc1Execute(Sender: TObject);
    procedure ViewDocsInvDblClick(Sender: TObject);
    procedure acDelDoc1Execute(Sender: TObject);
    procedure acOnDoc1Execute(Sender: TObject);
    procedure acOffDoc1Execute(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure acVidExecute(Sender: TObject);
    procedure SpeedItem1Click0(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmDocsInv: TfmDocsInv;
  bClearDocIn:Boolean = false;

implementation

uses Un1, dmOffice, PeriodUni, AddDoc1, AddInv, DMOReps;

{$R *.dfm}

procedure TfmDocsInv.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmDocsInv.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  Timer1.Enabled:=True;
  GridDocsInv.Align:=AlClient;
  ViewDocsInv.RestoreFromIniFile(CurDir+GridIni);
  ViewCardsInv.RestoreFromIniFile(CurDir+GridIni);
  StatusBar1.Color:=$00FFCACA;
  Memo1.Clear;
end;

procedure TfmDocsInv.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewDocsInv.StoreToIniFile(CurDir+GridIni,False);
  ViewCardsInv.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmDocsInv.acPeriodExecute(Sender: TObject);
begin
//  ������
  fmPeriodUni.DateTimePicker1.Date:=CommonSet.DateFrom;
//  fmPeriodUni.DateTimePicker2.Date:=CommonSet.DateTo-1;
  fmPeriodUni.ShowModal;
  if fmPeriodUni.ModalResult=mrOk then
  begin
    CommonSet.DateFrom:=Trunc(fmPeriodUni.DateTimePicker1.Date);
    CommonSet.DateTo:=Trunc(fmPeriodUni.DateTimePicker2.Date)+1;

    with dmO do
    with dmORep do
    begin
      if LevelDocsInv.Visible then
      begin
        if CommonSet.DateTo>=iMaxDate then fmDocsInv.Caption:='�������������� � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
        else fmDocsInv.Caption:='�������������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);

        ViewDocsInv.BeginUpdate;
        quDocsInvSel.Active:=False;
        quDocsInvSel.ParamByName('DATEB').AsDate:=CommonSet.DateFrom;
        quDocsInvSel.ParamByName('DATEE').AsDate:=CommonSet.DateTo;
        quDocsInvSel.Active:=True;
        ViewDocsInv.EndUpdate;
      end else
      begin
        if CommonSet.DateTo>=iMaxDate then fmDocsInv.Caption:='������� �� ������� � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
        else fmDocsInv.Caption:='������� �� ������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);

        ViewCardsInv.BeginUpdate;
{       quDocsInCard.Active:=False;
        quDocsInCard.ParamByName('DATEB').AsDate:=CommonSet.DateFrom;
        quDocsInCard.ParamByName('DATEE').AsDate:=CommonSet.DateTo;
        quDocsInCard.Active:=True;}
        ViewCardsInv.EndUpdate;
      end;
    end;
  end;
end;

procedure TfmDocsInv.acAddDoc1Execute(Sender: TObject);
//Var IDH:INteger;
//    rSum1,rSum2:Real;
begin
  //�������� ��������

  if not CanDo('prAddDocInv') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmO do
  begin
    fmAddInv.Caption:='��������������: ����� ��������.';
    fmAddInv.cxTextEdit1.Text:='';
    fmAddInv.cxTextEdit1.Properties.ReadOnly:=False;
    fmAddInv.cxTextEdit1.Tag:=0; //������� ���������� ��������� 
    fmAddInv.cxDateEdit1.Date:=Date;
    fmAddInv.cxDateEdit1.Properties.ReadOnly:=False;

    if quMHAll.Active=False then quMHAll.Active:=True;

    fmAddInv.cxLookupComboBox1.EditValue:=0;
    fmAddInv.cxLookupComboBox1.Text:='';
    fmAddInv.cxLookupComboBox1.Properties.ReadOnly:=False;

    if CurVal.IdMH<>0 then
    begin
      fmAddInv.cxLookupComboBox1.EditValue:=CurVal.IdMH;
      fmAddInv.cxLookupComboBox1.Text:=CurVal.NAMEMH;
    end else
    begin
       quMHAll.First;
       if not quMHAll.Eof then
       begin
         CurVal.IdMH:=quMHAllID.AsInteger;
         CurVal.NAMEMH:=quMHAllNAMEMH.AsString;
         fmAddInv.cxLookupComboBox1.EditValue:=CurVal.IdMH;
         fmAddInv.cxLookupComboBox1.Text:=CurVal.NAMEMH;
       end;
    end;
    if quMHAll.Locate('ID',CurVal.IdMH,[]) then
    begin
      fmAddInv.Label15.Caption:='��. ����: '+quMHAllNAMEPRICE.AsString;
      fmAddInv.Label15.Tag:=quMHAllDEFPRICE.AsInteger;
    end else
    begin
      fmAddInv.Label15.Caption:='��. ����: ';
      fmAddInv.Label15.Tag:=0;
    end;

    fmAddInv.cxLabel1.Enabled:=True;
    fmAddInv.cxLabel2.Enabled:=True;

    fmAddInv.cxButton1.Enabled:=True;
    fmAddInv.taSpec.Active:=False;
    fmAddInv.taSpec.CreateDataSet;
    fmAddInv.taSpecC.Active:=False;
    fmAddInv.taSpecC.CreateDataSet;

    fmAddInv.ShowModal;
  end;
end;

procedure TfmDocsInv.acEditDoc1Execute(Sender: TObject);
Var IDH:INteger;
begin
  //�������������
  if not CanDo('prEditDocInv') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmO do
  with dmORep do
  begin
    if quDocsInvSel.RecordCount>0 then //���� ��� �������������
    begin
      if quDocsInvSelIACTIVE.AsInteger=0 then
      begin
        Memo1.Clear;
        Memo1.Lines.Add('����� ���� �������� ���������.'); Delay(10);

        fmAddInv.Caption:='��������������: ��������������.';
        fmAddInv.cxTextEdit1.Text:=quDocsInvSelNUMDOC.AsString;
        fmAddInv.cxTextEdit1.Properties.ReadOnly:=False;
        fmAddInv.cxTextEdit1.Tag:=quDocsInvSelId.AsInteger;
        fmAddInv.cxDateEdit1.Date:=quDocsInvSelDATEDOC.AsDateTime;
        fmAddInv.cxDateEdit1.Properties.ReadOnly:=False;

        if quMHAll.Active=False then quMHAll.Active:=True;
        fmAddInv.cxLookupComboBox1.EditValue:=quDocsInvSelIDSKL.AsInteger;
        fmAddInv.cxLookupComboBox1.Text:=quDocsInvSelNAMEMH.AsString;
        fmAddInv.cxLookupComboBox1.Properties.ReadOnly:=False;

        CurVal.IdMH:=quDocsInvSelIDSKL.AsInteger;
        CurVal.NAMEMH:=quDocsInvSelNAMEMH.AsString;

        if quMHAll.Locate('ID',CurVal.IdMH,[]) then
        begin
          fmAddInv.Label15.Caption:='��. ����: '+quMHAllNAMEPRICE.AsString;
          fmAddInv.Label15.Tag:=quMHAllDEFPRICE.AsInteger;
        end else
        begin
          fmAddInv.Label15.Caption:='��. ����: ';
          fmAddInv.Label15.Tag:=0;
        end;

        fmAddInv.cxLabel1.Enabled:=True;
        fmAddInv.cxLabel2.Enabled:=True;
        fmAddInv.cxLabel7.Enabled:=True;
        fmAddInv.cxLabel8.Enabled:=True;
        fmAddInv.cxLabel9.Enabled:=True;
        fmAddInv.cxButton1.Enabled:=True;

        fmAddInv.taSpec.Active:=False;
        fmAddInv.taSpec.CreateDataSet;
        fmAddInv.taSpecC.Active:=False;
        fmAddInv.taSpecC.CreateDataSet;

        IDH:=quDocsInvSelID.AsInteger;

        Memo1.Lines.Add('   ������ � �����.'); Delay(10);

        quSpecInv.Active:=False;
        quSpecInv.ParamByName('IDH').AsInteger:=IDH;
        quSpecInv.Active:=True;

        quSpecInv.First;
        while not quSpecInv.Eof do
        begin
          with fmAddInv do
          begin
            taSpec.Append;
            taSpecNum.AsInteger:=quSpecInvNUM.AsInteger;
            taSpecIdGoods.AsInteger:=quSpecInvIDCARD.AsInteger;
            taSpecNameG.AsString:=quSpecInvNAME.AsString;
            taSpecIM.AsInteger:=quSpecInvIDMESSURE.AsInteger;
            taSpecSM.AsString:=quSpecInvNAMESHORT.AsString;
            taSpecQuant.AsFloat:=quSpecInvQUANT.AsFloat;
            taSpecSumIn.AsFloat:=quSpecInvSUMIN.AsFloat;
            taSpecSumUch.AsFloat:=quSpecInvSUMUCH.AsFloat;
            taSpecPriceIn.AsFloat:=0; taSpecPriceUch.AsFloat:=0;
            if quSpecInvQUANT.AsFloat<>0 then
            begin
              taSpecPriceIn.AsFloat:=quSpecInvSUMIN.AsFloat/quSpecInvQUANT.AsFloat;
              taSpecPriceUch.AsFloat:=quSpecInvSUMUCH.AsFloat/quSpecInvQUANT.AsFloat;
            end;
            taSpecQuantFact.AsFloat:=quSpecInvQUANTFACT.AsFloat;
            taSpecSumInF.AsFloat:=quSpecInvSUMINFACT.AsFloat;
            taSpecSumUchF.AsFloat:=quSpecInvSUMUCHFACT.AsFloat;
            taSpecPriceInF.AsFloat:=0; taSpecPriceUchF.AsFloat:=0;
            if quSpecInvQUANTFACT.AsFloat<>0 then
            begin
              taSpecPriceInF.AsFloat:=quSpecInvSUMINFACT.AsFloat/quSpecInvQUANTFACT.AsFloat;
              taSpecPriceUchF.AsFloat:=quSpecInvSUMUCHFACT.AsFloat/quSpecInvQUANTFACT.AsFloat;
            end;

            taSpecKm.AsFloat:=quSpecInvKM.AsFloat;
            taSpecTCard.AsInteger:=quSpecInvTCARD.AsInteger;
            taSpecId_Group.AsInteger:=quSpecInvIDGROUP.AsInteger;
            taSpecNameGr.AsString:=quSpecInvNAMECL.AsString;
            taSpec.Post;
          end;
          quSpecInv.Next;
        end;

        Memo1.Lines.Add('   ������.'); Delay(10);

        quSpecInvC.Active:=False;
        quSpecInvC.ParamByName('IDH').AsInteger:=IDH;
        quSpecInvC.Active:=True;

        quSpecInvC.First;
        while not quSpecInvC.Eof do
        begin
          with fmAddInv do
          begin
            taSpecC.Append;
            taSpecCNum.AsInteger:=quSpecInvCNUM.AsInteger;
            taSpecCIdGoods.AsInteger:=quSpecInvCIDCARD.AsInteger;
            taSpecCNameG.AsString:=quSpecInvCNAME.AsString;
            taSpecCIM.AsInteger:=quSpecInvCIDMESSURE.AsInteger;
            taSpecCSM.AsString:=quSpecInvCNAMESHORT.AsString;
            taSpecCQuant.AsFloat:=quSpecInvCQUANT.AsFloat;
            taSpecCSumIn.AsFloat:=quSpecInvCSUMIN.AsFloat;
            taSpecCSumUch.AsFloat:=quSpecInvCSUMUCH.AsFloat;
            taSpecCPriceIn.AsFloat:=0; taSpecCPriceUch.AsFloat:=0;
            if quSpecInvCQUANT.AsFloat<>0 then
            begin
              taSpecCPriceIn.AsFloat:=quSpecInvCSUMIN.AsFloat/quSpecInvCQUANT.AsFloat;
              taSpecCPriceUch.AsFloat:=quSpecInvCSUMUCH.AsFloat/quSpecInvCQUANT.AsFloat;
            end;
            taSpecCQuantFact.AsFloat:=quSpecInvCQUANTFACT.AsFloat;
            taSpecCSumInF.AsFloat:=quSpecInvCSUMINFACT.AsFloat;
            taSpecCSumUchF.AsFloat:=quSpecInvCSUMUCHFACT.AsFloat;
            taSpecCPriceInF.AsFloat:=0; taSpecCPriceUchF.AsFloat:=0;
            if quSpecInvCQUANTFACT.AsFloat<>0 then
            begin
              taSpecCPriceInF.AsFloat:=quSpecInvCSUMINFACT.AsFloat/quSpecInvCQUANTFACT.AsFloat;
              taSpecCPriceUchF.AsFloat:=quSpecInvCSUMUCHFACT.AsFloat/quSpecInvCQUANTFACT.AsFloat;
            end;

            taSpecCKm.AsFloat:=quSpecInvCKM.AsFloat;
            taSpecCTCard.AsInteger:=quSpecInvCTCARD.AsInteger;
            taSpecCId_Group.AsInteger:=quSpecInvCIDGROUP.AsInteger;
            taSpecCNameGr.AsString:=quSpecInvCNAMECL.AsString;
            taSpecC.Post;
          end;
          quSpecInvC.Next;
        end;
        Memo1.Lines.Add('   �����������.'); Delay(10);

        Memo1.Lines.Add('   �������� ��.'); Delay(10);
        fmAddInv.ShowModal;
      end else
      begin
        showmessage('������������� �������������� �������� ������.');
      end;
    end else
    begin
      showmessage('�������� �������� ��� ��������������.');
    end;//}
  end;
end;

procedure TfmDocsInv.acViewDoc1Execute(Sender: TObject);
//Var IDH:INteger;
begin
  //��������
  if not CanDo('prViewDocIn') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmO do
  begin
{    if quDocsInSel.RecordCount>0 then //���� ��� �������������
    begin
      fmAddInv.Caption:='���������: ��������.';
      fmAddInv.cxTextEdit1.Text:=quDocsInSelNUMDOC.AsString;
      fmAddInv.cxTextEdit1.Properties.ReadOnly:=True;
      fmAddInv.cxTextEdit2.Text:=quDocsInSelNUMSF.AsString;
      fmAddInv.cxTextEdit2.Properties.ReadOnly:=True;
      fmAddInv.cxDateEdit1.Date:=quDocsInSelDATEDOC.AsDateTime;
      fmAddInv.cxDateEdit1.Properties.ReadOnly:=True;
      fmAddInv.cxDateEdit2.Date:=quDocsInSelDATESF.AsDateTime;
      fmAddInv.cxDateEdit2.Properties.ReadOnly:=True;
      fmAddInv.cxCurrencyEdit1.EditValue:=quDocsInSelSUMIN.AsCurrency;
      fmAddInv.cxCurrencyEdit2.EditValue:=quDocsInSelSUMUCH.AsCurrency;

      if taNDS.Active=False then taNDS.Active:=True;
      taNds.First;
      if not taNDS.Eof then fmAddInv.Label7.Caption:=taNDSNAMENDS.AsString; vNds[1]:=taNDSPROC.AsFloat; taNds.Next;
      if not taNDS.Eof then fmAddInv.Label9.Caption:=taNDSNAMENDS.AsString; vNds[2]:=taNDSPROC.AsFloat; taNds.Next;
      if not taNDS.Eof then fmAddInv.Label10.Caption:=taNDSNAMENDS.AsString; vNds[3]:=taNDSPROC.AsFloat;
      taNds.First;
      fmAddInv.Label11.Caption:=quDocsInSelSUMNDS0.AsString;
      fmAddInv.Label13.Caption:=quDocsInSelSUMNDS1.AsString;
      fmAddInv.Label14.Caption:=quDocsInSelSUMNDS2.AsString;

      fmAddInv.cxButtonEdit1.Tag:=quDocsInSelIDCLI.AsInteger;
      fmAddInv.cxButtonEdit1.EditValue:=quDocsInSelIDCLI.AsInteger;
      fmAddInv.cxButtonEdit1.Text:=quDocsInSelNAMECL.AsString;
      fmAddInv.cxButtonEdit1.Properties.ReadOnly:=True;

      if quMHAll.Active=False then quMHAll.Active:=True;

      fmAddInv.cxLookupComboBox1.EditValue:=quDocsInSelIDSKL.AsInteger;
      fmAddInv.cxLookupComboBox1.Text:=quDocsInSelNAMEMH.AsString;
      fmAddInv.cxLookupComboBox1.Properties.ReadOnly:=True;

      CurVal.IdMH:=quDocsInSelIDSKL.AsInteger;
      CurVal.NAMEMH:=quDocsInSelNAMEMH.AsString;

      if quMHAll.Locate('ID',CurVal.IdMH,[]) then
      begin
        fmAddInv.Label15.Caption:='��. ����: '+quMHAllNAMEPRICE.AsString;
        fmAddInv.Label15.Tag:=quMHAllDEFPRICE.AsInteger;
      end else
      begin
        fmAddInv.Label15.Caption:='��. ����: ';
        fmAddInv.Label15.Tag:=0;
      end;

      fmAddInv.cxLabel1.Enabled:=False;
      fmAddInv.cxLabel2.Enabled:=False;
      fmAddInv.cxLabel3.Enabled:=False;
      fmAddInv.cxLabel4.Enabled:=False;
      fmAddInv.cxButton1.Enabled:=False;

      fmAddInv.taSpec.Active:=False;
      fmAddInv.taSpec.CreateDataSet;

      IDH:=quDocsInSelID.AsInteger;

      quSpecInSel.Active:=False;
      quSpecInSel.ParamByName('IDHD').AsInteger:=IDH;
      quSpecInSel.Active:=True;

      quSpecInSel.First;
      while not quSpecInSel.Eof do
      begin
        with fmAddInv do
        begin
          taSpec.Append;
          taSpecNum.AsInteger:=quSpecInSelNUM.AsInteger;
          taSpecIdGoods.AsInteger:=quSpecInSelIDCARD.AsInteger;
          taSpecNameG.AsString:=quSpecInSelNAMEC.AsString;
          taSpecIM.AsInteger:=quSpecInSelIDM.AsInteger;
          taSpecSM.AsString:=quSpecInSelSM.AsString;
          taSpecQuant.AsFloat:=quSpecInSelQUANT.AsFloat;
          taSpecPrice1.AsCurrency:=quSpecInSelPRICEIN.AsCurrency;
          taSpecSum1.AsCurrency:=quSpecInSelSUMIN.AsCurrency;
          taSpecPrice2.AsCurrency:=quSpecInSelPRICEUCH.AsCurrency;
          taSpecSum2.AsCurrency:=quSpecInSelSUMUCH.AsCurrency;
          taSpecINds.AsInteger:=quSpecInSelIDNDS.AsInteger;
          taSpecSNds.AsString:=quSpecInSelNAMENDS.AsString;
          taSpecRNds.AsCurrency:=quSpecInSelSUMNDS.AsCurrency;
          taSpecSumNac.AsCurrency:=quSpecInSelSUMUCH.AsCurrency-quSpecInSelSUMIN.AsCurrency;
          taSpecProcNac.AsCurrency:=0;
          if quSpecInSelSUMIN.AsCurrency<>0 then
            taSpecProcNac.AsCurrency:=RoundEx((quSpecInSelSUMUCH.AsCurrency-quSpecInSelSUMIN.AsCurrency)/quSpecInSelSUMIN.AsCurrency*10000)/100;
          taSpec.Post;
        end;
        quSpecInSel.Next;
      end;

      fmAddInv.ShowModal;
    end else
    begin
      showmessage('�������� �������� ��� ���������.');
    end;}
  end;
end;

procedure TfmDocsInv.ViewDocsInvDblClick(Sender: TObject);
begin
  //������� �������
  with dmO do
  begin
    if quDocsInSelIACTIVE.AsInteger=0 then acEditDoc1.Execute //��������������
    else acViewDoc1.Execute; //��������
  end;
end;

procedure TfmDocsInv.acDelDoc1Execute(Sender: TObject);
//Var IDH:INteger;
begin
  //������� ��������
  if not CanDo('prDelDocInv') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmORep do
  begin
    if quDocsInvSel.RecordCount>0 then //���� ��� �������������
    begin
      if quDocsInvSelIACTIVE.AsInteger=0 then
      begin
        if MessageDlg('�� ������������� ������ ������� �������������� �'+quDocsInvSelNUMDOC.AsString+' �� '+FormatDateTime('dd.mm.yyyy',quDocsInvSelDATEDOC.asDateTime),mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
//          IDH:=quDocsInvSelID.AsInteger;
          quDocsInvSel.Delete;

{          quSpecInv.Active:=False;
          quSpecInv.ParamByName('IDH').AsInteger:=IDH;
          quSpecInv.Active:=True;

          quSpecInv.First; //������
          while not quSpecInv.Eof do quSpecInv.Delete;
          quSpecInv.Active:=False;

          quSpecInvC.Active:=False;
          quSpecInvC.ParamByName('IDH').AsInteger:=IDH;
          quSpecInvC.Active:=True;

          quSpecInvC.First; //������
          while not quSpecInvC.Eof do quSpecInvC.Delete;
          quSpecInvC.Active:=False;
}
        end;
      end else
      begin
        showmessage('������� �������������� �������� ������.');
      end;
    end else
    begin
      showmessage('�������� �������� ��� ��������.');
    end;
  end;
end;

procedure TfmDocsInv.acOnDoc1Execute(Sender: TObject);
//Var IdH:INteger;
//    i:Integer;
begin
//������������
  if not CanDo('prOnDocInv') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmO do
  begin
  end;
end;

procedure TfmDocsInv.acOffDoc1Execute(Sender: TObject);
//Var iCountPartOut:Integer;
//    bStart:Boolean;
begin
//��������
  if not CanDo('prOffDocIn') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmO do
  begin
  end;
end;

procedure TfmDocsInv.Timer1Timer(Sender: TObject);
begin
  if bClearDocIn=True then begin StatusBar1.Panels[0].Text:=''; bClearDocIn:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearDocIn:=True;
end;

procedure TfmDocsInv.acVidExecute(Sender: TObject);
begin
  //���
  with dmO do
  with dmORep do
  begin
    if LevelDocsInv.Visible then
    begin
    end;
  end;
end;

procedure TfmDocsInv.SpeedItem1Click0(Sender: TObject);
begin
  Close;
end;

end.
