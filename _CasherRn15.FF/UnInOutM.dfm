object fmInOutM: TfmInOutM
  Left = 580
  Top = 381
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'fmInOutM'
  ClientHeight = 450
  ClientWidth = 456
  Color = 14941884
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label9: TLabel
    Left = 8
    Top = 88
    Width = 40
    Height = 13
    Caption = #1057#1091#1084#1084#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Panel1: TPanel
    Left = 0
    Top = 392
    Width = 456
    Height = 58
    Align = alBottom
    BevelInner = bvLowered
    Color = 5936386
    TabOrder = 1
    object Button1: TcxButton
      Left = 48
      Top = 8
      Width = 145
      Height = 45
      Caption = #1054#1082
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ModalResult = 1
      ParentFont = False
      TabOrder = 0
      Colors.Normal = 16449510
      LookAndFeel.Kind = lfFlat
    end
    object Button2: TcxButton
      Left = 248
      Top = 8
      Width = 145
      Height = 45
      Caption = #1054#1090#1084#1077#1085#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = Button2Click
      Colors.Normal = 16449510
      Glyph.Data = {
        5E040000424D5E04000000000000360000002800000012000000130000000100
        18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
        8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
        0000CED3D6848684848684848684848684848684848684848684848684848684
        848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
        7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
        FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
        FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
        007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
        FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
        00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
        494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
        00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000}
      LookAndFeel.Kind = lfFlat
    end
  end
  object Panel2: TPanel
    Left = 4
    Top = 8
    Width = 213
    Height = 49
    BevelInner = bvLowered
    Color = 13237880
    TabOrder = 2
    object Label3: TLabel
      Left = 12
      Top = 12
      Width = 90
      Height = 13
      Caption = #1057#1091#1084#1084#1072' '#1074' '#1082#1072#1089#1089#1077
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label8: TLabel
      Left = 120
      Top = 10
      Width = 81
      Height = 16
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Label8'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
  end
  object CurrencyEdit1: TcxCurrencyEdit
    Left = 64
    Top = 80
    EditValue = 0c
    ParentFont = False
    Properties.Alignment.Horz = taRightJustify
    Properties.MaxValue = 90000000.000000000000000000
    Style.BorderStyle = ebsFlat
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -16
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = [fsBold]
    Style.IsFontAssigned = True
    TabOrder = 0
    BiDiMode = bdLeftToRight
    ParentBiDiMode = False
    Width = 157
  end
  object Panel3: TPanel
    Left = 228
    Top = 8
    Width = 217
    Height = 369
    BevelInner = bvLowered
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    object Label4: TLabel
      Left = 16
      Top = 8
      Width = 55
      Height = 20
      Alignment = taCenter
      Caption = #1057#1091#1084#1084#1072
      Color = clBlack
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Transparent = True
    end
    object cxButton2: TcxButton
      Left = 16
      Top = 160
      Width = 57
      Height = 49
      Caption = '1'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 0
      TabStop = False
      OnClick = cxButton2Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfUltraFlat
    end
    object cxButton3: TcxButton
      Left = 80
      Top = 160
      Width = 57
      Height = 49
      Caption = '2'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 1
      TabStop = False
      OnClick = cxButton3Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfUltraFlat
    end
    object cxButton4: TcxButton
      Left = 144
      Top = 160
      Width = 57
      Height = 49
      Caption = '3'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 2
      TabStop = False
      OnClick = cxButton4Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfUltraFlat
    end
    object cxButton5: TcxButton
      Left = 16
      Top = 104
      Width = 57
      Height = 49
      Caption = '4'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 3
      TabStop = False
      OnClick = cxButton5Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfUltraFlat
    end
    object cxButton6: TcxButton
      Left = 80
      Top = 104
      Width = 57
      Height = 49
      Caption = '5'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 4
      TabStop = False
      OnClick = cxButton6Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfUltraFlat
    end
    object cxButton7: TcxButton
      Left = 144
      Top = 104
      Width = 57
      Height = 49
      Caption = '6'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 5
      TabStop = False
      OnClick = cxButton7Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfUltraFlat
    end
    object cxButton8: TcxButton
      Left = 16
      Top = 48
      Width = 57
      Height = 49
      Caption = '7'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 6
      TabStop = False
      OnClick = cxButton8Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfUltraFlat
    end
    object cxButton9: TcxButton
      Left = 80
      Top = 48
      Width = 57
      Height = 49
      Caption = '8'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 7
      TabStop = False
      OnClick = cxButton9Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfUltraFlat
    end
    object cxButton10: TcxButton
      Left = 144
      Top = 48
      Width = 57
      Height = 49
      Caption = '9'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 8
      TabStop = False
      OnClick = cxButton10Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfUltraFlat
    end
    object cxButton11: TcxButton
      Left = 16
      Top = 216
      Width = 57
      Height = 48
      Caption = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 9
      TabStop = False
      OnClick = cxButton11Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfUltraFlat
    end
    object cxButton12: TcxButton
      Left = 80
      Top = 216
      Width = 57
      Height = 48
      Caption = ','
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 10
      TabStop = False
      OnClick = cxButton12Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfUltraFlat
    end
    object cxButton13: TcxButton
      Left = 16
      Top = 320
      Width = 89
      Height = 41
      Caption = 'C'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 11
      TabStop = False
      OnClick = cxButton13Click
      Colors.Default = 8454143
      Colors.Normal = 8454143
      Colors.Hot = 11796479
      Colors.Pressed = 59110
      LookAndFeel.Kind = lfUltraFlat
    end
    object cxButton14: TcxButton
      Left = 16
      Top = 272
      Width = 89
      Height = 41
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 12
      TabStop = False
      OnClick = cxButton14Click
      Colors.Default = 8454143
      Colors.Normal = 8454143
      Colors.Hot = 11796479
      Colors.Pressed = 59110
      Glyph.Data = {
        12040000424D12040000000000003600000028000000190000000D0000000100
        180000000000DC030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000
        0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFF00
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000000000000000000000000000000000000FFFFFFFFFF
        FF00FFFFFF000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000FFFFFFFFFFFF00FFFFFFFFFFFFFFFFFF000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000000000000000000000FFFFFFFFFFFF00FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFF000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00}
      LookAndFeel.Kind = lfUltraFlat
    end
    object cxButton15: TcxButton
      Left = 128
      Top = 272
      Width = 81
      Height = 89
      Caption = 'Ok'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 13
      TabStop = False
      OnClick = cxButton15Click
      Colors.Default = 16750591
      Colors.Normal = 16750591
      Colors.Hot = 16763647
      Colors.Pressed = 16718591
      LookAndFeel.Kind = lfUltraFlat
    end
    object CEdit4: TcxCalcEdit
      Left = 112
      Top = 8
      TabStop = False
      EditValue = 1.000000000000000000
      ParentFont = False
      Properties.Alignment.Horz = taRightJustify
      Properties.ReadOnly = True
      Properties.QuickClose = True
      Style.BorderStyle = ebsUltraFlat
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = 8552960
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsBold]
      Style.LookAndFeel.Kind = lfUltraFlat
      Style.Shadow = True
      Style.ButtonTransparency = ebtHideInactive
      Style.IsFontAssigned = True
      StyleDisabled.LookAndFeel.Kind = lfUltraFlat
      StyleFocused.LookAndFeel.Kind = lfUltraFlat
      StyleHot.LookAndFeel.Kind = lfUltraFlat
      TabOrder = 14
      Width = 89
    end
  end
  object acInOut: TActionManager
    Left = 40
    Top = 224
    StyleName = 'XP Style'
    object acClose: TAction
      Caption = 'acClose'
      ShortCut = 121
    end
  end
  object dxfBackGround1: TdxfBackGround
    BkColor.BeginColor = 14941884
    BkColor.EndColor = 5936386
    BkColor.FillStyle = fsVert
    BkAnimate.Speed = 700
    Left = 120
    Top = 224
  end
end
