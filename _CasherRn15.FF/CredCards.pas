unit CredCards;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ComCtrls, cxLookAndFeelPainters, StdCtrls, cxButtons,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit, DB,
  cxDBData, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  cxContainer, cxTextEdit, cxCurrencyEdit, ActnList, XPStyleActnCtrls,
  ActnMan, Menus, cxDataStorage;

type
  TfmCredCards = class(TForm)
    Panel1: TPanel;
    Button1: TcxButton;
    Button2: TcxButton;
    ViewBN: TcxGridDBTableView;
    LevelBN: TcxGridLevel;
    GridBN: TcxGrid;
    ViewBNNAME: TcxGridDBColumn;
    CurrencyEdit1: TcxCurrencyEdit;
    Label10: TLabel;
    Panel2: TPanel;
    Label1: TLabel;
    acCredCards: TActionManager;
    acReadCC: TAction;
    Timer1: TTimer;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Edit1: TEdit;
    Label6: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure ViewBNKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure acReadCCExecute(Sender: TObject);
    procedure Edit1KeyPress(Sender: TObject; var Key: Char);
    procedure Timer1Timer(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

Procedure  prSaveTr(RESULT,IDTR,POINTNUM:INteger;SRESULT,BNBAR,COMMENT,AUTHCODE,CARDTYPE:String;BNSUM:Real);

var
  fmCredCards: TfmCredCards;
  iCountBn:Integer;
  bnBar:ShortString;
  bWait:Boolean = False;

implementation

uses Dm, Un1, UnitBN, UnCash;

{$R *.dfm}

Procedure  prSaveTr(RESULT,IDTR,POINTNUM:INteger;SRESULT,BNBAR,COMMENT,AUTHCODE,CARDTYPE:String;BNSUM:Real);
begin
  with dmC do
  begin
//  ?CURTIME, ?RESULT, ?SRESULT, ?BNBAR, ?IDTR, ?POINTNUM, ?COMMENT

    prAddBNTR.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
    prAddBNTR.ParamByName('ZNUM').AsInteger:=Nums.ZNum;
    prAddBNTR.ParamByName('CHECKNUM').AsInteger:=Nums.iCheckNum+1;
    prAddBNTR.ParamByName('PERSON').AsInteger:=Person.Id;
    prAddBNTR.ParamByName('CURTIME').AsDateTime:=Now;
    prAddBNTR.ParamByName('RESULT').AsInteger:=RESULT;
    prAddBNTR.ParamByName('SRESULT').AsString:=SRESULT;
    prAddBNTR.ParamByName('BNBAR').AsString:=BNBAR;
    prAddBNTR.ParamByName('IDTR').AsInteger:=IDTR;
    prAddBNTR.ParamByName('POINTNUM').AsInteger:=POINTNUM;
    prAddBNTR.ParamByName('COMMENT').AsString:=COMMENT;
    prAddBNTR.ParamByName('BNSUM').AsFloat:=BNSUM;
    prAddBNTR.ParamByName('AUTHCODE').AsString:=AUTHCODE;
    prAddBNTR.ParamByName('CARDTYPE').AsString:=CARDTYPE;

    prAddBNTR.ExecProc;

  end;
end;

procedure TfmCredCards.FormCreate(Sender: TObject);
begin
  GridBN.Align:=alTop;
  with dmC do
  begin
    taCredCard.Active:=False;
    taCredCard.Active:=True;
  end;
end;

procedure TfmCredCards.Button2Click(Sender: TObject);
begin
  close;
end;

procedure TfmCredCards.ViewBNKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=37 then
  begin
    Button1.Default:=True;
    Button2.Default:=False;
    GridBn.SetFocus;
  end;
  if key=39 then
  begin
    Button1.Default:=False;
    Button2.Default:=True;
    GridBn.SetFocus;
  end;
end;

procedure TfmCredCards.acReadCCExecute(Sender: TObject);
begin
  Edit1.Text:='';
// Edit1.Text:='4256090230767240=08041212374500000832'; //�� ������ ��������
  Edit1.SetFocus;
end;

procedure TfmCredCards.Edit1KeyPress(Sender: TObject; var Key: Char);
Var bCh:Byte;
    StrWk,StrWk1:String;
    iPos:INteger;
    bnDate,curDate:Integer;
    iErr:Integer;
    sErr:String;
begin
  if bWait then exit;
  bCh:=ord(Key);
  if bCh = 13 then
  begin //���� ����������
    bWait:=True;
    bnBar:=Edit1.Text;
    //������ ������ �������� � ��������� �� ������ ������
    prWriteLog(bnBar);
    while pos(';',bnBar)>0 do delete(bnBar,pos(';',bnBar),1);
    while pos('�',bnBar)>0 do delete(bnBar,pos('�',bnBar),1);
    while pos('?',bnBar)>0 do delete(bnBar,pos('?',bnBar),1);
    while pos(',',bnBar)>0 do delete(bnBar,pos(',',bnBar),1);

    if Length(bnBar)>37 then bnBar:=Copy(bnBar,Length(bnBar)-37+1,37);
    //���� �������� ��� �������
    prWriteLog(bnBar);

    Edit1.Text:='';
    Button2.Enabled:=False;
//    if Length(bnBar)<35 then close; //������������� ����
    iPos:=Pos('=',bnBar)+1;
    StrWk:=Copy(bnBar,iPos,4);
    bnDate:=StrToIntDef(StrWk,0);
    if bnDate=0 then begin Label2.Visible:=True; Label2.Caption:='���������� ����� ������������.('+IntToStr(bnDate)+')'; delay(5000); exit; end;
    StrWk:=FormatDateTime('yymm',date);
    curDate:=StrToIntDef(StrWk,0);
    if bnDate<curDate then begin Label2.Visible:=True; Label2.Caption:='���� �������� ����� �����.('+IntToStr(bnDate)+')'; delay(5000); exit; end;
    //���� ����� �� ���� ������ ���� ����������
    Label2.Caption:='����� - '+Copy(bnBar,1,iPos-2);

    StrWk:=INtToStr(RoundEx(Frac(bnDate/100)*100));
    if Length(StrWk)<2 then StrWk:='0'+StrWk;
    StrWk1:=INtToStr(RoundEx(bnDate/100));
    if Length(StrWk1)<2 then StrWk1:='0'+StrWk1;

    Label3.Caption:='���� - '+StrWk+'/'+StrWk1;
    Label6.Caption:='';

    Label2.Visible:=True;
    Label3.Visible:=True;
    Label4.Visible:=True;
    Label5.Visible:=True;
    Label6.Visible:=True;

   //��� �������� ����

    prClearAnsw; //������� ����� �� ������ ������

    prStartBnTr(CommonSet.CashNum,StrToIntDef(CommonSet.BNPointNum,0),0,fmCredCards.CurrencyEdit1.EditValue,CommonSet.BNPath,bnBar,iErr,sErr);
//    prStartBnTr(CashNum,PointNum,IdTr:Integer;bnSum:Real;bnPath,bnBar:String; Var iErr:Integer; Var sErr:String);

    iCountBn:=0;
    Timer1.Enabled:=True;
  end;
end;

procedure TfmCredCards.Timer1Timer(Sender: TObject);
Var StrWk:String;
begin
  if iCountBn>CommonSet.BNDelay-1 then
  begin
    prDelBnTr(CommonSet.BNPath,CommonSet.CashNum);
    iCountBn:=0;
    Timer1.Enabled:=False;
    Close;
  end else
  begin
    //��������� ������� ������
    prTestAnswerBN(CommonSet.CashNum,CommonSet.BNPath,Answ.iErr,Answ.PointNum,Answ.IdTr,Answ.sErr,Answ.bnBar,Answ.sDate,Answ.sTime,Answ.sId,Answ.sAuthCode,Answ.sCardType,Answ.sNumTerm,Answ.sErrCode,Answ.sErrCode26);
    //��������
    if Answ.iErr>0 then
    begin //���� ��������� � �����������
      Timer1.Enabled:=False;
      iCountBn:=0;
      prDelBnTrO(CommonSet.BNPath,CommonSet.CashNum);

      // ���� ��������� ��� ���������� � ����� ������
      StrWk:=Answ.sDate+'|'+Answ.sTime+'|'+Answ.sId+'|'+Answ.sNumTerm+'|'+Answ.sErrCode+'|'+Answ.sErrCode26;
      //�������� ����������
      prSaveTr(Answ.iErr,Answ.IdTr,Answ.PointNum,Answ.sErr,Answ.bnBar,StrWk,Answ.sAuthCode,Answ.sCardType,CurrencyEdit1.Value);
//      prSaveTr(RESULT,IDTR,POINTNUM:INteger;SRESULT,BNBAR,COMMENT,AUTHCODE,CARDTYPE:String;BNSUM:Real);

      StrWk:=Copy(bnBar,1,Pos('=',bnBar)-1);
      while pos(' ',Answ.bnBar)>0 do delete(Answ.bnBar,pos(' ',Answ.bnBar),1);

      if (Answ.iErr=1) then
      begin
        //����� ����
        if Answ.bnBar=StrWk then
        begin
          Label6.Caption:='�����';
          delay(2000);
          fmCredCards.ModalResult:=mrOk;
        end else //������ � ������ ������ ����� ��������
        begin
          Label6.Caption:='����� - ����� �� ������������� �������.';
          delay(5000);
          Close;
        end;
      end;
      if Answ.iErr=2 then
      begin
        //�����
        Label6.Caption:='����� - '+Answ.sErr+'('+Answ.sErrCode+';'+Answ.sErrCode26+')';
        delay(5000);
        Close;
      end;
    end;

    inc(iCountBn);
    Label5.Caption:=IntToStr(iCountBn);
  end;
end;

procedure TfmCredCards.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  bWait:=False; //�� ��� ������ ���� ������� ������� �����
end;

procedure TfmCredCards.FormShow(Sender: TObject);
begin
//  Edit1.Text:='';
//  Edit1.Text:='4256090230767240=08041212374500000832'; //�� ������ ��������
//  Edit1.SetFocus;
end;

end.
