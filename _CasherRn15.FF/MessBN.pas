unit MessBN;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, dxfBackGround, ActnList, XPStyleActnCtrls, ActnMan,
  jpeg, ExtCtrls;

type
  TfmMessBn = class(TForm)
    dxfBackGround1: TdxfBackGround;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    ActionManager1: TActionManager;
    acExit: TAction;
    procedure acExitExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmMessBn: TfmMessBn;

implementation

uses Un1;

{$R *.dfm}

procedure TfmMessBn.acExitExecute(Sender: TObject);
begin
  ModalResult:=mrOk;
end;

procedure TfmMessBn.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if Key=its(iAAnsw) then ModalResult:=mrOk;
end;

end.
