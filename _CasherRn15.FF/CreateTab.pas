unit CreateTab;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxLookAndFeelPainters, StdCtrls, cxButtons, ExtCtrls,
  cxMaskEdit, cxDropDownEdit, cxCalc, cxControls, cxContainer, cxEdit,
  cxTextEdit, Placemnt, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox,
  ActnList, XPStyleActnCtrls, ActnMan, cxGraphics, Menus;

type
  TfmCreateTab = class(TForm)
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    TextEdit1: TcxTextEdit;
    FormPlacement1: TFormPlacement;
    Label4: TLabel;
    ComboBox1: TcxLookupComboBox;
    CalcEdit1: TcxCalcEdit;
    am2: TActionManager;
    acExit: TAction;
    procedure FormCreate(Sender: TObject);
    procedure TextEdit1Enter(Sender: TObject);
    procedure CalcEdit1Enter(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmCreateTab: TfmCreateTab;

implementation

uses Un1, Dm, Calc;

{$R *.dfm}

procedure TfmCreateTab.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
end;

procedure TfmCreateTab.TextEdit1Enter(Sender: TObject);
Var bTab:Boolean;
    iCount:Integer;
begin
  fmCalc.CalcEdit1.Text:=TextEdit1.Text;
  bTab:=False;
  while not bTab do
  begin
    fmCalc.ShowModal;
    if fmCalc.ModalResult=mrOk then
    begin
      with dmC do
      begin
        quTabExists.Active:=False;
        quTabExists.ParamByName('Id_Personal').AsInteger:=ComboBox1.EditValue;
        quTabExists.ParamByName('NumTab').AsString:=fmCalc.CalcEdit1.Text;
        quTabExists.Active:=True;
        iCount:=quTabExistsCOUNT.AsInteger;
        if iCount>0 then showmessage('������ ����� ��� ���������. �������� ������.')
        else bTab:=true;
      end;
    end else
    begin
      bTab:=True;
      fmCalc.CalcEdit1.Text:=TextEdit1.Text;
    end;
  end;
  TextEdit1.Text:=fmCalc.CalcEdit1.Text;
//  CalcEdit1.SetFocus;
end;

procedure TfmCreateTab.CalcEdit1Enter(Sender: TObject);
begin
  fmCalc.CalcEdit1.EditValue:=CalcEdit1.EditValue;
  fmCalc.ShowModal;
  if fmCalc.ModalResult=mrOk then
  begin
    CalcEdit1.EditValue:=fmCalc.CalcEdit1.EditValue;
  end;
  cxButton1.SetFocus;
end;

procedure TfmCreateTab.cxButton1Click(Sender: TObject);
begin
  ModalResult:=mrOk;
end;

procedure TfmCreateTab.acExitExecute(Sender: TObject);
begin
  Modalresult:=mrCancel;
end;

end.
