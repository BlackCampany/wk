unit MainRZD;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGrid,
  ExtCtrls, Menus, cxLookAndFeelPainters, cxButtons, cxContainer,
  cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalc, RXClock, DB, cxDBData,
  cxGridCardView, cxGridDBCardView, ActnList, XPStyleActnCtrls, ActnMan,
  cxCurrencyEdit, cxImageComboBox, cxGridDBTableView,
  cxGridBandedTableView, cxGridDBBandedTableView, dxmdaset, cxMemo;

type
  TfmMainRZD = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Label9: TLabel;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    cxButton5: TcxButton;
    cxButton6: TcxButton;
    cxButton7: TcxButton;
    cxButton8: TcxButton;
    cxButton9: TcxButton;
    cxButton10: TcxButton;
    cxButton11: TcxButton;
    cxButton12: TcxButton;
    cxButton13: TcxButton;
    cxButton14: TcxButton;
    cxButton15: TcxButton;
    CEdit4: TcxCalcEdit;
    GridHK: TcxGrid;
    ViewHK: TcxGridTableView;
    LevelHK: TcxGridLevel;
    Panel3: TPanel;
    Button1: TcxButton;
    Button2: TcxButton;
    GridM: TcxGrid;
    ViewM: TcxGridDBCardView;
    ViewMINFO: TcxGridDBCardViewRow;
    ViewMTREETYPE: TcxGridDBCardViewRow;
    ViewMSPRICE: TcxGridDBCardViewRow;
    LevelM: TcxGridLevel;
    cxButton1: TcxButton;
    RxClock2: TRxClock;
    Label1: TLabel;
    acRZD: TActionManager;
    acOpenSelMenu: TAction;
    teSpec: TdxMemData;
    teSpecSIFR: TIntegerField;
    teSpecPRICE: TFloatField;
    teSpecQUANTITY: TFloatField;
    teSpecSUMMA: TFloatField;
    teSpecNAME: TStringField;
    teSpecISTATUS: TSmallintField;
    dsteSpec: TDataSource;
    teSpecID: TIntegerField;
    Panel6: TPanel;
    Panel7: TPanel;
    cxButton16: TcxButton;
    cxButton17: TcxButton;
    cxButton18: TcxButton;
    cxButton19: TcxButton;
    Panel4: TPanel;
    Panel5: TPanel;
    GridSpec: TcxGrid;
    ViewSpec: TcxGridDBBandedTableView;
    ViewSpecSIFR: TcxGridDBBandedColumn;
    ViewSpecNAME: TcxGridDBBandedColumn;
    ViewSpecPRICE: TcxGridDBBandedColumn;
    ViewSpecQUANTITY: TcxGridDBBandedColumn;
    ViewSpecSUMMA: TcxGridDBBandedColumn;
    ViewSpecDPROC: TcxGridDBBandedColumn;
    ViewSpecDSUM: TcxGridDBBandedColumn;
    ViewSpecISTATUS: TcxGridDBBandedColumn;
    ViewModif: TcxGridDBTableView;
    ViewModifID_TAB: TcxGridDBColumn;
    ViewModifID_POS: TcxGridDBColumn;
    ViewModifID: TcxGridDBColumn;
    ViewModifSIFR: TcxGridDBColumn;
    ViewModifNAME: TcxGridDBColumn;
    ViewModifQUANTITY: TcxGridDBColumn;
    LevelSpec: TcxGridLevel;
    LevelModif: TcxGridLevel;
    cxTextEdit1: TcxTextEdit;
    teMod: TdxMemData;
    dsteMod: TDataSource;
    teModID_POS: TIntegerField;
    teModID: TSmallintField;
    teModSIFR: TIntegerField;
    teModNAME: TStringField;
    teModQUANTITY: TFloatField;
    teSpecLIMITM: TIntegerField;
    teSpecLINKM: TIntegerField;
    teSpecITYPE: TSmallintField;
    acModify: TAction;
    tiP: TTimer;
    LevelP: TcxGridLevel;
    GridP: TcxGrid;
    ViewP: TcxGridDBCardView;
    ViewPID_TAB: TcxGridDBCardViewRow;
    ViewPSUMP: TcxGridDBCardViewRow;
    Panel8: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    TimerVes: TTimer;
    Memo1: TcxMemo;
    procedure FormCanResize(Sender: TObject; var NewWidth,
      NewHeight: Integer; var Resize: Boolean);
    procedure FormShow(Sender: TObject);
    procedure cxButton8Click(Sender: TObject);
    procedure cxButton9Click(Sender: TObject);
    procedure cxButton10Click(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
    procedure cxButton7Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure cxButton11Click(Sender: TObject);
    procedure cxButton12Click(Sender: TObject);
    procedure cxButton14Click(Sender: TObject);
    procedure cxButton13Click(Sender: TObject);
    procedure cxButton15Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxButton16Click(Sender: TObject);
    procedure RxClock1Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure ViewMCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure acOpenSelMenuExecute(Sender: TObject);
    procedure ViewMCellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure acModifyExecute(Sender: TObject);
    procedure ViewHKCellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxButton19Click(Sender: TObject);
    procedure cxButton17Click(Sender: TObject);
    procedure cxButton18Click(Sender: TObject);
    procedure tiPTimer(Sender: TObject);
    procedure ViewPCellDblClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure TimerVesTimer(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
    Procedure CreateHotList;
  end;

procedure prClickTabNum(iNum:Integer);

var
  fmMainRZD: TfmMainRZD;
  bFirst:Boolean;
  Sifr:INteger;
  sScanEv:String;
  iReadCount:Integer = 0;

  MaxCol:Integer;
  MaxLine:Integer;
  ColWidth:Integer;

implementation

uses Un1, Unit1, DmRZD, Calc, ModifFF1, Calc1;

{$R *.dfm}

Procedure TfmMainRZD.CreateHotList;
Var AColumn:TcxGridColumn;
    i,j:INteger;
    Str1:String;
begin
  try
    try
      for I := 0 to MaxCol-1 do
      begin
        AColumn := ViewHK.CreateColumn;
        AColumn.Caption := IntToStr(i);
        AColumn.Name := 'k'+IntToStr(i);
        AColumn.DataBinding.ValueType := 'String';
        AColumn.Styles.Content:=dmR.cxStyle18;
        AColumn.Styles.Content.Color:=$00EAEAD5;
        AColumn.Width:=ColWidth; //���� 100
        AColumn.Styles.Content.Font.Size:=CommonSet.GridHKFontSize;
        if CommonSet.GridHKFontStyle=1 then AColumn.Styles.Content.Font.Style := [fsBold] else  AColumn.Styles.Content.Font.Style := [];
      end;
    except
    end;

    ViewHK.DataController.RecordCount := MaxLine;

    for I := 0 to MaxLine-1 do    //������
      for J := 0 to MaxCol-1 do  //�������
        ViewHK.DataController.SetValue(I, J, '');
    with dmR do
    begin
      if CommonSet.UseDayMenu=0 then
      begin
        try
          quMenuHK.Active:=False;
          quMenuHK.SelectSQL.Clear;
          quMenuHK.SelectSQL.Add('SELECT SIFR,NAME,PRICE,CODE,TREETYPE,LIMITPRICE,CATEG,');
          quMenuHK.SelectSQL.Add('PARENT,LINK,STREAM,LACK,DESIGNSIFR,ALTNAME,NALOG,BARCODE,');
          quMenuHK.SelectSQL.Add('IMAGE,CONSUMMA,MINREST,PRNREST,COOKTIME,DISPENSER,');
          quMenuHK.SelectSQL.Add('DISPKOEF,ACCESS,FLAGS,TARA,CNTPRICE,BACKBGR,');
          quMenuHK.SelectSQL.Add('    FONTBGR,IACTIVE,IEDIT');
          quMenuHK.SelectSQL.Add('FROM MENU');
          quMenuHK.SelectSQL.Add('WHERE IACTIVE=1 and SIFR=:ISIFR');

          quHotKey.Active:=False;
          quHotKey.ParamByName('CASHNUM').AsInteger:=CommonSet.Station;
          quHotKey.Active:=True;
          quHotKey.First;
          while not quHotKey.Eof do
          begin
            try
              Str(quHotKeyPRICE.AsFloat:6:2,StrWk);
              Str1:=quHotKeyNAME.AsString; while pos(#13,Str1)>0 do delete(Str1,pos(#13,Str1),1);

              StrWk:=Copy(Str1,1,CommonSet.GridHKLen)+#13+Copy(Str1,1+CommonSet.GridHKLen,CommonSet.GridHKLen)+#13+'����:'+StrWk+'�.'+#13+IntToStr(quHotKeySIFR.AsInteger);

              if (quHotKeyIROW.AsInteger<MaxLine) and (quHotKeyICOL.AsInteger<MaxCol) then
                ViewHK.DataController.SetValue(quHotKeyIROW.AsInteger,quHotKeyICOL.AsInteger,StrWk);
            except
            end;
            quHotKey.Next;
          end;


        finally
          quHotKey.Active:=False;
        end;
      end;
      if CommonSet.UseDayMenu=1 then
      begin
        try
          quMenuHK.Active:=False;
          quMenuHK.SelectSQL.Clear;

          quMenuHK.SelectSQL.Add('SELECT me.SIFR,me.NAME,me.CODE,md.PRICE,me.TREETYPE,me.LIMITPRICE,me.CATEG,');
          quMenuHK.SelectSQL.Add('    me.PARENT,me.LINK,me.STREAM,me.LACK,me.DESIGNSIFR,me.ALTNAME,me.NALOG,me.BARCODE,');
          quMenuHK.SelectSQL.Add('    me.IMAGE,me.CONSUMMA,me.MINREST,me.PRNREST,me.COOKTIME,me.DISPENSER,');
          quMenuHK.SelectSQL.Add('    me.DISPKOEF,me.ACCESS,me.FLAGS,me.TARA,me.CNTPRICE,me.BACKBGR,');
          quMenuHK.SelectSQL.Add('    me.FONTBGR,me.IACTIVE,me.IEDIT');
          quMenuHK.SelectSQL.Add('FROM MENU me');
          quMenuHK.SelectSQL.Add('left join MENUD md on md.SIFR=me.SIFR');
          quMenuHK.SelectSQL.Add('WHERE me.IACTIVE=1 and me.SIFR=:ISIFR');

          quHotKey1.Active:=False;
          quHotKey1.ParamByName('CASHNUM').AsInteger:=CommonSet.Station;
          quHotKey1.Active:=True;
          quHotKey1.First;
          while not quHotKey1.Eof do
          begin
            try
              Str(quHotKey1PRICE.AsFloat:6:2,StrWk);
              Str1:=quHotKey1NAME.AsString; while pos(#13,Str1)>0 do delete(Str1,pos(#13,Str1),1);

              StrWk:=Copy(Str1,1,CommonSet.GridHKLen)+#13+Copy(Str1,1+CommonSet.GridHKLen,CommonSet.GridHKLen)+#13+'����:'+StrWk+'�.'+#13+IntToStr(quHotKey1SIFR.AsInteger);

              if (quHotKey1IROW.AsInteger<MaxLine) and (quHotKey1ICOL.AsInteger<MaxCol) then
                ViewHK.DataController.SetValue(quHotKey1IROW.AsInteger,quHotKey1ICOL.AsInteger,StrWk);
            except
            end;
            quHotKey1.Next;
          end;
        finally
          quHotKey1.Active:=False;
        end;
      end;
    end;
  except
  end;
end;


procedure prClickTabNum(iNum:Integer);
begin
  with fmMainRZD do
  begin
    if bFirst then CEdit4.Value:=iNum
    else
      if CEdit4.Value<10 then
      begin
        if pos(',',CEdit4.Text)=0 then CEdit4.EditValue:=CEdit4.EditValue*10+iNum
        else CEdit4.Text:=CEdit4.Text+its(iNum);
      end;
    bFirst:=False;
  end;
end;

procedure TfmMainRZD.FormCanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
  if fmMainRZD.Tag=17 then NewHeight:=1024 else NewHeight:=768;
  Left:=0;
  Top:=0;
end;

procedure TfmMainRZD.FormShow(Sender: TObject);
begin
  Memo1.Clear;
  CEdit4.Value:=0;
  bFirst:=True; //��� ����������� ����� ������ ����� ������ �����

  Person.Id:=CommonSet.Station*(-1);
  Person.Name:='������� (������� '+its(Person.Id)+')';
  Label1.Caption:=Person.Name;

  with dmR do
  begin
    CasherRnDb.Connected:=False;
    CasherRnDb.DBName:=DBName;
    try
      CasherRnDb.Open;
      if CasherRnDb.Connected then
      begin
        ViewM.BeginUpdate;
        dsMenu.DataSet:=Nil;
        prFormMenu(0);
        dsMenu.DataSet:=quMenu;
        ViewM.EndUpdate;

        CreateHotList;

        bScale:=False;
        Label3.Caption:='';
        if pos('COM',CommonSet.PortVesCAS_ER)=1 then
        begin //��������� �������� � ����� ����
          try
            devCas.DeviceName:=CommonSet.PortVesCAS_ER;
            devCas.Open;
            bScale:=True;
          except
            bScale:=False;
          end;
        end;
        if bScale then
        begin
          Label2.Caption:='���� �� '+CommonSet.PortVesCAS_ER;
          TimerVes.Enabled:=True;
          Memo1.Lines.Add('TimerVes �������.');
        end else Label2.Caption:='����� ���';
        
      end else showmessage('������ �������� ���� ������. ���������� ������ ����������.');
    except
    end;
  end;
end;

procedure TfmMainRZD.cxButton8Click(Sender: TObject);
begin
  prClickTabNum(7);
end;

procedure TfmMainRZD.cxButton9Click(Sender: TObject);
begin
  prClickTabNum(8);
end;

procedure TfmMainRZD.cxButton10Click(Sender: TObject);
begin
  prClickTabNum(9);
end;

procedure TfmMainRZD.cxButton5Click(Sender: TObject);
begin
  prClickTabNum(4);
end;

procedure TfmMainRZD.cxButton6Click(Sender: TObject);
begin
  prClickTabNum(5);
end;

procedure TfmMainRZD.cxButton7Click(Sender: TObject);
begin
  prClickTabNum(6);
end;

procedure TfmMainRZD.cxButton2Click(Sender: TObject);
begin
  prClickTabNum(1);
end;

procedure TfmMainRZD.cxButton3Click(Sender: TObject);
begin
  prClickTabNum(2);
end;

procedure TfmMainRZD.cxButton4Click(Sender: TObject);
begin
  prClickTabNum(3);
end;

procedure TfmMainRZD.cxButton11Click(Sender: TObject);
begin
  prClickTabNum(0);
end;

procedure TfmMainRZD.cxButton12Click(Sender: TObject);
begin
  if pos(',',CEdit4.Text)=0 then CEdit4.Text:=CEdit4.Text+',';
  bFirst:=False;
end;

procedure TfmMainRZD.cxButton14Click(Sender: TObject);
begin
  cEdit4.EditValue:=Trunc(cEdit4.EditValue*100/10/100);
end;

procedure TfmMainRZD.cxButton13Click(Sender: TObject);
begin
  CEdit4.Value:=0;
  bFirst:=True;
end;

procedure TfmMainRZD.cxButton15Click(Sender: TObject);
Var iNumP:INteger;
    iMaxM,iMaxB:INteger;
begin
  if (cEdit4.EditValue <> 0)and(cEdit4.EditValue < 100000)  then
  begin
    iNumP:=cEdit4.EditValue;
    if GridSpec.tag<>iNumP then
    begin
      if GridSpec.tag=0 then
      begin
        with dmR do
        begin
          GridSpec.tag:=iNumP;
          try
            ViewSpec.BeginUpdate;
            ViewModif.BeginUpdate;

            CloseTe(teMod);
            CloseTe(teSpec);
            Check.Max:=1;
            quSpecP.Active:=False;
            quSpecP.ParamByName('IDH').AsInteger:=iNumP;
            quSpecP.Active:=True;
            quSpecP.First;
            while not quSpecP.Eof do
            begin
              if quSpecPITYPE.AsInteger=0 then   //��� �����
              begin
                iMaxB:=teSpec.RecordCount;

                teSpec.Append;
                teSpecID.AsInteger:=iMaxB+1;
                teSpecSIFR.AsInteger:=quSpecPSIFR.AsInteger;
                teSpecPRICE.AsFloat:=quSpecPPRICE.AsFloat;
                teSpecQUANTITY.AsFloat:=quSpecPQUANTITY.AsFloat;
                teSpecSUMMA.AsFloat:=quSpecPSUMMA.AsFloat;
                teSpecNAME.AsString:=quSpecPNAME.AsString;
                teSpecISTATUS.AsInteger:=1;
                teSpecLIMITM.AsInteger:=quSpecPLIMITM.AsInteger;
                teSpecLINKM.AsInteger:=quSpecPLINKM.AsInteger;
                teSpecITYPE.AsInteger:=0;
                teSpec.Post;
              end else //��� �����������
              begin
                iMaxM:=teMod.RecordCount;

                teMod.Append;
                teModID_POS.AsInteger:=teSpecID.AsInteger;
                teModID.AsInteger:=iMaxM+1;
                teModSIFR.AsInteger:=quSpecPSIFR.AsInteger;
                teModNAME.AsString:=quSpecPNAME.AsString;
                teModQUANTITY.AsFloat:=quSpecPQUANTITY.AsFloat;
                teMod.Post;
              end;

              quSpecP.Next;
            end;
            quSpecP.Active:=False;
          finally
            ViewSpec.EndUpdate;
            ViewModif.EndUpdate;
          end;

          //�������� ��������������

          cxTextEdit1.Text:='������ � '+its(iNumP);
          Panel4.Visible:=True;
          Panel7.Visible:=True;
        end;
      end else Showmessage('������� ��������� ������ � �������� ������..');
    end;
  end;
  cEdit4.EditValue:=0;
  bFirst:=True;
end;

procedure TfmMainRZD.FormCreate(Sender: TObject);
begin
  CurDir := ExtractFilePath(ParamStr(0));
  ReadIni;

  StrPre:='';
  Person.Id:=0;
  Person.Name:='';

  MaxCol:=CommonSet.GridHKColQuant; //15-������
  MaxLine:=CommonSet.GridHKRowQuant;  //15-������
  ColWidth:=CommonSet.GridHKColW;
  ViewHK.OptionsView.DataRowHeight:=CommonSet.GridHKRowH;

end;

procedure TfmMainRZD.cxButton16Click(Sender: TObject);
begin
  GridSpec.Tag:=0;
  cxTextEdit1.Text:=' ';
  Panel4.Visible:=False;
  Panel7.Visible:=False;
  CloseTe(teMod);
  CloseTe(teSpec);
end;

procedure TfmMainRZD.RxClock1Click(Sender: TObject);
begin
  if CanDo('prExit') then
    if MessageDlg('�� ������������� ������ �������� ���������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      WriteHistory('CloseClock');
      delay(100);
      Close;
    end;
end;

procedure TfmMainRZD.cxButton1Click(Sender: TObject);
Var i:INteger;
begin
  if GridHK.Tag=0 then
  begin
    showmessage('�������� ������� ����� ��������� ������ ������.');
    GridHK.Tag:=1;
//    for i:=0 to ViewHK.ColumnCount-1 do ViewHK.Columns[i].Styles.Content:=dmC.cxStyle28;
    for i:=0 to ViewHK.ColumnCount-1 do ViewHK.Columns[i].Styles.Content.Color:=$00CCCCFF;
  end else
  begin
    showmessage('����� ��������� ������ ������ ��������.');
    GridHK.Tag:=0;
    for i:=0 to ViewHK.ColumnCount-1 do ViewHK.Columns[i].Styles.Content.Color:=$00EAEAD5;
  end;
end;

procedure TfmMainRZD.ViewMCustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);
var
  ARec: TRect;
  ATextToDraw: string;
  sType:String;
begin
  if (AViewInfo is TcxGridCardRowDataViewInfo) then
    ATextToDraw := AViewInfo.GridRecord.DisplayTexts[AViewInfo.Item.Index]
  else
    ATextToDraw := VarAsType(AViewInfo.Item.Caption, varString);

  ARec := AViewInfo.Bounds;

  sType := VarAsType(AViewInfo.GridRecord.DisplayTexts[1], varString);
  if sType='T'  then
  begin
    ACanvas.Canvas.Brush.Color:=$00CDAD98;
  end
  else
  begin
//    ACanvas.Canvas.Brush.Bitmap := ABitmap;
  end;
  ACanvas.Canvas.FillRect(ARec);
  SetBkMode(ACanvas.Canvas.Handle, TRANSPARENT);
  ACanvas.DrawText(ATextToDraw, AViewInfo.Bounds, 0, True);
  ADone := True; // }
end;

procedure TfmMainRZD.acOpenSelMenuExecute(Sender: TObject);
Var iParent:Integer;
    rQ:Real;
    iC,iR:INteger;
    Str1:String;
    iMax:INteger;
begin
  with dmR do
  begin
    if quMenuTREETYPE.AsString='T' then
    begin
      iParent:=quMenuSIFR.AsInteger;
      ViewM.BeginUpdate;
      dsMenu.DataSet:=Nil;

      prFormMenu(iParent);

      dsMenu.DataSet:=quMenu;
      ViewM.EndUpdate;
    end
    else //��������� ������� � �����
    begin

      if GridHK.Tag>0 then //��������� ������� � ������� �������
      begin
        iC:=ViewHK.Controller.FocusedColumnIndex;
        iR:=ViewHK.Controller.FocusedRowIndex;
        if (iC>=0) and (iR>=0) then
        begin
          if Sifr>0 then
          begin
            quMenu.Locate('SIFR',sifr,[]);

            try
              quHotKey.Active:=False;
              quHotKey.ParamByName('CASHNUM').AsInteger:=CommonSet.Station;
              quHotKey.Active:=True;

              quHotKey.First;
              while not quHotKey.Eof do
              begin
                if (quHotKeyCASHNUM.AsInteger=CommonSet.Station)and
                   (quHotKeyIROW.AsInteger=iR)and
                   (quHotKeyICOL.AsInteger=iC) then
                begin
                  quHotKey.Delete; break;
                end else quHotKey.Next;
              end;
              quHotKey.Append;
              quHotKeyCASHNUM.AsInteger:=CommonSet.Station;
              quHotKeyIROW.AsInteger:=iR;
              quHotKeyICOL.AsInteger:=iC;
              quHotKeySIFR.AsInteger:=quMenuSIFR.AsInteger;
              quHotKey.Post;

              Str(quMenuPRICE.AsFloat:6:2,StrWk);
              Str1:=quMenuNAME.AsString; while pos(#13,Str1)>0 do Str1[pos(#13,Str1)]:=' ';

              if MaxCol>=10 then //17
                StrWk:=Copy(Str1,1,14)+#13+Copy(Str1,15,14)+#13+'����:'+StrWk+'�.'+#13+IntToStr(quHotKeySIFR.AsInteger)
              else //15 ���� ��������������������
                StrWk:=Copy(Str1,1,14)+#13+Copy(Str1,15,14)+#13+'����:'+StrWk+'�.'+#13+IntToStr(quHotKeySIFR.AsInteger);

              quHotKey.Active:=False;

              ViewHK.DataController.SetValue(iR,iC,StrWk);
            except
            end;
          end;
        end;
      end else //� �����
      begin
        if GridSpec.Tag>0 then
        begin
          if Sifr>0 then quMenu.Locate('SIFR',sifr,[]);
          if quMenuSIFR.AsInteger<>Sifr then exit;
          if (teSpecSifr.AsInteger<>quMenuSIFR.AsInteger)or(quMenuDESIGNSIFR.AsInteger>0) then //����� ������� ��� ����������� ���-��
          begin
            rQ:=1;

            if quMenuDISPKOEF.AsInteger=1 then //������� �����
            begin
              if Label3.Tag>25 then //������ 25-� ����
              begin
                rQ:=Label3.Tag/1000;
              end;
            end;

            if quMenuDESIGNSIFR.AsInteger>0 then
            begin
              fmCalc1.CalcEdit1.EditValue:=rQ;
              fmCalc1.ShowModal;
              if (fmCalc1.ModalResult=mrOk)and(fmCalc1.CalcEdit1.EditValue>0.00001)  then
              begin
                rQ:=RoundEx(fmCalc1.CalcEdit1.EditValue*1000)/1000;
              end;
            end;

            iMax:=1;
            ViewSpec.BeginUpdate;
            teSpec.First;
            while not teSpec.Eof do
            begin
              if teSpecID.AsInteger>=iMax then iMax:=teSpecID.AsInteger+1;
              teSpec.Next;
            end;

            teSpec.Append;
            teSpecID.AsInteger:=iMax;
            teSpecSIFR.AsInteger:=quMenuSIFR.AsInteger;
            teSpecPRICE.AsFloat:=quMenuPRICE.AsFloat;
            teSpecQUANTITY.AsFloat:=rQ;
            teSpecSUMMA.AsFloat:=rv(rQ*quMenuPRICE.AsFloat);
            teSpecNAME.AsString:=quMenuNAME.AsString;
            teSpecISTATUS.AsInteger:=0;
            teSpecLinkM.AsInteger:=quMenuLINK.AsInteger;
            if quMenuLIMITPRICE.AsFloat>0 then
            teSpecLimitM.AsInteger:=RoundEx(quMenuLIMITPRICE.AsFloat)
            else teSpecLimitM.AsInteger:=99;
            teSpecITYPE.AsInteger:=0;
            teSpec.Post;
            ViewSpec.EndUpdate;

            if quMenuLINK.AsInteger>0 then
            begin //������������
              acModify.Execute;
            end;
          end else
          begin
            rQ:=teSpecQuantity.AsFloat;

            teSpec.Edit;
            teSpecQuantity.AsFloat:=rQ+1;
            teSpecSumma.AsFloat:=teSpecSumma.AsFloat+rv(quMenuPRICE.AsFloat);
            teSpec.Post;
          end;
        end else Showmessage('���������� � �������.');
      end;
    end;
  end;
end;

procedure TfmMainRZD.ViewMCellClick(Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
Var StrWk:String;
begin
  AHandled:=True;
  bAddPosFromMenu:=False; //��� ���� ����� ������ � ��������� ����� ������������
  bAddPosFromSpec:=False;
  Sifr:=-1;
  StrWk:= ACellViewInfo.Value;
  if Pos('|',StrWk)>0 then
  begin
    Delete(StrWk,1,Pos('|',StrWk));
    Sifr:=StrToIntDef(StrWk,-1);
  end;
  acOpenSelMenu.Execute;
end;

procedure TfmMainRZD.Button1Click(Sender: TObject);
begin
  with dmR do
  begin
    ViewM.BeginUpdate;
    dsMenu.DataSet:=Nil;

    prFormMenu(0);

    dsMenu.DataSet:=quMenu;
    ViewM.EndUpdate;

    GridM.SetFocus;
  end;
end;

procedure TfmMainRZD.Button2Click(Sender: TObject);
Var iParent:INteger;
begin
  with dmR do
  begin
    iParent:=0;
    if CommonSet.UseDayMenu=0 then
    begin
      quMenuId.Active:=False;
      quMenuId.ParamByName('SIFR').AsInteger:=quMenuPARENT.AsInteger;
      quMenuId.Active:=True;
      quMenuId.First;
      if quMenuId.RecordCount>0 then  iParent:=quMenuIdPARENT.AsInteger else iParent:=0;
      quMenuId.Active:=False;
    end;
    if CommonSet.UseDayMenu=1 then
    begin
      quMenuDRec.Active:=False;
      quMenuDRec.ParamByName('IGR').AsInteger:=quMenuPARENT.AsInteger;
      quMenuDRec.Active:=True;
      quMenuDRec.First;
      if quMenuDRec.RecordCount>0 then  iParent:=quMenuDRecPARENT.AsInteger else iParent:=0;
      quMenuDRec.Active:=False;
    end;

    ViewM.BeginUpdate;
    dsMenu.DataSet:=Nil;
    prFormMenu(iParent);
    dsMenu.DataSet:=quMenu;
    ViewM.EndUpdate;

    quMenu.First;
    GridM.SetFocus;
  end;
end;

procedure TfmMainRZD.acModifyExecute(Sender: TObject);
begin
  //������������
  with dmR do
  begin
    quModif.Active:=False;
    quModif.ParamByName('PARENT').AsInteger:=teSpecLINKM.AsInteger;
    quModif.Active:=True;
    fmModifFF1:=TFmModifFF1.Create(Application);
    //������� �����
    MaxMod:=teSpecLimitM.AsInteger;
    CountMod:=0;  //���� ��� ������� ��� ���������

    fmModifFF1.Label1.Caption:='�������� � ���������� '+INtToStr(MaxMod-CountMod)+' ������������.';

    fmModifFF1.ShowModal;
    fmModifFF1.Release;
    quModif.Active:=False;
  end;
end;

procedure TfmMainRZD.ViewHKCellClick(Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
Var StrWk:String;
    iCode:Integer;
    rQ:Real;
    iMax:INteger;
begin
  if GridHK.Tag>0 then exit; //����� �������������� ��������
  if GridSpec.Tag=0 then exit; //���� ������ - �� ������

  bAddPosFromMenu:=False; //��� ���� ����� ������ � ��������� ����� ������������
  bAddPosFromSpec:=False;

  StrWk:=ACellViewInfo.Text;
//  Label2.Caption:=StrWk; // ������ ������� � ����������
  delete(StrWk,1,POS(#13,StrWk)); //�������� ������ ������
  delete(StrWk,1,POS(#13,StrWk)); //�������� ������ ������
  delete(StrWk,1,POS(#13,StrWk)); //����
//  Label3.Caption:=StrWk;  //��� ������   4-�� ������
//  ShowMessage(StrWk);
  iCode:=StrToIntDef(StrWk,0);
  if iCode>0 then
  begin
    //��� ������, �������
    with dmR do
    begin
      quMenuHK.Active:=False;
      quMenuHK.ParamByName('ISIFR').AsInteger:=iCode;
      qumenuHK.Active:=True;
      if quMenuHK.RecordCount>0 then
      begin
        quMenuHK.First;

        if (teSpecSifr.AsInteger<>quMenuHKSIFR.AsInteger)or(quMenuHKDESIGNSIFR.AsInteger>0) then //����� ������� ��� ����������� ���-��
          begin
            rQ:=1;
            if quMenuHKDISPKOEF.AsInteger=1 then //������� �����
            begin
              if Label3.Tag>0 then //������ 0 ����
              begin
                rQ:=Label3.Tag/1000;
              end;
            end;

            if quMenuHKDESIGNSIFR.AsInteger>0 then
            begin
              fmCalc1.CalcEdit1.EditValue:=rQ;
              fmCalc1.ShowModal;
              if (fmCalc1.ModalResult=mrOk)and(fmCalc1.CalcEdit1.EditValue>0.00001)  then
              begin
                rQ:=RoundEx(fmCalc1.CalcEdit1.EditValue*1000)/1000;
              end;
            end;

            iMax:=1;
            ViewSpec.BeginUpdate;
            teSpec.First;
            while not teSpec.Eof do
            begin
              if teSpecID.AsInteger>=iMax then iMax:=teSpecID.AsInteger+1;
              teSpec.Next;
            end;

            teSpec.Append;
            teSpecID.AsInteger:=iMax;
            teSpecSIFR.AsInteger:=quMenuHKSIFR.AsInteger;
            teSpecPRICE.AsFloat:=quMenuHKPRICE.AsFloat;
            teSpecQUANTITY.AsFloat:=rQ;
            teSpecSUMMA.AsFloat:=rv(rQ*quMenuHKPRICE.AsFloat);
            teSpecNAME.AsString:=quMenuHKNAME.AsString;
            teSpecISTATUS.AsInteger:=0;
            teSpecLinkM.AsInteger:=quMenuHKLINK.AsInteger;
            if quMenuHKLIMITPRICE.AsFloat>0 then
            teSpecLimitM.AsInteger:=RoundEx(quMenuHKLIMITPRICE.AsFloat)
            else teSpecLimitM.AsInteger:=99;
            teSpecITYPE.AsInteger:=0;
            teSpec.Post;
            ViewSpec.EndUpdate;

            if quMenuHKLINK.AsInteger>0 then
            begin //������������
              acModify.Execute;
            end;
          end else
          begin
            rQ:=teSpecQuantity.AsFloat;

            teSpec.Edit;
            teSpecQuantity.AsFloat:=rQ+1;
            teSpecSumma.AsFloat:=teSpecSumma.AsFloat+rv(quMenuHKPRICE.AsFloat);
            teSpec.Post;
          end;
      end;
      quMenuHK.Active:=False;
    end;
  end;
end;

procedure TfmMainRZD.cxButton19Click(Sender: TObject);
Var iNumP:INteger;
    iMax,iB:Integer;
begin
  // ���������
  with dmR do
  begin
    iNumP:=GridSpec.Tag;
    try
      ViewSpec.BeginUpdate;
      ViewModif.BeginUpdate;
      dsteMod.DataSet:=nil;
      dsteSpec.DataSet:=nil;

      quSpecP.Active:=False;
      quSpecP.ParamByName('IDH').AsInteger:=iNumP;
      quSpecP.Active:=True;
      quSpecP.First;
      while not quSpecP.Eof do quSpecP.Delete; //���� ��� , ���������, �� ������ ��������

      teSpec.First; iMax:=1;
      while not teSpec.Eof do
      begin
        quSpecP.Append;
        quSpecPID_TAB.AsInteger:=iNumP;
        quSpecPID_POS.AsInteger:=iMax;
        quSpecPID.AsInteger:=0;
        quSpecPSIFR.AsInteger:=teSpecSIFR.AsInteger;
        quSpecPNAME.AsString:=teSpecNAME.AsString;
        quSpecPPRICE.AsFloat:=teSpecPRICE.AsFloat;
        quSpecPQUANTITY.AsFloat:=teSpecQUANTITY.AsFloat;
        quSpecPSUMMA.AsFloat:=teSpecSUMMA.AsFloat;
        quSpecPLIMITM.AsInteger:=teSpecLIMITM.AsInteger;
        quSpecPLINKM.AsInteger:=teSpecLINKM.AsInteger;
        quSpecPITYPE.AsInteger:=0;
        quSpecP.Post;

        iB:=iMax;
        inc(iMax);

        teMod.First;
        while not teMod.Eof do
        begin
          if teModID_POS.AsInteger=teSpecID.AsInteger then
          begin
            quSpecP.Append;
            quSpecPID_TAB.AsInteger:=iNumP;
            quSpecPID_POS.AsInteger:=iMax;
            quSpecPID.AsInteger:=iB;
            quSpecPSIFR.AsInteger:=teModSIFR.AsInteger;
            quSpecPNAME.AsString:=teModNAME.AsString;
            quSpecPPRICE.AsFloat:=0;
            quSpecPQUANTITY.AsFloat:=teSpecQUANTITY.AsFloat; //����� ���-�� �� ��������� �����
            quSpecPSUMMA.AsFloat:=0;
            quSpecPLIMITM.AsInteger:=0;
            quSpecPLINKM.AsInteger:=0;
            quSpecPITYPE.AsInteger:=1;
            quSpecP.Post;

            inc(iMax);
          end;

          teMod.Next;
        end;
        teSpec.Next;
      end;

      CloseTe(teMod);
      CloseTe(teSpec);
//      quSpecP.Active:=False; //����� �������� ���������

      GridSpec.Tag:=0;
      cxTextEdit1.Text:=' ';
      Panel4.Visible:=False;
      Panel7.Visible:=False;

      if tiP.Enabled then
      begin
        tiP.Enabled:=False;
        dsquTabP.DataSet:=nil;
        if quTabP.Active then quTabP.FullRefresh else quTabP.Active:=True;
        dsquTabP.DataSet:=quTabP;
        tiP.Enabled:=True;
      end;

    finally
      dsteMod.DataSet:=teMod;
      dsteSpec.DataSet:=teSpec;

      ViewSpec.EndUpdate;
      ViewModif.EndUpdate;
    end;
  end;
end;

procedure TfmMainRZD.cxButton17Click(Sender: TObject);
begin
  if teSpec.RecordCount>0 then
  begin
    try
      ViewSpec.BeginUpdate;
      ViewModif.BeginUpdate;
      dsteMod.DataSet:=nil;
      dsteSpec.DataSet:=nil;

      teMod.First;
      while not teMod.Eof do
      begin
        if teModID_POS.AsInteger=teSpecID.AsInteger then teMod.Delete else teMod.Next;
      end;

      teSpec.Delete;
    finally
      dsteMod.DataSet:=teMod;
      dsteSpec.DataSet:=teSpec;

      ViewSpec.EndUpdate;
      ViewModif.EndUpdate;
    end;
  end;
end;

procedure TfmMainRZD.cxButton18Click(Sender: TObject);
Var rQ:Real;
begin
  with dmR do
  begin
    bAddPosFromMenu:=False; //��� ���� ����� ������ � ��������� ����� ������������
    bAddPosFromSpec:=False;

    if teSpec.RecordCount=0 then exit;
    fmCalc1.CalcEdit1.EditValue:=teSpecQuantity.AsFloat;
    fmCalc1.ShowModal;
    if fmCalc1.ModalResult=mrOk then
    begin
      delay(10);
      rQ:=RoundEx(fmCalc1.CalcEdit1.EditValue*1000)/1000;

      teSpec.Edit;
      teSpecQuantity.AsFloat:=rQ;
      teSpecSUMMA.AsFloat:=rv(rQ*teSpecPRICE.AsFloat);
      teSpec.Post;
    end
  end;
end;

procedure TfmMainRZD.tiPTimer(Sender: TObject);
begin
  tiP.Enabled:=False;
  with dmR do
  begin
    if CasherRnDb.Connected then
    begin
      dsquTabP.DataSet:=nil;
      if quTabP.Active then quTabP.FullRefresh else quTabP.Active:=True;

      dsquTabP.DataSet:=quTabP;
    end;
  end;
  tiP.Enabled:=True;
end;

procedure TfmMainRZD.ViewPCellDblClick(Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
Var iNumP:INteger;
    iMaxM,iMaxB:INteger;
begin
  with dmR do
  begin
    iNumP:=quTabPID_TAB.AsInteger;
    if GridSpec.tag<>iNumP then
    begin
      if GridSpec.tag=0 then
      begin
        with dmR do
        begin
          GridSpec.tag:=iNumP;
          try
            ViewSpec.BeginUpdate;
            ViewModif.BeginUpdate;

            CloseTe(teMod);
            CloseTe(teSpec);
            Check.Max:=1;
            quSpecP.Active:=False;
            quSpecP.ParamByName('IDH').AsInteger:=iNumP;
            quSpecP.Active:=True;
            quSpecP.First;
            while not quSpecP.Eof do
            begin
              if quSpecPITYPE.AsInteger=0 then   //��� �����
              begin
                iMaxB:=teSpec.RecordCount;

                teSpec.Append;
                teSpecID.AsInteger:=iMaxB+1;
                teSpecSIFR.AsInteger:=quSpecPSIFR.AsInteger;
                teSpecPRICE.AsFloat:=quSpecPPRICE.AsFloat;
                teSpecQUANTITY.AsFloat:=quSpecPQUANTITY.AsFloat;
                teSpecSUMMA.AsFloat:=quSpecPSUMMA.AsFloat;
                teSpecNAME.AsString:=quSpecPNAME.AsString;
                teSpecISTATUS.AsInteger:=1;
                teSpecLIMITM.AsInteger:=quSpecPLIMITM.AsInteger;
                teSpecLINKM.AsInteger:=quSpecPLINKM.AsInteger;
                teSpecITYPE.AsInteger:=0;
                teSpec.Post;
              end else //��� �����������
              begin
                iMaxM:=teMod.RecordCount;

                teMod.Append;
                teModID_POS.AsInteger:=teSpecID.AsInteger;
                teModID.AsInteger:=iMaxM+1;
                teModSIFR.AsInteger:=quSpecPSIFR.AsInteger;
                teModNAME.AsString:=quSpecPNAME.AsString;
                teModQUANTITY.AsFloat:=quSpecPQUANTITY.AsFloat;
                teMod.Post;
              end;

              quSpecP.Next;
            end;
            quSpecP.Active:=False;
          finally
            ViewSpec.EndUpdate;
            ViewModif.EndUpdate;
          end;

          //�������� ��������������

          cxTextEdit1.Text:='������ � '+its(iNumP);
          Panel4.Visible:=True;
          Panel7.Visible:=True;
        end;
      end else Showmessage('������� ��������� ������ � �������� ������..');
    end;
  end;
end;

procedure TfmMainRZD.TimerVesTimer(Sender: TObject);
Var n: Integer;
    Buff:array[1..3] of char;
    rVes:Real;
    S1,S2:String;
begin
  TimerVes.Enabled:=False;
  with dmR do
  begin
    sScanEv:='';
    iReadCount:=0;
    n:=0;
//    Memo1.Lines.Add('.');
    Buff[1]:=#$05;
    devCas.WriteBuf(Buff,1);
    delay(5);
    Buff[1]:=#$12;
    devCas.WriteBuf(Buff,1);

//    Memo1.Lines.Add('..');
    while (iReadCount<38)and(n<10) do
    begin
      delay(50);
      inc(n);
    end;
    rVes:=0;
//    Memo1.Lines.Add('...'+sScanEv);
    if pos('kg',sScanEv)>0 then
    begin
      S1:=Copy(sScanEv,pos('kg',sScanEv)-8,8);
      S2:=S1[1];
      delete(S1,1,1);
      while pos(' ',S1)>0 do delete(S1,pos(' ',S1),1);
      while pos('.',S1)>0 do S1[pos('.',S1)]:=',';
      rVes:=StrToFloatDef(S1,0);
//      Memo1.Lines.Add('...'+S2);
    end;
    if S2='S' then
    begin
      fmCalc1.Label1.Caption:='���� ��. ��� - '+S1;
      fmCalc1.cxButton18.Tag:=RoundEx(rVes*1000);
      Label3.Caption:='������� ��� '+S1;
      Label3.Tag:=RoundEx(rVes*1000);
//      Memo1.Lines.Add('....'+S2+S1);
    end else
    begin
//      Memo1.Lines.Add('...._'+S2);
      fmCalc1.cxButton18.Tag:=0;
      fmCalc1.Label1.Caption:='���� ��. ��� ������������.';
      if Label3.Caption>'' then Label3.Caption:='..';
      Label3.Tag:=0;
    end;
  end;
  TimerVes.Enabled:=True;
end;

procedure TfmMainRZD.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  try
    if bScale then dmR.devCas.Close;
  except
  end;
end;

end.
