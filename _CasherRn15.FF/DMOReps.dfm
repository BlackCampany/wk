object dmORep: TdmORep
  OldCreateOrder = False
  Left = 271
  Top = 191
  Height = 583
  Width = 792
  object taPartTest: TClientDataSet
    Aggregates = <>
    FileName = 'PartTest.cds'
    Params = <>
    Left = 24
    Top = 16
    object taPartTestNum: TIntegerField
      FieldName = 'Num'
    end
    object taPartTestIdGoods: TIntegerField
      FieldName = 'IdGoods'
    end
    object taPartTestNameG: TStringField
      FieldName = 'NameG'
      Size = 200
    end
    object taPartTestIM: TIntegerField
      FieldName = 'IM'
    end
    object taPartTestSM: TStringField
      FieldName = 'SM'
      Size = 50
    end
    object taPartTestQuant: TFloatField
      FieldName = 'Quant'
      DisplayFormat = '0.000'
      EditFormat = '0.000'
    end
    object taPartTestPrice1: TCurrencyField
      FieldName = 'Price1'
      DisplayFormat = ',0.00;-,0.00'
      EditFormat = ',0.00;-,0.00'
    end
    object taPartTestiRes: TSmallintField
      FieldName = 'iRes'
    end
  end
  object dsPartTest: TDataSource
    DataSet = taPartTest
    Left = 24
    Top = 72
  end
  object taCalc: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 89
    Top = 16
    object taCalcArticul: TIntegerField
      FieldName = 'Articul'
    end
    object taCalcName: TStringField
      FieldName = 'Name'
      Size = 100
    end
    object taCalcIdm: TIntegerField
      FieldName = 'Idm'
    end
    object taCalcsM: TStringField
      FieldName = 'sM'
    end
    object taCalcKm: TFloatField
      FieldName = 'Km'
    end
    object taCalcQuant: TFloatField
      FieldName = 'Quant'
      DisplayFormat = '0.000'
    end
    object taCalcQuantFact: TFloatField
      FieldName = 'QuantFact'
      DisplayFormat = '0.000'
    end
    object taCalcQuantDiff: TFloatField
      FieldName = 'QuantDiff'
      DisplayFormat = '0.000'
    end
    object taCalcSumUch: TFloatField
      FieldName = 'SumUch'
      DisplayFormat = '0.00'
    end
    object taCalcSumIn: TFloatField
      FieldName = 'SumIn'
      DisplayFormat = '0.00'
    end
  end
  object dsCalc: TDataSource
    DataSet = taCalc
    Left = 89
    Top = 72
  end
  object frRep1: TfrReport
    InitialZoom = pzDefault
    PreviewButtons = [pbZoom, pbLoad, pbSave, pbPrint, pbFind, pbHelp, pbExit]
    RebuildPrinter = False
    Left = 248
    Top = 16
    ReportForm = {19000000}
  end
  object frTextExport1: TfrTextExport
    ScaleX = 1.000000000000000000
    ScaleY = 1.000000000000000000
    Left = 968
    Top = 560
  end
  object frRTFExport1: TfrRTFExport
    ScaleX = 1.300000000000000000
    ScaleY = 1.000000000000000000
    Left = 968
    Top = 504
  end
  object frCSVExport1: TfrCSVExport
    ScaleX = 1.000000000000000000
    ScaleY = 1.000000000000000000
    Delimiter = ';'
    Left = 968
    Top = 344
  end
  object frHTMExport1: TfrHTMExport
    ScaleX = 1.000000000000000000
    ScaleY = 1.000000000000000000
    Left = 968
    Top = 200
  end
  object frHTML2Export1: TfrHTML2Export
    Scale = 1.000000000000000000
    Navigator.Position = []
    Navigator.Font.Charset = DEFAULT_CHARSET
    Navigator.Font.Color = clWindowText
    Navigator.Font.Height = -11
    Navigator.Font.Name = 'MS Sans Serif'
    Navigator.Font.Style = []
    Navigator.InFrame = False
    Navigator.WideInFrame = False
    Left = 968
    Top = 248
  end
  object frOLEExcelExport1: TfrOLEExcelExport
    HighQuality = False
    CellsAlign = False
    CellsBorders = False
    CellsFillColor = False
    CellsFontColor = False
    CellsFontName = False
    CellsFontSize = False
    CellsFontStyle = False
    CellsMerged = False
    CellsWrapWords = False
    ExportPictures = False
    PageBreaks = False
    AsText = False
    Left = 968
    Top = 448
  end
  object frXMLExcelExport1: TfrXMLExcelExport
    Left = 968
    Top = 400
  end
  object frTextAdvExport1: TfrTextAdvExport
    ScaleWidth = 1.000000000000000000
    ScaleHeight = 1.000000000000000000
    Borders = True
    Pseudogrpahic = False
    PageBreaks = True
    OEMCodepage = False
    EmptyLines = True
    LeadSpaces = True
    PrintAfter = False
    PrinterDialog = True
    UseSavedProps = True
    Left = 968
    Top = 296
  end
  object frRtfAdvExport1: TfrRtfAdvExport
    OpenAfterExport = True
    Wysiwyg = True
    Creator = 'FastReport http://www.fast-report.com'
    Left = 968
    Top = 152
  end
  object frBMPExport1: TfrBMPExport
    Left = 968
    Top = 8
  end
  object frJPEGExport1: TfrJPEGExport
    Left = 968
    Top = 56
  end
  object frTIFFExport1: TfrTIFFExport
    Left = 968
    Top = 104
  end
  object taCalcB: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'ID'
        DataType = ftInteger
      end
      item
        Name = 'CODEB'
        DataType = ftInteger
      end
      item
        Name = 'NAMEB'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'QUANT'
        DataType = ftFloat
      end
      item
        Name = 'PRICEOUT'
        DataType = ftFloat
      end
      item
        Name = 'SUMOUT'
        DataType = ftFloat
      end
      item
        Name = 'IDCARD'
        DataType = ftInteger
      end
      item
        Name = 'NAMEC'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'QUANTC'
        DataType = ftFloat
      end
      item
        Name = 'PRICEIN'
        DataType = ftFloat
      end
      item
        Name = 'SUMIN'
        DataType = ftFloat
      end
      item
        Name = 'IM'
        DataType = ftInteger
      end
      item
        Name = 'SM'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'SB'
        DataType = ftString
        Size = 10
      end>
    IndexDefs = <
      item
        Name = 'taCalcBIndex'
        Fields = 'ID'
        Options = [ixPrimary]
      end>
    IndexFieldNames = 'ID'
    Params = <>
    StoreDefs = True
    Left = 161
    Top = 16
    object taCalcBID: TIntegerField
      FieldName = 'ID'
    end
    object taCalcBCODEB: TIntegerField
      FieldName = 'CODEB'
    end
    object taCalcBNAMEB: TStringField
      FieldName = 'NAMEB'
      Size = 100
    end
    object taCalcBQUANT: TFloatField
      FieldName = 'QUANT'
      DisplayFormat = '0.000'
    end
    object taCalcBPRICEOUT: TFloatField
      FieldName = 'PRICEOUT'
      DisplayFormat = '0.00'
    end
    object taCalcBSUMOUT: TFloatField
      FieldName = 'SUMOUT'
      DisplayFormat = '0.00'
    end
    object taCalcBIDCARD: TIntegerField
      FieldName = 'IDCARD'
    end
    object taCalcBNAMEC: TStringField
      FieldName = 'NAMEC'
      Size = 100
    end
    object taCalcBQUANTC: TFloatField
      FieldName = 'QUANTC'
      DisplayFormat = '0.000'
    end
    object taCalcBPRICEIN: TFloatField
      FieldName = 'PRICEIN'
      DisplayFormat = '0.00'
    end
    object taCalcBSUMIN: TFloatField
      FieldName = 'SUMIN'
      DisplayFormat = '0.00'
    end
    object taCalcBIM: TIntegerField
      FieldName = 'IM'
    end
    object taCalcBSM: TStringField
      FieldName = 'SM'
    end
    object taCalcBSB: TStringField
      FieldName = 'SB'
      Size = 10
    end
  end
  object dsCalcB: TDataSource
    DataSet = taCalcB
    Left = 160
    Top = 72
  end
  object frdsCalcB: TfrDBDataSet
    DataSource = dsCalcB
    Left = 320
    Top = 16
  end
  object taTORep: TClientDataSet
    Aggregates = <>
    FileName = 'to.cds'
    FieldDefs = <
      item
        Name = 'sType'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'sDocType'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'sCliName'
        DataType = ftString
        Size = 200
      end
      item
        Name = 'sDate'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'sDocNum'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'rSum'
        DataType = ftCurrency
      end
      item
        Name = 'rSum1'
        DataType = ftCurrency
      end
      item
        Name = 'rSumO'
        DataType = ftCurrency
      end
      item
        Name = 'rSumO1'
        DataType = ftCurrency
      end>
    IndexDefs = <
      item
        Name = 'taTORepIndex1'
        Fields = 'sType;sDocType'
      end>
    IndexName = 'taTORepIndex1'
    Params = <>
    StoreDefs = True
    Left = 32
    Top = 144
    object taTORepsType: TStringField
      FieldName = 'sType'
    end
    object taTORepsDocType: TStringField
      FieldName = 'sDocType'
      Size = 50
    end
    object taTORepsCliName: TStringField
      FieldName = 'sCliName'
      Size = 200
    end
    object taTORepsDate: TStringField
      FieldName = 'sDate'
      Size = 10
    end
    object taTORepsDocNum: TStringField
      FieldName = 'sDocNum'
    end
    object taTOReprSum: TCurrencyField
      FieldName = 'rSum'
    end
    object taTOReprSum1: TCurrencyField
      FieldName = 'rSum1'
    end
    object taTOReprSumO: TCurrencyField
      FieldName = 'rSumO'
    end
    object taTOReprSumO1: TCurrencyField
      FieldName = 'rSumO1'
    end
  end
  object dsTORep: TDataSource
    DataSet = taTORep
    Left = 32
    Top = 200
  end
  object frdsTORep: TfrDBDataSet
    DataSource = dsTORep
    Left = 320
    Top = 72
  end
  object taCMove: TClientDataSet
    Aggregates = <>
    FileName = 'CMove.cds'
    Params = <>
    Left = 104
    Top = 144
    object taCMoveARTICUL: TIntegerField
      FieldName = 'ARTICUL'
    end
    object taCMoveIDATE: TIntegerField
      FieldName = 'IDATE'
    end
    object taCMoveIDSTORE: TIntegerField
      FieldName = 'IDSTORE'
    end
    object taCMoveNAMEMH: TStringField
      FieldName = 'NAMEMH'
      Size = 200
    end
    object taCMoveRB: TFloatField
      FieldName = 'RB'
      DisplayFormat = '0.000'
    end
    object taCMovePOSTIN: TFloatField
      FieldName = 'POSTIN'
      DisplayFormat = '0.000'
    end
    object taCMovePOSTOUT: TFloatField
      FieldName = 'POSTOUT'
      DisplayFormat = '0.000'
    end
    object taCMoveVNIN: TFloatField
      FieldName = 'VNIN'
      DisplayFormat = '0.000'
    end
    object taCMoveVNOUT: TFloatField
      FieldName = 'VNOUT'
      DisplayFormat = '0.000'
    end
    object taCMoveINV: TFloatField
      FieldName = 'INV'
      DisplayFormat = '0.000'
    end
    object taCMoveQREAL: TFloatField
      FieldName = 'QREAL'
      DisplayFormat = '0.000'
    end
    object taCMoveRE: TFloatField
      FieldName = 'RE'
      DisplayFormat = '0.000'
    end
  end
  object dstaCMove: TDataSource
    DataSet = taCMove
    Left = 104
    Top = 200
  end
  object quDocsInvSel: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHEADINV'
      'SET '
      '    DATEDOC = :DATEDOC,'
      '    NUMDOC = :NUMDOC,'
      '    IDSKL = :IDSKL,'
      '    IACTIVE = :IACTIVE,'
      '    ITYPE = :ITYPE,'
      '    SUM1 = :SUM1,'
      '    SUM11 = :SUM11,'
      '    SUM2 = :SUM2,'
      '    SUM21 = :SUM21'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHEADINV'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHEADINV('
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    IDSKL,'
      '    IACTIVE,'
      '    ITYPE,'
      '    SUM1,'
      '    SUM11,'
      '    SUM2,'
      '    SUM21'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATEDOC,'
      '    :NUMDOC,'
      '    :IDSKL,'
      '    :IACTIVE,'
      '    :ITYPE,'
      '    :SUM1,'
      '    :SUM11,'
      '    :SUM2,'
      '    :SUM21'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT IH.ID,IH.DATEDOC,IH.NUMDOC,IH.IDSKL,IH.IACTIVE,IH.ITYPE,I' +
        'H.SUM1,'
      
        '       IH.SUM11,IH.SUM2,IH.SUM21,(IH.SUM2-IH.SUM1) as SumDifIn,(' +
        'IH.SUM21-IH.SUM11) as SumDifUch,'
      '       mh.NAMEMH'
      'FROM OF_DOCHEADINV IH'
      'left join OF_MH mh on mh.ID=IH.IDSKL'
      ''
      'Where(  IH.DATEDOC>=:DATEB and IH.DATEDOC<:DATEE'
      '     ) and (     IH.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT IH.ID,IH.DATEDOC,IH.NUMDOC,IH.IDSKL,IH.IACTIVE,IH.ITYPE,I' +
        'H.SUM1,'
      
        '       IH.SUM11,IH.SUM2,IH.SUM21,(IH.SUM2-IH.SUM1) as SumDifIn,(' +
        'IH.SUM21-IH.SUM11) as SumDifUch,'
      '       mh.NAMEMH'
      'FROM OF_DOCHEADINV IH'
      'left join OF_MH mh on mh.ID=IH.IDSKL'
      ''
      'Where IH.DATEDOC>=:DATEB and IH.DATEDOC<:DATEE')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 432
    Top = 16
    poAskRecordCount = True
    object quDocsInvSelID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quDocsInvSelDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object quDocsInvSelNUMDOC: TFIBStringField
      FieldName = 'NUMDOC'
      Size = 15
      EmptyStrToNull = True
    end
    object quDocsInvSelIDSKL: TFIBIntegerField
      FieldName = 'IDSKL'
    end
    object quDocsInvSelNAMEMH: TFIBStringField
      FieldName = 'NAMEMH'
      Size = 100
      EmptyStrToNull = True
    end
    object quDocsInvSelIACTIVE: TFIBIntegerField
      FieldName = 'IACTIVE'
    end
    object quDocsInvSelITYPE: TFIBIntegerField
      FieldName = 'ITYPE'
    end
    object quDocsInvSelSUM1: TFIBFloatField
      FieldName = 'SUM1'
      DisplayFormat = '0.00'
    end
    object quDocsInvSelSUM11: TFIBFloatField
      FieldName = 'SUM11'
      DisplayFormat = '0.00'
    end
    object quDocsInvSelSUM2: TFIBFloatField
      FieldName = 'SUM2'
      DisplayFormat = '0.00'
    end
    object quDocsInvSelSUM21: TFIBFloatField
      FieldName = 'SUM21'
      DisplayFormat = '0.00'
    end
    object quDocsInvSelSUMDIFIN: TFIBFloatField
      FieldName = 'SUMDIFIN'
      DisplayFormat = '0.00'
    end
    object quDocsInvSelSUMDIFUCH: TFIBFloatField
      FieldName = 'SUMDIFUCH'
      DisplayFormat = '0.00'
    end
  end
  object dsquDocsInvSel: TDataSource
    DataSet = quDocsInvSel
    Left = 432
    Top = 64
  end
  object quDocsInvId: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHEADINV'
      'SET '
      '    DATEDOC = :DATEDOC,'
      '    NUMDOC = :NUMDOC,'
      '    IDSKL = :IDSKL,'
      '    IACTIVE = :IACTIVE,'
      '    ITYPE = :ITYPE,'
      '    SUM1 = :SUM1,'
      '    SUM11 = :SUM11,'
      '    SUM2 = :SUM2,'
      '    SUM21 = :SUM21'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHEADINV'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHEADINV('
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    IDSKL,'
      '    IACTIVE,'
      '    ITYPE,'
      '    SUM1,'
      '    SUM11,'
      '    SUM2,'
      '    SUM21'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATEDOC,'
      '    :NUMDOC,'
      '    :IDSKL,'
      '    :IACTIVE,'
      '    :ITYPE,'
      '    :SUM1,'
      '    :SUM11,'
      '    :SUM2,'
      '    :SUM21'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT IH.ID,IH.DATEDOC,IH.NUMDOC,IH.IDSKL,IH.IACTIVE,IH.ITYPE,I' +
        'H.SUM1,IH.SUM11,IH.SUM2,IH.SUM21'
      'FROM OF_DOCHEADINV IH'
      'Where(  IH.ID=:IDH'
      '     ) and (     IH.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT IH.ID,IH.DATEDOC,IH.NUMDOC,IH.IDSKL,IH.IACTIVE,IH.ITYPE,I' +
        'H.SUM1,IH.SUM11,IH.SUM2,IH.SUM21'
      'FROM OF_DOCHEADINV IH'
      'Where IH.ID=:IDH')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 592
    Top = 16
    poAskRecordCount = True
    object quDocsInvIdID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quDocsInvIdDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object quDocsInvIdNUMDOC: TFIBStringField
      FieldName = 'NUMDOC'
      Size = 15
      EmptyStrToNull = True
    end
    object quDocsInvIdIDSKL: TFIBIntegerField
      FieldName = 'IDSKL'
    end
    object quDocsInvIdIACTIVE: TFIBIntegerField
      FieldName = 'IACTIVE'
    end
    object quDocsInvIdITYPE: TFIBIntegerField
      FieldName = 'ITYPE'
    end
    object quDocsInvIdSUM1: TFIBFloatField
      FieldName = 'SUM1'
    end
    object quDocsInvIdSUM11: TFIBFloatField
      FieldName = 'SUM11'
    end
    object quDocsInvIdSUM2: TFIBFloatField
      FieldName = 'SUM2'
    end
    object quDocsInvIdSUM21: TFIBFloatField
      FieldName = 'SUM21'
    end
  end
  object quSpecInv: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCSPECINV'
      'SET '
      '    NUM = :NUM,'
      '    IDCARD = :IDCARD,'
      '    QUANT = :QUANT,'
      '    SUMIN = :SUMIN,'
      '    SUMUCH = :SUMUCH,'
      '    IDMESSURE = :IDMESSURE,'
      '    QUANTFACT = :QUANTFACT,'
      '    SUMINFACT = :SUMINFACT,'
      '    SUMUCHFACT = :SUMUCHFACT,'
      '    KM = :KM,'
      '    TCARD = :TCARD,'
      '    IDGROUP = :IDGROUP'
      'WHERE'
      '    IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCSPECINV'
      'WHERE'
      '        IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCSPECINV('
      '    IDHEAD,'
      '    ID,'
      '    NUM,'
      '    IDCARD,'
      '    QUANT,'
      '    SUMIN,'
      '    SUMUCH,'
      '    IDMESSURE,'
      '    QUANTFACT,'
      '    SUMINFACT,'
      '    SUMUCHFACT,'
      '    KM,'
      '    TCARD,'
      '    IDGROUP'
      ')'
      'VALUES('
      '    :IDHEAD,'
      '    :ID,'
      '    :NUM,'
      '    :IDCARD,'
      '    :QUANT,'
      '    :SUMIN,'
      '    :SUMUCH,'
      '    :IDMESSURE,'
      '    :QUANTFACT,'
      '    :SUMINFACT,'
      '    :SUMUCHFACT,'
      '    :KM,'
      '    :TCARD,'
      '    :IDGROUP'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT IDHEAD,ID,NUM,IDCARD,QUANT,SUMIN,SUMUCH,IDMESSURE,QUANTFA' +
        'CT,SUMINFACT,SUMUCHFACT,KM,TCARD,IDGROUP'
      'FROM OF_DOCSPECINV '
      'where(  IDHEAD=:IDH'
      '     ) and (     OF_DOCSPECINV.IDHEAD = :OLD_IDHEAD'
      '    and OF_DOCSPECINV.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT s.IDHEAD,s.ID,s.NUM,s.IDCARD,s.QUANT,s.SUMIN,s.SUMUCH,s.I' +
        'DMESSURE,'
      's.QUANTFACT,s.SUMINFACT,s.SUMUCHFACT,s.KM,s.TCARD,s.IDGROUP,'
      'me.NAMESHORT,cl.NAMECL,ca.NAME'
      'FROM OF_DOCSPECINV s'
      'left join OF_MESSUR me on me.ID=s.IDMESSURE'
      'left join OF_CLASSIF cl on cl.ID=s.IDGROUP'
      'left join of_cards ca on ca.ID=s.IDCARD'
      'where IDHEAD=:IDH')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 664
    Top = 16
    poAskRecordCount = True
    object quSpecInvIDHEAD: TFIBIntegerField
      FieldName = 'IDHEAD'
    end
    object quSpecInvID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quSpecInvNUM: TFIBIntegerField
      FieldName = 'NUM'
    end
    object quSpecInvIDCARD: TFIBIntegerField
      FieldName = 'IDCARD'
    end
    object quSpecInvQUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quSpecInvSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quSpecInvSUMUCH: TFIBFloatField
      FieldName = 'SUMUCH'
    end
    object quSpecInvIDMESSURE: TFIBIntegerField
      FieldName = 'IDMESSURE'
    end
    object quSpecInvQUANTFACT: TFIBFloatField
      FieldName = 'QUANTFACT'
    end
    object quSpecInvSUMINFACT: TFIBFloatField
      FieldName = 'SUMINFACT'
    end
    object quSpecInvSUMUCHFACT: TFIBFloatField
      FieldName = 'SUMUCHFACT'
    end
    object quSpecInvKM: TFIBFloatField
      FieldName = 'KM'
    end
    object quSpecInvTCARD: TFIBSmallIntField
      FieldName = 'TCARD'
    end
    object quSpecInvIDGROUP: TFIBIntegerField
      FieldName = 'IDGROUP'
    end
    object quSpecInvNAMESHORT: TFIBStringField
      FieldName = 'NAMESHORT'
      Size = 50
      EmptyStrToNull = True
    end
    object quSpecInvNAMECL: TFIBStringField
      FieldName = 'NAMECL'
      Size = 150
      EmptyStrToNull = True
    end
    object quSpecInvNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 200
      EmptyStrToNull = True
    end
  end
  object quSpecInvC: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCSPECINVC'
      'SET '
      '    NUM = :NUM,'
      '    IDCARD = :IDCARD,'
      '    QUANT = :QUANT,'
      '    SUMIN = :SUMIN,'
      '    SUMUCH = :SUMUCH,'
      '    IDMESSURE = :IDMESSURE,'
      '    QUANTFACT = :QUANTFACT,'
      '    SUMINFACT = :SUMINFACT,'
      '    SUMUCHFACT = :SUMUCHFACT,'
      '    KM = :KM,'
      '    TCARD = :TCARD,'
      '    IDGROUP = :IDGROUP'
      'WHERE'
      '    IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCSPECINVC'
      'WHERE'
      '        IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCSPECINVC('
      '    IDHEAD,'
      '    ID,'
      '    NUM,'
      '    IDCARD,'
      '    QUANT,'
      '    SUMIN,'
      '    SUMUCH,'
      '    IDMESSURE,'
      '    QUANTFACT,'
      '    SUMINFACT,'
      '    SUMUCHFACT,'
      '    KM,'
      '    TCARD,'
      '    IDGROUP'
      ')'
      'VALUES('
      '    :IDHEAD,'
      '    :ID,'
      '    :NUM,'
      '    :IDCARD,'
      '    :QUANT,'
      '    :SUMIN,'
      '    :SUMUCH,'
      '    :IDMESSURE,'
      '    :QUANTFACT,'
      '    :SUMINFACT,'
      '    :SUMUCHFACT,'
      '    :KM,'
      '    :TCARD,'
      '    :IDGROUP'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT s.IDHEAD,s.ID,s.NUM,s.IDCARD,s.QUANT,s.SUMIN,s.SUMUCH,s.I' +
        'DMESSURE,'
      's.QUANTFACT,s.SUMINFACT,s.SUMUCHFACT,s.KM,s.TCARD,s.IDGROUP,'
      'me.NAMESHORT,cl.NAMECL,ca.NAME'
      'FROM OF_DOCSPECINVC s'
      'left join OF_MESSUR me on me.ID=s.IDMESSURE'
      'left join OF_CLASSIF cl on cl.ID=s.IDGROUP'
      'left join of_cards ca on ca.ID=s.IDCARD'
      'where(  IDHEAD=:IDH'
      '     ) and (     S.IDHEAD = :OLD_IDHEAD'
      '    and S.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT s.IDHEAD,s.ID,s.NUM,s.IDCARD,s.QUANT,s.SUMIN,s.SUMUCH,s.I' +
        'DMESSURE,'
      's.QUANTFACT,s.SUMINFACT,s.SUMUCHFACT,s.KM,s.TCARD,s.IDGROUP,'
      'me.NAMESHORT,cl.NAMECL,ca.NAME'
      'FROM OF_DOCSPECINVC s'
      'left join OF_MESSUR me on me.ID=s.IDMESSURE'
      'left join OF_CLASSIF cl on cl.ID=s.IDGROUP'
      'left join of_cards ca on ca.ID=s.IDCARD'
      'where IDHEAD=:IDH')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 664
    Top = 72
    poAskRecordCount = True
    object quSpecInvCIDHEAD: TFIBIntegerField
      FieldName = 'IDHEAD'
    end
    object quSpecInvCID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quSpecInvCNUM: TFIBIntegerField
      FieldName = 'NUM'
    end
    object quSpecInvCIDCARD: TFIBIntegerField
      FieldName = 'IDCARD'
    end
    object quSpecInvCQUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quSpecInvCSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quSpecInvCSUMUCH: TFIBFloatField
      FieldName = 'SUMUCH'
    end
    object quSpecInvCIDMESSURE: TFIBIntegerField
      FieldName = 'IDMESSURE'
    end
    object quSpecInvCQUANTFACT: TFIBFloatField
      FieldName = 'QUANTFACT'
    end
    object quSpecInvCSUMINFACT: TFIBFloatField
      FieldName = 'SUMINFACT'
    end
    object quSpecInvCSUMUCHFACT: TFIBFloatField
      FieldName = 'SUMUCHFACT'
    end
    object quSpecInvCKM: TFIBFloatField
      FieldName = 'KM'
    end
    object quSpecInvCTCARD: TFIBSmallIntField
      FieldName = 'TCARD'
    end
    object quSpecInvCIDGROUP: TFIBIntegerField
      FieldName = 'IDGROUP'
    end
    object quSpecInvCNAMESHORT: TFIBStringField
      FieldName = 'NAMESHORT'
      Size = 50
      EmptyStrToNull = True
    end
    object quSpecInvCNAMECL: TFIBStringField
      FieldName = 'NAMECL'
      Size = 150
      EmptyStrToNull = True
    end
    object quSpecInvCNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 200
      EmptyStrToNull = True
    end
  end
  object quSelPrib: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT cl.NAMECL,ca.PARENT,vr.CODEB,ca.NAME,'
      
        'SUM(vr.QUANT) as QUANT ,SUM(vr.SUMOUT)as SUMOUT,SUM(vr.SUMIN)as ' +
        'SUMIN'
      'FROM OF_VREALB vr'
      'left join OF_CARDS ca on ca.ID=vr.CODEB'
      'left join of_classif cl on cl.ID=ca.PARENT'
      'WHERE vr.DATEDOC>=:DATEB and vr.DATEDOC<:DATEE'
      'and vr.IDSKL=:IDSTORE'
      'GROUP by cl.NAMECL,ca.PARENT,vr.CODEB,ca.NAME'
      'ORDER by cl.NAMECL')
    Transaction = trSelRep
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    Left = 280
    Top = 136
    poAskRecordCount = True
    object quSelPribNAMECL: TFIBStringField
      FieldName = 'NAMECL'
      Size = 150
      EmptyStrToNull = True
    end
    object quSelPribPARENT: TFIBIntegerField
      FieldName = 'PARENT'
    end
    object quSelPribCODEB: TFIBIntegerField
      FieldName = 'CODEB'
    end
    object quSelPribNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 200
      EmptyStrToNull = True
    end
    object quSelPribQUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quSelPribSUMOUT: TFIBFloatField
      FieldName = 'SUMOUT'
    end
    object quSelPribSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
  end
  object quSelPribGr: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT cl.NAMECL,ca.PARENT,'
      
        'SUM(vr.QUANT) as QUANT ,SUM(vr.SUMOUT)as SUMOUT,SUM(vr.SUMIN)as ' +
        'SUMIN'
      'FROM OF_VREALB vr'
      'left join OF_CARDS ca on ca.ID=vr.CODEB'
      'left join of_classif cl on cl.ID=ca.PARENT'
      'WHERE vr.DATEDOC>=:DATEB and vr.DATEDOC<:DATEE'
      'and vr.IDSKL=:IDSTORE'
      'GROUP by cl.NAMECL,ca.PARENT')
    Transaction = trSelRep
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    Left = 352
    Top = 136
    poAskRecordCount = True
    object quSelPribGrNAMECL: TFIBStringField
      FieldName = 'NAMECL'
      Size = 150
      EmptyStrToNull = True
    end
    object quSelPribGrPARENT: TFIBIntegerField
      FieldName = 'PARENT'
    end
    object quSelPribGrQUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quSelPribGrSUMOUT: TFIBFloatField
      FieldName = 'SUMOUT'
    end
    object quSelPribGrSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
  end
  object trSelRep: TpFIBTransaction
    DefaultDatabase = dmO.OfficeRnDb
    TimeoutAction = TARollback
    Left = 208
    Top = 136
  end
  object quDocOutBPrint: TpFIBDataSet
    SelectSQL.Strings = (
      
        'SELECT vr.IDB,vr.CODEB,vr.QUANT,vr.SUMOUT,vr.IDSKL,vr.SUMIN,ds.N' +
        'AMEB,ds.PRICER'
      'FROM OF_VREALB_DOC vr'
      
        'left join of_docspecoutb ds on ((ds.IDHEAD=:IDH)and(ds.IDCARD=vr' +
        '.CODEB))')
    Transaction = trSelRep
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    Left = 280
    Top = 192
    poAskRecordCount = True
    object quDocOutBPrintIDB: TFIBIntegerField
      FieldName = 'IDB'
    end
    object quDocOutBPrintCODEB: TFIBIntegerField
      FieldName = 'CODEB'
    end
    object quDocOutBPrintQUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quDocOutBPrintSUMOUT: TFIBFloatField
      FieldName = 'SUMOUT'
    end
    object quDocOutBPrintIDSKL: TFIBIntegerField
      FieldName = 'IDSKL'
    end
    object quDocOutBPrintSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quDocOutBPrintNAMEB: TFIBStringField
      FieldName = 'NAMEB'
      Size = 100
      EmptyStrToNull = True
    end
    object quDocOutBPrintPRICER: TFIBFloatField
      FieldName = 'PRICER'
    end
  end
  object quSelInSum: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT ARTICUL,SUM(QPART*PRICEIN) as SUMIN,SUM(QPART) as QSUM'
      'FROM OF_PARTIN'
      'where IDATE>=:DATEB and IDATE<:DATEE and IDSTORE=:IDSKL'
      'Group by ARTICUL')
    Transaction = trSelRep
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    Left = 432
    Top = 136
    object quSelInSumARTICUL: TFIBIntegerField
      FieldName = 'ARTICUL'
    end
    object quSelInSumSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quSelInSumQSUM: TFIBFloatField
      FieldName = 'QSUM'
    end
  end
  object quSelNamePos: TpFIBDataSet
    SelectSQL.Strings = (
      
        'SELECT cl.NAMECL,ca.PARENT,ca.NAME,ME.NAMESHORT as SM,ME.KOEF,ca' +
        '.IMESSURE'
      'FROM OF_CARDS ca'
      'left join of_classif cl on cl.ID=ca.PARENT'
      'left join OF_MESSUR ME on ME.ID=ca.IMESSURE'
      'where ca.ID=:IDCARD')
    Transaction = trSelRep
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    Left = 504
    Top = 136
    poAskRecordCount = True
    object quSelNamePosNAMECL: TFIBStringField
      FieldName = 'NAMECL'
      Size = 150
      EmptyStrToNull = True
    end
    object quSelNamePosPARENT: TFIBIntegerField
      FieldName = 'PARENT'
    end
    object quSelNamePosNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 200
      EmptyStrToNull = True
    end
    object quSelNamePosSM: TFIBStringField
      FieldName = 'SM'
      Size = 50
      EmptyStrToNull = True
    end
    object quSelNamePosKOEF: TFIBFloatField
      FieldName = 'KOEF'
    end
    object quSelNamePosIMESSURE: TFIBIntegerField
      FieldName = 'IMESSURE'
    end
  end
  object quSelOutSum: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT ARTICUL,SUM(QUANT*PRICEIN) as SUMOUT,SUM(QUANT) as QSUM'
      'FROM OF_PARTOUT'
      'where IDDATE>=:DATEB and IDDATE<:DATEE and IDSTORE=:IDSKL'
      'Group by ARTICUL')
    Transaction = trSelRep
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    Left = 432
    Top = 200
    object quSelOutSumARTICUL: TFIBIntegerField
      FieldName = 'ARTICUL'
    end
    object quSelOutSumSUMOUT: TFIBFloatField
      FieldName = 'SUMOUT'
    end
    object quSelOutSumQSUM: TFIBFloatField
      FieldName = 'QSUM'
    end
  end
end
