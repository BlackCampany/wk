unit ClosePer;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, FIBDataSet, pFIBDataSet, FIBDatabase, pFIBDatabase, ComCtrls,
  Menus, cxLookAndFeelPainters, cxControls, cxContainer, cxEdit,
  cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar, StdCtrls, cxButtons,
  ExtCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxDBData, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid;

type
  TfmClosePer = class(TForm)
    RnDb: TpFIBDatabase;
    trSelect: TpFIBTransaction;
    trUpdate: TpFIBTransaction;
    quPer: TpFIBDataSet;
    quPerID: TFIBIntegerField;
    quPerID_PARENT: TFIBIntegerField;
    quPerNAME: TFIBStringField;
    quPerUVOLNEN: TFIBBooleanField;
    quPerCheck: TFIBStringField;
    quPerMODUL1: TFIBBooleanField;
    quPerMODUL2: TFIBBooleanField;
    quPerMODUL3: TFIBBooleanField;
    quPerMODUL4: TFIBBooleanField;
    quPerMODUL5: TFIBBooleanField;
    quPerMODUL6: TFIBBooleanField;
    taPersonal: TpFIBDataSet;
    taPersonalID: TFIBIntegerField;
    taPersonalID_PARENT: TFIBIntegerField;
    taPersonalNAME: TFIBStringField;
    taPersonalUVOLNEN: TFIBBooleanField;
    taPersonalPASSW: TFIBStringField;
    taPersonalMODUL1: TFIBBooleanField;
    taPersonalMODUL2: TFIBBooleanField;
    taPersonalMODUL3: TFIBBooleanField;
    taPersonalMODUL4: TFIBBooleanField;
    taPersonalMODUL5: TFIBBooleanField;
    taPersonalMODUL6: TFIBBooleanField;
    taPersonalBARCODE: TFIBStringField;
    dsPer: TDataSource;
    dsPersonal: TDataSource;
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Label1: TLabel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxDateEdit1: TcxDateEdit;
    ViewClose: TcxGridDBTableView;
    LevelClose: TcxGridLevel;
    GridClose: TcxGrid;
    taCloseHist: TpFIBDataSet;
    dstaCloseHist: TDataSource;
    taCloseHistID: TFIBIntegerField;
    taCloseHistDATEDOC: TFIBDateField;
    taCloseHistIDP: TFIBIntegerField;
    taCloseHistVALEDIT: TFIBDateTimeField;
    taCloseHistPNAME: TFIBStringField;
    ViewCloseID: TcxGridDBColumn;
    ViewCloseDATEDOC: TcxGridDBColumn;
    ViewCloseIDP: TcxGridDBColumn;
    ViewCloseVALEDIT: TcxGridDBColumn;
    ViewClosePNAME: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmClosePer: TfmClosePer;

implementation

uses Un1, PerA_Office;

{$R *.dfm}

procedure TfmClosePer.FormCreate(Sender: TObject);
begin
  CurDir := ExtractFilePath(ParamStr(0));
  ReadIni;

  RnDb.Connected:=False;
  RnDb.DBName:=CommonSet.OfficeDb;
  try
    sErr:=CommonSet.OfficeDb;
    RnDb.Open;

    taPersonal.Active:=True;
  except
    sErr:='���� �� ����������� - '+CommonSet.OfficeDb;
  end;
end;

procedure TfmClosePer.FormShow(Sender: TObject);
begin
  fmPerA_Close:=TfmPerA_Close.Create(Application);
  fmPerA_Close.StatusBar1.Panels[0].Text:=sErr;
  fmPerA_Close.ShowModal;
  if fmPerA_Close.ModalResult=mrOk then
  begin
    Caption:=Caption+'   : '+Person.Name;
    WriteIni; //�������� �������� �������
  end
  else
  begin
    delay(100);
    close;
    delay(100);
  end;
  fmPerA_Close.Release;
  taCloseHist.Active:=False;
  taCloseHist.Active:=True;
  taCloseHist.First;

  cxDateEdit1.Date:=date;
end;

procedure TfmClosePer.cxButton2Click(Sender: TObject);
begin
  Close;
end;

procedure TfmClosePer.cxButton1Click(Sender: TObject);
Var iMax:INteger;
begin
  if MessageDlg('�� ������������� ������ ������� ��� ��������� ������ �� '+FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date)+' ������������ ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
    cxButton2.Enabled:=False;
    ViewClose.BeginUpdate;

    taCloseHist.First;
    iMax:=taCloseHistID.AsInteger+1;

    taCloseHist.Append;
    taCloseHistID.AsInteger:=iMax;
    taCloseHistDATEDOC.AsDateTime:=Trunc(cxDateEdit1.Date);
    taCloseHistIDP.AsInteger:=Person.Id;
    taCloseHistVALEDIT.AsDateTime:=Now;
    taCloseHist.Post;

    taCloseHist.FullRefresh;

    taCloseHist.First;

    ViewClose.EndUpdate;
    cxButton2.Enabled:=True;

    showmessage('������ �� '+FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date)+' ������������, ������ ��� ���������.');
  end;
end;

end.
