unit UnInOutM;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ActnList, XPStyleActnCtrls, ActnMan, StdCtrls,
  dxfLabel, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, DBClient, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxContainer, cxTextEdit, cxMaskEdit,
  cxDropDownEdit, cxCalc, cxDBEdit, dxfQuickTyp, cxCurrencyEdit,
  cxLookAndFeelPainters, cxButtons, Menus, cxDataStorage, cxSpinEdit,
  dxfBackGround;

type
  TfmInOutM = class(TForm)
    Panel1: TPanel;
    acInOut: TActionManager;
    acClose: TAction;
    Panel2: TPanel;
    Label3: TLabel;
    CurrencyEdit1: TcxCurrencyEdit;
    Button1: TcxButton;
    Button2: TcxButton;
    dxfBackGround1: TdxfBackGround;
    Label8: TLabel;
    Label9: TLabel;
    Panel3: TPanel;
    Label4: TLabel;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    cxButton5: TcxButton;
    cxButton6: TcxButton;
    cxButton7: TcxButton;
    cxButton8: TcxButton;
    cxButton9: TcxButton;
    cxButton10: TcxButton;
    cxButton11: TcxButton;
    cxButton12: TcxButton;
    cxButton13: TcxButton;
    cxButton14: TcxButton;
    cxButton15: TcxButton;
    CEdit4: TcxCalcEdit;
    procedure Button2Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure cxButton13Click(Sender: TObject);
    procedure cxButton14Click(Sender: TObject);
    procedure cxButton8Click(Sender: TObject);
    procedure cxButton9Click(Sender: TObject);
    procedure cxButton10Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton11Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
    procedure cxButton7Click(Sender: TObject);
    procedure cxButton12Click(Sender: TObject);
    procedure cxButton15Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmInOutM: TfmInOutM;
  iSumInOut:Int64;
  iLast:INteger;
  bFirst:Boolean = False;

implementation

uses Un1;

{$R *.dfm}

procedure TfmInOutM.Button2Click(Sender: TObject);
begin
  close;
end;

procedure TfmInOutM.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  bMoneyCalc:=False;
end;

procedure TfmInOutM.FormShow(Sender: TObject);
begin
  CurrencyEdit1.EditValue:=0;
  CEdit4.Value:=0;
  bFirst:=True;  
end;

procedure TfmInOutM.cxButton13Click(Sender: TObject);
begin
  CEdit4.Value:=0;
end;

procedure TfmInOutM.cxButton14Click(Sender: TObject);
begin
  cEdit4.EditValue:=Trunc(cEdit4.EditValue*100/10)/100;
end;

procedure TfmInOutM.cxButton8Click(Sender: TObject);
begin
  if bFirst then CEdit4.Value:=7
  else
    if pos(',',CEdit4.Text)=0 then CEdit4.EditValue:=CEdit4.EditValue*10+7
    else CEdit4.Text:=CEdit4.Text+'7';
  bFirst:=False;
end;

procedure TfmInOutM.cxButton9Click(Sender: TObject);
begin
  if bFirst then CEdit4.Value:=8
  else
    if pos(',',CEdit4.Text)=0 then CEdit4.EditValue:=CEdit4.EditValue*10+8
    else CEdit4.Text:=CEdit4.Text+'8';
  bFirst:=False;
end;

procedure TfmInOutM.cxButton10Click(Sender: TObject);
begin
  if bFirst then CEdit4.Value:=9
  else
    if pos(',',CEdit4.Text)=0 then CEdit4.EditValue:=CEdit4.EditValue*10+9
    else CEdit4.Text:=CEdit4.Text+'9';
  bFirst:=False;
end;

procedure TfmInOutM.cxButton2Click(Sender: TObject);
begin
  if bFirst then CEdit4.Value:=1
  else
    if pos(',',CEdit4.Text)=0 then CEdit4.EditValue:=CEdit4.EditValue*10+1
    else CEdit4.Text:=CEdit4.Text+'1';
  bFirst:=False;
end;

procedure TfmInOutM.cxButton11Click(Sender: TObject);
begin
  if bFirst then CEdit4.Value:=0
  else
    if pos(',',CEdit4.Text)=0 then CEdit4.EditValue:=CEdit4.EditValue*10
    else CEdit4.Text:=CEdit4.Text+'0';
  bFirst:=False;
end;

procedure TfmInOutM.cxButton3Click(Sender: TObject);
begin
  if bFirst then CEdit4.Value:=2
  else
    if pos(',',CEdit4.Text)=0 then CEdit4.EditValue:=CEdit4.EditValue*10+2
    else CEdit4.Text:=CEdit4.Text+'2';
  bFirst:=False;
end;

procedure TfmInOutM.cxButton4Click(Sender: TObject);
begin
  if bFirst then CEdit4.Value:=3
  else
    if pos(',',CEdit4.Text)=0 then CEdit4.EditValue:=CEdit4.EditValue*10+3
    else CEdit4.Text:=CEdit4.Text+'3';
  bFirst:=False;
end;

procedure TfmInOutM.cxButton5Click(Sender: TObject);
begin
  if bFirst then CEdit4.Value:=4
  else
    if pos(',',CEdit4.Text)=0 then CEdit4.EditValue:=CEdit4.EditValue*10+4
    else CEdit4.Text:=CEdit4.Text+'4';
  bFirst:=False;
end;

procedure TfmInOutM.cxButton6Click(Sender: TObject);
begin
  if bFirst then CEdit4.Value:=5
  else
    if pos(',',CEdit4.Text)=0 then CEdit4.EditValue:=CEdit4.EditValue*10+5
    else CEdit4.Text:=CEdit4.Text+'5';
  bFirst:=False;
end;

procedure TfmInOutM.cxButton7Click(Sender: TObject);
begin
  if bFirst then CEdit4.Value:=6
  else
    if pos(',',CEdit4.Text)=0 then CEdit4.EditValue:=CEdit4.EditValue*10+6
    else CEdit4.Text:=CEdit4.Text+'6';
  bFirst:=False;
end;

procedure TfmInOutM.cxButton12Click(Sender: TObject);
begin
  if pos(',',CEdit4.Text)=0 then CEdit4.Text:=CEdit4.Text+',';
  bFirst:=False;
end;

procedure TfmInOutM.cxButton15Click(Sender: TObject);
begin
  CurrencyEdit1.Value:=CEdit4.Value;
end;

end.
