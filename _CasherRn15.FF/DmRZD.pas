unit DmRZD;

interface

uses
  SysUtils, Classes, ImgList, Controls, cxStyles, DB, FIBDataSet,
  pFIBDataSet, FIBDatabase, pFIBDatabase, VaComm, VaSystem, VaClasses;

type
  TdmR = class(TDataModule)
    imState: TImageList;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    cxStyle14: TcxStyle;
    cxStyle15: TcxStyle;
    cxStyle16: TcxStyle;
    cxStyle17: TcxStyle;
    cxStyle18: TcxStyle;
    cxStyle19: TcxStyle;
    cxStyle20: TcxStyle;
    cxStyle21: TcxStyle;
    cxStyle22: TcxStyle;
    cxStyle23: TcxStyle;
    cxStyle24: TcxStyle;
    cxStyle25: TcxStyle;
    cxStyle26: TcxStyle;
    cxStyle27: TcxStyle;
    cxStyle28: TcxStyle;
    cxStyle29: TcxStyle;
    CasherRnDb: TpFIBDatabase;
    trSelect: TpFIBTransaction;
    trUpdate: TpFIBTransaction;
    trDel: TpFIBTransaction;
    quMenuHK: TpFIBDataSet;
    quMenuHKSIFR: TFIBIntegerField;
    quMenuHKNAME: TFIBStringField;
    quMenuHKPRICE: TFIBFloatField;
    quMenuHKCODE: TFIBStringField;
    quMenuHKTREETYPE: TFIBStringField;
    quMenuHKLIMITPRICE: TFIBFloatField;
    quMenuHKCATEG: TFIBSmallIntField;
    quMenuHKPARENT: TFIBSmallIntField;
    quMenuHKLINK: TFIBSmallIntField;
    quMenuHKSTREAM: TFIBSmallIntField;
    quMenuHKLACK: TFIBSmallIntField;
    quMenuHKDESIGNSIFR: TFIBSmallIntField;
    quMenuHKALTNAME: TFIBStringField;
    quMenuHKNALOG: TFIBFloatField;
    quMenuHKBARCODE: TFIBStringField;
    quMenuHKIMAGE: TFIBSmallIntField;
    quMenuHKCONSUMMA: TFIBFloatField;
    quMenuHKMINREST: TFIBSmallIntField;
    quMenuHKPRNREST: TFIBSmallIntField;
    quMenuHKCOOKTIME: TFIBSmallIntField;
    quMenuHKDISPENSER: TFIBSmallIntField;
    quMenuHKDISPKOEF: TFIBSmallIntField;
    quMenuHKACCESS: TFIBSmallIntField;
    quMenuHKFLAGS: TFIBSmallIntField;
    quMenuHKTARA: TFIBSmallIntField;
    quMenuHKCNTPRICE: TFIBSmallIntField;
    quMenuHKBACKBGR: TFIBFloatField;
    quMenuHKFONTBGR: TFIBFloatField;
    quMenuHKIACTIVE: TFIBSmallIntField;
    quMenuHKIEDIT: TFIBSmallIntField;
    quHotKey: TpFIBDataSet;
    quHotKeyCASHNUM: TFIBIntegerField;
    quHotKeyIROW: TFIBSmallIntField;
    quHotKeyICOL: TFIBSmallIntField;
    quHotKeySIFR: TFIBIntegerField;
    quHotKeyNAME: TFIBStringField;
    quHotKeyPRICE: TFIBFloatField;
    trSelHK: TpFIBTransaction;
    trUpdHK: TpFIBTransaction;
    quHotKey1: TpFIBDataSet;
    quHotKey1CASHNUM: TFIBIntegerField;
    quHotKey1IROW: TFIBSmallIntField;
    quHotKey1ICOL: TFIBSmallIntField;
    quHotKey1SIFR: TFIBIntegerField;
    quHotKey1NAME: TFIBStringField;
    quHotKey1PRICE: TFIBFloatField;
    quCanDo: TpFIBDataSet;
    quCanDoID_PERSONAL: TFIBIntegerField;
    quCanDoNAME: TFIBStringField;
    quCanDoPREXEC: TFIBBooleanField;
    quMenu: TpFIBDataSet;
    quMenuSIFR: TFIBIntegerField;
    quMenuNAME: TFIBStringField;
    quMenuPARENT: TFIBSmallIntField;
    quMenuCODE: TFIBStringField;
    quMenuPRICE: TFIBFloatField;
    quMenuTREETYPE: TFIBStringField;
    quMenuINFO: TStringField;
    quMenuSPRICE: TStringField;
    quMenuLINK: TFIBSmallIntField;
    quMenuLIMITPRICE: TFIBFloatField;
    quMenuSTREAM: TFIBSmallIntField;
    quMenuDESIGNSIFR: TFIBSmallIntField;
    dsMenu: TDataSource;
    trMenu: TpFIBTransaction;
    quMenuDRec: TpFIBDataSet;
    quMenuDRecISTATION: TFIBIntegerField;
    quMenuDRecTREETYPE: TFIBStringField;
    quMenuDRecPARENT: TFIBIntegerField;
    quMenuDRecSIFR: TFIBIntegerField;
    quMenuDRecNAME: TFIBStringField;
    quMenuDRecPRICE: TFIBFloatField;
    quMenuDRecCODE: TFIBStringField;
    quMenuDRecLINK: TFIBSmallIntField;
    quMenuDRecLIMITPRICE: TFIBFloatField;
    quMenuDRecSTREAM: TFIBSmallIntField;
    quMenuDRecDESIGNSIFR: TFIBSmallIntField;
    quMenuDRecPRI: TFIBIntegerField;
    quMenuId: TpFIBDataSet;
    quMenuIdSIFR: TFIBIntegerField;
    quMenuIdNAME: TFIBStringField;
    quMenuIdPRICE: TFIBFloatField;
    quMenuIdCODE: TFIBStringField;
    quMenuIdTREETYPE: TFIBStringField;
    quMenuIdLIMITPRICE: TFIBFloatField;
    quMenuIdCATEG: TFIBSmallIntField;
    quMenuIdPARENT: TFIBSmallIntField;
    quMenuIdLINK: TFIBSmallIntField;
    quMenuIdSTREAM: TFIBSmallIntField;
    quMenuIdLACK: TFIBSmallIntField;
    quMenuIdDESIGNSIFR: TFIBSmallIntField;
    quMenuIdALTNAME: TFIBStringField;
    quMenuIdNALOG: TFIBFloatField;
    quMenuIdBARCODE: TFIBStringField;
    quMenuIdIMAGE: TFIBSmallIntField;
    quMenuIdCONSUMMA: TFIBFloatField;
    quMenuIdMINREST: TFIBSmallIntField;
    quMenuIdPRNREST: TFIBSmallIntField;
    quMenuIdCOOKTIME: TFIBSmallIntField;
    quMenuIdDISPENSER: TFIBSmallIntField;
    quMenuIdDISPKOEF: TFIBSmallIntField;
    quMenuIdACCESS: TFIBSmallIntField;
    quMenuIdFLAGS: TFIBSmallIntField;
    quMenuIdTARA: TFIBSmallIntField;
    quMenuIdCNTPRICE: TFIBSmallIntField;
    quMenuIdBACKBGR: TFIBFloatField;
    quMenuIdFONTBGR: TFIBFloatField;
    quMenuIdIACTIVE: TFIBSmallIntField;
    quMenuIdIEDIT: TFIBSmallIntField;
    quMenuIdDATEB: TFIBIntegerField;
    quMenuIdDATEE: TFIBIntegerField;
    quMenuIdDAYWEEK: TFIBStringField;
    quMenuIdTIMEB: TFIBTimeField;
    quMenuIdTIMEE: TFIBTimeField;
    quMenuIdALLTIME: TFIBSmallIntField;
    cxStyle30: TcxStyle;
    quSpecP: TpFIBDataSet;
    trSelP: TpFIBTransaction;
    trUpdP: TpFIBTransaction;
    quSpecPID_TAB: TFIBIntegerField;
    quSpecPID_POS: TFIBIntegerField;
    quSpecPID: TFIBIntegerField;
    quSpecPSIFR: TFIBIntegerField;
    quSpecPNAME: TFIBStringField;
    quSpecPPRICE: TFIBFloatField;
    quSpecPQUANTITY: TFIBFloatField;
    quSpecPSUMMA: TFIBFloatField;
    quSpecPLIMITM: TFIBIntegerField;
    quSpecPLINKM: TFIBIntegerField;
    quSpecPITYPE: TFIBSmallIntField;
    quModif: TpFIBDataSet;
    quModifSIFR: TFIBIntegerField;
    quModifNAME: TFIBStringField;
    quModifPARENT: TFIBIntegerField;
    quModifPRICE: TFIBFloatField;
    quModifREALPRICE: TFIBFloatField;
    quModifIACTIVE: TFIBSmallIntField;
    quModifIEDIT: TFIBSmallIntField;
    dsModif: TDataSource;
    quTabP: TpFIBDataSet;
    quTabPID_TAB: TFIBIntegerField;
    quTabPSUMP: TFIBFloatField;
    dsquTabP: TDataSource;
    devCas: TVaComm;
    VaWaitMessageCas: TVaWaitMessage;
    quMenuDISPKOEF: TFIBSmallIntField;
    procedure quMenuCalcFields(DataSet: TDataSet);
    procedure devCasRxBuf(Sender: TObject; Data: PVaData; Count: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

Function CanDo(Name_F:String):Boolean;
procedure prFormMenu(iParent:INteger);

var
  dmR: TdmR;

implementation

uses Un1, MainRZD;

{$R *.dfm}

Function CanDo(Name_F:String):Boolean;
begin
  result:=True;
  with dmR do
  begin
    quCanDo.Active:=False;
    quCanDo.ParamByName('Person_id').Value:=Person.Id;
    quCanDo.ParamByName('Name_F').Value:=Name_F;
    quCanDo.Active:=True;
    if quCanDoprExec.AsBoolean=True then Result:=False;
  end;
end;


procedure prFormMenu(iParent:INteger);
Var  sW,sT,sD:String;
    iWeek:Integer;
begin
  with dmR do
  begin
    sW:=FormatDateTime('ddd',Date);

    iWeek:=1;
    if Pos('��',sW)>0 then iWeek:=1;
    if Pos('��',sW)>0 then iWeek:=2;
    if Pos('��',sW)>0 then iWeek:=3;
    if Pos('��',sW)>0 then iWeek:=4;
    if Pos('��',sW)>0 then iWeek:=5;
    if Pos('��',sW)>0 then iWeek:=6;
    if Pos('��',sW)>0 then iWeek:=7;

    if Pos('Mon',sW)>0 then iWeek:=1;
    if Pos('Tue',sW)>0 then iWeek:=2;
    if Pos('Wed',sW)>0 then iWeek:=3;
    if Pos('Thu',sW)>0 then iWeek:=4;
    if Pos('Fri',sW)>0 then iWeek:=5;
    if Pos('Sat',sW)>0 then iWeek:=6;
    if Pos('Sun',sW)>0 then iWeek:=7;

    sW:=its(iWeek);

    iWeek:=Trunc(date);
    sD:=its(iWeek);

    sT:=FormatDateTime('hh:nn',now);

    if CommonSet.UseDayMenu=0 then
    begin
      quMenu.Active:=False;
      quMenu.SelectSQL.Clear;
      quMenu.SelectSQL.Add('Select Sifr, Name, Parent, Code, Price, TreeType, Link, LimitPrice, Stream, Designsifr, DISPKOEF');
      quMenu.SelectSQL.Add('from menu');
      quMenu.SelectSQL.Add('where Parent='+its(iParent));
      quMenu.SelectSQL.Add('and IACTIVE=1');
      quMenu.SelectSQL.Add('and ((ALLTIME=1)');
      quMenu.SelectSQL.Add('or ((ALLTIME=0)and(DAYWEEK like ''%'+sW+'%'')and(DATEB<='+sD+')and(DATEE>='+sD+')and(TIMEB<='''+sT+''')and(TIMEE>='''+sT+''')))');
      quMenu.SelectSQL.Add('Order by TreeType desc,Name');

      quMenu.Active:=True;
    end;

    if CommonSet.UseDayMenu=1 then
    begin  //�������� � ������� ����
      quMenu.Active:=False;
      quMenu.SelectSQL.Clear;
      quMenu.SelectSQL.Add('Select Sifr, Name, Parent, Code, Price, TreeType, Link, LimitPrice, Stream, Designsifr, 0 as DISPKOEF');
      quMenu.SelectSQL.Add('from menud');
      quMenu.SelectSQL.Add('where Parent='+its(iParent));
      quMenu.SelectSQL.Add('and ISTATION='+its(CommonSet.Station));
      quMenu.SelectSQL.Add('Order by TreeType desc, PRI, Name');

      quMenu.Active:=True;
    end;
  end;
end;



procedure TdmR.quMenuCalcFields(DataSet: TDataSet);
Var StrWk,StrWk1:String;
begin
  Str(quMenuPRICE.AsFloat:8:2,StrWk);
  While Pos(' ',StrWk)>0 do delete(StrWk,Pos(' ',StrWk),1);
  StrWk1:=quMenuNAME.AsString;
  if quMenuTREETYPE.AsString='T' then
  begin
 //   while Length(StrWk1)<50 do StrWk1:=StrWk1+' ';
    quMenuINFO.AsString:=StrWk1;
    quMenuSPRICE.AsString:='';
  end else
  begin
//    StrWk1:=StrWk1+'  ('+StrWk+'�.)';
//    while Length(StrWk1)<50 do StrWk1:=StrWk1+' ';
    quMenuINFO.AsString:=StrWk1+'                                                  |'+quMenuSIFR.AsString;
    quMenuSPRICE.AsString:='���� - '+StrWk+'�.'+'                                                  |'+quMenuSIFR.AsString;
  end;
end;

procedure TdmR.devCasRxBuf(Sender: TObject; Data: PVaData; Count: Integer);
var P: Integer;
begin
  if Count >0 then
  begin
    for P:=0 to Count-1 do
    begin
      sScanEv:=sScanEv + char(Data[P]);
    end;
  end;
  iReadCount:=iReadCount+Count;
end;

end.
