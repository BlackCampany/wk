unit ViewXML;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  NativeXml,
  Dialogs, cxControls, cxContainer, cxEdit, cxTextEdit, cxMemo, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore;

type
  TfmShowXML = class(TForm)
    Memo1: TcxMemo;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acSaveToFileExecute(Sender: TObject);
    procedure acCloseExecute(Sender: TObject);
  private
    { Private declarations }
    FId:Integer;
    //FXml: TNativeXml;
    FStrXML:string;
    FCURDIR:string;
    procedure Init;
  public
    { Public declarations }
  end;

procedure ShowXMLView(ID:Integer;Xmlstr:String);
function GetXmlReadable(XmlText: String): String;

var
  fmShowXML: TfmShowXML;

implementation

{$R *.dfm}

function GetXmlReadable(XmlText: String): String;
var
  Xml: TNativeXml;
begin
  Xml:= TNativeXml.Create(nil);
  Xml.XmlFormat := xfReadable;
  Xml.ReadFromString(XmlText);
  Result:=Xml.WriteToString;
  Xml.Free;
end;

procedure ShowXMLView(ID:Integer;Xmlstr:String);
var F:TfmShowXML;
begin
  F:=TfmShowXML.Create(Application);
//  ShowMessage(Xmlstr);
  F.FStrXML:=Xmlstr;
  F.FiD:=ID;
  F.FCurDir:=ExtractFilePath(ParamStr(0));
  F.Init;
  F.Show;
end;

procedure TfmShowXML.acCloseExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmShowXML.acSaveToFileExecute(Sender: TObject);
begin
//  Memo1.Lines.SaveToFile(FCurDir+IntToStr(FiD)+'.xml',TEncoding.UTF8);
end;

procedure TfmShowXML.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
end;


procedure TfmShowXML.Init;
begin
  Caption:='XML '+IntToStr(FiD);

  Memo1.Clear;
//  Memo1.Text:=UTF8ToString(GetXmlReadable(FStrXML));
  Memo1.Text:=Utf8ToAnsi(GetXmlReadable(FStrXML));

//  Memo1.Text:=GetXmlReadable(FStrXML);
end;

end.
