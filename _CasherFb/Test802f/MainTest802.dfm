object Form1: TForm1
  Left = 587
  Top = 173
  Width = 905
  Height = 898
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TcxMemo
    Left = 0
    Top = 351
    Align = alClient
    Lines.Strings = (
      'Memo1')
    TabOrder = 0
    Height = 509
    Width = 889
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 889
    Height = 351
    Align = alTop
    BevelInner = bvLowered
    Color = 16309438
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object Label1: TLabel
      Left = 11
      Top = 16
      Width = 55
      Height = 13
      Caption = #1055#1086#1088#1090'  COM'
    end
    object cxSpinEdit1: TcxSpinEdit
      Left = 72
      Top = 13
      Properties.MaxValue = 25.000000000000000000
      Properties.MinValue = 1.000000000000000000
      TabOrder = 0
      Value = 3
      Width = 62
    end
    object cxButton1: TcxButton
      Left = 148
      Top = 11
      Width = 160
      Height = 25
      Caption = #1055#1088#1086#1074#1077#1088#1082#1072' '#1087#1086#1088#1090#1072
      TabOrder = 1
      OnClick = cxButton1Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 24
      Top = 56
      Width = 199
      Height = 25
      Caption = #1057#1090#1072#1090#1091#1089
      TabOrder = 2
      OnClick = cxButton2Click
    end
    object cxButton3: TcxButton
      Left = 22
      Top = 92
      Width = 199
      Height = 25
      Caption = #1054#1090#1082#1088#1099#1090#1100' '#1089#1084#1077#1085#1091
      TabOrder = 3
      OnClick = cxButton3Click
    end
    object cxButton4: TcxButton
      Left = 22
      Top = 128
      Width = 199
      Height = 25
      Caption = #1056#1077#1075#1080#1089#1090#1088#1072#1094#1080#1103' '#1095#1077#1082#1072
      TabOrder = 4
      OnClick = cxButton4Click
    end
    object cxButton5: TcxButton
      Left = 22
      Top = 164
      Width = 199
      Height = 25
      Caption = 'Z'
      TabOrder = 5
      OnClick = cxButton5Click
    end
    object GroupBox1: TGroupBox
      Left = 300
      Top = 66
      Width = 574
      Height = 260
      Caption = ' TCP '
      Color = clWhite
      ParentBackground = False
      ParentColor = False
      TabOrder = 6
      object cxButton6: TcxButton
        Left = 25
        Top = 29
        Width = 199
        Height = 25
        Caption = #1057#1090#1072#1090#1091#1089
        TabOrder = 0
        OnClick = cxButton6Click
      end
      object cxButton7: TcxButton
        Left = 25
        Top = 63
        Width = 199
        Height = 25
        Caption = #1055#1077#1095#1072#1090#1100' '#1085#1077#1092#1080#1089#1082#1072#1083#1100#1085#1099#1081' '#1076#1086#1082#1091#1084#1077#1085#1090
        TabOrder = 1
        OnClick = cxButton7Click
      end
      object cxButton8: TcxButton
        Left = 25
        Top = 100
        Width = 199
        Height = 25
        Caption = #1055#1077#1095#1072#1090#1100' X'
        TabOrder = 2
        OnClick = cxButton8Click
      end
      object cxButton9: TcxButton
        Left = 25
        Top = 136
        Width = 199
        Height = 25
        Caption = #1054#1090#1082#1088#1099#1090#1100' '#1089#1084#1077#1085#1091
        TabOrder = 3
        OnClick = cxButton9Click
      end
      object cxButton10: TcxButton
        Left = 23
        Top = 173
        Width = 199
        Height = 25
        Caption = #1047#1072#1082#1088#1099#1090#1080' '#1089#1084#1077#1085#1099' '#1060#1053
        TabOrder = 4
        OnClick = cxButton10Click
      end
      object cxButton11: TcxButton
        Left = 253
        Top = 28
        Width = 199
        Height = 25
        Caption = #1055#1088#1086#1076#1072#1078#1072
        TabOrder = 5
        OnClick = cxButton11Click
      end
      object cxButton12: TcxButton
        Left = 252
        Top = 64
        Width = 199
        Height = 25
        Caption = #1047#1072#1082#1088#1099#1090#1080' '#1089#1084#1077#1085#1099' Z'
        TabOrder = 6
        OnClick = cxButton12Click
      end
      object cxButton13: TcxButton
        Left = 252
        Top = 99
        Width = 199
        Height = 25
        Caption = #1055#1086#1083#1091#1095#1077#1085#1080#1077' '#1089#1095#1077#1090#1095#1080#1082#1086#1074' '#1050#1050#1058
        TabOrder = 7
        OnClick = cxButton13Click
      end
    end
  end
  object FisPrint: TComPort
    BaudRate = br115200
    Port = 'COM3'
    Parity.Bits = prNone
    StopBits = sbOneStopBit
    DataBits = dbEight
    Events = [evRxChar, evTxEmpty, evRxFlag, evRing, evBreak, evCTS, evDSR, evError, evRLSD, evRx80Full]
    Buffer.InputSize = 10000
    Buffer.OutputSize = 10000
    FlowControl.OutCTSFlow = False
    FlowControl.OutDSRFlow = False
    FlowControl.ControlDTR = dtrDisable
    FlowControl.ControlRTS = rtsDisable
    FlowControl.XonXoffOut = False
    FlowControl.XonXoffIn = False
    StoredProps = [spBasic]
    TriggersOnRxChar = True
    OnRxChar = FisPrintRxChar
    Left = 343
    Top = 11
  end
  object ClientSock: TClientSocket
    Active = False
    Address = '192.168.0.44'
    ClientType = ctNonBlocking
    Port = 0
    OnRead = ClientSockRead
    Left = 119
    Top = 270
  end
end
