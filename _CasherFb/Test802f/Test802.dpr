// JCL_DEBUG_EXPERT_GENERATEJDBG OFF
// JCL_DEBUG_EXPERT_INSERTJDBG OFF
// JCL_DEBUG_EXPERT_DELETEMAPFILE OFF
program Test802;

uses
  Forms,
  MainTest802 in 'MainTest802.pas' {Form1},
  ViewXML in 'ViewXML.pas' {fmShowXML},
  U2FDK in 'U2FDK.PAS';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TfmShowXML, fmShowXML);
  Application.Run;
end.
