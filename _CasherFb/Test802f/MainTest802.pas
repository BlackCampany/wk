unit MainTest802;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, CPort, StdCtrls, cxButtons,
  cxMaskEdit, cxSpinEdit, ExtCtrls, cxControls, cxContainer, cxEdit,
  cxTextEdit, cxMemo, cxGraphics, cxLookAndFeels, dxSkinsCore, NativeXml, Sockets,
  ScktComp, xmldom, XMLIntf, msxmldom, XMLDoc, HTTPApp;

type
  TForm1 = class(TForm)
    Memo1: TcxMemo;
    Panel1: TPanel;
    Label1: TLabel;
    cxSpinEdit1: TcxSpinEdit;
    cxButton1: TcxButton;
    FisPrint: TComPort;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    cxButton5: TcxButton;
    GroupBox1: TGroupBox;
    cxButton6: TcxButton;
    ClientSock: TClientSocket;
    cxButton7: TcxButton;
    cxButton8: TcxButton;
    cxButton9: TcxButton;
    cxButton10: TcxButton;
    cxButton11: TcxButton;
    cxButton12: TcxButton;
    cxButton13: TcxButton;
    procedure cxButton1Click(Sender: TObject);
    procedure FisPrintRxChar(Sender: TObject; Count: Integer);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
    procedure ClientSockRead(Sender: TObject; Socket: TCustomWinSocket);
    procedure cxButton7Click(Sender: TObject);
    procedure cxButton8Click(Sender: TObject);
    procedure cxButton9Click(Sender: TObject);
    procedure cxButton10Click(Sender: TObject);
    procedure cxButton11Click(Sender: TObject);
    procedure cxButton12Click(Sender: TObject);
    procedure cxButton13Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;


function fFisOpen(StrP:String;FisPrint:TComPort):Boolean;
procedure prClearBuf;
procedure Delay(MSecs: Longint);
function Its(iV:Integer):String;

var
  Form1: TForm1;

  NumPacket         :Integer;
  sBuffer:string;
  BufferCount:Integer;


implementation

uses
  ViewXML, U2FDK;

{$R *.dfm}

procedure prClearBuf;
begin
  //Fillchar(Buffer, Sizeof(Buffer), 0);
  sBuffer:='';
  BufferCount:=0;
end;


function fFisOpen(StrP:String;FisPrint:TComPort):Boolean;
begin
  if FisPrint.Connected then Result:=True
  else
  begin
    try
      FisPrint.Port:=StrP;
      FisPrint.Baudrate:=br115200;
      FisPrint.Stopbits:=sbOneStopBit;
      FisPrint.Databits:=dbEight;
      FisPrint.Open;

      Result:=True;
    except
      Result:=False;
    end;
  end;
end;

procedure TForm1.cxButton1Click(Sender: TObject);
Var sPort:string;
begin
  sPort:='COM'+IntToStr(cxSpinEdit1.Value);

  if fFisOpen(sPort,FisPrint) then
  begin
    Memo1.Lines.Add('���� ��');
  end else
  begin
    Memo1.Lines.Add('������ �����..');
  end;
end;

procedure TForm1.FisPrintRxChar(Sender: TObject; Count: Integer);
Var Str:string;
begin
  FisPrint.ReadStr(Str, Count);
  sBuffer:=sBuffer+Str;
  BufferCount:=BufferCount+Count;
end;

procedure TForm1.cxButton2Click(Sender: TObject);
var StrSend,sRes:string;
    iC,n:Integer;
    Xml: TNativeXml;
begin
  //������
  if FisPrint.Connected then
  begin
    prClearBuf;
    StrSend:='';
    StrSend:=StrSend+Trim('<?xml version="1.0" encoding="UTF-8"?>');
    StrSend:=StrSend+Trim('<ArmRequest>');
    StrSend:=StrSend+Trim('    <RequestBody>');
    StrSend:=StrSend+Trim('        <ProtocolLabel>OFDFNARMUKM</ProtocolLabel>');
    StrSend:=StrSend+Trim('        <ProtocolVersion>1</ProtocolVersion>');
    StrSend:=StrSend+Trim('        <RequestId>{151f698e-6e19-4a5c-a562-ddd2f8571d33}</RequestId>');
    StrSend:=StrSend+Trim('        <DateTime>2016-10-11 10:59:20</DateTime>');
    StrSend:=StrSend+Trim('        <Command>2</Command>');
    StrSend:=StrSend+Trim('    </RequestBody>');
    StrSend:=StrSend+Trim('    <RequestData><![CDATA[');
    StrSend:=StrSend+Trim('    <ArmShiftOpen>');
    StrSend:=StrSend+Trim('        <FiscalDocNumber>111</FiscalDocNumber>');
    StrSend:=StrSend+Trim('        <FiscalSign>111</FiscalSign>');
    StrSend:=StrSend+Trim('        <ShiftNumber>111</ShiftNumber>');
    StrSend:=StrSend+Trim('        <UserLoginName>Ivanov</UserLoginName>');
    StrSend:=StrSend+Trim('        <DateTime>2017-04-004 12:58:37</DateTime>');
    StrSend:=StrSend+Trim('    </ArmShiftOpen>');
    StrSend:=StrSend+Trim('    ]]>');
    StrSend:=StrSend+Trim('    </RequestData>');
    StrSend:=StrSend+Trim('</ArmRequest>');

    Memo1.Lines.Add('');
    Memo1.Lines.Add(StrSend);

    FisPrint.WriteStr(StrSend);
    delay(10);

    for n:=1 to 1000 do
    begin
      iC:=BufferCount;
      delay(50);
      if (iC>0) and (iC=BufferCount) then break; //�� 50 �� ������ �� ����������...
    end;
    sRes:=sBuffer;

    Memo1.Lines.Add(its(n)+'  '+sRes);
    Memo1.Lines.Add('');
    Memo1.Lines.Add(its(n)+'  '+Utf8ToAnsi(sRes));
    Memo1.Lines.Add('');
    Memo1.Lines.Add(its(n)+'  '+OemToAnsiConvert(sRes));

    Memo1.Lines.Add('');

    Xml:= TNativeXml.Create(nil);
    Xml.XmlFormat := xfReadable;
    Xml.ReadFromString(sRes);
    Xml.SaveToFile('E:\_CasherFb\Test802f\BIN\'+'Ret.xml');
    Xml.Free;

//    ShowXMLView(1,sRes);
  end;
end;

procedure Delay(MSecs: Longint);
var
  FirstTickCount, Now: Longint;
begin
  FirstTickCount := GetTickCount;
  repeat
    Application.ProcessMessages;
    { allowing access to other controls, etc. }
    Now := GetTickCount;
  until (Now - FirstTickCount >= MSecs) or (Now < FirstTickCount);
end;

function Its(iV:Integer):String;
begin
  Result:=IntToStr(iV);
end;


procedure TForm1.cxButton3Click(Sender: TObject);
var StrSend,sRes:string;
    iC,n:Integer;
begin
  //������
  if FisPrint.Connected then
  begin
    prClearBuf;
    StrSend:='';
    StrSend:=StrSend+Trim('<?xml version="1.0" encoding="UTF-8"?>');
    StrSend:=StrSend+Trim('<ArmRequest>');
    StrSend:=StrSend+Trim('    <RequestBody>');
    StrSend:=StrSend+Trim('        <ProtocolLabel>OFDFNARMUKM</ProtocolLabel>');
    StrSend:=StrSend+Trim('        <ProtocolVersion>3</ProtocolVersion>');
    StrSend:=StrSend+Trim('        <RequestId>{b4be396b-3a93-4449-bbe6-8142c5d27b46}</RequestId>');
    StrSend:=StrSend+Trim('        <DateTime>2016-11-08 12:58:37</DateTime>');
    StrSend:=StrSend+Trim('        <Command>4</Command>');
    StrSend:=StrSend+Trim('    </RequestBody>');
    StrSend:=StrSend+Trim('    <RequestData>');
    StrSend:=StrSend+Trim('        <![CDATA[<ArmShiftOpen>');
    StrSend:=StrSend+Trim('        <FiscalDocNumber></FiscalDocNumber>');
    StrSend:=StrSend+Trim('        <FiscalSign></FiscalSign>');
    StrSend:=StrSend+Trim('        <ShiftNumber></ShiftNumber>');
    StrSend:=StrSend+Trim('        <UserLoginName></UserLoginName>');
    StrSend:=StrSend+Trim('        <DateTime>2017-04-04 12:58:37</DateTime>');
    StrSend:=StrSend+Trim('    </ArmShiftOpen>]]>');
    StrSend:=StrSend+Trim('</RequestData>');
    StrSend:=StrSend+Trim('</ArmRequest>');

    Memo1.Lines.Add(StrSend);

    FisPrint.WriteStr(StrSend);
    delay(10);

    for n:=1 to 1000 do
    begin
      iC:=BufferCount;
      delay(50);
      if (iC>0) and (iC=BufferCount) then break; //�� 50 �� ������ �� ����������...
    end;
    sRes:=sBuffer;

    Memo1.Lines.Add(its(n)+'  '+sRes);

    Memo1.Lines.Add('');

    ShowXMLView(1,sRes);
  end;
end;

procedure TForm1.cxButton4Click(Sender: TObject);
var StrSend,sRes:string;
    iC,n:Integer;
begin
  //������
  if FisPrint.Connected then
  begin
    prClearBuf;
    StrSend:='';
    StrSend:=StrSend+Trim('<?xml version="1.0" encoding="UTF-8"?>');
    StrSend:=StrSend+Trim('<ArmRequest>');
    StrSend:=StrSend+Trim('    <RequestBody>');
    StrSend:=StrSend+Trim('        <ProtocolLabel>OFDFNARMUKM</ProtocolLabel>');
    StrSend:=StrSend+Trim('        <ProtocolVersion>3</ProtocolVersion>');
    StrSend:=StrSend+Trim('        <RequestId>{079ef3c1-ac32-4424-900c-2c09a7ec89c0}</RequestId>');
    StrSend:=StrSend+Trim('        <DateTime>2016-11-10 16:13:19</DateTime>');
    StrSend:=StrSend+Trim('        <Command>8</Command>');
    StrSend:=StrSend+Trim('    </RequestBody>');
    StrSend:=StrSend+Trim('    <RequestData>');
    StrSend:=StrSend+Trim('        <![CDATA[<ArmReceipt>');
    StrSend:=StrSend+Trim('        <UserLoginName>Ivanov</UserLoginName>');
    StrSend:=StrSend+Trim('        <ReceiptType>1</ReceiptType>');
    StrSend:=StrSend+Trim('        <DateTime>2016-11-10 16:13:19</DateTime>');
    StrSend:=StrSend+Trim('        <Items>');
    StrSend:=StrSend+Trim('            <ArmReceiptItem>');
    StrSend:=StrSend+Trim('                <ItemArticle>10984</ItemArticle>');
    StrSend:=StrSend+Trim('                <ItemName>Good 10984</ItemName>');
    StrSend:=StrSend+Trim('                <Price>10.00</Price>');
    StrSend:=StrSend+Trim('                <Quantity>0.1</Quantity>');
    StrSend:=StrSend+Trim('                <ItemTotal>0.00</ItemTotal>');
    StrSend:=StrSend+Trim('                <TaxType>4</TaxType>');
    StrSend:=StrSend+Trim('                <Tax>0.00</Tax>');
    StrSend:=StrSend+Trim('                <ItemDiscounts>');
    StrSend:=StrSend+Trim('                    <ArmDiscount>');
    StrSend:=StrSend+Trim('                        <Name>*MALUS*</Name>');
    StrSend:=StrSend+Trim('                        <Value>0.00</Value>');
    StrSend:=StrSend+Trim('                    </ArmDiscount>');
    StrSend:=StrSend+Trim('                </ItemDiscounts>');
    StrSend:=StrSend+Trim('            </ArmReceiptItem>');
    StrSend:=StrSend+Trim('        </Items>');
    StrSend:=StrSend+Trim('        <CustomerAddress>');
    StrSend:=StrSend+Trim('        </CustomerAddress>');
    StrSend:=StrSend+Trim('        <Total>0.00</Total>');
    StrSend:=StrSend+Trim('        <CashSumm>1.00</CashSumm>');
    StrSend:=StrSend+Trim('        <NonCashSumm>0.00</NonCashSumm>');
    StrSend:=StrSend+Trim('        <PosNum>1</PosNum>');
    StrSend:=StrSend+Trim('        <PosShiftNum>1</PosShiftNum>');
    StrSend:=StrSend+Trim('        <PosReceiptNum>1</PosReceiptNum>');
    StrSend:=StrSend+Trim('        <ArmReceiptTax>');
    StrSend:=StrSend+Trim('            <nds18>0.00</nds18>');
    StrSend:=StrSend+Trim('            <nds10>0.00</nds10>');
    StrSend:=StrSend+Trim('            <nds00>0.00</nds00>');
    StrSend:=StrSend+Trim('            <ndsNo>0.00</ndsNo>');
    StrSend:=StrSend+Trim('            <nds18_118>0.00</nds18_118>');
    StrSend:=StrSend+Trim('            <nds10_110>0.00</nds10_110>');
    StrSend:=StrSend+Trim('        </ArmReceiptTax>');
    StrSend:=StrSend+Trim('        <FiscalDocNumber>1</FiscalDocNumber>');
    StrSend:=StrSend+Trim('        <FiscalSign>1</FiscalSign>');
    StrSend:=StrSend+Trim('        <ShiftNumber>1</ShiftNumber>');
    StrSend:=StrSend+Trim('        <ReceiptNumber>1</ReceiptNumber>');
    StrSend:=StrSend+Trim('    </ArmReceipt>]]>');
    StrSend:=StrSend+Trim('</RequestData>');
    StrSend:=StrSend+Trim('</ArmRequest>');

    Memo1.Lines.Add(StrSend);

    FisPrint.WriteStr(StrSend);
    delay(10);

    for n:=1 to 1000 do
    begin
      iC:=BufferCount;
      delay(50);
      if (iC>0) and (iC=BufferCount) then break; //�� 50 �� ������ �� ����������...
    end;
    sRes:=sBuffer;

    Memo1.Lines.Add('');
    Memo1.Lines.Add(its(n));
    Memo1.Lines.Add('');

    ShowXMLView(1,sRes);

  end;
end;

procedure TForm1.cxButton5Click(Sender: TObject);
var StrSend,sRes:string;
    iC,n:Integer;
begin
  //������
  if FisPrint.Connected then
  begin
    prClearBuf;
    StrSend:='';
    StrSend:=StrSend+Trim('<?xml version="1.0" encoding="UTF-8"?>');
    StrSend:=StrSend+Trim('<ArmResponse>');
    StrSend:=StrSend+Trim('    <ResponseBody>');
    StrSend:=StrSend+Trim('        <ProtocolLabel>OFDFNARMUKM</ProtocolLabel>');
    StrSend:=StrSend+Trim('        <ProtocolVersion>3</ProtocolVersion>');
    StrSend:=StrSend+Trim('        <RequestId>16</RequestId>');
    StrSend:=StrSend+Trim('        <Result>0</Result>');
    StrSend:=StrSend+Trim('        <ErrorCategory>0</ErrorCategory>');
    StrSend:=StrSend+Trim('        <ErrorSource>PRN_CONTROLLER</ErrorSource>');
    StrSend:=StrSend+Trim('        <ErrorCode>0</ErrorCode>');
    StrSend:=StrSend+Trim('        <ErrorDescription/>');
    StrSend:=StrSend+Trim('        <Command>11</Command>');
    StrSend:=StrSend+Trim('    </ResponseBody>');
    StrSend:=StrSend+Trim('    <ResponseData>');
    StrSend:=StrSend+Trim('        <![CDATA[<ArmCorrection>');
    StrSend:=StrSend+Trim('        <UserLoginName>Ivanov</UserLoginName>');
    StrSend:=StrSend+Trim('        <DateTime>2017-04-04 13:00:02</DateTime>');
    StrSend:=StrSend+Trim('        <PaymentType>1</PaymentType>');
    StrSend:=StrSend+Trim('        <Total>1</Total>');
    StrSend:=StrSend+Trim('        <CashSumm>1</CashSumm>');
    StrSend:=StrSend+Trim('        <NonCashSumm>5</NonCashSumm>');
    StrSend:=StrSend+Trim('        <FiscalDocNumber>5</FiscalDocNumber>');
    StrSend:=StrSend+Trim('        <FiscalSign>360775320</FiscalSign>');
    StrSend:=StrSend+Trim('        <ShiftNumber>1</ShiftNumber>');
    StrSend:=StrSend+Trim('        <ReceiptNumber>3</ReceiptNumber>');
    StrSend:=StrSend+Trim('    </ArmCorrection>]]>');
    StrSend:=StrSend+Trim('</ResponseData>');
    StrSend:=StrSend+Trim('</ArmResponse>');

    Memo1.Lines.Add(StrSend);

    FisPrint.WriteStr(StrSend);
    delay(10);

    for n:=1 to 1000 do
    begin
      iC:=BufferCount;
      delay(50);
      if (iC>0) and (iC=BufferCount) then break; //�� 50 �� ������ �� ����������...
    end;
    sRes:=sBuffer;

    Memo1.Lines.Add(its(n)+'  '+sRes);

    Memo1.Lines.Add('');

    ShowXMLView(1,sRes);
  end;
end;

procedure TForm1.cxButton6Click(Sender: TObject);
var StrSend:string;
    StrSendU: UTF8String;
    sRes:string;
    n:Integer;
begin
//
  prClearBuf;

  StrSend:='';
  StrSend:=StrSend+Trim('<?xml version="1.0" encoding="UTF-8"?>');
  StrSend:=StrSend+Trim('<ArmRequest>');
  StrSend:=StrSend+Trim('    <RequestBody>');
  StrSend:=StrSend+Trim('        <ProtocolLabel>OFDFNARMUKM</ProtocolLabel>');
  StrSend:=StrSend+Trim('        <ProtocolVersion>1</ProtocolVersion>');
  StrSend:=StrSend+Trim('        <RequestId>{151f698e-6e19-4a5c-a562-ddd2f8571d33}</RequestId>');
  StrSend:=StrSend+Trim('        <DateTime>2017-04-11 10:59:20</DateTime>');
  StrSend:=StrSend+Trim('        <Command>2</Command>');
  StrSend:=StrSend+Trim('    </RequestBody>');
  StrSend:=StrSend+Trim('    <RequestData><![CDATA[]]>');
  StrSend:=StrSend+Trim('    </RequestData>');
  StrSend:=StrSend+Trim('</ArmRequest>');
  Memo1.Lines.Add('');
  Memo1.Lines.Add(StrSend);

  StrSendU:=AnsiToUtf8(StrSend);

  if ClientSock.Active=False then
  begin
    ClientSock.Host:='';
    ClientSock.Address:='192.168.0.100';
    ClientSock.Port:=6667;
    ClientSock.Open;
    Delay(100);
  end;

  if ClientSock.Active then
  begin
    ClientSock.Socket.SendText(StrSend);

    for n:=1 to 100 do
    begin
      delay(50);
      if Length(sBuffer)>0 then break; //�� 50 �� ������ �� ����������...
    end;
    sRes:=sBuffer;

    Memo1.Lines.Add(its(n)+'  '+sRes);
    Memo1.Lines.Add('');

    ShowXMLView(1,sRes);
  end;
end;

procedure TForm1.ClientSockRead(Sender: TObject; Socket: TCustomWinSocket);
begin
//  sBuffer:=Utf8ToAnsi(Socket.ReceiveText);
  sBuffer:=Socket.ReceiveText;
end;

procedure TForm1.cxButton7Click(Sender: TObject);
var StrSend:string;
    StrSendU: UTF8String;
    sRes:string;
    n:Integer;
    StrPrint:string;
begin
  //������ ������������� ���������
  prClearBuf;

  StrPrint:='*********************************'+#$0D;
  StrPrint:=StrPrint+'*  ���� ������ ������ *'+#$0D;
  StrPrint:=StrPrint+'*  ���� ������ ������ *'+#$0D;
  StrPrint:=StrPrint+'*  ���� ������ ������ *'+#$0D;
  StrPrint:=StrPrint+'*  ���� ������ ������ *'+#$0D;
  StrPrint:=StrPrint+'*  ���� ������ ������ *'+#$0D;
  StrPrint:=StrPrint+'*  ���� ������ ������ *'+#$0D;
  StrPrint:=StrPrint+'*  ���� ������ ������ *'+#$0D;
  StrPrint:=StrPrint+'*  ���� ������ ������ *'+#$0D;
  StrPrint:=StrPrint+'*  ���� ������ ������ *'+#$0D;
  StrPrint:=StrPrint+'*  ���� ������ ������ *'+#$0D;
  StrPrint:=StrPrint+'*  ���� ������ ������ *'+#$0D;
  StrPrint:=StrPrint+'*  ���� ������ ������ *'+#$0D;
  StrPrint:=StrPrint+'*  ���� ������ ������ *'+#$0D;
  StrPrint:=StrPrint+'*  ���� ������ ������ *'+#$0D;
  StrPrint:=StrPrint+'*  ���� ������ ������ *'+#$0D;
  StrPrint:=StrPrint+'*  ���� ������ ������ *'+#$0D;
  StrPrint:=StrPrint+'*  ���� ������ ������ *'+#$0D;
  StrPrint:=StrPrint+'*  ���� ������ ������ *'+#$0D;
  StrPrint:=StrPrint+'*  ���� ������ ������ *'+#$0D;
  StrPrint:=StrPrint+'*  ���� ������ ������ *'+#$0D;
  StrPrint:=StrPrint+'*  ���� ������ ������ *'+#$0D;
  StrPrint:=StrPrint+'*  ���� ������ ������ *'+#$0D;
  StrPrint:=StrPrint+'*  ���� ������ ������ *'+#$0D;
  StrPrint:=StrPrint+'*  ���� ������ ������ *'+#$0D;
  StrPrint:=StrPrint+'*  ���� ������ ������ *'+#$0D;
  StrPrint:=StrPrint+'*  ���� ������ ������ *'+#$0D;
  StrPrint:=StrPrint+'*  ���� ������ ������ *'+#$0D;
  StrPrint:=StrPrint+'*  ���� ������ ������ *'+#$0D;
  StrPrint:=StrPrint+'*  ���� ������ ������ *'+#$0D;
  StrPrint:=StrPrint+'*  ���� ������ ������ *'+#$0D;
  StrPrint:=StrPrint+'*  ���� ������ ������ *'+#$0D;
  StrPrint:=StrPrint+'*  ���� ������ ������ *'+#$0D;
  StrPrint:=StrPrint+'*********************************'+#$0D+#$0A+#$0D+#$0D+'.';

  StrSend:='';
  StrSend:=StrSend+Trim('<?xml version="1.0" encoding="UTF-8"?>');
  StrSend:=StrSend+Trim('<ArmRequest>');
  StrSend:=StrSend+Trim('    <RequestBody>');
  StrSend:=StrSend+Trim('        <ProtocolLabel>OFDFNARMUKM</ProtocolLabel>');
  StrSend:=StrSend+Trim('        <ProtocolVersion>10.1</ProtocolVersion>');
  StrSend:=StrSend+Trim('        <RequestId>{151f698e-6e19-4a5c-a562-ddd2f8571d33}</RequestId>');
  StrSend:=StrSend+Trim('        <DateTime>2017-04-11 10:59:20</DateTime>');
  StrSend:=StrSend+Trim('        <Command>30</Command>');
  StrSend:=StrSend+Trim('    </RequestBody>');
  StrSend:=StrSend+Trim('    <RequestData><![CDATA[');
  StrSend:=StrSend+Trim('    <ArmNonFiscalDoc>');
  StrSend:=StrSend+Trim('        <NonFiscalText>'+StrPrint);
  StrSend:=StrSend+Trim('        </NonFiscalText>');
  StrSend:=StrSend+Trim('    </ArmNonFiscalDoc>');
  StrSend:=StrSend+Trim(']]>');
  StrSend:=StrSend+Trim('    </RequestData>');
  StrSend:=StrSend+Trim('</ArmRequest>');
  Memo1.Lines.Add('');
  Memo1.Lines.Add(StrSend);

  StrSendU:=AnsiToUtf8(StrSend);

  if ClientSock.Active=False then
  begin
    ClientSock.Host:='';
    ClientSock.Address:='192.168.0.100';
    ClientSock.Port:=6667;
    ClientSock.Open;
    Delay(100);
  end;

  if ClientSock.Active then
  begin
    ClientSock.Socket.SendText(StrSendU);

    for n:=1 to 100 do
    begin
      delay(50);
      if Length(sBuffer)>0 then break; //�� 50 �� ������ �� ����������...
    end;
    sRes:=Utf8ToAnsi(sBuffer);

    Memo1.Lines.Add(its(n)+'  '+sRes);
    Memo1.Lines.Add('');

    ShowXMLView(1,sBuffer);
  end;
end;

procedure TForm1.cxButton8Click(Sender: TObject);
var StrSend:string;
    StrSendU: UTF8String;
    sRes:string;
    n:Integer;
begin
  prClearBuf;
  StrSend:='';
  StrSend:=StrSend+Trim('<?xml version="1.0" encoding="UTF-8"?>');
  StrSend:=StrSend+Trim('<ArmRequest>');
  StrSend:=StrSend+Trim('    <RequestBody>');
  StrSend:=StrSend+Trim('        <ProtocolLabel>OFDFNARMUKM</ProtocolLabel>');
  StrSend:=StrSend+Trim('        <ProtocolVersion>10.1</ProtocolVersion>');
  StrSend:=StrSend+Trim('        <RequestId>{151f698e-6e19-4a5c-a562-ddd2f8571d33}</RequestId>');
  StrSend:=StrSend+Trim('        <DateTime>2017-04-11 10:59:20</DateTime>');
  StrSend:=StrSend+Trim('        <Command>46</Command>');
  StrSend:=StrSend+Trim('    </RequestBody>');
  StrSend:=StrSend+Trim('    <RequestData><![CDATA[');
  StrSend:=StrSend+Trim('        <ArmCXReport>');
  StrSend:=StrSend+Trim('        <UserLoginName>������ �.�.</UserLoginName>');
  StrSend:=StrSend+Trim('        <DateTime>2017-04-04 13:00:02</DateTime>');
  StrSend:=StrSend+Trim('        <HeaderReport>');
  StrSend:=StrSend+Trim('            ����� � 1 ');
  StrSend:=StrSend+Trim('            ����� � 5 ');
  StrSend:=StrSend+Trim('        </HeaderReport>');
  StrSend:=StrSend+Trim('        <FooterReport></FooterReport>');
  StrSend:=StrSend+Trim('    </ArmCXReport>]]>');
  StrSend:=StrSend+Trim('</RequestData>');
  StrSend:=StrSend+Trim('</ArmRequest>');

  Memo1.Lines.Add(StrSend);

  StrSendU:=AnsiToUtf8(StrSend);

  if ClientSock.Active=False then
  begin
    ClientSock.Host:='';
    ClientSock.Address:='192.168.0.100';
    ClientSock.Port:=6667;
    ClientSock.Open;
    Delay(100);
  end;

  if ClientSock.Active then
  begin
    ClientSock.Socket.SendText(StrSendU);

    for n:=1 to 100 do
    begin
      delay(50);
      if Length(sBuffer)>0 then break; //�� 50 �� ������ �� ����������...
    end;
    sRes:=Utf8ToAnsi(sBuffer);

    Memo1.Lines.Add(its(n)+'  '+sRes);
    Memo1.Lines.Add('');

    ShowXMLView(1,sBuffer);
  end;

end;

procedure TForm1.cxButton9Click(Sender: TObject);
var StrSend:string;
    StrSendU: UTF8String;
    sRes:string;
    n:Integer;
begin
  prClearBuf;
  StrSend:='';
  StrSend:=StrSend+Trim('<?xml version="1.0" encoding="UTF-8"?>');
  StrSend:=StrSend+Trim('<ArmRequest>');
  StrSend:=StrSend+Trim('    <RequestBody>');
  StrSend:=StrSend+Trim('        <ProtocolLabel>OFDFNARMUKM</ProtocolLabel>');
  StrSend:=StrSend+Trim('        <ProtocolVersion>10.1</ProtocolVersion>');
  StrSend:=StrSend+Trim('        <RequestId>{151f698e-6e19-4a5c-a562-ddd2f8571d33}</RequestId>');
  StrSend:=StrSend+Trim('        <DateTime>2017-04-11 10:59:20</DateTime>');
  StrSend:=StrSend+Trim('        <Command>4</Command>');
  StrSend:=StrSend+Trim('    </RequestBody>');
  StrSend:=StrSend+Trim('    <RequestData><![CDATA[');
  StrSend:=StrSend+Trim('        <ArmShiftOpen>');
  StrSend:=StrSend+Trim('        <FiscalDocNumber>100011</FiscalDocNumber>');
  StrSend:=StrSend+Trim('        <FiscalSign></FiscalSign>');
  StrSend:=StrSend+Trim('        <ShiftNumber>5</ShiftNumber>');
  StrSend:=StrSend+Trim('        <UserLoginName>������ �.�.</UserLoginName>');
  StrSend:=StrSend+Trim('        <DateTime>2017-04-04 12:58:37</DateTime>');
  StrSend:=StrSend+Trim('    </ArmShiftOpen>]]>');
  StrSend:=StrSend+Trim('</RequestData>');
  StrSend:=StrSend+Trim('</ArmRequest>');

  Memo1.Lines.Add(StrSend);

  StrSendU:=AnsiToUtf8(StrSend);

  if ClientSock.Active=False then
  begin
    ClientSock.Host:='';
    ClientSock.Address:='192.168.0.100';
    ClientSock.Port:=6667;
    ClientSock.Open;
    Delay(100);
  end;

  if ClientSock.Active then
  begin
    ClientSock.Socket.SendText(StrSendU);

    for n:=1 to 100 do
    begin
      delay(50);
      if Length(sBuffer)>0 then break; //�� 50 �� ������ �� ����������...
    end;
    sRes:=Utf8ToAnsi(sBuffer);

//    while Pos(#$0A,sRes)>0 do Delete(sRes,Pos(#$0A,sRes),1);

    Memo1.Lines.Add(its(n)+'  '+sRes);
    Memo1.Lines.Add('');

    ShowXMLView(1,sBuffer);
  end;
end;

procedure TForm1.cxButton10Click(Sender: TObject);
var StrSend:string;
    StrSendU: UTF8String;
    sRes:string;
    n:Integer;
begin
  prClearBuf;
  StrSend:='';
  StrSend:=StrSend+Trim('<?xml version="1.0" encoding="UTF-8"?>');
  StrSend:=StrSend+Trim('<ArmRequest>');
  StrSend:=StrSend+Trim('    <RequestBody>');
  StrSend:=StrSend+Trim('        <ProtocolLabel>OFDFNARMUKM</ProtocolLabel> ');
  StrSend:=StrSend+Trim('        <ProtocolVersion>3</ProtocolVersion> ');
  StrSend:=StrSend+Trim('        <RequestId>{b97ece8e-1f65-49f2-bca8-da7cac1743b7}</RequestId> ');
  StrSend:=StrSend+Trim('        <DateTime>2016-11-08 13:00:09</DateTime> ');
  StrSend:=StrSend+Trim('        <Command>6</Command> ');
  StrSend:=StrSend+Trim('    </RequestBody>');
  StrSend:=StrSend+Trim('    <RequestData> ');
  StrSend:=StrSend+Trim('        <![CDATA[<ArmShiftClose> ');
  StrSend:=StrSend+Trim('        <FiscalDocNumber>0</FiscalDocNumber> ');
  StrSend:=StrSend+Trim('        <FiscalSign>0</FiscalSign> ');
  StrSend:=StrSend+Trim('        <ShiftNumber>5</ShiftNumber> ');
  StrSend:=StrSend+Trim('        <OFDQueueLegth>0</OFDQueueLegth>');
  StrSend:=StrSend+Trim('        <OFDfirstQueueDocDateTime> ');
  StrSend:=StrSend+Trim('        </OFDfirstQueueDocDateTime> ');
  StrSend:=StrSend+Trim('        <ReceiptsCount>0</ReceiptsCount> ');
  StrSend:=StrSend+Trim('        <FiscalDocCount>0</FiscalDocCount>');
  StrSend:=StrSend+Trim('        <OFDLongWait>0</OFDLongWait> ');
  StrSend:=StrSend+Trim('        <FNNeedChange>1</FNNeedChange> ');
  StrSend:=StrSend+Trim('        <FNOwerflow>1</FNOwerflow>');
  StrSend:=StrSend+Trim('        <FNExhausted>1</FNExhausted> ');
  StrSend:=StrSend+Trim('        <UserLoginName>������ �.�.</UserLoginName>');
  StrSend:=StrSend+Trim('        <DateTime>2017-04-04 12:58:37</DateTime> ');
  StrSend:=StrSend+Trim('    </ArmShiftClose>]]> ');
  StrSend:=StrSend+Trim('</RequestData> ');
  StrSend:=StrSend+Trim('</ArmRequest>');

  Memo1.Lines.Add(StrSend);

  StrSendU:=AnsiToUtf8(StrSend);

  if ClientSock.Active=False then
  begin
    ClientSock.Host:='';
    ClientSock.Address:='192.168.0.100';
    ClientSock.Port:=6667;
    ClientSock.Open;
    Delay(100);
  end;

  if ClientSock.Active then
  begin
    ClientSock.Socket.SendText(StrSendU);

    for n:=1 to 100 do
    begin
      delay(50);
      if Length(sBuffer)>0 then break; //�� 50 �� ������ �� ����������...
    end;
    sRes:=Utf8ToAnsi(sBuffer);

//    while Pos(#$0A,sRes)>0 do Delete(sRes,Pos(#$0A,sRes),1);

    Memo1.Lines.Add(its(n)+'  '+sRes);
    Memo1.Lines.Add('');

    ShowXMLView(1,sBuffer);
  end;
end;

procedure TForm1.cxButton11Click(Sender: TObject);
var StrSend:string;
    StrSendU: UTF8String;
    sRes:string;
    n:Integer;
begin
 //�������
  prClearBuf;
  StrSend:='';
  StrSend:=StrSend+Trim('<?xml version="1.0" encoding="UTF-8"?>');
  StrSend:=StrSend+Trim('<ArmRequest> ');
  StrSend:=StrSend+Trim('    <RequestBody>');
  StrSend:=StrSend+Trim('        <ProtocolLabel>OFDFNARMUKM</ProtocolLabel>');
  StrSend:=StrSend+Trim('        <ProtocolVersion>10.1</ProtocolVersion>   ');
  StrSend:=StrSend+Trim('        <RequestId>{079ef3c1-ac32-4424-900c-2c09a7ec89c0}</RequestId>  ');
  StrSend:=StrSend+Trim('        <DateTime>2017-04-04 12:58:37</DateTime>');
  StrSend:=StrSend+Trim('        <Command>8</Command>                    ');
  StrSend:=StrSend+Trim('    </RequestBody>                              ');
  StrSend:=StrSend+Trim('    <RequestData>                              ');
  StrSend:=StrSend+Trim('        <![CDATA[<ArmReceipt>                   ');
  StrSend:=StrSend+Trim('        <UserLoginName>������� �.�.</UserLoginName>   ');
  StrSend:=StrSend+Trim('        <ReceiptType>1</ReceiptType>            ');
  StrSend:=StrSend+Trim('        <DateTime>2016-11-10 16:13:19</DateTime> ');
  StrSend:=StrSend+Trim('        <TaxSystem>1</TaxSystem>            ');
  StrSend:=StrSend+Trim('        <HeaderText>***********************************');
  StrSend:=StrSend+Trim('        *   ������ ��������               *');
  StrSend:=StrSend+Trim('        *                                 *');
  StrSend:=StrSend+Trim('        ***********************************');
  StrSend:=StrSend+Trim('        </HeaderText>            ');
  StrSend:=StrSend+Trim('        <Items>                                  ');
  StrSend:=StrSend+Trim('            <ArmReceiptItem>                     ');
  StrSend:=StrSend+Trim('                <ItemArticle>10984</ItemArticle> ');
  StrSend:=StrSend+Trim('                <ItemName>����� 10984</ItemName> ');
  StrSend:=StrSend+Trim('                <Price>0.10</Price>            ');
  StrSend:=StrSend+Trim('                <Quantity>1.000</Quantity>       ');
  StrSend:=StrSend+Trim('                <ItemTotal>0.00</ItemTotal>     ');
  StrSend:=StrSend+Trim('                <TaxType>1</TaxType>            ');
  StrSend:=StrSend+Trim('                <Tax>0.00</Tax>                 ');
  StrSend:=StrSend+Trim('                <ItemDiscounts>                 ');
  StrSend:=StrSend+Trim('                    <ArmDiscount>               ');
  StrSend:=StrSend+Trim('                        <Name>*MALUS*</Name>    ');
  StrSend:=StrSend+Trim('                        <Value>0.00</Value>     ');
  StrSend:=StrSend+Trim('                    </ArmDiscount>              ');
  StrSend:=StrSend+Trim('                </ItemDiscounts>                ');
  StrSend:=StrSend+Trim('                <ItemText>***���******************************'+#$0D);
  StrSend:=StrSend+Trim('                            ***********************************'+#$0D);
  StrSend:=StrSend+Trim('                            *+++++++++++++++++++++++++++++++++*'+#$0D);
  StrSend:=StrSend+Trim('                            ***********************************'+#$0D+'.');
  StrSend:=StrSend+Trim('                </ItemText>            ');
  StrSend:=StrSend+Trim('            </ArmReceiptItem>                   ');
  StrSend:=StrSend+Trim('            <ArmReceiptItem>                     ');
  StrSend:=StrSend+Trim('                <ItemArticle>10985</ItemArticle> ');
  StrSend:=StrSend+Trim('                <ItemName>����� 10985</ItemName> ');
  StrSend:=StrSend+Trim('                <Price>0.11</Price>            ');
  StrSend:=StrSend+Trim('                <Quantity>1.000</Quantity>       ');
  StrSend:=StrSend+Trim('                <ItemTotal>0.00</ItemTotal>     ');
  StrSend:=StrSend+Trim('                <TaxType>1</TaxType>            ');
  StrSend:=StrSend+Trim('                <Tax>0.00</Tax>                 ');
  StrSend:=StrSend+Trim('                <ItemDiscounts>                 ');
  StrSend:=StrSend+Trim('                    <ArmDiscount>               ');
  StrSend:=StrSend+Trim('                        <Name>*MALUS*</Name>    ');
  StrSend:=StrSend+Trim('                        <Value>0.00</Value>     ');
  StrSend:=StrSend+Trim('                    </ArmDiscount>              ');
  StrSend:=StrSend+Trim('                </ItemDiscounts>                ');
  StrSend:=StrSend+Trim('                <ItemText>*�������� ������            *******'+#$0D);
  StrSend:=StrSend+Trim('                            *---------------------------------*'+#$0D);
  StrSend:=StrSend+Trim('                            *+++++++++++++++++++++++++++++++++*'+#$0D);
  StrSend:=StrSend+Trim('                            ***********************************'+#$0D+'.');
  StrSend:=StrSend+Trim('                </ItemText>            ');
  StrSend:=StrSend+Trim('            </ArmReceiptItem>                   ');
  StrSend:=StrSend+Trim('        </Items>                                ');
  StrSend:=StrSend+Trim('        <CustomerAddress>                       ');
  StrSend:=StrSend+Trim('        </CustomerAddress>                      ');
  StrSend:=StrSend+Trim('        <Total>0.00</Total>                     ');
  StrSend:=StrSend+Trim('        <CashSumm>00.21</CashSumm>              ');
  StrSend:=StrSend+Trim('        <NonCashSumm>0.00</NonCashSumm>         ');
  StrSend:=StrSend+Trim('        <PosNum>1</PosNum>              ');
  StrSend:=StrSend+Trim('        <PosShiftNum>5</PosShiftNum>   ');
  StrSend:=StrSend+Trim('        <PosReceiptNum>10</PosReceiptNum>');
  StrSend:=StrSend+Trim('        <ArmReceiptTax>                         ');
  StrSend:=StrSend+Trim('            <nds18>0.00</nds18>                 ');
  StrSend:=StrSend+Trim('            <nds10>0.00</nds10>                 ');
  
  StrSend:=StrSend+Trim('            <nds00>0.00</nds00>                 ');
  StrSend:=StrSend+Trim('            <ndsNo>0.00</ndsNo>                 ');
  StrSend:=StrSend+Trim('            <nds18_118>0.00</nds18_118>         ');
  StrSend:=StrSend+Trim('            <nds10_110>0.00</nds10_110>         ');
  StrSend:=StrSend+Trim('        </ArmReceiptTax>                        ');
  StrSend:=StrSend+Trim('        <Footer>***********************************'+#$0D);
  StrSend:=StrSend+Trim('* ������� �� �������             *'+#$0D);
  StrSend:=StrSend+Trim('***********************************'+#$0D+'.');
  StrSend:=StrSend+Trim('                </Footer>            ');
  StrSend:=StrSend+Trim('        <FiscalDocNumber>0</FiscalDocNumber> ');
  StrSend:=StrSend+Trim('        <FiscalSign>0</FiscalSign>        ');
  StrSend:=StrSend+Trim('        <ShiftNumber>8</ShiftNumber>          ');
  StrSend:=StrSend+Trim('        <ReceiptNumber>0</ReceiptNumber> ');
  StrSend:=StrSend+Trim('    </ArmReceipt>]]>');
  StrSend:=StrSend+Trim('</RequestData>  ');
  StrSend:=StrSend+Trim('</ArmRequest>  ');

  Memo1.Lines.Add(StrSend);

  StrSendU:=AnsiToUtf8(StrSend);

  if ClientSock.Active=False then
  begin
    ClientSock.Host:='';
    ClientSock.Address:='192.168.0.100';
    ClientSock.Port:=6667;
    ClientSock.Open;
    Delay(100);
  end;

  if ClientSock.Active then
  begin
    ClientSock.Socket.SendText(StrSendU);

    for n:=1 to 100 do
    begin
      delay(50);
      if Length(sBuffer)>0 then break; //�� 50 �� ������ �� ����������...
    end;
    sRes:=Utf8ToAnsi(sBuffer);

    Memo1.Lines.Add(its(n)+'  '+sRes);
    Memo1.Lines.Add('');

    ShowXMLView(1,sBuffer);
  end;
end;

procedure TForm1.cxButton12Click(Sender: TObject);
var StrSend:string;
    StrSendU: UTF8String;
    sRes:string;
    n:Integer;
begin
  prClearBuf;
  StrSend:='';
  StrSend:=StrSend+Trim('<?xml version="1.0" encoding="UTF-8"?>');
  StrSend:=StrSend+Trim('<ArmRequest>');
  StrSend:=StrSend+Trim('    <RequestBody>');
  StrSend:=StrSend+Trim('        <ProtocolLabel>OFDFNARMUKM</ProtocolLabel> ');
  StrSend:=StrSend+Trim('        <ProtocolVersion>10.1</ProtocolVersion> ');
  StrSend:=StrSend+Trim('        <RequestId>{b97ece8e-1f65-49f2-bca8-da7cac1743b7}</RequestId> ');
  StrSend:=StrSend+Trim('        <DateTime>2016-11-08 13:00:09</DateTime> ');
  StrSend:=StrSend+Trim('        <Command>42</Command> ');
  StrSend:=StrSend+Trim('    </RequestBody>');
  StrSend:=StrSend+Trim('    <RequestData> ');
  StrSend:=StrSend+Trim('        <![CDATA[<ArmShiftClose> ');
  StrSend:=StrSend+Trim('        <FiscalDocNumber>0</FiscalDocNumber> ');
  StrSend:=StrSend+Trim('        <FiscalSign>0</FiscalSign> ');
  StrSend:=StrSend+Trim('        <ShiftNumber>5</ShiftNumber> ');
  StrSend:=StrSend+Trim('        <OFDQueueLegth>0</OFDQueueLegth>');
  StrSend:=StrSend+Trim('        <OFDfirstQueueDocDateTime> ');
  StrSend:=StrSend+Trim('        </OFDfirstQueueDocDateTime> ');
  StrSend:=StrSend+Trim('        <ReceiptsCount>0</ReceiptsCount> ');
  StrSend:=StrSend+Trim('        <FiscalDocCount>0</FiscalDocCount>');
  StrSend:=StrSend+Trim('        <OFDLongWait>0</OFDLongWait> ');
  StrSend:=StrSend+Trim('        <FNNeedChange>1</FNNeedChange> ');
  StrSend:=StrSend+Trim('        <FNOwerflow>1</FNOwerflow>');
  StrSend:=StrSend+Trim('        <FNExhausted>1</FNExhausted> ');
  StrSend:=StrSend+Trim('        <UserLoginName>������ �.�.</UserLoginName>');
  StrSend:=StrSend+Trim('        <DateTime>2017-04-04 12:58:37</DateTime> ');
  StrSend:=StrSend+Trim('        <HeaderReport>');
  StrSend:=StrSend+Trim('            ����� � 1 ');
  StrSend:=StrSend+Trim('            ����� � 5 ');
  StrSend:=StrSend+Trim('        </HeaderReport>');
  StrSend:=StrSend+Trim('        <FooterReport> 111222333 </FooterReport>');
  StrSend:=StrSend+Trim('    </ArmShiftClose>]]> ');
  StrSend:=StrSend+Trim('</RequestData> ');
  StrSend:=StrSend+Trim('</ArmRequest>');

  Memo1.Lines.Add(StrSend);

  StrSendU:=AnsiToUtf8(StrSend);

  if ClientSock.Active=False then
  begin
    ClientSock.Host:='';
    ClientSock.Address:='192.168.0.100';
    ClientSock.Port:=6667;
    ClientSock.Open;
    Delay(100);
  end;

  if ClientSock.Active then
  begin
    ClientSock.Socket.SendText(StrSendU);

    for n:=1 to 100 do
    begin
      delay(50);
      if Length(sBuffer)>0 then break; //�� 50 �� ������ �� ����������...
    end;
    sRes:=Utf8ToAnsi(sBuffer);

//    while Pos(#$0A,sRes)>0 do Delete(sRes,Pos(#$0A,sRes),1);

    Memo1.Lines.Add(its(n)+'  '+sRes);
    Memo1.Lines.Add('');

    ShowXMLView(1,sBuffer);
  end;
end;


procedure TForm1.cxButton13Click(Sender: TObject);
var StrSend:string;
    StrSendU: UTF8String;
    sRes:string;
    n:Integer;
begin
// ��������� ��������� ���
  prClearBuf;

  StrSend:='';
  StrSend:=StrSend+Trim('<?xml version="1.0" encoding="UTF-8"?>');
  StrSend:=StrSend+Trim('<ArmRequest>');
  StrSend:=StrSend+Trim('    <RequestBody>');
  StrSend:=StrSend+Trim('        <ProtocolLabel>OFDFNARMUKM</ProtocolLabel>');
  StrSend:=StrSend+Trim('        <ProtocolVersion>1</ProtocolVersion>');
  StrSend:=StrSend+Trim('        <RequestId>{151f698e-6e19-4a5c-a562-ddd2f8571d33}</RequestId>');
  StrSend:=StrSend+Trim('        <DateTime>2017-04-11 10:59:20</DateTime>');
  StrSend:=StrSend+Trim('        <Command>40</Command>');
  StrSend:=StrSend+Trim('    </RequestBody>');
  StrSend:=StrSend+Trim('    <RequestData><![CDATA[]]>');
  StrSend:=StrSend+Trim('    </RequestData>');
  StrSend:=StrSend+Trim('</ArmRequest>');
  Memo1.Lines.Add('');
  Memo1.Lines.Add(StrSend);

  StrSendU:=AnsiToUtf8(StrSend);

  if ClientSock.Active=False then
  begin
    ClientSock.Host:='';
    ClientSock.Address:='192.168.0.100';
    ClientSock.Port:=6667;
    ClientSock.Open;
    Delay(100);
  end;

  if ClientSock.Active then
  begin
    ClientSock.Socket.SendText(StrSend);

    for n:=1 to 100 do
    begin
      delay(50);
      if Length(sBuffer)>0 then break; //�� 50 �� ������ �� ����������...
    end;
    sRes:=sBuffer;

    Memo1.Lines.Add(its(n)+'  '+sRes);
    Memo1.Lines.Add('');

    ShowXMLView(1,sRes);
  end;
end;


end.
