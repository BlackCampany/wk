program Config;

uses
  Forms,
  uMainConfig in 'uMainConfig.pas' {fmMainConfig},
  uPressKey in 'uPressKey.pas' {fmPressKey},
  uAddOper in 'uAddOper.pas' {fmAddOper},
  uSaveLoad in 'uSaveLoad.pas' {fmSaveLoad},
  Un1 in 'Un1.pas',
  Passw in 'Passw.pas' {fmPerA},
  Dm in 'Dm.pas' {dmC: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfmMainConfig, fmMainConfig);
  Application.CreateForm(TdmC, dmC);
  Application.CreateForm(TfmPerA, fmPerA);
  Application.CreateForm(TfmPressKey, fmPressKey);
  Application.CreateForm(TfmAddOper, fmAddOper);
  Application.CreateForm(TfmSaveLoad, fmSaveLoad);
  Application.Run;
end.
