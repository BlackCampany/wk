unit MainFbConv;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, RXShell, Menus;

type
  TfmMainFBConv = class(TForm)
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    RxTray: TRxTrayIcon;
    Timer1: TTimer;
    Timer2: TTimer;
    v16A11: TMenuItem;
    procedure N1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
    procedure RxTrayClick(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Timer1Timer(Sender: TObject);
    procedure N3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

procedure WrMess(StrOut:String);
procedure prTransp;

var
  fmMainFBConv: TfmMainFBConv;

implementation

uses HistoryTr, Un1, Dm, uDBFB;

{$R *.dfm}

procedure TfmMainFBConv.N1Click(Sender: TObject);
begin
  fmHistoryTr.Show;
end;

procedure TfmMainFBConv.FormCreate(Sender: TObject);
begin
  CurDir := ExtractFilePath(ParamStr(0));
  ReadIni;
end;

procedure TfmMainFBConv.Timer2Timer(Sender: TObject);
begin
  if CommonSet.Turbo=1 then WrMess('������ �����. ������� '+IntToStr(1000)+'��')
  else WrMess('������ �����. ������� '+IntToStr(CommonSet.PeriodPing*1000)+'��');
  WrMess('  �� ����� - '+DBName);
  WrMess('  �� ������ - '+CommonSet.CashDB);

  Timer2.Enabled:=False;
  if CommonSet.Turbo=0 then
  begin
    Timer1.Enabled:=False;
    Timer1.Interval:=CommonSet.PeriodPing*1000;
    Timer1.Enabled:=True;
  end else
  begin
    Timer1.Enabled:=False;
    Timer1.Interval:=1000;
    Timer1.Enabled:=True;
  end;
end;

procedure WrMess(StrOut:String);
Var StrWk:String;
begin
  with fmHistoryTr do
  begin
    if Memo1.Lines.Count > 500 then Memo1.Clear;
    if StrOut>'' then StrWk:=FormatDateTime('dd.mm hh:nn:ss',now)+'  '+StrOut
    else StrWk:=StrOut;
    Memo1.Lines.Add(StrWk);
    WriteHistoryConv(StrWk);
  end;
end;


procedure TfmMainFBConv.RxTrayClick(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  fmHistoryTr.Show;
end;

procedure TfmMainFBConv.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled:=False;
  try
//    if CommonSet.Turbo=1 then fmHistoryTr.Memo1.Lines.Add('.');
    prTransp;
  except
  end;  
  Timer1.Enabled:=True;
end;

procedure prTransp;
var sNumCash:String;
    iCount:INteger;
    iPak:INteger;
begin
  with dmFB do
  with dmC do
  begin
    try
      Cash.Close;
      Cash.DBName:=CommonSet.CashDB;
      Cash.Open;
      if Cash.Connected then
      begin
        try
          CasherDb.Close;
          CasherDb.DBName:=DBName;
          CasherDb.Open;

          if CasherDb.Connected then
          begin
            sNumCash:=IntToStr(CommonSet.CashNum);
            if length(sNumCash)<2 then sNumCash:='0'+sNumCash;

            quSetActive.SQL.Clear;
            quSetActive.SQL.Add('update cash'+sNumCash+' set IACTIVE=2');
            quSetActive.SQL.Add('where IACTIVE=1');
            quSetActive.ExecQuery;
            delay(10);

            //���� �� 100 �������
            iPak:=CommonSet.ImportCount;
            if CommonSet.Turbo=1 then iPak:=100;

            quSelLoad.Active:=False;
            quSelLoad.SelectSQL.Clear;
            quSelLoad.SelectSQL.Add('SELECT first '+its(iPak)+' ID,IACTIVE,ITYPE,ARTICUL,BARCODE,CARDSIZE,QUANT,DSTOP,NAME,MESSUR,PRESISION,');
            quSelLoad.SelectSQL.Add('ADD1,ADD2,ADD3,ADDNUM1,ADDNUM2,ADDNUM3,SCALE,GROUP1,GROUP2,GROUP3,PRICE,CLIINDEX,DELETED,PASSW');
            quSelLoad.SelectSQL.Add('FROM CASH'+sNumCash);
            quSelLoad.SelectSQL.Add('where IACTIVE=2');


            quSelLoad.UpdateSQL.Clear;
            quSelLoad.UpdateSQL.Add('UPDATE CASH'+sNumCash+' SET IACTIVE = :IACTIVE');
            quSelLoad.UpdateSQL.Add('WHERE ID = :OLD_ID');

            quSelLoad.RefreshSQL.Clear;
            quSelLoad.RefreshSQL.Add('SELECT first '+its(iPak)+' ID,IACTIVE,ITYPE,ARTICUL,BARCODE,CARDSIZE,QUANT,DSTOP,NAME,MESSUR,PRESISION,');
            quSelLoad.RefreshSQL.Add('ADD1,ADD2,ADD3,ADDNUM1,ADDNUM2,ADDNUM3,SCALE,GROUP1,GROUP2,GROUP3,PRICE,CLIINDEX,DELETED,PASSW');
            quSelLoad.RefreshSQL.Add('FROM CASH'+sNumCash);
            quSelLoad.RefreshSQL.Add('where(IACTIVE=2) and (CASH'+sNumCash+'.ID = :OLD_ID)');

            quSelLoad.Active:=True;
            quSelLoad.First;     iCount:=0;
            while not quSelLoad.Eof do
            begin
              if quSelLoadITYPE.AsInteger=1 then
              begin
                try
                  if quSelLoadDELETED.AsInteger>0 then
                  begin
                    if CommonSet.Turbo=0 then WrMess('  ��������� ����.'+quSelLoadARTICUL.AsString);

                    //EXECUTE PROCEDURE PR_ADDCARD_A (?ARTICUL, ?CLASSIF, ?DEPART, ?NAME, ?CARD_TYPE, ?MESURIMENT, ?PRICE_RUB, ?DISCQUANT, ?DISCOUNT, ?SCALE, ?AVID, ?AKREP)
                    prAddCardA.ParamByName('ARTICUL').AsString:=quSelLoadARTICUL.AsString;
                    prAddCardA.ParamByName('CLASSIF').AsInteger:=quSelLoadGROUP3.AsInteger;
                    prAddCardA.ParamByName('DEPART').AsInteger:=quSelLoadCLIINDEX.AsInteger;
                    prAddCardA.ParamByName('NAME').AsString:=quSelLoadNAME.AsString;

                    if quSelLoadPRESISION.AsFloat=1 then prAddCardA.ParamByName('CARD_TYPE').AsInteger:=1
                    else prAddCardA.ParamByName('CARD_TYPE').AsInteger:=2;

                    prAddCardA.ParamByName('MESURIMENT').AsString:=quSelLoadMESSUR.AsString;
                    prAddCardA.ParamByName('PRICE_RUB').AsFloat:=quSelLoadPRICE.AsFloat;
                    prAddCardA.ParamByName('DISCOUNT').AsFloat:=0;
                    prAddCardA.ParamByName('DISCQUANT').AsFloat:=0;
                    prAddCardA.ParamByName('SCALE').AsString:=quSelLoadSCALE.AsString;

                    prAddCardA.ParamByName('AVID').AsInteger:=quSelLoadGROUP1.AsInteger;
                    prAddCardA.ParamByName('AKREP').AsFloat:=0;

                    prAddCardA.ParamByName('INDS').AsInteger:=quSelLoadGROUP2.AsInteger;

                    prAddCardA.ExecProc;

                  end else
                  begin //��������
                    if CommonSet.Turbo=0 then WrMess('  ������� ����.'+quSelLoadARTICUL.AsString);
                    prDelCard.ParamByName('ARTICUL').AsString:=quSelLoadARTICUL.AsString;
                    prDelCard.ExecProc
                  end;
                except
                end;
              end;

              if quSelLoadITYPE.AsInteger=2 then
              begin
                try
                  if CommonSet.Turbo=0 then WrMess('  ��������� �� '+quSelLoadBARCODE.AsString);

                  prAddBar.ParamByName('BARCODE').AsString:=quSelLoadBARCODE.AsString;
                  prAddBar.ParamByName('CARDARTICUL').AsString:=quSelLoadARTICUL.AsString;
                  prAddBar.ParamByName('CARDSIZE').AsString:=quSelLoadCARDSIZE.AsString;
                  prAddBar.ParamByName('QUANTITY').AsFloat:=quSelLoadQUANT.AsFloat;
                  prAddBar.ParamByName('PRICERUB').AsFloat:=quSelLoadPRICE.AsFloat;

                  prAddBar.ExecProc;
                except
                end;
              end;

              if quSelLoadITYPE.AsInteger=3 then
              begin
                try
                  if CommonSet.Turbo=0 then WrMess('  ��������� ������ '+quSelLoadNAME.AsString);

//EXECUTE PROCEDURE PR_ADDCLASS (?ID, ?PARENT, ?NAME)

                  prAddClassif.ParamByName('ID').AsInteger:=quSelLoadARTICUL.AsInteger;
                  prAddClassif.ParamByName('PARENT').AsInteger:=quSelLoadGROUP3.AsInteger;
                  prAddClassif.ParamByName('NAME').AsString:=quSelLoadNAME.AsString;

                  prAddClassif.ExecProc;
                except
                end;
              end;

              if quSelLoadITYPE.AsInteger=18 then
              begin
                try
                  if CommonSet.Turbo=0 then WrMess('  ����. ���� '+quSelLoadARTICUL.AsString+' '+IntToStr(Trunc(rv(quSelLoadDSTOP.AsFloat))));

                  if taFixDisc.Active=False then taFixDisc.Active:=True;

                  if taFixDisc.Locate('SART',quSelLoadARTICUL.AsString,[]) then
                  begin
                    taFixDisc.Edit;
                    taFixDiscPROC.AsFloat:=quSelLoadDSTOP.AsFloat;
                    taFixDisc.Post;
                  end else
                  begin
                    taFixDisc.Append;
                    taFixDiscSART.AsString:=quSelLoadARTICUL.AsString;
                    taFixDiscPROC.AsFloat:=quSelLoadDSTOP.AsFloat;
                    taFixDisc.Post;
                  end;
                except
                end;
              end;

              if quSelLoadITYPE.AsInteger=11 then
              begin
                try
                  if CommonSet.Turbo=0 then WrMess('  ����. ����� '+quSelLoadBARCODE.AsString+' '+IntToStr(Trunc(rv(quSelLoadDSTOP.AsFloat))));

                  if taDiscCard.Active=False then taDiscCard.Active:=True;

                  if taDiscCard.Locate('BARCODE',quSelLoadBARCODE.AsString,[]) then
                  begin
                    taDiscCard.Edit;
                    taDiscCardNAME.AsString:=quSelLoadNAME.AsString;
                    taDiscCardPERCENT.AsFloat:=quSelLoadDSTOP.AsFloat;
                    taDiscCardCLIENTINDEX.AsInteger:=quSelLoadCLIINDEX.AsInteger;
                    taDiscCardIACTIVE.AsInteger:=quSelLoadDELETED.AsInteger;
                    taDiscCard.Post;
                  end else
                  begin
                    taDiscCard.Append;
                    taDiscCardNAME.AsString:=quSelLoadNAME.AsString;
                    taDiscCardPERCENT.AsFloat:=quSelLoadDSTOP.AsFloat;
                    taDiscCardCLIENTINDEX.AsInteger:=quSelLoadCLIINDEX.AsInteger;
                    taDiscCardIACTIVE.AsInteger:=quSelLoadDELETED.AsInteger;
                    taDiscCard.Post;
                  end;
                except
                end;
              end;

              if quSelLoadITYPE.AsInteger=107 then //������� ���� �������� ����� �������
              begin
                try
                  quDelPers.ExecQuery;
                except
                end;
                delay(100);
              end;

              if quSelLoadITYPE.AsInteger=7 then //��� �������� ��������
              begin
                try
                  if taPersonal.Active=False then taPersonal.Active:=True;

                  if taPersonal.Locate('ID',quSelLoadARTICUL.AsInteger,[]) then
                  begin
                    if taPersonalID_PARENT.AsInteger<>0 then
                    begin
                      taPersonal.Edit;

                      if pos('�����',quSelLoadNAME.AsString)>0 then taPersonalID_PARENT.AsInteger:=1000
                      else taPersonalID_PARENT.AsInteger:=1002;

                      taPersonalUVOLNEN.AsInteger:=1;
                      taPersonalMODUL3.AsInteger:=0;
                      taPersonalNAME.AsString:=quSelLoadNAME.AsString;
                      taPersonalPASSW.AsString:=quSelLoadBARCODE.AsString;
                      taPersonalBARCODE.AsString:=quSelLoadBARCODE.AsString;
                      taPersonal.Post;
                    end;
                  end else
                  begin
                    taPersonal.Append;
                    taPersonalID.AsInteger:=quSelLoadARTICUL.AsInteger;

                    if pos('�����',quSelLoadNAME.AsString)>0 then taPersonalID_PARENT.AsInteger:=1000
                    else taPersonalID_PARENT.AsInteger:=1002;

                    taPersonalUVOLNEN.AsInteger:=1;
                    taPersonalMODUL3.AsInteger:=0;
                    taPersonalNAME.AsString:=quSelLoadNAME.AsString;
                    taPersonalPASSW.AsString:=quSelLoadBARCODE.AsString;
                    taPersonalBARCODE.AsString:=quSelLoadBARCODE.AsString;
                    taPersonal.Post;
                  end;
                except
                end;
                delay(100);
              end;

              quSelLoad.Edit;
              quSelLoadIACTIVE.AsInteger:=3;
              quSelLoad.Post;

              quSelLoad.Next; inc(iCount);
            end;
            if iCount>0 then WrMess(' ��. ('+IntToStr(iCount)+' �������)');
            if taFixDisc.Active=True then taFixDisc.Active:=False;
            if taDiscCard.Active=True then taDiscCard.Active:=False;
            if taPersonal.Active=True then taPersonal.Active:=False;
          end;
          CasherDb.Close;
        except
          CasherDb.Close;
        end;
      end;
      Cash.Close;
    except
      Cash.Close;
    end;
  end;
end;

procedure TfmMainFBConv.N3Click(Sender: TObject);
begin
  Close;
end;

end.
