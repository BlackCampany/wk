program StdConv;

uses
  Forms,
  MainStdConv in 'MainStdConv.pas' {fmMainConvStd},
  Dm in 'Dm.pas' {dmC: TDataModule},
  HistoryTr in 'HistoryTr.pas' {fmHistoryTr},
  Un1 in 'Un1.pas',
  u2fdk in 'U2FDK.PAS';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfmMainConvStd, fmMainConvStd);
  Application.CreateForm(TdmC, dmC);
  Application.CreateForm(TfmHistoryTr, fmHistoryTr);
  Application.ShowMainForm:=FALSE;
  Application.Run;
end.
