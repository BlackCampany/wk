object fmMainCasher: TfmMainCasher
  Tag = 1
  Left = 684
  Top = 120
  BorderIcons = []
  BorderStyle = bsNone
  ClientHeight = 606
  ClientWidth = 812
  Color = 8080936
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCanResize = FormCanResize
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object RxClock1: TRxClock
    Left = 672
    Top = 528
    Width = 113
    Height = 33
    Color = 6175776
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 14927276
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    OnClick = RxClock1Click
  end
  object Panel1: TPanel
    Left = 628
    Top = 4
    Width = 161
    Height = 205
    BevelInner = bvLowered
    Color = 6175776
    TabOrder = 1
    object Label8: TLabel
      Left = 8
      Top = 48
      Width = 59
      Height = 16
      Caption = #1057#1084#1077#1085#1072' '#8470
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = 15456197
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label9: TLabel
      Left = 8
      Top = 28
      Width = 61
      Height = 16
      Caption = #1058#1077#1082'. '#1076#1072#1090#1072
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = 15456197
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label14: TLabel
      Left = 8
      Top = 8
      Width = 55
      Height = 16
      Caption = #1050#1072#1089#1089#1072' '#8470
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = 15456197
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label10: TLabel
      Left = 8
      Top = 108
      Width = 41
      Height = 16
      Caption = #1063#1077#1082' '#8470
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = 15456197
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label15: TLabel
      Left = 8
      Top = 148
      Width = 76
      Height = 16
      Caption = #1057#1090#1072#1090#1091#1089' '#1050#1050#1052
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = 15456197
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label16: TLabel
      Left = 8
      Top = 128
      Width = 65
      Height = 16
      Caption = #1054#1087#1077#1088#1072#1094#1080#1103
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = 15456197
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label20: TLabel
      Left = 104
      Top = 168
      Width = 16
      Height = 11
      Caption = #1044#1055
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 15456197
      Font.Height = -13
      Font.Name = 'MS Serif'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label21: TLabel
      Left = 8
      Top = 168
      Width = 31
      Height = 11
      Caption = #1057#1082#1072#1085#1077#1088
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 15456197
      Font.Height = -13
      Font.Name = 'MS Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label22: TLabel
      Left = 8
      Top = 68
      Width = 56
      Height = 16
      Caption = #1054#1090#1082#1088#1099#1090#1072
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = 15456197
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label23: TLabel
      Left = 8
      Top = 88
      Width = 27
      Height = 16
      Caption = #1060#1053'  '
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = 15456197
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label27: TLabel
      Left = 12
      Top = 188
      Width = 100
      Height = 13
      AutoSize = False
      Caption = 'v12.12'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 15456197
      Font.Height = -8
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      WordWrap = True
    end
  end
  object Panel2: TPanel
    Left = 632
    Top = 216
    Width = 157
    Height = 41
    BevelInner = bvLowered
    Color = 10329423
    TabOrder = 2
    OnClick = acChangePersExecute
    object Label13: TLabel
      Left = 8
      Top = 13
      Width = 133
      Height = 16
      Alignment = taCenter
      AutoSize = False
      BiDiMode = bdLeftToRight
      Caption = #1056#1077#1078#1080#1084' '#1082#1072#1089#1089#1080#1088#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentBiDiMode = False
      ParentFont = False
      OnClick = acChangePersExecute
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 0
    Width = 625
    Height = 580
    BevelInner = bvLowered
    Color = 8080936
    TabOrder = 3
    Visible = False
    object Label7: TLabel
      Left = 368
      Top = 424
      Width = 130
      Height = 20
      Caption = #1042#1089#1077#1075#1086' '#1082' '#1086#1087#1083#1072#1090#1077
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 15456197
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label12: TLabel
      Left = 216
      Top = 440
      Width = 134
      Height = 16
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1064#1050
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 15456197
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label17: TLabel
      Left = 8
      Top = 424
      Width = 13
      Height = 16
      Caption = '...'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
    end
    object Label24: TLabel
      Left = 368
      Top = 536
      Width = 13
      Height = 16
      Caption = '...'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 15456197
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      Visible = False
    end
    object dxfLabel1: TdxfLabel
      Left = 536
      Top = 540
      Width = 89
      Height = 17
      AutoSize = False
      Caption = 'www.softur.ru'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clLime
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
      Visible = False
      Style = dxfNormal
      Angle = 0
      Effect3D.Style = dxfCool
      Effect3D.Orientation = dxfRightBottom
      Effect3D.Depth = 0
      Effect3D.ShadowedColor = clWhite
      PenWidth = 1
    end
    object Label11: TLabel
      Left = 12
      Top = 504
      Width = 46
      Height = 16
      Caption = #1057#1090#1072#1090#1091#1089
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 15456197
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label26: TLabel
      Left = 8
      Top = 440
      Width = 78
      Height = 16
      Caption = #1042#1074#1077#1076#1080#1090#1077' '#1064#1050
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 15456197
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Edit2: TcxTextEdit
      Left = 212
      Top = 464
      TabStop = False
      BeepOnEnter = False
      TabOrder = 6
      Text = 'Edit2'
      OnKeyPress = Edit2KeyPress
      Width = 121
    end
    object Edit1: TcxTextEdit
      Left = 80
      Top = 465
      TabStop = False
      BeepOnEnter = False
      Properties.MaxLength = 20
      TabOrder = 2
      Text = 'Edit1'
      OnKeyPress = Edit1KeyPress
      Width = 121
    end
    object CurrencyEdit3: TcxCurrencyEdit
      Left = 368
      Top = 444
      TabStop = False
      AutoSize = False
      EditValue = 99999.009999999990000000
      ParentFont = False
      Properties.Alignment.Horz = taCenter
      Properties.Alignment.Vert = taVCenter
      Properties.AutoSelect = False
      Properties.DisplayFormat = '0. '#1088'00;-,0. '#1088'00'
      Properties.ReadOnly = True
      Properties.UseThousandSeparator = True
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = 7405793
      Style.Font.Height = -55
      Style.Font.Name = 'Arial Narrow'
      Style.Font.Pitch = fpVariable
      Style.Font.Style = [fsBold]
      Style.StyleController = cxEditStyleController1
      Style.IsFontAssigned = True
      TabOrder = 1
      Height = 89
      Width = 249
    end
    object TextEdit3: TcxTextEdit
      Left = 8
      Top = 456
      BeepOnEnter = False
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -24
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsBold]
      Style.StyleController = cxEditStyleController1
      Style.IsFontAssigned = True
      TabOrder = 0
      OnKeyPress = TextEdit3KeyPress
      Width = 345
    end
    object GridCh: TcxGrid
      Left = 7
      Top = 4
      Width = 609
      Height = 285
      BorderWidth = 1
      TabOrder = 3
      LookAndFeel.Kind = lfFlat
      object ViewCh: TcxGridDBTableView
        NavigatorButtons.ConfirmDelete = False
        DataController.DataSource = dmC.dsCheck
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <
          item
            Format = '0.00'
            Kind = skSum
            FieldName = 'DSUM'
            Column = ViewChDSUM
          end
          item
            Format = '0.00'
            Kind = skSum
            FieldName = 'RSUM'
            Column = ViewChRSUM
          end>
        DataController.Summary.SummaryGroups = <>
        OptionsCustomize.ColumnFiltering = False
        OptionsCustomize.ColumnGrouping = False
        OptionsCustomize.ColumnSorting = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsSelection.CellSelect = False
        OptionsSelection.HideFocusRectOnExit = False
        OptionsSelection.UnselectFocusedRecordOnExit = False
        OptionsView.Footer = True
        OptionsView.GroupByBox = False
        Styles.Header = dmC.cxStyle4
        object ViewChCASHNUM: TcxGridDBColumn
          DataBinding.FieldName = 'CASHNUM'
          Visible = False
        end
        object ViewChNUMPOS: TcxGridDBColumn
          Caption = #8470' '#1087#1087
          DataBinding.FieldName = 'NUMPOS'
          Width = 29
        end
        object ViewChARTICUL: TcxGridDBColumn
          Caption = #1040#1088#1090#1080#1082#1091#1083
          DataBinding.FieldName = 'ARTICUL'
          Width = 55
        end
        object ViewChBAR: TcxGridDBColumn
          Caption = #1064#1090#1088#1080#1093#1082#1086#1076
          DataBinding.FieldName = 'RBAR'
          Styles.Content = dmC.cxStyle1
          Width = 70
        end
        object ViewChNAME: TcxGridDBColumn
          Caption = #1053#1072#1079#1074#1072#1085#1080#1077
          DataBinding.FieldName = 'NAME'
          Styles.Content = dmC.cxStyle2
          Width = 174
        end
        object ViewChQUANT: TcxGridDBColumn
          Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086
          DataBinding.FieldName = 'QUANT'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.DecimalPlaces = 3
          Properties.DisplayFormat = ',0.###;-,0.###'
          Styles.Content = dmC.cxStyle5
          Width = 50
        end
        object ViewChPRICE: TcxGridDBColumn
          Caption = #1062#1077#1085#1072
          DataBinding.FieldName = 'PRICE'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Width = 54
        end
        object ViewChDPROC: TcxGridDBColumn
          Caption = '% '#1089#1082#1080#1076#1082#1080
          DataBinding.FieldName = 'DPROC'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.DisplayFormat = ',0.00%;-,0.00%'
          Visible = False
          Styles.Content = dmC.cxStyle10
          Width = 35
        end
        object ViewChDSUM: TcxGridDBColumn
          Caption = #1057#1091#1084#1084#1072' '#1089#1082#1080#1076#1082#1080
          DataBinding.FieldName = 'DSUM'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.DisplayFormat = ',0.00;-,0.00'
          Visible = False
          Styles.Content = dmC.cxStyle10
          Width = 46
        end
        object ViewChRSUM: TcxGridDBColumn
          Caption = #1057#1091#1084#1084#1072
          DataBinding.FieldName = 'RSUM'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Styles.Content = dmC.cxStyle5
          Width = 62
        end
      end
      object LevelCh: TcxGridLevel
        GridView = ViewCh
      end
    end
    object Memo2: TcxMemo
      Left = 8
      Top = 500
      TabStop = False
      Lines.Strings = (
        '11111'
        '22222'
        '33333')
      ParentFont = False
      Properties.Alignment = taLeftJustify
      Properties.ReadOnly = True
      Properties.ScrollBars = ssVertical
      Style.Color = 8080936
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWhite
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = []
      Style.LookAndFeel.Kind = lfOffice11
      Style.StyleController = cxEditStyleController1
      Style.IsFontAssigned = True
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 4
      Height = 71
      Width = 345
    end
    object Panel4: TPanel
      Left = 8
      Top = 308
      Width = 609
      Height = 113
      BevelInner = bvLowered
      Color = 8080936
      TabOrder = 5
      object Label1: TLabel
        Left = 8
        Top = 5
        Width = 63
        Height = 16
        Caption = #1064#1090#1088#1080#1093#1082#1086#1076
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 15456197
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label2: TLabel
        Left = 8
        Top = 49
        Width = 55
        Height = 16
        Caption = #1040#1088#1090#1080#1082#1091#1083
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 15456197
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label3: TLabel
        Left = 128
        Top = 5
        Width = 66
        Height = 16
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 15456197
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label4: TLabel
        Left = 392
        Top = 5
        Width = 78
        Height = 16
        Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 15456197
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label5: TLabel
        Left = 484
        Top = 5
        Width = 33
        Height = 16
        Caption = #1062#1077#1085#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 15456197
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label6: TLabel
        Left = 484
        Top = 57
        Width = 43
        Height = 16
        Caption = #1057#1091#1084#1084#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 15456197
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label18: TLabel
        Left = 385
        Top = 69
        Width = 84
        Height = 13
        Alignment = taCenter
        AutoSize = False
        Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 15456197
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label19: TLabel
        Left = 386
        Top = 89
        Width = 83
        Height = 13
        Alignment = taCenter
        AutoSize = False
        Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 15456197
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object TextEdit1: TcxTextEdit
        Left = 8
        Top = 21
        TabStop = False
        ParentFont = False
        Properties.ReadOnly = True
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -13
        Style.Font.Name = 'MS Sans Serif'
        Style.Font.Style = [fsBold]
        Style.StyleController = cxEditStyleController1
        Style.IsFontAssigned = True
        TabOrder = 0
        Text = '1234567890123'
        Width = 117
      end
      object TextEdit2: TcxTextEdit
        Left = 8
        Top = 69
        TabStop = False
        ParentFont = False
        Properties.ReadOnly = True
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -19
        Style.Font.Name = 'MS Sans Serif'
        Style.Font.Style = [fsBold]
        Style.StyleController = cxEditStyleController1
        Style.IsFontAssigned = True
        TabOrder = 1
        Text = '10002'
        Width = 117
      end
      object Memo1: TcxMemo
        Left = 128
        Top = 20
        TabStop = False
        Lines.Strings = (
          #1050#1056#1059#1055#1040' '#1055#1064#1045#1053'.'#1055#1054#1051#1058#1040#1042#1057#1050#1040#1071' '
          #8470'3 '
          '500'#1043#1056)
        ParentFont = False
        Properties.Alignment = taCenter
        Properties.ReadOnly = True
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -15
        Style.Font.Name = 'MS Sans Serif'
        Style.Font.Style = [fsBold]
        Style.StyleController = cxEditStyleController1
        Style.IsFontAssigned = True
        TabOrder = 2
        Height = 81
        Width = 249
      end
      object CalcEdit1: TcxCalcEdit
        Left = 392
        Top = 21
        TabStop = False
        EditValue = 2.000000000000000000
        ParentFont = False
        Properties.Alignment.Horz = taCenter
        Properties.Alignment.Vert = taVCenter
        Properties.ReadOnly = True
        Properties.QuickClose = True
        Style.BorderStyle = ebsSingle
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -19
        Style.Font.Name = 'MS Sans Serif'
        Style.Font.Style = [fsBold]
        Style.LookAndFeel.Kind = lfFlat
        Style.LookAndFeel.NativeStyle = False
        Style.StyleController = cxEditStyleController1
        Style.ButtonStyle = btsFlat
        Style.ButtonTransparency = ebtHideInactive
        Style.IsFontAssigned = True
        StyleDisabled.LookAndFeel.Kind = lfFlat
        StyleDisabled.LookAndFeel.NativeStyle = False
        StyleFocused.LookAndFeel.Kind = lfFlat
        StyleFocused.LookAndFeel.NativeStyle = False
        StyleHot.LookAndFeel.Kind = lfFlat
        StyleHot.LookAndFeel.NativeStyle = False
        TabOrder = 3
        Width = 77
      end
      object CurrencyEdit1: TcxCurrencyEdit
        Left = 484
        Top = 73
        TabStop = False
        EditValue = 99999.990000000010000000
        ParentFont = False
        Properties.Alignment.Horz = taCenter
        Properties.Alignment.Vert = taVCenter
        Properties.DisplayFormat = '0. '#1088'00;-,0. '#1088'00'
        Properties.ReadOnly = True
        Properties.UseThousandSeparator = True
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -19
        Style.Font.Name = 'MS Sans Serif'
        Style.Font.Style = [fsBold]
        Style.StyleController = cxEditStyleController1
        Style.IsFontAssigned = True
        TabOrder = 4
        Width = 113
      end
      object CurrencyEdit2: TcxCurrencyEdit
        Left = 484
        Top = 20
        TabStop = False
        AutoSize = False
        EditValue = 99999.009999999990000000
        ParentFont = False
        Properties.Alignment.Horz = taCenter
        Properties.Alignment.Vert = taVCenter
        Properties.AutoSelect = False
        Properties.DisplayFormat = '0. '#1088'00;-,0. '#1088'00'
        Properties.ReadOnly = True
        Properties.UseThousandSeparator = True
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = 5412867
        Style.Font.Height = -19
        Style.Font.Name = 'MS Sans Serif'
        Style.Font.Pitch = fpVariable
        Style.Font.Style = [fsBold]
        Style.StyleController = cxEditStyleController1
        Style.IsFontAssigned = True
        TabOrder = 5
        Height = 33
        Width = 113
      end
    end
  end
  object cxButton1: TcxButton
    Left = 632
    Top = 264
    Width = 157
    Height = 36
    Action = acCashRep
    Caption = #1050#1072#1089#1089#1086#1074#1099#1077' '#1086#1090#1095#1077#1090#1099
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 5
    TabStop = False
    Visible = False
    Colors.Default = 14864576
    Colors.Normal = 12621940
    Colors.Pressed = 10843723
    Layout = blGlyphTop
    LookAndFeel.Kind = lfFlat
  end
  object cxButton10: TcxButton
    Left = 814
    Top = 120
    Width = 21
    Height = 25
    Caption = #1056#1072#1089#1095#1077#1090
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 6
    TabStop = False
    Visible = False
    Colors.Default = 14864576
    Colors.Normal = 12621940
    Colors.Pressed = 10843723
    Layout = blGlyphTop
    LookAndFeel.Kind = lfFlat
  end
  object cxButton11: TcxButton
    Left = 819
    Top = 97
    Width = 13
    Height = 17
    Caption = #1050#1086#1083'-'#1074#1086
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 7
    TabStop = False
    Visible = False
    Colors.Default = 14864576
    Colors.Normal = 12621940
    Colors.Pressed = 10843723
    Layout = blGlyphTop
    LookAndFeel.Kind = lfFlat
  end
  object Panel6: TPanel
    Left = 823
    Top = 38
    Width = 17
    Height = 21
    BevelInner = bvLowered
    Color = 8080936
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 8
    Visible = False
    object cxCalcEdit1: TcxCalcEdit
      Left = 16
      Top = 16
      TabStop = False
      EditValue = 1.000000000000000000
      ParentFont = False
      Properties.Alignment.Horz = taRightJustify
      Properties.Precision = 5
      Properties.ReadOnly = True
      Properties.QuickClose = True
      Style.BorderStyle = ebsFlat
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = 8552960
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsBold]
      Style.LookAndFeel.Kind = lfFlat
      Style.ButtonTransparency = ebtHideInactive
      Style.IsFontAssigned = True
      StyleDisabled.LookAndFeel.Kind = lfFlat
      StyleFocused.LookAndFeel.Kind = lfFlat
      StyleHot.LookAndFeel.Kind = lfFlat
      TabOrder = 0
      Visible = False
      Width = 121
    end
    object cxButton12: TcxButton
      Left = 8
      Top = 144
      Width = 40
      Height = 40
      Caption = '1'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 1
      TabStop = False
      OnClick = cxButton12Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfFlat
    end
    object cxButton13: TcxButton
      Left = 56
      Top = 144
      Width = 40
      Height = 40
      Caption = '2'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 2
      TabStop = False
      OnClick = cxButton13Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfFlat
    end
    object cxButton14: TcxButton
      Left = 104
      Top = 144
      Width = 40
      Height = 40
      Caption = '3'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 3
      TabStop = False
      OnClick = cxButton14Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfFlat
    end
    object cxButton15: TcxButton
      Left = 8
      Top = 96
      Width = 40
      Height = 40
      Caption = '4'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 4
      TabStop = False
      OnClick = cxButton15Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfFlat
    end
    object cxButton16: TcxButton
      Left = 56
      Top = 96
      Width = 40
      Height = 40
      Caption = '5'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 5
      TabStop = False
      OnClick = cxButton16Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfFlat
    end
    object cxButton17: TcxButton
      Left = 104
      Top = 96
      Width = 40
      Height = 40
      Caption = '6'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 6
      TabStop = False
      OnClick = cxButton17Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfFlat
    end
    object cxButton18: TcxButton
      Left = 8
      Top = 48
      Width = 40
      Height = 40
      Caption = '7'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 7
      TabStop = False
      OnClick = cxButton18Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfFlat
    end
    object cxButton19: TcxButton
      Left = 56
      Top = 48
      Width = 40
      Height = 40
      Caption = '8'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 8
      TabStop = False
      OnClick = cxButton19Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfFlat
    end
    object cxButton20: TcxButton
      Left = 104
      Top = 48
      Width = 40
      Height = 40
      Caption = '9'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 9
      TabStop = False
      OnClick = cxButton20Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfFlat
    end
    object cxButton21: TcxButton
      Left = 8
      Top = 192
      Width = 40
      Height = 40
      Caption = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 10
      TabStop = False
      OnClick = cxButton21Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfFlat
    end
    object cxButton25: TcxButton
      Left = 76
      Top = 192
      Width = 65
      Height = 40
      Caption = 'Ok'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ModalResult = 1
      ParentFont = False
      TabOrder = 11
      OnClick = cxButton25Click
      Colors.Default = 8421440
      Colors.Normal = 8421440
      Colors.Pressed = 13619102
      LookAndFeel.Kind = lfFlat
    end
    object cxButton32: TcxButton
      Left = 8
      Top = 8
      Width = 137
      Height = 33
      Caption = #1057#1073#1088#1086#1089
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 12
      TabStop = False
      OnClick = cxButton32Click
      Colors.Default = 12895487
      Colors.Normal = 12895487
      Colors.Pressed = 9671679
      LookAndFeel.Kind = lfFlat
    end
  end
  object GridHK: TcxGrid
    Left = 819
    Top = 64
    Width = 21
    Height = 25
    TabOrder = 9
    Visible = False
    LookAndFeel.Kind = lfOffice11
    object ViewHK: TcxGridTableView
      NavigatorButtons.ConfirmDelete = False
      OnCellClick = ViewHKCellClick
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnGrouping = False
      OptionsCustomize.ColumnHorzSizing = False
      OptionsCustomize.ColumnMoving = False
      OptionsCustomize.ColumnSorting = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.InvertSelect = False
      OptionsSelection.UnselectFocusedRecordOnExit = False
      OptionsView.DataRowHeight = 45
      OptionsView.GroupByBox = False
      OptionsView.GroupRowStyle = grsOffice11
      OptionsView.Header = False
    end
    object LevelHK: TcxGridLevel
      GridView = ViewHK
    end
  end
  object GridHK1: TcxGrid
    Left = 800
    Top = 8
    Width = 13
    Height = 25
    TabOrder = 10
    Visible = False
    LookAndFeel.Kind = lfOffice11
    object ViewHK1: TcxGridTableView
      NavigatorButtons.ConfirmDelete = False
      OnCellClick = ViewHK1CellClick
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnGrouping = False
      OptionsCustomize.ColumnHorzSizing = False
      OptionsCustomize.ColumnMoving = False
      OptionsCustomize.ColumnSorting = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.InvertSelect = False
      OptionsSelection.UnselectFocusedRecordOnExit = False
      OptionsView.DataRowHeight = 45
      OptionsView.GroupByBox = False
      OptionsView.GroupRowStyle = grsOffice11
      OptionsView.Header = False
    end
    object LevelHK1: TcxGridLevel
      GridView = ViewHK1
    end
  end
  object cxButton22: TcxButton
    Left = 814
    Top = 364
    Width = 19
    Height = 30
    Action = acSetHK
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 11
    TabStop = False
    Visible = False
    Colors.Default = 14864576
    Colors.Normal = 12621940
    Colors.Pressed = 10843723
    Layout = blGlyphTop
    LookAndFeel.Kind = lfFlat
  end
  object cxButton23: TcxButton
    Left = 815
    Top = 171
    Width = 21
    Height = 25
    Action = acPrintTCH
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 12
    TabStop = False
    Colors.Default = 14864576
    Colors.Normal = 12621940
    Colors.Pressed = 10843723
    Layout = blGlyphTop
    LookAndFeel.Kind = lfFlat
  end
  object cxButton24: TcxButton
    Left = 818
    Top = 276
    Width = 21
    Height = 17
    Action = acCopyCheck
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 13
    TabStop = False
    Colors.Default = 14864576
    Colors.Normal = 12621940
    Colors.Pressed = 10843723
    Layout = blGlyphTop
    LookAndFeel.Kind = lfFlat
  end
  object cxButton26: TcxButton
    Left = 817
    Top = 202
    Width = 21
    Height = 21
    Action = acCalc
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 14
    TabStop = False
    Colors.Default = 14864576
    Colors.Normal = 12621940
    Colors.Pressed = 10843723
    Layout = blGlyphTop
    LookAndFeel.Kind = lfFlat
  end
  object cxButton27: TcxButton
    Left = 817
    Top = 294
    Width = 21
    Height = 21
    Action = acCashDriverOpen
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 15
    TabStop = False
    Colors.Default = 14864576
    Colors.Normal = 12621940
    Colors.Pressed = 10843723
    Layout = blGlyphTop
    LookAndFeel.Kind = lfFlat
  end
  object cxButton28: TcxButton
    Left = 816
    Top = 227
    Width = 21
    Height = 21
    Action = acAnnul
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 16
    TabStop = False
    Colors.Default = 14864576
    Colors.Normal = 12621940
    Colors.Pressed = 10843723
    Layout = blGlyphTop
    LookAndFeel.Kind = lfFlat
  end
  object cxButton29: TcxButton
    Left = 815
    Top = 320
    Width = 21
    Height = 17
    Action = acError
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 17
    TabStop = False
    Colors.Default = 14864576
    Colors.Normal = 12621940
    Colors.Pressed = 10843723
    Layout = blGlyphTop
    LookAndFeel.Kind = lfFlat
  end
  object cxButton30: TcxButton
    Left = 817
    Top = 252
    Width = 21
    Height = 21
    Action = acDelPos
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 18
    TabStop = False
    Colors.Default = 14864576
    Colors.Normal = 12621940
    Colors.Pressed = 10843723
    Layout = blGlyphTop
    LookAndFeel.Kind = lfFlat
  end
  object cxButton31: TcxButton
    Left = 814
    Top = 340
    Width = 21
    Height = 17
    Action = acFindArt
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 19
    TabStop = False
    Colors.Default = 14864576
    Colors.Normal = 12621940
    Colors.Pressed = 10843723
    Layout = blGlyphTop
    LookAndFeel.Kind = lfFlat
  end
  object Panel5: TPanel
    Left = 632
    Top = 303
    Width = 157
    Height = 250
    BevelInner = bvLowered
    Color = 6832674
    TabOrder = 4
    Visible = False
    object cxButton6: TcxButton
      Left = 5
      Top = 225
      Width = 137
      Height = 29
      Action = acRet
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      TabStop = False
      Colors.Default = 14930628
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      Layout = blGlyphTop
      LookAndFeel.Kind = lfFlat
    end
    object cxButton2: TcxButton
      Left = 8
      Top = 9
      Width = 137
      Height = 24
      Caption = 'X-'#1086#1090#1095#1077#1090' '
      Default = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = cxButton2Click
      Colors.Default = 14930628
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      Layout = blGlyphTop
      LookAndFeel.Kind = lfFlat
    end
    object cxButton3: TcxButton
      Left = 8
      Top = 37
      Width = 137
      Height = 24
      Caption = 'Z-'#1086#1090#1095#1077#1090
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = cxButton3Click
      Colors.Default = 14930628
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      Layout = blGlyphTop
      LookAndFeel.Kind = lfFlat
    end
    object cxButton4: TcxButton
      Left = 8
      Top = 93
      Width = 137
      Height = 24
      Caption = #1048#1079#1098#1103#1090#1080#1077' '#1076#1077#1085#1077#1075
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      OnClick = cxButton4Click
      Colors.Default = 14930628
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      Layout = blGlyphTop
      LookAndFeel.Kind = lfFlat
    end
    object cxButton5: TcxButton
      Left = 8
      Top = 121
      Width = 137
      Height = 24
      Caption = #1042#1085#1077#1089#1077#1085#1080#1077' '#1076#1077#1085#1077#1075
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
      OnClick = cxButton5Click
      Colors.Default = 14930628
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      Layout = blGlyphTop
      LookAndFeel.Kind = lfFlat
    end
    object cxButton7: TcxButton
      Left = 8
      Top = 149
      Width = 137
      Height = 24
      Caption = #1041#1072#1085#1082
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
      OnClick = cxButton7Click
      Colors.Default = 14930628
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      Layout = blGlyphTop
      LookAndFeel.Kind = lfFlat
    end
    object cxButton8: TcxButton
      Left = 8
      Top = 65
      Width = 137
      Height = 24
      Caption = #1055#1086#1074#1090#1086#1088#1085#1072#1103' '#1074#1099#1075#1088#1091#1079#1082#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = cxButton8Click
      Colors.Default = 14930628
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      Layout = blGlyphTop
      LookAndFeel.Kind = lfFlat
    end
    object cxButton9: TcxButton
      Left = 8
      Top = 209
      Width = 137
      Height = 24
      Caption = #1042#1099#1082#1083#1102#1095#1080#1090#1100
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 8
      OnClick = cxButton9Click
      Colors.Default = 14930628
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      Layout = blGlyphTop
      LookAndFeel.Kind = lfFlat
    end
    object cxButton33: TcxButton
      Left = 8
      Top = 177
      Width = 137
      Height = 24
      Caption = #1040#1085#1085#1091#1083'. '#1095#1077#1082#1080
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 7
      OnClick = cxButton33Click
      Colors.Default = 14930628
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      Layout = blGlyphTop
      LookAndFeel.Kind = lfFlat
    end
  end
  object cxButton34: TcxButton
    Left = 651
    Top = 7
    Width = 100
    Height = 30
    Caption = '1'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 20
    TabStop = False
    Visible = False
    WordWrap = True
    OnClick = cxButton34Click
    Colors.Default = 14864576
    Colors.Normal = 12621940
    Colors.Pressed = 10843723
    Layout = blGlyphTop
    LookAndFeel.Kind = lfFlat
  end
  object cxButton35: TcxButton
    Left = 759
    Top = 7
    Width = 100
    Height = 30
    Caption = '2'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 21
    TabStop = False
    Visible = False
    WordWrap = True
    OnClick = cxButton35Click
    Colors.Default = 11599871
    Colors.Normal = 11599871
    Colors.Pressed = 2424831
    Layout = blGlyphTop
    LookAndFeel.Kind = lfFlat
  end
  object cxButton36: TcxButton
    Left = 867
    Top = 7
    Width = 100
    Height = 30
    Caption = '3'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 22
    TabStop = False
    Visible = False
    WordWrap = True
    OnClick = cxButton36Click
    Colors.Default = 13434828
    Colors.Normal = 13434828
    Colors.Pressed = 6487906
    Layout = blGlyphTop
    LookAndFeel.Kind = lfFlat
  end
  object cxButton37: TcxButton
    Tag = 1
    Left = 12
    Top = 966
    Width = 105
    Height = 35
    Caption = '1'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 23
    TabStop = False
    Visible = False
    WordWrap = True
    OnClick = cxButton37Click
    Colors.Default = 14864576
    Colors.Normal = 12621940
    Colors.Pressed = 10843723
    Layout = blGlyphTop
    LookAndFeel.Kind = lfFlat
  end
  object cxButton38: TcxButton
    Tag = 2
    Left = 121
    Top = 966
    Width = 105
    Height = 35
    Caption = '2'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 24
    TabStop = False
    Visible = False
    WordWrap = True
    OnClick = cxButton38Click
    Colors.Default = 11599871
    Colors.Normal = 11599871
    Colors.Pressed = 2424831
    Layout = blGlyphTop
    LookAndFeel.Kind = lfFlat
  end
  object cxButton39: TcxButton
    Tag = 3
    Left = 230
    Top = 966
    Width = 105
    Height = 35
    Caption = '3'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 25
    TabStop = False
    Visible = False
    WordWrap = True
    OnClick = cxButton39Click
    Colors.Default = 13434828
    Colors.Normal = 13434828
    Colors.Pressed = 6487906
    Layout = blGlyphTop
    LookAndFeel.Kind = lfFlat
  end
  object cxButton40: TcxButton
    Tag = 4
    Left = 339
    Top = 966
    Width = 105
    Height = 35
    Caption = '4'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 26
    TabStop = False
    Visible = False
    WordWrap = True
    OnClick = cxButton40Click
    Colors.Default = 16777109
    Colors.Normal = 16777109
    Colors.Pressed = 16382208
    Layout = blGlyphTop
    LookAndFeel.Kind = lfFlat
  end
  object cxButton41: TcxButton
    Tag = 5
    Left = 448
    Top = 966
    Width = 105
    Height = 35
    Caption = '5'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 27
    TabStop = False
    Visible = False
    WordWrap = True
    OnClick = cxButton41Click
    Colors.Default = 11786751
    Colors.Normal = 11786751
    Colors.Pressed = 3381759
    Layout = blGlyphTop
    LookAndFeel.Kind = lfFlat
  end
  object cxButton42: TcxButton
    Tag = 6
    Left = 557
    Top = 966
    Width = 105
    Height = 35
    Caption = '6'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 28
    TabStop = False
    Visible = False
    WordWrap = True
    OnClick = cxButton42Click
    Colors.Default = 16766463
    Colors.Normal = 16766463
    Colors.Pressed = 16739071
    Layout = blGlyphTop
    LookAndFeel.Kind = lfFlat
  end
  object cxButton43: TcxButton
    Tag = 7
    Left = 666
    Top = 966
    Width = 105
    Height = 35
    Caption = '7'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 29
    TabStop = False
    Visible = False
    WordWrap = True
    OnClick = cxButton43Click
    Colors.Default = 12237532
    Colors.Normal = 12237532
    Colors.Pressed = 11119059
    Layout = blGlyphTop
    LookAndFeel.Kind = lfFlat
  end
  object cxButton44: TcxButton
    Tag = 8
    Left = 775
    Top = 966
    Width = 105
    Height = 35
    Caption = '8'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 30
    TabStop = False
    Visible = False
    WordWrap = True
    OnClick = cxButton44Click
    Colors.Default = 14869218
    Colors.Normal = 14869218
    Colors.Pressed = 12303291
    Layout = blGlyphTop
    LookAndFeel.Kind = lfFlat
  end
  object cxButton45: TcxButton
    Tag = 4
    Left = 975
    Top = 7
    Width = 100
    Height = 30
    Caption = '4'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 31
    TabStop = False
    Visible = False
    WordWrap = True
    OnClick = cxButton45Click
    Colors.Default = 16777109
    Colors.Normal = 16777109
    Colors.Pressed = 16382208
    Layout = blGlyphTop
    LookAndFeel.Kind = lfFlat
  end
  object cxEditStyleController1: TcxEditStyleController
    Style.BorderColor = clLime
    Style.BorderStyle = ebsSingle
    Style.LookAndFeel.Kind = lfUltraFlat
    StyleDisabled.LookAndFeel.Kind = lfUltraFlat
    StyleFocused.LookAndFeel.Kind = lfUltraFlat
    StyleHot.LookAndFeel.Kind = lfUltraFlat
    Left = 208
    Top = 169
  end
  object TimerStart: TTimer
    Enabled = False
    Interval = 2000
    OnTimer = TimerStartTimer
    Left = 354
    Top = 112
  end
  object am1: TActionManager
    Left = 156
    Top = 120
    StyleName = 'XP Style'
    object acBar: TAction
      Caption = 'acBar'
      ShortCut = 123
      SecondaryShortCuts.Strings = (
        'Ctrl+F12')
      OnExecute = acBarExecute
    end
    object acChangePers: TAction
      Caption = 'acChangePers'
      ShortCut = 27
      OnExecute = acChangePersExecute
    end
    object acCalcNal: TAction
      Caption = #1056#1072#1089#1095#1077#1090
      ShortCut = 116
      OnExecute = acCalcNalExecute
    end
    object acFindArt: TAction
      Caption = #1055#1086' '#1082#1086#1076#1091
      ShortCut = 113
      OnExecute = acFindArtExecute
    end
    object acQuantity: TAction
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100' '#1082#1086#1083'-'#1074#1086
      ShortCut = 16505
      SecondaryShortCuts.Strings = (
        'Ctrl+F11')
      OnExecute = acQuantityExecute
    end
    object acError: TAction
      Caption = #1054#1096#1080#1073#1082#1072
      ShortCut = 119
      OnExecute = acErrorExecute
    end
    object acAnnul: TAction
      Caption = #1040#1085#1085#1091#1083'.'
      ShortCut = 120
      OnExecute = acAnnulExecute
    end
    object acPersDisc: TAction
      Caption = 'acPersDisc'
      ShortCut = 122
      SecondaryShortCuts.Strings = (
        #1078
        ';'
        'c'
        'Ctrl+F11')
      OnExecute = acPersDiscExecute
    end
    object acManDisc: TAction
      Caption = 'acManDisc'
      ShortCut = 16497
      OnExecute = acManDiscExecute
    end
    object acUp: TAction
      Caption = 'acUp'
      ShortCut = 38
      OnExecute = acUpExecute
    end
    object acDown: TAction
      Caption = 'acDown'
      ShortCut = 40
      OnExecute = acDownExecute
    end
    object acDelPos: TAction
      Caption = #1059#1076#1083'. '#1087#1086#1079'.'
      ShortCut = 16430
      SecondaryShortCuts.Strings = (
        'Alt+F4')
      OnExecute = acDelPosExecute
    end
    object acPrice: TAction
      Caption = 'acPrice'
      ShortCut = 16503
      OnExecute = acPriceExecute
    end
    object acCashDriverOpen: TAction
      Caption = #1044#1071
      ShortCut = 112
      OnExecute = acCashDriverOpenExecute
    end
    object acRet: TAction
      Caption = #1042#1086#1079#1074#1088#1072#1090
      ShortCut = 117
      OnExecute = acRetExecute
    end
    object acCashRep: TAction
      Caption = 'acCashRep'
      ShortCut = 121
      OnExecute = acCashRepExecute
    end
    object acCopyCheck: TAction
      Caption = #1050#1086#1087#1080#1103' '#1095#1077#1082#1072
      ShortCut = 114
      SecondaryShortCuts.Strings = (
        'Alt+F5')
      OnExecute = acCopyCheckExecute
    end
    object acCalc: TAction
      Caption = #1050#1072#1083#1100#1082'.'
      ShortCut = 118
      SecondaryShortCuts.Strings = (
        'Ctrl+F6')
      OnExecute = acCalcExecute
    end
    object acPrintTCH: TAction
      Caption = #1055#1077#1095#1072#1090#1100' '#1058#1063
      ShortCut = 8305
      SecondaryShortCuts.Strings = (
        'Alt+F2')
      OnExecute = acPrintTCHExecute
    end
    object acExitProgram: TAction
      Caption = 'acExitProgram'
      OnExecute = acExitProgramExecute
    end
    object acGetSums: TAction
      Caption = 'acGetSums'
      ShortCut = 49234
      OnExecute = acGetSumsExecute
    end
    object acReservCh: TAction
      Caption = 'acReservCh'
      ShortCut = 32889
      OnExecute = acReservChExecute
    end
    object acSaveHK: TAction
      Caption = 'acSaveHK'
      ShortCut = 49227
      OnExecute = acSaveHKExecute
    end
    object acSetHK: TAction
      Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072' '#1075#1086#1088#1103#1095#1080#1093' '#1082#1083#1072#1074#1080#1096
      OnExecute = acSetHKExecute
    end
    object acTestUTM: TAction
      Caption = 'acTestUTM'
      ShortCut = 24661
      OnExecute = acTestUTMExecute
    end
    object acTestPrintUTM: TAction
      Caption = 'acTestPrintUTM'
      ShortCut = 24656
      OnExecute = acTestPrintUTMExecute
    end
    object acSaveVid: TAction
      Caption = 'acSaveVid'
      ShortCut = 24689
      OnExecute = acSaveVidExecute
    end
    object acRefreshHK: TAction
      Caption = 'acRefreshHK'
    end
    object acResetVid: TAction
      Caption = 'acResetVid'
      ShortCut = 24692
      OnExecute = acResetVidExecute
    end
    object acComProxy: TAction
      Caption = 'acComProxy'
      ShortCut = 32882
      OnExecute = acComProxyExecute
    end
  end
  object Timer2: TTimer
    Enabled = False
    OnTimer = Timer2Timer
    Left = 272
    Top = 128
  end
  object TimerScan: TTimer
    Interval = 200
    OnTimer = TimerScanTimer
    Left = 517
    Top = 112
  end
  object Timer3: TTimer
    Enabled = False
    Interval = 5000
    OnTimer = Timer3Timer
    Left = 404
    Top = 196
  end
  object TimerSaveScan: TTimer
    Enabled = False
    Interval = 100
    OnTimer = TimerSaveScanTimer
    Left = 443
    Top = 111
  end
  object devScaner: TComPort
    BaudRate = br9600
    Port = 'COM1'
    Parity.Bits = prNone
    StopBits = sbOneStopBit
    DataBits = dbEight
    Events = [evRxChar, evTxEmpty, evRxFlag, evRing, evBreak, evCTS, evDSR, evError, evRLSD, evRx80Full]
    FlowControl.OutCTSFlow = False
    FlowControl.OutDSRFlow = False
    FlowControl.ControlDTR = dtrDisable
    FlowControl.ControlRTS = rtsDisable
    FlowControl.XonXoffOut = False
    FlowControl.XonXoffIn = False
    StoredProps = [spBasic]
    TriggersOnRxChar = True
    OnRxChar = devScanerRxChar
    Left = 63
    Top = 187
  end
  object DevCustD: TComPort
    BaudRate = br9600
    Port = 'COM1'
    Parity.Bits = prNone
    StopBits = sbOneStopBit
    DataBits = dbEight
    Events = [evRxChar, evTxEmpty, evRxFlag, evRing, evBreak, evCTS, evDSR, evError, evRLSD, evRx80Full]
    FlowControl.OutCTSFlow = False
    FlowControl.OutDSRFlow = False
    FlowControl.ControlDTR = dtrDisable
    FlowControl.ControlRTS = rtsDisable
    FlowControl.XonXoffOut = False
    FlowControl.XonXoffIn = False
    StoredProps = [spBasic]
    TriggersOnRxChar = True
    Left = 63
    Top = 105
  end
end
