object fmTestVes: TfmTestVes
  Left = 303
  Top = 112
  BorderStyle = bsDialog
  Caption = #1050#1086#1085#1090#1088#1086#1083#1100' '#1074#1077#1089#1072
  ClientHeight = 308
  ClientWidth = 466
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label3: TLabel
    Left = 104
    Top = 16
    Width = 254
    Height = 24
    Alignment = taCenter
    Caption = #1042#1085#1080#1084#1072#1085#1080#1077' !!! '#1050#1086#1085#1090#1088#1086#1083#1100' '#1074#1077#1089#1072'.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 24
    Top = 44
    Width = 420
    Height = 24
    Caption = #1055#1086#1083#1086#1078#1080#1090#1077' '#1090#1086#1074#1072#1088' '#1085#1072' '#1074#1077#1089#1099' '#1080' '#1085#1072#1078#1084#1080#1090#1077' '#1042#1042#1054#1044' '#1064#1050'.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label1: TLabel
    Left = 40
    Top = 80
    Width = 389
    Height = 29
    Alignment = taCenter
    AutoSize = False
    Caption = #1042#1077#1089' - 100'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -24
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 416
    Top = 8
    Width = 38
    Height = 16
    Caption = 'Label4'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
  end
  object Edit1: TEdit
    Left = 292
    Top = 260
    Width = 37
    Height = 21
    TabOrder = 2
    Text = 'Edit1'
    OnKeyPress = Edit1KeyPress
  end
  object Panel1: TPanel
    Left = 0
    Top = 251
    Width = 466
    Height = 57
    Align = alBottom
    BevelInner = bvLowered
    Color = 14864576
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 24
      Top = 12
      Width = 105
      Height = 33
      Caption = #1050#1086#1085#1090#1088#1086#1083#1100' '#1074#1077#1089#1072
      TabOrder = 0
      OnClick = cxButton1Click
      OnKeyPress = cxButton1KeyPress
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 180
      Top = 12
      Width = 105
      Height = 33
      Caption = #1042#1079#1103#1090#1100' '#1074#1077#1089' '#1089' '#1074#1077#1089#1086#1074
      Default = True
      TabOrder = 1
      OnClick = cxButton2Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton3: TcxButton
      Left = 332
      Top = 12
      Width = 105
      Height = 33
      Caption = #1054#1090#1084#1077#1085#1072' '#1082#1086#1085#1090#1088#1086#1083#1103
      TabOrder = 2
      OnClick = cxButton3Click
      LookAndFeel.Kind = lfOffice11
    end
  end
  object GrTestVes: TcxGrid
    Left = 8
    Top = 120
    Width = 449
    Height = 125
    TabOrder = 1
    LookAndFeel.Kind = lfOffice11
    object ViTestVes: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dsteTestVes
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'Quant'
          Column = ViTestVesQuant
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'rSum'
          Column = ViTestVesrSum
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      object ViTestVesArticul: TcxGridDBColumn
        Caption = #1040#1088#1090#1080#1082#1091#1083
        DataBinding.FieldName = 'Articul'
        Width = 43
      end
      object ViTestVesBarCode: TcxGridDBColumn
        Caption = #1064#1050
        DataBinding.FieldName = 'BarCode'
        Width = 69
      end
      object ViTestVesName: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'Name'
        Width = 114
      end
      object ViTestVesQuant: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086
        DataBinding.FieldName = 'Quant'
      end
      object ViTestVesPrice: TcxGridDBColumn
        Caption = #1062#1077#1085#1072
        DataBinding.FieldName = 'Price'
      end
      object ViTestVesrSum: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072
        DataBinding.FieldName = 'rSum'
      end
    end
    object LeTestVes: TcxGridLevel
      GridView = ViTestVes
    end
  end
  object TimerVes: TTimer
    Enabled = False
    Interval = 250
    OnTimer = TimerVesTimer
    Left = 16
    Top = 76
  end
  object teTestVes: TdxMemData
    Indexes = <>
    SortOptions = []
    AfterPost = teTestVesAfterPost
    Left = 80
    Top = 144
    object teTestVesNum: TSmallintField
      FieldName = 'Num'
    end
    object teTestVesArticul: TStringField
      FieldName = 'Articul'
      Size = 10
    end
    object teTestVesBarCode: TStringField
      FieldName = 'BarCode'
    end
    object teTestVesName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object teTestVesQuant: TFloatField
      FieldName = 'Quant'
      DisplayFormat = '0.000'
    end
    object teTestVesPrice: TFloatField
      FieldName = 'Price'
      DisplayFormat = '0.00'
    end
    object teTestVesrSum: TFloatField
      FieldName = 'rSum'
      DisplayFormat = '0.00'
    end
  end
  object dsteTestVes: TDataSource
    DataSet = teTestVes
    Left = 80
    Top = 192
  end
  object amTestVes: TActionManager
    Left = 164
    Top = 152
    StyleName = 'XP Style'
    object acAddPos: TAction
      Caption = 'acAddPos'
      ShortCut = 107
      SecondaryShortCuts.Strings = (
        'Num +')
      OnExecute = acAddPosExecute
    end
    object acBar: TAction
      Caption = 'acBar'
      ShortCut = 123
      SecondaryShortCuts.Strings = (
        'Ctrl+B')
      OnExecute = acBarExecute
    end
  end
end
