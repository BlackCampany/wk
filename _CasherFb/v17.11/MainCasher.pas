unit MainCasher;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,         //    Event_RegEx(35,Nums.iCheckNum,0,0,'','',0,0,0,DiscProc,DiscSum);
  Dialogs, cxMemo, StdCtrls, cxControls, cxContainer, cxEdit, cxTextEdit,
  cxMaskEdit, cxDropDownEdit, cxCalc, cxCurrencyEdit, cxStyles,
  cxCustomData, cxGraphics, cxFilter, cxData, DB, cxDBData, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,

  httpsend, synautil, nativexml, IniFiles,

  cxGridDBTableView, cxGrid, ExtCtrls, RXClock, ClipView,
  cxLookAndFeelPainters, cxButtons, ActnList, XPStyleActnCtrls, ActnMan,
  VaClasses, VaComm, cxDataStorage, Menus, dxfLabel,ShellApi, dxmdaset,
  CPort;   //, VaSystem;

const  sVer:String ='17.57 (P)'; //2017-03-24

//const  sVer:String ='17.11'; //2017-03-23

type
  TfmMainCasher = class(TForm)
    RxClock1: TRxClock;
    Panel1: TPanel;
    Label8: TLabel;
    Label9: TLabel;
    Panel2: TPanel;
    Label13: TLabel;
    Panel3: TPanel;
    Label7: TLabel;
    CurrencyEdit3: TcxCurrencyEdit;
    TextEdit3: TcxTextEdit;
    Label12: TLabel;
    GridCh: TcxGrid;
    ViewCh: TcxGridDBTableView;
    LevelCh: TcxGridLevel;
    Memo2: TcxMemo;
    cxEditStyleController1: TcxEditStyleController;
    Panel4: TPanel;
    Label1: TLabel;
    TextEdit1: TcxTextEdit;
    Label2: TLabel;
    TextEdit2: TcxTextEdit;
    Label3: TLabel;
    Memo1: TcxMemo;
    Label4: TLabel;
    CalcEdit1: TcxCalcEdit;
    Label5: TLabel;
    CurrencyEdit1: TcxCurrencyEdit;
    Label6: TLabel;
    CurrencyEdit2: TcxCurrencyEdit;
    TimerStart: TTimer;
    Edit1: TcxTextEdit;
    Label14: TLabel;
    ViewChCASHNUM: TcxGridDBColumn;
    ViewChNUMPOS: TcxGridDBColumn;
    ViewChARTICUL: TcxGridDBColumn;
    ViewChBAR: TcxGridDBColumn;
    ViewChNAME: TcxGridDBColumn;
    ViewChQUANT: TcxGridDBColumn;
    ViewChPRICE: TcxGridDBColumn;
    ViewChDPROC: TcxGridDBColumn;
    ViewChDSUM: TcxGridDBColumn;
    ViewChRSUM: TcxGridDBColumn;
    Label10: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Timer2: TTimer;
    Label18: TLabel;
    Label19: TLabel;
    Edit2: TcxTextEdit;
    Label17: TLabel;
    TimerScan: TTimer;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Panel5: TPanel;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    cxButton1: TcxButton;
    Label24: TLabel;
    cxButton4: TcxButton;
    cxButton5: TcxButton;
    dxfLabel1: TdxfLabel;
    cxButton6: TcxButton;
    cxButton7: TcxButton;
    cxButton8: TcxButton;
    Timer3: TTimer;
    Label11: TLabel;
    cxButton9: TcxButton;
    Label26: TLabel;
    am1: TActionManager;
    acBar: TAction;
    acChangePers: TAction;
    acCalcNal: TAction;
    acFindArt: TAction;
    acQuantity: TAction;
    acError: TAction;
    acAnnul: TAction;
    acPersDisc: TAction;
    acManDisc: TAction;
    acUp: TAction;
    acDown: TAction;
    acDelPos: TAction;
    acPrice: TAction;
    acCashDriverOpen: TAction;
    acRet: TAction;
    acCashRep: TAction;
    acCopyCheck: TAction;
    acCalc: TAction;
    acPrintTCH: TAction;
    acExitProgram: TAction;
    acGetSums: TAction;
    acReservCh: TAction;
    Label27: TLabel;
    acSaveHK: TAction;
    cxButton10: TcxButton;
    cxButton11: TcxButton;
    Panel6: TPanel;
    cxCalcEdit1: TcxCalcEdit;
    cxButton12: TcxButton;
    cxButton13: TcxButton;
    cxButton14: TcxButton;
    cxButton15: TcxButton;
    cxButton16: TcxButton;
    cxButton17: TcxButton;
    cxButton18: TcxButton;
    cxButton19: TcxButton;
    cxButton20: TcxButton;
    cxButton21: TcxButton;
    cxButton25: TcxButton;
    GridHK: TcxGrid;
    ViewHK: TcxGridTableView;
    LevelHK: TcxGridLevel;
    GridHK1: TcxGrid;
    ViewHK1: TcxGridTableView;
    LevelHK1: TcxGridLevel;
    acSetHK: TAction;
    cxButton22: TcxButton;
    cxButton23: TcxButton;
    cxButton24: TcxButton;
    cxButton26: TcxButton;
    cxButton27: TcxButton;
    cxButton28: TcxButton;
    cxButton29: TcxButton;
    cxButton30: TcxButton;
    cxButton31: TcxButton;
    cxButton32: TcxButton;
    cxButton33: TcxButton;
    acTestUTM: TAction;
    acTestPrintUTM: TAction;
    acSaveVid: TAction;
    cxButton34: TcxButton;
    cxButton35: TcxButton;
    cxButton36: TcxButton;
    cxButton37: TcxButton;
    cxButton38: TcxButton;
    cxButton39: TcxButton;
    cxButton40: TcxButton;
    cxButton41: TcxButton;
    cxButton42: TcxButton;
    cxButton43: TcxButton;
    cxButton44: TcxButton;
    acRefreshHK: TAction;
    cxButton45: TcxButton;
    acResetVid: TAction;
    acComProxy: TAction;
    TimerSaveScan: TTimer;
    devScaner: TComPort;
    DevCustD: TComPort;
    procedure FormCreate(Sender: TObject);
    procedure TimerStartTimer(Sender: TObject);
    procedure acBarExecute(Sender: TObject);
    procedure Edit1KeyPress(Sender: TObject; var Key: Char);
    procedure acChangePersExecute(Sender: TObject);
    procedure TextEdit3KeyPress(Sender: TObject; var Key: Char);
    procedure acCalcNalExecute(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
    procedure acFindArtExecute(Sender: TObject);
    procedure acQuantityExecute(Sender: TObject);
    procedure acErrorExecute(Sender: TObject);
    procedure acAnnulExecute(Sender: TObject);
    procedure Edit2KeyPress(Sender: TObject; var Key: Char);
    procedure acPersDiscExecute(Sender: TObject);
    procedure acManDiscExecute(Sender: TObject);
    procedure acUpExecute(Sender: TObject);
    procedure acDownExecute(Sender: TObject);
    procedure acDelPosExecute(Sender: TObject);
    procedure acPriceExecute(Sender: TObject);
    procedure devScanerRxChar(Sender: TObject; Count: Integer);
    procedure TimerScanTimer(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acCashDriverOpenExecute(Sender: TObject);
    procedure acRetExecute(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure acCashRepExecute(Sender: TObject);
    procedure acCopyCheckExecute(Sender: TObject);
    procedure acCalcExecute(Sender: TObject);
    procedure acPrintTCHExecute(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure RxClock1Click(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure acExitProgramExecute(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
    procedure cxButton7Click(Sender: TObject);
    procedure cxButton8Click(Sender: TObject);
    procedure acBloskExitExecute(Sender: TObject);
    procedure Timer3Timer(Sender: TObject);
    procedure FormCanResize(Sender: TObject; var NewWidth,
      NewHeight: Integer; var Resize: Boolean);
    procedure cxButton9Click(Sender: TObject);
    procedure acGetSumsExecute(Sender: TObject);
    procedure acReservChExecute(Sender: TObject);
    procedure acSaveHKExecute(Sender: TObject);
    procedure acSetHKExecute(Sender: TObject);
    procedure ViewHKCellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure ViewHK1CellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxButton21Click(Sender: TObject);
    procedure cxButton12Click(Sender: TObject);
    procedure cxButton13Click(Sender: TObject);
    procedure cxButton14Click(Sender: TObject);
    procedure cxButton15Click(Sender: TObject);
    procedure cxButton16Click(Sender: TObject);
    procedure cxButton17Click(Sender: TObject);
    procedure cxButton18Click(Sender: TObject);
    procedure cxButton19Click(Sender: TObject);
    procedure cxButton20Click(Sender: TObject);
    procedure cxButton25Click(Sender: TObject);
    procedure cxButton32Click(Sender: TObject);
    procedure cxButton33Click(Sender: TObject);
    procedure acTestUTMExecute(Sender: TObject);
    procedure acTestPrintUTMExecute(Sender: TObject);
    procedure acSaveVidExecute(Sender: TObject);
    procedure cxButton37Click(Sender: TObject);
    procedure cxButton38Click(Sender: TObject);
    procedure cxButton39Click(Sender: TObject);
    procedure cxButton40Click(Sender: TObject);
    procedure cxButton41Click(Sender: TObject);
    procedure cxButton42Click(Sender: TObject);
    procedure cxButton43Click(Sender: TObject);
    procedure cxButton44Click(Sender: TObject);
    procedure cxButton34Click(Sender: TObject);
    procedure cxButton35Click(Sender: TObject);
    procedure cxButton36Click(Sender: TObject);
    procedure cxButton45Click(Sender: TObject);
    procedure acResetVidExecute(Sender: TObject);
    procedure acComProxyExecute(Sender: TObject);
    procedure TimerSaveScanTimer(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Recievedata(iZ,iD:INteger);
    procedure WrMess(Strwk_: string);
    procedure prSetShortCut(iKeyType:INteger);
    Procedure CreateHotList;
    Procedure RefreshHotList;
    Procedure CreateHotList1;
    Procedure RefreshHotList1;
    Procedure prAddHKPos(s:Char);
    Procedure prSetVisibleAll(bV:Boolean);
    procedure prSaveVid;
    procedure prResetVid;
  end;

procedure prClearCheck1;
Function prAddPosSel(Param,TestVes:Integer):Boolean;
Procedure ClearPos;
procedure ClearSelPos;
Procedure ResetPos;
Procedure CalcSum;
Procedure prSaveAndCloseCheck(rPay:Real);
Procedure ClearCheck;
Procedure WriteStatus;
Procedure ToCustomDisp(Str1,Str2:String);
Procedure PrintNFDoc(iType:SmallInt);
Function FindBar(sBar:ShortString):Boolean;
Procedure TestFp(StrOp:String);
Procedure PrintF(iType:Integer;rSum,rSumR:Real); //0-������� 1-��������
procedure NumLock(State:boolean);
procedure prTestVes;

Procedure WriteStatusFN;


type
   TCli = record
              ClientRegId,FullName,ShortName,INN,KPP,Country,RegionCode,description:string;
          end;

var
  fmMainCasher: TfmMainCasher;
  bErrCash:Boolean;
  hToken: THandle;
  tkp: TTokenPrivileges;
  tkpo: TTokenPrivileges;
  zero: DWORD;
  bStopAddPos:Boolean = False;
  ArBar:array[1..200] of string;
  iNumBar: Integer = 1;


const
  SE_SHUTDOWN_NAME='SeShutdownPrivilege'; // Borland forgot this declaration

implementation

uses Un1, Dm, uPre, CashEnd, Calc, CardsFind, u2fdk, UnCash, BarMessage,
  Attention, Calcul, UnInOutM, EnterPassw, BnSber, St, uDBTr, Trans2,
  uDBFB, Shutd, RetCheck, ReservedCh, UnPrizma, Bonus, AutoUpd,
  AnnulsChecks, MessAlco, MessOff, TestVes, Vtb24un, uTermElisey, Ping,
  dbSql, GetAMark;

{$R *.dfm}

Function GetNumBar:INteger;
begin
  if iNumBar<200 then begin inc(iNumBar); Result:=iNumBar; end
  else begin iNumBar:=1; Result:=1; end;
end;

Procedure WriteStatusFN;
begin
  with fmMainCasher do
  begin
    if CommonSet.TypeFis='pirit' then
    begin
      Label23.Caption:=Nums.sFN;
      if Nums.FNWARN>0 then
      begin
        Label23.Font.Color:=clRed;   //$00EBD7C5
      end else
      begin
        Label23.Font.Color:=$00EBD7C5;   //$00EBD7C5
      end;

    end else
    begin
      Label23.Caption:='���.(����) '+IntToStr(Nums.ZYet);
    end;
  end;
end;

procedure TfmMainCasher.prResetVid;
var VidXml: TNativeXml;
    nodeRoot,nodeVid,nodeCMPS,nodePos:TXmlNode;
    i,iVid,iW,iH,l,iT,iL:Integer;
    cmp: TComponent;
    sType,sCaption,sName:string;
    ColQ,ColW,RowQ,RowH,TextL,FontSize,FontStyle:Integer;
begin
  //������ ��� �� XML �����
  if FileExists(CurDir+'Vids.xml') then
  begin
    try
      iVid:=-1;
      iW:=800; iH:=600;
      VidXml := TNativeXml.Create(nil);
      VidXml.XmlFormat := xfReadable;
      VidXml.LoadFromFile(CurDir+'Vids.xml');
      nodeRoot:=VidXml.Root;
      for i:=0 to nodeRoot.NodeCount-1 do
      begin
        nodeVid:=nodeRoot[i];
        if Assigned(nodeVid.NodeByName('TYPEV')) then iVid:=StrToIntDef(nodeVid.NodeByName('TYPEV').Value,-1);
        if Assigned(nodeVid.NodeByName('Width')) then iW:=StrToIntDef(nodeVid.NodeByName('Width').Value,-1);
        if Assigned(nodeVid.NodeByName('Height')) then iH:=StrToIntDef(nodeVid.NodeByName('Height').Value,-1);

        if iVid=Vid.Vid then
        begin //��� ��� ���
          if (iW>0) and (iH>0) then  begin  Vid.Width:=iW;  Vid.Height:=iH; end;

          if Assigned(nodeVid.NodeByName('Components')) then
          begin
            nodeCMPS:=nodeVid.NodeByName('Components');
            for l:=0 to nodeCMPS.NodeCount-1 do
            begin
              nodePos:=nodeCMPS[l];
              if Pos('OBJ',nodePos.Name)>0 then
              begin
                iW:=0; iH:=0; iT:=0; iL:=0; sType:=''; sCaption:=''; sName:='';  //�����
                ColQ:=0; ColW:=0; RowQ:=0; RowH:=0; TextL:=0; FontSize:=0; FontStyle:=0; //�� ������


                if Assigned(nodePos.NodeByName('Type')) then sType:=nodePos.NodeByName('Type').Value;
                if Assigned(nodePos.NodeByName('Name')) then sName:=nodePos.NodeByName('Name').Value;
                if Assigned(nodePos.NodeByName('Caption')) then sCaption:=UTF8Decode(nodePos.NodeByName('Caption').Value);

                if Assigned(nodePos.NodeByName('Top')) then iT:=StrToIntDef(nodePos.NodeByName('Top').Value,-1);
                if Assigned(nodePos.NodeByName('Left')) then iL:=StrToIntDef(nodePos.NodeByName('Left').Value,-1);
                if Assigned(nodePos.NodeByName('Width')) then iW:=StrToIntDef(nodePos.NodeByName('Width').Value,-1);
                if Assigned(nodePos.NodeByName('Height')) then iH:=StrToIntDef(nodePos.NodeByName('Height').Value,-1);


                cmp := FindComponent(sName);
                if cmp<>nil then
                begin
                  if sType='LBL' then
                  begin
                    if (cmp is TLabel) then
                    begin
                      if iW>0 then (cmp as TLabel).Width:=iW;
                      if iH>0 then (cmp as TLabel).Height:=iH;
                      if iT>0 then (cmp as TLabel).Top:=iT;
                      if iL>0 then (cmp as TLabel).Left:=iL;
                    end;
                  end;

                  if sType='CLOCK' then
                  begin
                    if (cmp is TRxClock) then
                    begin
                      if iW>0 then (cmp as TRxClock).Width:=iW;
                      if iH>0 then (cmp as TRxClock).Height:=iH;
                      if iT>0 then (cmp as TRxClock).Top:=iT;
                      if iL>0 then (cmp as TRxClock).Left:=iL;
                    end;
                  end;

                  if sType='PANEL' then
                  begin
                    if (cmp is TPanel) then
                    begin
                      if iW>0 then (cmp as TPanel).Width:=iW;
                      if iH>0 then (cmp as TPanel).Height:=iH;
                      if iT>0 then (cmp as TPanel).Top:=iT;
                      if iL>0 then (cmp as TPanel).Left:=iL;
                    end;
                  end;

                  if sType='BTN' then
                  begin
                    if (cmp is TcxButton) then
                    begin
                      if iW>0 then (cmp as TcxButton).Width:=iW;
                      if iH>0 then (cmp as TcxButton).Height:=iH;
                      if iT>0 then (cmp as TcxButton).Top:=iT;
                      if iL>0 then (cmp as TcxButton).Left:=iL;
                      if sCaption>'' then (cmp as TcxButton).Caption:=sCaption;
                    end;
                  end;

                  if sType='GRID' then
                  begin
                    if Assigned(nodePos.NodeByName('ColQuant')) then ColQ:=StrToIntDef(nodePos.NodeByName('ColQuant').Value,-1);
                    if Assigned(nodePos.NodeByName('ColWidth')) then ColW:=StrToIntDef(nodePos.NodeByName('ColWidth').Value,-1);
                    if Assigned(nodePos.NodeByName('RowQuant')) then RowQ:=StrToIntDef(nodePos.NodeByName('RowQuant').Value,-1);
                    if Assigned(nodePos.NodeByName('RowHeight')) then RowH:=StrToIntDef(nodePos.NodeByName('RowHeight').Value,-1);
                    if Assigned(nodePos.NodeByName('TextLen')) then TextL:=StrToIntDef(nodePos.NodeByName('TextLen').Value,-1);
                    if Assigned(nodePos.NodeByName('FontSize')) then FontSize:=StrToIntDef(nodePos.NodeByName('FontSize').Value,-1);
                    if Assigned(nodePos.NodeByName('FontStyle')) then FontStyle:=StrToIntDef(nodePos.NodeByName('FontStyle').Value,-1);

//  Vid.GrColQuant:=10; Vid.GrColWidth:=107; Vid.GrRowQuant:=8; Vid.GrRowHeight:=45;  Vid.GrTextLen:=14;  Vid.GrFontSize:=8;  Vid.GrFontStyle:=0;
//  Vid.Gr1ColQuant:=4; Vid.Gr1ColWidth:=107; Vid.Gr1RowQuant:=12; Vid.Gr1RowHeight:=45; Vid.Gr1TextLen:=14; Vid.Gr1FontSize:=8; Vid.Gr1FontStyle:=0;

                    if iW>0 then (cmp as TcxGrid).Width:=iW;
                    if iH>0 then (cmp as TcxGrid).Height:=iH;
                    if iT>0 then (cmp as TcxGrid).Top:=iT;
                    if iL>0 then (cmp as TcxGrid).Left:=iL;

                    if sName='GridHK' then
                    begin
                      if ColQ>0 then Vid.GrColQuant:=ColQ;
                      if ColW>0 then Vid.GrColWidth:=ColW;
                      if RowQ>0 then Vid.GrRowQuant:=RowQ;
                      if RowH>0 then Vid.GrRowHeight:=RowH;
                      if TextL>0 then Vid.GrTextLen:=TextL;
                      if FontSize>0 then Vid.GrFontSize:=FontSize;
                      if FontStyle>=0 then Vid.GrFontStyle:=FontStyle;

//                      CreateHotList;
                    end;

                    if sName='GridHK1' then
                    begin
                      if ColQ>0 then Vid.Gr1ColQuant:=ColQ;
                      if ColW>0 then Vid.Gr1ColWidth:=ColW;
                      if RowQ>0 then Vid.Gr1RowQuant:=RowQ;
                      if RowH>0 then Vid.Gr1RowHeight:=RowH;
                      if TextL>0 then Vid.Gr1TextLen:=TextL;
                      if FontSize>0 then Vid.Gr1FontSize:=FontSize;
                      if FontStyle>=0 then Vid.Gr1FontStyle:=FontStyle;

//                      CreateHotList1;
                    end;
                  end;
                end;
              end;
            end;
          end;
        end;
      end;

    finally
      VidXml.Free;
    end;
  end;
end;

procedure TfmMainCasher.prSaveVid;
var VidXml: TNativeXml;
    Comps: TXmlNode;
    i:Integer;
begin  //��������� ��� � XML ����
  try
    VidXml := TNativeXml.Create(nil);
    VidXml.XmlFormat := xfReadable;

    VidXml.CreateName('VIDS');
    VidXml.Root.NodeNew('VID');
    VidXml.Root.NodeByName('VID').NodeNew('TYPEV').Value:=Its(Vid.Vid);
    VidXml.Root.NodeByName('VID').NodeNew('Width').Value:=Its(fmMainCasher.Width);
    VidXml.Root.NodeByName('VID').NodeNew('Height').Value:=Its(fmMainCasher.Height);
    VidXml.Root.NodeByName('VID').NodeNew('Components');

    for i:=0 to ComponentCount-1 do
    begin
      if (Components[i] is TcxButton) then  //��� ������
      begin
        Comps:=VidXml.Root.NodeByName('VID').NodeByName('Components').NodeNew('OBJ');
        Comps.NodeNew('Type').Value:='BTN';
        Comps.NodeNew('Name').Value:=(Components[i] as TcxButton).Name;
        Comps.NodeNew('Caption').Value:=UTF8Encode((Components[i] as TcxButton).Caption);
        Comps.NodeNew('Top').Value:=Its((Components[i] as TcxButton).Top);
        Comps.NodeNew('Left').Value:=Its((Components[i] as TcxButton).Left);
//        Comps.NodeNew('Visible').Value:=bts((Components[i] as TcxButton).Visible);
        Comps.NodeNew('Width').Value:=Its((Components[i] as TcxButton).Width);
        Comps.NodeNew('Height').Value:=Its((Components[i] as TcxButton).Height);
      end;

      if (Components[i] is TcxGrid) then  //��� �����
      begin
        Comps:=VidXml.Root.NodeByName('VID').NodeByName('Components').NodeNew('OBJ');
        Comps.NodeNew('Type').Value:='GRID';
        Comps.NodeNew('Name').Value:=(Components[i] as TcxGrid).Name;
        Comps.NodeNew('Top').Value:=Its((Components[i] as TcxGrid).Top);
        Comps.NodeNew('Left').Value:=Its((Components[i] as TcxGrid).Left);
//        Comps.NodeNew('Visible').Value:=bts((Components[i] as TcxGrid).Visible);
        Comps.NodeNew('Width').Value:=Its((Components[i] as TcxGrid).Width);
        Comps.NodeNew('Height').Value:=Its((Components[i] as TcxGrid).Height);
        Comps.NodeNew('ColQuant').Value:=Its(0);
        Comps.NodeNew('ColWidth').Value:=Its(0);
        Comps.NodeNew('RowQuant').Value:=Its(0);
        Comps.NodeNew('RowHeight').Value:=Its(0);
        Comps.NodeNew('TextLen').Value:=Its(0);
        Comps.NodeNew('FontSize').Value:=Its(0);
        Comps.NodeNew('FontStyle').Value:=Its(0);
      end;

      if (Components[i] is TPanel) then  //��� ������
      begin
        Comps:=VidXml.Root.NodeByName('VID').NodeByName('Components').NodeNew('OBJ');
        Comps.NodeNew('Type').Value:='PANEL';
        Comps.NodeNew('Name').Value:=(Components[i] as TPanel).Name;
        Comps.NodeNew('Top').Value:=Its((Components[i] as TPanel).Top);
        Comps.NodeNew('Left').Value:=Its((Components[i] as TPanel).Left);
//        Comps.NodeNew('Visible').Value:=bts((Components[i] as TPanel).Visible);
        Comps.NodeNew('Width').Value:=Its((Components[i] as TPanel).Width);
        Comps.NodeNew('Height').Value:=Its((Components[i] as TPanel).Height);
      end;

      if (Components[i] is TLabel) then  //��� �������
      begin
        Comps:=VidXml.Root.NodeByName('VID').NodeByName('Components').NodeNew('OBJ');
        Comps.NodeNew('Type').Value:='LBL';
        Comps.NodeNew('Name').Value:=(Components[i] as TLabel).Name;
//        Comps.NodeNew('Caption').Value:=(Components[i] as TLabel).Caption;
        Comps.NodeNew('Top').Value:=Its((Components[i] as TLabel).Top);
        Comps.NodeNew('Left').Value:=Its((Components[i] as TLabel).Left);
//        Comps.NodeNew('Visible').Value:=bts((Components[i] as TLabel).Visible);
        Comps.NodeNew('Width').Value:=Its((Components[i] as TLabel).Width);
        Comps.NodeNew('Height').Value:=Its((Components[i] as TLabel).Height);
      end;

      if (Components[i] is TRxClock) then  //��� ����
      begin
        Comps:=VidXml.Root.NodeByName('VID').NodeByName('Components').NodeNew('OBJ');
        Comps.NodeNew('Type').Value:='CLOCK';
        Comps.NodeNew('Name').Value:=(Components[i] as TRxClock).Name;
        Comps.NodeNew('Top').Value:=Its((Components[i] as TRxClock).Top);
        Comps.NodeNew('Left').Value:=Its((Components[i] as TRxClock).Left);
//        Comps.NodeNew('Visible').Value:=bts((Components[i] as TRxClock).Visible);
        Comps.NodeNew('Width').Value:=Its((Components[i] as TRxClock).Width);
        Comps.NodeNew('Height').Value:=Its((Components[i] as TRxClock).Height);
      end;
    end;

    VidXml.SaveToFile(CurDir+'Vids.xml');

  finally
    VidXml.Free;
  end;
end;

procedure prTestVes;
Var iArt:INteger;
    sBar:String;
    bVes:Boolean;
begin
  with dmC do
  begin
    sScan:='';
    sBar:=SelPos.BAR;
    bVes:=False;
    if Length(sBar)>=7 then
      if sBar[1]='2' then
      begin
        if (sBar[2]='1')or(sBar[2]='2')or(sBar[2]='3') then bVes:=True; //�� ����� ������� �����
      end;

    if (bScale)and(bVes) then
    begin
      iArt:=StrToINtDef(SelPos.Articul,0);
      if (iArt>0) then
      begin
        if teVesT.Locate('Articul',iArt,[]) then
        begin //��� ����� ��� �������� ����� ��������������
          fmMainCasher.Memo2.Lines.Add('������� - '+its(iArt)+' �������� ����.'); delay(10);
          with fmTestVes do
          begin
            CloseTe(teTestVes);  //����� �������� � ������� , �� ��������� ��

            teTestVes.Append;
            teTestVesNum.AsInteger:=1;
            teTestVesArticul.AsInteger:=iArt;
            teTestVesBarCode.AsString:=SelPos.Bar;
            teTestVesName.AsString:=SelPos.Name;
            teTestVesQuant.AsFloat:=SelPos.Quant;
            teTestVesPrice.AsFloat:=SelPos.Price;
            teTestVesrSum.AsFloat:=SelPos.Quant*SelPos.Price;
            teTestVes.Post;

            GrTestVes.Visible:=True;
          end;
          fmTestVes.Tag:=RoundEx(SelPos.Quant*1000);
          fmTestVes.ShowModal;
          if fmTestVes.ModalResult=mrOk then
          begin
            with fmTestVes do
            begin
              CloseTe(teTestVes1);

              teTestVes.First;
              while not teTestVes.eof do
              begin
                teTestVes1.Append;
                teTestVes1Num.AsInteger:=teTestVesNum.AsInteger;
                teTestVes1Articul.AsInteger:=teTestVesArticul.AsInteger;
                teTestVes1BarCode.AsString:=teTestVesBarCode.AsString;
                teTestVes1Name.AsString:=teTestVesName.AsString;
                teTestVes1Quant.AsFloat:=teTestVesQuant.AsFloat;
                teTestVes1Price.AsFloat:=teTestVesPrice.AsFloat;
                teTestVes1rSum.AsFloat:=teTestVesrSum.AsFloat;
                teTestVes1.Post;

                teTestVes.Next;
              end;

              teTestVes1.First;
              while not teTestVes1.eof do
              begin
                if (teTestVes1Quant.AsFloat>0.001) and (teTestVes1rSum.AsFloat>0.001) then
                begin
                  SelPos.ARTICUL:=teTestVes1Articul.AsString;
                  SelPos.NAME:=teTestVes1Name.AsString;
                  SelPos.QUANT:=teTestVes1Quant.AsFloat;
                  SelPos.PRICE:=teTestVes1Price.AsFloat;
                  SelPos.DPROC:=0;
                  SelPos.DSUM:=0;
                  SelPos.DEPART:=CommonSet.DepartId;
                  SelPos.BAR:=teTestVes1BarCode.AsString;
                  SelPos.EdIzm:='��.';
                 
                end;

                prAddPosSel(0,0);

                teTestVes1.Next;
              end;

              ClearSelPos;
              teTestVes1.Close;
            end;
          end;
        end;
      end;
    end;
  end;
end;

Procedure TfmMainCasher.prSetVisibleAll(bV:Boolean);
Var StrP:String;
begin
  if Vid.Vid=2 then //���� ������ ������������
  begin
    cxButton6.Visible:=bV;    //�������
    cxButton10.Visible:=bV;   //������
    cxButton11.Visible:=bV;   //���-��

    cxButton34.Visible:=bV;
    cxButton35.Visible:=bV;
    cxButton36.Visible:=bV;
    cxButton37.Visible:=bV;
    cxButton38.Visible:=bV;
    cxButton39.Visible:=bV;
    cxButton40.Visible:=bV;
    cxButton41.Visible:=bV;
    cxButton42.Visible:=bV;
    cxButton43.Visible:=bV;
    cxButton44.Visible:=bV;
    cxButton45.Visible:=bV;

    if bV then
    begin
      GridHK1.Visible:=True;
      GridHK.Visible:=True;

      StrP:=AnsiUpperCase(Person.Name);

      if pos('�����',StrP)>0 then
      begin
        Panel6.Visible:=True;

        acCalcNal.Visible:=True;
        acFindArt.Visible:=True;
        acQuantity.Visible:=True;
        acError.Visible:=True;
        acAnnul.Visible:=True;
        acPersDisc.Visible:=True;
        acDelPos.Visible:=True;
        acCashDriverOpen.Visible:=True;
        acRet.Visible:=True;
        acCashRep.Visible:=True;
        acCopyCheck.Visible:=True;
        acPrintTCH.Visible:=True;
        acExitProgram.Visible:=True;
        acSaveHK.Visible:=True;
        acCalc.Visible:=True;
        acSetHK.Visible:=True;

      end else
      begin
        Panel6.Visible:=True;

        acCalcNal.Visible:=True;
        acFindArt.Visible:=True;
        acQuantity.Visible:=True;
        acError.Visible:=True;

        acAnnul.Visible:=False;
        acPersDisc.Visible:=True;

        acDelPos.Visible:=True;

        acCashDriverOpen.Visible:=True;
        acRet.Visible:=False;
        acCashRep.Visible:=False;
        acCopyCheck.Visible:=True;
        acPrintTCH.Visible:=True;
        acExitProgram.Visible:=False;

        acSaveHK.Visible:=False;
        acCalc.Visible:=True;

        acSetHK.Visible:=False;
      end;
    end else
    begin
      Panel6.Visible:=False;

      GridHK1.Visible:=False;
      GridHK.Visible:=False;
      acCalcNal.Visible:=False;
      acFindArt.Visible:=False;
      acQuantity.Visible:=False;
      acError.Visible:=False;
      acAnnul.Visible:=False;
      acPersDisc.Visible:=False;
      acDelPos.Visible:=False;
      acCashDriverOpen.Visible:=False;
      acRet.Visible:=False;
      acCashRep.Visible:=False;
      acCopyCheck.Visible:=False;
      acPrintTCH.Visible:=False;
      acExitProgram.Visible:=False;
      acSaveHK.Visible:=False;
      acCalc.Visible:=False;
      acSetHK.Visible:=False;

    end;
  end else
  begin
    cxButton6.Visible:=False;  //�������
    cxButton10.Visible:=False; //������
    cxButton11.Visible:=False; //���-��
  end;
end;

Procedure TfmMainCasher.prAddHKPos(s:Char);
begin
  if Panel6.Tag>0 then
  begin
    if cxCalcEdit1.Value=0 then bFirst:=True;

    if pos(',',cxCalcEdit1.Text)=0 then
    begin
      if length(cxCalcEdit1.Text)<5 then cxCalcEdit1.EditValue:=cxCalcEdit1.EditValue*10+StrToIntDef(s,0)
    end
    else
    begin
      if length(cxCalcEdit1.Text)<5 then cxCalcEdit1.Text:=cxCalcEdit1.Text+s;
    end;

    if bFirst then begin cxCalcEdit1.Text:=s; bFirst:=False; end;

    cxButton14.SetFocus;
  end;
  if Panel6.Tag=0 then
  begin
    TextEdit3.Text:=TextEdit3.Text+S;
  end;
end;


Procedure TfmMainCasher.CreateHotList1;
Var AColumn:TcxGridColumn;
    i:INteger;
    MaxCol:Integer;
begin
  if Vid.Vid<>2 then Exit;
  try
    try
      MaxCol:=Vid.Gr1ColQuant;

      ViewHK1.ClearItems;
      ViewHK1.OptionsView.DataRowHeight:=Vid.Gr1RowHeight;

      for I := 0 to MaxCol-1 do
      begin
        AColumn := ViewHK1.CreateColumn;
        AColumn.Caption := IntToStr(i);
        AColumn.Name := 'h'+IntToStr(i);
        AColumn.DataBinding.ValueType := 'String';

        AColumn.Styles.Content:=nil;

        dmC.cxStyle31.Font.Size:=Vid.Gr1FontSize;
        if Vid.Gr1FontStyle=1 then dmC.cxStyle31.Font.Style := [fsBold] else  dmC.cxStyle31.Font.Style := [];

        AColumn.Styles.Content:=dmC.cxStyle31;
        AColumn.Width:=Vid.Gr1ColWidth; //���� 100

        AColumn.Styles.Content.Font.Size:=Vid.Gr1FontSize;
        if Vid.Gr1FontStyle=1 then AColumn.Styles.Content.Font.Style := [fsBold] else  AColumn.Styles.Content.Font.Style := [];
      end;
    except
    end;

    RefreshHotList1;
  except
  end;
end;

//  Vid.Gr1ColQuant:=4; Vid.Gr1ColWidth:=107; Vid.Gr1RowQuant:=12; Vid.Gr1RowHeight:=45; Vid.Gr1TextLen:=14; Vid.Gr1FontSize:=8; Vid.Gr1FontStyle:=0;

Procedure TfmMainCasher.RefreshHotList1;
Var i,j:INteger;
    Str1:String;
    k:Integer;
    MaxRow:Integer;
    MaxCol:Integer;
    StrLen:Integer;
begin
  try
    ViewHK1.BeginUpdate;
    k:=100*ViewHK1.Tag;

    MaxRow:=Vid.Gr1RowQuant;
    MaxCol:=Vid.Gr1ColQuant;
    StrLen:=Vid.Gr1TextLen;

    ViewHK1.DataController.RecordCount := MaxRow;

    for I := 0 to MaxRow-1 do    //������
      for J := 0 to MaxCol-1 do  //�������
//      ViewHK.DataController.SetValue(I, J, '�������� '+IntToStr(i)+' '+IntToStr(J)+#13+'0,00'+#13+'111222');

        ViewHK1.DataController.SetValue(I, J, '');

    with dmC do
    begin
      try
        quHotKeySel.Active:=False;
        quHotKeySel.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
        quHotKeySel.ParamByName('ICOLB').AsInteger:=k;
        quHotKeySel.ParamByName('ICOLE').AsInteger:=k+99;
        quHotKeySel.Active:=True;
        quHotKeySel.First;
        while not quHotKeySel.Eof do
        begin
          try
            Str(quHotKeySelPRICE_RUB.AsFloat:6:2,StrWk);
            Str1:=quHotKeySelNAME.AsString; while pos(#13,Str1)>0 do delete(Str1,pos(#13,Str1),1);

            if (quHotKeySelICOL.AsInteger>=k) and (quHotKeySelICOL.AsInteger<=(k+99))  then
            begin
              if (quHotKeySelIROW.AsInteger<=(MaxRow-1)) and ((quHotKeySelICOL.AsInteger-k)<=(MaxCol-1)) then
              begin
                StrWk:=Copy(Str1,1,StrLen)+#13+Copy(Str1,StrLen+1,StrLen)+#13+'����:'+StrWk+'�.'+#13+quHotKeySelSIFR.AsString;
                ViewHK1.DataController.SetValue(quHotKeySelIROW.AsInteger,quHotKeySelICOL.AsInteger-k,StrWk);
              end;
            end;
          except
          end;
          quHotKeySel.Next;
        end;
      finally
        quHotKeySel.Active:=False;
      end;
    end;
  finally
    ViewHK1.EndUpdate;
  end;
end;

//
//  Vid.GrColQuant:=10; Vid.GrColWidth:=107; Vid.GrRowQuant:=8; Vid.GrRowHeight:=45;  Vid.GrTextLen:=14;  Vid.GrFontSize:=8;  Vid.GrFontStyle:=0;

Procedure TfmMainCasher.CreateHotList;
Var AColumn:TcxGridColumn;
    i:INteger;
    MaxCol:Integer;
begin
  if Vid.Vid<>2 then Exit;
  try
    try
      MaxCol:=Vid.GrColQuant;
      ViewHK.ClearItems;
      ViewHK.OptionsView.DataRowHeight:=Vid.GrRowHeight;

      for I := 0 to MaxCol-1 do
      begin
        AColumn := ViewHK.CreateColumn;
        AColumn.Caption := IntToStr(i);
        AColumn.Name := 'k'+IntToStr(i);
        AColumn.DataBinding.ValueType := 'String';

        AColumn.Styles.Content:=nil;

        dmC.cxStyle30.Font.Size:=Vid.GrFontSize;
        if Vid.GrFontStyle=1 then dmC.cxStyle30.Font.Style := [fsBold] else  dmC.cxStyle30.Font.Style := [];

        AColumn.Styles.Content:=dmC.cxStyle30;
//      AColumn.Styles.Content.Color:=$00AED2AE;
        AColumn.Width:=Vid.GrColWidth; //���� 100

        AColumn.Styles.Content.Font.Size:=Vid.GrFontSize;
        if Vid.GrFontStyle=1 then AColumn.Styles.Content.Font.Style := [fsBold] else  AColumn.Styles.Content.Font.Style := [];
      end;
    except
    end;

    RefreshHotList;
  except
  end;
end;

//  Vid.GrColQuant:=10; Vid.GrColWidth:=107; Vid.GrRowQuant:=8; Vid.GrRowHeight:=45;  Vid.GrTextLen:=14;  Vid.GrFontSize:=8;  Vid.GrFontStyle:=0;

Procedure TfmMainCasher.RefreshHotList;
Var i,j:INteger;
    Str1:String;
    k:Integer;
    MaxRow:Integer;
    MaxCol:Integer;
    StrLen:Integer;
begin
  try
    ViewHK.BeginUpdate;

    k:=ViewHK.Tag;
    if k=1 then k:=0;
    if k>1 then k:=300+k*100;   // k=1  1-100  k=2 500-600  k=3 600-700  k=4 700-800 ..           // �������� 100-500   ��� ������� ������

    MaxRow:=Vid.GrRowQuant;
    MaxCol:=Vid.GrColQuant;
    StrLen:=Vid.GrTextLen;

    ViewHK.DataController.RecordCount := MaxRow;

    for I := 0 to MaxRow-1 do    //������
      for J := 0 to MaxCol-1 do  //�������
        ViewHK.DataController.SetValue(I, J, '');

    with dmC do
    begin
      try
        quHotKeySel.Active:=False;
        quHotKeySel.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
        quHotKeySel.ParamByName('ICOLB').AsInteger:=k;
        quHotKeySel.ParamByName('ICOLE').AsInteger:=k+99;
        quHotKeySel.Active:=True;
        quHotKeySel.First;
        while not quHotKeySel.Eof do
        begin
          try
            Str(quHotKeySelPRICE_RUB.AsFloat:6:2,StrWk);
            Str1:=quHotKeySelNAME.AsString; while pos(#13,Str1)>0 do delete(Str1,pos(#13,Str1),1);

            if (quHotKeySelICOL.AsInteger>=k) and (quHotKeySelICOL.AsInteger<=(k+99)) then
            begin
              if (quHotKeySelIROW.AsInteger<=(MaxRow-1)) and ((quHotKeySelICOL.AsInteger-k)<=(MaxCol-1)) then
              begin
                StrWk:=Copy(Str1,1,StrLen)+#13+Copy(Str1,StrLen+1,StrLen)+#13+'����:'+StrWk+'�.'+#13+quHotKeySelSIFR.AsString;
                ViewHK.DataController.SetValue(quHotKeySelIROW.AsInteger,quHotKeySelICOL.AsInteger-k,StrWk);
              end;
            end;
          except
          end;
          quHotKeySel.Next;
        end;
      finally
        quHotKeySel.Active:=False;
      end;
    end;
  finally
    ViewHK.EndUpdate;
  end;
end;

procedure NumLock(State:boolean);
var Keyboard:TKeyboardState;
begin
  GetKeyboardState(Keyboard);
  if (State and (Keyboard[vk_NumLock]=0))or(not State and (Keyboard[vk_NumLock]=1)) then
  begin
    keybd_event(vk_NumLock,0,KeyEventF_ExtendedKey,0);
    keybd_event(vk_NumLock,0,KeyEventF_ExtendedKey or KeyEventF_KeyUp,0);
  end;
end;

procedure TfmMainCasher.prSetShortCut(iKeyType:INteger);
Var StrWk,StrKey:String;
begin
  if iKeyType=1 then exit;
  if iKeyType>=2 then //������
  begin
    with dmC do
    begin
      quKeys.Active:=False;
      quKeys.ParamByName('IKT').AsInteger:=iKeyType;
      quKeys.Active:=True;

      quKeys.First;
      while not quKeys.Eof do
      begin
        StrWk:=Trim(quKeysACTIONNAME.AsString);
        StrKey:=Trim(quKeysHOTKEY.AsString);
        if StrWk='acChangePers' then  acChangePers.ShortCut:=TextToShortCut(StrKey);
        if StrWk='acCalcNal' then  acCalcNal.ShortCut:=TextToShortCut(StrKey);
        if StrWk='acCalcNal' then  fmCash.acCalcN.ShortCut:=TextToShortCut(StrKey);
        if StrWk='acFindArt' then  acFindArt.ShortCut:=TextToShortCut(StrKey);
        if StrWk='acQuantity' then  acQuantity.ShortCut:=TextToShortCut(StrKey);
        if StrWk='acError' then  acError.ShortCut:=TextToShortCut(StrKey);
        if StrWk='acAnnul' then  acAnnul.ShortCut:=TextToShortCut(StrKey);
        if StrWk='acPersDisc' then  acPersDisc.ShortCut:=TextToShortCut(StrKey);
        if StrWk='acManDisc' then  acManDisc.ShortCut:=TextToShortCut(StrKey);
        if StrWk='acUp' then  acUp.ShortCut:=TextToShortCut(StrKey);
        if StrWk='acDown' then  acDown.ShortCut:=TextToShortCut(StrKey);
        if StrWk='acDelPos' then  acDelPos.ShortCut:=TextToShortCut(StrKey);
        if StrWk='acPrice' then  acPrice.ShortCut:=TextToShortCut(StrKey);
        if StrWk='acCashDriverOpen' then  acCashDriverOpen.ShortCut:=TextToShortCut(StrKey);
        if StrWk='acRet' then  acRet.ShortCut:=TextToShortCut(StrKey);
        if StrWk='acCashRep' then  acCashRep.ShortCut:=TextToShortCut(StrKey);
        if StrWk='acCopyCheck' then  acCopyCheck.ShortCut:=TextToShortCut(StrKey);
        if StrWk='acCalc' then  acCalc.ShortCut:=TextToShortCut(StrKey);
        if StrWk='acPrintTCH' then  acPrintTCH.ShortCut:=TextToShortCut(StrKey);
        if StrWk='acExitProgram' then  acExitProgram.ShortCut:=TextToShortCut(StrKey);
        if StrWk='acCalcBn' then fmCash.acCalcBN.ShortCut:=TextToShortCut(StrKey);
        if StrWk='acComProxy' then acComProxy.ShortCut:=TextToShortCut(StrKey);

        quKeys.Next;
      end;

      //������� NUMLOCK
      NumLock(True);
      
    end;
  end;
end;

procedure TfmMainCasher.Recievedata(iZ,iD:INteger);
Var dBeg,dEnd:TDateTime;
    par,par1:Variant;
    StrWk:String;
    rDisc,rDiscPr:Real;
    SumRealN,SumRetN,SumRealB,SumRetB:Real;
    iType:Integer;
    TimeShift,TimePos,TimePosPre:TDateTime;
    sIp,sWr:String;
    iShop:Integer;
    bAddCheck:Boolean;
    iDep,iDepMain,iTypeP,iOp,IdH:Integer;
    sDiscBar:String;
    iChAll,iChAdd:INteger;
    s:string;
    iCurCh:Integer;
    iDel:Integer;
    iDate:Integer;
    bProc:Boolean;
    ZArr:array[1..50] of Integer;
    i:Integer;
begin
  WrMess('  �������� �������� ������.');

  TimeShift:=StrToTime(CommonSet.ZTimeShift);

  dBeg:=iD+TimeShift;
  dEnd:=iD+1+TimeShift;

  if iZ=0 then
     WrMess('  ������ � '+FormatDateTime('dd.mm.yyyy hh:nn',dBeg)+'  �� '+FormatDateTime('dd.mm.yyyy hh:nn',dEnd))
  else
     WrMess('  ����� '+intToStr(iZ));

  if CommonSet.OutType=1 then
  begin
  with dmTr do
  begin
    //�������� ����� ��� ��������

    SumRetN:=0;
    SumRealN:=0;
    SumRetB:=0;
    SumRealB:=0;


    quSumTr.Active:=False;
    quSumTr.SelectSQL.Clear;
//          quSumTr.SelectSQL.Add('SELECT Operation,Sum(TOTALRUB-DSUM) as SumOp  FROM CASHSAIL');
    quSumTr.SelectSQL.Add('SELECT Operation,Sum(TOTALRUB) as SumOp  FROM CASHSAIL');
    quSumTr.SelectSQL.Add('WHERE');
    quSumTr.SelectSQL.Add('SHOPINDEX=1');
    quSumTr.SelectSQL.Add('and CASHNUMBER='+IntToStr(CommonSet.CashNum));

    if iZ>0 then
    begin
      quSumTr.SelectSQL.Add('and ZNUMBER='+IntToStr(iZ));
    end else
    begin
      quSumTr.SelectSQL.Add('and CHDATE>'''+FormatDateTime('dd.mm.yyyy hh:nn',dBeg)+'''');
      quSumTr.SelectSQL.Add('and CHDATE<='''+FormatDateTime('dd.mm.yyyy hh:nn',dEnd)+'''');
    end;

    quSumTr.SelectSQL.Add('Group by Operation');
    quSumTr.SelectSQL.Add('Order by Operation');
    quSumTr.Active:=True;

    quSumTr.First;
    while not quSumTr.Eof do
    begin
      Case quSumTrOPERATION.AsInteger of
      0:SumRetN:=quSumTrSUMOP.AsFloat;
      1:SumRealN:=quSumTrSUMOP.AsFloat;
      4:SumRetB:=quSumTrSUMOP.AsFloat;
      5:SumRealB:=quSumTrSUMOP.AsFloat;
      end;

      quSumTr.Next;
    end;
    quSumTr.Active:=False;

    WrMess('');
    WrMess('    ����� � �����:');
    WrMess('     �������� ���. - '+FloatToStr(SumRealN));
    WrMess('     �������� �/�. - '+FloatToStr(SumRetN));
    WrMess('     �������  ���. - '+FloatToStr(SumRetB));
    WrMess('     �������  �/�. - '+FloatToStr(SumRealB));
    WrMess('');

    SumRetN:=0;
    SumRealN:=0;
    SumRetB:=0;
    SumRealB:=0;

    //�������� �����
    fbCashSail.Active:=False;
    fbCashSail.SelectSQL.Clear;
    fbCashSail.SelectSQL.Add('SELECT cs.SHOPINDEX,cs.CASHNUMBER,cs.ZNUMBER,cs.CHECKNUMBER,cs.ID,cs.CHDATE,cs.ARTICUL,cs.BAR,cs.CARDSIZE,cs.QUANTITY,cs.PRICERUB,cs.DPROC,cs.DSUM,cs.TOTALRUB,cs.CASHER,cs.DEPART,cs.OPERATION,cs.SECPOS,cs.ECHECK');
    fbCashSail.SelectSQL.Add('FROM CASHSAIL cs');
    fbCashSail.SelectSQL.Add('where');
    fbCashSail.SelectSQL.Add('SHOPINDEX=1');
    fbCashSail.SelectSQL.Add('and cs.CASHNUMBER='+IntToStr(CommonSet.CashNum));
    if iZ>0 then
    begin
      fbCashSail.SelectSQL.Add('and cs.ZNUMBER='+IntToStr(iZ));
    end else
    begin
      fbCashSail.SelectSQL.Add('and cs.CHDATE>'''+FormatDateTime('dd.mm.yyyy hh:nn',dBeg)+'''');
      fbCashSail.SelectSQL.Add('and cs.CHDATE<='''+FormatDateTime('dd.mm.yyyy hh:nn',dEnd)+'''');
    end;
    fbCashSail.SelectSQL.Add('Order by cs.SHOPINDEX,cs.CASHNUMBER,cs.ZNUMBER,cs.CHECKNUMBER,cs.ID');
    fbCashSail.Active:=True;

    taCashSail.Active:=False;
    taCashSail.DatabaseName:=CommonSet.OutPut;
    taCashDCRD.Active:=False;
    taCashDCRD.DatabaseName:=CommonSet.OutPut;
    taCashDisc.Active:=False;
    taCashDisc.DatabaseName:=CommonSet.OutPut;
    try

      Session1.Active:=False;
      Session1.NetFileDir:=CommonSet.NetPath;
      Session1.Active:=True;

      WrMess('     ��������� CASHSAIL.DB');
      taCashSail.Active:=True;
      WrMess('     ��������� CASHDISC.DB');
      taCashDisc.Active:=True;
      WrMess('     ��������� CASHDCRD.DB');
      taCashDcrd.Active:=True;

      WrMess('');
      WrMess('    ������� - '+CommonSet.OutPut+'cash*.db'+' ������� �������.');
      WrMess('    ����� ���� ��������� ������.');
      WrMess('');

      fbCashSail.First;
      while not fbCashSail.Eof do
      begin
        // ��� ����� ������ ��� �������� TotalRub 0 - ��� ������ � ���� �����; 1- �� ������� � ���� �����
        iType:=0;
        if ( RoundEx(fbCashSailQUANTITY.AsFloat*fbCashSailPRICERUB.AsFloat*100)/100-fbCashSailTOTALRUB.AsFloat)>0.01
        then iType:=1; //�.�. TotalRub ��� �� �������, ������, ��� ���������� ��� ���������� ������ ������� ���� �� �����

        par := VarArrayCreate([0,4], varInteger);
        par[0]:=fbCashSailSHOPINDEX.AsInteger;
        par[1]:=fbCashSailCASHNUMBER.AsInteger;
        par[2]:=fbCashSailZNUMBER.AsInteger;
        par[3]:=fbCashSailCHECKNUMBER.AsInteger;
        par[4]:=fbCashSailID.AsInteger;

        if taCashSail.Locate('SHOPINDEX;CASHNUMBER;ZNUMBER;CHECKNUMBER;ID',par,[]) then taCashSail.Edit
        else
        begin
          taCashSail.Append;
          taCashSailShopIndex.AsInteger:=fbCashSailSHOPINDEX.AsInteger;
          taCashSailCashNumber.AsInteger:=fbCashSailCASHNUMBER.AsInteger;
          taCashSailZNumber.AsInteger:=fbCashSailZNUMBER.AsInteger;
          taCashSailCheckNumber.AsInteger:=fbCashSailCHECKNUMBER.AsInteger;
          taCashSailID.AsInteger:=fbCashSailID.AsInteger;
        end;

        taCashSailDate.AsDateTime:=Trunc(fbCashSailCHDATE.AsDateTime);
        StrWk:=FormatDateTime('hhnn',fbCashSailCHDATE.AsDateTime-Trunc(fbCashSailCHDATE.AsDateTime));
        taCashSailTime.AsInteger:=StrToIntDef(StrWk,0);
        taCashSailCardArticul.AsString:=fbCashSailARTICUL.AsString;
        taCashSailCardSize.AsString:=fbCashSailCARDSIZE.AsString;
        taCashSailQuantity.AsFloat:=fbCashSailQUANTITY.AsFloat;
        taCashSailPriceRub.AsFloat :=fbCashSailPRICERUB.AsFloat;
        taCashSailPriceCur.AsFloat:=fbCashSailPRICERUB.AsFloat;

        case iType of
        0:begin //������ ������� - TotalRub=Quant*Price
            taCashSailTotalRub.AsFloat:=fbCashSailTOTALRUB.AsFloat-fbCashSailDSUM.AsFloat;
            taCashSailTotalCur.AsFloat:=fbCashSailTOTALRUB.AsFloat-fbCashSailDSUM.AsFloat;
          end;
        1:begin //����� ������� - TotalRub=Quant*Price-DSum
            taCashSailTotalRub.AsFloat:=fbCashSailTOTALRUB.AsFloat;
            taCashSailTotalCur.AsFloat:=fbCashSailTOTALRUB.AsFloat;
          end;
        end;
        taCashSailDepartment.AsInteger:=0;
        taCashSailCasher.AsInteger:=fbCashSailCASHER.AsInteger;
        taCashSailUsingIndex.AsInteger:=fbCashSailDEPART.AsInteger;

        Case fbCashSailOPERATION.AsInteger of
        0:begin   //������� ���
            taCashSailReplace.AsInteger:=0;
            taCashSailOperation.AsInteger:=0;
            taCashSailCredCardIndex.AsInteger:=0;
          end;
        1:begin    //������� ���
            taCashSailReplace.AsInteger:=1;
            taCashSailOperation.AsInteger:=1;
            taCashSailCredCardIndex.AsInteger:=0;
          end;
        4:begin   //������� ������
            taCashSailReplace.AsInteger:=0;
            taCashSailOperation.AsInteger:=4;
            taCashSailCredCardIndex.AsInteger:=1;
          end;
        5:begin   //������� ������
            taCashSailReplace.AsInteger:=1;
            taCashSailOperation.AsInteger:=5;
            taCashSailCredCardIndex.AsInteger:=1;
          end;
        end;

        taCashSailDiscCliIndex.AsInteger:=0;
        taCashSailLinked.AsInteger:=0;

        taCashSail.Post;

        Case fbCashSailOPERATION.AsInteger of
        0:SumRetN:=SumRetN+taCashSailTotalRub.AsFloat;
        1:SumRealN:=SumRealN+taCashSailTotalRub.AsFloat;
        4:SumRetB:=SumRetB+taCashSailTotalRub.AsFloat;
        5:SumRealB:=SumRealB+taCashSailTotalRub.AsFloat;
        end;

        rDisc:=fbCashSailDSUM.AsFloat;
        rDiscPr:=fbCashSailDPROC.AsFloat;

        if rDisc<>0 then //���� ������ �� �������
        begin
          fbCashPay.Active:=False;
          fbCashPay.ParamByName('SHOPINDEX').AsInteger:=fbCashSailSHOPINDEX.AsInteger;
          fbCashPay.ParamByName('CASHNUMBER').AsInteger:=fbCashSailCASHNUMBER.AsInteger;
          fbCashPay.ParamByName('ZNUMBER').AsInteger:=fbCashSailZNUMBER.AsInteger;
          fbCashPay.ParamByName('CHECKNUMBER').AsInteger:=fbCashSailCHECKNUMBER.AsInteger;
          fbCashPay.Active:=True;
          if (fbCashPay.RecordCount>0) and (fbCashPay.FieldByName('DBAR').AsString>'') then
          begin //������ ���� �� ��
            par := VarArrayCreate([0,3], varInteger);
            par[0]:=fbCashSailSHOPINDEX.AsInteger;
            par[1]:=fbCashSailCASHNUMBER.AsInteger;
            par[2]:=fbCashSailZNUMBER.AsInteger;
            par[3]:=fbCashSailCHECKNUMBER.AsInteger;

            if taCashDCRD.Locate('SHOPINDEX;CASHNUMBER;ZNUMBER;CHECKNUMBER',par,[]) then taCashDCRD.Edit
            else
            begin
              taCashDCRD.Append;
              taCashDCRDShopIndex.AsInteger:=fbCashSailSHOPINDEX.AsInteger;
              taCashDCRDCashNumber.AsInteger:=fbCashSailCASHNUMBER.AsInteger;
              taCashDCRDZNumber.AsInteger:=fbCashSailZNUMBER.AsInteger;
              taCashDCRDCheckNumber.AsInteger:=fbCashSailCHECKNUMBER.AsInteger;
            end;
            taCashDCRDCardType.AsInteger:=0;
            taCashDCRDCardNumber.AsString:=fbCashPay.FieldByName('DBAR').AsString;
            taCashDCRDDiscountRub.AsFloat:=fbCashPay.FieldByName('DSUM').AsFloat;
            taCashDCRDDiscountCur.AsFloat:=fbCashPay.FieldByName('DSUM').AsFloat;
            taCashdcrd.Post;

            par := VarArrayCreate([0,4], varInteger);
            par[0]:=fbCashSailSHOPINDEX.AsInteger;
            par[1]:=fbCashSailCASHNUMBER.AsInteger;
            par[2]:=fbCashSailZNUMBER.AsInteger;
            par[3]:=fbCashSailCHECKNUMBER.AsInteger;
            par[4]:=fbCashSailID.AsInteger;

            if taCashDisc.Locate('SHOPINDEX;CASHNUMBER;ZNUMBER;CHECKNUMBER;ID',par,[]) then taCashDisc.Edit
            else
            begin
              taCashDisc.Append;
              taCashDiscShopIndex.AsInteger:=fbCashSailSHOPINDEX.AsInteger;
              taCashDiscCashNumber.AsInteger:=fbCashSailCASHNUMBER.AsInteger;
              taCashDiscZNumber.AsInteger:=fbCashSailZNUMBER.AsInteger;
              taCashDiscCheckNumber.AsInteger:=fbCashSailCHECKNUMBER.AsInteger;
              taCashDiscID.AsInteger:=fbCashSailID.AsInteger;
            end;

            taCashDiscDiscountIndex.AsInteger:=4;
            taCashDiscDiscountProc.AsFloat:=rDiscPr;
            taCashDiscDiscountRub.AsFloat:=rDisc;
            taCashDiscDiscountCur.AsFloat:=rDisc;
            taCashDisc.Post;
          end
          else //��� ��
          begin
            par := VarArrayCreate([0,4], varInteger);
            par[0]:=fbCashSailSHOPINDEX.AsInteger;
            par[1]:=fbCashSailCASHNUMBER.AsInteger;
            par[2]:=fbCashSailZNUMBER.AsInteger;
            par[3]:=fbCashSailCHECKNUMBER.AsInteger;
            par[4]:=fbCashSailID.AsInteger;

            if taCashDisc.Locate('SHOPINDEX;CASHNUMBER;ZNUMBER;CHECKNUMBER;ID',par,[]) then taCashDisc.Edit
            else
            begin
              taCashDisc.Append;
              taCashDiscShopIndex.AsInteger:=fbCashSailSHOPINDEX.AsInteger;
              taCashDiscCashNumber.AsInteger:=fbCashSailCASHNUMBER.AsInteger;
              taCashDiscZNumber.AsInteger:=fbCashSailZNUMBER.AsInteger;
              taCashDiscCheckNumber.AsInteger:=fbCashSailCHECKNUMBER.AsInteger;
              taCashDiscID.AsInteger:=fbCashSailID.AsInteger;
            end;

            taCashDiscDiscountIndex.AsInteger:=2;
            taCashDiscDiscountProc.AsFloat:=rDiscPr;
            taCashDiscDiscountRub.AsFloat:=rDisc;
            taCashDiscDiscountCur.AsFloat:=rDisc;
            taCashDisc.Post;

          end;
        end;
        delay(33);
        fbCashSail.Next;
      end;

      Session1.Active:=False;

      WrMess('');
      WrMess('    ���������� �����:');
      WrMess('     �������� ���. - '+FloatToStr(rv(SumRetN)));
      WrMess('     �������  ���. - '+FloatToStr(rv(SumRealN)));
      WrMess('     �������� �/�. - '+FloatToStr(rv(SumRetB)));
      WrMess('     �������  �/�. - '+FloatToStr(rv(SumRealB)));

      WrMess('');
      WrMess('   ��������� ������ �� ����� ���������.');

    except
      WrMess('   ������ �������� ������� - '+CommonSet.OutPut+'cashsail.db');
    end;

    taCashSail.Active:=False;
    taCashDCRD.Active:=False;
    taCashDisc.Active:=False;

    fbCashSail.Active:=False;
  end;
  end;
  if CommonSet.OutType=2 then //����� � ���� PosFb �����
  begin
    with dmC do
    with dmFB do
    with dmTr do
    begin
     //�������� ����� ��� ��������

      SumRetN:=0;
      SumRealN:=0;
      SumRetB:=0;
      SumRealB:=0;

      quSumTr.Active:=False;
      quSumTr.SelectSQL.Clear;
//          quSumTr.SelectSQL.Add('SELECT Operation,Sum(TOTALRUB-DSUM) as SumOp  FROM CASHSAIL');
      quSumTr.SelectSQL.Add('SELECT Operation,Sum(TOTALRUB) as SumOp  FROM CASHSAIL');
      quSumTr.SelectSQL.Add('WHERE');
      quSumTr.SelectSQL.Add('SHOPINDEX=1');
      quSumTr.SelectSQL.Add('and CASHNUMBER='+IntToStr(CommonSet.CashNum));

      if iZ>0 then
      begin
        quSumTr.SelectSQL.Add('and ZNUMBER='+IntToStr(iZ));
      end else
      begin
        quSumTr.SelectSQL.Add('and CHDATE>'''+FormatDateTime('dd.mm.yyyy hh:nn',dBeg)+'''');
        quSumTr.SelectSQL.Add('and CHDATE<='''+FormatDateTime('dd.mm.yyyy hh:nn',dEnd)+'''');
      end;

      quSumTr.SelectSQL.Add('Group by Operation');
      quSumTr.SelectSQL.Add('Order by Operation');
      quSumTr.Active:=True;

      quSumTr.First;
      while not quSumTr.Eof do
      begin
        Case quSumTrOPERATION.AsInteger of
        0:SumRetN:=quSumTrSUMOP.AsFloat;
        1:SumRealN:=quSumTrSUMOP.AsFloat;
        4:SumRetB:=quSumTrSUMOP.AsFloat;
        5:SumRealB:=quSumTrSUMOP.AsFloat;
        end;

        quSumTr.Next;
      end;
      quSumTr.Active:=False;

      WrMess('');
      WrMess('    ����� � �����:');
      WrMess('     �������  ���. - '+FloatToStr(rv(SumRealN)));
      WrMess('     �������� ���. - '+FloatToStr(rv(SumRetN)));
      WrMess('     �������  �/�. - '+FloatToStr(rv(SumRealB)));
      WrMess('     �������� �/�. - '+FloatToStr(rv(SumRetB)));
      WrMess('');

      SumRetN:=0;
      SumRealN:=0;
      SumRetB:=0;
      SumRealB:=0;

      if CommonSet.SendPosFB=1 then
      begin
        try
          Cash.Close;
          Cash.DBName:=CommonSet.CashDB;
          Cash.Open;
          WrMess('    ���� ������ ��. ����� ���� ��������...');
        except
          WrMess('    ������ �������� ���� ������. �������� �����������....');
        end;

        if Cash.Connected then
        begin
          //���������

          prUpd2;

          //������

  //        EXECUTE PROCEDURE DELCASHSZDAY (?CASHNUMBER, ?ZNUM, ?ZDATEB, ?ZDATEE)

          prDelCashZ.ParamByName('CASHNUMBER').AsInteger:=CommonSet.CashNum;
          prDelCashZ.ParamByName('ZNUM').AsInteger:=iZ;
          prDelCashZ.ParamByName('ZDATEB').AsDateTime:=dBeg;
          prDelCashZ.ParamByName('ZDATEE').AsDateTime:=dEnd;
          prDelCashZ.ExecProc;

          delay(33);

          quCashPay.Active:=False;
          quCashPay.SelectSQL.Clear;
          quCashPay.SelectSQL.Add('SELECT SHOPINDEX,CASHNUMBER,ZNUMBER,CHECKNUMBER,CASHER,CHECKSUM,PAYSUM,DSUM,DBAR,COUNTPOS,CHDATE,PAYMENT');
          quCashPay.SelectSQL.Add('FROM CASHPAY');
          quCashPay.SelectSQL.Add('where DSUM<>0');
          quCashPay.SelectSQL.Add('and CASHNUMBER='+IntToStr(CommonSet.CashNum));
          if iZ>0 then
          begin
            quCashPay.SelectSQL.Add('and ZNUMBER='+IntToStr(iZ));
          end else
          begin
            quCashPay.SelectSQL.Add('and CHDATE>'''+FormatDateTime('dd.mm.yyyy hh:nn',dBeg)+'''');
            quCashPay.SelectSQL.Add('and CHDATE<='''+FormatDateTime('dd.mm.yyyy hh:nn',dEnd)+'''');
          end;
          quCashPay.Active:=True;

          quCashPay.First;
          while not quCashPay.Eof do
          begin
            prAddCashP.ParamByName('SHOPINDEX').AsInteger:=quCashPaySHOPINDEX.AsInteger;
            prAddCashP.ParamByName('CASHNUMBER').AsInteger:=quCashPayCASHNUMBER.AsInteger;
            prAddCashP.ParamByName('ZNUMBER').AsInteger:=quCashPayZNUMBER.AsInteger;
            prAddCashP.ParamByName('CHECKNUMBER').AsInteger:=quCashPayCHECKNUMBER.AsInteger;
            prAddCashP.ParamByName('CASHER').AsInteger:=quCashPayCASHER.AsInteger;
            prAddCashP.ParamByName('CHECKSUM').AsFloat:=quCashPayCHECKSUM.AsFloat;
            prAddCashP.ParamByName('PAYSUM').AsFloat:=quCashPayPAYSUM.AsFloat;
            prAddCashP.ParamByName('DSUM').AsFloat:=quCashPayDSUM.AsFloat;
            prAddCashP.ParamByName('DBAR').AsString:=quCashPayDBAR.AsString;
            prAddCashP.ParamByName('COUNTPOS').AsInteger:=quCashPayCOUNTPOS.AsInteger;
            prAddCashP.ParamByName('CHDATE').AsDateTime:=quCashPayCHDATE.AsDateTime;
            prAddCashP.ParamByName('PAYMENT').AsInteger:=quCashPayPAYMENT.AsInteger;
            prAddCashP.ExecProc;

            quCashPay.Next;
          end;

          //�������� �����
          fbCashSail.Active:=False;
          fbCashSail.SelectSQL.Clear;
          fbCashSail.SelectSQL.Add('SELECT cs.SHOPINDEX,cs.CASHNUMBER,cs.ZNUMBER,cs.CHECKNUMBER,cs.ID,cs.CHDATE,cs.ARTICUL,cs.BAR,cs.CARDSIZE,cs.QUANTITY,cs.PRICERUB,cs.DPROC,cs.DSUM,cs.TOTALRUB,cs.CASHER,cs.DEPART,cs.OPERATION,cs.SECPOS,cs.ECHECK');
          fbCashSail.SelectSQL.Add('FROM CASHSAIL cs');
          fbCashSail.SelectSQL.Add('where');
          fbCashSail.SelectSQL.Add('SHOPINDEX=1');
          fbCashSail.SelectSQL.Add('and cs.CASHNUMBER='+IntToStr(CommonSet.CashNum));
          if iZ>0 then
          begin
            fbCashSail.SelectSQL.Add('and cs.ZNUMBER='+IntToStr(iZ));
          end else
          begin
            fbCashSail.SelectSQL.Add('and cs.CHDATE>'''+FormatDateTime('dd.mm.yyyy hh:nn',dBeg)+'''');
            fbCashSail.SelectSQL.Add('and cs.CHDATE<='''+FormatDateTime('dd.mm.yyyy hh:nn',dEnd)+'''');
          end;
          fbCashSail.SelectSQL.Add('Order by cs.SHOPINDEX,cs.CASHNUMBER,cs.ZNUMBER,cs.CHECKNUMBER,cs.ID');
          fbCashSail.Active:=True;

          fbCashSail.First;
          while not fbCashSail.Eof do
          begin
            Case fbCashSailOPERATION.AsInteger of
            0:SumRetN:=SumRetN+fbCashSailTotalRub.AsFloat;
            1:SumRealN:=SumRealN+fbCashSailTotalRub.AsFloat;
            4:SumRetB:=SumRetB+fbCashSailTotalRub.AsFloat;
            5:SumRealB:=SumRealB+fbCashSailTotalRub.AsFloat;
            end;
            try
    //          EXECUTE PROCEDURE ADDCASHS (?SHOPINDEX, ?CASHNUMBER, ?ZNUMBER, ?CHECKNUMBER, ?ID, ?CHDATE, ?ARTICUL, ?BAR, ?CARDSIZE, ?QUANTITY, ?PRICERUB, ?DPROC, ?DSUM, ?TOTALRUB, ?CASHER, ?DEPART, ?OPERATION)
              prAddCashS.ParamByName('SHOPINDEX').AsInteger:=fbCashSailSHOPINDEX.AsInteger;
              prAddCashS.ParamByName('CASHNUMBER').AsInteger:=fbCashSailCASHNUMBER.AsInteger;
              prAddCashS.ParamByName('ZNUMBER').AsInteger:=fbCashSailZNUMBER.AsInteger;
              prAddCashS.ParamByName('CHECKNUMBER').AsInteger:=fbCashSailCHECKNUMBER.AsInteger;
              prAddCashS.ParamByName('ID').AsInteger:=fbCashSailID.AsInteger;
              prAddCashS.ParamByName('CHDATE').AsDateTime :=fbCashSailCHDATE.AsDateTime;
              prAddCashS.ParamByName('ARTICUL').AsString:=fbCashSailARTICUL.AsString;
              prAddCashS.ParamByName('BAR').AsString:=fbCashSailBAR.AsString;
              prAddCashS.ParamByName('CARDSIZE').AsString:=fbCashSailCARDSIZE.AsString;
              prAddCashS.ParamByName('QUANTITY').AsFloat:=fbCashSailQUANTITY.AsFloat;
              prAddCashS.ParamByName('PRICERUB').AsFloat:=fbCashSailPRICERUB.AsFloat;
              prAddCashS.ParamByName('DPROC').AsFloat:=fbCashSailDPROC.AsFloat;
              prAddCashS.ParamByName('DSUM').AsFloat:=fbCashSailDSUM.AsFloat;
              prAddCashS.ParamByName('TOTALRUB').AsFloat:=fbCashSailTOTALRUB.AsFloat;
              prAddCashS.ParamByName('CASHER').AsInteger:=fbCashSailCASHER.AsInteger;
              prAddCashS.ParamByName('DEPART').AsInteger:=fbCashSailDEPART.AsInteger;
              prAddCashS.ParamByName('OPERATION').AsInteger:=fbCashSailOPERATION.AsInteger;
              prAddCashS.ParamByName('SECPOS').AsInteger:=fbCashSailSECPOS.AsInteger;
              prAddCashS.ParamByName('ECHECK').AsInteger:=fbCashSailECHECK.AsInteger;
              prAddCashS.ExecProc;
            except
              begin
                s:='������ ���������� ��������� prAddCashS: '+#13;
                s:=s+'SHOPINDEX='+inttostr(fbCashSailSHOPINDEX.AsInteger)+#13;
                s:=s+ 'CASHNUMBER='+inttostr(fbCashSailCASHNUMBER.AsInteger)+#13;
                s:=s+ 'ZNUMBER='+inttostr(fbCashSailZNUMBER.AsInteger)+#13;
                s:=s+ 'CHECKNUMBER='+inttostr(fbCashSailCHECKNUMBER.AsInteger)+#13;
                s:=s+ 'ID='+inttostr(fbCashSailID.AsInteger)+#13;
                s:=s+ 'CHDATE='+DateTostr(fbCashSailCHDATE.AsDateTime)+#13;
                s:=s+ 'ARTICUL='+fbCashSailARTICUL.AsString+#13;
                s:=s+ 'BAR='+fbCashSailBAR.AsString+#13;
                s:=s+ 'CARDSIZE='+fbCashSailCARDSIZE.AsString+#13;
                s:=s+ 'QUANTITY='+floattostr(fbCashSailQUANTITY.AsFloat)+#13;
                s:=s+ 'PRICERUB='+floattostr(fbCashSailPRICERUB.AsFloat)+#13;
                s:=s+ 'DPROC='+floattostr(fbCashSailDPROC.AsFloat)+#13;
                s:=s+ 'DSUM='+floattostr(fbCashSailDSUM.AsFloat)+#13;
                s:=s+ 'TOTALRUB='+floattostr(fbCashSailTOTALRUB.AsFloat)+#13;
                s:=s+ 'CASHER='+inttostr(fbCashSailCASHER.AsInteger)+#13;
                s:=s+ 'DEPART='+inttostr(fbCashSailDEPART.AsInteger)+#13;
                s:=s+ 'OPERATION='+inttostr(fbCashSailOPERATION.AsInteger)+#13;
                s:=s+ 'SECPOS='+inttostr(fbCashSailSECPOS.AsInteger)+#13;
                s:=s+ 'ECHECK='+inttostr(fbCashSailECHECK.AsInteger);
                ShowMessage(s);
              end;
            end;
            fbCashSail.Next;
          end;

          WrMess('');
          WrMess('    ����� � �����:');
          WrMess('     �������  ���. - '+FloatToStr(rv(SumRealN)));
          WrMess('     �������� ���. - '+FloatToStr(rv(SumRetN)));
          WrMess('     �������  �/�. - '+FloatToStr(rv(SumRealB)));
          WrMess('     �������� �/�. - '+FloatToStr(rv(SumRetB)));
          WrMess('');
          WrMess('     �������� ����� ..');

          quZListDet2.Active:=False;
          quZListDet2.ParamByName('CNUM').AsInteger:=CommonSet.CashNum;
          quZListDet2.ParamByName('ZNUM').AsInteger:=iZ;
          quZListDet2.Active:=True;
          quZListDet2.First;
          while not quZListDet2.Eof do quZListDet2.Delete;

          quZListDet.Active:=False;
          quZListDet.ParamByName('CNUM').AsInteger:=CommonSet.CashNum;
          quZListDet.ParamByName('ZNUM').AsInteger:=iZ;
          quZListDet.Active:=True;
          quZListDet.First;
          while not quZListDet.Eof do
          begin
            quZListDet2.Append;
            quZListDet2CASHNUM.AsInteger:=CommonSet.CashNum;
            quZListDet2ZNUM.AsInteger:=iZ;
            quZListDet2INOUT.AsInteger:=quZListDetINOUT.AsInteger; //�������
            quZListDet2ITYPE.AsInteger:=quZListDetITYPE.AsInteger; //��� ���� ��������
            quZListDet2RSUM.AsFloat:=quZListDetRSUM.AsFloat;
            quZListDet2IDATE.AsInteger:=quZListDetIDATE.AsInteger;
            quZListDet2DDATE.AsDateTime:=quZListDetDDATE.AsDateTime;
            quZListDet2.Post;

            quZListDet.Next;
          end;
          WrMess('     ����� ��.');


          quZListDet.Active:=False;
          quZListDet2.Active:=False;

          fbCashSail.Active:=False;
          quCashPay.Active:=False;

          delay(1000);

          Cash.Close;
        end;
      end;
    end;
  end;
  if CommonSet.SQL>=1 then //������� ��������� � SQL
  begin
    WrMess(' ');
    WrMess('  �������� � SQL.');
    if CommonSet.SQLIP='' then
    begin
      sIp:=prGetIP;    //192.168.3.101 ��������
      if pos('.',sIp)>0 then delete(sIp,1,pos('.',sIp));
      if pos('.',sIp)>0 then delete(sIp,1,pos('.',sIp));
      if pos('.',sIp)>0 then CommonSet.SQLIP:='192.168.'+Copy(sIp,1,pos('.',sIp)-1)+'.150';
    end;
    if CommonSet.SQLIP>'' then
    begin  // ip ����� ����� - ����� ����� ��������� �� ������ ������
      WrMess('  - ����� '+CommonSet.SQLIP);
      try
        if PingTest(CommonSet.SQLIP,50,sWr) then
        begin
          WrMess('  - ���� ��');
          with dmSQL do
          with dmC do
          with dmTr do
          begin
            msConnection.Connected:=false;
            msConnection.ConnectionString:='Provider=SQLOLEDB.1;Password=YKS15Le;Persist Security Info=True;User ID=sa;Initial Catalog=Debor;Data Source='+CommonSet.SQLIP+';Use Procedure for Prepare=1;Auto Translate=True;Packet Size=4096;Workstation ID=SYS-9;Use Encryption for Data=False;Tag with column collation when possible=False';
//            msConnection.ConnectionString:='Provider=SQLOLEDB.1;Password=314159;Persist Security Info=True;User ID=sa;Initial Catalog=scrystal;Data Source='+CommonSet.SQLIP+';Use Procedure for Prepare=1;Auto Translate=True;Packet Size=4096;Workstation ID=SYS-9;Use Encryption for Data=False;Tag with column collation when possible=False';
            msConnection.Connected:=true;
            if msConnection.Connected then
            begin
              WrMess('  - ���� ��');
              //����� ����� ����� �������� � SQL
//              iShop:=fShopSQL;
              iShop:=CommonSet.Shop;
              //�������

              if CommonSet.SQL=1 then  //����� �� 1 - �������� 2 �� �������� � SCrystal �� ������ ������ � �����
              begin
                WrMess('  [scrystal]  ������� '+its(iShop));
                WrMess('  - ����� ...');

                iChAll:=0;
                iChAdd:=0;

                quChecks.Active:=False;
                quChecks.SelectSQL.Clear;
                quChecks.SelectSQL.Add('SELECT SHOPINDEX,CASHNUMBER,ZNUMBER,CHECKNUMBER,sum(TOTALRUB) as TOTALRUB,sum(DSUM) as DSUM');
                quChecks.SelectSQL.Add('FROM CASHSAIL');
                quChecks.SelectSQL.Add('where SHOPINDEX=1');
                quChecks.SelectSQL.Add('and CASHNUMBER='+its(CommonSet.CashNum));
                if iZ>0 then
                begin
                  quChecks.SelectSQL.Add('and ZNUMBER='+IntToStr(iZ));
                end else
                begin
                  quChecks.SelectSQL.Add('and CHDATE>'''+FormatDateTime('dd.mm.yyyy hh:nn',dBeg)+'''');
                  quChecks.SelectSQL.Add('and CHDATE<='''+FormatDateTime('dd.mm.yyyy hh:nn',dEnd)+'''');
                end;
                quChecks.SelectSQL.Add('group by SHOPINDEX, CASHNUMBER, ZNUMBER, CHECKNUMBER');
                quChecks.SelectSQL.Add('order by ZNUMBER,CHECKNUMBER');
                quChecks.Active:=True;

                if quChecks.RecordCount>0 then
                begin
                  par1 := VarArrayCreate([0,3],varInteger);

                  //����� ������ �������� �����
                  iDepMain:=fSelDepCash(iShop,CommonSet.CashNum);

                  quChecksSQL.Active:=False;
                  quChecksSQL.SQL.Clear;
                  quChecksSQL.SQL.Add('SELECT [Id] ,[Id_Shop] ,[Id_Depart] ,[Operation] ,[DateOperation] ,[Ck_Number],[Cassir],[Cash_Code],[NSmena],[CardNumber],[PaymentType],[rSumCh],[rSumD],[CountPos]');
                  quChecksSQL.SQL.Add('FROM [scrystal].[dbo].[vcqHeads]');
                  quChecksSQL.SQL.Add('where [Id_Shop]='+its(iShop)+' and [Cash_Code] = '+its(CommonSet.CashNum));

                  if iZ>0 then quChecksSQL.SQL.Add(' and [NSmena] = '+IntToStr(iZ))
                  else
                  begin
                    quChecksSQL.SQL.Add('and [DateOperation]>'''+FormatDateTime('yyyymmdd hh:nn',dBeg)+'''');
                    quChecksSQL.SQL.Add('and [DateOperation]<='''+FormatDateTime('yyyymmdd hh:nn',dEnd)+'''');
                  end;
                  quChecksSQL.Active:=True;

                  iCurCh:=1;  iDel:=0;
                  quChecks.First;
                  while not quChecks.Eof do
                  begin
                    if iCurCh>quChecksCHECKNUMBER.AsInteger then iCurCh:=quChecksCHECKNUMBER.AsInteger;
                    while iCurCh<quChecksCHECKNUMBER.AsInteger do
                    begin //������� ��������
                      par1[0]:=iShop;
                      par1[1]:=CommonSet.CashNum;
                      par1[2]:=quChecksZNUMBER.AsInteger;
                      par1[3]:=iCurCh;

                      if quChecksSQL.Locate('Id_Shop;Cash_Code;NSmena;Ck_Number',par1,[]) then
                      begin //��� ����
                        // 1  - �������
                        WrMess('  - ������� '+its(iCurCh));

                        quD.Active:=False;
                        quD.SQL.Clear;
                        quD.SQL.Add('DELETE FROM [scrystal].[dbo].[cqHeads]');
                        quD.SQL.Add('WHERE Id_Shop = '+its(iShop)+'and Cash_Code = '+its(CommonSet.CashNum)+'and NSmena = '+its(quChecksZNUMBER.AsInteger)+'and Ck_Number = '+its(iCurCh));
                        quD.ExecSQL;
                      end;
                      inc(iCurCh);
                    end;

                    par1[0]:=iShop;
                    par1[1]:=CommonSet.CashNum;
                    par1[2]:=quChecksZNUMBER.AsInteger;
                    par1[3]:=quChecksCHECKNUMBER.AsInteger;

                    if quChecksSQL.Locate('Id_Shop;Cash_Code;NSmena;Ck_Number',par1,[]) then
                    begin //��� ����
                      if abs(quChecksSQLrSumCh.AsFloat-quChecksTOTALRUB.AsFloat)>0.0001 then bAddCheck:=True else bAddCheck:=False;
                    end
                    else bAddCheck:=True;  //���� ��� - ����� ���������

                    if bAddCheck then //��������� ���
                    begin
                      // 1  - �������
                      quD.Active:=False;
                      quD.SQL.Clear;
                      quD.SQL.Add('DELETE FROM [scrystal].[dbo].[cqHeads]');
                      quD.SQL.Add('WHERE Id_Shop = '+its(iShop)+'and Cash_Code = '+its(CommonSet.CashNum)+'and NSmena = '+its(quChecksZNUMBER.AsInteger)+'and Ck_Number = '+its(quChecksCHECKNUMBER.AsInteger));
                      quD.ExecSQL;

                      quCheckSel.Active:=False;
                      quCheckSel.ParamByName('CASNUM').AsInteger:=CommonSet.CashNum;
                      quCheckSel.ParamByName('ZNUM').AsInteger:=quChecksZNUMBER.AsInteger;
                      quCheckSel.ParamByName('CHNUM').AsInteger:=quChecksCHECKNUMBER.AsInteger;
                      quCheckSel.Active:=True;

                      quCheckSel.First;

                      quCheckPSel.Active:=False;
                      quCheckPSel.ParamByName('CASNUM').AsInteger:=CommonSet.CashNum;
                      quCheckPSel.ParamByName('ZNUM').AsInteger:=quChecksZNUMBER.AsInteger;
                      quCheckPSel.ParamByName('CHNUM').AsInteger:=quChecksCHECKNUMBER.AsInteger;
                      quCheckPSel.Active:=True;
                      quCheckPSel.First;

                      TimePos:=quCheckSelCHDATE.AsDateTime;
                      TimePosPre:=quCheckSelCHDATE.AsDateTime;

                      sDiscBar:='';
                      if quCheckPSel.RecordCount>0 then sDiscBar:=quCheckPSelDBAR.AsString;

                      if (quCheckSelARTICUL.AsString='910')or(quCheckSelARTICUL.AsString='911') then iDep:=6 else iDep:=iDepMain;

                      if quCheckSelOPERATION.AsInteger>2 then iTypeP:=1 else iTypeP:=0;
                      if (quCheckSelOPERATION.AsInteger=0)or(quCheckSelOPERATION.AsInteger=4) then iOp:=-1 else iOp:=1;


                      quA.Active:=False;
                      quA.SQL.Clear;
                      quA.SQL.Add('INSERT INTO [scrystal].[dbo].[cqHeads]');
                      quA.SQL.Add('           ([Id_Shop]');
                      quA.SQL.Add('           ,[Id_Depart]');
                      quA.SQL.Add('           ,[Operation]');
                      quA.SQL.Add('           ,[DateOperation]');
                      quA.SQL.Add('           ,[Ck_Number]');
                      quA.SQL.Add('           ,[Cassir]');
                      quA.SQL.Add('           ,[Cash_Code]');
                      quA.SQL.Add('           ,[NSmena]');
                      quA.SQL.Add('           ,[CardNumber]');
                      quA.SQL.Add('           ,[PaymentType])');
                      quA.SQL.Add('     VALUES');
                      quA.SQL.Add('           ('+its(iShop));
                      quA.SQL.Add('           ,'+its(iDep));
                      quA.SQL.Add('           ,'+its(iOp));
                      quA.SQL.Add('           ,'''+FormatDateTime('yyyymmdd hh:nn',quCheckSelCHDATE.AsDateTime)+'''');
                      quA.SQL.Add('           ,'+its(quChecksCHECKNUMBER.AsInteger));
                      quA.SQL.Add('           ,'+its(quCheckSelCASHER.AsInteger));
                      quA.SQL.Add('           ,'+its(CommonSet.CashNum));
                      quA.SQL.Add('           ,'+its(quChecksZNUMBER.AsInteger));
                      quA.SQL.Add('           ,'''+sDiscBar+'''');
                      quA.SQL.Add('           ,'+its(iTypeP)+')');
                      quA.ExecSQL;

                      IdH:=fGetId_AI;

                      while not quCheckSel.Eof do
                      begin
                        if quCheckSelID.AsInteger>1 then
                        begin
                          TimePosPre:=TimePos;
                          TimePos:=quCheckSelCHDATE.AsDateTime;
                        end;

                        quA.Active:=False;
                        quA.SQL.Clear;
                        quA.SQL.Add('INSERT INTO [scrystal].[dbo].[cqChLines]');
                        quA.SQL.Add('           ([IdHead]         ');
                        quA.SQL.Add('           ,[Num]            ');
                        quA.SQL.Add('           ,[Code]           ');
                        quA.SQL.Add('           ,[BarCode]        ');
                        quA.SQL.Add('           ,[Quant]          ');
                        quA.SQL.Add('           ,[Price]          ');
                        quA.SQL.Add('           ,[Summa]          ');
                        quA.SQL.Add('           ,[ProcessingTime] ');
                        quA.SQL.Add('           ,[DProc]          ');
                        quA.SQL.Add('           ,[DSum])          ');
                        quA.SQL.Add('     VALUES                  ');
                        quA.SQL.Add('           ('+its(IdH));
                        quA.SQL.Add('           ,'+quCheckSelID.AsString);
                        quA.SQL.Add('           ,'+quCheckSelARTICUL.AsString);
                        quA.SQL.Add('           ,'''+quCheckSelBAR.AsString+'''');
                        quA.SQL.Add('           ,'+fts(quCheckSelQUANTITY.asfloat));
                        quA.SQL.Add('           ,'+fts(quCheckSelPRICERUB.asfloat));
                        quA.SQL.Add('           ,'+fts(quCheckSelTOTALRUB.asfloat));
                        quA.SQL.Add('           ,'+fts(TimePos-TimePosPre));
                        quA.SQL.Add('           ,'+fts(quCheckSelDPROC.asfloat));
                        quA.SQL.Add('           ,'+fts(quCheckSelDSUM.asfloat)+')');
                        quA.ExecSQL;

                        quCheckSel.Next;
                      end;

                      inc(iChAdd);
                      quCheckPSel.Active:=False;
                      quCheckSel.Active:=False;
                    end;

                    inc(iChAll);
                    inc(iCurCh);
                    quChecks.Next;
                  end;
                  quChecksSQL.Active:=False;
                end;
                quChecks.Active:=False;
                WrMess('  - ���������: '+its(iChAll)+'     ���������: '+its(iChAdd));
              end;

              WrMess('  [Debor]  ������� '+its(iShop));

              //���� �������� ��������
              for i:=1 to 10 do ZArr[i]:=0; //�� 10 ���� � ���� ���������

              if iZ>0 then ZArr[1]:=iZ else
              begin
                quZ.Active:=False;
                quZ.SelectSQL.Clear;
                quZ.SelectSQL.Add('select shopindex, cashnumber, znumber, min(chdate) as MINDATE');
                quZ.SelectSQL.Add('from cashpay');
                quZ.SelectSQL.Add('where shopindex=1');
                quZ.SelectSQL.Add('and cashnumber='+its(CommonSet.CashNum));
                quZ.SelectSQL.Add('and chdate>'''+FormatDateTime('dd.mm.yyyy hh:nn',dBeg)+'''');
                quZ.SelectSQL.Add('and chdate<='''+FormatDateTime('dd.mm.yyyy hh:nn',dEnd)+'''');
                quZ.SelectSQL.Add('group by shopindex, cashnumber, znumber');
                quZ.Active:=True;
                quZ.First; i:=1;
                while not quZ.Eof do
                begin
                  ZArr[i]:=quZZNUMBER.AsInteger;
                  quZ.Next; inc(i);
                  if i=11 then Break;
                end;
                quZ.Active:=False;
              end;

              for i:=1 to 10 do
              begin
                if ZArr[i]>0 then
                begin
                  WrMess('  Z - '+its(ZArr[i]));

                  SumRetN:=0;
                  SumRealN:=0;
                  SumRetB:=0;
                  SumRealB:=0;

                  quSelDateZ.Active:=False;
                  quSelDateZ.ParamByName('ICASH').AsInteger:=CommonSet.CashNum;
                  quSelDateZ.ParamByName('IZ').AsInteger:=ZArr[i];
                  quSelDateZ.Active:=True;

                  iDate:=Trunc(quSelDateZCHDATE.AsDateTime++TimeShift);

                  quSelDateZ.Active:=False;


// ������                  iDate:=Trunc(dBeg);


                  bProc:=False;

                  //������� ��������� ����� ����� , ���� �� ���� �� ��� �� �����
                  //��� FB
                  quZListDet.Active:=False;
                  quZListDet.ParamByName('CNUM').AsInteger:=CommonSet.CashNum;
                  quZListDet.ParamByName('ZNUM').AsInteger:=ZArr[i];
                  quZListDet.Active:=True;
                  quZListDet.First;
                  while not quZListDet.Eof do
                  begin
                    if quZListDetINOUT.AsInteger=1 then  //�������
                    begin
                      if quZListDetITYPE.AsInteger=1 then //���
                      begin
                        SumRealN:=quZListDetRSUM.AsFloat;
                      end else //������
                      begin
                        SumRealB:=quZListDetRSUM.AsFloat;
                      end;
                    end;
                    if quZListDetINOUT.AsInteger=-1 then  //��������
                    begin
                      if quZListDetITYPE.AsInteger=1 then //���
                      begin
                        SumRetN:=quZListDetRSUM.AsFloat;
                      end else //������
                      begin
                        SumRetB:=quZListDetRSUM.AsFloat;
                      end;
                    end;

                    quZListDet.Next;
                  end;
                  quZListDet.Active:=False;

                  // ��� ��� SQL
                  quSumZ.Active:=False;
                  quSumZ.Parameters.ParamByName('ISHOP').Value:=iShop;
                  quSumZ.Parameters.ParamByName('IDATE').Value:=iDate; //>=
                  quSumZ.Parameters.ParamByName('ICASH').Value:=CommonSet.CashNum;
                  quSumZ.Parameters.ParamByName('IZ').Value:=ZArr[i];
                  quSumZ.Active:=True;
                  quSumZ.First;

                  if (SumRealN<>quSumZSummaNal.AsFloat)or(SumRealB<>quSumZSummaBn.AsFloat)or(SumRetN<>quSumZSummaRetNal.AsFloat)or(SumRetB<>quSumZSummaRetBn.AsFloat) then bProc:=True;

                  WrMess('');
                  WrMess('    ����� ��� �����������:');
                  WrMess('     �������  ���. - '+FloatToStr(rv(quSumZSummaNal.AsFloat)));
                  WrMess('     �������� ���. - '+FloatToStr(rv(quSumZSummaRetNal.AsFloat)));
                  WrMess('     �������  �/�. - '+FloatToStr(rv(quSumZSummaBn.AsFloat)));
                  WrMess('     �������� �/�. - '+FloatToStr(rv(quSumZSummaRetBn.AsFloat)));
                  WrMess('');

                  quSumZ.Active:=False;

                  if bProc then
                  begin
                    WrMess('    ���� �����������, ����� ... ���� ������ �����.');

                    iChAll:=0;
                    iChAdd:=0;

                    quChecks.Active:=False;
                    quChecks.SelectSQL.Clear;
                    quChecks.SelectSQL.Add('SELECT SHOPINDEX,CASHNUMBER,ZNUMBER,CHECKNUMBER,sum(TOTALRUB) as TOTALRUB,sum(DSUM) as DSUM');
                    quChecks.SelectSQL.Add('FROM CASHSAIL');
                    quChecks.SelectSQL.Add('where SHOPINDEX=1');
                    quChecks.SelectSQL.Add('and CASHNUMBER='+its(CommonSet.CashNum));
                    quChecks.SelectSQL.Add('and ZNUMBER='+IntToStr(ZArr[i]));
                    quChecks.SelectSQL.Add('group by SHOPINDEX, CASHNUMBER, ZNUMBER, CHECKNUMBER');
                    quChecks.SelectSQL.Add('order by ZNUMBER,CHECKNUMBER');
                    quChecks.Active:=True;

                    if quChecks.RecordCount>0 then
                    begin
                      //����� ������ �������� �����
                      iDepMain:=fSelDepCash(iShop,CommonSet.CashNum);

                      quChecksS.Active:=False;
                      quChecksS.Parameters.ParamByName('ISHOP').Value:=iShop;
                      quChecksS.Parameters.ParamByName('IDATE').Value:=iDate;
                      quChecksS.Parameters.ParamByName('ICASH').Value:=CommonSet.CashNum;
                      quChecksS.Parameters.ParamByName('IZ').Value:=ZArr[i];
                      quChecksS.Active:=True;

                      iCurCh:=1;  iDel:=0;
                      quChecks.First;
                      while not quChecks.Eof do
                      begin
                        if iCurCh>quChecksCHECKNUMBER.AsInteger then iCurCh:=quChecksCHECKNUMBER.AsInteger;
                        while iCurCh<quChecksCHECKNUMBER.AsInteger do
                        begin //������� ��������
                          if quChecksS.Locate('Ck_Number',iCurCh,[]) then
                          begin //��� ����
                            // 1  - �������
                            WrMess('  - ������� '+its(iCurCh));

                            quD.Active:=False;
                            quD.SQL.Clear;
                            quD.SQL.Add('DELETE FROM [dbo].[CH_HD]');
                            quD.SQL.Add('where [Id_Shop]='+its(iShop)+'and [IDate]>='+its(iDate)+' and [Cash_Code]='+its(CommonSet.CashNum)+' and [NSmena]='+its(quChecksZNUMBER.AsInteger)+'and [Ck_Number]='+its(iCurCh));
                            quD.ExecSQL;
                          end;
                          inc(iCurCh);
                        end;

                        if quChecksS.Locate('Ck_Number',quChecksCHECKNUMBER.AsInteger,[]) then
                        begin //��� ����
                          if abs(quChecksSRSUM.AsFloat-quChecksTOTALRUB.AsFloat)>0.0001 then bAddCheck:=True else bAddCheck:=False;
                        end
                        else bAddCheck:=True;  //���� ��� ��� ����� ���������� - ����� ��������� � ���������

                        if bAddCheck then //��������� ���
                        begin
                          // 1  - �������
                          quD.Active:=False;
                          quD.SQL.Clear;
                          quD.SQL.Add('DELETE FROM [dbo].[CH_HD]');
                          quD.SQL.Add('where [Id_Shop]='+its(iShop)+'and [IDate]>='+its(iDate)+' and [Cash_Code]='+its(CommonSet.CashNum)+' and [NSmena]='+its(quChecksZNUMBER.AsInteger)+'and [Ck_Number]='+its(quChecksCHECKNUMBER.AsInteger));
                          quD.ExecSQL;

                          quCheckSel.Active:=False;
                          quCheckSel.ParamByName('CASNUM').AsInteger:=CommonSet.CashNum;
                          quCheckSel.ParamByName('ZNUM').AsInteger:=quChecksZNUMBER.AsInteger;
                          quCheckSel.ParamByName('CHNUM').AsInteger:=quChecksCHECKNUMBER.AsInteger;
                          quCheckSel.Active:=True;

                          quCheckSel.First;

                          quCheckPSel.Active:=False;
                          quCheckPSel.ParamByName('CASNUM').AsInteger:=CommonSet.CashNum;
                          quCheckPSel.ParamByName('ZNUM').AsInteger:=quChecksZNUMBER.AsInteger;
                          quCheckPSel.ParamByName('CHNUM').AsInteger:=quChecksCHECKNUMBER.AsInteger;
                          quCheckPSel.Active:=True;
                          quCheckPSel.First;

                          TimePos:=quCheckSelCHDATE.AsDateTime;
                          TimePosPre:=quCheckSelCHDATE.AsDateTime;

                          sDiscBar:='';
                          if quCheckPSel.RecordCount>0 then sDiscBar:=quCheckPSelDBAR.AsString;

                          if (quCheckSelARTICUL.AsString='910')or(quCheckSelARTICUL.AsString='911') then iDep:=6 else iDep:=iDepMain;


                          quA.Active:=False;
                          quA.SQL.Clear;
                          quA.SQL.Add('INSERT INTO [dbo].[CH_HD]');
                          quA.SQL.Add('           ([Id_Shop]    ');
                          quA.SQL.Add('           ,[IDate]      ');
                          quA.SQL.Add('           ,[Cash_Code]  ');
                          quA.SQL.Add('           ,[NSmena]     ');
                          quA.SQL.Add('           ,[Ck_Number]  ');
                          quA.SQL.Add('           ,[Id_Depart]  ');
                          quA.SQL.Add('           ,[Operation]  ');
                          quA.SQL.Add('           ,[DateOperation]');
                          quA.SQL.Add('           ,[Cassir]      ');
                          quA.SQL.Add('           ,[CardNumber]) ');
                          quA.SQL.Add('     VALUES               ');
                          quA.SQL.Add('           ('+its(iShop));   //<Id_Shop, int,>
                          quA.SQL.Add('           ,'+its(iDate));   //<IDate, int,>
                          quA.SQL.Add('           ,'+its(CommonSet.CashNum));   //<Cash_Code, int,>
                          quA.SQL.Add('           ,'+its(quChecksZNUMBER.AsInteger));   //<NSmena, int,>
                          quA.SQL.Add('           ,'+its(quChecksCHECKNUMBER.AsInteger));   //<Ck_Number, int,>
                          quA.SQL.Add('           ,'+its(iDep));   //<Id_Depart, int,>
                          quA.SQL.Add('           ,'+its(quCheckSelOPERATION.AsInteger));   //<Operation, int,>
                          quA.SQL.Add('           ,'''+FormatDateTime('yyyymmdd hh:nn',quCheckSelCHDATE.AsDateTime)+'''');   //<DateOperation, datetime,>
                          quA.SQL.Add('           ,'+its(quCheckSelCASHER.AsInteger));   //<Cassir, int,>
                          quA.SQL.Add('           ,'''+sDiscBar+''')');   //<CardNumber, varchar(20),>)
                          quA.ExecSQL;

                          while not quCheckSel.Eof do
                          begin
                            if quCheckSelID.AsInteger>1 then
                            begin
                              TimePosPre:=TimePos;
                              TimePos:=quCheckSelCHDATE.AsDateTime;
                            end;

                            quA.Active:=False;
                            quA.SQL.Clear;
                            quA.SQL.Add('INSERT INTO [dbo].[CH_SP] ');
                            quA.SQL.Add('           ([Id_Shop]     ');
                            quA.SQL.Add('           ,[IDate]       ');
                            quA.SQL.Add('           ,[Cash_Code]   ');
                            quA.SQL.Add('           ,[NSmena]      ');
                            quA.SQL.Add('           ,[Ck_Number]   ');
                            quA.SQL.Add('           ,[Num]         ');
                            quA.SQL.Add('           ,[Code]        ');
                            quA.SQL.Add('           ,[BarCode]     ');
                            quA.SQL.Add('           ,[Quant]       ');
                            quA.SQL.Add('           ,[Price]       ');
                            quA.SQL.Add('           ,[Summa]       ');
                            quA.SQL.Add('           ,[ProcessingTime]');
                            quA.SQL.Add('           ,[DProc]       ');
                            quA.SQL.Add('           ,[DSum]        ');
                            quA.SQL.Add('           ,[PaymentType] ');
                            quA.SQL.Add('           ,[AMARK] )');
                            quA.SQL.Add('     VALUES               ');
                            quA.SQL.Add('           ('+its(iShop));     //<Id_Shop, int,>
                            quA.SQL.Add('           ,'+its(iDate));     //<IDate, int,>
                            quA.SQL.Add('           ,'+its(CommonSet.CashNum));     //<Cash_Code, int,>
                            quA.SQL.Add('           ,'+its(quChecksZNUMBER.AsInteger));     //<NSmena, int,>
                            quA.SQL.Add('           ,'+its(quChecksCHECKNUMBER.AsInteger));     //<Ck_Number, int,>
                            quA.SQL.Add('           ,'+its(quCheckSelID.AsInteger));     //<Num, tinyint,>
                            quA.SQL.Add('           ,'+quCheckSelARTICUL.AsString);     //<Code, int,>
                            quA.SQL.Add('           ,'''+quCheckSelBAR.AsString+'''');     //<BarCode, varchar(15),>
                            quA.SQL.Add('           ,'+fts(quCheckSelQUANTITY.asfloat));     //<Quant, real,>
                            quA.SQL.Add('           ,'+fts(quCheckSelPRICERUB.asfloat));     //<Price, real,>
                            quA.SQL.Add('           ,'+fts(quCheckSelTOTALRUB.asfloat));     //<Summa, float,>
                            quA.SQL.Add('           ,'+fts(TimePos-TimePosPre));     //<ProcessingTime, real,>
                            quA.SQL.Add('           ,'+fts(quCheckSelDPROC.asfloat));     //<DProc, real,>
                            quA.SQL.Add('           ,'+fts(quCheckSelDSUM.asfloat));     //<DSum, real,>
                            quA.SQL.Add('           ,'+its(quCheckSelOPERATION.AsInteger));     //<PaymentType, smallint,>)
                            quA.SQL.Add('           ,'''+quCheckSelAMARK.AsString+''')');     //<AMARK, varchar(150),>
                            quA.ExecSQL;

                            quCheckSel.Next;
                          end;

                          inc(iChAdd);
                          quCheckPSel.Active:=False;
                          quCheckSel.Active:=False;
                        end;

                        inc(iChAll);
                        inc(iCurCh);
                        quChecks.Next;
                      end;
                    end;
                    quChecksS.Active:=False;
                    WrMess('  - ���������: '+its(iChAll)+'     ���������: '+its(iChAdd));
                  end else WrMess('    ����������� ���.');
                end;
              end;
              msConnection.Close;
            end else WrMess('    ���� ����������.');
          end;
        end else  WrMess('    ��� �����.');
      except
        WrMess('  - ������.');
      end;
    end else WrMess('  - ����� �� ���������.');
  end;

  WrMess('  �������� ������ ���������.');
end;


procedure TfmMainCasher.WrMess(Strwk_: string);
var F: TextFile;
    Strwk1:String;
    FileN:String;
begin
  try
//    strwk1:=FormatDateTime('yyyy_mm',Date);
    StrWk_:='     '+StrWk_;
    fmSt.Memo1.Lines.Add(StrWk_);
    Strwk1:='Receiver.txt';
    Application.ProcessMessages;
    FileN:=CommonSet.PathHistory+strwk1;
    AssignFile(F, FileN);
    if Not FileExists(FileN) then Rewrite(F)
    else                           Append(F);
    Write(F, FormatDateTime('DD/MM/YYYY  HH:NN:SS ', Now));
    WriteLn(F, Strwk_);
    Flush(F);
  finally
    CloseFile(F);
  end;
end;



Procedure PrintF(iType:Integer;rSum,rSumR:Real); //0-������� 1-��������  rSumR-���������� ������� � ��
Var StrWk,StrWk1:String;
    bNotErr:Boolean;
begin
  //����� ����
  if not CanDo('prMoneyInOut') then begin fmMainCasher.Memo2.Clear; fmMainCasher.Memo2.Lines.Add('��� ���� �� ��������.');  exit;end;
        //�������� ������� �����
  Nums.iRet:=InspectSt(Nums.sRet);
  if TestStatus('InspectSt',sMessage)=False then
  begin
    WriteStatus;
    fmAttention.Label1.Caption:=sMessage;
    fmAttention.ShowModal;
    exit;// ������ ��� - ������� ��� ������������ ����
  end;

  CountSec:=0; fmMainCasher.Timer2.Enabled:=False;

  Case iType of
  0: FormLog('MoneyOut','CashNum '+INtToStr(CommonSet.CashNum));
  1: FormLog('MoneyIn','CashNum '+INtToStr(CommonSet.CashNum));
  end;

  with dmC do
  begin
    with fmInOutM do
    begin
      if taInOutM.Active then
      begin
        //���������
        bNotErr:=True;
        OpenNFDoc;
        SelectF(10);
        if bNotErr then bNotErr:=PrintNFStr(FormatDateTime('dd.mm.yyyy                 hh:nn:ss',Now));

        Case iType of
        0: if bNotErr then bNotErr:=PrintNFStr('������� �����              ����� '+INtToStr(CommonSet.CashNum));
        1: if bNotErr then bNotErr:=PrintNFStr('�������� �����             ����� '+INtToStr(CommonSet.CashNum));
        end;

        if bNotErr then bNotErr:=PrintNFStr('���������: '+Person.Name);

        if rSumR<0 then
        begin
          Str(quMoneyLastENDSUM.AsFloat:13:2,StrWk);
          if bNotErr then bNotErr:=PrintNFStr('�������           ���:'+StrWk);

          Str(quCashSumCASHSUM.AsFloat:13:2,StrWk);
          if bNotErr then bNotErr:=PrintNFStr('����� � �����     ���:'+StrWk);
        end else
        begin
          Str((rSumR-rSumR):13:2,StrWk);
          if bNotErr then bNotErr:=PrintNFStr('�������           ���:'+StrWk);

          Str(rSumR:13:2,StrWk);
          if bNotErr then bNotErr:=PrintNFStr('����� � �����     ���:'+StrWk);
        end;

        SelectF(3);
        StrWk:='                                                       ';
        if bNotErr then bNotErr:=PrintNFStr(StrWk);
        SelectF(10);
        StrWk:='  ������           ���-��          ����� ';
        if bNotErr then bNotErr:=PrintNFStr(StrWk);
        SelectF(3);
        StrWk:='                                                       ';
        if bNotErr then bNotErr:=PrintNFStr(StrWk);

        //������������

        taInOutM.First;
        while not taInOutM.Eof do
        begin
          Str(taInOutMrVal.AsFloat:10:2,StrWk1);
          StrWk:=StrWk1; //

          StrWk1:=IntToStr(taInOutMiQuant.AsInteger);
          While Length(StrWk1)<16 do StrWk1:=' '+StrWk1;
          StrWk:=StrWk+StrWk1;

          Str((taInOutMrVal.AsFloat*taInOutMiQuant.AsInteger):12:2,StrWk1);
          StrWk:=StrWk+StrWk1+'   ';

          SelectF(10);
          if bNotErr then bNotErr:=PrintNFStr(StrWk);

          taInOutM.Next;
        end;
        SelectF(10);
        if bNotErr then bNotErr:=PrintNFStr('');

        Str(fmInOutM.CurrencyEdit1.EditValue:12:2,strwk1);
        StrWk:='���������                 '+StrWk1+'   ';
        if bNotErr then bNotErr:=PrintNFStr(StrWk);

        SelectF(3);
        StrWk:='                                                       ';
        if bNotErr then bNotErr:=PrintNFStr(StrWk);

        //�������
        Case iType of
        0:begin
            Str(rSum:12:2,StrWk1);
            StrWk:='����� ������ ����� ���:'+StrWk1;
            SelectF(10);
            if bNotErr then bNotErr:=PrintNFStr(StrWk);

            if rSumR<0 then  rSum:=quMoneyLastENDSUM.AsFloat+quCashSumCASHSUM.AsFloat-rSum
            else rSum:=rSumR-rSum;
            Str(rSum:12:2,StrWk1);
            StrWk:='����� � �����      ���:'+StrWk1;
            SelectF(10);
            if bNotErr then bNotErr:=PrintNFStr(StrWk);


            StrWk:='';
            SelectF(10);
            if bNotErr then bNotErr:=PrintNFStr(StrWk);

            StrWk:=' ���� _____________________';
            SelectF(10);
            if bNotErr then bNotErr:=PrintNFStr(StrWk);

            StrWk:='';
            SelectF(10);
            if bNotErr then bNotErr:=PrintNFStr(StrWk);

            StrWk:=' ������ ___________________';
            SelectF(10);
            if bNotErr then PrintNFStr(StrWk);
          end;
        1:begin
            Str(rSum:11:2,StrWk1);
            StrWk:='����� ������� ����� ���:'+StrWk1;
            SelectF(10);
            if bNotErr then bNotErr:=PrintNFStr(StrWk);

            StrWk:='';
            SelectF(10);
            if bNotErr then bNotErr:=PrintNFStr(StrWk);

            if rSumR<0 then  rSum:=quMoneyLastENDSUM.AsFloat+quCashSumCASHSUM.AsFloat+rSum
            else rSum:=rSumR+rSum;
//            rSum:=quMoneyLastENDSUM.AsFloat+quCashSumCASHSUM.AsFloat+rSum;
            Str(rSum:11:2,StrWk1);
            StrWk:='����� � �����       ���:'+StrWk1;
            SelectF(10);
            if bNotErr then PrintNFStr(StrWk);
          end;
        end;

        if (CommonSet.TypeFis='prim')or(CommonSet.TypeFis='shtrih') then
        begin
          PrintNFStr('');
          PrintNFStr('');
          PrintNFStr('');
          PrintNFStr('');
          PrintNFStr('');
          PrintNFStr('');
        end;

        CloseNFDoc;
        
        CutDoc;
      end;
    end;
  end;
  CountSec:=0; fmMainCasher.Timer2.Enabled:=True;
end;

Procedure TestFp(StrOp:String);
begin
  while TestStatus(StrOp,sMessage)=False do
  begin
    fmAttention.Label1.Caption:=sMessage;
    fmAttention.ShowModal;
    Nums.iRet:=InspectSt(Nums.sRet);
    prWriteLog('~~AttentionShow;'+sMessage+';'+'InspectSt(Nums.sRet)='+IntToStr(Nums.iRet));
  end;
end;

Function FindBar(sBar:ShortString):Boolean;
var StrBar,StrWk:ShortString;
    rQ:Real;
begin
  Result:=False;
  ClearSelPos;

  if Length(sBar)>13 then sBar:=Copy(sBar,1,13);
//  if Length(StrBar)>14 then StrBar:=Copy(StrBar,1,14);

  StrBar:=sBar;

{
  if Length(sBar)=13 then
  begin //�������� �� ��� �������
    StrWk:=Copy(sBar,1,2);
    if (StrWk='21') or (StrWk='22') or (StrWk='23') or (StrWk='27') then StrBar:=Copy(sBar,1,7);
  end;
}
  //������� ��� ��� ���� - ���� �� ��������� �� ��� ��� �������
  with dmC do
  begin
    quFindBar.Active:=False;
    quFindBar.ParamByName('SBAR').AsString:=StrBar;
    quFindBar.Active:=True;

    quFindBar.First;
    if quFindBar.RecordCount>0 then
    begin
      rQ:=1;
      if quFindBarCARDSIZE.AsString='QUANTITY' then
      begin //������ ����������
        fmCalc.CalcEdit1.Value:=1;
        fmCalc.ShowModal;
        if fmCalc.ModalResult=mrOk then rQ:=fmCalc.CalcEdit1.EditValue;
      end;
        //��� ����������
      SelPos.Articul:=quFindBarARTICUL.AsString;
      SelPos.Bar:=StrBar;
      SelPos.Name:=quFindBarNAME.AsString;
      SelPos.Quant:=rQ;
      if quFindBarQUANTITY.AsFloat<>1 then SelPos.Quant:=quFindBarQUANTITY.AsFloat;
      SelPos.Price:=rv(quFindBarPRICE_RUB.AsFloat);
      if quFindBarPRICERUB.AsFloat>0 then SelPos.Price:=rv(quFindBarPRICERUB.AsFloat);
      if CommonSet.DepartId>0 then SelPos.Depart:=CommonSet.DepartId
      else SelPos.Depart:=quFindBarDEPART.AsInteger;
      SelPos.DProc:=0;
      SelPos.DSum:=0;
      SelPos.EdIzm:=quFindBarMESURIMENT.AsString;
      SelPos.AVid:=quFindBarAVID.AsInteger;
      SelPos.Krep:=quFindBarALCVOL.AsFloat;
      SelPos.iNDS:=quFindBarINDS.AsInteger;

      quFindBar.Active:=False;
      Result:=True;
    end else
    begin   //�������� �� �������
      quFindBar.Active:=False;
      if Length(sBar)=13 then
      begin //�������� �� ��� �������
        StrWk:=Copy(sBar,1,1);
        if (StrWk='2') then     //��� ����������� �� - ��������� ��� �������
        begin
          StrBar:=Copy(sBar,1,7);
          quFindBar.Active:=False;
          quFindBar.ParamByName('SBAR').AsString:=StrBar;
          quFindBar.Active:=True;

          quFindBar.First;
          if quFindBar.RecordCount>0 then  //����� �������
          begin
            Result:=True;

            //�������� ������� ������� �����
            StrWk:=Copy(sBar,8,5);
            rQ:=StrToIntDef(StrWk,0)/1000;
            if rQ=0 then rQ:=1;
            if rQ<>0 then
            begin
              SelPos.Articul:=quFindBarARTICUL.AsString;
              SelPos.Bar:=StrBar;
              SelPos.Name:=quFindBarNAME.AsString;
              SelPos.Quant:=rQ;
              SelPos.Price:=rv(quFindBarPRICE_RUB.AsFloat);  //�� ��������
              if quFindBarPRICERUB.AsFloat>0 then SelPos.Price:=rv(quFindBarPRICERUB.AsFloat);
              if CommonSet.DepartId>0 then SelPos.Depart:=CommonSet.DepartId
              else SelPos.Depart:=quFindBarDEPART.AsInteger;
              SelPos.DProc:=0;
              SelPos.DSum:=0;
              SelPos.EdIzm:=quFindBarMESURIMENT.AsString;
              SelPos.AVid:=quFindBarAVID.AsInteger;
              SelPos.Krep:=quFindBarALCVOL.AsFloat;
              SelPos.iNDS:=quFindBarINDS.AsInteger;
            end else result:=False;
          end;
        end;
      end;
    end;
  end;
end;


procedure PrintNFDoc(iType:SmallInt);
{Var sC:String;
    iNumZ,iNumCh,iPos:Integer;
    s1,s2:String;
    CHECKDATE:TDateTime;
    CASHER:INTEGER;
    CHECKSUM:Real;
    CHECKPAY:Real;
    CHECKDSUM:Real;
    OPERATION:Integer;
    StrWk,StrWk1:String;
    bNotErr:Boolean;}
begin
  //����� ����
{  sC:=TextEdit3.Text;
  if sC='' then
  begin
    iNumZ:=Nums.ZNum;
    iNumCh:=Nums.iCheckNum;
  end
  else
  begin
    iPos:=0;
    if iPos=0 then iPos:=pos('.',sc);
    if iPos=0 then iPos:=pos(',',sc);
    if iPos>0  then
    begin //�� ������
      s1:=Copy(sc,1,iPos-1);
      s2:=Copy(sc,iPos+1,length(sc)-iPos);
      iNumZ:=StrToIntDef(s1,0);
      iNumCh:=StrToIntDef(s2,0);
    end
    else //������ ����� ����
    begin
      iNumZ:=Nums.ZNum;
      iNumCh:=StrToIntDef(sC,0);
    end;
  end;
  FormLog('CopyCheck','CashNum '+INtToStr(CommonSet.CashNum)+' CashZ '+INtToStr(iNumZ)+' CheckNum '+INtToStr(iNumCh));
  Memo2.Clear;
  Memo2.Lines.Add('������ ����� ���� (�����:'+INtToStr(iNumZ)+', ���:'+INtToStr(iNumCh)+').');

  if (iNumZ>0)and(iNumCh>0) then
  begin
    with dmC do
    begin
      prReadCheck.ParamByName('SHOPINDEX').AsInteger:=CommonSet.ShopIndex;
      prReadCheck.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
      prReadCheck.ParamByName('CASHZ').AsInteger:=iNumZ;
      prReadCheck.ParamByName('CHECKNUM').AsInteger:=iNumCh;

      prReadCheck.ExecProc;

      CHECKDATE:=prReadCheck.ParamByName('CHECKDATE').AsDateTime;
      CASHER:=prReadCheck.ParamByName('CASHER').AsInteger;
      CHECKSUM:=prReadCheck.ParamByName('CHECKSUM').AsFloat;
      CHECKPAY:=prReadCheck.ParamByName('CHECKPAY').AsFloat;
      CHECKDSUM:=prReadCheck.ParamByName('CHECKDSUM').AsFloat;
      OPERATION:=prReadCheck.ParamByName('OPERATION').AsInteger;

      taCheck.Active:=False;
      taCheck.Active:=True;
      taCheck.First;
      while not taCheck.Eof do
      begin
        if fmMainCasher.GridCh.Height<329 then fmMainCasher.GridCh.Height:=fmMainCasher.GridCh.Height+18;
//        Memo2.Lines.Add(taCheckName.AsString);
        taCheck.Next;
      end;

      delay(33);

      //������ ������������� ���������

      bNotErr:=True;
      OpenNFDoc;
      SelectF(10);
      if bNotErr then bNotErr:=PrintNFStr('����� ���� �'+IntToStr(iNumCh));
      StrWk:=CommonSet.DepartName;
      while length(strwk)<iWideStr do StrWk:=' '+StrWk;
      if bNotErr then bNotErr:=PrintNFStr('�����'+StrWk);
      if bNotErr then bNotErr:=PrintNFStr('      ');
      if bNotErr then bNotErr:=PrintNFStr('�����: '+INtToStr(iNumZ)+'            ������: '+IntToStr(CASHER));
      if bNotErr then bNotErr:=PrintNFStr('����:   '+FormatDateTime(sFormatDate,CHECKDATE));

      SelectF(3);
      StrWk:='                                                       ';
      if bNotErr then bNotErr:=PrintNFStr(StrWk);
      SelectF(10);
      StrWk:=' ��������          ���-��   ����   ����� ';
      if bNotErr then bNotErr:=PrintNFStr(StrWk);
      SelectF(3);
      StrWk:='                                                       ';
      if bNotErr then bNotErr:=PrintNFStr(StrWk);

      SelectF(15);
      StrWk:='';
      if bNotErr then bNotErr:=PrintNFStr(StrWk);

      Case Operation of
      0: StrWk:='        �������� - ������� ��������         ';
      1: StrWk:='        �������� - ������� ��������         ';
      2: StrWk:='        �������� - ������� ��               ';
      3: StrWk:='        �������� - ������� ��               ';
      end;

      SelectF(3);
      if bNotErr then bNotErr:=PrintNFStr(StrWk);

      taCheck.First;
      while not taCheck.Eof do
      begin
        StrWk:= Copy((taCheckARTICUL.AsString+' '+taCheckName.AsString),1,40);
//        while Length(StrWk)<20 do StrWk:=StrWk+' '; //21
        SelectF(5);
        if bNotErr then bNotErr:=PrintNFStr(StrWk);

        Str(taCheckPrice.AsFloat:7:2,StrWk1); StrWk1:=DelSp(StrWk1);
        StrWk:=StrWk1; //

        Str(taCheckQUANT.AsFloat:6:3,StrWk1); StrWk1:=DelSp(StrWk1);
        StrWk:=StrWk+' X '+StrWk1; //


        while Length(StrWk)<30 do StrWk:=StrWk+' ';

        Str(taCheckRSUM.AsFloat:9:2,StrWk1);
        StrWk:=StrWk+' '+StrWk1+'�'; //
        SelectF(10);
        if bNotErr then bNotErr:=PrintNFStr(StrWk);
        taCheck.Next;
      end;

      SelectF(3);
      StrWk:='                                                       ';
      if bNotErr then bNotErr:=PrintNFStr(StrWk);

      SelectF(10);
      Str(CHECKSUM:8:2,StrWk1);
      StrWk:=' �����                      '+StrWk1+' ���';
      if bNotErr then bNotErr:=PrintNFStr(StrWk);

      if CHECKDSUM<>0 then
      begin
        SelectF(10);
        Str(CHECKDSUM:5:2,StrWk1);
        StrWk:='C����� - '+StrWk1+'���';
        if bNotErr then bNotErr:=PrintNFStr(StrWk);

        SelectF(10);
        Str((CHECKSUM-CHECKDSUM):8:2,StrWk1);
        StrWk:=' �����                      '+StrWk1+' ���';
        if bNotErr then bNotErr:=PrintNFStr(StrWk);
      end;

      SelectF(10);
      Str(CHECKPAY:8:2,StrWk1);
      StrWk:=' �������� �� ����������     '+StrWk1+' ���';
      if bNotErr then bNotErr:=PrintNFStr(StrWk);

      SelectF(10);
      Str((CHECKSUM-CHECKDSUM-CHECKPAY):8:2,StrWk1);
      StrWk:=' �����                      '+StrWk1+' ���';
      if bNotErr then bNotErr:=PrintNFStr(StrWk);

      StrWk:='';
      PrintNFStr(StrWk);
      StrWk:=' ��������!  ������������ ��������.';
      if bNotErr then PrintNFStr(StrWk);

      CloseNFDoc;
      CutDoc;
      delay(33);

      prClearCheck.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
      prClearCheck.ExecProc;

      taCheck.Active:=False;
      taCheck.Active:=True;

      ViewChDPROC.Visible:=False;
      ViewChDSUM.Visible:=False;

      Label17.Caption:='...';
      Label18.Caption:='';
      Label18.Visible:=False;
      Label19.Caption:='';
      Label19.Visible:=False;
      Label24.Caption:='';
      Label24.Visible:=False;

      ClearPos;
      ClearCheck;
      CalcSum;
    end;
  end;

  CountSec:=0; fmMainCasher.Timer2.Enabled:=True;
  TextEdit3.Text:='';}
end;


Procedure ToCustomDisp(Str1,Str2:String);
Var Buff:array[1..44] of byte;
    i:INteger;
    sStr:string;
begin
  if CommonSet.TypeDP=4 then
  begin
    if (CommonSet.TypeFis='sp101') then
    begin
      SetDP(Str1,Str2);
    end;
  end else
  begin

  if pos('COM',CommonSet.PortDP)=0 then exit;
  with fmMainCasher do
  begin
    try
      if devCustD.Connected=False then
      begin
        DevCustD.Port:=CommonSet.PortDP;
{        DevCustD.Baudrate:=br9600;
        DEvCustD.Parity:=paNone;
        DEvCustD.Stopbits:=sb1;
        DEvCustD.Databits:=db8;
        DevCustD.Open;         }
        DevCustD.Open;
      end;
    except
    end;
    if devCustD.Connected then
    begin
      if Length(Str1)>20 then Str1:=Copy(Str1,1,20);
      if Length(Str2)>20 then Str2:=Copy(Str2,1,20);

      Str1:=AnsiToOemConvert(Str1);
      Str2:=AnsiToOemConvert(Str2);

      if CommonSet.TypeDP=1 then
      begin
        {
        Buff[1]:=$0C;
        devCustD.WriteBuf(Buff,1); //T���
        delay(50);
        for i:=1 to 44 do Buff[i]:=$20; //����� ���������
        Buff[1]:=$1B; Buff[2]:=$51; Buff[3]:=$41;
        for i:=1 to Length(Str1) do Buff[i+3]:=ord(Str1[i]);
        Buff[44]:=$0D;
        devCustD.WriteBuf(Buff,44); //1 ������
//      devCustD.WriteBuf(Buff,43); //1 ������
        delay(50);


        for i:=1 to 44 do Buff[i]:=$20; //����� ���������
        Buff[1]:=$1B; Buff[2]:=$51; Buff[3]:=$42;
        for i:=1 to Length(Str2) do Buff[i+3]:=ord(Str2[i]);
        Buff[44]:=$0D;
        devCustD.WriteBuf(Buff,44); //2 ������
        }
        devCustD.WriteStr(#$0C);
        delay(50);

        sStr:='';
        for i:=1 to 44 do sStr:=sStr+#$20; //�������� ���������
        sStr[1]:=#$1B; sStr[2]:=#$51; sStr[3]:=#$41;

        for i:=1 to Length(Str1) do sStr[i+3]:=Str1[i];
        sStr[44]:=#$0D;
        devCustD.WriteStr(sStr);
        delay(50);

        sStr:='';
        for i:=1 to 44 do sStr:=sStr+#$20; //�������� ���������
        sStr[1]:=#$1B; sStr[2]:=#$51; sStr[3]:=#$42;

        for i:=1 to Length(Str2) do sStr[i+3]:=Str2[i];
        sStr[44]:=#$0D;
        devCustD.WriteStr(sStr); //

      end;
      if CommonSet.TypeDP=2 then
      begin
        devCustD.WriteStr(#$0C);
        delay(50);

        sStr:='';
        for i:=1 to 23 do sStr:=sStr+#$20; //�������� ���������
        sStr[1]:=#$1B; sStr[2]:=#$51; sStr[3]:=#$41;

        for i:=1 to Length(Str1) do sStr[i+3]:=Str1[i];
        sStr[44]:=#$0D;
        devCustD.WriteStr(sStr);
        delay(50);

        sStr:='';
        for i:=1 to 23 do sStr:=sStr+#$20; //�������� ���������
        sStr[1]:=#$1B; sStr[2]:=#$51; sStr[3]:=#$42;

        for i:=1 to Length(Str2) do sStr[i+3]:=Str2[i];
        sStr[44]:=#$0D;
        devCustD.WriteStr(sStr); //

      end;
      if CommonSet.TypeDP=3 then
      begin
        devCustD.WriteStr(#$0C);
        delay(50);

        sStr:=#$1B+#$52+#$0c;
        devCustD.WriteStr(sStr);
        delay(50);

        sStr:=#$1B+#$74+#$06;
        devCustD.WriteStr(sStr);
        delay(50);

        sStr:=#$0B;
        for i:=1 to 40 do sStr:=sStr+#$20;
        for i:=1 to Length(Str1) do sStr[i+1]:=Str1[i];
        for i:=1 to Length(Str2) do sStr[i+21]:=Str2[i];
        devCustD.WriteStr(sStr);
        delay(50);
      end;

      delay(50);
    end;
  end;
  end;
end;


Function prAddPosSel(Param,TestVes:Integer):Boolean;
Var iMax:Integer;
    StrWk:String;
    rSum1:Real;
    iSumPos,iR:Integer;
    bStop:Boolean;
    bAdd:Boolean;
    iSt:Integer;
begin
  Result:=True;

  if (CurPos.Articul='') then
  begin
{    if More24H(iSt) then
    begin
      ShowMessage('�������� �����. (����� 24�)');
      Result:=False;
      exit;
    end;}
  end;

  if (bAddPosSel)and(TestVes=1) then
  begin
    prWriteLog('!!prAddPosSelDoubleStart;'+its(Param)+'; ��� SelPos '+SelPos.Articul+'(CurPos '+CurPos.Articul+'); ���-�� SelPos '+fs(SelPos.Quant)+'(CurPos '+fs(CurPos.Quant)+'); � SelPos '+fs(SelPos.Price)+'('+fs(CurPos.Price)+');');
    exit;
  end;

  if bStopAddPos then
  begin
    prWriteLog('!!prStopAddPosSel; ������ ���������� ������� ����� ���������');
    exit;
  end;

  try
    bAddPosSel:=True;

  //������������ ������� ������
  fmMainCasher.Timer2.Enabled:=False;
  CountSec:=0;

  Check.ChBeg:=True;
  Check.ChEnd:=False;
  bBegCash:= False; //�������������� ��������� ������ ��� ������� ��������
  bBegCashEnd:= False;
  bStop:=False;

  with dmC do
  begin
    //����� ���� �� ������ ������ � ������ �� �������, �� ����� iMax ��� ����������� ������ ����

    if taCheck.Active=False then
    begin
      taCheck.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
      taCheck.Active:=True;
    end else taCheck.FullRefresh;
    taCheck.Last;

    if (CurPos.Articul>'') then
    begin
      if taCheck.RecordCount>0 then iMax:=taCheckNUMPOS.AsInteger+2 //����� ����� �������  //��������� + �������
      else iMax:=2;
    end else
    begin
      if taCheck.RecordCount>0 then iMax:=taCheckNUMPOS.AsInteger+1 //����� ����� �������  //��������� ��� �������
      else iMax:=1;
    end;

    SelPos.NumPos:=iMax;

    //���� ���� ������� �� ����������� � ����
    if (CurPos.Articul>'')and(abs(CurPos.Quant)>0.00001)  then //������� � �������� ����������
    begin
        // ���������� �� � ���
      try
        prWriteLog('!!prAddPosSel;'+its(Param)+'; ��� SelPos '+SelPos.Articul+'(CurPos '+CurPos.Articul+'); ���-�� SelPos '+fs(SelPos.Quant)+'(CurPos '+fs(CurPos.Quant)+'); � SelPos '+fs(SelPos.Price)+'(CurPos '+fs(CurPos.Price)+');���. '+its(iMax));

        if iMax=2 then
        begin
          //���� ������ ����� ��������� ���
          //������� ��������� �����
          if CommonSet.PrintFisCheckOnEnd=0 then
          begin
            //�������� ������� �����
            Nums.iRet:=InspectSt(Nums.sRet);
            if Nums.iRet<>0 then TestFp('InspectSt');

            if not OpenZ then ZOpen;

            Case Check.Operation of
            0: begin
                 if CheckRetStart=False then bStop:=True;
               end;
            1: begin
                 if CheckStart=False then bStop:=True;
               end;
            end;

            if bStop then  //��� ������� � ���� ���������
            begin
              Check.ChBeg:=False;
              Check.ChEnd:=True;
              ClearSelPos;
              ClearPos;
              ResetPos; //�������� � ���������
              CalcSum;

              fmMainCasher.TextEdit3.Text:='';
              fmMainCasher.TextEdit3.SetFocus;

              TestStatus('StartCheck',StrWk);
              fmMainCasher.Memo2.Clear;
              fmMainCasher.Memo2.Lines.Add(StrWk);
              fmAttention.Label1.Caption:=StrWk;
              fmAttention.ShowModal;
              bAddPosSel:=False;
              exit;
            end;
          end;
        end;

        if fmMainCasher.tag=0 then
          if fmMainCasher.GridCh.Height<329 then fmMainCasher.GridCh.Height:=fmMainCasher.GridCh.Height+18;
        if (fmMainCasher.tag=1)or(fmMainCasher.tag=17) then
          if fmMainCasher.GridCh.Height<280 then fmMainCasher.GridCh.Height:=fmMainCasher.GridCh.Height+18;

        bAdd:=True;
        rSum1:=CurPos.Quant*rv(CurPos.Price);
        iR:=RoundEx(rSum1*100);

//      ��� ���������� � ��������� ��������
        if CommonSet.PrintFisCheckOnEnd=0 then bAdd:=CheckAddPos(iSumPos) else iSumPos:=iR;

        if bAdd then
        begin
          trUpdCh.StartTransaction;

          taCheck.Append;
          taCheckCASHNUM.AsInteger:=CommonSet.CashNum;
          taCheckNUMPOS.AsInteger:=iMax-1;
          taCheckARTICUL.AsString:=CurPos.Articul;
          taCheckRBAR.AsString:=CurPos.Bar;
          taCheckNAME.AsString:=CurPos.Name;
          taCheckQUANT.AsFloat:=CurPos.Quant;
          taCheckPRICE.AsFloat:=rv(CurPos.Price);
          taCheckDPROC.AsFloat:=CurPos.DProc;
          taCheckDSUM.AsFloat:=rv(CurPos.DSum);
          taCheckRSUM.AsFloat:=rv(rSum1);
          taCheckDEPART.AsInteger:=CurPos.Depart;
          taCheckEDIZM.AsString:=CurPos.EdIzm;
          taCheckTIMEPOS.AsDateTime:=now;
          taCheckAMARK.AsString:=CurPos.AMark;
          taCheck.Post;

          trUpdCh.Commit;

          if abs(iR-iSumPos)>0 then
          begin //����������� ����� �� �������, ���������� ������ ���-�� �����������
            prWriteLog('~~CheckAddPos1;'+IntToStr((Nums.iCheckNum))+';'+CurPos.Articul+';'+CurPos.Bar+';'+FloatToStr(CurPos.Quant)+';'+FloatToStr(CurPos.Price)+';'+FloatToStr(CurPos.DSum)+';��-'+INtToStr(iSumPos)+';����-'+INtToStr(iR));
            prClearCheck1;  //��������� ���
            ClearSelPos;
            ClearPos;
            fmMainCasher.TextEdit3.Text:='';
          end else
          begin //����������� �������� �� ����� 1 ���, ���������� � ����
            trUpdCh.StartTransaction;
            taCheck.Edit;
            taCheckRSUM.AsFloat:=iSumPos/100;
            taCheck.Post;
            trUpdCh.Commit;
            ClearPos;
            ResetPos; //�������� � ���������
            fmMainCasher.TextEdit3.Text:='';
          end;

          //��������� - ���������� �������

          if (TestVes=1)and(Param=0) then prTestVes;

          if (SelPos.Articul>'')and(abs(SelPos.Quant)>0.00001)  then //��������� � �������� ����������
          begin
            ClearPos;
            CurPos:=SelPos;
            ClearSelPos;

            if Param=0 then  //���� �� �������� ������������ ������� (���������� � "-" �����������) �� ������ ������.
            begin
              CalcDiscont(CurPos.Articul,(Nums.iCheckNum),rv(CurPos.Price),CurPos.Quant,0,Check.DiscProc,CurPos.DProc,CurPos.DSum);
              CurPos.DSum:=RoundEx(CurPos.DSum*100)/100;
            end;
            //���� ���
            prWriteLog('~!Real;CurPos; ��� '+IntToStr((Nums.iCheckNum))+'; ��� CurPos '+CurPos.Articul+'; �� CurPos '+CurPos.Bar+';��� CurPos '+fs(CurPos.Quant)+';� CurPos '+fs(CurPos.Price)+';��������� CurPos '+fs(CurPos.DSum)+';');

            with fmMainCasher do
            begin
              if CurPos.DSum<>0 then
              begin
                //������ ������������
                Str(CurPos.DProc:5:2,StrWk); While Pos(' ',StrWk)>0 do Delete(StrWk,Pos(' ',StrWk),1);
                Label18.Caption:='������ '+StrWk+'%';
                Label18.Visible:=True;
                Str(CurPos.DSum:10:2,StrWk); While Pos(' ',StrWk)>0 do Delete(StrWk,Pos(' ',StrWk),1);
                Label19.Caption:=StrWk+'�.';
                Label19.Visible:=True;
              end else
              begin
                Label18.Visible:=False;
                Label19.Visible:=False;
              end;
            end;
            ResetPos;
            CalcSum;
          end else
          begin
            prWriteLog('~~ZerroSelPos;'+IntToStr((Nums.iCheckNum))+';'+SelPos.Articul+';'+SelPos.Bar+';'+FloatToStr(SelPos.Quant)+';'+FloatToStr(SelPos.Price)+';'+FloatToStr(SelPos.DSum));
//            ClearPos; ������ ��� ������� ������
            ClearSelPos;
            ResetPos;
            CalcSum;
          end;
        end else TestFp('CheckAddPos');
      except;
        trUpdCh.Rollback;
      end;
    end else
    begin // ��� �������
      if iMax=1 then  //  ��� ����� ������ ������� - ���� ��� ��� ��� �� �������  ,������ ��� ������ �������
      begin
        //������� ��������� �����
        if not OpenZ then ZOpen;

        GetNumCh; //����� ����
        prWriteLog('');
        prWriteLog('~!BegCheck;'+IntToStr((Nums.iCheckNum))+';');
      end;

//��������� - ���������� �������
      prWriteLog('!!prAddPosSel;_(������� ���);'+its(Param)+'; ��� SelPos '+SelPos.Articul+'(CurPos '+CurPos.Articul+'); ���-�� SelPos '+fs(SelPos.Quant)+'(CurPos '+fs(CurPos.Quant)+'); � SelPos '+fs(SelPos.Price)+'(CurPos '+fs(CurPos.Price)+');���. '+its(iMax));

//      CurPos:=SelPos;
//      if abs(SelPos.Quant)>0.00001 then
      if (TestVes=1)and(Param=0) then prTestVes;

      if (SelPos.Articul>'')and(abs(SelPos.Quant)>0.00001) then //��������� � �������� ����������
      begin

        ClearPos;   //
        CurPos:=SelPos;
        ClearSelPos; //����� �������

        if Param=0 then  //���� �� �������� ������������ ������� (���������� � "-" �����������) �� ������ ������.
        begin
          CalcDiscont(CurPos.Articul,(Nums.iCheckNum),rv(CurPos.Price),CurPos.Quant,0,Check.DiscProc,CurPos.DProc,CurPos.DSum);
          CurPos.DSum:=RoundEx(CurPos.DSum*100)/100;
        end;
        //���� ���
        prWriteLog('~!Real;_;CurPos; ��� '+IntToStr((Nums.iCheckNum))+'; ��� CurPos '+CurPos.Articul+'; �� CurPos '+CurPos.Bar+';��� CurPos '+fs(CurPos.Quant)+';� CurPos '+fs(CurPos.Price)+';��������� CurPos '+fs(CurPos.DSum)+';');

        with fmMainCasher do
        begin
          if CurPos.DSum<>0 then
          begin
          //������ ������������
            Str(CurPos.DProc:5:2,StrWk); While Pos(' ',StrWk)>0 do Delete(StrWk,Pos(' ',StrWk),1);
            Label18.Caption:='������ '+StrWk+'%';
            Label18.Visible:=True;
            Str(CurPos.DSum:10:2,StrWk); While Pos(' ',StrWk)>0 do Delete(StrWk,Pos(' ',StrWk),1);
            Label19.Caption:=StrWk+'�.';
            Label19.Visible:=True;
          end else
          begin
            Label18.Visible:=False;
            Label19.Visible:=False;
          end;
        end;
        ResetPos;
        fmMainCasher.TextEdit3.Text:='';

        CalcSum;
      end else
      begin
        prWriteLog('~~ZerroSelPos1;'+IntToStr((Nums.iCheckNum))+';'+SelPos.Articul+';'+SelPos.Bar+';'+FloatToStr(SelPos.Quant)+';'+FloatToStr(SelPos.Price)+';'+FloatToStr(SelPos.DSum));
//        ClearPos; //������� ������� ������
        ClearSelPos;
        ResetPos;
        fmMainCasher.TextEdit3.Text:='';

        CalcSum;
      end;
    end;

  end;
  finally
    bAddPosSel:=False;
  end;
end;



Procedure WriteStatus;
begin
  with fmMainCasher do
  begin
    Label14.Caption:='����� � '+IntToStr(CommonSet.CashNum)+'  ���. � '+its(CommonSet.Shop);
    Label9.Caption:='���. ����  '+DateToStr(date);
    Label10.Caption:='��� � '+IntToStr(Nums.iCheckNum);
    Label15.Caption:='������ ���: ������ ('+IntToStr(Nums.iRet)+')';
    Label8.Caption:='����� � '+INtToStr(Nums.ZNum);
    Label22.Caption:='������� '+Nums.sDateTimeBeg;

    WriteStatusFN;

    if Nums.iRet=0 then Label15.Caption:='������ ���: '+Nums.sRet;
    if (Nums.iRet=2)or(Nums.iRet=22) then Label15.Caption:='������ ���: ���������� ������� �����.';

    Case Check.Operation of
    0: Label16.Caption:='�������� "�������"';
    1: Label16.Caption:='�������� "�������"';
    end;

    if devCustD.Connected then Label20.Caption:='�� ('+CommonSet.PortDP+') ��.'
    else Label20.Caption:='�� ('+CommonSet.PortDP+') �����������';

    if devScaner.Connected then Label21.Caption:='������ ('+CommonSet.PortSC+') ��.'
    else Label21.Caption:='������ ('+CommonSet.PortSC+') �����������'
  end;
end;

Procedure ClearCheck;
begin
  Check.ChBeg:=False;
  Check.ChEnd:=True;
  bAddPosSel:=False;
  bStopAddPos:=False;   //������ �� ���������� �� �����

//��� ���� ����� ���� ����� � �����
//  inc(Check.Num);
//  WriteIni; //�������� ����� ���� - ��� �������������� ������ - ����� ��� �� �����.

  Check.Discount:='';
  Check.DiscProc:=0;
  Check.DiscName:='';
  Check.RSum:=0;
  Check.DSum:=0;
  Check.Operation:=1; //������� ��������� �� ���������
  Check.ECheck:=0;
  fmMainCasher.GridCh.Height:=55;
  fmMainCasher.Label17.Caption:='...';
  fmMainCasher.Label18.Caption:='';
  fmMainCasher.Label19.Caption:='';
  fmMainCasher.Label24.Caption:='';

  Check.ECheck:=0; //��� ������ ���
  Check.PayType:=0; //���
end;

Procedure prSaveAndCloseCheck(rPay:Real);
begin
  with dmC do
  begin
    fmMainCasher.ViewCh.BeginUpdate;

    prSaveCheck.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
    prSaveCheck.ParamByName('SHOPINDEX').AsInteger:=CommonSet.ShopIndex;
    prSaveCheck.ParamByName('ZNUMBER').AsInteger:=Nums.ZNum;
    prSaveCheck.ParamByName('NUMCHECK').AsInteger:=Nums.iCheckNum+1;
    prSaveCheck.ParamByName('CASHER').AsInteger:=Person.Id;
    prSaveCheck.ParamByName('OPERATION').AsInteger:=Check.Operation; //������� ������� /1/
    prSaveCheck.ParamByName('PAYSUM').AsFloat:=rPay;
    prSaveCheck.ParamByName('DBAR').AsString:=Check.Discount;

    prSaveCheck.ExecProc;

    taCheck.Active:=False;
    taCheck.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
    taCheck.Active:=True;

    fmMainCasher.ViewCh.EndUpdate;

    with fmMainCasher do
    begin
      ViewChDPROC.Visible:=False;
      ViewChDSUM.Visible:=False;

      Label17.Caption:='...';
      Label18.Caption:='';
      Label18.Visible:=False;
      Label19.Caption:='';
      Label19.Visible:=False;
      Label24.Caption:='';
      Label24.Visible:=False;
    end;

    ClearPos;
    ClearSelPos;
    ClearCheck;
    ResetPos;
    CalcSum;
  end;
end;


Procedure CalcSum;
Var rSum:Real;
    rSum1:Real;
    rSum0:Real;
    rProc:Real;
    StrWk,StrWk1:String;
    iR:Integer;
begin
  with dmC do
  begin
    taCheck.Active:=False;
    taCheck.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
    taCheck.Active:=True;

    fmMainCasher.ViewCh.BeginUpdate;
    rSum1:=rv(CurPos.Quant*rv(CurPos.Price))*100;
    iR:=RoundEx(rSum1);
    rSum:=iR/100-CurPos.DSum;
    rSum0:=iR/100;

    taCheck.First;
    while not taCheck.Eof do
    begin
      rSum:=rSum+taCheckRSum.AsFloat-taCheckDSUM.AsFloat;
      rSum0:=rSum0+taCheckRSum.AsFloat;
      taCheck.Next;
    end;

    //prRecalcDisc(rSum0);

    fmMainCasher.ViewCh.EndUpdate;

    fmMainCasher.CurrencyEdit3.EditValue:=rv(rSum);

    Check.RSum:=rSum;

    if abs(rSum0-rSum)>0.001 then   //������ ����
    begin
      fmMainCasher.ViewChDPROC.Visible:=True;
      fmMainCasher.ViewChDSUM.Visible:=True;
      rProc:=RoundEx((rSum0-rSum)/rSum0*100*10)/10;
      Str(rProc:5:2,StrWk); StrWk:=DelSp(StrWk);
      Str((rSum0-rSum):10:2,StrWk1); StrWk1:=DelSp(StrWk1);
      fmMainCasher.Label24.Caption :='������ ����� '+StrWk1+' ���. ('+StrWk+'%)';
      fmMainCasher.Label24.Visible:=True;
    end
    else
    begin
      fmMainCasher.Label24.Caption :='...';
      fmMainCasher.Label24.Visible:=False;
      fmMainCasher.ViewChDPROC.Visible:=False;
      fmMainCasher.ViewChDSUM.Visible:=False;
    end;

    if rSum<>0 then
    begin
      if CurPos.Articul>'' then
      begin
        Str((RoundEx(rSum1)/100):9:2,StrWk);
        while pos(' ',StrWk)>0 do delete(StrWk,pos(' ',StrWk),1);
//        While Length(StrWk)<5 do StrWk:=' '+StrWk;
        StrWk:=Copy(CurPos.Name,1,(20-Length(StrWk)-1))+' '+StrWk; //�������� �� ����

//        StrWk:='����'+StrWk;
      end else StrWk:='';
      Str(rSum:9:2,StrWk1); While Length(StrWk1)<15 do StrWk1:=' '+StrWk1;
      StrWk1:='�����'+StrWk1;
      ToCustomDisp(StrWk,StrWk1);
    end;
{     else //����� �������
    begin
      ToCustomDisp('      �������       ','    �� ������� !    ');
    end;}

    fmMainCasher.TextEdit3.SetFocus;
  end;
end;

Procedure ResetPos;
Var rSum,rSum1:Real;
begin
  with fmMainCasher do
  begin
    TextEdit1.Text:=CurPos.Bar;
    TextEdit2.Text:=CurPos.Articul;
    Memo1.Clear;
    Memo1.Lines.Add(Curpos.Name);
    CalcEdit1.EditValue:=CurPos.Quant;
    CurrencyEdit2.EditValue:=CurPos.Price;
    rSum1:=CurPos.Price*CurPos.Quant*100;
    rSum:=RoundEx(rSum1)/100;
    CurrencyEdit1.EditValue:=rSum;
  end;
end;

Procedure ClearSelPos;
begin
  SelPos.Articul:='';
  SelPos.Bar:='';
  SelPos.Name:='';
  SelPos.Quant:=0;
  SelPos.Price:=0;
  SelPos.DProc:=0;
  SelPos.DSum:=0;
  SelPos.EdIzm:='';
  SelPos.TypeIzm:=0;
  SelPos.FisPos:=0;
  SelPos.NumPos:=0;
  SelPos.AVid:=0;
  SelPos.AMark:='';
  SelPos.Krep:=0;
  SelPos.iNDS:=0;
end;


Procedure ClearPos;
begin
  CurPos.Articul:='';
  CurPos.Bar:='';
  CurPos.Name:='';
  CurPos.Quant:=0;
  CurPos.Price:=0;
  CurPos.DProc:=0;
  CurPos.DSum:=0;
  CurPos.EdIzm:='';
  CurPos.TypeIzm:=0;
  CurPos.FisPos:=0;
  CurPos.AVid:=0;
  CurPos.AMark:='';
  CurPos.Krep:=0;
  CurPos.iNDS:=0;
//  ResetPos; //�������� � ���������
  delay(10);
end;

procedure TfmMainCasher.FormCreate(Sender: TObject);
Var dtCurTime:TDateTime;
    i:Integer;
begin
  fmMainCasher.Top:=0;
  fmMainCasher.Left:=0;
  iNumBar:=0; //��� ����� ���������� �� �����
  for i:=1 to 200 do ArBar[i]:='';

  CurDir := ExtractFilePath(ParamStr(0));
  ReadIni;
  prWriteLog('');
  prWriteLog('~!StartProgram');

  Label27.Caption:='v'+sVer;

  try
    if DirectoryExists(AlcoSet.PathAlco)=False then createdir(AlcoSet.PathAlco);
  except
  end;

  Vid.Width:=800;
  Vid.Height:=600;
  Vid.GrColQuant:=10; Vid.GrColWidth:=107; Vid.GrRowQuant:=8; Vid.GrRowHeight:=45;  Vid.GrTextLen:=14;  Vid.GrFontSize:=8;  Vid.GrFontStyle:=0;
  Vid.Gr1ColQuant:=4; Vid.Gr1ColWidth:=107; Vid.Gr1RowQuant:=12; Vid.Gr1RowHeight:=45; Vid.Gr1TextLen:=14; Vid.Gr1FontSize:=8; Vid.Gr1FontStyle:=0;

  prResetVid;

  Width:=Vid.Width;
  Height:=Vid.Height;

  prSetVisibleAll(False);

  Label13.Caption:='';
  Edit1.Text:='';
  Memo2.Clear;
  if CommonSet.AlkoKrepMax>0 then
  begin
    Memo2.Lines.Add('��� �������� �������� ������ '+IntToStr(CommonSet.AlkoKrepMax)+'% ��������� ������ � '+FormatDateTime('hh:nn ss',CommonSet.WkTimeBeg)+' �� '+FormatDateTime('hh:nn ss',CommonSet.WkTimeEnd));
  end;

//  Memo2.Lines.Add('�������� �� � ������.');
  ClearPos;
  ClearSelPos;
  ClearCheck;
  ResetPos;

//  if Check.Num<0 then Nums.iCheckNum:=-1 else Nums.iCheckNum:=Check.Num-1;

  Nums.ZNum:=CommonSet.ZNum;
  Nums.SerNum:='0000';
  Nums.RegNum:=CommonSet.RegNum;
//  Nums.iCheckNum:=CommonSet.CashNum; //�.�.WriteStatus +1;

  Nums.iCheckNum:=0; //�� ���������
  Nums.CheckNum:=its(Nums.iCheckNum);
  Check.Num:=Nums.iCheckNum;

  Nums.iRet:=0;
  Nums.sRet:='���';
  Nums.sDate:=FormatDateTime(sFormatDate,Date);

  try
    if pos('COM',CommonSet.PortSC)>0 then
    begin
      {
      devScaner.Baudrate:=br9600;
      devScaner.Parity:=paNone;
      devScaner.Stopbits:=sb1;
      devScaner.Databits:=db8;
      devScaner.Active:=True;}
      devScaner.Port:=CommonSet.PortSC;
      devScaner.Open;
    end;
  except
  end;

//  dmC:=tdmC.Create(Application);
 {
  Nums.bOpen:=False;
  bErr:=False;
  try
    if CommonSet.NoFis=0 then //���������� �����
    begin
      if pos('COM',CommonSet.PortCash)>0 then
      begin
        if CashOpen(PChar(CommonSet.PortCash)) then   //dll �������
        begin
          Delay(100);
          Nums.iRet:=InspectSt(Nums.sRet);

          GetSerial;
          Delay(100);
          CashDate(Nums.sDate);
          Delay(100);
          GetNums; //������ ����������  Create
          GetRes; //�������

        end else
        begin
          bErr:=True;
        end;
      end;
    end;
  except
  end;
  WriteStatus;
 }

  dtCurTime:=frac(now);
  if dtCurTime<0.458333 then ToCustomDisp('    ������ ����!    ','                    ') //11 �����
  else
  begin
    if dtCurTime>0.75 then ToCustomDisp('    ������ �����!    ','                    ') //18 �����
    else  ToCustomDisp('    ������ ����!    ','                    ');
  end;

  Label18.Caption:='';
  Label18.Visible:=False;
  Label19.Caption:='';
  Label19.Visible:=False;
  Label24.Caption:='';
  Label24.Visible:=False;

  TimerStart.Interval:=CommonSet.StartDelay*1000;
  Memo2.Lines.Add('�������� ������ - '+its(CommonSet.StartDelay)+' ���.');

  if CommonSet.KeybordType>1 then NumLock(True);
  if CommonSet.Prizma=1 then
  begin
    if OpenDrv then Memo2.Lines.Add(' ������ ��.')
    else Memo2.Lines.Add(' ������  ���.');
  end;
end;

procedure TfmMainCasher.TimerStartTimer(Sender: TObject);
begin
  if bErrCash then
  begin
    bErrCash:=False;
    try
      if CommonSet.NoFis=0 then //���������� �����
      begin
        if pos('COM',CommonSet.PortCash)>0 then
        begin
          if CashOpen(PChar(CommonSet.PortCash)) then   //
          begin
            Delay(100);
            Nums.iRet:=InspectSt(Nums.sRet);
            if Nums.iRet<>0 then ShowMessage(Nums.sRet);

            GetSerial;
            Delay(100);
            CashDate(Nums.sDate);
            Delay(100);
            GetNums; //������ ����������  Create
            GetRes; //�������
            fGetFN; //������ �� ����������� ����������

          end else
          begin
            bErrCash:=True;
          end;
        end;
      end;
    except
    end;

    WriteStatus;
    delay(500);

    if bErrCash then fmMainCasher.Close;
  end;

  TimerStart.Enabled:=False;
  Person.Id:=0;
  Person.Name:='';

  with dmC do
  begin
    CasherDb.DBName:=DBName;
    try
      CasherDb.Open;

      if CasherDb.Connected=True then
      begin
        taPersonal.Active:=True;
        taRClassif.Active:=True;
        quFuncList.Active:=True;
        taFuncList.Active:=True;

        if CommonSet.NoFis=1 then
        begin
          Nums.ZNum:=prGetNFZ;
          Nums.SerNum:='0000';
          Nums.RegNum:='';
          Nums.iCheckNum:=prGetNFCheckNum; //�.�.WriteStatus +1;
          Nums.CheckNum:=INtToStr(Nums.iCheckNum);
          Check.Num:=Nums.iCheckNum;

          WriteStatus;
        end;

        //��� �������� ������ ���, ����� ������ ��� �������
        prUpd1;

        taCheck.Active:=False;
        taCheck.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
        taCheck.Active:=True;
        if taCheck.RecordCount>0 then prSaveCheckA(0);
        taCheck.Active:=False;

        delay(100);

        prClearCheck.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
        prClearCheck.ExecProc;

        //�������� �� ����������

        prSetShortCut(CommonSet.KeybordType);

        if Vid.Vid=2 then //���� ������ ������������
        begin
          CreateHotList;
          CreateHotList1;
        end;
        bScale:=False;

        if pos('COM',CommonSet.PortVesCAS_ER)=1 then
        begin //��������� �������� � ����� ����
          try
            devCas.DeviceName:=CommonSet.PortVesCAS_ER;
            devCas.Open;
            bScale:=True;
          except
            bScale:=False;
          end;
        end;

        CloseTe(teVesT);

        quTestVes.Active:=False;
        quTestVes.Active:=True;
        quTestVes.First;
        while not quTestVes.Eof do
        begin
          if StrToINtDef(quTestVesARTICUL.AsString,0)>0 then
          begin
            teVesT.Append;
            teVesTArticul.AsInteger:=StrToINtDef(quTestVesARTICUL.AsString,0);
            teVesT.Post;
          end;
          quTestVes.Next;
        end;

        quTestVes.Active:=False;


{        teVesT.Append; teVesTArticul.AsInteger:=1481; teVesT.Post;
        teVesT.Append; teVesTArticul.AsInteger:=1538; teVesT.Post;
        teVesT.Append; teVesTArticul.AsInteger:=58009; teVesT.Post;
        teVesT.Append; teVesTArticul.AsInteger:=1539; teVesT.Post;
        teVesT.Append; teVesTArticul.AsInteger:=5268; teVesT.Post;
        teVesT.Append; teVesTArticul.AsInteger:=72039; teVesT.Post;
        teVesT.Append; teVesTArticul.AsInteger:=4089; teVesT.Post;
}
      end;
    except
//    StatusBar1.Panels[0].Text:='���� �� ����������� - '+DBName;
      showmessage('������ ��� �������� ����.');
    end;
  end;

  //������� ����� ��� �������� ����������
  sCurVer:=sVer; //����� ���� ��� ��� �������

  if fNeedUpd then
  begin
    fmAutoUpd.Memo1.Clear;
    fmAutoUpd.Show;
    fmAutoUpd.Memo1.Lines.Add('�������� ���������� ����� ������. ���� �������������� ����������.'); delay(10);
    fmAutoUpd.Memo1.Lines.Add('����� ... ��������������� ����� ���������� 30 ���.'); delay(10);
    fmAutoUpd.Memo1.Lines.Add('  ��������� ������.'); delay(10);
    if fTakeUpd then
    begin
      fmAutoUpd.Memo1.Lines.Add('  ������ ��'); Delay(10);

      try
{        delay(1000);
        if RenameFile(CurDir+'casher.exe', CurDir+'casher.old') then //������� �������������� � old
        begin
          fmAutoUpd.Memo1.Lines.Add('  �������������� casher.exe � casher.old - ��'); Delay(10);
          delay(1000);
        end;
        if RenameFile(CurDir+'casher.new', CurDir+'casher.exe') then //����� � �������
        begin
          fmAutoUpd.Memo1.Lines.Add('  �������������� casher.new � casher.exe - ��'); Delay(10);
          delay(1000);
        end;
        }
        fmAutoUpd.Memo1.Lines.Add('���������� ������ �������. ��������� ����� ����������� ���������.');
        delay(1000);

        if FileExists(CurDir+'Reloader.exe') then
        begin
          ShellExecute(handle, 'open', PChar(CurDir+'Reloader.exe'),'','', SW_SHOWNORMAL);
        end;
        close;    //�������� ��������� � ����� �� ���������
      except
        fmAutoUpd.Memo1.Lines.Add('������ ����������. ���������� � ���������������.');
      end;
    end else
      fmAutoUpd.Memo1.Lines.Add('������ ����������. ���������� � ���������������.');
  end;

  fmPre.Label1.Caption:='v'+sVer;
  fmPre.ShowModal;
end;

procedure TfmMainCasher.acBarExecute(Sender: TObject);
begin
  if bAddPos then exit;
  Edit1.Text:='';
  Edit1.SelectAll;
  Edit1.SetFocus;
end;

procedure TfmMainCasher.Edit1KeyPress(Sender: TObject; var Key: Char);
Var sBar:ShortString;
    bCh:Byte;
    CurrTime:TDateTime;
    StrN:String;
    bRasch:Boolean;
    rKrep:Real;

    StrWk:String;
    rDProc:Real;
    rDSum:Real;
    iNumPos:ShortInt;
    sGr:String;
    sMess:String;
    sDisc:String;

begin
  bCh:=ord(Key);
//  if bCh=10 then Key:=#0;
  if (bCh = 13)or(bCh = 82) then    //82 ���� ���������� Ctrl-R ����� ��������� ������������� ��������� �������
  begin //���� ����������                       //10-0A
//    showmessage('Enter');
    bAddPos:=True;
    Edit1.SelectAll;
    Edit1.Enabled:=False;
    sBar:=Edit1.Text;
    Label12.Caption:=sBar;

    //�����������, ������� ��� �����, ����� ��� ���������

    if FindBar(SOnlyDigit1(sBar)) then  //��� ����� �� ��  -  ����� ����������� SelPos....
    begin  //����� ����� �� ��
      if SelPos.Price=0 then
      begin
        Memo2.Clear;
        Memo2.Lines.Add(ZerroMess);
        fmAttention.Label1.Caption:=ZerroMess;
        fmAttention.ShowModal;
      end else
      begin
        bRasch:=True;
        CurrTime:=frac(now);
        if (CommonSet.AlkoKrepMax>0)and((CurrTime<CommonSet.WkTimeBeg)or((CurrTime>CommonSet.WkTimeEnd))) then   //�������� ��������
        begin
          StrN:=SelPos.Name;
          //�������� ��������
          if pos('%',StrN)>0 then
          begin
            StrN:=Copy(StrN,1,pos('%',StrN)-1);
            while pos(' ',StrN)>0 do Delete(StrN,1,pos(' ',StrN));
            while pos('.',StrN)>0 do StrN[pos('.',StrN)]:=',';
            rKrep:=StrToFloatDef(StrN,0);
//              if (rKrep-CommonSet.AlkoKrepMax)>0.001 then bRasch:=False;
            if (rKrep-CommonSet.AlkoKrepMax)>0.001 then
            begin
              sGr:=FindClName(StrToIntDef(SelPos.Articul,0));
              if pos('���',sGr)=1 then bRasch:=False;
            end;
          end;
        end;
        if bRasch then
        begin
          //����� ������� ��� �������� ����� ���� �����
          if (SelPos.AVid>0)and(CommonSet.GetAMark=1) then //��� �� ���� � ����������� �� �����
          begin
            if prGetFixAVid(SelPos.AVid) then
            begin //����� ����������� ��
              Beep;
              fmGetAM.ShowModal;
              if fmGetAM.ModalResult=mrOk then
              begin
                SelPos.AMark:=fmGetAM.cxLabel2.Caption;
              end else bRasch:=False;
            end;
          end;

          if bRasch then prAddPosSel(0,1);
        end
        else
        begin
          Memo2.Lines.Add('��������� ������� �������� ��������� ����� '+IntToStr(CommonSet.AlkoKrepMax)+'%');
          fmAttention.Label1.Caption:='��������� ������� �������� ��������� ����� '+IntToStr(CommonSet.AlkoKrepMax)+'% � '+FormatDateTime('hh:nn ss',CommonSet.WkTimeBeg)+' �� '+FormatDateTime('hh:nn ss',CommonSet.WkTimeEnd);
          prWriteLog('~~AttentionShow;'+'��������� ������� �������� ��������� ����� '+IntToStr(CommonSet.AlkoKrepMax)+'%');
          fmAttention.ShowModal;
        end;
      end;
    end else
    begin //��� �� ����� - ����� �� ������
      if pos('22'+its(CommonSet.CutTailCode),sBar)=1 then //��� ������� - ������������ ��� �������
      begin
        Memo2.Clear;
        with dmFB do
        begin
          try
            Memo2.Lines.Add('����� ... ���� ����� ����.');
            Cash.Close;
            Cash.DBName:=CommonSet.CashDB;
            Cash.Open;
          except
            Memo2.Lines.Add('    ������ �������� ���� ������. ���������� � ���������������.');
          end;

          if Cash.Connected then
          begin
            if Check.Operation=1 then
            begin
              quXCheck.Active:=False;
              quXCheck.ParamByName('SBAR').AsString:=sBar;
              quXCheck.Active:=True;
              if quXCheck.RecordCount>0 then
              begin
                Check.ECheck:=1; //��� ����������� ���

                Memo2.Lines.Add('��� "'+sBar+'" ������ - '+its(quXCheck.RecordCount)+' �������.'); delay(10);
                quXCheck.First;
                while not quXCheck.Eof do
                begin
                  Memo2.Lines.Add('��� '+its(quXCheckICODE.AsInteger)+' ���-�� '+fs(quXCheckQUANT.asfloat)+' ����� '+fs(quXCheckRSUM.AsFloat)); delay(10);
                  //����������� ���������� �����
                  if quXCheckQUANT.AsFloat>0 then
                  begin
                    if prAddByCode(quXCheckICODE.AsInteger,quXCheckBARCODE.AsString,quXCheckQUANT.AsFloat,sMess) then prAddPosSel(0,1)
                    else
                    begin
                      Memo2.Lines.Add(sMess);
                      fmAttention.Label1.Caption:=sMess;
                      prWriteLog('~~AttentionShow;'+sMess);
                      fmAttention.ShowModal;
                    end;
                  end;

                  quXCheck.Next;
                end;
              end else
                Memo2.Lines.Add('��� "'+sBar+'" �� ������.');

              quXCheck.Active:=False;

            end else
              Memo2.Lines.Add('    ������ ���� ����� �������.');

            delay(100);
            Cash.Close;
          end;
        end;
      end else //��� �� �������
      begin
        if pos(CommonSet.DiscPre,sBar)=1 then
        begin  //���� ��� ��

          sDisc:='';

          sBar:=SOnlyDigit(sBar);
          if FindDiscount(sBar) then
          begin
            Str(Check.DiscProc:5:2,StrWk); While Pos(' ',StrWk)>0 do Delete(StrWk,Pos(' ',StrWk),1);
            Label17.Caption:='������������ ������ : '+Check.DiscName+' ('+StrWk+'%).';

            iNumPos:=0;
            //������ ����� ����������� ��� �������
            with dmC do
            begin
              ViewCh.BeginUpdate;
              dsCheck.DataSet:=nil;

              taCheck.Active:=False;
              taCheck.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
              taCheck.Active:=True;
              try
                trUpdCh.StartTransaction;
                taCheck.First;
                while not taCheck.Eof do
                begin
                  CalcDiscont(taCheckARTICUL.AsString,(Nums.iCheckNum+1),rv(taCheckPRICE.AsFloat),taCheckQUANT.AsFloat,0,Check.DiscProc,rDProc,rDSum);
                  rDSum:= RoundEx(rDSum*100)/100;

                  taCheck.Edit;
                  taCheckDPROC.AsFloat:=rDProc;
                  taCheckDSUM.AsFloat:=rDSum;
                  taCheck.Post;

                  sDisc:=sDisc+'~!Disc;'+IntToStr((Nums.iCheckNum))+';'+taCheckARTICUL.AsString+';'+FloatToStr(rDSum)+';'+FloatToStr(rDProc)+';'+IntToStr(taCheckNUMPOS.AsInteger)+';'+#$0D;
                  iNumPos:=taCheckNUMPOS.AsInteger;

                  taCheck.Next;
                end;
                trUpdCh.Commit;
                prWriteLog(sDisc);
              except
                trUpdCh.Rollback;
              end;
              taCheck.Active:=False;
              taCheck.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
              taCheck.Active:=True;

              dsCheck.DataSet:=taCheck;

              ViewCh.EndUpdate;
            end;

            //������ ������� �������
            if CurPos.Articul>'' then
            begin
              CalcDiscont(CurPos.Articul,(Nums.iCheckNum+1),rv(CurPos.Price),CurPos.Quant,0,Check.DiscProc,CurPos.DProc,CurPos.DSum);
              CurPos.DSum:= rv(CurPos.DSum);

              inc(iNumPos);
              prWriteLog('~!Disc;'+IntToStr((Nums.iCheckNum))+';'+CurPos.Articul+';'+FloatToStr(CurPos.DSum)+';'+FloatToStr(CurPos.DProc)+';'+IntToStr(iNumPos)+';');

              with fmMainCasher do
              begin
                CurrencyEdit1.EditValue:=rv(CurPos.Price)*CurPos.Quant-CurPos.DSum;

                Str(CurPos.DProc:5:2,StrWk); While Pos(' ',StrWk)>0 do Delete(StrWk,Pos(' ',StrWk),1);
                Label18.Caption:='������ '+StrWk+'%';
                Label18.Visible:=True;
                Str(CurPos.DSum:10:2,StrWk); While Pos(' ',StrWk)>0 do Delete(StrWk,Pos(' ',StrWk),1);
                Label19.Caption:=StrWk+'�.';
                Label19.Visible:=True;
              end;
//        end;
            end;
          end
          else Label17.Caption:='...';

          CalcSum;
          sBar:=''; //���� �� ���������� �� ������

        end else
        begin
          if FindPers(sBar) then
          begin
            //��� �� ��������� - ����� ��������
            if pos('�����',Person.Name)>0 then prWriteLog('&!PersA;'+IntToStr(Person.Id)+';'+Person.Name+';')
            else prWriteLog('&!Pers;'+IntToStr(Person.Id)+';'+Person.Name+';');

            Label13.Caption:=Person.Name;
            if CanDo('prCashRep') then cxButton1.Visible:=True else cxButton1.Visible:=False;

            fGetFN; //������ �� ����������� ����������
            WriteStatusFN;

            prSetVisibleAll(True);      //���� ������ ������������

            sBar:=''; //���� �� ���������� �� ������
          end
          else
          begin
            Beep;
            Memo2.Clear;
            Memo2.Lines.Add('�������� - "'+sBar+'" �� ������.');
            fmBarMessage.ShowModal;
          end;
        end;
      end;
    end;

    Edit1.Enabled:=True;
    bAddPos:=False;
    TextEdit3.Text:=sBar;
    TextEdit3.SetFocus;
    TextEdit3.SelectAll;
  end;
end;

procedure TfmMainCasher.acChangePersExecute(Sender: TObject);
begin
  if bBegCash then exit; //���� ������� ������, �� ����� ������������ ������ (����� �������� ��������� ���������� ����)
  if not Check.ChBeg then
  begin
    bAddPos:=False;
    fmMainCasher.Timer3.Enabled:=True;
    Label13.Caption:='';
    Person.Id:=0;
    Person.Name:='';
    Panel3.Visible:=False;
    Timer2.Enabled:=False;
    CountSec:=0;
    cxButton1.Visible:=False;
    Panel5.Visible:=False;

    prSetVisibleAll(False);

    fmPre.ShowModal;
  end;
end;

procedure TfmMainCasher.TextEdit3KeyPress(Sender: TObject; var Key: Char);
Var sBar:ShortString;
    bCh:Byte;
    CurrTime:TDateTime;
    StrN:String;
    bRasch:Boolean;
    rKrep:Real;
    sGr:String;
    sMess:String;
begin
  if bAddPos then exit;
  
  if Key='�' then Key:='.';
  bCh:=ord(Key);

  if bCh = 13 then
  begin //���� ����������
//    showmessage('Enter');
    sBar:=TextEdit3.Text;
    if pos('22'+its(CommonSet.CutTailCode),sBar)=1 then //��� ������� - ������������ ��� �������
    begin
      Memo2.Clear;
      with dmFB do
      begin
        try
          Memo2.Lines.Add('����� ... ���� ����� ����.');
          Cash.Close;
          Cash.DBName:=CommonSet.CashDB;
          Cash.Open;
        except
          Memo2.Lines.Add('    ������ �������� ���� ������. ���������� � ���������������.');
        end;

        if Cash.Connected then
        begin
          if Check.Operation=1 then
          begin
            quXCheck.Active:=False;
            quXCheck.ParamByName('SBAR').AsString:=sBar;
            quXCheck.Active:=True;
            if quXCheck.RecordCount>0 then
            begin
              Check.ECheck:=1;

              Memo2.Lines.Add('��� "'+sBar+'" ������ - '+its(quXCheck.RecordCount)+' �������.'); delay(10);
              quXCheck.First;
              while not quXCheck.Eof do
              begin
                Memo2.Lines.Add('��� '+its(quXCheckICODE.AsInteger)+' ���-�� '+fs(quXCheckQUANT.asfloat)+' ����� '+fs(quXCheckRSUM.AsFloat)); delay(10);
                //����������� ���������� �����
                if quXCheckQUANT.AsFloat>0 then
                begin
                  if prAddByCode(quXCheckICODE.AsInteger,quXCheckBARCODE.AsString,quXCheckQUANT.AsFloat,sMess) then prAddPosSel(0,1)
                  else
                  begin
                    Memo2.Lines.Add(sMess);
                    fmAttention.Label1.Caption:=sMess;
                    prWriteLog('~~AttentionShow;'+sMess);
                    fmAttention.ShowModal;
                  end;
                end;

                quXCheck.Next;
              end;
            end else
              Memo2.Lines.Add('��� "'+sBar+'" �� ������.');

            quXCheck.Active:=False;

          end else
            Memo2.Lines.Add('    ������ ���� ����� �������.');

          delay(100);
          Cash.Close;
        end;
      end;
    end else
    begin
      if not FindBar(SOnlyDigit1(sBar)) then
      begin
        Beep;
        Memo2.Clear;
        Memo2.Lines.Add('�������� - "'+sBar+'" �� ������.');
        fmBarMessage.ShowModal;
      end
      else
      begin
        if SelPos.Price=0 then
        begin
          Memo2.Clear;
          Memo2.Lines.Add(ZerroMess);
          fmAttention.Label1.Caption:=ZerroMess;
          fmAttention.ShowModal;
        end else
        begin
          bRasch:=True;
          CurrTime:=frac(now);
          if (CommonSet.AlkoKrepMax>0)and((CurrTime<CommonSet.WkTimeBeg)or((CurrTime>CommonSet.WkTimeEnd))) then   //�������� ��������
          begin
            StrN:=SelPos.Name;
            //�������� ��������
            if pos('%',StrN)>0 then
            begin
              StrN:=Copy(StrN,1,pos('%',StrN)-1);
              while pos(' ',StrN)>0 do Delete(StrN,1,pos(' ',StrN));
              while pos('.',StrN)>0 do StrN[pos('.',StrN)]:=',';
              rKrep:=StrToFloatDef(StrN,0);
//            if (rKrep-CommonSet.AlkoKrepMax)>0.001 then bRasch:=False;
              if (rKrep-CommonSet.AlkoKrepMax)>0.001 then
              begin
                sGr:=FindClName(StrToIntDef(SelPos.Articul,0));
                if pos('���',sGr)=1 then  bRasch:=False;
              end;
            end;
          end;
          if bRasch then
          begin
            //����� ������� ��� �������� ����� ���� �����
            if (SelPos.AVid>0)and(CommonSet.GetAMark=1) then //��� �� ���� � ����������� �� �����
            begin
              if prGetFixAVid(SelPos.AVid) then
              begin //����� ����������� ��
                Beep;
                fmGetAM.ShowModal;
                if fmGetAM.ModalResult=mrOk then
                begin
                  SelPos.AMark:=fmGetAM.cxLabel2.Caption;
                end else bRasch:=False;
              end;
            end;

            if bRasch then prAddPosSel(0,1);
          end else
          begin
            Memo2.Lines.Add('��������� ������� �������� ��������� ����� '+IntToStr(CommonSet.AlkoKrepMax)+'%');
            fmAttention.Label1.Caption:='��������� ������� �������� ��������� ����� '+IntToStr(CommonSet.AlkoKrepMax)+'% � '+FormatDateTime('hh:nn ss',CommonSet.WkTimeBeg)+' �� '+FormatDateTime('hh:nn ss',CommonSet.WkTimeEnd);
            prWriteLog('~~AttentionShow;'+'��������� ������� �������� ��������� ����� '+IntToStr(CommonSet.AlkoKrepMax)+'%');
            fmAttention.ShowModal;
          end;

//        prAddPosSel(0,1);
          TextEdit3.Text:='';
        end;
        TextEdit3.SetFocus;
        TextEdit3.SelectAll;
      end;
    end;
  end;
end;

procedure TfmMainCasher.acCalcNalExecute(Sender: TObject);
Var iMax:Integer;
    rSum1,rSumCh:Real;
    iSumPos,iR:Integer;
    bRasch:Boolean;
    StrN:String;
    rKrep:Real;
    CurrTime:TDateTime;
    bStop:Boolean;
    n:INteger;
    sGr:String;
    aCount:INteger;
    bAdd:Boolean;
    rSumChWithDopDisc:Real;
begin
  //�� ����� �������� ������ ���������� �� ����� -  ��� ����������� �� ��������� �������

  AlcoSet.URL:='';
  AlcoSet.CRCODE:='';
  AlcoSet.Ver:='';

  if CurrencyEdit3.EditValue<=0 then
  begin
    Memo2.Lines.Add('�������� ������������ �����.');
    exit;
  end;
  if bBegCash then exit; //���� ��� ��������� ��������, �� ������ �� ���������
  if not CanDo('prCalcNal1') then begin fmMainCasher.Memo2.Clear; fmMainCasher.Memo2.Lines.Add('��� ���� �� ��������.');  exit;end;
  if not Check.ChBeg then exit;


  bBegCash:=True;

  with dmC do
  begin
    bStop:=False;

    if (taCheck.RecordCount=0)and(CurPos.Articul='') then begin bBegCash:=False; exit; end; // ������ ��� ��� ������� ����
    taCheck.First;
//    if taCheck.Eof and (CurPos.Articul='') then begin bBegCash:=False; exit; end; // ������ ��� ��� ������� ����

    rDiscDop:=0;
    iArtDisc:=0;

    prWriteLog('!!Itog;');
    bStopAddPos:=True;   //������ �� ���������� �� �����

    //������� ������� �������� � ���
    if (CurPos.Articul>'')and(CurPos.Quant<>0) then //������� � �������� ����������
    begin
        //���������� �� � ���
      taCheck.Active:=False;
      taCheck.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
      taCheck.Active:=True;

      iMax:=1;
      if taCheck.RecordCount>0 then
      begin
        taCheck.Last;
        iMax:=taCheckNUMPOS.AsInteger+1; //����� ����� �������
      end;
      try
        //�������� ������� �����
        Nums.iRet:=InspectSt(Nums.sRet);
        if Nums.iRet<>0 then TestFp('InspectSt');

        if iMax=1 then
        begin
          //���� ������ ����� ��������� ���
          //������� ��������� �����
          if not OpenZ then ZOpen;

          if CommonSet.PrintFisCheckOnEnd=0 then
          begin
            Case Check.Operation of
            0: begin
                 if CheckRetStart=False then bStop:=True;
               end;
            1: begin
                 if CheckStart=False then bStop:=True;
               end;
            end;
          end;
        end;

        if bStop then  //��� ������� � ���� ���������
        begin
          Check.ChBeg:=False;
          Check.ChEnd:=True;
          bBegCash := False;
          ClearSelPos;
          ClearPos;
          ResetPos;
          CalcSum;
          fmMainCasher.TextEdit3.Text:='';
          fmMainCasher.TextEdit3.SetFocus;


          TestStatus('StartCheck',StrWk);
          fmMainCasher.Memo2.Clear;
          fmMainCasher.Memo2.Lines.Add(StrWk);
          fmAttention.Label1.Caption:=StrWk;
          fmAttention.ShowModal;
          exit;
        end;


        if fmMainCasher.tag=0 then
          if fmMainCasher.GridCh.Height<329 then fmMainCasher.GridCh.Height:=fmMainCasher.GridCh.Height+18;
        if (fmMainCasher.tag=1)or(fmMainCasher.tag=17) then
          if fmMainCasher.GridCh.Height<280 then fmMainCasher.GridCh.Height:=fmMainCasher.GridCh.Height+18;

//      ��� ���������� � ��������� ��������

        bAdd:=True;
        rSum1:=CurPos.Quant*rv(CurPos.Price);
        iR:=RoundEx(rSum1*100);

//      ��� ���������� � ��������� ��������
        if CommonSet.PrintFisCheckOnEnd=0 then bAdd:=CheckAddPos(iSumPos) else iSumPos:=iR;
        if bAdd then
        begin
          trUpdCh.StartTransaction;

          taCheck.Append;
          taCheckCASHNUM.AsInteger:=CommonSet.CashNum;
          taCheckNUMPOS.AsInteger:=iMax;
          taCheckARTICUL.AsString:=CurPos.Articul;
          taCheckRBAR.AsString:=CurPos.Bar;
          taCheckNAME.AsString:=CurPos.Name;
          taCheckQUANT.AsFloat:=CurPos.Quant;
          taCheckPRICE.AsFloat:=CurPos.Price;
          taCheckDPROC.AsFloat:=CurPos.DProc;
          taCheckDSUM.AsFloat:=CurPos.DSum;
          taCheckRSUM.AsFloat:=rv(rSum1);
          taCheckDEPART.AsInteger:=CurPos.Depart;
          taCheckEDIZM.AsString:=CurPos.EdIzm;
          taCheckTIMEPOS.AsDateTime:=now;
          taCheckAMARK.AsString:=CurPos.AMark;
          taCheck.Post;

          trUpdCh.Commit;
          taCheck.Refresh;

          if abs(iR-iSumPos)>1 then
          begin //����������� ����� �� �������, ���������� ������ ���-�� �����������
            prWriteLog('~~CheckAddPos2;'+IntToStr((Nums.iCheckNum))+';'+CurPos.Articul+';'+CurPos.Bar+';'+FloatToStr(CurPos.Quant)+';'+FloatToStr(CurPos.Price)+';'+FloatToStr(CurPos.DSum)+';��-'+INtToStr(iSumPos)+';����-'+INtToStr(iR));
            prClearCheck1;  //a�������� ���
          end else
          begin //����������� �������� �� ����� 1 ���, ���������� � ����
            trUpdCh.StartTransaction;
            taCheck.Edit;
            taCheckRSUM.AsFloat:=iSumPos/100;
            taCheck.Post;
            trUpdCh.Commit;
          end;
        end else
        begin
          n:=5;
          TestFp('CheckAddPos');
          while (CheckAddPos(iSumPos)=False)and(n>0) do
          begin
            TestFp('CheckAddPos');
            dec(n); delay(100);
          end;
          if n=0 then //������� �������� ������ � ������������ ���
          begin
            prClearCheck1;  //��������� ���
            exit;
          end;
        end;
      except;
        trUpdCh.Rollback;
      end;
      ClearPos;
      ResetPos;
      Label18.Caption:='';
      Label18.Visible:=False;
      Label19.Caption:='';
      Label19.Visible:=False;
    end;
    //����������� ����� ������ �� ������ �� �����

    ViewCh.BeginUpdate;
    prRecalcDisc(Check.RSum);  //�� ��������
    prRecalcDiscItog;          // ������ �� ���� - ������� ��������� ������
    CalcSum;
    ViewCh.EndUpdate;

    //���� ������ �� ���� ����� ������
    prCalcDiscSum.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
    prCalcDiscSum.ExecProc;

    rDiscont:=prCalcDiscSum.ParamByName('DISCSUM').AsFloat; //����� ������ �� ���� ��������
    rCheckSum:=prCalcDiscSum.ParamByName('CHECKSUM').AsFloat;

    bRasch:=True;
    CurrTime:=frac(now);
    if (CommonSet.AlkoKrepMax>0)and((CurrTime<CommonSet.WkTimeBeg)or((CurrTime>CommonSet.WkTimeEnd))) then   //�������� ��������
    begin
      try
        ViewCh.BeginUpdate;
        taCheck.First;
        while not taCheck.Eof do
        begin
          StrN:=taCheckNAME.AsString;
          //�������� ��������
          if pos('%',StrN)>0 then
          begin
            StrN:=Copy(StrN,1,pos('%',StrN)-1);
            while pos(' ',StrN)>0 do Delete(StrN,1,pos(' ',StrN));
            while pos('.',StrN)>0 do StrN[pos('.',StrN)]:=',';
            rKrep:=StrToFloatDef(StrN,0);
            if (rKrep-CommonSet.AlkoKrepMax)>0.001 then
            begin
              sGr:=FindClName(StrToIntDef(taCheckARTICUL.AsString,0));
              if pos('���',sGr)=1 then  bRasch:=False;
            end;
          end;
          taCheck.Next;
        end;
      finally
        ViewCh.EndUpdate;
      end;
    end;

    if (bRasch)and(CommonSet.MessAlco>0) then //����� ��������� ��������� ������� ��������
    begin
      quFACount.Active:=False;
      quFACount.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
      quFACount.Active:=True;
      aCount:=quFACountACOUNT.AsInteger;
      quFACount.Active:=False;
      if aCount>0 then
      begin
        if CommonSet.MessAlco>1 then
        begin
          if CommonSet.MessAlco=2 then
          begin
            Randomize;
            iAAnsw:=Random(9);
            fmMessAlco.Label3.Caption:='��� ������ �������';
            fmMessAlco.Label4.Caption:=its(iAAnsw);
            fmMessAlco.Label4.Visible:=True;
          end;
          if CommonSet.MessAlco=3 then
          begin
            Randomize;
            iSlag1:=Random(9);
            iSlag2:=Random(9-iSlag1);
            iAAnsw:=iSlag1+iSlag2;
            fmMessAlco.Label3.Caption:='��� ������ ������� ��������� �����';
            fmMessAlco.Label4.Caption:=its(iSlag1)+' + '+its(iSlag2);
            fmMessAlco.Label4.Visible:=True;
          end;
        end else
        begin
          iAAnsw:=0;
          fmMessAlco.Label3.Caption:='��� ������ ������� �����';
          fmMessAlco.Label4.Visible:=False;
        end;
        fmMessAlco.ShowModal;
      end;
    end;

    if bRasch=False then
    begin
      Memo2.Lines.Add('��������� ������� �������� ��������� ����� '+IntToStr(CommonSet.AlkoKrepMax)+'%');
      fmAttention.Label1.Caption:='��������� ������� �������� ��������� ����� '+IntToStr(CommonSet.AlkoKrepMax)+'% � '+FormatDateTime('hh:nn ss',CommonSet.WkTimeBeg)+' �� '+FormatDateTime('hh:nn ss',CommonSet.WkTimeEnd);
      prWriteLog('~~AttentionShow;'+'��������� ������� �������� ��������� ����� '+IntToStr(CommonSet.AlkoKrepMax)+'%');
      fmAttention.ShowModal;

      bBegCash:=False;
    end else
    begin

      //��� ������ ������ ��������� ��� ������������
      with fmCash do
      begin
  //      Left:=278;
  //      Top:=364;
        Caption:='������';
        Label5.Caption:='������:  '+Person.Name;

        rSumCh:=rv(fmMainCasher.CurrencyEdit3.EditValue);

        if (rSumCh>=CommonSet.Disc2)and(abs(Check.DiscProc)<0.1) then
        begin
          fmBonus.cxLabel1.Caption:='����� ���� ��������� 2500 ������ , ���������� ������ ������� �����?';
          fmBonus.ShowModal;
          if fmBonus.ModalResult=mrOk then
          begin
            iArtDisc:=666;
            prWriteLog('_!YesToAddPosDiscCard;'+'������������ �� (666 - �������)');
          end;
        end else
        begin
          if (rSumCh>=CommonSet.Disc1)and(Check.DiscProc=0) then
          begin
            fmBonus.cxLabel1.Caption:='����� ���� ��������� 1000 ������ , ���������� ������ ����������� �����?';
            fmBonus.ShowModal;
            if fmBonus.ModalResult=mrOk then
            begin
              iArtDisc:=13;
              prWriteLog('_!YesToAddPosDiscCard;'+'������������ �� (13 - �����������)');
            end;
          end;
        end;

        rSumChWithDopDisc:=rSumCh;
        if (rSumCh>0.5) and (Check.Operation=1) then
        begin
          rSumChWithDopDisc:=RoundSpec(rSumCh,rDiscDop);
  //        rSumCh:=RoundSpec(rSumCh,rDiscDop);
        end;

        cxCurrencyEdit1.EditValue:=rSumCh;
        cxCurrencyEdit10.EditValue:=rSumChWithDopDisc;

        cxCurrencyEdit2.EditValue:=rSumCh;
        cxCurrencyEdit3.EditValue:=0;
        cxCurrencyEdit2.SelectAll;

        cxCurrencyEdit1.Enabled:=True;
        cxCurrencyEdit2.Enabled:=True;
        cxCurrencyEdit3.Enabled:=True;

        taPay.Active:=False;
        taPay.CreateDataSet;

      end;
      TextEdit3.SetFocus;

      //�������� ������� �����
      Nums.iRet:=InspectSt(Nums.sRet);
      if Nums.iRet<>0 then TestFp('InspectSt');
      fmCash.ShowModal; //��� ���������� ��� ���� ��� ���������
    end;
  end;
end;

procedure TfmMainCasher.Timer2Timer(Sender: TObject);
begin
  if CommonSet.CountSecMax=0 then
  begin
    Timer2.Enabled:=False;
    exit;
  end else
  begin
    inc(CountSec);
    if (CountSec>=CommonSet.CountSecMax)and(fmAttention.Showing=False)and(fmSt.Showing=False)  then acChangePers.Execute;
  end;  
end;

procedure TfmMainCasher.acFindArtExecute(Sender: TObject);
Var StrWk:String;
    iCode:INteger;
    sMess:String;
    bRasch:Boolean;
begin //����� �� ��������
  Edit1.SetFocus;
  TextEdit3.SetFocus;

  StrWk:=TextEdit3.Text;

  if Length(StrWk)>0 then
  begin //���� � ��������� �� ��������
    if Length(StrWk)>30 then StrWk:=Copy(StrWk,1,30);
    iCode:=0;
    if Length(StrWk)<6 then iCode:=StrToINtDef(StrWk,0);
    if iCode>0 then
    begin
      if prAddByCode(iCode,'',0,sMess) then
      begin
        //����� ������� ��� �������� ����� ���� �����
        bRasch:=True;

        if (SelPos.AVid>0)and(CommonSet.GetAMark=1) then //��� �� ���� � ����������� �� �����
        begin
          if prGetFixAVid(SelPos.AVid) then
          begin //����� ����������� ��
            Beep;
            fmGetAM.ShowModal;
            if fmGetAM.ModalResult=mrOk then
            begin
              SelPos.AMark:=fmGetAM.cxLabel2.Caption;
            end else bRasch:=False;
          end;
        end;

        if bRasch then prAddPosSel(0,1);
      end
      else
      begin
        Memo2.Lines.Add(sMess);
        fmAttention.Label1.Caption:=sMess;
        prWriteLog('~~AttentionShow;'+sMess);
        fmAttention.ShowModal;
        TextEdit3.SetFocus;
        TextEdit3.SelectAll;
      end;
    end;
  end
  else //������� ������
  begin
    CountSec:=0;
    Timer2.Enabled:=False;
    fmCardsFind.CardsTree.Items.Clear;
    CardsExpandLevel(nil,fmCardsFind.CardsTree,dmC.quClassifCards);
    fmCardsFind.Panel1.Visible:=True;
    fmCardsFind.Panel2.Visible:=False;

    SelPos.Articul:='';
    {                   //�� ������� ������ - ��������
    fmCardsFind.ShowModal;
    if fmCardsFind.ModalResult=mrOk then
    begin
      if SelPos.Articul>'' then
         if SelPos.Price=0 then
         begin
           Memo2.Clear;
           Memo2.Lines.Add(ZerroMess);
           fmAttention.Label1.Caption:=ZerroMess;
           fmAttention.ShowModal;
         end else prAddPosSel(0,1);
    end;}
  end;
  //�������� ������� �������
  TextEdit3.Text:='';
end;

procedure TfmMainCasher.acQuantityExecute(Sender: TObject);
Var s:String;
    rQ:Real;
    iType:Integer;
begin
  if not CanDo('prChangeQuant') then begin fmMainCasher.Memo2.Clear; fmMainCasher.Memo2.Lines.Add('��� ���� �� ��������.');  exit; end;
  if CurPos.AMark>'' then begin fmMainCasher.Memo2.Clear; fmMainCasher.Memo2.Lines.Add('��������� ���-�� ��������� ������ ���������.'); exit; end;
  if (CurPos.Articul>'')and(CurPos.FisPos=0) then
  begin
    fmCalc.CalcEdit1.Value:=CurPos.Quant;
    rQ:=0;
    if TextEdit3.Text>'' then
    begin
      s:=TextEdit3.Text;
      rQ:=StrToFloatDef(s,0);
    end;
    if (rQ<>0)and(rQ<100) then fmCalc.CalcEdit1.Value:=rQ;

    fmCalc.ShowModal;
    if fmCalc.ModalResult=mrOk then
    begin
      if fmCalc.CalcEdit1.EditValue=0 then
      begin
        Memo2.Lines.Add('��������! ������� ���-�� ���������.');
      end else
      begin
        iType:=dmC.TypeEdIzm(CurPos.Articul);
        rQ:=fmCalc.CalcEdit1.EditValue;

        if (frac(rQ)<>0) and (iType=1) then
        begin
          //������� ����� ������� ���-��� ���� �� �����
          prWriteLog('~!AttentionBadQuant;'+IntToStr((Nums.iCheckNum))+';'+CurPos.Articul+';;'+FloatToStr(CurPos.Quant)+';'+FloatToStr(RoundEx(CurPos.Quant*CurPos.Price*100)/100)+';'+FloatToStr(CurPos.DSum)+';');
          fmAttention.Label1.Caption:='���������� �������� ������ �� ����� ���� ������� !!!';
          fmAttention.ShowModal;
        end
        else
        begin
          CurPos.Quant:=fmCalc.CalcEdit1.EditValue;

          if CurPos.Quant>=0 then  //���� �� �������� ������������ ������� (���������� � "-" �����������) �� ������ ������.
          begin
            CalcDiscont(CurPos.Articul,(Nums.iCheckNum+1),rv(CurPos.Price),CurPos.Quant,0,Check.DiscProc,CurPos.DProc,CurPos.DSum);
            CurPos.DSum:=RoundEx(CurPos.DSum*100)/100;
          end;

          if CurPos.DSum<>0 then
          begin
          //������ ������������
            Str(CurPos.DProc:5:2,StrWk); While Pos(' ',StrWk)>0 do Delete(StrWk,Pos(' ',StrWk),1);
            Label18.Caption:='������ '+StrWk+'%';
            Label18.Visible:=True;
            Str(CurPos.DSum:10:2,StrWk); While Pos(' ',StrWk)>0 do Delete(StrWk,Pos(' ',StrWk),1);
            Label19.Caption:=StrWk+'�.';
            Label19.Visible:=True;
          end else
          begin
            Label18.Visible:=False;
            Label19.Visible:=False;
          end;

          ResetPos;
          CalcSum;
{   ��� �� CalcSum
    rSum1:=CurPos.Quant*CurPos.Price*100;
    iR:=RoundEx(rSum1);
    rSum:=iR/100-CurPos.DSum;
    iR:=RoundEx(rSum1);
    rSum0:=iR/100;
}
         prWriteLog('~!Quant;'+IntToStr((Nums.iCheckNum))+';'+CurPos.Articul+';;'+FloatToStr(CurPos.Quant)+';'+FloatToStr(RoundEx(CurPos.Quant*CurPos.Price*100)/100)+';'+FloatToStr(CurPos.DSum)+';');
       end;  
     end;
    end;
  end;
end;

procedure TfmMainCasher.acErrorExecute(Sender: TObject);
begin
  if not CanDo('prError') then begin fmMainCasher.Memo2.Clear; fmMainCasher.Memo2.Lines.Add('��� ���� �� ��������.');  exit;end;
  if CurPos.Articul>'' then
  begin
    prWriteLog('~!Error;'+IntToStr((Nums.iCheckNum))+';'+CurPos.Articul+';');
    ClearPos;
    ClearSelPos;
    ResetPos;
    CalcSum;
//    FormLog('Error','CashNum '+INtToStr(CommonSet.CashNum));
  end;
end;

procedure prClearCheck1;
begin
  bBegCash:=False;
  fmAttention.Label1.Caption:='���� ��������� ���� � ��. ��������� ������������ ����!';
  fmAttention.Label2.Caption:='��� ����� �����������.';
  fmAttention.Label3.Caption:='';

  prWriteLog('~~AttentionShow;'+'���� ��������� ���� � ��. ��������� ������������ ����!');

  fmAttention.ShowModal;

  fmAttention.Label2.Caption:='����� ���������� ������ ������� "�����"';
  fmAttention.Label3.Caption:='��������� ������������ ��������� ��������.';

  //���� ��� �����
  prWriteLog('~!Annul;'+IntToStr((Nums.iCheckNum))+';'+CurPos.Articul+';'+FloatToStr(CurPos.Quant)+';');
  prSaveCheckA(Check.PayType);

  ClearPos;
  ClearSelPos;
  ResetPos;

  with dmC do
  begin
    prClearCheck.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
    prClearCheck.ExecProc;

    //������� �� ���� ���� ��� ���������
    quDel.SQL.Clear;
    quDel.SQL.Add('Delete from cashsail');
    quDel.SQL.Add('where');
    quDel.SQL.Add('SHOPINDEX=1 ');
    quDel.SQL.Add('and CASHNUMBER='+its(CommonSet.CashNum));
    quDel.SQL.Add('and ZNUMBER ='+its(Nums.ZNum));
    quDel.SQL.Add('and CHECKNUMBER ='+its(Nums.iCheckNum));
    quDel.ExecQuery;

    quDel.SQL.Clear;
    quDel.SQL.Add('Delete from cashpay');
    quDel.SQL.Add('where');
    quDel.SQL.Add('SHOPINDEX=1 ');
    quDel.SQL.Add('and CASHNUMBER='+its(CommonSet.CashNum));
    quDel.SQL.Add('and ZNUMBER ='+its(Nums.ZNum));
    quDel.SQL.Add('and CHECKNUMBER ='+its(Nums.iCheckNum));
    quDel.ExecQuery;

  end;

  CalcSum;
  ClearCheck;

  //���������� ��� �� ����� - �������� �� �������
  CheckCancel;

  FormLog('Annul','CashNum '+INtToStr(CommonSet.CashNum));
  fmMainCasher.Memo2.Clear;
  fmMainCasher.Memo2.Lines.Add('��� '+IntToStr((Nums.iCheckNum+1))+' �����������.');
  fmMainCasher.Memo2.Lines.Add('��� ������ ������ �������� ��.');
  
  fmMainCasher.Timer3.Enabled:=True;

  if fmCash.Visible then fmCash.Close;
end;


procedure TfmMainCasher.acAnnulExecute(Sender: TObject);
begin
  if not CanDo('prAnnCheck') then begin fmMainCasher.Memo2.Clear; fmMainCasher.Memo2.Lines.Add('��� ���� �� ��������.');  exit;end;

  if MessageDlg('������������ ���?', mtConfirmation, [mbYes, mbNo], 0) = mrNo then exit;

  //���� ��� �����
  prWriteLog('~!Annul;'+IntToStr((Nums.iCheckNum))+';'+CurPos.Articul+';'+FloatToStr(CurPos.Quant)+';');
  prSaveCheckA(0);

  bBegCash:=False;
  bAddPosSel:=False;

  ClearPos;
  ClearSelPos;
  ResetPos;

  with dmC do
  begin
    prClearCheck.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
    prClearCheck.ExecProc;

    //������� �� ���� ���� ��� ��������� �����  - ����� �� �� ��������� ��

{    quDel.SQL.Clear;
    quDel.SQL.Add('Delete from cashsail');
    quDel.SQL.Add('where');
    quDel.SQL.Add('SHOPINDEX=1 ');
    quDel.SQL.Add('and CASHNUMBER='+its(CommonSet.CashNum));
    quDel.SQL.Add('and ZNUMBER ='+its(Nums.ZNum));
    quDel.SQL.Add('and CHECKNUMBER ='+its(Nums.iCheckNum));
    quDel.ExecQuery;

    quDel.SQL.Clear;
    quDel.SQL.Add('Delete from cashpay');
    quDel.SQL.Add('where');
    quDel.SQL.Add('SHOPINDEX=1 ');
    quDel.SQL.Add('and CASHNUMBER='+its(CommonSet.CashNum));
    quDel.SQL.Add('and ZNUMBER ='+its(Nums.ZNum));
    quDel.SQL.Add('and CHECKNUMBER ='+its(Nums.iCheckNum));
    quDel.ExecQuery;
 }
  end;
  CalcSum;
  ClearCheck;
  WriteStatus;

  //���������� ��� �� �����
  if CommonSet.PrintFisCheckOnEnd=0 then
  begin
    Nums.iRet:=InspectSt(Nums.sRet);
    if Nums.iRet<>0 then
      if TestStatus('InspectSt',sMessage)=False then
      begin
    //          ShowMessage('������: "'+Nums.sRet+'". ��������� �������� ����� ���������� ������.');
        WriteStatus;
        fmAttention.Label1.Caption:=sMessage;
        fmAttention.ShowModal;
        exit;// ������ ��� - ������� ��� ������������ ����
      end;

    //  if Nums.iRet=21 then CheckCancel;
    if Nums.iRet=21 then CheckCancel
    else if CheckOpenFis then CheckCancel;
  end
  else if CheckOpenFis then CheckCancel;


  FormLog('Annul','CashNum '+INtToStr(CommonSet.CashNum));
  fmMainCasher.Memo2.Clear;
  fmMainCasher.Memo2.Lines.Add('��� '+IntToStr((Nums.iCheckNum+1))+' �����������.');
  fmMainCasher.Memo2.Lines.Add('��� ������ ������ �������� ��.');
end;

procedure TfmMainCasher.Edit2KeyPress(Sender: TObject; var Key: Char);
Var sBar:String;
    bCh:Byte;
    StrWk:String;
    iMax,iArt:INteger;
    bNotErr:Boolean;
begin
  bCh:=ord(Key);
  if bCh = 13 then
  begin //���� ����������
    sBar:=SOnlyDigit(Edit2.Text);
    if FindPers(sBar) then
    begin
      //��� �� ��������� - ����� ��������
      if pos('�����',Person.Name)>0 then prWriteLog('&!PersA;'+IntToStr(Person.Id)+';'+Person.Name+';')
      else prWriteLog('&!Pers;'+IntToStr(Person.Id)+';'+Person.Name+';');

      Label13.Caption:=Person.Name;
      if CanDo('prCashRep') then cxButton1.Visible:=True else cxButton1.Visible:=False;

      prSetVisibleAll(True);      //���� ������ ������������

      Edit2.Text:='';
      TextEdit3.SetFocus;
    end else
    begin
      if FindDiscount(sBar) then
      begin //��� ����� ���� �������� � ����������� �� ����� ����
        if (fmMainCasher.CurrencyEdit3.EditValue=0) and (Check.ChBeg=False) then
        begin
          if Check.DiscProc>2.8 then
          begin
            fmBonus.cxLabel1.Caption:='��������� ����� � �������?';
            fmBonus.ShowModal;
            if fmBonus.ModalResult=mrOk then
            begin
              Memo2.Lines.Add('���������� ����� � �������.');
              iArt:=13;
              if Check.DiscProc>4.8 then iArt:=666;

              prWriteLog('������������ �� ('+its(iArt)+')');

              //���� � ���� ���
              with dmC do
              begin
                quCheck0.Active:=False;
                quCheck0.ParamByName('CASHN').AsInteger:=CommonSet.CashNum;
                quCheck0.ParamByName('ZNUM').AsInteger:=Nums.ZNum;
                quCheck0.Active:=True;

                iMax:=quCheck0.RecordCount+1;

                quCheck0.Append;
                quCheck0SHOPINDEX.AsInteger:=1;
                quCheck0CASHNUMBER.AsInteger:=CommonSet.CashNum;
                quCheck0ZNUMBER.AsInteger:=Nums.ZNum;
                quCheck0CHECKNUMBER.AsInteger:=0;
                quCheck0ID.AsInteger:=iMax;
                quCheck0CHDATE.AsDateTime:=now;
                quCheck0ARTICUL.AsString:=its(iArt);
                quCheck0BAR.AsString:=Copy(sBar,1,15);
                quCheck0CARDSIZE.AsString:='NOSIZE';
                quCheck0QUANTITY.AsFloat:=1;
                quCheck0PRICERUB.AsFloat:=0;
                quCheck0DPROC.AsFloat:=0;
                quCheck0DSUM.AsFloat:=0;
                quCheck0TOTALRUB.AsFloat:=0;
                quCheck0CASHER.AsInteger:=Person.Id;
                quCheck0DEPART.AsInteger:=0;
                quCheck0OPERATION.AsInteger:=1;
                quCheck0SECPOS.AsInteger:=0;
                quCheck0ECHECK.AsInteger:=0;
                quCheck0.Post;

                quCheck0.Active:=False;

//              ������� ������������ ��������

                bNotErr:=True;
                if OpenNFDoc=false then  begin TestFp('PrintNFStr');  OpenNFDoc;  end;

                SelectF(10);
                if bNotErr then bNotErr:=PrintNFStr('��� �� ���������� �����') else TestFp('PrintNFStr');
                if bNotErr then bNotErr:=PrintNFStr('����� � '+INtToStr(CommonSet.CashNum)+'  ���.� '+Nums.SerNum) else TestFp('PrintNFStr');
                if bNotErr then bNotErr:=PrintNFStr('�����: '+INtToStr(Nums.ZNum)+'            ������: '+Person.Name) else TestFp('PrintNFStr');
                if bNotErr then bNotErr:=PrintNFStr('����:   '+FormatDateTime('dd.mm.yyyy hh:nn',now)) else TestFp('PrintNFStr');

                StrWk:='----------------------------------------';
                if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
                StrWk:=' ��������          ���-��   ����   ����� ';
                if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
                StrWk:='----------------------------------------';
                if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

                SelectF(15);
                StrWk:='';
                if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

                StrWk:='  �������� - ������� ��������         ';
                SelectF(15);
                if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

                SelectF(3);
                StrWk:='                                                     ';
                if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

                StrWk:= Copy((its(iArt)+' '+Copy(Check.DiscName,1,37)+' 1 ��.'),1,40);
                SelectF(5);
                if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
                StrWk:='----------------------------------------';
                if bNotErr then PrintNFStr(StrWk) else TestFp('PrintNFStr');

                if CloseNFDoc=False then begin  TestFp('PrintNFStr'); CloseNFDoc; end;
              end;
            end;
          end;

          Check.Discount:='';
          Check.DiscProc:=0;
          Check.DiscName:='';
          Check.RSum:=0;
          Check.DSum:=0;

        end else //������� ������
        begin
          with dmC do
          begin
             //������ ��������� ���� ������ ���-�� ���� � ����

            if taCheck.Active=False then
            begin
              taCheck.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
              taCheck.Active:=True;
            end else taCheck.FullRefresh;

            if (taCheck.RecordCount=0)and(CurPos.Articul='') then //��� ������ - ������ ������� �� ������ ������
            begin

              Check.Discount:='';
              Check.DiscProc:=0;
              Check.DiscName:='';
              Check.RSum:=0;
              Check.DSum:=0;
              Label17.Caption:='������������ ������ : ���';

            end else
            begin
              Str(Check.DiscProc:5:2,StrWk); While Pos(' ',StrWk)>0 do Delete(StrWk,Pos(' ',StrWk),1);
              Label17.Caption:='������������ ������ : '+Check.DiscName+' ('+StrWk+'%).';

              ViewCh.BeginUpdate;
              prRecalcDisc(fmMainCasher.CurrencyEdit3.EditValue);
              ViewCh.EndUpdate;

              //������ ������� �������
              if CurPos.Articul>'' then
              begin
                CurrencyEdit1.EditValue:=rv(CurPos.Price)*CurPos.Quant-CurPos.DSum;

                Str(CurPos.DProc:5:2,StrWk); While Pos(' ',StrWk)>0 do Delete(StrWk,Pos(' ',StrWk),1);
                Label18.Caption:='������ '+StrWk+'%';
                Label18.Visible:=True;
                Str(CurPos.DSum:10:2,StrWk); While Pos(' ',StrWk)>0 do Delete(StrWk,Pos(' ',StrWk),1);
                Label19.Caption:=StrWk+'�.';
                Label19.Visible:=True;
              end;
            end;
          end;
        end;
      end
      else Label17.Caption:='...';

      CalcSum;

      TextEdit3.SetFocus;
    end;
  end;
end;

procedure TfmMainCasher.acPersDiscExecute(Sender: TObject);
begin
  Edit2.SetFocus;
end;

procedure TfmMainCasher.acManDiscExecute(Sender: TObject);
Var StrWk:String;
    rProc:Real;
    iNumPos:ShortInt;
begin

  if not CanDo('prManDisc') then begin fmMainCasher.Memo2.Clear; fmMainCasher.Memo2.Lines.Add('��� ���� �� ��������.');  exit;end;

  //������ �������� �� ������ ������
  if CanDo('prManDisc') then begin fmMainCasher.Memo2.Clear; fmMainCasher.Memo2.Lines.Add('��� ���� �� ��������.');  exit;end;


  fmCalc.CalcEdit1.EditValue:=0;
  fmCalc.ShowModal;
  if fmCalc.ModalResult=mrOk then
  begin
    rProc:=fmCalc.CalcEdit1.EditValue;
//    if rProc<>0 then
//    begin
    Check.DiscProc:=rProc;
    Check.DiscName:='';
    Check.Discount:='';
    Check.RSum:=0;
    Check.DSum:=0;

    Str(Check.DiscProc:5:2,StrWk); While Pos(' ',StrWk)>0 do Delete(StrWk,Pos(' ',StrWk),1);
    Label17.Caption:='������ ������ - '+StrWk+'%.';

    iNumPos:=0;
      //������ ����� ����������� ��� �������
    with dmC do
    begin
      ViewCh.BeginUpdate;
      dsCheck.DataSet:=nil;
      try
        trUpdCh.StartTransaction;
        taCheck.First;
        while not taCheck.Eof do
        begin
          CurPos.DProc:=rProc;
          CurPos.DSum:=RoundEx(taCheckPRICE.AsFloat*taCheckQUANT.AsFloat*rProc)/100; //  /100*100 � ������� �������
//          if CurPos.DSum<>0 then
//          begin
          taCheck.Edit;
          taCheckDPROC.AsFloat:=CurPos.DProc;
          taCheckDSUM.AsFloat:=CurPos.DSum;
          taCheck.Post;

          prWriteLog('~!DiscManual;'+IntToStr((Nums.iCheckNum))+';'+taCheckARTICUL.AsString+';'+FloatToStr(CurPos.DSum)+';'+FloatToStr(CurPos.DProc)+';'+IntToStr(taCheckNUMPOS.AsInteger)+';');
          iNumPos:=taCheckNUMPOS.AsInteger;
//          end;
          taCheck.Next;
        end;
        trUpdCh.Commit;
      except
        trUpdCh.Rollback;
      end;
      dsCheck.DataSet:=taCheck;
      ViewCh.EndUpdate;
    end;

    //������ ������� �������
    if CurPos.Articul>'' then
    begin
      CurPos.DProc:=rProc;
      CurPos.DSum:=RoundEx(CurPos.Price*CurPos.Quant*rProc)/100; //  /100*100 � ������� �������

      inc(iNumPos);
      prWriteLog('~!Disc;'+IntToStr((Nums.iCheckNum))+';'+CurPos.Articul+';'+FloatToStr(CurPos.DSum)+';'+FloatToStr(CurPos.DProc)+';'+IntToStr(iNUMPOS)+';');

//      if CurPos.DSum<>0 then
//      begin
      //������ ������������
      with fmMainCasher do
      begin
        Str(CurPos.DProc:5:2,StrWk); While Pos(' ',StrWk)>0 do Delete(StrWk,Pos(' ',StrWk),1);
        Label18.Caption:='������ '+StrWk+'%';
        Label18.Visible:=True;
        Str(CurPos.DSum:10:2,StrWk); While Pos(' ',StrWk)>0 do Delete(StrWk,Pos(' ',StrWk),1);
        Label19.Caption:=StrWk+'�.';
        Label19.Visible:=True;
      end;
//      end;
    end;
    CalcSum;
    FormLog('ManualDiscount','CashNum '+INtToStr(CommonSet.CashNum));
    TextEdit3.SetFocus;
  end;
end;

procedure TfmMainCasher.acUpExecute(Sender: TObject);
begin
  with dmC do
  begin
    try
      taCheck.Prior;
    except
    end;
  end;
end;

procedure TfmMainCasher.acDownExecute(Sender: TObject);
begin
  with dmC do
  begin
    try
      taCheck.Next;
    except
    end;
  end;
end;

procedure TfmMainCasher.acDelPosExecute(Sender: TObject);
begin
  if not CanDo('prDelPos') then begin fmMainCasher.Memo2.Clear; fmMainCasher.Memo2.Lines.Add('��� ���� �� ��������.');  exit;end;
  with dmC do
  begin
    if taCheckNUMPOS.AsInteger>0 then
    begin
      if MessageDlg('�� ������������� ������ ������� '+IntToStr(taCheckNUMPOS.AsInteger)+' ������� ���� ? ('+taCheckNAME.AsString+')', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        if CommonSet.PrintFisCheckOnEnd=0 then
        begin
          SelPos.Articul:=taCheckARTICUL.AsString;
          SelPos.Bar:=taCheckRBAR.AsString;
          SelPos.Name:=taCheckNAME.AsString;
          SelPos.Quant:=taCheckQUANT.AsFloat*(-1);
          SelPos.Price:=rv(taCheckPRICE.AsFloat);
          SelPos.DProc:=taCheckDPROC.AsFloat;
          SelPos.DSum:=rv(taCheckDSUM.AsFloat*(-1));
          SelPos.Depart:=taCheckDEPART.AsInteger;
          SelPos.EdIzm:=taCheckEDIZM.AsString;
          SelPos.AMark:=taCheckAMARK.AsString;
          prAddPosSel(1,1);
        end else
        begin
          SelPos.Articul:=taCheckARTICUL.AsString;
          SelPos.Bar:=taCheckRBAR.AsString;
          SelPos.Name:=taCheckNAME.AsString;
          SelPos.Quant:=taCheckQUANT.AsFloat*(-1);
          SelPos.Price:=rv(taCheckPRICE.AsFloat);
          SelPos.DProc:=taCheckDPROC.AsFloat;
          SelPos.DSum:=rv(taCheckDSUM.AsFloat*(-1));
          SelPos.Depart:=taCheckDEPART.AsInteger;
          SelPos.EdIzm:=taCheckEDIZM.AsString;
          SelPos.AMark:=taCheckAMARK.AsString;

          prWriteLog('!!prAddPosSel;1; DelPos;');

          trUpdCh.StartTransaction;
          taCheck.Delete;
          trUpdCh.Commit;
          CalcSum;
        end;
      end;
    end;
  end;
end;

procedure TfmMainCasher.acPriceExecute(Sender: TObject);
begin
  if not CanDo('prChangePrice') then begin fmMainCasher.Memo2.Clear; fmMainCasher.Memo2.Lines.Add('��� ���� �� ��������.');  exit;end;
  if CurPos.Articul>'' then
  begin
    fmCalc.CalcEdit1.Value:=CurPos.Price;
    fmCalc.ShowModal;
    if fmCalc.ModalResult=mrOk then
    begin
      CurPos.Price:=fmCalc.CalcEdit1.EditValue;

      prWriteLog('~!Price;'+IntToStr((Nums.iCheckNum))+';'+CurPos.Articul+';'+FloatToStr(CurPos.Price)+';');

      ResetPos;
      CalcSum;
    end;
  end;
end;

procedure TfmMainCasher.devScanerRxChar(Sender: TObject; Count: Integer);
Var StrAdd:string;
begin
  devScaner.ReadStr(StrAdd, Count);
//  sScan:=sScan+Str;
  sScanN:=sScanN+StrAdd;
  TimerSaveScan.Enabled:=True;
end;

procedure TfmMainCasher.TimerScanTimer(Sender: TObject);
Var CurrTime:TDateTime;
    StrN:String;
    bRasch:Boolean;
    rKrep:Real;
    sGr:String;
    sMess:String;
    iArt,iNumPos:Integer;
    ScanBar:string;
    i:Integer;
begin
    TimerScan.Enabled:=False;
    ScanBar:='';

    for i:=1 to 200 do  if ArBar[i]>'' then begin ScanBar:=Trim(ArBar[i]); ArBar[i]:='';  Break; end;

      if ScanBar>'' then
      begin
//        prWriteLog('                          '+ScanBar+' '+its(i));
//        delay(500);
//        TimerScan.Enabled:=True;
//        Exit;

        if fmPre.Showing then
        begin  //���� ������ ��������
          if FindPers(ScanBar) then
          begin
            if pos('�����',Person.Name)>0 then prWriteLog('&!PersA;'+IntToStr(Person.Id)+';'+Person.Name+';')
            else prWriteLog('&!Pers;'+IntToStr(Person.Id)+';'+Person.Name+';');

            SetForegroundWindow(Application.Handle);

            Panel3.Visible:=True;
            with dmC do
            begin
              if taCheck.Active=False then
              begin
                taCheck.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
                taCheck.Active:=True;
              end;
              taCheck.First;
              while not taCheck.Eof do
              begin
                CountSec:=0;
                Timer2.Enabled:=False;
                if fmMainCasher.tag=0 then
                  if fmMainCasher.GridCh.Height<329 then fmMainCasher.GridCh.Height:=fmMainCasher.GridCh.Height+18;
                if (fmMainCasher.tag=1)or(fmMainCasher.tag=17) then
                  if fmMainCasher.GridCh.Height<280 then fmMainCasher.GridCh.Height:=fmMainCasher.GridCh.Height+18;

                taCheck.Next;
              end;
            end;
            CalcSum;
            CountSec:=0;
            Timer2.Enabled:=True;

            Label13.Caption:=Person.Name;

            fGetFN;       // ������ �� ����������� ����������
            WriteStatusFN;

            if CanDo('prCashRep') then cxButton1.Visible:=True else cxButton1.Visible:=False;

            fmPre.close;
          end;
        end else
        begin //���� ������� �����, ����� ��������
          if pos('22'+its(CommonSet.CutTailCode),ScanBar)=1 then //��� ������� - ������������ ��� �������
          begin
            Memo2.Clear;
            with dmFB do
            begin
              try
                Memo2.Lines.Add('����� ... ���� ����� ����.');
                Cash.Close;
                Cash.DBName:=CommonSet.CashDB;
                Cash.Open;
              except
                Memo2.Lines.Add('    ������ �������� ���� ������. ���������� � ���������������.');
              end;

              if Cash.Connected then
              begin
                if Check.Operation=1 then
                begin
                  quXCheck.Active:=False;
                  quXCheck.ParamByName('SBAR').AsString:=ScanBar;
                  quXCheck.Active:=True;
                  if quXCheck.RecordCount>0 then
                  begin
                    Check.ECheck:=1;

                    Memo2.Lines.Add('��� "'+ScanBar+'" ������ - '+its(quXCheck.RecordCount)+' �������.'); delay(10);
                    quXCheck.First;
                    while not quXCheck.Eof do
                    begin
                      Memo2.Lines.Add('��� '+its(quXCheckICODE.AsInteger)+' ���-�� '+fs(quXCheckQUANT.asfloat)+' ����� '+fs(quXCheckRSUM.AsFloat)); delay(10);
                      //����������� ���������� �����
                      if quXCheckQUANT.AsFloat>0 then
                      begin
                        if prAddByCode(quXCheckICODE.AsInteger,quXCheckBARCODE.AsString,quXCheckQUANT.AsFloat,sMess) then prAddPosSel(0,1)
                        else
                        begin
                          Memo2.Lines.Add(sMess);
                          fmAttention.Label1.Caption:=sMess;
                          prWriteLog('~~AttentionShow;'+sMess);
                          fmAttention.ShowModal;
                        end;
                      end;

                      quXCheck.Next;
                    end;
                  end else
                    Memo2.Lines.Add('��� "'+ScanBar+'" �� ������.');

                  quXCheck.Active:=False;

                end else
                  Memo2.Lines.Add('    ������ ���� ����� �������.');

                delay(100);
                Cash.Close;
              end;
            end;
          end else
          begin
            if not FindBar(SOnlyDigit1(ScanBar)) then  // �� ����� ����� �� ���������
            begin
              if FindPers(ScanBar) then
              begin
                if pos('�����',Person.Name)>0 then prWriteLog('&!PersA;'+IntToStr(Person.Id)+';'+Person.Name+';')
                else prWriteLog('&!Pers;'+IntToStr(Person.Id)+';'+Person.Name+';');

                //��� �� ��������� - ����� ��������

                Label13.Caption:=Person.Name;
                if CanDo('prCashRep') then cxButton1.Visible:=True else cxButton1.Visible:=False;
                ScanBar:='';
              end
              else
              begin
                Beep;
                Memo2.Clear;
                Memo2.Lines.Add('�������� - "'+ScanBar+'" �� ������.');
                fmBarMessage.ShowModal;
              end;
            end
            else  //����� ����� �� ��
            begin
              if SelPos.Price=0 then
              begin
                Memo2.Clear;
                Memo2.Lines.Add(ZerroMess);
                fmAttention.Label1.Caption:=ZerroMess;
                fmAttention.ShowModal;
              end else
              begin
                if (fmTestVes.Visible)and(bAddVesPos=True) then   //���� �������� ����������� � ���� ���� ������
                begin
                  with dmC do
                  begin
                    if bScale then
                    begin
    //                  if FindBar(SOnlyDigit1(ScanBar)) then
    //                  begin
                        iArt:=StrToINtDef(SelPos.Articul,0);
                        if (iArt>0) then
                        begin
                          if teVesT.Locate('Articul',iArt,[]) then
                          begin //��� ����� ��� �������� ����� ��������������
                            try

                              with fmTestVes do
                              begin
                                iNumPos:=teTestVesNum.AsInteger+1;

                                teTestVes.Append;
                                teTestVesNum.AsInteger:=iNumPos;
                                teTestVesArticul.AsInteger:=iArt;
                                teTestVesBarCode.AsString:=SelPos.Bar;
                                teTestVesName.AsString:=SelPos.Name;
                                teTestVesQuant.AsFloat:=SelPos.Quant;
                                teTestVesPrice.AsFloat:=SelPos.Price;
                                teTestVesrSum.AsFloat:=SelPos.Quant*SelPos.Price;
                                teTestVes.Post;

                                GrTestVes.Visible:=True;
                              end;
      //                      fmTestVes.ShowModal;
                            except
                            end;
                          end;
                        end;
    //                  end;
                    end;
                  end;
                  ScanBar:='';
                end else
                begin
                  bRasch:=True;
                  CurrTime:=frac(now);
                  if (CommonSet.AlkoKrepMax>0)and((CurrTime<CommonSet.WkTimeBeg)or((CurrTime>CommonSet.WkTimeEnd))) then   //�������� ��������
                  begin
                    StrN:=SelPos.Name;

                    //�������� ��������
                    if pos('%',StrN)>0 then
                    begin
                      StrN:=Copy(StrN,1,pos('%',StrN)-1);
                      while pos(' ',StrN)>0 do Delete(StrN,1,pos(' ',StrN));
                      while pos('.',StrN)>0 do StrN[pos('.',StrN)]:=',';
                      rKrep:=StrToFloatDef(StrN,0);
    //                if (rKrep-CommonSet.AlkoKrepMax)>0.001 then bRasch:=False;
                      if (rKrep-CommonSet.AlkoKrepMax)>0.001 then
                      begin
                        sGr:=FindClName(StrToIntDef(SelPos.Articul,0));
                        if pos('���',sGr)=1 then  bRasch:=False;
                      end;
                    end;
                  end;
                  if bRasch then
                  begin
                    //����� ������� ��� �������� ����� ���� �����
                    if (SelPos.AVid>0)and(CommonSet.GetAMark=1) then //��� �� ���� � ����������� �� �����
                    begin
                      if prGetFixAVid(SelPos.AVid) then
                      begin //����� ����������� ��
                        Beep;
                        fmGetAM.ShowModal;
                        if fmGetAM.ModalResult=mrOk then
                        begin
                          SelPos.AMark:=fmGetAM.cxLabel2.Caption;
                        end else bRasch:=False;
                      end;
                    end;

                    if bRasch then prAddPosSel(0,1);
                  end else
                  begin
                    Memo2.Lines.Add('��������� ������� �������� ��������� ����� '+IntToStr(CommonSet.AlkoKrepMax)+'%');
                    fmAttention.Label1.Caption:='��������� ������� �������� ��������� ����� '+IntToStr(CommonSet.AlkoKrepMax)+'% � '+FormatDateTime('hh:nn ss',CommonSet.WkTimeBeg)+' �� '+FormatDateTime('hh:nn ss',CommonSet.WkTimeEnd);
                    prWriteLog('~~AttentionShow;'+'��������� ������� �������� ��������� ����� '+IntToStr(CommonSet.AlkoKrepMax)+'%');
                    fmAttention.ShowModal;
                  end;
                end;
              end;
            end;
          end;
          TextEdit3.Text:=ScanBar;
          TextEdit3.SetFocus;
          TextEdit3.SelectAll;
        end;

      end;

    TimerScan.Enabled:=True;
end;

procedure TfmMainCasher.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  CashClose;
end;

procedure TfmMainCasher.acCashDriverOpenExecute(Sender: TObject);
begin
  if not CanDo('prOpenCashDrv') then begin fmMainCasher.Memo2.Clear; fmMainCasher.Memo2.Lines.Add('��� ���� �� ��������.');  exit;end;
  prWriteLog('!!CashDrOpen;');
  CashDriver;
end;

procedure TfmMainCasher.acRetExecute(Sender: TObject);
Var sC:String;
    iNumZ,iNumCh,iPos:Integer;
    s1,s2:String;
    bRet,bStop,bDisc:Boolean;
    iSumPos:INteger;
    iR:INteger;
    StrWk:String;
    bAdd:Boolean;
begin
  //�������
  if bBegCash then exit; //���� ������� ������, �� ������ ������ ������ (����� �������� ��������� ���������� ����)
  if not CanDo('prCanRet') then begin fmMainCasher.Memo2.Clear; fmMainCasher.Memo2.Lines.Add('��� ���� �� ��������.');  exit;end;
  if Check.ChBeg then
  begin
    ShowMessage('��� ���������� ��������, ��������� ���.');
    Memo2.Clear;
    Memo2.Lines.Add('��� ���������� ��������, ��������� ���.');
  end
  else
  begin
    if Check.Operation=0 then strwk:='�������� �������?';
    if Check.Operation=1 then strwk:='�������� �������?';
    if MessageDlg(StrWk,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      if Check.Operation=0 then
      begin      //������
        Check.Operation:=1;
        prWriteLog('~!RetCheckOff;'+IntToStr(Nums.iCheckNum));
      end else
      begin
        Check.Operation:=0;
        prWriteLog('~!RetCheckOn;'+IntToStr(Nums.iCheckNum));
      end;
      WriteStatus;

      iNumZ:=0;
      iNumCh:=0;

      if Check.Operation=0 then
      begin

      sC:=TextEdit3.Text;
      if sC>'' then
      begin
        iPos:=0;
        if iPos=0 then iPos:=pos('.',sc);
        if iPos=0 then iPos:=pos(',',sc);
        if iPos>0  then
        begin //�� ������
          s1:=Copy(sc,1,iPos-1);
          s2:=Copy(sc,iPos+1,length(sc)-iPos);
          iNumZ:=StrToIntDef(s1,0);
          iNumCh:=StrToIntDef(s2,0);
        end
        else //������ ����� ����
        begin
          iNumZ:=Nums.ZNum;
          iNumCh:=StrToIntDef(sC,0);
        end;
      end;

      prWriteLog('~!RetCheck;'+IntToStr((Nums.iCheckNum))+';'+its(Check.Operation)+';ZNumRet='+its(iNumZ)+';CheckToRet='+its(iNumCh));

      if (iNumZ>=0)and(iNumCh>0) then
      begin
        with dmC do
        begin
          quFindCheck.Active:=False;
          quFindCheck.ParamByName('SHOP').AsInteger:=CommonSet.ShopIndex;
          quFindCheck.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
          quFindCheck.ParamByName('ZNUM').AsInteger:=iNumZ;
          quFindCheck.ParamByName('CHNUM').AsInteger:=iNumCh;
          quFindCheck.Active:=True;

          if quFindCheck.RecordCount>0 then
          begin
            with fmRetCheck do
            begin
              teRet.Active:=False;
              teRet.Active:=True;
              fmRetCheck.ViewRet.BeginUpdate;
              quFindCheck.First;
              while not quFindCheck.Eof do
              begin
                teRet.Append;
                teRetCASHNUM.AsInteger:=CommonSet.CashNum;
                teRetNUMPOS.AsInteger:=quFindCheckID.AsInteger;
                teRetARTICUL.AsString:=quFindCheckARTICUL.AsString;
                teRetRBAR.AsString:=quFindCheckBAR.AsString;
                teRetNAME.AsString:=quFindCheckNAME.AsString;
                teRetQUANT.AsFloat:=quFindCheckQUANTITY.AsFloat;
                teRetPRICE.AsFloat:=quFindCheckPRICERUB.AsFloat;
                teRetDPROC.AsFloat:=quFindCheckDPROC.AsFloat;
                teRetDSUM.AsFloat:=quFindCheckDSUM.AsFloat;
                teRetRSUM.AsFloat:=quFindCheckTOTALRUB.AsFloat;
                teRetEDIZM.AsString:=quFindCheckMESURIMENT.AsString;
                teRetIRET.AsInteger:=0;
                teRetAMARK.AsString:=quFindCheckAMARK.AsString;
                teRet.Post;

                quFindCheck.Next;
              end;
              teRet.First;
              fmRetCheck.ViewRet.EndUpdate;

              cxCurrencyEdit1.Value:=0;
            end;
            fmRetCheck.ShowModal;
            if fmRetCheck.ModalResult=mrOk then
            begin
              with fmRetCheck do
              begin
                bRet:=False; //��� ������ ��� �������� �� ���������
                bDisc:=False;
                fmRetCheck.ViewRet.BeginUpdate;
                teRet.First;
                while not teRet.Eof do
                begin
                  if teRetIRET.AsInteger=1 then  bRet:=True;
                  if teRetDSUM.AsFloat>0 then bDisc:=True;
                  teRet.Next;
                end;
                if bRet then //���� ���-�� �� ������� - ������� ��� ��� ��������
                begin
                  // ���������� �� � ���
                  try

                    //������������ ������� ������
                    fmMainCasher.Timer2.Enabled:=False;
                    CountSec:=0;
                    Check.ChBeg:=True;
                    Check.ChEnd:=False;
                    bBegCash:= False; //�������������� ��������� ������ ��� ������� ��������
                    bBegCashEnd:= False;

                    bStop:=False;

                    if CommonSet.PrintFisCheckOnEnd=0 then
                    begin
                      //�������� ������� �����
                      Nums.iRet:=InspectSt(Nums.sRet);
                      if Nums.iRet<>0 then TestFp('InspectSt');
                      //���� ������ ����� ��������� ���
                      //������� ��������� �����

                      if not OpenZ then ZOpen;
                      if CheckRetStart=False then bStop:=True; //������ ����
                    end;

                    if bStop=False then
                    begin
                      prWriteLog('~!RetCheckStart;'+IntToStr((Nums.iCheckNum))+';'+its(Check.Operation)+';ZNumRet='+its(iNumZ)+';CheckToRet='+its(iNumCh));

                      with fmMainCasher do
                      begin
                        if bDisc=True then //������ ���� ����� �� �������� �.�. �������
                        begin
                          Label18.Visible:=False;
                          Label19.Visible:=False;
                        end;
                      end;

                      teRet.First;
                      while not teRet.Eof do
                      begin
                        if (teRetIRET.AsInteger=1)and(teRetQUANT.AsFloat>0) then
                        begin
                          if fmMainCasher.tag=0 then
                            if fmMainCasher.GridCh.Height<329 then fmMainCasher.GridCh.Height:=fmMainCasher.GridCh.Height+18;
                          if (fmMainCasher.tag=1)or(fmMainCasher.tag=17) then
                            if fmMainCasher.GridCh.Height<280 then fmMainCasher.GridCh.Height:=fmMainCasher.GridCh.Height+18;

                          CurPos.ARTICUL:=teRetARTICUL.AsString;
                          CurPos.NAME:=teRetNAME.AsString;
                          CurPos.QUANT:=teRetQUANT.AsFloat;
                          CurPos.PRICE:=rv(teRetRSUM.AsFloat/teRetQUANT.AsFloat);
                          CurPos.DPROC:=0;
                          CurPos.DSUM:=0;
                          CurPos.DEPART:=teRetDEPART.AsInteger;
                          iR:=RoundEx(CurPos.PRICE*CurPos.QUANT*100);
                          CurPos.AMark:=teRetAMARK.AsString;

                          bAdd:=True;
                          //      ��� ���������� � ��������� ��������
                          if CommonSet.PrintFisCheckOnEnd=0 then bAdd:=CheckAddPos(iSumPos) else iSumPos:=iR;
                          if bAdd then
                          begin
                            trUpdCh.StartTransaction;

                            taCheck.Append;
                            taCheckCASHNUM.AsInteger:=CommonSet.CashNum;
                            taCheckNUMPOS.AsInteger:=teRetNUMPOS.AsInteger;
                            taCheckARTICUL.AsString:=CurPos.Articul;
                            taCheckRBAR.AsString:=teRetRBAR.AsString;
                            taCheckNAME.AsString:=CurPos.Name;
                            taCheckQUANT.AsFloat:=CurPos.Quant;
                            taCheckPRICE.AsFloat:=CurPos.Price;
                            taCheckDPROC.AsFloat:=CurPos.DProc;
                            taCheckDSUM.AsFloat:=CurPos.DSum;
                            taCheckRSUM.AsFloat:=iR/100;
                            taCheckDEPART.AsInteger:=CurPos.Depart;
                            taCheckEDIZM.AsString:=CurPos.EdIzm;
                            taCheckTIMEPOS.AsDateTime:=now;
                            taCheckAMARK.AsString:=CurPos.AMark;
                            taCheck.Post;

                            trUpdCh.Commit;

                            if abs(iR-iSumPos)>0 then
                            begin //����������� ����� �� �������, ���������� ������ ���-�� �����������
                              if CommonSet.PrintFisCheckOnEnd=0 then
                              begin
                                prClearCheck1;  //��������� ���
                                prWriteLog('~~CheckAddPosRet;'+IntToStr((Nums.iCheckNum))+';'+CurPos.Articul+';'+CurPos.Bar+';'+FloatToStr(CurPos.Quant)+';'+FloatToStr(CurPos.Price)+';'+FloatToStr(CurPos.DSum)+';��-'+INtToStr(iSumPos)+';����-'+INtToStr(iR));
                              end;

                              ClearPos;
                              ClearSelPos;
                              ResetPos;

                              CalcSum;
                              quFindCheck.Active:=False;
                              TextEdit3.Text:='';
                              fmRetCheck.teRet.Active:=False;
                              exit;
                            end;
                          end;
                          TestFp('CheckAddPos');
                        end;
                        teRet.Next;
                      end;
                      ClearPos;
                      ClearSelPos;
                      ResetPos;

                      CalcSum;
                    end else
                    begin
                      Check.ChBeg:=False;
                      Check.ChEnd:=True;

                      ClearPos;
                      ClearSelPos;
                      ResetPos;

                      CalcSum;
                      fmMainCasher.TextEdit3.Text:='';
                      fmMainCasher.TextEdit3.SetFocus;

                      TestStatus('StartCheck',StrWk);
                      fmMainCasher.Memo2.Clear;
                      fmMainCasher.Memo2.Lines.Add(StrWk);
                      fmAttention.Label1.Caption:=StrWk;
                      fmAttention.ShowModal;
                    end;
                  except;
                    trUpdCh.Rollback;
                  end;
                end;
                fmRetCheck.ViewRet.EndUpdate;
              end;

//              showmessage('������');
            end;
          end;

          quFindCheck.Active:=False;
          TextEdit3.Text:='';
          fmRetCheck.teRet.Active:=False;
        end;
      end;
      end else  TextEdit3.Text:='';
    end;
  end;
end;

procedure TfmMainCasher.cxButton2Click(Sender: TObject);
var  rSum1,rSum2,rSum3,rSum4:Real;
    bNotErr:Boolean;
    StrWk:String;
    iNumSm,iCountDoc:Integer;
    rSumNal,rSumRet,rSumBNal,rSumBRet:Real;
    l:ShortInt;
    StrP:String;
    rSumDiscN,rSumDiscBN,rSumDiscNRet,rSumDiscBNRet:Real;
    rSumRetNI,rSumSaleNI:Real;
//    iACheck:Integer;

begin
  if not CanDo('prXRep') then begin fmMainCasher.Memo2.Clear; fmMainCasher.Memo2.Lines.Add('��� ���� �� ��������.');  exit;end;
  if Check.ChBeg then
  begin
    ShowMessage('��� ��������� �-������,��������� ���.');
    Memo2.Clear;
    Memo2.Lines.Add('��� ��������� �-������,��������� ���.');
  end
  else
  begin
    prWriteLog('!!XRep;');
    if OpenZ then
    begin
      if CommonSet.NoFis=0 then
      begin
        if GetX=False then
        begin
          Showmessage('������ ��� ��������� X-������.');
          Memo2.Clear;
          Memo2.Lines.Add('������ ��� ��������� X-������.');
        end else
        begin
          if (CommonSet.TypeFis='sp101') then
          begin
            if CommonSet.RepCassir=1 then
            begin
              bNotErr:=True;
              if OpenNFDoc=false then  begin  TestFp('PrintNFStr'); OpenNFDoc; end;
              StrWk:='  ����� �� ��������  (X)   ';
              if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
              StrWk:='----------------------------------------';
              if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

              StrWk:=FormatDateTime('dd.mm.yyyy hh:nn:ss ',now)+its(Nums.ZNum);
              if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

              StrWk:=' ';
              if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

              with dmC do
              begin
                quCassirZList.Active:=False;
                quCassirZList.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
                quCassirZList.ParamByName('ZNUM').AsInteger:=Nums.ZNum;
                quCassirZList.Active:=True;

                quCassirZList.First;
                while not quCassirZList.Eof do
                begin
                  rSumNal:=0; rSumRet:=0; rSumBNal:=0; rSumBRet:=0;

                  quCassirZSum.Active:=False;
                  quCassirZSum.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
                  quCassirZSum.ParamByName('ZNUM').AsInteger:=Nums.ZNum;
                  quCassirZSum.ParamByName('CASSIR').AsInteger:=quCassirZListCASHER.AsInteger;
                  quCassirZSum.Active:=True;
                  quCassirZSum.First;
                  while not quCassirZSum.Eof do
                  begin
                    if quCassirZSumOPERATION.AsInteger=0 then rSumRet:=quCassirZSumRSUM.AsFloat;
                    if quCassirZSumOPERATION.AsInteger=1 then rSumNal:=quCassirZSumRSUM.AsFloat;
                    if quCassirZSumOPERATION.AsInteger=4 then rSumBRet:=quCassirZSumRSUM.AsFloat;
                    if quCassirZSumOPERATION.AsInteger=5 then rSumBNal:=quCassirZSumRSUM.AsFloat;

                    quCassirZSum.Next;
                  end;
                  quCassirZSum.Active:=False;

                  StrWk:='������ - '+quCassirZListNAME.AsString;
                  if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

                  Str(rSumNal:9:2,StrWk);
                  StrWk:='  ����� ������ ��������    - '+StrWk;
                  if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

                  Str(rSumBNal:9:2,StrWk);
                  StrWk:='  ����� ������ ����.������ - '+StrWk;
                  if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

                  Str(rSumRet:9:2,StrWk);
                  StrWk:='  ����� ��������� �������� - '+StrWk;
                  if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

                  Str(rSumBRet:9:2,StrWk);
                  StrWk:='  ����� �����. ����.������ - '+StrWk;
                  if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

                  StrWk:=' ';
                  if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

                  quCassirZList.Next;
                end;

                quCassirZList.Active:=False;
               end;

              StrWk:='----------------------------------------';
              if bNotErr then PrintNFStr(StrWk) else TestFp('PrintNFStr');

              if CloseNFDoc=False then  begin   TestFp('PrintNFStr');  CloseNFDoc; end;
            end;

            if GetSumDisc(rSum1,rSum2,rSum3,rSum4) then
            begin
              bNotErr:=True;
              if OpenNFDoc=false then  begin  TestFp('PrintNFStr'); OpenNFDoc; end;
              StrWk:='  ������� �� ����������� �������  (X)   ';
              if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
              StrWk:='----------------------------------------';
              if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

              StrWk:=FormatDateTime('dd.mm.yyyy hh:nn:ss ',now)+its(Nums.ZNum);
              if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

              StrWk:=' ';
              if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

              Str(rSum1:9:2,StrWk);
              StrWk:='����� ������  �� ��������   - '+StrWk;
              if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

              Str(rSum2:9:2,StrWk);
              StrWk:='����� ������� �� ��������   - '+StrWk;
              if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

              Str(rSum3:9:2,StrWk);
              StrWk:='����� ������  �� ���������  - '+StrWk;
              if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

              Str(rSum4:9:2,StrWk);
              StrWk:='����� ������� �� ���������  - '+StrWk;
              if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

              StrWk:='----------------------------------------';
              if bNotErr then PrintNFStr(StrWk) else TestFp('PrintNFStr');

              if CloseNFDoc=False then  begin   TestFp('PrintNFStr');  CloseNFDoc; end;

            end;
          end;

          if CommonSet.DiscToPrice=1 then  //������ �������� � ���� -  ���������� ������ ��� - ���������
          begin
            with dmC do
            begin
              rSumDiscN:=0;
              rSumDiscBN:=0;
              rSumDiscNRet:=0;
              rSumDiscBNRet:=0;

              rSum1:=0; rSum2:=0; rSum3:=0; rSum4:=0;

              quDSumCh.Active:=False;
              quDSumCH.ParamByName('ZNUM').AsInteger:=Nums.ZNum;
              quDSumCH.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
              quDSumCH.Active:=True;
              quDSumCH.First;
              while not quDSumCH.Eof do
              begin
                if quDSumChOPERATION.AsInteger=0 then rSumDiscNRet:=quDSumChDSUM.asfloat; //������� ���
                if quDSumChOPERATION.AsInteger=1 then rSumDiscN:=quDSumChDSUM.asfloat; //������� ���
                if quDSumChOPERATION.AsInteger=4 then rSumDiscBNRet:=quDSumChDSUM.asfloat; //������� ����
                if quDSumChOPERATION.AsInteger=5 then rSumDiscBN:=quDSumChDSUM.asfloat; //������� ����

                quDSumCH.Next;
              end;
              quDSumCH.Active:=False;
            end;

            rSum1:=rSumDiscN+rSumDiscBN;
            rSum3:=rSumDiscNRet+rSumDiscBNRet;

            bNotErr:=True;
            if OpenNFDoc=false then  begin  TestFp('PrintNFStr'); OpenNFDoc; end;
            StrWk:='  ������� �� ����������� �������  (X)   ';
            if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
            StrWk:='----------------------------------------';
            if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

            StrWk:=FormatDateTime('dd.mm.yyyy hh:nn:ss ',now)+its(Nums.ZNum);
            if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

            StrWk:=' ';
            if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

            Str(rSum1:9:2,StrWk);
            StrWk:='����� ������  �� ��������   - '+StrWk;
            if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

            Str(rSum2:9:2,StrWk);
            StrWk:='����� ������� �� ��������   - '+StrWk;
            if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

            Str(rSum3:9:2,StrWk);
            StrWk:='����� ������  �� ���������  - '+StrWk;
            if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

            Str(rSum4:9:2,StrWk);
            StrWk:='����� ������� �� ���������  - '+StrWk;
            if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

            StrWk:='----------------------------------------';
            if bNotErr then PrintNFStr(StrWk) else TestFp('PrintNFStr');

            if CloseNFDoc=False then  begin   TestFp('PrintNFStr');  CloseNFDoc; end;
          end;
        end;
      end else //����� ������������
      begin
    //������ ������� ��� ���� ���������
        with dmC do
        with dmTr do
        begin
        //�������� ����� ��� ��������

          rSumRet:=0;
          rSumNal:=0;
          rSumBRet:=0;
          rSumBNal:=0;

          iNumSm:=Nums.ZNum;
          iCountDoc:=Nums.iCheckNum;


          quSumTr.Active:=False;
          quSumTr.SelectSQL.Clear;
          quSumTr.SelectSQL.Add('SELECT Operation,Sum(TOTALRUB) as SumOp  FROM CASHSAIL');
          quSumTr.SelectSQL.Add('WHERE');
          quSumTr.SelectSQL.Add('SHOPINDEX=1');
          quSumTr.SelectSQL.Add('and CASHNUMBER='+IntToStr(CommonSet.CashNum));
          quSumTr.SelectSQL.Add('and ZNUMBER='+IntToStr(Nums.ZNum));
          quSumTr.SelectSQL.Add('Group by Operation');
          quSumTr.SelectSQL.Add('Order by Operation');
          quSumTr.Active:=True;

          quSumTr.First;
          while not quSumTr.Eof do
          begin
            Case quSumTrOPERATION.AsInteger of
            0:rSumRet:=quSumTrSUMOP.AsFloat;
            1:rSumNal:=quSumTrSUMOP.AsFloat;
            4:rSumBRet:=quSumTrSUMOP.AsFloat;
            5:rSumBNal:=quSumTrSUMOP.AsFloat;
            end;

            quSumTr.Next;
          end;
          quSumTr.Active:=False;


          rSumDiscN:=0;
          rSumDiscBN:=0;

          quDSum.Active:=False;
          quDSum.ParamByName('ZNUM').AsInteger:=Nums.ZNum;
          quDSum.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
          quDSum.Active:=True;
          quDSum.First;
          while not quDSum.Eof do
          begin
            if quDSumPAYMENT.AsInteger>0 then
            begin //������
              if quDSumOPER.AsInteger=1 then rSumDiscBN:=rSumDiscBN+quDSumDSUM.asfloat //�������
              else rSumDiscBN:=rSumDiscBN-quDSumDSUM.asfloat;
            end else //���
            begin
              if quDSumOPER.AsInteger=1 then rSumDiscN:=rSumDiscN+quDSumDSUM.asfloat //�������
              else rSumDiscN:=rSumDiscN-quDSumDSUM.asfloat;
            end;
            quDSum.Next;
          end;
          quDSum.Active:=False;

          //��� ����������� ����

          rSumSaleNI:=0;
          rSumRetNI:=0;

          quNarItog.Active:=False;
          quNarItog.ParamByName('ZNUM').AsInteger:=iNumSm;
          quNarItog.Active:=True;
          if quNarItog.RecordCount>0 then
          begin
            rSumSaleNI:=quNarItogSUMSALE.AsFloat;
            rSumRetNI:=quNarItogSUMRET.AsFloat;
          end;
          quNarItog.Active:=True;


              //������� ��������
          if (Pos('COM',CommonSet.PortCash)>0)or(Pos('LPT',CommonSet.PortCash)>0) then
          begin
            try
              l:=40;
              prDevOpen(CommonSet.PortCash,0);
              StrP:=CommonSet.PortCash;
              prSetFont(StrP,10,0);
    //          PrintStr(SpecVal.SPH1);
    //          PrintStr(SpecVal.SPH2);
    //          PrintStr(SpecVal.SPH3);
    //          PrintStr(SpecVal.SPH4);
    //          PrintStr(SpecVal.SPH5);

              PrintStr(CommonSet.DepartName);
              PrintStr(CommonSet.PreStr1);
              PrintStr(CommonSet.PreStr2);

              PrintStr('����� '+Its(CommonSet.CashNum));
              PrintStr(FormatDateTime('dd-mm-yyyy                         hh:nn',now));
              PrintStr(' ');
              StrWk:='            X-����� N '+IntToStr(iNumSm);
              PrintStr(StrWk);
              PrintStr(' ');
              PrintStr('��������:');
              PrintStr(prDefFormatStr1(l,'�������',rSumNal));
              PrintStr(prDefFormatStr1(l,'������',rSumDiscN));
              PrintStr(prDefFormatStr1(l,'�����. �������',0));
              PrintStr(prDefFormatStr1(l,'�������',rSumRet));
              PrintStr(' ');
              PrintStr('������:');
              PrintStr(prDefFormatStr1(l,'�������',0));
              PrintStr(prDefFormatStr1(l,'�����. �������',0));
              PrintStr(' ');
              PrintStr('����. �����:');
              PrintStr(prDefFormatStr1(l,'�������',rSumBNal));
              PrintStr(prDefFormatStr1(l,'������',rSumDiscBN));
              PrintStr(prDefFormatStr1(l,'�����. �������',rSumBRet));
              PrintStr(' ');
              PrintStr('================= ����� ================');
              PrintStr(prDefFormatStr1(l,'�������',rSumNal+rSumBNal));
              PrintStr(prDefFormatStr1(l,' ������',rSumDiscN+rSumDiscBN));
              PrintStr(prDefFormatStr1(l,'�������',rSumRet+rSumBRet));
              PrintStr(' ');
              PrintStr(prDefFormatStr2(l,'��������� ����������:',iCountDoc+4));
              PrintStr(prDefFormatStr2(l,'� �.�. �����:',iCountDoc));
              PrintStr(prDefFormatStr2(l,'� �.�. �����. �����:',0));
              PrintStr(prDefFormatStr2(l,'� �.�. ������. ���. ���. �����:',0));
              PrintStr(' ');
              PrintStr(prDefFormatStr2(l,'�������:',iCountDoc));
              PrintStr(prDefFormatStr2(l,'�����. �������:',0));
              PrintStr(prDefFormatStr2(l,'�������:',0));
              PrintStr(prDefFormatStr2(l,'������������:',0));
              PrintStr(prDefFormatStr2(l,'����������:',0));
              PrintStr(' ');
              PrintStr(prDefFormatStr1(l,'������. �������:',rSumSaleNI));
              PrintStr(prDefFormatStr1(l,'������. ��������:',rSumRetNI));
              PrintStr(' ');

              prCutDoc(CommonSet.PortCash,0);
            finally
              prDevClose(CommonSet.PortCash,0);
            end;
          end;
        end;

      end;
    end
    else  //�������� �����
    begin
      Nums.iRet:=InspectSt(Nums.sRet);
      if Nums.iRet<>0 then
        if TestStatus('InspectSt',sMessage)=False then
        begin
  //          ShowMessage('������: "'+Nums.sRet+'". ��������� �������� ����� ���������� ������.');
          WriteStatus;
          fmAttention.Label1.Caption:=sMessage;
          fmAttention.ShowModal;
          exit;// ������ ��� - ������� ��� ������������ ����
        end;

      if Nums.iRet=0 then
      begin
        if ZOpen=False then
        begin
          Showmessage('������ ��� �������� �����.');
          Memo2.Clear;
          Memo2.Lines.Add('������ ��� �������� �����.');
        end
        else
        begin
          if GetX=False then
          begin
            Showmessage('������ ��� ��������� X-������.');
            Memo2.Clear;
            Memo2.Lines.Add('������ ��� ��������� X-������.');
          end else
          begin
            if CommonSet.TypeFis='sp101' then
            begin
              if CommonSet.RepCassir=1 then
              begin
                bNotErr:=True;
                if OpenNFDoc=false then  begin  TestFp('PrintNFStr'); OpenNFDoc; end;
                StrWk:='  ����� �� ��������  (X)   ';
                if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
                StrWk:='----------------------------------------';
                if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

                StrWk:=FormatDateTime('dd.mm.yyyy hh:nn:ss ',now)+its(Nums.ZNum);
                if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

                StrWk:=' ';
                if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

                with dmC do
                begin
                  quCassirZList.Active:=False;
                  quCassirZList.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
                  quCassirZList.ParamByName('ZNUM').AsInteger:=Nums.ZNum;
                  quCassirZList.Active:=True;

                  quCassirZList.First;
                  while not quCassirZList.Eof do
                  begin
                    rSumNal:=0; rSumRet:=0; rSumBNal:=0; rSumBRet:=0;

                    quCassirZSum.Active:=False;
                    quCassirZSum.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
                    quCassirZSum.ParamByName('ZNUM').AsInteger:=Nums.ZNum;
                    quCassirZSum.ParamByName('CASSIR').AsInteger:=quCassirZListCASHER.AsInteger;
                    quCassirZSum.Active:=True;
                    quCassirZSum.First;
                    while not quCassirZSum.Eof do
                    begin
                      if quCassirZSumOPERATION.AsInteger=0 then rSumRet:=quCassirZSumRSUM.AsFloat;
                      if quCassirZSumOPERATION.AsInteger=1 then rSumNal:=quCassirZSumRSUM.AsFloat;
                      if quCassirZSumOPERATION.AsInteger=4 then rSumBRet:=quCassirZSumRSUM.AsFloat;
                      if quCassirZSumOPERATION.AsInteger=5 then rSumBNal:=quCassirZSumRSUM.AsFloat;

                      quCassirZSum.Next;
                    end;
                    quCassirZSum.Active:=False;

                    StrWk:='������ - '+quCassirZListNAME.AsString;
                    if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

                    Str(rSumNal:9:2,StrWk);
                    StrWk:='  ����� ������ ��������    - '+StrWk;
                    if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

                    Str(rSumBNal:9:2,StrWk);
                    StrWk:='  ����� ������ ����.������ - '+StrWk;
                    if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

                    Str(rSumRet:9:2,StrWk);
                    StrWk:='  ����� ��������� �������� - '+StrWk;
                    if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

                    Str(rSumBRet:9:2,StrWk);
                    StrWk:='  ����� �����. ����.������ - '+StrWk;
                    if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

                    StrWk:=' ';
                    if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

                    quCassirZList.Next;
                  end;

                  quCassirZList.Active:=False;
                 end;

                StrWk:='----------------------------------------';
                if bNotErr then PrintNFStr(StrWk) else TestFp('PrintNFStr');

                if CloseNFDoc=False then  begin   TestFp('PrintNFStr');  CloseNFDoc; end;
              end;

              if GetSumDisc(rSum1,rSum2,rSum3,rSum4) then
              begin
                bNotErr:=True;
                if OpenNFDoc=false then  begin  TestFp('PrintNFStr'); OpenNFDoc; end;
                StrWk:='  ������� �� ����������� �������  (X)   ';
                if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
                StrWk:='----------------------------------------';
                if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

                StrWk:=FormatDateTime('dd.mm.yyyy hh:nn:ss ',now)+its(Nums.ZNum);
                if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

                StrWk:=' ';
                if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

                Str(rSum1:9:2,StrWk);
                StrWk:='����� ������  �� ��������   - '+StrWk;
                if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

                Str(rSum2:9:2,StrWk);
                StrWk:='����� ������� �� ��������   - '+StrWk;
                if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

                Str(rSum3:9:2,StrWk);
                StrWk:='����� ������  �� ���������  - '+StrWk;
                if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

                Str(rSum4:9:2,StrWk);
                StrWk:='����� ������� �� ���������  - '+StrWk;
                if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

                StrWk:='----------------------------------------';
                if bNotErr then PrintNFStr(StrWk) else TestFp('PrintNFStr');

                if CloseNFDoc=False then begin TestFp('PrintNFStr');  CloseNFDoc; end;

              end;
            end;
          end;
        end;
      end;
      CashDate(Nums.sDate);
      GetNums; //������  ���������� X
      GetRes; //�������
      WriteStatus;
    end;
  end;
end;

procedure TfmMainCasher.cxButton3Click(Sender: TObject);
Var iZ:INteger;
    rSum1,rSum2,rSum3,rSum4:Real;
    bNotErr:Boolean;
    StrWk:String;
    iNumSm,iCountDoc:Integer;
    rSumNal,rSumRet,rSumBNal,rSumBRet:Real;
    rSumDiscN,rSumDiscBN,rSumDiscNRet,rSumDiscBNRet:Real;
    l:ShortInt;
    StrP:String;
    rSumRetNI,rSumSaleNI:Real;
    i:INteger;
    dtNow:TDateTime;
begin
  if not CanDo('prZRep') then begin fmMainCasher.Memo2.Clear; fmMainCasher.Memo2.Lines.Add('��� ���� �� ��������.');  exit;end;
  if Check.ChBeg then
  begin
    ShowMessage('��� �������� �����,��������� ���.');
    Memo2.Clear;
    Memo2.Lines.Add('��� �������� �����,��������� ���.');
  end
  else
  begin
  {  if OpenZ then
    begin  }
    prWriteLog('!!ZRep;');

    if MessageDlg('�� ��������� �����?',mtConfirmation, [mbYes, mbNo], 0) = mrNo then
    begin
      dtNow:=now;

      CountSec:=0; fmMainCasher.Timer2.Enabled:=False;

      if CommonSet.NoFis=0 then  //���������� �����
      begin

//       Nums.iRet:=InspectSt(Nums.sRet);
      //����� ����������� �� ���� �.�. - ���������� ������� ����� ���� ������
        Nums.iRet:=0; //�������� ��� ������ �������

        if (Nums.iRet=0) or (Nums.iRet=22) or (Nums.iRet=2)then
        begin
          fmSt.Memo1.Clear;
          fmSt.Show;
          with fmSt do
          with dmC do
          with dmFB do
          begin
            cxButton1.Enabled:=False;
            Memo1.Lines.Add('������ �������� ���� ... �����.'); Delay(10);

//            bNotErr:=False;
            if CommonSet.TypeFis='sp101' then
            begin //���� � ����

              if GetRSumZ then
              begin
                //�������
                quZListDet.Active:=False;
                quZListDet.ParamByName('CNUM').AsInteger:=CommonSet.CashNum;
                quZListDet.ParamByName('ZNUM').AsInteger:=Nums.ZNum;
                quZListDet.Active:=True;
                quZListDet.First;
                while not quZListDet.Eof do quZListDet.Delete;

                for i:=1 to 16 do
                begin
                  if rArrSum[i]<>0 then
                  begin
                    quZListDet.Append;
                    quZListDetCASHNUM.AsInteger:=CommonSet.CashNum;
                    quZListDetZNUM.AsInteger:=Nums.ZNum;
                    quZListDetINOUT.AsInteger:=1; //�������
                    quZListDetITYPE.AsInteger:=i; //��� ���� ��������
                    quZListDetRSUM.AsFloat:=rArrSum[i];
                    quZListDetIDATE.AsInteger:=Trunc(Date);
                    quZListDetDDATE.AsDateTime:=dtNow;
                    quZListDet.Post;
                  end;
                end;

                if GetRetSumZ then
                begin
                  for i:=1 to 16 do
                  begin
                    if rArrSum[i]<>0 then
                    begin
                      quZListDet.Append;
                      quZListDetCASHNUM.AsInteger:=CommonSet.CashNum;
                      quZListDetZNUM.AsInteger:=Nums.ZNum;
                      quZListDetINOUT.AsInteger:=-1; //�������
                      quZListDetITYPE.AsInteger:=i; //��� ���� ��������
                      quZListDetRSUM.AsFloat:=rArrSum[i];
                      quZListDetIDATE.AsInteger:=Trunc(Date);
                      quZListDetDDATE.AsDateTime:=dtNow;
                      quZListDet.Post;
                    end;
                  end;

                  //������ �������
                  if GetSumDisc(rSum1,rSum2,rSum3,rSum4) then
                  begin
//                StrWk:='����� ������  �� ��������   - '+StrWk;
                    if rSum1<>0 then
                    begin
                      quZListDet.Append;
                      quZListDetCASHNUM.AsInteger:=CommonSet.CashNum;
                      quZListDetZNUM.AsInteger:=Nums.ZNum;
                      quZListDetINOUT.AsInteger:=0; //������ - �������
                      quZListDetITYPE.AsInteger:=1; //��� ���� ��������
                      quZListDetRSUM.AsFloat:=rSum1;
                      quZListDetIDATE.AsInteger:=Trunc(Date);
                      quZListDetDDATE.AsDateTime:=dtNow;
                      quZListDet.Post;
                    end;

//                  StrWk:='����� ������� �� ��������   - '+StrWk;
                    if rSum2<>0 then
                    begin
                      quZListDet.Append;
                      quZListDetCASHNUM.AsInteger:=CommonSet.CashNum;
                      quZListDetZNUM.AsInteger:=Nums.ZNum;
                      quZListDetINOUT.AsInteger:=0; //������ - �������
                      quZListDetITYPE.AsInteger:=2; //��� ���� ��������
                      quZListDetRSUM.AsFloat:=rSum2;
                      quZListDetIDATE.AsInteger:=Trunc(Date);
                      quZListDetDDATE.AsDateTime:=dtNow;
                      quZListDet.Post;
                    end;

//                  StrWk:='����� ������  �� ���������  - '+StrWk;
                    if rSum3<>0 then
                    begin
                      quZListDet.Append;
                      quZListDetCASHNUM.AsInteger:=CommonSet.CashNum;
                      quZListDetZNUM.AsInteger:=Nums.ZNum;
                      quZListDetINOUT.AsInteger:=0; //������ - �������
                      quZListDetITYPE.AsInteger:=3; //��� ���� ��������
                      quZListDetRSUM.AsFloat:=rSum3;
                      quZListDetIDATE.AsInteger:=Trunc(Date);
                      quZListDetDDATE.AsDateTime:=dtNow;
                      quZListDet.Post;
                    end;

//                  StrWk:='����� ������� �� ���������  - '+StrWk;
                    if rSum4<>0 then
                    begin
                      quZListDet.Append;
                      quZListDetCASHNUM.AsInteger:=CommonSet.CashNum;
                      quZListDetZNUM.AsInteger:=Nums.ZNum;
                      quZListDetINOUT.AsInteger:=0; //������ - �������
                      quZListDetITYPE.AsInteger:=4; //��� ���� ��������
                      quZListDetRSUM.AsFloat:=rSum4;
                      quZListDetIDATE.AsInteger:=Trunc(Date);
                      quZListDetDDATE.AsDateTime:=dtNow;
                      quZListDet.Post;
                    end;
                  end;
                end;
                quZListDet.Active:=False;
              end;

//            if GetSumDisc(rSum1,rSum2,rSum3,rSum4) then  bNotErr:=True;
            end;

            Memo1.Lines.Add('  ���������� ��������'); Delay(10);
            ZClose;
            Memo1.Lines.Add('  ���������� ����������'); Delay(10);
            WriteNums;
            iZ:=Nums.ZNum;

            if CommonSet.TypeFis='sp101' then
            begin
              if CommonSet.RepCassir=1 then
              begin
                bNotErr:=True;
                if OpenNFDoc=false then  begin  TestFp('PrintNFStr'); OpenNFDoc; end;
                StrWk:='  ����� �� ��������  (Z)   ';
                if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
                StrWk:='----------------------------------------';
                if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

                StrWk:=FormatDateTime('dd.mm.yyyy hh:nn:ss ',now)+its(Nums.ZNum);
                if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

                StrWk:=' ';
                if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

                with dmC do
                begin
                  quCassirZList.Active:=False;
                  quCassirZList.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
                  quCassirZList.ParamByName('ZNUM').AsInteger:=Nums.ZNum;
                  quCassirZList.Active:=True;

                  quCassirZList.First;
                  while not quCassirZList.Eof do
                  begin
                    rSumNal:=0; rSumRet:=0; rSumBNal:=0; rSumBRet:=0;

                    quCassirZSum.Active:=False;
                    quCassirZSum.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
                    quCassirZSum.ParamByName('ZNUM').AsInteger:=Nums.ZNum;
                    quCassirZSum.ParamByName('CASSIR').AsInteger:=quCassirZListCASHER.AsInteger;
                    quCassirZSum.Active:=True;
                    quCassirZSum.First;
                    while not quCassirZSum.Eof do
                    begin
                      if quCassirZSumOPERATION.AsInteger=0 then rSumRet:=quCassirZSumRSUM.AsFloat;
                      if quCassirZSumOPERATION.AsInteger=1 then rSumNal:=quCassirZSumRSUM.AsFloat;
                      if quCassirZSumOPERATION.AsInteger=4 then rSumBRet:=quCassirZSumRSUM.AsFloat;
                      if quCassirZSumOPERATION.AsInteger=5 then rSumBNal:=quCassirZSumRSUM.AsFloat;

                      quCassirZSum.Next;
                    end;
                    quCassirZSum.Active:=False;

                    StrWk:='������ - '+quCassirZListNAME.AsString;
                    if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

                    Str(rSumNal:9:2,StrWk);
                    StrWk:='  ����� ������ ��������    - '+StrWk;
                    if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

                    Str(rSumBNal:9:2,StrWk);
                    StrWk:='  ����� ������ ����.������ - '+StrWk;
                    if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

                    Str(rSumRet:9:2,StrWk);
                    StrWk:='  ����� ��������� �������� - '+StrWk;
                    if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

                    Str(rSumBRet:9:2,StrWk);
                    StrWk:='  ����� �����. ����.������ - '+StrWk;
                    if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

                    StrWk:=' ';
                    if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

                    quCassirZList.Next;
                  end;

                  quCassirZList.Active:=False;
                 end;

                StrWk:='----------------------------------------';
                if bNotErr then PrintNFStr(StrWk) else TestFp('PrintNFStr');

                if CloseNFDoc=False then  begin   TestFp('PrintNFStr');  CloseNFDoc; end;
              end;

              bNotErr:=True;
              if OpenNFDoc=false then  begin  TestFp('PrintNFStr'); OpenNFDoc; end;
              StrWk:='  ������� �� ����������� �������  (Z)   ';
              if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
              StrWk:='----------------------------------------';
              if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

              StrWk:=FormatDateTime('dd.mm.yyyy hh:nn:ss ',now)+its(iZ);
              if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

              StrWk:=' ';
              if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

              Str(rSum1:9:2,StrWk);
              StrWk:='����� ������  �� ��������   - '+StrWk;
              if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

              Str(rSum2:9:2,StrWk);
              StrWk:='����� ������� �� ��������   - '+StrWk;
              if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

              Str(rSum3:9:2,StrWk);
              StrWk:='����� ������  �� ���������  - '+StrWk;
              if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

              Str(rSum3:9:2,StrWk);
              StrWk:='����� ������� �� ���������  - '+StrWk;
              if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

              StrWk:='----------------------------------------';
              if bNotErr then PrintNFStr(StrWk) else TestFp('PrintNFStr');

              if CloseNFDoc=False then  begin   TestFp('PrintNFStr');  CloseNFDoc; end;
            end;

            if CommonSet.DiscToPrice=1 then  //������ �������� � ���� -  ���������� ������ ��� - ���������
            begin
              with dmC do
              begin
                rSumDiscN:=0;
                rSumDiscBN:=0;
                rSumDiscNRet:=0;
                rSumDiscBNRet:=0;

                rSum1:=0; rSum2:=0; rSum3:=0; rSum4:=0;

                quDSumCh.Active:=False;
                quDSumCH.ParamByName('ZNUM').AsInteger:=Nums.ZNum;
                quDSumCH.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
                quDSumCH.Active:=True;
                quDSumCH.First;
                while not quDSumCH.Eof do
                begin
                  if quDSumChOPERATION.AsInteger=0 then rSumDiscNRet:=quDSumChDSUM.asfloat; //������� ���
                  if quDSumChOPERATION.AsInteger=1 then rSumDiscN:=quDSumChDSUM.asfloat; //������� ���
                  if quDSumChOPERATION.AsInteger=4 then rSumDiscBNRet:=quDSumChDSUM.asfloat; //������� ����
                  if quDSumChOPERATION.AsInteger=5 then rSumDiscBN:=quDSumChDSUM.asfloat; //������� ����

                  quDSumCH.Next;
                end;
                quDSumCH.Active:=False;
              end;

              rSum1:=rSumDiscN+rSumDiscBN;
              rSum3:=rSumDiscNRet+rSumDiscBNRet;

              bNotErr:=True;
              if OpenNFDoc=false then  begin  TestFp('PrintNFStr'); OpenNFDoc; end;
              StrWk:='  ������� �� ����������� �������  (X)   ';
              if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
              StrWk:='----------------------------------------';
              if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

              StrWk:=FormatDateTime('dd.mm.yyyy hh:nn:ss ',now)+its(Nums.ZNum);
              if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

              StrWk:=' ';
              if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

              Str(rSum1:9:2,StrWk);
              StrWk:='����� ������  �� ��������   - '+StrWk;
              if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

              Str(rSum2:9:2,StrWk);
              StrWk:='����� ������� �� ��������   - '+StrWk;
              if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

              Str(rSum3:9:2,StrWk);
              StrWk:='����� ������  �� ���������  - '+StrWk;
              if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

              Str(rSum4:9:2,StrWk);
              StrWk:='����� ������� �� ���������  - '+StrWk;
              if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

              StrWk:='----------------------------------------';
              if bNotErr then PrintNFStr(StrWk) else TestFp('PrintNFStr');

              if CloseNFDoc=False then  begin   TestFp('PrintNFStr');  CloseNFDoc; end;
            end;

            Nums.iRet:=InspectSt(Nums.sRet);

            Memo1.Lines.Add('  ���������� ���������'); Delay(10);
            CashDate(Nums.sDate);
            GetNums; //������  ���������� Z
            GetRes; //�������

            Memo1.Lines.Add('  '); Delay(10);
            Memo1.Lines.Add('  ����� ���� �������� ������...'); Delay(10);

            Recievedata(iZ,0);

            if CommonSet.TypeFis='sp101' then //�������� ��������
            begin
              Memo1.Lines.Add('  �������� ��������.'); Delay(10);
              try
                if FileExists(CurDir+'logo.bmp') then
                begin

                  if prLoadLogo then begin    Memo1.Lines.Add('  �������� �������� ��.'); Delay(10);  end
                  else begin Memo1.Lines.Add('  ������ �������� �������� (prLoadLogo) ...'); Delay(10); end;

                end else Memo1.Lines.Add('  ���� '+CurDir+'logo.bmp �� ������. �������� ����������.');
              except
                Memo1.Lines.Add('  ������ �������� �������� ...'); Delay(10);
              end;
            end;

            Memo1.Lines.Add('�������� ��.'); Delay(10);
            cxButton1.Enabled:=True;
          end;
        end;
      end else
      begin
//������ ������� ��� ���� ���������
        with dmC do
        with dmTr do
        begin
    //�������� ����� ��� ��������

          fmSt.Memo1.Clear;
          fmSt.Show;
          with fmSt do
          begin
            cxButton1.Enabled:=False;
            Memo1.Lines.Add('������ �������� ���� ... �����.'); Delay(10);

            rSumRet:=0;
            rSumNal:=0;
            rSumBRet:=0;
            rSumBNal:=0;

            iNumSm:=Nums.ZNum;

            iCountDoc:=Nums.iCheckNum;

            quSumTr.Active:=False;
            quSumTr.SelectSQL.Clear;
            quSumTr.SelectSQL.Add('SELECT Operation,Sum(TOTALRUB) as SumOp  FROM CASHSAIL');
            quSumTr.SelectSQL.Add('WHERE');
            quSumTr.SelectSQL.Add('SHOPINDEX=1');
            quSumTr.SelectSQL.Add('and CASHNUMBER='+IntToStr(CommonSet.CashNum));
            quSumTr.SelectSQL.Add('and ZNUMBER='+IntToStr(Nums.ZNum));
            quSumTr.SelectSQL.Add('Group by Operation');
            quSumTr.SelectSQL.Add('Order by Operation');
            quSumTr.Active:=True;

            quSumTr.First;
            while not quSumTr.Eof do
            begin
              Case quSumTrOPERATION.AsInteger of
              0:rSumRet:=quSumTrSUMOP.AsFloat;
              1:rSumNal:=quSumTrSUMOP.AsFloat;
              4:rSumBRet:=quSumTrSUMOP.AsFloat;
              5:rSumBNal:=quSumTrSUMOP.AsFloat;
              end;

              quSumTr.Next;
            end;
            quSumTr.Active:=False;

            rSumDiscN:=0;
            rSumDiscBN:=0;

            quDSum.Active:=False;
            quDSum.ParamByName('ZNUM').AsInteger:=Nums.ZNum;
            quDSum.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
            quDSum.Active:=True;
            quDSum.First;
            while not quDSum.Eof do
            begin
              if quDSumPAYMENT.AsInteger>0 then
              begin //������
                if quDSumOPER.AsInteger=1 then rSumDiscBN:=rSumDiscBN+quDSumDSUM.asfloat //�������
                else rSumDiscBN:=rSumDiscBN-quDSumDSUM.asfloat;
              end else //���
              begin
                if quDSumOPER.AsInteger=1 then rSumDiscN:=rSumDiscN+quDSumDSUM.asfloat //�������
                else rSumDiscN:=rSumDiscN-quDSumDSUM.asfloat;
              end;
              quDSum.Next;
            end;
            quDSum.Active:=False;

            rSumRetNI:=0;
            rSumSaleNI:=0;

            //��� ����������� ����

            quNarItog.Active:=False;
            quNarItog.ParamByName('ZNUM').AsInteger:=iNumSm;
            quNarItog.Active:=True;
            if quNarItog.RecordCount>0 then
            begin
              rSumSaleNI:=quNarItogSUMSALE.AsFloat;
              rSumRetNI:=quNarItogSUMRET.AsFloat;
            end;
            quNarItog.Active:=True;

            //���� ���� �� ����� ��� ������������ �����

            quZList.Active:=False;
            quZList.Active:=True;
            if quZList.Locate('IZ',iNumSm,[]) then
            begin
              quZList.Edit;
              quZListSUMSALE.AsFloat:=rSumNal+rSumBNal;
              quZListSUMRET.AsFloat:=rSumRet+rSumBRet;
              quZList.Post;
            end else
            begin
              quZList.Append;
              quZListIZ.AsInteger:=iNumSm;
              quZListSUMSALE.AsFloat:=rSumNal+rSumBNal;
              quZListSUMRET.AsFloat:=rSumRet+rSumBRet;
              quZList.Post;
            end;
            quZList.Active:=False;

 //

            prSetNFZ; //��������
            Nums.ZNum:=prGetNFZ;
            Nums.iCheckNum:=prGetNFCheckNum;

            //������� ��������
            if (Pos('COM',CommonSet.PortCash)>0)or(Pos('LPT',CommonSet.PortCash)>0) then
            begin
              try
                l:=40;
                prDevOpen(CommonSet.PortCash,0);
                StrP:=CommonSet.PortCash;
                prSetFont(StrP,10,0);
//              PrintStr(SpecVal.SPH1);
//              PrintStr(SpecVal.SPH2);
//              PrintStr(SpecVal.SPH3);
//              PrintStr(SpecVal.SPH4);
//              PrintStr(SpecVal.SPH5);

                PrintStr(CommonSet.DepartName);
                PrintStr(CommonSet.PreStr1);
                PrintStr(CommonSet.PreStr2);
                PrintStr('����� '+Its(CommonSet.CashNum));
                PrintStr(FormatDateTime('dd-mm-yyyy                         hh:nn',now));
                PrintStr(' ');
                StrWk:='            Z-����� N '+IntToStr(iNumSm);
                PrintStr(StrWk);
                PrintStr(' ');
                PrintStr('��������:');
                PrintStr(prDefFormatStr1(l,'�������',rSumNal));
                PrintStr(prDefFormatStr1(l,'������',rSumDiscN));
                PrintStr(prDefFormatStr1(l,'�����. �������',0));
                PrintStr(prDefFormatStr1(l,'�������',rSumRet));
                PrintStr(' ');
                PrintStr('������:');
                PrintStr(prDefFormatStr1(l,'�������',0));
                PrintStr(prDefFormatStr1(l,'�����. �������',0));
                PrintStr(' ');
                PrintStr('����. �����:');
                PrintStr(prDefFormatStr1(l,'�������',rSumBNal));
                PrintStr(prDefFormatStr1(l,'������',rSumDiscBN));
                PrintStr(prDefFormatStr1(l,'�����. �������',rSumBRet));
                PrintStr(' ');
                PrintStr('================= ����� ================');
                PrintStr(prDefFormatStr1(l,'�������',rSumNal+rSumBNal));
                PrintStr(prDefFormatStr1(l,' ������',rSumDiscN+rSumDiscBN));
                PrintStr(prDefFormatStr1(l,'�������',rSumRet+rSumBRet));
                PrintStr(' ');
                PrintStr(prDefFormatStr2(l,'��������� ����������:',iCountDoc+4));
                PrintStr(prDefFormatStr2(l,'� �.�. �����:',iCountDoc));
                PrintStr(prDefFormatStr2(l,'� �.�. �����. �����:',0));
                PrintStr(prDefFormatStr2(l,'� �.�. ������. ���. ���. �����:',0));
                PrintStr(' ');
                PrintStr(prDefFormatStr2(l,'�������:',iCountDoc));
                PrintStr(prDefFormatStr2(l,'�����. �������:',0));
                PrintStr(prDefFormatStr2(l,'�������:',0));
                PrintStr(prDefFormatStr2(l,'������������:',0));
                PrintStr(prDefFormatStr2(l,'����������:',0));
                PrintStr(' ');
                PrintStr(prDefFormatStr1(l,'������. �������:',rSumSaleNI+rSumNal+rSumBNal));
                PrintStr(prDefFormatStr1(l,'������. ��������:',rSumRetNI+rSumRet+rSumBRet));
                PrintStr(' ');

                prCutDoc(CommonSet.PortCash,0);
              finally
                prDevClose(CommonSet.PortCash,0);
              end;
            //���� ��� ����� � ������������ ������
            end;

            quZListDet.Active:=False;
            quZListDet.ParamByName('CNUM').AsInteger:=CommonSet.CashNum;
            quZListDet.ParamByName('ZNUM').AsInteger:=Nums.ZNum;
            quZListDet.Active:=True;
            quZListDet.First;
            while not quZListDet.Eof do quZListDet.Delete;

            if rSumNal<>0 then
            begin
              quZListDet.Append;
              quZListDetCASHNUM.AsInteger:=CommonSet.CashNum;
              quZListDetZNUM.AsInteger:=iNumSm;
              quZListDetINOUT.AsInteger:=1; //�������
              quZListDetITYPE.AsInteger:=1; //
              quZListDetRSUM.AsFloat:=rSumNal;
              quZListDetIDATE.AsInteger:=Trunc(Date);
              quZListDetDDATE.AsDateTime:=dtNow;
              quZListDet.Post;
            end;

            if rSumBNal<>0 then
            begin
              quZListDet.Append;
              quZListDetCASHNUM.AsInteger:=CommonSet.CashNum;
              quZListDetZNUM.AsInteger:=iNumSm;
              quZListDetINOUT.AsInteger:=1; //�������
              quZListDetITYPE.AsInteger:=2; //������
              quZListDetRSUM.AsFloat:=rSumBNal;
              quZListDetIDATE.AsInteger:=Trunc(Date);
              quZListDetDDATE.AsDateTime:=dtNow;
              quZListDet.Post;
            end;

            if rSumRet<>0 then
            begin
              quZListDet.Append;
              quZListDetCASHNUM.AsInteger:=CommonSet.CashNum;
              quZListDetZNUM.AsInteger:=iNumSm;
              quZListDetINOUT.AsInteger:=-1; //�������
              quZListDetITYPE.AsInteger:=1; //
              quZListDetRSUM.AsFloat:=rSumRet;
              quZListDetIDATE.AsInteger:=Trunc(Date);
              quZListDetDDATE.AsDateTime:=dtNow;
              quZListDet.Post;
            end;

            if rSumBRet<>0 then
            begin
              quZListDet.Append;
              quZListDetCASHNUM.AsInteger:=CommonSet.CashNum;
              quZListDetZNUM.AsInteger:=iNumSm;
              quZListDetINOUT.AsInteger:=-1; //�������
              quZListDetITYPE.AsInteger:=2; //������
              quZListDetRSUM.AsFloat:=rSumBRet;
              quZListDetIDATE.AsInteger:=Trunc(Date);
              quZListDetDDATE.AsDateTime:=dtNow;
              quZListDet.Post;
            end;

            if rSumDiscN<>0 then
            begin
              quZListDet.Append;
              quZListDetCASHNUM.AsInteger:=CommonSet.CashNum;
              quZListDetZNUM.AsInteger:=iNumSm;
              quZListDetINOUT.AsInteger:=0; //������
              quZListDetITYPE.AsInteger:=1; //���
              quZListDetRSUM.AsFloat:=rSumDiscN;
              quZListDetIDATE.AsInteger:=Trunc(Date);
              quZListDetDDATE.AsDateTime:=dtNow;
              quZListDet.Post;
            end;

            if rSumDiscBN<>0 then
            begin
              quZListDet.Append;
              quZListDetCASHNUM.AsInteger:=CommonSet.CashNum;
              quZListDetZNUM.AsInteger:=iNumSm;
              quZListDetINOUT.AsInteger:=0; //������
              quZListDetITYPE.AsInteger:=2; //���
              quZListDetRSUM.AsFloat:=rSumDiscBN;
              quZListDetIDATE.AsInteger:=Trunc(Date);
              quZListDetDDATE.AsDateTime:=dtNow;
              quZListDet.Post;
            end;

            quZListDet.Active:=False;

            Memo1.Lines.Add('  '); Delay(10);
            Memo1.Lines.Add('  ����� ���� �������� ������...'); Delay(10);

            Recievedata(iNumSm,0);

            Memo1.Lines.Add('�������� ��.'); Delay(10);
            cxButton1.Enabled:=True;
          end;
        end;
      end;

      CountSec:=0; fmMainCasher.Timer2.Enabled:=True;

      //����� ����������� �������
      if CommonSet.ClearPasswPersDay=1 then dmC.quClearPasswPers.ExecQuery;

      WriteStatus;
    end;
{    end
    else
    begin
      Showmessage('����� ��� �������.');
      Memo2.Clear;
      Memo2.Lines.Add('����� ��� �������.');
    end;}
  end;
end;

procedure TfmMainCasher.acCashRepExecute(Sender: TObject);
begin
  if not CanDo('prCashRep') then exit;

  prWriteLog('!!CashReps;');

  if Panel5.Visible then
  begin
    Panel5.Visible:=False;
    TextEdit3.SetFocus;
    CountSec:=0;
    if not Check.ChBeg then Timer2.Enabled:=True;
  end
  else
  begin
    CountSec:=0;
    Timer2.Enabled:=False;

    Panel5.Visible:=True;
    Panel5.SetFocus;
    cxButton2.SetFocus;
  end;
end;

procedure TfmMainCasher.acCopyCheckExecute(Sender: TObject);
Var sC:String;
    iNumZ,iNumCh,iPos:Integer;
    s1,s2:String;
    CHECKDATE:TDateTime;
    CASHER:INTEGER;
    CHECKSUM:Real;
    CHECKPAY:Real;
    CHECKDSUM:Real;
    OPERATION:Integer;
    StrWk,StrWk1:String;
    bNotErr:Boolean;
    sCasher:String;
begin
  //  ShowMessage('Copy');
  //  exit;
  //  ����� ����
  if bBegCash then exit; //���� ������� ������, �� ������ ������ ������ (����� �������� ��������� ���������� ����)
  if not CanDo('prCopyCheck') then begin fmMainCasher.Memo2.Clear; fmMainCasher.Memo2.Lines.Add('��� ���� �� ��������.');  exit;end;
  if Check.ChBeg then
  begin
    ShowMessage('��������� ������� �������� ���.');
    Memo2.Clear;
    Memo2.Lines.Add('��������� ������� �������� ���.');
    exit;
  end;

  //�������� ������� �����

  Nums.iRet:=InspectSt(Nums.sRet);
  if Nums.iRet<>0 then
    if TestStatus('InspectSt',sMessage)=False then
    begin
  //          ShowMessage('������: "'+Nums.sRet+'". ��������� �������� ����� ���������� ������.');
      WriteStatus;
      fmAttention.Label1.Caption:=sMessage;
      fmAttention.ShowModal;
      exit;// ������ ��� - ������� ��� ������������ ����
    end;

  CountSec:=0; fmMainCasher.Timer2.Enabled:=False;

  sC:=TextEdit3.Text;
  if sC='' then
  begin
    iNumZ:=Nums.ZNum;
    iNumCh:=Nums.iCheckNum;
  end
  else
  begin
    iPos:=0;
    if iPos=0 then iPos:=pos('.',sc);
    if iPos=0 then iPos:=pos(',',sc);
    if iPos>0  then
    begin //�� ������
      s1:=Copy(sc,1,iPos-1);
      s2:=Copy(sc,iPos+1,length(sc)-iPos);
      iNumZ:=StrToIntDef(s1,0);
      iNumCh:=StrToIntDef(s2,0);
    end
    else //������ ����� ����
    begin
      iNumZ:=Nums.ZNum;
      iNumCh:=StrToIntDef(sC,0);
    end;
  end;
  FormLog('CopyCheck','CashNum '+INtToStr(CommonSet.CashNum)+' CashZ '+INtToStr(iNumZ)+' CheckNum '+INtToStr(iNumCh));
  prWriteLog('!!CopyCheck; CashNum '+INtToStr(CommonSet.CashNum)+' CashZ '+INtToStr(iNumZ)+' CheckNum '+INtToStr(iNumCh));
  Memo2.Clear;
  Memo2.Lines.Add('������ ����� ���� (�����:'+INtToStr(iNumZ)+', ���:'+INtToStr(iNumCh)+').');

  if (iNumZ>=0)and(iNumCh>0) then
  begin
    with dmC do
    begin
      prReadCheck.ParamByName('SHOPINDEX').AsInteger:=CommonSet.ShopIndex;
      prReadCheck.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
      prReadCheck.ParamByName('CASHZ').AsInteger:=iNumZ;
      prReadCheck.ParamByName('CHECKNUM').AsInteger:=iNumCh;

      prReadCheck.ExecProc;

      CHECKDATE:=prReadCheck.ParamByName('CHECKDATE').AsDateTime;
      CASHER:=prReadCheck.ParamByName('CASHER').AsInteger;
      CHECKSUM:=prReadCheck.ParamByName('CHECKSUM').AsFloat;
      CHECKPAY:=prReadCheck.ParamByName('CHECKPAY').AsFloat;
      CHECKDSUM:=prReadCheck.ParamByName('CHECKDSUM').AsFloat;
      OPERATION:=prReadCheck.ParamByName('OPERATION').AsInteger;

      quPersName.Active:=False;
      quPersName.ParamByName('ID').AsInteger:=CASHER;
      quPersName.Active:=True;
      quPersName.First;
      if not quPersName.Eof then
      begin
        sCasher:=quPersNameNAME.AsString;
        if Pos('������',sCasher)>0 then delete(sCasher,Pos('������',sCasher),6);
        While Pos(' ',sCasher)>0 do delete(sCasher,Pos(' ',sCasher),1);
      end else
      begin
        sCasher:=IntToStr(CASHER);
      end;

      quPersName.Active:=False;

      taCheck.Active:=False;
      taCheck.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
      taCheck.Active:=True;
      taCheck.First;
      while not taCheck.Eof do
      begin

        if fmMainCasher.tag=0 then
          if fmMainCasher.GridCh.Height<329 then fmMainCasher.GridCh.Height:=fmMainCasher.GridCh.Height+18;
        if (fmMainCasher.tag=1)or(fmMainCasher.tag=17) then
          if fmMainCasher.GridCh.Height<280 then fmMainCasher.GridCh.Height:=fmMainCasher.GridCh.Height+18;

        taCheck.Next;
      end;
      delay(50);

      //������ ������������� ���������

      bNotErr:=True;
      if OpenNFDoc=false then
      begin
        TestFp('PrintNFStr');
        OpenNFDoc;
      end;

      SelectF(10);
      if bNotErr then bNotErr:=PrintNFStr('����� ���� �'+IntToStr(iNumCh)) else TestFp('PrintNFStr');
      StrWk:=CommonSet.DepartName;
      while length(strwk)<35 do StrWk:=' '+StrWk;
      if bNotErr then bNotErr:=PrintNFStr('�����'+StrWk)else TestFp('PrintNFStr');
      if bNotErr then bNotErr:=PrintNFStr('      ')else TestFp('PrintNFStr');
      if bNotErr then bNotErr:=PrintNFStr('����� � '+INtToStr(CommonSet.CashNum)+'  ���.� '+Nums.SerNum) else TestFp('PrintNFStr');
//      if bNotErr then bNotErr:=PrintNFStr('�����: '+INtToStr(iNumZ)+'            ������: '+IntToStr(CASHER));
      if bNotErr then bNotErr:=PrintNFStr('�����: '+INtToStr(iNumZ)+'            ������: '+sCasher) else TestFp('PrintNFStr');
      if bNotErr then bNotErr:=PrintNFStr('����:   '+FormatDateTime('dd.mm.yyyy hh:nn',CHECKDATE)) else TestFp('PrintNFStr');

      SelectF(3);
      if CommonSet.TypeFis='prim' then
      begin
        StrWk:='                                                       ';
        if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
        SelectF(10);
        StrWk:=' ��������          ���-��   ����   ����� ';
        if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
        SelectF(3);
        StrWk:='                                                       ';
        if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
      end;

      if (CommonSet.TypeFis='sp101')or(CommonSet.TypeFis='shtrih')or(CommonSet.TypeFis='pirit') then
      begin
        StrWk:='----------------------------------------';
        if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
        StrWk:=' ��������          ���-��   ����   ����� ';
        if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
        StrWk:='----------------------------------------';
        if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
      end;

      SelectF(15);
      StrWk:='';
      if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

      Case Operation of
      0: StrWk:='  �������� - ������� ��������         ';
      1: StrWk:='  �������� - ������� ��������         ';
      5: begin
           StrWk:='  �������� - ������� ��               '; if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
           StrWk:='  ' ;                                    if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
           StrWk:='  ' ;                                    if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
           StrWk:='   ___________________________ '       ; if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
           StrWk:='         ������� �������   '           ; if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
           StrWk:=' '                                     ; if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
           StrWk:=' '                                     ; if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
           StrWk:='  ____________________________'        ; if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
           StrWk:='     ����.�������(����������)'         ; if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
           StrWk:=' ';
         end;
      4: begin
           StrWk:='  �������� - ������� ��               '; if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
           StrWk:='  ' ;                                    if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
           StrWk:='  ' ;                                    if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
           StrWk:='   ___________________________ '       ; if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
           StrWk:='         ������� �������   '           ; if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
           StrWk:=' '                                     ; if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
           StrWk:=' '                                     ; if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
           StrWk:='  ____________________________'        ; if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
           StrWk:='     ����.�������(����������)'         ; if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
           StrWk:=' ';
         end;
      end;

      SelectF(15);
      if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

      SelectF(3);
      StrWk:='                                                     ';
      if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

// ���� ������ ���� ������� ��� �������� ����� ������� � ��� �����������
      if Operation in [4,5] then
      begin
        quBntr.Active:=False;
        quBnTr.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
        quBnTr.ParamByName('ZNUM').AsInteger:=iNumZ;
        quBnTr.ParamByName('CHECKNUM').AsInteger:=iNumCh;
        quBnTr.Active:=True;
        if quBnTr.RecordCount>0 then
        begin
          StrWk:='�������� '+ quBnTrBNBAR.AsString;
          if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
          Str(quBnTrBNSUM.AsFloat:9:2,StrWk);
          StrWk:='�����: '+StrWk+' �.';
          if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
          StrWk:='��� �����������: '+quBnTrAUTHCODE.AsString;
          if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
        end;
        quBntr.Active:=False;
      end;

      taCheck.First;
      while not taCheck.Eof do
      begin
        StrWk:= Copy((taCheckARTICUL.AsString+' '+taCheckName.AsString),1,40);
//        while Length(StrWk)<20 do StrWk:=StrWk+' '; //21
        SelectF(5);
        if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

        Str(taCheckPrice.AsFloat:7:2,StrWk1); StrWk1:=DelSp(StrWk1);
        StrWk:=StrWk1; //

        Str(taCheckQUANT.AsFloat:6:3,StrWk1); StrWk1:=DelSp(StrWk1);
        StrWk:=StrWk+' X '+StrWk1; //

        if taCheckDSUM.AsFloat>0.01 then
        begin
          Str(taCheckDPROC.AsFloat:2:0,StrWk1); StrWk1:=DelSp(StrWk1);
          StrWk:=StrWk+'  ������ '+StrWk1+'%'; //

          Str(taCheckDSUM.AsFloat:7:2,StrWk1); StrWk1:=DelSp(StrWk1);
          StrWk:=StrWk+' '+StrWk1; //
        end; 

        while Length(StrWk)<30 do StrWk:=StrWk+' ';

//        Str(taCheckRSUM.AsFloat:9:2,StrWk1);
        Str((taCheckPrice.AsFloat*taCheckQUANT.AsFloat):9:2,StrWk1);

        StrWk:=StrWk+' '+StrWk1+'�'; //
        SelectF(10);
        if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
        taCheck.Next;
      end;

      SelectF(3);
      StrWk:='                                                     ';
      if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

      SelectF(10);
      Str((CHECKSUM+CHECKDSUM):8:2,StrWk1);  //CHECKSUM - ��� ��� ����� ������
      StrWk:=' �����                      '+StrWk1+' ���';
      if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

      if CHECKDSUM<>0 then
      begin
        SelectF(10);
        Str(CHECKDSUM:5:2,StrWk1);
        StrWk:='C����� - '+StrWk1+'���';
        if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

        SelectF(10);
//        Str((CHECKSUM-CHECKDSUM):8:2,StrWk1);
        Str(CHECKSUM:8:2,StrWk1); //CHECKSUM - ��� ��� ����� ������
        StrWk:=' �����                      '+StrWk1+' ���';
        if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
      end;

      SelectF(10);
      Str(CHECKPAY:8:2,StrWk1);
      StrWk:=' �������� �� ����������     '+StrWk1+' ���';
      if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

      SelectF(10);
      Str((CHECKPAY-CHECKSUM):8:2,StrWk1);   //CHECKSUM - ��� ��� ����� ������
      StrWk:=' �����                      '+StrWk1+' ���';
      if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

      StrWk:='';
      PrintNFStr(StrWk);
      StrWk:=' ��������!  ������������ ��������.';
      if bNotErr then PrintNFStr(StrWk) else TestFp('PrintNFStr');

      if CloseNFDoc=False then
      begin
        TestFp('PrintNFStr');
        CloseNFDoc;
      end;

      if (CommonSet.TypeFis='prim')or(CommonSet.TypeFis='shtrih') then
      begin
        PrintNFStr(' ');
        PrintNFStr(' ');
        PrintNFStr(' ');
        PrintNFStr(' ');
        PrintNFStr(' ');
        PrintNFStr(' ');
        CutDoc;
      end;

      delay(33);

      prClearCheck.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
      prClearCheck.ExecProc;

      taCheck.Active:=False;
      taCheck.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
      taCheck.Active:=True;

      ViewChDPROC.Visible:=False;
      ViewChDSUM.Visible:=False;

      Label17.Caption:='...';
      Label18.Caption:='';
      Label18.Visible:=False;
      Label19.Caption:='';
      Label19.Visible:=False;
      Label24.Caption:='';
      Label24.Visible:=False;

      ClearPos;
      ClearSelPos;
      ResetPos;

      ClearCheck;
      CalcSum;
    end;
  end;

  CountSec:=0; fmMainCasher.Timer2.Enabled:=True;
  TextEdit3.Text:='';
end;

procedure TfmMainCasher.acCalcExecute(Sender: TObject);
begin
  if bBegCash then exit; //���� ������� ������, �� ����� ������������ ������ (����� �������� ��������� ���������� ����)
  prWriteLog('!!Calc;');
  fmCalcul.ShowModal;
end;

procedure TfmMainCasher.acPrintTCHExecute(Sender: TObject);
Var sC:String;
    iNumZ,iNumCh,iPos:Integer;
    s1,s2:String;
    CHECKDATE:TDateTime;
    CASHER:INTEGER;
    CHECKSUM:Real;
    CHECKDSUM:Real;
    OPERATION:Integer;
    StrWk,StrWk1:String;
    bNotErr:Boolean;
begin
  //�������� ���

  if not CanDo('prCopyCheck') then begin fmMainCasher.Memo2.Clear; fmMainCasher.Memo2.Lines.Add('��� ���� �� ��������.');  exit;end;
  if Check.ChBeg then
  begin
    ShowMessage('��������� ������� �������� ���.');
    Memo2.Clear;
    Memo2.Lines.Add('��������� ������� �������� ���.');
    exit;
  end;
        //�������� ������� �����
  Nums.iRet:=InspectSt(Nums.sRet);
  if Nums.iRet<>0 then
    if TestStatus('InspectSt',sMessage)=False then
    begin
  //          ShowMessage('������: "'+Nums.sRet+'". ��������� �������� ����� ���������� ������.');
      WriteStatus;
      fmAttention.Label1.Caption:=sMessage;
      fmAttention.ShowModal;
      exit;// ������ ��� - ������� ��� ������������ ����
    end;

  CountSec:=0; fmMainCasher.Timer2.Enabled:=False;

  sC:=TextEdit3.Text;
  if sC='' then
  begin
    iNumZ:=Nums.ZNum;
    iNumCh:=Nums.iCheckNum;
  end
  else
  begin
    iPos:=0;
    if iPos=0 then iPos:=pos('.',sc);
    if iPos=0 then iPos:=pos(',',sc);
    if iPos>0  then
    begin //�� ������
      s1:=Copy(sc,1,iPos-1);
      s2:=Copy(sc,iPos+1,length(sc)-iPos);
      iNumZ:=StrToIntDef(s1,0);
      iNumCh:=StrToIntDef(s2,0);
    end
    else //������ ����� ����
    begin
      iNumZ:=Nums.ZNum;
      iNumCh:=StrToIntDef(sC,0);
    end;
  end;
//  FormLog('TovCheck','CashNum '+INtToStr(CommonSet.CashNum)+' CashZ '+INtToStr(iNumZ)+' CheckNum '+INtToStr(iNumCh));
  prWriteLog('!!TovCheck; CashNum '+INtToStr(CommonSet.CashNum)+' CashZ '+INtToStr(iNumZ)+' CheckNum '+INtToStr(iNumCh));
  Memo2.Clear;
  Memo2.Lines.Add('������ ��������� ���� (�����:'+INtToStr(iNumZ)+', ���:'+INtToStr(iNumCh)+').');

  if (iNumZ>0)and(iNumCh>0) then
  begin
    with dmC do
    begin
      prReadCheck.ParamByName('SHOPINDEX').AsInteger:=CommonSet.ShopIndex;
      prReadCheck.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
      prReadCheck.ParamByName('CASHZ').AsInteger:=iNumZ;
      prReadCheck.ParamByName('CHECKNUM').AsInteger:=iNumCh;

      prReadCheck.ExecProc;

      CHECKDATE:=prReadCheck.ParamByName('CHECKDATE').AsDateTime;
      CASHER:=prReadCheck.ParamByName('CASHER').AsInteger;
      CHECKSUM:=prReadCheck.ParamByName('CHECKSUM').AsFloat;
      CHECKDSUM:=prReadCheck.ParamByName('CHECKDSUM').AsFloat;
      OPERATION:=prReadCheck.ParamByName('OPERATION').AsInteger;

      taCheck.Active:=False;
      taCheck.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
      taCheck.Active:=True;
      taCheck.First;
      while not taCheck.Eof do
      begin
        if fmMainCasher.tag=0 then
          if fmMainCasher.GridCh.Height<329 then fmMainCasher.GridCh.Height:=fmMainCasher.GridCh.Height+18;
        if (fmMainCasher.tag=1)or(fmMainCasher.tag=17) then
          if fmMainCasher.GridCh.Height<280 then fmMainCasher.GridCh.Height:=fmMainCasher.GridCh.Height+18;

        taCheck.Next;
      end;

      //������ ������������� ���������

      bNotErr:=True;
      if OpenNFDoc=false then
      begin
        TestFp('PrintNFStr');
        OpenNFDoc;
      end;

      SelectF(10);
      if bNotErr then bNotErr:=PrintNFStr('�������� ��� �'+IntToStr(iNumCh)) else TestFp('PrintNFStr');
      StrWk:=CommonSet.DepartName;
      while length(strwk)<35 do StrWk:=' '+StrWk;
      if bNotErr then bNotErr:=PrintNFStr('�����'+StrWk) else TestFp('PrintNFStr');
      if bNotErr then bNotErr:=PrintNFStr('      ') else TestFp('PrintNFStr');
      if bNotErr then bNotErr:=PrintNFStr('�����: '+INtToStr(iNumZ)+'            ������: '+IntToStr(CASHER)) else TestFp('PrintNFStr');
      if bNotErr then bNotErr:=PrintNFStr('����:   '+FormatDateTime(sFormatDate,CHECKDATE)) else TestFp('PrintNFStr');

      if CommonSet.TypeFis='prim' then
      begin
        SelectF(3);
        StrWk:='                                                       ';
        if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
        SelectF(10);
        StrWk:=' ��������          ���-��   ����   ����� ';
        if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
        SelectF(3);
        StrWk:='                                                       ';
        if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
      end;

      if (CommonSet.TypeFis='sp101')or(CommonSet.TypeFis='shtrih') then
      begin
        StrWk:='----------------------------------------';
        if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
        StrWk:=' ��������         ���-��   ����   ����� ';
        if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
        StrWk:='----------------------------------------';
        if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
      end;

      SelectF(15);
      StrWk:='';
      if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

      Case Operation of
      0: StrWk:='        �������� - ������� ��������         ';
      1: StrWk:='        �������� - ������� ��������         ';
      2: StrWk:='        �������� - ������� ��               ';
      3: StrWk:='        �������� - ������� ��               ';
      end;

      if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');


      SelectF(3);
      StrWk:='                                                     ';
      if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
      taCheck.First;
      while not taCheck.Eof do
      begin
        StrWk:= Copy((taCheckARTICUL.AsString+' '+taCheckName.AsString),1,40);
//        while Length(StrWk)<20 do StrWk:=StrWk+' '; //21
        SelectF(5);
        if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

        Str(taCheckPrice.AsFloat:7:2,StrWk1); StrWk1:=DelSp(StrWk1);
        StrWk:=StrWk1; //

        Str(taCheckQUANT.AsFloat:6:3,StrWk1); StrWk1:=DelSp(StrWk1);
        StrWk:=StrWk+' X '+StrWk1; //


        while Length(StrWk)<30 do StrWk:=StrWk+' ';

//        Str(taCheckRSUM.AsFloat:9:2,StrWk1);
        Str((taCheckPrice.AsFloat*taCheckQUANT.AsFloat):9:2,StrWk1);
        StrWk:=StrWk+' '+StrWk1+'�'; //
        SelectF(10);
        if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
        taCheck.Next;
      end;

      SelectF(3);
      StrWk:='                                                       ';
      if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

      SelectF(10);
      Str((CHECKSUM+CHECKDSUM):8:2,StrWk1);   //CHECKSUM - ��� ��� ����� ������
      StrWk:=' �����                      '+StrWk1+' ���';
      if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

      if CHECKDSUM<>0 then
      begin
        SelectF(10);
        Str(CHECKDSUM:5:2,StrWk1);
        StrWk:='C����� - '+StrWk1+'���';
        if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

        SelectF(10);
        Str(CHECKSUM:8:2,StrWk1);   //CHECKSUM - ��� ��� ����� ������
        StrWk:=' �����                      '+StrWk1+' ���';
        if bNotErr then PrintNFStr(StrWk) else TestFp('PrintNFStr');
      end;

      if CloseNFDoc=False then
      begin
        TestFp('PrintNFStr');
        CloseNFDoc;
      end;

      if (CommonSet.TypeFis='prim')or(CommonSet.TypeFis='shtrih') then
      begin
        PrintNFStr(' ');
        PrintNFStr(' ');
        PrintNFStr(' ');
        PrintNFStr(' ');
        PrintNFStr(' ');
        PrintNFStr(' ');
        CutDoc;
      end;

      prClearCheck.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
      prClearCheck.ExecProc;

      taCheck.Active:=False;
      taCheck.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
      taCheck.Active:=True;

      ViewChDPROC.Visible:=False;
      ViewChDSUM.Visible:=False;

      Label17.Caption:='...';
      Label18.Caption:='';
      Label18.Visible:=False;
      Label19.Caption:='';
      Label19.Visible:=False;
      Label24.Caption:='';
      Label24.Visible:=False;

      ClearPos;
      ClearSelPos;
      ResetPos;

      ClearCheck;
      CalcSum;
    end;
  end;

  CountSec:=0; fmMainCasher.Timer2.Enabled:=True;
  TextEdit3.Text:='';
end;

procedure TfmMainCasher.cxButton4Click(Sender: TObject);
Var StrWk:String;
    rSum,rSumR:Real;
begin
  with dmC do
  begin
    prWriteLog('!!OutPutMoney;');

    if Check.ChBeg then
    begin
      Memo2.Clear;
      Memo2.Lines.Add('��������� ������� �������� ���.');
      ShowMessage('��������� ������� �������� ���.');
      exit;
    end;

    CountSec:=0; fmMainCasher.Timer2.Enabled:=False;

    fmInOutM.taInOutM.Active:=False;
    fmInOutM.taInOutM.CreateDataSet;
    iSumInOut:=0;

    taMoneyType.Active:=False;
    taMOneyType.Active:=True;

    taMoneyType.First;
    while not taMoneyType.Eof do
    begin
      with fmInOutM do
      begin

        taInOutM.Append;
        taInOutMIVal.AsInteger:=taMoneyTypeIVAL.AsInteger;
        taInOutMrVal.AsFloat:=taMoneyTypeIVAL.AsInteger/100;
        taInOutMiQuant.AsInteger:=0;
        taInOutM.Post;

      end;
      taMoneyType.Next;
    end;
    iLast:=taMoneyTypeIVAL.AsInteger;

    taMoneyType.Active:=False;

    //��������� ���� �����
    quMoneyLast.Active:=False;
    quMoneyLast.Active:=True;

    GetCashReg(rSumR);
    if rSumR<0 then
    begin

      Str(quMoneyLastENDSUM.AsFloat:12:2,StrWk);
      fmInOutM.Label6.Caption:=StrWk;

      quCashSum.Active:=False;
      quCashSum.ParamByName('DateB').AsString:=FormatDateTime('dd.mm.yyyy  hh:mm:ss',quMoneyLastINOUTDATE.AsDateTime);
      quCashSum.ParamByName('DateE').AsString:=FormatDateTime('dd.mm.yyyy  hh:mm:ss',now);
      quCashSum.Active:=True;

      Str(quCashSumCASHSUM.AsFloat:12:2,StrWk);
      fmInOutM.Label7.Caption:=StrWk;

      Str((quMoneyLastENDSUM.AsFloat+quCashSumCASHSUM.AsFloat):12:2,StrWk);
      fmInOutM.Label8.Caption:=StrWk;
    end else
    begin
      Str(0,StrWk);
      fmInOutM.Label6.Caption:=StrWk;

      Str(rSumR:12:2,StrWk);
      fmInOutM.Label7.Caption:=StrWk;

      Str(rSumR:12:2,StrWk);
      fmInOutM.Label8.Caption:=StrWk;
    end;

    fmInOutM.Caption:='������� ����� �� �����.';

    fmInOutM.taInOutM.First;

    bMoneyCalc:=True; //�������� ������������� ����� ��������� �����

    fmInOutM.ShowModal; // bMoneyCalc:=False - ��� ��������
    if fmInOutM.ModalResult=mrOk then
    begin
      //���� � ���� ������� �����
      rSum:=0;
      with fmInOutM do
      begin
        taInOutM.First;
        while not taInOutM.Eof do
        begin
          rSum:=rSum+taInOutMIVal.AsInteger*taInOutMiQuant.AsInteger/100;
          taInOutM.Next;
        end;

        rSum:=rSum+CurrencyEdit1.EditValue;
      end;
      if rSum<>0 then
      begin
        taMoneySum.Active:=False;
        taMoneySum.Active:=True;

        taMoneySum.Append;
        taMoneySumID.AsInteger:=quMoneyLastID.AsInteger+1;
        taMoneySumINOUTDATE.AsDateTime:=Now;
        taMoneySumBEGSUM.AsFloat:=quMoneyLastENDSUM.AsFloat+quCashSumCASHSUM.AsFloat;
        taMoneySumINOUTSUM.AsFloat:=(-1)*rSum;
        taMoneySumENDSUM.AsFloat:=quMoneyLastENDSUM.AsFloat+quCashSumCASHSUM.AsFloat-rSum;
        taMoneySumPERSONID.AsInteger:=Person.Id;
        taMoneySum.Post;

        taMoneySum.Active:=False;

        //������� ���������
        if InCass((-1)*rSum)=False then TestFp('������� �����.')
      end;

      //��� ���� ��������
      PrintF(0,rSum,rSumR); //0-�������
    end;
    quMoneyLast.Active:=False;
    quCashSum.Active:=False;
    fmInOutM.taInOutM.Active:=False;
    CountSec:=0; fmMainCasher.Timer2.Enabled:=True;
  end;
end;

procedure TfmMainCasher.FormShow(Sender: TObject);
begin
  Nums.bOpen:=False;
  NumPacket:=33;
  bErrCash:=False;
  try
    if CommonSet.NoFis=0 then //���������� �����
    begin
      if pos('COM',CommonSet.PortCash)>0 then
      begin
        if CashOpen(PChar(CommonSet.PortCash)) then   //
        begin
          Delay(100);
          Nums.iRet:=InspectSt(Nums.sRet);
          if Nums.iRet<>0 then ShowMessage(Nums.sRet);

          GetSerial;
          Delay(100);
          CashDate(Nums.sDate);
          Delay(100);
          GetNums; //������ ����������  Create
          GetRes; //�������
          fGetFN; //������ �� ����������� ����������

        end else
        begin
          bErrCash:=True;
        end;
      end;
    end;
  except
  end;
  WriteStatus;

  TimerStart.Enabled:=True;
// �������� ���� �� �������
end;

procedure TfmMainCasher.RxClock1Click(Sender: TObject);
begin
//
  acExitProgram.Execute;
end;

procedure TfmMainCasher.cxButton5Click(Sender: TObject);
Var StrWk:String;
    rSum,rSumR:Real;
begin
  with dmC do
  begin
    prWriteLog('!!InPutMoney;');

    if Check.ChBeg then
    begin
      Memo2.Clear;
      Memo2.Lines.Add('��������� ������� �������� ���.');
      ShowMessage('��������� ������� �������� ���.');
      exit;
    end;

    CountSec:=0; fmMainCasher.Timer2.Enabled:=False;

    fmInOutM.taInOutM.Active:=False;
    fmInOutM.taInOutM.CreateDataSet;
    iSumInOut:=0;

    taMoneyType.Active:=False;
    taMOneyType.Active:=True;

    taMoneyType.First;
    while not taMoneyType.Eof do
    begin
      with fmInOutM do
      begin

        taInOutM.Append;
        taInOutMIVal.AsInteger:=taMoneyTypeIVAL.AsInteger;
        taInOutMrVal.AsFloat:=taMoneyTypeIVAL.AsInteger/100;
        taInOutMiQuant.AsInteger:=0;
        taInOutM.Post;

      end;
      taMoneyType.Next;
    end;
    iLast:=taMoneyTypeIVAL.AsInteger;

    taMoneyType.Active:=False;

    quMoneyLast.Active:=False;
    quMoneyLast.Active:=True;

    //��������� ���� �����
    GetCashReg(rSumR);
    if rSumR<0 then
    begin

      Str(quMoneyLastENDSUM.AsFloat:12:2,StrWk);
      fmInOutM.Label6.Caption:=StrWk;

      quCashSum.Active:=False;
      quCashSum.ParamByName('DateB').AsString:=FormatDateTime('dd.mm.yyyy  hh:mm:ss',quMoneyLastINOUTDATE.AsDateTime);
      quCashSum.ParamByName('DateE').AsString:=FormatDateTime('dd.mm.yyyy  hh:mm:ss',now);
      quCashSum.Active:=True;

      Str(quCashSumCASHSUM.AsFloat:12:2,StrWk);
      fmInOutM.Label7.Caption:=StrWk;

      Str((quMoneyLastENDSUM.AsFloat+quCashSumCASHSUM.AsFloat):12:2,StrWk);
      fmInOutM.Label8.Caption:=StrWk;
    end else
    begin
      Str(0,StrWk);
      fmInOutM.Label6.Caption:=StrWk;

      Str(rSumR:12:2,StrWk);
      fmInOutM.Label7.Caption:=StrWk;

      Str(rSumR:12:2,StrWk);
      fmInOutM.Label8.Caption:=StrWk;
    end;

    fmInOutM.Caption:='�������� ����� � �����.';

    fmInOutM.taInOutM.First;

    bMoneyCalc:=True; //�������� ������������� ����� ��������� �����

    fmInOutM.ShowModal; // bMoneyCalc:=False - ��� ��������
    if fmInOutM.ModalResult=mrOk then
    begin
      //���� � ���� �������� �����
      rSum:=0;
      with fmInOutM do
      begin
        taInOutM.First;
        while not taInOutM.Eof do
        begin
          rSum:=rSum+taInOutMIVal.AsInteger*taInOutMiQuant.AsInteger/100;
          taInOutM.Next;
        end;
        rSum:=rSum+CurrencyEdit1.EditValue;
      end;
      if rSum<>0 then
      begin
        taMoneySum.Active:=False;
        taMoneySum.Active:=True;

        taMoneySum.Append;
        taMoneySumID.AsInteger:=quMoneyLastID.AsInteger+1;
        taMoneySumINOUTDATE.AsDateTime:=Now;
        taMoneySumBEGSUM.AsFloat:=quMoneyLastENDSUM.AsFloat+quCashSumCASHSUM.AsFloat;
        taMoneySumINOUTSUM.AsFloat:=rSum;
        taMoneySumENDSUM.AsFloat:=quMoneyLastENDSUM.AsFloat+quCashSumCASHSUM.AsFloat+rSum;
        taMoneySumPERSONID.AsInteger:=Person.Id;
        taMoneySum.Post;

        taMoneySum.Active:=False;

        //������ ���������
//        InCass(rSum);
        if InCass(rSum)=False then TestFp('�������� �����.');

      end;
      //��� ���� ��������

      PrintF(1,rSum,rSumR); //0-�������
    end;
    quMoneyLast.Active:=False;
    quCashSum.Active:=False;
    fmInOutM.taInOutM.Active:=False;

    CountSec:=0; fmMainCasher.Timer2.Enabled:=True;
  end;
end;

procedure TfmMainCasher.acExitProgramExecute(Sender: TObject);
begin
  if bBegCash then exit; //���� ������� ������, �� ������ (����� �������� ��������� ���������� ����)

  if not CanDo('prExitProgram') then begin fmMainCasher.Memo2.Clear; fmMainCasher.Memo2.Lines.Add('��� ���� �� ��������.');  exit;end;

  if Check.ChBeg then
  begin
    ShowMessage('��������� ������� �������� ���.');
    Memo2.Clear;
    Memo2.Lines.Add('��������� ������� �������� ���.');
    exit;
  end;

  if MessageDlg('�� ������������� ������ ����� �� ���������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
    FormLog('ExitProgram','CashNum '+INtToStr(CommonSet.CashNum)+' '+Person.Name);

    TimerStart.Enabled:=False;
    TimerScan.Enabled:=False;
    Timer2.Enabled:=False;
    Timer3.Enabled:=False;

    if dmC.CasherDb.Connected then dmC.CasherDb.Close;

    delay(100);

    Close;
  end;
end;

procedure TfmMainCasher.cxButton6Click(Sender: TObject);
begin
{  if Check.ChBeg then   //��������� ���������
  begin
    ShowMessage('��������� ���.');
    Memo2.Clear;
    Memo2.Lines.Add('��������� ���.');
  end
  else
  begin
    fmEnterPassw:=TfmEnterPassw.Create(Application);
    fmEnterPassw.ShowModal;
    fmEnterPassw.Release;
  end;}
end;

procedure TfmMainCasher.cxButton7Click(Sender: TObject);
begin
  if not CanDo('prSberRep') then begin Showmessage('��� ����.'); exit; end;
  if CommonSet.BNManual=2 then
  begin //��� ����
    try
//      fmSber:=tFmSber.Create(Application);

      SberTabStop:=3;

      iMoneyBn:=0; //� ��������
      fmSber.Label1.Visible:=False;

      fmSber.cxButton9.Enabled:=True;
      fmSber.cxButton9.TabStop:=True;
      fmSber.cxButton9.TabOrder:=1;
      fmSber.cxButton9.Default:=True;
      fmSber.cxButton9.Default:=True;

      fmSber.cxButton10.Enabled:=True;
      fmSber.cxButton10.TabOrder:=2;
      fmSber.cxButton10.TabStop:=True;

      fmSber.cxButton11.Enabled:=True;
      fmSber.cxButton11.TabOrder:=3;
      fmSber.cxButton11.TabStop:=True;

      fmSber.cxButton6.Enabled:=True;
      fmSber.cxButton6.TabOrder:=4;
      fmSber.cxButton6.TabStop:=True;

      fmSber.cxButton2.Enabled:=False;
      fmSber.cxButton2.TabStop:=False;

      fmSber.cxButton3.Enabled:=False;
      fmSber.cxButton3.TabStop:=False;
      fmSber.cxButton4.Enabled:=False;
      fmSber.cxButton4.TabStop:=False;
      fmSber.cxButton5.Enabled:=False;
      fmSber.cxButton5.TabStop:=False;
      fmSber.cxButton7.Enabled:=False;
      fmSber.cxButton7.TabStop:=False;

      fmSber.cxButton8.Enabled:=False;
      fmSber.cxButton8.TabStop:=False;

      fmSber.cxButton13.Enabled:=True;
      fmSber.cxButton13.TabOrder:=5;
      fmSber.cxButton13.TabStop:=True;

      fmSber.cxButton1.TabOrder:=6;

      BnStr:='';
      fmSber.Memo1.Clear;
      fmSber.ShowModal;
    finally
//      fmSber.Release;
    end;
  end;
  if CommonSet.BNManual=3 then //���
  begin //��� ����
    try
//      fmSber:=tFmSber.Create(Application);

      SberTabStop:=3;

      iMoneyBn:=0; //� ��������
      fmVTB.Label1.Visible:=False;

      fmVTB.cxButton9.Enabled:=True;
      fmVTB.cxButton9.TabStop:=True;
      fmVTB.cxButton9.TabOrder:=1;
      fmVTB.cxButton9.Default:=True;
      fmVTB.cxButton9.Default:=True;

      fmVTB.cxButton6.Enabled:=True;
      fmVTB.cxButton6.TabOrder:=4;
      fmVTB.cxButton6.TabStop:=True;

      fmVTB.cxButton2.Enabled:=False;
      fmVTB.cxButton2.TabStop:=False;

      fmVTB.cxButton8.Enabled:=False;
      fmVTB.cxButton8.TabStop:=False;

      fmVTB.cxButton13.Enabled:=True;
      fmVTB.cxButton13.TabOrder:=5;
      fmVTB.cxButton13.TabStop:=True;

      fmVTB.cxButton1.TabOrder:=6;

      BnStr:='';

      fmVTB.ShowModal;
    finally
//      fmSber.Release;
    end;
  end;
end;

procedure TfmMainCasher.cxButton8Click(Sender: TObject);
begin
  CountSec:=0; fmMainCasher.Timer2.Enabled:=False;

  fmTrans2.cxDateEdit1.Date:=Date-1;
  fmTrans2.cxSpinEdit1.EditValue:=Nums.ZNum-1;
  fmTrans2.ShowModal;
  if fmTrans2.ModalResult=mrOk then
  begin
//    showmessage('�������� �� ����.');
    fmSt.Memo1.Clear;
    fmSt.Show;
    with fmSt do
    begin
      cxButton1.Enabled:=False;
      Memo1.Lines.Add('������ �������� ... �����.'); Delay(10);

      Recievedata(0,Trunc(fmTrans2.cxDateEdit1.Date));

      Memo1.Lines.Add('�������� ��.'); Delay(10);
      cxButton1.Enabled:=True;
    end;
  end;
  if fmTrans2.ModalResult=mrYes then
  begin
//    showmessage('�������� �� �����.');
    fmSt.Memo1.Clear;
    fmSt.Show;
    with fmSt do
    begin
      CountSec:=0; fmMainCasher.Timer2.Enabled:=False;

      cxButton1.Enabled:=False;
      Memo1.Lines.Add('������ �������� ... �����.'); Delay(10);

      Recievedata(fmTrans2.cxSpinEdit1.EditValue,0);

      Memo1.Lines.Add('�������� ��.'); Delay(10);
      cxButton1.Enabled:=True;

      CountSec:=0; fmMainCasher.Timer2.Enabled:=True;
    end;
  end;

  CountSec:=0; fmMainCasher.Timer2.Enabled:=True;
end;

procedure TfmMainCasher.acBloskExitExecute(Sender: TObject);
begin
//������
end;

procedure TfmMainCasher.Timer3Timer(Sender: TObject);
Var dtCurTime:TDateTime;
begin
  Timer3.Enabled:=False;
  if CurPos.Articul='' then
  begin
    dtCurTime:=frac(now);
    if dtCurTime<0.458333 then ToCustomDisp('    ������ ����!    ','                    ') //11 �����
    else
    begin
      if dtCurTime>0.75 then ToCustomDisp('    ������ �����!    ','                    ') //18 �����
      else  ToCustomDisp('    ������ ����!    ','                    ');
    end;
  end;
//  ToCustomDisp('      �������       ','    �� ������� !    ');
end;

procedure TfmMainCasher.FormCanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
  NewWidth:=Vid.Width;
  NewHeight:=Vid.Height;
  Left:=0;
  Top:=0;

{
  if fmMainCasher.Tag=0 then
  begin
    NewWidth:=1024;
    NewHeight:=768;
  end;

  if fmMainCasher.Tag=17 then  //FF
  begin
    NewWidth:=1280;
    NewHeight:=1024;
    Left:=0;
    Top:=0;
  end;

  if fmMainCasher.Tag=1 then //��������
  begin
    NewWidth:=800;
    NewHeight:=600;
  end;}
end;

procedure TfmMainCasher.cxButton9Click(Sender: TObject);
Var fn:File;
begin
  if not CanDo('prCashOff') then begin fmMainCasher.Memo2.Clear; fmMainCasher.Memo2.Lines.Add('��� ���� �� ��������.');  exit;end;
  if Check.ChBeg then
  begin
    ShowMessage('��������� ���.');
    Memo2.Clear;
    Memo2.Lines.Add('��������� ���.');
  end
  else
  begin
    fmShutd.Showmodal;
    if fmShutd.ModalResult=mrOk then
    begin //���������� �����
//      fmMessOff.ShowModal;
//      if fmMessOff.ModalResult=mrOk then
//      begin
        if MessageDlg('����������� ���������� �����.',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          CashClose;
          try
            FormLog('ShutdWin','CashNum '+INtToStr(CommonSet.CashNum)+' '+Person.Name);

            if FileExists(CurDir+'ending.cmd') then
            begin  //���� ���� ������ - �� ��������� ���
              ShellExecute(handle, 'open', PChar(CurDir+'ending.cmd'),'','', SW_SHOWNORMAL);
              close;
            end else
            begin //����� ����
              zero:=0;
              if not OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES or TOKEN_QUERY, hToken)
              then
              begin
                Exit;
              end; // if not OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES or TOKEN_QUERY, hToken)
              if not OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES or TOKEN_QUERY, hToken)
              then
              begin
                Exit;
              end; // if not OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES or TOKEN_QUERY, hToken)
              // SE_SHUTDOWN_NAME
              if not LookupPrivilegeValue(nil, 'SeShutdownPrivilege' , tkp.Privileges[ 0 ].Luid)
              then
              begin
                Exit;
              end; // if not LookupPrivilegeValue(nil, 'SeShutdownPrivilege' , tkp.Privileges[0].Luid )
              tkp.PrivilegeCount:=1;
              tkp.Privileges[0].Attributes:=SE_PRIVILEGE_ENABLED;
              AdjustTokenPrivileges(hToken, False, tkp, SizeOf(TTokenPrivileges ), tkpo, zero);
              if Boolean(GetLastError()) then
              begin
                Exit;
              end // if Boolean(GetLastError())
              else
              begin
                ExitWindowsEx(EWX_Force or EWX_SHUTDOWN, 0);
                close;
              end;
            end;
          except
          end;
        end else
        begin
          if Panel5.Visible then
          begin
            Panel5.Visible:=False;
            TextEdit3.SetFocus;
            CountSec:=0;
            if not Check.ChBeg then Timer2.Enabled:=True;
          end;
        end;
{      end else
      begin
        if Panel5.Visible then
        begin
          Panel5.Visible:=False;
          TextEdit3.SetFocus;
          CountSec:=0;
          if not Check.ChBeg then Timer2.Enabled:=True;
        end;
      end;}
    end;
    if fmShutd.ModalResult=mrRetry then
    begin //������������ �����
      CashClose;
      try
        FormLog('RestartWin','CashNum '+INtToStr(CommonSet.CashNum)+' '+Person.Name);
        zero:=0;
        zero:=0;
        if not OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES or TOKEN_QUERY, hToken)
        then
        begin
          Exit;
        end; // if not OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES or TOKEN_QUERY, hToken)
        if not OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES or TOKEN_QUERY, hToken)
        then
        begin
          Exit;
        end; // if not OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES or TOKEN_QUERY, hToken)
         // SE_SHUTDOWN_NAME
        if not LookupPrivilegeValue(nil, 'SeShutdownPrivilege' , tkp.Privileges[ 0 ].Luid)
        then
        begin
          Exit;
        end; // if not LookupPrivilegeValue(nil, 'SeShutdownPrivilege' , tkp.Privileges[0].Luid )

        tkp.PrivilegeCount:=1;
        tkp.Privileges[0].Attributes:=SE_PRIVILEGE_ENABLED;
        AdjustTokenPrivileges(hToken, False, tkp, SizeOf(TTokenPrivileges ), tkpo, zero);
        if Boolean(GetLastError()) then
        begin
          Exit;
        end // if Boolean(GetLastError())
        else
        begin
          ExitWindowsEx(EWX_Force or EWX_REBOOT, 0);
          close;
        end;
//        ExitWindowsEx(EWX_Force or EWX_REBOOT, 0);
      except
      end;
    end;

    if fmShutd.ModalResult=mrYes then
    begin //������������ ���������
      CashClose;
      try
        FormLog('RestartPrg','CashNum '+INtToStr(CommonSet.CashNum)+' '+Person.Name);

        if FileExists(CurDir+'casher.new') then
        begin
          assignfile(fn,CurDir+'casher.new');
          erase(fn);
        end;

        if FileExists(CurDir+'Reloader.exe') then
        begin
          ShellExecute(handle, 'open', PChar(CurDir+'Reloader.exe'),'','', SW_SHOWNORMAL);
        end;

        close;
      except
      end;
    end;


  end;
end;

procedure TfmMainCasher.acGetSumsExecute(Sender: TObject);
Var i:Integer;
    StrWk:String;
    dtNow:TDateTime;
    rSum1,rSum2,rSum3,rSum4:Real;
begin
  //�������� ������ ���� �� ����������� - ������� � ��
  with dmC do
  begin
    dtNow:=now;
    Memo2.Clear;
    Memo2.Lines.Add('����������� ������� �� ���� �����.');
    if GetRSumZ then
    begin
      StrWk:='';
      for i:=1 to 16 do
      begin
        if rArrSum[i]<>0 then
          StrWk:=StrWk+fs(rArrSum[i])+'('+its(i)+'); ';
      end;
      Memo2.Lines.Add('������� - '+StrWk);

      //�������
      quZListDet.Active:=False;
      quZListDet.ParamByName('CNUM').AsInteger:=CommonSet.CashNum;
      quZListDet.ParamByName('ZNUM').AsInteger:=CommonSet.ZNum;
      quZListDet.Active:=True;
      quZListDet.First;
      while not quZListDet.Eof do quZListDet.Delete;

      for i:=1 to 16 do
      begin
        if rArrSum[i]<>0 then
        begin
          quZListDet.Append;
          quZListDetCASHNUM.AsInteger:=CommonSet.CashNum;
          quZListDetZNUM.AsInteger:=CommonSet.ZNum;
          quZListDetINOUT.AsInteger:=1; //�������
          quZListDetITYPE.AsInteger:=i; //��� ���� ��������
          quZListDetRSUM.AsFloat:=rArrSum[i];
          quZListDetIDATE.AsInteger:=Trunc(Date);
          quZListDetDDATE.AsDateTime:=dtNow;
          quZListDet.Post;
        end;
      end;

      if GetRetSumZ then
      begin
        StrWk:='';
        for i:=1 to 16 do
        begin
          if rArrSum[i]<>0 then
            StrWk:=StrWk+fs(rArrSum[i])+'('+its(i)+'); ';
        end;
        Memo2.Lines.Add('������� - '+StrWk);

        for i:=1 to 16 do
        begin
          if rArrSum[i]<>0 then
          begin
            quZListDet.Append;
            quZListDetCASHNUM.AsInteger:=CommonSet.CashNum;
            quZListDetZNUM.AsInteger:=CommonSet.ZNum;
            quZListDetINOUT.AsInteger:=-1; //�������
            quZListDetITYPE.AsInteger:=i; //��� ���� ��������
            quZListDetRSUM.AsFloat:=rArrSum[i];
            quZListDetIDATE.AsInteger:=Trunc(Date);
            quZListDetDDATE.AsDateTime:=dtNow;
            quZListDet.Post;
          end;
        end;

        //������ �������
        if GetSumDisc(rSum1,rSum2,rSum3,rSum4) then
        begin
//        StrWk:='����� ������  �� ��������   - '+StrWk;
          if rSum1<>0 then
          begin
            quZListDet.Append;
            quZListDetCASHNUM.AsInteger:=CommonSet.CashNum;
            quZListDetZNUM.AsInteger:=CommonSet.ZNum;
            quZListDetINOUT.AsInteger:=0; //������ - �������
            quZListDetITYPE.AsInteger:=1; //��� ���� ��������
            quZListDetRSUM.AsFloat:=rSum1;
            quZListDetIDATE.AsInteger:=Trunc(Date);
            quZListDetDDATE.AsDateTime:=dtNow;
            quZListDet.Post;
          end;

//          StrWk:='����� ������� �� ��������   - '+StrWk;
          if rSum2<>0 then
          begin
            quZListDet.Append;
            quZListDetCASHNUM.AsInteger:=CommonSet.CashNum;
            quZListDetZNUM.AsInteger:=CommonSet.ZNum;
            quZListDetINOUT.AsInteger:=0; //������ - �������
            quZListDetITYPE.AsInteger:=2; //��� ���� ��������
            quZListDetRSUM.AsFloat:=rSum2;
            quZListDetIDATE.AsInteger:=Trunc(Date);
            quZListDetDDATE.AsDateTime:=dtNow;
            quZListDet.Post;
          end;

//          StrWk:='����� ������  �� ���������  - '+StrWk;
          if rSum3<>0 then
          begin
            quZListDet.Append;
            quZListDetCASHNUM.AsInteger:=CommonSet.CashNum;
            quZListDetZNUM.AsInteger:=CommonSet.ZNum;
            quZListDetINOUT.AsInteger:=0; //������ - �������
            quZListDetITYPE.AsInteger:=3; //��� ���� ��������
            quZListDetRSUM.AsFloat:=rSum3;
            quZListDetIDATE.AsInteger:=Trunc(Date);
            quZListDetDDATE.AsDateTime:=dtNow;
            quZListDet.Post;
          end;

//          StrWk:='����� ������� �� ���������  - '+StrWk;
          if rSum4<>0 then
          begin
            quZListDet.Append;
            quZListDetCASHNUM.AsInteger:=CommonSet.CashNum;
            quZListDetZNUM.AsInteger:=CommonSet.ZNum;
            quZListDetINOUT.AsInteger:=0; //������ - �������
            quZListDetITYPE.AsInteger:=4; //��� ���� ��������
            quZListDetRSUM.AsFloat:=rSum4;
            quZListDetIDATE.AsInteger:=Trunc(Date);
            quZListDetDDATE.AsDateTime:=dtNow;
            quZListDet.Post;
          end;
        end;
        quZListDet.Active:=False;
      end else Memo2.Lines.Add('������..');
    end else Memo2.Lines.Add('������.');
  end;
end;

procedure TfmMainCasher.acReservChExecute(Sender: TObject);
Var iMax:Integer;
begin
  //�������� ���
//  showmessage('��������');
  exit; //��������
  if not CanDo('prReservCheck') then begin fmMainCasher.Memo2.Clear; fmMainCasher.Memo2.Lines.Add('��� ���� �� ��������.');  exit;end;

  if Check.ChBeg then
  begin
    if MessageDlg('�������� ���?', mtConfirmation, [mbYes, mbNo], 0) = mrNo then exit;

    //���� ��� �����
    prWriteLog('~!Reserv;'+IntToStr((Nums.ZNum))+';'+IntToStr((Nums.iCheckNum))+';');

    bBegCash:=False;

    with dmC do
    begin
      //���������� �� � ���
      taCheck.Active:=False;
      taCheck.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
      taCheck.Active:=True;

      dsquReservCh.DataSet:=nil;

      quReservCh.Active:=False;
      quReservCh.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
      quReservCh.ParamByName('ZNUM').AsInteger:=Nums.ZNum;
      quReservCh.ParamByName('CHECKNUM').AsInteger:=Nums.iCheckNum;
      quReservCh.Active:=True;

      quReservCh.First;
      while not quReservCh.Eof do quReservCh.Delete;

      iMax:=1;

      taCheck.First;
      while not taCheck.Eof do
      begin
        quReservCh.Append;
        quReservChCASHNUM.AsINteger:=CommonSet.CashNum;
        quReservChZNUM.AsINteger:=Nums.ZNum;
        quReservChCHECKNUM.AsINteger:=Nums.iCheckNum;
        quReservChNUMPOS.AsINteger:=taCheckNUMPOS.AsInteger;
        quReservChARTICUL.Asstring:=taCheckARTICUL.AsString;
        quReservChRBAR.Asstring:=taCheckRBAR.AsString;
        quReservChNAME.Asstring:=taCheckNAME.AsString;
        quReservChQUANT.AsFloat:=taCheckQUANT.AsFloat;
        quReservChPRICE.AsFloat:=taCheckPRICE.AsFloat;
        quReservChDPROC.AsFloat:=0;
        quReservChDSUM.AsFloat:=0;
        quReservChRSUM.AsFloat:=taCheckRSUM.AsFloat;
        quReservChDEPART.AsINteger:=taCheckDEPART.AsInteger;
        quReservChEDIZM.Asstring:=taCheckEDIZM.AsString;
        quReservCh.Post;

        if iMax<taCheckNUMPOS.AsInteger then iMax:=taCheckNUMPOS.AsInteger;

        taCheck.Next;
      end;


      if (CurPos.Articul>'')and(CurPos.Quant<>0) then //������� � �������� ����������
      begin
        quReservCh.Append;
        quReservChCASHNUM.AsINteger:=CommonSet.CashNum;
        quReservChZNUM.AsINteger:=Nums.ZNum;
        quReservChCHECKNUM.AsINteger:=Nums.iCheckNum;
        quReservChNUMPOS.AsINteger:=iMax+1;
        quReservChARTICUL.Asstring:=CurPos.Articul;
        quReservChRBAR.Asstring:=CurPos.Bar;
        quReservChNAME.Asstring:=CurPos.Name;
        quReservChQUANT.AsFloat:=CurPos.Quant;
        quReservChPRICE.AsFloat:=rv(CurPos.Price);
        quReservChDPROC.AsFloat:=0;
        quReservChDSUM.AsFloat:=0;
        quReservChRSUM.AsFloat:=rv(CurPos.Quant*rv(CurPos.Price));
        quReservChDEPART.AsINteger:=CurPos.Depart;
        quReservChEDIZM.Asstring:=CurPos.EdIzm;
        quReservCh.Post;

      end;

      quReservCh.Active:=False;

      dsquReservCh.DataSet:=quReservCh;

      prClearCheck.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
      prClearCheck.ExecProc;
    end;

    ClearPos;
    ClearSelPos;
    ResetPos;

    CalcSum;
    ClearCheck;
    WriteStatus;

    //����������� ��� �� �����
    Nums.iRet:=InspectSt(Nums.sRet);
    if Nums.iRet<>0 then
      if TestStatus('InspectSt',sMessage)=False then
      begin
  //          ShowMessage('������: "'+Nums.sRet+'". ��������� �������� ����� ���������� ������.');
        WriteStatus;
        fmAttention.Label1.Caption:=sMessage;
        fmAttention.ShowModal;
        exit;// ������ ��� - ������� ��� ������������ ����
      end;

//  if Nums.iRet=21 then CheckCancel;
    CheckCancel;

    FormLog('Reserv','CashNum '+INtToStr(CommonSet.CashNum)+IntToStr((Nums.ZNum))+' '+IntToStr((Nums.iCheckNum)));
    fmMainCasher.Memo2.Clear;
    fmMainCasher.Memo2.Lines.Add('��� '+IntToStr((Nums.iCheckNum))+' �������.');
    fmMainCasher.Memo2.Lines.Add('��� ������ ������ �������� ��.');
  end
  else
  begin  //������� �� ����������
    with dmC do
    begin
      quResChH.Active:=False;
      quResChH.Active:=True;

      quResChH.First;

//      fmReservedCh.ViewRes.BeginUpdate;
      if quResChH.RecordCount>0 then
      begin
        quReservCh.Active:=False;
        quReservCh.ParamByName('CASHNUM').AsInteger:=quResChHCASHNUM.AsInteger;
        quReservCh.ParamByName('ZNUM').AsInteger:=quResChHZNUM.AsInteger;
        quReservCh.ParamByName('CHECKNUM').AsInteger:=quResChHCHECKNUM.AsInteger;
        quReservCh.Active:=True;
      end;
//      fmReservedCh.ViewRes.EndUpdate;

      fmReservedCh.ShowModal;
      if fmReservedCh.ModalResult=mrOk then
      begin




      end;

      quResChH.Active:=False;
      quReservCh.Active:=False;
    end;
  end;
end;

procedure TfmMainCasher.acSaveHKExecute(Sender: TObject);
//Var iid:Integer;
begin
  //��������� �������������� ������� �������
  with dmC do
  begin
    quKeys.Active:=False;
    quKeys.ParamByName('IKT').AsInteger:=1;
    quKeys.Active:=True;

    //����
    quKeys.First;
    while not quKeys.Eof do quKeys.Delete;

    //TextToShortCut
    
    quKeys.Append; quKeysIKT.AsInteger:=1; quKeysACTIONNAME.AsString:='acChangePers'; quKeysID.AsInteger:=1; quKeysHOTKEY.AsString:=ShortCutToText(acChangePers.ShortCut); quKeys.Post;
    quKeys.Append; quKeysIKT.AsInteger:=1; quKeysACTIONNAME.AsString:='acCalcNal'; quKeysID.AsInteger:=1; quKeysHOTKEY.AsString:=ShortCutToText(acCalcNal.ShortCut); quKeys.Post;
    quKeys.Append; quKeysIKT.AsInteger:=1; quKeysACTIONNAME.AsString:='acFindArt'; quKeysID.AsInteger:=1; quKeysHOTKEY.AsString:=ShortCutToText(acFindArt.ShortCut); quKeys.Post;
    quKeys.Append; quKeysIKT.AsInteger:=1; quKeysACTIONNAME.AsString:='acQuantity'; quKeysID.AsInteger:=1; quKeysHOTKEY.AsString:=ShortCutToText(acQuantity.ShortCut); quKeys.Post;
    quKeys.Append; quKeysIKT.AsInteger:=1; quKeysACTIONNAME.AsString:='acError'; quKeysID.AsInteger:=1; quKeysHOTKEY.AsString:=ShortCutToText(acError.ShortCut); quKeys.Post;
    quKeys.Append; quKeysIKT.AsInteger:=1; quKeysACTIONNAME.AsString:='acAnnul'; quKeysID.AsInteger:=1; quKeysHOTKEY.AsString:=ShortCutToText(acAnnul.ShortCut); quKeys.Post;
    quKeys.Append; quKeysIKT.AsInteger:=1; quKeysACTIONNAME.AsString:='acPersDisc'; quKeysID.AsInteger:=1; quKeysHOTKEY.AsString:=ShortCutToText(acPersDisc.ShortCut); quKeys.Post;
    quKeys.Append; quKeysIKT.AsInteger:=1; quKeysACTIONNAME.AsString:='acManDisc'; quKeysID.AsInteger:=1; quKeysHOTKEY.AsString:=ShortCutToText(acManDisc.ShortCut); quKeys.Post;
    quKeys.Append; quKeysIKT.AsInteger:=1; quKeysACTIONNAME.AsString:='acUp'; quKeysID.AsInteger:=1; quKeysHOTKEY.AsString:=ShortCutToText(acUp.ShortCut); quKeys.Post;
    quKeys.Append; quKeysIKT.AsInteger:=1; quKeysACTIONNAME.AsString:='acDown'; quKeysID.AsInteger:=1; quKeysHOTKEY.AsString:=ShortCutToText(acDown.ShortCut); quKeys.Post;
    quKeys.Append; quKeysIKT.AsInteger:=1; quKeysACTIONNAME.AsString:='acDelPos'; quKeysID.AsInteger:=1; quKeysHOTKEY.AsString:=ShortCutToText(acDelPos.ShortCut); quKeys.Post;
    quKeys.Append; quKeysIKT.AsInteger:=1; quKeysACTIONNAME.AsString:='acPrice'; quKeysID.AsInteger:=1; quKeysHOTKEY.AsString:=ShortCutToText(acPrice.ShortCut); quKeys.Post;
    quKeys.Append; quKeysIKT.AsInteger:=1; quKeysACTIONNAME.AsString:='acCashDriverOpen'; quKeysID.AsInteger:=1; quKeysHOTKEY.AsString:=ShortCutToText(acCashDriverOpen.ShortCut); quKeys.Post;
    quKeys.Append; quKeysIKT.AsInteger:=1; quKeysACTIONNAME.AsString:='acRet'; quKeysID.AsInteger:=1; quKeysHOTKEY.AsString:=ShortCutToText(acRet.ShortCut); quKeys.Post;
    quKeys.Append; quKeysIKT.AsInteger:=1; quKeysACTIONNAME.AsString:='acCashRep'; quKeysID.AsInteger:=1; quKeysHOTKEY.AsString:=ShortCutToText(acCashRep.ShortCut); quKeys.Post;
    quKeys.Append; quKeysIKT.AsInteger:=1; quKeysACTIONNAME.AsString:='acCopyCheck'; quKeysID.AsInteger:=1; quKeysHOTKEY.AsString:=ShortCutToText(acCopyCheck.ShortCut); quKeys.Post;
    quKeys.Append; quKeysIKT.AsInteger:=1; quKeysACTIONNAME.AsString:='acCalc'; quKeysID.AsInteger:=1; quKeysHOTKEY.AsString:=ShortCutToText(acCalc.ShortCut); quKeys.Post;
    quKeys.Append; quKeysIKT.AsInteger:=1; quKeysACTIONNAME.AsString:='acPrintTCH'; quKeysID.AsInteger:=1; quKeysHOTKEY.AsString:=ShortCutToText(acPrintTCH.ShortCut); quKeys.Post;
    quKeys.Append; quKeysIKT.AsInteger:=1; quKeysACTIONNAME.AsString:='acExitProgram'; quKeysID.AsInteger:=1; quKeysHOTKEY.AsString:=ShortCutToText(acExitProgram.ShortCut); quKeys.Post;

    quKeys.Active:=False;
  end;

end;

procedure TfmMainCasher.acSetHKExecute(Sender: TObject);
Var i:INteger;
begin
  if GridHK.Tag=0 then
  begin
    showmessage('�������� ������� ����� ��������� ������ ������.');
    cxButton32.Visible:=False;
    cxCalcEdit1.Visible:=True;

    GridHK.Tag:=1;
    GridHK1.Tag:=1;
    for i:=0 to ViewHK.ColumnCount-1 do ViewHK.Columns[i].Styles.Content:=dmC.cxStyle28;
    for i:=0 to ViewHK1.ColumnCount-1 do ViewHK1.Columns[i].Styles.Content:=dmC.cxStyle28;
    Panel6.Tag:=1;


  end else
  begin
    showmessage('����� ��������� ������ ������ ��������.');

    cxButton32.Visible:=True;
    cxCalcEdit1.Visible:=False;

    GridHK.Tag:=0;
    GridHK1.Tag:=0;
    for i:=0 to ViewHK.ColumnCount-1 do ViewHK.Columns[i].Styles.Content:=dmC.cxStyle24;
    for i:=0 to ViewHK1.ColumnCount-1 do ViewHK1.Columns[i].Styles.Content:=dmC.cxStyle24;
    Panel6.Tag:=0;
  end;
end;

procedure TfmMainCasher.ViewHKCellClick(Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
Var iCode:INteger;
    Strwk:String;
    CurrTime:TDateTime;
    StrN:String;
    bRasch:Boolean;
    rKrep:Real;
    sGr:String;
begin
  //
  if GridHK.Tag>0 then
  begin
    //����������������
    cxCalcEdit1.Value:=0;
    cxCalcEdit1.SelectAll;
    Panel6.Tag:=1;
    Panel6.SetFocus;
  end else
  begin
    StrWk:=ACellViewInfo.Text;
//  Label2.Caption:=StrWk; // ������ ������� � ����������
    delete(StrWk,1,POS(#13,StrWk)); //�������� ������ ������
    delete(StrWk,1,POS(#13,StrWk)); //�������� ������ ������
    delete(StrWk,1,POS(#13,StrWk)); //����
    iCode:=StrToIntDef(StrWk,0);
    if iCode>0 then
    begin
//      ShowMessage(its(iCode));

      StrWk:=its(iCode);

      if Length(StrWk)>0 then
      begin //���� � ��������� �� ��������
        if Length(StrWk)>30 then StrWk:=Copy(StrWk,1,30);
        with dmC do
        begin
          quCardsFind.Active:=False;
          quCardsFind.ParamByName('ARTICUL').AsString:=StrWk;
          quCardsFind.Active:=True;
          if quCardsFind.RecordCount>0 then
          begin
            quCardsFind.First;

            SelPos.Articul:=quCardsFindARTICUL.AsString;
            if CommonSet.DepartId>0 then SelPos.Depart:=CommonSet.DepartId
            else SelPos.Depart:=quCardsFindDEPART.AsInteger;
            SelPos.Name:=quCardsFindNAME.AsString;
            SelPos.Quant:=1;
            SelPos.Price:=rv(quCardsFindPRICE_RUB.AsFloat);
            SelPos.DProc:=0;
            SelPos.DSum:=0;
            SelPos.Bar:='';
            SelPos.EdIzm:=quCardsFindMESURIMENT.AsString;
            SelPos.AVid:=quCardsFindAVID.AsInteger;
            SelPos.iNDS:=quCardsFindINDS.AsInteger;

            if SelPos.Price=0 then
            begin
              Memo2.Clear;
              Memo2.Lines.Add(ZerroMess);
              fmAttention.Label1.Caption:=ZerroMess;
              fmAttention.ShowModal;
              TextEdit3.SetFocus;
              TextEdit3.SelectAll;
            end else
            begin
              bRasch:=True;
              CurrTime:=frac(now);
              if (CommonSet.AlkoKrepMax>0)and((CurrTime<CommonSet.WkTimeBeg)or((CurrTime>CommonSet.WkTimeEnd))) then   //�������� ��������
              begin
                StrN:=SelPos.Name;
                //�������� ��������
                if pos('%',StrN)>0 then
                begin
                  StrN:=Copy(StrN,1,pos('%',StrN)-1);
                  while pos(' ',StrN)>0 do Delete(StrN,1,pos(' ',StrN));
                  while pos('.',StrN)>0 do StrN[pos('.',StrN)]:=',';
                  rKrep:=StrToFloatDef(StrN,0);
//              if (rKrep-CommonSet.AlkoKrepMax)>0.001 then bRasch:=False;
                  if (rKrep-CommonSet.AlkoKrepMax)>0.001 then
                  begin
                    sGr:=FindClName(StrToIntDef(SelPos.Articul,0));
                    if pos('���',sGr)=1 then  bRasch:=False;
                  end;
                end;
              end;
              if bRasch then
              begin
                //����� ������� ��� �������� ����� ���� �����
                if (SelPos.AVid>0)and(CommonSet.GetAMark=1) then //��� �� ���� � ����������� �� �����
                begin
                  if prGetFixAVid(SelPos.AVid) then
                  begin //����� ����������� ��
                    Beep;
                    fmGetAM.ShowModal;
                    if fmGetAM.ModalResult=mrOk then
                    begin
                      SelPos.AMark:=fmGetAM.cxLabel2.Caption;
                    end else bRasch:=False;
                  end;
                end;

                if bRasch then prAddPosSel(0,1);
              end
              else
              begin
                Memo2.Lines.Add('��������� ������� �������� ��������� ����� '+IntToStr(CommonSet.AlkoKrepMax)+'%');
                fmAttention.Label1.Caption:='��������� ������� �������� ��������� ����� '+IntToStr(CommonSet.AlkoKrepMax)+'% � '+FormatDateTime('hh:nn ss',CommonSet.WkTimeBeg)+' �� '+FormatDateTime('hh:nn ss',CommonSet.WkTimeEnd);
                prWriteLog('~~AttentionShow;'+'��������� ������� �������� ��������� ����� '+IntToStr(CommonSet.AlkoKrepMax)+'%');
                fmAttention.ShowModal;
              end;
            end;
          end;
          quCardsFind.Active:=False;
        end;
      end;
    end;
  end;
end;


procedure TfmMainCasher.ViewHK1CellClick(Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
Var iCode:INteger;
    Strwk:String;
    CurrTime:TDateTime;
    StrN:String;
    bRasch:Boolean;
    rKrep:Real;
    sGr:String;
begin
  //
  if GridHK1.Tag>0 then
  begin
    //����������������
    cxCalcEdit1.Value:=0;
    cxCalcEdit1.SelectAll;
    Panel6.Tag:=2;
    Panel6.SetFocus;
  end else
  begin
    StrWk:=ACellViewInfo.Text;
//  Label2.Caption:=StrWk; // ������ ������� � ����������
    delete(StrWk,1,POS(#13,StrWk)); //�������� ������ ������
    delete(StrWk,1,POS(#13,StrWk)); //�������� ������ ������
    delete(StrWk,1,POS(#13,StrWk)); //����
    iCode:=StrToIntDef(StrWk,0);
    if iCode>0 then
    begin
//      ShowMessage(its(iCode));

      StrWk:=its(iCode);

      if Length(StrWk)>0 then
      begin //���� � ��������� �� ��������
        if Length(StrWk)>30 then StrWk:=Copy(StrWk,1,30);
        with dmC do
        begin
          quCardsFind.Active:=False;
          quCardsFind.ParamByName('ARTICUL').AsString:=StrWk;
          quCardsFind.Active:=True;
          if quCardsFind.RecordCount>0 then
          begin
            quCardsFind.First;

            SelPos.Articul:=quCardsFindARTICUL.AsString;
            if CommonSet.DepartId>0 then SelPos.Depart:=CommonSet.DepartId
            else SelPos.Depart:=quCardsFindDEPART.AsInteger;
            SelPos.Name:=quCardsFindNAME.AsString;
            SelPos.Quant:=1;
            SelPos.Price:=rv(quCardsFindPRICE_RUB.AsFloat);
            SelPos.DProc:=0;
            SelPos.DSum:=0;
            SelPos.Bar:='';
            SelPos.EdIzm:=quCardsFindMESURIMENT.AsString;
            SelPos.AVid:=quCardsFindAVID.AsInteger;
            SelPos.iNDS:=quCardsFindINDS.AsInteger;

            if SelPos.Price=0 then
            begin
              Memo2.Clear;
              Memo2.Lines.Add(ZerroMess);
              fmAttention.Label1.Caption:=ZerroMess;
              fmAttention.ShowModal;
              TextEdit3.SetFocus;
              TextEdit3.SelectAll;
            end else
            begin
              bRasch:=True;
              CurrTime:=frac(now);
              if (CommonSet.AlkoKrepMax>0)and((CurrTime<CommonSet.WkTimeBeg)or((CurrTime>CommonSet.WkTimeEnd))) then   //�������� ��������
              begin
                StrN:=SelPos.Name;
                //�������� ��������
                if pos('%',StrN)>0 then
                begin
                  StrN:=Copy(StrN,1,pos('%',StrN)-1);
                  while pos(' ',StrN)>0 do Delete(StrN,1,pos(' ',StrN));
                  while pos('.',StrN)>0 do StrN[pos('.',StrN)]:=',';
                  rKrep:=StrToFloatDef(StrN,0);
//              if (rKrep-CommonSet.AlkoKrepMax)>0.001 then bRasch:=False;
                  if (rKrep-CommonSet.AlkoKrepMax)>0.001 then
                  begin
                    sGr:=FindClName(StrToIntDef(SelPos.Articul,0));
                    if pos('���',sGr)=1 then  bRasch:=False;
                  end;
                end;
              end;
              if bRasch then
              begin
                //����� ������� ��� �������� ����� ���� �����
                if (SelPos.AVid>0)and(CommonSet.GetAMark=1) then //��� �� ���� � ����������� �� �����
                begin
                  if prGetFixAVid(SelPos.AVid) then
                  begin //����� ����������� ��
                    Beep;
                    fmGetAM.ShowModal;
                    if fmGetAM.ModalResult=mrOk then
                    begin
                      SelPos.AMark:=fmGetAM.cxLabel2.Caption;
                    end else bRasch:=False;
                  end;
                end;

                if bRasch then prAddPosSel(0,1);
              end
              else
              begin
                Memo2.Lines.Add('��������� ������� �������� ��������� ����� '+IntToStr(CommonSet.AlkoKrepMax)+'%');
                fmAttention.Label1.Caption:='��������� ������� �������� ��������� ����� '+IntToStr(CommonSet.AlkoKrepMax)+'% � '+FormatDateTime('hh:nn ss',CommonSet.WkTimeBeg)+' �� '+FormatDateTime('hh:nn ss',CommonSet.WkTimeEnd);
                prWriteLog('~~AttentionShow;'+'��������� ������� �������� ��������� ����� '+IntToStr(CommonSet.AlkoKrepMax)+'%');
                fmAttention.ShowModal;
              end;
            end;
          end;
          quCardsFind.Active:=False;
        end;
      end;
    end;
  end;
end;

procedure TfmMainCasher.cxButton21Click(Sender: TObject);
begin
  prAddHKPos('0');
end;

procedure TfmMainCasher.cxButton12Click(Sender: TObject);
begin
  prAddHKPos('1');
end;

procedure TfmMainCasher.cxButton13Click(Sender: TObject);
begin
  prAddHKPos('2');
end;

procedure TfmMainCasher.cxButton14Click(Sender: TObject);
begin
  prAddHKPos('3');
end;

procedure TfmMainCasher.cxButton15Click(Sender: TObject);
begin
  prAddHKPos('4');
end;

procedure TfmMainCasher.cxButton16Click(Sender: TObject);
begin
  prAddHKPos('5');
end;

procedure TfmMainCasher.cxButton17Click(Sender: TObject);
begin
  prAddHKPos('6');
end;

procedure TfmMainCasher.cxButton18Click(Sender: TObject);
begin
  prAddHKPos('7');
end;

procedure TfmMainCasher.cxButton19Click(Sender: TObject);
begin
  prAddHKPos('8');
end;

procedure TfmMainCasher.cxButton20Click(Sender: TObject);
begin
  prAddHKPos('9');
end;

procedure TfmMainCasher.cxButton25Click(Sender: TObject);
Var iC,iR:INteger;
    StrWk,Str1:String;

    sBar:ShortString;
    CurrTime:TDateTime;
    StrN:String;
    bRasch:Boolean;
    rKrep:Real;
    sGr:String;
    sMess:String;
    k,StrLen:Integer;
begin
  //������� ����������� � ���������
  if (GridHK.Tag>0) then //��������� ������� � ������� �������
  begin
    StrLen:=14;
    with dmC do
    begin
      if cxCalcEdit1.Value>0 then
      begin
        quCardsFind.Active:=False;
        quCardsFind.ParamByName('ARTICUL').AsString:=cxCalcEdit1.Text;
        quCardsFind.Active:=True;
        if quCardsFind.RecordCount>0 then
        begin
          if Panel6.Tag=1 then //������ ����
          begin
            k:=ViewHK.Tag;
            if k=1 then k:=0;
            if k>1 then k:=300+k*100;   // k=1  1-100  k=2 500-600  k=3 600-700  k=4 700-800 ..           // �������� 100-500   ��� ������� ������

            iC:=ViewHK.Controller.FocusedColumnIndex;
            iR:=ViewHK.Controller.FocusedRowIndex;
            if (iC>=0) and (iR>=0) then
            begin
              try
                quHotKey.Active:=False;
                quHotKey.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
                quHotKey.Active:=True;

                quHotKey.First;
                while not quHotKey.Eof do
                begin
                  if (quHotKeyCASHNUM.AsInteger=CommonSet.CashNum)and
                     (quHotKeyIROW.AsInteger=iR)and
                     (quHotKeyICOL.AsInteger=iC+k) then
                  begin
                    quHotKey.Delete; break;
                  end else quHotKey.Next;
                end;
                quHotKey.Append;
                quHotKeyCASHNUM.AsInteger:=CommonSet.CashNum;
                quHotKeyIROW.AsInteger:=iR;
                quHotKeyICOL.AsInteger:=iC+k;
                quHotKeySIFR.AsString:=quCardsFindARTICUL.AsString;
                quHotKey.Post;

//              quHotKey.Refresh;

                Str(quCardsFindPRICE_RUB.AsFloat:6:2,StrWk);
                Str1:=quCardsFindNAME.AsString; while pos(#13,Str1)>0 do delete(Str1,pos(#13,Str1),1);

                StrWk:=Copy(Str1,1,StrLen)+#13+Copy(Str1,StrLen+1,StrLen)+#13+'����:'+StrWk+'�.'+#13+quCardsFindARTICUL.AsString;
                ViewHK.DataController.SetValue(iR,iC,StrWk);

                quHotKey.Active:=False;
              except
              end;
            end;
          end;
          if Panel6.Tag=2 then //������� ����
          begin
            k:=ViewHK1.Tag;
            k:=k*100;

            iC:=ViewHK1.Controller.FocusedColumnIndex;
            iR:=ViewHK1.Controller.FocusedRowIndex;
            if (iC>=0) and (iR>=0) then
            begin
              try
                quHotKey.Active:=False;
                quHotKey.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
                quHotKey.Active:=True;

                quHotKey.First;
                while not quHotKey.Eof do
                begin
                  if (quHotKeyCASHNUM.AsInteger=CommonSet.CashNum)and
                     (quHotKeyIROW.AsInteger=iR)and
                     (quHotKeyICOL.AsInteger=iC+k) then
                  begin
                    quHotKey.Delete; break;
                  end else quHotKey.Next;
                end;
                quHotKey.Append;
                quHotKeyCASHNUM.AsInteger:=CommonSet.CashNum;
                quHotKeyIROW.AsInteger:=iR;
                quHotKeyICOL.AsInteger:=iC+k;
                quHotKeySIFR.AsString:=quCardsFindARTICUL.AsString;
                quHotKey.Post;

//              quHotKey.Refresh;

                Str(quCardsFindPRICE_RUB.AsFloat:6:2,StrWk);
                Str1:=quCardsFindNAME.AsString; while pos(#13,Str1)>0 do delete(Str1,pos(#13,Str1),1);

                StrWk:=Copy(Str1,1,StrLen)+#13+Copy(Str1,StrLen+1,StrLen)+#13+'����:'+StrWk+'�.'+#13+quCardsFindARTICUL.AsString;

                ViewHK1.DataController.SetValue(iR,iC,StrWk);

                quHotKey.Active:=False;

              except
              end;
            end;
          end;
        end;
      end else
      begin
        if Panel6.Tag=1 then //������ ����
        begin
          k:=ViewHK.Tag;
          if k=1 then k:=0;
          if k>1 then k:=300+k*100;   // k=1  1-100  k=2 500-600  k=3 600-700  k=4 700-800 ..           // �������� 100-500   ��� ������� ������

          iC:=ViewHK.Controller.FocusedColumnIndex;
          iR:=ViewHK.Controller.FocusedRowIndex;
          if (iC>=0) and (iR>=0) then
          begin
            try
              quHotKey.Active:=False;
              quHotKey.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
              quHotKey.Active:=True;

              quHotKey.First;
              while not quHotKey.Eof do
              begin
                if (quHotKeyCASHNUM.AsInteger=CommonSet.CashNum)and
                   (quHotKeyIROW.AsInteger=iR)and
                   (quHotKeyICOL.AsInteger=iC+k) then
                begin
                  quHotKey.Delete; break;
                end else quHotKey.Next;
              end;

              ViewHK.DataController.SetValue(iR,iC,'');

              quHotKey.Active:=False;
            except
            end;
          end;
        end;
        if Panel6.Tag=2 then //������� ����
        begin
          k:=ViewHK1.Tag;
          k:=k*100;

          iC:=ViewHK1.Controller.FocusedColumnIndex;
          iR:=ViewHK1.Controller.FocusedRowIndex;
          if (iC>=0) and (iR>=0) then
          begin
            try
              quHotKey.Active:=False;
              quHotKey.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
              quHotKey.Active:=True;

              quHotKey.First;
              while not quHotKey.Eof do
              begin
                if (quHotKeyCASHNUM.AsInteger=CommonSet.CashNum)and
                   (quHotKeyIROW.AsInteger=iR)and
                   (quHotKeyICOL.AsInteger=iC+k) then
                begin
                  quHotKey.Delete; break;
                end else quHotKey.Next;
              end;

              ViewHK1.DataController.SetValue(iR,iC,'');

              quHotKey.Active:=False;
            except
            end;
          end;
        end;
      end;
    end;
  end else
  begin
    if Panel6.Tag=0 then
    begin
      if bAddPos then exit;

      sBar:=TextEdit3.Text;
      if pos('22'+its(CommonSet.CutTailCode),sBar)=1 then //��� ������� - ������������ ��� �������
      begin
        Memo2.Clear;
        with dmFB do
        begin
          try
            Memo2.Lines.Add('����� ... ���� ����� ����.');
            Cash.Close;
            Cash.DBName:=CommonSet.CashDB;
            Cash.Open;
          except
            Memo2.Lines.Add('    ������ �������� ���� ������. ���������� � ���������������.');
          end;

          if Cash.Connected then
          begin
            if Check.Operation=1 then
            begin
              quXCheck.Active:=False;
              quXCheck.ParamByName('SBAR').AsString:=sBar;
              quXCheck.Active:=True;
              if quXCheck.RecordCount>0 then
              begin
                Check.ECheck:=1;

                Memo2.Lines.Add('��� "'+sBar+'" ������ - '+its(quXCheck.RecordCount)+' �������.'); delay(10);
                quXCheck.First;
                while not quXCheck.Eof do
                begin
                  Memo2.Lines.Add('��� '+its(quXCheckICODE.AsInteger)+' ���-�� '+fs(quXCheckQUANT.asfloat)+' ����� '+fs(quXCheckRSUM.AsFloat)); delay(10);
                  //����������� ���������� �����
                  if quXCheckQUANT.AsFloat>0 then
                  begin
                    if prAddByCode(quXCheckICODE.AsInteger,quXCheckBARCODE.AsString,quXCheckQUANT.AsFloat,sMess) then prAddPosSel(0,1)
                    else
                    begin
                      Memo2.Lines.Add(sMess);
                      fmAttention.Label1.Caption:=sMess;
                      prWriteLog('~~AttentionShow;'+sMess);
                      fmAttention.ShowModal;
                    end;
                  end;

                  quXCheck.Next;
                end;
              end else
                Memo2.Lines.Add('��� "'+sBar+'" �� ������.');

              quXCheck.Active:=False;

            end else
              Memo2.Lines.Add('    ������ ���� ����� �������.');

            delay(100);
            Cash.Close;
          end;
        end;
      end else
      begin
        if not FindBar(SOnlyDigit1(sBar)) then
        begin
          Beep;
          Memo2.Clear;
          Memo2.Lines.Add('�������� - "'+sBar+'" �� ������.');
          fmBarMessage.ShowModal;
        end
        else
        begin
          if SelPos.Price=0 then
          begin
            Memo2.Clear;
            Memo2.Lines.Add(ZerroMess);
            fmAttention.Label1.Caption:=ZerroMess;
            fmAttention.ShowModal;
          end else
          begin
            bRasch:=True;
            CurrTime:=frac(now);
            if (CommonSet.AlkoKrepMax>0)and((CurrTime<CommonSet.WkTimeBeg)or((CurrTime>CommonSet.WkTimeEnd))) then   //�������� ��������
            begin
              StrN:=SelPos.Name;
              //�������� ��������
              if pos('%',StrN)>0 then
              begin
                StrN:=Copy(StrN,1,pos('%',StrN)-1);
                while pos(' ',StrN)>0 do Delete(StrN,1,pos(' ',StrN));
                while pos('.',StrN)>0 do StrN[pos('.',StrN)]:=',';
                rKrep:=StrToFloatDef(StrN,0);
//              if (rKrep-CommonSet.AlkoKrepMax)>0.001 then bRasch:=False;
                if (rKrep-CommonSet.AlkoKrepMax)>0.001 then
                begin
                  sGr:=FindClName(StrToIntDef(SelPos.Articul,0));
                  if pos('���',sGr)=1 then  bRasch:=False;
                end;
              end;
            end;

            if bRasch then
            begin
              //����� ������� ��� �������� ����� ���� �����
              if (SelPos.AVid>0)and(CommonSet.GetAMark=1) then //��� �� ���� � ����������� �� �����
              begin
                if prGetFixAVid(SelPos.AVid) then
                begin //����� ����������� ��
                  Beep;
                  fmGetAM.ShowModal;
                  if fmGetAM.ModalResult=mrOk then
                  begin
                    SelPos.AMark:=fmGetAM.cxLabel2.Caption;
                  end else bRasch:=False;
                end;
              end;

              if bRasch then prAddPosSel(0,1);
            end
            else
            begin
              Memo2.Lines.Add('��������� ������� �������� ��������� ����� '+IntToStr(CommonSet.AlkoKrepMax)+'%');
              fmAttention.Label1.Caption:='��������� ������� �������� ��������� ����� '+IntToStr(CommonSet.AlkoKrepMax)+'% � '+FormatDateTime('hh:nn ss',CommonSet.WkTimeBeg)+' �� '+FormatDateTime('hh:nn ss',CommonSet.WkTimeEnd);
              prWriteLog('~~AttentionShow;'+'��������� ������� �������� ��������� ����� '+IntToStr(CommonSet.AlkoKrepMax)+'%');
              fmAttention.ShowModal;
            end;
            TextEdit3.Text:='';
          end;
          TextEdit3.SetFocus;
          TextEdit3.SelectAll;
        end;
      end;
    end;
  end;
end;

procedure TfmMainCasher.cxButton32Click(Sender: TObject);
begin
  TextEdit3.Text:='';
end;

procedure TfmMainCasher.cxButton33Click(Sender: TObject);
//Var bStop:Boolean;
begin
  with dmC do
  begin
    with fmAChecks do
    begin
      cxSpinEdit1.Value:=Nums.ZNum; // ������ �� �������

{      quADH.Active:=False;
      quADH.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
      quADH.ParamByName('ZNUM').AsInteger:=Nums.ZNum;
      quADH.Active:=True;
      quADH.First;}
      
    end;
    fmAChecks.ShowModal;
    if fmAChecks.ModalResult=mrOk then
    begin //���������������
      if (CurPos.Articul='')and(abs(CurPos.Quant)<0.00001) then //������� ���
      begin
        if taCheck.Active=False then
        begin
          taCheck.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
          taCheck.Active:=True;
        end else taCheck.FullRefresh;
        if (taCheck.RecordCount=0)and(fmAChecks.quADH.RecordCount>0) then
        begin
          //��� ����� ������ � ���� ��� ��������� - ������
          with fmAChecks do
          begin
            FindDiscount(quADHDBAR.AsString); //������ �������������
//            Check.Operation:=quADHOPER.AsInteger;
            Check.Operation:=1; Label16.Caption:='�������� "�������"';
            try
              //������������ ������� ������
              fmMainCasher.Timer2.Enabled:=False;
              CountSec:=0;
              Check.ChBeg:=True;
              Check.ChEnd:=False;
              bBegCash:= False; //�������������� ��������� ������ ��� ������� ��������
              bBegCashEnd:= False;

              //�������� ������� �����
              Nums.iRet:=InspectSt(Nums.sRet);
              if Nums.iRet<>0 then TestFp('InspectSt');
              //���� ������ ����� ��������� ���
              //������� ��������� �����

              quADS.First;
              while not quADS.Eof do
              begin
                if (quADSQUANTITY.AsFloat>0.001) and (quADSTOTALRUB.AsFloat>0.001) then
                begin
                  CurPos.ARTICUL:=quADSARTICUL.AsString;
                  CurPos.NAME:=quADSNAME.AsString;
                  CurPos.QUANT:=quADSQUANTITY.AsFloat;
                  CurPos.PRICE:=quADSPRICERUB.AsFloat;
                  CurPos.DPROC:=quADSDPROC.AsFloat;
                  CurPos.DSUM:=quADSDSUM.AsFloat;
                  CurPos.DEPART:=CommonSet.DepartId;
                  CurPos.BAR:=quADSBAR.AsString;
                  CurPos.EdIzm:=quADSMESURIMENT.AsString;

                  prAddPosSel(0,1);  //���������� �������
                end;
                if (quADSQUANTITY.AsFloat<-0.001) and (quADSTOTALRUB.AsFloat<-0.001) then
                begin
                  CurPos.ARTICUL:=quADSARTICUL.AsString;
                  CurPos.NAME:=quADSNAME.AsString;
                  CurPos.QUANT:=quADSQUANTITY.AsFloat;
                  CurPos.PRICE:=quADSPRICERUB.AsFloat;
                  CurPos.DPROC:=quADSDPROC.AsFloat;
                  CurPos.DSUM:=quADSDSUM.AsFloat;
                  CurPos.DEPART:=CommonSet.DepartId;
                  CurPos.BAR:=quADSBAR.AsString;
                  CurPos.EdIzm:=quADSMESURIMENT.AsString;

                  prAddPosSel(1,1); //�������� �������
                end;

                quADS.Next;
              end;
            except;
              trUpdCh.Rollback;
            end;
          end;
        end else Memo2.Lines.Add('�������������� ����������. ��������� �������� ���.');
      end else Memo2.Lines.Add('�������������� ����������. ��������� �������� ���..');
    end;
  end;
end;

procedure TfmMainCasher.acTestUTMExecute(Sender: TObject);
{Const
  CR = #$0d;
  LF = #$0a;
  CRLF = CR + LF;
  Boundary = 'END_OF_PART';

var  IdF:Integer;
     SendXml,RetXml: TNativeXml;

     httpsend: THTTPSend;
     s: AnsiString;
     FS: TMemoryStream;
     RetVal:String;
}
var StrWk:string;
begin
  StrWk:='UseUTM='+its(AlcoSet.UseUTM)+#$0D+' UTMIP='+AlcoSet.UTMIP+#$0D+' PRINTQR='+its(AlcoSet.PRINTQR)+#$0D+' QRTYPE='+its(AlcoSet.QRTYPE)+#$0D+' NameOrg='+AlcoSet.NameOrg+#$0D+' NameShop='+AlcoSet.NameShop+#$0D+' AdrShop='+AlcoSet.AdrShop+#$0D+' INN='+AlcoSet.INN+#$0D+' KPP='+AlcoSet.KPP+#$0D+' PathAlco='+AlcoSet.PathAlco;

  if fTestAlcoConn then ShowMessage('����� � ��� ����� ��. '+#$0D+StrWk)
  else ShowMessage('����������� ����� � ��� �����. '+StrWk);

{
  try
    SendXml := TNativeXml.Create(nil);
    RetXml  := TNativeXml.Create(nil);
    httpsend := THTTPSend.Create;
    FS := TMemoryStream.Create;

    IdF:=Nums.ZNum*1000+Nums.iCheckNum;

    SendXml.XmlFormat := xfReadable;
    SendXml.CreateName('Cheque');
    SendXml.Root.WriteAttributeString('inn',Trim(AlcoSet.INN));
    SendXml.Root.WriteAttributeString('datetime',FormatDateTime('ddmmyyhhnn',now));
    SendXml.Root.WriteAttributeString('kpp',Trim(AlcoSet.KPP));
    SendXml.Root.WriteAttributeString('address',UTF8Encode(Trim(AlcoSet.AdrShop)));
    SendXml.Root.WriteAttributeString('name',UTF8Encode(Trim(AlcoSet.NameShop)));

    SendXml.Root.WriteAttributeString('kassa','ALK060200439');
    SendXml.Root.WriteAttributeString('shift','1804');
    SendXml.Root.WriteAttributeString('number','54');

//    SendXml.Root.WriteAttributeString('shift',Its(Nums.ZNum));
//    SendXml.Root.WriteAttributeString('number',Its(Nums.iCheckNum));
//    SendXml.Root.WriteAttributeString('kassa',Trim(Nums.RegNum));

    SendXml.Root.NodeNew('Bottle');
    SendXml.Root.NodeByName('Bottle').WriteAttributeString('barcode','22N00000XON5PCG7U2C2N10602130020139481Z9OC73V19NFPINLC9ORT3KKEOQ9XDO');
    SendXml.Root.NodeByName('Bottle').WriteAttributeString('ean','4607062868683');
    SendXml.Root.NodeByName('Bottle').WriteAttributeString('price',fts00(222.50));

//    SendXml.SaveToFile(Its(Nums.ZNum)+'_'+its(Nums.iCheckNum)+'.xml');
    SendXml.SaveToStream(FS);

    httpsend.MimeType := 'multipart/form-data; boundary='+Boundary;  //���������� Contetn-Type �������
    s:='--'+Boundary+CRLF+'Content-Disposition: form-data; name="xml_file"; filename="'+IdToName(IdF)+'"'+CRLF+'Content-Type: application/xml'+CRLF+CRLF;
    httpsend.Document.Write(PAnsiChar(s)^, Length(s));
    FS.Position := 0;
    httpsend.Document.CopyFrom(FS, FS.Size);  //���������� ����� � ���� ���������
    S:=CRLF+CRLF+'--'+Boundary+'--'+CRLF;  //��������� ���� �������
    httpsend.Document.Write(PAnsiChar(s)^, Length(s)); // ��������� ���� ���������
    httpsend.KeepAlive:=False;
    httpsend.Status100:=True;
    httpsend.Headers.Add('Accept: */*');

    // ���������� ������
    if httpsend.HTTPMethod('POST','http://'+AlcoSet.UTMIP+':8080/xml') then
    begin
      httpsend.Document.Position:=0;
      RetXml.LoadFromStream(httpsend.Document);
      RetXml.XmlFormat := xfReadable;
      RetXml.SaveToFile('UTM_'+IdToName(IdF));

      RetVal:='';
      AlcoSet.URL:='';
      AlcoSet.CRCODE:='';
      AlcoSet.Ver:='';


      if httpsend.ResultCode=200 then  //��� ������
      begin
        if Assigned(RetXml.Root.NodeByName('url')) then AlcoSet.URL:=UTF8Decode(RetXml.Root.NodeByName('url').Value);
        if Assigned(RetXml.Root.NodeByName('sign')) then AlcoSet.CRCODE:=UTF8Decode(RetXml.Root.NodeByName('sign').Value);
        if Assigned(RetXml.Root.NodeByName('ver')) then AlcoSet.Ver:=UTF8Decode(RetXml.Root.NodeByName('ver').Value);

        RetVal:=AlcoSet.URL;

        ShowMessage('�k ('+its(httpsend.ResultCode)+' '+httpsend.ResultString+' "'+RetVal+'"');
      end else
      begin
        if Assigned(RetXml.Root.NodeByName('error')) then RetVal:=UTF8Decode(RetXml.Root.NodeByName('error').Value);

        ShowMessage('������ ��� ('+its(httpsend.ResultCode)+' '+httpsend.ResultString+' "'+RetVal+'"');
      end;
    end;

  finally
    SendXml.Free;
    httpsend.Free;
    FS.Free;
    RetXml.Free;
  end;}
end;

procedure TfmMainCasher.acTestPrintUTMExecute(Sender: TObject);
begin
  AlcoSet.URL:='http://check.egais.ru?id=effdcb77-a989-464d-b6f4-69e9cbf77ce4&dt=1906160926&cn=020000021354';
  AlcoSet.CRCODE:='181790D79C79729926ED2A31DFD93480ED5B62D0130F2A92A829C049B5D8DF11F49D317A669315D6BDDC6F13F03B329CB017F2FA695B2328D26FBC3EF91335D9';

  prPrintQRCode;

  AlcoSet.URL:='';
  AlcoSet.CRCODE:='';
end;

procedure TfmMainCasher.acSaveVidExecute(Sender: TObject);
begin
  prSaveVid;
end;

procedure TfmMainCasher.cxButton37Click(Sender: TObject);
begin
  ViewHK.Tag:=1; RefreshHotList;
end;

procedure TfmMainCasher.cxButton38Click(Sender: TObject);
begin
  ViewHK.Tag:=2; RefreshHotList;
end;

procedure TfmMainCasher.cxButton39Click(Sender: TObject);
begin
  ViewHK.Tag:=3; RefreshHotList;
end;

procedure TfmMainCasher.cxButton40Click(Sender: TObject);
begin
  ViewHK.Tag:=4; RefreshHotList;
end;

procedure TfmMainCasher.cxButton41Click(Sender: TObject);
begin
  ViewHK.Tag:=5; RefreshHotList;
end;

procedure TfmMainCasher.cxButton42Click(Sender: TObject);
begin
  ViewHK.Tag:=6; RefreshHotList;
end;

procedure TfmMainCasher.cxButton43Click(Sender: TObject);
begin
  ViewHK.Tag:=7; RefreshHotList;
end;

procedure TfmMainCasher.cxButton44Click(Sender: TObject);
begin
  ViewHK.Tag:=8; RefreshHotList;
end;

procedure TfmMainCasher.cxButton34Click(Sender: TObject);
begin
  ViewHK1.Tag:=1; RefreshHotList1;
end;

procedure TfmMainCasher.cxButton35Click(Sender: TObject);
begin
  ViewHK1.Tag:=2; RefreshHotList1;
end;

procedure TfmMainCasher.cxButton36Click(Sender: TObject);
begin
  ViewHK1.Tag:=3; RefreshHotList1;
end;

procedure TfmMainCasher.cxButton45Click(Sender: TObject);
begin
  ViewHK1.Tag:=4; RefreshHotList1;
end;

procedure TfmMainCasher.acResetVidExecute(Sender: TObject);
begin
  prResetVid;
  CreateHotList;
  CreateHotList1;
end;

procedure TfmMainCasher.acComProxyExecute(Sender: TObject);
begin
  //������� COMPROXY
  fmSt.Memo1.Clear;
  fmSt.Show;
  with fmSt do
  with dmC do
  begin
    FisPrint.Close;

    cxButton1.Enabled:=False;
    Memo1.Lines.Add('�����... ���� ��������� ������ ComProxy'); Delay(10);
    try
      StopService('ComProxy');
      Memo1.Lines.Add(' - ����������.'); Delay(10);
    except
      Memo1.Lines.Add(' ... ������'); Delay(10);
    end;

    Delay(1000);

    Memo1.Lines.Add('�����... ���� ������ ������ ComProxy'); Delay(10);
    try
      StartService('ComProxy');
      Delay(5000);
      Memo1.Lines.Add(' - ���������.'); Delay(10);
    except
      Memo1.Lines.Add(' ... ������'); Delay(10);
    end;

    Memo1.Lines.Add('  ��������� ����� � ��.'); Delay(10);

    Nums.bOpen:=False;
    NumPacket:=33;
    try
      if CommonSet.NoFis=0 then //���������� �����
      begin
        if pos('COM',CommonSet.PortCash)>0 then
        begin
          if CashOpen(PChar(CommonSet.PortCash)) then   //
          begin
            Memo1.Lines.Add('  �� ��.'); Delay(10);

            GetNums; //������ ����������  Create
            fGetFN; //������ �� ����������� ����������
          end else
          begin
            Memo1.Lines.Add('  ������ ��.'); Delay(10);
          end;
        end;
      end;
    except
    end;
    WriteStatus;
    
    Memo1.Lines.Add('������� ��������.'); Delay(10);
    cxButton1.Enabled:=True;
  end;
end;

procedure TfmMainCasher.TimerSaveScanTimer(Sender: TObject);
Var sStr,sStr1:String;
begin
  TimerSaveScan.Enabled:=False;
  sStr:=sScanN;
//  sStr1:=sScan;
  sScanN:='';

// if bStopAddPos then begin sScan:=''; exit; end; //����� ������ �� ���������� �� �����

  if fmBarMessage.Showing then begin sStr:=''; exit; end;
  if fmAttention.Showing then begin sStr:=''; exit; end;
  if fmCalcul.Showing then begin sStr:=''; exit; end;
  if fmCalc.Showing then begin sStr:=''; exit; end;

  if fmCash.Showing then
  begin
    if iStatus=1 then
    begin
      ToCustomDisp('      �������       ','    �� ������� !    ');
      fmCash.Close;
    end
    else begin sStr:=''; exit; end;
  end;

  ArBar[GetNumBar]:=sStr;
//  prWriteLog('&'+sStr+' '+its(iNumBar));

//  TimerScan.Enabled:=False; //������ �������
end;

end.


