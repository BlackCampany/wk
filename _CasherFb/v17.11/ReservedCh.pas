unit ReservedCh;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxCurrencyEdit, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxControls, cxGridCustomView, cxGrid,
  StdCtrls, cxButtons, ExtCtrls, ActnList, XPStyleActnCtrls, ActnMan;

type
  TfmReservedCh = class(TForm)
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    GrRes: TcxGrid;
    ViewRes: TcxGridDBTableView;
    ViewResNUMPOS: TcxGridDBColumn;
    ViewResARTICUL: TcxGridDBColumn;
    ViewResNAME: TcxGridDBColumn;
    ViewResQUANT: TcxGridDBColumn;
    ViewResPRICE: TcxGridDBColumn;
    ViewResRSUM: TcxGridDBColumn;
    LeRes: TcxGridLevel;
    ViewRH: TcxGridDBTableView;
    LeRH: TcxGridLevel;
    GrRH: TcxGrid;
    ViewRHCHECKNUM: TcxGridDBColumn;
    ViewRHRSUM: TcxGridDBColumn;
    amRet: TActionManager;
    acExit: TAction;
    procedure FormShow(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure ViewRHFocusedRecordChanged(Sender: TcxCustomGridTableView;
      APrevFocusedRecord, AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmReservedCh: TfmReservedCh;

implementation

uses Dm;

{$R *.dfm}

procedure TfmReservedCh.FormShow(Sender: TObject);
begin
  GrRH.SetFocus;
end;

procedure TfmReservedCh.acExitExecute(Sender: TObject);
begin
  ModalResult:=mrCancel;
end;

procedure TfmReservedCh.ViewRHFocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord;
  ANewItemRecordFocusingChanged: Boolean);
begin
  with dmC do
  begin
    fmReservedCh.ViewRes.BeginUpdate;
    if quResChH.RecordCount>0 then
    begin
      quReservCh.Active:=False;
      quReservCh.ParamByName('CASHNUM').AsInteger:=quResChHCASHNUM.AsInteger;
      quReservCh.ParamByName('ZNUM').AsInteger:=quResChHZNUM.AsInteger;
      quReservCh.ParamByName('CHECKNUM').AsInteger:=quResChHCHECKNUM.AsInteger;
      quReservCh.Active:=True;
    end;
    fmReservedCh.ViewRes.EndUpdate;
  end;
end;

end.
