// JCL_DEBUG_EXPERT_GENERATEJDBG OFF
// JCL_DEBUG_EXPERT_INSERTJDBG OFF
// JCL_DEBUG_EXPERT_DELETEMAPFILE OFF
program Casher;

uses
  Forms,
  MainCasher in 'MainCasher.pas' {fmMainCasher},
  Dm in 'Dm.pas' {dmC: TDataModule},
  Un1 in 'Un1.pas',
  uPre in 'uPre.pas' {fmPre},
  CashEnd in 'CashEnd.pas' {fmCash},
  Calc in 'Calc.pas' {fmCalc},
  CardsFind in 'CardsFind.pas' {fmCardsFind},
  u2fdk in 'U2FDK.PAS',
  UnCash in 'UnCash.pas',
  BarMessage in 'BarMessage.pas' {fmBarMessage},
  Attention in 'Attention.pas' {fmAttention},
  Calcul in 'Calcul.pas' {fmCalcul},
  CredCards in 'CredCards.pas' {fmCredCards},
  UnInOutM in 'UnInOutM.pas' {fmInOutM},
  UnitBN in 'UnitBN.pas',
  EnterPassw in 'EnterPassw.pas' {fmEnterPassw},
  BnSber in 'BnSber.pas' {fmSber},
  St in 'St.pas' {fmSt},
  uDBTr in 'uDBTr.pas' {dmTr: TDataModule},
  Trans2 in 'Trans2.pas' {fmTrans2},
  uDBFB in 'uDBFB.pas' {dmFB: TDataModule},
  Shutd in 'Shutd.pas' {fmShutd},
  RetCheck in 'RetCheck.pas' {fmRetCheck},
  ReservedCh in 'ReservedCh.pas' {fmReservedCh},
  UnPrizma in 'UnPrizma.pas',
  Bonus in 'Bonus.pas' {fmBonus},
  AutoUpd in 'AutoUpd.pas' {fmAutoUpd},
  AnnulsChecks in 'AnnulsChecks.pas' {fmAChecks},
  MessAlco in 'MessAlco.pas' {fmMessAlco},
  MessOff in 'MessOff.pas' {fmMessOff},
  MessBN in 'MessBN.pas' {fmMessBn},
  TestVes in 'TestVes.pas' {fmTestVes},
  Vtb24un in 'Vtb24un.pas' {fmVtb},
  dbSql in 'dbSql.pas' {dmSQL},
  uTermElisey in 'uTermElisey.pas' {Service1: TService},
  Ping in 'Ping.pas',
  GetAMark in 'GetAMark.pas' {fmGetAM};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfmMainCasher, fmMainCasher);
  Application.CreateForm(TdmC, dmC);
  Application.CreateForm(TfmCash, fmCash);
  Application.CreateForm(TfmCalc, fmCalc);
  Application.CreateForm(TfmCardsFind, fmCardsFind);
  Application.CreateForm(TfmBarMessage, fmBarMessage);
  Application.CreateForm(TfmAttention, fmAttention);
  Application.CreateForm(TfmCalcul, fmCalcul);
  Application.CreateForm(TfmInOutM, fmInOutM);
  Application.CreateForm(TfmSber, fmSber);
  Application.CreateForm(TfmSt, fmSt);
  Application.CreateForm(TdmTr, dmTr);
  Application.CreateForm(TfmTrans2, fmTrans2);
  Application.CreateForm(TfmPre, fmPre);
  Application.CreateForm(TdmFB, dmFB);
  Application.CreateForm(TfmShutd, fmShutd);
  Application.CreateForm(TfmRetCheck, fmRetCheck);
  Application.CreateForm(TfmReservedCh, fmReservedCh);
  Application.CreateForm(TfmBonus, fmBonus);
  Application.CreateForm(TfmAutoUpd, fmAutoUpd);
  Application.CreateForm(TfmAChecks, fmAChecks);
  Application.CreateForm(TfmMessAlco, fmMessAlco);
  Application.CreateForm(TfmMessOff, fmMessOff);
  Application.CreateForm(TfmMessBn, fmMessBn);
  Application.CreateForm(TfmTestVes, fmTestVes);
  Application.CreateForm(TfmVtb, fmVtb);
  Application.CreateForm(TdmSQL, dmSQL);
  Application.CreateForm(TService1, Service1);
  Application.CreateForm(TfmGetAM, fmGetAM);
  Application.Run;
end.
