unit BnSber;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxControls, cxContainer, cxEdit, cxTextEdit, cxMemo, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons,
  ComObj, ActiveX, OleServer, cxCurrencyEdit, dxfBackGround, cxMaskEdit,
  cxSpinEdit, ActnList, XPStyleActnCtrls, ActnMan, ExtCtrls;

type
  TfmSber = class(TForm)
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    cxButton5: TcxButton;
    cxButton6: TcxButton;
    cxButton7: TcxButton;
    dxfBackGround1: TdxfBackGround;
    cxButton9: TcxButton;
    cxButton10: TcxButton;
    cxButton11: TcxButton;
    cxButton12: TcxButton;
    Label1: TLabel;
    cxButton1: TcxButton;
    amBn: TActionManager;
    acExit: TAction;
    Panel1: TPanel;
    Memo1: TMemo;
    cxButton8: TcxButton;
    cxButton13: TcxButton;
    procedure FormShow(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
    procedure cxButton7Click(Sender: TObject);
    procedure cxButton9Click(Sender: TObject);
    procedure cxButton10Click(Sender: TObject);
    procedure cxButton11Click(Sender: TObject);
    procedure cxButton12Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure acMBCExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxButton8Click(Sender: TObject);
    procedure cxButton13Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function IsOLEObjectInstalled(Name: String): boolean;

var
  fmSber: TfmSber;
  Drv: OleVariant;
  vCh: Variant;
//  iMoneyBn:Integer;

implementation

uses Un1, UnCash, MessBN;

{$R *.dfm}


function IsOLEObjectInstalled(Name: String): boolean;
var
  ClassID: TCLSID;
  Rez : HRESULT;
begin

// ���� CLSID OLE-�������
  Rez := CLSIDFromProgID(PWideChar(WideString(Name)), ClassID);
  if Rez = S_OK then
 // ������ ������
    Result := true
  else
    Result := false;
end;


procedure TfmSber.FormShow(Sender: TObject);
Var tTime:TDateTime;
    StrT:String;
    StrWk:String;
begin
//�������
//  Memo1.Clear;
  Memo1.Lines.Add('');
  Memo1.Lines.Add('��������� ����������.');
  tTime:=now;

  if IsOLEObjectInstalled('SBRFSRV.Server') then
  begin
    tTime:=Now-tTime;
    StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
    Memo1.Lines.Add('������� ����������.'+StrT);
  end else
  begin
    tTime:=Now-tTime;
    StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
    Memo1.Lines.Add('������� �� ���������� !!!'+StrT);
    exit;
  end;

{  Memo1.Lines.Add('');
  Memo1.Lines.Add('������� ������ ��������.');
  tTime:=now;
  Drv := CreateOleObject('SBRFSRV.Server');
  }
//  Memo1.Lines.Add('');
  Memo1.Lines.Add('������������� Drv.');
  tTime:=now;
  Drv.Clear;
  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('��.'+StrT);
  Str((iMoneyBn/100):9:2,StrWk); StrWk:=TrimStr(StrWk);
  Label1.Caption:='����� � ������ '+StrWk+'�.';

  if SberTabStop=1 then cxButton2.SetFocus;

  if SberTabStop=2 then cxButton5.SetFocus;

  if SberTabStop=3 then cxButton9.SetFocus;

end;

procedure TfmSber.cxButton2Click(Sender: TObject);
Var tTime:TDateTime;
    StrT:String;
    iRes:Integer;
    StrWk:String;
begin
  Memo1.Lines.Add('');
  Memo1.Lines.Add('������ ��� (��������� ������) 4000');
  tTime:=now;

  Drv.Clear;
  Drv.SParam('Amount',iMoneyBn);

  iRes:=Drv.NFun(4000);
  
//  iRes:=0;

  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('����� : '+StrT);
  Memo1.Lines.Add('----��������� : '+IntToStr(iRes));

  StrWk:=Drv.GParam('ClientCard');
//  StrWk:='111222333444555';

  Memo1.Lines.Add('----����� ������� : '+StrWk);

  StrWk:=Drv.GParam('ClientExpiryDate');
//  StrWk:='01.01.2009';

  Memo1.Lines.Add('----���� ��������� �������� : '+StrWk);
  Memo1.Lines.Add('');
  Memo1.Lines.Add('���������� ���');

  vCh:=Drv.GParam('Cheque');
{  vCh:='       ������� "������� ���"'+chr($0D)+chr($0A)+'      ������,��.��������,�.19,      '+
'           ���. 123-4567            '+chr($0D)+chr($0A)+
'                                    '+chr($0D)+chr($0A)+
'                                    '+chr($0D)+chr($0A)+
'29.02.08                       09:12'+chr($0D)+chr($0A)+
'           ������ ������            '+chr($0D)+chr($0A)+
'��������:                   00001603'+chr($0D)+chr($0A)+
'����� ������������:     164444444455'+chr($0D)+chr($0A)+
'------------------------------------'+chr($0D)+chr($0A)+
'����� ������� � ����                '+chr($0D)+chr($0A)+
'------------------------------------'+chr($0D)+chr($0A)+
'------------------------------------'+chr($0D)+chr($0A)+
'*********  ����� ��������  *********'+chr($0D)+chr($0A)+
'===================================='+chr($0D)+chr($0A)+
'.'+chr($0D)+chr($0A)+
'.'+chr($0D)+chr($0A)+
'.'+chr($0D)+chr($0A)+
'.'+chr($0D)+chr($0A)+
'.'+chr($0D)+chr($0A)+
'.'+chr($0D)+chr($0A)+chr($01);
}
  BnStr:= vCh;

  Memo1.Lines.Add('--------------------------------------');
  if length(BnStr)>0 then
  begin
    Memo1.Lines.Add(BnStr);
  end;
  Memo1.Lines.Add('--------------------------------------');
  Memo1.Lines.Add('');
  Memo1.Lines.Add('');
  if iRes=0 then ModalResult:=mrOk;
end;

procedure TfmSber.cxButton4Click(Sender: TObject);
Var tTime:TDateTime;
    StrT:String;
    iRes:Integer;
    StrWk:String;
begin
  Memo1.Lines.Add('');
  Memo1.Lines.Add('������ ������� ������� 4006');
  tTime:=now;

  Drv.Clear;
  Drv.SParam('Amount',iMoneyBn);
//  Drv.SParam('CardType',0);
  iRes:=Drv.NFun(4006);
  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('����� : '+StrT);
  Memo1.Lines.Add('----��������� : '+IntToStr(iRes));

  StrWk:=Drv.GParam('ClientCard');
  Memo1.Lines.Add('----����� ������� : '+StrWk);

  StrWk:=Drv.GParam('ClientExpiryDate');
  Memo1.Lines.Add('----���� ��������� �������� : '+StrWk);
  Memo1.Lines.Add('');
  Memo1.Lines.Add('���������� ���');
  vCh:=Drv.GParam('Cheque');
  Memo1.Lines.Add('� ������');
  BnStr:= vCh;
  Memo1.Lines.Add('--------------------------------------');
  if length(BnStr)>0 then
  begin
    Memo1.Lines.Add(BnStr);
  end;
  Memo1.Lines.Add('--------------------------------------');
  Memo1.Lines.Add('');
  Memo1.Lines.Add('');
  if iRes=0 then ModalResult:=mrOk;
end;

procedure TfmSber.cxButton3Click(Sender: TObject);
Var tTime:TDateTime;
    StrT:String;
    iRes:Integer;
    StrWk:String;
begin
  Memo1.Lines.Add('');
  Memo1.Lines.Add('������ ��������   1000');
  tTime:=now;

  Drv.Clear;
  Drv.SParam('Amount',iMoneyBn);
//  Drv.SParam('CardType',0);
  iRes:=Drv.NFun(1000);
  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('����� : '+StrT);
  Memo1.Lines.Add('----��������� : '+IntToStr(iRes));

  StrWk:=Drv.GParam('ClientCard');
  Memo1.Lines.Add('----����� ������� : '+StrWk);

  StrWk:=Drv.GParam('ClientExpiryDate');
  Memo1.Lines.Add('----���� ��������� �������� : '+StrWk);
  Memo1.Lines.Add('');
  Memo1.Lines.Add('���������� ���');
  vCh:=Drv.GParam('Cheque');
//  Memo1.Lines.Add('� ������');
  BnStr:= vCh;
  Memo1.Lines.Add('--------------------------------------');
  if length(BnStr)>0 then
  begin
    Memo1.Lines.Add(BnStr);
  end;
  Memo1.Lines.Add('--------------------------------------');
  Memo1.Lines.Add('');
  Memo1.Lines.Add('');
  if iRes=0 then ModalResult:=mrOk;
end;

procedure TfmSber.cxButton5Click(Sender: TObject);
Var tTime:TDateTime;
    StrT:String;
    iRes:Integer;
    StrWk:String;
begin
  Memo1.Lines.Add('');
  Memo1.Lines.Add('������� ���   4002');
  tTime:=now;

  Drv.Clear;
  Drv.SParam('Amount',iMoneyBn);
//  Drv.SParam('CardType',0);
  iRes:=Drv.NFun(4002);
  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('����� : '+StrT);
  Memo1.Lines.Add('----��������� : '+IntToStr(iRes));

  StrWk:=Drv.GParam('ClientCard');
  Memo1.Lines.Add('----����� ������� : '+StrWk);

  StrWk:=Drv.GParam('ClientExpiryDate');
  Memo1.Lines.Add('----���� ��������� �������� : '+StrWk);
  Memo1.Lines.Add('');
  Memo1.Lines.Add('���������� ���');
  vCh:=Drv.GParam('Cheque');
  BnStr:= vCh;
  Memo1.Lines.Add('--------------------------------------');
  if length(StrWk)>0 then
  begin
    Memo1.Lines.Add(StrWk);
  end;
  Memo1.Lines.Add('--------------------------------------');
  Memo1.Lines.Add('');
  Memo1.Lines.Add('');
  if iRes=0 then ModalResult:=mrOk;
end;

procedure TfmSber.cxButton6Click(Sender: TObject);
Var tTime:TDateTime;
    StrT:String;
    iRes:Integer;
    StrWk:String;
begin
  Memo1.Lines.Add('');
  Memo1.Lines.Add('������ �������� �� ���   4003 ');
  tTime:=now;

  Drv.Clear;
  Drv.SParam('Amount',iMoneyBn);
//  Drv.SParam('CardType',0);
  iRes:=Drv.NFun(4003);
  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('����� : '+StrT);
  Memo1.Lines.Add('----��������� : '+IntToStr(iRes));

  StrWk:=Drv.GParam('ClientCard');
  Memo1.Lines.Add('----����� ������� : '+StrWk);

  StrWk:=Drv.GParam('ClientExpiryDate');
  Memo1.Lines.Add('----���� ��������� �������� : '+StrWk);
  Memo1.Lines.Add('');
  Memo1.Lines.Add('���������� ���');
  vCh:=Drv.GParam('Cheque');
  BnStr:=vCh;
  Memo1.Lines.Add('� ������');
  StrWk:= vCh;
  Memo1.Lines.Add('--------------------------------------');
  if length(StrWk)>0 then
  begin
    Memo1.Lines.Add(StrWk);
  end;
  Memo1.Lines.Add('--------------------------------------');
  Memo1.Lines.Add('');
  Memo1.Lines.Add('');
  prBnPrint;
end;

procedure TfmSber.cxButton7Click(Sender: TObject);
Var tTime:TDateTime;
    StrT:String;
    iRes:Integer;
    StrWk:String;
begin
  Memo1.Lines.Add('');
  Memo1.Lines.Add('������� �� ��������   1002 ');
  tTime:=now;

  Drv.Clear;
  Drv.SParam('Amount',iMoneyBn);
//  Drv.SParam('CardType',0);
  iRes:=Drv.NFun(1002);
  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('����� : '+StrT);
  Memo1.Lines.Add('----��������� : '+IntToStr(iRes));

  StrWk:=Drv.GParam('ClientCard');
  Memo1.Lines.Add('----����� ������� : '+StrWk);

  StrWk:=Drv.GParam('ClientExpiryDate');
  Memo1.Lines.Add('----���� ��������� �������� : '+StrWk);
  Memo1.Lines.Add('');
  Memo1.Lines.Add('���������� ���');
  vCh:=Drv.GParam('Cheque');
  BnStr:= vCh;
  Memo1.Lines.Add('--------------------------------------');
  if length(StrWk)>0 then
  begin
    Memo1.Lines.Add(StrWk);
  end;
  Memo1.Lines.Add('--------------------------------------');
  Memo1.Lines.Add('');
  Memo1.Lines.Add('');
  if iRes=0 then ModalResult:=mrOk;

end;


procedure TfmSber.cxButton9Click(Sender: TObject);
Var tTime:TDateTime;
    StrT:String;
    iRes:Integer;
    StrWk:String;
begin
  Memo1.Lines.Add('');
  Memo1.Lines.Add('�������� ��� �� ��������� � ����������� ������� (6000) ');
  tTime:=now;

  try
//    Memo1.Lines.Add('������ ������.');
    Drv.Clear;
    Memo1.Lines.Add('������� ������.');
    Drv := Unassigned;
    Delay(100);
    Memo1.Lines.Add('������� ������.');
    if IsOLEObjectInstalled('SBRFSRV.Server') then
    begin
      Memo1.Lines.Add('������� ����������.');

      Memo1.Lines.Add('������� ������ ��������.');
      Drv := CreateOleObject('SBRFSRV.Server');
      Drv.Clear;
      Memo1.Lines.Add('�������� ��.');
    end else Memo1.Lines.Add('������� �� ������.');
  except
  end;

  iRes:=Drv.NFun(6000);
  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('����� : '+StrT);
  Memo1.Lines.Add('----��������� : '+IntToStr(iRes));
  Memo1.Lines.Add('');
  Memo1.Lines.Add('���������� ���');
  vCh:=Drv.GParam('Cheque');
  Memo1.Lines.Add('� ������');
  BnStr:= vCh;
  Memo1.Lines.Add('--------------------------------------');
  if length(StrWk)>0 then
  begin
    Memo1.Lines.Add(StrWk);
  end;
  Memo1.Lines.Add('--------------------------------------');
  Memo1.Lines.Add('');
  Memo1.Lines.Add('');
  prBnPrint;
end;


procedure TfmSber.cxButton10Click(Sender: TObject);
Var tTime:TDateTime;
    StrT:String;
    iRes:Integer;
    StrWk:String;
begin
  Memo1.Lines.Add('');
  Memo1.Lines.Add('���������� ��������   3001 ');
  tTime:=now;

  Drv.Clear;
  iRes:=Drv.NFun(3001);
  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('����� : '+StrT);
  Memo1.Lines.Add('----��������� : '+IntToStr(iRes));
  Memo1.Lines.Add('');
  Memo1.Lines.Add('���������� ���');
  vCh:=Drv.GParam('Cheque');
  Memo1.Lines.Add('� ������');
  StrWk:= vCh;
  Memo1.Lines.Add('--------------------------------------');
  if length(StrWk)>0 then
  begin
    Memo1.Lines.Add(StrWk);
  end;
  Memo1.Lines.Add('--------------------------------------');
  Memo1.Lines.Add('');
  Memo1.Lines.Add('');
  prBnPrint;
end;


procedure TfmSber.cxButton11Click(Sender: TObject);
Var tTime:TDateTime;
    StrT:String;
    iRes:Integer;
    StrWk:String;
begin
  Memo1.Lines.Add('');
  Memo1.Lines.Add('������� �����    7000 ');
  tTime:=now;

  Drv.Clear;
  iRes:=Drv.NFun(7000);
  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('����� : '+StrT);
  Memo1.Lines.Add('----��������� : '+IntToStr(iRes));
  Memo1.Lines.Add('');
  Memo1.Lines.Add('���������� ���');
  vCh:=Drv.GParam('Cheque');
  Memo1.Lines.Add('� ������');
  StrWk:= vCh;
  Memo1.Lines.Add('--------------------------------------');
  if length(StrWk)>0 then
  begin
    Memo1.Lines.Add(StrWk);
  end;
  Memo1.Lines.Add('--------------------------------------');
  Memo1.Lines.Add('');
  Memo1.Lines.Add('');
  prBnPrint;
end;


procedure TfmSber.cxButton12Click(Sender: TObject);
Var tTime:TDateTime;
    StrT:String;
    iRes:Integer;
    StrWk:String;
begin
  Memo1.Lines.Add('');
  Memo1.Lines.Add('������ ����. ���������  7001 ');
  tTime:=now;

  Drv.Clear;
  iRes:=Drv.NFun(7001);
  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('����� : '+StrT);
  Memo1.Lines.Add('----��������� : '+IntToStr(iRes));
  Memo1.Lines.Add('');
  Memo1.Lines.Add('���������� ���');
  vCh:=Drv.GParam('Cheque');
  Memo1.Lines.Add('� ������');
  StrWk:= vCh;
  Memo1.Lines.Add('--------------------------------------');
  if length(StrWk)>0 then
  begin
    Memo1.Lines.Add(StrWk);
  end;
  Memo1.Lines.Add('--------------------------------------');
  Memo1.Lines.Add('');
  Memo1.Lines.Add('');
end;

procedure TfmSber.cxButton1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmSber.acExitExecute(Sender: TObject);
begin
  ModalResult:=mrCancel;
end;

procedure TfmSber.acMBCExecute(Sender: TObject);
begin
  cxButton2.Click;
end;

procedure TfmSber.FormCreate(Sender: TObject);
begin
  if CommonSet.BNManual=2 then
  begin
    if IsOLEObjectInstalled('SBRFSRV.Server') then
    begin
      Drv := CreateOleObject('SBRFSRV.Server');
    end else
    begin
      showmessage('������� ��������� (sbrf.dll) �� ����������.');
    end;
  end;  
end;

procedure TfmSber.cxButton8Click(Sender: TObject);
begin
  Randomize;
  iAAnsw:=Random(9);
  fmMessBn.Label3.Caption:='��� ������ �������';
  fmMessBn.Label4.Caption:=its(iAAnsw);
  fmMessBn.Label4.Visible:=True;
  fmMessBn.ShowModal;

  if MessageDlg('�� ��������� ������ ���� �������� ��� ����������� � �����?',mtConfirmation, [mbYes, mbNo], 0) = mrNo then
  begin
    BnStr:='������ �� ������� '+#13+'��� ����������� � �����';

    Memo1.Lines.Add('--------------------------------------');
    if length(BnStr)>0 then
    begin
      Memo1.Lines.Add(BnStr);
    end;
    Memo1.Lines.Add('--------------------------------------');
    Memo1.Lines.Add('');
    Memo1.Lines.Add('');
    ModalResult:=mrOk;
  end else ModalResult:=mrCancel;
end;

procedure TfmSber.cxButton13Click(Sender: TObject);
Var tTime:TDateTime;
    StrT:String;
    iRes:Integer;
    StrWk:String;
begin
  Memo1.Lines.Add('');
  Memo1.Lines.Add('������ ����. ����   7001 ');
  tTime:=now;

  Drv.Clear;
  iRes:=Drv.NFun(7001);
  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('����� : '+StrT);
  Memo1.Lines.Add('----��������� : '+IntToStr(iRes));

  StrWk:=Drv.GParam('ClientCard');
  Memo1.Lines.Add('----����� ������� : '+StrWk);

  StrWk:=Drv.GParam('ClientExpiryDate');
  Memo1.Lines.Add('----���� ��������� �������� : '+StrWk);
  Memo1.Lines.Add('');
  Memo1.Lines.Add('���������� ���');
  vCh:=Drv.GParam('Cheque');
  BnStr:=vCh;
  Memo1.Lines.Add('� ������');
  StrWk:= vCh;
  Memo1.Lines.Add('--------------------------------------');
  if length(StrWk)>0 then
  begin
    Memo1.Lines.Add(StrWk);
  end;
  Memo1.Lines.Add('--------------------------------------');
  Memo1.Lines.Add('');
  Memo1.Lines.Add('');
  prBnPrint;
end;

end.
