unit uDBFB;

interface

uses
  SysUtils, Classes, FIBDatabase, pFIBDatabase, FIBQuery, pFIBQuery,
  pFIBStoredProc, DB, FIBDataSet, pFIBDataSet;

type
  TdmFB = class(TDataModule)
    Cash: TpFIBDatabase;
    trSelectCash: TpFIBTransaction;
    trUpdateCash: TpFIBTransaction;
    prAddCashP: TpFIBStoredProc;
    prAddCashS: TpFIBStoredProc;
    prDelCash: TpFIBStoredProc;
    trDelCash: TpFIBTransaction;
    prDelCashZ: TpFIBStoredProc;
    quSelLoad: TpFIBDataSet;
    quSetActive: TpFIBQuery;
    quSelLoadID: TFIBIntegerField;
    quSelLoadIACTIVE: TFIBSmallIntField;
    quSelLoadITYPE: TFIBSmallIntField;
    quSelLoadARTICUL: TFIBIntegerField;
    quSelLoadBARCODE: TFIBStringField;
    quSelLoadCARDSIZE: TFIBStringField;
    quSelLoadQUANT: TFIBFloatField;
    quSelLoadDSTOP: TFIBFloatField;
    quSelLoadNAME: TFIBStringField;
    quSelLoadMESSUR: TFIBStringField;
    quSelLoadPRESISION: TFIBFloatField;
    quSelLoadADD1: TFIBStringField;
    quSelLoadADD2: TFIBStringField;
    quSelLoadADD3: TFIBStringField;
    quSelLoadADDNUM1: TFIBFloatField;
    quSelLoadADDNUM2: TFIBFloatField;
    quSelLoadADDNUM3: TFIBFloatField;
    quSelLoadSCALE: TFIBStringField;
    quSelLoadGROUP1: TFIBIntegerField;
    quSelLoadGROUP2: TFIBIntegerField;
    quSelLoadGROUP3: TFIBIntegerField;
    quSelLoadPRICE: TFIBFloatField;
    quSelLoadCLIINDEX: TFIBIntegerField;
    quSelLoadDELETED: TFIBSmallIntField;
    quSelLoadPASSW: TFIBStringField;
    quUpd2: TpFIBQuery;
    trUpd2: TpFIBTransaction;
    quTabDBList2: TpFIBDataSet;
    quTabDBList2RDBRELATION_NAME: TFIBWideStringField;
    quTabDBList2RDBFIELD_POSITION: TFIBSmallIntField;
    quTabDBList2RDBFIELD_NAME: TFIBWideStringField;
    quZListDet2: TpFIBDataSet;
    quZListDet2CASHNUM: TFIBSmallIntField;
    quZListDet2ZNUM: TFIBIntegerField;
    quZListDet2INOUT: TFIBSmallIntField;
    quZListDet2ITYPE: TFIBSmallIntField;
    quZListDet2RSUM: TFIBFloatField;
    quZListDet2IDATE: TFIBIntegerField;
    quZListDet2DDATE: TFIBDateTimeField;
    quAUpd: TpFIBDataSet;
    quAUpdID: TFIBIntegerField;
    quAUpdSVER: TFIBStringField;
    quAUpdTIME_VER: TFIBDateTimeField;
    quAUpdCASH_BIN: TFIBBlobField;
    quAUpdUPD_TYPE: TFIBSmallIntField;
    quAUpdILOADER: TFIBSmallIntField;
    quAUpdLOADER: TFIBBlobField;
    quXCheck: TpFIBDataSet;
    quXCheckBARCH: TFIBStringField;
    quXCheckINUM: TFIBIntegerField;
    quXCheckICODE: TFIBIntegerField;
    quXCheckQUANT: TFIBFloatField;
    quXCheckPRICE: TFIBFloatField;
    quXCheckRSUM: TFIBFloatField;
    quXCheckXDATE: TFIBDateTimeField;
    quXCheckBARCODE: TFIBStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

Procedure prUpd2; //���������� 1
function fNeedUpd:Boolean; //����� �� ����������
function fTakeUpd:Boolean; //���������� ���������



var
  dmFB: TdmFB;
  sCurVer: String;

implementation

uses Un1;

{$R *.dfm}

function fTakeUpd:Boolean; //���������� ���������
var sNVer:String;
    f:File;
begin
  Result:=False;
  with dmFB do
  begin
    try
      Cash.Close;
      Cash.DBName:=CommonSet.CashDB;
      Cash.Open;
      if Cash.Connected then //������������ � ���� ������
      begin
        quAUpd.Active:=False;
        quAUpd.Active:=True;
        if quAUpd.RecordCount>0 then
        begin
          sNVer:=quAUpdSVER.AsString;
          while pos(' ',StrWk)>0 do delete(StrWk,pos(' ',StrWk),1);
          if sNVer>sCurVer then
          begin
            if quAUpdILOADER.AsInteger=1 then
            begin
              try
                //������ �������
                if FileExists(curdir+'Reloader.exe') then
                begin
                  assignfile(f,curdir+'Reloader.exe');
                  Erase(f);

                  if fileexists(CurDir+'Reloader.exe') then DeleteFile(CurDir+'Reloader.exe');
                end;

                delay(20);

                quAUpdLOADER.SaveToFile(curdir+'Reloader.exe');
                result:=True;
              except
                result:=False;
              end;
            end;
            if Result=True then
            begin
              try
                //������ ������� ���������
                if FileExists(curdir+'casher.new') then
                begin
                  assignfile(f,curdir+'casher.new');
                  Erase(f);
                  delay(100);

                  if fileexists(CurDir+'casher.new') then DeleteFile(CurDir+'casher.new');

                end;

                delay(20);

                quAUpdCASH_BIN.SaveToFile(curdir+'casher.new');
              except
                result:=False;
              end;
            end;
          end;
        end;
        quAUpd.Active:=False;
      end;
      Cash.Close;
    except
      Cash.Close;
    end;
  end;
end;


function fNeedUpd:Boolean; //����� �� ����������
var sNVer:String;
begin
  Result:=False;
  if CommonSet.CashDB='' then exit;
  with dmFB do
  begin
    try
      Cash.Close;
      Cash.DBName:=CommonSet.CashDB;
      Cash.Open;
      if Cash.Connected then //������������ � ���� ������
      begin
        quAUpd.Active:=False;
        quAUpd.Active:=True;
        if quAUpd.RecordCount>0 then
        begin
          sNVer:=quAUpdSVER.AsString;
          while pos(' ',StrWk)>0 do delete(StrWk,pos(' ',StrWk),1);
          if sNVer>sCurVer then Result:=True;
        end;
        quAUpd.Active:=False;
      end;
      Cash.Close;
    except
      Cash.Close;
    end;
  end;
end;

Procedure prUpd2; //���������� PosFb
begin
  with dmFB do
  begin
    quTabDBList2.Active:=False;
    quTabDBList2.ParamByName('STABNAME').AsString:='ZLISTDET';
    quTabDBList2.Active:=True;
    if quTabDBList2.RecordCount=0 then
    begin //����� ��������� �������� ZLISTDET
      quUpd2.SQL.Clear;
      quUpd2.SQL.Add('CREATE TABLE ZLISTDET (');
      quUpd2.SQL.Add('CASHNUM SMALLINT NOT NULL,');
      quUpd2.SQL.Add('ZNUM INTEGER NOT NULL,');
      quUpd2.SQL.Add('INOUT SMALLINT NOT NULL,');
      quUpd2.SQL.Add('ITYPE SMALLINT NOT NULL,');
      quUpd2.SQL.Add('RSUM DOUBLE PRECISION,');
      quUpd2.SQL.Add('IDATE INTEGER,');
      quUpd2.SQL.Add('DDATE TIMESTAMP);');
      quUpd2.ExecQuery;
      delay(100);

      quUpd2.SQL.Clear;
      quUpd2.SQL.Add('ALTER TABLE ZLISTDET ADD CONSTRAINT PK_ZLISTDET PRIMARY KEY (CASHNUM, ZNUM, INOUT, ITYPE);');
      quUpd2.ExecQuery;

      delay(100);
    end;
    quTabDBList2.Active:=False;
  end;
end;


end.
