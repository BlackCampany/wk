program SQLConv;

uses
  Forms,
  MainSQLConv in 'MainSQLConv.pas' {fmMainSQLConv},
  Dm in 'Dm.pas' {dmC: TDataModule},
  HistoryTr in 'HistoryTr.pas' {fmHistoryTr},
  Un1 in 'Un1.pas',
  uPosDb in 'uPosDb.pas' {dmPOSDB: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'SQL ���������';
  Application.CreateForm(TfmMainSQLConv, fmMainSQLConv);
  Application.CreateForm(TdmC, dmC);
  Application.CreateForm(TfmHistoryTr, fmHistoryTr);
  Application.CreateForm(TdmPOSDB, dmPOSDB);
  Application.ShowMainForm:=FALSE;
  Application.Run;
end.
