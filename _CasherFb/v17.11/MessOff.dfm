object fmMessOff: TfmMessOff
  Left = 389
  Top = 520
  BorderStyle = bsDialog
  ClientHeight = 225
  ClientWidth = 371
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 116
    Top = 20
    Width = 123
    Height = 24
    Caption = #1042#1085#1080#1084#1072#1085#1080#1077' !!!'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 20
    Top = 60
    Width = 335
    Height = 77
    Alignment = taCenter
    AutoSize = False
    Caption = 
      #1044#1083#1103' '#1087#1088#1086#1074#1077#1076#1077#1085#1080#1103' '#1085#1086#1095#1100#1102' '#1090#1077#1093#1085#1086#1083#1086#1075#1080#1095#1077#1089#1082#1080#1093' '#1087#1088#1086#1094#1077#1076#1091#1088', '#1086#1089#1090#1072#1074#1100#1090#1077' '#1082#1072#1089#1089#1091' '#1074#1086 +
      ' '#1074#1082#1083#1102#1095#1077#1085#1085#1086#1084' '#1089#1086#1089#1090#1086#1103#1085#1080#1080' !!!'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    WordWrap = True
  end
  object Panel1: TPanel
    Left = 0
    Top = 166
    Width = 371
    Height = 59
    Align = alBottom
    BevelInner = bvLowered
    Color = 6832674
    TabOrder = 0
    object cxButton2: TcxButton
      Left = 208
      Top = 13
      Width = 101
      Height = 32
      Caption = #1042#1099#1082#1083#1102#1095#1080#1090#1100' '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ModalResult = 1
      ParentFont = False
      TabOrder = 0
      Colors.Default = 14930628
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      Layout = blGlyphTop
      LookAndFeel.Kind = lfFlat
    end
    object cxButton1: TcxButton
      Left = 48
      Top = 13
      Width = 101
      Height = 32
      Caption = #1054#1090#1084#1077#1085#1072
      Default = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ModalResult = 2
      ParentFont = False
      TabOrder = 1
      Colors.Default = 14930628
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      Layout = blGlyphTop
      LookAndFeel.Kind = lfFlat
    end
  end
end
