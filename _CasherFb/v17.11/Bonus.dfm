object fmBonus: TfmBonus
  Left = 494
  Top = 224
  BorderStyle = bsSingle
  Caption = #1041#1086#1085#1091#1089
  ClientHeight = 228
  ClientWidth = 389
  Color = 13473127
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 4
    Top = 4
    Width = 381
    Height = 221
    BevelInner = bvLowered
    Color = 6832674
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 28
      Top = 144
      Width = 137
      Height = 61
      Caption = #1044#1072
      Default = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -21
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ModalResult = 1
      ParentFont = False
      TabOrder = 0
      TabStop = False
      Colors.Default = 14930628
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      Layout = blGlyphTop
      LookAndFeel.Kind = lfFlat
    end
    object cxButton3: TcxButton
      Left = 208
      Top = 144
      Width = 137
      Height = 61
      Caption = #1053#1077#1090
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -21
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ModalResult = 2
      ParentFont = False
      TabOrder = 1
      TabStop = False
      Colors.Default = 14930628
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      Layout = blGlyphTop
      LookAndFeel.Kind = lfFlat
    end
    object Panel2: TPanel
      Left = 8
      Top = 8
      Width = 361
      Height = 121
      BevelInner = bvLowered
      Color = 16771797
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      object cxLabel1: TcxLabel
        Left = 2
        Top = 2
        Align = alClient
        AutoSize = False
        Caption = 
          #1057#1091#1084#1084#1072' '#1095#1077#1082#1072' '#1087#1088#1077#1074#1099#1096#1072#1077#1090' 2500 '#1088#1091#1073#1083#1077#1081' , '#1087#1086#1082#1091#1087#1072#1090#1077#1083#1102' '#1074#1099#1076#1072#1085#1072' '#1079#1086#1083#1086#1090#1072#1103' '#1082#1072#1088 +
          #1090#1072'?'
        Properties.Alignment.Horz = taCenter
        Properties.Alignment.Vert = taVCenter
        Properties.WordWrap = True
        Height = 117
        Width = 357
      end
    end
  end
end
