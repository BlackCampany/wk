unit CashEnd;

interface


uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxLookAndFeelPainters, StdCtrls, cxButtons, cxControls,
  cxContainer, cxEdit, cxTextEdit, cxCurrencyEdit, ExtCtrls, Placemnt,
  ActnList, XPStyleActnCtrls, ActnMan, cxStyles, cxCustomData, cxGraphics,
  cxFilter, cxData, DB, cxDBData, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  DBClient, cxDataStorage, Menus, cxMaskEdit, cxDropDownEdit, cxCalc,
  cxLabel;

type
  TfmCash = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    cxCurrencyEdit1: TcxCurrencyEdit;
    Label2: TLabel;
    cxCurrencyEdit2: TcxCurrencyEdit;
    Label3: TLabel;
    cxCurrencyEdit3: TcxCurrencyEdit;
    Label4: TLabel;
    Timer1: TTimer;
    Panel2: TPanel;
    Label5: TLabel;
    Panel3: TPanel;
    Panel4: TPanel;
    am2: TActionManager;
    acExit: TAction;
    acCalcN: TAction;
    Edit1: TEdit;
    acBarC: TAction;
    PayView: TcxGridDBTableView;
    PayLevel: TcxGridLevel;
    GridPay: TcxGrid;
    taPay: TClientDataSet;
    taPayId: TSmallintField;
    taPayTypePl: TSmallintField;
    taPayNamePl: TStringField;
    taPaySumPl: TCurrencyField;
    taPaySumOst: TCurrencyField;
    dsPay: TDataSource;
    PayViewId: TcxGridDBColumn;
    PayViewTypePl: TcxGridDBColumn;
    PayViewNamePl: TcxGridDBColumn;
    PayViewSumPl: TcxGridDBColumn;
    PayViewSumOst: TcxGridDBColumn;
    acCalcBN: TAction;
    taPayVidPl: TSmallintField;
    taPayComment: TStringField;
    Panel5: TPanel;
    Label9: TLabel;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    cxButton5: TcxButton;
    cxButton6: TcxButton;
    cxButton7: TcxButton;
    cxButton8: TcxButton;
    cxButton9: TcxButton;
    cxButton10: TcxButton;
    cxButton11: TcxButton;
    cxButton12: TcxButton;
    cxButton13: TcxButton;
    cxButton14: TcxButton;
    cxButton15: TcxButton;
    CalcEdit1: TcxCalcEdit;
    cxButton1: TcxButton;
    cxButton16: TcxButton;
    cxButton17: TcxButton;
    cxLabel1: TcxLabel;
    cxCurrencyEdit10: TcxCurrencyEdit;
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure acCalcNExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure acBarCExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acCalcBNExecute(Sender: TObject);
    procedure cxCurrencyEdit2KeyPress(Sender: TObject; var Key: Char);
    procedure cxCurrencyEdit1KeyPress(Sender: TObject; var Key: Char);
    procedure cxButton11Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
    procedure cxButton7Click(Sender: TObject);
    procedure cxButton8Click(Sender: TObject);
    procedure cxButton9Click(Sender: TObject);
    procedure cxButton10Click(Sender: TObject);
    procedure cxButton12Click(Sender: TObject);
    procedure cxButton14Click(Sender: TObject);
    procedure cxButton13Click(Sender: TObject);
    procedure cxButton15Click(Sender: TObject);
  private
    { Private declarations }
  public
    Procedure prSaveCheck;
    { Public declarations }
  end;

procedure prAddNum(iNum:INteger);
Procedure fToCurPos1;
procedure prPrintQRCode;
function prRecalcDiscPrice:Integer;

var
  fmCash: TfmCash;
  MessClear:Boolean = False;
  iStatus:SmallINt;
  bFirst:Boolean = False;

implementation

uses Un1, MainCasher, UnCash, Dm, Attention, CredCards, UnitBN, BnSber,Vtb24un,EliseyQr;

{$R *.dfm}

function prRecalcDiscPrice:Integer;
var iDopD:Integer;
    iSumPos:Integer;
    rPrice,rSumPos:Real;
    iSumCh:Integer;
    iDiscInPrice:Integer;
    iDiscPos:Integer;
begin
  //������������� ��� ������� ���� - ������ ���������� � ����
  //��� � ������
  //  fmMainCasher.ViewCh.BeginUpdate;
  //  dsCheck.DataSet:=nil;
  iDiscInPrice:=0;

  iDopD:=RoundEx(rDiscDop*100);
  iSumCh:=0;
  with dmC do
  begin
    trUpdCh.StartTransaction;

    taCheck.First;
    while not taCheck.Eof do
    begin
      iDiscPos:=RoundEx(RV(taCheckDSUM.AsFloat)*100);

      rSumPos:=RV(taCheckRSUM.AsFloat-iDiscPos/100);
      iSumPos:=RoundEx(rSumPos*100);

      if iDopD>0 then
        if (iSumPos-iDopD)>=300 then begin iSumPos:=iSumPos-iDopD; iDiscPos:=iDiscPos+iDopD; iDopD:=0;end;
      rPrice:=iSumPos/100/taCheckQUANT.AsFloat;
      iSumCh:=iSumCh+iSumPos;

      iDiscInPrice:=iDiscInPrice+iDiscPos;

      taCheck.Edit;
      taCheckDSUM.AsFloat:=iDiscPos/100;
      taCheckPRICE.AsFloat:=rPrice;
      taCheckRSUM.AsFloat:=(iSumPos/100);
      taCheck.Post;

      taCheck.Next;
    end;
    trUpdCh.Commit;
  end;
  rDiscDop:=0;
  rDiscont:=0;
  rCheckSum:=iSumCh/100;
  Result:=iDiscInPrice;
end;

procedure prPrintQRCode;
var bNotErr:Boolean;
    StrWk:string;
    iLen:Integer;
    iBmpW,iBmpH:Integer;
    EliseyQRCODE:TEliseyQRCODE;
    FName:string;
begin
  if Length(AlcoSet.CRCODE)>10  then  //���-�� ���� � ������ -  ���������
  begin
    try
      bNotErr:=True;
      iLen:=40;

      if OpenNFDoc=false then
      begin
        TestFp('PrintNFStr');
        OpenNFDoc;
      end;
      SelectF(10);
      if bNotErr then bNotErr:=PrintNFStr(AlcoSet.NameOrg) else TestFp('PrintNFStr');
      if bNotErr then bNotErr:=PrintNFStr('���: '+AlcoSet.INN+'          ���:'+AlcoSet.KPP) else TestFp('PrintNFStr');
      if bNotErr then bNotErr:=PrintNFStr('�����: '+Nums.RegNum+'    �����: '+its(Nums.ZNum)) else TestFp('PrintNFStr');
      if bNotErr then bNotErr:=PrintNFStr('���: '+its(Nums.iCheckNum)+'        ����: '+Formatdatetime('dd.mm.yyyy hh:nn',now)) else TestFp('PrintNFStr');

      //������ QR ����
      if AlcoSet.PRINTQR=1 then
      begin
        if CommonSet.TypeFis='pirit' then
        begin
          PrintQR(AlcoSet.URL);
        end else
        begin
          iBmpH:=140;
          iBmpW:=576;

          if AlcoSet.QRTYPE=2 then iBmpW:=448;
          try

            FName:=AlcoSet.PathAlco+FormatDateTime('yy_mm_dd-',date)+ Its(Nums.ZNum)+'-'+its(Nums.iCheckNum)+'.bmp';
            EliseyQRCODE:=TEliseyQRCODE.Create(AlcoSet.URL,iBmpW,iBmpH);
            EliseyQRCODE.SaveToFile(FName);

          finally
            EliseyQRCODE.Free;
          end;

  //        FName:=AlcoSet.PathAlco+'16_06_20-1574-3_1.bmp';

          if FileExists(FName) then
          begin
            PrintQR(FName);
            try
              DeleteFile(FName);
            except
            end;
          end;
        end;
      end;

//      if bNotErr then bNotErr:=PrintNFStr('') else TestFp('PrintNFStr');
//      if bNotErr then bNotErr:=PrintNFStr('') else TestFp('PrintNFStr');
//      if bNotErr then bNotErr:=PrintNFStr('') else TestFp('PrintNFStr');

      StrWk:=AlcoSet.URL;
      while StrWk>'' do
      begin
        if Length(StrWk)>iLen then
        begin
          PrintNFStr(Copy(StrWk,1,iLen));
          Delete(StrWk,1,iLen);
        end else
        begin
          PrintNFStr(StrWk);
          StrWk:='';
        end;
      end;

      if bNotErr then bNotErr:=PrintNFStr('') else TestFp('PrintNFStr');
      if bNotErr then bNotErr:=PrintNFStr('') else TestFp('PrintNFStr');

      StrWk:=AlcoSet.CRCODE;
      while StrWk>'' do
      begin
        if Length(StrWk)>iLen then
        begin
          PrintNFStr(Copy(StrWk,1,iLen));
          Delete(StrWk,1,iLen);
        end else
        begin
          PrintNFStr(StrWk);
          StrWk:='';
        end;
      end;

    finally
      if CloseNFDoc=False then
      begin
        TestFp('PrintNFStr');
        CloseNFDoc;
      end;
      delay(33);
    end;
  end;
end;


Procedure fToCurPos1;
begin
  with dmC do
  begin
    CurPos.ARTICUL:=taCheckARTICUL.AsString;
    CurPos.NAME:=taCheckNAME.AsString;
    CurPos.QUANT:=taCheckQUANT.AsFloat;
    CurPos.PRICE:=taCheckPRICE.AsFloat;
    CurPos.DPROC:=taCheckDPROC.AsFloat;
    CurPos.DSUM:=taCheckDSUM.AsFloat+rDiscDop;
    CurPos.DEPART:=taCheckDEPART.AsInteger;
    CurPos.BAR:=taCheckRBAR.AsString;
    CurPos.NUMPOS:=taCheckNUMPOS.AsInteger;
  end;
end;


procedure prAddNum(iNum:INteger);
begin
  with fmCash do
  begin
    if bFirst then CalcEdit1.Value:=iNum
    else
      if pos(',',CalcEdit1.Text)=0 then CalcEdit1.EditValue:=CalcEdit1.EditValue*10+iNum
      else CalcEdit1.Text:=CalcEdit1.Text+its(iNum);
    bFirst:=False;
  end;
end;

Procedure TfmCash.prSaveCheck;
type TCurPos = record
     CASHNUM:Integer;
     NUMPOS:Integer;
     ARTICUL:String;
     NAME:String;
     QUANT:Real;
     PRICE:Real;
     DPROC:Real;
     DSUM:Real;
     RSUM:Real;
     DEPART:SmallInt;
     RBAR:String;
     FULLSUM:Real;
     FULLQUANT:Real;
     FULLDSUM:Real;
     FULLSUMPL:Real;
     TIMEPOS:TDateTime;
     AMARK:string;
     end;

Var rSumPl:Real;
    CurPos:TCurPos;
    iMax:Integer;
    rK,rQuant,rSumD,rSumPay:Real;
    sErr:String;
    iNumPosMinus:Integer;

Procedure fToCurPos;
begin
  with dmC do
  begin
    CurPos.ARTICUL:=taCheckARTICUL.AsString;
    CurPos.NAME:=taCheckNAME.AsString;
    CurPos.QUANT:=taCheckQUANT.AsFloat;
    CurPos.DPROC:=taCheckDPROC.AsFloat;
    CurPos.DSUM:=taCheckDSUM.AsFloat+rDiscDop;
    CurPos.RSUM:=taCheckRSUM.AsFloat;
    CurPos.DEPART:=taCheckDEPART.AsInteger;
    CurPos.RBAR:=taCheckRBAR.AsString;
    CurPos.FULLQUANT:=taCheckQUANT.AsFloat;
    CurPos.FULLDSUM:=taCheckDSUM.AsFloat+rDiscDop;

    if CommonSet.DiscToPrice=1 then
    begin    //����� ��� �� �������
      CurPos.PRICE:=rv((taCheckRSUM.AsFloat+taCheckDSUM.AsFloat)/taCheckQUANT.AsFloat);
      CurPos.FULLSUM:=taCheckRSUM.AsFloat;
      CurPos.FULLSUMPL:=taCheckRSUM.AsFloat;
    end else
    begin
      CurPos.PRICE:=taCheckPRICE.AsFloat;
      CurPos.FULLSUM:=taCheckRSUM.AsFloat-taCheckDSUM.AsFloat-rDiscDop;
      CurPos.FULLSUMPL:=taCheckRSUM.AsFloat-taCheckDSUM.AsFloat-rDiscDop;
    end;

    CurPos.TIMEPOS:=taCheckTIMEPOS.AsDateTime;
    CurPos.NUMPOS:=taCheckNUMPOS.AsInteger;
    CurPos.AMARK:=taCheckAMARK.AsString;
    rDiscDop:=0;
  end;
end;

begin
  with dmC do
  begin
    //������ ������������ ��������
    sSave:=sSave+'4';
    prClearCheckTr.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
    prClearCheckTr.ExecProc;
    sSave:=sSave+'5';

    //� ���� ����� ������������� ������� � �� ���-�� � ����� - ��� �����������
    // ������� ��������� �� ���� �������

{    quCheck.Active:=False;
    quCheck.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
    quCheck.Active:=True;}

    if taCheck.Active=False then
    begin
      taCheck.Active:=False;
      taCheck.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
      taCheck.Active:=True;
    end;

    sSave:=sSave+'6';

    taCheckTr.Active:=False;
    taCheckTr.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
    taCheckTr.Active:=True;
    sSave:=sSave+'7';

    iMax:=1;
    rSumPay:=0;
    fmMainCasher.ViewCh.BeginUpdate;

//    PayView.BeginUpdate;

    trUpdCh.StartTransaction;
    taCheck.First;
    //��� ������������� ���-�� ����� �������
    while not taCheck.Eof do
    begin
      if taCheckQUANT.AsFloat<0 then
      begin
        if taCheckNUMPOS.AsInteger>=0 then
        begin
          iNumPosMinus:=taCheckNUMPOS.AsInteger*(-1);

          taCheck.Edit;
          taCheckNUMPOS.AsInteger:=iNumPosMinus;
          taCheck.Post;
        end;
      end;
      taCheck.Next;
    end;
    trUpdCh.Commit;

    taCheck.FullRefresh;

    trUpdChTr.StartTransaction;

    sSave:=sSave+'8';

    taCheck.First;
    fToCurPos; //�������� ������� ������� � ���������� - ���������
    sSave:=sSave+'9';
    taPay.First;
    while not taPay.Eof do
    begin
      rSumPay:=rSumPay+taPaySumPl.AsFloat;
      rSumPl:=rv(taPaySumPl.AsFloat);
      while (rSumPl>=0) and (taCheck.Eof=False) do
      begin
        if rSumPl>=(CurPos.FULLSUMPL) then //��� ����. ����� ������� �� �������
        begin
          //��������� � ���. ��������
//          if CurPos.FULLSUMPL>0.001 then
//          begin
            taCheckTr.Append;
            taCheckTrCASHNUM.AsInteger:=CommonSet.CashNum;
//            taCheckTrNUMPOS.AsInteger:=iMax;

            taCheckTrNUMPOS.AsInteger:=abs(CurPos.NUMPOS);
            taCheckTrARTICUL.AsString:=CurPos.ARTICUL;
            taCheckTrNAME.AsString:=CurPos.NAME;
            taCheckTrQUANT.AsFloat:=CurPos.QUANT;
            taCheckTrPRICE.AsFloat:=CurPos.PRICE;
            taCheckTrDPROC.AsFloat:=CurPos.DPROC;
            taCheckTrDSUM.AsFloat:=CurPos.DSUM;
            taCheckTrRSUM.AsFloat:=CurPos.FULLSUMPL;
            taCheckTrDEPART.AsInteger:=CurPos.DEPART;
            taCheckTrRBAR.AsString:=CurPos.RBAR;
            taCheckTrTYPEP.AsInteger:=taPayTypePl.AsInteger;
            taCheckTrVIDP.AsInteger:=taPayVidPl.AsInteger;
            taCheckTrTIMEPOS.AsDateTime:=CurPos.TIMEPOS;
            taCheckTrAMARK.AsString:=CurPos.AMARK;

            if taPayTypePl.AsInteger=0 then //���
            begin
              if Check.Operation=1 then taCheckTrOPERATION.AsInteger:=1; //�������
              if Check.Operation=0 then taCheckTrOPERATION.AsInteger:=0; //�������
            end;

            if taPayTypePl.AsInteger=1 then //������
            begin
              if Check.Operation=1 then taCheckTrOPERATION.AsInteger:=3; //�������
              if Check.Operation=0 then taCheckTrOPERATION.AsInteger:=2; //�������
            end;

            if taPayTypePl.AsInteger=2 then //����. �����
            begin
              if Check.Operation=1 then taCheckTrOPERATION.AsInteger:=5; //�������
              if Check.Operation=0 then taCheckTrOPERATION.AsInteger:=4; //�������
            end;

            taCheckTr.Post;

            inc(iMax);
//            CurPos.FULLSUMPL:=CurPos.FULLSUMPL-rSumPl;
//            rSumPl:=rSumPl-rv(CurPos.FULLSUM);
            rSumPl:=rSumPl-rv(CurPos.FULLSUMPL);
            CurPos.FULLSUMPL:=0;

//          end;
          if not taCheck.Eof then taCheck.Next;
          if not taCheck.Eof then fToCurPos;
        end
        else //����� �� ������� �� ������� - ���� ����� �������
        begin
          //��������� � ���. ��������
          rK:=rSumPl/CurPos.FULLSUM;  //��� �����������
          rQuant:=RoundEx(CurPos.FULLQUANT*rK*1000)/1000; //�������� �� 4-�� �����
          rSumD:=RoundEx(CurPos.FULLDSUM*rK*100)/100;

          CurPos.FULLSUMPL:=CurPos.FULLSUMPL-rSumPl;
          CurPos.QUANT:=CurPos.QUANT-rQuant;
          CurPos.DSUM:=CurPos.DSUM-rSumD;
          CurPos.RSUM:=CurPos.RSUM-rSumPl;

//          if CurPos.FULLSUMPL>0.001 then
//          begin

            taCheckTr.Append;
            taCheckTrCASHNUM.AsInteger:=CommonSet.CashNum;
            taCheckTrNUMPOS.AsInteger:=abs(CurPos.NUMPOS);
            taCheckTrARTICUL.AsString:=CurPos.ARTICUL;
            taCheckTrNAME.AsString:=CurPos.NAME;
            taCheckTrQUANT.AsFloat:=rQuant;
            taCheckTrPRICE.AsFloat:=CurPos.PRICE;
            taCheckTrDPROC.AsFloat:=CurPos.DPROC;
            taCheckTrDSUM.AsFloat:=rSumD;
            taCheckTrRSUM.AsFloat:=rSumPl;
            taCheckTrDEPART.AsInteger:=CurPos.DEPART;
            taCheckTrRBAR.AsString:=CurPos.RBAR;
            taCheckTrTYPEP.AsInteger:=taPayTypePl.AsInteger;
            taCheckTrVIDP.AsInteger:=taPayVidPl.AsInteger;
            taCheckTrTIMEPOS.AsDateTime:=CurPos.TIMEPOS;
            taCheckTrAMARK.AsString:=CurPos.AMARK;

            if taPayTypePl.AsInteger=0 then //���
            begin
              if Check.Operation=1 then taCheckTrOPERATION.AsInteger:=1; //�������
              if Check.Operation=0 then taCheckTrOPERATION.AsInteger:=0; //�������
            end;

            if taPayTypePl.AsInteger=1 then //������
            begin
              if Check.Operation=1 then taCheckTrOPERATION.AsInteger:=3; //�������
              if Check.Operation=0 then taCheckTrOPERATION.AsInteger:=2; //�������
            end;

            if taPayTypePl.AsInteger=2 then //����. �����
            begin
              if Check.Operation=1 then taCheckTrOPERATION.AsInteger:=5; //�������
              if Check.Operation=0 then taCheckTrOPERATION.AsInteger:=4; //�������
            end;

            taCheckTr.Post;

            inc(iMax);

            rSumPl:=-1;
//          end;
        end;
      end;

      taPay.Next;
    end;
    trUpdChTr.Commit;
    sSave:=sSave+'10';
    taCheck.Active:=False;
    taCheckTr.Active:=False;
    sSave:=sSave+'11';

    //��� ������������ ����������� - ���� ������ ��� � ����
    try
      delay((iMax mod 10)*33);
      if Length(Check.Discount)>20 then Check.Discount:=Copy(Check.Discount,1,20);

      sErr:=its(CommonSet.CashNum)+','+its(Nums.ZNum)+','+its(CommonSet.ShopIndex)+','+its(Nums.iCheckNum)+','+its(Person.Id)+','+fs(rSumPay)+',"'+Check.Discount+'",'+its(Check.ECheck)+', �������� - '+its((iMax mod 10)*33);
//    12:12:03 ;!!SaveCheck;.01234567891011 ERR(4 ,41 ,1 ,29 , 2371, 448, , �������� - 297) 1213141516_EndPrintBn


      prSaveCheckTr.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
      prSaveCheckTr.ParamByName('ZNUMBER').AsInteger:=Nums.ZNum;
      prSaveCheckTr.ParamByName('SHOPINDEX').AsInteger:=CommonSet.ShopIndex;
      prSaveCheckTr.ParamByName('NUMCHECK').AsInteger:=Nums.iCheckNum;
      prSaveCheckTr.ParamByName('CASHER').AsInteger:=Person.Id;
      prSaveCheckTr.ParamByName('PAYSUM').AsFloat:=rSumPay;
      prSaveCheckTr.ParamByName('DBAR').AsString:=Check.Discount;
      prSaveCheckTr.ParamByName('ECHECK').AsInteger:=Check.ECheck;
      prSaveCheckTr.ExecProc;
    except
      sSave:=sSave+' ERR('+sErr+') ';
    end;

    sSave:=sSave+'12';

    taCheck.Active:=False;
    taCheck.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
    taCheck.Active:=True;
    sSave:=sSave+'13';

    with fmMainCasher do
    begin
      ViewChDPROC.Visible:=False;
      ViewChDSUM.Visible:=False;

      Label17.Caption:='...';
      Label18.Caption:='';
      Label18.Visible:=False;
      Label19.Caption:='';
      Label19.Visible:=False;
      Label24.Caption:='';
      Label24.Visible:=False;
    end;

    sSave:=sSave+'14';

    ClearPos;
    ClearSelPos;
    ResetPos;

    sSave:=sSave+'15';
    ClearCheck;
    sSave:=sSave+'16';
    CalcSum;

    fmMainCasher.ViewCh.EndUpdate;
//    PayView.EndUpdate;
  end;
end;


procedure TfmCash.FormCreate(Sender: TObject);
begin
//  FormPlacement1.IniFileName:=CurDir+GridIni;
//  FormPlacement1.Active:=True;
end;

procedure TfmCash.Timer1Timer(Sender: TObject);
begin
  if MessClear then
  begin
    Label4.Caption:='';
    MessClear:=False;
  end;
  if Label4.Caption>'' then MessClear:=True;
end;

procedure TfmCash.acExitExecute(Sender: TObject);
begin
  prWriteLog('~!KeyExit');
  if bBegCashEnd then exit; //�������� ����, ��� ������ ����
  if fmCash.Visible then
  begin

  end;
  prWriteLog('!!ExitItog;');
  bStopAddPos:=False;
  ModalResult:=mrCancel;
end;

procedure TfmCash.acCalcNExecute(Sender: TObject);
Var rMoney,rSumPl:Real;
    StrWk,StrWk1:String;
    iTotal,iMoney,iDopl:Integer;
    iMax,iDef:Integer;
    n:INteger;
    iTotalCh:INteger;
    bStop,bErr:Boolean;
    iSumPos,iSumTotal,iTotalDB:INteger;
    bAlcoStop:Boolean;
    iDiscInPrice:Integer;
    sDisc:String;
    iSt:Integer;
begin
  Check.RSum:=RoundEx(cxCurrencyEdit10.Value*100)/100;
  iDiscInPrice:=0; //���������� ��� ������� ������ ������ ���������� � ����
  sDisc:=''; //������ ����������� ������

  prWriteLog('~!acCalcNKey');
  if iStatus=1 then //������ �������� - ��������� ����
  begin
    prWriteLog('~!EndCheck;'+IntToStr(Nums.iCheckNum)+';');
    ToCustomDisp('      �������       ','    �� ������� !    ');
    CountSec:=0;
    fmMainCasher.Timer2.Enabled:=True;
    fmMainCasher.Timer3.Enabled:=True;

    bStopAddPos:=False;
    ModalResult:=mrOk;
  end;
  if iStatus=0 then //������ ��������
  begin
    if bBegCashEnd then exit; //������ �� �������� �������
    bBegCashEnd:=True;  //�������� ������ ������� ����

    prWriteLog('~!acCalcN_');

    //��������� �������� �����
    cxCurrencyEdit3.SetFocus;
    cxCurrencyEdit2.SetFocus;

    if abs(cxCurrencyEdit2.EditValue-rDiscDop-cxCurrencyEdit10.EditValue)<0.01 then cxCurrencyEdit2.EditValue:=cxCurrencyEdit10.EditValue;

    try
      rMoney:=rv(cxCurrencyEdit2.EditValue); // ���� ��� ����� ����� - �������� ''
    except
      rMoney:=0;
      cxCurrencyEdit3.EditValue:=-cxCurrencyEdit10.EditValue;
      cxCurrencyEdit2.EditValue:=rMoney;
    end;

    try
      if (cxCurrencyEdit2.EditValue-cxCurrencyEdit10.EditValue)>5000 then
      begin
        cxCurrencyEdit2.EditValue:=0;
        cxCurrencyEdit3.EditValue:=-cxCurrencyEdit10.EditValue;
        rMoney:=0;
      end;
    except
      cxCurrencyEdit2.EditValue:=0;
      cxCurrencyEdit3.EditValue:=-cxCurrencyEdit10.EditValue;
      rMoney:=0;
    end;

    cxCurrencyEdit2.SetFocus;
    cxCurrencyEdit2.SelectAll;

    //��� ����� ���-�� ������
    if rMoney>=0.01 then
    begin

      //������ �������� - �������� � ������� ��������
//      PayView.BeginUpdate;
      iMax:=1;
      rSumPl:=rv(cxCurrencyEdit1.EditValue);  //��� ����� � �������
      taPay.First;
      while not taPay.Eof do begin iMax:=iMax+1; rSumPl:=rSumPl-taPaySumPl.AsFloat; taPay.Next; end;


      taPay.Append;
      taPayId.AsInteger:=iMax;
      taPayTypePl.AsInteger:=0; //��������
      taPayVidPl.AsInteger:=0;  //��������
      taPayNamePl.AsString:='��������';
      taPaySumPl.AsFloat:=rMoney;
      taPaySumOst.AsFloat:=rSumPl-rMoney;
      taPayComment.AsString:='';
      taPay.Post;

      //����� ����� ������ �� ������� ��������

      rSumPl:=0; //��� ����� �����
      taPay.First;
      while not taPay.Eof do begin rSumPl:=rSumPl+rv(taPaySumPl.AsFloat); taPay.Next; end;
      rSumPl:=rSumPl-rv(cxCurrencyEdit1.EditValue); //����� � ����� �� ���� ��������

      if (abs(rSumPl+rDiscDop)<0.01) then rSumPl:=0;
      if rSumPl>0 then rSumPl:=rSumPl+rDiscDop;

//      PayView.EndUpdate;

      cxCurrencyEdit3.EditValue:=rSumPl; //�����
      delay(33); //���� ����� ����

      prWriteLog('$!Money;'+FloatToStr(cxCurrencyEdit10.EditValue)+';'+FloatToStr(cxCurrencyEdit2.EditValue)+';'+FloatToStr(cxCurrencyEdit3.EditValue)+';');

      if cxCurrencyEdit3.EditValue<0 then
      begin
        Beep;

      //�������� �������� ���������� �������
//      Top:=333;

        if fmCash.tag=1 then  Height:=400;  //�������� 800�600
        GridPay.Visible:=True;

        Label4.Caption:='��������� ����� ������������ !';
        if fmCash.Visible then
        begin
          cxCurrencyEdit2.EditValue:=RV((-1)*cxCurrencyEdit3.EditValue);
          cxCurrencyEdit2.SetFocus;
          cxCurrencyEdit2.SelectAll;
        end;
      end
      else
      begin
        bAlcoStop:=False;

        if AlcoSet.UseUTM=1 then  //����� ����������� � ���
          if fTestAlcoInCh then
          begin
            fmMainCasher.Memo2.Lines.Add('�������� ���������� � �����.');
            if fSendAlco=False then
            begin
              bAlcoStop:=True;
              fmMainCasher.Memo2.Lines.Add('������ ��������.');
            end else
            begin
              fmMainCasher.Memo2.Lines.Add('�������� ��.');
            end;
          end;

        if bAlcoStop then
        begin
          fmAttention.Label1.Caption:='��������. ������ �������� ���������� � �����. ������� �������� �������� ���������. ('+AlcoSet.URL+')';
          fmAttention.ShowModal;
        end else
        begin
          if fmCash.Showing then
          begin
            iTotalCh:=RoundEx(cxCurrencyEdit10.Value*100);

            sSave:='.';

            CashDriver;//������ �������� ���� - ����� ����� ��� ��� ����������

            Edit1.SetFocus;
            cxCurrencyEdit10.Enabled:=False;
            cxCurrencyEdit2.Enabled:=False;
            //��� ���� ��������� ��� - ������ �� ������ �������� �� ���������� �������

            //�������� ��� ����������
            Nums.iRet:=InspectSt(Nums.sRet);
            if Nums.iRet<>0 then TestFp('InspectSt');
            // ������� ����������� ���

            if CommonSet.PrintFisCheckOnEnd=1 then
            begin
              bStop:=False;

              if not OpenZ then ZOpen;

              if More24H(iSt) then
              begin
                ShowMessage('�������� �����. (����� 24�)');
                bStop:=True;
              end;

              if not bStop then
              begin
                Case Check.Operation of
                0: begin
                     if CheckRetStart=False then bStop:=True;
                   end;
                1: begin
                     if CheckStart=False then bStop:=True;
                   end;
                end;
              end;

              if not bStop then
              begin
                with dmC do
                begin
                  iSumTotal:=0;
                  iTotalDB:=0;
                  bErr:=True;  //��� ������

                  fmMainCasher.ViewCh.BeginUpdate;
                  dsCheck.DataSet:=nil;

                  if CommonSet.DiscToPrice=1 then iDiscInPrice:=prRecalcDiscPrice;

                  taCheck.FullRefresh;

                  taCheck.First;
                  while not taCheck.Eof do
                  begin
                    if bErr then
                    begin
                      iTotalDB:=iTotalDB+RoundEX(taCheckRSUM.asfloat*100);
                      fToCurPos1; // �� taCheck � CurPos
                      bErr:=CheckAddPos(iSumPos);
                      iSumTotal:=iSumTotal+iSumPos;
                    end else
                    begin
                      //������ ������� �������
                      TestFp('CheckAddPos');
                      prClearCheck1;  //��������� ���
                      bBegCashEnd:=False;
                      exit;
                    end;
                    taCheck.Next;
                  end;

                  dsCheck.DataSet:=taCheck;
                  fmMainCasher.ViewCh.EndUpdate;

                  prWriteLog('_!AddAllPosCheck;'+its(iTotalDB)+'-���;'+its(iSumTotal)+'-���;');
                end;
              end;
            end;

            //���� ������ ��� ����� �� ����

            Check.PayType:=0; //���
            if CommonSet.PrintFisCheckOnEnd>=0 then // �������� �.�. ��� ��������� ��������� ��� ���� ������� //���������� ��������� ��� ������
            begin
              if CommonSet.TypeFis<>'pirit' then
              begin
                if CheckTotal(iTotal)=False then
                begin
                  prWriteLog('_!TotalFisBad;'+IntToStr(iTotal)+';');
                  n:=5;
                  TestFp('CheckTotal');
                  while (CheckTotal(iTotal)=False)and(n>0) do
                  begin
                    TestFp('CheckTotal');
                    dec(n); delay(100);
                  end;
                  if n=0 then //������� �������� ������ � ������������ ���
                  begin
                    prClearCheck1;  //��������� ���
                    bBegCashEnd:=False;
                    exit;
                  end;
                end else
                begin
                  prWriteLog('_!CheckTotal;'+its(iTotalCh)+'-���;'+its(iTotal)+'-���;');
                end;
              end;

              if rDiscont<>0 then
              begin
                if CheckDiscount('������ ',rDiscont,iTotal)=False then
                begin
                  prWriteLog('_!DiscFisBad;'+IntToStr(iTotal)+';');
                  n:=5;
                  TestFp('CheckDiscount');
                  while (CheckDiscount('������ ',rDiscont,iTotal)=False)and(n>0) do
                  begin
                    TestFp('CheckDiscount');
                    dec(n); delay(100);
                  end;
                  if n=0 then //������� �������� ������ � ������������ ���
                  begin
                    prClearCheck1;  //��������� ���
                    bBegCashEnd:=False;
                    exit;
                  end else
                  begin
                    Check.DSum:=rDiscont;
                    Check.RSum:=iTotal/100;
                    prWriteLog('_!DiscFis;'+IntToStr(iTotal)+';'+IntToStr(roundEx(rDiscont*100))+';');
                  end;
                end else
                begin
                  Check.DSum:=rDiscont;
                  Check.RSum:=iTotal/100;
                  prWriteLog('_!DiscFis;'+IntToStr(iTotal)+';'+IntToStr(roundEx(rDiscont*100))+';');
                  TestFp('CheckDiscount');
                end;
              end;

              if rDiscDop>0.001 then
              begin
                if CheckDiscount('���.������ ',rDiscDop,iTotal)=False then
                begin
                  prWriteLog('_!DiscDopFisBad;'+IntToStr(iTotal)+';');
                  n:=5;
                  TestFp('CheckDiscDop');
                  while (CheckDiscount('���.������ ',rDiscDop,iTotal)=False)and(n>0) do
                  begin
                    TestFp('CheckDiscDop');
                    dec(n); delay(100);
                  end;
                  if n=0 then //������� �������� ������ � ������������ ���
                  begin
                    prClearCheck1;  //��������� ���
                    bBegCashEnd:=False;
                    exit;
                  end else prWriteLog('_!DiscDopFis;'+IntToStr(iTotal)+';'+IntToStr(roundEx(rDiscDop*100))+';');
                end else
                begin
                  prWriteLog('_!DiscDopFis;'+IntToStr(iTotal)+';'+IntToStr(roundEx(rDiscDop*100))+';');
                  TestFp('CheckDiscDop');
                end;
              end;

              if CheckTotal(iTotal)=False then
              begin
                prWriteLog('_!TotalFisBad;'+IntToStr(iTotal)+';');
                n:=5;
                TestFp('CheckTotal');
                while (CheckTotal(iTotal)=False)and(n>0) do
                begin
                  TestFp('CheckTotal');
                  dec(n); delay(100);
                end;
                if n=0 then //������� �������� ������ � ������������ ���
                begin
                  prClearCheck1;  //��������� ���
                  bBegCashEnd:=False;
                  exit;
                end;
              end;

              //��� ������������� ������
              if iTotal=0 then iTotal:=RoundEx(cxCurrencyEdit10.EditValue*100); //��� ������������� ������

              Check.RSum:=iTotal/100;

              //��������� �������� �� �������� �����
              prWriteLog('_!EndTest_iTotal;'+its(iTotalCh)+'-���;'+its(iTotal)+'-���;');
              if (abs(iTotalCh-iTotal)>2) then
              begin
                prClearCheck1;  //��������� ���
                bBegCashEnd:=False;
                exit;
              end;
            end else
            begin
              //��� ������������� ������
              if iTotal=0 then iTotal:=RoundEx(cxCurrencyEdit10.EditValue*100); //��� ������������� ������
              Check.RSum:=iTotal/100;

            end;

            try
              sSave:=sSave+'0';
              sSave:=sSave+'1';
              sSave:=sSave+'2';

              iDef:=RoundEx(rCheckSum*100)-RoundEx(rDiscont*100)-RoundEx(rDiscDop*100);
              if (abs(iDef-iTotal)>0)and(abs(iDef-iTotal)<3) then //���� �� ���� �� ���� � ������ �� ����
              begin //���� ����������� �� 3-�� ������ �� ��������� - ����������� ���������� ������, ����� ������������.
                WriteHistory(' ������ (3): ����� � '+INtToStr(CommonSet.CashNum)+'; ����� � '+IntToStr(Nums.ZNum)+';  ��� �'+IntToStr(Nums.iCheckNum)+'; ����� �� ����� - '+IntToStr(iTotal)+'; ����� �� ���� - '+FloatToStr(rCheckSum)+'; ������ �� ����� - '+IntToStr(roundEx(rDiscont*100))+'; ������ �� ���� - '+FloatToStr(rDiscont));
                prWriteLog('_!FisDiferenceWithBase;'+IntToStr(iDef)+';'+IntToStr(iTotal)+';');

                //���� � ���� ��������
                with dmC do
                begin
                  taCheck.Active:=False;
                  taCheck.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
                  taCheck.Active:=True;

                  taCheck.Last;
                  iMax:=taCheckNUMPOS.AsInteger+1; //����� ����� �������

                  trUpdCh.StartTransaction;

                  taCheck.Append;
                  taCheckCASHNUM.AsInteger:=CommonSet.CashNum;
                  taCheckNUMPOS.AsInteger:=iMax;
                  taCheckRBAR.AsString:='';
                  taCheckARTICUL.AsString:=CommonSet.Articul0;
                  taCheckNAME.AsString:='';
                  taCheckQUANT.AsFloat:=iTotal-iDef;
                  taCheckPRICE.AsFloat:=0.01;
                  taCheckDPROC.AsFloat:=0;
                  taCheckDSUM.AsFloat:=0;
                  taCheckRSUM.AsFloat:=(iTotal-iDef)/100;
                  taCheckDepart.AsInteger:=CommonSet.DepartId;
                  taCheckEDIZM.AsString:='';
                  taCheckTIMEPOS.AsDateTime:=now;
                  taCheckAMARK.AsString:='';
                  taCheck.Post;

                  trUpdCh.Commit;

                  taCheck.Active:=False;
                end;
              end else
              begin //����������� ������ 2-�� ������ - ���� ������������ ���
                if abs(iDef-iTotal)>2 then
                begin
                  prWriteLog('_!FisDifWithBase;'+IntToStr(iDef)+';'+IntToStr(iTotal)+';');
                  prClearCheck1;  //��������� ���
                  ToCustomDisp('      �������       ','    �� ������� !    ');
                  CountSec:=0;
                  fmMainCasher.Timer2.Enabled:=True;
                  bBegCashEnd:=False;
                  ModalResult:=mrOk; //������
                end;
              end;

              sSave:=sSave+'3';
  //        prSaveAndCloseCheck(cxCurrencyEdit2.EditValue);

              if iArtDisc>0 then
              begin //�������� � ��� ������� ��
                with dmC do
                begin


                  taCheck.Active:=False;
                  taCheck.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
                  taCheck.Active:=True;

                  taCheck.Last;
                  iMax:=taCheckNUMPOS.AsInteger+1; //����� ����� �������

                  prWriteLog('_!AddPosDiscCard;'+its(iArtDisc)+';'+its(iMax));

                  trUpdCh.StartTransaction;

                  taCheck.Append;
                  taCheckCASHNUM.AsInteger:=CommonSet.CashNum;
                  taCheckNUMPOS.AsInteger:=iMax;
                  taCheckRBAR.AsString:='';
                  taCheckARTICUL.AsString:=its(iArtDisc);
                  taCheckNAME.AsString:='';
                  taCheckQUANT.AsFloat:=1;
                  taCheckPRICE.AsFloat:=0;
                  taCheckDPROC.AsFloat:=0;
                  taCheckDSUM.AsFloat:=0;
                  taCheckRSUM.AsFloat:=0;
                  taCheckDepart.AsInteger:=CommonSet.DepartId;
                  taCheckEDIZM.AsString:='';
                  taCheckTIMEPOS.AsDateTime:=now;
                  taCheckAMARK.AsString:='';
                  taCheck.Post;

                  trUpdCh.Commit;

                  taCheck.Active:=False;
                end;
              end;

              prSaveCheck;
            finally
              prWriteLog('!!SaveCheck;'+sSave);
            end;

            if iDiscInPrice>0 then  //����� � ������ ������� ������ � ����� ��������������� ������
              sDisc:='                     ���� ������ '+fts(iDiscInPrice/100)+' ���.';
//              sDisc:='       ���� ������� � ������ ������.';

//            PrintNFStr('       ���� ������� � ������ ������....');

            if CommonSet.TypeFis<>'shtrih' then //���� �� �����
            begin
              taPay.First;
              while not taPay.Eof do
              begin
                iMoney:=RoundEx(taPaySumPl.AsFloat*100);
                if taPayTypePl.AsInteger<>2 then
                begin
                  if CheckRasch(taPayTypePl.AsInteger,iMoney,'',sDisc,iDopl)=False then //����� iDopl ����� �������
                  begin
                    prWriteLog('_!CheckRaschError1;TypePl='+its(taPayTypePl.AsInteger)+';iMoney='+its(iMoney)+';iDopl='+its(iDopl)+';');
                    prClearCheck1;  //��������� ���
                    bBegCashEnd:=False;
                    exit;
                  end;
                end
                else
                begin
                  if sDisc='' then sDisc:=taPayComment.AsString;
                  if CheckRasch(taPayTypePl.AsInteger,iMoney,taPayNamePl.AsString,sDisc,iDopl)=False then  //����� iDopl ����� �������
                  begin
                    prWriteLog('_!CheckRaschError2;TypePl='+its(taPayTypePl.AsInteger)+';iMoney='+its(iMoney)+';iDopl='+its(iDopl)+';');
                    prClearCheck1;  //��������� ���
                    bBegCashEnd:=False;
                    exit;
                  end;
                end;
                TestFp('CheckRasch');

                prWriteLog('_!FisRasch;'+IntToStr(iMoney)+';');

                taPay.Next;
              end;
            end else      //���� �����
            begin
              rSumNal:=0; rSumBn:=0;

              taPay.First;
              while not taPay.Eof do
              begin
                if taPayTypePl.AsInteger=2 then rSumBn:=rSumBn+taPaySumPl.AsFloat
                else rSumNal:=rSumNal+taPaySumPl.AsFloat;

                taPay.Next;
              end;

              iMoney:=RoundEx(rSumBn*100+rSumNal*100);

              if CheckRasch(taPayTypePl.AsInteger,iMoney,taPayNamePl.AsString,taPayComment.AsString,iDopl)=False then
              begin
                prWriteLog('_!CheckRaschErrStrih;TypePl=0;iMoney='+its(iMoney)+';iDopl='+its(iDopl)+';');
                prClearCheck1;  //��������� ���
                bBegCashEnd:=False;
                exit;
              end;
              TestFp('CheckRasch');
              prWriteLog('_!FisRasch;'+its(RoundEx(rSumBn*100))+';'+its(RoundEx(rSumNal*100)));

              iDopl:=iDopl*(-1); //����� , � �� �������
            end;

            if iDopl>0 then //���� - ����� ���� ����� -  ��� ���-�� �������� � �������
            begin
              prWriteLog('_!DoplFisTotal;'+IntToStr(iDopl)+';');
              iMoney:=iDopl;
              begin
                if CheckRasch(0,iMoney,'','',iDopl)=False then  //����� iDopl = 0 �� �����
                begin
                  prWriteLog('_!CheckRaschError3;TypePl=0'+';iMoney='+its(iMoney)+';iDopl='+its(iDopl)+';');
                  prClearCheck1;  //��������� ���
                  bBegCashEnd:=False;
                  exit;
                end;
              end;
              TestFp('CheckRasch');
            end;

  //        CheckClose;
            if CheckClose=False then
            begin
              prWriteLog('_!CheckCloseBad;');
              n:=5;
              TestFp('CheckClose');
              if Nums.iRet<>0 then
              begin
                TestFp('CheckClose');
                if Nums.iRet<>0 then
                begin
                  while (CheckClose=False)and(n>0) do
                  begin
                    TestFp('CheckClose');
                    dec(n); delay(100); delay(100);
                  end;
                  if n=0 then //������� �������� ������ � ������������ ���
                  begin
                    prClearCheck1;  //��������� ���
                    bBegCashEnd:=False;
                    exit;
                  end else prWriteLog('_!CheckClose;');
                end;
              end;
            end else
            begin
              prWriteLog('_!CheckClose;');
//              TestFp('CheckClose');
            end;

  //        prWriteLog('_!CheckClose;');

            taPay.Active:=False;

            prPrintQRCode;

  //        rMoney:=cxCurrencyEdit2.EditValue;
            fmMainCasher.TextEdit3.Text:='';
            if Check.Operation=0 then Check.Operation:=1; //������� �������� �������

            Nums.iRet:=InspectSt(Nums.sRet);
            if Nums.iRet<>0 then TestStatus('InspectSt',sMessage);

  //        GetNums; //����� ������� ����� ����
            WriteNums;
            if Nums.iCheckNum <3 then GetRes; //�������
            WriteStatus;  //����� ����� ���� � �.�. �.�. GetNums;// �� +1

            bStopAddPos:=False; //�������� ��������� �� �� �����
            inc(iStatus); //� ��������� �������
          end;

          Str((iTotal/100):9:2,StrWk1); While Length(StrWk1)<15 do StrWk1:=' '+StrWk1;
          StrWk:='';
          if cxCurrencyEdit3.EditValue>0 then
          begin
            rMoney:=cxCurrencyEdit3.EditValue;
            Str(rMoney:9:2,StrWk); While Length(StrWk)<15 do StrWk:=' '+StrWk;
            StrWk:='�����'+StrWk;
            ToCustomDisp('�����'+StrWk1,StrWk);
          end else
          begin
            prWriteLog('~!EndCheck;'+IntToStr(Nums.iCheckNum)+';');
  //        ToCustomDisp('      �������       ','    �� ������� !    ');
            ToCustomDisp('�����'+StrWk1,StrWk);

  //          inc(Nums.iCheckNum); //�������� ������ �� ������� ���� �� ������������ ����� ��� �������

            CountSec:=0;
            fmMainCasher.Timer2.Enabled:=True;
            bBegCashEnd:=False;
            ModalResult:=mrOk;
          end;
          fmMainCasher.Timer3.Enabled:=True;

        end;
      end;

//      Nums.iRet:=InspectSt(Nums.sRet);
      WriteStatus;
    end; //if rMoney>=0.01
    bBegCashEnd:=False;
  end;
end;

procedure TfmCash.FormShow(Sender: TObject);
begin
  iStatus:=0;
  cxCurrencyEdit2.SetFocus;
  GridPay.Visible:=False;
  bFirst:=True;
//  Top:=375;
//  Left:=275;
  if fmCash.tag=1 then  Height:=275;  //�������� 800�600
end;

procedure TfmCash.acBarCExecute(Sender: TObject);
begin
  if iStatus=1 then
  begin
    prWriteLog('~!EndCheck;'+IntToStr(Nums.iCheckNum)+';');

    bStopAddPos:=False;
    Close;
    fmMainCasher.TextEdit3.SetFocus;
  end;
end;

procedure TfmCash.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  bBegCash:=False;
  bStopAddPos:=False; //���� ������ ��������� �� �����

  SetForegroundWindow(Application.Handle);
End;

procedure TfmCash.acCalcBNExecute(Sender: TObject);
Var rMoney:Real;
    StrWk,StrWk1,StrWk2:String;
    iTotal,iMoney,iDopl:Integer;
    iMax,iDef,i:Integer;
    bNotErr:Boolean;
    n:INteger;
    iTotalCh:INteger;
    StrP:String;
    BnStr1:String;
    sOper:String;
    bErr,bStop:Boolean;
    iSumTotal,iSumPos,iTotalDB:Integer;
    bAlcoStop:Boolean;
    rDiscDopTMP:Real;
    iDiscInPrice:Integer;
    sDisc:string;
    iSt:Integer;
begin //������ �����������
  Check.RSum:=RoundEx(cxCurrencyEdit1.Value*100)/100;

  rDiscDopTMP:=rDiscDop; //�������� �� ������ ���� ������ �� ������� , �� ����� ����� �������.
  rDiscDop:=0; //��� ������� �������� �������������� ������ �� ���������� ���...

  iDiscInPrice:=0; //���������� ��� ������� ������ ������ ���������� � ����
  sDisc:=''; //������ ����������� ������

  prWriteLog('~!acCalcBNKey');
  if iStatus=1 then //������ �������� - ��������� ����
  begin
    prWriteLog('~!EndCheck;'+IntToStr(Nums.iCheckNum)+';');
    ToCustomDisp('      �������       ','    �� ������� !    ');
    CountSec:=0;
    fmMainCasher.Timer2.Enabled:=True;

    bStopAddPos:=False;
    ModalResult:=mrOk;
  end;
  if iStatus=0 then //������ ������
  begin
    //������ ��� ��������� ������ - ������������ ������� � ����� , ��� ������� ��� ���������
    bAlcoStop:=False;

    if AlcoSet.UseUTM=1 then  //����� ����������� � ���
      if fTestAlcoInCh then
      begin
        fmMainCasher.Memo2.Lines.Add('�������� ����� � �����.');
        if fTestAlcoConn=False then
        begin
          bAlcoStop:=True;
          fmMainCasher.Memo2.Lines.Add('����������� ����� � ��� �����');
        end else
        begin
          fmMainCasher.Memo2.Lines.Add('����� ��');
        end;
      end;

    if bAlcoStop then
    begin
      fmAttention.Label1.Caption:='��������. ������ ����� � ��� �����. ������� �������� �������� ���������.';
      fmAttention.ShowModal;
    end else
    begin

      if bBegCashEnd then exit; //������ �� �������� �������
      bBegCashEnd:=True;  //�������� ������ ������� ����
      prWriteLog('~!acCalcBN_');

      //��� ������� ����� ����� ����� ��� ��������� ������ (������ �����)
      //���� �������, ��� ���������������
      //� �������� ������ �� ������� ����� ��������� ������ �� ��� ���������� �����
      //���� ��������� �� ����� ������� ��� �������, � ����� �� ��� ���������� ����� ������

      {
      cxCurrencyEdit3.SetFocus; //��������� ������, ���� ������������� ��������
      cxCurrencyEdit2.SetFocus;
      try
        rMoney:=cxCurrencyEdit2.EditValue; // ���� ��� ����� ����� - �������� ''
      except
        rMoney:=cxCurrencyEdit1.EditValue;
      end;
      }
      BnStr:='';
      sSave:='.';

      //�������� ����� � �������
      rMoney:=rv(cxCurrencyEdit1.EditValue);  //��� ����� � �������
      taPay.First;
      while not taPay.Eof do begin rMoney:=rMoney-taPaySumPl.AsFloat; taPay.Next; end;
  //    rMoney = ������ ���������� ����� � ������ � ������

      if CommonSet.BNManual=1 then //������ ����� �������
      begin
        //������� ���� ������ ���� ����� � �����.
        try
          fmCredCards:=TfmCredCards.Create(Application);
          fmCredCards.CurrencyEdit1.EditValue:=rMoney;
          fmCredCards.Panel2.Visible:=False;
          fmCredCards.Edit1.Visible:=False;
          fmCredCards.GridBN.Visible:=True;
          fmCredCards.Button1.Visible:=True;
          fmCredCards.ShowModal;
          if fmCredCards.ModalResult<>mrOk then
          begin
            rMoney:=0; //���� ������ �� �����=0
            rDiscDop:=rDiscDopTMP;
          end;

        finally
          fmCredCards.Release;
        end;
      end;
      if CommonSet.BNManual=0 then ////�������������� ����� ������� ABG
      begin
        //������� ���� ���������� ����� � �����.
        //������� ���� ������ ���� ����� � �����.
        try
          fmCredCards:=TfmCredCards.Create(Application);
          fmCredCards.CurrencyEdit1.EditValue:=rMoney;
          fmCredCards.Panel2.Visible:=True;
          fmCredCards.Edit1.Visible:=True;
          fmCredCards.GridBN.Visible:=False;
          fmCredCards.Button1.Visible:=False;
          bnBar:='';

          fmCredCards.ShowModal;
          if fmCredCards.ModalResult<>mrOk then
          begin
            rMoney:=0; //���� ������ �� �����=0
            rDiscDop:=rDiscDopTMP;
           end;
         finally
          fmCredCards.Release;
        end;
      end;

      if CommonSet.BNManual=2 then //�������������� ����� ������� ����
      begin
        try
  //        fmSber:=tFmSber.Create(Application);

          iMoneyBn:=RoundEx(rMoney*100); //� ��������
  //        iMoneyBn:=Trunc(Rv(rMoney)*100); //� ��������

  //        iMoneyBn:=Trunc(rMoney*100);

          fmSber.Label1.Visible:=True;
          fmSber.cxButton9.Enabled:=False;
          fmSber.cxButton10.Enabled:=False;
          fmSber.cxButton11.Enabled:=False;
          fmSber.cxButton6.Enabled:=False;
          fmSber.cxButton8.Enabled:=False;
          fmSber.cxButton8.TabStop:=False;

          sOper:='_';
          if Check.Operation=1 then sOper:='�������';
          if Check.Operation=0 then sOper:='�������';

          fmSber.Memo1.Clear;
          fmSber.Memo1.Lines.Add('');
          fmSber.Memo1.Lines.Add('��������: '+sOper+'  �����: '+FloatToStr(iMoneyBn/100));
          delay(10);


          if Check.Operation=1 then //�������
          begin

            SberTabStop:=1;

            fmSber.cxButton2.Enabled:=True;
            fmSber.cxButton2.TabOrder:=1;
            fmSber.cxButton2.TabStop:=True;

            StrP:=AnsiUpperCase(Person.Name);

            if (pos('�����',StrP)>0)
            or (Person.Id=1706)
            or (Person.Id=1707)
            or (Person.Id=1708)
            or (Person.Id=1709)
            then
            begin
              fmSber.cxButton8.Enabled:=True;
              fmSber.cxButton8.TabOrder:=2;
              fmSber.cxButton8.TabStop:=True;

              fmSber.cxButton1.TabOrder:=3;
            end else
            begin
              fmSber.cxButton8.Enabled:=False;
              fmSber.cxButton8.TabStop:=False;

              fmSber.cxButton1.TabOrder:=2;
            end;

            fmSber.cxButton3.Enabled:=False;
            fmSber.cxButton3.TabStop:=False;

            fmSber.cxButton4.Enabled:=False;
            fmSber.cxButton4.TabStop:=False;

            fmSber.cxButton5.Enabled:=False;
            fmSber.cxButton5.TabStop:=False;

            fmSber.cxButton7.Enabled:=False;
            fmSber.cxButton7.TabStop:=False;

            fmSber.cxButton13.Enabled:=False;
            fmSber.cxButton13.TabStop:=False;

          end;
          if Check.Operation=0 then //�������
          begin
            SberTabStop:=2;

            fmSber.cxButton2.Enabled:=False;
            fmSber.cxButton2.TabStop:=False;

            fmSber.cxButton3.Enabled:=False;
            fmSber.cxButton3.TabStop:=False;

            fmSber.cxButton4.Enabled:=False;
            fmSber.cxButton4.TabStop:=False;

            fmSber.cxButton5.Enabled:=True;
            fmSber.cxButton5.TabOrder:=1;
            fmSber.cxButton5.TabStop:=True;

            fmSber.cxButton7.Enabled:=True;
            fmSber.cxButton7.TabOrder:=2;
            fmSber.cxButton7.TabStop:=True;

            fmSber.cxButton1.TabOrder:=3;

            StrP:=AnsiUpperCase(Person.Name);

            if (pos('�����',StrP)>0)
            or (Person.Id=1706)
            or (Person.Id=1707)
            or (Person.Id=1708)
            or (Person.Id=1709)
            then
            begin
              fmSber.cxButton8.Enabled:=True;
              fmSber.cxButton8.TabOrder:=3;
              fmSber.cxButton8.TabStop:=True;

              fmSber.cxButton1.TabOrder:=4;
            end else
            begin
              fmSber.cxButton8.Enabled:=False;
              fmSber.cxButton8.TabStop:=False;

              fmSber.cxButton1.TabOrder:=3;
            end;

            fmSber.cxButton13.Enabled:=False;
            fmSber.cxButton13.TabStop:=False;

          end;

          bnBar:='';
          BnStr:='';

          prWriteLog('~!fmSber.ShowModal');

          fmSber.ShowModal;

          prWriteLog('');
          prWriteLog('������ ����� ����� ���� �����');
          prWriteLog('-----------------');
          prWriteLog(BnStr);
          prWriteLog('-----------------');
          prWriteLog('');

  // ��� �������� ������, ��� ������ �� ��������
  //        prBnPrint;

          if fmSber.ModalResult<>mrOk then
          begin
            rMoney:=0; //���� ������ �� �����=0
            iMoneyBn:=0;
            rDiscDop:=rDiscDopTMP;  // ��������� ��� ������ �� ���������� ���
          end;
          if iMoneyBn<>0 then
          begin
            rMoney:=iMoneyBn/100;
          end;

        finally
  //        fmSber.Release;
        end;
      end;

      if CommonSet.BNManual=3 then //�������������� ����� ������� ���
      begin
        try
  //        fmSber:=tFmSber.Create(Application);

          iMoneyBn:=RoundEx(rMoney*100); //� ��������

          fmVTB.Label1.Visible:=True;
          fmVTB.cxButton9.Enabled:=False;
          fmVTB.cxButton6.Enabled:=False;
          fmVTB.cxButton8.Enabled:=False;
          fmVTB.cxButton8.TabStop:=False;


          if Check.Operation=1 then //�������
          begin

            SberTabStop:=1;

            fmVTB.cxButton2.Enabled:=True;
            fmVTB.cxButton2.TabOrder:=1;
            fmVTB.cxButton2.TabStop:=True;

            StrP:=AnsiUpperCase(Person.Name);

            if (pos('�����',StrP)>0)
            or (Person.Id=1706)
            or (Person.Id=1707)
            or (Person.Id=1708)
            or (Person.Id=1709)
            then
            begin
              fmVTB.cxButton8.Enabled:=True;
              fmVTB.cxButton8.TabOrder:=2;
              fmVTB.cxButton8.TabStop:=True;

              fmVTB.cxButton1.TabOrder:=3;
            end else
            begin
              fmVTB.cxButton8.Enabled:=False;
              fmVTB.cxButton8.TabStop:=False;

              fmVTB.cxButton1.TabOrder:=2;
            end;

            fmVTB.cxButton5.Enabled:=False;
            fmVTB.cxButton5.TabStop:=False;

            fmVTB.cxButton13.Enabled:=False;
            fmVTB.cxButton13.TabStop:=False;

          end;
          if Check.Operation=0 then //�������
          begin
            SberTabStop:=2;

            fmVTB.cxButton2.Enabled:=False;
            fmVTB.cxButton2.TabStop:=False;

            fmVTB.cxButton5.Enabled:=True;
            fmVTB.cxButton5.TabOrder:=1;
            fmVTB.cxButton5.TabStop:=True;

            fmVTB.cxButton1.TabOrder:=3;

            StrP:=AnsiUpperCase(Person.Name);

            if (pos('�����',StrP)>0)
            or (Person.Id=1706)
            or (Person.Id=1707)
            or (Person.Id=1708)
            or (Person.Id=1709)
            then
            begin
              fmVTB.cxButton8.Enabled:=True;
              fmVTB.cxButton8.TabOrder:=3;
              fmVTB.cxButton8.TabStop:=True;

              fmVTB.cxButton1.TabOrder:=4;
            end else
            begin
              fmVTB.cxButton8.Enabled:=False;
              fmVTB.cxButton8.TabStop:=False;

              fmVTB.cxButton1.TabOrder:=3;
            end;

            fmVTB.cxButton13.Enabled:=False;
            fmVTB.cxButton13.TabStop:=False;

          end;

          bnBar:='';
          BnStr:='';

          prWriteLog('~!fmVTB.ShowModal');

          fmVTB.ShowModal;

          prWriteLog('');
          prWriteLog('������ ��� ����� ���� ���');
          prWriteLog('-----------------');
          prWriteLog(BnStr);
          prWriteLog('-----------------');
          prWriteLog('');

  // ��� �������� ������, ��� ������ �� ��������
  //        prBnPrint;

          if fmVTB.ModalResult<>mrOk then
          begin
            rMoney:=0; //���� ������ �� �����=0
            iMoneyBn:=0;
            rDiscDop:=rDiscDopTMP;  // ��������� ��� ������ �� ���������� ���
          end;
          if iMoneyBn<>0 then
          begin
            rMoney:=iMoneyBn/100;
          end;

        finally
        end;
      end;


      if rMoney>0 then
      begin //������ ������ ��������� - �������� � ������� ��������
  //      PayView.BeginUpdate;

        iMax:=1;
        taPay.First;
        if taPay.RecordCount>0 then
        begin
          while not taPay.Eof do begin iMax:=iMax+1; taPay.Next; end;
        end;

        taPay.Append;
        taPayId.AsInteger:=iMax;
        taPayTypePl.AsInteger:=2; //������  - 2; ������ - 1 ; 0-���

        if CommonSet.BNManual=1 then //������ ����� �������
        begin
          taPayVidPl.AsInteger:=dmC.taCredCardID.AsInteger; //�������� 0, Viza 1, MC 2 � �.� , ������ - 100
          taPayNamePl.AsString:=dmC.taCredCardNAME.AsString;
          taPayComment.AsString:='';

          //��������� ������� � ������������ �������.

          BnStr:='';
          //���� � �����
          BnStr:=BnStr+Formatdatetime('dd.mm.yyyy hh:nn:ss',now)+'  '+#$0D;
          //��������
          if Check.Operation=1 then BnStr:=BnStr+'��������: ������� '+#$0D
          else BnStr:=BnStr+'��������: ������� '+#$0D;
          //�����

          BnStr:=BnStr+'Visa';
          BnStr:=BnStr+' ******* '+#$0D;
          //��� �����������
          BnStr:=BnStr+'��� �����������  000000000'+#$0D;
          //�����
          Str(rMoney:9:2,StrWk1);
          BnStr:=BnStr+'�����: '+StrWk1+' ���.'+#$0D;
          BnStr:=BnStr+'������� _______________________________'+#$0D;
        end;
        if CommonSet.BNManual=0 then //������� ABG
        begin
          taPayVidPl.AsInteger:=100; //���� ����� 100, �� 100 ���� ��������� ���� ����� �� �������
          taPayNamePl.AsString:='BankCard';

          //��������� ������� � ������������ �������.

          BnStr:='|';
          //���� � �����
          BnStr:=BnStr+Copy(Answ.sDate,1,2)+'/'+Copy(Answ.sDate,3,2)+'/'+Copy(Answ.sDate,5,2)+'  ';
          BnStr:=BnStr+Copy(Answ.sTime,1,2)+':'+Copy(Answ.sTime,3,2)+'||';
          //��������
          if Check.Operation=1 then BnStr:=BnStr+'��������: �������||' //�������
          else BnStr:=BnStr+'��������: �������||';
          //�����
          StrWk1:=Copy(bnBar,pos('=',bnBar)+1,4); //YYMM

          StrWk2:=Copy(Answ.bnBar,1,16); //�������� �����
          if length(StrWk2)>=12 then for i:=5 to 12 do StrWk2[i]:='X';

          if Answ.bnBar[1]='4' then BnStr:=BnStr+'Visa';
          if Answ.bnBar[1]='5' then BnStr:=BnStr+'Master Card';
          if Answ.bnBar[1]='6' then BnStr:=BnStr+'Union';
          BnStr:=BnStr+' '+StrWk2+' '+Copy(StrWk1,3,2)+'/'+Copy(StrWk1,1,2)+'||';
          //��� �����������
          BnStr:=BnStr+'��� ����������� '+Answ.sAuthCode+'||';
          //�����
          Str(rMoney:9:2,StrWk1);
          BnStr:=BnStr+'�����: '+StrWk1+' ���.||';
          BnStr:=BnStr+'������� _______________________________||';

          taPayComment.AsString:=BnStr; //����� ��������� ������� ����������
        end;
        if (CommonSet.BNManual=2)or(CommonSet.BNManual=3) then //������� ���� � ���
        begin
          taPayVidPl.AsInteger:=100; //���� ����� 100, �� 100 ���� ��������� ���� ����� �� �������
          taPayNamePl.AsString:='BankCard';
          taPayComment.AsString:=''; //��� �������� ����� ����
        end;

        taPaySumOst.AsFloat:=0; //������ ���� ������ =0 �.�. ����������� ������ ������ �����������
        taPaySumPl.AsFloat:=rMoney;
        taPay.Post;

        //����� ����� =0

        cxCurrencyEdit3.EditValue:=0; //�����
        delay(33); //���� ����� ����

        prWriteLog('$!MoneyBn;'+FloatToStr(cxCurrencyEdit1.EditValue)+';'+FloatToStr(cxCurrencyEdit2.EditValue)+';'+FloatToStr(cxCurrencyEdit3.EditValue)+';');

        if fmCash.Showing then
        begin
          iTotalCh:=RoundEx(cxCurrencyEdit1.Value*100);

          // �� ������� �������� �������� ���� �� �����....
          if CommonSet.OpenBoxBnOper=1 then CashDriver;//������ �������� ���� �� ������ ������ - ����� ����� ��� ��� ����������

          Edit1.SetFocus;
          cxCurrencyEdit1.Enabled:=False;
          cxCurrencyEdit2.Enabled:=False;
          //��� ���� ��������� ��� - ������ �� ������ �������� �� ���������� �������

          //�������� ��� ����������
          Nums.iRet:=InspectSt(Nums.sRet);
          if Nums.iRet<>0 then TestFp('InspectSt');

          if CommonSet.PrintFisCheckOnEnd=1 then
          begin
            bStop:=False;

            if not OpenZ then ZOpen;

            if More24H(iSt) then
            begin
              ShowMessage('�������� �����. (����� 24�)');
              bStop:=True;
            end;

            if not bStop then
            begin
              Case Check.Operation of
              0: begin
                   if CheckRetStart=False then bStop:=True;
                 end;
              1: begin
                   if CheckStart=False then bStop:=True;
                 end;
              end;
            end;

            if not bStop then
            begin
              with dmC do
              begin
                iSumTotal:=0;
                iTotalDB:=0;
                bErr:=True;  //��� ������

                fmMainCasher.ViewCh.BeginUpdate;
                dsCheck.DataSet:=nil;

                if CommonSet.DiscToPrice=1 then iDiscInPrice:=prRecalcDiscPrice;

                taCheck.FullRefresh;

                taCheck.First;
                while not taCheck.Eof do
                begin
                  if bErr then
                  begin
                    iTotalDB:=iTotalDB+RoundEX(taCheckRSUM.asfloat*100);
                    fToCurPos1; // �� taCheck � CurPos
                    bErr:=CheckAddPos(iSumPos);
                    iSumTotal:=iSumTotal+iSumPos;
                  end else
                  begin
                      //������ ������� �������
                    TestFp('CheckAddPos');
                    prClearCheck1;  //��������� ���
                    bBegCashEnd:=False;
                    exit;
                  end;
                  taCheck.Next;
                end;

                dsCheck.DataSet:=taCheck;
                fmMainCasher.ViewCh.EndUpdate;

                prWriteLog('_!AddAllPosCheck;'+its(iTotalDB)+'-���;'+its(iSumTotal)+'-���;');
              end;
            end;
          end;
          //���� ������ ��� ����� �� ����

          Check.PayType:=2; //

          if CommonSet.PrintFisCheckOnEnd>=0 then  //���������� ��������� ��� ������
          begin
            if CheckTotal(iTotal)=False then
            begin
              prWriteLog('_!TotalFisBad;'+IntToStr(iTotal)+';');
              n:=5;
              TestFp('CheckTotal');
              while (CheckTotal(iTotal)=False)and(n>0) do
              begin
                TestFp('CheckTotal');
                dec(n); delay(100);
              end;
              if n=0 then //������� �������� ������ � ������������ ���
              begin
                prClearCheck1;  //��������� ���
                bBegCashEnd:=False;
                exit;
              end;
            end else
            begin
              prWriteLog('_!CheckTotal;'+its(iTotalCh)+'-���;'+its(iTotal)+'-���;');
            end;

            if rDiscont<>0 then
            begin
              if CheckDiscount('������ ',rDiscont,iTotal)=False then
              begin
                prWriteLog('_!DiscFisBad;'+IntToStr(iTotal)+';');
                n:=5;
                TestFp('CheckDiscount');
                while (CheckDiscount('������ ',rDiscont,iTotal)=False)and(n>0) do
                begin
                  TestFp('CheckDiscount');
                  dec(n); delay(100);
                end;
                if n=0 then //������� �������� ������ � ������������ ���
                begin
                  prClearCheck1;  //��������� ���
                  bBegCashEnd:=False;
                  exit;
                end else
                begin
                  Check.DSum:=rDiscont;
                  Check.RSum:=iTotal/100;
                  prWriteLog('_!DiscFis;'+IntToStr(iTotal)+';'+IntToStr(roundEx(rDiscont*100))+';');
                end;
              end else
              begin
                Check.DSum:=rDiscont;
                Check.RSum:=iTotal/100;
                prWriteLog('_!DiscFis;'+IntToStr(iTotal)+';'+IntToStr(roundEx(rDiscont*100))+';');
                TestFp('CheckDiscount');
              end;
            end;
            {   // � ������� ��������� �� �����
            if rDiscDop>0.001 then
            begin
              if CheckDiscount('���.������ ',rDiscDop,iTotal)=False then
              begin
                prWriteLog('_!DiscDopFisBad;'+IntToStr(iTotal)+';');
                n:=5;
                TestFp('CheckDiscDop');
                while (CheckDiscount('���.������ ',rDiscDop,iTotal)=False)and(n>0) do
                begin
                  TestFp('CheckDiscDop');
                  dec(n); delay(100);
                end;
                if n=0 then //������� �������� ������ � ������������ ���
                begin
                  prClearCheck1;  //��������� ���
                  bBegCashEnd:=False;
                  exit;
                end else prWriteLog('_!DiscDopFis;'+IntToStr(iTotal)+';'+IntToStr(roundEx(rDiscDop*100))+';');
              end else
              begin
                prWriteLog('_!DiscDopFis;'+IntToStr(iTotal)+';'+IntToStr(roundEx(rDiscDop*100))+';');
                TestFp('CheckDiscDop');
              end;
            end;
            }
            if CheckTotal(iTotal)=False then
            begin
              prWriteLog('_!TotalFisBad;'+IntToStr(iTotal)+';');
              n:=5;
              TestFp('CheckTotal');
              while (CheckTotal(iTotal)=False)and(n>0) do
              begin
                TestFp('CheckTotal');
                dec(n); delay(100);
              end;
              if n=0 then //������� �������� ������ � ������������ ���
              begin
                prClearCheck1;  //��������� ���
                bBegCashEnd:=False;
                exit;
              end;
            end;

            //��� ������������� ������
            if iTotal=0 then iTotal:=RoundEx(cxCurrencyEdit1.EditValue*100); //��� ������������� ������

            Check.RSum:=iTotal/100;    //��� ������

          //��������� �������� �� �������� �����
            prWriteLog('_!EndTest_iTotal;'+its(iTotalCh)+'-���;'+its(iTotal)+'-���;');
            if (abs(iTotalCh-iTotal)>2) then
            begin
              prClearCheck1;  //��������� ���
              bBegCashEnd:=False;
              exit;
            end;
          end else
          begin
            //��� ������������� ������
            if iTotal=0 then iTotal:=RoundEx(cxCurrencyEdit1.EditValue*100); //��� ������������� ������
            Check.RSum:=iTotal/100;    //��� ������
          end;

//          iDef:=RoundEx(rCheckSum*100)-RoundEx(rDiscont*100)-RoundEx(rDiscDop*100);
          iDef:=RoundEx(rCheckSum*100)-RoundEx(rDiscont*100);

          if (abs(iDef-iTotal)>0)and(abs(iDef-iTotal)<3) then //���� �� ���� �� ���� � ������ �� ����
          begin //���� ����������� �� 3-�� ������ �� ��������� - ����������� ���������� ������, ����� ������������.
            WriteHistory(' ������ (3): ����� � '+INtToStr(CommonSet.CashNum)+'; ����� � '+IntToStr(Nums.ZNum)+';  ��� �'+IntToStr(Nums.iCheckNum)+'; ����� �� ����� - '+IntToStr(iTotal)+'; ����� �� ���� - '+FloatToStr(rCheckSum)+'; ������ �� ����� - '+IntToStr(roundEx(rDiscont*100))+'; ������ �� ���� - '+FloatToStr(rDiscont));
            prWriteLog('_!FisDiferenceWithBase;'+IntToStr(iDef)+';'+IntToStr(iTotal)+';');

            //���� � ���� ��������
            with dmC do
            begin
              taCheck.Active:=False;
              taCheck.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
              taCheck.Active:=True;

              taCheck.Last;
              iMax:=taCheckNUMPOS.AsInteger+1; //����� ����� �������

              trUpdCh.StartTransaction;

              taCheck.Append;
              taCheckCASHNUM.AsInteger:=CommonSet.CashNum;
              taCheckNUMPOS.AsInteger:=iMax;
              taCheckRBAR.AsString:='';
              taCheckARTICUL.AsString:=CommonSet.Articul0;
              taCheckNAME.AsString:='';
              taCheckQUANT.AsFloat:=iTotal-iDef;
              taCheckPRICE.AsFloat:=0.01;
              taCheckDPROC.AsFloat:=0;
              taCheckDSUM.AsFloat:=0;
              taCheckRSUM.AsFloat:=(iTotal-iDef)/100;
              taCheckDepart.AsInteger:=CommonSet.DepartId;
              taCheckEDIZM.AsString:='';
              taCheckTIMEPOS.AsDateTime:=now;
              taCheckAMARK.AsString:='';
              taCheck.Post;

              trUpdCh.Commit;

              taCheck.Active:=False;
            end;
          end else
          begin //����������� ������ 2-�� ������ - ���� ������������ ���
            if abs(iDef-iTotal)>2 then
            begin
              prWriteLog('_!FisDifWithBase;'+IntToStr(iDef)+';'+IntToStr(iTotal)+';');
              prClearCheck1;  //��������� ���
              ToCustomDisp('      �������       ','    �� ������� !    ');
              CountSec:=0;
              fmMainCasher.Timer2.Enabled:=True;
              bBegCashEnd:=False;
              ModalResult:=mrOk; //������
            end;
          end;

          if iDiscInPrice>0 then  //����� � ������ ������� ������ � ����� ��������������� ������
           sDisc:='                     ���� ������ '+fts(iDiscInPrice/100)+' ���.';
//            sDisc:='       ���� ������� � ������ ������.';

          if CommonSet.TypeFis<>'shtrih' then //���� �� �����
          begin
            //������ �������� �� ������� - ����� �����
            //�� ����� ��������
            taPay.First;
            while not taPay.Eof do
            begin
              iMoney:=RoundEx(taPaySumPl.AsFloat*100);
              if taPayTypePl.AsInteger<>2 then //����������� ��� ��������� ����� � ��
              begin
                if CheckRasch(taPayTypePl.AsInteger,iMoney,'',sDisc,iDopl)=False then  //����� iDopl ����� �������
                begin
                  prWriteLog('_!CheckRaschError1;TypePl='+its(taPayTypePl.AsInteger)+';iMoney='+its(iMoney)+';iDopl='+its(iDopl)+';');
                  prClearCheck1;  //��������� ���
                  bBegCashEnd:=False;
                  exit;
                end;
              end
              else
              begin
                if sDisc='' then sDisc:=taPayComment.AsString;
                if CheckRasch(taPayTypePl.AsInteger,iMoney,taPayNamePl.AsString,sDisc,iDopl)=False then //����� iDopl ����� �������
                begin
                  prWriteLog('_!CheckRaschError2;TypePl='+its(taPayTypePl.AsInteger)+';iMoney='+its(iMoney)+';iDopl='+its(iDopl)+';');
                  prClearCheck1;  //��������� ���
                  bBegCashEnd:=False;
                  exit;
                end;
              end;
              TestFp('CheckRasch');

              prWriteLog('_!FisRasch;'+IntToStr(iMoney)+';');
              taPay.Next;
            end;
          end else
          begin
            rSumNal:=0; rSumBn:=0;

            taPay.First;
            while not taPay.Eof do
            begin
              if taPayTypePl.AsInteger=2 then rSumBn:=rSumBn+taPaySumPl.AsFloat
              else rSumNal:=rSumNal+taPaySumPl.AsFloat;

              taPay.Next;
            end;

            iMoney:=RoundEx(rSumBn*100+rSumNal*100);

            if CheckRasch(taPayTypePl.AsInteger,iMoney,taPayNamePl.AsString,taPayComment.AsString,iDopl)=False then
            begin
              prWriteLog('_!CheckRaschErrStrih;TypePl=0;iMoney='+its(iMoney)+';iDopl='+its(iDopl)+';');
              prClearCheck1;  //��������� ���
              bBegCashEnd:=False;
              exit;
            end;
            TestFp('CheckRasch');
            prWriteLog('_!FisRasch;'+its(RoundEx(rSumBn*100))+';'+its(RoundEx(rSumNal*100)));

            iDopl:=iDopl*(-1); //����� , � �� �������
          end;
          //���� ������� ����� ����

          sSave:=sSave+'0';
          try
            if iDopl>0 then //���� - ����� ���� ����� -  ��� ���-�� �������� � �������
            begin
              prWriteLog('_!DoplFisTotal;'+IntToStr(iDopl)+';');
              iMoney:=iDopl;
              if CheckRasch(0,iMoney,'','',iDopl)=False then  //����� iDopl = 0 �� �����
              begin
                prWriteLog('_!CheckRaschError3;TypePl=0'+';iMoney='+its(iMoney)+';iDopl='+its(iDopl)+';');
                prClearCheck1;  //��������� ���
                bBegCashEnd:=False;
                exit;
              end;
              TestFp('CheckRasch');
            end;

            sSave:=sSave+'1';

  //          CheckClose;
            if  BnStr>'' then
            begin
              if CheckCloseNoCut=False then   //��� ������
              begin
                prWriteLog('_!CheckCloseBad;');
                n:=5;
                TestFp('CheckClose');
                if Nums.iRet<>0 then
                begin
                  TestFp('CheckClose');
                  if Nums.iRet<>0 then
                  begin
                    while (CheckClose=False)and(n>0) do
                    begin
                      TestFp('CheckClose'); dec(n); delay(100);
                    end;
                    if n=0 then //������� �������� ������ � ������������ ���
                    begin
                      prClearCheck1; bBegCashEnd:=False; //��������� ���
                      exit;
                    end else prWriteLog('_!CheckClose;');
                  end;
                end;
              end else
              begin
                prWriteLog('_!CheckClose;'); TestFp('CheckClose');
              end;
            end else
            begin
              if CheckClose=False then   //� �������
              begin
                prWriteLog('_!CheckCloseBad;');
                n:=5;
                TestFp('CheckClose');
                if Nums.iRet<>0 then
                begin
                  TestFp('CheckClose');
                  if Nums.iRet<>0 then
                  begin
                    while (CheckClose=False)and(n>0) do
                    begin
                      TestFp('CheckClose'); dec(n); delay(100);
                    end;
                    if n=0 then //������� �������� ������ � ������������ ���
                    begin
                      prClearCheck1; bBegCashEnd:=False; //��������� ���
                      exit;
                    end else prWriteLog('_!CheckClose;');
                  end;
                end;
              end else
              begin
                prWriteLog('_!CheckClose;'); TestFp('CheckClose');
              end;
            end;
            //��� � �������� ������� - ������������ ������� � ���  �� ���������� � ���� �.�. ��� curcheck ��������
            if AlcoSet.UseUTM=1 then  //����� ����������� � ���
              if fTestAlcoInCh then
              begin
                fmMainCasher.Memo2.Lines.Add('�������� ���������� � �����.');
                if fSendAlco=False then fmMainCasher.Memo2.Lines.Add('������ ��������.')
                else fmMainCasher.Memo2.Lines.Add('�������� ��.');
              end;

            //���� ��� ������, � �� ��� �� ���� ����������� ��� ���� ���
            if CommonSet.BNManual=0 then
            if BnStr>'' then //���� ��� �������� �� �������
            begin
              bNotErr:=True;
              try
                if OpenNFDoc=false then
                begin
                  TestFp('PrintNFStr');
                  OpenNFDoc;
                end;
                SelectF(10);
                if bNotErr then bNotErr:=PrintNFStr('����� ��������. ��� �'+IntToStr(Nums.iCheckNum+1)) else TestFp('PrintNFStr');
                StrWk:=CommonSet.DepartName;
                while length(strwk)<35 do StrWk:=' '+StrWk;
                if bNotErr then bNotErr:=PrintNFStr('�����'+StrWk) else TestFp('PrintNFStr');
                if bNotErr then bNotErr:=PrintNFStr('      ') else TestFp('PrintNFStr');
                if bNotErr then bNotErr:=PrintNFStr('�����: '+INtToStr(Nums.ZNum)+'            ������: '+IntToStr(Person.Id)) else TestFp('PrintNFStr');
                if bNotErr then bNotErr:=PrintNFStr('') else TestFp('PrintNFStr');

                i:=0;
                Delete(BnStr,1,1); //������� |
                while (length(BnBar)>0)and(bNotErr=True)and(i<20) do
                begin
                  StrWk:=Copy(BnStr,1,Pos('||',BnStr)-1);
                  if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
                  if bNotErr then bNotErr:=PrintNFStr('') else TestFp('PrintNFStr');
                  Delete(BnStr,1,Pos('||',BnStr)+1);
                  inc(i); //�� ������ ������, �������� ����� �� �����
                end;

                if bNotErr then PrintNFStr(BnStr) else TestFp('PrintNFStr'); //������ �������� ������
              finally
                if CloseNFDoc=False then
                begin
                  TestFp('PrintNFStr');
                  CloseNFDoc;
                end;

                if (CommonSet.TypeFis='prim')or(CommonSet.TypeFis='shtrih') then
                begin
                  PrintNFStr(' ');
                  PrintNFStr(' ');
                  PrintNFStr(' ');
                  PrintNFStr(' ');
                  PrintNFStr(' ');
                  PrintNFStr(' ');
                  CutDoc;
                end;
                delay(33);
              end;
            end;

            sSave:=sSave+'2';
            sSave:=sSave+'3';

            if iArtDisc>0 then
            begin //�������� � ��� ������� ��
              with dmC do
              begin
                taCheck.Active:=False;
                taCheck.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
                taCheck.Active:=True;

                taCheck.Last;
                iMax:=taCheckNUMPOS.AsInteger+1; //����� ����� �������

                prWriteLog('_!AddPosDiscCard;'+its(iArtDisc)+';'+its(iMax));

                trUpdCh.StartTransaction;

                taCheck.Append;
                taCheckCASHNUM.AsInteger:=CommonSet.CashNum;
                taCheckNUMPOS.AsInteger:=iMax;
                taCheckRBAR.AsString:='';
                taCheckARTICUL.AsString:=its(iArtDisc);
                taCheckNAME.AsString:='';
                taCheckQUANT.AsFloat:=1;
                taCheckPRICE.AsFloat:=0;
                taCheckDPROC.AsFloat:=0;
                taCheckDSUM.AsFloat:=0;
                taCheckRSUM.AsFloat:=0;
                taCheckDepart.AsInteger:=CommonSet.DepartId;
                taCheckEDIZM.AsString:='';
                taCheckTIMEPOS.AsDateTime:=now;
                taCheckAMARK.AsString:='';
                taCheck.Post;

                trUpdCh.Commit;

                taCheck.Active:=False;
              end;
            end;

            prSaveCheck;

            if CommonSet.BNManual=1 then prBnPrint;

            if CommonSet.BNManual=2 then
            begin
              prWriteLog('');
              prWriteLog('������ ����� - ��c���');
              prWriteLog('-----------------');
              prWriteLog(BnStr);
              prWriteLog('-----------------');
              prWriteLog('');

              prBnPrint;
            end;

            if CommonSet.BNManual=3 then
            begin
              prWriteLog('');
              prWriteLog('������ ��� - ��c���');
              prWriteLog('-----------------');
              prWriteLog(BnStr);
              prWriteLog('-----------------');
              prWriteLog('');

              BnStr1:=BnStr;
              prBnPrint;

              if CommonSet.NumberBNCheck=2 then
              begin
                BnStr:=BnStr1;
                prBnPrint;
              end;  
            end;

            sSave:=sSave+'_EndPrintBn';


          finally
            prWriteLog('!!SaveCheck;'+sSave);
            taPay.Active:=False;
          end;

          prPrintQRCode;

  //        rMoney:=cxCurrencyEdit2.EditValue;
          fmMainCasher.TextEdit3.Text:='';
          if Check.Operation=0 then Check.Operation:=1; //������� �������� �������

        //����� ���� �����
  //        Nums.iRet:=InspectSt(Nums.sRet);
  //        TestStatus('InspectSt',sMessage);
  //        GetNums;

          WriteNums;
          if Nums.iCheckNum <3 then GetRes; //�������
          WriteStatus;

          inc(iStatus); //� ��������� �������
        end;

        Str((iTotal/100):9:2,StrWk1); While Length(StrWk1)<15 do StrWk1:=' '+StrWk1;
        StrWk:='';
        if cxCurrencyEdit3.EditValue>0 then
        begin
          rMoney:=cxCurrencyEdit3.EditValue;
          Str(rMoney:9:2,StrWk); While Length(StrWk)<15 do StrWk:=' '+StrWk;
          StrWk:='�����'+StrWk;
          ToCustomDisp('�����'+StrWk1,StrWk);
        end else
        begin
          prWriteLog('~!EndCheck;'+IntToStr(Nums.iCheckNum)+';');
  //        ToCustomDisp('      �������       ','    �� ������� !    ');
          ToCustomDisp('�����'+StrWk1,StrWk);
          CountSec:=0;
          fmMainCasher.Timer2.Enabled:=True;
          bBegCashEnd:=False;
          ModalResult:=mrOk;
        end;
        fmMainCasher.Timer3.Enabled:=True;

       { if cxCurrencyEdit3.EditValue=0 then //�����
        begin
          prWriteLog('~!EndCheck;'+IntToStr(Nums.iCheckNum)+';');
          ToCustomDisp('      �������       ','    �� ������� !    ');
          CountSec:=0;
          fmMainCasher.Timer2.Enabled:=True;
          bBegCashEnd:=False;
          ModalResult:=mrOk;
        end
        else
        begin
          rMoney:=cxCurrencyEdit3.EditValue;
          Str(rMoney:9:2,StrWk); While Length(StrWk)<15 do StrWk:=' '+StrWk;
          ToCustomDisp('                    ','�����'+StrWk);
        end;}
      end else //������ ������� - ������ �� ������ � ��.
      begin
        //�������� ������� ������������

      end;


      Nums.iRet:=InspectSt(Nums.sRet);
      WriteStatus;

      bBegCashEnd:=False;
    end;
  end;
end;

procedure TfmCash.cxCurrencyEdit2KeyPress(Sender: TObject; var Key: Char);
begin
  if (Key=#191)or(Key=#110)or(Key=#188)or(Key=#110)or(Key='.')or(Key='/')or(Key='�') then Key:=',';
end;

procedure TfmCash.cxCurrencyEdit1KeyPress(Sender: TObject; var Key: Char);
begin
  if (Key=#191)or(Key=#110)or(Key=#188)or(Key=#110)or(Key='.')or(Key='/')or(Key='�') then Key:=',';
end;

procedure TfmCash.cxButton11Click(Sender: TObject);
begin
  prAddNum(0);
end;

procedure TfmCash.cxButton2Click(Sender: TObject);
begin
  prAddNum(1);
end;

procedure TfmCash.cxButton3Click(Sender: TObject);
begin
  prAddNum(2);
end;

procedure TfmCash.cxButton4Click(Sender: TObject);
begin
  prAddNum(3);
end;

procedure TfmCash.cxButton5Click(Sender: TObject);
begin
  prAddNum(4);
end;

procedure TfmCash.cxButton6Click(Sender: TObject);
begin
  prAddNum(5);
end;

procedure TfmCash.cxButton7Click(Sender: TObject);
begin
  prAddNum(6);
end;

procedure TfmCash.cxButton8Click(Sender: TObject);
begin
  prAddNum(7);
end;

procedure TfmCash.cxButton9Click(Sender: TObject);
begin
  prAddNum(8);
end;

procedure TfmCash.cxButton10Click(Sender: TObject);
begin
  prAddNum(9);
end;

procedure TfmCash.cxButton12Click(Sender: TObject);
begin
  if pos(',',CalcEdit1.Text)=0 then CalcEdit1.Text:=CalcEdit1.Text+',';
  bFirst:=False;
end;

procedure TfmCash.cxButton14Click(Sender: TObject);
begin
  CalcEdit1.EditValue:=Trunc(CalcEdit1.EditValue*100/10)/100;
end;

procedure TfmCash.cxButton13Click(Sender: TObject);
begin
  CalcEdit1.EditValue:=0;
end;

procedure TfmCash.cxButton15Click(Sender: TObject);
Var rSum:Real;
begin
  if (CalcEdit1.EditValue <> 0)and(CalcEdit1.EditValue < 100000)  then
  begin
    try
      rSum:=rv(CalcEdit1.EditValue); // ���� ��� ����� ����� - �������� ''
      cxCurrencyEdit2.EditValue:=rSum;
      cxCurrencyEdit3.EditValue:=rSum-cxCurrencyEdit1.EditValue;
    except
      rSum:=0;
      cxCurrencyEdit3.EditValue:=-cxCurrencyEdit1.EditValue;
      cxCurrencyEdit2.EditValue:=rSum;
    end;
  end else
  begin
    CalcEdit1.EditValue:=0;
  end;

end;

end.
