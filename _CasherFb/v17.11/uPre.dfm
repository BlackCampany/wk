object fmPre: TfmPre
  Left = 519
  Top = 329
  BorderIcons = []
  BorderStyle = bsNone
  Caption = 'fmPre'
  ClientHeight = 301
  ClientWidth = 452
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Image1: TImage
    Left = 0
    Top = 0
    Width = 452
    Height = 301
    Align = alClient
    Proportional = True
    Stretch = True
  end
  object Label1: TLabel
    Left = 12
    Top = 276
    Width = 33
    Height = 13
    Caption = 'v12.12'
    Transparent = True
  end
  object Edit1: TcxTextEdit
    Left = 17
    Top = 176
    TabStop = False
    BeepOnEnter = False
    Style.BorderStyle = ebsNone
    TabOrder = 0
    Text = 'Edit1'
    OnKeyPress = Edit1KeyPress
    Width = 0
  end
  object amRegist: TActionManager
    Left = 280
    Top = 144
    StyleName = 'XP Style'
    object acReg: TAction
      Caption = 'acReg'
      ShortCut = 123
      SecondaryShortCuts.Strings = (
        #1078
        ';'
        'c')
      OnExecute = acRegExecute
    end
    object acStopExit: TAction
      Caption = 'acStopExit'
      ShortCut = 32883
      OnExecute = acStopExitExecute
    end
  end
end
