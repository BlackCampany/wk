object fmInOutM: TfmInOutM
  Left = 318
  Top = 229
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'fmInOutM'
  ClientHeight = 402
  ClientWidth = 440
  Color = 14941884
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label4: TLabel
    Left = 232
    Top = 304
    Width = 40
    Height = 13
    Caption = #1057#1091#1084#1084#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label5: TLabel
    Left = 288
    Top = 296
    Width = 144
    Height = 24
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'Label5'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -24
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object Label9: TLabel
    Left = 16
    Top = 304
    Width = 67
    Height = 13
    Caption = #1054#1089#1090#1072#1083#1100#1085#1099#1077
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Panel1: TPanel
    Left = 0
    Top = 344
    Width = 440
    Height = 58
    Align = alBottom
    BevelInner = bvLowered
    Color = 5936386
    TabOrder = 2
    object Button1: TcxButton
      Left = 80
      Top = 16
      Width = 113
      Height = 25
      Caption = #1054#1082
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ModalResult = 1
      ParentFont = False
      TabOrder = 0
      Colors.Normal = 16449510
      LookAndFeel.Kind = lfFlat
    end
    object Button2: TcxButton
      Left = 248
      Top = 16
      Width = 113
      Height = 25
      Caption = #1054#1090#1084#1077#1085#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = Button2Click
      Colors.Normal = 16449510
      Glyph.Data = {
        5E040000424D5E04000000000000360000002800000012000000130000000100
        18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
        8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
        0000CED3D6848684848684848684848684848684848684848684848684848684
        848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
        7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
        FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
        FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
        007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
        FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
        00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
        494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
        00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000}
      LookAndFeel.Kind = lfFlat
    end
  end
  object Panel2: TPanel
    Left = 240
    Top = 16
    Width = 185
    Height = 121
    BevelInner = bvLowered
    Color = 13237880
    TabOrder = 3
    object Label1: TLabel
      Left = 16
      Top = 16
      Width = 50
      Height = 13
      Caption = #1054#1089#1090#1072#1090#1086#1082
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label2: TLabel
      Left = 16
      Top = 48
      Width = 51
      Height = 13
      Caption = #1042#1099#1088#1091#1095#1082#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label3: TLabel
      Left = 16
      Top = 88
      Width = 36
      Height = 13
      Caption = #1048#1090#1086#1075#1086
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label6: TLabel
      Left = 88
      Top = 14
      Width = 81
      Height = 16
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Label6'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label7: TLabel
      Left = 88
      Top = 46
      Width = 81
      Height = 16
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Label7'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label8: TLabel
      Left = 88
      Top = 86
      Width = 81
      Height = 16
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Label8'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
  end
  object Grid: TcxGrid
    Left = 16
    Top = 16
    Width = 193
    Height = 257
    TabOrder = 0
    LookAndFeel.Kind = lfFlat
    object ViewMoney: TcxGridDBTableView
      OnKeyDown = ViewMoneyKeyDown
      NavigatorButtons.ConfirmDelete = False
      OnEditChanged = ViewMoneyEditChanged
      DataController.DataSource = dsInOutM
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Kind = skSum
          FieldName = 'iQuant'
          Column = ViewMoneyiQuant
        end>
      DataController.Summary.SummaryGroups = <>
      DataController.OnDataChanged = ViewDataControllerDataChanged
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnGrouping = False
      OptionsCustomize.ColumnMoving = False
      OptionsCustomize.ColumnSorting = False
      OptionsSelection.InvertSelect = False
      OptionsSelection.UnselectFocusedRecordOnExit = False
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      Styles.Footer = cxStyle1
      Styles.Header = cxStyle1
      object ViewMoneyIVal: TcxGridDBColumn
        DataBinding.FieldName = 'IVal'
        Visible = False
      end
      object ViewMoneyrVal: TcxGridDBColumn
        Caption = #1050#1091#1087#1102#1088#1072
        DataBinding.FieldName = 'rVal'
        Options.Editing = False
        Options.Focusing = False
        Width = 97
      end
      object ViewMoneyiQuant: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086
        DataBinding.FieldName = 'iQuant'
        PropertiesClassName = 'TcxSpinEditProperties'
        Properties.MaxValue = 1000.000000000000000000
        Width = 85
      end
    end
    object Level: TcxGridLevel
      GridView = ViewMoney
    end
  end
  object CurrencyEdit1: TcxCurrencyEdit
    Left = 96
    Top = 300
    EditValue = 0c
    Properties.Alignment.Horz = taRightJustify
    Properties.MaxValue = 90000000.000000000000000000
    Properties.OnChange = CurrencyEdit1PropertiesChange
    Style.BorderStyle = ebsFlat
    TabOrder = 1
    OnEnter = CurrencyEdit1Enter
    OnKeyPress = CurrencyEdit1KeyPress
    BiDiMode = bdLeftToRight
    ParentBiDiMode = False
    Width = 113
  end
  object acInOut: TActionManager
    Left = 360
    Top = 328
    StyleName = 'XP Style'
    object acClose: TAction
      Caption = 'acClose'
      ShortCut = 121
      OnExecute = acCloseExecute
    end
  end
  object taInOutM: TClientDataSet
    Aggregates = <>
    Params = <>
    AfterPost = taInOutMAfterPost
    Left = 240
    Top = 176
    object taInOutMIVal: TIntegerField
      FieldName = 'IVal'
    end
    object taInOutMiQuant: TIntegerField
      FieldName = 'iQuant'
    end
    object taInOutMrVal: TCurrencyField
      FieldName = 'rVal'
    end
  end
  object dsInOutM: TDataSource
    DataSet = taInOutM
    Left = 240
    Top = 232
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 344
    Top = 176
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = 15531993
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
    end
  end
  object dxfBackGround1: TdxfBackGround
    BkColor.BeginColor = 14941884
    BkColor.EndColor = 5936386
    BkColor.FillStyle = fsVert
    BkAnimate.Speed = 700
    Left = 320
    Top = 224
  end
end
