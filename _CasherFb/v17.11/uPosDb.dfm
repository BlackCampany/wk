object dmPOSDB: TdmPOSDB
  OldCreateOrder = False
  Left = 1146
  Top = 283
  Height = 484
  Width = 657
  object msConnPosDb: TADOConnection
    CommandTimeout = 800
    ConnectionString = 'FILE NAME=E:\_CasherFb\Bin\PosDb.udl'
    ConnectionTimeout = 800
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 24
    Top = 17
  end
  object quSetActive: TADOQuery
    Connection = msConnPosDb
    Parameters = <>
    Left = 120
    Top = 16
  end
  object quSelLoad: TADOQuery
    Connection = msConnPosDb
    Parameters = <>
    SQL.Strings = (
      
        'SELECT top 100 ID,IACTIVE,ITYPE,ARTICUL,BARCODE,CARDSIZE,QUANT,D' +
        'STOP,NAME,MESSUR,PRESISION,'
      
        'ADD1,ADD2,ADD3,ADDNUM1,ADDNUM2,ADDNUM3,SCALE,GROUP1,GROUP2,GROUP' +
        '3,PRICE,CLIINDEX,DELETED,PASSW'
      'FROM CASH01'
      'where IACTIVE=2')
    Left = 200
    Top = 16
    object quSelLoadID: TLargeintField
      FieldName = 'ID'
      ReadOnly = True
    end
    object quSelLoadIACTIVE: TSmallintField
      FieldName = 'IACTIVE'
    end
    object quSelLoadITYPE: TSmallintField
      FieldName = 'ITYPE'
    end
    object quSelLoadARTICUL: TIntegerField
      FieldName = 'ARTICUL'
    end
    object quSelLoadBARCODE: TStringField
      FieldName = 'BARCODE'
      Size = 15
    end
    object quSelLoadCARDSIZE: TStringField
      FieldName = 'CARDSIZE'
      Size = 10
    end
    object quSelLoadQUANT: TFloatField
      FieldName = 'QUANT'
    end
    object quSelLoadDSTOP: TFloatField
      FieldName = 'DSTOP'
    end
    object quSelLoadNAME: TStringField
      FieldName = 'NAME'
      Size = 80
    end
    object quSelLoadMESSUR: TStringField
      FieldName = 'MESSUR'
      Size = 5
    end
    object quSelLoadPRESISION: TFloatField
      FieldName = 'PRESISION'
    end
    object quSelLoadADD1: TStringField
      FieldName = 'ADD1'
    end
    object quSelLoadADD2: TStringField
      FieldName = 'ADD2'
    end
    object quSelLoadADD3: TStringField
      FieldName = 'ADD3'
    end
    object quSelLoadADDNUM1: TFloatField
      FieldName = 'ADDNUM1'
    end
    object quSelLoadADDNUM2: TFloatField
      FieldName = 'ADDNUM2'
    end
    object quSelLoadADDNUM3: TFloatField
      FieldName = 'ADDNUM3'
    end
    object quSelLoadSCALE: TStringField
      FieldName = 'SCALE'
      Size = 10
    end
    object quSelLoadGROUP1: TIntegerField
      FieldName = 'GROUP1'
    end
    object quSelLoadGROUP2: TIntegerField
      FieldName = 'GROUP2'
    end
    object quSelLoadGROUP3: TIntegerField
      FieldName = 'GROUP3'
    end
    object quSelLoadPRICE: TFloatField
      FieldName = 'PRICE'
    end
    object quSelLoadCLIINDEX: TIntegerField
      FieldName = 'CLIINDEX'
    end
    object quSelLoadDELETED: TSmallintField
      FieldName = 'DELETED'
    end
    object quSelLoadPASSW: TStringField
      FieldName = 'PASSW'
    end
  end
  object quE: TADOQuery
    Connection = msConnPosDb
    Parameters = <>
    Left = 272
    Top = 16
  end
end
