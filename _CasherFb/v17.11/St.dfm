object fmSt: TfmSt
  Left = 337
  Top = 398
  Width = 424
  Height = 421
  Caption = #1057#1090#1072#1090#1091#1089' '#1086#1087#1077#1088#1072#1094#1080#1080
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 368
    Width = 416
    Height = 19
    Panels = <
      item
        Width = 250
      end
      item
        Width = 50
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 316
    Width = 416
    Height = 52
    Align = alBottom
    BevelInner = bvLowered
    Color = 11796326
    TabOrder = 1
    object cxButton1: TcxButton
      Left = 148
      Top = 12
      Width = 137
      Height = 33
      Caption = 'Ok'
      Default = True
      TabOrder = 0
      OnClick = cxButton1Click
      LookAndFeel.Kind = lfOffice11
    end
  end
  object Memo1: TcxMemo
    Left = 16
    Top = 20
    TabStop = False
    Lines.Strings = (
      'Memo1')
    Properties.ScrollBars = ssVertical
    Style.BorderStyle = ebsOffice11
    Style.LookAndFeel.Kind = lfOffice11
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 2
    Height = 249
    Width = 365
  end
end
