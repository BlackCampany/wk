program Administrator;

uses
  Forms,
  MainAdm in 'MainAdm.pas' {fmMainAdm},
  Dm in 'Dm.pas' {dmC: TDataModule},
  Passw in 'Passw.pas' {fmPerA},
  Right in 'Right.pas' {fmRight},
  Un1 in 'Un1.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfmMainAdm, fmMainAdm);
  Application.CreateForm(TdmC, dmC);
  Application.CreateForm(TfmPerA, fmPerA);
  Application.CreateForm(TfmRight, fmRight);
  Application.Run;
end.
