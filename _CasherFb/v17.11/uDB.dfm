object dmCasher: TdmCasher
  OldCreateOrder = False
  Left = 297
  Top = 319
  Height = 401
  Width = 695
  object dsCF_Operations: TDataSource
    DataSet = quCF_Operations
    Left = 128
    Top = 184
  end
  object dsOperations: TDataSource
    DataSet = taOperations
    Left = 96
    Top = 72
  end
  object dsStates: TDataSource
    DataSet = taStates
    Left = 168
    Top = 72
  end
  object dsCF_Classif: TDataSource
    DataSet = quCF_Classif
    Left = 208
    Top = 184
  end
  object dsCardScla: TDataSource
    DataSet = quCardScla
    Left = 288
    Top = 184
  end
  object dsCF_CardScla: TDataSource
    DataSet = quCF_CardScla
    Left = 368
    Top = 184
  end
  object dsDepart: TDataSource
    DataSet = taDeparts
    Left = 496
    Top = 72
  end
  object dsCF_Depart: TDataSource
    DataSet = quCF_Depart
    Left = 448
    Top = 184
  end
  object dbCasher: TpFIBDatabase
    DBName = 'C:\Projects\Casher\FB\DB\CasherDb.gdb'
    DBParams.Strings = (
      'user_name=SYSDBA'
      'password=masterkey'
      'lc_ctype=WIN1251')
    DefaultTransaction = trRead
    DefaultUpdateTransaction = trUpdate
    SQLDialect = 3
    Timeout = 0
    UpperOldNames = True
    SynchronizeTime = False
    DesignDBOptions = [ddoIsDefaultDatabase]
    AliasName = 'CasherDb'
    WaitForRestoreConnect = 0
    Left = 24
    Top = 16
  end
  object trRead: TpFIBTransaction
    DefaultDatabase = dbCasher
    TimeoutAction = TARollback
    Left = 24
    Top = 72
  end
  object trUpdate: TpFIBTransaction
    DefaultDatabase = dbCasher
    TimeoutAction = TARollback
    Left = 24
    Top = 128
  end
  object taOperations: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OPERATIONS'
      'SET '
      '    NAME = :NAME,'
      '    ID_DEVICE = :ID_DEVICE'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OPERATIONS'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OPERATIONS('
      '    ID,'
      '    NAME,'
      '    ID_DEVICE'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAME,'
      '    :ID_DEVICE'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    NAME,'
      '    ID_DEVICE'
      'FROM'
      '    OPERATIONS '
      ' WHERE '
      '        OPERATIONS.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    NAME,'
      '    ID_DEVICE'
      'FROM'
      '    OPERATIONS ')
    Transaction = trRead
    Database = dbCasher
    UpdateTransaction = trUpdate
    AutoCommit = True
    Left = 96
    Top = 16
    object taOperationsID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taOperationsNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 30
      EmptyStrToNull = True
    end
    object taOperationsID_DEVICE: TFIBIntegerField
      FieldName = 'ID_DEVICE'
    end
  end
  object taStates: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE STATES'
      'SET '
      '    NAME = :NAME,'
      '    BARCODE = :BARCODE,'
      '    KEY_POSITION = :KEY_POSITION,'
      '    ID_DEVICE = :ID_DEVICE'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    STATES'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO STATES('
      '    ID,'
      '    NAME,'
      '    BARCODE,'
      '    KEY_POSITION,'
      '    ID_DEVICE'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAME,'
      '    :BARCODE,'
      '    :KEY_POSITION,'
      '    :ID_DEVICE'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    NAME,'
      '    BARCODE,'
      '    KEY_POSITION,'
      '    ID_DEVICE'
      'FROM'
      '    STATES '
      ' WHERE '
      '        STATES.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    NAME,'
      '    BARCODE,'
      '    KEY_POSITION,'
      '    ID_DEVICE'
      'FROM'
      '    STATES ')
    Transaction = trRead
    Database = dbCasher
    UpdateTransaction = trUpdate
    AutoCommit = True
    Left = 168
    Top = 16
    object taStatesID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taStatesNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 30
      EmptyStrToNull = True
    end
    object taStatesBARCODE: TFIBStringField
      FieldName = 'BARCODE'
      Size = 30
      EmptyStrToNull = True
    end
    object taStatesKEY_POSITION: TFIBIntegerField
      FieldName = 'KEY_POSITION'
    end
    object taStatesID_DEVICE: TFIBIntegerField
      FieldName = 'ID_DEVICE'
    end
  end
  object taCF_Operations: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE CF_OPERATIONS'
      'SET '
      '    KEY_CHAR = :KEY_CHAR,'
      '    ID_OPERATIONS = :ID_OPERATIONS,'
      '    ID_STATES = :ID_STATES,'
      '    BARCODE = :BARCODE,'
      '    KEY_POSITION = :KEY_POSITION,'
      '    ID_CLASSIF = :ID_CLASSIF,'
      '    ARTICUL = :ARTICUL,'
      '    ID_DEPARTS = :ID_DEPARTS,'
      '    BAR = :BAR'
      'WHERE'
      '    KEY_CODE = :OLD_KEY_CODE'
      '    and KEY_STATUS = :OLD_KEY_STATUS'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    CF_OPERATIONS'
      'WHERE'
      '        KEY_CODE = :OLD_KEY_CODE'
      '    and KEY_STATUS = :OLD_KEY_STATUS'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO CF_OPERATIONS('
      '    KEY_CODE,'
      '    KEY_STATUS,'
      '    KEY_CHAR,'
      '    ID_OPERATIONS,'
      '    ID_STATES,'
      '    BARCODE,'
      '    KEY_POSITION,'
      '    ID_CLASSIF,'
      '    ARTICUL,'
      '    ID_DEPARTS,'
      '    BAR'
      ')'
      'VALUES('
      '    :KEY_CODE,'
      '    :KEY_STATUS,'
      '    :KEY_CHAR,'
      '    :ID_OPERATIONS,'
      '    :ID_STATES,'
      '    :BARCODE,'
      '    :KEY_POSITION,'
      '    :ID_CLASSIF,'
      '    :ARTICUL,'
      '    :ID_DEPARTS,'
      '    :BAR'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    KEY_CODE,'
      '    KEY_STATUS,'
      '    KEY_CHAR,'
      '    ID_OPERATIONS,'
      '    ID_STATES,'
      '    BARCODE,'
      '    KEY_POSITION,'
      '    ID_CLASSIF,'
      '    ARTICUL,'
      '    ID_DEPARTS,'
      '    BAR'
      'FROM'
      '    CF_OPERATIONS '
      ' WHERE '
      '        CF_OPERATIONS.KEY_CODE = :OLD_KEY_CODE'
      '    and CF_OPERATIONS.KEY_STATUS = :OLD_KEY_STATUS'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    KEY_CODE,'
      '    KEY_STATUS,'
      '    KEY_CHAR,'
      '    ID_OPERATIONS,'
      '    ID_STATES,'
      '    BARCODE,'
      '    KEY_POSITION,'
      '    ID_CLASSIF,'
      '    ARTICUL,'
      '    ID_DEPARTS,'
      '    BAR'
      'FROM'
      '    CF_OPERATIONS ')
    Transaction = trRead
    Database = dbCasher
    UpdateTransaction = trUpdate
    AutoCommit = True
    Left = 248
    Top = 16
    object taCF_OperationsKEY_CODE: TFIBIntegerField
      FieldName = 'KEY_CODE'
    end
    object taCF_OperationsKEY_STATUS: TFIBIntegerField
      FieldName = 'KEY_STATUS'
    end
    object taCF_OperationsKEY_CHAR: TFIBStringField
      FieldName = 'KEY_CHAR'
      Size = 10
      EmptyStrToNull = True
    end
    object taCF_OperationsID_OPERATIONS: TFIBIntegerField
      FieldName = 'ID_OPERATIONS'
    end
    object taCF_OperationsID_STATES: TFIBIntegerField
      FieldName = 'ID_STATES'
    end
    object taCF_OperationsBARCODE: TFIBStringField
      FieldName = 'BARCODE'
      Size = 30
      EmptyStrToNull = True
    end
    object taCF_OperationsKEY_POSITION: TFIBIntegerField
      FieldName = 'KEY_POSITION'
    end
    object taCF_OperationsID_CLASSIF: TFIBIntegerField
      FieldName = 'ID_CLASSIF'
    end
    object taCF_OperationsARTICUL: TFIBStringField
      FieldName = 'ARTICUL'
      Size = 30
      EmptyStrToNull = True
    end
    object taCF_OperationsID_DEPARTS: TFIBIntegerField
      FieldName = 'ID_DEPARTS'
    end
    object taCF_OperationsBAR: TFIBStringField
      FieldName = 'BAR'
      Size = 15
      EmptyStrToNull = True
    end
  end
  object taClassif: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE CLASSIF'
      'SET '
      '    ID_PARENT = :ID_PARENT,'
      '    TYPE_CLASSIF = :TYPE_CLASSIF,'
      '    NAME = :NAME'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    CLASSIF'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO CLASSIF('
      '    ID,'
      '    ID_PARENT,'
      '    TYPE_CLASSIF,'
      '    NAME'
      ')'
      'VALUES('
      '    :ID,'
      '    :ID_PARENT,'
      '    :TYPE_CLASSIF,'
      '    :NAME'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    ID_PARENT,'
      '    TYPE_CLASSIF,'
      '    NAME'
      'FROM'
      '    CLASSIF '
      ' WHERE '
      '        CLASSIF.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    ID_PARENT,'
      '    TYPE_CLASSIF,'
      '    NAME'
      'FROM'
      '    CLASSIF ')
    Transaction = trRead
    Database = dbCasher
    UpdateTransaction = trUpdate
    AutoCommit = True
    Left = 328
    Top = 16
    object taClassifID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taClassifID_PARENT: TFIBIntegerField
      FieldName = 'ID_PARENT'
    end
    object taClassifTYPE_CLASSIF: TFIBIntegerField
      FieldName = 'TYPE_CLASSIF'
    end
    object taClassifNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 40
      EmptyStrToNull = True
    end
  end
  object taCardScla: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE CARDSCLA'
      'SET '
      '    CLASSIF = :CLASSIF,'
      '    DEPART = :DEPART,'
      '    NAME = :NAME,'
      '    CARD_TYPE = :CARD_TYPE,'
      '    MESURIMENT = :MESURIMENT,'
      '    PRICE_RUB = :PRICE_RUB,'
      '    DISCOUNT = :DISCOUNT,'
      '    SCALE = :SCALE'
      'WHERE'
      '    ARTICUL = :OLD_ARTICUL'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    CARDSCLA'
      'WHERE'
      '        ARTICUL = :OLD_ARTICUL'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO CARDSCLA('
      '    ARTICUL,'
      '    CLASSIF,'
      '    DEPART,'
      '    NAME,'
      '    CARD_TYPE,'
      '    MESURIMENT,'
      '    PRICE_RUB,'
      '    DISCOUNT,'
      '    SCALE'
      ')'
      'VALUES('
      '    :ARTICUL,'
      '    :CLASSIF,'
      '    :DEPART,'
      '    :NAME,'
      '    :CARD_TYPE,'
      '    :MESURIMENT,'
      '    :PRICE_RUB,'
      '    :DISCOUNT,'
      '    :SCALE'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ARTICUL,'
      '    CLASSIF,'
      '    DEPART,'
      '    NAME,'
      '    CARD_TYPE,'
      '    MESURIMENT,'
      '    PRICE_RUB,'
      '    DISCOUNT,'
      '    SCALE'
      'FROM'
      '    CARDSCLA '
      ' WHERE '
      '        CARDSCLA.ARTICUL = :OLD_ARTICUL'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    ARTICUL,'
      '    CLASSIF,'
      '    DEPART,'
      '    NAME,'
      '    CARD_TYPE,'
      '    MESURIMENT,'
      '    PRICE_RUB,'
      '    DISCOUNT,'
      '    SCALE'
      'FROM'
      '    CARDSCLA ')
    Transaction = trRead
    Database = dbCasher
    UpdateTransaction = trUpdate
    AutoCommit = True
    Left = 408
    Top = 16
    object taCardSclaARTICUL: TFIBStringField
      FieldName = 'ARTICUL'
      Size = 30
      EmptyStrToNull = True
    end
    object taCardSclaCLASSIF: TFIBIntegerField
      FieldName = 'CLASSIF'
    end
    object taCardSclaDEPART: TFIBIntegerField
      FieldName = 'DEPART'
    end
    object taCardSclaNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 80
      EmptyStrToNull = True
    end
    object taCardSclaMESURIMENT: TFIBStringField
      FieldName = 'MESURIMENT'
      Size = 10
      EmptyStrToNull = True
    end
    object taCardSclaCARD_TYPE: TFIBIntegerField
      FieldName = 'CARD_TYPE'
    end
    object taCardSclaPRICE_RUB: TFIBFloatField
      FieldName = 'PRICE_RUB'
    end
    object taCardSclaDISCOUNT: TFIBFloatField
      FieldName = 'DISCOUNT'
    end
    object taCardSclaSCALE: TFIBStringField
      FieldName = 'SCALE'
      Size = 10
      EmptyStrToNull = True
    end
  end
  object taDeparts: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE DEPARTS'
      'SET '
      '    NAME = :NAME,'
      '    PAR1 = :PAR1,'
      '    PAR2 = :PAR2'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    DEPARTS'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO DEPARTS('
      '    ID,'
      '    NAME,'
      '    PAR1,'
      '    PAR2'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAME,'
      '    :PAR1,'
      '    :PAR2'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    NAME,'
      '    PAR1,'
      '    PAR2'
      'FROM'
      '    DEPARTS '
      ' WHERE '
      '        DEPARTS.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    NAME,'
      '    PAR1,'
      '    PAR2'
      'FROM'
      '    DEPARTS ')
    Transaction = trRead
    Database = dbCasher
    UpdateTransaction = trUpdate
    AutoCommit = True
    Left = 496
    Top = 16
    object taDepartsID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taDepartsNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 40
      EmptyStrToNull = True
    end
    object taDepartsPAR1: TFIBIntegerField
      FieldName = 'PAR1'
    end
    object taDepartsPAR2: TFIBIntegerField
      FieldName = 'PAR2'
    end
  end
  object quCF_Operations: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select a.ID_OPERATIONS, a.KEY_CODE, a.KEY_STATUS, a.KEY_CHAR, b.' +
        'NAME, c.NAME, a.BARCODE, a.KEY_POSITION'
      'from CF_OPERATIONS a, OPERATIONS b, STATES c'
      'where'
      'b.ID=a.ID_OPERATIONS'
      'and c.ID=a.ID_STATES'
      'and a.KEY_STATUS=0'
      'order by a.ID_OPERATIONS')
    Transaction = trRead
    Database = dbCasher
    UpdateTransaction = trUpdate
    AutoCommit = True
    Left = 128
    Top = 128
    object quCF_OperationsID_OPERATIONS: TFIBIntegerField
      FieldName = 'ID_OPERATIONS'
    end
    object quCF_OperationsKEY_CODE: TFIBIntegerField
      FieldName = 'KEY_CODE'
    end
    object quCF_OperationsKEY_STATUS: TFIBIntegerField
      FieldName = 'KEY_STATUS'
    end
    object quCF_OperationsKEY_CHAR: TFIBStringField
      FieldName = 'KEY_CHAR'
      Size = 10
      EmptyStrToNull = True
    end
    object quCF_OperationsNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 30
      EmptyStrToNull = True
    end
    object quCF_OperationsNAME1: TFIBStringField
      FieldName = 'NAME1'
      Size = 30
      EmptyStrToNull = True
    end
    object quCF_OperationsBARCODE: TFIBStringField
      FieldName = 'BARCODE'
      Size = 30
      EmptyStrToNull = True
    end
    object quCF_OperationsKEY_POSITION: TFIBIntegerField
      FieldName = 'KEY_POSITION'
    end
  end
  object quCF_Classif: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select b.NAME, a.ID_CLASSIF, a.KEY_CODE, a.KEY_CHAR, a.KEY_STATU' +
        'S'
      'from CF_OPERATIONS a, CLASSIF b'
      'where'
      'b.ID=a.ID_CLASSIF'
      'and a.KEY_STATUS=1'
      'order by b.NAME')
    Transaction = trRead
    Database = dbCasher
    UpdateTransaction = trUpdate
    AutoCommit = True
    Left = 208
    Top = 128
    object quCF_ClassifNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 40
      EmptyStrToNull = True
    end
    object quCF_ClassifID_CLASSIF: TFIBIntegerField
      FieldName = 'ID_CLASSIF'
    end
    object quCF_ClassifKEY_CODE: TFIBIntegerField
      FieldName = 'KEY_CODE'
    end
    object quCF_ClassifKEY_CHAR: TFIBStringField
      FieldName = 'KEY_CHAR'
      Size = 10
      EmptyStrToNull = True
    end
    object quCF_ClassifKEY_STATUS: TFIBIntegerField
      FieldName = 'KEY_STATUS'
    end
  end
  object quCardScla: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select b.BARCODE, b.CARDARTICUL, b.QUANTITY, b.PRICERUB, b.CARDS' +
        'IZE'
      ', A.PRICE_RUB, A.NAME, A.MESURIMENT, A.DISCOUNT'
      'FROM BAR B, CARDSCLA A'
      'where B.CARDARTICUL=A.ARTICUL')
    Transaction = trRead
    Database = dbCasher
    UpdateTransaction = trUpdate
    AutoCommit = True
    Left = 288
    Top = 128
    object quCardSclaBARCODE: TFIBStringField
      FieldName = 'BARCODE'
      Size = 14
      EmptyStrToNull = True
    end
    object quCardSclaCARDARTICUL: TFIBStringField
      FieldName = 'CARDARTICUL'
      Size = 30
      EmptyStrToNull = True
    end
    object quCardSclaQUANTITY: TFIBIntegerField
      FieldName = 'QUANTITY'
    end
    object quCardSclaPRICERUB: TFIBFloatField
      FieldName = 'PRICERUB'
    end
    object quCardSclaCARDSIZE: TFIBStringField
      FieldName = 'CARDSIZE'
      Size = 10
      EmptyStrToNull = True
    end
    object quCardSclaPRICE_RUB: TFIBFloatField
      FieldName = 'PRICE_RUB'
    end
    object quCardSclaNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 80
      EmptyStrToNull = True
    end
    object quCardSclaMESURIMENT: TFIBStringField
      FieldName = 'MESURIMENT'
      Size = 10
      EmptyStrToNull = True
    end
    object quCardSclaDISCOUNT: TFIBFloatField
      FieldName = 'DISCOUNT'
    end
  end
  object quCF_CardScla: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select b.NAME, b.Price_Rub, b.Mesuriment, a.ARTICUL, a.BAR, a.KE' +
        'Y_CODE, a.KEY_CHAR, a.KEY_STATUS'
      'from CF_OPERATIONS a, CARDSCLA b'
      'where'
      'b.ARTICUL=a.ARTICUL'
      'and a.KEY_STATUS=2'
      'order by b.NAME')
    Transaction = trRead
    Database = dbCasher
    UpdateTransaction = trUpdate
    AutoCommit = True
    Left = 368
    Top = 128
    object quCF_CardSclaNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 80
      EmptyStrToNull = True
    end
    object quCF_CardSclaPRICE_RUB: TFIBFloatField
      FieldName = 'PRICE_RUB'
    end
    object quCF_CardSclaMESURIMENT: TFIBStringField
      FieldName = 'MESURIMENT'
      Size = 10
      EmptyStrToNull = True
    end
    object quCF_CardSclaARTICUL: TFIBStringField
      FieldName = 'ARTICUL'
      Size = 30
      EmptyStrToNull = True
    end
    object quCF_CardSclaBAR: TFIBStringField
      FieldName = 'BAR'
      Size = 15
      EmptyStrToNull = True
    end
    object quCF_CardSclaKEY_CODE: TFIBIntegerField
      FieldName = 'KEY_CODE'
    end
    object quCF_CardSclaKEY_CHAR: TFIBStringField
      FieldName = 'KEY_CHAR'
      Size = 10
      EmptyStrToNull = True
    end
    object quCF_CardSclaKEY_STATUS: TFIBIntegerField
      FieldName = 'KEY_STATUS'
    end
  end
  object quCF_Depart: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select b.NAME, a.ID_DEPARTS, a.KEY_CODE, a.KEY_CHAR, a.KEY_STATU' +
        'S'
      'from CF_OPERATIONS a, DEPARTS b'
      'where'
      'b.ID=a.ID_DEPARTS'
      'and a.KEY_STATUS=3'
      'order by b.NAME'
      '')
    Transaction = trRead
    Database = dbCasher
    UpdateTransaction = trUpdate
    AutoCommit = True
    Left = 448
    Top = 128
    object quCF_DepartNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 40
      EmptyStrToNull = True
    end
    object quCF_DepartID_DEPARTS: TFIBIntegerField
      FieldName = 'ID_DEPARTS'
    end
    object quCF_DepartKEY_CODE: TFIBIntegerField
      FieldName = 'KEY_CODE'
    end
    object quCF_DepartKEY_CHAR: TFIBStringField
      FieldName = 'KEY_CHAR'
      Size = 10
      EmptyStrToNull = True
    end
    object quCF_DepartKEY_STATUS: TFIBIntegerField
      FieldName = 'KEY_STATUS'
    end
  end
end
