object fmTrans2: TfmTrans2
  Left = 277
  Top = 307
  Width = 361
  Height = 217
  Caption = #1055#1086#1074#1090#1086#1088#1085#1072#1103' '#1074#1099#1075#1088#1091#1079#1082#1072
  Color = 13539951
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 345
    Height = 61
    Align = alTop
    Caption = '  '#1055#1086' '#1076#1072#1090#1077'  '
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 188
      Top = 12
      Width = 148
      Height = 40
      Caption = #1042#1099#1075#1088#1091#1079#1082#1072' '#1087#1086' '#1076#1072#1090#1077
      ModalResult = 1
      TabOrder = 1
      LookAndFeel.Kind = lfOffice11
    end
    object cxDateEdit1: TcxDateEdit
      Left = 20
      Top = 24
      Style.BorderStyle = ebsOffice11
      TabOrder = 0
      Width = 137
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 61
    Width = 345
    Height = 61
    Align = alTop
    Caption = '  '#1055#1086' '#1089#1084#1077#1085#1077'  '
    TabOrder = 1
    object cxButton2: TcxButton
      Left = 188
      Top = 12
      Width = 148
      Height = 40
      Caption = #1042#1099#1075#1088#1091#1079#1082#1072' '#1087#1086' '#8470' '#1089#1084#1077#1085#1099
      ModalResult = 6
      TabOrder = 1
      LookAndFeel.Kind = lfOffice11
    end
    object cxSpinEdit1: TcxSpinEdit
      Left = 24
      Top = 24
      Style.BorderStyle = ebsOffice11
      TabOrder = 0
      Width = 121
    end
  end
  object cxButton3: TcxButton
    Left = 188
    Top = 132
    Width = 148
    Height = 40
    Caption = #1054#1090#1084#1077#1085#1072
    TabOrder = 2
    OnClick = cxButton3Click
    LookAndFeel.Kind = lfOffice11
  end
  object amTrans2: TActionManager
    Left = 92
    Top = 128
    StyleName = 'XP Style'
    object acFocusNext: TAction
      Caption = 'acFocusNext'
      ShortCut = 40
      OnExecute = acFocusNextExecute
    end
    object acFocusPre: TAction
      Caption = 'acFocusPre'
      ShortCut = 38
      OnExecute = acFocusPreExecute
    end
    object acLow: TAction
      Caption = 'acLow'
      ShortCut = 37
      OnExecute = acLowExecute
    end
    object acHi: TAction
      Caption = 'acHi'
      ShortCut = 39
      OnExecute = acHiExecute
    end
  end
end
