unit MainStdConv;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, RXShell, Menus, ImgList, ExtCtrls, DB, DBTables;

type
  TfmMainConvStd = class(TForm)
    RxTray: TRxTrayIcon;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Timer1: TTimer;
    N2: TMenuItem;
    N3: TMenuItem;
    taCard: TTable;
    taCardArticul: TStringField;
    taCardName: TStringField;
    taCardMesuriment: TStringField;
    taCardMesPresision: TFloatField;
    taCardAdd1: TStringField;
    taCardAdd2: TStringField;
    taCardAdd3: TStringField;
    taCardAddNum1: TFloatField;
    taCardAddNum2: TFloatField;
    taCardAddNum3: TFloatField;
    taCardScale: TStringField;
    taCardGroop1: TSmallintField;
    taCardGroop2: TSmallintField;
    taCardGroop3: TSmallintField;
    taCardGroop4: TSmallintField;
    taCardGroop5: TSmallintField;
    taCardPriceRub: TCurrencyField;
    taCardPriceCur: TCurrencyField;
    taCardClientIndex: TSmallintField;
    taCardCommentary: TStringField;
    taCardDeleted: TSmallintField;
    taCardModDate: TDateField;
    taCardModTime: TSmallintField;
    taCardModPersonIndex: TSmallintField;
    taBar: TTable;
    tbTax: TTable;
    tbTaxCardArticul: TStringField;
    tbTaxTaxIndex: TSmallintField;
    tbTaxTax: TFloatField;
    tbTaxTaxSumRub: TFloatField;
    tbTaxTaxSumCur: TFloatField;
    taBarBarCode: TStringField;
    taBarCardArticul: TStringField;
    taBarCardSize: TStringField;
    taBarQuantity: TFloatField;
    taList: TTable;
    taListTName: TStringField;
    taListDataType: TSmallintField;
    taListOperation: TSmallintField;
    taClass: TTable;
    taClassGroop1: TSmallintField;
    taClassGroop2: TSmallintField;
    taClassGroop3: TSmallintField;
    taClassGroop4: TSmallintField;
    taClassGroop5: TSmallintField;
    taClassName: TStringField;
    taClassGr1: TIntegerField;
    taClassGr2: TIntegerField;
    taClassGr3: TIntegerField;
    taClassGr4: TIntegerField;
    taClassGr5: TIntegerField;
    taCardGr1: TIntegerField;
    taCardGr2: TIntegerField;
    taCardGr3: TIntegerField;
    taCardGr4: TIntegerField;
    taCardGr5: TIntegerField;
    tbTaxT: TTable;
    tbTaxTID: TSmallintField;
    tbTaxTPriority: TSmallintField;
    tbTaxTName: TStringField;
    tbCashers: TTable;
    tbCashersIdent: TSmallintField;
    tbCashersName: TStringField;
    tbCashersPassw: TStringField;
    tbCashersOfficialIndex: TSmallintField;
    tbDeparts: TTable;
    tbDepartsID: TSmallintField;
    tbDepartsName: TStringField;
    tbDiscSum: TTable;
    tbDiscSumPriceIndex: TSmallintField;
    tbDiscSumTime: TSmallintField;
    tbDiscSumSumma: TCurrencyField;
    tbDiscSumDiscount: TFloatField;
    tbDiscQ: TTable;
    tbDiscQCardArticul: TStringField;
    tbDiscQQuantity: TFloatField;
    tbDiscQDiscount: TCurrencyField;
    tbDiscCard: TTable;
    tbDiscCardBarCode: TStringField;
    tbDiscCardName: TStringField;
    tbDiscCardPercent: TFloatField;
    tbDiscCardClientIndex: TSmallintField;
    tbDiscStop: TTable;
    tbDiscStopCodeEnd: TStringField;
    tbDiscStopCodeStart: TStringField;
    tbAdvDisc: TTable;
    tbAdvDiscGroop1: TSmallintField;
    tbAdvDiscGroop2: TSmallintField;
    tbAdvDiscGroop3: TSmallintField;
    tbAdvDiscGroop4: TSmallintField;
    tbAdvDiscGroop5: TSmallintField;
    tbAdvDiscSumma: TCurrencyField;
    tbAdvDiscBarCode: TStringField;
    tbAdvDiscPercent: TFloatField;
    tbCredCard: TTable;
    tbCredCardID: TSmallintField;
    tbCredCardName: TStringField;
    tbCredCardClientIndex: TSmallintField;
    tbCredCardLimitSum: TCurrencyField;
    tbCredCardCanReturn: TSmallintField;
    tbCredPref: TTable;
    tbCredPrefPrefix: TStringField;
    tbCredPrefCredCardIndex: TSmallintField;
    Session1: TSession;
    Timer2: TTimer;
    taDStop: TTable;
    taDStopCardArticul: TStringField;
    taDStopPercent: TFloatField;
    procedure N3Click(Sender: TObject);
    procedure RxTrayClick(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure N1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure taClassCalcFields(DataSet: TDataSet);
    procedure Timer1Timer(Sender: TObject);
    procedure taCardCalcFields(DataSet: TDataSet);
    procedure Timer2Timer(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure prTransp;
  end;

procedure WrMess(StrOut:String);

var
  fmMainConvStd: TfmMainConvStd;

implementation

uses HistoryTr, Un1, Dm, u2fdk;

{$R *.dfm}

procedure TfmMainConvStd.prTransp;
Var StrWk, StrF, StrNum,sCashLoad:String;
    F:TextFile;
    iCount,iGr,iGrPar:INteger;
label ToNextFile;
begin
{ 1. ��������� *.non
  2. ��������� cash00n.db
  3. ������� cash00n.db
  4. ����������� ����� � ���������
}
{
�����������
  1. ��������� *.non - ������ ������� - ����� ���� �� �����
  2. ��������� cash00*.ldd - ������ ��������� - ������ ��������� ����� ��������

  3. ��������� cash00n.db
  4. ������� cash00n.db
  5. ����������� ����� � ���������

  6. ��������� cash00*.ldd - ������ ��������� - ������� ������ ��������
}

  StrWk:=IntToStr(CommonSet.CashNum);
  while length(StrWk)<3 do strwk:='0'+StrWk;
  StrNum:=StrWk;
  StrWk:='cash'+StrWk+'.db';
  sCashLoad:='cash'+StrNum+'.ldd';

  WrMess('--------');

  if Fileexists(CommonSet.PathImport+CashNon) then
  begin
    WrMess('������ ������ ����������� - ��������� ������.');
    Timer1.Enabled:=True;
    exit; //���� ���� �� ��������
  end;
  if Fileexists(CommonSet.PathImport+sCashLoad) then deletefile(CommonSet.PathImport+sCashLoad); //�������� ��� ��� ���� �� ��� ���� ������ � ���� �������
  if Fileexists(CommonSet.PathImport+StrWk) then
  begin //���� cashxxx.db ��������� - ������ ������������
    try
      WrMess('����� ������.');

      //������ cash001.ldd
      if not FileExists(CommonSet.PathImport+sCashLoad) then
      begin
        AssignFile(F,CommonSet.PathImport+sCashLoad);
        rewrite(F);
        CloseFile(F);
      end;
//      delay(1000);

// �������� ���� ��
      try
        dmC.CasherDb.Close;
        dmC.CasherDb.Open;
      except
        WrMess('  ������ �������� ���� CasherDb');
        exit;
      end;

      Session1.Active:=False;
      Session1.NetFileDir:=CommonSet.NetPath;
      Session1.Active:=True;

      StrF:=StrWk;
      taList.Active:=False;
      taList.DatabaseName:=CommonSet.PathImport;
      taList.TableName:=StrF;
      taList.Active:=True;

      WrMess('  ������� '+StrF);
      taList.First;
      while not taList.Eof do
      begin
        StrF:=taListTName.AsString+'.db';
        if not FileExists(CommonSet.PathImport+StrF) then goto ToNextFile;
        delay(1000);
        case taListDataType.AsInteger of
        1:begin
            WrMess(' �������� ������.');
            iCount:=0;
            try
              taCard.Active:=False;
              taCard.DatabaseName:=CommonSet.PathImport;
              taCard.TableName:=StrF;
              taCard.Active:=True;
              WrMess(' ���� �������, ���� ������ � ����������.');
            except
              WrMess(' ���� "'+CommonSet.PathImport+StrWk+'" ������ ��� ��������.');
              taCard.Active:=False;
              break;
            end;
            with dmC do
            begin
              try
//                taCardScla.Open;

                taCard.First;
                while not taCard.Eof do
                begin
                  if taCardDeleted.AsInteger>0 then
                  begin
                    WrMess('  ��������� '+taCardArticul.AsString);

                    iGr:=taCardGroop1.AsInteger;
                    if taCardGroop2.AsInteger>0 then iGr:=taCardGroop2.AsInteger+1000;
                    if taCardGroop3.AsInteger>0 then iGr:=taCardGroop3.AsInteger+2000;
                    if taCardGroop4.AsInteger>0 then iGr:=taCardGroop4.AsInteger+3000;
                    if taCardGroop5.AsInteger>0 then iGr:=taCardGroop5.AsInteger+4000;

                    prAddCard.ParamByName('ARTICUL').AsString:=taCardArticul.AsString;
                    prAddCard.ParamByName('CLASSIF').AsInteger:=iGr;
                    prAddCard.ParamByName('DEPART').AsInteger:=taCardClientIndex.AsInteger;
                    prAddCard.ParamByName('NAME').AsString:=OemToAnsiConvert(taCardName.AsString);

                    if taCardMesPresision.AsFloat=1 then prAddCard.ParamByName('CARD_TYPE').AsInteger:=1
                    else prAddCard.ParamByName('CARD_TYPE').AsInteger:=2;

                    prAddCard.ParamByName('MESURIMENT').AsString:=OemToAnsiConvert(taCardMesuriment.AsString);
                    prAddCard.ParamByName('PRICE_RUB').AsFloat:=taCardPriceRub.AsFloat;
                    prAddCard.ParamByName('DISCOUNT').AsFloat:=0;
                    prAddCard.ParamByName('DISCQUANT').AsFloat:=0;
                    prAddCard.ParamByName('SCALE').AsString:=taCardScale.AsString;

                    prAddCard.ExecProc;
                  end else
                  begin //��������
                    WrMess('  ������� '+taCardArticul.AsString);
                    prDelCard.ParamByName('ARTICUL').AsString:=taCardArticul.AsString;
                    prDelCard.ExecProc
                  end;

                  taCard.Next;
                  delay(33);
                  inc(iCount);
{                  if iCount>500 then
                  begin
                    trUpdate.Commit;
                    trUpdate.StartTransaction;
                  end;}
                end;
//                trUpdate.Commit;
                WrMess(' ��. ('+IntToStr(iCount)+' �������)');
              finally
//                taCardScla.Close;

                taCard.Active:=False;
              end;
            end;
          end;
        2:begin
            WrMess(' ��������� ������.');
            iCount:=0;
            try
              taBar.Active:=False;
              taBar.DatabaseName:=CommonSet.PathImport;
              taBar.TableName:=StrF;
              taBar.Active:=True;
            except
              taBar.Active:=False;
              WrMess(' ���� "'+CommonSet.PathImport+StrWk+'" ������ ��� ��������.');
              Break;
            end;
            with dmC do
            begin
              try
//                taBarCode.Open;
//                trUpdate.StartTransaction;
                while not taBar.Eof do
                begin

{                  if taBarCode.Locate('BARCODE',taBarBarCode.AsString,[]) then taBarcode.Edit
                  else taBarcode.Append;
                  taBarCodeBARCODE.AsString:=taBarBarCode.AsString;
                  taBarCodeCARDARTICUL.AsString:=taBarCardArticul.AsString;
                  taBarCodeCARDSIZE.AsString:=taBarCardSize.AsString;
                  taBarCodeQUANTITY.AsFloat:=taBarQuantity.AsFloat;

                  taBarCode.Post;
EXECUTE PROCEDURE PR_ADDBAR (?BARCODE, ?CARDARTICUL, ?CARDSIZE, ?QUANTITY, ?PRICERUB)

                  }
                  prAddBar.ParamByName('BARCODE').AsString:=taBarBarCode.AsString;
                  prAddBar.ParamByName('CARDARTICUL').AsString:=taBarCardArticul.AsString;
                  prAddBar.ParamByName('CARDSIZE').AsString:=taBarCardSize.AsString;
                  prAddBar.ParamByName('QUANTITY').AsFloat:=taBarQuantity.AsFloat;
                  prAddBar.ParamByName('PRICERUB').AsFloat:=0;

                  prAddBar.ExecProc;

                  taBar.Next;
                  delay(33);
                  inc(iCount);
{                  if iCount>500 then
                  begin
                    trUpdate.Commit;
                    trUpdate.StartTransaction;
                  end;}
                end;
//                trUpdate.Commit;
                WrMess(' ��. ('+IntToStr(iCount)+' �������)');
              finally
                taBar.Active:=False;
//                taBarCode.Close;
              end;
            end;
          end;
        3:begin
            WrMess(' ������ �������.');
            iCount:=0;
            try
              taClass.Active:=False;
              taClass.DatabaseName:=CommonSet.PathImport;
              taClass.TableName:=StrF;
              taClass.Active:=True;
            except
              taClass.Active:=False;
              WrMess(' ���� "'+CommonSet.PathImport+StrWk+'" ������ ��� ��������.');
              Break;
            end;
            with dmC do
            begin
              try
                taClassif.Open;
                trUpdate.StartTransaction;

                while not taClass.Eof do
                begin
                  iGrPar:=0;
                  iGr:=taClassGroop1.AsInteger;
                  if taClassGroop2.AsInteger>0 then begin iGr:=taClassGr2.AsInteger; iGrPar:=taClassGr1.AsInteger; end;
                  if taClassGroop3.AsInteger>0 then begin iGr:=taClassGr3.AsInteger; iGrPar:=taClassGr2.AsInteger; end;
                  if taClassGroop4.AsInteger>0 then begin iGr:=taClassGr4.AsInteger; iGrPar:=taClassGr3.AsInteger; end;
                  if taClassGroop5.AsInteger>0 then begin iGr:=taClassGr5.AsInteger; iGrPar:=taClassGr4.AsInteger; end;

                  if taClassif.Locate('ID',iGr,[]) then taClassif.Edit
                  else taClassif.Append;

                  taClassifID.AsInteger:=iGr;
                  taClassifID_PARENT.AsInteger:=iGrPar;
                  taClassifTYPE_CLASSIF.AsInteger:=1;
                  taClassifNAME.AsString:=OemToAnsiConvert(taClassName.AsString);
                  taClassifIACTIVE.AsInteger:=1;
                  taClassifIEDIT.AsInteger:=0;

                  taClassif.Post;

                  taClass.Next;
                  delay(10);
                  inc(iCount);
                  if iCount>500 then
                  begin
                    trUpdate.Commit;
                    trUpdate.StartTransaction;
                  end;
                end;
                trUpdate.Commit;
                WrMess(' ��. ('+IntToStr(iCount)+' �������)');
              finally
                taClass.Active:=False;
                taClassif.Close;
              end;
            end;
          end;
        4:begin
            WrMess(' ������ �� ����������.');
            iCount:=0;
            try
              tbDiscQ.Active:=False;
              tbDiscQ.DatabaseName:=CommonSet.PathImport;
              tbDiscQ.TableName:=StrF;
              tbDiscQ.Active:=True;
            except
              WrMess(' ���� "'+CommonSet.PathImport+StrWk+'" ������ ��� ��������.');
              tbDiscQ.Active:=False;
            end;

            tbDiscQ.First;

            with dmC do
            begin
              try
                taCardScla.Open;
                trUpdate.StartTransaction;
                while not tbDiscQ.Eof do
                begin
                  if taCardScla.Locate('ARTICUL',tbDiscQCardArticul.AsString,[]) then
                  begin
                    taCardScla.Edit;
                    taCardSclaDISCQUANT.AsFloat:=tbDiscQQuantity.AsFloat;
                    taCardSclaDISCOUNT.AsFloat:=tbDiscQDiscount.AsFloat;
                    taCardScla.Post;
                  end;

                  tbDiscQ.Next;
                  delay(10);
                  inc(iCount);
                  if iCount>500 then
                  begin
                    trUpdate.Commit;
                    trUpdate.StartTransaction;
                  end;
                end;
                trUpdate.Commit;
                WrMess(' ��. ('+IntToStr(iCount)+' �������)');
              finally
                taCardScla.Close;
                tbDiscQ.Active:=False;
              end;
            end;
          end;
        7:begin
            WrMess(' �������.');
            iCount:=0;
            try
              tbCashers.Active:=False;
              tbCashers.DatabaseName:=CommonSet.PathImport;
              tbCashers.TableName:=StrF;
              tbCashers.Active:=True;
            except
              tbCashers.Active:=False;
              WrMess(' ���� "'+CommonSet.PathImport+StrWk+'" ������ ��� ��������.');
              Break;
            end;
            tbCashers.First;

//            dmC.trUpdate.StartTransaction;
            with dmC do
            begin
              try
                taPersonal.Active:=False;
                taPersonal.Active:=true;

                while not tbCashers.Eof do
                begin
                  if taPersonal.Locate('ID',tbCashersIdent.AsInteger,[]) then taPersonal.Edit
                  else taPersonal.Append;

                  taPersonalID.AsInteger:=tbCashersIdent.AsInteger;
                  taPersonalID_PARENT.AsInteger:=1002;
                  taPersonalNAME.AsString:=OemToAnsiConvert(tbCashersName.AsString);
                  taPersonalUVOLNEN.AsString:='T';
                  taPersonalPASSW.AsString:=OemToAnsiConvert(tbCashersPassw.AsString);
                  taPersonalModul1.AsInteger:=1;
                  taPersonalModul2.AsInteger:=1;
                  taPersonalModul3.AsInteger:=0;
                  taPersonalModul4.AsInteger:=1;
                  taPersonalModul5.AsInteger:=1;
                  taPersonalModul6.AsInteger:=1;
                  taPersonalBARCODE.AsString:=OemToAnsiConvert(tbCashersPassw.AsString);
                  taPersonal.Post;

                  tbCashers.Next;
                  delay(10);
                  inc(iCount);
              {  if iCount>500 then
              begin
                trUpdate.Commit;
                trUpdate.StartTransaction;
              end;}
                end;
//              dmC.trUpdate.Commit;
              finally
                taPersonal.Active:=False;
              end;
            end;
            tbCashers.Active:=False;
            WrMess(' ��. ('+IntToStr(iCount)+' �������)');
          end;
        8:begin
            WrMess(' ����������� �������.');
            iCount:=0;
            try
              tbCredCard.Active:=False;
              tbCredCard.DatabaseName:=CommonSet.PathImport;
              tbCredCard.TableName:=StrF;
              tbCredCard.Active:=True;
            except
              tbCredCard.Active:=False;
              WrMess(' ���� "'+CommonSet.PathImport+StrWk+'" ������ ��� ��������.');
              Break;
            end;

            tbCredCard.First;

            with dmC do
            begin
              try
                taCredCard.Open;
                trUpdate.StartTransaction;
                while not tbCredCard.Eof do
                begin
                  if taCredCard.Locate('ID',tbCredCardId.AsInteger,[]) then taCredCard.Edit
                  else taCredCard.Append;

                  taCredCardID.AsInteger:=tbCredCardID.AsInteger;
                  taCredCardNAME.AsString:=OemToAnsiConvert(tbCredCardName.AsString);
                  taCredCardCLIENTINDEX.AsInteger:=tbCredCardClientIndex.AsInteger;
                  taCredCardLIMITSUM.AsFloat:=tbCredCardLimitSum.AsFloat;
                  taCredCardCANRETURN.AsInteger:=tbCredCardCanReturn.AsInteger;

                  taCredCard.Post;

                  tbCredCard.Next;
                  delay(10);
                  inc(iCount);
                  if iCount>500 then
                  begin
                    trUpdate.Commit;
                    trUpdate.StartTransaction;
                  end;
                end;
                trUpdate.Commit;
                WrMess(' ��. ('+IntToStr(iCount)+' �������)');
              finally
                taCredCard.Close;
                tbCredCard.Active:=False;
              end;
            end;
          end;
        9:begin
            WrMess(' �������� ����������� ��������.');
            iCount:=0;
            try
              tbCredPref.Active:=False;
              tbCredPref.DatabaseName:=CommonSet.PathImport;
              tbCredPref.TableName:=StrF;
              tbCredPref.Active:=True;
            except
              tbCredPref.Active:=False;
              WrMess(' ���� "'+CommonSet.PathImport+StrWk+'" ������ ��� ��������.');
              Break;
            end;

            tbCredPref.First;

            with dmC do
            begin
              try
                taCredPref.Open;
                trUpdate.StartTransaction;
                while not tbCredPref.Eof do
                begin
                  if taCredPref.Locate('PREFIX',tbCredPrefPrefix.AsString,[]) then taCredPref.Edit
                  else taCredPref.Append;

                  taCredPrefPREFIX.AsString:=tbCredPrefPrefix.AsString;
                  taCredPrefID_CARD.AsInteger:=tbCredPrefCredCardIndex.AsInteger;
                  taCredPref.Post;

                  tbCredPref.Next;
                  delay(10);
                  inc(iCount);
                  if iCount>500 then
                  begin
                    trUpdate.Commit;
                    trUpdate.StartTransaction;
                  end;
                end;
                trUpdate.Commit;
                WrMess(' ��. ('+IntToStr(iCount)+' �������)');
              finally
                taCredPref.Close;
                tbCredPref.Active:=False;
              end;
            end;
          end;
       10:begin
            WrMess(' ������.');
            iCount:=0;
            try
              tbDeparts.Active:=False;
              tbDeparts.DatabaseName:=CommonSet.PathImport;
              tbDeparts.TableName:=StrF;
              tbDeparts.Active:=True;
            except
              tbDeparts.Active:=False;
              WrMess(' ���� "'+CommonSet.PathImport+StrWk+'" ������ ��� ��������.');
              Break;
            end;
            tbDeparts.First;

            with dmC do
            begin
              try
                taDeparts.Open;
                trUpdate.StartTransaction;

                while not tbDeparts.Eof do
                begin
                  if taDeparts.Locate('ID',tbDepartsID.AsInteger,[]) then taDeparts.Edit
                  else taDeparts.Append;

                  taDepartsID.AsInteger:=tbDepartsID.AsInteger;
                  taDepartsNAME.AsString:=OemToAnsiConvert(tbDepartsNAME.AsString);

                  taDeparts.Post;

                  tbDeparts.Next;
                  delay(10);
                  inc(iCount);
                  if iCount>500 then
                  begin
                    trUpdate.Commit;
                    trUpdate.StartTransaction;
                  end;
                end;
                trUpdate.Commit;
                WrMess(' ��. ('+IntToStr(iCount)+' �������)');
              finally
                taDeparts.Close;
                tbDeparts.Active:=False;
              end;
            end;
          end;
       11:begin
            WrMess(' ���������� �����.');
            iCount:=0;
            try
              tbDiscCard.Active:=False;
              tbDiscCard.DatabaseName:=CommonSet.PathImport;
              tbDiscCard.TableName:=StrF;
              tbDiscCard.Active:=True;
            except
              tbDiscCard.Active:=False;
              WrMess(' ���� "'+CommonSet.PathImport+StrWk+'" ������ ��� ��������.');
              Break;
            end;

            tbDiscCard.First;

            with dmC do
            begin
              try
                taDiscCard.Open;
                trUpdate.StartTransaction;

                while not tbDiscCard.Eof do
                begin
                  if taDiscCard.Locate('BARCODE',tbDiscCardBARCODE.AsString,[]) then taDiscCard.Edit
                  else taDiscCard.Append;

                  taDiscCardBARCODE.AsString:=tbDiscCardBARCODE.AsString;
                  taDiscCardNAME.AsString:=OemToAnsiConvert(tbDiscCardNAME.AsString);
                  taDiscCardPERCENT.AsFloat:=tbDiscCardPERCENT.AsFloat;
                  taDiscCardCLIENTINDEX.AsInteger:=tbDiscCardCLIENTINDEX.AsInteger;

                  taDiscCard.Post;

                  tbDiscCard.Next;
                  delay(10);
                  inc(iCount);
                  if iCount>500 then
                  begin
                    trUpdate.Commit;
                    trUpdate.StartTransaction;
                  end;
                end;
                trUpdate.Commit;
                WrMess(' ��. ('+IntToStr(iCount)+' �������)');
              finally
                taDiscCard.Close;
                tbDiscCard.Active:=False;
              end;
            end;
          end;
       13:begin
            WrMess(' ������ �� ����� ����.');
            iCount:=0;
            try
              tbDiscSum.Active:=False;
              tbDiscSum.DatabaseName:=CommonSet.PathImport;
              tbDiscSum.TableName:=StrF;
              tbDiscSum.Active:=True;
            except
              tbDiscSum.Active:=False;
              WrMess(' ���� "'+CommonSet.PathImport+StrWk+'" ������ ��� ��������.');
              Break;
            end;
            tbDiscSum.First;

            with dmC do
            begin
              try
                taDiscSum.Open;
                trUpdate.StartTransaction;
                taDiscSum.First;
                while not taDiscSum.Eof do
                begin
                  if taDiscSumBARCODE.AsString='' then taDiscSum.Delete
                  else taDiscSum.Next;
                end;
                trUpdate.Commit;
              except
                tbDiscSum.Active:=False;
                Break;
              end;
            end;

            with dmC do
            begin
              try
                trUpdate.StartTransaction;

                while not tbDiscSum.Eof do
                begin
                  if taDiscSum.Locate('FROMTIME',tbDiscSumTime.AsInteger,[]) then taDiscSum.Edit
                  else taDiscSum.Append;
                  taDiscSumFROMTIME.AsInteger:=tbDiscSumTime.AsInteger;
                  taDiscSumSUMMA.AsFloat:=tbDiscSumSumma.AsCurrency;
                  taDiscSumDISCOUNT.AsFloat:=tbDiscSumDiscount.AsFloat;

                  taDiscSum.Post;

                  tbDiscSum.Next;
                  delay(10);
                  inc(iCount);
                  if iCount>500 then
                  begin
                    trUpdate.Commit;
                    trUpdate.StartTransaction;
                  end;
                end;
                trUpdate.Commit;
                WrMess(' ��. ('+IntToStr(iCount)+' �������)');
              finally
                taDiscSum.Close;
                tbDiscSum.Active:=False;
              end;
            end;
          end;
       14:begin
            WrMess(' �������� ���������� ����.');
            iCount:=0;
            try
              tbDiscStop.Active:=False;
              tbDiscStop.DatabaseName:=CommonSet.PathImport;
              tbDiscStop.TableName:=StrF;
              tbDiscStop.Active:=True;
            except
              tbDiscStop.Active:=False;
              WrMess(' ���� "'+CommonSet.PathImport+StrWk+'" ������ ��� ��������.');
              Break;
            end;

            tbDiscStop.First;

            with dmC do
            while not tbDiscStop.Eof do
            begin

              prSetDiscCardStop.ParamByName('BSTART').Value:=tbDiscStopCodeStart.AsString;
              prSetDiscCardStop.ParamByName('BEND').Value:=tbDiscStopCodeEnd.AsString;
              prSetDiscCardStop.ExecProc;

              tbDiscStop.Next;
              delay(10);
              inc(iCount);
            end;
            tbDiscStop.Active:=False;
            WrMess(' ��. ('+IntToStr(iCount)+' �������)');
          end;
       18:begin
            with dmC do
            begin
              WrMess(' �������� ������ �� �����.');

              taFixDisc.Active:=False;
              taFixDisc.Active:=True;

              iCount:=0;
              try
                taDStop.Active:=False;
                taDStop.DatabaseName:=CommonSet.PathImport;
                taDStop.TableName:=StrF;
                taDStop.Active:=True;
              except
                taDStop.Active:=False;
                WrMess(' ���� "'+CommonSet.PathImport+StrWk+'" ������ ��� ��������.');
                Break;
              end;

              taDStop.First;

              while not taDStop.Eof do
              begin
                if taFixDisc.Locate('SART',taDStopCardArticul.AsString,[]) then
                begin
                  taFixDisc.Edit;
                  taFixDiscPROC.AsFloat:=taDStopPercent.AsFloat;
                  taFixDisc.Post;
                end else
                begin
                  taFixDisc.Append;
                  taFixDiscSART.AsString:=taDStopCardArticul.AsString;
                  taFixDiscPROC.AsFloat:=taDStopPercent.AsFloat;
                  taFixDisc.Post;
                end;

                taDStop.Next;
                delay(10);
                inc(iCount);
              end;
              taDStop.Active:=False;
              taFixDisc.Active:=False;

              WrMess(' ��. ('+IntToStr(iCount)+' �������)');
            end;
          end;
       23:begin
            WrMess(' ���� �������.');
            iCount:=0;
            try
              tbTaxT.Active:=False;
              tbTaxT.DatabaseName:=CommonSet.PathImport;
              tbTaxT.TableName:=StrF;
              tbTaxT.Active:=True;
            except
              tbTaxT.Active:=False;
              WrMess(' ���� "'+CommonSet.PathImport+StrWk+'" ������ ��� ��������.');
              Break;
            end;
            with dmC do
            begin
              try
                taTaxType.Open;
                trUpdate.StartTransaction;

                while not tbTaxT.Eof do
                begin
                  if taTaxType.Locate('ARTICUL',tbTaxTId.AsInteger,[]) then taTaxType.Edit
                  else taTaxType.Append;
                  taTaxTypeID.AsInteger:=tbTaxTID.AsInteger;
                  taTaxTypePRIOR.AsInteger:=tbTaxTPriority.AsInteger;
                  taTaxTypeNAME.AsString:=OemToAnsiConvert(tbTaxTName.AsString);
                  taTaxType.Post;

                  tbTaxT.Next;
                  inc(iCount);
                  delay(10);
                  if iCount>500 then
                  begin
                    trUpdate.Commit;
                    trUpdate.StartTransaction;
                  end;
                end;
                trUpdate.Commit;
                WrMess(' ��. ('+IntToStr(iCount)+' �������)');
              finally
                taTaxType.Close;
                tbTaxT.Active:=False;
              end;
            end;
          end;
       25:begin //���� �� ����� ��������� ������
          {  WrMess(' ��������� ������.');
            iCount:=0;
            try
              tbTax.Active:=False;
              tbTax.DatabaseName:=CommonSet.PathImport;
              tbTax.TableName:=StrF;
              tbTax.Active:=True;
            except
              tbTax.Active:=False;
              Break;
            end;
            with dmC do
            begin
              try
                taTax.Open;
                trUpdate.StartTransaction;

                while not tbTax.Eof do
                begin
                  if taTax.Locate('ARTICUL',tbTaxCardArticul.AsString,[]) then taTax.Edit
                  else taTax.Append;
                  taTaxARTICUL.AsString:= tbTaxCardArticul.AsString;
                  taTaxTAXINDEX.AsInteger:=tbTaxTaxIndex.AsInteger;
                  taTaxTAX.AsFloat:= tbTaxTax.AsFloat;
                  taTax.Post;

                  tbTax.Next;
                  inc(iCount);
                  delay(10);
                  if iCount>500 then
                  begin
                    trUpdate.Commit;
                    trUpdate.StartTransaction;
                  end;
                end;
                trUpdate.Commit;
                WrMess(' ��. ('+IntToStr(iCount)+' �������)');
              finally
                taTax.Close;
                tbTax.Active:=False;
              end;
            end;}
          end;
       26:begin
            WrMess(' ����������� ������.');
            iCount:=0;
            try
              tbAdvDisc.Active:=False;
              tbAdvDisc.DatabaseName:=CommonSet.PathImport;
              tbAdvDisc.TableName:=StrF;
              tbAdvDisc.Active:=True;
            except
              tbAdvDisc.Active:=False;
              WrMess(' ���� "'+CommonSet.PathImport+StrWk+'" ������ ��� ��������.');
              Break;
            end;
            tbAdvDisc.First;

            with dmC do
            begin
              try
                taDiscSum.Open;
                taDiscSum.First;
                trUpdate.StartTransaction;
                while not taDiscSum.Eof do
                begin
                  if taDiscSumBARCODE.AsString<>'' then taDiscSum.Delete
                  else taDiscSum.Next;
                end;
                trUpdate.Commit;
              except
                taDiscSum.Close;
                tbAdvDisc.Active:=False;
                Break;
              end;
            end;

            with dmC do
            begin
              try
                trUpdate.StartTransaction;
                while not tbAdvDisc.Eof do
                begin
                  if tbAdvDiscBarCode.AsString>'' then
                  begin
                    taDiscSum.Append;
                    taDiscSumFROMTIME.AsInteger:=0;
                    taDiscSumBARCODE.AsString:=OemToAnsiConvert(tbAdvDiscBarCode.AsString);
                    taDiscSumSUMMA.AsFloat:=tbAdvDiscSumma.AsCurrency;
                    taDiscSumDISCOUNT.AsFloat:=tbAdvDiscPercent.AsFloat;

                    taDiscSum.Post;
                  end;

                  tbAdvDisc.Next;
                  delay(10);
                  inc(iCount);
                  if iCount>500 then
                  begin
                    trUpdate.Commit;
                    trUpdate.StartTransaction;
                  end;
                end;
                trUpdate.Commit;
                WrMess(' ��. ('+IntToStr(iCount)+' �������)');
              finally
                taDiscSum.Close;
                tbAdvDisc.Active:=False;
              end;
            end;
          end;
        else
        end;
ToNextFile:
        if FileExists(CommonSet.PathImport+StrF) then DeleteFile(CommonSet.PathImport+StrF);
        if FileExists(CommonSet.PathImport+taListTName.AsString+'.px') then DeleteFile(CommonSet.PathImport+taListTName.AsString+'.px');

        if FileExists(CommonSet.PathImport+taListTName.AsString+'.xg0') then DeleteFile(CommonSet.PathImport+taListTName.AsString+'.xg0');
        if FileExists(CommonSet.PathImport+taListTName.AsString+'.x02') then DeleteFile(CommonSet.PathImport+taListTName.AsString+'.x02');
        if FileExists(CommonSet.PathImport+taListTName.AsString+'.x04') then DeleteFile(CommonSet.PathImport+taListTName.AsString+'.x04');
        if FileExists(CommonSet.PathImport+taListTName.AsString+'.xg1') then DeleteFile(CommonSet.PathImport+taListTName.AsString+'.xg1');
        if FileExists(CommonSet.PathImport+taListTName.AsString+'.xg2') then DeleteFile(CommonSet.PathImport+taListTName.AsString+'.xg2');
        if FileExists(CommonSet.PathImport+taListTName.AsString+'.xg3') then DeleteFile(CommonSet.PathImport+taListTName.AsString+'.xg3');
        if FileExists(CommonSet.PathImport+taListTName.AsString+'.xg4') then DeleteFile(CommonSet.PathImport+taListTName.AsString+'.xg4');
        if FileExists(CommonSet.PathImport+taListTName.AsString+'.yg0') then DeleteFile(CommonSet.PathImport+taListTName.AsString+'.yg0');
        if FileExists(CommonSet.PathImport+taListTName.AsString+'.y02') then DeleteFile(CommonSet.PathImport+taListTName.AsString+'.y02');
        if FileExists(CommonSet.PathImport+taListTName.AsString+'.y04') then DeleteFile(CommonSet.PathImport+taListTName.AsString+'.y04');
        if FileExists(CommonSet.PathImport+taListTName.AsString+'.yg1') then DeleteFile(CommonSet.PathImport+taListTName.AsString+'.yg1');
        if FileExists(CommonSet.PathImport+taListTName.AsString+'.yg2') then DeleteFile(CommonSet.PathImport+taListTName.AsString+'.yg2');
        if FileExists(CommonSet.PathImport+taListTName.AsString+'.yg3') then DeleteFile(CommonSet.PathImport+taListTName.AsString+'.yg3');
        if FileExists(CommonSet.PathImport+taListTName.AsString+'.yg4') then DeleteFile(CommonSet.PathImport+taListTName.AsString+'.yg4');

        taList.Next;
      end;
      taList.Active:=False;
      delay(100);

      Session1.Active:=False;

      try
        dmC.CasherDb.Close;
      except
        WrMess('  ������ �������� ���� CasherDb');
      end;

      StrWk:='cash'+StrNum+'.db';
      WrMess('   ������ - '+CommonSet.PathImport+StrWk);
      if FileExists(CommonSet.PathImport+StrWk) then DeleteFile(CommonSet.PathImport+StrWk);
      WrMess('   ������ - '+CommonSet.PathImport+sCashLoad);
      if FileExists(CommonSet.PathImport+sCashLoad) then DeleteFile(CommonSet.PathImport+sCashLoad);

      WrMess('������ �������.');
      WrMess('');
    except
      taList.Active:=False;
      Session1.Active:=False;
      WrMess('������ �������������� ����� - '+StrF);
    end;
  end;
  Timer1.Enabled:=True;
end;

procedure WrMess(StrOut:String);
Var StrWk:String;
begin
  with fmHistoryTr do
  begin
    if Memo1.Lines.Count > 500 then Memo1.Clear;
    if StrOut>'' then StrWk:=FormatDateTime('dd.mm hh:nn:ss',now)+'  '+StrOut
    else StrWk:=StrOut;
    Memo1.Lines.Add(StrWk);
    WriteHistoryConv(StrWk);
  end;
end;

procedure TfmMainConvStd.N3Click(Sender: TObject);
begin
  close;
end;

procedure TfmMainConvStd.RxTrayClick(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
//  PopUpMenu1.Popup(Screen.DesktopRect.Right-100,Screen.DesktopRect.Bottom-80);
  fmHistoryTr.Show;
end;

procedure TfmMainConvStd.N1Click(Sender: TObject);
begin
  fmHistoryTr.Show;
end;

procedure TfmMainConvStd.FormCreate(Sender: TObject);
begin
  CurDir := ExtractFilePath(ParamStr(0));
  ReadIni;
//  TestDir;
end;

procedure TfmMainConvStd.taClassCalcFields(DataSet: TDataSet);
begin
  taClassGr1.AsInteger:=taClassGroop1.AsInteger;
  taClassGr2.AsInteger:=taClassGroop2.AsInteger+1000;
  taClassGr3.AsInteger:=taClassGroop3.AsInteger+2000;
  taClassGr4.AsInteger:=taClassGroop4.AsInteger+3000;
  taClassGr5.AsInteger:=taClassGroop5.AsInteger+4000;
end;

procedure TfmMainConvStd.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled:=False;
  prTransp;
//  Timer1.Enabled:=True;
end;

procedure TfmMainConvStd.taCardCalcFields(DataSet: TDataSet);
begin
{  taCardGr1.AsInteger:=taCardGroop1.AsInteger;
  taCardGr2.AsInteger:=taCardGroop2.AsInteger+1000;
  taCardGr3.AsInteger:=taCardGroop3.AsInteger+2000;
  taCardGr4.AsInteger:=taCardGroop4.AsInteger+3000;
  taCardGr5.AsInteger:=taCardGroop5.AsInteger+4000;}
end;

procedure TfmMainConvStd.Timer2Timer(Sender: TObject);
begin
  WrMess('������ �����. ������� '+IntToStr(CommonSet.PeriodPing*1000)+'��');
  Timer2.Enabled:=False;
  Timer1.Enabled:=True;
end;

end.
