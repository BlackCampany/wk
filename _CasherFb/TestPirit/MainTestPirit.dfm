object fmMainTestPirit: TfmMainTestPirit
  Left = 693
  Top = 177
  Width = 831
  Height = 629
  Caption = #1058#1077#1089#1090' '#1055#1048#1056#1048#1058#1040
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 815
    Height = 304
    Align = alTop
    BevelInner = bvLowered
    TabOrder = 0
    object Label1: TLabel
      Left = 11
      Top = 16
      Width = 55
      Height = 13
      Caption = #1055#1086#1088#1090'  COM'
    end
    object cxSpinEdit1: TcxSpinEdit
      Left = 72
      Top = 13
      Properties.MaxValue = 25.000000000000000000
      Properties.MinValue = 1.000000000000000000
      TabOrder = 0
      Value = 11
      Width = 62
    end
    object cxButton1: TcxButton
      Left = 148
      Top = 11
      Width = 118
      Height = 25
      Caption = #1055#1088#1086#1074#1077#1088#1082#1072' '#1087#1086#1088#1090#1072
      TabOrder = 1
      OnClick = cxButton1Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 9
      Top = 44
      Width = 255
      Height = 25
      Caption = #1055#1088#1086#1074#1077#1088#1082#1072' '#1089#1074#1103#1079#1080' '#1089' '#1050#1050#1058
      TabOrder = 2
      OnClick = cxButton2Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton3: TcxButton
      Left = 9
      Top = 77
      Width = 255
      Height = 25
      Caption = #1055#1088#1086#1084#1086#1090#1072#1090#1100' '#1073#1091#1084#1072#1075#1091
      TabOrder = 3
      OnClick = cxButton3Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton5: TcxButton
      Left = 9
      Top = 171
      Width = 252
      Height = 25
      Caption = #1057#1090#1072#1090#1091#1089'  0x00'
      TabOrder = 4
      OnClick = cxButton5Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton6: TcxButton
      Left = 9
      Top = 110
      Width = 252
      Height = 25
      Caption = #1053#1072#1095#1072#1083#1086' '#1088#1072#1073#1086#1090#1099
      TabOrder = 5
      OnClick = cxButton6Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton7: TcxButton
      Left = 9
      Top = 139
      Width = 252
      Height = 25
      Caption = 'X-'#1086#1090#1095#1077#1090' ('#1085#1086#1074#1099#1081')'
      TabOrder = 6
      OnClick = cxButton7Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton8: TcxButton
      Left = 9
      Top = 200
      Width = 252
      Height = 25
      Caption = #1057#1090#1072#1090#1091#1089' '#1087#1088#1080#1085#1090#1077#1088#1072
      TabOrder = 7
      OnClick = cxButton8Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton9: TcxButton
      Left = 9
      Top = 231
      Width = 252
      Height = 25
      Caption = #1047#1072#1087#1088#1086#1089' '#1089#1084#1077#1085#1085#1099#1093' '#1089#1095#1077#1090#1095#1080#1082#1086#1074' '#1080' '#1088#1077#1075#1080#1089#1090#1088#1086#1074
      TabOrder = 8
      OnClick = cxButton9Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton10: TcxButton
      Left = 284
      Top = 12
      Width = 112
      Height = 25
      Caption = #1054#1090#1082#1088#1099#1090#1100' '#1089#1084#1077#1085#1091
      TabOrder = 9
      OnClick = cxButton10Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton11: TcxButton
      Left = 272
      Top = 265
      Width = 252
      Height = 25
      Caption = #1053#1072#1087#1077#1095#1072#1090#1072#1090#1100' '#1085#1077#1092#1080#1089#1082#1072#1083#1100#1085#1099#1081' '#1076#1086#1082#1091#1084#1077#1085#1090
      TabOrder = 10
      OnClick = cxButton11Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton12: TcxButton
      Left = 326
      Top = 97
      Width = 104
      Height = 25
      Caption = #1054#1090#1082#1088#1099#1090#1100' '#1095#1077#1082
      TabOrder = 11
      OnClick = cxButton12Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton13: TcxButton
      Left = 325
      Top = 126
      Width = 104
      Height = 25
      Caption = #1054#1090#1073#1080#1090#1100' 1 '#1082#1086#1087
      TabOrder = 12
      OnClick = cxButton13Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton14: TcxButton
      Left = 326
      Top = 226
      Width = 104
      Height = 25
      Caption = #1047#1072#1082#1088#1099#1090#1100' '#1095#1077#1082
      TabOrder = 13
      OnClick = cxButton14Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton15: TcxButton
      Left = 571
      Top = 97
      Width = 104
      Height = 25
      Caption = #1040#1085#1085#1091#1083#1080#1088#1086#1074#1072#1090#1100' '#1095#1077#1082
      TabOrder = 14
      OnClick = cxButton15Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton16: TcxButton
      Left = 326
      Top = 157
      Width = 104
      Height = 25
      Caption = #1055#1086#1076#1080#1090#1086#1075
      TabOrder = 15
      OnClick = cxButton16Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton17: TcxButton
      Left = 326
      Top = 187
      Width = 104
      Height = 25
      Caption = #1054#1087#1083#1072#1090#1072
      TabOrder = 16
      OnClick = cxButton17Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton18: TcxButton
      Left = 9
      Top = 263
      Width = 252
      Height = 25
      Caption = 'Z-'#1086#1090#1095#1077#1090
      TabOrder = 17
      OnClick = cxButton18Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton19: TcxButton
      Left = 571
      Top = 127
      Width = 104
      Height = 25
      Caption = #1057#1086#1089#1090'. '#1095#1077#1082#1072
      TabOrder = 18
      OnClick = cxButton19Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton20: TcxButton
      Left = 571
      Top = 157
      Width = 104
      Height = 25
      Caption = #1057#1086#1089#1090'. '#1095#1077#1082#1072' 1'
      TabOrder = 19
      OnClick = cxButton20Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton21: TcxButton
      Left = 688
      Top = 69
      Width = 104
      Height = 25
      Caption = #1044#1072#1090#1072' '#1074#1088#1077#1084#1103' '#1050#1050#1058
      TabOrder = 20
      OnClick = cxButton21Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton22: TcxButton
      Left = 688
      Top = 98
      Width = 104
      Height = 25
      Caption = #1057#1084#1077#1085#1072' '#1086#1090#1088#1099#1090#1072'?'
      TabOrder = 21
      OnClick = cxButton22Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton23: TcxButton
      Left = 688
      Top = 129
      Width = 104
      Height = 25
      Caption = #1057#1084#1077#1085#1072' > 24 '#1095#1072#1089#1086#1074'?'
      TabOrder = 22
      OnClick = cxButton23Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton24: TcxButton
      Left = 441
      Top = 97
      Width = 104
      Height = 25
      Caption = #1054#1090#1082#1088#1099#1090#1100' '#1074#1086#1079#1074#1088#1072#1090
      TabOrder = 23
      OnClick = cxButton24Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton25: TcxButton
      Left = 571
      Top = 187
      Width = 104
      Height = 25
      Caption = #1063#1077#1082' '#1086#1090#1082#1088#1099#1090'?'
      TabOrder = 24
      OnClick = cxButton25Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton26: TcxButton
      Left = 571
      Top = 217
      Width = 104
      Height = 25
      Caption = #8470' '#1090#1077#1082' '#1095#1077#1082#1072
      TabOrder = 25
      OnClick = cxButton26Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton27: TcxButton
      Left = 688
      Top = 159
      Width = 104
      Height = 25
      Caption = #1057#1091#1084#1084#1072' '#1089#1082'.'#1085#1072#1094'.'
      TabOrder = 26
      OnClick = cxButton27Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton28: TcxButton
      Left = 688
      Top = 189
      Width = 107
      Height = 25
      Caption = #1055#1077#1095#1072#1090#1100' QR'
      TabOrder = 27
      OnClick = cxButton28Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton29: TcxButton
      Left = 689
      Top = 220
      Width = 107
      Height = 25
      Caption = #1054#1090#1082#1088#1099#1090#1100' '#1044#1071
      TabOrder = 28
      OnClick = cxButton29Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton30: TcxButton
      Left = 571
      Top = 248
      Width = 104
      Height = 25
      Caption = #1057#1091#1084#1084#1072' '#1074' '#1044#1071
      TabOrder = 29
      OnClick = cxButton30Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton31: TcxButton
      Left = 689
      Top = 9
      Width = 104
      Height = 25
      Caption = #1057#1091#1084#1084#1072' '#1087#1088#1086#1076#1072#1078
      TabOrder = 30
      OnClick = cxButton31Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton32: TcxButton
      Left = 442
      Top = 12
      Width = 104
      Height = 25
      Caption = #1042#1085#1077#1089#1077#1085#1080#1077' '
      TabOrder = 31
      OnClick = cxButton32Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton33: TcxButton
      Left = 442
      Top = 41
      Width = 104
      Height = 25
      Caption = #1048#1079#1098#1103#1090#1080#1077
      TabOrder = 32
      OnClick = cxButton33Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton4: TcxButton
      Left = 689
      Top = 39
      Width = 103
      Height = 25
      Caption = 'Nums'
      TabOrder = 33
      OnClick = cxButton4Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton34: TcxButton
      Left = 689
      Top = 252
      Width = 107
      Height = 25
      Caption = 'InspectSt'
      TabOrder = 34
      OnClick = cxButton34Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton35: TcxButton
      Left = 442
      Top = 146
      Width = 104
      Height = 25
      Caption = #1054#1090#1073#1080#1090#1100' 1.145 '#1082#1086#1083'-'#1074#1086
      TabOrder = 35
      OnClick = cxButton35Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton36: TcxButton
      Left = 572
      Top = 16
      Width = 103
      Height = 25
      Caption = #1060#1053
      TabOrder = 36
      OnClick = cxButton36Click
      LookAndFeel.Kind = lfOffice11
    end
  end
  object Memo1: TcxMemo
    Left = 0
    Top = 304
    Align = alClient
    Lines.Strings = (
      'Memo1')
    PopupMenu = PopupMenu1
    TabOrder = 1
    Height = 287
    Width = 815
  end
  object DevPrint: TVaComm
    Baudrate = br57600
    FlowControl.OutCtsFlow = False
    FlowControl.OutDsrFlow = False
    FlowControl.ControlDtr = dtrDisabled
    FlowControl.ControlRts = rtsDisabled
    FlowControl.XonXoffOut = False
    FlowControl.XonXoffIn = False
    FlowControl.DsrSensitivity = False
    FlowControl.TxContinueOnXoff = False
    DeviceName = '\\.\COM%d'
    Buffers.ReadSize = 8000
    Buffers.ReadTimeout = 2000
    OnRxChar = DevPrintRxChar
    Left = 257
    Top = 317
  end
  object PopupMenu1: TPopupMenu
    Left = 340
    Top = 317
    object N1: TMenuItem
      Caption = #1054#1095#1080#1089#1090#1080#1090#1100
      OnClick = N1Click
    end
  end
end
