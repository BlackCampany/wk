unit MainTestPirit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, cxMemo, StdCtrls, cxButtons,
  cxControls, cxContainer, cxEdit, cxTextEdit, cxMaskEdit, cxSpinEdit,
  ExtCtrls, VaClasses, VaComm;

type
  TfmMainTestPirit = class(TForm)
    Panel1: TPanel;
    cxSpinEdit1: TcxSpinEdit;
    Label1: TLabel;
    cxButton1: TcxButton;
    Memo1: TcxMemo;
    DevPrint: TVaComm;
    cxButton2: TcxButton;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    cxButton3: TcxButton;
    cxButton5: TcxButton;
    cxButton6: TcxButton;
    cxButton7: TcxButton;
    cxButton8: TcxButton;
    cxButton9: TcxButton;
    cxButton10: TcxButton;
    cxButton11: TcxButton;
    cxButton12: TcxButton;
    cxButton13: TcxButton;
    cxButton14: TcxButton;
    cxButton15: TcxButton;
    cxButton16: TcxButton;
    cxButton17: TcxButton;
    cxButton18: TcxButton;
    cxButton19: TcxButton;
    cxButton20: TcxButton;
    cxButton21: TcxButton;
    cxButton22: TcxButton;
    cxButton23: TcxButton;
    cxButton24: TcxButton;
    cxButton25: TcxButton;
    cxButton26: TcxButton;
    cxButton27: TcxButton;
    cxButton28: TcxButton;
    cxButton29: TcxButton;
    cxButton30: TcxButton;
    cxButton31: TcxButton;
    cxButton32: TcxButton;
    cxButton33: TcxButton;
    cxButton4: TcxButton;
    cxButton34: TcxButton;
    cxButton35: TcxButton;
    cxButton36: TcxButton;
    procedure FormShow(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure DevPrintRxChar(Sender: TObject; Count: Integer);
    procedure N1Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
    procedure cxButton7Click(Sender: TObject);
    procedure cxButton8Click(Sender: TObject);
    procedure cxButton9Click(Sender: TObject);
    procedure cxButton10Click(Sender: TObject);
    procedure cxButton11Click(Sender: TObject);
    procedure cxButton12Click(Sender: TObject);
    procedure cxButton14Click(Sender: TObject);
    procedure cxButton15Click(Sender: TObject);
    procedure cxButton13Click(Sender: TObject);
    procedure cxButton16Click(Sender: TObject);
    procedure cxButton17Click(Sender: TObject);
    procedure cxButton18Click(Sender: TObject);
    procedure cxButton19Click(Sender: TObject);
    procedure cxButton20Click(Sender: TObject);
    procedure cxButton21Click(Sender: TObject);
    procedure cxButton22Click(Sender: TObject);
    procedure cxButton23Click(Sender: TObject);
    procedure cxButton24Click(Sender: TObject);
    procedure cxButton25Click(Sender: TObject);
    procedure cxButton26Click(Sender: TObject);
    procedure cxButton27Click(Sender: TObject);
    procedure cxButton28Click(Sender: TObject);
    procedure cxButton29Click(Sender: TObject);
    procedure cxButton30Click(Sender: TObject);
    procedure cxButton31Click(Sender: TObject);
    procedure cxButton32Click(Sender: TObject);
    procedure cxButton33Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure cxButton34Click(Sender: TObject);
    procedure cxButton35Click(Sender: TObject);
    procedure cxButton36Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

procedure Delay(MSecs: Longint);
function Its(iV:Integer):String;
procedure prWM1(StrWk:string);
//procedure prClearBuf;
Procedure TestFp(StrOp:String);


var
  fmMainTestPirit: TfmMainTestPirit;

implementation

uses UnCash, Attention, Dm, Un1, u2fdk;

{$R *.dfm}

Procedure TestFp(StrOp:String);
begin
  while TestStatus(StrOp,sMessage)=False do
  begin
    fmAttention.Label1.Caption:=sMessage;
    fmAttention.ShowModal;
    Nums.iRet:=InspectSt(Nums.sRet);
    prWriteLog('~~AttentionShow;'+sMessage+';'+'InspectSt(Nums.sRet)='+IntToStr(Nums.iRet));
  end;
end;

{
procedure prClearBuf;
begin
  with fmMainTestPirit do
  begin
    Fillchar(Buffer, Sizeof(Buffer), 0);
    BufferCount:=0;
  end;
end;
}
procedure prWM1(StrWk:string);
begin
  with fmMainTestPirit do
  begin
    Memo1.Lines.Add(FormatDateTime('hh.mm ss:zzz',now)+' '+StrWk)
  end;
end;

procedure TfmMainTestPirit.FormShow(Sender: TObject);
begin
  Memo1.Clear;
  NumPacket:=33;
end;

procedure Delay(MSecs: Longint);
var
  FirstTickCount, Now: Longint;
begin
  FirstTickCount := GetTickCount;
  repeat
    Application.ProcessMessages;
    { allowing access to other controls, etc. }
    Now := GetTickCount;
  until (Now - FirstTickCount >= MSecs) or (Now < FirstTickCount);
end;

function Its(iV:Integer):String;
begin
  Result:=IntToStr(iV);
end;

procedure TfmMainTestPirit.cxButton1Click(Sender: TObject);
var StrP:string;
begin
  try
//    if DevPrint.Active then DevPrint.Close;

    if DevPrint.Active=False then
    begin
      cxButton1.Enabled:=False;
      StrP:='COM'+its(cxSpinEdit1.Value);
      prWM1('�������� ����� '+StrP);

      DevPrint.DeviceName:=StrP;
      DevPrint.Baudrate:=br57600;
      DevPrint.Parity:=paNone;
      DevPrint.Stopbits:=sb1;
      DevPrint.Databits:=db8;

      DevPrint.Open;
      prWM1('���� '+StrP+'  ��');
      delay(33);
//      DevPrint.Close;
      delay(100);
    end;
  except
    prWM1('������ �������� ����� '+StrP);
  end;
  cxButton1.Enabled:=True;
end;

procedure TfmMainTestPirit.cxButton2Click(Sender: TObject);
var StrP:string;
begin
  cxButton2.Enabled:=False;
  StrP:='COM'+its(cxSpinEdit1.Value);
  if fFisOpen(StrP,DevPrint) then
  begin
    prWM1('�������� ����� � ���');

    if fTestFis(StrP,DevPrint) then prWM1('Ok') else prWM1('Error');

  end;
  cxButton2.Enabled:=True;
end;

procedure TfmMainTestPirit.DevPrintRxChar(Sender: TObject; Count: Integer);
begin
  BufferCount:=DevPrint.ReadBuf(Buffer, Count);
  {
  if Bytes >0 then
  begin
    for P:=0 to Bytes-1 do
    begin
      sScan:=sScan + Char(buffer[P]);
    end;
  end;
  }
end;

procedure TfmMainTestPirit.N1Click(Sender: TObject);
begin
  Memo1.Clear;
end;

procedure TfmMainTestPirit.cxButton3Click(Sender: TObject);
var StrP:string;
    Buff:Array[1..10] of Char;
begin
  cxButton3.Enabled:=False;
  try

    if DevPrint.Active=False then
    begin
      StrP:='COM'+its(cxSpinEdit1.Value);
      DevPrint.DeviceName:=StrP;
      DevPrint.Baudrate:=br57600;
      DevPrint.Parity:=paNone;
      DevPrint.Stopbits:=sb1;
      DevPrint.Databits:=db8;

      DevPrint.Open;
      prWM1('���� '+StrP+'  ��'); delay(10);
    end;
    if DevPrint.Active=True then
    begin
      prWM1('��������� ������');

      prClearBuf;
{
      Buff[1]:=#$02;
      Buff[2]:='P';
      Buff[3]:='I';
      Buff[4]:='R';
      Buff[5]:='I';
      Buff[6]:=#$33;
      Buff[7]:=#$0A;
      Buff[8]:=#$0A;
      Buff[9]:=#$0A;
      Buff[10]:=#$0A;
      Buff[11]:=#$0A;
      Buff[12]:=#$0A;
      }
      Buff[1]:=#$0A;
      DevPrint.WriteBuf(Buff,1);
    end;
  except
    prWM1('������ �������� ����� '+StrP);
  end;
  cxButton3.Enabled:=True;
end;

procedure TfmMainTestPirit.cxButton5Click(Sender: TObject);
var StrP:string;
    sRes:string;
    StrS:string;
begin
  StrP:='COM'+its(cxSpinEdit1.Value);
  if fFisOpen(StrP,DevPrint) then
  begin
    prWM1('');
    prWM1('������');
//    StrS:='00'+#$1C;
    StrS:='00';
    sRes:=fSendFis(StrS,DevPrint);
    prWM1(sRes);
  end;

  {
  try
    if DevPrint.Active=False then
    begin
      StrP:='COM'+its(cxSpinEdit1.Value);
      DevPrint.DeviceName:=StrP;
      DevPrint.Baudrate:=br57600;
      DevPrint.Parity:=paNone;
      DevPrint.Stopbits:=sb1;
      DevPrint.Databits:=db8;

      DevPrint.Open;
      prWM1('���� '+StrP+'  ��'); delay(10);
    end;

    if DevPrint.Active=True then
//    if 1=1 then
    begin
      prWM1('��������� �������');

      for n:=1 to 100 do Buff[n]:=#$00;

      prClearBuf;

      Buff[1]:=#$02;
      Buff[2]:='P';
      Buff[3]:='I';
      Buff[4]:='R';
      Buff[5]:='I';
      Buff[6]:='(';
      Buff[7]:='0';
      Buff[8]:='0';
      Buff[9]:=#$1C;
      Buff[10]:=#$03;

      iCRC:=ord(Buff[2]) xor ord(Buff[3]) xor ord(Buff[4]) xor ord(Buff[5]) xor ord(Buff[6]) xor ord(Buff[7]) xor ord(Buff[8]) xor ord(Buff[9]) xor ord(Buff[10]);
      sCRC:=IntToHex(iCRC,2);

      Buff[11]:=sCRC[1];
      Buff[12]:=sCRC[2];

      sRes:='';
      for n:=1 to 12 do
      begin
        sRes:=sRes+Buff[n];
        prWM1(Its(ord(Buff[n]))+'   '+IntToHex(ord(Buff[n]),2)+'   '+Buff[n]);
      end;

      prWM1(sRes);
      prWM1(Its(iCRC)+'  '+IntToHex(iCRC,2));


      DevPrint.WriteBuf(Buff,12);

//      Delay(200);
      prWM1('-------------------------------');

      for n:=1 to 100 do
      begin
        if BufferCount>0 then
        begin
          delay(20);
          Break;
        end;
        Delay(10);
      end;

      sRes:='';  l:=BufferCount;
      for n:=1 to l do
      begin
        sRes:=sRes+Buffer[n];
        prWM1(Its(ord(Buffer[n])));
      end;

      prWM1(sRes);
    end;
  except
    prWM1('������ �������� ����� '+StrP);
  end;}
end;

procedure TfmMainTestPirit.cxButton6Click(Sender: TObject);
var StrP:string;
    sRes:string;
    StrS,StrPr:string;
    n:Integer;
begin
  StrP:='COM'+its(cxSpinEdit1.Value);
  if fFisOpen(StrP,DevPrint) then
  begin
    prWM1('');
    prWM1('������ ������, ����� �������');
    StrS:='10'+Formatdatetime('ddmmyy',Date)+#$1C+Formatdatetime('hhnnss',now)+#$1C;
    StrPr:='';
    for n:=1 to Length(StrS) do StrPr:=StrPr+'|'+IntToHex(ord(StrS[n]),2);
    sRes:=fSendFis(StrS,DevPrint);
    StrPr:=StrPr+'   ';
    for n:=1 to Length(sRes) do StrPr:=StrPr+'|'+IntToHex(ord(sRes[n]),2);
    prWM1(StrPr);
  end;

  {
  try
    if DevPrint.Active=False then
    begin
      StrP:='COM'+its(cxSpinEdit1.Value);
      DevPrint.DeviceName:=StrP;
      DevPrint.Baudrate:=br57600;
      DevPrint.Parity:=paNone;
      DevPrint.Stopbits:=sb1;
      DevPrint.Databits:=db8;

      DevPrint.Open;
      prWM1('���� '+StrP+'  ��'); delay(10);
    end;

    if DevPrint.Active=True then
//    if 1=1 then
    begin
      prWM1('������ ������');

      prClearBuf;
      StrS:=#$02+'PIRI'+#$33+'10'+Formatdatetime('ddmmyy',Date)+#$1C+Formatdatetime('hhnnss',now)+#$1C+#$03;
      StrS:=StrS+fCRC(StrS);
      l:=Length(StrS);

      for n:=1 to l do prWM1(Its(ord(StrS[n]))+'   '+IntToHex(ord(StrS[n]),2)+'   '+StrS[n]);
      for n:=1 to l do Buff[n]:=StrS[n];

      prWM1(StrS);

      DevPrint.WriteBuf(Buff,l);

//      Delay(200);
      prWM1('-------------------------------');

      for n:=1 to 100 do
      begin
        if BufferCount>0 then
        begin
          delay(20);
          Break;
        end;
        Delay(10);
      end;

      sRes:='';  l:=BufferCount;
      for n:=1 to l do
      begin
        sRes:=sRes+Buffer[n];
        prWM1(Its(ord(Buffer[n]))+'   '+IntToHex(ord(Buffer[n]),2)+'   '+Buffer[n]);
      end;

      prWM1(sRes);
    end;
  except
    prWM1('������ �������� ����� '+StrP);
  end;
  }
end;

procedure TfmMainTestPirit.cxButton7Click(Sender: TObject);
var StrP:string;
    sRes:string;
    StrS:string;
begin
  StrP:='COM'+its(cxSpinEdit1.Value);
  if fFisOpen(StrP,DevPrint) then
  begin
    prWM1('');
    prWM1('��������� X ������, ����� �������');
    StrS:='20'+AnsiToOemConvert('������ �.�.')+#$1C;
    sRes:=fSendFis(StrS,DevPrint);
    prWM1(sRes);
  end;
end;

procedure TfmMainTestPirit.cxButton8Click(Sender: TObject);
var StrP:string;
begin
  StrP:='COM'+its(cxSpinEdit1.Value);
  if fFisPrinter(StrP,DevPrint) then prWM1('������� ��') else prWM1('��������!! ��������� �������� ����������');

{
  if fFisOpen(StrP,DevPrint) then
  begin
    prWM1('');
    prWM1('������ ��');
    StrS:='04';
    sRes:=fSendFis(StrS,DevPrint);
    prWM1(sRes);
    prWM1(sRes[6]+' 6');
    prWM1(sRes[7]+' 7');
    prWM1(sRes[8]+' 8');

  end;
}
end;

procedure TfmMainTestPirit.cxButton9Click(Sender: TObject);
var StrP:string;
    sRes:string;
    StrS:string;
    n:Integer;
begin
  StrP:='COM'+its(cxSpinEdit1.Value);
  if fFisOpen(StrP,DevPrint) then
  begin
    prWM1('');

    for n:=1 to 18 do
    begin
      prWM1('');
      StrS:='01'+IntToHex(n,2)+#$1C;
      sRes:=fSendFis(StrS,DevPrint);
      prWM1(Its(n)+'   '+sRes);
    end;
  end;
end;

procedure TfmMainTestPirit.cxButton10Click(Sender: TObject);
var StrP:string;
    sRes:string;
    StrS:string;
begin
  StrP:='COM'+its(cxSpinEdit1.Value);
  if fFisOpen(StrP,DevPrint) then
  begin
    prWM1('');
    prWM1('������� �����');
    StrS:='23'+AnsiToOemConvert('������ �.�.')+#$1C;
    sRes:=fSendFis(StrS,DevPrint);
    prWM1(sRes);
  end;
end;

procedure TfmMainTestPirit.cxButton11Click(Sender: TObject);
var StrP:string;
    sRes:string;
    StrS:string;
begin
  StrP:='COM'+its(cxSpinEdit1.Value);
  if fFisOpen(StrP,DevPrint) then
  begin
    prWM1('');
    prWM1('������� ��������');
    StrS:='30'+'01'+#$1C+'01'+#$1C+AnsiToOemConvert('������ �.�.')+#$1C+'33'+#$1C;
    sRes:=fSendFis(StrS,DevPrint);
    prWM1(sRes);

    prWM1('  ������ ������');
    StrS:='40'+AnsiToOemConvert('���� ������ ������')+#$1C+'0'+#$1C;
    sRes:=fSendFis(StrS,DevPrint);
    prWM1(sRes);

    StrS:='40'+AnsiToOemConvert('������ 2')+#$1C+'0'+#$1C;
    sRes:=fSendFis(StrS,DevPrint);
    prWM1(sRes);

    StrS:='40'+AnsiToOemConvert(' ')+#$1C+'0'+#$1C;
    sRes:=fSendFis(StrS,DevPrint);
    prWM1(sRes);

    prWM1('  ������ ��');
    StrS:='40'+AnsiToOemConvert('  ������ ��')+#$1C+'0'+#$1C;
    sRes:=fSendFis(StrS,DevPrint);
    prWM1(sRes);

    StrS:='41'+'02'+#$1C+'03'+#$1C+'60'+#$1C+'02'+#$1C+AnsiToOemConvert('4607074070043')+#$1C;
    sRes:=fSendFis(StrS,DevPrint);
    prWM1(sRes);

    StrS:='40'+AnsiToOemConvert(' ')+#$1C+'0'+#$1C;
    sRes:=fSendFis(StrS,DevPrint);
    prWM1(sRes);

    prWM1('  ������ QR');
    StrS:='40'+AnsiToOemConvert('  ������ QR')+#$1C+'0'+#$1C;
    sRes:=fSendFis(StrS,DevPrint);
    prWM1(sRes);

    StrS:='41'+'02'+#$1C+'05'+#$1C+'05'+#$1C+'08'+#$1C+'22N00000VRCE8S3VU252HLI61028005476735PHMDFLTXKM5B5830J737S032YOYY6S7'+#$1C;
    sRes:=fSendFis(StrS,DevPrint);
    prWM1(sRes);

    StrS:='40'+AnsiToOemConvert(' ')+#$1C+'0'+#$1C;
    sRes:=fSendFis(StrS,DevPrint);
    prWM1(sRes);

     prWM1('������� ��������');
    StrS:='31'+'0'+#$1C+AnsiToOemConvert('����� ����������')+#$1C;
    sRes:=fSendFis(StrS,DevPrint);
    prWM1(sRes);
  end;
end;

procedure TfmMainTestPirit.cxButton12Click(Sender: TObject);
var StrP:string;
    sRes:string;
    StrS:string;
begin
  StrP:='COM'+its(cxSpinEdit1.Value);
  if fFisOpen(StrP,DevPrint) then
  begin
    prWM1('');
    prWM1('������� ���������� ��������');
    StrS:='30'+'02'+#$1C+'01'+#$1C+AnsiToOemConvert('������ �.�.')+#$1C+'33'+#$1C;
    sRes:=fSendFis(StrS,DevPrint);
    prWM1(sRes);

    {

01 16:496 ������� ���������� ��������
01 16:574 #300023
01 19:119   �������� ������� 1
01 20:291   �������� ������� 1
01 20:369 %420020
01 20:400 %420020
01 22:589   �������� ������� 1
01 22:668 &420023
01 25:373   �������� ������� 1
01 25:467 '420022
01 29:782   �������� ������� 1
01 29:869 (42002D
01 29:947   �������� ������� 1
01 30:041 )42002C
01 35:281 
01 35:296 ������� 1
01 35:359 *440029
01 35:359 
01 35:359 ������� 2
01 35:380 +440028
01 44:908 ������������ ���
01 46:035 ,32002E


    prWM1('  �������� ������� 1');
    StrS:='42'+AnsiToOemConvert('����� 1')+#$1C+'25532'+#$1C+'1'+#$1C+'0.01'+#$1C+'1'+#$1C+'1'+#$1C+'1'+#$1C+'0'+#$1C+' '+#$1C+'0.0'+#$1C;
    sRes:=fSendFis(StrS,DevPrint);
    prWM1(sRes);

    prWM1('  �������� ������� 2');
    StrS:='42'+AnsiToOemConvert('����� 2')+#$1C+'25533'+#$1C+'1'+#$1C+'0.01'+#$1C+'1'+#$1C+'1'+#$1C+'1'+#$1C+'0'+#$1C+' '+#$1C+'0.0'+#$1C;
    sRes:=fSendFis(StrS,DevPrint);
    prWM1(sRes);

    prWM1('');
    prWM1('������� 1');
    StrS:='44';
    sRes:=fSendFis(StrS,DevPrint);
    prWM1(sRes);

    prWM1('');
    prWM1('������� 2');
    StrS:='44';
    sRes:=fSendFis(StrS,DevPrint);
    prWM1(sRes);

    prWM1('������� ��������');
    StrS:='31'+'0'+#$1C+AnsiToOemConvert('����� ����������')+#$1C;
    sRes:=fSendFis(StrS,DevPrint);
    prWM1(sRes);
    }
  end;
end;

procedure TfmMainTestPirit.cxButton14Click(Sender: TObject);
var StrP:string;
    sRes:string;
    StrS:string;
begin
  StrP:='COM'+its(cxSpinEdit1.Value);
  if fFisOpen(StrP,DevPrint) then
  begin
    prWM1('������� ��������');
    StrS:='31'+'0'+#$1C+AnsiToOemConvert(' ')+#$1C;
    sRes:=fSendFis(StrS,DevPrint);
    prWM1(sRes);
  end;
end;

procedure TfmMainTestPirit.cxButton15Click(Sender: TObject);
var StrP:string;
    sRes:string;
    StrS:string;
begin
  StrP:='COM'+its(cxSpinEdit1.Value);
  if fFisOpen(StrP,DevPrint) then
  begin
    prWM1('������������ ���');
    StrS:='32';
    sRes:=fSendFis(StrS,DevPrint);
    prWM1(sRes);
  end;
end;

procedure TfmMainTestPirit.cxButton13Click(Sender: TObject);
var StrP,sRes,StrS:string;
begin
  StrP:='COM'+its(cxSpinEdit1.Value);
  if fFisOpen(StrP,DevPrint) then
  begin                                                                                         //3 -�� ����������
    prWM1('  �������� ������� 1');                                                              //10 ������� ������ ���������� �� �����
    StrS:='42'+AnsiToOemConvert('����� ��������� ����')+#$1C+'25532'+#$1C+'100'+#$1C+'0.019999999'+#$1C+'3'+#$1C+'1'+#$1C+'1'+#$1C+'0'+#$1C+' '+#$1C+'0.0'+#$1C;
    sRes:=fSendFis(StrS,DevPrint);
    prWM1(sRes);
  end;
end;

procedure TfmMainTestPirit.cxButton16Click(Sender: TObject);
var StrP:string;
    sRes:string;
    StrS:string;
begin
  StrP:='COM'+its(cxSpinEdit1.Value);
  if fFisOpen(StrP,DevPrint) then
  begin
    prWM1('');
    prWM1('������� 1');
    StrS:='44';
    sRes:=fSendFis(StrS,DevPrint);
    prWM1(sRes);

{
    prWM1('');
    prWM1('������� 2');
    StrS:='44';
    sRes:=fSendFis(StrS,DevPrint);
    prWM1(sRes);}
  end;
end;

procedure TfmMainTestPirit.cxButton17Click(Sender: TObject);
var StrP:string;
    sRes:string;
    StrS:string;
begin
  StrP:='COM'+its(cxSpinEdit1.Value);
  if fFisOpen(StrP,DevPrint) then
  begin
    prWM1('  ������');
    StrS:='47'+'0'+#$1C+'1'+#$1C+AnsiToOemConvert(' ')+#$1C;
    sRes:=fSendFis(StrS,DevPrint);
    prWM1(sRes);
  end;
end;

procedure TfmMainTestPirit.cxButton18Click(Sender: TObject);
var StrP:string;
    sRes:string;
    StrS:string;
begin
  StrP:='COM'+its(cxSpinEdit1.Value);
  if fFisOpen(StrP,DevPrint) then
  begin
    prWM1('');
    prWM1('��������� Z ������');
    StrS:='21'+AnsiToOemConvert('������ �.�.')+#$1C+'0'+#$1C;
    sRes:=fSendFis(StrS,DevPrint);
    prWM1(sRes);
  end;
end;

procedure TfmMainTestPirit.cxButton19Click(Sender: TObject);
var StrP:string;
    sRes:string;
    StrS:string;
begin
  StrP:='COM'+its(cxSpinEdit1.Value);
  if fFisOpen(StrP,DevPrint) then
  begin
    prWM1('');
    prWM1('��������� ����');
    StrS:='03'+'1'+#$1C;
    sRes:=fSendFis(StrS,DevPrint);
    prWM1(sRes);
  end;
end;

procedure TfmMainTestPirit.cxButton20Click(Sender: TObject);
var rSum,dSum,nSum:Real;
begin
  prWM1('��������� ���� (�����������)');
  if fChSum(DevPrint,rSum,dSum,nSum) then
  begin
    prWM1('   ����� ���� '+fts(rSum));
    prWM1('   ����� ������ '+fts(dSum));
    prWM1('   ����� ������� '+fts(nSum));

  end else prWM1('������ ��������� ����������');
end;

procedure TfmMainTestPirit.cxButton21Click(Sender: TObject);
Var sDateT:string;
begin
  Commonset.CashNum:=1;
  if dmC.FisPrint.Active=False then dmC.FisPrint.DeviceName:='COM'+its(cxSpinEdit1.Value);
  if CashDate(sDateT) then prWM1(sDateT) else prWM1('������ ��������� ���� ���');
end;

procedure TfmMainTestPirit.cxButton22Click(Sender: TObject);
begin
  Commonset.CashNum:=1;
  if dmC.FisPrint.Active=False then dmC.FisPrint.DeviceName:='COM'+its(cxSpinEdit1.Value);
  if OpenZ then prWM1('����� �������.') else prWM1('����� �������');
end;

procedure TfmMainTestPirit.cxButton23Click(Sender: TObject);
var iSt:Integer;
begin
  Commonset.CashNum:=1;
  if dmC.FisPrint.Active=False then dmC.FisPrint.DeviceName:='COM'+its(cxSpinEdit1.Value);
  if More24H(iSt) then prWM1('����� ������ 24 �����.') else prWM1('����� ����� 24 �����');
end;

procedure TfmMainTestPirit.cxButton24Click(Sender: TObject);
var StrP:string;
    sRes:string;
    StrS:string;
begin
  StrP:='COM'+its(cxSpinEdit1.Value);
  if fFisOpen(StrP,DevPrint) then
  begin
    prWM1('');
    prWM1('������� ���������� ��������');

    StrS:='30'+'03'+#$1C+'01'+#$1C+AnsiToOemConvert('������ �.�.')+#$1C+'33'+#$1C;
    sRes:=fSendFis(StrS,DevPrint);
    prWM1(sRes);
  end;
end;

procedure TfmMainTestPirit.cxButton25Click(Sender: TObject);
var StrP:string;
    sRes:string;
    StrS,StrWk,StrPar:string;
    iF1,iF2,iR:Integer;
begin
  StrP:='COM'+its(cxSpinEdit1.Value);
  if fFisOpen(StrP,DevPrint) then
  begin
    prWM1('');
    prWM1('������');
//    StrS:='00'+#$1C;
    StrS:='00';
    sRes:=fSendFis(StrS,DevPrint);
    prWM1(sRes);

    StrWk:=sRes;

    if Pos(#$1C,StrWk)>0 then
    begin
      Delete(StrWk,1,Pos(#$1C,StrWk));
      if Pos(#$1C,StrWk)>0 then
      begin
        StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1); //������ ������� ������ ���
        iF1:=StrToIntDef(Trim(StrPar),0);
        Delete(StrWk,1,Pos(#$1C,StrWk));
        if Pos(#$1C,StrWk)>0 then
        begin
          StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1); //������ ���������
          iF2:=StrToIntDef(Trim(StrPar),0);
        end;
      end;
    end;
    iR:=iF2 and $0F;  //������ ��� �� ������� 2-�������   3-�������
    if iR in [2,3] then prWM1('��� ������ '+its(iR));
  end;
end;

procedure TfmMainTestPirit.cxButton26Click(Sender: TObject);
var StrP,sRes,StrS,StrWk:string;
    iF1:Integer;
    StrPar:string;
begin
  StrP:='COM'+its(cxSpinEdit1.Value);
  if fFisOpen(StrP,DevPrint) then
  begin
    prWM1(' ������ �������� ����.');
    StrS:='01'+'02'+#$1C;
    sRes:=fSendFis(StrS,DevPrint);

    StrWk:=sRes;
    iF1:=0;

    if Pos(#$1C,StrWk)>0 then
    begin
      Delete(StrWk,1,Pos(#$1C,StrWk));
      if Pos(#$1C,StrWk)>0 then
      begin
        StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1); //������ ������� ������ ���
        iF1:=StrToIntDef(Trim(StrPar),0);
      end;
    end;

    prWM1('  � ���� '+Its(iF1)+'   '+sRes);
  end;
end;

procedure TfmMainTestPirit.cxButton27Click(Sender: TObject);
var rSum1,rSum2,rSum3,rSum4:Real;
begin
  Commonset.CashNum:=1;
  if dmC.FisPrint.Active=False then dmC.FisPrint.DeviceName:='COM'+its(cxSpinEdit1.Value);
  if GetSumDisc(rSum1,rSum2,rSum3,rSum4) then
  begin
    prWM1('  ����� ');
    prWM1('  '+fts(rSum1));
    prWM1('  '+fts(rSum2));
    prWM1('  '+fts(rSum3));
    prWM1('  '+fts(rSum4));
  end else prWM1('������');
end;

procedure TfmMainTestPirit.cxButton28Click(Sender: TObject);
begin
  Commonset.CashNum:=1;
  if dmC.FisPrint.Active=False then dmC.FisPrint.DeviceName:='COM'+its(cxSpinEdit1.Value);

  prWM1('  ������ QR ');
  if OpenNFDoc then
  begin
    PrintQR('22N00000VRCE8S3VU252HLI61028005476735PHMDFLTXKM5B5830J737S032YOYY6S7');
    CloseNFDoc;
  end;
  prWM1('  ������ ���������.');
end;

procedure TfmMainTestPirit.cxButton29Click(Sender: TObject);
begin
  Commonset.CashNum:=1;
  if dmC.FisPrint.Active=False then dmC.FisPrint.DeviceName:='COM'+its(cxSpinEdit1.Value);

  prWM1('  ������� �� ');
  if CashDriver then prWM1('  �� ��') else prWM1('  ������ �������� ��.');
end;

procedure TfmMainTestPirit.cxButton30Click(Sender: TObject);
var StrP,sRes,StrS,StrWk:string;
    rSumE,rSumR,rSumRet,rSumIn,rSumOut,rSumB:Real;
    StrPar:string;
    rSumNal,rSumBn,rSumNalRet,rSumBnRet:Real;
    rSumEnd:Real;
begin
  StrP:='COM'+its(cxSpinEdit1.Value);
  if fFisOpen(StrP,DevPrint) then
  begin
    rSumE:=0; rSumR:=0; rSumRet:=0; rSumIn:=0; rSumOut:=0;

    prWM1(' ������ ���� �� ���������� X.');
    StrS:='01'+'12'+#$1C;
    sRes:=fSendFis(StrS,DevPrint);
    StrWk:=sRes;
    if Pos(#$1C,StrWk)>0 then
    begin
      Delete(StrWk,1,Pos(#$1C,StrWk));
      if Pos(#$1C,StrWk)>0 then   //1
      begin
        StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1);
        Delete(StrWk,1,Pos(#$1C,StrWk));
      end;
      if Pos(#$1C,StrWk)>0 then   //2
      begin
        StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1);
        Delete(StrWk,1,Pos(#$1C,StrWk));
      end;
      if Pos(#$1C,StrWk)>0 then   //3
      begin
        StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1);
        rSumE:=fS2F(StrPar);
        Delete(StrWk,1,Pos(#$1C,StrWk));
      end;
      if Pos(#$1C,StrWk)>0 then   //4
      begin
        StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1);
        Delete(StrWk,1,Pos(#$1C,StrWk));
      end;
      if Pos(#$1C,StrWk)>0 then   //5
      begin
        StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1);
        rSumR:=fS2F(StrPar);
        Delete(StrWk,1,Pos(#$1C,StrWk));
      end;
      if Pos(#$1C,StrWk)>0 then   //6
      begin
        StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1);
        Delete(StrWk,1,Pos(#$1C,StrWk));
      end;
      if Pos(#$1C,StrWk)>0 then   //7
      begin
        StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1);
        rSumRet:=fS2F(StrPar);
        Delete(StrWk,1,Pos(#$1C,StrWk));
      end;
      if Pos(#$1C,StrWk)>0 then   //8
      begin
        StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1);
        Delete(StrWk,1,Pos(#$1C,StrWk));
      end;
      if Pos(#$1C,StrWk)>0 then   //9
      begin
        StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1);
        Delete(StrWk,1,Pos(#$1C,StrWk));
      end;
      if Pos(#$1C,StrWk)>0 then   //10
      begin
        StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1);
        Delete(StrWk,1,Pos(#$1C,StrWk));
      end;
      if Pos(#$1C,StrWk)>0 then   //11
      begin
        StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1);
        rSumIn:=fS2F(StrPar);
        Delete(StrWk,1,Pos(#$1C,StrWk));
      end;
      if Pos(#$1C,StrWk)>0 then   //12
      begin
        StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1);
        Delete(StrWk,1,Pos(#$1C,StrWk));
      end;
      if Pos(#$1C,StrWk)>0 then   //13
      begin
        StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1);
        rSumOut:=fS2F(StrPar);
        Delete(StrWk,1,Pos(#$1C,StrWk));
      end;
    end;

    rSumB:=rSumE-rSumIn+rSumOut+rSumRet-rSumR;  //����� �� ������ �����

//    prWM1('  ����� � ����� �� ���. ����� '+fts(rSumB)+'   '+sRes);


    StrS:='01'+'03'+#$1C;
    sRes:=fSendFis(StrS,DevPrint);
    StrWk:=sRes;
    if Pos(#$1C,StrWk)>0 then
    begin
      Delete(StrWk,1,Pos(#$1C,StrWk));
      if Pos(#$1C,StrWk)>0 then   //1
      begin
        StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1);
        rSumNal:=fS2F(StrPar);
        Delete(StrWk,1,Pos(#$1C,StrWk));
      end;
      if Pos(#$1C,StrWk)>0 then   //2
      begin
        StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1);
        rSumBn:=fS2F(StrPar);
        Delete(StrWk,1,Pos(#$1C,StrWk));
      end;
    end;

    StrS:='01'+'05'+#$1C;
    sRes:=fSendFis(StrS,DevPrint);
    StrWk:=sRes;
    if Pos(#$1C,StrWk)>0 then
    begin
      Delete(StrWk,1,Pos(#$1C,StrWk));
      if Pos(#$1C,StrWk)>0 then   //1
      begin
        StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1);
        rSumNalRet:=fS2F(StrPar);
        Delete(StrWk,1,Pos(#$1C,StrWk));
      end;
      if Pos(#$1C,StrWk)>0 then   //2
      begin
        StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1);
        rSumBnRet:=fS2F(StrPar);
        Delete(StrWk,1,Pos(#$1C,StrWk));
      end;
    end;

    prWM1('�����:   �� ���� '+fts(rSumB)+'   ������ '+fts(rSumNal)+'   ��������� '+fts(rSumNalRet)+'   ����� '+fts(rSumB+rSumNal-rSumNalRet));


  end;
end;

procedure TfmMainTestPirit.cxButton31Click(Sender: TObject);
var StrP,sRes,StrS,StrWk:string;
    StrPar:string;
    rSumNal,rSumBn,rSumNalRet,rSumBnRet:Real;
    rSumEnd:Real;
begin
  StrP:='COM'+its(cxSpinEdit1.Value);

  if fFisOpen(StrP,DevPrint) then
  begin
    StrS:='01'+'03'+#$1C;
    sRes:=fSendFis(StrS,DevPrint);
    StrWk:=sRes;
    if Pos(#$1C,StrWk)>0 then
    begin
      Delete(StrWk,1,Pos(#$1C,StrWk));
      if Pos(#$1C,StrWk)>0 then   //1
      begin
        StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1);
        rSumNal:=fS2F(StrPar);
        Delete(StrWk,1,Pos(#$1C,StrWk));
      end;
      if Pos(#$1C,StrWk)>0 then   //2
      begin
        StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1);
        rSumBn:=fS2F(StrPar);
        Delete(StrWk,1,Pos(#$1C,StrWk));
      end;
    end;

    StrS:='01'+'05'+#$1C;
    sRes:=fSendFis(StrS,DevPrint);
    StrWk:=sRes;
    if Pos(#$1C,StrWk)>0 then
    begin
      Delete(StrWk,1,Pos(#$1C,StrWk));
      if Pos(#$1C,StrWk)>0 then   //1
      begin
        StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1);
        rSumNalRet:=fS2F(StrPar);
        Delete(StrWk,1,Pos(#$1C,StrWk));
      end;
      if Pos(#$1C,StrWk)>0 then   //2
      begin
        StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1);
        rSumBnRet:=fS2F(StrPar);
        Delete(StrWk,1,Pos(#$1C,StrWk));
      end;
    end;

    prWM1('�����:   '+'   ������ '+fts(rSumNal)+'   ��������� '+fts(rSumNalRet));

  end;
end;

procedure TfmMainTestPirit.cxButton32Click(Sender: TObject);
var StrP,sRes,StrS:string;
    rSum:Real;
begin
 //��������
  StrP:='COM'+its(cxSpinEdit1.Value);

  if fFisOpen(StrP,DevPrint) then
  begin
    //������� ��� ��������

    StrS:='30'+'04'+#$1C+'01'+#$1C+AnsiToOemConvert('������ �.�.')+#$1C+'33'+#$1C;
    sRes:=fSendFis(StrS,DevPrint);

    rSum:=10;

    StrS:='48'+AnsiToOemConvert('���. ���.')+#$1C+fs(rv(rSum))+#$1C;
    sRes:=fSendFis(StrS,DevPrint);

    StrS:='31'+'0'+#$1C+AnsiToOemConvert('����� ����������')+#$1C;
    sRes:=fSendFis(StrS,DevPrint);
  end;
end;

procedure TfmMainTestPirit.cxButton33Click(Sender: TObject);
var StrP,sRes,StrS:string;
    rSum:Real;
begin
  StrP:='COM'+its(cxSpinEdit1.Value);
  if fFisOpen(StrP,DevPrint) then
  begin
    //������� ��� �������

    StrS:='30'+'05'+#$1C+'01'+#$1C+AnsiToOemConvert('������ �.�.')+#$1C+'33'+#$1C;
    sRes:=fSendFis(StrS,DevPrint);

    rSum:=10;

    StrS:='48'+AnsiToOemConvert('���. ���.')+#$1C+fs(rv(rSum))+#$1C;
    sRes:=fSendFis(StrS,DevPrint);

    StrS:='31'+'0'+#$1C+AnsiToOemConvert('����� ����������')+#$1C;
    sRes:=fSendFis(StrS,DevPrint);
  end;
end;

procedure TfmMainTestPirit.cxButton4Click(Sender: TObject);
begin
  Commonset.CashNum:=1;
  if dmC.FisPrint.Active=False then dmC.FisPrint.DeviceName:='COM'+its(cxSpinEdit1.Value);

  if GetNums then prWM1('  � ����� '+its(Nums.ZNum)+'   � ���� '+its(Nums.iCheckNum)) else prWM1('  ������.');
end;

procedure TfmMainTestPirit.cxButton34Click(Sender: TObject);
var sMess:string;
begin
  Commonset.CashNum:=1;
  if dmC.FisPrint.Active=False then dmC.FisPrint.DeviceName:='COM'+its(cxSpinEdit1.Value);

  if InspectSt(sMess)=0 then prWM1('OK '+sMess) else prWM1('������.'+sMess);
end;

procedure TfmMainTestPirit.cxButton35Click(Sender: TObject);
var StrP,sRes,StrS:string;
begin
// ������� ���-��
  StrP:='COM'+its(cxSpinEdit1.Value);
  if fFisOpen(StrP,DevPrint) then
  begin                                                                                         //3 -�� ����������
    prWM1('  �������� ������� 1');                                                              //10 ������� ������ ���������� �� �����
    StrS:='42'+AnsiToOemConvert('����� ��������� ����')+#$1C+'25532'+#$1C+'1.145'+#$1C+'1.19999999'+#$1C+'3'+#$1C+'1'+#$1C+'1'+#$1C+'0'+#$1C+' '+#$1C+'0.0'+#$1C;
    sRes:=fSendFis(StrS,DevPrint);
    prWM1(sRes);
  end;
end;

procedure TfmMainTestPirit.cxButton36Click(Sender: TObject);
var StrP:string;
    sRes:string;
    StrS:string;
begin
  StrP:='COM'+its(cxSpinEdit1.Value);
  if fFisOpen(StrP,DevPrint) then
  begin
    prWM1('');
    prWM1('������ ��');

    StrS:='78'+'07'+#$1C;
    sRes:=fSendFis(StrS,DevPrint);
    prWM1(sRes);
  end;
end;

end.
