unit MessAlco;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, dxfBackGround, ActnList, XPStyleActnCtrls, ActnMan,
  jpeg, ExtCtrls;

type
  TfmMessAlco = class(TForm)
    dxfBackGround1: TdxfBackGround;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    ActionManager1: TActionManager;
    acExit: TAction;
    procedure acExitExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmMessAlco: TfmMessAlco;

implementation

uses Un1;

{$R *.dfm}

procedure TfmMessAlco.acExitExecute(Sender: TObject);
begin
  if CommonSet.MessAlco<2 then ModalResult:=mrOk;
end;

procedure TfmMessAlco.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if CommonSet.MessAlco<2 then ModalResult:=mrOk
  else
  begin
    if Key=its(iAAnsw) then ModalResult:=mrOk;
  end;
end;

end.
