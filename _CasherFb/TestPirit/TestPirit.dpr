// JCL_DEBUG_EXPERT_GENERATEJDBG OFF
// JCL_DEBUG_EXPERT_INSERTJDBG OFF
// JCL_DEBUG_EXPERT_DELETEMAPFILE OFF
program TestPirit;

uses
  Forms,
  MainTestPirit in 'MainTestPirit.pas' {fmMainTestPirit},
  UnCash in 'UnCash.pas',
  Attention in 'Attention.pas' {fmAttention},
  Dm in 'Dm.pas' {dmC: TDataModule},
  Un1 in 'Un1.pas',
  u2fdk in 'U2FDK.PAS';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfmMainTestPirit, fmMainTestPirit);
  Application.CreateForm(TfmAttention, fmAttention);
  Application.CreateForm(TdmC, dmC);
  Application.Run;
end.
