unit EliseyQr;

interface
 uses DelphiZXIngQRCode,QR_Win1251,QR_URL,QRGraphics,Graphics,SysUtils;

 Type TEliseyQRCODE = class
   constructor Create(Text:string;Width,Height:integer);virtual;
   procedure Free;virtual;
 public
   pText:string;
   pWidth:integer;
   pHeight:integer;
   procedure SaveToFile(FileName:string);
 private
    FQRCode: TDelphiZXingQRCode;
    procedure RemakeQR;
 End;

 implementation

 constructor TEliseyQRCODE.Create(Text:string;Width,Height:integer);
 begin
  pText:=Text;
  pWidth:=Width;
  pHeight:=Height;
  FQRCode := nil;
  FQRCode := TDelphiZXingQRCode.Create;
  FQRCode.RegisterEncoder(ENCODING_WIN1251, TWin1251Encoder);
  FQRCode.RegisterEncoder(ENCODING_URL, TURLEncoder);
  RemakeQR;
 end;

 procedure TEliseyQRCODE.Free;
 begin
  FQRCode.Destroy;
  FQRCode := nil;
 end;

 procedure TEliseyQRCODE.RemakeQR;
 begin
  with FQRCode do
  try
    BeginUpdate;
    Data := pText;
    Encoding := 0;
    ErrorCorrectionOrdinal := TErrorCorrectionOrdinal(0);
    QuietZone := 4;
    MainWidth:=pWidth;
    MainHeight:=pHeight;
    MainH:=44;
    EndUpdate(True);
  finally
  end;
 end;

 procedure TEliseyQRCODE.SaveToFile(FileName:string);
 var
  Bmp: TBitmap;
  S:string;
 begin
    S := LowerCase(ExtractFileExt(FileName));
     if S = '' then raise Exception.Create('������� ���������� .BMP');

    Bmp := nil;
    if S = '.bmp' then
    try
      Bmp := TBitmap.Create;
      MakeBmpElisey(Bmp,pWidth,pHeight, FQRCode, clWhite, clBlack);
      Bmp.SaveToFile(FileName);
      Bmp.Free;
    except
      Bmp.Free;
      raise;
    end
    else
 end;

end.
