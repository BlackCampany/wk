object dmTr: TdmTr
  OldCreateOrder = False
  Left = 766
  Top = 436
  Height = 264
  Width = 445
  object trSelTransp: TpFIBTransaction
    DefaultDatabase = dmC.CasherDb
    TimeoutAction = TARollback
    Left = 29
    Top = 15
  end
  object quSumTr: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT Operation,Sum(TOTALRUB-DSUM) as SumOp  FROM CASHSAIL'
      'WHERE'
      'SHOPINDEX=1'
      'and CASHNUMBER=2'
      'and ZNUMBER=2'
      'Group by Operation'
      'Order by Operation')
    Transaction = trSelTransp
    Database = dmC.CasherDb
    Left = 94
    Top = 12
    object quSumTrOPERATION: TFIBSmallIntField
      FieldName = 'OPERATION'
    end
    object quSumTrSUMOP: TFIBFloatField
      FieldName = 'SUMOP'
    end
  end
  object fbCashSail: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT'
      '    cs.SHOPINDEX,'
      '    cs.CASHNUMBER,'
      '    cs.ZNUMBER,'
      '    cs.CHECKNUMBER,'
      '    cs.ID,'
      '    cs.CHDATE,'
      '    cs.ARTICUL,'
      '    cs.BAR,'
      '    cs.CARDSIZE,'
      '    cs.QUANTITY,'
      '    cs.PRICERUB,'
      '    cs.DPROC,'
      '    cs.DSUM,'
      '    cs.TOTALRUB,'
      '    cs.CASHER,'
      '    cs.DEPART,'
      '    cs.OPERATION,'
      '    cs.SECPOS,'
      '    cs.ECHECK'
      ''
      'FROM'
      '    CASHSAIL cs')
    Transaction = trSelTransp
    Database = dmC.CasherDb
    Left = 154
    Top = 11
    object fbCashSailSHOPINDEX: TFIBSmallIntField
      FieldName = 'SHOPINDEX'
    end
    object fbCashSailCASHNUMBER: TFIBIntegerField
      FieldName = 'CASHNUMBER'
    end
    object fbCashSailZNUMBER: TFIBIntegerField
      FieldName = 'ZNUMBER'
    end
    object fbCashSailCHECKNUMBER: TFIBIntegerField
      FieldName = 'CHECKNUMBER'
    end
    object fbCashSailID: TFIBIntegerField
      FieldName = 'ID'
    end
    object fbCashSailCHDATE: TFIBDateTimeField
      FieldName = 'CHDATE'
    end
    object fbCashSailARTICUL: TFIBStringField
      FieldName = 'ARTICUL'
      Size = 30
      EmptyStrToNull = True
    end
    object fbCashSailBAR: TFIBStringField
      FieldName = 'BAR'
      Size = 15
      EmptyStrToNull = True
    end
    object fbCashSailCARDSIZE: TFIBStringField
      FieldName = 'CARDSIZE'
      Size = 10
      EmptyStrToNull = True
    end
    object fbCashSailQUANTITY: TFIBFloatField
      FieldName = 'QUANTITY'
    end
    object fbCashSailPRICERUB: TFIBFloatField
      FieldName = 'PRICERUB'
    end
    object fbCashSailDPROC: TFIBFloatField
      FieldName = 'DPROC'
    end
    object fbCashSailDSUM: TFIBFloatField
      FieldName = 'DSUM'
    end
    object fbCashSailTOTALRUB: TFIBFloatField
      FieldName = 'TOTALRUB'
    end
    object fbCashSailCASHER: TFIBIntegerField
      FieldName = 'CASHER'
    end
    object fbCashSailDEPART: TFIBIntegerField
      FieldName = 'DEPART'
    end
    object fbCashSailOPERATION: TFIBSmallIntField
      FieldName = 'OPERATION'
    end
    object fbCashSailSECPOS: TFIBSmallIntField
      FieldName = 'SECPOS'
    end
    object fbCashSailECHECK: TFIBSmallIntField
      FieldName = 'ECHECK'
    end
  end
  object fbCashPay: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT DBAR , Sum(DSUM) as DSUM'
      'FROM CASHPAY'
      'where'
      'SHOPINDEX=:SHOPINDEX'
      'and CASHNUMBER=:CASHNUMBER'
      'and ZNUMBER=:ZNUMBER'
      'and CHECKNUMBER=:CHECKNUMBER'
      'Group by DBAR')
    Transaction = dmC.trSelect
    Database = dmC.CasherDb
    UpdateTransaction = dmC.trUpdate
    Left = 216
    Top = 12
    poAskRecordCount = True
  end
  object taCashSail: TTable
    DatabaseName = 'C:\Crystal\POS\1\OUTPUT\'
    SessionName = 'Ses1'
    TableName = 'CASHSAIL.DB'
    Left = 92
    Top = 80
    object taCashSailShopIndex: TSmallintField
      FieldName = 'ShopIndex'
    end
    object taCashSailCashNumber: TSmallintField
      FieldName = 'CashNumber'
    end
    object taCashSailZNumber: TSmallintField
      FieldName = 'ZNumber'
    end
    object taCashSailCheckNumber: TSmallintField
      FieldName = 'CheckNumber'
    end
    object taCashSailID: TSmallintField
      FieldName = 'ID'
    end
    object taCashSailDate: TDateField
      FieldName = 'Date'
    end
    object taCashSailTime: TSmallintField
      FieldName = 'Time'
    end
    object taCashSailCardArticul: TStringField
      FieldName = 'CardArticul'
      Size = 30
    end
    object taCashSailCardSize: TStringField
      FieldName = 'CardSize'
      Size = 10
    end
    object taCashSailQuantity: TFloatField
      FieldName = 'Quantity'
    end
    object taCashSailPriceRub: TCurrencyField
      FieldName = 'PriceRub'
    end
    object taCashSailPriceCur: TCurrencyField
      FieldName = 'PriceCur'
    end
    object taCashSailTotalRub: TCurrencyField
      FieldName = 'TotalRub'
    end
    object taCashSailTotalCur: TCurrencyField
      FieldName = 'TotalCur'
    end
    object taCashSailDepartment: TSmallintField
      FieldName = 'Department'
    end
    object taCashSailCasher: TSmallintField
      FieldName = 'Casher'
    end
    object taCashSailUsingIndex: TSmallintField
      FieldName = 'UsingIndex'
    end
    object taCashSailReplace: TSmallintField
      FieldName = 'Replace'
    end
    object taCashSailOperation: TSmallintField
      FieldName = 'Operation'
    end
    object taCashSailCredCardIndex: TSmallintField
      FieldName = 'CredCardIndex'
    end
    object taCashSailDiscCliIndex: TSmallintField
      FieldName = 'DiscCliIndex'
    end
    object taCashSailLinked: TSmallintField
      FieldName = 'Linked'
    end
  end
  object taCashDCRD: TTable
    DatabaseName = 'C:\Crystal\POS\1\OUTPUT\'
    SessionName = 'Ses1'
    TableName = 'CASHDCRD.DB'
    Left = 252
    Top = 80
    object taCashDCRDShopIndex: TSmallintField
      FieldName = 'ShopIndex'
    end
    object taCashDCRDCashNumber: TSmallintField
      FieldName = 'CashNumber'
    end
    object taCashDCRDZNumber: TSmallintField
      FieldName = 'ZNumber'
    end
    object taCashDCRDCheckNumber: TSmallintField
      FieldName = 'CheckNumber'
    end
    object taCashDCRDCardType: TSmallintField
      FieldName = 'CardType'
    end
    object taCashDCRDCardNumber: TStringField
      FieldName = 'CardNumber'
      Size = 22
    end
    object taCashDCRDDiscountRub: TCurrencyField
      FieldName = 'DiscountRub'
    end
    object taCashDCRDDiscountCur: TCurrencyField
      FieldName = 'DiscountCur'
    end
  end
  object dsCash: TDataSource
    Left = 92
    Top = 132
  end
  object taCashDisc: TTable
    DatabaseName = 'C:\Crystal\POS\1\OUTPUT\'
    SessionName = 'Ses1'
    TableName = 'CASHDISC.DB'
    Left = 184
    Top = 80
    object taCashDiscShopIndex: TSmallintField
      FieldName = 'ShopIndex'
    end
    object taCashDiscCashNumber: TSmallintField
      FieldName = 'CashNumber'
    end
    object taCashDiscZNumber: TSmallintField
      FieldName = 'ZNumber'
    end
    object taCashDiscCheckNumber: TSmallintField
      FieldName = 'CheckNumber'
    end
    object taCashDiscID: TSmallintField
      FieldName = 'ID'
    end
    object taCashDiscDiscountIndex: TSmallintField
      FieldName = 'DiscountIndex'
    end
    object taCashDiscDiscountProc: TFloatField
      FieldName = 'DiscountProc'
    end
    object taCashDiscDiscountRub: TCurrencyField
      FieldName = 'DiscountRub'
    end
    object taCashDiscDiscountCur: TCurrencyField
      FieldName = 'DiscountCur'
    end
  end
  object Session1: TSession
    Active = True
    NetFileDir = 'C:\'
    SessionName = 'Ses1'
    Left = 293
    Top = 14
  end
  object quCashPay: TpFIBDataSet
    SelectSQL.Strings = (
      
        'SELECT SHOPINDEX,CASHNUMBER,ZNUMBER,CHECKNUMBER,CASHER,CHECKSUM,' +
        'PAYSUM,DSUM,DBAR,COUNTPOS,CHDATE,PAYMENT'
      'FROM CASHPAY '
      'where DSUM<>0'
      'and CASHNUMBER=1'
      'and ZNUMBER=1'
      '')
    Transaction = dmC.trSelect
    Database = dmC.CasherDb
    UpdateTransaction = dmC.trUpdate
    Left = 180
    Top = 152
    poAskRecordCount = True
    object quCashPaySHOPINDEX: TFIBSmallIntField
      FieldName = 'SHOPINDEX'
    end
    object quCashPayCASHNUMBER: TFIBIntegerField
      FieldName = 'CASHNUMBER'
    end
    object quCashPayZNUMBER: TFIBIntegerField
      FieldName = 'ZNUMBER'
    end
    object quCashPayCHECKNUMBER: TFIBIntegerField
      FieldName = 'CHECKNUMBER'
    end
    object quCashPayCASHER: TFIBIntegerField
      FieldName = 'CASHER'
    end
    object quCashPayCHECKSUM: TFIBFloatField
      FieldName = 'CHECKSUM'
    end
    object quCashPayPAYSUM: TFIBFloatField
      FieldName = 'PAYSUM'
    end
    object quCashPayDSUM: TFIBFloatField
      FieldName = 'DSUM'
    end
    object quCashPayDBAR: TFIBStringField
      FieldName = 'DBAR'
      Size = 22
      EmptyStrToNull = True
    end
    object quCashPayCOUNTPOS: TFIBSmallIntField
      FieldName = 'COUNTPOS'
    end
    object quCashPayCHDATE: TFIBDateTimeField
      FieldName = 'CHDATE'
    end
    object quCashPayPAYMENT: TFIBSmallIntField
      FieldName = 'PAYMENT'
    end
  end
end
