unit GetAMark;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons, ExtCtrls,
  cxControls, cxContainer, cxEdit, cxLabel, ActnList, XPStyleActnCtrls,
  ActnMan;

type
  TfmGetAM = class(TForm)
    Panel1: TPanel;
    cxButton1: TcxButton;
    Panel2: TPanel;
    cxLabel1: TcxLabel;
    cxButton2: TcxButton;
    Panel3: TPanel;
    cxLabel2: TcxLabel;
    Edit1: TEdit;
    amGetAM: TActionManager;
    acReadAM: TAction;
    acExit: TAction;
    procedure FormShow(Sender: TObject);
    procedure acReadAMExecute(Sender: TObject);
    procedure Edit1KeyPress(Sender: TObject; var Key: Char);
    procedure acExitExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmGetAM: TfmGetAM;

implementation

uses Un1, Dm, dbSql;

{$R *.dfm}

procedure TfmGetAM.FormShow(Sender: TObject);
begin
  prWriteLog('_!ShowQuestGetAMark;');
  cxLabel2.Caption:='';
  cxLabel1.Caption:='�������� �������� �����';
end;

procedure TfmGetAM.acReadAMExecute(Sender: TObject);
begin
  Edit1.Text:='';
  Edit1.SetFocus;
  Edit1.SelectAll;
end;

procedure TfmGetAM.Edit1KeyPress(Sender: TObject; var Key: Char);
var bCh:Byte;
    sBar:string;
    iNumPos:Integer;
begin
  bCh:=ord(Key);
  if (bCh = 13) then
  begin //���� ����������                       //10-0A
    sBar:=Edit1.Text;
    cxLabel2.Caption:=sBar;

    if Length(sBar)>=55 then
//      if Pos('N',sBar)=3 then
        if Pos('-',sBar)=0 then
        begin
          iNumPos:=prFindAMInCh(sBar);
          if iNumPos<0 then
          begin
            if Check.Operation=1 then
            begin
              if fCanSaleAM(sBar)=False then
              begin
                cxLabel1.Caption:='��������� ������� ������ �� ����� �������� ����� ���������.';
              end else
              begin
                Delay(200);
                fmGetAM.ModalResult:=mrOk;
              end;
            end else
            begin
              Delay(200);
              fmGetAM.ModalResult:=mrOk;
            end;
          end else cxLabel1.Caption:='������� ������ ����������. �������� ����� ��� ���� � ���� ���. ('+its(iNumPos)+').';
        end;
    Edit1.Text:='';
    Edit1.SetFocus;
    Edit1.SelectAll;
  end;
end;

procedure TfmGetAM.acExitExecute(Sender: TObject);
begin
  fmGetAM.ModalResult:=mrCancel;
end;

end.
