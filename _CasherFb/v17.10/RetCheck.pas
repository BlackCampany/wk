unit RetCheck;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxCurrencyEdit, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxControls, cxGridCustomView, cxGrid, ExtCtrls, dxmdaset, StdCtrls,
  Menus, cxLookAndFeelPainters, cxButtons, cxContainer, cxTextEdit,
  cxImageComboBox, ActnList, XPStyleActnCtrls, ActnMan;

type
  TfmRetCheck = class(TForm)
    Panel1: TPanel;
    GrRet: TcxGrid;
    ViewRet: TcxGridDBTableView;
    ViewRetNUMPOS: TcxGridDBColumn;
    ViewRetARTICUL: TcxGridDBColumn;
    ViewRetBAR: TcxGridDBColumn;
    ViewRetNAME: TcxGridDBColumn;
    ViewRetQUANT: TcxGridDBColumn;
    ViewRetPRICE: TcxGridDBColumn;
    ViewRetDPROC: TcxGridDBColumn;
    ViewRetDSUM: TcxGridDBColumn;
    ViewRetRSUM: TcxGridDBColumn;
    LeRet: TcxGridLevel;
    teRet: TdxMemData;
    dsteRet: TDataSource;
    Label1: TLabel;
    cxCurrencyEdit1: TcxCurrencyEdit;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    teRetCASHNUM: TIntegerField;
    teRetNUMPOS: TIntegerField;
    teRetARTICUL: TStringField;
    teRetRBAR: TStringField;
    teRetNAME: TStringField;
    teRetQUANT: TFloatField;
    teRetPRICE: TFloatField;
    teRetDPROC: TFloatField;
    teRetDSUM: TFloatField;
    teRetRSUM: TFloatField;
    teRetDEPART: TIntegerField;
    teRetEDIZM: TStringField;
    teRetIRET: TSmallintField;
    ViewRetIRET: TcxGridDBColumn;
    amRet: TActionManager;
    acExit: TAction;
    acSel: TAction;
    teRetAMARK: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure acSelExecute(Sender: TObject);
    procedure ViewRetCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure acExitExecute(Sender: TObject);
    procedure ViewRetDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmRetCheck: TfmRetCheck;

implementation

uses Dm, Un1;

{$R *.dfm}

procedure TfmRetCheck.FormCreate(Sender: TObject);
begin
  GrRet.Align:=AlClient;
end;

procedure TfmRetCheck.FormShow(Sender: TObject);
begin
  GrRet.SetFocus;
end;

procedure TfmRetCheck.acSelExecute(Sender: TObject);
Var iRet:INteger;
begin
  if (teRet.Active=True)and(teRet.RecordCount>0) then
  begin
    iRet:=teRetIRET.AsInteger;
    teRet.Edit;
    if iRet=0 then
    begin
      teRetIRET.AsInteger:=1; //�� �������
      cxCurrencyEdit1.Value:=cxCurrencyEdit1.Value+teRetRSUM.AsFloat;
    end else
    begin
      teRetIRET.AsInteger:=0; //������ ��������
      cxCurrencyEdit1.Value:=cxCurrencyEdit1.Value-teRetRSUM.AsFloat;
    end;
    teRet.Post;

    teRet.Next;
  end;
end;

procedure TfmRetCheck.ViewRetCustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);
Var iType,i:Integer;

begin

//  if dmC.quCardFind.Active=False then exit;

  if AViewInfo.GridRecord.Selected then exit;

  iType:=0;
  for i:=0 to ViewRet.ColumnCount-1 do
  begin
    if ViewRet.Columns[i].Name='ViewRetIRET' then
    begin
      if pos('+',AViewInfo.GridRecord.DisplayTexts[i])>0 then iType:=1;
      break;
    end;
  end;

  if iType>0  then
  begin
      ACanvas.Canvas.Brush.Color := clWhite;
      ACanvas.Canvas.Font.Color := clGray;
  end;

end;

procedure TfmRetCheck.acExitExecute(Sender: TObject);
begin
  ModalResult:=mrCancel;
end;

procedure TfmRetCheck.ViewRetDblClick(Sender: TObject);
begin
  if teRet.RecordCount>0 then acSel.Execute;
end;

end.
