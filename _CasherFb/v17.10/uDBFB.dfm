object dmFB: TdmFB
  OldCreateOrder = False
  Left = 813
  Top = 192
  Height = 710
  Width = 357
  object Cash: TpFIBDatabase
    DBName = 'localhost:E:\_Proton\PosFb\cash.gdb'
    DBParams.Strings = (
      'user_name=SYSDBA'
      'lc_ctype=WIN1251'
      'password=masterkey'
      'sql_role_name=SYSDBA')
    DefaultTransaction = trSelectCash
    DefaultUpdateTransaction = trUpdateCash
    SQLDialect = 3
    Timeout = 0
    SynchronizeTime = False
    DesignDBOptions = [ddoIsDefaultDatabase]
    AliasName = 'Cash'
    WaitForRestoreConnect = 20
    Left = 24
    Top = 16
  end
  object trSelectCash: TpFIBTransaction
    DefaultDatabase = Cash
    TimeoutAction = TARollback
    Left = 24
    Top = 72
  end
  object trUpdateCash: TpFIBTransaction
    DefaultDatabase = Cash
    TimeoutAction = TARollback
    Left = 24
    Top = 128
  end
  object prAddCashP: TpFIBStoredProc
    Transaction = trUpdateCash
    Database = Cash
    SQL.Strings = (
      
        'EXECUTE PROCEDURE ADDCASHP (?SHOPINDEX, ?CASHNUMBER, ?ZNUMBER, ?' +
        'CHECKNUMBER, ?CASHER, ?CHECKSUM, ?PAYSUM, ?DSUM, ?DBAR, ?COUNTPO' +
        'S, ?CHDATE, ?PAYMENT)')
    StoredProcName = 'ADDCASHP'
    Left = 96
    Top = 16
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object prAddCashS: TpFIBStoredProc
    Transaction = trUpdateCash
    Database = Cash
    SQL.Strings = (
      
        'EXECUTE PROCEDURE ADDCASHS (?SHOPINDEX, ?CASHNUMBER, ?ZNUMBER, ?' +
        'CHECKNUMBER, ?ID, ?CHDATE, ?ARTICUL, ?BAR, ?CARDSIZE, ?QUANTITY,' +
        ' ?PRICERUB, ?DPROC, ?DSUM, ?TOTALRUB, ?CASHER, ?DEPART, ?OPERATI' +
        'ON, ?SECPOS, ?ECHECK)')
    StoredProcName = 'ADDCASHS'
    Left = 164
    Top = 16
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object prDelCash: TpFIBStoredProc
    Transaction = trDelCash
    Database = Cash
    SQL.Strings = (
      'EXECUTE PROCEDURE DELCASHS (?CASHNUMBER)')
    StoredProcName = 'DELCASHS'
    Left = 232
    Top = 16
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object trDelCash: TpFIBTransaction
    DefaultDatabase = Cash
    TimeoutAction = TARollback
    Left = 24
    Top = 196
  end
  object prDelCashZ: TpFIBStoredProc
    Transaction = trDelCash
    Database = Cash
    SQL.Strings = (
      
        'EXECUTE PROCEDURE DELCASHSZDAY (?CASHNUMBER, ?ZNUM, ?ZDATEB, ?ZD' +
        'ATEE)')
    StoredProcName = 'DELCASHSZDAY'
    Left = 232
    Top = 100
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object quSelLoad: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE CASH01'
      'SET '
      '    IACTIVE = :IACTIVE,'
      '    ITYPE = :ITYPE,'
      '    ARTICUL = :ARTICUL,'
      '    BARCODE = :BARCODE,'
      '    CARDSIZE = :CARDSIZE,'
      '    QUANT = :QUANT,'
      '    DSTOP = :DSTOP,'
      '    NAME = :NAME,'
      '    MESSUR = :MESSUR,'
      '    PRESISION = :PRESISION,'
      '    ADD1 = :ADD1,'
      '    ADD2 = :ADD2,'
      '    ADD3 = :ADD3,'
      '    ADDNUM1 = :ADDNUM1,'
      '    ADDNUM2 = :ADDNUM2,'
      '    ADDNUM3 = :ADDNUM3,'
      '    SCALE = :SCALE,'
      '    GROUP1 = :GROUP1,'
      '    GROUP2 = :GROUP2,'
      '    GROUP3 = :GROUP3,'
      '    PRICE = :PRICE,'
      '    CLIINDEX = :CLIINDEX,'
      '    DELETED = :DELETED,'
      '    PASSW = :PASSW'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    CASH01'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO CASH01('
      '    ID,'
      '    IACTIVE,'
      '    ITYPE,'
      '    ARTICUL,'
      '    BARCODE,'
      '    CARDSIZE,'
      '    QUANT,'
      '    DSTOP,'
      '    NAME,'
      '    MESSUR,'
      '    PRESISION,'
      '    ADD1,'
      '    ADD2,'
      '    ADD3,'
      '    ADDNUM1,'
      '    ADDNUM2,'
      '    ADDNUM3,'
      '    SCALE,'
      '    GROUP1,'
      '    GROUP2,'
      '    GROUP3,'
      '    PRICE,'
      '    CLIINDEX,'
      '    DELETED,'
      '    PASSW'
      ')'
      'VALUES('
      '    :ID,'
      '    :IACTIVE,'
      '    :ITYPE,'
      '    :ARTICUL,'
      '    :BARCODE,'
      '    :CARDSIZE,'
      '    :QUANT,'
      '    :DSTOP,'
      '    :NAME,'
      '    :MESSUR,'
      '    :PRESISION,'
      '    :ADD1,'
      '    :ADD2,'
      '    :ADD3,'
      '    :ADDNUM1,'
      '    :ADDNUM2,'
      '    :ADDNUM3,'
      '    :SCALE,'
      '    :GROUP1,'
      '    :GROUP2,'
      '    :GROUP3,'
      '    :PRICE,'
      '    :CLIINDEX,'
      '    :DELETED,'
      '    :PASSW'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT first 100 ID,IACTIVE,ITYPE,ARTICUL,BARCODE,CARDSIZE,QUANT' +
        ',DSTOP,NAME,MESSUR,PRESISION,'
      
        'ADD1,ADD2,ADD3,ADDNUM1,ADDNUM2,ADDNUM3,SCALE,GROUP1,GROUP2,GROUP' +
        '3,PRICE,CLIINDEX,DELETED,PASSW'
      'FROM CASH01'
      'where(  IACTIVE=2'
      '     ) and (     CASH01.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT first 100 ID,IACTIVE,ITYPE,ARTICUL,BARCODE,CARDSIZE,QUANT' +
        ',DSTOP,NAME,MESSUR,PRESISION,'
      
        'ADD1,ADD2,ADD3,ADDNUM1,ADDNUM2,ADDNUM3,SCALE,GROUP1,GROUP2,GROUP' +
        '3,PRICE,CLIINDEX,DELETED,PASSW'
      'FROM CASH01'
      'where IACTIVE=2')
    Transaction = trSelectCash
    Database = Cash
    UpdateTransaction = trUpdateCash
    AutoCommit = True
    Left = 112
    Top = 88
    poAskRecordCount = True
    object quSelLoadID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quSelLoadIACTIVE: TFIBSmallIntField
      FieldName = 'IACTIVE'
    end
    object quSelLoadITYPE: TFIBSmallIntField
      FieldName = 'ITYPE'
    end
    object quSelLoadARTICUL: TFIBIntegerField
      FieldName = 'ARTICUL'
    end
    object quSelLoadBARCODE: TFIBStringField
      FieldName = 'BARCODE'
      Size = 15
      EmptyStrToNull = True
    end
    object quSelLoadCARDSIZE: TFIBStringField
      FieldName = 'CARDSIZE'
      Size = 10
      EmptyStrToNull = True
    end
    object quSelLoadQUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quSelLoadDSTOP: TFIBFloatField
      FieldName = 'DSTOP'
    end
    object quSelLoadNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 80
      EmptyStrToNull = True
    end
    object quSelLoadMESSUR: TFIBStringField
      FieldName = 'MESSUR'
      Size = 5
      EmptyStrToNull = True
    end
    object quSelLoadPRESISION: TFIBFloatField
      FieldName = 'PRESISION'
    end
    object quSelLoadADD1: TFIBStringField
      FieldName = 'ADD1'
      EmptyStrToNull = True
    end
    object quSelLoadADD2: TFIBStringField
      FieldName = 'ADD2'
      EmptyStrToNull = True
    end
    object quSelLoadADD3: TFIBStringField
      FieldName = 'ADD3'
      EmptyStrToNull = True
    end
    object quSelLoadADDNUM1: TFIBFloatField
      FieldName = 'ADDNUM1'
    end
    object quSelLoadADDNUM2: TFIBFloatField
      FieldName = 'ADDNUM2'
    end
    object quSelLoadADDNUM3: TFIBFloatField
      FieldName = 'ADDNUM3'
    end
    object quSelLoadSCALE: TFIBStringField
      FieldName = 'SCALE'
      Size = 10
      EmptyStrToNull = True
    end
    object quSelLoadGROUP1: TFIBIntegerField
      FieldName = 'GROUP1'
    end
    object quSelLoadGROUP2: TFIBIntegerField
      FieldName = 'GROUP2'
    end
    object quSelLoadGROUP3: TFIBIntegerField
      FieldName = 'GROUP3'
    end
    object quSelLoadPRICE: TFIBFloatField
      FieldName = 'PRICE'
    end
    object quSelLoadCLIINDEX: TFIBIntegerField
      FieldName = 'CLIINDEX'
    end
    object quSelLoadDELETED: TFIBSmallIntField
      FieldName = 'DELETED'
    end
    object quSelLoadPASSW: TFIBStringField
      FieldName = 'PASSW'
      EmptyStrToNull = True
    end
  end
  object quSetActive: TpFIBQuery
    Transaction = trSelectCash
    Database = Cash
    SQL.Strings = (
      'update cash01 set IACTIVE=2'
      'where IACTIVE=1')
    Left = 232
    Top = 156
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object quUpd2: TpFIBQuery
    Transaction = trUpd2
    Database = Cash
    SQL.Strings = (
      '')
    Left = 96
    Top = 196
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object trUpd2: TpFIBTransaction
    DefaultDatabase = Cash
    TimeoutAction = TARollback
    Left = 152
    Top = 196
  end
  object quTabDBList2: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select R.RDB$RELATION_NAME, R.RDB$FIELD_POSITION, R.RDB$FIELD_NA' +
        'ME'
      'from RDB$RELATION_FIELDS R'
      'where R.RDB$SYSTEM_FLAG = 0'
      'and R.RDB$RELATION_NAME = :STABNAME')
    Transaction = trSelectCash
    Database = Cash
    UpdateTransaction = trUpdateCash
    AutoCommit = True
    Left = 44
    Top = 264
    poAskRecordCount = True
    object quTabDBList2RDBRELATION_NAME: TFIBWideStringField
      FieldName = 'RDB$RELATION_NAME'
      Size = 31
    end
    object quTabDBList2RDBFIELD_POSITION: TFIBSmallIntField
      FieldName = 'RDB$FIELD_POSITION'
    end
    object quTabDBList2RDBFIELD_NAME: TFIBWideStringField
      FieldName = 'RDB$FIELD_NAME'
      Size = 31
    end
  end
  object quZListDet2: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE ZLISTDET'
      'SET '
      '    RSUM = :RSUM,'
      '    IDATE = :IDATE,'
      '    DDATE = :DDATE'
      'WHERE'
      '    CASHNUM = :OLD_CASHNUM'
      '    and ZNUM = :OLD_ZNUM'
      '    and INOUT = :OLD_INOUT'
      '    and ITYPE = :OLD_ITYPE'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    ZLISTDET'
      'WHERE'
      '        CASHNUM = :OLD_CASHNUM'
      '    and ZNUM = :OLD_ZNUM'
      '    and INOUT = :OLD_INOUT'
      '    and ITYPE = :OLD_ITYPE'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO ZLISTDET('
      '    CASHNUM,'
      '    ZNUM,'
      '    INOUT,'
      '    ITYPE,'
      '    RSUM,'
      '    IDATE,'
      '    DDATE'
      ')'
      'VALUES('
      '    :CASHNUM,'
      '    :ZNUM,'
      '    :INOUT,'
      '    :ITYPE,'
      '    :RSUM,'
      '    :IDATE,'
      '    :DDATE'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    CASHNUM,'
      '    ZNUM,'
      '    INOUT,'
      '    ITYPE,'
      '    RSUM,'
      '    IDATE,'
      '    DDATE'
      'FROM'
      '    ZLISTDET '
      'where(  CASHNUM=:CNUM'
      'and ZNUM=:ZNUM'
      '     ) and (     ZLISTDET.CASHNUM = :OLD_CASHNUM'
      '    and ZLISTDET.ZNUM = :OLD_ZNUM'
      '    and ZLISTDET.INOUT = :OLD_INOUT'
      '    and ZLISTDET.ITYPE = :OLD_ITYPE'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    CASHNUM,'
      '    ZNUM,'
      '    INOUT,'
      '    ITYPE,'
      '    RSUM,'
      '    IDATE,'
      '    DDATE'
      'FROM'
      '    ZLISTDET '
      'where CASHNUM=:CNUM'
      'and ZNUM=:ZNUM')
    Transaction = trSelectCash
    Database = Cash
    AutoCommit = True
    Left = 124
    Top = 264
    poAskRecordCount = True
    object quZListDet2CASHNUM: TFIBSmallIntField
      FieldName = 'CASHNUM'
    end
    object quZListDet2ZNUM: TFIBIntegerField
      FieldName = 'ZNUM'
    end
    object quZListDet2INOUT: TFIBSmallIntField
      FieldName = 'INOUT'
    end
    object quZListDet2ITYPE: TFIBSmallIntField
      FieldName = 'ITYPE'
    end
    object quZListDet2RSUM: TFIBFloatField
      FieldName = 'RSUM'
    end
    object quZListDet2IDATE: TFIBIntegerField
      FieldName = 'IDATE'
    end
    object quZListDet2DDATE: TFIBDateTimeField
      FieldName = 'DDATE'
    end
  end
  object quAUpd: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE CASH01'
      'SET '
      '    IACTIVE = :IACTIVE,'
      '    ITYPE = :ITYPE,'
      '    ARTICUL = :ARTICUL,'
      '    BARCODE = :BARCODE,'
      '    CARDSIZE = :CARDSIZE,'
      '    QUANT = :QUANT,'
      '    DSTOP = :DSTOP,'
      '    NAME = :NAME,'
      '    MESSUR = :MESSUR,'
      '    PRESISION = :PRESISION,'
      '    ADD1 = :ADD1,'
      '    ADD2 = :ADD2,'
      '    ADD3 = :ADD3,'
      '    ADDNUM1 = :ADDNUM1,'
      '    ADDNUM2 = :ADDNUM2,'
      '    ADDNUM3 = :ADDNUM3,'
      '    SCALE = :SCALE,'
      '    GROUP1 = :GROUP1,'
      '    GROUP2 = :GROUP2,'
      '    GROUP3 = :GROUP3,'
      '    PRICE = :PRICE,'
      '    CLIINDEX = :CLIINDEX,'
      '    DELETED = :DELETED,'
      '    PASSW = :PASSW'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    CASH01'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO CASH01('
      '    ID,'
      '    IACTIVE,'
      '    ITYPE,'
      '    ARTICUL,'
      '    BARCODE,'
      '    CARDSIZE,'
      '    QUANT,'
      '    DSTOP,'
      '    NAME,'
      '    MESSUR,'
      '    PRESISION,'
      '    ADD1,'
      '    ADD2,'
      '    ADD3,'
      '    ADDNUM1,'
      '    ADDNUM2,'
      '    ADDNUM3,'
      '    SCALE,'
      '    GROUP1,'
      '    GROUP2,'
      '    GROUP3,'
      '    PRICE,'
      '    CLIINDEX,'
      '    DELETED,'
      '    PASSW'
      ')'
      'VALUES('
      '    :ID,'
      '    :IACTIVE,'
      '    :ITYPE,'
      '    :ARTICUL,'
      '    :BARCODE,'
      '    :CARDSIZE,'
      '    :QUANT,'
      '    :DSTOP,'
      '    :NAME,'
      '    :MESSUR,'
      '    :PRESISION,'
      '    :ADD1,'
      '    :ADD2,'
      '    :ADD3,'
      '    :ADDNUM1,'
      '    :ADDNUM2,'
      '    :ADDNUM3,'
      '    :SCALE,'
      '    :GROUP1,'
      '    :GROUP2,'
      '    :GROUP3,'
      '    :PRICE,'
      '    :CLIINDEX,'
      '    :DELETED,'
      '    :PASSW'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT first 100 ID,IACTIVE,ITYPE,ARTICUL,BARCODE,CARDSIZE,QUANT' +
        ',DSTOP,NAME,MESSUR,PRESISION,'
      
        'ADD1,ADD2,ADD3,ADDNUM1,ADDNUM2,ADDNUM3,SCALE,GROUP1,GROUP2,GROUP' +
        '3,PRICE,CLIINDEX,DELETED,PASSW'
      'FROM CASH01'
      'where(  IACTIVE=2'
      '     ) and (     CASH01.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT first 1 ID,'
      '       SVER,'
      '       TIME_VER,'
      '       CASH_BIN,'
      '       UPD_TYPE,'
      '       ILOADER,'
      '       LOADER'
      'FROM CASH_VERS'
      'order by ID desc')
    Transaction = trSelectCash
    Database = Cash
    UpdateTransaction = trUpdateCash
    AutoCommit = True
    Left = 36
    Top = 332
    poAskRecordCount = True
    object quAUpdID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quAUpdSVER: TFIBStringField
      FieldName = 'SVER'
      Size = 10
      EmptyStrToNull = True
    end
    object quAUpdTIME_VER: TFIBDateTimeField
      FieldName = 'TIME_VER'
    end
    object quAUpdCASH_BIN: TFIBBlobField
      FieldName = 'CASH_BIN'
      Size = 8
    end
    object quAUpdUPD_TYPE: TFIBSmallIntField
      FieldName = 'UPD_TYPE'
    end
    object quAUpdILOADER: TFIBSmallIntField
      FieldName = 'ILOADER'
    end
    object quAUpdLOADER: TFIBBlobField
      FieldName = 'LOADER'
      Size = 8
    end
  end
  object quXCheck: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE XCHECK'
      'SET '
      '    ICODE = :ICODE,'
      '    QUANT = :QUANT,'
      '    PRICE = :PRICE,'
      '    RSUM = :RSUM,'
      '    XDATE = :XDATE,'
      '    BARCODE = :BARCODE'
      'WHERE'
      '    BARCH = :OLD_BARCH'
      '    and INUM = :OLD_INUM'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    XCHECK'
      'WHERE'
      '        BARCH = :OLD_BARCH'
      '    and INUM = :OLD_INUM'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO XCHECK('
      '    BARCH,'
      '    INUM,'
      '    ICODE,'
      '    QUANT,'
      '    PRICE,'
      '    RSUM,'
      '    XDATE,'
      '    BARCODE'
      ')'
      'VALUES('
      '    :BARCH,'
      '    :INUM,'
      '    :ICODE,'
      '    :QUANT,'
      '    :PRICE,'
      '    :RSUM,'
      '    :XDATE,'
      '    :BARCODE'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    BARCH,'
      '    INUM,'
      '    ICODE,'
      '    QUANT,'
      '    PRICE,'
      '    RSUM,'
      '    XDATE,'
      '    BARCODE'
      'FROM'
      '    XCHECK '
      'where(  BARCH=:SBAR'
      '     ) and (     XCHECK.BARCH = :OLD_BARCH'
      '    and XCHECK.INUM = :OLD_INUM'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    BARCH,'
      '    INUM,'
      '    ICODE,'
      '    QUANT,'
      '    PRICE,'
      '    RSUM,'
      '    XDATE,'
      '    BARCODE'
      'FROM'
      '    XCHECK '
      'where BARCH=:SBAR'
      'order by INUM')
    Transaction = trSelectCash
    Database = Cash
    UpdateTransaction = trUpdateCash
    AutoCommit = True
    Left = 198
    Top = 262
    poAskRecordCount = True
    object quXCheckBARCH: TFIBStringField
      FieldName = 'BARCH'
      Size = 13
      EmptyStrToNull = True
    end
    object quXCheckINUM: TFIBIntegerField
      FieldName = 'INUM'
    end
    object quXCheckICODE: TFIBIntegerField
      FieldName = 'ICODE'
    end
    object quXCheckQUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quXCheckPRICE: TFIBFloatField
      FieldName = 'PRICE'
    end
    object quXCheckRSUM: TFIBFloatField
      FieldName = 'RSUM'
    end
    object quXCheckXDATE: TFIBDateTimeField
      FieldName = 'XDATE'
    end
    object quXCheckBARCODE: TFIBStringField
      FieldName = 'BARCODE'
      Size = 15
      EmptyStrToNull = True
    end
  end
end
