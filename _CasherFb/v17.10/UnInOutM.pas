unit UnInOutM;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ActnList, XPStyleActnCtrls, ActnMan, StdCtrls,
  dxfLabel, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, DBClient, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxContainer, cxTextEdit, cxMaskEdit,
  cxDropDownEdit, cxCalc, cxDBEdit, dxfQuickTyp, cxCurrencyEdit,
  cxLookAndFeelPainters, cxButtons, Menus, cxDataStorage, cxSpinEdit,
  dxfBackGround;

type
  TfmInOutM = class(TForm)
    Panel1: TPanel;
    acInOut: TActionManager;
    acClose: TAction;
    Panel2: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    ViewMoney: TcxGridDBTableView;
    Level: TcxGridLevel;
    Grid: TcxGrid;
    taInOutM: TClientDataSet;
    taInOutMIVal: TIntegerField;
    taInOutMiQuant: TIntegerField;
    dsInOutM: TDataSource;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    Label4: TLabel;
    Label5: TLabel;
    taInOutMrVal: TCurrencyField;
    ViewMoneyIVal: TcxGridDBColumn;
    ViewMoneyiQuant: TcxGridDBColumn;
    ViewMoneyrVal: TcxGridDBColumn;
    CurrencyEdit1: TcxCurrencyEdit;
    Button1: TcxButton;
    Button2: TcxButton;
    dxfBackGround1: TdxfBackGround;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    procedure acCloseExecute(Sender: TObject);
    procedure ViewMoneyEditChanged(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem);
    procedure ViewDataControllerDataChanged(Sender: TObject);
    procedure ViewMoneyKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CurrencyEdit1KeyPress(Sender: TObject; var Key: Char);
    procedure Button2Click(Sender: TObject);
    procedure CurrencyEdit1Enter(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure taInOutMAfterPost(DataSet: TDataSet);
    procedure CurrencyEdit1PropertiesChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmInOutM: TfmInOutM;
  iSumInOut:Int64;
  iLast:INteger;

implementation

uses Un1;

{$R *.dfm}

procedure TfmInOutM.acCloseExecute(Sender: TObject);
begin
  Grid.SetFocus;
end;

procedure TfmInOutM.ViewMoneyEditChanged(Sender: TcxCustomGridTableView;
 AItem: TcxCustomGridTableItem);
begin
//  iSumInOut:=iSumInOut+taInOutMIVal.AsInteger*taInOutMiQuant.AsInteger;
//  Label5.Caption:=IntToStr(iSumInOut);
end;

procedure TfmInOutM.ViewDataControllerDataChanged(Sender: TObject);
begin
//
end;

procedure TfmInOutM.ViewMoneyKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
Var bCh:Byte;
begin
  bCh:=ord(Key);
//  if (bCh = 39)or(bCh = 37) then
  if bCh = 13 then
  begin
    if taInOutM.State in [dsInsert,dsEdit] then taInOutM.Post; 

    if taInOutMiVal.AsInteger=iLast then
    begin
      CurrencyEdit1.SetFocus;
      exit;
    end;

    try
      taInOutM.Next;
    except
      CurrencyEdit1.SetFocus;
    end;
  end;
end;

procedure TfmInOutM.CurrencyEdit1KeyPress(Sender: TObject; var Key: Char);
Var bCh:Byte;
begin
  if Key='�' then Key:=',';
  if Key='.' then Key:=',';
  bCh:=ord(Key);
  if bCh = 13 then
  begin
    Button1.SetFocus;
  end;
end;

procedure TfmInOutM.Button2Click(Sender: TObject);
begin
  close;
end;

procedure TfmInOutM.CurrencyEdit1Enter(Sender: TObject);
begin
//  Button1.SetFocus;
end;

procedure TfmInOutM.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  bMoneyCalc:=False;
end;

procedure TfmInOutM.taInOutMAfterPost(DataSet: TDataSet);
Var iCur:INteger;
    StrWk:String;
begin
  if bMoneyCalc then
  begin
    ViewMoney.BeginUpdate;
    iCur:=taInOutMIVal.AsInteger;

    iSumInOut:=0;

    //��� ����� �� ���� ������� � +
    taInOutM.First;
    while not taInOutM.Eof do
    begin
      iSumInOut:=iSumInOut+taInOutMIVal.AsInteger*taInOutMiQuant.AsInteger;
      taInOutM.Next;
    end;

    iSumInOut:=iSumInOut+RoundEx(CurrencyEdit1.EditValue*100);

    //��������� ��������� �� ������� ������ ���������
    taInOutM.First;
    while not taInOutM.Eof do
    begin
      if taInOutMIVal.AsInteger=iCur then Break;
      taInOutM.Next;
    end;

    Str((iSumInOut/100):12:2,StrWk);

    ViewMoney.EndUpdate;

    Label5.Caption:=StrWk;
  end;
end;

procedure TfmInOutM.CurrencyEdit1PropertiesChange(Sender: TObject);
Var StrWk:String;
begin
  if bMoneyCalc then
  begin
    ViewMoney.BeginUpdate;

    iSumInOut:=0;

    //��� ����� �� ���� ������� � +
    taInOutM.First;
    while not taInOutM.Eof do
    begin
      iSumInOut:=iSumInOut+taInOutMIVal.AsInteger*taInOutMiQuant.AsInteger;
      taInOutM.Next;
    end;

    iSumInOut:=iSumInOut+RoundEx(CurrencyEdit1.EditValue*100);

    Str((iSumInOut/100):12:2,StrWk);

    ViewMoney.EndUpdate;

    Label5.Caption:=StrWk;
  end;
end;

procedure TfmInOutM.FormShow(Sender: TObject);
begin
  Grid.SetFocus;
  Label5.Caption:='0.00';
  CurrencyEdit1.EditValue:=0;
end;

end.
