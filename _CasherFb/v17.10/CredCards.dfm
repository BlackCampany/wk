object fmCredCards: TfmCredCards
  Left = 375
  Top = 221
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = #1058#1080#1087' '#1073#1077#1079#1085#1072#1083#1080#1095#1085#1086#1081' '#1086#1087#1083#1072#1090#1099
  ClientHeight = 343
  ClientWidth = 310
  Color = 8080936
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label10: TLabel
    Left = 19
    Top = 252
    Width = 114
    Height = 13
    Caption = #1057#1091#1084#1084#1072' '#1082' '#1086#1087#1083#1072#1090#1077' '#1087#1086' '#1041#1053
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 15456197
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object Panel1: TPanel
    Left = 0
    Top = 288
    Width = 310
    Height = 55
    Align = alBottom
    BevelInner = bvLowered
    Color = 8080936
    TabOrder = 2
    object Button1: TcxButton
      Left = 48
      Top = 16
      Width = 75
      Height = 25
      Caption = 'Ok'
      Default = True
      ModalResult = 1
      TabOrder = 0
      Colors.Default = 14864576
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfFlat
    end
    object Button2: TcxButton
      Left = 200
      Top = 16
      Width = 75
      Height = 25
      Caption = #1054#1090#1084#1077#1085#1072
      TabOrder = 1
      OnClick = Button2Click
      Colors.Default = 14864576
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfFlat
    end
  end
  object GridBN: TcxGrid
    Left = 0
    Top = 0
    Width = 305
    Height = 233
    TabOrder = 0
    LookAndFeel.Kind = lfFlat
    object ViewBN: TcxGridDBTableView
      OnKeyDown = ViewBNKeyDown
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dmC.dsCredCard
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnGrouping = False
      OptionsCustomize.ColumnMoving = False
      OptionsCustomize.ColumnSorting = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.InvertSelect = False
      OptionsView.GroupByBox = False
      object ViewBNNAME: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'NAME'
        Styles.Content = dmC.cxStyle2
        Width = 270
      end
    end
    object LevelBN: TcxGridLevel
      GridView = ViewBN
    end
  end
  object CurrencyEdit1: TcxCurrencyEdit
    Left = 176
    Top = 248
    TabStop = False
    Properties.ReadOnly = True
    TabOrder = 1
    Width = 121
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 310
    Height = 241
    Align = alTop
    BevelInner = bvLowered
    Color = 8080936
    TabOrder = 3
    Visible = False
    object Label1: TLabel
      Left = 19
      Top = 20
      Width = 269
      Height = 13
      Caption = #1055#1088#1086#1074#1077#1076#1080#1090#1077' '#1073#1072#1085#1082#1086#1074#1089#1082#1091#1102' '#1082#1072#1088#1090#1091' '#1095#1077#1088#1077#1079' '#1089#1095#1080#1090#1099#1074#1072#1090#1077#1083#1100' '#1052#1050
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 15456197
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Label2: TLabel
      Left = 19
      Top = 52
      Width = 30
      Height = 13
      Caption = #1050#1072#1088#1090#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 15456197
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
      Visible = False
    end
    object Label3: TLabel
      Left = 19
      Top = 76
      Width = 25
      Height = 13
      Caption = #1057#1088#1086#1082
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 15456197
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
      Visible = False
    end
    object Label4: TLabel
      Left = 19
      Top = 108
      Width = 127
      Height = 13
      Caption = #1054#1078#1080#1076#1072#1077#1084' '#1086#1090#1074#1077#1090' '#1089#1077#1088#1074#1077#1088#1072' '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 15456197
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
      Visible = False
    end
    object Label5: TLabel
      Left = 171
      Top = 108
      Width = 6
      Height = 13
      Caption = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 15456197
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
      Visible = False
    end
    object Label6: TLabel
      Left = 19
      Top = 148
      Width = 270
      Height = 24
      Alignment = taCenter
      AutoSize = False
      Caption = #1059#1089#1087#1077#1093
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 15456197
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
      Visible = False
    end
  end
  object Edit1: TEdit
    Left = 48
    Top = 200
    Width = 0
    Height = 21
    TabOrder = 4
    Text = 'Edit1'
    OnKeyPress = Edit1KeyPress
  end
  object acCredCards: TActionManager
    Left = 248
    Top = 48
    StyleName = 'XP Style'
    object acReadCC: TAction
      Caption = 'acReadCC'
      ShortCut = 122
      OnExecute = acReadCCExecute
    end
  end
  object Timer1: TTimer
    Enabled = False
    OnTimer = Timer1Timer
    Left = 232
    Top = 128
  end
end
