unit St;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ComCtrls, cxControls, cxContainer, cxEdit, cxTextEdit,
  cxMemo, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons;

type
  TfmSt = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Memo1: TcxMemo;
    cxButton1: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmSt: TfmSt;

implementation

{$R *.dfm}

procedure TfmSt.FormCreate(Sender: TObject);
begin
  Memo1.Align:=AlClient;
  Memo1.Clear;
end;

procedure TfmSt.cxButton1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmSt.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if cxButton1.Enabled=False then Action:=caNone else Action:=caHide; 
end;

end.
