unit AddVer;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, cxGraphics, cxDropDownEdit, cxMaskEdit, cxButtonEdit,
  cxControls, cxContainer, cxEdit, cxTextEdit, StdCtrls, Menus,
  cxLookAndFeelPainters, cxButtons;

type
  TfmAddVer = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    cxTextEdit1: TcxTextEdit;
    Label2: TLabel;
    cxButtonEdit1: TcxButtonEdit;
    Label3: TLabel;
    cxComboBox1: TcxComboBox;
    Label4: TLabel;
    cxComboBox2: TcxComboBox;
    Label5: TLabel;
    cxButtonEdit2: TcxButtonEdit;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    OpenDialog1: TOpenDialog;
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxButtonEdit2PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAddVer: TfmAddVer;

implementation

{$R *.dfm}

procedure TfmAddVer.cxButtonEdit1PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  if OpenDialog1.Execute then cxButtonEdit1.Text:=OpenDialog1.FileName;
end;

procedure TfmAddVer.cxButtonEdit2PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  if OpenDialog1.Execute then cxButtonEdit2.Text:=OpenDialog1.FileName;
end;

end.
