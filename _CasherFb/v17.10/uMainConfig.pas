unit uMainConfig;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, ComCtrls, ExtCtrls, StdCtrls, Grids, DBGrids, DBCtrls,
  Buttons, RXDBCtrl, Placemnt, cxLookAndFeelPainters, cxButtons, ActnList,
  XPStyleActnCtrls, ActnMan, SpeedBar, cxStyles, cxCustomData, cxGraphics,
  cxFilter, cxData, cxEdit, DB, cxDBData, cxGridLevel, cxClasses,
  cxControls, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxDataStorage, cxCurrencyEdit;

type
  TfmMainConfig = class(TForm)
    StatusBar1: TStatusBar;
    MainMenu1: TMainMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    GroupBox1: TGroupBox;
    Panel2: TPanel;
    Tree1: TTreeView;
    Panel3: TPanel;
    Memo1: TRichEdit;
    Panel4: TPanel;
    TreeClassif: TTreeView;
    Panel5: TPanel;
    Panel6: TPanel;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    SpeedBar1: TSpeedBar;
    AM1: TActionManager;
    acAdd: TAction;
    acEdit: TAction;
    acDel: TAction;
    FormPlacement1: TFormPlacement;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    View1: TcxGridDBTableView;
    Level1: TcxGridLevel;
    Grid1: TcxGrid;
    View1KEY_CODE: TcxGridDBColumn;
    View1KEY_CHAR: TcxGridDBColumn;
    View1BARCODE: TcxGridDBColumn;
    View1KEY_POSITION: TcxGridDBColumn;
    View1NAME: TcxGridDBColumn;
    View1NAME1: TcxGridDBColumn;
    ViewCl: TcxGridDBTableView;
    Level2: TcxGridLevel;
    GridCl: TcxGrid;
    ViewClKEY_CODE: TcxGridDBColumn;
    ViewClKEY_CHAR: TcxGridDBColumn;
    ViewClNAMECL: TcxGridDBColumn;
    ViewGoods: TcxGridDBTableView;
    Level3: TcxGridLevel;
    GridGoods: TcxGrid;
    ViewGoodsARTICUL: TcxGridDBColumn;
    ViewGoodsNAME: TcxGridDBColumn;
    ViewGoodsMESURIMENT: TcxGridDBColumn;
    ViewGoodsPRICE_RUB: TcxGridDBColumn;
    ViewGoodsDISCOUNT: TcxGridDBColumn;
    ViewGoodsBARCODE: TcxGridDBColumn;
    ViewGoodsPRICEBAR: TcxGridDBColumn;
    ViewGoodsNAMECL: TcxGridDBColumn;
    GridCFGoods: TcxGrid;
    ViewCFGoods: TcxGridDBTableView;
    cxGridDBColumn1: TcxGridDBColumn;
    cxGridDBColumn2: TcxGridDBColumn;
    cxGridDBColumn3: TcxGridDBColumn;
    Level4: TcxGridLevel;
    ViewCFGoodsDBColumn1: TcxGridDBColumn;
    ViewDep: TcxGridDBTableView;
    Level5: TcxGridLevel;
    GridDep: TcxGrid;
    ViewCFDep: TcxGridDBTableView;
    Level6: TcxGridLevel;
    GridCFDep: TcxGrid;
    ViewDepID: TcxGridDBColumn;
    ViewDepNAME: TcxGridDBColumn;
    ViewCFDepKEY_CODE: TcxGridDBColumn;
    ViewCFDepKEY_CHAR: TcxGridDBColumn;
    ViewCFDepID_DEPARTS: TcxGridDBColumn;
    ViewCFDepNAMEDEP: TcxGridDBColumn;
    procedure N3Click(Sender: TObject);
    procedure Tree1Change(Sender: TObject; Node: TTreeNode);
    procedure FormCreate(Sender: TObject);
    procedure N5Click(Sender: TObject);
    procedure N6Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure acAddExecute(Sender: TObject);
    procedure acEditExecute(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure SpeedItem4Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure TreeClassifExpanding(Sender: TObject; Node: TTreeNode;
      var AllowExpansion: Boolean);
  private
    { Private declarations }
  public
    Procedure WrMess(S:String);
    { Public declarations }
  end;

var
  fmMainConfig: TfmMainConfig;

procedure pAddButton;
procedure pAddButtonGroup;
procedure pDelButton;
procedure pEditButton;
procedure pEditGroupButton;
procedure pAddButtonCard;
procedure pEditCardButton;
procedure pAddButtonDepart;
procedure pEditDepartButton;
Function prFindKey(iC:Integer; Var StrMess:String):Boolean;


implementation

uses Dm,uAddOper, uPressKey, uSaveLoad, Passw, Un1;

{$R *.dfm}

Function prFindKey(iC:Integer; Var StrMess:String):Boolean;
begin
  result:=False;
  with dmC do
  begin
    if taCF_All.locate('KEY_CODE',iC,[]) then
    begin
      Result:=True;
      case taCF_AllKEY_STATUS.AsInteger of
        0:begin //��������
            taOperations.Locate('ID',taCF_AllID_OPERATIONS.AsInteger,[]);
            StrMess:=' �������� - '+taOperationsName.AsString;
          end;
        1:begin //������ �������
            taClassif.Locate('ID',taCF_AllID_CLASSIF.AsInteger,[]);
            StrMess:=' ������ �������������� ������� - '+taClassifName.AsString;
          end;
        2:begin //������
            StrMess:=' ����� - ���:'+taCF_AllARTICUL.AsString;
          end;
        3:begin //������
            taDeparts.Locate('ID',taCF_AllID_DEPARTS.AsInteger,[]);
            StrMess:=' ����� - '+taDepartsName.AsString;
          end;
        else
          StrMess:='...';
        end;
      StrMess:='������ ��������� ������ ��� ����������.'+StrMess;
    end;
  end;
end;

Procedure TfmMainConfig.WrMess(S:String);
Var StrWk:String;
begin
  if Memo1.Lines.Count > 500 then Memo1.Clear;
//  WriteHistory(S);
  if S>'' then StrWk:=FormatDateTime('dd.mm hh:nn:ss:zzz',now)+'  '+S
  else StrWk:=S;
  Memo1.Lines.Add(StrWk);
end;

procedure pEditDepartButton;
var iCode,MemCode:Integer;
begin
  //��������� ������
  CommonSet.Key_Add:=False;
  with dmC do
  begin
    try
      CommonSEt.Key_Char:=taCF_OperationsKEY_CHAR.AsString;
      if taCF_OperationsKEY_CODE.AsInteger<1000 then
      begin
        CommonSet.Key_Code:=taCF_OperationsKEY_CODE.AsInteger;
        CommonSet.Key_Shift:=0;   //������
      end
      else
      begin
        if taCF_OperationsKEY_CODE.AsInteger<2000 then
        begin
          CommonSet.Key_Code:=taCF_OperationsKEY_CODE.AsInteger-1000;
          CommonSet.Key_Shift:=1; //shift
        end
        else
        begin
          CommonSet.Key_Code:=taCF_OperationsKEY_CODE.AsInteger-2000;
          CommonSet.Key_Shift:=2; //ctrl
        end;
      end;

      fmPressKey.Edit1.Text:=taCF_OperationsKEY_CHAR.AsString;
      fmPressKey.Edit2.Text:=IntToStr(taCF_OperationsKEY_CODE.AsInteger);
      fmPressKey.Label1.Caption:='������� ������ �������.';

      memCode:=CommonSet.Key_Code+1000*(CommonSet.Key_Shift);
      vSet:=False;
      fmPressKey.ShowModal;
      if vSet then  //���-�� �������
      begin
        iCode:=CommonSet.Key_Code+1000*(CommonSet.Key_Shift);
        if (iCode<>memCode) and (iCode<>0) then
        begin //���-�� ����������
            //���� , ����� ��� ����.
          if prFindKey(iCode,StrWk) then  ShowMessage(Strwk)
          else //������ ��������� ��� ��� - ����� ��������������
          begin
            iCode:=taCF_OperationsID_DEPARTS.AsInteger;
            trUpdate.StartTransaction;

            taCF_Operations.Edit;
            taCF_OperationsKey_CODE.AsInteger:=CommonSet.Key_Code+1000*(CommonSet.Key_Shift);
            taCF_OperationsKey_Status.AsInteger:=3; //�������
            taCF_OperationsKey_Char.AsString:=CommonSet.Key_Char;
            taCF_Operations.Post;

            trUpdate.Commit;

            taCF_Operations.FullRefresh;
            taCF_Operations.Locate('ID_DEPARTS',iCode,[]);
          end;
        end;
      end;
    finally
    end;
  end;
end;


procedure pAddButtonDepart;
var iCode:Integer;
begin
  with dmC do
  begin
    CommonSet.Key_Char:='';
    CommonSet.Key_Code:=0;
    CommonSet.Key_Shift:=0;
    CommonSet.Key_Add:=True;

    fmPressKey.Edit1.Text:='';
    fmPressKey.Edit2.Text:='';
    fmPressKey.Label1.Caption:='������� ������ �������.';

    vSet:=False;
    fmPressKey.ShowModal;
    if vSet then  //���-�� �������
    begin
       if CommonSet.Key_Code>0 then
       begin
         iCode:=CommonSet.Key_Code+1000*(CommonSet.Key_Shift);
         //���� , ����� ��� ����.
         if prFindKey(iCode,StrWk) then showmessage(StrWk)
         else //������ ��������� ��� ��� - ���������
         begin
           trUpdate.StartTransaction;

           taCF_Operations.Append;
           taCF_OperationsKey_CODE.AsInteger:=CommonSet.Key_Code+1000*(CommonSet.Key_Shift);
           taCF_OperationsKey_Status.AsInteger:=3; //������
           taCF_OperationsKey_Char.AsString:=CommonSet.Key_Char;
           taCF_OperationsID_CLASSIF.AsInteger:=-1;
           taCF_OperationsID_Operations.AsInteger:=-1;
           taCF_OperationsID_States.AsInteger:=-1;
           taCF_OperationsBARCODE.AsString:='-1';
           taCF_OperationsKEY_POSITION.AsInteger:=-1;
           taCF_OperationsBAR.AsString:='';
           taCF_OperationsARTICUL.AsString:='';
           taCF_OperationsID_DEPARTS.AsInteger:=taDepartsID.AsInteger;
           taCF_Operations.Post;

           trUpdate.Commit;

           taCF_Operations.FullRefresh;
           taCF_Operations.Locate('ID_DEPARTS',taDepartsID.AsInteger,[]);
        end;
      end;
    end;
  end;
end;


procedure pEditCardButton;
var iCode,MemCode:Integer;
begin
  //��������� ������
  CommonSet.Key_Add:=False;
  with dmC do
  begin
    try
      CommonSEt.Key_Char:=taCF_OperationsKEY_CHAR.AsString;
      if taCF_OperationsKEY_CODE.AsInteger<1000 then
      begin
        CommonSet.Key_Code:=taCF_OperationsKEY_CODE.AsInteger;
        CommonSet.Key_Shift:=0;   //������
      end
      else
      begin
        if taCF_OperationsKEY_CODE.AsInteger<2000 then
        begin
          CommonSet.Key_Code:=taCF_OperationsKEY_CODE.AsInteger-1000;
          CommonSet.Key_Shift:=1; //shift
        end
        else
        begin
          CommonSet.Key_Code:=taCF_OperationsKEY_CODE.AsInteger-2000;
          CommonSet.Key_Shift:=2; //ctrl
        end;
      end;

      fmPressKey.Edit1.Text:=taCF_OperationsKEY_CHAR.AsString;
      fmPressKey.Edit2.Text:=IntToStr(taCF_OperationsKEY_CODE.AsInteger);
      fmPressKey.Label1.Caption:='������� ������ �������.';

      memCode:=CommonSet.Key_Code+1000*(CommonSet.Key_Shift);
      vSet:=False;
      fmPressKey.ShowModal;
      if vSet then  //���-�� �������
      begin
        iCode:=CommonSet.Key_Code+1000*(CommonSet.Key_Shift);
        if (iCode<>memCode) and (iCode<>0) then
        begin //���-�� ����������
            //���� , ����� ��� ����.
          if prFindKey(iCode,StrWk) then ShowMessage(Strwk)
          else //������ ��������� ��� ��� - ����� ����������
          begin
            trUpdate.StartTransaction;

            taCF_Operations.Edit;
            taCF_OperationsKey_CODE.AsInteger:=CommonSet.Key_Code+1000*(CommonSet.Key_Shift);
            taCF_OperationsKey_Status.AsInteger:=2; //������
            taCF_OperationsKey_Char.AsString:=CommonSet.Key_Char;
            taCF_Operations.Post;

            trUpdate.Commit;

            taCF_Operations.FullRefresh;
          end;
        end;
      end;
    finally
    end;
  end;
end;


procedure pAddButtonCard;
var iCode:Integer;
begin
  with dmC do
  begin
  CommonSet.Key_Char:='';
  CommonSet.Key_Code:=0;
  CommonSet.Key_Shift:=0;
  CommonSet.Key_Add:=True;

  fmPressKey.Edit1.Text:='';
  fmPressKey.Edit2.Text:='';
  fmPressKey.Label1.Caption:='������� ������ �������.';

  vSet:=False;
  fmPressKey.ShowModal;
  if vSet then  //���-�� �������
  begin
    if CommonSet.Key_Code>0 then
    begin
      iCode:=CommonSet.Key_Code+1000*(CommonSet.Key_Shift);
      //���� , ����� ��� ����.
      if prFindKey(iCode,StrWk) then ShowMessage(Strwk)
      else //������ ��������� ��� ��� - ���������
      begin
        trUpdate.StartTransaction;

        taCF_Operations.Append;
        taCF_OperationsKey_CODE.AsInteger:=CommonSet.Key_Code+1000*(CommonSet.Key_Shift);
        taCF_OperationsKey_Status.AsInteger:=2; //������
        taCF_OperationsKey_Char.AsString:=CommonSet.Key_Char;
        taCF_OperationsID_CLASSIF.AsInteger:=-1;
        taCF_OperationsID_Operations.AsInteger:=-1;
        taCF_OperationsID_States.AsInteger:=-1;
        taCF_OperationsBARCODE.AsString:='-1';
        taCF_OperationsKEY_POSITION.AsInteger:=-1;
        taCF_OperationsARTICUL.AsString:=taCardSclaarticul.AsString;
        taCF_OperationsBAR.AsString:=taCardSclabarcode.AsString;
        taCF_Operations.Post;

        trUpdate.Commit;

        taCF_Operations.FullRefresh;
        if taCardSclabarcode.AsString>'' then taCF_Operations.Locate('Bar',taCardSclabarcode.AsString,[])
        else taCF_Operations.Locate('Articul',taCardSclaArticul.AsString,[]);

      end;
    end;
  end;
  end;
end;


procedure pEditGroupButton;
var iCode,MemCode:Integer;
begin
  //��������� ��������������
  CommonSet.Key_Add:=False;
  with dmC do
  begin
    try
      CommonSEt.Key_Char:=taCF_OperationsKEY_CHAR.AsString;
      if taCF_OperationsKEY_CODE.AsInteger<1000 then
      begin
        CommonSet.Key_Code:=taCF_OperationsKEY_CODE.AsInteger;
        CommonSet.Key_Shift:=0;   //������
      end
      else
      begin
        if taCF_OperationsKEY_CODE.AsInteger<2000 then
        begin
          CommonSet.Key_Code:=taCF_OperationsKEY_CODE.AsInteger-1000;
          CommonSet.Key_Shift:=1; //shift
        end
        else
        begin
          CommonSet.Key_Code:=taCF_OperationsKEY_CODE.AsInteger-2000;
          CommonSet.Key_Shift:=2; //ctrl
        end;
      end;

      fmPressKey.Edit1.Text:=taCF_OperationsKEY_CHAR.AsString;
      fmPressKey.Edit2.Text:=IntToStr(taCF_OperationsKEY_CODE.AsInteger);
      fmPressKey.Label1.Caption:='������� ������ �������.';

      memCode:=CommonSet.Key_Code+1000*(CommonSet.Key_Shift);
      vSet:=False;
      fmPressKey.ShowModal;
      if vSet then  //���-�� �������
      begin
        iCode:=CommonSet.Key_Code+1000*(CommonSet.Key_Shift);
        if (iCode<>memCode) and (iCode<>0) then
        begin //���-�� ����������
            //���� , ����� ��� ����.
          if prFindKey(iCode,StrWk) then ShowMessage(Strwk)
          else //������ ��������� ��� ��� - ����� ��������������
          begin
              if taCF_Operations.locate('KEY_CODE',taCF_OperationsKEY_CODE.AsInteger,[]) then
              begin
                trUpdate.StartTransaction;

                taCF_Operations.Edit;
                taCF_OperationsKey_CODE.AsInteger:=CommonSet.Key_Code+1000*(CommonSet.Key_Shift);
                taCF_OperationsKey_Status.AsInteger:=1; //������ �������
                taCF_OperationsKey_Char.AsString:=CommonSet.Key_Char;
                taCF_Operations.Post;

                trUpdate.Commit;
              end;

              taCF_Operations.FullRefresh;
            end;
          end;
        end;
    finally
    end;
  end;
end;


procedure pAddButtonGroup;
var iCode:Integer;
begin
  with dmC do
  begin
  CommonSet.Key_Char:='';
  CommonSet.Key_Code:=0;
  CommonSet.Key_Shift:=0;
  CommonSet.Key_Add:=True;

  fmPressKey.Edit1.Text:='';
  fmPressKey.Edit2.Text:='';
  fmPressKey.Label1.Caption:='������� ������ �������.';

  vSet:=False;
  fmPressKey.ShowModal;
  if vSet then  //���-�� �������
  begin
    if CommonSet.Key_Code>0 then
    begin
      iCode:=CommonSet.Key_Code+1000*(CommonSet.Key_Shift);
      //���� , ����� ��� ����.
      if prFindKey(iCode,StrWk) then ShowMessage(Strwk)
      else //������ ��������� ��� ��� - ���������
      begin
        trUpdate.StartTransaction;

        taCF_Operations.Append;
        taCF_OperationsKey_CODE.AsInteger:=CommonSet.Key_Code+1000*(CommonSet.Key_Shift);
        taCF_OperationsKey_Status.AsInteger:=1; //������ �������
        taCF_OperationsKey_Char.AsString:=CommonSet.Key_Char;
        taCF_OperationsID_CLASSIF.AsInteger:=Integer(fmMainConfig.TreeClassif.Selected.Data);
        taCF_OperationsID_Operations.AsInteger:=-1;
        taCF_OperationsID_States.AsInteger:=-1;
        taCF_OperationsBARCODE.AsString:='-1';
        taCF_OperationsKEY_POSITION.AsInteger:=-1;
        taCF_Operations.Post;

        trUpdate.Commit;
        delay(10);
        taCF_Operations.FullRefresh;
      end;
    end;
  end;
  end;
end;

procedure pEditButton;
begin
  //��������� ������
  CommonSet.Key_Add:=False;
  with dmC do
  begin
    try
      CommonSEt.Key_Char:=taCF_OperationsKEY_CHAR.AsString;
      if taCF_OperationsKEY_CODE.AsInteger<1000 then
      begin
        CommonSet.Key_Code:=taCF_OperationsKEY_CODE.AsInteger;
        CommonSet.Key_Shift:=0;   //������
      end
      else
      begin
        if taCF_OperationsKEY_CODE.AsInteger<2000 then
        begin
          CommonSet.Key_Code:=taCF_OperationsKEY_CODE.AsInteger-1000;
          CommonSet.Key_Shift:=1; //shift
        end
        else
        begin
          CommonSet.Key_Code:=taCF_OperationsKEY_CODE.AsInteger-2000;
          CommonSet.Key_Shift:=2; //ctrl
        end;
      end;

      fmAddOper.ComboBox1.EditValue:=taCF_OperationsID_OPERATIONS.AsInteger;
      fmAddOper.ComboBox2.EditValue:=taCF_OperationsID_STATES.AsInteger;

      fmAddOper.Edit1.Text:=taCF_OperationsKEY_CHAR.AsString;
      fmAddOper.Edit2.Text:=IntToStr(taCF_OperationsKEY_CODE.AsInteger);

      fmAddOper.Edit3.Text:=taCF_OperationsBARCODE.AsString; //�������� BarCode
      fmAddOper.ComboBox3.ItemIndex:=taCF_OperationsKEY_POSITION.AsInteger;//��������� �����

      fmAddOper.ComboBox1.Enabled:=False;
      fmAddOper.Edit1.Enabled:=False;
      fmAddOper.Edit2.Enabled:=False;
      fmAddOper.BitBtn1.Enabled:=False;

      fmAddOper.ShowModal;
    except
    end;
  end;
end;


procedure pDelButton;
Var iCode:INteger;
begin
  with dmC do
  begin
    try
      if MessageDlg('�� ������������� ������ ������� ������ ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        iCode:=0;
        if not taCF_Operations.Eof then
        begin
          taCF_Operations.Next;
          if not taCF_Operations.Eof then
          begin
            iCode:=taCF_OperationsKEY_CODE.AsInteger;
            taCF_Operations.Prior;
          end;
        end;

        trUpdate.StartTransaction;
        taCF_Operations.Delete;
        trUpdate.Commit;


        taCF_Operations.FullRefresh;
        if iCode>0 then taCF_Operations.Locate('KEY_CODE',iCode,[]);
      end;
    except
    end;
  end;
end;


procedure pAddButton;
begin
  // ����������
  CommonSet.Key_Add:=True;
  with dmC do
  begin

    CommonSEt.Key_Char:='';
    CommonSet.Key_Code:=0;
    CommonSet.Key_Shift:=0;

    fmAddOper.Edit1.Text:='';
    fmAddOper.Edit2.Text:='';

    taStates.First; //����� ������
    taOperations.First;

    fmAddOper.ComboBox1.EditValue:=taOperationsId.AsInteger;
    fmAddOper.ComboBox2.EditValue:=taStatesId.AsInteger;

    fmAddOper.Edit3.Text:=taStatesBarCode.AsString; //�������� BarCode �� ���������
    fmAddOper.ComboBox3.ItemIndex:=taStatesKey_Position.AsInteger;



    fmAddOper.ComboBox1.Enabled:=True;
    fmAddOper.Edit1.Enabled:=True;
    fmAddOper.Edit2.Enabled:=True;
    fmAddOper.BitBtn1.Enabled:=True;

    fmAddOper.ShowModal;
  end;
end;

procedure TfmMainConfig.N3Click(Sender: TObject);
begin
  close;
end;

procedure TfmMainConfig.Tree1Change(Sender: TObject; Node: TTreeNode);
var strwk:string;
begin
  with dmC do
  begin
    strwk:=Tree1.Selected.Text;
    Panel3.Visible:=False;
    Panel4.Visible:=False;
    Panel5.Visible:=False;
    Panel6.Visible:=False;

    TreeClassif.Items.Clear;

    if strwk='�������� ��������' then
    begin
      acAdd.Enabled:=True;
      acEdit.Enabled:=True;
      acDel.Enabled:=True;
      Panel3.Visible:=True;
      delay(10);
      taCF_Operations.Active:=False;
      taCF_Operations.ParamByName('TYPEOP').AsInteger:=0; //������� �� ���������
      taCF_Operations.Active:=True;
    end;
    if strwk='������ �������' then
    begin
      acAdd.Enabled:=True;
      acEdit.Enabled:=True;
      acDel.Enabled:=True;
      Panel4.Visible:=True;
      delay(10);
      taCF_Operations.Active:=False;
      taCF_Operations.ParamByName('TYPEOP').AsInteger:=1; // ������� �� ������� �������
      taCF_Operations.Active:=True;
      //      MakeTree;
      TreeClassif.Items.Clear;
      ClassifExpandLevel(Nil,TreeClassif,dmC.quClassifR,Person.Id);
    end;
    if strwk='������' then
    begin
      acAdd.Enabled:=True;
      acEdit.Enabled:=True;
      acDel.Enabled:=True;
      Panel5.Visible:=True;
      delay(10);

      taCF_Operations.Active:=False;
      taCF_Operations.ParamByName('TYPEOP').AsInteger:=2; // ������� �� �������
      taCF_Operations.Active:=True;
    end;
    if strwk='������' then
    begin
      acAdd.Enabled:=True;
      acEdit.Enabled:=True;
      acDel.Enabled:=True;
      Panel6.Visible:=True;
      delay(10);

      taCF_Operations.Active:=False;
      taCF_Operations.ParamByName('TYPEOP').AsInteger:=3; // ������� �� �������
      taCF_Operations.Active:=True;
    end;
  end;
end;

procedure TfmMainConfig.FormCreate(Sender: TObject);
begin
  CurDir := ExtractFilePath(ParamStr(0));
  ReadIni;
  Memo1.Clear;

  Panel3.Visible:=False;
  Panel3.Align:=alClient;
  grid1.Align:=alClient;

  Panel4.Visible:=False;
  Panel4.Align:=alClient;
  GridCl.Align:=alBottom;
  TreeClassif.Align:=alClient;

  Panel5.Visible:=False;
  Panel5.Align:=alClient;
  GridCFGoods.Align:=alBottom;
  GridGoods.Align:=alClient;

  Panel6.Visible:=False;
  Panel6.Align:=alClient;
  GridDep.Align:=alClient;
  GridCFDep.Align:=alBottom;

  FormPlacement1.IniFileName := CurDir+GridIni;
  FormPlacement1.Active:=True;
  View1.RestoreFromIniFile(CurDir+GridIni);
end;

procedure TfmMainConfig.N5Click(Sender: TObject);
begin
  bSave:=True;
  fmSaveLoad.BitBtn1.Caption:='���������';
  fmSaveLoad.ShowModal;
end;

procedure TfmMainConfig.N6Click(Sender: TObject);
begin
  bSave:=False;
  fmSaveLoad.BitBtn1.Caption:='���������';
  fmSaveLoad.ShowModal;
end;

procedure TfmMainConfig.FormShow(Sender: TObject);
begin
  with dmC do
  begin
    quPer.Active:=False;
    quPer.SelectSQL.Clear;
    quPer.SelectSQL.Add('select * from rpersonal');
    quPer.SelectSQL.Add('where uvolnen=1 and modul2=0');
    quPer.SelectSQL.Add('order by Name');
  end;
  fmPerA.ShowModal;
  if fmPerA.ModalResult=mrOk then
  begin
    Caption:=Caption+'   : '+Person.Name;
    WriteIni; //�������� �������� �������
  end
  else
  begin
    delay(100);
    close;
    delay(100);
  end;
  WrMess('���� - "'+DBName+'" Ok.');
  with dmC do
  begin
    taOperations.Open;
    taStates.Open;
    taClassif.Open;
    taDeparts.Open;
    taCardScla.Open;
    taCF_All.Open;
  end;
end;

procedure TfmMainConfig.acAddExecute(Sender: TObject);
begin
  if Tree1.Selected.Text='�������� ��������' then pAddButton;
  if Tree1.Selected.Text='������ �������' then pAddButtonGroup;
  if Tree1.Selected.Text='������' then pAddButtonCard;
  if Tree1.Selected.Text='������' then pAddButtonDepart;
end;

procedure TfmMainConfig.acEditExecute(Sender: TObject);
begin
  if Tree1.Selected.Text='�������� ��������' then pEditButton;
  if Tree1.Selected.Text='������ �������' then pEditGroupButton;
  if Tree1.Selected.Text='������' then pEditCardButton;
  if Tree1.Selected.Text='������' then pEditDepartButton;
end;

procedure TfmMainConfig.acDelExecute(Sender: TObject);
begin
  pDelButton;
end;

procedure TfmMainConfig.SpeedItem4Click(Sender: TObject);
begin
  close;
end;

procedure TfmMainConfig.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  View1.StoreToIniFile(CurDir+GridIni,False);
//  dmC.cxStyle1.Color
end;

procedure TfmMainConfig.TreeClassifExpanding(Sender: TObject;
  Node: TTreeNode; var AllowExpansion: Boolean);
begin
  if Node.getFirstChild.Data = nil then
  begin
    Node.DeleteChildren;
    ClassifExpandLevel(Node,TreeClassif,dmC.quClassifR,Person.Id);
  end;
end;

end.


