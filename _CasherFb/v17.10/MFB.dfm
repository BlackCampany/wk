object dmFB: TdmFB
  OldCreateOrder = False
  Left = 233
  Top = 278
  Height = 308
  Width = 434
  object Cash: TpFIBDatabase
    DBName = 'localhost:C:\_MCrystal\Cash\DB\cash.gdb'
    DBParams.Strings = (
      'user_name=SYSDBA'
      'lc_ctype=WIN1251'
      'password=masterkey'
      'sql_role_name=SYSDBA')
    DefaultTransaction = trSelect
    DefaultUpdateTransaction = trUpdate
    SQLDialect = 3
    Timeout = 0
    SynchronizeTime = False
    DesignDBOptions = []
    AliasName = 'Cash'
    WaitForRestoreConnect = 20
    Left = 24
    Top = 16
  end
  object trSelect: TpFIBTransaction
    DefaultDatabase = Cash
    TimeoutAction = TARollback
    Left = 24
    Top = 72
  end
  object trUpdate: TpFIBTransaction
    DefaultDatabase = Cash
    TimeoutAction = TARollback
    Left = 24
    Top = 128
  end
  object quSetActive: TpFIBQuery
    Transaction = trUpdate
    Database = Cash
    SQL.Strings = (
      'update cash01 set IACTIVE=2'
      'where IACTIVE=1')
    Left = 92
    Top = 16
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object quSelLoad: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE CASH01'
      'SET '
      '    IACTIVE = :IACTIVE,'
      '    ITYPE = :ITYPE,'
      '    ARTICUL = :ARTICUL,'
      '    BARCODE = :BARCODE,'
      '    CARDSIZE = :CARDSIZE,'
      '    QUANT = :QUANT,'
      '    DSTOP = :DSTOP,'
      '    NAME = :NAME,'
      '    MESSUR = :MESSUR,'
      '    PRESISION = :PRESISION,'
      '    ADD1 = :ADD1,'
      '    ADD2 = :ADD2,'
      '    ADD3 = :ADD3,'
      '    ADDNUM1 = :ADDNUM1,'
      '    ADDNUM2 = :ADDNUM2,'
      '    ADDNUM3 = :ADDNUM3,'
      '    SCALE = :SCALE,'
      '    GROUP1 = :GROUP1,'
      '    GROUP2 = :GROUP2,'
      '    GROUP3 = :GROUP3,'
      '    PRICE = :PRICE,'
      '    CLIINDEX = :CLIINDEX,'
      '    DELETED = :DELETED,'
      '    PASSW = :PASSW'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    CASH01'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO CASH01('
      '    ID,'
      '    IACTIVE,'
      '    ITYPE,'
      '    ARTICUL,'
      '    BARCODE,'
      '    CARDSIZE,'
      '    QUANT,'
      '    DSTOP,'
      '    NAME,'
      '    MESSUR,'
      '    PRESISION,'
      '    ADD1,'
      '    ADD2,'
      '    ADD3,'
      '    ADDNUM1,'
      '    ADDNUM2,'
      '    ADDNUM3,'
      '    SCALE,'
      '    GROUP1,'
      '    GROUP2,'
      '    GROUP3,'
      '    PRICE,'
      '    CLIINDEX,'
      '    DELETED,'
      '    PASSW'
      ')'
      'VALUES('
      '    :ID,'
      '    :IACTIVE,'
      '    :ITYPE,'
      '    :ARTICUL,'
      '    :BARCODE,'
      '    :CARDSIZE,'
      '    :QUANT,'
      '    :DSTOP,'
      '    :NAME,'
      '    :MESSUR,'
      '    :PRESISION,'
      '    :ADD1,'
      '    :ADD2,'
      '    :ADD3,'
      '    :ADDNUM1,'
      '    :ADDNUM2,'
      '    :ADDNUM3,'
      '    :SCALE,'
      '    :GROUP1,'
      '    :GROUP2,'
      '    :GROUP3,'
      '    :PRICE,'
      '    :CLIINDEX,'
      '    :DELETED,'
      '    :PASSW'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT first 100 ID,IACTIVE,ITYPE,ARTICUL,BARCODE,CARDSIZE,QUANT' +
        ',DSTOP,NAME,MESSUR,PRESISION,'
      
        'ADD1,ADD2,ADD3,ADDNUM1,ADDNUM2,ADDNUM3,SCALE,GROUP1,GROUP2,GROUP' +
        '3,PRICE,CLIINDEX,DELETED,PASSW'
      'FROM CASH01'
      'where(  IACTIVE=2'
      '     ) and (     CASH01.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT first 100 ID,IACTIVE,ITYPE,ARTICUL,BARCODE,CARDSIZE,QUANT' +
        ',DSTOP,NAME,MESSUR,PRESISION,'
      
        'ADD1,ADD2,ADD3,ADDNUM1,ADDNUM2,ADDNUM3,SCALE,GROUP1,GROUP2,GROUP' +
        '3,PRICE,CLIINDEX,DELETED,PASSW'
      'FROM CASH01'
      'where IACTIVE=2')
    Transaction = trSelect
    Database = Cash
    UpdateTransaction = trUpdate
    AutoCommit = True
    Left = 176
    Top = 16
    object quSelLoadID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quSelLoadIACTIVE: TFIBSmallIntField
      FieldName = 'IACTIVE'
    end
    object quSelLoadITYPE: TFIBSmallIntField
      FieldName = 'ITYPE'
    end
    object quSelLoadARTICUL: TFIBIntegerField
      FieldName = 'ARTICUL'
    end
    object quSelLoadBARCODE: TFIBStringField
      FieldName = 'BARCODE'
      Size = 15
      EmptyStrToNull = True
    end
    object quSelLoadCARDSIZE: TFIBStringField
      FieldName = 'CARDSIZE'
      Size = 10
      EmptyStrToNull = True
    end
    object quSelLoadQUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quSelLoadDSTOP: TFIBFloatField
      FieldName = 'DSTOP'
    end
    object quSelLoadNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 80
      EmptyStrToNull = True
    end
    object quSelLoadMESSUR: TFIBStringField
      FieldName = 'MESSUR'
      Size = 5
      EmptyStrToNull = True
    end
    object quSelLoadPRESISION: TFIBFloatField
      FieldName = 'PRESISION'
    end
    object quSelLoadADD1: TFIBStringField
      FieldName = 'ADD1'
      EmptyStrToNull = True
    end
    object quSelLoadADD2: TFIBStringField
      FieldName = 'ADD2'
      EmptyStrToNull = True
    end
    object quSelLoadADD3: TFIBStringField
      FieldName = 'ADD3'
      EmptyStrToNull = True
    end
    object quSelLoadADDNUM1: TFIBFloatField
      FieldName = 'ADDNUM1'
    end
    object quSelLoadADDNUM2: TFIBFloatField
      FieldName = 'ADDNUM2'
    end
    object quSelLoadADDNUM3: TFIBFloatField
      FieldName = 'ADDNUM3'
    end
    object quSelLoadSCALE: TFIBStringField
      FieldName = 'SCALE'
      Size = 10
      EmptyStrToNull = True
    end
    object quSelLoadGROUP1: TFIBIntegerField
      FieldName = 'GROUP1'
    end
    object quSelLoadGROUP2: TFIBIntegerField
      FieldName = 'GROUP2'
    end
    object quSelLoadGROUP3: TFIBIntegerField
      FieldName = 'GROUP3'
    end
    object quSelLoadPRICE: TFIBFloatField
      FieldName = 'PRICE'
    end
    object quSelLoadCLIINDEX: TFIBIntegerField
      FieldName = 'CLIINDEX'
    end
    object quSelLoadDELETED: TFIBSmallIntField
      FieldName = 'DELETED'
    end
    object quSelLoadPASSW: TFIBStringField
      FieldName = 'PASSW'
      EmptyStrToNull = True
    end
  end
end
