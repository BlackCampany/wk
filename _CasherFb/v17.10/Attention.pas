unit Attention;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, jpeg, ExtCtrls, ActnList, XPStyleActnCtrls, ActnMan, StdCtrls;

type
  TfmAttention = class(TForm)
    Image1: TImage;
    amAttention: TActionManager;
    acExit: TAction;
    Action1: TAction;
    Action2: TAction;
    Action3: TAction;
    Action4: TAction;
    Action5: TAction;
    Action6: TAction;
    Action7: TAction;
    Action8: TAction;
    Action9: TAction;
    Action10: TAction;
    Action11: TAction;
    Action12: TAction;
    Action13: TAction;
    Action14: TAction;
    Action15: TAction;
    Action16: TAction;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    procedure acExitExecute(Sender: TObject);
    procedure Action12Execute(Sender: TObject);
    procedure Action15Execute(Sender: TObject);
    procedure Action16Execute(Sender: TObject);
    procedure Action1Execute(Sender: TObject);
    procedure Action2Execute(Sender: TObject);
    procedure Action3Execute(Sender: TObject);
    procedure Action4Execute(Sender: TObject);
    procedure Action5Execute(Sender: TObject);
    procedure Action6Execute(Sender: TObject);
    procedure Action7Execute(Sender: TObject);
    procedure Action8Execute(Sender: TObject);
    procedure Action9Execute(Sender: TObject);
    procedure Action10Execute(Sender: TObject);
    procedure Action11Execute(Sender: TObject);
    procedure Action13Execute(Sender: TObject);
    procedure Action14Execute(Sender: TObject);
    procedure Image1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAttention: TfmAttention;

implementation

{$R *.dfm}

procedure TfmAttention.acExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmAttention.Action12Execute(Sender: TObject);
begin
//
end;

procedure TfmAttention.Action15Execute(Sender: TObject);
begin
//
end;

procedure TfmAttention.Action16Execute(Sender: TObject);
begin
//
end;

procedure TfmAttention.Action1Execute(Sender: TObject);
begin
//
end;

procedure TfmAttention.Action2Execute(Sender: TObject);
begin
//
end;

procedure TfmAttention.Action3Execute(Sender: TObject);
begin
//
end;

procedure TfmAttention.Action4Execute(Sender: TObject);
begin
//
end;

procedure TfmAttention.Action5Execute(Sender: TObject);
begin
//
end;

procedure TfmAttention.Action6Execute(Sender: TObject);
begin
//
end;

procedure TfmAttention.Action7Execute(Sender: TObject);
begin
//
end;

procedure TfmAttention.Action8Execute(Sender: TObject);
begin
//
end;

procedure TfmAttention.Action9Execute(Sender: TObject);
begin
//
end;

procedure TfmAttention.Action10Execute(Sender: TObject);
begin
//
end;

procedure TfmAttention.Action11Execute(Sender: TObject);
begin
//
end;

procedure TfmAttention.Action13Execute(Sender: TObject);
begin
//
end;

procedure TfmAttention.Action14Execute(Sender: TObject);
begin
//
end;

procedure TfmAttention.Image1Click(Sender: TObject);
begin
  ModalResult:=mrOk;
end;

end.
