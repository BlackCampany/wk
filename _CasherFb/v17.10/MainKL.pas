unit MainKL;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ComCtrls, StdCtrls, cxControls, cxContainer, cxEdit,
  cxTextEdit, cxMaskEdit, cxButtonEdit, cxLookAndFeelPainters, cxButtons,
  DB, DBClient, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, FIBDatabase,
  pFIBDatabase, FIBDataSet, pFIBDataSet, Menus, cxDataStorage,
  cxCurrencyEdit, RXSplit, cxSpinEdit;

type
  TfmMainKL = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Panel2: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    OpenDialog1: TOpenDialog;
    OpenDialog2: TOpenDialog;
    ButtonEdit1: TcxButtonEdit;
    ButtonEdit2: TcxButtonEdit;
    Button1: TcxButton;
    Button2: TcxButton;
    taKl: TClientDataSet;
    dsKl: TDataSource;
    taKlCheckNum: TIntegerField;
    taKlArticul: TStringField;
    taKlPrice: TCurrencyField;
    taKlQuant: TFloatField;
    taKlSumD: TCurrencyField;
    taKlOper: TStringField;
    ViewKl: TcxGridDBTableView;
    LevelKl: TcxGridLevel;
    GridKl: TcxGrid;
    ViewKlOper: TcxGridDBColumn;
    ViewKlCheckNum: TcxGridDBColumn;
    ViewKlArticul: TcxGridDBColumn;
    ViewKlQuant: TcxGridDBColumn;
    ViewKlPrice: TcxGridDBColumn;
    ViewKlSumD: TcxGridDBColumn;
    ViewCS: TcxGridDBTableView;
    LevelCS: TcxGridLevel;
    GridCS: TcxGrid;
    dmC: TpFIBDatabase;
    trSelect: TpFIBTransaction;
    trUpdate: TpFIBTransaction;
    taCS: TpFIBDataSet;
    dsCS: TDataSource;
    taCSCHECKNUMBER: TFIBIntegerField;
    taCSID: TFIBIntegerField;
    taCSARTICUL: TFIBStringField;
    taCSQUANTITY: TFIBFloatField;
    taCSPRICERUB: TFIBFloatField;
    taCSDPROC: TFIBFloatField;
    taCSDSUM: TFIBFloatField;
    taCSTOTALRUB: TFIBFloatField;
    taCSOPERATION: TFIBSmallIntField;
    ViewCSCHECKNUMBER: TcxGridDBColumn;
    ViewCSID: TcxGridDBColumn;
    ViewCSARTICUL: TcxGridDBColumn;
    ViewCSQUANTITY: TcxGridDBColumn;
    ViewCSPRICERUB: TcxGridDBColumn;
    ViewCSDPROC: TcxGridDBColumn;
    ViewCSDSUM: TcxGridDBColumn;
    ViewCSTOTALRUB: TcxGridDBColumn;
    ViewCSOPERATION: TcxGridDBColumn;
    cxButton1: TcxButton;
    Memo1: TMemo;
    RxSplitter1: TRxSplitter;
    SpinEdit1: TcxSpinEdit;
    procedure ButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure ButtonEdit2PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure FormCreate(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function RoundEx( X: Double ): Integer;

var
  fmMainKL: TfmMainKL;

implementation

uses Un1;

{$R *.dfm}

function RoundEx( X: Double ): Integer;
var  ScaledFractPart:Integer;
     Temp : Integer;
begin
 ScaledFractPart := Trunc(X);
 Temp := Trunc(Frac(X)*1000000)+1;
 if Temp >=  500000 then ScaledFractPart := ScaledFractPart + 1;
 if Temp <= -500000 then ScaledFractPart := ScaledFractPart - 1;
{ if Temp >=  0.5 then ScaledFractPart := ScaledFractPart + 1;
 if Temp <= -0.5 then ScaledFractPart := ScaledFractPart - 1;}
 RoundEx := ScaledFractPart;
end;


procedure TfmMainKL.ButtonEdit1PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  OpenDialog1.Execute;
  ButtonEdit1.Text:=OpenDialog1.FileName;
end;

procedure TfmMainKL.ButtonEdit2PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  OpenDialog2.Execute;
  ButtonEdit2.Text:=OpenDialog2.FileName;
end;

procedure TfmMainKL.FormCreate(Sender: TObject);
begin
  CurDir:=ExtractFilePath(ParamStr(0));
  ReadKlIni;
  OpenDialog1.InitialDir:=CurDir;
  OpenDialog1.Filter := 'Log ����� (*.log)|*.log|��� (*.*)|*.*';
  OpenDialog2.InitialDir:=CurDir;
  OpenDialog2.Filter := 'Gdb ����� (*.gdb)|*.gdb|��� (*.*)|*.*';
{  ButtonEdit1.Text:='C:\_CasherFb\Errors\Db\2006_05_14.log';
  ButtonEdit2.Text:='Master:C:\_CasherFb\Db\CASHERDB.GDB';
}
  ButtonEdit2.Text:=DBName;
  ButtonEdit1.Text:=CommonSet.PathArh;

  Memo1.Clear;
end;

procedure TfmMainKL.Button2Click(Sender: TObject);
begin
  Close;
end;

procedure TfmMainKL.Button1Click(Sender: TObject);
Var f:TextFile;
    StrWk,StrWk1,StrWk2:String;
    CurDate:TDateTime;
    i:Integer;
begin
  try
    Memo1.Clear;
    Memo1.Lines.Add('��������� ���.'); Delay(10);
    taKl.Active:=False;
    taKl.CreateDataSet;
    ViewKl.BeginUpdate;
    //��������� ���� �� �������� �����
    StrWk:=ButtonEdit1.Text;
    i:=Pos('.log',strwk);
    if i=0 then exit;
    StrWk:=Copy(strwk,i-10,10);
    strwk:=Copy(strwk,9,2)+'.'+Copy(strwk,6,2)+'.'+Copy(strwk,1,4);
    CurDate:=StrToDate(StrWk);

    DBName:=ButtonEdit2.Text;
    CommonSet.PathArh:=ButtonEdit1.Text;
    WriteKlIni;


    i:=0;
    AssignFile(f,ButtonEdit1.Text);
    Reset(f);
    while not eof(f) do
    begin
      ReadLn(f,StrWk);
      if (pos(';~!',strwk)>0)and(pos('EndCheck',strwk)=0)and(pos('BegCheck',strwk)=0) then
      begin //�������� ������ ����

        taKl.Append;

        StrWk1:=Copy(StrWk,pos(';~!',strwk)+3,Length(StrWk)-(pos(';~!',strwk)+3)+1);
        StrWk2:=Copy(StrWk1,1,Pos(';',StrWk1)-1); Delete(StrWk1,1,Pos(';',StrWk1));
        taKlOper.AsString:=StrWk2;

        StrWk2:=Copy(StrWk1,1,Pos(';',StrWk1)-1); Delete(StrWk1,1,Pos(';',StrWk1));
        taKlCheckNum.AsInteger:=StrToIntDef(StrWk2,0);

        StrWk2:=Copy(StrWk1,1,Pos(';',StrWk1)-1); Delete(StrWk1,1,Pos(';',StrWk1));
        taKlArticul.AsString:=StrWk2;

        //�������� ���������
        StrWk2:=Copy(StrWk1,1,Pos(';',StrWk1)-1); Delete(StrWk1,1,Pos(';',StrWk1));

        StrWk2:=Copy(StrWk1,1,Pos(';',StrWk1)-1); Delete(StrWk1,1,Pos(';',StrWk1));
        taKlQuant.AsFloat:=StrToFloatDef(StrWk2,0);

        StrWk2:=Copy(StrWk1,1,Pos(';',StrWk1)-1); Delete(StrWk1,1,Pos(';',StrWk1));
        taKlPrice.AsFloat:=StrToFloatDef(StrWk2,0);

        StrWk2:=Copy(StrWk1,1,Pos(';',StrWk1)-1); Delete(StrWk1,1,Pos(';',StrWk1));
        taKlSumD.AsFloat:=StrToFloatDef(StrWk2,0);

        taKl.Post;
      end;
      inc(i);
      if i mod 100=0 then
      begin
        Memo1.Lines.Add('  ������� - '+IntToStr(i)+' �����.'); Delay(10);
      end;
    end;
  finally
    CloseFile(f);
  end;
  ViewKl.EndUpdate;

  Memo1.Lines.Add('��������� ����.'); Delay(10);

  dmC.Connected:=False;
  dmC.DBName:=DBName;
  try
    dmC.Connected:=True;
    taCS.Active:=False;
    taCS.ParamByName('DATEB').AsDateTime:=Trunc(CurDate);
    taCS.ParamByName('DATEE').AsDateTime:=Trunc(CurDate)+1;
    taCS.Active:=True;

    Memo1.Lines.Add('���� ��.'); Delay(10);
  except
    dmC.Connected:=False;
  end;
end;

procedure TfmMainKL.cxButton1Click(Sender: TObject);
Var iCh:Integer;
    rSum,rSum1,rSumQ,rSumTmp:Currency;
    sArt:String;  //sArt1 ������� ������� ��� ����������� � ���� ��� ���� ��������� ���-��
    bQ:Boolean;
    iAddCS:Integer;
begin
  if (taKl.Active) and (taCS.Active) then
  begin
    iAddCS:=SpinEdit1.EditValue;

    Memo1.Clear;
    Memo1.Lines.Add('�������� �������� �� �����.');
    delay(10);
    rSum:=0; //������������� ����� �� ����
    rSumQ:=0; //����� ��� * ���-��
    rSumTmp:=0; // ����� �� ���������� ���.
    sArt:=''; //���������� �������
    taKl.First;
    iCh:=taKlCheckNum.AsInteger;
    bQ:=False; //���������� ������� �� ���-��
    while not taKl.Eof do
    begin
      if taKlOper.AsString='Error' then
      begin
        rSum:=rSum-rSumTmp;
        bQ:=False; //���������� ������� �� ���-��
      end;
      if taKlOper.AsString='Quant' then
      begin
        if taKlArticul.AsString<>sArt then
        begin //���-�� � ���������� ����� ����� �� ���������
          rSumQ:=taKlPrice.AsCurrency; //�� ��������
          bQ:=True;
        end else
        begin
          rSumQ:=taKlPrice.AsCurrency;
          rSum:=rSum+rSumQ-rSumTmp; //����� ����� ���������� ������� + ����� �� ���-��
          rSumTmp:=rSumQ; //��� �������� ���������� ���-��
        end;
      end;
      if taKlOper.AsString='Annul' then
      begin
        rSum:=0;
        rSumQ:=0;
        rSumTmp:=0;
        bQ:=False; //���������� ������� �� ���-��
      end;

      if taKlOper.AsString='Real' then
      begin
        if taKlCheckNum.AsInteger<>iCh then
        begin //����� ���� ��������
          rSum1:=0;
          if taCS.Locate('CHECKNUMBER',iCh+iAddCS,[]) then
          begin
            while (taCSCHECKNUMBER.AsInteger=iCh+iAddCS)and(not taCS.Eof) do
            begin
              rSum1:=rSum1+taCSTOTALRUB.AsFloat+taCSDSUM.AsFloat;
              taCS.Next;
            end;
          end;
          if rSum<>rSum1 then
          begin
            Memo1.Lines.Add('  ��� '+INtToStr(iCh)+' . ����� � �� '+FloatToStr(rSum)+'. ����� � CS '+FloatToStr(rSum1));
            delay(10);
          end;
          rSumTmp:=RoundEx(taKlQuant.AsFloat*taKlPrice.AsFloat*100)/100;
          rSum:=rSumTmp;
          iCh:=taKlCheckNum.AsInteger;

        end
        else  //������������ ��� ���-�� ���
        begin
          if bQ then //� ���������� ������ ���� �����
          begin
            if taKlArticul.AsString=sArt then
            begin //������� ����� - �������� Quant ������ ����� ��������
              rSum:=rSum+rSumQ;
            end
            else //������� �������� - �������� Quant ��������� � ���������� �������
            begin
{
              rSum:=rSum+rSumQ-rSumTmp; //����� ����� ���������� �������
              rSumTmp:=RoundEx(taKlQuant.AsFloat*taKlPrice.AsFloat*100)/100;
              rSum:=rSum+rSumTmp;
              }
            end;
            rSumQ:=0;
          end
          else
          begin
            rSumTmp:=RoundEx(taKlQuant.AsFloat*taKlPrice.AsFloat*100)/100;
            rSum:=rSum+rSumTmp;
          end;
        end;
        bQ:=False; //���������� ������� �� ���-��
      end;
      sArt:=taKlArticul.AsString; //��� �� ���������� �������
      taKl.Next;
    end;
  //  C:\_CasherFb\Dbel\14-05-2006\2006_05_14.log
  // Master:C:\_CasherFb\Dbel\CASHERDB.GDB
    //��� �������� ���������� ����
    rSum1:=0;
    if taCS.Locate('CHECKNUMBER',iCh+iAddCS,[]) then
    begin
      while (taCSCHECKNUMBER.AsInteger=iCh+iAddCS)and(not taCS.Eof) do
      begin
        rSum1:=rSum1+taCSTOTALRUB.AsFloat+taCSDSUM.AsFloat;
        taCS.Next;
      end;
    end;
    if rSum<>rSum1 then
    begin
      Memo1.Lines.Add('  ��� '+INtToStr(iCh+iAddCS)+' . ����� � �� '+FloatToStr(rSum)+'. ����� � CS '+FloatToStr(rSum1));
      delay(10);
    end;

    Memo1.Lines.Add('�������� ���������.');
    delay(10);
  end;
end;

procedure TfmMainKL.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  WriteKlIni;
end;

end.
