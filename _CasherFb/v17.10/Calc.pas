unit Calc;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxControls, cxContainer, cxEdit, cxTextEdit, cxMaskEdit,
  cxDropDownEdit, cxCalc, ExtCtrls, cxLookAndFeelPainters, StdCtrls,
  cxButtons, ActnList, XPStyleActnCtrls, ActnMan, Menus;

type
  TfmCalc = class(TForm)
    Panel1: TPanel;
    CalcEdit1: TcxCalcEdit;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    cxButton5: TcxButton;
    cxButton6: TcxButton;
    cxButton7: TcxButton;
    cxButton8: TcxButton;
    cxButton9: TcxButton;
    cxButton10: TcxButton;
    cxButton11: TcxButton;
    cxButton12: TcxButton;
    cxButton13: TcxButton;
    cxButton14: TcxButton;
    cxButton15: TcxButton;
    amCalc: TActionManager;
    Action1: TAction;
    Action2: TAction;
    Action3: TAction;
    Action4: TAction;
    Action5: TAction;
    Action6: TAction;
    Action7: TAction;
    Action8: TAction;
    Action9: TAction;
    Action10: TAction;
    Action11: TAction;
    Action12: TAction;
    Action13: TAction;
    Action14: TAction;
    Action15: TAction;
    acExit: TAction;
    acScaner: TAction;
    Button1: TButton;
    Action16: TAction;
    Action17: TAction;
    Label1: TLabel;
    cxButton18: TcxButton;
    TimerVes: TTimer;
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
    procedure cxButton7Click(Sender: TObject);
    procedure cxButton8Click(Sender: TObject);
    procedure cxButton9Click(Sender: TObject);
    procedure cxButton10Click(Sender: TObject);
    procedure cxButton11Click(Sender: TObject);
    procedure Action1Execute(Sender: TObject);
    procedure Action2Execute(Sender: TObject);
    procedure Action3Execute(Sender: TObject);
    procedure Action4Execute(Sender: TObject);
    procedure Action5Execute(Sender: TObject);
    procedure Action6Execute(Sender: TObject);
    procedure Action7Execute(Sender: TObject);
    procedure Action8Execute(Sender: TObject);
    procedure Action9Execute(Sender: TObject);
    procedure Action10Execute(Sender: TObject);
    procedure Action11Execute(Sender: TObject);
    procedure Action12Execute(Sender: TObject);
    procedure Action13Execute(Sender: TObject);
    procedure Action14Execute(Sender: TObject);
    procedure Action15Execute(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CalcEdit1PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
    procedure Button1Click(Sender: TObject);
    procedure acScanerExecute(Sender: TObject);
    procedure Action16Execute(Sender: TObject);
    procedure Action17Execute(Sender: TObject);
    procedure cxButton14KeyPress(Sender: TObject; var Key: Char);
    procedure cxButton18Click(Sender: TObject);
    procedure TimerVesTimer(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    Procedure prAddPos(s:Char);
  end;

var
  fmCalc: TfmCalc;
  bFirst: Boolean = False;
  bScanRead: Boolean = False;

implementation

uses Dm, Un1;

{$R *.dfm}

Procedure TfmCalc.prAddPos(s:Char);
begin
  if pos(',',CalcEdit1.Text)=0 then
  begin
    if length(CalcEdit1.Text)<5 then CalcEdit1.EditValue:=CalcEdit1.EditValue*10+StrToIntDef(s,0)
  end
  else
  begin
    if length(CalcEdit1.Text)<5 then CalcEdit1.Text:=CalcEdit1.Text+s;
  end;
  if bFirst then begin CalcEdit1.Text:=s; bFirst:=False; end;
  cxButton14.SetFocus;
end;


procedure TfmCalc.cxButton1Click(Sender: TObject);
begin
  Action1.Execute;
end;

procedure TfmCalc.cxButton2Click(Sender: TObject);
begin
  Action2.Execute;
end;

procedure TfmCalc.cxButton3Click(Sender: TObject);
begin
  Action3.Execute;
end;

procedure TfmCalc.cxButton4Click(Sender: TObject);
begin
  Action4.Execute;
end;

procedure TfmCalc.cxButton5Click(Sender: TObject);
begin
  Action5.Execute;
end;

procedure TfmCalc.cxButton6Click(Sender: TObject);
begin
  Action6.Execute;
end;

procedure TfmCalc.cxButton7Click(Sender: TObject);
begin
  Action7.Execute;
end;

procedure TfmCalc.cxButton8Click(Sender: TObject);
begin
  Action8.Execute;
end;

procedure TfmCalc.cxButton9Click(Sender: TObject);
begin
  Action9.Execute;
end;

procedure TfmCalc.cxButton10Click(Sender: TObject);
begin
  Action10.Execute;
end;

procedure TfmCalc.cxButton11Click(Sender: TObject);
begin
  Action11.Execute;
end;

procedure TfmCalc.Action1Execute(Sender: TObject);
begin
  prAddPos('1');
end;

procedure TfmCalc.Action2Execute(Sender: TObject);
begin
  prAddPos('2');
end;

procedure TfmCalc.Action3Execute(Sender: TObject);
begin
  prAddPos('3');
end;

procedure TfmCalc.Action4Execute(Sender: TObject);
begin
  prAddPos('4');
end;

procedure TfmCalc.Action5Execute(Sender: TObject);
begin
  prAddPos('5');
end;

procedure TfmCalc.Action6Execute(Sender: TObject);
begin
  prAddPos('6');
end;

procedure TfmCalc.Action7Execute(Sender: TObject);
begin
  prAddPos('7');
end;

procedure TfmCalc.Action8Execute(Sender: TObject);
begin
  prAddPos('8');
end;

procedure TfmCalc.Action9Execute(Sender: TObject);
begin
  prAddPos('9');
end;

procedure TfmCalc.Action10Execute(Sender: TObject);
begin
  prAddPos('0');
end;

procedure TfmCalc.Action11Execute(Sender: TObject);
begin
  if pos(',',CalcEdit1.Text)=0 then CalcEdit1.Text:=CalcEdit1.Text+',';
end;

procedure TfmCalc.Action12Execute(Sender: TObject);
begin
  CalcEdit1.Value:=0;
end;

procedure TfmCalc.Action13Execute(Sender: TObject);
begin
  CalcEdit1.EditValue:=CalcEdit1.EditValue+1;
end;

procedure TfmCalc.Action14Execute(Sender: TObject);
Var rQ:Real;
begin
  rQ:=CalcEdit1.EditValue-1;
  if rQ >=0 then  CalcEdit1.EditValue:=rQ;
end;

procedure TfmCalc.Action15Execute(Sender: TObject);
begin
  CalcEdit1.EditValue:=0;
end;

procedure TfmCalc.acExitExecute(Sender: TObject);
begin
  ModalResult:=mrCancel;
end;

procedure TfmCalc.FormShow(Sender: TObject);
begin
//  CalcEdit1.Value:=1;
//  Button1.SetFocus;
  cxButton14.SetFocus;
  bFirst:=True;
  bScanRead:=False;

  if bScale then Label1.Caption:='���� ��' else Label1.Caption:='����� ���';
  with dmC do
  begin
    if bScale then //���� ������� ����
    begin
      TimerVes.Enabled:=True;
    end;
  end;
end;

procedure TfmCalc.CalcEdit1PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
begin
  CalcEdit1.EditValue:=1;
end;

procedure TfmCalc.Button1Click(Sender: TObject);
begin
  if bScanRead then
  begin
    bScanRead:=False;
    exit;
  end
  else
  begin
    ModalResult:=mrOk;
  end;
end;

procedure TfmCalc.acScanerExecute(Sender: TObject);
begin
  bScanRead:=True;
end;

procedure TfmCalc.Action16Execute(Sender: TObject);
begin
  if pos(',',CalcEdit1.Text)=0 then CalcEdit1.Text:=CalcEdit1.Text+',';
end;

procedure TfmCalc.Action17Execute(Sender: TObject);
begin
  if pos(',',CalcEdit1.Text)=0 then CalcEdit1.Text:=CalcEdit1.Text+',';
end;

procedure TfmCalc.cxButton14KeyPress(Sender: TObject; var Key: Char);
begin
  if (Key=#191)or(Key=#110)or(Key=#188)or(Key=#110)or(Key='.')or(Key='/')or(Key='�')  then
    if pos(',',CalcEdit1.Text)=0 then CalcEdit1.Text:=CalcEdit1.Text+',';

  if Key='1' then prAddPos('1');
  if Key='2' then prAddPos('2');
  if Key='3' then prAddPos('3');
  if Key='4' then prAddPos('4');
  if Key='5' then prAddPos('5');
  if Key='6' then prAddPos('6');
  if Key='7' then prAddPos('7');
  if Key='8' then prAddPos('8');
  if Key='9' then prAddPos('9');
  if Key='0' then prAddPos('0');
end;

procedure TfmCalc.cxButton18Click(Sender: TObject);
begin
  CalcEdit1.Value:=cxButton18.Tag/1000;
end;

procedure TfmCalc.TimerVesTimer(Sender: TObject);
Var n: Integer;
    Buff:array[1..3] of char;
    rVes:Real;
    S1,S2:String;
begin
  TimerVes.Enabled:=False;
  with dmC do
  begin
    sScanEv:='';
    iReadCount:=0;
    n:=0;

    Buff[1]:=#$05;
    devCas.WriteBuf(Buff,1);
    delay(5);
    Buff[1]:=#$12;
    devCas.WriteBuf(Buff,1);

    while (iReadCount<38)and(n<10) do
    begin
      delay(50);
      inc(n);
    end;
    rVes:=0;
    if pos('kg',sScanEv)>0 then
    begin
      S1:=Copy(sScanEv,pos('kg',sScanEv)-8,8);
      S2:=S1[1];
      delete(S1,1,1);
      while pos(' ',S1)>0 do delete(S1,pos(' ',S1),1);
      while pos('.',S1)>0 do S1[pos('.',S1)]:=',';
      rVes:=StrToFloatDef(S1,0);
    end;
    if bFirst then
    begin
      if S2='S' then
      begin
        if rVes>0.020 then CalcEdit1.Value:=rVes;
        bFirst:=False;
        Label1.Caption:='���� ��. ��� - '+S1;
        cxButton18.Tag:=RoundEx(rVes*1000);
      end else
      begin
        cxButton18.Tag:=0;
        Label1.Caption:='���� ��. ��� ������������.';
      end;
    end else
    begin
      if S2='S' then
      begin
        Label1.Caption:='���� ��. ��� - '+S1;
        cxButton18.Tag:=RoundEx(rVes*1000);
      end else
      begin
        cxButton18.Tag:=0;
        Label1.Caption:='���� ��. ��� ������������.';
      end;
    end;
  end;
  TimerVes.Enabled:=True;
end;

end.
