unit TestVes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Menus, cxLookAndFeelPainters, cxButtons,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, dxmdaset, ActnList, XPStyleActnCtrls, ActnMan;

type
  TfmTestVes = class(TForm)
    Label3: TLabel;
    Label2: TLabel;
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    Label1: TLabel;
    TimerVes: TTimer;
    ViTestVes: TcxGridDBTableView;
    LeTestVes: TcxGridLevel;
    GrTestVes: TcxGrid;
    teTestVes: TdxMemData;
    teTestVesNum: TSmallintField;
    teTestVesArticul: TStringField;
    teTestVesBarCode: TStringField;
    teTestVesName: TStringField;
    teTestVesQuant: TFloatField;
    teTestVesPrice: TFloatField;
    teTestVesrSum: TFloatField;
    dsteTestVes: TDataSource;
    ViTestVesArticul: TcxGridDBColumn;
    ViTestVesBarCode: TcxGridDBColumn;
    ViTestVesName: TcxGridDBColumn;
    ViTestVesQuant: TcxGridDBColumn;
    ViTestVesPrice: TcxGridDBColumn;
    ViTestVesrSum: TcxGridDBColumn;
    amTestVes: TActionManager;
    acAddPos: TAction;
    Edit1: TEdit;
    acBar: TAction;
    Label4: TLabel;
    procedure cxButton3Click(Sender: TObject);
    procedure TimerVesTimer(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure acBarExecute(Sender: TObject);
    procedure Edit1KeyPress(Sender: TObject; var Key: Char);
    procedure acAddPosExecute(Sender: TObject);
    procedure teTestVesAfterPost(DataSet: TDataSet);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure cxButton1KeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmTestVes: TfmTestVes;
  bFirst: Boolean = False;
  bScanRead: Boolean = False;
  bAddVesPos:Boolean = False;

implementation

uses Dm, Un1, UnCash, MainCasher;

{$R *.dfm}

procedure TfmTestVes.cxButton3Click(Sender: TObject);
begin
  ModalResult:=mrOk;
end;

procedure TfmTestVes.TimerVesTimer(Sender: TObject);
Var n: Integer;
    Buff:array[1..3] of char;
    rVes,rVesPos,rVesAll:Real;
    S1,S2:String;
begin
  TimerVes.Enabled:=False;
  with dmC do
  begin
    sScanEv:='';
    iReadCount:=0;
    n:=0;
    rVesPos:=fmTestVes.Tag/1000;

    rVesAll:=0;
    teTestVes.First;
    while not teTestVes.Eof do
    begin
      rVesAll:=rVesAll+teTestVesQuant.AsFloat;
      teTestVes.Next;
    end;


    Buff[1]:=#$05;
    devCas.WriteBuf(Buff,1);
    delay(5);
    Buff[1]:=#$12;
    devCas.WriteBuf(Buff,1);

    while (iReadCount<38)and(n<10) do
    begin
      delay(50);
      inc(n);
    end;
    rVes:=0;
    if pos('kg',sScanEv)>0 then
    begin
      S1:=Copy(sScanEv,pos('kg',sScanEv)-8,8);
      S2:=S1[1];
      delete(S1,1,1);
      while pos(' ',S1)>0 do delete(S1,pos(' ',S1),1);
      while pos('.',S1)>0 do S1[pos('.',S1)]:=',';
      rVes:=StrToFloatDef(S1,0);
    end;
    if bFirst then
    begin
      if S2='S' then
      begin
//        CalcEdit1.Value:=rVes;
        bFirst:=False;
        Label1.Caption:='���� ��. ��� - '+S1;
        cxButton1.Tag:=RoundEx(rVes*1000);

        if abs(rVesPos-rVes)<=0.02 then cxButton1.Click;
        if abs(rVesAll-rVes)<=0.02 then cxButton1.Click;

      end else
      begin
        cxButton1.Tag:=0;
        Label1.Caption:='���� ��. ��� ������������.';
      end;
    end else
    begin
      if S2='S' then
      begin
        Label1.Caption:='���� ��. ��� - '+S1;
        cxButton1.Tag:=RoundEx(rVes*1000);

        if abs(rVesPos-rVes)<=0.02 then cxButton1.Click;  //������� , ���� ����������� ������������� ���� ��� ��������� �� 20 �
        if abs(rVesAll-rVes)<=0.02 then cxButton1.Click;
      end else
      begin
        cxButton1.Tag:=0;
        Label1.Caption:='���� ��. ��� ������������.';
      end;
    end;
  end;
  TimerVes.Enabled:=True;
end;

procedure TfmTestVes.FormShow(Sender: TObject);
begin
  cxButton1.Tag:=0;
  cxButton2.SetFocus;
  bFirst:=True;
  bScanRead:=False;

  if bScale then Label1.Caption:='���� ��' else Label1.Caption:='����� ���';
  with dmC do
  begin
    if bScale then //���� ������� ����
    begin
      TimerVes.Enabled:=True;
    end;
  end;

  cxButton2.SetFocus;
  bAddVesPos:=False;
  Label4.Caption:='';

//  GrTestVes.SetFocus;
end;

procedure TfmTestVes.cxButton1Click(Sender: TObject);
Var rVesVes,rVesAll:Real;
begin
  rVesAll:=0;
  teTestVes.First;
  while not teTestVes.Eof do
  begin
    rVesAll:=rVesAll+teTestVesQuant.AsFloat;
    teTestVes.Next;
  end;

  rVesVes:=cxButton1.Tag/1000;

//  if abs(rVesVes-rVesAll)>2 then   //10 ����� ������������
  if abs(rVesVes-rVesAll)>0.02 then   //10 �����
  begin
    fmMainCasher.Memo2.Lines.Add('������� ����������� ���� !!! ���������� ������ ����������.'); delay(10);
    SelPos.Quant:=0;
    ShowMessage('������� ����������� ���� !!! ���������� ������ ����������.');
    ModalResult:=mrNone;
  end else ModalResult:=mrOk;
end;

procedure TfmTestVes.cxButton2Click(Sender: TObject);
Var rVes:Real;
begin
  rVes:=cxButton1.Tag/1000;
  if rVes<0.002 then Exit;

  if teTestVes.RecordCount=1 then
  begin
    teTestVes.Edit;
    teTestVesQuant.AsFloat:=rVes;
    teTestVesrSum.AsFloat:=rv(rVes*teTestVesPrice.AsFloat);
    teTestVes.Post;
  end;

  fmMainCasher.Memo2.Lines.Add('��� ���� � �����.'); delay(10);
  ModalResult:=mrOk;
end;

procedure TfmTestVes.acBarExecute(Sender: TObject);
begin
  Edit1.Text:='';
  Edit1.SelectAll;
  Edit1.SetFocus;
end;

procedure TfmTestVes.Edit1KeyPress(Sender: TObject; var Key: Char);
Var sBar:ShortString;
    bCh:Byte;
    iNumPos:ShortInt;
begin
  bCh:=ord(Key);
  if (bCh = 13) then
  begin //���� ����������

    Edit1.SelectAll;
    Edit1.Enabled:=False;
    sBar:=Edit1.Text;
    Edit1.Text:='';


    if bAddVesPos then
    begin
      if FindBar(SOnlyDigit1(sBar)) then //��� ����� �� ��
      begin
        iNumPos:=teTestVesNum.AsInteger+1;

        teTestVes.Append;
        teTestVesNum.AsInteger:=iNumPos;
        teTestVesArticul.AsInteger:=StrToINtDef(SelPos.Articul,0);
        teTestVesBarCode.AsString:=SelPos.Bar;
        teTestVesName.AsString:=SelPos.Name;
        teTestVesQuant.AsFloat:=SelPos.Quant;
        teTestVesPrice.AsFloat:=SelPos.Price;
        teTestVesrSum.AsFloat:=SelPos.Quant*SelPos.Price;
        teTestVes.Post;

        GrTestVes.Visible:=True;
      end
      else Beep;

//      bAddVesPos:=False;  //����� ���� , ��� ������ ��� �� �������� ������ ������ �����
//      Label4.Caption:='';
    end;

    Key:=#0;
    Edit1.Enabled:=True;

    cxButton1.SetFocus;
  end;
end;

procedure TfmTestVes.acAddPosExecute(Sender: TObject);
begin
  bAddVesPos:=True;
  Label4.Caption:='+';
end;

procedure TfmTestVes.teTestVesAfterPost(DataSet: TDataSet);
begin
  if teTestVes.RecordCount=1 then cxButton2.Enabled:=True else cxButton2.Enabled:=False; 
end;

procedure TfmTestVes.FormKeyPress(Sender: TObject; var Key: Char);
Var bCh:Byte;
begin
  if (Key = '+') then
  begin //���� ����������
    acAddPos.Execute;
  end;
  Key:=#0;
end;

procedure TfmTestVes.cxButton1KeyPress(Sender: TObject; var Key: Char);
begin
  if (Key = '+') then
  begin //���� ����������
    acAddPos.Execute;
  end;
  Key:=#0;
end;

end.
