unit Vtb24un;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxControls, cxContainer, cxEdit, cxTextEdit, cxMemo, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons,
  cxCurrencyEdit, dxfBackGround, cxMaskEdit,
  cxSpinEdit, ActnList, XPStyleActnCtrls, ActnMan, ExtCtrls, ShellAPI;

type
  TfmVtb = class(TForm)
    cxButton2: TcxButton;
    cxButton5: TcxButton;
    cxButton6: TcxButton;
    dxfBackGround1: TdxfBackGround;
    cxButton9: TcxButton;
    Label1: TLabel;
    cxButton1: TcxButton;
    amBn: TActionManager;
    acExit: TAction;
    Panel1: TPanel;
    cxButton8: TcxButton;
    cxButton13: TcxButton;
    Memo1: TcxMemo;
    procedure FormShow(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure acMBCExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxButton8Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure cxButton9Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure prWMU(sStr:String);
  end;

function fVTB(iOp,iSum:INteger;Memo1:TcxMemo):Boolean;
function prClearAnsw:Boolean;
function fGetAnsw(Var sAnsw,sCheck:String):Boolean;
function BringWindowToForeground(Wnd: HWND): BOOL;

var
  fmVtb: TfmVtb;
  vCh: Variant;

implementation

uses Un1,UnCash,MessBN;

{$R *.dfm}

function BringWindowToForeground(Wnd: HWND): BOOL;
var
  hCurWnd, dwThreadID, dwCurThreadID: THandle;
  OldTimeOut: DWORD;
begin
  Result := False;
  if Wnd = 0 then Exit;
  if not IsWindow(Wnd) then Exit;
  SystemParametersInfo(SPI_GETFOREGROUNDLOCKTIMEOUT, 0, @OldTimeOut, 0);
  SystemParametersInfo(SPI_SETFOREGROUNDLOCKTIMEOUT, 0, Pointer(0), 0);
  SetWindowPos(Wnd, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE or SWP_NOSIZE);
  hCurWnd := GetForegroundWindow;
  while not Result do
  begin
    dwThreadID := GetCurrentThreadId;
    dwCurThreadID := GetWindowThreadProcessId(hCurWnd);
    AttachThreadInput(dwThreadID, dwCurThreadID, True);
    Result := SetForegroundWindow(Wnd);
    AttachThreadInput(dwThreadID, dwCurThreadID, False);
  end;
  SetWindowPos(Wnd, HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NOMOVE or SWP_NOSIZE);
  SystemParametersInfo(SPI_SETFOREGROUNDLOCKTIMEOUT, 0, Pointer(OldTimeOut), 0);
end;


procedure TfmVtb.prWMU(sStr:String);
begin
  Memo1.Lines.Add(sStr+' '+FormatDateTime('hh:nn:sss',now));
  prWriteLog('~~BnVTB;'+sStr);
 delay(10);
end;


function fGetAnsw(Var sAnsw,sCheck:String):Boolean;
Var f:TextFile;
    bErr:Boolean;
    strwk:String;
begin
  bErr:=True;
  sAnsw:='';
  sCheck:='';

  if FileExists(CommonSet.VTBPath+'rc.out') then
  begin
    try
      bErr:=False;
      assignfile(f,CommonSet.VTBPath+'rc.out');
      reset(f);
      readln(f,sAnsw);
      sAnsw:=Trim(sAnsw);
      bErr:=True;
    finally
      closefile(f);
    end;
  end;

  if bErr then
  begin
    if FileExists(CommonSet.VTBPath+'cheq.out') then
    begin
      try
        bErr:=False;
        assignfile(f,CommonSet.VTBPath+'cheq.out');
        reset(f);
        while not EOF(f) do
        begin
          readln(f,StrWk);
          sCheck:=sCheck+trim(StrWk)+#$0D;
        end;
        bErr:=True;
      finally
        closefile(f);
      end;
    end;
  end;
  result:=bErr;
end;

function prClearAnsw:Boolean;   //������ ��������� ����� ����� �� ����������� �������� �����
Var f:TextFile;
    bErr:Boolean;
begin
  bErr:=True; //��� ������

  if FileExists(CommonSet.VTBPath+'rc.out') then
  begin
    try
      bErr:=False;
      assignfile(f,CommonSet.VTBPath+'rc.out');
      rewrite(f);
      bErr:=True;
    finally
      closefile(f);
    end;
  end;

  if bErr then
  begin
    if FileExists(CommonSet.VTBPath+'cheq.out') then
    begin
      try
        bErr:=False;
        assignfile(f,CommonSet.VTBPath+'cheq.out');
        rewrite(f);
        bErr:=True;
      finally
        closefile(f);
      end;
    end;
  end;

  result:=bErr;
end;

function fVTB(iOp,iSum:INteger;Memo1:TcxMemo):Boolean;
var
  SEInfo: TShellExecuteInfo;
  ExitCode: DWORD;
  ExecuteFile, ParamString: string;
  iC:INteger;
  hWind: hWnd;
  sPar:String;

  procedure prWM(sStr:String);
  begin
    if Memo1<>nil then
    begin
      Memo1.Lines.Add(sStr+' '+FormatDateTime('hh:nn:sss',now));
      delay(10);
    end;
    prWriteLog('~~BnVTB;'+sStr);
  end;

begin
  if not FileExists(CurDir+'CommandLineTool.exe') then
  begin
    prWM(' ������ ��� �� �������� (CommandLineTool.exe). ���������� �������� ���������.');
    Result:=False;
    exit;
  end;

  if prClearAnsw=False then
  begin
    prWM(' ������ ������ � ������� ������. ���������� �������� ���������.');
    Result:=False;
    exit;
  end;

  hWind:=fmVtb.Handle;

  ExecuteFile := 'CommandLineTool.exe';
  ParamString:=' /o'+its(iOp);

  if iSum>0 then ParamString:=ParamString+' /a'+its(iSum);

  sPar:='CommandLineTool.exe'+ParamString;

  FillChar(SEInfo, SizeOf(SEInfo), 0);
  SEInfo.cbSize := SizeOf(TShellExecuteInfo);
  with SEInfo do
  begin
    fMask := SEE_MASK_NOCLOSEPROCESS;
    Wnd := Application.Handle;
    lpFile := PChar(ExecuteFile);
    lpParameters := PChar(ParamString);
  {StartInString specifies thename of the working
  directory.If ommited, the current
  directory is used.}
    lpDirectory := PChar(CurDir);
    nShow := SW_SHOWNORMAL;
  end;
  try
    prWM(' ������ ��������. ('+sPar+')');
    iC:=0;

//    if ShellExecute(SEInfo.Wnd,'open',SEInfo.lpFile,SEInfo.lpParameters,SW_SHOWNORMAL) then

    if ShellExecuteEx(@SEInfo) then
    begin
      repeat
        delay(100);
        GetExitCodeProcess(SEInfo.hProcess, ExitCode);
        inc(iC);
        if iC mod 50 = 0 then prWM('  ������� ���� '); //������ 5 ���
      until (ExitCode <> STILL_ACTIVE) or Application.Terminated;

      prWM(' ������� ��������. ');


//      if IsIconic(Application.Handle) then
//      ShowWindow(hWind, SW_SHOWNORMAL); delay(10);

      BringWindowToForeground(hWind);

//      SetForegroundWindow(hWind); delay(10);

//      SetForegroundWindow(Application.Handle);

      prWM(' .. ');

    end else  prWM('������ ������� ��������.');
    Result:=True;

  except
    prWM('������ ������� ��������..');
    result:=False;

    SetForegroundWindow(Application.Handle);
  end;
end;

procedure TfmVtb.FormShow(Sender: TObject);
Var StrWk:String;
begin
//�������
  Memo1.Clear;
  prWMU('');
  Str((iMoneyBn/100):9:2,StrWk); StrWk:=TrimStr(StrWk);
  Label1.Caption:='����� � ������ '+StrWk+'�.';

  if SberTabStop=1 then cxButton2.SetFocus;
  if SberTabStop=2 then cxButton5.SetFocus;
  if SberTabStop=3 then cxButton9.SetFocus;

end;

procedure TfmVtb.cxButton1Click(Sender: TObject);
begin
  ModalResult:=mrCancel;
  Close;
end;

procedure TfmVtb.acExitExecute(Sender: TObject);
begin
  ModalResult:=mrCancel;
end;

procedure TfmVtb.acMBCExecute(Sender: TObject);
begin
  cxButton2.Click;
end;

procedure TfmVtb.FormCreate(Sender: TObject);
begin
  Memo1.Clear;
  if CommonSet.BNManual=3 then
  begin
    if not FileExists(CurDir+'CommandLineTool.exe') then  prWMU(' ������ ��� �� �������� (CommandLineTool.exe). ') else prWMU(' ������ ��� ���� (CommandLineTool.exe). ');
  end;
end;

procedure TfmVtb.cxButton8Click(Sender: TObject);
begin
  Randomize;
  iAAnsw:=Random(9);
  fmMessBn.Label3.Caption:='��� ������ �������';
  fmMessBn.Label4.Caption:=its(iAAnsw);
  fmMessBn.Label4.Visible:=True;
  fmMessBn.ShowModal;

  if MessageDlg('�� ��������� ������ ���� �������� ��� ����������� � �����?',mtConfirmation, [mbYes, mbNo], 0) = mrNo then
  begin
    BnStr:='������ �� ������� '+#13+'��� ����������� � �����';

    prWMU('--------------------------------------');
    if length(BnStr)>0 then
    begin
      prWMU(BnStr);
    end;
    prWMU('--------------------------------------');
    prWMU('');
    prWMU('');
    ModalResult:=mrOk;
  end else ModalResult:=mrCancel;
end;



procedure TfmVtb.cxButton2Click(Sender: TObject);
Var sAnsw:String;
begin
  if fVTB(1,iMoneyBn,Memo1) then
  begin
    if fGetAnsw(sAnsw,BnStr) then  //���� ����� � ��� ��������
    begin
      prWMU('��� ������ �����������  '+sAnsw);
      prWMU('');
      prWMU('--------------------------------------');
      if length(BnStr)>0 then
      begin
        prWMU(BnStr);
      end;
      prWMU('--------------------------------------');
      prWMU('');
      prWMU('');
//      sAnsw:='000'; ��������
      if sAnsw='000' then
      begin
//        delay(3000);
        ModalResult:=mrOk;
      end;
    end else
    begin
      prWMU('������ ������� � ������ ������. �������� �����������.');
    end;
  end else prWMU('������. �������� �����������');
end;

procedure TfmVtb.cxButton5Click(Sender: TObject);
Var sAnsw:String;
begin
  if fVTB(3,iMoneyBn,Memo1) then
  begin
    if fGetAnsw(sAnsw,BnStr) then  //���� ����� � ��� ��������
    begin
      prWMU('��� ������ �����������  '+sAnsw);
      prWMU('');
      prWMU('--------------------------------------');
      if length(BnStr)>0 then
      begin
        prWMU(BnStr);
      end;
      prWMU('--------------------------------------');
      prWMU('');
      prWMU('');
      if sAnsw='000' then
      begin
//        delay(3000);
        ModalResult:=mrOk;
      end;
    end else
    begin
      prWMU('������ ������� � ������ ������. �������� �����������.');
    end;
  end else prWMU('������. �������� �����������');
end;

procedure TfmVtb.cxButton9Click(Sender: TObject);
Var sAnsw:String;
begin
//  if fVTB(12,0,Memo1) then //������� ����
//  if fVTB(12,0,Memo1) then //������� ����
//  if fVTB(7,0,Memo1) then //������� �����
  if fVTB(CommonSet.VTBCloseOp,0,Memo1) then //������� �����
  begin
    if fGetAnsw(sAnsw,BnStr) then  //���� ����� � ��� ��������
    begin
      prWMU('��� ������ �����������  '+sAnsw);
      prWMU('');
      prWMU('--------------------------------------');
      if length(BnStr)>0 then
      begin
        prWMU(BnStr);
      end;
      prWMU('--------------------------------------');
      prWMU('');
      prWMU('');
      if sAnsw='000' then
      begin
        prBnPrint;
      end;
    end else
    begin
      prWMU('������ ������� � ������ ������. �������� �����������.');
    end;
  end else prWMU('������. �������� �����������');
end;

procedure TfmVtb.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  SetForegroundWindow(Application.Handle);
end;

end.
