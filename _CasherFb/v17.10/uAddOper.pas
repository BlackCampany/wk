unit uAddOper;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, DBCtrls, Buttons, Menus, cxControls,
  cxContainer, cxEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxDBEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxGraphics;

type
  TfmAddOper = class(TForm)
    Panel1: TPanel;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    Bevel1: TBevel;
    Bevel2: TBevel;
    BitBtn1: TBitBtn;
    Bevel3: TBevel;
    Bevel4: TBevel;
    Label5: TLabel;
    Label6: TLabel;
    ComboBox3: TComboBox;
    Edit3: TEdit;
    ComboBox1: TcxLookupComboBox;
    ComboBox2: TcxLookupComboBox;
    procedure Button2Click(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure ComboBox2PropertiesChange(Sender: TObject);
  private
    { Private declarations }
  public
    Function prAddKey:Boolean;
    { Public declarations }
  end;

var
  fmAddOper: TfmAddOper;

implementation

uses Dm, uPressKey, Un1;

{$R *.dfm}

Function TfmAddOper.prAddKey:Boolean;
var iCode:Integer;
begin
  Result:=False;
  with dmC do
  begin
    // �������� ������� ������ ���������
    if CommonSet.Key_Code<>0 then
    begin

      iCode:=CommonSet.Key_Code+1000*(CommonSet.Key_Shift);
      if taCF_All.locate('KEY_CODE',iCode,[]) then
      begin
        if CommonSet.Key_Add then //�� ����������
        begin
          case taCF_AllKEY_STATUS.AsInteger of
          0:begin //��������
              taOperations.Locate('ID',taCF_AllID_OPERATIONS.AsInteger,[]);
              strwk:=' �������� - '+taOperationsName.AsString;
            end;
          1:begin //������ �������
              taClassif.Locate('ID',taCF_AllID_CLASSIF.AsInteger,[]);
              strwk:=' ������ �������������� ������� - '+taClassifName.AsString;
            end;
          2:begin //������
              strwk:=' ����� - ���:'+taCF_AllARTICUL.AsString;
            end;
          3:begin //������
              taDeparts.Locate('ID',taCF_AllID_DEPARTS.AsInteger,[]);
              strwk:=' ����� - '+taDepartsName.AsString;
            end;
          else
            strwk:='...';
          end;
          strwk:='������ ��������� ������ ��� ����������.'+strwk;
          ShowMessage(Strwk);
        end
        else
        begin
          //�������� �����, ����, barcode
          try

            trUpdate.StartTransaction;

            taCF_Operations.Edit;

            taCF_OperationsID_States.AsInteger:=ComboBox2.EditValue;
            taCF_OperationsBARCODE.AsString:=Edit3.Text;
            taCF_OperationsKEY_POSITION.AsInteger:=ComboBox3.ItemIndex;

            taCF_Operations.Post;
            delay(10);

            trUpdate.Commit;
            taCF_Operations.FullRefresh;
            taCF_Operations.locate('KEY_CODE',iCode,[]);


            Result:=True;
          except
            Result:=False;
          end;
        end;
      end
      else //������ ��������� ��� ���, ���������
      begin
        try
          trUpdate.StartTransaction;

          taCF_Operations.Append;

          taCF_OperationsKey_CODE.AsInteger:=CommonSet.Key_Code+1000*(CommonSet.Key_Shift);
          taCF_OperationsKey_Status.AsInteger:=0; //��������
          taCF_OperationsKey_Char.AsString:=CommonSet.Key_Char;
          taCF_OperationsID_Operations.AsInteger:=ComboBox1.EditValue;
          taCF_OperationsID_States.AsInteger:=ComboBox2.EditValue;
          taCF_OperationsBARCODE.AsString:=Edit3.Text;
          taCF_OperationsKEY_POSITION.AsInteger:=ComboBox3.ItemIndex;

          taCF_Operations.Post;
          delay(10);

          trUpdate.Commit;
          taCF_Operations.FullRefresh;
          taCF_Operations.locate('KEY_CODE',iCode,[]);

          CommonSEt.Key_Char:='';
          CommonSet.Key_Code:=0;
          CommonSet.Key_Shift:=0;

          Edit1.Text:='';
          Edit2.Text:='';

          Result:=True;

        except
            Result:=False;
        end;
      end;
      Button3.Enabled:=False;
    end
    else
      showmessage('���������� �������.');
  end;
end;

procedure TfmAddOper.Button2Click(Sender: TObject);
begin
  close;
end;

procedure TfmAddOper.ComboBox1Change(Sender: TObject);
begin
  Button3.Enabled:=True;
end;

procedure TfmAddOper.BitBtn1Click(Sender: TObject);
var sShift:TShiftState;
    CurSet:TCommonSet;
begin
  vSet:=False; //��������� ������� ���-�� ��� ��������
  fmPressKey.Edit1.Text:='';
  fmPressKey.Edit2.Text:='';
  fmPressKey.Label1.Caption:='';

  CurSet:=CommonSet;

  CommonSEt.Key_Char:='';
  CommonSet.Key_Code:=0;
  CommonSet.Key_Shift:=0;

  fmPressKey.ShowModal;

  if vSet then
  begin
    if CommonSet.Key_Code<>0 then
    begin

      Edit2.Text:=IntToStr(CommonSet.Key_Code);
      sShift:=[ssMiddle];
      if CommonSet.Key_Shift=1 then sShift:=[ssShift];
      if CommonSet.Key_Shift=2 then sShift:=[ssCtrl];

      Edit1.Text:=ShortCuttoText(ShortCut(CommonSet.Key_Code,sShift));
    end
    else
    Commonset:=CurSet;
  end
  else
    Commonset:=CurSet;
end;

procedure TfmAddOper.Button3Click(Sender: TObject);
begin
  prAddKey;
end;

procedure TfmAddOper.Button1Click(Sender: TObject);
begin
  if prAddKey then close;
end;

procedure TfmAddOper.ComboBox2PropertiesChange(Sender: TObject);
begin
  Button3.Enabled:=True;
  with dmC do
  begin
    if taStates.Locate('Id',ComboBox2.EditValue,[]) then
    begin
      Edit3.Text:=taStatesBarCode.AsString;
      ComboBox3.ItemIndex:=taStatesKey_Position.AsInteger;
    end;  
  end;
end;

end.
