object fmReservedCh: TfmReservedCh
  Left = 341
  Top = 314
  BorderStyle = bsDialog
  Caption = #1054#1090#1083#1086#1078#1077#1085#1085#1099#1077' '#1095#1077#1082#1080
  ClientHeight = 321
  ClientWidth = 648
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 275
    Width = 648
    Height = 46
    Align = alBottom
    BevelInner = bvLowered
    Color = 3106866
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 368
      Top = 8
      Width = 97
      Height = 29
      Caption = #1054#1082
      Default = True
      ModalResult = 1
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 500
      Top = 8
      Width = 93
      Height = 29
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      LookAndFeel.Kind = lfOffice11
    end
  end
  object GrRes: TcxGrid
    Left = 224
    Top = 12
    Width = 405
    Height = 245
    BorderWidth = 1
    TabOrder = 1
    LookAndFeel.Kind = lfOffice11
    object ViewRes: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dmC.dsquReservCh
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'DSUM'
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'RSUM'
          Column = ViewResRSUM
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnGrouping = False
      OptionsCustomize.ColumnSorting = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.CellSelect = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.UnselectFocusedRecordOnExit = False
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      Styles.Header = dmC.cxStyle4
      object ViewResNUMPOS: TcxGridDBColumn
        Caption = #8470' '#1087#1087
        DataBinding.FieldName = 'NUMPOS'
        Width = 29
      end
      object ViewResARTICUL: TcxGridDBColumn
        Caption = #1040#1088#1090#1080#1082#1091#1083
        DataBinding.FieldName = 'ARTICUL'
        Width = 41
      end
      object ViewResNAME: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'NAME'
        Styles.Content = dmC.cxStyle2
        Width = 140
      end
      object ViewResQUANT: TcxGridDBColumn
        Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086
        DataBinding.FieldName = 'QUANT'
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.DecimalPlaces = 3
        Properties.DisplayFormat = ',0.###;-,0.###'
        Styles.Content = dmC.cxStyle5
        Width = 50
      end
      object ViewResPRICE: TcxGridDBColumn
        Caption = #1062#1077#1085#1072
        DataBinding.FieldName = 'PRICE'
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Width = 54
      end
      object ViewResRSUM: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072
        DataBinding.FieldName = 'RSUM'
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Styles.Content = dmC.cxStyle5
        Width = 62
      end
    end
    object LeRes: TcxGridLevel
      GridView = ViewRes
    end
  end
  object GrRH: TcxGrid
    Left = 12
    Top = 12
    Width = 193
    Height = 245
    TabOrder = 2
    LookAndFeel.Kind = lfOffice11
    object ViewRH: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      OnFocusedRecordChanged = ViewRHFocusedRecordChanged
      DataController.DataSource = dmC.dsquResChH
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'RSUM'
          Column = ViewRHRSUM
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      object ViewRHCHECKNUM: TcxGridDBColumn
        Caption = #8470' '#1095#1077#1082#1072
        DataBinding.FieldName = 'CHECKNUM'
        Width = 59
      end
      object ViewRHRSUM: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072
        DataBinding.FieldName = 'RSUM'
        Styles.Content = dmC.cxStyle5
        Width = 102
      end
    end
    object LeRH: TcxGridLevel
      GridView = ViewRH
    end
  end
  object amRet: TActionManager
    Left = 248
    Top = 84
    StyleName = 'XP Style'
    object acExit: TAction
      Caption = 'acExit'
      ShortCut = 27
      SecondaryShortCuts.Strings = (
        'F10')
      OnExecute = acExitExecute
    end
  end
end
