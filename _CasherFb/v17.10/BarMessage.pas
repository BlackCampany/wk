unit BarMessage;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, jpeg, ExtCtrls, ActnList, XPStyleActnCtrls, ActnMan;

type
  TfmBarMessage = class(TForm)
    Image1: TImage;
    Label17: TLabel;
    amBarMess: TActionManager;
    acExit: TAction;
    Action1: TAction;
    Action2: TAction;
    Action3: TAction;
    Action4: TAction;
    Action5: TAction;
    Action6: TAction;
    Action7: TAction;
    Action8: TAction;
    Action9: TAction;
    Action10: TAction;
    Action11: TAction;
    Action12: TAction;
    Action13: TAction;
    Action14: TAction;
    Action15: TAction;
    Action16: TAction;
    procedure acExitExecute(Sender: TObject);
    procedure Action16Execute(Sender: TObject);
    procedure Action15Execute(Sender: TObject);
    procedure Action14Execute(Sender: TObject);
    procedure Action13Execute(Sender: TObject);
    procedure Action12Execute(Sender: TObject);
    procedure Action11Execute(Sender: TObject);
    procedure Action10Execute(Sender: TObject);
    procedure Action9Execute(Sender: TObject);
    procedure Action8Execute(Sender: TObject);
    procedure Action7Execute(Sender: TObject);
    procedure Action6Execute(Sender: TObject);
    procedure Action5Execute(Sender: TObject);
    procedure Action4Execute(Sender: TObject);
    procedure Action3Execute(Sender: TObject);
    procedure Action2Execute(Sender: TObject);
    procedure Action1Execute(Sender: TObject);
    procedure Image1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmBarMessage: TfmBarMessage;

implementation

{$R *.dfm}

procedure TfmBarMessage.acExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmBarMessage.Action16Execute(Sender: TObject);
begin
//
end;

procedure TfmBarMessage.Action15Execute(Sender: TObject);
begin
//
end;

procedure TfmBarMessage.Action14Execute(Sender: TObject);
begin
//
end;

procedure TfmBarMessage.Action13Execute(Sender: TObject);
begin
//
end;

procedure TfmBarMessage.Action12Execute(Sender: TObject);
begin
//
end;

procedure TfmBarMessage.Action11Execute(Sender: TObject);
begin
//
end;

procedure TfmBarMessage.Action10Execute(Sender: TObject);
begin
//
end;

procedure TfmBarMessage.Action9Execute(Sender: TObject);
begin
//
end;

procedure TfmBarMessage.Action8Execute(Sender: TObject);
begin
//
end;

procedure TfmBarMessage.Action7Execute(Sender: TObject);
begin
//
end;

procedure TfmBarMessage.Action6Execute(Sender: TObject);
begin
//
end;

procedure TfmBarMessage.Action5Execute(Sender: TObject);
begin
//
end;

procedure TfmBarMessage.Action4Execute(Sender: TObject);
begin
//
end;

procedure TfmBarMessage.Action3Execute(Sender: TObject);
begin
//
end;

procedure TfmBarMessage.Action2Execute(Sender: TObject);
begin
//
end;

procedure TfmBarMessage.Action1Execute(Sender: TObject);
begin
//
end;

procedure TfmBarMessage.Image1Click(Sender: TObject);
begin
  ModalResult:=mrOk;
end;

end.
