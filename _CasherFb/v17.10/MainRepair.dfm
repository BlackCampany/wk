object fmMainRepair: TfmMainRepair
  Left = 745
  Top = 176
  Width = 1166
  Height = 900
  Caption = #1042#1086#1089#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1095#1077#1082' '#1080#1079' '#1083#1086#1075#1086#1074' (17.01.2017)'
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 24
    Top = 16
    Width = 60
    Height = 13
    Caption = #1055#1091#1090#1100' '#1082' '#1073#1072#1079#1077
    Transparent = True
  end
  object Label2: TLabel
    Left = 24
    Top = 48
    Width = 26
    Height = 13
    Caption = #1044#1072#1090#1072
    Transparent = True
  end
  object Label3: TLabel
    Left = 24
    Top = 96
    Width = 37
    Height = 13
    Caption = #8470' '#1095#1077#1082#1072
    Transparent = True
  end
  object Label4: TLabel
    Left = 12
    Top = 72
    Width = 48
    Height = 13
    Caption = #8470' '#1089#1084#1077#1085#1099
    Transparent = True
  end
  object Label5: TLabel
    Left = 216
    Top = 48
    Width = 46
    Height = 13
    Caption = #8470' '#1082#1072#1089#1089#1099
    Transparent = True
  end
  object Label6: TLabel
    Left = 216
    Top = 72
    Width = 64
    Height = 13
    Caption = #1050#1086#1076' '#1082#1072#1089#1089#1080#1088#1072
    Transparent = True
  end
  object Label7: TLabel
    Left = 216
    Top = 96
    Width = 50
    Height = 13
    Caption = #1054#1087#1077#1088#1072#1094#1080#1103
    Transparent = True
  end
  object Label8: TLabel
    Left = 17
    Top = 166
    Width = 103
    Height = 13
    Caption = #1055#1091#1090#1100' '#1082' '#1073#1072#1079#1077' MCrystal'
    Enabled = False
    Transparent = True
    Visible = False
  end
  object Label9: TLabel
    Left = 296
    Top = 133
    Width = 7
    Height = 13
    Caption = #1057
    Transparent = True
  end
  object Label10: TLabel
    Left = 422
    Top = 133
    Width = 12
    Height = 13
    Caption = #1087#1086
    Transparent = True
  end
  object cxButtonEdit1: TcxButtonEdit
    Left = 104
    Top = 12
    Properties.Buttons = <
      item
        Default = True
        Kind = bkEllipsis
      end>
    TabOrder = 0
    Text = '192.168.0.1:E:\_Casher\DB\CASHERDB.GDB'
    Width = 277
  end
  object cxButton1: TcxButton
    Left = 396
    Top = 8
    Width = 85
    Height = 25
    Caption = #1054#1090#1082#1088#1099#1090#1100
    TabOrder = 1
    OnClick = cxButton1Click
    LookAndFeel.Kind = lfOffice11
  end
  object cxButton2: TcxButton
    Left = 492
    Top = 8
    Width = 85
    Height = 25
    Caption = #1047#1072#1082#1088#1099#1090#1100
    TabOrder = 2
    OnClick = cxButton2Click
    LookAndFeel.Kind = lfOffice11
  end
  object cxDateEdit1: TcxDateEdit
    Left = 72
    Top = 44
    TabOrder = 3
    Width = 125
  end
  object cxSpinEdit1: TcxSpinEdit
    Left = 72
    Top = 92
    TabOrder = 4
    Width = 85
  end
  object cxButton3: TcxButton
    Left = 420
    Top = 44
    Width = 137
    Height = 29
    Caption = #1055#1086#1083#1091#1095#1080#1090#1100' '#1080#1079' '#1083#1086#1075#1072
    TabOrder = 5
    OnClick = cxButton3Click
    LookAndFeel.Kind = lfOffice11
  end
  object cxButton4: TcxButton
    Left = 420
    Top = 80
    Width = 137
    Height = 29
    Caption = #1042#1086#1089#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1074' '#1073#1072#1079#1091
    TabOrder = 6
    OnClick = cxButton4Click
    LookAndFeel.Kind = lfOffice11
  end
  object Memo1: TcxMemo
    Left = 16
    Top = 192
    Lines.Strings = (
      'Memo1')
    TabOrder = 7
    Height = 169
    Width = 577
  end
  object cxSpinEdit2: TcxSpinEdit
    Left = 72
    Top = 68
    TabOrder = 8
    Width = 85
  end
  object cxSpinEdit3: TcxSpinEdit
    Left = 296
    Top = 44
    TabOrder = 9
    Width = 89
  end
  object GrCheck: TcxGrid
    Left = 16
    Top = 364
    Width = 1097
    Height = 457
    TabOrder = 10
    LookAndFeel.Kind = lfOffice11
    object ViewCh: TcxGridDBTableView
      PopupMenu = PopupMenu1
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dsteCheck
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'DSum'
          Column = ViewChDSum
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'RSum'
          Column = ViewChRSum
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      OptionsView.GroupFooters = gfAlwaysVisible
      object ViewChRecId: TcxGridDBColumn
        DataBinding.FieldName = 'RecId'
        Visible = False
      end
      object ViewChCashNum: TcxGridDBColumn
        DataBinding.FieldName = 'CashNum'
      end
      object ViewChCheckNum: TcxGridDBColumn
        DataBinding.FieldName = 'CheckNum'
      end
      object ViewChId: TcxGridDBColumn
        DataBinding.FieldName = 'Id'
      end
      object ViewChArticul: TcxGridDBColumn
        DataBinding.FieldName = 'Articul'
      end
      object ViewChBar: TcxGridDBColumn
        DataBinding.FieldName = 'Bar'
      end
      object ViewChQuant: TcxGridDBColumn
        DataBinding.FieldName = 'Quant'
      end
      object ViewChPrice: TcxGridDBColumn
        DataBinding.FieldName = 'Price'
      end
      object ViewChDProc: TcxGridDBColumn
        DataBinding.FieldName = 'DProc'
      end
      object ViewChDSum: TcxGridDBColumn
        DataBinding.FieldName = 'DSum'
      end
      object ViewChRSum: TcxGridDBColumn
        DataBinding.FieldName = 'RSum'
      end
      object ViewChCassir: TcxGridDBColumn
        DataBinding.FieldName = 'Cassir'
      end
      object ViewChOperation: TcxGridDBColumn
        DataBinding.FieldName = 'Operation'
      end
    end
    object LevelCh: TcxGridLevel
      GridView = ViewCh
    end
  end
  object cxSpinEdit4: TcxSpinEdit
    Left = 296
    Top = 68
    TabOrder = 11
    Width = 89
  end
  object cxComboBox1: TcxComboBox
    Left = 296
    Top = 92
    Properties.Items.Strings = (
      #1053#1072#1083
      #1041#1077#1079#1085#1072#1083)
    TabOrder = 12
    Text = #1053#1072#1083
    Width = 89
  end
  object cxButton5: TcxButton
    Left = 246
    Top = 156
    Width = 117
    Height = 29
    Caption = #1042#1086#1089#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1074' '#1073#1072#1079#1091' '#1087#1086' '#1074#1089#1077#1084#1091' '#1083#1086#1075#1091
    Enabled = False
    TabOrder = 13
    Visible = False
    OnClick = cxButton5Click
    LookAndFeel.Kind = lfOffice11
  end
  object cxButton6: TcxButton
    Left = 16
    Top = 156
    Width = 217
    Height = 29
    Caption = #1057#1087#1077#1094#1080#1072#1083#1100#1085#1086#1077' '#1074#1086#1089#1089#1090#1072#1085#1086#1074#1083#1077#1085#1080#1077
    TabOrder = 14
    Visible = False
    OnClick = cxButton6Click
    LookAndFeel.Kind = lfOffice11
  end
  object cxTextEdit1: TcxTextEdit
    Left = 86
    Top = 159
    Enabled = False
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 15
    Text = '\\192.168.3.150\k$\SCrystal\data'
    Visible = False
    Width = 277
  end
  object cxCheckBox1: TcxCheckBox
    Left = 274
    Top = 161
    Caption = #1048#1089#1087#1088#1072#1074#1083#1103#1090#1100
    TabOrder = 16
    Visible = False
    Width = 121
  end
  object cxButton7: TcxButton
    Left = 596
    Top = 44
    Width = 197
    Height = 25
    Caption = #1055#1088#1086#1074#1077#1088#1082#1072' '#1095#1077#1082#1086#1074' '#1087#1086' '#1083#1086#1075#1091
    TabOrder = 17
    OnClick = cxButton7Click
    LookAndFeel.Kind = lfOffice11
  end
  object GrCh: TcxGrid
    Left = 616
    Top = 84
    Width = 145
    Height = 273
    TabOrder = 18
    LookAndFeel.Kind = lfOffice11
    object ViCh: TcxGridDBTableView
      OnDblClick = ViChDblClick
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dsteCh
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      object ViChRecId: TcxGridDBColumn
        DataBinding.FieldName = 'RecId'
        Visible = False
      end
      object ViChNum: TcxGridDBColumn
        Caption = #8470' '#1095#1077#1082#1072
        DataBinding.FieldName = 'Num'
        Width = 111
      end
    end
    object LeCh: TcxGridLevel
      GridView = ViCh
    end
  end
  object cxButton8: TcxButton
    Left = 848
    Top = 40
    Width = 169
    Height = 33
    Caption = #1055#1088#1086#1074#1077#1088#1082#1072' '#1095#1077#1082#1086#1074' '#1087#1086' '#1089#1091#1084#1084#1077
    TabOrder = 19
    OnClick = cxButton8Click
    LookAndFeel.Kind = lfOffice11
  end
  object GrChSum: TcxGrid
    Left = 800
    Top = 84
    Width = 329
    Height = 273
    TabOrder = 20
    LookAndFeel.Kind = lfOffice11
    object ViewChSum: TcxGridDBTableView
      PopupMenu = PopupMenu2
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dsteChSum
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'rDif'
          Column = ViewChSumrDif
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'rSum'
          Column = ViewChSumrSum
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'rSumCh'
          Column = ViewChSumrSumCh
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.Footer = True
      OptionsView.GroupFooters = gfAlwaysVisible
      OnCustomization = ViewChSumCustomization
      object ViewChSumRecId: TcxGridDBColumn
        DataBinding.FieldName = 'RecId'
        Visible = False
      end
      object ViewChSumNum: TcxGridDBColumn
        DataBinding.FieldName = 'Num'
        Width = 64
      end
      object ViewChSumrSum: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1087#1086' '#1051#1086#1075#1091
        DataBinding.FieldName = 'rSum'
        Width = 95
      end
      object ViewChSumrSumCh: TcxGridDBColumn
        Caption = #1055#1086' '#1073#1072#1079#1077
        DataBinding.FieldName = 'rSumCh'
        Width = 79
      end
      object ViewChSumrDif: TcxGridDBColumn
        DataBinding.FieldName = 'rDif'
      end
    end
    object LeChSum: TcxGridLevel
      GridView = ViewChSum
    end
  end
  object cxButton9: TcxButton
    Left = 14
    Top = 127
    Width = 254
    Height = 25
    Caption = #1042#1086#1089#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1087#1086' '#1076#1080#1072#1087#1072#1079#1086#1085#1091
    TabOrder = 21
    OnClick = cxButton9Click
    LookAndFeel.Kind = lfOffice11
  end
  object cxSpinEdit5: TcxSpinEdit
    Left = 318
    Top = 129
    TabOrder = 22
    Width = 85
  end
  object cxSpinEdit6: TcxSpinEdit
    Left = 444
    Top = 129
    TabOrder = 23
    Width = 85
  end
  object teCheck: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 300
    Top = 400
    object teCheckCashNum: TIntegerField
      FieldName = 'CashNum'
    end
    object teCheckCheckNum: TIntegerField
      FieldName = 'CheckNum'
    end
    object teCheckId: TIntegerField
      FieldName = 'Id'
    end
    object teCheckArticul: TIntegerField
      FieldName = 'Articul'
    end
    object teCheckBar: TStringField
      FieldName = 'Bar'
    end
    object teCheckQuant: TFloatField
      FieldName = 'Quant'
      DisplayFormat = '0.000'
    end
    object teCheckPrice: TFloatField
      FieldName = 'Price'
      DisplayFormat = '0.00'
    end
    object teCheckDProc: TFloatField
      FieldName = 'DProc'
    end
    object teCheckDSum: TFloatField
      FieldName = 'DSum'
      DisplayFormat = '0.00'
    end
    object teCheckRSum: TFloatField
      FieldName = 'RSum'
      DisplayFormat = '0.00'
    end
    object teCheckCassir: TIntegerField
      FieldName = 'Cassir'
    end
    object teCheckOperation: TIntegerField
      FieldName = 'Operation'
    end
    object teCheckDepart: TIntegerField
      FieldName = 'Depart'
    end
  end
  object dsteCheck: TDataSource
    DataSet = teCheck
    Left = 300
    Top = 460
  end
  object dbCh: TpFIBDatabase
    DBName = 'localhost:E:\_CasherFb\DB\CASHERDB.GDB'
    DBParams.Strings = (
      'lc_ctype=WIN1251'
      'password=masterkey'
      'sql_role_name=SYSDBA'
      'user_name=SYSDBA')
    DefaultTransaction = trSel
    DefaultUpdateTransaction = trUpd
    SQLDialect = 3
    Timeout = 0
    DesignDBOptions = [ddoIsDefaultDatabase]
    WaitForRestoreConnect = 0
    Left = 48
    Top = 240
  end
  object trSel: TpFIBTransaction
    DefaultDatabase = dbCh
    TimeoutAction = TARollback
    Left = 120
    Top = 240
  end
  object trUpd: TpFIBTransaction
    DefaultDatabase = dbCh
    TimeoutAction = TARollback
    Left = 172
    Top = 240
  end
  object quCheck: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE CASHSAIL'
      'SET '
      '    CHDATE = :CHDATE,'
      '    ARTICUL = :ARTICUL,'
      '    BAR = :BAR,'
      '    CARDSIZE = :CARDSIZE,'
      '    QUANTITY = :QUANTITY,'
      '    PRICERUB = :PRICERUB,'
      '    DPROC = :DPROC,'
      '    DSUM = :DSUM,'
      '    TOTALRUB = :TOTALRUB,'
      '    CASHER = :CASHER,'
      '    DEPART = :DEPART,'
      '    OPERATION = :OPERATION'
      'WHERE'
      '    SHOPINDEX = :OLD_SHOPINDEX'
      '    and CASHNUMBER = :OLD_CASHNUMBER'
      '    and ZNUMBER = :OLD_ZNUMBER'
      '    and CHECKNUMBER = :OLD_CHECKNUMBER'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    CASHSAIL'
      'WHERE'
      '        SHOPINDEX = :OLD_SHOPINDEX'
      '    and CASHNUMBER = :OLD_CASHNUMBER'
      '    and ZNUMBER = :OLD_ZNUMBER'
      '    and CHECKNUMBER = :OLD_CHECKNUMBER'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO CASHSAIL('
      '    SHOPINDEX,'
      '    CASHNUMBER,'
      '    ZNUMBER,'
      '    CHECKNUMBER,'
      '    ID,'
      '    CHDATE,'
      '    ARTICUL,'
      '    BAR,'
      '    CARDSIZE,'
      '    QUANTITY,'
      '    PRICERUB,'
      '    DPROC,'
      '    DSUM,'
      '    TOTALRUB,'
      '    CASHER,'
      '    DEPART,'
      '    OPERATION'
      ')'
      'VALUES('
      '    :SHOPINDEX,'
      '    :CASHNUMBER,'
      '    :ZNUMBER,'
      '    :CHECKNUMBER,'
      '    :ID,'
      '    :CHDATE,'
      '    :ARTICUL,'
      '    :BAR,'
      '    :CARDSIZE,'
      '    :QUANTITY,'
      '    :PRICERUB,'
      '    :DPROC,'
      '    :DSUM,'
      '    :TOTALRUB,'
      '    :CASHER,'
      '    :DEPART,'
      '    :OPERATION'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    SHOPINDEX,'
      '    CASHNUMBER,'
      '    ZNUMBER,'
      '    CHECKNUMBER,'
      '    ID,'
      '    CHDATE,'
      '    ARTICUL,'
      '    BAR,'
      '    CARDSIZE,'
      '    QUANTITY,'
      '    PRICERUB,'
      '    DPROC,'
      '    DSUM,'
      '    TOTALRUB,'
      '    CASHER,'
      '    DEPART,'
      '    OPERATION'
      'FROM'
      '    CASHSAIL '
      'where(  '
      'SHOPINDEX = :ISHOP'
      'and CASHNUMBER = :ICASH'
      'and ZNUMBER = :IZ'
      'and CHECKNUMBER = :CHECK'
      '     ) and (     CASHSAIL.SHOPINDEX = :OLD_SHOPINDEX'
      '    and CASHSAIL.CASHNUMBER = :OLD_CASHNUMBER'
      '    and CASHSAIL.ZNUMBER = :OLD_ZNUMBER'
      '    and CASHSAIL.CHECKNUMBER = :OLD_CHECKNUMBER'
      '    and CASHSAIL.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    SHOPINDEX,'
      '    CASHNUMBER,'
      '    ZNUMBER,'
      '    CHECKNUMBER,'
      '    ID,'
      '    CHDATE,'
      '    ARTICUL,'
      '    BAR,'
      '    CARDSIZE,'
      '    QUANTITY,'
      '    PRICERUB,'
      '    DPROC,'
      '    DSUM,'
      '    TOTALRUB,'
      '    CASHER,'
      '    DEPART,'
      '    OPERATION'
      'FROM'
      '    CASHSAIL '
      'where '
      'SHOPINDEX = :ISHOP'
      'and CASHNUMBER = :ICASH'
      'and ZNUMBER = :IZ'
      'and CHECKNUMBER = :CHECK')
    Transaction = trSel
    Database = dbCh
    UpdateTransaction = trUpd
    AutoCommit = True
    Left = 236
    Top = 240
    poAskRecordCount = True
    object quCheckSHOPINDEX: TFIBSmallIntField
      FieldName = 'SHOPINDEX'
    end
    object quCheckCASHNUMBER: TFIBIntegerField
      FieldName = 'CASHNUMBER'
    end
    object quCheckZNUMBER: TFIBIntegerField
      FieldName = 'ZNUMBER'
    end
    object quCheckCHECKNUMBER: TFIBIntegerField
      FieldName = 'CHECKNUMBER'
    end
    object quCheckID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quCheckCHDATE: TFIBDateTimeField
      FieldName = 'CHDATE'
    end
    object quCheckARTICUL: TFIBStringField
      FieldName = 'ARTICUL'
      Size = 30
      EmptyStrToNull = True
    end
    object quCheckBAR: TFIBStringField
      FieldName = 'BAR'
      Size = 15
      EmptyStrToNull = True
    end
    object quCheckCARDSIZE: TFIBStringField
      FieldName = 'CARDSIZE'
      Size = 10
      EmptyStrToNull = True
    end
    object quCheckQUANTITY: TFIBFloatField
      FieldName = 'QUANTITY'
    end
    object quCheckPRICERUB: TFIBFloatField
      FieldName = 'PRICERUB'
    end
    object quCheckDPROC: TFIBFloatField
      FieldName = 'DPROC'
    end
    object quCheckDSUM: TFIBFloatField
      FieldName = 'DSUM'
    end
    object quCheckTOTALRUB: TFIBFloatField
      FieldName = 'TOTALRUB'
    end
    object quCheckCASHER: TFIBIntegerField
      FieldName = 'CASHER'
    end
    object quCheckDEPART: TFIBIntegerField
      FieldName = 'DEPART'
    end
    object quCheckOPERATION: TFIBSmallIntField
      FieldName = 'OPERATION'
    end
  end
  object quCheckP: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE CASHPAY'
      'SET '
      '    CASHER = :CASHER,'
      '    CHECKSUM = :CHECKSUM,'
      '    PAYSUM = :PAYSUM,'
      '    DSUM = :DSUM,'
      '    DBAR = :DBAR,'
      '    COUNTPOS = :COUNTPOS,'
      '    CHDATE = :CHDATE,'
      '    OPER = :OPER'
      'WHERE'
      '    SHOPINDEX = :OLD_SHOPINDEX'
      '    and CASHNUMBER = :OLD_CASHNUMBER'
      '    and ZNUMBER = :OLD_ZNUMBER'
      '    and CHECKNUMBER = :OLD_CHECKNUMBER'
      '    and PAYMENT = :OLD_PAYMENT'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    CASHPAY'
      'WHERE'
      '        SHOPINDEX = :OLD_SHOPINDEX'
      '    and CASHNUMBER = :OLD_CASHNUMBER'
      '    and ZNUMBER = :OLD_ZNUMBER'
      '    and CHECKNUMBER = :OLD_CHECKNUMBER'
      '    and PAYMENT = :OLD_PAYMENT'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO CASHPAY('
      '    SHOPINDEX,'
      '    CASHNUMBER,'
      '    ZNUMBER,'
      '    CHECKNUMBER,'
      '    CASHER,'
      '    CHECKSUM,'
      '    PAYSUM,'
      '    DSUM,'
      '    DBAR,'
      '    COUNTPOS,'
      '    CHDATE,'
      '    PAYMENT,'
      '    OPER'
      ')'
      'VALUES('
      '    :SHOPINDEX,'
      '    :CASHNUMBER,'
      '    :ZNUMBER,'
      '    :CHECKNUMBER,'
      '    :CASHER,'
      '    :CHECKSUM,'
      '    :PAYSUM,'
      '    :DSUM,'
      '    :DBAR,'
      '    :COUNTPOS,'
      '    :CHDATE,'
      '    :PAYMENT,'
      '    :OPER'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    SHOPINDEX,'
      '    CASHNUMBER,'
      '    ZNUMBER,'
      '    CHECKNUMBER,'
      '    CASHER,'
      '    CHECKSUM,'
      '    PAYSUM,'
      '    DSUM,'
      '    DBAR,'
      '    COUNTPOS,'
      '    CHDATE,'
      '    PAYMENT,'
      '    OPER'
      'FROM'
      '    CASHPAY '
      ''
      'where(  '
      'SHOPINDEX = :ISHOP'
      'and CASHNUMBER = :ICASH'
      'and ZNUMBER = :IZ'
      'and CHECKNUMBER = :CHECK'
      '     ) and (     CASHPAY.SHOPINDEX = :OLD_SHOPINDEX'
      '    and CASHPAY.CASHNUMBER = :OLD_CASHNUMBER'
      '    and CASHPAY.ZNUMBER = :OLD_ZNUMBER'
      '    and CASHPAY.CHECKNUMBER = :OLD_CHECKNUMBER'
      '    and CASHPAY.PAYMENT = :OLD_PAYMENT'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    SHOPINDEX,'
      '    CASHNUMBER,'
      '    ZNUMBER,'
      '    CHECKNUMBER,'
      '    CASHER,'
      '    CHECKSUM,'
      '    PAYSUM,'
      '    DSUM,'
      '    DBAR,'
      '    COUNTPOS,'
      '    CHDATE,'
      '    PAYMENT,'
      '    OPER'
      'FROM'
      '    CASHPAY '
      ''
      'where '
      'SHOPINDEX = :ISHOP'
      'and CASHNUMBER = :ICASH'
      'and ZNUMBER = :IZ'
      'and CHECKNUMBER = :CHECK')
    Transaction = trSel
    Database = dbCh
    UpdateTransaction = trUpd
    AutoCommit = True
    Left = 288
    Top = 240
    poAskRecordCount = True
    object quCheckPSHOPINDEX: TFIBSmallIntField
      FieldName = 'SHOPINDEX'
    end
    object quCheckPCASHNUMBER: TFIBIntegerField
      FieldName = 'CASHNUMBER'
    end
    object quCheckPZNUMBER: TFIBIntegerField
      FieldName = 'ZNUMBER'
    end
    object quCheckPCHECKNUMBER: TFIBIntegerField
      FieldName = 'CHECKNUMBER'
    end
    object quCheckPCASHER: TFIBIntegerField
      FieldName = 'CASHER'
    end
    object quCheckPCHECKSUM: TFIBFloatField
      FieldName = 'CHECKSUM'
    end
    object quCheckPPAYSUM: TFIBFloatField
      FieldName = 'PAYSUM'
    end
    object quCheckPDSUM: TFIBFloatField
      FieldName = 'DSUM'
    end
    object quCheckPDBAR: TFIBStringField
      FieldName = 'DBAR'
      Size = 22
      EmptyStrToNull = True
    end
    object quCheckPCOUNTPOS: TFIBSmallIntField
      FieldName = 'COUNTPOS'
    end
    object quCheckPCHDATE: TFIBDateTimeField
      FieldName = 'CHDATE'
    end
    object quCheckPPAYMENT: TFIBSmallIntField
      FieldName = 'PAYMENT'
    end
    object quCheckPOPER: TFIBSmallIntField
      FieldName = 'OPER'
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 376
    Top = 400
    object Excel1: TMenuItem
      Caption = #1069#1082#1089#1087#1086#1088#1090' '#1074' Excel'
      OnClick = Excel1Click
    end
  end
  object teCh: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 692
    Top = 112
    object teChNum: TIntegerField
      FieldName = 'Num'
    end
  end
  object dsteCh: TDataSource
    DataSet = teCh
    Left = 648
    Top = 168
  end
  object teChSum: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 996
    Top = 144
    object teChSumrSum: TFloatField
      FieldName = 'rSum'
      DisplayFormat = '0.00'
    end
    object teChSumrSumCh: TFloatField
      FieldName = 'rSumCh'
      DisplayFormat = '0.00'
    end
    object teChSumNum: TIntegerField
      FieldName = 'Num'
    end
    object teChSumrDif: TFloatField
      FieldName = 'rDif'
      DisplayFormat = '0.00'
    end
  end
  object dsteChSum: TDataSource
    DataSet = teChSum
    Left = 1000
    Top = 192
  end
  object quChSum: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE CASHSAIL'
      'SET '
      '    CHDATE = :CHDATE,'
      '    ARTICUL = :ARTICUL,'
      '    BAR = :BAR,'
      '    CARDSIZE = :CARDSIZE,'
      '    QUANTITY = :QUANTITY,'
      '    PRICERUB = :PRICERUB,'
      '    DPROC = :DPROC,'
      '    DSUM = :DSUM,'
      '    TOTALRUB = :TOTALRUB,'
      '    CASHER = :CASHER,'
      '    DEPART = :DEPART,'
      '    OPERATION = :OPERATION'
      'WHERE'
      '    SHOPINDEX = :OLD_SHOPINDEX'
      '    and CASHNUMBER = :OLD_CASHNUMBER'
      '    and ZNUMBER = :OLD_ZNUMBER'
      '    and CHECKNUMBER = :OLD_CHECKNUMBER'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    CASHSAIL'
      'WHERE'
      '        SHOPINDEX = :OLD_SHOPINDEX'
      '    and CASHNUMBER = :OLD_CASHNUMBER'
      '    and ZNUMBER = :OLD_ZNUMBER'
      '    and CHECKNUMBER = :OLD_CHECKNUMBER'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO CASHSAIL('
      '    SHOPINDEX,'
      '    CASHNUMBER,'
      '    ZNUMBER,'
      '    CHECKNUMBER,'
      '    ID,'
      '    CHDATE,'
      '    ARTICUL,'
      '    BAR,'
      '    CARDSIZE,'
      '    QUANTITY,'
      '    PRICERUB,'
      '    DPROC,'
      '    DSUM,'
      '    TOTALRUB,'
      '    CASHER,'
      '    DEPART,'
      '    OPERATION'
      ')'
      'VALUES('
      '    :SHOPINDEX,'
      '    :CASHNUMBER,'
      '    :ZNUMBER,'
      '    :CHECKNUMBER,'
      '    :ID,'
      '    :CHDATE,'
      '    :ARTICUL,'
      '    :BAR,'
      '    :CARDSIZE,'
      '    :QUANTITY,'
      '    :PRICERUB,'
      '    :DPROC,'
      '    :DSUM,'
      '    :TOTALRUB,'
      '    :CASHER,'
      '    :DEPART,'
      '    :OPERATION'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    SHOPINDEX,'
      '    CASHNUMBER,'
      '    ZNUMBER,'
      '    CHECKNUMBER,'
      '    ID,'
      '    CHDATE,'
      '    ARTICUL,'
      '    BAR,'
      '    CARDSIZE,'
      '    QUANTITY,'
      '    PRICERUB,'
      '    DPROC,'
      '    DSUM,'
      '    TOTALRUB,'
      '    CASHER,'
      '    DEPART,'
      '    OPERATION'
      'FROM'
      '    CASHSAIL '
      'where(  '
      'SHOPINDEX = :ISHOP'
      'and CASHNUMBER = :ICASH'
      'and ZNUMBER = :IZ'
      'and CHECKNUMBER = :CHECK'
      '     ) and (     CASHSAIL.SHOPINDEX = :OLD_SHOPINDEX'
      '    and CASHSAIL.CASHNUMBER = :OLD_CASHNUMBER'
      '    and CASHSAIL.ZNUMBER = :OLD_ZNUMBER'
      '    and CASHSAIL.CHECKNUMBER = :OLD_CHECKNUMBER'
      '    and CASHSAIL.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select SUM(totalrub) as RSUM'
      'from cashsail'
      'where shopindex=1'
      'and  cashnumber=3'
      'and  znumber=890'
      'and  checknumber=2')
    Transaction = trSel
    Database = dbCh
    UpdateTransaction = trUpd
    AutoCommit = True
    Left = 236
    Top = 304
    poAskRecordCount = True
    object quChSumRSUM: TFIBFloatField
      FieldName = 'RSUM'
    end
  end
  object PopupMenu2: TPopupMenu
    Left = 776
    Top = 451
    object MenuItem1: TMenuItem
      Caption = #1069#1082#1089#1087#1086#1088#1090' '#1074' Excel'
      OnClick = Excel1Click
    end
  end
  object quChMax: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE CASHSAIL'
      'SET '
      '    CHDATE = :CHDATE,'
      '    ARTICUL = :ARTICUL,'
      '    BAR = :BAR,'
      '    CARDSIZE = :CARDSIZE,'
      '    QUANTITY = :QUANTITY,'
      '    PRICERUB = :PRICERUB,'
      '    DPROC = :DPROC,'
      '    DSUM = :DSUM,'
      '    TOTALRUB = :TOTALRUB,'
      '    CASHER = :CASHER,'
      '    DEPART = :DEPART,'
      '    OPERATION = :OPERATION'
      'WHERE'
      '    SHOPINDEX = :OLD_SHOPINDEX'
      '    and CASHNUMBER = :OLD_CASHNUMBER'
      '    and ZNUMBER = :OLD_ZNUMBER'
      '    and CHECKNUMBER = :OLD_CHECKNUMBER'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    CASHSAIL'
      'WHERE'
      '        SHOPINDEX = :OLD_SHOPINDEX'
      '    and CASHNUMBER = :OLD_CASHNUMBER'
      '    and ZNUMBER = :OLD_ZNUMBER'
      '    and CHECKNUMBER = :OLD_CHECKNUMBER'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO CASHSAIL('
      '    SHOPINDEX,'
      '    CASHNUMBER,'
      '    ZNUMBER,'
      '    CHECKNUMBER,'
      '    ID,'
      '    CHDATE,'
      '    ARTICUL,'
      '    BAR,'
      '    CARDSIZE,'
      '    QUANTITY,'
      '    PRICERUB,'
      '    DPROC,'
      '    DSUM,'
      '    TOTALRUB,'
      '    CASHER,'
      '    DEPART,'
      '    OPERATION'
      ')'
      'VALUES('
      '    :SHOPINDEX,'
      '    :CASHNUMBER,'
      '    :ZNUMBER,'
      '    :CHECKNUMBER,'
      '    :ID,'
      '    :CHDATE,'
      '    :ARTICUL,'
      '    :BAR,'
      '    :CARDSIZE,'
      '    :QUANTITY,'
      '    :PRICERUB,'
      '    :DPROC,'
      '    :DSUM,'
      '    :TOTALRUB,'
      '    :CASHER,'
      '    :DEPART,'
      '    :OPERATION'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    SHOPINDEX,'
      '    CASHNUMBER,'
      '    ZNUMBER,'
      '    CHECKNUMBER,'
      '    ID,'
      '    CHDATE,'
      '    ARTICUL,'
      '    BAR,'
      '    CARDSIZE,'
      '    QUANTITY,'
      '    PRICERUB,'
      '    DPROC,'
      '    DSUM,'
      '    TOTALRUB,'
      '    CASHER,'
      '    DEPART,'
      '    OPERATION'
      'FROM'
      '    CASHSAIL '
      'where(  '
      'SHOPINDEX = :ISHOP'
      'and CASHNUMBER = :ICASH'
      'and ZNUMBER = :IZ'
      'and CHECKNUMBER = :CHECK'
      '     ) and (     CASHSAIL.SHOPINDEX = :OLD_SHOPINDEX'
      '    and CASHSAIL.CASHNUMBER = :OLD_CASHNUMBER'
      '    and CASHSAIL.ZNUMBER = :OLD_ZNUMBER'
      '    and CASHSAIL.CHECKNUMBER = :OLD_CHECKNUMBER'
      '    and CASHSAIL.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select MAX(checknumber) as MAXCH'
      'from cashsail'
      'where shopindex=1'
      'and  cashnumber=3'
      'and  znumber=890'
      '')
    Transaction = trSel
    Database = dbCh
    UpdateTransaction = trUpd
    AutoCommit = True
    Left = 316
    Top = 304
    poAskRecordCount = True
    object quChMaxMAXCH: TFIBIntegerField
      FieldName = 'MAXCH'
    end
  end
end
