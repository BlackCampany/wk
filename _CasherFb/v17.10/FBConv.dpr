// JCL_DEBUG_EXPERT_GENERATEJDBG OFF
// JCL_DEBUG_EXPERT_INSERTJDBG OFF
// JCL_DEBUG_EXPERT_DELETEMAPFILE OFF
program FBConv;

uses
  Forms,
  MainFbConv in 'MainFbConv.pas' {fmMainFBConv},
  Dm in 'Dm.pas' {dmC: TDataModule},
  HistoryTr in 'HistoryTr.pas' {fmHistoryTr},
  Un1 in 'Un1.pas',
  uDBFB in 'uDBFB.pas' {dmFB: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfmMainFBConv, fmMainFBConv);
  Application.CreateForm(TdmC, dmC);
  Application.CreateForm(TfmHistoryTr, fmHistoryTr);
  Application.CreateForm(TdmFB, dmFB);
  Application.ShowMainForm:=FALSE;
  Application.Run;
end.
