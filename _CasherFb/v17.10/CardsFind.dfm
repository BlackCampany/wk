object fmCardsFind: TfmCardsFind
  Left = 421
  Top = 4
  AlphaBlend = True
  AlphaBlendValue = 225
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = #1042#1099#1073#1086#1088' '#1090#1086#1074#1072#1088#1072' '#1080#1079' '#1089#1087#1080#1089#1082#1072
  ClientHeight = 629
  ClientWidth = 391
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 610
    Width = 391
    Height = 19
    Panels = <
      item
        Width = 300
      end
      item
        Width = 50
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 392
    Height = 201
    BevelInner = bvLowered
    TabOrder = 1
    object CardsTree: TTreeView
      Left = 2
      Top = 2
      Width = 388
      Height = 197
      Align = alClient
      Images = dmC.imState
      Indent = 19
      TabOrder = 0
      OnExpanding = CardsTreeExpanding
      OnKeyPress = CardsTreeKeyPress
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 216
    Width = 385
    Height = 225
    BevelInner = bvLowered
    TabOrder = 2
    object GridCF: TcxGrid
      Left = 2
      Top = 2
      Width = 381
      Height = 221
      Align = alClient
      TabOrder = 0
      LookAndFeel.Kind = lfFlat
      object ViewCF: TcxGridDBTableView
        OnKeyPress = ViewCFKeyPress
        NavigatorButtons.ConfirmDelete = False
        DataController.DataSource = dmC.dsCards
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsCustomize.ColumnFiltering = False
        OptionsCustomize.ColumnGrouping = False
        OptionsCustomize.ColumnSorting = False
        OptionsSelection.CellSelect = False
        OptionsView.GroupByBox = False
        Styles.Header = dmC.cxStyle4
        object ViewCFARTICUL: TcxGridDBColumn
          Caption = #1040#1088#1090#1080#1082#1091#1083
          DataBinding.FieldName = 'ARTICUL'
          Width = 46
        end
        object ViewCFNAME: TcxGridDBColumn
          Caption = #1053#1072#1079#1074#1072#1085#1080#1077
          DataBinding.FieldName = 'NAME'
          Width = 214
        end
        object ViewCFMESURIMENT: TcxGridDBColumn
          Caption = #1045#1076'.'#1080#1079#1084
          DataBinding.FieldName = 'MESURIMENT'
          Width = 39
        end
        object ViewCFPRICE_RUB: TcxGridDBColumn
          Caption = #1062#1077#1085#1072
          DataBinding.FieldName = 'PRICE_RUB'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.DisplayFormat = ',0.00;-,0.00'
          Width = 56
        end
      end
      object LevelCF: TcxGridLevel
        GridView = ViewCF
      end
    end
  end
  object amCF: TActionManager
    Left = 48
    Top = 16
    StyleName = 'XP Style'
    object acExit: TAction
      Caption = 'acExit'
      ShortCut = 121
      OnExecute = acExitExecute
    end
  end
end
