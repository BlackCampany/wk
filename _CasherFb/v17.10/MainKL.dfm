object fmMainKL: TfmMainKL
  Left = 82
  Top = 141
  Width = 882
  Height = 569
  Caption = #1058#1077#1089#1090' '#1082#1086#1085#1090#1088#1086#1083#1100#1085#1086#1081' '#1083#1077#1085#1090#1099
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 516
    Width = 874
    Height = 19
    Panels = <
      item
        Width = 300
      end
      item
        Width = 50
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 400
    Width = 874
    Height = 116
    Align = alBottom
    BevelInner = bvLowered
    TabOrder = 1
    object Memo1: TMemo
      Left = 16
      Top = 8
      Width = 513
      Height = 97
      Lines.Strings = (
        'Memo1')
      TabOrder = 0
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 874
    Height = 57
    Align = alTop
    BevelInner = bvLowered
    Color = 14079702
    TabOrder = 2
    object Label1: TLabel
      Left = 16
      Top = 36
      Width = 58
      Height = 13
      Caption = #1055#1091#1090#1100' '#1076#1086' '#1041#1044
    end
    object Label2: TLabel
      Left = 16
      Top = 12
      Width = 94
      Height = 13
      Caption = #1055#1091#1090#1100' '#1076#1086' '#1083#1086#1075'-'#1092#1072#1081#1083#1072
    end
    object ButtonEdit1: TcxButtonEdit
      Left = 128
      Top = 8
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.OnButtonClick = ButtonEdit1PropertiesButtonClick
      Style.BorderStyle = ebsFlat
      TabOrder = 0
      Text = 'ButtonEdit1'
      Width = 249
    end
    object ButtonEdit2: TcxButtonEdit
      Left = 128
      Top = 32
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.OnButtonClick = ButtonEdit2PropertiesButtonClick
      Style.BorderStyle = ebsFlat
      TabOrder = 1
      Text = 'ButtonEdit2'
      Width = 249
    end
    object Button1: TcxButton
      Left = 424
      Top = 4
      Width = 75
      Height = 41
      Caption = #1054#1090#1082#1088#1099#1090#1100
      TabOrder = 2
      OnClick = Button1Click
      LookAndFeel.Kind = lfFlat
    end
    object Button2: TcxButton
      Left = 672
      Top = 4
      Width = 75
      Height = 41
      Caption = #1042#1099#1093#1086#1076
      TabOrder = 3
      OnClick = Button2Click
      Glyph.Data = {
        5E040000424D5E04000000000000360000002800000012000000130000000100
        18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
        8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
        0000CED3D6848684848684848684848684848684848684848684848684848684
        848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
        7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
        FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
        FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
        007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
        FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
        00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
        494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
        00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000}
      LookAndFeel.Kind = lfFlat
    end
    object cxButton1: TcxButton
      Left = 520
      Top = 4
      Width = 97
      Height = 41
      Caption = #1048#1089#1082#1072#1090#1100' '#1088#1072#1079#1083#1080#1095#1080#1077
      TabOrder = 4
      OnClick = cxButton1Click
      LookAndFeel.Kind = lfFlat
    end
    object SpinEdit1: TcxSpinEdit
      Left = 800
      Top = 24
      TabOrder = 5
      Width = 57
    end
  end
  object GridKl: TcxGrid
    Left = 0
    Top = 57
    Width = 368
    Height = 343
    Align = alLeft
    TabOrder = 3
    LookAndFeel.Kind = lfOffice11
    object ViewKl: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dsKl
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = ',0.00'#1088#39'.'#39';-,0.00'#1088#39'.'#39
          Kind = skSum
          FieldName = 'Price'
          Column = ViewKlPrice
        end
        item
          Format = ',0.00'#1088#39'.'#39';-,0.00'#1088#39'.'#39
          Kind = skSum
          FieldName = 'SumD'
          Column = ViewKlSumD
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.DragDropText = True
      OptionsBehavior.GoToNextCellOnEnter = True
      OptionsCustomize.ColumnGrouping = False
      OptionsCustomize.ColumnSorting = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.CellMultiSelect = True
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      OptionsView.GroupRowStyle = grsOffice11
      object ViewKlOper: TcxGridDBColumn
        Caption = #1054#1087#1077#1088#1072#1094#1080#1103
        DataBinding.FieldName = 'Oper'
      end
      object ViewKlCheckNum: TcxGridDBColumn
        Caption = #8470' '#1095#1077#1082#1072
        DataBinding.FieldName = 'CheckNum'
        Width = 42
      end
      object ViewKlArticul: TcxGridDBColumn
        Caption = #1040#1088#1090#1080#1082#1091#1083
        DataBinding.FieldName = 'Articul'
      end
      object ViewKlQuant: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086
        DataBinding.FieldName = 'Quant'
      end
      object ViewKlPrice: TcxGridDBColumn
        Caption = #1062#1077#1085#1072
        DataBinding.FieldName = 'Price'
        PropertiesClassName = 'TcxCurrencyEditProperties'
      end
      object ViewKlSumD: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1089#1082#1080#1076#1082#1080
        DataBinding.FieldName = 'SumD'
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Width = 48
      end
    end
    object LevelKl: TcxGridLevel
      GridView = ViewKl
    end
  end
  object GridCS: TcxGrid
    Left = 371
    Top = 57
    Width = 503
    Height = 343
    Align = alClient
    TabOrder = 4
    LookAndFeel.Kind = lfOffice11
    object ViewCS: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dsCS
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = ',0.00'#1088#39'.'#39';-,0.00'#1088#39'.'#39
          Kind = skSum
          FieldName = 'TOTALRUB'
          Column = ViewCSTOTALRUB
        end
        item
          Format = ',0.00'#1088#39'.'#39';-,0.00'#1088#39'.'#39
          Kind = skSum
          FieldName = 'DSUM'
          Column = ViewCSDSUM
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.DragDropText = True
      OptionsCustomize.ColumnGrouping = False
      OptionsCustomize.ColumnSorting = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.CellMultiSelect = True
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      object ViewCSCHECKNUMBER: TcxGridDBColumn
        Caption = #8470' '#1095#1077#1082#1072
        DataBinding.FieldName = 'CHECKNUMBER'
        Width = 40
      end
      object ViewCSID: TcxGridDBColumn
        Caption = 'Id'
        DataBinding.FieldName = 'ID'
        Width = 27
      end
      object ViewCSARTICUL: TcxGridDBColumn
        Caption = #1040#1088#1090#1080#1082#1091#1083
        DataBinding.FieldName = 'ARTICUL'
        Width = 83
      end
      object ViewCSQUANTITY: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086
        DataBinding.FieldName = 'QUANTITY'
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.DecimalPlaces = 3
        Properties.DisplayFormat = ',0.000;-,0.000'
      end
      object ViewCSPRICERUB: TcxGridDBColumn
        Caption = #1062#1077#1085#1072
        DataBinding.FieldName = 'PRICERUB'
      end
      object ViewCSTOTALRUB: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1074#1089#1077#1075#1086
        DataBinding.FieldName = 'TOTALRUB'
        PropertiesClassName = 'TcxCurrencyEditProperties'
      end
      object ViewCSDPROC: TcxGridDBColumn
        Caption = '% '#1089#1082#1080#1076#1082#1080
        DataBinding.FieldName = 'DPROC'
        Width = 25
      end
      object ViewCSDSUM: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1089#1082
        DataBinding.FieldName = 'DSUM'
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Width = 39
      end
      object ViewCSOPERATION: TcxGridDBColumn
        Caption = #1054#1087#1077#1088#1072#1094#1080#1103
        DataBinding.FieldName = 'OPERATION'
      end
    end
    object LevelCS: TcxGridLevel
      GridView = ViewCS
    end
  end
  object RxSplitter1: TRxSplitter
    Left = 368
    Top = 57
    Width = 3
    Height = 343
    ControlFirst = GridKl
    Align = alLeft
  end
  object OpenDialog1: TOpenDialog
    DefaultExt = '*.log'
    Title = #1042#1099#1073#1077#1088#1080#1090#1077' '#1092#1072#1081#1083' '#1080#1089#1090#1086#1088#1080#1080
    Left = 312
    Top = 8
  end
  object OpenDialog2: TOpenDialog
    DefaultExt = '*.gdb'
    Left = 352
    Top = 8
  end
  object taKl: TClientDataSet
    Aggregates = <>
    FileName = 'Kl.cds'
    Params = <>
    Left = 32
    Top = 200
    object taKlOper: TStringField
      FieldName = 'Oper'
      Size = 10
    end
    object taKlCheckNum: TIntegerField
      FieldName = 'CheckNum'
    end
    object taKlArticul: TStringField
      FieldName = 'Articul'
      Size = 10
    end
    object taKlQuant: TFloatField
      FieldName = 'Quant'
    end
    object taKlPrice: TCurrencyField
      FieldName = 'Price'
    end
    object taKlSumD: TCurrencyField
      FieldName = 'SumD'
    end
  end
  object dsKl: TDataSource
    DataSet = taKl
    Left = 80
    Top = 200
  end
  object dmC: TpFIBDatabase
    DBName = 'Master:C:\Work\CASHERDB.GDB'
    DBParams.Strings = (
      'lc_ctype=WIN1251'
      'password=masterkey'
      'sql_role_name=SYSDBA'
      'user_name=SYSDBA')
    DefaultTransaction = trSelect
    DefaultUpdateTransaction = trUpdate
    SQLDialect = 3
    Timeout = 0
    DesignDBOptions = [ddoIsDefaultDatabase, ddoStoreConnected]
    WaitForRestoreConnect = 0
    Left = 424
    Top = 128
  end
  object trSelect: TpFIBTransaction
    DefaultDatabase = dmC
    TimeoutAction = TARollback
    Left = 424
    Top = 184
  end
  object trUpdate: TpFIBTransaction
    DefaultDatabase = dmC
    TimeoutAction = TARollback
    Left = 424
    Top = 248
  end
  object taCS: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT'
      '    CHECKNUMBER,'
      '    ID,'
      '    ARTICUL,'
      '    QUANTITY,'
      '    PRICERUB,'
      '    DPROC,'
      '    DSUM,'
      '    TOTALRUB,'
      '    OPERATION'
      'FROM'
      '    CASHSAIL '
      'Where'
      '    CHDATE>=:DATEB'
      '    and CHDATE<:DATEE')
    Transaction = trSelect
    Database = dmC
    UpdateTransaction = trUpdate
    Left = 496
    Top = 128
    object taCSCHECKNUMBER: TFIBIntegerField
      FieldName = 'CHECKNUMBER'
    end
    object taCSID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taCSARTICUL: TFIBStringField
      FieldName = 'ARTICUL'
      Size = 30
      EmptyStrToNull = True
    end
    object taCSQUANTITY: TFIBFloatField
      FieldName = 'QUANTITY'
    end
    object taCSPRICERUB: TFIBFloatField
      FieldName = 'PRICERUB'
    end
    object taCSDPROC: TFIBFloatField
      FieldName = 'DPROC'
    end
    object taCSDSUM: TFIBFloatField
      FieldName = 'DSUM'
    end
    object taCSTOTALRUB: TFIBFloatField
      FieldName = 'TOTALRUB'
    end
    object taCSOPERATION: TFIBSmallIntField
      FieldName = 'OPERATION'
    end
  end
  object dsCS: TDataSource
    DataSet = taCS
    Left = 560
    Top = 128
  end
end
