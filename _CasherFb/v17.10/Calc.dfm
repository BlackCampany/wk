object fmCalc: TfmCalc
  Left = 390
  Top = 346
  BorderIcons = []
  BorderStyle = bsDialog
  ClientHeight = 250
  ClientWidth = 309
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 309
    Height = 249
    Align = alTop
    BevelInner = bvLowered
    Color = 8080936
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object Label1: TLabel
      Left = 168
      Top = 116
      Width = 56
      Height = 13
      Caption = #1042#1077#1089#1086#1074' '#1053#1045#1058
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentColor = False
      ParentFont = False
      Transparent = True
    end
    object Button1: TButton
      Left = 184
      Top = 24
      Width = 35
      Height = 25
      Caption = 'Button1'
      TabOrder = 16
      OnClick = Button1Click
    end
    object CalcEdit1: TcxCalcEdit
      Left = 24
      Top = 16
      TabStop = False
      EditValue = 1.000000000000000000
      ParentFont = False
      Properties.Alignment.Horz = taRightJustify
      Properties.Precision = 5
      Properties.ReadOnly = True
      Properties.QuickClose = True
      Properties.OnValidate = CalcEdit1PropertiesValidate
      Style.BorderStyle = ebsFlat
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = 8552960
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsBold]
      Style.LookAndFeel.Kind = lfFlat
      Style.ButtonTransparency = ebtHideInactive
      Style.IsFontAssigned = True
      StyleDisabled.LookAndFeel.Kind = lfFlat
      StyleFocused.LookAndFeel.Kind = lfFlat
      StyleHot.LookAndFeel.Kind = lfFlat
      TabOrder = 0
      Width = 121
    end
    object cxButton1: TcxButton
      Left = 16
      Top = 144
      Width = 40
      Height = 40
      Caption = '1'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 1
      TabStop = False
      OnClick = cxButton1Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfFlat
    end
    object cxButton2: TcxButton
      Left = 64
      Top = 144
      Width = 40
      Height = 40
      Caption = '2'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 2
      TabStop = False
      OnClick = cxButton2Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfFlat
    end
    object cxButton3: TcxButton
      Left = 112
      Top = 144
      Width = 40
      Height = 40
      Caption = '3'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 3
      TabStop = False
      OnClick = cxButton3Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfFlat
    end
    object cxButton4: TcxButton
      Left = 16
      Top = 96
      Width = 40
      Height = 40
      Caption = '4'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 4
      TabStop = False
      OnClick = cxButton4Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfFlat
    end
    object cxButton5: TcxButton
      Left = 64
      Top = 96
      Width = 40
      Height = 40
      Caption = '5'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 5
      TabStop = False
      OnClick = cxButton5Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfFlat
    end
    object cxButton6: TcxButton
      Left = 112
      Top = 96
      Width = 40
      Height = 40
      Caption = '6'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 6
      TabStop = False
      OnClick = cxButton6Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfFlat
    end
    object cxButton7: TcxButton
      Left = 16
      Top = 48
      Width = 40
      Height = 40
      Caption = '7'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 7
      TabStop = False
      OnClick = cxButton7Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfFlat
    end
    object cxButton8: TcxButton
      Left = 64
      Top = 48
      Width = 40
      Height = 40
      Caption = '8'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 8
      TabStop = False
      OnClick = cxButton8Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfFlat
    end
    object cxButton9: TcxButton
      Left = 112
      Top = 48
      Width = 40
      Height = 40
      Caption = '9'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 9
      TabStop = False
      OnClick = cxButton9Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfFlat
    end
    object cxButton10: TcxButton
      Left = 16
      Top = 192
      Width = 40
      Height = 40
      Caption = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 10
      TabStop = False
      OnClick = cxButton10Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfFlat
    end
    object cxButton11: TcxButton
      Left = 112
      Top = 192
      Width = 40
      Height = 40
      Caption = ','
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 11
      TabStop = False
      OnClick = cxButton11Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfFlat
    end
    object cxButton12: TcxButton
      Left = 168
      Top = 16
      Width = 65
      Height = 40
      Action = Action13
      Caption = '+1'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 12
      TabStop = False
      Colors.Default = 8421440
      Colors.Normal = 8421440
      Colors.Pressed = 13619102
      LookAndFeel.Kind = lfFlat
    end
    object cxButton13: TcxButton
      Left = 236
      Top = 16
      Width = 65
      Height = 40
      Action = Action14
      Caption = '-1'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 13
      TabStop = False
      Colors.Default = 8421631
      Colors.Normal = 8421631
      Colors.Pressed = 4605695
      LookAndFeel.Kind = lfFlat
    end
    object cxButton14: TcxButton
      Left = 168
      Top = 192
      Width = 133
      Height = 40
      Caption = 'Ok'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ModalResult = 1
      ParentFont = False
      TabOrder = 14
      OnKeyPress = cxButton14KeyPress
      Colors.Default = 8421440
      Colors.Normal = 8421440
      Colors.Pressed = 13619102
      LookAndFeel.Kind = lfFlat
    end
    object cxButton15: TcxButton
      Left = 236
      Top = 64
      Width = 65
      Height = 40
      Action = Action15
      Caption = #1057#1073#1088#1086#1089
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 15
      TabStop = False
      Colors.Default = 8421631
      Colors.Normal = 8421631
      Colors.Pressed = 4605695
      LookAndFeel.Kind = lfFlat
    end
    object cxButton18: TcxButton
      Left = 168
      Top = 140
      Width = 133
      Height = 37
      Caption = #1042#1077#1089' '#1089' '#1074#1077#1089#1086#1074
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 17
      TabStop = False
      OnClick = cxButton18Click
      Colors.Default = 16750899
      Colors.Normal = 16750899
      Colors.Pressed = 16762508
      LookAndFeel.Kind = lfFlat
    end
  end
  object amCalc: TActionManager
    Left = 36
    Top = 132
    StyleName = 'XP Style'
    object Action1: TAction
      Caption = 'Action1'
      ShortCut = 49
      SecondaryShortCuts.Strings = (
        '1'
        '1')
      OnExecute = Action1Execute
    end
    object Action2: TAction
      Caption = 'Action2'
      ShortCut = 50
      SecondaryShortCuts.Strings = (
        '2'
        '2')
      OnExecute = Action2Execute
    end
    object Action3: TAction
      Caption = 'Action3'
      ShortCut = 51
      SecondaryShortCuts.Strings = (
        '3'
        '3')
      OnExecute = Action3Execute
    end
    object Action4: TAction
      Caption = 'Action4'
      ShortCut = 52
      SecondaryShortCuts.Strings = (
        '4'
        '4')
      OnExecute = Action4Execute
    end
    object Action5: TAction
      Caption = 'Action5'
      ShortCut = 53
      SecondaryShortCuts.Strings = (
        '5'
        '5')
      OnExecute = Action5Execute
    end
    object Action6: TAction
      Caption = 'Action6'
      ShortCut = 54
      SecondaryShortCuts.Strings = (
        '6'
        '6')
      OnExecute = Action6Execute
    end
    object Action7: TAction
      Caption = 'Action7'
      ShortCut = 55
      SecondaryShortCuts.Strings = (
        '7'
        '7')
      OnExecute = Action7Execute
    end
    object Action8: TAction
      Caption = 'Action8'
      ShortCut = 56
      SecondaryShortCuts.Strings = (
        '8'
        '8')
      OnExecute = Action8Execute
    end
    object Action9: TAction
      Caption = 'Action9'
      ShortCut = 57
      SecondaryShortCuts.Strings = (
        '9'
        '9')
      OnExecute = Action9Execute
    end
    object Action10: TAction
      Caption = 'Action10'
      ShortCut = 48
      SecondaryShortCuts.Strings = (
        '0'
        '0')
      OnExecute = Action10Execute
    end
    object Action11: TAction
      Caption = 'Action11'
      ShortCut = 188
      SecondaryShortCuts.Strings = (
        #1073
        '.')
      OnExecute = Action11Execute
    end
    object Action12: TAction
      Caption = 'Action12'
      ShortCut = 8
      OnExecute = Action12Execute
    end
    object Action13: TAction
      Caption = 'Action13'
      ShortCut = 107
      OnExecute = Action13Execute
    end
    object Action14: TAction
      Caption = 'Action14'
      ShortCut = 109
      OnExecute = Action14Execute
    end
    object Action15: TAction
      Caption = 'Action15'
      ShortCut = 113
      OnExecute = Action15Execute
    end
    object acExit: TAction
      Caption = 'acExit'
      ShortCut = 121
      SecondaryShortCuts.Strings = (
        'Esc')
      OnExecute = acExitExecute
    end
    object acScaner: TAction
      Caption = 'acScaner'
      ShortCut = 123
      OnExecute = acScanerExecute
    end
    object Action16: TAction
      Caption = 'Action16'
      ShortCut = 190
      OnExecute = Action16Execute
    end
    object Action17: TAction
      Caption = 'Action17'
      ShortCut = 111
      OnExecute = Action17Execute
    end
  end
  object TimerVes: TTimer
    Enabled = False
    Interval = 250
    OnTimer = TimerVesTimer
    Left = 72
    Top = 192
  end
end
