object fmVtb: TfmVtb
  Left = 831
  Top = 128
  BorderStyle = bsDialog
  Caption = #1041#1077#1079#1085#1072#1083' '#1042#1058#1041'24'
  ClientHeight = 350
  ClientWidth = 441
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 164
    Width = 60
    Height = 20
    Caption = #1057#1091#1084#1084#1072' '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object cxButton2: TcxButton
    Left = 16
    Top = 8
    Width = 313
    Height = 41
    Caption = #1052#1045#1046#1044#1059#1053#1040#1056#1054#1044#1053#1040#1071' '#1041#1040#1053#1050#1054#1042#1057#1050#1040#1071' '#1050#1040#1056#1058#1040
    Default = True
    TabOrder = 0
    OnClick = cxButton2Click
    Colors.Default = 16752706
    Colors.Normal = 16765348
    Colors.Hot = 16754386
    Colors.Pressed = 16754386
    LookAndFeel.Kind = lfUltraFlat
  end
  object cxButton5: TcxButton
    Left = 16
    Top = 96
    Width = 313
    Height = 25
    Caption = #1042#1086#1079#1074#1088#1072#1090' '#1052#1041#1050
    TabOrder = 1
    OnClick = cxButton5Click
    Colors.Default = 16752706
    Colors.Normal = 16765348
    Colors.Hot = 16754386
    Colors.Pressed = 16754386
    LookAndFeel.Kind = lfUltraFlat
  end
  object cxButton6: TcxButton
    Left = 320
    Top = 172
    Width = 105
    Height = 17
    Caption = #1054#1090#1084#1077#1085#1072' '#1052#1041#1050'  '
    TabOrder = 2
    Visible = False
    Colors.Default = 16752706
    Colors.Normal = 16765348
    Colors.Hot = 16754386
    Colors.Pressed = 16754386
    LookAndFeel.Kind = lfUltraFlat
  end
  object cxButton9: TcxButton
    Left = 16
    Top = 132
    Width = 313
    Height = 25
    Caption = #1047#1072#1082#1088#1099#1090#1080#1077' '#1076#1085#1103
    TabOrder = 3
    OnClick = cxButton9Click
    Colors.Default = 16752706
    Colors.Normal = 16765348
    Colors.Pressed = 15365376
    LookAndFeel.Kind = lfUltraFlat
  end
  object cxButton1: TcxButton
    Left = 344
    Top = 8
    Width = 81
    Height = 161
    Caption = #1054#1090#1084#1077#1085#1072
    ModalResult = 2
    TabOrder = 4
    OnClick = cxButton1Click
    Colors.Default = 16752706
    Colors.Normal = 16765348
    Colors.Hot = 16754386
    Colors.Pressed = 16754386
    LookAndFeel.Kind = lfUltraFlat
  end
  object Panel1: TPanel
    Left = 12
    Top = 188
    Width = 417
    Height = 157
    BevelInner = bvLowered
    TabOrder = 5
    object Memo1: TcxMemo
      Left = 2
      Top = 2
      Align = alClient
      Lines.Strings = (
        'Memo1')
      TabOrder = 0
      Height = 153
      Width = 413
    end
  end
  object cxButton8: TcxButton
    Left = 16
    Top = 60
    Width = 313
    Height = 25
    Caption = #1041#1077#1079' '#1072#1074#1090#1086#1088#1080#1079#1072#1094#1080#1080
    TabOrder = 6
    OnClick = cxButton8Click
    Colors.Default = 4210943
    Colors.Normal = 16765348
    Colors.Hot = 16754386
    Colors.Pressed = 16754386
    LookAndFeel.Kind = lfUltraFlat
  end
  object cxButton13: TcxButton
    Left = 320
    Top = 200
    Width = 105
    Height = 17
    Caption = #1055#1077#1095#1072#1090#1100' '#1087#1086#1089#1083'. '#1095#1077#1082#1072
    TabOrder = 7
    Visible = False
    Colors.Default = 16752706
    Colors.Normal = 16765348
    Colors.Hot = 16754386
    Colors.Pressed = 16754386
    LookAndFeel.Kind = lfUltraFlat
  end
  object dxfBackGround1: TdxfBackGround
    BkColor.BeginColor = 16767449
    BkColor.EndColor = clBlue
    BkColor.FillStyle = fsVert
    BkAnimate.Speed = 700
    Left = 172
    Top = 216
  end
  object amBn: TActionManager
    Left = 252
    Top = 216
    StyleName = 'XP Style'
    object acExit: TAction
      Caption = 'acExit'
      ShortCut = 121
      OnExecute = acExitExecute
    end
  end
end
