unit uPre;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, jpeg, cxControls, cxContainer, cxEdit, cxTextEdit,
  ActnList, XPStyleActnCtrls, ActnMan, StdCtrls, dxfLabel;

type
  TfmPre = class(TForm)
    Image1: TImage;
    Edit1: TcxTextEdit;
    amRegist: TActionManager;
    acReg: TAction;
    acStopExit: TAction;
    Label1: TLabel;
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Edit1KeyPress(Sender: TObject; var Key: Char);
    procedure acRegExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure acStopExitExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;



var
  fmPre: TfmPre;

implementation

uses Un1, Dm, MainCasher;

{$R *.dfm}

procedure TfmPre.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
//var bCh:Byte;
//    iShift:Integer;
begin
{  bCh:=ord(Key);
  //121,27,18
  iShift:=0;
  if Shift=[ssShift]  then iShift:=1;
  if Shift=[ssCtrl]   then iShift:=2;
  if Shift=[ssAlt]  then iShift:=3;

  if iShift>0 then StrPre:='' else
  begin
    if bCh = 13 then
    begin //���� ����������
      If FindPers(StrPre) then
      begin
        //��� ������ ������������ ����������
        fmMainCasher.Panel3.Visible:=True;
        with dmC do
        begin
          if taCheck.Active=False then
          begin
            taCheck.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
            taCheck.Active:=True;
          end;
          taCheck.First;
          while not taCheck.Eof do
          begin
            CountSec:=0;
            fmMainCasher.Timer2.Enabled:=False;
            if fmMainCasher.GridCh.Height<329 then fmMainCasher.GridCh.Height:=fmMainCasher.GridCh.Height+18;
            taCheck.Next;
          end;
        end;


        fmMainCasher.Label13.Caption:=Person.Name;
        CalcSum;
        CountSec:=0;
        fmMainCasher.Timer2.Enabled:=True;

        StrPre:='';
        close;
      end
      else
      begin
        StrPre:='';
        Person.Id:=0;
        Person.Name:='';
      end;
    end
    else //�����
    if bCh <> 123 then StrPre:=StrPre+Chr(bCh);   //F12 ����������
  end;}
end;

procedure TfmPre.Edit1KeyPress(Sender: TObject; var Key: Char);
Var sBar:String;
    bCh:Byte;
begin
  bCh:=ord(Key);
  if bCh = 13 then
  begin //���� ����������
//    showmessage('Enter');
    sBar:=SOnlyDigit(Edit1.Text);
    Edit1.Text:='';
    if FindPers(sBar) then
    begin
      //��� �� ��������� - ����� ��������
//      prWriteLog('&!Pers;'+IntToStr(Person.Id)+';'+Person.Name+';');
      if pos('�����',Person.Name)>0 then prWriteLog('&!PersA;'+IntToStr(Person.Id)+';'+Person.Name+';')
      else prWriteLog('&!Pers;'+IntToStr(Person.Id)+';'+Person.Name+';');

      //��� ������ ������������ ����������
      fmMainCasher.Panel3.Visible:=True;
      if CanDo('prCashRep') then fmMainCasher.cxButton1.Visible:=True else fmMainCasher.cxButton1.Visible:=False;
      with dmC do
      begin
        if taCheck.Active=False then
        begin
          taCheck.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
          taCheck.Active:=True;
        end;
        taCheck.First;
        while not taCheck.Eof do
        begin
          CountSec:=0;
          fmMainCasher.Timer2.Enabled:=False;

          if fmMainCasher.tag=0 then
            if fmMainCasher.GridCh.Height<329 then fmMainCasher.GridCh.Height:=fmMainCasher.GridCh.Height+18;
          if (fmMainCasher.tag=1)or(fmMainCasher.tag=17) then
            if fmMainCasher.GridCh.Height<280 then fmMainCasher.GridCh.Height:=fmMainCasher.GridCh.Height+18;

          taCheck.Next;
        end;
      end;

      fmMainCasher.prSetVisibleAll(True);      //���� ������ ������������

      fmMainCasher.Label13.Caption:=Person.Name;
      CalcSum;
      CountSec:=0;
      fmMainCasher.Timer2.Enabled:=True;
      close;
    end
    else
    begin
      Person.Id:=0;
      Person.Name:='';
    end;
  end;
end;

procedure TfmPre.acRegExecute(Sender: TObject);
begin
  Edit1.Text:='';
  Edit1.SetFocus;
end;

procedure TfmPre.FormShow(Sender: TObject);
begin
  Edit1.Text:='';
  Edit1.SetFocus;
//  WindowState := wsMinimized;
//  WindowState := wsNormal;
end;

procedure TfmPre.acStopExitExecute(Sender: TObject);
begin
// ������ �� ���� ������
end;

procedure TfmPre.FormCreate(Sender: TObject);
begin
  Image1.Picture.LoadFromFile(CurDir+'Pre.jpg');
  Image1.Visible:=True;
end;

end.
