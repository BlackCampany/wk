unit HistoryTr;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxControls, cxContainer, cxEdit, cxTextEdit, cxMemo;

type
  TfmHistoryTr = class(TForm)
    Memo1: TcxMemo;
    procedure FormCreate(Sender: TObject);
    procedure Memo1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmHistoryTr: TfmHistoryTr;

implementation

uses Un1, MainStdConv, MainFbConv;

{$R *.dfm}

procedure TfmHistoryTr.FormCreate(Sender: TObject);
begin
  Memo1.Clear;
end;

procedure TfmHistoryTr.Memo1Click(Sender: TObject);
begin
  if AlphaBlend then AlphaBlend:=False
  else AlphaBlend:=True;
end;

end.
