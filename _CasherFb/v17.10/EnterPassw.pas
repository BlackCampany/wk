unit EnterPassw;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, cxControls, cxContainer, cxEdit, cxTextEdit, StdCtrls,
  Menus, cxLookAndFeelPainters, cxButtons, FileUtil, dxfProgressBar,
  ComCtrls;

type
  TfmEnterPassw = class(TForm)
    Label1: TLabel;
    cxTextEdit1: TcxTextEdit;
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    ProgressBar1: TProgressBar;
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmEnterPassw: TfmEnterPassw;

implementation

uses Un1, Dm, UnCash;

{$R *.dfm}
{$I dll.inc}

procedure TfmEnterPassw.cxButton2Click(Sender: TObject);
begin
  Close;
end;

procedure TfmEnterPassw.cxButton1Click(Sender: TObject);
Var NameF2,NameF1:String;
begin
  //���������
//  delay(3000);
  if cxTextEdit1.Text<>CommonSet.Passw then
  begin
    Label1.Caption:='������ ��������. ��������� �������.';
  end else
  begin
    cxButton1.Enabled:=False;
    cxButton2.Enabled:=False;

//    showmessage('Ok1');
    //�������
    //1.����������� ����
    //2.�������� ����
    //3.������� ����������

    //1.����������� ����
    Label1.Caption:='�����, ���� ����������� ������.'; Delay(10);
    with dmC do
    begin
      taPersonal.Active:=False;
      taRClassif.Active:=False;
      quFuncList.Active:=False;
      taFuncList.Active:=False;
      CasherDb.Close;
    end;

    NameF1:=DBName;
    delete(NameF1,1,pos(':',NameF1));

    NameF2:=DBName;
    delete(NameF2,pos('CasherDb.gdb',NameF2),12);
    delete(NameF2,pos('casherdb.gdb',NameF2),12);
    delete(NameF2,1,pos(':',NameF2));
    NameF2:=NameF2+FormatDateTime('yyyy-mm-dd hh-nn-ss',Now)+'.gdb';
    if FileExists(NameF1) then
    begin
      ProgressBar1.Visible:=True; ProgressBar1.Position:=0;
      Delay(10);
      CopyFile(NameF1,NameF2,ProgressBar1);
    end;
    //������� ����
    with dmC do
    begin
      CasherDb.DBName:=DBName;
      CasherDb.Open;

      taPersonal.Active:=True;
      taRClassif.Active:=True;
      quFuncList.Active:=True;
      taFuncList.Active:=True;
        //��� �������� ������ ���, ����� ������ ��� �������
      prClearCheck.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
      prClearCheck.ExecProc;

      //2.�������� ����
      Label1.Caption:='�����, ���� ������ ����.'; Delay(10);
      ProgressBar1.Visible:=True;
      ProgressBar1.Position:=0; Delay(10);
      quDel.SQL.Clear;
      quDel.SQL.Add('Delete from cashsail');
      quDel.ExecQuery;
      ProgressBar1.Position:=30; Delay(100);
      quDel.SQL.Clear;
      quDel.SQL.Add('Delete from cashpay');
      quDel.ExecQuery;
      ProgressBar1.Position:=60; Delay(100);
      quDel.SQL.Clear;
      quDel.SQL.Add('Delete from bntr');
      quDel.ExecQuery;
      ProgressBar1.Position:=100; Delay(500);

      //3.������� ����������

      Label1.Caption:='�����, ���� ���������������� ��.'; Delay(10);
      if OpenZ then
      begin
        Label1.Caption:='�������� ����� � ��������� ��������.'; Delay(2000);
      end else
      begin
        ProgressBar1.Visible:=True;
        ProgressBar1.Position:=0; Delay(10);
        SetParamDoc($19A6,$0000,15);
        ProgressBar1.Position:=30; Delay(100);
        //��������� ���������
        SetTail('      ','          ������� �� �������.','','');
        ProgressBar1.Position:=60; Delay(100);
        //���� ��������
        SetPayment(0,'��������',0,1,0,7,'1.00');
        SetPayment(1,'��������� �����',0,0,0,7,'1.00');
        SetPayment(2,'���������� �����',0,0,0,7,'1.00');
        ProgressBar1.Position:=100; Delay(500);
      end;
      ProgressBar1.Visible:=False; delay(10);
    end;
    cxButton1.Enabled:=True;
    cxButton2.Enabled:=True;
    close;
  end;
end;

procedure TfmEnterPassw.FormShow(Sender: TObject);
begin
  cxTextEdit1.SetFocus;
  cxTextEdit1.SelectAll;
end;


end.
