unit Shutd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons, ExtCtrls,IB_Services;

type
  TfmShutd = class(TForm)
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    cxButton5: TcxButton;
    procedure FormShow(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmShutd: TfmShutd;

implementation

uses Un1, St, Dm;

{$R *.dfm}

procedure TfmShutd.FormShow(Sender: TObject);
begin
  cxButton3.SetFocus;
end;

procedure TfmShutd.cxButton5Click(Sender: TObject);
var sDB:string;
begin
  //������������� ���� - �������� ������� - �������� 10 ����
  if Person.Id=1001 then
  begin
    if MessageDlg('��������� ���������� � ����������� �����. ����������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      fmSt.Memo1.Clear;
      fmSt.Show;
      with fmSt do
      with dmC do
      begin
        cxButton1.Enabled:=False;
        Memo1.Lines.Add('����� .. ���� ������� �������������.'); Delay(10);
        with pBackUpServ do
        begin
          Active := True;
          try
            Verbose := True;
//            DatabaseName := DBName;
            sDb:=DBName;
            Delete(sDB,1,Pos(':',sDB));
//            DatabaseName :='E:\_CasherFb\DB\CASHERDB.GDB';
            DatabaseName :=sDB;
            BackupFile.Add(CurDir+'CasherDb_'+formatdatetime('yyyymmdd_hh_nn_ss.bak',Now));
            ServiceStart;
            while not pBackUpServ.Eof do
            begin
              Memo1.Lines.Add(GetNextLine);
              Delay(10);
            end;
            Active := False;

            Memo1.Lines.Add('������������� ��.'); Delay(10);

            //����� ��� ����� ����� ������
            //������� �� ����
            Memo1.Lines.Add('����� ... ���� �������� �����.'); Delay(10);

            quDel.SQL.Clear;
            quDel.SQL.Add('Delete from cashsail');
            quDel.SQL.Add('where');
            quDel.SQL.Add('SHOPINDEX=1 ');
//            quDel.SQL.Add('and CASHNUMBER='+its(CommonSet.CashNum));
            quDel.SQL.Add('and chdate<='''+formatdatetime('YYYY-MM-DD',date-30)+'''');  //2015-08-20'

            quDel.ExecQuery;

            Memo1.Lines.Add('����� ... ���� �������� ��������.'); Delay(10);

            quDel.SQL.Clear;
            quDel.SQL.Add('Delete from cashpay');
            quDel.SQL.Add('where');
            quDel.SQL.Add('SHOPINDEX=1 ');
//            quDel.SQL.Add('and CASHNUMBER='+its(CommonSet.CashNum));
            quDel.SQL.Add('and chdate<='''+formatdatetime('YYYY-MM-DD',date-30)+'''');  //2015-08-20'
            quDel.ExecQuery;

            Memo1.Lines.Add('��������� ��'); Delay(10);

            dmC.CasherDb.Close;  //��������� ���� ������

            Memo1.Lines.Add('��������� �������� ������ "ReBuild.bat".'); Delay(10);
            fExecCMD('/k ReBuild.bat');
            
          except
            Active := False;
          end;
        end;
        Memo1.Lines.Add('������� ��������.'); Delay(10);
        cxButton1.Enabled:=True;
      end;
    end;
  end else
  begin
    ShowMessage('��� ����. ���������� � ��������������.');
  end;
end;

end.
