unit dbSql;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB;

type
  TdmSQL = class(TForm)
    msConnection: TADOConnection;
    quSel: TADOQuery;
    quChecksSQL: TADOQuery;
    quChecksSQLId: TLargeintField;
    quChecksSQLId_Shop: TIntegerField;
    quChecksSQLId_Depart: TIntegerField;
    quChecksSQLOperation: TIntegerField;
    quChecksSQLDateOperation: TDateTimeField;
    quChecksSQLCk_Number: TIntegerField;
    quChecksSQLCassir: TIntegerField;
    quChecksSQLCash_Code: TIntegerField;
    quChecksSQLNSmena: TIntegerField;
    quChecksSQLCardNumber: TStringField;
    quChecksSQLPaymentType: TSmallintField;
    quChecksSQLrSumCh: TFloatField;
    quChecksSQLrSumD: TFloatField;
    quChecksSQLCountPos: TIntegerField;
    quD: TADOQuery;
    quA: TADOQuery;
    quGetId: TADOQuery;
    quGetIdID: TBCDField;
    quSumZ: TADOQuery;
    quSumZRSum: TFloatField;
    quSumZSummaNal: TFloatField;
    quSumZSummaBn: TFloatField;
    quSumZSummaRetNal: TFloatField;
    quSumZSummaRetBn: TFloatField;
    quChecksS: TADOQuery;
    quChecksSCk_Number: TIntegerField;
    quChecksSRSUM: TFloatField;
    quGetAMQ: TADOQuery;
    quGetAMQQUANT: TFloatField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function fShopSQL:INteger;
function fSelDepCash(iNum:INteger):INteger;
function fGetId_AI:INteger;
function fCanSaleAM(AM:String):Boolean;

var
  dmSQL: TdmSQL;

implementation

uses Un1, Ping;

{$R *.dfm}

function fCanSaleAM(AM:String):Boolean;
Var sWr:string;
    iQ:Integer;
begin
  Result:=True;
  with dmSQL do
  begin
    if CommonSet.SQLIP>'' then
    begin  // ip ����� ����� - ����� ����� ��������� �� ������ ������
      try
        if PingTest(CommonSet.SQLIP,50,sWr) then
        begin
          msConnection.Connected:=false;
          msConnection.ConnectionString:='Provider=SQLOLEDB.1;Password=YKS15Le;Persist Security Info=True;User ID=sa;Initial Catalog=Debor;Data Source='+CommonSet.SQLIP+';Use Procedure for Prepare=1;Auto Translate=True;Packet Size=4096;Workstation ID=SYS-9;Use Encryption for Data=False;Tag with column collation when possible=False';
          msConnection.Connected:=true;
          if msConnection.Connected then
          begin
            quGetAMQ.Active:=False;
            quGetAMQ.Parameters.ParamByName('AM').Value:=AM;
            quGetAMQ.Active:=True;
            if quGetAMQQUANT.AsFloat>0.001 then
            begin
              iQ:=RoundEx(quGetAMQQUANT.AsFloat);
              if iQ mod 2 <> 0 then Result:=False;
            end;
            quGetAMQ.Active:=False;
            msConnection.Connected:=false;
          end;
        end;
      except
      end;
    end;
  end;
end;

function fGetId_AI:INteger;
begin
  with dmSQL do
  begin
    Result:=0;
    quGetId.Active:=False;
    quGetId.Active:=True;
    if quGetId.RecordCount>0 then Result:=quGetIdID.AsInteger;
    quGetId.Active:=False;
  end;
end;

function fSelDepCash(iNum:INteger):INteger;
begin
  with dmSQL do
  begin
    Result:=0;
    quSel.Active:=False;
    quSel.SQL.Clear;
    quSel.SQL.Add('SELECT [IDEP] FROM [Debor].[dbo].[CASHLIST] where [ID]='+its(iNum));
    quSel.Active:=True;
    if quSel.RecordCount>0 then
    begin
      quSel.First;
      Result:=quSel.FieldByName('IDEP').Value;
    end;
    quSel.Active:=False;
  end;
end;

function fShopSQL:INteger;
begin
  with dmSQL do
  begin
    Result:=0;
    quSel.Active:=False;
    quSel.SQL.Clear;
    quSel.SQL.Add('SELECT [Debor].[dbo].[sfShop] () as ISHOP');
    quSel.Active:=True;
    if quSel.RecordCount>0 then
    begin
      quSel.First;
      Result:=quSel.FieldByName('ISHOP').AsInteger;
    end;
    quSel.Active:=False;
  end;
end;

end.
