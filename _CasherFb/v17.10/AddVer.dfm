object fmAddVer: TfmAddVer
  Left = 368
  Top = 405
  Width = 594
  Height = 298
  Caption = #1044#1086#1073#1072#1074#1083#1077#1085#1080#1077' '#1074#1077#1088#1089#1080#1080
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 24
    Top = 20
    Width = 54
    Height = 13
    Caption = #1042#1077#1088#1089#1080#1103' '#8470' '
    Transparent = True
  end
  object Label2: TLabel
    Left = 24
    Top = 56
    Width = 58
    Height = 13
    Caption = #1042#1077#1088#1089#1080#1103' '#1073#1080#1085
    Transparent = True
  end
  object Label3: TLabel
    Left = 24
    Top = 88
    Width = 82
    Height = 13
    Caption = #1058#1080#1087' '#1086#1073#1085#1086#1074#1083#1077#1085#1080#1103
    Transparent = True
  end
  object Label4: TLabel
    Left = 24
    Top = 120
    Width = 79
    Height = 13
    Caption = #1043#1088#1091#1079#1080#1090#1100' '#1083#1086#1072#1076#1077#1088
  end
  object Label5: TLabel
    Left = 24
    Top = 156
    Width = 59
    Height = 13
    Caption = #1051#1086#1072#1076#1077#1088' '#1073#1080#1085
    Transparent = True
  end
  object Panel1: TPanel
    Left = 0
    Top = 191
    Width = 586
    Height = 73
    Align = alBottom
    BevelInner = bvLowered
    Color = 16768959
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 100
      Top = 24
      Width = 89
      Height = 33
      Caption = #1054#1082
      ModalResult = 1
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 236
      Top = 24
      Width = 89
      Height = 33
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      LookAndFeel.Kind = lfOffice11
    end
  end
  object cxTextEdit1: TcxTextEdit
    Left = 96
    Top = 16
    TabOrder = 1
    Text = 'cxTextEdit1'
    Width = 137
  end
  object cxButtonEdit1: TcxButtonEdit
    Left = 96
    Top = 52
    Properties.Buttons = <
      item
        Default = True
        Kind = bkEllipsis
      end>
    Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
    TabOrder = 2
    Text = 'cxButtonEdit1'
    Width = 377
  end
  object cxComboBox1: TcxComboBox
    Left = 128
    Top = 84
    Properties.Items.Strings = (
      #1055#1086#1089#1083#1077'  '#1087#1077#1088#1077#1079#1072#1075#1088#1091#1079#1082#1080
      #1055#1086' '#1086#1082#1086#1085#1095#1072#1085#1080#1080' '#1095#1077#1082#1072)
    TabOrder = 3
    Text = 'cxComboBox1'
    Width = 189
  end
  object cxComboBox2: TcxComboBox
    Left = 128
    Top = 116
    Properties.Items.Strings = (
      #1053#1077#1090
      #1044#1072)
    TabOrder = 4
    Text = 'cxComboBox2'
    Width = 125
  end
  object cxButtonEdit2: TcxButtonEdit
    Left = 96
    Top = 148
    Properties.Buttons = <
      item
        Default = True
        Kind = bkEllipsis
      end>
    Properties.OnButtonClick = cxButtonEdit2PropertiesButtonClick
    TabOrder = 5
    Text = 'cxButtonEdit2'
    Width = 377
  end
  object OpenDialog1: TOpenDialog
    Left = 496
    Top = 89
  end
end
