unit uDB;

interface

uses
  SysUtils, Classes, DB, ADODB, FIBDatabase, pFIBDatabase, FIBDataSet,
  pFIBDataSet;

type
  TdmCasher = class(TDataModule)
    dsCF_Operations: TDataSource;
    dsOperations: TDataSource;
    dsStates: TDataSource;
    dsCF_Classif: TDataSource;
    dsCardScla: TDataSource;
    dsCF_CardScla: TDataSource;
    dsDepart: TDataSource;
    dsCF_Depart: TDataSource;
    dbCasher: TpFIBDatabase;
    trRead: TpFIBTransaction;
    trUpdate: TpFIBTransaction;
    taOperations: TpFIBDataSet;
    taOperationsID: TFIBIntegerField;
    taOperationsNAME: TFIBStringField;
    taOperationsID_DEVICE: TFIBIntegerField;
    taStates: TpFIBDataSet;
    taCF_Operations: TpFIBDataSet;
    taClassif: TpFIBDataSet;
    taCardScla: TpFIBDataSet;
    taDeparts: TpFIBDataSet;
    taStatesID: TFIBIntegerField;
    taStatesNAME: TFIBStringField;
    taStatesBARCODE: TFIBStringField;
    taStatesKEY_POSITION: TFIBIntegerField;
    taStatesID_DEVICE: TFIBIntegerField;
    taCF_OperationsKEY_CODE: TFIBIntegerField;
    taCF_OperationsKEY_STATUS: TFIBIntegerField;
    taCF_OperationsKEY_CHAR: TFIBStringField;
    taCF_OperationsID_OPERATIONS: TFIBIntegerField;
    taCF_OperationsID_STATES: TFIBIntegerField;
    taCF_OperationsBARCODE: TFIBStringField;
    taCF_OperationsKEY_POSITION: TFIBIntegerField;
    taCF_OperationsID_CLASSIF: TFIBIntegerField;
    taCF_OperationsARTICUL: TFIBStringField;
    taCF_OperationsID_DEPARTS: TFIBIntegerField;
    taCF_OperationsBAR: TFIBStringField;
    taClassifID: TFIBIntegerField;
    taClassifID_PARENT: TFIBIntegerField;
    taClassifTYPE_CLASSIF: TFIBIntegerField;
    taClassifNAME: TFIBStringField;
    taCardSclaARTICUL: TFIBStringField;
    taCardSclaCLASSIF: TFIBIntegerField;
    taCardSclaDEPART: TFIBIntegerField;
    taCardSclaNAME: TFIBStringField;
    taCardSclaCARD_TYPE: TFIBIntegerField;
    taCardSclaPRICE_RUB: TFIBFloatField;
    taCardSclaDISCOUNT: TFIBFloatField;
    taCardSclaSCALE: TFIBStringField;
    taDepartsID: TFIBIntegerField;
    taDepartsNAME: TFIBStringField;
    taDepartsPAR1: TFIBIntegerField;
    taDepartsPAR2: TFIBIntegerField;
    quCF_Operations: TpFIBDataSet;
    quCF_Classif: TpFIBDataSet;
    quCardScla: TpFIBDataSet;
    quCF_CardScla: TpFIBDataSet;
    quCF_Depart: TpFIBDataSet;
    quCF_OperationsID_OPERATIONS: TFIBIntegerField;
    quCF_OperationsKEY_CODE: TFIBIntegerField;
    quCF_OperationsKEY_STATUS: TFIBIntegerField;
    quCF_OperationsKEY_CHAR: TFIBStringField;
    quCF_OperationsNAME: TFIBStringField;
    quCF_OperationsNAME1: TFIBStringField;
    quCF_OperationsBARCODE: TFIBStringField;
    quCF_OperationsKEY_POSITION: TFIBIntegerField;
    quCF_ClassifNAME: TFIBStringField;
    quCF_ClassifID_CLASSIF: TFIBIntegerField;
    quCF_ClassifKEY_CODE: TFIBIntegerField;
    quCF_ClassifKEY_CHAR: TFIBStringField;
    quCF_ClassifKEY_STATUS: TFIBIntegerField;
    taCardSclaMESURIMENT: TFIBStringField;
    quCF_DepartNAME: TFIBStringField;
    quCF_DepartID_DEPARTS: TFIBIntegerField;
    quCF_DepartKEY_CODE: TFIBIntegerField;
    quCF_DepartKEY_CHAR: TFIBStringField;
    quCF_DepartKEY_STATUS: TFIBIntegerField;
    quCF_CardSclaNAME: TFIBStringField;
    quCF_CardSclaPRICE_RUB: TFIBFloatField;
    quCF_CardSclaMESURIMENT: TFIBStringField;
    quCF_CardSclaARTICUL: TFIBStringField;
    quCF_CardSclaBAR: TFIBStringField;
    quCF_CardSclaKEY_CODE: TFIBIntegerField;
    quCF_CardSclaKEY_CHAR: TFIBStringField;
    quCF_CardSclaKEY_STATUS: TFIBIntegerField;
    quCardSclaBARCODE: TFIBStringField;
    quCardSclaCARDARTICUL: TFIBStringField;
    quCardSclaQUANTITY: TFIBIntegerField;
    quCardSclaPRICERUB: TFIBFloatField;
    quCardSclaCARDSIZE: TFIBStringField;
    quCardSclaPRICE_RUB: TFIBFloatField;
    quCardSclaNAME: TFIBStringField;
    quCardSclaMESURIMENT: TFIBStringField;
    quCardSclaDISCOUNT: TFIBFloatField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmCasher: TdmCasher;

implementation

{$R *.dfm}

end.
