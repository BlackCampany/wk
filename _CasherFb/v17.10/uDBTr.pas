unit uDBTr;

interface

uses
  SysUtils, Classes, DB, DBTables, DBClient, FIBDatabase, pFIBDatabase,
  FIBDataSet, pFIBDataSet;

type
  TdmTr = class(TDataModule)
    trSelTransp: TpFIBTransaction;
    quSumTr: TpFIBDataSet;
    quSumTrOPERATION: TFIBSmallIntField;
    quSumTrSUMOP: TFIBFloatField;
    fbCashSail: TpFIBDataSet;
    fbCashSailSHOPINDEX: TFIBSmallIntField;
    fbCashSailCASHNUMBER: TFIBIntegerField;
    fbCashSailZNUMBER: TFIBIntegerField;
    fbCashSailCHECKNUMBER: TFIBIntegerField;
    fbCashSailID: TFIBIntegerField;
    fbCashSailCHDATE: TFIBDateTimeField;
    fbCashSailARTICUL: TFIBStringField;
    fbCashSailBAR: TFIBStringField;
    fbCashSailCARDSIZE: TFIBStringField;
    fbCashSailQUANTITY: TFIBFloatField;
    fbCashSailPRICERUB: TFIBFloatField;
    fbCashSailDPROC: TFIBFloatField;
    fbCashSailDSUM: TFIBFloatField;
    fbCashSailTOTALRUB: TFIBFloatField;
    fbCashSailCASHER: TFIBIntegerField;
    fbCashSailDEPART: TFIBIntegerField;
    fbCashSailOPERATION: TFIBSmallIntField;
    fbCashPay: TpFIBDataSet;
    taCashSail: TTable;
    taCashSailShopIndex: TSmallintField;
    taCashSailCashNumber: TSmallintField;
    taCashSailZNumber: TSmallintField;
    taCashSailCheckNumber: TSmallintField;
    taCashSailID: TSmallintField;
    taCashSailDate: TDateField;
    taCashSailTime: TSmallintField;
    taCashSailCardArticul: TStringField;
    taCashSailCardSize: TStringField;
    taCashSailQuantity: TFloatField;
    taCashSailPriceRub: TCurrencyField;
    taCashSailPriceCur: TCurrencyField;
    taCashSailTotalRub: TCurrencyField;
    taCashSailTotalCur: TCurrencyField;
    taCashSailDepartment: TSmallintField;
    taCashSailCasher: TSmallintField;
    taCashSailUsingIndex: TSmallintField;
    taCashSailReplace: TSmallintField;
    taCashSailOperation: TSmallintField;
    taCashSailCredCardIndex: TSmallintField;
    taCashSailDiscCliIndex: TSmallintField;
    taCashSailLinked: TSmallintField;
    taCashDCRD: TTable;
    taCashDCRDShopIndex: TSmallintField;
    taCashDCRDCashNumber: TSmallintField;
    taCashDCRDZNumber: TSmallintField;
    taCashDCRDCheckNumber: TSmallintField;
    taCashDCRDCardType: TSmallintField;
    taCashDCRDCardNumber: TStringField;
    taCashDCRDDiscountRub: TCurrencyField;
    taCashDCRDDiscountCur: TCurrencyField;
    dsCash: TDataSource;
    taCashDisc: TTable;
    taCashDiscShopIndex: TSmallintField;
    taCashDiscCashNumber: TSmallintField;
    taCashDiscZNumber: TSmallintField;
    taCashDiscCheckNumber: TSmallintField;
    taCashDiscID: TSmallintField;
    taCashDiscDiscountIndex: TSmallintField;
    taCashDiscDiscountProc: TFloatField;
    taCashDiscDiscountRub: TCurrencyField;
    taCashDiscDiscountCur: TCurrencyField;
    Session1: TSession;
    quCashPay: TpFIBDataSet;
    quCashPaySHOPINDEX: TFIBSmallIntField;
    quCashPayCASHNUMBER: TFIBIntegerField;
    quCashPayZNUMBER: TFIBIntegerField;
    quCashPayCHECKNUMBER: TFIBIntegerField;
    quCashPayCASHER: TFIBIntegerField;
    quCashPayCHECKSUM: TFIBFloatField;
    quCashPayPAYSUM: TFIBFloatField;
    quCashPayDSUM: TFIBFloatField;
    quCashPayDBAR: TFIBStringField;
    quCashPayCOUNTPOS: TFIBSmallIntField;
    quCashPayCHDATE: TFIBDateTimeField;
    quCashPayPAYMENT: TFIBSmallIntField;
    fbCashSailSECPOS: TFIBSmallIntField;
    fbCashSailECHECK: TFIBSmallIntField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmTr: TdmTr;

implementation

uses Dm;

{$R *.dfm}

end.
