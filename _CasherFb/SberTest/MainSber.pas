unit MainSber;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxControls, cxContainer, cxEdit, cxTextEdit, cxMemo, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons,
  ComObj, ActiveX, OleServer, cxCurrencyEdit, dxfBackGround, cxMaskEdit,
  cxSpinEdit;

type
  TfmMainSber = class(TForm)
    Memo1: TcxMemo;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    cxButton5: TcxButton;
    cxButton6: TcxButton;
    cxButton7: TcxButton;
    cxButton8: TcxButton;
    dxfBackGround1: TdxfBackGround;
    cxButton9: TcxButton;
    cxButton10: TcxButton;
    cxButton11: TcxButton;
    cxButton12: TcxButton;
    cxCEdit1: TcxSpinEdit;
    procedure FormShow(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton8Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
    procedure cxButton7Click(Sender: TObject);
    procedure cxButton9Click(Sender: TObject);
    procedure cxButton10Click(Sender: TObject);
    procedure cxButton11Click(Sender: TObject);
    procedure cxButton12Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function IsOLEObjectInstalled(Name: String): boolean;

var
  fmMainSber: TfmMainSber;
  Drv: OleVariant;
  vCh: Variant;
  iMoney:Integer;

implementation

{$R *.dfm}

function IsOLEObjectInstalled(Name: String): boolean;
var
  ClassID: TCLSID;
  Rez : HRESULT;
begin

// ���� CLSID OLE-�������
  Rez := CLSIDFromProgID(PWideChar(WideString(Name)), ClassID);
  if Rez = S_OK then
 // ������ ������
    Result := true
  else
    Result := false;
end;


procedure TfmMainSber.FormShow(Sender: TObject);
begin
  Memo1.Clear;
end;

procedure TfmMainSber.cxButton1Click(Sender: TObject);
Var tTime:TDateTime;
    StrT:String;
begin
//�������
  Memo1.Lines.Add('');
  Memo1.Lines.Add('��������� ����������.');
  tTime:=now;

  if IsOLEObjectInstalled('SBRFSRV.Server') then
  begin
    tTime:=Now-tTime;
    StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
    Memo1.Lines.Add('������� ����������.'+StrT);
  end else
  begin
    tTime:=Now-tTime;
    StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
    Memo1.Lines.Add('������� �� ���������� !!!'+StrT);
    exit;
  end;

  Memo1.Lines.Add('');
  Memo1.Lines.Add('������� ������ ��������.');
  tTime:=now;
  Drv := CreateOleObject('SBRFSRV.Server');
  Drv.Clear;
  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('�������� �������.'+StrT);

end;

procedure TfmMainSber.cxButton8Click(Sender: TObject);
Var tTime:TDateTime;
    StrT:String;
begin
  Memo1.Lines.Add('');
  Memo1.Lines.Add('��������� ����������. (����)');
  tTime:=now;

  Drv.Clear;

  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('��������� ������. '+StrT);
end;

procedure TfmMainSber.cxButton2Click(Sender: TObject);
Var tTime:TDateTime;
    StrT:String;
    iRes:Integer;
    StrWk:String;
begin
  Memo1.Lines.Add('');
  Memo1.Lines.Add('������ ��� (��������� ������) 4000');
  tTime:=now;
  iMoney:=cxCEdit1.EditValue;

  Drv.Clear;
  Drv.SParam('Amount',iMoney);
//  Drv.SParam('CardType',0);
  iRes:=Drv.NFun(4000);
  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('����� : '+StrT);
  Memo1.Lines.Add('----��������� : '+IntToStr(iRes));

  StrWk:=Drv.GParam('ClientCard');
  Memo1.Lines.Add('----����� ������� : '+StrWk);

  StrWk:=Drv.GParam('ClientExpiryDate');
  Memo1.Lines.Add('----���� ��������� �������� : '+StrWk);
  Memo1.Lines.Add('');
  Memo1.Lines.Add('���������� ���');
  vCh:=Drv.GParam('Cheque');
  Memo1.Lines.Add('� ������');
  StrWk:= vCh;
  Memo1.Lines.Add('--------------------------------------');
  if length(StrWk)>0 then
  begin
    Memo1.Lines.Add(StrWk);
  end;
  Memo1.Lines.Add('--------------------------------------');
  Memo1.Lines.Add('');
  Memo1.Lines.Add('');
end;

procedure TfmMainSber.cxButton4Click(Sender: TObject);
Var tTime:TDateTime;
    StrT:String;
    iRes:Integer;
    StrWk:String;
begin
  Memo1.Lines.Add('');
  Memo1.Lines.Add('������ ������� ������� 4006');
  tTime:=now;

  Drv.Clear;
  Drv.SParam('Amount',cxCEdit1.EditValue);
//  Drv.SParam('CardType',0);
  iRes:=Drv.NFun(4006);
  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('����� : '+StrT);
  Memo1.Lines.Add('----��������� : '+IntToStr(iRes));

  StrWk:=Drv.GParam('ClientCard');
  Memo1.Lines.Add('----����� ������� : '+StrWk);

  StrWk:=Drv.GParam('ClientExpiryDate');
  Memo1.Lines.Add('----���� ��������� �������� : '+StrWk);
  Memo1.Lines.Add('');
  Memo1.Lines.Add('���������� ���');
  vCh:=Drv.GParam('Cheque');
  Memo1.Lines.Add('� ������');
  StrWk:= vCh;
  Memo1.Lines.Add('--------------------------------------');
  if length(StrWk)>0 then
  begin
    Memo1.Lines.Add(StrWk);
  end;
  Memo1.Lines.Add('--------------------------------------');
  Memo1.Lines.Add('');
  Memo1.Lines.Add('');
end;

procedure TfmMainSber.cxButton3Click(Sender: TObject);
Var tTime:TDateTime;
    StrT:String;
    iRes:Integer;
    StrWk:String;
begin
  Memo1.Lines.Add('');
  Memo1.Lines.Add('������ ��������   1000');
  tTime:=now;

  Drv.Clear;
  Drv.SParam('Amount',cxCEdit1.EditValue);
//  Drv.SParam('CardType',0);
  iRes:=Drv.NFun(1000);
  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('����� : '+StrT);
  Memo1.Lines.Add('----��������� : '+IntToStr(iRes));

  StrWk:=Drv.GParam('ClientCard');
  Memo1.Lines.Add('----����� ������� : '+StrWk);

  StrWk:=Drv.GParam('ClientExpiryDate');
  Memo1.Lines.Add('----���� ��������� �������� : '+StrWk);
  Memo1.Lines.Add('');
  Memo1.Lines.Add('���������� ���');
  vCh:=Drv.GParam('Cheque');
  Memo1.Lines.Add('� ������');
  StrWk:= vCh;
  Memo1.Lines.Add('--------------------------------------');
  if length(StrWk)>0 then
  begin
    Memo1.Lines.Add(StrWk);
  end;
  Memo1.Lines.Add('--------------------------------------');
  Memo1.Lines.Add('');
  Memo1.Lines.Add('');
end;

procedure TfmMainSber.cxButton5Click(Sender: TObject);
Var tTime:TDateTime;
    StrT:String;
    iRes:Integer;
    StrWk:String;
begin
  Memo1.Lines.Add('');
  Memo1.Lines.Add('������� ���   4002');
  tTime:=now;

  Drv.Clear;
  Drv.SParam('Amount',cxCEdit1.EditValue);
//  Drv.SParam('CardType',0);
  iRes:=Drv.NFun(4002);
  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('����� : '+StrT);
  Memo1.Lines.Add('----��������� : '+IntToStr(iRes));

  StrWk:=Drv.GParam('ClientCard');
  Memo1.Lines.Add('----����� ������� : '+StrWk);

  StrWk:=Drv.GParam('ClientExpiryDate');
  Memo1.Lines.Add('----���� ��������� �������� : '+StrWk);
  Memo1.Lines.Add('');
  Memo1.Lines.Add('���������� ���');
  vCh:=Drv.GParam('Cheque');
  Memo1.Lines.Add('� ������');
  StrWk:= vCh;
  Memo1.Lines.Add('--------------------------------------');
  if length(StrWk)>0 then
  begin
    Memo1.Lines.Add(StrWk);
  end;
  Memo1.Lines.Add('--------------------------------------');
  Memo1.Lines.Add('');
  Memo1.Lines.Add('');
end;

procedure TfmMainSber.cxButton6Click(Sender: TObject);
Var tTime:TDateTime;
    StrT:String;
    iRes:Integer;
    StrWk:String;
begin
  Memo1.Lines.Add('');
  Memo1.Lines.Add('������ �������� �� ���   4003 ');
  tTime:=now;

  Drv.Clear;
  Drv.SParam('Amount',cxCEdit1.EditValue);
//  Drv.SParam('CardType',0);
  iRes:=Drv.NFun(4003);
  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('����� : '+StrT);
  Memo1.Lines.Add('----��������� : '+IntToStr(iRes));

  StrWk:=Drv.GParam('ClientCard');
  Memo1.Lines.Add('----����� ������� : '+StrWk);

  StrWk:=Drv.GParam('ClientExpiryDate');
  Memo1.Lines.Add('----���� ��������� �������� : '+StrWk);
  Memo1.Lines.Add('');
  Memo1.Lines.Add('���������� ���');
  vCh:=Drv.GParam('Cheque');
  Memo1.Lines.Add('� ������');
  StrWk:= vCh;
  Memo1.Lines.Add('--------------------------------------');
  if length(StrWk)>0 then
  begin
    Memo1.Lines.Add(StrWk);
  end;
  Memo1.Lines.Add('--------------------------------------');
  Memo1.Lines.Add('');
  Memo1.Lines.Add('');
end;

procedure TfmMainSber.cxButton7Click(Sender: TObject);
Var tTime:TDateTime;
    StrT:String;
    iRes:Integer;
    StrWk:String;
begin
  Memo1.Lines.Add('');
  Memo1.Lines.Add('������� �� ��������   1002 ');
  tTime:=now;

  Drv.Clear;
  Drv.SParam('Amount',cxCEdit1.EditValue);
//  Drv.SParam('CardType',0);
  iRes:=Drv.NFun(1002);
  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('����� : '+StrT);
  Memo1.Lines.Add('----��������� : '+IntToStr(iRes));

  StrWk:=Drv.GParam('ClientCard');
  Memo1.Lines.Add('----����� ������� : '+StrWk);

  StrWk:=Drv.GParam('ClientExpiryDate');
  Memo1.Lines.Add('----���� ��������� �������� : '+StrWk);
  Memo1.Lines.Add('');
  Memo1.Lines.Add('���������� ���');
  vCh:=Drv.GParam('Cheque');
  Memo1.Lines.Add('� ������');
  StrWk:= vCh;
  Memo1.Lines.Add('--------------------------------------');
  if length(StrWk)>0 then
  begin
    Memo1.Lines.Add(StrWk);
  end;
  Memo1.Lines.Add('--------------------------------------');
  Memo1.Lines.Add('');
  Memo1.Lines.Add('');
end;


procedure TfmMainSber.cxButton9Click(Sender: TObject);
Var tTime:TDateTime;
    StrT:String;
    iRes:Integer;
    StrWk:String;
begin
  Memo1.Lines.Add('');
  Memo1.Lines.Add('�������� ��� �� ��������� � ����������� ������� (6000) ');
  tTime:=now;

  Drv.Clear;
  iRes:=Drv.NFun(6000);
  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('����� : '+StrT);
  Memo1.Lines.Add('----��������� : '+IntToStr(iRes));
  Memo1.Lines.Add('');
  Memo1.Lines.Add('���������� ���');
  vCh:=Drv.GParam('Cheque');
  Memo1.Lines.Add('� ������');
  StrWk:= vCh;
  Memo1.Lines.Add('--------------------------------------');
  if length(StrWk)>0 then
  begin
    Memo1.Lines.Add(StrWk);
  end;
  Memo1.Lines.Add('--------------------------------------');
  Memo1.Lines.Add('');
  Memo1.Lines.Add('');
end;


procedure TfmMainSber.cxButton10Click(Sender: TObject);
Var tTime:TDateTime;
    StrT:String;
    iRes:Integer;
    StrWk:String;
begin
  Memo1.Lines.Add('');
  Memo1.Lines.Add('���������� ��������   3001 ');
  tTime:=now;

  Drv.Clear;
  iRes:=Drv.NFun(3001);
  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('����� : '+StrT);
  Memo1.Lines.Add('----��������� : '+IntToStr(iRes));
  Memo1.Lines.Add('');
  Memo1.Lines.Add('���������� ���');
  vCh:=Drv.GParam('Cheque');
  Memo1.Lines.Add('� ������');
  StrWk:= vCh;
  Memo1.Lines.Add('--------------------------------------');
  if length(StrWk)>0 then
  begin
    Memo1.Lines.Add(StrWk);
  end;
  Memo1.Lines.Add('--------------------------------------');
  Memo1.Lines.Add('');
  Memo1.Lines.Add('');
end;


procedure TfmMainSber.cxButton11Click(Sender: TObject);
Var tTime:TDateTime;
    StrT:String;
    iRes:Integer;
    StrWk:String;
begin
  Memo1.Lines.Add('');
  Memo1.Lines.Add('������� �����    7000 ');
  tTime:=now;

  Drv.Clear;
  iRes:=Drv.NFun(7000);
  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('����� : '+StrT);
  Memo1.Lines.Add('----��������� : '+IntToStr(iRes));
  Memo1.Lines.Add('');
  Memo1.Lines.Add('���������� ���');
  vCh:=Drv.GParam('Cheque');
  Memo1.Lines.Add('� ������');
  StrWk:= vCh;
  Memo1.Lines.Add('--------------------------------------');
  if length(StrWk)>0 then
  begin
    Memo1.Lines.Add(StrWk);
  end;
  Memo1.Lines.Add('--------------------------------------');
  Memo1.Lines.Add('');
  Memo1.Lines.Add('');
end;


procedure TfmMainSber.cxButton12Click(Sender: TObject);
Var tTime:TDateTime;
    StrT:String;
    iRes:Integer;
    StrWk:String;
begin
  Memo1.Lines.Add('');
  Memo1.Lines.Add('������ ����. ���������  7001 ');
  tTime:=now;

  Drv.Clear;
  iRes:=Drv.NFun(7001);
  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('����� : '+StrT);
  Memo1.Lines.Add('----��������� : '+IntToStr(iRes));
  Memo1.Lines.Add('');
  Memo1.Lines.Add('���������� ���');
  vCh:=Drv.GParam('Cheque');
  Memo1.Lines.Add('� ������');
  StrWk:= vCh;
  Memo1.Lines.Add('--------------------------------------');
  if length(StrWk)>0 then
  begin
    Memo1.Lines.Add(StrWk);
  end;
  Memo1.Lines.Add('--------------------------------------');
  Memo1.Lines.Add('');
  Memo1.Lines.Add('');
end;

end.
