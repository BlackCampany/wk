object fmMainSber: TfmMainSber
  Left = 648
  Top = 143
  Width = 696
  Height = 610
  Caption = #1041#1077#1079#1085#1072#1083' '#1089#1073#1077#1088#1072
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TcxMemo
    Left = 184
    Top = 8
    Lines.Strings = (
      'Memo1')
    TabOrder = 0
    Height = 561
    Width = 497
  end
  object cxButton1: TcxButton
    Left = 24
    Top = 24
    Width = 140
    Height = 25
    Caption = #1054#1090#1082#1088#1099#1090#1100
    TabOrder = 1
    OnClick = cxButton1Click
    LookAndFeel.Kind = lfOffice11
  end
  object cxButton2: TcxButton
    Left = 24
    Top = 64
    Width = 140
    Height = 25
    Caption = #1052#1041#1050' (4000)'
    TabOrder = 2
    OnClick = cxButton2Click
    LookAndFeel.Kind = lfOffice11
  end
  object cxButton3: TcxButton
    Left = 24
    Top = 104
    Width = 140
    Height = 25
    Caption = #1057#1041#1045#1056#1050#1040#1056#1058' (1000)'
    TabOrder = 3
    OnClick = cxButton3Click
    LookAndFeel.Kind = lfOffice11
  end
  object cxButton4: TcxButton
    Left = 24
    Top = 144
    Width = 140
    Height = 25
    Caption = 'AmEx (4006)'
    TabOrder = 4
    OnClick = cxButton4Click
    LookAndFeel.Kind = lfOffice11
  end
  object cxButton5: TcxButton
    Left = 24
    Top = 184
    Width = 140
    Height = 25
    Caption = #1042#1086#1079#1074#1088#1072#1090' '#1052#1041#1050' (4002) '
    TabOrder = 5
    OnClick = cxButton5Click
    LookAndFeel.Kind = lfOffice11
  end
  object cxButton6: TcxButton
    Left = 24
    Top = 224
    Width = 140
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1072' '#1052#1041#1050' (4003) '
    TabOrder = 6
    OnClick = cxButton6Click
    LookAndFeel.Kind = lfOffice11
  end
  object cxButton7: TcxButton
    Left = 24
    Top = 264
    Width = 140
    Height = 25
    Caption = #1042#1086#1079#1074#1088#1072#1090' '#1057#1050' (1002) '
    TabOrder = 7
    OnClick = cxButton7Click
    LookAndFeel.Kind = lfOffice11
  end
  object cxButton8: TcxButton
    Left = 24
    Top = 464
    Width = 140
    Height = 25
    Caption = #1047#1072#1082#1088#1099#1090#1100
    TabOrder = 8
    OnClick = cxButton8Click
    LookAndFeel.Kind = lfOffice11
  end
  object cxButton9: TcxButton
    Left = 24
    Top = 304
    Width = 140
    Height = 25
    Caption = #1047#1072#1082#1088#1099#1090#1080#1077' '#1076#1085#1103' (6000)'
    TabOrder = 9
    OnClick = cxButton9Click
    LookAndFeel.Kind = lfOffice11
  end
  object cxButton10: TcxButton
    Left = 24
    Top = 344
    Width = 140
    Height = 25
    Caption = #1048#1053#1050#1040#1057#1057#1040#1062#1048#1071' '#1057#1041#1045#1056#1050#1040#1056#1044
    TabOrder = 10
    OnClick = cxButton10Click
    LookAndFeel.Kind = lfOffice11
  end
  object cxButton11: TcxButton
    Left = 24
    Top = 384
    Width = 140
    Height = 25
    Caption = #1058#1077#1082#1091#1097#1080#1081' '#1086#1090#1095#1077#1090' (7000)'
    TabOrder = 11
    OnClick = cxButton11Click
    LookAndFeel.Kind = lfOffice11
  end
  object cxButton12: TcxButton
    Left = 24
    Top = 424
    Width = 140
    Height = 25
    Caption = #1055#1077#1095#1072#1090#1100' '#1087#1086#1089#1083'. '#1076#1086#1082#1091#1084#1077#1085#1090#1072
    TabOrder = 12
    OnClick = cxButton12Click
    LookAndFeel.Kind = lfOffice11
  end
  object cxCEdit1: TcxSpinEdit
    Left = 24
    Top = 512
    Style.LookAndFeel.Kind = lfOffice11
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.BorderStyle = ebsOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 13
    Value = 1
    Width = 121
  end
  object dxfBackGround1: TdxfBackGround
    BkColor.BeginColor = 16767449
    BkColor.EndColor = clBlue
    BkColor.FillStyle = fsVert
    BkAnimate.Speed = 700
    Left = 184
    Top = 360
  end
end
