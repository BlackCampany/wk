unit SP101FRKLib_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : $Revision:   1.130  $
// File generated on 27.06.2005 12:24:19 from Type Library described below.

// ************************************************************************  //
// Type Lib: C:\Projects\FR101Com\SP101FRKLib.tlb (1)
// LIBID: {685E9290-6CB3-4E73-A74A-3D161051D893}
// LCID: 0
// Helpfile: 
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINNT\system32\stdole2.tlb)
//   (2) v4.0 StdVCL, (C:\WINNT\System32\STDVCL40.DLL)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}

interface

uses ActiveX, Classes, Graphics, StdVCL, Variants, Windows;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  SP101FRKLibMajorVersion = 1;
  SP101FRKLibMinorVersion = 0;

  LIBID_SP101FRKLib: TGUID = '{685E9290-6CB3-4E73-A74A-3D161051D893}';

  IID_ISP101FRObject: TGUID = '{1B15AF2D-CD27-4863-AEC8-7CF3D078233A}';
  CLASS_SP101FRObject: TGUID = '{C01711A3-2958-4074-812F-26CB00A655E3}';

// *********************************************************************//
// Declaration of Enumerations defined in Type Library                    
// *********************************************************************//
// Constants for enum FR_ERRORS
type
  FR_ERRORS = TOleEnum;
const
  SPFRE_OK = $00000000;
  SPFRE_STATUS = $00000001;
  SPFRE_INVALID_FUNCTION = $00000002;
  SPFRE_INVALID_PARAMETER = $00000003;
  SPFRE_UART_OVER = $00000004;
  SPFRE_INTERCHAR_TIMEOUT = $00000005;
  SPFRE_INVALID_PASSWORD = $00000006;
  SPFRE_INVALID_CHECKSUM = $00000007;
  SPFRE_PAPER_END = $00000008;
  SPFRE_PRINTER_NOTREADY = $00000009;
  SPFRE_SHIFT_TIME_OVER = $0000000A;
  SPFRE_SYNC_TIME_OVER = $0000000B;
  SPFRE_TIME_LOW = $0000000C;
  SPFRE_NO_HEADER = $0000000D;
  SPFRE_FALSE = $0000000E;
  SPFRE_DATA_SEND = $00000010;
  SPFRE_NODATA = $00000011;
  SPFRE_PORT_INACCESSIBLE = $00000012;
  SPFRE_NOREGISTRATOR = $00000013;
  SPFRE_FATAL = $00000020;
  SPFRE_NO_FREE_SPACE = $00000021;
  SPFRE_DEVICE_TIMEOUT = $00000030;
  SPFRE_BUSY = $00000040;
  SPFRE_EEJ1 = $00000041;
  SPFRE_EEJ2 = $00000042;
  SPFRE_EEJ3 = $00000043;
  SPFRE_EEJ4 = $00000044;
  SPFRE_EEJ5 = $00000045;
  SPFRE_EEJ6 = $00000046;
  SPFRE_EEJ7 = $00000047;
  SPFRE_EEJ8 = $00000048;
  SPFRE_EEJ9 = $00000049;
  SPFRE_EEJA = $0000004A;
  SPFRE_EEJB = $0000004B;

// Constants for enum SPFR_DATA_AREA
type
  SPFR_DATA_AREA = TOleEnum;
const
  arBIOS = $00000000;
  arDATA = $00000001;
  arRAM = $00000002;

// Constants for enum BARCODE_TYPE
type
  BARCODE_TYPE = TOleEnum;
const
  UPCA = $00000000;
  UPCE = $00000001;
  EAN13 = $00000002;
  EAN8 = $00000003;
  Code39 = $00000004;
  Interleaved = $00000005;
  Codabar = $00000006;

// Constants for enum BARCODE_WIDTH
type
  BARCODE_WIDTH = TOleEnum;
const
  bw2 = $00000000;
  bw3 = $00000001;
  bw4 = $00000002;

// Constants for enum BARCODE_AS_DIGIT
type
  BARCODE_AS_DIGIT = TOleEnum;
const
  NO_DIGIT = $00000000;
  DIGIT_UP = $00000001;
  DIGIT_DOWN = $00000002;
  DIGIT_UPDOWN = $00000003;

type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  ISP101FRObject = interface;
  ISP101FRObjectDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  SP101FRObject = ISP101FRObject;


// *********************************************************************//
// Interface: ISP101FRObject
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {1B15AF2D-CD27-4863-AEC8-7CF3D078233A}
// *********************************************************************//
  ISP101FRObject = interface(IDispatch)
    ['{1B15AF2D-CD27-4863-AEC8-7CF3D078233A}']
    function  Connect(PortNumber: Byte): Smallint; safecall;
    procedure Disconnect; safecall;
    procedure GetObjectState(out WasConnected: WordBool); safecall;
    procedure GetErrorMesageRU(ErrorCode: Smallint; out ErrorDescription: WideString); safecall;
    function  GetStatus(out FatalStatus: Byte; out Status: Byte; out ReceiptStatus: Byte): Smallint; safecall;
    function  CheckActive(out IsActive: WordBool): Smallint; safecall;
    function  Init(CheckDateTime: TDateTime): Smallint; safecall;
    function  Register(const OldPassword: WideString; const RegNumber: WideString; 
                       const INN: WideString; const NewPassword: WideString): Smallint; safecall;
    function  ActivateEJJ: Smallint; safecall;
    function  CloseEEJArchive: Smallint; safecall;
    function  OpenReceipt(ReceiptType: Smallint; SectionNumber: Smallint; 
                          const OperatorName: WideString; ReceiptNumber: LongWord): Smallint; safecall;
    function  PrintString(const Buffer: WideString): Smallint; safecall;
    function  CloseReceipt: Smallint; safecall;
    function  BreakReceipt: Smallint; safecall;
    function  AddArticle(const ArticleName: WideString; const ArticleCode: WideString; 
                         ArticleQuantity: Currency; ArticleTotal: Currency; Department: Smallint; 
                         out ReceiptTotal: Currency): Smallint; safecall;
    function  StornoArticle(const ArticleName: WideString; const ArticleCode: WideString; 
                            ArticleQuantity: Currency; ArticleTotal: Currency; 
                            Department: Smallint; out ReceiptTotal: Currency): Smallint; safecall;
    function  DiscountPercent(const DiscountName: WideString; Percent: Currency; 
                              out ArticleTotal: Currency): Smallint; safecall;
    function  DiscountTotal(const DiscountName: WideString; Total: Currency; 
                            out ArticleTotal: Currency): Smallint; safecall;
    function  ExtraPercent(const ExtraName: WideString; Percent: Currency; 
                           out ArticleTotal: Currency): Smallint; safecall;
    function  ExtraTotal(const ExtraName: WideString; Total: Currency; out ArticleTotal: Currency): Smallint; safecall;
    function  SubTotal(out ReceiptTotal: Currency): Smallint; safecall;
    function  Payment(PaymentType: Smallint; PaymentTotal: Currency; out ReceiptTotal: Currency): Smallint; safecall;
    function  CashInOut(const BanknoteName: WideString; Total: Currency; out ReceiptTotal: Currency): Smallint; safecall;
    function  XReport(const OperatorName: WideString): Smallint; safecall;
    function  ZReport(const OperatorName: WideString): Smallint; safecall;
    function  FiscShiftReport(IsFull: WordBool; FirstShift: Smallint; LastShift: Smallint; 
                              const FiscPassword: WideString): Smallint; safecall;
    function  FiscDateReport(IsFull: WordBool; FirstDate: TDateTime; LastDate: TDateTime; 
                             const FiscPassword: WideString): Smallint; safecall;
    function  EEJJournal(ShiftNumber: Smallint): Smallint; safecall;
    function  GetReceipt(KPKNumber: LongWord): Smallint; safecall;
    function  EEJShiftReport(IsFull: WordBool; FirstShift: Smallint; LastShift: Smallint): Smallint; safecall;
    function  EEJDateReport(IsFull: WordBool; FirstDate: TDateTime; LastDate: TDateTime): Smallint; safecall;
    function  EEJActivationReport: Smallint; safecall;
    function  EEJReport(ShiftNumber: Smallint): Smallint; safecall;
    function  ReadConfig(i: Smallint; j: Smallint; out Param: WideString): Smallint; safecall;
    function  WriteConfig(i: Smallint; j: Smallint; const Param: WideString): Smallint; safecall;
    function  SetDate(NewDateTime: TDateTime): Smallint; safecall;
    function  GetEEJStatus: Smallint; safecall;
    function  GetCurrentShift(out ShiftNumber: Smallint): Smallint; safecall;
    function  GetLastRegShift(out ShiftNumber: Smallint): Smallint; safecall;
    function  GetRegNumber(out RegNumber: WideString): Smallint; safecall;
    function  OpenCashDrawer: Smallint; safecall;
    function  Install(NewDateTime: TDateTime; const SerialNumber: WideString): Smallint; safecall;
    function  ReadMemBlock(DataAreaType: Smallint; ByteOffset: Smallint; ByteCount: Smallint; 
                           DataBufferPointer: OleVariant): Smallint; safecall;
    function  LoadLogo(ByteCount: Smallint; DataBufferPointer: OleVariant): Smallint; safecall;
    function  LoadLogoFromFile(const FileName: WideString): Smallint; safecall;
    function  CancelReport: Smallint; safecall;
    function  AddBarcode(BarcodeType: Smallint; BarcodeAsDigit: Smallint; BarcodeWidth: Smallint; 
                         BarcodeHeight: Smallint; const BarcodeData: WideString): Smallint; safecall;
    function  CutAndPrint: Smallint; safecall;
    function  EEJEmergencyShiftClose: Smallint; safecall;
    function  EEJShowClientDisplayText(const Row1: WideString; const Row2: WideString): Smallint; safecall;
    function  ClearLogo: Smallint; safecall;
    function  Init2: Smallint; safecall;
    function  Install2(const SerialNumber: WideString): Smallint; safecall;
  end;

// *********************************************************************//
// DispIntf:  ISP101FRObjectDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {1B15AF2D-CD27-4863-AEC8-7CF3D078233A}
// *********************************************************************//
  ISP101FRObjectDisp = dispinterface
    ['{1B15AF2D-CD27-4863-AEC8-7CF3D078233A}']
    function  Connect(PortNumber: Byte): Smallint; dispid 1;
    procedure Disconnect; dispid 2;
    procedure GetObjectState(out WasConnected: WordBool); dispid 3;
    procedure GetErrorMesageRU(ErrorCode: Smallint; out ErrorDescription: WideString); dispid 4;
    function  GetStatus(out FatalStatus: Byte; out Status: Byte; out ReceiptStatus: Byte): Smallint; dispid 5;
    function  CheckActive(out IsActive: WordBool): Smallint; dispid 6;
    function  Init(CheckDateTime: TDateTime): Smallint; dispid 7;
    function  Register(const OldPassword: WideString; const RegNumber: WideString; 
                       const INN: WideString; const NewPassword: WideString): Smallint; dispid 8;
    function  ActivateEJJ: Smallint; dispid 9;
    function  CloseEEJArchive: Smallint; dispid 10;
    function  OpenReceipt(ReceiptType: Smallint; SectionNumber: Smallint; 
                          const OperatorName: WideString; ReceiptNumber: LongWord): Smallint; dispid 11;
    function  PrintString(const Buffer: WideString): Smallint; dispid 12;
    function  CloseReceipt: Smallint; dispid 13;
    function  BreakReceipt: Smallint; dispid 14;
    function  AddArticle(const ArticleName: WideString; const ArticleCode: WideString; 
                         ArticleQuantity: Currency; ArticleTotal: Currency; Department: Smallint; 
                         out ReceiptTotal: Currency): Smallint; dispid 15;
    function  StornoArticle(const ArticleName: WideString; const ArticleCode: WideString; 
                            ArticleQuantity: Currency; ArticleTotal: Currency; 
                            Department: Smallint; out ReceiptTotal: Currency): Smallint; dispid 16;
    function  DiscountPercent(const DiscountName: WideString; Percent: Currency; 
                              out ArticleTotal: Currency): Smallint; dispid 17;
    function  DiscountTotal(const DiscountName: WideString; Total: Currency; 
                            out ArticleTotal: Currency): Smallint; dispid 18;
    function  ExtraPercent(const ExtraName: WideString; Percent: Currency; 
                           out ArticleTotal: Currency): Smallint; dispid 19;
    function  ExtraTotal(const ExtraName: WideString; Total: Currency; out ArticleTotal: Currency): Smallint; dispid 20;
    function  SubTotal(out ReceiptTotal: Currency): Smallint; dispid 21;
    function  Payment(PaymentType: Smallint; PaymentTotal: Currency; out ReceiptTotal: Currency): Smallint; dispid 22;
    function  CashInOut(const BanknoteName: WideString; Total: Currency; out ReceiptTotal: Currency): Smallint; dispid 23;
    function  XReport(const OperatorName: WideString): Smallint; dispid 24;
    function  ZReport(const OperatorName: WideString): Smallint; dispid 25;
    function  FiscShiftReport(IsFull: WordBool; FirstShift: Smallint; LastShift: Smallint; 
                              const FiscPassword: WideString): Smallint; dispid 26;
    function  FiscDateReport(IsFull: WordBool; FirstDate: TDateTime; LastDate: TDateTime; 
                             const FiscPassword: WideString): Smallint; dispid 27;
    function  EEJJournal(ShiftNumber: Smallint): Smallint; dispid 28;
    function  GetReceipt(KPKNumber: LongWord): Smallint; dispid 29;
    function  EEJShiftReport(IsFull: WordBool; FirstShift: Smallint; LastShift: Smallint): Smallint; dispid 30;
    function  EEJDateReport(IsFull: WordBool; FirstDate: TDateTime; LastDate: TDateTime): Smallint; dispid 31;
    function  EEJActivationReport: Smallint; dispid 32;
    function  EEJReport(ShiftNumber: Smallint): Smallint; dispid 33;
    function  ReadConfig(i: Smallint; j: Smallint; out Param: WideString): Smallint; dispid 34;
    function  WriteConfig(i: Smallint; j: Smallint; const Param: WideString): Smallint; dispid 35;
    function  SetDate(NewDateTime: TDateTime): Smallint; dispid 36;
    function  GetEEJStatus: Smallint; dispid 37;
    function  GetCurrentShift(out ShiftNumber: Smallint): Smallint; dispid 38;
    function  GetLastRegShift(out ShiftNumber: Smallint): Smallint; dispid 39;
    function  GetRegNumber(out RegNumber: WideString): Smallint; dispid 40;
    function  OpenCashDrawer: Smallint; dispid 41;
    function  Install(NewDateTime: TDateTime; const SerialNumber: WideString): Smallint; dispid 42;
    function  ReadMemBlock(DataAreaType: Smallint; ByteOffset: Smallint; ByteCount: Smallint; 
                           DataBufferPointer: OleVariant): Smallint; dispid 43;
    function  LoadLogo(ByteCount: Smallint; DataBufferPointer: OleVariant): Smallint; dispid 45;
    function  LoadLogoFromFile(const FileName: WideString): Smallint; dispid 46;
    function  CancelReport: Smallint; dispid 47;
    function  AddBarcode(BarcodeType: Smallint; BarcodeAsDigit: Smallint; BarcodeWidth: Smallint; 
                         BarcodeHeight: Smallint; const BarcodeData: WideString): Smallint; dispid 48;
    function  CutAndPrint: Smallint; dispid 49;
    function  EEJEmergencyShiftClose: Smallint; dispid 50;
    function  EEJShowClientDisplayText(const Row1: WideString; const Row2: WideString): Smallint; dispid 51;
    function  ClearLogo: Smallint; dispid 52;
    function  Init2: Smallint; dispid 44;
    function  Install2(const SerialNumber: WideString): Smallint; dispid 53;
  end;

// *********************************************************************//
// The Class CoSP101FRObject provides a Create and CreateRemote method to          
// create instances of the default interface ISP101FRObject exposed by              
// the CoClass SP101FRObject. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoSP101FRObject = class
    class function Create: ISP101FRObject;
    class function CreateRemote(const MachineName: string): ISP101FRObject;
  end;

implementation

uses ComObj;

class function CoSP101FRObject.Create: ISP101FRObject;
begin
  Result := CreateComObject(CLASS_SP101FRObject) as ISP101FRObject;
end;

class function CoSP101FRObject.CreateRemote(const MachineName: string): ISP101FRObject;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_SP101FRObject) as ISP101FRObject;
end;

end.
