unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComObj, StdCtrls, DateUtils;

type
  TFRTest = class(TForm)
    btnDLL: TButton;
    btnOLE: TButton;
    procedure btnOLEClick(Sender: TObject);
    procedure btnDLLClick(Sender: TObject);
  private
    { Private declarations }
    procedure CheckResult(ARslt: WORD);
  public
    { Public declarations }
  end;

var
  FRTest: TFRTest;

implementation

uses SP101FRK;

{$R *.dfm}

procedure TFRTest.btnOLEClick(Sender: TObject);
var
  opername, ArticleName, ArticleCode: WideString;
  m_RecptNumber: word;
  v: variant;
  RT: currency;
begin
  //������������ ������ � �� ����� OLE-Automation ������
  m_RecptNumber := 0;
  opername := '01����-������';
  ArticleCode := '123456789012';
  ArticleName := '����-�����1';
  try
    v := CreateoleObject('SP101FRKLib.SP101FRObject');

    //������� � FR �� COM1
    CheckResult(v.Connect(1));
    try
      //������������� FR
      CheckResult(v.Init(Now));

      //�������� ����
      CheckResult(v.OpenReceipt(2, 1, opername, m_RecptNumber));

      //���������� �������� �������
      CheckResult(v.AddArticle(ArticleName, ArticleCode, 1.06,1.06,1, RT));

      //���������� ������
      CheckResult(v.PrintString('����� �����'));

      //�������
      CheckResult(v.SubTotal(RT));

      //������
      CheckResult(v.DiscountTotal('������ 1', 0.5, RT));

      //������
      CheckResult(v.Payment(1, RT, RT));

      //�������� ����
      CheckResult(v.CloseReceipt);



      //�������� �����(Z-�����)
      CheckResult(v.ZReport(opername));
    finally
      //�������� ����������� �����.
      v.Disconnect;
    end;
  except
    on E: Exception do begin
      MessageBox(handle, PChar('������ ������ � OLE-��������: '+E.Message), '������', MB_ICONERROR or MB_OK);
      Exit;
    end;
  end;
end;

procedure TFRTest.CheckResult(ARslt: WORD);
begin
  if (ARslt <> 0) then begin
    raise Exception.Create('������ ��� ������ � FR: '+ IntToStr(Arslt));
  end;
end;

procedure TFRTest.btnDLLClick(Sender: TObject);
var
  opername, ArticleName, ArticleCode: WideString;
  m_RecptNumber: word;
  RT: currency;
  hfr: THandle;
  ErrCode: Byte;
  dt: TSPFRDT;
  tm: TSPFRTM;
  year, month, day, hour, minute, second, ms: WORD;
  oname: TSPFR_OPERATOR_NAME;
  aname: TSPFR_ARTICLE_NAME;
  acode: TSPFR_ARTICLE_CODE;
  str: TSPFR_STRING;
  dname: TSPFR_DISCOUNT_NAME;
begin
  //������������ ������ � �� ����� sp101fr.dll

  m_RecptNumber := 0;
  opername := '01����-������';
  ArticleCode := '123456789012';
  ArticleName := '����-�����1';
  try
    hfr := SPFR_CheckPort(1, ErrCode);
    if (hfr <> 0) and (hfr <> INVALID_HANDLE_VALUE) then begin
      try
        //�������������
        DecodeDateTime(now, year, month, day, hour, minute, second, ms);
        dt.day := day;
        dt.month := month;
        dt.year := year;
        tm.hour := hour;
        tm.minute := minute;
        tm.second := second;
        CheckResult(SPFR_Init(hfr, @dt, @tm));

        //�������� ����
        FillChar(oname.s, 21, #0);
        StrPCopy(oname.s, Copy(opername, 1, 20));
        CheckResult(SPFR_OpenReceipt(hfr, 2, 1, @oname, m_RecptNumber));


        //���������� �������� �������
        FillChar(aname.s, 35, #0);
        StrPCopy(aname.s, Copy(ArticleName, 1, 34));
        FillChar(acode.s, 14, #0);
        StrPCopy(acode.s, Copy(ArticleCode, 1, 13));
        CheckResult(SPFR_AddArticle(hfr, @aname, @acode, 1.06, 1.06, 1, RT));


        //���������� ������
        FillChar(str.s, 41, #0);
        StrPCopy(str.s, '����� �����');
        CheckResult(SPFR_PrintString(hfr, @str));


        //�������
        CheckResult(SPFR_SubTotal(hfr, RT));

        //������
        FillChar(dname.s, 33, #0);
        StrPCopy(dname.s, '������ 1');
        CheckResult(SPFR_DiscountTotal(hfr, @dname, 0.5, RT));

        //������
        CheckResult(SPFR_Payment(hfr, 1, RT, RT));

        //�������� ����
        CheckResult(SPFR_CloseReceipt(hfr));



        //�������� �����(Z-�����)
        CheckResult(SPFR_ZReport(hfr, @oname));
      finally
        SPFR_ReleasePort(hfr);
      end;
    end else begin
      MessageBox(handle, PChar('������ ��� ���������� � FR'), '������', MB_ICONERROR or MB_OK);

    end;
  except
    on E: Exception do begin
      MessageBox(handle, PChar('������: '+E.Message), '������', MB_ICONERROR or MB_OK);
      Exit;
    end;
  end;


end;

end.
