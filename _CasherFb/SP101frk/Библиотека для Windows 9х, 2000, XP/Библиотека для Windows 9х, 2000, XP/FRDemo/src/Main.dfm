object FRTest: TFRTest
  Left = 204
  Top = 108
  BorderStyle = bsDialog
  Caption = 'FR Demo'
  ClientHeight = 109
  ClientWidth = 243
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object btnDLL: TButton
    Left = 24
    Top = 40
    Width = 75
    Height = 25
    Caption = 'DLL'
    TabOrder = 0
    OnClick = btnDLLClick
  end
  object btnOLE: TButton
    Left = 136
    Top = 40
    Width = 75
    Height = 25
    Caption = 'OLE'
    TabOrder = 1
    OnClick = btnOLEClick
  end
end
