// fr101win_demo.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "time.h"
#include "windows.h"

#include "../fr101win.h"

int _tmain(int argc, _TCHAR* argv[])
{
	CFR101_WIN obFR(1);
	if(obFR.COMPortOpen() == 0)
	{
		ST_TIMESTAMP stDate;
		struct tm *newtime;
		time_t t = time(0);
        newtime = localtime( &t ); /* Convert to local time. */
		stDate.date.year = newtime->tm_year;
		stDate.date.month = newtime->tm_mon + 1;
		stDate.date.day = newtime->tm_mday;
		stDate.time.hour = newtime->tm_hour;
		stDate.time.minute = newtime->tm_min;
		stDate.time.second = newtime->tm_sec;
		
		//obFR.nFR_Install(&stDate, "123456");
		obFR.nFR_Initialize(&stDate);
		obFR.nFR_WriteConfig(20, 0, "��� �������");

		double dblTemp = 0;
		obFR.nFR_OpenReceipt(2, 50, "��������1", 0);
		obFR.nFR_PrintBarcode(2, 2, 100, 2, "4606272001286");
		obFR.nFR_AddItem("����� #1", "1", 1.00, 50.00, 1, &dblTemp);
		obFR.nFR_AddDiscount(DSC_PERCENT, "���������� ������", 5.00, &dblTemp);
		obFR.nFR_AddMarkup(DSC_PERCENT, "���������� �������", 5.00, &dblTemp);
		obFR.nFR_AddItem("����� #2", "2", 1.00, 100.00, 2, &dblTemp);
		obFR.nFR_VoidItem("����� #2", "2", 1.00, 100.00, 2, &dblTemp);
		obFR.nFR_AddItemExt("����� #3", "3", 2.00, 130.00, 3, 1, &dblTemp);
		obFR.nFR_Subtotal(&dblTemp);
		obFR.nFR_PrintText("��� ��� �����������!");
		obFR.nFR_AddDiscount(DSC_ABSOLUTE, "���������� ������", 5.00, &dblTemp);
		obFR.nFR_Payment(1, 30.00, &dblTemp);
		obFR.nFR_CloseReceipt();
		
		//obFR.nFR_PrintXReport("OPER01");
		obFR.nFR_PrintZReport("OPER01");
		
		//obFR.nFR_PrintFisShiftReport(1, 1, 2, "79113");
		//obFR.nFR_PrintFisDateReport(1, &stDate, &stDate, "79113");
		//obFR.nFR_PrintEejJournal(3);
		//obFR.nFR_PrintEejDocument("904");
		//obFR.nFR_PrintEejShiftReport(1, 1, 2, "79113");
		//obFR.nFR_PrintEejDateReport(1, &stDate, &stDate, "79113");
		//obFR.nFR_PrintActivateReport();
		//obFR.nFR_PrintEejSingleShiftReport(1);
		//Sleep(10);
		//obFR.nFR_BreakReport();

		//char szSerial[100];
		//obFR.nFR_GetSerialNum(szSerial, 100);		
		//UN_FRSTATUS unStatus;
		//obFR.nFR_GetStatus(&unStatus);
		//BYTE dbStatus = 0;
		//obFR.nFR_GetEejStatus(&dbStatus);
		//unsigned short sShift;
		//ST_FRTOTALS stTotals;
		//obFR.nFR_GetEejShift(REQ_CURRENT_SHIFT, &sShift);
		//obFR.nFR_GetEejShift(REQ_LAST_REG_SHIFT, &sShift);
		//obFR.nFR_GetEejTotals(REQ_RETURN_TOTALS, &stTotals);
		//obFR.nFR_GetEejTotals(REQ_SALE_TOTALS, &stTotals);
		//obFR.nFR_EEJ_CloseArchieve();
		//char szBuffer[100];
		//obFR.nFR_ReadConfig(20, 0, szBuffer, sizeof(szBuffer));
		//obFR.nFR_GetDateTime(&stDate);
		//stDate.time.minute += 2;
		//obFR.nFR_SetDateTime(&stDate);
		//obFR.nFR_UpdateCustDisp("LINE1", "LINE2");
		
		//obFR.nFR_Register("79113", "7770", "76543210", "79113");
		//obFR.nFR_EEJ_Activate();

		//obFR.nFR_PaidInOut("10 baksov", 10.00, &dblTemp);

		/*----------------------�������� �������� (��� �������� �����)----------------*/		
		/* ������ ����������� � �� ����� 288 �����, ������ � �� 107 �� 126 �����.     */
/*
		HANDLE hFile=NULL;
		obFR.nFR_ClearLogo();
		hFile=CreateFile("C:\\logo.bmp",GENERIC_READ,NULL,NULL,OPEN_EXISTING,NULL,NULL);
		if (hFile != INVALID_HANDLE_VALUE) 
		{
			DWORD buf_size = GetFileSize(hFile, NULL);
			if (buf_size != INVALID_FILE_SIZE) 
			{
				BYTE* buf = new BYTE[buf_size]; 
				DWORD sizebuf;
				ReadFile(hFile, buf, buf_size, &sizebuf, 0);
				if (sizebuf !=0) 
				{
					obFR.nFR_LoadLogo( sizebuf, buf);
				}
				else
				{
					printf( "nFR_LoadLogo - invalid parameter");
				}
				delete []buf;
			}
			else
			{
				printf( "nFR_LoadLogo - invalid parameter");					
			}
		}
		else
		{
			printf( "nFR_LoadLogo - invalid parameter");
		}		
		obFR.nFR_WriteConfig(5, 0, "1"); // ���������� ���� "�������� �������"
		*/
		/*------------------------------------------------------------------*/			
		obFR.COMPortClose();
	}

	return 0;
}

