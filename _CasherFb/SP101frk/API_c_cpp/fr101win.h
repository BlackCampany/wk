#ifndef _FR101WIN_H
#define _FR101WIN_H

#include "fr101api.h"

#if defined(FR101DLL_EXPORTS) || defined(FR101DLL_IMPORTS)
#define FR101WIN_CLASS

#else
#define FR101WIN_CLASS CFR101_WIN::

class CFR101_WIN: public CFR101_API
{
protected:
        HANDLE handle;
        int nComPort;
public:
	CFR101_WIN(int);
	~CFR101_WIN();
	void NativeToDos(char *, int);
	void DosToNative(char *, int);
	int  COMPortOpen();
	void COMPortClear();
	void COMPortClose();
	int  COMPortRead(char *, int, int);
	int  COMPortWrite(char *, int);
};

#endif
#endif /* _FR101WIN_H */
