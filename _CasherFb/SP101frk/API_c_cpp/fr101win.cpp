#include "windows.h"
#include "stdio.h"
#include "fr101win.h"

#if defined(FR101DLL_EXPORTS) || defined(FR101DLL_IMPORTS)

HANDLE handle = INVALID_HANDLE_VALUE;
DLLAPI int nComPort = 1;

#else

FR101WIN_CLASS CFR101_WIN(int nCom) : nComPort(nCom), handle(INVALID_HANDLE_VALUE)
{
  CFR101_API::sFR_AsyncMode = 0;
  CFR101_API::dbFR_Packet = 0x20;
}

FR101WIN_CLASS ~CFR101_WIN(void)
{
}

#endif

void FR101WIN_CLASS NativeToDos(char *szBuffer, int)
{
  CharToOem(szBuffer, szBuffer);
}

void FR101WIN_CLASS DosToNative(char *szBuffer, int)
{
  OemToChar(szBuffer, szBuffer);	
}

DLLAPI int FR101WIN_CLASS COMPortOpen()
{
  char szComPort[20];
  COMMTIMEOUTS CommTimeOuts;
  DCB dcb;

  sprintf(szComPort, "COM%i", nComPort);
  handle = CreateFile(szComPort, GENERIC_READ | GENERIC_WRITE,
                      NULL, NULL, OPEN_EXISTING, 0, NULL);

  if(handle == INVALID_HANDLE_VALUE) 
  {
    return -1;
  }

  if(!SetupComm(handle, 2048, 2048)) 
  {
    COMPortClose();
    return -1; 
  }
		
  if(!GetCommState(handle, &dcb)) 
  {
    COMPortClose();
    return -1; 
  }

  dcb.BaudRate = CBR_19200;
  dcb.fBinary  = TRUE;
  dcb.ByteSize = 8;
  dcb.Parity   = 0;
  dcb.StopBits = 0;

  if(!SetCommState(handle, &dcb))
  {
    COMPortClose();
    return -1; 
  }
		
  Sleep(3000);

  if(!GetCommTimeouts(handle, &CommTimeOuts)) 
  {
    COMPortClose();
    return -1; 
  }

  CommTimeOuts.ReadIntervalTimeout         = 200;
  CommTimeOuts.ReadTotalTimeoutMultiplier  = 200;
  CommTimeOuts.ReadTotalTimeoutConstant    = 200;
  CommTimeOuts.WriteTotalTimeoutMultiplier = 0;
  CommTimeOuts.WriteTotalTimeoutConstant   = 0;

  if(!SetCommTimeouts(handle, &CommTimeOuts)) 
  {
    COMPortClose();
    return -1; 
  }

  COMPortClear();
  return 0;
}

DLLAPI void FR101WIN_CLASS COMPortClear() 
{
  if(handle != INVALID_HANDLE_VALUE)
  {
    PurgeComm(handle, PURGE_RXCLEAR);
    PurgeComm(handle, PURGE_TXCLEAR);
  }
}

DLLAPI void FR101WIN_CLASS COMPortClose() 
{
  if(handle != INVALID_HANDLE_VALUE)
  {
    CloseHandle(handle);
    handle = INVALID_HANDLE_VALUE;
  }
}

int FR101WIN_CLASS COMPortRead(char *buffer, int numbytes, int) 
{ 
  DWORD temp;
  COMSTAT          ComState;
  if(handle != INVALID_HANDLE_VALUE)
  {
    ClearCommError(handle, &temp, &ComState);
    if(!temp)
    {
      BOOL result = ReadFile(handle, buffer, numbytes, &temp, 0);
      if(result)
      {
        return temp;
      }
    }
  }
  return 0;
}

int FR101WIN_CLASS COMPortWrite(char *buffer, int numbytes) 
{
  DWORD temp;
  COMSTAT          ComState;
  if(handle != INVALID_HANDLE_VALUE)
  {
    if(numbytes)
    {
      ClearCommError(handle, &temp, &ComState);
      if((numbytes + ComState.cbOutQue) >= 2048)
      {
        return -1;
      }

      BOOL result = WriteFile(handle, buffer, numbytes, &temp, 0);
      if(result)
      {
        return temp;
      }
    }
  }
  return 0;
}
