#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>

#include "fr101api.h"

#if !defined(__linux__)
#define snprintf _snprintf
#endif

#define	STX	0x02
#define	ETX	0x03
#define	ENQ	0x05
#define	ACK	0x06
#define	CAN	0x18
#define	FS	0x1C

#define STR_FS "\x1C"

#if !defined(__cplusplus) || defined(FR101DLL_EXPORTS) || defined(FR101DLL_IMPORTS)

BYTE  dbFR_Packet = 0x20;
short sFR_AsyncMode = 0;

char szFR_Command[FR_SIZEOF_BUFFER];
char szFR_Answer[FR_SIZEOF_BUFFER];

#endif

/* ************************************************************************** *
 FUNCTIONS ONLY FOR INTERNAL USAGE
 * ************************************************************************** */
BYTE FR101API_CLASS dbFR_GetLRC(char *snzData, int nSize)
/* Calculate LRC for data */
{
  BYTE dbLRC = 0;
  for( ; nSize > 0; nSize --, snzData ++)
  {
    dbLRC ^= (BYTE)*snzData;
  }
  return dbLRC;
}

int FR101API_CLASS nFR_CheckLRC(char *snzData, int nSize)
/* Validate LRC, taken from last 2 bytes of data */
{
  int nRet = ERROR, nLRC = 0;
  char szTemp[3];

  if(nSize > 2)
  {
    memcpy(szTemp, snzData + nSize - 2, 2);
    szTemp[2] = 0;
    sscanf(szTemp, "%02X", &nLRC);
    if(nLRC == (int)dbFR_GetLRC(snzData, nSize - 2))
    {
      nRet = SUCCESS;
    }
  }
  return nRet;
}

int FR101API_CLASS nFR_BuildPacket(char *szCommand, char *szBuffer, int nSize)
{
  int nRet = ERROR, nLen;
  BYTE dbLRC = 0;
  if(snprintf(szBuffer, nSize, "%cPONE%c%s%c00", STX, dbFR_Packet, szCommand, ETX) > 0)
  {
    nLen = (int)strlen(szBuffer);
    if(nLen <= nSize)
    {
      dbLRC = dbFR_GetLRC(szBuffer + 1, nLen - 3);
      sprintf(szBuffer + nLen - 2, "%02X", dbLRC);
      nRet = SUCCESS;
    }
  }
  return nRet;
}

int FR101API_CLASS nFR_SendPacket(char *szCommand)
{
  int nRet = SUCCESS;
  COMPortClear();
  COMPortWrite(szCommand, (int)strlen(szCommand));
  if(dbFR_Packet == 0xFD)
  {
    dbFR_Packet = 0x20;
  }
  else
  {
    dbFR_Packet ++;
  }
  return nRet;
}

DLLAPI int FR101API_CLASS nFR_IsActive()
{
   int nRet = ERROR;
   BYTE dbCommand = ENQ, dbResult = 0;
   COMPortWrite((char *)&dbCommand, 1);
   COMPortRead((char *)&dbResult, 1, FR_INTERCHAR_TIMEOUT);
   if(dbResult == ACK)
   {
     nRet = SUCCESS;
   }
   return nRet;
}

void FR101API_CLASS nFR_SetMode(short sAsyncMode)
{
  sFR_AsyncMode = sAsyncMode;
}

int FR101API_CLASS nFR_ReceivePacket(int *pnStatus, char *szAnswer, int nSize)
{
  int  i = 0, nRet = SUCCESS;
  char szBuffer[FR_SIZEOF_BUFFER];
  char szTemp[3];

  BYTE dbCommand = ENQ;
  char bWaitACK = 0, bWaitEOT = 3, bWaitSTX = 1;

  time_t tTimer = time(0);
  *szAnswer = 0; *pnStatus = 0;

  do
  {
    nRet = COMPortRead(&szBuffer[i], 1, FR_INTERCHAR_TIMEOUT);
    if(nRet < 0 || (nRet == 0 && bWaitACK))
    {
      nRet = FR_ERR_DEVICE_UNPLUGGED;
      break;
    }
    else if(nRet == 0)
    {
      if(!bWaitSTX)
      {
        nRet = FR_ERR_PROTOCOL;
        break;
      }

      COMPortWrite((char *)&dbCommand, 1);
      bWaitACK = 1;
      continue;
    }
    else if(szBuffer[i] == ACK)
    {
      if(sFR_AsyncMode && i == 0)
      {
        nRet = FR_BUSY;
        break;
      }
      else
      {
        bWaitACK = 0;
        continue;
      }
    }
	else if(szBuffer[i] != STX && bWaitSTX)
	{
		continue;
	}
	else if(szBuffer[i] == STX)
	{
		i ++;
		bWaitSTX = 0;
	}
    else if(szBuffer[i ++] == ETX || bWaitEOT < 3)
    {
      bWaitEOT --;
    }
  } while(bWaitEOT);

  szBuffer[i] = 0;
  if(i >= 9 && szBuffer[i - 3] == ETX)
  {
    nRet = nFR_CheckLRC(szBuffer + 1, i - 1);
    if(nRet)
    {
      nRet = FR_ERR_INVALID_CHECKSUM;
    }
    else
    {
      /* Remove STX, ID, COMMAND, ERROR, ETX, LRC */
      szBuffer[i - 3] = 0;
      strncpy(szAnswer, szBuffer + 6, nSize);
      szAnswer[nSize - 1] = 0;

      /* ERROR */
      memcpy(szTemp, szBuffer + 4, 2);
      szTemp[2] = 0;
      sscanf(szTemp, "%02X", pnStatus);

      nRet = SUCCESS;
    }
  }
  else if(nRet != FR_ERR_DEVICE_UNPLUGGED && nRet != FR_BUSY)
  {
    nRet = FR_ERR_DEVICE_TIMEOUT;
  }

  return nRet;
}

int FR101API_CLASS nFR_RunCommand(char *szCommand, char *szAnswer, int nSize)
{
  int nRet = ERROR, nStatus;
  char szBuffer[FR_SIZEOF_BUFFER];

  NativeToDos(szCommand, (int)strlen(szCommand));

  nRet = nFR_BuildPacket(szCommand, szBuffer, sizeof(szBuffer));
  if(nRet == SUCCESS)
  {
    nRet = nFR_SendPacket(szBuffer);
    if(nRet == SUCCESS)
    {
      nRet = nFR_ReceivePacket(&nStatus, szAnswer, nSize);
      if(nRet == SUCCESS)
      {
		DosToNative(szAnswer, (int)strlen(szAnswer));
        nRet = nStatus;
      }
    }
  }
  return nRet;
}

/* ************************************************************************** *
 FUNCTIONS FOR STARTUP COMMAND GROUP IMPLEMENTATION
 * ************************************************************************** */
DLLAPI int FR101API_CLASS nFR_Initialize(ST_TIMESTAMP *pstDate)
{
  int nRet = ERROR;
  if(pstDate)
  {
    if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X%02d%02d%02d%c%02d%02d%02d%c",
                FR_INITIALIZE,
                pstDate->date.day, pstDate->date.month, pstDate->date.year % 100, FS,
                pstDate->time.hour, pstDate->time.minute, pstDate->time.second, FS) > 0)
    {
      nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
    }
  }
  return nRet;
}

DLLAPI int FR101API_CLASS nFR_Register(char *szPassword, char *szRegNum, char *szINN, char *szNewPassword)
{
  int nRet = ERROR;
  if(szPassword && szRegNum && szINN && szNewPassword)
  {
	if(strspn(szRegNum, "0123456789") == strlen(szRegNum) &&
	   strspn(szINN, "0123456789") == strlen(szINN))
	{
		if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X%s%c%s%c%s%c%s%c",
				    FR_REGISTER,
					szPassword, FS,
					szRegNum, FS,
					szINN, FS,
					szNewPassword, FS) > 0)
		{
		  nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
		}
	}
  }
  return nRet;
}

DLLAPI int FR101API_CLASS nFR_EEJ_Activate()
{
  int nRet = ERROR;
  if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X",
              FR_EEJ_ACTIVATE) > 0)
  {
    nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
  }
  return nRet;
}

DLLAPI int FR101API_CLASS nFR_EEJ_CloseArchieve()
{
  int nRet = ERROR;
  if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X",
              FR_EEJ_CLOSE_ARCHIEVE) > 0)
  {
    nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
  }
  return nRet;
}

/* ************************************************************************** *
 FUNCTIONS FOR RECEIPT COMMAND GROUP IMPLEMENTATION
 * ************************************************************************** */
DLLAPI int FR101API_CLASS nFR_OpenReceipt(unsigned short usType, unsigned short usDepartment,
                    CONST char *szOperator, unsigned long  ulReceiptNum)
{
  int nRet = ERROR;
  if(szOperator)
  {
    if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X%hu%c%hu%c%s%c%lu%c",
                FR_OPEN_RECEIPT,
                usType, FS,
                usDepartment, FS,
                szOperator, FS,
                ulReceiptNum, FS) > 0)
    {
      nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
    }
  }
  return nRet;
}

DLLAPI int FR101API_CLASS nFR_PrintText(CONST char *szText)
{
  int nRet = ERROR;
  if(szText)
  {
    if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X%s%c",
                FR_PRINT_TEXT,
                szText, FS) > 0)
    {
      nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
    }
  }
  return nRet;
}

DLLAPI int FR101API_CLASS nFR_PrintTextExt(CONST char *szText, int nFont)
{
	int nRet = ERROR;
	if(szText)
	{
		if(nFont)
		{
			if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X%s%c%d%c",
				FR_PRINT_TEXT,
				szText, FS, nFont, FS) > 0)
			{
				nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
			}
		}
		else
		{ 
			if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X%s%c",
				FR_PRINT_TEXT,
				szText, FS) > 0)
			{
				nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
			}
		}
	}
	return nRet;
}

DLLAPI int FR101API_CLASS nFR_CloseReceipt()
{
  int nRet = ERROR;
  if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X",
              FR_CLOSE_RECEIPT) > 0)
  {
    nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
  }
  return nRet;
}

DLLAPI int FR101API_CLASS nFR_CancelReceipt()
{
  int nRet = ERROR;
  if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X",
              FR_CANCEL_RECEIPT) > 0)
  {
    nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
  }
  return nRet;
}

DLLAPI int FR101API_CLASS nFR_PrintBarcode(unsigned short usAlign, unsigned short usWidth,
                     unsigned short usHeight, unsigned short usType,
                     CONST char *szBarcode)
{
  int nRet = ERROR;
  if(szBarcode)
  {
    if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X%hu%c%hu%c%hu%c%hu%c%s%c",
                FR_PRINT_BARCODE,
                usAlign, FS,
                usWidth, FS,
                usHeight, FS,
                usType, FS,
                szBarcode, FS) > 0)
    {
      nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
    }
  }
  return nRet;
}

DLLAPI int FR101API_CLASS nFR_Banner()
{
	int nRet = ERROR;
	if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X",
		FR_BANNER) > 0)
	{
		nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
	}
	return nRet;
}

DLLAPI int FR101API_CLASS nFR_AddItem(CONST char *szName, CONST char *szCode, double dblQuantity, double dblPrice,
                unsigned short usDepartment, double *pdblTotal)
{
  int nRet = ERROR; char *pszToken = NULL;
  if(pdblTotal)
  {
    *pdblTotal = 0.0;
  }
  if(szCode && szName)
  {
    if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X%s%c%s%c%.4f%c%.4f%c%hu%c",
                FR_ADD_ITEM,
                szName, FS,
                szCode, FS,
                dblQuantity, FS,
                dblPrice, FS,
                usDepartment, FS) > 0)
    {
      nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
      if(nRet == FR_SUCCESS && pdblTotal)
      {
        pszToken = strtok(szFR_Answer, STR_FS);
        if(pszToken)
        {
          *pdblTotal = atof(pszToken);
        }
      }
    }
  }
  return nRet;
}

DLLAPI int FR101API_CLASS nFR_AddItemExt(CONST char *szName, CONST char *szCode, double dblQuantity, double dblPrice,
									  unsigned short usDepartment, long lVatIndex, double *pdblTotal)
{
	int nRet = ERROR; char *pszToken = NULL;
	if(pdblTotal)
	{
		*pdblTotal = 0.0;
	}
	if(szCode && szName)
	{
		if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X%s%c%s%c%.4f%c%.4f%c%hu%c%d%c",
			FR_ADD_ITEM,
			szName, FS,
			szCode, FS,
			dblQuantity, FS,
			dblPrice, FS,
			usDepartment, FS,
			lVatIndex, FS) > 0)
		{
			nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
			if(nRet == FR_SUCCESS && pdblTotal)
			{
				pszToken = strtok(szFR_Answer, STR_FS);
				if(pszToken)
				{
					*pdblTotal = atof(pszToken);
				}
			}
		}
	}
	return nRet;
}

DLLAPI int FR101API_CLASS nFR_StoreReceipt(CONST char *szText)
{
	int nRet = ERROR;
	if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X%s%c",
		FR_STORE_RECEIPT,
		szText, FS) > 0)
	{
		nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
	}
	return nRet;
}

DLLAPI int FR101API_CLASS nFR_VoidItem(CONST char *szName, CONST char *szCode, double dblQuantity, double dblPrice,
                 unsigned short usDepartment, double *pdblTotal)
{
  int nRet = ERROR; char *pszToken = NULL;
  if(pdblTotal)
  {
    *pdblTotal = 0.0;
  }
  if(szCode && szName)
  {
    if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X%s%c%s%c%.4f%c%.4f%c%hu%c",
                FR_VOID_ITEM,
                szName, FS,
                szCode, FS,
                dblQuantity, FS,
                dblPrice, FS,
                usDepartment, FS) > 0)
    {
      nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
      if(nRet == FR_SUCCESS && pdblTotal)
      {
        pszToken = strtok(szFR_Answer, STR_FS);
        if(pszToken)
        {
          *pdblTotal = atof(pszToken);
        }
      }
    }
  }
  return nRet;
}

DLLAPI int FR101API_CLASS nFR_AddDiscount(enum EDiscType eType, CONST char *szName, double dblAmount, double *pdblTotal)
{
  int nRet = ERROR; char *pszToken = NULL;
  if(pdblTotal)
  {
    *pdblTotal = 0.0;
  }
  if(szName)
  {
    if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X%c%c%s%c%.4f%c",
                FR_ADD_DISCOUNT,
                eType, FS,
                szName, FS,
                dblAmount, FS) > 0)
    {
      nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
      if(nRet == FR_SUCCESS && pdblTotal)
      {
        pszToken = strtok(szFR_Answer, STR_FS);
        if(pszToken)
        {
          *pdblTotal = atof(pszToken);
        }
      }
    }
  }
  return nRet;
}

DLLAPI int FR101API_CLASS nFR_AddMarkup(enum EDiscType eType, CONST char *szName, double dblAmount, double *pdblTotal)
{
  int nRet = ERROR; char *pszToken = NULL;
  if(pdblTotal)
  {
    *pdblTotal = 0.0;
  }
  if(szName)
  {
    if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X%c%c%s%c%.4f%c",
                FR_ADD_MARKUP,
                eType, FS,
                szName, FS,
                dblAmount, FS) > 0)
    {
      nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
      if(nRet == FR_SUCCESS && pdblTotal)
      {
        pszToken = strtok(szFR_Answer, STR_FS);
        if(pszToken)
        {
          *pdblTotal = atof(pszToken);
        }
      }
    }
  }
  return nRet;
}


DLLAPI int FR101API_CLASS nFR_Subtotal(double *pdblTotal)
{
  int nRet = ERROR; char *pszToken = NULL;
  if(pdblTotal)
  {
    *pdblTotal = 0.0;
  }

  if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X",
              FR_SUBTOTAL) > 0)
  {
    nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
    if(nRet == FR_SUCCESS && pdblTotal)
    {
      pszToken = strtok(szFR_Answer, STR_FS);
      if(pszToken)
      {
        *pdblTotal = atof(pszToken);
      }
    }
  }
  return nRet;
}

DLLAPI int FR101API_CLASS nFR_SetTaxAmount(short usType, double dblAmount)
{
	int nRet = ERROR; 
	if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X%hu%c%.4f%c",
		FR_SET_TAX_AMOUNT,
		usType, FS,
		dblAmount, FS) > 0)
	{
		nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
	}
	return nRet;
}

DLLAPI int FR101API_CLASS nFR_Payment(short usType, double dblAmount, double *pdblTotal)
{
  int nRet = ERROR; char *pszToken = NULL;
  if(pdblTotal)
  {
    *pdblTotal = 0.0;
  }

  if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X%hu%c%.4f%c",
              FR_PAYMENT,
              usType, FS,
              dblAmount, FS) > 0)
  {
    nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
    if(nRet == FR_SUCCESS && pdblTotal)
    {
      pszToken = strtok(szFR_Answer, STR_FS);
      if(pszToken)
      {
        *pdblTotal = atof(pszToken);
      }
    }
  }
  return nRet;
}

DLLAPI int FR101API_CLASS nFR_PaidInOut(CONST char *szBanknote, double dblAmount, double *pdblTotal)
{
  int nRet = ERROR; char *pszToken = NULL;
  if(pdblTotal)
  {
    *pdblTotal = 0.0;
  }

  if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X%s%c%.4f%c",
              FR_PAID_IN_OUT,
              szBanknote, FS,
              dblAmount, FS) > 0)
  {
    nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
    if(nRet == FR_SUCCESS && pdblTotal)
    {
      pszToken = strtok(szFR_Answer, STR_FS);
      if(pszToken)
      {
        *pdblTotal = atof(pszToken);
      }
    }
  }
  return nRet;
}

/* ************************************************************************** *
 FUNCTIONS FOR REPORT COMMAND GROUP IMPLEMENTATION
 * ************************************************************************** */
DLLAPI int FR101API_CLASS nFR_PrintXReport(CONST char *szOperator)
{
  int nRet = ERROR;
  if(szOperator)
  {
    if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X%s%c",
                FR_PRINT_X_REP,
                szOperator, FS) > 0)
    {
      nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
    }
  }

  return nRet;
}

DLLAPI int FR101API_CLASS nFR_PrintZReport(CONST char *szOperator)
{
  int nRet = ERROR;
  if(szOperator)
  {
    if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X%s%c",
                FR_PRINT_Z_REP,
                szOperator, FS) > 0)
    {
      nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
    }
  }

  return nRet;
}

DLLAPI int FR101API_CLASS nFR_PrintFisShiftReport(unsigned short usType, unsigned short usFirstShift,
                            unsigned short usLastShift, char *szPassword)
{
  int nRet = ERROR;
  if(szPassword)
  {
    if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X%hu%c%hu%c%hu%c%s%c",
                FR_PRINT_FIS_SHIFT_REP,
		usType, FS,
		usFirstShift, FS,
		usLastShift, FS,
                szPassword, FS) > 0)
    {
      nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
    }
  }

  return nRet;
}

DLLAPI int FR101API_CLASS nFR_PrintFisDateReport(unsigned short usType, ST_TIMESTAMP *pstFirstDate,
                           ST_TIMESTAMP *pstLastDate, char *szPassword)
{
  int nRet = ERROR;
  if(pstFirstDate && pstLastDate && szPassword)
  {
    if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X%hu%c%02d%02d%02d%c%02d%02d%02d%c%s%c",
                FR_PRINT_FIS_DATE_REP,
		usType, FS,
                pstFirstDate->date.day, pstFirstDate->date.month, pstFirstDate->date.year % 100, FS,
                pstLastDate->date.day, pstLastDate->date.month, pstLastDate->date.year % 100, FS,
                szPassword, FS) > 0)
    {
      nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
    }
  }

  return nRet;
}

DLLAPI int FR101API_CLASS nFR_PrintEejJournal(unsigned short usShift)
{
  int nRet = ERROR;
  if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X%hu%c",
              FR_PRINT_EEJ_JOURNAL,
              usShift, FS) > 0)
    {
      nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
    }

  return nRet;
}

DLLAPI int FR101API_CLASS nFR_PrintEejDocument(CONST char *szKPK)
{
  int nRet = ERROR;
  if(szKPK)
  {
    if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X%s%c",
                FR_PRINT_EEJ_DOCUMENT,
                szKPK, FS) > 0)
    {
      nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
    }
  }
  return nRet;
}

DLLAPI int FR101API_CLASS nFR_PrintEejShiftReport(unsigned short usType, unsigned short usFirstShift,
                            unsigned short usLastShift, CONST char *szPassword)
{
  int nRet = ERROR;
  if(szPassword)
  {
    if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X%hu%c%hu%c%hu%c%s%c",
                FR_PRINT_EEJ_SHIFT_REP,
		usType, FS,
		usFirstShift, FS,
		usLastShift, FS,
                szPassword, FS) > 0)
    {
      nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
    }
  }

  return nRet;
}

DLLAPI int FR101API_CLASS nFR_PrintEejDateReport(unsigned short usType, ST_TIMESTAMP *pstFirstDate,
                           ST_TIMESTAMP *pstLastDate, CONST char *szPassword)
{
  int nRet = ERROR;
  if(pstFirstDate && pstLastDate && szPassword)
  {
    if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X%hu%c%02d%02d%02d%c%02d%02d%02d%c%s%c",
                FR_PRINT_EEJ_DATE_REP,
		usType, FS,
                pstFirstDate->date.day, pstFirstDate->date.month, pstFirstDate->date.year % 100, FS,
                pstLastDate->date.day, pstLastDate->date.month, pstLastDate->date.year % 100, FS,
                szPassword, FS) > 0)
    {
      nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
    }
  }

  return nRet;
}

DLLAPI int FR101API_CLASS nFR_PrintActivateReport()
{
  int nRet = ERROR;
  if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X",
              FR_PRINT_EEJ_ACTIVATE_REP) > 0)
  {
    nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
  }
  return nRet;
}

DLLAPI int FR101API_CLASS nFR_PrintEejSingleShiftReport(unsigned short usShift)
{
  int nRet = ERROR;
  if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X%hu%c",
              FR_PRINT_EEJ_SINGLE_SHIFT_REP,
              usShift, FS) > 0)
    {
      nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
    }

  return nRet;
}

DLLAPI int FR101API_CLASS nFR_EmergentShiftClose()
{
  int nRet = ERROR;
  if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X",
              FR_EMERGENT_CLOSE_SHIFT) > 0)
  {
    nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
  }
  return nRet;
}

DLLAPI int FR101API_CLASS nFR_OpenDrawer()
{
  int nRet = ERROR;
  if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X",
              FR_OPEN_DRAWER) > 0)
  {
    nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
  }
  return nRet;
}

DLLAPI int FR101API_CLASS nFR_UpdateCustDisp(CONST char *szLine1, CONST char *szLine2)
{
  int nRet = ERROR;
  if(szLine1 && szLine2)
  {
    if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X%s%c%s%c",
                FR_PRINT_CUST_DISP_TEXT,
                szLine1, FS,
                szLine2, FS) > 0)
    {
      nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
    }
  }
  return nRet;
}

DLLAPI int FR101API_CLASS nFR_BreakReport()
{
  BYTE dbCommand = CAN;
  COMPortWrite((char *)&dbCommand, 1);
  return SUCCESS;
}

/* ************************************************************************** *
 FUNCTIONS FOR SYSTEM COMMAND GROUP IMPLEMENTATION
 * ************************************************************************** */
DLLAPI int FR101API_CLASS nFR_GetStatus(UN_FRSTATUS *punStatus)
{
  int i, nRet = ERROR; char *pszToken = NULL;

  if(punStatus)
  {
    memset(punStatus, 0, sizeof(UN_FRSTATUS));
  }

  if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X",
              FR_GET_STATUS) > 0)
  {
    nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
    if(nRet == FR_SUCCESS && punStatus)
    {
      pszToken = strtok(szFR_Answer, STR_FS);
      for(i = 0; i < 3 && pszToken != NULL; i ++)
      {
        punStatus->dbStatus[i] = (unsigned char)atoi(pszToken);
        pszToken = strtok(NULL, STR_FS);
      }
    }
  }
  return nRet;
}

DLLAPI int FR101API_CLASS nFR_ReadConfig(unsigned short usRow, unsigned short usColumn,
                   char *szValue, int nSize)
{
  int nRet = ERROR; char *pszToken = NULL;

  if(szValue)
  {
    *szValue = 0;
  }

  if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X%hu%c%hu%c",
              FR_READ_CONFIG,
              usRow, FS,
              usColumn, FS) > 0)
  {
    nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
    if(nRet == FR_SUCCESS && szValue)
    {
      pszToken = strtok(szFR_Answer, STR_FS);
      if(pszToken)
      {
        snprintf(szValue, nSize, "%s", pszToken);
      }
    }
  }
  return nRet;
}

DLLAPI int FR101API_CLASS nFR_WriteConfig(unsigned short usRow, unsigned short usColumn,
                    CONST char *szValue)
{
  int nRet = ERROR;

  if(szValue)
  {
    if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X%hu%c%hu%c%s%c",
                FR_WRITE_CONFIG,
                usRow, FS,
                usColumn, FS,
                szValue, FS) > 0)
    {
      nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
    }
  }
  return nRet;
}

DLLAPI int FR101API_CLASS nFR_SetDateTime(ST_TIMESTAMP *pstDate)
{
  int nRet = ERROR;

  if(pstDate)
  {
    if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X%02d%02d%02d%c%02d%02d%02d%c",
                FR_SET_DATE_TIME,
                pstDate->date.day, pstDate->date.month, pstDate->date.year % 100, FS,
                pstDate->time.hour, pstDate->time.minute, pstDate->time.second, FS) > 0)
    {
      nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
    }
  }
  return nRet;
}

DLLAPI int FR101API_CLASS nFR_GetEejStatus(BYTE *pdbStatus)
{
  int nRet = ERROR; char *pszToken = NULL;

  if(pdbStatus)
  {
    *pdbStatus = 0;
  }

  if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X",
              FR_GET_EEJ_STATUS) > 0)
  {
    nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
    if(nRet == FR_SUCCESS && pdbStatus)
    {
      pszToken = strtok(szFR_Answer, STR_FS);
      if(pszToken)
      {
        *pdbStatus = (unsigned char)atoi(pszToken);
      }
    }
  }
  return nRet;
}

DLLAPI int FR101API_CLASS nFR_GetEejShift(enum ERequestType eType, unsigned short *pusShift)
{
  int nRet = ERROR; char *pszToken = NULL;
  if(pusShift)
  {
    *pusShift = 0;
  }
  if(eType == REQ_CURRENT_SHIFT || eType == REQ_LAST_REG_SHIFT)
  {
    if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X%c%c",
                FR_GET_SHIFT_STATUS,
                eType, FS) > 0)
    {
      nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
      if(nRet == FR_SUCCESS && pusShift)
      {
        pszToken = strtok(szFR_Answer, STR_FS);
        if(pszToken)
        {
          *pusShift = (unsigned char)atoi(pszToken);
        }
      }
    }
  }
  return nRet;
}

DLLAPI int FR101API_CLASS nFR_GetEejTotals(enum ERequestType eType, ST_FRTOTALS *pstTotals)
{
  int i, nRet = ERROR; char *pszToken = NULL;
  if(pstTotals)
  {
    memset(pstTotals, 0, sizeof(ST_FRTOTALS));
  }

  if(eType == REQ_SALE_TOTALS || eType == REQ_RETURN_TOTALS)
  {
    if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X%c%c",
                FR_GET_SHIFT_STATUS,
                eType, FS) > 0)
    {
      nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
      if(nRet == FR_SUCCESS && pstTotals)
      {
        pszToken = strtok(szFR_Answer, STR_FS);
        for(i = 0; i < 16 && pszToken != NULL; i ++)
        {
          pstTotals->dblTotal[i] = atof(pszToken);
          pszToken = strtok(NULL, STR_FS);
        }
      }
    }
  }
  return nRet;
}

DLLAPI int FR101API_CLASS nFR_GetCashInDrawer( double *pdCashInDrawerTotal )
{
	int nRet = ERROR; char *pszToken = NULL;
	if( pdCashInDrawerTotal )
	{
		*pdCashInDrawerTotal = 0;
	}

	if( snprintf(szFR_Command, sizeof(szFR_Command), "%02X%c%c",
		FR_GET_SHIFT_STATUS,
		'5', FS) > 0)
	{
		nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
		if(nRet == FR_SUCCESS && pdCashInDrawerTotal)
		{
			pszToken = strtok(szFR_Answer, STR_FS);
			if( pszToken ) *pdCashInDrawerTotal = atof( pszToken );
		}
	}
	return nRet;
}

DLLAPI int FR101API_CLASS nFR_GetSerialNum(char *szValue, int nSize)
{
  int nRet = ERROR; char *pszToken = NULL;
  if(szValue)
  {
    *szValue = 0;
  }

  if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X",
              FR_GET_KKM_SERIAL_NUM) > 0)
  {
    nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
    if(nRet == FR_SUCCESS && szValue)
    {
      pszToken = strtok(szFR_Answer, STR_FS);
      if(pszToken)
      {
        snprintf(szValue, nSize, "%s", pszToken);
      }
    }
  }
  return nRet;
}

DLLAPI int FR101API_CLASS nFR_GetDateTime(ST_TIMESTAMP *pstDate)
{
  int nRet = ERROR; char *pszToken = NULL;
  int n1, n2, n3;

  if(pstDate)
  {
    memset(pstDate, 0, sizeof(ST_TIMESTAMP));
  }

  if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X",
              FR_GET_DATE_TIME) > 0)
  {
    nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
    if(nRet == FR_SUCCESS && pstDate)
    {
      pszToken = strtok(szFR_Answer, STR_FS);
      if(pszToken)
      {
        if(sscanf(pszToken, "%02d%02d%02d", &n1, &n2, &n3) == 3)
		{
		  pstDate->date.day   = (unsigned char)n1;
		  pstDate->date.month = (unsigned char)n2;
		  pstDate->date.year  = n3 + 2000;
		}
        pszToken = strtok(NULL, STR_FS);
        if(pszToken)
        {
          if(sscanf(pszToken, "%02d%02d%02d", &n1, &n2, &n3) == 3)
		  {
			pstDate->time.hour   = (unsigned char)n1;
			pstDate->time.minute = (unsigned char)n2;
			pstDate->time.second = (unsigned char)n3;
		  }
        }
      }
    }
  }
  return nRet;
}

DLLAPI int FR101API_CLASS nFR_GetDriverOpened(BYTE *pdbStatusOpened)
{
  int nRet = ERROR; char *pszToken = NULL;

  if(pdbStatusOpened)
  {
    *pdbStatusOpened = 0;
  }

  if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X",
              FR_GET_DRIVER_OPENED) > 0)
  {
    nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
    if(nRet == FR_SUCCESS && pdbStatusOpened)
    {
      pszToken = strtok(szFR_Answer, STR_FS);
      if(pszToken)
      {
        *pdbStatusOpened = (unsigned char)atoi(pszToken);
      }
    }
  }
  return nRet;
}


DLLAPI int  FR101API_CLASS nFR_GetFrID(short int *pdbFrId)
{
  int nRet = ERROR; char *pszToken = NULL;
  if(pdbFrId)
  {
    *pdbFrId = 0;
  }
  if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X%c%c",
                FR_GET_SHIFT_STATUS,
                '0', FS) > 0)
  {
    nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
    if(nRet == FR_SUCCESS && pdbFrId)
    {
      pszToken = strtok(szFR_Answer, STR_FS);
      if(pszToken)
      {
        *pdbFrId = (unsigned char)atoi(pszToken);
      }
    }
  }
  return nRet;
}


DLLAPI int  FR101API_CLASS nFR_DoCommand(BYTE bCommand, char *szInOutBuffer, int nBufferSize)
{
  int nRet = ERROR; 
  if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X%s", bCommand,
                szInOutBuffer) > 0)
  {
    nRet = nFR_RunCommand(szFR_Command, szInOutBuffer, nBufferSize);
  }
  return nRet;
}


/* ************************************************************************** *
 FUNCTIONS FOR INTERNAL COMMAND GROUP IMPLEMENTATION
 * ************************************************************************** */
DLLAPI int FR101API_CLASS nFR_Install(ST_TIMESTAMP *pstDate, char *szSerialNum)
{
  int nRet = ERROR;

  if(pstDate && szSerialNum)
  {
    if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X%02d%02d%02d%c%02d%02d%02d%c%s%c",
                FR_INSTALL_KKM,
                pstDate->date.day, pstDate->date.month, pstDate->date.year % 100, FS,
                pstDate->time.hour, pstDate->time.minute, pstDate->time.second, FS,
                szSerialNum, FS) > 0)
    {
      nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
    }
  }
  return nRet;
}

DLLAPI int FR101API_CLASS nFR_LoadLogo(long lByteCount, unsigned char* pbData)
{
	int  nStatus;
	int  nRet = ERROR;
	unsigned char cAnswer;
	char szBuffer[FR_SIZEOF_BUFFER];
	nRet = nFR_ClearLogo();
	if( nRet == FR_SUCCESS )
	{
		long lBufSize = lByteCount+1;
		char* pszBuf = new char[lBufSize];

		memmove((pszBuf + 1),pbData,lByteCount);
		pszBuf[0] = 0x1B;

		if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X%d%c",	FR_LOAD_LOGO, lBufSize, FS) > 0)
		{
			nRet = nFR_BuildPacket(szFR_Command, szBuffer, sizeof(szBuffer));
			if(nRet != SUCCESS) return nRet;

			nRet = nFR_SendPacket(szBuffer);
			if(nRet != SUCCESS) return nRet;

			COMPortRead((char*)&cAnswer, 1, FR_INTERCHAR_TIMEOUT);
			if(cAnswer == ACK)
			{
				const short sPacketSize = 256;
				long  lCount = lBufSize / sPacketSize;				
				for(int i = 0; i < lCount; i++) 
				{   
					COMPortClear();
					COMPortWrite( pszBuf + i*sPacketSize, sPacketSize );					
				}	
				COMPortClear();
				COMPortWrite( pszBuf + lCount*sPacketSize, lBufSize - lCount*sPacketSize );				
				delete[] pszBuf;
				
				nRet = nFR_ReceivePacket(&nStatus, szFR_Answer, sizeof(szFR_Answer));
				if(nRet == SUCCESS)
				{	
					nRet = nStatus;
				}
			}
		}   
	}

	return nRet;
}

DLLAPI int FR101API_CLASS nFR_ClearLogo()
{
	int nRet = ERROR;

	if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X",	FR_CLEAR_LOGO) > 0)
	{
		nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
	}
	return nRet;
}

#if defined(LONGLONG_SUPPORT)
/* ************************************************************************** *
 FUNCTIONS FOR INT64 COMMAND GROUP IMPLEMENTATION
 * ************************************************************************** */
#define INTERNAL_PRECISION 10000

DLLAPI int FR101API_CLASS nFR_AtoLL(CONST char *szString, LONGLONG *pllOut)
{
int i=0,flag=0;
unsigned long ulMul=1;
char tmp[20];
    if((!pllOut) || (!szString))  return ERROR;
    *pllOut=0;
    if(strlen(szString)==0) return SUCCESS;
    do {
        if(flag) {
            tmp[i++] = *szString;
            if(*szString) ulMul*=10;
        }
        else {
            if(*szString == '.') flag=1;
            else tmp[i++]=*szString;
        }
    } while( *szString++ );

    i=0;
    if(tmp[i]=='-') {
        i++; flag=1;
    }
    else {
        flag=0;
    }
    while(tmp[i]) if(!isdigit(tmp[i++])) return ERROR;

	if(flag) i=1;
	else i=0;
    while( tmp[i] ) *pllOut=*pllOut * 10 + (tmp[i++]-'0');

    if(ulMul < INTERNAL_PRECISION ) {
        *pllOut*= (LONGLONG)INTERNAL_PRECISION / (LONGLONG)ulMul;
    }
    if(ulMul > INTERNAL_PRECISION ) {
        *pllOut/= (LONGLONG)ulMul / (LONGLONG)INTERNAL_PRECISION;
    }
    if(flag) *pllOut=-*pllOut;
    return SUCCESS;
}

DLLAPI void FR101API_CLASS nFR_LLtoA(LONGLONG llIn, char *szString)
{
	if(llIn<0) {
		llIn=-llIn;
		sprintf(szString, "-%"LONGLONG_PREFIX"d.%04"LONGLONG_PREFIX"d", llIn/INTERNAL_PRECISION, llIn%INTERNAL_PRECISION);

	}
	else {
		sprintf(szString, "%"LONGLONG_PREFIX"d.%04"LONGLONG_PREFIX"d", llIn/INTERNAL_PRECISION, llIn%INTERNAL_PRECISION);
	}
}


DLLAPI int FR101API_CLASS nFR_AddItem(CONST char *szName, CONST char *szCode,
                 LONGLONG llQuantity, LONGLONG llPrice,
                 unsigned short usDepartment, LONGLONG *pllTotal)
{
  int nRet = ERROR; char *pszToken = NULL, sQuantity[30], sPrice[30];
  if(pllTotal) *pllTotal = 0;
  if(szCode && szName)
  {
    nFR_LLtoA(llQuantity, sQuantity);
    nFR_LLtoA(llPrice, sPrice);
    if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X%s%c%s%c%s%c%s%c%hu%c",
                FR_ADD_ITEM,
                szName, FS,
                szCode, FS,
                sQuantity, FS,
                sPrice, FS,
                usDepartment, FS) > 0)
    {
      nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
      if(nRet == FR_SUCCESS && pllTotal)
      {
        pszToken = strtok(szFR_Answer, STR_FS);
        if(pszToken)
        {
          if(nFR_AtoLL(pszToken,pllTotal)) return ERROR;
        }
      }
    }
  }
  return nRet;
}

DLLAPI int FR101API_CLASS nFR_AddItemExt(CONST char *szName, CONST char *szCode,
					   LONGLONG llQuantity, LONGLONG llPrice,
					   unsigned short usDepartment, long lVatIndex, LONGLONG *pllTotal)
{
	int nRet = ERROR; char *pszToken = NULL, sQuantity[30], sPrice[30];
	if(pllTotal) *pllTotal = 0;
	if(szCode && szName)
	{
		nFR_LLtoA(llQuantity, sQuantity);
		nFR_LLtoA(llPrice, sPrice);
		if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X%s%c%s%c%s%c%s%c%hu%c%d%c",
			FR_ADD_ITEM,
			szName, FS,
			szCode, FS,
			sQuantity, FS,
			sPrice, FS,
			usDepartment, FS,
			lVatIndex, FS) > 0)
		{
			nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
			if(nRet == FR_SUCCESS && pllTotal)
			{
				pszToken = strtok(szFR_Answer, STR_FS);
				if(pszToken)
				{
					if(nFR_AtoLL(pszToken,pllTotal)) return ERROR;
				}
			}
		}
	}
	return nRet;
}


DLLAPI int FR101API_CLASS nFR_VoidItem(CONST char *szName, CONST char *szCode,
                  LONGLONG llQuantity, LONGLONG llPrice,
                  unsigned short usDepartment, LONGLONG *pllTotal)
{
  int nRet = ERROR; char *pszToken = NULL, sQuantity[30], sPrice[30];
  if(pllTotal) *pllTotal = 0;
  if(szCode && szName)
  {
    nFR_LLtoA(llQuantity, sQuantity);
    nFR_LLtoA(llPrice, sPrice);
    if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X%s%c%s%c%s%c%s%c%hu%c",
                FR_VOID_ITEM,
                szName, FS,
                szCode, FS,
                sQuantity, FS,
                sPrice, FS,
                usDepartment, FS) > 0)
    {
      nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
      if(nRet == FR_SUCCESS && pllTotal)
      {
        pszToken = strtok(szFR_Answer, STR_FS);
        if(pszToken)
        {
          if(nFR_AtoLL(pszToken,pllTotal)) return ERROR;
        }
      }
    }
  }
  return nRet;
}


DLLAPI int FR101API_CLASS nFR_AddDiscount(enum EDiscType eType, CONST char *szName,
                     LONGLONG llAmount, LONGLONG *pllTotal)
{
  int nRet = ERROR; char *pszToken = NULL, sAmount[30];
  if(pllTotal) *pllTotal = 0;
  if(szName)
  {
    nFR_LLtoA(llAmount, sAmount);
    if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X%c%c%s%c%s%c",
                FR_ADD_DISCOUNT,
                eType, FS,
                szName, FS,
                sAmount, FS) > 0)
    {
      nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
      if(nRet == FR_SUCCESS && pllTotal)
      {
        pszToken = strtok(szFR_Answer, STR_FS);
        if(pszToken)
        {
          if(nFR_AtoLL(pszToken,pllTotal)) return ERROR;
        }
      }
    }
  }
  return nRet;
}


DLLAPI int FR101API_CLASS nFR_AddMarkup(enum EDiscType eType, CONST char *szName,
                   LONGLONG llAmount, LONGLONG *pllTotal)
{
  int nRet = ERROR; char *pszToken = NULL, sAmount[30];
  if(pllTotal) *pllTotal = 0;
  if(szName)
  {
    nFR_LLtoA(llAmount, sAmount);
    if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X%c%c%s%c%s%c",
                FR_ADD_MARKUP,
                eType, FS,
                szName, FS,
                sAmount, FS) > 0)
    {
      nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
      if(nRet == FR_SUCCESS && pllTotal)
      {
        pszToken = strtok(szFR_Answer, STR_FS);
        if(pszToken)
        {
          if(nFR_AtoLL(pszToken,pllTotal)) return ERROR;
        }
      }
    }
  }
  return nRet;
}

DLLAPI int FR101API_CLASS nFR_Subtotal(LONGLONG *pllTotal)
{
  int nRet = ERROR; char *pszToken = NULL;
  if(pllTotal) *pllTotal = 0;

  if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X",
              FR_SUBTOTAL) > 0)
  {
    nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
    if(nRet == FR_SUCCESS && pllTotal)
    {
      pszToken = strtok(szFR_Answer, STR_FS);
      if(pszToken)
      {
        if(nFR_AtoLL(pszToken,pllTotal)) return ERROR;
      }
    }
  }
  return nRet;
}

DLLAPI int FR101API_CLASS nFR_Payment(short usType, LONGLONG llAmount, LONGLONG *pllTotal)
{
  int nRet = ERROR; char *pszToken = NULL, sAmount[30];
  if(pllTotal) *pllTotal = 0;
  nFR_LLtoA(llAmount, sAmount);

  if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X%hu%c%s%c",
              FR_PAYMENT,
              usType, FS,
              sAmount, FS) > 0)
  {
    nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
    if(nRet == FR_SUCCESS && pllTotal)
    {
      pszToken = strtok(szFR_Answer, STR_FS);
      if(pszToken)
      {
        if(nFR_AtoLL(pszToken,pllTotal)) return ERROR;
      }
    }
  }
  return nRet;
}

DLLAPI int FR101API_CLASS nFR_PaidInOut(CONST char *szBanknote, LONGLONG llAmount, LONGLONG *pllTotal)
{
  int nRet = ERROR; char *pszToken = NULL, sAmount[30];
  if(pllTotal) *pllTotal = 0;
  nFR_LLtoA(llAmount, sAmount);

  if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X%s%c%s%c",
              FR_PAID_IN_OUT,
              szBanknote, FS,
              sAmount, FS) > 0)
  {
    nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
    if(nRet == FR_SUCCESS && pllTotal)
    {
      pszToken = strtok(szFR_Answer, STR_FS);
      if(pszToken)
      {
        if(nFR_AtoLL(pszToken,pllTotal)) return ERROR;
      }
    }
  }
  return nRet;
}

DLLAPI int FR101API_CLASS nFR_GetEejTotals(enum ERequestType eType, ST_FRTOTALS_LONGLONG *pllTotals)
{
  int i, nRet = ERROR; char *pszToken = NULL;
  if(pllTotals)
  {
    memset(pllTotals, 0, sizeof(ST_FRTOTALS_LONGLONG));
  }

  if(eType == REQ_SALE_TOTALS || eType == REQ_RETURN_TOTALS)
  {
    if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X%c%c",
                FR_GET_SHIFT_STATUS,
                eType, FS) > 0)
    {
      nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
      if(nRet == FR_SUCCESS && pllTotals)
      {
        pszToken = strtok(szFR_Answer, STR_FS);
        for(i = 0; i < 16 && pszToken != NULL; i ++)
        {
          if(nFR_AtoLL(pszToken,&(pllTotals->llTotal[i]))) return ERROR;
          pszToken = strtok(NULL, STR_FS);
        }
      }
    }
  }
  return nRet;
};

DLLAPI int FR101API_CLASS nFR_GetCashInDrawer( LONGLONG *pllCashInDrawerTotal )
{
	int nRet = ERROR; char *pszToken = NULL;
	if( pllCashInDrawerTotal )
	{
		*pllCashInDrawerTotal = 0;
	}

	if(snprintf(szFR_Command, sizeof(szFR_Command), "%02X%c%c",
		FR_GET_SHIFT_STATUS,
		'5', FS) > 0)
	{
		nRet = nFR_RunCommand(szFR_Command, szFR_Answer, sizeof(szFR_Answer));
		if(nRet == FR_SUCCESS && pllCashInDrawerTotal)
		{
			pszToken = strtok(szFR_Answer, STR_FS);
			if( pllCashInDrawerTotal )
			{
				if(nFR_AtoLL(pszToken,pllCashInDrawerTotal)) return ERROR;
			}
		}
	}
	return nRet;
}

#endif

