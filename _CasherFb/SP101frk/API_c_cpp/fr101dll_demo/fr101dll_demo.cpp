// fr101dll_demo.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "time.h"

#define FR101DLL_IMPORTS
#include "../fr101win.h"

int _tmain(int argc, _TCHAR* argv[])
{
	nComPort = 1;
	if(COMPortOpen() == 0)
	{
		ST_TIMESTAMP stDate;
		struct tm *newtime;
		time_t t = time(0);
        newtime = localtime( &t ); /* Convert to local time. */
		stDate.date.year = newtime->tm_year;
		stDate.date.month = newtime->tm_mon + 1;
		stDate.date.day = newtime->tm_mday;
		stDate.time.hour = newtime->tm_hour;
		stDate.time.minute = newtime->tm_min;
		stDate.time.second = newtime->tm_sec;
		
		//nFR_Install(&stDate, "123456");
		nFR_Initialize(&stDate);
		nFR_WriteConfig(20, 0, "��� �������");

		double dblTemp = 0;
		nFR_OpenReceipt(2, 50, "��������1", 0);
		nFR_PrintBarcode(2, 2, 100, 2, "4606272001286");
		nFR_AddItem("����� #1", "1", 1.00, 50.00, 1, &dblTemp);
		nFR_AddDiscount(DSC_PERCENT, "���������� ������", 5.00, &dblTemp);
		nFR_AddMarkup(DSC_PERCENT, "���������� �������", 5.00, &dblTemp);
		nFR_AddItem("����� #2", "2", 1.00, 100.00, 2, &dblTemp);
		nFR_VoidItem("����� #2", "2", 1.00, 100.00, 2, &dblTemp);		
		nFR_AddItemExt("����� #3", "3", 2.00, 130.00, 3, 1, &dblTemp);
		nFR_Subtotal(&dblTemp);
		nFR_PrintText("��� ��� �����������!");
		nFR_AddDiscount(DSC_ABSOLUTE, "���������� ������", 5.00, &dblTemp);
		nFR_Payment(1, 30.00, &dblTemp);
		nFR_CloseReceipt();
		
		//nFR_PrintXReport("OPER01");
		nFR_PrintZReport("OPER01");

		/*----------------------�������� �������� (��� �������� �����)-----------------*/
		/* ������ ����������� � �� ����� 288 �����, ������ � �� 107 �� 126 �����.     */
		/*
		nFR_ClearLogo();
		HANDLE hFile=NULL;
		hFile=CreateFile("C:\\logo.bmp",GENERIC_READ,NULL,NULL,OPEN_EXISTING,NULL,NULL);
		if (hFile != INVALID_HANDLE_VALUE) 
		{
			DWORD buf_size = GetFileSize(hFile, NULL);
			if (buf_size != INVALID_FILE_SIZE) 
			{
				BYTE* buf = new BYTE[buf_size]; 
				DWORD sizebuf;
				ReadFile(hFile, buf, buf_size, &sizebuf, 0);
				if (sizebuf !=0) 
				{
					nFR_LoadLogo( sizebuf, buf);
				}
				else
				{
					printf( "nFR_LoadLogo - invalid parameter");
				}
				delete []buf;
			}
			else
			{
				printf( "nFR_LoadLogo - invalid parameter");					
			}
		}
		else
		{
			printf( "nFR_LoadLogo - invalid parameter");
		}
		nFR_WriteConfig(5, 0, "1"); // ���������� ���� "�������� �������"
		*/
		/*------------------------------------------------------------------*/

		//nFR_PrintFisShiftReport(1, 1, 2, "79113");
		//nFR_PrintFisDateReport(1, &stDate, &stDate, "79113");
		//nFR_PrintEejJournal(3);
		//nFR_PrintEejDocument("904");
		//nFR_PrintEejShiftReport(1, 1, 2, "79113");
		//nFR_PrintEejDateReport(1, &stDate, &stDate, "79113");
		//nFR_PrintActivateReport();
		//nFR_PrintEejSingleShiftReport(1);
		//Sleep(10);
		//nFR_BreakReport();

		//char szSerial[100];
		//nFR_GetSerialNum(szSerial, 100);		
		//UN_FRSTATUS unStatus;
		//nFR_GetStatus(&unStatus);
		//BYTE dbStatus = 0;
		//nFR_GetEejStatus(&dbStatus);
		//unsigned short sShift;
		//ST_FRTOTALS stTotals;
		//nFR_GetEejShift(REQ_CURRENT_SHIFT, &sShift);
		//nFR_GetEejShift(REQ_LAST_REG_SHIFT, &sShift);
		//nFR_GetEejTotals(REQ_RETURN_TOTALS, &stTotals);
		//nFR_GetEejTotals(REQ_SALE_TOTALS, &stTotals);
		//nFR_EEJ_CloseArchieve();
		//char szBuffer[100];
		//nFR_ReadConfig(20, 0, szBuffer, sizeof(szBuffer));
		//nFR_GetDateTime(&stDate);
		//stDate.time.minute += 2;
		//nFR_SetDateTime(&stDate);
		//nFR_UpdateCustDisp("LINE1", "LINE2");
		
		//nFR_Register("79113", "7770", "76543210", "79113");
		//nFR_EEJ_Activate();

		//nFR_PaidInOut("10 baksov", 10.00, &dblTemp);	
		
		COMPortClose();
	}
	return 0;
}

