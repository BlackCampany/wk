#ifndef _FR101API_H
#define _FR101API_H

/* Compiler/language settings */
#if defined(__cplusplus)
#define CONST const
#else
#define CONST
#endif

/* Microsoft */
#if defined(_MSC_VER)

#if (_MSC_VER >= 1200)
#define LONGLONG_SUPPORT
#define LONGLONG __int64
#define LONGLONG_PREFIX "I64"
#elif (_MSC_VER >= 1310) && defined(_MSC_EXTENSIONS)
#define LONGLONG_SUPPORT
#define LONGLONG long long
#define LONGLONG_PREFIX "I64"
#endif

#endif

/* GNU */
#if defined(__GNUC__) && (!defined(__linux__))
#define LONGLONG_SUPPORT
#define LONGLONG long long
#define LONGLONG_PREFIX "ll"
#endif

/* STARTUP COMMAND GROUP */
#define FR_INITIALIZE                   0x00
#define FR_REGISTER                     0x08
#define FR_EEJ_ACTIVATE                 0x09
#define FR_EEJ_CLOSE_ARCHIEVE           0x0A
/* RECEIPT COMMAND GROUP */
#define FR_OPEN_RECEIPT                 0x20
#define FR_PRINT_TEXT                   0x21
#define FR_CLOSE_RECEIPT                0x22
#define FR_CANCEL_RECEIPT               0x23
#define FR_PRINT_BARCODE                0x24
#define FR_BANNER                       0x25
#define FR_STORE_RECEIPT                0x27
#define FR_ADD_ITEM                     0x30
#define FR_VOID_ITEM                    0x31
#define FR_ADD_DISCOUNT                 0x32
#define FR_ADD_MARKUP                   0x33
#define FR_SUBTOTAL                     0x34
#define FR_PAYMENT                      0x35
#define FR_PAID_IN_OUT                  0x36
#define FR_SET_TAX_AMOUNT               0x37
/* REPORT COMMAND GROUP */
#define FR_PRINT_X_REP                  0x60
#define FR_PRINT_Z_REP                  0x61
#define FR_PRINT_FIS_SHIFT_REP          0x62
#define FR_PRINT_FIS_DATE_REP           0x63
#define FR_PRINT_EEJ_JOURNAL            0x64
#define FR_PRINT_EEJ_DOCUMENT           0x65
#define FR_PRINT_EEJ_SHIFT_REP          0x66
#define FR_PRINT_EEJ_DATE_REP           0x67
#define FR_PRINT_EEJ_ACTIVATE_REP       0x68
#define FR_PRINT_EEJ_SINGLE_SHIFT_REP   0x69
#define FR_EMERGENT_CLOSE_SHIFT         0x70
#define FR_OPEN_DRAWER                  0x80
#define FR_PRINT_CUST_DISP_TEXT         0x81
/* SYSTEM COMMAND GROUP */
#define FR_GET_STATUS                   0xA0
#define FR_READ_CONFIG                  0xA1
#define FR_WRITE_CONFIG                 0xA2
#define FR_SET_DATE_TIME                0xA3
#define FR_GET_EEJ_STATUS               0xA4
#define FR_GET_SHIFT_STATUS             0xA5
#define FR_GET_KKM_SERIAL_NUM           0xA6
#define FR_GET_DATE_TIME                0xA7
#define FR_GET_DRIVER_OPENED            0xA8
/* INTERNAL COMMAND GROUP */
#define FR_INSTALL_KKM                  0xE0
#define FR_READ_MEMORY_BLOCK            0xE1
#define FR_LOAD_LOGO                    0xE2
#define FR_CLEAR_LOGO                   0xE3


/* ERROR CODES */
#define FR_SUCCESS                      0x00
#define FR_BUSY                         0x7F
/* I/O ERRORS */
#define FR_ERR_INVALID_STATUS           0x01
#define FR_ERR_INVALID_FUNCTION         0x02
#define FR_ERR_INVALID_PARAMETER        0x03
/* TRANSPORT LEVEL ERRORS */
#define FR_ERR_UART_OVERFLOW            0x04
#define FR_ERR_INTERCHAR_TIMEOUT        0x05
#define FR_ERR_INVALID_PASSWORD         0x06
#define FR_ERR_INVALID_CHECKSUM         0x07
/* PRINTER ERRORS */
#define FR_ERR_OUT_OF_PAPER             0x08
#define FR_ERR_PRINTER_NOT_READY        0x09
/* DATE/TIME ERRORS */
#define FR_ERR_24HOUR_OVERFLOW          0x0A
#define FR_ERR_TIMESYNC_OVERFLOW        0x0B
#define FR_ERR_TOO_LOW_DOC_TIME         0x0C
#define FR_ERR_NO_DOC_HEADER            0x0D
#define FR_ERR_NEGATIVE_TOTAL           0x0E
#define FR_ERR_DISPLAY_NOT_READY        0x0F
/* FATAL ERRORS - BY KOMAX */
#define FR_ERR_DATA_SEND                0x10
#define FR_ERR_NO_DATA                  0x11
#define FR_ERR_PORT_OPEN                0x12
#define FR_ERR_DEVICE_UNPLUGGED         0x13
#define FR_ERR_DEVICE_BUSY              0x14
#define FR_ERR_PROTOCOL                 0x15
/* FATAL ERRORS */
#define FR_ERR_FATAL                    0x20
#define FR_ERR_OUT_OF_MEMORY            0x21
#define FR_ERR_DEVICE_TIMEOUT           0x30
/* EEJ ERRORS */
#define FR_ERR_EEJ_WRONG_ARG_FORMAT     0x41
#define FR_ERR_EEJ_INCORRECT_STATUS     0x42
#define FR_ERR_EEJ_EMERGENCY            0x43
#define FR_ERR_EEJ_CRYPTO_EMERGENCY     0x44
#define FR_ERR_EEJ_EXPIRED              0x45
#define FR_ERR_EEJ_OVERFLOW             0x46
#define FR_ERR_EEJ_WRONG_DATE_TIME      0x47
#define FR_ERR_EEJ_NO_DATA              0x48
#define FR_ERR_EEJ_DOC_OVERFLOW         0x49
#define FR_ERR_EEJ_TIMEOUT              0x4A
#define FR_ERR_EEJ_COMMUNICATION        0x4B

#ifndef BYTE
#define BYTE unsigned char
#endif

#ifndef SUCCESS
#define SUCCESS 0
#endif

#ifndef ERROR
#define ERROR -1
#endif

#define FR_INTERCHAR_TIMEOUT   1
#define FR_SIZEOF_BUFFER     300
#define FR_MAX_LENGTH         40

#if defined(DOS)

#include <dos.h>

#else

#if !defined(DOSXXXX_T)
#define DOSXXXX_T

typedef struct
{
  unsigned char day;		/* 1-31 */
  unsigned char month;		/* 1-12 */
  unsigned int  year;		/* 1980-2099 */
  unsigned char dayofweek;	/* 0-6, 0=Sunday */
} dosdate_t;

typedef struct
{
  unsigned char hour;		/* 0-23 */
  unsigned char minute;		/* 0-59 */
  unsigned char second;		/* 0-59 */
  unsigned char hsecond;	/* 0-99 */
} dostime_t;

#endif

#endif

typedef struct
{
  dosdate_t date;
  dostime_t time;
} ST_TIMESTAMP;

enum EDiscType
{
  DSC_PERCENT='%',
  DSC_ABSOLUTE='S'
};

enum ERequestType
{
  REQ_CURRENT_SHIFT='1',
  REQ_LAST_REG_SHIFT='2',
  REQ_SALE_TOTALS='3',
  REQ_RETURN_TOTALS='4'
};

#define FR_DOC_NFISCAL  0x01
#define FR_DOC_SALE     0x02
#define FR_DOC_RETURN   0x03
#define FR_DOC_PAID_IN  0x04
#define FR_DOC_PAID_OUT 0x05

#define FR_DOC_OPEN  0x10
#define FR_DOC_TOTAL 0x20
#define FR_DOC_MEDIA 0x30
#define FR_DOC_CLOSE 0x40

#pragma pack(1)

typedef struct
{
  unsigned short fbNVRIncorrectCRC    :1;
  unsigned short fbConfigIncorrectCRC :1;
  unsigned short fbSPIInterfaceError  :1;
  unsigned short fbMFIncorrectCRC     :1;
  unsigned short fbMFWriteError       :1;
  unsigned short fbMFNotInstalled     :1;
  unsigned short fbEEJFatalError      :1;
  unsigned short fbUnused1            :1;

  unsigned short fbNotInitialized     :1;
  unsigned short fbNonFiscalMode      :1;
  unsigned short fbShiftOpened        :1;
  unsigned short fb24HourOverflow     :1;
  unsigned short fbEEJArchieveClosed  :1;
  unsigned short fbEEJNotActivated    :1;
  unsigned short fbMFNoMemoryForShift :1;
  unsigned short fbMFWrongPassword    :1;

  unsigned char dbDocumentType;
} ST_FRSTATUS;

typedef union
{
  ST_FRSTATUS stStatus;
  unsigned char dbStatus[3];
} UN_FRSTATUS;

typedef struct
{
  double dblTotal[16];
} ST_FRTOTALS;

#if defined(LONGLONG_SUPPORT)
typedef struct
{
  LONGLONG llTotal[16];
} ST_FRTOTALS_LONGLONG;
#endif

#pragma pack()

#ifdef FR101DLL_EXPORTS
#define DLLAPI __declspec(dllexport)
#elif defined FR101DLL_IMPORTS
#define DLLAPI __declspec(dllimport)
#else
#define DLLAPI
#endif

#if defined(FR101DLL_EXPORTS) || defined(FR101DLL_IMPORTS)
#define FR101API_CLASS

extern DLLAPI int nComPort;

DLLAPI int  COMPortOpen(void);
DLLAPI void COMPortClear(void);
DLLAPI void COMPortClose(void);

int  COMPortRead(char *, int, int);
int  COMPortWrite(char *, int);
void NativeToDos(char *, int);
void DosToNative(char *, int);

#elif defined(__cplusplus)
#define FR101API_CLASS CFR101_API::

class CFR101_API
{
  virtual int  COMPortOpen() = 0;
  virtual void COMPortClear() = 0;
  virtual void COMPortClose() = 0;
  virtual int  COMPortRead(char *, int, int) = 0;
  virtual int  COMPortWrite(char *, int) = 0;
  virtual void NativeToDos(char *, int) = 0;
  virtual void DosToNative(char *, int) = 0;

protected:
  BYTE  dbFR_Packet; /* 0x20 */
  short sFR_AsyncMode; /* 0 */
  char  szFR_Command[FR_SIZEOF_BUFFER];
  char  szFR_Answer[FR_SIZEOF_BUFFER];

/* ************************************************************************** *
 FUNCTIONS ONLY FOR INTERNAL USAGE
 * ************************************************************************** */
BYTE dbFR_GetLRC(char *snzData, int nSize);
int  nFR_CheckLRC(char *snzData, int nSize);
int  nFR_BuildPacket(char *szCommand, char *szBuffer, int nSize);
int  nFR_SendPacket(char *szCommand);
int  nFR_ReceivePacket(int *pnStatus, char *szAnswer, int nSize);
int  nFR_RunCommand(char *szCommand, char *szAnswer, int nSize);

#else
#define FR101API_CLASS

  extern int  COMPortOpen(void);
  extern void COMPortClear(void);
  extern void COMPortClose(void);
  extern int  COMPortRead(char *, int, int);
  extern int  COMPortWrite(char *, int);
  extern void NativeToDos(char *, int);
  extern void DosToNative(char *, int);

#endif


#if defined(__cplusplus) && !defined(FR101DLL_EXPORTS) && !defined(FR101DLL_IMPORTS)
public:
#endif

DLLAPI int  nFR_IsActive(void);
DLLAPI void nFR_SetMode(short sAsyncMode);

/* ************************************************************************** *
 FUNCTIONS FOR STARTUP COMMAND GROUP IMPLEMENTATION
 * ************************************************************************** */
DLLAPI int  nFR_Initialize(ST_TIMESTAMP *);
DLLAPI int  nFR_Register(char *, char *, char *, char *);
DLLAPI int  nFR_EEJ_Activate(void);
DLLAPI int  nFR_EEJ_CloseArchieve(void);

/* ************************************************************************** *
 FUNCTIONS FOR RECEIPT COMMAND GROUP IMPLEMENTATION
 * ************************************************************************** */
DLLAPI int  nFR_OpenReceipt(unsigned short usType, unsigned short usDepartment,
                     CONST char *szOperator, unsigned long  ulReceiptNum);
DLLAPI int  nFR_PrintText(CONST char *szText);
DLLAPI int  nFR_PrintTextExt(CONST char *szText, int nFont);
DLLAPI int  nFR_CloseReceipt(void);
DLLAPI int  nFR_CancelReceipt(void);
DLLAPI int  nFR_PrintBarcode(unsigned short usAlign, unsigned short usWidth,
                      unsigned short usHeight, unsigned short usType,
                      CONST char *szBarcode);
DLLAPI int  nFR_Banner(void);
DLLAPI int  nFR_StoreReceipt(CONST char *szText);
DLLAPI int  nFR_AddItem(CONST char *szName, CONST char *szCode,
                 double dblQuantity, double dblPrice,
                 unsigned short usDepartment, double *pdblTotal);
DLLAPI int  nFR_AddItemExt(CONST char *szName, CONST char *szCode,
						double dblQuantity, double dblPrice,
						unsigned short usDepartment, long lVatIndex, double *pdblTotal);
DLLAPI int  nFR_VoidItem(CONST char *szName, CONST char *szCode,
                  double dblQuantity, double dblPrice,
                  unsigned short usDepartment, double *pdblTotal);
DLLAPI int  nFR_AddDiscount(enum EDiscType eType, CONST char *szName,
                     double dblAmount, double *pdblTotal);
DLLAPI int  nFR_AddMarkup(enum EDiscType eType, CONST char *szName,
                   double dblAmount, double *pdblTotal);
DLLAPI int  nFR_Subtotal(double *pdblTotal);
DLLAPI int  nFR_SetTaxAmount(short usType, double dblAmount);
DLLAPI int  nFR_Payment(short usType, double dblAmount, double *pdblTotal);
DLLAPI int  nFR_PaidInOut(CONST char *szBanknote, double dblAmount, double *pdblTotal);

/* ************************************************************************** *
 FUNCTIONS FOR REPORT COMMAND GROUP IMPLEMENTATION
 * ************************************************************************** */
DLLAPI int  nFR_PrintXReport(CONST char *szOperator);
DLLAPI int  nFR_PrintZReport(CONST char *szOperator);
DLLAPI int  nFR_PrintFisShiftReport(unsigned short usType, unsigned short usFirstShift,
                             unsigned short usLastShift, char *szPassword);
DLLAPI int  nFR_PrintFisDateReport(unsigned short usType, ST_TIMESTAMP *pstFirstDate,
                            ST_TIMESTAMP *pstLastDate, char *szPassword);
DLLAPI int  nFR_PrintEejJournal(unsigned short usShift);
DLLAPI int  nFR_PrintEejDocument(CONST char *szKPK);
DLLAPI int  nFR_PrintEejShiftReport(unsigned short usType, unsigned short usFirstShift,
                             unsigned short usLastShift, CONST char *szPassword);
DLLAPI int  nFR_PrintEejDateReport(unsigned short usType, ST_TIMESTAMP *pstFirstDate,
                            ST_TIMESTAMP *pstLastDate, CONST char *szPassword);
DLLAPI int  nFR_PrintActivateReport(void);
DLLAPI int  nFR_PrintEejSingleShiftReport(unsigned short usShift);
DLLAPI int  nFR_EmergentShiftClose(void);
DLLAPI int  nFR_OpenDrawer(void);
DLLAPI int  nFR_UpdateCustDisp(CONST char *szLine1, CONST char *szLine2);
DLLAPI int  nFR_BreakReport(void);

/* ************************************************************************** *
 FUNCTIONS FOR SYSTEM COMMAND GROUP IMPLEMENTATION
 * ************************************************************************** */
DLLAPI int  nFR_GetStatus(UN_FRSTATUS *punStatus);
DLLAPI int  nFR_ReadConfig(unsigned short usRow, unsigned short usColumn,
                    char *szValue, int nSize);
DLLAPI int  nFR_WriteConfig(unsigned short usRow, unsigned short usColumn,
                    CONST char *szValue);
DLLAPI int  nFR_SetDateTime(ST_TIMESTAMP *pstDate);
DLLAPI int  nFR_GetEejStatus(BYTE *pdbStatus);
DLLAPI int  nFR_GetEejShift(enum ERequestType eType, unsigned short *pusShift);
DLLAPI int  nFR_GetEejTotals(enum ERequestType eType, ST_FRTOTALS *pstTotals);
DLLAPI int  nFR_GetCashInDrawer( double *pdCashInDrawerTotal );
DLLAPI int  nFR_GetSerialNum(char *szValue, int nSize);
DLLAPI int  nFR_GetDateTime(ST_TIMESTAMP *pstDate);
DLLAPI int  nFR_GetDriverOpened(BYTE *pdbStatusOpened);
DLLAPI int  nFR_GetFrID(short int *pdbFrId);
DLLAPI int  nFR_DoCommand(BYTE bCommand, char *szInOutBuffer, int nBufferSize);


/* ************************************************************************** *
 FUNCTIONS FOR INTERNAL COMMAND GROUP IMPLEMENTATION
 * ************************************************************************** */
DLLAPI int  nFR_Install(ST_TIMESTAMP *pstDate, char *szSerialNum);
/*DLLAPI int  nFR_ReadMemBlock(unsigned short usDataType,
                             unsigned short usByteOffset,
                             unsigned short usByteCount,
                             unsigned char* pcData);*/
DLLAPI int  nFR_LoadLogo(long lByteCount, unsigned char* pbData);
DLLAPI int  nFR_ClearLogo();


#if defined(LONGLONG_SUPPORT)
/* ************************************************************************** *
 FUNCTIONS FOR INT64 COMMAND GROUP IMPLEMENTATION
 * ************************************************************************** */
DLLAPI int  nFR_AtoLL(CONST char *szString, LONGLONG *pllOut);
DLLAPI void nFR_LLtoA(LONGLONG llIn, char *szString);

DLLAPI int  nFR_AddItem(CONST char *szName, CONST char *szCode,
                 LONGLONG llQuantity, LONGLONG llPrice,
                 unsigned short usDepartment, LONGLONG *pllTotal);
DLLAPI int  nFR_AddItemExt(CONST char *szName, CONST char *szCode,
						LONGLONG llQuantity, LONGLONG llPrice,
						unsigned short usDepartment, long lVatIndex, LONGLONG *pllTotal);
DLLAPI int  nFR_VoidItem(CONST char *szName, CONST char *szCode,
                  LONGLONG llQuantity, LONGLONG llPrice,
                  unsigned short usDepartment, LONGLONG *pllTotal);
DLLAPI int  nFR_AddDiscount(enum EDiscType eType, CONST char *szName,
                     LONGLONG llAmount, LONGLONG *pllTotal);
DLLAPI int  nFR_AddMarkup(enum EDiscType eType, CONST char *szName,
                   LONGLONG llAmount, LONGLONG *pllTotal);
DLLAPI int  nFR_Subtotal(LONGLONG *pllTotal);
DLLAPI int  nFR_Payment(short usType, LONGLONG llAmount, LONGLONG *pllTotal);
DLLAPI int  nFR_PaidInOut(CONST char *szBanknote, LONGLONG llAmount, LONGLONG *pllTotal);
DLLAPI int  nFR_GetEejTotals(enum ERequestType eType, ST_FRTOTALS_LONGLONG *pllTotals);
DLLAPI int  nFR_GetCashInDrawer( LONGLONG *pllCashInDrawerTotal );

#endif

#if defined(__cplusplus) && !defined(FR101DLL_EXPORTS) && !defined(FR101DLL_IMPORTS)
};
#endif

#endif /* _FR101API_H */
