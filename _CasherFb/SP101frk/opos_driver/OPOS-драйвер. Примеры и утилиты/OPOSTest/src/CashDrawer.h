#pragma once

#include "ErrorDef.h"
#import  "ocx/OPOSCashDrawer.ocx" no_namespace high_property_prefixes("Get","Set","PutRef")
#import  "ocx/Drawer.ocx" no_namespace high_property_prefixes("Get","Set","PutRef")

class CPOSDevice;
 
class CCashDrawer : public CPOSDevice
{
	IOPOSCashDrawerPtr     m_CashDrawerPtr;
	IOPOSCashDrawerPtr     m_CashDrawer19Ptr;	
	IOPOSCashDrawer18Ptr   m_CashDrawer18Ptr;	
	IOPOSCashDrawer15Ptr   m_CashDrawer15Ptr;
	IOPOSCashDrawer14Ptr   m_CashDrawer14Ptr;	
	_DOPOS_DrawerPtr       m_CashDrawerWNPtr;

public:
	CCashDrawer(void);
	~CCashDrawer(void);

private:
	void SetLastError( DWORD dwErrorCode, long lExtErrorCode );
	//------------------------------------------------------------------
	// Setting a last error number. You can get text description of the last error 
	// after any function calling.
	//      IN:     dwErrorCode   - an error code	
	//              lExtErrorCode - Error code, returned by OPOS-driver
	//      return: void
	//------------------------------------------------------------------

	_bstr_t GetErrorString() {return "";};
	//------------------------------------------------------------------
	// Gets error description
	//       return: error description string
	//------------------------------------------------------------------

	void AttachWorkInterface();
	//------------------------------------------------------------------
	// Get current interface pointer	
	//       return: current interface pointer
	//------------------------------------------------------------------

	void Release();
	//------------------------------------------------------------------
	// Release all interface pointer	
	//       return: void
	//------------------------------------------------------------------


protected:
	// error codes, depending on the device
	virtual DWORD GetError_DrvNotLoaded() { return DR_ERROR_DRV_NOT_LOADED; };
	virtual DWORD GetError_Opening()      { return DR_ERROR_OPENING;        };	

public:

	BOOL  Init();
	//------------------------------------------------------------------
	// Init the device. Attaching OPOS Control Object.
	//      return: TRUE if success, else FALSE	
	//------------------------------------------------------------------

	BOOL  IsDrvLoaded();
	//------------------------------------------------------------------
	// Checks, if driver is loaded. If Init() is success, driver is 
	// loaded	
	//      return: TRUE if success, else FALSE	
	//------------------------------------------------------------------

	BOOL IsDeviceOpen();
	//------------------------------------------------------------------
	// Checks, if cash drawer is opened
	//      return: TRUE, if opened, else FALSE
	//------------------------------------------------------------------

	DWORD OpenDevice(LPCSTR lpszDeviceName);
	//------------------------------------------------------------------
	// Open the cash drawer
	//      return: SUCCESS if ready, else error code
	//------------------------------------------------------------------

	void  CloseDevice();
	//------------------------------------------------------------------
	// Close the cash drawer
	//      return: void
	//------------------------------------------------------------------

	void Close();
	//------------------------------------------------------------------
	// Close the cash drawer, delete m_CashDrawerPtr
	//      return: void
	//------------------------------------------------------------------

	DWORD CheckReady();
	//------------------------------------------------------------------
	// Checks, if cash drawer is ready, that's if loaded a driver and opened port
	//      return: SUCCESS if ready, else error code
	//------------------------------------------------------------------

	DWORD OpenDrawer();
	//------------------------------------------------------------------
	// Open cash drawer 
	//      return: void
	//------------------------------------------------------------------
private:
	// template function for OPOS
	template<class T> DWORD OpenDrawer( T pInterface );
};
