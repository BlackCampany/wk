#pragma once

//#undef CreateWindow

#include "ErrorDef.h"
#include "OPOSInclude/OposDisp.hi"
#import  "ocx/OPOSLineDisplay.ocx" no_namespace high_property_prefixes("Get","Set","PutRef")
#import  "ocx/LineDisp.ocx" no_namespace high_property_prefixes("Get","Set","PutRef")

class CPOSDevice;

class CLineDisplay : public CPOSDevice
{
	//IOPOSLineDisplayPtr    *m_LineDisplayPtr;
	//_DOPOS_LineDisplayPtr  *m_LineDisplayOldPtr;
	IOPOSLineDisplayPtr    m_LineDisplayPtr;	
	IOPOSLineDisplayPtr    m_LineDisplay19Ptr;	
	IOPOSLineDisplay18Ptr  m_LineDisplay18Ptr;
	IOPOSLineDisplay16Ptr  m_LineDisplay16Ptr;
	IOPOSLineDisplay15Ptr  m_LineDisplay15Ptr;
	IOPOSLineDisplay14Ptr  m_LineDisplay14Ptr;	
	_DOPOS_LineDisplayPtr  m_LineDisplayWNPtr;

public:
	CLineDisplay(void);
	~CLineDisplay(void);

private: 

	virtual DWORD GetError_DrvNotLoaded() { return CD_ERROR_DRV_NOT_LOADED; }
	virtual DWORD GetError_Opening()      { return CD_ERROR_OPENING;        }

protected:
	void SetLastError( DWORD dwErrorCode, long lExtErrorCode );
	//------------------------------------------------------------------
	// Setting a last error number. You can get text description of the last error 
	// after any function calling.
	//      IN:     dwErrorCode      - an error code
	//              lExtErrorCode    - Error code, returned by OPOS-driver
	//      return: void
	//------------------------------------------------------------------

	_bstr_t GetErrorString() {return "";};
	//------------------------------------------------------------------
	// Gets error description
	//       return: error description string
	//------------------------------------------------------------------

	void AttachWorkInterface();
	//------------------------------------------------------------------
	// Get current interface pointer	
	//       return: current interface pointer
	//------------------------------------------------------------------

	void Release();
	//------------------------------------------------------------------
	// Release all interface pointer	
	//       return: void
	//------------------------------------------------------------------


public:

	BOOL  Init();
	//------------------------------------------------------------------
	// Init the device. Attaching OPOS Control Object.
	//      return: TRUE if success, else FALSE	
	//------------------------------------------------------------------

	BOOL  IsDrvLoaded();
	//------------------------------------------------------------------	
	// Checks, if driver is loaded. If Init() is success, driver is 
	// loaded	
	//      return: TRUE if success, else FALSE	
	//------------------------------------------------------------------

	BOOL IsDeviceOpen();
	//------------------------------------------------------------------
	// Checks, if line display is opened
	//      return: TRUE, if opened, else FALSE
	//------------------------------------------------------------------
	
	DWORD OpenDevice(LPCSTR lpszDeviceName);
	//------------------------------------------------------------------
	// Open line display
	//      return: FP_SUCCESS if success, or error code
	//------------------------------------------------------------------

	void  CloseDevice();
	//------------------------------------------------------------------
	// Close line display
	//      return: void
	//------------------------------------------------------------------

	void Close();
	//------------------------------------------------------------------
	// Close line display, delete m_LineDisplayPtr
	//      return: void
	//------------------------------------------------------------------	

	DWORD CheckReady();
	//------------------------------------------------------------------
	// Checks, if line display is ready, that's if driver is loaded or not
	// and port is open or not
	//      return: TRUE, if ready, else FALSE
	//------------------------------------------------------------------

	DWORD PrintText();
	//------------------------------------------------------------------
	// Output text to display
	//      return: void
	//------------------------------------------------------------------

protected:
	
	// template functions for OPOS
	template<class T> DWORD PrintText( T pInterface );

};