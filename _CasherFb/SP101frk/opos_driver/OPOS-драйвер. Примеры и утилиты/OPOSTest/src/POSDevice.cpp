#include "StdAfx.h"
#include ".\posdevice.h"


CPOSDevice::CPOSDevice(OPOSDeviceType enDeviceType)
{
	m_enDeviceType        = enDeviceType;
	m_bWNInterfaceUsed    = FALSE;
	m_dwErrorCode         = SUCCESS;
}

CPOSDevice::~CPOSDevice(void)
{
}
//------------------------------------------------------------------
// AddItemToLog()
//------------------------------------------------------------------
void CPOSDevice::AddItemToLog( IN const CLogItem& Item )
{	
	INT_PTR iIndex = m_Log.Add( Item );
	PostThreadMessage( AfxGetApp()->m_nThreadID, TM_ADD_ITEM_TO_LOG, (WPARAM)iIndex, (LPARAM)m_enDeviceType );	
}
//------------------------------------------------------------------
// SetLastError()
//------------------------------------------------------------------
void CPOSDevice::SetLastError( DWORD dwErrorCode, LPCTSTR lpszExtInfo /* = NULL  */)
{	
	m_dwErrorCode = dwErrorCode;

	m_sError   .Empty();
	m_sErrorExt.Empty();	

	if( lpszExtInfo != NULL )	
		m_sErrorExt = lpszExtInfo;	
}
//------------------------------------------------------------------
// GetLastErrorText()
//------------------------------------------------------------------
CString& CPOSDevice::GetLastErrorText()
{
	UINT  unResourceID;
	DWORD dwOldError = ::GetLastError();

	m_sError.Empty();

	if( m_dwErrorCode < USER_ERROR_BASE || m_dwErrorCode > USER_ERROR_MAX_BASE )
	{	
		::SetLastError( m_dwErrorCode );
		GetErrorMessageWin32( m_sError );
	}
	else
	{
		unResourceID = GetErrorResourceID( m_dwErrorCode );		
		m_sError.LoadString( unResourceID );
	}

	m_sError += m_sErrorExt;
	::SetLastError(dwOldError);

	return m_sError;
}
//------------------------------------------------------------------
// GetErrorMessageWin32()
//------------------------------------------------------------------
void CPOSDevice::GetErrorMessageWin32( CString& sMessage )
{
	LPVOID lpMsgBuf;	

	string str;
	if( FormatMessage( 
		FORMAT_MESSAGE_ALLOCATE_BUFFER | 
		FORMAT_MESSAGE_FROM_SYSTEM | 
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		m_dwErrorCode,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
		(LPTSTR) &lpMsgBuf,
		0,
		NULL 
		) )
	{
		str = (LPCSTR)lpMsgBuf;
		LocalFree( lpMsgBuf );
	}
	else
		str = _T("Unknown error");

	TCHAR sBuf[8000];
	_stprintf( sBuf, _T("%d, %s"), m_dwErrorCode, str.c_str() );

	sMessage = sBuf;

	//int nIndx;	
	//while( ( nIndx=sMessage.Find( '\r' ) ) != -1 ) sMessage.Delete( nIndx );
	//while( ( nIndx=sMessage.Find( '\n' ) ) != -1 ) sMessage.Delete( nIndx );
}
//------------------------------------------------------------------
// GetLog()
//------------------------------------------------------------------
void CPOSDevice::GetLog( OUT CArray<CLogItem>& Log )
{
	Log.RemoveAll();
	Log.Append( m_Log );
}