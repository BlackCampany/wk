// OPOSTestDlg.h : header file
//

#pragma once
#include "afxwin.h"
#include "afxcmn.h"

// COPOSTestDlg dialog
class COPOSTestDlg : public CDialog
{	
	CImageList           m_IL;

	DWORD                m_dwFiscalPrinterThreadID;
	DWORD                m_dwLineDisplayThreadID;
	DWORD                m_dwCashDrawerThreadID;

	HANDLE               m_hFiscalPrinterThread;	
	HANDLE               m_hLineDisplayThread;	
	HANDLE               m_hCashDrawerThread;
	
// Construction
public:
	COPOSTestDlg(CWnd* pParent = NULL);	// standard constructor
	~COPOSTestDlg();

// Dialog Data
	enum { IDD = IDD_OPOSTEST_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

private:
	void ProcessFocusedButton( UINT nID /*������������� ������� ������*/ );
	BOOL FillSOVersion();

	virtual DWORD PrintThread_FN();
	virtual DWORD DisplayText_FN();
	virtual DWORD OpenCashDrawer_FN();

	static  DWORD WINAPI PrintThread( LPVOID lpvParam );
	static  DWORD WINAPI DisplayText( LPVOID lpvParam );
	static  DWORD WINAPI OpenCashDrawer( LPVOID lpvParam );

	void KillThread( HANDLE& hThread );
	void CreateProcessThread( HANDLE& hThread, LPTHREAD_START_ROUTINE lpStartAddress, LPDWORD lpThreadId );
	void EnableAllControls(BOOL bEnable);

	// ��������� ������������� ������
	void AddStringToLog      ( LPCSTR lpszText );
	void AddItemToLog        ( int nItemIndex );
	void SetCompletedFRProcess();
	void SetCompletedLDProcess();
	void SetCompletedCDProcess();
	void SetCompletedFunction( LPCSTR lpszFunction );
	void SetStartedFunction  ( LPCSTR lpszFunction );
	void DisplayError        ( LPCSTR lpszError );
	void SetWaitCursor();
	void SetStopCursor();

	// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedBtn( UINT nID );	
//	afx_msg void OnDestroy();	
	afx_msg void OnClose();		
	afx_msg void OnCbnSelchangeCmbDevice();
	afx_msg void OnCbnSelchangeCmbComPort();

	afx_msg LRESULT OnAddStringToLog       ( WPARAM wParam, LPARAM lParam );
	afx_msg LRESULT OnSetCompletedFunction ( WPARAM wParam, LPARAM lParam );
	afx_msg LRESULT OnAddItemToLog         ( WPARAM wParam, LPARAM lParam );
	afx_msg LRESULT OnSetStartedFunction   ( WPARAM wParam, LPARAM lParam );
	afx_msg LRESULT OnSetCompletedFRProcess( WPARAM wParam, LPARAM lParam );
	afx_msg LRESULT OnSetCompletedLDProcess( WPARAM wParam, LPARAM lParam );
	afx_msg LRESULT OnSetCompletedCDProcess( WPARAM wParam, LPARAM lParam );
	afx_msg LRESULT OnDisplayError         ( WPARAM wParam, LPARAM lParam );
	afx_msg LRESULT OnSetWaitCursor        ( WPARAM wParam, LPARAM lParam );
	afx_msg LRESULT OnSetStopCursor        ( WPARAM wParam, LPARAM lParam );
	
	CComboBox m_CmbDeviceName;
	CListCtrl m_ListLog;		
	CComboBox m_CmbComPort;
	CString   m_sCOVersion;
	CString   m_sSOVersion;
	BOOL      m_bLogFile;
	afx_msg void OnBnClickedBtnDysplayText();
	afx_msg void OnBnClickedBtnOpenCashDrawer();
	afx_msg void OnBnClickedBtnDirectio();
	afx_msg void OnDestroy();	
	afx_msg void OnBnClickedChkLog();
};
