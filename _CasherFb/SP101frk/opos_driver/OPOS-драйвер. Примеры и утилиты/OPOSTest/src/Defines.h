#pragma once

// messages types to GUI thread
#define TM_ADD_STRING_TO_LOG     WM_USER + 2001
#define TM_STARTED_FUNCTION      WM_USER + 2002
#define TM_COMPLETED_FUNCTION    WM_USER + 2003
#define TM_COMPLETED_FR_PROCESS  WM_USER + 2004
#define TM_COMPLETED_LD_PROCESS  WM_USER + 2005
#define TM_COMPLETED_CD_PROCESS  WM_USER + 2006
#define TM_ADD_ITEM_TO_LOG       WM_USER + 2007
#define TM_DISPLAY_ERROR         WM_USER + 2008
#define TM_SET_WAIT_CURSOR       WM_USER + 2009
#define TM_SET_STOP_CURSOR       WM_USER + 2010

#define WaitCursor() ::SetCursor(::LoadCursor(NULL, IDC_WAIT));
#define StopCursor() ::SetCursor(::LoadCursor(NULL, IDC_ARROW));	

