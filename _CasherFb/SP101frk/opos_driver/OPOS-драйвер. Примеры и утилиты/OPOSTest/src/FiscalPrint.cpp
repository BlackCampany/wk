#include "StdAfx.h"
#include "POSDevice.h"
#include "fiscalprint.h"
#include "math.h"


//------------------------------------------------------------------
// Constructor / destructor
//------------------------------------------------------------------
CFiscalPrint::CFiscalPrint(void) : CPOSDevice( FR )
{
//	m_FiscalPrinterPtr    = NULL;
//	m_FiscalPrinterWNPtr  = NULL;
	m_bSPFR101            = FALSE;
	m_sProgID.Format("Used ProgID: %s", "OPOS.FiscalPrinter" );
}
CFiscalPrint::~CFiscalPrint(void)
{	
	Close();
}
//------------------------------------------------------------------
// Close()
//------------------------------------------------------------------
void CFiscalPrint::Close()
{	
	//CPOSDevice::Close( m_FiscalPrinterPtr, m_FiscalPrinterWNPtr );	
	if( IsDeviceOpen() )
		CloseDevice();

	Release();
}
//------------------------------------------------------------------
// Init();
//------------------------------------------------------------------
BOOL CFiscalPrint::Init()
{
	HRESULT hres;
	
	try
	{	
		hres = m_FiscalPrinter112Ptr.CreateInstance("OPOS.FiscalPrinter"); m_OPOSVersion = v112;
		if( hres != S_OK && hres != E_NOINTERFACE) throw _com_error(hres);
		if( hres == E_NOINTERFACE ) { hres = m_FiscalPrinter111Ptr.CreateInstance("OPOS.FiscalPrinter"); m_OPOSVersion = v111; }
		if( hres == E_NOINTERFACE ) { hres = m_FiscalPrinter19Ptr.CreateInstance("OPOS.FiscalPrinter"); m_OPOSVersion = v19; }
		if( hres == E_NOINTERFACE ) { hres = m_FiscalPrinter18Ptr.CreateInstance("OPOS.FiscalPrinter"); m_OPOSVersion = v18; }
		if( hres == E_NOINTERFACE ) { hres = m_FiscalPrinter16Ptr.CreateInstance("OPOS.FiscalPrinter"); m_OPOSVersion = v16; }
//		if( hres == E_NOINTERFACE ) { hres = m_FiscalPrinter15Ptr.CreateInstance("OPOS.FiscalPrinter"); m_OPOSVersion = v15; }
//		if( hres == E_NOINTERFACE ) { hres = m_FiscalPrinter14Ptr.CreateInstance("OPOS.FiscalPrinter"); m_OPOSVersion = v14; }
		if( hres == E_NOINTERFACE ) 
		{
			hres = m_FiscalPrinterWNPtr.CreateInstance("OPOS.FiscalPrinter");
			if( hres == S_OK ) { m_bWNInterfaceUsed = TRUE; m_OPOSVersion = vWN; }
		}

		if( hres != S_OK ) 
		{
			throw _com_error(hres);
			
//			CPOSDevice::SetLastError( E_NOINTERFACE, m_sProgID );
			return FALSE;
		}
		else
		{			
            AttachWorkInterface();
		}
	}
	catch(_com_error &ex)
	{	
		CString sExt;
		if( ex.Error() != CO_E_CLASSSTRING )		
		{
			sExt.Format("%s.\n%s", m_sProgID, ex.ErrorMessage() );
			CPOSDevice::SetLastError( ERROR_CO_CLASSSTRING, sExt );					
		}
		else
		{			
			sExt.Format( "%s.\n%s.\nIt is possible Common Control Object is not installed.", m_sProgID, ex.ErrorMessage() );
			CPOSDevice::SetLastError( ERROR_CO_GENERAL, sExt );	
		}
		return FALSE;
	}
	catch (...) 
	{
		CPOSDevice::SetLastError( ERROR_UNKNOWN );
		return FALSE;
	}

	return TRUE;
}
//------------------------------------------------------------------
// IsDrvLoaded()
//------------------------------------------------------------------
BOOL CFiscalPrint::IsDrvLoaded()
{
	if( IsOldInterfaceUsed() )
	{
		return m_FiscalPrinterWNPtr == NULL ? FALSE : TRUE;
	}
	else
	{		
		return m_FiscalPrinterPtr == NULL ? FALSE : TRUE;
	}
}
//------------------------------------------------------------------
// IsDeviceOpen()
//------------------------------------------------------------------
BOOL CFiscalPrint::IsDeviceOpen()
{	
	if( !IsDrvLoaded() )
	{	
		return FALSE;
	}

	if( IsOldInterfaceUsed() )
	{
		if( m_FiscalPrinterWNPtr->GetState() != OPOS_S_CLOSED ) 		
			return TRUE;		
		else
			return FALSE;
	}
	else
	{
		if( m_FiscalPrinterPtr->GetState() != OPOS_S_CLOSED ) 		
			return TRUE;		
		else
			return FALSE;        
	}
}
//------------------------------------------------------------------
// CheckReady()
//------------------------------------------------------------------
DWORD CFiscalPrint::CheckReady()
{
	m_sError      = "";
	m_dwErrorCode = 0;
	m_sErrorExt   = "";

	if( !IsDrvLoaded() )
	{
		CPOSDevice::SetLastError( FP_ERROR_DRV_NOT_LOADED );
		return FP_ERROR_DRV_NOT_LOADED;
	};

	if( !IsDeviceOpen() )
	{
		CPOSDevice::SetLastError( FP_ERROR_NOT_OPEN );
		return FP_ERROR_NOT_OPEN;
	};

	return SUCCESS;
}
//------------------------------------------------------------------
// IsSPFR101()
//------------------------------------------------------------------
BOOL CFiscalPrint::IsSPFR101()
{
	return m_bSPFR101;
}
//------------------------------------------------------------------
// AttachWorkInterface()
//------------------------------------------------------------------
void CFiscalPrint::AttachWorkInterface()
{
	if( m_FiscalPrinter112Ptr != NULL ) { m_FiscalPrinterPtr.Attach(m_FiscalPrinter112Ptr); return; };
	if( m_FiscalPrinter111Ptr != NULL ) { m_FiscalPrinterPtr.Attach(m_FiscalPrinter111Ptr); return; };
	if( m_FiscalPrinter19Ptr != NULL ) { m_FiscalPrinterPtr.Attach(m_FiscalPrinter19Ptr); return; };
	if( m_FiscalPrinter18Ptr != NULL ) { m_FiscalPrinterPtr.Attach(m_FiscalPrinter18Ptr); return; };
	if( m_FiscalPrinter16Ptr != NULL ) { m_FiscalPrinterPtr.Attach(m_FiscalPrinter16Ptr); return; };
//	if( m_FiscalPrinter15Ptr != NULL ) { m_FiscalPrinterPtr.Attach(m_FiscalPrinter15Ptr); return; };
//	if( m_FiscalPrinter14Ptr != NULL ) { m_FiscalPrinterPtr.Attach(m_FiscalPrinter14Ptr); return; };
}
//------------------------------------------------------------------
// Release()
//------------------------------------------------------------------
void CFiscalPrint::Release()
{	
	if( m_FiscalPrinterPtr   != NULL ) m_FiscalPrinterPtr.Detach();
	if( m_FiscalPrinter112Ptr != NULL ) m_FiscalPrinter112Ptr.Release();
	if( m_FiscalPrinter111Ptr != NULL ) m_FiscalPrinter111Ptr.Release();
	if( m_FiscalPrinter19Ptr != NULL ) m_FiscalPrinter19Ptr.Release();
	if( m_FiscalPrinter18Ptr != NULL ) m_FiscalPrinter18Ptr.Release();
	if( m_FiscalPrinter16Ptr != NULL ) m_FiscalPrinter16Ptr.Release();
//	if( m_FiscalPrinter15Ptr != NULL ) m_FiscalPrinter15Ptr.Release();
//	if( m_FiscalPrinter14Ptr != NULL ) m_FiscalPrinter14Ptr.Release();	
	if( m_FiscalPrinterWNPtr != NULL ) m_FiscalPrinterWNPtr.Release();
}
//------------------------------------------------------------------
// ResetPrinter()
//------------------------------------------------------------------
DWORD CFiscalPrint::ResetPrinter()
{
	if( IsOldInterfaceUsed() )    return ResetPrinter( m_FiscalPrinterWNPtr );
	else                          return ResetPrinter( m_FiscalPrinterPtr   );
}
//------------------------------------------------------------------
// SetLastError()
//------------------------------------------------------------------
void CFiscalPrint::SetLastError( DWORD dwErrorCode, long dwExtErrorCode )
{
	if( IsOldInterfaceUsed() )    return CPOSDevice::SetLastError( m_FiscalPrinterWNPtr, dwErrorCode, dwExtErrorCode );
	else                          return CPOSDevice::SetLastError( m_FiscalPrinterPtr,   dwErrorCode, dwExtErrorCode );
}
//------------------------------------------------------------------
DWORD CFiscalPrint::OpenDevice(LPCSTR lpszDeviceName)
{	
	USES_CONVERSION;
	if( strcmp( lpszDeviceName, "SPFR101" ) == 0 ||	strcmp( lpszDeviceName, "SPFR101.COM" ) == 0 )
		m_bSPFR101 = TRUE;
	else
		m_bSPFR101 = FALSE;

	if( IsOldInterfaceUsed() )  return CPOSDevice::OpenDevice( m_FiscalPrinterWNPtr, lpszDeviceName );		
	else                        return CPOSDevice::OpenDevice( m_FiscalPrinterPtr,    lpszDeviceName );
}
//------------------------------------------------------------------
void CFiscalPrint::CloseDevice()
{
	if( IsOldInterfaceUsed() )    return CPOSDevice::CloseDevice( m_FiscalPrinterWNPtr );
	else                          return CPOSDevice::CloseDevice( m_FiscalPrinterPtr    );	
}
//------------------------------------------------------------------
DWORD CFiscalPrint::PrintXReport()
{
	if( IsOldInterfaceUsed() )    return PrintXReport( m_FiscalPrinterWNPtr );
	else                          return PrintXReport( m_FiscalPrinterPtr    );		
}
//------------------------------------------------------------------
DWORD CFiscalPrint::PrintZReport()
{
	if( IsOldInterfaceUsed() )    return PrintZReport( m_FiscalPrinterWNPtr );
	else                          return PrintZReport( m_FiscalPrinterPtr    );
}
//------------------------------------------------------------------
DWORD CFiscalPrint::DirectIO()
{
	if( IsOldInterfaceUsed() )    return DirectIO( m_FiscalPrinterWNPtr );
	else                          return DirectIO( m_FiscalPrinterPtr    );
}
//------------------------------------------------------------------
DWORD CFiscalPrint::PrintNonfiscal()
{
	if( IsOldInterfaceUsed() )    return PrintNonfiscal( m_FiscalPrinterWNPtr );
	else                          return PrintNonfiscal( m_FiscalPrinterPtr    );	
}
//------------------------------------------------------------------
DWORD CFiscalPrint::PrintSaleReceipt()
{
	if( IsOldInterfaceUsed() )    return PrintSaleReceipt( m_FiscalPrinterWNPtr );
	else                          return PrintSaleReceipt( m_FiscalPrinterPtr    );	
}
//------------------------------------------------------------------
DWORD CFiscalPrint::PrintRefundReceipt()
{
	switch(m_OPOSVersion)
	{
	case vWN: 
		return PrintRefundReceipt( m_FiscalPrinterWNPtr );
	case v112:
		return PrintRefundReceiptNew( m_FiscalPrinter112Ptr );
	default:
		return PrintRefundReceipt( m_FiscalPrinterPtr );
	}
}
//------------------------------------------------------------------
DWORD CFiscalPrint::PrintCashIn()
{
	if( IsOldInterfaceUsed() )    return PrintCashIn( m_FiscalPrinterWNPtr );
	else                          return PrintCashIn( m_FiscalPrinterPtr    );	
}
//------------------------------------------------------------------
DWORD CFiscalPrint::PrintCashOut()
{
	if( IsOldInterfaceUsed() )    return PrintCashOut( m_FiscalPrinterWNPtr );
	else                          return PrintCashOut( m_FiscalPrinterPtr    );		
}
//------------------------------------------------------------------
DWORD CFiscalPrint::CancelReceipt()
{
	if( IsOldInterfaceUsed() )    return CancelReceipt( m_FiscalPrinterWNPtr );
	else                          return CancelReceipt( m_FiscalPrinterPtr    );		
}
//------------------------------------------------------------------
DWORD CFiscalPrint::SetDate()
{
	if( IsOldInterfaceUsed() )    return SetDate( m_FiscalPrinterWNPtr );
	else                          return SetDate( m_FiscalPrinterPtr    );
}
//------------------------------------------------------------------
DWORD CFiscalPrint::SetHeaderAndTrailer()
{
	if( IsOldInterfaceUsed() )    return SetHeaderAndTrailer( m_FiscalPrinterWNPtr );
	else                          return SetHeaderAndTrailer( m_FiscalPrinterPtr    );
}

/*
// commented code illustrates setting and getting a vat info for SPFR101
// the day must be closed
if( pInterface->GetCapSetVatTable() != FALSE && pInterface->GetCapHasVatTable() != FALSE )
{
RUN_OPOS_FUNC( SetVatValue(1, "0"),      FP_ERROR_BEGIN_NONFISCAL );
RUN_OPOS_FUNC( SetVatValue(2, "205000"), FP_ERROR_BEGIN_NONFISCAL ); // 20.50%
RUN_OPOS_FUNC( SetVatValue(3, "30000"),  FP_ERROR_BEGIN_NONFISCAL );
RUN_OPOS_FUNC( SetVatValue(3, "60000"),  FP_ERROR_BEGIN_NONFISCAL );
RUN_OPOS_FUNC( SetVatValue(4, "160000"), FP_ERROR_BEGIN_NONFISCAL );
RUN_OPOS_FUNC( SetVatTable(),            FP_ERROR_BEGIN_NONFISCAL );
}
if( pInterface->GetCapSetVatTable() != FALSE )		
{			
RUN_OPOS_FUNC( GetVatEntry(2, lExt, &lRate), FP_ERROR_BEGIN_NONFISCAL );
}
*/
//------------------------------------------------------------------
// PrintNonfiscal()
//------------------------------------------------------------------
template<class T>
DWORD CFiscalPrint::PrintNonfiscal( T pInterface )
{
	DWORD dwError;
	VARIANT_BOOL  bCap  = FALSE;
	long          lExt  = 0;
	long          lRate = 0;
	long          lPar  = 0;

	if( (dwError = CheckReady()) != SUCCESS )	
		return dwError;	

	try
	{	
		bCap = pInterface->GetCapNonFiscalMode();
		AddItemToLog( CLogItem( "GetCapNonFiscalMode()", bCap ) );
		if( bCap != FALSE )
		{
			ResetPrinter();		

			RUN_OPOS_FUNC( BeginNonFiscal(),                                                       FP_ERROR_BEGIN_NONFISCAL );
			RUN_OPOS_FUNC( PrintNormal(FPTR_S_RECEIPT,"\x1b|bC\x1b|cATest Text 1\r\nTest Text 2"), FP_ERROR_PRINT_NORMAL );
			RUN_OPOS_FUNC( PrintNormal(FPTR_S_RECEIPT,"\x1b|rATest Text 3"),                       FP_ERROR_PRINT_NORMAL );
			RUN_OPOS_FUNC( PrintNormal(FPTR_S_RECEIPT,"\x1b|N����� �� ������\n"),                  FP_ERROR_PRINT_NORMAL );					
			RUN_OPOS_FUNC( PrintNormal(FPTR_S_RECEIPT,"-------------------------------------------------------------------------------------------"),                    FP_ERROR_PRINT_NORMAL );		
			RUN_OPOS_FUNC( EndNonFiscal(),                                                         FP_ERROR_END_NONFISCAL );			
		}
	}
	catch(_com_error &ex)
	{	
		CString sExt;
		if( ex.Error() != CO_E_CLASSSTRING )		
		{
			sExt.Format("%s.\n%s", m_sProgID, ex.ErrorMessage() );
			CPOSDevice::SetLastError( ERROR_CO_CLASSSTRING, sExt );								
		}
		else
		{			
			sExt.Format( "%s.\n%s.\nIt is possible Common Control Object is not installed.", m_sProgID, ex.ErrorMessage() );
			CPOSDevice::SetLastError( ERROR_CO_GENERAL, sExt );	
		}
		return FP_ERROR_BEGIN_NONFISCAL;		
	}	
	
	return SUCCESS;
}
//------------------------------------------------------------------
// PrintSaleReceipt()
//------------------------------------------------------------------
template<class T>
DWORD CFiscalPrint::PrintSaleReceipt(T pInterface)
{
	USES_CONVERSION;
	DWORD dwError;
	if( (dwError = CheckReady()) != SUCCESS )	
		return dwError;

	CURRENCY cyPrice, cyNotUsed, cyUnitPrice;
	cyNotUsed.int64 = 0;
	long lVatInfo = 0;		
	long lAmount = 0;
	VARIANT_BOOL bCap;
	
	ResetPrinter();

	bCap = pInterface->GetCapFiscalReceiptStation();
	AddItemToLog( CLogItem( "GetCapFiscalReceiptStation()", bCap ) );
	if( bCap != FALSE )
	{	
		pInterface->SetFiscalReceiptStation( FPTR_S_RECEIPT );
	}

	if( pInterface->GetCapFiscalReceiptType() != VARIANT_FALSE )
	{
		pInterface->SetFiscalReceiptType( FPTR_RT_SALES );
	}			

	RUN_OPOS_FUNC( BeginFiscalReceipt( TRUE ), FP_ERROR_SALE_RECEIPT );	

	if( pInterface->GetCapHasVatTable() != VARIANT_FALSE )	
		lVatInfo = 2;	
	else
		lVatInfo = 0;	

    // ���� ��� ����-������
/*
	cyPrice.int64     = 143100;
	cyUnitPrice.int64 = 143100;		
	RUN_OPOS_FUNC( PrintRecItem( "Item 1", cyPrice, 1000, lVatInfo, cyUnitPrice, "" ), FP_ERROR_SALE_RECEIPT );			
	RUN_OPOS_FUNC( PrintRecItem( "Item 1", cyPrice, 1000, 3, cyUnitPrice, "" ), FP_ERROR_SALE_RECEIPT );			
	RUN_OPOS_FUNC( PrintRecSubtotal( cyNotUsed ), FP_ERROR_SALE_RECEIPT );
	cyPrice.int64 = 143100;
	RUN_OPOS_FUNC( PrintRecVoidItem( "Item 1", cyPrice, 0, 0, cyNotUsed, lVatInfo ), FP_ERROR_SALE_RECEIPT );
	cyPrice.int64     = 143100;
	cyUnitPrice.int64 = 143100;		
	RUN_OPOS_FUNC( PrintRecItem( "Item 1", cyPrice, 1000, lVatInfo, cyUnitPrice, "" ), FP_ERROR_SALE_RECEIPT );
	//RUN_OPOS_FUNC( PrintRecSubtotal( cyNotUsed ), FP_ERROR_SALE_RECEIPT );
	CURRENCY cyPercent;	
	cyPercent.int64 = 126660;
	RUN_OPOS_FUNC( PrintRecSubtotalAdjustment(FPTR_AT_PERCENTAGE_DISCOUNT, "Discount 3" , cyPercent ), FP_ERROR_SALE_RECEIPT );
	CURRENCY cyPayment;	
	cyPayment.int64 = 1000000;
	RUN_OPOS_FUNC( PrintRecTotal( cyNotUsed, cyPayment, "0" ), FP_ERROR_SALE_RECEIPT );
*/

	if( pInterface->GetCapOrderAdjustmentFirst() == VARIANT_FALSE )
	{	// �� ����� �������� ������� ����������� ������ ������� PrintRecItem � PrintRecItemAdjustment

		bCap = pInterface->GetCapPostPreLine();
		AddItemToLog( CLogItem( "GetCapPostPreLine()", bCap ) );
		if( bCap != FALSE )
		{
			pInterface->SetPostLine( "--Test PostLine 1----------------" );			
			pInterface->SetPreLine ( "--Test PreLine 1-----------------" );			
		}
	
		cyPrice.int64     = 496000;
		cyUnitPrice.int64 = 49600;		
		RUN_OPOS_FUNC( PrintRecItem( "Item 1", cyPrice, 10000, lVatInfo, cyUnitPrice, "" ), FP_ERROR_SALE_RECEIPT );			
		if( pInterface->GetCapPercentAdjustment() != VARIANT_FALSE)
		{
			cyPrice.int64 = 125000;
			RUN_OPOS_FUNC( PrintRecItemAdjustment(FPTR_AT_PERCENTAGE_DISCOUNT, "Discount 1" , cyPrice, 0 ), FP_ERROR_SALE_RECEIPT );
		}
		if( pInterface->GetCapAmountAdjustment() != VARIANT_FALSE)
		{
			cyPrice.int64 = 16700;
			RUN_OPOS_FUNC( PrintRecItemAdjustment(FPTR_AT_AMOUNT_SURCHARGE, "Surcharge 1", cyPrice, 0), FP_ERROR_SALE_RECEIPT );
		}	

		bCap = pInterface->GetCapPostPreLine();
		AddItemToLog( CLogItem( "GetCapPostPreLine()", bCap ) );
		if( bCap != FALSE )
		{
			pInterface->SetPostLine( "--Test PostLine 1----------------" );			
			pInterface->SetPreLine ( "--Test PreLine 1-----------------" );			
		}

		cyPrice.int64   = 4205000;		
		RUN_OPOS_FUNC( PrintRecItem( "Item 2", cyPrice, 2000, lVatInfo, cyNotUsed, "" ), FP_ERROR_SALE_RECEIPT );		

		if( pInterface->GetCapPercentAdjustment() != VARIANT_FALSE)
		{
			cyPrice.int64 = 25000;			
			RUN_OPOS_FUNC( PrintRecItemAdjustment(FPTR_AT_PERCENTAGE_SURCHARGE, "Surcharge 2", cyPrice, 0), FP_ERROR_SALE_RECEIPT );
		}
		cyPrice.int64 = 4205000;
		if( m_OPOSVersion ==  v111 )
		{
			RUN_OPOS_FUNC_111( PrintRecItemVoid( "Item 2", cyNotUsed, 2000, lVatInfo, cyPrice, "" ), FP_ERROR_SALE_RECEIPT );
		}
		else if( m_OPOSVersion ==  v112 )
		{
			RUN_OPOS_FUNC_112( PrintRecItemVoid( "Item 2", cyNotUsed, 2000, lVatInfo, cyPrice, "" ), FP_ERROR_SALE_RECEIPT );
		}
		else
		{
			RUN_OPOS_FUNC( PrintRecVoidItem( "Item 2", cyPrice, 0, 0, cyNotUsed, lVatInfo ), FP_ERROR_SALE_RECEIPT );
		}
		
		
		if( pInterface->GetCapPercentAdjustment() != VARIANT_FALSE)
		{	
			cyPrice.int64 = 25000;
			if( m_OPOSVersion == v111 )
			{
				RUN_OPOS_FUNC_111( PrintRecItemAdjustmentVoid(FPTR_AT_PERCENTAGE_SURCHARGE, "Surcharge 2", cyPrice, 0), FP_ERROR_SALE_RECEIPT );
			}
			else if( m_OPOSVersion == v112 )
			{
				RUN_OPOS_FUNC_112( PrintRecItemAdjustmentVoid(FPTR_AT_PERCENTAGE_SURCHARGE, "Surcharge 2", cyPrice, 0), FP_ERROR_SALE_RECEIPT );
			}
			else
				RUN_OPOS_FUNC( PrintRecItemAdjustment(FPTR_AT_PERCENTAGE_SURCHARGE, "Surcharge 2", cyPrice, 0), FP_ERROR_SALE_RECEIPT );

		}
	}
	else
	{
		if( pInterface->GetCapPercentAdjustment() != VARIANT_FALSE)
		{
			cyPrice.int64 = 125000;
			RUN_OPOS_FUNC( PrintRecItemAdjustment(FPTR_AT_PERCENTAGE_DISCOUNT, "Discount 1" , cyPrice, 0 ), FP_ERROR_SALE_RECEIPT );
		}
		if( pInterface->GetCapAmountAdjustment() != VARIANT_FALSE)
		{
			cyPrice.int64 = 16700;
			RUN_OPOS_FUNC( PrintRecItemAdjustment(FPTR_AT_AMOUNT_SURCHARGE, "Surcharge 1", cyPrice, 0), FP_ERROR_SALE_RECEIPT );
		}
		cyPrice.int64 = 49600;	
		RUN_OPOS_FUNC( PrintRecItem( "Item 1", cyPrice, 10000, lVatInfo, cyNotUsed, "" ), FP_ERROR_SALE_RECEIPT );			
		
		if( pInterface->GetCapPercentAdjustment() != VARIANT_FALSE)
		{
			cyPrice.int64 = 25000;
			RUN_OPOS_FUNC( PrintRecItemAdjustment(FPTR_AT_PERCENTAGE_SURCHARGE, "Surcharge 2", cyPrice, 0), FP_ERROR_SALE_RECEIPT );
		}
		cyPrice.int64 = 49600;	
		RUN_OPOS_FUNC( PrintRecItem( "Item 1", cyPrice, 10000, lVatInfo, cyNotUsed, "" ), FP_ERROR_SALE_RECEIPT );			
		if( pInterface->GetCapPercentAdjustment() != VARIANT_FALSE)
		{	
			cyPrice.int64 = 25000;
			RUN_OPOS_FUNC( PrintRecItemAdjustment(FPTR_AT_PERCENTAGE_SURCHARGE, "Surcharge 2", cyPrice, 0), FP_ERROR_SALE_RECEIPT );
		}
		cyPrice.int64 = 49600;	
		RUN_OPOS_FUNC( PrintRecItem( "Item 1", cyPrice, 10000, 0, cyNotUsed, "" ), FP_ERROR_SALE_RECEIPT );			
	}

	RUN_OPOS_FUNC( PrintRecSubtotal(cyNotUsed), FP_ERROR_SALE_RECEIPT );	
	cyPrice.int64 = 5500;
	if( pInterface->GetCapAmountAdjustment() != VARIANT_FALSE)
	{
		if(IsSPFR101())
		{	
			cyPrice.int64 = 100000;
			RUN_OPOS_FUNC( PrintRecSubtotalAdjustment(FPTR_AT_AMOUNT_DISCOUNT, "Discount 4" , cyPrice ), FP_ERROR_SALE_RECEIPT );			
		}
	}	

	if( pInterface->GetCapOrderAdjustmentFirst() == VARIANT_FALSE )
	{	// �� ����� �������� ������� ����������� ������ ������� PrintRecItem � PrintRecItemAdjustment

		cyPrice.int64     = 100000;
		cyUnitPrice.int64 = 10000;		
		RUN_OPOS_FUNC( PrintRecItem( "Item 1", cyPrice, 10000, lVatInfo, cyUnitPrice, "" ), FP_ERROR_SALE_RECEIPT );			
		if( pInterface->GetCapPercentAdjustment() != VARIANT_FALSE)
		{
			cyPrice.int64 = 125000;
			RUN_OPOS_FUNC( PrintRecItemAdjustment(FPTR_AT_PERCENTAGE_DISCOUNT, "Discount 1" , cyPrice, 0 ), FP_ERROR_SALE_RECEIPT );
		}
		if( pInterface->GetCapAmountAdjustment() != VARIANT_FALSE)
		{
			cyPrice.int64 = 16700;
			RUN_OPOS_FUNC( PrintRecItemAdjustment(FPTR_AT_AMOUNT_SURCHARGE, "Surcharge 1", cyPrice, 0), FP_ERROR_SALE_RECEIPT );
		}	
		
		RUN_OPOS_FUNC( PrintRecSubtotal(cyNotUsed), FP_ERROR_SALE_RECEIPT );	
		if( pInterface->GetCapAmountAdjustment() != VARIANT_FALSE)
		{
			if(IsSPFR101())
			{
				CURRENCY cyPercent;	
				cyPercent.int64 = 126660;
				RUN_OPOS_FUNC( PrintRecSubtotalAdjustment(FPTR_AT_PERCENTAGE_DISCOUNT, "Discount 3" , cyPercent ), FP_ERROR_SALE_RECEIPT );
				RUN_OPOS_FUNC( PrintRecSubtotalAdjustVoid(FPTR_AT_PERCENTAGE_DISCOUNT, cyPercent ), FP_ERROR_SALE_RECEIPT );
			}
		}	
	}
	else
	{
		if( pInterface->GetCapPercentAdjustment() != VARIANT_FALSE)
		{
			cyPrice.int64 = 125000;
			RUN_OPOS_FUNC( PrintRecItemAdjustment(FPTR_AT_PERCENTAGE_DISCOUNT, "Discount 1" , cyPrice, 0 ), FP_ERROR_SALE_RECEIPT );
		}
		if( pInterface->GetCapAmountAdjustment() != VARIANT_FALSE)
		{
			cyPrice.int64 = 16700;
			RUN_OPOS_FUNC( PrintRecItemAdjustment(FPTR_AT_AMOUNT_SURCHARGE, "Surcharge 1", cyPrice, 0), FP_ERROR_SALE_RECEIPT );
		}
		cyPrice.int64 = 49600;	
		RUN_OPOS_FUNC( PrintRecItem( "Item 1", cyPrice, 10000, lVatInfo, cyNotUsed, "" ), FP_ERROR_SALE_RECEIPT );	
	}

	CURRENCY cyPayment;	
	cyPayment.int64 = 500000;
	RUN_OPOS_FUNC( PrintRecTotal( cyNotUsed, cyPayment, "1" ), FP_ERROR_SALE_RECEIPT );	

	double dTotal = 0;
	GetCurrentTotal( dTotal );
	if( dTotal > 0 )
	{
		cyPayment.int64 = (LONGLONG)(floor(dTotal+1) * 10000)-cyPayment.int64;
		if( cyPayment.int64 > 0 )
		{
			RUN_OPOS_FUNC( PrintRecTotal( cyNotUsed, cyPayment, "0"), FP_ERROR_SALE_RECEIPT );		
		}
	}
	RUN_OPOS_FUNC( PrintRecMessage( "Test Message"), FP_ERROR_SALE_RECEIPT );
	
	RUN_OPOS_FUNC( EndFiscalReceipt( TRUE ), FP_ERROR_SALE_RECEIPT );

	return SUCCESS;
}
//------------------------------------------------------------------
// PrintRefundReceipt()
//------------------------------------------------------------------
template<class T>
DWORD CFiscalPrint::PrintRefundReceipt( T pInterface )
{
	DWORD dwError;
	if( (dwError = CheckReady()) != SUCCESS )	
		return dwError;	

	CURRENCY cyPrice, cyNotUsed;
	cyNotUsed.int64 = 0;  

	ResetPrinter();

	pInterface->SetAdditionalHeader( "Cashier" );
	
	if( pInterface->GetCapFiscalReceiptType() != FALSE )
	{
		pInterface->SetFiscalReceiptType( FPTR_RT_SALES );
	}

	RUN_OPOS_FUNC( BeginFiscalReceipt( TRUE ), FP_ERROR_REFUND_RECEIPT );

	if( pInterface->GetCapOrderAdjustmentFirst() == FALSE )
	{	
		cyPrice.int64 = 49600;

		RUN_OPOS_FUNC( PrintRecRefund( "Item 1", cyPrice, 0 ), FP_ERROR_REFUND_RECEIPT );

		if( pInterface->GetCapPercentAdjustment() != FALSE)
		{
			cyPrice.int64 = 125000;
			RUN_OPOS_FUNC( PrintRecItemAdjustment(FPTR_AT_PERCENTAGE_DISCOUNT, "Discount 1" , cyPrice, 0 ), FP_ERROR_REFUND_RECEIPT );
		}
		if( pInterface->GetCapAmountAdjustment() != FALSE)
		{
			cyPrice.int64 = 16700;							//1.67 in CURRENCY
			RUN_OPOS_FUNC( PrintRecItemAdjustment(FPTR_AT_AMOUNT_SURCHARGE, "Surcharge 1", cyPrice, 0), FP_ERROR_REFUND_RECEIPT );
		}
	}
	else
	{
		if( pInterface->GetCapPercentAdjustment() != FALSE)
		{
			cyPrice.int64 = 125000;
			RUN_OPOS_FUNC( PrintRecItemAdjustment(FPTR_AT_PERCENTAGE_DISCOUNT, "Discount 1" , cyPrice, 0 ), FP_ERROR_REFUND_RECEIPT );
		}
		if( pInterface->GetCapAmountAdjustment() != FALSE)
		{
			cyPrice.int64 = 16700;
			RUN_OPOS_FUNC( PrintRecItemAdjustment(FPTR_AT_AMOUNT_SURCHARGE, "Surcharge 1", cyPrice, 0), FP_ERROR_REFUND_RECEIPT );
		}
		cyPrice.int64 = 49600;
		
		RUN_OPOS_FUNC( PrintRecRefund( "Item 1", cyPrice, 0 ), FP_ERROR_REFUND_RECEIPT );				
	}

	RUN_OPOS_FUNC( PrintRecSubtotal(cyNotUsed), FP_ERROR_REFUND_RECEIPT );
	if( pInterface->GetCapAmountAdjustment() != FALSE)
	{
		cyPrice.int64 = 55000;
		RUN_OPOS_FUNC( PrintRecSubtotalAdjustment(FPTR_AT_AMOUNT_DISCOUNT, "Discount 2" ,cyPrice), FP_ERROR_REFUND_RECEIPT );
	}
	
	cyPrice.int64 = 500000;
	RUN_OPOS_FUNC( PrintRecTotal( cyNotUsed, cyPrice, "0"/*CASH*/), FP_ERROR_REFUND_RECEIPT );

	RUN_OPOS_FUNC( EndFiscalReceipt( TRUE ), FP_ERROR_REFUND_RECEIPT );
	
	return SUCCESS;
}
//------------------------------------------------------------------
// PrintRefundReceiptNew()
//------------------------------------------------------------------
DWORD CFiscalPrint::PrintRefundReceiptNew(IOPOSFiscalPrinter_1_12Ptr pInterface)
{
	DWORD dwError;
	if( (dwError = CheckReady()) != SUCCESS )	
		return dwError;	

	CURRENCY cyPrice, cyNotUsed;
	cyNotUsed.int64 = 0;  

	ResetPrinter();

	pInterface->SetAdditionalHeader( "Cashier" );

	if( pInterface->GetCapFiscalReceiptType() != FALSE )
	{
		pInterface->SetFiscalReceiptType( FPTR_RT_REFUND );
	}

	RUN_OPOS_FUNC( BeginFiscalReceipt( TRUE ), FP_ERROR_REFUND_RECEIPT );

	if( pInterface->GetCapOrderAdjustmentFirst() == FALSE )
	{	
		cyPrice.int64 = 49600;

		RUN_OPOS_FUNC_112( PrintRecItemRefund( "Item 1", cyNotUsed, 12000, 0, cyPrice, "" ), FP_ERROR_REFUND_RECEIPT );

		if( pInterface->GetCapPercentAdjustment() != FALSE)
		{
			cyPrice.int64 = 125000;
			RUN_OPOS_FUNC( PrintRecItemAdjustment(FPTR_AT_PERCENTAGE_DISCOUNT, "Discount 1" , cyPrice, 0 ), FP_ERROR_REFUND_RECEIPT );
		}
		if( pInterface->GetCapAmountAdjustment() != FALSE)
		{
			cyPrice.int64 = 16700;							//1.67 in CURRENCY
			RUN_OPOS_FUNC( PrintRecItemAdjustment(FPTR_AT_AMOUNT_SURCHARGE, "Surcharge 1", cyPrice, 0), FP_ERROR_REFUND_RECEIPT );			
		}
	}
	else
	{
		if( pInterface->GetCapPercentAdjustment() != FALSE)
		{
			cyPrice.int64 = 125000;
			RUN_OPOS_FUNC( PrintRecItemAdjustment(FPTR_AT_PERCENTAGE_DISCOUNT, "Discount 1" , cyPrice, 0 ), FP_ERROR_REFUND_RECEIPT );
		}
		if( pInterface->GetCapAmountAdjustment() != FALSE)
		{
			cyPrice.int64 = 16700;
			RUN_OPOS_FUNC( PrintRecItemAdjustment(FPTR_AT_AMOUNT_SURCHARGE, "Surcharge 1", cyPrice, 0), FP_ERROR_REFUND_RECEIPT );
		}
		cyPrice.int64 = 49600;

		RUN_OPOS_FUNC_112( PrintRecItemRefund( "Item 1", cyNotUsed, 12000, 0, cyPrice, "" ), FP_ERROR_REFUND_RECEIPT );		
	}

	/////
	RUN_OPOS_FUNC_112( PrintRecItemRefund( "Item 2", cyNotUsed, 12000, 1, cyPrice, "" ), FP_ERROR_REFUND_RECEIPT );		
	RUN_OPOS_FUNC_112( PrintRecItemRefundVoid( "Item 2", cyNotUsed, 12000, 1, cyPrice, "" ), FP_ERROR_REFUND_RECEIPT );		
	////

	RUN_OPOS_FUNC( PrintRecSubtotal(cyNotUsed), FP_ERROR_REFUND_RECEIPT );
	if( pInterface->GetCapAmountAdjustment() != FALSE)
	{
		cyPrice.int64 = 55000;
		RUN_OPOS_FUNC( PrintRecSubtotalAdjustment(FPTR_AT_AMOUNT_DISCOUNT, "Discount 2" ,cyPrice), FP_ERROR_REFUND_RECEIPT );
	}

	cyPrice.int64 = 500000;
	RUN_OPOS_FUNC( PrintRecTotal( cyNotUsed, cyPrice, "0"/*CASH*/), FP_ERROR_REFUND_RECEIPT );

	RUN_OPOS_FUNC( EndFiscalReceipt( TRUE ), FP_ERROR_REFUND_RECEIPT );

	return SUCCESS;
}
//------------------------------------------------------------------
// PrintCashIn()
//------------------------------------------------------------------
template<class T>
DWORD CFiscalPrint::PrintCashIn(T pInterface)
{
	DWORD dwError;
	if( (dwError = CheckReady()) != SUCCESS )	
		return dwError;

	CURRENCY cyAmount, cyNotUsed;
	cyNotUsed.int64 = 0;  	
	VARIANT_BOOL bCap;

	ResetPrinter();

	pInterface->SetAdditionalHeader( "Cashier" );	

	bCap = pInterface->GetCapFiscalReceiptType();
	AddItemToLog( CLogItem( "GetCapFiscalReceiptType()", bCap ) );

	if( bCap != FALSE )
	{
		pInterface->SetFiscalReceiptType( FPTR_RT_CASH_IN );				

		RUN_OPOS_FUNC( BeginFiscalReceipt( TRUE ), FP_ERROR_CASH_OUT );
		
		cyAmount.int64 = 1000000;
		RUN_OPOS_FUNC( PrintRecCash( cyAmount ), FP_ERROR_CASH_IN );		

		cyAmount.int64 = 2000000;
		RUN_OPOS_FUNC( PrintRecCash( cyAmount ), FP_ERROR_CASH_IN );		
		
		cyAmount.int64 = 3000000;
		RUN_OPOS_FUNC( PrintRecTotal( cyAmount, cyAmount, "" ), FP_ERROR_CASH_IN );		

		RUN_OPOS_FUNC( EndFiscalReceipt( TRUE ), FP_ERROR_CASH_IN );
	};
	
	return SUCCESS;
}
//------------------------------------------------------------------
// PrintCashIn()
//------------------------------------------------------------------
template<class T>
DWORD CFiscalPrint::PrintCashOut(T pInterface)
{
	DWORD dwError;
	if( (dwError = CheckReady()) != SUCCESS )	
		return dwError;

	ResetPrinter();

	CURRENCY cyAmount, cyNotUsed;
	cyNotUsed.int64 = 0;  	

	pInterface->SetAdditionalHeader( "Cashier" );	

	VARIANT_BOOL bCap = pInterface->GetCapFiscalReceiptType();
	AddItemToLog( CLogItem( "GetCapFiscalReceiptType()", bCap ) );

	if( bCap != FALSE )
	{
		pInterface->SetFiscalReceiptType( FPTR_RT_CASH_OUT );				

		RUN_OPOS_FUNC( BeginFiscalReceipt( TRUE ), FP_ERROR_CASH_OUT );

		cyAmount.int64 = 1000000;
		RUN_OPOS_FUNC( PrintRecCash( cyAmount ), FP_ERROR_CASH_OUT );

		cyAmount.int64 = 2000000;
		RUN_OPOS_FUNC( PrintRecCash( cyAmount ), FP_ERROR_CASH_OUT );

		cyAmount.int64 = 3000000;
		RUN_OPOS_FUNC( PrintRecTotal( cyAmount, cyAmount, "" ), FP_ERROR_CASH_IN );		

		RUN_OPOS_FUNC( EndFiscalReceipt( TRUE ), FP_ERROR_CASH_OUT );
	};		

	return SUCCESS;    
}
//------------------------------------------------------------------
// CancelReceipt()
//------------------------------------------------------------------
template<class T>
DWORD CFiscalPrint::CancelReceipt(T pInterface)
{
	DWORD dwError;
	if( (dwError = CheckReady()) != SUCCESS )	
		return dwError;	

	pInterface->SetFiscalReceiptStation( FPTR_RS_RECEIPT );		

	long lState = pInterface->GetPrinterState();

	if( lState != FPTR_PS_MONITOR )
	{	
		RUN_OPOS_FUNC( PrintRecVoid("Void"), FP_ERROR_CANCEL_RECEIPT );
		RUN_OPOS_FUNC( EndFiscalReceipt(FALSE), FP_ERROR_CANCEL_RECEIPT );
	}

	return SUCCESS;    
}
//------------------------------------------------------------------
// PrintXReport()
//------------------------------------------------------------------
template<class T>
DWORD CFiscalPrint::PrintXReport( T pInterface )
{
	DWORD dwError;
	if( (dwError = CheckReady()) != SUCCESS )	
		return dwError;

	ResetPrinter();

	pInterface->SetAdditionalHeader( "Cashier" );	
	if( pInterface->GetCapXReport() != FALSE )
	{
		RUN_OPOS_FUNC( PrintXReport(), FP_ERROR_X_REPORT );
	}
	return SUCCESS;
}
//------------------------------------------------------------------
// PrintZReport()
//------------------------------------------------------------------
template<class T>
DWORD CFiscalPrint::PrintZReport( T pInterface )
{
	DWORD dwError;
	if( (dwError = CheckReady()) != SUCCESS )	
		return dwError;
	VARIANT_BOOL bOpened;

	ResetPrinter();
	
	pInterface->SetAdditionalHeader( "Cashier" );	
	bOpened = pInterface->GetDayOpened();
	AddItemToLog( CLogItem( "GetDayOpened()", bOpened ) );
	if( bOpened != FALSE )
	{
		RUN_OPOS_FUNC( PrintZReport(), FP_ERROR_Z_REPORT );		
	}
	
	return SUCCESS;
}
//------------------------------------------------------------------
// DirectIO()
//------------------------------------------------------------------
long g_long;
template<class T>
DWORD CFiscalPrint::DirectIO( T pInterface )
{
	DWORD dwError;
	if( (dwError = CheckReady()) != SUCCESS )	
		return dwError;

	char    szBuf[300];
	OLECHAR wstr [300]; 
	BSTR    bstrTmp = NULL;  
	BSTR    bstrTmp1 = NULL;  
	long    lData = 0;
	CString sStatus;
//
	long  lOpt = FPTR_PDL_FREE_GIFT;	

	if( pInterface->GetDayOpened() == FALSE ) 
	{ 
		AddItemToLog( CLogItem( CLogItem::stNeutral, "DirectIO(SetPayment(1,VISA GOLD))", 0, 0, "" ) );
		
		strcpy(szBuf,"22 1 VISA GOLD");
		szBuf[2] = szBuf[4] = 0x1c; // �����������
		MultiByteToWideChar( CP_ACP, 0, szBuf, -1, wstr, sizeof(wstr)/sizeof(wstr[0]) );
		bstrTmp = SysAllocString( wstr );
		
		RUN_OPOS_FUNC( DirectIO( 0xA2, &lData, &bstrTmp ), FP_ERROR_DIRECTIO );
		SysFreeString( bstrTmp );
		bstrTmp = NULL;

		AddItemToLog( CLogItem( CLogItem::stNeutral, "DirectIO(SetPayment(8,CREDIT CARD))", 0, 0, "" ) );

		strcpy(szBuf,"22 8 ��������� �����");
		szBuf[2] = szBuf[4] = 0x1c;
		MultiByteToWideChar( CP_ACP, 0, szBuf, -1, wstr, sizeof(wstr)/sizeof(wstr[0]) );		
		bstrTmp = SysAllocString( wstr );
		
		RUN_OPOS_FUNC( DirectIO( 0xA2, &lData, &bstrTmp ), FP_ERROR_DIRECTIO );		
		SysFreeString( bstrTmp );



		strcpy(szBuf,"C:\\logo.bmp");		
		MultiByteToWideChar( CP_ACP, 0, szBuf, -1, wstr, sizeof(wstr)/sizeof(wstr[0]) );		
		bstrTmp = SysAllocString( wstr );
		RUN_OPOS_FUNC( DirectIO( 0xE2, &lData, &bstrTmp ), FP_ERROR_DIRECTIO );		
		SysFreeString( bstrTmp );
	}	

	AddItemToLog( CLogItem( CLogItem::stNeutral, "OPOS DirectIO(GetStatusSPFR101())", 0, 0, "" ) );
	bstrTmp = SysAllocString( wstr );	
	RUN_OPOS_FUNC( DirectIO( 0xA0, &lData, &bstrTmp ), FP_ERROR_DIRECTIO );	
	WideCharToMultiByte( CP_ACP, 0, bstrTmp, -1, szBuf, sizeof(szBuf)/sizeof(szBuf[0]), NULL, NULL );
	SysFreeString( bstrTmp );

	int nStatus[3];	

	char *pi = szBuf, *p;
	for( int i = 0; i < 3; i++ )
	{
		nStatus[i] = 0;
		p = pi;
		pi = strchr(pi,0x1c);
		if( pi == NULL ) 
		{
			AddItemToLog( CLogItem( CLogItem::stNeutral, "DirectIO StatusSPFR101 not valid", 0, 0, "" ) );
			return FP_ERROR_DIRECTIO;
		}
		else 
		{ 
			*pi++=0;
			nStatus[i] = atoi(p);
		}
	};

	sStatus.Format( "DirectIO StatusSPFR101: Status1=%X  Status2=%X  Status3=%X", nStatus[0], nStatus[1], nStatus[2] );
	AddItemToLog( CLogItem( CLogItem::stNeutral, sStatus, 0, 0, "" ) );
	return SUCCESS;
}
//------------------------------------------------------------------
// SetDate()
//------------------------------------------------------------------
template<class T>
DWORD CFiscalPrint::SetDate( T pInterface )
{
	DWORD dwError;
	if( (dwError = CheckReady()) != SUCCESS )	
        return dwError;

	ResetPrinter();
	
	struct tm *tmNewTime;	
	TCHAR      tszDate[20];
	time_t     t = time(0);

	tmNewTime = localtime( &t );

	wsprintf( tszDate, "%02i%02i%04i%02i%02i", tmNewTime->tm_mday, tmNewTime->tm_mon+1,
		tmNewTime->tm_year+1900, tmNewTime->tm_hour, tmNewTime->tm_min );

	pInterface->SetDateType( FPTR_DT_RTC );		
	RUN_OPOS_FUNC( SetDate ( tszDate ),   FP_ERROR_SET_DATE );
	if( (pInterface->GetCapSetPOSID()) != FALSE )
		RUN_OPOS_FUNC( SetPOSID( "102", "Cashier1" ), FP_ERROR_SET_DATE );	
	return SUCCESS;
}
//------------------------------------------------------------------
// SetDate()
//------------------------------------------------------------------
template<class T>
DWORD CFiscalPrint::SetHeaderAndTrailer( T pInterface )
{
	DWORD dwError;
	if( (dwError = CheckReady()) != SUCCESS )	
		return dwError;

	ResetPrinter();

	if( pInterface->GetCapSetHeader() != FALSE )
	{
		long lLinesNum = pInterface->GetNumHeaderLines();
		if( lLinesNum >= 1 )
		{
			RUN_OPOS_FUNC( SetHeaderLine (1, "*** HEADER LINE 1 ***"  , 0), FP_ERROR_SET_HEADER_AND_TAILER );
		}
		if( lLinesNum >= 2 )
		{
			RUN_OPOS_FUNC( SetHeaderLine (2, "*** HEADER LINE 2 ***"  , 0), FP_ERROR_SET_HEADER_AND_TAILER );
		}
		if( lLinesNum >= 4 )
		{
			RUN_OPOS_FUNC( SetHeaderLine (4, "*** HEADER LINE 4 ***"  , 0), FP_ERROR_SET_HEADER_AND_TAILER );
		}		
	}

	if( pInterface->GetCapSetHeader() != FALSE )
	{
		long lLinesNum = pInterface->GetNumTrailerLines();
		if( lLinesNum >= 1 )
		{
			RUN_OPOS_FUNC( SetTrailerLine(1, "*** TRAILER LINE 1 ***" , 0), FP_ERROR_SET_HEADER_AND_TAILER );
		}		
		if( lLinesNum >= 2 )
		{
			RUN_OPOS_FUNC( SetTrailerLine(2, "*** TRAILER LINE 2 ***" , 0), FP_ERROR_SET_HEADER_AND_TAILER );
		}
	}
	
	return SUCCESS;
}
//------------------------------------------------------------------
// ResetPrinter()
//------------------------------------------------------------------
template<class T>
DWORD CFiscalPrint::ResetPrinter(T pInterface)
{
	long lState = pInterface->GetPrinterState();
	if( lState != FPTR_PS_MONITOR )
	{
		RUN_OPOS_FUNC( ResetPrinter(), FP_ERROR_RESET_PRINTER );
		// for version 1.6.0.0
		{
			lState = pInterface->GetPrinterState();
			if( lState == FPTR_PS_NONFISCAL )
			{
				RUN_OPOS_FUNC( EndNonFiscal(), FP_ERROR_END_NONFISCAL );
			}
			else if( lState != FPTR_PS_MONITOR )
			{
				RUN_OPOS_FUNC( PrintRecVoid("Cancel Receipt"), FP_ERROR_RESET_PRINTER );
				RUN_OPOS_FUNC( ResetPrinter(), FP_ERROR_RESET_PRINTER );
			}
		}		
	}
	return SUCCESS;
}
//------------------------------------------------------------------
// GetCOVersion()
//------------------------------------------------------------------
DWORD CFiscalPrint::GetCOVersion( CString& strCOVersion )
{
	if( !IsDrvLoaded() )
	{
		CPOSDevice::SetLastError( FP_ERROR_DRV_NOT_LOADED );
		return FP_ERROR_DRV_NOT_LOADED;
	};
	
	_bstr_t bstrCOVersion;

	if( IsOldInterfaceUsed() )	
	{
		bstrCOVersion = m_FiscalPrinterWNPtr->GetControlObjectDescription();			
	}
	else
	{
		bstrCOVersion = m_FiscalPrinterPtr->GetControlObjectDescription();
	}
	
	strCOVersion = (const char*)(bstrCOVersion);

	return SUCCESS;	
}
//------------------------------------------------------------------
// GetSOVersion()
//------------------------------------------------------------------
DWORD CFiscalPrint::GetSOVersion( CString& strSOVersion )
{
	if( !IsDrvLoaded() )
	{
		CPOSDevice::SetLastError( FP_ERROR_DRV_NOT_LOADED );
		return FP_ERROR_DRV_NOT_LOADED;
	};

	_bstr_t bstrSOVersion;

	if( IsOldInterfaceUsed() )	
	{	
		bstrSOVersion = m_FiscalPrinterWNPtr->GetServiceObjectDescription();
	}
	else
	{	
		bstrSOVersion = m_FiscalPrinterPtr->GetServiceObjectDescription();
	}
	
	strSOVersion = (const char*)(bstrSOVersion);

	return SUCCESS;
}
//------------------------------------------------------------------
// GetErrorString()
//------------------------------------------------------------------
_bstr_t CFiscalPrint::GetErrorString()
{	
	if( IsOldInterfaceUsed() )  return m_FiscalPrinterWNPtr->GetErrorString();
	else                        return m_FiscalPrinterPtr->GetErrorString();
}
//------------------------------------------------------------------
// GetCurrentTotal
//------------------------------------------------------------------
void CFiscalPrint::GetCurrentTotal( OUT double& dTotal)
{
	DWORD dwError;
	if( (dwError = CheckReady()) != SUCCESS )	
		return;

	long lArg = 0;

	dTotal = 0;
	
	TCHAR buf[100];	
	BSTR bstrTotal = ::SysAllocString(NULL);
	if( IsOldInterfaceUsed() )
		dwError = m_FiscalPrinterWNPtr->GetData(FPTR_GD_CURRENT_TOTAL,lArg,&bstrTotal);
	else
		dwError = m_FiscalPrinterPtr->GetData(FPTR_GD_CURRENT_TOTAL,&lArg,&bstrTotal);

	if( dwError == OPOS_SUCCESS )
	{
		WideCharToMultiByte(CP_ACP, 0,bstrTotal,-1,buf,sizeof(buf),NULL,NULL);
		dTotal = _tstof( buf );
	}		
	::SysFreeString(bstrTotal);
}