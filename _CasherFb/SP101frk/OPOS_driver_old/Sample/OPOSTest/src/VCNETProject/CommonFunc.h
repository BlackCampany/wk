#pragma once

void ErrMessage();
UINT GetErrorResourceID( DWORD dwErrorCode );
void GetDevicesNames(OUT CArray<CString,CString>& arrDevicesNames);
BOOL GetSPFR101COMPort(OUT CString& sComPort);
void SetSPFR101COMPort(IN const CString&  lpszComPort);
BOOL GetLogFileKept( OUT BOOL& bKeep );
void SetLogFileKept( IN BOOL bKeep );

// general functions of the test program
DWORD PrintNonfiscal     (CFiscalPrint& FR);
DWORD PrintSaleReceipt   (CFiscalPrint& FR);
DWORD PrintFefundReceipt (CFiscalPrint& FR);
DWORD PrintCashIn        (CFiscalPrint& FR);
DWORD PrintCashOut       (CFiscalPrint& FR);
DWORD PrintCancelReceipt (CFiscalPrint& FR);
DWORD PrintXReport       (CFiscalPrint& FR);
DWORD PrintZReport       (CFiscalPrint& FR);
DWORD DirectIO           (CFiscalPrint& FR);
DWORD SetDate            (CFiscalPrint& FR);
DWORD SetHeaderAndTrailer(CFiscalPrint& FR);



