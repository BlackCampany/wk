#include "StdAfx.h"
#include "POSDevice.h"
#include "cashdrawer.h"

CCashDrawer::CCashDrawer(void) : CPOSDevice( DR )
{
	//m_CashDrawerPtr    = NULL;
	//m_CashDrawerOldPtr = NULL;	
	m_sProgID.Format("Used ProgID: %s", "OPOS.CashDrawer" );
}

CCashDrawer::~CCashDrawer(void)
{
	Close();
}

//------------------------------------------------------------------
// Close()
//------------------------------------------------------------------
void CCashDrawer::Close()
{
	//CPOSDevice::Close( m_CashDrawerPtr, m_CashDrawerOldPtr );	
	if( IsDeviceOpen() )
		CloseDevice();

	Release();
}
//------------------------------------------------------------------
// AttachWorkInterface()
//------------------------------------------------------------------
void CCashDrawer::AttachWorkInterface()
{	
	if( m_CashDrawer19Ptr != NULL ) { m_CashDrawerPtr.Attach(m_CashDrawer19Ptr); return; };
	if( m_CashDrawer18Ptr != NULL ) { m_CashDrawerPtr.Attach(m_CashDrawer18Ptr); return; };	
	if( m_CashDrawer15Ptr != NULL ) { m_CashDrawerPtr.Attach(m_CashDrawer15Ptr); return; };
	if( m_CashDrawer14Ptr != NULL ) { m_CashDrawerPtr.Attach(m_CashDrawer14Ptr); return; };
}
//------------------------------------------------------------------
// Release()
//------------------------------------------------------------------
void CCashDrawer::Release()
{
	if( m_CashDrawerPtr   != NULL ) m_CashDrawerPtr.Detach();
	if( m_CashDrawer19Ptr != NULL ) m_CashDrawer19Ptr.Release();
	if( m_CashDrawer18Ptr != NULL ) m_CashDrawer18Ptr.Release();
	if( m_CashDrawer15Ptr != NULL ) m_CashDrawer15Ptr.Release();
	if( m_CashDrawer14Ptr != NULL ) m_CashDrawer14Ptr.Release();	
	if( m_CashDrawerWNPtr != NULL ) m_CashDrawerWNPtr.Release();
}
//------------------------------------------------------------------
// Init()
//------------------------------------------------------------------
BOOL CCashDrawer::Init()
{	
	//return CPOSDevice::Init( m_CashDrawerPtr, m_CashDrawerOldPtr, "OPOS.CashDrawer" );	

	HRESULT hres;

	try
	{	
		hres = m_CashDrawerPtr.CreateInstance("OPOS.CashDrawer");
		if( hres != S_OK && hres != E_NOINTERFACE) throw _com_error(hres);
		if( hres == E_NOINTERFACE ) hres = m_CashDrawer18Ptr.CreateInstance("OPOS.CashDrawer");		
		if( hres == E_NOINTERFACE ) hres = m_CashDrawer15Ptr.CreateInstance("OPOS.CashDrawer");
		if( hres == E_NOINTERFACE ) hres = m_CashDrawer14Ptr.CreateInstance("OPOS.CashDrawer");		
		if( hres == E_NOINTERFACE ) 
		{
			hres = m_CashDrawerWNPtr.CreateInstance("OPOS.CashDrawer");
			if( hres == S_OK ) m_bWNInterfaceUsed = TRUE;
		}
		if( hres == E_NOINTERFACE ) 
		{
			throw _com_error(hres);
			return FALSE;
		}
		else
		{
			AttachWorkInterface();
		}
	}
	catch(_com_error &ex)
	{	
		CString sExt;
		if( ex.Error() != CO_E_CLASSSTRING )		
		{
			sExt.Format("%s.\n%s", m_sProgID, ex.ErrorMessage() );
			CPOSDevice::SetLastError( ERROR_CO_CLASSSTRING, sExt );					
		}
		else
		{			
			sExt.Format( "%s.\n%s.\nIt is possible, Common Control Object is not installed.", m_sProgID, ex.ErrorMessage() );
			CPOSDevice::SetLastError( ERROR_CO_GENERAL, sExt );	
		}
		return FALSE;
	}
	catch (...) 
	{
		CPOSDevice::SetLastError( ERROR_UNKNOWN );
		return FALSE;
	}

	return TRUE;
}
//------------------------------------------------------------------
// CloseDevice()
//------------------------------------------------------------------
void CCashDrawer::CloseDevice()
{
	if( IsOldInterfaceUsed() )    return CPOSDevice::CloseDevice( m_CashDrawerWNPtr );
	else                          return CPOSDevice::CloseDevice( m_CashDrawerPtr    );	
}
//------------------------------------------------------------------
// CheckReady()
//------------------------------------------------------------------
DWORD CCashDrawer::CheckReady()
{
	if( !IsDrvLoaded() )
	{
		CPOSDevice::SetLastError( CD_ERROR_DRV_NOT_LOADED );
		return CD_ERROR_DRV_NOT_LOADED;
	};

	if( !IsDeviceOpen() )
	{
		CPOSDevice::SetLastError( CD_ERROR_NOT_OPEN );
		return CD_ERROR_NOT_OPEN;
	};

	return SUCCESS;
}
//------------------------------------------------------------------
// IsDrvLoaded()
//------------------------------------------------------------------
BOOL CCashDrawer::IsDrvLoaded()
{
	if( IsOldInterfaceUsed() )
	{
		return m_CashDrawerWNPtr == NULL ? FALSE : TRUE;
	}
	else
	{
		return m_CashDrawerPtr == NULL ? FALSE : TRUE;
	}
}
//------------------------------------------------------------------
// IsDeviceOpen()
//------------------------------------------------------------------
BOOL CCashDrawer::IsDeviceOpen()
{	
	if( !IsDrvLoaded() )
	{	
		return FALSE;
	}

	if( IsOldInterfaceUsed() )
	{
		if( m_CashDrawerWNPtr->GetState() != OPOS_S_CLOSED ) 		
			return TRUE;		
		else
			return FALSE;
	}
	else
	{
		if( m_CashDrawerPtr->GetState() != OPOS_S_CLOSED ) 		
			return TRUE;		
		else
			return FALSE;        
	}
}
//------------------------------------------------------------------
// OpenDrawer()
//------------------------------------------------------------------
DWORD CCashDrawer::OpenDrawer()
{
	if( IsOldInterfaceUsed() ) return OpenDrawer( m_CashDrawerWNPtr);
	else                       return OpenDrawer( m_CashDrawerPtr );
}
//------------------------------------------------------------------
template<class T> 
DWORD CCashDrawer::OpenDrawer( T pInterface )
{
	DWORD dwError;
	if( (dwError = CheckReady()) != SUCCESS )	
		return dwError;

	if( pInterface->GetDrawerOpened()) 
	{		
	}
	else 
	{	
		RUN_OPOS_FUNC( OpenDrawer(), DR_ERROR_PRINT_TEXT );
		//Sleep(500);		
		//RUN_OPOS_FUNC( WaitForDrawerClose(3000,2000,500,1000), DR_ERROR_PRINT_TEXT );
	}	
	return SUCCESS;
}
//------------------------------------------------------------------
// SetLastError()
//------------------------------------------------------------------
void CCashDrawer::SetLastError( DWORD dwErrorCode, long dwExtErrorCode )
{
	if( IsOldInterfaceUsed() )    return CPOSDevice::SetLastError( m_CashDrawerWNPtr, dwErrorCode, dwExtErrorCode );
	else                          return CPOSDevice::SetLastError( m_CashDrawerPtr,    dwErrorCode, dwExtErrorCode );
}
//------------------------------------------------------------------
// OPenDevice()
//------------------------------------------------------------------
DWORD CCashDrawer::OpenDevice(LPCSTR lpszDeviceName)
{
	if( IsOldInterfaceUsed() )    return CPOSDevice::OpenDevice( m_CashDrawerWNPtr, lpszDeviceName );
	else                          return CPOSDevice::OpenDevice( m_CashDrawerPtr,    lpszDeviceName );
}
//------------------------------------------------------------------

