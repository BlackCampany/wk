// OPOSTest.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "OPOSTest.h"
#include "OPOSTestDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// COPOSTestApp

BEGIN_MESSAGE_MAP(COPOSTestApp, CWinApp)
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)	
	ON_THREAD_MESSAGE( TM_ADD_STRING_TO_LOG,   OnAddStringToLog       )     
	ON_THREAD_MESSAGE( TM_COMPLETED_FUNCTION,  OnSetCompletedFunction )
	ON_THREAD_MESSAGE( TM_ADD_ITEM_TO_LOG,     OnAddItemToLog         )     
	ON_THREAD_MESSAGE( TM_STARTED_FUNCTION,    OnSetStartedFunction   )	
	ON_THREAD_MESSAGE( TM_DISPLAY_ERROR,       OnDisplayError         )
	ON_THREAD_MESSAGE( TM_SET_WAIT_CURSOR,     OnSetWaitCursor        )
	ON_THREAD_MESSAGE( TM_SET_STOP_CURSOR,     OnSetStopCursor        )
	ON_THREAD_MESSAGE( TM_COMPLETED_FR_PROCESS,OnSetCompletedFRProcess)
	ON_THREAD_MESSAGE( TM_COMPLETED_LD_PROCESS,OnSetCompletedLDProcess)
	ON_THREAD_MESSAGE( TM_COMPLETED_CD_PROCESS,OnSetCompletedCDProcess)

//TM_OPEN_PRINTER_PORT 
//TM_CLOSE_PRINTER_PORT

END_MESSAGE_MAP()


// COPOSTestApp construction

COPOSTestApp::COPOSTestApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


// The one and only COPOSTestApp object

COPOSTestApp theApp;
CFiscalPrint g_FR;
CLineDisplay g_LD;
CCashDrawer  g_CD;

CString      g_sFunction;
UINT         g_nCurrentButtonID = 0;
CString      g_sFRDeviceName;


// COPOSTestApp initialization

BOOL COPOSTestApp::InitInstance()
{
	// InitCommonControls() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	HRESULT hr = ::CoInitializeEx( NULL, COINIT_MULTITHREADED);

	InitCommonControls();

	

	CWinApp::InitInstance();

	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	// Change the registry key under which our settings are stored
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization
	SetRegistryKey(_T("Local AppWizard-Generated Applications"));

		
	if( !g_FR.Init() )
	{
		AfxMessageBox( g_FR.GetLastErrorText(), MB_OK|MB_ICONERROR );
		return FALSE;
	};
	
	if( !g_LD.Init() )
	{
		AfxMessageBox( g_LD.GetLastErrorText(), MB_OK|MB_ICONERROR );
		return FALSE;
	};

	if( !g_CD.Init() )
	{
		AfxMessageBox( g_CD.GetLastErrorText(), MB_OK|MB_ICONERROR );
		return FALSE;
	};

	COPOSTestDlg dlg;
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}	

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}

int COPOSTestApp::ExitInstance()
{
	g_FR.Close();
	g_CD.Close();
	g_LD.Close();
	::CoUninitialize();
	return CWinApp::ExitInstance();
}

void COPOSTestApp::OnSetCompletedFunction( WPARAM wParam, LPARAM lParam )
{
    SendMessage( m_pMainWnd->m_hWnd, TM_COMPLETED_FUNCTION, wParam, lParam );
}

void COPOSTestApp::OnAddStringToLog( WPARAM wParam, LPARAM lParam )
{
	SendMessage( m_pMainWnd->m_hWnd, TM_ADD_STRING_TO_LOG, wParam, lParam );
}

void COPOSTestApp::OnAddItemToLog( WPARAM wParam, LPARAM lParam )
{
	SendMessage( m_pMainWnd->m_hWnd, TM_ADD_ITEM_TO_LOG, wParam, lParam );

}
void COPOSTestApp::OnSetStartedFunction( WPARAM wParam, LPARAM lParam )
{
	SendMessage( m_pMainWnd->m_hWnd, TM_STARTED_FUNCTION, wParam, lParam );
}
void COPOSTestApp::OnSetCompletedFRProcess( WPARAM wParam, LPARAM lParam )
{
	SendMessage( m_pMainWnd->m_hWnd, TM_COMPLETED_FR_PROCESS, wParam, lParam );
}
void COPOSTestApp::OnSetCompletedLDProcess( WPARAM wParam, LPARAM lParam )
{
	SendMessage( m_pMainWnd->m_hWnd, TM_COMPLETED_LD_PROCESS, wParam, lParam );
}
void COPOSTestApp::OnSetCompletedCDProcess( WPARAM wParam, LPARAM lParam )
{
	SendMessage( m_pMainWnd->m_hWnd, TM_COMPLETED_CD_PROCESS, wParam, lParam );
}
void COPOSTestApp::OnDisplayError( WPARAM wParam, LPARAM lParam )
{
	SendMessage( m_pMainWnd->m_hWnd, TM_DISPLAY_ERROR, wParam, lParam );
}
void COPOSTestApp::OnSetWaitCursor( WPARAM wParam, LPARAM lParam )
{
	SendMessage( m_pMainWnd->m_hWnd, TM_SET_WAIT_CURSOR, wParam, lParam );
}
void COPOSTestApp::OnSetStopCursor( WPARAM wParam, LPARAM lParam )
{
	SendMessage( m_pMainWnd->m_hWnd, TM_SET_STOP_CURSOR, wParam, lParam );
}