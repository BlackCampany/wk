// OPOSTest.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols


// COPOSTestApp:
// See OPOSTest.cpp for the implementation of this class
//

class COPOSTestApp : public CWinApp
{	
public:
	COPOSTestApp();

protected:	
	afx_msg void OnAddStringToLog       ( WPARAM wParam, LPARAM lParam );
	afx_msg void OnSetCompletedFunction ( WPARAM wParam, LPARAM lParam );
	afx_msg void OnAddItemToLog         ( WPARAM wParam, LPARAM lParam );
	afx_msg void OnSetStartedFunction   ( WPARAM wParam, LPARAM lParam );
	afx_msg void OnSetCompletedFRProcess( WPARAM wParam, LPARAM lParam );
	afx_msg void OnSetCompletedLDProcess( WPARAM wParam, LPARAM lParam );
	afx_msg void OnSetCompletedCDProcess( WPARAM wParam, LPARAM lParam );
	afx_msg void OnDisplayError         ( WPARAM wParam, LPARAM lParam );
	afx_msg void OnSetWaitCursor        ( WPARAM wParam, LPARAM lParam );
	afx_msg void OnSetStopCursor        ( WPARAM wParam, LPARAM lParam );

	// Overrides
	public:
	virtual BOOL InitInstance();
	virtual int  ExitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern COPOSTestApp theApp;