
#include "stdafx.h"
#include "FiscalPrint.h"
#include "CommonFunc.h"

//--------------------------------------------------------------------------------
// Display message about system error
//--------------------------------------------------------------------------------
void ErrMessage()
{
	LPVOID lpMsgBuf;

	FormatMessage( 
		FORMAT_MESSAGE_ALLOCATE_BUFFER | 
		FORMAT_MESSAGE_FROM_SYSTEM | 
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		GetLastError(),
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
		(LPTSTR) &lpMsgBuf,
		0,
		NULL 
		);
	// Process any inserts in lpMsgBuf.
	// ...
	// Display the string.
	MessageBox( AfxGetApp()->GetMainWnd()->GetSafeHwnd(), (LPCSTR)lpMsgBuf, "Error", MB_OK | MB_ICONINFORMATION );

	LocalFree( lpMsgBuf );
}
//--------------------------------------------------------------------------------
// Get Error Resource ID by error number
//--------------------------------------------------------------------------------
UINT GetErrorResourceID( DWORD dwErrorCode )
{
	UINT unResourceID;

	switch( dwErrorCode ) 
	{		
		//common
	case ERROR_CO_CLASSSTRING:     unResourceID = IDS_ERROR_CO_CLASSSTRING;      break;
	case ERROR_CO_GENERAL:         unResourceID = IDS_ERROR_CO_GENERAL;          break;	

		//fiscal printer
	case FP_ERROR_DRV_NOT_LOADED:  unResourceID = IDS_FP_ERROR_DRV_NOT_LOADED;   break;
	case FP_ERROR_OPENING:         unResourceID = IDS_FP_ERROR_OPENING;          break;
	case FP_ERROR_NOT_OPEN:        unResourceID = IDS_FP_ERROR_NOT_OPEN;         break;
	case FP_ERROR_BEGIN_NONFISCAL: unResourceID = IDS_FP_ERROR_BEGIN_NONFISCAL;  break;
	case FP_ERROR_PRINT_NORMAL:    unResourceID = IDS_FP_ERROR_PRINT_NORMAL;     break;
	case FP_ERROR_END_NONFISCAL:   unResourceID = IDS_FP_ERROR_END_NONFISCAL;    break;
	case FP_ERROR_SALE_RECEIPT:    unResourceID = IDS_FP_ERROR_SALE_RECEIPT;     break;	

		//customer display
	case CD_ERROR_DRV_NOT_LOADED:  unResourceID = IDS_CD_ERROR_DRV_NOT_LOADED;   break;
	case CD_ERROR_OPENING:         unResourceID = IDS_CD_ERROR_OPENING;          break;
	case CD_ERROR_NOT_OPEN:        unResourceID = IDS_CD_ERROR_NOT_OPEN;         break;	
	default:                       unResourceID = IDS_ERROR_UNKNOWN;             break;
	}
	return unResourceID;
}
//--------------------------------------------------------------------------------
// get list of devices, for which SO are registered
//--------------------------------------------------------------------------------
void GetDevicesNames(OUT CArray<CString,CString>& arrDevicesNames)
{
	HKEY     hKey = NULL;
	char     szName[512];
	BYTE     szData[512];
	DWORD    dwIndex = 0;
	ZeroMemory( szName,  sizeof(szName) );	
	ZeroMemory( szData,  sizeof(szData) );	
	DWORD dwNameLen  = sizeof(szName);	
	DWORD dwDataLen  = sizeof(szData);		
	CArray<CString>  arrValue;
	BOOL   fAlreadyExist = FALSE;
	
	arrDevicesNames.RemoveAll();
	arrValue.RemoveAll();

	RegOpenKeyEx(HKEY_LOCAL_MACHINE, "SOFTWARE\\OLEforRetail\\ServiceOPOS\\FiscalPrinter", 0, KEY_READ, &hKey);	
	
	while( RegEnumValue(hKey, dwIndex, szName, &dwNameLen, NULL, NULL, szData, &dwDataLen) == ERROR_SUCCESS)
	{
		arrValue.Add( (char*)szData );
		arrDevicesNames.Add( szName );
		++dwIndex;
	}
	
	dwIndex = 0;
	ZeroMemory( szName,  sizeof(szName) );		
	dwNameLen  = sizeof(szName);		
	FILETIME ftLastWriteTime;	
	while( RegEnumKeyEx(hKey, dwIndex, szName, &dwNameLen, NULL, NULL, NULL, &ftLastWriteTime) == ERROR_SUCCESS)
	{
		for( int i = 0; i < arrValue.GetSize(); i++ )
		{
            if( arrValue[i].Compare( szName ) == 0 )
			{
				fAlreadyExist = TRUE;
			}
		}

		if( !fAlreadyExist )
		{
			arrDevicesNames.Add( szName );
		}
		++dwIndex;
	}

	RegCloseKey(hKey);
};
//--------------------------------------------------------------------------------
// retrieve COM-port number from Registry
//--------------------------------------------------------------------------------
BOOL GetSPFR101COMPort(OUT CString& sComPort)
{
	HKEY     hKey = NULL;	
	char     szData[512];	
	ZeroMemory( szData,  sizeof(szData) );		
	DWORD     dwDataLen  = sizeof(szData);

	CRegKey key;	
	if( key.Open( HKEY_LOCAL_MACHINE, "SOFTWARE\\OLEforRetail\\ServiceOPOS\\FiscalPrinter\\SPFR101.COM" ) != ERROR_SUCCESS 
		||
		key.QueryStringValue( "Port", szData, &dwDataLen ) != ERROR_SUCCESS )
	{
		return FALSE;
	};

	key.Close();
	sComPort = szData;
	return TRUE;
}
//--------------------------------------------------------------------------------
// set COM-port value in Registry
//--------------------------------------------------------------------------------
void SetSPFR101COMPort(IN const CString&  lpszComPort)
{
	HKEY     hKey = NULL;	
	char     szData[512];	
	ZeroMemory( szData,  sizeof(szData) );		
	DWORD     dwDataLen  = sizeof(szData);

	CRegKey key;	
	if( key.Open( HKEY_LOCAL_MACHINE, "SOFTWARE\\OLEforRetail\\ServiceOPOS\\FiscalPrinter\\SPFR101.COM" ) != ERROR_SUCCESS 
		||
		key.SetStringValue( "Port", lpszComPort ) != ERROR_SUCCESS )
	{
		return;
	};
	key.Close();
}
//--------------------------------------------------------------------------------
// GetLogFileKept
//--------------------------------------------------------------------------------
BOOL GetLogFileKept( OUT BOOL& bKeep )
{
	HKEY     hKey = NULL;	
	char     szData[512];	
	ZeroMemory( szData,  sizeof(szData) );		
	DWORD     dwDataLen  = sizeof(szData);

	CRegKey key;	
	if( key.Open( HKEY_LOCAL_MACHINE, "SOFTWARE\\OLEforRetail\\ServiceOPOS\\FiscalPrinter\\SPFR101.COM\\Trace" ) != ERROR_SUCCESS 
		||
		key.QueryStringValue( "Level", szData, &dwDataLen ) != ERROR_SUCCESS )
	{
		return FALSE;
	};
	key.Close();
	if( atol(szData) > 0 ) bKeep = TRUE;
	else                   bKeep = FALSE;
	return TRUE;
}
//--------------------------------------------------------------------------------
// SetLogFileKept
//--------------------------------------------------------------------------------
void SetLogFileKept( BOOL bKeep )
{
	HKEY     hKey = NULL;	
	char     szData[512];	
	ZeroMemory( szData,  sizeof(szData) );		
	DWORD     dwDataLen  = sizeof(szData);

	CRegKey key;	
	if( key.Open( HKEY_LOCAL_MACHINE, "SOFTWARE\\OLEforRetail\\ServiceOPOS\\FiscalPrinter\\SPFR101.COM\\Trace" ) != ERROR_SUCCESS 
		||
		key.SetStringValue( "Level", (bKeep)?"1":"0" ) != ERROR_SUCCESS )
	{
		return;
	};
	key.Close();
}
//--------------------------------------------------------------------------------
// documents printing
//--------------------------------------------------------------------------------
DWORD PrintNonfiscal(CFiscalPrint& FR)
{		
	return FR.PrintNonfiscal();	
}

DWORD PrintSaleReceipt(CFiscalPrint& FR)
{	
	return FR.PrintSaleReceipt();
}

DWORD PrintFefundReceipt(CFiscalPrint& FR)
{
	return FR.PrintRefundReceipt();	
}

DWORD PrintCashIn(CFiscalPrint& FR)
{
	return FR.PrintCashIn();
}

DWORD PrintCashOut(CFiscalPrint& FR)
{
	return FR.PrintCashOut();
}

DWORD PrintCancelReceipt(CFiscalPrint& FR)
{
	return FR.CancelReceipt();
}

DWORD PrintXReport(CFiscalPrint& FR)
{
	return FR.PrintXReport();
}

DWORD PrintZReport(CFiscalPrint& FR)
{
	return FR.PrintZReport();
}

DWORD DirectIO(CFiscalPrint& FR)
{
	return FR.DirectIO();
}

DWORD SetDate(CFiscalPrint& FR)
{
	return FR.SetDate();
}

DWORD SetHeaderAndTrailer(CFiscalPrint& FR)
{
	return FR.SetHeaderAndTrailer();
}


