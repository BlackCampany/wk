#pragma once
#include "ErrorDef.h"
//#import  "ocx/OPOSFiscalPrinter.ocx" no_namespace high_property_prefixes("Get","Set","PutRef")

#include "oposfiscalprinter.tlh"

#import  "ocx/FPrinter.ocx" no_namespace high_property_prefixes("Get","Set","PutRef")

class CPOSDevice;

class CFiscalPrint : public CPOSDevice
{
	//OposFiscalPrinter_1_9_Lib::IOPOSFiscalPrinterPtr    *m_FiscalPrinterPtr;
	IOPOSFiscalPrinterPtr   m_FiscalPrinter19Ptr;	
	IOPOSFiscalPrinter18Ptr m_FiscalPrinter18Ptr;
	IOPOSFiscalPrinter16Ptr m_FiscalPrinter16Ptr;
	IOPOSFiscalPrinter15Ptr m_FiscalPrinter15Ptr;
	IOPOSFiscalPrinter14Ptr m_FiscalPrinter14Ptr;	

	IOPOSFiscalPrinterPtr   m_FiscalPrinterPtr;
	//_DOPOS_FiscalPrinterPtr  *m_FiscalPrinterOldPtr;
	_DOPOS_FiscalPrinterPtr m_FiscalPrinterWNPtr;
	COPOSFiscalPrinter      m_FiscalPrinter;

	BOOL                     m_bSPFR101;	

public:
	CFiscalPrint(void);
	~CFiscalPrint(void);

private:

	DWORD GetError_DrvNotLoaded() { return FP_ERROR_DRV_NOT_LOADED; };
	DWORD GetError_Opening()      { return FP_ERROR_OPENING;        };
	
	void SetLastError( DWORD dwErrorCode, long lExtErrorCode );
	//------------------------------------------------------------------
	// Setting a last error number. You can get text description of the last error 
	// after any function calling.
	//      IN:     dwErrorCode      - an error code
	//              1) lpszExtInfo   - extended info about error
	//              2) lExtErrorCode - Error code, returned by OPOS-driver
	//      return: void
	//------------------------------------------------------------------	
	
	virtual _bstr_t GetErrorString();
	//------------------------------------------------------------------
	// Get error description
	//       return: error description string
	//------------------------------------------------------------------

	BOOL IsSPFR101();
	//------------------------------------------------------------------
	// Is current opened printer SPFR101
	//       return: TRUE or FALSE
	//------------------------------------------------------------------

	void GetCurrentTotal(OUT double& dTotal );
	//------------------------------------------------------------------
	// Get total receipt amount
	//       OUT:    dTotal - total receipt amount
	//       return: void
	//------------------------------------------------------------------

	void AttachWorkInterface();
	//------------------------------------------------------------------
	// Get current interface pointer	
	//       return: current interface pointer
	//------------------------------------------------------------------
	
	void Release();
	//------------------------------------------------------------------
	// Release all interface pointer	
	//       return: void
	//------------------------------------------------------------------

public:	

	BOOL  Init();
	//------------------------------------------------------------------
	// Init FR. Attaching OPOS Control Object.
	//      return: TRUE if success, else FALSE	
	//------------------------------------------------------------------
	
	void Close();
	//------------------------------------------------------------------
	// Close the device, delete m_FiscalPrinterPtr
	//      return: void
	//------------------------------------------------------------------
	
	BOOL  IsDrvLoaded();	
	//------------------------------------------------------------------
	// Checks, if driver is loaded. If Init() is success, driver is 
	// loaded	
	//      return: TRUE if success, else FALSE	
	//------------------------------------------------------------------	
	
	DWORD OpenDevice(LPCSTR lpszDeviceName);
	//------------------------------------------------------------------
	// Open device
	//      return: error code or  FP_SUCCESS if success
	//------------------------------------------------------------------
	
	BOOL IsDeviceOpen();
	//------------------------------------------------------------------
	// Checks, if FR is opened
	//      return: TRUE, if opened, else FALSE
	//------------------------------------------------------------------

	DWORD CheckReady();
	//------------------------------------------------------------------
	// checks, if device is ready, that's if driver is loaded and port 
	// is opened
	//      return: TRUE, if ready, else FALSE	
	//------------------------------------------------------------------	
	
	void  CloseDevice();
	//------------------------------------------------------------------
	// Close FR
	//      return: void
	//------------------------------------------------------------------
	
	DWORD PrintNonfiscal();
	//------------------------------------------------------------------
	// Prints a non-fiscal document
	//      return: FP_SUCCESS if success, else error code
	//------------------------------------------------------------------

	DWORD PrintSaleReceipt();
	//------------------------------------------------------------------
	// Prints sale receipt
	//      return: FP_SUCCESS if success, else error code
	//------------------------------------------------------------------

	DWORD PrintRefundReceipt();
	//------------------------------------------------------------------
	// Prints refund receipt
	//      return: FP_SUCCESS if success, else error code
	//------------------------------------------------------------------

	DWORD PrintCashIn();
	//------------------------------------------------------------------
	// Prints cash-in document
	//      return: FP_SUCCESS if success, else error code
	//------------------------------------------------------------------

	DWORD PrintCashOut();
	//------------------------------------------------------------------
	// Prints cash-out document
	//      return: FP_SUCCESS if success, else error code
	//------------------------------------------------------------------

	DWORD CancelReceipt();
	//------------------------------------------------------------------
	// Cancels current receipt
	//      return: FP_SUCCESS if success, else error code
	//------------------------------------------------------------------
	
	DWORD PrintXReport();
	//------------------------------------------------------------------
	// Prints X-report
	//      return: FP_SUCCESS if success, else error code
	//------------------------------------------------------------------

	DWORD PrintZReport();
	//------------------------------------------------------------------
	// Prints Z-report
	//      return: FP_SUCCESS if success, else error code
	//------------------------------------------------------------------

	DWORD DirectIO();
	//------------------------------------------------------------------
	// Demonstrates a direct access
	//      return: FP_SUCCESS if success, else error code
	//------------------------------------------------------------------

	DWORD SetDate();
	//------------------------------------------------------------------
	// Set date
	//      return: FP_SUCCESS if success, else error code
	//------------------------------------------------------------------

	DWORD SetHeaderAndTrailer();
	//------------------------------------------------------------------
	// Sets header and trailer 
	//      return: FP_SUCCESS if success, else error code
	//------------------------------------------------------------------

	DWORD GetCOVersion( CString& strCOVersion );
	//------------------------------------------------------------------
	// Gets Control Object version
	//      OUT:    Control Object version
	//      return: FP_SUCCESS if success, else error code
	//------------------------------------------------------------------

	DWORD GetSOVersion( CString& strSOVersion );
	//------------------------------------------------------------------
	// Gets Service Object version
	//      OUT:    Service Object version
	//      return: FP_SUCCESS if success, else error code
	//------------------------------------------------------------------

	DWORD ResetPrinter();
	//------------------------------------------------------------------
	// Resets printer to default state. 
	//      return: void
	//------------------------------------------------------------------


private:
    // template functions for OPOS	
	template<class T> DWORD PrintNonfiscal     ( T pInterface );
	template<class T> DWORD PrintSaleReceipt   ( T pInterface );
	template<class T> DWORD PrintRefundReceipt ( T pInterface );
	template<class T> DWORD PrintCashIn        ( T pInterface );
	template<class T> DWORD PrintCashOut       ( T pInterface );
	template<class T> DWORD CancelReceipt      ( T pInterface );
	template<class T> DWORD PrintXReport       ( T pInterface );
	template<class T> DWORD PrintZReport       ( T pInterface );
	template<class T> DWORD DirectIO           ( T pInterface );
	template<class T> DWORD SetDate            ( T pInterface );
	template<class T> DWORD SetHeaderAndTrailer( T pInterface );
	template<class T> DWORD ResetPrinter       ( T pInterface );

};
