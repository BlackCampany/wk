//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by OPOSTest.rc
//
#define IDD_OPOSTEST_DIALOG             102
#define IDR_MAINFRAME                   128
#define IDS_ERROR_CO_GENERAL            130
#define IDS_ERROR_CO_CLASSSTRING        131
#define IDS_ERROR_UNKNOWN               132
#define IDS_FP_ERROR_DRV_NOT_LOADED     133
#define IDS_FP_ERROR_OPENING            134
#define IDS_FP_ERROR_NOT_OPEN           135
#define IDS_FP_ERROR_BEGIN_NONFISCAL    136
#define IDS_FP_ERROR_PRINT_NORMAL       137
#define IDS_FP_ERROR_END_NONFISCAL      138
#define IDS_FP_ERROR_SALE_RECEIPT       139
#define IDS_CD_ERROR_DRV_NOT_LOADED     143
#define IDS_CD_ERROR_NOT_OPEN           145
#define IDS_CD_ERROR_OPENING            146
#define IDI_SUCCESS_ICON                147
#define IDI_EMPTY_ICON                  148
#define IDI_ERROR_ICON                  149
#define IDC_CMB_COM_PORT                1000
#define IDC_BTN_NONFISCAL               1004
#define IDC_BTN_SALE_RECEIPT            1005
#define IDC_BTN_FEFUND_RECEIPT          1006
#define IDC_BTN_CASH_IN                 1007
#define IDC_BTN_CASH_OUT                1008
#define IDC_BTN_CANCEL_RECEIPT          1009
#define IDC_BTN_X_REPORT                1010
#define IDC_BTN_Z_REPORT                1011
#define IDC_BTN_SET_DATE                1012
#define IDC_BTN_SET_HEAD_AND_TAIL       1013
#define IDC_BTN_DIRECTIO                1014
#define IDC_BTN_DISPLAY_TEXT            1015
#define IDC_BTN_OPEN_CASH_DRAWER        1016
#define IDC_LIST_LOG                    1017
#define IDC_CMB_DEVICE                  1018
#define IDC_STC_CO_VERSION              1019
#define IDC_STC_SO_VERSION              1020
#define IDC_CHECK1                      1022
#define IDC_CHK_LOG                     1022

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        147
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1023
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
