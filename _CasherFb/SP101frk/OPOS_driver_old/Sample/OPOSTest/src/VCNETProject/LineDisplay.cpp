#include "StdAfx.h"
#include "LineDisplay.h"

CLineDisplay::CLineDisplay(void) : CPOSDevice( CD )
{
//	m_LineDisplayPtr    = NULL;
//	m_LineDisplayOldPtr = NULL;	
	m_sProgID.Format("Used ProgID: %s", "OPOS.LineDisplay" );
}

CLineDisplay::~CLineDisplay(void)
{
	Close();
}
//------------------------------------------------------------------
// Close()
//------------------------------------------------------------------
void CLineDisplay::Close()
{
	if( IsDeviceOpen() )
		CloseDevice();

	Release();
}
//------------------------------------------------------------------
// AttachWorkInterface()
//------------------------------------------------------------------
void CLineDisplay::AttachWorkInterface()
{	
	if( m_LineDisplay19Ptr != NULL ) { m_LineDisplayPtr.Attach(m_LineDisplay19Ptr); return; };
	if( m_LineDisplay18Ptr != NULL ) { m_LineDisplayPtr.Attach(m_LineDisplay18Ptr); return; };
	if( m_LineDisplay16Ptr != NULL ) { m_LineDisplayPtr.Attach(m_LineDisplay16Ptr); return; };
	if( m_LineDisplay15Ptr != NULL ) { m_LineDisplayPtr.Attach(m_LineDisplay15Ptr); return; };
	if( m_LineDisplay14Ptr != NULL ) { m_LineDisplayPtr.Attach(m_LineDisplay14Ptr); return; };
}
//------------------------------------------------------------------
// Release()
//------------------------------------------------------------------
void CLineDisplay::Release()
{	
	if( m_LineDisplayPtr  != NULL )   m_LineDisplayPtr.Detach();
	if( m_LineDisplay19Ptr  != NULL ) m_LineDisplay19Ptr .Release();
	if( m_LineDisplay18Ptr  != NULL ) m_LineDisplay18Ptr .Release();
	if( m_LineDisplay16Ptr  != NULL ) m_LineDisplay16Ptr .Release();
	if( m_LineDisplay15Ptr  != NULL ) m_LineDisplay15Ptr .Release();
	if( m_LineDisplay14Ptr  != NULL ) m_LineDisplay14Ptr .Release();	
	if( m_LineDisplayWNPtr  != NULL ) m_LineDisplayWNPtr.Release();
}
//------------------------------------------------------------------
// Init();
//------------------------------------------------------------------
BOOL CLineDisplay::Init()
{	
	HRESULT hres;

	try
	{	
		hres = m_LineDisplayPtr.CreateInstance("OPOS.LineDisplay");
		if( hres != S_OK && hres != E_NOINTERFACE) throw _com_error(hres);
		if( hres == E_NOINTERFACE ) hres = m_LineDisplay18Ptr.CreateInstance("OPOS.LineDisplay");
		if( hres == E_NOINTERFACE ) hres = m_LineDisplay16Ptr.CreateInstance("OPOS.LineDisplay");
		if( hres == E_NOINTERFACE ) hres = m_LineDisplay15Ptr.CreateInstance("OPOS.LineDisplay");
		if( hres == E_NOINTERFACE ) hres = m_LineDisplay14Ptr.CreateInstance("OPOS.LineDisplay");		
		if( hres == E_NOINTERFACE ) 
		{
			hres = m_LineDisplayWNPtr.CreateInstance("OPOS.LineDisplay");
			if( hres == S_OK ) m_bWNInterfaceUsed = TRUE;
		}
		if( hres != S_OK ) 
		{
			throw _com_error(hres);
			return FALSE;
		}
		else
		{			
			AttachWorkInterface();
		}
	}
	catch(_com_error &ex)
	{	
		CString sExt;
		if( ex.Error() != CO_E_CLASSSTRING )		
		{
			sExt.Format("%s.\n%s", m_sProgID, ex.ErrorMessage() );
			CPOSDevice::SetLastError( ERROR_CO_CLASSSTRING, sExt );					
		}
		else
		{			
			sExt.Format( "%s.\n%s.\nCannot init OPOS-driver. It is possible Common Control Object is not installed.", m_sProgID, ex.ErrorMessage() );
			CPOSDevice::SetLastError( ERROR_CO_GENERAL, sExt );	
		}
		return FALSE;
	}
	catch (...) 
	{
		CPOSDevice::SetLastError( ERROR_UNKNOWN );
		return FALSE;
	}

	return TRUE;
}
//-------------------------------------------------------------------------
// OpenDevice()
//-------------------------------------------------------------------------
BOOL  CLineDisplay::IsDrvLoaded()
{
	if( IsOldInterfaceUsed() )
	{
		return m_LineDisplayWNPtr == NULL ? FALSE : TRUE;
	}
	else
	{
		return m_LineDisplayPtr == NULL ? FALSE : TRUE;
	}
}
//-------------------------------------------------------------------------
// OpenDevice()
//-------------------------------------------------------------------------
DWORD CLineDisplay::OpenDevice(LPCSTR lpszDeviceName)
{
	if( IsOldInterfaceUsed() )    return CPOSDevice::OpenDevice( m_LineDisplayWNPtr, lpszDeviceName );
	else                          return CPOSDevice::OpenDevice( m_LineDisplayPtr,    lpszDeviceName );
}
//-------------------------------------------------------------------------
// CloseDevice()
//-------------------------------------------------------------------------
void CLineDisplay::CloseDevice()
{
	if( IsOldInterfaceUsed() )    return CPOSDevice::CloseDevice( m_LineDisplayWNPtr );
	else                          return CPOSDevice::CloseDevice( m_LineDisplayPtr    );	
}
//-------------------------------------------------------------------------
// SetLastError()
//-------------------------------------------------------------------------
void CLineDisplay::SetLastError( DWORD dwErrorCode, long dwExtErrorCode )
{
	if( IsOldInterfaceUsed() )    return CPOSDevice::SetLastError( m_LineDisplayWNPtr, dwErrorCode, dwExtErrorCode );
	else                          return CPOSDevice::SetLastError( m_LineDisplayPtr,   dwErrorCode, dwExtErrorCode );
}
//-------------------------------------------------------------------------
DWORD CLineDisplay::PrintText()
{
	if( IsOldInterfaceUsed() )    return PrintText( m_LineDisplayWNPtr );
	else                          return PrintText( m_LineDisplayPtr    );	
}
//------------------------------------------------------------------
// PrintText()
//------------------------------------------------------------------
template<class T> 
DWORD CLineDisplay::PrintText( T pInterface )
{
	DWORD dwError;
	
	if( (dwError = CheckReady()) != SUCCESS )	
		return dwError;	

	pInterface->SetMarqueeType( DISP_MT_INIT );		
	
	RUN_OPOS_FUNC( DisplayText(">234567890123456789<<ABCDE--------abcde>",0), CD_ERROR_PRINT_TEXT );
	Sleep(2000);
	RUN_OPOS_FUNC( CreateWindow(0,1,2,18,4,40), CD_ERROR_PRINT_TEXT );	
	RUN_OPOS_FUNC( DisplayText("------------------++++++++++++++++++",DISP_DT_NORMAL), CD_ERROR_PRINT_TEXT );
	RUN_OPOS_FUNC( DisplayTextAt(1,1,"***1234567890***",DISP_DT_NORMAL), CD_ERROR_PRINT_TEXT );
	RUN_OPOS_FUNC( DisplayTextAt(2,0,"wwwwwwwwwwwwwwwwwwWWWWWWWWWWWWWWWWWW",DISP_DT_NORMAL), CD_ERROR_PRINT_TEXT );
	RUN_OPOS_FUNC( DisplayTextAt(3,0,"asdfghjklzxcvbnms Test text",DISP_DT_NORMAL), CD_ERROR_PRINT_TEXT );	
	Sleep(2000);
	if( 
		pInterface->GetMarqueeType() != DISP_MT_NONE 
		|| 
		pInterface->GetInterCharacterWait() != 0 
		)
	{
		AddItemToLog( CLogItem( "Display is not in Immediate Mode" ) );
		return CD_ERROR_PRINT_TEXT;
	}
	RUN_OPOS_FUNC( ScrollText(DISP_ST_UP,2), CD_ERROR_PRINT_TEXT );
	Sleep(2000);
	RUN_OPOS_FUNC( ScrollText(DISP_ST_LEFT,17), CD_ERROR_PRINT_TEXT );
	Sleep(2000);
	RUN_OPOS_FUNC( ClearText(), CD_ERROR_PRINT_TEXT );	

	RUN_OPOS_FUNC( CreateWindow(0, 0, 2, 20, 2, 27), CD_ERROR_PRINT_TEXT );		

	long lNum = pInterface->GetDeviceWindows();

	RUN_OPOS_FUNC( DisplayText ("Testing", DISP_DT_NORMAL ), CD_ERROR_PRINT_TEXT );	
	Sleep(500);	
	if( 
		pInterface->GetMarqueeType() != DISP_MT_NONE 
		|| 
		pInterface->GetInterCharacterWait() != 0 
		)
	{
		AddItemToLog( CLogItem( "Display is not in Immediate Mode" ) );
		return CD_ERROR_PRINT_TEXT;
	}
	for( int i = 0; i < 8; i ++ )
	{
		RUN_OPOS_FUNC( ScrollText(DISP_ST_LEFT, 1), CD_ERROR_PRINT_TEXT ); Sleep(100);
	}	
	
	return SUCCESS;
}
//------------------------------------------------------------------
// CheckReady()
//------------------------------------------------------------------
DWORD CLineDisplay::CheckReady()
{
	if( !IsDrvLoaded() )
	{
		CPOSDevice::SetLastError( CD_ERROR_DRV_NOT_LOADED );
		return CD_ERROR_DRV_NOT_LOADED;
	};

	if( !IsDeviceOpen() )
	{
		CPOSDevice::SetLastError( CD_ERROR_NOT_OPEN );
		return CD_ERROR_NOT_OPEN;
	};

	return SUCCESS;
}
//------------------------------------------------------------------
// IsDeviceOpen()
//------------------------------------------------------------------
BOOL CLineDisplay::IsDeviceOpen()
{	
	if( !IsDrvLoaded() )
	{	
		return FALSE;
	}

	if( IsOldInterfaceUsed() )
	{
		if( m_LineDisplayWNPtr->GetState() != OPOS_S_CLOSED ) 		
			return TRUE;
		else
			return FALSE;
	}
	else
	{
		if( m_LineDisplayPtr->GetState() != OPOS_S_CLOSED ) 		
			return TRUE;
		else
			return FALSE;
	}
}