// OPOSTestDlg.cpp : implementation file
//

#include "stdafx.h"
#include "OPOSTest.h"
#include "OPOSTestDlg.h"
#include "CommonFunc.h"
#include ".\opostestdlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


struct FuncDesc
{
	UINT unControlID;
	DWORD (*Func)(CFiscalPrint& FR);
};

FuncDesc aDescrFn[]=
{
	{ IDC_BTN_NONFISCAL,         PrintNonfiscal      },
	{ IDC_BTN_SALE_RECEIPT,      PrintSaleReceipt    },
	{ IDC_BTN_FEFUND_RECEIPT,    PrintFefundReceipt  },
	{ IDC_BTN_CASH_IN,           PrintCashIn         },
	{ IDC_BTN_CASH_OUT,          PrintCashOut        },
	{ IDC_BTN_CANCEL_RECEIPT,    PrintCancelReceipt  },
	{ IDC_BTN_X_REPORT,          PrintXReport        },
	{ IDC_BTN_Z_REPORT,          PrintZReport        },
	{ IDC_BTN_DIRECTIO,          DirectIO            },
	{ IDC_BTN_SET_DATE,          SetDate             },
	{ IDC_BTN_SET_HEAD_AND_TAIL, SetHeaderAndTrailer },
};

// COPOSTestDlg dialog
COPOSTestDlg::COPOSTestDlg(CWnd* pParent /*=NULL*/)
	: CDialog(COPOSTestDlg::IDD, pParent)
	, m_sCOVersion(_T(""))
	, m_sSOVersion(_T(""))
	, m_bLogFile(FALSE)
{
	m_hIcon   = AfxGetApp()->LoadIcon(IDR_MAINFRAME);	
	m_hFiscalPrinterThread = NULL;	
	m_hLineDisplayThread   = NULL;	
	m_hCashDrawerThread    = NULL;
}
COPOSTestDlg::~COPOSTestDlg()
{		
}

void COPOSTestDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CMB_DEVICE,     m_CmbDeviceName);
	DDX_Control(pDX, IDC_LIST_LOG,       m_ListLog      );
	DDX_Control(pDX, IDC_CMB_COM_PORT,   m_CmbComPort   );
	DDX_Text   (pDX, IDC_STC_CO_VERSION, m_sCOVersion   );
	DDX_Text   (pDX, IDC_STC_SO_VERSION, m_sSOVersion   );
	DDX_Check(pDX, IDC_CHK_LOG, m_bLogFile);
}

BEGIN_MESSAGE_MAP(COPOSTestDlg, CDialog)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_CONTROL_RANGE( BN_CLICKED, IDC_BTN_NONFISCAL, IDC_BTN_DIRECTIO, OnBnClickedBtn )	
	ON_WM_CLOSE()
	ON_CBN_SELCHANGE(IDC_CMB_DEVICE,        OnCbnSelchangeCmbDevice)
	ON_CBN_SELCHANGE(IDC_CMB_COM_PORT,      OnCbnSelchangeCmbComPort)
	ON_BN_CLICKED(IDC_BTN_DISPLAY_TEXT,     OnBnClickedBtnDysplayText)
	ON_BN_CLICKED(IDC_BTN_OPEN_CASH_DRAWER, OnBnClickedBtnOpenCashDrawer)

	ON_MESSAGE( TM_ADD_STRING_TO_LOG,   OnAddStringToLog        )
	ON_MESSAGE( TM_COMPLETED_FUNCTION,  OnSetCompletedFunction  )
	ON_MESSAGE( TM_STARTED_FUNCTION,    OnSetStartedFunction    )
	ON_MESSAGE( TM_ADD_ITEM_TO_LOG,     OnAddItemToLog          )
	ON_MESSAGE( TM_COMPLETED_FR_PROCESS,OnSetCompletedFRProcess )
	ON_MESSAGE( TM_COMPLETED_LD_PROCESS,OnSetCompletedLDProcess )
	ON_MESSAGE( TM_COMPLETED_CD_PROCESS,OnSetCompletedCDProcess )
	ON_MESSAGE( TM_DISPLAY_ERROR,       OnDisplayError          )
	ON_MESSAGE( TM_SET_WAIT_CURSOR,     OnSetWaitCursor         )
	ON_MESSAGE( TM_SET_STOP_CURSOR,     OnSetStopCursor         )

	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_CHK_LOG, OnBnClickedChkLog)
END_MESSAGE_MAP()


// COPOSTestDlg message handlers

BOOL COPOSTestDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	CArray<CString,CString> arrDevicesNames;

	m_CmbComPort.EnableWindow( FALSE );
	GetDlgItem( IDC_CHK_LOG )->EnableWindow( FALSE );
	GetDevicesNames(arrDevicesNames);
	for( int i = 0; i < arrDevicesNames.GetSize(); i ++ )
	{
		m_CmbDeviceName.AddString(arrDevicesNames[i]);
	}	

	m_CmbComPort.AddString( "COM1" );
	m_CmbComPort.AddString( "COM2" );
	m_CmbComPort.AddString( "COM3" );
	m_CmbComPort.AddString( "COM4" );	
	
	m_ListLog.InsertColumn( 0, "Function",      LVCFMT_LEFT, 300 );	
	m_ListLog.InsertColumn( 1, "ResultCode",    LVCFMT_LEFT, 70  );
	m_ListLog.InsertColumn( 2, "ResultCodeExt", LVCFMT_LEFT, 70  );	
	m_ListLog.InsertColumn( 3, "ErrorString",   LVCFMT_LEFT, 100  );
	
	m_IL.Create( 16,16,TRUE,4,4);
	m_IL.Add( AfxGetApp()->LoadIcon( IDI_ERROR_ICON ) );
	m_IL.Add( AfxGetApp()->LoadIcon( IDI_SUCCESS_ICON ) );
	m_IL.Add( AfxGetApp()->LoadIcon( IDI_EMPTY_ICON ) );
	m_ListLog.SetImageList( &m_IL, LVSIL_SMALL );

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	

	if( g_FR.GetCOVersion( m_sCOVersion ) != SUCCESS )
	{
		AfxMessageBox( g_FR.GetLastErrorText(), MB_OK|MB_ICONERROR );
		return FALSE;
	};
	UpdateData(FALSE);	
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void COPOSTestDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR COPOSTestDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void COPOSTestDlg::OnBnClickedBtn( UINT nID )
{		
	CString              sDeviceName;

	// event
	EnableAllControls( FALSE );

	m_CmbDeviceName.GetWindowText(sDeviceName);	

	if( sDeviceName.IsEmpty() ) 
	{
		AfxMessageBox("Device is not selected");
		EnableAllControls( TRUE );
		return;
	};

	(GetDlgItem( nID ))->GetWindowText( g_sFunction );	

	g_nCurrentButtonID = nID;

	CreateProcessThread( m_hFiscalPrinterThread, PrintThread,    &m_dwFiscalPrinterThreadID );
}
//-------------------------------------------------------------------------------------//
void COPOSTestDlg::CreateProcessThread( HANDLE& hThread, LPTHREAD_START_ROUTINE lpStartAddress, LPDWORD lpThreadId )
{
	if( hThread ) CloseHandle( hThread );
	hThread = CreateThread( NULL, 0, lpStartAddress, this, 0, lpThreadId );
	if( hThread == NULL )
	{	
		ErrMessage();		
	};
}
//-------------------------------------------------------------------------------------//
DWORD COPOSTestDlg::PrintThread_FN()
{	
	AddStringToLog      ( "   OPENING THE FISCAL PRINTER PORT" );
	if( g_FR.OpenDevice(g_sFRDeviceName) != SUCCESS )
	{		
		SetCompletedFRProcess();
		DisplayError( g_FR.GetLastErrorText() );
		return 0;
	};	
	SetStartedFunction  ( g_sFunction        );
	ProcessFocusedButton( g_nCurrentButtonID );
	SetCompletedFunction( g_sFunction        );	
	AddStringToLog      ( "   CLOSING THE FISCAL PRINTER PORT");
	g_FR.CloseDevice();
	SetCompletedFRProcess();
	return 0;
}
//-------------------------------------------------------------------------------------//
DWORD COPOSTestDlg::PrintThread( LPVOID lpvParam )
{
	ASSERT(lpvParam);
	COPOSTestDlg* pDlg = (COPOSTestDlg*)lpvParam;
	DWORD dwRes = pDlg->PrintThread_FN();	
	return dwRes;
}
//-------------------------------------------------------------------------------------//
void COPOSTestDlg::SetStartedFunction( LPCSTR lpszFunction )
{
	PostThreadMessage( AfxGetApp()->m_nThreadID, TM_STARTED_FUNCTION, (WPARAM)(lpszFunction), (LPARAM)this->GetSafeHwnd() );
}
//-------------------------------------------------------------------------------------//
void COPOSTestDlg::SetCompletedFunction( LPCSTR lpszFunction )
{
	PostThreadMessage( AfxGetApp()->m_nThreadID, TM_COMPLETED_FUNCTION, (WPARAM)(lpszFunction), (LPARAM)this->GetSafeHwnd() );
}
//-------------------------------------------------------------------------------------//
void COPOSTestDlg::AddStringToLog(LPCSTR lpszText)
{
	PostThreadMessage( AfxGetApp()->m_nThreadID, TM_ADD_STRING_TO_LOG, (WPARAM)((LPCSTR)lpszText), (LPARAM)this->GetSafeHwnd() );
}
//-------------------------------------------------------------------------------------//
void COPOSTestDlg::DisplayError( LPCSTR lpszError )
{	
	PostThreadMessage( AfxGetApp()->m_nThreadID, TM_DISPLAY_ERROR, (WPARAM)((LPCSTR)lpszError), (LPARAM)this->GetSafeHwnd() );	
}
//-------------------------------------------------------------------------------------//
void COPOSTestDlg::SetCompletedFRProcess()
{
	PostThreadMessage( AfxGetApp()->m_nThreadID, TM_COMPLETED_FR_PROCESS, 0, (LPARAM)this->GetSafeHwnd() );
}
//-------------------------------------------------------------------------------------//
void COPOSTestDlg::SetCompletedLDProcess()
{
	PostThreadMessage( AfxGetApp()->m_nThreadID, TM_COMPLETED_LD_PROCESS, 0, (LPARAM)this->GetSafeHwnd() );
}
//-------------------------------------------------------------------------------------//
void COPOSTestDlg::SetCompletedCDProcess()
{
	PostThreadMessage( AfxGetApp()->m_nThreadID, TM_COMPLETED_CD_PROCESS, 0, (LPARAM)this->GetSafeHwnd() );
}
//-------------------------------------------------------------------------------------//
void COPOSTestDlg::SetWaitCursor()
{
	PostThreadMessage( AfxGetApp()->m_nThreadID, TM_SET_WAIT_CURSOR, 0, (LPARAM)this->GetSafeHwnd() );
}
//-------------------------------------------------------------------------------------//
void COPOSTestDlg::SetStopCursor()
{
	PostThreadMessage( AfxGetApp()->m_nThreadID, TM_SET_STOP_CURSOR, 0, (LPARAM)this->GetSafeHwnd() );
}
//-------------------------------------------------------------------------------------//
void COPOSTestDlg::ProcessFocusedButton( UINT nID )
{
	CWnd*   wndCurrent = NULL;
	long    lSize      = sizeof(aDescrFn)/sizeof(aDescrFn[0]);
	DWORD   dwResult;
	CArray<CLogItem> Log;	

	for( int i = 0; i < lSize; i++ )
	{
		if( aDescrFn[i].unControlID == nID )
		{
			dwResult = aDescrFn[i].Func( g_FR );
			if( dwResult != SUCCESS )
			{
				//AfxMessageBox( g_FR.GetLastErrorText(), MB_ICONERROR | MB_OK );			
			}
			break;
		}
	}    
}



//void COPOSTestDlg::OnDestroy()
//{
//	CDialog::OnDestroy();		
//}

void COPOSTestDlg::OnClose()
{
	WaitCursor();	
	
	KillThread( m_hFiscalPrinterThread );
	KillThread( m_hLineDisplayThread   );
	KillThread( m_hCashDrawerThread    );

	g_FR.Close();
	g_LD.Close();
	g_CD.Close();
	StopCursor();	
	CDialog::OnClose();
}

void COPOSTestDlg::KillThread( HANDLE& hThread )
{
	if( hThread ) 
	{
		WaitCursor();
		if( WaitForSingleObject( hThread, INFINITE ) != WAIT_OBJECT_0 )		
		{			
			ErrMessage();
		}
		else
		{
			CloseHandle( hThread );
			hThread = NULL;
		}
		StopCursor();
	}
}

void COPOSTestDlg::OnCbnSelchangeCmbDevice()
{
	CString s;
	CString sCOMPort;
	CString sSOVersion;	

	m_sSOVersion.Empty();	

	m_CmbDeviceName.GetWindowText( s );
	
	WaitCursor();
	
	if( !s.IsEmpty() ) FillSOVersion();	

	if( s == "SPFR101")	
	{
		m_CmbComPort.EnableWindow( TRUE );
		if( GetSPFR101COMPort(sCOMPort) == FALSE 
			||
			GetLogFileKept( m_bLogFile ) == FALSE
			)
		{
			AfxMessageBox("Service Object has not installed");
			return;
		}
		else
		{
			if( m_CmbComPort.FindString( -1, sCOMPort) == CB_ERR )
			{
				m_CmbComPort.AddString( sCOMPort );
			}
			m_CmbComPort.SelectString( -1, sCOMPort);	
			UpdateData( FALSE );
		}
		

		this->GetDlgItem( IDC_BTN_DISPLAY_TEXT )->EnableWindow(TRUE && !m_sSOVersion.IsEmpty());
		this->GetDlgItem( IDC_BTN_OPEN_CASH_DRAWER )->EnableWindow(TRUE && !m_sSOVersion.IsEmpty());
		this->GetDlgItem( IDC_BTN_DIRECTIO )->EnableWindow(TRUE && !m_sSOVersion.IsEmpty());
	}
	else
	{
		m_CmbComPort.SetWindowText("");
		m_CmbComPort.EnableWindow( FALSE );	

		this->GetDlgItem( IDC_BTN_DISPLAY_TEXT )->EnableWindow(FALSE);
		this->GetDlgItem( IDC_BTN_OPEN_CASH_DRAWER )->EnableWindow(FALSE);
		this->GetDlgItem( IDC_BTN_DIRECTIO )->EnableWindow(FALSE);
	}	
	m_CmbDeviceName.GetWindowText( g_sFRDeviceName );
	EnableAllControls( TRUE );
	StopCursor();
}

void COPOSTestDlg::OnCbnSelchangeCmbComPort()
{
	CString sCOMPort;
	m_CmbComPort.GetWindowText( sCOMPort );	
	
	SetSPFR101COMPort( sCOMPort );	
	
	if( !sCOMPort.IsEmpty() )
		FillSOVersion();

	g_FR.Close();
	if( !g_FR.Init() )
	{
		AfxMessageBox( g_FR.GetLastErrorText(), MB_OK|MB_ICONERROR );
		return;
	};
}

BOOL COPOSTestDlg::FillSOVersion()
{	
	CString sSOVersion;
	DWORD   dwError;
	BOOL    bResult = TRUE;
	m_sSOVersion.Empty();

	m_CmbDeviceName.GetWindowText( g_sFRDeviceName );
	if( !g_sFRDeviceName.IsEmpty() )
	{
		WaitCursor();	

		dwError = g_FR.OpenDevice( g_sFRDeviceName );
		if( dwError != SUCCESS )
		{	
			AfxMessageBox( g_FR.GetLastErrorText(), MB_OK | MB_ICONERROR  );
			g_FR.Close();
			if( !g_FR.Init() )
			{
				AfxMessageBox( g_FR.GetLastErrorText(), MB_OK|MB_ICONERROR );
				bResult = FALSE;
			};
		}
		else
		{
			dwError = g_FR.GetSOVersion( sSOVersion );
			if( dwError == SUCCESS )
			{
				m_sSOVersion = sSOVersion;				
			}			
		}
		g_FR.CloseDevice();
		StopCursor();
	}
	UpdateData( FALSE );	
	return bResult;
}
//-------------------------------------------------------------------------------------//
void COPOSTestDlg::OnBnClickedBtnDysplayText()
{	
	CString sDeviceName;
	// event
	EnableAllControls( FALSE );

	m_CmbDeviceName.GetWindowText(sDeviceName);	

	if( sDeviceName.IsEmpty() ) 
	{
		AfxMessageBox("Device is not selected");	
		EnableAllControls( TRUE );
		return;
	};

	(GetDlgItem( IDC_BTN_DISPLAY_TEXT ))->GetWindowText( g_sFunction );	

	CreateProcessThread( m_hLineDisplayThread,   DisplayText,    &m_dwLineDisplayThreadID   );		
}
//-------------------------------------------------------------------------------------//
DWORD COPOSTestDlg::DisplayText_FN()
{		
	AddStringToLog( "   OPENING THE LINE DISPLAY" );
	if( g_LD.OpenDevice("SPFR101BA") != SUCCESS )
	{		
		SetCompletedLDProcess();
		DisplayError( g_LD.GetLastErrorText() );
		return 0;
	};	
	SetStartedFunction( g_sFunction );
	g_LD.PrintText();
	SetCompletedFunction( g_sFunction );	

	AddStringToLog      ( "   CLOSING THE LINE DISPLAY");
	g_LD.CloseDevice();
	SetCompletedLDProcess();
	return 0;
}
//-------------------------------------------------------------------------------------//
DWORD COPOSTestDlg::DisplayText( LPVOID lpvParam )
{
	ASSERT(lpvParam);
	COPOSTestDlg* pDlg = (COPOSTestDlg*)lpvParam;
	DWORD dwRes = pDlg->DisplayText_FN();	
	return dwRes;
}
//-------------------------------------------------------------------------------------//
void COPOSTestDlg::OnBnClickedBtnOpenCashDrawer()
{
	// event
	CString sDeviceName;
	EnableAllControls( FALSE );

	m_CmbDeviceName.GetWindowText(sDeviceName);	

	if( sDeviceName.IsEmpty() ) 
	{
		AfxMessageBox("Device is not selected");	
		EnableAllControls( TRUE );
		return;
	};

	(GetDlgItem( IDC_BTN_OPEN_CASH_DRAWER ))->GetWindowText( g_sFunction );	

	CreateProcessThread( m_hCashDrawerThread, OpenCashDrawer, &m_dwCashDrawerThreadID );	
}
//-------------------------------------------------------------------------------------//
DWORD COPOSTestDlg::OpenCashDrawer_FN()
{		
	AddStringToLog( "   OPENING THE CASH DRAWER" );
	if( g_CD.OpenDevice("SPFR101KA") != SUCCESS )
	{		
		SetCompletedCDProcess();
		DisplayError( g_CD.GetLastErrorText() );
		return 0;
	};	
	SetStartedFunction( g_sFunction );
	g_CD.OpenDrawer();
	SetCompletedFunction( g_sFunction );	

	AddStringToLog      ( "   CLOSING THE CASH DRAWER");
	g_CD.CloseDevice();
	SetCompletedCDProcess();
	return 0;
}
//-------------------------------------------------------------------------------------//
DWORD COPOSTestDlg::OpenCashDrawer( LPVOID lpvParam )
{
	ASSERT(lpvParam);
	COPOSTestDlg* pDlg = (COPOSTestDlg*)lpvParam;
	DWORD dwRes = pDlg->OpenCashDrawer_FN();	
	return dwRes;
}
//-------------------------------------------------------------------------------------//

LRESULT COPOSTestDlg::OnAddStringToLog( WPARAM wParam, LPARAM lParam )
{
	CString s = (LPCSTR)wParam;	
	m_ListLog.InsertItem   ( m_ListLog.GetItemCount(), s, 2);
	m_ListLog.EnsureVisible( m_ListLog.GetItemCount()-1, FALSE );	
	return 0;
}

LRESULT COPOSTestDlg::OnSetCompletedFunction( WPARAM wParam, LPARAM lParam )
{		
	CString sFunc = (LPCSTR)wParam;
	CString s = "   COMPLETED " + sFunc;	
	m_ListLog.InsertItem   ( m_ListLog.GetItemCount(), s, 2);
	m_ListLog.EnsureVisible( m_ListLog.GetItemCount()-1, FALSE );	
	return 0;
}

LRESULT COPOSTestDlg::OnSetStartedFunction  ( WPARAM wParam, LPARAM lParam )
{
	CString sFunc = (LPCSTR)wParam;	
	CString s = "   STARTED " + sFunc;	
	m_ListLog.InsertItem   ( m_ListLog.GetItemCount(), s, 2);
	m_ListLog.EnsureVisible( m_ListLog.GetItemCount()-1, FALSE );	
	return 0;
}

LRESULT COPOSTestDlg::OnSetCompletedFRProcess( WPARAM wParam, LPARAM lParam )
{
	g_FR.CloseDevice();
	KillThread( m_hFiscalPrinterThread );		
	EnableAllControls(TRUE);
	return 0;
}

LRESULT COPOSTestDlg::OnSetCompletedLDProcess( WPARAM wParam, LPARAM lParam )
{
	g_LD.CloseDevice();	
	KillThread( m_hLineDisplayThread );		
	EnableAllControls(TRUE);
	return 0;
}

LRESULT COPOSTestDlg::OnSetCompletedCDProcess( WPARAM wParam, LPARAM lParam )
{
	g_CD.CloseDevice();
	KillThread( m_hCashDrawerThread );			
	EnableAllControls(TRUE);
	return 0;
}

LRESULT COPOSTestDlg::OnDisplayError( WPARAM wParam, LPARAM lParam )
{	
	CString s = (LPCSTR)wParam;	
	AfxMessageBox( s, MB_OK | MB_ICONERROR );	
	return 0;
}

LRESULT COPOSTestDlg::OnSetWaitCursor( WPARAM wParam, LPARAM lParam )
{
	WaitCursor();
	return 0;
}
LRESULT COPOSTestDlg::OnSetStopCursor( WPARAM wParam, LPARAM lParam )
{
	StopCursor();
	return 0;
}

LRESULT COPOSTestDlg::OnAddItemToLog( WPARAM wParam, LPARAM lParam )
{
	int   iLogItemIndex = (int)wParam;
	CSize sz;
	CRect rc;

	CArray<CLogItem> Log;
	
	
	switch( (OPOSDeviceType)lParam )
	{
	case FR:  g_FR.GetLog( Log ); break;
	case CD:  g_LD.GetLog( Log ); break;
	case DR:  g_CD.GetLog( Log ); break;
	}

	if( iLogItemIndex < Log.GetSize() )
	{	
		int iIndex = m_ListLog.GetItemCount();
		m_ListLog.InsertItem ( iIndex, Log[iLogItemIndex].m_sFunction, Log[iLogItemIndex].m_enState );				
		m_ListLog.SetItemText( iIndex, 1,  Log[iLogItemIndex].m_sResultCode    );
		m_ListLog.SetItemText( iIndex, 2,  Log[iLogItemIndex].m_sResultCodeExt );
		m_ListLog.SetItemText( iIndex, 3,  Log[iLogItemIndex].m_sErrorString   );				
		m_ListLog.EnsureVisible( iIndex, FALSE );			
	};	
	return 0;
}

void COPOSTestDlg::EnableAllControls(BOOL bEnable)
{
	CString s;

	GetDlgItem(IDC_BTN_NONFISCAL)->EnableWindow( bEnable );
	GetDlgItem(IDC_BTN_SALE_RECEIPT)->EnableWindow( bEnable );
	GetDlgItem(IDC_BTN_FEFUND_RECEIPT)->EnableWindow( bEnable );
	GetDlgItem(IDC_BTN_CASH_IN)->EnableWindow( bEnable );
	GetDlgItem(IDC_BTN_CASH_OUT)->EnableWindow( bEnable );
	GetDlgItem(IDC_BTN_CANCEL_RECEIPT)->EnableWindow( bEnable );
	GetDlgItem(IDC_BTN_X_REPORT)->EnableWindow( bEnable );
	GetDlgItem(IDC_BTN_Z_REPORT)->EnableWindow( bEnable );	
	GetDlgItem(IDC_BTN_SET_DATE)->EnableWindow( bEnable );
	GetDlgItem(IDC_BTN_SET_HEAD_AND_TAIL)->EnableWindow( bEnable );		
	GetDlgItem(IDC_CMB_DEVICE)->EnableWindow( bEnable );		
	
	m_CmbDeviceName.GetWindowText( s );
	if( s == "SPFR101" || s == "SPFR101.COM" )	
	{
		GetDlgItem( IDC_CMB_COM_PORT         )->EnableWindow(TRUE&&bEnable);
		GetDlgItem( IDC_BTN_DISPLAY_TEXT     )->EnableWindow(TRUE&&bEnable);
		GetDlgItem( IDC_BTN_OPEN_CASH_DRAWER )->EnableWindow(TRUE&&bEnable);
		GetDlgItem( IDC_BTN_DIRECTIO         )->EnableWindow(TRUE&&bEnable);
		GetDlgItem( IDC_CHK_LOG              )->EnableWindow(TRUE&&bEnable);
	}
	else
	{
		GetDlgItem( IDC_CMB_COM_PORT         )->EnableWindow(FALSE);
		GetDlgItem( IDC_BTN_DISPLAY_TEXT     )->EnableWindow(FALSE);
		GetDlgItem( IDC_BTN_OPEN_CASH_DRAWER )->EnableWindow(FALSE);
		GetDlgItem( IDC_BTN_DIRECTIO         )->EnableWindow(FALSE);
		GetDlgItem( IDC_CHK_LOG              )->EnableWindow(FALSE);
	};
}
void COPOSTestDlg::OnDestroy()
{
	CDialog::OnDestroy();	
}

void COPOSTestDlg::OnBnClickedChkLog()
{
	UpdateData(TRUE);
	SetLogFileKept( m_bLogFile );        
}
