#pragma once

#include "ErrorDef.h"


enum OPOSDeviceType
{
	FR = 0,
	CD = 1,
	DR = 2
};

//------------------------------------------------------------------
// RUN_OPOS_FUNC - helpful macros
//                 runs function and adds item to log
//   func                   - function calling
//   lErrorCodeIfNotSuccess - error code to be returned if function 
//                            have not returned OPOS_SUCCESS
//------------------------------------------------------------------
#define RUN_OPOS_FUNC(func, lErrorCodeIfNotSuccess)\
{\
	USES_CONVERSION;\
	long lResult = pInterface->func;\
	CLogItem::ItemState enState = ( lResult != OPOS_SUCCESS ) ? \
		CLogItem::ItemState::stWrong : CLogItem::ItemState::stOK;\
	BSTR bstr = GetErrorString();\
	CString str = OLE2CA(bstr);\
	::SysFreeString(bstr);\
	AddItemToLog( CLogItem( enState, #func, pInterface->GetResultCode(), \
		pInterface->GetResultCodeExtended(), str ) );\
	if( lResult != OPOS_SUCCESS )\
	{\
	SetLastError( lErrorCodeIfNotSuccess, lResult );\
	return lErrorCodeIfNotSuccess;\
	}\
}

class CLogItem
{
public:
	enum ItemState
	{
        stWrong   = 0,
		stOK      = 1,
		stNeutral = 2,
	} m_enState;

	CString m_sFunction;
	CString m_sResultCode;   
	CString m_sResultCodeExt;
	CString m_sErrorString;
	CLogItem()
	{
		m_enState = stNeutral;
	}
	CLogItem( ItemState enState, LPCSTR lpszFunction, DWORD dwResultCode, DWORD dwResultCodeExt, LPCSTR lpszText )
	{
		m_enState        = enState;
		m_sFunction      = lpszFunction;
		m_sErrorString   = lpszText;
		m_sResultCode.Format( "%d", dwResultCode );
		m_sResultCodeExt.Format( "%d", dwResultCodeExt );		
	}

	CLogItem( LPCSTR lpszFunction, VARIANT_BOOL bResult )
	{
		m_enState        = stNeutral;
		m_sFunction      = lpszFunction;
		m_sErrorString   = "";
		if( bResult != FALSE ) m_sResultCode = "TRUE";
		else                   m_sResultCode = "FALSE";
		m_sResultCodeExt = "";
	}

	CLogItem( LPCSTR lpszFunction )
	{
		m_enState        = stNeutral;
		m_sFunction      = lpszFunction;
		m_sErrorString   = "";
		m_sResultCode = "";		
		m_sResultCodeExt = "";
	}

};

class CPOSDevice
{
	
public:
	CPOSDevice(OPOSDeviceType);
	~CPOSDevice(void);

	CArray<CLogItem>       m_Log;

protected:
	OPOSDeviceType          m_enDeviceType;
	BOOL                    m_bWNInterfaceUsed;

	// error
	CString                 m_sError;
	CString                 m_sErrorExt;
	DWORD                   m_dwErrorCode;

	CString                  m_sProgID;

protected:
	// error codes, depending on the device
	virtual DWORD GetError_DrvNotLoaded() = 0;
	virtual DWORD GetError_Opening()      = 0;	

	void SetLastError( DWORD dwErrorCode, LPCTSTR lpszExtInfo = NULL );
	virtual void SetLastError( DWORD dwErrorCode, long lExtErrorCode ) = 0;
	void GetErrorMessageWin32( CString& sMessage );
	//------------------------------------------------------------------
	// Setting a last error number. You can get text description of the last error 
	// after any function calling.
	//      IN:     dwErrorCode      - an error code
	//              1) lpszExtInfo   - extended info about error
	//              2) lExtErrorCode - Error code, returned by OPOS-driver
	//      return: void
	//------------------------------------------------------------------

	void AddItemToLog( IN const CLogItem& Item );
	//------------------------------------------------------------------
	// Adds an item to log, sends message UM_WRITE_LOG to main window
	//      IN:     string in log
	//      return: void	
	//------------------------------------------------------------------

	BOOL IsOldInterfaceUsed() {return m_bWNInterfaceUsed;}
	//------------------------------------------------------------------
	// Return TRUE, if is used an old interface (version 1.6), FALSE, if new (1.7 - ...)
	//      return: TRUE or FALSE
	//------------------------------------------------------------------

	virtual _bstr_t GetErrorString() = 0;

	virtual void Release() = 0;
	//------------------------------------------------------------------
	// Release all interface pointer	
	//       return: void
	//------------------------------------------------------------------

public:
	void     GetLog( OUT CArray<CLogItem>& Log );
	//------------------------------------------------------------------
	// Get log
	//      return: void
	//------------------------------------------------------------------

	CString& GetLastErrorText();
	//------------------------------------------------------------------
	// Getting a text description of last error. This function can be 
	// called after any function of this class is completed	
	//      return: text description of the last error or an empty 
	//              string, if previous function have been completed
	//              successfully 	
	//------------------------------------------------------------------

	virtual BOOL Init() = 0;
	//------------------------------------------------------------------
	// Init the device. Attaching OPOS Control Object.
	//      return: TRUE if success, else FALSE	
	//------------------------------------------------------------------

	virtual void Close() = 0;
	//------------------------------------------------------------------
	// Close the device, delete interface pointer
	//      return: void
	//------------------------------------------------------------------	

	virtual BOOL IsDrvLoaded() = 0;
	//------------------------------------------------------------------
	// Checks, if driver is loaded. If Init() is success, driver is 
	// loaded	
	//      return: TRUE if success, else FALSE	
	//------------------------------------------------------------------

	DWORD OpenDevice(LPCSTR lpszDeviceName);
	//------------------------------------------------------------------
	// Open device
	//      return: SUCCESS if ready, else error code
	//------------------------------------------------------------------

	virtual BOOL IsDeviceOpen() = 0;
	//------------------------------------------------------------------
	// Checks, if device is opened
	//      return: TRUE, if opened, else FALSE
	//------------------------------------------------------------------

	virtual DWORD CheckReady() = 0;
	//------------------------------------------------------------------
	// checks, if device ready, that's if loaded a driver and opened port
	//      return: SUCCESS if ready, else error code
	//------------------------------------------------------------------

	virtual void  CloseDevice() = 0;
	//------------------------------------------------------------------
	// Close device
	//      return: void
	//------------------------------------------------------------------
	
	// template functions for OPOS-interfaces	
	template<class T>  void  CloseDevice ( T pInterface );
	template<class T>  DWORD OpenDevice  ( T pInterface, LPCSTR lpszDeviceName );
	template<class T>  void  SetLastError( T pInterface, DWORD dwErrorCode, long dwExtErrorCode );
};

//------------------------------------------------------------------
// CloseDevice()
//------------------------------------------------------------------
template<class T> void  CPOSDevice::CloseDevice( T pInterface )
{
	if( !IsDrvLoaded() )
	{	
		return;
	}
	long lError;

	if( pInterface->GetState() == OPOS_S_CLOSED )
		return;

	if( pInterface->GetDeviceEnabled() ) 
		pInterface->SetDeviceEnabled(FALSE);

	if( pInterface->GetClaimed() ) 
	{
		lError = pInterface->ReleaseDevice();
	}

	if( pInterface->GetState() != OPOS_S_CLOSED ) 
	{ 
		lError = pInterface->Close();
	}
};

//------------------------------------------------------------------
// OpenDevice()
//------------------------------------------------------------------
template<class T>
DWORD CPOSDevice::OpenDevice(T pInterface, LPCSTR lpszDeviceName)
{
	long lState;
	long lError;
	if( !IsDrvLoaded() )
	{
		SetLastError( GetError_DrvNotLoaded() );
		return GetError_DrvNotLoaded();
	}

	lState = pInterface->GetState();	

	if( lState == OPOS_S_CLOSED ) 
	{
		lError = pInterface->Open(lpszDeviceName);
		if( lError != OPOS_SUCCESS )
		{				
			SetLastError( GetError_Opening(), lError );
			return GetError_Opening();
		}
	};

	if( !pInterface->GetClaimed() ) 
	{ 
		lError = pInterface->ClaimDevice(0);
		if( lError != OPOS_SUCCESS )
		{				
			SetLastError( GetError_Opening(), lError );
			return GetError_Opening();
		}
	};	

	pInterface->SetDeviceEnabled( TRUE );
	if( !pInterface->GetDeviceEnabled() )
	{
		SetLastError( GetError_Opening(), pInterface->GetResultCode() );
		return GetError_Opening();
	};

	return SUCCESS;
}
//------------------------------------------------------------------
// SetLastError()
//------------------------------------------------------------------
template<class T>
void CPOSDevice::SetLastError( T pInterface, DWORD dwErrorCode, long dwExtErrorCode )
{
	USES_CONVERSION;

	CString sError, sErrorExt;	
	_bstr_t bstrError;
	if( dwExtErrorCode != OPOS_SUCCESS)
	{
		if( IsDrvLoaded() )
		{	
			sErrorExt = "";
			bstrError = GetErrorString();
			CString sError( OLE2CA(bstrError));
			
			if( sError.IsEmpty() )
			{
				sErrorExt.Format( "Error code: %d\nExtended error code: %d\n", dwExtErrorCode, 
					pInterface->GetResultCodeExtended() );
			}
			else
			{
				sErrorExt.Format( "Error code: %d\nExtended error code: %d\nText description: %s\n", 
					dwExtErrorCode, pInterface->GetResultCodeExtended(), sError );
			}
		}   
		else
		{
			sErrorExt.Format( "Unknown OPOS-driver error � %d.\n", dwExtErrorCode );
		}
		SetLastError( dwErrorCode, sErrorExt );
	};
}