#pragma once

#define   UM_WRITE_LOG  (WM_USER+1000) // user message about adding item to log

#define   SUCCESS                               0

#define   USER_ERROR_BASE                       100000L
#define   ERROR_CO_CLASSSTRING                  (USER_ERROR_BASE +  2L)
#define   ERROR_CO_GENERAL                      (USER_ERROR_BASE +  3L)
#define   ERROR_UNKNOWN                         (USER_ERROR_BASE +  4L)


#define   FP_ERROR_DRV_NOT_LOADED               (USER_ERROR_BASE +  5L)
#define   FP_ERROR_OPENING                      (USER_ERROR_BASE +  6L)
#define   FP_ERROR_NOT_OPEN                     (USER_ERROR_BASE +  7L)
#define   FP_ERROR_BEGIN_NONFISCAL              (USER_ERROR_BASE +  8L)
#define   FP_ERROR_PRINT_NORMAL                 (USER_ERROR_BASE +  9L)
#define   FP_ERROR_END_NONFISCAL                (USER_ERROR_BASE + 10L)
#define   FP_ERROR_SALE_RECEIPT                 (USER_ERROR_BASE + 11L)
#define   FP_ERROR_REFUND_RECEIPT               (USER_ERROR_BASE + 12L)
#define   FP_ERROR_CASH_IN                      (USER_ERROR_BASE + 13L)
#define   FP_ERROR_CASH_OUT                     (USER_ERROR_BASE + 14L)
#define   FP_ERROR_CANCEL_RECEIPT               (USER_ERROR_BASE + 15L)
#define   FP_ERROR_X_REPORT                     (USER_ERROR_BASE + 16L)
#define   FP_ERROR_Z_REPORT                     (USER_ERROR_BASE + 17L)
#define   FP_ERROR_SET_DATE                     (USER_ERROR_BASE + 18L)
#define   FP_ERROR_SET_HEADER_AND_TAILER        (USER_ERROR_BASE + 19L)
#define   FP_ERROR_RESET_PRINTER                (USER_ERROR_BASE + 20L)
#define   FP_ERROR_DIRECTIO                     (USER_ERROR_BASE + 21L)

#define   CD_ERROR_CO_CLASSSTRING               (USER_ERROR_BASE + 103L)
#define   CD_ERROR_DRV_NOT_LOADED               (USER_ERROR_BASE + 104L)
#define   CD_ERROR_OPENING                      (USER_ERROR_BASE + 105L)
#define   CD_ERROR_NOT_OPEN                     (USER_ERROR_BASE + 106L)
#define   CD_ERROR_PRINT_TEXT                   (USER_ERROR_BASE + 107L)

#define   DR_ERROR_PRINT_TEXT                   (USER_ERROR_BASE + 201L)
#define   DR_ERROR_DRV_NOT_LOADED               (USER_ERROR_BASE + 202L)
#define   DR_ERROR_OPENING                      (USER_ERROR_BASE + 105L)
#define   USER_ERROR_MAX_BASE                   200000L
/*
inline UINT GetErrorResourceID( DWORD dwErrorCode )
{
	UINT unResourceID;

	switch( dwErrorCode ) 
	{		
	//common
	case ERROR_CO_CLASSSTRING:     unResourceID = IDS_ERROR_CO_CLASSSTRING;      break;
	case ERROR_CO_GENERAL:         unResourceID = IDS_ERROR_CO_GENERAL;          break;	
	
	//fiscal printer
	case FP_ERROR_DRV_NOT_LOADED:  unResourceID = IDS_FP_ERROR_DRV_NOT_LOADED;   break;
	case FP_ERROR_OPENING:         unResourceID = IDS_FP_ERROR_OPENING;          break;
	case FP_ERROR_NOT_OPEN:        unResourceID = IDS_FP_ERROR_NOT_OPEN;         break;
	case FP_ERROR_BEGIN_NONFISCAL: unResourceID = IDS_FP_ERROR_BEGIN_NONFISCAL;  break;
	case FP_ERROR_PRINT_NORMAL:    unResourceID = IDS_FP_ERROR_PRINT_NORMAL;     break;
	case FP_ERROR_END_NONFISCAL:   unResourceID = IDS_FP_ERROR_END_NONFISCAL;    break;
	case FP_ERROR_SALE_RECEIPT:    unResourceID = IDS_FP_ERROR_SALE_RECEIPT;     break;	
	
	//customer display
	case CD_ERROR_DRV_NOT_LOADED:  unResourceID = IDS_CD_ERROR_DRV_NOT_LOADED;   break;
	case CD_ERROR_OPENING:         unResourceID = IDS_CD_ERROR_OPENING;          break;
	case CD_ERROR_NOT_OPEN:        unResourceID = IDS_CD_ERROR_NOT_OPEN;         break;	
	default:                       unResourceID = IDS_ERROR_UNKNOWN;             break;
	}
	return unResourceID;
}
*/