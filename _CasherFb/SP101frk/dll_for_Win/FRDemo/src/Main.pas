unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComObj, StdCtrls, DateUtils;

type
  TFRTest = class(TForm)
    btnDLL: TButton;
    btnOLE: TButton;
    procedure btnOLEClick(Sender: TObject);
    procedure btnDLLClick(Sender: TObject);
  private
    { Private declarations }
    procedure CheckResult(ARslt: WORD);
  public
    { Public declarations }
  end;

var
  FRTest: TFRTest;

implementation

uses SP101FRK;

{$R *.dfm}

procedure TFRTest.btnOLEClick(Sender: TObject);
var
  opername, ArticleName, ArticleCode: WideString;
  m_RecptNumber: word;
  v: variant;
  RT: currency;
begin
  // working with Fiscal Printer via OLE-Automation Object
  m_RecptNumber := 0;
  opername := '01TEST-TASK';
  ArticleCode := '123456789012';

  try
    v := CreateoleObject('SP101FRKLib.SP101FRObject');

    //Connect to Fiscal Printer: COM1
    CheckResult(v.Connect(1));
    try
      //Fiscal Printer initialization
      CheckResult(v.Init(Now));

      //start sale receipt
      CheckResult(v.OpenReceipt(2, 1, opername, m_RecptNumber));

      //add item
      ArticleName := 'TEST-ITEM';
      CheckResult(v.AddArticle(ArticleName, ArticleCode, 1.06,1.06,1, RT));

      ArticleName := 'Test item  - 40 chars 123456789012345';
      CheckResult(v.AddArticleEx(ArticleName, ArticleCode, 1.06,1.06,1, 0, RT));

      //Void item
      CheckResult(v.StornoArticleEx(ArticleName, ArticleCode, 1.06,1.06,1, 0,  RT));

      //Print any text
      CheckResult(v.PrintString('Text'));

      //Subtotal
      CheckResult(v.SubTotal(RT));

      //Discount
      CheckResult(v.DiscountTotal('Discount 1', 0.5, RT));

      //Discount with long name
      CheckResult(v.DiscountPercent('Discount 1 12345678901123456789012345678', 0.5, RT));

      //Payment
      CheckResult(v.Payment(1, RT, RT));

      //Close receipt
      CheckResult(v.CloseReceipt);

      //Close the shift (Z-report)
      CheckResult(v.ZReport(opername));

    finally
      //Close port handle
      v.Disconnect;
    end;
  except
    on E: Exception do begin
      MessageBox(handle, PChar('OLE-object error: '+E.Message), 'Error', MB_ICONERROR or MB_OK);
      Exit;
    end;
  end;
end;

procedure TFRTest.CheckResult(ARslt: WORD);
begin
  if (ARslt <> 0) then begin
    raise Exception.Create('Fiscal Printer Error: '+ IntToStr(Arslt));
  end;
end;

procedure TFRTest.btnDLLClick(Sender: TObject);
var
  opername, ArticleName, ArticleCode: WideString;
  m_RecptNumber: word;
  RT: currency;
  hfr: THandle;
  ErrCode: Byte;
  dt: TSPFRDT;
  tm: TSPFRTM;
  year, month, day, hour, minute, second, ms: WORD;
  oname: TSPFR_OPERATOR_NAME;
  aname: TSPFR_ARTICLE_NAME;
  acode: TSPFR_ARTICLE_CODE;
  str: TSPFR_STRING;
  dname: TSPFR_DISCOUNT_NAME;

  aLongname: TSPFR_ARTICLE_NAME2;
  dLongname: TSPFR_DISCOUNT_NAME2;
  aLongcode: TSPFR_ARTICLE_CODE2;
begin
  // working with Fiscal Printer via sp101fr.dll
  m_RecptNumber := 0;
  opername := '01TEST-TASK';
  try
    hfr := SPFR_CheckPort(1, ErrCode);
    if (hfr <> 0) and (hfr <> INVALID_HANDLE_VALUE) then begin
      try
        //Initialization
        DecodeDateTime(now, year, month, day, hour, minute, second, ms);
        dt.day := day;
        dt.month := month;
        dt.year := year;
        tm.hour := hour;
        tm.minute := minute;
        tm.second := second;
        CheckResult(SPFR_Init(hfr, @dt, @tm));

        //start sale receipt
        FillChar(oname.s, 21, #0);
        StrPCopy(oname.s, Copy(opername, 1, 20));
        CheckResult(SPFR_OpenReceipt(hfr, 2, 1, @oname, m_RecptNumber));


        //add item
        ArticleCode := '123456789012';
        ArticleName := 'TEST_ITEM1';

        FillChar(aname.s, 35, #0);
        StrPCopy(aname.s, Copy(ArticleName, 1, 34));
        FillChar(acode.s, 14, #0);
        StrPCopy(acode.s, Copy(ArticleCode, 1, 13));
        CheckResult(SPFR_AddArticle(hfr, @aname, @acode, 1.06, 1.06, 1, RT));

        //add item position - long item name + VAT id
        ArticleName := 'TEST_ITEM - 40 chars 123456789012345';
        FillChar(aLongname.s, 41, #0);
        StrPCopy(aLongname.s, Copy(ArticleName, 1, 40));
        FillChar(aLongcode.s, 19, #0);
        StrPCopy(aLongcode.s, Copy(ArticleCode, 1, 18));
        CheckResult(SPFR_AddArticleEx2(hfr, @aLongname, @aLongcode, 1.06, 1.06, 1, 0, RT));

        //void item
        CheckResult(SPFR_StornoArticleEx2(hfr, @aLongname, @aLongcode, 1.06, 1.06, 1, 0, RT));

        //print any text
        FillChar(str.s, 41, #0);
        StrPCopy(str.s, 'Text');
        CheckResult(SPFR_PrintString(hfr, @str));


        //Subtotal
        CheckResult(SPFR_SubTotal(hfr, RT));

        //Discount
        FillChar(dname.s, 33, #0);
        StrPCopy(dname.s, 'Discount 1');
        CheckResult(SPFR_DiscountTotal(hfr, @dname, 0.5, RT));

        //Payment
        CheckResult(SPFR_Payment(hfr, 1, RT, RT));

        //Close receipt
        CheckResult(SPFR_CloseReceipt(hfr));

        //Close the shift (Z-report)
        CheckResult(SPFR_ZReport(hfr, @oname));
      finally
        SPFR_ReleasePort(hfr);
      end;
    end else begin
      MessageBox(handle, PChar('Fiscal Printer connect error'), 'Error', MB_ICONERROR or MB_OK);

    end;
  except
    on E: Exception do begin
      MessageBox(handle, PChar('Error: '+E.Message), 'Error', MB_ICONERROR or MB_OK);
      Exit;
    end;
  end;


end;

end.
