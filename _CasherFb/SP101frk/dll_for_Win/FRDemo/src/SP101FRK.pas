{*******************************************************}
{                                                       }
{         SP1010FR-K interface library                  }
{                                                       }
{         Copyright (c) 2003-2004 Service Plus AT.      }
{         All rights reserved.                          }
{                                                       }
{                                                       }
{*******************************************************}
unit SP101FRK;

interface
uses
  Windows;

const
//sp101fr functions return values
  SPFRE_OK                    = 0;	//no error
                        //IO errors
  SPFRE_STATUS		      = 1;	//couldn't execute function, invalid status value
  SPFRE_INVALID_FUNCTION      = 2;	//invalid function code
  SPFRE_INVALID_PARAMETER     = 3;	//invalid parameter
                        //protocol errors
  SPFRE_UART_OVER             = 4;	//port buffer overflow
  SPFRE_INTERCHAR_TIMEOUT     = 5;	//data sending timeout
  SPFRE_INVALID_PASSWORD      = 6;	//invalid password in command
  SPFRE_INVALID_CHECKSUM      = 7;	//CRC error
                        //printer errors
  SPFRE_PAPER_END             = 8;	//paper end
  SPFRE_PRINTER_NOTREADY      = 9;	//printer not ready
                        //date & time errors
  SPFRE_SHIFT_TIME_OVER       = 10;	//current shift is longer than 24h
  SPFRE_SYNC_TIME_OVER        = 11;	//time sync error
  SPFRE_TIME_LOW              = 12;	//
  SPFRE_NO_HEADER             = 13;	//document header not found
  SPFRE_FALSE		      = 14;	//erroneous result
                        //fatal errors
  SPFRE_DATA_SEND	      = $10;	//couldn't send data
  SPFRE_NODATA		      =	$11;	//no data from FR
  SPFRE_PORT_INACCESSIBLE     =	$12;	//port inaccessible
  SPFRE_NOREGISTRATOR	      =	$13;	//FR not found


  SPFRE_FATAL		      =	$20;	//fatal error
  SPFRE_NO_FREE_SPACE	      =	$21;	//memory low
  SPFRE_DEVICE_TIMEOUT	      =	$30;	//device timeout
  SPFRE_BUSY		      = $40;	//device busy
                        //EEJ errors
  SPFRE_EEJ1		      =	$41;	//incorrect command or parameter
  SPFRE_EEJ2		      =	$42;	//invalid EEJ state
  SPFRE_EEJ3		      =	$43;	//EEJ crash
  SPFRE_EEJ4		      =	$44;	//EEJ crypto coprocessor
  SPFRE_EEJ5		      =	$45;	//EEJ using time deadline
  SPFRE_EEJ6		      =	$46;	//EEJ overflowed
  SPFRE_EEJ7		      =	$47;	//invalid date or time
  SPFRE_EEJ8		      =	$48;	//requested data not found
  SPFRE_EEJ9		      =	$49;	//overflow
  SPFRE_EEJA		      =	$4A;	//no response
  SPFRE_EEJB		      =	$4B;	//EEJ data eschange error

  SPFR_COMPACT = $01; //compact
  SPFR_DOUBLEHEIGHT = $10; //DOUBLE_HEIGHT
  SPFR_DOUBLEWIDTH = $20; //DOUBLE_WIDTH
  SPFR_UNDERLINE = $80; //Underline

type

{$Z4} //Size of constants enumerated types = 4 bytes

  TSPRF_DATA_AREA = (
                arBIOS, //BIOS
                arDATA, //Data area
                arRAM	//RAM area
                );


  TBARCODE_TYPE = (
	UPCA = 0,		//UPC-A
	UPCE,			//UPC-E
	EAN13,			//EAN-13
	EAN8,			//EAN-8
	Code39,			//Code-39
	Interleaved,    	//Interleaved 2 of 5
	Codabar			//Codabar
        );
  TBARCODE_WIDTH = (
	bw2 = 0,		//width = 2
	bw3 = 1,		//width = 3
	bw4 = 2			//width = 4
        );

  TBARCODE_ASDIGIT = (
	NO_DIGIT = 0,	//do dot print digits in barcode
	DIGIT_UP,	//print digits at the top
	DIGIT_DOWN,	//print digits at the bottom
	DIGIT_UPDOWN	//print digits at the top and at the bottom
        );


  TSPFR_EEJ_NUMBER = packed record
    s: array[0..16] of char;
  end;
  PSPFR_EEJ_NUMBER = ^TSPFR_EEJ_NUMBER;

  TSPFR_SUMS = packed record
    sums: packed array[0..15] of currency;
  end;
  PSPFR_SUMS = ^TSPFR_SUMS;


  TSPFR_CURRENTRECEIPTSUMS = packed record
    ReceiptSum: currency;
    ReceiptDiscountSum: currency;
    ReceiptExtraSum: currency;
  end;
  PSPFR_CURRENTRECEIPTSUMS = ^TSPFR_CURRENTRECEIPTSUMS;

  TSPFR_RECEIPTCOUNTERS = packed record
    SellReceipts: integer;
    RebookReceipts: integer;
    CancelledReceipts: integer;
    StoredReceipts: integer;
    DepositReceipts: integer;
    TakingReceipts: integer;
  end;
  PSPFR_RECEIPTCOUNTERS = ^TSPFR_RECEIPTCOUNTERS;


  TSPFR_RECEIPTSSUMS = packed record
    CancelledReceiptsSum: currency;
    StoredReceiptsSum: currency;
    DepositReceiptsSum: currency;
    TakingReceiptsSum: currency;
  end;
  PSPFR_RECEIPTSSUMS = ^TSPFR_RECEIPTSSUMS;

  TSPFR_DISCOUNTANDEXTRASUMS = packed record
    SellDiscountSum: currency;
    SellExtraSum: currency;
    RebookDiscountSum: currency;
    RebookExtraSum: currency;
  end;
  PSPFR_DISCOUNTANDEXTRASUMS = ^TSPFR_DISCOUNTANDEXTRASUMS;

  TSPFR_TAXSUMS = packed record
    sums: packed array[0..5] of currency;
  end;
  PSPFR_TAXSUMS = ^TSPFR_TAXSUMS;

  TSPFR_PAYMENTCOUNTS = packed record
    paymentcounts: packed array[0..15] of integer;
  end;
  PSPFR_PAYMENTCOUNTS = ^TSPFR_PAYMENTCOUNTS;




  TSPFR_BARCODE = packed record
    s: array[0..40] of char;
    BarcodeType         : TBARCODE_TYPE;
    BarcodeAsDigit      : TBARCODE_ASDIGIT;
    BarcodeWidth        : TBARCODE_WIDTH;
    BarcodeHeight       : BYTE;
  end;
  PSPFR_BARCODE = ^TSPFR_BARCODE;

  TSPFR_CLIENTDISPLAYTEXT = packed record
    s: array[0..20] of char;
  end;
  PSPFR_CLIENTDISPLAYTEXT = ^TSPFR_CLIENTDISPLAYTEXT;

  TSPFR_INN = packed record
    s: array[0..12] of char;
  end;
  PSPFR_INN = ^TSPFR_INN;

  TSPFR_REGISTRATION_NUMBER = packed record
    s: array[0..12] of char;
  end;
  PSPFR_REGISTRATION_NUMBER = ^TSPFR_REGISTRATION_NUMBER;

  TSPFR_SERIAL_NUMBER = packed record
    s: array[0..12] of char;
  end;
  PSPFR_SERIAL_NUMBER = ^TSPFR_SERIAL_NUMBER;

  TSPFR_PASSWORD = packed record
    s: array[0..10] of char;
  end;
  PSPFR_PASSWORD = ^TSPFR_PASSWORD;

  TSPFR_ARTICLE_CODE = packed record
    s: array[0..13] of char;
  end;
  PSPFR_ARTICLE_CODE = ^TSPFR_ARTICLE_CODE;

  TSPFR_ARTICLE_CODE2 = packed record
    s: array[0..18] of char;
  end;
  PSPFR_ARTICLE_CODE2 = ^TSPFR_ARTICLE_CODE2;


  TSPFR_ARTICLE_NAME = packed record
    s: array[0..34] of char;
  end;
  PSPFR_ARTICLE_NAME = ^TSPFR_ARTICLE_NAME;

  TSPFR_ARTICLE_NAME2 = packed record
    s: array[0..40] of char;
  end;
  PSPFR_ARTICLE_NAME2 = ^TSPFR_ARTICLE_NAME2;


  TSPFR_ERROR_MESSAGE = packed record
    s: array[0..511] of char;
  end;
  PSPFR_ERROR_MESSAGE = ^TSPFR_ERROR_MESSAGE;

  TSPFR_DISCOUNT_NAME = packed record
    s: array[0..32] of char;
  end;
  PSPFR_DISCOUNT_NAME = ^TSPFR_DISCOUNT_NAME;

  TSPFR_DISCOUNT_NAME2 = packed record
    s: array[0..40] of char;
  end;
  PSPFR_DISCOUNT_NAME2 = ^TSPFR_DISCOUNT_NAME2;

  TSPFR_EXTRA_NAME = TSPFR_DISCOUNT_NAME;
  PSPFR_EXTRA_NAME = ^TSPFR_EXTRA_NAME;

  TSPFR_EXTRA_NAME2 = TSPFR_DISCOUNT_NAME2;
  PSPFR_EXTRA_NAME2 = ^TSPFR_EXTRA_NAME2;

  TSPFR_OPERATOR_NAME = packed record
    s: array[0..20] of char;
  end;
  PSPFR_OPERATOR_NAME = ^TSPFR_OPERATOR_NAME;

  TSPFR_PAYMENT_NAME = packed record
    s: array[0..21] of char;
  end;
  PSPFR_PAYMENT_NAME = ^TSPFR_PAYMENT_NAME;

  TSPFR_BANKNOTE_NAME = packed record
    s: array[0..22] of char;
  end;
  PSPFR_BANKNOTE_NAME = ^TSPFR_BANKNOTE_NAME;

  TSPFR_STRING = packed record
    s: array[0..40] of char;
  end;
  PSPFR_STRING = ^TSPFR_STRING;

  TSPFR_STRINGEX = packed record
    s: array[0..56] of char;
  end;
  PSPFR_STRINGEX = ^TSPFR_STRINGEX;


  TSPFR_LOGO_NAME = packed record
    s: array[0..255] of char;
  end;
  PSPFR_LOGO_NAME = ^TSPFR_LOGO_NAME;

  TSPFRDT = packed record
    day         : BYTE;
    month       : BYTE;
    year        : WORD;
  end;
  PSPFRDT = ^TSPFRDT;

  TSPFRTM = packed record
    hour        : BYTE;
    minute      : BYTE;
    second      : BYTE;
  end;
  PSPFRTM = ^TSPFRTM;

  TSPFR_STATUS = packed record
    status: BYTE;
  end;
  PSPFR_STATUS = ^TSPFR_STATUS;

  TSPFR_FATAL_STATUS = packed record
    status: BYTE;
  end;
  PSPFR_FATAL_STATUS = ^TSPFR_FATAL_STATUS;

  TSPFR_REC_STATUS = packed record
    status: BYTE;
  end;
  PSPFR_REC_STATUS = ^TSPFR_REC_STATUS;





function SPFR_GetErrorMessageRU(hFR: THandle; wError: WORD; mErrorMessage: PSPFR_ERROR_MESSAGE): boolean;  stdcall; external 'sp101fr.DLL';

procedure SPFR_ReleasePort(hFR: THandle); stdcall; external 'sp101fr.DLL';


function SPFR_CheckActive(hFR: THandle; var IsActive: longbool): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_GetStatus(hFR             : THandle;
		        sFatalStatus    : PSPFR_FATAL_STATUS;
			sStatus         : PSPFR_STATUS;
			sReceiptStatus  : PSPFR_REC_STATUS): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_CheckPort(APortNum: integer; var AError: BYTE): THandle; stdcall; external 'sp101fr.DLL';

function SPFR_Init(hFR: Thandle; dtCheckDate: PSPFRDT; tmCheckTime: PSPFRTM): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_Register(hFR              : THandle;
        	       pOldPassword     : PSPFR_PASSWORD;
		       rRegNumber       : PSPFR_REGISTRATION_NUMBER;
		       iINN             : PSPFR_INN;
		       pNewPassword     : PSPFR_PASSWORD): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_ActivateEEJ(hFR: THandle): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_CloseEEJArchive(hFR: THandle): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_OpenReceipt(hFR           : THandle;
			  wReceiptType  : WORD;
			  wSectionNumber: WORD;
			  nOperatorName : PSPFR_OPERATOR_NAME;
			  lReceiptNumber: DWORD): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_PrintString(hFR           : THandle;
                          sBuffer       : PSPFR_STRING): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_PrintStringEx(hFR           : THandle;
                            sBuffer       : PSPFR_STRING;
                            bAttr       : BYTE
                            ): WORD; stdcall; external 'sp101fr.DLL';


function SPFR_CloseReceipt(hFR: THandle): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_BreakReceipt(hFR: THandle): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_AddArticle(hFR                    : THandle;
 			 nArticleName           : PSPFR_ARTICLE_NAME;
  			 cArticleCode           : PSPFR_ARTICLE_CODE;
                         dArticleQty            : currency;
                         dArticleTotal          : currency;
			 wDepartment            : WORD;
			 var dpReceiptTotal     : currency{currency}): WORD; stdcall; external 'sp101fr.DLL';


function SPFR_StornoArticle(hFR                 : THandle;
                            nArticleName        : PSPFR_ARTICLE_NAME;
                            cArticleCode        : PSPFR_ARTICLE_CODE;
                            dArticleQty         : currency;
                            dArticleTotal       : currency;
                            wDepartment         : WORD;
                            var dpReceiptTotal  : currency): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_DiscountPercent(hFR               : THandle;
                              nDiscountName     : PSPFR_DISCOUNT_NAME;
                              dPercent          : currency;
                              var dpArticleTotal: currency): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_DiscountPercent2(hFR               : THandle;
                              nDiscountName     : PSPFR_DISCOUNT_NAME2;
                              dPercent          : currency;
                              var dpArticleTotal: currency): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_DiscountTotal(hFR                 : THandle;
                            nDiscountName       : PSPFR_DISCOUNT_NAME;
                            dTotal              : currency;
                            var dpArticleTotal  : currency): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_DiscountTotal2(hFR                 : THandle;
                            nDiscountName       : PSPFR_DISCOUNT_NAME2;
                            dTotal              : currency;
                            var dpArticleTotal  : currency): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_ExtraPercent(hFR                  : THandle;
                           nExtraName           : PSPFR_EXTRA_NAME;
                           dPercent             : currency;
                           var dpArticleTotal   : currency): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_ExtraPercent2(hFR                  : THandle;
                           nExtraName           : PSPFR_EXTRA_NAME2;
                           dPercent             : currency;
                           var dpArticleTotal   : currency): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_ExtraTotal(hFR                    : THandle;
                         nExtraName             : PSPFR_EXTRA_NAME;
                         dTotal                 : currency;
                         var dpArticleTotal     : currency): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_ExtraTotal2(hFR                    : THandle;
                         nExtraName             : PSPFR_EXTRA_NAME2;
                         dTotal                 : currency;
                         var dpArticleTotal     : currency): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_SubTotal(hFR                      : THandle;
                       var dpReceiptTotal       : currency): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_Payment(hFR               : THandle;
                      wPaymentType      : WORD;
                      dPaymentTotal     : currency;
                      var dpReceiptTotal    : currency): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_CashInOut(hFR                     : THandle;
                        nBanknoteName           : PSPFR_BANKNOTE_NAME;
                        dTotal                  : currency;
                        var dpReceiptTotal      : currency): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_XReport(hFR               : THandle;
                      nOperatorName     : PSPFR_OPERATOR_NAME): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_ZReport(hFR               : THandle;
                      nOperatorName     : PSPFR_OPERATOR_NAME): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_FiscShiftReport(hFR               : THandle;
                              bIsFull           : boolean;
                              wFirstShift       : WORD;
                              wLastShift        : WORD;
                              pFiscPassword     : PSPFR_PASSWORD): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_FiscDateReport(hFR                : THandle;
                             bIsFull            : boolean;
                             dtFirstDate        : PSPFRDT;
                             dtLastDate         : PSPFRDT;
                             pFiscPassword      : PSPFR_PASSWORD): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_EEJJournal(hFR            : THandle;
                         wShiftNumber   : WORD): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_GetReceipt(hFR            : THandle;
                         dwKPKNumber: DWORD): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_EEJShiftReport(hFR                : THandle;
                             bIsFull            : boolean;
                             wFirstShift        : WORD;
                             wLastShift         : WORD): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_EEJDateReport(hFR                 : THandle;
                            bIsFull             : boolean;
                            dtFirstDate         : PSPFRDT;
                            dtLastDate          : PSPFRDT): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_EEJActivationReport(hFR: THandle): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_EEJReport(hFR             : THandle;
                        wShiftNumber    : WORD): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_ReadConfig(hFR: THandle; i: WORD; j: WORD; sParam: PSPFR_STRING): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_WriteConfig(hFR: THandle; i: WORD; j: WORD; sParam: PSPFR_STRING): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_SetDate(hFR       : THandle;
                      dtNewDate : PSPFRDT;
                      tmNewTime : PSPFRTM): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_GetEEJStatus(hFR: THandle; var aStatus: BYTE): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_GetCurrentShift(hFR             : THandle;
                              var wShiftNumber: WORD): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_GetLastRegShift(hFR             : THandle;
			      var wShiftNumber: WORD): WORD; stdcall; external 'sp101fr.DLL';


function SPFR_GetRegNumber(hFR          : THandle;
                           nRegNumber   : PSPFR_REGISTRATION_NUMBER): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_OpenCashDrawer(hFR: THandle): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_Install(hFR               : THandle;
                      dtNewDate         : PSPFRDT;
                      dtNewTime         : PSPFRTM;
                      nSerialNumber     : PSPFR_SERIAL_NUMBER): WORD; stdcall; external 'sp101fr.DLL';


function SPFR_ReadMemBlock(hFR          : THandle;
                           sdaDataType  : TSPRF_DATA_AREA;
                           wByteOffset  : WORD;
                           wByteCount   : WORD;
                           pbData       : Pointer): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_LoadLogo(hFR              : THandle;
                       wByteCount       : WORD;
                       pbData           : Pointer): WORD; stdcall; external 'sp101fr.DLL';


function SPFR_LoadLogoFromFile(hFR              : THandle;
                               nFileName        : PSPFR_LOGO_NAME): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_CancelReport(hFR: THandle): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_AddBarcode(hFR             : THandle;
                         bBarcode        : PSPFR_BARCODE): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_CutAndPrint(hFR: THandle): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_EEJEmergencyShiftClose(hFR: THandle): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_EEJShowClientDisplayText(hFR              : THandle;
                                       tRow1: PSPFR_CLIENTDISPLAYTEXT;
                                       tRow2: PSPFR_CLIENTDISPLAYTEXT): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_ClearLogo(hFR: THandle): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_GetCurrentDocNum(hFR             : THandle;
                              var wDocNum: WORD): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_GetCurrentReceiptNum(hFR             : THandle;
                              var wReceiptNum: WORD): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_GetCurrentReportNum(hFR             : THandle;
                              var wReportNum: WORD): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_GetCashInDrawer(hFR             : THandle;
                              var dCashSum: currency): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_GetSumByCashtype(hFR            : THandle;
                              wCashType       : WORD;
                              var dCashSum    : currency): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_GetRebookSumByCashtype(hFR            : THandle;
                              wCashType       : WORD;
                              var dRebookSum    : currency): WORD; stdcall; external 'sp101fr.DLL';

function  SPFR_GetCurrentDateTime(hFR: THandle; dtDate: PSPFRDT; tmTime: PSPFRTM): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_GetRebookSumForAllPayments(hFR: THandle; sSums: PSPFR_SUMS): WORD; stdcall; external 'sp101fr.DLL';
function SPFR_GetSumForAllPayments(hFR: THandle; sSums: PSPFR_SUMS): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_AddArticleEx(hFR                    : THandle;
                   			 nArticleName           : PSPFR_ARTICLE_NAME;
                  			 cArticleCode           : PSPFR_ARTICLE_CODE;
                         dArticleQty            : double;
                         dArticleTotal          : currency;
                  			 wDepartment            : WORD;
                  			 wTaxNumber             : WORD;
                  			 var dpReceiptTotal     : currency): WORD; stdcall; external 'sp101fr.DLL';
function SPFR_StornoArticleEx(hFR                    : THandle;
                   			 nArticleName           : PSPFR_ARTICLE_NAME;
                  			 cArticleCode           : PSPFR_ARTICLE_CODE;
                         dArticleQty            : double;
                         dArticleTotal          : currency;
                  			 wDepartment            : WORD;
                  			 wTaxNumber             : WORD;
                  			 var dpReceiptTotal     : currency): WORD; stdcall; external 'sp101fr.DLL';


function SPFR_AddArticleEx2(hFR                    : THandle;
                   			 nArticleName           : PSPFR_ARTICLE_NAME2;
                  			 cArticleCode           : PSPFR_ARTICLE_CODE2;
                         dArticleQty            : double;
                         dArticleTotal          : currency;
                  			 wDepartment            : WORD;
                  			 wTaxNumber             : WORD;
                  			 var dpReceiptTotal     : currency): WORD; stdcall; external 'sp101fr.DLL';
function SPFR_StornoArticleEx2(hFR                    : THandle;
                   			 nArticleName           : PSPFR_ARTICLE_NAME2;
                  			 cArticleCode           : PSPFR_ARTICLE_CODE2;
                         dArticleQty            : double;
                         dArticleTotal          : currency;
                  			 wDepartment            : WORD;
                  			 wTaxNumber             : WORD;
                  			 var dpReceiptTotal     : currency): WORD; stdcall; external 'sp101fr.DLL';


function SPFR_ReprintReceipt(hFR: THandle): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_SetTaxAmount(hFR: THandle; wTaxNumber: WORD; dTaxAmount: currency): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_GetINN(hFR          : THandle;
                     nINN   : PSPFR_INN): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_GetFiscNumber(hFR          : THandle;
                            nFiscNum   : PSPFR_REGISTRATION_NUMBER): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_GetEEJNumber(hFR          : THandle;
                            nEEJNum   : PSPFR_EEJ_NUMBER): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_CheckPortEx(APortNum: integer; BaudRate: DWORD; var AError: BYTE): THandle; stdcall; external 'sp101fr.DLL';

function SPFR_PrintStringExWide(hFR       : THandle;
                            sBuffer       : PSPFR_STRINGEX;
                            bAttr       : BYTE
                            ): WORD; stdcall; external 'sp101fr.DLL';




procedure SPFR_SetTriesNumber(wTriesNumber: WORD); stdcall; external 'sp101fr.DLL';

function SPFR_PaymentWithComment(hFR               : THandle;
                                  wPaymentType      : WORD;
                                  dPaymentTotal     : currency;
                                  sComment: PSPFR_STRING;
                                  var dpReceiptTotal    : currency): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_CloseReceiptEx(hFR: THandle; aDoCut: boolean): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_SetBaudrate(hFR: THandle; lBaudRate: DWORD): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_StoreReceipt(hFR: THandle; sText: PSPFR_STRING): WORD; stdcall; external 'sp101fr.DLL';




function SPFR_GetCurrentReceiptSums(hFR: THandle; sSums: PSPFR_CURRENTRECEIPTSUMS): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_GetReceiptCounters(hFR: THandle; sCounters: PSPFR_RECEIPTCOUNTERS): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_GetReceiptsSums(hFR: THandle; sSums: PSPFR_RECEIPTSSUMS): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_GetDiscountAndExtraSums(hFR: THandle; sSums: PSPFR_DISCOUNTANDEXTRASUMS): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_GetSellTaxSums(hFR: THandle; sSums: PSPFR_TAXSUMS): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_GetRebookTaxSums(hFR: THandle; sSums: PSPFR_TAXSUMS): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_GetSellPaymentsCount(hFR: THandle; sPaymentCounts: PSPFR_PAYMENTCOUNTS): WORD; stdcall; external 'sp101fr.DLL';

function SPFR_GetRebookPaymentsCount(hFR: THandle; sPaymentCounts: PSPFR_PAYMENTCOUNTS): WORD; stdcall; external 'sp101fr.DLL';


implementation

end.
