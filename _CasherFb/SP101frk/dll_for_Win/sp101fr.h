/* ===========================================================
 * Fiscal registrator SP101FR-K interface library
 * Version 1.0
 * Copyright (c) 2003-2004 Service Plus AT.  All rights
 * reserved.
 * ===========================================================
*/

#ifndef SP101FR_H
#define SP101FR_H
#endif

#ifdef SP101FR_EXPORTS
#define SP101FR_API __declspec(dllexport)  
#else
#define SP101FR_API __declspec(dllimport) 
#endif

/**
 * @defgroup sp101fr functions return values
 * @{
 */
#define SPFRE_OK					 0	//no error
								//IO errors
#define SPFRE_STATUS				 1	//couldn't execute function, invalid status 
#define SPFRE_INVALID_FUNCTION       2	//invalid function code
#define SPFRE_INVALID_PARAMETER      3	//invalid parameter
								//protocol errors
#define SPFRE_UART_OVER              4	//port buffer overflow
#define SPFRE_INTERCHAR_TIMEOUT      5	//data sending timeout
#define SPFRE_INVALID_PASSWORD       6	//invalid password in command
#define SPFRE_INVALID_CHECKSUM       7	//CRC error
								//printer errors
#define SPFRE_PAPER_END              8	//paper end
#define SPFRE_PRINTER_NOTREADY       9	//printer not ready
								//date & time errors
#define SPFRE_SHIFT_TIME_OVER        10	//current shift is longer than 24h
#define SPFRE_SYNC_TIME_OVER         11	//time sync error
#define SPFRE_TIME_LOW               12	//
#define SPFRE_NO_HEADER              13	//document header not found
#define SPFRE_FALSE					 14	//erroneous result
								//fatal errors
#define SPFRE_DATA_SEND				 0x10	//couldn't send data
#define	SPFRE_NODATA				 0x11	//no data from FR
#define	SPFRE_PORT_INACCESSIBLE		 0x12	//port inaccessible
#define	SPFRE_NOREGISTRATOR			 0x13	//FR not found
		

#define SPFRE_FATAL					 0x20	//fatal error
#define SPFRE_NO_FREE_SPACE			 0x21	//memory low
#define SPFRE_DEVICE_TIMEOUT		 0x30	//device timeout
#define SPFRE_BUSY					 0x40	//device busy
								//      EEJ errors
#define SPFRE_EEJ1					 0x41	//incorrect command or parameter
#define SPFRE_EEJ2					 0x42	//invalid EEJ state
#define SPFRE_EEJ3					 0x43	//EEJ crash
#define SPFRE_EEJ4					 0x44	//EEJ crypto coprocessor crash
#define SPFRE_EEJ5					 0x45	//EEJ using time deadline
#define SPFRE_EEJ6					 0x46	//EEJ overflowed
#define SPFRE_EEJ7					 0x47	//invalid date or time
#define SPFRE_EEJ8					 0x48	//requested data not found
#define SPFRE_EEJ9					 0x49	//overflow
#define SPFRE_EEJA					 0x4A	//no response
#define SPFRE_EEJB					 0x4B	//EEJ data eschange error
/** @} */

/**
 * @defgroup sp101fr types definitions
 * @{
 */
typedef unsigned short USHORT;
typedef unsigned long  ULONG;
typedef unsigned char  BYTE;
typedef LONGLONG FRCURRENCY;



enum  SPRF_DATA_AREA {
	arBIOS, //BIOS
	arDATA, //Data area
	arRAM	//RAM area
};



enum  BARCODE_TYPE {
	UPCA = 0,		//UPC-A
	UPCE,			//UPC-E
	EAN13,			//EAN-13
	EAN8,			//EAN-8
	Code39,			//Code-39
	Interleaved,	//Interleaved 2 of 5
	Codabar			//Codabar
};

enum BARCODE_WIDTH
{
	bw2 = 0,		//width = 2
	bw3 = 1,		//width = 3
	bw4 = 2			//width = 4
};

enum BARCODE_ASDIGIT
{
	NO_DIGIT = 0,	//do dot print digits in barcode
	DIGIT_UP,		//print digits at the top
	DIGIT_DOWN,		//print digits at the bottom
	DIGIT_UPDOWN	//print digits at the top and at the bottom
};


typedef struct SPFR_SUMS {
  FRCURRENCY  sums[16];  
} SPFR_SUMS;



typedef struct SPFR_CURRENTRECEIPTSUMS {
  FRCURRENCY  ReceiptSum;  
  FRCURRENCY  ReceiptDiscountSum;
  FRCURRENCY  ReceiptExtraSum;
} SPFR_CURRENTRECEIPTSUMS;


typedef struct SPFR_RECEIPTCOUNTERS {
  long SellReceipts;
  long RebookReceipts;
  long CancelledReceipts;
  long StoredReceipts;
  long DepositReceipts;
  long TakingReceipts;
} SPFR_RECEIPTCOUNTERS;


typedef struct SPFR_RECEIPTSSUMS {
  FRCURRENCY  CancelledReceiptsSum;  
  FRCURRENCY  StoredReceiptsSum;
  FRCURRENCY  DepositReceiptsSum;
  FRCURRENCY  TakingReceiptsSum;
} SPFR_RECEIPTSSUMS;

typedef struct SPFR_DISCOUNTANDEXTRASUMS {
  FRCURRENCY  SellDiscountSum;  
  FRCURRENCY  SellExtraSum;
  FRCURRENCY  RebookDiscountSum;
  FRCURRENCY  RebookExtraSum;
} SPFR_DISCOUNTANDEXTRASUMS;


typedef struct SPFR_TAXSUMS {
  FRCURRENCY  sums[6];  
} SPFR_TAXSUMS;


typedef struct SPFR_PAYMENTCOUNTS {
  long  paymentcounts[16];  
} SPFR_PAYMENTCOUNTS;





/** EEJ number structure */
typedef struct SPFR_EEJ_NUMBER {
	char s[17];					
} SPFR_EEJ_NUMBER;


/** Barcode structure */
typedef struct SPFR_BARCODE {
  char  s[41];
  BARCODE_TYPE		BarcodeType;	//Barcode type
  BARCODE_ASDIGIT	BarcodeAsDigit;	//Digits in barcode
  BARCODE_WIDTH		BarcodeWidth;	//Width of barcode
  BYTE				BarcodeHeight;  //Height of barcode
} SPFR_BARCODE;

/** Text to print on client display structure */
typedef struct SPFR_CLIENTDISPLAYTEXT {
  char  s[21];					//Text data
} SPFR_CLIENTDISPLAYTEXT;

/** INN structure */
typedef struct SPFR_INN {
  char  s[13];					//INN data
} SPFR_INN;

/** FR registration number structure */
typedef struct SPFR_REGISTRATION_NUMBER {
  char  s[13];					//Registration number
} SPFR_REGISTRATION_NUMBER;

/** FR serial number structure */
typedef struct SPFR_SERIAL_NUMBER {
  char  s[13];					//Serial number
} SPFR_SERIAL_NUMBER;

/** Password structure */
typedef struct SPFR_PASSWORD {
  char  s[11];					//Password
} SPFR_PASSWORD;

/** Article code structure */
typedef struct SPFR_ARTICLE_CODE {
  char  s[14];					//Article code data    18
} SPFR_ARTICLE_CODE;


typedef struct SPFR_ARTICLE_CODE2 {
	char  s[19];					//Article code data    18
} SPFR_ARTICLE_CODE2;



/** Article name structure */
typedef struct SPFR_ARTICLE_NAME {
  char  s[35];					//Article name
} SPFR_ARTICLE_NAME;

/** Second article name structure */
typedef struct SPFR_ARTICLE_NAME2 {
	char  s[41];					//Article name
} SPFR_ARTICLE_NAME2;


/** Discount name structure */
typedef struct SPFR_DISCOUNT_NAME {
  char  s[33];					//Discount name
} SPFR_DISCOUNT_NAME;
typedef SPFR_DISCOUNT_NAME  SPFR_EXTRA_NAME;


/** Discount name structure */
typedef struct SPFR_DISCOUNT_NAME2 {
	char  s[41];					//Discount name
} SPFR_DISCOUNT_NAME2;
typedef SPFR_DISCOUNT_NAME2  SPFR_EXTRA_NAME2;


/** Operator name structure */
typedef struct SPFR_OPERATOR_NAME {
	char s[21];					//Operator name
} SPFR_OPERATOR_NAME;

/** Payment name structure */
typedef struct SPFR_PAYMENT_NAME {
  char  s[22];					//Payment name
} SPFR_PAYMENT_NAME;

/** Banknote name structure */
typedef struct SPFR_BANKNOTE_NAME {
  char  s[23];					//Banknote name
} SPFR_BANKNOTE_NAME;


/** FR string structure */
typedef struct SPFR_STRING {
  char  s[41];					//FR string
} SPFR_STRING;

typedef struct SPFR_STRINGEX {
  char  s[57];					//FR string
} SPFR_STRINGEX;

/** FR error message structure */
typedef struct SPFR_ERROR_MESSAGE {
  char  s[512];					//Error message
} SPFR_ERROR_MESSAGE;

/** Name of logotype file structure */
typedef struct SPFR_LOGO_NAME {
  char  s[256];					//Logotype file name
} SPFR_LOGO_NAME;

/** FR date structure */
typedef struct {
  unsigned char		day;		/*day,	1-31 */
  unsigned char		month;		/*month,1-12 */
  unsigned short	year;		/*year,	1980-2078 */
} SPFRDT;

/** FR time structure */
typedef struct {
  unsigned char hour;			/*hour,		0-23 */
  unsigned char minute;			/*minute,	0-59 */
  unsigned char second;			/*year,		0-59 */
} SPFRTM;


/** FR flags status */
typedef union {
	struct {
		unsigned no_init:1;			//not initialized
		unsigned no_fiscal:1;		//nonfiscal mode
		unsigned shift_opened:1;	//shift opened
		unsigned time_over:1;		//shift opened for more than 24h
		unsigned eej_arc_closed:1;	//EEJ archive closed
		unsigned eej_not_active:1;	//EEJ not active
		unsigned fm_full:1;			//No memory to close shift
		unsigned fm_errpass:1;		//Wrong password
	}			flags;
	BYTE	status;					//Flags as byte
} SPFR_STATUS;


/** FR fatal state status */
typedef union {
	struct {
		unsigned nvr_error:1;		//NVR checksum error
		unsigned config_crc_error:1;//Configuration checksum error
		unsigned spi_error:1;		//SPI error
		unsigned fm_crc_error:1;	//Fiscal memory checksum error
		unsigned fm_write_error:1;	//Fiscal memory writing error
		unsigned not_installed:1;	//Fiscal module not installed
		unsigned eej_error:1;		//EEJ fatal error
	}			flags;
	BYTE	status;					//fatal status as byte
} SPFR_FATAL_STATUS;

/** Current document status */
typedef union {
	struct {
		unsigned type:4;			/*Current document type
									0	Document closed
									1	Nonfiscal document
									2	Receipt (selling)
									3	Receipt (repayment)
									4	Cash payment
									5	Incassation									
									*/
		unsigned mode:4;			/*Current document state
									0	Document closed
									1	New document command issued
									2	Subtotal command issued
									3	Payment happens
									4	Document completed - to be closed												
									*/
	}			flags;				//document status as byte
	BYTE	status;
} SPFR_REC_STATUS;

/** @} */


/**
 * @defgroup sp101fr functions
 * @{
 */
extern SP101FR_API USHORT SPFRTMOUT; //timeout

/**
 * Releases FR handle
 * @param hFr		FR handle (IN)
 */
SP101FR_API void __stdcall SPFR_ReleasePort(HANDLE hFR);

/**
 * Checks if any FR connected to com-port
 * @param hFr		FR handle (IN)
 * @param IsActive  TRUE if FR presents FALSE otherwise (OUT)
 */
SP101FR_API USHORT __stdcall SPFR_CheckActive(HANDLE hFR, LPBOOL IsActive);


/**
 * Gets status bytes from FR
 * @param hFr				FR handle (IN)
 * @param __FatalStatus		byte of fatal status (OUT)
 * @param __Status			flags status (OUT)
 * @param __ReceiptStatus	document status (OUT)
 */
SP101FR_API USHORT __stdcall SPFR_GetStatus(HANDLE hFR,	
								  SPFR_FATAL_STATUS* sFatalStatus,
								  SPFR_STATUS*       sStatus,
								  SPFR_REC_STATUS*   sReceiptStatus);

/**
 * Connects to FR
 * @param PortNum COM-port number (IN)
 * @param bError  error code (OUT)
 */
SP101FR_API HANDLE __stdcall SPFR_CheckPort(int PortNum, LPBYTE bError);

/**
 * Initializes FR
 * @param hFr			FR handle (IN)
 * @param dtCheckDate	Current date (IN)
 * @param tmCheckTime	Current time code (OUT)
 */
SP101FR_API USHORT __stdcall SPFR_Init(HANDLE hFR, SPFRDT* dtCheckDate, SPFRTM* tmCheckTime);

/**
 * FR fiscalization
 * @param hFr			FR handle (IN)
 * @param pOldPassword	Current password of tax official (IN)
 * @param rRegNumber	Registration number (IN)
 * @param iINN			INN number (IN)
 * @param pNewPassword	New password of tax official(IN)
 */
SP101FR_API USHORT __stdcall SPFR_Register(HANDLE					   hFR,
								 SPFR_PASSWORD*			   pOldPassword,
								 SPFR_REGISTRATION_NUMBER* rRegNumber,
								 SPFR_INN*                 iINN,
								 SPFR_PASSWORD*            pNewPassword);
/**
 * Activates EJJ
 * @param hFr			FR handle (IN)
 */
SP101FR_API USHORT __stdcall SPFR_ActivateEEJ(HANDLE hFR);

/**
 * Closes EJJ archive
 * @param hFr			FR handle (IN)
 */
SP101FR_API USHORT __stdcall SPFR_CloseEEJArchive(HANDLE hFR);

/**
 * Opens new receipt and changes FR mode to input mode
 * @param hFr				FR handle (IN)
 * @param wReceiptType		Document type code (IN)
 * @param wSectionNumber	Section number (IN)
 * @param nOperatorName		Operator name or operator code (look up documentation) (IN)
 * @param lReceiptNumber	Document number or 0 if autonumeration enabled(IN)
 */
SP101FR_API USHORT __stdcall SPFR_OpenReceipt(HANDLE				hFR,
									USHORT				wReceiptType,
									USHORT              wSectionNumber,
									SPFR_OPERATOR_NAME* nOperatorName,
									ULONG               lReceiptNumber);
/**
 * Prints supplementary text info
 * @param hFr				FR handle (IN)
 * @param lRecesBuffer		Buffer containing string to print on(IN)
 */
SP101FR_API USHORT __stdcall SPFR_PrintString(HANDLE		  hFR,
									SPFR_STRING*  sBuffer);

/**
 * Closes current receipt
 * @param hFr				FR handle (IN)
 */
SP101FR_API USHORT __stdcall SPFR_CloseReceipt(HANDLE	hFR);

/**
 * Cancels current receipt
 * @param hFr				FR handle (IN)
 */
SP101FR_API USHORT __stdcall SPFR_BreakReceipt(HANDLE	hFR);

/**
 * Inserts new article into document
 * @param hFr				FR handle (IN)
 * @param nArticleName		Commodity name (IN)
 * @param cArticleCode		Article (IN)
 * @param dArticleQty		Commodity quantity (IN)
 * @param dArticleTotal		Price of commodity (IN)
 * @param wDepartment		Department (section) number, 0 if not to print (IN)
 * @param dpReceiptTotal	Total price of curent article (OUT)
 */
SP101FR_API USHORT __stdcall SPFR_AddArticle(HANDLE	hFR,
								   SPFR_ARTICLE_NAME*	nArticleName,
								   SPFR_ARTICLE_CODE*	cArticleCode,								   
								   FRCURRENCY			dArticleQty,
								   FRCURRENCY			dArticleTotal,								   
								   USHORT				wDepartment,
								   FRCURRENCY*			dpReceiptTotal);

/**
 * Reverses an article entry
 * @param hFr				FR handle (IN)
 * @param nArticleName		Commodity name (IN)
 * @param cArticleCode		Article or barcode (IN)
 * @param dArticleQty		Commodity quantity (IN)
 * @param dArticleTotal		Price of commodity (IN)
 * @param wDepartment		Department (section) number, 0 if not to print (IN)
 * @param dpReceiptTotal	Total price of receipt (OUT)
 */
SP101FR_API USHORT __stdcall SPFR_StornoArticle(HANDLE	hFR,
									  SPFR_ARTICLE_NAME*  nArticleName,
									  SPFR_ARTICLE_CODE* cArticleCode,
									  FRCURRENCY         dArticleQty,
									  FRCURRENCY         dArticleTotal,
									  USHORT             wDepartment,
									  FRCURRENCY*        dpReceiptTotal);

/**
 * Makes a percent discount on article/receipt
 * @param hFr				FR handle (IN)
 * @param nDiscountName		Discount name (IN)
 * @param dPercent			Discount percent (IN)
 * @param dpArticleTotal	Total price of curent article (OUT)
 */
SP101FR_API USHORT __stdcall SPFR_DiscountPercent(HANDLE	hFR,
										SPFR_DISCOUNT_NAME*  nDiscountName,
										FRCURRENCY           dPercent,
										FRCURRENCY*          dpArticleTotal);

/**
 * Makes a total discount on article/receipt
 * @param hFr				FR handle (IN)
 * @param nDiscountName		Discount name (IN)
 * @param dTotal			Discount total (IN)
 * @param dpArticleTotal	Total price of curent article (OUT)
 */
SP101FR_API USHORT __stdcall SPFR_DiscountTotal(HANDLE			   hFR,
									  SPFR_DISCOUNT_NAME*  nDiscountName,
									  FRCURRENCY           dTotal,
									  FRCURRENCY*          dpArticleTotal);
/**
 * Makes a percent extra charge on article/receipt
 * @param hFr				FR handle (IN)
 * @param nExtraName		charge name (IN)
 * @param dPercent			charge percent (IN)
 * @param dpArticleTotal	Total price of curent article (OUT)
 */

SP101FR_API USHORT __stdcall SPFR_ExtraPercent(HANDLE			   hFR,
									 SPFR_EXTRA_NAME*  nExtraName,
									 FRCURRENCY        dPercent,
									 FRCURRENCY*       dpArticleTotal);
/**
 * Makes a total extra charge on article/receipt
 * @param hFr				FR handle (IN)
 * @param nExtraName		charge name (IN)
 * @param dTotal			charge total (IN)
 * @param dpArticleTotal	Total price of curent article (OUT)
 */
SP101FR_API USHORT __stdcall SPFR_ExtraTotal(HANDLE			 hFR,
								   SPFR_EXTRA_NAME*  nExtraName,
								   FRCURRENCY        dTotal,
								   FRCURRENCY*       dpArticleTotal);

/**
 * Subtotal of receipt
 * @param dpReceiptTotal	Total price of receipt (OUT)
 */
SP101FR_API USHORT __stdcall SPFR_SubTotal(HANDLE			hFR,
								 FRCURRENCY*	dpReceiptTotal);

/**
 * Payment fixation
 * @param hFr				FR handle (IN)
 * @param wPaymentType		Payment type code (IN)
 * @param dPaymentTotal		Total money taken (IN)
 * @param dpReceiptTotal	Total price of receipt (OUT)
 */
SP101FR_API USHORT __stdcall SPFR_Payment(HANDLE			hFR,
								USHORT			wPaymentType,
								FRCURRENCY		dPaymentTotal,
								FRCURRENCY*		dpReceiptTotal);
/**
 * Cash in/out
 * @param hFr				FR handle (IN)
 * @param nBanknoteName		Banknote name (IN)
 * @param dTotal			Total sum of cash deposited/withdrawn (IN)
 * @param dpReceiptTotal	Total sum of cash deposited/withdrawn (OUT)
 */
SP101FR_API USHORT __stdcall SPFR_CashInOut(HANDLE				hFR,
								  SPFR_BANKNOTE_NAME*   nBanknoteName,
								  FRCURRENCY            dTotal,
								  FRCURRENCY*           dpReceiptTotal);

/**
 * Prints X-report
 * @param hFr				FR handle (IN)
 * @param nOperatorName		Operator's code (IN)
 */
SP101FR_API USHORT __stdcall SPFR_XReport(HANDLE				hFR,
								SPFR_OPERATOR_NAME* nOperatorName);

/**
 * Prints Z-report
 * @param hFr				FR handle (IN)
 * @param nOperatorName		Operator's code (IN)
 */
SP101FR_API USHORT __stdcall SPFR_ZReport(HANDLE				hFR,
								SPFR_OPERATOR_NAME* nOperatorName);
/**
 * Prints shift report
 * @param hFr				FR handle (IN)
 * @param bIsFull			True if full format false otherwise (IN)
 * @param wFirstShift		Shift to begin from (IN)
 * @param wLastShift		Shift to end at (IN)
 * @param pFiscPassword		Tax official password (IN)
 */
SP101FR_API USHORT __stdcall SPFR_FiscShiftReport(HANDLE				hFR,
										BOOL			bIsFull,
										USHORT			wFirstShift,
										USHORT			wLastShift,
										SPFR_PASSWORD*	pFiscPassword);

/**
 * Prints date report
 * @param hFr				FR handle (IN)
 * @param bIsFull			True if full format false otherwise (IN)
 * @param dtFirstDate		Beginning date (IN)
 * @param dtLastDate		End date (IN)
 * @param pFiscPassword		Tax official password (IN)
 */
SP101FR_API USHORT __stdcall SPFR_FiscDateReport(HANDLE			hFR,
									   BOOL				bIsFull,
									   SPFRDT*			dtFirstDate,
									   SPFRDT*			dtLastDate,
									   SPFR_PASSWORD*	pFiscPassword);
/**
 * Prints EEJ journal
 * @param hFr				FR handle (IN)
 * @param wShiftNumber		Fiscal shift number (IN)
 */
SP101FR_API USHORT __stdcall SPFR_EEJJournal(HANDLE	hFR,
								   USHORT	wShiftNumber);
/**
 * Prints EEJ document
 * @param hFr				FR handle (IN)
 * @param dwKPKNumber		Document number (IN)
 */
SP101FR_API USHORT __stdcall SPFR_GetReceipt(HANDLE	hFR,
								   ULONG	dwKPKNumber);
/**
 * Prints EEJ shift report
 * @param hFr				FR handle (IN)
 * @param bIsFull			True if full format false otherwise (IN)
 * @param wFirstShift		Shift to begin from (IN)
 * @param wLastShift		Shift to end at (IN)
 */
SP101FR_API USHORT __stdcall SPFR_EEJShiftReport(HANDLE	hFR,
									   BOOL		bIsFull,
									   USHORT	wFirstShift,
									   USHORT	wLastShift);

/**
 * Prints EEJ date report
 * @param hFr				FR handle (IN)
 * @param bIsFull			True if full format false otherwise (IN)
 * @param dtFirstDate		Beginning date (IN)
 * @param dtLastDate		End date (IN)
 */
SP101FR_API USHORT __stdcall SPFR_EEJDateReport(HANDLE	hFR,
									  BOOL		bIsFull,
									  SPFRDT*	dtFirstDate,
									  SPFRDT*	dtLastDate);
/**
 * Prints EEJ activation report
 * @param hFr				FR handle (IN)
 */
SP101FR_API USHORT __stdcall SPFR_EEJActivationReport(HANDLE hFR);

/**
 * Prints EEJ shift report
 * @param hFr				FR handle (IN)
 * @param wShiftNumber		Shift number (IN)
 */
SP101FR_API USHORT __stdcall SPFR_EEJReport(HANDLE	hFR,
								  USHORT	wShiftNumber);

/**
 * Reads FR configuration parameter
 * @param hFr				FR handle (IN)
 * @param i					Row index (IN)
 * @param j					Column index (IN)
 * @param sParam			Parameter value (OUT)
 */
SP101FR_API USHORT __stdcall SPFR_ReadConfig(HANDLE hFR, USHORT i, USHORT j, SPFR_STRING* sParam);

/**
 * Writes FR configuration parameter
 * @param hFr				FR handle (IN)
 * @param i					Row index (IN)
 * @param j					Column index (IN)
 * @param sParam			Parameter value (IN)
 */
SP101FR_API USHORT __stdcall SPFR_WriteConfig(HANDLE hFR, USHORT i, USHORT j, SPFR_STRING* sParam);

/**
 * Sets date and time
 * @param hFr				FR handle (IN)
 * @param dtNewDate			Current date (IN)
 * @param tmNewTime			Current time (IN) 
 */
SP101FR_API USHORT __stdcall SPFR_SetDate(HANDLE hFR,
								SPFRDT* dtNewDate,
								SPFRTM* tmNewTime);
/**
 * Requests EEJ status
 * @param hFr				FR handle (IN)
 * @param EEJStatus			Status byte (OUT)
 */
SP101FR_API USHORT __stdcall SPFR_GetEEJStatus(HANDLE hFR, LPBYTE EEJStatus);

/**
 * Returns current shift number
 * @param hFr				FR handle (IN)
 * @param wShiftNumber		Curent shift number (OUT)
 */
SP101FR_API USHORT __stdcall SPFR_GetCurrentShift(HANDLE hFR,
										USHORT* wShiftNumber);
/**
 * Returns last registration shift number
 * @param hFr				FR handle (IN)
 * @param wShiftNumber		Shift number (OUT)
 */
SP101FR_API USHORT __stdcall SPFR_GetLastRegShift(HANDLE hFR,
										USHORT* wShiftNumber);
/**
 * Returns FR serial number
 * @param hFr				FR handle (IN)
 * @param nRegNumber		Serial number (OUT)
 */
SP101FR_API USHORT __stdcall SPFR_GetRegNumber(HANDLE						hFR,
									 SPFR_REGISTRATION_NUMBER* nRegNumber);

/**
 * Opens cash drawer
 * @param hFr				FR handle (IN)
 */
SP101FR_API USHORT __stdcall SPFR_OpenCashDrawer(HANDLE hFR);

/**
 * FR installing
 * @param hFr				FR handle (IN)
 * @param dtNewDate			Current date (IN)
 * @param tmNewTime			Current time (IN) 
 * @param nSerialNumber		Serial number (IN) 
 */
SP101FR_API USHORT __stdcall SPFR_Install(HANDLE hFR,
								SPFRDT*				dtNewDate,
								SPFRTM*				dtNewTime,
								SPFR_SERIAL_NUMBER* nSerialNumber);
/**
 * Gets memory block dump from FR
 * @param hFr				FR handle (IN)
 * @param sdaDataType		Memory area to read data from (IN)
 * @param wByteOffset		Dump offset (IN) 
 * @param wByteCount		Dump bytes count (64 max) (IN) 
 * @param pbData			Memory block data (OUT) 
 */
SP101FR_API USHORT __stdcall SPFR_ReadMemBlock(HANDLE hFR,
									 SPRF_DATA_AREA sdaDataType,									 
									 USHORT	wByteOffset,
									 USHORT	wByteCount,
									 BYTE*	pbData);
/**
 * Writes logotype into FR
 * @param hFr				FR handle (IN)
 * @param wByteCount		Logotype data bytes count (IN)
 * @param pbData			Logotype data (IN) 
 */
SP101FR_API USHORT __stdcall SPFR_LoadLogo(HANDLE hFR,
								 USHORT	wByteCount,
								 BYTE*	pbData);
/**
 * Writes logotype into FR
 * @param hFr				FR handle (IN)
 * @param nFileName			Logotype file name (IN)
 */
SP101FR_API USHORT __stdcall SPFR_LoadLogoFromFile(HANDLE hFR,
										 SPFR_LOGO_NAME*	nFileName);

/**
 * Interrupts report execution
 * @param hFr				FR handle (IN)
 */
SP101FR_API USHORT __stdcall SPFR_CancelReport(HANDLE hFR);

/**
 * Inserts new barcode into document
 * @param hFr				FR handle (IN)
 * @param bBarcode			Barcode structure (IN)
 */
SP101FR_API USHORT __stdcall SPFR_AddBarcode(HANDLE			hFR,
								   SPFR_BARCODE*	bBarcode);

/**
 * Cuts receipt paper and prints next receiptheader
 * @param hFr				FR handle (IN)
 */
SP101FR_API USHORT __stdcall SPFR_CutAndPrint(HANDLE	hFR);

/**
 * Emergency EJJ shift closing
 * @param hFr				FR handle (IN)
 */
SP101FR_API USHORT __stdcall SPFR_EEJEmergencyShiftClose(HANDLE	hFR);

/**
 * Shows 2 rows of text on client display
 * @param hFr				FR handle (IN)
 * @param tRow1				First row (IN)
 * @param tRow2				Second row (IN)
 */
SP101FR_API USHORT __stdcall SPFR_EEJShowClientDisplayText(HANDLE	hFR,
												 SPFR_CLIENTDISPLAYTEXT* tRow1,
												 SPFR_CLIENTDISPLAYTEXT* tRow2);
/**
 * Clears logotype in FR
 * @param hFr				FR handle (IN)
 */
SP101FR_API USHORT __stdcall SPFR_ClearLogo(HANDLE hFR);

/**
 * Returns error description as a russian language string
 * @param hFr				FR handle (IN)
 * @param wError			Error code (IN)
 * @param mErrorMessage		First row (OUT)
 */
SP101FR_API BOOL __stdcall SPFR_GetErrorMessageRU(HANDLE hFR,
										USHORT				wError,
									    SPFR_ERROR_MESSAGE*	mErrorMessage);


/**
 * Returns current document number
 * @param hFr				FR handle (IN)
 * @param wDocNum			Current document number (OUT)
 */
SP101FR_API USHORT __stdcall SPFR_GetCurrentDocNum(HANDLE hFR,									    
										USHORT* wDocNum);

/**
 * Returns current receipt number
 * @param hFr				FR handle (IN)
 * @param wReceiptNum		Current receipt number (OUT)
 */
SP101FR_API USHORT __stdcall SPFR_GetCurrentReceiptNum(HANDLE hFR,									    
										USHORT* wReceiptNum);

/**
 * Returns current report number
 * @param hFr				FR handle (IN)
 * @param wReportNum		Current report number (OUT)
 */
SP101FR_API USHORT __stdcall SPFR_GetCurrentReportNum(HANDLE hFR,									    
										USHORT* wReportNum);

/**
 * Returns amount of cash in the cashdrawer
 * @param hFr				FR handle (IN)
 * @param dCashSum			Cash in the cashdrawer (OUT)
 */
SP101FR_API USHORT __stdcall SPFR_GetCashInDrawer(HANDLE hFR,									    
										FRCURRENCY* dCashSum);

/**
 * Returns amount of payment by payment type
 * @param hFr				FR handle (IN)
 * @param wCashType			Payment type (IN)
 * @param dCashSum			Cash by givent payment type (OUT)
 */
SP101FR_API USHORT __stdcall SPFR_GetSumByCashtype(HANDLE hFR,
										USHORT wCashType,
										FRCURRENCY* dCashSum);
/**
 * Returns amount of rebook by payment type
 * @param hFr				FR handle (IN)
 * @param wCashType			Payment type (IN)
 * @param dRebookSum		Rebooked by givent payment type (OUT)
 */
SP101FR_API USHORT __stdcall SPFR_GetRebookSumByCashtype(HANDLE hFR,
										USHORT wCashType,
										FRCURRENCY* dRebookSum);

/**
 * Prints string with extended attributes
 * @param hFr				FR handle (IN)
 * @param sBuffer			string to be printed (IN)
 * @param taAttrib			Font attributes (IN)
 */
SP101FR_API USHORT __stdcall SPFR_PrintStringEx(HANDLE		  hFR,
									SPFR_STRING*  sBuffer,
									BYTE bAttr);

SP101FR_API USHORT __stdcall SPFR_GetCurrentDateTime(HANDLE	hFR,  SPFRDT* dtDate, SPFRTM* tmTime);


SP101FR_API USHORT __stdcall SPFR_GetRebookSumForAllPayments(HANDLE hFR,
										           SPFR_SUMS*	sSums);

SP101FR_API USHORT __stdcall SPFR_GetSumForAllPayments(HANDLE hFR,
										           SPFR_SUMS*	sSums);

SP101FR_API USHORT __stdcall SPFR_AddArticleEx(HANDLE	hFR,
								   SPFR_ARTICLE_NAME*	nArticleName,
								   SPFR_ARTICLE_CODE*	cArticleCode,								   
								   double				dArticleQty,
								   FRCURRENCY			dArticleTotal,								   
								   USHORT				wDepartment,
								   USHORT				wTaxNumber,
								   FRCURRENCY*			dpReceiptTotal);

SP101FR_API USHORT __stdcall SPFR_ReprintReceipt(HANDLE hFR);

SP101FR_API USHORT __stdcall SPFR_SetTaxAmount(HANDLE hFR, USHORT wTaxNumber, FRCURRENCY dTaxAmount);

SP101FR_API USHORT __stdcall SPFR_GetINN(HANDLE	hFR, SPFR_INN* nINN);

SP101FR_API USHORT __stdcall SPFR_GetFiscNumber(HANDLE	hFR, SPFR_REGISTRATION_NUMBER* nFiscNum);

SP101FR_API USHORT __stdcall SPFR_GetEEJNumber(HANDLE	hFR, SPFR_EEJ_NUMBER* nEEJNum);

SP101FR_API USHORT __stdcall SPFR_StornoArticleEx(HANDLE	hFR,
								   SPFR_ARTICLE_NAME*	nArticleName,
								   SPFR_ARTICLE_CODE*	cArticleCode,								   
								   double				dArticleQty,
								   FRCURRENCY			dArticleTotal,								   
								   USHORT				wDepartment,
								   USHORT				wTaxNumber,
								   FRCURRENCY*			dpReceiptTotal);


SP101FR_API HANDLE __stdcall SPFR_CheckPortEx(int PortNum, UINT BaudRate, LPBYTE bError);

SP101FR_API USHORT __stdcall SPFR_PrintStringExWide(HANDLE		  hFR,
									SPFR_STRINGEX*  sBuffer,
									BYTE bAttr);



/**
 * Sets number of retries on connection error. Retries are to be done in 100ms interval. Default = 1500
 * @param aTruesNumber				number of retries
 */
SP101FR_API VOID __stdcall SPFR_SetTriesNumber(USHORT wTriesNumber);


SP101FR_API USHORT __stdcall SPFR_PaymentWithComment(HANDLE		hFR,
								USHORT		wPaymentType,
								FRCURRENCY  dPaymentTotal,
								SPFR_STRING* sComment,
								FRCURRENCY*	dpReceiptTotal);

SP101FR_API USHORT __stdcall SPFR_CloseReceiptEx(HANDLE	hFR, bool aDoCut);

SP101FR_API USHORT __stdcall SPFR_SetBaudrate(HANDLE	hFR, ULONG lBaudRate);

SP101FR_API USHORT __stdcall SPFR_StoreReceipt(HANDLE hFR, SPFR_STRING*  sText);

SP101FR_API USHORT __stdcall SPFR_GetCurrentReceiptSums(HANDLE hFR,
										           SPFR_CURRENTRECEIPTSUMS*	sSums);

SP101FR_API USHORT __stdcall SPFR_GetReceiptCounters(HANDLE hFR,
										             SPFR_RECEIPTCOUNTERS*	sCounters);

SP101FR_API USHORT __stdcall SPFR_GetReceiptsSums(HANDLE hFR,
										            SPFR_RECEIPTSSUMS*	sSums);

SP101FR_API USHORT __stdcall SPFR_GetDiscountAndExtraSums(HANDLE hFR,
										            SPFR_DISCOUNTANDEXTRASUMS*	sSums);

SP101FR_API USHORT __stdcall SPFR_GetSellTaxSums(HANDLE hFR,
										           SPFR_TAXSUMS*	sSums);

SP101FR_API USHORT __stdcall SPFR_GetRebookTaxSums(HANDLE hFR,
										           SPFR_TAXSUMS*	sSums);

SP101FR_API USHORT __stdcall SPFR_GetSellPaymentsCount(HANDLE hFR,
										           SPFR_PAYMENTCOUNTS*	sPaymentCounts);

SP101FR_API USHORT __stdcall SPFR_GetRebookPaymentsCount(HANDLE hFR,
										           SPFR_PAYMENTCOUNTS*	sPaymentCounts);

SP101FR_API USHORT __stdcall SPFR_AddArticleEx2(HANDLE	hFR,
											   SPFR_ARTICLE_NAME2*	nArticleName,
											   SPFR_ARTICLE_CODE2*	cArticleCode,								   
											   double				dArticleQty,
											   FRCURRENCY			dArticleTotal,								   
											   USHORT				wDepartment,
											   USHORT				wTaxNumber,
											   FRCURRENCY*			dpReceiptTotal);

SP101FR_API USHORT __stdcall SPFR_StornoArticleEx2(HANDLE	hFR,
												  SPFR_ARTICLE_NAME2*	nArticleName,
												  SPFR_ARTICLE_CODE2*	cArticleCode,								   
												  double				dArticleQty,
												  FRCURRENCY			dArticleTotal,								   
												  USHORT				wDepartment,
												  USHORT				wTaxNumber,
												  FRCURRENCY*			dpReceiptTotal);



SP101FR_API USHORT __stdcall SPFR_DiscountPercent2(HANDLE	hFR,
												  SPFR_DISCOUNT_NAME2*  nDiscountName,
												  FRCURRENCY           dPercent,
												  FRCURRENCY*          dpArticleTotal);

SP101FR_API USHORT __stdcall SPFR_DiscountTotal2(HANDLE			   hFR,
												SPFR_DISCOUNT_NAME2*  nDiscountName,
												FRCURRENCY           dTotal,
												FRCURRENCY*          dpArticleTotal);


SP101FR_API USHORT __stdcall SPFR_ExtraPercent2(HANDLE			   hFR,
											   SPFR_EXTRA_NAME2*  nExtraName,
											   FRCURRENCY        dPercent,
											   FRCURRENCY*       dpArticleTotal);

SP101FR_API USHORT __stdcall SPFR_ExtraTotal2(HANDLE			 hFR,
											 SPFR_EXTRA_NAME2*  nExtraName,
											 FRCURRENCY        dTotal,
											 FRCURRENCY*       dpArticleTotal);

SP101FR_API USHORT __stdcall SPFR_ExtraTotal2(HANDLE			 hFR,
											  SPFR_EXTRA_NAME2*  nExtraName,
											  FRCURRENCY        dTotal,
											  FRCURRENCY*       dpArticleTotal);

SP101FR_API USHORT __stdcall SPFR_GetDrawerOpened(HANDLE      hFR,
											      LPBOOL      IsOpened);


/** @} */