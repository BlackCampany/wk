unit UnCash;

interface
uses
//  ������� ��� ����� 2�

  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  CPort,
  Dialogs, StdCtrls;

Function  InspectSt(Var sMess:String):Integer;           //OK
Function  GetSt(Var sMess:String):INteger;               //OK �� ������������ - ��������
Function  TestStatus(StrOp:String; Var SMess:String):Boolean; //OK

Function  OpenZ:Boolean;         //OK
Function  GetSerial:Boolean;     //OK ��������
Function  GetNums:Boolean;                               //OK � ����� � � ����
Function  GetRes:Boolean;                                //OK �� ������������ - ��������
Function  GetSums(Var rSum1,rSum2,rSum3:Real):Boolean;   //OK �� ������������ - ��������
function  GetRSumZ:Boolean;                    //OK
function  GetRetSumZ:Boolean;                  //OK

Function   CashOpen(sPort:PChar):Boolean;      //Ok
Function   CashClose:Boolean;                  //Ok
Function   CashDate(var sDate:String):Boolean; //OK
Function   GetX:Boolean;                       //OK
Function   ZOpen:Boolean; //������ �����       //OK
Function   ZClose:Boolean;                     //OK
Function   CheckStart:Boolean;                 //Ok
Function   CheckRetStart:Boolean;              //OK
Function   CheckAddPos(Var iSum:Integer):Boolean;   //OK
Function   CheckTotal(Var iTotal:Integer):Boolean;  //OK
Function   CheckDiscount(sDisc:String;rDiscount:Real;Var iItog:Integer):Boolean;  //OK
Function   CheckRasch(iPayType,iClient:Integer; ValName,Comment:String; Var iDopl:Integer):Boolean;  //Ok ����� �� ������������
Function   CheckClose:Boolean;                   //OK
Function   CheckCloseNoCut:Boolean;                   //OK
Procedure  CheckCancel;                          //OK

function  InCass(rSum:Real):Boolean;//����������   //OK

Function  GetCashReg(Var rSum:Real):Boolean;//���������� � �����   //OK  ��� ���������� �� ������� �����

Function    CashDriver:Boolean;                   // 150 �� �� ���������
Procedure   TestPosCh;                            //OK ������������

Function   OpenNFDoc:Boolean;                     //Ok
Function   CloseNFDoc:Boolean;                    //Ok
Function   PrintNFStr(StrPr:String):Boolean;      //Ok
//Function   PrintNFStrFast(StrPr:String):Boolean;      //Ok

Function   SelectF(iNum:Integer):Boolean;         //Ok  ��� ���� � ���������� ������
Function   CutDoc:Boolean;                        //Ok ���� ��������
Function   CutDoc1:Boolean;                       //Ok ���� ��������

procedure   WriteHistoryFR(Strwk_: string);             //Ok  ������������ ���������

function   IsOLEObjectInstalled(Name: String): boolean;  //Ok ������������
function   More24H(Var iSt:INteger):Boolean;             //Ok
Function   CheckOpenFis:Boolean;                         //OK

Function   GetNumCh:Boolean;                             //OK

Function   GetSumDisc(Var rSum1,rSum2,rSum3,rSum4:Real):Boolean; //OK
Function   SetDP(Str1,Str2:String):Boolean;                      //OK ������������ ��������

procedure prBnPrint;

function prLoadLogo:Boolean;                    //OK ��������

Function PrintQR(StrFile:String):Boolean;       //OK


                                                //��� ��� ���� ����������� ������� ��� ������ � �������
function fCRC(StrS:String):string;
function fSendFis(StrS:String;FisPrint:TComPort):string;
function fIncNOP:Integer;
function fFisOpen(StrP:String;FisPrint:TComPort):Boolean;
function fFisPrinter(StrP:String;FisPrint:TComPort):Boolean;
function fTestFis(StrP:String;FisPrint:TComPort):Boolean;
function fChSum(FisPrint:TComPort;Var rSum,dSum,nSum:Real):Boolean;
function fS2F(S:String):Real;
procedure prClearBuf;
function fGetFN:string;


Type
     TStatusFR = record
     St1:String[2];
     St2:String[4];
     St3:String[4];
     St4:String[10];
     end;

     TNums = record
     bOpen:Boolean;
     ZNum:Integer;
     SerNum:String[11];
     RegNum:string[20];
     INN:string[20];
     CheckNum:string[4];
     iCheckNum:Integer;
     iRet:INteger;
     sRet:String;
     sDate:String;
     ZYet:Integer;
     sDateTimeBeg:String;
     sFN:string;
     FNWARN:SmallInt;
     end;

     TCurPos = record
     Articul:String;
     Bar:String;
     Name:String;
     EdIzm:String;
     TypeIzm:INteger;
     Quant:Real;
     Price:Real;
     DProc:Real;
     DSum:Real;
     Depart:SmallInt;
     FisPos:SmallInt;
     NumPos:INteger;
     AVid:Integer;
     Krep:Real;
     AMark:string;
     iNDS:SmallInt;
     end;

var
  CommandsArray : array [1..32000] of Char;
  HeapStatus    : THeapStatus;
  PressCount    : Byte;
  StatusFr      : TStatusFr;
  Nums          : TNums;
  CurPos        :TCurPos;
  SelPos        :TCurPos;
  sMessage      :String;
//  PosCh         : TPos;
  iRfr:SmallInt;
  rSumR,rSumD:Real;

  NumPacket         :Integer;
//  Buffer: array[0..1024] of Char;
  sBuffer:string;
  BufferCount:Integer;

Const iPassw:Integer = 30;

implementation

uses Un1, Dm, u2fdk,MainCasher;

function fGetFN:string;
Var StrP,StrS,sRes,StrWk:string;
    StrPar:string;
    iF0,iF1,iF2:Integer;
    sDate,sDateCur:string;
    sErr:String;
    bErr:Boolean;
begin
  result:='';
  Nums.sFN:='';
  Nums.FNWARN:=0;
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then begin result:=''; exit; end;

  with dmC do
  begin
    StrP:=FisPrint.Port;
    if fFisOpen(StrP,dmC.FisPrint) then
    begin
      iF0:=0; iF1:=0; iF2:=0; sDate:='';
      StrS:='78'+'7'+#$1C;
      sRes:=fSendFis(StrS,FisPrint); //
      StrWk:=sRes;
      if (Length(sRes)>=20) then
      begin
        if Pos(#$1C,StrWk)>0 then         //#2 x 78007 #1C 2 #1C 2 #1C 1 #1C 070317 #1�
        begin
          Delete(StrWk,1,Pos(#$1C,StrWk));
        end;
        if Pos(#$1C,StrWk)>0 then                    //2 #1C 2 #1C 1 #1C 070317 #1�
        begin
          StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1);
          iF0:=StrToIntDef(Trim(StrPar),0);
          Delete(StrWk,1,Pos(#$1C,StrWk));
        end;
        if Pos(#$1C,StrWk)>0 then
        begin
          StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1);  //2 #1C 1 #1C 070317 #1�
          iF1:=StrToIntDef(Trim(StrPar),0);
          Delete(StrWk,1,Pos(#$1C,StrWk));
        end;
        if Pos(#$1C,StrWk)>0 then
        begin
          StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1);  //1 #1C 070317 #1�
          iF2:=StrToIntDef(Trim(StrPar),0);
          Delete(StrWk,1,Pos(#$1C,StrWk));
        end;
        if Pos(#$1C,StrWk)>0 then
        begin
          StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1);  //1 #1C 070317 #1�
          sDate:=Trim(StrPar);
          Delete(StrWk,1,Pos(#$1C,StrWk));
          if sDate='000000' then sDate:='';
        end;

//        iF1:=3;  ����
//        sDate:='100317'; //����

        if Length(sDate)>=6 then Nums.sFN:='��-'+Copy(sDate,1,2)+'.'+Copy(sDate,3,2)+'.'+Copy(sDate,5,2)+' (� ����.'+its(iF1)+')'
        else Nums.sFN:='�� - (� ����.'+its(iF1)+')';

        if iF1>3 then Nums.FNWARN:=1
        else
        begin
          if sDate>'' then
          begin
            sDateCur:=FormatdateTime('ddmmyy',date);
            if sDate<>sDateCur then Nums.FNWARN:=1;
          end;
        end;

        result:=Nums.sFN;
        //  StrD:=Copy(StrWk,7,2)+'.'+Copy(StrWk,9,2)+'.20'+Copy(StrWk,11,2);
      end;
    end;
  end;
end;

procedure prClearBuf;
begin
  //Fillchar(Buffer, Sizeof(Buffer), 0);
  sBuffer:='';
  BufferCount:=0;
end;


function fS2F(S:String):Real;
begin
  if S>'' then
  begin
    while pos(' ',S)>0 do Delete(S,1,pos(' ',S));
    while pos('.',S)>0 do S[pos('.',S)]:=',';
    Result:=StrToFloatDef(S,0);
  end else Result:=0;
end;

function fChSum(FisPrint:TComPort;Var rSum,dSum,nSum:Real):Boolean;
var StrWk,StrSum,StrS,sRes:string;
begin
  Result:=False;

    rSum:=0;  //����� ����
    dSum:=0;  //����� ������
    nSum:=0;  //����� �������

    if fFisOpen(FisPrint.Port,FisPrint) then
    begin
      StrS:='03'+'1'+#$1C;
      sRes:=fSendFis(StrS,FisPrint);
      StrWk:=sRes;
      if Pos(#$1C,StrWk)>0 then
      begin
        Delete(StrWk,1,Pos(#$1C,StrWk));
        if Pos(#$1C,StrWk)>0 then
        begin
          StrSum:=Copy(StrWk,1,Pos(#$1C,StrWk)-1);
          rSum:=fS2F(StrSum); //����� ����
          Delete(StrWk,1,Pos(#$1C,StrWk));
          if Pos(#$1C,StrWk)>0 then
          begin
            StrSum:=Copy(StrWk,1,Pos(#$1C,StrWk)-1);
            dSum:=fS2F(StrSum); //����� ������
            Delete(StrWk,1,Pos(#$1C,StrWk));
            if Pos(#$1C,StrWk)>0 then
            begin
              StrSum:=Copy(StrWk,1,Pos(#$1C,StrWk)-1);
              nSum:=fS2F(StrSum); //����� �������
              Result:=True;
            end;
          end;
        end;
      end;
    end;

  {
 57:994   �������� ������� 1
01 58:088 /42002A
01 02:295 
01 02:295 ��������� ����
01 02:357 0030010.120.000.001C
01 32:107   �������� ������� 1
01 32:201 1420034
01 33:990
01 34:006 ��������� ����
01 34:024 2030010.130.000.001F
01 46:989   �������� ������� 1
01 47:083 3420036
01 48:499 
01 48:515 ��������� ����
01 48:577 4030010.140.000.001E
01 52:581 
01 52:581 ������� 1
01 52:644 5440036
01 54:661 
01 54:661 ��������� ����
01 54:724 6030010.140.000.001C
01 19:890 
01 19:890 ��������� ����
01 25:835 7030010.140.000.001D
01 30:410 ������������ ���
01 00:288 832003A
  }

end;

function fFisPrinter(StrP:String;FisPrint:TComPort):Boolean;
var sRes:string;
begin
  Result:=False;
  if fFisOpen(StrP,FisPrint) then
  begin
    sRes:=fSendFis('04',FisPrint);
    if Length(sRes)>=7 then
    begin
      if (sRes[6]='0')and(sRes[7]='0') then Result:=True
    end;
  end;
end;

function fTestFis(StrP:String;FisPrint:TComPort):Boolean;
var bRes:Byte;
    n:Integer;
//    Buff:Array[1..3] of Char;
    sBuff:string;
    iC:Integer;
begin
  Result:=False;
  if FisPrint.Connected then
  begin
    prClearBuf;

    sBuff:=#$05;

    FisPrint.WriteStr(sBuff);

    for n:=1 to 1000 do
    begin
      iC:=BufferCount;
      Delay(10);
      if (iC>0) and (iC=BufferCount) then Break; //���-�� ��������� ������������
    end;

    bRes:=0;
    if BufferCount>0 then bRes:=Ord(sBuffer[1]);
    if bRes=$06 then Result:=True;
  end;
end;


function fFisOpen(StrP:String;FisPrint:TComPort):Boolean;
begin
  if FisPrint.Connected then Result:=True
  else
  begin
    try
      {
      FisPrint.Port:=StrP;
      FisPrint.Baudrate:=br57600;
      FisPrint.Parity:=paNone;
      FisPrint.Stopbits:=sb1;
      FisPrint.Databits:=db8;
      FisPrint.Open;
      }
      FisPrint.Port:=StrP;
      FisPrint.Baudrate:=br57600;
//      FisPrint.Parity:=prNone;
      FisPrint.Stopbits:=sbOneStopBit;
      FisPrint.Databits:=dbEight;
      FisPrint.Open;

      Result:=True;
    except
      Result:=False;
    end;
  end;
end;

function fIncNOP;
begin
  inc(NumPacket);
  if NumPacket>239 then NumPacket:=33;
  Result:=NUmPacket;
end;

function fSendFis(StrS:String;FisPrint:TComPort):string;
Var //Buff:Array[1..200] of Char;
    l,n,j:Integer;
    sRes,sLog:string;
    bErr:Boolean;
    NumPackRet:Integer;
//    StrP:string;
    StrSend:string;
    iC:Integer;
begin
  Result:='-';
  try
    if FisPrint.Connected then
    begin
      bErr:=False; //�� ��������� ������ ������ � ������ ���

      fIncNOP; //��������� ������ ������

      prClearBuf;

      StrSend:=#$02+'PIRI'+chr(NumPacket)+StrS+#$03;
      StrSend:=StrSend+fCRC(StrSend);
      sLog:='~~~~~ => '+StrSend+'(';
      l:=Length(StrSend);
      for n:=1 to l do begin sLog:=sLog+'|'+IntToHex(ord(StrSend[n]),2); end;

      FisPrint.WriteStr(StrSend);

      delay(10);

      sLog:=sLog+') ����� $ '+IntToHex(NumPacket,2);
      if CommonSet.WriteFisLog=1 then prWriteLog(sLog);

      for n:=1 to 1000 do
      begin
        iC:=BufferCount;
        delay(10);
        if (iC>0) and (iC=BufferCount) then  Break;
      end;

      sRes:=sBuffer;  l:=BufferCount;

      sLog:='';
      for j:=1 to l do begin sLog:=sLog+'|'+IntToHex(ord(sBuffer[j]),2); end;
      sLog:='~~~~~ <= '+sRes+'('+sLog+') ������ - '+its(n);

      if CommonSet.WriteFisLog=1 then prWriteLog(sLog);

      if l>=2 then
      begin
        NumPackRet:=ord(sBuffer[2]);
        if NumPackRet<>NumPacket then bErr:=True;
      end else bErr:=True;

      if StrS='~~~~' then bErr:=True; // �������� ��� �����

      if bErr then //��������� ����� ������
      begin
        //������ ������ � ������ - ����� �����������������

      end else Result:=sRes; //��� ��
    end else if CommonSet.WriteFisLog=1 then prWriteLog('~~~~~ ������ ��������� ����� '+FisPrint.Port);
  except
    if CommonSet.WriteFisLog=1 then prWriteLog('~~~~~ ������ fSendFis');
  end;
end;

function fCRC(StrS:String):string;
var n:Integer;
    iCRC:byte;
begin
  iCRC:=0;
  for n:=2 to Length(StrS) do iCRC:=iCRC xor ord(StrS[n]);
  Result:=IntToHex(iCRC,2);
end;

Function PrintQR(StrFile:String):Boolean;  //��� ������ StrFile - ��� ��� QR ���
Var StrP,StrS,sRes:string;
begin
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then
  begin
    Result:=True;
    exit;
  end;
  with dmC do
  begin
    Result:=False;

    StrP:=FisPrint.Port;
    if fFisOpen(StrP,FisPrint) then
    begin
      StrS:='41'+'02'+#$1C+'05'+#$1C+'05'+#$1C+'08'+#$1C+AnsiToOemConvert(StrFile)+#$1C;
      sRes:=fSendFis(StrS,FisPrint);
      sRes:=Copy(sRes,5,2);
      if sRes='00' then Result:=True;
    end;
  end;
end;


function prLoadLogo:Boolean;
begin
  Result:=True;
end;

procedure prBnPrint;
Var StrWk:string;
begin
//����� ����� ������ ������ ���������� �� ����������
  try
//      with fmMainCasher do
//      begin
      if  BnStr>'' then
      begin
        if OpenNFDoc then
        begin
          while BnStr>'' do
          begin
            if (ord(BnStr[1])=$0D)or(ord(BnStr[1])=$0A) then Delete(BnStr,1,1)
            else
            begin
              if ord(BnStr[1])=$01 then
              begin
                try
                  StrWk:=BnStr;
                  Delete(StrWk,1,1);
                  if Length(Trim(StrWk))>=40 then  //���� ���������� ������ ������ 40 �������� �� ��������� ����� �� ������������...
                  begin
                    if CloseNFDoc=False then
                    begin
                      TestFp('PrintNFStr');
                      CloseNFDoc;
                    end;

                    if (CommonSet.TypeFis='prim')or(CommonSet.TypeFis='shtrih') then
                    begin
                      PrintNFStr(' ');
                      PrintNFStr(' ');
                      PrintNFStr(' ');
                      PrintNFStr(' ');
                      PrintNFStr(' ');
                      PrintNFStr(' ');
                      CutDoc;
                    end;

                    delay(33);

                    if OpenNFDoc=false then
                    begin
                      TestFp('PrintNFStr');
                      OpenNFDoc;
                    end;
                  end;
                finally
                  Delete(BnStr,1,1);
                end;
              end
              else
              begin
                if Pos(Chr($0D),BnStr)>0 then
                begin
                  StrWk:=Copy(BnStr,1,Pos(Chr($0D),BnStr)-1);
                  Delete(BnStr,1,Pos(Chr($0D),BnStr));
                end
                else
                begin
                  if Pos(Chr($01),BnStr)>0 then
                  begin
                    StrWk:=Copy(BnStr,1,Pos(Chr($01),BnStr)-1);
                    Delete(BnStr,1,Pos(Chr($01),BnStr)-1);
                  end
                  else
                  begin
                    StrWk:=BnStr;
                    BnStr:='';
                  end;
                end;
                if pos('�����',StrWk)>0 then PrintNFStr('')
                else
                if PrintNFStr(StrWk)=False then
                begin
                   TestFp('PrintNFStr');
                   PrintNFStr(StrWk);
                end;
              end;
            end;
          end;
          if CloseNFDoc=False then
          begin
            TestFp('CloseNFDoc');
          end;
        end else TestFp('OpenNFDoc');
      end;
  except
    prWriteLog('ErrPrint');
  end;
end;


function  GetRSumZ:Boolean;
var StrP,sRes,StrS,StrWk,StrPar:string;
    i:INteger;
begin
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then begin Result:=True; exit; end;
  for i:=1 to 16 do rArrSum[i]:=0;

  with dmC do
  begin
    Result:=False;
    StrP:=FisPrint.Port;

    if fFisOpen(StrP,FisPrint) then
    begin
      StrS:='01'+'03'+#$1C;
      sRes:=fSendFis(StrS,FisPrint);
      StrWk:=sRes;
      if Pos(#$1C,StrWk)>0 then
      begin
        Delete(StrWk,1,Pos(#$1C,StrWk));
        if Pos(#$1C,StrWk)>0 then   //1       ���
        begin
          StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1);
          rArrSum[1]:=fS2F(StrPar);
          Delete(StrWk,1,Pos(#$1C,StrWk));
        end;
        if Pos(#$1C,StrWk)>0 then   //2       ��
        begin
          StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1);
          rArrSum[2]:=fS2F(StrPar);
          Delete(StrWk,1,Pos(#$1C,StrWk));
        end;

        Result:=True;
      end;
    end;
  end;
end;

function  GetRetSumZ:Boolean;
var StrP,sRes,StrS,StrWk,StrPar:string;
    i:INteger;
begin
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then begin Result:=True; exit; end;
  for i:=1 to 16 do rArrSum[i]:=0;

  with dmC do
  begin
    Result:=False;
    StrP:=FisPrint.Port;

    if fFisOpen(StrP,FisPrint) then
    begin
      StrS:='01'+'05'+#$1C;
      sRes:=fSendFis(StrS,FisPrint);
      StrWk:=sRes;
      if Pos(#$1C,StrWk)>0 then
      begin
        Delete(StrWk,1,Pos(#$1C,StrWk));
        if Pos(#$1C,StrWk)>0 then   //1       ���
        begin
          StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1);
          rArrSum[1]:=fS2F(StrPar);
          Delete(StrWk,1,Pos(#$1C,StrWk));
        end;
        if Pos(#$1C,StrWk)>0 then   //2       ��
        begin
          StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1);
          rArrSum[2]:=fS2F(StrPar);
          Delete(StrWk,1,Pos(#$1C,StrWk));
        end;

        Result:=True;
      end;
    end;
  end;
end;


Function SetDP(Str1,Str2:String):Boolean;
begin
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then begin Result:=True; exit; end;
  Result:=True;
end;


Function GetSumDisc(Var rSum1,rSum2,rSum3,rSum4:Real):Boolean;
Var StrP,StrS,sRes,StrWk:string;
    StrPar:string;
begin
  rSum1:=0; rSum2:=0; rSum3:=0; rSum4:=0;
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then begin Result:=True; exit; end;

  with dmC do
  begin
    Result:=False;

    StrP:=FisPrint.Port;
    if fFisOpen(StrP,FisPrint) then
    begin
      StrS:='01'+'09'+#$1C;
      sRes:=fSendFis(StrS,FisPrint); //
      StrWk:=sRes;

      if Pos(#$1C,StrWk)>0 then
      begin
        Delete(StrWk,1,Pos(#$1C,StrWk));
        if Pos(#$1C,StrWk)>0 then
        begin
          StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1); //����� ����
          rSum1:=fS2F(StrPar);
          Delete(StrWk,1,Pos(#$1C,StrWk));
          if Pos(#$1C,StrWk)>0 then
          begin
            StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1); //������ ���������
            rSum2:=fS2F(StrPar);
            Delete(StrWk,1,Pos(#$1C,StrWk));
            if Pos(#$1C,StrWk)>0 then
            begin
              StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1); //������ ���������
              rSum3:=fS2F(StrPar);
              Delete(StrWk,1,Pos(#$1C,StrWk));
              if Pos(#$1C,StrWk)>0 then
              begin
                StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1); //������ ���������
                rSum4:=fS2F(StrPar);
                Result:=True;
              end;
            end;
          end;
        end;
      end;
    end;
  end;
end;


Function GetNumCh:Boolean;
Var iNum:Integer;
    StrP,StrS,sRes,StrWk:string;
    StrPar:string;
begin
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then begin inc(Nums.iCheckNum); Result:=True; exit; end;
  Result:=False;

  try
    iNum:=0;
    with dmC do
    begin

      StrP:=FisPrint.Port;
      if fFisOpen(StrP,FisPrint) then
      begin
        StrS:='01'+'02'+#$1C;
        sRes:=fSendFis(StrS,FisPrint); //
        StrWk:=sRes;

        if Pos(#$1C,StrWk)>0 then
        begin
          Delete(StrWk,1,Pos(#$1C,StrWk));
          if Pos(#$1C,StrWk)>0 then
          begin
            StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1); //����� ����
            iNum:=StrToIntDef(Trim(StrPar),0);
            Result:=True;
          end;
        end;
      end;
    end;

    Nums.iCheckNum:=iNum;
    Nums.CheckNum:=IntToStr(Nums.iCheckNum);

{
3 ���
11.37 18:919 1   "01001322
11.37 18:919
11.37 18:982 2   #01002320}
{ 5 ���
11.40 14:643 1   .0100132E
11.40 14:645
11.40 14:802 2   /0100252 c
}

  except
  end;
end;


Function GetCashReg(Var rSum:Real):Boolean;//���������� � �����
var StrP,sRes,StrS,StrWk:string;
//    rSumE,rSumR,rSumRet,rSumIn,rSumOut,rSumB:Real;
    StrPar:string;
//    rSumNal,rSumNalRet:Real;
begin
  rSum:=-1;
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then begin Result:=True; exit; end;

  with dmC do
  begin
    Result:=False;

    StrP:=FisPrint.Port;

    if fFisOpen(StrP,FisPrint) then
    begin
      StrS:='02'+'07'+#$1C;
      sRes:=fSendFis(StrS,FisPrint);
      StrWk:=sRes;
      if Pos(#$1C,StrWk)>0 then
      begin
        Delete(StrWk,1,Pos(#$1C,StrWk));
        if Pos(#$1C,StrWk)>0 then   //3
        begin
          StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1);
          rSum:=fS2F(StrPar);
        end;
      end;


      {
      rSumE:=0; rSumR:=0; rSumRet:=0; rSumIn:=0; rSumOut:=0;

      StrS:='01'+'12'+#$1C;
      sRes:=fSendFis(StrS,FisPrint);
      StrWk:=sRes;
      if Pos(#$1C,StrWk)>0 then
      begin
        Delete(StrWk,1,Pos(#$1C,StrWk));
        if Pos(#$1C,StrWk)>0 then   //1
        begin
          StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1);
          Delete(StrWk,1,Pos(#$1C,StrWk));
        end;
        if Pos(#$1C,StrWk)>0 then   //2
        begin
          StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1);
          Delete(StrWk,1,Pos(#$1C,StrWk));
        end;
        if Pos(#$1C,StrWk)>0 then   //3
        begin
          StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1);
          rSumE:=fS2F(StrPar);
          Delete(StrWk,1,Pos(#$1C,StrWk));
        end;
        if Pos(#$1C,StrWk)>0 then   //4
        begin
          StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1);
          Delete(StrWk,1,Pos(#$1C,StrWk));
        end;
        if Pos(#$1C,StrWk)>0 then   //5
        begin
          StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1);
          rSumR:=fS2F(StrPar);
          Delete(StrWk,1,Pos(#$1C,StrWk));
        end;
        if Pos(#$1C,StrWk)>0 then   //6
        begin
          StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1);
          Delete(StrWk,1,Pos(#$1C,StrWk));
        end;
        if Pos(#$1C,StrWk)>0 then   //7
        begin
          StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1);
          rSumRet:=fS2F(StrPar);
          Delete(StrWk,1,Pos(#$1C,StrWk));
        end;
        if Pos(#$1C,StrWk)>0 then   //8
        begin
          StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1);
          Delete(StrWk,1,Pos(#$1C,StrWk));
        end;
        if Pos(#$1C,StrWk)>0 then   //9
        begin
          StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1);
          Delete(StrWk,1,Pos(#$1C,StrWk));
        end;
        if Pos(#$1C,StrWk)>0 then   //10
        begin
          StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1);
          Delete(StrWk,1,Pos(#$1C,StrWk));
        end;
        if Pos(#$1C,StrWk)>0 then   //11
        begin
          StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1);
          rSumIn:=fS2F(StrPar);
          Delete(StrWk,1,Pos(#$1C,StrWk));
        end;
        if Pos(#$1C,StrWk)>0 then   //12
        begin
          StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1);
          Delete(StrWk,1,Pos(#$1C,StrWk));
        end;
        if Pos(#$1C,StrWk)>0 then   //13
        begin
          StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1);
          rSumOut:=fS2F(StrPar);
          Delete(StrWk,1,Pos(#$1C,StrWk));
        end;
      end;

      rSumB:=rSumE-rSumIn+rSumOut+rSumRet-rSumR;  //����� �� ������ �����

      rSumNal:=0;
      rSumNalRet:=0;

      StrS:='01'+'03'+#$1C;
      sRes:=fSendFis(StrS,FisPrint);
      StrWk:=sRes;
      if Pos(#$1C,StrWk)>0 then
      begin
        Delete(StrWk,1,Pos(#$1C,StrWk));
        if Pos(#$1C,StrWk)>0 then   //1
        begin
          StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1);
          rSumNal:=fS2F(StrPar);
          Delete(StrWk,1,Pos(#$1C,StrWk));
        end;
        if Pos(#$1C,StrWk)>0 then   //2
        begin
          StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1);
//          rSumBn:=fS2F(StrPar);
          Delete(StrWk,1,Pos(#$1C,StrWk));
        end;
      end;

      StrS:='01'+'05'+#$1C;
      sRes:=fSendFis(StrS,FisPrint);
      StrWk:=sRes;
      if Pos(#$1C,StrWk)>0 then
      begin
        Delete(StrWk,1,Pos(#$1C,StrWk));
        if Pos(#$1C,StrWk)>0 then   //1
        begin
          StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1);
          rSumNalRet:=fS2F(StrPar);
          Delete(StrWk,1,Pos(#$1C,StrWk));
        end;
        if Pos(#$1C,StrWk)>0 then   //2
        begin
          StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1);
//          rSumBnRet:=fS2F(StrPar);
          Delete(StrWk,1,Pos(#$1C,StrWk));
        end;
      end;

      rSum:=rSumB+rSumNal-rSumNalRet;
      }
      Result:=True;
    end;
  end;
end;

Function InCass(rSum:Real):Boolean;
var StrP,StrS,sRes:string;
begin
  Result:=True;
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then begin exit; end;

  with dmC do
  begin
    StrP:=FisPrint.Port;

    if rSum>0 then  //�������� �����
    begin
      if fFisPrinter(StrP,FisPrint) then
      begin
        StrS:='30'+'04'+#$1C+'01'+#$1C+AnsiToOemConvert(Person.Name)+#$1C+'33'+#$1C;
        sRes:=fSendFis(StrS,FisPrint);
      end;

      if fFisPrinter(StrP,FisPrint) then
      begin
        StrS:='48'+AnsiToOemConvert('���. ���.')+#$1C+fs(rv(rSum))+#$1C;
        sRes:=fSendFis(StrS,FisPrint);
      end;

      if fFisPrinter(StrP,FisPrint) then
      begin
        StrS:='31'+'0'+#$1C+AnsiToOemConvert('   ')+#$1C;
        sRes:=fSendFis(StrS,FisPrint);
      end;
    end else //������
    begin
      if fFisPrinter(StrP,FisPrint) then
      begin
        StrS:='30'+'05'+#$1C+'01'+#$1C+AnsiToOemConvert(Person.Name)+#$1C+'33'+#$1C;
        sRes:=fSendFis(StrS,FisPrint);
      end;

      if fFisPrinter(StrP,FisPrint) then
      begin
        StrS:='48'+AnsiToOemConvert('���. ���.')+#$1C+fs(rv(Abs(rSum)))+#$1C;
        sRes:=fSendFis(StrS,FisPrint);
      end;

      if fFisPrinter(StrP,FisPrint) then
      begin
        StrS:='31'+'0'+#$1C+AnsiToOemConvert('   ')+#$1C;
        sRes:=fSendFis(StrS,FisPrint);
      end;
    end;
  end;
end;



Function GetSums(Var rSum1,rSum2,rSum3:Real):Boolean;
begin
  result:=True;
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then begin Result:=True; exit; end;
  rSum1:=0;
  rSum2:=0;
  rSum3:=0;
end;


function More24H(Var iSt:INteger):Boolean;
Var StrP,StrS,sRes,StrWk:string;
    StrPar:string;
    iF1,iF2,iR:Integer;
begin
  iSt:=0;

  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then begin Result:=False; exit; end;

  with dmC do
  begin
    Result:=False;

    StrP:=FisPrint.Port;
    if fFisOpen(StrP,dmC.FisPrint) then
    begin
      iF1:=0;
      iF2:=0;
      StrS:='00';
      sRes:=fSendFis(StrS,FisPrint); //
      StrWk:=sRes;

      if Pos(#$1C,StrWk)>0 then
      begin
        Delete(StrWk,1,Pos(#$1C,StrWk));
        if Pos(#$1C,StrWk)>0 then
        begin
          StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1); //������ ������� ������ ���
          iF1:=StrToIntDef(Trim(StrPar),0);
          Delete(StrWk,1,Pos(#$1C,StrWk));
          if Pos(#$1C,StrWk)>0 then
          begin
            StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1); //������ ���������
            iF2:=StrToIntDef(Trim(StrPar),0);
          end;
        end;
      end;
      iR:=iF1 and $08; //������ ����� 24 � ������� ��������
      if iR<>0 then
      begin
        iSt:=8;
        Result:=True;
      end;
    end;
  end;
end;

function IsOLEObjectInstalled(Name: String): boolean;
begin
  Result := false;
end;


Function GetSt(Var sMess:String):Integer;
begin
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then begin Result:=0; exit; end;

  result:=0;
  sMess:='';
end;



Function TestStatus(StrOp:String; Var SMess:String):Boolean;
Var St1,St2,St3:ShortINt;
begin
  Result:=True;
  sMess:='';
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then exit;

  if InspectSt(sMess)<>0 then  //����� ��������� ����������� ���� ������ � ������� ������
  begin
    prWriteLog('--'+StrOp+';'+sMess);
    Result:=False;
  end;

//  if iRfr=0 then iRfr:=Drv.GetStatus(St1,St2,St3);
//  Drv.GetErrorMesageRU(iRfr,sMess);
end;

procedure WriteHistoryFR(Strwk_: string);
var F: TextFile;
    Strwk1:String;
    FileN:String;
begin
  try
    if not DirectoryExists(CommonSet.PathHistory) then
    begin
//      ShowMessage('������: ����������� ����������� - "'+CommonSet.PathHistory+'"');
      exit;
    end;

    strwk1:='fr'+FormatDateTime('yyyy_mm',Date);
    Strwk1:=StrWk1+'.txt';
    Application.ProcessMessages;

    FileN:=CommonSet.PathHistory+strwk1;

    AssignFile(F, FileN);
    if Not FileExists(FileN) then Rewrite(F)
    else                           Append(F);
    Write(F, FormatDateTime('DD/MM/YYYY  HH:NN:SS ', Now));
    WriteLn(F, Strwk_);
    Flush(F);
  finally
    CloseFile(F);
  end;
end;


Function CutDoc:Boolean;
begin
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then
  begin
    dmC.prCutDoc(CommonSet.PortCash,0);
    Result:=True;
    exit;
  end;
//  iRfr:=Drv.CutAndPrint;
  iRfr:=0;
  if iRfr=0 then Result:=True
  else Result:=False;
end;

Function CutDoc1:Boolean;
begin
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then begin Result:=True; exit; end;
//iRfr:=Drv.CutAndPrint;
  iRfr:=0;
  if iRfr=0 then Result:=True
  else Result:=False;
end;

Function SelectF(iNum:Integer):Boolean;
Var iFl,iFl1:Byte;
    Buff:Array[1..100] of Char;
    iTypeP:INteger;
    StrP:String;
begin
  Result:=True;
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then
  begin
    StrP:=Commonset.PortCash;
    if pos('COM',StrP)>0 then
    begin
      iTypeP:=0;
      if (StrP[1]='S')or(StrP[1]='s') then iTypeP:=1;

      if iTypeP=0 then //������ �� ���������
      begin
        iFl:=$00;
        Case iNum of
        0: begin iFl:=$01 end;  //�������
        1: begin iFl:=$21 end;  //������� ������
        2: begin iFl:=$11 end;  //������� ������
        3: begin iFl:=$81 end;  //�������������
        4: begin iFl:=$31 end;  //������� ��� � ���

        5: begin iFl:=$09 end;  //�������
        6: begin iFl:=$29 end;  //������� ������
        7: begin iFl:=$19 end;  //������� ������
        8: begin iFl:=$89 end;  //�������������
        9: begin iFl:=$39 end;  //������� ��� � ���

        10: begin iFl:=$00 end; //�������
        11: begin iFl:=$20 end; //������� ������
        12: begin iFl:=$10 end; //������� ������
        13: begin iFl:=$80 end; //�������������
        14: begin iFl:=$30 end; //������� ��� � ���

        15: begin iFl:=$08 end; //�������
        16: begin iFl:=$28 end; //������� ������
        17: begin iFl:=$18 end; //������� ������
        18: begin iFl:=$88 end; //�������������
        19: begin iFl:=$38 end; //������� ��� � ���

        end;

        Buff[1]:=#$1B;
        Buff[2]:=#$21;
        Buff[3]:=chr(iFl);

        try
          dmC.DevPrint.WriteBuf(Buff,3);
        except
        end;
      end;

      if iTypeP=1 then //Star600
      begin
        iFl:=$30;  iFl1:=$30;
        Case iNum of
        10: begin iFl:=$30; iFl1:=$30; end; //�������
        13: begin iFl:=$30; iFl1:=$30; end; //�������������
        14: begin iFl:=$31; iFl1:=$31; end; //������� ��� � ���
        15: begin iFl:=$30; iFl1:=$30; end; //�������
        end;

        Buff[1]:=#$1B;
        Buff[2]:=#$69; // 30 30
        Buff[3]:=chr(iFl);
        Buff[4]:=chr(iFl1);

        try
          dmC.DevPrint.WriteBuf(Buff,4);
        except
        end;
      end;
    end;
    Result:=True;
    exit;
  end;
end;


Function PrintNFStr(StrPr:String):Boolean;
var StrP,StrS,sRes:string;
begin
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then
  begin
    PrintStr(StrPr);
    Result:=True;
    exit;
  end;

  with dmC do
  begin
    Result:=False;

    StrP:=FisPrint.Port;
    if fFisOpen(StrP,FisPrint) then
    begin
//      if fFisPrinter(StrP,FisPrint) then
//      begin
        StrS:='40'+AnsiToOemConvert(StrPr)+#$1C+'0'+#$1C;
        sRes:=fSendFis(StrS,FisPrint); //  #400024
        sRes:=Copy(sRes,5,2);
        if sRes='00' then Result:=True;
//      end;
    end;
  end;
end;


Function OpenNFDoc:Boolean;
var StrP,StrS,sRes:string;
begin
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then
  begin
    dmC.prDevOpen(CommonSet.PortCash,0);
    Result:=True;
    exit;
  end;

  with dmC do
  begin
    Result:=False;

    StrP:=FisPrint.Port;
    if fFisOpen(StrP,FisPrint) then
    begin
      if fFisPrinter(StrP,FisPrint) then
      begin

        if CommonSet.WriteFisLog=1 then prWriteLog('~~~~ OpenNFDoc ~~~~');

        StrS:='30'+'01'+#$1C+'01'+#$1C+AnsiToOemConvert(Person.Name)+#$1C+'33'+#$1C;
        sRes:=fSendFis(StrS,FisPrint); // "300022
        sRes:=Copy(sRes,5,2);
        if sRes='00' then Result:=True;
      end;
    end;
  end;
end;

Function CloseNFDoc:Boolean;
var StrP,StrS:string;
begin
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then
  begin
    dmC.prCutDoc(CommonSet.PortCash,0);
    dmC.prDevClose(CommonSet.PortCash,0);
    Result:=True;
    exit;
  end;
  with dmC do
  begin
    Result:=False;

    StrP:=FisPrint.Port;
    if fFisOpen(StrP,FisPrint) then
    begin
      if CommonSet.WriteFisLog=1 then prWriteLog('~~~~ CloseNFDoc ~~~~');

      StrS:='31'+'0'+#$1C+AnsiToOemConvert(Person.Name)+#$1C;
      fSendFis(StrS,FisPrint); //
      Result:=True;
      FisPrint.Close;
    end;
  end;
end;


Function CashDriver:Boolean;
Var  StrP,StrS,sRes:string;
begin
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then
  begin
    OpenMoneyBox;
    Result:=True;
    exit;
  end;

  with dmC do
  begin
    Result:=False;

    StrP:=FisPrint.Port;
    if fFisOpen(StrP,FisPrint) then
    begin
      if CommonSet.WriteFisLog=1 then prWriteLog('~~~~ CashDriver ~~~~');

      StrS:='80'+'150'+#$1C;
      sRes:=fSendFis(StrS,FisPrint); //
      sRes:=Copy(sRes,5,2);
      if sRes='00' then  Result:=True;
    end;
  end;
end;


Function CheckClose:Boolean;
Var  StrP,StrS,sRes:string;
begin
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then
  begin
    PrintStr(CommonSet.LastStr1);
    PrintStr(CommonSet.LastStr2);
    dmC.prCutDoc(CommonSet.PortCash,0);
    dmC.prDevClose(CommonSet.PortCash,0);
    Result:=True;
    exit;
  end;

  with dmC do
  begin
    Result:=False;

    StrP:=FisPrint.Port;
    if fFisOpen(StrP,FisPrint) then
    begin
      if fFisPrinter(StrP,FisPrint) then
      begin
        StrS:='31'+'0'+#$1C+AnsiToOemConvert(' ')+#$1C;
        sRes:=fSendFis(StrS,FisPrint); //
        sRes:=Copy(sRes,5,2);
        if sRes='00' then  Result:=True;
        FisPrint.Close;
      end;
    end;
  end;
end;

Function   CheckCloseNoCut:Boolean;                   //OK
Var  StrP,StrS,sRes:string;
begin
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then
  begin
    PrintStr(CommonSet.LastStr1);
    PrintStr(CommonSet.LastStr2);
    dmC.prCutDoc(CommonSet.PortCash,0);
    dmC.prDevClose(CommonSet.PortCash,0);
    Result:=True;
    exit;
  end;

  with dmC do
  begin
    Result:=False;

    StrP:=FisPrint.Port;
    if fFisOpen(StrP,FisPrint) then
    begin
      if fFisPrinter(StrP,FisPrint) then
      begin
        StrS:='31'+'5'+#$1C+AnsiToOemConvert(' ')+#$1C;
        sRes:=fSendFis(StrS,FisPrint); //
        sRes:=Copy(sRes,5,2);
        if sRes='00' then  Result:=True;
        FisPrint.Close;
      end;
    end;
  end;
end;


Function CheckRasch(iPayType,iClient:Integer; ValName,Comment:String; Var iDopl:Integer):Boolean;   //����� ������ �� ���������
Var rSumSd:Currency;
    rCli:Real;
    Str1,Str2:String;
    StrP,StrS,sRes:string;
begin
  iDopl:=0;
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then
  begin
    rCli:=iClient/100;
    Str(rCli:8:2,Str2);
    if iPayType=0 then
    begin
      Str1:=SetL('�������� ��������� ',32)+Str2;
      PrintStr(Str1);
    end;
    if iPayType=2 then
    begin
      Str1:=SetL('�������� ����.������ ',32)+Str2;
      PrintStr(Str1);
    end;
    if rCli>(rSumR-rSumD) then
    begin
      Str(rCli-(rSumR-rSumD):8:2,Str2);
      Str1:=SetL('����� ',32)+Str2;
      PrintStr(Str1);
    end;

    Result:=True;
    exit;
  end;
  with dmC do
  begin
    StrP:=FisPrint.Port;
    if fFisOpen(StrP,FisPrint) then
    begin
      if iPayType=0 then //���
        StrS:='47'+'0'+#$1C+fs(iClient/100)+#$1C+AnsiToOemConvert(Comment)+#$1C
      else
        StrS:='47'+'1'+#$1C+fs(iClient/100)+#$1C+AnsiToOemConvert(Comment)+#$1C;

      sRes:=fSendFis(StrS,FisPrint);
      Result:=True;
    end;
  end;
end;

Function CheckDiscount(sDisc:String;rDiscount:Real;Var iItog:Integer):Boolean;
Var iDiscount:Integer;
    rDiscCurr,rItog:Currency;
    StrP,StrS,sRes:string;
    rSumChFis,dSumFis,nSumFis:Real;
begin
  iDiscount:=RoundEx(rDiscount*100);
  iItog:=0;
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then begin Result:=True; exit; end;
  if iDiscount>=0 then
  begin //������
    rDiscCurr:=iDiscount/100;
    with dmC do
    begin
      Result:=False;

      StrP:=FisPrint.Port;
      if fFisOpen(StrP,FisPrint) then
      begin
        if fFisPrinter(StrP,FisPrint) then
        begin
          StrS:='45'+'1'+#$1C+AnsiToOemConvert(sDisc)+#$1C+fs(rDiscCurr)+#$1C;
          sRes:=fSendFis(StrS,FisPrint); //
          sRes:=Copy(sRes,5,2);
          if sRes='00' then
          begin
            Delay(20);
            Result:=True;

            if fChSum(FisPrint,rSumChFis,dSumFis,nSumFis) then
              iItog:=RoundEx(rSumChFis*100); //� ��������
          end;
        end;
      end;
    end;
  end;
end;

Function CheckTotal(Var iTotal:Integer):Boolean;
Var rSum:Currency;
    Str1,Str2:String;
    rDiscDop:Real;
    rSumCh:Real;

    StrP,StrS,sRes:string;
    rSumChFis,dSumFis,nSumFis:Real;
begin
  iTotal:=0;
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then
  begin
    PrintStr('----------------------------------------');
    rSumR:=0;
    rSumD:=0;
    with dmC do
    begin
      dmC.taCheck.First;
      while not dmC.taCheck.Eof do
      begin
        rSumR:=rSumR+dmC.taCheckRSUM.AsFloat;
        rSumD:=rSumD+dmC.taCheckDSUM.AsFloat;
        dmC.taCheck.Next;
      end;
    end;
    Str(rSumR:8:2,Str2);
    Str1:=SetL('����� ',32)+Str2;
    PrintStr(Str1);

    if ((rSumR-rSumD)>0.1) then
    begin
      rSumCh:=rSumR-rSumD;
//      rSumCh:=RoundSpec(rSumCh,rDiscDop);
//      rSumD:=rSumD+rDiscDop;
    end;


    if rSumD<>0 then
    begin
      Str(rSumD:7:2,Str2);
      Str1:=SetL('������ ',33)+Str2;
      PrintStr(Str1);

      Str((rSumR-rSumD):8:2,Str2);
      Str1:=SetL('����� ',32)+Str2;
      PrintStr(Str1);
    end;

    Result:=True;
    exit;
  end;

  with dmC do
  begin
    Result:=False;

    StrP:=FisPrint.Port;
    if fFisOpen(StrP,FisPrint) then
    begin
      if fFisPrinter(StrP,FisPrint) then
      begin
        StrS:='44';
        sRes:=fSendFis(StrS,FisPrint); //
        sRes:=Copy(sRes,5,2);
        if sRes='00' then
        begin
          Delay(20);
          Result:=True;
          iTotal:=0;

          if fChSum(FisPrint,rSumChFis,dSumFis,nSumFis) then
            iTotal:=RoundEx(rSumChFis*100); //� ��������
        end;
      end;
    end;
  end;
end;


Procedure CheckCancel;
Var  StrP,StrS:string;
begin
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then
  begin
    PrintStr('��� �����������');
    dmC.prCutDoc(CommonSet.PortCash,0);
    dmC.prDevClose(CommonSet.PortCash,0);
    exit;
  end;

  with dmC do
  begin
    StrP:=FisPrint.Port;
    if fFisOpen(StrP,FisPrint) then
    begin
      if fFisPrinter(StrP,FisPrint) then
      begin
        StrS:='32';
        fSendFis(StrS,FisPrint); //
      end;
    end;
  end;
end;

Procedure TestPosCh;
begin
//  if Length(PosCh.Name)>36 then PosCh.Name:=Copy(PosCh.Name,1,36);
//  if Length(PosCh.Code)>19 then PosCh.Code:=Copy(PosCh.Code,1,19);
//  if Length(PosCh.AddName)>250 then PosCh.AddName:=Copy(PosCh.AddName,1,250);

  if Length(CurPos.Name)>32 then CurPos.Name:=Copy(CurPos.Name,1,32)+' ';
  if Length(CurPos.Articul)>5 then CurPos.Articul:=Copy(CurPos.Articul,1,5);
end;


Function CheckAddPos(Var iSum:Integer):Boolean;
Var rSum,rPr:Currency;
    rQ:Real;
    Str1,Str2:String;
    StrP,StrS,sRes:string;
    iNDS:SmallInt;
    sPrice,sDrobn:string;

begin
  delay(10);
  iSum:=RoundEx(CurPos.Price*RoundEx(CurPos.Quant*1000)/1000 *100);
  TestPosCh;
  rPr:=rv(CurPos.Price);

  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then
  begin
    Str1:=SetL(CurPos.Name,17)+' '+Copy(CurPos.EdIzm,1,2);
    Str(RoundEx(CurPos.Quant*1000)/1000:6:3,Str2);
    if Str2[6]='0' then delete(Str2,6,1) else delete(Str2,1,1);
    Str1:=Str1+Str2;
    Str(rPr:6:2,Str2);
    Str1:=Str1+'*'+Str2;
    Str(iSum/100:7:2,Str2);
    Str1:=Str1+':'+Str2;
    PrintStr(Str1);

    prWriteLog('~~NoFisPos;'+its(Nums.iCheckNum)+';'+CurPos.Articul+';'+CurPos.Bar+';'+fs(CurPos.Quant)+';'+fs(rPr)+';');

    Result:=True;
    delay(100);
    exit;
  end;

  CurPos.FisPos:=1; //����� �������� ���������� �������� � ������� ��������
  prWriteLog('~~FisPos;'+its(Nums.iCheckNum)+';'+CurPos.Articul+';'+CurPos.Bar+';'+fs(CurPos.Quant)+';'+fs(CurPos.Price)+';');

  if CurPos.Quant>=0 then
  begin
    with dmC do
    begin
      Result:=False;

      StrP:=FisPrint.Port;
      if fFisOpen(StrP,FisPrint) then
      begin
        if fFisPrinter(StrP,FisPrint) then
        begin
          Str1:=SetL(CurPos.Name,25)+' '+Copy(CurPos.EdIzm,1,2);
          iNDS:=3;  // �� ����������  2 - 0% ���
          //���� �������� ������ ���
//          if CurPos.iNDS=2 then iNDS:=0; //18 % ��� � ��
//          if CurPos.iNDS=1 then iNDS:=1; //10 % ��� � ��

          sPrice:=fs(CurPos.Price);
          sDrobn:='';
          if Pos('.',sPrice)>0 then
          begin
            sDrobn:=Copy(sPrice,Pos('.',sPrice),Length(sPrice)-Pos('.',sPrice)+1);
            Delete(sDrobn,1,1);
            if Length(sDrobn)>9 then sDrobn:=Copy(sDrobn,1,9);
            sPrice:=Copy(sPrice,1,Pos('.',sPrice)-1);
            sPrice:=sPrice+'.'+sDrobn;
          end;


          StrS:='42'+AnsiToOemConvert(Str1)+#$1C+CurPos.Articul+#$1C+fs(CurPos.Quant)+#$1C+sPrice+#$1C+its(iNDS)+#$1C+'0'+#$1C+'0'+#$1C+'0'+#$1C+' '+#$1C+'0.0'+#$1C;

          sRes:=fSendFis(StrS,FisPrint); //
          sRes:=Copy(sRes,5,2);
          if sRes='00' then Result:=True;
        end;
      end;
    end;
  end else
  begin

  end;
end;


Function CheckStart:Boolean;
Var StrWk:String;
    StrP,StrS,sRes:string;
begin
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then
  begin
    dmC.prDevOpen(CommonSet.PortCash,0);
    PrintStr(CommonSet.DepartName);
    PrintStr(CommonSet.PreStr1);
    PrintStr(CommonSet.PreStr2);
    PrintStr('-----------------���������--------------');
    PrintStr('����� '+its(CommonSet.CashNum)+'   ��� �'+its(Nums.ZNum)+'.'+its(Nums.iCheckNum));
    PrintStr('����, ����� '+FormatDateTime('dd.mm.yyyy hh:nn',now));

    StrWk:=Copy('������ '+Person.Name,1,32);
    while length(StrWk)<32 do StrWk:=StrWk+' ';
    Case Check.Operation of
    0: StrWk:=StrWk+' �������';
    1: StrWk:=StrWk+' �������';
    end;
    PrintStr(StrWk);

    PrintStr('----------------------------------------');

    Result:=True;
    exit;
  end;

  with dmC do
  begin
    Result:=False;

    StrP:=FisPrint.Port;
    if fFisOpen(StrP,FisPrint) then
    begin
      if fFisPrinter(StrP,FisPrint) then
      begin
        StrS:='30'+'02'+#$1C+'01'+#$1C+AnsiToOemConvert(Person.Name)+#$1C+'33'+#$1C;
        sRes:=fSendFis(StrS,FisPrint); //
        sRes:=Copy(sRes,5,2);
        if sRes='00' then Result:=True;
      end;
    end;
  end;
end;

Function CheckRetStart:Boolean;
Var  StrP,StrS,sRes:string;
begin
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then
  begin
    dmC.prDevOpen(CommonSet.PortCash,0);
    PrintStr(CommonSet.DepartName);
    PrintStr(CommonSet.PreStr1);
    PrintStr(CommonSet.PreStr2);
    PrintStr('-----------------���������--------------');
    PrintStr('����� '+its(CommonSet.CashNum)+'   ��� �'+its(Nums.ZNum)+'.'+its(Nums.iCheckNum));
    PrintStr('����, ����� '+FormatDateTime('dd.mm.yyyy hh:nn',now));

    StrWk:=Copy('������ '+Person.Name,1,32);
    while length(StrWk)<32 do StrWk:=StrWk+' ';
    Case Check.Operation of
    0: StrWk:=' �������';
    1: StrWk:=' �������';
    end;
    PrintStr(StrWk);

    PrintStr('----------------------------------------');

    Result:=True;
    exit;
  end;

  with dmC do
  begin
    Result:=False;

    StrP:=FisPrint.Port;
    if fFisOpen(StrP,FisPrint) then
    begin
      if fFisPrinter(StrP,FisPrint) then
      begin
        StrS:='30'+'03'+#$1C+'01'+#$1C+AnsiToOemConvert(Person.Name)+#$1C+'33'+#$1C;
        sRes:=fSendFis(StrS,FisPrint); //
        sRes:=Copy(sRes,5,2);
        if sRes='00' then Result:=True;
      end;
    end;
  end;

end;


Function ZClose:Boolean;
var StrP,StrS:string;
begin
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then begin Result:=True; exit; end;
  with dmC do
  begin
    StrP:=FisPrint.Port;
    if fFisOpen(StrP,FisPrint) then
    begin
      StrS:='21'+AnsiToOemConvert(Person.Name)+#$1C;
      fSendFis(StrS,FisPrint);
    end;
    Result:=True;
  end;
end;


Function ZOpen:Boolean;
var StrP,StrS:string;
begin
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then begin Result:=True; exit; end;
  with dmC do
  begin
    StrP:=FisPrint.Port;
    if fFisOpen(StrP,FisPrint) then
    begin
      StrS:='23'+AnsiToOemConvert(Person.Name)+#$1C;
      fSendFis(StrS,FisPrint);
    end;
    Result:=True;
  end;
end;

Function GetX:Boolean;
var StrP,StrS:string;
begin
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then begin Result:=True; exit; end;
  Result:=False;
  with dmC do
  begin
    StrP:=FisPrint.Port;
    if fFisOpen(StrP,FisPrint) then
    begin
      StrS:='20'+AnsiToOemConvert(Person.Name)+#$1C;
      fSendFis(StrS,FisPrint);
      Result:=True;
    end;
  end;
end;

Function CashDate(var sDate:String):Boolean;
Var iDD,iMM,iYY,iSS,iHH,iMI:Smallint;
    StrP,StrS,sRes:string;
    StrWk,StrD,StrT:string;
begin
//
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then begin Result:=True; exit; end;
  Result:=False;
  sDate:='';
  with dmC do
  begin
    //�� �������� ������-��
    StrP:=FisPrint.Port;
    if fFisOpen(StrP,FisPrint) then
    begin
      StrS:='13';
      sRes:=fSendFis(StrS,FisPrint);
      StrWk:=sRes;
      if Length(sRes)>=20 then
      begin
        sRes:=Copy(sRes,5,2);
        if sRes='00' then
        begin
          Result:=True;
          StrD:=Copy(StrWk,7,2)+'.'+Copy(StrWk,9,2)+'.20'+Copy(StrWk,11,2);
          StrT:=Copy(StrWk,14,2)+':'+Copy(StrWk,16,2)+':'+Copy(StrWk,18,2);
          sDate:=StrD+' '+StrT;
        end;
      end;
    end;
  end;
end;


Function CashClose:Boolean;
Var iRet:Integer;
begin
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then begin Result:=True; exit; end;
  Result:=False;
  try
    dmC.FisPrint.Close;
    result:=True;
  except
  end;
end;

Function CashOpen(sPort:PChar):Boolean;
Var StrWk:String;
    iP:INteger;
    sMess:String;
    StrS,sRes:String;
begin
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then begin Result:=True; exit; end;

  CommonSet.TypeFis:='pirit';


  StrWk:=String(sPort);

  if fFisOpen(StrWk,dmC.FisPrint) then   //���� �������
  begin
    if fTestFis(StrWk,dmC.FisPrint) then //� ���� �� �� ������ ����� ����������?
    begin
      //���������� ���� - ������ �����
      StrS:='10'+Formatdatetime('ddmmyy',Date)+#$1C+Formatdatetime('hhnnss',now)+#$1C;
      sRes:=fSendFis(StrS,dmC.FisPrint);

      //����� ��������� ������
      StrS:='00';
      sRes:=fSendFis(StrS,dmC.FisPrint);
      sRes:=Copy(sRes,3,5);
      if sRes<>'00000' then
      begin
        showmessage('������ ����� - '+sRes+' ���������� ������ ����������. ��c�� ���������� ������������� - ������������� �����.');
        result:=False;
      end else Result:=True;

{  01 00:838
01 00:838 ������ ��� ������ ������
01 00:884 "00000100C
01 03:856
01 03:856 ������ ������, ����� �������
01 04:785 #100021
01 10:267
01 10:267 ������
01 10:329 $00000000B
}
    end;
  end;
end;

Function GetNums:Boolean;
Var iZ:SmallINt;
    StrP,StrS,sRes,StrWk:string;
    StrPar:string;
begin
  Result:=False;

  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then
  begin
    Nums.iCheckNum:=prGetNFCheckNum;
    Result:=True;
    exit;
  end;

  with dmC do
  begin
    StrP:=FisPrint.Port;
    if fFisOpen(StrP,FisPrint) then
    begin
      if GetNumCh then
      begin
        StrS:='01'+'01'+#$1C;
        sRes:=fSendFis(StrS,FisPrint); //
        StrWk:=sRes;

        if Pos(#$1C,StrWk)>0 then
        begin
          Delete(StrWk,1,Pos(#$1C,StrWk));
          if Pos(#$1C,StrWk)>0 then
          begin
            StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1); //����� ����
            iZ:=StrToIntDef(Trim(StrPar),0);

            Nums.ZNum:=iZ;
            CommonSet.ZNum:=Nums.ZNum;

            Result:=True;
          end;
        end;
      end;
    end;
  end;
end;

Function GetRes:Boolean;
begin
  Result:=True;
end;

Function GetSerial:Boolean;
Var sNum:String;
begin
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then begin Result:=True; exit; end;
  {
//  iRfr:=Drv.GetFiscNumber(sNum);
  if iRfr=0 then Nums.SerNum:=sNum
  else Nums.SerNum:='';

//  iRfr:=Drv.GetRegNumber(sNum);
  if iRfr=0 then Nums.RegNum:=sNum  //��������� �����
  else Nums.RegNum:='';

//  iRfr:=Drv.GetINN(sNum);
  if iRfr=0 then Nums.INN:=sNum  //��� �����������
  else Nums.INN:='';
  }
  result:=True;
end;


Function OpenZ:Boolean;
Var StrP,StrS,sRes,StrWk:string;
    StrPar:string;
    iF1,iF2,iR:Integer;

begin
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then begin Result:=True; exit; end;

  with dmC do
  begin
    Result:=False;

    StrP:=FisPrint.Port;
    if fFisOpen(StrP,dmC.FisPrint) then
    begin
      iF1:=0;
      iF2:=0;
      StrS:='00';
      sRes:=fSendFis(StrS,FisPrint); //
      StrWk:=sRes;

      if Pos(#$1C,StrWk)>0 then
      begin
        Delete(StrWk,1,Pos(#$1C,StrWk));
        if Pos(#$1C,StrWk)>0 then
        begin
          StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1); //������ ������� ������ ���
          iF1:=StrToIntDef(Trim(StrPar),0);
          Delete(StrWk,1,Pos(#$1C,StrWk));
          if Pos(#$1C,StrWk)>0 then
          begin
            StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1); //������ ���������
            iF2:=StrToIntDef(Trim(StrPar),0);
          end;
        end;
      end;
      iR:=iF1 and $04;  //����� �������
      if iR<>0 then Result:=True;
    end;
  end;
end;



Function InspectSt(Var sMess:String):Integer;
Var StrP,StrS,sRes,StrWk:string;
    StrPar:string;
    iF0,iF1,iF2:Integer;
    sErr:String;
    bErr:Boolean;
begin
  sMess:='��� ��.';
  Result:=0;
  bErr:=False;

  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then begin Result:=0; exit; end;

  with dmC do
  begin
    StrP:=FisPrint.Port;
    if fFisOpen(StrP,dmC.FisPrint) then
    begin
      iF0:=-1; iF1:=-1; iF2:=-1;
      StrS:='00';
      sRes:=fSendFis(StrS,FisPrint); //
      StrWk:=sRes;
      sErr:=Copy(sRes,5,2);
      if (sErr='00') then
      begin
        if Pos(#$1C,StrWk)>7 then
        begin
          StrPar:=Copy(StrWk,7,Pos(#$1C,StrWk)-7);
          iF0:=StrToIntDef(Trim(StrPar),0);
          Delete(StrWk,1,Pos(#$1C,StrWk));
        end;
        if Pos(#$1C,StrWk)>0 then
        begin
          StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1);
          iF1:=StrToIntDef(Trim(StrPar),0);
          Delete(StrWk,1,Pos(#$1C,StrWk));
        end;
        if Pos(#$1C,StrWk)>0 then
        begin
          StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1);
          iF2:=StrToIntDef(Trim(StrPar),0);
          Delete(StrWk,1,Pos(#$1C,StrWk));
        end;

//        iF1:=iF1 and $04; //��������� ������ ����� = �������� � ��������  , ��� ��������� ��������
//        iF1:=iF1 and $D9; //��������� ������ ����� = �������� � ��������  , ��� ��������� ��������
        iF1:=iF1 and $D1; //��������� ������ ����� = �������� � ��������  , ��� ��������� ��������

//        if (iF0<>0)or(iF1<>0)or(iF2<>0) then bErr:=True;
        if (iF0<>0)or(iF1<>0) then
        begin
          sMess:='������ ������ � �� ( '+Its(iF0)+' (h'+IntToHex(iF0,2)+'); '+its(iF1)+' (h'+IntToHex(iF1,2)+'); '+its(iF2)+' (h'+IntToHex(iF2,2)+');'+sRes+';'+')';
          bErr:=True;
        end;

        if bErr=False then
        begin
          if fFisPrinter(StrP,FisPrint)=False then
          begin
            sMess:='������ �������� ��.';
            bErr:=True;
          end;
        end;
      end else  bErr:=True; //������ ��������
    end;
  end;

  if bErr then
  begin
    prWriteLog(sMess);
    Result:=100;
  end;

 {
//  iRfr:=Drv.GetStatus(St1,St2,St3);
//���� ��� ������� ��� ��������
//  Drv.GetErrorMessageRU(iRfr,sMess);
  if (iRfr<>0)or(St1<>0)or((St2<>2)and(St2<>0))or(St3<>0) then
    prWriteLog('--GetStatus;'+Its(iRfr)+':'+its(St1)+';'+its(St2)+';'+its(St3)+';');
  result:=iRfr;
}
end;

Function CheckOpenFis:Boolean;
Var StrP,StrS,sRes,StrWk:string;
    StrPar:string;
    iF1,iF2,iR:Integer;
begin
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then begin Result:=False; exit; end;

  //������ �� ���������� ��������?
  with dmC do
  begin
    Result:=False;

    StrP:=FisPrint.Port;
    if fFisOpen(StrP,dmC.FisPrint) then
    begin
      iF1:=0;
      iF2:=0;
      StrS:='00';
      sRes:=fSendFis(StrS,FisPrint); //
      StrWk:=sRes;

      if Pos(#$1C,StrWk)>0 then
      begin
        Delete(StrWk,1,Pos(#$1C,StrWk));
        if Pos(#$1C,StrWk)>0 then
        begin
          StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1); //������ ������� ������ ���
          iF1:=StrToIntDef(Trim(StrPar),0);
          Delete(StrWk,1,Pos(#$1C,StrWk));
          if Pos(#$1C,StrWk)>0 then
          begin
            StrPar:=Copy(StrWk,1,Pos(#$1C,StrWk)-1); //������ ���������
            iF2:=StrToIntDef(Trim(StrPar),0);
          end;
        end;
      end;
      iR:=iF2 and $02;  //������ ��� �� �������   2-�������   3-�������
      if iR in [2,3] then Result:=True;
    end;
  end;
end;



end.
