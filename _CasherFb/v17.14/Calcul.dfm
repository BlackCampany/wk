object fmCalcul: TfmCalcul
  Left = 127
  Top = 271
  BorderIcons = []
  BorderStyle = bsDialog
  ClientHeight = 281
  ClientWidth = 248
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 248
    Height = 281
    Align = alClient
    BevelInner = bvLowered
    Color = 8080936
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object E1: TcxTextEdit
      Left = 20
      Top = 24
      TabOrder = 19
      Text = 'E1'
      OnKeyPress = E1KeyPress
      Width = 121
    end
    object cxButton1: TcxButton
      Left = 16
      Top = 184
      Width = 40
      Height = 40
      Caption = '1'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 0
      TabStop = False
      OnClick = cxButton1Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfFlat
    end
    object cxButton2: TcxButton
      Left = 64
      Top = 184
      Width = 40
      Height = 40
      Caption = '2'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 1
      TabStop = False
      OnClick = cxButton2Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfFlat
    end
    object cxButton3: TcxButton
      Left = 112
      Top = 184
      Width = 40
      Height = 40
      Caption = '3'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 2
      TabStop = False
      OnClick = cxButton3Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfFlat
    end
    object cxButton4: TcxButton
      Left = 16
      Top = 136
      Width = 40
      Height = 40
      Caption = '4'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 3
      TabStop = False
      OnClick = cxButton4Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfFlat
    end
    object cxButton5: TcxButton
      Left = 64
      Top = 136
      Width = 40
      Height = 40
      Caption = '5'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 4
      TabStop = False
      OnClick = cxButton5Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfFlat
    end
    object cxButton6: TcxButton
      Left = 112
      Top = 136
      Width = 40
      Height = 40
      Caption = '6'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 5
      TabStop = False
      OnClick = cxButton6Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfFlat
    end
    object cxButton7: TcxButton
      Left = 16
      Top = 88
      Width = 40
      Height = 40
      Caption = '7'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 6
      TabStop = False
      OnClick = cxButton7Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfFlat
    end
    object cxButton8: TcxButton
      Left = 64
      Top = 88
      Width = 40
      Height = 40
      Caption = '8'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 7
      TabStop = False
      OnClick = cxButton8Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfFlat
    end
    object cxButton9: TcxButton
      Left = 112
      Top = 88
      Width = 40
      Height = 40
      Caption = '9'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 8
      TabStop = False
      OnClick = cxButton9Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfFlat
    end
    object cxButton10: TcxButton
      Left = 16
      Top = 232
      Width = 40
      Height = 40
      Caption = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 9
      TabStop = False
      OnClick = cxButton10Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfFlat
    end
    object cxButton11: TcxButton
      Left = 112
      Top = 232
      Width = 40
      Height = 40
      Caption = ','
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 10
      TabStop = False
      OnClick = cxButton11Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfFlat
    end
    object cxButton14: TcxButton
      Left = 168
      Top = 232
      Width = 65
      Height = 40
      Caption = #1042#1099#1093#1086#1076
      Default = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ModalResult = 1
      ParentFont = False
      TabOrder = 11
      OnKeyPress = cxButton14KeyPress
      Colors.Default = 8421440
      Colors.Normal = 8421440
      Colors.Pressed = 13619102
      LookAndFeel.Kind = lfFlat
    end
    object List1: TcxListBox
      Left = 16
      Top = 8
      Width = 137
      Height = 49
      BiDiMode = bdLeftToRight
      ItemHeight = 13
      Items.Strings = (
        '001'
        '002'
        '003')
      ParentBiDiMode = False
      ParentFont = False
      Style.BorderColor = clLime
      Style.BorderStyle = cbsSingle
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clBlack
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsBold]
      Style.IsFontAssigned = True
      TabOrder = 12
    end
    object cxButton12: TcxButton
      Left = 168
      Top = 8
      Width = 65
      Height = 33
      Hint = #1055#1088#1080#1073#1072#1074#1080#1090#1100
      Action = acPlus
      Caption = '+'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 13
      Colors.Default = 15523797
      Colors.Normal = 14996421
      Colors.Pressed = clWhite
      LookAndFeel.Kind = lfFlat
    end
    object cxButton13: TcxButton
      Left = 168
      Top = 48
      Width = 65
      Height = 33
      Hint = #1042#1099#1095#1077#1089#1090#1100
      Action = acMinus
      Caption = '-'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 14
      Colors.Default = 15523797
      Colors.Normal = 14996421
      Colors.Pressed = clWhite
      LookAndFeel.Kind = lfFlat
    end
    object cxButton15: TcxButton
      Left = 167
      Top = 88
      Width = 65
      Height = 33
      Hint = #1059#1084#1085#1086#1078#1080#1090#1100
      Action = acMult
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 15
      Colors.Default = 15523797
      Colors.Normal = 14996421
      Colors.Pressed = clWhite
      LookAndFeel.Kind = lfFlat
    end
    object cxButton16: TcxButton
      Left = 167
      Top = 128
      Width = 65
      Height = 33
      Hint = #1056#1072#1079#1076#1077#1083#1080#1090#1100
      Action = acDiv
      Caption = '/'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 16
      Colors.Default = 15523797
      Colors.Normal = 14996421
      Colors.Pressed = clWhite
      LookAndFeel.Kind = lfFlat
    end
    object cxButton17: TcxButton
      Left = 167
      Top = 184
      Width = 65
      Height = 33
      Hint = #1056#1072#1074#1085#1086
      Action = acResult
      Caption = '='
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 17
      Colors.Default = 15523797
      Colors.Normal = 14996421
      Colors.Pressed = clWhite
      LookAndFeel.Kind = lfFlat
    end
    object CalcEdit1: TcxCalcEdit
      Left = 16
      Top = 56
      EditValue = 11.000000000000000000
      ParentFont = False
      Properties.Alignment.Horz = taLeftJustify
      Style.BorderColor = clLime
      Style.BorderStyle = ebsSingle
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clBlack
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsBold]
      Style.ButtonTransparency = ebtHideUnselected
      Style.IsFontAssigned = True
      TabOrder = 18
      Width = 137
    end
  end
  object amCalc: TActionManager
    Left = 68
    Top = 164
    StyleName = 'XP Style'
    object Action1: TAction
      Caption = 'Action1'
      ShortCut = 49
      SecondaryShortCuts.Strings = (
        '1')
      OnExecute = Action1Execute
    end
    object Action2: TAction
      Caption = 'Action2'
      ShortCut = 50
      SecondaryShortCuts.Strings = (
        '2')
      OnExecute = Action2Execute
    end
    object Action3: TAction
      Caption = 'Action3'
      ShortCut = 51
      SecondaryShortCuts.Strings = (
        '3')
      OnExecute = Action3Execute
    end
    object Action4: TAction
      Caption = 'Action4'
      ShortCut = 52
      SecondaryShortCuts.Strings = (
        '4')
      OnExecute = Action4Execute
    end
    object Action5: TAction
      Caption = 'Action5'
      ShortCut = 53
      SecondaryShortCuts.Strings = (
        '5')
      OnExecute = Action5Execute
    end
    object Action6: TAction
      Caption = 'Action6'
      ShortCut = 54
      SecondaryShortCuts.Strings = (
        '6')
      OnExecute = Action6Execute
    end
    object Action7: TAction
      Caption = 'Action7'
      ShortCut = 55
      SecondaryShortCuts.Strings = (
        '7')
      OnExecute = Action7Execute
    end
    object Action8: TAction
      Caption = 'Action8'
      ShortCut = 56
      SecondaryShortCuts.Strings = (
        '8')
      OnExecute = Action8Execute
    end
    object Action9: TAction
      Caption = 'Action9'
      ShortCut = 57
      SecondaryShortCuts.Strings = (
        '9')
      OnExecute = Action9Execute
    end
    object Action10: TAction
      Caption = 'Action10'
      ShortCut = 48
      SecondaryShortCuts.Strings = (
        '0')
      OnExecute = Action10Execute
    end
    object Action11: TAction
      Caption = 'Action11'
      ShortCut = 188
      OnExecute = Action11Execute
    end
    object Action12: TAction
      Caption = 'Action12'
      ShortCut = 8
      OnExecute = Action12Execute
    end
    object Action15: TAction
      Caption = 'Action15'
      ShortCut = 113
      OnExecute = Action15Execute
    end
    object acExit: TAction
      Caption = 'acExit'
      ShortCut = 121
      SecondaryShortCuts.Strings = (
        'Esc')
      OnExecute = acExitExecute
    end
    object acPlus: TAction
      Caption = 'acPlus'
      ShortCut = 107
      SecondaryShortCuts.Strings = (
        'Shift+='
        '+')
      OnExecute = acPlusExecute
    end
    object acMinus: TAction
      Caption = 'acMinus'
      ShortCut = 109
      SecondaryShortCuts.Strings = (
        '-')
      OnExecute = acMinusExecute
    end
    object acMult: TAction
      Caption = 'acMult'
      ShortCut = 106
      OnExecute = acMultExecute
    end
    object acDiv: TAction
      Caption = 'acDiv'
      ShortCut = 111
      SecondaryShortCuts.Strings = (
        '/'
        '/')
      OnExecute = acDivExecute
    end
    object acResult: TAction
      Caption = 'acResult'
      ShortCut = 187
      OnExecute = acResultExecute
    end
  end
end
