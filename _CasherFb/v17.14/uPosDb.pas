unit uPosDb;

interface

uses
  SysUtils, Classes, DB, ADODB;

type
  TdmPOSDB = class(TDataModule)
    msConnPosDb: TADOConnection;
    quSetActive: TADOQuery;
    quSelLoad: TADOQuery;
    quSelLoadID: TLargeintField;
    quSelLoadIACTIVE: TSmallintField;
    quSelLoadITYPE: TSmallintField;
    quSelLoadARTICUL: TIntegerField;
    quSelLoadBARCODE: TStringField;
    quSelLoadCARDSIZE: TStringField;
    quSelLoadQUANT: TFloatField;
    quSelLoadDSTOP: TFloatField;
    quSelLoadNAME: TStringField;
    quSelLoadMESSUR: TStringField;
    quSelLoadPRESISION: TFloatField;
    quSelLoadADD1: TStringField;
    quSelLoadADD2: TStringField;
    quSelLoadADD3: TStringField;
    quSelLoadADDNUM1: TFloatField;
    quSelLoadADDNUM2: TFloatField;
    quSelLoadADDNUM3: TFloatField;
    quSelLoadSCALE: TStringField;
    quSelLoadGROUP1: TIntegerField;
    quSelLoadGROUP2: TIntegerField;
    quSelLoadGROUP3: TIntegerField;
    quSelLoadPRICE: TFloatField;
    quSelLoadCLIINDEX: TIntegerField;
    quSelLoadDELETED: TSmallintField;
    quSelLoadPASSW: TStringField;
    quE: TADOQuery;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmPOSDB: TdmPOSDB;

implementation

{$R *.dfm}

end.
