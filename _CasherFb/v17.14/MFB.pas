unit MFB;

interface

uses
  SysUtils, Classes, FIBDatabase, pFIBDatabase, FIBQuery, pFIBQuery, DB,
  FIBDataSet, pFIBDataSet;

type
  TdmFB = class(TDataModule)
    Cash: TpFIBDatabase;
    trSelect: TpFIBTransaction;
    trUpdate: TpFIBTransaction;
    quSetActive: TpFIBQuery;
    quSelLoad: TpFIBDataSet;
    quSelLoadID: TFIBIntegerField;
    quSelLoadIACTIVE: TFIBSmallIntField;
    quSelLoadITYPE: TFIBSmallIntField;
    quSelLoadARTICUL: TFIBIntegerField;
    quSelLoadBARCODE: TFIBStringField;
    quSelLoadCARDSIZE: TFIBStringField;
    quSelLoadQUANT: TFIBFloatField;
    quSelLoadDSTOP: TFIBFloatField;
    quSelLoadNAME: TFIBStringField;
    quSelLoadMESSUR: TFIBStringField;
    quSelLoadPRESISION: TFIBFloatField;
    quSelLoadADD1: TFIBStringField;
    quSelLoadADD2: TFIBStringField;
    quSelLoadADD3: TFIBStringField;
    quSelLoadADDNUM1: TFIBFloatField;
    quSelLoadADDNUM2: TFIBFloatField;
    quSelLoadADDNUM3: TFIBFloatField;
    quSelLoadSCALE: TFIBStringField;
    quSelLoadGROUP1: TFIBIntegerField;
    quSelLoadGROUP2: TFIBIntegerField;
    quSelLoadGROUP3: TFIBIntegerField;
    quSelLoadPRICE: TFIBFloatField;
    quSelLoadCLIINDEX: TFIBIntegerField;
    quSelLoadDELETED: TFIBSmallIntField;
    quSelLoadPASSW: TFIBStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmFB: TdmFB;

implementation

{$R *.dfm}

end.
