unit CardsFind;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ActnList, XPStyleActnCtrls, ActnMan, ExtCtrls,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit, DB,
  cxDBData, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  cxDataStorage, cxCurrencyEdit;

type
  TfmCardsFind = class(TForm)
    StatusBar1: TStatusBar;
    amCF: TActionManager;
    acExit: TAction;
    Panel1: TPanel;
    CardsTree: TTreeView;
    Panel2: TPanel;
    ViewCF: TcxGridDBTableView;
    LevelCF: TcxGridLevel;
    GridCF: TcxGrid;
    ViewCFARTICUL: TcxGridDBColumn;
    ViewCFNAME: TcxGridDBColumn;
    ViewCFMESURIMENT: TcxGridDBColumn;
    ViewCFPRICE_RUB: TcxGridDBColumn;
    procedure acExitExecute(Sender: TObject);
    procedure CardsTreeExpanding(Sender: TObject; Node: TTreeNode;
      var AllowExpansion: Boolean);
    procedure CardsTreeKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ViewCFKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmCardsFind: TfmCardsFind;

implementation

uses Dm, Un1, MainCasher, UnCash;

{$R *.dfm}

procedure TfmCardsFind.acExitExecute(Sender: TObject);
begin
  if Panel1.Visible then ModalResult:=mrCancel;
  if Panel2.Visible then
  begin
    Panel1.Visible:=True;
    Panel2.Visible:=False;
    CardsTree.SetFocus;
  end;
end;

procedure TfmCardsFind.CardsTreeExpanding(Sender: TObject; Node: TTreeNode;
  var AllowExpansion: Boolean);
begin
  if Node.getFirstChild.Data = nil then
  begin
    Node.DeleteChildren;
    CardsExpandLevel(Node,CardsTree,dmC.quClassifCards);
  end;
end;

procedure TfmCardsFind.CardsTreeKeyPress(Sender: TObject; var Key: Char);
Var bCh:Byte;
begin
  bCh:=ord(Key);
  if bCh = 13 then
  begin //�������� ������
    with dmC do
    begin
      Panel1.Visible:=False;
      Panel2.Visible:=True;
      quCards.Active:=False;
      quCards.ParamByName('IDCLASS').AsInteger:=Integer(CardsTree.Selected.Data);
      quCards.Active:=True;
      GridCF.SetFocus;
    end;
  end;
end;

procedure TfmCardsFind.FormCreate(Sender: TObject);
begin
  Panel1.Align:=alClient;
  Panel1.Visible:=True;
  Panel2.Align:=alClient;
  Panel2.Visible:=False;
end;

procedure TfmCardsFind.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  CountSec:=0;
  fmMainCasher.Timer2.Enabled:=True;
end;

procedure TfmCardsFind.ViewCFKeyPress(Sender: TObject; var Key: Char);
Var bCh:Byte;
begin
  bCh:=ord(Key);
  if bCh = 13 then
  begin //�������� ������
    with dmC do
    begin
      SelPos.Articul:=quCardsARTICUL.AsString;
      if CommonSet.DepartId>0 then SelPos.Depart:=CommonSet.DepartId
      else SelPos.Depart:=quCardsDEPART.AsInteger;
      SelPos.Name:=quCardsNAME.AsString;
      SelPos.Quant:=1;
      SelPos.Price:=quCardsPRICE_RUB.AsFloat;
      SelPos.DProc:=0;
      SelPos.DSum:=0;
      SelPos.Bar:='';
      SelPos.EdIzm:=quCardsMESURIMENT.AsString;

      quCards.Active:=False;
      ModalResult:=mrOk;
    end;
  end;
end;

end.
