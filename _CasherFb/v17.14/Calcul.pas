unit Calcul;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxControls, cxContainer, cxEdit, cxTextEdit, cxMaskEdit,
  cxDropDownEdit, cxCalc, ExtCtrls, cxLookAndFeelPainters, StdCtrls,
  cxButtons, ActnList, XPStyleActnCtrls, ActnMan, cxListBox, cxCurrencyEdit,
  Menus;

type
  TfmCalcul = class(TForm)
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    cxButton5: TcxButton;
    cxButton6: TcxButton;
    cxButton7: TcxButton;
    cxButton8: TcxButton;
    cxButton9: TcxButton;
    cxButton10: TcxButton;
    cxButton11: TcxButton;
    cxButton14: TcxButton;
    amCalc: TActionManager;
    Action1: TAction;
    Action2: TAction;
    Action3: TAction;
    Action4: TAction;
    Action5: TAction;
    Action6: TAction;
    Action7: TAction;
    Action8: TAction;
    Action9: TAction;
    Action10: TAction;
    Action11: TAction;
    Action12: TAction;
    Action15: TAction;
    acExit: TAction;
    List1: TcxListBox;
    acPlus: TAction;
    acMinus: TAction;
    acMult: TAction;
    acDiv: TAction;
    acResult: TAction;
    cxButton12: TcxButton;
    cxButton13: TcxButton;
    cxButton15: TcxButton;
    cxButton16: TcxButton;
    cxButton17: TcxButton;
    CalcEdit1: TcxCalcEdit;
    E1: TcxTextEdit;
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
    procedure cxButton7Click(Sender: TObject);
    procedure cxButton8Click(Sender: TObject);
    procedure cxButton9Click(Sender: TObject);
    procedure cxButton10Click(Sender: TObject);
    procedure cxButton11Click(Sender: TObject);
    procedure Action1Execute(Sender: TObject);
    procedure Action2Execute(Sender: TObject);
    procedure Action3Execute(Sender: TObject);
    procedure Action4Execute(Sender: TObject);
    procedure Action5Execute(Sender: TObject);
    procedure Action6Execute(Sender: TObject);
    procedure Action7Execute(Sender: TObject);
    procedure Action8Execute(Sender: TObject);
    procedure Action9Execute(Sender: TObject);
    procedure Action10Execute(Sender: TObject);
    procedure Action11Execute(Sender: TObject);
    procedure Action12Execute(Sender: TObject);
    procedure Action15Execute(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure acPlusExecute(Sender: TObject);
    procedure acMinusExecute(Sender: TObject);
    procedure acMultExecute(Sender: TObject);
    procedure acDivExecute(Sender: TObject);
    procedure acResultExecute(Sender: TObject);
    procedure cxButton14KeyPress(Sender: TObject; var Key: Char);
    procedure E1KeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
    Procedure prAddPos(s:Char);
  end;

var
  fmCalcul: TfmCalcul;
  bFirst: Boolean = False;
  rResult: Real = 0;
  iStep:ShortInt = 0;

implementation

{$R *.dfm}

Procedure TfmCalcul.prAddPos(s:Char);
begin
  if (iStep=5) and (List1.Items.Count>0) then begin List1.Items.Clear; iStep:=0; rResult:=0; CalcEdit1.EditValue:=0;  end;
  if pos(',',CalcEdit1.Text)=0 then CalcEdit1.EditValue:=CalcEdit1.EditValue*10+StrToIntDef(s,0)
  else CalcEdit1.Text:=CalcEdit1.Text+s;
  if bFirst then begin CalcEdit1.Text:=s; bFirst:=False; end;
  E1.SetFocus;
end;

procedure TfmCalcul.cxButton1Click(Sender: TObject);     
begin
  Action1.Execute;
end;

procedure TfmCalcul.cxButton2Click(Sender: TObject);
begin
  Action2.Execute;
end;

procedure TfmCalcul.cxButton3Click(Sender: TObject);
begin
  Action3.Execute;
end;

procedure TfmCalcul.cxButton4Click(Sender: TObject);
begin
  Action4.Execute;
end;

procedure TfmCalcul.cxButton5Click(Sender: TObject);
begin
  Action5.Execute;
end;

procedure TfmCalcul.cxButton6Click(Sender: TObject);
begin
  Action6.Execute;
end;

procedure TfmCalcul.cxButton7Click(Sender: TObject);
begin
  Action7.Execute;
end;

procedure TfmCalcul.cxButton8Click(Sender: TObject);
begin
  Action8.Execute;
end;

procedure TfmCalcul.cxButton9Click(Sender: TObject);
begin
  Action9.Execute;
end;

procedure TfmCalcul.cxButton10Click(Sender: TObject);
begin
  Action10.Execute;
end;

procedure TfmCalcul.cxButton11Click(Sender: TObject);
begin
  Action11.Execute;
end;

procedure TfmCalcul.Action1Execute(Sender: TObject);
begin
  prAddPos('1');
end;

procedure TfmCalcul.Action2Execute(Sender: TObject);
begin
  prAddPos('2');
end;

procedure TfmCalcul.Action3Execute(Sender: TObject);
begin
  prAddPos('3');
end;

procedure TfmCalcul.Action4Execute(Sender: TObject);
begin
  prAddPos('4');
end;

procedure TfmCalcul.Action5Execute(Sender: TObject);
begin
  prAddPos('5');
end;

procedure TfmCalcul.Action6Execute(Sender: TObject);
begin
  prAddPos('6');
end;

procedure TfmCalcul.Action7Execute(Sender: TObject);
begin
  prAddPos('7');
end;

procedure TfmCalcul.Action8Execute(Sender: TObject);
begin
  prAddPos('8');
end;

procedure TfmCalcul.Action9Execute(Sender: TObject);
begin
  prAddPos('9');
end;

procedure TfmCalcul.Action10Execute(Sender: TObject);
begin
  prAddPos('0');
end;

procedure TfmCalcul.Action11Execute(Sender: TObject);
begin
  if pos(',',CalcEdit1.Text)=0 then CalcEdit1.Text:=CalcEdit1.Text+',';
//  E1.SetFocus;
end;

procedure TfmCalcul.Action12Execute(Sender: TObject);
begin
  CalcEdit1.EditValue:=0;
  List1.Items.Clear;
  rResult:=0;
  iStep:=0;
  E1.SetFocus;
end;

procedure TfmCalcul.Action15Execute(Sender: TObject);
begin
  CalcEdit1.EditValue:=0;
  E1.SetFocus;
end;

procedure TfmCalcul.acExitExecute(Sender: TObject);
begin
  ModalResult:=mrCancel;
end;

procedure TfmCalcul.FormShow(Sender: TObject);
begin
  CalcEdit1.Value:=0;
  bFirst:=True;
  List1.Items.Clear;
  rResult:=0;
  iStep:=0;
  E1.SetFocus;
end;

procedure TfmCalcul.acPlusExecute(Sender: TObject);
Var StrOp:String;
begin
//
//  List1.Items.Add('+');
  Case iStep of
  0: begin
       rResult:=CalcEdit1.EditValue;
     end;
  1: begin
       rResult:=rResult+CalcEdit1.EditValue;
     end;
  2: begin
       rResult:=rResult-CalcEdit1.EditValue;
     end;
  3: begin
       rResult:=rResult*CalcEdit1.EditValue;
     end;
  4: begin
       if CalcEdit1.EditValue<>0 then rResult:=rResult/CalcEdit1.EditValue;
     end;
  5: begin
       if List1.Items.Count>0 then begin List1.Items.Clear; iStep:=0; rResult:=0; CalcEdit1.EditValue:=0;  end;
     end;
  end;
  if CalcEdit1.EditValue<>0 then
  begin
    Str(CalcEdit1.EditValue:10:3,StrOp);
    while StrOp[1]=' ' do delete(StrOp,1,1);
    CalcEdit1.EditValue:=0;
    StrOp:=StrOp+'     +';
    List1.Items.Add(StrOp);
  end;
  iStep:=1;
  E1.SetFocus;
end;

procedure TfmCalcul.acMinusExecute(Sender: TObject);
Var StrOp:String;
begin
//
//  List1.Items.Add('-');
  Case iStep of
  0: begin
       rResult:=CalcEdit1.EditValue;
     end;
  1: begin
       rResult:=rResult+CalcEdit1.EditValue;
     end;
  2: begin
       rResult:=rResult-CalcEdit1.EditValue;
     end;
  3: begin
       rResult:=rResult*CalcEdit1.EditValue;
     end;
  4: begin
       if CalcEdit1.EditValue<>0 then rResult:=rResult/CalcEdit1.EditValue;
     end;
  5: begin
       if List1.Items.Count>0 then begin List1.Items.Clear; iStep:=0; rResult:=0; CalcEdit1.EditValue:=0;  end;
     end;
  end;
  if CalcEdit1.EditValue<>0 then
  begin
    Str(CalcEdit1.EditValue:10:3,StrOp);
    while StrOp[1]=' ' do delete(StrOp,1,1);
    CalcEdit1.EditValue:=0;
    StrOp:=StrOp+'     -';
    List1.Items.Add(StrOp);
  end;
  iStep:=2;
  E1.SetFocus;
end;

procedure TfmCalcul.acMultExecute(Sender: TObject);
Var StrOp:String;
begin
//
//  List1.Items.Add('*');
  Case iStep of
  0: begin
       rResult:=CalcEdit1.EditValue;
     end;
  1: begin
       rResult:=rResult+CalcEdit1.EditValue;
     end;
  2: begin
       rResult:=rResult-CalcEdit1.EditValue;
     end;
  3: begin
       rResult:=rResult*CalcEdit1.EditValue;
     end;
  4: begin
       if CalcEdit1.EditValue<>0 then rResult:=rResult/CalcEdit1.EditValue;
     end;
  5: begin
       if List1.Items.Count>0 then begin List1.Items.Clear; iStep:=0; rResult:=0; CalcEdit1.EditValue:=0;  end;
     end;
  end;
  if CalcEdit1.EditValue<>0 then
  begin
    Str(CalcEdit1.EditValue:10:3,StrOp);
    while StrOp[1]=' ' do delete(StrOp,1,1);
    CalcEdit1.EditValue:=0;
    StrOp:=StrOp+'     *';
    List1.Items.Add(StrOp);
  end;
  iStep:=3;
  E1.SetFocus;
end;

procedure TfmCalcul.acDivExecute(Sender: TObject);
Var StrOp:String;
begin
//
//  List1.Items.Add('/');
  Case iStep of
  0: begin
       rResult:=CalcEdit1.EditValue;
     end;
  1: begin
       rResult:=rResult+CalcEdit1.EditValue;
     end;
  2: begin
       rResult:=rResult-CalcEdit1.EditValue;
     end;
  3: begin
       rResult:=rResult*CalcEdit1.EditValue;
     end;
  4: begin
       if CalcEdit1.EditValue<>0 then rResult:=rResult/CalcEdit1.EditValue;
     end;
  5: begin
       if List1.Items.Count>0 then begin List1.Items.Clear; iStep:=0; rResult:=0; CalcEdit1.EditValue:=0;  end;
     end;
  end;
  if CalcEdit1.EditValue<>0 then
  begin
    Str(CalcEdit1.EditValue:10:3,StrOp);
    while StrOp[1]=' ' do delete(StrOp,1,1);
    CalcEdit1.EditValue:=0;
    StrOp:=StrOp+'     /';
    List1.Items.Add(StrOp);
  end;
  iStep:=4;
  E1.SetFocus;
end;

procedure TfmCalcul.acResultExecute(Sender: TObject);
Var StrOp:String;
begin
//
//  List1.Items.Clear;
//  List1.Items.Add('=');
  Case iStep of
  0: begin
       rResult:=CalcEdit1.EditValue;
     end;
  1: begin
       rResult:=rResult+CalcEdit1.EditValue;
     end;
  2: begin
       rResult:=rResult-CalcEdit1.EditValue;
     end;
  3: begin
       rResult:=rResult*CalcEdit1.EditValue;
     end;
  4: begin
       if CalcEdit1.EditValue<>0 then rResult:=rResult/CalcEdit1.EditValue;
     end;
  5: begin
       exit;
     end;
  end;
  if (rResult<>0)or(List1.Items.Count>0) then
  begin
    if CalcEdit1.EditValue=0 then StrOp:=' = '
    else
    begin
      Str(CalcEdit1.EditValue:10:3,StrOp);
      while StrOp[1]=' ' do delete(StrOp,1,1);
      StrOp:=StrOp+'     =';
    end;
    CalcEdit1.EditValue:=rResult;
    List1.Items.Add(StrOp);
    rResult:=0;
  end;
  iStep:=5;
  E1.SetFocus;
end;

procedure TfmCalcul.cxButton14KeyPress(Sender: TObject; var Key: Char);
begin
  if (Key=#191)or(Key=#110)or(Key=#188)or(Key=#110)or(Key='.')or(Key='/')or(Key='�') then Key:=',';

  if Key='1' then prAddPos('1');
  if Key='2' then prAddPos('2');
  if Key='3' then prAddPos('3');
  if Key='4' then prAddPos('4');
  if Key='5' then prAddPos('5');
  if Key='6' then prAddPos('6');
  if Key='7' then prAddPos('7');
  if Key='8' then prAddPos('8');
  if Key='9' then prAddPos('9');
  if Key='0' then prAddPos('0');

end;

procedure TfmCalcul.E1KeyPress(Sender: TObject; var Key: Char);
begin
  if (Key=#191)or(Key=#110)or(Key=#188)or(Key=#110)or(Key='�')or(Key='.') then
  begin
    if pos(',',CalcEdit1.Text)=0 then CalcEdit1.Text:=CalcEdit1.Text+',';
  end;
  if (Key='/')or(Key=' ') then
  begin
    acDiv.Execute;
  end;
  if (Key='*') then acMult.Execute;

  if Key='1' then prAddPos('1');
  if Key='2' then prAddPos('2');
  if Key='3' then prAddPos('3');
  if Key='4' then prAddPos('4');
  if Key='5' then prAddPos('5');
  if Key='6' then prAddPos('6');
  if Key='7' then prAddPos('7');
  if Key='8' then prAddPos('8');
  if Key='9' then prAddPos('9');
  if Key='0' then prAddPos('0');

end;

end.

