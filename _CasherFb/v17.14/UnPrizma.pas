unit UnPrizma;

interface
uses
//  ������� ������

  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ComObj, ActiveX, OleServer, StdCtrls;

Function   OpenDrv:Boolean;
Function   CloseDrv:Boolean;
function   IsOLEObjectInstalled(Name: String): boolean;
Function   Event_Reg(iNum:INteger):Boolean;

var

  Pri: OleVariant;
  iRfr:SmallInt;
  Prizma:Boolean = False;

implementation

uses Un1, UnCash;



Function   Event_Reg(iNum:INteger):Boolean;
Var sD:String;
begin
  Result:=True;
  try
    if Prizma then
    begin
      Pri.Prefix:='KKM';
      Pri.Number:=CommonSet.CashNum;
      Pri.CassirItem:=Person.Id;
      Pri.Cassir:=Person.Name;
      Pri.Host:=CommonSet.PrizmaIP;
      Pri.Port:=21845;
      Pri.UseDate:=1;
      Pri.PacketDate:=formatdatetime('ddmmyy000ssnnhh',now);  //"211209120031511";

      if iNum in [5,35,37,38,47] then //������
      begin
        sD:='0';
        if iNum=35 then sD:='D';

        Pri.SendEvent(iNum,Nums.iCheckNum,SelPos.NumPos,'','','',0,0,0,Check.RSum,sD,Check.Discount,0,Check.DSum);
      end else
      begin
        Pri.SendEvent(iNum,Nums.iCheckNum,SelPos.NumPos,SelPos.Bar,SelPos.Articul,SelPos.Name,SelPos.Price,SelPos.Quant,SelPos.Quant*SelPos.Price,Check.RSum,'0','0',fs(SelPos.DProc),SelPos.DSum);
      end;
   //Mode CK_Number Count BarCode GoodsItem GoodsName GoodsPrice GoodsQuant GoodsSum Sum CardType CardNumber DiscStr DiscSum
    end;
  except
    Result:=False;
  end;
end;


function IsOLEObjectInstalled(Name: String): boolean;
var
  ClassID: TCLSID;
  Rez : HRESULT;
begin

// ���� CLSID OLE-�������
  Rez := CLSIDFromProgID(PWideChar(WideString(Name)), ClassID);
  if Rez = S_OK then
 // ������ ������
    Result := true
  else
    Result := false;
end;

Function CloseDrv:Boolean;
Var iRet:Integer;
begin
  if CommonSet.Prizma=0 then begin  Result:=True; exit;  end;
  Result:=False;
  iRet:=Pri.Disconnect;
  if iRet=0 then result:=True;
end;

Function OpenDrv:Boolean;
begin
  if CommonSet.Prizma=0 then begin  Result:=True; exit;  end;
  if not IsOLEObjectInstalled('PrismSender.PPacket') then
  begin
    showmessage('������� ������ �� ����������. !!!');
    Result:=False;
    exit;
  end;
  try
   Pri:=CreateOleObject('PrismSender.PPacket');
   Prizma:=True;
  except
    showmessage('���������� ������� ������ "PrismSender.PPacket". !!!');
    Result:=False;
    exit;
  end;

  Result:=True;
end;

end.
