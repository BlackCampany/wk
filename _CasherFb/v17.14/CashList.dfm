object fmCashList: TfmCashList
  Left = 908
  Top = 98
  Width = 660
  Height = 860
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 652
    Height = 41
    Align = alTop
    BevelInner = bvLowered
    Color = 16763025
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 24
      Top = 4
      Width = 257
      Height = 33
      Caption = #1042#1099#1087#1086#1083#1085#1080#1090#1100' '#1079#1072#1087#1088#1086#1089' '#1087#1086' '#1074#1099#1076#1077#1083#1077#1085#1085#1099#1084' '#1082#1072#1089#1089#1072#1084' '
      TabOrder = 0
      OnClick = cxButton1Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 528
      Top = 4
      Width = 101
      Height = 33
      Caption = #1055#1088#1086#1074#1077#1088#1082#1072' '#1089#1074#1103#1079#1080
      TabOrder = 1
      OnClick = cxButton2Click
      LookAndFeel.Kind = lfOffice11
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 41
    Width = 652
    Height = 128
    Align = alTop
    Caption = #1058#1077#1082#1089#1090' '#1079#1072#1087#1088#1086#1089#1072
    TabOrder = 1
    object MemoSQL: TcxMemo
      Left = 2
      Top = 15
      Align = alClient
      Lines.Strings = (
        'Memo1')
      Style.BorderStyle = ebsOffice11
      TabOrder = 0
      Height = 111
      Width = 648
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 169
    Width = 401
    Height = 657
    Align = alLeft
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 2
    object GrCash: TcxGrid
      Left = 2
      Top = 2
      Width = 397
      Height = 653
      Align = alClient
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
      object ViCash: TcxGridDBTableView
        PopupMenu = PopupMenu1
        NavigatorButtons.ConfirmDelete = False
        DataController.DataSource = fmMainCashUpd.dsquCashList
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        object ViCashIST: TcxGridDBColumn
          Caption = #1042#1099#1073#1088#1072#1090#1100
          DataBinding.FieldName = 'IST'
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.ValueChecked = 1
          Properties.ValueUnchecked = 0
          Width = 32
        end
        object ViCashISHOP: TcxGridDBColumn
          Caption = #1052#1072#1075#1072#1079#1080#1085
          DataBinding.FieldName = 'ISHOP'
          Width = 32
        end
        object ViCashINUM: TcxGridDBColumn
          Caption = #8470' '#1082#1072#1089#1089#1099
          DataBinding.FieldName = 'INUM'
          Width = 30
        end
        object ViCashIRES: TcxGridDBColumn
          Caption = #1056#1077#1079#1091#1083#1100#1090#1072#1090
          DataBinding.FieldName = 'IRES'
          Width = 38
        end
        object ViCashSPATH: TcxGridDBColumn
          Caption = #1055#1091#1090#1100
          DataBinding.FieldName = 'SPATH'
          Width = 237
        end
      end
      object LeCash: TcxGridLevel
        GridView = ViCash
      end
    end
  end
  object Memo2: TcxMemo
    Left = 401
    Top = 169
    Align = alClient
    Lines.Strings = (
      'Memo2')
    TabOrder = 3
    Height = 657
    Width = 251
  end
  object amCashList: TActionManager
    Left = 112
    Top = 305
    StyleName = 'XP Style'
    object acSetAll: TAction
      Caption = #1042#1099#1076#1077#1083#1080#1090#1100' '#1074#1089#1077
      OnExecute = acSetAllExecute
    end
    object acResetAll: TAction
      Caption = #1057#1085#1103#1090#1100' '#1074#1099#1076#1077#1083#1077#1085#1080#1103
      OnExecute = acResetAllExecute
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 100
    Top = 357
    object N1: TMenuItem
      Action = acSetAll
    end
    object N2: TMenuItem
      Action = acResetAll
    end
  end
  object CashDB: TpFIBDatabase
    DBName = '192.168.1.119:D:\_Casher\db\casherdb.gdb'
    DBParams.Strings = (
      'user_name=SYSDBA'
      'lc_ctype=WIN1251'
      'password=masterkey'
      'sql_role_name=SYSDBA')
    DefaultTransaction = trSelect
    DefaultUpdateTransaction = trUpdate
    SQLDialect = 3
    Timeout = 0
    SynchronizeTime = False
    DesignDBOptions = []
    AliasName = 'CasherFb'
    WaitForRestoreConnect = 20
    Left = 316
    Top = 252
  end
  object trSelect: TpFIBTransaction
    DefaultDatabase = CashDB
    TimeoutAction = TARollback
    Left = 316
    Top = 308
  end
  object trUpdate: TpFIBTransaction
    DefaultDatabase = CashDB
    TimeoutAction = TARollback
    Left = 316
    Top = 364
  end
  object trDel: TpFIBTransaction
    DefaultDatabase = CashDB
    Timeout = 300
    TimeoutAction = TARollback
    Left = 316
    Top = 424
  end
  object quUpdCash: TpFIBQuery
    Transaction = trUpdate
    Database = CashDB
    Left = 260
    Top = 309
    qoAutoCommit = True
    qoStartTransaction = True
  end
end
