program SQLConv2;

uses
  Forms,
  MainSQLConv in 'MainSQLConv.pas' {fmMainSQLConv},
  Dm in 'Dm.pas' {dmC: TDataModule},
  HistoryTr in 'HistoryTr.pas' {fmHistoryTr},
  Un1 in 'Un1.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfmMainSQLConv, fmMainSQLConv);
  Application.CreateForm(TdmC, dmC);
  Application.CreateForm(TfmHistoryTr, fmHistoryTr);
  Application.Run;
end.
