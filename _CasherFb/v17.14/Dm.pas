unit Dm;

interface

uses
  SysUtils, Classes, DB, ADODB, ImgList, Controls, FIBDatabase,
  pFIBDatabase, FIBDataSet, pFIBDataSet, FIBQuery, pFIBQuery,
  pFIBStoredProc, cxStyles, VaComm, VaClasses, VaSystem, WinSpool, Windows,
  httpsend, synautil, nativexml, IniFiles,
  dxmdaset, IB_Services, CPort, ScktComp;

type
  TdmC = class(TDataModule)
    dsPersonal: TDataSource;
    dsClassif: TDataSource;
    dsFuncList: TDataSource;
    imState: TImageList;
    CasherDb: TpFIBDatabase;
    trSelect: TpFIBTransaction;
    trUpdate: TpFIBTransaction;
    quPer: TpFIBDataSet;
    quPerID: TFIBIntegerField;
    quPerID_PARENT: TFIBIntegerField;
    quPerNAME: TFIBStringField;
    quPerUVOLNEN: TFIBBooleanField;
    quPerCheck: TFIBStringField;
    quPerMODUL1: TFIBBooleanField;
    quPerMODUL2: TFIBBooleanField;
    quPerMODUL3: TFIBBooleanField;
    quPerMODUL4: TFIBBooleanField;
    quPerMODUL5: TFIBBooleanField;
    quPerMODUL6: TFIBBooleanField;
    dsPer: TDataSource;
    taPersonal: TpFIBDataSet;
    taPersonalID: TFIBIntegerField;
    taPersonalID_PARENT: TFIBIntegerField;
    taPersonalNAME: TFIBStringField;
    taPersonalUVOLNEN: TFIBBooleanField;
    taPersonalPASSW: TFIBStringField;
    taPersonalMODUL1: TFIBBooleanField;
    taPersonalMODUL2: TFIBBooleanField;
    taPersonalMODUL3: TFIBBooleanField;
    taPersonalMODUL4: TFIBBooleanField;
    taPersonalMODUL5: TFIBBooleanField;
    taPersonalMODUL6: TFIBBooleanField;
    taRClassif: TpFIBDataSet;
    quFuncList: TpFIBDataSet;
    taRClassifID_PERSONAL: TFIBIntegerField;
    taRClassifID_CLASSIF: TFIBIntegerField;
    taRClassifRIGHTS: TFIBIntegerField;
    quFuncListID_PERSONAL: TFIBIntegerField;
    quFuncListNAME: TFIBStringField;
    quFuncListPREXEC: TFIBBooleanField;
    quFuncListCOMMENT: TFIBStringField;
    quClassif: TpFIBDataSet;
    quClassifID_CLASSIF: TFIBIntegerField;
    quClassifRIGHTS: TFIBIntegerField;
    quClassifID_PARENT: TFIBIntegerField;
    quClassifNAME: TFIBStringField;
    quPersonal: TpFIBDataSet;
    prChangeRFunction: TpFIBStoredProc;
    prExistPersonal: TpFIBStoredProc;
    prDelPersonal: TpFIBStoredProc;
    taFuncList: TpFIBDataSet;
    taFuncListNAME: TFIBStringField;
    taFuncListCOMMENT: TFIBStringField;
    trDel: TpFIBTransaction;
    quCanDo: TpFIBDataSet;
    quCanDoID_PERSONAL: TFIBIntegerField;
    quCanDoNAME: TFIBStringField;
    quCanDoPREXEC: TFIBBooleanField;
    taStates: TpFIBDataSet;
    taStatesID: TFIBIntegerField;
    taStatesNAME: TFIBStringField;
    taStatesBARCODE: TFIBStringField;
    taStatesKEY_POSITION: TFIBIntegerField;
    taStatesID_DEVICE: TFIBIntegerField;
    taCF_Operations: TpFIBDataSet;
    taOperations: TpFIBDataSet;
    taOperationsID: TFIBIntegerField;
    taOperationsNAME: TFIBStringField;
    taOperationsID_DEVICE: TFIBIntegerField;
    taClassif: TpFIBDataSet;
    taClassifID: TFIBIntegerField;
    taClassifID_PARENT: TFIBIntegerField;
    taClassifTYPE_CLASSIF: TFIBIntegerField;
    taClassifNAME: TFIBStringField;
    taDeparts: TpFIBDataSet;
    taDepartsID: TFIBIntegerField;
    taDepartsNAME: TFIBStringField;
    taDepartsPAR1: TFIBIntegerField;
    taDepartsPAR2: TFIBIntegerField;
    taCardScla: TpFIBDataSet;
    taCardSclaARTICUL: TFIBStringField;
    taCardSclaCLASSIF: TFIBIntegerField;
    taCardSclaDEPART: TFIBIntegerField;
    taCardSclaNAME: TFIBStringField;
    taCardSclaCARD_TYPE: TFIBIntegerField;
    taCardSclaMESURIMENT: TFIBStringField;
    taCardSclaPRICE_RUB: TFIBFloatField;
    taCardSclaDISCOUNT: TFIBFloatField;
    taCardSclaSCALE: TFIBStringField;
    dsCF_Operations: TDataSource;
    taCF_OperationsID_OPERATIONS: TFIBIntegerField;
    taCF_OperationsKEY_CODE: TFIBIntegerField;
    taCF_OperationsKEY_STATUS: TFIBIntegerField;
    taCF_OperationsKEY_CHAR: TFIBStringField;
    taCF_OperationsID_STATES: TFIBIntegerField;
    taCF_OperationsBARCODE: TFIBStringField;
    taCF_OperationsKEY_POSITION: TFIBIntegerField;
    taCF_OperationsID_CLASSIF: TFIBIntegerField;
    taCF_OperationsARTICUL: TFIBStringField;
    taCF_OperationsID_DEPARTS: TFIBIntegerField;
    taCF_OperationsBAR: TFIBStringField;
    taCF_OperationsNAME: TFIBStringField;
    taCF_OperationsNAME1: TFIBStringField;
    dsOperations: TDataSource;
    dsStates: TDataSource;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    quClassifR: TpFIBDataSet;
    quClassifRTYPE_CLASSIF: TFIBIntegerField;
    quClassifRID: TFIBIntegerField;
    quClassifRIACTIVE: TFIBSmallIntField;
    quClassifRID_PARENT: TFIBIntegerField;
    quClassifRNAME: TFIBStringField;
    quClassifRIEDIT: TFIBSmallIntField;
    taCF_OperationsNAMECL: TFIBStringField;
    taCF_All: TpFIBDataSet;
    taCF_AllID_OPERATIONS: TFIBIntegerField;
    taCF_AllKEY_CODE: TFIBIntegerField;
    taCF_AllKEY_STATUS: TFIBIntegerField;
    taCF_AllKEY_CHAR: TFIBStringField;
    taCF_AllID_STATES: TFIBIntegerField;
    taCF_AllBARCODE: TFIBStringField;
    taCF_AllKEY_POSITION: TFIBIntegerField;
    taCF_AllID_CLASSIF: TFIBIntegerField;
    taCF_AllARTICUL: TFIBStringField;
    taCF_AllID_DEPARTS: TFIBIntegerField;
    taCF_AllBAR: TFIBStringField;
    dsCardScla: TDataSource;
    taCardSclaBARCODE: TFIBStringField;
    taCardSclaPRICEBAR: TFIBFloatField;
    taCF_OperationsNAMECS: TFIBStringField;
    dsDeparts: TDataSource;
    taCF_OperationsNAMEDEP: TFIBStringField;
    taBarCode: TpFIBDataSet;
    taBarCodeBARCODE: TFIBStringField;
    taBarCodeCARDARTICUL: TFIBStringField;
    taBarCodeCARDSIZE: TFIBStringField;
    taBarCodePRICERUB: TFIBFloatField;
    taBarCodeQUANTITY: TFIBFloatField;
    taClassifIACTIVE: TFIBSmallIntField;
    taClassifIEDIT: TFIBSmallIntField;
    taTax: TpFIBDataSet;
    taTaxARTICUL: TFIBStringField;
    taTaxTAXINDEX: TFIBSmallIntField;
    taTaxTAX: TFIBFloatField;
    taTaxType: TpFIBDataSet;
    taTaxTypeID: TFIBIntegerField;
    taTaxTypePRIOR: TFIBSmallIntField;
    taTaxTypeNAME: TFIBStringField;
    taDiscSum: TpFIBDataSet;
    taCardSclaDISCQUANT: TFIBFloatField;
    taDiscCard: TpFIBDataSet;
    taDiscCardBARCODE: TFIBStringField;
    taDiscCardNAME: TFIBStringField;
    taDiscCardPERCENT: TFIBFloatField;
    taDiscCardCLIENTINDEX: TFIBIntegerField;
    prSetDiscCardStop: TpFIBStoredProc;
    taDiscSumFROMTIME: TFIBIntegerField;
    taDiscSumBARCODE: TFIBStringField;
    taDiscSumSUMMA: TFIBFloatField;
    taDiscSumDISCOUNT: TFIBFloatField;
    taCredCard: TpFIBDataSet;
    taCredCardID: TFIBSmallIntField;
    taCredCardNAME: TFIBStringField;
    taCredCardCLIENTINDEX: TFIBIntegerField;
    taCredCardLIMITSUM: TFIBFloatField;
    taCredCardCANRETURN: TFIBIntegerField;
    taCredPref: TpFIBDataSet;
    taCredPrefPREFIX: TFIBStringField;
    taCredPrefID_CARD: TFIBIntegerField;
    taPersonalBARCODE: TFIBStringField;
    taCheck: TpFIBDataSet;
    dsCheck: TDataSource;
    trSelCh: TpFIBTransaction;
    trUpdCh: TpFIBTransaction;
    quFindBar: TpFIBDataSet;
    quFindBarBARCODE: TFIBStringField;
    quFindBarCARDSIZE: TFIBStringField;
    quFindBarQUANTITY: TFIBFloatField;
    quFindBarPRICERUB: TFIBFloatField;
    quFindBarARTICUL: TFIBStringField;
    quFindBarDEPART: TFIBIntegerField;
    quFindBarNAME: TFIBStringField;
    quFindBarPRICE_RUB: TFIBFloatField;
    prSaveCheck: TpFIBStoredProc;
    quClassifCards: TpFIBDataSet;
    quCards: TpFIBDataSet;
    dsCards: TDataSource;
    quCardsARTICUL: TFIBStringField;
    quCardsCLASSIF: TFIBIntegerField;
    quCardsDEPART: TFIBIntegerField;
    quCardsNAME: TFIBStringField;
    quCardsCARD_TYPE: TFIBIntegerField;
    quCardsMESURIMENT: TFIBStringField;
    quCardsPRICE_RUB: TFIBFloatField;
    quCardsDISCQUANT: TFIBFloatField;
    quCardsDISCOUNT: TFIBFloatField;
    quCardsSCALE: TFIBStringField;
    quCardsFind: TpFIBDataSet;
    quCardsFindARTICUL: TFIBStringField;
    quCardsFindCLASSIF: TFIBIntegerField;
    quCardsFindDEPART: TFIBIntegerField;
    quCardsFindNAME: TFIBStringField;
    quCardsFindCARD_TYPE: TFIBIntegerField;
    quCardsFindMESURIMENT: TFIBStringField;
    quCardsFindPRICE_RUB: TFIBFloatField;
    quCardsFindDISCQUANT: TFIBFloatField;
    quCardsFindDISCOUNT: TFIBFloatField;
    quCardsFindSCALE: TFIBStringField;
    taDiscPre: TpFIBDataSet;
    taDiscPreBARCODE: TFIBStringField;
    taDiscPreNAME: TFIBStringField;
    taDiscPrePERCENT: TFIBFloatField;
    taDiscPreIACTIVE: TFIBSmallIntField;
    taDiscFind: TpFIBDataSet;
    taDiscFindBARCODE: TFIBStringField;
    taDiscFindNAME: TFIBStringField;
    taDiscFindPERCENT: TFIBFloatField;
    taDiscFindCLIENTINDEX: TFIBIntegerField;
    taDiscFindIACTIVE: TFIBSmallIntField;
    prCalcDisc: TpFIBStoredProc;
    cxStyle10: TcxStyle;
    prClearCheck: TpFIBStoredProc;
    prFormLog: TpFIBStoredProc;
    prCalcDiscSum: TpFIBStoredProc;
    trSelBar: TpFIBTransaction;
    prAddCard: TpFIBStoredProc;
    prAddBar: TpFIBStoredProc;
    prReadCheck: TpFIBStoredProc;
    quTestSail: TpFIBDataSet;
    dsCredCard: TDataSource;
    taCheckTr: TpFIBDataSet;
    prClearCheckTr: TpFIBStoredProc;
    taCheckTrCASHNUM: TFIBIntegerField;
    taCheckTrNUMPOS: TFIBIntegerField;
    taCheckTrARTICUL: TFIBStringField;
    taCheckTrRBAR: TFIBStringField;
    taCheckTrNAME: TFIBStringField;
    taCheckTrQUANT: TFIBFloatField;
    taCheckTrPRICE: TFIBFloatField;
    taCheckTrDPROC: TFIBFloatField;
    taCheckTrDSUM: TFIBFloatField;
    taCheckTrRSUM: TFIBFloatField;
    taCheckTrDEPART: TFIBSmallIntField;
    taCheckTrOPERATION: TFIBSmallIntField;
    taCheckTrTYPEP: TFIBSmallIntField;
    taCheckTrVIDP: TFIBSmallIntField;
    prSaveCheckTr: TpFIBStoredProc;
    taMoneyType: TpFIBDataSet;
    taMoneyTypeID: TFIBSmallIntField;
    taMoneyTypeIVAL: TFIBIntegerField;
    quCheck: TpFIBDataSet;
    quCheckARTICUL: TFIBStringField;
    quCheckRBAR: TFIBStringField;
    quCheckNAME: TFIBStringField;
    quCheckPRICE: TFIBFloatField;
    quCheckDEPART: TFIBSmallIntField;
    quCheckDPROC: TFIBFloatField;
    quCheckNUMPOS: TFIBIntegerField;
    quCheckQUANT: TFIBFloatField;
    quCheckDSUM: TFIBFloatField;
    quCheckPRSUM: TFIBFloatField;
    trMoneySel: TpFIBTransaction;
    quMoneyLast: TpFIBDataSet;
    quMoneyLastINOUTDATE: TFIBDateTimeField;
    quMoneyLastENDSUM: TFIBFloatField;
    quMoneyLastID: TFIBIntegerField;
    quCashSum: TpFIBDataSet;
    trCashSel: TpFIBTransaction;
    quCashSumCASHSUM: TFIBFloatField;
    taMoneySum: TpFIBDataSet;
    trMoneyUpd: TpFIBTransaction;
    taMoneySumID: TFIBIntegerField;
    taMoneySumINOUTDATE: TFIBDateTimeField;
    taMoneySumBEGSUM: TFIBFloatField;
    taMoneySumINOUTSUM: TFIBFloatField;
    taMoneySumENDSUM: TFIBFloatField;
    taMoneySumPERSONID: TFIBIntegerField;
    prDelCard: TpFIBStoredProc;
    trDelCard: TpFIBTransaction;
    trBN: TpFIBTransaction;
    prAddBNTR: TpFIBStoredProc;
    quBnTr: TpFIBDataSet;
    quBnTrBNBAR: TFIBStringField;
    quBnTrIDTR: TFIBIntegerField;
    quBnTrPOINTNUM: TFIBIntegerField;
    quBnTrCOMMENT: TFIBStringField;
    quBnTrBNSUM: TFIBFloatField;
    quBnTrAUTHCODE: TFIBStringField;
    quBnTrCARDTYPE: TFIBStringField;
    quPersName: TpFIBDataSet;
    trPers: TpFIBTransaction;
    quPersNameNAME: TFIBStringField;
    trSelChTr: TpFIBTransaction;
    trUpdChTr: TpFIBTransaction;
    quDel: TpFIBQuery;
    trSelMessur: TpFIBTransaction;
    quIM: TpFIBDataSet;
    quIMCARD_TYPE: TFIBIntegerField;
    quIMMESURIMENT: TFIBStringField;
    taFixDisc: TpFIBDataSet;
    taFixDiscSART: TFIBStringField;
    taFixDiscPROC: TFIBFloatField;
    taCheckCASHNUM: TFIBIntegerField;
    taCheckNUMPOS: TFIBIntegerField;
    taCheckARTICUL: TFIBStringField;
    taCheckRBAR: TFIBStringField;
    taCheckNAME: TFIBStringField;
    taCheckQUANT: TFIBFloatField;
    taCheckPRICE: TFIBFloatField;
    taCheckDPROC: TFIBFloatField;
    taCheckDSUM: TFIBFloatField;
    taCheckRSUM: TFIBFloatField;
    taCheckDEPART: TFIBSmallIntField;
    taDiscCardIACTIVE: TFIBSmallIntField;
    VaWaitMessage1: TVaWaitMessage;
    DevPrint: TVaComm;
    trSel1: TpFIBTransaction;
    prGetNFChNum: TpFIBStoredProc;
    prGetId: TpFIBStoredProc;
    taCheckEDIZM: TFIBStringField;
    quFindBarMESURIMENT: TFIBStringField;
    quDSum: TpFIBDataSet;
    quDSumOPER: TFIBSmallIntField;
    quDSumPAYMENT: TFIBSmallIntField;
    quDSumDSUM: TFIBFloatField;
    quNarItog: TpFIBDataSet;
    quNarItogSUMSALE: TFIBFloatField;
    quNarItogSUMRET: TFIBFloatField;
    quZList: TpFIBDataSet;
    quZListIZ: TFIBIntegerField;
    quZListSUMSALE: TFIBFloatField;
    quZListSUMRET: TFIBFloatField;
    prAddClassif: TpFIBStoredProc;
    quNameClFind: TpFIBDataSet;
    quNameClFindNAME: TFIBStringField;
    quZListDet: TpFIBDataSet;
    quZListDetCASHNUM: TFIBSmallIntField;
    quZListDetZNUM: TFIBIntegerField;
    quZListDetINOUT: TFIBSmallIntField;
    quZListDetITYPE: TFIBSmallIntField;
    quZListDetRSUM: TFIBFloatField;
    quZListDetIDATE: TFIBIntegerField;
    quZListDetDDATE: TFIBDateTimeField;
    quTabDBList: TpFIBDataSet;
    quTabDBListRDBRELATION_NAME: TFIBWideStringField;
    quTabDBListRDBFIELD_POSITION: TFIBSmallIntField;
    quTabDBListRDBFIELD_NAME: TFIBWideStringField;
    quUpd: TpFIBQuery;
    quFindCheck: TpFIBDataSet;
    quFindCheckNAME: TFIBStringField;
    quFindCheckMESURIMENT: TFIBStringField;
    quFindCheckZNUMBER: TFIBIntegerField;
    quFindCheckCHECKNUMBER: TFIBIntegerField;
    quFindCheckID: TFIBIntegerField;
    quFindCheckCHDATE: TFIBDateTimeField;
    quFindCheckARTICUL: TFIBStringField;
    quFindCheckBAR: TFIBStringField;
    quFindCheckQUANTITY: TFIBFloatField;
    quFindCheckPRICERUB: TFIBFloatField;
    quFindCheckDPROC: TFIBFloatField;
    quFindCheckDSUM: TFIBFloatField;
    quFindCheckTOTALRUB: TFIBFloatField;
    quFindCheckCASHER: TFIBIntegerField;
    quFindCheckDEPART: TFIBIntegerField;
    quFindCheckOPERATION: TFIBSmallIntField;
    quReservCh: TpFIBDataSet;
    quReservChCASHNUM: TFIBIntegerField;
    quReservChZNUM: TFIBIntegerField;
    quReservChCHECKNUM: TFIBIntegerField;
    quReservChNUMPOS: TFIBIntegerField;
    quReservChARTICUL: TFIBStringField;
    quReservChRBAR: TFIBStringField;
    quReservChNAME: TFIBStringField;
    quReservChQUANT: TFIBFloatField;
    quReservChPRICE: TFIBFloatField;
    quReservChDPROC: TFIBFloatField;
    quReservChDSUM: TFIBFloatField;
    quReservChRSUM: TFIBFloatField;
    quReservChDEPART: TFIBSmallIntField;
    quReservChEDIZM: TFIBStringField;
    trUpd1: TpFIBTransaction;
    quResChH: TpFIBDataSet;
    quResChHCASHNUM: TFIBIntegerField;
    quResChHZNUM: TFIBIntegerField;
    quResChHCHECKNUM: TFIBIntegerField;
    quResChHRSUM: TFIBFloatField;
    dsquResChH: TDataSource;
    dsquReservCh: TDataSource;
    quKeys: TpFIBDataSet;
    quKeysIKT: TFIBIntegerField;
    quKeysID: TFIBIntegerField;
    quKeysHOTKEY: TFIBStringField;
    quKeysACTIONNAME: TFIBStringField;
    quCheck0: TpFIBDataSet;
    quCheck0SHOPINDEX: TFIBSmallIntField;
    quCheck0CASHNUMBER: TFIBIntegerField;
    quCheck0ZNUMBER: TFIBIntegerField;
    quCheck0CHECKNUMBER: TFIBIntegerField;
    quCheck0ID: TFIBIntegerField;
    quCheck0CHDATE: TFIBDateTimeField;
    quCheck0ARTICUL: TFIBStringField;
    quCheck0BAR: TFIBStringField;
    quCheck0CARDSIZE: TFIBStringField;
    quCheck0QUANTITY: TFIBFloatField;
    quCheck0PRICERUB: TFIBFloatField;
    quCheck0DPROC: TFIBFloatField;
    quCheck0DSUM: TFIBFloatField;
    quCheck0TOTALRUB: TFIBFloatField;
    quCheck0CASHER: TFIBIntegerField;
    quCheck0DEPART: TFIBIntegerField;
    quCheck0OPERATION: TFIBSmallIntField;
    trSaveChTr: TpFIBTransaction;
    quDelPers: TpFIBQuery;
    cxStyle24: TcxStyle;
    quHotKey: TpFIBDataSet;
    quHotKeyCASHNUM: TFIBIntegerField;
    quHotKeyIROW: TFIBSmallIntField;
    quHotKeyICOL: TFIBSmallIntField;
    quHotKeySIFR: TFIBStringField;
    quHotKeyNAME: TFIBStringField;
    quHotKeyPRICE_RUB: TFIBFloatField;
    cxStyle28: TcxStyle;
    devCas: TVaComm;
    VaWaitMessageCas: TVaWaitMessage;
    quClearPasswPers: TpFIBQuery;
    taCheckTrTIMEPOS: TFIBDateTimeField;
    taCheckTIMEPOS: TFIBDateTimeField;
    quCheckTIMEPOS: TFIBDateTimeField;
    quFindBar1: TpFIBDataSet;
    quFindBar1BARCODE: TFIBStringField;
    quFindBar1CARDSIZE: TFIBStringField;
    quFindBar1QUANTITY: TFIBFloatField;
    quFindBar1PRICERUB: TFIBFloatField;
    quTabDBListI: TpFIBDataSet;
    quTabDBListIRDBINDEX_NAME: TFIBWideStringField;
    quCheck0SECPOS: TFIBSmallIntField;
    quCheck0ECHECK: TFIBSmallIntField;
    prSAVECHECKANN: TpFIBStoredProc;
    quProcDBList: TpFIBDataSet;
    quProcDBListRDBPROCEDURE_NAME: TFIBWideStringField;
    quFACount: TpFIBDataSet;
    trSel2: TpFIBTransaction;
    quFACountACOUNT: TFIBIntegerField;
    teVesT: TdxMemData;
    teVesTArticul: TIntegerField;
    quTestVes: TpFIBDataSet;
    quTestVesARTICUL: TFIBStringField;
    teTestVes1: TdxMemData;
    teTestVes1Num: TSmallintField;
    teTestVes1Articul: TStringField;
    teTestVes1BarCode: TStringField;
    teTestVes1Name: TStringField;
    teTestVes1Quant: TFloatField;
    teTestVes1Price: TFloatField;
    teTestVes1rSum: TFloatField;
    quChecks: TpFIBDataSet;
    quChecksSHOPINDEX: TFIBSmallIntField;
    quChecksCASHNUMBER: TFIBIntegerField;
    quChecksZNUMBER: TFIBIntegerField;
    quChecksCHECKNUMBER: TFIBIntegerField;
    quChecksTOTALRUB: TFIBFloatField;
    quChecksDSUM: TFIBFloatField;
    quCheckSel: TpFIBDataSet;
    quCheckSelSHOPINDEX: TFIBSmallIntField;
    quCheckSelCASHNUMBER: TFIBIntegerField;
    quCheckSelZNUMBER: TFIBIntegerField;
    quCheckSelCHECKNUMBER: TFIBIntegerField;
    quCheckSelID: TFIBIntegerField;
    quCheckSelCHDATE: TFIBDateTimeField;
    quCheckSelARTICUL: TFIBStringField;
    quCheckSelBAR: TFIBStringField;
    quCheckSelCARDSIZE: TFIBStringField;
    quCheckSelQUANTITY: TFIBFloatField;
    quCheckSelPRICERUB: TFIBFloatField;
    quCheckSelDPROC: TFIBFloatField;
    quCheckSelDSUM: TFIBFloatField;
    quCheckSelTOTALRUB: TFIBFloatField;
    quCheckSelCASHER: TFIBIntegerField;
    quCheckSelDEPART: TFIBIntegerField;
    quCheckSelOPERATION: TFIBSmallIntField;
    quCheckSelSECPOS: TFIBSmallIntField;
    quCheckSelECHECK: TFIBSmallIntField;
    quCheckPSel: TpFIBDataSet;
    quCheckPSelSHOPINDEX: TFIBSmallIntField;
    quCheckPSelCASHNUMBER: TFIBIntegerField;
    quCheckPSelZNUMBER: TFIBIntegerField;
    quCheckPSelCHECKNUMBER: TFIBIntegerField;
    quCheckPSelCASHER: TFIBIntegerField;
    quCheckPSelCHECKSUM: TFIBFloatField;
    quCheckPSelPAYSUM: TFIBFloatField;
    quCheckPSelDSUM: TFIBFloatField;
    quCheckPSelDBAR: TFIBStringField;
    quCheckPSelCOUNTPOS: TFIBSmallIntField;
    quCheckPSelCHDATE: TFIBDateTimeField;
    quCheckPSelPAYMENT: TFIBSmallIntField;
    quCheckPSelOPER: TFIBSmallIntField;
    pBackUpServ: TpFIBBackupService;
    quZ: TpFIBDataSet;
    quZSHOPINDEX: TFIBSmallIntField;
    quZCASHNUMBER: TFIBIntegerField;
    quZZNUMBER: TFIBIntegerField;
    quZMINDATE: TFIBDateTimeField;
    quFindBarAVID: TFIBIntegerField;
    quFindBarALCVOL: TFIBFloatField;
    quFixAVid: TpFIBDataSet;
    quFixAVidIID: TFIBIntegerField;
    quCardsFindAVID: TFIBIntegerField;
    quCardsFindALCVOL: TFIBFloatField;
    taCheckAMARK: TFIBStringField;
    quSelAMarhInCh: TpFIBDataSet;
    quSelAMarhInChNUMPOS: TFIBIntegerField;
    taCheckTrAMARK: TFIBStringField;
    quSelAlcoInCh: TpFIBDataSet;
    quSelAlcoInChCASHNUM: TFIBIntegerField;
    quSelAlcoInChNUMPOS: TFIBIntegerField;
    quSelAlcoInChARTICUL: TFIBStringField;
    quSelAlcoInChRBAR: TFIBStringField;
    quSelAlcoInChNAME: TFIBStringField;
    quSelAlcoInChQUANT: TFIBFloatField;
    quSelAlcoInChPRICE: TFIBFloatField;
    quSelAlcoInChDPROC: TFIBFloatField;
    quSelAlcoInChDSUM: TFIBFloatField;
    quSelAlcoInChRSUM: TFIBFloatField;
    quSelAlcoInChDEPART: TFIBSmallIntField;
    quSelAlcoInChEDIZM: TFIBStringField;
    quSelAlcoInChTIMEPOS: TFIBDateTimeField;
    quSelAlcoInChAMARK: TFIBStringField;
    prAddCardA: TpFIBStoredProc;
    quCassirZList: TpFIBDataSet;
    quCassirZListCASHER: TFIBIntegerField;
    quCassirZListNAME: TFIBStringField;
    quCassirZSum: TpFIBDataSet;
    quCassirZSumCASHER: TFIBIntegerField;
    quCassirZSumOPERATION: TFIBSmallIntField;
    quCassirZSumDSUM: TFIBFloatField;
    quCassirZSumRSUM: TFIBFloatField;
    quFindCheckAMARK: TFIBStringField;
    prCalcDiscItog: TpFIBStoredProc;
    quCheckSelAMARK: TFIBStringField;
    quFindBarINDS: TFIBSmallIntField;
    quCardsFindINDS: TFIBSmallIntField;
    quHotKeySel: TpFIBDataSet;
    quHotKeySelCASHNUM: TFIBIntegerField;
    quHotKeySelIROW: TFIBSmallIntField;
    quHotKeySelICOL: TFIBSmallIntField;
    quHotKeySelSIFR: TFIBStringField;
    quHotKeySelNAME: TFIBStringField;
    quHotKeySelPRICE_RUB: TFIBFloatField;
    cxStyle30: TcxStyle;
    cxStyle31: TcxStyle;
    FisPrint: TComPort;
    quDSumCh: TpFIBDataSet;
    quDSumChOPERATION: TFIBSmallIntField;
    quDSumChDSUM: TFIBFloatField;
    quSelDateZ: TpFIBDataSet;
    quSelDateZCHDATE: TFIBDateTimeField;
    ClientSock: TClientSocket;
    quSelIdLogXML: TpFIBDataSet;
    quSelIdLogXMLMAX: TFIBIntegerField;
    quInsLogXML: TpFIBQuery;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure quFuncListBeforePost(DataSet: TDataSet);
    procedure devCasRxBuf(Sender: TObject; Data: PVaData; Count: Integer);
    procedure FisPrint_1RxChar(Sender: TObject; Count: Integer);
    procedure FisPrintRxChar(Sender: TObject; Count: Integer);
    procedure ClientSockRead(Sender: TObject; Socket: TCustomWinSocket);
  private
    { Private declarations }
  public
    function TypeEdIzm(sArt:String):Integer;
    { Public declarations }
    Procedure prDevOpen(StrP:String;iLog:ShortInt);
    Procedure prDevClose(StrP:String;iLog:ShortInt);
    Procedure prCutDoc(StrP:String;iLog:ShortInt); //�������
    Procedure prRing(StrP:String); //��������
    Procedure prSetFont(StrP:String;iFont:INteger;iLog:ShortInt); //��������� �����
    Procedure prPrintStr(StrP,S:String); //
    Procedure PrintStrDev(StrP,S:String);
  end;

Function GetId(ta:String):Integer;
Function CanDo(Name_F:String):Boolean;
Function FindPers(sP:String):Boolean;
Function CheckOpen:Boolean;
Procedure CalcDiscont(sArt:String;PosNum:Integer;Price:Currency;Quantity:Real;Summa:Currency;DiscProc:Real;Var DiscountProc:Real;Var DiscountSum:Real);
Function FindDiscount(sBar:String):Boolean;
Procedure FormLog(NAMEOP,CONTENT:String);

Function prOpenPrinter(PName:String):Boolean;
Procedure prWritePrinter(S:String);
Procedure prClosePrinter(PName:String);

procedure prWriteLogPS(sStr:String);

Function prGetNFZ:Integer;
Function prSetNFZ:Integer;
Function prGetNFCheckNum:Integer;
Procedure OpenMoneyBox;

Procedure PrintStr(StrP:String);
Function FindClName(iArt:Integer):String;
Procedure prUpd1;
procedure prRecalcDisc(rSumCh:Real);
procedure prRecalcDiscItog;
function prAddByCode(iCode:INteger;sBar:String;rQ:Real;Var sMess:String):Boolean;
function prFindBarCode(iCode:INteger):String;
procedure prSaveCheckA(iPay:Smallint);
function prGetFixAVid(iAVid:Integer):Boolean;
function prFindAMInCh(sAMark:String):Integer;
function fTestAlcoInCh:Boolean;
function fSendAlco:Boolean;
function fTestAlcoConn:Boolean;
procedure writefislog(StrSend,StrRet:String);

var
  dmC: TdmC;

  bPrintOpen: Boolean = False; //��� LPT
  PrintHandle:THandle; //��� LPT

  iReadCount:Integer = 0;
  sScanEv:String;
  bScale:Boolean = False;

implementation

uses MainAdm, Un1, Passw, UnCash, u2fdk, MessAlco;

{$R *.dfm}

function fTestAlcoConn:Boolean;
Const
  CR = #$0d;
  LF = #$0a;
  CRLF = CR + LF;
  Boundary = 'END_OF_PART';
var  httpsend: THTTPSend;
begin
  Result:=False;
  //�������� ����� � ���
  try
    httpsend:=THTTPSend.Create;

    if httpsend.HTTPMethod('get','http://'+AlcoSet.UTMIP+':8080/opt/in') then
    begin
      if httpsend.ResultCode=200 then Result:=True; //��� ������
    end;
  finally
    httpsend.Free;
  end;
end;

function fSendAlco:Boolean;
Const
  CR = #$0d;
  LF = #$0a;
  CRLF = CR + LF;
  Boundary = 'END_OF_PART';

var  IdF:Integer;
     SendXml,RetXml: TNativeXml;

     httpsend: THTTPSend;
     s: AnsiString;
     FS: TMemoryStream;
     RetVal:String;
     rKOp:Integer;

     nodePos: TXmlNode;
begin
  Result:=False;
  with dmC do
  begin
    //��������� XML

    try
      SendXml := TNativeXml.Create(nil);
      RetXml  := TNativeXml.Create(nil);
      httpsend := THTTPSend.Create;
      FS := TMemoryStream.Create;

      IdF:=Nums.ZNum*1000+Nums.iCheckNum;

      if Check.Operation=1 then  rKOp:=1 else rKOp:=-1;  //��������� ����������� ��� �������� - ������������� ����

      SendXml.XmlFormat := xfReadable;
      SendXml.CreateName('Cheque');
      SendXml.Root.WriteAttributeString('inn',Trim(AlcoSet.INN));
      SendXml.Root.WriteAttributeString('datetime',FormatDateTime('ddmmyyhhnn',now));
      SendXml.Root.WriteAttributeString('kpp',Trim(AlcoSet.KPP));
      SendXml.Root.WriteAttributeString('address',UTF8Encode(Trim(AlcoSet.AdrShop)));
      SendXml.Root.WriteAttributeString('name',UTF8Encode(Trim(AlcoSet.NameShop)));

//      SendXml.Root.WriteAttributeString('kassa','ALK060200439');
//      SendXml.Root.WriteAttributeString('shift','1804');
//      SendXml.Root.WriteAttributeString('number','54');

      SendXml.Root.WriteAttributeString('shift',Its(Nums.ZNum));
      SendXml.Root.WriteAttributeString('number',Its(Nums.iCheckNum));
      SendXml.Root.WriteAttributeString('kassa',Trim(Nums.RegNum));


{
      quSelAlcoInCh.Active:=False;
      quSelAlcoInCh.SelectSQL.Clear;
      quSelAlcoInCh.SelectSQL.Add('');
      quSelAlcoInCh.SelectSQL.Add('select cashnum, numpos, articul, rbar, name, quant, price, dproc, dsum, rsum, depart, edizm, timepos, amark');
      quSelAlcoInCh.SelectSQL.Add('from curcheck');
      quSelAlcoInCh.SelectSQL.Add('where cashnum='+its(CommonSet.CashNum)+' and amark is not null and amark>''0''');
      quSelAlcoInCh.Active:=True;
}
      quSelAlcoInCh.Active:=False;
      quSelAlcoInCh.SelectSQL.Clear;
      quSelAlcoInCh.SelectSQL.Add('');
      quSelAlcoInCh.SelectSQL.Add('select cashnum, depart, edizm, amark, articul, rbar, name, max(numpos) as numpos, SUM(quant) as quant, max(price) as price, max(dproc) as dproc, max(dsum) as dsum, max(rsum) as rsum, max(timepos) as timepos');
      quSelAlcoInCh.SelectSQL.Add('from curcheck');
      quSelAlcoInCh.SelectSQL.Add('where cashnum='+its(CommonSet.CashNum)+' and amark is not null and amark>''0''');
      quSelAlcoInCh.SelectSQL.Add('group by cashnum, articul, rbar, name, depart, edizm, amark');
      quSelAlcoInCh.SelectSQL.Add('having SUM(quant)<>0');
      quSelAlcoInCh.Active:=True;


      quSelAlcoInCh.First;
      while not quSelAlcoInCh.Eof do
      begin
        nodePos:=SendXml.Root.NodeNew('Bottle');
        nodePos.WriteAttributeString('barcode',UTF8Encode(Trim(quSelAlcoInChAMARK.AsString)));
        nodePos.WriteAttributeString('ean',UTF8Encode(Trim(quSelAlcoInChRBAR.AsString)));
        nodePos.WriteAttributeString('price',fts00(rKOp*quSelAlcoInChPRICE.AsFloat));

        quSelAlcoInCh.Next;
      end;
      quSelAlcoInCh.Active:=False;
      try  //��� ���������� ���� �������� �� ������ ������ �������
        SendXml.SaveToFile(AlcoSet.PathAlco+FormatDateTime('yy_mm_dd-',date)+ Its(Nums.ZNum)+'-'+its(Nums.iCheckNum)+'.xml');
      except
      end;
      SendXml.SaveToStream(FS);

      httpsend.MimeType := 'multipart/form-data; boundary='+Boundary;  //���������� Contetn-Type �������
      s:='--'+Boundary+CRLF+'Content-Disposition: form-data; name="xml_file"; filename="'+IdToName(IdF)+'"'+CRLF+'Content-Type: application/xml'+CRLF+CRLF;
      httpsend.Document.Write(PAnsiChar(s)^, Length(s));
      FS.Position := 0;
      httpsend.Document.CopyFrom(FS, FS.Size);  //���������� ����� � ���� ���������
      S:=CRLF+CRLF+'--'+Boundary+'--'+CRLF;  //��������� ���� �������
      httpsend.Document.Write(PAnsiChar(s)^, Length(s)); // ��������� ���� ���������
      httpsend.KeepAlive:=False;
      httpsend.Status100:=True;
      httpsend.Headers.Add('Accept: */*');

      // ���������� ������
      if httpsend.HTTPMethod('POST','http://'+AlcoSet.UTMIP+':8080/xml') then
      begin
        httpsend.Document.Position:=0;
        RetXml.LoadFromStream(httpsend.Document);
        RetXml.XmlFormat := xfReadable;
        RetXml.SaveToFile(AlcoSet.PathAlco+FormatDateTime('yy_mm_dd-',date)+IdToName(IdF)); //��������� ����� ���� ���� �� ������ ������

        RetVal:='';
        AlcoSet.URL:='';
        AlcoSet.CRCODE:='';
        AlcoSet.Ver:='';

        if httpsend.ResultCode=200 then  //��� ������
        begin
          if Assigned(RetXml.Root.NodeByName('url')) then AlcoSet.URL:=UTF8Decode(RetXml.Root.NodeByName('url').Value);
          if Assigned(RetXml.Root.NodeByName('sign')) then AlcoSet.CRCODE:=UTF8Decode(RetXml.Root.NodeByName('sign').Value);
          if Assigned(RetXml.Root.NodeByName('ver')) then AlcoSet.Ver:=UTF8Decode(RetXml.Root.NodeByName('ver').Value);

          RetVal:=AlcoSet.URL;
          prWriteLog('~~UTM �k;'+IdToName(IdF)+';'+its(httpsend.ResultCode)+' '+httpsend.ResultString+' "'+RetVal+'"');

          Result:=True;
        end else
        begin
          if Assigned(RetXml.Root.NodeByName('error')) then RetVal:=UTF8Decode(RetXml.Root.NodeByName('error').Value);
          prWriteLog('~~UTM ERR;'+IdToName(IdF)+';'+its(httpsend.ResultCode)+' '+httpsend.ResultString+' "'+RetVal+'"');
          AlcoSet.URL:=RetVal;
        end;
      end;

    finally
      SendXml.Free;
      httpsend.Free;
      FS.Free;
      RetXml.Free;
    end;

  end;
end;


function fTestAlcoInCh:Boolean;
begin
  Result:=False;
  with dmC do
  begin
    quSelAlcoInCh.Active:=False;
    quSelAlcoInCh.SelectSQL.Clear;
    quSelAlcoInCh.SelectSQL.Add('');
    quSelAlcoInCh.SelectSQL.Add('select cashnum, depart, edizm, amark, articul, rbar, name, max(numpos) as numpos, SUM(quant) as quant, max(price) as price, max(dproc) as dproc, max(dsum) as dsum, max(rsum) as rsum, max(timepos) as timepos');
    quSelAlcoInCh.SelectSQL.Add('from curcheck');
    quSelAlcoInCh.SelectSQL.Add('where cashnum='+its(CommonSet.CashNum)+' and amark is not null and amark>''''');
    quSelAlcoInCh.SelectSQL.Add('group by cashnum, articul, rbar, name, depart, edizm, amark');
    quSelAlcoInCh.SelectSQL.Add('having SUM(quant)<>0');
    quSelAlcoInCh.Active:=True;
    if quSelAlcoInCh.RecordCount>0 then Result:=True;
    quSelAlcoInCh.Active:=False;
  end;
end;

function prFindAMInCh(sAMark:String):INteger;
begin
  Result:=-1;
  with dmC do
  begin
    try
      if CurPos.AMark=sAMark then Result:=0
      else
      begin
        quSelAMarhInCh.Active:=False;
        quSelAMarhInCh.SelectSQL.Clear;
        quSelAMarhInCh.SelectSQL.Add('select numpos from curcheck where cashnum='+its(CommonSet.CashNum)+' and amark='''+sAMark+'''');
        quSelAMarhInCh.Active:=True;
        if quSelAMarhInCh.RecordCount>0 then Result:=quSelAMarhInChNUMPOS.AsInteger;
        quSelAMarhInCh.Active:=False;
      end;
    except
    end;
  end;
end;

function prGetFixAVid(iAVid:Integer):Boolean;
begin
  Result:=False;
  with dmC do
  begin
    try
      quFixAVid.Active:=False;
      quFixAVid.SelectSQL.Clear;
      quFixAVid.SelectSQL.Add('select iid from fixavid where iid='+its(iAVid));
      quFixAVid.Active:=True;
      if quFixAVid.RecordCount>0 then Result:=True;
      quFixAVid.Active:=False;
    except
    end;
  end;
end;


procedure prSaveCheckA(iPay:Smallint);
begin
  with dmC do
  begin
    try
      prSAVECHECKANN.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
      prSAVECHECKANN.ParamByName('ZNUMBER').AsInteger:=Nums.ZNum;
      prSAVECHECKANN.ParamByName('NUMCHECK').AsInteger:=Nums.iCheckNum;
      prSAVECHECKANN.ParamByName('CASHER').AsInteger:=Person.Id;
      prSAVECHECKANN.ParamByName('DBAR').AsString:=Check.Discount;
      prSAVECHECKANN.ParamByName('PAYMENT').AsInteger:=iPay;
      prSAVECHECKANN.ParamByName('OPER').AsInteger:=Check.Operation;
      prSAVECHECKANN.ExecProc;
    except
    end;
  end;
end;

function prFindBarCode(iCode:INteger):String;
begin
  Result:='';
  with dmC do
  begin
    if iCode>0 then
    begin
      quFindBar1.Active:=False;
      quFindBar1.ParamByName('ICODE').AsString:=its(iCode);
      quFindBar1.Active:=True;

      quFindBar1.First;
      if quFindBar1.RecordCount>0 then Result:=quFindBar1BARCODE.AsString
      else Result:=itS(iCode);

      quFindBar1.Active:=True;
    end;
  end;
end;

function prAddByCode(iCode:INteger;sBar:String;rQ:Real;Var sMess:String):Boolean;
Var CurrTime:TDateTime;
    StrN:String;
    bRasch:Boolean;
    rKrep:Real;
    sGr:String;
begin
  Result:=True;
  sMess:='';
  with dmC do
  begin
    quCardsFind.Active:=False;
    quCardsFind.ParamByName('ARTICUL').AsString:=its(iCode);
    quCardsFind.Active:=True;
    if quCardsFind.RecordCount>0 then
    begin
      quCardsFind.First;

      SelPos.Articul:=quCardsFindARTICUL.AsString;
      if CommonSet.DepartId>0 then SelPos.Depart:=CommonSet.DepartId
      else SelPos.Depart:=quCardsFindDEPART.AsInteger;
      SelPos.Name:=quCardsFindNAME.AsString;

      if rQ=0 then SelPos.Quant:=1 else SelPos.Quant:=rQ;

      if Length(sBar)<3 then
      begin
        sBar:=prFindBarCode(iCode);
      end;

      SelPos.Price:=rv(quCardsFindPRICE_RUB.AsFloat);
      SelPos.DProc:=0;
      SelPos.DSum:=0;
      SelPos.Bar:=sBar;
      SelPos.EdIzm:=quCardsFindMESURIMENT.AsString;
      SelPos.AVid:=quCardsFindAVID.AsInteger;
      SelPos.Krep:=quCardsFindALCVOL.AsFloat;
      SelPos.iNDS:=quCardsFindINDS.AsInteger;

      if SelPos.Price=0 then
      begin
        result:=False;
        sMess:=ZerroMess;
        {  Memo2.Clear;
          Memo2.Lines.Add(ZerroMess);
          fmAttention.Label1.Caption:=ZerroMess;
          fmAttention.ShowModal;
          TextEdit3.SetFocus;
          TextEdit3.SelectAll;}
      end else
      begin
        bRasch:=True;
        CurrTime:=frac(now);
        if (CommonSet.AlkoKrepMax>0)and((CurrTime<CommonSet.WkTimeBeg)or((CurrTime>CommonSet.WkTimeEnd))) then   //�������� ��������
        begin
          StrN:=SelPos.Name;
            //�������� ��������
          if pos('%',StrN)>0 then
          begin
            StrN:=Copy(StrN,1,pos('%',StrN)-1);
            while pos(' ',StrN)>0 do Delete(StrN,1,pos(' ',StrN));
            while pos('.',StrN)>0 do StrN[pos('.',StrN)]:=',';
            rKrep:=StrToFloatDef(StrN,0);
//              if (rKrep-CommonSet.AlkoKrepMax)>0.001 then bRasch:=False;
            if (rKrep-CommonSet.AlkoKrepMax)>0.001 then
            begin
              sGr:=FindClName(StrToIntDef(SelPos.Articul,0));
              if pos('���',sGr)=1 then  bRasch:=False;
            end;
          end;
        end;
        if not bRasch then
        begin
          result:=False;
          sMess:='��������� ������� �������� ��������� ����� '+IntToStr(CommonSet.AlkoKrepMax)+'%';
        end;
      end;
    end else
    begin
      result:=False;
      sMess:='����� '+its(iCode)+' �� ������.';
    end;
    quCardsFind.Active:=False;
  end;
end;

procedure prRecalcDiscItog;
begin
  with dmC do
  begin
    if Check.Operation=1 then
    begin
      //   CASHNUM, ?ICURDATE
      try
        prCalcDiscItog.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
        prCalcDiscItog.ParamByName('ICURDATE').AsInteger:=Trunc(Date);
        prCalcDiscItog.ExecProc;
      except
      end;
    end;
  end;
end;


procedure prRecalcDisc(rSumCh:Real);
Var iNumPos:INteger;
    rDProc,rDSum:Real;
    sDisc:String;
begin
  with dmC do
  begin
    iNumPos:=0; //��� ����
    //������ ����� ����������� ��� �������

    with dmC do
    begin
      dsCheck.DataSet:=nil;
      if taCheck.Active=False then
      begin
        taCheck.Active:=False;
        taCheck.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
        taCheck.Active:=True;
      end else
      begin
        taCheck.FullRefresh;
      end;
      try
        sDisc:='';
        trUpdCh.StartTransaction;
        taCheck.First;
        while not taCheck.Eof do
        begin
          CalcDiscont(taCheckARTICUL.AsString,(Nums.iCheckNum+1),rv(taCheckPRICE.AsFloat),taCheckQUANT.AsFloat,rSumCh,Check.DiscProc,rDProc,rDSum);
          rDSum:= RoundEx(rDSum*100)/100;

          taCheck.Edit;
          taCheckDPROC.AsFloat:=rDProc;
          taCheckDSUM.AsFloat:=rDSum;
          taCheck.Post;

          sDisc:=sDisc+'~!Disc;'+IntToStr((Nums.iCheckNum))+';'+taCheckARTICUL.AsString+';'+FloatToStr(rDSum)+';'+FloatToStr(rDProc)+';'+IntToStr(taCheckNUMPOS.AsInteger)+';'+#$0D;
          iNumPos:=taCheckNUMPOS.AsInteger;

          taCheck.Next;

//          if rDProc>=10 then Check.DiscName:='����������� ������'; //����
          if rDProc>=90 then Check.DiscName:='����������� ������ (2012)'; //������� �������� �� ������� 20% �������� -> �������� ������� ������ � ��������� calcsum;
        end;
        trUpdCh.Commit;
        prWriteLog(sDisc);

      except
        trUpdCh.Rollback;
      end;
      taCheck.Active:=False;
      taCheck.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
      taCheck.Active:=True;

      dsCheck.DataSet:=taCheck;
    end;

    //������ ������� �������
    if CurPos.Articul>'' then
    begin
      CalcDiscont(CurPos.Articul,(Nums.iCheckNum+1),rv(CurPos.Price),CurPos.Quant,rSumCh,Check.DiscProc,CurPos.DProc,CurPos.DSum);
      CurPos.DSum:= rv(CurPos.DSum);


      inc(iNumPos);
      prWriteLog('~!Disc;'+IntToStr((Nums.iCheckNum))+';'+CurPos.Articul+';'+FloatToStr(CurPos.DSum)+';'+FloatToStr(CurPos.DProc)+';'+IntToStr(iNumPos)+';');
    end;

  end;
end;

Procedure prUpd1; //���������� 1
Var bUpd:Boolean;
begin
  with dmC do
  begin
    try
      bUpd:=True;

      quTabDBList.Active:=False;
      quTabDBList.ParamByName('STABNAME').AsString:='LOGXML';
      quTabDBList.Active:=True;
      if quTabDBList.RecordCount=0 then
      begin //����� ��������� �������� ZLISTDET
        quUpd.SQL.Clear;
        quUpd.SQL.Add('');
        quUpd.SQL.Add('CREATE TABLE LOGXML (');
        quUpd.SQL.Add('    IDATE  INTEGER NOT NULL,');
        quUpd.SQL.Add('    ID     INTEGER NOT NULL,');
        quUpd.SQL.Add('    SSEND  BLOB SUB_TYPE 1 SEGMENT SIZE 80,');
        quUpd.SQL.Add('    SRET   BLOB SUB_TYPE 1 SEGMENT SIZE 80');
        quUpd.SQL.Add(');');

        quUpd.ExecQuery;
        delay(100);

        quUpd.SQL.Clear;
        quUpd.SQL.Add('ALTER TABLE LOGXML ADD CONSTRAINT PK_LOGXML PRIMARY KEY (IDATE, ID);');
        quUpd.ExecQuery;

        delay(100);
      end;

      quTabDBList.Active:=False;
      quTabDBList.ParamByName('STABNAME').AsString:='CLASSIF';
      quTabDBList.Active:=True;
      if quTabDBList.RecordCount>0 then
      begin //�������� ����
        quTabDBList.First;
        while not quTabDBList.Eof do
        begin
          if quTabDBListRDBFIELD_NAME.AsString='DISCSTOP' then begin bUpd:=False; break; end;
          quTabDBList.Next;
        end;

        if bUpd then //������� ���� ��� - ��������
        begin
          quUpd.SQL.Clear;
          quUpd.SQL.Add('ALTER TABLE CLASSIF');
          quUpd.SQL.Add('ADD DISCSTOP INTEGER');
          quUpd.SQL.Add('DEFAULT 0');
          quUpd.ExecQuery;
          delay(100);
        end;
      end;

      quTabDBList.Active:=False;
      quTabDBList.ParamByName('STABNAME').AsString:='CASHSAIL';
      quTabDBList.Active:=True;
      if quTabDBList.RecordCount>0 then
      begin //�������� ����
        bUpd:=True;

        quTabDBList.First;
        while not quTabDBList.Eof do
        begin
          if quTabDBListRDBFIELD_NAME.AsString='SECPOS' then begin bUpd:=False; break; end;
          quTabDBList.Next;
        end;

        if bUpd then //������� ���� ��� - ��������
        begin
          quUpd.SQL.Clear;
          quUpd.SQL.Add('ALTER TABLE CASHSAIL');
          quUpd.SQL.Add('ADD SECPOS SMALLINT');
          quUpd.SQL.Add('DEFAULT 0');
          quUpd.ExecQuery;
          delay(100);
        end;

        bUpd:=True;

        quTabDBList.First;
        while not quTabDBList.Eof do
        begin
          if quTabDBListRDBFIELD_NAME.AsString='ECHECK' then begin bUpd:=False; break; end;
          quTabDBList.Next;
        end;

        if bUpd then //������� ���� ��� - ��������
        begin
          quUpd.SQL.Clear;
          quUpd.SQL.Add('ALTER TABLE CASHSAIL');
          quUpd.SQL.Add('ADD ECHECK SMALLINT');
          quUpd.SQL.Add('DEFAULT 0');
          quUpd.ExecQuery;
          delay(100);
        end;
      end;

      bUpd:=True;

      quTabDBList.Active:=False;
      quTabDBList.ParamByName('STABNAME').AsString:='CURCHECK';
      quTabDBList.Active:=True;
      if quTabDBList.RecordCount>0 then
      begin //�������� ����
        quTabDBList.First;
        while not quTabDBList.Eof do
        begin
          if quTabDBListRDBFIELD_NAME.AsString='TIMEPOS' then begin bUpd:=False; break; end;
          quTabDBList.Next;
        end;

        if bUpd then //������� ���� ��� - ��������
        begin
          quUpd.SQL.Clear;
          quUpd.SQL.Add('ALTER TABLE CURCHECK');
          quUpd.SQL.Add('ADD TIMEPOS TIMESTAMP');
          quUpd.ExecQuery;
          delay(100);
        end;
      end;


      bUpd:=True;

      quTabDBList.Active:=False;
      quTabDBList.ParamByName('STABNAME').AsString:='TRCHECK';
      quTabDBList.Active:=True;
      if quTabDBList.RecordCount>0 then
      begin //�������� ����
        quTabDBList.First;
        while not quTabDBList.Eof do
        begin
          if quTabDBListRDBFIELD_NAME.AsString='TIMEPOS' then begin bUpd:=False; break; end;
          quTabDBList.Next;
        end;

        if bUpd then //������� ���� ��� - ��������
        begin
          quUpd.SQL.Clear;
          quUpd.SQL.Add('ALTER TABLE TRCHECK');
          quUpd.SQL.Add('ADD TIMEPOS TIMESTAMP');
          quUpd.ExecQuery;
          delay(100);
        end;
      end;


      bUpd:=True;   //�������� ������� �������

      quTabDBListI.Active:=False;
      quTabDBListI.ParamByName('STABNAME').AsString:='BAR';
      quTabDBListI.Active:=True;
      if quTabDBListI.RecordCount>0 then
      begin //�������� ����
        quTabDBListI.First;
        while not quTabDBListI.Eof do
        begin
          if quTabDBListIRDBINDEX_NAME.AsString='IDX_BAR' then begin bUpd:=False; break; end;
          quTabDBListI.Next;
        end;

        if bUpd then //������� ���� ��� - ��������
        begin
          quUpd.SQL.Clear;
          quUpd.SQL.Add('create index IDX_BAR');
          quUpd.SQL.Add('on BAR (CARDARTICUL)');
          quUpd.ExecQuery;
          delay(100);
        end;
      end;


      quTabDBList.Active:=False;
      quTabDBList.ParamByName('STABNAME').AsString:='ZLISTDET';
      quTabDBList.Active:=True;
      if quTabDBList.RecordCount=0 then
      begin //����� ��������� �������� ZLISTDET
        quUpd.SQL.Clear;
        quUpd.SQL.Add('CREATE TABLE ZLISTDET (');
        quUpd.SQL.Add('CASHNUM SMALLINT NOT NULL,');
        quUpd.SQL.Add('ZNUM INTEGER NOT NULL,');
        quUpd.SQL.Add('INOUT SMALLINT NOT NULL,');
        quUpd.SQL.Add('ITYPE SMALLINT NOT NULL,');
        quUpd.SQL.Add('RSUM DOUBLE PRECISION,');
        quUpd.SQL.Add('IDATE INTEGER,');
        quUpd.SQL.Add('DDATE TIMESTAMP);');
        quUpd.ExecQuery;
        delay(100);

        quUpd.SQL.Clear;
        quUpd.SQL.Add('ALTER TABLE ZLISTDET ADD CONSTRAINT PK_ZLISTDET PRIMARY KEY (CASHNUM, ZNUM, INOUT, ITYPE);');
        quUpd.ExecQuery;

        delay(100);

      end;


      quTabDBList.Active:=False;
      quTabDBList.ParamByName('STABNAME').AsString:='REZERVCHECK';
      quTabDBList.Active:=True;
      if quTabDBList.RecordCount=0 then
      begin //����� ��������� �������� REZERVCHECK
        quUpd.SQL.Clear;
        quUpd.SQL.Add('CREATE TABLE REZERVCHECK (');
        quUpd.SQL.Add('CASHNUM INTEGER DEFAULT 0 NOT NULL,');
        quUpd.SQL.Add('ZNUM INTEGER NOT NULL,');
        quUpd.SQL.Add('CHECKNUM INTEGER NOT NULL,');
        quUpd.SQL.Add('NUMPOS INTEGER NOT NULL,');
        quUpd.SQL.Add('ARTICUL VARCHAR (30) character set WIN1251 collate WIN1251,');
        quUpd.SQL.Add('RBAR VARCHAR (14) character set WIN1251 collate WIN1251,');
        quUpd.SQL.Add('NAME VARCHAR (80) character set WIN1251 collate WIN1251,');
        quUpd.SQL.Add('QUANT DOUBLE PRECISION,');
        quUpd.SQL.Add('PRICE DOUBLE PRECISION,');
        quUpd.SQL.Add('DPROC FLOAT,');
        quUpd.SQL.Add('DSUM DOUBLE PRECISION,');
        quUpd.SQL.Add('RSUM DOUBLE PRECISION,');
        quUpd.SQL.Add('DEPART SMALLINT,');
        quUpd.SQL.Add('EDIZM CHAR (5) character set WIN1251 collate WIN1251);');
        quUpd.ExecQuery;
        delay(100);

        quUpd.SQL.Clear;
        quUpd.SQL.Add('ALTER TABLE REZERVCHECK ADD CONSTRAINT PK_REZERVCHECK PRIMARY KEY (CASHNUM, ZNUM, CHECKNUM, NUMPOS);');
        quUpd.ExecQuery;
        delay(100);

      end;
      quTabDBList.Active:=False;


      quTabDBList.Active:=False;
      quTabDBList.ParamByName('STABNAME').AsString:='CURCHECK';
      quTabDBList.Active:=True;
      if quTabDBList.RecordCount>0 then
      begin //�������� ����
        bUpd:=True;

        quTabDBList.First;
        while not quTabDBList.Eof do
        begin
          if quTabDBListRDBFIELD_NAME.AsString='AMARK' then begin bUpd:=False; break; end;
          quTabDBList.Next;
        end;

        if bUpd then //������� ���� ��� - ��������
        begin
          quUpd.SQL.Clear;
          quUpd.SQL.Add('ALTER TABLE CURCHECK  ADD AMARK VARCHAR(150) DEFAULT '''' ');
          quUpd.ExecQuery;
          delay(100);
        end;
      end;

      quTabDBList.Active:=False;
      quTabDBList.ParamByName('STABNAME').AsString:='TRCHECK';
      quTabDBList.Active:=True;
      if quTabDBList.RecordCount>0 then
      begin //�������� ����
        bUpd:=True;

        quTabDBList.First;
        while not quTabDBList.Eof do
        begin
          if quTabDBListRDBFIELD_NAME.AsString='AMARK' then begin bUpd:=False; break; end;
          quTabDBList.Next;
        end;

        if bUpd then //������� ���� ��� - ��������
        begin
          quUpd.SQL.Clear;
          quUpd.SQL.Add('ALTER TABLE TRCHECK  ADD AMARK VARCHAR(150) DEFAULT '''' ');
          quUpd.ExecQuery;
          delay(100);
        end;
      end;

      quTabDBList.Active:=False;
      quTabDBList.ParamByName('STABNAME').AsString:='CASHSAIL';
      quTabDBList.Active:=True;
      if quTabDBList.RecordCount>0 then
      begin //�������� ����
        bUpd:=True;

        quTabDBList.First;
        while not quTabDBList.Eof do
        begin
          if quTabDBListRDBFIELD_NAME.AsString='AMARK' then begin bUpd:=False; break; end;
          quTabDBList.Next;
        end;

        if bUpd then //������� ���� ��� - ��������
        begin
          quUpd.SQL.Clear;
          quUpd.SQL.Add('ALTER TABLE CASHSAIL ADD AMARK VARCHAR(150)');
          quUpd.ExecQuery;
          delay(100);
        end;
      end;

      quTabDBList.Active:=False;
      quTabDBList.ParamByName('STABNAME').AsString:='ACHECK_SP';
      quTabDBList.Active:=True;
      if quTabDBList.RecordCount>0 then
      begin //�������� ����
        bUpd:=True;

        quTabDBList.First;
        while not quTabDBList.Eof do
        begin
          if quTabDBListRDBFIELD_NAME.AsString='AMARK' then begin bUpd:=False; break; end;
          quTabDBList.Next;
        end;

        if bUpd then //������� ���� ��� - ��������
        begin
          quUpd.SQL.Clear;
          quUpd.SQL.Add('ALTER TABLE ACHECK_SP  ADD AMARK VARCHAR(150) DEFAULT '''' ');
          quUpd.ExecQuery;
          delay(100);
        end;
      end;

      quTabDBList.Active:=False;
      quTabDBList.ParamByName('STABNAME').AsString:='AKCIYA';
      quTabDBList.Active:=True;
      if quTabDBList.RecordCount=0 then
      begin //����� ��������� �������� AKCIYA
        quUpd.SQL.Clear;

        quUpd.SQL.Add('CREATE TABLE AKCIYA ( ');
        quUpd.SQL.Add('    IDA      INTEGER NOT NULL,');
        quUpd.SQL.Add('    ID       BIGINT NOT NULL,');
        quUpd.SQL.Add('    IDATEB   INTEGER,');
        quUpd.SQL.Add('    IDATEE   INTEGER,');
        quUpd.SQL.Add('    IACTIVE  SMALLINT,');
        quUpd.SQL.Add('    ARTICUL  INTEGER,');
        quUpd.SQL.Add('    QUANT    INTEGER,');
        quUpd.SQL.Add('    DPROC    DOUBLE PRECISION');
        quUpd.SQL.Add(');');

        quUpd.ExecQuery;
        delay(100);

        quUpd.SQL.Clear;
        quUpd.SQL.Add('ALTER TABLE AKCIYA ADD CONSTRAINT PK_AKCIYA PRIMARY KEY (IDA, ID);');
        quUpd.ExecQuery;
        delay(100);

      end;
      quTabDBList.Active:=False;

      quTabDBList.Active:=False;
      quTabDBList.ParamByName('STABNAME').AsString:='VCURCHECKQ';
      quTabDBList.Active:=True;
      if quTabDBList.RecordCount=0 then
      begin //����� ��������� view VCURCHECKQ
        quUpd.SQL.Clear;

        quUpd.SQL.Add('CREATE VIEW VCURCHECKQ(');
        quUpd.SQL.Add('    CASHNUM,');
        quUpd.SQL.Add('    ARTICUL,');
        quUpd.SQL.Add('    QUANT)');
        quUpd.SQL.Add('AS');
        quUpd.SQL.Add('Select CASHNUM,ARTICUL,SUM(QUANT) as QUANT from CURCHECK group by CASHNUM,ARTICUL;');

        quUpd.ExecQuery;
        delay(100);
      end;
      quTabDBList.Active:=False;







      quUpd.SQL.Clear;

      quUpd.SQL.Add('ALTER PROCEDURE PR_CALCDISCOUNT (');
      quUpd.SQL.Add('    SART VARCHAR (30),');
      quUpd.SQL.Add('    POSNUM INTEGER,');
      quUpd.SQL.Add('    PRICE DOUBLE PRECISION,');
      quUpd.SQL.Add('    QUANTITY DOUBLE PRECISION,');
      quUpd.SQL.Add('    SUMMA DOUBLE PRECISION,');
      quUpd.SQL.Add('    DISCPROC DOUBLE PRECISION,');
      quUpd.SQL.Add('    CURTIME DOUBLE PRECISION,');
      quUpd.SQL.Add('    CURDATE TIMESTAMP, ');
      quUpd.SQL.Add('    TYPEDISC INTEGER)  ');
      quUpd.SQL.Add('RETURNS ( ');
      quUpd.SQL.Add('    DISCOUNTPROC DOUBLE PRECISION,');
      quUpd.SQL.Add('    DISCOUNTSUM DOUBLE PRECISION)');
      quUpd.SQL.Add('AS  ');
      quUpd.SQL.Add('DECLARE VARIABLE CALCPROC double precision;');
      quUpd.SQL.Add('DECLARE VARIABLE MAXPROC double precision;');
      quUpd.SQL.Add('DECLARE VARIABLE PROC double precision;');
      quUpd.SQL.Add('DECLARE VARIABLE DCURDATE TIMESTAMP;');
      quUpd.SQL.Add('DECLARE VARIABLE SUMCH DOUBLE PRECISION;');
      quUpd.SQL.Add('DECLARE VARIABLE IGR INTEGER;');
      quUpd.SQL.Add('begin');
      quUpd.SQL.Add('  DISCOUNTPROC = 0;');
      quUpd.SQL.Add('  DISCOUNTSUM = 0;');
      quUpd.SQL.Add('  CALCPROC = 0;');
      quUpd.SQL.Add('  SUMCH=:PRICE*:QUANTITY;');
      quUpd.SQL.Add('  CALCPROC = :DISCPROC;');
      quUpd.SQL.Add('  if ((CURTIME > 0.25) and (CURTIME < 0.46)) then  ');
      quUpd.SQL.Add('  begin');
      quUpd.SQL.Add('    if ((DISCPROC>2.9)and((DISCPROC<3.1))) then CALCPROC=7;');
      quUpd.SQL.Add('    if ((DISCPROC>4.9)and((DISCPROC<5.1))) then CALCPROC=7;');
      quUpd.SQL.Add('  end ');
      quUpd.SQL.Add('  if (TYPEDISC=0) then ');
      quUpd.SQL.Add('  begin                ');
      quUpd.SQL.Add('    if ((CURDATE>=''15.12.2014'') and (CURDATE<''05.01.2015'')) then ');
      quUpd.SQL.Add('    begin ');
      quUpd.SQL.Add('      if (SUMMA>=2015) then CALCPROC=7; ');
      quUpd.SQL.Add('    end ');
      quUpd.SQL.Add('  end ');

      quUpd.SQL.Add('  select CLASSIF from CARDSCLA ');
      quUpd.SQL.Add('  where ARTICUL=:SART ');
      quUpd.SQL.Add('  into :IGR; ');

      quUpd.SQL.Add('  if (((CURTIME > 0.833334)or(CURTIME < 0.416667))and(TYPEDISC<>2)) then ');  //�� 10 ���� � � 20:00
      quUpd.SQL.Add('  begin ');

      quUpd.SQL.Add('    if (IGR=20411) then CALCPROC = 20; ');
      quUpd.SQL.Add('    if (IGR=20414) then CALCPROC = 20; ');
      quUpd.SQL.Add('    if (IGR=20582) then CALCPROC = 20; ');
      quUpd.SQL.Add('    if (IGR=20576) then CALCPROC = 20; ');
      quUpd.SQL.Add('    if (IGR=20596) then CALCPROC = 20; ');
      quUpd.SQL.Add('    if (IGR=20769) then CALCPROC = 20; ');
      quUpd.SQL.Add('    if (IGR=20770) then CALCPROC = 20; ');
      quUpd.SQL.Add('    if (IGR=20745) then CALCPROC = 20; ');
      quUpd.SQL.Add('    if (IGR=20798) then CALCPROC = 20; ');
//      quUpd.SQL.Add('    if (IGR=20754) then CALCPROC = 20; ');
      quUpd.SQL.Add('  end ');

      quUpd.SQL.Add('  MAXPROC=100; ');
      quUpd.SQL.Add('  if (exists(select * from fixdisc where SART=:SART)) then ');
      quUpd.SQL.Add('  begin ');
      quUpd.SQL.Add('    select PROC from fixdisc where SART=:SART into :PROC; ');
      quUpd.SQL.Add('    MAXPROC=100-:PROC; ');
      quUpd.SQL.Add('  end ');
      quUpd.SQL.Add('  if (MAXPROC<CALCPROC) then CALCPROC = :MAXPROC; ');

      quUpd.SQL.Add('  if (IGR=20429) then CALCPROC = 0; ');
      quUpd.SQL.Add('  if (IGR=20712) then CALCPROC = 0; ');

      quUpd.SQL.Add('  DISCOUNTPROC = :CALCPROC; ');
      quUpd.SQL.Add('  DISCOUNTSUM = :SUMCH/100*:CALCPROC;');
      quUpd.SQL.Add('  suspend;');
      quUpd.SQL.Add('end');
      quUpd.ExecQuery;
      delay(100);

      //������� ��������� ����� ��������� ��.�. �� ������� ���� ������ ����� 20-00 �� ������   - ����� ������ ����

      quUpd.SQL.Clear;

      quUpd.SQL.Add('ALTER PROCEDURE PR_READCHECK ( ');
      quUpd.SQL.Add('    SHOPINDEX INTEGER, ');
      quUpd.SQL.Add('    CASHNUM INTEGER, ');
      quUpd.SQL.Add('    CASHZ INTEGER,');
      quUpd.SQL.Add('    CHECKNUM INTEGER)');
      quUpd.SQL.Add('RETURNS (');
      quUpd.SQL.Add('    CHECKDATE TIMESTAMP,');
      quUpd.SQL.Add('    CASHER INTEGER, ');
      quUpd.SQL.Add('    CHECKSUM DOUBLE PRECISION,');
      quUpd.SQL.Add('    CHECKPAY DOUBLE PRECISION, ');
      quUpd.SQL.Add('    CHECKDSUM DOUBLE PRECISION,');
      quUpd.SQL.Add('    OPERATION INTEGER)');
      quUpd.SQL.Add('AS ');
      quUpd.SQL.Add('DECLARE VARIABLE NUMPOS INTEGER;');
      quUpd.SQL.Add('DECLARE VARIABLE ARTICUL varchar (30);');
      quUpd.SQL.Add('DECLARE VARIABLE SBAR VARCHAR (15);');
      quUpd.SQL.Add('DECLARE VARIABLE QUANT double precision;');
      quUpd.SQL.Add('DECLARE VARIABLE PRICE double precision; ');
      quUpd.SQL.Add('DECLARE VARIABLE DPROC FLOAT;');
      quUpd.SQL.Add('DECLARE VARIABLE DSUM double precision;');
      quUpd.SQL.Add('DECLARE VARIABLE RSUM double precision;');
      quUpd.SQL.Add('DECLARE VARIABLE DEPART INTEGER;');
      quUpd.SQL.Add('DECLARE VARIABLE NAME VARCHAR (80); ');
      quUpd.SQL.Add('begin ');
      quUpd.SQL.Add('  for select cs.ID,cs.ARTICUL,cs.BAR,cs.QUANTITY,cs.PRICERUB,cs.DPROC,cs.DSUM,cs.TOTALRUB,cs.DEPART,cd.NAME from cashsail cs');
      quUpd.SQL.Add('  left join cardscla cd on cd.ARTICUL=cs.ARTICUL');
      quUpd.SQL.Add('  where SHOPINDEX=:SHOPINDEX');
      quUpd.SQL.Add('  and CASHNUMBER=:CASHNUM ');
      quUpd.SQL.Add('  and ZNUMBER=:CASHZ ');
      quUpd.SQL.Add('  and CHECKNUMBER=:CHECKNUM ');
      quUpd.SQL.Add('  into :NUMPOS,:ARTICUL,:SBAR,:QUANT,:PRICE,:DPROC,:DSUM,:RSUM,:DEPART,:NAME ');
      quUpd.SQL.Add('  do begin ');
      quUpd.SQL.Add('    insert into CURCHECK (CASHNUM,NUMPOS,ARTICUL,RBAR,QUANT,PRICE,DPROC,DSUM,RSUM,DEPART,NAME)');
      quUpd.SQL.Add('    values (:CASHNUM,:NUMPOS,:ARTICUL,:SBAR,:QUANT,:PRICE,:DPROC,:DSUM,(:RSUM+:DSUM),:DEPART,:NAME);');
      quUpd.SQL.Add('  end');
      quUpd.SQL.Add('  select MAX(CASHER),SUM(CHECKSUM),SUM(PAYSUM),SUM(DSUM),MAX(CHDATE) from cashpay ');
      quUpd.SQL.Add(' where SHOPINDEX=:SHOPINDEX ');
      quUpd.SQL.Add(' and CASHNUMBER=:CASHNUM ');
      quUpd.SQL.Add('  and ZNUMBER=:CASHZ ');
      quUpd.SQL.Add('  and CHECKNUMBER=:CHECKNUM ');
      quUpd.SQL.Add('  into :CASHER,:CHECKSUM,:CHECKPAY,:CHECKDSUM,:CHECKDATE; ');
      quUpd.SQL.Add('  select MAX(operation) from cashsail ');
      quUpd.SQL.Add('  where SHOPINDEX=:SHOPINDEX ');
      quUpd.SQL.Add('  and CASHNUMBER=:CASHNUM');
      quUpd.SQL.Add(' and ZNUMBER=:CASHZ');
      quUpd.SQL.Add(' and CHECKNUMBER=:CHECKNUM ');
      quUpd.SQL.Add(' into :OPERATION;');
      quUpd.SQL.Add('  suspend;');
      quUpd.SQL.Add('end');

      quUpd.ExecQuery;
      delay(100);

      quTabDBList.Active:=False;
      quTabDBList.ParamByName('STABNAME').AsString:='KEYS';
      quTabDBList.Active:=True;
      if quTabDBList.RecordCount=0 then
      begin //����� ��������� �������� REZERVCHECK
        quUpd.SQL.Clear;
        quUpd.SQL.Add('CREATE TABLE KEYS (');
        quUpd.SQL.Add('IKT INTEGER NOT NULL,');
        quUpd.SQL.Add('ACTIONNAME VARCHAR (30) character set WIN1251 NOT NULL collate WIN1251,');
        quUpd.SQL.Add('ID INTEGER NOT NULL,');
        quUpd.SQL.Add('HOTKEY VARCHAR (20) character set WIN1251 collate WIN1251);');
        quUpd.ExecQuery;
        delay(100);

        quUpd.SQL.Clear;
        quUpd.SQL.Add('ALTER TABLE KEYS ADD CONSTRAINT PK_KEYS PRIMARY KEY (IKT, ACTIONNAME, ID);');
        quUpd.ExecQuery;
        delay(100);
      end;
      quTabDBList.Active:=False;


      quTabDBList.Active:=False;
      quTabDBList.ParamByName('STABNAME').AsString:='HOTKEY';
      quTabDBList.Active:=True;
      if quTabDBList.RecordCount=0 then
      begin //����� ��������� �������� REZERVCHECK
        quUpd.SQL.Clear;
        quUpd.SQL.Add('CREATE TABLE HOTKEY (');
        quUpd.SQL.Add('CASHNUM INTEGER NOT NULL,');
        quUpd.SQL.Add('IROW SMALLINT NOT NULL,');
        quUpd.SQL.Add('ICOL SMALLINT NOT NULL,');
        quUpd.SQL.Add('SIFR VARCHAR (30) character set WIN1251 NOT NULL collate WIN1251);');
        quUpd.ExecQuery;
        delay(100);

        quUpd.SQL.Clear;
        quUpd.SQL.Add('ALTER TABLE HOTKEY ADD CONSTRAINT PK_HOTKEY PRIMARY KEY (CASHNUM, IROW, ICOL);');
        quUpd.ExecQuery;
        delay(100);
      end;
      quTabDBList.Active:=False;

      quUpd.SQL.Clear;

      quUpd.SQL.Add('ALTER PROCEDURE PR_SAVECHECKTR (');
      quUpd.SQL.Add('    CASHNUM INTEGER,');
      quUpd.SQL.Add('    SHOPINDEX INTEGER,');
      quUpd.SQL.Add('    ZNUMBER INTEGER,');
      quUpd.SQL.Add('    NUMCHECK INTEGER,');
      quUpd.SQL.Add('    CASHER INTEGER,');
      quUpd.SQL.Add('    PAYSUM DOUBLE PRECISION,');
      quUpd.SQL.Add('    DBAR VARCHAR (22),');
      quUpd.SQL.Add('    ECHECK INTEGER)');
      quUpd.SQL.Add('AS ');
      quUpd.SQL.Add('DECLARE VARIABLE CURDATE timestamp;');
      quUpd.SQL.Add('   ');
      quUpd.SQL.Add('DECLARE VARIABLE TIMEPOS timestamp;');
      quUpd.SQL.Add('   ');
      quUpd.SQL.Add('DECLARE VARIABLE NUMPOS INTEGER;');
      quUpd.SQL.Add('DECLARE VARIABLE ARTICUL varchar (30);');
      quUpd.SQL.Add('DECLARE VARIABLE SBAR VARCHAR (15);');
      quUpd.SQL.Add('DECLARE VARIABLE QUANT double precision;');
      quUpd.SQL.Add('DECLARE VARIABLE PRICE double precision;');
      quUpd.SQL.Add('DECLARE VARIABLE DPROC FLOAT;');
      quUpd.SQL.Add('DECLARE VARIABLE DSUM double precision;');
      quUpd.SQL.Add('DECLARE VARIABLE RSUM double precision;');
      quUpd.SQL.Add('DECLARE VARIABLE DEPART INTEGER;');
      quUpd.SQL.Add('DECLARE VARIABLE OPERATION INTEGER;');
      quUpd.SQL.Add('DECLARE VARIABLE VIDPL INTEGER;');
      quUpd.SQL.Add('    ');
      quUpd.SQL.Add('DECLARE VARIABLE CHSUMD double precision;');
      quUpd.SQL.Add('DECLARE VARIABLE CHSUMR double precision; ');
      quUpd.SQL.Add('DECLARE VARIABLE CARDSIZE VARCHAR (10);');
      quUpd.SQL.Add('DECLARE VARIABLE COUNTPOS INTEGER;');
      quUpd.SQL.Add('DECLARE VARIABLE AMARK VARCHAR (150);');
      quUpd.SQL.Add('    ');
      quUpd.SQL.Add('begin  ');
      quUpd.SQL.Add('  CURDATE = cast(''now'' as timestamp);');
      quUpd.SQL.Add('  CHSUMD = 0;');
      quUpd.SQL.Add('  CHSUMR = 0;');
      quUpd.SQL.Add('  CARDSIZE = ''NOSIZE'';');
      quUpd.SQL.Add('  COUNTPOS = 0;');
      quUpd.SQL.Add('     ');
      quUpd.SQL.Add('  if ((exists(select * from cashsail where SHOPINDEX=:SHOPINDEX and CASHNUMBER=:CASHNUM and ZNUMBER=:ZNUMBER and CHECKNUMBER=:NUMCHECK)) or  ');
      quUpd.SQL.Add('    (exists(select * from cashpay where SHOPINDEX=:SHOPINDEX and CASHNUMBER=:CASHNUM and ZNUMBER=:ZNUMBER and CHECKNUMBER=:NUMCHECK))) then  ');
      quUpd.SQL.Add('    NUMCHECK = GEN_ID(gen_checknum,1);  ');
      quUpd.SQL.Add('      ');
      quUpd.SQL.Add('  for select ARTICUL,RBAR,PRICE,DPROC,DEPART,OPERATION,VIDP,QUANT,DSUM,RSUM,NUMPOS,TIMEPOS,AMARK ');
      quUpd.SQL.Add('  from TRCHECK CH ');
      quUpd.SQL.Add('  where CASHNUM=:CASHNUM and ARTICUL is not null ');
      quUpd.SQL.Add('  order by NUMPOS ');
      quUpd.SQL.Add('  into :ARTICUL,:SBAR,:PRICE,:DPROC,:DEPART,:OPERATION,:VIDPL,:QUANT,:DSUM,:RSUM,:NUMPOS,:TIMEPOS,:AMARK ');
      quUpd.SQL.Add('  do begin ');
      quUpd.SQL.Add('    COUNTPOS = :COUNTPOS+1; ');
      quUpd.SQL.Add('    insert into cashsail (SHOPINDEX,CASHNUMBER,ZNUMBER,CHECKNUMBER,ID,CHDATE,ARTICUL,BAR,CARDSIZE,QUANTITY,PRICERUB,DPROC,DSUM,TOTALRUB,CASHER,DEPART,OPERATION,ECHECK,AMARK) ');
      quUpd.SQL.Add('    values (:SHOPINDEX,:CASHNUM,:ZNUMBER,:NUMCHECK,:COUNTPOS,:TIMEPOS,:ARTICUL,:SBAR,:CARDSIZE,:QUANT,:PRICE,:DPROC,:DSUM,:RSUM,:CASHER,:DEPART,:OPERATION,:ECHECK,:AMARK); ');
      quUpd.SQL.Add('  end  ');
      quUpd.SQL.Add('       ');
      quUpd.SQL.Add('  for Select Sum(RSUM),Sum(DSUM),OPERATION,VIDP from trcheck ');
      quUpd.SQL.Add('  where CASHNUM=:CASHNUM  ');
      quUpd.SQL.Add('  Group by OPERATION,VIDP ');
      quUpd.SQL.Add('  into :CHSUMR,:CHSUMD,:OPERATION,:VIDPL  ');
      quUpd.SQL.Add('  do begin ');
      quUpd.SQL.Add('    insert into cashpay (SHOPINDEX,CASHNUMBER,ZNUMBER,CHECKNUMBER,PAYMENT,CASHER,CHECKSUM,PAYSUM,DSUM,DBAR,COUNTPOS,CHDATE,OPER) ');
      quUpd.SQL.Add('    values (:SHOPINDEX,:CASHNUM,:ZNUMBER,:NUMCHECK,:VIDPL,:CASHER,:CHSUMR,:PAYSUM,:CHSUMD,:DBAR,:COUNTPOS,:CURDATE,:OPERATION);  ');
      quUpd.SQL.Add('    PAYSUM = 0;   ');
      quUpd.SQL.Add('    COUNTPOS = 0; ');
      quUpd.SQL.Add('  end             ');
      quUpd.SQL.Add('                  ');
      quUpd.SQL.Add('  DELETE FROM TRCHECK ');
      quUpd.SQL.Add('  WHERE CASHNUM=:CASHNUM; ');
      quUpd.SQL.Add('                   ');
      quUpd.SQL.Add('  DELETE FROM CURCHECK ');
      quUpd.SQL.Add('  WHERE CASHNUM=:CASHNUM; ');
      quUpd.SQL.Add('      ');
      quUpd.SQL.Add('  suspend; ');
      quUpd.SQL.Add('end ');

      quUpd.ExecQuery;
      delay(100);

      quTabDBList.Active:=False;
      quTabDBList.ParamByName('STABNAME').AsString:='ACHECK_HD';
      quTabDBList.Active:=True;
      if quTabDBList.RecordCount=0 then
      begin //����� ��������� �������� ACHECK_HD
        quUpd.SQL.Clear;
        quUpd.SQL.Add('CREATE TABLE ACHECK_HD (');
        quUpd.SQL.Add('CASHNUMBER INTEGER NOT NULL,');
        quUpd.SQL.Add('ZNUMBER INTEGER NOT NULL,');
        quUpd.SQL.Add('CHECKNUMBER INTEGER NOT NULL,');
        quUpd.SQL.Add('ID INTEGER NOT NULL,');
        quUpd.SQL.Add('CASHER INTEGER,');
        quUpd.SQL.Add('DBAR VARCHAR (22) character set WIN1251 collate WIN1251,');
        quUpd.SQL.Add('CHDATE TIMESTAMP,');
        quUpd.SQL.Add('PAYMENT SMALLINT NOT NULL,');
        quUpd.SQL.Add('OPER SMALLINT DEFAULT 1);');
        quUpd.ExecQuery;
        delay(100);

        quUpd.SQL.Clear;
        quUpd.SQL.Add('ALTER TABLE ACHECK_HD ADD CONSTRAINT PK_ACHECK_HD PRIMARY KEY (CASHNUMBER, ZNUMBER, CHECKNUMBER, ID);');
        quUpd.ExecQuery;
        delay(100);
      end;
      quTabDBList.Active:=False;

      quTabDBList.Active:=False;
      quTabDBList.ParamByName('STABNAME').AsString:='ACHECK_SP';
      quTabDBList.Active:=True;
      if quTabDBList.RecordCount=0 then
      begin //����� ��������� �������� ACHECK_HD
        quUpd.SQL.Clear;
        quUpd.SQL.Add('CREATE TABLE ACHECK_SP (');
        quUpd.SQL.Add('IDH INTEGER NOT NULL,');
        quUpd.SQL.Add('ID INTEGER NOT NULL,');
        quUpd.SQL.Add('ARTICUL VARCHAR (30) character set WIN1251 collate WIN1251,');
        quUpd.SQL.Add('BAR VARCHAR (15) character set WIN1251 collate WIN1251,');
        quUpd.SQL.Add('QUANTITY DOUBLE PRECISION,');
        quUpd.SQL.Add('PRICERUB DOUBLE PRECISION,');
        quUpd.SQL.Add('DPROC FLOAT,');
        quUpd.SQL.Add('DSUM DOUBLE PRECISION,');
        quUpd.SQL.Add('TOTALRUB DOUBLE PRECISION);');
        quUpd.ExecQuery;
        delay(100);

        quUpd.SQL.Clear;
        quUpd.SQL.Add('ALTER TABLE ACHECK_SP ADD CONSTRAINT PK_ACHECK_SP PRIMARY KEY (IDH, ID);');
        quUpd.ExecQuery;
        delay(100);
      end;
      quTabDBList.Active:=False;

      quProcDBList.Active:=False;
      quProcDBList.ParamByName('STABNAME').AsString:='PR_SAVECHECKA';
      quProcDBList.Active:=True;
      if quProcDBList.RecordCount=0 then
      begin //����� ��������� ���������
        quUpd.SQL.Clear;

        quUpd.SQL.Add('CREATE PROCEDURE PR_SAVECHECKA (');
        quUpd.SQL.Add('    CASHNUM INTEGER,');
        quUpd.SQL.Add('    ZNUMBER INTEGER,');
        quUpd.SQL.Add('    NUMCHECK INTEGER,');
        quUpd.SQL.Add('    CASHER INTEGER,');
        quUpd.SQL.Add('    DBAR VARCHAR (22),');
        quUpd.SQL.Add('    PAYMENT SMALLINT,');
        quUpd.SQL.Add('    OPER SMALLINT)');
        quUpd.SQL.Add('AS ');
        quUpd.SQL.Add('DECLARE VARIABLE CURDATE timestamp;');
        quUpd.SQL.Add('DECLARE VARIABLE MAXID INTEGER;');
        quUpd.SQL.Add('DECLARE VARIABLE NUMPOS INTEGER;');
        quUpd.SQL.Add('DECLARE VARIABLE ARTICUL varchar (30);');
        quUpd.SQL.Add('DECLARE VARIABLE SBAR VARCHAR (15);');
        quUpd.SQL.Add('DECLARE VARIABLE QUANT double precision;');
        quUpd.SQL.Add('DECLARE VARIABLE PRICE double precision;');
        quUpd.SQL.Add('DECLARE VARIABLE DPROC FLOAT;');
        quUpd.SQL.Add('DECLARE VARIABLE DSUM double precision;');
        quUpd.SQL.Add('DECLARE VARIABLE RSUM double precision;');
        quUpd.SQL.Add('BEGIN ');
        quUpd.SQL.Add('  CURDATE = cast(''now'' as timestamp); ');
        quUpd.SQL.Add('  Select Max(ID) from acheck_hd into :MAXID;');
        quUpd.SQL.Add('  if (MAXID is null) then MAXID=0; ');
        quUpd.SQL.Add('  MAXID=:MAXID+1;');
        quUpd.SQL.Add('  insert into acheck_hd (CASHNUMBER,ZNUMBER,CHECKNUMBER,ID,CASHER,DBAR,CHDATE,PAYMENT,OPER)');
        quUpd.SQL.Add('  values (:CASHNUM,:ZNUMBER,:NUMCHECK,:MAXID,:CASHER,:DBAR,:CURDATE,:PAYMENT,:OPER);');
        quUpd.SQL.Add('  for select NUMPOS,ARTICUL,RBAR,QUANT,PRICE,DPROC,DSUM,RSUM from CURCHECK CH');
        quUpd.SQL.Add('  where CASHNUM=:CASHNUM');
        quUpd.SQL.Add('  into :NUMPOS,:ARTICUL,:SBAR,:QUANT,:PRICE,:DPROC,:DSUM,:RSUM');
        quUpd.SQL.Add('  do begin');
        quUpd.SQL.Add('     insert into acheck_sp (IDH,ID,ARTICUL,BAR,QUANTITY,PRICERUB,DPROC,DSUM,TOTALRUB)');
        quUpd.SQL.Add('     values (:MAXID,:NUMPOS,:ARTICUL,:SBAR,:QUANT,:PRICE,:DPROC,:DSUM,:RSUM);');
        quUpd.SQL.Add('  end');
        quUpd.SQL.Add('  /* Procedure body */');
        quUpd.SQL.Add('  SUSPEND;');
        quUpd.SQL.Add('END');

        quUpd.ExecQuery;
        delay(100);
      end;
      quProcDBList.Active:=False;


      quUpd.SQL.Clear;

      quUpd.SQL.Add('ALTER PROCEDURE PR_SAVECHECKA ( ');
      quUpd.SQL.Add('    cashnum integer,');
      quUpd.SQL.Add('    znumber integer,');
      quUpd.SQL.Add('    numcheck integer,');
      quUpd.SQL.Add('    casher integer,');
      quUpd.SQL.Add('    dbar varchar(22),');
      quUpd.SQL.Add('    payment smallint,');
      quUpd.SQL.Add('    oper smallint)');
      quUpd.SQL.Add('as');
      quUpd.SQL.Add('declare variable curdate timestamp;');
      quUpd.SQL.Add('declare variable maxid integer;');
      quUpd.SQL.Add('declare variable numpos integer;');
      quUpd.SQL.Add('declare variable articul varchar(30);');
      quUpd.SQL.Add('declare variable sbar varchar(15);');
      quUpd.SQL.Add('declare variable quant double precision;');
      quUpd.SQL.Add('declare variable price double precision;');
      quUpd.SQL.Add('declare variable dproc float;');
      quUpd.SQL.Add('declare variable dsum double precision;');
      quUpd.SQL.Add('declare variable rsum double precision;');
      quUpd.SQL.Add('declare variable amark varchar(150);');
      quUpd.SQL.Add('BEGIN');
      quUpd.SQL.Add('  CURDATE = cast(''now'' as timestamp);');
      quUpd.SQL.Add('  Select Max(ID) from acheck_hd into :MAXID;');
      quUpd.SQL.Add('  if (MAXID is null) then MAXID=0;');
      quUpd.SQL.Add('  MAXID=:MAXID+1;');
      quUpd.SQL.Add('  insert into acheck_hd (CASHNUMBER,ZNUMBER,CHECKNUMBER,ID,CASHER,DBAR,CHDATE,PAYMENT,OPER)');
      quUpd.SQL.Add('  values (:CASHNUM,:ZNUMBER,:NUMCHECK,:MAXID,:CASHER,:DBAR,:CURDATE,:PAYMENT,:OPER);');
      quUpd.SQL.Add('  for select NUMPOS,ARTICUL,RBAR,QUANT,PRICE,DPROC,DSUM,RSUM,AMARK from CURCHECK CH');
      quUpd.SQL.Add('  where CASHNUM=:CASHNUM');
      quUpd.SQL.Add('  into :NUMPOS,:ARTICUL,:SBAR,:QUANT,:PRICE,:DPROC,:DSUM,:RSUM,:AMARK');
      quUpd.SQL.Add('  do begin');
      quUpd.SQL.Add('     insert into acheck_sp (IDH,ID,ARTICUL,BAR,QUANTITY,PRICERUB,DPROC,DSUM,TOTALRUB,AMARK)');
      quUpd.SQL.Add('     values (:MAXID,:NUMPOS,:ARTICUL,:SBAR,:QUANT,:PRICE,:DPROC,:DSUM,:RSUM,:AMARK);');
      quUpd.SQL.Add('  end');
      quUpd.SQL.Add('  SUSPEND;');
      quUpd.SQL.Add('END');

      quUpd.ExecQuery;
      delay(100);


      quTabDBList.Active:=False;
      quTabDBList.ParamByName('STABNAME').AsString:='TESTVES';
      quTabDBList.Active:=True;
      if quTabDBList.RecordCount=0 then
      begin //����� ��������� �������� TESTVES
        quUpd.SQL.Clear;
        quUpd.SQL.Add('CREATE TABLE TESTVES (');
        quUpd.SQL.Add('ARTICUL VARCHAR (30) character set WIN1251 NOT NULL collate WIN1251);');
        quUpd.ExecQuery;
        delay(100);

        quUpd.SQL.Clear;
        quUpd.SQL.Add('ALTER TABLE TESTVES ADD CONSTRAINT PK_TESTVES PRIMARY KEY (ARTICUL);');
        quUpd.ExecQuery;
        delay(100);
      end;
      quTabDBList.Active:=False;


      quTabDBList.Active:=False;
      quTabDBList.ParamByName('STABNAME').AsString:='CARDSCLA';
      quTabDBList.Active:=True;
      if quTabDBList.RecordCount>0 then
      begin //�������� ����
        bUpd:=True;

        quTabDBList.First;
        while not quTabDBList.Eof do
        begin
          if quTabDBListRDBFIELD_NAME.AsString='AVID' then begin bUpd:=False; break; end;
          quTabDBList.Next;
        end;

        if bUpd then //������� ���� ��� - ��������
        begin
          quUpd.SQL.Clear;
          quUpd.SQL.Add('ALTER TABLE CARDSCLA  ADD AVID INTEGER DEFAULT 0');
          quUpd.ExecQuery;
          delay(100);

          quUpd.SQL.Clear;
          quUpd.SQL.Add('ALTER TABLE CARDSCLA ADD ALCVOL DOUBLE PRECISION DEFAULT 0');
          quUpd.ExecQuery;
          delay(100);

          quUpd.SQL.Clear;
          quUpd.SQL.Add('update cardscla set avid = 0, alcvol = 0');
          quUpd.ExecQuery;
          delay(100);
        end;
      end;

      quTabDBList.Active:=False;
      quTabDBList.ParamByName('STABNAME').AsString:='CARDSCLA';
      quTabDBList.Active:=True;
      if quTabDBList.RecordCount>0 then
      begin //�������� ����
        bUpd:=True;

        quTabDBList.First;
        while not quTabDBList.Eof do
        begin
          if quTabDBListRDBFIELD_NAME.AsString='INDS' then begin bUpd:=False; break; end;
          quTabDBList.Next;
        end;

        if bUpd then //������� ���� ��� - ��������
        begin
          quUpd.SQL.Clear;
          quUpd.SQL.Add('ALTER TABLE CARDSCLA ADD INDS SMALLINT DEFAULT 2');
          quUpd.ExecQuery;
          delay(100);

          quUpd.SQL.Clear;
          quUpd.SQL.Add('update cardscla set inds = 2');
          quUpd.ExecQuery;
          delay(100);
        end;
      end;


      quTabDBList.Active:=False;
      quTabDBList.ParamByName('STABNAME').AsString:='FIXAVID';
      quTabDBList.Active:=True;
      if quTabDBList.RecordCount=0 then
      begin //����� ��������� �������� FIXAVID
        quUpd.SQL.Clear;
        quUpd.SQL.Add('CREATE TABLE FIXAVID (');
        quUpd.SQL.Add('    IID       INTEGER NOT NULL,');
        quUpd.SQL.Add('    AVIDNAME  VARCHAR(100),');
        quUpd.SQL.Add('    SID       VARCHAR(10)');
        quUpd.SQL.Add(');');

        quUpd.ExecQuery;
        delay(100);

        quUpd.SQL.Clear;
        quUpd.SQL.Add('ALTER TABLE FIXAVID ADD CONSTRAINT PK_FIXAVID PRIMARY KEY (IID);');
        quUpd.ExecQuery;

        delay(100);
      end;

      quProcDBList.Active:=False;
      quProcDBList.ParamByName('STABNAME').AsString:='PR_ADDCARD_A';
      quProcDBList.Active:=True;
      if quProcDBList.RecordCount=0 then
      begin //����� ��������� ���������
        quUpd.SQL.Clear;

        quUpd.SQL.Add('CREATE PROCEDURE PR_ADDCARD_A (');
        quUpd.SQL.Add('    articul varchar(30),');
        quUpd.SQL.Add('    classif integer,');
        quUpd.SQL.Add('    depart integer,');
        quUpd.SQL.Add('    name varchar(80),');
        quUpd.SQL.Add('    card_type integer,');
        quUpd.SQL.Add('    mesuriment char(10),');
        quUpd.SQL.Add('    price_rub float,');
        quUpd.SQL.Add('    discquant double precision,');
        quUpd.SQL.Add('    discount float,');
        quUpd.SQL.Add('    scale char(10),');
        quUpd.SQL.Add('    avid integer,');
        quUpd.SQL.Add('    akrep float)');
        quUpd.SQL.Add('as');
        quUpd.SQL.Add('begin');
        quUpd.SQL.Add('  /* ������������ ��� ������ �������� */');
        quUpd.SQL.Add('  if (exists(select * from cardscla where ARTICUL = :ARTICUL)) then');
        quUpd.SQL.Add('  begin');
        quUpd.SQL.Add('    update cardscla');
        quUpd.SQL.Add('    set');
        quUpd.SQL.Add('    DEPART=:DEPART,');
        quUpd.SQL.Add('    CLASSIF=:CLASSIF,');
        quUpd.SQL.Add('    NAME=:NAME,');
        quUpd.SQL.Add('    CARD_TYPE=:CARD_TYPE,');
        quUpd.SQL.Add('    MESURIMENT=:MESURIMENT,');
        quUpd.SQL.Add('    PRICE_RUB=:PRICE_RUB,');
        quUpd.SQL.Add('    DISCQUANT=:DISCQUANT,');
        quUpd.SQL.Add('    DISCOUNT=:DISCOUNT,');
        quUpd.SQL.Add('    SCALE=:SCALE,');
        quUpd.SQL.Add('    AVID=:AVID,');
        quUpd.SQL.Add('    ALCVOL=:AKREP');
        quUpd.SQL.Add('    where ARTICUL=:ARTICUL;');
        quUpd.SQL.Add('  end');
        quUpd.SQL.Add('  else');
        quUpd.SQL.Add('  begin');
        quUpd.SQL.Add('    insert into cardscla (ARTICUL,CLASSIF,DEPART,NAME,CARD_TYPE,MESURIMENT,PRICE_RUB,DISCQUANT,DISCOUNT,SCALE,AVID,ALCVOL)');
        quUpd.SQL.Add('    values (:ARTICUL,:CLASSIF,:DEPART,:NAME,:CARD_TYPE,:MESURIMENT,:PRICE_RUB,:DISCQUANT,:DISCOUNT,:SCALE,:AVID,:AKREP);');
        quUpd.SQL.Add('  end');
        quUpd.SQL.Add('  suspend;');
        quUpd.SQL.Add('end');

        quUpd.ExecQuery;
        delay(100);
      end;
      quProcDBList.Active:=False;

      quProcDBList.Active:=False;
      quProcDBList.ParamByName('STABNAME').AsString:='PR_ADDCARD_AN';
      quProcDBList.Active:=True;
      if quProcDBList.RecordCount=0 then
      begin //����� ��������� ���������
        quUpd.SQL.Clear;

        quUpd.SQL.Add('CREATE PROCEDURE PR_ADDCARD_AN (');
        quUpd.SQL.Add('    articul varchar(30),');
        quUpd.SQL.Add('    classif integer,');
        quUpd.SQL.Add('    depart integer,');
        quUpd.SQL.Add('    name varchar(80),');
        quUpd.SQL.Add('    card_type integer,');
        quUpd.SQL.Add('    mesuriment char(10),');
        quUpd.SQL.Add('    price_rub float,');
        quUpd.SQL.Add('    discquant double precision,');
        quUpd.SQL.Add('    discount float,');
        quUpd.SQL.Add('    scale char(10),');
        quUpd.SQL.Add('    avid integer,');
        quUpd.SQL.Add('    akrep float,');
        quUpd.SQL.Add('    INDS smallint)');
        quUpd.SQL.Add('as');
        quUpd.SQL.Add('begin');
        quUpd.SQL.Add('  /* ������������ ��� ������ �������� */');
        quUpd.SQL.Add('  if (exists(select * from cardscla where ARTICUL = :ARTICUL)) then');
        quUpd.SQL.Add('  begin');
        quUpd.SQL.Add('    update cardscla');
        quUpd.SQL.Add('    set');
        quUpd.SQL.Add('    DEPART=:DEPART,');
        quUpd.SQL.Add('    CLASSIF=:CLASSIF,');
        quUpd.SQL.Add('    NAME=:NAME,');
        quUpd.SQL.Add('    CARD_TYPE=:CARD_TYPE,');
        quUpd.SQL.Add('    MESURIMENT=:MESURIMENT,');
        quUpd.SQL.Add('    PRICE_RUB=:PRICE_RUB,');
        quUpd.SQL.Add('    DISCQUANT=:DISCQUANT,');
        quUpd.SQL.Add('    DISCOUNT=:DISCOUNT,');
        quUpd.SQL.Add('    SCALE=:SCALE,');
        quUpd.SQL.Add('    AVID=:AVID,');
        quUpd.SQL.Add('    ALCVOL=:AKREP,');
        quUpd.SQL.Add('    INDS=:INDS');

        quUpd.SQL.Add('    where ARTICUL=:ARTICUL;');
        quUpd.SQL.Add('  end');
        quUpd.SQL.Add('  else');
        quUpd.SQL.Add('  begin');
        quUpd.SQL.Add('    insert into cardscla (ARTICUL,CLASSIF,DEPART,NAME,CARD_TYPE,MESURIMENT,PRICE_RUB,DISCQUANT,DISCOUNT,SCALE,AVID,ALCVOL,INDS)');
        quUpd.SQL.Add('    values (:ARTICUL,:CLASSIF,:DEPART,:NAME,:CARD_TYPE,:MESURIMENT,:PRICE_RUB,:DISCQUANT,:DISCOUNT,:SCALE,:AVID,:AKREP,:INDS);');
        quUpd.SQL.Add('  end');
        quUpd.SQL.Add('  suspend;');
        quUpd.SQL.Add('end');

        quUpd.ExecQuery;
        delay(100);
      end;
      quProcDBList.Active:=False;


      quUpd.SQL.Clear;

      quUpd.SQL.Add('');
      quUpd.SQL.Add('CREATE OR ALTER PROCEDURE PR_CALCDISCITOG (');
      quUpd.SQL.Add('    cashnum integer,');
      quUpd.SQL.Add('    icurdate integer)');
      quUpd.SQL.Add('as');
      quUpd.SQL.Add('declare variable anum integer;');
      quUpd.SQL.Add('declare variable dproc double precision;');
      quUpd.SQL.Add('declare variable articul integer;');
      quUpd.SQL.Add('declare variable quanta double precision;');
      quUpd.SQL.Add('declare variable quantch double precision;');
      quUpd.SQL.Add('declare variable quantach double precision;');
      quUpd.SQL.Add('declare variable quantpacket double precision;');
      quUpd.SQL.Add('declare variable iquantpacket integer;');
      quUpd.SQL.Add('declare variable quantpacketch double precision;');
      quUpd.SQL.Add('declare variable numpos integer;');
      quUpd.SQL.Add('declare variable quantpos double precision;');
      quUpd.SQL.Add('declare variable pricepos double precision;');
      quUpd.SQL.Add('declare variable dprocpos double precision;');
      quUpd.SQL.Add('declare variable dsumpos double precision;');
      quUpd.SQL.Add('declare variable rsumpos double precision;');
      quUpd.SQL.Add('declare variable quant_packet_cur double precision;');
      quUpd.SQL.Add('declare variable dprocpos_a double precision;');
      quUpd.SQL.Add('declare variable dsumpos_a double precision;');
      quUpd.SQL.Add('declare variable rsumpos_a double precision;');
      quUpd.SQL.Add('declare variable quantpos_dif double precision;');
      quUpd.SQL.Add('BEGIN');
      quUpd.SQL.Add('  for select ida, max(dproc) from akciya');
      quUpd.SQL.Add('            where idateb<=:ICURDATE');
      quUpd.SQL.Add('            and idatee>=:ICURDATE');
      quUpd.SQL.Add('            and iactive=1');
      quUpd.SQL.Add('            group by ida');
      quUpd.SQL.Add('  into :ANUM,:DPROC');
      quUpd.SQL.Add('  do');
      quUpd.SQL.Add('  begin');
      quUpd.SQL.Add('    QUANTPACKET=1000;');
      quUpd.SQL.Add('');
      quUpd.SQL.Add('    for select ac.articul, ac.quant, coalesce(ch.quant,0) as quantch');
      quUpd.SQL.Add('        from akciya ac');
      quUpd.SQL.Add('        left join vcurcheckq ch on ch.cashnum=:cashnum and cast(ac.articul as varchar(30))=ch.articul');
      quUpd.SQL.Add('        where ac.ida=:ANUM');
      quUpd.SQL.Add('        and ac.idateb<=:ICURDATE');
      quUpd.SQL.Add('        and ac.idatee>=:ICURDATE');
      quUpd.SQL.Add('        and ac.iactive=1');
      quUpd.SQL.Add('     into :articul,:QUANTA,:QUANTCH');
      quUpd.SQL.Add('     do');
      quUpd.SQL.Add('     begin  -- ������� ����������� ����� ��������� � ���� �� ��������');
      quUpd.SQL.Add('       QUANTACH=:QUANTCH/:QUANTA;');
      quUpd.SQL.Add('       if (:QUANTACH<:QUANTPACKET) then QUANTPACKET=:QUANTACH;');
      quUpd.SQL.Add('     end');
      quUpd.SQL.Add('     if (QUANTPACKET<1000) then');
      quUpd.SQL.Add('     begin');
      quUpd.SQL.Add('       if (QUANTPACKET>=1) then');
      quUpd.SQL.Add('       begin  --  �������� � ���� ���� :QUANTPACKET ���');
      quUpd.SQL.Add('         IQUANTPACKET=cast(:QUANTPACKET as integer);');
      quUpd.SQL.Add('         -- ����� ���� �� ������� ���������');
      quUpd.SQL.Add('         for select ac.articul, ac.quant from akciya ac');
      quUpd.SQL.Add('             where ac.ida=:ANUM');
      quUpd.SQL.Add('             and ac.idateb<=:ICURDATE');
      quUpd.SQL.Add('             and ac.idatee>=:ICURDATE');
      quUpd.SQL.Add('             and ac.iactive=1');
      quUpd.SQL.Add('             into :articul,:QUANTA');
      quUpd.SQL.Add('             do');
      quUpd.SQL.Add('             begin');
      quUpd.SQL.Add('               quantpacketch=:QUANTA*IQUANTPACKET; -- ���-�� ��������� ������� ������� ��������� � ��������� ������');
      quUpd.SQL.Add('               quant_packet_cur=:quantpacketch;');
      quUpd.SQL.Add('               -- ����� �� �������� CURCHECK');
      quUpd.SQL.Add('               for select numpos, quant, price, dproc, dsum, rsum');
      quUpd.SQL.Add('                    from curcheck');
      quUpd.SQL.Add('                    where cashnum=:cashnum and articul=:articul');
      quUpd.SQL.Add('               into :numpos,:quantpos,:pricepos,:dprocpos,:dsumpos,:rsumpos');
      quUpd.SQL.Add('               do');
      quUpd.SQL.Add('               begin');
      quUpd.SQL.Add('                 if (quantpos>0) then');
      quUpd.SQL.Add('                 begin');
      quUpd.SQL.Add('                   if (quant_packet_cur>0) then');
      quUpd.SQL.Add('                   begin');
      quUpd.SQL.Add('                     if (quantpos<=quant_packet_cur) then  -- ��� ������� ��� ������');
      quUpd.SQL.Add('                     begin');
      quUpd.SQL.Add('                       dsumpos_a=:quantpos*:pricepos*:DPROC/100;');
      quUpd.SQL.Add('                       rsumpos_a=:quantpos*:pricepos-:dsumpos_a;');
      quUpd.SQL.Add('                        dprocpos_a=:DPROC;');
      quUpd.SQL.Add('                       quant_packet_cur=:quant_packet_cur-:quantpos;  -- ������� ������� �� �����');
      quUpd.SQL.Add('                     end else  -- ����� ������� ��������� ���-�� �� �����');
      quUpd.SQL.Add('                     begin');
      quUpd.SQL.Add('                       quantpos_dif=:quantpos-:quant_packet_cur;');
      quUpd.SQL.Add('                       dsumpos_a=:quant_packet_cur*:pricepos*:DPROC/100 + :quantpos_dif*:pricepos*:dprocpos/100;');
      quUpd.SQL.Add('                       rsumpos_a=:quantpos*:pricepos-:dsumpos_a;');
      quUpd.SQL.Add('                       dprocpos_a=:dsumpos_a/:rsumpos_a*100;');
      quUpd.SQL.Add('                       quant_packet_cur=0;  -- ������� ������� �� �����');
      quUpd.SQL.Add('                     end');
      quUpd.SQL.Add('                     update curcheck Set dproc=:dprocpos_a, dsum=:dsumpos_a   --, rsum=:rsumpos_a');
      quUpd.SQL.Add('                     where cashnum=:cashnum and articul=:articul and numpos=:numpos;');
      quUpd.SQL.Add('                   end');
      quUpd.SQL.Add('                 end');
      quUpd.SQL.Add('               end');
      quUpd.SQL.Add('             end');
      quUpd.SQL.Add('       end');
      quUpd.SQL.Add('     end');
      quUpd.SQL.Add('  end');
      quUpd.SQL.Add('  SUSPEND;');
      quUpd.SQL.Add('END');

      quUpd.ExecQuery;
      delay(100);


      quUpd.SQL.Clear;

      quUpd.SQL.Add('ALTER PROCEDURE PR_CHECKNUM (');
      quUpd.SQL.Add('    cashnum integer,         ');
      quUpd.SQL.Add('    shopindex integer,       ');
      quUpd.SQL.Add('    znumber integer)         ');
      quUpd.SQL.Add('returns (                    ');
      quUpd.SQL.Add('    checknum integer)        ');
      quUpd.SQL.Add('as                           ');
      quUpd.SQL.Add('BEGIN                        ');
      quUpd.SQL.Add('  SELECT max(CHECKNUMBER) from CASHPAY');
      quUpd.SQL.Add('  where                               ');
      quUpd.SQL.Add('  SHOPINDEX=:SHOPINDEX                ');
      quUpd.SQL.Add('  and CASHNUMBER=:CASHNUM             ');
      quUpd.SQL.Add('  and ZNUMBER=:ZNUMBER                ');
      quUpd.SQL.Add('  and CHECKNUMBER<5000                ');
      quUpd.SQL.Add('  into :CHECKNUM;                     ');
      quUpd.SQL.Add('  if (CHECKNUM is null) then CHECKNUM=0; ');
      quUpd.SQL.Add('  SUSPEND;');
      quUpd.SQL.Add('END');
      quUpd.ExecQuery;
      delay(100);


    except
    end;
  end;
end;


Function FindClName(iArt:Integer):String;
begin
  with dmC do
  begin
    quNameClFind.Active:=False;
    quNameClFind.ParamByName('IART').AsInteger:=iArt;
    quNameClFind.Active:=True;
    if quNameClFind.RecordCount>0 then
    begin
      quNameClFind.First;
      Result:=quNameClFindNAME.AsString;
    end else Result:='';
    quNameClFind.Active:=False;
  end;
end;



Procedure OpenMoneyBox;
Var Buff:Array[1..5] of Char;
begin
  with dmC do
  begin
  try

    Buff[1]:=#$1B;   //�������������
    Buff[2]:=#$70;
    Buff[3]:=#$00;
    Buff[4]:=#$32;
    Buff[5]:=#$32;

    DevPrint.WriteBuf(Buff,5);
    delay(50);

  finally
//    DevPrint.Close;
  end;
  end;
end;


Procedure PrintStr(StrP:String);
Var Buff:Array[1..100] of Char;
    i,iCount:Integer;
begin
  with dmC do
  begin
    StrP:=AnsiToOemConvert(StrP);
    iCount:=Length(StrP);
    for i:=1 to iCount do Buff[i]:=StrP[i];
    Buff[iCount+1]:=#$0A;
    try
      DevPrint.WriteBuf(Buff,iCount+1);
    except
    end;
    delay(CommonSet.ComDelay);
  end;
end;


Function prGetNFCheckNum:Integer;
begin
  Result:=0;
  with dmC do
  begin
    if CasherDb.Connected then
    begin
  //?CASHNUM, ?SHOPINDEX, ?ZNUMBER
      prGetNFChNum.ParamByName('SHOPINDEX').AsInteger:=1;
      prGetNFChNum.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
      prGetNFChNum.ParamByName('ZNUMBER').AsInteger:=prGetNFZ;
      prGetNFChNum.ExecProc;
      result:=prGetNFChNum.ParamByName('CHECKNUM').AsInteger;
    end;
  end;
end;

Function prSetNFZ:Integer;
begin
  with dmC do
  begin
    if CommonSet.TypeFis='sp802' then //������� ������� Z  ��� ���������� ������ ��� 802 �����������
    begin
      quZList.Active:=False;
      quZList.Active:=True;
      if quZList.RecordCount>0 then
      begin
        quZList.First;
        quZList.Edit;
//        quZListIZ.AsInteger:=Nums.ZNum;
        quZListSUMSALE.AsFloat:=Nums.ZNum;
        quZListSUMRET.AsFloat:=Nums.ZNum;
        quZList.Post;
      end else
      begin
        quZList.Append;
        quZListIZ.AsInteger:=Nums.ZNum;
        quZListSUMSALE.AsFloat:=Nums.ZNum;
        quZListSUMRET.AsFloat:=Nums.ZNum;
        quZList.Post;
        result:=0;
      end;
      quZList.Active:=False;
    end else result:=GetId('SetZ'); //��� ������������� ������
  end;
end;


Function prGetNFZ:Integer;
begin
  Result:=0;
  with dmC do
  begin
    if CommonSet.TypeFis='sp802' then //������� ������� Z  ��� ���������� ������ ��� 802 �����������
    begin
      if CasherDb.Connected then
      begin
        quZList.Active:=False;
        quZList.Active:=True;
        if quZList.RecordCount>0 then
        begin
          quZList.First;
          result:=RoundEx(quZListSUMSALE.AsFloat);
        end else
        begin
          quZList.Append;
          quZListIZ.AsInteger:=Nums.ZNum;
          quZListSUMSALE.AsFloat:=Nums.ZNum;
          quZListSUMRET.AsFloat:=Nums.ZNum;
          quZList.Post;
          result:=Nums.ZNum;
        end;
        quZList.Active:=False;
      end;
    end else result:=GetId('CurZ'); //��� ������������� ������
  end;
end;


procedure prWriteLogPS(sStr:String);
begin
//���� ������
end;


Procedure TdmC.PrintStrDev(StrP,S:String);
Var Buff:Array[1..100] of Char;
    i,iCount:Integer;
begin
  if pos('fis',StrP)>0 then PrintNFStr(S);

  if (StrP[1]='S')or(StrP[1]='s')or(StrP[1]='B')or(StrP[1]='b') then
  begin
    if S='-' then s:='----------------------------------------'
  end else
  begin
    if S='-' then s:='                                        '
  end;
  if pos('COM',StrP)>0 then
  begin

    S:=AnsiToOemConvert(S);
    iCount:=Length(S);
//    for i:=1 to iCount do Buff[i]:=S[i];
    for i:=1 to iCount do prINBuf(S[i]);
    Buff[iCount+1]:=#$0A; prINBuf(#$0A);

    try
//      DevPrint.WriteBuf(Buff,iCount+1);
    except
    end;
//    delay(CommonSet.ComDelay);
  end;
  if pos('LPT',StrP)>0 then
  begin
    if (StrP[1]='S')or(StrP[1]='s') then
    begin
      if bPrintOpen then prWritePrinter(AnsiToOemConvert(S)+#$0D+#$0A);
    end else
    begin
      if bPrintOpen then prWritePrinter(AnsiToOemConvert(S)+#$0D+#$0A);
    end;
  end;
end;



Function prOpenPrinter(PName:String):Boolean;
Var  DocInfo1: TDocInfo1;
begin
  prWriteLog('   �������� ������� - '+PName);
  Result:=False;
  if OpenPrinter(PChar(PName), PrintHandle, nil) then
  begin
    Result:=True;
    with DocInfo1 do begin
      pDocName := PChar('Tmp_doc');
      pOutputFile := nil;
      pDataType := 'RAW';
    end;
    StartDocPrinter(PrintHandle, 1, @DocInfo1);
    StartPagePrinter(PrintHandle);
  end
  else prWriteLog('   ������� �� ���� - '+PName);
end;

Procedure prClosePrinter(PName:String);
begin
  prWriteLog('   �������� ������� - '+PName);
  EndPagePrinter(PrintHandle);
  EndDocPrinter(PrintHandle);
  ClosePrinter(PrintHandle);
  bPrintOpen:=False;
end;

Procedure prWritePrinter(S:String);
Var  N: DWORD;
begin
  WritePrinter(PrintHandle, PChar(S), Length(S), N);
end;



Procedure TdmC.prPrintStr(StrP,S:String);
Var Buff:Array[1..100] of Char;
    i,iCount:Integer;
begin
//  prWriteLogPS('   ������ ������ ('+StrP+') "'+S+'"');
  if pos('fis',StrP)>0 then PrintNFStr(S);

  if (StrP[1]='S')or(StrP[1]='s')or(StrP[1]='B')or(StrP[1]='b') then
  begin
    if S='-' then s:='----------------------------------------'
  end else
  begin
    if S='-' then s:='                                        '
  end;
  if pos('COM',StrP)>0 then
  begin

    S:=AnsiToOemConvert(S);
    iCount:=Length(S);

//    for i:=1 to iCount do Buff[i]:=S[i];
    for i:=1 to iCount do prInBuf(S[i]);

    Buff[iCount+1]:=#$0A;
    prInBuf(#$0A);

    try
//      DevPrint.WriteBuf(Buff,iCount+1);
    except
    end;
//    delay(CommonSet.ComDelay);
  end;
  if pos('LPT',StrP)>0 then
  begin
    if (StrP[1]='S')or(StrP[1]='s') then
    begin
      if bPrintOpen then prWritePrinter(AnsiToOemConvert(S)+#$0D+#$0A);
    end else
    begin
      if bPrintOpen then prWritePrinter(AnsiToOemConvert(S)+#$0D+#$0A);
    end;
  end;
end;


Procedure TdmC.prSetFont(StrP:String;iFont:INteger;iLog:ShortInt); //��������� �����
Var iFl,iFl1:Byte;
    Buff:Array[1..100] of Char;
    iTypeP:INteger;
begin
  if iLog=1 then prWriteLogPS('   ��������� ����� '+IntToStr(iFont)+'('+StrP+')');
  if pos('fis',StrP)>0 then SelectF(iFont);
  if pos('COM',StrP)>0 then
  begin
    iTypeP:=0;
    if (StrP[1]='S')or(StrP[1]='s') then iTypeP:=1;

    if iTypeP=0 then //������ �� ���������
    begin
      iFl:=$00;
      Case iFont of
      0: begin iFl:=$01 end;  //�������
      1: begin iFl:=$21 end;  //������� ������
      2: begin iFl:=$11 end;  //������� ������
      3: begin iFl:=$81 end;  //�������������
      4: begin iFl:=$31 end;  //������� ��� � ���

      5: begin iFl:=$09 end;  //�������
      6: begin iFl:=$29 end;  //������� ������
      7: begin iFl:=$19 end;  //������� ������
      8: begin iFl:=$89 end;  //�������������
      9: begin iFl:=$39 end;  //������� ��� � ���

      10: begin iFl:=$00 end; //�������
      11: begin iFl:=$20 end; //������� ������
      12: begin iFl:=$10 end; //������� ������
      13: begin iFl:=$80 end; //�������������
      14: begin iFl:=$30 end; //������� ��� � ���

      15: begin iFl:=$08 end; //�������
      16: begin iFl:=$28 end; //������� ������
      17: begin iFl:=$18 end; //������� ������
      18: begin iFl:=$88 end; //�������������
      19: begin iFl:=$38 end; //������� ��� � ���

      end;

      Buff[1]:=#$1B;
      Buff[2]:=#$21;
      Buff[3]:=chr(iFl);

      prInBuf(Buff[1]);
      prInBuf(Buff[2]);
      prInBuf(Buff[3]);

      try
//        DevPrint.WriteBuf(Buff,3);
      except
      end;
    end;

    if iTypeP=1 then //Star600
    begin
      iFl:=$30;  iFl1:=$30;
      Case iFont of
      10: begin iFl:=$30; iFl1:=$30; end; //�������
      13: begin iFl:=$30; iFl1:=$30; end; //�������������
      14: begin iFl:=$31; iFl1:=$31; end; //������� ��� � ���
      15: begin iFl:=$30; iFl1:=$30; end; //�������
      end;

      Buff[1]:=#$1B;
      Buff[2]:=#$69; // 30 30
      Buff[3]:=chr(iFl);
      Buff[4]:=chr(iFl1);

      prInBuf(Buff[1]);
      prInBuf(Buff[2]);
      prInBuf(Buff[3]);
      prInBuf(Buff[4]);

      try
//        DevPrint.WriteBuf(Buff,4);
      except
      end;
    end;
  end;

  if pos('LPT',StrP)>0 then
  begin
    iTypeP:=0;
    if (StrP[1]='S')or(StrP[1]='s') then iTypeP:=1;
    if (StrP[1]='B')or(StrP[1]='b') then iTypeP:=2;
    if iTypeP=1 then //Star600
    begin
      iFl:=$30;  iFl1:=$30;
      Case iFont of
      10: begin iFl:=$30; iFl1:=$30; end; //�������
      13: begin iFl:=$30; iFl1:=$30; end; //�������������
      14: begin iFl:=$31; iFl1:=$31; end; //������� ��� � ���
      15: begin iFl:=$30; iFl1:=$30; end; //�������
      end;

      if bPrintOpen then prWritePrinter(#$1B+#$69+chr(iFl)+chr(iFl1));
    end;
    if iTypeP=2 then //Star600
    begin
      iFl:=$00;
      Case iFont of
      0: begin iFl:=$01 end;  //�������
      1: begin iFl:=$21 end;  //������� ������
      2: begin iFl:=$11 end;  //������� ������
      3: begin iFl:=$81 end;  //�������������
      4: begin iFl:=$31 end;  //������� ��� � ���

      5: begin iFl:=$09 end;  //�������
      6: begin iFl:=$29 end;  //������� ������
      7: begin iFl:=$19 end;  //������� ������
      8: begin iFl:=$89 end;  //�������������
      9: begin iFl:=$39 end;  //������� ��� � ���

      10: begin iFl:=$00 end; //�������
      11: begin iFl:=$20 end; //������� ������
      12: begin iFl:=$10 end; //������� ������
      13: begin iFl:=$80 end; //�������������
      14: begin iFl:=$30 end; //������� ��� � ���

      15: begin iFl:=$08 end; //�������
      16: begin iFl:=$28 end; //������� ������
      17: begin iFl:=$18 end; //������� ������
      18: begin iFl:=$88 end; //�������������
      19: begin iFl:=$38 end; //������� ��� � ���
      end;

      if bPrintOpen then prWritePrinter(#$1B+#$21+chr(iFl));
    end;
  end;
end;


Procedure TdmC.prRing(StrP:String); //��������
Var Buff:Array[1..100] of Char;
begin
  if pos('COM',StrP)>0 then
  begin
    if (StrP[1]='S')or(StrP[1]='s') then
    begin

    end else
    begin
      Buff[1]:=#$1B;
      Buff[2]:=#$70;
      Buff[3]:=#$00;
      Buff[4]:=#$FF;
      Buff[5]:=#$FF;
      try
        DevPrint.WriteBuf(Buff,5);
        delay(500);
      except
      end;
    end;
  end;
end;


Procedure TdmC.prCutDoc(StrP:String;iLog:ShortInt); //�������
//  Memo1.Lines.Add('�����');
Var Buff:Array[1..14] of Char;
    i:Integer;
    iTypeP:INteger;
begin
//  SelFont(0);
  if iLog=1 then prWriteLogPS(' �����.('+StrP+')');
  iTypeP:=0; //��� �� ���������
  if pos('fis',StrP)>0 then
  begin
    PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');
    CutDoc;
  end;

  if pos('COM',StrP)>0 then
  begin
    if (StrP[1]='S')or(StrP[1]='s') then iTypeP:=1;
    if (StrP[1]='A')or(StrP[1]='a') then iTypeP:=2;

    if (iTypeP=2)or(iTypeP=0) then //������ � ��� ���������
    begin
      {Buff[1]:=#$0A;

      try
        for i:=1 to 7 do DevPrint.WriteBuf(Buff,1); //�������
      except
      end;

      Buff[1]:=#$1B;
      Buff[2]:=#$69;
      try
        DevPrint.WriteBuf(Buff,2);
      except
      end;
      }
      delay(CommonSet.ComDelay);  //��� ����� �� ��������
      prSetFont(StrP,0,0);
      PrintStr(' ');
      PrintStr(' ');

      Buff[1]:=#$20;
      Buff[2]:=#$0A;

      Buff[3]:=#$20;
      Buff[4]:=#$0A;

      Buff[5]:=#$20;
      Buff[6]:=#$0A;

      Buff[7]:=#$20;
      Buff[8]:=#$0A;

      Buff[9]:=#$20;
      Buff[10]:=#$0A;

      Buff[11]:=#$1B;
      Buff[12]:=#$69;

      Buff[13]:=#$00;
      Buff[14]:=#$00;

      try
        DevPrint.WriteBuf(Buff,12);
        delay(CommonSet.ComDelay);
      except
      end;

//      PrintStr(StrP,' ');
    end;
    if iTypeP=1 then //Star 600
    begin
      Buff[1]:=#$1B;
      Buff[2]:=#$64;
      Buff[3]:=#$33;
      try
        DevPrint.WriteBuf(Buff,3);
        delay(100);
      except
      end;
    end;
  end;
  if pos('LPT',StrP)>0 then
  begin
    if (StrP[1]='S')or(StrP[1]='s') then iTypeP:=1;
    if (StrP[1]='B')or(StrP[1]='b') then iTypeP:=2;
    if iTypeP=1 then //Star 600
    begin
      if bPrintOpen then prWritePrinter(#$1B+#$64+#$33);
    end else
    begin
      if iTypeP=2 then
      begin
        if bPrintOpen then
        begin
          for i:=1 to 7 do prWritePrinter(#$0A);
          prWritePrinter(#$1B+#$69);
        end;
      end else
        if bPrintOpen then prWritePrinter(#$1B+#$69);
    end;
  end;
//  BufPr.iC:=0;
end;


Procedure TdmC.prDevClose(StrP:String;iLog:ShortInt);
Var pName:String;
//    kDelay:INteger;
begin //�������� ����������
  if iLog=1 then prWriteLogPS(' ��������.('+StrP+')');
  if pos('fis',StrP)>0 then CloseNFDoc;
  if pos('COM',StrP)>0 then
  begin
    try
//      kDelay:=(BufPr.iC div 40)-10;
//      if kDelay<0 then kDelay:=0;
//      delay(CommonSet.ComDelay*5+trunc(kDelay*CommonSet.ComDelay/2));

//      delay(CommonSet.ComDelay*2);
//      DevPrint.Close;
    except
    end;
  end;
  if pos('LPT',StrP)>0 then
  begin
    pName:=Copy(StrP,POS('LPT',StrP)+3,length(StrP)-POS('LPT',StrP)+2);
//    Memo1.Lines.Add(FormatDateTime('dd.mm.yyyy hh.nn.ss',now)+'  '+pName);
    prClosePrinter(pName);
  end;
//  BufPr.iC:=0;
end;


Procedure TdmC.prDevOpen(StrP:String;iLog:ShortInt);
Var Buff:Array[1..3] of Char;
    bAxiohm,bEpson:Boolean;
    PName:String;
begin  //�������� ����������
//  Memo1.Lines.Add('����-'+StrP);
  BufPr.iC:=0;
  if DevPrint.Active then
  begin
    if pos('COM',StrP)>0 then
    begin
      try
        bAxiohm:=False;
        bEpson:=False;
        if (StrP[1]='A') or(StrP[1]='Z') then  bAxiohm:=True;
        if (StrP[1]='E')or(StrP[1]='e') then   bEpson:=True;

       //��� ����������� ��� ������� - � ������ ��������� ����� �� �����
        if bAxiohm then
        begin
          Buff[1]:=#$1B;
          Buff[2]:=#$74;
          Buff[3]:=#$07;
          DevPrint.WriteBuf(Buff,3);
        end;
      //��� ����������� ��� Epsona - � ������ ��������� ����� �� �����
        if bEpson then
        begin
          Buff[1]:=#$1B;
          Buff[2]:=#$74;
          Buff[3]:=#$11;
          DevPrint.WriteBuf(Buff,3);
        end;

      except
        if iLog=1 then prWriteLogPS('   ������ �������� - '+StrP);
      end;
    end;

    exit;
  end;
  
  if iLog=1 then prWriteLogPS('   �������� - '+StrP);
  if pos('fis',StrP)>0 then OpenNFDoc;

  if pos('COM',StrP)>0 then
  begin
    try
      bAxiohm:=False;
      bEpson:=False;
      if (StrP[1]='A') or(StrP[1]='Z') then
      begin
        bAxiohm:=True;
        Delete(StrP,1,1);
      end;
      if (StrP[1]='E')or(StrP[1]='e') then
      begin
        bEpson:=True;
        Delete(StrP,1,1); //���� � �������� ����� ������ E - �� Epson ��������
      end;

      if (StrP[1]='S')then Delete(StrP,1,1);

      while pos(' ',StrP)>0 do delete(StrP,pos(' ',StrP),1);

      DevPrint.DeviceName:=StrP;
      DevPrint.Open;

   //��� ����������� ��� ������� - � ������ ��������� ����� �� �����

      if bAxiohm then
      begin
        Buff[1]:=#$1B;
        Buff[2]:=#$74;
        Buff[3]:=#$07;
        DevPrint.WriteBuf(Buff,3);
      end;
    //��� ����������� ��� Epsona - � ������ ��������� ����� �� �����
      if bEpson then
      begin
        Buff[1]:=#$1B;
        Buff[2]:=#$74;
        Buff[3]:=#$11;
        DevPrint.WriteBuf(Buff,3);
      end;

    except
      if iLog=1 then prWriteLogPS('   ������ �������� - '+StrP);
    end;
  end;
  if pos('LPT',StrP)>0 then
  begin
    pName:=Copy(StrP,POS('LPT',StrP)+3,length(StrP)-POS('LPT',StrP)+2);
    if prOpenPrinter(pName) then bPrintOpen:=True else bPrintOpen:=False;
  end;
end;


function TdmC.TypeEdIzm(sArt:String):Integer;
begin
  Result:=0;
  quIM.Active:=False;
  quIM.ParamByName('SART').AsString:=sART;
  quIM.Active:=True;
  if quIM.RecordCount>0 then
  begin
    quIM.First;
    Result:=quIMCARD_TYPE.AsInteger;
  end;
  quIM.Active:=False;
end;

Procedure FormLog(NAMEOP,CONTENT:String);
begin
  with dmC do
  begin
    prFormLog.ParamByName('IDPERSONAL').AsInteger:=Person.Id;
    prFormLog.ParamByName('NAMEOP').AsString:=NAMEOP;
    prFormLog.ParamByName('CONTENT').AsString:=CONTENT;
    prFormLog.ExecProc;
  end;
end;


Procedure CalcDiscont(sArt:String;PosNum:Integer;Price:Currency;Quantity:Real;Summa:Currency;DiscProc:Real; Var DiscountProc:Real;Var DiscountSum:Real);
begin
  //������ ������ �� ��������� �������
  with dmC do
  begin
//(?SART, ?POSNUM, ?PRICE, ?QUANTITY, ?SUMMA, ?DISCPROC, ?CURTIME, ?CURDATE)

    DiscountProc:=0;
    DiscountSum:=0;

    prCalcDisc.ParamByName('SART').AsString:=sArt;
    prCalcDisc.ParamByName('POSNUM').AsInteger:=PosNum;
    prCalcDisc.ParamByName('PRICE').AsFloat:=Price;
    prCalcDisc.ParamByName('QUANTITY').AsFloat:=Quantity;
    prCalcDisc.ParamByName('SUMMA').AsFloat:=Summa;
    prCalcDisc.ParamByName('DISCPROC').AsFloat:=DiscProc;
    prCalcDisc.ParamByName('CURTIME').AsFloat:=Now - Trunc(Now); //�����
    prCalcDisc.ParamByName('CURDATE').AsDateTime:=Now;
    prCalcDisc.ParamByName('TYPEDISC').AsInteger:=CommonSet.PremiumShop;
    prCalcDisc.ExecProc;

    DiscountProc:=prCalcDisc.ParamByName('DISCOUNTPROC').AsFloat;
    DiscountSum:=rv(prCalcDisc.ParamByName('DISCOUNTSUM').AsFloat);

  end;
end;

Function FindDiscount(sBar:String):Boolean;
Var bPrefFind:Boolean;
    StrWk:String;
    iL:Integer;
    StrWk1:String;
begin
  result:=False;
  Check.Discount:='';
  Check.DiscProc:=0;
  Check.DiscName:='';
  Check.RSum:=0;
  Check.DSum:=0;

  if Length(sBar)>30 then sBar:=Copy(sBar,1,30);

  if (CommonSet.KeybordType=2)or(CommonSet.KeybordType=3) then
  begin
    if pos('110020',sBar)=1 then delete(sBar,1,6);
    if pos('110020',sBar)=1 then delete(sBar,1,6);
  end;

  with dmC do
  begin    //������� ��� ������������ �� ��������� ����� �� ����� ����
    bPrefFind:=False;

    taDiscPre.Active:=False;
    taDiscPre.Active:=True;
    taDiscPre.First;
    while not taDiscPre.Eof do
    begin
      StrWk:=taDiscPreBARCODE.AsString;
      iL:=Length(StrWk);
      if Length(sBar)>=iL then
      begin
        StrWk1:=Copy(sBar,1,iL);
        if StrWk=StrWk1 then
        begin
          Result:=True;
          bPrefFind:=True;
          Check.Discount:=sBar;
          Check.DiscProc:=taDiscPrePERCENT.AsFloat;
          Check.DiscName:=taDiscPreNAME.AsString;
        end;
      end;
      taDiscPre.Next;
    end;
    taDiscPre.Active:=False;

    if not bPrefFind then
    begin //�� �������� �� ����� - ���� � �������� ����
      taDiscFind.Active:=False;
      taDiscFind.ParamByName('SBAR').AsString:=sBar;
      taDiscFind.Active:=True;
      if taDiscFind.RecordCount>0 then
      begin
        Result:=True;
        Check.Discount:=sBar;
        Check.DiscProc:=taDiscFindPERCENT.AsFloat;
        Check.DiscName:=taDiscFindNAME.AsString;
      end;
      taDiscFind.Active:=False;
    end;
  end;
end;


{Procedure CalcDiscont(Sifr,Id:Integer;Price,Quantity,Summa:Real;Discount:String;CurDateTime:TDateTime; Var DiscProc,DiscSum:Real);
begin
  DiscProc:=0;
  DiscSum:=0;

  if FindDiscount(Discount) then
  begin
    DiscProc:=Tab.DPercent;
    DiscSum:=Price*Quantity*Tab.DPercent/100;
  end;
end;
}


Function CheckOpen:Boolean;
Begin
  result:=False;
end;

Function FindPers(sP:String):Boolean;
begin
  result:=False;
  if Length(sP)>20 then sP:=Copy(sP,1,20);
  with dmC do
  begin
    taPersonal.Active:=true;
    if taPersonal.Locate('BARCODE',sP,[]) then
    begin
      if (taPersonalUVOLNEN.AsInteger=1) and (taPersonalMODUL3.AsInteger=0) then
      begin
        Person.Id:=taPersonalID.AsInteger;
        Person.Name:=taPersonalNAME.AsString;
        Result:=True;
      end;
    end;
    tapersonal.Active:=False;
  end;
end;


Function CanDo(Name_F:String):Boolean;
begin
  result:=True;
  with dmC do
  begin
    quCanDo.Active:=False;
    quCanDo.ParamByName('Person_id').Value:=Person.Id;
    quCanDo.ParamByName('Name_F').Value:=Name_F;
    quCanDo.Active:=True;
    if quCanDoprExec.AsBoolean=True then Result:=False;
  end;
end;

Function GetId(ta:String):Integer;
Var iType:Integer;
begin
  with dmC do
  begin
    iType:=0;
    if ta='Pe' then iType:=1; //��������
    if ta='Cl' then iType:=2; //�������������
    if ta='Trh' then iType:=3; //��������� ������
    if ta='Trs' then iType:=4; //������������ ������
    if ta='CurZ' then iType:=5; //������� Z �����
    if ta='SetZ' then iType:=6; //����� Z �����

    prGetId.ParamByName('ITYPE').AsInteger:=iType;
    prGetId.ExecProc;
    result:=prGetId.ParamByName('RESULT').Value;
  end;
end;


procedure TdmC.DataModuleCreate(Sender: TObject);
begin
{  with dmC do
  begin
    CasherDb.DBName:=DBName;
    try
      CasherDb.Open;

      taPersonal.Active:=True;
      taRClassif.Active:=True;
      quFuncList.Active:=True;
      taFuncList.Active:=True;

    except
      fmPerA.StatusBar1.Panels[0].Text:='���� �� ����������� - '+DBName;
    end;
  end;}
end;                                           

procedure TdmC.DataModuleDestroy(Sender: TObject);
begin
  taPersonal.Active:=False;
  taRClassif.Active:=False;
  quFuncList.Active:=False;
  taFuncList.Active:=False;

{  if trUpdate.Active then trUpdate.Commit;
  if trDel.Active then trDel.Commit;
  if trSelect.Active then trSelect.Commit;
}
  CasherDb.Close;
end;

procedure TdmC.quFuncListBeforePost(DataSet: TDataSet);
begin
  if taFuncList.Locate('Name',quFuncListNAME.AsString,[]) then
  begin
    taFuncList.Edit;
    taFuncListCOMMENT.AsString:=quFuncListCOMMENT.AsString;
    taFuncList.Post;
  end;
end;

procedure TdmC.devCasRxBuf(Sender: TObject; Data: PVaData; Count: Integer);
var P: Integer;
begin
  if Count >0 then
  begin
    for P:=0 to Count-1 do
    begin
      sScanEv:=sScanEv + char(Data[P]);
    end;
  end;
  iReadCount:=iReadCount+Count;
end;

procedure TdmC.FisPrint_1RxChar(Sender: TObject; Count: Integer);
begin
//  BufferCount:=FisPrint_1.ReadBuf(Buffer, Count);
end;

procedure TdmC.FisPrintRxChar(Sender: TObject; Count: Integer);
Var Str:string;
begin
  FisPrint.ReadStr(Str, Count);
  sBuffer:=sBuffer+Str;
  BufferCount:=BufferCount+Count;
end;

procedure TdmC.ClientSockRead(Sender: TObject; Socket: TCustomWinSocket);
begin
  sBuffer:=sBuffer+Socket.ReceiveText;
end;

procedure writefislog(StrSend,StrRet:String);
var iDate:Integer;
    IdMax:Integer;
begin
  with dmC do
  begin
    if CasherDb.Connected then
    begin
      iDate:=Trunc(date);
      IdMax:=1;
      quSelIdLogXML.Active:=False;
      quSelIdLogXML.Active:=True;
      if quSelIdLogXML.RecordCount>0 then IdMax:=quSelIdLogXMLMAX.AsInteger+1;
      quSelIdLogXML.Active:=False;

      quInsLogXML.SQL.Clear;
      quInsLogXML.SQL.Add('');
      quInsLogXML.SQL.Add('insert into logxml (idate, id, ssend, sret)');
      quInsLogXML.SQL.Add('values ('+its(iDate)+','+its(IdMax)+','''+StrSend+''','''+StrRet+''')');
      quInsLogXML.ExecQuery;
    end;
  end;
end;


end.
