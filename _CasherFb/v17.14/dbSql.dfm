object dmSQL: TdmSQL
  Left = 429
  Top = 93
  Width = 527
  Height = 327
  Caption = 'dmSQL'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object msConnection: TADOConnection
    CommandTimeout = 1800
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=YKS15Le;Persist Security Info=True;' +
      'User ID=sa;Initial Catalog=Debor;Data Source=192.168.1.150;Use P' +
      'rocedure for Prepare=1;Auto Translate=True;Packet Size=4096;Work' +
      'station ID=SYS-9;Use Encryption for Data=False;Tag with column c' +
      'ollation when possible=False'
    ConnectionTimeout = 30
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 36
    Top = 21
  end
  object quSel: TADOQuery
    Connection = msConnection
    CommandTimeout = 800
    Parameters = <>
    Left = 36
    Top = 80
  end
  object quChecksSQL: TADOQuery
    Connection = msConnection
    CommandTimeout = 800
    Parameters = <>
    SQL.Strings = (
      
        'SELECT [Id] ,[Id_Shop] ,[Id_Depart] ,[Operation] ,[DateOperation' +
        '] ,[Ck_Number],[Cassir],[Cash_Code],[NSmena],[CardNumber],[Payme' +
        'ntType],[rSumCh],[rSumD],[CountPos]'
      '  FROM [dbo].[vcqHeads]'
      
        '  where [Id_Shop]=1  and [Cash_Code] = 1 and [NSmena] = 1 and  [' +
        'Ck_Number]=1')
    Left = 140
    Top = 24
    object quChecksSQLId: TLargeintField
      FieldName = 'Id'
      ReadOnly = True
    end
    object quChecksSQLId_Shop: TIntegerField
      FieldName = 'Id_Shop'
    end
    object quChecksSQLId_Depart: TIntegerField
      FieldName = 'Id_Depart'
    end
    object quChecksSQLOperation: TIntegerField
      FieldName = 'Operation'
    end
    object quChecksSQLDateOperation: TDateTimeField
      FieldName = 'DateOperation'
    end
    object quChecksSQLCk_Number: TIntegerField
      FieldName = 'Ck_Number'
    end
    object quChecksSQLCassir: TIntegerField
      FieldName = 'Cassir'
    end
    object quChecksSQLCash_Code: TIntegerField
      FieldName = 'Cash_Code'
    end
    object quChecksSQLNSmena: TIntegerField
      FieldName = 'NSmena'
    end
    object quChecksSQLCardNumber: TStringField
      FieldName = 'CardNumber'
    end
    object quChecksSQLPaymentType: TSmallintField
      FieldName = 'PaymentType'
    end
    object quChecksSQLrSumCh: TFloatField
      FieldName = 'rSumCh'
      ReadOnly = True
    end
    object quChecksSQLrSumD: TFloatField
      FieldName = 'rSumD'
      ReadOnly = True
    end
    object quChecksSQLCountPos: TIntegerField
      FieldName = 'CountPos'
      ReadOnly = True
    end
  end
  object quD: TADOQuery
    Connection = msConnection
    CommandTimeout = 800
    Parameters = <>
    Left = 32
    Top = 132
  end
  object quA: TADOQuery
    Connection = msConnection
    CommandTimeout = 800
    Parameters = <>
    Left = 32
    Top = 188
  end
  object quGetId: TADOQuery
    Connection = msConnection
    CommandTimeout = 800
    Parameters = <>
    SQL.Strings = (
      'SELECT SCOPE_IDENTITY() AS [ID]')
    Left = 140
    Top = 80
    object quGetIdID: TBCDField
      FieldName = 'ID'
      ReadOnly = True
      Precision = 32
      Size = 0
    end
  end
  object quSumZ: TADOQuery
    Connection = msConnection
    Parameters = <
      item
        Name = 'ISHOP'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = 'IDATE'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = 'ICASH'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = 'IZ'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      'Declare @ISHOP int = :ISHOP'
      'Declare @IDATE int = :IDATE'
      'Declare @ICASH int = :ICASH'
      'Declare @IZ int = :IZ'
      ''
      ''
      'SELECT  round(isnull(SUM(t1.Summa),0),2) AS RSum'
      '        ,round(isnull(SUM(t1.SummaNal),0),2) AS SummaNal'
      #9#9',round(isnull(SUM(t1.SummaBn),0),2) AS SummaBn'
      '        ,round(isnull(SUM(t1.SummaRetNal),0),2) AS SummaRetNal'
      #9#9',round(isnull(SUM(t1.SummaRetBn),0),2) AS SummaRetBn'
      
        'FROM (SELECT Id_Shop, IDate, Cash_Code, NSmena, Ck_Number, DSum,' +
        ' Summa'
      
        #9'          ,CASE WHEN [PaymentType]=0 THEN [Summa] ELSE 0 END AS' +
        ' SummaRetNal '
      
        '              ,CASE WHEN [PaymentType]=4 THEN [Summa] ELSE 0 END' +
        ' AS SummaRetBn'
      
        #9'          ,CASE WHEN [PaymentType]=1 THEN [Summa] ELSE 0 END AS' +
        ' SummaNal '
      
        '              ,CASE WHEN [PaymentType]=5 THEN [Summa] ELSE 0 END' +
        ' AS SummaBn'
      '       FROM dbo.CH_SP'
      '       where [Id_Shop]=@ISHOP'
      '       and [IDate]>=@IDATE'
      '       and [Cash_Code]=@ICASH'
      '       and [NSmena]=@IZ ) AS t1')
    Left = 248
    Top = 24
    object quSumZRSum: TFloatField
      FieldName = 'RSum'
      ReadOnly = True
    end
    object quSumZSummaNal: TFloatField
      FieldName = 'SummaNal'
      ReadOnly = True
    end
    object quSumZSummaBn: TFloatField
      FieldName = 'SummaBn'
      ReadOnly = True
    end
    object quSumZSummaRetNal: TFloatField
      FieldName = 'SummaRetNal'
      ReadOnly = True
    end
    object quSumZSummaRetBn: TFloatField
      FieldName = 'SummaRetBn'
      ReadOnly = True
    end
  end
  object quChecksS: TADOQuery
    Connection = msConnection
    CommandTimeout = 1800
    Parameters = <
      item
        Name = 'ISHOP'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = 'IDATE'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = 'ICASH'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = 'IZ'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      'Declare @ISHOP int = :ISHOP'
      'Declare @IDATE int = :IDATE'
      'Declare @ICASH int = :ICASH'
      'Declare @IZ int = :IZ'
      ''
      ''
      'SELECT [Ck_Number],SUM([Summa]) as RSUM '
      'FROM [Debor].[dbo].[CH_SP]'
      'where [Id_Shop]=@ISHOP'
      'and [IDate]>=@IDATE'
      'and [IDate]<=@IDATE+1'
      'and [Cash_Code]=@ICASH'
      'and [NSmena]=@IZ'
      'group by [Ck_Number]'
      'order by [Ck_Number]')
    Left = 248
    Top = 80
    object quChecksSCk_Number: TIntegerField
      FieldName = 'Ck_Number'
    end
    object quChecksSRSUM: TFloatField
      FieldName = 'RSUM'
      ReadOnly = True
    end
  end
  object quGetAMQ: TADOQuery
    Connection = msConnection
    Parameters = <
      item
        Name = 'AM'
        DataType = ftString
        Size = 6
        Value = '111111'
      end>
    SQL.Strings = (
      'Declare @AM varchar(100) = :AM'
      'SELECT isnull(SUM([Quant]),0) as QUANT'
      '  FROM [Debor].[dbo].[CH_SP]'
      '  where [AMARK]=@AM')
    Left = 245
    Top = 148
    object quGetAMQQUANT: TFloatField
      FieldName = 'QUANT'
      ReadOnly = True
    end
  end
end
