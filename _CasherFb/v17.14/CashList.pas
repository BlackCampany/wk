unit CashList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, cxControls, cxContainer, cxEdit, cxTextEdit, cxMemo,
  StdCtrls, Menus, cxLookAndFeelPainters, cxButtons, cxStyles,
  cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, DB, cxDBData,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, cxCheckBox, ActnList,
  XPStyleActnCtrls, ActnMan, FIBDatabase, pFIBDatabase, FIBQuery, pFIBQuery;

type
  TfmCashList = class(TForm)
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    MemoSQL: TcxMemo;
    cxButton1: TcxButton;
    Panel2: TPanel;
    ViCash: TcxGridDBTableView;
    LeCash: TcxGridLevel;
    GrCash: TcxGrid;
    ViCashISHOP: TcxGridDBColumn;
    ViCashINUM: TcxGridDBColumn;
    ViCashSPATH: TcxGridDBColumn;
    ViCashIRES: TcxGridDBColumn;
    Memo2: TcxMemo;
    ViCashIST: TcxGridDBColumn;
    amCashList: TActionManager;
    acSetAll: TAction;
    acResetAll: TAction;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    cxButton2: TcxButton;
    CashDB: TpFIBDatabase;
    trSelect: TpFIBTransaction;
    trUpdate: TpFIBTransaction;
    trDel: TpFIBTransaction;
    quUpdCash: TpFIBQuery;
    procedure FormCreate(Sender: TObject);
    procedure acSetAllExecute(Sender: TObject);
    procedure acResetAllExecute(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmCashList: TfmCashList;

implementation

uses MainCashUpd;

{$R *.dfm}

procedure TfmCashList.FormCreate(Sender: TObject);
begin
  MemoSQL.Clear;
  Memo2.Clear;
end;

procedure TfmCashList.acSetAllExecute(Sender: TObject);
begin
  with fmMainCashUpd do
  begin
    fmCashList.ViCash.BeginUpdate;
    quCashList.First;
    while not quCashList.Eof do
    begin
      quCashList.Edit;
      quCashListIST.AsInteger:=1;
      quCashListIRES.AsInteger:=0;
      quCashList.Post;
      quCashList.Next;
    end;
    fmCashList.ViCash.EndUpdate;
  end;
end;

procedure TfmCashList.acResetAllExecute(Sender: TObject);
begin
  with fmMainCashUpd do
  begin
    fmCashList.ViCash.BeginUpdate;
    quCashList.First;
    while not quCashList.Eof do
    begin
      quCashList.Edit;
      quCashListIST.AsInteger:=0;
      quCashListIRES.AsInteger:=0;
      quCashList.Post;
      quCashList.Next;
    end;
    fmCashList.ViCash.EndUpdate;
  end;
end;

procedure TfmCashList.cxButton2Click(Sender: TObject);
begin
  with fmMainCashUpd do
  begin
    Memo2.Clear;
    delay(10);
    Memo2.Lines.Add('�������� ����� �� �����.'); delay(10);
    quCashList.First;
    while not quCashList.Eof do
    begin
      quCashList.Edit;
      quCashListIRES.AsInteger:=1;  //����� ��������� ��� ������ - ����� �������
      quCashList.Post;

      if quCashListIST.AsInteger=1 then
      begin
        Memo2.Lines.Add('������� '+quCashListISHOP.AsString+' ����� � '+quCashListINUM.AsString); delay(10);

        try
          CashDB.Close;
          CashDB.DatabaseName:=Trim(quCashListSPATH.AsString); delay(10);
          CashDB.Open;
          if CashDB.Connected then
          begin
            Memo2.Lines.Add('   ����� � ����� ��'); delay(10);

            quCashList.Edit;
            quCashListIST.AsInteger:=0;
            quCashListIRES.AsInteger:=0;
            quCashList.Post;
          end else Memo2.Lines.Add('   ������ ����� � ����� !!!'); delay(10);
          CashDB.Close;
        except
          CashDB.Close;
          Memo2.Lines.Add('   ������ ����� � ����� !!!'); delay(10);
        end;
      end else
      begin
        quCashList.Edit;
        quCashListIST.AsInteger:=0;
        quCashListIRES.AsInteger:=0;
        quCashList.Post;
      end;
      quCashList.Next;
    end;

    Memo2.Lines.Add('�������� ���������.');

  end;
end;

procedure TfmCashList.cxButton1Click(Sender: TObject);
Var StrWk,StrWk1:String;
begin
  with fmMainCashUpd do
  begin
    Memo2.Clear;
    delay(10);
    Memo2.Lines.Add('���������� ������� �� �����.'); delay(10);
    quCashList.First;
    while not quCashList.Eof do
    begin
      quCashList.Edit;
      quCashListIRES.AsInteger:=1;  //����� ��������� ��� ������ - ����� �������
      quCashList.Post;

      if quCashListIST.AsInteger=1 then
      begin
        Memo2.Lines.Add('������� '+quCashListISHOP.AsString+' ����� � '+quCashListINUM.AsString); delay(10);

        try
          CashDB.Close;
          CashDB.DatabaseName:=Trim(quCashListSPATH.AsString); delay(10);
          CashDB.Open;
          if CashDB.Connected then
          begin
            Memo2.Lines.Add('   ����� � ����� ��'); delay(10);
            Memo2.Lines.Add('      ������ ..'); delay(10);

            StrWk:=MemoSQL.Text;

            quUpdCash.SQL.Clear;

            while pos(#$0D,StrWk)>0 do
            begin
              StrWk1:=Copy(StrWk,1,pos(#$0D,StrWk)-1);
              quUpdCash.SQL.Add(trim(StrWk1));
              delete(StrWk,1,pos(#$0D,StrWk));
            end;
            if Length(StrWk)>0 then quUpdCash.SQL.Add(trim(StrWk));

            quUpdCash.ExecQuery;

            Memo2.Lines.Add('        -- ��.'); delay(10);

            quCashList.Edit;
            quCashListIST.AsInteger:=0;
            quCashListIRES.AsInteger:=0;
            quCashList.Post;
          end else Memo2.Lines.Add('   ������ ����� � ����� !!!'); delay(10);
          CashDB.Close;
        except
          CashDB.Close;
          Memo2.Lines.Add('   ������ ����� � ����� !!!'); delay(10);
        end;
        Memo2.Lines.Add('');
      end else
      begin
        quCashList.Edit;
        quCashListIST.AsInteger:=0;
        quCashListIRES.AsInteger:=0;
        quCashList.Post;
      end;
      quCashList.Next;
    end;

    Memo2.Lines.Add('���������� ������� ���������.');
    Memo2.Lines.Add('');
  end;
end;

end.
