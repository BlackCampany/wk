object fmMainCashUpd: TfmMainCashUpd
  Left = 716
  Top = 118
  Width = 952
  Height = 718
  Caption = #1054#1073#1085#1086#1074#1083#1077#1085#1080#1077' '#1082#1072#1089#1089#1086#1074#1086#1075#1086' '#1055#1054
  Color = 14211288
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GrPath: TcxGrid
    Left = 0
    Top = 372
    Width = 944
    Height = 312
    Align = alBottom
    TabOrder = 0
    LookAndFeel.Kind = lfOffice11
    object ViPath: TcxGridDBTableView
      PopupMenu = PopupMenu2
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dsquPathFils
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsSelection.MultiSelect = True
      object ViPathIDSHOP: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1084#1072#1075#1072#1079#1080#1085#1072
        DataBinding.FieldName = 'IDSHOP'
        Width = 51
      end
      object ViPathNameShop: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'NameShop'
        Width = 130
      end
      object ViPathLASTUPD: TcxGridDBColumn
        Caption = #1055#1086#1089#1083#1077#1076#1085#1077#1077' '#1086#1073#1085#1086#1074#1083#1077#1085#1080#1077
        DataBinding.FieldName = 'LASTUPD'
        Options.Editing = False
      end
      object ViPathLASTSTATUS: TcxGridDBColumn
        Caption = #1057#1090#1072#1090#1091#1089
        DataBinding.FieldName = 'LASTSTATUS'
        Options.Editing = False
      end
      object ViPathSPATH: TcxGridDBColumn
        Caption = #1055#1091#1090#1100
        DataBinding.FieldName = 'SPATH'
        Width = 301
      end
    end
    object LePath: TcxGridLevel
      GridView = ViPath
    end
  end
  object Memo1: TcxMemo
    Left = 0
    Top = 33
    Align = alTop
    Lines.Strings = (
      'Memo1')
    TabOrder = 1
    Height = 137
    Width = 944
  end
  object GrVers: TcxGrid
    Left = 0
    Top = 170
    Width = 944
    Height = 202
    Align = alClient
    TabOrder = 2
    LookAndFeel.Kind = lfOffice11
    object ViVers: TcxGridDBTableView
      PopupMenu = PopupMenu1
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dsquVers
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      object ViVersID: TcxGridDBColumn
        DataBinding.FieldName = 'ID'
        Width = 56
      end
      object ViVersSVER: TcxGridDBColumn
        Caption = #1042#1077#1088#1089#1080#1103
        DataBinding.FieldName = 'SVER'
      end
      object ViVersTIME_VER: TcxGridDBColumn
        DataBinding.FieldName = 'TIME_VER'
      end
      object ViVersCASH_BIN: TcxGridDBColumn
        DataBinding.FieldName = 'CASH_BIN'
      end
      object ViVersUPD_TYPE: TcxGridDBColumn
        Caption = #1058#1080#1087' '#1086#1073#1085#1086#1074#1083#1077#1085#1080#1103
        DataBinding.FieldName = 'UPD_TYPE'
      end
      object ViVersILOADER: TcxGridDBColumn
        Caption = #1051#1086#1072#1076#1077#1088' '#1075#1088#1091#1079#1080#1090#1100
        DataBinding.FieldName = 'ILOADER'
        Width = 83
      end
      object ViVersLOADER: TcxGridDBColumn
        Caption = #1051#1086#1072#1076#1077#1088
        DataBinding.FieldName = 'LOADER'
        Width = 97
      end
    end
    object LeVers: TcxGridLevel
      GridView = ViVers
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 944
    Height = 33
    Align = alTop
    BevelInner = bvLowered
    Color = 16763025
    TabOrder = 3
    object cxButton1: TcxButton
      Left = 12
      Top = 4
      Width = 75
      Height = 25
      Caption = #1050#1072#1089#1089#1099
      TabOrder = 0
      OnClick = cxButton1Click
      LookAndFeel.Kind = lfOffice11
    end
  end
  object CashDBUpdater: TpFIBDatabase
    DBName = '192.168.1.150:D:\_CasherFb\DBUPD\CASHUPD.gdb'
    DBParams.Strings = (
      'user_name=SYSDBA'
      'lc_ctype=WIN1251'
      'password=masterkey'
      'sql_role_name=SYSDBA')
    DefaultTransaction = trSelect
    DefaultUpdateTransaction = trUpdate
    SQLDialect = 3
    Timeout = 0
    SynchronizeTime = False
    DesignDBOptions = []
    AliasName = 'DBUpdater'
    WaitForRestoreConnect = 20
    Left = 40
    Top = 360
  end
  object trSelect: TpFIBTransaction
    DefaultDatabase = CashDBUpdater
    TimeoutAction = TARollback
    Left = 40
    Top = 416
  end
  object trUpdate: TpFIBTransaction
    DefaultDatabase = CashDBUpdater
    TimeoutAction = TARollback
    Left = 40
    Top = 472
  end
  object trDel: TpFIBTransaction
    DefaultDatabase = CashDBUpdater
    Timeout = 300
    TimeoutAction = TARollback
    Left = 40
    Top = 532
  end
  object quPathFils: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE PATHEXP'
      'SET '
      '    "NameShop" = :"NameShop",'
      '    SPATH = :SPATH,'
      '    LASTUPD = :LASTUPD,'
      '    LASTSTATUS = :LASTSTATUS'
      'WHERE'
      '    IDSHOP = :OLD_IDSHOP'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    PATHEXP'
      'WHERE'
      '        IDSHOP = :OLD_IDSHOP'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO PATHEXP('
      '    IDSHOP,'
      '    "NameShop",'
      '    SPATH,'
      '    LASTUPD,'
      '    LASTSTATUS'
      ')'
      'VALUES('
      '    :IDSHOP,'
      '    :"NameShop",'
      '    :SPATH,'
      '    :LASTUPD,'
      '    :LASTSTATUS'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    IDSHOP,'
      '    "NameShop",'
      '    SPATH,'
      '    LASTUPD,'
      '    LASTSTATUS'
      'FROM'
      '    PATHEXP '
      ''
      ' WHERE '
      '        PATHEXP.IDSHOP = :OLD_IDSHOP'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    IDSHOP,'
      '    "NameShop",'
      '    SPATH,'
      '    LASTUPD,'
      '    LASTSTATUS'
      'FROM'
      '    PATHEXP ')
    Transaction = trSelect
    Database = CashDBUpdater
    UpdateTransaction = trUpdate
    AutoCommit = True
    Left = 132
    Top = 364
    object quPathFilsIDSHOP: TFIBIntegerField
      FieldName = 'IDSHOP'
    end
    object quPathFilsNameShop: TFIBStringField
      FieldName = 'NameShop'
      Size = 50
      EmptyStrToNull = True
    end
    object quPathFilsSPATH: TFIBStringField
      FieldName = 'SPATH'
      Size = 300
      EmptyStrToNull = True
    end
    object quPathFilsLASTUPD: TFIBDateTimeField
      FieldName = 'LASTUPD'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object quPathFilsLASTSTATUS: TFIBStringField
      FieldName = 'LASTSTATUS'
      EmptyStrToNull = True
    end
  end
  object dsquPathFils: TDataSource
    DataSet = quPathFils
    Left = 132
    Top = 420
  end
  object quVers: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE CASH_VERS'
      'SET '
      '    SVER = :SVER,'
      '    TIME_VER = :TIME_VER,'
      '    CASH_BIN = :CASH_BIN,'
      '    UPD_TYPE = :UPD_TYPE,'
      '    ILOADER = :ILOADER,'
      '    LOADER = :LOADER'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    CASH_VERS'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO CASH_VERS('
      '    ID,'
      '    SVER,'
      '    TIME_VER,'
      '    CASH_BIN,'
      '    UPD_TYPE,'
      '    ILOADER,'
      '    LOADER'
      ')'
      'VALUES('
      '    :ID,'
      '    :SVER,'
      '    :TIME_VER,'
      '    :CASH_BIN,'
      '    :UPD_TYPE,'
      '    :ILOADER,'
      '    :LOADER'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    SVER,'
      '    TIME_VER,'
      '    CASH_BIN,'
      '    UPD_TYPE,'
      '    ILOADER,'
      '    LOADER'
      'FROM'
      '    CASH_VERS '
      ''
      ' WHERE '
      '        CASH_VERS.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    SVER,'
      '    TIME_VER,'
      '    CASH_BIN,'
      '    UPD_TYPE,'
      '    ILOADER,'
      '    LOADER'
      'FROM'
      '    CASH_VERS '
      'order by ID')
    Transaction = trSelect
    Database = CashDBUpdater
    UpdateTransaction = trUpdate
    AutoCommit = True
    Left = 136
    Top = 484
    poAskRecordCount = True
    object quVersID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quVersSVER: TFIBStringField
      FieldName = 'SVER'
      Size = 10
      EmptyStrToNull = True
    end
    object quVersTIME_VER: TFIBDateTimeField
      FieldName = 'TIME_VER'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object quVersCASH_BIN: TFIBBlobField
      FieldName = 'CASH_BIN'
      Size = 8
    end
    object quVersUPD_TYPE: TFIBSmallIntField
      FieldName = 'UPD_TYPE'
    end
    object quVersILOADER: TFIBSmallIntField
      FieldName = 'ILOADER'
    end
    object quVersLOADER: TFIBBlobField
      FieldName = 'LOADER'
      Size = 8
    end
  end
  object dsquVers: TDataSource
    DataSet = quVers
    Left = 136
    Top = 540
  end
  object PopupMenu1: TPopupMenu
    Left = 200
    Top = 176
    object N1: TMenuItem
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1074#1077#1088#1089#1080#1102
      OnClick = N1Click
    end
    object N2: TMenuItem
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1074#1077#1088#1089#1080#1102
      OnClick = N2Click
    end
  end
  object PopupMenu2: TPopupMenu
    Left = 276
    Top = 472
    object N3: TMenuItem
      Caption = #1047#1072#1075#1088#1091#1079#1080#1090#1100' '#1085#1072' '#1074#1099#1076#1077#1083#1077#1085#1085#1099#1077' '#1092#1080#1083#1080#1072#1083#1099
      OnClick = N3Click
    end
  end
  object PosFb: TpFIBDatabase
    DBName = '192.168.1.150:D:\kassa\PosFb\cash.gdb'
    DBParams.Strings = (
      'user_name=SYSDBA'
      'lc_ctype=WIN1251'
      'password=masterkey'
      'sql_role_name=SYSDBA')
    DefaultTransaction = trSel
    DefaultUpdateTransaction = trUpd
    SQLDialect = 3
    Timeout = 0
    SynchronizeTime = False
    DesignDBOptions = []
    AliasName = 'PosFB'
    WaitForRestoreConnect = 20
    Left = 384
    Top = 444
  end
  object trSel: TpFIBTransaction
    DefaultDatabase = PosFb
    TimeoutAction = TARollback
    Left = 384
    Top = 504
  end
  object trUpd: TpFIBTransaction
    DefaultDatabase = PosFb
    TimeoutAction = TARollback
    Left = 384
    Top = 560
  end
  object quUpd: TpFIBQuery
    Transaction = trUpd1
    Database = PosFb
    SQL.Strings = (
      '')
    Left = 436
    Top = 448
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object trUpd1: TpFIBTransaction
    DefaultDatabase = PosFb
    TimeoutAction = TARollback
    Left = 496
    Top = 448
  end
  object quTabDBList: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select R.RDB$RELATION_NAME, R.RDB$FIELD_POSITION, R.RDB$FIELD_NA' +
        'ME'
      'from RDB$RELATION_FIELDS R'
      'where R.RDB$SYSTEM_FLAG = 0'
      'and R.RDB$RELATION_NAME = :STABNAME')
    Transaction = trSel
    Database = PosFb
    UpdateTransaction = trUpd
    AutoCommit = True
    Left = 440
    Top = 508
    poAskRecordCount = True
    object quTabDBListRDBRELATION_NAME: TFIBWideStringField
      FieldName = 'RDB$RELATION_NAME'
      Size = 31
    end
    object quTabDBListRDBFIELD_POSITION: TFIBSmallIntField
      FieldName = 'RDB$FIELD_POSITION'
    end
    object quTabDBListRDBFIELD_NAME: TFIBWideStringField
      FieldName = 'RDB$FIELD_NAME'
      Size = 31
    end
  end
  object quVersPFB: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE CASH_VERS'
      'SET '
      '    SVER = :SVER,'
      '    TIME_VER = :TIME_VER,'
      '    CASH_BIN = :CASH_BIN,'
      '    UPD_TYPE = :UPD_TYPE,'
      '    ILOADER = :ILOADER,'
      '    LOADER = :LOADER'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    CASH_VERS'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO CASH_VERS('
      '    ID,'
      '    SVER,'
      '    TIME_VER,'
      '    CASH_BIN,'
      '    UPD_TYPE,'
      '    ILOADER,'
      '    LOADER'
      ')'
      'VALUES('
      '    :ID,'
      '    :SVER,'
      '    :TIME_VER,'
      '    :CASH_BIN,'
      '    :UPD_TYPE,'
      '    :ILOADER,'
      '    :LOADER'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    SVER,'
      '    TIME_VER,'
      '    CASH_BIN,'
      '    UPD_TYPE,'
      '    ILOADER,'
      '    LOADER'
      'FROM'
      '    CASH_VERS '
      ''
      ' WHERE '
      '        CASH_VERS.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    SVER,'
      '    TIME_VER,'
      '    CASH_BIN,'
      '    UPD_TYPE,'
      '    ILOADER,'
      '    LOADER'
      'FROM'
      '    CASH_VERS ')
    Transaction = trSel
    Database = PosFb
    UpdateTransaction = trUpd
    AutoCommit = True
    Left = 532
    Top = 508
    poAskRecordCount = True
    object quVersPFBID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quVersPFBSVER: TFIBStringField
      FieldName = 'SVER'
      Size = 10
      EmptyStrToNull = True
    end
    object quVersPFBTIME_VER: TFIBDateTimeField
      FieldName = 'TIME_VER'
    end
    object quVersPFBCASH_BIN: TFIBBlobField
      FieldName = 'CASH_BIN'
      Size = 8
    end
    object quVersPFBUPD_TYPE: TFIBSmallIntField
      FieldName = 'UPD_TYPE'
    end
    object quVersPFBILOADER: TFIBSmallIntField
      FieldName = 'ILOADER'
    end
    object quVersPFBLOADER: TFIBBlobField
      FieldName = 'LOADER'
      Size = 8
    end
  end
  object quCashList: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE CASHERS'
      'SET '
      '    SPATH = :SPATH,'
      '    IRES = :IRES,'
      '    IST = :IST'
      'WHERE'
      '    ISHOP = :OLD_ISHOP'
      '    and INUM = :OLD_INUM'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    CASHERS'
      'WHERE'
      '        ISHOP = :OLD_ISHOP'
      '    and INUM = :OLD_INUM'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO CASHERS('
      '    ISHOP,'
      '    INUM,'
      '    SPATH,'
      '    IRES,'
      '    IST'
      ')'
      'VALUES('
      '    :ISHOP,'
      '    :INUM,'
      '    :SPATH,'
      '    :IRES,'
      '    :IST'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ISHOP,'
      '    INUM,'
      '    SPATH,'
      '    IRES,'
      '    IST'
      'FROM'
      '    CASHERS '
      ''
      ' WHERE '
      '        CASHERS.ISHOP = :OLD_ISHOP'
      '    and CASHERS.INUM = :OLD_INUM'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    ISHOP,'
      '    INUM,'
      '    SPATH,'
      '    IRES,'
      '    IST'
      'FROM'
      '    CASHERS '
      'order by ISHOP,INUM  ')
    Transaction = trSelect
    Database = CashDBUpdater
    UpdateTransaction = trUpdate
    AutoCommit = True
    Left = 672
    Top = 440
    poAskRecordCount = True
    object quCashListISHOP: TFIBIntegerField
      FieldName = 'ISHOP'
    end
    object quCashListINUM: TFIBIntegerField
      FieldName = 'INUM'
    end
    object quCashListSPATH: TFIBStringField
      FieldName = 'SPATH'
      Size = 150
      EmptyStrToNull = True
    end
    object quCashListIRES: TFIBSmallIntField
      FieldName = 'IRES'
    end
    object quCashListIST: TFIBIntegerField
      FieldName = 'IST'
    end
  end
  object dsquCashList: TDataSource
    DataSet = quCashList
    Left = 676
    Top = 500
  end
end
