object fmCash: TfmCash
  Left = 838
  Top = 314
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = #1056#1072#1089#1095#1077#1090' '#1085#1072#1083#1080#1095#1085#1099#1081
  ClientHeight = 416
  ClientWidth = 641
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clBlack
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 32
    Top = 46
    Width = 98
    Height = 33
    Caption = #1057#1091#1084#1084#1072' '
    Color = clWhite
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clBlack
    Font.Height = -29
    Font.Name = 'Arial'
    Font.Style = [fsUnderline]
    ParentColor = False
    ParentFont = False
    Transparent = True
  end
  object Label2: TLabel
    Left = 32
    Top = 133
    Width = 129
    Height = 33
    Caption = #1055#1086#1083#1091#1095#1077#1085#1086
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clBlack
    Font.Height = -29
    Font.Name = 'Arial'
    Font.Style = [fsUnderline]
    ParentFont = False
    Transparent = True
  end
  object Label3: TLabel
    Left = 32
    Top = 181
    Width = 84
    Height = 33
    Caption = #1057#1076#1072#1095#1072
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clRed
    Font.Height = -29
    Font.Name = 'Arial'
    Font.Style = [fsUnderline]
    ParentFont = False
    Transparent = True
  end
  object Edit1: TEdit
    Left = 256
    Top = 48
    Width = 65
    Height = 21
    TabOrder = 7
    Text = 'Edit1'
  end
  object Panel1: TPanel
    Left = 0
    Top = 387
    Width = 641
    Height = 29
    Align = alBottom
    BevelOuter = bvNone
    Color = 8421440
    TabOrder = 0
    object Label4: TLabel
      Left = 16
      Top = 8
      Width = 13
      Height = 13
      Caption = '...'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object cxCurrencyEdit1: TcxCurrencyEdit
    Left = 216
    Top = 40
    TabStop = False
    EditValue = 0c
    ParentFont = False
    Properties.Alignment.Horz = taRightJustify
    Properties.NullString = '0.00'
    Properties.ReadOnly = True
    Properties.UseLeftAlignmentOnEditing = False
    Properties.UseThousandSeparator = True
    Style.Font.Charset = RUSSIAN_CHARSET
    Style.Font.Color = clBlack
    Style.Font.Height = -29
    Style.Font.Name = 'Arial'
    Style.Font.Style = []
    Style.Shadow = True
    Style.IsFontAssigned = True
    TabOrder = 1
    OnKeyPress = cxCurrencyEdit1KeyPress
    BiDiMode = bdLeftToRight
    ParentBiDiMode = False
    Width = 234
  end
  object cxCurrencyEdit2: TcxCurrencyEdit
    Left = 216
    Top = 128
    BeepOnEnter = False
    EditValue = 0c
    ParentFont = False
    Properties.Alignment.Horz = taRightJustify
    Properties.HideSelection = False
    Properties.UseLeftAlignmentOnEditing = False
    Properties.UseThousandSeparator = True
    Style.Font.Charset = RUSSIAN_CHARSET
    Style.Font.Color = clBlack
    Style.Font.Height = -29
    Style.Font.Name = 'Arial'
    Style.Font.Style = []
    Style.Shadow = True
    Style.IsFontAssigned = True
    TabOrder = 2
    OnKeyPress = cxCurrencyEdit2KeyPress
    Width = 233
  end
  object cxCurrencyEdit3: TcxCurrencyEdit
    Left = 216
    Top = 176
    TabStop = False
    EditValue = 0c
    ParentFont = False
    Properties.Alignment.Horz = taRightJustify
    Properties.ReadOnly = True
    Properties.UseThousandSeparator = True
    Style.BorderStyle = ebsUltraFlat
    Style.Font.Charset = RUSSIAN_CHARSET
    Style.Font.Color = clRed
    Style.Font.Height = -29
    Style.Font.Name = 'Arial'
    Style.Font.Style = []
    Style.Shadow = True
    Style.IsFontAssigned = True
    TabOrder = 3
    Width = 233
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 641
    Height = 33
    Align = alTop
    BevelOuter = bvNone
    Color = 8421440
    TabOrder = 4
    object Label5: TLabel
      Left = 16
      Top = 8
      Width = 13
      Height = 13
      Caption = '...'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 33
    Width = 15
    Height = 354
    Align = alLeft
    BevelOuter = bvNone
    Color = 8421440
    TabOrder = 5
  end
  object Panel4: TPanel
    Left = 626
    Top = 33
    Width = 15
    Height = 354
    Align = alRight
    BevelOuter = bvNone
    Color = 8421440
    TabOrder = 6
  end
  object GridPay: TcxGrid
    Left = 20
    Top = 232
    Width = 433
    Height = 85
    TabOrder = 8
    Visible = False
    LookAndFeel.Kind = lfFlat
    object PayView: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dsPay
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnGrouping = False
      OptionsCustomize.ColumnSorting = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.CellSelect = False
      OptionsSelection.InvertSelect = False
      OptionsSelection.UnselectFocusedRecordOnExit = False
      OptionsView.GroupByBox = False
      Styles.Background = dmC.cxStyle6
      Styles.Header = dmC.cxStyle1
      object PayViewId: TcxGridDBColumn
        Caption = #8470' '#1087'.'#1087
        DataBinding.FieldName = 'Id'
        Styles.Content = dmC.cxStyle2
        Width = 56
      end
      object PayViewTypePl: TcxGridDBColumn
        Caption = #1058#1080#1087
        DataBinding.FieldName = 'TypePl'
        Visible = False
      end
      object PayViewNamePl: TcxGridDBColumn
        Caption = #1042#1080#1076' '#1087#1083#1072#1090#1077#1078#1072
        DataBinding.FieldName = 'NamePl'
        Styles.Content = dmC.cxStyle5
        Width = 173
      end
      object PayViewSumPl: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072
        DataBinding.FieldName = 'SumPl'
        Styles.Content = dmC.cxStyle2
        Width = 90
      end
      object PayViewSumOst: TcxGridDBColumn
        Caption = #1082' '#1076#1086#1087#1083#1072#1090#1077
        DataBinding.FieldName = 'SumOst'
        Styles.Content = dmC.cxStyle10
        Width = 81
      end
    end
    object PayLevel: TcxGridLevel
      GridView = PayView
    end
  end
  object Panel5: TPanel
    Left = 460
    Top = 44
    Width = 161
    Height = 329
    BevelInner = bvLowered
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 9
    object Label9: TLabel
      Left = 8
      Top = 8
      Width = 76
      Height = 16
      Alignment = taCenter
      Caption = #1053#1072#1083#1080#1095#1085#1099#1077
      Color = clBlack
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Transparent = True
    end
    object cxButton2: TcxButton
      Left = 12
      Top = 152
      Width = 41
      Height = 41
      Caption = '1'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 0
      TabStop = False
      OnClick = cxButton2Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfUltraFlat
    end
    object cxButton3: TcxButton
      Left = 60
      Top = 152
      Width = 41
      Height = 41
      Caption = '2'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 1
      TabStop = False
      OnClick = cxButton3Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfUltraFlat
    end
    object cxButton4: TcxButton
      Left = 108
      Top = 152
      Width = 41
      Height = 41
      Caption = '3'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 2
      TabStop = False
      OnClick = cxButton4Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfUltraFlat
    end
    object cxButton5: TcxButton
      Left = 12
      Top = 104
      Width = 41
      Height = 41
      Caption = '4'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 3
      TabStop = False
      OnClick = cxButton5Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfUltraFlat
    end
    object cxButton6: TcxButton
      Left = 60
      Top = 104
      Width = 41
      Height = 41
      Caption = '5'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 4
      TabStop = False
      OnClick = cxButton6Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfUltraFlat
    end
    object cxButton7: TcxButton
      Left = 108
      Top = 104
      Width = 41
      Height = 41
      Caption = '6'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 5
      TabStop = False
      OnClick = cxButton7Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfUltraFlat
    end
    object cxButton8: TcxButton
      Left = 12
      Top = 56
      Width = 41
      Height = 41
      Caption = '7'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 6
      TabStop = False
      OnClick = cxButton8Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfUltraFlat
    end
    object cxButton9: TcxButton
      Left = 60
      Top = 56
      Width = 41
      Height = 41
      Caption = '8'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 7
      TabStop = False
      OnClick = cxButton9Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfUltraFlat
    end
    object cxButton10: TcxButton
      Left = 108
      Top = 56
      Width = 41
      Height = 41
      Caption = '9'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 8
      TabStop = False
      OnClick = cxButton10Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfUltraFlat
    end
    object cxButton11: TcxButton
      Left = 12
      Top = 200
      Width = 41
      Height = 41
      Caption = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 9
      TabStop = False
      OnClick = cxButton11Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfUltraFlat
    end
    object cxButton12: TcxButton
      Left = 60
      Top = 200
      Width = 41
      Height = 41
      Caption = ','
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 10
      TabStop = False
      OnClick = cxButton12Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfUltraFlat
    end
    object cxButton13: TcxButton
      Left = 12
      Top = 288
      Width = 89
      Height = 33
      Caption = 'C'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 11
      TabStop = False
      OnClick = cxButton13Click
      Colors.Default = 8454143
      Colors.Normal = 8454143
      Colors.Hot = 11796479
      Colors.Pressed = 59110
      LookAndFeel.Kind = lfUltraFlat
    end
    object cxButton14: TcxButton
      Left = 12
      Top = 248
      Width = 89
      Height = 33
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 12
      TabStop = False
      OnClick = cxButton14Click
      Colors.Default = 8454143
      Colors.Normal = 8454143
      Colors.Hot = 11796479
      Colors.Pressed = 59110
      Glyph.Data = {
        12040000424D12040000000000003600000028000000190000000D0000000100
        180000000000DC030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000
        0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFF00
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000000000000000000000000000000000000FFFFFFFFFF
        FF00FFFFFF000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000FFFFFFFFFFFF00FFFFFFFFFFFFFFFFFF000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000000000000000000000FFFFFFFFFFFF00FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFF000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00}
      LookAndFeel.Kind = lfUltraFlat
    end
    object cxButton15: TcxButton
      Left = 108
      Top = 200
      Width = 45
      Height = 121
      Caption = 'Ok'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 13
      TabStop = False
      OnClick = cxButton15Click
      Colors.Default = 16750591
      Colors.Normal = 16750591
      Colors.Hot = 16763647
      Colors.Pressed = 16718591
      LookAndFeel.Kind = lfUltraFlat
    end
    object CalcEdit1: TcxCalcEdit
      Left = 88
      Top = 8
      TabStop = False
      EditValue = 1.000000000000000000
      ParentFont = False
      Properties.Alignment.Horz = taRightJustify
      Properties.ReadOnly = True
      Properties.QuickClose = True
      Style.BorderStyle = ebsUltraFlat
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = 8552960
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsBold]
      Style.LookAndFeel.Kind = lfUltraFlat
      Style.Shadow = True
      Style.ButtonTransparency = ebtHideInactive
      Style.IsFontAssigned = True
      StyleDisabled.LookAndFeel.Kind = lfUltraFlat
      StyleFocused.LookAndFeel.Kind = lfUltraFlat
      StyleHot.LookAndFeel.Kind = lfUltraFlat
      TabOrder = 14
      Width = 65
    end
  end
  object cxButton1: TcxButton
    Left = 324
    Top = 325
    Width = 125
    Height = 56
    Action = acCalcN
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 10
    TabStop = False
    Colors.Default = 14864576
    Colors.Normal = 12621940
    Colors.Pressed = 10843723
    Layout = blGlyphTop
    LookAndFeel.Kind = lfFlat
  end
  object cxButton16: TcxButton
    Left = 188
    Top = 325
    Width = 125
    Height = 56
    Action = acCalcBN
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 11
    TabStop = False
    Colors.Default = 14864576
    Colors.Normal = 12621940
    Colors.Pressed = 10843723
    Layout = blGlyphTop
    LookAndFeel.Kind = lfFlat
  end
  object cxButton17: TcxButton
    Left = 20
    Top = 325
    Width = 85
    Height = 56
    Action = acExit
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 12
    TabStop = False
    Colors.Default = 14864576
    Colors.Normal = 12621940
    Colors.Pressed = 10843723
    Layout = blGlyphTop
    LookAndFeel.Kind = lfFlat
  end
  object cxLabel1: TcxLabel
    Left = 40
    Top = 88
    Caption = #1057#1091#1084#1084#1072' '#1087#1088#1080' '#1085#1072#1083#1080#1095#1085#1086#1081' '#1086#1087#1083#1072#1090#1077
    ParentFont = False
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clBlack
    Style.Font.Height = -16
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = [fsBold]
    Style.IsFontAssigned = True
    Transparent = True
  end
  object cxCurrencyEdit10: TcxCurrencyEdit
    Left = 296
    Top = 88
    TabStop = False
    EditValue = 0c
    ParentFont = False
    Properties.Alignment.Horz = taRightJustify
    Properties.NullString = '0.00'
    Properties.ReadOnly = True
    Properties.UseLeftAlignmentOnEditing = False
    Properties.UseThousandSeparator = True
    Style.Font.Charset = RUSSIAN_CHARSET
    Style.Font.Color = clBlack
    Style.Font.Height = -21
    Style.Font.Name = 'Arial Narrow'
    Style.Font.Style = [fsBold]
    Style.Shadow = True
    Style.IsFontAssigned = True
    TabOrder = 14
    OnKeyPress = cxCurrencyEdit1KeyPress
    BiDiMode = bdLeftToRight
    ParentBiDiMode = False
    Width = 154
  end
  object Timer1: TTimer
    Interval = 3000
    OnTimer = Timer1Timer
    Left = 168
    Top = 40
  end
  object am2: TActionManager
    Left = 167
    Top = 160
    StyleName = 'XP Style'
    object acExit: TAction
      Caption = #1054#1090#1084#1077#1085#1072
      ShortCut = 121
      SecondaryShortCuts.Strings = (
        'Esc')
      OnExecute = acExitExecute
    end
    object acCalcN: TAction
      Caption = #1056#1072#1089#1095#1077#1090' '#1085#1072#1083#1080#1095#1085#1099#1081
      ShortCut = 116
      OnExecute = acCalcNExecute
    end
    object acBarC: TAction
      Caption = 'acBarC'
      ShortCut = 123
      OnExecute = acBarCExecute
    end
    object acCalcBN: TAction
      Caption = #1056#1072#1089#1095#1077#1090' '#1073#1077#1079#1085#1072#1083'.'
      ShortCut = 16500
      OnExecute = acCalcBNExecute
    end
  end
  object taPay: TClientDataSet
    Aggregates = <>
    FileName = 'Pay.cds'
    Params = <>
    Left = 120
    Top = 264
    object taPayId: TSmallintField
      FieldName = 'Id'
    end
    object taPayTypePl: TSmallintField
      FieldName = 'TypePl'
    end
    object taPayNamePl: TStringField
      FieldName = 'NamePl'
      Size = 30
    end
    object taPaySumPl: TCurrencyField
      FieldName = 'SumPl'
    end
    object taPaySumOst: TCurrencyField
      FieldName = 'SumOst'
    end
    object taPayVidPl: TSmallintField
      FieldName = 'VidPl'
    end
    object taPayComment: TStringField
      DisplayWidth = 500
      FieldName = 'Comment'
      Size = 500
    end
  end
  object dsPay: TDataSource
    DataSet = taPay
    Left = 176
    Top = 264
  end
end
