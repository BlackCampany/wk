object fmEnterPassw: TfmEnterPassw
  Left = 416
  Top = 207
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = #1042#1074#1077#1076#1080#1090#1077' '#1087#1072#1088#1086#1083#1100
  ClientHeight = 165
  ClientWidth = 316
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 8
    Width = 289
    Height = 49
    Alignment = taCenter
    AutoSize = False
    Caption = #1042#1074#1077#1076#1080#1090#1077' '#1087#1072#1088#1086#1083#1100
    Color = clWindow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    Transparent = True
    WordWrap = True
  end
  object cxTextEdit1: TcxTextEdit
    Left = 56
    Top = 64
    Properties.EchoMode = eemPassword
    Properties.PasswordChar = '*'
    TabOrder = 0
    Text = 'cxTextEdit1'
    Width = 201
  end
  object Panel1: TPanel
    Left = 0
    Top = 112
    Width = 316
    Height = 53
    Align = alBottom
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 1
    object cxButton1: TcxButton
      Left = 72
      Top = 16
      Width = 75
      Height = 25
      Caption = #1054#1082
      Default = True
      TabOrder = 0
      OnClick = cxButton1Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 192
      Top = 16
      Width = 75
      Height = 25
      Caption = #1054#1090#1084#1077#1085#1072
      TabOrder = 1
      OnClick = cxButton2Click
      LookAndFeel.Kind = lfOffice11
    end
  end
  object ProgressBar1: TProgressBar
    Left = 24
    Top = 96
    Width = 265
    Height = 9
    Position = 20
    TabOrder = 2
    Visible = False
  end
end
