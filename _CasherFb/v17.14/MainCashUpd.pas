unit MainCashUpd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, FIBDatabase, pFIBDatabase, cxStyles, cxCustomData, cxGraphics,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, FIBDataSet, pFIBDataSet,
  cxContainer, cxTextEdit, cxMemo, Menus, FIBQuery, pFIBQuery, ExtCtrls,
  cxLookAndFeelPainters, StdCtrls, cxButtons, dxmdaset;

type
  TfmMainCashUpd = class(TForm)
    CashDBUpdater: TpFIBDatabase;
    trSelect: TpFIBTransaction;
    trUpdate: TpFIBTransaction;
    trDel: TpFIBTransaction;
    ViPath: TcxGridDBTableView;
    LePath: TcxGridLevel;
    GrPath: TcxGrid;
    quPathFils: TpFIBDataSet;
    quPathFilsIDSHOP: TFIBIntegerField;
    quPathFilsNameShop: TFIBStringField;
    quPathFilsSPATH: TFIBStringField;
    dsquPathFils: TDataSource;
    ViPathIDSHOP: TcxGridDBColumn;
    ViPathNameShop: TcxGridDBColumn;
    ViPathSPATH: TcxGridDBColumn;
    ViPathLASTUPD: TcxGridDBColumn;
    quPathFilsLASTUPD: TFIBDateTimeField;
    Memo1: TcxMemo;
    quPathFilsLASTSTATUS: TFIBStringField;
    ViPathLASTSTATUS: TcxGridDBColumn;
    quVers: TpFIBDataSet;
    dsquVers: TDataSource;
    quVersID: TFIBIntegerField;
    quVersSVER: TFIBStringField;
    quVersTIME_VER: TFIBDateTimeField;
    quVersCASH_BIN: TFIBBlobField;
    quVersUPD_TYPE: TFIBSmallIntField;
    quVersILOADER: TFIBSmallIntField;
    quVersLOADER: TFIBBlobField;
    ViVers: TcxGridDBTableView;
    LeVers: TcxGridLevel;
    GrVers: TcxGrid;
    ViVersID: TcxGridDBColumn;
    ViVersSVER: TcxGridDBColumn;
    ViVersTIME_VER: TcxGridDBColumn;
    ViVersCASH_BIN: TcxGridDBColumn;
    ViVersUPD_TYPE: TcxGridDBColumn;
    ViVersILOADER: TcxGridDBColumn;
    ViVersLOADER: TcxGridDBColumn;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    PopupMenu2: TPopupMenu;
    N3: TMenuItem;
    PosFb: TpFIBDatabase;
    trSel: TpFIBTransaction;
    trUpd: TpFIBTransaction;
    quUpd: TpFIBQuery;
    trUpd1: TpFIBTransaction;
    quTabDBList: TpFIBDataSet;
    quTabDBListRDBRELATION_NAME: TFIBWideStringField;
    quTabDBListRDBFIELD_POSITION: TFIBSmallIntField;
    quTabDBListRDBFIELD_NAME: TFIBWideStringField;
    quVersPFB: TpFIBDataSet;
    quVersPFBID: TFIBIntegerField;
    quVersPFBSVER: TFIBStringField;
    quVersPFBTIME_VER: TFIBDateTimeField;
    quVersPFBCASH_BIN: TFIBBlobField;
    quVersPFBUPD_TYPE: TFIBSmallIntField;
    quVersPFBILOADER: TFIBSmallIntField;
    quVersPFBLOADER: TFIBBlobField;
    Panel1: TPanel;
    cxButton1: TcxButton;
    quCashList: TpFIBDataSet;
    quCashListISHOP: TFIBIntegerField;
    quCashListINUM: TFIBIntegerField;
    quCashListSPATH: TFIBStringField;
    quCashListIRES: TFIBSmallIntField;
    dsquCashList: TDataSource;
    quCashListIST: TFIBIntegerField;
    procedure FormShow(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

procedure Delay(MSecs: Longint);
procedure prUpd1;

var
  fmMainCashUpd: TfmMainCashUpd;

implementation

uses AddVer, CashList;

{$R *.dfm}

procedure prUpd1;
begin
  with fmMainCashUpd do
  begin
    quTabDBList.Active:=False;
    quTabDBList.ParamByName('STABNAME').AsString:='CASH_VERS';
    quTabDBList.Active:=True;
    if quTabDBList.RecordCount=0 then
    begin //����� ��������� �������� CASH_VERS
      quUpd.SQL.Clear;
      quUpd.SQL.Add('CREATE TABLE CASH_VERS (');
      quUpd.SQL.Add('ID INTEGER NOT NULL,');
      quUpd.SQL.Add('SVER VARCHAR (10) character set WIN1251 collate WIN1251,');
      quUpd.SQL.Add('TIME_VER TIMESTAMP,');
      quUpd.SQL.Add('CASH_BIN BLOB sub_type 2 segment size 1,');
      quUpd.SQL.Add('UPD_TYPE SMALLINT,');
      quUpd.SQL.Add('ILOADER SMALLINT,');
      quUpd.SQL.Add('LOADER BLOB sub_type 2 segment size 1);');
      quUpd.ExecQuery;
      delay(100);

      quUpd.SQL.Clear;
      quUpd.SQL.Add('ALTER TABLE CASH_VERS ADD CONSTRAINT PK_CASH_VERS PRIMARY KEY (ID);');
      quUpd.ExecQuery;

      delay(100);
    end;
    quTabDBList.Active:=False;
  end;
end;

procedure Delay(MSecs: Longint);
var
  FirstTickCount, Now: Longint;
begin
  FirstTickCount := GetTickCount;
  repeat
    Application.ProcessMessages;
    { allowing access to other controls, etc. }
    Now := GetTickCount;
  until (Now - FirstTickCount >= MSecs) or (Now < FirstTickCount);
end;


procedure TfmMainCashUpd.FormShow(Sender: TObject);
begin
  try
    Memo1.Clear;
    Memo1.Lines.Add('������� �� '); delay(10);
    CashDBUpdater.Close;
    CashDBUpdater.DatabaseName:='192.168.1.150:D:\_CasherFb\DBUPD\CASHUPD.gdb';
    CashDBUpdater.Open;
    Memo1.Lines.Add('�� ��'); delay(1000);
    if CashDBUpdater.Connected then
    begin
      Memo1.Lines.Add('');
      Memo1.Lines.Add('��������'); delay(10);
      quPathFils.Active:=True;
      Memo1.Lines.Add('�������� ��'); delay(10);

      Memo1.Lines.Add('');
      Memo1.Lines.Add('������'); delay(10);
      quVers.Active:=True;
      Memo1.Lines.Add('������ Ok'); delay(10);
    end;
  except
    Memo1.Lines.Add('������.'); delay(10);
  end;
end;

procedure TfmMainCashUpd.N1Click(Sender: TObject);
Var iMax:Integer;
    sPath:String;
begin
  //�������� ������ � ����
  fmAddVer.cxTextEdit1.Text:=formatdatetime('yy',date)+'.00';
  fmAddVer.cxButtonEdit1.Text:='';
  fmAddVer.cxButtonEdit2.Text:='';
  fmAddVer.cxComboBox1.ItemIndex:=0;
  fmAddVer.cxComboBox2.ItemIndex:=1;
  fmAddVer.ShowModal;
  if fmAddVer.ModalResult=mrOk then
  begin
    Delay(10);
    Memo1.Clear;
    Memo1.Lines.Add('����� ... ��������� � ���� ����� ������ - '+fmAddVer.cxTextEdit1.Text);

    iMax:=1;
    quVers.Last;
    if quVers.RecordCount>0 then iMax:=quVersID.AsInteger+1;

    quVers.Append;
    quVersID.AsInteger:=iMax;
    quVersSVER.AsString:=fmAddVer.cxTextEdit1.Text;
    quVersTIME_VER.AsDateTime:=now;
    quVersUPD_TYPE.AsInteger:=fmAddVer.cxComboBox1.ItemIndex;
    quVersILOADER.AsInteger:=fmAddVer.cxComboBox2.ItemIndex;
    sPath:=fmAddVer.cxButtonEdit1.Text;
    if FileExists(sPath) then quVersCASH_BIN.LoadFromFile(sPath);
    sPath:=fmAddVer.cxButtonEdit2.Text;
    if FileExists(sPath) then quVersLOADER.LoadFromFile(sPath);
    quVers.Post;

    Memo1.Lines.Add('���������� ��.');
  end;
end;

procedure TfmMainCashUpd.N2Click(Sender: TObject);
begin
  if quVers.RecordCount>0 then
  begin
    if MessageDlg('������� ������ '+quVersSVER.AsString +' �� ���� ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      quVers.Delete;
    end;
  end;
end;

procedure TfmMainCashUpd.N3Click(Sender: TObject);
Var
    i,j,iNum:INteger;
    Rec:TcxCustomGridRecord;
    stTrF,stTrF1:TMemoryStream;
begin
  //��������� �� ���������� �������
  if ViPath.Controller.SelectedRecordCount=0 then exit;

  Memo1.Clear;
  Memo1.Lines.Add('����� � �������� - '+IntToStr(ViPath.Controller.SelectedRecordCount)+' ��������.');

  for i:=0 to ViPath.Controller.SelectedRecordCount-1 do
  begin
    Rec:=ViPath.Controller.SelectedRecords[i];

    iNum:=0;

    for j:=0 to Rec.ValueCount-1 do
    begin
      if ViPath.Columns[j].Name='ViPathIDSHOP' then begin iNum:=Rec.Values[j]; Break; end;
    end;

    if (iNum>0) then
    begin
      if quPathFils.Locate('IDSHOP',iNum,[]) then
      begin
        try
          Memo1.Lines.Add(''); Delay(10);
          Memo1.Lines.Add('�������� ���������� �� ������� - '+IntToStr(iNum)+' '+quPathFilsNameShop.AsString);
          Memo1.Lines.Add('  ����� ...');  Delay(10);

          if quPathFilsSPATH.AsString>'' then
          begin
            PosFb.Close;
            try
              Memo1.Lines.Add('  ���������� � �����.');  Delay(10);
              PosFb.DatabaseName:=quPathFilsSPATH.AsString;
              PosFb.Open;
              Memo1.Lines.Add('  ���������� ��.');  Delay(10);

              prUpd1; //���������� ���� �� ������ ������

              try
                Memo1.Lines.Add('  ����� ... ���� �������� ������.');  Delay(10);
                quVersPFB.Active:=False;
                quVersPFB.Active:=True;

                quVersPFB.First;
                while not quVersPFB.Eof do
                begin
                  if quVersPFBID.AsInteger>=quVersID.AsInteger then quVersPFB.Delete
                  else quVersPFB.Next;
                end;

                quVersPFB.Append;
                quVersPFBID.AsInteger:=quVersID.AsInteger;
                quVersPFBSVER.AsString:=quVersSVER.AsString;
                quVersPFBTIME_VER.AsDateTime:=Now;

                stTrF:=TMemoryStream.Create;
                quVersCASH_BIN.SaveToStream(stTrF);
                stTrF.Position := 0;
                quVersPFBCASH_BIN.LoadFromStream(stTrF);
                stTrF.Free;
//                quVersPFBCASH_BIN.asBlob:=quVersCASH_BIN.asBlob;

                quVersPFBUPD_TYPE.AsInteger:=quVersUPD_TYPE.AsInteger;
                quVersPFBILOADER.AsInteger:=quVersILOADER.AsInteger;

                stTrF1:=TMemoryStream.Create;
                quVersLOADER.SaveToStream(stTrF1);
                stTrF1.Position := 0;
                quVersPFBLOADER.LoadFromStream(stTrF1);
                stTrF1.Free;
//                quVersPFBLOADER.asBlob:=quVersLOADER.asBlob;

                quVersPFB.Post;

                quVersPFB.Active:=False;

                //���� ��� �� - �����������
                quPathFils.Edit;
                quPathFilsLASTUPD.AsDateTime:=now;
                quPathFilsLASTSTATUS.AsInteger:=1;
                quPathFils.Post;

              except
                Memo1.Lines.Add('  ������ �������� ������.');  Delay(10);
                quPathFils.Edit;
                quPathFilsLASTUPD.AsDateTime:=now;
                quPathFilsLASTSTATUS.AsInteger:=0;
                quPathFils.Post;
              end;

            except
              Memo1.Lines.Add('  ������ ����������.');  Delay(10);

              quPathFils.Edit;
              quPathFilsLASTUPD.AsDateTime:=now;
              quPathFilsLASTSTATUS.AsInteger:=0;
              quPathFils.Post;

            end;
          end else
          begin
            Memo1.Lines.Add('���� � ���� ������ �� ���������.');  Delay(10);

            quPathFils.Edit;
            quPathFilsLASTUPD.AsDateTime:=now;
            quPathFilsLASTSTATUS.AsInteger:=0;
            quPathFils.Post;
          end;

          Memo1.Lines.Add('�������� ��.');  Delay(10);
        except
          Memo1.Lines.Add('������ ��� ��������.');  Delay(10);

          quPathFils.Edit;
          quPathFilsLASTUPD.AsDateTime:=now;
          quPathFilsLASTSTATUS.AsInteger:=0;
          quPathFils.Post;

        end;
      end;
    end;
  end;
end;

procedure TfmMainCashUpd.cxButton1Click(Sender: TObject);
begin
  fmCashList.ViCash.BeginUpdate;
  quCashList.Active:=False;
  quCashList.Active:=True;
  fmCashList.ViCash.EndUpdate;

  fmCashList.Show;
end;

end.
