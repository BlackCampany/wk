object fmAChecks: TfmAChecks
  Left = 755
  Top = 595
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = #1040#1085#1085#1091#1083#1080#1088#1086#1074#1072#1085#1085#1099#1077' '#1095#1077#1082#1080
  ClientHeight = 429
  ClientWidth = 665
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 4
    Top = 32
    Width = 31
    Height = 13
    Caption = #1063#1077#1082#1080
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 4
    Top = 152
    Width = 119
    Height = 13
    Caption = #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1103' '#1095#1077#1082#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 4
    Top = 8
    Width = 39
    Height = 13
    Caption = #1057#1084#1077#1085#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Memo1: TcxMemo
    Left = 8
    Top = 380
    TabStop = False
    Lines.Strings = (
      'Memo1')
    Properties.ReadOnly = True
    TabOrder = 3
    OnKeyDown = cxSpinEdit1KeyDown
    Height = 45
    Width = 505
  end
  object Panel1: TPanel
    Left = 519
    Top = 0
    Width = 146
    Height = 429
    Align = alRight
    BevelInner = bvLowered
    Color = 12621940
    TabOrder = 4
    object cxButton1: TcxButton
      Left = 12
      Top = 12
      Width = 121
      Height = 53
      Caption = #1042#1086#1089#1089#1090#1072#1085#1086#1074#1080#1090#1100
      Default = True
      ModalResult = 1
      TabOrder = 0
      TabStop = False
      OnKeyDown = cxSpinEdit1KeyDown
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton3: TcxButton
      Left = 12
      Top = 352
      Width = 121
      Height = 57
      Caption = #1042#1099#1093#1086#1076
      ModalResult = 2
      TabOrder = 1
      TabStop = False
      OnClick = cxButton3Click
      OnKeyDown = cxSpinEdit1KeyDown
      LookAndFeel.Kind = lfOffice11
    end
  end
  object GridChSp: TcxGrid
    Left = 4
    Top = 168
    Width = 509
    Height = 209
    TabOrder = 2
    LookAndFeel.Kind = lfOffice11
    object ViewChSp: TcxGridDBTableView
      OnKeyDown = cxSpinEdit1KeyDown
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dsquADS
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'DSUM'
          Column = ViewChSpDSUM
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'TOTALRUB'
          Column = ViewChSpTOTALRUB
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      object ViewChSpID: TcxGridDBColumn
        Caption = #8470' '#1087#1087
        DataBinding.FieldName = 'ID'
        Width = 33
      end
      object ViewChSpARTICUL: TcxGridDBColumn
        Caption = #1040#1088#1090#1080#1082#1091#1083
        DataBinding.FieldName = 'ARTICUL'
        Width = 49
      end
      object ViewChSpNAME: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'NAME'
        Width = 145
      end
      object ViewChSpQUANTITY: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086
        DataBinding.FieldName = 'QUANTITY'
        Width = 54
      end
      object ViewChSpPRICERUB: TcxGridDBColumn
        Caption = #1062#1077#1085#1072
        DataBinding.FieldName = 'PRICERUB'
        Width = 60
      end
      object ViewChSpDPROC: TcxGridDBColumn
        Caption = '% '#1089#1082#1080#1076#1082#1080
        DataBinding.FieldName = 'DPROC'
        Width = 40
      end
      object ViewChSpDSUM: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1089#1082#1080#1076#1082#1080
        DataBinding.FieldName = 'DSUM'
        Width = 48
      end
      object ViewChSpTOTALRUB: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1080#1090#1086#1075#1086
        DataBinding.FieldName = 'TOTALRUB'
        Width = 56
      end
    end
    object LevelChSp: TcxGridLevel
      GridView = ViewChSp
    end
  end
  object GridChHd: TcxGrid
    Left = 60
    Top = 36
    Width = 453
    Height = 113
    TabOrder = 1
    LookAndFeel.Kind = lfOffice11
    object ViewChHd: TcxGridDBTableView
      OnKeyDown = cxSpinEdit1KeyDown
      NavigatorButtons.ConfirmDelete = False
      OnFocusedRecordChanged = ViewChHdFocusedRecordChanged
      DataController.DataSource = dsquADH
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'DISCOUNTSUM'
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SUMMA'
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      object ViewChHdZNUMBER: TcxGridDBColumn
        Caption = #8470' '#1089#1084#1077#1085#1099
        DataBinding.FieldName = 'ZNUMBER'
        Width = 50
      end
      object ViewChHdCHECKNUMBER: TcxGridDBColumn
        Caption = #8470' '#1095#1077#1082#1072
        DataBinding.FieldName = 'CHECKNUMBER'
        Width = 48
      end
      object ViewChHdCASHER: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1082#1072#1089#1089#1080#1088#1072
        DataBinding.FieldName = 'CASHER'
        Width = 50
      end
      object ViewChHdCHDATE: TcxGridDBColumn
        Caption = #1042#1088#1077#1084#1103
        DataBinding.FieldName = 'CHDATE'
        PropertiesClassName = 'TcxTimeEditProperties'
        Width = 97
      end
      object ViewChHdPAYMENT: TcxGridDBColumn
        Caption = #1058#1080#1087' '#1086#1087#1083#1072#1090#1099
        DataBinding.FieldName = 'PAYMENT'
        Width = 102
      end
      object ViewChHdOPER: TcxGridDBColumn
        Caption = #1054#1087#1077#1088#1072#1094#1080#1103
        DataBinding.FieldName = 'OPER'
        Width = 84
      end
    end
    object LevelChHd: TcxGridLevel
      GridView = ViewChHd
    end
  end
  object cxSpinEdit1: TcxSpinEdit
    Left = 60
    Top = 8
    Properties.OnChange = cxSpinEdit1PropertiesChange
    Style.BorderStyle = ebsOffice11
    TabOrder = 0
    OnKeyDown = cxSpinEdit1KeyDown
    Width = 121
  end
  object trASel: TpFIBTransaction
    DefaultDatabase = dmC.CasherDb
    TimeoutAction = TARollback
    Left = 32
    Top = 180
  end
  object quADH: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT CASHNUMBER,'
      '       ZNUMBER,'
      '       CHECKNUMBER,'
      '       ID,'
      '       CASHER,'
      '       DBAR,'
      '       CHDATE,'
      '       PAYMENT,'
      '       OPER'
      'FROM ACHECK_HD'
      'where CASHNUMBER=:CASHNUM'
      'and ZNUMBER=:ZNUM'
      'order by CHDATE desc')
    Transaction = trASel
    Database = dmC.CasherDb
    Left = 112
    Top = 180
    poAskRecordCount = True
    object quADHCASHNUMBER: TFIBIntegerField
      FieldName = 'CASHNUMBER'
    end
    object quADHZNUMBER: TFIBIntegerField
      FieldName = 'ZNUMBER'
    end
    object quADHCHECKNUMBER: TFIBIntegerField
      FieldName = 'CHECKNUMBER'
    end
    object quADHID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quADHCASHER: TFIBIntegerField
      FieldName = 'CASHER'
    end
    object quADHDBAR: TFIBStringField
      FieldName = 'DBAR'
      Size = 22
      EmptyStrToNull = True
    end
    object quADHCHDATE: TFIBDateTimeField
      FieldName = 'CHDATE'
    end
    object quADHPAYMENT: TFIBSmallIntField
      FieldName = 'PAYMENT'
    end
    object quADHOPER: TFIBSmallIntField
      FieldName = 'OPER'
    end
  end
  object quADS: TpFIBDataSet
    SelectSQL.Strings = (
      
        'SELECT sp.IDH,sp.ID,sp.ARTICUL,sp.BAR,sp.QUANTITY,sp.PRICERUB,sp' +
        '.DPROC,'
      '       sp.DSUM,sp.TOTALRUB,cs.NAME,cs.MESURIMENT'
      'FROM ACHECK_SP sp'
      'left join CARDSCLA cs on cs.ARTICUL=sp.ARTICUL'
      'where IDH=:IDH'
      'order by sp.ID')
    Transaction = trASel
    Database = dmC.CasherDb
    Left = 188
    Top = 180
    poAskRecordCount = True
    object quADSIDH: TFIBIntegerField
      FieldName = 'IDH'
    end
    object quADSID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quADSARTICUL: TFIBStringField
      FieldName = 'ARTICUL'
      Size = 30
      EmptyStrToNull = True
    end
    object quADSBAR: TFIBStringField
      FieldName = 'BAR'
      Size = 15
      EmptyStrToNull = True
    end
    object quADSQUANTITY: TFIBFloatField
      FieldName = 'QUANTITY'
      DisplayFormat = '0.000'
    end
    object quADSPRICERUB: TFIBFloatField
      FieldName = 'PRICERUB'
      DisplayFormat = '0.00'
    end
    object quADSDPROC: TFIBFloatField
      FieldName = 'DPROC'
      DisplayFormat = '0.00'
    end
    object quADSDSUM: TFIBFloatField
      FieldName = 'DSUM'
      DisplayFormat = '0.00'
    end
    object quADSTOTALRUB: TFIBFloatField
      FieldName = 'TOTALRUB'
      DisplayFormat = '0.00'
    end
    object quADSNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 80
      EmptyStrToNull = True
    end
    object quADSMESURIMENT: TFIBStringField
      FieldName = 'MESURIMENT'
      Size = 10
      EmptyStrToNull = True
    end
  end
  object dsquADH: TDataSource
    DataSet = quADH
    Left = 112
    Top = 232
  end
  object dsquADS: TDataSource
    DataSet = quADS
    Left = 188
    Top = 232
  end
  object anAnnCheck: TActionManager
    Left = 376
    Top = 200
    StyleName = 'XP Style'
    object acExit: TAction
      Caption = 'acExit'
      ShortCut = 27
      SecondaryShortCuts.Strings = (
        'F10')
      OnExecute = acExitExecute
    end
  end
end
