unit uTermElisey;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, SvcMgr, Dialogs,
  ExtCtrls,IniFiles, winsock, IdBaseComponent, IdComponent,
  IdTCPConnection, IdTCPClient, IdFTP, Forms;

Const CurIni:String = 'ProfilesEl.ini';
      MAX_PACKET_SIZE = $10000; // 2^16
      SIO_RCVALL = $98000001;
      WSA_VER = $202;
      MAX_ADAPTER_NAME_LENGTH        = 256;
      MAX_ADAPTER_DESCRIPTION_LENGTH = 128;
      MAX_ADAPTER_ADDRESS_LENGTH    = 8;
      IPHelper = 'iphlpapi.dll';
      r:String = ';';
      NameFi:String = 'ip';

type
{    TCommonSet = record
     FtpExport:String;
     HostIp:String;
     HostUser:String;
     HostPassw:string;
     PeriodSec:Integer;
    end;}

    USHORT = WORD;
    ULONG = DWORD;
    time_t = Longint;
    IP_ADDRESS_STRING = record
      S: array [0..15] of Char;
    end;

    IP_MASK_STRING = IP_ADDRESS_STRING;
    PIP_MASK_STRING = ^IP_MASK_STRING;

    PIP_ADDR_STRING = ^IP_ADDR_STRING;
    IP_ADDR_STRING = record
      Next: PIP_ADDR_STRING;
      IpAddress: IP_ADDRESS_STRING;
      IpMask: IP_MASK_STRING;
      Context: DWORD;
    end;

    PIP_ADAPTER_INFO = ^IP_ADAPTER_INFO;
    IP_ADAPTER_INFO = record
      Next: PIP_ADAPTER_INFO;
      ComboIndex: DWORD;
      AdapterName: array [0..MAX_ADAPTER_NAME_LENGTH + 3] of Char;
      Description: array [0..MAX_ADAPTER_DESCRIPTION_LENGTH + 3] of Char;
      AddressLength: UINT;
      Address: array [0..MAX_ADAPTER_ADDRESS_LENGTH - 1] of BYTE;
      Index: DWORD;
      Type_: UINT;
      DhcpEnabled: UINT;
      CurrentIpAddress: PIP_ADDR_STRING;
      IpAddressList: IP_ADDR_STRING;
      GatewayList: IP_ADDR_STRING;
      DhcpServer: IP_ADDR_STRING;
      HaveWins: BOOL;
      PrimaryWinsServer: IP_ADDR_STRING;
      SecondaryWinsServer: IP_ADDR_STRING;
      LeaseObtained: time_t;
      LeaseExpires: time_t;
    end;

type
  TService1 = class(TService)
    IdFTP1: TIdFTP;
  private
    { Private declarations }
  public
  end;


function prGetIP:String;
function GetAdaptersInfo(pAdapterInfo: PIP_ADAPTER_INFO; var pOutBufLen: ULONG): DWORD; stdcall; external IPHelper;
procedure Delay(MSecs: Longint);

var
  Service1:TService1;
//  CurDir,DBName:String;
//  CommonSet:TCommonSet;

implementation

{$R *.DFM}

function prGetIP:String;
var
InterfaceInfo, TmpPointer: PIP_ADAPTER_INFO;
IP: PIP_ADDR_STRING;
Len: ULONG;
StrWk:String;
begin
  Result:='��';
  StrWk:='';

  if GetAdaptersInfo(nil, Len) = ERROR_BUFFER_OVERFLOW then
    begin
      GetMem(InterfaceInfo, Len);
      try
        if GetAdaptersInfo(InterfaceInfo, Len) = ERROR_SUCCESS then
          begin
            TmpPointer := InterfaceInfo;
            repeat
              IP := @TmpPointer.IpAddressList;
              repeat
                StrWk:=StrWk+Format('%s',[IP^.IpAddress.S])+' ';
//              memo1.Lines.Add(Format('%s - [%s]',[IP^.IpAddress.S, TmpPointer.Description]));
              IP := IP.Next;
              until IP = nil;
              TmpPointer := TmpPointer.Next;
            until TmpPointer = nil;
        end;
      finally
        FreeMem(InterfaceInfo);
      end;
  end
  else Result:='��';
//  ShowMessage('ERROR BUFER OVERFLOW');
  if StrWk>'' then
  begin
    if Length(StrWk)>14 then StrWk:=Copy(StrWk,1,14);
    Result:=StrWk;
  end;
end;


procedure Delay(MSecs: Longint);
var
  FirstTickCount, Now: Longint;
begin
  FirstTickCount := GetTickCount;
  repeat
    Application.ProcessMessages;
    { allowing access to other controls, etc. }
    Now := GetTickCount;
  until (Now - FirstTickCount >= MSecs) or (Now < FirstTickCount);
end;

end.

