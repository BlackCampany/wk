object fmRetCheck: TfmRetCheck
  Left = 296
  Top = 325
  BorderStyle = bsDialog
  Caption = #1042#1086#1079#1074#1088#1072#1090' '#1087#1086' '#1095#1077#1082#1091
  ClientHeight = 358
  ClientWidth = 624
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 312
    Width = 624
    Height = 46
    Align = alBottom
    BevelInner = bvLowered
    Color = 3106866
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 16
      Width = 109
      Height = 13
      Caption = #1057#1091#1084#1084#1072' '#1082' '#1074#1086#1079#1074#1088#1072#1090#1091
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object cxCurrencyEdit1: TcxCurrencyEdit
      Left = 124
      Top = 8
      EditValue = 0.000000000000000000
      ParentFont = False
      Properties.Alignment.Horz = taRightJustify
      Properties.EditFormat = '0.00'
      Properties.ReadOnly = True
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -19
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = []
      Style.IsFontAssigned = True
      TabOrder = 0
      Width = 153
    end
    object cxButton1: TcxButton
      Left = 368
      Top = 8
      Width = 97
      Height = 29
      Caption = #1054#1082
      Default = True
      ModalResult = 1
      TabOrder = 1
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 500
      Top = 8
      Width = 93
      Height = 29
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 2
      LookAndFeel.Kind = lfOffice11
    end
  end
  object GrRet: TcxGrid
    Left = 0
    Top = 0
    Width = 609
    Height = 297
    BorderWidth = 1
    TabOrder = 1
    LookAndFeel.Kind = lfFlat
    object ViewRet: TcxGridDBTableView
      OnDblClick = ViewRetDblClick
      NavigatorButtons.ConfirmDelete = False
      OnCustomDrawCell = ViewRetCustomDrawCell
      DataController.DataSource = dsteRet
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'DSUM'
          Column = ViewRetDSUM
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'RSUM'
          Column = ViewRetRSUM
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnGrouping = False
      OptionsCustomize.ColumnSorting = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.CellSelect = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.UnselectFocusedRecordOnExit = False
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      Styles.Header = dmC.cxStyle4
      object ViewRetNUMPOS: TcxGridDBColumn
        Caption = #8470' '#1087#1087
        DataBinding.FieldName = 'NUMPOS'
        Width = 29
      end
      object ViewRetARTICUL: TcxGridDBColumn
        Caption = #1040#1088#1090#1080#1082#1091#1083
        DataBinding.FieldName = 'ARTICUL'
        Width = 41
      end
      object ViewRetBAR: TcxGridDBColumn
        Caption = #1064#1090#1088#1080#1093#1082#1086#1076
        DataBinding.FieldName = 'RBAR'
        Styles.Content = dmC.cxStyle1
        Width = 95
      end
      object ViewRetNAME: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'NAME'
        Styles.Content = dmC.cxStyle2
        Width = 140
      end
      object ViewRetQUANT: TcxGridDBColumn
        Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086
        DataBinding.FieldName = 'QUANT'
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.DecimalPlaces = 3
        Properties.DisplayFormat = ',0.###;-,0.###'
        Styles.Content = dmC.cxStyle5
        Width = 50
      end
      object ViewRetPRICE: TcxGridDBColumn
        Caption = #1062#1077#1085#1072
        DataBinding.FieldName = 'PRICE'
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Width = 54
      end
      object ViewRetDPROC: TcxGridDBColumn
        Caption = '% '#1089#1082#1080#1076#1082#1080
        DataBinding.FieldName = 'DPROC'
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.DisplayFormat = ',0.00%;-,0.00%'
        Styles.Content = dmC.cxStyle10
        Width = 47
      end
      object ViewRetDSUM: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1089#1082#1080#1076#1082#1080
        DataBinding.FieldName = 'DSUM'
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.DisplayFormat = ',0.00;-,0.00'
        Styles.Content = dmC.cxStyle10
        Width = 52
      end
      object ViewRetRSUM: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072
        DataBinding.FieldName = 'RSUM'
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Styles.Content = dmC.cxStyle5
        Width = 62
      end
      object ViewRetIRET: TcxGridDBColumn
        Caption = #1042#1099#1073#1088#1072#1085
        DataBinding.FieldName = 'IRET'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Items = <
          item
            Description = '-'
            ImageIndex = 0
            Value = 0
          end
          item
            Description = '+'
            Value = 1
          end>
        Width = 40
      end
    end
    object LeRet: TcxGridLevel
      GridView = ViewRet
    end
  end
  object teRet: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 144
    Top = 72
    object teRetCASHNUM: TIntegerField
      FieldName = 'CASHNUM'
    end
    object teRetNUMPOS: TIntegerField
      FieldName = 'NUMPOS'
    end
    object teRetARTICUL: TStringField
      FieldName = 'ARTICUL'
      Size = 10
    end
    object teRetRBAR: TStringField
      FieldName = 'RBAR'
      Size = 15
    end
    object teRetNAME: TStringField
      FieldName = 'NAME'
      Size = 60
    end
    object teRetQUANT: TFloatField
      FieldName = 'QUANT'
    end
    object teRetPRICE: TFloatField
      FieldName = 'PRICE'
    end
    object teRetDPROC: TFloatField
      FieldName = 'DPROC'
    end
    object teRetDSUM: TFloatField
      FieldName = 'DSUM'
    end
    object teRetRSUM: TFloatField
      FieldName = 'RSUM'
    end
    object teRetDEPART: TIntegerField
      FieldName = 'DEPART'
    end
    object teRetEDIZM: TStringField
      FieldName = 'EDIZM'
      Size = 10
    end
    object teRetIRET: TSmallintField
      FieldName = 'IRET'
    end
    object teRetAMARK: TStringField
      FieldName = 'AMARK'
      Size = 150
    end
  end
  object dsteRet: TDataSource
    DataSet = teRet
    Left = 144
    Top = 124
  end
  object amRet: TActionManager
    Left = 248
    Top = 84
    StyleName = 'XP Style'
    object acExit: TAction
      Caption = 'acExit'
      ShortCut = 27
      SecondaryShortCuts.Strings = (
        'F10')
      OnExecute = acExitExecute
    end
    object acSel: TAction
      Caption = 'acSel'
      ShortCut = 32
      OnExecute = acSelExecute
    end
  end
end
