object fmMainConvStd: TfmMainConvStd
  Left = 568
  Top = 269
  Width = 489
  Height = 551
  AlphaBlend = True
  AlphaBlendValue = 150
  Caption = 'fmMainConvStd'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object RxTray: TRxTrayIcon
    Icon.Data = {
      0000010001002020100000000000E80200001600000028000000200000004000
      0000010004000000000080020000000000000000000000000000000000000000
      000000008000008000000080800080000000800080008080000080808000C0C0
      C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000FFFFFFFFF00000000000000000000000F9999999F000000
      00000000000000000F9999999F00000000000000000000000F9999999F000000
      00000000000000000F9999999F00000000000000000000000F9999999F000000
      00000000000000000F9999999F00000000000000000000000F9999999F000000
      00000000000000000FFFFFFFFF00000000000000000000000000000000000000
      00000000000000000000000000000FFFFFFFFF00000000000000000000000FCC
      CCCCCF00000000000000000000000FCCCCCCCF00000000000000000000000FCC
      CCCCCF00000000000000000000000FCCCCCCCF00000000000000000000000FCC
      CCCCCF00000000000000000000000FCCCCCCCF00000000000000000000000FCC
      CCCCCF00000000000000000000000FFFFFFFFF00000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FFFFEFFFFFBFEFFFFFBFEFFFFF1FEFFFFF1FEF83F803EF7DF001EEFEF001EEFE
      F001EEFEF001EEFC7001EEF83001EEF83001EEF83001C4783001C03838038038
      3C4700183C4700183EEF00183EEF00183EEF001C7EEF001EFEEF001EFEEF001E
      FEEF001F7DEF803F83EFF1FFFFEFF1FFFFEFFBFFFFEFFBFFFFEFFFFFFFFF}
    PopupMenu = PopupMenu1
    OnClick = RxTrayClick
    Left = 200
    Top = 32
  end
  object PopupMenu1: TPopupMenu
    Left = 112
    Top = 32
    object N1: TMenuItem
      Caption = #1048#1089#1090#1086#1088#1080#1103
      OnClick = N1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object N3: TMenuItem
      Caption = #1042#1099#1093#1086#1076
      OnClick = N3Click
    end
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 10000
    OnTimer = Timer1Timer
    Left = 272
    Top = 33
  end
  object taCard: TTable
    OnCalcFields = taCardCalcFields
    DatabaseName = 'C:\Projects\Casher\FB\Bin\Import\'
    SessionName = 'Ses1'
    ReadOnly = True
    TableName = '_710OE74.DB'
    Left = 64
    Top = 104
    object taCardArticul: TStringField
      FieldName = 'Articul'
      Size = 30
    end
    object taCardName: TStringField
      FieldName = 'Name'
      Size = 80
      Transliterate = False
    end
    object taCardMesuriment: TStringField
      FieldName = 'Mesuriment'
      Size = 10
      Transliterate = False
    end
    object taCardMesPresision: TFloatField
      FieldName = 'MesPresision'
    end
    object taCardAdd1: TStringField
      FieldName = 'Add1'
    end
    object taCardAdd2: TStringField
      FieldName = 'Add2'
    end
    object taCardAdd3: TStringField
      FieldName = 'Add3'
    end
    object taCardAddNum1: TFloatField
      FieldName = 'AddNum1'
    end
    object taCardAddNum2: TFloatField
      FieldName = 'AddNum2'
    end
    object taCardAddNum3: TFloatField
      FieldName = 'AddNum3'
    end
    object taCardScale: TStringField
      FieldName = 'Scale'
      Size = 10
    end
    object taCardGroop1: TSmallintField
      FieldName = 'Groop1'
    end
    object taCardGroop2: TSmallintField
      FieldName = 'Groop2'
    end
    object taCardGroop3: TSmallintField
      FieldName = 'Groop3'
    end
    object taCardGroop4: TSmallintField
      FieldName = 'Groop4'
    end
    object taCardGroop5: TSmallintField
      FieldName = 'Groop5'
    end
    object taCardPriceRub: TCurrencyField
      FieldName = 'PriceRub'
    end
    object taCardPriceCur: TCurrencyField
      FieldName = 'PriceCur'
    end
    object taCardClientIndex: TSmallintField
      FieldName = 'ClientIndex'
    end
    object taCardCommentary: TStringField
      FieldName = 'Commentary'
      Size = 80
    end
    object taCardDeleted: TSmallintField
      FieldName = 'Deleted'
    end
    object taCardModDate: TDateField
      FieldName = 'ModDate'
    end
    object taCardModTime: TSmallintField
      FieldName = 'ModTime'
    end
    object taCardModPersonIndex: TSmallintField
      FieldName = 'ModPersonIndex'
    end
    object taCardGr1: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'Gr1'
      Calculated = True
    end
    object taCardGr2: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'Gr2'
      Calculated = True
    end
    object taCardGr3: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'Gr3'
      Calculated = True
    end
    object taCardGr4: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'Gr4'
      Calculated = True
    end
    object taCardGr5: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'Gr5'
      Calculated = True
    end
  end
  object taBar: TTable
    DatabaseName = 'C:\Projects\Casher\FB\Bin\Import\'
    SessionName = 'Ses1'
    ReadOnly = True
    TableName = '_710OE79.DB'
    Left = 112
    Top = 104
    object taBarBarCode: TStringField
      FieldName = 'BarCode'
      Size = 15
    end
    object taBarCardArticul: TStringField
      FieldName = 'CardArticul'
      Size = 30
    end
    object taBarCardSize: TStringField
      FieldName = 'CardSize'
      Size = 10
    end
    object taBarQuantity: TFloatField
      FieldName = 'Quantity'
    end
  end
  object tbTax: TTable
    DatabaseName = 'C:\Projects\Casher\FB\Bin\Import\'
    SessionName = 'Ses1'
    ReadOnly = True
    TableName = '_710OE7F.DB'
    Left = 160
    Top = 104
    object tbTaxCardArticul: TStringField
      FieldName = 'CardArticul'
      Size = 30
    end
    object tbTaxTaxIndex: TSmallintField
      FieldName = 'TaxIndex'
    end
    object tbTaxTax: TFloatField
      FieldName = 'Tax'
    end
    object tbTaxTaxSumRub: TFloatField
      FieldName = 'TaxSumRub'
    end
    object tbTaxTaxSumCur: TFloatField
      FieldName = 'TaxSumCur'
    end
  end
  object taList: TTable
    AutoRefresh = True
    DatabaseName = 'C:\Projects\Casher\FB\Bin\Import\'
    SessionName = 'Ses1'
    ReadOnly = True
    TableName = 'CASH001.DB'
    Left = 64
    Top = 168
    object taListTName: TStringField
      FieldName = 'TName'
      Size = 8
    end
    object taListDataType: TSmallintField
      FieldName = 'DataType'
    end
    object taListOperation: TSmallintField
      FieldName = 'Operation'
    end
  end
  object taClass: TTable
    OnCalcFields = taClassCalcFields
    DatabaseName = 'C:\Projects\Casher\FB\Bin\Import\'
    SessionName = 'Ses1'
    ReadOnly = True
    TableName = '_710Q4AW.DB'
    Left = 208
    Top = 104
    object taClassGroop1: TSmallintField
      FieldName = 'Groop1'
    end
    object taClassGroop2: TSmallintField
      FieldName = 'Groop2'
    end
    object taClassGroop3: TSmallintField
      FieldName = 'Groop3'
    end
    object taClassGroop4: TSmallintField
      FieldName = 'Groop4'
    end
    object taClassGroop5: TSmallintField
      FieldName = 'Groop5'
    end
    object taClassName: TStringField
      FieldName = 'Name'
      Size = 80
      Transliterate = False
    end
    object taClassGr1: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'Gr1'
      Calculated = True
    end
    object taClassGr2: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'Gr2'
      Calculated = True
    end
    object taClassGr3: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'Gr3'
      Calculated = True
    end
    object taClassGr4: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'Gr4'
      Calculated = True
    end
    object taClassGr5: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'Gr5'
      Calculated = True
    end
  end
  object tbTaxT: TTable
    DatabaseName = 'C:\Projects\Casher\FB\Bin\Import\'
    SessionName = 'Ses1'
    ReadOnly = True
    TableName = '_710OWQ5.DB'
    Left = 256
    Top = 104
    object tbTaxTID: TSmallintField
      FieldName = 'ID'
    end
    object tbTaxTPriority: TSmallintField
      FieldName = 'Priority'
    end
    object tbTaxTName: TStringField
      FieldName = 'Name'
      Size = 40
      Transliterate = False
    end
  end
  object tbCashers: TTable
    DatabaseName = 'C:\Crystal\POS\1\'
    SessionName = 'Ses1'
    ReadOnly = True
    TableName = '_710OXC4.DB'
    Left = 120
    Top = 168
    object tbCashersIdent: TSmallintField
      FieldName = 'Ident'
    end
    object tbCashersName: TStringField
      FieldName = 'Name'
      Size = 40
      Transliterate = False
    end
    object tbCashersPassw: TStringField
      FieldName = 'Passw'
      Size = 15
      Transliterate = False
    end
    object tbCashersOfficialIndex: TSmallintField
      FieldName = 'OfficialIndex'
    end
  end
  object tbDeparts: TTable
    DatabaseName = 'C:\Crystal\POS\1\'
    SessionName = 'Ses1'
    ReadOnly = True
    TableName = '_710OY2Z.DB'
    Left = 180
    Top = 168
    object tbDepartsID: TSmallintField
      FieldName = 'ID'
    end
    object tbDepartsName: TStringField
      FieldName = 'Name'
      Size = 40
      Transliterate = False
    end
  end
  object tbDiscSum: TTable
    DatabaseName = 'C:\Crystal\POS\1\'
    SessionName = 'Ses1'
    ReadOnly = True
    TableName = '_716VZ1M.DB'
    Left = 239
    Top = 168
    object tbDiscSumPriceIndex: TSmallintField
      FieldName = 'PriceIndex'
    end
    object tbDiscSumTime: TSmallintField
      FieldName = 'Time'
    end
    object tbDiscSumSumma: TCurrencyField
      FieldName = 'Summa'
    end
    object tbDiscSumDiscount: TFloatField
      FieldName = 'Discount'
    end
  end
  object tbDiscQ: TTable
    DatabaseName = 'C:\Crystal\POS\1\'
    SessionName = 'Ses1'
    ReadOnly = True
    TableName = '_7160X8B.DB'
    Left = 292
    Top = 168
    object tbDiscQCardArticul: TStringField
      FieldName = 'CardArticul'
      Size = 30
    end
    object tbDiscQQuantity: TFloatField
      FieldName = 'Quantity'
    end
    object tbDiscQDiscount: TCurrencyField
      FieldName = 'Discount'
    end
  end
  object tbDiscCard: TTable
    DatabaseName = 'C:\Crystal\POS\1\'
    SessionName = 'Ses1'
    ReadOnly = True
    TableName = '_71695H6.DB'
    Left = 64
    Top = 232
    object tbDiscCardBarCode: TStringField
      FieldName = 'BarCode'
      Size = 22
    end
    object tbDiscCardName: TStringField
      FieldName = 'Name'
      Size = 40
      Transliterate = False
    end
    object tbDiscCardPercent: TFloatField
      FieldName = 'Percent'
    end
    object tbDiscCardClientIndex: TSmallintField
      FieldName = 'ClientIndex'
    end
  end
  object tbDiscStop: TTable
    DatabaseName = 'C:\Crystal\POS\1\'
    SessionName = 'Ses1'
    ReadOnly = True
    TableName = '_716KOXZ.DB'
    Left = 136
    Top = 232
    object tbDiscStopCodeStart: TStringField
      FieldName = 'CodeStart'
      Size = 22
      Transliterate = False
    end
    object tbDiscStopCodeEnd: TStringField
      FieldName = 'CodeEnd'
      Size = 22
      Transliterate = False
    end
  end
  object tbAdvDisc: TTable
    DatabaseName = 'C:\Crystal\POS\1\'
    SessionName = 'Ses1'
    ReadOnly = True
    TableName = '_716R1KK.DB'
    Left = 200
    Top = 232
    object tbAdvDiscGroop1: TSmallintField
      FieldName = 'Groop1'
    end
    object tbAdvDiscGroop2: TSmallintField
      FieldName = 'Groop2'
    end
    object tbAdvDiscGroop3: TSmallintField
      FieldName = 'Groop3'
    end
    object tbAdvDiscGroop4: TSmallintField
      FieldName = 'Groop4'
    end
    object tbAdvDiscGroop5: TSmallintField
      FieldName = 'Groop5'
    end
    object tbAdvDiscSumma: TCurrencyField
      FieldName = 'Summa'
    end
    object tbAdvDiscBarCode: TStringField
      FieldName = 'BarCode'
      Size = 22
      Transliterate = False
    end
    object tbAdvDiscPercent: TFloatField
      FieldName = 'Percent'
    end
  end
  object tbCredCard: TTable
    DatabaseName = 'C:\Crystal\POS\1\'
    SessionName = 'Ses1'
    ReadOnly = True
    TableName = '_716TVR7.DB'
    Left = 64
    Top = 288
    object tbCredCardID: TSmallintField
      FieldName = 'ID'
    end
    object tbCredCardName: TStringField
      FieldName = 'Name'
      Size = 80
      Transliterate = False
    end
    object tbCredCardClientIndex: TSmallintField
      FieldName = 'ClientIndex'
    end
    object tbCredCardLimitSum: TCurrencyField
      FieldName = 'LimitSum'
    end
    object tbCredCardCanReturn: TSmallintField
      FieldName = 'CanReturn'
    end
  end
  object tbCredPref: TTable
    DatabaseName = 'C:\Crystal\POS\1\'
    SessionName = 'Ses1'
    ReadOnly = True
    TableName = '_716TW7U.DB'
    Left = 136
    Top = 288
    object tbCredPrefPrefix: TStringField
      FieldName = 'Prefix'
      Size = 19
    end
    object tbCredPrefCredCardIndex: TSmallintField
      FieldName = 'CredCardIndex'
    end
  end
  object Session1: TSession
    NetFileDir = 'C:\'
    SessionName = 'Ses1'
    Left = 405
    Top = 38
  end
  object Timer2: TTimer
    Enabled = False
    OnTimer = Timer2Timer
    Left = 326
    Top = 32
  end
  object taDStop: TTable
    DatabaseName = 'C:\_MCrystal\Bin\TRF'
    SessionName = 'Ses1'
    TableName = 'DSTOP.DB'
    Left = 260
    Top = 232
    object taDStopCardArticul: TStringField
      FieldName = 'CardArticul'
      Size = 30
    end
    object taDStopPercent: TFloatField
      FieldName = 'Percent'
    end
  end
end
