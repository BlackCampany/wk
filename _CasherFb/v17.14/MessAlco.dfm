object fmMessAlco: TfmMessAlco
  Left = 1118
  Top = 1122
  Width = 617
  Height = 278
  Caption = #1055#1088#1077#1076#1091#1087#1088#1077#1078#1076#1077#1085#1080#1077' '#1087#1088#1080' '#1087#1088#1086#1076#1072#1078#1077' '#1072#1083#1082#1086#1075#1086#1083#1103
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 28
    Width = 577
    Height = 29
    Alignment = taCenter
    AutoSize = False
    Caption = #1053#1077' '#1087#1088#1086#1076#1072#1074#1072#1081' '#1072#1083#1082#1086#1075#1086#1083#1100' '#1085#1077#1089#1086#1074#1077#1088#1096#1077#1085#1085#1086#1083#1077#1090#1085#1080#1084' !'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -24
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label2: TLabel
    Left = 16
    Top = 64
    Width = 577
    Height = 32
    Alignment = taCenter
    AutoSize = False
    Caption = #1055#1088#1086#1074#1077#1088#1100' '#1076#1086#1082#1091#1084#1077#1085#1090' !!!'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -27
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object Label3: TLabel
    Left = 16
    Top = 168
    Width = 577
    Height = 27
    Alignment = taCenter
    AutoSize = False
    Caption = #1076#1083#1103' '#1074#1099#1093#1086#1076#1072' '#1085#1072#1078#1084#1080#1090#1077
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clYellow
    Font.Height = -24
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object Label4: TLabel
    Left = 300
    Top = 200
    Width = 15
    Height = 32
    Caption = '5'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clYellow
    Font.Height = -27
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object dxfBackGround1: TdxfBackGround
    BkColor.BeginColor = 16767449
    BkColor.EndColor = clBlue
    BkColor.FillStyle = fsVert
    BkAnimate.Speed = 700
    Left = 464
    Top = 108
  end
  object ActionManager1: TActionManager
    Left = 368
    Top = 108
    StyleName = 'XP Style'
    object acExit: TAction
      Caption = 'acExit'
      ShortCut = 121
      SecondaryShortCuts.Strings = (
        'ESC')
      OnExecute = acExitExecute
    end
  end
end
