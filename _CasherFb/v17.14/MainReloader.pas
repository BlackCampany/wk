unit MainReloader;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxControls, cxContainer, cxEdit, cxTextEdit, cxMemo, ExtCtrls, ShellAPI;

type
  TForm1 = class(TForm)
    Memo1: TcxMemo;
    Timer1: TTimer;
    Timer2: TTimer;
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

procedure Delay(MSecs: Longint);

var
  Form1: TForm1;
  iCount: integer = 5;
  CurDir:String;

implementation

{$R *.dfm}

procedure Delay(MSecs: Longint);
var
  FirstTickCount, Now: Longint;
begin
  FirstTickCount := GetTickCount;
  repeat
    Application.ProcessMessages;
    { allowing access to other controls, etc. }
    Now := GetTickCount;
  until (Now - FirstTickCount >= MSecs) or (Now < FirstTickCount);
end;


procedure TForm1.FormCreate(Sender: TObject);
begin
  CurDir := ExtractFilePath(ParamStr(0));
  Memo1.Clear;
  Memo1.Lines.Add('����� .. ���� ������������ ��.');
  Timer1.Enabled:=True;
end;

procedure TForm1.Timer1Timer(Sender: TObject);
Var fs,fd:File;
    NumRead, NumWritten: Integer;
    Buf: array[1..2048] of Char;
begin
  if iCount>0 then
  begin
    dec(iCount);
    Memo1.Lines.Add(' ..');
  end
  else
  begin
    Timer1.Enabled:=False;

    if FileExists(CurDir+'casher.new') then
    begin
      Memo1.Lines.Add('��������������  casher.exe � casher.old'); delay(10);

      if FileExists(CurDir+'casher.exe') then
      begin
        try
          assignfile(fs,CurDir+'casher.exe');
          Reset(fs,1);

          assignfile(fd,CurDir+'casher.old');
          rewrite(fd,1);

          repeat
            BlockRead(fs, Buf, SizeOf(Buf), NumRead);
            BlockWrite(fd, Buf, NumRead, NumWritten);
          until (NumRead = 0) or (NumWritten <> NumRead);

        finally
          CloseFile(fs);
          CloseFile(fd);
        end;
      end;
    end;
    delay(1000);

    if FileExists(CurDir+'casher.new') then
    begin
      Memo1.Lines.Add('��������������  casher.new � casher.exe'); delay(10);

      try
        assignfile(fs,CurDir+'casher.new');
        Reset(fs,1);

        assignfile(fd,CurDir+'casher.exe');
        rewrite(fd,1);

        repeat
          BlockRead(fs, Buf, SizeOf(Buf), NumRead);
          BlockWrite(fd, Buf, NumRead, NumWritten);
        until (NumRead = 0) or (NumWritten <> NumRead);

      finally
        CloseFile(fs);
        CloseFile(fd);
      end;

      delay(1000);

      try
        assignfile(fs,CurDir+'casher.new');
        erase(fs);
        delay(100);
        if fileexists(CurDir+'casher.new') then DeleteFile(CurDir+'casher.new');
      except
      end;
    end;

    Memo1.Lines.Add('������ ��.');

    if FileExists(CurDir+'casher.exe') then
    begin
      ShellExecute(handle, 'open', PChar(CurDir+'casher.exe'),'','', SW_SHOWNORMAL);
     end else
       Memo1.Lines.Add('���������� �� ��������. ���������� � ��������������.');
    Delay(1000);
    close;

  end;
end;

procedure TForm1.Timer2Timer(Sender: TObject);
begin
  Timer2.Enabled:=False;
end;

end.
