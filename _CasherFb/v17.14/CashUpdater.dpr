program CashUpdater;

uses
  Forms,
  MainCashUpd in 'MainCashUpd.pas' {fmMainCashUpd},
  AddVer in 'AddVer.pas' {fmAddVer},
  CashList in 'CashList.pas' {fmCashList};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfmMainCashUpd, fmMainCashUpd);
  Application.CreateForm(TfmAddVer, fmAddVer);
  Application.CreateForm(TfmCashList, fmCashList);
  Application.Run;
end.
