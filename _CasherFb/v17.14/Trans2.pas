unit Trans2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons, cxSpinEdit,
  cxControls, cxContainer, cxEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit,
  cxCalendar, XPStyleActnCtrls, ActnList, ActnMan;

type
  TfmTrans2 = class(TForm)
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    cxDateEdit1: TcxDateEdit;
    cxSpinEdit1: TcxSpinEdit;
    amTrans2: TActionManager;
    acFocusNext: TAction;
    acFocusPre: TAction;
    acLow: TAction;
    acHi: TAction;
    procedure acFocusNextExecute(Sender: TObject);
    procedure acFocusPreExecute(Sender: TObject);
    procedure acLowExecute(Sender: TObject);
    procedure acHiExecute(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmTrans2: TfmTrans2;

implementation

{$R *.dfm}

procedure TfmTrans2.acFocusNextExecute(Sender: TObject);
begin
  if cxDateEdit1.Focused then begin cxButton1.SetFocus; exit; end;
  if cxButton1.Focused then begin cxSpinEdit1.SetFocus; exit; end;
  if cxSpinEdit1.Focused then begin cxButton2.SetFocus; exit; end;
  if cxButton2.Focused then begin cxButton3.SetFocus; exit; end;
  if cxButton3.Focused then begin cxDateEdit1.SetFocus; exit; end;
end;

procedure TfmTrans2.acFocusPreExecute(Sender: TObject);
begin
  if cxButton1.Focused then begin cxDateEdit1.SetFocus; exit; end;
  if cxSpinEdit1.Focused then begin cxButton1.SetFocus; exit; end;
  if cxButton2.Focused then begin cxSpinEdit1.SetFocus; exit; end;
  if cxButton3.Focused then begin cxButton2.SetFocus; exit; end;
  if cxDateEdit1.Focused then begin cxButton3.SetFocus; exit; end;
end;

procedure TfmTrans2.acLowExecute(Sender: TObject);
begin
  if cxDateEdit1.Focused then begin cxDateEdit1.EditValue:=cxDateEdit1.EditValue-1; exit; end;
  if cxSpinEdit1.Focused then begin cxSpinEdit1.EditValue:=cxSpinEdit1.EditValue-1; exit; end;
end;

procedure TfmTrans2.acHiExecute(Sender: TObject);
begin
  if cxDateEdit1.Focused then begin cxDateEdit1.EditValue:=cxDateEdit1.EditValue+1; exit; end;
  if cxSpinEdit1.Focused then begin cxSpinEdit1.EditValue:=cxSpinEdit1.EditValue+1; exit; end;
end;

procedure TfmTrans2.cxButton3Click(Sender: TObject);
begin
  ModalResult:=mrCancel;
end;

end.
