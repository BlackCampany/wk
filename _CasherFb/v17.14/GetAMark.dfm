object fmGetAM: TfmGetAM
  Left = 809
  Top = 494
  BorderStyle = bsSingle
  Caption = #1050#1086#1085#1090#1088#1086#1083#1100' '#1040#1082#1094#1080#1079#1085#1086#1081' '#1052#1072#1088#1082#1080
  ClientHeight = 228
  ClientWidth = 389
  Color = 13473127
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 4
    Top = 4
    Width = 381
    Height = 221
    BevelInner = bvLowered
    Color = 6832674
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 12
      Top = 24
      Width = 137
      Height = 25
      Caption = #1054#1082
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -21
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ModalResult = 1
      ParentFont = False
      TabOrder = 0
      TabStop = False
      WordWrap = True
      Colors.Default = 14930628
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      Layout = blGlyphTop
      LookAndFeel.Kind = lfFlat
    end
    object Panel2: TPanel
      Left = 8
      Top = 8
      Width = 361
      Height = 89
      BevelInner = bvLowered
      Color = 16771797
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      object Edit1: TEdit
        Left = 16
        Top = 8
        Width = 313
        Height = 24
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        Text = 'Edit1'
        OnKeyPress = Edit1KeyPress
      end
      object cxLabel1: TcxLabel
        Left = 2
        Top = 2
        Align = alClient
        AutoSize = False
        Caption = #1057#1095#1080#1090#1072#1081#1090#1077' '#1072#1082#1094#1080#1079#1085#1091#1102' '#1084#1072#1088#1082#1091
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -19
        Style.Font.Name = 'MS Sans Serif'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        Properties.Alignment.Horz = taCenter
        Properties.Alignment.Vert = taVCenter
        Properties.WordWrap = True
        Height = 85
        Width = 357
      end
    end
    object cxButton2: TcxButton
      Left = 56
      Top = 160
      Width = 269
      Height = 45
      Caption = #1054#1090#1084#1077#1085#1072' '#1087#1088#1086#1076#1072#1078#1080' '
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -21
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ModalResult = 2
      ParentFont = False
      TabOrder = 2
      Colors.Default = 14930628
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      Layout = blGlyphTop
      LookAndFeel.Kind = lfFlat
    end
    object Panel3: TPanel
      Left = 8
      Top = 104
      Width = 361
      Height = 41
      BevelInner = bvLowered
      Color = 16771797
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      object cxLabel2: TcxLabel
        Left = 2
        Top = 2
        Align = alClient
        AutoSize = False
        Caption = '19Nkjhkjhkjhkhkhkyuouy;j;lj;;oi;ojh;'
        ParentColor = False
        ParentFont = False
        Style.Color = clWhite
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -13
        Style.Font.Name = 'MS Sans Serif'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        Properties.Alignment.Horz = taCenter
        Properties.Alignment.Vert = taVCenter
        Properties.WordWrap = True
        Height = 37
        Width = 357
      end
    end
  end
  object amGetAM: TActionManager
    Left = 332
    Top = 92
    StyleName = 'XP Style'
    object acReadAM: TAction
      Caption = 'acReadAM'
      ShortCut = 123
      OnExecute = acReadAMExecute
    end
    object acExit: TAction
      Caption = 'acExit'
      ShortCut = 27
      OnExecute = acExitExecute
    end
  end
end
