unit MainRepair;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxControls, cxContainer, cxEdit, cxTextEdit, cxMaskEdit,
  cxButtonEdit, StdCtrls, Menus, cxLookAndFeelPainters, cxMemo, cxSpinEdit,
  cxDropDownEdit, cxCalendar, cxButtons, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, DB, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxGridCustomView, cxGrid, dxmdaset, FIBDatabase, pFIBDatabase,
  FIBDataSet, pFIBDataSet,
  //pvtables, btvtables,
  cxCheckBox;

type
  TfmMainRepair = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    cxButtonEdit1: TcxButtonEdit;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxDateEdit1: TcxDateEdit;
    cxSpinEdit1: TcxSpinEdit;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    Memo1: TcxMemo;
    cxSpinEdit2: TcxSpinEdit;
    Label4: TLabel;
    Label5: TLabel;
    cxSpinEdit3: TcxSpinEdit;
    teCheck: TdxMemData;
    ViewCh: TcxGridDBTableView;
    LevelCh: TcxGridLevel;
    GrCheck: TcxGrid;
    teCheckCashNum: TIntegerField;
    teCheckCheckNum: TIntegerField;
    teCheckId: TIntegerField;
    teCheckArticul: TIntegerField;
    teCheckBar: TStringField;
    teCheckQuant: TFloatField;
    teCheckPrice: TFloatField;
    teCheckDProc: TFloatField;
    teCheckDSum: TFloatField;
    teCheckRSum: TFloatField;
    dsteCheck: TDataSource;
    teCheckCassir: TIntegerField;
    teCheckOperation: TIntegerField;
    ViewChRecId: TcxGridDBColumn;
    ViewChCashNum: TcxGridDBColumn;
    ViewChCheckNum: TcxGridDBColumn;
    ViewChId: TcxGridDBColumn;
    ViewChArticul: TcxGridDBColumn;
    ViewChBar: TcxGridDBColumn;
    ViewChQuant: TcxGridDBColumn;
    ViewChPrice: TcxGridDBColumn;
    ViewChDProc: TcxGridDBColumn;
    ViewChDSum: TcxGridDBColumn;
    ViewChRSum: TcxGridDBColumn;
    ViewChCassir: TcxGridDBColumn;
    ViewChOperation: TcxGridDBColumn;
    Label6: TLabel;
    cxSpinEdit4: TcxSpinEdit;
    Label7: TLabel;
    cxComboBox1: TcxComboBox;
    dbCh: TpFIBDatabase;
    trSel: TpFIBTransaction;
    trUpd: TpFIBTransaction;
    quCheck: TpFIBDataSet;
    quCheckSHOPINDEX: TFIBSmallIntField;
    quCheckCASHNUMBER: TFIBIntegerField;
    quCheckZNUMBER: TFIBIntegerField;
    quCheckCHECKNUMBER: TFIBIntegerField;
    quCheckID: TFIBIntegerField;
    quCheckCHDATE: TFIBDateTimeField;
    quCheckARTICUL: TFIBStringField;
    quCheckBAR: TFIBStringField;
    quCheckCARDSIZE: TFIBStringField;
    quCheckQUANTITY: TFIBFloatField;
    quCheckPRICERUB: TFIBFloatField;
    quCheckDPROC: TFIBFloatField;
    quCheckDSUM: TFIBFloatField;
    quCheckTOTALRUB: TFIBFloatField;
    quCheckCASHER: TFIBIntegerField;
    quCheckDEPART: TFIBIntegerField;
    quCheckOPERATION: TFIBSmallIntField;
    quCheckP: TpFIBDataSet;
    quCheckPSHOPINDEX: TFIBSmallIntField;
    quCheckPCASHNUMBER: TFIBIntegerField;
    quCheckPZNUMBER: TFIBIntegerField;
    quCheckPCHECKNUMBER: TFIBIntegerField;
    quCheckPCASHER: TFIBIntegerField;
    quCheckPCHECKSUM: TFIBFloatField;
    quCheckPPAYSUM: TFIBFloatField;
    quCheckPDSUM: TFIBFloatField;
    quCheckPDBAR: TFIBStringField;
    quCheckPCOUNTPOS: TFIBSmallIntField;
    quCheckPCHDATE: TFIBDateTimeField;
    quCheckPPAYMENT: TFIBSmallIntField;
    quCheckPOPER: TFIBSmallIntField;
    PopupMenu1: TPopupMenu;
    Excel1: TMenuItem;
    cxButton5: TcxButton;
    cxButton6: TcxButton;
    Label8: TLabel;
    cxTextEdit1: TcxTextEdit;
    teCheckDepart: TIntegerField;
    cxCheckBox1: TcxCheckBox;
    cxButton7: TcxButton;
    teCh: TdxMemData;
    teChNum: TIntegerField;
    dsteCh: TDataSource;
    ViCh: TcxGridDBTableView;
    LeCh: TcxGridLevel;
    GrCh: TcxGrid;
    ViChRecId: TcxGridDBColumn;
    ViChNum: TcxGridDBColumn;
    cxButton8: TcxButton;
    GrChSum: TcxGrid;
    ViewChSum: TcxGridDBTableView;
    LeChSum: TcxGridLevel;
    teChSum: TdxMemData;
    dsteChSum: TDataSource;
    teChSumrSum: TFloatField;
    teChSumrSumCh: TFloatField;
    teChSumNum: TIntegerField;
    ViewChSumRecId: TcxGridDBColumn;
    ViewChSumrSum: TcxGridDBColumn;
    ViewChSumrSumCh: TcxGridDBColumn;
    ViewChSumNum: TcxGridDBColumn;
    quChSum: TpFIBDataSet;
    quChSumRSUM: TFIBFloatField;
    PopupMenu2: TPopupMenu;
    MenuItem1: TMenuItem;
    quChMax: TpFIBDataSet;
    quChMaxMAXCH: TFIBIntegerField;
    teChSumrDif: TFloatField;
    ViewChSumrDif: TcxGridDBColumn;
    cxButton9: TcxButton;
    Label9: TLabel;
    cxSpinEdit5: TcxSpinEdit;
    Label10: TLabel;
    cxSpinEdit6: TcxSpinEdit;
    cxCheckBox2: TcxCheckBox;
    cxButton10: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure Excel1Click(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
    procedure cxButton7Click(Sender: TObject);
    procedure ViChDblClick(Sender: TObject);
    procedure cxButton8Click(Sender: TObject);
    procedure ViewChSumCustomization(Sender: TObject);
    procedure cxButton9Click(Sender: TObject);
    procedure cxButton10Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure prSaveCh;
    procedure prWr(S:String);
    procedure prSaveChSpec(iCh,iDate,iCash:INteger);
  end;

Function fs1(rSum:Real):String;
function RoundSpec(rSumCh:Real; Var rSumD:Real):Real;
function S2f(sVal:string):Real;

var
  fmMainRepair: TfmMainRepair;
  bStopCompare:Boolean;

implementation

uses Un1, u2fdk;

function S2f(sVal:string):Real;
begin
  while pos('.',sVal)>0 do sVal[pos('.',sVal)]:=',';
  Result:=StrToFloatDef(Trim(sVal),0);
end;

{$R *.dfm}
procedure TfmMainRepair.prWr(S:String);
begin
  Memo1.Lines.Add(S);
  delay(10);
end;

Function fs1(rSum:Real):String;
Var s:String;
begin
  s:=FloatToStr(rSum);
//  while pos(',',s)>0 do s[pos(',',s)]:='.';
  Result:=s;
end;


function RoundSpec(rSumCh:Real; Var rSumD:Real):Real;
Var iSum,iRound,iRes:INteger;
    rS1,rS2,rS3:Real;
begin
  iSum:=RoundEx(rSumCh*100);
  iRound:=CommonSet.RoundSum;
  if iRound<100 then
  begin
    rS1:=iSum/100;
    rS2:=Frac(rS1);
    rS3:=rv(rS2*100);
    iSum:=RoundEx(rS3);

//    iSum:=Trunc(rv(Frac(iSum/100))*100);

    iRes := iSum mod iRound;
    rSumD:=iRes/100;
    Result:=rSumCh-rSumD;
  end else
  begin
    iRound:=Trunc(iRound/100)*100; //������ � ����� �����
    iRes := iSum mod iRound;
    rSumD:=iRes/100;
    Result:=rSumCh-rSumD;
  end;
end;


procedure TfmMainRepair.prSaveChSpec(iCh,iDate,iCash:INteger);
{Var sOp,sCassir:String;
    iDep,iZ:Integer;
    CurD,CurT:TDateTime;
    rSum,rSumL:Real;}
begin
  {
  try
    iDep:=0;
    iZ:=0;
    sOp:='';
    sCassir:='';
    rSum:=0;

    CurD:=iDate;
    if ptCQ.Active=False then
    begin
      ptCQ.DatabaseName:=cxTextEdit1.Text;
      ptCQ.Active:=True;
    end;
    ptCQ.CancelRange;
    ptCQ.SetRange([CurD,iCash,iCh],[CurD,iCash,iCh]);
    ptCQ.First;
    while not ptCQ.Eof do
    begin
      iDep:=ptCQGrCode.AsInteger;
      iZ:=ptCQNSmena.AsInteger;
      sOp:=OemToAnsiConvert(ptCQOperation.AsString);
      sCassir:=OemToAnsiConvert(ptCQCassir.AsString);
      CurT:=ptCQTimes.AsDateTime;

      rSum:=rSum+ptCQSumma.AsFloat;

      ptCQ.Next;
    end;

    rSumL:=0;
    teCheck.First;
    while not teCheck.Eof do
    begin
      rSumL:=rSumL+teCheckRSum.AsFloat;

      teCheck.Next;
    end;

    if iDep>0 then
    begin
      if abs(rSumL-rSum)<0.5 then
        prWr('      '+its(iDep)+','+its(iZ)+','+its(iCh)+','+sOp+','+sCassir+', ����� ��')
      else
      begin
        prWr('      ');
        prWr('      '+its(iDep)+','+its(iZ)+','+its(iCh)+','+sOp+','+sCassir+', ������� �����������!!!');
        prWr(' ��� � ����  ');
        prWr('      ');

        ptCQ.First;
        while not ptCQ.Eof do
        begin
          prWr('  ��� '+its(ptCQCode.AsInteger)+' ���-�� '+fs1(ptCQQuant.AsFloat)+' ����� '+fs1(ptCQSumma.AsFloat));

          ptCQ.Next;
        end;
        prWr('      ');
        prWr(' ��� � ���� ');
        prWr('      ');

        teCheck.First;
        while not teCheck.Eof do
        begin
          prWr('  ��� '+its(teCheckArticul.AsInteger)+' ���-�� '+fs1(teCheckQuant.AsFloat)+' ����� '+fs1(teCheckRSum.AsFloat));

          teCheck.Next;
        end;

        prWr('      ');
      end;
      if cxCheckBox1.Checked then
      begin
        //������ �� �������� ����
        ptCQ.First;
        while not ptCQ.Eof do ptCQ.Delete;

        //���� � ����
        teCheck.First;
        while not teCheck.Eof do
        begin
          ptCQ.Append;
          ptCQQuant.AsFloat:=teCheckQuant.AsFloat;
          ptCQOperation.AsString:=AnsiToOemConvert(sOp); //Operation
          ptCQDateOperation.AsDateTime:=CurD;
          ptCQPrice.AsFloat:=RV(teCheckPrice.AsFloat);
          ptCQStore.AsString:='';
          ptCQCk_Number.AsInteger:=iCh;
          ptCQCk_Curs.AsFloat:=0;
          ptCQCk_CurAbr.AsInteger:=0;
          ptCQCk_Disg.AsFloat:=teCheckDProc.AsFloat;
          ptCQCk_Disc.AsFloat:=teCheckDSum.AsFloat;
          ptCQQuant_S.AsFloat:=0;
          ptCQGrCode.AsFloat:=iDep;
          ptCQCode.AsInteger:=teCheckArticul.AsInteger;
          ptCQCassir.AsString:=AnsiToOemConvert(sCassir);
          ptCQCash_Code.AsInteger:=iCash;

          ptCQCk_Card.AsInteger:=0;

          ptCQContr_Code.AsString:='';
          ptCQContr_Cost.AsFloat:=0;
          ptCQSeria.AsString:='';
          ptCQBestB.AsString:='';
          ptCQNDSx1.AsFloat:=0;
          ptCQNDSx2.AsFloat:=0;
          ptCQTimes.AsDateTime:=CurT;
          ptCQSumma.AsFloat:=teCheckRSum.AsFloat;
          ptCQSumNDS.AsFloat:=0;
          ptCQSumNSP.AsFloat:=0;
          ptCQPriceNSP.AsFloat:=0;
          ptCQNSmena.AsInteger:=iZ;
          ptCQ.Post;

          teCheck.Next;
        end;

        prWr('         ���������');

      end;
    end;
  except
    prWr('  ������ ������ � ����� ��������.');
  end;

{
if quCashSailARTICUL.AsString>='0' then
                      begin
                        ptCQ.Append;
                        ptCQQuant.AsFloat:=quCashSailQuantity.AsFloat*k;
                        if k=1 then ptCQOperation.AsString:='P' //Operation
                               else ptCQOperation.AsString:='R';//Operation
                        ptCQDateOperation.AsDateTime:=iCurD;
                        ptCQPrice.AsFloat:=RoundVal(quCashSailPRICERUB.AsFloat);
                        ptCQStore.AsString:='';
                        ptCQCk_Number.AsInteger:=quCashSailCHECKNUMBER.AsInteger;
                        ptCQCk_Curs.AsFloat:=0;
                        ptCQCk_CurAbr.AsInteger:=0;
                        ptCQCk_Disg.AsFloat:=quCashSailDPROC.AsFloat;
                        ptCQCk_Disc.AsFloat:=quCashSailDSUM.AsFloat;
                        ptCQQuant_S.AsFloat:=0;

                        iCurDep:=quCashSailDEPART.AsInteger;
                        if iCurDep=0 then iCurDep:=iMainDep;
                        if iCurDep<>iMainDep then
                        begin
                          if iCurDep<>iAddDep then iCurDep:=iMainDep;
                        end;

                        ptCQGrCode.AsFloat:=iCurDep;
                        ptCQCode.AsInteger:=StrToINtDef(quCashSailARTICUL.AsString,0);
                        ptCQCassir.AsString:=quCashSailCASHER.AsString;
                        ptCQCash_Code.AsInteger:=quCashSailCASHNUMBER.AsInteger;

                        if (quCashSailOPERATION.AsInteger=5)or(quCashSailOPERATION.AsInteger=4)
                          then ptCQCk_Card.AsInteger:=1 //Ck_Card
                          else ptCQCk_Card.AsInteger:=0;

                        ptCQContr_Code.AsString:='';
                        ptCQContr_Cost.AsFloat:=0;
                        ptCQSeria.AsString:='';
                        ptCQBestB.AsString:='';
                        ptCQNDSx1.AsFloat:=0;
                        ptCQNDSx2.AsFloat:=0;
                        ptCQTimes.AsDateTime:=StrToTime(formatDateTime('hh:nn',quCashSailCHDATE.AsDateTime));
                        ptCQSumma.AsFloat:=quCashSailTOTALRUB.AsFloat*k;
                        ptCQSumNDS.AsFloat:=0;
                        ptCQSumNSP.AsFloat:=0;
                        ptCQPriceNSP.AsFloat:=0;
                        ptCQNSmena.AsInteger:=quCashSailZNumber.AsInteger;
                        ptCQ.Post;

                      end else prWH('     ���.  '+quCashSailARTICUL.AsString,Memo1);
}
end;

procedure TfmMainRepair.prSaveCh;
Var iC:INteger;
    DSum,RSum:Real;
begin
  if dbCh.Connected then
  begin
    if (teCheck.Active)and(teCheck.RecordCount>0) then
    begin
{
SHOPINDEX = :ISHOP
and CASHNUMBER = :ICASH
and ZNUMBER = :IZ
and CHECKNUMBER = :CHECK
}
      quCheck.Active:=False;
      quCheck.ParamByName('ISHOP').AsInteger:=1;
      quCheck.ParamByName('ICASH').AsInteger:=cxSpinEdit3.Value;
      quCheck.ParamByName('IZ').AsInteger:=cxSpinEdit2.Value;
      quCheck.ParamByName('CHECK').AsInteger:=teCheckCheckNum.AsInteger;
      quCheck.Active:=True;
      if quCheck.RecordCount=0 then
      begin
        Memo1.Lines.Add('  ����...');

        iC:=0;
        DSum:=0;
        RSum:=0;
        teCheck.First;
        while not teCheck.Eof do
        begin
          quCheck.Append;

          quCheckSHOPINDEX.AsINteger:=1;
          quCheckCASHNUMBER.AsINteger:=cxSpinEdit3.Value;
          quCheckZNUMBER.AsINteger:=cxSpinEdit2.Value;
          quCheckCHECKNUMBER.AsINteger:=teCheckCheckNum.AsInteger;
          quCheckID.AsINteger:=teCheckId.AsInteger;
          quCheckCHDATE.AsDateTime:=Trunc(cxDateEdit1.Date)+0.5; //12:00
          quCheckARTICUL.Asstring:=its(teCheckArticul.AsInteger);
          quCheckBAR.Asstring:=teCheckBar.AsString;
          quCheckCARDSIZE.Asstring:='NOSIZE';
          quCheckQUANTITY.AsFloat:=teCheckQuant.AsFloat;
          quCheckPRICERUB.AsFloat:=teCheckPrice.AsFloat;
          quCheckDPROC.AsFloat:=teCheckDProc.AsFloat;
          quCheckDSUM.AsFloat:=teCheckDSum.AsFloat;
          quCheckTOTALRUB.AsFloat:=teCheckRSum.AsFloat;
          quCheckCASHER.AsINteger:=teCheckCassir.AsInteger;
          quCheckDEPART.AsINteger:=5;
          quCheckOPERATION.AsINteger:=teCheckOperation.AsInteger;

          quCheck.Post;

          inc(iC);

          DSum:=DSum+teCheckDSum.AsFloat;
          RSum:=RSum+teCheckRSum.AsFloat;

          teCheck.Next;
        end;

        quCheckP.Active:=False;
        quCheckP.ParamByName('ISHOP').AsInteger:=1;
        quCheckP.ParamByName('ICASH').AsInteger:=cxSpinEdit3.Value;
        quCheckP.ParamByName('IZ').AsInteger:=cxSpinEdit2.Value;
        quCheckP.ParamByName('CHECK').AsInteger:=teCheckCheckNum.AsInteger;
        quCheckP.Active:=True;

        while quCheckP.RecordCount>0 do quCheckP.Delete;

        quCheckP.Append;
        quCheckPSHOPINDEX.AsInteger:=1;
        quCheckPCASHNUMBER.AsInteger:=cxSpinEdit3.Value;
        quCheckPZNUMBER.AsInteger:=cxSpinEdit2.Value;
        quCheckPCHECKNUMBER.AsInteger:=teCheckCheckNum.AsInteger;
        quCheckPCASHER.AsINteger:=teCheckCassir.AsInteger;
        quCheckPCHECKSUM.AsFloat:=RSum;
        quCheckPPAYSUM.AsFloat:=RSum;
        quCheckPDSUM.AsFloat:=DSum;
        quCheckPDBAR.AsString:='';
        quCheckPCOUNTPOS.AsInteger:=iC;
        quCheckPCHDATE.AsDateTime:=Trunc(cxDateEdit1.Date)+0.5;
        if teCheckOperation.AsInteger=1 then
        begin
          quCheckPPAYMENT.AsInteger:=0;
          quCheckPOPER.AsInteger:=1;
        end;
        if teCheckOperation.AsInteger=5 then
        begin
          quCheckPPAYMENT.AsInteger:=100;
          quCheckPOPER.AsInteger:=5;
        end;
        quCheckP.Post;

        quCheckP.Active:=False;
      end else
      begin
        Memo1.Lines.Add('  ��� � ���� ���� - ������ ����������.');
      end;

      quCheckP.Active:=False;
    end;
  end;
end;

procedure TfmMainRepair.FormCreate(Sender: TObject);
begin
  CurDir:=ExtractFilePath(ParamStr(0));
  Memo1.Clear;
  Memo1.Lines.Add(CurDir);
  cxDateEdit1.Date:=Date-1;
  ReadIni;
  CommonSet.RoundSum:=50;
end;

procedure TfmMainRepair.cxButton3Click(Sender: TObject);
Var fl:TextFile;
    NameF:String;
    StrWk:String;
    StrWk1,StrWk2,StrWk3:String;
    iC:Integer;
    iCode,iPos:Integer;
    DProc,DSum:Real;
    rSumCh:Real;
    rDopDisc:Real;
    iOper:Integer;
begin
  Memo1.Lines.Add('������'); delay(10);
  try
    NameF:=FormatDateTime('yyyy_mm_dd',cxDateEdit1.Date)+'.log';
    if FileExists(CurDir+NameF) then
    begin
      Memo1.Lines.Add('  ���� - '+CurDir+NameF+' ������ . ���� ������.'); delay(10);
      try
        assignfile(fl,CurDir+NameF);
        Reset(Fl);

        teCheck.Active:=False;
        teCheck.Active:=True;

        Memo1.Lines.Add('   ������� ..'); delay(10);

        iC:=1;
        while not EOF(Fl) do
        begin
          ReadLn(Fl,StrWk);
//~~FisPos;344;
//20:10:59 ;~~FisPos;344;7756;46084675;1;36,2;
//20:10:55 ;~!Disc;344;13188;0;0;28;
          if pos('~~FisPos;'+its(cxSpinEdit1.Value)+';',StrWk)>0 then
          begin    //������� ������� ���� - �������� � �������
            teCheck.Append;

            teCheckCashNum.AsInteger:=cxSpinEdit3.Value;
            teCheckCheckNum.AsInteger:=cxSpinEdit1.Value;
            teCheckId.AsInteger:=iC;

            StrWk1:=StrWk;
            Delete(StrWk1,1,pos('~~FisPos;',StrWk1)+8);
            StrWk2:=Copy(StrWk1,1,pos(';',StrWk1)-1);  //����� ����
            StrWk3:=Copy(StrWk1,1,pos(';',StrWk1)-1);

            Delete(StrWk1,1,pos(';',StrWk1));
            StrWk2:=StrWk2+' '+Copy(StrWk1,1,pos(';',StrWk1)-1);  //��� ������
            StrWk3:=Copy(StrWk1,1,pos(';',StrWk1)-1);  //��� ������

            teCheckArticul.AsInteger:=StrToIntDef(StrWk3,0);

            Delete(StrWk1,1,pos(';',StrWk1));
            StrWk2:=StrWk2+' '+Copy(StrWk1,1,pos(';',StrWk1)-1);  //��������
            StrWk3:=Copy(StrWk1,1,pos(';',StrWk1)-1);  //��������

            teCheckBar.AsString:=StrWk3;

            Delete(StrWk1,1,pos(';',StrWk1));
            StrWk2:=StrWk2+' '+Copy(StrWk1,1,pos(';',StrWk1)-1);  //�����
            StrWk3:=Copy(StrWk1,1,pos(';',StrWk1)-1);  //�����

            while pos('.',StrWk3)>0 do StrWk3[pos('.',StrWk3)]:=',';

            teCheckQuant.AsFloat:=S2f(StrWk3);

            Delete(StrWk1,1,pos(';',StrWk1));
            StrWk2:=StrWk2+' '+Copy(StrWk1,1,pos(';',StrWk1)-1);  //����
            StrWk3:=Copy(StrWk1,1,pos(';',StrWk1)-1);  //����
//            while pos(',',StrWk3)>0 do StrWk3[pos(',',StrWk3)]:='.';

            teCheckPrice.AsFloat:=S2f(StrWk3);
            teCheckDProc.AsFloat:=0;
            teCheckDSum.AsFloat:=0;
            teCheckRSum.AsFloat:=rv(teCheckPrice.AsFloat*teCheckQuant.AsFloat);
            teCheckCassir.AsInteger:=cxSpinEdit4.Value;

            if cxComboBox1.ItemIndex=0 then teCheckOperation.AsInteger:=1; //5 - ������   1- ���
            if cxComboBox1.ItemIndex=1 then teCheckOperation.AsInteger:=5; //5 - ������   1- ���

            teCheck.Post;

            Memo1.Lines.Add('    '+StrWk2); delay(10);
            inc(iC);

          end;
{
          if iOper=-1 then    //��� ������ ��������� � ����
          begin
            if pos('$!MoneyBn;',StrWk)>0 then iOper:=5;
            if pos('$!Money;',StrWk)>0 then iOper:=1;
          end;}
        end;

        Memo1.Lines.Add('   ������ ..'); delay(10);

        ViewCh.BeginUpdate;
        CloseFile(Fl);

        Reset(Fl);
        while not EOF(Fl) do
        begin
          ReadLn(Fl,StrWk);
          StrWk1:=StrWk;

//~~FisPos;344;
//20:10:59 ;~~FisPos;344;7756;46084675;1;36,2;
//20:10:55 ;~!Disc;344;13188;0;0;28;
          while pos('~!Disc;'+its(cxSpinEdit1.Value)+';',StrWk1)>0 do
          begin    //������� ������� ���� - �������� � �������
            Delete(StrWk1,1,pos('~!Disc;',StrWk1)+7);
            StrWk2:=Copy(StrWk1,1,pos(';',StrWk1)-1); //����� ����
            StrWk3:=Copy(StrWk1,1,pos(';',StrWk1)-1);

            Delete(StrWk1,1,pos(';',StrWk1));
            StrWk2:=StrWk2+' '+Copy(StrWk1,1,pos(';',StrWk1)-1);  //��� ������
            StrWk3:=Copy(StrWk1,1,pos(';',StrWk1)-1);  //��� ������
            iCode:=StrToIntDef(StrWk3,0);

            Delete(StrWk1,1,pos(';',StrWk1));
            StrWk2:=StrWk2+' '+Copy(StrWk1,1,pos(';',StrWk1)-1);
            StrWk3:=Copy(StrWk1,1,pos(';',StrWk1)-1);  //������� ������
            DSum:=S2f(StrWk3);

            Delete(StrWk1,1,pos(';',StrWk1));
            StrWk2:=StrWk2+' '+Copy(StrWk1,1,pos(';',StrWk1)-1);  // ����� ������
            StrWk3:=Copy(StrWk1,1,pos(';',StrWk1)-1);
            DProc:=S2f(StrWk3);

            Delete(StrWk1,1,pos(';',StrWk1));
            StrWk2:=StrWk2+' '+Copy(StrWk1,1,pos(';',StrWk1)-1);  // �������
            StrWk3:=Copy(StrWk1,1,pos(';',StrWk1)-1);
            iPos:=StrToIntDef(StrWk3,0);

//    iCode,iPos:Integer;
//    DProc,DSum:Real;

//            Memo1.Lines.Add('    '+StrWk2); delay(10);
            if teCheck.Locate('Id',iPos,[]) then
            begin
              if iCode=teCheckArticul.AsInteger then
              begin
                teCheck.Edit;
                teCheckDProc.AsFloat:=DProc;
                teCheckDSum.AsFloat:=DSum;
                if cxCheckBox2.Checked=False then teCheckRSum.AsFloat:=rv(teCheckPrice.AsFloat*teCheckQuant.AsFloat)-DSum;
                teCheck.Post;
              end else begin Memo1.Lines.Add('   err '+StrWk1); delay(10); end;
            end else begin Memo1.Lines.Add('   err '+StrWk1); delay(10); end;
          end;
        end;

        rSumCh:=0; rDopDisc:=0;

        Memo1.Lines.Add('   ������ ..'); delay(10);

//        ViewCh.BeginUpdate;
//        CloseFile(Fl);

        Memo1.Lines.Add('   �������� ..'); delay(10);

        iOper:=-2; //�������� �� ����������     -2 ����� -1 ����� ��� ������ �������� ��������

        Reset(Fl);
        while not EOF(Fl) do
        begin
          ReadLn(Fl,StrWk);
          StrWk1:=StrWk;
          if pos('~!BegCheck;'+its(cxSpinEdit1.Value)+';',StrWk1)>0 then iOper:=-1; //����� ������ ���

          if iOper=-1 then    //��� ������ ��������� � ����
          begin
            if pos('$!MoneyBn;',StrWk)>0 then begin iOper:=5; Break; end;
            if pos('$!Money;',StrWk)>0 then begin iOper:=1; Break; end;
          end;
        end;


        teCheck.First;
        while not teCheck.Eof do
        begin
          rSumCh:=rSumCh+teCheckRSum.AsFloat;

          if iOper>0 then
          begin
            teCheck.Edit;
            teCheckOperation.AsInteger:=iOper;
            teCheck.Post;
          end;

          teCheck.Next;
        end;

        Memo1.Lines.Add('  ����� ���� - '+fs(rSumCh)); delay(10);

        if iOper=1 then
        begin
          rSumCh:=RoundSpec(rSumCh,rDopDisc);
          if rDopDisc>0 then
          begin
            teCheck.First;
            while not teCheck.Eof do
            begin
              if teCheckRSum.AsFloat>rDopDisc then
              begin
                teCheck.Edit;
                teCheckRSum.AsFloat:=teCheckRSum.AsFloat-rDopDisc;
                teCheck.Post;

                break;
              end;
              teCheck.Next;
            end;
          end;
          Memo1.Lines.Add('  ���.������ - '+fs(rDopDisc)); delay(10);
        end;

      finally
        CloseFile(Fl);
        ViewCh.EndUpdate;

//        cxSpinEdit1.Value:=cxSpinEdit1.Value+1;
        cxComboBox1.ItemIndex:=0;
      end;
    end else
    begin
      Memo1.Lines.Add('  �� ������ ���� - '+CurDir+NameF); delay(10);
    end;
  finally
    Memo1.Lines.Add('�����'); delay(10);
  end;
end;

procedure TfmMainRepair.cxButton2Click(Sender: TObject);
begin
  dbCh.Close;
  Close;
end;

procedure TfmMainRepair.cxButton1Click(Sender: TObject);
begin
  dbCh.Close;
  dbCh.DatabaseName:=cxButtonEdit1.Text;
  try
    dbCh.Open;
    Memo1.Lines.Add('');
    Memo1.Lines.Add('���� ������� ��');
  except
    Memo1.Lines.Add('������ �������� ��');
  end;
end;

procedure TfmMainRepair.cxButton4Click(Sender: TObject);
Var iC:INteger;
    DSum,RSum:Real;
begin
  Memo1.Lines.Add('');
  Memo1.Lines.Add('���� ���.');
  if dbCh.Connected then
  begin
    if teCheck.Active then
    begin
{
SHOPINDEX = :ISHOP
and CASHNUMBER = :ICASH
and ZNUMBER = :IZ
and CHECKNUMBER = :CHECK
}
      quCheck.Active:=False;
      quCheck.ParamByName('ISHOP').AsInteger:=1;
      quCheck.ParamByName('ICASH').AsInteger:=cxSpinEdit3.Value;
      quCheck.ParamByName('IZ').AsInteger:=cxSpinEdit2.Value;
      quCheck.ParamByName('CHECK').AsInteger:=cxSpinEdit1.Value;
      quCheck.Active:=True;

      Memo1.Lines.Add('  ������...');
      while quCheck.RecordCount>0 do quCheck.Delete;

      if quCheck.RecordCount=0 then
      begin
        Memo1.Lines.Add('  ����...');

        iC:=0;
        DSum:=0;
        RSum:=0;
        teCheck.First;
        while not teCheck.Eof do
        begin
          quCheck.Append;

          quCheckSHOPINDEX.AsINteger:=1;
          quCheckCASHNUMBER.AsINteger:=cxSpinEdit3.Value;
          quCheckZNUMBER.AsINteger:=cxSpinEdit2.Value;
          quCheckCHECKNUMBER.AsINteger:=cxSpinEdit1.Value;
          quCheckID.AsINteger:=teCheckId.AsInteger;
          quCheckCHDATE.AsDateTime:=Trunc(cxDateEdit1.Date)+0.5; //12:00
          quCheckARTICUL.Asstring:=its(teCheckArticul.AsInteger);
          quCheckBAR.Asstring:=teCheckBar.AsString;
          quCheckCARDSIZE.Asstring:='NOSIZE';
          quCheckQUANTITY.AsFloat:=teCheckQuant.AsFloat;
          quCheckPRICERUB.AsFloat:=teCheckPrice.AsFloat;
          quCheckDPROC.AsFloat:=teCheckDProc.AsFloat;
          quCheckDSUM.AsFloat:=teCheckDSum.AsFloat;
          quCheckTOTALRUB.AsFloat:=teCheckRSum.AsFloat;
          quCheckCASHER.AsINteger:=teCheckCassir.AsInteger;
          quCheckDEPART.AsINteger:=5;
          quCheckOPERATION.AsINteger:=teCheckOperation.AsInteger;

          quCheck.Post;

          inc(iC);

          DSum:=DSum+teCheckDSum.AsFloat;
          RSum:=RSum+teCheckRSum.AsFloat;

          teCheck.Next;
        end;

        quCheckP.Active:=False;
        quCheckP.ParamByName('ISHOP').AsInteger:=1;
        quCheckP.ParamByName('ICASH').AsInteger:=cxSpinEdit3.Value;
        quCheckP.ParamByName('IZ').AsInteger:=cxSpinEdit2.Value;
        quCheckP.ParamByName('CHECK').AsInteger:=cxSpinEdit1.Value;
        quCheckP.Active:=True;

        while quCheckP.RecordCount>0 do quCheckP.Delete;

        quCheckP.Append;
        quCheckPSHOPINDEX.AsInteger:=1;
        quCheckPCASHNUMBER.AsInteger:=cxSpinEdit3.Value;
        quCheckPZNUMBER.AsInteger:=cxSpinEdit2.Value;
        quCheckPCHECKNUMBER.AsInteger:=cxSpinEdit1.Value;
        quCheckPCASHER.AsINteger:=teCheckCassir.AsInteger;
        quCheckPCHECKSUM.AsFloat:=RSum;
        quCheckPPAYSUM.AsFloat:=RSum;
        quCheckPDSUM.AsFloat:=DSum;
        quCheckPDBAR.AsString:='';
        quCheckPCOUNTPOS.AsInteger:=iC;
        quCheckPCHDATE.AsDateTime:=Trunc(cxDateEdit1.Date)+0.5;
        if teCheckOperation.AsInteger=1 then
        begin
          quCheckPPAYMENT.AsInteger:=0;
          quCheckPOPER.AsInteger:=1;
        end;
        if teCheckOperation.AsInteger=5 then
        begin
          quCheckPPAYMENT.AsInteger:=100;
          quCheckPOPER.AsInteger:=5;
        end;
        quCheckP.Post;

        quCheckP.Active:=False;

        cxSpinEdit1.EditValue:=cxSpinEdit1.EditValue+1;
      end else
      begin
        Memo1.Lines.Add('  ��� � ���� ���� - ������ ����������.');
      end;
      quCheckP.Active:=False;

    end;
  end;
  Memo1.Lines.Add('�����.');
end;

procedure TfmMainRepair.Excel1Click(Sender: TObject);
begin
//������� � ������
  prNExportExel5(ViewChSum);

end;

procedure TfmMainRepair.cxButton5Click(Sender: TObject);
Var fl:TextFile;
    NameF:String;
    StrWk:String;
    StrWk1,StrWk2,StrWk3:String;
    iC:Integer;
    iCode,iPos:Integer;
    DProc,DSum:Real;
    iCurCh,iCh,iOp:INteger;
    iDPos,iDCode:Integer;
//    iChMin,iChMax,iCh:INteger;
begin
  //������������ �� ����� ����
  if MessageDlg('�� ������� ��� ����� ������� �������������� ������ �� ����� ����?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
    if MessageDlg(' ���������� �������?', mtConfirmation, [mbYes, mbNo],0) <> mrYes then
    begin
      // �������
      Memo1.Lines.Add('������'); delay(10);
      try
        NameF:=FormatDateTime('yyyy_mm_dd',cxDateEdit1.Date)+'.log';
        if FileExists(CurDir+NameF) then
        begin
          Memo1.Lines.Add('  ���� - '+CurDir+NameF+' ������ . ���� ������.'); delay(10);
          try
            ViewCh.BeginUpdate;

            assignfile(fl,CurDir+NameF);
            Reset(Fl);

            Memo1.Lines.Add('   ������� �����.'); delay(10);

//            iChMin:=1500;
//            iChMax:=0;
            iCurCh:=0;
            iOp:=1; //1- ���,  5 - ������
            iC:=1;

            teCheck.Active:=False;
            teCheck.Active:=True;

            while not EOF(Fl) do
            begin
              ReadLn(Fl,StrWk);
//08:14:54 ;~!BegCheck;2;
              if pos('~!BegCheck;',StrWk)>0 then
              begin
                StrWk1:=StrWk;
                Delete(StrWk1,1,pos('~!BegCheck;',StrWk1)+10);
                StrWk2:=Copy(StrWk1,1,pos(';',StrWk1)-1);  //����� ����
                iCh:=StrToIntDef(StrWk2,0);
                if iCh>iCurCh then
                begin
                  //����� ���� - ���������� � ��
                  if iCurCh>0 then
                  begin //��������� ��� � ����
                    prSaveCh;
                  end;
                  iCurCh:=iCh;
                  iOp:=1; //��� �� ���������
                  iC:=1;
                  iDPos:=0;
                  iDCode:=0;

                  Memo1.Lines.Add('   ��� '+its(iCh)); delay(10);

                  //������ ������� ���
                  teCheck.Active:=False;
                  teCheck.Active:=True;
                  teCheck.First; while not teCheck.Eof do teCheck.Delete;

                  delay(10);

                end else //����� ������ ���� �� ��������� - ���� ������, ���� �������������
                begin
                  //������ ������� ���
                  teCheck.Active:=False;
                  teCheck.Active:=True;
                  teCheck.First; while not teCheck.Eof do teCheck.Delete;
                  iC:=1;

                  iDPos:=0;
                  iDCode:=0;

                  delay(10);
                end;
              end;
              if iCurCh>0 then //������� ��� ��������� - ��������� ������������
              begin
//~~FisPos;344;
//20:10:59 ;~~FisPos;344;7756;46084675;1;36,2;
//20:10:55 ;~!Disc;344;13188;0;0;28;

                if pos('!MoneyBn;',StrWk)>0 then
                begin
                  iOp:=5;
                  teCheck.First;
                  while not teCheck.Eof do
                  begin
                    teCheck.Edit;
                    teCheckOperation.AsInteger:=iOp;
                    teCheck.Post;

                    teCheck.Next;
                  end;
                end;


                if pos('~~FisPos;'+its(iCurCh)+';',StrWk)>0 then
                begin    //������� ������� ���� - �������� � �������
                  teCheck.Append;

                  teCheckCashNum.AsInteger:=cxSpinEdit3.Value;
                  teCheckCheckNum.AsInteger:=iCurCh;
                  teCheckId.AsInteger:=iC;

                  StrWk1:=StrWk;
                  Delete(StrWk1,1,pos('~~FisPos;',StrWk1)+8);
                  StrWk2:=Copy(StrWk1,1,pos(';',StrWk1)-1);  //����� ����
                  StrWk3:=Copy(StrWk1,1,pos(';',StrWk1)-1);

                  Delete(StrWk1,1,pos(';',StrWk1));
                  StrWk2:=StrWk2+' '+Copy(StrWk1,1,pos(';',StrWk1)-1);  //��� ������
                  StrWk3:=Copy(StrWk1,1,pos(';',StrWk1)-1);  //��� ������

                  teCheckArticul.AsInteger:=StrToIntDef(StrWk3,0);

                  Delete(StrWk1,1,pos(';',StrWk1));
                  StrWk2:=StrWk2+' '+Copy(StrWk1,1,pos(';',StrWk1)-1);  //��������
                  StrWk3:=Copy(StrWk1,1,pos(';',StrWk1)-1);  //��������

                  teCheckBar.AsString:=StrWk3;

                  Delete(StrWk1,1,pos(';',StrWk1));
                  StrWk2:=StrWk2+' '+Copy(StrWk1,1,pos(';',StrWk1)-1);  //�����
                  StrWk3:=Copy(StrWk1,1,pos(';',StrWk1)-1);  //�����
//                while pos(',',StrWk3)>0 do StrWk3[pos(',',StrWk3)]:='.';

                  while pos('.',StrWk3)>0 do StrWk3[pos('.',StrWk3)]:=',';

                  teCheckQuant.AsFloat:=S2f(StrWk3);

                  Delete(StrWk1,1,pos(';',StrWk1));
                  StrWk2:=StrWk2+' '+Copy(StrWk1,1,pos(';',StrWk1)-1);  //����
                  StrWk3:=Copy(StrWk1,1,pos(';',StrWk1)-1);  //����
//                while pos(',',StrWk3)>0 do StrWk3[pos(',',StrWk3)]:='.';

                  teCheckPrice.AsFloat:=S2f(StrWk3);

                  teCheckDProc.AsFloat:=0;
                  teCheckDSum.AsFloat:=0;
                  teCheckRSum.AsFloat:=rv(teCheckPrice.AsFloat*teCheckQuant.AsFloat);

                  if iDPos=iC then
                  begin
                    if iDCode=teCheckArticul.AsInteger then
                    begin
                      teCheckDProc.AsFloat:=DProc;
                      teCheckDSum.AsFloat:=DSum;
                      teCheckRSum.AsFloat:=rv(teCheckPrice.AsFloat*teCheckQuant.AsFloat)-DSum;

                      Memo1.Lines.Add('   last disc '+its(iDPos)); delay(10);
                    end;
                  end;

                  teCheckCassir.AsInteger:=cxSpinEdit4.Value;
                  teCheckOperation.AsInteger:=iOp; //5 - ������   1- ���
                  teCheck.Post;

                  Memo1.Lines.Add('    '+StrWk2); delay(10);

                  inc(iC);
                end;

                StrWk1:=StrWk;

                while pos('~!Disc;'+its(iCurCh)+';',StrWk1)>0 do
                begin    //������� ������� ���� - �������� � �������
                  Delete(StrWk1,1,pos('~!Disc;',StrWk1)+7);
                  StrWk2:=Copy(StrWk1,1,pos(';',StrWk1)-1); //����� ����
                  StrWk3:=Copy(StrWk1,1,pos(';',StrWk1)-1);

                  Delete(StrWk1,1,pos(';',StrWk1));
                  StrWk2:=StrWk2+' '+Copy(StrWk1,1,pos(';',StrWk1)-1);  //��� ������
                  StrWk3:=Copy(StrWk1,1,pos(';',StrWk1)-1);  //��� ������
                  iCode:=StrToIntDef(StrWk3,0);

                  Delete(StrWk1,1,pos(';',StrWk1));
                  StrWk2:=StrWk2+' '+Copy(StrWk1,1,pos(';',StrWk1)-1);
                  StrWk3:=Copy(StrWk1,1,pos(';',StrWk1)-1);  //������� ������
                  DSum:=S2f(StrWk3);

                  Delete(StrWk1,1,pos(';',StrWk1));
                  StrWk2:=StrWk2+' '+Copy(StrWk1,1,pos(';',StrWk1)-1);  // ����� ������
                  StrWk3:=Copy(StrWk1,1,pos(';',StrWk1)-1);
                  DProc:=S2f(StrWk3);

                  Delete(StrWk1,1,pos(';',StrWk1));
                  StrWk2:=StrWk2+' '+Copy(StrWk1,1,pos(';',StrWk1)-1);  // �������
                  StrWk3:=Copy(StrWk1,1,pos(';',StrWk1)-1);
                  iPos:=StrToIntDef(StrWk3,0);

//    iCode,iPos:Integer;
//    DProc,DSum:Real;

//            Memo1.Lines.Add('    '+StrWk2); delay(10);

                  if teCheck.Locate('Id',iPos,[]) then
                  begin
                    if iCode=teCheckArticul.AsInteger then
                    begin
                      teCheck.Edit;
                      teCheckDProc.AsFloat:=DProc;
                      teCheckDSum.AsFloat:=DSum;
                      teCheckRSum.AsFloat:=rv(teCheckPrice.AsFloat*teCheckQuant.AsFloat)-DSum;
                      teCheck.Post;
                    end else begin Memo1.Lines.Add('   err '+StrWk1); delay(10); end;
                  end else
                  begin
                    iDPos:=iPos;
                    iDCode:=iCode;
//                    Memo1.Lines.Add('   err '+StrWk1);
                    delay(10);
                  end;
                end;
              end;
            end;

            if teCheck.RecordCount>0 then
            begin //��������� ��������� ���
              prSaveCh;
            end;

          finally
            CloseFile(Fl);
            ViewCh.EndUpdate;
          end;
        end;
      finally
        Memo1.Lines.Add('�����'); delay(10);
      end;
    end;
  end;
end;

procedure TfmMainRepair.cxButton6Click(Sender: TObject);
Var n:INteger;
    sdir:String;
    CurD,MaxD:TDateTime;

    fl:TextFile;
    NameF:String;
    StrWk:String;
    StrWk1,StrWk2,StrWk3:String;
    iC:Integer;
    iCode,iPos:Integer;
    DProc,DSum:Real;
    iCurCh,iCh,iOp:INteger;
    iDPos,iDCode:Integer;

    bNeedS:Boolean;

begin
  //����������� ��������������
  //��������� ����� � 01 �� 20 � ��� ���� � 01.04.13 �� 10.04.13
  Memo1.Clear;
  prWr('������ ����������� ��������.');
  for n:=1 to 20 do
  begin
    sDir:=its(n);
    while length(sDir)<2 do sDir:='0'+sDir;

    if DirectoryExists(CurDir+sDir) then
    begin
      prWr(' ');
      prWr('  ����������� ����� '+its(n)+' �������. ���� ���������.');
      CurD:=StrToDate('01.04.2013');
      MaxD:=CurD+10;
      while CurD<MaxD do
      begin
        if FileExists(CurDir+sDir+'\'+FormatDateTime('yyyy_mm_dd',CurD)+'.log') then
        begin
          prWr('     ���� '+CurDir+sDir+'\'+FormatDateTime('yyyy_mm_dd',CurD)+'.log'+' ������. ������������.');
          //!!prAddPosSel;1;

          try
            ViewCh.BeginUpdate;

            NameF:=sDir+'\'+FormatDateTime('yyyy_mm_dd',CurD)+'.log';

            assignfile(fl,CurDir+NameF);
            Reset(Fl);

//            iChMin:=1500;
//            iChMax:=0;
            iCurCh:=0;
            iOp:=1; //1- ���,  5 - ������
            iC:=1;
            bNeedS:=False;

            teCheck.Active:=False;
            teCheck.Active:=True;

            while not EOF(Fl) do
            begin
              ReadLn(Fl,StrWk);
//08:14:54 ;~!BegCheck;2;
              if pos('~!BegCheck;',StrWk)>0 then
              begin
                StrWk1:=StrWk;
                Delete(StrWk1,1,pos('~!BegCheck;',StrWk1)+10);
                StrWk2:=Copy(StrWk1,1,pos(';',StrWk1)-1);  //����� ����
                iCh:=StrToIntDef(StrWk2,0);
                if iCh>iCurCh then
                begin
                  //����� ���� - ���������� � ��
                  if iCurCh>0 then
                  begin //��������� ��� � ����
                    if bNeedS then prSaveChSpec(iCurCh,Trunc(CurD),n);
                  end;
                  iCurCh:=iCh;
                  iOp:=1; //��� �� ���������
                  iC:=1;
                  iDPos:=0;
                  iDCode:=0;

//                  Memo1.Lines.Add('   ��� '+its(iCh)); delay(10);

                  //������ ������� ���
                  teCheck.Active:=False;
                  teCheck.Active:=True;
                  teCheck.First; while not teCheck.Eof do teCheck.Delete;

                  bNeedS:=False;

                  delay(10);
                end else //����� ������ ���� �� ��������� - ���� ������, ���� �������������
                begin
                  //������ ������� ���
                  teCheck.Active:=False;
                  teCheck.Active:=True;
                  teCheck.First; while not teCheck.Eof do teCheck.Delete;

                  bNeedS:=False;

                  iC:=1;

                  iDPos:=0;
                  iDCode:=0;

                  delay(10);
                end;
              end;
              if iCurCh>0 then //������� ��� ��������� - ��������� ������������
              begin

//~~FisPos;344;
//20:10:59 ;~~FisPos;344;7756;46084675;1;36,2;
//20:10:55 ;~!Disc;344;13188;0;0;28;

                if pos('!MoneyBn;',StrWk)>0 then
                begin
                  iOp:=5;
                  teCheck.First;
                  while not teCheck.Eof do
                  begin
                    teCheck.Edit;
                    teCheckOperation.AsInteger:=iOp;
                    teCheck.Post;

                    teCheck.Next;
                  end;
                end;


                if pos('~~FisPos;'+its(iCurCh)+';',StrWk)>0 then
                begin    //������� ������� ���� - �������� � �������
                  teCheck.Append;

                  teCheckCashNum.AsInteger:=cxSpinEdit3.Value;
                  teCheckCheckNum.AsInteger:=iCurCh;
                  teCheckId.AsInteger:=iC;

                  StrWk1:=StrWk;
                  Delete(StrWk1,1,pos('~~FisPos;',StrWk1)+8);
                  StrWk2:=Copy(StrWk1,1,pos(';',StrWk1)-1);  //����� ����
                  StrWk3:=Copy(StrWk1,1,pos(';',StrWk1)-1);

                  Delete(StrWk1,1,pos(';',StrWk1));
                  StrWk2:=StrWk2+' '+Copy(StrWk1,1,pos(';',StrWk1)-1);  //��� ������
                  StrWk3:=Copy(StrWk1,1,pos(';',StrWk1)-1);  //��� ������

                  teCheckArticul.AsInteger:=StrToIntDef(StrWk3,0);

                  Delete(StrWk1,1,pos(';',StrWk1));
                  StrWk2:=StrWk2+' '+Copy(StrWk1,1,pos(';',StrWk1)-1);  //��������
                  StrWk3:=Copy(StrWk1,1,pos(';',StrWk1)-1);  //��������

                  teCheckBar.AsString:=StrWk3;

                  Delete(StrWk1,1,pos(';',StrWk1));
                  StrWk2:=StrWk2+' '+Copy(StrWk1,1,pos(';',StrWk1)-1);  //�����
                  StrWk3:=Copy(StrWk1,1,pos(';',StrWk1)-1);  //�����
//                  while pos(',',StrWk3)>0 do StrWk3[pos(',',StrWk3)]:='.';

                  while pos('.',StrWk3)>0 do StrWk3[pos('.',StrWk3)]:=',';

                  teCheckQuant.AsFloat:=S2f(StrWk3);

                  Delete(StrWk1,1,pos(';',StrWk1));
                  StrWk2:=StrWk2+' '+Copy(StrWk1,1,pos(';',StrWk1)-1);  //����
                  StrWk3:=Copy(StrWk1,1,pos(';',StrWk1)-1);  //����
//                  while pos(',',StrWk3)>0 do StrWk3[pos(',',StrWk3)]:='.';

                  teCheckPrice.AsFloat:=S2f(StrWk3);

                  teCheckDProc.AsFloat:=0;
                  teCheckDSum.AsFloat:=0;
                  teCheckRSum.AsFloat:=rv(teCheckPrice.AsFloat*teCheckQuant.AsFloat);

                  if iDPos=iC then
                  begin
                    if iDCode=teCheckArticul.AsInteger then
                    begin
                      teCheckDProc.AsFloat:=DProc;
                      teCheckDSum.AsFloat:=DSum;
                      teCheckRSum.AsFloat:=rv(teCheckPrice.AsFloat*teCheckQuant.AsFloat)-DSum;

//                      Memo1.Lines.Add('   last disc '+its(iDPos)); delay(10);
                    end;
                  end;

                  teCheckCassir.AsInteger:=cxSpinEdit4.Value;
                  teCheckOperation.AsInteger:=iOp; //5 - ������   1- ���
                  teCheck.Post;

                  if teCheckQuant.AsFloat<0 then bNeedS:=True; //� ���� ���� ���� �������� �������

//                  Memo1.Lines.Add('    '+StrWk2); delay(10);

                  inc(iC);
                end;

                if pos('~!Disc;'+its(iCurCh)+';',StrWk)>0 then
                begin    //������� ������� ���� - �������� � �������
                  StrWk1:=StrWk;
                  Delete(StrWk1,1,pos('~!Disc;',StrWk1)+7);
                  StrWk2:=Copy(StrWk1,1,pos(';',StrWk1)-1); //����� ����
                  StrWk3:=Copy(StrWk1,1,pos(';',StrWk1)-1);

                  Delete(StrWk1,1,pos(';',StrWk1));
                  StrWk2:=StrWk2+' '+Copy(StrWk1,1,pos(';',StrWk1)-1);  //��� ������
                  StrWk3:=Copy(StrWk1,1,pos(';',StrWk1)-1);  //��� ������
                  iCode:=StrToIntDef(StrWk3,0);

                  Delete(StrWk1,1,pos(';',StrWk1));
                  StrWk2:=StrWk2+' '+Copy(StrWk1,1,pos(';',StrWk1)-1);
                  StrWk3:=Copy(StrWk1,1,pos(';',StrWk1)-1);  //������� ������
                  DSum:=S2f(StrWk3);

                  Delete(StrWk1,1,pos(';',StrWk1));
                  StrWk2:=StrWk2+' '+Copy(StrWk1,1,pos(';',StrWk1)-1);  // ����� ������
                  StrWk3:=Copy(StrWk1,1,pos(';',StrWk1)-1);
                  DProc:=S2f(StrWk3);

                  Delete(StrWk1,1,pos(';',StrWk1));
                  StrWk2:=StrWk2+' '+Copy(StrWk1,1,pos(';',StrWk1)-1);  // �������
                  StrWk3:=Copy(StrWk1,1,pos(';',StrWk1)-1);
                  iPos:=StrToIntDef(StrWk3,0);

//    iCode,iPos:Integer;
//    DProc,DSum:Real;

//            Memo1.Lines.Add('    '+StrWk2); delay(10);

                  if teCheck.Locate('Id',iPos,[]) then
                  begin
                    if iCode=teCheckArticul.AsInteger then
                    begin
                      teCheck.Edit;
                      teCheckDProc.AsFloat:=DProc;
                      teCheckDSum.AsFloat:=DSum;
                      teCheckRSum.AsFloat:=rv(teCheckPrice.AsFloat*teCheckQuant.AsFloat)-DSum;
                      teCheck.Post;
                    end else
                    begin
                    //  Memo1.Lines.Add('   err '+StrWk1);
                      delay(10);
                    end;
                  end else
                  begin
                    iDPos:=iPos;
                    iDCode:=iCode;
//                    Memo1.Lines.Add('   err '+StrWk1);
                    delay(10);
                  end;
                end;

                if pos('!!prAddPosSel;1;',StrWk)>0 then bNeedS:=True; //� ���� ���� ���� �������� �������

              end;
            end;

            if teCheck.RecordCount>0 then
            begin //��������� ��������� ���
              if bNeedS then prSaveChSpec(iCurCh,Trunc(CurD),n);
            end;

          finally
            CloseFile(Fl);
            ViewCh.EndUpdate;
          end;

        end else  prWr('     ���� ���� '+CurDir+sDir+'\'+FormatDateTime('yyyy_mm_dd',CurD)+'.log'+' �� ������.');
        CurD:=CurD+1;
      end;
      prWr('  ����� '+its(n)+'. ��������� ��.');
    end;
  end;

  prWr('����������� �������� ���������.');
end;

procedure TfmMainRepair.cxButton7Click(Sender: TObject);
Var fl:TextFile;
    NameF:String;
    StrWk:String;
    StrWk1,StrWk2,StrWk3:String;
    iC,iCurC:Integer;
begin
  Memo1.Lines.Add('������'); delay(10);
  try
    NameF:=FormatDateTime('yyyy_mm_dd',cxDateEdit1.Date)+'.log';
    if FileExists(CurDir+NameF) then
    begin
      Memo1.Lines.Add('  ���� - '+CurDir+NameF+' ������ . ���� ������.'); delay(10);
      try
        assignfile(fl,CurDir+NameF);
        Reset(Fl);

        teCh.Active:=False;
        teCh.Active:=True;

        Memo1.Lines.Add('   ������� ..'); delay(10);

        iC:=0; iCurC:=0;
        while not EOF(Fl) do
        begin
          ReadLn(Fl,StrWk);
//~!BegCheck;2;
//_!CheckClose;
          if pos('~!BegCheck;',StrWk)>0 then
          begin //������ ����
            StrWk1:=StrWk;
            delete(StrWk1,1,pos('~!BegCheck;',StrWk1)+10);
            if pos(';',StrWk1)>0 then
            begin
              StrWk2:=Copy(StrWk1,1,pos(';',StrWk1)-1);
              iCurC:=StrToINtDef(StrWk2,0);
              if iCurC>0 then //������ ���� ������������
              begin
                teCh.Append;
                teChNum.AsInteger:=iCurC;
                teCh.Post;
              end;
            end;
          end;
          if pos('_!CheckClose;',StrWk)>0 then
          begin //����� ����
            if iCurC>0 then
            begin
              if teCh.Locate('Num',iCurC,[]) then teCh.Delete;
            end;
          end;
        end;
      finally
        CloseFile(Fl);
      end;
    end else
    begin
      Memo1.Lines.Add('  �� ������ ���� - '+CurDir+NameF); delay(10);
    end;
  finally
    Memo1.Lines.Add('�����'); delay(10);
  end;
end;

procedure TfmMainRepair.ViChDblClick(Sender: TObject);
begin
  if teCh.RecordCount>0 then cxSpinEdit1.Value:=teChNum.AsInteger;
 end;

procedure TfmMainRepair.cxButton8Click(Sender: TObject);
Var n_ch,n_chmax:Integer;
    rSum,rSumCh:Real;
begin
  if dbCh.Connected then
  begin
    try
      cxButton8.Enabled:=False;
      bStopCompare:=False;

      teChSum.Active:=False;
      teChSum.Active:=True;

      quChMax.Active:=False;
      quChMax.SelectSQL.Clear;
      quChMax.SelectSQL.Add('');
      quChMax.SelectSQL.Add('select MAX(checknumber) as MAXCH');
      quChMax.SelectSQL.Add('from cashsail               ');
      quChMax.SelectSQL.Add('where shopindex=1           ');
      quChMax.SelectSQL.Add('and  cashnumber='+IntToStr(cxSpinEdit3.EditValue));
      quChMax.SelectSQL.Add('and  znumber='+IntToStr(cxSpinEdit2.EditValue));
      quChMax.Active:=True;

      n_chmax:=quChMaxMAXCH.AsInteger+10;

      quChMax.Active:=False;

      for n_ch:=1 to n_chmax do
      begin
        if bStopCompare then Break;
        cxSpinEdit1.EditValue:=n_ch;
        cxButton3.Click;

        rSum:=0;
        rSumCh:=0;

        ViewCh.BeginUpdate;
        teCheck.First;
        while not teCheck.Eof do
        begin
          rSum:=rSum+teCheckRSum.AsFloat;
          teCheck.Next;
        end;
        ViewCh.EndUpdate;

        quChSum.Active:=False;
        quChSum.SelectSQL.Clear;
        quChSum.SelectSQL.Add('');
        quChSum.SelectSQL.Add('select SUM(totalrub) as RSUM');
        quChSum.SelectSQL.Add('from cashsail               ');
        quChSum.SelectSQL.Add('where shopindex=1           ');
        quChSum.SelectSQL.Add('and  cashnumber='+IntToStr(cxSpinEdit3.EditValue));
        quChSum.SelectSQL.Add('and  znumber='+IntToStr(cxSpinEdit2.EditValue));
        quChSum.SelectSQL.Add('and  checknumber='+IntToStr(cxSpinEdit1.EditValue));
        quChSum.Active:=True;

        rSumCh:=quChSumRSUM.AsFloat;

        quChSum.Active:=False;

        if (rSum<>0)or(rSumCh<>0) then
        begin
          teChSum.Append;
          teChSumNum.AsInteger:=n_ch;
          teChSumrSum.AsFloat:=rSum;
          teChSumrSumCh.AsFloat:=rSumCh;
          teChSumrDif.AsFloat:=rSum-rSumCh;
          teChSum.Post;
        end;
      end;
    except
      ShowMessage('������.');
    end;

    cxButton8.Enabled:=True;

  end else ShowMessage('��� ����� � ����� ������');
end;

procedure TfmMainRepair.ViewChSumCustomization(Sender: TObject);
begin
  if teChSum.RecordCount>0 then cxSpinEdit1.Value:=teChSumNum.AsInteger;
end;

procedure TfmMainRepair.cxButton9Click(Sender: TObject);
var ChMin,ChMax,n_ch:Integer;
begin
  if dbCh.Connected then
  begin
    ChMin:=cxSpinEdit5.EditValue;
    ChMax:=cxSpinEdit6.EditValue;

    for n_ch:=ChMin to ChMax do
    begin
      cxSpinEdit1.EditValue:=n_ch;
      cxButton3.Click;
      Delay(100);
      if teCheck.RecordCount>0 then
      begin
        cxButton4.Click;
        Delay(100);
      end;
    end;
  end else ShowMessage('��� ����� � ����� ������');
end;

procedure TfmMainRepair.cxButton10Click(Sender: TObject);
begin
  bStopCompare:=True;
end;

end.
