unit MainSQLConv;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, RXShell, Menus;

type
  TfmMainSQLConv = class(TForm)
    PopupMenu1: TPopupMenu;
    v16A11: TMenuItem;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    RxTray: TRxTrayIcon;
    Timer1: TTimer;
    Timer2: TTimer;
    procedure N1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure RxTrayClick(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure N3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

procedure WrMess(StrOut:String);
procedure prTransp;

var
  fmMainSQLConv: TfmMainSQLConv;

implementation

uses Dm, HistoryTr, Un1, uPosDb;

{$R *.dfm}

procedure prTransp;
var sNumCash:String;
    iCount:INteger;
    iPak:INteger;
begin
  //��������
  with dmPOSDB do
  with dmC do
  begin
    try
//      WrMess('������ ���������.');
      msConnPosDb.Connected:=False;
      Strwk:='FILE NAME='+CurDir+'PosDb.udl';
      msConnPosDb.ConnectionString:=Strwk;
      delay(10);
      msConnPosDb.Connected:=True;
      delay(10);
      if msConnPosDb.Connected then
      begin
//        WrMess('���� ������ ��.');
//        Delay(100);
        try
          CasherDb.Close;
          CasherDb.DBName:=DBName;
          CasherDb.Open;

          if CasherDb.Connected then
          begin
            sNumCash:=IntToStr(CommonSet.CashNum);
            if length(sNumCash)<2 then sNumCash:='0'+sNumCash;

            quSetActive.SQL.Clear;
            quSetActive.SQL.Add('update cash'+sNumCash+' set IACTIVE=2');
            quSetActive.SQL.Add('where IACTIVE=1');
            quSetActive.ExecSQL;
            delay(10);

            //���� �� 100 �������
            iPak:=CommonSet.ImportCount;
            if CommonSet.Turbo=1 then iPak:=100;

            quSelLoad.Active:=False;
            quSelLoad.SQL.Clear;
            quSelLoad.SQL.Add('SELECT TOP '+its(iPak)+' ID,IACTIVE,ITYPE,ARTICUL,BARCODE,CARDSIZE,QUANT,DSTOP,NAME,MESSUR,PRESISION,');
            quSelLoad.SQL.Add('ADD1,ADD2,ADD3,ADDNUM1,ADDNUM2,ADDNUM3,SCALE,GROUP1,GROUP2,GROUP3,PRICE,CLIINDEX,DELETED,PASSW');
            quSelLoad.SQL.Add('FROM CASH'+sNumCash);
            quSelLoad.SQL.Add('where IACTIVE=2');

            quSelLoad.Active:=True;
            quSelLoad.First;     iCount:=0;
            while not quSelLoad.Eof do
            begin
              if quSelLoadITYPE.AsInteger=1 then
              begin
                try
                  if quSelLoadDELETED.AsInteger>0 then
                  begin
                    if CommonSet.Turbo=0 then WrMess('  ��������� ����.'+quSelLoadARTICUL.AsString);

                    if quSelLoadGROUP1.AsInteger=0 then
                    begin
                      prAddCard.ParamByName('ARTICUL').AsString:=quSelLoadARTICUL.AsString;
                      prAddCard.ParamByName('CLASSIF').AsInteger:=quSelLoadGROUP3.AsInteger;
                      prAddCard.ParamByName('DEPART').AsInteger:=quSelLoadCLIINDEX.AsInteger;
                      prAddCard.ParamByName('NAME').AsString:=quSelLoadNAME.AsString;

                      if quSelLoadPRESISION.AsFloat=1 then prAddCard.ParamByName('CARD_TYPE').AsInteger:=1
                      else prAddCard.ParamByName('CARD_TYPE').AsInteger:=2;

                      prAddCard.ParamByName('MESURIMENT').AsString:=quSelLoadMESSUR.AsString;
                      prAddCard.ParamByName('PRICE_RUB').AsFloat:=quSelLoadPRICE.AsFloat;
                      prAddCard.ParamByName('DISCOUNT').AsFloat:=0;
                      prAddCard.ParamByName('DISCQUANT').AsFloat:=0;
                      prAddCard.ParamByName('SCALE').AsString:=quSelLoadSCALE.AsString;

                      prAddCard.ExecProc;
                    end else
                    begin
                      //EXECUTE PROCEDURE PR_ADDCARD_A (?ARTICUL, ?CLASSIF, ?DEPART, ?NAME, ?CARD_TYPE, ?MESURIMENT, ?PRICE_RUB, ?DISCQUANT, ?DISCOUNT, ?SCALE, ?AVID, ?AKREP)
                      prAddCardA.ParamByName('ARTICUL').AsString:=quSelLoadARTICUL.AsString;
                      prAddCardA.ParamByName('CLASSIF').AsInteger:=quSelLoadGROUP3.AsInteger;
                      prAddCardA.ParamByName('DEPART').AsInteger:=quSelLoadCLIINDEX.AsInteger;
                      prAddCardA.ParamByName('NAME').AsString:=quSelLoadNAME.AsString;

                      if quSelLoadPRESISION.AsFloat=1 then prAddCardA.ParamByName('CARD_TYPE').AsInteger:=1
                      else prAddCardA.ParamByName('CARD_TYPE').AsInteger:=2;

                      prAddCardA.ParamByName('MESURIMENT').AsString:=quSelLoadMESSUR.AsString;
                      prAddCardA.ParamByName('PRICE_RUB').AsFloat:=quSelLoadPRICE.AsFloat;
                      prAddCardA.ParamByName('DISCOUNT').AsFloat:=0;
                      prAddCardA.ParamByName('DISCQUANT').AsFloat:=0;
                      prAddCardA.ParamByName('SCALE').AsString:=quSelLoadSCALE.AsString;

                      prAddCardA.ParamByName('AVID').AsInteger:=quSelLoadGROUP1.AsInteger;
                      prAddCardA.ParamByName('AKREP').AsFloat:=0;

                      prAddCardA.ExecProc;
                    end;
                  end else
                  begin //��������
                    if CommonSet.Turbo=0 then WrMess('  ������� ����.'+quSelLoadARTICUL.AsString);
                    prDelCard.ParamByName('ARTICUL').AsString:=quSelLoadARTICUL.AsString;
                    prDelCard.ExecProc
                  end;
                except
                end;
              end;

              if quSelLoadITYPE.AsInteger=2 then
              begin
                try
                  if CommonSet.Turbo=0 then WrMess('  ��������� �� '+quSelLoadBARCODE.AsString);

                  prAddBar.ParamByName('BARCODE').AsString:=quSelLoadBARCODE.AsString;
                  prAddBar.ParamByName('CARDARTICUL').AsString:=quSelLoadARTICUL.AsString;
                  prAddBar.ParamByName('CARDSIZE').AsString:=quSelLoadCARDSIZE.AsString;
                  prAddBar.ParamByName('QUANTITY').AsFloat:=quSelLoadQUANT.AsFloat;
                  prAddBar.ParamByName('PRICERUB').AsFloat:=quSelLoadPRICE.AsFloat;

                  prAddBar.ExecProc;
                except
                end;
              end;

              if quSelLoadITYPE.AsInteger=3 then
              begin
                try
                  if CommonSet.Turbo=0 then WrMess('  ��������� ������ '+quSelLoadNAME.AsString);

//EXECUTE PROCEDURE PR_ADDCLASS (?ID, ?PARENT, ?NAME)

                  prAddClassif.ParamByName('ID').AsInteger:=quSelLoadARTICUL.AsInteger;
                  prAddClassif.ParamByName('PARENT').AsInteger:=quSelLoadGROUP3.AsInteger;
                  prAddClassif.ParamByName('NAME').AsString:=quSelLoadNAME.AsString;

                  prAddClassif.ExecProc;
                except
                end;
              end;

              if quSelLoadITYPE.AsInteger=18 then
              begin
                try
                  if CommonSet.Turbo=0 then WrMess('  ����. ���� '+quSelLoadARTICUL.AsString+' '+IntToStr(Trunc(rv(quSelLoadDSTOP.AsFloat))));

                  if taFixDisc.Active=False then taFixDisc.Active:=True;

                  if taFixDisc.Locate('SART',quSelLoadARTICUL.AsString,[]) then
                  begin
                    taFixDisc.Edit;
                    taFixDiscPROC.AsFloat:=quSelLoadDSTOP.AsFloat;
                    taFixDisc.Post;
                  end else
                  begin
                    taFixDisc.Append;
                    taFixDiscSART.AsString:=quSelLoadARTICUL.AsString;
                    taFixDiscPROC.AsFloat:=quSelLoadDSTOP.AsFloat;
                    taFixDisc.Post;
                  end;
                except
                end;
              end;

              if quSelLoadITYPE.AsInteger=11 then
              begin
                try
                  if CommonSet.Turbo=0 then WrMess('  ����. ����� '+quSelLoadBARCODE.AsString+' '+IntToStr(Trunc(rv(quSelLoadDSTOP.AsFloat))));

                  if taDiscCard.Active=False then taDiscCard.Active:=True;

                  if taDiscCard.Locate('BARCODE',quSelLoadBARCODE.AsString,[]) then
                  begin
                    taDiscCard.Edit;
                    taDiscCardNAME.AsString:=quSelLoadNAME.AsString;
                    taDiscCardPERCENT.AsFloat:=quSelLoadDSTOP.AsFloat;
                    taDiscCardCLIENTINDEX.AsInteger:=quSelLoadCLIINDEX.AsInteger;
                    taDiscCardIACTIVE.AsInteger:=quSelLoadDELETED.AsInteger;
                    taDiscCard.Post;
                  end else
                  begin
                    taDiscCard.Append;
                    taDiscCardNAME.AsString:=quSelLoadNAME.AsString;
                    taDiscCardPERCENT.AsFloat:=quSelLoadDSTOP.AsFloat;
                    taDiscCardCLIENTINDEX.AsInteger:=quSelLoadCLIINDEX.AsInteger;
                    taDiscCardIACTIVE.AsInteger:=quSelLoadDELETED.AsInteger;
                    taDiscCard.Post;
                  end;
                except
                end;
              end;

              if quSelLoadITYPE.AsInteger=107 then //������� ���� �������� ����� �������
              begin
                try
                  quDelPers.ExecQuery;
                except
                end;
                delay(100);
              end;

              if quSelLoadITYPE.AsInteger=7 then //��� �������� ��������
              begin
                try
                  if taPersonal.Active=False then taPersonal.Active:=True;

                  if taPersonal.Locate('ID',quSelLoadARTICUL.AsInteger,[]) then
                  begin
                    if taPersonalID_PARENT.AsInteger<>0 then
                    begin
                      taPersonal.Edit;

                      if pos('�����',quSelLoadNAME.AsString)>0 then taPersonalID_PARENT.AsInteger:=1000
                      else taPersonalID_PARENT.AsInteger:=1002;

                      taPersonalUVOLNEN.AsInteger:=1;
                      taPersonalMODUL3.AsInteger:=0;
                      taPersonalNAME.AsString:=quSelLoadNAME.AsString;
                      taPersonalPASSW.AsString:=quSelLoadBARCODE.AsString;
                      taPersonalBARCODE.AsString:=quSelLoadBARCODE.AsString;
                      taPersonal.Post;
                    end;
                  end else
                  begin
                    taPersonal.Append;
                    taPersonalID.AsInteger:=quSelLoadARTICUL.AsInteger;

                    if pos('�����',quSelLoadNAME.AsString)>0 then taPersonalID_PARENT.AsInteger:=1000
                    else taPersonalID_PARENT.AsInteger:=1002;

                    taPersonalUVOLNEN.AsInteger:=1;
                    taPersonalMODUL3.AsInteger:=0;
                    taPersonalNAME.AsString:=quSelLoadNAME.AsString;
                    taPersonalPASSW.AsString:=quSelLoadBARCODE.AsString;
                    taPersonalBARCODE.AsString:=quSelLoadBARCODE.AsString;
                    taPersonal.Post;
                  end;
                except
                end;
                delay(100);
              end;

//              quSelLoad.Edit;
//              quSelLoadIACTIVE.AsInteger:=3;
//              quSelLoad.Post;

              quE.Active:=False;
              quE.SQL.Clear;
              quE.SQL.Add('UPDATE CASH'+sNumCash+' SET IACTIVE = 3 WHERE ID = '+its(quSelLoadID.AsInteger));
              quE.ExecSQL;

              quSelLoad.Next; inc(iCount);
            end;
            if iCount>0 then WrMess(' ��. ('+IntToStr(iCount)+' �������)');
            if taFixDisc.Active=True then taFixDisc.Active:=False;
            if taDiscCard.Active=True then taDiscCard.Active:=False;
            if taPersonal.Active=True then taPersonal.Active:=False;
          end;
          CasherDb.Close;
        except
          CasherDb.Close;
        end;

//        WrMess('����� ��������.');
      end;
    except
//      WrMess('������ ���������.');
    end;
  end;
end;

procedure WrMess(StrOut:String);
Var StrWk:String;
begin
  with fmHistoryTr do
  begin
    if Memo1.Lines.Count > 500 then Memo1.Clear;
    if StrOut>'' then StrWk:=FormatDateTime('dd.mm hh:nn:ss',now)+'  '+StrOut
    else StrWk:=StrOut;
    Memo1.Lines.Add(StrWk);
    WriteHistoryConv(StrWk);
  end;
end;


procedure TfmMainSQLConv.N1Click(Sender: TObject);
begin
  fmHistoryTr.Show;
end;

procedure TfmMainSQLConv.FormCreate(Sender: TObject);
begin
  CurDir := ExtractFilePath(ParamStr(0));
  ReadIni;
end;

procedure TfmMainSQLConv.Timer2Timer(Sender: TObject);
begin
  if CommonSet.Turbo=1 then WrMess('������ �����. ������� '+IntToStr(1000)+'��')
  else WrMess('������ �����. ������� '+IntToStr(CommonSet.PeriodPing*1000)+'��');
  WrMess('  �� ����� - '+DBName);
//  WrMess('  �� ������ - '+CommonSet.CashDB);

  Timer2.Enabled:=False;
  if CommonSet.Turbo=0 then
  begin
    Timer1.Enabled:=False;
    Timer1.Interval:=CommonSet.PeriodPing*1000;
    Timer1.Enabled:=True;
  end else
  begin
    Timer1.Enabled:=False;
    Timer1.Interval:=1000;
    Timer1.Enabled:=True;
  end;
end;

procedure TfmMainSQLConv.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled:=False;
  try
//    if CommonSet.Turbo=1 then fmHistoryTr.Memo1.Lines.Add('.');
    prTransp;
  except
  end;
  Timer1.Enabled:=True;
end;

procedure TfmMainSQLConv.RxTrayClick(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  fmHistoryTr.Show;
end;

procedure TfmMainSQLConv.N3Click(Sender: TObject);
begin
  Close;
end;

end.
