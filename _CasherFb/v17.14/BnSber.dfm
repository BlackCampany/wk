object fmSber: TfmSber
  Left = 761
  Top = 141
  BorderStyle = bsDialog
  Caption = #1041#1077#1079#1085#1072#1083' '#1057#1041#1045#1056#1041#1040#1053#1050' '#1056#1086#1089#1089#1080#1080
  ClientHeight = 410
  ClientWidth = 441
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 24
    Top = 264
    Width = 60
    Height = 20
    Caption = #1057#1091#1084#1084#1072' '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object cxButton2: TcxButton
    Left = 16
    Top = 8
    Width = 313
    Height = 41
    Caption = #1052#1045#1046#1044#1059#1053#1040#1056#1054#1044#1053#1040#1071' '#1041#1040#1053#1050#1054#1042#1057#1050#1040#1071' '#1050#1040#1056#1058#1040' ('#1084#1072#1075#1085'. '#1087#1086#1083#1086#1089#1072')'
    Default = True
    TabOrder = 0
    OnClick = cxButton2Click
    Colors.Default = 16752706
    Colors.Normal = 16765348
    Colors.Hot = 16754386
    Colors.Pressed = 16754386
    LookAndFeel.Kind = lfUltraFlat
  end
  object cxButton3: TcxButton
    Left = 16
    Top = 60
    Width = 313
    Height = 33
    Caption = #1057#1041#1045#1056#1050#1040#1056#1058
    TabOrder = 1
    OnClick = cxButton3Click
    Colors.Default = 16752706
    Colors.Normal = 16765348
    Colors.Hot = 16754386
    Colors.Pressed = 16754386
    LookAndFeel.Kind = lfUltraFlat
  end
  object cxButton4: TcxButton
    Left = 16
    Top = 100
    Width = 313
    Height = 29
    Caption = 'AMERICAN EXPRESS'
    TabOrder = 2
    OnClick = cxButton4Click
    Colors.Default = 16752706
    Colors.Normal = 16765348
    Colors.Hot = 16754386
    Colors.Pressed = 16754386
    LookAndFeel.Kind = lfUltraFlat
  end
  object cxButton5: TcxButton
    Left = 16
    Top = 176
    Width = 153
    Height = 33
    Caption = #1042#1086#1079#1074#1088#1072#1090' '#1052#1041#1050
    TabOrder = 3
    OnClick = cxButton5Click
    Colors.Default = 16752706
    Colors.Normal = 16765348
    Colors.Hot = 16754386
    Colors.Pressed = 16754386
    LookAndFeel.Kind = lfUltraFlat
  end
  object cxButton6: TcxButton
    Left = 320
    Top = 216
    Width = 105
    Height = 33
    Caption = #1054#1090#1084#1077#1085#1072' '#1052#1041#1050'  '
    TabOrder = 4
    OnClick = cxButton6Click
    Colors.Default = 16752706
    Colors.Normal = 16765348
    Colors.Hot = 16754386
    Colors.Pressed = 16754386
    LookAndFeel.Kind = lfUltraFlat
  end
  object cxButton7: TcxButton
    Left = 16
    Top = 216
    Width = 153
    Height = 33
    Caption = #1042#1086#1079#1074#1088#1072#1090' '#1057#1041#1045#1056#1050#1040#1056#1058
    TabOrder = 5
    OnClick = cxButton7Click
    Colors.Default = 16752706
    Colors.Normal = 16765348
    Colors.Hot = 16754386
    Colors.Pressed = 16754386
    LookAndFeel.Kind = lfUltraFlat
  end
  object cxButton9: TcxButton
    Left = 200
    Top = 176
    Width = 105
    Height = 33
    Caption = #1047#1072#1082#1088#1099#1090#1080#1077' '#1076#1085#1103
    TabOrder = 6
    OnClick = cxButton9Click
    Colors.Default = 16752706
    Colors.Normal = 16765348
    Colors.Pressed = 15365376
    LookAndFeel.Kind = lfUltraFlat
  end
  object cxButton10: TcxButton
    Left = 320
    Top = 176
    Width = 105
    Height = 33
    Caption = #1048#1053#1050#1040#1057#1057#1040#1062#1048#1071' '#1057#1041#1050
    TabOrder = 7
    OnClick = cxButton10Click
    Colors.Default = 16752706
    Colors.Normal = 16765348
    Colors.Hot = 16754386
    Colors.Pressed = 16754386
    LookAndFeel.Kind = lfUltraFlat
  end
  object cxButton11: TcxButton
    Left = 200
    Top = 216
    Width = 105
    Height = 33
    Caption = #1058#1077#1082#1091#1097#1080#1081' '#1086#1090#1095#1077#1090
    TabOrder = 8
    OnClick = cxButton11Click
    Colors.Default = 16752706
    Colors.Normal = 16765348
    Colors.Hot = 16754386
    Colors.Pressed = 16754386
    LookAndFeel.Kind = lfUltraFlat
  end
  object cxButton12: TcxButton
    Left = 232
    Top = 312
    Width = 140
    Height = 25
    Caption = #1055#1077#1095#1072#1090#1100' '#1087#1086#1089#1083'. '#1076#1086#1082#1091#1084#1077#1085#1090#1072
    TabOrder = 9
    Visible = False
    OnClick = cxButton12Click
    LookAndFeel.Kind = lfOffice11
  end
  object cxButton1: TcxButton
    Left = 344
    Top = 8
    Width = 81
    Height = 161
    Caption = #1054#1090#1084#1077#1085#1072
    ModalResult = 2
    TabOrder = 10
    OnClick = cxButton1Click
    Colors.Default = 16752706
    Colors.Normal = 16765348
    Colors.Hot = 16754386
    Colors.Pressed = 16754386
    LookAndFeel.Kind = lfUltraFlat
  end
  object Panel1: TPanel
    Left = 16
    Top = 296
    Width = 417
    Height = 105
    BevelInner = bvLowered
    TabOrder = 11
    object Memo1: TMemo
      Left = 2
      Top = 2
      Width = 413
      Height = 101
      Align = alClient
      Lines.Strings = (
        'Memo1')
      TabOrder = 0
    end
  end
  object cxButton8: TcxButton
    Left = 16
    Top = 136
    Width = 313
    Height = 25
    Caption = #1041#1077#1079' '#1072#1074#1090#1086#1088#1080#1079#1072#1094#1080#1080
    TabOrder = 12
    OnClick = cxButton8Click
    Colors.Default = 4210943
    Colors.Normal = 16765348
    Colors.Hot = 16754386
    Colors.Pressed = 16754386
    LookAndFeel.Kind = lfUltraFlat
  end
  object cxButton13: TcxButton
    Left = 320
    Top = 256
    Width = 105
    Height = 33
    Caption = #1055#1077#1095#1072#1090#1100' '#1087#1086#1089#1083'. '#1095#1077#1082#1072
    TabOrder = 13
    OnClick = cxButton13Click
    Colors.Default = 16752706
    Colors.Normal = 16765348
    Colors.Hot = 16754386
    Colors.Pressed = 16754386
    LookAndFeel.Kind = lfUltraFlat
  end
  object dxfBackGround1: TdxfBackGround
    BkColor.BeginColor = 16767449
    BkColor.EndColor = clBlue
    BkColor.FillStyle = fsVert
    BkAnimate.Speed = 700
    Left = 180
    Top = 176
  end
  object amBn: TActionManager
    Left = 184
    Top = 264
    StyleName = 'XP Style'
    object acExit: TAction
      Caption = 'acExit'
      ShortCut = 121
      OnExecute = acExitExecute
    end
  end
end
