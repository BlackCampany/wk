unit AnnulsChecks;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxMemo, cxControls, cxContainer, cxEdit, cxTextEdit, cxMaskEdit,
  cxDropDownEdit, cxCalc, Menus, cxLookAndFeelPainters, StdCtrls,
  cxButtons, ExtCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter,
  cxData, cxDataStorage, DB, cxDBData, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, FIBDataSet, pFIBDataSet, FIBDatabase,
  pFIBDatabase, cxTimeEdit, ActnList, XPStyleActnCtrls, ActnMan, cxSpinEdit,
  dxfQuickTyp;

type
  TfmAChecks = class(TForm)
    Memo1: TcxMemo;
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton3: TcxButton;
    ViewChSp: TcxGridDBTableView;
    LevelChSp: TcxGridLevel;
    GridChSp: TcxGrid;
    GridChHd: TcxGrid;
    ViewChHd: TcxGridDBTableView;
    LevelChHd: TcxGridLevel;
    Label1: TLabel;
    Label2: TLabel;
    trASel: TpFIBTransaction;
    quADH: TpFIBDataSet;
    quADS: TpFIBDataSet;
    dsquADH: TDataSource;
    dsquADS: TDataSource;
    quADHCASHNUMBER: TFIBIntegerField;
    quADHZNUMBER: TFIBIntegerField;
    quADHCHECKNUMBER: TFIBIntegerField;
    quADHID: TFIBIntegerField;
    quADHCASHER: TFIBIntegerField;
    quADHDBAR: TFIBStringField;
    quADHCHDATE: TFIBDateTimeField;
    quADHPAYMENT: TFIBSmallIntField;
    quADHOPER: TFIBSmallIntField;
    ViewChHdZNUMBER: TcxGridDBColumn;
    ViewChHdCHECKNUMBER: TcxGridDBColumn;
    ViewChHdCASHER: TcxGridDBColumn;
    ViewChHdCHDATE: TcxGridDBColumn;
    ViewChHdPAYMENT: TcxGridDBColumn;
    ViewChHdOPER: TcxGridDBColumn;
    quADSIDH: TFIBIntegerField;
    quADSID: TFIBIntegerField;
    quADSARTICUL: TFIBStringField;
    quADSBAR: TFIBStringField;
    quADSQUANTITY: TFIBFloatField;
    quADSPRICERUB: TFIBFloatField;
    quADSDPROC: TFIBFloatField;
    quADSDSUM: TFIBFloatField;
    quADSTOTALRUB: TFIBFloatField;
    quADSNAME: TFIBStringField;
    quADSMESURIMENT: TFIBStringField;
    ViewChSpID: TcxGridDBColumn;
    ViewChSpARTICUL: TcxGridDBColumn;
    ViewChSpQUANTITY: TcxGridDBColumn;
    ViewChSpPRICERUB: TcxGridDBColumn;
    ViewChSpDPROC: TcxGridDBColumn;
    ViewChSpDSUM: TcxGridDBColumn;
    ViewChSpTOTALRUB: TcxGridDBColumn;
    ViewChSpNAME: TcxGridDBColumn;
    anAnnCheck: TActionManager;
    acExit: TAction;
    Label3: TLabel;
    cxSpinEdit1: TcxSpinEdit;
    procedure cxButton3Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ViewChHdFocusedRecordChanged(Sender: TcxCustomGridTableView;
      APrevFocusedRecord, AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure acExitExecute(Sender: TObject);
    procedure cxSpinEdit1PropertiesChange(Sender: TObject);
    procedure cxSpinEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAChecks: TfmAChecks;

implementation

uses Un1,Dm;

{$R *.dfm}

procedure TfmAChecks.cxButton3Click(Sender: TObject);
begin
  Close;
end;

procedure TfmAChecks.FormShow(Sender: TObject);
begin
  Memo1.Clear;
  cxSpinEdit1.SetFocus;
  cxSpinEdit1.SelectAll;
{  GridChHd.SetFocus;
  ViewChHd.Focused:=True;
  }
end;

procedure TfmAChecks.ViewChHdFocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord;
  ANewItemRecordFocusingChanged: Boolean);
begin
  ViewChSp.BeginUpdate;
  quADS.Active:=False;
  quADS.ParamByName('IDH').AsInteger:=quADHID.AsInteger;
  quADS.Active:=true;
  ViewChSp.EndUpdate;
end;

procedure TfmAChecks.acExitExecute(Sender: TObject);
begin
  ModalResult:=mrCancel;
end;

procedure TfmAChecks.cxSpinEdit1PropertiesChange(Sender: TObject);
begin
  //����� ������ �����
  ViewChHd.BeginUpdate;
  quADH.Active:=False;
  quADH.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
  quADH.ParamByName('ZNUM').AsInteger:=cxSpinEdit1.EditingValue;
  quADH.Active:=True;
  quADH.First;
  ViewChHd.EndUpdate;
end;

procedure TfmAChecks.cxSpinEdit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=39 then SelectNext(screen.ActiveControl,True,True);
  if Key=37 then SelectNext(screen.ActiveControl,False,True);
end;

end.
