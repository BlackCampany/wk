unit MainFRK;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons, cxControls,
  cxContainer, cxEdit, cxTextEdit, cxMemo, Buttons, cxGraphics, cxMaskEdit,
  cxDropDownEdit;

type
  TForm1 = class(TForm)
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    Memo1: TcxMemo;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    cxComboBox1: TcxComboBox;
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  CurDir:string;

implementation

uses Un1, UnCash;

{$R *.dfm}

procedure TForm1.cxButton1Click(Sender: TObject);
begin
  Memo1.Clear;
  delay(10);
  if CashOpen(PChar(cxComboBox1.Text)) then
  begin
    Memo1.Lines.Add('������� ��.');
  end else Memo1.Lines.Add('������� �� ������.');
end;

procedure TForm1.cxButton3Click(Sender: TObject);
begin
  CashClose;
  Memo1.Lines.Add('������� ��.');
end;

procedure TForm1.cxButton2Click(Sender: TObject);
begin
// �������� ������������ ��������
  OpenNFDoc;
//PrintNFStr('----------------------------------------');
  PrintNFStr('��� N:DNK080601145     ���� 0877515798  ');
  PrintNFStr('��� N:18844             ���:665800519293');
  PrintNFStr('�����:0003 �����:000819 ���:000000180442');
  PrintNFStr('01 �������������        ���.01/000000127');
  PrintNFStrExWide('    ��� �� �������  ',False,True,False,False);
  PrintNFStr('������� ������������� ����       =28.00 ');
  PrintNFStr('----------------------------------------');
  PrintNFStr('                  �����          =28.00 ');
  PrintNFStr('  ������                                ');
  PrintNFStr('  ������       �� �����           =1.40-');
  PrintNFStrExWide('         ����� � ������          =26.60 ',True,False,False,False);
  PrintNFStr('               ��������          =30.00 ');
  PrintNFStr('                  �����           =3.40 ');
  PrintNFStrExWide('                 ��',False,True,False,False);
  PrintNFStr('00000443 #023870                        ');

//  PrintNFStr('----------------------------------------');
  CloseNFDoc;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  CommonSet.CashNum:=1;
  CommonSet.NoFis:=0;

  CurDir := ExtractFilePath(ParamStr(0));
end;

procedure TForm1.BitBtn1Click(Sender: TObject);
var tBeg,tEnd:TDateTime;
begin
  OpenNFDoc;
  PrintNFStr('------------------- ');
  PrintNFStr('���� ������ �������');
  tBeg:=now;
  PrintNFStr('-------------- '+FormatDateTime('hh:nn:ss',tBeg));
  PrintQR(CurDir+'qr576_01.bmp');
  tEnd:=now-tBeg;
  tBeg:=now;
  PrintNFStr('-------------- '+FormatDateTime('hh:nn:ss',tBeg)+FormatDateTime('   ����� ss:zzz',tEnd));
  PrintQR(CurDir+'qr576_02.bmp');
  tEnd:=now-tBeg;
  tBeg:=now;
  PrintNFStr('-------------- '+FormatDateTime('hh:nn:ss',tBeg)+FormatDateTime('   ����� ss:zzz',tEnd));
  PrintQR(CurDir+'qr576_03.bmp');
  tEnd:=now-tBeg;
  tBeg:=now;
  PrintNFStr('-------------- '+FormatDateTime('hh:nn:ss',tBeg)+FormatDateTime('   ����� ss:zzz',tEnd));
  CloseNFDoc;
end;

procedure TForm1.BitBtn2Click(Sender: TObject);
Var iSum,iTotal,iDopl:Integer;
begin
  CheckStart;

  CurPos.Articul:='917';
  CurPos.Name:='����� 1';
  CurPos.Quant:=1;
  CurPos.Price:=0.01;
  CurPos.DProc:=0;
  CurPos.DSum:=0;
  CurPos.EdIzm:='��.';
  CurPos.Bar:='46000111000111';
  CurPos.TypeIzm:=1;

  PrintQR(CurDir+'qr002.bmp');

  CheckAddPos(iSum);

  PrintQR(CurDir+'qr002.bmp');

  CheckTotal(iTotal);

  PrintQR(CurDir+'qr002.bmp');

  CheckRasch(0,1,'','',iDopl);

  PrintQR(CurDir+'qr002.bmp');

  CheckClose;

  PrintQR(CurDir+'qr002.bmp');
end;

procedure TForm1.BitBtn3Click(Sender: TObject);
var tBeg,tEnd:TDateTime;
begin
  OpenNFDoc;
  PrintNFStr('------------------- ');
  PrintNFStr('���� ������ �������');
  tBeg:=now;
  PrintNFStr('-------------- '+FormatDateTime('hh:nn:ss',tBeg));
  PrintQR(CurDir+'qr448_01.bmp');
  tEnd:=now-tBeg;
  tBeg:=now;
  PrintNFStr('-------------- '+FormatDateTime('hh:nn:ss',tBeg)+FormatDateTime('   ����� ss:zzz',tEnd));
  PrintQR(CurDir+'qr448_02.bmp');
  tEnd:=now-tBeg;
  tBeg:=now;
  PrintNFStr('-------------- '+FormatDateTime('hh:nn:ss',tBeg)+FormatDateTime('   ����� ss:zzz',tEnd));
  PrintQR(CurDir+'qr448_03.bmp');
  tEnd:=now-tBeg;
  tBeg:=now;
  PrintNFStr('-------------- '+FormatDateTime('hh:nn:ss',tBeg)+FormatDateTime('   ����� ss:zzz',tEnd));
  CloseNFDoc;
end;

end.
