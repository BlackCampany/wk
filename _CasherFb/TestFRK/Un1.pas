unit Un1;

interface
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  IniFiles, ADODB, Variants, IdGlobal, ComCtrls, pFIBDataSet, EasyCompression;

type TPerson = record
     Id:Integer;
     Name:String;
     end;

     TCommonSet = record
     AutoTake:Integer;
     CashNum:Integer;
     ShopIndex:Integer;
     PathExport:String;
     PathImport:String;
     NetPath:String;
     FtpExport:String;
     FtpImport:String;
     PathHistory:String;
     PathArh:String;
     DepartId:Integer;
     DepartName:String;
     DateFrom:TDateTime;
     DateTo:TDateTime;
     PeriodPing:INteger;
     Key_Char:String;
     Key_Code:Integer;
     Key_Shift:Integer;
     Key_Add: Boolean;
     PortDP:String;
     PortSC:String;
     PortCash:String;
     Articul0:String;
     WriteLog:ShortInt;
     BNManual:ShortInt;
     BNPath:String;
     BNPointNum:String;
     BNDelay:INteger;
     ZNum:INteger;
     Passw:String;
     OutPut:String;
     ZTimeShift:String;
     WkTimeBeg,WkTimeEnd:TDateTime;
     AlkoKrepMax:INteger;
     TypeFis:String;
     ComDelay:Integer;
     CountSecMax:INteger;
     CashDB:String;
     TypeDP:Integer;
     OutType:Integer;   
     DiscPre:String;
     RoundSum:Integer;
     NoFis:SmallInt;
     PreStr1,PreStr2,LastStr1,LastStr2:String;
     Turbo:Integer;
     StartDelay:Integer;
     ImportCount:Integer;
     end;

     TTrebSel= record
     CLTO:Integer;
     CLTO_Name:String;
     DType:Integer;
     DType_Name:String;
     DateFrom:TDateTime;
     DateTo:TDateTime;
     end;


     tClassifRec = record
     TYPE_CLASSIF:Integer;
     ID:Integer;
     Id_Parent:Integer;
     Name:String;
     end;

     tMesuriment = record
     Id:Integer;
     Name:String;
     end;

     tCardRec = record
     Id:Integer;
     Id_Classif:Integer;
     Name:String;
     Mesuriment:Integer;
     end;

     tCategRec = record
     Id:Integer;
     Name:String;
     Tax_Group:Integer;
     end;

     TCardSel = Record
     iType:ShortInt;
     Id:Integer;
     Name:String;
     Id_Group:Integer;
     NameGr:String;
     Quant:Real;
     Depart:String;
     DepartId:String;
     mesuriment:String;
     end;

     TCheck = record
     ChBeg:Boolean;
     ChEnd:Boolean;
     Num:Integer;
     Discount:String;
     DiscProc:Real;
     DiscName:String;
     Operation:SmallInt;
     end;

     TBufPr = Record
     Arr:Array[1..8000] of Char;
     iC:Integer;
     end;


procedure Delay(MSecs: Longint);
Function TrimStr(StrIn:String):String;
Procedure CheckNodeOn(Node:TTreeNode;bOn:Boolean);
Procedure ExpandLevel( Node : TTreeNode; Tree:TTreeView; quTree:TpFIBDataSet);
Procedure RExpandLevel( Node : TTreeNode; Tree:TTreeView; quTree:TpFIBDataSet; PersonalId:Integer);
Procedure RefreshTree(PersonalId:Integer; Tree:TTreeView; quTree:TpFIBDataSet);
Procedure ReadIni;
Procedure WriteIni;
Procedure WriteNums;
Procedure ReadKlIni;
Procedure WriteKlIni;
Function TestExch:Boolean;
procedure WriteHistory(Strwk_: string);
procedure WriteHistoryConv(Strwk_: string);
procedure prWriteLog(Strwk_: string);
Function StrToClassif(StrIn:String; Var ClRec:TClassifRec):Boolean;
Function StrToMes(StrIn:String; Var vMes:TMesuriment):Boolean;
Function StrToCard(StrIn:String; Var vCard:TCardRec):Boolean;
Function StrToCateg(StrIn:String; Var vCateg:TCategRec):Boolean;
Procedure CardsExpandLevel( Node : TTreeNode; Tree:TTreeView; quTree:TpFIBDataSet);
Procedure ClassifExpandLevel( Node : TTreeNode; Tree:TTreeView; quTree:TpFIBDataSet; PersonalId:Integer);
procedure TestDir;
Function TestExport:Boolean;
Function DelSp(sStr:String):String;
function RoundEx( X: Double ): Integer;
function RV( X: Double ): Double;
function Its(iV:Integer):String;
Function SOnlyDigit(S:String):String;
Procedure prInBuf(Ch:Char);
function SetL(S1:String;iL:Integer):String;
Function prDefFormatStr1(l:ShortInt;sN:String;rS:Real):String;
Function prDefFormatStr2(l:ShortInt;sN:String;rS:Integer):String;
Function SOnlyDigit1(S:String):String;


Const CurIni:String = 'Profiles.ini';
      KlIni:String = 'Kl.Ini';
      GridIni:String = 'ProfilesGr.ini';
      FileInd:String = 'Start';
      CashNon:String = 'Cash.non';
      R:String = ';';
      kClassif:Integer = 10000;
      TrExt:String = '.cr~';

      levelValue: array [0..9] of TECLCompressionLevel =
              (eclNone, zlibFastest, zlibNormal, zlibMax, bzipFastest,
               bzipNormal, bzipMax, ppmFastest, ppmNormal, ppmMax);
      odInac:String = 'odNoFocusRect_';
      WarnMess:String = '����� �������� ��������� �������.';
      VerMess:String = 'Ver 2 for Elisey (pr. Ivanchenco)';
      ZerroMess:String = '������� ������ � ������� ����� ��������� !!!';

Var StrWk:String;
    Person:TPerson;
    CurDir:String;
    DBName:String;
    CommonSet:TCommonSet;
    bClear1,bClear2,bClear3,bClear4,bClear5,bClear6:Boolean;
    ClassifRec:TClassifRec;
    vMesuriment:TMesuriment;
    CardRec:TCardRec;
    CategRec:TCategRec;
    CardSel:TCardSel;
    BegDrag:Boolean = False;
    TrebSel:TTrebSel;
    ViewOnly:Boolean = False;
    CountSec:Integer;
    bSave:Boolean;
    StrPre,DiscountBar :String;
    sFormatDate:String = 'dd.mm.yyyy';
    Check:TCheck;
    sScan:ShortString = '';
    rDiscont,rCheckSum,rDiscDop:Real;
    bBegCash:Boolean = False;
    bBegCashEnd:Boolean = False;
    bMoneyCalc:Boolean = False;
    sSave:String = '';
    BnStr:String = '';
    SberTabStop:SmallInt;
    bAddPos:Boolean = False;
    rSumNal,rSumBn:Real;
    BufPr:TBufPr;

implementation

uses UnCash;

Function prDefFormatStr1(l:ShortInt;sN:String;rS:Real):String;
Var StrWk,StrWk1:String;
begin
  StrWk:=sN;
  Str(rS:8:2,StrWk1);
  StrWk1:=DelSp(StrWk1);
  StrWk1:=StrWk1+' ���';
  while (Length(StrWk)+Length(StrWk1))<l do StrWk:=StrWk+' ';
  Result:=StrWk+StrWk1;
end;

Function prDefFormatStr2(l:ShortInt;sN:String;rS:Integer):String;
Var StrWk,StrWk1:String;
begin
  StrWk:=sN;
  StrWk1:=IntToStr(rS);
  while (Length(StrWk)+Length(StrWk1))<l do StrWk:=StrWk+' ';
  Result:=StrWk+StrWk1;
end;


function SetL(S1:String;iL:Integer):String;
begin
  S1:=Copy(S1,1,iL);
  while length(S1)<iL do s1:=s1+' ';
  Result:=S1;
end;

Procedure prInBuf(Ch:Char);
begin
  if BufPr.iC<16000 then
  begin
    BufPr.Arr[BufPr.iC+1]:=Ch;
    inc(BufPr.iC);
  end;
end;


Function SOnlyDigit1(S:String):String;
//Var i,l:Integer;
begin
  result:=S;
{
  while pos(#186,S)>0 do delete(S,pos(#186,S),1);
  while pos(#187,S)>0 do delete(S,pos(#187,S),1);

  l:=length(S);
  for i:=1 to l do
  begin
    if s[i] in ['0','1','2','3','4','5','6','7','8','9','='] then
    begin
      Result:=Result+s[i];
    end;
  end;
  if result='' then result:='0';}
end;

Function SOnlyDigit(S:String):String;
Var i,l:Integer;
begin
  result:='';

  while pos(#186,S)>0 do delete(S,pos(#186,S),1);
  while pos(#187,S)>0 do delete(S,pos(#187,S),1);

  l:=length(S);
  for i:=1 to l do
  begin
    if s[i] in ['0','1','2','3','4','5','6','7','8','9','='] then
    begin
      Result:=Result+s[i];
    end;
  end;
  if result='' then result:='0';
end;

function Its(iV:Integer):String;
begin
  Result:=IntToStr(iV);
end;

function RV( X: Double ): Double;
var  ScaledFractPart:Integer;
     Temp : Double;

begin
  try
    ScaledFractPart := Trunc(X*100);
    if X>=0 then Temp := Trunc(Frac(X*100)*1000000)+1 else  Temp := Trunc(Frac(X*100)*1000000)-1;
    if Temp >=  500000 then ScaledFractPart := ScaledFractPart + 1;
    if Temp <= -500000 then ScaledFractPart := ScaledFractPart - 1;
    RV:= ScaledFractPart/100;
  except
    RV:=0;
  end;
end;


procedure prWriteLog(Strwk_: string);
var F: TextFile;
    Strwk1:String;
    FileN:String;
begin
  if CommonSet.WriteLog<>1 then exit;
  try
    if not DirectoryExists(CommonSet.PathArh) then
    begin
      exit;
    end;

    strwk1:=FormatDateTime('yyyy_mm_dd',Date);
    Strwk1:=StrWk1+'.log';
//    Application.ProcessMessages;

    FileN:=CommonSet.PathArh+strwk1;

    AssignFile(F, FileN);
    if Not FileExists(FileN) then Rewrite(F)
    else                           Append(F);
    Write(F, FormatDateTime('HH:NN:SS ;', Now));
    WriteLn(F, Strwk_);
    Flush(F);
  finally
    CloseFile(F);
  end;
end;



function RoundEx( X: Double ): Integer;
var  ScaledFractPart:Integer;
     Temp : Double;
begin
 ScaledFractPart := Trunc(X);
 Temp := Trunc(Frac(X)*1000000)+1;
 if Temp >=  500000 then ScaledFractPart := ScaledFractPart + 1;
 if Temp <= -500000 then ScaledFractPart := ScaledFractPart - 1;
{ if Temp >=  0.5 then ScaledFractPart := ScaledFractPart + 1;
 if Temp <= -0.5 then ScaledFractPart := ScaledFractPart - 1;}
 RoundEx := ScaledFractPart;
end;



Function DelSp(sStr:String):String;
begin
  while pos(' ',sStr)>0 do delete(sStr,pos(' ',sStr),1);
  Result:=sStr;
end;

Procedure ClassifExpandLevel( Node : TTreeNode; Tree:TTreeView; quTree:TpFIBDataSet; PersonalId:Integer );
Var ID , i   : Integer;
    TreeNode : TTreeNode;
Begin
// ��� ������ �������� ������ ������� ������ ���,
// ��� �� ����� ���������.
  if Node = nil then ID:=0
  else ID:=Integer(Node.Data);
  quTree.Close;
  quTree.ParamByName('ParentID').Value:=ID;
  quTree.ParamByName('PersonalID').Value:=PersonalId;
  quTree.Open;
  Tree.Items.BeginUpdate;
  for i:=1 to quTree.RecordCount do
  begin    // ������� � ���� Data ����� �� ����������������� �����(ID) � �������
    TreeNode:=Tree.Items.AddChildObject(Node, quTree.FieldByName('Name').AsString, Pointer(quTree.FieldByName('ID').AsInteger));
    TreeNode.ImageIndex:=8;
    TreeNode.SelectedIndex:=7;
    // ������� ��������� (������) �������� ����� ������ ��� ����,
    // ����� ��� ��������� [+] �� ����� � �� ����� ���� �� ��������
    Tree.Items.AddChildObject(TreeNode,'', nil);
    quTree.Next;
  end;
  Tree.Items.EndUpdate;
end;


Procedure CardsExpandLevel( Node : TTreeNode; Tree:TTreeView; quTree:TpFIBDataSet);
Var ID , i   : Integer;
    TreeNode : TTreeNode;
Begin
// ��� ������ �������� ������ ������� ������ ���,
// ��� �� ����� ���������.
  if Node = nil then ID:=0
  else ID:=Integer(Node.Data);
  quTree.Close;
  quTree.ParamByName('ParentID').Value:=ID;
  quTree.ParamByName('PersonID').Value:=Person.Id;
  quTree.Open;
  Tree.Items.BeginUpdate;
  // ��� ������ ������ �� ����������� ������ ������
  // ��������� ����� � TreeView, ��� �������� ����� � ���,
  // ������� �� ������ ��� "��������"
  for i:=1 to quTree.RecordCount do
  begin    // ������� � ���� Data ����� �� ����������������� �����(ID) � �������
    TreeNode:=Tree.Items.AddChildObject(Node, quTree.FieldByName('Name').AsString, Pointer(quTree.FieldByName('ID').AsInteger));
    TreeNode.ImageIndex:=8;
    TreeNode.SelectedIndex:=7;
    // ������� ��������� (������) �������� ����� ������ ��� ����,
    // ����� ��� ��������� [+] �� ����� � �� ����� ���� �� ��������
    Tree.Items.AddChildObject(TreeNode,'', nil);
    quTree.Next;
  end;
  Tree.Items.EndUpdate;
end;

Function StrToCateg(StrIn:String; Var vCateg:TCategRec):Boolean;
Var strwk:String;
    n:Integer;
begin
  result:=True;
  for n:=1 to 3 do
  begin
    Delete(StrIn,1,pos(r,strin));
    if pos(r,strin)>0 then strwk:=Copy(StrIn,1,pos(r,strin)-1);
    Case n of
    1: begin
         vCateg.Id:=StrToIntDef(StrWk,-1);
         if vCateg.Id<0 then result:=False;
       end;
    2: begin
         vCateg.Name:=StrWk;
       end;
    3: begin
         vCateg.Tax_Group:=StrToIntDef(StrIn,-1);
         if vCateg.Tax_Group<0 then result:=False;
       end;
    end;
  end;
end;



Function StrToCard(StrIn:String; Var vCard:TCardRec):Boolean;
Var strwk:String;
    n:Integer;
begin
  result:=True;
  for n:=1 to 4 do
  begin
    Delete(StrIn,1,pos(r,strin));
    if pos(r,strin)>0 then strwk:=Copy(StrIn,1,pos(r,strin)-1);
    Case n of
    1: begin
         CardRec.Id:=StrToIntDef(StrWk,-1);
         if CardRec.Id<0 then result:=False;
       end;
    2: begin
         CardRec.Name:=StrWk;
       end;
    3: begin
         CardRec.Mesuriment:=StrToIntDef(StrWk,-1);
         if CardRec.Mesuriment<0 then result:=False;
       end;
    4: begin
         CardRec.Id_Classif:=StrToIntDef(StrIn,-1);
         if CardRec.Id_Classif<0 then result:=False;
       end;
    end;
  end;
end;


Function StrToMes(StrIn:String; Var vMes:TMesuriment):Boolean;
Var strwk:String;
    n:Integer;
begin
  result:=True;
  for n:=1 to 2 do
  begin
    Delete(StrIn,1,pos(r,strin));
    if pos(r,strin)>0 then strwk:=Copy(StrIn,1,pos(r,strin)-1);
    Case n of
    1: begin
         vMes.Id:=StrToIntDef(StrWk,-1);
         if vMes.Id<0 then result:=False;
       end;
    2: begin
         vMes.Name:=StrIn;
       end;
    end;
  end;
end;


Function StrToClassif(StrIn:String; Var ClRec:TClassifRec):Boolean;
Var strwk:String;
    n:Integer;
begin
  result:=True;
  for n:=1 to 4 do
  begin
    Delete(StrIn,1,pos(r,strin));
    if pos(r,strin)>0 then strwk:=Copy(StrIn,1,pos(r,strin)-1);
    Case n of
    1: begin
         ClRec.TYPE_CLASSIF:=StrToIntDef(StrWk,-1);
         if ClRec.TYPE_CLASSIF<0 then result:=False;
       end;
    2: begin
         ClRec.ID:=StrToIntDef(StrWk,-1);
         if ClRec.Id<0 then result:=False;
       end;
    3: begin
         ClRec.Id_Parent:=StrToIntDef(StrWk,-1);
         if ClRec.Id_Parent<0 then result:=False;
       end;
    4: begin
         ClRec.Name:=StrIn;
       end;
    end;
  end;
end;

{Function StrToTrHead(StrIn:String; Var HRec:tTrebHRec):Boolean;
Var strwk:String;
    n:Integer;
begin
  result:=True;
  for n:=1 to 8 do
  begin
    Delete(StrIn,1,pos(r,strin));
    if pos(r,strin)>0 then strwk:=Copy(StrIn,1,pos(r,strin)-1);
    Case n of
    1: begin
         HRec.SID:=StrWk;
       end;
    2: begin
         HRec.DEPARTID:=StrWk;
       end;
    3: begin
         HRec.DEPARTNAME:=StrWk;
       end;
    4: begin
         HRec.DTYPE:=StrToIntDef(StrWk,-1);
         if HRec.DTYPE<0 then result:=False;
       end;
    5: begin
         try
           HRec.TREBDATE:=StrToDate(StrWk);
         except
           result:=False;
         end;
       end;
    6: begin
         try
           HRec.SENDDATE:=StrToDate(StrWk);
         except
           result:=False;
         end;
       end;
    7: begin
         HRec.CLTO:=StrToIntDef(StrWk,-1);
         if HRec.CLTO<0 then result:=False;
       end;
    8: begin
         HRec.Comment:=StrIn;
       end;
    end;
  end;
end;

Function StrToSpec(StrIn:String; Var SRec:tSpecRec):Boolean;
Var strwk:String;
    n:Integer;
begin
  result:=True;
  for n:=1 to 8 do
  begin
    Delete(StrIn,1,pos(r,strin));
    if pos(r,strin)>0 then strwk:=Copy(StrIn,1,pos(r,strin)-1);
    Case n of
    1: begin
         SRec.SID_HEAD:=StrWk;
       end;
    2: begin
         SRec.ID:=333; //�� ������������ ��� �������
       end;
    3: begin
         SRec.CARD_ID:=StrToIntDef(StrWk,-1);
         if SRec.CARD_ID<0 then result:=False;
       end;
    4: begin
         SRec.CARD1_ID:=StrToIntDef(StrWk,-1);
         if SRec.CARD1_ID<0 then result:=False;
       end;
    5: begin
         SRec.NUM:=StrToIntDef(StrWk,-1);
         if SRec.NUM<0 then result:=False;
       end;
    6: begin
         SRec.QUANT:=StrToFloatDef(StrWk,-1);
         if SRec.QUANT<0 then result:=False;
       end;
    7: begin
         SRec.GROUPCARD:=StrToIntDef(StrWk,-1);
         if SRec.GROUPCARD<0 then result:=False;
       end;
    8: begin
         SRec.GROUPCARD1:=StrToIntDef(StrIn,-1);
         if SRec.GROUPCARD1<0 then result:=False;
       end;
    end;
  end;
end;
}

procedure WriteHistoryConv(Strwk_: string);
var F: TextFile;
    Strwk1:String;
    FileN:String;
begin
  try
    if not DirectoryExists(CommonSet.PathHistory) then
    begin
//      ShowMessage('������: ����������� ����������� - "'+CommonSet.PathHistory+'"');
      exit;
    end;

    strwk1:='Conv'+FormatDateTime('yyyy_mm',Date);
    Strwk1:=StrWk1+'.txt';
    Application.ProcessMessages;

    FileN:=CommonSet.PathHistory+strwk1;

    AssignFile(F, FileN);
    if Not FileExists(FileN) then Rewrite(F)
    else                           Append(F);
    Write(F, FormatDateTime('DD/MM/YYYY  HH:NN:SS ', Now));
    WriteLn(F, Strwk_);
    Flush(F);
  finally
    CloseFile(F);
  end;
end;


procedure WriteHistory(Strwk_: string);
var F: TextFile;
    Strwk1:String;
    FileN:String;
begin
  try
    if not DirectoryExists(CommonSet.PathHistory) then
    begin
      ShowMessage('������: ����������� ����������� - "'+CommonSet.PathHistory+'"');
      exit;
    end;

    strwk1:=FormatDateTime('yyyy_mm',Date);
    Strwk1:=StrWk1+'.txt';
    Application.ProcessMessages;

    FileN:=CommonSet.PathHistory+strwk1;

    AssignFile(F, FileN);
    if Not FileExists(FileN) then Rewrite(F)
    else                           Append(F);
    Write(F, FormatDateTime('DD/MM/YYYY  HH:NN:SS ', Now));
    WriteLn(F, Strwk_);
    Flush(F);
  finally
    CloseFile(F);
  end;
end;

procedure TestDir;
begin
  if not DirectoryExists(CommonSet.PathExport) then
    if not CreateDir(CommonSet.PathExport) then
    raise Exception.Create('�� ���� ������� ����������� "'+CommonSet.PathExport+'"');
  if not DirectoryExists(CommonSet.PathImport) then
    if not CreateDir(CommonSet.PathImport) then
    raise Exception.Create('�� ���� ������� ����������� "'+CommonSet.PathImport+'"');
  if not DirectoryExists(CommonSet.PathHistory) then
    if not CreateDir(CommonSet.PathHistory) then
    raise Exception.Create('�� ���� ������� ����������� "'+CommonSet.PathHistory+'"');
  if not DirectoryExists(CommonSet.PathArh) then
    if not CreateDir(CommonSet.PathArh) then
    raise Exception.Create('�� ���� ������� ����������� "'+CommonSet.PathArh+'"');
end;

Function TestExch:Boolean;
Var F:TextFile;
begin
  Result:=False;
  if FileExists(CommonSet.PathImport+FileInd) then
  begin
    Result:=True;
    AssignFile(F,CommonSet.PathImport+FileInd);
    try
      Erase(F);
    except
      result:=False;
    end;
  end;
end;

Function TestExport:Boolean;
Var F:TextFile;
begin
  Result:=False;
  if FileExists(CommonSet.PathExport+FileInd) then
  begin
    Result:=True;
    AssignFile(F,CommonSet.PathExport+FileInd);
    try
      Erase(F);
    except
      result:=False;
    end;
  end;
end;


Procedure ReadIni;
Var f:TIniFile;
    StrWk:String;
begin
  f:=TIniFile.create(CurDir+CurIni);

  DBName:=f.ReadString('Config_','DBName','C:\Database\Ust\Ust.GDB');
  Person.Id:=f.ReadInteger('Config_','PersinId',0);
  CommonSet.AutoTake:=f.ReadInteger('Config_','AutoTake',1);
  CommonSet.CashNum:=f.ReadInteger('Config_','CashNum',1);
  CommonSet.ShopIndex:=f.ReadInteger('Config_','ShopIndex',1);
  CommonSet.PathImport:=f.ReadString('Config_','PathImport',CurDir+'Import\');
  CommonSet.PathExport:=f.ReadString('Config_','PathExport',CurDir+'Export\');
  CommonSet.NetPath:=f.ReadString('Config_','NetPath','C:\');

  CommonSet.FtpImport:=f.ReadString('Config_','FtpImport',CurDir+'temp\04\');
  CommonSet.FtpExport:=f.ReadString('Config_','FtpExport',CurDir+'temp\Office\;temp\office1\');
  CommonSet.PathHistory:=f.ReadString('Config_','PathHistory',CurDir+'History\');
  CommonSet.PathArh:=f.ReadString('Config_','PathArh',CurDir+'Arh\');
  CommonSet.DepartId:=f.ReadInteger('Config_','DepartId',0);
  CommonSet.DepartName:=f.ReadString('Config_','DepartName','����� �������� ������� ���������� !!!');
  CommonSet.PeriodPing:=f.ReadInteger('Config_','PeriodPing',30);
  CommonSet.PortDP:=f.ReadString('Config_','PortDP','COM2');
  CommonSet.PortSC:=f.ReadString('Config_','PortSC','COM3');
  CommonSet.PortCash:=f.ReadString('Config_','PortCash','COM1');
  CommonSet.ComDelay:=f.ReadInteger('Config_','ComDelay',100);

  CommonSet.ZNum:=f.ReadInteger('Config_','ZNum',1);
  Check.Num:=f.ReadInteger('Config_','CheckNum',1);
  CommonSet.Articul0:=f.ReadString('Config_','Articul0','917');
  CommonSet.WriteLog:=f.ReadINteger('Config_','WriteLog',0);  //���
  CommonSet.BNManual:=f.ReadINteger('Config_','BNManual',1);  //��
  CommonSet.BNPath:=f.ReadString('Config_','BNPath','C:\_CasherFb\BIN\BN');
  CommonSet.BNPointNum:=f.ReadString('Config_','BNPointNum','00001');
  CommonSet.BNDelay:=f.ReadINteger('Config_','BNDelay',30);  //��
  CommonSet.Passw:=f.ReadString('Config_','Password','314159');  //��
  CommonSet.OutPut:=f.ReadString('Config_','OutPut',CurDir+'OutPut\');
  CommonSet.ZTimeShift:=f.ReadString('Config_','ZTimeShift','00:30'); //����� �� ������� ��� �������� Z-������
  StrWk:=f.ReadString('Config_','WkTimeBeg','07:00'); //������ ������ ��� ��������
  CommonSet.WkTimeBeg:=Frac(StrToTimeDef(StrWk,0));
  StrWk:=f.ReadString('Config_','WkTimeEnd','24:00'); //����� ������ ��� ��������
  CommonSet.WkTimeEnd:=Frac(StrToTimeDef(StrWk,0.999999));
  CommonSet.AlkoKrepMax:=f.ReadINteger('Config_','AlkoKrepMax',15);  //������������ �������� �����
  CommonSet.CountSecMax:=f.ReadINteger('Config_','CountSecMax',0);  // �������� ��������� ������
  CommonSet.CashDB:=f.ReadString('Config_','CashDB','btr1:D:\kassa\PosFb\cash.gdb'); //���� � ���� ������
  CommonSet.TypeDP:=f.ReadInteger('Config_','TypeDP',1);
  CommonSet.OutType:=f.ReadInteger('Config_','OutType',2); //1 - ����������� ������, 2 - � FB
  CommonSet.DiscPre:=f.ReadString('Config_','DiscPre','29'); //������ ���������� ���� ��� ������� ������� ��� ����������
  CommonSet.RoundSum:=f.ReadInteger('Config_','RoundSum',10); //���������� �����������
  CommonSet.NoFis:=f.ReadInteger('Config_','NoFis',0); // � ������������ ������ 1
  CommonSet.PreStr1:=f.ReadString('Config_','PreStr1','1-1'); //
  CommonSet.PreStr2:=f.ReadString('Config_','PreStr2','1-2'); //
  CommonSet.LastStr1:=f.ReadString('Config_','LastStr1','2-1'); //
  CommonSet.LastStr2:=f.ReadString('Config_','LastStr2','2-2'); //
  CommonSet.Turbo:=f.ReadInteger('Config_','Turbo',0); //
  CommonSet.StartDelay:=f.ReadInteger('Config_','StartDelaySec',10); //
  CommonSet.ImportCount:=f.ReadInteger('Config_','ImportCount',50); //


  if CommonSet.PathExport[Length(CommonSet.PathExport)]<>'\' then CommonSet.PathExport:=CommonSet.PathExport+'\';
  if CommonSet.PathImport[Length(CommonSet.PathImport)]<>'\' then CommonSet.PathImport:=CommonSet.PathImport+'\';
  if CommonSet.FtpExport[Length(CommonSet.FtpExport)]<>'\' then CommonSet.FtpExport:=CommonSet.FtpExport+'\';
  if CommonSet.FtpImport[Length(CommonSet.FtpImport)]<>'\' then CommonSet.FtpImport:=CommonSet.FtpImport+'\';
  if CommonSet.PathArh[Length(CommonSet.PathArh)]<>'\' then CommonSet.PathArh:=CommonSet.PathArh+'\';
  if CommonSet.PathHistory[Length(CommonSet.PathHistory)]<>'\' then CommonSet.PathHistory:=CommonSet.PathHistory+'\';
  if CommonSet.NetPath[Length(CommonSet.NetPath)]<>'\' then CommonSet.NetPath:=CommonSet.NetPath+'\';


  f.WriteString('Config_','DBName',DBName);
  f.WriteInteger('Config_','PersinId',Person.Id);
  f.WriteInteger('Config_','AutoTake',CommonSet.AutoTake);
  f.WriteInteger('Config_','CashNum',CommonSet.CashNum);
  f.WriteString('Config_','PathExport',CommonSet.PathExport);
  f.WriteString('Config_','PathImport',CommonSet.PathImport);
  f.WriteString('Config_','FtpExport',CommonSet.FtpExport);
  f.WriteString('Config_','FtpImport',CommonSet.FtpImport);
  f.WriteString('Config_','PathHistory',CommonSet.PathHistory);
  f.WriteString('Config_','PathArh',CommonSet.PathArh);
  f.WriteInteger('Config_','DepartId',CommonSet.DepartId);
  f.WriteString('Config_','DepartName',CommonSet.DepartName);
  f.WriteInteger('Config_','PeriodPing',CommonSet.PeriodPing);
  f.WriteInteger('Config_','ShopIndex',CommonSet.ShopIndex);
  f.WriteString('Config_','PortDP', CommonSet.PortDP);
  f.WriteString('Config_','PortSC', CommonSet.PortSC);
  f.WriteString('Config_','PortCash', CommonSet.PortCash);
  f.WriteString('Config_','NetPath',CommonSet.NetPath);
  f.WriteString('Config_','Articul0',CommonSet.Articul0);
  f.WriteINteger('Config_','WriteLog',CommonSet.WriteLog);
  f.WriteINteger('Config_','BNManual',CommonSet.BNManual);
  f.WriteString('Config_','BNPath',CommonSet.BNPath);
  f.WriteString('Config_','BNPointNum',CommonSet.BNPointNum);
  f.WriteINteger('Config_','BNDelay',CommonSet.BNDelay);
  f.WriteInteger('Config_','ZNum',CommonSet.ZNum);
  f.WriteInteger('Config_','CheckNum',Check.Num);
  f.WriteString('Config_','Password',CommonSet.Passw);
  f.WriteString('Config_','OutPut',CommonSet.OutPut);
  f.WriteString('Config_','ZTimeShift',CommonSet.ZTimeShift);
  f.WriteString('Config_','WkTimeBeg',FormatDateTime('hh:nn:ss',CommonSet.WkTimeBeg)); //������ ������ ��� ��������
  f.WriteString('Config_','WkTimeEnd',FormatDateTime('hh:nn:ss',CommonSet.WkTimeEnd)); //����� ������ ��� ��������
  f.WriteINteger('Config_','AlkoKrepMax',CommonSet.AlkoKrepMax);  //������������ �������� �����
  f.WriteInteger('Config_','ComDelay',CommonSet.ComDelay);
  f.WriteString('Config_','CashDB',CommonSet.CashDB); //���� � ���� ������
  f.WriteInteger('Config_','TypeDP',CommonSet.TypeDP);
  f.WriteInteger('Config_','OutType',CommonSet.OutType); //1 - ����������� ������, 2 - � FB
  f.WriteString('Config_','DiscPre',CommonSet.DiscPre); //������ ���������� ���� ��� ������� ������� ��� ����������
  f.WriteInteger('Config_','RoundSum',CommonSet.RoundSum); //���������� �����������
  f.WriteInteger('Config_','NoFis',CommonSet.NoFis); // � ������������ ������ 1

  f.WriteString('Config_','PreStr1',CommonSet.PreStr1);
  f.WriteString('Config_','PreStr2',CommonSet.PreStr2);
  f.WriteString('Config_','LastStr1',CommonSet.LastStr1);
  f.WriteString('Config_','LastStr2',CommonSet.LastStr2);
  f.WriteInteger('Config_','Turbo',CommonSet.Turbo);
  f.WriteInteger('Config_','StartDelaySec',CommonSet.StartDelay); //
  f.WriteInteger('Config_','ImportCount',CommonSet.ImportCount); //

  f.Free;
end;

Procedure WriteIni;
Var f:TIniFile;
begin
  f:=TIniFile.create(CurDir+CurIni);

  f.WriteString('Config_','DBName',DBName);
  f.WriteInteger('Config_','PersinId',Person.Id);
  f.WriteInteger('Config_','AutoTake',CommonSet.AutoTake);
  f.WriteInteger('Config_','CashNum',CommonSet.CashNum);
  f.WriteString('Config_','PathExport',CommonSet.PathExport);
  f.WriteString('Config_','PathImport',CommonSet.PathImport);
  f.WriteString('Config_','FtpExport',CommonSet.FtpExport);
  f.WriteString('Config_','FtpImport',CommonSet.FtpImport);
  f.WriteString('Config_','PathHistory',CommonSet.PathHistory);
  f.WriteString('Config_','PathArh',CommonSet.PathArh);
  f.WriteInteger('Config_','DepartId',CommonSet.DepartId);
  f.WriteString('Config_','DepartName',CommonSet.DepartName);
  f.WriteInteger('Config_','PeriodPing',CommonSet.PeriodPing);
  f.WriteString('Config_','PortDP', CommonSet.PortDP);
  f.WriteString('Config_','PortSC', CommonSet.PortSC);
  f.WriteString('Config_','PortCash', CommonSet.PortCash);
  f.WriteString('Config_','NetPath',CommonSet.NetPath);

  f.WriteInteger('Config_','CheckNum',Check.Num);
  f.WriteString('Config_','Articul0',CommonSet.Articul0);
  f.WriteINteger('Config_','WriteLog',CommonSet.WriteLog);
  f.WriteINteger('Config_','BNManual',CommonSet.BNManual);
  f.WriteInteger('Config_','ZNum',CommonSet.ZNum);
  f.WriteString('Config_','Password',CommonSet.Passw);

  f.Free;
end;

Procedure WriteNums;
Var f:TIniFile;
begin
  f:=TIniFile.create(CurDir+CurIni);
  f.WriteInteger('Config_','CheckNum',Nums.iCheckNum);
  f.WriteInteger('Config_','ZNum',CommonSet.ZNum);
  f.Free;
end;


Function TrimStr(StrIn:String):String;
begin
  delete(StrIn,Pos('//',StrIn),(Length(StrIn)-Pos('//',StrIn)+1)); //������ ����������
  while Pos(' ',StrIn)>0 do delete(StrIn,Pos(' ',StrIn),1);        //������ �������
  Result:=StrIn;
end;

Procedure RefreshTree(PersonalId:Integer; Tree:TTreeView; quTree:TpFIBDataSet);
//Var I:Integer;
begin
//  for I:=1 to tree.Items.Count do tree.Items[i].Delete;
  while tree.Items.Count>0 do tree.Items[0].Delete;
  RExpandLevel( Nil,Tree,quTree,PersonalId);
//  tree.Items[0].Expand(True);
  delay(10);
end;

Procedure RExpandLevel( Node : TTreeNode; Tree:TTreeView; quTree:TpFIBDataSet; PersonalId:Integer );
Var ID , i   : Integer;
    TreeNode : TTreeNode;
Begin
// ��� ������ �������� ������ ������� ������ ���,
// ��� �� ����� ���������.
  if Node = nil then ID:=0
  else ID:=Integer(Node.Data);
  quTree.Close;
  quTree.ParamByName('ParentID').Value:=ID;
  quTree.ParamByName('PersonalID').Value:=PersonalId;
  quTree.Open;
  Tree.Items.BeginUpdate;
  for i:=1 to quTree.RecordCount do
  begin    // ������� � ���� Data ����� �� ����������������� �����(ID) � �������
    TreeNode:=Tree.Items.AddChildObject(Node, quTree.FieldByName('Name').AsString, Pointer(quTree.FieldByName('ID_Classif').AsInteger));
    if quTree.FieldByName('Rights').AsInteger=1 then //��� �������
    begin
      TreeNode.ImageIndex:=0;
      TreeNode.SelectedIndex:=2;
    end;
    if quTree.FieldByName('Rights').AsInteger=0 then  //���� ������
    begin
      TreeNode.ImageIndex:=1;
      TreeNode.SelectedIndex:=3;
    end;
    // ������� ��������� (������) �������� ����� ������ ��� ����,
    // ����� ��� ��������� [+] �� ����� � �� ����� ���� �� ��������
    Tree.Items.AddChildObject(TreeNode,'', nil);
    quTree.Next;
  end;
  Tree.Items.EndUpdate;
end;



Procedure ExpandLevel( Node : TTreeNode; Tree:TTreeView; quTree:TpFIBDataSet);
Var ID , i   : Integer;
    TreeNode : TTreeNode;
Begin
// ��� ������ �������� ������ ������� ������ ���,
// ��� �� ����� ���������.
  if Node = nil then ID:=0
  else ID:=Integer(Node.Data);
  quTree.Close;
  quTree.ParamByName('ParentID').Value:=ID;
  quTree.Open;
  Tree.Items.BeginUpdate;
  // ��� ������ ������ �� ����������� ������ ������
  // ��������� ����� � TreeView, ��� �������� ����� � ���,
  // ������� �� ������ ��� "��������"
  for i:=1 to quTree.RecordCount do
  begin    // ������� � ���� Data ����� �� ����������������� �����(ID) � �������
    TreeNode:=Tree.Items.AddChildObject(Node, quTree.FieldByName('Name').AsString, Pointer(quTree.FieldByName('ID').AsInteger));
    if quTree.FieldByName('ID_PARENT').AsInteger=0 then
    begin
      TreeNode.ImageIndex:=0;
      TreeNode.SelectedIndex:=2;
    end;
    if quTree.FieldByName('ID_PARENT').AsInteger>0 then
    begin
      TreeNode.ImageIndex:=1;
      TreeNode.SelectedIndex:=3;
    end;
    // ������� ��������� (������) �������� ����� ������ ��� ����,
    // ����� ��� ��������� [+] �� ����� � �� ����� ���� �� ��������
    Tree.Items.AddChildObject(TreeNode,'', nil);
    quTree.Next;
  end;
  Tree.Items.EndUpdate;
end;



Procedure CheckNodeOn(Node:TTreeNode;bOn:Boolean);
Var ChildNode,CurNode:TTreeNode;
begin
  ChildNode:=Node.getFirstChild;
  while ChildNode<>Nil do
  begin
    if bOn then ChildNode.ImageIndex:=2
    else ChildNode.ImageIndex:=0;
    CurNode:=ChildNode;
    CheckNodeOn(CurNode,bOn);
    ChildNode:=CurNode.getNextChild(CurNode);
  end;
end;


procedure Delay(MSecs: Longint);
var
  FirstTickCount, Now: Longint;
begin
  FirstTickCount := GetTickCount;
  repeat
    Application.ProcessMessages;
    { allowing access to other controls, etc. }
    Now := GetTickCount;
  until (Now - FirstTickCount >= MSecs) or (Now < FirstTickCount);
end;


Procedure ReadKlIni;
Var f:TIniFile;
begin
  f:=TIniFile.create(CurDir+KlIni);

  DBName:=f.ReadString('Config_','DBName','C:\Database\Ust\Ust.GDB');
  CommonSet.PathArh:=f.ReadString('Config_','PathArh',CurDir+'Arh\');

  f.WriteString('Config_','DBName',DBName);
  f.WriteString('Config_','PathArh',CommonSet.PathArh);
  f.Free;
end;

Procedure WriteKlIni;
Var f:TIniFile;
begin
  f:=TIniFile.create(CurDir+KlIni);

  f.WriteString('Config_','DBName',DBName);
  f.WriteString('Config_','PathArh',CommonSet.PathArh);
  f.Free;
end;


end.
