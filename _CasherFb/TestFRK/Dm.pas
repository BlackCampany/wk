unit Dm;

interface

uses
  SysUtils, Classes, DB, ADODB, ImgList, Controls, FIBDatabase,
  pFIBDatabase, FIBDataSet, pFIBDataSet, FIBQuery, pFIBQuery,
  pFIBStoredProc, cxStyles, VaComm, VaClasses, VaSystem, WinSpool, Windows;

type
  TdmC = class(TDataModule)
    dsPersonal: TDataSource;
    dsClassif: TDataSource;
    dsFuncList: TDataSource;
    imState: TImageList;
    CasherDb: TpFIBDatabase;
    trSelect: TpFIBTransaction;
    trUpdate: TpFIBTransaction;
    quPer: TpFIBDataSet;
    quPerID: TFIBIntegerField;
    quPerID_PARENT: TFIBIntegerField;
    quPerNAME: TFIBStringField;
    quPerUVOLNEN: TFIBBooleanField;
    quPerCheck: TFIBStringField;
    quPerMODUL1: TFIBBooleanField;
    quPerMODUL2: TFIBBooleanField;
    quPerMODUL3: TFIBBooleanField;
    quPerMODUL4: TFIBBooleanField;
    quPerMODUL5: TFIBBooleanField;
    quPerMODUL6: TFIBBooleanField;
    dsPer: TDataSource;
    taPersonal: TpFIBDataSet;
    taPersonalID: TFIBIntegerField;
    taPersonalID_PARENT: TFIBIntegerField;
    taPersonalNAME: TFIBStringField;
    taPersonalUVOLNEN: TFIBBooleanField;
    taPersonalPASSW: TFIBStringField;
    taPersonalMODUL1: TFIBBooleanField;
    taPersonalMODUL2: TFIBBooleanField;
    taPersonalMODUL3: TFIBBooleanField;
    taPersonalMODUL4: TFIBBooleanField;
    taPersonalMODUL5: TFIBBooleanField;
    taPersonalMODUL6: TFIBBooleanField;
    taRClassif: TpFIBDataSet;
    quFuncList: TpFIBDataSet;
    taRClassifID_PERSONAL: TFIBIntegerField;
    taRClassifID_CLASSIF: TFIBIntegerField;
    taRClassifRIGHTS: TFIBIntegerField;
    quFuncListID_PERSONAL: TFIBIntegerField;
    quFuncListNAME: TFIBStringField;
    quFuncListPREXEC: TFIBBooleanField;
    quFuncListCOMMENT: TFIBStringField;
    quClassif: TpFIBDataSet;
    quClassifID_CLASSIF: TFIBIntegerField;
    quClassifRIGHTS: TFIBIntegerField;
    quClassifID_PARENT: TFIBIntegerField;
    quClassifNAME: TFIBStringField;
    quPersonal: TpFIBDataSet;
    prChangeRFunction: TpFIBStoredProc;
    prExistPersonal: TpFIBStoredProc;
    prDelPersonal: TpFIBStoredProc;
    taFuncList: TpFIBDataSet;
    taFuncListNAME: TFIBStringField;
    taFuncListCOMMENT: TFIBStringField;
    trDel: TpFIBTransaction;
    quCanDo: TpFIBDataSet;
    quCanDoID_PERSONAL: TFIBIntegerField;
    quCanDoNAME: TFIBStringField;
    quCanDoPREXEC: TFIBBooleanField;
    taStates: TpFIBDataSet;
    taStatesID: TFIBIntegerField;
    taStatesNAME: TFIBStringField;
    taStatesBARCODE: TFIBStringField;
    taStatesKEY_POSITION: TFIBIntegerField;
    taStatesID_DEVICE: TFIBIntegerField;
    taCF_Operations: TpFIBDataSet;
    taOperations: TpFIBDataSet;
    taOperationsID: TFIBIntegerField;
    taOperationsNAME: TFIBStringField;
    taOperationsID_DEVICE: TFIBIntegerField;
    taClassif: TpFIBDataSet;
    taClassifID: TFIBIntegerField;
    taClassifID_PARENT: TFIBIntegerField;
    taClassifTYPE_CLASSIF: TFIBIntegerField;
    taClassifNAME: TFIBStringField;
    taDeparts: TpFIBDataSet;
    taDepartsID: TFIBIntegerField;
    taDepartsNAME: TFIBStringField;
    taDepartsPAR1: TFIBIntegerField;
    taDepartsPAR2: TFIBIntegerField;
    taCardScla: TpFIBDataSet;
    taCardSclaARTICUL: TFIBStringField;
    taCardSclaCLASSIF: TFIBIntegerField;
    taCardSclaDEPART: TFIBIntegerField;
    taCardSclaNAME: TFIBStringField;
    taCardSclaCARD_TYPE: TFIBIntegerField;
    taCardSclaMESURIMENT: TFIBStringField;
    taCardSclaPRICE_RUB: TFIBFloatField;
    taCardSclaDISCOUNT: TFIBFloatField;
    taCardSclaSCALE: TFIBStringField;
    dsCF_Operations: TDataSource;
    taCF_OperationsID_OPERATIONS: TFIBIntegerField;
    taCF_OperationsKEY_CODE: TFIBIntegerField;
    taCF_OperationsKEY_STATUS: TFIBIntegerField;
    taCF_OperationsKEY_CHAR: TFIBStringField;
    taCF_OperationsID_STATES: TFIBIntegerField;
    taCF_OperationsBARCODE: TFIBStringField;
    taCF_OperationsKEY_POSITION: TFIBIntegerField;
    taCF_OperationsID_CLASSIF: TFIBIntegerField;
    taCF_OperationsARTICUL: TFIBStringField;
    taCF_OperationsID_DEPARTS: TFIBIntegerField;
    taCF_OperationsBAR: TFIBStringField;
    taCF_OperationsNAME: TFIBStringField;
    taCF_OperationsNAME1: TFIBStringField;
    dsOperations: TDataSource;
    dsStates: TDataSource;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    quClassifR: TpFIBDataSet;
    quClassifRTYPE_CLASSIF: TFIBIntegerField;
    quClassifRID: TFIBIntegerField;
    quClassifRIACTIVE: TFIBSmallIntField;
    quClassifRID_PARENT: TFIBIntegerField;
    quClassifRNAME: TFIBStringField;
    quClassifRIEDIT: TFIBSmallIntField;
    taCF_OperationsNAMECL: TFIBStringField;
    taCF_All: TpFIBDataSet;
    taCF_AllID_OPERATIONS: TFIBIntegerField;
    taCF_AllKEY_CODE: TFIBIntegerField;
    taCF_AllKEY_STATUS: TFIBIntegerField;
    taCF_AllKEY_CHAR: TFIBStringField;
    taCF_AllID_STATES: TFIBIntegerField;
    taCF_AllBARCODE: TFIBStringField;
    taCF_AllKEY_POSITION: TFIBIntegerField;
    taCF_AllID_CLASSIF: TFIBIntegerField;
    taCF_AllARTICUL: TFIBStringField;
    taCF_AllID_DEPARTS: TFIBIntegerField;
    taCF_AllBAR: TFIBStringField;
    dsCardScla: TDataSource;
    taCardSclaBARCODE: TFIBStringField;
    taCardSclaPRICEBAR: TFIBFloatField;
    taCF_OperationsNAMECS: TFIBStringField;
    dsDeparts: TDataSource;
    taCF_OperationsNAMEDEP: TFIBStringField;
    taBarCode: TpFIBDataSet;
    taBarCodeBARCODE: TFIBStringField;
    taBarCodeCARDARTICUL: TFIBStringField;
    taBarCodeCARDSIZE: TFIBStringField;
    taBarCodePRICERUB: TFIBFloatField;
    taBarCodeQUANTITY: TFIBFloatField;
    taClassifIACTIVE: TFIBSmallIntField;
    taClassifIEDIT: TFIBSmallIntField;
    taTax: TpFIBDataSet;
    taTaxARTICUL: TFIBStringField;
    taTaxTAXINDEX: TFIBSmallIntField;
    taTaxTAX: TFIBFloatField;
    taTaxType: TpFIBDataSet;
    taTaxTypeID: TFIBIntegerField;
    taTaxTypePRIOR: TFIBSmallIntField;
    taTaxTypeNAME: TFIBStringField;
    taDiscSum: TpFIBDataSet;
    taCardSclaDISCQUANT: TFIBFloatField;
    taDiscCard: TpFIBDataSet;
    taDiscCardBARCODE: TFIBStringField;
    taDiscCardNAME: TFIBStringField;
    taDiscCardPERCENT: TFIBFloatField;
    taDiscCardCLIENTINDEX: TFIBIntegerField;
    prSetDiscCardStop: TpFIBStoredProc;
    taDiscSumFROMTIME: TFIBIntegerField;
    taDiscSumBARCODE: TFIBStringField;
    taDiscSumSUMMA: TFIBFloatField;
    taDiscSumDISCOUNT: TFIBFloatField;
    taCredCard: TpFIBDataSet;
    taCredCardID: TFIBSmallIntField;
    taCredCardNAME: TFIBStringField;
    taCredCardCLIENTINDEX: TFIBIntegerField;
    taCredCardLIMITSUM: TFIBFloatField;
    taCredCardCANRETURN: TFIBIntegerField;
    taCredPref: TpFIBDataSet;
    taCredPrefPREFIX: TFIBStringField;
    taCredPrefID_CARD: TFIBIntegerField;
    taPersonalBARCODE: TFIBStringField;
    taCheck: TpFIBDataSet;
    dsCheck: TDataSource;
    trSelCh: TpFIBTransaction;
    trUpdCh: TpFIBTransaction;
    quFindBar: TpFIBDataSet;
    quFindBarBARCODE: TFIBStringField;
    quFindBarCARDSIZE: TFIBStringField;
    quFindBarQUANTITY: TFIBFloatField;
    quFindBarPRICERUB: TFIBFloatField;
    quFindBarARTICUL: TFIBStringField;
    quFindBarDEPART: TFIBIntegerField;
    quFindBarNAME: TFIBStringField;
    quFindBarPRICE_RUB: TFIBFloatField;
    prSaveCheck: TpFIBStoredProc;
    quClassifCards: TpFIBDataSet;
    quCards: TpFIBDataSet;
    dsCards: TDataSource;
    quCardsARTICUL: TFIBStringField;
    quCardsCLASSIF: TFIBIntegerField;
    quCardsDEPART: TFIBIntegerField;
    quCardsNAME: TFIBStringField;
    quCardsCARD_TYPE: TFIBIntegerField;
    quCardsMESURIMENT: TFIBStringField;
    quCardsPRICE_RUB: TFIBFloatField;
    quCardsDISCQUANT: TFIBFloatField;
    quCardsDISCOUNT: TFIBFloatField;
    quCardsSCALE: TFIBStringField;
    quCardsFind: TpFIBDataSet;
    quCardsFindARTICUL: TFIBStringField;
    quCardsFindCLASSIF: TFIBIntegerField;
    quCardsFindDEPART: TFIBIntegerField;
    quCardsFindNAME: TFIBStringField;
    quCardsFindCARD_TYPE: TFIBIntegerField;
    quCardsFindMESURIMENT: TFIBStringField;
    quCardsFindPRICE_RUB: TFIBFloatField;
    quCardsFindDISCQUANT: TFIBFloatField;
    quCardsFindDISCOUNT: TFIBFloatField;
    quCardsFindSCALE: TFIBStringField;
    taDiscPre: TpFIBDataSet;
    taDiscPreBARCODE: TFIBStringField;
    taDiscPreNAME: TFIBStringField;
    taDiscPrePERCENT: TFIBFloatField;
    taDiscPreIACTIVE: TFIBSmallIntField;
    taDiscFind: TpFIBDataSet;
    taDiscFindBARCODE: TFIBStringField;
    taDiscFindNAME: TFIBStringField;
    taDiscFindPERCENT: TFIBFloatField;
    taDiscFindCLIENTINDEX: TFIBIntegerField;
    taDiscFindIACTIVE: TFIBSmallIntField;
    prCalcDisc: TpFIBStoredProc;
    cxStyle10: TcxStyle;
    prClearCheck: TpFIBStoredProc;
    prFormLog: TpFIBStoredProc;
    prCalcDiscSum: TpFIBStoredProc;
    trSelBar: TpFIBTransaction;
    prAddCard: TpFIBStoredProc;
    prAddBar: TpFIBStoredProc;
    prReadCheck: TpFIBStoredProc;
    quTestSail: TpFIBDataSet;
    dsCredCard: TDataSource;
    taCheckTr: TpFIBDataSet;
    prClearCheckTr: TpFIBStoredProc;
    taCheckTrCASHNUM: TFIBIntegerField;
    taCheckTrNUMPOS: TFIBIntegerField;
    taCheckTrARTICUL: TFIBStringField;
    taCheckTrRBAR: TFIBStringField;
    taCheckTrNAME: TFIBStringField;
    taCheckTrQUANT: TFIBFloatField;
    taCheckTrPRICE: TFIBFloatField;
    taCheckTrDPROC: TFIBFloatField;
    taCheckTrDSUM: TFIBFloatField;
    taCheckTrRSUM: TFIBFloatField;
    taCheckTrDEPART: TFIBSmallIntField;
    taCheckTrOPERATION: TFIBSmallIntField;
    taCheckTrTYPEP: TFIBSmallIntField;
    taCheckTrVIDP: TFIBSmallIntField;
    prSaveCheckTr: TpFIBStoredProc;
    taMoneyType: TpFIBDataSet;
    taMoneyTypeID: TFIBSmallIntField;
    taMoneyTypeIVAL: TFIBIntegerField;
    quCheck: TpFIBDataSet;
    quCheckARTICUL: TFIBStringField;
    quCheckRBAR: TFIBStringField;
    quCheckNAME: TFIBStringField;
    quCheckPRICE: TFIBFloatField;
    quCheckDEPART: TFIBSmallIntField;
    quCheckDPROC: TFIBFloatField;
    quCheckNUMPOS: TFIBIntegerField;
    quCheckQUANT: TFIBFloatField;
    quCheckDSUM: TFIBFloatField;
    quCheckPRSUM: TFIBFloatField;
    trMoneySel: TpFIBTransaction;
    quMoneyLast: TpFIBDataSet;
    quMoneyLastINOUTDATE: TFIBDateTimeField;
    quMoneyLastENDSUM: TFIBFloatField;
    quMoneyLastID: TFIBIntegerField;
    quCashSum: TpFIBDataSet;
    trCashSel: TpFIBTransaction;
    quCashSumCASHSUM: TFIBFloatField;
    taMoneySum: TpFIBDataSet;
    trMoneyUpd: TpFIBTransaction;
    taMoneySumID: TFIBIntegerField;
    taMoneySumINOUTDATE: TFIBDateTimeField;
    taMoneySumBEGSUM: TFIBFloatField;
    taMoneySumINOUTSUM: TFIBFloatField;
    taMoneySumENDSUM: TFIBFloatField;
    taMoneySumPERSONID: TFIBIntegerField;
    prDelCard: TpFIBStoredProc;
    trDelCard: TpFIBTransaction;
    trBN: TpFIBTransaction;
    prAddBNTR: TpFIBStoredProc;
    quBnTr: TpFIBDataSet;
    quBnTrBNBAR: TFIBStringField;
    quBnTrIDTR: TFIBIntegerField;
    quBnTrPOINTNUM: TFIBIntegerField;
    quBnTrCOMMENT: TFIBStringField;
    quBnTrBNSUM: TFIBFloatField;
    quBnTrAUTHCODE: TFIBStringField;
    quBnTrCARDTYPE: TFIBStringField;
    quPersName: TpFIBDataSet;
    trPers: TpFIBTransaction;
    quPersNameNAME: TFIBStringField;
    trSelChTr: TpFIBTransaction;
    trUpdChTr: TpFIBTransaction;
    quDel: TpFIBQuery;
    trSelMessur: TpFIBTransaction;
    quIM: TpFIBDataSet;
    quIMCARD_TYPE: TFIBIntegerField;
    quIMMESURIMENT: TFIBStringField;
    taFixDisc: TpFIBDataSet;
    taFixDiscSART: TFIBStringField;
    taFixDiscPROC: TFIBFloatField;
    taCheckCASHNUM: TFIBIntegerField;
    taCheckNUMPOS: TFIBIntegerField;
    taCheckARTICUL: TFIBStringField;
    taCheckRBAR: TFIBStringField;
    taCheckNAME: TFIBStringField;
    taCheckQUANT: TFIBFloatField;
    taCheckPRICE: TFIBFloatField;
    taCheckDPROC: TFIBFloatField;
    taCheckDSUM: TFIBFloatField;
    taCheckRSUM: TFIBFloatField;
    taCheckDEPART: TFIBSmallIntField;
    taDiscCardIACTIVE: TFIBSmallIntField;
    VaWaitMessage1: TVaWaitMessage;
    DevPrint: TVaComm;
    trSel1: TpFIBTransaction;
    prGetNFChNum: TpFIBStoredProc;
    prGetId: TpFIBStoredProc;
    taCheckEDIZM: TFIBStringField;
    quFindBarMESURIMENT: TFIBStringField;
    quDSum: TpFIBDataSet;
    quDSumOPER: TFIBSmallIntField;
    quDSumPAYMENT: TFIBSmallIntField;
    quDSumDSUM: TFIBFloatField;
    quNarItog: TpFIBDataSet;
    quNarItogSUMSALE: TFIBFloatField;
    quNarItogSUMRET: TFIBFloatField;
    quZList: TpFIBDataSet;
    quZListIZ: TFIBIntegerField;
    quZListSUMSALE: TFIBFloatField;
    quZListSUMRET: TFIBFloatField;
    prAddClassif: TpFIBStoredProc;
    quNameClFind: TpFIBDataSet;
    quNameClFindNAME: TFIBStringField;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure quFuncListBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    function TypeEdIzm(sArt:String):Integer;
    { Public declarations }
    Procedure prDevOpen(StrP:String;iLog:ShortInt);
    Procedure prDevClose(StrP:String;iLog:ShortInt);
    Procedure prCutDoc(StrP:String;iLog:ShortInt); //�������
    Procedure prRing(StrP:String); //��������
    Procedure prSetFont(StrP:String;iFont:INteger;iLog:ShortInt); //��������� �����
    Procedure prPrintStr(StrP,S:String); //
    Procedure PrintStrDev(StrP,S:String);
  end;

Function GetId(ta:String):Integer;
Function CanDo(Name_F:String):Boolean;
Function FindPers(sP:String):Boolean;
Function CheckOpen:Boolean;
Procedure CalcDiscont(sArt:String;PosNum:Integer;Price:Currency;Quantity:Real;Summa:Currency;DiscProc:Real;Var DiscountProc:Real;Var DiscountSum:Real);
Function FindDiscount(sBar:String):Boolean;
Procedure FormLog(NAMEOP,CONTENT:String);

Function prOpenPrinter(PName:String):Boolean;
Procedure prWritePrinter(S:String);
Procedure prClosePrinter(PName:String);

procedure prWriteLogPS(sStr:String);

Function prGetNFZ:Integer;
Function prSetNFZ:Integer;
Function prGetNFCheckNum:Integer;
Procedure OpenMoneyBox;

Procedure PrintStr(StrP:String);
Function FindClName(iArt:Integer):String;


var
  dmC: TdmC;

  bPrintOpen: Boolean = False; //��� LPT
  PrintHandle:THandle; //��� LPT

implementation

uses MainAdm, Un1, Passw, UnCash, u2fdk;

{$R *.dfm}
Function FindClName(iArt:Integer):String;
begin
  with dmC do
  begin
    quNameClFind.Active:=False;
    quNameClFind.ParamByName('IART').AsInteger:=iArt;
    quNameClFind.Active:=True;
    if quNameClFind.RecordCount>0 then
    begin
      quNameClFind.First;
      Result:=quNameClFindNAME.AsString;
    end else Result:='';
    quNameClFind.Active:=False;
  end;
end;



Procedure OpenMoneyBox;
Var Buff:Array[1..5] of Char;
begin
  with dmC do
  begin
  try

    Buff[1]:=#$1B;   //�������������
    Buff[2]:=#$70;
    Buff[3]:=#$00;
    Buff[4]:=#$32;
    Buff[5]:=#$32;

    DevPrint.WriteBuf(Buff,5);
    delay(50);

  finally
//    DevPrint.Close;
  end;
  end;
end;


Procedure PrintStr(StrP:String);
Var Buff:Array[1..100] of Char;
    i,iCount:Integer;
begin
  with dmC do
  begin
    StrP:=AnsiToOemConvert(StrP);
    iCount:=Length(StrP);
    for i:=1 to iCount do Buff[i]:=StrP[i];
    Buff[iCount+1]:=#$0A;
    try
      DevPrint.WriteBuf(Buff,iCount+1);
    except
    end;
    delay(CommonSet.ComDelay);
  end;
end;


Function prGetNFCheckNum:Integer;
begin
  with dmC do
  begin
//?CASHNUM, ?SHOPINDEX, ?ZNUMBER
    prGetNFChNum.ParamByName('SHOPINDEX').AsInteger:=1;
    prGetNFChNum.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
    prGetNFChNum.ParamByName('ZNUMBER').AsInteger:=prGetNFZ;
    prGetNFChNum.ExecProc;
    result:=prGetNFChNum.ParamByName('CHECKNUM').AsInteger+1;
  end;
end;

Function prSetNFZ:Integer;
begin
  with dmC do
  begin
    result:=GetId('SetZ');
  end;
end;


Function prGetNFZ:Integer;
begin
  with dmC do
  begin
    result:=GetId('CurZ');
  end;
end;


procedure prWriteLogPS(sStr:String);
begin
//���� ������
end;


Procedure TdmC.PrintStrDev(StrP,S:String);
Var Buff:Array[1..100] of Char;
    i,iCount:Integer;
begin
  if pos('fis',StrP)>0 then PrintNFStr(S);

  if (StrP[1]='S')or(StrP[1]='s')or(StrP[1]='B')or(StrP[1]='b') then
  begin
    if S='-' then s:='----------------------------------------'
  end else
  begin
    if S='-' then s:='                                        '
  end;
  if pos('COM',StrP)>0 then
  begin

    S:=AnsiToOemConvert(S);
    iCount:=Length(S);
//    for i:=1 to iCount do Buff[i]:=S[i];
    for i:=1 to iCount do prINBuf(S[i]);
    Buff[iCount+1]:=#$0A; prINBuf(#$0A);

    try
//      DevPrint.WriteBuf(Buff,iCount+1);
    except
    end;
//    delay(CommonSet.ComDelay);
  end;
  if pos('LPT',StrP)>0 then
  begin
    if (StrP[1]='S')or(StrP[1]='s') then
    begin
      if bPrintOpen then prWritePrinter(AnsiToOemConvert(S)+#$0D+#$0A);
    end else
    begin
      if bPrintOpen then prWritePrinter(AnsiToOemConvert(S)+#$0D+#$0A);
    end;
  end;
end;



Function prOpenPrinter(PName:String):Boolean;
Var  DocInfo1: TDocInfo1;
begin
  prWriteLog('   �������� ������� - '+PName);
  Result:=False;
  if OpenPrinter(PChar(PName), PrintHandle, nil) then
  begin
    Result:=True;
    with DocInfo1 do begin
      pDocName := PChar('Tmp_doc');
      pOutputFile := nil;
      pDataType := 'RAW';
    end;
    StartDocPrinter(PrintHandle, 1, @DocInfo1);
    StartPagePrinter(PrintHandle);
  end
  else prWriteLog('   ������� �� ���� - '+PName);
end;

Procedure prClosePrinter(PName:String);
begin
  prWriteLog('   �������� ������� - '+PName);
  EndPagePrinter(PrintHandle);
  EndDocPrinter(PrintHandle);
  ClosePrinter(PrintHandle);
  bPrintOpen:=False;
end;

Procedure prWritePrinter(S:String);
Var  N: DWORD;
begin
  WritePrinter(PrintHandle, PChar(S), Length(S), N);
end;



Procedure TdmC.prPrintStr(StrP,S:String);
Var Buff:Array[1..100] of Char;
    i,iCount:Integer;
begin
//  prWriteLogPS('   ������ ������ ('+StrP+') "'+S+'"');
  if pos('fis',StrP)>0 then PrintNFStr(S);

  if (StrP[1]='S')or(StrP[1]='s')or(StrP[1]='B')or(StrP[1]='b') then
  begin
    if S='-' then s:='----------------------------------------'
  end else
  begin
    if S='-' then s:='                                        '
  end;
  if pos('COM',StrP)>0 then
  begin

    S:=AnsiToOemConvert(S);
    iCount:=Length(S);

//    for i:=1 to iCount do Buff[i]:=S[i];
    for i:=1 to iCount do prInBuf(S[i]);

    Buff[iCount+1]:=#$0A;
    prInBuf(#$0A);

    try
//      DevPrint.WriteBuf(Buff,iCount+1);
    except
    end;
//    delay(CommonSet.ComDelay);
  end;
  if pos('LPT',StrP)>0 then
  begin
    if (StrP[1]='S')or(StrP[1]='s') then
    begin
      if bPrintOpen then prWritePrinter(AnsiToOemConvert(S)+#$0D+#$0A);
    end else
    begin
      if bPrintOpen then prWritePrinter(AnsiToOemConvert(S)+#$0D+#$0A);
    end;
  end;
end;


Procedure TdmC.prSetFont(StrP:String;iFont:INteger;iLog:ShortInt); //��������� �����
Var iFl,iFl1:Byte;
    Buff:Array[1..100] of Char;
    iTypeP:INteger;
begin
  if iLog=1 then prWriteLogPS('   ��������� ����� '+IntToStr(iFont)+'('+StrP+')');
  if pos('fis',StrP)>0 then SelectF(iFont);
  if pos('COM',StrP)>0 then
  begin
    iTypeP:=0;
    if (StrP[1]='S')or(StrP[1]='s') then iTypeP:=1;

    if iTypeP=0 then //������ �� ���������
    begin
      iFl:=$00;
      Case iFont of
      0: begin iFl:=$01 end;  //�������
      1: begin iFl:=$21 end;  //������� ������
      2: begin iFl:=$11 end;  //������� ������
      3: begin iFl:=$81 end;  //�������������
      4: begin iFl:=$31 end;  //������� ��� � ���

      5: begin iFl:=$09 end;  //�������
      6: begin iFl:=$29 end;  //������� ������
      7: begin iFl:=$19 end;  //������� ������
      8: begin iFl:=$89 end;  //�������������
      9: begin iFl:=$39 end;  //������� ��� � ���

      10: begin iFl:=$00 end; //�������
      11: begin iFl:=$20 end; //������� ������
      12: begin iFl:=$10 end; //������� ������
      13: begin iFl:=$80 end; //�������������
      14: begin iFl:=$30 end; //������� ��� � ���

      15: begin iFl:=$08 end; //�������
      16: begin iFl:=$28 end; //������� ������
      17: begin iFl:=$18 end; //������� ������
      18: begin iFl:=$88 end; //�������������
      19: begin iFl:=$38 end; //������� ��� � ���

      end;

      Buff[1]:=#$1B;
      Buff[2]:=#$21;
      Buff[3]:=chr(iFl);

      prInBuf(Buff[1]);
      prInBuf(Buff[2]);
      prInBuf(Buff[3]);

      try
//        DevPrint.WriteBuf(Buff,3);
      except
      end;
    end;

    if iTypeP=1 then //Star600
    begin
      iFl:=$30;  iFl1:=$30;
      Case iFont of
      10: begin iFl:=$30; iFl1:=$30; end; //�������
      13: begin iFl:=$30; iFl1:=$30; end; //�������������
      14: begin iFl:=$31; iFl1:=$31; end; //������� ��� � ���
      15: begin iFl:=$30; iFl1:=$30; end; //�������
      end;

      Buff[1]:=#$1B;
      Buff[2]:=#$69; // 30 30
      Buff[3]:=chr(iFl);
      Buff[4]:=chr(iFl1);

      prInBuf(Buff[1]);
      prInBuf(Buff[2]);
      prInBuf(Buff[3]);
      prInBuf(Buff[4]);

      try
//        DevPrint.WriteBuf(Buff,4);
      except
      end;
    end;
  end;

  if pos('LPT',StrP)>0 then
  begin
    iTypeP:=0;
    if (StrP[1]='S')or(StrP[1]='s') then iTypeP:=1;
    if (StrP[1]='B')or(StrP[1]='b') then iTypeP:=2;
    if iTypeP=1 then //Star600
    begin
      iFl:=$30;  iFl1:=$30;
      Case iFont of
      10: begin iFl:=$30; iFl1:=$30; end; //�������
      13: begin iFl:=$30; iFl1:=$30; end; //�������������
      14: begin iFl:=$31; iFl1:=$31; end; //������� ��� � ���
      15: begin iFl:=$30; iFl1:=$30; end; //�������
      end;

      if bPrintOpen then prWritePrinter(#$1B+#$69+chr(iFl)+chr(iFl1));
    end;
    if iTypeP=2 then //Star600
    begin
      iFl:=$00;
      Case iFont of
      0: begin iFl:=$01 end;  //�������
      1: begin iFl:=$21 end;  //������� ������
      2: begin iFl:=$11 end;  //������� ������
      3: begin iFl:=$81 end;  //�������������
      4: begin iFl:=$31 end;  //������� ��� � ���

      5: begin iFl:=$09 end;  //�������
      6: begin iFl:=$29 end;  //������� ������
      7: begin iFl:=$19 end;  //������� ������
      8: begin iFl:=$89 end;  //�������������
      9: begin iFl:=$39 end;  //������� ��� � ���

      10: begin iFl:=$00 end; //�������
      11: begin iFl:=$20 end; //������� ������
      12: begin iFl:=$10 end; //������� ������
      13: begin iFl:=$80 end; //�������������
      14: begin iFl:=$30 end; //������� ��� � ���

      15: begin iFl:=$08 end; //�������
      16: begin iFl:=$28 end; //������� ������
      17: begin iFl:=$18 end; //������� ������
      18: begin iFl:=$88 end; //�������������
      19: begin iFl:=$38 end; //������� ��� � ���
      end;

      if bPrintOpen then prWritePrinter(#$1B+#$21+chr(iFl));
    end;
  end;
end;


Procedure TdmC.prRing(StrP:String); //��������
Var Buff:Array[1..100] of Char;
begin
  if pos('COM',StrP)>0 then
  begin
    if (StrP[1]='S')or(StrP[1]='s') then
    begin

    end else
    begin
      Buff[1]:=#$1B;
      Buff[2]:=#$70;
      Buff[3]:=#$00;
      Buff[4]:=#$FF;
      Buff[5]:=#$FF;
      try
        DevPrint.WriteBuf(Buff,5);
        delay(500);
      except
      end;
    end;
  end;
end;


Procedure TdmC.prCutDoc(StrP:String;iLog:ShortInt); //�������
//  Memo1.Lines.Add('�����');
Var Buff:Array[1..14] of Char;
    i:Integer;
    iTypeP:INteger;
begin
//  SelFont(0);
  if iLog=1 then prWriteLogPS(' �����.('+StrP+')');
  iTypeP:=0; //��� �� ���������
  if pos('fis',StrP)>0 then
  begin
    PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');
    CutDoc;
  end;

  if pos('COM',StrP)>0 then
  begin
    if (StrP[1]='S')or(StrP[1]='s') then iTypeP:=1;
    if (StrP[1]='A')or(StrP[1]='a') then iTypeP:=2;

    if (iTypeP=2)or(iTypeP=0) then //������ � ��� ���������
    begin
      {Buff[1]:=#$0A;

      try
        for i:=1 to 7 do DevPrint.WriteBuf(Buff,1); //�������
      except
      end;

      Buff[1]:=#$1B;
      Buff[2]:=#$69;
      try
        DevPrint.WriteBuf(Buff,2);
      except
      end;
      }
      delay(CommonSet.ComDelay);  //��� ����� �� ��������
      prSetFont(StrP,0,0);
      PrintStr(' ');
      PrintStr(' ');

      Buff[1]:=#$20;
      Buff[2]:=#$0A;

      Buff[3]:=#$20;
      Buff[4]:=#$0A;

      Buff[5]:=#$20;
      Buff[6]:=#$0A;

      Buff[7]:=#$20;
      Buff[8]:=#$0A;

      Buff[9]:=#$20;
      Buff[10]:=#$0A;

      Buff[11]:=#$1B;
      Buff[12]:=#$69;

      Buff[13]:=#$00;
      Buff[14]:=#$00;

      try
        DevPrint.WriteBuf(Buff,12);
        delay(CommonSet.ComDelay);
      except
      end;

//      PrintStr(StrP,' ');
    end;
    if iTypeP=1 then //Star 600
    begin
      Buff[1]:=#$1B;
      Buff[2]:=#$64;
      Buff[3]:=#$33;
      try
        DevPrint.WriteBuf(Buff,3);
        delay(100);
      except
      end;
    end;
  end;
  if pos('LPT',StrP)>0 then
  begin
    if (StrP[1]='S')or(StrP[1]='s') then iTypeP:=1;
    if (StrP[1]='B')or(StrP[1]='b') then iTypeP:=2;
    if iTypeP=1 then //Star 600
    begin
      if bPrintOpen then prWritePrinter(#$1B+#$64+#$33);
    end else
    begin
      if iTypeP=2 then
      begin
        if bPrintOpen then
        begin
          for i:=1 to 7 do prWritePrinter(#$0A);
          prWritePrinter(#$1B+#$69);
        end;
      end else
        if bPrintOpen then prWritePrinter(#$1B+#$69);
    end;
  end;
//  BufPr.iC:=0;
end;


Procedure TdmC.prDevClose(StrP:String;iLog:ShortInt);
Var pName:String;
//    kDelay:INteger;
begin //�������� ����������
  if iLog=1 then prWriteLogPS(' ��������.('+StrP+')');
  if pos('fis',StrP)>0 then CloseNFDoc;
  if pos('COM',StrP)>0 then
  begin
    try
//      kDelay:=(BufPr.iC div 40)-10;
//      if kDelay<0 then kDelay:=0;
//      delay(CommonSet.ComDelay*5+trunc(kDelay*CommonSet.ComDelay/2));

//      delay(CommonSet.ComDelay*2);
//      DevPrint.Close;
    except
    end;
  end;
  if pos('LPT',StrP)>0 then
  begin
    pName:=Copy(StrP,POS('LPT',StrP)+3,length(StrP)-POS('LPT',StrP)+2);
//    Memo1.Lines.Add(FormatDateTime('dd.mm.yyyy hh.nn.ss',now)+'  '+pName);
    prClosePrinter(pName);
  end;
//  BufPr.iC:=0;
end;


Procedure TdmC.prDevOpen(StrP:String;iLog:ShortInt);
Var Buff:Array[1..3] of Char;
    bAxiohm,bEpson:Boolean;
    PName:String;
begin  //�������� ����������
//  Memo1.Lines.Add('����-'+StrP);
  BufPr.iC:=0;
  if DevPrint.Active then
  begin
    if pos('COM',StrP)>0 then
    begin
      try
        bAxiohm:=False;
        bEpson:=False;
        if (StrP[1]='A') or(StrP[1]='Z') then  bAxiohm:=True;
        if (StrP[1]='E')or(StrP[1]='e') then   bEpson:=True;

       //��� ����������� ��� ������� - � ������ ��������� ����� �� �����
        if bAxiohm then
        begin
          Buff[1]:=#$1B;
          Buff[2]:=#$74;
          Buff[3]:=#$07;
          DevPrint.WriteBuf(Buff,3);
        end;
      //��� ����������� ��� Epsona - � ������ ��������� ����� �� �����
        if bEpson then
        begin
          Buff[1]:=#$1B;
          Buff[2]:=#$74;
          Buff[3]:=#$11;
          DevPrint.WriteBuf(Buff,3);
        end;

      except
        if iLog=1 then prWriteLogPS('   ������ �������� - '+StrP);
      end;
    end;

    exit;
  end;
  
  if iLog=1 then prWriteLogPS('   �������� - '+StrP);
  if pos('fis',StrP)>0 then OpenNFDoc;

  if pos('COM',StrP)>0 then
  begin
    try
      bAxiohm:=False;
      bEpson:=False;
      if (StrP[1]='A') or(StrP[1]='Z') then
      begin
        bAxiohm:=True;
        Delete(StrP,1,1);
      end;
      if (StrP[1]='E')or(StrP[1]='e') then
      begin
        bEpson:=True;
        Delete(StrP,1,1); //���� � �������� ����� ������ E - �� Epson ��������
      end;

      if (StrP[1]='S')then Delete(StrP,1,1);

      while pos(' ',StrP)>0 do delete(StrP,pos(' ',StrP),1);

      DevPrint.DeviceName:=StrP;
      DevPrint.Open;

   //��� ����������� ��� ������� - � ������ ��������� ����� �� �����
      if bAxiohm then
      begin
        Buff[1]:=#$1B;
        Buff[2]:=#$74;
        Buff[3]:=#$07;
        DevPrint.WriteBuf(Buff,3);
      end;
    //��� ����������� ��� Epsona - � ������ ��������� ����� �� �����
      if bEpson then
      begin
        Buff[1]:=#$1B;
        Buff[2]:=#$74;
        Buff[3]:=#$11;
        DevPrint.WriteBuf(Buff,3);
      end;

    except
      if iLog=1 then prWriteLogPS('   ������ �������� - '+StrP);
    end;
  end;
  if pos('LPT',StrP)>0 then
  begin
    pName:=Copy(StrP,POS('LPT',StrP)+3,length(StrP)-POS('LPT',StrP)+2);
    if prOpenPrinter(pName) then bPrintOpen:=True else bPrintOpen:=False;
  end;
end;


function TdmC.TypeEdIzm(sArt:String):Integer;
begin
  Result:=0;
  quIM.Active:=False;
  quIM.ParamByName('SART').AsString:=sART;
  quIM.Active:=True;
  if quIM.RecordCount>0 then
  begin
    quIM.First;
    Result:=quIMCARD_TYPE.AsInteger;
  end;
  quIM.Active:=False;
end;

Procedure FormLog(NAMEOP,CONTENT:String);
begin
  with dmC do
  begin
    prFormLog.ParamByName('IDPERSONAL').AsInteger:=Person.Id;
    prFormLog.ParamByName('NAMEOP').AsString:=NAMEOP;
    prFormLog.ParamByName('CONTENT').AsString:=CONTENT;
    prFormLog.ExecProc;
  end;
end;


Procedure CalcDiscont(sArt:String;PosNum:Integer;Price:Currency;Quantity:Real;Summa:Currency;DiscProc:Real; Var DiscountProc:Real;Var DiscountSum:Real);
begin
  //������ ������ �� ��������� �������
  with dmC do
  begin
//(?SART, ?POSNUM, ?PRICE, ?QUANTITY, ?SUMMA, ?DISCPROC, ?CURTIME, ?CURDATE)

    DiscountProc:=0;
    DiscountSum:=0;

    prCalcDisc.ParamByName('SART').AsString:=sArt;
    prCalcDisc.ParamByName('POSNUM').AsInteger:=PosNum;
    prCalcDisc.ParamByName('PRICE').AsFloat:=Price;
    prCalcDisc.ParamByName('QUANTITY').AsFloat:=Quantity;
    prCalcDisc.ParamByName('SUMMA').AsFloat:=Summa;
    prCalcDisc.ParamByName('DISCPROC').AsFloat:=DiscProc;
    prCalcDisc.ParamByName('CURTIME').AsFloat:=Now - Trunc(Now); //�����
    prCalcDisc.ParamByName('CURDATE').AsDateTime:=Now;

    prCalcDisc.ExecProc;

    DiscountProc:=prCalcDisc.ParamByName('DISCOUNTPROC').AsFloat;
    DiscountSum:=rv(prCalcDisc.ParamByName('DISCOUNTSUM').AsFloat);

  end;
end;

Function FindDiscount(sBar:String):Boolean;
Var bPrefFind:Boolean;
    StrWk:String;
    iL:Integer;
    StrWk1:String;
begin
  result:=False;
  Check.Discount:='';
  Check.DiscProc:=0;
  Check.DiscName:='';

  if Length(sBar)>30 then sBar:=Copy(sBar,1,30);

  with dmC do
  begin    //������� ��� ������������ �� ��������� ����� �� ����� ����
    bPrefFind:=False;

    taDiscPre.Active:=False;
    taDiscPre.Active:=True;
    taDiscPre.First;
    while not taDiscPre.Eof do
    begin
      StrWk:=taDiscPreBARCODE.AsString;
      iL:=Length(StrWk);
      if Length(sBar)>=iL then
      begin
        StrWk1:=Copy(sBar,1,iL);
        if StrWk=StrWk1 then
        begin
          Result:=True;
          bPrefFind:=True;
          Check.Discount:=sBar;
          Check.DiscProc:=taDiscPrePERCENT.AsFloat;
          Check.DiscName:=taDiscPreNAME.AsString;
        end;
      end;
      taDiscPre.Next;
    end;
    taDiscPre.Active:=False;

    if not bPrefFind then
    begin //�� �������� �� ����� - ���� � �������� ����
      taDiscFind.Active:=False;
      taDiscFind.ParamByName('SBAR').AsString:=sBar;
      taDiscFind.Active:=True;
      if taDiscFind.RecordCount>0 then
      begin
        Result:=True;
        Check.Discount:=sBar;
        Check.DiscProc:=taDiscFindPERCENT.AsFloat;
        Check.DiscName:=taDiscFindNAME.AsString;
      end;
      taDiscFind.Active:=False;
    end;
  end;
end;

{Procedure CalcDiscont(Sifr,Id:Integer;Price,Quantity,Summa:Real;Discount:String;CurDateTime:TDateTime; Var DiscProc,DiscSum:Real);
begin
  DiscProc:=0;
  DiscSum:=0;

  if FindDiscount(Discount) then
  begin
    DiscProc:=Tab.DPercent;
    DiscSum:=Price*Quantity*Tab.DPercent/100;
  end;
end;
}





Function CheckOpen:Boolean;
Begin
  result:=False;
end;

Function FindPers(sP:String):Boolean;
begin
  result:=False;
  if Length(sP)>14 then sP:=Copy(sP,1,14);
  with dmC do
  begin
    taPersonal.Active:=true;
    if taPersonal.Locate('BARCODE',sP,[]) then
    begin
      if (taPersonalUVOLNEN.AsInteger=1) and (taPersonalMODUL3.AsInteger=0) then
      begin
        Person.Id:=taPersonalID.AsInteger;
        Person.Name:=taPersonalNAME.AsString;
        Result:=True;
      end;
    end;
    tapersonal.Active:=False;
  end;
end;


Function CanDo(Name_F:String):Boolean;
begin
  result:=True;
  with dmC do
  begin
    quCanDo.Active:=False;
    quCanDo.ParamByName('Person_id').Value:=Person.Id;
    quCanDo.ParamByName('Name_F').Value:=Name_F;
    quCanDo.Active:=True;
    if quCanDoprExec.AsBoolean=True then Result:=False;
  end;
end;

Function GetId(ta:String):Integer;
Var iType:Integer;
begin
  with dmC do
  begin
    iType:=0;
    if ta='Pe' then iType:=1; //��������
    if ta='Cl' then iType:=2; //�������������
    if ta='Trh' then iType:=3; //��������� ������
    if ta='Trs' then iType:=4; //������������ ������
    if ta='CurZ' then iType:=5; //������� Z �����
    if ta='SetZ' then iType:=6; //����� Z �����

    prGetId.ParamByName('ITYPE').AsInteger:=iType;
    prGetId.ExecProc;
    result:=prGetId.ParamByName('RESULT').Value;
  end;
end;


procedure TdmC.DataModuleCreate(Sender: TObject);
begin
{  with dmC do
  begin
    CasherDb.DBName:=DBName;
    try
      CasherDb.Open;

      taPersonal.Active:=True;
      taRClassif.Active:=True;
      quFuncList.Active:=True;
      taFuncList.Active:=True;

    except
      fmPerA.StatusBar1.Panels[0].Text:='���� �� ����������� - '+DBName;
    end;
  end;}
end;                                           

procedure TdmC.DataModuleDestroy(Sender: TObject);
begin
  taPersonal.Active:=False;
  taRClassif.Active:=False;
  quFuncList.Active:=False;
  taFuncList.Active:=False;

{  if trUpdate.Active then trUpdate.Commit;
  if trDel.Active then trDel.Commit;
  if trSelect.Active then trSelect.Commit;
}
  CasherDb.Close;
end;

procedure TdmC.quFuncListBeforePost(DataSet: TDataSet);
begin
  if taFuncList.Locate('Name',quFuncListNAME.AsString,[]) then
  begin
    taFuncList.Edit;
    taFuncListCOMMENT.AsString:=quFuncListCOMMENT.AsString;
    taFuncList.Post;
  end;
end;

end.
