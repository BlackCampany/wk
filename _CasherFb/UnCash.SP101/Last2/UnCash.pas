unit UnCash;

interface
uses
//  ������� ��� ��-101 ���

  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ComObj, ActiveX, OleServer, StdCtrls;

Function  InspectSt(Var sMess:String):Integer;
Function  GetSt(Var sMess:String):INteger;
Function  OpenZ:Boolean;  //��������
Function  GetSerial:Boolean;
Function  GetNums:Boolean;
Function  GetRes:Boolean;
Function  GetSums(Var rSum1,rSum2,rSum3:Real):Boolean;
function  GetRSumZ:Boolean;
function  GetRetSumZ:Boolean;

Function   CashOpen(sPort:PChar):Boolean;
Function   CashClose:Boolean;
Function   CashDate(var sDate:String):Boolean;
Function   GetX:Boolean;
Function   ZOpen:Boolean; //������ �����
Function   ZClose:Boolean;
Function   CheckStart:Boolean;
Function   CheckRetStart:Boolean;
Function   CheckAddPos(Var iSum:Integer):Boolean;
Function   CheckTotal(Var iTotal:Integer):Boolean;
Function   CheckDiscount(sDisc:String;rDiscount:Real;Var iItog:Integer):Boolean;
Function   CheckRasch(iPayType,iClient:Integer; ValName,Comment:String; Var iDopl:Integer):Boolean;
Function   CheckClose:Boolean;
Procedure  CheckCancel;
function  InCass(rSum:Real):Boolean;//����������
Function  GetCashReg(Var rSum:Real):Boolean;//���������� � �����

Function    CashDriver:Boolean;
Procedure   TestPosCh;

Function   OpenNFDoc:Boolean;
Function   CloseNFDoc:Boolean;
Function   PrintNFStr(StrPr:String):Boolean;
Function   SelectF(iNum:Integer):Boolean;
Function   CutDoc:Boolean;
Function   CutDoc1:Boolean;

procedure   WriteHistoryFR(Strwk_: string);
Function    TestStatus(StrOp:String; Var SMess:String):Boolean;

function   IsOLEObjectInstalled(Name: String): boolean;
function   More24H(Var iSt:INteger):Boolean;
Function   CheckOpenFis:Boolean;

Function   GetNumCh:Boolean;

Function   GetSumDisc(Var rSum1,rSum2,rSum3,rSum4:Real):Boolean;
Function   SetDP(Str1,Str2:String):Boolean;


Type
     TStatusFR = record
     St1:String[2];
     St2:String[4];
     St3:String[4];
     St4:String[10];
     end;

     TNums = record
     bOpen:Boolean;
     ZNum:Integer;
     SerNum:String[11];
     RegNum:string[10];
     CheckNum:string[4];
     iCheckNum:Integer;
     iRet:INteger;
     sRet:String;
     sDate:String;
     ZYet:Integer;
     sDateTimeBeg:String;
     end;

     TCurPos = record
     Articul:String;
     Bar:String;
     Name:String;
     EdIzm:String;
     TypeIzm:INteger;
     Quant:Real;
     Price:Real;
     DProc:Real;
     DSum:Real;
     Depart:SmallInt;
     FisPos:SmallInt;
     NumPos:INteger;
     end;

var
  CommandsArray : array [1..32000] of Char;
  HeapStatus    : THeapStatus;
  PressCount    : Byte;
  StatusFr      : TStatusFr;
  Nums          : TNums;
  CurPos        :TCurPos;
  SelPos        :TCurPos;
  sMessage      :String;
//  PosCh         : TPos;
  Drv: OleVariant;
  iRfr:SmallInt;
  rSumR,rSumD:Real;

Const iPassw:Integer = 30;

implementation

uses Un1, Dm, MainCasher;

function  GetRSumZ:Boolean;
var rSum:Array[1..16] of Currency;
    i:INteger;
begin
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then begin Result:=True; exit; end;
  for i:=1 to 16 do rSum[i]:=0;
  for i:=1 to 16 do rArrSum[i]:=0;

  try
    iRfr:=Drv.GetSumForAllPayments(rSum[1],rSum[2],rSum[3],rSum[4],rSum[5],rSum[6],rSum[7],rSum[8],rSum[9],rSum[10],rSum[11],rSum[12],rSum[13],rSum[14],rSum[15],rSum[16]);
    for i:=1 to 16 do rArrSum[i]:=rSum[i];
  except
  end;

  if iRfr=0 then Result:=True
  else Result:=False;
end;

function  GetRetSumZ:Boolean;
var rSum:Array[1..16] of Currency;
    i:INteger;
begin
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then begin Result:=True; exit; end;
  for i:=1 to 16 do rSum[i]:=0;
  for i:=1 to 16 do rArrSum[i]:=0;

  try
    iRfr:=Drv.GetRebookSumForAllPayments(rSum[1],rSum[2],rSum[3],rSum[4],rSum[5],rSum[6],rSum[7],rSum[8],rSum[9],rSum[10],rSum[11],rSum[12],rSum[13],rSum[14],rSum[15],rSum[16]);
    for i:=1 to 16 do rArrSum[i]:=rSum[i];
  except
  end;

  if iRfr=0 then Result:=True
  else Result:=False;
end;


Function SetDP(Str1,Str2:String):Boolean;
begin
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then begin Result:=True; exit; end;
  iRfr:=Drv.EEJShowClientDisplayText(Str1,Str2);
  if iRfr=0 then Result:=True
  else Result:=False;
end;


Function GetSumDisc(Var rSum1,rSum2,rSum3,rSum4:Real):Boolean;
Var cSum1,cSum2,cSum3,cSum4:Currency;
begin
  rSum1:=0; rSum2:=0; rSum3:=0; rSum4:=0;
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then begin inc(Nums.iCheckNum); Result:=True; exit; end;

  iRfr:=Drv.GetDiscountAndExtraSums(cSum1,cSum2,cSum3,cSum4);
  rSum1:=cSum1;
  rSum2:=cSum2;
  rSum3:=cSum3;
  rSum4:=cSum4;

  if iRfr=0 then Result:=True
  else Result:=False;
end;


Function GetNumCh:Boolean;
Var iNum:SmallINt;
begin
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then begin inc(Nums.iCheckNum); Result:=True; exit; end;
  iNum:=0;
  iRfr:=Drv.GetCurrentReceiptNum(iNum); //����� ���������� ����
  Nums.iCheckNum:=iNum;
  Nums.CheckNum:=IntToStr(Nums.iCheckNum);

  if iRfr=0 then Result:=True
  else Result:=False;
end;


Function GetCashReg(Var rSum:Real):Boolean;//���������� � �����
Var cSum:Currency;
begin
  rSum:=-1;
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then begin inc(Nums.iCheckNum); Result:=True; exit; end;

  iRfr:=Drv.GetCashInDrawer(cSum);
  rSum:=cSum;

  if iRfr=0 then Result:=True
  else Result:=False;
end;

Function InCass(rSum:Real):Boolean;
var cSum,cSum1:Currency;
begin
  Result:=True;
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then begin exit; end;

  cSum:=rv(abs(rSum));
  if rSum>0 then  //�������� �����
  begin
    if iRfr=0 then iRfr:=Drv.OpenReceipt(4, 1, Person.Name, Nums.iCheckNum);
    delay(100);
    if iRfr=0 then iRfr:=Drv.CashInOut('����� ',cSum,cSum1);
    delay(100);
    if iRfr=0 then iRfr:=Drv.CloseReceipt;
  end else //������
  begin
    if iRfr=0 then iRfr:=Drv.OpenReceipt(5, 1, Person.Name, Nums.iCheckNum);
    delay(100);
    if iRfr=0 then iRfr:=Drv.CashInOut('����� ',cSum,cSum1);
    delay(100);
    if iRfr=0 then iRfr:=Drv.CloseReceipt;
  end;

  if iRfr=0 then Result:=True
  else Result:=False;
end;



Function GetSums(Var rSum1,rSum2,rSum3:Real):Boolean;
begin
  result:=True;
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then begin Result:=True; exit; end;
  rSum1:=0;
  rSum2:=0;
  rSum3:=0;
end;


function More24H(Var iSt:INteger):Boolean;
begin
  result:=False;
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then begin Result:=False; exit; end;
//  Drv.GetECRStatus;
//  iSt:=Drv.ECRMode;
//  if Drv.ECRMode=3 then Result:=True;
end;

function IsOLEObjectInstalled(Name: String): boolean;
var
  ClassID: TCLSID;
  Rez : HRESULT;
begin

// ���� CLSID OLE-�������
  Rez := CLSIDFromProgID(PWideChar(WideString(Name)), ClassID);
  if Rez = S_OK then
 // ������ ������
    Result := true
  else
    Result := false;
end;


Function GetSt(Var sMess:String):Integer;
begin
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then begin Result:=0; exit; end;

  result:=0;
  sMess:='';
end;



Function TestStatus(StrOp:String; Var SMess:String):Boolean;
Var St1,St2,St3:ShortINt;
begin
  Result:=True;
  sMess:='';
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then exit;

  St1:=0; St2:=0; St3:=0;

  if iRfr=0 then iRfr:=Drv.GetStatus(St1,St2,St3);

  Drv.GetErrorMesageRU(iRfr,sMess);

  if (iRfr<>0)or(St1<>0)or((St2<>2)and(St2<>0))or(St3<>0) then
  begin
    prWriteLog('--'+StrOp+';'+its(iRfr)+';'+its(St1)+';'+its(St2)+';'+its(St3)+';'+sMess);
    Result:=False; //���� ���������� �.�. ����� ����������� ������� �������, �������� ������� ����
  end;
end;

procedure WriteHistoryFR(Strwk_: string);
var F: TextFile;
    Strwk1:String;
    FileN:String;
begin
  try
    if not DirectoryExists(CommonSet.PathHistory) then
    begin
//      ShowMessage('������: ����������� ����������� - "'+CommonSet.PathHistory+'"');
      exit;
    end;

    strwk1:='fr'+FormatDateTime('yyyy_mm',Date);
    Strwk1:=StrWk1+'.txt';
    Application.ProcessMessages;

    FileN:=CommonSet.PathHistory+strwk1;

    AssignFile(F, FileN);
    if Not FileExists(FileN) then Rewrite(F)
    else                           Append(F);
    Write(F, FormatDateTime('DD/MM/YYYY  HH:NN:SS ', Now));
    WriteLn(F, Strwk_);
    Flush(F);
  finally
    CloseFile(F);
  end;
end;


Function CutDoc:Boolean;
begin
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then
  begin

    dmC.prCutDoc(CommonSet.PortCash,0);

    Result:=True;
    exit;
  end;
  iRfr:=Drv.CutAndPrint;
  if iRfr=0 then Result:=True
  else Result:=False;
end;

Function CutDoc1:Boolean;
begin
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then begin Result:=True; exit; end;
  iRfr:=Drv.CutAndPrint;
  if iRfr=0 then Result:=True
  else Result:=False;
end;

Function SelectF(iNum:Integer):Boolean;
Var iFl,iFl1:Byte;
    Buff:Array[1..100] of Char;
    iTypeP:INteger;
    StrP:String;
begin
  Result:=True;
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then
  begin
    StrP:=Commonset.PortCash;
if pos('COM',StrP)>0 then
  begin
    iTypeP:=0;
    if (StrP[1]='S')or(StrP[1]='s') then iTypeP:=1;

    if iTypeP=0 then //������ �� ���������
    begin
      iFl:=$00;
      Case iNum of
      0: begin iFl:=$01 end;  //�������
      1: begin iFl:=$21 end;  //������� ������
      2: begin iFl:=$11 end;  //������� ������
      3: begin iFl:=$81 end;  //�������������
      4: begin iFl:=$31 end;  //������� ��� � ���

      5: begin iFl:=$09 end;  //�������
      6: begin iFl:=$29 end;  //������� ������
      7: begin iFl:=$19 end;  //������� ������
      8: begin iFl:=$89 end;  //�������������
      9: begin iFl:=$39 end;  //������� ��� � ���

      10: begin iFl:=$00 end; //�������
      11: begin iFl:=$20 end; //������� ������
      12: begin iFl:=$10 end; //������� ������
      13: begin iFl:=$80 end; //�������������
      14: begin iFl:=$30 end; //������� ��� � ���

      15: begin iFl:=$08 end; //�������
      16: begin iFl:=$28 end; //������� ������
      17: begin iFl:=$18 end; //������� ������
      18: begin iFl:=$88 end; //�������������
      19: begin iFl:=$38 end; //������� ��� � ���

      end;

      Buff[1]:=#$1B;
      Buff[2]:=#$21;
      Buff[3]:=chr(iFl);

      try
        dmC.DevPrint.WriteBuf(Buff,3);
      except
      end;
    end;

    if iTypeP=1 then //Star600
    begin
      iFl:=$30;  iFl1:=$30;
      Case iNum of
      10: begin iFl:=$30; iFl1:=$30; end; //�������
      13: begin iFl:=$30; iFl1:=$30; end; //�������������
      14: begin iFl:=$31; iFl1:=$31; end; //������� ��� � ���
      15: begin iFl:=$30; iFl1:=$30; end; //�������
      end;

      Buff[1]:=#$1B;
      Buff[2]:=#$69; // 30 30
      Buff[3]:=chr(iFl);
      Buff[4]:=chr(iFl1);

      try
        dmC.DevPrint.WriteBuf(Buff,4);
      except
      end;
    end;
  end;
    Result:=True;
    exit;
  end;
end;


Function PrintNFStr(StrPr:String):Boolean;
begin
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then
  begin
    PrintStr(StrPr);
    Result:=True;
    exit;
  end;
  if Length(StrPr)>40 then StrPr:=copy(StrPr,1,40);
  iRfr:=Drv.PrintString(StrPr);
  if iRfr=0 then Result:=True
  else Result:=False;
  delay(30);
end;


Function OpenNFDoc:Boolean;
begin
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then
  begin
    dmC.prDevOpen(CommonSet.PortCash,0);

    Result:=True;
    exit;
  end;
  iRfr:=Drv.OpenReceipt(1, 1, Person.Name, Nums.iCheckNum);  //1 - ������������ ��������
  if iRfr=0 then Result:=True
  else Result:=False;
  delay(30);
end;

Function CloseNFDoc:Boolean;
begin
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then
  begin
    dmC.prCutDoc(CommonSet.PortCash,0);
    dmC.prDevClose(CommonSet.PortCash,0);
    Result:=True;
    exit;
  end;
  iRfr:=Drv.CloseReceiptEx(True); //c �������
  if iRfr=0 then Result:=True
  else Result:=False;
  delay(30);
end;


Function CashDriver:Boolean;
begin
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then
  begin
    OpenMoneyBox;
    Result:=True;
    exit;
  end;
  iRfr:=Drv.OpenCashDrawer;
  if iRfr=0 then Result:=True else Result:=False;
end;


Function CheckClose:Boolean;
begin
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then
  begin
    PrintStr(CommonSet.LastStr1);
    PrintStr(CommonSet.LastStr2);
    dmC.prCutDoc(CommonSet.PortCash,0);
    dmC.prDevClose(CommonSet.PortCash,0);
    Result:=True;
    exit;
  end;
  iRfr:=Drv.CloseReceipt;
  if iRfr=0 then Result:=True
  else Result:=False;
end;

Function CheckRasch(iPayType,iClient:Integer; ValName,Comment:String; Var iDopl:Integer):Boolean;
Var rSumSd:Currency;
    rCli:Real;
    Str1,Str2:String;
begin
  iDopl:=0;
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then
  begin
    rCli:=iClient/100;
    Str(rCli:8:2,Str2);
    if iPayType=0 then
    begin
      Str1:=SetL('�������� ��������� ',32)+Str2;
      PrintStr(Str1);
    end;
    if iPayType=2 then
    begin
      Str1:=SetL('�������� ����.������ ',32)+Str2;
      PrintStr(Str1);
    end;
    if rCli>(rSumR-rSumD) then
    begin
      Str(rCli-(rSumR-rSumD):8:2,Str2);
      Str1:=SetL('����� ',32)+Str2;
      PrintStr(Str1);
    end;

    Result:=True;
    exit;
  end;
  if iPayType=0 then //���
  begin
    iRfr:=Drv.Payment(0,iClient/100,rSumSd);
    iDopl:=RoundEx(rSumSd*100);
  end else
  begin
    iRfr:=Drv.Payment(1,iClient/100,rSumSd);
    iDopl:=RoundEx(rSumSD*100);
  end;
  if iRfr=0 then Result:=True
  else Result:=False;
  delay(100);

end;

Function CheckDiscount(sDisc:String;rDiscount:Real;Var iItog:Integer):Boolean;
Var iDiscount:Integer;
    rDiscCurr,rItog:Currency;
begin
  iDiscount:=RoundEx(rDiscount*100);
  iItog:=0;
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then begin Result:=True; exit; end;
  if iDiscount>=0 then
  begin //������
    rDiscCurr:=iDiscount/100;
    iRfr:=Drv.DiscountTotal(sDisc,rDiscCurr,rItog);
  end else
  begin
  end;
  if iRfr=0 then
  begin
    iItog:=RoundEx(rItog*100);
    Result:=True;
  end
  else Result:=False;
  delay(100);
end;

Function CheckTotal(Var iTotal:Integer):Boolean;
Var rSum:Currency;
    Str1,Str2:String;
    rDiscDop:Real;
begin
  iTotal:=0;
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then
  begin
    PrintStr('----------------------------------------');
    rSumR:=0;
    rSumD:=0;
    with dmC do
    begin
      fmMainCasher.ViewCh.BeginUpdate;
      dmC.taCheck.First;
      while not dmC.taCheck.Eof do
      begin
        rSumR:=rSumR+dmC.taCheckRSUM.AsFloat;
        rSumD:=rSumD+dmC.taCheckDSUM.AsFloat;
        dmC.taCheck.Next;
      end;
      fmMainCasher.ViewCh.EndUpdate;
    end;
    Str(rSumR:8:2,Str2);
    Str1:=SetL('����� ',32)+Str2;
    PrintStr(Str1);

    if ((rSumR-rSumD)>0.1) then
    begin
//        rDiscDop:=rv(rSumCh-(Trunc(rv(rSumCh)*10)/10));
      rDiscDop:=rv((rSumR-rSumD)*CommonSet.RoundSum);
      rDiscDop:=Trunc(rDiscDop);
      rDiscDop:=rDiscDop/CommonSet.RoundSum;
      rDiscDop:=(rSumR-rSumD)-rDiscDop;

      rSumD:=rSumD+rDiscDop;
    end;


    if rSumD<>0 then
    begin
      Str(rSumD:7:2,Str2);
      Str1:=SetL('������ ',33)+Str2;
      PrintStr(Str1);

      Str((rSumR-rSumD):8:2,Str2);
      Str1:=SetL('����� ',32)+Str2;
      PrintStr(Str1);
    end;

    Result:=True;
    exit;
  end;
  iRfr:=Drv.SubTotal(rSum);

  if iRfr=0 then
  begin
    iTotal:=RoundEx(rSum*100);
    Result:=True;
  end
  else Result:=False;

  delay(100);

end;


Procedure CheckCancel;
begin
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then
  begin
    PrintStr('��� �����������');
    dmC.prCutDoc(CommonSet.PortCash,0);
    dmC.prDevClose(CommonSet.PortCash,0);
    exit;
  end;
  iRfr:=Drv.BreakReceipt;
end;

Procedure TestPosCh;
begin
//  if Length(PosCh.Name)>36 then PosCh.Name:=Copy(PosCh.Name,1,36);
//  if Length(PosCh.Code)>19 then PosCh.Code:=Copy(PosCh.Code,1,19);
//  if Length(PosCh.AddName)>250 then PosCh.AddName:=Copy(PosCh.AddName,1,250);

  if Length(CurPos.Name)>32 then CurPos.Name:=Copy(CurPos.Name,1,32)+' ';
  if Length(CurPos.Articul)>5 then CurPos.Articul:=Copy(CurPos.Articul,1,5);
end;


Function CheckAddPos(Var iSum:Integer):Boolean;
Var rSum,rPr:Currency;
    rQ:Real;
    Str1,Str2:String;
begin
  delay(10);
  iSum:=RoundEx(rv(CurPos.Price)*RoundEx(CurPos.Quant*1000)/1000 *100);
  TestPosCh;
  rPr:=rv(CurPos.Price);

  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then
  begin
    Str1:=SetL(CurPos.Name,17)+' '+Copy(CurPos.EdIzm,1,2);
    Str(RoundEx(CurPos.Quant*1000)/1000:6:3,Str2);
    if Str2[6]='0' then delete(Str2,6,1) else delete(Str2,1,1);
    Str1:=Str1+Str2;
    Str(rPr:6:2,Str2);
    Str1:=Str1+'*'+Str2;
    Str(iSum/100:7:2,Str2);
    Str1:=Str1+':'+Str2;
    PrintStr(Str1);

    prWriteLog('~~NoFisPos;'+its(Nums.iCheckNum)+';'+CurPos.Articul+';'+CurPos.Bar+';'+fs(CurPos.Quant)+';'+FloatToStr(rPr)+';');

    Result:=True;
    delay(100);
    exit;
  end;

  CurPos.FisPos:=1; //����� �������� ���������� �������� � ������� ��������
  prWriteLog('~~FisPos;'+its(Nums.iCheckNum)+';'+CurPos.Articul+';'+CurPos.Bar+';'+fs(CurPos.Quant)+';'+FloatToStr(rPr)+';');

  if CurPos.Quant>=0 then
  begin
    iRfr:=Drv.AddArticle(CurPos.Name,CurPos.Articul, RoundEx(CurPos.Quant*1000)/1000, rPr, 1, rSum);

    if iRfr=0 then
    begin
      iSum:=RoundEx(rSum*100);
      Result:=True;
    end
    else Result:=False;

  end else
  begin
    rQ:=RoundEx(CurPos.Quant*1000)/1000*(-1);
    iRfr:=Drv.StornoArticle(CurPos.Name,CurPos.Articul, rQ, rPr, 1, rSum);

    if iRfr=0 then
    begin
      iSum:=RoundEx(rSum*100)*(-1);
      Result:=True;
    end
    else Result:=False;
  end;

  delay(100);
end;


Function CheckStart:Boolean;
Var StrWk:String;
begin
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then
  begin
    dmC.prDevOpen(CommonSet.PortCash,0);
    PrintStr(CommonSet.DepartName);
    PrintStr(CommonSet.PreStr1);
    PrintStr(CommonSet.PreStr2);
    PrintStr('-----------------���������--------------');
    PrintStr('����� '+its(CommonSet.CashNum)+'   ��� �'+its(Nums.ZNum)+'.'+its(Nums.iCheckNum));
    PrintStr('����, ����� '+FormatDateTime('dd.mm.yyyy hh:nn',now));

    StrWk:=Copy('������ '+Person.Name,1,32);
    while length(StrWk)<32 do StrWk:=StrWk+' ';
    Case Check.Operation of
    0: StrWk:=StrWk+' �������';
    1: StrWk:=StrWk+' �������';
    end;
    PrintStr(StrWk);

    PrintStr('----------------------------------------');

    Result:=True;
    exit;
  end;
  iRfr:=Drv.OpenReceipt(2, 1, Person.Name, Nums.iCheckNum);  //��� �� ������� - 2, ������ -1
  if iRfr=0 then Result:=True
  else Result:=False;
  delay(100);
end;

Function CheckRetStart:Boolean;
begin
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then
  begin
    dmC.prDevOpen(CommonSet.PortCash,0);
    PrintStr(CommonSet.DepartName);
    PrintStr(CommonSet.PreStr1);
    PrintStr(CommonSet.PreStr2);
    PrintStr('-----------------���������--------------');
    PrintStr('����� '+its(CommonSet.CashNum)+'   ��� �'+its(Nums.ZNum)+'.'+its(Nums.iCheckNum));
    PrintStr('����, ����� '+FormatDateTime('dd.mm.yyyy hh:nn',now));

    StrWk:=Copy('������ '+Person.Name,1,32);
    while length(StrWk)<32 do StrWk:=StrWk+' ';
    Case Check.Operation of
    0: StrWk:=' �������';
    1: StrWk:=' �������';
    end;
    PrintStr(StrWk);

    PrintStr('----------------------------------------');

    Result:=True;
    exit;
  end;
  iRfr:=Drv.OpenReceipt(3, 1, Person.Name, Nums.iCheckNum);  //��� �� ������� - 3, ������-1
  if iRfr=0 then Result:=True
  else Result:=False;
  delay(100);
end;


Function ZClose:Boolean;
begin
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then begin Result:=True; exit; end;
  Result:=False;
  iRfr:=Drv.ZReport('');
  if iRfr=0 then
  begin
    Result:=True;
  end;
end;


Function ZOpen:Boolean;
begin
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then begin Result:=True; exit; end;
  Result:=True;
end;

Function GetX:Boolean;
begin
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then begin Result:=True; exit; end;
  Result:=False;
  iRfr:=Drv.XReport('');
  if iRfr=0 then
  begin
    Result:=True;
  end;
end;

Function CashDate(var sDate:String):Boolean;
Var iRet:Integer;
    iDD,iMM,iYY,iSS,iHH,iMI:Smallint;
begin
//
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then begin Result:=True; exit; end;
  Result:=False;
  sDate:='';
  iRet:=Drv.GetCurrentDateTime(iDD,iMM,iYY,iHH,iMI,iSS);
  if iRet<>0 then
  begin
    Exit;
  end
  else
  begin
    sDate:=its(iDD)+'.'+its(iMM)+'.'+its(iYY)+':'+its(iHH)+':'+its(iMI)+':'+its(iSS);
    Result:=true;
  end;
end;


Function CashClose:Boolean;
Var iRet:Integer;
begin
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then begin Result:=True; exit; end;
  Result:=False;
  iRet:=Drv.Disconnect;
  if iRet=0 then result:=True;
end;

Function CashOpen(sPort:PChar):Boolean;
Var StrWk:String;
    iP:INteger;
    sMess:String;
begin
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then begin Result:=True; exit; end;

  CommonSet.TypeFis:='sp101';

  if not IsOLEObjectInstalled('SP101FRKLib.SP101FRObject') then
  begin
    showmessage('������� ����� �� ����������. !!!');
    Result:=False;
    exit;
  end;
  try
   Drv := CreateOleObject('SP101FRKLib.SP101FRObject');
  except
    showmessage('���������� ������� ������ "SP101FRKLib.SP101FRObject". !!!');
    Result:=False;
    exit;
  end;

  StrWk:=String(sPort);
  if pos('COM',StrWk)>0 then delete(StrWk,pos('COM',StrWk),3);
  iP:=StrToINtDef(StrWk,2);
  iRfr:=Drv.Connect(iP);
  if iRfr=0 then
  begin
    iRfr:=Drv.Init(Now); //������������� ���������� �������
    if iRfr=0 then result:=True else
    begin
      Drv.GetErrorMesageRU(iRfr,sMess);
      showmessage('������ ����� - '+sMess+' ���������� ������ ����������. ��c�� ���������� ������������� - ������������� �����.');
      result:=False;
    end;
  end else result:=False;
end;

Function GetNums:Boolean;
Var iNum,iZ,iKL:SmallINt;
begin
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then
  begin
    Nums.iCheckNum:=prGetNFCheckNum;
    Result:=True;
    exit;
  end;

  Result:=True;

  iRfr:=Drv.GetCurrentReceiptNum(iNum); //����� ���������� ����
  if iRfr=0 then
  begin
    iRfr:=Drv.GetCurrentShift(iZ); //����� ������� �����
    if iRfr=0 then
    begin
      iRfr:=Drv.GetEEJStatus(iKl);
      if iRfr=0 then Result:=True else Result:=False;

      Nums.iCheckNum:=iNum;
      Nums.CheckNum:=IntToStr(Nums.iCheckNum);
      Nums.ZNum:=iZ;
      Nums.ZYet:=iKl;

      CommonSet.ZNum:=Nums.ZNum;
    end;
  end;
end;

Function GetRes:Boolean;
begin
  Result:=True;
end;

Function GetSerial:Boolean;
Var sNum:String;
begin
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then begin Result:=True; exit; end;

  iRfr:=Drv.GetFiscNumber(sNum);
  if iRfr=0 then Nums.SerNum:=sNum
  else Nums.SerNum:='';

  result:=True;
end;


Function OpenZ:Boolean;
begin
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then begin Result:=True; exit; end;
  result:=True;
end;



Function InspectSt(Var sMess:String):Integer;
Var  St1,St2,St3:ShortINt;
begin
  sMess:='��� ��.';
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then begin Result:=0; exit; end;
  St1:=0; St2:=2; St3:=0;

  iRfr:=Drv.GetStatus(St1,St2,St3);
//���� ��� ������� ��� ��������
//  Drv.GetErrorMessageRU(iRfr,sMess);
  if (iRfr<>0)or(St1<>0)or((St2<>2)and(St2<>0))or(St3<>0) then
    prWriteLog('--GetStatus;'+Its(iRfr)+':'+its(St1)+';'+its(St2)+';'+its(St3)+';');

  result:=iRfr;
end;

Function CheckOpenFis:Boolean;
//Var iSt:INteger;
begin
  if (Commonset.CashNum=0) or (CommonSet.NoFis=1) then begin Result:=True; exit; end;
  result:=True;
end;



end.
