/* this ALWAYS GENERATED file contains the definitions for the interfaces */


/* File created by MIDL compiler version 5.01.0164 */
/* at Wed May 13 16:39:54 2009
 */
/* Compiler settings for D:\work\ActiveX\Arcom_inp_2ActX\ArcCom.idl:
    Oicf (OptLev=i2), W1, Zp8, env=Win32, ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
*/
//@@MIDL_FILE_HEADING(  )


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 440
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __ArcCom_h__
#define __ArcCom_h__

#ifdef __cplusplus
extern "C"{
#endif 

/* Forward Declarations */ 

#ifndef __ISAPacketObj_FWD_DEFINED__
#define __ISAPacketObj_FWD_DEFINED__
typedef interface ISAPacketObj ISAPacketObj;
#endif 	/* __ISAPacketObj_FWD_DEFINED__ */


#ifndef __IPCPOSTConnectorObj_FWD_DEFINED__
#define __IPCPOSTConnectorObj_FWD_DEFINED__
typedef interface IPCPOSTConnectorObj IPCPOSTConnectorObj;
#endif 	/* __IPCPOSTConnectorObj_FWD_DEFINED__ */


#ifndef __SAPacketObj_FWD_DEFINED__
#define __SAPacketObj_FWD_DEFINED__

#ifdef __cplusplus
typedef class SAPacketObj SAPacketObj;
#else
typedef struct SAPacketObj SAPacketObj;
#endif /* __cplusplus */

#endif 	/* __SAPacketObj_FWD_DEFINED__ */


#ifndef __PCPOSTConnectorObj_FWD_DEFINED__
#define __PCPOSTConnectorObj_FWD_DEFINED__

#ifdef __cplusplus
typedef class PCPOSTConnectorObj PCPOSTConnectorObj;
#else
typedef struct PCPOSTConnectorObj PCPOSTConnectorObj;
#endif /* __cplusplus */

#endif 	/* __PCPOSTConnectorObj_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

void __RPC_FAR * __RPC_USER MIDL_user_allocate(size_t);
void __RPC_USER MIDL_user_free( void __RPC_FAR * ); 

#ifndef __ISAPacketObj_INTERFACE_DEFINED__
#define __ISAPacketObj_INTERFACE_DEFINED__

/* interface ISAPacketObj */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_ISAPacketObj;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("DA5F8E20-E4D5-4043-A0EB-5578230851EC")
    ISAPacketObj : public IDispatch
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Amount( 
            /* [retval][out] */ BSTR __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Amount( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_CurrencyCode( 
            /* [retval][out] */ BSTR __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_CurrencyCode( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_DateTimeHost( 
            /* [retval][out] */ BSTR __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_DateTimeHost( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_CardEntryMode( 
            /* [retval][out] */ long __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_CardEntryMode( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_PAN( 
            /* [retval][out] */ BSTR __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_PAN( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_CardExpiryDate( 
            /* [retval][out] */ BSTR __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_CardExpiryDate( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_TRACK2( 
            /* [retval][out] */ BSTR __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_TRACK2( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_AuthorizationCode( 
            /* [retval][out] */ BSTR __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_AuthorizationCode( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ReferenceNumber( 
            /* [retval][out] */ BSTR __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ReferenceNumber( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ResponseCodeHost( 
            /* [retval][out] */ BSTR __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ResponseCodeHost( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_TextResponse( 
            /* [retval][out] */ BSTR __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_TextResponse( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_DateTimeCRM( 
            /* [retval][out] */ BSTR __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_DateTimeCRM( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_TrxID( 
            /* [retval][out] */ long __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_TrxID( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_OrigOperation( 
            /* [retval][out] */ long __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_OrigOperation( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_OperationCode( 
            /* [retval][out] */ long __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_OperationCode( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_TrxIDCRM( 
            /* [retval][out] */ long __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_TrxIDCRM( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_CRMID( 
            /* [retval][out] */ BSTR __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_CRMID( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Status( 
            /* [retval][out] */ long __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Status( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_TrxIDHost( 
            /* [retval][out] */ long __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_TrxIDHost( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_CommodityCode( 
            /* [retval][out] */ BSTR __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_CommodityCode( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_PaymentDetails( 
            /* [retval][out] */ BSTR __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_PaymentDetails( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ProviderCode( 
            /* [retval][out] */ BSTR __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ProviderCode( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ProcessingFlag( 
            /* [retval][out] */ long __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ProcessingFlag( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_FileNameResult( 
            /* [retval][out] */ BSTR __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_FileNameResult( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_FileNameReport( 
            /* [retval][out] */ BSTR __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_FileNameReport( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_FileName( 
            /* [retval][out] */ BSTR __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_FileName( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_CommandResult( 
            /* [retval][out] */ long __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_CommandResult( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_PathParameters( 
            /* [retval][out] */ BSTR __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_PathParameters( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_CVV2( 
            /* [retval][out] */ BSTR __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_CVV2( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_AmountFee( 
            /* [retval][out] */ BSTR __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_AmountFee( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_TerminalOutID( 
            /* [retval][out] */ BSTR __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_TerminalOutID( 
            /* [in] */ BSTR newVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ISAPacketObjVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            ISAPacketObj __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            ISAPacketObj __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            ISAPacketObj __RPC_FAR * This);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfoCount )( 
            ISAPacketObj __RPC_FAR * This,
            /* [out] */ UINT __RPC_FAR *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfo )( 
            ISAPacketObj __RPC_FAR * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo __RPC_FAR *__RPC_FAR *ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetIDsOfNames )( 
            ISAPacketObj __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR __RPC_FAR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID __RPC_FAR *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Invoke )( 
            ISAPacketObj __RPC_FAR * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS __RPC_FAR *pDispParams,
            /* [out] */ VARIANT __RPC_FAR *pVarResult,
            /* [out] */ EXCEPINFO __RPC_FAR *pExcepInfo,
            /* [out] */ UINT __RPC_FAR *puArgErr);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Amount )( 
            ISAPacketObj __RPC_FAR * This,
            /* [retval][out] */ BSTR __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_Amount )( 
            ISAPacketObj __RPC_FAR * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_CurrencyCode )( 
            ISAPacketObj __RPC_FAR * This,
            /* [retval][out] */ BSTR __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_CurrencyCode )( 
            ISAPacketObj __RPC_FAR * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_DateTimeHost )( 
            ISAPacketObj __RPC_FAR * This,
            /* [retval][out] */ BSTR __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_DateTimeHost )( 
            ISAPacketObj __RPC_FAR * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_CardEntryMode )( 
            ISAPacketObj __RPC_FAR * This,
            /* [retval][out] */ long __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_CardEntryMode )( 
            ISAPacketObj __RPC_FAR * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_PAN )( 
            ISAPacketObj __RPC_FAR * This,
            /* [retval][out] */ BSTR __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_PAN )( 
            ISAPacketObj __RPC_FAR * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_CardExpiryDate )( 
            ISAPacketObj __RPC_FAR * This,
            /* [retval][out] */ BSTR __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_CardExpiryDate )( 
            ISAPacketObj __RPC_FAR * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_TRACK2 )( 
            ISAPacketObj __RPC_FAR * This,
            /* [retval][out] */ BSTR __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_TRACK2 )( 
            ISAPacketObj __RPC_FAR * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_AuthorizationCode )( 
            ISAPacketObj __RPC_FAR * This,
            /* [retval][out] */ BSTR __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_AuthorizationCode )( 
            ISAPacketObj __RPC_FAR * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_ReferenceNumber )( 
            ISAPacketObj __RPC_FAR * This,
            /* [retval][out] */ BSTR __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_ReferenceNumber )( 
            ISAPacketObj __RPC_FAR * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_ResponseCodeHost )( 
            ISAPacketObj __RPC_FAR * This,
            /* [retval][out] */ BSTR __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_ResponseCodeHost )( 
            ISAPacketObj __RPC_FAR * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_TextResponse )( 
            ISAPacketObj __RPC_FAR * This,
            /* [retval][out] */ BSTR __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_TextResponse )( 
            ISAPacketObj __RPC_FAR * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_DateTimeCRM )( 
            ISAPacketObj __RPC_FAR * This,
            /* [retval][out] */ BSTR __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_DateTimeCRM )( 
            ISAPacketObj __RPC_FAR * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_TrxID )( 
            ISAPacketObj __RPC_FAR * This,
            /* [retval][out] */ long __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_TrxID )( 
            ISAPacketObj __RPC_FAR * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_OrigOperation )( 
            ISAPacketObj __RPC_FAR * This,
            /* [retval][out] */ long __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_OrigOperation )( 
            ISAPacketObj __RPC_FAR * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_OperationCode )( 
            ISAPacketObj __RPC_FAR * This,
            /* [retval][out] */ long __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_OperationCode )( 
            ISAPacketObj __RPC_FAR * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_TrxIDCRM )( 
            ISAPacketObj __RPC_FAR * This,
            /* [retval][out] */ long __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_TrxIDCRM )( 
            ISAPacketObj __RPC_FAR * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_CRMID )( 
            ISAPacketObj __RPC_FAR * This,
            /* [retval][out] */ BSTR __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_CRMID )( 
            ISAPacketObj __RPC_FAR * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Status )( 
            ISAPacketObj __RPC_FAR * This,
            /* [retval][out] */ long __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_Status )( 
            ISAPacketObj __RPC_FAR * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_TrxIDHost )( 
            ISAPacketObj __RPC_FAR * This,
            /* [retval][out] */ long __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_TrxIDHost )( 
            ISAPacketObj __RPC_FAR * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_CommodityCode )( 
            ISAPacketObj __RPC_FAR * This,
            /* [retval][out] */ BSTR __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_CommodityCode )( 
            ISAPacketObj __RPC_FAR * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_PaymentDetails )( 
            ISAPacketObj __RPC_FAR * This,
            /* [retval][out] */ BSTR __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_PaymentDetails )( 
            ISAPacketObj __RPC_FAR * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_ProviderCode )( 
            ISAPacketObj __RPC_FAR * This,
            /* [retval][out] */ BSTR __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_ProviderCode )( 
            ISAPacketObj __RPC_FAR * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_ProcessingFlag )( 
            ISAPacketObj __RPC_FAR * This,
            /* [retval][out] */ long __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_ProcessingFlag )( 
            ISAPacketObj __RPC_FAR * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_FileNameResult )( 
            ISAPacketObj __RPC_FAR * This,
            /* [retval][out] */ BSTR __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_FileNameResult )( 
            ISAPacketObj __RPC_FAR * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_FileNameReport )( 
            ISAPacketObj __RPC_FAR * This,
            /* [retval][out] */ BSTR __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_FileNameReport )( 
            ISAPacketObj __RPC_FAR * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_FileName )( 
            ISAPacketObj __RPC_FAR * This,
            /* [retval][out] */ BSTR __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_FileName )( 
            ISAPacketObj __RPC_FAR * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_CommandResult )( 
            ISAPacketObj __RPC_FAR * This,
            /* [retval][out] */ long __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_CommandResult )( 
            ISAPacketObj __RPC_FAR * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_PathParameters )( 
            ISAPacketObj __RPC_FAR * This,
            /* [retval][out] */ BSTR __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_PathParameters )( 
            ISAPacketObj __RPC_FAR * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_CVV2 )( 
            ISAPacketObj __RPC_FAR * This,
            /* [retval][out] */ BSTR __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_CVV2 )( 
            ISAPacketObj __RPC_FAR * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_AmountFee )( 
            ISAPacketObj __RPC_FAR * This,
            /* [retval][out] */ BSTR __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_AmountFee )( 
            ISAPacketObj __RPC_FAR * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_TerminalOutID )( 
            ISAPacketObj __RPC_FAR * This,
            /* [retval][out] */ BSTR __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_TerminalOutID )( 
            ISAPacketObj __RPC_FAR * This,
            /* [in] */ BSTR newVal);
        
        END_INTERFACE
    } ISAPacketObjVtbl;

    interface ISAPacketObj
    {
        CONST_VTBL struct ISAPacketObjVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ISAPacketObj_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define ISAPacketObj_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define ISAPacketObj_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define ISAPacketObj_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define ISAPacketObj_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define ISAPacketObj_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define ISAPacketObj_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define ISAPacketObj_get_Amount(This,pVal)	\
    (This)->lpVtbl -> get_Amount(This,pVal)

#define ISAPacketObj_put_Amount(This,newVal)	\
    (This)->lpVtbl -> put_Amount(This,newVal)

#define ISAPacketObj_get_CurrencyCode(This,pVal)	\
    (This)->lpVtbl -> get_CurrencyCode(This,pVal)

#define ISAPacketObj_put_CurrencyCode(This,newVal)	\
    (This)->lpVtbl -> put_CurrencyCode(This,newVal)

#define ISAPacketObj_get_DateTimeHost(This,pVal)	\
    (This)->lpVtbl -> get_DateTimeHost(This,pVal)

#define ISAPacketObj_put_DateTimeHost(This,newVal)	\
    (This)->lpVtbl -> put_DateTimeHost(This,newVal)

#define ISAPacketObj_get_CardEntryMode(This,pVal)	\
    (This)->lpVtbl -> get_CardEntryMode(This,pVal)

#define ISAPacketObj_put_CardEntryMode(This,newVal)	\
    (This)->lpVtbl -> put_CardEntryMode(This,newVal)

#define ISAPacketObj_get_PAN(This,pVal)	\
    (This)->lpVtbl -> get_PAN(This,pVal)

#define ISAPacketObj_put_PAN(This,newVal)	\
    (This)->lpVtbl -> put_PAN(This,newVal)

#define ISAPacketObj_get_CardExpiryDate(This,pVal)	\
    (This)->lpVtbl -> get_CardExpiryDate(This,pVal)

#define ISAPacketObj_put_CardExpiryDate(This,newVal)	\
    (This)->lpVtbl -> put_CardExpiryDate(This,newVal)

#define ISAPacketObj_get_TRACK2(This,pVal)	\
    (This)->lpVtbl -> get_TRACK2(This,pVal)

#define ISAPacketObj_put_TRACK2(This,newVal)	\
    (This)->lpVtbl -> put_TRACK2(This,newVal)

#define ISAPacketObj_get_AuthorizationCode(This,pVal)	\
    (This)->lpVtbl -> get_AuthorizationCode(This,pVal)

#define ISAPacketObj_put_AuthorizationCode(This,newVal)	\
    (This)->lpVtbl -> put_AuthorizationCode(This,newVal)

#define ISAPacketObj_get_ReferenceNumber(This,pVal)	\
    (This)->lpVtbl -> get_ReferenceNumber(This,pVal)

#define ISAPacketObj_put_ReferenceNumber(This,newVal)	\
    (This)->lpVtbl -> put_ReferenceNumber(This,newVal)

#define ISAPacketObj_get_ResponseCodeHost(This,pVal)	\
    (This)->lpVtbl -> get_ResponseCodeHost(This,pVal)

#define ISAPacketObj_put_ResponseCodeHost(This,newVal)	\
    (This)->lpVtbl -> put_ResponseCodeHost(This,newVal)

#define ISAPacketObj_get_TextResponse(This,pVal)	\
    (This)->lpVtbl -> get_TextResponse(This,pVal)

#define ISAPacketObj_put_TextResponse(This,newVal)	\
    (This)->lpVtbl -> put_TextResponse(This,newVal)

#define ISAPacketObj_get_DateTimeCRM(This,pVal)	\
    (This)->lpVtbl -> get_DateTimeCRM(This,pVal)

#define ISAPacketObj_put_DateTimeCRM(This,newVal)	\
    (This)->lpVtbl -> put_DateTimeCRM(This,newVal)

#define ISAPacketObj_get_TrxID(This,pVal)	\
    (This)->lpVtbl -> get_TrxID(This,pVal)

#define ISAPacketObj_put_TrxID(This,newVal)	\
    (This)->lpVtbl -> put_TrxID(This,newVal)

#define ISAPacketObj_get_OrigOperation(This,pVal)	\
    (This)->lpVtbl -> get_OrigOperation(This,pVal)

#define ISAPacketObj_put_OrigOperation(This,newVal)	\
    (This)->lpVtbl -> put_OrigOperation(This,newVal)

#define ISAPacketObj_get_OperationCode(This,pVal)	\
    (This)->lpVtbl -> get_OperationCode(This,pVal)

#define ISAPacketObj_put_OperationCode(This,newVal)	\
    (This)->lpVtbl -> put_OperationCode(This,newVal)

#define ISAPacketObj_get_TrxIDCRM(This,pVal)	\
    (This)->lpVtbl -> get_TrxIDCRM(This,pVal)

#define ISAPacketObj_put_TrxIDCRM(This,newVal)	\
    (This)->lpVtbl -> put_TrxIDCRM(This,newVal)

#define ISAPacketObj_get_CRMID(This,pVal)	\
    (This)->lpVtbl -> get_CRMID(This,pVal)

#define ISAPacketObj_put_CRMID(This,newVal)	\
    (This)->lpVtbl -> put_CRMID(This,newVal)

#define ISAPacketObj_get_Status(This,pVal)	\
    (This)->lpVtbl -> get_Status(This,pVal)

#define ISAPacketObj_put_Status(This,newVal)	\
    (This)->lpVtbl -> put_Status(This,newVal)

#define ISAPacketObj_get_TrxIDHost(This,pVal)	\
    (This)->lpVtbl -> get_TrxIDHost(This,pVal)

#define ISAPacketObj_put_TrxIDHost(This,newVal)	\
    (This)->lpVtbl -> put_TrxIDHost(This,newVal)

#define ISAPacketObj_get_CommodityCode(This,pVal)	\
    (This)->lpVtbl -> get_CommodityCode(This,pVal)

#define ISAPacketObj_put_CommodityCode(This,newVal)	\
    (This)->lpVtbl -> put_CommodityCode(This,newVal)

#define ISAPacketObj_get_PaymentDetails(This,pVal)	\
    (This)->lpVtbl -> get_PaymentDetails(This,pVal)

#define ISAPacketObj_put_PaymentDetails(This,newVal)	\
    (This)->lpVtbl -> put_PaymentDetails(This,newVal)

#define ISAPacketObj_get_ProviderCode(This,pVal)	\
    (This)->lpVtbl -> get_ProviderCode(This,pVal)

#define ISAPacketObj_put_ProviderCode(This,newVal)	\
    (This)->lpVtbl -> put_ProviderCode(This,newVal)

#define ISAPacketObj_get_ProcessingFlag(This,pVal)	\
    (This)->lpVtbl -> get_ProcessingFlag(This,pVal)

#define ISAPacketObj_put_ProcessingFlag(This,newVal)	\
    (This)->lpVtbl -> put_ProcessingFlag(This,newVal)

#define ISAPacketObj_get_FileNameResult(This,pVal)	\
    (This)->lpVtbl -> get_FileNameResult(This,pVal)

#define ISAPacketObj_put_FileNameResult(This,newVal)	\
    (This)->lpVtbl -> put_FileNameResult(This,newVal)

#define ISAPacketObj_get_FileNameReport(This,pVal)	\
    (This)->lpVtbl -> get_FileNameReport(This,pVal)

#define ISAPacketObj_put_FileNameReport(This,newVal)	\
    (This)->lpVtbl -> put_FileNameReport(This,newVal)

#define ISAPacketObj_get_FileName(This,pVal)	\
    (This)->lpVtbl -> get_FileName(This,pVal)

#define ISAPacketObj_put_FileName(This,newVal)	\
    (This)->lpVtbl -> put_FileName(This,newVal)

#define ISAPacketObj_get_CommandResult(This,pVal)	\
    (This)->lpVtbl -> get_CommandResult(This,pVal)

#define ISAPacketObj_put_CommandResult(This,newVal)	\
    (This)->lpVtbl -> put_CommandResult(This,newVal)

#define ISAPacketObj_get_PathParameters(This,pVal)	\
    (This)->lpVtbl -> get_PathParameters(This,pVal)

#define ISAPacketObj_put_PathParameters(This,newVal)	\
    (This)->lpVtbl -> put_PathParameters(This,newVal)

#define ISAPacketObj_get_CVV2(This,pVal)	\
    (This)->lpVtbl -> get_CVV2(This,pVal)

#define ISAPacketObj_put_CVV2(This,newVal)	\
    (This)->lpVtbl -> put_CVV2(This,newVal)

#define ISAPacketObj_get_AmountFee(This,pVal)	\
    (This)->lpVtbl -> get_AmountFee(This,pVal)

#define ISAPacketObj_put_AmountFee(This,newVal)	\
    (This)->lpVtbl -> put_AmountFee(This,newVal)

#define ISAPacketObj_get_TerminalOutID(This,pVal)	\
    (This)->lpVtbl -> get_TerminalOutID(This,pVal)

#define ISAPacketObj_put_TerminalOutID(This,newVal)	\
    (This)->lpVtbl -> put_TerminalOutID(This,newVal)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_get_Amount_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [retval][out] */ BSTR __RPC_FAR *pVal);


void __RPC_STUB ISAPacketObj_get_Amount_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_put_Amount_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB ISAPacketObj_put_Amount_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_get_CurrencyCode_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [retval][out] */ BSTR __RPC_FAR *pVal);


void __RPC_STUB ISAPacketObj_get_CurrencyCode_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_put_CurrencyCode_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB ISAPacketObj_put_CurrencyCode_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_get_DateTimeHost_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [retval][out] */ BSTR __RPC_FAR *pVal);


void __RPC_STUB ISAPacketObj_get_DateTimeHost_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_put_DateTimeHost_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB ISAPacketObj_put_DateTimeHost_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_get_CardEntryMode_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [retval][out] */ long __RPC_FAR *pVal);


void __RPC_STUB ISAPacketObj_get_CardEntryMode_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_put_CardEntryMode_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [in] */ long newVal);


void __RPC_STUB ISAPacketObj_put_CardEntryMode_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_get_PAN_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [retval][out] */ BSTR __RPC_FAR *pVal);


void __RPC_STUB ISAPacketObj_get_PAN_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_put_PAN_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB ISAPacketObj_put_PAN_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_get_CardExpiryDate_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [retval][out] */ BSTR __RPC_FAR *pVal);


void __RPC_STUB ISAPacketObj_get_CardExpiryDate_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_put_CardExpiryDate_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB ISAPacketObj_put_CardExpiryDate_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_get_TRACK2_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [retval][out] */ BSTR __RPC_FAR *pVal);


void __RPC_STUB ISAPacketObj_get_TRACK2_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_put_TRACK2_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB ISAPacketObj_put_TRACK2_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_get_AuthorizationCode_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [retval][out] */ BSTR __RPC_FAR *pVal);


void __RPC_STUB ISAPacketObj_get_AuthorizationCode_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_put_AuthorizationCode_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB ISAPacketObj_put_AuthorizationCode_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_get_ReferenceNumber_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [retval][out] */ BSTR __RPC_FAR *pVal);


void __RPC_STUB ISAPacketObj_get_ReferenceNumber_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_put_ReferenceNumber_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB ISAPacketObj_put_ReferenceNumber_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_get_ResponseCodeHost_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [retval][out] */ BSTR __RPC_FAR *pVal);


void __RPC_STUB ISAPacketObj_get_ResponseCodeHost_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_put_ResponseCodeHost_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB ISAPacketObj_put_ResponseCodeHost_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_get_TextResponse_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [retval][out] */ BSTR __RPC_FAR *pVal);


void __RPC_STUB ISAPacketObj_get_TextResponse_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_put_TextResponse_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB ISAPacketObj_put_TextResponse_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_get_DateTimeCRM_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [retval][out] */ BSTR __RPC_FAR *pVal);


void __RPC_STUB ISAPacketObj_get_DateTimeCRM_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_put_DateTimeCRM_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB ISAPacketObj_put_DateTimeCRM_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_get_TrxID_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [retval][out] */ long __RPC_FAR *pVal);


void __RPC_STUB ISAPacketObj_get_TrxID_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_put_TrxID_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [in] */ long newVal);


void __RPC_STUB ISAPacketObj_put_TrxID_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_get_OrigOperation_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [retval][out] */ long __RPC_FAR *pVal);


void __RPC_STUB ISAPacketObj_get_OrigOperation_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_put_OrigOperation_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [in] */ long newVal);


void __RPC_STUB ISAPacketObj_put_OrigOperation_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_get_OperationCode_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [retval][out] */ long __RPC_FAR *pVal);


void __RPC_STUB ISAPacketObj_get_OperationCode_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_put_OperationCode_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [in] */ long newVal);


void __RPC_STUB ISAPacketObj_put_OperationCode_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_get_TrxIDCRM_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [retval][out] */ long __RPC_FAR *pVal);


void __RPC_STUB ISAPacketObj_get_TrxIDCRM_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_put_TrxIDCRM_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [in] */ long newVal);


void __RPC_STUB ISAPacketObj_put_TrxIDCRM_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_get_CRMID_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [retval][out] */ BSTR __RPC_FAR *pVal);


void __RPC_STUB ISAPacketObj_get_CRMID_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_put_CRMID_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB ISAPacketObj_put_CRMID_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_get_Status_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [retval][out] */ long __RPC_FAR *pVal);


void __RPC_STUB ISAPacketObj_get_Status_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_put_Status_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [in] */ long newVal);


void __RPC_STUB ISAPacketObj_put_Status_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_get_TrxIDHost_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [retval][out] */ long __RPC_FAR *pVal);


void __RPC_STUB ISAPacketObj_get_TrxIDHost_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_put_TrxIDHost_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [in] */ long newVal);


void __RPC_STUB ISAPacketObj_put_TrxIDHost_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_get_CommodityCode_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [retval][out] */ BSTR __RPC_FAR *pVal);


void __RPC_STUB ISAPacketObj_get_CommodityCode_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_put_CommodityCode_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB ISAPacketObj_put_CommodityCode_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_get_PaymentDetails_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [retval][out] */ BSTR __RPC_FAR *pVal);


void __RPC_STUB ISAPacketObj_get_PaymentDetails_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_put_PaymentDetails_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB ISAPacketObj_put_PaymentDetails_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_get_ProviderCode_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [retval][out] */ BSTR __RPC_FAR *pVal);


void __RPC_STUB ISAPacketObj_get_ProviderCode_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_put_ProviderCode_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB ISAPacketObj_put_ProviderCode_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_get_ProcessingFlag_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [retval][out] */ long __RPC_FAR *pVal);


void __RPC_STUB ISAPacketObj_get_ProcessingFlag_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_put_ProcessingFlag_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [in] */ long newVal);


void __RPC_STUB ISAPacketObj_put_ProcessingFlag_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_get_FileNameResult_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [retval][out] */ BSTR __RPC_FAR *pVal);


void __RPC_STUB ISAPacketObj_get_FileNameResult_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_put_FileNameResult_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB ISAPacketObj_put_FileNameResult_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_get_FileNameReport_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [retval][out] */ BSTR __RPC_FAR *pVal);


void __RPC_STUB ISAPacketObj_get_FileNameReport_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_put_FileNameReport_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB ISAPacketObj_put_FileNameReport_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_get_FileName_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [retval][out] */ BSTR __RPC_FAR *pVal);


void __RPC_STUB ISAPacketObj_get_FileName_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_put_FileName_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB ISAPacketObj_put_FileName_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_get_CommandResult_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [retval][out] */ long __RPC_FAR *pVal);


void __RPC_STUB ISAPacketObj_get_CommandResult_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_put_CommandResult_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [in] */ long newVal);


void __RPC_STUB ISAPacketObj_put_CommandResult_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_get_PathParameters_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [retval][out] */ BSTR __RPC_FAR *pVal);


void __RPC_STUB ISAPacketObj_get_PathParameters_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_put_PathParameters_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB ISAPacketObj_put_PathParameters_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_get_CVV2_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [retval][out] */ BSTR __RPC_FAR *pVal);


void __RPC_STUB ISAPacketObj_get_CVV2_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_put_CVV2_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB ISAPacketObj_put_CVV2_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_get_AmountFee_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [retval][out] */ BSTR __RPC_FAR *pVal);


void __RPC_STUB ISAPacketObj_get_AmountFee_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_put_AmountFee_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB ISAPacketObj_put_AmountFee_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_get_TerminalOutID_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [retval][out] */ BSTR __RPC_FAR *pVal);


void __RPC_STUB ISAPacketObj_get_TerminalOutID_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ISAPacketObj_put_TerminalOutID_Proxy( 
    ISAPacketObj __RPC_FAR * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB ISAPacketObj_put_TerminalOutID_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __ISAPacketObj_INTERFACE_DEFINED__ */


#ifndef __IPCPOSTConnectorObj_INTERFACE_DEFINED__
#define __IPCPOSTConnectorObj_INTERFACE_DEFINED__

/* interface IPCPOSTConnectorObj */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IPCPOSTConnectorObj;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("7524E334-414D-4EBD-BCEC-56E96791F5DC")
    IPCPOSTConnectorObj : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE InitResources( 
            /* [retval][out] */ long __RPC_FAR *retval) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE FreeResources( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetChanelParam( 
            /* [in] */ BSTR IPAddress,
            /* [in] */ long IPPort,
            /* [in] */ BSTR to,
            /* [retval][out] */ long __RPC_FAR *Result) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetProtocolParam( 
            /* [in] */ long TimeoutAck,
            /* [in] */ long TimeoutPacket,
            /* [in] */ long CountNAK,
            /* [in] */ long PacketSize,
            /* [retval][out] */ long __RPC_FAR *retval) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Exchange( 
            /* [in] */ ISAPacketObj __RPC_FAR *__RPC_FAR *Request,
            /* [in] */ ISAPacketObj __RPC_FAR *__RPC_FAR *Response,
            /* [in] */ long Timeout,
            /* [retval][out] */ long __RPC_FAR *retval) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ErrorDescription( 
            /* [retval][out] */ BSTR __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ErrorDescription( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ErrorCode( 
            /* [retval][out] */ short __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ErrorCode( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetControlServerParam( 
            /* [in] */ BSTR IPAddress,
            /* [in] */ long IPPort,
            /* [in] */ long Timeout,
            /* [retval][out] */ long __RPC_FAR *retval) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ControlServerStart( 
            /* [retval][out] */ long __RPC_FAR *retval) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ControlServerStop( 
            /* [retval][out] */ long __RPC_FAR *retval) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IPCPOSTConnectorObjVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IPCPOSTConnectorObj __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IPCPOSTConnectorObj __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IPCPOSTConnectorObj __RPC_FAR * This);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfoCount )( 
            IPCPOSTConnectorObj __RPC_FAR * This,
            /* [out] */ UINT __RPC_FAR *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfo )( 
            IPCPOSTConnectorObj __RPC_FAR * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo __RPC_FAR *__RPC_FAR *ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetIDsOfNames )( 
            IPCPOSTConnectorObj __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR __RPC_FAR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID __RPC_FAR *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Invoke )( 
            IPCPOSTConnectorObj __RPC_FAR * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS __RPC_FAR *pDispParams,
            /* [out] */ VARIANT __RPC_FAR *pVarResult,
            /* [out] */ EXCEPINFO __RPC_FAR *pExcepInfo,
            /* [out] */ UINT __RPC_FAR *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *InitResources )( 
            IPCPOSTConnectorObj __RPC_FAR * This,
            /* [retval][out] */ long __RPC_FAR *retval);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *FreeResources )( 
            IPCPOSTConnectorObj __RPC_FAR * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *SetChanelParam )( 
            IPCPOSTConnectorObj __RPC_FAR * This,
            /* [in] */ BSTR IPAddress,
            /* [in] */ long IPPort,
            /* [in] */ BSTR to,
            /* [retval][out] */ long __RPC_FAR *Result);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *SetProtocolParam )( 
            IPCPOSTConnectorObj __RPC_FAR * This,
            /* [in] */ long TimeoutAck,
            /* [in] */ long TimeoutPacket,
            /* [in] */ long CountNAK,
            /* [in] */ long PacketSize,
            /* [retval][out] */ long __RPC_FAR *retval);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Exchange )( 
            IPCPOSTConnectorObj __RPC_FAR * This,
            /* [in] */ ISAPacketObj __RPC_FAR *__RPC_FAR *Request,
            /* [in] */ ISAPacketObj __RPC_FAR *__RPC_FAR *Response,
            /* [in] */ long Timeout,
            /* [retval][out] */ long __RPC_FAR *retval);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_ErrorDescription )( 
            IPCPOSTConnectorObj __RPC_FAR * This,
            /* [retval][out] */ BSTR __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_ErrorDescription )( 
            IPCPOSTConnectorObj __RPC_FAR * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_ErrorCode )( 
            IPCPOSTConnectorObj __RPC_FAR * This,
            /* [retval][out] */ short __RPC_FAR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_ErrorCode )( 
            IPCPOSTConnectorObj __RPC_FAR * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *SetControlServerParam )( 
            IPCPOSTConnectorObj __RPC_FAR * This,
            /* [in] */ BSTR IPAddress,
            /* [in] */ long IPPort,
            /* [in] */ long Timeout,
            /* [retval][out] */ long __RPC_FAR *retval);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *ControlServerStart )( 
            IPCPOSTConnectorObj __RPC_FAR * This,
            /* [retval][out] */ long __RPC_FAR *retval);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *ControlServerStop )( 
            IPCPOSTConnectorObj __RPC_FAR * This,
            /* [retval][out] */ long __RPC_FAR *retval);
        
        END_INTERFACE
    } IPCPOSTConnectorObjVtbl;

    interface IPCPOSTConnectorObj
    {
        CONST_VTBL struct IPCPOSTConnectorObjVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IPCPOSTConnectorObj_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IPCPOSTConnectorObj_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IPCPOSTConnectorObj_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IPCPOSTConnectorObj_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IPCPOSTConnectorObj_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IPCPOSTConnectorObj_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IPCPOSTConnectorObj_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IPCPOSTConnectorObj_InitResources(This,retval)	\
    (This)->lpVtbl -> InitResources(This,retval)

#define IPCPOSTConnectorObj_FreeResources(This)	\
    (This)->lpVtbl -> FreeResources(This)

#define IPCPOSTConnectorObj_SetChanelParam(This,IPAddress,IPPort,to,Result)	\
    (This)->lpVtbl -> SetChanelParam(This,IPAddress,IPPort,to,Result)

#define IPCPOSTConnectorObj_SetProtocolParam(This,TimeoutAck,TimeoutPacket,CountNAK,PacketSize,retval)	\
    (This)->lpVtbl -> SetProtocolParam(This,TimeoutAck,TimeoutPacket,CountNAK,PacketSize,retval)

#define IPCPOSTConnectorObj_Exchange(This,Request,Response,Timeout,retval)	\
    (This)->lpVtbl -> Exchange(This,Request,Response,Timeout,retval)

#define IPCPOSTConnectorObj_get_ErrorDescription(This,pVal)	\
    (This)->lpVtbl -> get_ErrorDescription(This,pVal)

#define IPCPOSTConnectorObj_put_ErrorDescription(This,newVal)	\
    (This)->lpVtbl -> put_ErrorDescription(This,newVal)

#define IPCPOSTConnectorObj_get_ErrorCode(This,pVal)	\
    (This)->lpVtbl -> get_ErrorCode(This,pVal)

#define IPCPOSTConnectorObj_put_ErrorCode(This,newVal)	\
    (This)->lpVtbl -> put_ErrorCode(This,newVal)

#define IPCPOSTConnectorObj_SetControlServerParam(This,IPAddress,IPPort,Timeout,retval)	\
    (This)->lpVtbl -> SetControlServerParam(This,IPAddress,IPPort,Timeout,retval)

#define IPCPOSTConnectorObj_ControlServerStart(This,retval)	\
    (This)->lpVtbl -> ControlServerStart(This,retval)

#define IPCPOSTConnectorObj_ControlServerStop(This,retval)	\
    (This)->lpVtbl -> ControlServerStop(This,retval)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IPCPOSTConnectorObj_InitResources_Proxy( 
    IPCPOSTConnectorObj __RPC_FAR * This,
    /* [retval][out] */ long __RPC_FAR *retval);


void __RPC_STUB IPCPOSTConnectorObj_InitResources_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IPCPOSTConnectorObj_FreeResources_Proxy( 
    IPCPOSTConnectorObj __RPC_FAR * This);


void __RPC_STUB IPCPOSTConnectorObj_FreeResources_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IPCPOSTConnectorObj_SetChanelParam_Proxy( 
    IPCPOSTConnectorObj __RPC_FAR * This,
    /* [in] */ BSTR IPAddress,
    /* [in] */ long IPPort,
    /* [in] */ BSTR to,
    /* [retval][out] */ long __RPC_FAR *Result);


void __RPC_STUB IPCPOSTConnectorObj_SetChanelParam_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IPCPOSTConnectorObj_SetProtocolParam_Proxy( 
    IPCPOSTConnectorObj __RPC_FAR * This,
    /* [in] */ long TimeoutAck,
    /* [in] */ long TimeoutPacket,
    /* [in] */ long CountNAK,
    /* [in] */ long PacketSize,
    /* [retval][out] */ long __RPC_FAR *retval);


void __RPC_STUB IPCPOSTConnectorObj_SetProtocolParam_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IPCPOSTConnectorObj_Exchange_Proxy( 
    IPCPOSTConnectorObj __RPC_FAR * This,
    /* [in] */ ISAPacketObj __RPC_FAR *__RPC_FAR *Request,
    /* [in] */ ISAPacketObj __RPC_FAR *__RPC_FAR *Response,
    /* [in] */ long Timeout,
    /* [retval][out] */ long __RPC_FAR *retval);


void __RPC_STUB IPCPOSTConnectorObj_Exchange_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPCPOSTConnectorObj_get_ErrorDescription_Proxy( 
    IPCPOSTConnectorObj __RPC_FAR * This,
    /* [retval][out] */ BSTR __RPC_FAR *pVal);


void __RPC_STUB IPCPOSTConnectorObj_get_ErrorDescription_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IPCPOSTConnectorObj_put_ErrorDescription_Proxy( 
    IPCPOSTConnectorObj __RPC_FAR * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB IPCPOSTConnectorObj_put_ErrorDescription_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IPCPOSTConnectorObj_get_ErrorCode_Proxy( 
    IPCPOSTConnectorObj __RPC_FAR * This,
    /* [retval][out] */ short __RPC_FAR *pVal);


void __RPC_STUB IPCPOSTConnectorObj_get_ErrorCode_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IPCPOSTConnectorObj_put_ErrorCode_Proxy( 
    IPCPOSTConnectorObj __RPC_FAR * This,
    /* [in] */ short newVal);


void __RPC_STUB IPCPOSTConnectorObj_put_ErrorCode_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IPCPOSTConnectorObj_SetControlServerParam_Proxy( 
    IPCPOSTConnectorObj __RPC_FAR * This,
    /* [in] */ BSTR IPAddress,
    /* [in] */ long IPPort,
    /* [in] */ long Timeout,
    /* [retval][out] */ long __RPC_FAR *retval);


void __RPC_STUB IPCPOSTConnectorObj_SetControlServerParam_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IPCPOSTConnectorObj_ControlServerStart_Proxy( 
    IPCPOSTConnectorObj __RPC_FAR * This,
    /* [retval][out] */ long __RPC_FAR *retval);


void __RPC_STUB IPCPOSTConnectorObj_ControlServerStart_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IPCPOSTConnectorObj_ControlServerStop_Proxy( 
    IPCPOSTConnectorObj __RPC_FAR * This,
    /* [retval][out] */ long __RPC_FAR *retval);


void __RPC_STUB IPCPOSTConnectorObj_ControlServerStop_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IPCPOSTConnectorObj_INTERFACE_DEFINED__ */



#ifndef __ARCCOMLib_LIBRARY_DEFINED__
#define __ARCCOMLib_LIBRARY_DEFINED__

/* library ARCCOMLib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_ARCCOMLib;

EXTERN_C const CLSID CLSID_SAPacketObj;

#ifdef __cplusplus

class DECLSPEC_UUID("69ABA4AF-536D-497D-A0D7-6956AEA24CF5")
SAPacketObj;
#endif

EXTERN_C const CLSID CLSID_PCPOSTConnectorObj;

#ifdef __cplusplus

class DECLSPEC_UUID("B69550A7-8488-4AFB-8A58-19A293920EAB")
PCPOSTConnectorObj;
#endif
#endif /* __ARCCOMLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long __RPC_FAR *, unsigned long            , BSTR __RPC_FAR * ); 
unsigned char __RPC_FAR * __RPC_USER  BSTR_UserMarshal(  unsigned long __RPC_FAR *, unsigned char __RPC_FAR *, BSTR __RPC_FAR * ); 
unsigned char __RPC_FAR * __RPC_USER  BSTR_UserUnmarshal(unsigned long __RPC_FAR *, unsigned char __RPC_FAR *, BSTR __RPC_FAR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long __RPC_FAR *, BSTR __RPC_FAR * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif
