object fmMainSber: TfmMainSber
  Left = 249
  Top = 378
  Width = 696
  Height = 610
  Caption = #1041#1077#1079#1085#1072#1083' '#1089#1073#1077#1088#1072
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TcxMemo
    Left = 184
    Top = 8
    Lines.Strings = (
      'Memo1')
    TabOrder = 0
    Height = 561
    Width = 497
  end
  object cxButton1: TcxButton
    Left = 24
    Top = 24
    Width = 140
    Height = 25
    Caption = #1054#1090#1082#1088#1099#1090#1100
    TabOrder = 1
    OnClick = cxButton1Click
    LookAndFeel.Kind = lfOffice11
  end
  object cxButton2: TcxButton
    Left = 24
    Top = 64
    Width = 140
    Height = 25
    Caption = #1055#1088#1086#1076#1072#1078#1072'  '#1087#1088#1086#1089#1090#1072#1103
    TabOrder = 2
    LookAndFeel.Kind = lfOffice11
  end
  object cxButton4: TcxButton
    Left = 24
    Top = 100
    Width = 140
    Height = 25
    Caption = #1055#1088#1086#1076#1072#1078#1072' DLL'
    TabOrder = 3
    OnClick = cxButton4Click
    LookAndFeel.Kind = lfOffice11
  end
  object cxButton5: TcxButton
    Left = 24
    Top = 140
    Width = 140
    Height = 25
    Caption = #1055#1088#1086#1076#1072#1078#1072' EXE'
    TabOrder = 4
    OnClick = cxButton5Click
    LookAndFeel.Kind = lfOffice11
  end
  object cxButton6: TcxButton
    Left = 24
    Top = 184
    Width = 140
    Height = 25
    Caption = #1055#1088#1086#1076#1072#1078#1072' 1'
    TabOrder = 5
    OnClick = cxButton6Click
    LookAndFeel.Kind = lfOffice11
  end
  object cxCEdit1: TcxSpinEdit
    Left = 24
    Top = 512
    Style.LookAndFeel.Kind = lfOffice11
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.BorderStyle = ebsOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 6
    Value = 100
    Width = 121
  end
  object cxButton8: TcxButton
    Left = 24
    Top = 464
    Width = 140
    Height = 25
    Caption = #1047#1072#1082#1088#1099#1090#1100
    TabOrder = 7
    OnClick = cxButton8Click
    LookAndFeel.Kind = lfOffice11
  end
  object cxButton3: TcxButton
    Left = 28
    Top = 232
    Width = 75
    Height = 25
    Caption = 'cxButton3'
    TabOrder = 8
    OnClick = cxButton3Click
    LookAndFeel.Kind = lfOffice11
  end
  object cxButton7: TcxButton
    Left = 28
    Top = 272
    Width = 75
    Height = 25
    Caption = 'cxButton7'
    TabOrder = 9
    OnClick = cxButton7Click
    LookAndFeel.Kind = lfOffice11
  end
  object dxfBackGround1: TdxfBackGround
    BkColor.BeginColor = 16767449
    BkColor.EndColor = clBlue
    BkColor.FillStyle = fsVert
    BkAnimate.Speed = 700
    Left = 184
    Top = 360
  end
end
