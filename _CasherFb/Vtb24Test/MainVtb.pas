unit MainVtb;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxControls, cxContainer, cxEdit, cxTextEdit, cxMemo, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons,
  ComObj, ActiveX, OleServer, cxCurrencyEdit, dxfBackGround, cxMaskEdit,
  cxSpinEdit, ShellAPI;

type
  TfmMainSber = class(TForm)
    Memo1: TcxMemo;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton4: TcxButton;
    cxButton5: TcxButton;
    cxButton6: TcxButton;
    dxfBackGround1: TdxfBackGround;
    cxCEdit1: TcxSpinEdit;
    cxButton8: TcxButton;
    cxButton3: TcxButton;
    cxButton7: TcxButton;
    procedure FormShow(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton8Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
    procedure cxButton9Click(Sender: TObject);
    procedure cxButton10Click(Sender: TObject);
    procedure cxButton11Click(Sender: TObject);
    procedure cxButton12Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton7Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;


type TReqv = record //ProcessOW
       handle:Integer;
       abg_id:Integer;
       operType:Integer; //[in] ��� �������� (��������)
       track2:pchar; //[in] ����2
       pan:pchar; //[out] PAN
       expiry:pchar; //[out] Expiry Date ����
       pay_acc:pchar; //�� ������������
       additional_payment_data:pchar; //�� ������������
       amount:pchar; //[in] ����� � ��������
       original_amount:pchar; //[in]������������ ����� � //��������
       currency:pchar; //[in] ��� ������
       terminalID:pchar; //[out][in] ID ���������
       rrn:pchar; //[in][out] ������ (��������� ������ ��� ��� �������� ��� ������� ��� �����, � ��������� ������� ������ ���� �����)
       authCode:pchar; //[out][in] ��� �����������
       responseCode:pchar; //[out] ��� ������
       cardType:pchar; //[out] �������� ���� �����
       date:pchar; //[out] ���� ����������
       time:pchar; //[out] ����� ����������
       payment_data:pchar; //�� ������������
       data_to_print:pchar; //�� ������������
       home_operator:pchar; //�� ������������
       received_text_message:pchar;//�� ������������
       text_message:pchar; //[out] �����������
       AID:pchar; //[out]EMV AID
       ApplicationLabel:pchar; //[out]EMV ApplicationLabel
       TVR:pchar; //[out]EMV TVR
       system_res:Integer; //�� ������������
       enc_data:pchar; // [in][out]����������� ������
     end;

function IsOLEObjectInstalled(Name: String): boolean;

function fVTB1(iOp:INteger;rSum:Real;Memo1:TcxMemo):Boolean;

function fVTB2(iOp,iSum:INteger;Memo1:TcxMemo):Boolean;

function prClearAnsw2:Boolean;   //������ ��������� ����� ����� �� ����������� �������� �����

//Function ProcessOW(Reqv:TReqv):Integer; stdcall; far; external 'arccom.dll' name 'PROCFUNC';

function fGetAnsw2(Var sAnsw,sCheck:String):Boolean;

var
  fmMainSber: TfmMainSber;
  Drv,Req,Resp: OleVariant;
  vCh: Variant;
  iMoney:Integer;
  Reqv:TReqv;

implementation

uses Un1, Vtb24un;


{$R *.dfm}

function fGetAnsw2(Var sAnsw,sCheck:String):Boolean;
Var f:TextFile;
    bErr:Boolean;
    strwk:String;
begin
  bErr:=True;
  sAnsw:='';
  sCheck:='';

  if FileExists(CommonSet.VTBPath+'rc.out') then
  begin
    try
      bErr:=False;
      assignfile(f,CommonSet.VTBPath+'rc.out');
      reset(f);
      readln(f,sAnsw);
      sAnsw:=Trim(sAnsw);
      bErr:=True;
    finally
      closefile(f);
    end;
  end;

  if bErr then
  begin
    if FileExists(CommonSet.VTBPath+'cheq.out') then
    begin
      try
        bErr:=False;
        assignfile(f,CommonSet.VTBPath+'cheq.out');
        reset(f);
        while not EOF(f) do
        begin
          readln(f,StrWk);
          sCheck:=sCheck+trim(StrWk)+#$0D;
//          sCheck:=sCheck+StrWk;
        end;
        bErr:=True;
      finally
        closefile(f);
      end;
    end;
  end;
  result:=bErr;
end;


function prClearAnsw2:Boolean;   //������ ��������� ����� ����� �� ����������� �������� �����
Var f:TextFile;
    bErr:Boolean;
begin
  bErr:=True; //��� ������

  if FileExists(CommonSet.VTBPath+'rc.out') then
  begin
    try
      bErr:=False;
      assignfile(f,CommonSet.VTBPath+'rc.out');
      rewrite(f);
      bErr:=True;
    finally
      closefile(f);
    end;
  end;

  if bErr then
  begin
    if FileExists(CommonSet.VTBPath+'cheq.out') then
    begin
      try
        bErr:=False;
        assignfile(f,CommonSet.VTBPath+'cheq.out');
        rewrite(f);
        bErr:=True;
      finally
        closefile(f);
      end;
    end;
  end;

  result:=bErr;
end;


function fVTB2(iOp,iSum:INteger;Memo1:TcxMemo):Boolean;
var
  SEInfo: TShellExecuteInfo;
  ExitCode: DWORD;
  ExecuteFile, ParamString: string;
  iC:INteger;

  procedure prWM(sStr:String);
  begin
    if Memo1<>nil then
    begin
      Memo1.Lines.Add(sStr+' '+FormatDateTime('hh:nn:sss',now));
      delay(10);
    end;
  end;

begin
  if not FileExists(CurDir+'CommandLineTool.exe') then
  begin
    prWM(' ������ ��� �� �������� (CommandLineTool.exe). ���������� �������� ���������.');
    Result:=False;
    exit;
  end;

  if prClearAnsw2=False then
  begin
    prWM(' ������ ������ � ������� ������. ���������� �������� ���������.');
    Result:=False;
    exit;
  end;

  ExecuteFile := 'CommandLineTool.exe';
  ParamString:=' /o'+its(iOp)+' /a'+its(iSum);

  FillChar(SEInfo, SizeOf(SEInfo), 0);
  SEInfo.cbSize := SizeOf(TShellExecuteInfo);
  with SEInfo do
  begin
    fMask := SEE_MASK_NOCLOSEPROCESS;
    Wnd := Application.Handle;
    lpFile := PChar(ExecuteFile);
    lpParameters := PChar(ParamString);
  {StartInString specifies thename of the working
  directory.If ommited, the current
  directory is used.}
  // lpDirectory := PChar(StartInString);
    nShow := SW_SHOWNORMAL;
  end;
  try
    prWM(' ������ ��������. ');
    iC:=0;

//    if ShellExecute(SEInfo.Wnd,'open',SEInfo.lpFile,SEInfo.lpParameters,SW_SHOWNORMAL) then

    if ShellExecuteEx(@SEInfo) then
    begin
      repeat
        delay(100);
        GetExitCodeProcess(SEInfo.hProcess, ExitCode);
        inc(iC);
        if iC mod 50 = 0 then prWM('  ������� ���� '); //������ 5 ���
      until (ExitCode <> STILL_ACTIVE) or Application.Terminated;
      prWM(' ������� ��������. ');
    end else  prWM('������ ������� ��������.');
    Result:=True;
  except
    prWM('������ ������� ��������..');
    result:=False;
  end;
end;


function fVTB1(iOp:INteger;rSum:Real;Memo1:TcxMemo):Boolean;
var
  SEInfo: TShellExecuteInfo;
  ExitCode: DWORD;
  ExecuteFile, ParamString: string;
  iSum:INteger;
  iC:INteger;

  procedure prWM(sStr:String);
  begin
    if Memo1<>nil then
    begin
      Memo1.Lines.Add(sStr+' '+FormatDateTime('hh:nn:sss',now));
      delay(10);
    end;
  end;

begin
  ExecuteFile := 'CommandLineTool.exe';
  iSum:=roundex(rSum*100);
  ParamString:=' /o'+its(iOp)+' /a'+its(iSum);

  FillChar(SEInfo, SizeOf(SEInfo), 0);
  SEInfo.cbSize := SizeOf(TShellExecuteInfo);
  with SEInfo do
  begin
    fMask := SEE_MASK_NOCLOSEPROCESS;
    Wnd := Application.Handle;
    lpFile := PChar(ExecuteFile);
    lpParameters := PChar(ParamString);
  {StartInString specifies thename of the working
  directory.If ommited, the current
  directory is used.}
  // lpDirectory := PChar(StartInString);
    nShow := SW_SHOWNORMAL;
  end;
  try
    prWM(' ������ ��������. ');
    iC:=0;

    if ShellExecuteEx(@SEInfo) then
    begin
      repeat
        delay(100);
        GetExitCodeProcess(SEInfo.hProcess, ExitCode);
        inc(iC);
        if iC mod 50 = 0 then prWM('  ������� ���� '); //������ 5 ���
      until (ExitCode <> STILL_ACTIVE) or Application.Terminated;
      prWM(' ������� ��������. ');
    end else  prWM('������ ������� ��������.');
  except
    prWM('������ ������� ��������..');
  end;
  Result:=True;
end;


function IsOLEObjectInstalled(Name: String): boolean;
var
  ClassID: TCLSID;
  Rez : HRESULT;
begin

// ���� CLSID OLE-�������
  Rez := CLSIDFromProgID(PWideChar(WideString(Name)), ClassID);
  if Rez = S_OK then
 // ������ ������
    Result := true
  else
    Result := false;
end;


procedure TfmMainSber.FormShow(Sender: TObject);
begin
  Memo1.Clear;
end;

procedure TfmMainSber.cxButton1Click(Sender: TObject);
Var tTime:TDateTime;
    StrT:String;
begin
//�������
  Memo1.Lines.Add('');
  Memo1.Lines.Add('��������� ����������.');
  tTime:=now;

  if IsOLEObjectInstalled('ArcCom.SAPacketObj') then
  begin
    tTime:=Now-tTime;
    StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
    Memo1.Lines.Add('������� ArcCom.SAPacketObj ����������.'+StrT);
  end else
  begin
    tTime:=Now-tTime;
    StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
    Memo1.Lines.Add('������� ArcCom.SAPacketObj �� ���������� !!!'+StrT);
    exit;
  end;

  tTime:=now;

  if IsOLEObjectInstalled('ArcCom.PCPOSTConnectorObj') then
  begin
    tTime:=Now-tTime;
    StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
    Memo1.Lines.Add('������� ArcCom.PCPOSTConnectorObj ����������.'+StrT);
  end else
  begin
    tTime:=Now-tTime;
    StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
    Memo1.Lines.Add('�������   ArcCom.PCPOSTConnectorObj  �� ���������� !!!'+StrT);
    exit;
  end;


  Memo1.Lines.Add('');
  Memo1.Lines.Add('������� ������ ��������.');
  tTime:=now;
  Drv := CreateOleObject('ArcCom.PCPOSTConnectorObj');
  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('�������� �������.'+StrT);

  Memo1.Lines.Add('');
  Memo1.Lines.Add('������� ������ �������.');
  tTime:=now;
  Req := CreateOleObject('ArcCom.SAPacketObj');
  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('�������� �������.'+StrT);

  Memo1.Lines.Add('');
  Memo1.Lines.Add('������� ������ ������.');
  tTime:=now;
  Resp := CreateOleObject('ArcCom.SAPacketObj');
  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('�������� �������.'+StrT);

end;

procedure TfmMainSber.cxButton8Click(Sender: TObject);
Var tTime:TDateTime;
    StrT:String;
begin
  Memo1.Lines.Add('');
  Memo1.Lines.Add('��������� ����������. (����)');
  tTime:=now;

{
  Drv.re
  Req.FreeResources;
  Resp.FreeResources;
}
  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('��������� ������. '+StrT);
end;

procedure TfmMainSber.cxButton4Click(Sender: TObject);
Var tTime:TDateTime;
    StrT:String;
    iRes:Integer;
    StrWk:String;
begin
  Memo1.Lines.Add('');
  Memo1.Lines.Add('������� DLL');

  Reqv.operType:=1;
  Reqv.amount:='111'; //[in] ����� � ��������
  Reqv.original_amount:='111'; //[in]������������ ����� � //��������
  Reqv.currency:='643'; //[in] ��� ������

//  ProcessOW(Reqv);

  tTime:=now;

//  iRes:=Drv.NFun(4006);
  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('����� : '+StrT);

  Memo1.Lines.Add('');
end;

procedure TfmMainSber.cxButton5Click(Sender: TObject);
Var tTime:TDateTime;
    StrT:String;
    iRes:Integer;
    StrWk:String;
begin
  Memo1.Lines.Add('');
  Memo1.Lines.Add('������� EXE');
  tTime:=now;

  try
    ShellExecute(handle, 'open', 'CommandLineTool.exe',' /o1 /a102','', SW_SHOWNORMAL);
  except
    showmessage('������ �� ��������. ���������� � ��������������.');
  end;



  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('����� : '+StrT);

 {
  Memo1.Lines.Add('----��������� : '+IntToStr(iRes));
  StrWk:=Drv.GParam('ClientCard');
  Memo1.Lines.Add('----����� ������� : '+StrWk);
  StrWk:=Drv.GParam('ClientExpiryDate');
  Memo1.Lines.Add('----���� ��������� �������� : '+StrWk);
  Memo1.Lines.Add('');
  Memo1.Lines.Add('���������� ���');
  vCh:=Drv.GParam('Cheque');
  Memo1.Lines.Add('� ������');
  StrWk:= vCh;
  Memo1.Lines.Add('--------------------------------------');
  if length(StrWk)>0 then
  begin
    Memo1.Lines.Add(StrWk);
  end;
  Memo1.Lines.Add('--------------------------------------');
  Memo1.Lines.Add('');
  Memo1.Lines.Add('');}
end;

procedure TfmMainSber.cxButton6Click(Sender: TObject);
begin
  fVTB2(1,cxCEdit1.Value,Memo1);
end;

procedure TfmMainSber.cxButton9Click(Sender: TObject);
Var tTime:TDateTime;
    StrT:String;
    iRes:Integer;
    StrWk:String;
begin
  Memo1.Lines.Add('');
  Memo1.Lines.Add('�������� ��� �� ��������� � ����������� ������� (6000) ');
  tTime:=now;

  Drv.Clear;
  iRes:=Drv.NFun(6000);
  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('����� : '+StrT);
  Memo1.Lines.Add('----��������� : '+IntToStr(iRes));
  Memo1.Lines.Add('');
  Memo1.Lines.Add('���������� ���');
  vCh:=Drv.GParam('Cheque');
  Memo1.Lines.Add('� ������');
  StrWk:= vCh;
  Memo1.Lines.Add('--------------------------------------');
  if length(StrWk)>0 then
  begin
    Memo1.Lines.Add(StrWk);
  end;
  Memo1.Lines.Add('--------------------------------------');
  Memo1.Lines.Add('');
  Memo1.Lines.Add('');
end;


procedure TfmMainSber.cxButton10Click(Sender: TObject);
Var tTime:TDateTime;
    StrT:String;
    iRes:Integer;
    StrWk:String;
begin
  Memo1.Lines.Add('');
  Memo1.Lines.Add('���������� ��������   3001 ');
  tTime:=now;

  Drv.Clear;
  iRes:=Drv.NFun(3001);
  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('����� : '+StrT);
  Memo1.Lines.Add('----��������� : '+IntToStr(iRes));
  Memo1.Lines.Add('');
  Memo1.Lines.Add('���������� ���');
  vCh:=Drv.GParam('Cheque');
  Memo1.Lines.Add('� ������');
  StrWk:= vCh;
  Memo1.Lines.Add('--------------------------------------');
  if length(StrWk)>0 then
  begin
    Memo1.Lines.Add(StrWk);
  end;
  Memo1.Lines.Add('--------------------------------------');
  Memo1.Lines.Add('');
  Memo1.Lines.Add('');
end;


procedure TfmMainSber.cxButton11Click(Sender: TObject);
Var tTime:TDateTime;
    StrT:String;
    iRes:Integer;
    StrWk:String;
begin
  Memo1.Lines.Add('');
  Memo1.Lines.Add('������� �����    7000 ');
  tTime:=now;

  Drv.Clear;
  iRes:=Drv.NFun(7000);
  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('����� : '+StrT);
  Memo1.Lines.Add('----��������� : '+IntToStr(iRes));
  Memo1.Lines.Add('');
  Memo1.Lines.Add('���������� ���');
  vCh:=Drv.GParam('Cheque');
  Memo1.Lines.Add('� ������');
  StrWk:= vCh;
  Memo1.Lines.Add('--------------------------------------');
  if length(StrWk)>0 then
  begin
    Memo1.Lines.Add(StrWk);
  end;
  Memo1.Lines.Add('--------------------------------------');
  Memo1.Lines.Add('');
  Memo1.Lines.Add('');
end;


procedure TfmMainSber.cxButton12Click(Sender: TObject);
Var tTime:TDateTime;
    StrT:String;
    iRes:Integer;
    StrWk:String;
begin
  Memo1.Lines.Add('');
  Memo1.Lines.Add('������ ����. ���������  7001 ');
  tTime:=now;

  Drv.Clear;
  iRes:=Drv.NFun(7001);
  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('����� : '+StrT);
  Memo1.Lines.Add('----��������� : '+IntToStr(iRes));
  Memo1.Lines.Add('');
  Memo1.Lines.Add('���������� ���');
  vCh:=Drv.GParam('Cheque');
  Memo1.Lines.Add('� ������');
  StrWk:= vCh;
  Memo1.Lines.Add('--------------------------------------');
  if length(StrWk)>0 then
  begin
    Memo1.Lines.Add(StrWk);
  end;
  Memo1.Lines.Add('--------------------------------------');
  Memo1.Lines.Add('');
  Memo1.Lines.Add('');
end;

procedure TfmMainSber.FormCreate(Sender: TObject);
begin
  CommonSet.VTBPath:='C:\Arcus2\';
end;

procedure TfmMainSber.cxButton3Click(Sender: TObject);
Var sAnsw:String;
begin
  if fVTB2(1,cxCEdit1.Value,Memo1) then
  begin
    if fGetAnsw2(sAnsw,BnStr) then  //���� ����� � ��� ��������
    begin
      Memo1.Lines.Add('��� ������ �����������  '+sAnsw);
      Memo1.Lines.Add('');
      Memo1.Lines.Add('--------------------------------------');
      if length(BnStr)>0 then
      begin
        Memo1.Lines.Add(BnStr);
      end;
      Memo1.Lines.Add('--------------------------------------');
      Memo1.Lines.Add('');
      Memo1.Lines.Add('');
      if sAnsw='000' then ModalResult:=mrOk;
    end else
    begin
      Memo1.Lines.Add('������ ������� � ������ ������. �������� �����������.');
    end;
  end else Memo1.Lines.Add('������. �������� �����������');
end;

procedure TfmMainSber.cxButton7Click(Sender: TObject);
begin
  iMoneyBn:=cxCEdit1.Value;
  fmVTB.ShowModal;
end;

end.
