unit MainSber;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxControls, cxContainer, cxEdit, cxTextEdit, cxMemo, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons,
  ComObj, ActiveX, OleServer, cxCurrencyEdit, dxfBackGround, cxMaskEdit,
  cxSpinEdit, ShellAPI;

type
  TfmMainSber = class(TForm)
    Memo1: TcxMemo;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    cxButton5: TcxButton;
    cxButton6: TcxButton;
    cxButton7: TcxButton;
    cxButton8: TcxButton;
    dxfBackGround1: TdxfBackGround;
    cxButton9: TcxButton;
    cxButton10: TcxButton;
    cxButton11: TcxButton;
    cxButton12: TcxButton;
    cxCEdit1: TcxSpinEdit;
    procedure FormShow(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton8Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
    procedure cxButton7Click(Sender: TObject);
    procedure cxButton9Click(Sender: TObject);
    procedure cxButton10Click(Sender: TObject);
    procedure cxButton11Click(Sender: TObject);
    procedure cxButton12Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;


type TReqv = record //ProcessOW
       handle:Integer;
       abg_id:Integer;
       operType:Integer; //[in] ��� �������� (��������)
       track2:pchar; //[in] ����2
       pan:pchar; //[out] PAN
       expiry:pchar; //[out] Expiry Date ����
       pay_acc:pchar; //�� ������������
       additional_payment_data:pchar; //�� ������������
       amount:pchar; //[in] ����� � ��������
       original_amount:pchar; //[in]������������ ����� � //��������
       currency:pchar; //[in] ��� ������
       terminalID:pchar; //[out][in] ID ���������
       rrn:pchar; //[in][out] ������ (��������� ������ ��� ��� �������� ��� ������� ��� �����, � ��������� ������� ������ ���� �����)
       authCode:pchar; //[out][in] ��� �����������
       responseCode:pchar; //[out] ��� ������
       cardType:pchar; //[out] �������� ���� �����
       date:pchar; //[out] ���� ����������
       time:pchar; //[out] ����� ����������
       payment_data:pchar; //�� ������������
       data_to_print:pchar; //�� ������������
       home_operator:pchar; //�� ������������
       received_text_message:pchar;//�� ������������
       text_message:pchar; //[out] �����������
       AID:pchar; //[out]EMV AID
       ApplicationLabel:pchar; //[out]EMV ApplicationLabel
       TVR:pchar; //[out]EMV TVR
       system_res:Integer; //�� ������������
       enc_data:pchar; // [in][out]����������� ������
     end;

function IsOLEObjectInstalled(Name: String): boolean;

function fVTB(iOp:INteger;rSum:Real):Boolean;

//Function ProcessOW(Reqv:TReqv):Integer; stdcall; far; external 'arccom.dll' name 'ProcessOW';

var
  fmMainSber: TfmMainSber;
  Drv,Req,Resp: OleVariant;
  vCh: Variant;
  iMoney:Integer;
  Reqv:TReqv;

implementation


{$R *.dfm}

function fVTB(iOp:INteger;rSum:Real):Boolean;
var
  SEInfo: TShellExecuteInfo;
  ExitCode: DWORD;
  ExecuteFile, ParamString, StartInString: string;
  iSum:INteger;
begin
  ExecuteFile := 'CommandLineTool.exe';
  iSum:=roundex(rSum*100);

  FillChar(SEInfo, SizeOf(SEInfo), 0);
  SEInfo.cbSize := SizeOf(TShellExecuteInfo);
  with SEInfo do
    beginfMask := SEE_MASK_NOCLOSEPROCESS;
  Wnd := Application.Handle;
  lpFile := PChar(ExecuteFile);
  {ParamString can contain theapplication parameters.}
  // lpParameters := PChar(ParamString);
  {StartInString specifies thename of the working
  directory.If ommited, the current
  directory is used.}
  // lpDirectory := PChar(StartInString);
  nShow := SW_SHOWNORMAL;

end;


function IsOLEObjectInstalled(Name: String): boolean;
var
  ClassID: TCLSID;
  Rez : HRESULT;
begin

// ���� CLSID OLE-�������
  Rez := CLSIDFromProgID(PWideChar(WideString(Name)), ClassID);
  if Rez = S_OK then
 // ������ ������
    Result := true
  else
    Result := false;
end;


procedure TfmMainSber.FormShow(Sender: TObject);
begin
  Memo1.Clear;
end;

procedure TfmMainSber.cxButton1Click(Sender: TObject);
Var tTime:TDateTime;
    StrT:String;
begin
//�������
  Memo1.Lines.Add('');
  Memo1.Lines.Add('��������� ����������.');
  tTime:=now;

  if IsOLEObjectInstalled('ArcCom.SAPacketObj') then
  begin
    tTime:=Now-tTime;
    StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
    Memo1.Lines.Add('������� ArcCom.SAPacketObj ����������.'+StrT);
  end else
  begin
    tTime:=Now-tTime;
    StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
    Memo1.Lines.Add('������� ArcCom.SAPacketObj �� ���������� !!!'+StrT);
    exit;
  end;

  tTime:=now;

  if IsOLEObjectInstalled('ArcCom.PCPOSTConnectorObj') then
  begin
    tTime:=Now-tTime;
    StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
    Memo1.Lines.Add('������� ArcCom.PCPOSTConnectorObj ����������.'+StrT);
  end else
  begin
    tTime:=Now-tTime;
    StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
    Memo1.Lines.Add('�������   ArcCom.PCPOSTConnectorObj  �� ���������� !!!'+StrT);
    exit;
  end;


  Memo1.Lines.Add('');
  Memo1.Lines.Add('������� ������ ��������.');
  tTime:=now;
  Drv := CreateOleObject('ArcCom.PCPOSTConnectorObj');
  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('�������� �������.'+StrT);

  Memo1.Lines.Add('');
  Memo1.Lines.Add('������� ������ �������.');
  tTime:=now;
  Req := CreateOleObject('ArcCom.SAPacketObj');
  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('�������� �������.'+StrT);

  Memo1.Lines.Add('');
  Memo1.Lines.Add('������� ������ ������.');
  tTime:=now;
  Resp := CreateOleObject('ArcCom.SAPacketObj');
  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('�������� �������.'+StrT);

end;

procedure TfmMainSber.cxButton8Click(Sender: TObject);
Var tTime:TDateTime;
    StrT:String;
begin
  Memo1.Lines.Add('');
  Memo1.Lines.Add('��������� ����������. (����)');
  tTime:=now;

{
  Drv.re
  Req.FreeResources;
  Resp.FreeResources;
}
  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('��������� ������. '+StrT);
end;

procedure TfmMainSber.cxButton2Click(Sender: TObject);
Var tTime:TDateTime;
    StrT:String;
    iRes:LongInt;
    StrWk:String;
begin
  Memo1.Lines.Add('');
  Memo1.Lines.Add(cxButton2.Caption);
  tTime:=now;
  iMoney:=cxCEdit1.EditValue;

  Req.Amount:='100';
  Req.CurrencyCode:='643';
  Req.OperationCode:=1; //�������

  Req.AmountFee:='0';
  Req.CardEntryMode:=0;
  Req.CardExpiryDate:='1401';
  Req.PAN:='4272290597422021';
  Req.TRACK2:='';

  Drv.Exchange(Req,Resp,10000);

//  Drv.Clear;
//  Drv.SParam('Amount',iMoney);
//  Drv.SParam('CardType',0);
//  iRes:=Drv.NFun(4000);
  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('����� : '+StrT);
  Memo1.Lines.Add('----��������� : '+IntToStr(iRes));

  StrWk:=Drv.GParam('ClientCard');
  Memo1.Lines.Add('----����� ������� : '+StrWk);

//  StrWk:=Drv.GParam('ClientExpiryDate');
//  Memo1.Lines.Add('----���� ��������� �������� : '+StrWk);
//  Memo1.Lines.Add('');
//  Memo1.Lines.Add('���������� ���');
//  vCh:=Drv.GParam('Cheque');
//  Memo1.Lines.Add('� ������');
//  StrWk:= vCh;
//  Memo1.Lines.Add('--------------------------------------');
//  if length(StrWk)>0 then
//  begin
//    Memo1.Lines.Add(StrWk);
//  end;
  Memo1.Lines.Add('--------------------------------------');
  Memo1.Lines.Add('');
  Memo1.Lines.Add('');
end;

procedure TfmMainSber.cxButton4Click(Sender: TObject);
Var tTime:TDateTime;
    StrT:String;
    iRes:Integer;
    StrWk:String;
begin
  Memo1.Lines.Add('');
  Memo1.Lines.Add('������� DLL');

  Reqv.operType:=1;
  Reqv.amount:='111'; //[in] ����� � ��������
  Reqv.original_amount:='111'; //[in]������������ ����� � //��������
  Reqv.currency:='643'; //[in] ��� ������

//  ProcessOW(Reqv);

  tTime:=now;

//  iRes:=Drv.NFun(4006);
  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('����� : '+StrT);

  Memo1.Lines.Add('');
end;

procedure TfmMainSber.cxButton3Click(Sender: TObject);
Var tTime:TDateTime;
    StrT:String;
    iRes:Integer;
    StrWk:String;
begin
  Memo1.Lines.Add('');
  Memo1.Lines.Add('������ ��������   1000');
  tTime:=now;

  Drv.Clear;
  Drv.SParam('Amount',cxCEdit1.EditValue);
//  Drv.SParam('CardType',0);
  iRes:=Drv.NFun(1000);
  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('����� : '+StrT);
  Memo1.Lines.Add('----��������� : '+IntToStr(iRes));

  StrWk:=Drv.GParam('ClientCard');
  Memo1.Lines.Add('----����� ������� : '+StrWk);

  StrWk:=Drv.GParam('ClientExpiryDate');
  Memo1.Lines.Add('----���� ��������� �������� : '+StrWk);
  Memo1.Lines.Add('');
  Memo1.Lines.Add('���������� ���');
  vCh:=Drv.GParam('Cheque');
  Memo1.Lines.Add('� ������');
  StrWk:= vCh;
  Memo1.Lines.Add('--------------------------------------');
  if length(StrWk)>0 then
  begin
    Memo1.Lines.Add(StrWk);
  end;
  Memo1.Lines.Add('--------------------------------------');
  Memo1.Lines.Add('');
  Memo1.Lines.Add('');
end;

procedure TfmMainSber.cxButton5Click(Sender: TObject);
Var tTime:TDateTime;
    StrT:String;
    iRes:Integer;
    StrWk:String;
begin
  Memo1.Lines.Add('');
  Memo1.Lines.Add('������� EXE');
  tTime:=now;

  try
    ShellExecute(handle, 'open', 'CommandLineTool.exe',' /o1 /a102','', SW_SHOWNORMAL);
  except
    showmessage('������ �� ��������. ���������� � ��������������.');
  end;



  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('����� : '+StrT);

 {
  Memo1.Lines.Add('----��������� : '+IntToStr(iRes));
  StrWk:=Drv.GParam('ClientCard');
  Memo1.Lines.Add('----����� ������� : '+StrWk);
  StrWk:=Drv.GParam('ClientExpiryDate');
  Memo1.Lines.Add('----���� ��������� �������� : '+StrWk);
  Memo1.Lines.Add('');
  Memo1.Lines.Add('���������� ���');
  vCh:=Drv.GParam('Cheque');
  Memo1.Lines.Add('� ������');
  StrWk:= vCh;
  Memo1.Lines.Add('--------------------------------------');
  if length(StrWk)>0 then
  begin
    Memo1.Lines.Add(StrWk);
  end;
  Memo1.Lines.Add('--------------------------------------');
  Memo1.Lines.Add('');
  Memo1.Lines.Add('');}
end;

procedure TfmMainSber.cxButton6Click(Sender: TObject);
Var tTime:TDateTime;
    StrT:String;
    iRes:Integer;
    StrWk:String;
begin
  Memo1.Lines.Add('');
  Memo1.Lines.Add('������ �������� �� ���   4003 ');
  tTime:=now;

  Drv.Clear;
  Drv.SParam('Amount',cxCEdit1.EditValue);
//  Drv.SParam('CardType',0);
  iRes:=Drv.NFun(4003);
  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('����� : '+StrT);
  Memo1.Lines.Add('----��������� : '+IntToStr(iRes));

  StrWk:=Drv.GParam('ClientCard');
  Memo1.Lines.Add('----����� ������� : '+StrWk);

  StrWk:=Drv.GParam('ClientExpiryDate');
  Memo1.Lines.Add('----���� ��������� �������� : '+StrWk);
  Memo1.Lines.Add('');
  Memo1.Lines.Add('���������� ���');
  vCh:=Drv.GParam('Cheque');
  Memo1.Lines.Add('� ������');
  StrWk:= vCh;
  Memo1.Lines.Add('--------------------------------------');
  if length(StrWk)>0 then
  begin
    Memo1.Lines.Add(StrWk);
  end;
  Memo1.Lines.Add('--------------------------------------');
  Memo1.Lines.Add('');
  Memo1.Lines.Add('');
end;

procedure TfmMainSber.cxButton7Click(Sender: TObject);
Var tTime:TDateTime;
    StrT:String;
    iRes:Integer;
    StrWk:String;
begin
  Memo1.Lines.Add('');
  Memo1.Lines.Add('������� �� ��������   1002 ');
  tTime:=now;

  Drv.Clear;
  Drv.SParam('Amount',cxCEdit1.EditValue);
//  Drv.SParam('CardType',0);
  iRes:=Drv.NFun(1002);
  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('����� : '+StrT);
  Memo1.Lines.Add('----��������� : '+IntToStr(iRes));

  StrWk:=Drv.GParam('ClientCard');
  Memo1.Lines.Add('----����� ������� : '+StrWk);

  StrWk:=Drv.GParam('ClientExpiryDate');
  Memo1.Lines.Add('----���� ��������� �������� : '+StrWk);
  Memo1.Lines.Add('');
  Memo1.Lines.Add('���������� ���');
  vCh:=Drv.GParam('Cheque');
  Memo1.Lines.Add('� ������');
  StrWk:= vCh;
  Memo1.Lines.Add('--------------------------------------');
  if length(StrWk)>0 then
  begin
    Memo1.Lines.Add(StrWk);
  end;
  Memo1.Lines.Add('--------------------------------------');
  Memo1.Lines.Add('');
  Memo1.Lines.Add('');
end;


procedure TfmMainSber.cxButton9Click(Sender: TObject);
Var tTime:TDateTime;
    StrT:String;
    iRes:Integer;
    StrWk:String;
begin
  Memo1.Lines.Add('');
  Memo1.Lines.Add('�������� ��� �� ��������� � ����������� ������� (6000) ');
  tTime:=now;

  Drv.Clear;
  iRes:=Drv.NFun(6000);
  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('����� : '+StrT);
  Memo1.Lines.Add('----��������� : '+IntToStr(iRes));
  Memo1.Lines.Add('');
  Memo1.Lines.Add('���������� ���');
  vCh:=Drv.GParam('Cheque');
  Memo1.Lines.Add('� ������');
  StrWk:= vCh;
  Memo1.Lines.Add('--------------------------------------');
  if length(StrWk)>0 then
  begin
    Memo1.Lines.Add(StrWk);
  end;
  Memo1.Lines.Add('--------------------------------------');
  Memo1.Lines.Add('');
  Memo1.Lines.Add('');
end;


procedure TfmMainSber.cxButton10Click(Sender: TObject);
Var tTime:TDateTime;
    StrT:String;
    iRes:Integer;
    StrWk:String;
begin
  Memo1.Lines.Add('');
  Memo1.Lines.Add('���������� ��������   3001 ');
  tTime:=now;

  Drv.Clear;
  iRes:=Drv.NFun(3001);
  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('����� : '+StrT);
  Memo1.Lines.Add('----��������� : '+IntToStr(iRes));
  Memo1.Lines.Add('');
  Memo1.Lines.Add('���������� ���');
  vCh:=Drv.GParam('Cheque');
  Memo1.Lines.Add('� ������');
  StrWk:= vCh;
  Memo1.Lines.Add('--------------------------------------');
  if length(StrWk)>0 then
  begin
    Memo1.Lines.Add(StrWk);
  end;
  Memo1.Lines.Add('--------------------------------------');
  Memo1.Lines.Add('');
  Memo1.Lines.Add('');
end;


procedure TfmMainSber.cxButton11Click(Sender: TObject);
Var tTime:TDateTime;
    StrT:String;
    iRes:Integer;
    StrWk:String;
begin
  Memo1.Lines.Add('');
  Memo1.Lines.Add('������� �����    7000 ');
  tTime:=now;

  Drv.Clear;
  iRes:=Drv.NFun(7000);
  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('����� : '+StrT);
  Memo1.Lines.Add('----��������� : '+IntToStr(iRes));
  Memo1.Lines.Add('');
  Memo1.Lines.Add('���������� ���');
  vCh:=Drv.GParam('Cheque');
  Memo1.Lines.Add('� ������');
  StrWk:= vCh;
  Memo1.Lines.Add('--------------------------------------');
  if length(StrWk)>0 then
  begin
    Memo1.Lines.Add(StrWk);
  end;
  Memo1.Lines.Add('--------------------------------------');
  Memo1.Lines.Add('');
  Memo1.Lines.Add('');
end;


procedure TfmMainSber.cxButton12Click(Sender: TObject);
Var tTime:TDateTime;
    StrT:String;
    iRes:Integer;
    StrWk:String;
begin
  Memo1.Lines.Add('');
  Memo1.Lines.Add('������ ����. ���������  7001 ');
  tTime:=now;

  Drv.Clear;
  iRes:=Drv.NFun(7001);
  tTime:=Now-tTime;
  StrT:=' ('+FormatDateTime('s ���. zzz ����',tTime)+') ';
  Memo1.Lines.Add('����� : '+StrT);
  Memo1.Lines.Add('----��������� : '+IntToStr(iRes));
  Memo1.Lines.Add('');
  Memo1.Lines.Add('���������� ���');
  vCh:=Drv.GParam('Cheque');
  Memo1.Lines.Add('� ������');
  StrWk:= vCh;
  Memo1.Lines.Add('--------------------------------------');
  if length(StrWk)>0 then
  begin
    Memo1.Lines.Add(StrWk);
  end;
  Memo1.Lines.Add('--------------------------------------');
  Memo1.Lines.Add('');
  Memo1.Lines.Add('');
end;

end.
