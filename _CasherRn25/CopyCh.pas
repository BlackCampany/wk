unit CopyCh;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxMemo, cxControls, cxContainer, cxEdit, cxTextEdit, cxMaskEdit,
  cxDropDownEdit, cxCalc, Menus, cxLookAndFeelPainters, StdCtrls,
  cxButtons, ExtCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter,
  cxData, cxDataStorage, DB, cxDBData, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, FIBDataSet, pFIBDataSet, FIBDatabase,
  pFIBDatabase;

type
  TfmCopyCh = class(TForm)
    Memo1: TcxMemo;
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    Label1: TLabel;
    cxButton4: TcxButton;
    cxButton5: TcxButton;
    ViCopy: TcxGridDBTableView;
    LeCopy: TcxGridLevel;
    GrCopy: TcxGrid;
    cxTextEdit1: TcxTextEdit;
    cxButton6: TcxButton;
    cxButton7: TcxButton;
    trSelCh: TpFIBTransaction;
    quCh: TpFIBDataSet;
    quChID_TAB: TFIBIntegerField;
    quChID: TFIBIntegerField;
    quChID_PERSONAL: TFIBIntegerField;
    quChNUMTABLE: TFIBStringField;
    quChSIFR: TFIBIntegerField;
    quChPRICE: TFIBFloatField;
    quChQUANTITY: TFIBFloatField;
    quChDISCOUNTPROC: TFIBFloatField;
    quChDISCOUNTSUM: TFIBFloatField;
    quChSUMMA: TFIBFloatField;
    quChISTATUS: TFIBIntegerField;
    quChITYPE: TFIBSmallIntField;
    quChID_PERSONAL1: TFIBIntegerField;
    quChTABSUM: TFIBFloatField;
    quChBEGTIME: TFIBDateTimeField;
    quChENDTIME: TFIBDateTimeField;
    quChDISCONT: TFIBStringField;
    quChOPERTYPE: TFIBStringField;
    quChCHECKNUM: TFIBIntegerField;
    dsquCh: TDataSource;
    ViCopyID: TcxGridDBColumn;
    ViCopySIFR: TcxGridDBColumn;
    ViCopyPRICE: TcxGridDBColumn;
    ViCopyQUANTITY: TcxGridDBColumn;
    ViCopyDISCOUNTPROC: TcxGridDBColumn;
    ViCopyDISCOUNTSUM: TcxGridDBColumn;
    ViCopySUMMA: TcxGridDBColumn;
    ViCopyITYPE: TcxGridDBColumn;
    quFCh: TpFIBDataSet;
    quFChID: TFIBIntegerField;
    quFChCASHNUM: TFIBIntegerField;
    quFChZNUM: TFIBIntegerField;
    quFChCHECKNUM: TFIBIntegerField;
    quFChTAB_ID: TFIBIntegerField;
    quFChTABSUM: TFIBFloatField;
    quFChCLIENTSUM: TFIBFloatField;
    quFChCASHERID: TFIBIntegerField;
    quFChWAITERID: TFIBIntegerField;
    quFChCHDATE: TFIBDateTimeField;
    quFChPAYTYPE: TFIBSmallIntField;
    quFChPAYID: TFIBIntegerField;
    quFChPAYBAR: TFIBStringField;
    quFChNAME: TFIBStringField;
    quChNAMECARD: TFIBStringField;
    ViCopyNAMECARD: TcxGridDBColumn;
    procedure cxButton3Click(Sender: TObject);
    procedure cxCalcEdit1PropertiesCloseUp(Sender: TObject);
    procedure cxCalcEdit1PropertiesInitPopup(Sender: TObject);
    procedure cxCalcEdit1PropertiesPopup(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxButton7Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

Procedure TestFp(StrOp:String);

var
  fmCopyCh: TfmCopyCh;

implementation

uses Calc, Un1, UnCash, Attention, Dm, UnPrizma;

{$R *.dfm}

Procedure TestFp(StrOp:String);
begin
  while TestStatus(StrOp,sMessage)=False do
  begin
    fmAttention.Label1.Caption:=sMessage;
    fmAttention.ShowModal;
    Nums.iRet:=InspectSt(Nums.sRet);
    prWriteLog('~~AttentionShow;'+sMessage+';'+'InspectSt(Nums.sRet)='+IntToStr(Nums.iRet));
  end;
end;


procedure TfmCopyCh.cxButton3Click(Sender: TObject);
begin
  Close;
end;

procedure TfmCopyCh.cxCalcEdit1PropertiesCloseUp(Sender: TObject);
begin
  Memo1.Lines.Add('CloseUp');
end;

procedure TfmCopyCh.cxCalcEdit1PropertiesInitPopup(Sender: TObject);
begin
  Memo1.Lines.Add('InitPopup');
end;

procedure TfmCopyCh.cxCalcEdit1PropertiesPopup(Sender: TObject);
begin
  Memo1.Lines.Add('Popup');
end;

procedure TfmCopyCh.FormShow(Sender: TObject);
begin
 Memo1.Clear;
end;

procedure TfmCopyCh.cxButton7Click(Sender: TObject);
begin
  fmCalc.CalcEdit1.Text:=fmCopyCh.cxTextEdit1.Text;
  fmCalc.ShowModal;
  if fmCalc.ModalResult=mrOk then cxTextEdit1.Text:=fmCalc.CalcEdit1.Text;
end;

procedure TfmCopyCh.cxButton4Click(Sender: TObject);
Var ich,iz:INteger;
    StrWk,StrZ,StrCh:String;
begin
  Memo1.Clear;
  StrWk:=cxTextEdit1.Text;
  if pos(',',StrWk)>0 then
  begin
    StrZ:=Copy(StrWk,1,pos(',',StrWk)-1);
    StrCh:=StrWk;
    while pos(',',StrCh)>0 do delete(StrCh,1,1);
    iZ:=StrToIntDef(StrZ,Nums.ZNum);
    iCh:=StrToIntDef(StrCh,Nums.iCheckNum);
  end else
  begin
    iZ:=Nums.ZNum;
    iCh:=StrToIntDef(StrWk,Nums.iCheckNum);
  end;

  quFCh.Active:=False;
  quFCh.ParamByName('ZNUM').AsInteger:=iZ;
  quFCh.ParamByName('CHNUM').AsInteger:=iCh;
  quFCh.Active:=True;
  quFCh.First;
  if quFCh.RecordCount>0 then
  begin
    quCh.Active:=False;
    quCh.ParamByName('IDH').AsInteger:=quFChTAB_ID.AsInteger;
    quCh.Active:=True;

    Memo1.Lines.Add('��� '+its(iZ)+','+its(iCh)+' �����: '+fts(quChTABSUM.AsFloat)+FormatDateTime(' dd.mm.yyyy hh:nn:ss',quChENDTIME.AsDateTime));
  end else Memo1.Lines.Add('��� '+its(iZ)+','+its(iCh)+' �� ������.');
//  quFCh.Active:=False;
end;

procedure TfmCopyCh.cxButton6Click(Sender: TObject);
Var ich,iz:INteger;
    StrWk,StrZ,StrCh:String;
begin         //�� ���� ������
  Memo1.Clear;
  StrWk:=cxTextEdit1.Text;
  if pos(',',StrWk)>0 then
  begin
    StrZ:=Copy(StrWk,1,pos(',',StrWk)-1);
    StrCh:=StrWk;
    while pos(',',StrCh)>0 do delete(StrCh,1,1);
    iZ:=StrToIntDef(StrZ,Nums.ZNum);
    iCh:=StrToIntDef(StrCh,Nums.iCheckNum);
  end else
  begin
    iZ:=Nums.ZNum;
    iCh:=StrToIntDef(StrWk,Nums.iCheckNum);
  end;

  inc(iCh);

  quFCh.Active:=False;
  quFCh.ParamByName('ZNUM').AsInteger:=iZ;
  quFCh.ParamByName('CHNUM').AsInteger:=iCh;
  quFCh.Active:=True;
  quFCh.First;
  if quFCh.RecordCount>0 then
  begin
    quCh.Active:=False;
    quCh.ParamByName('IDH').AsInteger:=quFChTAB_ID.AsInteger;
    quCh.Active:=True;

    cxTextEdit1.Text:=its(iZ)+','+its(iCh);

    Memo1.Lines.Add('��� '+its(iZ)+','+its(iCh)+' �����: '+fts(quChTABSUM.AsFloat)+FormatDateTime(' dd.mm.yyyy hh:nn:ss',quChENDTIME.AsDateTime));
  end else Memo1.Lines.Add('��� '+its(iZ)+','+its(iCh)+' �� ������.');
//  quFCh.Active:=False;
end;

procedure TfmCopyCh.cxButton5Click(Sender: TObject);
Var ich,iz:INteger;
    StrWk,StrZ,StrCh:String;
begin         //�� ���� �����
  Memo1.Clear;
  StrWk:=cxTextEdit1.Text;
  if pos(',',StrWk)>0 then
  begin
    StrZ:=Copy(StrWk,1,pos(',',StrWk)-1);
    StrCh:=StrWk;
    while pos(',',StrCh)>0 do delete(StrCh,1,1);
    iZ:=StrToIntDef(StrZ,Nums.ZNum);
    iCh:=StrToIntDef(StrCh,Nums.iCheckNum);
  end else
  begin
    iZ:=Nums.ZNum;
    iCh:=StrToIntDef(StrWk,Nums.iCheckNum);
  end;

  dec(iCh);
  if iCh<0 then iCh:=0;

  if iCh >0 then
  begin
  quFCh.Active:=False;
  quFCh.ParamByName('ZNUM').AsInteger:=iZ;
  quFCh.ParamByName('CHNUM').AsInteger:=iCh;
  quFCh.Active:=True;
  quFCh.First;
  if quFCh.RecordCount>0 then
  begin
    quCh.Active:=False;
    quCh.ParamByName('IDH').AsInteger:=quFChTAB_ID.AsInteger;
    quCh.Active:=True;

    cxTextEdit1.Text:=its(iZ)+','+its(iCh);

    Memo1.Lines.Add('��� '+its(iZ)+','+its(iCh)+' �����: '+fts(quChTABSUM.AsFloat)+FormatDateTime(' dd.mm.yyyy hh:nn:ss',quChENDTIME.AsDateTime));
  end else Memo1.Lines.Add('��� '+its(iZ)+','+its(iCh)+' �� ������.');
//  quFCh.Active:=False;
  end else Memo1.Lines.Add('��� '+its(iZ)+','+its(iCh)+' �� ������.');
end;

procedure TfmCopyCh.cxButton1Click(Sender: TObject);
Var iNumZ,iNumCh:Integer;
    CHECKDATE:TDateTime;
    CHECKSUM:Real;
    CHECKPAY:Real;
    CHECKDSUM:Real;
    OPERATION:Integer;
    StrWk,StrWk1,StrZ,StrCh:String;
    bNotErr:Boolean;
    sCasher,sOper:String;
begin
  //����� ����

  //�������� ������� �����

  Nums.iRet:=InspectSt(Nums.sRet);

  if TestStatus('InspectSt',sMessage)=False then
  begin
//          ShowMessage('������: "'+Nums.sRet+'". ��������� �������� ����� ���������� ������.');
    fmAttention.Label1.Caption:=sMessage;
    fmAttention.ShowModal;
    exit;// ������ ��� - ������� ��� ������������ ����
  end;

  Memo1.Clear;
  StrWk:=cxTextEdit1.Text;
  if pos(',',StrWk)>0 then
  begin
    StrZ:=Copy(StrWk,1,pos(',',StrWk)-1);
    StrCh:=StrWk;
    while pos(',',StrCh)>0 do delete(StrCh,1,1);
    iNumZ:=StrToIntDef(StrZ,Nums.ZNum);
    iNumCh:=StrToIntDef(StrCh,Nums.iCheckNum);
  end else
  begin
    iNumZ:=Nums.ZNum;
    iNumCh:=StrToIntDef(StrWk,Nums.iCheckNum);
  end;

  if Prizma then
  begin
    prWriteLog('!!PrintCopyCH');
    Event_RegEx(58,iNumCh,0,0,'','',0,0,0,0,0,0);
  end;

  prWriteLog('!!CopyCheck; CashNum '+INtToStr(CommonSet.CashNum)+' CashZ '+INtToStr(iNumZ)+' CheckNum '+INtToStr(iNumCh));
  Memo1.Clear;
  Memo1.Lines.Add('������ ����� ���� (�����:'+INtToStr(iNumZ)+', ���:'+INtToStr(iNumCh)+').');

  if (iNumZ>0)and(iNumCh>0) then
  begin
    ViCopy.BeginUpdate;

    with dmC do
    begin
      CHECKDATE:=quChENDTIME.AsDateTime;
      CHECKDSUM:=0;

      quCh.First;
      while not quCh.Eof do
      begin
        CHECKDSUM:=CHECKDSUM+quChDISCOUNTSUM.AsFloat;
        quCh.Next;
      end;
      quCh.First;

      CHECKSUM:=quFChTABSUM.AsFloat;
      CHECKPAY:=quFChCLIENTSUM.AsFloat;
      sCasher:=quFChNAME.AsString;
      sOper:=quChOPERTYPE.AsString;

      OPERATION:=1;
      if sOper='Ret' then OPERATION:=0;
      if sOper='RetBank' then OPERATION:=4;
      if sOper='Sale' then OPERATION:=1;
      if sOper='SaleBank' then OPERATION:=5;


     //������ ������������� ���������

      bNotErr:=True;
      if OpenNFDoc=false then
      begin
//        exit;
        TestFp('PrintNFStr');
        OpenNFDoc;
      end;

      SelectF(10);
      if bNotErr then bNotErr:=PrintNFStr('����� ���� �'+IntToStr(iNumCh)) else TestFp('PrintNFStr');
      StrWk:=CommonSet.DepartName;
      while length(strwk)<35 do StrWk:=' '+StrWk;
      if bNotErr then bNotErr:=PrintNFStr('�����'+StrWk)else TestFp('PrintNFStr');
      if bNotErr then bNotErr:=PrintNFStr('      ')else TestFp('PrintNFStr');
      if bNotErr then bNotErr:=PrintNFStr('����� � '+INtToStr(CommonSet.CashNum)+'  ���.� '+Nums.SerNum) else TestFp('PrintNFStr');
//      if bNotErr then bNotErr:=PrintNFStr('�����: '+INtToStr(iNumZ)+'            ������: '+IntToStr(CASHER));
      if bNotErr then bNotErr:=PrintNFStr('�����: '+INtToStr(iNumZ)+'            ������: '+sCasher) else TestFp('PrintNFStr');
      if bNotErr then bNotErr:=PrintNFStr('����:   '+FormatDateTime('dd.mm.yyyy hh:nn',CHECKDATE)) else TestFp('PrintNFStr');

      SelectF(3);
      if CommonSet.TypeFis='prim' then
      begin
        StrWk:='                                                       ';
        if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
        SelectF(10);
        StrWk:=' ��������          ���-��   ����   ����� ';
        if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
        SelectF(3);
        StrWk:='                                                       ';
        if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
      end;

      if (CommonSet.TypeFis='sp101')or(CommonSet.TypeFis='shtrih') then
      begin
        StrWk:='----------------------------------------';
        if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
        StrWk:=' ��������          ���-��   ����   ����� ';
        if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
        StrWk:='----------------------------------------';
        if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
      end;

      SelectF(15);
      StrWk:='';
      if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

      Case Operation of
      0: StrWk:='  �������� - ������� ��������         ';
      1: StrWk:='  �������� - ������� ��������         ';
      5: StrWk:='  �������� - ������� ��               ';
      4: StrWk:='  �������� - ������� ��               ';
      end;

      SelectF(15);
      if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

      SelectF(3);
      StrWk:='                                                     ';
      if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');


      quCh.First;
      while not quCh.Eof do
      begin
        StrWk:= Copy((quChSIFR.AsString+' '+quChNAMECARD.AsString),1,40);
//        while Length(StrWk)<20 do StrWk:=StrWk+' '; //21
        SelectF(5);
        if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

        Str(quChPRICE.AsFloat:7:2,StrWk1); StrWk1:=DelSp(StrWk1);
        StrWk:=StrWk1; //

        Str(quChQUANTITY.AsFloat:6:3,StrWk1); StrWk1:=DelSp(StrWk1);
        StrWk:=StrWk+' X '+StrWk1; //


        while Length(StrWk)<30 do StrWk:=StrWk+' ';

//        Str(quChRSUM.AsFloat:9:2,StrWk1);
        Str((quChPRICE.AsFloat*quChQUANTITY.AsFloat):9:2,StrWk1);

        StrWk:=StrWk+' '+StrWk1+'�'; //
        SelectF(10);
        if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
        quCh.Next;
      end;

      SelectF(3);
      StrWk:='                                                     ';
      if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

      SelectF(10);
      Str((CHECKSUM+CHECKDSUM):8:2,StrWk1);  //CHECKSUM - ��� ��� ����� ������
      StrWk:=' �����                      '+StrWk1+' ���';
      if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

      if CHECKDSUM<>0 then
      begin
        SelectF(10);
        Str(CHECKDSUM:5:2,StrWk1);
        StrWk:='C����� - '+StrWk1+'���';
        if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

        SelectF(10);
//        Str((CHECKSUM-CHECKDSUM):8:2,StrWk1);
        Str(CHECKSUM:8:2,StrWk1); //CHECKSUM - ��� ��� ����� ������
        StrWk:=' �����                      '+StrWk1+' ���';
        if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
      end;

      SelectF(10);
      Str(CHECKPAY:8:2,StrWk1);
      StrWk:=' �������� �� ����������     '+StrWk1+' ���';
      if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

      SelectF(10);
      Str((CHECKPAY-CHECKSUM):8:2,StrWk1);   //CHECKSUM - ��� ��� ����� ������
      StrWk:=' �����                      '+StrWk1+' ���';
      if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

      StrWk:='';
      PrintNFStr(StrWk);
      StrWk:=' ��������!  ������������ ��������.';
      if bNotErr then PrintNFStr(StrWk) else TestFp('PrintNFStr');

      if CloseNFDoc=False then
      begin
        TestFp('PrintNFStr');
        CloseNFDoc;
      end;

      if (CommonSet.TypeFis='prim')or(CommonSet.TypeFis='shtrih') then
      begin
        PrintNFStr(' ');
        PrintNFStr(' ');
        PrintNFStr(' ');
        PrintNFStr(' ');
        PrintNFStr(' ');
        PrintNFStr(' ');
        CutDoc;
      end;

      delay(33);

      ViCopy.EndUpdate;
    end;
  end;
end;

procedure TfmCopyCh.cxButton2Click(Sender: TObject);
Var iNumZ,iNumCh:Integer;
    CHECKDATE:TDateTime;
    CHECKSUM:Real;
    CHECKDSUM:Real;
    OPERATION:Integer;
    StrWk,StrWk1,StrZ,StrCh:String;
    bNotErr:Boolean;
    sCasher,sOper:String;
begin
  //����� ����

  //�������� ������� �����

  Nums.iRet:=InspectSt(Nums.sRet);

  if TestStatus('InspectSt',sMessage)=False then
  begin
//          ShowMessage('������: "'+Nums.sRet+'". ��������� �������� ����� ���������� ������.');
    fmAttention.Label1.Caption:=sMessage;
    fmAttention.ShowModal;
    exit;// ������ ��� - ������� ��� ������������ ����
  end;

  Memo1.Clear;
  StrWk:=cxTextEdit1.Text;
  if pos(',',StrWk)>0 then
  begin
    StrZ:=Copy(StrWk,1,pos(',',StrWk)-1);
    StrCh:=StrWk;
    while pos(',',StrCh)>0 do delete(StrCh,1,1);
    iNumZ:=StrToIntDef(StrZ,Nums.ZNum);
    iNumCh:=StrToIntDef(StrCh,Nums.iCheckNum);
  end else
  begin
    iNumZ:=Nums.ZNum;
    iNumCh:=StrToIntDef(StrWk,Nums.iCheckNum);
  end;

  if Prizma then
  begin
    prWriteLog('!!PrintTCH');
    Event_RegEx(51,iNumCh,0,0,'','',0,0,0,0,0,0);
  end;

  prWriteLog('!!CopyTCheck; CashNum '+INtToStr(CommonSet.CashNum)+' CashZ '+INtToStr(iNumZ)+' CheckNum '+INtToStr(iNumCh));
  Memo1.Clear;
  Memo1.Lines.Add('������ ��������� ���� (�����:'+INtToStr(iNumZ)+', ���:'+INtToStr(iNumCh)+').');

  if (iNumZ>0)and(iNumCh>0) then
  begin
    ViCopy.BeginUpdate;

    with dmC do
    begin
      CHECKDATE:=quChENDTIME.AsDateTime;
      CHECKDSUM:=0;

      quCh.First;
      while not quCh.Eof do
      begin
        CHECKDSUM:=CHECKDSUM+quChDISCOUNTSUM.AsFloat;
        quCh.Next;
      end;
      quCh.First;

      CHECKSUM:=quFChTABSUM.AsFloat;
      sCasher:=quFChNAME.AsString;
      sOper:=quChOPERTYPE.AsString;

      OPERATION:=1;
      if sOper='Ret' then OPERATION:=0;
      if sOper='RetBank' then OPERATION:=4;
      if sOper='Sale' then OPERATION:=1;
      if sOper='SaleBank' then OPERATION:=5;


     //������ ������������� ���������

      bNotErr:=True;
      if OpenNFDoc=false then
      begin
        TestFp('PrintNFStr');
        OpenNFDoc;
      end;

      SelectF(10);
//      if bNotErr then bNotErr:=PrintNFStr('����� ���� �'+IntToStr(iNumCh)) else TestFp('PrintNFStr');

      if bNotErr then bNotErr:=PrintNFStr('�������� ��� �'+IntToStr(iNumCh)) else TestFp('PrintNFStr');

      StrWk:=CommonSet.DepartName;
      while length(strwk)<35 do StrWk:=' '+StrWk;
      if bNotErr then bNotErr:=PrintNFStr('�����'+StrWk)else TestFp('PrintNFStr');
      if bNotErr then bNotErr:=PrintNFStr('      ')else TestFp('PrintNFStr');
      if bNotErr then bNotErr:=PrintNFStr('����� � '+INtToStr(CommonSet.CashNum)+'  ���.� '+Nums.SerNum) else TestFp('PrintNFStr');
//      if bNotErr then bNotErr:=PrintNFStr('�����: '+INtToStr(iNumZ)+'            ������: '+IntToStr(CASHER));
      if bNotErr then bNotErr:=PrintNFStr('�����: '+INtToStr(iNumZ)+'            ������: '+sCasher) else TestFp('PrintNFStr');
      if bNotErr then bNotErr:=PrintNFStr('����:   '+FormatDateTime('dd.mm.yyyy hh:nn',CHECKDATE)) else TestFp('PrintNFStr');

      SelectF(3);
      if CommonSet.TypeFis='prim' then
      begin
        StrWk:='                                                       ';
        if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
        SelectF(10);
        StrWk:=' ��������          ���-��   ����   ����� ';
        if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
        SelectF(3);
        StrWk:='                                                       ';
        if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
      end;

      if (CommonSet.TypeFis='sp101')or(CommonSet.TypeFis='shtrih') then
      begin
        StrWk:='----------------------------------------';
        if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
        StrWk:=' ��������          ���-��   ����   ����� ';
        if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
        StrWk:='----------------------------------------';
        if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
      end;

      SelectF(15);
      StrWk:='';
      if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

      Case Operation of
      0: StrWk:='  �������� - ������� ��������         ';
      1: StrWk:='  �������� - ������� ��������         ';
      5: StrWk:='  �������� - ������� ��               ';
      4: StrWk:='  �������� - ������� ��               ';
      end;

      SelectF(15);
      if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

      SelectF(3);
      StrWk:='                                                     ';
      if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');


      quCh.First;
      while not quCh.Eof do
      begin
        StrWk:= Copy((quChSIFR.AsString+' '+quChNAMECARD.AsString),1,40);
//        while Length(StrWk)<20 do StrWk:=StrWk+' '; //21
        SelectF(5);
        if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

        Str(quChPRICE.AsFloat:7:2,StrWk1); StrWk1:=DelSp(StrWk1);
        StrWk:=StrWk1; //

        Str(quChQUANTITY.AsFloat:6:3,StrWk1); StrWk1:=DelSp(StrWk1);
        StrWk:=StrWk+' X '+StrWk1; //


        while Length(StrWk)<30 do StrWk:=StrWk+' ';

//        Str(quChRSUM.AsFloat:9:2,StrWk1);
        Str((quChPRICE.AsFloat*quChQUANTITY.AsFloat):9:2,StrWk1);

        StrWk:=StrWk+' '+StrWk1+'�'; //
        SelectF(10);
        if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
        quCh.Next;
      end;

      SelectF(3);
      StrWk:='                                                     ';
      if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

      SelectF(10);
      Str((CHECKSUM+CHECKDSUM):8:2,StrWk1);  //CHECKSUM - ��� ��� ����� ������
      StrWk:=' �����                      '+StrWk1+' ���';
      if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

      if CHECKDSUM<>0 then
      begin
        SelectF(10);
        Str(CHECKDSUM:5:2,StrWk1);
        StrWk:='C����� - '+StrWk1+'���';
        if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');

        SelectF(10);
//        Str((CHECKSUM-CHECKDSUM):8:2,StrWk1);
        Str(CHECKSUM:8:2,StrWk1); //CHECKSUM - ��� ��� ����� ������
        StrWk:=' �����                      '+StrWk1+' ���';
        if bNotErr then bNotErr:=PrintNFStr(StrWk) else TestFp('PrintNFStr');
      end;

      StrWk:='';
      PrintNFStr(StrWk);
      StrWk:=' ��������!  ������������ ��������.';
      if bNotErr then PrintNFStr(StrWk) else TestFp('PrintNFStr');

      if CloseNFDoc=False then
      begin
        TestFp('PrintNFStr');
        CloseNFDoc;
      end;

      if (CommonSet.TypeFis='prim')or(CommonSet.TypeFis='shtrih') then
      begin
        PrintNFStr(' ');
        PrintNFStr(' ');
        PrintNFStr(' ');
        PrintNFStr(' ');
        PrintNFStr(' ');
        PrintNFStr(' ');
        CutDoc;
      end;

      delay(33);

      ViCopy.EndUpdate;
    end;
  end;
end;

end.
