unit RecalcReal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, cxMemo, StdCtrls, cxButtons,
  cxControls, cxContainer, cxEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit,
  cxCalendar, ComCtrls, FIBQuery, pFIBQuery, pFIBStoredProc, FIBDatabase,
  pFIBDatabase, DB, FIBDataSet, pFIBDataSet, cxGraphics, cxLookupEdit,
  cxDBLookupEdit, cxDBLookupComboBox;

type
  TfmRecalcPer1 = class(TForm)
    StatusBar1: TStatusBar;
    Label1: TLabel;
    cxDateEdit1: TcxDateEdit;
    cxButton1: TcxButton;
    Memo1: TcxMemo;
    cxButton2: TcxButton;
    prDelPer1: TpFIBStoredProc;
    trS: TpFIBTransaction;
    trD: TpFIBTransaction;
    trU: TpFIBTransaction;
    prTestPart: TpFIBStoredProc;
    prSetSpecActive: TpFIBStoredProc;
    quDR: TpFIBDataSet;
    quDRID: TFIBIntegerField;
    quDRDATEDOC: TFIBDateField;
    quDRNUMDOC: TFIBStringField;
    quDRDATESF: TFIBDateField;
    quDRNUMSF: TFIBStringField;
    quDRIDCLI: TFIBIntegerField;
    quDRIDSKL: TFIBIntegerField;
    quDRSUMIN: TFIBFloatField;
    quDRSUMUCH: TFIBFloatField;
    quDRSUMTAR: TFIBFloatField;
    quDRSUMNDS0: TFIBFloatField;
    quDRSUMNDS1: TFIBFloatField;
    quDRSUMNDS2: TFIBFloatField;
    quDRPROCNAC: TFIBFloatField;
    quDRIACTIVE: TFIBIntegerField;
    Label2: TLabel;
    cxDateEdit2: TcxDateEdit;
    Label3: TLabel;
    prRECALCGDS: TpFIBStoredProc;
    Label4: TLabel;
    cxLookupComboBox2: TcxLookupComboBox;
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure prWr(StrWk:String);
  end;

var
  fmRecalcPer1: TfmRecalcPer1;

implementation

uses Un1, dmOffice, DMOReps, DocCompl, AddCompl, ActPer, DocsVn, DocsOut,
  DocOutB, DOBSpec, DocInv, DocOutR;

{$R *.dfm}

procedure TfmRecalcPer1.prWr(StrWk:String);
Var StrWk1:String;
begin
  StrWk1:=FormatDateTime('mm.dd hh:nn:sss',now)+' '+StrWk;
  Memo1.Lines.Add(StrWk1);
end;


procedure TfmRecalcPer1.cxButton1Click(Sender: TObject);
Var iDateB,iDateE,iDateCur:Integer;
    iE:INteger;
    rSum1,rSum2:Real;
    iSkl:INteger;
begin
  //�������� �������
  Memo1.Clear;
  iDateB:=Trunc(cxDateEdit1.Date);
  iDateE:=Trunc(cxDateEdit2.Date);
  iSkl:=cxLookupComboBox2.EditValue;

  if not CanEdit(iDateB,iSkl) then begin prWr('������ ������.'); exit; end;

  prWr('�������� ��������.');

  prWr('  ������ ������.');
  prDelPer1.ParamByName('IDATEB').AsInteger:=iDateB;
  prDelPer1.ParamByName('IDATEE').AsInteger:=iDateE+1;
  prDelPer1.ParamByName('ISKL').AsInteger:=iSkl;
  prDelPer1.ExecProc;
  prWr('  --- ������� ��.'); delay(10);

//  prWr('  ���� ����������� �����.');
//  prTestPart.ExecProc;
//  iE:=prTestPart.ParamByName('RESULT').AsInteger;
//  prWr('  --- ������� ��. ('+INtToStr(iE)+')'); delay(10);

  prWr('  ����������� ������ ����������.(2)');
  prSetSpecActive.ParamByName('IDATEB').AsDate:=iDateB;
  prSetSpecActive.ParamByName('IDATEE').AsDate:=iDateE+1;
  prSetSpecActive.ParamByName('ATYPE').AsInteger:=2;
  prSetSpecActive.ParamByName('ISKL').AsInteger:=iSkl;
  prSetSpecActive.ExecProc;
  prWr('  --- ������� ��. '); delay(10);

  with dmO do
  with dmORep do
  begin
    for iDateCur:=iDateB to iDateE do
    begin
      prWr('    ��������� - '+FormatDateTime('dd.mm.yyyy',iDateCur)+' ('+IntToStr(iDateCur)+')');

      prWr('      ���������� �� �������.');

      iE:=0;

      quDR.Active:=False;
      quDR.ParamByName('DDATE').AsDate:=iDateCur;
      quDR.ParamByName('ISKL').AsInteger:=iSkl;
      quDR.Active:=True;
      quDR.First;
      while not quDR.Eof do
      begin
        fmDocsReal.prOn(quDRID.AsInteger,quDRIDSKL.AsInteger,iDateCur,quDRIDCLI.AsInteger,rSum1,rSum2);

        quDR.Edit;
        quDRIACTIVE.AsInteger:=1;
        quDRSUMIN.AsFloat:=RoundVal(rSum1);
        quDRSUMTAR.AsFloat:=RoundVal(rSum2);
        quDR.Post;

        quDR.Next; delay(10);inc(iE);
      end;
      quDR.Active:=False;
      prWr('      --- ������� ��. ('+INtToStr(iE)+')'); delay(10);

      prWr('      �������� ��������������.');
      prRECALCGDS.ParamByName('DDATE').AsDate:=iDateCur;
      prRECALCGDS.ParamByName('IDATE').AsInteger:=iDateCur;
      prRECALCGDS.ExecProc;
      prWr('      --- ������� ��. '); delay(10);
    end;

    quDocsRSel.FullRefresh;

    prWr('  ����������� ������ ����������.(1)');
    prSetSpecActive.ParamByName('IDATEB').AsDate:=iDateB;
    prSetSpecActive.ParamByName('IDATEE').AsDate:=iDateE+1;
    prSetSpecActive.ParamByName('ATYPE').AsInteger:=1;
    prSetSpecActive.ExecProc;
    prWr('  --- ������� ��. '); delay(10);

    prWr('�������� ��������.');
  end;
end;

procedure TfmRecalcPer1.cxButton2Click(Sender: TObject);
begin
  close;
end;

end.
