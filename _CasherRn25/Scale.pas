unit Scale;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, cxGraphics, Menus, cxLookAndFeelPainters, StdCtrls,
  cxButtons, cxControls, cxContainer, cxEdit, cxTextEdit, cxMaskEdit,
  cxDropDownEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, DB, cxDBData,
  ComCtrls, cxMemo, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Placemnt, cxImageComboBox, ActnList, XPStyleActnCtrls, ActnMan, ShellApi,
  dxmdaset,IniFiles;

type
  TfmScale = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    cxLookupComboBox1: TcxLookupComboBox;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    ViewScale: TcxGridDBTableView;
    LevelScale: TcxGridLevel;
    GridScale: TcxGrid;
    Memo1: TcxMemo;
    StatusBar1: TStatusBar;
    FormPlacement1: TFormPlacement;
    ViewScalePLU: TcxGridDBColumn;
    ViewScaleSIFR: TcxGridDBColumn;
    ViewScaleNAME1: TcxGridDBColumn;
    ViewScaleNAME2: TcxGridDBColumn;
    ViewScalePRICE: TcxGridDBColumn;
    ViewScaleSHELFLIFE: TcxGridDBColumn;
    ViewScaleTAREW: TcxGridDBColumn;
    ViewScaleSTATUS: TcxGridDBColumn;
    cxButton4: TcxButton;
    amScale: TActionManager;
    acEditPlu: TAction;
    cxButton5: TcxButton;
    cxButton6: TcxButton;
    cxButton7: TcxButton;
    ViewScaleMESSNO: TcxGridDBColumn;
    cxButton8: TcxButton;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    teTestVes: TdxMemData;
    teTestVesArticul: TIntegerField;
    teL: TdxMemData;
    teLiPlu: TIntegerField;
    teLiSrok: TIntegerField;
    teLsItem: TStringField;
    teLName1: TStringField;
    teLName2: TStringField;
    teLrPrice: TFloatField;
    teLiFormat: TSmallintField;
    teLStrI1: TStringField;
    teLStrI2: TStringField;
    teLIngr: TIntegerField;
    teLIStr1: TStringField;
    teLIStr2: TStringField;
    procedure cxButton3Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cxLookupComboBox1PropertiesChange(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure acEditPluExecute(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
    procedure cxButton7Click(Sender: TObject);
    procedure cxButton8Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure prButton(bSt:Boolean);
  end;

function Transfer_Ethernet(S:PChar):Integer; export; stdcall; far; external 'TransferEth.dll' name 'Transfer_Ethernet';
function prAddPlu(Name1,Name2,sIngr1,sIngr2:String; Plu,SReal,FormatL,iCode,LenStrI:INteger; rPrice:Real):String;

var
  fmScale: TfmScale;

implementation

uses Un1, EditPlu, dmRnEdit, u2fdk;

{$R *.dfm}

function prAddPlu(Name1,Name2,sIngr1,sIngr2:String; Plu,SReal,FormatL,iCode,LenStrI:INteger; rPrice:Real):String;
Var StrPos,S,sN,sIng,sINgCur:String;
    iSg:INteger;
    i,l:INteger;
    bIngr:Boolean;
    sTerm:String;
    St1,St2:String;
begin
  St1:='5400';
  if (sIngr1+sIngr2)>'' then St2:='0DE011' else St2:='0D2011';

  StrPos:='';
  S:=its(Plu); while length(S)<8 do S:='0'+S;   StrPos:=StrPos+S; // ����� PLU
  StrPos:=StrPos+St1; // ������ 1  (0101 01000 0000 0000)  �������� ����� � ���� �������� ������� ����� , �������� ���� �������
  StrPos:=StrPos+St2;

//  S:='5400';  StrPos:=StrPos+S; // ������ 1  (0101 01000 0000 0000)  �������� ����� � ���� �������� ������� ����� , �������� ���� �������
//  S:='0D2601';
//  S:='0D2009';
//  S:='0DA009';
//  S:='0DE009';
//  S:='0DE011';    //0DE011

  S:=fts1(Trunc(rPrice*100)); while length(S)<8 do S:='0'+S; StrPos:=StrPos+S; // ����

  S:=its(FormatL);  StrPos:=StrPos+S; // ������ F1 -11 1-�� ��������  (F2 -12)

  S:='05';  StrPos:=StrPos+S; // ������ ��������� F1F2 CCCCC XXXXX CR
  S:=its(iCode);  while length(S)<5 do S:='0'+S; S:='22'+S+'0000001'; StrPos:=StrPos+S; //��������
  if SReal<=999 then iSg:=SReal else iSg:=0;
  S:=its(iSg); while length(S)<4 do S:='0'+S; StrPos:=StrPos+S; //���� ������� � ����
  S:='00';  StrPos:=StrPos+S; // ����� �������������
  S:='00';  StrPos:=StrPos+S; // ����� �����������

  sN:=AnsiToOemConvert(Trim(Name1)); S:=IntToHex(Length(sN),2); while length(S)<2 do S:='0'+S; S:='04'+S;
  for i:=1 to Length(sN) do S:=S+IntToHex(ord(sN[i]),2);
  StrPos:=StrPos+S+'0D';

  sN:=AnsiToOemConvert(Trim(Name2)); S:=IntToHex(Length(sN),2); while length(S)<2 do S:='0'+S; S:='04'+S;
  for i:=1 to Length(sN) do S:=S+IntToHex(ord(sN[i]),2);

  StrPos:=StrPos+S+'0C';

  bIngr:=False;

  if sIngr1>'' then
  begin
    sIng:=sIngr1;
    while pos(#$0D,sIng)>0 do Delete(sIng,pos(#$0D,sIng),1);
    while pos(#$0A,sIng)>0 do sIng[pos(#$0A,sIng)]:=' ';
    if length(sIng)>200 then sIng:=Copy(sIng,1,200);
    if (LenStrI>=10) and (LenStrI<=100) then
    begin
      while sIng>'' do
      begin
        if length(sIng)>LenStrI then
        begin
          sIngCur:=Copy(sIng,1,LenStrI);
          delete(sIng,1,LenStrI);
          sTerm:='0D';
        end else
        begin
          sIngCur:=sIng;
          sIng:='';
          sTerm:='';
        end;

        sN:=AnsiToOemConvert(sIngCur); S:=IntToHex(Length(sN),2); while length(S)<2 do S:='0'+S; S:='02'+S;
        for i:=1 to Length(sN) do S:=S+IntToHex(ord(sN[i]),2);
        StrPos:=StrPos+S+sTerm;

        bIngr:=True;
      end;
    end;
  end;

  if sIngr2>'' then
  begin
    sIng:=sIngr2;
    while pos(#$0D,sIng)>0 do Delete(sIng,pos(#$0D,sIng),1);
    while pos(#$0A,sIng)>0 do sIng[pos(#$0A,sIng)]:=' ';
    if length(sIng)>200 then sIng:=Copy(sIng,1,200);
    if (LenStrI>=10) and (LenStrI<=100) then
    begin
      if sIng>'' then StrPos:=StrPos+'0D';

      while sIng>'' do
      begin
        if length(sIng)>LenStrI then
        begin
          sIngCur:=Copy(sIng,1,LenStrI);
          delete(sIng,1,LenStrI);
          sTerm:='0D';
        end else
        begin
          sIngCur:=sIng;
          sIng:='';
          sTerm:='';
        end;

        sN:=AnsiToOemConvert(sIngCur); S:=IntToHex(Length(sN),2); while length(S)<2 do S:='0'+S; S:='02'+S;
        for i:=1 to Length(sN) do S:=S+IntToHex(ord(sN[i]),2);
        StrPos:=StrPos+S+sTerm;

        bIngr:=True;
      end;
    end;
  end;

  if bIngr then   StrPos:=StrPos+'0C';

  StrPos:=StrPos+'00';

  l:=(Length(StrPos)+4) div 2;
  s:=IntToHex(l,2); while length(S)<4 do S:='0'+S; insert(S,StrPos,9);

  Result:=StrPos;
end;


procedure TfmScale.cxButton3Click(Sender: TObject);
begin
  Close;
end;

procedure TfmScale.FormCreate(Sender: TObject);
begin
  GridScale.Align:=AlClient;
  FormPlacement1.IniFileName:=CurDir+'\'+GridIni;
  FormPlacement1.Active:=True;
  ViewScale.RestoreFromIniFile(CurDir+'\'+GridIni);
//  Memo1.Clear;
end;

procedure TfmScale.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewScale.StoreToIniFile(CurDir+'\'+GridIni,False);
  if not cxButton3.Enabled then Action := caNone;
end;

procedure TfmScale.cxLookupComboBox1PropertiesChange(Sender: TObject);
begin
  with dmC do
  begin
    ViewScale.BeginUpdate;
    if quScales.RecordCount>0 then
    begin
      quScaleItems.Active:=False;
      quScaleItems.ParamByName('IDSC').AsINteger:=quScalesID.AsINteger;
      quScaleItems.Active:=True;
    end;
    ViewScale.EndUpdate;
  end;
end;

procedure TfmScale.cxButton4Click(Sender: TObject);
begin
  with dmC do
  begin
    if (quScales.RecordCount>0)and(cxLookupComboBox1.EditValue>0)then
    begin
      ViewScale.BeginUpdate;
      if quScales.RecordCount>0 then
      begin
        quScaleItems.Active:=False;
        quScaleItems.ParamByName('IDSC').AsINteger:=cxLookupComboBox1.EditValue;
        quScaleItems.Active:=True;
      end;
      ViewScale.EndUpdate;
    end;
  end;
end;

procedure TfmScale.cxButton1Click(Sender: TObject);
Var iP,n,iC:INteger;
    StrIp,StrIpFull:String;
    sPlus:String;
    f:TextFile;
    SEInfo: TShellExecuteInfo;
    ExitCode: DWORD;
    ExecuteFile, ParamString, StrErr: string;
    iFormat:integer;
begin
  with dmC do
  begin
    Memo1.Clear;    //�������� ���������

    quScales.Locate('ID',cxLookupComboBox1.EditValue,[]);

    try
      ViewScale.BeginUpdate;
      prButton(False);

      //��� ������
      if (quScalesSCALETYPE.AsString='DIGI_DRV_TCP') then
      begin
        Memo1.Lines.Add('����� ... ���� �������� ����� (DIGI).'); delay(10);

        for n:=1 to 5 do
        begin
          StrIp:=''; StrIpFull:='';
          if n=1 then StrIPFull:=quScalesIP1.AsString;
          if n=2 then StrIPFull:=quScalesIP2.AsString;
          if n=3 then StrIPFull:=quScalesIP3.AsString;
          if n=4 then StrIPFull:=quScalesIP4.AsString;
          if n=5 then StrIPFull:=quScalesIP5.AsString;

          StrIp:=StrIpFull;
          while pos('.',StrIp)>0 do delete(StrIp,1,pos('.',StrIp)); //���������� ������ ���� ��������� ��������

          iP:=StrToINtDef(StrIp,0);

          if iP>0 then //���-�� ���������� - ������
          begin
            Memo1.Lines.Add('  �������� - '+StrIpFull); delay(10);
            sPlus:=''; //������������� ������
            quScaleItems.First;
            while not quScaleItems.Eof do
            begin
              iFormat:=11; if quScaleItemsMESSNO.AsInteger>0 then iFormat:=12;

              if quScaleItemsStatus.AsInteger<>1 then
                sPlus:=sPlus+prAddPlu(quScaleItemsNAME1.AsString,quScaleItemsNAME2.AsString,'','',quScaleItemsPLU.AsInteger,quScaleItemsSHELFLIFE.AsInteger,iFormat,quScaleItemsSIFR.AsInteger,50,quScaleItemsPRICE.AsFloat);
              quScaleItems.Edit;
              quScaleItemsSTATUS.AsInteger:=1;
              quScaleItems.Post;

              quScaleItems.Next;
            end;

            if sPlus>'' then
            begin
              assignfile(f,CurDir+'sm'+StrIp+'f25.dat');
              try
                rewrite(f);
                write(f,sPlus);
                Memo1.Lines.Add('  ���� �����������.');
              finally
                closefile(f);
              end;

              try
                if FileExists(CurDir+'twswtcp.exe') then
                begin
                  Memo1.Lines.Add('  ������� "twswtcp.exe" ������.'); delay(10);
                  if FileExists(CurDir+'sm'+StrIp+'f25.dat') then
                  begin
                    Memo1.Lines.Add('  ���� ����'); delay(10);
                    Memo1.Lines.Add('  ��������� "'+CurDir+'sm'+StrIp+'f25.dat'); delay(10);
                    Memo1.Lines.Add('  ����� ... ���� ��������. '); delay(10);

                    ExecuteFile := 'twswtcp.exe';
                    ParamString:=' F25.DAT '+StrIp;
                    //sPar:='CommandLineTool.exe'+ParamString;
                    FillChar(SEInfo, SizeOf(SEInfo), 0);
                    SEInfo.cbSize := SizeOf(TShellExecuteInfo);
                    with SEInfo do
                    begin
                      fMask := SEE_MASK_NOCLOSEPROCESS;
                      Wnd := Application.Handle;
                      lpFile := PChar(ExecuteFile);
                      lpParameters := PChar(ParamString);
                      {StartInString specifies thename of the working
                      directory.If ommited, the current
                      directory is used.}
                      lpDirectory := PChar(CurDir);
                      nShow := SW_HIDE;
                    end;

                    //ShellExecute(handle, 'open', PChar(CurDir+'twswtcp.exe'),PChar(' F25.DAT '+StrIp),'', SW_HIDE);
                    try
                      iC:=0;
                      if ShellExecuteEx(@SEInfo) then
                      begin
                        repeat
                          delay(100);
                          GetExitCodeProcess(SEInfo.hProcess, ExitCode);
                          inc(iC);
                          if iC mod 50 = 0 then Memo1.Lines.Add('  ---- ������� ���� '); //������ 5 ���
                        until (ExitCode <> STILL_ACTIVE) or Application.Terminated;

                        Memo1.Lines.Add('    ������� ��������. ');

                        //BringWindowToForeground(hWind);
                        //prWM(' .. ');
                      end else  Memo1.Lines.Add('������ ������� ��������.');
                    except
                      Memo1.Lines.Add('������ ������� ��������..');
                      SetForegroundWindow(Application.Handle);
                    end;

                    delay(100);
                    if FileExists(CurDir+'error') then
                    begin
                      Memo1.Lines.Add('  ��������� ������ ��������. '); delay(10);
                      try
                        assignfile(F,CurDir+'error');
                        Reset(F);
                        ReadLn(F,StrErr);
                        StrErr:=Trim(StrErr);
                        if StrErr='0' then
                        begin
                          Memo1.Lines.Add('  �������� ��. "'+StrErr+'"'); delay(10);
                        end else
                        begin
                          Memo1.Lines.Add('  ������ �������� - "'+StrErr+'"'); delay(10);
                        end;
                      finally
                        Closefile(F);
                      end;
                    end else begin Memo1.Lines.Add('  ���� "'+CurDir+'error" �� ������. ������ �������� �� ��������.'); delay(10); end;
                  end else begin Memo1.Lines.Add('  ������ - ���� �� ������.'); delay(10); end;
                end else begin Memo1.Lines.Add('  ������ - ������� "'+CurDir+'twswtcp.exe" �� ������.'); delay(10); end;
              except
              end;
            end;
          end;
        end; //����� IP
      end;

    finally
      ViewScale.EndUpdate;
      ViewScale.Controller.ClearSelection;
      prButton(True);
    end;

    Memo1.Lines.Add('�������� ����� ���������.');
  end;
end;

procedure TfmScale.prButton(bSt:Boolean);
begin
  cxButton1.Enabled:=bSt;
  cxButton2.Enabled:=bSt;
  cxButton3.Enabled:=bSt;
  cxButton4.Enabled:=bSt;
  cxButton5.Enabled:=bSt;
  cxButton6.Enabled:=bSt;
  cxButton7.Enabled:=bSt;
  cxButton8.Enabled:=bSt;
end;

procedure TfmScale.cxButton2Click(Sender: TObject);
Var iP,n,i,j:INteger;
    StrIp:String;
    Rec:TcxCustomGridRecord;
    iPlu,iSrok,iC,iItem:INteger;
    Name1,Name2:String;
    rPrice:Real;
    iFormat:Integer;
    StrIpFull:String;
    sPlus:String;
    f:TextFile;
    SEInfo: TShellExecuteInfo;
    ExitCode: DWORD;
    ExecuteFile, ParamString, StrErr: string;
begin
  with dmC do
  begin
    if ViewScale.Controller.SelectedRecordCount=0 then exit;

    Memo1.Clear;    //�������� ���������

    ViewScale.BeginUpdate;
    prButton(False);
    quScales.Locate('ID',cxLookupComboBox1.EditValue,[]);

    //��� ������
    if (quScalesSCALETYPE.AsString='DIGI_DRV_TCP') then
    begin
      Memo1.Lines.Add('����� ... ���� �������� ����� (DIGI).'); delay(10);

      for n:=1 to 5 do
      begin
        StrIp:=''; StrIpFull:='';
        if n=1 then StrIPFull:=quScalesIP1.AsString;
        if n=2 then StrIPFull:=quScalesIP2.AsString;
        if n=3 then StrIPFull:=quScalesIP3.AsString;
        if n=4 then StrIPFull:=quScalesIP4.AsString;
        if n=5 then StrIPFull:=quScalesIP5.AsString;

        StrIp:=StrIpFull;
        while pos('.',StrIp)>0 do delete(StrIp,1,pos('.',StrIp)); //���������� ������ ���� ��������� ��������

        iP:=StrToINtDef(StrIp,0);

        if iP>0 then //���-�� ���������� - ������
        begin
          Memo1.Lines.Add('  �������� - '+StrIpFull); delay(10);
          try
            sPlus:='';
            for i:=0 to ViewScale.Controller.SelectedRecordCount-1 do
            begin
              Rec:=ViewScale.Controller.SelectedRecords[i];

              iPlu:=0;
              iSrok:=0;
              iItem:=0;
              Name1:='';
              Name2:='';
              rPrice:=0;
              iFormat:=0;

              for j:=0 to Rec.ValueCount-1 do
              begin
                if ViewScale.Columns[j].Name='ViewScalePLU' then begin try iPlu:=Rec.Values[j]; except; end; end;
                if ViewScale.Columns[j].Name='ViewScaleSHELFLIFE' then begin try iSrok:=Rec.Values[j]; except; end; end;
                if ViewScale.Columns[j].Name='ViewScaleSIFR' then begin try iItem:=Rec.Values[j]; except; end; end;
                if ViewScale.Columns[j].Name='ViewScaleNAME1' then begin try Name1:=Rec.Values[j]; except; end; end;
                if ViewScale.Columns[j].Name='ViewScaleNAME2' then begin try Name2:=Rec.Values[j]; except; end; end;
                if ViewScale.Columns[j].Name='ViewScalePRICE' then begin try rPrice:=Rec.Values[j]; except; end; end;
                if ViewScale.Columns[j].Name='ViewScaleMESSNO' then begin try iFormat:=Rec.Values[j]; except; end; end;
              end;

              if iPlu>0 then   
              begin
                if iFormat>0 then iFormat:=12 else iFormat:=11;
                sPlus:=sPlus+prAddPlu(Name1,Name2,'','',iPlu,iSrok,iFormat,iItem,50,rPrice);

                if quScaleItems.Locate('SIFR',iItem,[]) then
                begin
                  quScaleItems.Edit;
                  quScaleItemsSTATUS.AsInteger:=1;
                  quScaleItems.Post;
                end;
              end;
            end;

            if sPlus>'' then
            begin
              assignfile(f,CurDir+'sm'+StrIp+'f25.dat');
              try
                rewrite(f);
                write(f,sPlus);
                Memo1.Lines.Add('  ���� �����������.');
              finally
                closefile(f);
              end;

              try
                if FileExists(CurDir+'twswtcp.exe') then
                begin
                  Memo1.Lines.Add('  ������� "twswtcp.exe" ������.'); delay(10);
                  if FileExists(CurDir+'sm'+StrIp+'f25.dat') then
                  begin
                    Memo1.Lines.Add('  ���� ����'); delay(10);
                    Memo1.Lines.Add('  ��������� "'+CurDir+'sm'+StrIp+'f25.dat'); delay(10);
                    Memo1.Lines.Add('  ����� ... ���� ��������. '); delay(10);

                    ExecuteFile := 'twswtcp.exe';
                    ParamString:=' F25.DAT '+StrIp;
                    //sPar:='CommandLineTool.exe'+ParamString;
                    FillChar(SEInfo, SizeOf(SEInfo), 0);
                    SEInfo.cbSize := SizeOf(TShellExecuteInfo);
                    with SEInfo do
                    begin
                      fMask := SEE_MASK_NOCLOSEPROCESS;
                      Wnd := Application.Handle;
                      lpFile := PChar(ExecuteFile);
                      lpParameters := PChar(ParamString);
                      {StartInString specifies thename of the working
                      directory.If ommited, the current
                      directory is used.}
                      lpDirectory := PChar(CurDir);
                      nShow := SW_HIDE;
                    end;

                    //ShellExecute(handle, 'open', PChar(CurDir+'twswtcp.exe'),PChar(' F25.DAT '+StrIp),'', SW_HIDE);
                    try
                      iC:=0;
                      if ShellExecuteEx(@SEInfo) then
                      begin
                        repeat
                          delay(100);
                          GetExitCodeProcess(SEInfo.hProcess, ExitCode);
                          inc(iC);
                          if iC mod 50 = 0 then Memo1.Lines.Add('  ---- ������� ���� '); //������ 5 ���
                        until (ExitCode <> STILL_ACTIVE) or Application.Terminated;

                        Memo1.Lines.Add('    ������� ��������. ');

                        //BringWindowToForeground(hWind);
                        //prWM(' .. ');
                      end else  Memo1.Lines.Add('������ ������� ��������.');
                    except
                      Memo1.Lines.Add('������ ������� ��������..');
                      SetForegroundWindow(Application.Handle);
                    end;

                    delay(100);
                    if FileExists(CurDir+'error') then
                    begin
                      Memo1.Lines.Add('  ��������� ������ ��������. '); delay(10);
                      try
                        assignfile(F,CurDir+'error');
                        Reset(F);
                        ReadLn(F,StrErr);
                        StrErr:=Trim(StrErr);
                        if StrErr='0' then
                        begin
                          Memo1.Lines.Add('  �������� ��. "'+StrErr+'"'); delay(10);
                        end else
                        begin
                          Memo1.Lines.Add('  ������ �������� - "'+StrErr+'"'); delay(10);
                        end;
                      finally
                        Closefile(F);
                      end;


                    end else begin Memo1.Lines.Add('  ���� "'+CurDir+'error" �� ������. ������ �������� �� ��������.'); delay(10); end;
                  end else begin Memo1.Lines.Add('  ������ - ���� �� ������.'); delay(10); end;
                end else begin Memo1.Lines.Add('  ������ - ������� "'+CurDir+'twswtcp.exe" �� ������.'); delay(10); end;
              except
              end;
            end;

          except
          end;
        end;
      end;
    end;  
    ViewScale.EndUpdate;
    ViewScale.Controller.ClearSelection;
    prButton(True);
    Memo1.Lines.Add('�������� ����� ���������.');
  end;
end;

procedure TfmScale.acEditPluExecute(Sender: TObject);
begin
  //������������� ���
  with dmC do
  begin
    if quScaleItems.RecordCount>0 then
    begin
      fmEditPlu:=tfmEditPlu.Create(Application);
      fmEditPlu.cxSpinEdit1.EditValue:=quScaleItemsPLU.AsInteger;
      fmEditPlu.cxSpinEdit2.EditValue:=quScaleItemsSIFR.AsInteger;
      fmEditPlu.cxTextEdit2.Text:=quScaleItemsNAME1.AsString;
      fmEditPlu.cxTextEdit3.Text:=quScaleItemsNAME2.AsString;
      fmEditPlu.cxSpinEdit3.EditValue:=quScaleItemsSHELFLIFE.AsInteger;
      fmEditPlu.cxCalcEdit1.EditValue:=quScaleItemsPRICE.AsFloat;
      fmEditPlu.cxComboBox1.ItemIndex:=quScaleItemsMESSNO.AsInteger;
      fmEditPlu.ShowModal;
      if fmEditPlu.ModalResult=mrOk then
      begin //���������
        quScaleItems.Edit;
//        quScaleItemsPLU.AsInteger:=iPlu;
//        quScaleItemsSIFR.AsInteger:=iNum;
        quScaleItemsNAME1.AsString:=fmEditPlu.cxTextEdit2.Text;
        quScaleItemsNAME2.AsString:=fmEditPlu.cxTextEdit3.Text;
        quScaleItemsPRICE.AsFloat:=fmEditPlu.cxCalcEdit1.EditValue;
        quScaleItemsSTATUS.AsInteger:=0; //�������
        quScaleItemsSHELFLIFE.AsInteger:=fmEditPlu.cxSpinEdit3.EditValue;
        quScaleItemsMESSNO.AsInteger:=fmEditPlu.cxComboBox1.ItemIndex;
        quScaleItems.Post;
      end;
      fmEditPlu.Release;
    end;
  end;
end;

procedure TfmScale.cxButton5Click(Sender: TObject);
begin
  prNExportExel5(ViewScale);
end;

procedure TfmScale.cxButton6Click(Sender: TObject);
Var iC,l:Integer;
    sN1,sN2,sN,s:String;
    bE:Boolean;
begin
  with dmC do
  begin
    Memo1.Clear;    //�������� ���������

    Memo1.Lines.Add('����� ... ���� ���������� ���������� � ������ �� ��������.'); delay(10);
    ViewScale.BeginUpdate;
    prButton(False);

    iC:=0;
    quScaleItems.First;
    while not quScaleItems.Eof do
    begin
      quMenuID.Active:=False;
      quMenuID.ParamByName('IDM').AsInteger:=quScaleItemsSIFR.AsInteger;
      quMenuID.Active:=True;
      if quMenuID.RecordCount>0 then
      begin
        sN:=quMenuIDNAME.AsString;
        sN1:=Copy(sN,1,29);
        l:=28;
        if (Pos(' ',sN1)>0) and(length(sN)>28) then
        begin
          s:=sN1[l];     //��� ������ ������ � ����� � ������ �������� - ������ �������������� � ���� ��������
          while s<>' ' do
          begin
            dec(l);
            s:=sN1[l];
          end;
        end;

        sN1:=Copy(sN,1,l);
        if Length(sN)>l then sN2:=Copy(sN,(l+1),28) else sN2:='';

        bE:=False;

        sN1:=TrimRight(sN1);
        sN2:=TrimRight(sN2);

        if abs(quScaleItemsPRICE.AsFloat-quMenuIDPRICE.AsFloat)>=0.01 then bE:=True;
        if sN1<>quScaleItemsNAME1.AsString then bE:=True;
        if sN2<>quScaleItemsNAME2.AsString then bE:=True;

        if bE then
        begin //���� ����������
          try
            quScaleItems.Edit;
            quScaleItemsNAME1.AsString:=sN1;
            quScaleItemsNAME2.AsString:=sN2;
            quScaleItemsPRICE.AsFloat:=quMenuIDPRICE.AsFloat;
            quScaleItemsSTATUS.AsInteger:=0; //�������
            quScaleItems.Post;
          except
          end;  
        end;
      end;
      quMenuID.Active:=False;

      quScaleItems.Next; inc(iC);
      if iC mod 100 = 0 then
      begin
        Memo1.Lines.Add('  ���������� - '+its(iC)); delay(100);
      end;
    end;

    ViewScale.EndUpdate;
    ViewScale.Controller.ClearSelection;

    prButton(True);
    Memo1.Lines.Add('���������� ���������.');
  end;
end;

procedure TfmScale.cxButton7Click(Sender: TObject);
{Var iL,iP,n,i,j:INteger;
    StrIp:String;
    Rec:TcxCustomGridRecord;
    iPlu,iSrok:INteger;
    sItem,Name1,Name2:String;}
begin
   //�������
  with dmC do
  begin
    if ViewScale.Controller.SelectedRecordCount=0 then exit;
    Memo1.Clear;    //�������� ���������

  end;
end;

procedure TfmScale.cxButton8Click(Sender: TObject);
Var n:INteger;
    StrIp,StrIpLast:String;
    bErr:Boolean;
    F:TextFile;
    StrErr:String;
begin
  with dmC do
  begin
    Memo1.Clear;    //�������� �������� �������� � ��������� ����
    quScales.Locate('ID',cxLookupComboBox1.EditValue,[]);
    if (quScalesSCALETYPE.AsString='DIGI_TCP')or(quScalesSCALETYPE.AsString='DIGI_DRV_TCP') then
    begin
      Memo1.Lines.Add('����� ... ���� �������� �������� �������� � ����.'); delay(10);
      prButton(False);
      bErr:=False;

      for n:=1 to 5 do
      begin
        StrIp:='';
        if n=1 then StrIP:=quScalesIP1.AsString;
        if n=2 then StrIP:=quScalesIP2.AsString;
        if n=3 then StrIP:=quScalesIP3.AsString;
        if n=4 then StrIP:=quScalesIP4.AsString;
        if n=5 then StrIP:=quScalesIP5.AsString;

        StrIpLast:=StrIp;
        while pos('.',StrIpLast)>0 do delete(StrIpLast,1,pos('.',StrIpLast)); //���������� ������ ���� ��������� ��������
//        while pos('0',StrIpLast)=1 do delete(StrIpLast,1,1);

        if (StrIp>'0')and(StrIpLast>'0') then //���-�� ���������� - ������
        begin
          Memo1.Lines.Add('  �������� - '+StrIp); delay(10);
          try
            if FileExists(CurDir+'twswtcp.exe') then
            begin
              Memo1.Lines.Add('  ������� "twswtcp.exe" ������.'); delay(10);
              if FileExists(CurDir+'f34_1.dat') then
              begin
                Memo1.Lines.Add('  ������ "'+CurDir+'f34_1.dat'+'" ������.'); delay(10);
                if FileExists(CurDir+'f34_2.dat') then
                begin
                  Memo1.Lines.Add('  ������ "'+CurDir+'f34_2.dat'+'" ������.'); delay(10);

                  Memo1.Lines.Add('  ������� "'+CurDir+'sm'+StrIpLast+'f34.dat'); delay(10);
                  DeleteFile(CurDir+'sm'+StrIpLast+'f34.dat');

                  Memo1.Lines.Add('  �������� "'+CurDir+'f34_1.dat'+'" � '+CurDir+'sm'+StrIpLast+'f34.dat'); delay(10);
                  CopyFile(PChar(CurDir+'f34_1.dat'),PChar(CurDir+'sm'+StrIpLast+'f34.dat'),False);

                  Memo1.Lines.Add('  ��������� "'+CurDir+'sm'+StrIpLast+'f34.dat'); delay(10);
                  Memo1.Lines.Add('  ����� ... ���� ��������. '); delay(10);
                  ShellExecute(handle, 'open', PChar(CurDir+'twswtcp.exe'),PChar(' F34.DAT '+StrIpLast),'', SW_HIDE);
                  delay(3000);
                  if FileExists(CurDir+'error') then
                  begin
                    Memo1.Lines.Add('  ��������� ������ ��������. '); delay(10);
                    try
                      assignfile(F,CurDir+'error');
                      Reset(F);
                      ReadLn(F,StrErr);
                      StrErr:=Trim(StrErr);
                      if StrErr='0' then
                      begin
                        Memo1.Lines.Add('  �������� ��. "'+StrErr+'"'); delay(10);
                      end else
                      begin
                        Memo1.Lines.Add('  ������ �������� - "'+StrErr+'"'); delay(10);
                        bErr:=True;
                      end;
                    finally
                      Closefile(F);
                    end;
                  end else
                  begin
                    Memo1.Lines.Add('  ���� "'+CurDir+'error" �� ������. ������ �������� �� ��������.'); delay(10);
                    bErr:=True;
                  end;

                  Memo1.Lines.Add('  ������� "'+CurDir+'sm'+StrIpLast+'f34.dat'); delay(10);
                  DeleteFile(CurDir+'sm'+StrIpLast+'f34.dat');
                  Memo1.Lines.Add('  �������� "'+CurDir+'f34_2.dat'+'" � '+CurDir+'sm'+StrIpLast+'f34.dat'); delay(10);
                  CopyFile(PChar(CurDir+'f34_2.dat'),PChar(CurDir+'sm'+StrIpLast+'f34.dat'),False);

                  Memo1.Lines.Add('  ��������� "'+CurDir+'sm'+StrIpLast+'f34.dat'); delay(10);
                  Memo1.Lines.Add('  ����� ... ���� ��������. '); delay(10);
                  ShellExecute(handle, 'open', PChar(CurDir+'twswtcp.exe'),PChar(' F34.DAT '+StrIpLast),'', SW_HIDE);
                  delay(3000);
                  if FileExists(CurDir+'error') then
                  begin
                    Memo1.Lines.Add('  ��������� ������ ��������. '); delay(10);
                    try
                      assignfile(F,CurDir+'error');
                      Reset(F);
                      ReadLn(F,StrErr);
                      StrErr:=Trim(StrErr);
                      if StrErr='0' then
                      begin
                        Memo1.Lines.Add('  �������� ��. "'+StrErr+'"'); delay(10);
                      end else
                      begin
                        Memo1.Lines.Add('  ������ �������� - "'+StrErr+'"'); delay(10);
                        bErr:=True;
                      end;
                    finally
                      Closefile(F);
                    end;
                  end else
                  begin
                    Memo1.Lines.Add('  ���� "'+CurDir+'error" �� ������. ������ �������� �� ��������.'); delay(10);
                    bErr:=True;
                  end;

                end else
                begin
                  Memo1.Lines.Add('  ������ - ������ "'+CurDir+'f34_2.dat'+'" �� ������.'); delay(10);
                  bErr:=True;
                end;
              end else
              begin
                Memo1.Lines.Add('  ������ - ������ "'+CurDir+'f34_1.dat'+'" �� ������.'); delay(10);
                bErr:=True;
              end;
            end else
            begin
              Memo1.Lines.Add('  ������ - ������� "'+CurDir+'twswtcp.exe" �� ������.'); delay(10);
              bErr:=True;
            end;
          except
            bErr:=True;
          end;
        end; //����� IP
      end;
//      Memo1.Lines.Add('');
      prButton(True);
      if bErr=False then
        Memo1.Lines.Add('�������� �������� �������� ���������.')
      else
        Memo1.Lines.Add('������ ��� ��������� �������� ��������.');
    end;

  end;
end;




procedure TfmScale.FormShow(Sender: TObject);
begin
  Memo1.Clear;
end;

end.
