object dmCB: TdmCB
  OldCreateOrder = False
  Left = 930
  Top = 388
  Height = 594
  Width = 964
  object quDocZHRec: TADOQuery
    Connection = msConnection
    CursorType = ctStatic
    CommandTimeout = 1800
    Parameters = <
      item
        Name = 'IDH'
        Attributes = [paSigned]
        DataType = ftLargeint
        Precision = 19
        Size = 8
        Value = Null
      end>
    SQL.Strings = (
      'SELECT '
      '* FROM [DOCZHEAD] DZH'
      'where DZH.[ID]=:IDH')
    Left = 192
    Top = 9
    object quDocZHRecID: TLargeintField
      FieldName = 'ID'
      ReadOnly = True
    end
    object quDocZHRecDOCDATE: TWideStringField
      FieldName = 'DOCDATE'
      Size = 10
    end
    object quDocZHRecDOCDATEZ: TWideStringField
      FieldName = 'DOCDATEZ'
      FixedChar = True
      Size = 10
    end
    object quDocZHRecIDATE: TIntegerField
      FieldName = 'IDATE'
    end
    object quDocZHRecIDATEZ: TIntegerField
      FieldName = 'IDATEZ'
    end
    object quDocZHRecDAYSTOZ: TSmallintField
      FieldName = 'DAYSTOZ'
    end
    object quDocZHRecDOCNUM: TStringField
      FieldName = 'DOCNUM'
    end
    object quDocZHRecISHOP: TIntegerField
      FieldName = 'ISHOP'
    end
    object quDocZHRecDEPART: TIntegerField
      FieldName = 'DEPART'
    end
    object quDocZHRecITYPE: TSmallintField
      FieldName = 'ITYPE'
    end
    object quDocZHRecINSUMIN: TFloatField
      FieldName = 'INSUMIN'
    end
    object quDocZHRecINSUMIN0: TFloatField
      FieldName = 'INSUMIN0'
    end
    object quDocZHRecDEPARTORG: TIntegerField
      FieldName = 'DEPARTORG'
    end
    object quDocZHRecCLITYPE: TSmallintField
      FieldName = 'CLITYPE'
    end
    object quDocZHRecCLICODE: TIntegerField
      FieldName = 'CLICODE'
    end
    object quDocZHRecIACTIVE: TSmallintField
      FieldName = 'IACTIVE'
    end
    object quDocZHRecDATEEDIT: TDateTimeField
      FieldName = 'DATEEDIT'
    end
    object quDocZHRecPERSON: TStringField
      FieldName = 'PERSON'
      Size = 50
    end
    object quDocZHRecCLISUMIN: TFloatField
      FieldName = 'CLISUMIN'
    end
    object quDocZHRecCLISUMIN0: TFloatField
      FieldName = 'CLISUMIN0'
    end
    object quDocZHRecCLISUMINN: TFloatField
      FieldName = 'CLISUMINN'
    end
    object quDocZHRecCLISUMIN0N: TFloatField
      FieldName = 'CLISUMIN0N'
    end
    object quDocZHRecQUANT: TFloatField
      FieldName = 'QUANT'
    end
    object quDocZHRecCLIQUANT: TFloatField
      FieldName = 'CLIQUANT'
    end
    object quDocZHRecCLIQUANTN: TFloatField
      FieldName = 'CLIQUANTN'
    end
    object quDocZHRecQUANTZAUTO: TFloatField
      FieldName = 'QUANTZAUTO'
    end
    object quDocZHRecIDATEPOST: TIntegerField
      FieldName = 'IDATEPOST'
    end
    object quDocZHRecIDATENACL: TIntegerField
      FieldName = 'IDATENACL'
    end
    object quDocZHRecSNUMNACL: TStringField
      FieldName = 'SNUMNACL'
      Size = 50
    end
    object quDocZHRecIDATESCHF: TIntegerField
      FieldName = 'IDATESCHF'
    end
    object quDocZHRecSNUMSCHF: TStringField
      FieldName = 'SNUMSCHF'
      Size = 50
    end
    object quDocZHRecSENDTO: TSmallintField
      FieldName = 'SENDTO'
    end
    object quDocZHRecSENDDATE: TDateTimeField
      FieldName = 'SENDDATE'
    end
    object quDocZHRecSNUMPODTV: TWideStringField
      FieldName = 'SNUMPODTV'
      Size = 50
    end
    object quDocZHRecSENDNACL: TSmallintField
      FieldName = 'SENDNACL'
    end
    object quDocZHRecIDHPRO: TIntegerField
      FieldName = 'IDHPRO'
    end
  end
  object quDocZSSel: TADOQuery
    Connection = msConnection
    CursorType = ctStatic
    CommandTimeout = 1800
    Parameters = <
      item
        Name = 'IIDH'
        Attributes = [paSigned]
        DataType = ftLargeint
        Precision = 19
        Size = 8
        Value = '1'
      end>
    SQL.Strings = (
      'SELECT ds.[IDH]'
      '      ,ds.[IDS]'
      '      ,ds.[CODE]'
      '      ,ds.[BARCODE]'
      '      ,ds.[QUANTPACK]'
      '      ,ds.[PRICEIN]'
      '      ,ds.[PRICEIN0]'
      '      ,ds.[PRICEM]'
      '      ,ds.[SUMIN]'
      '      ,ds.[SUMIN0]'
      '      ,ds.[SUMNDS]'
      '      ,ds.[NDSPROC]'
      '      ,ds.[REMN1]'
      '      ,ds.[VREAL]'
      '      ,ds.[REMNDAY]'
      '      ,ds.[REMNMIN]'
      '      ,ds.[REMNMAX]'
      '      ,ds.[REMNSTRAH]'
      '      ,ds.[REMN2]'
      '      ,ds.[PKDOC]'
      '      ,ds.[PKCARD]'
      '      ,ds.[DAYSTOZ]'
      '      ,ds.[QUANTZ]'
      '      ,ds.[QUANTZALR]'
      '      ,ds.[QUANTZITOG]'
      '      ,ds.[QUANTZFACT]'
      '      ,ds.[CLIQUANT]'
      '      ,ds.[CLIPRICE]'
      '      ,ds.[CLIPRICE0]'
      '      ,ds.[CLIQUANTN]'
      '      ,ds.[CLIPRICEN]'
      '      ,ds.[CLIPRICE0N]'
      '      ,ds.[CLINNUM]'
      '     ,ds.[ROUNDPACK]'
      '  '
      'FROM [DOCZSPEC] ds'
      'where ds.[IDH]=:IIDH')
    Left = 112
    Top = 9
    object quDocZSSelIDH: TLargeintField
      FieldName = 'IDH'
    end
    object quDocZSSelIDS: TLargeintField
      FieldName = 'IDS'
      ReadOnly = True
    end
    object quDocZSSelCODE: TIntegerField
      FieldName = 'CODE'
    end
    object quDocZSSelBARCODE: TStringField
      FieldName = 'BARCODE'
      FixedChar = True
      Size = 15
    end
    object quDocZSSelQUANTPACK: TFloatField
      FieldName = 'QUANTPACK'
    end
    object quDocZSSelPRICEIN: TFloatField
      FieldName = 'PRICEIN'
    end
    object quDocZSSelPRICEIN0: TFloatField
      FieldName = 'PRICEIN0'
    end
    object quDocZSSelPRICEM: TFloatField
      FieldName = 'PRICEM'
    end
    object quDocZSSelSUMIN: TFloatField
      FieldName = 'SUMIN'
    end
    object quDocZSSelSUMIN0: TFloatField
      FieldName = 'SUMIN0'
    end
    object quDocZSSelSUMNDS: TFloatField
      FieldName = 'SUMNDS'
    end
    object quDocZSSelNDSPROC: TFloatField
      FieldName = 'NDSPROC'
    end
    object quDocZSSelREMN1: TFloatField
      FieldName = 'REMN1'
    end
    object quDocZSSelVREAL: TFloatField
      FieldName = 'VREAL'
    end
    object quDocZSSelREMNDAY: TFloatField
      FieldName = 'REMNDAY'
    end
    object quDocZSSelREMNMIN: TFloatField
      FieldName = 'REMNMIN'
    end
    object quDocZSSelREMNMAX: TFloatField
      FieldName = 'REMNMAX'
    end
    object quDocZSSelREMNSTRAH: TFloatField
      FieldName = 'REMNSTRAH'
    end
    object quDocZSSelREMN2: TFloatField
      FieldName = 'REMN2'
    end
    object quDocZSSelPKDOC: TFloatField
      FieldName = 'PKDOC'
    end
    object quDocZSSelPKCARD: TFloatField
      FieldName = 'PKCARD'
    end
    object quDocZSSelDAYSTOZ: TSmallintField
      FieldName = 'DAYSTOZ'
    end
    object quDocZSSelQUANTZ: TFloatField
      FieldName = 'QUANTZ'
    end
    object quDocZSSelQUANTZALR: TFloatField
      FieldName = 'QUANTZALR'
    end
    object quDocZSSelQUANTZITOG: TFloatField
      FieldName = 'QUANTZITOG'
    end
    object quDocZSSelQUANTZFACT: TFloatField
      FieldName = 'QUANTZFACT'
    end
    object quDocZSSelCLIQUANT: TFloatField
      FieldName = 'CLIQUANT'
    end
    object quDocZSSelCLIPRICE: TFloatField
      FieldName = 'CLIPRICE'
    end
    object quDocZSSelCLIPRICE0: TFloatField
      FieldName = 'CLIPRICE0'
    end
    object quDocZSSelCLIQUANTN: TFloatField
      FieldName = 'CLIQUANTN'
    end
    object quDocZSSelCLIPRICEN: TFloatField
      FieldName = 'CLIPRICEN'
    end
    object quDocZSSelCLIPRICE0N: TFloatField
      FieldName = 'CLIPRICE0N'
    end
    object quDocZSSelCLINNUM: TIntegerField
      FieldName = 'CLINNUM'
    end
    object quDocZSSelROUNDPACK: TIntegerField
      FieldName = 'ROUNDPACK'
    end
  end
  object msConnection: TADOConnection
    CommandTimeout = 800
    ConnectionString = 'FILE NAME=C:\_CasherRn\BIN\ECr.udl'
    ConnectionTimeout = 800
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 32
    Top = 9
  end
  object quDocZSNacl: TADOQuery
    Connection = msConnection
    CursorType = ctStatic
    CommandTimeout = 1800
    Parameters = <
      item
        Name = 'IDH'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM [DOCZSPECNACL] ds'
      'where ds.[IDH]=:IDH')
    Left = 112
    Top = 73
    object quDocZSNaclIDH: TIntegerField
      FieldName = 'IDH'
    end
    object quDocZSNaclIDS: TIntegerField
      FieldName = 'IDS'
    end
    object quDocZSNaclICODE: TIntegerField
      FieldName = 'ICODE'
    end
    object quDocZSNaclICARDTYPE: TIntegerField
      FieldName = 'ICARDTYPE'
    end
    object quDocZSNaclQUANT: TFloatField
      FieldName = 'QUANT'
    end
    object quDocZSNaclPRICE0: TFloatField
      FieldName = 'PRICE0'
    end
    object quDocZSNaclPRICE: TFloatField
      FieldName = 'PRICE'
    end
    object quDocZSNaclRNDS: TFloatField
      FieldName = 'RNDS'
    end
    object quDocZSNaclRSUM0: TFloatField
      FieldName = 'RSUM0'
    end
    object quDocZSNaclRSUM: TFloatField
      FieldName = 'RSUM'
    end
    object quDocZSNaclRSUMNDS: TFloatField
      FieldName = 'RSUMNDS'
    end
    object quDocZSNaclNAME: TStringField
      FieldName = 'NAME'
      Size = 200
    end
  end
  object taNums: TADOQuery
    Connection = msConnection
    CursorType = ctStatic
    CommandTimeout = 1800
    Parameters = <
      item
        Name = 'ITYPE'
        Attributes = [paSigned]
        DataType = ftSmallint
        Precision = 5
        Size = 2
        Value = Null
      end>
    SQL.Strings = (
      'SELECT [IT]'
      '      ,[SPRE]'
      '      ,[CURNUM]'
      '  FROM [DOCNUMS]'
      '  where [IT]=:ITYPE')
    Left = 268
    Top = 9
    object taNumsIT: TSmallintField
      FieldName = 'IT'
    end
    object taNumsSPRE: TStringField
      FieldName = 'SPRE'
      FixedChar = True
      Size = 10
    end
    object taNumsCURNUM: TLargeintField
      FieldName = 'CURNUM'
    end
  end
  object quDocZH: TADOQuery
    Connection = msConnection
    CursorType = ctStatic
    CommandTimeout = 1800
    Parameters = <
      item
        Name = 'IDATE'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'SNUM'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = ''
      end>
    SQL.Strings = (
      'SELECT [ID]'
      '      ,[DOCDATE]'
      '      ,[DOCDATEZ]'
      '      ,[IDATE]'
      '      ,[IDATEZ]'
      '      ,[DAYSTOZ]'
      '      ,[DOCNUM]'
      '      ,[ISHOP]'
      '      ,[DEPART]'
      '      ,[ITYPE]'
      '      ,[INSUMIN]'
      '      ,[INSUMIN0]'
      '      ,[DEPARTORG]'
      '      ,[CLITYPE]'
      '      ,[CLICODE]'
      '      ,[IACTIVE]'
      '      ,[DATEEDIT]'
      '      ,[PERSON]'
      '      ,[CLISUMIN]'
      '      ,[CLISUMIN0]'
      '      ,[CLISUMINN]'
      '      ,[CLISUMIN0N]'
      '      ,[QUANT]'
      '      ,[CLIQUANT]'
      '      ,[CLIQUANTN]'
      '      ,[QUANTZAUTO]'
      '      ,[IDATEPOST]'
      '      ,[IDATENACL]'
      '      ,[SNUMNACL]'
      '      ,[IDATESCHF]'
      '      ,[SNUMSCHF]'
      '      ,[SENDTO]'
      '      ,[SENDDATE]'
      '      ,[SNUMPODTV]'
      '      ,[SENDNACL]'
      '      ,[IDHPRO]'
      '  FROM [ECrystal ].[dbo].[DOCZHEAD] '
      '  where IDATE=:IDATE '
      '  and DOCNUM = :SNUM')
    Left = 352
    Top = 9
    object quDocZHID: TLargeintField
      FieldName = 'ID'
      ReadOnly = True
    end
    object quDocZHDOCDATE: TWideStringField
      FieldName = 'DOCDATE'
      Size = 10
    end
    object quDocZHDOCDATEZ: TWideStringField
      FieldName = 'DOCDATEZ'
      FixedChar = True
      Size = 10
    end
    object quDocZHIDATE: TIntegerField
      FieldName = 'IDATE'
    end
    object quDocZHIDATEZ: TIntegerField
      FieldName = 'IDATEZ'
    end
    object quDocZHDAYSTOZ: TSmallintField
      FieldName = 'DAYSTOZ'
    end
    object quDocZHDOCNUM: TStringField
      FieldName = 'DOCNUM'
    end
    object quDocZHISHOP: TIntegerField
      FieldName = 'ISHOP'
    end
    object quDocZHDEPART: TIntegerField
      FieldName = 'DEPART'
    end
    object quDocZHITYPE: TSmallintField
      FieldName = 'ITYPE'
    end
    object quDocZHINSUMIN: TFloatField
      FieldName = 'INSUMIN'
    end
    object quDocZHINSUMIN0: TFloatField
      FieldName = 'INSUMIN0'
    end
    object quDocZHDEPARTORG: TIntegerField
      FieldName = 'DEPARTORG'
    end
    object quDocZHCLITYPE: TSmallintField
      FieldName = 'CLITYPE'
    end
    object quDocZHCLICODE: TIntegerField
      FieldName = 'CLICODE'
    end
    object quDocZHIACTIVE: TSmallintField
      FieldName = 'IACTIVE'
    end
    object quDocZHDATEEDIT: TDateTimeField
      FieldName = 'DATEEDIT'
    end
    object quDocZHPERSON: TStringField
      FieldName = 'PERSON'
      Size = 50
    end
    object quDocZHCLISUMIN: TFloatField
      FieldName = 'CLISUMIN'
    end
    object quDocZHCLISUMIN0: TFloatField
      FieldName = 'CLISUMIN0'
    end
    object quDocZHCLISUMINN: TFloatField
      FieldName = 'CLISUMINN'
    end
    object quDocZHCLISUMIN0N: TFloatField
      FieldName = 'CLISUMIN0N'
    end
    object quDocZHQUANT: TFloatField
      FieldName = 'QUANT'
    end
    object quDocZHCLIQUANT: TFloatField
      FieldName = 'CLIQUANT'
    end
    object quDocZHCLIQUANTN: TFloatField
      FieldName = 'CLIQUANTN'
    end
    object quDocZHQUANTZAUTO: TFloatField
      FieldName = 'QUANTZAUTO'
    end
    object quDocZHIDATEPOST: TIntegerField
      FieldName = 'IDATEPOST'
    end
    object quDocZHIDATENACL: TIntegerField
      FieldName = 'IDATENACL'
    end
    object quDocZHSNUMNACL: TStringField
      FieldName = 'SNUMNACL'
      Size = 50
    end
    object quDocZHIDATESCHF: TIntegerField
      FieldName = 'IDATESCHF'
    end
    object quDocZHSNUMSCHF: TStringField
      FieldName = 'SNUMSCHF'
      Size = 50
    end
    object quDocZHSENDTO: TSmallintField
      FieldName = 'SENDTO'
    end
    object quDocZHSENDDATE: TDateTimeField
      FieldName = 'SENDDATE'
    end
    object quDocZHSNUMPODTV: TWideStringField
      FieldName = 'SNUMPODTV'
      Size = 50
    end
    object quDocZHSENDNACL: TSmallintField
      FieldName = 'SENDNACL'
    end
    object quDocZHIDHPRO: TIntegerField
      FieldName = 'IDHPRO'
    end
  end
  object quCliSel: TADOQuery
    Connection = msConnection
    CursorType = ctStatic
    CommandTimeout = 1800
    Parameters = <
      item
        Name = 'SGLN'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = ''
      end>
    SQL.Strings = (
      'SELECT TOP 1 [Id]'
      '      ,[INN]'
      '      ,[Name]'
      '      ,[FullName]'
      '      ,[Gln]'
      '     ,[EDIProvider]'
      '    ,[PayNDS]'
      '   ,[Email]'
      ',[IActive]'
      '  FROM [dbo].[Clients]'
      '  where [Gln]=:SGLN')
    Left = 228
    Top = 77
    object quCliSelId: TAutoIncField
      FieldName = 'Id'
      ReadOnly = True
    end
    object quCliSelINN: TStringField
      FieldName = 'INN'
    end
    object quCliSelName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object quCliSelFullName: TStringField
      FieldName = 'FullName'
      Size = 100
    end
    object quCliSelGln: TWideStringField
      FieldName = 'Gln'
    end
    object quCliSelEDIProvider: TIntegerField
      FieldName = 'EDIProvider'
    end
    object quCliSelPayNDS: TBooleanField
      FieldName = 'PayNDS'
    end
    object quCliSelEmail: TWideStringField
      FieldName = 'Email'
      Size = 50
    end
    object quCliSelIActive: TSmallintField
      FieldName = 'IActive'
    end
  end
  object quFindDep: TADOQuery
    Connection = msConnection
    CommandTimeout = 1800
    Parameters = <
      item
        Name = 'SGLN'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = ''
      end>
    SQL.Strings = (
      'SELECT [Id_Shop]'
      '      ,[Id]'
      '      ,[Name]'
      '      ,[FullName]'
      '      ,[IStatus]'
      '      ,[IdOrg]'
      '      ,[GLN]'
      '     ,[Addres]'
      'FROM [Departs]'
      'where [GLN]=:SGLN'
      'and [IStatus]>0'
      '')
    Left = 292
    Top = 77
    object quFindDepId_Shop: TIntegerField
      FieldName = 'Id_Shop'
    end
    object quFindDepId: TIntegerField
      FieldName = 'Id'
    end
    object quFindDepName: TStringField
      FieldName = 'Name'
      Size = 100
    end
    object quFindDepFullName: TStringField
      FieldName = 'FullName'
      Size = 150
    end
    object quFindDepIStatus: TSmallintField
      FieldName = 'IStatus'
    end
    object quFindDepIdOrg: TIntegerField
      FieldName = 'IdOrg'
    end
    object quFindDepGLN: TStringField
      FieldName = 'GLN'
    end
    object quFindDepAddres: TStringField
      FieldName = 'Addres'
      Size = 200
    end
  end
  object quFCardPro: TADOQuery
    Connection = msConnection
    CommandTimeout = 1000
    Parameters = <
      item
        Name = 'ICODE'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT [iCode]'
      '      ,[Kb]'
      '      ,[iCodePro]'
      '      ,[iM]'
      '      ,[Km]'
      '      ,[NamePro]'
      '      ,[SM]'
      '  FROM [dbo].[GoodsPro]'
      '  where iCode=:ICODE')
    Left = 420
    Top = 12
    object quFCardProiCode: TIntegerField
      FieldName = 'iCode'
    end
    object quFCardProKb: TFloatField
      FieldName = 'Kb'
    end
    object quFCardProiCodePro: TIntegerField
      FieldName = 'iCodePro'
    end
    object quFCardProiM: TIntegerField
      FieldName = 'iM'
    end
    object quFCardProKm: TFloatField
      FieldName = 'Km'
    end
    object quFCardProNamePro: TStringField
      FieldName = 'NamePro'
      Size = 100
    end
    object quFCardProSM: TStringField
      FieldName = 'SM'
      Size = 10
    end
  end
end
