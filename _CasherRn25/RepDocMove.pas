unit RepDocMove;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxImageComboBox, SpeedBar,
  cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxClasses, cxControls, cxGridCustomView, cxGrid, cxProgressBar,
  cxContainer, cxTextEdit, cxMemo, ExtCtrls, dxmdaset, Placemnt,
  FIBDatabase, pFIBDatabase, FIBDataSet, pFIBDataSet, dxPSGlbl, dxPSUtl,
  dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
  dxPScxGridLnk, FR_DSet, FR_DBSet, FR_Class;

type
  TfmRepDocMove = class(TForm)
    Panel1: TPanel;
    MemoRDM: TcxMemo;
    GridRDM: TcxGrid;
    ViewRDM: TcxGridDBTableView;
    LevelRDM: TcxGridLevel;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    SpeedItem6: TSpeedItem;
    teRDM: TdxMemData;
    dsteRDM: TDataSource;
    teRDMiType: TSmallintField;
    teRDMsType: TStringField;
    teRDMiTypeD: TSmallintField;
    teRDMsTypeD: TStringField;
    teRDMDateDoc: TDateField;
    teRDMNumDoc: TStringField;
    teRDMiCli: TIntegerField;
    teRDMsCli: TStringField;
    teRDMIdDoc: TIntegerField;
    teRDMiCode: TIntegerField;
    teRDMNameCard: TStringField;
    teRDMiM: TIntegerField;
    teRDMSM: TStringField;
    teRDMQuantIn: TFloatField;
    teRDMQuantOut: TFloatField;
    teRDMPrice: TFloatField;
    teRDMRSum: TFloatField;
    ViewRDMiType: TcxGridDBColumn;
    ViewRDMsType: TcxGridDBColumn;
    ViewRDMiTypeD: TcxGridDBColumn;
    ViewRDMsTypeD: TcxGridDBColumn;
    ViewRDMIdDoc: TcxGridDBColumn;
    ViewRDMDateDoc: TcxGridDBColumn;
    ViewRDMNumDoc: TcxGridDBColumn;
    ViewRDMiCli: TcxGridDBColumn;
    ViewRDMsCli: TcxGridDBColumn;
    ViewRDMiCode: TcxGridDBColumn;
    ViewRDMNameCard: TcxGridDBColumn;
    ViewRDMSM: TcxGridDBColumn;
    ViewRDMQuantIn: TcxGridDBColumn;
    ViewRDMQuantOut: TcxGridDBColumn;
    ViewRDMPrice: TcxGridDBColumn;
    ViewRDMRSum: TcxGridDBColumn;
    FormPlacement1: TFormPlacement;
    teRDMiNum: TIntegerField;
    trSelRep: TpFIBTransaction;
    quDHIn: TpFIBDataSet;
    quDHInID: TFIBIntegerField;
    quDHInDATEDOC: TFIBDateField;
    quDHInNUMDOC: TFIBStringField;
    quDHInIDCLI: TFIBIntegerField;
    quDHInIDSKL: TFIBIntegerField;
    quDHInNAMECL: TFIBStringField;
    quDSIn: TpFIBDataSet;
    quDHVnIn: TpFIBDataSet;
    quDSVnIn: TpFIBDataSet;
    quDHVnInID: TFIBIntegerField;
    quDHVnInDATEDOC: TFIBDateField;
    quDHVnInNUMDOC: TFIBStringField;
    quDHVnInIDSKL_FROM: TFIBIntegerField;
    quDHVnInIDSKL_TO: TFIBIntegerField;
    quDHVnInSUMIN: TFIBFloatField;
    quDHVnInSUMUCH: TFIBFloatField;
    quDHVnInSUMUCH1: TFIBFloatField;
    quDHVnInSUMTAR: TFIBFloatField;
    quDHVnInPROCNAC: TFIBFloatField;
    quDHVnInIACTIVE: TFIBIntegerField;
    quDSInIDHEAD: TFIBIntegerField;
    quDSInID: TFIBIntegerField;
    quDSInNAME: TFIBStringField;
    quDSInNUM: TFIBIntegerField;
    quDSInIDCARD: TFIBIntegerField;
    quDSInQUANT: TFIBFloatField;
    quDSInPRICEIN: TFIBFloatField;
    quDSInSUMIN: TFIBFloatField;
    quDSInPRICEUCH: TFIBFloatField;
    quDSInIDM: TFIBIntegerField;
    quDSInKM: TFIBFloatField;
    quDSInNAMESHORT: TFIBStringField;
    quDHActIn: TpFIBDataSet;
    quDSActIn: TpFIBDataSet;
    quDHActInID: TFIBIntegerField;
    quDHActInDATEDOC: TFIBDateField;
    quDHActInNUMDOC: TFIBStringField;
    quDHActInIDSKL: TFIBIntegerField;
    quDHActInSUMIN: TFIBFloatField;
    quDHActInIACTIVE: TFIBIntegerField;
    quDHActInNAMEMH: TFIBStringField;
    quDSActInIDHEAD: TFIBIntegerField;
    quDSActInID: TFIBIntegerField;
    quDSActInNAME: TFIBStringField;
    quDSActInIDCARD: TFIBIntegerField;
    quDSActInQUANT: TFIBFloatField;
    quDSActInPRICEIN: TFIBFloatField;
    quDSActInSUMIN: TFIBFloatField;
    quDSActInIDM: TFIBIntegerField;
    quDSActInKM: TFIBFloatField;
    quDSActInNAMESHORT: TFIBStringField;
    quDHComplIn: TpFIBDataSet;
    quDSComplIn: TpFIBDataSet;
    quDHComplInID: TFIBIntegerField;
    quDHComplInDATEDOC: TFIBDateField;
    quDHComplInNUMDOC: TFIBStringField;
    quDHComplInIDSKL: TFIBIntegerField;
    quDHComplInSUMIN: TFIBFloatField;
    quDHComplInIACTIVE: TFIBIntegerField;
    quDHComplInNAMEMH: TFIBStringField;
    quDHOut: TpFIBDataSet;
    quDSOut: TpFIBDataSet;
    quDHOutID: TFIBIntegerField;
    quDHOutDATEDOC: TFIBDateField;
    quDHOutNUMDOC: TFIBStringField;
    quDHOutIDCLI: TFIBIntegerField;
    quDHOutIDSKL: TFIBIntegerField;
    quDHOutNAMECL: TFIBStringField;
    quDSOutIDHEAD: TFIBIntegerField;
    quDSOutID: TFIBIntegerField;
    quDSOutNUM: TFIBIntegerField;
    quDSOutNAME: TFIBStringField;
    quDSOutIDCARD: TFIBIntegerField;
    quDSOutQUANT: TFIBFloatField;
    quDSOutPRICEIN: TFIBFloatField;
    quDSOutSUMIN: TFIBFloatField;
    quDSOutIDM: TFIBIntegerField;
    quDSOutNAMESHORT: TFIBStringField;
    quDHROut: TpFIBDataSet;
    quDSROut: TpFIBDataSet;
    quDHROutID: TFIBIntegerField;
    quDHROutDATEDOC: TFIBDateField;
    quDHROutNUMDOC: TFIBStringField;
    quDHROutIDCLI: TFIBIntegerField;
    quDHROutIDSKL: TFIBIntegerField;
    quDHROutNAMECL: TFIBStringField;
    quDHSaleOut: TpFIBDataSet;
    quDSSaleOut: TpFIBDataSet;
    quDHSaleOutID: TFIBIntegerField;
    quDHSaleOutDATEDOC: TFIBDateField;
    quDHSaleOutNUMDOC: TFIBStringField;
    quDHSaleOutIDCLI: TFIBIntegerField;
    quDHSaleOutIDSKL: TFIBIntegerField;
    quDHSaleOutCOMMENT: TFIBStringField;
    quDSROutIDHEAD: TFIBIntegerField;
    quDSROutID: TFIBIntegerField;
    quDSROutNAME: TFIBStringField;
    quDSROutIDCARD: TFIBIntegerField;
    quDSROutQUANT: TFIBFloatField;
    quDSROutPRICEIN: TFIBFloatField;
    quDSROutSUMIN: TFIBFloatField;
    quDSROutIDM: TFIBIntegerField;
    quDSROutNAMESHORT: TFIBStringField;
    teRDMOper: TStringField;
    teRDMComment: TStringField;
    teRDMSGr: TStringField;
    teRDMSSGr: TStringField;
    quDHVnOut: TpFIBDataSet;
    quDSVnOut: TpFIBDataSet;
    quDHActOut: TpFIBDataSet;
    quDSActOut: TpFIBDataSet;
    quDHComplOut: TpFIBDataSet;
    quDSComplOut: TpFIBDataSet;
    quDHVnOutID: TFIBIntegerField;
    quDHVnOutDATEDOC: TFIBDateField;
    quDHVnOutNUMDOC: TFIBStringField;
    quDHVnOutIDSKL_FROM: TFIBIntegerField;
    quDHVnOutIDSKL_TO: TFIBIntegerField;
    quDHVnOutSUMIN: TFIBFloatField;
    quDHVnOutSUMUCH: TFIBFloatField;
    quDHVnOutSUMUCH1: TFIBFloatField;
    quDHVnOutSUMTAR: TFIBFloatField;
    quDHVnOutPROCNAC: TFIBFloatField;
    quDHVnOutIACTIVE: TFIBIntegerField;
    quDSVnOutIDHEAD: TFIBIntegerField;
    quDSVnOutID: TFIBIntegerField;
    quDSVnOutNAME: TFIBStringField;
    quDSVnOutNUM: TFIBIntegerField;
    quDSVnOutIDCARD: TFIBIntegerField;
    quDSVnOutQUANT: TFIBFloatField;
    quDSVnOutPRICEIN: TFIBFloatField;
    quDSVnOutSUMIN: TFIBFloatField;
    quDSVnOutPRICEUCH: TFIBFloatField;
    quDSVnOutSUMUCH: TFIBFloatField;
    quDSVnOutPRICEUCH1: TFIBFloatField;
    quDSVnOutSUMUCH1: TFIBFloatField;
    quDSVnOutIDM: TFIBIntegerField;
    quDSVnOutKM: TFIBFloatField;
    quDSVnOutNAMESHORT: TFIBStringField;
    quDHActOutID: TFIBIntegerField;
    quDHActOutDATEDOC: TFIBDateField;
    quDHActOutNUMDOC: TFIBStringField;
    quDHActOutIDSKL: TFIBIntegerField;
    quDHActOutSUMIN: TFIBFloatField;
    quDHActOutIACTIVE: TFIBIntegerField;
    quDHActOutNAMEMH: TFIBStringField;
    quDSActOutIDHEAD: TFIBIntegerField;
    quDSActOutID: TFIBIntegerField;
    quDSActOutNAME: TFIBStringField;
    quDSActOutIDCARD: TFIBIntegerField;
    quDSActOutQUANT: TFIBFloatField;
    quDSActOutPRICEIN: TFIBFloatField;
    quDSActOutSUMIN: TFIBFloatField;
    quDSActOutIDM: TFIBIntegerField;
    quDSActOutKM: TFIBFloatField;
    quDSActOutNAMESHORT: TFIBStringField;
    quDHComplOutID: TFIBIntegerField;
    quDHComplOutDATEDOC: TFIBDateField;
    quDHComplOutNUMDOC: TFIBStringField;
    quDHComplOutIDSKL: TFIBIntegerField;
    quDHComplOutSUMIN: TFIBFloatField;
    quDHComplOutIACTIVE: TFIBIntegerField;
    quDHComplOutNAMEMH: TFIBStringField;
    quDSComplOutIDHEAD: TFIBIntegerField;
    quDSComplOutID: TFIBIntegerField;
    quDSComplOutNAME: TFIBStringField;
    quDSComplOutIDCARD: TFIBIntegerField;
    quDSComplOutQUANT: TFIBFloatField;
    quDSComplOutPRICEIN: TFIBFloatField;
    quDSComplOutSUMIN: TFIBFloatField;
    quDSComplOutIDM: TFIBIntegerField;
    quDSComplOutKM: TFIBFloatField;
    quDSComplOutNAMESHORT: TFIBStringField;
    quDSComplInIDHEAD: TFIBIntegerField;
    quDSComplInID: TFIBIntegerField;
    quDSComplInNAME: TFIBStringField;
    quDSComplInIDCARD: TFIBIntegerField;
    quDSComplInQUANT: TFIBFloatField;
    quDSComplInPRICEIN: TFIBFloatField;
    quDSComplInSUMIN: TFIBFloatField;
    quDSComplInIDM: TFIBIntegerField;
    quDSComplInKM: TFIBFloatField;
    quDSComplInNAMESHORT: TFIBStringField;
    quDSInPARENT: TFIBIntegerField;
    quDSVnInIDHEAD: TFIBIntegerField;
    quDSVnInID: TFIBIntegerField;
    quDSVnInNAME: TFIBStringField;
    quDSVnInNUM: TFIBIntegerField;
    quDSVnInIDCARD: TFIBIntegerField;
    quDSVnInQUANT: TFIBFloatField;
    quDSVnInPRICEIN: TFIBFloatField;
    quDSVnInSUMIN: TFIBFloatField;
    quDSVnInPRICEUCH: TFIBFloatField;
    quDSVnInSUMUCH: TFIBFloatField;
    quDSVnInPRICEUCH1: TFIBFloatField;
    quDSVnInSUMUCH1: TFIBFloatField;
    quDSVnInIDM: TFIBIntegerField;
    quDSVnInKM: TFIBFloatField;
    quDSVnInNAMESHORT: TFIBStringField;
    quDSVnInPARENT: TFIBIntegerField;
    quDSActInPARENT: TFIBIntegerField;
    quDSComplInPARENT: TFIBIntegerField;
    quDSVnOutPARENT: TFIBIntegerField;
    quDSActOutPARENT: TFIBIntegerField;
    quDSOutPARENT: TFIBIntegerField;
    quDSROutPARENT: TFIBIntegerField;
    quDSComplOutPARENT: TFIBIntegerField;
    quDHSaleOutOPER: TFIBStringField;
    quDHSaleOutNAMEOPER: TFIBStringField;
    quDHSaleOutIDOP: TFIBIntegerField;
    ViewRDMComment: TcxGridDBColumn;
    ViewRDMOper: TcxGridDBColumn;
    ViewRDMSGr: TcxGridDBColumn;
    ViewRDMSSGr: TcxGridDBColumn;
    quDHSaleOutSUMIN: TFIBFloatField;
    frRepRDM: TfrReport;
    frtaRDM: TfrDBDataSet;
    Print2: TdxComponentPrinter;
    Print2Link1: TdxGridReportLink;
    quDHVnInNAMEMHTO: TFIBStringField;
    quDHVnInNAMEMHFROM: TFIBStringField;
    quDHVnOutNAMEMHFROM: TFIBStringField;
    quDHVnOutNAMEMHTO: TFIBStringField;
    quDHInCOMMENT: TFIBStringField;
    quDHComplInNAMEMHTO: TFIBStringField;
    quDHComplOutNAMEMHTO: TFIBStringField;
    quDHComplInIDSKLTO: TFIBIntegerField;
    quDSSaleOutIDHEAD: TFIBIntegerField;
    quDSSaleOutIDB: TFIBIntegerField;
    quDSSaleOutCODEB: TFIBIntegerField;
    quDSSaleOutNAMEB: TFIBStringField;
    quDSSaleOutQUANT: TFIBFloatField;
    quDSSaleOutPARENT: TFIBIntegerField;
    quDSSaleOutIMESSURE: TFIBIntegerField;
    quDSSaleOutNAMESHORT: TFIBStringField;
    quDSSaleOutSUMIN: TFIBFloatField;
    quDSSaleOutSUMIN0: TFIBFloatField;
    procedure FormCreate(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
    procedure SpeedItem4Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure prFormDataRDM;
  end;


var
  fmRepDocMove: TfmRepDocMove;
  iNUmIn,iNumOut:INteger;

implementation

uses Un1, dmOffice, DMOReps;

{$R *.dfm}

procedure TfmRepDocMove.prFormDataRDM;
Var iC:INteger;
    S1,S2:String;
//    rSumRemnB,rSumRemnE,rSumRemnT:Real;
//    iR:INteger;
begin
  fmRepDocMove.MemoRDM.Clear;
  fmRepDocMove.MemoRDM.Lines.Add('����� ... ���� ������������ ������.');
  with dmORep do
  begin
  try
    CloseTe(teRDM);

    ViewRDM.BeginUpdate;

    iNUmIn:=0;
    iNumOut:=0;

    iC:=1;
    {
    if RDM.i6=1 then  //����� ������� �� ������
    begin
      iR:=prTOFindRemB(Trunc(RDM.d1),RDM.MHId,rSumRemnB,rSumRemnT);
      if iR<>0 then rSumRemnB:=0;

      teRDM.Append;
      teRDMiType.AsInteger:=0;
      teRDMsType.AsString:='������� �� ������';
      teRDMiTypeD.AsInteger:=0;
      teRDMsTypeD.AsString:='�������';
      teRDMIdDoc.AsInteger:=0;
      teRDMDateDoc.AsDateTime:=RDM.d1;
      teRDMNumDoc.AsString:='';
      teRDMiCli.AsInteger:=0;
      teRDMsCli.AsString:='';
      teRDMiCode.AsInteger:=0;
      teRDMNameCard.AsString:='';
      teRDMiM.AsInteger:=0;
      teRDMSM.AsString:='';
      teRDMQuantIn.AsFloat:=0;
//          teRDMQuantOut.AsFloat:=0;
      teRDMPrice.AsFloat:=0;
      teRDMRSum.AsFloat:=rSumRemn;
      teRDMiNum.AsInteger:=iC;
      teRDMOper.AsString:='';
      teRDMComment.AsString:='';
      teRDMSGr.AsString:='';
      teRDMSSGr.AsString:='';
      teRDM.Post;

      inc(iC);
    end;      }

    if RDM.i1=1 then //�������
    begin
      fmRepDocMove.MemoRDM.Lines.Add('  �������.');

      quDHIn.Active:=False;
      quDHIn.ParamByName('ISKL').AsInteger:=RDM.MHId;
      quDHIn.ParamByName('DATEB').AsDateTime:=Trunc(RDM.d1);
      quDHIn.ParamByName('DATEE').AsDateTime:=Trunc(RDM.d2)+1;
      quDHIn.Active:=True;

      iNUmIn:=iNUmIn+quDHIn.RecordCount;

      quDHIn.First;
      while not quDHIn.Eof do
      begin
        quDSIn.Active:=False;
        quDSIn.ParamByName('IDH').AsInteger:=quDHInID.AsInteger;
        quDSIn.Active:=True;
        quDSIn.First;
        while not quDSIn.Eof do
        begin
          prFind2group(quDSInPARENT.AsInteger,S1,S2);

          teRDM.Append;
          teRDMiType.AsInteger:=1;
          teRDMsType.AsString:='������';
          teRDMiTypeD.AsInteger:=1;
          teRDMsTypeD.AsString:='�����������';
          teRDMIdDoc.AsInteger:=quDHInID.AsInteger;
          teRDMDateDoc.AsDateTime:=quDHInDATEDOC.AsDateTime;
          teRDMNumDoc.AsString:=quDHInNUMDOC.AsString;
          teRDMiCli.AsInteger:=quDHInIDCLI.AsInteger;
          teRDMsCli.AsString:=quDHInNAMECL.AsString;
          teRDMiCode.AsInteger:=quDSInIDCARD.AsInteger;
          teRDMNameCard.AsString:=quDSInNAME.AsString;
          teRDMiM.AsInteger:=quDSInIDM.AsInteger;
          teRDMSM.AsString:=quDSInNAMESHORT.AsString;
          teRDMQuantIn.AsFloat:=quDSInQUANT.AsFloat;
//          teRDMQuantOut.AsFloat:=0;
          teRDMPrice.AsFloat:=quDSInPRICEIN.AsFloat;
          teRDMRSum.AsFloat:=quDSInSUMIN.AsFloat;
          teRDMiNum.AsInteger:=iC;
          teRDMOper.AsString:='�����������';
          teRDMComment.AsString:=quDHInCOMMENT.AsString;
          teRDMSGr.AsString:=S1;
          teRDMSSGr.AsString:=S2;
          teRDM.Post;

          quDSIn.Next; inc(iC);
        end;
        quDSIn.Active:=False;

        quDHIn.Next;
      end;

      quDHIn.Active:=False;
    end;

    //������

    if RDM.i5=1 then //���������� ���������
    begin
      fmRepDocMove.MemoRDM.Lines.Add('  ��. ������');

      quDHVnIn.Active:=False;
      quDHVnIn.ParamByName('ISKL').AsInteger:=RDM.MHId;
      quDHVnIn.ParamByName('DATEB').AsDateTime:=Trunc(RDM.d1);
      quDHVnIn.ParamByName('DATEE').AsDateTime:=Trunc(RDM.d2)+1;
      quDHVnIn.Active:=True;

      iNUmIn:=iNUmIn+quDHVnIn.RecordCount;

      quDHVnIn.First;
      while not quDHVnIn.Eof do
      begin
        quDSVnIn.Active:=False;
        quDSVnIn.ParamByName('IDH').AsInteger:=quDHVnInID.AsInteger;
        quDSVnIn.Active:=True;
        quDSVnIn.First;
        while not quDSVnIn.Eof do
        begin
          prFind2group(quDSVnInPARENT.AsInteger,S1,S2);

          teRDM.Append;
          teRDMiType.AsInteger:=1;
          teRDMsType.AsString:='������';
          teRDMiTypeD.AsInteger:=4;
          teRDMsTypeD.AsString:='��. ������';
          teRDMIdDoc.AsInteger:=quDHVnInID.AsInteger;
          teRDMDateDoc.AsDateTime:=quDHVnInDATEDOC.AsDateTime;
          teRDMNumDoc.AsString:=quDHVnInNUMDOC.AsString;
          teRDMiCli.AsInteger:=quDHVnInIDSKL_FROM.AsInteger;
          teRDMsCli.AsString:=quDHVnInNAMEMHFROM.AsString+' -> '+quDHVnInNAMEMHTO.AsString;
          teRDMiCode.AsInteger:=quDSVnInIDCARD.AsInteger;
          teRDMNameCard.AsString:=quDSVnInNAME.AsString;
          teRDMiM.AsInteger:=quDSVnInIDM.AsInteger;
          teRDMSM.AsString:=quDSVnInNAMESHORT.AsString;
          teRDMQuantIn.AsFloat:=quDSVnInQUANT.AsFloat;
//          teRDMQuantOut.AsFloat:=0;
          teRDMPrice.AsFloat:=quDSVnInPRICEIN.AsFloat;
          teRDMRSum.AsFloat:=quDSVnInSUMIN.AsFloat;
          teRDMiNum.AsInteger:=iC;
          teRDMOper.AsString:='��. ������';
          teRDMComment.AsString:='';
          teRDMSGr.AsString:=S1;
          teRDMSSGr.AsString:=S2;
          teRDM.Post;

          quDSVnIn.Next; inc(iC);
        end;
        quDSVnIn.Active:=False;

        quDHVnIn.Next;
      end;
      quDHVnIn.Active:=False;

      fmRepDocMove.MemoRDM.Lines.Add('  ���. ����������� ������');

      quDHActIn.Active:=False;
      quDHActIn.ParamByName('ISKL').AsInteger:=RDM.MHId;
      quDHActIn.ParamByName('DATEB').AsDateTime:=Trunc(RDM.d1);
      quDHActIn.ParamByName('DATEE').AsDateTime:=Trunc(RDM.d2)+1;
      quDHActIn.Active:=True;

      iNUmIn:=iNUmIn+quDHActIn.RecordCount;

      quDHActIn.First;
      while not quDHActIn.Eof do
      begin
        quDSActIn.Active:=False;
        quDSActIn.ParamByName('IDH').AsInteger:=quDHActInID.AsInteger;
        quDSActIn.Active:=True;
        quDSActIn.First;
        while not quDSActIn.Eof do
        begin
          prFind2group(quDSActInPARENT.AsInteger,S1,S2);

          teRDM.Append;
          teRDMiType.AsInteger:=1;
          teRDMsType.AsString:='������';
          teRDMiTypeD.AsInteger:=5;
          teRDMsTypeD.AsString:='��� ����������� ������';
          teRDMIdDoc.AsInteger:=quDHActInID.AsInteger;
          teRDMDateDoc.AsDateTime:=quDHActInDATEDOC.AsDateTime;
          teRDMNumDoc.AsString:=quDHActInNUMDOC.AsString;
          teRDMiCli.AsInteger:=quDHActInIDSKL.AsInteger;
          teRDMsCli.AsString:=quDHActInNAMEMH.AsString;
          teRDMiCode.AsInteger:=quDSActInIDCARD.AsInteger;
          teRDMNameCard.AsString:=quDSActInNAME.AsString;
          teRDMiM.AsInteger:=quDSActInIDM.AsInteger;
          teRDMSM.AsString:=quDSActInNAMESHORT.AsString;
          teRDMQuantIn.AsFloat:=quDSActInQUANT.AsFloat;
//          teRDMQuantOut.AsFloat:=0;
          teRDMPrice.AsFloat:=quDSActInPRICEIN.AsFloat;
          teRDMRSum.AsFloat:=quDSActInSUMIN.AsFloat;
          teRDMiNum.AsInteger:=iC;
          teRDMOper.AsString:='��� ����������� ������';
          teRDMComment.AsString:='';
          teRDMSGr.AsString:=S1;
          teRDMSSGr.AsString:=S2;
          teRDM.Post;

          quDSActIn.Next; inc(iC);
        end;
        quDSActIn.Active:=False;

        quDHActIn.Next;
      end;
      quDHActIn.Active:=False;

      fmRepDocMove.MemoRDM.Lines.Add('  ������������ ������');

      quDHComplIn.Active:=False;
      quDHComplIn.ParamByName('ISKL').AsInteger:=RDM.MHId;
      quDHComplIn.ParamByName('DATEB').AsDateTime:=Trunc(RDM.d1);
      quDHComplIn.ParamByName('DATEE').AsDateTime:=Trunc(RDM.d2)+1;
      quDHComplIn.Active:=True;

      iNUmIn:=iNUmIn+quDHComplIn.RecordCount;

      quDHComplIn.First;
      while not quDHComplIn.Eof do
      begin
        quDSComplIn.Active:=False;
        quDSComplIn.ParamByName('IDH').AsInteger:=quDHComplInID.AsInteger;
        quDSComplIn.Active:=True;
        quDSComplIn.First;
        while not quDSComplIn.Eof do
        begin
          prFind2group(quDSComplInPARENT.AsInteger,S1,S2);

          teRDM.Append;
          teRDMiType.AsInteger:=1;
          teRDMsType.AsString:='������';
          teRDMiTypeD.AsInteger:=6;
          teRDMsTypeD.AsString:='������������ ������';
          teRDMIdDoc.AsInteger:=quDHComplInID.AsInteger;
          teRDMDateDoc.AsDateTime:=quDHComplInDATEDOC.AsDateTime;
          teRDMNumDoc.AsString:=quDHComplInNUMDOC.AsString;
          teRDMiCli.AsInteger:=quDHComplInIDSKLTO.AsInteger;
          teRDMsCli.AsString:=quDHComplInNAMEMH.AsString+' -> '+quDHComplInNAMEMHTO.AsString;
          teRDMiCode.AsInteger:=quDSComplInIDCARD.AsInteger;
          teRDMNameCard.AsString:=quDSComplInNAME.AsString;
          teRDMiM.AsInteger:=quDSComplInIDM.AsInteger;
          teRDMSM.AsString:=quDSComplInNAMESHORT.AsString;
          teRDMQuantIn.AsFloat:=quDSComplInQUANT.AsFloat;
//          teRDMQuantOut.AsFloat:=0;
          teRDMPrice.AsFloat:=quDSComplInPRICEIN.AsFloat;
          teRDMRSum.AsFloat:=quDSComplInSUMIN.AsFloat;
          teRDMiNum.AsInteger:=iC;

          teRDMOper.AsString:='������������ ������';
          teRDMComment.AsString:='';
          teRDMSGr.AsString:=S1;
          teRDMSSGr.AsString:=S2;

          teRDM.Post;

          quDSComplIn.Next; inc(iC);
        end;
        quDSComplIn.Active:=False;

        quDHComplIn.Next;
      end;
      quDHComplIn.Active:=False;
    end;

    if RDM.i2=1 then //�������
    begin
      fmRepDocMove.MemoRDM.Lines.Add('  �������');

      quDHOut.Active:=False;
      quDHOut.ParamByName('ISKL').AsInteger:=RDM.MHId;
      quDHOut.ParamByName('DATEB').AsDateTime:=Trunc(RDM.d1);
      quDHOut.ParamByName('DATEE').AsDateTime:=Trunc(RDM.d2)+1;
      quDHOut.Active:=True;

      iNumOut:=iNumOut+quDHOut.RecordCount;

      quDHOut.First;
      while not quDHOut.Eof do
      begin
        quDSOut.Active:=False;
        quDSOut.ParamByName('IDH').AsInteger:=quDHOutID.AsInteger;
        quDSOut.Active:=True;
        quDSOut.First;
        while not quDSOut.Eof do
        begin
          prFind2group(quDSOutPARENT.AsInteger,S1,S2);

          teRDM.Append;
          teRDMiType.AsInteger:=2;
          teRDMsType.AsString:='������';
          teRDMiTypeD.AsInteger:=7;
          teRDMsTypeD.AsString:='�������';
          teRDMIdDoc.AsInteger:=quDHOutID.AsInteger;
          teRDMDateDoc.AsDateTime:=quDHOutDATEDOC.AsDateTime;
          teRDMNumDoc.AsString:=quDHOutNUMDOC.AsString;
          teRDMiCli.AsInteger:=quDHOutIDCLI.AsInteger;
          teRDMsCli.AsString:=quDHOutNAMECL.AsString;
          teRDMiCode.AsInteger:=quDSOutIDCARD.AsInteger;
          teRDMNameCard.AsString:=quDSOutNAME.AsString;
          teRDMiM.AsInteger:=quDSOutIDM.AsInteger;
          teRDMSM.AsString:=quDSOutNAMESHORT.AsString;
//          teRDMQuantIn.AsFloat:=0;
          teRDMQuantOut.AsFloat:=quDSOutQUANT.AsFloat;
          teRDMPrice.AsFloat:=quDSOutPRICEIN.AsFloat;
          teRDMRSum.AsFloat:=quDSOutSUMIN.AsFloat;
          teRDMiNum.AsInteger:=iC;
          teRDMOper.AsString:='�������';
          teRDMComment.AsString:='';
          teRDMSGr.AsString:=S1;
          teRDMSSGr.AsString:=S2;
          teRDM.Post;

          quDSOut.Next; inc(iC);
        end;
        quDSOut.Active:=False;

        quDHOut.Next;
      end;

      quDHOut.Active:=False;
    end;

    if RDM.i3=1 then //���������� �� �������
    begin
      fmRepDocMove.MemoRDM.Lines.Add('  ���������� �� �������');

      quDHROut.Active:=False;
      quDHROut.ParamByName('ISKL').AsInteger:=RDM.MHId;
      quDHROut.ParamByName('DATEB').AsDateTime:=Trunc(RDM.d1);
      quDHROut.ParamByName('DATEE').AsDateTime:=Trunc(RDM.d2)+1;
      quDHROut.Active:=True;

      iNumOut:=iNumOut+quDHROut.RecordCount;

      quDHROut.First;
      while not quDHROut.Eof do
      begin
        quDSROut.Active:=False;
        quDSROut.ParamByName('IDH').AsInteger:=quDHROutID.AsInteger;
        quDSROut.Active:=True;
        quDSROut.First;
        while not quDSROut.Eof do
        begin
          prFind2group(quDSROutPARENT.AsInteger,S1,S2);

          teRDM.Append;
          teRDMiType.AsInteger:=2;
          teRDMsType.AsString:='������';
          teRDMiTypeD.AsInteger:=8;
          teRDMsTypeD.AsString:='���������� �� �������';
          teRDMIdDoc.AsInteger:=quDHROutID.AsInteger;
          teRDMDateDoc.AsDateTime:=quDHROutDATEDOC.AsDateTime;
          teRDMNumDoc.AsString:=quDHROutNUMDOC.AsString;
          teRDMiCli.AsInteger:=quDHROutIDCLI.AsInteger;
          teRDMsCli.AsString:=quDHROutNAMECL.AsString;
          teRDMiCode.AsInteger:=quDSROutIDCARD.AsInteger;
          teRDMNameCard.AsString:=quDSROutNAME.AsString;
          teRDMiM.AsInteger:=quDSROutIDM.AsInteger;
          teRDMSM.AsString:=quDSROutNAMESHORT.AsString;
//          teRDMQuantIn.AsFloat:=0;
          teRDMQuantOut.AsFloat:=quDSROutQUANT.AsFloat;
          teRDMPrice.AsFloat:=quDSROutPRICEIN.AsFloat;
          teRDMRSum.AsFloat:=quDSROutSUMIN.AsFloat;
          teRDMiNum.AsInteger:=iC;
          teRDMOper.AsString:='���������� �� �������';
          teRDMComment.AsString:='';
          teRDMSGr.AsString:=S1;
          teRDMSSGr.AsString:=S2;
          teRDM.Post;

          quDSROut.Next; inc(iC);
        end;
        quDSROut.Active:=False;

        quDHROut.Next;
      end;

      quDHROut.Active:=False;
    end;

    //������
    if RDM.i5=1 then //���������� ���������
    begin
      fmRepDocMove.MemoRDM.Lines.Add('  ��. ������');

      quDHVnOut.Active:=False;
      quDHVnOut.ParamByName('ISKL').AsInteger:=RDM.MHId;
      quDHVnOut.ParamByName('DATEB').AsDateTime:=Trunc(RDM.d1);
      quDHVnOut.ParamByName('DATEE').AsDateTime:=Trunc(RDM.d2)+1;
      quDHVnOut.Active:=True;

      iNumOut:=iNumOut+quDHVnOut.RecordCount;

      quDHVnOut.First;
      while not quDHVnOut.Eof do
      begin
        quDSVnOut.Active:=False;
        quDSVnOut.ParamByName('IDH').AsInteger:=quDHVnOutID.AsInteger;
        quDSVnOut.Active:=True;
        quDSVnOut.First;
        while not quDSVnOut.Eof do
        begin
          prFind2group(quDSVnOutPARENT.AsInteger,S1,S2);

          teRDM.Append;
          teRDMiType.AsInteger:=2;
          teRDMsType.AsString:='������';
          teRDMiTypeD.AsInteger:=4;
          teRDMsTypeD.AsString:='��. ������';
          teRDMIdDoc.AsInteger:=quDHVnOutID.AsInteger;
          teRDMDateDoc.AsDateTime:=quDHVnOutDATEDOC.AsDateTime;
          teRDMNumDoc.AsString:=quDHVnOutNUMDOC.AsString;
          teRDMiCli.AsInteger:=quDHVnOutIDSKL_FROM.AsInteger;
          teRDMsCli.AsString:=quDHVnOutNAMEMHFROM.AsString+' -> '+quDHVnOutNAMEMHTO.AsString;
          teRDMiCode.AsInteger:=quDSVnOutIDCARD.AsInteger;
          teRDMNameCard.AsString:=quDSVnOutNAME.AsString;
          teRDMiM.AsInteger:=quDSVnOutIDM.AsInteger;
          teRDMSM.AsString:=quDSVnOutNAMESHORT.AsString;
//          teRDMQuantIn.AsFloat:=0;
          teRDMQuantOut.AsFloat:=quDSVnOutQUANT.AsFloat;
          teRDMPrice.AsFloat:=quDSVnOutPRICEIN.AsFloat;
          teRDMRSum.AsFloat:=quDSVnOutSUMIN.AsFloat;
          teRDMiNum.AsInteger:=iC;
          teRDMOper.AsString:='��. ������';
          teRDMComment.AsString:='';
          teRDMSGr.AsString:=S1;
          teRDMSSGr.AsString:=S2;
          teRDM.Post;

          quDSVnOut.Next; inc(iC);
        end;
        quDSVnOut.Active:=False;

        quDHVnOut.Next;
      end;
      quDHVnOut.Active:=False;


      fmRepDocMove.MemoRDM.Lines.Add('  ���. ����������� ������');

      quDHActOut.Active:=False;
      quDHActOut.ParamByName('ISKL').AsInteger:=RDM.MHId;
      quDHActOut.ParamByName('DATEB').AsDateTime:=Trunc(RDM.d1);
      quDHActOut.ParamByName('DATEE').AsDateTime:=Trunc(RDM.d2)+1;
      quDHActOut.Active:=True;

      iNumOut:=iNumOut+quDHActOut.RecordCount;

      quDHActOut.First;
      while not quDHActOut.Eof do
      begin
        quDSActOut.Active:=False;
        quDSActOut.ParamByName('IDH').AsInteger:=quDHActOutID.AsInteger;
        quDSActOut.Active:=True;
        quDSActOut.First;
        while not quDSActOut.Eof do
        begin
          prFind2group(quDSActOutPARENT.AsInteger,S1,S2);

          teRDM.Append;
          teRDMiType.AsInteger:=2;
          teRDMsType.AsString:='������';
          teRDMiTypeD.AsInteger:=5;
          teRDMsTypeD.AsString:='��� ����������� ������';
          teRDMIdDoc.AsInteger:=quDHActOutID.AsInteger;
          teRDMDateDoc.AsDateTime:=quDHActOutDATEDOC.AsDateTime;
          teRDMNumDoc.AsString:=quDHActOutNUMDOC.AsString;
          teRDMiCli.AsInteger:=quDHActOutIDSKL.AsInteger;
          teRDMsCli.AsString:=quDHActOutNAMEMH.AsString;
          teRDMiCode.AsInteger:=quDSActOutIDCARD.AsInteger;
          teRDMNameCard.AsString:=quDSActOutNAME.AsString;
          teRDMiM.AsInteger:=quDSActOutIDM.AsInteger;
          teRDMSM.AsString:=quDSActOutNAMESHORT.AsString;
//          teRDMQuantIn.AsFloat:=0;
          teRDMQuantOut.AsFloat:=quDSActOutQUANT.AsFloat;
          teRDMPrice.AsFloat:=quDSActOutPRICEIN.AsFloat;
          teRDMRSum.AsFloat:=quDSActOutSUMIN.AsFloat;
          teRDMiNum.AsInteger:=iC;
          teRDMOper.AsString:='��� ����������� ������';
          teRDMComment.AsString:='';
          teRDMSGr.AsString:=S1;
          teRDMSSGr.AsString:=S2;
          teRDM.Post;

          quDSActOut.Next; inc(iC);
        end;
        quDSActOut.Active:=False;

        quDHActOut.Next;
      end;
      quDHActOut.Active:=False;

      fmRepDocMove.MemoRDM.Lines.Add('  ������������ ������');

      quDHComplOut.Active:=False;
      quDHComplOut.ParamByName('ISKL').AsInteger:=RDM.MHId;
      quDHComplOut.ParamByName('DATEB').AsDateTime:=Trunc(RDM.d1);
      quDHComplOut.ParamByName('DATEE').AsDateTime:=Trunc(RDM.d2)+1;
      quDHComplOut.Active:=True;

      iNumOut:=iNumOut+quDHComplOut.RecordCount;

      quDHComplOut.First;
      while not quDHComplOut.Eof do
      begin
        quDSComplOut.Active:=False;
        quDSComplOut.ParamByName('IDH').AsInteger:=quDHComplOutID.AsInteger;
        quDSComplOut.Active:=True;
        quDSComplOut.First;
        while not quDSComplOut.Eof do
        begin
          prFind2group(quDSComplOutPARENT.AsInteger,S1,S2);

          teRDM.Append;
          teRDMiType.AsInteger:=2;
          teRDMsType.AsString:='������';
          teRDMiTypeD.AsInteger:=6;
          teRDMsTypeD.AsString:='������������ ������';
          teRDMIdDoc.AsInteger:=quDHComplOutID.AsInteger;
          teRDMDateDoc.AsDateTime:=quDHComplOutDATEDOC.AsDateTime;
          teRDMNumDoc.AsString:=quDHComplOutNUMDOC.AsString;
          teRDMiCli.AsInteger:=quDHComplOutIDSKL.AsInteger;
          teRDMsCli.AsString:=quDHComplOutNAMEMH.AsString+' -> '+quDHComplOutNAMEMHTO.AsString;
          teRDMiCode.AsInteger:=quDSComplOutIDCARD.AsInteger;
          teRDMNameCard.AsString:=quDSComplOutNAME.AsString;
          teRDMiM.AsInteger:=quDSComplOutIDM.AsInteger;
          teRDMSM.AsString:=quDSComplOutNAMESHORT.AsString;
//          teRDMQuantIn.AsFloat:=0;
          teRDMQuantOut.AsFloat:=quDSComplOutQUANT.AsFloat;
          teRDMPrice.AsFloat:=quDSComplOutPRICEIN.AsFloat;
          teRDMRSum.AsFloat:=quDSComplOutSUMIN.AsFloat;
          teRDMiNum.AsInteger:=iC;
          teRDMOper.AsString:='������������ ������';
          teRDMComment.AsString:='';
          teRDMSGr.AsString:=S1;
          teRDMSSGr.AsString:=S2;

          teRDM.Post;

          quDSComplOut.Next; inc(iC);
        end;
        quDSComplOut.Active:=False;

        quDHComplOut.Next;
      end;
      quDHComplOut.Active:=False;

    end;


    if RDM.i4=1 then //���������� �� �����
    begin
      fmRepDocMove.MemoRDM.Lines.Add('  ���������� �� �����');

      quDHSaleOut.Active:=False;
      quDHSaleOut.ParamByName('ISKL').AsInteger:=RDM.MHId;
      quDHSaleOut.ParamByName('DATEB').AsDateTime:=Trunc(RDM.d1);
      quDHSaleOut.ParamByName('DATEE').AsDateTime:=Trunc(RDM.d2)+1;
      quDHSaleOut.Active:=True;

      iNumOut:=iNumOut+quDHSaleOut.RecordCount;

      quDHSaleOut.First;
      while not quDHSaleOut.Eof do
      begin
        if quDHSaleOutIDOP.AsInteger>=0 then
        begin
          quDSSaleOut.Active:=False;
          quDSSaleOut.ParamByName('IDH').AsInteger:=quDHSaleOutID.AsInteger;
          quDSSaleOut.Active:=True;
          quDSSaleOut.First;
          while not quDSSaleOut.Eof do
          begin
            prFind2group(quDSSaleOutPARENT.AsInteger,S1,S2);

            teRDM.Append;
            teRDMiType.AsInteger:=2;
            teRDMsType.AsString:='������';
            teRDMiTypeD.AsInteger:=2;
            teRDMsTypeD.AsString:='���������� �� �����';
            teRDMIdDoc.AsInteger:=quDHSaleOutID.AsInteger;
            teRDMDateDoc.AsDateTime:=quDHSaleOutDATEDOC.AsDateTime;
            teRDMNumDoc.AsString:=quDHSaleOutNUMDOC.AsString;
            teRDMiCli.AsInteger:=quDHSaleOutIDCLI.AsInteger;
            teRDMsCli.AsString:=quDHSaleOutCOMMENT.AsString;
            teRDMiCode.AsInteger:=quDSSaleOutCODEB.AsInteger;
            teRDMNameCard.AsString:=quDSSaleOutNAMEB.AsString;
            teRDMiM.AsInteger:=quDSSaleOutIMESSURE.AsInteger;
            teRDMSM.AsString:=quDSSaleOutNAMESHORT.AsString;
//            teRDMQuantIn.AsFloat:=0;
            teRDMQuantOut.AsFloat:=quDSSaleOutQUANT.AsFloat;
            teRDMPrice.AsFloat:=rv(quDSSaleOutSUMIN.AsFloat/quDSSaleOutQUANT.AsFloat);
            teRDMRSum.AsFloat:=quDSSaleOutSUMIN.AsFloat;
            teRDMiNum.AsInteger:=iC;
            teRDMOper.AsString:=quDHSaleOutNAMEOPER.AsString;
            teRDMComment.AsString:='';
            teRDMSGr.AsString:=S1;
            teRDMSSGr.AsString:=S2;
            teRDM.Post;

            quDSSaleOut.Next; inc(iC);
          end;
          quDSSaleOut.Active:=False;

        end else
        begin
          teRDM.Append;
          teRDMiType.AsInteger:=2;
          teRDMsType.AsString:='������';
          teRDMiTypeD.AsInteger:=2;
          teRDMsTypeD.AsString:='���������� �� �����';
          teRDMIdDoc.AsInteger:=quDHSaleOutID.AsInteger;
          teRDMDateDoc.AsDateTime:=quDHSaleOutDATEDOC.AsDateTime;
          teRDMNumDoc.AsString:=quDHSaleOutNUMDOC.AsString;
          teRDMiCli.AsInteger:=quDHSaleOutIDCLI.AsInteger;
          teRDMsCli.AsString:=quDHSaleOutCOMMENT.AsString;
//          teRDMiCode.AsInteger:=0;
          teRDMNameCard.AsString:='';
//          teRDMiM.AsInteger:=0;
          teRDMSM.AsString:='';
//          teRDMQuantIn.AsFloat:=0;
//          teRDMQuantOut.AsFloat:=0;
//          teRDMPrice.AsFloat:=0;
          teRDMRSum.AsFloat:=quDHSaleOutSUMIN.AsFloat;
          teRDMiNum.AsInteger:=iC;
          teRDMOper.AsString:=quDHSaleOutNAMEOPER.AsString;
          teRDMComment.AsString:=quDHSaleOutCOMMENT.AsString;
          teRDMSGr.AsString:='';
          teRDMSSGr.AsString:='';
          teRDM.Post;

          inc(iC);
        end;

        quDHSaleOut.Next;
      end;

      quDHSaleOut.Active:=False;
    end;
{
    if RDM.i6=1 then  //����� ������� �� ������
    begin
      iR:=prTOFindRemB(Trunc(RDM.d2)+1,RDM.MHId,rSumRemnE,rSumRemnT); //�� ����� ���-��
      if iR<>0 then rSumRemnE:=0;

      teRDM.Append;
      teRDMiType.AsInteger:=3;
      teRDMsType.AsString:='������� �� �����';
      teRDMiTypeD.AsInteger:=0;
      teRDMsTypeD.AsString:='�������';
      teRDMIdDoc.AsInteger:=0;
      teRDMDateDoc.AsDateTime:=RDM.d2;
      teRDMNumDoc.AsString:='';
      teRDMiCli.AsInteger:=0;
      teRDMsCli.AsString:='';
      teRDMiCode.AsInteger:=0;
      teRDMNameCard.AsString:='';
      teRDMiM.AsInteger:=0;
      teRDMSM.AsString:='';
      teRDMQuantIn.AsFloat:=0;
//          teRDMQuantOut.AsFloat:=0;
      teRDMPrice.AsFloat:=0;
      teRDMRSum.AsFloat:=rSumRemn;
      teRDMiNum.AsInteger:=iC;
      teRDMOper.AsString:='';
      teRDMComment.AsString:='';
      teRDMSGr.AsString:='';
      teRDMSSGr.AsString:='';
      teRDM.Post;
 //      inc(iC);
    end;         }


  finally
    ViewRDM.EndUpdate;
  end;

  end;
  fmRepDocMove.MemoRDM.Lines.Add('������������ ��.');
end;

procedure TfmRepDocMove.FormCreate(Sender: TObject);
begin
  GridRDM.Align:=AlClient;
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
end;

procedure TfmRepDocMove.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmRepDocMove.SpeedItem3Click(Sender: TObject);
begin
  prNExportExel5(ViewRDM);
end;

procedure TfmRepDocMove.SpeedItem4Click(Sender: TObject);
Var rSumRemnB,rSumRemnE,rSumRemnT:Real;
    iR:INteger;
begin
  //������ ������//������

  ViewRDM.BeginUpdate;

  frRepRDM.LoadFromFile(CurDir + 'ReestrDocs.frf');

  if RDM.i6=1 then  //����� ������� �� ������
  begin
    iR:=prTOFindRemB(Trunc(RDM.d1),RDM.MHId,rSumRemnB,rSumRemnT);
    if iR<>0 then rSumRemnB:=0;

    iR:=prTOFindRemB(Trunc(RDM.d2)+1,RDM.MHId,rSumRemnE,rSumRemnT);
    if iR<>0 then rSumRemnE:=0;

    frVariables.Variable['SBEG']:='������� �� ������';
    frVariables.Variable['SEND']:='������� �� �����';
    frVariables.Variable['SUMB']:=rSumRemnB;
    frVariables.Variable['SUME']:=rSumRemnE;

  end else
  begin
    frVariables.Variable['SBEG']:='';
    frVariables.Variable['SEND']:='';
    frVariables.Variable['SUMB']:='';
    frVariables.Variable['SUME']:='';
  end;

  frVariables.Variable['SPER']:=' � '+FormatDateTiMe('dd.mm.yyyy',RDM.d1)+' �� '+FormatDateTiMe('dd.mm.yyyy',RDM.d2);
  frVariables.Variable['SMH']:=RDM.sMH;
  frVariables.Variable['NUMIN']:=iNumIn;
  frVariables.Variable['NUMOUT']:=iNumOut;

//  frVariables.Variable['DocPars']:=StrWk;

  frRepRDM.ReportName:='������ ��������� � ��������� ����������.';
  frRepRDM.PrepareReport;
  frRepRDM.ShowPreparedReport;
  ViewRDM.EndUpdate;
end;

end.
