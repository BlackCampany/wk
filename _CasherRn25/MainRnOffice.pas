unit MainRnOffice;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ToolWin, ActnMan, ActnCtrls, ActnMenus,
  ActnList, ExtCtrls, SpeedBar, XPStyleActnCtrls, StdCtrls, pFIBDataSet, Menus, FR_Class;

type
  TfmMainRnOffice = class(TForm)
    StatusBar1: TStatusBar;
    am1: TActionManager;
    ActionMainMenuBar1: TActionMainMenuBar;
    acExit: TAction;
    acGoods: TAction;
    acModify: TAction;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    acCateg: TAction;
    SpeedItem5: TSpeedItem;
    acMessure: TAction;
    acPriceType: TAction;
    acMHP: TAction;
    acClients: TAction;
    SpeedItem2: TSpeedItem;
    acDocIn: TAction;
    SpeedItem3: TSpeedItem;
    acMove: TAction;
    acOut: TAction;
    acRemn: TAction;
    acInv: TAction;
    acMenu: TAction;
    acMoveDate: TAction;
    acOutB: TAction;
    SpeedItem4: TSpeedItem;
    SpeedItem6: TSpeedItem;
    acExportBuh: TAction;
    acTovRep: TAction;
    SpeedItem7: TSpeedItem;
    acRepPrib: TAction;
    acObVed: TAction;
    acKomplekt: TAction;
    acInDocs: TAction;
    SpeedItem8: TSpeedItem;
    SpeedItem9: TSpeedItem;
    acActs: TAction;
    SpeedItem10: TSpeedItem;
    amOperT: TAction;
    SpeedItem11: TSpeedItem;
    acReal: TAction;
    acRecalcPer: TAction;
    acRecalcReal: TAction;
    acRepPost: TAction;
    Action1: TAction;
    acPartRemn: TAction;
    acAvans: TAction;
    TimerClose: TTimer;
    acBGU: TAction;
    acTermoObr: TAction;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    acCategR: TAction;
    acRemnSpeedReal: TAction;
    acImp1CDocs: TAction;
    acClassAlg: TAction;
    acMakers: TAction;
    acAlcogol: TAction;
    acAlcoDecl: TAction;
    acChangeSS: TAction;
    acUpolnLica: TAction;
    acDocsPrice: TAction;
    acRepDocMove: TAction;
    acRealAkciz: TAction;
    acDrv: TAction;
    acSelTermoPr: TAction;
    acChange: TAction;
    procedure FormCanResize(Sender: TObject; var NewWidth,
      NewHeight: Integer; var Resize: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure acMessureExecute(Sender: TObject);
    procedure acGoodsExecute(Sender: TObject);
    procedure acPriceTypeExecute(Sender: TObject);
    procedure acMHPExecute(Sender: TObject);
    procedure acClientsExecute(Sender: TObject);
    procedure acDocInExecute(Sender: TObject);
    procedure acMoveExecute(Sender: TObject);
    procedure acOutExecute(Sender: TObject);
    procedure acRemnExecute(Sender: TObject);
    procedure acInvExecute(Sender: TObject);
    procedure acMenuExecute(Sender: TObject);
    procedure acMoveDateExecute(Sender: TObject);
    procedure acOutBExecute(Sender: TObject);
    procedure acExportBuhExecute(Sender: TObject);
    procedure acTovRepExecute(Sender: TObject);
    procedure acRepPribExecute(Sender: TObject);
    procedure acObVedExecute(Sender: TObject);
    procedure acKomplektExecute(Sender: TObject);
    procedure acInDocsExecute(Sender: TObject);
    procedure acActsExecute(Sender: TObject);
    procedure amOperTExecute(Sender: TObject);
    procedure acRealExecute(Sender: TObject);
    procedure acRecalcPerExecute(Sender: TObject);
    procedure acRecalcRealExecute(Sender: TObject);
    procedure acRepPostExecute(Sender: TObject);
    procedure Action1Execute(Sender: TObject);
    procedure acPartRemnExecute(Sender: TObject);
    procedure acAvansExecute(Sender: TObject);
    procedure TimerCloseTimer(Sender: TObject);
    procedure acBGUExecute(Sender: TObject);
    procedure acTermoObrExecute(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure acCategRExecute(Sender: TObject);
    procedure acRemnSpeedRealExecute(Sender: TObject);
    procedure acImp1CDocsExecute(Sender: TObject);
    procedure acClassAlgExecute(Sender: TObject);
    procedure acMakersExecute(Sender: TObject);
    procedure acAlcogolExecute(Sender: TObject);
    procedure acAlcoDeclExecute(Sender: TObject);
    procedure acChangeSSExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acUpolnLicaExecute(Sender: TObject);
    procedure acDocsPriceExecute(Sender: TObject);
    procedure acRepDocMoveExecute(Sender: TObject);
    procedure acRealAkcizExecute(Sender: TObject);
    procedure acDrvExecute(Sender: TObject);
    procedure acSelTermoPrExecute(Sender: TObject);
    procedure acChangeExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }

  end;

procedure prFindPartRemn(IdGr:Integer);
procedure prAllViewOff;
procedure prAllViewOn;
procedure prPreShowMove(Id:INteger;DateB,DateE:TDateTime;IdSkl:INteger;NameC:String);

var
  fmMainRnOffice: TfmMainRnOffice;

implementation

uses Un1, dmOffice, OMessure, Goods, PriceType, MH, Clients,
  DocsIn, GoodsSel, TransMenuBack, SelPerSkl, MoveSel, DocsOut, DocOutB,
  ExportFromOf, TOSel, DocInv, DMOReps, RepPrib, RepObSa, DocCompl, ActPer,
  DocsVn, OperType, DocOutR, RecalcPer, RecalcReal, PerA_Office, ClassSel,
  ParamSel, RepPost, AddAct, AddBar, AddCompl, AddDoc1, AddDoc2, AddDoc3,
  AddDoc4, AddInv, DOBSpec, RecalcPart, of_AvansRep, PeriodUni, SprBGU,
  TermoObr, rCategory, SelPerSkl2, RemnsDay, ImportDoc, ClassAlg, Makers,
  PreAlcogol, RepAlcogol, AlcoDecl, RepSS, RepSSRep, Pre, CardsMove,
  UpLica, DocPrice, RepPars, RepDocMove, Drivers, SelTermoPr, Change;

{$R *.dfm}

procedure prPreShowMove(Id:INteger;DateB,DateE:TDateTime;IdSkl:INteger;NameC:String);
Var iDateB,iDateE:INteger;
    iType:INteger;
begin
  fmCardsMove.ViewPartIn.BeginUpdate;
  fmCardsMove.ViewPartOut.BeginUpdate;
  fmCardsMove.ViewMove.BeginUpdate;
  fmCardsMove.ViCardM.BeginUpdate;
  fmCardsMove.ViewRealC.BeginUpdate;
  fmCardsMove.ViewRealB.BeginUpdate;

  iType:=prTypeTC(Id); //����� ��� �����

  fmCardsMove.Tag:=Id;
  fmCardsMove.Edit1.Text:=NameC;
  iDateB:=Trunc(DateB);
  iDateE:=Trunc(DateE);
  rQb:=0; rQe:=0;
  with dmO do
  begin
    quCPartIn.Active:=False;
    quCPartIn.SelectSQL.Clear;
    quCPartIn.SelectSQL.Add('SELECT pi.ID,pi.IDSTORE,mh.NAMEMH,pi.IDDOC,');
    quCPartIn.SelectSQL.Add('din.NUMDOC as NUMDOCIN,dinv.NUMDOC as NUMDOCINV,dvn.NUMDOC as NUMDOCVN,dacts.NUMDOC as NUMDOCACTS,dcompl.NUMDOC as NUMDOCCOMPL,dr.NUMDOC as NDR,');
    quCPartIn.SelectSQL.Add('pi.ARTICUL,pi.IDCLI,cl.NAMECL,pi.DTYPE,pi.QPART,pi.QREMN,');
//    quCPartIn.SelectSQL.Add('pi.PRICEIN,pi.PRICEOUT,pi.IDATE,(pi.PRICEIN*pi.QPART) as SUMIN,(pi.PRICEIN*pi.QREMN) as SUMREMN ');
    quCPartIn.SelectSQL.Add('pi.PRICEIN,pi.PRICEIN0,pi.PRICEOUT,pi.IDATE,(pi.PRICEIN*pi.QPART) as SUMIN,(pi.PRICEIN0*pi.QPART) as SUMIN0,(pi.PRICEIN*pi.QREMN) as SUMREMN');
    quCPartIn.SelectSQL.Add('FROM OF_PARTIN PI');
    quCPartIn.SelectSQL.Add('left join OF_MH mh on mh.ID=pi.IDSTORE');
    quCPartIn.SelectSQL.Add('left join OF_CLIENTS cl on cl.ID=pi.IDCLI');
    quCPartIn.SelectSQL.Add('left join of_docheadin din on din.ID=pi.IDDOC');
    quCPartIn.SelectSQL.Add('left join of_docheadinv dinv on dinv.ID=pi.IDDOC');
    quCPartIn.SelectSQL.Add('left join of_docheadvn dvn on dvn.ID=pi.IDDOC');
    quCPartIn.SelectSQL.Add('left join of_docheadacts dacts on dacts.ID=pi.IDDOC');
    quCPartIn.SelectSQL.Add('left join of_docheadcompl dcompl on dcompl.ID=pi.IDDOC');
    quCPartIn.SelectSQL.Add('left join of_docheadoutr dr on dr.ID=pi.IDDOC');
    quCPartIn.SelectSQL.Add('where pi.ARTICUL='+IntToStr(Id));
    quCPartIn.SelectSQL.Add('and pi.IDATE>='+IntToStr(iDateB));
    if iDateE<(iMaxDate-1) then
      quCPartIn.SelectSQL.Add('and pi.IDATE<'+IntToStr(iDateE+1));
    if IdSkl>0 then
      quCPartIn.SelectSQL.Add('and pi.IDSTORE='+IntToStr(IdSkl));
    quCPartIn.SelectSQL.Add('ORDER BY pi.IDATE');
    quCPartIn.Active:=True;


    quCPartOut.Active:=False;
    quCPartOut.SelectSQL.Clear;
    quCPartOut.SelectSQL.Add('SELECT po.ARTICUL,po.IDDATE,po.IDSTORE,mh.NAMEMH,po.IDPARTIN,po.IDDOC,');
    quCPartOut.SelectSQL.Add('dob.NUMDOC as NDOB,di.NUMDOC as NDI,dv.NUMDOC as NDV,da.NUMDOC as NDA,dc.NUMDOC as NDC,dou.NUMDOC as NDO,dr.NUMDOC as NDR,');
    quCPartOut.SelectSQL.Add('po.IDCLI,cl.NAMECL,po.DTYPE,');
    quCPartOut.SelectSQL.Add('po.QUANT,po.PRICEIN,po.SUMOUT,(po.PRICEIN*po.QUANT) as SUMIN');
    if iType=0 then quCPartOut.SelectSQL.Add('FROM OF_PARTOUT po')
    else            quCPartOut.SelectSQL.Add('FROM OF_PARTOUT_T po');
    quCPartOut.SelectSQL.Add('left join OF_MH mh on mh.ID=po.IDSTORE');
    quCPartOut.SelectSQL.Add('left join OF_CLIENTS cl on cl.ID=po.IDCLI');

    quCPartOut.SelectSQL.Add('left join of_docheadoutb dob on dob.ID=po.IDDOC');
    quCPartOut.SelectSQL.Add('left join of_docheadinv di on di.ID=po.IDDOC');
    quCPartOut.SelectSQL.Add('left join of_docheadvn dv on dv.ID=po.IDDOC');
    quCPartOut.SelectSQL.Add('left join of_docheadacts da on da.ID=po.IDDOC');
    quCPartOut.SelectSQL.Add('left join of_docheadcompl dc on dc.ID=po.IDDOC');
    quCPartOut.SelectSQL.Add('left join of_docheadout dou on dou.ID=po.IDDOC');
    quCPartOut.SelectSQL.Add('left join of_docheadoutr dr on dr.ID=po.IDDOC');

    quCPartOut.SelectSQL.Add('where po.ARTICUL='+IntToStr(Id));
    quCPartOut.SelectSQL.Add('and po.IDDATE>='+IntToStr(iDateB));
    if iDateE<(iMaxDate-1) then
      quCPartOut.SelectSQL.Add('and po.IDDATE<'+IntToStr(iDateE));
    if IdSkl>0 then
      quCPartOut.SelectSQL.Add('and po.IDSTORE='+IntToStr(IdSkl));
    quCPartOut.SelectSQL.Add('ORDER BY po.IDDATE');
    quCPartOut.Active:=True;

    quCRemn.Active:=False;
    quCRemn.SelectSQL.Clear;
    quCRemn.SelectSQL.Add('SELECT SUM(POSTIN-POSTOUT+VNIN-VNOUT+INV-QREAL) as REMN');
    quCRemn.SelectSQL.Add('FROM OF_GDSMOVE');
    quCRemn.SelectSQL.Add('where ARTICUL='+IntToStr(Id));
    quCRemn.SelectSQL.Add('and IDATE<'+IntToStr(iDateB));
    if IdSkl>0 then
      quCRemn.SelectSQL.Add('and IDSTORE='+IntToStr(IdSkl));
    quCRemn.Active:=True;
    quCRemn.First;
    if quCRemn.RecordCount>0 then rQb:=quCRemnREMN.AsFloat;
    quCRemn.Active:=False;


    quCMove.Active:=False;
    quCMove.SelectSQL.Clear;
    quCMove.SelectSQL.Add('SELECT m.ARTICUL,m.IDATE,m.IDSTORE,mh.NAMEMH,m.POSTIN,m.POSTOUT,m.VNIN,m.VNOUT,m.INV,m.QREAL');
    quCMove.SelectSQL.Add('FROM OF_GDSMOVE m');
    quCMove.SelectSQL.Add('left join OF_MH mh on mh.ID=m.IDSTORE');
    quCMove.SelectSQL.Add('where m.ARTICUL='+IntToStr(Id));
    quCMove.SelectSQL.Add('and m.IDATE>='+IntToStr(iDateB));
    if iDateE<(iMaxDate-1) then
      quCMove.SelectSQL.Add('and m.IDATE<'+IntToStr(iDateE));
    if IdSkl>0 then
      quCMove.SelectSQL.Add('and m.IDSTORE='+IntToStr(IdSkl));
    quCMove.SelectSQL.Add('ORDER BY m.IDATE');
    quCMove.Active:=True;

    with dmORep do
    begin
//      taCMove.Active:=False;
      CloseTa(taCMove);

      quCMove.First;
      while not quCMove.Eof do
      begin
        rQe:=rQb+quCMovePOSTIN.AsFloat-quCMovePOSTOUT.AsFloat+quCMoveVNIN.AsFloat-quCMoveVNOUT.AsFloat+quCMoveINV.AsFloat-quCMoveQREAL.AsFloat;
        taCMove.Append;
        taCMoveARTICUL.AsInteger:=quCMoveARTICUL.AsInteger;
        taCMoveIDATE.AsInteger:=quCMoveIDATE.AsInteger;
        taCMoveIDSTORE.AsInteger:=quCMoveIDSTORE.AsInteger;
        taCMoveNAMEMH.AsString:=quCMoveNAMEMH.AsString;
        taCMoveRB.AsFloat:=rQb;
        taCMovePOSTIN.AsFloat:=quCMovePOSTIN.AsFloat;
        taCMovePOSTOUT.AsFloat:=quCMovePOSTOUT.AsFloat;
        taCMoveVNIN.AsFloat:=quCMoveVNIN.AsFloat;
        taCMoveVNOUT.AsFloat:=quCMoveVNOUT.AsFloat;
        taCMoveINV.AsFloat:=quCMoveINV.AsFloat;
        taCMoveQREAL.AsFloat:=quCMoveQREAL.AsFloat;
        taCMoveRE.AsFloat:=rQe;
        taCMove.Post;

        rQb:=rQe;

        quCMove.Next;
      end;
      quCMove.First;

      CloseTe(taCardM);
      taCMove.First;

      rQb:=0;
      if taCMove.RecordCount>0 then rQb:=taCMoveRE.AsFloat-taCMovePOSTIN.AsFloat-taCMoveVNIN.AsFloat-taCMoveINV.AsFloat+(taCMovePOSTOUT.AsFloat+taCMoveVNOUT.AsFloat+taCMoveQREAL.AsFloat);

      while not taCMove.Eof do
      begin
        if (taCMovePOSTIN.AsFloat<>0)or(taCMoveVNIN.AsFloat<>0)or(taCMoveINV.AsFloat<>0) then
        begin  //���� ��� ������� �� ����
          if quCPartIn.Locate('IDATE',taCMoveIDATE.AsInteger,[]) then
          begin
            while  (quCPartInIDATE.AsInteger=taCMoveIDATE.AsInteger)and(quCPartIn.eof=False) do
            begin
              rQb:=rQb+RoundEx(quCPartInQPART.AsFloat*1000)/1000;

              taCardM.Append;
              taCardMIdDoc.AsInteger:=quCPartInIDDOC.AsInteger;
              taCardMTypeD.AsInteger:=quCPartInDTYPE.AsInteger;
              taCardMDateD.AsDateTime:=quCPartInIDATE.AsInteger;
              if quCPartInDTYPE.AsInteger=1 then taCardMsFrom.AsString:=quCPartInNAMECL.AsString
              else taCardMsFrom.AsString:='';
              taCardMsTo.AsString:=quCPartInNAMEMH.AsString;
              taCardMiM.AsInteger:=1;
              taCardMsM.AsString:=CardSel.mesuriment;
              taCardMQuant.AsFloat:=RoundEx(quCPartInQPART.AsFloat*1000)/1000;
              taCardMQRemn.AsFloat:=rQb;
              taCardMComment.AsString:='';
              taCardM.Post;

              quCPartIn.Next;
            end;
          end;
        end;
        if (taCMovePOSTOUT.AsFloat<>0)or(taCMoveVNOUT.AsFloat<>0)or(taCMoveQREAL.AsFloat<>0) then
        begin  //���� ��� ������� �� ����
          if quCPartOut.Locate('IDDATE',taCMoveIDATE.AsInteger,[]) then
          begin
            while  (quCPartOutIDDATE.AsInteger=taCMoveIDATE.AsInteger)and(quCPartOut.eof=False) do
            begin
              rQb:=rQb-RoundEx(quCPartOutQUANT.AsFloat*1000)/1000;

              taCardM.Append;
              taCardMIdDoc.AsInteger:=quCPartOutIDDOC.AsInteger;
              taCardMTypeD.AsInteger:=quCPartOutDTYPE.AsInteger;
              taCardMDateD.AsDateTime:=quCPartOutIDDATE.AsInteger;
              taCardMsFrom.AsString:=quCPartOutNAMEMH.AsString;

              taCardMComment.AsString:='';
              taCardMsTo.AsString:='';

              if quCPartOutDTYPE.AsInteger=2 then
              begin
                quDOBHEAD.Active:=False;
                quDOBHEAD.ParamByName('IDH').AsInteger:=quCPartOutIDDOC.AsInteger;
                quDOBHEAD.Active:=True;
                if quDOBHEAD.RecordCount>0 then
                begin
                  taCardMComment.AsString:=quDOBHEADCOMMENT.AsString;
                  taCardMsTo.AsString:=quDOBHEADOPER.AsString;
                end;
                quDOBHEAD.Active:=False;
              end;
              taCardMiM.AsInteger:=1;
              taCardMsM.AsString:=CardSel.mesuriment;
              taCardMQuant.AsFloat:=(-1)*RoundEx(quCPartOutQUANT.AsFloat*1000)/1000;
              taCardMQRemn.AsFloat:=rQb;
              taCardM.Post;

              quCPartOut.Next;
            end;
          end;
        end;

        taCMove.Next;
      end;

    end;

    quCMove.Active:=False;

    if iType=0 then //������ ��� ������
    begin
      quCReal.Active:=False;
      quCReal.SelectSQL.Clear;
      quCReal.SelectSQL.Add('SELECT dh.DATEDOC,spb.CODEB,spb.NAMEB,spb.QUANT,spb.PRICEOUT,spb.SUMOUT,spb.QUANTC,');
      quCReal.SelectSQL.Add('spb.PRICEIN,spb.SUMIN,spb.IM,spb.SM');
      quCReal.SelectSQL.Add('FROM OF_DOCSPECOUTBC spb');
      quCReal.SelectSQL.Add('left join OF_DOCHEADOUTB dh on dh.ID=spb.IDHEAD');
      quCReal.SelectSQL.Add('where spb.IDCARD='+IntToStr(Id));
      quCReal.SelectSQL.Add('and dh.DATEDOC>='''+FormatDateTime('dd.mm.yyyy',iDateB)+'''');
      quCReal.SelectSQL.Add('and dh.DATEDOC<'''+FormatDateTime('dd.mm.yyyy',iDateE)+'''');
      quCReal.SelectSQL.Add('and dh.IDSKL='+IntToStr(IdSkl));
      quCReal.SelectSQL.Add('and dh.IACTIVE=1');
      quCReal.Active:=True;

      fmCardsMove.LevelRealC.Visible:=True;
      fmCardsMove.LevelRealB.Visible:=False;

    end;

    if iType=1 then //������ ��� ����
    begin
      quBReal.Active:=False;
      quBReal.SelectSQL.Clear;
      quBReal.SelectSQL.Add('SELECT dh.DATEDOC, dh.NUMDOC, bc.IDHEAD, bc.IDB, bc.CODEB, bc.NAMEB, bc.QUANT, bc.PRICEOUT, bc.SUMOUT, SUM(bc.SUMIN) as SUMIN');
      quBReal.SelectSQL.Add('FROM OF_DOCSPECOUTBC bc');
      quBReal.SelectSQL.Add('left join OF_DOCHEADOUTB dh on dh.ID=bc.IDHEAD');

      quBReal.SelectSQL.Add('where bc.CODEB='+IntToStr(Id));
      quBReal.SelectSQL.Add('and dh.DATEDOC>='''+FormatDateTime('dd.mm.yyyy',iDateB)+'''');
      quBReal.SelectSQL.Add('and dh.DATEDOC<'''+FormatDateTime('dd.mm.yyyy',iDateE)+'''');
      quBReal.SelectSQL.Add('and dh.IDSKL='+IntToStr(IdSkl));
      quBReal.SelectSQL.Add('and dh.IACTIVE=1');
      quBReal.SelectSQL.Add('group by dh.DATEDOC, dh.NUMDOC, bc.IDHEAD,bc.IDB,bc.CODEB,bc.NAMEB, bc.QUANT,bc.PRICEOUT,bc.SUMOUT');
      quBReal.Active:=True;

      fmCardsMove.LevelRealC.Visible:=False;
      fmCardsMove.LevelRealB.Visible:=True;
    end;


    if iType=0 then
    begin
      if iDateE>=iMaxDate then fmCardsMove.Caption:='�������� �� ������ "'+NameC+'" � '+FormatDateTiMe('dd.mm.yyyy',iDateB)
      else fmCardsMove.Caption:='�������� �� ������ "'+NameC+'" �� ������ � '+FormatDateTiMe('dd.mm.yyyy',iDateB)+' �� '+FormatDateTiMe('dd.mm.yyyy',iDateE-1);
    end else
    begin
      if iDateE>=iMaxDate then fmCardsMove.Caption:='�������� �� ����� "'+NameC+'" � '+FormatDateTiMe('dd.mm.yyyy',iDateB)
      else fmCardsMove.Caption:='�������� �� ����� "'+NameC+'" �� ������ � '+FormatDateTiMe('dd.mm.yyyy',iDateB)+' �� '+FormatDateTiMe('dd.mm.yyyy',iDateE-1);
    end;

    fmCardsMove.ViewPartIn.EndUpdate;
    fmCardsMove.ViewPartOut.EndUpdate;
    fmCardsMove.ViewMove.EndUpdate;
    fmCardsMove.ViCardM.EndUpdate;
    fmCardsMove.ViewRealC.EndUpdate;
    fmCardsMove.ViewRealB.EndUpdate;
  end;
end;


procedure prAllViewOff;
begin
  fmAddAct.ViewAO.BeginUpdate;
  fmAddAct.ViewAi.BeginUpdate;

  fmAddCompl.ViewCom.BeginUpdate;
  fmAddCompl.ViewComBC.BeginUpdate;
  fmAddCompl.ViewComC.BeginUpdate;

  fmAddDoc1.ViewDoc1.BeginUpdate;

  fmAddDoc2.ViewDoc2.BeginUpdate;

  fmAddDoc3.ViewDoc3.BeginUpdate;

  fmAddDoc4.ViewDoc4.BeginUpdate;

  fmAddInv.ViewInv.BeginUpdate;
  fmAddInv.ViewBC.BeginUpdate;
  fmAddInv.ViewInvC.BeginUpdate;

  fmDobSpec.ViewB.BeginUpdate;
  fmDobSpec.ViewC.BeginUpdate;
  fmDobSpec.ViewBC.BeginUpdate;
end;

procedure prAllViewOn;
begin
  fmAddAct.ViewAO.EndUpdate;
  fmAddAct.ViewAi.EndUpdate;

  fmAddCompl.ViewCom.EndUpdate;
  fmAddCompl.ViewComBC.EndUpdate;
  fmAddCompl.ViewComC.EndUpdate;

  fmAddDoc1.ViewDoc1.EndUpdate;

  fmAddDoc2.ViewDoc2.EndUpdate;

  fmAddDoc3.ViewDoc3.EndUpdate;

  fmAddDoc4.ViewDoc4.EndUpdate;

  fmAddInv.ViewInv.EndUpdate;
  fmAddInv.ViewBC.EndUpdate;
  fmAddInv.ViewInvC.EndUpdate;

  fmDobSpec.ViewB.EndUpdate;
  fmDobSpec.ViewC.EndUpdate;
  fmDobSpec.ViewBC.EndUpdate;
end;

procedure prFindPartRemn(IdGr:Integer);
Var sM:String;
    iM:Integer;
    QT1:TpFIBDataSet;
    iDate:Integer;
    rQ,rQrec:Real;
    rSum,rSum0:Real;
begin
  with dmO do
  with dmORep do
  begin
    quClass.Active:=False;
    quClass.ParamByName('IPARENT').AsInteger:=IdGr;
    quClass.Active:=True;

    iDate:=Trunc(Date);

    quClass.First;    //��� �� �������
    while not quClass.Eof do
    begin
      with fmRepPost do
      begin
        quMh_.First;
        while not quMH_.Eof do
        begin
          rQ:=prCalcRemn(quClassID.AsInteger,iDate,quMH_ID.AsInteger);

          if rQ>0.001 then //������� ������������� ����������� �� �������, ����� �������������
          begin
            quFindPart.Active:=False;
            quFindPart.ParamByName('IDCARD').AsInteger:=quClassID.AsInteger;
            quFindPart.ParamByName('IDSTORE').AsInteger:=quMH_ID.AsInteger;
            quFindPart.Active:=True;
{
SELECT pi.ID,pi.IDSTORE,pi.IDDOC,pi.ARTICUL,pi.IDCLI,pi.DTYPE,
pi.QPART,pi.QREMN,pi.PRICEIN,pi.IDATE,Cli.NAMECL,mh.NAMEMH
FROM OF_PARTIN pi
left join OF_CLIENTS cli on pi.IDCLI=Cli.ID
left join OF_MH mh on pi.IDSTORE=mh.ID

where ARTICUL=:IDCARD and pi.QREMN>0.001 and pi.IDSTORE=:IDSTORE
Order by pi.IDATE DESC
}
            quFindPart.First; //������ ������

            //���� ���������� ���� ���������. �� ����������� ���� ������� ������ ������ ��������� �������

            while (quFindPart.Eof=False)and(rQ>0.001) do
            begin
              if rQ>=quFindPartQREMN.AsFloat then
              begin
                rQRec:=quFindPartQREMN.AsFloat;
                rQ:=rQ-rQRec;
              end else
              begin
                rQRec:=rQ;
                rQ:=0;
              end;

              taPost.Append;
              taPostIdCard.AsInteger:=quClassID.AsInteger;
              taPostNameC.AsString:=quClassNAME.AsString;
              prFindSM(quClassIMESSURE.AsInteger,sM,iM);
              taPostsM.AsString:=sM;
              taPostiM.AsInteger:=iM;
              taPostrQ.AsFloat:= rQRec;
              taPostrPrice.AsFloat:=quFindPartPRICEIN.AsFloat;
              taPostrSum.AsFloat:=rQRec*quFindPartPRICEIN.AsFloat;
              taPostiPost.AsInteger:=quFindPartIDCLI.AsInteger;
              taPostNameP.AsString:=quFindPartNAMECL.AsString;
              taPostiGr.AsInteger:=quClassPARENT.AsInteger;
              taPostNameG.AsString:=quClassNAMECL.asstring;
              taPostiDoc.AsInteger:=quFindPartIDDOC.AsInteger;
              taPostiTDoc.AsInteger:=quFindPartDTYPE.AsInteger;
              taPostsDoc.AsString:=FormatDateTime('dd.mm.yyyy',quFindPartIDATE.AsInteger);
              taPostNameMH.AsString:=quFindPartNAMEMH.AsString;
              taPost.Post;

              quFindPart.Next; Delay(10);
            end;
            quFindPart.Active:=False;
          end else
          begin
            if rQ<=(-0.001) then
            begin
              rSum:=prCalcRemnSumF(quClassID.AsInteger,iDate,quMH_ID.AsInteger,rQ,rSum0);
             //rSum - ����� ������� (������������ � - ), �.�. �������������� �� ��������� �������

              taPost.Append;
              taPostIdCard.AsInteger:=quClassID.AsInteger;
              taPostNameC.AsString:=quClassNAME.AsString;
              prFindSM(quClassIMESSURE.AsInteger,sM,iM);
              taPostsM.AsString:=sM;
              taPostiM.AsInteger:=iM;
              taPostrQ.AsFloat:= rQ;
              taPostrPrice.AsFloat:=rSum/rQ;
              taPostrSum.AsFloat:=rSum;
              taPostiPost.AsInteger:=-1;
              taPostNameP.AsString:='';
              taPostiGr.AsInteger:=quClassPARENT.AsInteger;
              taPostNameG.AsString:=quClassNAMECL.asstring;
              taPostiDoc.AsInteger:=-1;
              taPostiTDoc.AsInteger:=-1;
              taPostsDoc.AsString:='';
              taPostNameMH.AsString:=quMH_NAMEMH.AsString;
              taPost.Post;
            end;
          end;

          quMH_.Next;
        end;
      end;
      quClass.Next;Delay(10);
    end;
    quClass.Active:=False;

    QT1:=TpFIBDataSet.Create(Owner);
    QT1.Active:=False;
    QT1.Database:=OfficeRnDb;
    QT1.Transaction:=trSel;
    QT1.SelectSQL.Clear;
    QT1.SelectSQL.Add('SELECT ID FROM OF_CLASSIF');
    QT1.SelectSQL.Add('WHERE ID_PARENT='+IntToStr(IdGr));
    QT1.Active:=True;

    QT1.First;
    while not QT1.Eof do
    begin
      prFindPartRemn(QT1.FieldByName('ID').AsInteger);
      QT1.Next; Delay(10);
    end;
    QT1.Active:=False;
    QT1.Free;
  end;
end;


procedure TfmMainRnOffice.FormCanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
  NewHeight:=125
end;

procedure TfmMainRnOffice.FormCreate(Sender: TObject);
Var WkDir:String;
begin
  CurDir := ExtractFilePath(ParamStr(0));
  WkDir:=GetCurrentDir;
  if CurDir<>WkDir then CurDir:=WkDir;

//  SetWorkingDirectory(PChar(ExtractFilePath(FileName)));

  if CurDir[Length(CurDir)]<>'\' then CurDir:=CurDir+'\';

  StatusBar1.Panels[1].Text:=CurDir;

  ReadIni;
  CommonSet.DateFrom:=Trunc(Date-7);
  CommonSet.DateTo:=Trunc(Date+7);

  CommonSet.ADateBeg:=Date-200;
  CommonSet.ADateEnd:=Date+90;

  CurVal.IdMH:=0;
  CurVal.NAMEMH:='';
  if FileExists(CurDir+'Prib.cds') then DeleteFile(CurDir+'Prib.cds');
//  if FileExists(CurDir+'Spec.cds') then DeleteFile(CurDir+'Spec.cds');
  if FileExists(CurDir+'SpecC.cds') then DeleteFile(CurDir+'SpecC.cds');
  if FileExists(CurDir+'SpecCInv.cds') then DeleteFile(CurDir+'SpecCInv.cds');
  if FileExists(CurDir+'ttkspec.cds') then DeleteFile(CurDir+'ttkspec.cds');

  try
    if DirectoryExists(CurDir+'Arh')=False then createdir(CurDir+'Arh');
    if DirectoryExists(CurDir+'History')=False then createdir(CurDir+'History');
    if DirectoryExists(CurDir+'Export')=False then createdir(CurDir+'Export');
  except
  end;


//  if FileExists(CurDir+'SpecInv.cds') then DeleteFile(CurDir+'SpecInv.cds');

  SpeedBar1.Color := UserColor.Main;
//  StatusBar1.Color:= UserColor.Main;

  RDM.MHId:=0;
  RDM.d1:=Date-1;
  RDM.d2:=Date-1;
  RDM.i1:=1;
  RDM.i2:=1;
  RDM.i3:=1;
  RDM.i4:=1;
  RDM.i5:=1;
  RDM.i6:=1;
end;

procedure TfmMainRnOffice.FormResize(Sender: TObject);
begin
  StatusBar1.Width:=Width;
end;

procedure TfmMainRnOffice.FormShow(Sender: TObject);
Var StrC:String;
begin
  prWriteStart('�������� �����.');

  Left:=0;
  Top:=0;
//  Width:=1024;

  StrC:=CommonSet.DepartName;
  if SpecVal.DateTo<StrToDate('01.01.2100') then
    StrC:=StrC+' ��������� �� '+FormatDateTime('dd.mm.yyyy',SpecVal.DateTo);
  if SpecVal.CountStarts>=0 then
    StrC:=StrC+' �������� '+INtToStr(SpecVal.CountStarts)+' �������� ���������.';
  if SpecVal.CountStarts=-1 then
    StrC:=StrC+' �������� 0 �������� ���������.';

  Caption:=StrC;

  with dmO do
  begin
    quPer.Active:=False;
    quPer.SelectSQL.Clear;
    quPer.SelectSQL.Add('select * from rpersonal');
    quPer.SelectSQL.Add('where uvolnen=1 and modul2=0');
    quPer.SelectSQL.Add('order by Name');
//    quPer.Active:=True; ����������� �� show fmPerA
  end;
  fmPerA_Office:=TfmPerA_Office.Create(Application);

  fmPerA_Office.StatusBar1.Color:=UserColor.Main;
  fmPerA_Office.GroupBox1.Color:=UserColor.Main;

  fmPerA_Office.ShowModal;
  if fmPerA_Office.ModalResult=mrOk then
  begin
    Caption:=Caption+'   : '+Person.Name;
    WriteIni1; //�������� �������� �������
    fmPerA_Office.Release;

    bMenuList:=False;
    bMenuListMo:=False;

    try
      if DirectoryExists(CurDir+Person.Name)=False then createdir(CurDir+Person.Name);
    except
    end;


    fmGoods:=tfmGoods.Create(Application);
    fmGoodsSel:=tfmGoodsSel.Create(Application);
    fmTermoObr:=tfmTermoObr.Create(Application);
//    fmModCr:=tfmModCr.Create(Application);
//    fmMenuCr.Show;
    fmSprBGU:=TfmSprBGU.Create(Application);

    fmDocsReal:=TfmDocsReal.Create(Application);
    fmChange:=TfmChange.Create(Application);

    fmAddDoc4.ViewDoc4.RestoreFromIniFile(CurDir+Person.Name+'\'+GridIni);

    bMenuList:=True;
    bMenuListMo:=True;

    try
      dmORep.quDB.Active:=False;
      dmORep.quDB.Active:=True;
      dmORep.quDB.First;
      if dmORep.quDB.RecordCount>0 then
        DBName:=dmORep.quDBPATHDB.AsString;
    except
    end;

    if (SpecVal.CountStarts=-1)or(SpecVal.DateTo<Date) then TimerClose.Enabled:=True;

    if dmO.quCateg.Active=False then dmO.quCateg.Active:=True;
    if dmO.quCTO.Active=False then dmO.quCTO.Active:=True;
    with dmO do
    begin
      taNDS.Active:=False;
      taNDS.Active:=True;
      taNds.First;
      if not taNDS.Eof then vNds[1]:=taNDSPROC.AsFloat; taNds.Next;
      if not taNDS.Eof then vNds[2]:=taNDSPROC.AsFloat; taNds.Next;
      if not taNDS.Eof then vNds[3]:=taNDSPROC.AsFloat;

      taNDS.Active:=False;
    end;
  end
  else
  begin
    fmPerA_Office.Release;
    delay(100);
    close;
    delay(100);
  end;
end;

procedure TfmMainRnOffice.acExitExecute(Sender: TObject);
begin
  close;
end;

procedure TfmMainRnOffice.acMessureExecute(Sender: TObject);
begin
  //������� ���������
  //������� , ��������  - ����� ����� �������� ��� ������� ����� ����
  bAddSpec:=False;
  fmMessure.ShowModal;
  
end;

procedure TfmMainRnOffice.acGoodsExecute(Sender: TObject);
begin
  bAddSpecRet:= False;
  bAddSpecIn:= False;
  bAddSpecB:= False;
  bAddSpecB1:= False;
  bAddSpecInv:= False;
  bAddSpecCompl:= False;
  bAddTSpec:= False;
  bAddSpecAO:= False;
  bAddSpecVn:=False;
  bAddSpecR:=False;
  bResetCodeIn:=False;
  bAddToChange:=False;

  fmGoods.Show;
end;

procedure TfmMainRnOffice.acPriceTypeExecute(Sender: TObject);
begin
  //���� ���
  with dmO do
  begin
    taPriceT.Active:=False;
    taPriceT.Active:=True;

    fmPriceType.ShowModal;
    taPriceT.Active:=False;
  end;
end;

procedure TfmMainRnOffice.acMHPExecute(Sender: TObject);
begin
  fmMH.TreeMH.Items.Clear;
  ClassifEx(nil,fmMH.TreeMH,dmO.quMHTree,0,'NAMEMH');
  fmMH.ShowModal;
end;

procedure TfmMainRnOffice.acClientsExecute(Sender: TObject);
begin
//�����������
  with dmO do
  begin
    if taClients.Active=False then taClients.Active:=True;
    fmClients.Show;
  end;
end;

procedure TfmMainRnOffice.acDocInExecute(Sender: TObject);
begin
  //��������� ���������
  if CommonSet.DateTo>=iMaxDate then fmDocsIn.Caption:='��������� ��������� � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
  else fmDocsIn.Caption:='��������� ��������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);
  with dmO do
  begin
    quDocsInSel.Active:=False;
    quDocsInSel.ParamByName('DATEB').AsDate:=CommonSet.DateFrom;
    quDocsInSel.ParamByName('DATEE').AsDate:=CommonSet.DateTo;
    quDocsInSel.ParamByName('IDPERSON').AsInteger:=Person.Id;
    quDocsInSel.Active:=True;
    quDocsInSel.Last;
  end;
  fmDocsIn.LevelDocsIn.Visible:=True;
  fmDocsIn.LevelCards.Visible:=False;
  fmDocsIn.SpeedItem3.Visible:=True;
  fmDocsIn.SpeedItem4.Visible:=True;
  fmDocsIn.SpeedItem5.Visible:=True;
  fmDocsIn.SpeedItem6.Visible:=True;
  fmDocsIn.SpeedItem7.Visible:=True;
  fmDocsIn.SpeedItem8.Visible:=True;

  fmDocsIn.Show;
end;

procedure TfmMainRnOffice.acMoveExecute(Sender: TObject);
begin
//
end;

procedure TfmMainRnOffice.acOutExecute(Sender: TObject);
begin
  if CommonSet.DateTo>=iMaxDate then fmDocsOut.Caption:='�������� � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
  else fmDocsOut.Caption:='�������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);
  with dmO do
  begin
    quDocsOutSel.Active:=False;
    quDocsOutSel.ParamByName('DATEB').AsDate:=CommonSet.DateFrom;
    quDocsOutSel.ParamByName('DATEE').AsDate:=CommonSet.DateTo;
    quDocsOutSel.ParamByName('IDPERSON').AsInteger:=Person.Id;
    quDocsOutSel.Active:=True;
  end;
  fmDocsOut.Show;
end;

procedure TfmMainRnOffice.acRemnExecute(Sender: TObject);
// �������
Var // IdPar:INteger;
    iDate:Integer;
    iC,iM:Integer;
    rQ,rSum,rSum0:Real;
    S1,S2:String;
begin
  fmSelPerSkl:=tfmSelPerSkl.Create(Application);
  with fmSelPerSkl do
  begin
    if CommonSet.DateTo>=iMaxDate then CommonSet.DateTo:=Date;
    cxDateEdit1.Date:=CommonSet.DateTo;
    quMHAll1.Active:=False;
    quMHAll1.ParamByName('IDPERSON').AsInteger:=Person.Id;
    quMHAll1.Active:=True;
    quMHAll1.First;
    cxLookupComboBox1.EditValue:=quMHAll1ID.AsInteger;
    cxCheckBox1.Visible:=False;
    Label1.Caption:='�� �����';
  end;
  fmSelPerSkl.ShowModal;
  if fmSelPerSkl.ModalResult=mrOk then
  begin
//    CommonSet.DateFrom:=Trunc(fmSelPerSkl.cxDateEdit1.Date);
    CommonSet.DateTo:=Trunc(fmSelPerSkl.cxDateEdit1.Date)+1;
    iDate:=Trunc(fmSelPerSkl.cxDateEdit1.Date);
    CommonSet.IdStore:=fmSelPerSkl.cxLookupComboBox1.EditValue;
    CommonSet.NameStore:=fmSelPerSkl.cxLookupComboBox1.Text;
    fmSelPerSkl.quMHAll1.Active:=False;
    fmSelPerSkl.Release;

    fmMoveSel.Caption:='������� �� �� '+CommonSet.NameStore+' �� ����� '+FormatDateTiMe('dd.mm.yyyy',iDate);
    fmMoveSel.LevelRemn.Visible:=True;
    fmMoveSel.LevelMoveSel.Visible:=False;

    Cursor:=crDefault; Delay(10);
    fmMoveSel.Memo1.Clear;
    fmMoveSel.Memo1.Lines.Add('����� ... ���� ������������ ������.');

    fmMoveSel.Show;
    Delay(100);

    fmMoveSel.ViewRemn.BeginUpdate;
    iC:=0;
    CloseTe(fmMoveSel.meRemns);

    with dmO do
    begin
      quRemnDate.Active:=False;
      quRemnDate.Active:=True;
      if quRemnDate.RecordCount>100 then iM:=Trunc(quRemnDate.RecordCount/100)
      else iM:=1;

      fmMoveSel.PBar1.Properties.Min:=0;
      fmMoveSel.PBar1.Properties.Max:=quRemnDate.RecordCount;
      fmMoveSel.PBar1.Visible:=True;

      quRemnDate.First;
      while not quRemnDate.Eof do
      begin
        with fmMoveSel do
        begin
          rQ:=prCalcRemn(quRemnDateID.AsInteger,iDate,CommonSet.IdStore);

//        rQ:=prCalcRemn(taSpecIdGoods.AsInteger,Trunc(cxDateEdit1.Date),cxLookupComboBox1.EditValue);
//        rSum:=prCalcRemnSumF(taSpecIdGoods.AsInteger,Trunc(cxDateEdit1.Date),cxLookupComboBox1.EditValue,rQ);

          if abs(rQ)>=0.0001 then
          begin
            rSum:=prCalcRemnSumF(quRemnDateID.AsInteger,iDate,CommonSet.IdStore,rQ,rSum0);

            prFind2group(quRemnDatePARENT.AsInteger,S1,S2);

            meRemns.Append;
            meRemnsArticul.AsInteger:=quRemnDateID.AsInteger;
            meRemnsName.AsString:=quRemnDateNAME.AsString;
            meRemnsGr.AsString:=S1;
            meRemnsSGr.AsString:=S2;
            meRemnsIdM.AsInteger:=quRemnDateIMESSURE.AsInteger;
            meRemnsKM.AsFloat:=quRemnDateKOEF.AsFloat;
            meRemnsSM.AsString:=quRemnDateNAMESHORT.AsString;
            meRemnsTCard.AsInteger:=quRemnDateTCARD.AsInteger;
            if quRemnDateKOEF.AsFloat>0 then meRemnsQuant.AsFloat:=rQ/quRemnDateKOEF.AsFloat
            else meRemnsQuant.AsFloat:=rQ;
            meRemnsRSum.AsFloat:=rSum;
            meRemnsCType.AsInteger:=quRemnDateCATEGORY.AsInteger;
            meRemns.Post;
          end;
        end;

        quRemnDate.Next;
        inc(iC);
        if iC mod iM = 0 then
        begin
          fmMoveSel.PBar1.Position:=iC;
          delay(10);
        end;
      end;
    end;
    fmMoveSel.PBar1.Position:=iC; Delay(100);
    fmMoveSel.ViewRemn.EndUpdate;
    fmMoveSel.PBar1.Visible:=False;
    fmMoveSel.Memo1.Lines.Add('������������ ��.');

  end else
  begin
    fmSelPerSkl.quMHAll1.Active:=False;
    fmSelPerSkl.Release;
  end;
end;

procedure TfmMainRnOffice.acInvExecute(Sender: TObject);
begin
  //��������������
  if CommonSet.DateTo>=iMaxDate then fmDocsInv.Caption:='�������������� � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
  else fmDocsInv.Caption:='�������������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);
  with dmO do
  with dmORep do
  begin
    quDocsInvSel.Active:=False;
    quDocsInvSel.ParamByName('DATEB').AsDate:=CommonSet.DateFrom;
    quDocsInvSel.ParamByName('DATEE').AsDate:=CommonSet.DateTo+1;
    quDocsInvSel.ParamByName('IDPERSON').AsInteger:=Person.Id;
    quDocsInvSel.Active:=True;
  end;
  fmDocsInv.LevelDocsInv.Visible:=True;
  fmDocsInv.LevelCardsInv.Visible:=False;
  fmDocsInv.SpeedItem3.Visible:=True;
  fmDocsInv.SpeedItem4.Visible:=True;
  fmDocsInv.SpeedItem5.Visible:=True;
  fmDocsInv.SpeedItem6.Visible:=True;
  fmDocsInv.SpeedItem7.Visible:=True;
  fmDocsInv.SpeedItem8.Visible:=True;

  fmDocsInv.Show;
end;

procedure TfmMainRnOffice.acMenuExecute(Sender: TObject);
begin
  //���� ���������

  fmMenuCr:=tfmMenuCr.Create(Application);

  fmMenuCr.Show;
end;

procedure TfmMainRnOffice.acMoveDateExecute(Sender: TObject);
Var IdPar:INteger;
begin
//�������� �� ������ �� ������
  fmSelPerSkl:=tfmSelPerSkl.Create(Application);
  with fmSelPerSkl do
  begin
    cxDateEdit1.Date:=CommonSet.DateFrom;
    quMHAll1.Active:=False;
    quMHAll1.ParamByName('IDPERSON').AsInteger:=Person.Id;
    quMHAll1.Active:=True;
    quMHAll1.First;
    cxLookupComboBox1.EditValue:=quMHAll1ID.AsInteger;
  end;
  fmSelPerSkl.ShowModal;
  if fmSelPerSkl.ModalResult=mrOk then
  begin
    CommonSet.DateFrom:=Trunc(fmSelPerSkl.cxDateEdit1.Date);
    CommonSet.DateTo:=Trunc(fmSelPerSkl.cxDateEdit2.Date)+1;
    CommonSet.IdStore:=fmSelPerSkl.cxLookupComboBox1.EditValue;
    CommonSet.NameStore:=fmSelPerSkl.cxLookupComboBox1.Text;

    if CommonSet.DateTo>=iMaxDate then fmMoveSel.Caption:='�������� �� �� '+CommonSet.NameStore+' � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
    else fmMoveSel.Caption:='�������� �� �� '+CommonSet.NameStore+' �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);
    fmSelPerSkl.quMHAll1.Active:=False;
    fmSelPerSkl.Release;
    with dmO do
    begin
      idPar:=GetId('Pars');
      taParams.Active:=False;
      taParams.Active:=True;
      taParams.Append;
      taParamsID.AsInteger:=idPar;
      taParamsIDATEB.AsInteger:=Trunc(CommonSet.DateFrom);
      taParamsIDATEE.AsInteger:=Trunc(CommonSet.DateTo-1);
      taParamsIDSTORE.AsInteger:=CommonSet.IdStore;
      taParams.Post;
      taParams.Active:=False;

      quMoveSel.Active:=False;
      quMoveSel.Active:=True;

    end;
    fmMoveSel.LevelRemn.Visible:=False;
    fmMoveSel.LevelMoveSel.Visible:=True;

    Cursor:=crDefault; Delay(10);
    fmMoveSel.Show;

  end else
  begin
    fmSelPerSkl.quMHAll1.Active:=False;
    fmSelPerSkl.Release;
  end;
end;

procedure TfmMainRnOffice.acOutBExecute(Sender: TObject);
begin
  //������ ����
  if CommonSet.DateTo>=iMaxDate then fmDocsOutB.Caption:='���������� � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
  else fmDocsOutB.Caption:='���������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);
  with dmO do
  begin
    fmDocsOutB.ViewDocsOutB.BeginUpdate;

    quDocsOutB.Active:=False;
    quDocsOutB.ParamByName('DATEB').AsDate:=CommonSet.DateFrom;
    quDocsOutB.ParamByName('DATEE').AsDate:=CommonSet.DateTo;
    quDocsOutB.ParamByName('IDPERSON').AsInteger:=Person.Id;
    quDocsOutB.Active:=True;

    fmDocsOutB.ViewDocsOutB.EndUpdate;
  end;
  fmDocsOutB.Show;

end;

procedure TfmMainRnOffice.acExportBuhExecute(Sender: TObject);
begin
//������� � �����������
  fmExport:=TfmExport.Create(Application);
  with fmExport do
  begin
    cxDateEdit1.Date:=CommonSet.DateFrom;
    cxDateEdit2.Date:=Date;
    ProgressBar1.Visible:=False;
    ProgressBar1.Position:=0;
  end;
  fmExport.ShowModal;
  fmExport.Release;
end;

procedure TfmMainRnOffice.acTovRepExecute(Sender: TObject);
begin
 //�������� �����
  fmSelPerSkl:=tfmSelPerSkl.Create(Application);
  with fmSelPerSkl do
  begin
    cxDateEdit1.Date:=CommonSet.DateFrom;
    quMHAll1.Active:=False;
    quMHAll1.ParamByName('IDPERSON').AsInteger:=Person.Id;
    quMHAll1.Active:=True;
    cxLookupComboBox1.EditValue:=quMHAll1ID.AsInteger;
    quMHAll1.First;
    if CommonSet.IdStore>0 then cxLookupComboBox1.EditValue:=CommonSet.IdStore;
  end;
  fmSelPerSkl.ShowModal;
  if fmSelPerSkl.ModalResult=mrOk then
  begin
    CommonSet.DateFrom:=Trunc(fmSelPerSkl.cxDateEdit1.Date);
    CommonSet.DateTo:=Trunc(fmSelPerSkl.cxDateEdit2.Date)+1;
    CommonSet.IdStore:=fmSelPerSkl.cxLookupComboBox1.EditValue;
    CommonSet.NameStore:=fmSelPerSkl.cxLookupComboBox1.Text;

    if CommonSet.DateTo>=iMaxDate then fmTO.Caption:='�������� ������ �� �� '+CommonSet.NameStore+' � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
    else fmTO.Caption:='�������� ������ �� �� '+CommonSet.NameStore+' �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);
    fmSelPerSkl.quMHAll1.Active:=False;
    fmSelPerSkl.Release;
    with dmO do
    begin
      quTO.Active:=False;
      quTO.ParamByName('DATEB').AsInteger:=Trunc(CommonSet.DateFrom);
      quTO.ParamByName('DATEE').AsInteger:=Trunc(CommonSet.DateTo);
      quTO.ParamByName('IDSKL').AsInteger:=CommonSet.IdStore;
      quTO.Active:=True;

      fmTO.Show;
    end;
  end else
  begin
    fmSelPerSkl.quMHAll1.Active:=False;
    fmSelPerSkl.Release;
  end;
end;

procedure TfmMainRnOffice.acRepPribExecute(Sender: TObject);
begin
  //����� �� �������
  if not CanDo('prRepPrib') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;

  fmSelPerSkl2:=tfmSelPerSkl2.Create(Application);
  with fmSelPerSkl2 do
  begin
    cxDateEdit1.Date:=CommonSet.DateFrom;
    quMHAll1.Active:=False; quMHAll1.Active:=True; quMHAll1.First;
    if Prib.MHId>0 then cxLookupComboBox1.EditValue:=Prib.MHId else cxLookupComboBox1.EditValue:=quMHAll1ID.AsInteger;
    CheckBox1.Checked:=Prib.MHAll;
    CheckBox11.Checked:=Prib.MHUnion;

    quMProd.Active:=False; quMProd.Active:=True;  quMProd.First;
    if Prib.MProd>0 then cxLookupComboBox2.EditValue:=Prib.MProd else cxLookupComboBox2.EditValue:=quMProdID.AsInteger;
    CheckBox2.Checked:=Prib.MProdAll;
    CheckBox22.Checked:=Prib.MProdUnion;

    quCateg.Active:=False; quCateg.Active:=True; quCateg.First;

    if Prib.Cat>0 then cxLookupComboBox3.EditValue:=Prib.Cat else cxLookupComboBox3.EditValue:=quCategID.AsInteger;
    CheckBox3.Checked:=Prib.CatAll;
    CheckBox33.Checked:=Prib.CatUnion;

    quSalet.Active:=False; quSalet.Active:=True; quSalet.First;
    cxLookupComboBox4.EditValue:=quSaletSalet.AsInteger;
    CheckBox4.Checked:=Prib.SaletAll;
    CheckBox44.Checked:=Prib.SaletUnion;

    cxLookupComboBox5.EditValue:=Prib.Oper; 
    CheckBox5.Checked:=Prib.OperAll;
    CheckBox55.Checked:=Prib.OperUnion;

    CheckBox6.Checked:=Prib.OperUnionCli;

  end;
  fmSelPerSkl2.ShowModal;
  if fmSelPerSkl2.ModalResult=mrOk then
  begin
    CommonSet.DateFrom:=Trunc(fmSelPerSkl2.cxDateEdit1.Date);
    CommonSet.DateTo:=Trunc(fmSelPerSkl2.cxDateEdit2.Date)+1;
    CommonSet.IdStore:=fmSelPerSkl2.cxLookupComboBox1.EditValue;
    CommonSet.NameStore:=fmSelPerSkl2.cxLookupComboBox1.Text;

    if CommonSet.DateTo>=iMaxDate then fmRepPrib.Caption:='����� �� ������� �� �� '+CommonSet.NameStore+' � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
    else fmRepPrib.Caption:='����� �� ������� �� �� '+CommonSet.NameStore+' �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);

    fmRepPrib.teMH.Active:=False;
    fmRepPrib.teMH.Active:=True;

    fmSelPerSkl2.quMHAll1.First;
    while not fmSelPerSkl2.quMHAll1.Eof do
    begin
      fmRepPrib.teMH.Append;
      fmRepPrib.teMHID.AsInteger:=fmSelPerSkl2.quMHAll1ID.AsInteger;
      fmRepPrib.teMHNAMEMH.AsString:=fmSelPerSkl2.quMHAll1NAMEMH.AsString;
      fmRepPrib.teMH.Post;

      fmSelPerSkl2.quMHAll1.Next;
    end;

    fmSelPerSkl2.quMHAll1.Active:=False;
    fmSelPerSkl2.Release;


    fmRepPrib.Show;  delay(10);//}
    fmRepPrib.prFormPrib;
  end else
  begin
    fmSelPerSkl2.quMHAll1.Active:=False;
    fmSelPerSkl2.Release;
  end;
end;

procedure TfmMainRnOffice.acObVedExecute(Sender: TObject);
type TRSum = record
     qRemn,qIn,qRet,qVnIn,qVnOut,qOut,qInv,qInvIn,qInvOut,qRes:Real;
     sRemn,sIn,sRet,sVnIn,sVnOut,sOut,sInv,sInvIn,sInvOut,sRes:Real;
     end;

Var  bAdd:Boolean;
     iCount:Integer;
     arSum:TRSum;
     rQ,rQRec,rPr,rPrU,rPr0,rSum0:Real;
     S1,S2:String;

begin
//��������� ���������
  if not CanDo('prRepObVed') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;

  fmSelPerSkl:=tfmSelPerSkl.Create(Application);
  with fmSelPerSkl do
  begin
    cxDateEdit1.Date:=CommonSet.DateFrom;
    quMHAll1.Active:=False;
    quMHAll1.ParamByName('IDPERSON').AsInteger:=Person.Id;
    quMHAll1.Active:=True;
    quMHAll1.First;
    cxLookupComboBox1.EditValue:=quMHAll1ID.AsInteger;
  end;
  fmSelPerSkl.ShowModal;
  if fmSelPerSkl.ModalResult=mrOk then
  begin
    CommonSet.DateFrom:=Trunc(fmSelPerSkl.cxDateEdit1.Date);
    CommonSet.DateTo:=Trunc(fmSelPerSkl.cxDateEdit2.Date)+1;
    CommonSet.IdStore:=fmSelPerSkl.cxLookupComboBox1.EditValue;
    CommonSet.NameStore:=fmSelPerSkl.cxLookupComboBox1.Text;

    if CommonSet.DateTo>=iMaxDate then fmRepOb.Caption:='��������� ��������� �� �� '+CommonSet.NameStore+' � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
    else fmRepOb.Caption:='��������� ��������� �� �� '+CommonSet.NameStore+' �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);
    fmSelPerSkl.quMHAll1.Active:=False;
    fmSelPerSkl.Release;

    with dmO do
    with dmORep do
    begin
      fmRepOb.Show;  delay(10);//}
      with fmRepOb do
      begin
        Memo1.Lines.Add('����� .. ���� ������������ ������.'); delay(10);
        ViewObVed.BeginUpdate;

        CloseTe(fmRepOb.teObVed);

        quAllCards.Active:=False;
        quAllCards.Active:=True;

        dsquAllCards.DataSet:=dmO.quAllCards;
        PBar1.Properties.Min:=0;
        PBar1.Properties.Max:=quAllCards.RecordCount;
        iCount:=0;
        PBar1.Position:=iCount;
        PBar1.Visible:=True;

        quAllCards.First;
        while not quAllCards.Eof do
        begin
          bAdd:=False;
          //� �������
          arSum.qRemn:=prCalcRemn(quAllCardsID.AsInteger,Trunc(CommonSet.DateFrom-1),CommonSet.IdStore);

          quMovePer.Active:=False;
          quMovePer.ParamByName('IDGOOD').AsInteger:=quAllCardsID.AsInteger;
          quMovePer.ParamByName('DATEB').AsInteger:=Trunc(CommonSet.DateFrom);
          quMovePer.ParamByName('DATEE').AsInteger:=Trunc(CommonSet.DateTo);
          quMovePer.ParamByName('IDSTORE').AsInteger:=CommonSet.IdStore;
          quMovePer.Active:=True;

          arSum.qIn:=round(quMovePerPOSTIN.AsFloat*1000)/1000;
          arSum.qRet :=round(quMovePerPOSTOUT.AsFloat*1000)/1000;
          arSum.qVnIn :=round(quMovePerVNIN.AsFloat*1000)/1000;
          arSum.qVnOut :=round(quMovePerVNOUT.AsFloat*1000)/1000;
          arSum.qOut:=round(quMovePerQREAL.AsFloat*1000)/1000;
          arSum.qInv:=round(quMovePerINV.AsFloat*1000)/1000;

          if abs(arSum.qRemn)>=0.001 then bAdd:=True;
          if abs(arSum.qIn)>=0.001 then bAdd:=True;
          if abs(arSum.qRet)>=0.001 then bAdd:=True;
          if abs(arSum.qVnIn)>=0.001 then bAdd:=True;
          if abs(arSum.qVnOut)>=0.001 then bAdd:=True;
          if abs(arSum.qOut)>=0.001 then bAdd:=True;
          if abs(arSum.qInv)>=0.001 then bAdd:=True;

          quMovePer.Active:=False;

          if bAdd then
          begin
            //������������ �� ��������� �������
            //��� � ��������
            arSum.sRemn:=prCalcRemnSumF(quAllCardsID.AsInteger,Trunc(CommonSet.DateFrom-1),CommonSet.IdStore,arSum.qRemn,rSum0);
            //��� � �������
            if quAllCardsKOEF.AsFloat<>0 then arSum.qRemn:=arSum.qRemn/quAllCardsKOEF.AsFloat; //�� �������� ����� � �������

            if quAllCardsKOEF.AsFloat<>0 then
            begin
             arSum.qIn:=arSum.qIn/quAllCardsKOEF.AsFloat;
             arSum.qRet:=arSum.qRet/quAllCardsKOEF.AsFloat;
             arSum.qVnIn:=arSum.qVnIn/quAllCardsKOEF.AsFloat;
             arSum.qVnOut:=arSum.qVnOut/quAllCardsKOEF.AsFloat;
             arSum.qOut:=arSum.qOut/quAllCardsKOEF.AsFloat;
             arSum.qInv:=arSum.qInv/quAllCardsKOEF.AsFloat;
            end;

            if arSum.qInv>=0 then begin arSum.qInvIn:=arSum.qInv; arSum.qInvOut:=0; end;
            if arSum.qInv<0 then begin arSum.qInvIn:=0; arSum.qInvOut:=(-1)*arSum.qInv; end;

            arSum.sIn:=0;arSum.sVnIn:=0;arSum.sInvIn:=0;
            arSum.sRet:=0;arSum.sVnOut:=0;arSum.sInvOut:=0;arSum.sOut:=0;
            rPr:=0; rPrU:=0;

            //������

            if arSum.qVnIn>0 then
            begin
              quSelInSum.Active:=False;
              quSelInSum.SelectSQL.Clear;
              quSelInSum.SelectSQL.Add('SELECT IDATE,QPART,PRICEIN');
              quSelInSum.SelectSQL.Add('FROM OF_PARTIN');
              quSelInSum.SelectSQL.Add('where IDATE>='+IntToStr(Trunc(CommonSet.DateFrom))+' and IDATE<='+IntToStr(Trunc(CommonSet.DateTo))+ ' and IDSTORE='+IntToStr(CommonSet.IdStore)+' and ARTICUL='+IntToStr(quAllCardsID.AsInteger)+' and DTYPE<>1 and DTYPE<>3');
              quSelInSum.SelectSQL.Add('Order by IDATE DESC');

              quSelInSum.Active:=True;
              quSelInSum.First;
             //���� ���������� ���� ���������. �� ����������� ���� ������� ������ ������ ��������� �������

              rQ:=arSum.qVnIn;
              arSum.sVnIn:=0;

              while (quSelInSum.Eof=False)and(rQ>0.001) do
              begin
                if rPr=0 then rPr:=quSelInSumPRICEIN.AsFloat;

                if rQ>=quSelInSumQPART.AsFloat then
                begin
                  rQRec:=quSelInSumQPART.AsFloat;
                  rQ:=rQ-rQRec;
                end else
                begin
                  rQRec:=rQ;
                  rQ:=0;
                end;
                arSum.sVnIn:=arSum.sVnIn+rQRec*quSelInSumPRICEIN.AsFloat*quAllCardsKOEF.AsFloat;

                quSelInSum.Next;
              end;
              if rQ>0 then //��� ���-�� �������� ��������
              begin
                if rPr=0 then prCalcLastPricePos(quAllCardsID.AsInteger,CommonSet.IdStore,Trunc(CommonSet.DateTo),rPr,rPrU,rPr0);
                arSum.sVnIn:=arSum.sVnIn+rQ*rPr*quAllCardsKOEF.AsFloat;
              end;

              quSelInSum.Active:=False;
            end;

            if arSum.qInvIn>0 then
            begin
              quSelInSum.Active:=False;
              quSelInSum.SelectSQL.Clear;
              quSelInSum.SelectSQL.Add('SELECT IDATE,QPART,PRICEIN');
              quSelInSum.SelectSQL.Add('FROM OF_PARTIN');
              quSelInSum.SelectSQL.Add('where IDATE>='+IntToStr(Trunc(CommonSet.DateFrom))+' and IDATE<='+IntToStr(Trunc(CommonSet.DateTo))+ ' and IDSTORE='+IntToStr(CommonSet.IdStore)+' and ARTICUL='+IntToStr(quAllCardsID.AsInteger)+' and DTYPE=3');
              quSelInSum.SelectSQL.Add('Order by IDATE DESC');

              quSelInSum.Active:=True;
              quSelInSum.First;
             //���� ���������� ���� ���������. �� ����������� ���� ������� ������ ������ ��������� �������

              rQ:=arSum.qInvIn;
              arSum.sInvIn:=0;
              rPr:=0; rPrU:=0;

              while (quSelInSum.Eof=False)and(rQ>0.001) do
              begin
                if rPr=0 then rPr:=quSelInSumPRICEIN.AsFloat;

                if rQ>=quSelInSumQPART.AsFloat then
                begin
                  rQRec:=quSelInSumQPART.AsFloat;
                  rQ:=rQ-rQRec;
                end else
                begin
                  rQRec:=rQ;
                  rQ:=0;
                end;
                arSum.sInvIn:=arSum.sInvIn+rQRec*quSelInSumPRICEIN.AsFloat*quAllCardsKOEF.AsFloat;

                quSelInSum.Next;
              end;
              if rQ>0 then //��� ���-�� �������� ��������
              begin
                if rPr=0 then prCalcLastPricePos(quAllCardsID.AsInteger,CommonSet.IdStore,Trunc(CommonSet.DateTo),rPr,rPrU,rPr0);
                arSum.sInvIn:=arSum.sInvIn+rQ*rPr*quAllCardsKOEF.AsFloat;
              end;

              quSelInSum.Active:=False;
            end;

            if arSum.qIn>0 then
            begin
              quSelInSum.Active:=False;
              quSelInSum.SelectSQL.Clear;
              quSelInSum.SelectSQL.Add('SELECT IDATE,QPART,PRICEIN');
              quSelInSum.SelectSQL.Add('FROM OF_PARTIN');
              quSelInSum.SelectSQL.Add('where IDATE>='+IntToStr(Trunc(CommonSet.DateFrom))+' and IDATE<='+IntToStr(Trunc(CommonSet.DateTo))+ ' and IDSTORE='+IntToStr(CommonSet.IdStore)+' and ARTICUL='+IntToStr(quAllCardsID.AsInteger)+' and DTYPE=1');
              quSelInSum.SelectSQL.Add('Order by IDATE DESC');
              quSelInSum.Active:=True;
              quSelInSum.First;
             //���� ���������� ���� ���������. �� ����������� ���� ������� ������ ������ ��������� �������

              rQ:=arSum.qIn;
              arSum.sIn:=0;
              rPr:=0; rPrU:=0;

              while (quSelInSum.Eof=False)and(rQ>0.001) do
              begin
                if rPr=0 then rPr:=quSelInSumPRICEIN.AsFloat;

                if rQ>=quSelInSumQPART.AsFloat then
                begin
                  rQRec:=quSelInSumQPART.AsFloat;
                  rQ:=rQ-rQRec;
                end else
                begin
                  rQRec:=rQ;
                  rQ:=0;
                end;
                arSum.sIn:=arSum.sIn+rQRec*quSelInSumPRICEIN.AsFloat*quAllCardsKOEF.AsFloat;

                quSelInSum.Next;
              end;
              if rQ>0 then //��� ���-�� �������� ��������
              begin
                if rPr=0 then prCalcLastPricePos(quAllCardsID.AsInteger,CommonSet.IdStore,Trunc(CommonSet.DateTo),rPr,rPrU,rPr0);
                arSum.sIn:=arSum.sIn+rQ*rPr*quAllCardsKOEF.AsFloat;
              end;
              quSelInSum.Active:=False;
            end;




{
SELECT IDDATE,QUANT,PRICEIN,SUMOUT
FROM OF_PARTOUT
where IDDATE>=:DATEB and IDDATE<=:DATEE and IDSTORE=:IDSKL and ARTICUL=:IDGOOD and DTYPE=1
order by IDDATE desc
}

// ������

            if arSum.qRet>0 then  //�������
            begin
              quSelOutSum.Active:=False;
              quSelOutSum.SelectSQL.Clear;
              quSelOutSum.SelectSQL.Add('SELECT IDDATE,QUANT,PRICEIN,SUMOUT');
              quSelOutSum.SelectSQL.Add('FROM OF_PARTOUT');
              quSelOutSum.SelectSQL.Add('where IDDATE>='+IntToStr(Trunc(CommonSet.DateFrom))+' and IDDATE<='+IntToStr(Trunc(CommonSet.DateTo))+ ' and IDSTORE='+IntToStr(CommonSet.IdStore)+' and ARTICUL='+IntToStr(quAllCardsID.AsInteger)+' and DTYPE=7');
              quSelOutSum.SelectSQL.Add('Order by IDDATE DESC');

              quSelOutSum.Active:=True;
              quSelOutSum.First;
              rQ:=arSum.qRet; arSum.sRet:=0;

              while (quSelOutSum.Eof=False)and(rQ>0.001) do
              begin
                if rQ>=quSelOutSumQUANT.AsFloat then
                begin
                  rQRec:=quSelOutSumQUANT.AsFloat;
                  rQ:=rQ-rQRec;
                end else
                begin
                  rQRec:=rQ;
                  rQ:=0;
                end;
                arSum.sRet:=arSum.sRet+rQRec*quSelOutSumPRICEIN.AsFloat*quAllCardsKOEF.AsFloat;
                quSelOutSum.Next;
              end;
              if rQ>0.001 then //��� ���-�� �������� ��������
                arSum.sRet:=arSum.sRet+rQ*rPr*quAllCardsKOEF.AsFloat;
              quSelOutSum.Active:=False;
            end;

            if arSum.qOut>0 then //�������
            begin
              quSelOutSum.Active:=False;
              quSelOutSum.SelectSQL.Clear;
              quSelOutSum.SelectSQL.Add('SELECT IDDATE,QUANT,PRICEIN,SUMOUT');
              quSelOutSum.SelectSQL.Add('FROM OF_PARTOUT');
              quSelOutSum.SelectSQL.Add('where IDDATE>='+IntToStr(Trunc(CommonSet.DateFrom))+' and IDDATE<='+IntToStr(Trunc(CommonSet.DateTo))+ ' and IDSTORE='+IntToStr(CommonSet.IdStore)+' and ARTICUL='+IntToStr(quAllCardsID.AsInteger)+' and (DTYPE=2 or DTYPE=8)');
              quSelOutSum.SelectSQL.Add('Order by IDDATE DESC');

              quSelOutSum.Active:=True;
              quSelOutSum.First;
              rQ:=arSum.qOut; arSum.sOut:=0;

              while (quSelOutSum.Eof=False)and(rQ>0.001) do
              begin
                if rQ>=quSelOutSumQUANT.AsFloat then
                begin
                  rQRec:=quSelOutSumQUANT.AsFloat;
                  rQ:=rQ-rQRec;
                end else
                begin
                  rQRec:=rQ;
                  rQ:=0;
                end;
                arSum.sOut:=arSum.sOut+rQRec*quSelOutSumPRICEIN.AsFloat*quAllCardsKOEF.AsFloat;
                quSelOutSum.Next;
              end;
              if rQ>0.001 then //��� ���-�� �������� ��������
                arSum.sOut:=arSum.sOut+rQ*rPr*quAllCardsKOEF.AsFloat;
              quSelOutSum.Active:=False;
            end;

            if arSum.qInvOut>0 then //��������������
            begin
              quSelOutSum.Active:=False;
              quSelOutSum.SelectSQL.Clear;
              quSelOutSum.SelectSQL.Add('SELECT IDDATE,QUANT,PRICEIN,SUMOUT');
              quSelOutSum.SelectSQL.Add('FROM OF_PARTOUT');
              quSelOutSum.SelectSQL.Add('where IDDATE>='+IntToStr(Trunc(CommonSet.DateFrom))+' and IDDATE<='+IntToStr(Trunc(CommonSet.DateTo))+ ' and IDSTORE='+IntToStr(CommonSet.IdStore)+' and ARTICUL='+IntToStr(quAllCardsID.AsInteger)+' and DTYPE=3');
              quSelOutSum.SelectSQL.Add('Order by IDDATE DESC');

              quSelOutSum.Active:=True;
              quSelOutSum.First;
              rQ:=arSum.qInvOut; arSum.sInvOut:=0;

              while (quSelOutSum.Eof=False)and(rQ>0.001) do
              begin
                if rQ>=quSelOutSumQUANT.AsFloat then
                begin
                  rQRec:=quSelOutSumQUANT.AsFloat;
                  rQ:=rQ-rQRec;
                end else
                begin
                  rQRec:=rQ;
                  rQ:=0;
                end;
                arSum.sInvOut:=arSum.sInvOut+rQRec*quSelOutSumPRICEIN.AsFloat*quAllCardsKOEF.AsFloat;
                quSelOutSum.Next;
              end;
              if rQ>0.001 then //��� ���-�� �������� ��������
                arSum.sInvOut:=arSum.sInvOut+rQ*rPr*quAllCardsKOEF.AsFloat;
              quSelOutSum.Active:=False;
            end;

            if arSum.qVnOut>0 then //���������� ������
            begin
              quSelOutSum.Active:=False;
              quSelOutSum.SelectSQL.Clear;
              quSelOutSum.SelectSQL.Add('SELECT IDDATE,QUANT,PRICEIN,SUMOUT');
              quSelOutSum.SelectSQL.Add('FROM OF_PARTOUT');
              quSelOutSum.SelectSQL.Add('where IDDATE>='+IntToStr(Trunc(CommonSet.DateFrom))+' and IDDATE<='+IntToStr(Trunc(CommonSet.DateTo))+ ' and IDSTORE='+IntToStr(CommonSet.IdStore)+' and ARTICUL='+IntToStr(quAllCardsID.AsInteger)+' and (DTYPE=4 or DTYPE=5 or DTYPE=6)');
              quSelOutSum.SelectSQL.Add('Order by IDDATE DESC');

              quSelOutSum.Active:=True;
              quSelOutSum.First;
              rQ:=arSum.qVnOut; arSum.sVnOut:=0;

              while (quSelOutSum.Eof=False)and(rQ>0.001) do
              begin
                if rQ>=quSelOutSumQUANT.AsFloat then
                begin
                  rQRec:=quSelOutSumQUANT.AsFloat;
                  rQ:=rQ-rQRec;
                end else
                begin
                  rQRec:=rQ;
                  rQ:=0;
                end;
                arSum.sVnOut:=arSum.sVnOut+rQRec*quSelOutSumPRICEIN.AsFloat*quAllCardsKOEF.AsFloat;
                quSelOutSum.Next;
              end;
              if rQ>0.001 then //��� ���-�� �������� ��������
                arSum.sVnOut:=arSum.sVnOut+rQ*rPr*quAllCardsKOEF.AsFloat;
              quSelOutSum.Active:=False;
            end;

            arSum.QRes:=arSum.qRemn+(arSum.qIn+arSum.qVnIn+arSum.qInvIn)-(arSum.qRet+arSum.qVnOut+arSum.qOut+arSum.qInvOut);
            //����� ��� � ������ ��������

            arSum.SRes:=prCalcRemnSumF(quAllCardsID.AsInteger,Trunc(CommonSet.DateTo-1),CommonSet.IdStore,(arSum.QRes*quAllCardsKOEF.AsFloat),rSum0);

            prFind2group(quAllCardsPARENT.AsInteger,S1,S2);

            teObVed.Append;
            teObVedIdCode.AsInteger:=quAllCardsID.AsInteger;
            teObVedNameC.AsString:=quAllCardsNAME.AsString;
            teObVediM.AsInteger:=quAllCardsIMESSURE.AsInteger;
            teObVedSm.AsString:=quAllCardsNAMESHORT.AsString;
            teObVedIdGroup.AsInteger:=quAllCardsPARENT.AsInteger;
            teObVedNameGr1.AsString:=S1;
            teObVedNameGr2.AsString:=S2;
            teObVedQBeg.AsFloat:=arSum.qRemn;
            teObVedSBeg.AsFloat:=arSum.sRemn;
            teObVedQIn.AsFloat:=arSum.qIn+arSum.qVnIn+arSum.qInvIn;
            teObVedSIn.AsFloat:=arSum.sIn+arSum.sVnIn+arSum.sInvIn;
            teObVedQOut.AsFloat:=arSum.qRet+arSum.qVnOut+arSum.qOut+arSum.qInvOut;
            teObVedSOut.AsFloat:=arSum.sRet+arSum.sVnOut+arSum.sOut+arSum.sInvOut;
            teObVedQRes.AsFloat:=arSum.QRes;
            teObVedSRes.AsFloat:=arSum.SRes;
            teObVedPrice.AsFloat:=rPr;
            teObVedKm.AsFloat:=quAllCardsKOEF.AsFloat;
            teObVed.Post;

          end;

          quAllCards.Next;
          inc(iCount);
          PBar1.Position:=iCount; delay(10);
        end;

        dsquAllCards.DataSet:=Nil;
        PBar1.Visible:=False;

        ViewObVed.EndUpdate;
      end;
      fmRepOb.Memo1.Lines.Add('������������ ��.'); delay(10);
    end;
  end else
  begin
    fmSelPerSkl.quMHAll1.Active:=False;
    fmSelPerSkl.Release;
  end;
end;

//      fmRepOb.Memo1.Lines.Add('  ������� �� ������.'); delay(10);

{      idPar:=GetId('Pars');
      taParams.Active:=False;
      taParams.Active:=True;
      taParams.Append;
      taParamsID.AsInteger:=idPar;
      taParamsIDATEB.AsInteger:=Trunc(CommonSet.DateFrom-1); //-1 �.�. ���� ������
      taParamsIDATEE.AsInteger:=Trunc(CommonSet.DateFrom-1);
      taParamsIDSTORE.AsInteger:=CommonSet.IdStore;
      taParams.Post;
      taParams.Active:=False;

      quRemnDate.Active:=False;
      quRemnDate.Active:=True;

      fmRepOb.Memo1.Lines.Add('  ��������� ��������.'); delay(10);

      fmRepOb.ViewObVed.BeginUpdate;
//      fmRepOb.taObVed.Active:=False;
      CloseTa(fmRepOb.taObVed);

      iCount:=0;
      quRemnDate.First;
      while not quRemnDate.Eof do
      begin
        with fmRepOb do
        begin
          bAdd:=False;
//          if quRemnDateTCARD.AsInteger=1 then  //��� �����
//          begin
          if abs(quRemnDateREMN.AsFloat)>0.0001 then bAdd:=True; //�� ������� ���� !!!
//          end
//          else bAdd:=True;

          if bAdd then
          begin
            //��� ��� ������� ����� ������� - ����� ���� �� ������ ���������
            //Function prCalcRemnSum(iCode,iDate,iSkl:Integer;rQ:Real):Real;
            //MOV.REMN/ME.KOEF
            rSum:=prCalcRemnSum(quRemnDateID.AsInteger,Trunc(CommonSet.DateFrom-1),CommonSet.IdStore);

            taObVed.Append;
            taObVedIdCode.AsInteger:=quRemnDateID.AsInteger;
            taObVedNameC.AsString:=quRemnDateNAME.AsString;
            taObVediM.AsInteger:=quRemnDateIMESSURE.AsInteger;
            taObVedSm.AsString:=quRemnDateNAMEMESSURE.AsString;
            taObVedIdGroup.AsInteger:=quRemnDatePARENT.AsInteger;
            taObVedNameClass.AsString:=quRemnDateNAMECL.AsString;
            taObVedQBeg.AsFloat:=quRemnDateREMN.AsFloat;
            taObVedSBeg.AsFloat:=rSum;
            taObVedQIn.AsFloat:=0;
            taObVedSIn.AsFloat:=0;
            taObVedQOut.AsFloat:=0;
            taObVedSOut.AsFloat:=0;
            taObVedPrice.AsFloat:=0;
            taObVedKm.AsFloat:=quRemnDateKOEF.AsFloat;
            taObVed.Post;
          end;
        end;
        quRemnDate.Next; delay(10); inc(iCount);
        if (iCount mod 100)=0 then
        begin
          fmRepOb.Memo1.Lines.Add('  ���������� - '+IntToStr(iCount)); delay(10);
        end;
      end;

      quRemnDate.Active:=False;

      fmRepOb.Memo1.Lines.Add('  ��������� ��������.'); delay(10);

      quSelInSum.Active:=False;
      quSelInSum.ParamByName('DATEB').AsInteger:=Trunc(CommonSet.DateFrom);
      quSelInSum.ParamByName('DATEE').AsInteger:=Trunc(CommonSet.DateTo);
      quSelInSum.ParamByName('IDSKL').AsInteger:=CommonSet.IdStore;
      quSelInSum.Active:=True;

      iCount:=0;

      quSelInSum.First;
      while not quSelInSum.Eof do
      begin
        with fmRepOb do
        begin
          if taObVed.Locate('IdCode',quSelInSumARTICUL.AsInteger,[]) then
          begin
            rKm:=taObVedKm.AsFloat;
            taObVed.Edit;
            if rKm<>0 then taObVedQIn.AsFloat:=quSelInSumQSUM.AsFloat/rKm
            else taObVedQIn.AsFloat:=0;
            taObVedSIn.AsFloat:=quSelInSumSUMIN.AsFloat;
            taObVed.Post;
          end else
          begin  //���� ���������
            quSelNamePos.Active:=False;
            quSelNamePos.ParamByName('IDCARD').AsInteger:=quSelInSumARTICUL.AsInteger;
            quSelNamePos.Active:=True;

            quSelNamePos.First;
            if quSelNamePos.RecordCount>0 then
            begin
              rKm:=quSelNamePosKOEF.AsFloat;
              taObVed.Append;
              taObVedIdCode.AsInteger:=quSelInSumARTICUL.AsInteger;
              taObVedNameC.AsString:=quSelNamePosNAME.AsString;
              taObVediM.AsInteger:=quSelNamePosIMESSURE.AsInteger;
              taObVedSm.AsString:=quSelNamePosSM.AsString;
              taObVedIdGroup.AsInteger:=quSelNamePosPARENT.AsInteger;
              taObVedNameClass.AsString:=quSelNamePosNAMECL.AsString;
              taObVedQBeg.AsFloat:=0;
              taObVedSBeg.AsFloat:=0;
              taObVedQIn.AsFloat:=0;

              if rKm<>0 then taObVedQIn.AsFloat:=quSelInSumQSUM.AsFloat/rKm;
              taObVedSIn.AsFloat:=quSelInSumSUMIN.AsFloat;

              taObVedQOut.AsFloat:=0;
              taObVedSOut.AsFloat:=0;
              taObVedPrice.AsFloat:=0;
              taObVedKm.AsFloat:=quSelNamePosKOEF.AsFloat;
              taObVed.Post;
            end;
            quSelNamePos.Active:=False;
          end;
        end;
        quSelInSum.Next;  inc(iCount); delay(10);
        if (iCount mod 100)=0 then
        begin
          fmRepOb.Memo1.Lines.Add('  ���������� - '+IntToStr(iCount)); delay(10);
        end;
      end;
      quSelInSum.Active:=False;

      fmRepOb.Memo1.Lines.Add('  ��������� ��������.'); delay(10);

      quSelOutSum.Active:=False;
      quSelOutSum.ParamByName('DATEB').AsInteger:=Trunc(CommonSet.DateFrom);
      quSelOutSum.ParamByName('DATEE').AsInteger:=Trunc(CommonSet.DateTo);
      quSelOutSum.ParamByName('IDSKL').AsInteger:=CommonSet.IdStore;
      quSelOutSum.Active:=True;

      iCount:=0;

      quSelOutSum.First;
      while not quSelOutSum.Eof do
      begin
        with fmRepOb do
        begin
          if taObVed.Locate('IdCode',quSelOutSumARTICUL.AsInteger,[]) then
          begin
            rKm:=taObVedKm.AsFloat;
            taObVed.Edit;
            if rKm<>0 then taObVedQOut.AsFloat:=quSelOutSumQSUM.AsFloat/rKm
            else taObVedQOut.AsFloat:=0;
            taObVedSOut.AsFloat:=quSelOutSumSUMOUT.AsFloat;
            taObVed.Post;
          end else
          begin  //���� ���������
            quSelNamePos.Active:=False;
            quSelNamePos.ParamByName('IDCARD').AsInteger:=quSelOutSumARTICUL.AsInteger;
            quSelNamePos.Active:=True;

            quSelNamePos.First;
            if quSelNamePos.RecordCount>0 then
            begin
              rKm:=quSelNamePosKOEF.AsFloat;
              taObVed.Append;
              taObVedIdCode.AsInteger:=quSelOutSumARTICUL.AsInteger;
              taObVedNameC.AsString:=quSelNamePosNAME.AsString;
              taObVediM.AsInteger:=quSelNamePosIMESSURE.AsInteger;
              taObVedSm.AsString:=quSelNamePosSM.AsString;
              taObVedIdGroup.AsInteger:=quSelNamePosPARENT.AsInteger;
              taObVedNameClass.AsString:=quSelNamePosNAMECL.AsString;
              taObVedQBeg.AsFloat:=0;
              taObVedSBeg.AsFloat:=0;
              taObVedQIn.AsFloat:=0;
              taObVedSIn.AsFloat:=0;

              taObVedQOut.AsFloat:=0;
              if rKm<>0 then taObVedQOut.AsFloat:=quSelOutSumQSUM.AsFloat/rKm;
              taObVedSOut.AsFloat:=quSelOutSumSUMOut.AsFloat;

              taObVedPrice.AsFloat:=0;
              taObVedKm.AsFloat:=quSelNamePosKOEF.AsFloat;
              taObVed.Post;
            end;
            quSelNamePos.Active:=False;
          end;
        end;
        quSelOutSum.Next; inc(iCount); delay(10);
        if (iCount mod 100)=0 then
        begin
          fmRepOb.Memo1.Lines.Add('  ���������� - '+IntToStr(iCount)); delay(10);
        end;
      end;
      quSelOutSum.Active:=False;

      fmRepOb.ViewObVed.EndUpdate;
      }



procedure TfmMainRnOffice.acKomplektExecute(Sender: TObject);
begin
//������������
  if CommonSet.DateTo>=iMaxDate then fmDocsCompl.Caption:='������������ ���� (���� ����) � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
  else fmDocsCompl.Caption:='������������ ���� (���� ����) �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo);
  with dmORep do
  begin
    quDocsCompl.Active:=False;
    quDocsCompl.ParamByName('DATEB').AsDate:=CommonSet.DateFrom;
    quDocsCompl.ParamByName('DATEE').AsDate:=CommonSet.DateTo;
    quDocsCompl.ParamByName('IDPERSON').AsInteger:=Person.Id;
    quDocsCompl.ParamByName('IDPERSON1').AsInteger:=Person.Id;
    quDocsCompl.Active:=True;
  end;
  fmDocsCompl.Show;
end;

procedure TfmMainRnOffice.acInDocsExecute(Sender: TObject);
begin
  //���������� ���������
  if CommonSet.DateTo>=iMaxDate then fmDocsVn.Caption:='���������� ��������� � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
  else fmDocsVn.Caption:='���������� ��������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);
  with dmORep do
  begin
    quDocsVnSel.Active:=False;
    quDocsVnSel.ParamByName('DATEB').AsDate:=CommonSet.DateFrom;
    quDocsVnSel.ParamByName('DATEE').AsDate:=CommonSet.DateTo;
    quDocsVnSel.ParamByName('IDPERSON').AsInteger:=Person.Id;
    quDocsVnSel.ParamByName('IDPERSON1').AsInteger:=Person.Id;
    quDocsVnSel.Active:=True;
  end;
  fmDocsVn.LevelDocsVn.Visible:=True;
  fmDocsVn.LevelCardsVn.Visible:=False;
  fmDocsVn.SpeedItem3.Visible:=True;
  fmDocsVn.SpeedItem4.Visible:=True;
  fmDocsVn.SpeedItem5.Visible:=True;
  fmDocsVn.SpeedItem6.Visible:=True;
  fmDocsVn.SpeedItem7.Visible:=True;
  fmDocsVn.SpeedItem8.Visible:=True;

  fmDocsVn.Show;

end;

procedure TfmMainRnOffice.acActsExecute(Sender: TObject);
begin
//���� �����������
  if CommonSet.DateTo>=iMaxDate then fmDocsActs.Caption:='���� ����������� � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
  else fmDocsActs.Caption:='���� ����������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);
  with dmORep do
  begin
    quDocsActs.Active:=False;
    quDocsActs.ParamByName('DATEB').AsDate:=CommonSet.DateFrom;
    quDocsActs.ParamByName('DATEE').AsDate:=CommonSet.DateTo;
    quDocsActs.ParamByName('IDPERSON').AsInteger:=Person.Id;
    quDocsActs.Active:=True;
  end;
  fmDocsActs.LevelActs.Visible:=True;
  fmDocsActs.LevelCardsActs.Visible:=False;
  fmDocsActs.SpeedItem3.Visible:=True;
  fmDocsActs.SpeedItem4.Visible:=True;
  fmDocsActs.SpeedItem5.Visible:=True;
  fmDocsActs.SpeedItem6.Visible:=True;
  fmDocsActs.SpeedItem7.Visible:=True;
  fmDocsActs.SpeedItem8.Visible:=True;

  fmDocsActs.Show;
end;

procedure TfmMainRnOffice.amOperTExecute(Sender: TObject);
begin
  //��������
  //���� ���
  with dmORep do
  begin
    quOperT.Active:=False;
    quOperT.Active:=True;

    fmOperType:=TfmOperType.Create(Application);
    fmOperType.ShowModal;
    fmOperType.Release;
    quOperT.Active:=False;
  end;
end;

procedure TfmMainRnOffice.acRealExecute(Sender: TObject);
begin
//����������
  if CommonSet.DateTo>=iMaxDate then fmDocsReal.Caption:='���������� �� ������� � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
  else fmDocsReal.Caption:='���������� �� ������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo);
  with dmORep do
  begin
    fmDocsReal.LevelDocsR.Visible:=True;
    fmDocsReal.LevelCardsR.Visible:=False;
    fmDocsReal.Memo1.Clear;

    fmDocsReal.SpeedItem3.Visible:=True;
    fmDocsReal.SpeedItem4.Visible:=True;
    fmDocsReal.SpeedItem5.Visible:=True;
    fmDocsReal.SpeedItem6.Visible:=True;
    fmDocsReal.SpeedItem7.Visible:=True;
    fmDocsReal.SpeedItem8.Visible:=True;
    fmDocsReal.SpeedItem10.Visible:=False;

    fmDocsReal.Show;
    fmDocsReal.Memo1.Lines.Add('����� ���� ������������ ������ ...');
    delay(10);

    fmDocsReal.ViewDocsR.BeginUpdate;
    prFormQuDocsR;
    fmDocsReal.ViewDocsR.EndUpdate;

    fmDocsReal.Memo1.Lines.Add('��.');
    delay(10);
  end;
end;

procedure TfmMainRnOffice.acRecalcPerExecute(Sender: TObject);
begin
//������������ ������
  if not CanDo('prRecalcPer') then begin ShowMessage('��� ����.'); exit; end;
  fmRecalcPer:=tfmRecalcPer.Create(Application);
  fmRecalcPer.Memo1.Clear;
  fmRecalcPer.cxDateEdit1.Date:=Date;
  with dmO do
  begin
    if quMHAll.Active=False then
    begin
       quMHAll.ParamByName('IDPERSON').AsInteger:=Person.Id;
       quMHAll.Active:=True;
    end;
    quMHAll.FullRefresh;
    quMHAll.First;
    if quMHAll.RecordCount>1 then fmRecalcPer.cxCheckBox2.Visible:=True else fmRecalcPer.cxCheckBox2.Visible:=False;
    fmRecalcPer.cxLookupComboBox2.EditValue:=quMHAllID.AsInteger;
    fmRecalcPer.cxCheckBox2.Checked:=False;
  end;
  fmRecalcPer.ShowModal;
  fmRecalcPer.Release;
end;

procedure TfmMainRnOffice.acRecalcRealExecute(Sender: TObject);
begin
  if not CanDo('prRecalcPer1') then begin ShowMessage('��� ����.'); exit; end;
  fmRecalcPer1:=tfmRecalcPer1.Create(Application);
  fmRecalcPer1.Memo1.Clear;
  fmRecalcPer1.cxDateEdit1.Date:=Date;
  fmRecalcPer1.cxDateEdit2.Date:=Date;
  with dmO do
  begin
    if quMHAll.Active=False then
    begin
       quMHAll.ParamByName('IDPERSON').AsInteger:=Person.Id;
       quMHAll.Active:=True;
    end;
    quMHAll.FullRefresh;
    quMHAll.First;
    fmRecalcPer1.cxLookupComboBox2.EditValue:=quMHAllID.AsInteger;
  end;
  fmRecalcPer1.ShowModal;
  fmRecalcPer1.Release;
end;

procedure TfmMainRnOffice.acRepPostExecute(Sender: TObject);
begin
  //������� �� �����������
  if not CanDo('prRepPost') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;

  fmParamSel:=TfmParamSel.Create(Application);
  fmParamSel.cxButtonEdit1.Text:=CurVal.SCLASS;
  fmParamSel.cxButtonEdit1.Tag:=CurVal.ICLASS;
  if CurVal.ICLALL=1 then fmParamSel.cxCheckBox1.Checked:=True else fmParamSel.cxCheckBox1.Checked:=False;
  fmParamSel.ShowModal;
  if fmParamSel.ModalResult=mrOk then
  begin
    CurVal.SCLASS:=fmParamSel.cxButtonEdit1.Text;
    CurVal.ICLASS:=fmParamSel.cxButtonEdit1.Tag;
    if fmParamSel.cxCheckBox1.Checked then CurVal.ICLALL:=1 else CurVal.ICLALL:=0;

    fmParamSel.Release;

    fmRepPost.Show;  delay(10);//}
    fmRepPost.Memo1.Lines.Add('����� .. ���� ������������ ������.'); delay(10);
    fmRepPost.Memo1.Lines.Add('  ������� �� �����������.'); delay(10);
    fmRepPrib.Memo1.Lines.Add('  ��������� ������.'); delay(10);
    with fmRepPost do
    begin
      ViewPost.BeginUpdate;
      dmO.quMH_.Active:=False;
      dmO.quMH_.Active:=True;

      CloseTa(taPost);
      if CurVal.ICLALL=0 then  //�� ����� ������
      begin
        prFindPartRemn(CurVal.ICLASS);
      end else  //�� ���� ������
      begin
        with dmORep do
        begin
          quMainClass.Active:=False;
          quMainClass.Active:=True;
          quMainClass.First;
          while not quMainClass.Eof do
          begin
            prFindPartRemn(quMainClassID.AsInteger);
            quMainClass.Next;
          end;
          quMainClass.Active:=False;
        end;
      end;
      dmO.quMH_.Active:=False;

      ViewPost.EndUpdate;
    end;

    fmRepPost.Memo1.Lines.Add('������������ ��.'); delay(10);

  end else
   fmParamSel.Release;

end;

procedure TfmMainRnOffice.Action1Execute(Sender: TObject);
begin
  //
  fmClassSel:=TfmClassSel.Create(Application);
  fmClassSel.ShowModal;
  fmClassSel.Release;
end;

procedure TfmMainRnOffice.acPartRemnExecute(Sender: TObject);
begin
//������������ ������
  if not CanDo('prRecalcPart') then begin ShowMessage('��� ����.'); exit; end;
  fmRecalcPer2:=tfmRecalcPer2.Create(Application);
  fmRecalcPer2.Memo1.Clear;
  with dmO do
  begin
    if quMHAll.Active=False then
    begin
       quMHAll.ParamByName('IDPERSON').AsInteger:=Person.Id;
       quMHAll.Active:=True;
    end;
    quMHAll.FullRefresh;
    quMHAll.First;
    fmRecalcPer2.cxLookupComboBox2.EditValue:=quMHAllID.AsInteger;
  end;
  fmRecalcPer2.ShowModal;
  fmRecalcPer2.Release;
end;

procedure TfmMainRnOffice.acAvansExecute(Sender: TObject);
begin
  //����� �� ������� � �������
  if not CanDo('prRepAvans') then begin ShowMessage('��� ����.'); exit; end;

  fmPeriodUni.DateTimePicker1.Date:=CommonSet.DateFrom;
  fmPeriodUni.ShowModal;
  if fmPeriodUni.ModalResult=mrOk then
  begin
    CommonSet.DateFrom:=Trunc(fmPeriodUni.DateTimePicker1.Date);
    CommonSet.DateTo:=Trunc(fmPeriodUni.DateTimePicker2.Date)+1;

    if fmRepAvans=nil then fmRepAvans:=tfmRepAvans.Create(Application);

    if CommonSet.DateTo>=iMaxDate then fmRepAvans.Caption:='������ � ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
    else fmRepAvans.Caption:='������ � ������ �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);

    fmRepAvans.Show;
    with fmRepAvans do
    begin
      Memo1.Clear;
      try
        Memo1.Lines.Add('����� ���� ������������ ������ ...');
        ViewAvans.BeginUpdate;
        quAv.Active:=False;
        quAv.ParamByName('DATEB').AsDateTime:=CommonSet.DateFrom;
        quAv.ParamByName('DATEE').AsDateTime:=CommonSet.DateTo;
        quAv.Active:=True;
      finally
        ViewAvans.EndUpdate;
        Memo1.Lines.Add('������������ ��.');
      end;
    end;
  end;
end;

procedure TfmMainRnOffice.TimerCloseTimer(Sender: TObject);
begin
  Close;
end;

procedure TfmMainRnOffice.acBGUExecute(Sender: TObject);
begin
  //����� ���� �������� - ����������


  fmSprBGU.Show;
end;

procedure TfmMainRnOffice.acTermoObrExecute(Sender: TObject);
begin
  //���� ���������
  fmTermoObr.ViewTermoObr.BeginUpdate;
  with dmO do
  begin
    taTermoObr.Active:=False;
    taTermoObr.Active:=True;
  end;
  fmTermoObr.ViewTermoObr.EndUpdate;
  fmTermoObr.Show;
end;

procedure TfmMainRnOffice.N1Click(Sender: TObject);
begin
  dmO.ColorDialog1.Color:=SpeedBar1.Color;
  if dmO.ColorDialog1.Execute then
  begin
    SpeedBar1.Color := dmO.ColorDialog1.Color;
//    StatusBar1.Color:= dmO.ColorDialog1.Color;
    UserColor.Main:=dmO.ColorDialog1.Color;
    WriteColor;
  end;
end;

procedure TfmMainRnOffice.acCategRExecute(Sender: TObject);
begin
//��������� ���� � �������
  //���� ���������
  fmRCategory.ViewCateg.BeginUpdate;
  with dmO do
  begin
    quCateg.Active:=False;
    quCateg.Active:=True;
  end;
  fmRCategory.ViewCateg.EndUpdate;
  fmRCategory.Show;
end;

procedure TfmMainRnOffice.acRemnSpeedRealExecute(Sender: TObject);
// �������
Var // IdPar:INteger;
    iDate:Integer;
    iC,iM:Integer;
    rQ,rQSpeed,rQDay:Real;
    S1,S2:String;
begin
  //������� �������� �������
  fmSelPerSkl:=tfmSelPerSkl.Create(Application);
  with fmSelPerSkl do
  begin
    if CommonSet.DateTo>=iMaxDate then CommonSet.DateTo:=Date;
    cxDateEdit1.Date:=CommonSet.DateTo;
    quMHAll1.Active:=False;
    quMHAll1.ParamByName('IDPERSON').AsInteger:=Person.Id;
    quMHAll1.Active:=True;
    quMHAll1.First;
    cxLookupComboBox1.EditValue:=quMHAll1ID.AsInteger;
    cxCheckBox1.Visible:=False;
    Label1.Caption:='�� �����';
  end;
  fmSelPerSkl.ShowModal;
  if fmSelPerSkl.ModalResult=mrOk then
  begin
//    CommonSet.DateFrom:=Trunc(fmSelPerSkl.cxDateEdit1.Date);
    CommonSet.DateTo:=Trunc(fmSelPerSkl.cxDateEdit1.Date)+1;
    iDate:=Trunc(fmSelPerSkl.cxDateEdit1.Date);
    CommonSet.IdStore:=fmSelPerSkl.cxLookupComboBox1.EditValue;
    CommonSet.NameStore:=fmSelPerSkl.cxLookupComboBox1.Text;
    fmSelPerSkl.quMHAll1.Active:=False;
    fmSelPerSkl.Release;

    fmRemnsSpeed.Caption:='������� �������� ������� �� �� '+CommonSet.NameStore;
    fmRemnsSpeed.LevelRemnsSpeed.Visible:=True;

    Cursor:=crDefault; Delay(10);
    fmRemnsSpeed.Memo1.Clear;
    fmRemnsSpeed.Memo1.Lines.Add('����� ... ���� ������������ ������.');

    fmRemnsSpeed.Show;
    Delay(100);

    fmRemnsSpeed.ViewRemnsSpeed.BeginUpdate;
    iC:=0;
    CloseTe(fmRemnsSpeed.teRemnsSp);

    with dmO do
    begin
      quRemnDate.Active:=False;
      quRemnDate.Active:=True;
      if quRemnDate.RecordCount>100 then iM:=Trunc(quRemnDate.RecordCount/100)
      else iM:=1;

      fmRemnsSpeed.PBar1.Properties.Min:=0;
      fmRemnsSpeed.PBar1.Properties.Max:=quRemnDate.RecordCount;
      fmRemnsSpeed.PBar1.Visible:=True;

      quRemnDate.First;
      while not quRemnDate.Eof do
      begin
        with fmRemnsSpeed do
        begin
//          rQ:=prCalcRemn(quRemnDateID.AsInteger,iDate,CommonSet.IdStore);
          rQ:=prCalcRemnSpeed(quRemnDateID.AsInteger,iDate,CommonSet.IdStore,rQSpeed,rQDay);

          if (abs(rQ)>=0.0001) or (rQSpeed>0) then
          begin
            prFind2group(quRemnDatePARENT.AsInteger,S1,S2);

            teRemnsSp.Append;
            teRemnsSpArticul.AsInteger:=quRemnDateID.AsInteger;
            teRemnsSpName.AsString:=quRemnDateNAME.AsString;
            teRemnsSpGr.AsString:=S1;
            teRemnsSpSGr.AsString:=S2;
            teRemnsSpIdM.AsInteger:=quRemnDateIMESSURE.AsInteger;
            teRemnsSpKM.AsFloat:=quRemnDateKOEF.AsFloat;
            teRemnsSpSM.AsString:=quRemnDateNAMESHORT.AsString;
            teRemnsSpTCard.AsInteger:=quRemnDateTCARD.AsInteger;
            if quRemnDateKOEF.AsFloat>0 then
            begin
              teRemnsSpQuant.AsFloat:=rQ/quRemnDateKOEF.AsFloat;
              teRemnsSpRealSpeed.AsFloat:=rQSpeed/quRemnDateKOEF.AsFloat;
            end
            else
            begin
              teRemnsSpQuant.AsFloat:=rQ;
              teRemnsSpRealSpeed.AsFloat:=rQSpeed;
            end;
            teRemnsSpCType.AsInteger:=quRemnDateCATEGORY.AsInteger;
            teRemnsSpQuantDay.AsFloat:=rQDay;
            teRemnsSp.Post;
          end;
        end;

        quRemnDate.Next;
        inc(iC);
        if iC mod iM = 0 then
        begin
          fmRemnsSpeed.PBar1.Position:=iC;
          delay(10);
        end;
      end;
    end;
    fmRemnsSpeed.PBar1.Position:=iC; Delay(100);
    fmRemnsSpeed.ViewRemnsSpeed.EndUpdate;
    fmRemnsSpeed.PBar1.Visible:=False;
    fmRemnsSpeed.Memo1.Lines.Add('������������ ��.');

  end else
  begin
    fmSelPerSkl.quMHAll1.Active:=False;
    fmSelPerSkl.Release;
  end;
end;

procedure TfmMainRnOffice.acImp1CDocsExecute(Sender: TObject);
begin
 //������ ����������
  if not CanDo('prImpDocR') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  try
    fmImportDocR:=TfmImportDocR.Create(Application);
    fmImportDocR.Label2.Caption:=CommonSet.PathImport+'doc*.txt';
    fmImportDocR.Memo1.Clear;
    fmImportDocR.ShowModal;
  finally
    fmImportDocR.Release;
  end;
end;

procedure TfmMainRnOffice.acClassAlgExecute(Sender: TObject);
begin
  //������������� ����� ���������
  with dmORep do
  begin
    try
      fmAlgClass:=tfmAlgClass.Create(Application);
      quAlgClass.Active:=False; quAlgClass.Active:=True;

      fmAlgClass.ShowModal;
    finally
      quAlgClass.Active:=False;

      fmAlgClass.Release;
    end;
  end;
end;

procedure TfmMainRnOffice.acMakersExecute(Sender: TObject);
begin
  with dmORep do
  begin
    fmMakers.ViewMakers.BeginUpdate;
    quMakers.Active:=False;
    quMakers.Active:=True;
    fmMakers.ViewMakers.EndUpdate;

    fmMakers.Show;
  end;
end;

procedure TfmMainRnOffice.acAlcogolExecute(Sender: TObject);
Var iStep,iC,iType:INteger;
    iDateB,iDateE,iDep:Integer;
    iMax:INteger;
    rQB,rQIn,rQR,rQRet,rQE:Real;
    kVol:Real;
    par:Variant;
    StrWk:String;
    iVb,iVe,IdDH:INteger;
    Sinn,Skpp:String;
    iCliCB:INteger;
    LicSerN,LicOrg:String;
    LicDateB,LicDateE:String;
    dDateB,dDateE:TDateTime;
begin                  // ����� �� ��������
  with dmO do
  with dmORep do
  begin

    fmPreAlg.quMHAll1.Active:=False;
    fmPreAlg.quMHAll1.Active:=True;
    fmPreAlg.quMHAll1.First;
    fmPreAlg.cxLookupComboBox2.EditValue:=fmPreAlg.quMHAll1ID.AsInteger;

    fmPreAlg.ShowModal;
    if fmPreAlg.ModalResult=mrOk then
    begin
      iDep:=fmPreAlg.cxLookupComboBox2.EditValue;
      iDateB:=Trunc(fmPreAlg.cxDateEdit1.Date);
      iDateE:=Trunc(fmPreAlg.cxDateEdit2.Date);
      dDateB:=iDateB;
      dDateE:=iDateE;

      fmPreAlg.quMHAll1.Active:=False;

      iType:=0;
      if fmPreAlg.cxRadioButton1.Checked then iType:=1;
      if fmPreAlg.cxRadioButton2.Checked then iType:=2;
      if fmPreAlg.cxRadioButton3.Checked then iType:=3;
      if fmPreAlg.cxRadioButton4.Checked then iType:=4;

      fmAlg.Memo1.Clear;


      fmAlg.PBar1.Position:=0;
      fmAlg.Caption:='����� �� �������� �� ������ c '+ds1(iDateB)+' �� '+ds1(iDateE);
      fmAlg.PBar1.Visible:=True;

      if (iType=1)or(iType=3) then
      begin
        fmAlg.LevelAlg1.Visible:=True;
        fmAlg.LevelAlg2.Visible:=False;
      end else
      begin
        fmAlg.LevelAlg1.Visible:=False;
        fmAlg.LevelAlg2.Visible:=True;
      end;

      CloseTe(fmAlg.teAlg1);
      CloseTe(fmAlg.teAlg2);

      fmAlg.Show;
      fmAlg.Memo1.Lines.Add('����� ... ���� ������');
      fmAlg.Memo1.Lines.Add('   ��� '+its(iType));
      fmAlg.Memo1.Lines.Add('   ����� '+its(iDep));

      bStopAlg:=False;

      fmAlg.Tag:=iType;

      if (iType=1)or(iType=3) then  //������ �����
      begin
        quCardsAlg.Active:=False;
        quCardsAlg.Active:=True;


        iMax:=quCardsAlg.RecordCount;
        iStep:=iMax div 100;

        if iStep=0 then iStep:=1;

        fmAlg.Memo1.Lines.Add('   ����� '+its(iMax));
        iC:=0;

        par := VarArrayCreate([0,1], varInteger);


        try
          fmAlg.ViewAlg1.BeginUpdate;

          quCardsAlg.First;
          while (quCardsAlg.Eof=False)and(bStopAlg=False) do
          begin
            //������ ��������
            if (quCardsAlg.Active) then //�������� �� ������ ������
            begin
              iVb:=0; iVe:=500;
              if iType=1 then begin iVb:=0; iVe:=500; end;
              if iType=3 then begin iVb:=500; iVe:=550; end;

              if (quCardsAlgALGCLASS.AsInteger>=iVb)and(quCardsAlgALGCLASS.AsInteger<=iVe) then
              begin //�������� ���� - ��� �������� ���������
               // kVol:=1;   //������� ��� ������ ���� �������� ������� � ������

                kVol:=quCardsAlgVOL.AsFloat;

                rQB:=prCalcRemn(quCardsAlgID.AsInteger,iDateB-1,iDep); //������� �� ����� ������� -1
                rQE:=prCalcRemn(quCardsAlgID.AsInteger,iDateE,iDep); //������� �� �����

                quQuantInOut.Active:=False;
                quQuantInOut.ParamByName('IDCARD').AsInteger:=quCardsAlgID.AsInteger;
                quQuantInOut.ParamByName('ISKL').AsInteger:=iDep;
                quQuantInOut.ParamByName('IDATEB').AsInteger:=iDateB;
                quQuantInOut.ParamByName('IDATEE').AsInteger:=iDateE;
                quQuantInOut.Active:=True;

                rQIn:=quQuantInOutQIN.AsFloat;
                rQRet:=quQuantInOutQOUT.AsFloat;

                quQuantInOut.Active:=False;

                rQR:=rQB+rQIn-rQRet-rQE; //���������� ��������� �����

                if (abs(rQB)>0.001)
                or (abs(rQIn)>0.001)
                or (abs(rQRet)>0.001)
                or (abs(rQR)>0.001)
                or (abs(rQE)>0.001) then
                begin //���� �������� - ����� ��������� � �������
                  par[0]:=quCardsAlgALGCLASS.AsInteger;
                  par[1]:=quCardsAlgALGMAKER.AsInteger;
                  with fmAlg do
                  begin
                    if teAlg1.Locate('iVid;ProdId',par,[]) then
                    begin //�����������
                      teAlg1.Edit;
                      teAlg1QuantB.AsFloat:=teAlg1QuantB.AsFloat+(rQB*kVol)/10;
                      teAlg1QuantIn.AsFloat:=teAlg1QuantIn.AsFloat+(rQIn*kVol)/10;
                      teAlg1QuantIn1.AsFloat:=teAlg1QuantIn1.AsFloat+(rQIn*kVol)/10;
                      teAlg1QuantIn2.AsFloat:=teAlg1QuantIn2.AsFloat+(rQIn*kVol)/10;
                      teAlg1QuantR.AsFloat:=teAlg1QuantR.AsFloat+(rQR*kVol)/10;
                      teAlg1QuantRet.AsFloat:=teAlg1QuantRet.AsFloat+(rQRet*kVol)/10;
                      teAlg1QuantOut.AsFloat:=teAlg1QuantOut.AsFloat+((rQR+rQRet)*kVol)/10;
                      teAlg1QuantE.AsFloat:=teAlg1QuantE.AsFloat+(rQE*kVol)/10;
                      teAlg1.Post;
                    end else
                    begin  //��������� ������
                      StrWk:=quCardsAlgALGCLASS.AsString;
                      while length(StrWk)<3 do StrWk:='0'+StrWk;

                      teAlg1.Append;
                      teAlg1sVid.AsString:=quCardsAlgNAMECLA.AsString;
                      teAlg1iVid.AsInteger:=quCardsAlgALGCLASS.AsInteger;
                      teAlg1sIVid.AsString:=StrWk;
                      teAlg1ProdId.AsInteger:=quCardsAlgALGMAKER.AsInteger;
                      teAlg1Prod.AsString:=quCardsAlgNAMEM.AsString;
                      teAlg1ProdInn.AsString:=quCardsAlgINNM.AsString;
                      teAlg1ProdKpp.AsString:=quCardsAlgKPPM.AsString;
                      teAlg1QuantB.AsFloat:=(rQB*kVol)/10;
                      teAlg1QuantIn.AsFloat:=(rQIn*kVol)/10;
                      teAlg1Col1_1.AsString:=' ';
                      teAlg1Col1_2.AsString:=' ';
                      teAlg1QuantIn1.AsFloat:=(rQIn*kVol)/10;
                      teAlg1Col1_3.AsString:=' ';
                      teAlg1Col1_4.AsString:=' ';
                      teAlg1Col1_5.AsString:=' ';
                      teAlg1QuantIn2.AsFloat:=(rQIn*kVol)/10;
                      teAlg1QuantR.AsFloat:=(rQR*kVol)/10;
                      teAlg1Col2_1.AsString:=' ';
                      teAlg1QuantRet.AsFloat:=(rQRet*kVol)/10;
                      teAlg1Col2_2.AsString:=' ';
                      teAlg1QuantOut.AsFloat:=((rQR+rQRet)*kVol)/10;
                      teAlg1QuantE.AsFloat:=(rQE*kVol)/10;
                      teAlg1.Post;
                    end;
                  end;
                end;
              end;
            end;

            quCardsAlg.Next; inc(iC);
            if iC mod iStep = 0 then
            begin
              fmAlg.PBar1.Position:=iC div iStep;
              delay(10);
            end;
          end;

        finally
          fmAlg.ViewAlg1.EndUpdate;
        end;
      end;

      if (iType=2)or(iType=4) then  //������ �����
      begin
        if quCardsAlg.Active=False then quCardsAlg.Active:=True;
        iMax:=quCardsAlg.RecordCount;
        iStep:=iMax div 100;

        if iStep=0 then iStep:=1;

        fmAlg.Memo1.Lines.Add('   ����� '+its(iMax));
        iC:=0;

        par := VarArrayCreate([0,2], varInteger);

        try
          fmAlg.ViewAlg2.BeginUpdate;

          quCardsAlg.First;
          while (quCardsAlg.Eof=False)and(bStopAlg=False) do
          begin
            if (quCardsAlg.Active) then //�������� �� ������ ������
            begin
              iVb:=0; iVe:=500;
              if iType=2 then begin iVb:=0; iVe:=500; end;
              if iType=4 then begin iVb:=500; iVe:=550; end;

              if (quCardsAlgALGCLASS.AsInteger>=iVb)and(quCardsAlgALGCLASS.AsInteger<=iVe) then
              begin //�������� ���� - ��� �������� ���������
//                kVol:=1;
                kVol:=quCardsAlgVOL.AsFloat;

{
where sp.IDCARD=:ICARD
and hd.IDSKL=:ISKL
and hd.IACTIVE=1
and hd.DATEDOC>=:DATEB
and hd.DATEDOC<=:DATEE
}
                quInLnAlg.Active:=False;
                quInLnAlg.ParamByName('ICARD').AsInteger:=quCardsAlgID.AsInteger;
                quInLnAlg.ParamByName('ISKL').AsInteger:=iDep;
                quInLnAlg.ParamByName('DATEB').AsDate:=dDateB;
                quInLnAlg.ParamByName('DATEE').AsDate:=dDateE;
                quInLnAlg.Active:=True;

                quInLnAlg.First;
                while not quInLnAlg.Eof do
                begin
                  IdDH:=quInLnAlgIDHEAD.AsInteger;

                  par[0]:=quCardsAlgALGCLASS.AsInteger;
                  par[1]:=quCardsAlgALGMAKER.AsInteger;
                  par[2]:=IdDH;

                  rQIn:=quInLnAlgQUANT.AsFloat*quInLnAlgKM.AsFloat;

                  with fmAlg do
                  begin
                    if teAlg2.Locate('iVid;ProdId;DocId',par,[]) then
                    begin //�����������
                      teAlg2.Edit;
                      teAlg2Quant.AsFloat:=teAlg2Quant.AsFloat+(rQIn*kVol)/10;
                      teAlg2.Post;
                    end else
                    begin  //��������� ������  -  ����� ���� ������
                      LicSerN:='';
                      LicOrg:='';
                      LicDateB:='';
                      LicDateE:='';

                      quCliLicMax.Active:=False;
                      quCliLicMax.ParamByName('ICLI').AsInteger:=quInLnAlgIDCLI.AsInteger;
                      quCliLicMax.Active:=True;
                      quCliLicMax.First;

                      if quCliLicMax.RecordCount>0 then
                      begin
                        LicSerN:=quCliLicMaxSER.AsString+' '+quCliLicMaxSNUM.AsString;
                        LicOrg:=quCliLicMaxORGAN.AsString;
                        LicDateB:=ds1(quCliLicMaxDDATEB.AsDateTime);
                        LicDateE:=ds1(quCliLicMaxDDATEE.AsDateTime);;
                      end;
                      quCliLicMax.Active:=False;


                      StrWk:=quCardsAlgALGCLASS.AsString;
                      while length(StrWk)<3 do StrWk:='0'+StrWk;

                      teAlg2.Append;
                      teAlg2sVid.AsString:=quCardsAlgNAMECLA.AsString;
                      teAlg2iVid.AsInteger:=quCardsAlgALGCLASS.AsInteger;
                      teAlg2sIVid.AsString:=StrWk;
                      teAlg2ProdId.AsInteger:=quCardsAlgALGMAKER.AsInteger;
                      teAlg2Prod.AsString:=quCardsAlgNAMEM.AsString;
                      teAlg2ProdInn.AsString:=quCardsAlgINNM.AsString;
                      teAlg2ProdKpp.AsString:=quCardsAlgKPPM.AsString;
                      teAlg2iCli.AsInteger:=quInLnAlgIDCLI.AsInteger;
                      teAlg2CliName.AsString:=quInLnAlgFULLNAMECL.AsString;
                      teAlg2CliInn.AsString:=quInLnAlgINN.AsString;
                      teAlg2CliKpp.AsString:=quInLnAlgKPP.AsString;
                      teAlg2LicSerN.AsString:=LicSerN;
                      teAlg2LicDateB.AsString:=LicDateB;
                      teAlg2LicDateE.AsString:=LicDateE;
                      teAlg2LicOrg.AsString:=LicOrg;
                      teAlg2DocId.AsInteger:=IdDh;
                      teAlg2DocDate.AsDateTime:=quInLnAlgDATEDOC.AsDateTime;
                      teAlg2DocNum.AsString:=quInLnAlgNUMDOC.AsString;
                      teAlg2DocGTD.AsString:=' ';
                      teAlg2Quant.AsFloat:=(rQIn*kVol)/10;
                      teAlg2.Post;
                    end;
                  end;

                  quInLnAlg.Next;
                end;

                quInLnAlg.Active:=False
              end;
            end;

            quCardsAlg.Next; inc(iC);
            if iC mod iStep = 0 then
            begin
              fmAlg.PBar1.Position:=iC div iStep;
              delay(10);
            end;
          end;

        finally
          fmAlg.ViewAlg2.EndUpdate;
        end;
      end;

      fmAlg.Memo1.Lines.Add('������ ��');

      fmAlg.PBar1.Position:=100;
      delay(1000);
      fmAlg.PBar1.Visible:=False;

    end else fmPreAlg.quMHAll1.Active:=False;
  end;
end;

procedure TfmMainRnOffice.acAlcoDeclExecute(Sender: TObject);
begin
  //���������� �� ��������
  //����� �����
  with dmORep do
  begin
    fmAlcoE.Caption:='������ ����������� ��������� �� ������ � '+ds1(CommonSet.ADateBeg)+' �� '+ds1(CommonSet.ADateEnd);
    fmAlcoE.Memo1.Clear;

    fmAlcoE.Show;

    fmAlcoE.Memo1.Lines.Add('����� ... ���� ������������ ������.'); delay(10);

    fmAlcoE.ViewAlco.BeginUpdate;
    quAlcoDH.Active:=False;
    quAlcoDH.ParamByName('IDATEB').AsInteger:=Trunc(CommonSet.ADateBeg);
    quAlcoDH.ParamByName('IDATEE').AsInteger:=Trunc(CommonSet.ADateEnd);
    quAlcoDH.Active:=True;
    fmAlcoE.ViewAlco.EndUpdate;

    fmAlcoE.Memo1.Lines.Add('������������ ������ ��.'); delay(10);

  end;
end;

procedure TfmMainRnOffice.acChangeSSExecute(Sender: TObject);
Var d1,d2:TDateTime;
    iYM:INteger;
    sM,sY:String;
begin
  // ����� �� ��������� ������������� ����
  //����� �� �������
  if not CanDo('prRepChSS') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;

  try
    fmPreRepSS:=tfmPreRepSS.Create(Application);
    sM:=FormatDateTime('mm',date);
    sY:=FormatDateTime('yyyy',date);
    iYM:=StrToIntDef(sY+sM,0);
    if iYM>0 then
    begin
      with fmPreRepSS do
      begin
        DToMBE(iYM,d1,d2);

        fmPreRepSS.cxDateEdit5.Date:=d1;
        fmPreRepSS.cxDateEdit6.Date:=d2;

        iYM:=DecrYYMM(iYM);

        DToMBE(iYM,d1,d2);

        fmPreRepSS.cxDateEdit3.Date:=d1;
        fmPreRepSS.cxDateEdit4.Date:=d2;

        iYM:=DecrYYMM(iYM);

        DToMBE(iYM,d1,d2);

        fmPreRepSS.cxDateEdit1.Date:=d1;
        fmPreRepSS.cxDateEdit2.Date:=d2;

        quMHAll1.Active:=False; quMHAll1.Active:=True; quMHAll1.First;
        if Prib.MHId>0 then cxLookupComboBox1.EditValue:=Prib.MHId else cxLookupComboBox1.EditValue:=quMHAll1ID.AsInteger;
        CheckBox1.Checked:=Prib.MHAll;

        quCateg.Active:=False; quCateg.Active:=True; quCateg.First;
        if Prib.Cat>0 then cxLookupComboBox3.EditValue:=Prib.Cat else cxLookupComboBox3.EditValue:=quCategID.AsInteger;
        CheckBox3.Checked:=Prib.CatAll;

        Prib.Oper:=5; //���������� �� �������

        cxLookupComboBox2.EditValue:=Prib.Oper;
        CheckBox2.Checked:=Prib.OperAll;
      end;
      fmPreRepSS.ShowModal;
      if fmPreRepSS.ModalResult=mrOk then
      begin
        fmPreRepSS.Release;
        delay(10);

        fmRepSS.Show;
        delay(10);

        fmRepSS.prFormSS;

      end else fmPreRepSS.Release;
    end;
  except
  end;
end;

procedure TfmMainRnOffice.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  prWriteStart('Close');
end;

procedure TfmMainRnOffice.acUpolnLicaExecute(Sender: TObject);
begin
  //�������������� ����
  with dmORep do
  begin
    fmUpLica.ViewUPL.BeginUpdate;
    quUPL.Active:=False;
    quUPL.ParamByName('IDPERSON').AsInteger:=Person.Id;
    quUPL.Active:=True;
    quUPL.First;
    fmUpLica.ViewUPL.EndUpdate;
    fmUpLica.Show;
  end;
end;

procedure TfmMainRnOffice.acDocsPriceExecute(Sender: TObject);
begin
  with dmORep do
  begin
    fmDocsPrice.Memo1.Clear;
    fmDocsPrice.Show;
    fmDocsPrice.Memo1.Lines.Add('����� .. ���� ������������.');  delay(10);
    fmDocsPrice.ViewDocsPrice.BeginUpdate;
    quDocsPrice.Active:=False;
    quDocsPrice.ParamByName('DATEB').AsInteger:=Trunc(CommonSet.DateFrom);
    quDocsPrice.ParamByName('DATEE').AsInteger:=Trunc(CommonSet.DateTo+1);
    quDocsPrice.Active:=True;
    fmDocsPrice.ViewDocsPrice.EndUpdate;
    fmDocsPrice.Memo1.Lines.Add('������������ ��.');  delay(10);
  end;
end;

procedure TfmMainRnOffice.acRepDocMoveExecute(Sender: TObject);
begin
  //����� - ������ ����������
  fmRepPars:=tfmRepPars.Create(Application);
  fmRepPars.cxDateEdit1.Date:=RDM.d1;
  fmRepPars.cxDateEdit2.Date:=RDM.d2;

  fmRepPars.quMHAll1.Active:=False;
  fmRepPars.quMHAll1.Active:=True;
  fmRepPars.quMHAll1.First;
  if RDM.MHId>0 then fmRepPars.quMHAll1.Locate('ID',RDM.MHId,[]);
  fmRepPars.cxLookupComboBox2.EditValue:=fmRepPars.quMHAll1ID.AsInteger;

  fmRepPars.cxCheckBox1.EditValue:=RDM.i1;
  fmRepPars.cxCheckBox2.EditValue:=RDM.i2;
  fmRepPars.cxCheckBox3.EditValue:=RDM.i3;
  fmRepPars.cxCheckBox4.EditValue:=RDM.i4;
  fmRepPars.cxCheckBox5.EditValue:=RDM.i5;
  fmRepPars.cxCheckBox6.EditValue:=RDM.i6;

  fmRepPars.ShowModal;
  if fmRepPars.ModalResult=mrOk then
  begin
    RDM.MHId:=fmRepPars.cxLookupComboBox2.EditValue;
    RDM.sMH:=fmRepPars.cxLookupComboBox2.Text;

    RDM.i1:=fmRepPars.cxCheckBox1.EditValue;
    RDM.i2:=fmRepPars.cxCheckBox2.EditValue;
    RDM.i3:=fmRepPars.cxCheckBox3.EditValue;
    RDM.i4:=fmRepPars.cxCheckBox4.EditValue;
    RDM.i5:=fmRepPars.cxCheckBox5.EditValue;
    RDM.i6:=fmRepPars.cxCheckBox6.EditValue;
    RDM.d1:=fmRepPars.cxDateEdit1.Date;
    RDM.d2:=fmRepPars.cxDateEdit2.Date;

    fmRepPars.Release;

    fmRepDocMove.Caption:='������ ��������� � ��������� ���������� �� ������ � '+ds1(RDM.d1)+' �� '+ds1(RDM.d2)+' ('+RDM.sMH+')';
    fmRepDocMove.Show;
    delay(100);
    //������������ ������
    fmRepDocMove.prFormDataRDM;

  end else fmRepPars.Release;
end;

procedure TfmMainRnOffice.acRealAkcizExecute(Sender: TObject);
Var rQ,rS,rS0:Real;
begin
  fmSelPerSkl:=tfmSelPerSkl.Create(Application);
  with fmSelPerSkl do
  begin
    cxDateEdit1.Date:=CommonSet.DateFrom;
    cxDateEdit2.Date:=CommonSet.DateTo;

    cxDateEdit2.Visible:=True;

    quMHAll1.Active:=False;
    quMHAll1.ParamByName('IDPERSON').AsInteger:=Person.Id;
    quMHAll1.Active:=True;
    quMHAll1.First;
    cxLookupComboBox1.EditValue:=quMHAll1ID.AsInteger;
    cxCheckBox1.Visible:=True;
    cxCheckBox1.Checked:=True;
//    Label1.Caption:='�� �����';
  end;
  fmSelPerSkl.ShowModal;
  if fmSelPerSkl.ModalResult=mrOk then
  begin
    CommonSet.DateFrom:=Trunc(fmSelPerSkl.cxDateEdit1.Date);
    CommonSet.DateTo:=Trunc(fmSelPerSkl.cxDateEdit2.Date);

    fmSelPerSkl.Release;

    with dmORep do
    begin
      quQD.Active:=False;
      quQD.Active:=True;

      CloseTe(teRepAkciz);

      quQD.First;
      while not quQD.Eof do
      begin
        teRepAkciz.Append;
        teRepAkciziCode.AsInteger:=quQDIDCARD.AsInteger;
        teRepAkciziCodeDoc.AsInteger:=quQDSETIDCARDS.AsInteger;
        teRepAkcizNameC.AsString:=quQDNAMEC.AsString;
        teRepAkcizSM.AsString:=quQDSM.AsString;
        teRepAkcizQuantDoc.AsFloat:=0;
        teRepAkcizQuant.AsFloat:=0;
        teRepAkcizrSumIn.AsFloat:=0;
        teRepAkcizrSumIn0.AsFloat:=0;
        teRepAkcizrSumOut.AsFloat:=0;
        teRepAkcizrSumOut0.AsFloat:=0;
        teRepAkcizrSumNDSOut.AsFloat:=0;
        teRepAkciz.Post;

        quQD.Next;
      end;

      quQD.Active:=False;

      quRepAkciz.Active:=False;
      quRepAkciz.ParamByName('DATEB').AsDateTime:=CommonSet.DateFrom;
      quRepAkciz.ParamByName('DATEE').AsDateTime:=CommonSet.DateTo;
      quRepAkciz.Active:=True;

      rQ:=0;
      rS:=0;
      rS0:=0;

      quRepAkciz.First;
      while not quRepAkciz.Eof do
      begin
        if teRepAkciz.Locate('iCode',quRepAkcizIDCARD.AsInteger,[]) then
        begin
          try
            teRepAkciz.Edit;
            teRepAkcizQuantDoc.AsFloat:=quRepAkcizQUANTDOC.AsFloat;
            teRepAkcizrSumIn.AsFloat:=rv(quRepAkcizSUMIN.AsFloat*quRepAkcizQUANTDOC.AsFloat/quRepAkcizQUANT.AsFloat);
            teRepAkcizrSumIn0.AsFloat:=rv(quRepAkcizSUMIN0.AsFloat*quRepAkcizQUANTDOC.AsFloat/quRepAkcizQUANT.AsFloat);
            teRepAkcizrSumOut.AsFloat:=rv(quRepAkcizSUMR.AsFloat*quRepAkcizQUANTDOC.AsFloat/quRepAkcizQUANT.AsFloat);
            teRepAkcizrSumOut0.AsFloat:=rv(quRepAkcizSUMOUT0.AsFloat*quRepAkcizQUANTDOC.AsFloat/quRepAkcizQUANT.AsFloat);
            teRepAkcizrSumNDSOut.AsFloat:=teRepAkcizrSumOut.AsFloat-teRepAkcizrSumOut0.AsFloat;
            teRepAkcizQuant.AsFloat:=quRepAkcizQUANT.AsFloat;
            teRepAkciz.Post;
          except
          end;
        end;

        rQ:=rQ+quRepAkcizQUANT.AsFloat-quRepAkcizQUANTDOC.AsFloat;
        rS:=rS+quRepAkcizSUMR.AsFloat-teRepAkcizrSumOut.AsFloat;
        rS0:=rS0+quRepAkcizSUMOUT0.AsFloat-teRepAkcizrSumOut0.AsFloat;

        quRepAkciz.Next;
      end;

      quRepAkciz.Active:=False;

      teRepAkciz.First;
      while not teRepAkciz.Eof do
      begin
        if teRepAkciziCode.AsInteger=5056 then
        begin
          teRepAkciz.Edit;
//          teRepAkcizNameC.AsString:=quRepAkcizNAMEC.AsString;
//          teRepAkcizSM.AsString:=quRepAkcizSM.AsString;
          teRepAkcizQuantDoc.AsFloat:=rQ;
//          teRepAkcizrSumIn.AsFloat:=rv(quRepAkcizSUMIN.AsFloat*quRepAkcizQUANTDOC.AsFloat/quRepAkcizQUANT.AsFloat);
//          teRepAkcizrSumIn0.AsFloat:=rv(quRepAkcizSUMIN0.AsFloat*quRepAkcizQUANTDOC.AsFloat/quRepAkcizQUANT.AsFloat);
          teRepAkcizrSumOut.AsFloat:=rS;
          teRepAkcizrSumOut0.AsFloat:=rS0;
          teRepAkcizrSumNDSOut.AsFloat:=rS-rS0;
          teRepAkcizQuant.AsFloat:=0;
          teRepAkciz.Post;
        end;

        teRepAkciz.Next;
      end;

//      teRepAkciz.First;
//      while not teRepAkciz.Eof do
 //     begin
//        if teRepAkcizQuantDoc.AsFloat<0.001 then teRepAkciz.Delete else teRepAkciz.Next;
//      end;

      frRep1.LoadFromFile(CurDir + 'RealAkcizTov.frf');

      frVariables.Variable['DATEB']:=FormatDateTime('dd.mm.yyyy',CommonSet.DateFrom);
      frVariables.Variable['DATEE']:=FormatDateTime('dd.mm.yyyy',CommonSet.DateTo);

      frRep1.ReportName:='���������� �������� �������.';
      frRep1.PrepareReport;
      frRep1.ShowPreparedReport;




    end;

  end else
  begin
    fmSelPerSkl.quMHAll1.Active:=False;
    fmSelPerSkl.Release;
  end;
end;

procedure TfmMainRnOffice.acDrvExecute(Sender: TObject);
begin
  //��������
  with dmORep do
  begin
    if quDrv.Active=False then quDrv.Active:=True else quDrv.FullRefresh;

    fmDrivers:=TfmDrivers.Create(Application);
    fmDrivers.ShowModal;
    fmDrivers.Release;
    
//    quDrv.Active:=False;
  end;
end;

procedure TfmMainRnOffice.acSelTermoPrExecute(Sender: TObject);
begin
  //��������� �������������
  fmTermoPr:=TfmTermoPr.Create(Application);
  fmTermoPr.cxTextEdit1.Text:=CommonSet.TPrintN;
  fmTermoPr.cxRadioGroup1.ItemIndex:=CommonSet.TPrintCode-1;
  fmTermoPr.ShowModal;
  fmTermoPr.Release;
end;

procedure TfmMainRnOffice.acChangeExecute(Sender: TObject);
begin
  fmChange.Show;
end;

end.
