object fmPreRepSS: TfmPreRepSS
  Left = 766
  Top = 453
  BorderStyle = bsDialog
  Caption = #1054#1090#1095#1077#1090' '#1087#1086' '#1080#1079#1084#1077#1085#1077#1085#1080#1102' '#1089#1077#1073#1077#1089#1086#1080#1084#1086#1089#1090#1080' '#1073#1083#1102#1076
  ClientHeight = 255
  ClientWidth = 463
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 20
    Top = 16
    Width = 85
    Height = 13
    Caption = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103' '
    Transparent = False
  end
  object Label2: TLabel
    Left = 20
    Top = 44
    Width = 50
    Height = 13
    Caption = #1054#1087#1077#1088#1072#1094#1080#1103
  end
  object Label3: TLabel
    Left = 20
    Top = 72
    Width = 53
    Height = 13
    Caption = #1050#1072#1090#1077#1075#1086#1088#1080#1103
  end
  object Label4: TLabel
    Left = 20
    Top = 104
    Width = 56
    Height = 13
    Caption = #1055#1077#1088#1080#1086#1076' 1 '#1089
  end
  object Label5: TLabel
    Left = 20
    Top = 136
    Width = 56
    Height = 13
    Caption = #1055#1077#1088#1080#1086#1076' 2 '#1089
  end
  object Label6: TLabel
    Left = 20
    Top = 164
    Width = 70
    Height = 13
    Caption = #1058#1077#1082'. '#1087#1077#1088#1080#1086#1076' '#1089
  end
  object Label7: TLabel
    Left = 264
    Top = 104
    Width = 12
    Height = 13
    Caption = #1087#1086
  end
  object Label8: TLabel
    Left = 264
    Top = 132
    Width = 15
    Height = 13
    Caption = #1087#1086' '
  end
  object Label9: TLabel
    Left = 264
    Top = 160
    Width = 12
    Height = 13
    Caption = #1087#1086
  end
  object Panel1: TPanel
    Left = 0
    Top = 201
    Width = 463
    Height = 54
    Align = alBottom
    BevelInner = bvLowered
    Color = 14803425
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 88
      Top = 12
      Width = 85
      Height = 33
      Caption = 'Ok'
      ModalResult = 1
      TabOrder = 0
      OnClick = cxButton1Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 248
      Top = 12
      Width = 85
      Height = 33
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      LookAndFeel.Kind = lfOffice11
    end
  end
  object cxLookupComboBox1: TcxLookupComboBox
    Left = 124
    Top = 12
    Properties.KeyFieldNames = 'ID'
    Properties.ListColumns = <
      item
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        FieldName = 'NAMEMH'
      end>
    Properties.ListOptions.AnsiSort = True
    Properties.ListSource = dsMHAll1
    Style.BorderStyle = ebsOffice11
    Style.LookAndFeel.Kind = lfOffice11
    Style.PopupBorderStyle = epbsDefault
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 1
    Width = 161
  end
  object CheckBox1: TcxCheckBox
    Left = 300
    Top = 12
    Caption = #1087#1086' '#1074#1089#1077#1084
    TabOrder = 2
    Width = 73
  end
  object CheckBox2: TcxCheckBox
    Left = 300
    Top = 40
    Caption = #1087#1086' '#1074#1089#1077#1084
    TabOrder = 3
    Width = 73
  end
  object cxLookupComboBox3: TcxLookupComboBox
    Left = 124
    Top = 68
    Properties.KeyFieldNames = 'ID'
    Properties.ListColumns = <
      item
        Caption = #1050#1072#1090#1077#1075#1086#1088#1080#1103
        FieldName = 'NAMECAT'
      end>
    Properties.ListOptions.AnsiSort = True
    Properties.ListSource = dsquCateg
    Style.BorderStyle = ebsOffice11
    Style.LookAndFeel.Kind = lfOffice11
    Style.PopupBorderStyle = epbsDefault
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 4
    Width = 161
  end
  object CheckBox3: TcxCheckBox
    Left = 300
    Top = 68
    Caption = #1087#1086' '#1074#1089#1077#1084
    TabOrder = 5
    Width = 73
  end
  object cxDateEdit1: TcxDateEdit
    Left = 124
    Top = 100
    TabOrder = 6
    Width = 121
  end
  object cxDateEdit2: TcxDateEdit
    Left = 292
    Top = 100
    TabOrder = 7
    Width = 121
  end
  object cxDateEdit3: TcxDateEdit
    Left = 124
    Top = 128
    TabOrder = 8
    Width = 121
  end
  object cxDateEdit4: TcxDateEdit
    Left = 292
    Top = 128
    TabOrder = 9
    Width = 121
  end
  object cxDateEdit5: TcxDateEdit
    Left = 124
    Top = 156
    TabOrder = 10
    Width = 121
  end
  object cxDateEdit6: TcxDateEdit
    Left = 292
    Top = 156
    TabOrder = 11
    Width = 121
  end
  object cxLookupComboBox2: TcxImageComboBox
    Left = 124
    Top = 40
    Properties.Items = <
      item
        Description = #1055#1088#1086#1076#1072#1078#1080
        ImageIndex = 0
        Value = 1
      end
      item
        Description = #1057#1087#1080#1089#1072#1085#1080#1077
        ImageIndex = 0
        Value = 2
      end
      item
        Description = #1062#1077#1093#1086#1074#1086#1077' '#1087#1080#1090#1072#1085#1080#1077
        ImageIndex = 0
        Value = 3
      end
      item
        Description = #1055#1088#1086#1095#1077#1077
        ImageIndex = 0
        Value = 4
      end
      item
        Description = #1056#1077#1072#1083#1080#1079#1072#1094#1080#1103' '#1085#1072' '#1089#1090#1086#1088#1086#1085#1091
        Value = 5
      end>
    Style.BorderStyle = ebsOffice11
    TabOrder = 12
    Width = 161
  end
  object quMHAll1: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT MH.ID,MH.PARENT,MH.ITYPE,MH.NAMEMH,'
      '       MH.DEFPRICE,PT.NAMEPRICE'
      'FROM OF_MH MH'
      'left JOIN OF_PRICETYPE PT ON PT.ID=MH.DEFPRICE'
      'WHERE MH.ITYPE=1'
      'Order by MH.ID')
    Transaction = dmO.trSelect
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    Left = 372
    Top = 76
    object quMHAll1ID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quMHAll1PARENT: TFIBIntegerField
      FieldName = 'PARENT'
    end
    object quMHAll1ITYPE: TFIBIntegerField
      FieldName = 'ITYPE'
    end
    object quMHAll1NAMEMH: TFIBStringField
      FieldName = 'NAMEMH'
      Size = 100
      EmptyStrToNull = True
    end
    object quMHAll1DEFPRICE: TFIBIntegerField
      FieldName = 'DEFPRICE'
    end
    object quMHAll1NAMEPRICE: TFIBStringField
      FieldName = 'NAMEPRICE'
      Size = 100
      EmptyStrToNull = True
    end
  end
  object dsMHAll1: TDataSource
    DataSet = quMHAll1
    Left = 420
    Top = 80
  end
  object quCateg: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    NAMECAT'
      'FROM'
      '    OF_CARDSRCATEGORY ')
    Transaction = dmO.trSelect
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    Left = 372
    Top = 124
    object quCategID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quCategNAMECAT: TFIBStringField
      FieldName = 'NAMECAT'
      Size = 100
      EmptyStrToNull = True
    end
  end
  object dsquCateg: TDataSource
    DataSet = quCateg
    Left = 416
    Top = 124
  end
end
