unit RemnsDay;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, Placemnt, ExtCtrls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  SpeedBar, ActnList, XPStyleActnCtrls, ActnMan, cxTextEdit,
  cxImageComboBox, cxCurrencyEdit, cxContainer, cxMemo, dxmdaset,
  cxProgressBar;

type
  TfmRemnsSpeed = class(TForm)
    StatusBar1: TStatusBar;
    Timer1: TTimer;
    FormPlacement1: TFormPlacement;
    GridRemnsSpeed: TcxGrid;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    amRemnsSpeed: TActionManager;
    acExit: TAction;
    Panel1: TPanel;
    acPerSkl: TAction;
    SpeedItem3: TSpeedItem;
    acExportEx: TAction;
    LevelRemnsSpeed: TcxGridLevel;
    ViewRemnsSpeed: TcxGridDBTableView;
    Memo1: TcxMemo;
    teRemnsSp: TdxMemData;
    teRemnsSpArticul: TIntegerField;
    teRemnsSpName: TStringField;
    teRemnsSpGr: TStringField;
    teRemnsSpSGr: TStringField;
    teRemnsSpIdM: TIntegerField;
    teRemnsSpKM: TFloatField;
    teRemnsSpSM: TStringField;
    teRemnsSpTCard: TIntegerField;
    teRemnsSpQuant: TFloatField;
    dsteRemnsSp: TDataSource;
    ViewRemnsSpeedRecId: TcxGridDBColumn;
    ViewRemnsSpeedArticul: TcxGridDBColumn;
    ViewRemnsSpeedName: TcxGridDBColumn;
    ViewRemnsSpeedGr: TcxGridDBColumn;
    ViewRemnsSpeedSGr: TcxGridDBColumn;
    ViewRemnsSpeedIdM: TcxGridDBColumn;
    ViewRemnsSpeedSM: TcxGridDBColumn;
    ViewRemnsSpeedTCard: TcxGridDBColumn;
    ViewRemnsSpeedQuant: TcxGridDBColumn;
    PBar1: TcxProgressBar;
    acMovePos: TAction;
    teRemnsSpCType: TSmallintField;
    ViewRemnsSpeedCType: TcxGridDBColumn;
    teRemnsSpRealSpeed: TFloatField;
    teRemnsSpQuantDay: TFloatField;
    ViewRemnsSpeedRealSpeed: TcxGridDBColumn;
    ViewRemnsSpeedQuantDay: TcxGridDBColumn;
    ViewLastCli: TcxGridDBTableView;
    LeLastCli: TcxGridLevel;
    GrLastCli: TcxGrid;
    ViewLastCliNAMECL: TcxGridDBColumn;
    ViewLastCliQUANT: TcxGridDBColumn;
    ViewLastCliNAMESHORT: TcxGridDBColumn;
    ViewLastCliPRICEIN: TcxGridDBColumn;
    ViewLastCliSUMIN: TcxGridDBColumn;
    ViewLastCliDATEDOC: TcxGridDBColumn;
    ViewLastCliNUMDOC: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Timer1Timer(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure acExportExExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure acMovePosExecute(Sender: TObject);
    procedure ViewRemnsSpeedSelectionChanged(
      Sender: TcxCustomGridTableView);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmRemnsSpeed: TfmRemnsSpeed;
  bClearMoveSel: Boolean = False;

implementation

uses Un1, dmOffice, SelPerSkl, CardsMove, DMOReps, MainRnOffice;

{$R *.dfm}

procedure TfmRemnsSpeed.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  Timer1.Enabled:=True;
  GridRemnsSpeed.Align:=AlClient;
  ViewRemnsSpeed.RestoreFromIniFile(CurDir+GridIni);
end;

procedure TfmRemnsSpeed.SpeedItem1Click(Sender: TObject);
begin
  close;
end;

procedure TfmRemnsSpeed.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewRemnsSpeed.StoreToIniFile(CurDir+GridIni,False);
  teRemnsSp.Active:=False;
end;

procedure TfmRemnsSpeed.Timer1Timer(Sender: TObject);
begin
  if bClearMoveSel=True then begin StatusBar1.Panels[0].Text:=''; bClearMoveSel:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearMoveSel:=True;
end;

procedure TfmRemnsSpeed.acExitExecute(Sender: TObject);
begin
  close;
end;

procedure TfmRemnsSpeed.acExportExExecute(Sender: TObject);
begin
  prNExportExel5(ViewRemnsSpeed);
end;

procedure TfmRemnsSpeed.FormShow(Sender: TObject);
begin
  Memo1.Clear;
end;

procedure TfmRemnsSpeed.acMovePosExecute(Sender: TObject);
Var iDateB,iDateE,iM:INteger;
    IdCard:INteger;
    NameC:String;
    iMe,RecC:INteger;
begin
  //�������� �� ������
  with dmO do
  begin
    RecC:=teRemnsSp.RecordCount;
    IdCard:=teRemnsSpArticul.AsInteger;
    NameC:=teRemnsSpName.AsString;
    iMe:=teRemnsSpIdM.AsInteger;

    if RecC>0 then
    begin
      //�� ������
      fmSelPerSkl:=tfmSelPerSkl.Create(Application);
      with fmSelPerSkl do
      begin
        cxDateEdit1.Date:=CommonSet.DateFrom;
        quMHAll1.Active:=False;
        quMHAll1.ParamByName('IDPERSON').AsInteger:=Person.Id;
        quMHAll1.Active:=True;
        quMHAll1.First;
        if CommonSet.IdStore>0 then cxLookupComboBox1.EditValue:=CommonSet.IdStore
        else  cxLookupComboBox1.EditValue:=quMHAll1ID.AsInteger;
      end;
      fmSelPerSkl.ShowModal;
      if fmSelPerSkl.ModalResult=mrOk then
      begin
        iDateB:=Trunc(fmSelPerSkl.cxDateEdit1.Date);
        iDateE:=Trunc(fmSelPerSkl.cxDateEdit2.Date)+1;

        CommonSet.IdStore:=fmSelPerSkl.cxLookupComboBox1.EditValue;
        CommonSet.NameStore:=fmSelPerSkl.cxLookupComboBox1.Text;
        CommonSet.DateFrom:=iDateB;
        CommonSet.DateTo:=iDateE+1;

        CardSel.Id:=IdCard;
        CardSel.Name:=NameC;
        prFindSM(iMe,CardSel.mesuriment,iM);
//        CardSel.NameGr:=ClassTree.Selected.Text;

        prPreShowMove(IdCard,iDateB,iDateE,fmSelPerSkl.cxLookupComboBox1.EditValue,NameC);
        fmSelPerSkl.Release;

//        fmCardsMove.PageControl1.ActivePageIndex:=2;
        fmCardsMove.ShowModal;

      end else fmSelPerSkl.Release;
    end;
  end;
end;

procedure TfmRemnsSpeed.ViewRemnsSpeedSelectionChanged(
  Sender: TcxCustomGridTableView);
begin
  if ViewRemnsSpeed.Controller.SelectedRecordCount<>1 then exit;
  with dmO do
  begin
    ViewLastCli.BeginUpdate;
    quLastCli.Active:=False;
    quLastCli.ParamByName('IART').Value:=teRemnsSpArticul.AsInteger;
    quLastCli.Active:=True;
    ViewLastCli.EndUpdate;
  end;
end;

end.
