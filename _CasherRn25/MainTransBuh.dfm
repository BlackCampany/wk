object fmMainAppend: TfmMainAppend
  Left = 429
  Top = 234
  BorderStyle = bsDialog
  Caption = #1042#1099#1075#1088#1091#1079#1082#1072' '#1076#1072#1085#1085#1099#1093' '#1087#1086' '#1088#1077#1072#1083#1080#1079#1072#1094#1080#1080
  ClientHeight = 359
  ClientWidth = 321
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 72
    Top = 76
    Width = 9
    Height = 13
    Caption = #1057
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
    Visible = False
  end
  object Label2: TLabel
    Left = 72
    Top = 132
    Width = 15
    Height = 13
    Caption = #1087#1086
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
    Visible = False
  end
  object Memo1: TMemo
    Left = 16
    Top = 208
    Width = 249
    Height = 73
    Align = alCustom
    Lines.Strings = (
      'Memo1')
    TabOrder = 0
  end
  object ProgressBar1: TdxfProgressBar
    Left = 280
    Top = 8
    Width = 17
    Height = 273
    BarBevelOuter = bvRaised
    BeginColor = clWhite
    BevelOuter = bvLowered
    Color = clWhite
    EndColor = clBlue
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Max = 100
    Min = 0
    Orientation = orVertical
    ParentColor = False
    ParentFont = False
    Position = 50
    ShowText = True
    ShowTextStyle = stsPercent
    Step = 10
    Style = sExRectangles
    TabOrder = 1
    TransparentGlyph = True
    Visible = False
  end
  object Button1: TdxfColorButton
    Left = 40
    Top = 296
    Width = 89
    Height = 40
    Caption.Strings = (
      #1055#1091#1089#1082)
    Color = clLime
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    OnClick = Button1Click
  end
  object Button2: TdxfColorButton
    Left = 200
    Top = 296
    Width = 89
    Height = 40
    Caption.Strings = (
      #1042#1099#1093#1086#1076)
    Color = clRed
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    OnClick = Button2Click
  end
  object CheckBox1: TdxfCheckBox
    Left = 24
    Top = 16
    Width = 241
    Height = 18
    Action = aCheck1
    Checked = True
    AutoSize = False
    GroupIndex = 0
    AllowAllUp = False
    Caption = #1042#1099#1075#1088#1091#1079#1082#1072' '#1085#1077#1074#1099#1075#1088#1091#1078#1077#1085#1085#1099#1093'   '
    Color = 16697288
    Ctl3D = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentColor = False
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 4
  end
  object CheckBox2: TdxfCheckBox
    Left = 24
    Top = 40
    Width = 241
    Height = 18
    Action = aCheck2
    Checked = False
    AutoSize = False
    GroupIndex = 0
    AllowAllUp = False
    Caption = #1042#1099#1075#1088#1091#1079#1082#1072' '#1079#1072' '#1087#1077#1088#1080#1086#1076'  '
    Color = 16697288
    Ctl3D = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentColor = False
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 5
  end
  object DateTimePicker1: TDateTimePicker
    Left = 112
    Top = 72
    Width = 154
    Height = 21
    Date = 38827.705350289360000000
    Time = 38827.705350289360000000
    DateFormat = dfLong
    TabOrder = 6
    Visible = False
  end
  object DateTimePicker2: TDateTimePicker
    Left = 112
    Top = 128
    Width = 153
    Height = 21
    Date = 38827.705552118060000000
    Time = 38827.705552118060000000
    DateFormat = dfLong
    TabOrder = 7
    Visible = False
  end
  object CheckBox3: TdxfCheckBox
    Left = 24
    Top = 184
    Width = 241
    Height = 18
    Checked = False
    AutoSize = False
    GroupIndex = 0
    AllowAllUp = False
    Caption = #1044#1072#1085#1085#1099#1077' '#1076#1086#1073#1072#1074#1083#1103#1090#1100' '#1082' '#1080#1084#1077#1102#1097#1080#1084#1089#1103
    Color = 16697288
    Ctl3D = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentColor = False
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 8
  end
  object DateTimePicker3: TDateTimePicker
    Left = 112
    Top = 96
    Width = 73
    Height = 21
    Date = 38828.000000000000000000
    Time = 38828.000000000000000000
    Kind = dtkTime
    TabOrder = 9
    Visible = False
  end
  object DateTimePicker4: TDateTimePicker
    Left = 112
    Top = 152
    Width = 73
    Height = 21
    Date = 38828.000000000000000000
    Time = 38828.000000000000000000
    Kind = dtkTime
    TabOrder = 10
    Visible = False
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 500
    OnTimer = Timer1Timer
    Left = 16
    Top = 128
  end
  object dmC: TpFIBDatabase
    DBName = 'localhost:C:\_CasherRn\DB2\CasherRn.gdb'
    DBParams.Strings = (
      'lc_ctype=WIN1251'
      'password=masterkey'
      'sql_role_name=SYSDBA'
      'user_name=SYSDBA')
    DefaultTransaction = trSelect
    DefaultUpdateTransaction = trUpdate
    SQLDialect = 3
    Timeout = 0
    DesignDBOptions = [ddoIsDefaultDatabase]
    WaitForRestoreConnect = 0
    Left = 200
    Top = 96
  end
  object trSelect: TpFIBTransaction
    DefaultDatabase = dmC
    TimeoutAction = TARollback
    Left = 104
    Top = 64
  end
  object trUpdate: TpFIBTransaction
    DefaultDatabase = dmC
    TimeoutAction = TARollback
    Left = 104
    Top = 112
  end
  object taCashB: TpFIBDataSet
    SelectSQL.Strings = (
      
        'Select c.CASHNUM, c.ZNUM, s.ITYPE, s.Sifr, mn.CODE,mn.NAME,mn.ST' +
        'REAM,mn.PARENT,s.Price,t.DISCONT,t.OPERTYPE,Sum(s.Quantity) as S' +
        'umQ, sum(s.DiscountSum) as SumD, sum(s.Summa) as SumR, min(c.ChD' +
        'ate)  from cashsail c'
      'left join tables_all t on c.Tab_Id=t.Id'
      'left join  spec_all s on s.Id_Tab=Tab_Id'
      'left join  Menu mn on mn.Sifr=s.Sifr'
      ''
      'where'
      ''
      'c.ID>=:IdBeg'
      'and c.Id<=:IdEnd'
      'and t.ENDTIME>:DateB'
      'and t.ENDTIME<:DateE'
      ''
      
        'Group by c.CASHNUM, c.ZNUM, s.ITYPE, s.Sifr, mn.CODE,mn.NAME,mn.' +
        'STREAM,mn.PARENT,s.Price,t.DISCONT,t.OPERTYPE'
      'Order by c.CASHNUM, c.ZNUM, s.ITYPE, s.Sifr')
    Transaction = trSelect
    Database = dmC
    UpdateTransaction = trUpdate
    Left = 72
    Top = 240
    poAskRecordCount = True
    object taCashBCASHNUM: TFIBIntegerField
      FieldName = 'CASHNUM'
    end
    object taCashBZNUM: TFIBIntegerField
      FieldName = 'ZNUM'
    end
    object taCashBITYPE: TFIBSmallIntField
      FieldName = 'ITYPE'
    end
    object taCashBSIFR: TFIBIntegerField
      FieldName = 'SIFR'
    end
    object taCashBPRICE: TFIBFloatField
      FieldName = 'PRICE'
    end
    object taCashBSUMQ: TFIBFloatField
      FieldName = 'SUMQ'
    end
    object taCashBSUMD: TFIBFloatField
      FieldName = 'SUMD'
    end
    object taCashBSUMR: TFIBFloatField
      FieldName = 'SUMR'
    end
    object taCashBMIN: TFIBDateTimeField
      FieldName = 'MIN'
    end
    object taCashBCODE: TFIBStringField
      FieldName = 'CODE'
      Size = 10
      EmptyStrToNull = True
    end
    object taCashBNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 100
      EmptyStrToNull = True
    end
    object taCashBSTREAM: TFIBSmallIntField
      FieldName = 'STREAM'
    end
    object taCashBPARENT: TFIBSmallIntField
      FieldName = 'PARENT'
    end
    object taCashBDISCONT: TFIBStringField
      FieldName = 'DISCONT'
      EmptyStrToNull = True
    end
    object taCashBOPERTYPE: TFIBStringField
      FieldName = 'OPERTYPE'
      Size = 10
      EmptyStrToNull = True
    end
  end
  object Timer2: TTimer
    Enabled = False
    Interval = 500
    OnTimer = Timer2Timer
    Left = 224
    Top = 192
  end
  object dxfBackGround1: TdxfBackGround
    BkColor.BeginColor = clBlue
    BkColor.EndColor = clWhite
    BkColor.FillStyle = fsVert
    BkAnimate.Speed = 700
    Left = 256
    Top = 224
  end
  object am1: TActionManager
    Left = 184
    Top = 240
    StyleName = 'XP Style'
    object aCheck1: TAction
      Caption = 'aCheck1'
      OnExecute = aCheck1Execute
    end
    object aCheck2: TAction
      Caption = 'aCheck2'
      OnExecute = aCheck2Execute
    end
  end
  object taFindBound: TpFIBDataSet
    SelectSQL.Strings = (
      'SElect min(Id) as MinId, Max(id) as MaxId from cashsail'
      'where'
      'Chdate>:DateB'
      'and ChDate<:DateE')
    Transaction = trSelect
    Database = dmC
    UpdateTransaction = trUpdate
    Left = 128
    Top = 240
    object taFindBoundMINID: TFIBIntegerField
      FieldName = 'MINID'
    end
    object taFindBoundMAXID: TFIBIntegerField
      FieldName = 'MAXID'
    end
  end
  object taFindLast: TpFIBDataSet
    SelectSQL.Strings = (
      'SElect Max(id) as MaxId from cashsail')
    Transaction = trSelect
    Database = dmC
    UpdateTransaction = trUpdate
    Left = 104
    Top = 192
    object taFindLastMAXID: TFIBIntegerField
      FieldName = 'MAXID'
    end
  end
  object taSpecDel: TpFIBDataSet
    SelectSQL.Strings = (
      
        'SELECT s.ITYPE,s.Sifr,mn.CODE,mn.NAME,mn.STREAM,mn.PARENT,s.Pric' +
        'e,t.DISCONT,t.OPERTYPE, t.ENDTIME, Sum(s.Quantity) as SumQ,sum(s' +
        '.DiscountSum) as SumD,sum(s.Summa) as SumR'
      'FROM TABLES_ALL t'
      'left join  spec_all s on s.Id_Tab=t.Id'
      'left join Menu mn on s.Sifr=mn.Sifr'
      ''
      'where'
      't.ID>=:IDBEG'
      'and t.Id<=:IDEND'
      'and (t.OperType='#39'Del'#39'or t.OperType='#39'SalePC'#39')'
      'and t.Sklad=0'
      'and t.ENDTIME>:DateB'
      'and t.ENDTIME<:DateE'
      ''
      
        'Group by s.ITYPE, s.Sifr, mn.CODE,mn.NAME,mn.STREAM,mn.PARENT,s.' +
        'Price,t.DISCONT,t.OPERTYPE, t.ENDTIME'
      'Order by s.ITYPE, s.Sifr')
    Transaction = trSel1
    Database = dmC
    UpdateTransaction = trUpd1
    Left = 72
    Top = 304
    poAskRecordCount = True
    object taSpecDelITYPE: TFIBSmallIntField
      FieldName = 'ITYPE'
    end
    object taSpecDelSIFR: TFIBIntegerField
      FieldName = 'SIFR'
    end
    object taSpecDelCODE: TFIBStringField
      FieldName = 'CODE'
      Size = 10
      EmptyStrToNull = True
    end
    object taSpecDelPRICE: TFIBFloatField
      FieldName = 'PRICE'
    end
    object taSpecDelENDTIME: TFIBDateTimeField
      FieldName = 'ENDTIME'
    end
    object taSpecDelSUMQ: TFIBFloatField
      FieldName = 'SUMQ'
    end
    object taSpecDelSUMD: TFIBFloatField
      FieldName = 'SUMD'
    end
    object taSpecDelSUMR: TFIBFloatField
      FieldName = 'SUMR'
    end
    object taSpecDelNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 100
      EmptyStrToNull = True
    end
    object taSpecDelSTREAM: TFIBSmallIntField
      FieldName = 'STREAM'
    end
    object taSpecDelPARENT: TFIBSmallIntField
      FieldName = 'PARENT'
    end
    object taSpecDelDISCONT: TFIBStringField
      FieldName = 'DISCONT'
      EmptyStrToNull = True
    end
    object taSpecDelOPERTYPE: TFIBStringField
      FieldName = 'OPERTYPE'
      Size = 10
      EmptyStrToNull = True
    end
  end
  object taFindBDel: TpFIBDataSet
    SelectSQL.Strings = (
      'SElect min(Id) as MinId, Max(id) as MaxId from TABLES_ALL'
      'where'
      'EndTime>:DateB'
      'and EndTime<:DateE')
    Transaction = trSel1
    Database = dmC
    UpdateTransaction = trUpd1
    Left = 136
    Top = 304
    object taFindBDelMINID: TFIBIntegerField
      FieldName = 'MINID'
    end
    object taFindBDelMAXID: TFIBIntegerField
      FieldName = 'MAXID'
    end
  end
  object trSel1: TpFIBTransaction
    DefaultDatabase = dmC
    TimeoutAction = TARollback
    Left = 48
    Top = 64
  end
  object trUpd1: TpFIBTransaction
    DefaultDatabase = dmC
    TimeoutAction = TARollback
    Left = 48
    Top = 112
  end
  object taFindLDel: TpFIBDataSet
    SelectSQL.Strings = (
      'Select Max(id) as MaxId from TABLES_ALL'
      '')
    Transaction = trSelect
    Database = dmC
    UpdateTransaction = trUpdate
    Left = 16
    Top = 304
    object taFindLDelMAXID: TFIBIntegerField
      FieldName = 'MAXID'
    end
  end
  object taCashZ: TpFIBDataSet
    SelectSQL.Strings = (
      
        'Select c.CASHNUM, c.ZNUM, min(c.ChDate) as DateZ  from cashsail ' +
        'c'
      'where'
      'c.ID>=:IdBeg'
      'and c.Id<=:IdEnd'
      'group by c.CASHNUM, c.ZNUM')
    Transaction = trSelect
    Database = dmC
    UpdateTransaction = trUpdate
    Left = 24
    Top = 240
    poAskRecordCount = True
    object taCashZCASHNUM: TFIBIntegerField
      FieldName = 'CASHNUM'
    end
    object taCashZZNUM: TFIBIntegerField
      FieldName = 'ZNUM'
    end
    object taCashZDATEZ: TFIBDateTimeField
      FieldName = 'DATEZ'
    end
  end
  object taCashB1_: TpFIBDataSet
    SelectSQL.Strings = (
      
        'Select c.CASHNUM, c.ZNUM,c.Tab_Id,s.Sifr,s.ITYPE, mn.CODE,mn.NAM' +
        'E,mn.STREAM,mn.PARENT,s.Price,'
      
        't.DISCONT,t.OPERTYPE,s.Quantity,s.DiscountSum,s.Summa,t.ID_PERSO' +
        'NAL,p.BARCODE,t.ENDTIME,t.NUMZ'
      ''
      'from cashsail c'
      'left join tables_all t on c.Tab_Id=t.Id'
      'left join  spec_all s on s.Id_Tab=Tab_Id'
      'left join  Menu mn on mn.Sifr=s.Sifr'
      'left join RPersonal p on p.ID=t.ID_PERSONAL'
      ''
      'where'
      ''
      'c.ID>=:IdBeg'
      'and c.Id<=:IdEnd'
      ''
      'Order by c.CASHNUM, c.ZNUM, s.ITYPE,c.Tab_Id,s.Sifr')
    Transaction = trSelect
    Database = dmC
    UpdateTransaction = trUpdate
    Left = 224
    Top = 256
    poAskRecordCount = True
    object taCashB1_CASHNUM: TFIBIntegerField
      FieldName = 'CASHNUM'
    end
    object taCashB1_ZNUM: TFIBIntegerField
      FieldName = 'ZNUM'
    end
    object taCashB1_TAB_ID: TFIBIntegerField
      FieldName = 'TAB_ID'
    end
    object taCashB1_SIFR: TFIBIntegerField
      FieldName = 'SIFR'
    end
    object taCashB1_ITYPE: TFIBSmallIntField
      FieldName = 'ITYPE'
    end
    object taCashB1_CODE: TFIBStringField
      FieldName = 'CODE'
      Size = 10
      EmptyStrToNull = True
    end
    object taCashB1_NAME: TFIBStringField
      FieldName = 'NAME'
      Size = 100
      EmptyStrToNull = True
    end
    object taCashB1_STREAM: TFIBSmallIntField
      FieldName = 'STREAM'
    end
    object taCashB1_PARENT: TFIBSmallIntField
      FieldName = 'PARENT'
    end
    object taCashB1_PRICE: TFIBFloatField
      FieldName = 'PRICE'
    end
    object taCashB1_DISCONT: TFIBStringField
      FieldName = 'DISCONT'
      EmptyStrToNull = True
    end
    object taCashB1_OPERTYPE: TFIBStringField
      FieldName = 'OPERTYPE'
      Size = 10
      EmptyStrToNull = True
    end
    object taCashB1_QUANTITY: TFIBFloatField
      FieldName = 'QUANTITY'
    end
    object taCashB1_DISCOUNTSUM: TFIBFloatField
      FieldName = 'DISCOUNTSUM'
    end
    object taCashB1_SUMMA: TFIBFloatField
      FieldName = 'SUMMA'
    end
    object taCashB1_ID_PERSONAL: TFIBIntegerField
      FieldName = 'ID_PERSONAL'
    end
    object taCashB1_BARCODE: TFIBStringField
      FieldName = 'BARCODE'
      EmptyStrToNull = True
    end
    object taCashB1_ENDTIME: TFIBDateTimeField
      FieldName = 'ENDTIME'
    end
    object taCashB1_NUMZ: TFIBIntegerField
      FieldName = 'NUMZ'
    end
  end
  object taSpecDel1: TpFIBDataSet
    SelectSQL.Strings = (
      
        'SELECT s.ITYPE,s.Sifr,mn.CODE,mn.NAME,mn.STREAM,mn.PARENT,s.Pric' +
        'e,t.ID,'
      
        't.DISCONT,t.OPERTYPE,s.Quantity,s.DiscountSum,s.Summa,t.ID_PERSO' +
        'NAL,p.BARCODE,t.ENDTIME,t.NUMZ'
      'FROM TABLES_ALL t'
      'left join  spec_all s on s.Id_Tab=t.Id'
      'left join Menu mn on s.Sifr=mn.Sifr'
      'left join RPersonal p on p.ID=t.ID_PERSONAL'
      ''
      'where'
      't.ID>=:IDBEG'
      'and t.Id<=:IDEND'
      '/*and (t.OperType='#39'Del'#39'or t.OperType='#39'SalePC'#39')*/'
      'and t.Sklad=0'
      'Order by s.ITYPE,t.ID,s.Sifr')
    Transaction = trSel1
    Database = dmC
    UpdateTransaction = trUpd1
    Left = 224
    Top = 312
    poAskRecordCount = True
    object taSpecDel1ITYPE: TFIBSmallIntField
      FieldName = 'ITYPE'
    end
    object taSpecDel1SIFR: TFIBIntegerField
      FieldName = 'SIFR'
    end
    object taSpecDel1CODE: TFIBStringField
      FieldName = 'CODE'
      Size = 10
      EmptyStrToNull = True
    end
    object taSpecDel1NAME: TFIBStringField
      FieldName = 'NAME'
      Size = 100
      EmptyStrToNull = True
    end
    object taSpecDel1STREAM: TFIBSmallIntField
      FieldName = 'STREAM'
    end
    object taSpecDel1PARENT: TFIBSmallIntField
      FieldName = 'PARENT'
    end
    object taSpecDel1PRICE: TFIBFloatField
      FieldName = 'PRICE'
    end
    object taSpecDel1ID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taSpecDel1DISCONT: TFIBStringField
      FieldName = 'DISCONT'
      EmptyStrToNull = True
    end
    object taSpecDel1OPERTYPE: TFIBStringField
      FieldName = 'OPERTYPE'
      Size = 10
      EmptyStrToNull = True
    end
    object taSpecDel1QUANTITY: TFIBFloatField
      FieldName = 'QUANTITY'
    end
    object taSpecDel1DISCOUNTSUM: TFIBFloatField
      FieldName = 'DISCOUNTSUM'
    end
    object taSpecDel1SUMMA: TFIBFloatField
      FieldName = 'SUMMA'
    end
    object taSpecDel1ID_PERSONAL: TFIBIntegerField
      FieldName = 'ID_PERSONAL'
    end
    object taSpecDel1BARCODE: TFIBStringField
      FieldName = 'BARCODE'
      EmptyStrToNull = True
    end
    object taSpecDel1ENDTIME: TFIBDateTimeField
      FieldName = 'ENDTIME'
    end
    object taSpecDel1NUMZ: TFIBIntegerField
      FieldName = 'NUMZ'
    end
  end
end
