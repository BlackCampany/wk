unit SelPerSkl2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, cxGraphics, cxDropDownEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxControls,
  cxContainer, cxEdit, cxTextEdit, cxMaskEdit, cxCalendar, StdCtrls,
  cxButtons, ExtCtrls, DB, FIBDataSet, pFIBDataSet, cxCheckBox,
  dxfProgressBar, cxRadioGroup, cxImageComboBox, cxCheckComboBox;

type
  TfmSelPerSkl2 = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    Label3: TLabel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxDateEdit1: TcxDateEdit;
    cxDateEdit2: TcxDateEdit;
    cxLookupComboBox1: TcxLookupComboBox;
    quMHAll1: TpFIBDataSet;
    quMHAll1ID: TFIBIntegerField;
    quMHAll1PARENT: TFIBIntegerField;
    quMHAll1ITYPE: TFIBIntegerField;
    quMHAll1NAMEMH: TFIBStringField;
    quMHAll1DEFPRICE: TFIBIntegerField;
    quMHAll1NAMEPRICE: TFIBStringField;
    dsMHAll1: TDataSource;
    cxCheckBox1: TcxCheckBox;
    CheckBox1: TcxCheckBox;
    CheckBox11: TcxCheckBox;
    Label2: TLabel;
    cxLookupComboBox2: TcxLookupComboBox;
    CheckBox2: TcxCheckBox;
    CheckBox22: TcxCheckBox;
    Label4: TLabel;
    cxLookupComboBox3: TcxLookupComboBox;
    CheckBox3: TcxCheckBox;
    CheckBox33: TcxCheckBox;
    Label5: TLabel;
    cxLookupComboBox4: TcxLookupComboBox;
    CheckBox4: TcxCheckBox;
    CheckBox44: TcxCheckBox;
    Label6: TLabel;
    CheckBox5: TcxCheckBox;
    CheckBox55: TcxCheckBox;
    quMProd: TpFIBDataSet;
    dsquMProd: TDataSource;
    quMProdID: TFIBIntegerField;
    quMProdNAMEDB: TFIBStringField;
    quCateg: TpFIBDataSet;
    dsquCateg: TDataSource;
    quCategID: TFIBIntegerField;
    quCategNAMECAT: TFIBStringField;
    quSalet: TpFIBDataSet;
    dsquSalet: TDataSource;
    quSaletSALET: TFIBIntegerField;
    quSaletSSALET: TFIBStringField;
    cxLookupComboBox5: TcxImageComboBox;
    CheckBox6: TcxCheckBox;
    cxCheckComboBox1: TcxCheckComboBox;
    procedure cxButton2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxCheckBox1PropertiesChange(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure CheckBox1PropertiesChange(Sender: TObject);
    procedure CheckBox2PropertiesChange(Sender: TObject);
    procedure CheckBox3PropertiesChange(Sender: TObject);
    procedure CheckBox4PropertiesChange(Sender: TObject);
    procedure CheckBox5PropertiesChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmSelPerSkl2: TfmSelPerSkl2;

implementation

uses dmOffice, Un1;

{$R *.dfm}

procedure TfmSelPerSkl2.cxButton2Click(Sender: TObject);
begin
  close;
end;

procedure TfmSelPerSkl2.FormCreate(Sender: TObject);
begin
  cxDateEdit2.Date:=iMaxDate;
end;

procedure TfmSelPerSkl2.cxCheckBox1PropertiesChange(Sender: TObject);
begin
  if cxCheckBox1.Checked then
  begin
    cxDateEdit2.Date:=Trunc(Date);
    cxDateEdit2.Visible:=True;
  end else
  begin
    cxDateEdit2.Date:=iMaxDate;
    cxDateEdit2.Visible:=False;
  end;
end;

procedure TfmSelPerSkl2.cxButton1Click(Sender: TObject);
begin
  Cursor:=crHourGlass; Delay(10);
  Prib.MHId:=cxLookupComboBox1.EditValue;
  Prib.MHAll:=CheckBox1.Checked;
  Prib.MHUnion:=CheckBox11.Checked;
  Prib.MProd:=cxLookupComboBox2.EditValue;
  Prib.MProdAll:=CheckBox2.Checked;
  Prib.MProdUnion:=CheckBox22.Checked;
  Prib.Cat:=cxLookupComboBox3.EditValue;
  Prib.CatAll:=CheckBox3.Checked;
  Prib.CatUnion:=CheckBox33.Checked;
  Prib.Salet:=cxLookupComboBox4.Text;
  Prib.SaletAll:=CheckBox4.Checked;
  Prib.SaletUnion:=CheckBox44.Checked;
  Prib.Oper:=cxLookupComboBox5.EditValue;
  Prib.OperAll:=CheckBox5.Checked;
  Prib.OperUnion:=CheckBox55.Checked;
  Prib.OperUnionCli:=CheckBox6.Checked;
  if Pos('�� �������',cxCheckComboBox1.Text)>0 then Prib.sMHId:='0'
  else Prib.sMHId:=cxCheckComboBox1.Text;
end;

procedure TfmSelPerSkl2.CheckBox1PropertiesChange(Sender: TObject);
var i:Integer;
begin
  if CheckBox1.Checked then
  begin
   //�������� ���
   for i:=0 to cxCheckComboBox1.Properties.Items.Count do
   begin
     cxCheckComboBox1.States[i]:=cbsChecked;
   end;
  end else
  begin
   for i:=0 to cxCheckComboBox1.Properties.Items.Count do
   begin
     cxCheckComboBox1.States[i]:=cbsUnChecked;
   end;
  end;
end;

procedure TfmSelPerSkl2.CheckBox2PropertiesChange(Sender: TObject);
begin
  if CheckBox2.Checked then cxLookupComboBox2.Enabled:=False
  else cxLookupComboBox2.Enabled:=True;
end;

procedure TfmSelPerSkl2.CheckBox3PropertiesChange(Sender: TObject);
begin
  if CheckBox3.Checked then cxLookupComboBox3.Enabled:=False
  else cxLookupComboBox3.Enabled:=True;
end;

procedure TfmSelPerSkl2.CheckBox4PropertiesChange(Sender: TObject);
begin
  if CheckBox4.Checked then cxLookupComboBox4.Enabled:=False
  else cxLookupComboBox4.Enabled:=True;
end;

procedure TfmSelPerSkl2.CheckBox5PropertiesChange(Sender: TObject);
begin
  if CheckBox5.Checked then cxLookupComboBox5.Enabled:=False
  else cxLookupComboBox5.Enabled:=True;
end;

procedure TfmSelPerSkl2.FormShow(Sender: TObject);
var It:TcxCheckComboBoxItem;
begin
  cxCheckComboBox1.Clear;
  cxCheckComboBox1.Properties.Items.Clear;
  quMHAll1.First;
  while not quMHAll1.Eof do
  begin
   It:=cxCheckComboBox1.Properties.Items.AddCheckItem(quMHAll1NAMEMH.asString,quMHAll1ID.AsString);
   It.Tag:=quMHAll1ID.asinteger;
   quMHAll1.next;
  end;
end;

end.
