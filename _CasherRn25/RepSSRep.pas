unit RepSSRep;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxTextEdit, cxImageComboBox,
  DBClient, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxControls, cxGridCustomView, cxGrid,
  cxContainer, cxMemo, ExtCtrls, SpeedBar, ComCtrls, Placemnt, FR_DSet,
  FR_DBSet, FR_Class, FIBDataSet, pFIBDataSet, cxProgressBar, dxPSGlbl,
  dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon, dxPScxGridLnk,
  Menus, cxDBLookupComboBox, dxmdaset, FIBDatabase, pFIBDatabase;

type
  TfmRepSS = class(TForm)
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    Panel1: TPanel;
    Memo1: TcxMemo;
    GridSS: TcxGrid;
    ViewSS: TcxGridDBTableView;
    LevelSS: TcxGridLevel;
    dstaSS: TDataSource;
    FormPlacement1: TFormPlacement;
    SpeedItem4: TSpeedItem;
    PBar1: TcxProgressBar;
    SpeedItem5: TSpeedItem;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    SpeedItem6: TSpeedItem;
    taSS: TdxMemData;
    taSSiCode: TIntegerField;
    taSSName: TStringField;
    taSSiCat: TIntegerField;
    taSSsCat: TStringField;
    taSSiCodeCB: TStringField;
    taSSrSS1: TFloatField;
    taSSrProc1: TFloatField;
    taSSrSS2: TFloatField;
    taSSrProc2: TFloatField;
    taSSrSS: TFloatField;
    taSSrProcN1: TFloatField;
    taSSrProcN2: TFloatField;
    taSSrProcN: TFloatField;
    taSSrPrice: TFloatField;
    ViewSSiCode: TcxGridDBColumn;
    ViewSSiCodeCB: TcxGridDBColumn;
    ViewSSName: TcxGridDBColumn;
    ViewSSiCat: TcxGridDBColumn;
    ViewSSsCat: TcxGridDBColumn;
    ViewSSrSS1: TcxGridDBColumn;
    ViewSSrProc1: TcxGridDBColumn;
    ViewSSrProcN1: TcxGridDBColumn;
    ViewSSrSS2: TcxGridDBColumn;
    ViewSSrProc2: TcxGridDBColumn;
    ViewSSrProcN2: TcxGridDBColumn;
    ViewSSrSS: TcxGridDBColumn;
    ViewSSrProcN: TcxGridDBColumn;
    ViewSSrPrice: TcxGridDBColumn;
    quSS: TpFIBDataSet;
    trSelSS: TpFIBTransaction;
    quSSIDCARD: TFIBIntegerField;
    quSSNAME: TFIBStringField;
    quSSCODEZAK: TFIBIntegerField;
    quSSID: TFIBIntegerField;
    quSSNAMECAT: TFIBStringField;
    quSSQUANT: TFIBFloatField;
    quSSSUMIN: TFIBFloatField;
    quSSSUMR: TFIBFloatField;
    quSSSUMIN0: TFIBFloatField;
    quSSPRICE: TFIBFloatField;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure  prFormSS;
  end;

var
  fmRepSS: TfmRepSS;

implementation

uses Un1, SelPerSkl, dmOffice, DMOReps, SummaryReal, SelPerSkl2;

{$R *.dfm}

procedure  TfmRepSS.prFormSS;
begin
  with dmO do
  with dmORep do
  begin
    fmRepSS.Memo1.Clear;
    fmRepSS.Memo1.Lines.Add('����� .. ���� ������������ ������.'); delay(10);

    fmRepSS.Memo1.Lines.Add('  ���������� �� �������.'); delay(10);

    PBar1.Position:=0;
    PBar1.Visible:=True;
    delay(100);


    ViewSS.BeginUpdate;

    CloseTe(taSS);

    //���� ������� ������

    quSS.Active:=False;
    quSS.SelectSQL.Clear;
    quSS.SelectSQL.Add('SELECT ds.IDCARD, ca.Name, ca.CODEZAK, cat.ID, cat.NAMECAT,');
    quSS.SelectSQL.Add('       SUM(ds.QUANT*ds.KM) as QUANT,');
    quSS.SelectSQL.Add('       SUM(ds.SUMIN) as SUMIN,');
    quSS.SelectSQL.Add('       SUM(ds.SUMR) as SUMR,');
    quSS.SelectSQL.Add('       SUM(ds.SUMIN0) as SUMIN0,');
    quSS.SelectSQL.Add('       max(ds.PRICER) as PRICE');
    quSS.SelectSQL.Add('FROM OF_DOCSPECOUTR ds');
    quSS.SelectSQL.Add('left join of_docheadoutr dh on dh.ID=ds.IDHEAD');
    quSS.SelectSQL.Add('left join of_cards ca on ca.ID=ds.IDCARD');
    quSS.SelectSQL.Add('left join of_cardsrcategory cat on cat.ID=ca.RCATEGORY');
    quSS.SelectSQL.Add('where ds.QUANT>0');
    quSS.SelectSQL.Add('and dh.DATEDOC>='''+FormatDateTime('dd.mm.yyyy',Prib.d31)+''' and dh.DATEDOC<='''+FormatDateTime('dd.mm.yyyy',Prib.d32)+'''');
    quSS.SelectSQL.Add('and dh.IACTIVE=1');
    if Prib.MHAll=False then quSS.SelectSQL.Add('and dh.IDSKL='+its(Prib.MHId));
    quSS.SelectSQL.Add('group by ds.IDCARD, ca.Name, ca.CODEZAK, cat.ID, cat.NAMECAT');
    quSS.Active:=True;
    quSS.First;
    while not quSS.Eof do
    begin
      taSS.Append;
      taSSiCode.AsInteger:=quSSIDCARD.AsInteger;
      taSSiCodeCB.AsInteger:=quSSCODEZAK.AsInteger;
      taSSName.AsString:=quSSNAME.AsString;
      taSSiCat.AsInteger:=quSSID.AsInteger;
      taSSsCat.AsString:=quSSNAMECAT.AsString;
      taSSrSS1.AsFloat:=0;
      taSSrProc1.AsFloat:=0;
      taSSrProcN1.AsFloat:=0;
      taSSrSS2.AsFloat:=0;
      taSSrProc2.AsFloat:=0;
      taSSrProcN2.AsFloat:=0;
      if quSSQUANT.AsFloat>0 then taSSrSS.AsFloat:=quSSSUMIN.AsFloat/quSSQUANT.AsFloat
      else taSSrSS.AsFloat:=0;

      if quSSSUMIN.AsFloat>0 then taSSrProcN.AsFloat:=(quSSSUMR.AsFloat-quSSSUMIN.AsFloat)/quSSSUMIN.AsFloat*100
      else taSSrProcN.AsFloat:=0;

      taSSrPrice.AsFloat:=quSSPRICE.AsFloat;
      taSS.Post;

      quSS.Next;
    end;

    quSS.Active:=False;

    PBar1.Position:=30;
    delay(10);

    //���� ������ ������

    quSS.Active:=False;
    quSS.SelectSQL.Clear;
    quSS.SelectSQL.Add('SELECT ds.IDCARD, ca.Name, ca.CODEZAK, cat.ID, cat.NAMECAT,');
    quSS.SelectSQL.Add('       SUM(ds.QUANT*ds.KM) as QUANT,');
    quSS.SelectSQL.Add('       SUM(ds.SUMIN) as SUMIN,');
    quSS.SelectSQL.Add('       SUM(ds.SUMR) as SUMR,');
    quSS.SelectSQL.Add('       SUM(ds.SUMIN0) as SUMIN0,');
    quSS.SelectSQL.Add('       max(ds.PRICER) as PRICE');
    quSS.SelectSQL.Add('FROM OF_DOCSPECOUTR ds');
    quSS.SelectSQL.Add('left join of_docheadoutr dh on dh.ID=ds.IDHEAD');
    quSS.SelectSQL.Add('left join of_cards ca on ca.ID=ds.IDCARD');
    quSS.SelectSQL.Add('left join of_cardsrcategory cat on cat.ID=ca.RCATEGORY');
    quSS.SelectSQL.Add('where ds.QUANT>0');
    quSS.SelectSQL.Add('and dh.DATEDOC>='''+FormatDateTime('dd.mm.yyyy',Prib.d21)+''' and dh.DATEDOC<='''+FormatDateTime('dd.mm.yyyy',Prib.d22)+'''');
    quSS.SelectSQL.Add('and dh.IACTIVE=1');
    if Prib.MHAll=False then quSS.SelectSQL.Add('and dh.IDSKL='+its(Prib.MHId));
    quSS.SelectSQL.Add('group by ds.IDCARD, ca.Name, ca.CODEZAK, cat.ID, cat.NAMECAT');
    quSS.Active:=True;
    quSS.First;
    while not quSS.Eof do
    begin
      if taSS.Locate('iCode',quSSIDCARD.AsInteger,[]) then
      begin
        taSS.Edit;

        if quSSQUANT.AsFloat>0 then taSSrSS2.AsFloat:=quSSSUMIN.AsFloat/quSSQUANT.AsFloat
        else taSSrSS2.AsFloat:=0;

        if quSSSUMIN.AsFloat>0 then taSSrProcN2.AsFloat:=(quSSSUMR.AsFloat-quSSSUMIN.AsFloat)/quSSSUMIN.AsFloat*100
        else taSSrProcN2.AsFloat:=0;

        if taSSrSS2.AsFloat>0 then taSSrProc2.AsFloat:=(taSSrSS.AsFloat-taSSrSS2.AsFloat)/taSSrSS2.AsFloat*100
        else taSSrProc2.AsFloat:=0;

        taSS.Post;
      end else
      begin
        taSS.Append;
        taSSiCode.AsInteger:=quSSIDCARD.AsInteger;
        taSSiCodeCB.AsInteger:=quSSCODEZAK.AsInteger;
        taSSName.AsString:=quSSNAME.AsString;
        taSSiCat.AsInteger:=quSSID.AsInteger;
        taSSsCat.AsString:=quSSNAMECAT.AsString;
        taSSrSS1.AsFloat:=0;
        taSSrProc1.AsFloat:=0;
        taSSrProcN1.AsFloat:=0;

        if quSSQUANT.AsFloat>0 then taSSrSS2.AsFloat:=quSSSUMIN.AsFloat/quSSQUANT.AsFloat
        else taSSrSS2.AsFloat:=0;

        taSSrProc2.AsFloat:=0;

        if quSSSUMIN.AsFloat>0 then taSSrProcN2.AsFloat:=(quSSSUMR.AsFloat-quSSSUMIN.AsFloat)/quSSSUMIN.AsFloat*100
        else taSSrProcN2.AsFloat:=0;

        taSSrSS.AsFloat:=0;
        taSSrProcN.AsFloat:=0;
        taSSrPrice.AsFloat:=0;

        taSS.Post;
      end;

      quSS.Next;
    end;

    quSS.Active:=False;
    PBar1.Position:=60;
    delay(10);

    //���� ������ ������

    quSS.Active:=False;
    quSS.SelectSQL.Clear;
    quSS.SelectSQL.Add('SELECT ds.IDCARD, ca.Name, ca.CODEZAK, cat.ID, cat.NAMECAT,');
    quSS.SelectSQL.Add('       SUM(ds.QUANT*ds.KM) as QUANT,');
    quSS.SelectSQL.Add('       SUM(ds.SUMIN) as SUMIN,');
    quSS.SelectSQL.Add('       SUM(ds.SUMR) as SUMR,');
    quSS.SelectSQL.Add('       SUM(ds.SUMIN0) as SUMIN0,');
    quSS.SelectSQL.Add('       max(ds.PRICER) as PRICE');
    quSS.SelectSQL.Add('FROM OF_DOCSPECOUTR ds');
    quSS.SelectSQL.Add('left join of_docheadoutr dh on dh.ID=ds.IDHEAD');
    quSS.SelectSQL.Add('left join of_cards ca on ca.ID=ds.IDCARD');
    quSS.SelectSQL.Add('left join of_cardsrcategory cat on cat.ID=ca.RCATEGORY');
    quSS.SelectSQL.Add('where ds.QUANT>0');
    quSS.SelectSQL.Add('and dh.DATEDOC>='''+FormatDateTime('dd.mm.yyyy',Prib.d11)+''' and dh.DATEDOC<='''+FormatDateTime('dd.mm.yyyy',Prib.d12)+'''');
    quSS.SelectSQL.Add('and dh.IACTIVE=1');
    if Prib.MHAll=False then quSS.SelectSQL.Add('and dh.IDSKL='+its(Prib.MHId));
    quSS.SelectSQL.Add('group by ds.IDCARD, ca.Name, ca.CODEZAK, cat.ID, cat.NAMECAT');
    quSS.Active:=True;
    quSS.First;
    while not quSS.Eof do
    begin
      if taSS.Locate('iCode',quSSIDCARD.AsInteger,[]) then
      begin
        taSS.Edit;

        if quSSQUANT.AsFloat>0 then taSSrSS1.AsFloat:=quSSSUMIN.AsFloat/quSSQUANT.AsFloat
        else taSSrSS1.AsFloat:=0;

        if quSSSUMIN.AsFloat>0 then taSSrProcN1.AsFloat:=(quSSSUMR.AsFloat-quSSSUMIN.AsFloat)/quSSSUMIN.AsFloat*100
        else taSSrProcN1.AsFloat:=0;

        if taSSrSS1.AsFloat>0 then taSSrProc1.AsFloat:=(taSSrSS.AsFloat-taSSrSS1.AsFloat)/taSSrSS1.AsFloat*100
        else taSSrProc1.AsFloat:=0;

        taSS.Post;
      end else
      begin
        taSS.Append;
        taSSiCode.AsInteger:=quSSIDCARD.AsInteger;
        taSSiCodeCB.AsInteger:=quSSCODEZAK.AsInteger;
        taSSName.AsString:=quSSNAME.AsString;
        taSSiCat.AsInteger:=quSSID.AsInteger;
        taSSsCat.AsString:=quSSNAMECAT.AsString;

        if quSSQUANT.AsFloat>0 then taSSrSS1.AsFloat:=quSSSUMIN.AsFloat/quSSQUANT.AsFloat
        else taSSrSS1.AsFloat:=0;

        taSSrProc1.AsFloat:=0;

        if quSSSUMIN.AsFloat>0 then taSSrProcN1.AsFloat:=(quSSSUMR.AsFloat-quSSSUMIN.AsFloat)/quSSSUMIN.AsFloat*100
        else taSSrProcN1.AsFloat:=0;

        taSSrSS2.AsFloat:=0;
        taSSrProc2.AsFloat:=0;
        taSSrProcN2.AsFloat:=0;
        
        taSSrSS.AsFloat:=0;
        taSSrProcN.AsFloat:=0;
        taSSrPrice.AsFloat:=0;

        taSS.Post;
      end;

      quSS.Next;
    end;

    quSS.Active:=False;
    PBar1.Position:=90;
    delay(10);

    ViewSS.EndUpdate;

    PBar1.Position:=100;
    delay(200);
    PBar1.Visible:=False;

    fmRepSS.Memo1.Lines.Add('������������ ��.'); delay(10);
  end;
end;


procedure TfmRepSS.FormCreate(Sender: TObject);
begin
  GridSS.Align:=AlClient;
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
//  ViewSS.RestoreFromIniFile(CurDir+GridIni);
  taSS.Active:=False;
//  taPrib.FileName:=CurDir+'Prib.cds';

  Prib.MHId:=0;
  Prib.MHAll:=False;
  Prib.MHUnion:=False;
  Prib.MProd:=0;
  Prib.MProdAll:=False;
  Prib.MProdUnion:=False;
  Prib.Cat:=0;
  Prib.CatAll:=False;
  Prib.CatUnion:=False;
  Prib.Salet:='';
  Prib.SaletAll:=False;
  Prib.SaletUnion:=False;
  Prib.Oper:=1;
  Prib.OperAll:=False;
  Prib.OperUnion:=False;
  Prib.OperUnionCli:=False;
end;

procedure TfmRepSS.FormClose(Sender: TObject; var Action: TCloseAction);
begin
//  ViewSS.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmRepSS.FormShow(Sender: TObject);
begin
  Memo1.Clear;
end;

procedure TfmRepSS.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmRepSS.SpeedItem3Click(Sender: TObject);
begin
  prNExportExel5(ViewSS);
end;

procedure TfmRepSS.N1Click(Sender: TObject);
begin
//��������
  ViewSS.BeginUpdate;
  ViewSS.DataController.Groups.FullCollapse;
  ViewSS.EndUpdate;
end;

procedure TfmRepSS.N2Click(Sender: TObject);
begin
//��������
  ViewSS.BeginUpdate;
  ViewSS.DataController.Groups.FullExpand;
  ViewSS.EndUpdate;
end;

end.
