unit MH;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, XPStyleActnCtrls, ActnList, ActnMan, ToolWin,
  ActnCtrls, ActnMenus, ActnColorMaps, cxStyles, cxCustomData, cxGraphics,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, Placemnt, cxContainer,
  cxTreeView, ExtCtrls, Menus;

type
  TfmMH = class(TForm)
    StatusBar1: TStatusBar;
    amMH: TActionManager;
    ActionMainMenuBar1: TActionMainMenuBar;
    acExit: TAction;
    acD: TAction;
    XPColorMap1: TXPColorMap;
    acGroup: TAction;
    acMH: TAction;
    acGroupAdd: TAction;
    acGroupEdit: TAction;
    acGroupDel: TAction;
    acMHAdd: TAction;
    acMHEdit: TAction;
    acMHDel: TAction;
    ViewMH: TcxGridDBTableView;
    LevelMH: TcxGridLevel;
    GridMH: TcxGrid;
    FormPlacement1: TFormPlacement;
    Timer1: TTimer;
    TreeMH: TcxTreeView;
    ViewMHID: TcxGridDBColumn;
    ViewMHNAMEMH: TcxGridDBColumn;
    ViewMHNAMEPRICE: TcxGridDBColumn;
    scSGroupAdd: TAction;
    PopupMenu1: TPopupMenu;
    PopupMenu2: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    ViewMHGLN: TcxGridDBColumn;
    procedure acExitExecute(Sender: TObject);
    procedure acDExecute(Sender: TObject);
    procedure acGroupAddExecute(Sender: TObject);
    procedure acGroupEditExecute(Sender: TObject);
    procedure acGroupDelExecute(Sender: TObject);
    procedure acMHExecute(Sender: TObject);
    procedure acGroupExecute(Sender: TObject);
    procedure acMHAddExecute(Sender: TObject);
    procedure acMHEditExecute(Sender: TObject);
    procedure acMHDelExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TreeMHExpanded(Sender: TObject; Node: TTreeNode);
    procedure TreeMHChange(Sender: TObject; Node: TTreeNode);
    procedure scSGroupAddExecute(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure ViewMHStartDrag(Sender: TObject;
      var DragObject: TDragObject);
    procedure TreeMHDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure TreeMHDragDrop(Sender, Source: TObject; X, Y: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmMH: TfmMH;
  bClearMH: Boolean = False;
  bDr: Boolean = False;

implementation

uses Un1, dmOffice, AddPriceT, AddClass, AddMH;

{$R *.dfm}

procedure TfmMH.acExitExecute(Sender: TObject);
begin
  close;
end;

procedure TfmMH.acDExecute(Sender: TObject);
begin
//
end;

procedure TfmMH.acGroupAddExecute(Sender: TObject);
Var TreeNode : TTreeNode;
    iId,i:Integer;
begin
// �������� ������
  if not CanDo('prAddGroupMh') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  fmAddPriceT:=TfmAddPriceT.Create(Application);
  fmAddPriceT.Caption:='���������� ������.';
  fmAddPriceT.cxTextEdit1.Text:='����� ������';

  fmAddPriceT.ShowModal;
  if fmAddPriceT.ModalResult=mrOk then
  begin
    with dmO do
    begin
      iId:=GetId('MH');

      taMH.Active:=False;
      taMH.Active:=True;

      taMH.Append;
      taMHID.AsInteger:=iId;
      taMHPARENT.AsInteger:=0;
      taMHITYPE.AsInteger:=0;
      taMHNAMEMH.AsString:=Copy(fmAddPriceT.cxTextEdit1.Text,1,150);
      taMHDEFPRICE.AsInteger:=0;
      taMH.Post;

      TreeMH.Items.BeginUpdate;
      TreeNode:=TreeMH.Items.AddChildObject(nil,Copy(fmAddPriceT.cxTextEdit1.Text,1,150),Pointer(iId));
      TreeNode.ImageIndex:=8;
      TreeNode.SelectedIndex:=7;
      TreeMH.Items.AddChildObject(TreeNode,'', nil);
      TreeMH.Items.EndUpdate;

      for i:=0 To TreeMH.Items.Count-1 do
        if Integer(TreeMH.Items[i].Data) = iId then
        begin
          TreeMH.Items[i].Expand(False);
          TreeMH.Items[i].Selected:=True;
          TreeMH.Repaint;
          Break;
        end;

      taMH.Active:=False;
    end;
  end;
  fmAddPriceT.Release;
end;

procedure TfmMH.acGroupEditExecute(Sender: TObject);
begin
//������������� ������ ��
  if not CanDo('prEditGroupMh') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;

  if TreeMH.Selected=nil then
  begin
    showmessage('�������� ������ ��� ��������������.');
    exit;
  end;

  fmAddPriceT:=TfmAddPriceT.Create(Application);
  fmAddPriceT.Caption:='�������������� ������.'+TreeMH.Selected.Text;
  fmAddPriceT.cxTextEdit1.Text:=TreeMH.Selected.Text;

  fmAddPriceT.ShowModal;
  if fmAddPriceT.ModalResult=mrOk then
  begin
    with dmO do
    begin
      taMH.Active:=False;
      taMH.Active:=True;

      if taMH.Locate('ID',Integer(TreeMH.Selected.Data),[]) then
      begin
        taMH.Edit;
        taMHNAMEMH.AsString:=Copy(fmAddPriceT.cxTextEdit1.Text,1,150);
        taMH.Post;

        TreeMH.Items.BeginUpdate;
        TreeMH.Selected.Text:=fmAddPriceT.cxTextEdit1.Text;
        TreeMH.Items.EndUpdate;
      end;

      taMH.Active:=False;
    end;
  end;
  fmAddPriceT.Release;
end;

procedure TfmMH.acGroupDelExecute(Sender: TObject);
begin
//������� ������ ��
  if not CanDo('prDelGroupMh') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  if (TreeMH.Items.Count=0)  then
  begin
//    showmessage('�������� ������.');
    exit;
  end;
  if (TreeMH.Selected=nil)  then
  begin
    showmessage('�������� ������,���������.');
    exit;
  end;

  with dmO do
  begin
    if MessageDlg('�� ������������� ������ ������� ������ - "'+TreeMH.Selected.Text+'"?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      if quMHSel.RecordCount>0 then
      begin
        showmessage('������ �� �����, �������� ����������!!!');
        exit;
      end;
      quMHParent.Active:=False;
      quMHParent.ParamByName('PARENTID').AsInteger:=Integer(TreeMH.Selected.Data);
      quMHParent.Active:=True;

      if quMHParent.RecordCount>0 then
      begin
        showmessage('������ �� �����, �������� ����������!!!');
        quMHParent.Active:=False;
        exit;
      end;
      quMHParent.Active:=False;

      //���� ����� �� ���� �� �������� ��������

      //������
      taMH.Active:=False;
      taMH.Active:=True;
      if taMH.Locate('ID',Integer(TreeMH.Selected.Data),[]) then taMH.Delete;
      taMH.Active:=False;

      //������ � ������
      TreeMH.Selected.Delete;

      //��� ����� ���������� ������ �������� - ����� ������� ������� �� ����
      quMHSel.Active:=False;
      quMHSel.ParamByName('PARENTID').AsInteger:=Integer(TreeMH.Selected.Data);
      quMHSel.Active:=True;
    end;
  end;
end;

procedure TfmMH.acMHExecute(Sender: TObject);
begin
//
end;

procedure TfmMH.acGroupExecute(Sender: TObject);
begin
//
end;

procedure TfmMH.acMHAddExecute(Sender: TObject);
Var iId:Integer;
begin
//�������� ��
  if not CanDo('prAddMh') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  if TreeMH.Selected=nil then
  begin
    showmessage('�������� ������.');
    exit;
  end;


  with dmO do
  begin
    fmAddMH:=TfmAddMH.Create(Application);
    fmAddMH.Caption:='���������� ����� ��������, ������������.';
    fmAddMH.Label4.Caption:='���������� ����� �������� � ������ "'+TreeMh.Selected.Text+'"';
    fmAddMH.cxTextEdit1.Text:='����� ����� ��������';
    fmAddMH.cxTextEdit2.Text:='';
    fmAddMH.cxTextEdit3.Text:='';
    fmAddMH.cxTextEdit4.Text:='';
    fmAddMH.cxTextEdit5.Text:='';
    fmAddMH.cxTextEdit6.Text:='';
    fmAddMH.cxTextEdit7.Text:='';
    fmAddMH.cxTextEdit8.Text:='';
    fmAddMH.cxTextEdit9.Text:='';
    fmAddMH.cxTextEdit10.Text:='';
    fmAddMH.cxTextEdit11.Text:='';
    fmAddMH.cxTextEdit12.Text:='';
    fmAddMH.cxTextEdit13.Text:='';
    fmAddMH.cxTextEdit14.Text:='';
    fmAddMH.cxTextEdit15.Text:='';
    fmAddMH.cxTextEdit16.Text:='';
    fmAddMH.cxTextEdit17.Text:='';
    fmAddMH.cxTextEdit18.Text:='';
    fmAddMH.cxTextEdit19.Text:='';
    fmAddMH.cxTextEdit20.Text:='';
    fmAddMH.cxTextEdit21.Text:='';

    fmAddMH.cxComboBox1.ItemIndex:=0;
    fmAddMH.cxComboBox2.ItemIndex:=0;
    fmAddMH.cxComboBox3.ItemIndex:=0;

    fmAddMH.cxDateEdit1.Date:=date;
    fmAddMH.cxDateEdit2.Date:=date;

    quPriceTSel.Active:=False;
    quPriceTSel.Active:=True;
    fmAddMH.cxLookupComboBox1.EditValue:=1;
    fmAddMH.ShowModal;
    if fmAddMH.ModalResult=mrOk then
    begin
      iId:=GetId('MH');

      taMH.Active:=False;
      taMH.Active:=True;

      taMH.Append;
      taMHID.AsInteger:=iId;
      taMHPARENT.AsInteger:=Integer(TreeMH.Selected.Data);
      taMHITYPE.AsInteger:=1;
      taMHNAMEMH.AsString:=Copy(fmAddMH.cxTextEdit1.Text,1,150);
      taMHDEFPRICE.AsInteger:=fmAddMH.cxLookupComboBox1.EditValue;
      taMHGLN.AsString:=fmAddMH.cxTextEdit2.Text;
      if fmAddMH.cxComboBox1.ItemIndex=0 then taMHISS.AsInteger:=0
      else taMHISS.AsInteger:=2;
      taMHPRIOR.AsInteger:=fmAddMH.cxComboBox2.ItemIndex;
      taMHFULLNAME.AsString:=fmAddMH.cxTextEdit5.Text;
      taMHINN.AsString:=fmAddMH.cxTextEdit3.Text;
      taMHKPP.AsString:=fmAddMH.cxTextEdit4.Text;
      taMHDIR1.AsString:=fmAddMH.cxTextEdit12.Text;
      taMHDIR2.AsString:=fmAddMH.cxTextEdit13.Text;
      taMHDIR3.AsString:=fmAddMH.cxTextEdit14.Text;
      taMHGB1.AsString:=fmAddMH.cxTextEdit15.Text;
      taMHGB2.AsString:=fmAddMH.cxTextEdit16.Text;
      taMHGB3.AsString:=fmAddMH.cxTextEdit17.Text;
      taMHCITY.AsString:=fmAddMH.cxTextEdit6.Text;
      taMHSTREET.AsString:=fmAddMH.cxTextEdit7.Text;
      taMHHOUSE.AsString:=fmAddMH.cxTextEdit8.Text;
      taMHKORP.AsString:=fmAddMH.cxTextEdit9.Text;
      taMHPOSTINDEX.AsString:=fmAddMH.cxTextEdit10.Text;
      taMHPHONE.AsString:=fmAddMH.cxTextEdit11.Text;
      taMHSERLIC.AsString:=fmAddMH.cxTextEdit18.Text;
      taMHNUMLIC.AsString:=fmAddMH.cxTextEdit19.Text;
      taMHORGAN.AsString:=fmAddMH.cxTextEdit20.Text;
      taMHDATEB.AsDateTime:=fmAddMH.cxDateEdit1.Date;
      taMHDATEE.AsDateTime:=fmAddMH.cxDateEdit2.Date;
      taMHIP.AsInteger:=fmAddMH.cxComboBox3.ItemIndex;
      taMHIPREG.AsString:=fmAddMH.cxTextEdit21.Text;
      taMH.Post;

      taMH.Active:=False;

      quMHSel.Active:=False;
      quMHSel.ParamByName('PARENTID').AsInteger:=Integer(TreeMH.Selected.Data);
      quMHSel.Active:=True;

    end;
    quPriceTSel.Active:=False;
    fmAddMH.Release;
  end;
end;

procedure TfmMH.acMHEditExecute(Sender: TObject);
begin
// ������������� ��
  if not CanDo('prEditMh') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  if TreeMH.Selected=nil then
  begin
    showmessage('�������� ������.');
    exit;
  end;

  with dmO do
  begin
    if quMHSel.RecordCount=0 then exit;

    fmAddMH:=TfmAddMH.Create(Application);
    fmAddMH.Caption:='�������������� ����� ��������, ������������.';
    fmAddMH.Label4.Caption:='�������������� �� "'+quMHSelNAMEMH.AsString+'"';
    fmAddMH.cxTextEdit1.Text:=quMHSelNAMEMH.AsString;
    fmAddMH.cxTextEdit2.Text:=quMHSelGLN.AsString;

    if quMHSelISS.AsInteger=0 then fmAddMH.cxComboBox1.ItemIndex:=0
    else fmAddMH.cxComboBox1.ItemIndex:=1;

    fmAddMH.cxComboBox2.ItemIndex:=quMHSelPRIOR.AsInteger;

    fmAddMH.cxTextEdit5.Text:=quMHSelFULLNAME.AsString;
    fmAddMH.cxTextEdit3.Text:=quMHSelINN.AsString;
    fmAddMH.cxTextEdit4.Text:=quMHSelKPP.AsString;
    fmAddMH.cxTextEdit12.Text:=quMHSelDIR1.AsString;
    fmAddMH.cxTextEdit13.Text:=quMHSelDIR2.AsString;
    fmAddMH.cxTextEdit14.Text:=quMHSelDIR3.AsString;
    fmAddMH.cxTextEdit15.Text:=quMHSelGB1.AsString;
    fmAddMH.cxTextEdit16.Text:=quMHSelGB2.AsString;
    fmAddMH.cxTextEdit17.Text:=quMHSelGB3.AsString;
    fmAddMH.cxTextEdit6.Text:=quMHSelCITY.AsString;
    fmAddMH.cxTextEdit7.Text:=quMHSelSTREET.AsString;
    fmAddMH.cxTextEdit8.Text:=quMHSelHOUSE.AsString;
    fmAddMH.cxTextEdit9.Text:=quMHSelKORP.AsString;
    fmAddMH.cxTextEdit10.Text:=quMHSelPOSTINDEX.AsString;
    fmAddMH.cxTextEdit11.Text:=quMHSelPHONE.AsString;
    fmAddMH.cxTextEdit18.Text:=quMHSelSERLIC.AsString;
    fmAddMH.cxTextEdit19.Text:=quMHSelNUMLIC.AsString;
    fmAddMH.cxTextEdit20.Text:=quMHSelORGAN.AsString;
    fmAddMH.cxDateEdit1.Date:=quMHSelDATEB.AsDateTime;
    fmAddMH.cxDateEdit2.Date:=quMHSelDATEE.AsDateTime;
    fmAddMH.cxComboBox3.ItemIndex:=quMHSelIP.AsInteger;
    fmAddMH.cxTextEdit21.Text:=quMHSelIPREG.AsString;

    quPriceTSel.Active:=False;
    quPriceTSel.Active:=True;
    fmAddMH.cxLookupComboBox1.EditValue:=quMHSelDEFPRICE.AsInteger;
    fmAddMH.ShowModal;
    if fmAddMH.ModalResult=mrOk then
    begin
      quMHSel.Edit;
      quMHSelNAMEMH.AsString:=Copy(fmAddMH.cxTextEdit1.Text,1,150);
      quMHSelDEFPRICE.AsInteger:=fmAddMH.cxLookupComboBox1.EditValue;
      quMHSelGLN.AsString:=fmAddMH.cxTextEdit2.Text;

      if fmAddMH.cxComboBox1.ItemIndex=0 then quMHSelISS.AsInteger:=0
      else quMHSelISS.AsInteger:=2;
      quMHSelPRIOR.AsInteger:=fmAddMH.cxComboBox2.ItemIndex;
      quMHSelFULLNAME.AsString:=fmAddMH.cxTextEdit5.Text;
      quMHSelINN.AsString:=fmAddMH.cxTextEdit3.Text;
      quMHSelKPP.AsString:=fmAddMH.cxTextEdit4.Text;
      quMHSelDIR1.AsString:=fmAddMH.cxTextEdit12.Text;
      quMHSelDIR2.AsString:=fmAddMH.cxTextEdit13.Text;
      quMHSelDIR3.AsString:=fmAddMH.cxTextEdit14.Text;
      quMHSelGB1.AsString:=fmAddMH.cxTextEdit15.Text;
      quMHSelGB2.AsString:=fmAddMH.cxTextEdit16.Text;
      quMHSelGB3.AsString:=fmAddMH.cxTextEdit17.Text;
      quMHSelCITY.AsString:=fmAddMH.cxTextEdit6.Text;
      quMHSelSTREET.AsString:=fmAddMH.cxTextEdit7.Text;
      quMHSelHOUSE.AsString:=fmAddMH.cxTextEdit8.Text;
      quMHSelKORP.AsString:=fmAddMH.cxTextEdit9.Text;
      quMHSelPOSTINDEX.AsString:=fmAddMH.cxTextEdit10.Text;
      quMHSelPHONE.AsString:=fmAddMH.cxTextEdit11.Text;
      quMHSelSERLIC.AsString:=fmAddMH.cxTextEdit18.Text;
      quMHSelNUMLIC.AsString:=fmAddMH.cxTextEdit19.Text;
      quMHSelORGAN.AsString:=fmAddMH.cxTextEdit20.Text;
      quMHSelDATEB.AsDateTime:=fmAddMH.cxDateEdit1.Date;
      quMHSelDATEE.AsDateTime:=fmAddMH.cxDateEdit2.Date;
      quMHSelIP.AsInteger:=fmAddMH.cxComboBox3.ItemIndex;
      quMHSelIPREG.AsString:=fmAddMH.cxTextEdit21.Text;

      quMHSel.Post;
    end;
    quPriceTSel.Active:=False;
    fmAddMH.Release;
  end;
end;

procedure TfmMH.acMHDelExecute(Sender: TObject);
begin
// �������� ����� ��������
  if not CanDo('prDelMh') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  with dmO do
  begin
    if quMHSel.Eof then exit;

    if MessageDlg('�� ������������� ������ ������� �� - "'+quMHSelNAMEMH.AsString+'"?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      if MessageDlg('����������� ��������, �������� �������� ����� ����������!!!', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        quMHSel.Delete;
      end;
    end;
  end;
end;

procedure TfmMH.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  GridMH.Align:=AlClient;
  Timer1.Enabled:=True;
end;

procedure TfmMH.TreeMHExpanded(Sender: TObject; Node: TTreeNode);
begin
  if Node.getFirstChild.Data = nil then
  begin
    Node.DeleteChildren;
    ClassifEx(Node,TreeMH,dmO.quMHTree,0,'NAMEMH');
  end;
end;

procedure TfmMH.TreeMHChange(Sender: TObject; Node: TTreeNode);
begin
  with dmO do
  begin
    quMHSel.Active:=False;
    quMHSel.ParamByName('PARENTID').AsInteger:=Integer(Node.Data);
    quMHSel.Active:=True;
  end;
end;

procedure TfmMH.scSGroupAddExecute(Sender: TObject);
Var TreeNode,CurNode: TTreeNode;
    iId,i,iParent:Integer;
begin
//�������� ���������
  if not CanDo('prAddSGroupMh') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;

  if TreeMH.Selected=nil then
  begin
    showmessage('�������� ������ ��� ���������� ���������.');
    exit;
  end;

  iParent:=Integer(TreeMH.Selected.Data);
  CurNode:=TreeMH.Selected;
  with dmO do
  begin
    taMH.Active:=False;
    taMH.Active:=True;
    if taMH.Locate('ID',iParent,[]) then
    begin
      fmAddClass.Caption:='���������� ��������� � ������ "'+CurNode.Text+'"';
      fmAddClass.label4.Caption:='���������� ��������� � ������ "'+CurNode.Text+'"';
      fmAddClass.cxTextEdit1.Text:='';

      fmAddClass.ShowModal;
      if fmAddClass.ModalResult=mrOk then
      begin
        iId:=GetId('MH');

        taMH.Append;
        taMHID.AsInteger:=iId;
        taMHPARENT.AsInteger:=iParent;
        taMHITYPE.AsInteger:=0;
        taMHNAMEMH.AsString:=Copy(fmAddClass.cxTextEdit1.Text,1,150);
        taMHDEFPRICE.AsInteger:=0;
        taMH.Post;

        TreeMH.Items.BeginUpdate;
        TreeNode:=TreeMH.Items.AddChildObject(CurNode,Copy(fmAddClass.cxTextEdit1.Text,1,150),Pointer(iId));
        TreeNode.ImageIndex:=8;
        TreeNode.SelectedIndex:=7;
        TreeMH.Items.AddChildObject(TreeNode,'', nil);
        TreeMH.Items.EndUpdate;

        for i:=0 To TreeMH.Items.Count-1 do
          if Integer(TreeMH.Items[i].Data) = iId then
          begin
            TreeMH.Items[i].Expand(False);
            TreeMH.Items[i].Selected:=True;
            TreeMH.Repaint;
            Break;
          end;

        taMH.Active:=False;
      end;
    end;
  end;
end;

procedure TfmMH.Timer1Timer(Sender: TObject);
begin
  if bClearMH=True then begin StatusBar1.Panels[0].Text:=''; bClearMH:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearMH:=True;
end;

procedure TfmMH.ViewMHStartDrag(Sender: TObject;
  var DragObject: TDragObject);
begin
//
  if not CanDo('prMoveMH') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  bDr:=True;
end;

procedure TfmMH.TreeMHDragOver(Sender, Source: TObject; X, Y: Integer;
  State: TDragState; var Accept: Boolean);
begin
  Accept:=False;
  if bDr then  Accept:=True;
end;

procedure TfmMH.TreeMHDragDrop(Sender, Source: TObject; X, Y: Integer);
Var sGr:String;
    iGr:Integer;
    iCo:Integer;
    i,j: Integer;
    iNum: Integer;
    Rec:TcxCustomGridRecord;
begin
// ����������� �� � ������ ������
  if bDr then
  begin
    bDr:=False;
    sGr:=TreeMH.DropTarget.Text;
    iGr:=Integer(TreeMH.DropTarget.data);
    iCo:=ViewMH.Controller.SelectedRecordCount;
    if iCo>0 then
    begin
      if MessageDlg('�� ������������� ������ ����������� ��������� ������� ('+IntToStr(iCo)+' ��.) � ������ "'+sGr+'"?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        with dmO do
        begin
          taMH.Active:=False;
          taMH.Active:=True;

          for i:=0 to ViewMH.Controller.SelectedRecordCount-1 do
          begin
            Rec:=ViewMH.Controller.SelectedRecords[i];

            for j:=0 to Rec.ValueCount-1 do
            begin
              if ViewMH.Columns[j].Name='ViewMHID' then break;
            end;

            iNum:=Rec.Values[j];
          //��� ��� - ����������

            if taMH.Locate('ID',iNum,[]) then
            begin
              taMH.Edit;
              taMHPARENT.AsInteger:=iGr;
              taMH.Post;
            end;
          end;
          taMH.Active:=False;
          quMHSel.FullRefresh;
        end;
      end;
    end;
  end;
end;

end.
