unit MainRnEdit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ToolWin, ActnMan, ActnCtrls, ActnMenus,
  XPStyleActnCtrls, ActnList, ExtCtrls, SpeedBar, Menus;

type
  TfmMainRnEdit = class(TForm)
    StatusBar1: TStatusBar;
    am1: TActionManager;
    ActionMainMenuBar1: TActionMainMenuBar;
    acExit: TAction;
    acMenu: TAction;
    acModify: TAction;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    acCateg: TAction;
    SpeedItem5: TSpeedItem;
    acMessage: TAction;
    SpeedItem6: TSpeedItem;
    acDayMenu: TAction;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    ColorDialog1: TColorDialog;
    acStandartDBar: TAction;
    SpeedItem7: TSpeedItem;
    acDelPar: TAction;
    SpeedItem8: TSpeedItem;
    acCategSale: TAction;
    acStopList: TAction;
    SpeedItem9: TSpeedItem;
    acScalePlu: TAction;
    procedure FormCanResize(Sender: TObject; var NewWidth,
      NewHeight: Integer; var Resize: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure acMenuExecute(Sender: TObject);
    procedure acModifyExecute(Sender: TObject);
    procedure acCategExecute(Sender: TObject);
    procedure acMessageExecute(Sender: TObject);
    procedure acDayMenuExecute(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure acStandartDBarExecute(Sender: TObject);
    procedure acDelParExecute(Sender: TObject);
    procedure acCategSaleExecute(Sender: TObject);
    procedure acStopListExecute(Sender: TObject);
    procedure acScalePluExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmMainRnEdit: TfmMainRnEdit;

implementation

uses Un1, dmRnEdit, PerA, MenuCr, ModifCr, Categories, messagecash,
  MenuDayList, DelPars, CategSale, StopList, Scale;

{$R *.dfm}

procedure TfmMainRnEdit.FormCanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
  NewHeight:=125;
end;

procedure TfmMainRnEdit.FormCreate(Sender: TObject);
begin
  CurDir := ExtractFilePath(ParamStr(0));
  ReadIni;
  CommonSet.DateFrom:=Date-14;
  CommonSet.DateTo:=Date+1;
  if UserColor.Color1<>0 then SpeedBar1.Color:=UserColor.Color1;
end;

procedure TfmMainRnEdit.FormResize(Sender: TObject);
begin
  StatusBar1.Width:=Width;
end;

procedure TfmMainRnEdit.FormShow(Sender: TObject);
begin
  Left:=0;
  Top:=0;
  Width:=1024;

  with dmC do
  begin
    quPer.Active:=False;
    quPer.SelectSQL.Clear;
    quPer.SelectSQL.Add('select * from rpersonal');
    quPer.SelectSQL.Add('where uvolnen=1 and modul6=0');
    quPer.SelectSQL.Add('order by Name');
//    quPer.Active:=True; ����������� �� show fmPerA
  end;
  fmPerA:=TfmPerA.Create(Application);
  fmPerA.ShowModal;
  if fmPerA.ModalResult=mrOk then
  begin
//    prUpd;

    Caption:=Caption+'   : '+Person.Name;
    WriteIni; //�������� �������� �������
    fmPerA.Release;

    bMenuList:=False;
    bMenuListMo:=False;
    fmMenuCr:=tfmMenuCr.Create(Application);
    fmModCr:=tfmModCr.Create(Application);

    fmMenuCr.Show;
    bMenuList:=True;
    bMenuListMo:=True;

  end
  else
  begin
    fmPerA.Release;
    delay(100);
    close;
    delay(100);
  end;
end;

procedure TfmMainRnEdit.acExitExecute(Sender: TObject);
begin
  close;
end;

procedure TfmMainRnEdit.acMenuExecute(Sender: TObject);
begin
  //Menu
  fmMenuCr.Show;
end;

procedure TfmMainRnEdit.acModifyExecute(Sender: TObject);
begin
  //modify
  fmModCr.Show;
end;

procedure TfmMainRnEdit.acCategExecute(Sender: TObject);
begin
  fmCateg.Show;
end;

procedure TfmMainRnEdit.acMessageExecute(Sender: TObject);
begin
  //���������
  with dmC do
  begin
    if taMessage.Active=False then taMessage.Active:=True;
    taMessage.FullRefresh;
    
    fmMessage.Show;
  end;
end;

procedure TfmMainRnEdit.acDayMenuExecute(Sender: TObject);
begin
 //������� ����
  with dmC do
  begin
    fmMenuDayList.ViewMDL.BeginUpdate;
    quMenuDayList.Active:=False;
    quMenuDayList.ParamByName('DATEB').AsDateTime:=CommonSet.DateFrom;
    quMenuDayList.ParamByName('DATEE').AsDateTime:=CommonSet.DateTo;
    quMenuDayList.Active:=True;
    fmMenuDayList.ViewMDL.EndUpdate;

    fmMenuDayList.Show;

  end;
end;

procedure TfmMainRnEdit.N1Click(Sender: TObject);
begin
  ColorDialog1.Color:=SpeedBar1.Color;
  if ColorDialog1.Execute then
  begin
    SpeedBar1.Color := ColorDialog1.Color;
    UserColor.Color1:=ColorDialog1.Color;
    WriteColor;
  end;  
end;

procedure TfmMainRnEdit.acStandartDBarExecute(Sender: TObject);
Var Str1,Str2:String;
    iC:Integer;

function fSta(S:String):String;
begin
  while pos(';',S)>0 do delete(S,pos(';',S),1);
  while pos('?',S)>0 do delete(S,pos('?',S),1);
  Result:=S;
end;

begin
  with dmC do
  begin
    taTabsAll.Active:=False;
    taTabsAll.Active:=True;
    taTabsAll.First;
    iC:=0;
    while not taTabsAll.Eof do
    begin
      if (taTabsAllDISCONT1.AsString>'')or(taTabsAllDISCONT.AsString>'') then
      begin
        Str1:=taTabsAllDISCONT.AsString;
        Str2:=taTabsAllDISCONT1.AsString;
        Str1:=fSta(Str1);
        Str2:=fSta(Str2);

        taTabsAll.Edit;
        taTabsAllDISCONT.AsString:=Str1;
        taTabsAllDISCONT1.AsString:=Str2;
        taTabsAll.Post;

        delay(10);
      end;
      taTabsAll.Next;
      inc(iC);
      if iC mod 100 = 0 then
      begin
        StatusBar1.Panels[0].Text:=IntToStr(iC);
        delay(10);
      end;
    end;
    taTabsAll.Active:=False;
    ShowMessage('Ok');
  end;
end;

procedure TfmMainRnEdit.acDelParExecute(Sender: TObject);
begin
//������� ��������
  //���������
  with dmC do
  begin
    if taDelPar.Active=False then taDelPar.Active:=True;
    taDelPar.FullRefresh;

    fmDelPar.Show;
  end;
end;

procedure TfmMainRnEdit.acCategSaleExecute(Sender: TObject);
begin
  //��������� ������
  with dmC do
  begin
    if taCategSale.Active=False then taCategSale.Active:=True;
    taCategSale.FullRefresh;

    fmCategSale.Show;
  end;
end;

procedure TfmMainRnEdit.acStopListExecute(Sender: TObject);
begin
  fmStopList.Show;

  with dmC do
  begin
    fmStopList.ViewSL.BeginUpdate;
    quMenuSL.Active:=False;
    quMenuSL.Active:=True;
    fmStopList.ViewSL.EndUpdate;
  end;

end;

procedure TfmMainRnEdit.acScalePluExecute(Sender: TObject);
begin
  //������ ��� �������� �����
  with dmC do
  begin
    quScales.Active:=False;
    quScales.Active:=True;
    if quScales.RecordCount>0 then
    begin
      quScales.First;
      quScaleItems.Active:=False;
      quScaleItems.ParamByName('IDSC').AsInteger:=quScalesID.AsInteger;
      quScaleItems.Active:=True;

      fmScale:=tfmScale.Create(Application);
      fmScale.cxLookupComboBox1.EditValue:=quScalesID.AsInteger;
      fmScale.ShowModal;
      fmScale.Release;
      quScaleItems.Active:=False;
    end;
    quScales.Active:=False;
  end;
end;

end.
