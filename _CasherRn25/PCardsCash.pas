unit PCardsCash;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons, ExtCtrls,
  cxCurrencyEdit, cxControls, cxContainer, cxEdit, cxTextEdit;

type
  TfmPCardsRn = class(TForm)
    Panel2: TPanel;
    cxButton10: TcxButton;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    cxButton5: TcxButton;
    cxButton6: TcxButton;
    cxButton7: TcxButton;
    cxButton8: TcxButton;
    cxButton9: TcxButton;
    cxButton11: TcxButton;
    cxButton12: TcxButton;
    cxButton14: TcxButton;
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxTextEdit1: TcxTextEdit;
    Label1: TLabel;
    cxCurrencyEdit1: TcxCurrencyEdit;
    cxCurrencyEdit2: TcxCurrencyEdit;
    cxCurrencyEdit3: TcxCurrencyEdit;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    cxCurrencyEdit4: TcxCurrencyEdit;
    cxButton2: TcxButton;
    Label5: TLabel;
    cxCurrencyEdit5: TcxCurrencyEdit;
    Label6: TLabel;
    Label7: TLabel;
    procedure FormShow(Sender: TObject);
    procedure cxButton10Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
    procedure cxButton7Click(Sender: TObject);
    procedure cxButton8Click(Sender: TObject);
    procedure cxButton9Click(Sender: TObject);
    procedure cxButton11Click(Sender: TObject);
    procedure cxButton12Click(Sender: TObject);
    procedure cxCurrencyEdit4Enter(Sender: TObject);
    procedure cxTextEdit1Enter(Sender: TObject);
    procedure cxButton14Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxCurrencyEdit5Enter(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmPCardsRn: TfmPCardsRn;

implementation

uses Un1, prdb, Dm, Attention, UnCash, UnPrizma, MainCashRn, uDB1;

{$R *.dfm}

procedure TfmPCardsRn.FormShow(Sender: TObject);
begin
  cxCurrencyEdit4.Value:=0;
  cxTextEdit1.Clear;
  cxTextEdit1.SetFocus;
  cxTextEdit1.Tag:=1;
  cxCurrencyEdit4.Tag:=0;
  cxCurrencyEdit5.Tag:=0;

  cxCurrencyEdit1.Value:=0;
  cxCurrencyEdit2.Value:=0;
  cxCurrencyEdit3.Value:=0;
  cxCurrencyEdit4.Value:=0;
  cxCurrencyEdit5.Value:=0;
  Label5.Caption:='';
end;

procedure TfmPCardsRn.cxButton10Click(Sender: TObject);
begin
  if cxTextEdit1.Tag=1 then  cxTextEdit1.Text:=cxTextEdit1.Text+'0';
  if cxCurrencyEdit4.tag=1 then
  begin
    if cxCurrencyEdit4.Value>9999999 then exit;
    cxCurrencyEdit4.Value:=cxCurrencyEdit4.Value*10+0;
  end;
  if cxCurrencyEdit5.tag=1 then
  begin
    if cxCurrencyEdit5.Value>9999999 then exit;
    cxCurrencyEdit5.Value:=cxCurrencyEdit5.Value*10+0;
  end;
end;

procedure TfmPCardsRn.cxButton3Click(Sender: TObject);
begin
  if cxTextEdit1.Tag=1 then  cxTextEdit1.Text:=cxTextEdit1.Text+'1';
  if cxCurrencyEdit4.tag=1 then
  begin
    if cxCurrencyEdit4.Value>9999999 then exit;
    cxCurrencyEdit4.Value:=cxCurrencyEdit4.Value*10+1;
  end;
  if cxCurrencyEdit5.tag=1 then
  begin
    if cxCurrencyEdit5.Value>9999999 then exit;
    cxCurrencyEdit5.Value:=cxCurrencyEdit5.Value*10+1;
  end;
end;

procedure TfmPCardsRn.cxButton4Click(Sender: TObject);
begin
  if cxTextEdit1.Tag=1 then  cxTextEdit1.Text:=cxTextEdit1.Text+'2';
  if cxCurrencyEdit4.tag=1 then
  begin
    if cxCurrencyEdit4.Value>9999999 then exit;
    cxCurrencyEdit4.Value:=cxCurrencyEdit4.Value*10+2;
  end;
  if cxCurrencyEdit5.tag=1 then
  begin
    if cxCurrencyEdit5.Value>9999999 then exit;
    cxCurrencyEdit5.Value:=cxCurrencyEdit5.Value*10+2;
  end;

end;

procedure TfmPCardsRn.cxButton5Click(Sender: TObject);
begin
  if cxTextEdit1.Tag=1 then  cxTextEdit1.Text:=cxTextEdit1.Text+'3';
  if cxCurrencyEdit4.tag=1 then
  begin
    if cxCurrencyEdit4.Value>9999999 then exit;
    cxCurrencyEdit4.Value:=cxCurrencyEdit4.Value*10+3;
  end;
  if cxCurrencyEdit5.tag=1 then
  begin
    if cxCurrencyEdit5.Value>9999999 then exit;
    cxCurrencyEdit5.Value:=cxCurrencyEdit5.Value*10+3;
  end;

end;

procedure TfmPCardsRn.cxButton6Click(Sender: TObject);
begin
  if cxTextEdit1.Tag=1 then  cxTextEdit1.Text:=cxTextEdit1.Text+'4';
  if cxCurrencyEdit4.tag=1 then
  begin
    if cxCurrencyEdit4.Value>9999999 then exit;
    cxCurrencyEdit4.Value:=cxCurrencyEdit4.Value*10+4;
  end;
  if cxCurrencyEdit5.tag=1 then
  begin
    if cxCurrencyEdit5.Value>9999999 then exit;
    cxCurrencyEdit5.Value:=cxCurrencyEdit5.Value*10+4;
  end;
end;

procedure TfmPCardsRn.cxButton7Click(Sender: TObject);
begin
  if cxTextEdit1.Tag=1 then  cxTextEdit1.Text:=cxTextEdit1.Text+'5';
  if cxCurrencyEdit4.tag=1 then
  begin
    if cxCurrencyEdit4.Value>9999999 then exit;
    cxCurrencyEdit4.Value:=cxCurrencyEdit4.Value*10+5;
  end;
  if cxCurrencyEdit5.tag=1 then
  begin
    if cxCurrencyEdit5.Value>9999999 then exit;
    cxCurrencyEdit5.Value:=cxCurrencyEdit5.Value*10+5;
  end;
end;

procedure TfmPCardsRn.cxButton8Click(Sender: TObject);
begin
  if cxTextEdit1.Tag=1 then  cxTextEdit1.Text:=cxTextEdit1.Text+'6';
  if cxCurrencyEdit4.tag=1 then
  begin
    if cxCurrencyEdit4.Value>9999999 then exit;
    cxCurrencyEdit4.Value:=cxCurrencyEdit4.Value*10+6;
  end;
  if cxCurrencyEdit5.tag=1 then
  begin
    if cxCurrencyEdit5.Value>9999999 then exit;
    cxCurrencyEdit5.Value:=cxCurrencyEdit5.Value*10+6;
  end;

end;

procedure TfmPCardsRn.cxButton9Click(Sender: TObject);
begin
  if cxTextEdit1.Tag=1 then  cxTextEdit1.Text:=cxTextEdit1.Text+'7';
  if cxCurrencyEdit4.tag=1 then
  begin
    if cxCurrencyEdit4.Value>9999999 then exit;
    cxCurrencyEdit4.Value:=cxCurrencyEdit4.Value*10+7;
  end;
  if cxCurrencyEdit5.tag=1 then
  begin
    if cxCurrencyEdit5.Value>9999999 then exit;
    cxCurrencyEdit5.Value:=cxCurrencyEdit5.Value*10+7;
  end;
end;

procedure TfmPCardsRn.cxButton11Click(Sender: TObject);
begin
  if cxTextEdit1.Tag=1 then  cxTextEdit1.Text:=cxTextEdit1.Text+'8';
  if cxCurrencyEdit4.tag=1 then
  begin
    if cxCurrencyEdit4.Value>9999999 then exit;
    cxCurrencyEdit4.Value:=cxCurrencyEdit4.Value*10+8;
  end;
  if cxCurrencyEdit5.tag=1 then
  begin
    if cxCurrencyEdit5.Value>9999999 then exit;
    cxCurrencyEdit5.Value:=cxCurrencyEdit5.Value*10+8;
  end;
end;

procedure TfmPCardsRn.cxButton12Click(Sender: TObject);
begin
  if cxTextEdit1.Tag=1 then  cxTextEdit1.Text:=cxTextEdit1.Text+'9';
  if cxCurrencyEdit4.tag=1 then
  begin
    if cxCurrencyEdit4.Value>9999999 then exit;
    cxCurrencyEdit4.Value:=cxCurrencyEdit4.Value*10+9;
  end;
  if cxCurrencyEdit5.tag=1 then
  begin
    if cxCurrencyEdit5.Value>9999999 then exit;
    cxCurrencyEdit5.Value:=cxCurrencyEdit5.Value*10+9;
  end;

end;

procedure TfmPCardsRn.cxCurrencyEdit4Enter(Sender: TObject);
begin
  cxTextEdit1.Tag:=0;
  cxCurrencyEdit4.Tag:=1;
  cxCurrencyEdit4.SelectAll;
  cxCurrencyEdit5.Tag:=0;

  cxTextEdit1.SelectAll;
  cxCurrencyEdit4.SetFocus;
end;

procedure TfmPCardsRn.cxTextEdit1Enter(Sender: TObject);
begin
  cxTextEdit1.Tag:=1;
  cxTextEdit1.SelectAll;
  cxCurrencyEdit4.Tag:=0;
  cxCurrencyEdit5.Tag:=0;
  cxTextEdit1.SetFocus;
end;

procedure TfmPCardsRn.cxButton14Click(Sender: TObject);
Var DiscountBar:String;
    rBalans,rBalansDay,DLimit:Real;
    CliName,CliType:String;
begin
  cxTextEdit1.Tag:=0;
  cxTextEdit1.SelectAll;
  cxCurrencyEdit4.Tag:=1;
  cxCurrencyEdit4.SelectAll;
  cxCurrencyEdit4.SetFocus;

  if cxTextEdit1.Text>'' then
  begin
    DiscountBar:=SOnlyDigit(cxTextEdit1.Text);
    prFindPCardBalans(DiscountBar,rBalans,rBalansDay,DLimit,CliName,CliType);
    cxCurrencyEdit1.Value:=rBalans;
    cxCurrencyEdit2.Value:=rBalansDay;
    cxCurrencyEdit3.Value:=DLimit;
    Label5.Caption:=CliName+' ('+CliType+')';
//    Label5.Tag:=CliType;
//    Showmessage('     �������� - '+CliName+#$0D+'     ��� - '+CliType+#$0D+'     ������ - '+fts(rv(rBalans))+#$0D+'     ������ ������� - '+fts(rv(rBalansDay))+#$0D+'     ������� ����� - '+fts(rv(DLimit)));
  end;
end;

procedure TfmPCardsRn.cxButton2Click(Sender: TObject);
Var DiscountBar:String;
    rBalans,rBalansDay,DLimit:Real;
    CliName,CliType:String;
    bChOk:Boolean;
    iRet,TestPrint:Integer;
    rSum,rSumN,rSumBn:Real;
    iSumPos,iSumTotal,iSumIt,iCurCheck:Integer;
    idHead:Integer;
    idC:Integer;
    StrWk,StrWk1:string;
    StrP:string;
    iQ:Integer;
begin
  if not CanDo('prEditBalancePC') then begin  Showmessage('��� ����'); exit; end;
  with dmC do
  with dmPC do
  begin
    DiscountBar:=SOnlyDigit(cxTextEdit1.Text);
    if (DiscountBar>'')and((cxCurrencyEdit4.Value>0)or(cxCurrencyEdit5.Value>0)) then
    begin
      prFindPCardBalans(DiscountBar,rBalans,rBalansDay,DLimit,CliName,CliType);
      if CliName>'' then
      begin
        rSumN:=RV(cxCurrencyEdit4.Value);
        rSumBn:=RV(cxCurrencyEdit5.Value);
        rSum:=rSumN+rSumBn;

        if MessageDlg('������ ����� '+fts(rSum)+' �� ����� '+CliName,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          bChOk:=True;

          iCurCheck:=Nums.iCheckNum+1;

          if (CommonSet.DebetCheck=1) and (rSum>0) then
          begin //�������� ���
            bChOk:=False;

            if More24H(iRet) then
            begin
              fmAttention.Label1.Caption:='������ ����� 24 �����. ���������� ������� �����.';
              prWriteLog('~~AttentionShow;'+'������ ����� 24 �����. ���������� ������� �����.');
              fmAttention.ShowModal;
              exit;
            end;

            prWriteLog('--RaschPCardDebet;'+fts(cxCurrencyEdit4.Value)+';'+Person.Name);

            TestPrint:=1;  
            While PrintQu and (TestPrint<=MaxDelayPrint) do
            begin
      //        showmessage('������� �����, ��������� �������.');
              prWriteLog('������������� ������ ���� � �������.');
              delay(1000);
              inc(TestPrint);
            end;

            if (CommonSet.SpecChar=0)and(CommonSet.CashNum>0) then //������� ���������� �����
            begin
              CashDriver;
              GetNums;
              prWriteLog('!!CheckStart;'+IntToStr((Nums.iCheckNum+1))+';'+IntToStr(Person.Id)+';'+Person.Name+';');

              Event_RegEx(4,Nums.iCheckNum,0,0,'','',0,0,0,0,0,0); //������ ����

              if CheckStart = False then
              begin
                TestStatus('CheckStart',sMessage);
                fmAttention.Label1.Caption:=sMessage;
                prWriteLog('~~AttentionShow;'+sMessage);
                fmAttention.ShowModal;
                CheckCancel;
                exit;
              end;

              PosCh.Name:='�������� ����� �� ����';
              PosCh.Code:='';
              PosCh.AddName:='';
              PosCh.Price:=RoundEx(rSum*100); //� ��������
              PosCh.Count:=1000; //� �������
              PosCh.Sum:=RoundEx(rSum*100)/100;

              Event_RegEx(6,Nums.iCheckNum,1,0,'',PosCh.Name,PosCh.Price,1,PosCh.Sum,PosCh.Sum,0,0);

              CheckAddPos(iSumPos);
              while TestStatus('CheckAddPos',sMessage)=False do
              begin
                fmAttention.Label1.Caption:=sMessage;
                prWriteLog('~~AttentionShow;'+sMessage);
                fmAttention.ShowModal;
                Nums.iRet:=InspectSt(Nums.sRet);
              end;

              CheckTotal(iSumTotal);
              prWriteLog('!!AfterCheckTotal;'+IntToStr((Nums.iCheckNum+1))+';'+inttostr(iSumTotal)+';');

              Event_RegEx(26,Nums.iCheckNum,1,0,'','',0,0,iSumTotal/100,iSumTotal/100,0,0);

              while TestStatus('CheckTotal',sMessage)=False do
              begin
                fmAttention.Label1.Caption:=sMessage;
                prWriteLog('~~AttentionShow;'+sMessage);
                fmAttention.ShowModal;
                Nums.iRet:=InspectSt(Nums.sRet);
              end;

              CheckTotal(iSumTotal);
              prWriteLog('!!AfterCheckTotal;'+IntToStr((Nums.iCheckNum+1))+';'+inttostr(iSumTotal)+';');
              while TestStatus('CheckTotal',sMessage)=False do
              begin
                fmAttention.Label1.Caption:=sMessage;
                prWriteLog('~~AttentionShow;'+sMessage);
                fmAttention.ShowModal;
                Nums.iRet:=InspectSt(Nums.sRet);
              end;

              if rSumBn>0 then
              begin
                CheckRasch(1,RoundEx(rSumBn*100),'',iSumIt); //������
              end;
              if rSumN>0 then
              begin
                CheckRasch(0,RoundEx(rSumN*100),'',iSumIt) //���
              end;

              prWriteLog('!!AfterCheckRasch;'+IntToStr((Nums.iCheckNum+1))+';'+FloatToStr(rSumBn)+';'+FloatToStr(rSumN)+';'+INtToStr(iSumIt)+';');
              while TestStatus('CheckRasch',sMessage)=False do
              begin
                fmAttention.Label1.Caption:=sMessage;
                prWriteLog('~~AttentionShow;'+sMessage);
                fmAttention.ShowModal;
                Nums.iRet:=InspectSt(Nums.sRet);
              end;

              prWriteLog('!!CheckClose;'+IntToStr((Nums.iCheckNum+1))+';');

              Event_RegEx(5,Nums.iCheckNum,0,0,'','',0,0,rSum,rSum,0,0); //����� ����

              CheckClose;
              while TestStatus('CheckClose',sMessage)=False do
              begin
                fmAttention.Label1.Caption:=sMessage;
                prWriteLog('~~AttentionShow;'+sMessage);
                fmAttention.ShowModal;
                Nums.iRet:=InspectSt(Nums.sRet);
              end;

            //�������� ������ ����� ������
              iRet:=InspectSt(StrWk);
              iCurCheck:=Nums.iCheckNum+1; //��� ���-�� ������� ���

              if iRet=0 then
              begin
                bChOk:=True;

                fmMainCashRn.Label3.Caption:='������ ���: ��� ��.';
                if CashDate(StrWk) then fmMainCashRn.Label2.Caption:='�������� ����: '+StrWk;
                if GetNums then fmMainCashRn.Label4.Caption:='��� � '+Nums.CheckNum;
            //��� ����������� �������� CommonSet.CashChNum - ����� �������� ��� � ���
                WriteCheckNum;
              end
              else
              begin
                fmMainCashRn.Label3.Caption:='������ ���: ������ ('+IntToStr(iRet)+')';

                while TestStatus('InspectSt',sMessage)=False do
                begin
                  fmAttention.Label1.Caption:=sMessage;
                  prWriteLog('~~AttentionShow;'+sMessage);
                  fmAttention.ShowModal;
                  Nums.iRet:=InspectSt(Nums.sRet);
                end;
              end;
            end;

            if (CommonSet.SpecChar=1)and(CommonSet.CashNum<0) then //������ ������������ �����
            begin
              if pos('fis',CommonSet.SpecBox)=0 then OpenMoneyBox
              else
              begin
                try
                  StrWk:='COM'+IntToStr(CommonSet.CashPort);
                  CommonSet.CashNum:=CommonSet.CashNum*(-1);
                  CashOpen(PChar(StrWk));
                  CashDriver;
                  CashClose;
                  CommonSet.CashNum:=CommonSet.CashNum*(-1);
                except
                end;
              end;

              if Pos('COM',CommonSet.PrePrintPort)>0 then
              begin
                try
                  prDevOpen(CommonSet.PrePrintPort,0);
                  StrP:=CommonSet.PrePrintPort;

                  BufPr.iC:=0;

                  prSetFont(StrP,10,0);
                  PrintStr(StrP,SpecVal.SPH1);
                  PrintStr(StrP,SpecVal.SPH2);
                  PrintStr(StrP,SpecVal.SPH3);
                  PrintStr(StrP,SpecVal.SPH4);
                  PrintStr(StrP,SpecVal.SPH5);

//                PrintStr(StrP,'�������� N: 0718668         N ���: 00012');
                  StrWk:=IntToStr(Nums.ZNum); //����� ���������
                  while Length(StrWk)<5 do StrWk:='0'+StrWk;
                  PrintStr(StrP,'�������� N: '+CommonSet.CashSer+'         N ���: '+StrWk);
                  PrintStr(StrP,FormatDateTime('dd-mm-yyyy                         hh:nn',now));

//                PrintStr(StrP,'������: ������              ��� N: 00012');
                  StrWk:='������: '+Copy(Person.Name,1,19);
                  while Length(StrWk)<28 do StrWk:=StrWk+' ';
                  StrWk:=StrWk+'��� N: ';
                  StrWk1:=IntToStr(CommonSet.CashChNum); StrWk1:=Copy(StrWk1,1,5);
                  while Length(StrWk1)<5 do StrWk1:='0'+StrWk1;
                  StrWk:=StrWk+StrWk1;
                  PrintStr(StrP,StrWk);

                  PrintStr(StrP,'����� 1');
                  PrintStr(StrP,'���������� �����');
                  PrintStr(StrP,prDefFormatStr(40,fts(rSum)+' X 1 ��.',rSum));
                  PrintStr(StrP,prDefFormatStr(40,'�����:',rSum));

                  PrintStr(StrP,'�������                                 ');
                  if rSumBn>0 then PrintStr(StrP,prDefFormatStr(40,'��������� �����:',rSumBn));
                  if rSumN>0 then PrintStr(StrP,prDefFormatStr(40,'��������:',rSumN));

                  CutDocPrSpec;
                  prWrBuf(StrP);

                finally
                  prDevClose(StrP,0);
                end;
              end;
              if Pos('DB',CommonSet.PrePrintPort)>0 then
              begin
                with dmC1 do
                begin
                  quPrint.Active:=False;
                  quPrint.Active:=True;
                  iQ:=GetId('PQH'); //����������� ������� �� 1-�
                  PrintDBStr(iQ,999,'Check',SpecVal.SPH1,10,0);
                  PrintDBStr(iQ,999,'Check',SpecVal.SPH2,10,0);
                  PrintDBStr(iQ,999,'Check',SpecVal.SPH3,10,0);
                  PrintDBStr(iQ,999,'Check',SpecVal.SPH4,10,0);
                  PrintDBStr(iQ,999,'Check',SpecVal.SPH5,10,0);
//                PrintDBStr(iQ,999,'Check','�������� N: 0718668         N ���: 00012');
                  StrWk:=IntToStr(Nums.ZNum); //����� ���������
                  while Length(StrWk)<5 do StrWk:='0'+StrWk;
                  PrintDBStr(iQ,999,'Check','�������� N: '+CommonSet.CashSer+'         N ���: '+StrWk,10,0);
                  PrintDBStr(iQ,999,'Check',FormatDateTime('dd-mm-yyyy                         hh:nn',now),10,0);

//                PrintDBStr(iQ,999,'Check','������: ������              ��� N: 00012');
                  StrWk:='������: '+Copy(Person.Name,1,19);
                  while Length(StrWk)<28 do StrWk:=StrWk+' ';
                  StrWk:=StrWk+'��� N: ';
                  StrWk1:=IntToStr(CommonSet.CashChNum); StrWk1:=Copy(StrWk1,1,5);
                  while Length(StrWk1)<5 do StrWk1:='0'+StrWk1;
                  StrWk:=StrWk+StrWk1;
                  PrintDBStr(iQ,999,'Check',StrWk,10,0);

                  PrintDBStr(iQ,999,'Check','����� 1',10,0);
                  PrintDBStr(iQ,999,'Check','���������� �����',10,0);
                  PrintDBStr(iQ,999,'Check',prDefFormatStr(40,fts(rSum)+' X 1 ��.',rSum),10,0);
                  PrintDBStr(iQ,999,'Check',prDefFormatStr(40,'�����: ',rSum),10,0);

                  PrintDBStr(iQ,999,'Check','�������                                 ',10,0);
                  if rSumBn>0 then PrintDBStr(iQ,999,'Check',prDefFormatStr(40,'��������� �����:',rSumBn),10,0);
                  if rSumN>0 then PrintDBStr(iQ,999,'Check',prDefFormatStr(40,'��������:',rSumN),10,0);
                  prAddPrintQH(999,iQ,FormatDateTime('dd.mm hh:nn:sss',now)+',������� - '+IntToStr(CommonSet.Station)+','+CommonSet.PrePrintPort);

                  PrintNFStr('������: '+Person.Name);
                  PrintNFStr('�������: _____________________');

                  quPrint.Active:=False;
                end;
              end;
              bChOk:=True;
            end;
          end;

          if bChOk then
          begin
  //EXECUTE PROCEDURE PRSAVEPC1 (?PCBAR, ?PCSUM, ?STATION, ?IDPERSON, ?PERSNAME)
            prInpMoney.ParamByName('PCBAR').AsString:=DiscountBar;
            prInpMoney.ParamByName('PCSUM').AsFloat:=rSum;
            prInpMoney.ParamByName('STATION').AsINteger:=CommonSet.Station;
            prInpMoney.ParamByName('IDPERSON').AsINteger:=Person.Id;
            prInpMoney.ParamByName('PERSNAME').AsString:=Person.Name;
            prInpMoney.ParamByName('DATEOP').AsDateTime:=now;

            prInpMoney.ExecProc;
            delay(100);

            //������������ �����
            if (rSumN>0)and(CommonSet.DebetCode>0)and(CommonSet.DebetCheck=1) then
            begin
              idHead:=GetId('TH');

              //���� ���������
              taTabAll.Active:=False;
              taTabAll.ParamByName('TID').AsInteger:=idHead;
              taTabAll.Active:=True;

              trUpdTab.StartTransaction;
              taTabAll.Append;
              taTabAllID.AsInteger:=IdHead;
              taTabAllID_PERSONAL.AsInteger:=Person.Id;
              taTabAllNUMTABLE.AsString:=its(IdHead);
              taTabAllQUESTS.AsInteger:=0;
              taTabAllTABSUM.AsFloat:=rSumN;
              taTabAllBEGTIME.AsDateTime:=now;
              taTabAllENDTIME.AsDateTime:=now;
              taTabAllDISCONT.AsString:='';
              taTabAllOPERTYPE.AsString:='Sale';   //SaleBank
              taTabAllCHECKNUM.AsInteger:=iCurCheck;
              taTabAllSKLAD.AsInteger:=0; //�� ��������� ��� ���
              taTabAllSTATION.AsInteger:=CommonSet.Station*100+CommonSet.Station;
              taTabAllNUMZ.AsInteger:=0;
              taTabAllDELT.AsInteger:=0;
              taTabAllSALET.AsInteger:=1;
              taTabAllID_PERSONALCLOSE.AsInteger:=Person.Id;
              taTabAll.Post;
              trUpdTab.Commit;

              taTabAll.Active:=False;

              //���� ������������ 1-�������
              taSpecAll.Active:=False;
              taSpecAll.ParamByName('TID').AsInteger:=idHead;
              taSpecAll.Active:=True;

              trUpdTab.StartTransaction;
              taSpecAll.Append;
              taSpecAllID_TAB.AsInteger:=IdHead;
              taSpecAllID.AsInteger:=1;
              taSpecAllID_PERSONAL.AsInteger:=Person.Id;
              taSpecAllNUMTABLE.AsString:=its(idHead);
              taSpecAllSIFR.AsInteger:=CommonSet.DebetCode;
              taSpecAllPRICE.AsFloat:=rSumN;
              taSpecAllQUANTITY.AsFloat:=1;
              taSpecAllDISCOUNTPROC.AsFloat:=0;
              taSpecAllDISCOUNTSUM.AsFloat:=0;
              taSpecAllSUMMA.AsFloat:=rSumN;
              taSpecAllISTATUS.AsInteger:=1;
              taSpecAllITYPE.AsInteger:=0;//�����
              taSpecAll.Post;
              trUpdTab.Commit;

              taSpecAll.Active:=False;

              idC:=GetId('CS');
              taCashSailSel.Active:=False;
              taCashSailSel.ParamByName('IDH').AsInteger:=IDC;
              taCashSailSel.Active:=True;

              taCashSailSel.Append;
              taCashSailSelID.AsInteger:=IdC;
              taCashSailSelCASHNUM.AsInteger:=CommonSet.CashNum;
              if CommonSet.CashNum<0 then
              begin
                taCashSailSelZNUM.AsInteger:=CommonSet.CashZ;
                taCashSailSelCHECKNUM.AsInteger:=CommonSet.CashChNum;
              end
              else
              begin
                taCashSailSelZNUM.AsInteger:=Nums.ZNum;
                taCashSailSelCHECKNUM.AsInteger:=Nums.iCheckNum;
              end;
              taCashSailSelTAB_ID.AsInteger:=IdHead;
              taCashSailSelTABSUM.AsFloat:=rSumN;
              taCashSailSelCLIENTSUM.AsFloat:=rSumN;
              taCashSailSelCHDATE.AsDateTime:=now;
              taCashSailSelCASHERID.AsInteger:=Person.Id;
              taCashSailSelWAITERID.AsInteger:=Person.Id;
              taCashSailSelPAYTYPE.AsInteger:=0;
              taCashSailSelPAYID.AsInteger:=0;
              taCashSailSel.Post;

              taCashSailSel.Active:=False;
            end;

//������������ �����
            if (rSumBn>0)and(CommonSet.DebetCode>0)and(CommonSet.DebetCheck=1) then
            begin
              idHead:=GetId('TH');

              //���� ���������
              taTabAll.Active:=False;
              taTabAll.ParamByName('TID').AsInteger:=idHead;
              taTabAll.Active:=True;

              trUpdTab.StartTransaction;
              taTabAll.Append;
              taTabAllID.AsInteger:=IdHead;
              taTabAllID_PERSONAL.AsInteger:=Person.Id;
              taTabAllNUMTABLE.AsString:=its(IdHead);
              taTabAllQUESTS.AsInteger:=0;
              taTabAllTABSUM.AsFloat:=rSumBn;
              taTabAllBEGTIME.AsDateTime:=now;
              taTabAllENDTIME.AsDateTime:=now;
              taTabAllDISCONT.AsString:='';
              taTabAllOPERTYPE.AsString:='SaleBank';   //SaleBank
              taTabAllCHECKNUM.AsInteger:=iCurCheck;
              taTabAllSKLAD.AsInteger:=0; //�� ��������� ��� ���
              taTabAllSTATION.AsInteger:=CommonSet.Station*100+CommonSet.Station;
              taTabAllNUMZ.AsInteger:=0;
              taTabAllDELT.AsInteger:=0;
              taTabAllSALET.AsInteger:=1;
              taTabAllID_PERSONALCLOSE.AsInteger:=Person.Id;
              taTabAll.Post;
              trUpdTab.Commit;

              taTabAll.Active:=False;

              //���� ������������ 1-�������
              taSpecAll.Active:=False;
              taSpecAll.ParamByName('TID').AsInteger:=idHead;
              taSpecAll.Active:=True;

              trUpdTab.StartTransaction;
              taSpecAll.Append;
              taSpecAllID_TAB.AsInteger:=IdHead;
              taSpecAllID.AsInteger:=1;
              taSpecAllID_PERSONAL.AsInteger:=Person.Id;
              taSpecAllNUMTABLE.AsString:=its(idHead);
              taSpecAllSIFR.AsInteger:=CommonSet.DebetCode;
              taSpecAllPRICE.AsFloat:=rSumBn;
              taSpecAllQUANTITY.AsFloat:=1;
              taSpecAllDISCOUNTPROC.AsFloat:=0;
              taSpecAllDISCOUNTSUM.AsFloat:=0;
              taSpecAllSUMMA.AsFloat:=rSumBn;
              taSpecAllISTATUS.AsInteger:=1;
              taSpecAllITYPE.AsInteger:=0;//�����
              taSpecAll.Post;
              trUpdTab.Commit;

              taSpecAll.Active:=False;

              idC:=GetId('CS');
              taCashSailSel.Active:=False;
              taCashSailSel.ParamByName('IDH').AsInteger:=IDC;
              taCashSailSel.Active:=True;

              taCashSailSel.Append;
              taCashSailSelID.AsInteger:=IdC;
              taCashSailSelCASHNUM.AsInteger:=CommonSet.CashNum;
              if CommonSet.CashNum<0 then
              begin
                taCashSailSelZNUM.AsInteger:=CommonSet.CashZ;
                taCashSailSelCHECKNUM.AsInteger:=CommonSet.CashChNum;
              end
              else
              begin
                taCashSailSelZNUM.AsInteger:=Nums.ZNum;
                taCashSailSelCHECKNUM.AsInteger:=Nums.iCheckNum;
              end;
              taCashSailSelTAB_ID.AsInteger:=IdHead;
              taCashSailSelTABSUM.AsFloat:=rSumBn;
              taCashSailSelCLIENTSUM.AsFloat:=rSumBn;
              taCashSailSelCHDATE.AsDateTime:=now;
              taCashSailSelCASHERID.AsInteger:=Person.Id;
              taCashSailSelWAITERID.AsInteger:=Person.Id;
              taCashSailSelPAYTYPE.AsInteger:=1;
              taCashSailSelPAYID.AsInteger:=1;
              taCashSailSel.Post;

              taCashSailSel.Active:=False;
            end;

            prFindPCardBalans(DiscountBar,rBalans,rBalansDay,DLimit,CliName,CliType);
            cxCurrencyEdit1.Value:=rBalans;
            cxCurrencyEdit2.Value:=rBalansDay;
            cxCurrencyEdit3.Value:=DLimit;
            Label5.Caption:=CliName+' ('+CliType+')';
          end else ShowMessage('������ ������������ ����. ��������� �������� � ��������� ��������.');
        end;
      end;
    end;
  end;
end;

procedure TfmPCardsRn.cxCurrencyEdit5Enter(Sender: TObject);
begin
  cxTextEdit1.Tag:=0;
  cxCurrencyEdit5.Tag:=1;
  cxCurrencyEdit5.SelectAll;
  cxCurrencyEdit4.Tag:=0;
  cxTextEdit1.SelectAll;
  cxCurrencyEdit5.SetFocus;
end;

end.
