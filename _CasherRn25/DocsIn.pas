unit DocsIn;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SpeedBar, ExtCtrls, ComCtrls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Placemnt, cxImageComboBox, XPStyleActnCtrls, ActnList, ActnMan, Menus,
  FR_DSet, FR_DBSet, FR_Class, cxContainer, cxTextEdit, cxMemo;

type
  TfmDocsIn = class(TForm)
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    Timer1: TTimer;
    FormPlacement1: TFormPlacement;
    GridDocsIn: TcxGrid;
    ViewDocsIn: TcxGridDBTableView;
    LevelDocsIn: TcxGridLevel;
    ViewDocsInID: TcxGridDBColumn;
    ViewDocsInDATEDOC: TcxGridDBColumn;
    ViewDocsInNUMDOC: TcxGridDBColumn;
    ViewDocsInDATESF: TcxGridDBColumn;
    ViewDocsInNUMSF: TcxGridDBColumn;
    ViewDocsInIDCLI: TcxGridDBColumn;
    ViewDocsInIDSKL: TcxGridDBColumn;
    ViewDocsInSUMIN: TcxGridDBColumn;
    ViewDocsInSUMUCH: TcxGridDBColumn;
    ViewDocsInSUMTAR: TcxGridDBColumn;
    ViewDocsInSUMNDS0: TcxGridDBColumn;
    ViewDocsInSUMNDS1: TcxGridDBColumn;
    ViewDocsInSUMNDS2: TcxGridDBColumn;
    ViewDocsInPROCNAC: TcxGridDBColumn;
    ViewDocsInNAMECL: TcxGridDBColumn;
    ViewDocsInNAMEMH: TcxGridDBColumn;
    ViewDocsInIACTIVE: TcxGridDBColumn;
    amDocsIn: TActionManager;
    acPeriod: TAction;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    SpeedItem6: TSpeedItem;
    acAddDoc1: TAction;
    acEditDoc1: TAction;
    acViewDoc1: TAction;
    acDelDoc1: TAction;
    acOnDoc1: TAction;
    acOffDoc1: TAction;
    SpeedItem7: TSpeedItem;
    SpeedItem8: TSpeedItem;
    acVid: TAction;
    SpeedItem9: TSpeedItem;
    LevelCards: TcxGridLevel;
    ViewCards: TcxGridDBTableView;
    ViewCardsNAME: TcxGridDBColumn;
    ViewCardsNAMESHORT: TcxGridDBColumn;
    ViewCardsIDCARD: TcxGridDBColumn;
    ViewCardsQUANT: TcxGridDBColumn;
    ViewCardsPRICEIN: TcxGridDBColumn;
    ViewCardsSUMIN: TcxGridDBColumn;
    ViewCardsPRICEUCH: TcxGridDBColumn;
    ViewCardsSUMUCH: TcxGridDBColumn;
    ViewCardsIDNDS: TcxGridDBColumn;
    ViewCardsSUMNDS: TcxGridDBColumn;
    ViewCardsDATEDOC: TcxGridDBColumn;
    ViewCardsNUMDOC: TcxGridDBColumn;
    ViewCardsNAMECL: TcxGridDBColumn;
    ViewCardsNAMEMH: TcxGridDBColumn;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    acPrint1: TAction;
    frRepDocsIn: TfrReport;
    frquSpecInSel: TfrDBDataSet;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    acCopy: TAction;
    acInsertD: TAction;
    SpeedItem10: TSpeedItem;
    acExpExcel: TAction;
    Memo1: TcxMemo;
    Excel1: TMenuItem;
    PopupMenu2: TPopupMenu;
    N5: TMenuItem;
    ViewCardsCATEGORY: TcxGridDBColumn;
    acExit: TAction;
    ViewDocsInCOMMENT: TcxGridDBColumn;
    ViewDocsInOPRIZN: TcxGridDBColumn;
    ViewDocsInCREATETYPE: TcxGridDBColumn;
    procedure SpeedItem1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acPeriodExecute(Sender: TObject);
    procedure acAddDoc1Execute(Sender: TObject);
    procedure acEditDoc1Execute(Sender: TObject);
    procedure acViewDoc1Execute(Sender: TObject);
    procedure ViewDocsInDblClick(Sender: TObject);
    procedure acDelDoc1Execute(Sender: TObject);
    procedure acOnDoc1Execute(Sender: TObject);
    procedure acOffDoc1Execute(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure acVidExecute(Sender: TObject);
    procedure SpeedItem1Click0(Sender: TObject);
    procedure acPrint1Execute(Sender: TObject);
    procedure acCopyExecute(Sender: TObject);
    procedure acInsertDExecute(Sender: TObject);
    procedure acExpExcelExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Excel1Click(Sender: TObject);
    procedure ViewDocsInCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure N5Click(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure Memo1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure prButtonSet(bSet:Boolean);

  end;

var
  fmDocsIn: TfmDocsIn;
  bClearDocIn:Boolean = false;

implementation

uses Un1, dmOffice, PeriodUni, AddDoc1, DMOReps, TBuff, MainRnOffice,
  Message;

{$R *.dfm}

procedure TfmDocsIn.prButtonSet(bSet:Boolean);
begin
  SpeedItem1.Enabled:=bSet;
  SpeedItem2.Enabled:=bSet;
  SpeedItem3.Enabled:=bSet;
  SpeedItem4.Enabled:=bSet;
  SpeedItem5.Enabled:=bSet;
  SpeedItem6.Enabled:=bSet;
  SpeedItem7.Enabled:=bSet;
  SpeedItem8.Enabled:=bSet;
  SpeedItem9.Enabled:=bSet;
  SpeedItem10.Enabled:=bSet;
  delay(100);
end;

procedure TfmDocsIn.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmDocsIn.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  Timer1.Enabled:=True;
  GridDocsIn.Align:=AlClient;
  ViewDocsIn.RestoreFromIniFile(CurDir+GridIni);
  ViewCards.RestoreFromIniFile(CurDir+GridIni);
  //���� ����� ������� �������� ������ ���
  with dmO do
  begin
    if taNDS.Active=False then taNDS.Active:=True;
    taNDS.First;
    while not taNDS.Eof do
    begin
      if taNDSID.AsInteger=1 then ViewDocsInSUMNDS0.Caption:=taNDSNAMENDS.AsString;
      if taNDSID.AsInteger=2 then ViewDocsInSUMNDS1.Caption:=taNDSNAMENDS.AsString;
      if taNDSID.AsInteger=3 then ViewDocsInSUMNDS2.Caption:=taNDSNAMENDS.AsString;
      taNDS.Next;
    end;
  end;

  StatusBar1.Color:= UserColor.TTnIn;
  SpeedBar1.Color := UserColor.TTnIn;
end;

procedure TfmDocsIn.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewDocsIn.StoreToIniFile(CurDir+GridIni,False);
  ViewCards.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmDocsIn.acPeriodExecute(Sender: TObject);
begin
//  ������
  fmPeriodUni.DateTimePicker1.Date:=CommonSet.DateFrom;
//  fmPeriodUni.DateTimePicker2.Date:=CommonSet.DateTo-1;
  fmPeriodUni.ShowModal;
  if fmPeriodUni.ModalResult=mrOk then
  begin
    CommonSet.DateFrom:=Trunc(fmPeriodUni.DateTimePicker1.Date);
    CommonSet.DateTo:=Trunc(fmPeriodUni.DateTimePicker2.Date)+1;

    with dmO do
    with dmORep do
    begin
      if LevelDocsIn.Visible then
      begin
        if CommonSet.DateTo>=iMaxDate then fmDocsIn.Caption:='��������� ��������� � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
        else fmDocsIn.Caption:='��������� ��������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);

        ViewDocsIn.BeginUpdate;
        quDocsInSel.Active:=False;
        quDocsInSel.ParamByName('DATEB').AsDate:=CommonSet.DateFrom;
        quDocsInSel.ParamByName('DATEE').AsDate:=CommonSet.DateTo;
        quDocsInSel.ParamByName('IDPERSON').AsInteger:=Person.Id;
        quDocsInSel.Active:=True;
        ViewDocsIn.EndUpdate;
      end else
      begin
        if CommonSet.DateTo>=iMaxDate then fmDocsIn.Caption:='������� �� ������� � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
        else fmDocsIn.Caption:='������� �� ������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);

        ViewCards.BeginUpdate;
        quDocsInCard.Active:=False;
        quDocsInCard.ParamByName('DATEB').AsDate:=CommonSet.DateFrom;
        quDocsInCard.ParamByName('DATEE').AsDate:=CommonSet.DateTo;
        quDocsInCard.Active:=True;
        ViewCards.EndUpdate;
      end;
    end;
  end;
end;

procedure TfmDocsIn.acAddDoc1Execute(Sender: TObject);
//Var IDH:INteger;
//    rSum1,rSum2:Real;
begin
  //�������� ��������

  if not CanDo('prAddDocIn') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;

  if fmAddDoc1.Visible then if MessageDlg('������� �������� ��������?',mtConfirmation, [mbYes, mbNo], 0) <> mrYes then exit;

  //��� ���������
  with dmO do
  begin
    fmAddDoc1.Caption:='���������: ����� ��������.';
    fmAddDoc1.cxTextEdit1.Text:=prGetNum(1,0);
    fmAddDoc1.cxTextEdit1.Tag:=0;
    fmAddDoc1.cxTextEdit1.Properties.ReadOnly:=False;
    fmAddDoc1.cxTextEdit2.Text:='';
    fmAddDoc1.cxTextEdit2.Properties.ReadOnly:=False;
    fmAddDoc1.cxDateEdit1.Date:=Date;
    fmAddDoc1.cxDateEdit1.Properties.ReadOnly:=False;
    fmAddDoc1.cxDateEdit2.Date:=Date;
    fmAddDoc1.cxDateEdit2.Properties.ReadOnly:=False;
    fmAddDoc1.cxCurrencyEdit1.EditValue:=0;
    fmAddDoc1.cxCurrencyEdit2.EditValue:=0;

    fmAddDoc1.cxTextEdit3.Text:='';
    fmAddDoc1.cxTextEdit3.Properties.ReadOnly:=False;

    if taNDS.Active=False then taNDS.Active:=True;
    taNds.First;
    if not taNDS.Eof then fmAddDoc1.Label7.Caption:=taNDSNAMENDS.AsString; vNds[1]:=taNDSPROC.AsFloat; taNds.Next;
    if not taNDS.Eof then fmAddDoc1.Label9.Caption:=taNDSNAMENDS.AsString; vNds[2]:=taNDSPROC.AsFloat; taNds.Next;
    if not taNDS.Eof then fmAddDoc1.Label10.Caption:=taNDSNAMENDS.AsString; vNds[3]:=taNDSPROC.AsFloat;
    taNds.First;
    fmAddDoc1.Label11.Caption:='0.00�.';
    fmAddDoc1.Label13.Caption:='0.00�.';
    fmAddDoc1.Label14.Caption:='0.00�.';

    fmAddDoc1.cxButtonEdit1.Tag:=0;
    fmAddDoc1.cxButtonEdit1.EditValue:=0;
    fmAddDoc1.cxButtonEdit1.Text:='';
    fmAddDoc1.cxButtonEdit1.Properties.ReadOnly:=False;

    if quMHAll.Active=False then
    begin
       quMHAll.ParamByName('IDPERSON').AsInteger:=Person.Id;
       quMHAll.Active:=True;
    end;
    quMHAll.FullRefresh;

    fmAddDoc1.cxLookupComboBox1.EditValue:=0;
    fmAddDoc1.cxLookupComboBox1.Text:='';
    fmAddDoc1.cxLookupComboBox1.Properties.ReadOnly:=False;

    if (CurVal.IdMH<>0)and(quMHAll.Locate('ID',CurVal.IdMH,[])) then
    begin
      fmAddDoc1.cxLookupComboBox1.EditValue:=CurVal.IdMH;
      fmAddDoc1.cxLookupComboBox1.Text:=CurVal.NAMEMH;
    end else
    begin
      quMHAll.First;
      if not quMHAll.Eof then
      begin
        CurVal.IdMH:=quMHAllID.AsInteger;
        CurVal.NAMEMH:=quMHAllNAMEMH.AsString;

        fmAddDoc1.cxLookupComboBox1.EditValue:=CurVal.IdMH;
        fmAddDoc1.cxLookupComboBox1.Text:=CurVal.NAMEMH;
      end else
      begin
        CurVal.IdMH:=0;
        CurVal.NAMEMH:='';
      end;
    end;

    if quMHAll.Locate('ID',CurVal.IdMH,[]) then
    begin
      fmAddDoc1.Label15.Caption:='��. ����: '+quMHAllNAMEPRICE.AsString;
      fmAddDoc1.Label15.Tag:=quMHAllDEFPRICE.AsInteger;
    end else
    begin
      fmAddDoc1.Label15.Caption:='��. ����: ';
      fmAddDoc1.Label15.Tag:=0;
    end;

    fmAddDoc1.cxLabel1.Enabled:=True;
    fmAddDoc1.cxLabel2.Enabled:=True;
    fmAddDoc1.cxLabel3.Enabled:=True;
    fmAddDoc1.cxLabel4.Enabled:=True;
    fmAddDoc1.cxLabel5.Enabled:=True;
    fmAddDoc1.cxLabel6.Enabled:=True;
    fmAddDoc1.N1.Enabled:=True;

    fmAddDoc1.ViewDoc1.OptionsData.Editing:=True;
    fmAddDoc1.ViewDoc1.OptionsData.Deleting:=True;

    fmAddDoc1.cxSpinEdit1.Value:=0;
    fmAddDoc1.cxSpinEdit1.Properties.ReadOnly:=False;

    fmAddDoc1.ViewDoc1iCodeCB.Visible:=False;
    fmAddDoc1.ViewDoc1NameCB.Visible:=False;
    fmAddDoc1.ViewDoc1SMCB.Visible:=False;
    fmAddDoc1.ViewDoc1KBCB.Visible:=False;

    fmAddDoc1.Label18.Caption:='������';
    fmAddDoc1.Label18.Tag:=0;

    fmAddDoc1.cxButton1.Enabled:=True;
//    CloseTa(fmAddDoc1.taSpec);
    CloseTe(fmAddDoc1.taSpec);

    fmAddDoc1.acSaveDoc.Enabled:=True;

    fmAddDoc1.Show;

  end;
end;

procedure TfmDocsIn.acEditDoc1Execute(Sender: TObject);
Var IDH:INteger;
//    rSum1,rSum2:Real;
begin
  //�������������
  if not CanDo('prEditDocIn') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;

  if fmAddDoc1.Visible then if MessageDlg('������� �������� ��������?',mtConfirmation, [mbYes, mbNo], 0) <> mrYes then exit;

  //��� ���������
  with dmO do
  with dmORep do
  begin
    if quDocsInSel.RecordCount>0 then //���� ��� �������������
    begin
      if quDocsInSelIACTIVE.AsInteger=0 then
      begin
        prAllViewOff;

        fmAddDoc1.Caption:='���������: ��������������.';
        fmAddDoc1.cxTextEdit1.Text:=quDocsInSelNUMDOC.AsString;
        fmAddDoc1.cxTextEdit1.Properties.ReadOnly:=False;
        fmAddDoc1.cxTextEdit1.Tag:=quDocsInSelID.AsInteger;

        fmAddDoc1.cxTextEdit2.Text:=quDocsInSelNUMSF.AsString;
        fmAddDoc1.cxTextEdit2.Properties.ReadOnly:=False;

        fmAddDoc1.cxTextEdit3.Text:=quDocsInSelCOMMENT.AsString;
        fmAddDoc1.cxTextEdit3.Properties.ReadOnly:=False;

        fmAddDoc1.cxDateEdit1.Date:=quDocsInSelDATEDOC.AsDateTime;
        fmAddDoc1.cxDateEdit1.Properties.ReadOnly:=False;
        fmAddDoc1.cxDateEdit2.Date:=quDocsInSelDATESF.AsDateTime;
        fmAddDoc1.cxDateEdit2.Properties.ReadOnly:=False;
        fmAddDoc1.cxCurrencyEdit1.EditValue:=quDocsInSelSUMIN.AsFloat;
        fmAddDoc1.cxCurrencyEdit2.EditValue:=quDocsInSelSUMUCH.AsFloat;

        if taNDS.Active=False then taNDS.Active:=True;
        taNds.First;
        if not taNDS.Eof then fmAddDoc1.Label7.Caption:=taNDSNAMENDS.AsString; vNds[1]:=taNDSPROC.AsFloat; taNds.Next;
        if not taNDS.Eof then fmAddDoc1.Label9.Caption:=taNDSNAMENDS.AsString; vNds[2]:=taNDSPROC.AsFloat; taNds.Next;
        if not taNDS.Eof then fmAddDoc1.Label10.Caption:=taNDSNAMENDS.AsString; vNds[3]:=taNDSPROC.AsFloat;
        taNds.First;

        fmAddDoc1.Label11.Caption:=quDocsInSelSUMNDS0.AsString;
        fmAddDoc1.Label13.Caption:=quDocsInSelSUMNDS1.AsString;
        fmAddDoc1.Label14.Caption:=quDocsInSelSUMNDS2.AsString;

        fmAddDoc1.cxButtonEdit1.Tag:=quDocsInSelIDCLI.AsInteger;
        fmAddDoc1.cxButtonEdit1.EditValue:=quDocsInSelIDCLI.AsInteger;
        fmAddDoc1.cxButtonEdit1.Text:=quDocsInSelNAMECL.AsString;
        fmAddDoc1.cxButtonEdit1.Properties.ReadOnly:=False;
        fmAddDoc1.Label4.Tag:=quDocsInSelINDS.AsInteger;

        if quMHAll.Active=False then
        begin
          quMHAll.ParamByName('IDPERSON').AsInteger:=Person.Id;
          quMHAll.Active:=True;
        end;
        quMHAll.FullRefresh;

        fmAddDoc1.cxLookupComboBox1.EditValue:=quDocsInSelIDSKL.AsInteger;
        fmAddDoc1.cxLookupComboBox1.Text:=quDocsInSelNAMEMH.AsString;
        fmAddDoc1.cxLookupComboBox1.Properties.ReadOnly:=False;

        CurVal.IdMH:=quDocsInSelIDSKL.AsInteger;
        CurVal.NAMEMH:=quDocsInSelNAMEMH.AsString;

        if quMHAll.Locate('ID',CurVal.IdMH,[]) then
        begin
          fmAddDoc1.Label15.Caption:='��. ����: '+quMHAllNAMEPRICE.AsString;
          fmAddDoc1.Label15.Tag:=quMHAllDEFPRICE.AsInteger;
        end else
        begin
          fmAddDoc1.Label15.Caption:='��. ����: ';
          fmAddDoc1.Label15.Tag:=0;
        end;

        fmAddDoc1.cxLabel1.Enabled:=True;
        fmAddDoc1.cxLabel2.Enabled:=True;
        fmAddDoc1.cxLabel3.Enabled:=True;
        fmAddDoc1.cxLabel4.Enabled:=True;
        fmAddDoc1.cxLabel5.Enabled:=True;
        fmAddDoc1.cxLabel6.Enabled:=True;
        fmAddDoc1.cxButton1.Enabled:=True;
        fmAddDoc1.N1.Enabled:=True;

        fmAddDoc1.cxSpinEdit1.Value:=quDocsInSelOPRIZN.AsInteger;
        fmAddDoc1.cxSpinEdit1.Properties.ReadOnly:=False;

        if quDocsInSelCREATETYPE.AsInteger=0 then
        begin
          fmAddDoc1.ViewDoc1iCodeCB.Visible:=False;
          fmAddDoc1.ViewDoc1NameCB.Visible:=False;
          fmAddDoc1.ViewDoc1SMCB.Visible:=False;
          fmAddDoc1.ViewDoc1KBCB.Visible:=False;
          fmAddDoc1.ViewDoc1SBARCB.Visible:=False;
          fmAddDoc1.ViewDoc1QUANTCB.Visible:=False;

          fmAddDoc1.Label18.Caption:='������';
          fmAddDoc1.Label18.Tag:=0;
        end else
        begin
          fmAddDoc1.ViewDoc1iCodeCB.Visible:=True;
          fmAddDoc1.ViewDoc1NameCB.Visible:=True;
          fmAddDoc1.ViewDoc1SMCB.Visible:=True;
          fmAddDoc1.ViewDoc1KBCB.Visible:=True;
          fmAddDoc1.ViewDoc1SBARCB.Visible:=True;
          fmAddDoc1.ViewDoc1QUANTCB.Visible:=True;

          fmAddDoc1.Label18.Caption:='����';
          fmAddDoc1.Label18.Tag:=1;
        end;

        fmAddDoc1.ViewDoc1.OptionsData.Editing:=True;
        fmAddDoc1.ViewDoc1.OptionsData.Deleting:=True;

        CloseTe(fmAddDoc1.taSpec);

        fmAddDoc1.acSaveDoc.Enabled:=True;

        IDH:=quDocsInSelID.AsInteger;

        quSpecInSel.Active:=False;
        quSpecInSel.ParamByName('IDHD').AsInteger:=IDH;
        quSpecInSel.Active:=True;

        quSpecInSel.First;
        while not quSpecInSel.Eof do
        begin
          with fmAddDoc1 do
          begin
            taSpec.Append;
            taSpecNum.AsInteger:=quSpecInSelNUM.AsInteger;
            taSpecIdGoods.AsInteger:=quSpecInSelIDCARD.AsInteger;
            taSpecNameG.AsString:=quSpecInSelNAMEC.AsString;
            taSpecIM.AsInteger:=quSpecInSelIDM.AsInteger;
            taSpecSM.AsString:=quSpecInSelSM.AsString;
            taSpecQuant.AsFloat:=quSpecInSelQUANT.AsFloat;
            taSpecPrice1.AsFloat:=quSpecInSelPRICEIN.AsFloat;
            taSpecSum1.AsFloat:=quSpecInSelSUMIN.AsFloat;
            taSpecPrice2.AsFloat:=quSpecInSelPRICEUCH.AsFloat;
            taSpecSum2.AsFloat:=quSpecInSelSUMUCH.AsFloat;
//            taSpecINds.AsInteger:=quSpecInSelINDS.AsInteger;

            taSpecINds.AsInteger:=quSpecInSelIDNDS.AsInteger;

//            taSpecSNds.AsString:=quSpecInSelNAMENDS.AsString;
            taSpecRNds.AsFloat:=quSpecInSelSUMNDS.AsFloat;
            taSpecSumNac.AsFloat:=quSpecInSelSUMUCH.AsFloat-quSpecInSelSUMIN.AsFloat;
            taSpecProcNac.AsFloat:=0;
            if quSpecInSelSUMIN.AsFloat<>0 then
              taSpecProcNac.AsFloat:=RoundEx((quSpecInSelSUMUCH.AsFloat-quSpecInSelSUMIN.AsFloat)/quSpecInSelSUMIN.AsFloat*10000)/100;
            if quSpecInSelKM.AsFloat>0 then taSpecKM.AsFloat:=quSpecInSelKM.AsFloat
            else
            begin
              taSpecKM.AsFloat:=prFindKM(quSpecInSelIDM.AsInteger);
            end;
            taSpecTCard.AsInteger:=quSpecInSelTCARD.AsInteger;
            taSpecCType.AsInteger:=quSpecInSelCATEGORY.AsInteger;

            taSpecPrice0.AsFloat:=quSpecInSelPRICE0.AsFloat;
            taSpecSum0.AsFloat:=quSpecInSelSUM0.AsFloat;
            taSpecNDSProc.AsFloat:=quSpecInSelNDSPROC.AsFloat;

            taSpecOPrizn.AsInteger:=quSpecInSelOPRIZN.AsInteger;

            taSpeciCodeCB.AsInteger:=quSpecInSelICODECB.AsInteger;
            taSpecNameCB.AsString:=quSpecInSelNAMECB.AsString;
            taSpeciMCB.AsInteger:=quSpecInSelIMCB.AsInteger;
            taSpecSMCB.AsString:=quSpecInSelSMCB.AsString;
            taSpecKBCB.AsFloat:=quSpecInSelKBCB.AsFloat;
            taSpecSBARCB.AsString:=quSpecInSelSBARCB.AsString;
            taSpecQUANTCB.AsFloat:=quSpecInSelQUANTCB.AsFloat;

            taSpecGTD.AsString:=quSpecInSelGTD.AsString;

            taSpec.Post;
          end;
          quSpecInSel.Next;
        end;

        prAllViewOn;

        fmAddDoc1.Show;
      end else
      begin
        showmessage('������������� �������������� �������� ������.');
      end;
    end else
    begin
      showmessage('�������� �������� ��� ��������������.');
    end;
  end;
end;

procedure TfmDocsIn.acViewDoc1Execute(Sender: TObject);
Var IDH:INteger;
begin
  //��������
  if not CanDo('prViewDocIn') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  if fmAddDoc1.Visible then if MessageDlg('������� �������� ��������?',mtConfirmation, [mbYes, mbNo], 0) <> mrYes then exit;

  with dmO do
  with dmORep do
  begin
    if quDocsInSel.RecordCount>0 then //���� ��� �������������
    begin
      prAllViewOff;

      fmAddDoc1.Caption:='���������: ��������.';
      fmAddDoc1.cxTextEdit1.Text:=quDocsInSelNUMDOC.AsString;
      fmAddDoc1.cxTextEdit1.Properties.ReadOnly:=True;

      fmAddDoc1.cxTextEdit2.Text:=quDocsInSelNUMSF.AsString;
      fmAddDoc1.cxTextEdit2.Properties.ReadOnly:=True;

      fmAddDoc1.cxTextEdit3.Text:=quDocsInSelCOMMENT.AsString;
      fmAddDoc1.cxTextEdit3.Properties.ReadOnly:=True;

      fmAddDoc1.cxDateEdit1.Date:=quDocsInSelDATEDOC.AsDateTime;
      fmAddDoc1.cxDateEdit1.Properties.ReadOnly:=True;
      fmAddDoc1.cxDateEdit2.Date:=quDocsInSelDATESF.AsDateTime;
      fmAddDoc1.cxDateEdit2.Properties.ReadOnly:=True;
      fmAddDoc1.cxCurrencyEdit1.EditValue:=quDocsInSelSUMIN.AsFloat;
      fmAddDoc1.cxCurrencyEdit2.EditValue:=quDocsInSelSUMUCH.AsFloat;

      if taNDS.Active=False then taNDS.Active:=True;
      taNds.First;
      if not taNDS.Eof then fmAddDoc1.Label7.Caption:=taNDSNAMENDS.AsString; vNds[1]:=taNDSPROC.AsFloat; taNds.Next;
      if not taNDS.Eof then fmAddDoc1.Label9.Caption:=taNDSNAMENDS.AsString; vNds[2]:=taNDSPROC.AsFloat; taNds.Next;
      if not taNDS.Eof then fmAddDoc1.Label10.Caption:=taNDSNAMENDS.AsString; vNds[3]:=taNDSPROC.AsFloat;
      taNds.First;
      fmAddDoc1.Label11.Caption:=quDocsInSelSUMNDS0.AsString;
      fmAddDoc1.Label13.Caption:=quDocsInSelSUMNDS1.AsString;
      fmAddDoc1.Label14.Caption:=quDocsInSelSUMNDS2.AsString;

      fmAddDoc1.cxButtonEdit1.Tag:=quDocsInSelIDCLI.AsInteger;
      fmAddDoc1.cxButtonEdit1.EditValue:=quDocsInSelIDCLI.AsInteger;
      fmAddDoc1.cxButtonEdit1.Text:=quDocsInSelNAMECL.AsString;
      fmAddDoc1.cxButtonEdit1.Properties.ReadOnly:=True;
      fmAddDoc1.Label4.Tag:=quDocsInSelINDS.AsInteger;

      if quMHAll.Active=False then
      begin
         quMHAll.ParamByName('IDPERSON').AsInteger:=Person.Id;
         quMHAll.Active:=True;
      end;

      quMHAll.FullRefresh;

      fmAddDoc1.cxLookupComboBox1.EditValue:=quDocsInSelIDSKL.AsInteger;
      fmAddDoc1.cxLookupComboBox1.Text:=quDocsInSelNAMEMH.AsString;
      fmAddDoc1.cxLookupComboBox1.Properties.ReadOnly:=True;

      CurVal.IdMH:=quDocsInSelIDSKL.AsInteger;
      CurVal.NAMEMH:=quDocsInSelNAMEMH.AsString;

      if quMHAll.Locate('ID',CurVal.IdMH,[]) then
      begin
        fmAddDoc1.Label15.Caption:='��. ����: '+quMHAllNAMEPRICE.AsString;
        fmAddDoc1.Label15.Tag:=quMHAllDEFPRICE.AsInteger;
      end else
      begin
        fmAddDoc1.Label15.Caption:='��. ����: ';
        fmAddDoc1.Label15.Tag:=0;
      end;

      fmAddDoc1.cxLabel1.Enabled:=False;
      fmAddDoc1.cxLabel2.Enabled:=False;
      fmAddDoc1.cxLabel3.Enabled:=False;
      fmAddDoc1.cxLabel4.Enabled:=False;
      fmAddDoc1.cxLabel5.Enabled:=False;
      fmAddDoc1.cxLabel6.Enabled:=False;
      fmAddDoc1.N1.Enabled:=False;

      fmAddDoc1.cxSpinEdit1.Value:=quDocsInSelOPRIZN.AsInteger;
      fmAddDoc1.cxSpinEdit1.Properties.ReadOnly:=True;

      if quDocsInSelCREATETYPE.AsInteger=0 then
      begin
        fmAddDoc1.ViewDoc1iCodeCB.Visible:=False;
        fmAddDoc1.ViewDoc1NameCB.Visible:=False;
        fmAddDoc1.ViewDoc1SMCB.Visible:=False;
        fmAddDoc1.ViewDoc1KBCB.Visible:=False;
        fmAddDoc1.ViewDoc1SBARCB.Visible:=False;
        fmAddDoc1.ViewDoc1QUANTCB.Visible:=False;

        fmAddDoc1.Label18.Caption:='������';
        fmAddDoc1.Label18.Tag:=0;
      end else
      begin
        fmAddDoc1.ViewDoc1iCodeCB.Visible:=True;
        fmAddDoc1.ViewDoc1NameCB.Visible:=True;
        fmAddDoc1.ViewDoc1SMCB.Visible:=True;
        fmAddDoc1.ViewDoc1KBCB.Visible:=True;
        fmAddDoc1.ViewDoc1SBARCB.Visible:=True;
        fmAddDoc1.ViewDoc1QUANTCB.Visible:=True;

        fmAddDoc1.Label18.Caption:='����';
        fmAddDoc1.Label18.Tag:=1;
      end;

      fmAddDoc1.cxButton1.Enabled:=False;

      fmAddDoc1.ViewDoc1.OptionsData.Editing:=False;
      fmAddDoc1.ViewDoc1.OptionsData.Deleting:=False;

      CloseTe(fmAddDoc1.taSpec);

      fmAddDoc1.acSaveDoc.Enabled:=False;

      IDH:=quDocsInSelID.AsInteger;

      quSpecInSel.Active:=False;
      quSpecInSel.ParamByName('IDHD').AsInteger:=IDH;
      quSpecInSel.Active:=True;

      quSpecInSel.First;
      while not quSpecInSel.Eof do
      begin
        with fmAddDoc1 do
        begin
          taSpec.Append;
          taSpecNum.AsInteger:=quSpecInSelNUM.AsInteger;
          taSpecIdGoods.AsInteger:=quSpecInSelIDCARD.AsInteger;
          taSpecNameG.AsString:=quSpecInSelNAMEC.AsString;
          taSpecIM.AsInteger:=quSpecInSelIDM.AsInteger;
          taSpecSM.AsString:=quSpecInSelSM.AsString;
          taSpecQuant.AsFloat:=quSpecInSelQUANT.AsFloat;
          taSpecPrice1.AsFloat:=quSpecInSelPRICEIN.AsFloat;
          taSpecSum1.AsFloat:=quSpecInSelSUMIN.AsFloat;
          taSpecPrice2.AsFloat:=quSpecInSelPRICEUCH.AsFloat;
          taSpecSum2.AsFloat:=quSpecInSelSUMUCH.AsFloat;
//          taSpecINds.AsInteger:=quSpecInSelINDS.AsInteger;
          taSpecINds.AsInteger:=quSpecInSelIDNDS.AsInteger;

//          taSpecSNds.AsString:=quSpecInSelNAMENDS.AsString;
          taSpecRNds.AsFloat:=quSpecInSelSUMNDS.AsFloat;
          taSpecSumNac.AsFloat:=quSpecInSelSUMUCH.AsFloat-quSpecInSelSUMIN.AsFloat;
          taSpecProcNac.AsFloat:=0;
          if quSpecInSelSUMIN.AsFloat<>0 then
            taSpecProcNac.AsFloat:=RoundEx((quSpecInSelSUMUCH.AsFloat-quSpecInSelSUMIN.AsFloat)/quSpecInSelSUMIN.AsFloat*10000)/100;
          if quSpecInSelKM.AsFloat>0 then taSpecKM.AsFloat:=quSpecInSelKM.AsFloat
          else
          begin
            taSpecKM.AsFloat:=prFindKM(quSpecInSelIDM.AsInteger);
          end;
          taSpecTCard.AsInteger:=quSpecInSelTCARD.AsInteger;
          taSpecCType.AsInteger:=quSpecInSelCATEGORY.AsInteger;

          taSpecPrice0.AsFloat:=quSpecInSelPRICE0.AsFloat;
          taSpecSum0.AsFloat:=quSpecInSelSUM0.AsFloat;
          taSpecNDSProc.AsFloat:=quSpecInSelNDSPROC.AsFloat;

          taSpecOPrizn.AsInteger:=quSpecInSelOPRIZN.AsInteger;

          taSpeciCodeCB.AsInteger:=quSpecInSelICODECB.AsInteger;
          taSpecNameCB.AsString:=quSpecInSelNAMECB.AsString;
          taSpeciMCB.AsInteger:=quSpecInSelIMCB.AsInteger;
          taSpecSMCB.AsString:=quSpecInSelSMCB.AsString;
          taSpecKBCB.AsFloat:=quSpecInSelKBCB.AsFloat;
          taSpecSBARCB.AsString:=quSpecInSelSBARCB.AsString;
          taSpecQUANTCB.AsFloat:=quSpecInSelQUANTCB.AsFloat;

          taSpecGTD.AsString:=quSpecInSelGTD.AsString;

          taSpec.Post;
        end;
        quSpecInSel.Next;
      end;

      prAllViewOn;

      fmAddDoc1.Show;
    end else
    begin
      showmessage('�������� �������� ��� ���������.');
    end;
  end;
end;

procedure TfmDocsIn.ViewDocsInDblClick(Sender: TObject);
begin
  //������� �������
  with dmO do
  begin
    if quDocsInSelIACTIVE.AsInteger=0 then acEditDoc1.Execute //��������������
    else acViewDoc1.Execute; //��������
  end;
end;

procedure TfmDocsIn.acDelDoc1Execute(Sender: TObject);
Var IDH:INteger;
begin
  prButtonSet(False);
  //������� ��������
  if not CanDo('prDelDocIn') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmO do
  begin
    if quDocsInSel.RecordCount>0 then //���� ��� �������������
    begin
      if quDocsInSelIACTIVE.AsInteger=0 then
      begin
        if MessageDlg('�� ������������� ������ ������� ��������� �'+quDocsInSelNUMDOC.AsString+' �� '+quDocsInSelNAMECL.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          prLog(1,quDocsInSelID.AsInteger,3,quDocsInSelIDSKL.AsInteger); //��������

          IDH:=quDocsInSelID.AsInteger;
          quDocsInSel.Delete;

          quSpecInSel.Active:=False;
          quSpecInSel.ParamByName('IDHD').AsInteger:=IDH;
          quSpecInSel.Active:=True;

          quSpecInSel.First; //������
          while not quSpecInSel.Eof do quSpecInSel.Delete;
          quSpecInSel.Active:=False;

        end;
      end else
      begin
        showmessage('������� �������������� �������� ������.');
      end;
    end else
    begin
      showmessage('�������� �������� ��� ��������.');
    end;
  end;
  prButtonSet(True);
end;

procedure TfmDocsIn.acOnDoc1Execute(Sender: TObject);
Var IdH:INteger;
    i,iSS:Integer;
    rQRemn:Real;
    iNDS:Integer;
    rProcNDS,rSumIn0,rPriceIn0,rSumNDS,rSum0,rSumIn,rSum2,rSumT:Real;
begin
//������������
  with dmO do
  with dmORep do
  begin
    if quDocsInSel.RecordCount=0 then exit;
    if not CanDo('prOnDocIn') then begin StatusBar1.Panels[0].Text:='��� ����.'; SpeedItem7.Enabled:=True; exit; end;
    if not CanEdit(Trunc(quDocsInSelDATEDOC.AsDateTime),quDocsInSelIDSKL.AsInteger) then begin StatusBar1.Panels[0].Text:='������ ������.'; exit; end;

    //��� ���������
    prButtonSet(False);

    if quDocsInSel.RecordCount>0 then //���� ��� ������������
    begin
      if quDocsInSelIACTIVE.AsInteger=0 then
      begin
        if MessageDlg('������������ �������� �'+quDocsInSelNUMDOC.AsString+' �� '+quDocsInSelNAMECL.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          if prTOFind(Trunc(quDocsInSelDATEDOC.AsDateTime),quDocsInSelIDSKL.AsInteger)=1 then
          begin //�� ����
            if MessageDlg('������� ��� �������� ������ �� �� '+quDocsInSelNAMEMH.AsString+' � '+FormatDateTime('dd.mm.yyyy',quDocsInSelDATEDOC.AsDateTime)+'?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
              prTODel(Trunc(quDocsInSelDATEDOC.AsDateTime),quDocsInSelIDSKL.AsInteger)
            else
            begin
              showmessage('��������� ������� ��������� ����������...');
              SpeedItem7.Enabled:=True;
              exit;
            end;
          end;
          Memo1.Clear;

          prWH('����� ... ���� ������.',Memo1); Delay(10);

         // 1 - ������� ��������� ������ �� ��������� (�� ������ ������)
         // 2 - �������� ����� ������
         // 3 - �������� ��������������
         // 4 - �������� ������

          IDH:=quDocsInSelID.AsInteger;

          prLog(1,IDH,1,quDocsInSelIDSKL.AsInteger); //�������������

         // 1 - ������� ��������� ������ �� ��������� (�� ������ ������) ��� �������� ������ ��������� ��������� ������
          prDelPart.ParamByName('IDDOC').AsInteger:=IDH;
          prDelPart.ParamByName('DTYPE').AsInteger:=1;
          prDelPart.ExecProc;
          Delay(100);

          //���� ��������� ������� ������ ���������

          rSum0:=0;  rSumIn:=0;
          iSS:=prISS(quDocsInSelIDSKL.AsInteger);
          sNDS[1]:=0;sNDS[2]:=0;sNDS[3]:=0;
          rSum2:=0;  rSumT:=0;

          quSpecInSel.Active:=False;
          quSpecInSel.ParamByName('IDHD').AsInteger:=IDH;
          quSpecInSel.Active:=True;

          quSpecInSel.First;
          while not quSpecInSel.Eof do
          begin
            //�������� �������� �������� �� ����. ���� ������� =0 �� ������� ��� ���������� ������
            rQRemn:=prCalcRemn(quSpecInSelIDCARD.AsInteger,Trunc(quDocsInSelDATEDOC.AsDateTime)-1,quDocsInSelIDSKL.AsInteger);
            if rQRemn<=0.001 then //������� ��� ������ �� ������� ������
            begin
              quClosePartIn.ParamByName('IDCARD').AsInteger:=quSpecInSelIDCARD.AsInteger;
              quClosePartIn.ParamByName('IDSKL').AsInteger:=quDocsInSelIDSKL.AsInteger;
              quClosePartIn.ParamByName('IDATE').AsInteger:=Trunc(quDocsInSelDATEDOC.AsDateTime)-1;
              quClosePartIn.ExecQuery;
            end;

            if CommonSet.RecalcNDS=1 then //������������ ���
            begin
              quFindCard.Active:=False;
              quFindCard.ParamByName('IDCARD').AsInteger:=quSpecInSelIDCARD.AsInteger;
              quFindCard.Active:=True;
              if quFindCard.RecordCount>0 then
              begin
                iNDS:=quFindCardINDS.AsInteger;
                prGetNDS(INds,rProcNDS);
                rSumIn0:=rv(quSpecInSelSUMIN.AsFloat*100/(100+rProcNDS));
                rSumNDS:=rSumIn0-quSpecInSelSUMIN.AsFloat;
                rPriceIn0:=rv(quSpecInSelPRICEIN.AsFloat*100/(100+rProcNDS));
                if quSpecInSelQUANT.AsFloat<>0 then rPriceIn0:=rSumIn0/quSpecInSelQUANT.AsFloat;

                quSpecInSel.Edit;
                quSpecInSelPRICE0.AsFloat:=rPriceIn0;
                quSpecInSelSUM0.AsFloat:=rSumIn0;
                quSpecInSelNDSPROC.AsFloat:=rProcNDS;
                quSpecInSelIDNDS.AsInteger:=iNDS;
                quSpecInSelSUMNDS.AsFloat:=rSumNDS;
                quSpecInSel.Post;
              end;
            end;

            sNDS[quSpecInSelIDNDS.AsInteger]:=sNDS[quSpecInSelIDNDS.AsInteger]+quSpecInSelSUMNDS.AsFloat;

            if (quSpecInSelCATEGORY.AsInteger=1)or(quSpecInSelCATEGORY.AsInteger=2)or(quSpecInSelCATEGORY.AsInteger=3) then
            begin
              rSum0:=rSum0+rv(quSpecInSelSUM0.AsFloat);
              rSumIn:=rSumIn+rv(quSpecInSelSUMIN.AsFloat);
              rSum2:=rSum2+rv(quSpecInSelSUMUCH.AsFloat);
            end;
            if (quSpecInSelCATEGORY.AsInteger=4) then
            begin
              rSumT:=rSumT+rv(quSpecInSelSUMIN.AsFloat);
            end;

            quSpecInSel.Next;
          end;
          quSpecInSel.Active:=False;

         // 2 - �������� ����� ������
         // 3 - �������� �������������� � ����� ��������� ��� �����
         //     � ��������� ������� ������

          prAddPartIn.ParamByName('IDDOC').AsInteger:=IDH;
          prAddPartIn.ParamByName('DTYPE').AsInteger:=1;
          i:=quDocsInSelIDSKL.AsInteger;
          prAddPartIn.ParamByName('IDSKL').AsInteger:=i;
          i:=quDocsInSelIDCLI.AsInteger;
          prAddPartIn.ParamByName('IDCLI').AsInteger:=i;
          i:=Trunc(quDocsInSelDATEDOC.AsDateTime);
          prAddPartIn.ParamByName('IDATE').AsInteger:=i;
          prAddPartIn.ParamByName('ISS').AsInteger:=prISS(quDocsInSelIDSKL.AsInteger);
          prAddPartIn.ExecProc;

         // 4 - �������� ������ � ����� �� ������ ������
          quDocsInSel.Edit;
          if iSS<>2 then
          begin
            quDocsInSelSUMIN.AsFloat:=RoundVal(rSumIn);
            quDocsInSelSUMUCH.AsFloat:=RoundVal(rSum2);
            quDocsInSelSUMTAR.AsFloat:=RoundVal(rSumT);
            quDocsInSelSUMNDS0.AsFloat:=RoundVal(sNDS[1]);
            quDocsInSelSUMNDS1.AsFloat:=RoundVal(sNDS[2]);
            quDocsInSelSUMNDS2.AsFloat:=RoundVal(sNDS[3]);
          end else
          begin
            quDocsInSelSUMIN.AsFloat:=RoundVal(rSum0);
            quDocsInSelSUMUCH.AsFloat:=RoundVal(rSum2);
            quDocsInSelSUMTAR.AsFloat:=RoundVal(rSumT);
            quDocsInSelSUMNDS0.AsFloat:=RoundVal(sNDS[1]);
            quDocsInSelSUMNDS1.AsFloat:=RoundVal(sNDS[2]);
            quDocsInSelSUMNDS2.AsFloat:=RoundVal(sNDS[3]);
          end;
          
          quDocsInSelIACTIVE.AsInteger:=1;
          quDocsInSel.Post;
          quDocsInSel.Refresh;

          prWh('������ ��.',Memo1); Delay(10);

        end;
      end;
    end;
  end;
  prButtonSet(True);
end;

procedure TfmDocsIn.acOffDoc1Execute(Sender: TObject);
Var iCountPartOut:Integer;
    bStart:Boolean;
    StrWk:String;
begin
//��������
  with dmO do
  with dmORep do
  begin
    if quDocsInSel.RecordCount=0 then exit;
    if not CanDo('prOffDocIn') then begin StatusBar1.Panels[0].Text:='��� ����.'; SpeedItem8.Enabled:=True; exit; end;
    if not CanEdit(Trunc(quDocsInSelDATEDOC.AsDateTime),quDocsInSelIDSKL.AsInteger) then begin StatusBar1.Panels[0].Text:='������ ������.'; exit; end;

    //��� ���������
    prButtonSet(False);

    if quDocsInSel.RecordCount>0 then //���� ��� ������������
    begin
      if quDocsInSelIACTIVE.AsInteger=1 then
      begin
        if MessageDlg('�������� �������� �'+quDocsInSelNUMDOC.AsString+' �� '+quDocsInSelNAMECL.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          if prTOFind(Trunc(quDocsInSelDATEDOC.AsDateTime),quDocsInSelIDSKL.AsInteger)=1 then
          begin //�� ����
            if MessageDlg('������� ��� �������� ������ �� �� '+quDocsInSelNAMEMH.AsString+' � '+quDocsInSelNUMDOC.AsString+'?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
              prTODel(Trunc(quDocsInSelDATEDOC.AsDateTime),quDocsInSelIDSKL.AsInteger)
            else
            begin
              showmessage('��������� ������� ��������� ����������...');
              SpeedItem8.Enabled:=True;
              exit;
            end;
          end;

         // 1 - ��������� ���� �� �������� �� ������� ���������� �����
         // ���� ������ ��
          prFindPartOut.ParamByName('IDDOC').AsInteger:=quDocsInSelID.AsInteger;
          prFindPartOut.ParamByName('DTYPE').AsInteger:=1;
          prFindPartOut.ExecProc;
          iCountPartOut:=prFindPartOut.ParamByName('RESULT').Value;
          if iCountPartOut>0 then
          begin //���� ��������, ��� �� ��������� ��������� � �������
            if MessageDlg('� ������� ��������� ��������� '+IntToStr(iCountPartOut)+' ��������� ������.('+StrWk+') ����������?.',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
            begin
              //����� ������� - ���������� �������� � ������������ ������� ������


              bStart:=True;
//              showmessage('�� ������� ����������� ������������� � '+FormatDateTime('dd.mm.yyyy',quDocsInSelDATEDOC.AsDateTime)+' �����.');
//              bStart:=False;
            end else
            begin
              bStart:=False;
            end;
          end else bStart:=True;
          if bStart then
          begin
            prWH('����� ... ���� ������.',Memo1); Delay(10);
           // 1 - �������� ��������� ������
           // 2 - ������� ��������� ������ �� ���������
           // 3 - �������� ��������������

            prLog(1,quDocsInSelID.AsInteger,0,quDocsInSelIDSKL.AsInteger); //�����

            prPartInDel.ParamByName('IDDOC').AsInteger:=quDocsInSelID.AsInteger;
            prPartInDel.ParamByName('DTYPE').AsInteger:=1;
            prPartInDel.ParamByName('IDATEINV').AsInteger:=Trunc(quDocsInSelDATEDOC.AsDateTime);
            prPartInDel.ExecProc;

           // 4 - �������� ������
            quDocsInSel.Edit;
            quDocsInSelIACTIVE.AsInteger:=0;
            quDocsInSel.Post;
            quDocsInSel.Refresh;

            prWH('������ ��.',Memo1); Delay(10);

          end;
        end;
      end;
    end;
  end;
  prButtonSet(True);
end;

procedure TfmDocsIn.Timer1Timer(Sender: TObject);
begin
  if bClearDocIn=True then begin StatusBar1.Panels[0].Text:=''; bClearDocIn:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearDocIn:=True;
end;

procedure TfmDocsIn.acVidExecute(Sender: TObject);
begin
  //���
  with dmO do
  with dmORep do
  begin
    if LevelDocsIn.Visible then
    begin
      if CommonSet.DateTo>=iMaxDate then fmDocsIn.Caption:='������� �� ������� � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
      else fmDocsIn.Caption:='������� �� ������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);

      LevelDocsIn.Visible:=False;
      LevelCards.Visible:=True;

      ViewCards.BeginUpdate;
      quDocsInCard.Active:=False;
      quDocsInCard.ParamByName('DATEB').AsDate:=CommonSet.DateFrom;
      quDocsInCard.ParamByName('DATEE').AsDate:=CommonSet.DateTo;
      quDocsInCard.Active:=True;
      ViewCards.EndUpdate;

      SpeedItem3.Visible:=False;
      SpeedItem4.Visible:=False;
      SpeedItem5.Visible:=False;
      SpeedItem6.Visible:=False;
      SpeedItem7.Visible:=False;
      SpeedItem8.Visible:=False;

    end else
    begin
      if CommonSet.DateTo>=iMaxDate then fmDocsIn.Caption:='��������� ��������� � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
      else fmDocsIn.Caption:='��������� ��������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);

      LevelDocsIn.Visible:=True;
      LevelCards.Visible:=False;

      ViewDocsIn.BeginUpdate;
      quDocsInSel.Active:=False;
      quDocsInSel.ParamByName('DATEB').AsDate:=CommonSet.DateFrom;
      quDocsInSel.ParamByName('DATEE').AsDate:=CommonSet.DateTo;
      quDocsInSel.ParamByName('IDPERSON').AsInteger:=Person.Id;
      quDocsInSel.Active:=True;
      ViewDocsIn.EndUpdate;

      SpeedItem3.Visible:=True;
      SpeedItem4.Visible:=True;
      SpeedItem5.Visible:=True;
      SpeedItem6.Visible:=True;
      SpeedItem7.Visible:=True;
      SpeedItem8.Visible:=True;

    end;
  end;
end;

procedure TfmDocsIn.SpeedItem1Click0(Sender: TObject);
begin
  Close;
end;

procedure TfmDocsIn.acPrint1Execute(Sender: TObject);
begin
//������ �������
  if LevelDocsIn.Visible=False then exit;
  with dmO do
  begin
    if quDocsInSel.RecordCount>0 then //���� ��� �������������
    begin
      quSpecInSel.Active:=False;
      quSpecInSel.ParamByName('IDHD').AsInteger:=quDocsInSelID.AsInteger;
      quSpecInSel.Active:=True;

      frRepDocsIn.LoadFromFile(CurDir + 'DocInReestr.frf');

      frVariables.Variable['CliName']:=quDocsInSelNAMECL.AsString;
      frVariables.Variable['DocNum']:=quDocsInSelNUMDOC.AsString;
      frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',quDocsInSelDATEDOC.AsDateTime);
      frVariables.Variable['DocStore']:=quDocsInSelNAMEMH.AsString;
      frVariables.Variable['Depart']:=CommonSet.DepartName;

      frRepDocsIn.ReportName:='������ �� ��������� ���������.';
      frRepDocsIn.PrepareReport;
      frRepDocsIn.ShowPreparedReport;

      quSpecInSel.Active:=False;
    end else
    begin
      showmessage('�������� �������� ��� ������.');
    end;
  end;
end;

procedure TfmDocsIn.acCopyExecute(Sender: TObject);
var Par:Variant;
begin
  //����������
  with dmO do
  with dmORep do
  begin
    if quDocsInSel.RecordCount=0 then exit;

    taHeadDoc.Active:=False;
    taHeadDoc.FileName:=CurDir+'HeadDoc.cds';
    if FileExists(CurDir+'HeadDoc.cds') then taHeadDoc.Active:=True
    else taHeadDoc.CreateDataSet;

    taSpecDoc.Active:=False;
    taSpecDoc.FileName:=CurDir+'SpecDoc.cds';
    if FileExists(CurDir+'SpecDoc.cds') then taSpecDoc.Active:=True
    else taSpecDoc.CreateDataSet;

    par := VarArrayCreate([0,1], varInteger);
    par[0]:=1;
    par[1]:=quDocsInSelID.AsInteger;
    if taHeadDoc.Locate('IType;Id',par,[])=False then
    begin
      taHeadDoc.Append;
      taHeadDocIType.AsInteger:=1;
      taHeadDocId.AsInteger:=quDocsInSelID.AsInteger;
      taHeadDocDateDoc.AsInteger:=Trunc(quDocsInSelDATEDOC.AsDateTime);
      taHeadDocNumDoc.AsString:=quDocsInSelNUMDOC.AsString;
      taHeadDocIdCli.AsInteger:=quDocsInSelIDCLI.AsInteger;
      taHeadDocNameCli.AsString:=Copy(quDocsInSelNAMECL.AsString,1,70);
      taHeadDocIdSkl.AsInteger:=quDocsInSelIDSKL.AsInteger;
      taHeadDocNameSkl.AsString:=Copy(quDocsInSelNAMEMH.AsString,1,70);
      taHeadDocSumIN.AsFloat:=quDocsInSelSUMIN.AsFloat;
      taHeadDocSumUch.AsFloat:=quDocsInSelSUMUCH.AsFloat;
      taHeadDoc.Post;

      quSpecInSel.Active:=False;
      quSpecInSel.ParamByName('IDHD').AsInteger:=quDocsInSelID.AsInteger;
      quSpecInSel.Active:=True;

      quSpecInSel.First;
      while not quSpecInSel.Eof do
      begin
        taSpecDoc.Append;
        taSpecDocIType.AsInteger:=1;
        taSpecDocIdHead.AsInteger:=quDocsInSelID.AsInteger;
        taSpecDocNum.AsInteger:=quSpecInSelNUM.AsInteger;
        taSpecDocIdCard.AsInteger:=quSpecInSelIDCARD.AsInteger;
        taSpecDocQuant.AsFloat:=quSpecInSelQUANT.AsFloat;
        taSpecDocPriceIn.AsFloat:=quSpecInSelPRICEIN.AsFloat;
        taSpecDocSumIn.AsFloat:=quSpecInSelSUMIN.AsFloat;
        taSpecDocPriceUch.AsFloat:=quSpecInSelPRICEUCH.AsFloat;
        taSpecDocSumUch.AsFloat:=quSpecInSelSUMUCH.AsFloat;
        taSpecDocIdNds.AsInteger:=quSpecInSelINDS.AsInteger;
        taSpecDocSumNds.AsFloat:=quSpecInSelSUMNDS.AsFloat;
        taSpecDocNameC.AsString:=Copy(quSpecInSelNAMEC.AsString,1,30);
        taSpecDocSm.AsString:=quSpecInSelSM.AsString;
        taSpecDocIdM.AsInteger:=quSpecInSelIDM.AsInteger;
        taSpecDocKm.AsFloat:=prFindKM(quSpecInSelIDM.AsInteger);
        taSpecDocPriceUch1.AsFloat:=quSpecInSelPRICEUCH.AsFloat;
        taSpecDocSumUch1.AsFloat:=quSpecInSelSUMUCH.AsFloat;
        taSpecDoc.Post;

        quSpecInSel.Next;
      end;
    end else
    begin
      showmessage('�������� ��� ���� � ������.');
    end;
    taHeadDoc.Active:=False;
    taSpecDoc.Active:=False;
  end;
end;

procedure TfmDocsIn.acInsertDExecute(Sender: TObject);
Var  iTCard,iCType:INteger;
     rNDS:Real;
begin
  // ��������
  with dmO do
  with dmORep do
  begin
    taHeadDoc.Active:=False;
    taHeadDoc.FileName:=CurDir+'HeadDoc.cds';
    if FileExists(CurDir+'HeadDoc.cds') then taHeadDoc.Active:=True
    else taHeadDoc.CreateDataSet;

    taSpecDoc.Active:=False;
    taSpecDoc.FileName:=CurDir+'SpecDoc.cds';
    if FileExists(CurDir+'SpecDoc.cds') then taSpecDoc.Active:=True
    else taSpecDoc.CreateDataSet;

    fmTBuff:=TfmTBuff.Create(Application);

    fmTBuff.LevelTH.Visible:=False;
    fmTBuff.LevelTS.Visible:=False;
    fmTBuff.LevelD.Visible:=True;
    fmTBuff.LevelDSpec.Visible:=True;

    fmTBuff.ShowModal;
    if fmTBuff.ModalResult=mrOk then
    begin //���������
      fmTBuff.Release;
      if taHeadDoc.RecordCount>0 then
      begin
        if CanDo('prAddDocIn') then
        begin
          prAllViewOff;

          fmAddDoc1.Caption:='���������: ����� ��������.';
          fmAddDoc1.cxTextEdit1.Text:=prGetNum(1,0);
          fmAddDoc1.cxTextEdit1.Tag:=0;
          fmAddDoc1.cxTextEdit1.Properties.ReadOnly:=False;
          fmAddDoc1.cxTextEdit2.Text:='';
          fmAddDoc1.cxTextEdit2.Properties.ReadOnly:=False;
          fmAddDoc1.cxDateEdit1.Date:=Date;
          fmAddDoc1.cxDateEdit1.Properties.ReadOnly:=False;
          fmAddDoc1.cxDateEdit2.Date:=Date;
          fmAddDoc1.cxDateEdit2.Properties.ReadOnly:=False;
          fmAddDoc1.cxCurrencyEdit1.EditValue:=0;
          fmAddDoc1.cxCurrencyEdit2.EditValue:=0;

          if taHeadDocIType.AsInteger=1 then
          begin
            fmAddDoc1.cxButtonEdit1.Tag:=taHeadDocIdCli.AsInteger;
            fmAddDoc1.cxButtonEdit1.EditValue:=taHeadDocIdCli.AsInteger;
            fmAddDoc1.cxButtonEdit1.Text:=taHeadDocNameCli.AsString;
          end else
          begin
            fmAddDoc1.cxButtonEdit1.Tag:=0;
            fmAddDoc1.cxButtonEdit1.EditValue:=0;
            fmAddDoc1.cxButtonEdit1.Text:='';
          end;

          fmAddDoc1.cxButtonEdit1.Properties.ReadOnly:=False;

          fmAddDoc1.cxLookupComboBox1.EditValue:=taHeadDocIdSkl.AsInteger;
          fmAddDoc1.cxLookupComboBox1.Text:=taHeadDocNameSkl.AsString;
          fmAddDoc1.cxLookupComboBox1.Properties.ReadOnly:=False;

          if quMHAll.Active=False then
          begin
            quMHAll.ParamByName('IDPERSON').AsInteger:=Person.Id;
            quMHAll.Active:=True;
          end;
          quMHAll.FullRefresh;

          if quMHAll.Locate('ID',CurVal.IdMH,[]) then
          begin
            fmAddDoc1.Label15.Caption:='��. ����: '+quMHAllNAMEPRICE.AsString;
            fmAddDoc1.Label15.Tag:=quMHAllDEFPRICE.AsInteger;
          end else
          begin
            fmAddDoc1.Label15.Caption:='��. ����: ';
            fmAddDoc1.Label15.Tag:=0;
          end;

          fmAddDoc1.cxLabel1.Enabled:=True;
          fmAddDoc1.cxLabel2.Enabled:=True;
          fmAddDoc1.cxLabel3.Enabled:=True;
          fmAddDoc1.cxLabel4.Enabled:=True;
          fmAddDoc1.cxLabel5.Enabled:=True;
          fmAddDoc1.cxLabel6.Enabled:=True;
          fmAddDoc1.N1.Enabled:=True;

          fmAddDoc1.ViewDoc1.OptionsData.Editing:=True;
          fmAddDoc1.ViewDoc1.OptionsData.Deleting:=True;

          fmAddDoc1.cxButton1.Enabled:=True;

          CloseTe(fmAddDoc1.taSpec);

          fmAddDoc1.acSaveDoc.Enabled:=True;

          taSpecDoc.First;
          while not taSpecDoc.Eof do
          begin
            if (taSpecDocIType.AsInteger=taHeadDocIType.AsInteger)and(taSpecDocIdHead.AsInteger=taHeadDocId.AsInteger) then
            begin
              with fmAddDoc1 do
              begin
                iTCard:=0;
                iCType:=1;

                quFCard.Active:=False;
                quFCard.SelectSQL.Clear;
                quFCard.SelectSQL.Add('SELECT ID,NAME,IMESSURE,PARENT,TCARD,INDS,LASTPRICEOUT,CATEGORY');
                quFCard.SelectSQL.Add('FROM OF_CARDS');
                quFCard.SelectSQL.Add('where ID='+IntToStr(taSpecDocIdCard.AsInteger));
                quFCard.SelectSQL.Add('and IACTIVE>0');
                quFCard.SelectSQL.Add('and PARENT in (SELECT ID_CLASSIF FROM RCLASSIF WHERE RIGHTS=0 AND ID_PERSONAL='+its(Person.Id)+')');
                quFCard.Active:=True;

                if quFCard.RecordCount=1 then
                begin
                  iTCard:=quFCardTCARD.AsInteger;
                  iCType:=quFCardCATEGORY.AsInteger;
                end;


                Case taSpecDocIdNds.AsInteger of
                1: rNDS:=0;
                2: rNDS:=10;
                3: rNDS:=18;
                else rNDS:=0;
                end;

                if fmAddDoc1.Label4.Tag=0 then rNDS:=0;


                taSpec.Append;
                taSpecNum.AsInteger:=taSpecDocNum.AsInteger;
                taSpecIdGoods.AsInteger:=taSpecDocIdCard.AsInteger;
                taSpecNameG.AsString:=taSpecDocNameC.AsString;
                taSpecIM.AsInteger:=taSpecDocIdM.AsInteger;
                taSpecSM.AsString:=taSpecDocSm.AsString;
                taSpecQuant.AsFloat:=RoundEx(taSpecDocQuant.AsFloat*1000)/1000;
                taSpecPrice1.AsFloat:=taSpecDocPriceIn.AsFloat;
                taSpecSum1.AsFloat:=taSpecDocSumIn.AsFloat;
                taSpecPrice2.AsFloat:=taSpecDocPriceUch.AsFloat;
                taSpecSum2.AsFloat:=taSpecDocSumUch.AsFloat;
                taSpecINds.AsInteger:=taSpecDocIdNds.AsInteger;
                taSpecSNds.AsString:='���';
                taSpecRNds.AsFloat:=taSpecDocSumNds.AsFloat;
                taSpecSumNac.AsFloat:=taSpecDocSumUch.AsFloat-taSpecDocSumIn.AsFloat;
                taSpecProcNac.AsFloat:=0;
                if taSpecDocSumIn.AsFloat<>0 then
                taSpecProcNac.AsFloat:=RoundEx((taSpecDocSumUch.AsFloat-taSpecDocSumIn.AsFloat)/taSpecDocSumIn.AsFloat*10000)/100;

                taSpecTCard.AsInteger:=iTCard;
                taSpecCType.AsInteger:=iCType;

                taSpecNDSProc.AsFloat:=rNDS;
                try
                  taSpecPrice0.AsFloat:=taSpecDocPriceIn.AsFloat*100/(taSpecDocPriceIn.AsFloat+rNDS);
                  taSpecSum0.AsFloat:=taSpecDocSumIn.AsFloat-taSpecDocSumNds.AsFloat;
                except
                end;

                taSpec.Post;
              end;
            end;
            taSpecDoc.Next;
          end;

          taHeadDoc.Active:=False;
          taSpecDoc.Active:=False;

          prAllViewOn;

     //     fmAddDoc1.ShowModal;
          fmAddDoc1.Show;
        end else showmessage('��� ����.');
      end;
    end else
    begin
      fmTBuff.Release;
      taHeadDoc.Active:=False;
      taSpecDoc.Active:=False;
    end;  
  end;
end;

procedure TfmDocsIn.acExpExcelExecute(Sender: TObject);
begin
  //������� � ������
  if LevelDocsIn.Visible then
  begin
    prNExportExel5(ViewDocsIn);
  end;
  if LevelCards.Visible then
  begin
    prNExportExel5(ViewCards);
  end;
end;

procedure TfmDocsIn.FormShow(Sender: TObject);
begin
  Memo1.Clear;
end;

procedure TfmDocsIn.Excel1Click(Sender: TObject);
begin
  prNExportExel5(ViewDocsIn);
end;

procedure TfmDocsIn.ViewDocsInCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
Var i:Integer;
    sA:String;
begin
  sA:=' ';
  for i:=0 to ViewDocsIn.ColumnCount-1 do
  begin
    if ViewDocsIn.Columns[i].Name='ViewDocsInIACTIVE' then
    begin
      sA:=AViewInfo.GridRecord.DisplayTexts[i];
      break;
    end;
  end;

//  if pos('�����',sA)=0  then  ACanvas.Canvas.Brush.Color := $00ECD9FF;
  if pos('�����',sA)=0  then  ACanvas.Canvas.Brush.Color := $00CACAFF;
end;

procedure TfmDocsIn.N5Click(Sender: TObject);
begin
  dmO.ColorDialog1.Color:=SpeedBar1.Color;
  if dmO.ColorDialog1.Execute then
  begin
    SpeedBar1.Color := dmO.ColorDialog1.Color;
    StatusBar1.Color:= dmO.ColorDialog1.Color;
    UserColor.TTnIn := dmO.ColorDialog1.Color;
    WriteColor;
  end;
end;

procedure TfmDocsIn.acExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmDocsIn.Memo1DblClick(Sender: TObject);
begin
  fmMessage:=tfmMessage.Create(Application);
  fmMessage.Memo1.Lines:=Memo1.Lines;
  fmMessage.ShowModal;
  fmMessage.Release;
end;

end.
