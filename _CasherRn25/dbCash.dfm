object dmCash: TdmCash
  OldCreateOrder = False
  Left = 1174
  Top = 153
  Height = 361
  Width = 529
  object CasherRnDb: TpFIBDatabase
    DBName = '192.168.3.176:D:\_SoftUr\DB\CASHERRN.GDB'
    DBParams.Strings = (
      'user_name=SYSDBA'
      'lc_ctype=WIN1251'
      'password=masterkey'
      'sql_role_name=SYSDBA')
    DefaultTransaction = trSelM
    DefaultUpdateTransaction = trUpdM
    SQLDialect = 3
    Timeout = 0
    SynchronizeTime = False
    DesignDBOptions = []
    AliasName = 'CasherRnDb'
    WaitForRestoreConnect = 20
    Left = 28
    Top = 16
  end
  object prSetStream: TpFIBStoredProc
    Transaction = trUpdM
    Database = CasherRnDb
    SQL.Strings = (
      'EXECUTE PROCEDURE PR_SETSTREAM (?DATEB, ?DATEE)')
    StoredProcName = 'PR_SETSTREAM'
    Left = 108
    Top = 12
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object trSelM: TpFIBTransaction
    DefaultDatabase = CasherRnDb
    TimeoutAction = TARollback
    Left = 16
    Top = 72
  end
  object trUpdM: TpFIBTransaction
    DefaultDatabase = CasherRnDb
    TimeoutAction = TARollback
    Left = 20
    Top = 124
  end
  object quSelMH: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT sts.STORE'
      'FROM SPEC_ALL SP'
      'left join TABLES_ALL T on T.ID=SP.ID_TAB'
      'left join MENU M on M.SIFR=SP.SIFR'
      
        'left join streamtostore sts on (sts.STREAM=M.STREAM and sts.STAT' +
        'ION=(T.STATION-T.STATION/100*100))'
      'GROUP by sts.STORE'
      'ORDER by sts.STORE')
    Transaction = trSelM
    Database = CasherRnDb
    UpdateTransaction = trUpdM
    Left = 196
    Top = 12
    poAskRecordCount = True
    object quSelMHSTORE: TFIBIntegerField
      FieldName = 'STORE'
    end
  end
  object quSpecAll: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT T.OPERTYPE,T.DISCONT1,pc.CLINAME,SP.SIFR,SP.ITYPE,'
      'M.NAME,M.CODE,M.CONSUMMA,'
      
        'MO.NAME,MO.CODEB,MO.KB,T.SALET,M.CNTPRICE,cas1.NAMECS as TCAS,ca' +
        's2.NAMECS as MCAS,'
      
        'SUM(SP.QUANTITY) as QSUM,SUM(SP.DISCOUNTSUM) as DSUM,SUM(SP.SUMM' +
        'A) as RSUM'
      'FROM SPEC_ALL SP'
      'left join TABLES_ALL T on T.ID=SP.ID_TAB'
      'left join MENU M on M.SIFR=SP.SIFR'
      
        'left join streamtostore sts on (sts.STREAM=SP.STREAM and sts.STA' +
        'TION=(T.STATION-T.STATION/100*100))'
      'left join platcard pc on pc.BARCODE=T.DISCONT1'
      'left join MODIFY mo on mo.SIFR=SP.SIFR'
      'left join CATEGSALE cas1 on cas1.ID=T.SALET'
      'left join CATEGSALE cas2 on cas2.ID=M.CNTPRICE'
      ''
      'where T.ENDTIME>=:DATEB'
      'and T.ENDTIME<:DATEE'
      'and T.SKLAD=0'
      'and sts.STORE=:IDSKL'
      ''
      'GROUP BY T.OPERTYPE,T.DISCONT1,pc.CLINAME,SP.SIFR,SP.ITYPE,'
      
        'M.NAME,M.CODE,M.CONSUMMA,MO.NAME,MO.CODEB,MO.KB,T.SALET,M.CNTPRI' +
        'CE,cas1.NAMECS,cas2.NAMECS'
      ''
      'ORDER BY T.OPERTYPE,T.DISCONT1,M.NAME')
    Transaction = trSelM
    Database = CasherRnDb
    UpdateTransaction = trUpdM
    Left = 200
    Top = 64
    object quSpecAllOPERTYPE: TFIBStringField
      FieldName = 'OPERTYPE'
      Size = 10
      EmptyStrToNull = True
    end
    object quSpecAllSIFR: TFIBIntegerField
      FieldName = 'SIFR'
    end
    object quSpecAllNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 100
      EmptyStrToNull = True
    end
    object quSpecAllCODE: TFIBStringField
      FieldName = 'CODE'
      Size = 10
      EmptyStrToNull = True
    end
    object quSpecAllCONSUMMA: TFIBFloatField
      FieldName = 'CONSUMMA'
    end
    object quSpecAllQSUM: TFIBFloatField
      FieldName = 'QSUM'
    end
    object quSpecAllDSUM: TFIBFloatField
      FieldName = 'DSUM'
    end
    object quSpecAllRSUM: TFIBFloatField
      FieldName = 'RSUM'
    end
    object quSpecAllDISCONT1: TFIBStringField
      FieldName = 'DISCONT1'
      Size = 30
      EmptyStrToNull = True
    end
    object quSpecAllCLINAME: TFIBStringField
      FieldName = 'CLINAME'
      Size = 50
      EmptyStrToNull = True
    end
    object quSpecAllITYPE: TFIBSmallIntField
      FieldName = 'ITYPE'
    end
    object quSpecAllNAME1: TFIBStringField
      FieldName = 'NAME1'
      Size = 50
      EmptyStrToNull = True
    end
    object quSpecAllCODEB: TFIBIntegerField
      FieldName = 'CODEB'
    end
    object quSpecAllKB: TFIBFloatField
      FieldName = 'KB'
    end
    object quSpecAllSALET: TFIBSmallIntField
      FieldName = 'SALET'
    end
    object quSpecAllCNTPRICE: TFIBSmallIntField
      FieldName = 'CNTPRICE'
    end
    object quSpecAllTCAS: TFIBStringField
      FieldName = 'TCAS'
      EmptyStrToNull = True
    end
    object quSpecAllMCAS: TFIBStringField
      FieldName = 'MCAS'
      EmptyStrToNull = True
    end
  end
end
