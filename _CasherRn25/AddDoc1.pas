unit AddDoc1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, cxGraphics, cxCurrencyEdit, cxDropDownEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxButtonEdit,
  cxMaskEdit, cxCalendar, cxControls, cxContainer, cxEdit, cxTextEdit,
  StdCtrls, ExtCtrls, Menus, cxLookAndFeelPainters, cxButtons, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, DB, cxDBData, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxLabel, dxfLabel, Placemnt, FIBDataSet,
  pFIBDataSet, DBClient, FIBQuery, pFIBQuery, pFIBStoredProc,
  XPStyleActnCtrls, ActnList, ActnMan, dxmdaset, cxImageComboBox,
  cxSpinEdit;

type
  TfmAddDoc1 = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Label1: TLabel;
    cxTextEdit1: TcxTextEdit;
    cxDateEdit1: TcxDateEdit;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    cxTextEdit2: TcxTextEdit;
    cxDateEdit2: TcxDateEdit;
    cxButtonEdit1: TcxButtonEdit;
    cxLookupComboBox1: TcxLookupComboBox;
    cxCurrencyEdit1: TcxCurrencyEdit;
    cxCurrencyEdit2: TcxCurrencyEdit;
    Label13: TLabel;
    Label14: TLabel;
    Panel2: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Panel3: TPanel;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    cxLabel3: TcxLabel;
    Label15: TLabel;
    FormPlacement1: TFormPlacement;
    taSpec2: TClientDataSet;
    dsSpec: TDataSource;
    taSpec2Num: TIntegerField;
    taSpec2IdGoods: TIntegerField;
    taSpec2NameG: TStringField;
    taSpec2IM: TIntegerField;
    taSpec2SM: TStringField;
    taSpec2Quant: TFloatField;
    taSpec2Price1: TCurrencyField;
    taSpec2Sum1: TCurrencyField;
    taSpec2Price2: TCurrencyField;
    taSpec2Sum2: TCurrencyField;
    taSpec2INds: TIntegerField;
    taSpec2SNds: TStringField;
    taSpec2RNds: TCurrencyField;
    taSpec2SumNac: TCurrencyField;
    taSpec2ProcNac: TFloatField;
    prCalcPrice: TpFIBStoredProc;
    cxLabel4: TcxLabel;
    cxLabel5: TcxLabel;
    cxLabel6: TcxLabel;
    taSpec2Km: TFloatField;
    amDocIn: TActionManager;
    acAddPos: TAction;
    acDelPos: TAction;
    acAddList: TAction;
    acDelAll: TAction;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    acSaveDoc: TAction;
    acExitDoc: TAction;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    GridDoc1: TcxGrid;
    ViewDoc1: TcxGridDBTableView;
    ViewDoc1Num: TcxGridDBColumn;
    ViewDoc1IdGoods: TcxGridDBColumn;
    ViewDoc1NameG: TcxGridDBColumn;
    ViewDoc1IM: TcxGridDBColumn;
    ViewDoc1SM: TcxGridDBColumn;
    ViewDoc1Quant: TcxGridDBColumn;
    ViewDoc1Price1: TcxGridDBColumn;
    ViewDoc1Sum1: TcxGridDBColumn;
    ViewDoc1Price2: TcxGridDBColumn;
    ViewDoc1Sum2: TcxGridDBColumn;
    ViewDoc1RNds: TcxGridDBColumn;
    ViewDoc1SumNac: TcxGridDBColumn;
    ViewDoc1ProcNac: TcxGridDBColumn;
    LevelDoc1: TcxGridLevel;
    N2: TMenuItem;
    Excel1: TMenuItem;
    taSpec2PricePP: TFloatField;
    ViewDoc1PricePP: TcxGridDBColumn;
    mePrice: TdxMemData;
    mePriceNum: TIntegerField;
    mePriceIdGoods: TIntegerField;
    mePriceNameG: TStringField;
    mePriceSM: TStringField;
    mePriceQuant: TFloatField;
    mePricePrice1: TFloatField;
    mePricePricePP: TFloatField;
    mePriceDif: TFloatField;
    dsmePrice: TDataSource;
    acMovePos: TAction;
    N3: TMenuItem;
    taSpec2TCard: TIntegerField;
    ViewDoc1TCard: TcxGridDBColumn;
    taSpec2CType: TSmallintField;
    ViewDoc1CType: TcxGridDBColumn;
    taSpec2QuantRemn: TFloatField;
    ViewDoc1QuantRemn: TcxGridDBColumn;
    taSpec: TdxMemData;
    taSpecNum: TIntegerField;
    taSpecIdGoods: TIntegerField;
    taSpecNameG: TStringField;
    taSpecIM: TIntegerField;
    taSpecSM: TStringField;
    taSpecQuant: TFloatField;
    taSpecPrice1: TFloatField;
    taSpecSum1: TFloatField;
    taSpecPrice2: TFloatField;
    taSpecSum2: TFloatField;
    taSpecINds: TSmallintField;
    taSpecSNds: TStringField;
    taSpecRNds: TFloatField;
    taSpecSumNac: TFloatField;
    taSpecProcNac: TFloatField;
    taSpecKm: TFloatField;
    taSpecPricePP: TFloatField;
    taSpecTCard: TIntegerField;
    taSpecCType: TSmallintField;
    taSpecQuantRemn: TFloatField;
    taSpecPrice0: TFloatField;
    ViewDoc1Price0: TcxGridDBColumn;
    ViewDoc1NDSProc: TcxGridDBColumn;
    taSpecNDSProc: TFloatField;
    taSpecSum0: TFloatField;
    ViewDoc1Sum0: TcxGridDBColumn;
    ViewDoc1INds: TcxGridDBColumn;
    Label16: TLabel;
    cxTextEdit3: TcxTextEdit;
    taSpecOPrizn: TIntegerField;
    ViewDoc1OPrizn: TcxGridDBColumn;
    Label17: TLabel;
    cxSpinEdit1: TcxSpinEdit;
    Label18: TLabel;
    taSpeciCodeCB: TIntegerField;
    taSpecNameCB: TStringField;
    taSpeciMCB: TSmallintField;
    taSpecSMCB: TStringField;
    taSpecKBCB: TFloatField;
    ViewDoc1iCodeCB: TcxGridDBColumn;
    ViewDoc1NameCB: TcxGridDBColumn;
    ViewDoc1SMCB: TcxGridDBColumn;
    ViewDoc1KBCB: TcxGridDBColumn;
    taSpecSBARCB: TStringField;
    taSpecQUANTCB: TFloatField;
    ViewDoc1SBARCB: TcxGridDBColumn;
    ViewDoc1QUANTCB: TcxGridDBColumn;
    acSaveCodeCB: TAction;
    N4: TMenuItem;
    acSelCodeCB: TAction;
    N5: TMenuItem;
    taSpecGTD: TStringField;
    ViewDoc1GTD: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure cxLookupComboBox1PropertiesChange(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxLabel1Click(Sender: TObject);
    procedure ViewDoc1DragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure ViewDoc1DragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure taSpec2QuantChange(Sender: TField);
    procedure taSpec2Price1Change(Sender: TField);
    procedure taSpec2Sum1Change(Sender: TField);
    procedure taSpec2Price2Change(Sender: TField);
    procedure taSpec2Sum2Change(Sender: TField);
    procedure cxLabel3Click(Sender: TObject);
    procedure cxLabel2Click(Sender: TObject);
    procedure cxLabel4Click(Sender: TObject);
    procedure ViewDoc1Editing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cxLabel5Click(Sender: TObject);
    procedure ViewDoc1EditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure ViewDoc1EditKeyPress(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
    procedure acAddPosExecute(Sender: TObject);
    procedure acDelPosExecute(Sender: TObject);
    procedure acAddListExecute(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure acDelAllExecute(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure cxLabel6Click(Sender: TObject);
    procedure cxDateEdit1PropertiesChange(Sender: TObject);
    procedure ViewDoc1SMPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxButton2Click(Sender: TObject);
    procedure acSaveDocExecute(Sender: TObject);
    procedure acExitDocExecute(Sender: TObject);
    procedure ViewTaraDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure cxButtonEdit1KeyPress(Sender: TObject; var Key: Char);
    procedure Excel1Click(Sender: TObject);
    procedure acMovePosExecute(Sender: TObject);
    procedure ViewDoc1CustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure taSpecQuantChange(Sender: TField);
    procedure taSpecPrice1Change(Sender: TField);
    procedure taSpecSum1Change(Sender: TField);
    procedure taSpecPrice2Change(Sender: TField);
    procedure taSpecSum2Change(Sender: TField);
    procedure taSpecPrice0Change(Sender: TField);
    procedure taSpecSum0Change(Sender: TField);
    procedure taSpecNDSProcChange(Sender: TField);
    procedure taSpecRNdsChange(Sender: TField);
    procedure acSaveCodeCBExecute(Sender: TObject);
    procedure ViewDoc1DblClick(Sender: TObject);
    procedure acSelCodeCBExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAddDoc1: TfmAddDoc1;
  iCol:Integer = 0;
  iColT:Integer = 0;
  bAdd:Boolean = False;

implementation

uses Un1, dmOffice, Clients, Goods, FCards, DocsIn, CurMessure, OMessure,
  DMOReps, TestPrice, SelPerSkl, CardsMove, MainRnOffice, CB, Status;

{$R *.dfm}

procedure TfmAddDoc1.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  PageControl1.Align:=alClient;
  PageControl1.ActivePageIndex:=0;
//  GridDoc1.Align:=alClient;
  ViewDoc1.RestoreFromIniFile(CurDir+GridIni);
end;

procedure TfmAddDoc1.cxLookupComboBox1PropertiesChange(Sender: TObject);
begin
  with dmO do
  begin
    if quMHAll.Locate('ID',cxLookupComboBox1.EditValue,[]) then
    begin
      Label15.Caption:='��. ����: '+quMHAllNAMEPRICE.AsString;
      Label15.Tag:=quMHAllDEFPRICE.AsInteger;
    end else
    begin
      Label15.Caption:='';
      Label15.Tag:=0;
    end;  
  end;
end;

procedure TfmAddDoc1.cxButtonEdit1PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
var rNDS:Real;

begin
  if dmO.taClients.Active=False then dmO.taClients.Active:=True;
  if fmClients.Visible then fmClients.Close;
  bSelCli:=True;
  if cxButtonEdit1.Tag>0 then dmO.taClients.Locate('ID',cxButtonEdit1.Tag,[]);
  fmClients.ShowModal;
  if fmClients.ModalResult=mrOk then
  begin
    cxButtonEdit1.Tag:=dmO.taClientsID.AsInteger;
    cxButtonEdit1.Text:=dmO.taClientsNAMECL.AsString;
    Label4.Tag:=dmO.taClientsINDS.AsInteger;

    ViewDoc1.BeginUpdate;
    try
      if dmO.taClientsINDS.AsInteger=0 then
      begin //������������ ���
        taSpec.First;
        while not taSpec.Eof do
        begin
          taSpec.Edit;
          if abs(taSpecQuant.AsFloat)>0 then
          begin
            taSpecNDSProc.AsFloat:=0;
            taSpecSum0.AsFloat:=taSpecSum1.AsFloat;
            taSpecPrice0.AsFloat:=taSpecSum0.AsFloat/taSpecQuant.AsFloat;
            taSpecSumNac.AsFloat:=0;
            taSpecSum2.AsFloat:=taSpecSum1.AsFloat;
            taSpecRNds.AsFloat:=taSpecSum1.AsFloat-taSpecSum0.AsFloat;
          end;
          taSpec.Post;

          taSpec.Next;
        end;
      end else
      begin  //����������
        taSpec.First;
        while not taSpec.Eof do
        begin
          taSpec.Edit;
          if abs(taSpecQuant.AsFloat)>0 then
          begin
            rNds:=0;
            if taSpecINds.AsInteger=1 then rNds:=0;
            if taSpecINds.AsInteger=2 then rNds:=10;
            if taSpecINds.AsInteger=3 then rNds:=18;

            taSpecNDSProc.AsFloat:=rNDS;
            taSpecSum0.AsFloat:=rv(taSpecSum1.AsFloat*100/(100+rNDS));
            taSpecPrice0.AsFloat:=taSpecSum0.AsFloat/taSpecQuant.AsFloat;
            taSpecSumNac.AsFloat:=0;
            taSpecSum2.AsFloat:=taSpecSum1.AsFloat;
            taSpecRNds.AsFloat:=taSpecSum1.AsFloat-taSpecSum0.AsFloat;
          end;
          taSpec.Post;

          taSpec.Next;
        end;
      end;
    finally
      ViewDoc1.EndUpdate;
    end;
  end else
  begin
    cxButtonEdit1.Tag:=0;
    cxButtonEdit1.Text:='';
    Label4.Tag:=1;
  end;
  bSelCli:=False;
end;

procedure TfmAddDoc1.cxLabel1Click(Sender: TObject);
begin
  acAddList.Execute;
end;

procedure TfmAddDoc1.ViewDoc1DragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept:=False;
  if bDrIn then  Accept:=True;
end;

procedure TfmAddDoc1.ViewDoc1DragDrop(Sender, Source: TObject; X,
  Y: Integer);
Var iCo:Integer;
    i,j: Integer;
    iNum: Integer;
    Rec:TcxCustomGridRecord;
    iMax:Integer;
    iSS:Integer;
    Rec1:TcxCustomGridRecord;
    AHitTest: TcxCustomGridHitTest;
begin
//
  if bDrIn then
  begin
    ResetAddVars;
    iCo:=fmGoods.ViewGoods.Controller.SelectedRecordCount;
    if iCo>0 then
    begin
      if fmAddDoc1.Label18.Tag=0 then
      begin
        if MessageDlg('�� ������������� ������ ����������� ��������� ������� ('+IntToStr(iCo)+' ��.) � ������������ ���������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          iMax:=1;
          fmAddDoc1.taSpec.First;
          iSS:=prIss(fmAddDoc1.cxLookupComboBox1.EditValue);

          if not fmAddDoc1.taSpec.Eof then
          begin
            fmAddDoc1.taSpec.Last;
            iMax:=fmAddDoc1.taSpecNum.AsInteger+1;
          end;

          with dmO do
          begin
            ViewDoc1.BeginUpdate;
            for i:=0 to fmGoods.ViewGoods.Controller.SelectedRecordCount-1 do
            begin
              Rec:=fmGoods.ViewGoods.Controller.SelectedRecords[i];

              for j:=0 to Rec.ValueCount-1 do
              begin
                if fmGoods.ViewGoods.Columns[j].Name='ViewGoodsID' then break;
              end;

              iNum:=Rec.Values[j];
             //��� ��� - ����������

              if quCardsSel.Locate('ID',iNum,[]) then
              begin
                with fmAddDoc1 do
                begin
                  taSpec.Append;
                  taSpecNum.AsInteger:=iMax;
                  taSpecIdGoods.AsInteger:=quCardsSelID.AsInteger;
                  taSpecNameG.AsString:=quCardsSelNAME.AsString;
                  taSpecIM.AsInteger:=quCardsSelIMESSURE.AsInteger;
                  taSpecSM.AsString:=quCardsSelNAMESHORT.AsString;
                  taSpecKm.AsFloat:=quCardsselKOEF.AsFloat;
                  taSpecQuant.AsFloat:=1;
                  taSpecQuantRemn.AsFloat:=0;
                  taSpecPrice1.AsFloat:=0;
                  taSpecSum1.AsFloat:=0;
                  taSpecPrice2.AsFloat:=0;
                  taSpecSum2.AsFloat:=0;
                  if iSS<>2 then
                  begin
                    taSpecINds.AsInteger:=1;
                    taSpecSNds.AsString:='��� ���';
                    taSpecNDSProc.AsFloat:=0;
                  end else
                  begin
                    taSpecINds.AsInteger:=quCardsSelINDS.AsInteger;
                    taSpecSNds.AsString:=quCardsSelNAMENDS.AsString;
                    taSpecNDSProc.AsFloat:=quCardsSelPROC.AsFloat*fmAddDoc1.Label4.Tag;
                  end;
                  taSpecRNds.AsFloat:=0;
                  taSpecSumNac.AsFloat:=0;
                  taSpecProcNac.AsFloat:=0;
                  taSpecTCard.AsInteger:=quCardsSelTCARD.AsInteger;
                  taSpecCType.AsInteger:=quCardsSelCATEGORY.AsInteger;

                  taSpecPrice0.AsFloat:=0;
                  taSpecSum0.AsFloat:=0;

                  taSpec.Post;
                  inc(iMax);
                end;
              end;
            end;
            ViewDoc1.EndUpdate;
          end;
        end;
      end;

      if fmAddDoc1.Label18.Tag=1 then
      begin
        if iCo=1 then //���� � ���� ������������ �����
        begin
          if Sender is TcxGridSite then
          begin
            AHitTest := TcxGridSite(Sender).ViewInfo.GetHitTest(X, Y);
            if AHitTest.HitTestCode=107 then //������
            begin
              Rec1:=TcxGridRecordHitTest(AHitTest).GridRecord;
              for j:=0 to Rec1.ValueCount-1 do
                if ViewDoc1.Columns[j].Name='ViewDoc1Num' then break;

              iNum:=Rec1.Values[j];

              if taSpec.Locate('Num',iNum,[]) then
              begin
                with dmO do
                begin
                  iSS:=prIss(fmAddDoc1.cxLookupComboBox1.EditValue);

                  taSpec.Edit;
                  taSpecIdGoods.AsInteger:=quCardsSelID.AsInteger;
                  taSpecNameG.AsString:=quCardsSelNAME.AsString;
                  taSpecIM.AsInteger:=quCardsSelIMESSURE.AsInteger;
                  taSpecSM.AsString:=quCardsSelNAMESHORT.AsString;
                  taSpecKm.AsFloat:=quCardsselKOEF.AsFloat;

                  if iSS<>2 then
                  begin
                    taSpecINds.AsInteger:=1;
                    taSpecSNds.AsString:='��� ���';
                    taSpecNDSProc.AsFloat:=0;

                    taSpecRNds.AsFloat:=0;
                    taSpecPrice0.AsFloat:=taSpecPrice1.AsFloat;
                    taSpecSum0.AsFloat:=taSpecSum1.AsFloat;
                  end else
                  begin
                    taSpecINds.AsInteger:=quCardsSelINDS.AsInteger;
                    taSpecSNds.AsString:=quCardsSelNAMENDS.AsString;
                    taSpecNDSProc.AsFloat:=(quCardsSelPROC.AsFloat)*fmAddDoc1.Label4.Tag;
                  end;
                  taSpecTCard.AsInteger:=quCardsSelTCard.AsINteger;
                  taSpecCType.AsInteger:=quCardsSelCATEGORY.AsInteger;
                  taSpec.Post;
                end;

              end;
            end;
          end;

        end;
      end;

    end;
  end;
end;

procedure TfmAddDoc1.taSpec2QuantChange(Sender: TField);
begin
  //���������� �����
  if iCol=1 then
  begin
    taSpecSum1.AsFloat:=taSpecPrice1.AsFloat*taSpecQuant.AsFloat;
    taSpecSum2.AsFloat:=taSpecPrice2.AsFloat*taSpecQuant.AsFloat;
    taSpecSumNac.AsFloat:=taSpecSum2.AsFloat-taSpecSum1.AsFloat;
    if taSpecSum1.AsFloat>0.01 then
    begin
      taSpecProcNac.AsFloat:=(taSpecSum2.AsFloat-taSpecSum1.AsFloat)/taSpecSum1.AsFloat*100;
    end;
    taSpecRNds.AsFloat:=RoundEx(taSpecSum1.AsFloat*vNds[taSpecINds.AsInteger]/(100+vNds[taSpecINds.AsInteger])*100)/100;
  end;
end;

procedure TfmAddDoc1.taSpec2Price1Change(Sender: TField);
begin
  //���������� ����
  if iCol=2 then
  begin
    taSpecSum1.AsFloat:=taSpecPrice1.AsFloat*taSpecQuant.AsFloat;
//  taSpecSum2.AsFloat:=taSpecPrice2.AsFloat*taSpecQuant.AsFloat;
    taSpecSumNac.AsFloat:=taSpecSum2.AsFloat-taSpecSum1.AsFloat;
    if taSpecSum1.AsFloat>0.01 then
    begin
      taSpecProcNac.AsFloat:=(taSpecSum2.AsFloat-taSpecSum1.AsFloat)/taSpecSum1.AsFloat*100;
    end;
    taSpecRNds.AsFloat:=RoundEx(taSpecSum1.AsFloat*vNds[taSpecINds.AsInteger]/(100+vNds[taSpecINds.AsInteger])*100)/100;
  end;
end;

procedure TfmAddDoc1.taSpec2Sum1Change(Sender: TField);
begin
  if iCol=3 then
  begin
    if abs(taSpecQuant.AsFloat)>0 then taSpecPrice1.AsFloat:=RoundEx(taSpecSum1.AsFloat/taSpecQuant.AsFloat*100)/100;
    taSpecSumNac.AsFloat:=taSpecSum2.AsFloat-taSpecSum1.AsFloat;
    if taSpecSum1.AsFloat>0.01 then
    begin
      taSpecProcNac.AsFloat:=(taSpecSum2.AsFloat-taSpecSum1.AsFloat)/taSpecSum1.AsFloat*100;
    end;
    taSpecRNds.AsFloat:=RoundEx(taSpecSum1.AsFloat*vNds[taSpecINds.AsInteger]/(100+vNds[taSpecINds.AsInteger])*100)/100;
  end;
end;

procedure TfmAddDoc1.taSpec2Price2Change(Sender: TField);
begin
  //���������� ����
  if iCol=4 then
  begin
//  taSpecSum1.AsFloat:=taSpecPrice1.AsFloat*taSpecQuant.AsFloat;
    taSpecSum2.AsFloat:=taSpecPrice2.AsFloat*taSpecQuant.AsFloat;
    taSpecSumNac.AsFloat:=taSpecSum2.AsFloat-taSpecSum1.AsFloat;
    if taSpecSum1.AsFloat>0.01 then
    begin
      taSpecProcNac.AsFloat:=(taSpecSum2.AsFloat-taSpecSum1.AsFloat)/taSpecSum1.AsFloat*100;
    end;  
  end;
end;

procedure TfmAddDoc1.taSpec2Sum2Change(Sender: TField);
begin
  if iCol=5 then
  begin
    if abs(taSpecQuant.AsFloat)>0 then taSpecPrice2.AsFloat:=RoundEx(taSpecSum2.AsFloat/taSpecQuant.AsFloat*100)/100;
    taSpecSumNac.AsFloat:=taSpecSum2.AsFloat-taSpecSum1.AsFloat;
    if taSpecSum1.AsFloat>0.01 then
    begin
      taSpecProcNac.AsFloat:=(taSpecSum2.AsFloat-taSpecSum1.AsFloat)/taSpecSum1.AsFloat*100;
    end;
  end;
end;

procedure TfmAddDoc1.cxLabel3Click(Sender: TObject);
Var rPriceIn:Currency;
    rMessure:Real;
    rQr:Real;
begin
  //����������� ����
  if PageControl1.ActivePageIndex=0 then
  begin
    ViewDoc1.BeginUpdate;
    try
      taSpec.First;
      while not taSpec.Eof do
      begin
        dmO.prCalcLastPrice1.ParamByName('IDGOOD').AsInteger:=taSpecIdGoods.AsInteger;
        dmO.prCalcLastPrice1.ParamByName('ISKL').AsInteger:=cxLookupComboBox1.EditValue;
        dmO.prCalcLastPrice1.ExecProc;
        rPriceIn:=dmO.prCalcLastPrice1.ParamByName('PRICEIN').AsFloat;
        rMessure:=dmO.prCalcLastPrice1.ParamByName('KOEF').AsFloat;
        if (rMessure<>0)and(rMessure<>1)and(rMessure<>taSpecKm.AsFloat)
          then rPriceIn:=RoundEx(rPriceIn*taSpecKm.AsFloat/rMessure*100)/100;

//?INPRICE, ?IDGOOD, ?PRICE0, ?PRICE1
{     prCalcPrice.ParamByName('INPRICE').AsInteger:=Label15.Tag;
      prCalcPrice.ParamByName('IDGOOD').AsInteger:=taSpecIdGoods.AsInteger;
      prCalcPrice.ParamByName('PRICE0').AsFloat:=rPriceIn;
      prCalcPrice.ParamByName('PRICE1').AsFloat:=0;
      prCalcPrice.ExecProc;

      rPrice:=prCalcPrice.ParamByName('PRICEOUT').AsFloat;  //������� ����
}

        if cxLookupComboBox1.EditValue>0 then
          rQr:=prCalcRemn(taSpecIdGoods.AsInteger,Trunc(Date),cxLookupComboBox1.EditValue)
        else rQr:=0;

        taSpec.Edit;
        if taSpecPrice1.AsFloat<0.01 then taSpecPrice1.AsFloat:=rPriceIn;
        if taSpecSum1.AsFloat<0.01 then taSpecSum1.AsFloat:=rv(rPriceIn*taSpecQuant.AsFloat);

        taSpecPrice2.AsFloat:=taSpecPrice1.AsFloat;
        taSpecSum2.AsFloat:=taSpecSum1.AsFloat;

        taSpecSum0.AsFloat:=rv(taSpecSum1.AsFloat*100/(100+taSpecNDSProc.AsFloat));
        if abs(taSpecQuant.AsFloat)>0 then taSpecPrice0.AsFloat:=taSpecSum0.AsFloat/taSpecQuant.AsFloat;

        taSpecSumNac.AsFloat:=0;
        taSpecProcNac.AsFloat:=0;
        taSpecPricePP.AsFloat:=rPriceIn;
        taSpecQuantRemn.AsFloat:=rQr;
        taSpec.Post;

        taSpec.Next;
        delay(10);
      end;
      taSpec.First;
    finally
      ViewDoc1.EndUpdate;
    end;
  end;
end;

procedure TfmAddDoc1.cxLabel2Click(Sender: TObject);
begin
  acDelPos.Execute;
end;

procedure TfmAddDoc1.cxLabel4Click(Sender: TObject);
Var rQr:Real;
begin
  //�������� ����
{  if PageControl1.ActivePageIndex=0 then
  begin
    ViewDoc1.BeginUpdate;
    taSpec.First;
    while not taSpec.Eof do
    begin
      taSpec.Edit;
      taSpecPrice2.AsFloat:=taSpecPrice1.AsFloat;
      taSpecSum2.AsFloat:=taSpecSum1.AsFloat;
      taSpecSumNac.AsFloat:=0;
      taSpecProcNac.AsFloat:=0;
      taSpec.Post;

      taSpec.Next;
      delay(10);
    end;
    taSpec.First;
    ViewDoc1.EndUpdate;
  end;}

  //��������� �������
  if PageControl1.ActivePageIndex=0 then
  begin
    taSpec.First;
    while not taSpec.Eof do
    begin
      if cxLookupComboBox1.EditValue>0 then
        rQr:=prCalcRemn(taSpecIdGoods.AsInteger,Trunc(Date),cxLookupComboBox1.EditValue)
      else rQr:=0;

      taSpec.Edit;
      taSpecQuantRemn.AsFloat:=rQr;
      taSpec.Post;

      taSpec.Next;
      delay(10);
    end;
    taSpec.First;
  end;
end;

procedure TfmAddDoc1.ViewDoc1Editing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  iCol:=0;
  if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1Quant' then iCol:=1;
  if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1Price1' then iCol:=2;
  if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1Sum1' then iCol:=3;
  if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1Price2' then iCol:=4;
  if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1Sum2' then iCol:=5;

  if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1Price0' then iCol:=6;
  if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1Sum0' then iCol:=7;
  if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1NDSProc' then iCol:=8;
  if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1RNds' then iCol:=9;


end;

procedure TfmAddDoc1.FormShow(Sender: TObject);
begin
  iCol:=0;
  iColT:=0;
  PageControl1.ActivePageIndex:=0;

  if cxTextEdit1.Text='' then
  begin
    cxTextEdit1.SetFocus;
    cxTextEdit1.SelectAll;
  end else GridDoc1.SetFocus;

  ViewDoc1NameG.Options.Editing:=False;
end;

procedure TfmAddDoc1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if cxButton1.Enabled=True then
  begin
    if MessageDlg('�� ������������� ������ ����� �� ���������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      ViewDoc1.StoreToIniFile(CurDir+GridIni,False);
      Action := caHide;
    end  
    else  Action := caNone;
  end;
end;

procedure TfmAddDoc1.cxLabel5Click(Sender: TObject);
begin
  acAddPos.Execute;
end;

procedure TfmAddDoc1.ViewDoc1EditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
Var iCode:Integer;
    Km:Real;
    sName:String;
    //���� ������ ��� ������ ����� �� ������
    rK,rNDS:Real;
    iM:Integer;
    Sm:String;
    iSS:INteger;

begin
  with dmO do
  begin
    if (Key=$0D) then
    begin
      rNDS:=0;

      if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1IdGoods' then
      begin
        iCode:=VarAsType(AEdit.EditingValue, varInteger);

        quFCard.Active:=False;
        quFCard.SelectSQL.Clear;
        quFCard.SelectSQL.Add('SELECT ID,NAME,IMESSURE,PARENT,TCARD,INDS,LASTPRICEOUT,CATEGORY');
        quFCard.SelectSQL.Add('FROM OF_CARDS');
        quFCard.SelectSQL.Add('where ID='+IntToStr(iCode));
        quFCard.SelectSQL.Add('and IACTIVE>0');
        quFCard.SelectSQL.Add('and PARENT in (SELECT ID_CLASSIF FROM RCLASSIF WHERE RIGHTS=0 AND ID_PERSONAL='+its(Person.Id)+')');
        quFCard.Active:=True;

        if quFCard.RecordCount=1 then
        begin
          iSS:=prIss(fmAddDoc1.cxLookupComboBox1.EditValue);

          ViewDoc1.BeginUpdate;
          taSpec.Edit;
          taSpecIdGoods.AsInteger:=iCode;
          taSpecNameG.AsString:=quFCardNAME.AsString;
          taSpecIM.AsInteger:=quFCardIMESSURE.AsInteger;
          taSpecSM.AsString:=prFindKNM(quFCardIMESSURE.AsInteger,Km);
          taSpecKm.AsFloat:=Km;

          if (Label4.Tag=1)and(iSS=2) then //��������� - ���������� ���
          begin
            if taNDS.Active=False then taNDS.Active:=True;
            if taNds.Locate('ID',quFCardINDS.AsInteger,[]) then
            begin
              taSpecINds.AsInteger:=quFCardINDS.AsInteger;
              taSpecSNds.AsString:=taNDSNAMENDS.AsString;
              rNDS:=taNDSPROC.AsFloat;
            end;
          end else
          begin
            taSpecINds.AsInteger:=1;
            taSpecSNds.AsString:='��� ���';
            rNDS:=0;
          end;
          taSpecTCard.AsInteger:=quFCardTCARD.AsInteger;
          taSpecCType.AsInteger:=quFCardCATEGORY.AsInteger;
          taSpecNDSProc.AsFloat:=rNDS;
          {
          if Label18.Tag=0 then  //�� �������
          begin
            taSpecQuant.AsFloat:=0;
            taSpecQuantRemn.AsFloat:=0;
            taSpecPrice1.AsFloat:=0;
            taSpecSum1.AsFloat:=0;
            taSpecPrice2.AsFloat:=0;
            taSpecSum2.AsFloat:=0;
            taSpecINds.AsInteger:=0;
            taSpecSNds.AsString:='';


            taSpecRNds.AsFloat:=0;
            taSpecSumNac.AsFloat:=0;
            taSpecProcNac.AsFloat:=0;

            taSpecPrice0.AsFloat:=0;
            taSpecSum0.AsFloat:=0;
          end;
          }
          taSpec.Post;

          ViewDoc1.EndUpdate;
        end;
      end;
      if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1NameG' then
      begin
        sName:=VarAsType(AEdit.EditingValue, varString);

        fmFCards.ViewFCards.BeginUpdate;
        quFCard.Active:=False;
        quFCard.SelectSQL.Clear;
        quFCard.SelectSQL.Add('SELECT first 100 ID,NAME,IMESSURE,PARENT,TCARD,INDS,LASTPRICEOUT,CATEGORY');
        quFCard.SelectSQL.Add('FROM OF_CARDS');
        quFCard.SelectSQL.Add('where UPPER(NAME) like ''%'+AnsiUpperCase(sName)+'%''');
        quFCard.SelectSQL.Add('and IACTIVE>0');
        quFCard.SelectSQL.Add('and PARENT in (SELECT ID_CLASSIF FROM RCLASSIF WHERE RIGHTS=0 AND ID_PERSONAL='+its(Person.Id)+')');
        quFCard.SelectSQL.Add('Order by NAME');

        quFCard.Active:=True;

        bAdd:=True;

        quFCard.Locate('NAME',sName,[loCaseInsensitive, loPartialKey]);
        prRowFocus(fmFCards.ViewFCards);
        fmFCards.ViewFCards.EndUpdate;

        //�������� ����� ������ � ����� ������
        fmFCards.ShowModal;
        if fmFCards.ModalResult=mrOk then
        begin
          if quFCard.RecordCount>0 then
          begin
            iSS:=prIss(fmAddDoc1.cxLookupComboBox1.EditValue);

            ViewDoc1.BeginUpdate;
            taSpec.Edit;
            taSpecIdGoods.AsInteger:=quFCardID.AsInteger;
            taSpecNameG.AsString:=quFCardNAME.AsString;
            taSpecIM.AsInteger:=quFCardIMESSURE.AsInteger;
            taSpecSM.AsString:=prFindKNM(quFCardIMESSURE.AsInteger,Km);
            taSpecKm.AsFloat:=Km;

            if (Label4.Tag=1)and(iSS=2) then //��������� - ���������� ���
            begin
              if taNDS.Active=False then taNDS.Active:=True;
              if taNds.Locate('ID',quFCardINDS.AsInteger,[]) then
              begin
                taSpecINds.AsInteger:=quFCardINDS.AsInteger;
                taSpecSNds.AsString:=taNDSNAMENDS.AsString;
                rNDS:=taNDSPROC.AsFloat;
              end;
            end else
            begin
              taSpecINds.AsInteger:=1;
              taSpecSNds.AsString:='��� ���';
              rNDS:=0;
            end;

            taSpecTCard.AsInteger:=quFCardTCARD.AsInteger;
            taSpecCType.AsInteger:=quFCardCATEGORY.AsInteger;
            taSpecNDSProc.AsFloat:=rNDS;
           {
            if Label18.Tag=0 then  //�� �������
            begin
              taSpecQuant.AsFloat:=0;
              taSpecQuantRemn.AsFloat:=0;
              taSpecPrice1.AsFloat:=0;
              taSpecSum1.AsFloat:=0;
              taSpecPrice2.AsFloat:=0;
              taSpecSum2.AsFloat:=0;
              taSpecINds.AsInteger:=0;
              taSpecSNds.AsString:='';
              taSpecRNds.AsFloat:=0;

              taSpecSumNac.AsFloat:=0;
              taSpecProcNac.AsFloat:=0;

              taSpecPrice0.AsFloat:=0;
              taSpecSum0.AsFloat:=0;
            end;
            }
            taSpec.Post;
            ViewDoc1.EndUpdate;
            AEdit.SelectAll;
          end;
        end;
        bAdd:=False;
      end;
      if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1SM' then
      begin
        if taSpecIm.AsInteger>0 then
        begin
          bAddSpec:=True;
          fmMessure.ShowModal;
          if fmMessure.ModalResult=mrOk then
          begin
            iM:=iMSel; iMSel:=0;
            Sm:=prFindKNM(iM,rK); //�������� ����� �������� � ����� �����

            taSpec.Edit;
            taSpecIM.AsInteger:=iM;
            taSpecKm.AsFloat:=rK;
            taSpecSM.AsString:=Sm;
            taSpec.Post;
          end;
        end;
      end;

      ViewDoc1Quant.Focused:=True;

    end else
      if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1IdGoods' then
        if fTestKey(Key)=False then
          if taSpec.State in [dsEdit,dsInsert] then taSpec.Cancel;
  end;
end;

procedure TfmAddDoc1.ViewDoc1EditKeyPress(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
Var Km,rNDS:Real;
    sName:String;
    iSS:INteger;
begin
  if bAdd then exit;
  with dmO do
  begin
    rNDS:=0;
    if (ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1NameG') then
    begin
      sName:=VarAsType(AEdit.EditingValue ,varString)+Key;
//      Label2.Caption:=sName;
      if length(sName)>2 then
      begin
        quFCard.Active:=False;
        quFCard.SelectSQL.Clear;
        quFCard.SelectSQL.Add('SELECT  first 2 ID,NAME,IMESSURE,PARENT,TCARD,INDS,LASTPRICEOUT,CATEGORY');
        quFCard.SelectSQL.Add('FROM OF_CARDS');
        quFCard.SelectSQL.Add('where UPPER(NAME) like ''%'+AnsiUpperCase(sName)+'%''');
        quFCard.SelectSQL.Add('and IACTIVE>0');
        quFCard.SelectSQL.Add('and PARENT in (SELECT ID_CLASSIF FROM RCLASSIF WHERE RIGHTS=0 AND ID_PERSONAL='+its(Person.Id)+')');
        quFCard.SelectSQL.Add('Order by NAME');
        quFCard.Active:=True;

        if quFCard.RecordCount=1 then
        begin
          iSS:=prIss(fmAddDoc1.cxLookupComboBox1.EditValue);

          ViewDoc1.BeginUpdate;
          taSpec.Edit;
          taSpecIdGoods.AsInteger:=quFCardID.AsInteger;
          taSpecNameG.AsString:=quFCardNAME.AsString;
          taSpecIM.AsInteger:=quFCardIMESSURE.AsInteger;
          taSpecSM.AsString:=prFindKNM(quFCardIMESSURE.AsInteger,Km);
          taSpecKm.AsFloat:=Km;

          if (Label4.Tag=1)and(iSS=2) then //��������� - ���������� ���
          begin
            if taNDS.Active=False then taNDS.Active:=True;
            if taNds.Locate('ID',quFCardINDS.AsInteger,[]) then
            begin
              taSpecINds.AsInteger:=quFCardINDS.AsInteger;
              taSpecSNds.AsString:=taNDSNAMENDS.AsString;
              rNDS:=taNDSPROC.AsFloat;
            end;
          end else
          begin
            taSpecINds.AsInteger:=1;
            taSpecSNds.AsString:='��� ���';
            rNDS:=0;
          end;

          taSpecTCard.AsInteger:=quFCardTCARD.AsInteger;
          taSpecCType.AsInteger:=quFCardCATEGORY.AsInteger;
          taSpecNDSProc.AsFloat:=rNDS;
         {
          if Label18.Tag=0 then  //�� �������
          begin
            taSpecQuant.AsFloat:=0;
            taSpecQuantRemn.AsFloat:=0;
            taSpecPrice1.AsFloat:=0;
            taSpecSum1.AsFloat:=0;
            taSpecPrice2.AsFloat:=0;
            taSpecSum2.AsFloat:=0;
            taSpecINds.AsInteger:=0;
            taSpecSNds.AsString:='';

            taSpecRNds.AsFloat:=0;
            taSpecSumNac.AsFloat:=0;
            taSpecProcNac.AsFloat:=0;
            taSpecPrice0.AsFloat:=0;
            taSpecSum0.AsFloat:=0;
          end;
          }
          taSpec.Post;
          ViewDoc1.EndUpdate;
          AEdit.SelectAll;
          ViewDoc1NameG.Options.Editing:=False;
          ViewDoc1NameG.Focused:=True;
          Key:=#0;
        end;
      end;
    end;
  end;//}
end;

procedure TfmAddDoc1.acAddPosExecute(Sender: TObject);
Var iMax:Integer;
begin
  //�������� �������
  if PageControl1.ActivePageIndex=0 then
  begin
    iMax:=1;
    ViewDoc1.BeginUpdate;

    taSpec.First;
    if not taSpec.Eof then
    begin
      taSpec.Last;
      iMax:=taSpecNum.AsInteger+1;
    end;

    taSpec.Append;
    taSpecNum.AsInteger:=iMax;
    taSpecIdGoods.AsInteger:=0;
    taSpecNameG.AsString:='';
    taSpecIM.AsInteger:=0;
    taSpecSM.AsString:='';
    taSpecKm.AsFloat:=0;
    taSpecQuant.AsFloat:=0;
    taSpecQuantRemn.AsFloat:=0;
    taSpecPrice1.AsFloat:=0;
    taSpecSum1.AsFloat:=0;
    taSpecPrice2.AsFloat:=0;
    taSpecSum2.AsFloat:=0;
    taSpecINds.AsInteger:=0;
    taSpecSNds.AsString:='';
    taSpecRNds.AsFloat:=0;
    taSpecSumNac.AsFloat:=0;
    taSpecProcNac.AsFloat:=0;
    taSpecTCard.AsInteger:=0;
    taSpecCType.AsInteger:=1;
    taSpecNDSProc.AsFloat:=0;
    taSpecPrice0.AsFloat:=0;
    taSpecSum0.AsFloat:=0;
    taSpec.Post;
    ViewDoc1.EndUpdate;
    GridDoc1.SetFocus;

    ViewDoc1NameG.Options.Editing:=True;
    ViewDoc1NameG.Focused:=True;

    prRowFocus(ViewDoc1);
  end;
end;

procedure TfmAddDoc1.acDelPosExecute(Sender: TObject);
begin
  //������� �������
  if PageControl1.ActivePageIndex=0 then
  begin
    if taSpec.RecordCount>0 then
    begin
      taSpec.Delete;
    end;
  end;
end;

procedure TfmAddDoc1.acAddListExecute(Sender: TObject);
begin
  bAddSpecIn:=True;
  fmGoods.Show;
end;

procedure TfmAddDoc1.cxButton1Click(Sender: TObject);
Var IdH:Integer;
    rSum1,rSum2,rSumT:Real;
    bErr:Boolean;
    iC:INteger;
begin
  //��������
  with dmO do
  with dmORep do
  begin
    CurVal.IdMH:=fmAddDoc1.cxLookupComboBox1.EditValue;
    CurVal.NAMEMH:=fmAddDoc1.cxLookupComboBox1.Text;

    IdH:=cxTextEdit1.Tag;
    if taSpec.State in [dsEdit,dsInsert] then taSpec.Post;
    if cxDateEdit1.Date<3000 then  cxDateEdit1.Date:=Date;
    if cxDateEdit2.Date<3000 then  cxDateEdit2.Date:=Date;


    ViewDoc1.BeginUpdate;
    iC:=iCol;
    iCol:=0;

    bErr:=False;
    taSpec.First;
    while not taSpec.Eof do
    begin
      if taSpecIdGoods.AsInteger=0 then bErr:=True;
      if taSpecQuant.AsFloat=0 then bErr:=True;

      taSpec.Edit;
      taSpecSum0.AsFloat:=rv(taSpecSum0.AsFloat);
      taSpecSum1.AsFloat:=rv(taSpecSum1.AsFloat);
      taSpecRNds.AsFloat:=taSpecSum1.AsFloat-taSpecSum0.AsFloat;
      taSpec.Post;

      taSpec.Next;
    end;
    if bErr then
    begin
      showmessage('������ ��������� ������������ (������� ��� ��� ������� ���-��). ���������� ����������!');
      ViewDoc1.EndUpdate;
      exit;
    end;

    //��������� ���� ��
    CloseTe(mePrice);
    taSpec.First;
    while not taSpec.Eof do
    begin
      if (abs(taSpecPrice1.AsFloat-taSpecPricePP.AsFloat)>0.01) and (taSpecPricePP.AsFloat>0) then
      begin
        mePrice.Append;
        mePriceNum.AsInteger:=taSpecNum.AsInteger;
        mePriceIdGoods.AsInteger:=quSpecInSelIDCARD.AsInteger;
        mePriceNameG.AsString:=taSpecNameG.AsString;
        mePriceSM.AsString:=taSpecSM.AsString;
        mePriceQuant.AsFloat:=taSpecQuant.AsFloat;
        mePricePrice1.AsFloat:=taSpecPrice1.AsFloat;
        mePricePricePP.AsFloat:=taSpecPricePP.AsFloat;
        if mePricePricePP.AsFloat<>0 then  mePriceDif.AsFloat:=(taSpecPrice1.AsFloat-mePricePricePP.AsFloat)/mePricePricePP.AsFloat*100
        else mePriceDif.AsFloat:=100;
        mePrice.Post;
      end;

      taSpec.Next;
    end;

    if mePrice.RecordCount>0 then
    begin
      fmTestPrice:=TfmTestPrice.Create(Application);
      fmTestPrice.ShowModal;
      if fmTestPrice.ModalResult<>mrOk then
      begin
        fmTestPrice.Release;
        ViewDoc1.EndUpdate;
        mePrice.Active:=False;
        exit;
      end;
      fmTestPrice.Release;
    end;

    //������� ������ ������

    if cxTextEdit1.Tag=0 then
    begin
      IDH:=GetId('DocIn');
      cxTextEdit1.Tag:=IDH;
      if cxTextEdit1.Text=prGetNum(1,0) then prGetNum(1,1); //��������
    end;

    quDocsInId.Active:=False;
    quDocsInId.ParamByName('IDH').AsInteger:=IDH;
    quDocsInId.Active:=True;

    quDocsInId.First;
    if quDocsInId.RecordCount=0 then
    begin
     quDocsInId.Append;
     quDocsInIdCREATETYPE.AsINteger:=0;
    end else
    begin
      quDocsInId.Edit;
    end;

    quDocsInIdID.AsInteger:=IDH;
    quDocsInIdDATEDOC.AsDateTime:=Trunc(cxDateEdit1.Date);
    quDocsInIdNUMDOC.AsString:=cxTextEdit1.Text;
    quDocsInIdDATESF.AsDateTime:=Trunc(cxDateEdit2.Date);
    quDocsInIdNUMSF.AsString:=cxTextEdit2.Text;
    quDocsInIdIDCLI.AsInteger:=cxButtonEdit1.Tag;
    quDocsInIdIDSKL.AsInteger:=cxLookupComboBox1.EditValue;
    quDocsInIdSUMIN.AsFloat:=0;
    quDocsInIdSUMUCH.AsFloat:=0;
    quDocsInIdSUMTAR.AsFloat:=0;
    quDocsInIdSUMNDS0.AsFloat:=sNDS[1];
    quDocsInIdSUMNDS1.AsFloat:=sNDS[2];
    quDocsInIdSUMNDS2.AsFloat:=sNDS[3];
    quDocsInIdPROCNAC.AsFloat:=0;
    quDocsInIdIACTIVE.AsInteger:=0;
    quDocsInIdCOMMENT.AsString:=cxTextEdit3.Text;
    quDocsInIdOPRIZN.AsInteger:=cxSpinEdit1.Value;
    quDocsInId.Post;

    //�������� ������������
    quSpecInSel.Active:=False;
    quSpecInSel.ParamByName('IDHD').AsInteger:=IDH;
    quSpecInSel.Active:=True;

    quSpecInSel.First; //������� ������
    while not quSpecInSel.Eof do quSpecInSel.Delete;

    sNDS[1]:=0;sNDS[2]:=0;sNDS[3]:=0;
    rSum1:=0; rSum2:=0;  rSumT:=0;
    taSpec.First;
    while not taSpec.Eof do
    begin
      quSpecInSel.Append;
      quSpecInSelIDHEAD.AsInteger:=IDH;
      quSpecInSelID.AsInteger:=GetId('SpecIn');
      quSpecInSelNUM.AsInteger:=taSpecNum.AsInteger;
      quSpecInSelIDCARD.AsInteger:=taSpecIdGoods.AsInteger;
      quSpecInSelQUANT.AsFloat:=taSpecQuant.AsFloat;
      quSpecInSelPRICEIN.AsFloat:=taSpecPrice1.AsFloat;
      quSpecInSelSUMIN.AsFloat:=taSpecSum1.AsFloat;
      quSpecInSelPRICEUCH.AsFloat:=taSpecPrice2.AsFloat;
      quSpecInSelSUMUCH.AsFloat:=taSpecSum2.AsFloat;
      quSpecInSelIDNDS.AsInteger:=taSpecINds.AsInteger;
      quSpecInSelSUMNDS.AsFloat:=taSpecRNds.AsFloat;
      quSpecInSelIDM.AsInteger:=taSpecIM.AsInteger;
      quSpecInSelKM.AsFloat:=taSpecKM.AsFloat;

      quSpecInSelSUM0.AsFloat:=taSpecSum0.AsFloat;
      quSpecInSelPRICE0.AsFloat:=taSpecPrice0.AsFloat;
      quSpecInSelNDSPROC.AsFloat:=taSpecNDSProc.AsFloat;

      quSpecInSelOPRIZN.AsInteger:=taSpecOPrizn.AsInteger;

      if Label18.Tag=1 then
      begin
        quSpecInSelICODECB.AsInteger:=taSpeciCodeCB.AsInteger;
        quSpecInSelNAMECB.AsString:=taSpecNameCB.AsString;
        quSpecInSelIMCB.AsInteger:=taSpeciMCB.AsInteger;
        quSpecInSelSMCB.AsString:=taSpecSMCB.AsString;
        quSpecInSelSBARCB.AsString:=taSpecSBARCB.AsString;
        quSpecInSelKBCB.AsFloat:=taSpecKBCB.AsFloat;
        quSpecInSelQUANTCB.AsFloat:=taSpecQUANTCB.AsFloat;
      end;

      quSpecInSelGTD.AsString:=taSpecGTD.AsString;

      quSpecInSel.Post;

      sNDS[taSpecINds.AsInteger]:=sNDS[taSpecINds.AsInteger]+taSpecRNds.AsFloat;

      if (taSpecCType.AsInteger=1)or(taSpecCType.AsInteger=2)or(taSpecCType.AsInteger=3) then
      begin
        rSum1:=rSum1+taSpecSum1.AsFloat; rSum2:=rSum2+taSpecSum2.AsFloat;
      end;
      if (taSpecCType.AsInteger=4) then
      begin
        rSumT:=rSumT+taSpecSum1.AsFloat;
      end;

      taSpec.Next;
    end;

    quDocsInId.Edit;
    quDocsInIdSUMIN.AsFloat:=RoundVal(rSum1);
    quDocsInIdSUMUCH.AsFloat:=RoundVal(rSum2);
    quDocsInIdSUMTAR.AsFloat:=RoundVal(rSumT);
    quDocsInIdSUMNDS0.AsFloat:=RoundVal(sNDS[1]);
    quDocsInIdSUMNDS1.AsFloat:=RoundVal(sNDS[2]);
    quDocsInIdSUMNDS2.AsFloat:=RoundVal(sNDS[3]);
    quDocsInIdPROCNAC.AsFloat:=0;
    quDocsInIdIACTIVE.AsInteger:=0;
    quDocsInId.Post;

    quDocsInId.Active:=False;

    fmDocsIn.ViewDocsIn.BeginUpdate;
    quDocsInSel.FullRefresh;
    quDocsInSel.Locate('ID',IDH,[]);
    fmDocsIn.ViewDocsIn.EndUpdate;

    iCol:=iC;
    ViewDoc1.EndUpdate;             
  end;
end;

procedure TfmAddDoc1.acDelAllExecute(Sender: TObject);
begin
  if PageControl1.ActivePageIndex=0 then
  begin
    if taSpec.RecordCount>0 then
    begin
      if MessageDlg('�� ������������� ������ �������� ������������ �������?',
       mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        taSpec.First; while not taSpec.Eof do taSpec.Delete;
      end;
    end;
  end;  
  if PageControl1.ActivePageIndex=1 then
  begin
  end;
end;

procedure TfmAddDoc1.N1Click(Sender: TObject);
Type tRecSpec = record
     Num:INteger;
     IdGoods:Integer;
     NameG:String;
     IM:Integer;
     SM:String;
     Quant,Price1,Price2,Sum1,Sum2,QuantRemn,Price0,Sum0:Real;
     iNds,TCard,CType:INteger;
     sNds:String;
     rNds,SumNac,ProcNac,Km,NDSProc:Real;
     end;
Var RecSpec:TRecSpec;
    iMax:Integer;
begin
  //���������� �������
  if not taSpec.Eof then
  begin
    RecSpec.Num:=taSpecNum.AsInteger;
    RecSpec.IdGoods:=taSpecIdGoods.AsInteger;
    RecSpec.Quant:=taSpecQuant.AsFloat;
    RecSpec.QuantRemn:=taSpecQuantRemn.AsFloat;
    RecSpec.Price1:=taSpecPrice1.AsFloat;
    RecSpec.Sum1:=taSpecSum1.AsFloat;
    RecSpec.Price2:=taSpecPrice2.AsFloat;
    RecSpec.Sum2:=taSpecSum2.AsFloat;
    RecSpec.iNds:=taSpecINds.AsInteger;
    RecSpec.rNds:=taSpecRNds.AsFloat;
    RecSpec.IM:=taSpecIM.AsInteger;
    RecSpec.NameG:=taSpecNameG.AsString;
    RecSpec.SM:=taSpecSM.AsString;
    RecSpec.sNds:=taSpecsNds.AsString;
    RecSpec.SumNac:=taSpecSumNac.AsFloat;
    RecSpec.ProcNac:=taSpecProcNac.AsFloat;
    RecSpec.Km:=taSpecKm.AsFloat;
    RecSpec.TCard:=taSpecTCard.AsInteger;
    RecSpec.CType:=taSpecCType.AsInteger;
    RecSpec.Price0:=taSpecPrice0.AsFloat;
    RecSpec.Sum0:=taSpecSum0.AsFloat;
    RecSpec.NDSProc:=taSpecNDSProc.AsFloat;


    iMax:=1;
    ViewDoc1.BeginUpdate;

    taSpec.First;
    if not taSpec.Eof then
    begin
      taSpec.Last;
      iMax:=taSpecNum.AsInteger+1;
    end;

    taSpec.Append;
    taSpecNum.AsInteger:=iMax;
    taSpecIdGoods.AsInteger:=RecSpec.IdGoods;
    taSpecNameG.AsString:=RecSpec.NameG;
    taSpecIM.AsInteger:=RecSpec.IM;
    taSpecSM.AsString:=RecSpec.SM;
    taSpecKm.AsFloat:=RecSpec.Km;
    taSpecQuant.AsFloat:=RecSpec.Quant;
    taSpecQuantRemn.AsFloat:=RecSpec.QuantRemn;
    taSpecPrice1.AsFloat:=RecSpec.Price1;
    taSpecSum1.AsFloat:=RecSpec.Sum1;
    taSpecPrice2.AsFloat:=RecSpec.Price2;
    taSpecSum2.AsFloat:=RecSpec.Sum2;
    taSpecINds.AsInteger:=RecSpec.iNds;
    taSpecSNds.AsString:=RecSpec.sNds;
    taSpecRNds.AsFloat:=RecSpec.rNds;
    taSpecSumNac.AsFloat:=RecSpec.SumNac;
    taSpecProcNac.AsFloat:=RecSpec.ProcNac;
    taSpecTCard.AsInteger:=RecSpec.TCard;
    taSpecCType.AsInteger:=RecSpec.CType;
    taSpecNDSProc.AsFloat:=RecSpec.NDSProc;
    taSpecPrice0.AsFloat:=RecSpec.Price0;
    taSpecSum0.AsFloat:=RecSpec.Sum0;
    taSpec.Post;
    ViewDoc1.EndUpdate;
    GridDoc1.SetFocus;
  end;
end;

procedure TfmAddDoc1.cxLabel6Click(Sender: TObject);
begin
  acDelall.Execute;
end;

procedure TfmAddDoc1.cxDateEdit1PropertiesChange(Sender: TObject);
begin
  cxDateEdit2.EditValue:=cxDateEdit1.EditValue;
end;

procedure TfmAddDoc1.ViewDoc1SMPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
Var rK:Real;
    iM:Integer;
    Sm:String;
begin
  with dmO do
  begin
    if taSpecIm.AsInteger>0 then
    begin
      bAddSpec:=True;
      fmMessure.ShowModal;
      if fmMessure.ModalResult=mrOk then
      begin
        iM:=iMSel; iMSel:=0;
        Sm:=prFindKNM(iM,rK); //�������� ����� �������� � ����� �����

        taSpec.Edit;
        taSpecIM.AsInteger:=iM;
        taSpecKm.AsFloat:=rK;
        taSpecSM.AsString:=Sm;
        taSpec.Post;
      end;
    end;
  end;
end;

procedure TfmAddDoc1.cxButton2Click(Sender: TObject);
begin
  Close;
end;

procedure TfmAddDoc1.acSaveDocExecute(Sender: TObject);
begin
  if cxButton1.Enabled then cxButton1.Click;
end;

procedure TfmAddDoc1.acExitDocExecute(Sender: TObject);
begin
  cxButton2.Click;
end;

procedure TfmAddDoc1.ViewTaraDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept:=False;
  if bDrIn then  Accept:=True;
end;

procedure TfmAddDoc1.cxButtonEdit1KeyPress(Sender: TObject; var Key: Char);
Var sName:String;
begin
  sName:=cxButtonEdit1.Text+Key;
  if Length(sName)>1 then
  begin
    with dmORep do
    begin
      quFindCli.Close;
      quFindCli.SelectSQL.Clear;
      quFindCli.SelectSQL.Add('SELECT first 2 ID,NAMECL');
      quFindCli.SelectSQL.Add('FROM OF_CLIENTS');
      quFindCli.SelectSQL.Add('where UPPER(NAMECL) like ''%'+AnsiUpperCase(sName)+'%''');
      quFindCli.Open;
      if quFindCli.RecordCount=1 then
      begin
        cxButtonEdit1.Text:=quFindCliNAMECL.AsString;
        cxButtonEdit1.Tag:=quFindCliID.AsInteger;
        Key:=#0;
      end;
      quFindCli.Close;
    end;
  end;
end;

procedure TfmAddDoc1.Excel1Click(Sender: TObject);
begin
  prNExportExel5(ViewDoc1);
end;

procedure TfmAddDoc1.acMovePosExecute(Sender: TObject);
Var iDateB,iDateE,iM:INteger;
    IdCard:INteger;
    NameC:String;
    iMe:INteger;
begin
  //�������� �� ������
  IdCard:=taSpecIdGoods.AsInteger;
  NameC:=taSpecNameG.AsString;
  iMe:=taSpecIM.AsInteger;
  with dmO do
  begin
    if taSpec.RecordCount>0 then
    begin
      //�� ������
      fmSelPerSkl:=tfmSelPerSkl.Create(Application);
      with fmSelPerSkl do
      begin
        cxDateEdit1.Date:=CommonSet.DateFrom;
        quMHAll1.Active:=False;
        quMHAll1.ParamByName('IDPERSON').AsInteger:=Person.Id;
        quMHAll1.Active:=True;
        quMHAll1.First;
        if CommonSet.IdStore>0 then cxLookupComboBox1.EditValue:=CommonSet.IdStore
        else  cxLookupComboBox1.EditValue:=quMHAll1ID.AsInteger;
      end;
      fmSelPerSkl.ShowModal;
      if fmSelPerSkl.ModalResult=mrOk then
      begin
        iDateB:=Trunc(fmSelPerSkl.cxDateEdit1.Date);
        iDateE:=Trunc(fmSelPerSkl.cxDateEdit2.Date)+1;

        CommonSet.IdStore:=fmSelPerSkl.cxLookupComboBox1.EditValue;
        CommonSet.NameStore:=fmSelPerSkl.cxLookupComboBox1.Text;
        CommonSet.DateFrom:=iDateB;
        CommonSet.DateTo:=iDateE+1;

        CardSel.Id:=IdCard;
        CardSel.Name:=NameC;
        prFindSM(iMe,CardSel.mesuriment,iM);
//        CardSel.NameGr:=ClassTree.Selected.Text;

        prPreShowMove(IdCard,iDateB,iDateE,fmSelPerSkl.cxLookupComboBox1.EditValue,NameC);
        fmSelPerSkl.Release;

//        fmCardsMove.PageControl1.ActivePageIndex:=2;
        fmCardsMove.ShowModal;

      end else fmSelPerSkl.Release;
    end;
  end;
end;

procedure TfmAddDoc1.ViewDoc1CustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);
Var iType,i:Integer;
begin

  iType:=0;
  for i:=0 to ViewDoc1.ColumnCount-1 do
  begin
    if ViewDoc1.Columns[i].Name='ViewDoc1TCard' then
    begin
      iType:=VarAsType(AViewInfo.GridRecord.DisplayTexts[i], varInteger);
      break;
    end;
  end;

  if iType=1 then
  begin
    ACanvas.Canvas.Font.Color := clHotLight;
    ACanvas.Canvas.Font.Style :=[fsBold];
  end;
end;

procedure TfmAddDoc1.taSpecQuantChange(Sender: TField);
begin
  //���������� �����
  if iCol=1 then
  begin
    taSpecSum0.AsFloat:=taSpecPrice0.AsFloat*taSpecQuant.AsFloat;
    taSpecSum1.AsFloat:=taSpecPrice1.AsFloat*taSpecQuant.AsFloat;
    taSpecPrice2.AsFloat:=taSpecPrice1.AsFloat;
    taSpecSum2.AsFloat:=taSpecSum1.AsFloat;
    taSpecSumNac.AsFloat:=0;

    if taSpecNDSProc.AsFloat<1 then
    begin
      taSpecSum0.AsFloat:=taSpecSum1.AsFloat;
      taSpecPrice0.AsFloat:=taSpecPrice1.AsFloat;
    end;

    taSpecRNds.AsFloat:=taSpecSum1.AsFloat-taSpecSum0.AsFloat;
  end;
end;

procedure TfmAddDoc1.taSpecPrice1Change(Sender: TField);
Var rQ:Real;
begin
  //���������� ���� ������ � ���
  if iCol=2 then
  begin
    rQ:=taSpecQuant.AsFloat;
    if abs(rQ)>0 then
    begin

      taSpecSum1.AsFloat:=rv(taSpecPrice1.AsFloat*rQ);
      taSpecSum0.AsFloat:=rv(taSpecSum1.AsFloat*100/(100+taSpecNDSProc.AsFloat));
      taSpecPrice0.AsFloat:=taSpecSum0.AsFloat/rQ;

      taSpecPrice2.AsFloat:=taSpecPrice1.AsFloat;
      taSpecSum2.AsFloat:=taSpecSum1.AsFloat;
      taSpecSumNac.AsFloat:=0;

      if taSpecNDSProc.AsFloat<1 then
      begin
        taSpecSum0.AsFloat:=taSpecSum1.AsFloat;
        taSpecPrice0.AsFloat:=taSpecPrice1.AsFloat;
      end;

      taSpecRNds.AsFloat:=taSpecSum1.AsFloat-taSpecSum0.AsFloat;
    end;
  end;
end;

procedure TfmAddDoc1.taSpecSum1Change(Sender: TField);
Var rQ:Real;
begin
  if iCol=3 then
  begin
    rQ:=taSpecQuant.AsFloat;
    if abs(rQ)>0 then
    begin
      taSpecPrice1.AsFloat:=taSpecSum1.AsFloat/rQ;
      taSpecSum0.AsFloat:=rv(taSpecSum1.AsFloat*100/(100+taSpecNDSProc.AsFloat));
      taSpecPrice0.AsFloat:=taSpecSum0.AsFloat/rQ;

      taSpecPrice2.AsFloat:=taSpecPrice1.AsFloat;
      taSpecSum2.AsFloat:=taSpecSum1.AsFloat;
      taSpecSumNac.AsFloat:=0;

      if taSpecNDSProc.AsFloat<1 then
      begin
        taSpecSum0.AsFloat:=taSpecSum1.AsFloat;
        taSpecPrice0.AsFloat:=taSpecPrice1.AsFloat;
      end;

      taSpecRNds.AsFloat:=taSpecSum1.AsFloat-taSpecSum0.AsFloat;
    end;
  end;
end;

procedure TfmAddDoc1.taSpecPrice2Change(Sender: TField);
begin
  //���������� ���� ������� (�������)
  if iCol=4 then
  begin
    taSpecPrice2.AsFloat:=taSpecPrice1.AsFloat;
    taSpecSum2.AsFloat:=taSpecSum1.AsFloat;
  end;
end;

procedure TfmAddDoc1.taSpecSum2Change(Sender: TField);
begin
  if iCol=5 then
  begin
    taSpecPrice2.AsFloat:=taSpecPrice1.AsFloat;
    taSpecSum2.AsFloat:=taSpecSum1.AsFloat;
  end;
end;

procedure TfmAddDoc1.taSpecPrice0Change(Sender: TField);
Var rQ:Real;
begin
  //���������� ���� ��� ���
  if iCol=6 then
  begin
    rQ:=taSpecQuant.AsFloat;
    taSpecSum0.AsFloat:=rv(taSpecPrice0.AsFloat*rQ);
    taSpecPrice1.AsFloat:=taSpecPrice0.AsFloat*(100+taSpecNDSProc.AsFloat)/100;
    taSpecPrice2.AsFloat:=taSpecPrice1.AsFloat;

    taSpecSum1.AsFloat:=rv(taSpecPrice1.AsFloat*rQ);
    taSpecSumNac.AsFloat:=0;
    taSpecSum2.AsFloat:=taSpecSum1.AsFloat;
    taSpecRNds.AsFloat:=taSpecSum1.AsFloat-taSpecSum0.AsFloat;
  end;
end;

procedure TfmAddDoc1.taSpecSum0Change(Sender: TField);
Var rQ:Real;
begin
  //���������� ����� ��� ���
  if iCol=7 then
  begin
    rQ:=taSpecQuant.AsFloat;
    if abs(rQ)>0 then
    begin
      taSpecSum1.AsFloat:=taSpecSum0.AsFloat*(100+taSpecNDSProc.AsFloat)/100;
      taSpecPrice0.AsFloat:=taSpecSum0.AsFloat/rQ;
      taSpecPrice1.AsFloat:=taSpecSum1.AsFloat/rQ;
      taSpecPrice2.AsFloat:=taSpecPrice1.AsFloat;

      taSpecSumNac.AsFloat:=0;
      taSpecSum2.AsFloat:=taSpecSum1.AsFloat;
      taSpecRNds.AsFloat:=taSpecSum1.AsFloat-taSpecSum0.AsFloat;
    end;
  end;
end;

procedure TfmAddDoc1.taSpecNDSProcChange(Sender: TField);
Var rQ:Real;
begin
  //���������� ������� ���
  if iCol=8 then
  begin
    rQ:=taSpecQuant.AsFloat;
    if abs(rQ)>0 then
    begin
      taSpecSum0.AsFloat:=rv(taSpecSum1.AsFloat*100/(100+taSpecNDSProc.AsFloat));
      taSpecPrice0.AsFloat:=taSpecSum0.AsFloat/rQ;
      taSpecSumNac.AsFloat:=0;
      taSpecSum2.AsFloat:=taSpecSum1.AsFloat;
      taSpecRNds.AsFloat:=taSpecSum1.AsFloat-taSpecSum0.AsFloat;
    end;
  end;
end;

procedure TfmAddDoc1.taSpecRNdsChange(Sender: TField);
Var rQ:Real;
begin
//���������� ����� ���
  if iCol=9 then
  begin
    rQ:=taSpecQuant.AsFloat;
    if abs(rQ)>0 then
    begin
      taSpecSum0.AsFloat:=taSpecSum1.AsFloat-taSpecRNds.AsFloat;
      taSpecPrice0.AsFloat:=taSpecSum0.AsFloat/rQ;
      taSpecSumNac.AsFloat:=0;
      taSpecSum2.AsFloat:=taSpecSum1.AsFloat;
    end;
  end;
end;

procedure TfmAddDoc1.acSaveCodeCBExecute(Sender: TObject);
begin
  //��������� ������������ � ��
  if Label18.Tag=1 then
  begin
    if not CanDo('prSaveGDSPROCB') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;

    with dmCB do
    begin
      fmSt.Memo1.Clear;
      fmSt.cxButton1.Enabled:=False;
      fmSt.Show;
      fmSt.Memo1.Lines.Add('����� ���� ���������� ������������...'); delay(10);
      try
        msConnection.Connected:=False;
        Strwk:='FILE NAME='+CurDir+'Ecr.udl';
        msConnection.ConnectionString:=Strwk;
        delay(10);
        msConnection.Connected:=True;
        delay(10);
        if msConnection.Connected then
        begin
          fmSt.Memo1.Lines.Add('  ����� � �� ��.');

          ViewDoc1.BeginUpdate;
          taSpec.First;
          while not taSpec.Eof do
          begin
            if (taSpecIdGoods.AsInteger>0)and(taSpeciCodeCB.AsInteger>0)then
            begin
              quFCardPro.Active:=False;
              quFCardPro.Parameters.ParamByName('ICODE').Value:=taSpeciCodeCB.AsInteger;
              quFCardPro.Active:=True;
              if quFCardPro.RecordCount>0 then
              begin
                fmSt.Memo1.Lines.Add('     '+taSpecNameG.AsString+'( ��� '+taSpecIdGoods.AsString+') - ����������.');

                quFCardPro.Edit;
                quFCardProKb.asfloat:=taSpecKBCB.AsFloat;
                quFCardProiCodePro.AsInteger:=taSpecIdGoods.AsInteger;
                quFCardProiM.AsInteger:=taSpecIM.AsInteger;
                quFCardProKm.asfloat:=taSpecKm.AsFloat;
                quFCardProNamePro.AsString:=taSpecNameG.AsString;
                quFCardProSM.AsString:=taSpecSM.AsString;
                quFCardPro.Post;

              end else
              begin
                fmSt.Memo1.Lines.Add('     '+taSpecNameG.AsString+'( ��� '+taSpecIdGoods.AsString+') - ��������.');

                quFCardPro.Append;
                quFCardProiCode.AsInteger:=taSpeciCodeCB.AsInteger;
                quFCardProKb.asfloat:=taSpecKBCB.AsFloat;
                quFCardProiCodePro.AsInteger:=taSpecIdGoods.AsInteger;
                quFCardProiM.AsInteger:=taSpecIM.AsInteger;
                quFCardProKm.asfloat:=taSpecKm.AsFloat;
                quFCardProNamePro.AsString:=taSpecNameG.AsString;
                quFCardProSM.AsString:=taSpecSM.AsString;
                quFCardPro.Post;
              end;

              quFCardPro.Active:=False;
            end;
            taSpec.Next;
          end;
          ViewDoc1.EndUpdate;

        end;
      except
        fmSt.Memo1.Lines.Add('������ ����� � ��.');
        msConnection.Connected:=False;
      end;

      fmSt.Memo1.Lines.Add('C��������� ���������.'); delay(10);
      fmSt.cxButton1.Enabled:=True;
    end;
  end else Showmessage('�������� ��� ��������.');
end;

procedure TfmAddDoc1.ViewDoc1DblClick(Sender: TObject);
begin
  if (ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1NameG') then
  begin
    ViewDoc1NameG.Options.Editing:=True;
    ViewDoc1NameG.Focused:=True;
  end;  
end;

procedure TfmAddDoc1.acSelCodeCBExecute(Sender: TObject);
begin
  bResetCodeIn:=True;
  fmGoods.Show;
end;

end.
