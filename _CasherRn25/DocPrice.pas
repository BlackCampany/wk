unit DocPrice;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxImageComboBox, Menus, ExtCtrls,
  Placemnt, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxControls, cxGridCustomView, cxGrid,
  SpeedBar, cxContainer, cxTextEdit, cxMemo, ComCtrls, ActnList,
  XPStyleActnCtrls, ActnMan;

type
  TfmDocsPrice = class(TForm)
    StatusBar1: TStatusBar;
    Memo1: TcxMemo;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    SpeedItem6: TSpeedItem;
    SpeedItem7: TSpeedItem;
    SpeedItem8: TSpeedItem;
    SpeedItem9: TSpeedItem;
    SpeedItem10: TSpeedItem;
    GridDocsPrice: TcxGrid;
    ViewDocsPrice: TcxGridDBTableView;
    LevelDocsPrice: TcxGridLevel;
    FormPlacement1: TFormPlacement;
    Timer1: TTimer;
    PopupMenu2: TPopupMenu;
    N5: TMenuItem;
    ViewDocsPriceID: TcxGridDBColumn;
    ViewDocsPriceDDATE: TcxGridDBColumn;
    ViewDocsPriceCOMMENT: TcxGridDBColumn;
    amPr: TActionManager;
    acAddDocPr: TAction;
    acEditDoc: TAction;
    procedure N5Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SpeedItem1Click(Sender: TObject);
    procedure SpeedItem10Click(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
    procedure acAddDocPrExecute(Sender: TObject);
    procedure acEditDocExecute(Sender: TObject);
    procedure ViewDocsPriceCellDblClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure SpeedItem6Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmDocsPrice: TfmDocsPrice;

implementation

uses dmOffice, Un1, DMOReps, PeriodUni, AddDocPrice;

{$R *.dfm}

procedure TfmDocsPrice.N5Click(Sender: TObject);
begin
  dmO.ColorDialog1.Color:=SpeedBar1.Color;
  if dmO.ColorDialog1.Execute then
  begin
    SpeedBar1.Color := dmO.ColorDialog1.Color;
    StatusBar1.Color:= dmO.ColorDialog1.Color;
    UserColor.TTNPrice := dmO.ColorDialog1.Color;
    WriteColor;
  end;
end;

procedure TfmDocsPrice.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  Timer1.Enabled:=True;
  GridDocsPrice.Align:=AlClient;

  ViewDocsPrice.RestoreFromIniFile(CurDir+GridIni);

  StatusBar1.Color:= UserColor.TTnPrice;
  SpeedBar1.Color := UserColor.TTnPrice;
end;

procedure TfmDocsPrice.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  ViewDocsPrice.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmDocsPrice.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmDocsPrice.SpeedItem10Click(Sender: TObject);
begin
  prNExportExel5(ViewDocsPrice);
end;

procedure TfmDocsPrice.SpeedItem2Click(Sender: TObject);
begin
  fmPeriodUni.DateTimePicker1.Date:=CommonSet.DateFrom;
  fmPeriodUni.DateTimePicker2.Date:=CommonSet.DateTo-1;
  fmPeriodUni.ShowModal;
  if fmPeriodUni.ModalResult=mrOk then
  begin
    CommonSet.DateFrom:=Trunc(fmPeriodUni.DateTimePicker1.Date);
    CommonSet.DateTo:=Trunc(fmPeriodUni.DateTimePicker2.Date)+1;

    with dmORep do
    begin
      fmDocsPrice.ViewDocsPrice.BeginUpdate;
      quDocsPrice.Active:=False;
      quDocsPrice.ParamByName('DATEB').AsInteger:=Trunc(CommonSet.DateFrom);
      quDocsPrice.ParamByName('DATEE').AsInteger:=Trunc(CommonSet.DateTo+1);
      quDocsPrice.Active:=True;
      fmDocsPrice.ViewDocsPrice.EndUpdate;
    end;  

  end;
end;

procedure TfmDocsPrice.acAddDocPrExecute(Sender: TObject);
begin
  fmAddDocsPrice:=tfmAddDocsPrice.Create(Application);
  fmAddDocsPrice.cxDateEdit1.Date:=Date;
  fmAddDocsPrice.cxTextEdit3.Text:='';
  CloseTe(fmAddDocsPrice.taSpecPr);
  fmAddDocsPrice.cxTextEdit3.Tag:=0;

  fmAddDocsPrice.ShowModal;
  fmAddDocsPrice.Release;
end;

procedure TfmDocsPrice.acEditDocExecute(Sender: TObject);
begin
  with dmORep do
  begin
    if quDocsPrice.RecordCount>0 then
    begin
      fmAddDocsPrice:=tfmAddDocsPrice.Create(Application);
      fmAddDocsPrice.cxDateEdit1.Date:=quDocsPriceDDATE.AsDateTime;
      fmAddDocsPrice.cxTextEdit3.Text:=quDocsPriceCOMMENT.AsString;
      CloseTe(fmAddDocsPrice.taSpecPr);
      fmAddDocsPrice.cxTextEdit3.Tag:=quDocsPriceID.AsInteger;

      quSpecPrSel.Active:=False;
      quSpecPrSel.ParamByName('IDH').AsInteger:=quDocsPriceID.AsInteger;
      quSpecPrSel.Active:=True;

      quSpecPrSel.First;
      while not quSpecPrSel.Eof do
      begin
        with fmAddDocsPrice do
        begin
          taSpecPr.Append;
          taSpecPrIdGoods.AsInteger:=quSpecPrSelICODE.AsInteger;
          taSpecPrNameG.AsString:=quSpecPrSelNAME.AsString;
          taSpecPrIM.AsInteger:=quSpecPrSelIM.AsInteger;
          taSpecPrSM.AsString:=quSpecPrSelNAMESHORT.AsString;
          taSpecPrPriceOld.AsFloat:=quSpecPrSelOLDPRICE.AsFloat;
          taSpecPrPriceNew.AsFloat:=quSpecPrSelNEWPRICE.AsFloat;
          taSpecPrKM.AsFloat:=quSpecPrSelKM.AsFloat;
          taSpecPr.Post;
        end;
        quSpecPrSel.Next;
      end;

      quSpecPrSel.Active:=False;

      fmAddDocsPrice.ShowModal;
      fmAddDocsPrice.Release;
    end;
  end;
end;

procedure TfmDocsPrice.ViewDocsPriceCellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  acEditDoc.Execute;
end;

procedure TfmDocsPrice.SpeedItem6Click(Sender: TObject);
begin
  with dmORep do
  begin
    if quDocsPrice.RecordCount>0 then
    begin
      if MessageDlg('������� �������� ��������� ��������� ��� "'+quDocsPriceCOMMENT.AsString+'" �� '+quDocsPriceDDATE.AsString+'?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        quDelSpecPrice.ParamByName('IDH').AsInteger:=quDocsPriceID.AsInteger;
        quDelSpecPrice.ExecQuery;

        quDocsPrice.Delete;

        showmessage('�������� ��');
      end;
    end;
  end;
end;

end.
