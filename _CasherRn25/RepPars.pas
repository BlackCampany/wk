unit RepPars;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons, ExtCtrls,
  dxfProgressBar, cxControls, cxContainer, cxEdit, cxTextEdit, cxMaskEdit,
  cxDropDownEdit, cxCalendar, DB, FIBDataSet, pFIBDataSet, cxCheckBox,
  IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, IdFTP, cxMemo,
  cxGraphics, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox;

type
  TfmRepPars = class(TForm)
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Label2: TLabel;
    Label3: TLabel;
    cxDateEdit1: TcxDateEdit;
    cxDateEdit2: TcxDateEdit;
    cxCheckBox1: TcxCheckBox;
    cxCheckBox2: TcxCheckBox;
    cxCheckBox3: TcxCheckBox;
    cxCheckBox4: TcxCheckBox;
    cxCheckBox5: TcxCheckBox;
    Label4: TLabel;
    cxLookupComboBox2: TcxLookupComboBox;
    quMHAll1: TpFIBDataSet;
    quMHAll1ID: TFIBIntegerField;
    quMHAll1PARENT: TFIBIntegerField;
    quMHAll1ITYPE: TFIBIntegerField;
    quMHAll1NAMEMH: TFIBStringField;
    quMHAll1DEFPRICE: TFIBIntegerField;
    quMHAll1NAMEPRICE: TFIBStringField;
    dsMHAll1: TDataSource;
    cxCheckBox6: TcxCheckBox;
    procedure cxButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmRepPars: TfmRepPars;

implementation

uses Un1, u2fdk, dmOffice;

{$R *.dfm}

procedure TfmRepPars.cxButton1Click(Sender: TObject);
begin
  close;
end;

end.
