object fmAddDocsPrice: TfmAddDocsPrice
  Left = 365
  Top = 363
  Width = 899
  Height = 607
  Caption = 'fmAddDocsPrice'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 554
    Width = 891
    Height = 19
    Color = clWhite
    Panels = <
      item
        Width = 300
      end
      item
        Width = 50
      end>
  end
  object Panel2: TPanel
    Left = 0
    Top = 513
    Width = 891
    Height = 41
    Align = alBottom
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 1
    object cxButton1: TcxButton
      Left = 32
      Top = 8
      Width = 121
      Height = 25
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100'   Ctrl+S'
      TabOrder = 0
      OnClick = cxButton1Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 208
      Top = 8
      Width = 137
      Height = 25
      Action = acExit
      ModalResult = 2
      TabOrder = 1
      Glyph.Data = {
        5E040000424D5E04000000000000360000002800000012000000130000000100
        18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
        8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
        0000CED3D6848684848684848684848684848684848684848684848684848684
        848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
        7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
        FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
        FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
        007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
        FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
        00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
        494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
        00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000}
      LookAndFeel.Kind = lfOffice11
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 891
    Height = 49
    Align = alTop
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 2
    object Label1: TLabel
      Left = 24
      Top = 16
      Width = 68
      Height = 13
      Caption = #1044#1086#1082#1091#1084#1077#1085#1090' '#1086#1090' '
      Transparent = True
    end
    object Label16: TLabel
      Left = 256
      Top = 16
      Width = 50
      Height = 13
      Caption = #1054#1087#1080#1089#1072#1085#1080#1077
    end
    object cxDateEdit1: TcxDateEdit
      Left = 104
      Top = 12
      Style.BorderStyle = ebsOffice11
      TabOrder = 0
      Width = 121
    end
    object cxTextEdit3: TcxTextEdit
      Left = 316
      Top = 12
      Style.BorderStyle = ebsOffice11
      TabOrder = 1
      Text = 'cxTextEdit3'
      Width = 381
    end
    object cxButton3: TcxButton
      Left = 728
      Top = 8
      Width = 61
      Height = 33
      TabOrder = 2
      OnClick = cxButton3Click
      Glyph.Data = {
        86070000424D86070000000000003600000028000000180000001A0000000100
        1800000000005007000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0BDB2
        B3B0A7FFFFFFFFFFFFD7D7D7D7D7D7D7D7D7FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFB0AEA8989896838280898883A9A7A0B4B3AA93928D85848096948EAFADA5
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFA1A09BAEAEADC9C9C8ABABAA84848371706D767573A2A2A1A4
        A4A376767472716D93918CACAAA3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFB8B7B0A3A3A0D8D8D8E7E7E7D5D5D5C7C7C7A9A9A98585
        85A0A09FA8A8A8BABABAD7D7D7C3C3C38686856E6D6B8B8A85FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFA5A4A0C6C6C5F6F6F6F8F8F8E2E2E2D2D2D2
        C6C6C6AEAEAE787878777777929292A4A4A4B3B3B3C9C9C9E2E2E2D9D9D9A1A1
        A07D7C7AB0AEA8FFFFFFFFFFFFFFFFFFFFFFFFADADABE7E7E7FDFDFDFEFEFEF4
        F4F4E3E3E3D4D4D4C3C3C3B7B7B79E9E9E888888828282929292AAAAAABDBDBD
        C6C6C6BDBDBDA1A1A181817FBEBDB6FFFFFFFFFFFFFFFFFFBCBBB9FDFDFDFFFF
        FFFFFFFFFFFFFFF3F3F3D7D7D7C9C9C9BEBEBEB6B6B6C4C4C4D0D0D0C6C6C6AB
        ABAB9B9B9B9090905353537E7E7E7C7C7B9B9A96FFFFFFFFFFFFFFFFFFFFFFFF
        BCBCBAFFFFFFFFFFFFFBFBFBE9E9E9D7D7D7D4D4D4D7D7D7D1D1D1C8C8C8C0C0
        C0C3C3C3CECECEDBDBDBD9D9D9BBBBBB8D8D8D9F9D9E878686979692FFFFFFFF
        FFFFFFFFFFFFFFFFBDBDBBFDFDFDE9E9E9D8D8D8DEDEDEE4E4E4E0E0E0DBDBDB
        D6D6D6CFCFCFC9C9C9C2C2C2BBBBBBBABABAC1C1C1C9C9C9C5C1C448FF737D94
        87979692FFFFFFFFFFFFFFFFFFFFFFFFBEBEBBEAEAEAE5E5E5EEEEEEEBEBEBE3
        E3E3E7E7E7F1F1F1EAEAEADCDCDCCFCFCFC6C6C6BFBFBFB6B6B6B0B0B0B2B2B2
        B4B3B3A7B7AE8C9A93979592FFFFFFFFFFFFFFFFFFFFFFFFBFBEBDFEFEFEF7F7
        F7EEEEEEEBEBEBF2F2F2F9F9F9E6E6E6D8D8D8DCDCDCDCDCDCD5D5D5CCCCCCC0
        C0C0B7B7B7B3B3B3B2B2B2B6B2B4B2ADAFA09F9DFFFFFFFFFFFFFFFFFFFFFFFF
        C0C0C0FBFBFBF8F8F8F7F7F7FBFBFBF7F7F7DFDFDFD6D6D6E6E6E6E5E5E5E1E1
        E1DDDDDDD7D7D7D0D0D0C6C6C6BEBEBEB8B8B8B3B3B3B0B0B0B8B7B4FFFFFFFF
        FFFFFFFFFFFFFFFFDCDCDBDCDCDBF7F7F7F4F4F4E5E5E5D2D2D2D1D1D1E0E0E0
        E9E8E8F1F1F1F9F8F8FCFCFCFEFEFEF4F4F4EBEBEBDADADABABABAB3B3B3B1B1
        B0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE0DFDDCFCFCED5D6D7D7
        D8DAD7D9DBCED0D2CED0D2D5D5D7DCDDDDE9E9E9F3F4F4FFFFFFFBFBFBDADADA
        B3B3B3BABAB9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFDDDCDBEEECEBE6E1DADFD9CFD4D0CAC8C7C4BCBCBCB3B4B7B4B6BABEC1C2D3
        D3D3C3C2C2C1C1C0DBDBD9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFE7D5C6F4E5C7F4E0BEF2DDBBECD8B7E4D2B4D9C8
        B0CCBDABAFA9A6B2B2B2E0DFDDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2D7C3FFF5D3FFEBC7FFE9C1
        FFE6B8FFE3B1FFE3AFFED9AAAD9B95D7D7D5FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE5E1E0F7E1CEFF
        F4D9FFECCDFFEAC7FFE7C0FFE4B8FFE6B4FCD9ACA79B98FFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFDED3D0FEF0DFFFF4DCFFEFD3FFECCDFFE9C6FFE6C0FFEABBF3D2ADB1A8A6FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFE7D5D0FFFBECFFF4E1FFF1DBFFEED4FFEBCDFFE9C6FFEE
        C1DBBEA6DCDADAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFE6E3E3F7EBE5FFFEF4FFF5E7FFF3E1FFF1DA
        FFEED3FFEDCDFFEFC8C0A89EDCDADAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE4D9D9FFFEFDFFFEF9FF
        F9EFFFF6E8FFF3E1FFF0D9FFF4D6FAE5C8C2B5B3F3F3F3FFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEAD7
        D7FFFEFEFFFFFEFFFEF8FFFDF2FFFBECFFFCE9FFFBE0D9C2B9E9E5E5FFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFEFEEEAF5EEEEF1E8E8EDE1E1EAD9D8E5D1CEE5CCC7E3CBC6FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFF}
      LookAndFeel.Kind = lfOffice11
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 49
    Width = 153
    Height = 464
    Align = alLeft
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 3
    object cxLabel1: TcxLabel
      Left = 8
      Top = 32
      Cursor = crHandPoint
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1089#1087#1080#1089#1086#1082' Ctrl+Ins'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = 4227072
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel1Click
    end
    object cxLabel2: TcxLabel
      Left = 8
      Top = 72
      Cursor = crHandPoint
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1087#1086#1079#1080#1094#1080#1102'  F8'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clRed
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel2Click
    end
    object cxLabel5: TcxLabel
      Left = 8
      Top = 16
      Cursor = crHandPoint
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1087#1086#1079#1080#1094#1080#1102' Ins'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = 4227072
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel5Click
    end
    object cxLabel6: TcxLabel
      Left = 8
      Top = 88
      Cursor = crHandPoint
      Caption = #1054#1095#1080#1089#1090#1080#1090#1100' Ctrl+F8'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clRed
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel6Click
    end
    object cxLabel3: TcxLabel
      Left = 12
      Top = 152
      Cursor = crHandPoint
      Caption = #1047#1072#1087#1086#1083#1085#1080#1090#1100' '#1089#1090#1072#1088#1099#1077' '#1094#1077#1085#1099
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clRed
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.TextColor = clBlack
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel3Click
    end
    object cxLabel4: TcxLabel
      Left = 12
      Top = 180
      Cursor = crHandPoint
      Caption = #1047#1072#1087#1086#1083#1085#1080#1090#1100' '#1094#1077#1085#1099' '#1055#1055
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clRed
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.TextColor = clBlack
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel4Click
    end
  end
  object PageControl1: TPageControl
    Left = 160
    Top = 64
    Width = 669
    Height = 397
    ActivePage = TabSheet1
    Style = tsFlatButtons
    TabOrder = 4
    object TabSheet1: TTabSheet
      Caption = #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1103
      object GridAddDocPr: TcxGrid
        Left = 0
        Top = 0
        Width = 661
        Height = 366
        Align = alClient
        TabOrder = 0
        LookAndFeel.Kind = lfOffice11
        object ViewAddDocPr: TcxGridDBTableView
          PopupMenu = PopupMenu1
          NavigatorButtons.ConfirmDelete = False
          OnEditKeyDown = ViewAddDocPrEditKeyDown
          OnEditKeyPress = ViewAddDocPrEditKeyPress
          DataController.DataSource = dstaSpecPr
          DataController.Summary.DefaultGroupSummaryItems = <
            item
              Format = '0.00'
              Kind = skSum
              Position = spFooter
              FieldName = 'RNds'
            end
            item
              Format = '0.00'
              Kind = skSum
              Position = spFooter
              FieldName = 'Sum1'
            end
            item
              Format = '0.00'
              Kind = skSum
              Position = spFooter
              FieldName = 'Sum2'
            end
            item
              Format = '0.00'
              Kind = skSum
              Position = spFooter
              FieldName = 'SumNac'
            end
            item
              Format = '0.00'
              Kind = skSum
              Position = spFooter
              FieldName = 'Sum0'
            end>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = '0.00'
              Kind = skSum
              FieldName = 'RNds'
            end
            item
              Format = '0.00'
              Kind = skSum
              FieldName = 'Sum1'
            end
            item
              Format = '0.00'
              Kind = skSum
              FieldName = 'Sum2'
            end
            item
              Format = '0.00'
              Kind = skSum
              FieldName = 'SumNac'
            end
            item
              Format = ',0.0%;-,0.0%'
              Kind = skAverage
              FieldName = 'ProcNac'
            end
            item
              Format = '0.00'
              Kind = skSum
              FieldName = 'Sum0'
            end>
          DataController.Summary.SummaryGroups = <>
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsData.Inserting = False
          OptionsSelection.MultiSelect = True
          OptionsView.Footer = True
          OptionsView.GroupFooters = gfAlwaysVisible
          OptionsView.Indicator = True
          object ViewAddDocPrIdGoods: TcxGridDBColumn
            Caption = #1050#1086#1076' '#1090#1086#1074#1072#1088#1072
            DataBinding.FieldName = 'IdGoods'
          end
          object ViewAddDocPrNameG: TcxGridDBColumn
            Caption = #1053#1072#1079#1074#1072#1085#1080#1077
            DataBinding.FieldName = 'NameG'
            Width = 225
          end
          object ViewAddDocPrIM: TcxGridDBColumn
            Caption = #1050#1086#1076' '#1045#1076'.'#1080#1079#1084'.'
            DataBinding.FieldName = 'IM'
            Options.Editing = False
          end
          object ViewAddDocPrSM: TcxGridDBColumn
            Caption = #1045#1076'.'#1080#1079#1084'.'
            DataBinding.FieldName = 'SM'
            Width = 83
          end
          object ViewAddDocPrPriceOld: TcxGridDBColumn
            Caption = #1057#1090#1072#1088#1072#1103' '#1094#1077#1085#1072
            DataBinding.FieldName = 'PriceOld'
            Options.Editing = False
            Styles.Content = dmO.cxStyle25
          end
          object ViewAddDocPrPriceNew: TcxGridDBColumn
            Caption = #1053#1086#1074#1072#1103' '#1094#1077#1085#1072
            DataBinding.FieldName = 'PriceNew'
            Styles.Content = dmO.cxStyle25
          end
          object ViewAddDocPrPricePP: TcxGridDBColumn
            Caption = #1062#1077#1085#1072' '#1055#1055
            DataBinding.FieldName = 'PricePP'
            Options.Editing = False
          end
        end
        object LevelAddDocPr: TcxGridLevel
          GridView = ViewAddDocPr
        end
      end
    end
  end
  object taSpecPr: TdxMemData
    Indexes = <
      item
        FieldName = 'IdGoods'
        SortOptions = []
      end>
    SortOptions = []
    SortedField = 'IdGoods'
    Left = 276
    Top = 179
    object taSpecPrIdGoods: TIntegerField
      FieldName = 'IdGoods'
    end
    object taSpecPrNameG: TStringField
      FieldName = 'NameG'
      Size = 200
    end
    object taSpecPrIM: TIntegerField
      FieldName = 'IM'
    end
    object taSpecPrSM: TStringField
      FieldName = 'SM'
      Size = 50
    end
    object taSpecPrPriceOld: TFloatField
      FieldName = 'PriceOld'
      DisplayFormat = '0.00'
    end
    object taSpecPrPriceNew: TFloatField
      FieldName = 'PriceNew'
      DisplayFormat = '0.00'
    end
    object taSpecPrKM: TFloatField
      FieldName = 'KM'
    end
    object taSpecPrPricePP: TFloatField
      FieldName = 'PricePP'
      DisplayFormat = '0.00'
    end
    object taSpecPrPriceRemn: TFloatField
      FieldName = 'PriceRemn'
      DisplayFormat = '0.00'
    end
  end
  object dstaSpecPr: TDataSource
    DataSet = taSpecPr
    Left = 268
    Top = 248
  end
  object FormPlacement1: TFormPlacement
    Active = False
    Left = 440
    Top = 180
  end
  object amADPr: TActionManager
    Left = 352
    Top = 179
    StyleName = 'XP Style'
    object acExit: TAction
      Caption = #1042#1099#1093#1086#1076'   F10'
      ShortCut = 121
      OnExecute = acExitExecute
    end
    object acAddPos: TAction
      Caption = 'acAddPos'
      ShortCut = 45
      OnExecute = acAddPosExecute
    end
    object acDelPos: TAction
      Caption = 'acDelPos'
      ShortCut = 119
      OnExecute = acDelPosExecute
    end
    object acAddList: TAction
      Caption = 'acAddList'
      ShortCut = 16429
      OnExecute = acAddListExecute
    end
    object acDelAll: TAction
      Caption = 'acDelAll'
      ShortCut = 16503
      OnExecute = acDelAllExecute
    end
    object acSaveDoc: TAction
      Caption = 'acSaveDoc'
      ShortCut = 16467
      OnExecute = acSaveDocExecute
    end
    object acRecalcPrice: TAction
      Caption = #1055#1077#1088#1077#1089#1095#1080#1090#1072#1090#1100' '#1094#1077#1085#1099' '#1085#1072' '#1074#1099#1076#1077#1083#1077#1085#1085#1099#1077
      OnExecute = acRecalcPriceExecute
    end
    object acFillOld: TAction
      Caption = #1047#1072#1087#1086#1083#1085#1080#1090#1100' '#1089#1090#1072#1088#1099#1077
      OnExecute = acFillOldExecute
    end
    object acFillPricePP: TAction
      Caption = 'acFillPricePP'
      OnExecute = acFillPricePPExecute
    end
    object acSetPricePP: TAction
      Caption = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1079#1085#1072#1095#1077#1085#1080#1077' '#1094#1077#1085#1099' '#1080#1079' '#1094#1077#1085#1099' '#1055#1055
      OnExecute = acSetPricePPExecute
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 448
    Top = 247
    object N1: TMenuItem
      Action = acSetPricePP
    end
  end
  object frRepUstCost: TfrReport
    InitialZoom = pzDefault
    PreviewButtons = [pbZoom, pbLoad, pbSave, pbPrint, pbFind, pbHelp, pbExit]
    RebuildPrinter = False
    Left = 324
    Top = 311
    ReportForm = {19000000}
  end
  object frtaSpecPr: TfrDBDataSet
    DataSet = taSpecPr
    Left = 452
    Top = 315
  end
end
