object fmMoveSel: TfmMoveSel
  Left = 320
  Top = 349
  Width = 696
  Height = 480
  Caption = 'fmMoveSel'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 427
    Width = 688
    Height = 19
    Panels = <
      item
        Width = 300
      end
      item
        Width = 50
      end>
  end
  object GridMoveSel: TcxGrid
    Left = 16
    Top = 56
    Width = 661
    Height = 305
    TabOrder = 1
    LookAndFeel.Kind = lfOffice11
    object ViewMoveSel: TcxGridDBTableView
      OnDblClick = ViewMoveSelDblClick
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dmO.dsMoveSel
      DataController.Summary.DefaultGroupSummaryItems = <
        item
          Format = '0.000'
          Kind = skSum
          Position = spFooter
          FieldName = 'POSTIN'
          Column = ViewMoveSelPOSTIN
        end
        item
          Format = '0.000'
          Kind = skSum
          Position = spFooter
          FieldName = 'POSTOUT'
          Column = ViewMoveSelPOSTOUT
        end
        item
          Format = '0.000'
          Kind = skSum
          Position = spFooter
          FieldName = 'VNIN'
          Column = ViewMoveSelVNIN
        end
        item
          Format = '0.000'
          Kind = skSum
          Position = spFooter
          FieldName = 'INV'
          Column = ViewMoveSelINV
        end
        item
          Format = '0.000'
          Kind = skSum
          Position = spFooter
          FieldName = 'VNOUT'
          Column = ViewMoveSelVNOUT
        end
        item
          Format = '0.000'
          Kind = skSum
          Position = spFooter
          FieldName = 'QREAL'
          Column = ViewMoveSelQREAL
        end
        item
          Format = '0.000'
          Kind = skSum
          Position = spFooter
          FieldName = 'Itog'
          Column = ViewMoveSelItog
        end>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'POSTIN'
          Column = ViewMoveSelPOSTIN
        end
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'POSTOUT'
          Column = ViewMoveSelPOSTOUT
        end
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'VNIN'
          Column = ViewMoveSelVNIN
        end
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'VNOUT'
          Column = ViewMoveSelVNOUT
        end
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'INV'
          Column = ViewMoveSelINV
        end
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'QREAL'
          Column = ViewMoveSelQREAL
        end
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'Itog'
          Column = ViewMoveSelItog
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.MultiSelect = True
      OptionsView.Footer = True
      OptionsView.GroupFooters = gfAlwaysVisible
      OptionsView.Indicator = True
      object ViewMoveSelID: TcxGridDBColumn
        Caption = #1040#1088#1090#1080#1082#1091#1083
        DataBinding.FieldName = 'ID'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taCenter
        Properties.Alignment.Vert = taVCenter
      end
      object ViewMoveSelPARENT: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1075#1088#1091#1087#1087#1099
        DataBinding.FieldName = 'PARENT'
        Visible = False
      end
      object ViewMoveSelNAME: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'NAME'
        Styles.Content = dmO.cxStyle25
        Width = 132
      end
      object ViewMoveSelCATEGORY: TcxGridDBColumn
        Caption = #1058#1080#1087' '#1087#1086#1079#1080#1094#1080#1080
        DataBinding.FieldName = 'CATEGORY'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Items = <
          item
            Description = #1058#1086#1074#1072#1088
            ImageIndex = 0
            Value = 1
          end
          item
            Description = #1059#1089#1083#1091#1075#1072
            Value = 2
          end
          item
            Description = #1040#1074#1072#1085#1089
            Value = 3
          end
          item
            Description = #1058#1072#1088#1072
            Value = 4
          end>
        Styles.Content = dmO.cxStyle5
      end
      object ViewMoveSelIMESSURE: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1045#1076'.'#1080#1079#1084
        DataBinding.FieldName = 'IMESSURE'
        Visible = False
      end
      object ViewMoveSelMINREST: TcxGridDBColumn
        Caption = #1052#1080#1085'. '#1086#1089#1090#1072#1090#1086#1082
        DataBinding.FieldName = 'MINREST'
      end
      object ViewMoveSelTCARD: TcxGridDBColumn
        Caption = #1058#1077#1093#1085'. '#1082#1072#1088#1090#1072
        DataBinding.FieldName = 'TCARD'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Images = dmO.imState
        Properties.Items = <
          item
            Value = 0
          end
          item
            ImageIndex = 11
            Value = 1
          end>
      end
      object ViewMoveSelNAMECL: TcxGridDBColumn
        Caption = #1043#1088#1091#1087#1087#1072
        DataBinding.FieldName = 'NAMECL'
        Styles.Content = dmO.cxStyle25
        Width = 141
      end
      object ViewMoveSelNAMEMESSURE: TcxGridDBColumn
        Caption = #1045#1076'.'#1080#1079#1084
        DataBinding.FieldName = 'NAMEMESSURE'
        Width = 51
      end
      object ViewMoveSelKOEF: TcxGridDBColumn
        Caption = #1050#1086#1101#1092#1092'. '#1045#1076'.'#1048#1079#1084
        DataBinding.FieldName = 'KOEF'
        Visible = False
      end
      object ViewMoveSelPOSTIN: TcxGridDBColumn
        Caption = #1055#1088#1080#1093#1086#1076
        DataBinding.FieldName = 'POSTIN'
        Styles.Content = dmO.cxStyle25
      end
      object ViewMoveSelPOSTOUT: TcxGridDBColumn
        Caption = #1042#1086#1079#1074#1088#1072#1090
        DataBinding.FieldName = 'POSTOUT'
        Styles.Content = dmO.cxStyle25
      end
      object ViewMoveSelVNIN: TcxGridDBColumn
        Caption = #1042#1085'.'#1087#1088#1080#1093#1086#1076
        DataBinding.FieldName = 'VNIN'
        Styles.Content = dmO.cxStyle25
      end
      object ViewMoveSelVNOUT: TcxGridDBColumn
        Caption = #1042#1085'.'#1088#1072#1089#1093#1086#1076
        DataBinding.FieldName = 'VNOUT'
        Styles.Content = dmO.cxStyle25
      end
      object ViewMoveSelINV: TcxGridDBColumn
        Caption = #1048#1085#1074#1077#1085#1090#1072#1088#1080#1079#1072#1094#1080#1103
        DataBinding.FieldName = 'INV'
        Styles.Content = dmO.cxStyle25
      end
      object ViewMoveSelQREAL: TcxGridDBColumn
        Caption = #1056#1077#1072#1083#1080#1079#1086#1074#1072#1085#1086
        DataBinding.FieldName = 'QREAL'
        Styles.Content = dmO.cxStyle25
      end
      object ViewMoveSelItog: TcxGridDBColumn
        Caption = #1042#1089#1077#1075#1086
        DataBinding.FieldName = 'Itog'
        Styles.Content = dmO.cxStyle1
      end
    end
    object ViewRemn: TcxGridDBTableView
      OnDblClick = ViewRemnDblClick
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dsmeRemns
      DataController.Summary.DefaultGroupSummaryItems = <
        item
          Format = '0.000'
          Kind = skSum
          Position = spFooter
          FieldName = 'Quant'
          Column = ViewRemnQuant
        end
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'RSum'
          Column = ViewRemnRSum
        end>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'Quant'
          Column = ViewRemnQuant
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'RSum'
          Column = ViewRemnRSum
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.MultiSelect = True
      OptionsView.Footer = True
      OptionsView.GroupFooters = gfAlwaysVisible
      OptionsView.Indicator = True
      object ViewRemnRecId: TcxGridDBColumn
        DataBinding.FieldName = 'RecId'
        Visible = False
        IsCaptionAssigned = True
      end
      object ViewRemnArticul: TcxGridDBColumn
        Caption = #1050#1086#1076
        DataBinding.FieldName = 'Articul'
      end
      object ViewRemnName: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'Name'
        Styles.Content = dmO.cxStyle25
        Width = 146
      end
      object ViewRemnCType: TcxGridDBColumn
        Caption = #1058#1080#1087' '#1087#1086#1079#1080#1094#1080#1080
        DataBinding.FieldName = 'CType'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Items = <
          item
            Description = #1058#1086#1074#1072#1088
            ImageIndex = 0
            Value = 1
          end
          item
            Description = #1059#1089#1083#1091#1075#1080
            ImageIndex = 0
            Value = 2
          end
          item
            Description = #1040#1074#1072#1085#1089#1099
            ImageIndex = 0
            Value = 3
          end
          item
            Description = #1058#1072#1088#1072
            ImageIndex = 0
            Value = 4
          end>
        Styles.Content = dmO.cxStyle5
      end
      object ViewRemnGr: TcxGridDBColumn
        Caption = #1043#1088#1091#1087#1087#1072
        DataBinding.FieldName = 'Gr'
        Width = 132
      end
      object ViewRemnSGr: TcxGridDBColumn
        Caption = #1055#1086#1076#1075#1088#1091#1087#1087#1072
        DataBinding.FieldName = 'SGr'
        Width = 166
      end
      object ViewRemnIdM: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1077#1076'.'#1080#1079#1084'.'
        DataBinding.FieldName = 'IdM'
      end
      object ViewRemnSM: TcxGridDBColumn
        Caption = #1045#1076'. '#1080#1079#1084'.'
        DataBinding.FieldName = 'SM'
      end
      object ViewRemnTCard: TcxGridDBColumn
        Caption = #1058#1058#1050
        DataBinding.FieldName = 'TCard'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Images = dmO.imState
        Properties.Items = <
          item
            ImageIndex = 11
            Value = 1
          end>
      end
      object ViewRemnQuant: TcxGridDBColumn
        Caption = #1054#1089#1090#1072#1090#1086#1082
        DataBinding.FieldName = 'Quant'
        Styles.Content = dmO.cxStyle25
      end
      object ViewRemnRSum: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072
        DataBinding.FieldName = 'RSum'
        Styles.Content = dmO.cxStyle25
      end
    end
    object LevelMoveSel: TcxGridLevel
      GridView = ViewMoveSel
      Visible = False
    end
    object LevelRemn: TcxGridLevel
      GridView = ViewRemn
    end
  end
  object SpeedBar1: TSpeedBar
    Left = 0
    Top = 0
    Width = 688
    Height = 48
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Options = [sbAllowDrag, sbFlatBtns, sbTransparentBtns]
    BtnOffsetHorz = 4
    BtnOffsetVert = 4
    BtnWidth = 60
    BtnHeight = 40
    BevelInner = bvLowered
    Color = 16764134
    TabOrder = 2
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object SpeedItem1: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = 'SpeedItem1'
      Glyph.Data = {
        5E040000424D5E04000000000000360000002800000012000000130000000100
        18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
        8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
        0000CED3D6848684848684848684848684848684848684848684848684848684
        848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
        7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
        FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
        FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
        007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
        FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
        00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
        494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
        00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000}
      Hint = 'SpeedItem1|'
      Spacing = 1
      Left = 424
      Top = 4
      Visible = True
      OnClick = SpeedItem1Click
      SectionName = 'Untitled (0)'
    end
    object SpeedItem2: TSpeedItem
      Action = acPerSkl
      BtnCaption = #1055#1077#1088#1080#1086#1076
      Caption = #1055#1077#1088#1080#1086#1076
      Glyph.Data = {
        BA030000424DBA030000000000003600000028000000140000000F0000000100
        18000000000084030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00800000800000
        8000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFF008000008000008000FFFFFFFFFFFF008000000000000000FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFF000000000000008000FFFFFFFFFFFF008000000000FFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FF000000008000FFFFFFFFFFFF008000000000FFFFFFFFFFFFFFFFFF000000FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFF000000
        008000FFFFFFFFFFFF008000000000FFFFFFFFFFFF000000008000FFFFFFFFFF
        FF000000000000FFFFFFFFFFFF008000000000FFFFFFFFFFFF000000008000FF
        FFFFFFFFFF008000000000FFFFFF00000000800000E100000000000000000000
        00000000000000000000E100008000000000FFFFFF000000008000FFFFFFFFFF
        FF00800000000000000000800000E10000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000E100008000000000000000008000FFFFFFFFFFFF008000
        000000FFFFFF00000000800000E1000000000000000000000000000000000000
        0000E100008000000000FFFFFF000000008000FFFFFFFFFFFF008000000000FF
        FFFFFFFFFF000000008000FFFFFFFFFFFF000000000000FFFFFFFFFFFF008000
        000000FFFFFFFFFFFF000000008000FFFFFFFFFFFF008000000000FFFFFFFFFF
        FFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFF
        FFFFFFFFFF000000008000FFFFFFFFFFFF008000000000FFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FF000000008000FFFFFFFFFFFF008000000000000000FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000
        008000FFFFFFFFFFFF008000008000008000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF008000008000008000FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      Hint = #1055#1077#1088#1080#1086#1076
      Spacing = 1
      Left = 24
      Top = 4
      Visible = True
      OnClick = acPerSklExecute
      SectionName = 'Untitled (0)'
    end
    object SpeedItem3: TSpeedItem
      Action = acExportEx
      Glyph.Data = {
        56080000424D560800000000000036000000280000001A0000001A0000000100
        18000000000020080000C40E0000C40E00000000000000000000C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0004000004000004000004000C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C00000C0C0C0004000
        80808080808080808080808080808080808080808000400040E02040E020FFFF
        FFC8D0D4A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0808080C0
        C0C0C0C0C0C0C0C00000C0C0C0C0C0C000400040E02000C02000C02000C02000
        C02000400040E02040E020FFFFFF004000C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4A4A0A0A4A0A0404040C0C0C0C0C0C00000C0C0C0C0C0C0
        C0C0C000400000C02000C02000C02000400000C02040E020FFFFFF00400000C0
        20C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4A4A0A0A4A0A040
        2020808080C0C0C00000C0C0C0C0C0C0C0C0C0A4A0A000400000C02000400000
        C02040E020FFFFFF004000004000004000C8D0D4A4A0A0A4A0A0A4A0A0A4A0A0
        A4A0A0A4A0A0808080C8D0D4808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C000400040E02040E020FFFFFF004000F0FBFFF0FBFFF0FB
        FFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFA4A0A0C8D0D4C8D0D440
        2020808080C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C000400040E02040E020FF
        FFFF004000808080004000C8D0D4C0DCC0C0DCC0C0DCC0C0DCC0C0DCC0C0DCC0
        C0DCC0C8D0D4F0FBFFA4A0A0C8D0D4402020808080C0C0C00000C0C0C0C0C0C0
        C0C0C000400000C02040E020FFFFFF00400080E0A000C020808080004000FFFF
        FFFFFFFFFFFFFFF0FBFFC0DCC0C0DCC0F0FBFFC0DCC0F0FBFF808080C8D0D440
        2020808080C0C0C00000C0C0C0C0C0C000400000C02040E020FFFFFF004000FF
        FFFF00400000C02000C020808080004000FFFFFFFFFFFFC8D0D4F0FBFFC0DCC0
        C0DCC0C0DCC0F0FBFFC0DCC0A4A0A0404020808080C0C0C00000C0C0C0004000
        004000004000004000FFFFFFFFFFFFFFFFFFFFFFFF0040000040000040000040
        00C8D0D4F0FBFFC0DCC0C8D0D4C8D0D4F0FBFFC0DCC0F0FBFFFFFFFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFC0DCC0FFFFFFF0FBFFFFFFFFFFFFFFF0FBFFF0FBFFF0FBFF
        FFFFFFC0DCC0F0FBFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0F0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FB
        FFF0FBFFFFFFFFFFFFFFF0FBFFF0FBFFFFFFFFC0DCC0F0FBFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0F0
        FBFFF0FBFFF0FBFFC0DCC0C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4F0FBFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0C0DCC0C0DCC0F0FBFFF0FBFFC0DCC0FFFF
        FFC0DCC0F0FBFFC0DCC0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0C0
        DCC0F0FBFFF0FBFFF0FBFFF0FBFFFFFFFFF0FBFFF0FBFFC0DCC0F0FBFFFFFFFF
        FFFFFFFFFFFFFFFFFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0C0DCC0C8D0D4F0FBFFF0FBFFC8D0D4FFFF
        FFC8D0D4F0FBFFC0DCC0F0FBFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0F0
        FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFC8D0D4F0FBFFFFFFFF
        FFFFFFFFFFFFFFFFFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFF0FBFFC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4C8D0D4C8D0D4C8D0D4F0FBFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFF0FBFFF0
        FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080C0DCC0C8D0D4C8D0D4F0FBFFC8D0D4C0DCC0C0DCC0C8D0D4F0FB
        FFC8D0D4C0DCC0F0FBFFC8D0D4F0FBFFC8D0D4C8D0D4F0FBFFA4A0A080808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C080808080E0E040E0E040E0E080
        E0E040E0E080E0E080E0E040E0E080E0E040E0E040E0E080E0E040E0E080E0E0
        40E0E040E0E080E0E000E0E0408080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C080808080E0E080E0E0C0DCC080E0E0C0DCC080E0E080E0E0C0DCC080E0
        E0C0DCC080E0E080E0E0C0DCC080E0E0C0DCC080E0E080E0E0C0DCC000808000
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080F0FBFFF0FBFFF0FBFF80
        E0E0F0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFF
        F0FBFFF0FBFFF0FBFFF0FBFF00C0C0002040808080C0C0C00000C0C0C0C0C0C0
        C0C0C08080808080808080808080808080808080808080808080808080808080
        8080808080808080808080808080808080808080808080808080808080808000
        2040C0C0C0C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C00000}
      Hint = #1069#1082#1089#1087#1086#1088#1090' '#1074' Excel'
      Spacing = 1
      Left = 154
      Top = 4
      Visible = True
      OnClick = acExportExExecute
      SectionName = 'Untitled (0)'
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 368
    Width = 688
    Height = 59
    Align = alBottom
    BevelInner = bvLowered
    Color = 16764134
    TabOrder = 3
    object Memo1: TcxMemo
      Left = 2
      Top = 2
      Align = alLeft
      Lines.Strings = (
        'Memo1')
      Style.BorderStyle = ebsOffice11
      TabOrder = 0
      Height = 55
      Width = 231
    end
    object PBar1: TcxProgressBar
      Left = 376
      Top = 32
      ParentColor = False
      Position = 100.000000000000000000
      Properties.BarBevelOuter = cxbvRaised
      Properties.BarStyle = cxbsGradient
      Properties.BeginColor = clBlue
      Properties.EndColor = 16745090
      Properties.PeakValue = 100.000000000000000000
      Style.Color = clWhite
      TabOrder = 1
      Visible = False
      Width = 201
    end
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 3000
    OnTimer = Timer1Timer
    Left = 352
    Top = 184
  end
  object FormPlacement1: TFormPlacement
    Active = False
    Left = 432
    Top = 184
  end
  object amMoveSel: TActionManager
    Left = 92
    Top = 192
    StyleName = 'XP Style'
    object acExit: TAction
      Caption = 'acExit'
      OnExecute = acExitExecute
    end
    object acPerSkl: TAction
      Caption = #1055#1077#1088#1080#1086#1076
      OnExecute = acPerSklExecute
    end
    object acExportEx: TAction
      OnExecute = acExportExExecute
    end
    object acMovePos: TAction
      Caption = #1044#1074#1080#1078#1077#1085#1080#1077' '#1087#1086' '#1090#1086#1074#1072#1088#1091
      ShortCut = 32885
      OnExecute = acMovePosExecute
    end
  end
  object meRemns: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 188
    Top = 152
    object meRemnsArticul: TIntegerField
      FieldName = 'Articul'
    end
    object meRemnsName: TStringField
      FieldName = 'Name'
      Size = 100
    end
    object meRemnsGr: TStringField
      FieldName = 'Gr'
      Size = 50
    end
    object meRemnsSGr: TStringField
      FieldName = 'SGr'
      Size = 50
    end
    object meRemnsIdM: TIntegerField
      FieldName = 'IdM'
    end
    object meRemnsKM: TFloatField
      FieldName = 'KM'
    end
    object meRemnsSM: TStringField
      FieldName = 'SM'
      Size = 10
    end
    object meRemnsTCard: TIntegerField
      FieldName = 'TCard'
    end
    object meRemnsQuant: TFloatField
      FieldName = 'Quant'
      DisplayFormat = '0.000'
    end
    object meRemnsRSum: TFloatField
      FieldName = 'RSum'
      DisplayFormat = '0.00'
    end
    object meRemnsCType: TSmallintField
      FieldName = 'CType'
    end
  end
  object dsmeRemns: TDataSource
    DataSet = meRemns
    Left = 260
    Top = 152
  end
end
