unit MainAppend;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, cxLookAndFeelPainters, cxButtons, ExtCtrls,
  FIBDatabase, pFIBDatabase, DBTables, DB, FIBDataSet, pFIBDataSet,
  ComCtrls, Menus, cxControls, cxContainer, cxEdit, cxTextEdit, cxMaskEdit,
  cxDropDownEdit, cxCalendar;

type
  TfmMainAppend = class(TForm)
    Memo1: TMemo;
    cxButton1: TcxButton;
    Timer1: TTimer;
    dmC: TpFIBDatabase;
    trSelect: TpFIBTransaction;
    trUpdate: TpFIBTransaction;
    taCheck: TTable;
    Session1: TSession;
    taCheckUNI: TIntegerField;
    taCheckSys_Num: TIntegerField;
    taCheckCnum: TSmallintField;
    taCheckLogicDate: TDateField;
    taCheckRealDate: TDateField;
    taCheckOpenTime: TStringField;
    taCheckCloseTime: TStringField;
    taCheckCover: TSmallintField;
    taCheckCashier: TSmallintField;
    taCheckWaiter: TSmallintField;
    taCheckUnit: TStringField;
    taCheckDepart: TStringField;
    taCheckTotal: TFloatField;
    taCheckBaseKurs: TFloatField;
    taCheckDeleted: TSmallintField;
    taCheckManager: TSmallintField;
    taCheckCharge: TFloatField;
    taCheckTable: TStringField;
    taCheckOpenDate: TDateField;
    taCheckCount: TSmallintField;
    taCheckNacKurs: TFloatField;
    taCheckTotalR: TFloatField;
    taCheckCoverR: TSmallintField;
    taCheckTaxSum: TFloatField;
    taCheckTaxSumR: TFloatField;
    taCheckTaxRate: TFloatField;
    taCheckTaxRateR: TFloatField;
    taCheckBonus: TFloatField;
    taCheckBonusCard: TFloatField;
    taRCheck: TTable;
    taDCheck: TTable;
    taDCheckUNI: TIntegerField;
    taDCheckSys_Num: TIntegerField;
    taDCheckCnum: TSmallintField;
    taDCheckSifr: TSmallintField;
    taDCheckSum: TFloatField;
    taDCheckCardCod: TFloatField;
    taDCheckPerson: TSmallintField;
    taDCheckSumR: TFloatField;
    taPCheck: TTable;
    taVCheck: TTable;
    taCashSail: TpFIBDataSet;
    Timer2: TTimer;
    ProgressBar1: TProgressBar;
    taSpec: TpFIBDataSet;
    taSpecID_TAB: TFIBIntegerField;
    taSpecID: TFIBIntegerField;
    taSpecID_PERSONAL: TFIBIntegerField;
    taSpecNUMTABLE: TFIBStringField;
    taSpecSIFR: TFIBIntegerField;
    taSpecPRICE: TFIBFloatField;
    taSpecQUANTITY: TFIBFloatField;
    taSpecDISCOUNTPROC: TFIBFloatField;
    taSpecDISCOUNTSUM: TFIBFloatField;
    taSpecSUMMA: TFIBFloatField;
    taSpecISTATUS: TFIBIntegerField;
    taSpecITYPE: TFIBSmallIntField;
    taRCheckUNI: TIntegerField;
    taRCheckSys_Num: TIntegerField;
    taRCheckCnum: TSmallintField;
    taRCheckSifr: TSmallintField;
    taRCheckQnt: TFloatField;
    taRCheckPrice: TFloatField;
    taRCheckComp: TStringField;
    taRCheckRealPrice: TFloatField;
    taRCheckQntR: TFloatField;
    taRCheckNalog: TFloatField;
    taVCheckUNI: TAutoIncField;
    taVCheckLogicDate: TDateField;
    taVCheckRealDate: TDateField;
    taVCheckTime: TStringField;
    taVCheckSifr: TSmallintField;
    taVCheckComp: TSmallintField;
    taVCheckQnt: TFloatField;
    taVCheckPrice: TFloatField;
    taVCheckReason: TSmallintField;
    taVCheckManager: TSmallintField;
    taVCheckWaiter: TSmallintField;
    taVCheckTable: TStringField;
    taVCheckUnit: TStringField;
    taVCheckDepart: TStringField;
    taVCheckTabNum: TFloatField;
    taDel: TpFIBDataSet;
    taDelID: TFIBIntegerField;
    taDelID_PERSONAL: TFIBIntegerField;
    taDelNUMTABLE: TFIBStringField;
    taDelQUESTS: TFIBIntegerField;
    taDelTABSUM: TFIBFloatField;
    taDelBEGTIME: TFIBDateTimeField;
    taDelENDTIME: TFIBDateTimeField;
    taDelDISCONT: TFIBStringField;
    taDelOPERTYPE: TFIBStringField;
    taDelCHECKNUM: TFIBIntegerField;
    taDelSKLAD: TFIBSmallIntField;
    taMenu: TTable;
    taMenuDel: TTable;
    taMenuDelSifr: TSmallintField;
    taMenuDelName: TStringField;
    taMenuDelCode: TStringField;
    taMenuDelCateg: TSmallintField;
    taMenuDelReplace: TSmallintField;
    taMod: TTable;
    taModSifr: TSmallintField;
    taModName: TStringField;
    taModParent: TSmallintField;
    taModPrice: TFloatField;
    taModRealPrice: TFloatField;
    quMenu: TpFIBDataSet;
    quMod: TpFIBDataSet;
    quModSIFR: TFIBIntegerField;
    quModNAME: TFIBStringField;
    quModPARENT: TFIBIntegerField;
    quModPRICE: TFIBFloatField;
    quModREALPRICE: TFIBFloatField;
    quModIACTIVE: TFIBSmallIntField;
    quModIEDIT: TFIBSmallIntField;
    quMenuSIFR: TFIBIntegerField;
    quMenuNAME: TFIBStringField;
    quMenuPRICE: TFIBFloatField;
    quMenuCODE: TFIBStringField;
    quMenuTREETYPE: TFIBStringField;
    quMenuLIMITPRICE: TFIBFloatField;
    quMenuCATEG: TFIBSmallIntField;
    quMenuPARENT: TFIBSmallIntField;
    quMenuLINK: TFIBSmallIntField;
    quMenuSTREAM: TFIBSmallIntField;
    quMenuLACK: TFIBSmallIntField;
    quMenuDESIGNSIFR: TFIBSmallIntField;
    quMenuALTNAME: TFIBStringField;
    quMenuNALOG: TFIBFloatField;
    quMenuBARCODE: TFIBStringField;
    quMenuIMAGE: TFIBSmallIntField;
    quMenuCONSUMMA: TFIBFloatField;
    quMenuMINREST: TFIBSmallIntField;
    quMenuPRNREST: TFIBSmallIntField;
    quMenuCOOKTIME: TFIBSmallIntField;
    quMenuDISPENSER: TFIBSmallIntField;
    quMenuDISPKOEF: TFIBSmallIntField;
    quMenuACCESS: TFIBSmallIntField;
    quMenuFLAGS: TFIBSmallIntField;
    quMenuTARA: TFIBSmallIntField;
    quMenuCNTPRICE: TFIBSmallIntField;
    quMenuBACKBGR: TFIBFloatField;
    quMenuFONTBGR: TFIBFloatField;
    quMenuIACTIVE: TFIBSmallIntField;
    quMenuIEDIT: TFIBSmallIntField;
    taMenuSifr: TSmallintField;
    taMenuName: TStringField;
    taMenuPrice: TFloatField;
    taMenuCode: TStringField;
    taMenuTreeType: TStringField;
    taMenuLimitPrice: TFloatField;
    taMenuCateg: TSmallintField;
    taMenuParent: TSmallintField;
    taMenuLink: TSmallintField;
    taMenuStream: TSmallintField;
    taMenuLack: TSmallintField;
    taMenuDesignSifr: TSmallintField;
    taMenuAltName: TStringField;
    taMenuNalog: TFloatField;
    taMenuBarcode: TStringField;
    taMenuImage: TSmallintField;
    taMenuConsumma: TFloatField;
    taMenuMinRest: TSmallintField;
    taMenuPrnRest: TSmallintField;
    taMenuCookTime: TSmallintField;
    taMenuDispenser: TSmallintField;
    taMenuDispKoef: TSmallintField;
    taMenuAccess: TSmallintField;
    taCateg: TTable;
    taCategSifr: TSmallintField;
    taCategName: TStringField;
    taCategImage: TGraphicField;
    taCategTaxGroup: TSmallintField;
    quCateg: TpFIBDataSet;
    quCategSIFR: TFIBIntegerField;
    quCategNAME: TFIBStringField;
    quCategTAX_GROUP: TFIBIntegerField;
    quCategIACTIVE: TFIBSmallIntField;
    quCategIEDIT: TFIBSmallIntField;
    taFis: TpFIBDataSet;
    taFisID_TAB: TFIBIntegerField;
    taFisID: TFIBIntegerField;
    taFisNUMTABLE: TFIBStringField;
    taFisSIFR: TFIBIntegerField;
    taFisPRICE: TFIBFloatField;
    taFisQUANTITY: TFIBFloatField;
    taFisDISCOUNTPROC: TFIBFloatField;
    taFisDISCOUNTSUM: TFIBFloatField;
    taFisSUMMA: TFIBFloatField;
    taFisITYPE: TFIBSmallIntField;
    taFisQUANTITY1: TFIBFloatField;
    taFisENDTIME: TFIBDateTimeField;
    taFisID_PERSONAL: TFIBIntegerField;
    taPCheckUNI: TIntegerField;
    taPCheckSys_Num: TIntegerField;
    taPCheckCnum: TSmallintField;
    taPCheckCurency: TSmallintField;
    taPCheckBaseSumEqw: TFloatField;
    taPCheckOriginalSum: TFloatField;
    taPCheckKurs: TFloatField;
    taPCheckDisc: TFloatField;
    taPCheckExtra: TStringField;
    taPCheckBaseSumR: TFloatField;
    taPCheckOrigSumR: TFloatField;
    taPCheckIsTax: TSmallintField;
    taPC: TpFIBDataSet;
    taPCID: TFIBIntegerField;
    taPCID_PERSONAL: TFIBIntegerField;
    taPCNUMTABLE: TFIBStringField;
    taPCQUESTS: TFIBIntegerField;
    taPCTABSUM: TFIBFloatField;
    taPCBEGTIME: TFIBDateTimeField;
    taPCENDTIME: TFIBDateTimeField;
    taPCDISCONT: TFIBStringField;
    taPCOPERTYPE: TFIBStringField;
    taPCCHECKNUM: TFIBIntegerField;
    taPCSKLAD: TFIBSmallIntField;
    cxButton2: TcxButton;
    cxDateEdit1: TcxDateEdit;
    taCashSailCASHNUM: TFIBIntegerField;
    taCashSailZNUM: TFIBIntegerField;
    taCashSailCHECKNUM: TFIBIntegerField;
    taCashSailTAB_ID: TFIBIntegerField;
    taCashSailCASHERID: TFIBIntegerField;
    taCashSailWAITERID: TFIBIntegerField;
    taCashSailNUMTABLE: TFIBStringField;
    taCashSailQUESTS: TFIBIntegerField;
    taCashSailDISCONT: TFIBStringField;
    taCashSailTABSUM: TFIBFloatField;
    taCashSailCHDATE: TFIBDateTimeField;
    taCashSailBEGTIME: TFIBDateTimeField;
    taCashSailID: TFIBIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    Procedure prExportCash;
  end;

var
  fmMainAppend: TfmMainAppend;
  iProc:Integer;
  iCurR:Integer;

implementation

uses Un1;

{$R *.dfm}

Procedure TfmMainAppend.prExportCash;
Var iNum,iNumR,iNumD,iNumV,iNumP,iC:Integer;
    IdBeg,iUni:Integer;
    StrWk:String;
    rDSum,rPrice,rSum:Real;
    sCode:String;
    TimeShift,tDateB:TDateTime;
begin
  Memo1.Lines.Add('�������� ���� FB.');
  try
    dmC.Connected:=False;
    dmC.DatabaseName:=DBName;
    dmC.Connected:=True;
    if dmC.Connected=True then Memo1.Lines.Add('���������� ��.');
    Memo1.Lines.Add('�������� ���� Db.');
    Session1.Active:=False;
    Session1.AddPassword('767186508400000');
    Session1.Active:=True;

    taCheck.Active:=False;
    taCheck.DatabaseName:=CommonSet.PathRkData;
    taCheck.Active:=True;

    taRCheck.Active:=False;
    taRCheck.DatabaseName:=CommonSet.PathRkData;
    taRCheck.Active:=True;

    taPCheck.Active:=False;
    taPCheck.DatabaseName:=CommonSet.PathRkData;
    taPCheck.Active:=True;

    taDCheck.Active:=False;
    taDCheck.DatabaseName:=CommonSet.PathRkData;
    taDCheck.Active:=True;

    taVCheck.Active:=False;
    taVCheck.DatabaseName:=CommonSet.PathRkData;
    taVCheck.Active:=True;

    taMenu.Active:=False;
    taMenu.DatabaseName:=CommonSet.PathRkData;
    taMenu.Active:=True;

    taMod.Active:=False;
    taMod.DatabaseName:=CommonSet.PathRkData;
    taMod.Active:=True;

    taCateg.Active:=False;
    taCateg.DatabaseName:=CommonSet.PathRkData;
    taCateg.Active:=True;

    Memo1.Lines.Add('���� Db ��.');

    //���������
    Memo1.Lines.Add('�����. ���� �������� ������.');

    //����
    iNum:=1;
    iNumR:=1;
    iNumD:=1;
    iNumV:=1;
    IdBeg:=1;
    iNumP:=1;
    iUni:=1;
    iC:=1;
    TimeShift:=StrToTimeDef(CommonSet.ZTimeShift,0);

    taCheck.First;
    if not taCheck.Eof then
    begin
      taCheck.Last;
      iNum:=taCheckUNI.AsInteger+1;
      iDBeg:=RoundEx(taCheckBonusCard.AsFloat);
      iUni:=taCheckSys_Num.AsInteger+1;
    end;
    taPCheck.First;
    if not taPCheck.Eof then
    begin
      taPCheck.Last;
      iNumP:=taPCheckUNI.AsInteger+1;
    end;
    taRCheck.First;
    if not taRCheck.Eof then
    begin
      taRCheck.Last;
      iNumR:=taRCheckUNI.AsInteger+1;
    end;
    taDCheck.First;
    if not taDCheck.Eof then
    begin
      taDCheck.Last;
      iNumD:=taDCheckUNI.AsInteger+1;
    end;
    taVCheck.First;
    if not taVCheck.Eof then
    begin
      taVCheck.Last;
      iNumV:=taVCheckTabNum.AsInteger+1;
    end;

    taCashSail.Active:=False;
    taCashSail.ParamByName('IDBEG').AsInteger:=IdBeg;
    taCashSail.Active:=True;

    tDateB:=Now;
    taCashSail.First;
    if not taCashSail.Eof then tDateB:=taCashSailCHDATE.AsDateTime-0.0005;//������ ������
    taCashSail.Active:=False;

    taPC.Active:=False;
    taPC.ParamByName('DBEG').AsDateTime:=tDateB; //��� � ���� ����������
    taPC.Active:=True;
    taPC.First;
    while not taPC.Eof do
    begin
      taCheck.Append;
      taCheckUNI.AsInteger:=iNum;
      taCheckSys_Num.AsInteger:=iUni;
      taCheckCnum.AsInteger:=iC;
      taCheckLogicDate.AsDateTime:=Trunc(taPCENDTIME.AsDateTime-TimeShift);
      taCheckRealDate.AsDateTime:=Trunc(taPCENDTIME.AsDateTime);
      taCheckOpenTime.AsString:=FormatDateTime('hh:nn',taPCBEGTIME.AsDateTime);
      taCheckCloseTime.AsString:=FormatDateTime('hh:nn',taPCENDTIME.AsDateTime);
      taCheckCover.AsInteger:=taPCQUESTS.AsInteger;
      taCheckCashier.AsInteger:=taPCID_PERSONAL.AsInteger;
      taCheckWaiter.AsInteger:=taPCID_PERSONAL.AsInteger;
      taCheckUnit.AsString:=Copy(CommonSet.sUnit,1,2);
      StrWk:=CommonSet.DepartId;
      If Length(StrWk)<2 then StrWk:='0'+StrWk;
      taCheckDepart.AsString:=Copy(StrWk,1,2);
      taCheckTotal.AsFloat:=taPCTABSUM.AsFloat;
      taCheckBaseKurs.AsFloat:=1;
      taCheckDeleted.AsInteger:=0;
      taCheckManager.AsInteger:=0;
      StrWk:=taPCNUMTABLE.AsString;
      taCheckTable.AsString:=Copy(StrWk,1,4);
      taCheckOpenDate.AsDateTime:=Trunc(taPCBEGTIME.AsDateTime);
      taCheckCount.AsInteger:=29701;
      taCheckNacKurs.AsFloat:=1;
      taCheckTotalR.AsFloat:=taPCTABSUM.AsFloat;
      taCheckCoverR.AsInteger:=taPCQUESTS.AsInteger;
      taCheckTaxSum.AsFloat:=0;
      taCheckTaxSumR.AsFloat:=0;
      taCheckTaxRate.AsFloat:=0;
      taCheckTaxRateR.AsFloat:=0;
      taCheckBonus.AsFloat:=0;
      taCheckBonusCard.AsFloat:=0;
      taCheck.Post;

      taPCheck.Append;
      taPCheckUNI.AsInteger:=iNumP;
      taPCheckSys_Num.AsInteger:=iUni;
      taPCheckCnum.AsInteger:=iC;
      taPCheckCurency.AsInteger:=5;
      taPCheckBaseSumEqw.AsFloat:=taPCTABSUM.AsFloat;
      taPCheckOriginalSum.AsFloat:=taPCTABSUM.AsFloat;
      taPCheckKurs.AsFloat:=1;
      taPCheckDisc.AsFloat:=0;
      taPCheckExtra.AsString:='';
      taPCheckBaseSumR.AsFloat:=taPCTABSUM.AsFloat;
      taPCheckOrigSumR.AsFloat:=taPCTABSUM.AsFloat;
      taPCheckIsTax.AsInteger:=0;
      taPCheck.Post;

      rDSum:=0;
      taSpec.Active:=False;
      taSpec.ParamByName('IDHEAD').AsInteger:=taPCID.AsInteger;
      taSpec.Active:=True;
      taSpec.First;
      while not taSpec.Eof do
      begin
        taRCheck.Append;
        taRCheckUNI.AsInteger:=iNumR;
        taRCheckSys_Num.AsInteger:=iUni;
        taRCheckCnum.AsInteger:=iC;
        taRCheckSifr.AsInteger:=taSpecSIFR.AsInteger;
        taRCheckQnt.AsFloat:=taSpecQUANTITY.AsFloat;
        taRCheckPrice.AsFloat:=taSpecPRICE.AsFloat;
        if taSpecITYPE.AsInteger=0 then taRCheckComp.AsInteger:=1
        else taRCheckComp.AsInteger:=0;    //������������ ��� ���
        if taSpecQUANTITY.AsFloat<>0 then taRCheckRealPrice.AsFloat:=taSpecSUMMA.AsFloat/taSpecQUANTITY.AsFloat
        else taRCheckRealPrice.AsFloat:=taSpecPRICE.AsFloat;
        taRCheckQntR.AsFloat:=taSpecQUANTITY.AsFloat;
        taRCheckNalog.AsFloat:=0;
        taRCheck.Post;
        inc(iNumR);

        rDSum:=rDSum+taSpecDISCOUNTSUM.AsFloat;
        taSpec.Next;
      end;
      taSpec.Active:=False;

      if rDSum<>0 then
      begin
        taDCheck.Append;
        taDCheckUNI.AsInteger:=iNumD;
        taDCheckSys_Num.AsInteger:=iUni;
        taDCheckCnum.AsInteger:=iC;
        taDCheckSifr.AsInteger:=1;
        taDCheckSum.AsFloat:=-1*rDSum;
        StrWk:=taCashSailDISCONT.AsString;
        if length(StrWk)>=5 then StrWk:=Copy(StrWk,length(StrWk)-4,5);
        taDCheckCardCod.AsFloat:=StrToIntDef(StrWk,0);
        taDCheckPerson.AsInteger:=-1;
        taDCheckSumR.AsFloat:=-1*rDSum;
        taDCheck.Post;
        inc(iNumD);
      end;

      Inc(iNum); inc(iNumP); inc(iUni); inc(iC);

      taPC.Next;
    end;
    taPC.Active:=False;


    if CommonSet.SpecChar=1 then
    begin //����������� �����������   //(sp.QUANTITY1*sp.PRICE)*(100-DISCOUNTPROC)/100 as RSUM
      taCashSail.Active:=False;
      taCashSail.ParamByName('IDBEG').AsInteger:=IdBeg;
      taCashSail.Active:=True;

      taCashSail.First;

      ProgressBar1.Position:=0;
      ProgressBar1.Visible:=True;
      Timer2.Enabled:=True;
      iProc:=taCashSail.RecordCount;
      iCurR:=1;
      while not taCashSail.Eof do
      begin
        taFis.Active:=False;
        taFis.ParamByName('IDHEAD').AsInteger:=taCashSailTAB_ID.AsInteger;
        taFis.Active:=True;
        taFis.First;
        rSum:=0;
        while not taFis.Eof do
        begin
          if (taFisQUANTITY1.AsFloat>0)and(taFisPRICE.AsFloat>0) then
          begin
            rPrice:=taFisSUMMA.AsFloat/taFisQUANTITY.AsFloat;

            taRCheck.Append;
            taRCheckUNI.AsInteger:=iNumR;
            taRCheckSys_Num.AsInteger:=iUni;
            taRCheckCnum.AsInteger:=iC;
            taRCheckSifr.AsInteger:=taFisSIFR.AsInteger;
            taRCheckQnt.AsFloat:=taFisQUANTITY1.AsFloat;
            taRCheckPrice.AsFloat:=rPrice;
            taRCheckComp.AsInteger:=taFisITYPE.AsInteger;
            taRCheckRealPrice.AsFloat:=rPrice;
            taRCheckQntR.AsFloat:=taFisQUANTITY1.AsFloat;
            taRCheckNalog.AsFloat:=0;
            taRCheck.Post;
            inc(iNumR);

            rSum:=rSum+rPrice*taFisQUANTITY1.AsFloat;
          end;
          taFis.Next;
        end;

        if taFis.RecordCount>0 then
        begin  //���-�� ����
        // 1. �������� ���
        // 2. �������� ��� ������
          taCheck.Append;
          taCheckUNI.AsInteger:=iNum;
          taCheckSys_Num.AsInteger:=iUni;
          taCheckCnum.AsInteger:=iC;
          taCheckLogicDate.AsDateTime:=Trunc(taCashSailCHDATE.AsDateTime-TimeShift);
          taCheckRealDate.AsDateTime:=Trunc(taCashSailCHDATE.AsDateTime);
          taCheckOpenTime.AsString:=FormatDateTime('hh:nn',taCashSailBEGTIME.AsDateTime);
          taCheckCloseTime.AsString:=FormatDateTime('hh:nn',taCashSailCHDATE.AsDateTime);
          taCheckCover.AsInteger:=taCashSailQUESTS.AsInteger;
          taCheckCashier.AsInteger:=taCashSailCASHERID.AsInteger;
          taCheckWaiter.AsInteger:=taCashSailWAITERID.AsInteger;
          taCheckUnit.AsString:=Copy(CommonSet.sUnit,1,2);
          StrWk:=CommonSet.DepartId;
          If Length(StrWk)<2 then StrWk:='0'+StrWk;
          taCheckDepart.AsString:=Copy(StrWk,1,2);
          taCheckTotal.AsFloat:=rSum;
          taCheckBaseKurs.AsFloat:=1;
          taCheckDeleted.AsInteger:=0;
          taCheckManager.AsInteger:=0;
          StrWk:=taCashSailNUMTABLE.AsString;
          taCheckTable.AsString:=Copy(StrWk,1,4);
          taCheckOpenDate.AsDateTime:=Trunc(taCashSailCHDATE.AsDateTime);
          taCheckCount.AsInteger:=29701;
          taCheckNacKurs.AsFloat:=1;
          taCheckTotalR.AsFloat:=rSum;
          taCheckCoverR.AsInteger:=taCashSailQUESTS.AsInteger;
          taCheckTaxSum.AsFloat:=0;
          taCheckTaxSumR.AsFloat:=0;
          taCheckTaxRate.AsFloat:=0;
          taCheckTaxRateR.AsFloat:=0;
          taCheckBonus.AsFloat:=0;
          taCheckBonusCard.AsFloat:=taCashSailID.AsInteger;
          taCheck.Post;


          taPCheck.Append;
          taPCheckUNI.AsInteger:=iNumP;
          taPCheckSys_Num.AsInteger:=iUni;
          taPCheckCnum.AsInteger:=iC;
          taPCheckCurency.AsInteger:=6;
          taPCheckBaseSumEqw.AsFloat:=rSum;
          taPCheckOriginalSum.AsFloat:=rSum;
          taPCheckKurs.AsFloat:=1;
          taPCheckDisc.AsFloat:=0;
          taPCheckExtra.AsString:='';
          taPCheckBaseSumR.AsFloat:=rSum;
          taPCheckOrigSumR.AsFloat:=rSum;
          taPCheckIsTax.AsInteger:=0;
          taPCheck.Post;

          inc(iNum); inc(iNumP); inc(iUni); inc(iC);
        end;
        taFis.Active:=False;

        taCashSail.Next;
        inc(iCurR);
      end;
      taCashSail.Active:=False;
    end;

    taCashSail.Active:=False;
    taCashSail.ParamByName('IDBEG').AsInteger:=IdBeg;
    taCashSail.Active:=True;

    taCashSail.First;

    ProgressBar1.Position:=0;
    ProgressBar1.Visible:=True;
    Timer2.Enabled:=True;
    iProc:=taCashSail.RecordCount;
    iCurR:=1;
    while not taCashSail.Eof do
    begin
      taCheck.Append;
      taCheckUNI.AsInteger:=iNum;
      taCheckSys_Num.AsInteger:=iUni;
      taCheckCnum.AsInteger:=iC;
      taCheckLogicDate.AsDateTime:=Trunc(taCashSailCHDATE.AsDateTime-TimeShift);
      taCheckRealDate.AsDateTime:=Trunc(taCashSailCHDATE.AsDateTime);
      taCheckOpenTime.AsString:=FormatDateTime('hh:nn',taCashSailBEGTIME.AsDateTime);
      taCheckCloseTime.AsString:=FormatDateTime('hh:nn',taCashSailCHDATE.AsDateTime);
      taCheckCover.AsInteger:=taCashSailQUESTS.AsInteger;
      taCheckCashier.AsInteger:=taCashSailCASHERID.AsInteger;
      taCheckWaiter.AsInteger:=taCashSailWAITERID.AsInteger;
      taCheckUnit.AsString:=Copy(CommonSet.sUnit,1,2);
      StrWk:=CommonSet.DepartId;
      If Length(StrWk)<2 then StrWk:='0'+StrWk;
      taCheckDepart.AsString:=Copy(StrWk,1,2);
      taCheckTotal.AsFloat:=taCashSailTABSUM.AsFloat;
      taCheckBaseKurs.AsFloat:=1;
      taCheckDeleted.AsInteger:=0;
      taCheckManager.AsInteger:=0;
      StrWk:=taCashSailNUMTABLE.AsString;
      taCheckTable.AsString:=Copy(StrWk,1,4);
      taCheckOpenDate.AsDateTime:=Trunc(taCashSailCHDATE.AsDateTime);
      taCheckCount.AsInteger:=29701;
      taCheckNacKurs.AsFloat:=1;
      taCheckTotalR.AsFloat:=taCashSailTABSUM.AsFloat;
      taCheckCoverR.AsInteger:=taCashSailQUESTS.AsInteger;
      taCheckTaxSum.AsFloat:=0;
      taCheckTaxSumR.AsFloat:=0;
      taCheckTaxRate.AsFloat:=0;
      taCheckTaxRateR.AsFloat:=0;
      taCheckBonus.AsFloat:=0;
      taCheckBonusCard.AsFloat:=taCashSailID.AsInteger;
      taCheck.Post;

      taPCheck.Append;
      taPCheckUNI.AsInteger:=iNumP;
      taPCheckSys_Num.AsInteger:=iUni;
      taPCheckCnum.AsInteger:=iC;
      taPCheckCurency.AsInteger:=4;
      taPCheckBaseSumEqw.AsFloat:=taCashSailTABSUM.AsFloat;
      taPCheckOriginalSum.AsFloat:=taCashSailTABSUM.AsFloat;
      taPCheckKurs.AsFloat:=1;
      taPCheckDisc.AsFloat:=0;
      taPCheckExtra.AsString:='';
      taPCheckBaseSumR.AsFloat:=taCashSailTABSUM.AsFloat;
      taPCheckOrigSumR.AsFloat:=taCashSailTABSUM.AsFloat;
      taPCheckIsTax.AsInteger:=0;
      taPCheck.Post;


      rDSum:=0;
      taSpec.Active:=False;
      taSpec.ParamByName('IDHEAD').AsInteger:=taCashSailTAB_ID.AsInteger;
      taSpec.Active:=True;
      taSpec.First;
      while not taSpec.Eof do
      begin
        taRCheck.Append;
        taRCheckUNI.AsInteger:=iNumR;
        taRCheckSys_Num.AsInteger:=iUni;
        taRCheckCnum.AsInteger:=iC;
        taRCheckSifr.AsInteger:=taSpecSIFR.AsInteger;
        taRCheckQnt.AsFloat:=taSpecQUANTITY.AsFloat;
        taRCheckPrice.AsFloat:=taSpecPRICE.AsFloat;
        if taSpecITYPE.AsInteger=0 then taRCheckComp.AsInteger:=1
        else taRCheckComp.AsInteger:=0;    //������������ ��� ���
        if taSpecQUANTITY.AsFloat<>0 then taRCheckRealPrice.AsFloat:=taSpecSUMMA.AsFloat/taSpecQUANTITY.AsFloat
        else taRCheckRealPrice.AsFloat:=taSpecPRICE.AsFloat;
        taRCheckQntR.AsFloat:=taSpecQUANTITY.AsFloat;
        taRCheckNalog.AsFloat:=0;
        taRCheck.Post;
        inc(iNumR);

        rDSum:=rDSum+taSpecDISCOUNTSUM.AsFloat;
        taSpec.Next;
      end;
      taSpec.Active:=False;

      if rDSum<>0 then
      begin
        taDCheck.Append;
        taDCheckUNI.AsInteger:=iNumD;
        taDCheckSys_Num.AsInteger:=iUni;
        taDCheckCnum.AsInteger:=iC;
        taDCheckSifr.AsInteger:=1;
        taDCheckSum.AsFloat:=-1*rDSum;
        StrWk:=taCashSailDISCONT.AsString;
        if length(StrWk)>=5 then StrWk:=Copy(StrWk,length(StrWk)-4,5);
        taDCheckCardCod.AsFloat:=StrToIntDef(StrWk,0);
        taDCheckPerson.AsInteger:=-1;
        taDCheckSumR.AsFloat:=-1*rDSum;
        taDCheck.Post;
        inc(iNumD);
      end;

      Inc(iNum); inc(iNumP); inc(iUni); inc(iC);
      inc(iCurR);
      taCashSail.Next;
    end;
    //�� �������� ����������
    // ������ ����� ��������� VCheck
    // �������� ��� ���������
    taDel.Active:=False;
    taDel.ParamByName('IDBEG').AsInteger:=iNumV;
    taDel.Active:=True;
    taDel.First;
    while not taDel.Eof do
    begin
      taSpec.Active:=False;
      taSpec.ParamByName('IDHEAD').AsInteger:=taDelID.AsInteger;
      taSpec.Active:=True;
      taSpec.First;
      while not taSpec.Eof do
      begin
        if taDelOPERTYPE.AsString='Del' then
        begin
          taVCheck.Append;
          taVCheckLogicDate.AsDateTime:=Trunc(taDelENDTIME.AsDateTime-TimeShift);
          taVCheckRealDate.AsDateTime:=Trunc(taDelENDTIME.AsDateTime);
          taVCheckTime.AsString:=FormatDateTime('hh:nn',taDelENDTIME.AsDateTime);
          taVCheckSifr.AsInteger:=taSpecSIFR.AsInteger;
          taVCheckComp.AsInteger:=taSpecITYPE.AsInteger; //0-����� 1-���
          taVCheckQnt.AsFloat:=taSpecQUANTITY.AsFloat;
          taVCheckPrice.AsFloat:=taSpecPRICE.AsFloat;
          if taDelSKLAD.AsInteger=0 then taVCheckReason.AsInteger:=2;  //��������� ��������
          if taDelSKLAD.AsInteger=1 then taVCheckReason.AsInteger:=1;  //�� ��������� ��������
          taVCheckManager.AsInteger:=taDelID_PERSONAL.AsInteger;
          taVCheckWaiter.AsInteger:=taDelID_PERSONAL.AsInteger;
          if length(taDelNUMTABLE.AsString)>3 then
          taVCheckTable.AsString:=Copy(taDelNUMTABLE.AsString,1,4)
          else taVCheckTable.AsString:=taDelNUMTABLE.AsString;
          taVCheckUnit.AsString:=Copy(CommonSet.sUnit,1,2);
          StrWk:=CommonSet.DepartId;
          If Length(StrWk)<2 then StrWk:='0'+StrWk;
          taVCheckDepart.AsString:=Copy(StrWk,1,2);
          taVCheckTabNum.AsInteger:=taDelID.AsInteger;
          taVCheck.Post;
        end;

        taSpec.Next;
      end;
      taSpec.Active:=False;

      taDel.Next;
    end;
    taDel.Active:=False;

    Timer2.Enabled:=False;

    Memo1.Lines.Add('�������� ���������� ��.');
    ProgressBar1.Visible:=False;

    Memo1.Lines.Add('����. �������������.');

    quMenu.Active:=False;
    quMenu.Active:=True;
    quMenu.First;
    while not quMenu.Eof do
    begin

      if taMenu.Locate('Sifr',quMenuSIFR.AsInteger,[]) then taMenu.Edit
      else taMenu.Append;

      taMenuSifr.AsInteger:=quMenuSIFR.AsInteger;
      taMenuName.AsString:=Copy(quMenuNAME.AsString,1,25);
      taMenuPrice.AsFloat:=quMenuPRICE.AsFloat;
      sCode:=IntToStr(quMenuSIFR.AsInteger);
      while length(sCode)>4 do delete(sCode,1,1);
      taMenuCode.AsString:=sCode;
      taMenuTreeType.AsString:=quMenuTREETYPE.AsString;
      taMenuLimitPrice.AsFloat:=0;
      taMenuCateg.AsInteger:=1;
      taMenuParent.AsInteger:=quMenuPARENT.AsInteger;
      taMenuLink.AsInteger:=0;
      taMenuStream.AsInteger:=quMenuSTREAM.AsInteger;
      taMenuLack.AsInteger:=0;
      taMenuDesignSifr.AsInteger:=0;
      taMenuAltName.AsString:='';
      taMenuNalog.AsFloat:=0;
      taMenuBarcode.AsString:='';
      taMenuImage.AsInteger:=0;
      taMenuConsumma.AsFloat:=0;
      taMenuMinRest.AsInteger:=0;
      taMenuPrnRest.AsInteger:=0;
      taMenuCookTime.AsInteger:=0;
      taMenuDispenser.AsInteger:=0;
      taMenuDispKoef.AsInteger:=0;
      taMenuAccess.AsInteger:=0;
//      taMenuFlags.AsInteger:=4;
//      taMenuTara.AsInteger:=0;
//      taMenuCntPrice.AsInteger:=1;
//      taMenuBackBGR.AsFloat:=0;
//      taMenuFontBGR.AsFloat:=0;
//      taMenuTaxGroup.AsInteger:=0;
//      taMenuMinPrice.AsFloat:=0;
//      taMenuReccom.AsInteger:=0;
//      taMenuDateIncl.AsFloat:=0;
//      taMenuDateExcl.AsFloat:=0;
//      taMenuTimeIncl.AsInteger:=0;
//      taMenuTimeExcl.AsInteger:=0;
//      taMenuFWeek.AsInteger:=0;

      taMenu.Post;

      quMenu.Next;
    end;
    quMenu.Active:=False;


    Memo1.Lines.Add('��.'); Delay(10);

    Memo1.Lines.Add('������������. �������������.'); Delay(10);

    quMod.Active:=False;
    quMod.Active:=True;
    quMod.First;
    while not quMod.Eof do
    begin
      if taMod.Locate('Sifr',quModSIFR.AsInteger,[]) then taMod.Edit
      else taMod.Append;

      taModSifr.AsInteger:=quModSIFR.AsInteger;
      taModName.AsString:=quModNAME.AsString;
      taModParent.AsInteger:=quModPARENT.AsInteger;
      taModPrice.AsFloat:=quModPRICE.AsFloat;
      taModRealPrice.AsFloat:=quModREALPRICE.AsFloat;
      taMod.Post;

      quMod.Next;
    end;
    quMod.Active:=False;

    Memo1.Lines.Add('��.'); Delay(10);
    Memo1.Lines.Add('���������. �������������.'); Delay(10);

    quCateg.Active:=False;
    quCateg.Active:=True;
    quCateg.First;
    while not quCateg.Eof do
    begin
      if taCateg.Locate('Sifr',quCategSIFR.AsInteger,[]) then taCateg.Edit
      else taCateg.Append;

      taCategSifr.AsInteger:=quCategSIFR.AsInteger;
      taCategName.AsString:=quCategNAME.AsString;
      taCategTaxGroup.AsInteger:=quCategTAX_GROUP.AsInteger;
      taCateg.Post;

      quCateg.Next;
    end;
    quCateg.Active:=False;

    Memo1.Lines.Add('��.'); Delay(10);

  finally
    cxButton1.Enabled:=True;
//    Memo1.Lines.Add('�������� ���� RK.');
    taCheck.Active:=False;
    taRCheck.Active:=False;
    taPCheck.Active:=False;
    taDCheck.Active:=False;
    taVCheck.Active:=False;

    taMenu.Active:=False;
    taMod.Active:=False;
    taCateg.Active:=False;

    Memo1.Lines.Add('�������� ���� FB.');
    dmC.Connected:=False;
  end;
end;

procedure TfmMainAppend.FormCreate(Sender: TObject);
begin
  CurDir := ExtractFilePath(ParamStr(0));
  ReadIni;
  Memo1.Clear;
//  Timer1.Enabled:=True;
  cxDateEdit1.Date:=Date-1;
end;

procedure TfmMainAppend.Timer1Timer(Sender: TObject);
begin
//  Timer1.Enabled:=False;
//  cxButton1.Enabled:=False;
//  prExportCash;
end;

procedure TfmMainAppend.cxButton1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmMainAppend.Timer2Timer(Sender: TObject);
begin
  ProgressBar1.Position:=round(iCurR/iProc*100);
  delay(10);
end;

procedure TfmMainAppend.cxButton2Click(Sender: TObject);
Var StrWk:String;
    iDate:Integer;
    TimeShift:TDateTime;
    iNum,iNumR,iNumD,iNumP,iC,iUni:Integer;
    rDSum,rPrice,rSum:Real;
    sCode:String;
begin
  cxButton1.Enabled:=False;
  cxButton2.Enabled:=False;
  try
    //������� ������ ����
    TimeShift:=StrToTimeDef(CommonSet.ZTimeShift,0);
    iDate:=Trunc(cxDateEdit1.Date);

    dmC.Connected:=False;
    dmC.DatabaseName:=DBName;
    dmC.Connected:=True;
    if dmC.Connected=True then Memo1.Lines.Add('���������� ��.');

    Memo1.Lines.Add('�������� ���� Db.');
    Session1.Active:=False;
    Session1.AddPassword('767186508400000');
    Session1.Active:=True;

    taCheck.Active:=False;
    taCheck.DatabaseName:=CommonSet.PathRkData;
    taCheck.Active:=True;

    taRCheck.Active:=False;
    taRCheck.DatabaseName:=CommonSet.PathRkData;
    taRCheck.Active:=True;

    taPCheck.Active:=False;
    taPCheck.DatabaseName:=CommonSet.PathRkData;
    taPCheck.Active:=True;

    taDCheck.Active:=False;
    taDCheck.DatabaseName:=CommonSet.PathRkData;
    taDCheck.Active:=True;

    taVCheck.Active:=False;
    taVCheck.DatabaseName:=CommonSet.PathRkData;
    taVCheck.Active:=True;

    taMenu.Active:=False;
    taMenu.DatabaseName:=CommonSet.PathRkData;
    taMenu.Active:=True;

    taMod.Active:=False;
    taMod.DatabaseName:=CommonSet.PathRkData;
    taMod.Active:=True;

    taCateg.Active:=False;
    taCateg.DatabaseName:=CommonSet.PathRkData;
    taCateg.Active:=True;

    Memo1.Lines.Add('���� Db ��.');
    Memo1.Lines.Add('');
    StrWk:='������ c '+FormatDateTime('dd.mm.yyyy hh:nn',iDate+TimeShift)+ ' �� '+FormatDateTime('dd.mm.yyyy hh:nn',iDate+1+TimeShift);
    Memo1.Lines.Add(StrWk);
    Memo1.Lines.Add('');
    Memo1.Lines.Add('  ������ �������.');

    while taCheck.Locate('LogicDate',iDate,[]) do
    begin
      while taRCheck.Locate('Sys_Num',taCheckSys_Num.AsInteger,[]) do taRCheck.Delete;
      while taPCheck.Locate('Sys_Num',taCheckSys_Num.AsInteger,[]) do taPCheck.Delete;
      while taDCheck.Locate('Sys_Num',taCheckSys_Num.AsInteger,[]) do taDCheck.Delete;
      taCheck.Delete;
    end;
    while taVCheck.Locate('LogicDate',iDate,[]) do taVCheck.Delete;

    Memo1.Lines.Add('  ������ ��.');

//���������
    Memo1.Lines.Add('�����. ���� �������� ������.');

    //����
    iNum:=1;
    iNumR:=1;
    iNumD:=1;
    iNumP:=1;
    iUni:=1;
    iC:=1;

    taCheck.First;
    if not taCheck.Eof then
    begin
      taCheck.Last;
      iNum:=taCheckUNI.AsInteger+1;
      iUni:=taCheckSys_Num.AsInteger+1;
    end;
    taPCheck.First;
    if not taPCheck.Eof then
    begin
      taPCheck.Last;
      iNumP:=taPCheckUNI.AsInteger+1;
    end;
    taRCheck.First;
    if not taRCheck.Eof then
    begin
      taRCheck.Last;
      iNumR:=taRCheckUNI.AsInteger+1;
    end;
    taDCheck.First;
    if not taDCheck.Eof then
    begin
      taDCheck.Last;
      iNumD:=taDCheckUNI.AsInteger+1;
    end;

    taCashSail.Active:=False;
    taCashSail.ParamByName('DBEG').AsDateTime:=TimeShift+iDate;
    taCashSail.ParamByName('DEND').AsDateTime:=iDate+TimeShift+1;
    taCashSail.Active:=True;

    taPC.Active:=False;  //��������� �����
    taPC.ParamByName('DBEG').AsDateTime:=iDate+TimeShift;
    taPC.ParamByName('DEND').AsDateTime:=iDate+TimeShift+1;
    taPC.Active:=True;
    Memo1.Lines.Add('  PC - '+intToStr(taPC.RecordCount));

    taPC.First;
    while not taPC.Eof do
    begin
      taCheck.Append;
      taCheckUNI.AsInteger:=iNum;
      taCheckSys_Num.AsInteger:=iUni;
      taCheckCnum.AsInteger:=iC;
      taCheckLogicDate.AsDateTime:=Trunc(taPCENDTIME.AsDateTime-TimeShift);
      taCheckRealDate.AsDateTime:=Trunc(taPCENDTIME.AsDateTime);
      taCheckOpenTime.AsString:=FormatDateTime('hh:nn',taPCBEGTIME.AsDateTime);
      taCheckCloseTime.AsString:=FormatDateTime('hh:nn',taPCENDTIME.AsDateTime);
      taCheckCover.AsInteger:=taPCQUESTS.AsInteger;
      taCheckCashier.AsInteger:=taPCID_PERSONAL.AsInteger;
      taCheckWaiter.AsInteger:=taPCID_PERSONAL.AsInteger;
      taCheckUnit.AsString:=Copy(CommonSet.sUnit,1,2);
      StrWk:=CommonSet.DepartId;
      If Length(StrWk)<2 then StrWk:='0'+StrWk;
      taCheckDepart.AsString:=Copy(StrWk,1,2);
      taCheckTotal.AsFloat:=taPCTABSUM.AsFloat;
      taCheckBaseKurs.AsFloat:=1;
      taCheckDeleted.AsInteger:=0;
      taCheckManager.AsInteger:=0;
      StrWk:=taPCNUMTABLE.AsString;
      taCheckTable.AsString:=Copy(StrWk,1,4);
      taCheckOpenDate.AsDateTime:=Trunc(taPCBEGTIME.AsDateTime);
      taCheckCount.AsInteger:=29701;
      taCheckNacKurs.AsFloat:=1;
      taCheckTotalR.AsFloat:=taPCTABSUM.AsFloat;
      taCheckCoverR.AsInteger:=taPCQUESTS.AsInteger;
      taCheckTaxSum.AsFloat:=0;
      taCheckTaxSumR.AsFloat:=0;
      taCheckTaxRate.AsFloat:=0;
      taCheckTaxRateR.AsFloat:=0;
      taCheckBonus.AsFloat:=0;
      taCheckBonusCard.AsFloat:=0;
      taCheck.Post;

      taPCheck.Append;
      taPCheckUNI.AsInteger:=iNumP;
      taPCheckSys_Num.AsInteger:=iUni;
      taPCheckCnum.AsInteger:=iC;
      taPCheckCurency.AsInteger:=5;
      taPCheckBaseSumEqw.AsFloat:=taPCTABSUM.AsFloat;
      taPCheckOriginalSum.AsFloat:=taPCTABSUM.AsFloat;
      taPCheckKurs.AsFloat:=1;
      taPCheckDisc.AsFloat:=0;
      taPCheckExtra.AsString:='';
      taPCheckBaseSumR.AsFloat:=taPCTABSUM.AsFloat;
      taPCheckOrigSumR.AsFloat:=taPCTABSUM.AsFloat;
      taPCheckIsTax.AsInteger:=0;
      taPCheck.Post;

      rDSum:=0;
      taSpec.Active:=False;
      taSpec.ParamByName('IDHEAD').AsInteger:=taPCID.AsInteger;
      taSpec.Active:=True;
      taSpec.First;
      while not taSpec.Eof do
      begin
        taRCheck.Append;
        taRCheckUNI.AsInteger:=iNumR;
        taRCheckSys_Num.AsInteger:=iUni;
        taRCheckCnum.AsInteger:=iC;
        taRCheckSifr.AsInteger:=taSpecSIFR.AsInteger;
        taRCheckQnt.AsFloat:=taSpecQUANTITY.AsFloat;
        taRCheckPrice.AsFloat:=taSpecPRICE.AsFloat;
        if taSpecITYPE.AsInteger=0 then taRCheckComp.AsInteger:=1
        else taRCheckComp.AsInteger:=0;    //������������ ��� ���
        if taSpecQUANTITY.AsFloat<>0 then taRCheckRealPrice.AsFloat:=taSpecSUMMA.AsFloat/taSpecQUANTITY.AsFloat
        else taRCheckRealPrice.AsFloat:=taSpecPRICE.AsFloat;
        taRCheckQntR.AsFloat:=taSpecQUANTITY.AsFloat;
        taRCheckNalog.AsFloat:=0;
        taRCheck.Post;
        inc(iNumR);

        rDSum:=rDSum+taSpecDISCOUNTSUM.AsFloat;
        taSpec.Next;
      end;
      taSpec.Active:=False;

      if rDSum<>0 then
      begin
        taDCheck.Append;
        taDCheckUNI.AsInteger:=iNumD;
        taDCheckSys_Num.AsInteger:=iUni;
        taDCheckCnum.AsInteger:=iC;
        taDCheckSifr.AsInteger:=1;
        taDCheckSum.AsFloat:=-1*rDSum;
        StrWk:=taCashSailDISCONT.AsString;
        if length(StrWk)>=5 then StrWk:=Copy(StrWk,length(StrWk)-4,5);
        taDCheckCardCod.AsFloat:=StrToIntDef(StrWk,0);
        taDCheckPerson.AsInteger:=-1;
        taDCheckSumR.AsFloat:=-1*rDSum;
        taDCheck.Post;
        inc(iNumD);
      end;

      Inc(iNum); inc(iNumP); inc(iUni); inc(iC);

      taPC.Next;
    end;
    taPC.Active:=False;


    if CommonSet.SpecChar=1 then //��� ��� ����������
    begin //����������� �����������   //(sp.QUANTITY1*sp.PRICE)*(100-DISCOUNTPROC)/100 as RSUM
      taCashSail.Active:=False;
      taCashSail.ParamByName('DBEG').AsDateTime:=iDate+TimeShift;
      taCashSail.ParamByName('DEND').AsDateTime:=iDate+TimeShift+1;
      taCashSail.Active:=True;

      taCashSail.First;

      ProgressBar1.Position:=0;
      ProgressBar1.Visible:=True;
      Timer2.Enabled:=True;
      iProc:=taCashSail.RecordCount;
      iCurR:=1;
      while not taCashSail.Eof do
      begin
        taFis.Active:=False;
        taFis.ParamByName('IDHEAD').AsInteger:=taCashSailTAB_ID.AsInteger;
        taFis.Active:=True;
        taFis.First;
        rSum:=0;
        while not taFis.Eof do
        begin
          if (taFisQUANTITY1.AsFloat>0)and(taFisPRICE.AsFloat>0) then
          begin
            rPrice:=taFisSUMMA.AsFloat/taFisQUANTITY.AsFloat;

            taRCheck.Append;
            taRCheckUNI.AsInteger:=iNumR;
            taRCheckSys_Num.AsInteger:=iUni;
            taRCheckCnum.AsInteger:=iC;
            taRCheckSifr.AsInteger:=taFisSIFR.AsInteger;
            taRCheckQnt.AsFloat:=taFisQUANTITY1.AsFloat;
            taRCheckPrice.AsFloat:=rPrice;
            if taFisITYPE.AsInteger=0 then taRCheckComp.AsInteger:=1
            else taRCheckComp.AsInteger:=0;    //������������ ��� ���
            taRCheckRealPrice.AsFloat:=rPrice;
            taRCheckQntR.AsFloat:=taFisQUANTITY1.AsFloat;
            taRCheckNalog.AsFloat:=0;
            taRCheck.Post;
            inc(iNumR);

            rSum:=rSum+rPrice*taFisQUANTITY1.AsFloat;
          end;
          taFis.Next;
        end;

        if taFis.RecordCount>0 then
        begin  //���-�� ����
        // 1. �������� ���
        // 2. �������� ��� ������
          taCheck.Append;
          taCheckUNI.AsInteger:=iNum;
          taCheckSys_Num.AsInteger:=iUni;
          taCheckCnum.AsInteger:=iC;
          taCheckLogicDate.AsDateTime:=iDate+TimeShift+0.0417; //+1 ���
          taCheckRealDate.AsDateTime:=iDate+TimeShift+0.0417;
          taCheckOpenTime.AsString:=FormatDateTime('hh:nn',TimeShift+0.0417);
          taCheckCloseTime.AsString:=FormatDateTime('hh:nn',TimeShift+0.0417);
          taCheckCover.AsInteger:=taCashSailQUESTS.AsInteger;
          taCheckCashier.AsInteger:=taCashSailCASHERID.AsInteger;
          taCheckWaiter.AsInteger:=taCashSailWAITERID.AsInteger;
          taCheckUnit.AsString:=Copy(CommonSet.sUnit,1,2);
          StrWk:=CommonSet.DepartId;
          If Length(StrWk)<2 then StrWk:='0'+StrWk;
          taCheckDepart.AsString:=Copy(StrWk,1,2);
          taCheckTotal.AsFloat:=rSum;
          taCheckBaseKurs.AsFloat:=1;
          taCheckDeleted.AsInteger:=0;
          taCheckManager.AsInteger:=0;
          StrWk:=taCashSailNUMTABLE.AsString;
          taCheckTable.AsString:=Copy(StrWk,1,4);
          taCheckOpenDate.AsDateTime:=iDate+TimeShift+0.0417;
          taCheckCount.AsInteger:=29701;
          taCheckNacKurs.AsFloat:=1;
          taCheckTotalR.AsFloat:=rSum;
          taCheckCoverR.AsInteger:=taCashSailQUESTS.AsInteger;
          taCheckTaxSum.AsFloat:=0;
          taCheckTaxSumR.AsFloat:=0;
          taCheckTaxRate.AsFloat:=0;
          taCheckTaxRateR.AsFloat:=0;
          taCheckBonus.AsFloat:=0;
          taCheckBonusCard.AsFloat:=taCashSailID.AsInteger;
          taCheck.Post;


          taPCheck.Append;
          taPCheckUNI.AsInteger:=iNumP;
          taPCheckSys_Num.AsInteger:=iUni;
          taPCheckCnum.AsInteger:=iC;
          taPCheckCurency.AsInteger:=6;
          taPCheckBaseSumEqw.AsFloat:=rSum;
          taPCheckOriginalSum.AsFloat:=rSum;
          taPCheckKurs.AsFloat:=1;
          taPCheckDisc.AsFloat:=0;
          taPCheckExtra.AsString:='';
          taPCheckBaseSumR.AsFloat:=rSum;
          taPCheckOrigSumR.AsFloat:=rSum;
          taPCheckIsTax.AsInteger:=0;
          taPCheck.Post;

          inc(iNum); inc(iNumP); inc(iUni); inc(iC);
        end;
        taFis.Active:=False;

        taCashSail.Next;
        inc(iCurR);
      end;
      taCashSail.Active:=False;
    end;

    taCashSail.Active:=False;
    taCashSail.ParamByName('DBEG').AsDateTime:=iDate+TimeShift;
    taCashSail.ParamByName('DEND').AsDateTime:=iDate+TimeShift+1;
    taCashSail.Active:=True;

    taCashSail.First;

    ProgressBar1.Position:=0;
    ProgressBar1.Visible:=True;
    Timer2.Enabled:=True;
    iProc:=taCashSail.RecordCount;
    iCurR:=1;
    while not taCashSail.Eof do
    begin
      taCheck.Append;
      taCheckUNI.AsInteger:=iNum;
      taCheckSys_Num.AsInteger:=iUni;
      taCheckCnum.AsInteger:=iC;
      taCheckLogicDate.AsDateTime:=Trunc(taCashSailCHDATE.AsDateTime-TimeShift);
      taCheckRealDate.AsDateTime:=Trunc(taCashSailCHDATE.AsDateTime);
      taCheckOpenTime.AsString:=FormatDateTime('hh:nn',taCashSailBEGTIME.AsDateTime);
      taCheckCloseTime.AsString:=FormatDateTime('hh:nn',taCashSailCHDATE.AsDateTime);
      taCheckCover.AsInteger:=taCashSailQUESTS.AsInteger;
      taCheckCashier.AsInteger:=taCashSailCASHERID.AsInteger;
      taCheckWaiter.AsInteger:=taCashSailWAITERID.AsInteger;
      taCheckUnit.AsString:=Copy(CommonSet.sUnit,1,2);
      StrWk:=CommonSet.DepartId;
      If Length(StrWk)<2 then StrWk:='0'+StrWk;
      taCheckDepart.AsString:=Copy(StrWk,1,2);
      taCheckTotal.AsFloat:=taCashSailTABSUM.AsFloat;
      taCheckBaseKurs.AsFloat:=1;
      taCheckDeleted.AsInteger:=0;
      taCheckManager.AsInteger:=0;
      StrWk:=taCashSailNUMTABLE.AsString;
      taCheckTable.AsString:=Copy(StrWk,1,4);
      taCheckOpenDate.AsDateTime:=Trunc(taCashSailCHDATE.AsDateTime);
      taCheckCount.AsInteger:=29701;
      taCheckNacKurs.AsFloat:=1;
      taCheckTotalR.AsFloat:=taCashSailTABSUM.AsFloat;
      taCheckCoverR.AsInteger:=taCashSailQUESTS.AsInteger;
      taCheckTaxSum.AsFloat:=0;
      taCheckTaxSumR.AsFloat:=0;
      taCheckTaxRate.AsFloat:=0;
      taCheckTaxRateR.AsFloat:=0;
      taCheckBonus.AsFloat:=0;
      taCheckBonusCard.AsFloat:=taCashSailID.AsInteger;
      taCheck.Post;

      taPCheck.Append;
      taPCheckUNI.AsInteger:=iNumP;
      taPCheckSys_Num.AsInteger:=iUni;
      taPCheckCnum.AsInteger:=iC;
      taPCheckCurency.AsInteger:=4;
      taPCheckBaseSumEqw.AsFloat:=taCashSailTABSUM.AsFloat;
      taPCheckOriginalSum.AsFloat:=taCashSailTABSUM.AsFloat;
      taPCheckKurs.AsFloat:=1;
      taPCheckDisc.AsFloat:=0;
      taPCheckExtra.AsString:='';
      taPCheckBaseSumR.AsFloat:=taCashSailTABSUM.AsFloat;
      taPCheckOrigSumR.AsFloat:=taCashSailTABSUM.AsFloat;
      taPCheckIsTax.AsInteger:=0;
      taPCheck.Post;


      rDSum:=0;
      taSpec.Active:=False;
      taSpec.ParamByName('IDHEAD').AsInteger:=taCashSailTAB_ID.AsInteger;
      taSpec.Active:=True;
      taSpec.First;
      while not taSpec.Eof do
      begin
        taRCheck.Append;
        taRCheckUNI.AsInteger:=iNumR;
        taRCheckSys_Num.AsInteger:=iUni;
        taRCheckCnum.AsInteger:=iC;
        taRCheckSifr.AsInteger:=taSpecSIFR.AsInteger;
        taRCheckQnt.AsFloat:=taSpecQUANTITY.AsFloat;
        taRCheckPrice.AsFloat:=taSpecPRICE.AsFloat;
        if taSpecITYPE.AsInteger=0 then taRCheckComp.AsInteger:=1
        else taRCheckComp.AsInteger:=0;    //������������ ��� ���
        if taSpecQUANTITY.AsFloat<>0 then taRCheckRealPrice.AsFloat:=taSpecSUMMA.AsFloat/taSpecQUANTITY.AsFloat
        else taRCheckRealPrice.AsFloat:=taSpecPRICE.AsFloat;
        taRCheckQntR.AsFloat:=taSpecQUANTITY.AsFloat;
        taRCheckNalog.AsFloat:=0;
        taRCheck.Post;
        inc(iNumR);

        rDSum:=rDSum+taSpecDISCOUNTSUM.AsFloat;
        taSpec.Next;
      end;
      taSpec.Active:=False;

      if rDSum<>0 then
      begin
        taDCheck.Append;
        taDCheckUNI.AsInteger:=iNumD;
        taDCheckSys_Num.AsInteger:=iUni;
        taDCheckCnum.AsInteger:=iC;
        taDCheckSifr.AsInteger:=1;
        taDCheckSum.AsFloat:=-1*rDSum;
        StrWk:=taCashSailDISCONT.AsString;
        if length(StrWk)>=5 then StrWk:=Copy(StrWk,length(StrWk)-4,5);
        taDCheckCardCod.AsFloat:=StrToIntDef(StrWk,0);
        taDCheckPerson.AsInteger:=-1;
        taDCheckSumR.AsFloat:=-1*rDSum;
        taDCheck.Post;
        inc(iNumD);
      end;

      Inc(iNum); inc(iNumP); inc(iUni); inc(iC);
      inc(iCurR);
      taCashSail.Next;
    end;
    //�� �������� ����������
    // ������ ����� ��������� VCheck
    // �������� ��� ���������
    taDel.Active:=False;
    taDel.ParamByName('DBEG').AsDateTime:=iDate+TimeShift;
    taDel.ParamByName('DEND').AsDateTime:=iDate+TimeShift+1;
    taDel.Active:=True;
    taDel.First;
    while not taDel.Eof do
    begin
      taSpec.Active:=False;
      taSpec.ParamByName('IDHEAD').AsInteger:=taDelID.AsInteger;
      taSpec.Active:=True;
      taSpec.First;
      while not taSpec.Eof do
      begin
        if taDelOPERTYPE.AsString='Del' then
        begin
          taVCheck.Append;
          taVCheckLogicDate.AsDateTime:=Trunc(taDelENDTIME.AsDateTime-TimeShift);
          taVCheckRealDate.AsDateTime:=Trunc(taDelENDTIME.AsDateTime);
          taVCheckTime.AsString:=FormatDateTime('hh:nn',taDelENDTIME.AsDateTime);
          taVCheckSifr.AsInteger:=taSpecSIFR.AsInteger;
          taVCheckComp.AsInteger:=taSpecITYPE.AsInteger; //0-����� 1-���
          taVCheckQnt.AsFloat:=taSpecQUANTITY.AsFloat;
          taVCheckPrice.AsFloat:=taSpecPRICE.AsFloat;
          if taDelSKLAD.AsInteger=0 then taVCheckReason.AsInteger:=2;  //��������� ��������
          if taDelSKLAD.AsInteger=1 then taVCheckReason.AsInteger:=1;  //�� ��������� ��������
          taVCheckManager.AsInteger:=taDelID_PERSONAL.AsInteger;
          taVCheckWaiter.AsInteger:=taDelID_PERSONAL.AsInteger;
          if length(taDelNUMTABLE.AsString)>3 then
          taVCheckTable.AsString:=Copy(taDelNUMTABLE.AsString,1,4)
          else taVCheckTable.AsString:=taDelNUMTABLE.AsString;
          taVCheckUnit.AsString:=Copy(CommonSet.sUnit,1,2);
          StrWk:=CommonSet.DepartId;
          If Length(StrWk)<2 then StrWk:='0'+StrWk;
          taVCheckDepart.AsString:=Copy(StrWk,1,2);
          taVCheckTabNum.AsInteger:=taDelID.AsInteger;
          taVCheck.Post;
        end;

        taSpec.Next;
      end;
      taSpec.Active:=False;

      taDel.Next;
    end;
    taDel.Active:=False;

    Timer2.Enabled:=False;

    Memo1.Lines.Add('�������� ���������� ��.');
    ProgressBar1.Visible:=False;

    Memo1.Lines.Add('����. �������������.');

    quMenu.Active:=False;
    quMenu.Active:=True;
    quMenu.First;
    while not quMenu.Eof do
    begin

      if taMenu.Locate('Sifr',quMenuSIFR.AsInteger,[]) then taMenu.Edit
      else taMenu.Append;

      taMenuSifr.AsInteger:=quMenuSIFR.AsInteger;
      taMenuName.AsString:=Copy(quMenuNAME.AsString,1,25);
      taMenuPrice.AsFloat:=quMenuPRICE.AsFloat;
      sCode:=IntToStr(quMenuSIFR.AsInteger);
      while length(sCode)>4 do delete(sCode,1,1);
      taMenuCode.AsString:=sCode;
      taMenuTreeType.AsString:=quMenuTREETYPE.AsString;
      taMenuLimitPrice.AsFloat:=0;
      taMenuCateg.AsInteger:=1;
      taMenuParent.AsInteger:=quMenuPARENT.AsInteger;
      taMenuLink.AsInteger:=0;
      taMenuStream.AsInteger:=quMenuSTREAM.AsInteger;
      taMenuLack.AsInteger:=0;
      taMenuDesignSifr.AsInteger:=0;
      taMenuAltName.AsString:='';
      taMenuNalog.AsFloat:=0;
      taMenuBarcode.AsString:='';
      taMenuImage.AsInteger:=0;
      taMenuConsumma.AsFloat:=0;
      taMenuMinRest.AsInteger:=0;
      taMenuPrnRest.AsInteger:=0;
      taMenuCookTime.AsInteger:=0;
      taMenuDispenser.AsInteger:=0;
      taMenuDispKoef.AsInteger:=0;
      taMenuAccess.AsInteger:=0;
//      taMenuFlags.AsInteger:=4;
//      taMenuTara.AsInteger:=0;
//      taMenuCntPrice.AsInteger:=1;
//      taMenuBackBGR.AsFloat:=0;
//      taMenuFontBGR.AsFloat:=0;
//      taMenuTaxGroup.AsInteger:=0;
//      taMenuMinPrice.AsFloat:=0;
//      taMenuReccom.AsInteger:=0;
//      taMenuDateIncl.AsFloat:=0;
//      taMenuDateExcl.AsFloat:=0;
//      taMenuTimeIncl.AsInteger:=0;
//      taMenuTimeExcl.AsInteger:=0;
//      taMenuFWeek.AsInteger:=0;

      taMenu.Post;

      quMenu.Next;
    end;
    quMenu.Active:=False;


    Memo1.Lines.Add('��.'); Delay(10);

    Memo1.Lines.Add('������������. �������������.'); Delay(10);

    quMod.Active:=False;
    quMod.Active:=True;
    quMod.First;
    while not quMod.Eof do
    begin
      if taMod.Locate('Sifr',quModSIFR.AsInteger,[]) then taMod.Edit
      else taMod.Append;

      taModSifr.AsInteger:=quModSIFR.AsInteger;
      taModName.AsString:=quModNAME.AsString;
      taModParent.AsInteger:=quModPARENT.AsInteger;
      taModPrice.AsFloat:=quModPRICE.AsFloat;
      taModRealPrice.AsFloat:=quModREALPRICE.AsFloat;
      taMod.Post;

      quMod.Next;
    end;
    quMod.Active:=False;

    Memo1.Lines.Add('��.'); Delay(10);
    Memo1.Lines.Add('���������. �������������.'); Delay(10);

    quCateg.Active:=False;
    quCateg.Active:=True;
    quCateg.First;
    while not quCateg.Eof do
    begin
      if taCateg.Locate('Sifr',quCategSIFR.AsInteger,[]) then taCateg.Edit
      else taCateg.Append;

      taCategSifr.AsInteger:=quCategSIFR.AsInteger;
      taCategName.AsString:=quCategNAME.AsString;
      taCategTaxGroup.AsInteger:=quCategTAX_GROUP.AsInteger;
      taCateg.Post;

      quCateg.Next;
    end;
    quCateg.Active:=False;

    Memo1.Lines.Add('��.'); Delay(10);

    

    //����� �������� ����

  finally
    cxButton1.Enabled:=True;
    cxButton2.Enabled:=True;

    taCheck.Active:=False;
    taRCheck.Active:=False;
    taPCheck.Active:=False;
    taDCheck.Active:=False;
    taVCheck.Active:=False;

    taMenu.Active:=False;
    taMod.Active:=False;
    taCateg.Active:=False;

    Memo1.Lines.Add('�������� ���� FB.');
    Memo1.Lines.Add('');
    Memo1.Lines.Add('������� �������� ��.');
    Memo1.Lines.Add('');
    dmC.Connected:=False;

  end;
end;

procedure TfmMainAppend.FormShow(Sender: TObject);
Var StrWk:String;
    iDate:Integer;
    TimeShift:TDateTime;
begin
  TimeShift:=StrToTimeDef(CommonSet.ZTimeShift,0);
  iDate:=Trunc(cxDateEdit1.Date);
  StrWk:='������ c '+FormatDateTime('dd.mm.yyyy hh:nn',iDate+TimeShift)+ ' �� '+FormatDateTime('dd.mm.yyyy hh:nn',iDate+1+TimeShift);
  Memo1.Lines.Add(StrWk);
end;

end.
