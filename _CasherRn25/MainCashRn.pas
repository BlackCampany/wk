unit MainCashRn;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, RXClock, StdCtrls, ExtCtrls, cxStyles, cxCustomData, cxGraphics,
  cxFilter, cxData, cxEdit, DB, cxDBData, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxGrid, cxGridCardView, cxGridDBCardView,
  cxLookAndFeelPainters, cxButtons, ActnList, XPStyleActnCtrls, ActnMan,
  dxfBackGround, cxDataStorage, Menus, FR_Class, FR_DSet, FR_DBSet, Grids,
  DBGrids, cxContainer, cxProgressBar, dxfLabel,cxImageComboBox;

type
  TfmMainCashRn = class(TForm)
    Panel1: TPanel;
    Label8: TLabel;
    Label9: TLabel;
    Panel2: TPanel;
    Label13: TLabel;
    RxClock1: TRxClock;
    Timer1: TTimer;
    Panel3: TPanel;
    Label1: TLabel;
    GridTab: TcxGrid;
    PersLevel: TcxGridLevel;
    PersView: TcxGridDBTableView;
    PersViewNAME: TcxGridDBColumn;
    PersViewsCountTab: TcxGridDBColumn;
    TabLevel: TcxGridLevel;
    TabView: TcxGridDBCardView;
    TabViewQUESTS: TcxGridDBCardViewRow;
    TabViewTABSUM: TcxGridDBCardViewRow;
    TabViewSSTAT: TcxGridDBCardViewRow;
    TabViewSTIME: TcxGridDBCardViewRow;
    Button1: TcxButton;
    Button2: TcxButton;
    Button3: TcxButton;
    Button4: TcxButton;
    Button5: TcxButton;
    amRn: TActionManager;
    acCreateTab: TAction;
    acDel: TAction;
    acMove: TAction;
    Button6: TcxButton;
    acRetPre: TAction;
    acCashEnd: TAction;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    cxButton1: TcxButton;
    Panel4: TPanel;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    acCashRep: TAction;
    acExit: TAction;
    cxButton5: TcxButton;
    Label5: TLabel;
    dxfBackGround1: TdxfBackGround;
    Label6: TLabel;
    acOpen: TAction;
    Button7: TcxButton;
    acPrintPre: TAction;
    acCashPCard: TAction;
    Button8: TcxButton;
    frRepMain: TfrReport;
    frquCheck: TfrDBDataSet;
    TimerDelayPrintQ: TTimer;
    TabViewNAME: TcxGridDBCardViewRow;
    TiRefresh: TTimer;
    cxButton6: TcxButton;
    cxButton7: TcxButton;
    cxButton8: TcxButton;
    cxButton9: TcxButton;
    acRecalcCount: TAction;
    Action1: TAction;
    Panel5: TPanel;
    dxfLabel1: TdxfLabel;
    PBar1: TcxProgressBar;
    PBar2: TcxProgressBar;
    PBar3: TcxProgressBar;
    Label7: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    TimerPrintQ: TTimer;
    cxButton10: TcxButton;
    TimerClose: TTimer;
    cxButton11: TcxButton;
    acViewTab: TAction;
    cxButton12: TcxButton;
    Label12: TLabel;
    Label14: TLabel;
    cxButton13: TcxButton;
    cxButton18: TcxButton;
    cxButton19: TcxButton;
    procedure FormCanResize(Sender: TObject; var NewWidth,
      NewHeight: Integer; var Resize: Boolean);
    procedure Timer1Timer(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Panel2Click(Sender: TObject);
    procedure TabViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure Button2Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure acCreateTabExecute(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure acMoveExecute(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure acRetPreExecute(Sender: TObject);
    procedure acCashEndExecute(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure acCashRepExecute(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure RxClock1Click(Sender: TObject);
    procedure acOpenExecute(Sender: TObject);
    procedure TabViewDblClick(Sender: TObject);
    procedure acPrintPreExecute(Sender: TObject);
    procedure acCashPCardExecute(Sender: TObject);
    procedure TimerDelayPrintQTimer(Sender: TObject);
    procedure TimerPrintQTimer(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
    procedure cxButton7Click(Sender: TObject);
    procedure cxButton8Click(Sender: TObject);
    procedure cxButton9Click(Sender: TObject);
    procedure TiRefreshTimer(Sender: TObject);
    procedure acRecalcCountExecute(Sender: TObject);
    procedure Action1Execute(Sender: TObject);
    procedure TabViewCellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxButton10Click(Sender: TObject);
    procedure TimerCloseTimer(Sender: TObject);
//    procedure acViewTabExecute(Sender: TObject);
    procedure PersViewCellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxButton11Click(Sender: TObject);
    procedure acViewTabExecute(Sender: TObject);
    procedure cxButton12Click(Sender: TObject);
    procedure cxButton13Click(Sender: TObject);
    procedure cxButton19Click(Sender: TObject);
    procedure cxButton18Click(Sender: TObject);
  private
    { Private declarations }
  public
    Procedure CreateViewPers(bCh:Boolean);
    Procedure DestrViewPers;
    procedure AssignBrush(ABrush: TBrush; ABitMap: TBitMap);
    procedure prDisButton;
    procedure prEnButton;
    procedure prSetWaitersSum;
    { Public declarations }
  end;





Procedure WriteStatus;
procedure prGetCashPar(iOrg:INteger);
procedure prWriteChNumToBase(iOrg:Integer);


var
  fmMainCashRn: TfmMainCashRn;
  TableImage:TBitmap;
  FGridBrush:TBrush;
  bOpenTable:Boolean=False; //true � �������� ��������, ���� ������ �������� �������
  arQ:Array[1..100] of Integer;

implementation

uses uPre, Un1, Dm, Menu, Calc, CreateTab, MessDel, Spec, CashEnd, UnCash,
  Attention, Discont, fmDiscountShape, uDB1, BnSber, MessTxt,
  cash_WarnPaper, CashInOut, ViewTab, SendMessage, prdb, PCardsCash,
  StopListCash, UnPrizma, CopyCh, Vtb24un, Unit1, SelOrg, UnInOutM;

{$R *.dfm}

procedure prWriteChNumToBase(iOrg:Integer);
begin
  with dmC do
  begin
    quSelCashPar.Active:=False;
    quSelCashPar.ParamByName('IST').AsInteger:=CommonSet.Station;
    quSelCashPar.ParamByName('IORG').AsInteger:=iOrg;
    quSelCashPar.Active:=True;

    quSelCashPar.Edit;
    quSelCashParCASHZ.AsInteger:=Nums.ZNum;
    quSelCashParCASHCHNUM.AsInteger:=Nums.iCheckNum;
    quSelCashPar.Post;

    quSelCashPar.Active:=False;
  end;
end;

procedure prGetCashPar(iOrg:INteger);
begin
  with dmC do
  begin
    quSelCashPar.Active:=False;
    quSelCashPar.ParamByName('IST').AsInteger:=CommonSet.Station;
    quSelCashPar.ParamByName('IORG').AsInteger:=iOrg;
    quSelCashPar.Active:=True;

    CommonSet.iOrg:=iOrg;
    CommonSet.CashNum:=quSelCashParCASHNUM.AsInteger;
    CommonSet.CashChNum:=quSelCashParCASHCHNUM.AsInteger;
    CommonSet.CashZ:=quSelCashParCASHZ.AsInteger;
    CommonSet.CashPort:=quSelCashParCASHPORT.AsInteger;
    CommonSet.ComDelay:=quSelCashParCOMDELAY.AsInteger;
    CommonSet.CashSer:=quSelCashParCASHSER.AsString;
    CommonSet.Fis:=quSelCashParFIS.AsInteger;
    CommonSet.DepartName:=quSelCashParDEPARTNAME.AsString;
    CommonSet.LastLine:=quSelCashParLASTLINE.AsString;  // ��������� ������ � �����
    CommonSet.PreLine:=quSelCashParPRELINE.AsString;  // ������ ������ � �����
    CommonSet.LastLine1:=quSelCashParLASTLINE1.AsString;  // ��������� ������ � ����
    CommonSet.PreLine1:=quSelCashParPRELINE1.AsString;  // ������ ������ � ����

    Nums.ZNum:=quSelCashParCASHZ.AsInteger;
    Nums.iCheckNum:=quSelCashParCASHCHNUM.AsInteger;

    CommonSet.SpecBoxX:=Trim(quSelCashParSPECBOXX.AsString);

    SpecVal.SPH1:=Trim(quSelCashParSPH1.AsString);
    SpecVal.SPH2:=Trim(quSelCashParSPH2.AsString);
    SpecVal.SPH3:=Trim(quSelCashParSPH3.AsString);
    SpecVal.SPH4:=Trim(quSelCashParSPH4.AsString);
    SpecVal.SPH5:=Trim(quSelCashParSPH5.AsString);

    quSelCashPar.Active:=False;
  end;
end;

procedure TfmMainCashRn.prSetWaitersSum;
Var rSumMax:Real;
    iMax:Integer;
begin
  with dmC1 do
  begin
    quWaitersSum.Active:=False;
    quWaitersSum.ParamByName('DATEB').AsDateTime:=Trunc(Date)+StrToTime(CommonSet.ZTimeShift);
    quWaitersSum.ParamByName('DATEE').AsDateTime:=Trunc(Date)+1+StrToTime(CommonSet.ZTimeShift);
    quWaitersSum.Active:=True;

    iMax:=0;
    quWaitersSum.First;
    while not quWaitersSum.Eof do
    begin
      inc(iMax);
      quWaitersSum.Next;
      if iMax>3 then break;
    end;
    quWaitersSum.First;


    if iMax=0 then
    begin
      PBar1.Visible:=False; Label7.Visible:=False;
      PBar2.Visible:=False; Label10.Visible:=False;
      PBar3.Visible:=False; Label11.Visible:=False;
    end;

    if iMax=1 then
    begin
      PBar1.Visible:=True; Label7.Visible:=True;
      PBar2.Visible:=False; Label10.Visible:=False;
      PBar3.Visible:=False; Label11.Visible:=False;

      quWaitersSum.First;
      Label7.Caption:=quWaitersSumNAME.AsString;
    end;

    if iMax=2 then
    begin
      PBar1.Visible:=True; Label7.Visible:=True;
      PBar2.Visible:=True; Label10.Visible:=True;
      PBar3.Visible:=False; Label11.Visible:=False;

      try
        quWaitersSum.First;
        Label7.Caption:=quWaitersSumNAME.AsString; rSumMax:=quWaitersSumSUMR.AsFloat;
        quWaitersSum.Next;
        Label10.Caption:=quWaitersSumNAME.AsString;
        PBar2.Width:=Trunc(quWaitersSumSUMR.AsFloat/rSumMax*180);
      except
      end;
    end;

    if iMax>=3 then
    begin
      PBar1.Visible:=True; Label7.Visible:=True;
      PBar2.Visible:=True; Label10.Visible:=True;
      PBar3.Visible:=True; Label11.Visible:=True;

      try
        quWaitersSum.First;
        Label7.Caption:=quWaitersSumNAME.AsString; rSumMax:=quWaitersSumSUMR.AsFloat;
        quWaitersSum.Next;
        Label10.Caption:=quWaitersSumNAME.AsString;
        PBar2.Width:=Trunc(quWaitersSumSUMR.AsFloat/rSumMax*180);
        quWaitersSum.Next;
        Label11.Caption:=quWaitersSumNAME.AsString;
        PBar3.Width:=Trunc(quWaitersSumSUMR.AsFloat/rSumMax*180);
      except
      end;
    end;

    quWaitersSum.Active:=False;
  end;
end;


procedure TfmMainCashRn.prDisButton;
begin
  Button1.Enabled:=False;
  Button2.Enabled:=False;
  Button3.Enabled:=False;
  Button4.Enabled:=False;
  Button5.Enabled:=False;
  Button6.Enabled:=False;
  Button7.Enabled:=False;
  Button8.Enabled:=False;

  bPressB5Main:=True;

  TiRefresh.Enabled:=False; //���������� �������������� ����������
end;

procedure TfmMainCashRn.prEnButton;
begin
  Button1.Enabled:=True;
  Button2.Enabled:=True;
  Button3.Enabled:=True;
  Button4.Enabled:=True;
  Button5.Enabled:=True;
  Button6.Enabled:=True;
  Button7.Enabled:=True;
  Button8.Enabled:=True;

  bPressB5Main:=False;

  TiRefresh.Enabled:=True; //���������� �������������� ����������
end;

Procedure WriteStatus;
begin
  with fmMainCashRn do
  begin
    //��� ���� �������

    Label2.Caption:='����: '+Nums.sDate;
    Label4.Caption:='����� �'+IntToStr(Nums.ZNum)+'  ��� � '+IntToStr(Nums.iCheckNum);
    Label9.Caption:='���.�����  '+CommonSet.CashSer;
    Label6.Caption:='';

    Case Operation of
    0: begin
         Label5.Caption:='�������� - ������� ��������.';
       end;
    1: begin
         Label5.Caption:='�������� - �������.';
       end;
    end;

    if CommonSet.CashNum=0 then
    begin
      label8.caption:='������� ���������.';
      Label6.Caption:='';
      Label3.Caption:='������ ���: ���';
    end;
    if CommonSet.Fis>0 then
    begin

      label8.caption:='����� � '+IntToStr(CommonSet.CashNum);
      Label6.Caption:='�������� (����) '+INtToStr(Nums.ZYet);
      Label3.Caption:='������ ���: ������ ('+IntToStr(Nums.iRet)+')';

      if Nums.iRet=0 then Label3.Caption:='������ ���: '+Nums.sRet;
      if (Nums.iRet=2)or(Nums.iRet=22) then Label3.Caption:='������ ���: ���������� ������� �����';
    end;
    if CommonSet.Fis=0 then
    begin
      if CommonSet.SpecChar=0 then
      begin
        label8.caption:='������� �������.';
        Label3.Caption:='������ ���: ���';
      end else
      begin
        label8.caption:='����� � '+IntToStr((-1)*CommonSet.CashNum);
        Label3.Caption:='������ ���: ��';
      end;
    end;
  end;
end;



Procedure TfmMainCashRn.DestrViewPers;
begin
  Label13.Caption:='';
  Person.Id:=0;
  Person.Name:='';
  Panel3.Visible:=False;
  Panel4.Visible:=False;
  Button1.Visible:=False;
  Button2.Visible:=False;
  Button3.Visible:=False;
  Button4.Visible:=False;
  Button5.Visible:=False;
  Button6.Visible:=False;
  Button7.Visible:=False;
  Button8.Visible:=False;
  cxButton1.Visible:=False;
  cxButton6.Visible:=False;
  cxButton11.Visible:=False;
  cxButton12.Visible:=False;

  acOpen.Enabled:=False;

  if iColor=1 then
  begin
    dxfBackGround1.BkColor.BeginColor:=$00E8CDB7; //��� ������� �����
    dxfBackGround1.BkColor.EndColor:=$0058371D;
    Panel1.Color:=$005E3C20;
    Panel2.Color:=$009D9D4F;
    RxClock1.Color:=$005E3C20;
  end;
  if iColor=2 then
  begin
    dxfBackGround1.BkColor.BeginColor:=$00A6A6A6; //��� ����� �����
    dxfBackGround1.BkColor.EndColor:=$00000000;
    Panel1.Color:=$00252525;
    Panel2.Color:=$00BBBBBB;
    RxClock1.Color:=$00000000;
  end;

  PrintFCheck:=False; //��� ���� � ������� ������� ��������� ������
  // ������������ ��� �� ������ ������, ��� ������ ���� ��� False

  prSetWaitersSum;
  Panel5.Visible:=True;

  TiRefresh.Enabled:=False; //���������� �������������� ����������
end;


Procedure TfmMainCashRn.CreateViewPers(bCh:Boolean);
Var TiShift:TDateTime;
begin
  TiRefresh.Enabled:=True; //���������� �������������� ����������

  Panel5.Visible:=False;

  if bCh=False then exit; //���� ��������� ������� ������ �� ������ �������
  bChangeView:=False; //������� ������ �� ������ ������ ���������� ��� ������.
  TiShift:=Now;
  TiShift:=TiShift-(CommonSet.HourShift/24);

  Label13.Caption:=Person.Name;
  with dmC do
  begin

    PersView.BeginUpdate;
    TabView.BeginUpdate;

    quPers.Active:=False;
//    dsPers.DataSet:=nil;

    if CanDo('prViewAll') then
    begin
      quPers.SelectSQL.Clear;
      quPers.SelectSQL.Add('select t.Id_personal, p.Name, count(*) as CountTab, sum(TabSum) as TotalSum from tables t');
      quPers.SelectSQL.Add('left join rpersonal p on p.Id=t.Id_Personal');
      quPers.SelectSQL.Add('Where t.BEGTIME >'''+FormatDateTime('dd.mm.yyyy hh:nn',TiShift)+'''');
      quPers.SelectSQL.Add('group by t.Id_personal, p.Name');
      quPers.SelectSQL.Add('order by p.Name');
    end
    else
    begin
      quPers.SelectSQL.Clear;
      quPers.SelectSQL.Add('select t.Id_personal, p.Name, count(*) as CountTab, sum(TabSum) as TotalSum from tables t');
      quPers.SelectSQL.Add('left join rpersonal p on p.Id=t.Id_Personal');
      quPers.SelectSQL.Add('Where Id_personal='+IntToStr(Person.Id));
      quPers.SelectSQL.Add('and t.BEGTIME >'''+FormatDateTime('dd.mm.yyyy hh:nn',TiShift)+'''');
      quPers.SelectSQL.Add('group by t.Id_personal, p.Name');
      quPers.SelectSQL.Add('order by p.Name');
    end;

    quPers.Active:=True;
//    dsPers.DataSet:=quPers;

    quTabs.Active:=False;
//    dsTabs.DataSet:=nil;

    if CanDo('prViewAll') then
    begin
      quTabs.SelectSQL.Clear;
      quTabs.SelectSQL.Add('SELECT t.ID, t.ID_PERSONAL, t.NUMTABLE, t.QUESTS, t.TABSUM, t.BEGTIME,');
      quTabs.SelectSQL.Add('t.ISTATUS, t.DISCONT, dc.Name, t.NUMZ, t.SALET');
      quTabs.SelectSQL.Add('FROM TABLES t');
      quTabs.SelectSQL.Add('left join DISCCARD dc On dc.BARCODE=t.DISCONT');
      quTabs.SelectSQL.Add('Where t.BEGTIME >'''+FormatDateTime('dd.mm.yyyy hh:nn',TiShift)+'''');
      quTabs.SelectSQL.Add('ORDER BY ID_PERSONAL, t.BEGTIME');
    end
    else
    begin
      quTabs.SelectSQL.Clear;
      quTabs.SelectSQL.Add('SELECT t.ID, t.ID_PERSONAL, t.NUMTABLE, t.QUESTS, t.TABSUM, t.BEGTIME,');
      quTabs.SelectSQL.Add('t.ISTATUS, t.DISCONT, dc.Name, t.NUMZ, t.SALET');
      quTabs.SelectSQL.Add('FROM TABLES t');
      quTabs.SelectSQL.Add('left join DISCCARD dc On dc.BARCODE=t.DISCONT');
      quTabs.SelectSQL.Add('Where Id_personal='+IntToStr(Person.Id));
      quTabs.SelectSQL.Add('and t.BEGTIME >'''+FormatDateTime('dd.mm.yyyy hh:nn',TiShift)+'''');
      quTabs.SelectSQL.Add('ORDER BY ID_PERSONAL, t.BEGTIME');
    end;

//t.BEGTIME>'27.01.2008 01:00'

    quTabs.Active:=True;
//    dsTabs.DataSet:=quTabs;

    PersView.EndUpdate;
    TabView.EndUpdate;

//    delay(10);
//    PersView.ViewData.Expand(True);

    PersView.Focused:=True;
    quPers.Last;

    while not quPers.Bof do
    begin
      PersView.Controller.FocusedRow.Expand(True);
      quPers.Prior;
    end;


    if CanDo('prAddAll')or CanDo('prAdd') then
    begin
      Button1.Visible:=True;
      acCreateTab.Enabled:=True;
    end
    else acCreateTab.Enabled:=False;
    if CanDo('prEditAll')or CanDo('prEdit') then
    begin
      Button2.Visible:=True;
      acOpen.Enabled:=True;
    end
    else acOpen.Enabled:=False;
    if CanDo('prDelAll')or CanDo('prDel') then
    begin
      Button3.Visible:=True;
      acDel.Enabled:=True;
    end
    else acDel.Enabled:=False;
    if CanDo('prMovAll') then
    begin
      Button4.Visible:=True;
      acMove.Enabled:=True;
    end
    else acMove.Enabled:=False;

    if CanDo('prPrintCheck') then
    begin
      if CommonSet.CashNum<>0 then
      begin
        Button5.Visible:=True; //��� =0 - ������� ���������;
        acCashEnd.Enabled:=True;
      end
      else acCashEnd.Enabled:=False;
    end
    else acCashEnd.Enabled:=False;

    if CanDo('prRetPre') then
    begin
      Button6.Visible:=True;
      acRetPre.Enabled:=True;
    end
    else acRetPre.Enabled:=False;

    if CanDo('prPrintPre') then   //�������� ����
    begin
      if CommonSet.iPrePrintSt=0 then
      begin
        Button7.Visible:=True;
        acPrintPre.Enabled:=True;
      end else
      begin
        if CanDo('prPrintPreSt') then
        begin
          Button7.Visible:=True;
          acPrintPre.Enabled:=True;
        end else acPrintPre.Enabled:=False;
      end;
    end
    else acPrintPre.Enabled:=False;


    if CanDo('prPrintBNCheck') then
    begin
      Button8.Visible:=True;
      acCashPCard.Enabled:=True;
    end
    else acCashPCard.Enabled:=False;


    if CanDo('prCashRep') then
    begin
      cxButton1.Visible:=True;
      acCashRep.Enabled:=True;
    end
    else acCashRep.Enabled:=False;

    cxButton6.Visible:=True;
    cxButton11.Visible:=True;
    cxButton12.Visible:=True;

    Button5.Enabled:=True; //������ ������� �������� - ����� �� ������ ������
    bPressB5Main:=False;
    Label1.Caption:='�������� ������ �� ��������� '+IntToStr(CommonSet.HourShift)+' �.';

    prFormMenuList(0); //������

    Event_RegEx(3,Nums.iCheckNum,0,0,'','',0,0,0,0,0,0);

    prEnButton; //�������� ��� ������ �� ������ ������
  end;
end;

procedure TfmMainCashRn.FormCanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
  if GridTab.Tag=0 then NewHeight:=768;
  if GridTab.Tag=1 then NewHeight:=600;
  if GridTab.Tag=2 then NewHeight:=1024;
end;

procedure TfmMainCashRn.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled:=False;
  Person.Id:=0;
  Person.Name:='';
  fmPre.ShowModal;
end;

procedure TfmMainCashRn.FormShow(Sender: TObject);
Var StrC:String;
//Const WarnMess:String = '����� �������� ��������� �������.';
begin
//
//  delay(3000);

{  StrWk:=#$33+#$30+#$2E+#$31+#$31+#$2E+#$32+#$30+#$30+#$37;
  MaxDate:=StrToDate(StrWk);
  if MaxDate<Date then
  begin
    ShowMessage(WarnMess);
//    close;
    delay(300000);
  end;

  StrWk:=#$33+#$30+#$2E+#$31+#$31+#$2E+#$32+#$30+#$30+#$37;
  MaxDate:=StrToDate(StrWk);
  if MaxDate<Date then
  begin
    ShowMessage(WarnMess);
    close;
//    delay(300000);
  end;
}

  StrC:=CommonSet.DepartName;
  if SpecVal.DateTo<StrToDate('01.01.2100') then
    StrC:=StrC+' ��������� �� '+FormatDateTime('dd.mm.yyyy',SpecVal.DateTo);
  if SpecVal.CountStarts>=0 then
    StrC:=StrC+' �������� '+INtToStr(SpecVal.CountStarts)+' �������� ���������.';
  if SpecVal.CountStarts=-1 then
    StrC:=StrC+' �������� 0 �������� ���������.';

  Caption:=StrC;  

  Timer1.Enabled:=True;
  with dmC do
  begin
    try
      CasherRnDb.Connected:=False;
      CasherRnDb.DatabaseName:=DBName;
      CasherRnDb.Connected:=True;

//      prFormMenuList(0); //������

      CommonSet.PrePrintPort:=prGetPrePrintPort;

    except
      ShowMessage('������ �������� ���� 1 - '+DBName);
      Close;
    end;
  end;
  with dmC1 do
  begin
    try
      CasherRnDb1.Connected:=False;
      CasherRnDb1.DatabaseName:=DBName;
      CasherRnDb1.Connected:=True;

      quSaleT.Active:=True;

    except
      ShowMessage('������ �������� ���� 2 - '+DBName);
      Close;
    end;
  end;

  with dmPC do
  begin
    try
      PCDb.Connected:=False;
      PCDb.DBName:=DBNamePC;

      PCDb.Open;
    except
      ShowMessage('������ �������� ���� 3');
      Close;
    end;
  end;

  if (SpecVal.CountStarts=-1)or(SpecVal.DateTo<Date) then TimerClose.Enabled:=True;
end;

procedure TfmMainCashRn.FormCreate(Sender: TObject);
Var StrWk:String;
begin

  CurDir := ExtractFilePath(ParamStr(0));
  Label8.Caption:='';
  Label9.Caption:='';
  Label2.Caption:='';
  Label3.Caption:='';
  Label4.Caption:='';

  Operation:=0; //�������� �������
  Label5.Caption:='�������� - ������� ��������.';

  ReadIni;

  CommonSet.PortCash:='COM'+its(CommonSet.CashPort);

  if iColor=1 then
  begin
    dxfBackGround1.BkColor.BeginColor:=$00E8CDB7; //��� ������� �����
    dxfBackGround1.BkColor.EndColor:=$0058371D;
    Panel1.Color:=$005E3C20;
    Panel2.Color:=$009D9D4F;
    RxClock1.Color:=$005E3C20;
  end;

  if iColor=2 then
  begin
    dxfBackGround1.BkColor.BeginColor:=$00A6A6A6; //��� ����� �����
    dxfBackGround1.BkColor.EndColor:=$00000000;
    Panel1.Color:=$00252525;
    Panel2.Color:=$00BBBBBB;
    RxClock1.Color:=$00000000;
  end;

//��������� �������� Nums

  Nums.ZNum:=CommonSet.CashZ;
  Nums.SerNum:='0';
  Nums.RegNum:='0';
  Nums.CheckNum:=INtToStr(CommonSet.CashChNum);
  Nums.iCheckNum:=CommonSet.CashChNum;
  Nums.iRet:=0;
  Nums.sRet:='���';
  Nums.sDate:=FormatDateTime(sFormatDate,Date);


  TimerDelayPrintQ.Interval:=CommonSet.iStartDelaySec;
  TimerDelayPrintQ.Enabled:=True;
  TiRefresh.Interval:=CommonSet.iRefreshTime*1000; //�������� � �����������
  TiRefresh.Enabled:=False;

  // ���������� �������

//SetWindowPos(FindWindow('Shell_TrayWnd', nil), 0, 0, Screen.Height-24, Screen.Width, 24, SWP_HIDEWINDOW );

  Prizma:=False;
  if CommonSet.Prizma=1 then
  begin
    if OpenDrv then
    begin
      Label12.Caption:='P+';
      Prizma:=True;
    end else
    begin
      Label12.Caption:='P-';
      Prizma:=False;
    end;
  end else Label12.Caption:='--';

  //�������� ����� ����� ��� ����������� �������
  if CommonSet.CashNum>0 then CommonSet.Fis:=1;

  if CommonSet.Fis>0 then
  begin
    try
      if CommonSet.CashPort>0 then
      begin
        StrWk:='COM'+IntToStr(CommonSet.CashPort);
        if not CashOpen(PChar(StrWk)) then   //dll �������
        begin
          CommonSet.Fis:=0;
          showmessage('������ �������� �����.');
        end;
      end
      else
      begin
        CommonSet.Fis:=0;
      end;
    except
      CommonSet.Fis:=0;
    end;
  end;

  if CommonSet.Fis>0 then //��������� ���� ������ - �������� �����
  begin
//    Label9.Caption:='���.�����  '+Nums.SerNum;
    Nums.iRet:=InspectSt(Nums.sRet);
    if (Nums.iRet=0) or (Nums.iRet=2)or(Nums.iRet=22)or(Nums.iRet=21)or(Nums.iRet=1) then Nums.bOpen:=True else
    begin
      CommonSet.Fis:=0;
      Nums.bOpen:=False;
      showmessage('������ �������� �����.');
    end;

    if CheckOpen then
    begin
      prWriteLog('!!CheckCancel; ��� ��������');
      CheckCancel;
      Nums.iRet:=InspectSt(Nums.sRet);
    end;
    if CommonSet.SpecChar=0 then
    begin
      GetSerial;
      CashDate(Nums.sDate);
      if GetNums then CommonSet.CashZ:=Nums.ZNum; //������
      GetRes; //�������
    end;
  end;

  WriteStatus;
  prWriteLog('!!OpenProgram; z-'+IntToStr(Nums.ZNum)+';  �-'+INtToStr(Nums.iCheckNum)+';');
  Event_RegEx(121,0,0,0,'','',0,0,0,0,0,0);
  
  StrPre:='';
  Person.Id:=0;
  Person.Name:='';

  TableImage:=TBitMap.Create;
  TableImage.LoadFromFile(CurDir+'Pict1.bmp');
  FGridBrush := TBrush.Create;

  if CommonSet.iQuestVisible=1 then
  begin
    TabViewQUESTS.Visible:=True;
    TabViewNAME.Visible:=False;
  end;
  if CommonSet.iQuestVisible=2 then
  begin
    TabViewQUESTS.Visible:=False;
    TabViewNAME.Visible:=True;
  end;
  if CommonSet.iQuestVisible=3 then
  begin
    TabViewQUESTS.Visible:=True;
    TabViewNAME.Visible:=True;
  end;

  Label1.Caption:='�������� ������ �� ��������� '+IntToStr(CommonSet.HourShift)+' �.';
end;

procedure TfmMainCashRn.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  SetWindowPos(FindWindow('Shell_TrayWnd', nil), 0, 0, Screen.Height-24, Screen.Width, 24, SWP_SHOWWINDOW);

  if CommonSet.Fis>0 then CashClose;

  TableImage.Free;
  FGridBrush.Free;
  Panel3.Visible:=False;
  dmC.quTabs.Active:=False;
  dmC.quPers.Active:=False;
  dmC.CasherRnDb.Connected:=False;
//  delay(500);
end;

procedure TfmMainCashRn.Panel2Click(Sender: TObject);
begin
  acExit.Execute;
end;

procedure TfmMainCashRn.AssignBrush(ABrush: TBrush; ABitMap: TBitMap);
begin
  FGridBrush.Bitmap := ABitMap;
  ABrush.Assign(FGridBrush);
end;


procedure TfmMainCashRn.TabViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);

var
  ARect: TRect;
  ATextToDraw: String;

  procedure SetTextToDraw;
  begin
    if (AViewInfo is TcxGridCardRowDataViewInfo) then
    begin
      ATextToDraw := AViewInfo.GridRecord.DisplayTexts[AViewInfo.Item.Index];
    end
    else
      ATextToDraw := VarAsType(AViewInfo.Item.Caption, varString);
  end;

begin
{ remove/add the closing brace on this line to disable/enable the following code}

  ARect := AViewInfo.Bounds;
//  ACanvas.Canvas.Font.Assign(FFont); ===
  SetTextToDraw;

  AssignBrush(ACanvas.Canvas.Brush, TableImage);

  ACanvas.Canvas.FillRect(ARect);

  SetBkMode(ACanvas.Canvas.Handle, TRANSPARENT);
 // SetBkMode(ACanvas.Canvas.Handle, 50); ===

  ACanvas.DrawText(ATextToDraw, AViewInfo.Bounds, 0);
  ADone := true;
end;

procedure TfmMainCashRn.Button2Click(Sender: TObject);
Var sMessage:String;
begin
//  acOpenTab.Execute;
  delay(10);
  if bPrintCheck then
  begin
    sMessage:='���������� ��������� ��������� ����.';
    fmAttention.Label1.Caption:=sMessage;
    prWriteLog('~~AttentionShow;'+sMessage);
    fmAttention.ShowModal;
    exit;
  end;
  fmSpec.Tag:=0;
  acOpen.Execute;
end;

procedure TfmMainCashRn.Button5Click(Sender: TObject);
begin
  if bPressB5Main then exit;
  if bPrintCheck then
  begin
    sMessage:='���������� ��������� ��������� ����.';
    fmAttention.Label1.Caption:=sMessage;
    prWriteLog('~~AttentionShow;'+sMessage);
    fmAttention.ShowModal;
    exit;
  end;
  bPressB5Main:=True;
  Button5.Enabled:=False;
  acCashEnd.Execute;
  bPressB5Main:=False;
end;

procedure TfmMainCashRn.Button6Click(Sender: TObject);
begin
  close;
end;

procedure TfmMainCashRn.Button1Click(Sender: TObject);
begin
  acCreateTab.Execute;
end;

procedure TfmMainCashRn.acCreateTabExecute(Sender: TObject);
Var StrWk,SCapt:String;
    iNumZ:INteger;
begin
  if  bPrintCheck then exit;
  if not CanDo('prAdd') then exit;

  TiRefresh.Enabled:=False; //���������� �������������� ����������

  fmCreateTab:=TfmCreateTab.Create(Application);
  with fmCreateTab do
  begin
    TextEdit1.Text:='0';
    CalcEdit1.EditValue:=0;
    iNumZ:=dmC1.GetNumZ;
    Label1.Caption:='�������� ����� � '+INtToStr(iNumZ)+' ?';

    dmC.quPersonal1.Active:=True;
    fmCreateTab.ComboBox1.EditValue:=Person.Id;
    if not CanDo('prEditAll') then
    begin
      fmCreateTab.ComboBox1.Properties.ReadOnly:=True;
    end;
    if CommonSet.IncNumTab>0 then
    begin
      StrWk:=IntToStr(CommonSet.IncNumTab);
      while Length(StrWk)<3 do StrWk:='0'+StrWk;
      fmCreateTab.TextEdit1.Text:=StrWk;
      inc(CommonSet.IncNumTab); WriteTabNum;
    end;
    fmCreateTab.ShowModal;
    if fmCreateTab.ModalResult=mrOk then
    begin
      Tab.Id_Personal:=fmCreateTab.ComboBox1.EditValue;
      Tab.Name:=fmCreateTab.ComboBox1.Text;
      Tab.OpenTime:=Now;
      Tab.NumTable:=fmCreateTab.TextEdit1.Text;
      Tab.Quests:=fmCreateTab.CalcEdit1.EditValue;
      Tab.iStatus:=0; //������� �� �����
      Tab.DBar:='';
      Tab.Id:=GetId('TabH');
      Tab.Summa:=0;
      Tab.iNumZ:=iNumZ;
      Check.Max:=0;

      FormLog('CreateTab',IntToStr(Tab.Id_Personal)+' '+Tab.NumTable+' '+IntToStr(Tab.iNumZ));
      prWriteLog('!!--CreateTab;'+IntToStr(Tab.Id_Personal)+';'+Tab.NumTable);

      with dmC do
      with dmC1 do
      begin
        quCurSpec.Active:=False;
        quCurSpec.ParamByName('IDT').AsInteger:=Tab.Id;
        quCurSpec.ParamByName('STATION').AsInteger:=CommonSet.Station;
        quCurSpec.Active:=True;
        quCurSpec.First;

        quCurMod.Active:=False;
        quCurMod.ParamByName('IDT').AsInteger:=Tab.Id;
        quCurMod.ParamByName('STATION').AsInteger:=CommonSet.Station;
        quCurMod.ParamByName('IDP').AsInteger:=0;
        quCurMod.Active:=True;

        quSaleT.First;
        fmSpec.cxButton17.Caption:=Copy(quSaleTNAMECS.AsString,1,15);
        fmSpec.cxButton17.Tag:=quSaleTID.AsInteger;
      end;

      fmSpec.SetStatus(SCapt);

      fmSpec.Label7.Caption:=Tab.Name;
      fmSpec.BEdit1.Text:=Tab.NumTable;
      if Tab.Quests>=0 then fmSpec.SEdit1.EditValue:=Tab.Quests else fmSpec.SEdit1.EditValue:=0;
      fmSpec.Label10.Caption:=FormatdateTime('dd.mm.yyyy hh:nn',Tab.OpenTime);
      fmSpec.Label11.Caption:=SCapt;
      fmSpec.Label17.Caption:='';

      fmSpec.ViewSpecDPROC.Visible:=False;
      fmSpec.ViewSpecDSum.Visible:=False;

      dmC.quPersonal1.Active:=False;
      fmCreateTab.Release;
      fmSpec.Label8.Caption:='����� � '+INtToStr(Tab.iNumZ);

      fmSpec.ShowModal;
    end
    else
    begin
      dmC.quPersonal1.Active:=False;
      fmCreateTab.Release;
    end;
  end;
//  delay(100);
  CreateViewPers(bChangeView);
end;

procedure TfmMainCashRn.acDelExecute(Sender: TObject);
Var StrWk:String;
begin

  if not CanDo('prDel') then exit;
  if dmC.quTabs.RecordCount=0 then exit;
  if not FindTab(dmC.quTabsID.AsInteger) then exit;

  if dmC.quTabsISTATUS.AsInteger=2 then begin showmessage('���� �������������. �������� � �������������� ���������.');  exit; end;

  TiRefresh.Enabled:=False; //���������� �������������� ����������

  fmMessDel:=TfmMessDel.Create(Application);
  if not CanDo('prDelNoSkl') then fmMessDel.cxButton1.Enabled:=False;
  if not CanDo('prDelWithSkl') then fmMessDel.cxButton2.Enabled:=False;

  with dmC do
  with dmC1 do
  begin
    fmMessDel.Label1.Caption:='����  '+quTabsNUMTABLE.AsString;
    Str(quTabsTABSUM.AsFloat:10:2,StrWk);
    fmMessDel.Label2.Caption:='����� '+StrWk;
    fmMessDel.CalcEdit1.Visible:=False;

    taDelPar.Active:=False;
    taDelPar.Active:=True;
    taDelPar.First;
    if taDelPar.RecordCount>0 then fmMessDel.cxLookupComboBox1.EditValue:=taDelParID.AsInteger
    else fmMessDel.cxLookupComboBox1.EditValue:=0;

    fmMessDel.ShowModal;

    if fmMessDel.ModalResult<>mrCancel then
    begin  //����������� ������������� ����
      Event_RegEx(25,Nums.iCheckNum,0,0,'','',0,0,quTabsTABSUM.AsFloat,quTabsTABSUM.AsFloat,0,0);
    end;

    if fmMessDel.ModalResult=mrYes then
    begin
      FormLog('DelTab0',IntToStr(Person.Id)+' '+quTabsID_Personal.AsString+' '+quTabsNUMTABLE.AsString+' '+quTabsNUMZ.AsString);
      prWriteLog('!!DelTab0;'+quTabsID.AsString+' '+IntToStr(Person.Id)+' '+quTabsID_Personal.AsString+' '+quTabsNUMTABLE.AsString+' '+quTabsNUMZ.AsString);

      prSaveToAll.ParamByName('ID_TAB').AsInteger:=quTabsID.AsInteger;
      prSaveToAll.ParamByName('OPERTYPE').AsString:='Del';
      prSaveToAll.ParamByName('CHECKNUM').AsInteger:=fmMessDel.cxLookupComboBox1.EditValue;
      prSaveToAll.ParamByName('SKLAD').AsInteger:=0; //�������� �����������
      prSaveToAll.ParamByName('STATION').AsInteger:=CommonSet.Station; //����� �������
      prSaveToAll.ParamByName('ID_PERSONCLOSE').AsInteger:=Person.Id; //��� ������
      prSaveToAll.ParamByName('SPBAR').AsString:='';
      prSaveToAll.ExecProc;

//       ������ ����
      quDelTab.Active:=False;
      quDelTab.ParamByName('Id').AsInteger:=quTabsID.AsInteger;

      trDel.StartTransaction;
      quDelTab.Active:=True;
      trDel.Commit;

      fmMainCashRn.CreateViewPers(True);
    end;
    if fmMessDel.ModalResult=mrNo then  //�������� ��� ��������
    begin
      FormLog('DelTab1',IntToStr(Person.Id)+' '+quTabsID_Personal.AsString+' '+quTabsNUMTABLE.AsString+' '+quTabsNUMZ.AsString);
      prWriteLog('!!DelTab1;'+quTabsID.AsString+' '+IntToStr(Person.Id)+' '+quTabsID_Personal.AsString+' '+quTabsNUMTABLE.AsString+' '+quTabsNUMZ.AsString);

      prSaveToAll.ParamByName('ID_TAB').AsInteger:=quTabsID.AsInteger;
      prSaveToAll.ParamByName('OPERTYPE').AsString:='Del';
      prSaveToAll.ParamByName('CHECKNUM').AsInteger:=fmMessDel.cxLookupComboBox1.EditValue;
      prSaveToAll.ParamByName('SKLAD').AsInteger:=1; //�������� �� �����������
      prSaveToAll.ParamByName('STATION').AsInteger:=CommonSet.Station; //����� �������
      prSaveToAll.ParamByName('ID_PERSONCLOSE').AsInteger:=Person.Id; //��� ������
      prSaveToAll.ParamByName('SPBAR').AsString:=''; //��� ��������� �����
      prSaveToAll.ExecProc;

      //������� ������ ���� �� ������ �.�. �������� ��������� �� ���� - ������ �� �����

      Tab.NumTable:=quTabsNUMTABLE.AsString;
      Tab.Id_Personal:=quTabsID_PERSONAL.AsInteger;

      quFindPers.Active:=False;
      quFindPers.ParamByName('IDP').AsInteger:=Tab.Id_Personal;
      quFindPers.Active:=True;
      Tab.Name:=quFindPersNAME.AsString;
      quFindPers.Active:=False;

      CloseTe(taServP);
//      taServP.Active:=False;
//      taServP.CreateDataSet;

      quCheck.Active:=False;
      quCheck.ParamByName('IDTAB').Value:=quTabsID.AsInteger;
      quCheck.Active:=True;

      quCheck.First;
      while not quCheck.Eof do
      begin
        if quCheckITYPE.AsInteger=0 then
        begin  //������ �����
          //���-�� ����� �������������� ��� ��� ������� -  ����� �������
          prAddMove(2,quCheckSIFR.AsInteger,quCheckQUANTITY.AsFloat);

          taServP.Append;
          taServPName.AsString:=quCheckNAME.AsString;
          taServPCode.AsString:=quCheckSIFR.AsString;
          taServPQuant.AsFloat:=quCheckQUANTITY.AsFloat;
          taServPStream.AsInteger:=quCheckSTREAM.AsInteger;
          taServPiType.AsInteger:=0; //�����
          taServP.Post;
        end;

        quCheck.Next;
      end;
      PrintServCh('������ ');

      quCheck.Active:=False;

//       ������ ����
      quDelTab.Active:=False;
      quDelTab.ParamByName('Id').AsInteger:=quTabsID.AsInteger;

      trDel.StartTransaction;
      quDelTab.Active:=True;
      trDel.Commit;

      fmMainCashRn.CreateViewPers(True);
    end;

    taDelPar.Active:=False;
  end;
  fmMessDel.Release;

  TiRefresh.Enabled:=True; //���������� �������������� ����������
end;

procedure TfmMainCashRn.Button3Click(Sender: TObject);
begin
  acDel.Execute;
end;

procedure TfmMainCashRn.acMoveExecute(Sender: TObject);
  //����������� �� ������� ���������
Var StrWk,StrWk1:String;  
begin
  if not CanDo('prMoveAll') then exit;
  if dmC.quTabs.RecordCount=0 then exit;
  if not FindTab(dmC.quTabsID.AsInteger) then exit;

  if dmC.quTabsISTATUS.AsInteger=2 then begin showmessage('���� �������������. �������� � �������������� ���������.');  exit; end;

  TiRefresh.Enabled:=False; //���������� �������������� ����������

  fmCreateTab:=TfmCreateTab.Create(Application);
  with fmCreateTab do
  begin
    TextEdit1.Text:=dmC.quTabsNUMTABLE.AsString;
    CalcEdit1.EditValue:=dmC.quTabsQUESTS.AsInteger;

    dmC.quPersonal1.Active:=True;
    fmCreateTab.ComboBox1.EditValue:=Person.Id;
    Label1.Caption:='���� ��������� �����?';
    fmCreateTab.Caption:='������� ������.';


    fmCreateTab.ShowModal;
    if fmCreateTab.ModalResult=mrOk then
    begin
      with dmC do
      begin

        Tab.Id:=quTabsID.AsInteger;
        Tab.Id_Personal:=fmCreateTab.ComboBox1.EditValue;
        Tab.NumTable:=fmCreateTab.TextEdit1.Text;
        Tab.Quests:=fmCreateTab.CalcEdit1.EditValue;
        Tab.iNumZ:=quTabsNUMZ.AsInteger;

        FormLog('MoveTab',IntToStr(Person.Id)+' '+quTabsID_Personal.AsString+' '+quTabsNUMTABLE.AsString+' '+intToStr(Tab.Id_Personal)+' '+Tab.NumTable+' '+IntToStr(Tab.iNumZ));
        prWriteLog('!!MoveTab;'+IntToStr(Tab.Id)+';'+IntToStr(Person.Id)+';'+Person.Name+';'+quTabsID_Personal.AsString+' '+quTabsNUMTABLE.AsString+' '+intToStr(Tab.Id_Personal)+' '+Tab.NumTable);

        quCheck.Active:=False;
        quCheck.ParamByName('IDTAB').Value:=Tab.Id;
        quCheck.Active:=True;

        quCheck.First;
        while not quCheck.Eof do
        begin
          PosCh.Name:=quCheckNAME.AsString;
          PosCh.Code:=quCheckCODE.AsString;

          StrWk:= Copy(PosCh.Name,1,19);
          while Length(StrWk)<19 do StrWk:=StrWk+' ';
          Str(quCheckQUANTITY.AsFloat:5:1,StrWk1);
          StrWk:=StrWk+' '+StrWk1;
          Str(quCheckPRICE.AsFloat:7:2,StrWk1);
          StrWk:=StrWk+' '+StrWk1;
          Str(quCheckSUMMA.AsFloat:8:2,StrWk1);
//          Str((quCheckPRICE.AsFloat*quCheckQUANTITY.AsFloat):8:2,StrWk1);
          StrWk:=StrWk+StrWk1+'�';
          prWriteLog('-----MovePos;'+StrWk);

          quCheck.Next;
        end;

        quCheck.Active:=False;


        TabView.BeginUpdate;

//        trUpdate.StartTransaction;
        quTabs.Edit;
        quTabsID_PERSONAL.AsInteger:=Tab.Id_Personal;
        quTabsNUMTABLE.AsString:=Tab.NumTable;
        quTabsQUESTS.AsInteger:=Tab.Quests;
        quTabs.Post;
//        trUpdate.Commit;
        TabView.EndUpdate;
        quTabs.Locate('ID',Tab.Id,[]);

        fmMainCashRn.CreateViewPers(True);
      end;
    end;
    dmC.quPersonal1.Active:=False;
    fmCreateTab.Release;
  end;

  TiRefresh.Enabled:=True; //���������� �������������� ����������
end;

procedure TfmMainCashRn.Button4Click(Sender: TObject);
begin
  acMove.Execute;
end;

procedure TfmMainCashRn.acRetPreExecute(Sender: TObject);
begin
  if not CanDo('prRetPre') then exit;
  if dmC.quTabs.RecordCount=0 then exit;
  if not FindTab(dmC.quTabsID.AsInteger) then exit;
  with dmC do
  begin
    if quTabsISTATUS.AsInteger>0 then // � ��������� �������� ��� ��������������
    begin
      TiRefresh.Enabled:=False; //���������� �������������� ����������

      Tab.Id:=quTabsID.AsInteger;

      FormLog('RetPre',IntToStr(Person.Id)+' '+quTabsID_Personal.AsString+' '+quTabsNUMTABLE.AsString+' '+quTabsNUMZ.AsString);

      TabView.BeginUpdate;

//      trUpdate.StartTransaction;
      quTabs.Edit;
      quTabsISTATUS.AsInteger:=0;
      quTabs.Post;
//      trUpdate.Commit;
      TabView.EndUpdate;
      quTabs.Locate('ID',Tab.Id,[]);

//      fmMainCashRn.CreateViewPers;
      quTabs.Refresh;

      TiRefresh.Enabled:=True; //���������� �������������� ����������
      
    end;
  end;
end;

procedure TfmMainCashRn.acCashEndExecute(Sender: TObject);
Var strWk,StrWk1:String;
    iRet,IdH,IdC:Integer;
    bCheckOk:Boolean;
    rDiscont,rSum,rDProc:Real;
    iSumPos,iSumTotal,iSumDisc,iSumIt:INteger;
    TabCh:TTab;
    iAvans,iSumA,iQ:INteger;
    iCurCheck,iCurStatus:Integer;
    StrP:String;
    iPC:INteger;
    nd:Smallint;
    sM:String;
    iPos:INteger;
    iSumTotalFis:INteger;
    i,k:Integer;
    iOrg:Integer;
    rSumA,rSumDt,rSumBn:Real;
    iFisCheck:Integer;
    rDiscDop:Real;

 procedure prClearCheck;
 begin
   fmAttention.Label1.Caption:='���� ��������� ���� � ��. ��������� ������������ ����!';
   fmAttention.Label2.Caption:='';
   fmAttention.Label3.Caption:='';

   fmAttention.ShowModal;
   Nums.iRet:=InspectSt(Nums.sRet);
   prWriteLog('~~AttentionShow;'+'���� ��������� ���� � ��. ��������� ������������ ����!');

   fmAttention.Label2.Caption:='����� ���������� ������ ������� "�����"';
   fmAttention.Label3.Caption:='��������� ������������ ��������� ��������.';

   prWriteLog('~~CheckCancel;');
   CheckCancel;
    //��������� �����
   bCheckOk:=False;
 end;

begin
  if not CanDo('prPrintCheck') then begin
    Button5.Enabled:=True; //������ ������� ��������
    bPressB5Main:=False;
    exit;
  end;
  TestPrint:=1;
  While PrintQu and (TestPrint<=MaxDelayPrint) do
  begin
//        showmessage('������� �����, ��������� �������.');
    prWriteLog('������������� ������ ���� � �������.');
    delay(1000);
    inc(TestPrint);
  end;

  with dmC do
  begin
    if quTabs.RecordCount=0 then begin Button5.Enabled:=True; bPressB5Main:=False; exit; end;
    if not FindTab(quTabsID.AsInteger) then begin Button5.Enabled:=True; bPressB5Main:=False; exit; end;
    if quTabsISTATUS.AsInteger=2 then begin showmessage('���� �������������. �������� � �������������� ���������.'); Button5.Enabled:=True; bPressB5Main:=False; exit; end;

    iCurStatus:=quTabsISTATUS.AsInteger;

    if (CommonSet.MustPrePrint=1)and(quTabsISTATUS.AsInteger=0)then
    begin
      ShowMessage('�������� ������� ����.');
      Button5.Enabled:=True; //������ ������� ��������
      bPressB5Main:=False;
      exit;
    end;

    if More24H(iRet) then
    begin
      fmAttention.Label1.Caption:='������ ����� 24 �����. ���������� ������� �����.';
      prWriteLog('~~AttentionShow;'+'������ ����� 24 �����. ���������� ������� �����.');
      fmAttention.ShowModal;
      Button5.Enabled:=True; //������ ������� ��������
      bPressB5Main:=False;
      Exit;   //������� , �� ���� �� ������ �� ���������� ������ �� ����
    end;

    TabCh.Id:=quTabsID.AsInteger;
    TiRefresh.Enabled:=False; //���������� �������������� ����������

    //�������� ������� ����� ������ 0 - ���   1-����   2 - ������

    quTabs.Edit;
    quTabsISTATUS.AsInteger:=2;
    quTabs.Post;
    quTabs.Refresh;

    //���� ��������� �������� �� ������ �������� iAvans
    // 1 ����� ������� ������ ����� ����  ---- ������������ ������
    // 2 ����� ������ = ����� ����  ----- TabCh.Summa=0
    // 3 ����� ������ < ����� ����  ----- �� ���������� ����� ���� �������� ���

    quCheck.Active:=False;
    quCheck.ParamByName('IDTAB').Value:=TabCh.Id;
    quCheck.Active:=True;

    rSum:=0; iSumA:=0;
    iAvans:=0;
    quCheck.First;
    while not quCheck.Eof do
    begin
      rSum:=rSum+quCheckSUMMA.AsFloat;
      if quCheckPRICE.AsFloat<0 then iSumA:=RoundEx(quCheckSUMMA.AsFloat*100);
      quCheck.Next;
    end;
    if iSumA<0 then //������ �� ���� � ������
    begin
      rSum:=rv(rSum);
      if abs(rSum)>0.001 then iAvans:=3; // 3 ����� ������ < ����� ����  ----- �� ���������� ����� ���� �������� ���
      if abs(rSum)<0.001 then iAvans:=2; // 2 ����� ������ = ����� ����
      if rSum<0 then iAvans:=1; // 1 ����� ������� ������ ����� ����  ---- ������������ ������
      if not CanDo('prAvansCheck') then
      begin
        fmAttention.Label1.Caption:='��� ���� ��� ������ � �������� !!!.';
        prWriteLog('~~AttentionShow;'+'��� ���� ��� ������ � �������� !!!.');
        fmAttention.ShowModal;
        Button5.Enabled:=True; //������ ������� ��������
        bPressB5Main:=False;

        TiRefresh.Enabled:=True; //���������� �������������� ����������

        quTabs.Edit;
        quTabsISTATUS.AsInteger:=iCurStatus;
        quTabs.Post;
        quTabs.Refresh;

        exit;
      end;
    end;

    rDiscDop:=0; //��� ��� ��������� �������� ���. ������ ����� �� ��������� ��� ������ DiscToPrice=1

    TabView.BeginUpdate;
    quTabs.Refresh; //���� ������� ��� �� ������� �� ������ ������� - ������ ���-�� �����������
    TabView.EndUpdate;
    delay(10);

    TabCh.Id_Personal:=quTabsID_PERSONAL.AsInteger;
    taPersonal.Active:=true;
    taPersonal.Locate('Id',TabCh.Id_Personal,[]);
    TabCh.Name:=taPersonalNAME.AsString;
    Tab.Name:=TabCh.Name; //����� ��� ������ ��������� �� ���� � �������
    taPersonal.Active:=False;

    TabCh.OpenTime:=quTabsBEGTIME.AsDateTime;
    TabCh.NumTable:=quTabsNUMTABLE.AsString;
    TabCh.Quests:=quTabsQUESTS.AsInteger;
    TabCh.iStatus:=quTabsISTATUS.AsInteger;
    TabCh.DBar:=quTabsDISCONT.AsString;
    TabCh.Summa:=quTabsTABSUM.AsFloat;

    FindDiscount(TabCh.DBar); //��� ������������ Tab.DBar, Tab.DPercent, Tab.DName, Tab.DType

    if TabCh.Summa<>rSum then
    begin  //������������ ���������� �� ��������� - ��� ������� ��-�� ����������� �������
           //���� �������� ���������
      TabView.BeginUpdate;
      quTabs.Refresh; //���� ������� ��� �� ������� �� ������ ������� - ������ ���-�� �����������
      TabView.EndUpdate;
      delay(10);

      TabCh.Summa:=rSum;
    end;

    prWriteLog('--RaschMain;'+IntToStr(TabCh.Id)+';'+TabCh.NumTable+';'+IntToStr(TabCh.Id_Personal)+';'+TabCh.Name+';'+fts(TabCh.Summa)+';'+TabCh.DBar+';');

    if TabCh.Summa<0.01 then
    begin
      if iAvans<2 then  //��������, ��� �������� ������
      begin
        fmAttention.Label1.Caption:='��� � ������� � ������������� ������ �������� ������ !!!.';
        prWriteLog('~~AttentionShow;'+'��� � ������� � ������������� ������ �������� ������ !!!.');
        fmAttention.ShowModal;
        Button5.Enabled:=True; //������ ������� ��������
        bPressB5Main:=False;

        quTabs.Edit;
        quTabsISTATUS.AsInteger:=iCurStatus;
        quTabs.Post;
        quTabs.Refresh;

        TiRefresh.Enabled:=True; //���������� �������������� ����������
        Exit;   //������� , �� ���� �� ������ �� ���������� ������ �� ����
      end;
    end;

    if (Operation=1) and (iAvans>1) then
    begin
      fmAttention.Label1.Caption:='������ �������� ��� �������� (������)!!!.';
      prWriteLog('~~AttentionShow;'+'������ �������� ��� �������� (������)!!!.');
      fmAttention.ShowModal;
      Button5.Enabled:=True; //������ ������� ��������
      bPressB5Main:=False;

      quTabs.Edit;
      quTabsISTATUS.AsInteger:=iCurStatus;
      quTabs.Post;
      quTabs.Refresh;

      TiRefresh.Enabled:=True; //���������� �������������� ����������
      Exit;   //������� , �� ���� �� ������ �� ���������� ������ �� ����
    end;

    taModif.Active:=True;

    {
    //����������� �� ��������� ���� - ����� � �������� ���� ����

    if iAvans=2 then // 2 ����� ������ = ����� ����  ������� ������ ������������ ��������
    begin
      PosCh.Name:='';
      PosCh.Code:='';
      PosCh.Price:=0; //� ��������
      PosCh.Count:=0; //� �������
      PosCh.Sum:=0;

      try
//        if CommonSet.PrePrintPort='fisprim' then
        if pos('fis',CommonSet.PrePrintPort)>0 then
        begin
          OpenNFDoc;

          SelectF(13);PrintNFStr(' '+CommonSet.DepartName); PrintNFStr('');
          SelectF(14); PrintNFStr('       ������');
          SelectF(13); PrintNFStr('');
          PrintNFStr('��������: '+TabCh.Name);
          PrintNFStr('����: '+TabCh.NumTable);
          SelectF(13);PrintNFStr('������: '+IntToStr(TabCh.Quests));
          PrintNFStr('������: '+FormatDateTime('dd.mm.yyyy hh:nn:ss',TabCh.OpenTime));
          SelectF(13);StrWk:='                                          ';
          PrintNFStr(StrWk);
          SelectF(15);StrWk:=' ��������          ���-��   ����   ����� ';
          PrintNFStr(StrWk);
          SelectF(13);StrWk:='                                          ';
          PrintNFStr(StrWk);
          SelectF(15); PrintNFStr(' ');PrintNFStr(' ');

          iSumA:=iSumA*(-1);
          Str((rSum+iSumA/100):10:2,StrWk);
          StrWk:='����� ����� �����:    '+StrWk+' ���';
          PrintNFStr(StrWk); PrintNFStr(' ');PrintNFStr(' ');

          Str((iSumA/100):10:2,StrWk);
          StrWk:='������ ����� �� �����:'+StrWk+' ���';
          PrintNFStr(StrWk); PrintNFStr(' ');PrintNFStr(' ');

          StrWk:='����� �� �����       :      0,00 ���';
          PrintNFStr(StrWk); PrintNFStr(' ');

          SelectF(13);StrWk:='                                          ';
          PrintNFStr(StrWk);
          SelectF(11);
          PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');
          CloseNFDoc;
          if CommonSet.TypeFis<>'sp101' then CutDoc;
        end;
        if pos('DB',CommonSet.PrePrintPort)>0 then
        begin
        //��������� �������
          with dmC1 do
          begin
            quPrint.Active:=False;
            quPrint.Active:=True;
            iQ:=GetId('PQH'); //����������� ������� �� 1-�

            PrintDBStr(iQ,999,CommonSet.PrePrintPort,' '+CommonSet.DepartName,13,0);
            PrintDBStr(iQ,999,CommonSet.PrePrintPort,'',13,0);
            PrintDBStr(iQ,999,CommonSet.PrePrintPort,'       ������',14,0);
            PrintDBStr(iQ,999,CommonSet.PrePrintPort,'',13,0);
            PrintDBStr(iQ,999,CommonSet.PrePrintPort,'��������: '+TabCh.Name,13,0);
            PrintDBStr(iQ,999,CommonSet.PrePrintPort,'����: '+TabCh.NumTable,13,0);
            if Tab.DBar>'' then
            begin
              Str(Tab.DPercent:5:2,StrWk);
              PrintDBStr(iQ,999,CommonSet.PrePrintPort,'�����: '+Tab.DName+' ('+StrWk+'%)',13,0);
            end;
            PrintDBStr(iQ,999,CommonSet.PrePrintPort,'������: '+IntToStr(TabCh.Quests),13,0);
            PrintDBStr(iQ,999,CommonSet.PrePrintPort,'������: '+FormatDateTime('dd.mm.yyyy hh:nn:ss',TabCh.OpenTime),13,0);
            PrintDBStr(iQ,999,CommonSet.PrePrintPort,'-',13,0);
            PrintDBStr(iQ,999,CommonSet.PrePrintPort,' ��������      ���-��   ����   �����',15,0);
            PrintDBStr(iQ,999,CommonSet.PrePrintPort,'-',13,0);
            PrintDBStr(iQ,999,CommonSet.PrePrintPort,'',13,0);
            PrintDBStr(iQ,999,CommonSet.PrePrintPort,'',13,0);


            iSumA:=iSumA*(-1);
            Str((rSum+iSumA/100):10:2,StrWk);
            StrWk:='����� ����� �����:    '+StrWk+' ���';
            PrintDBStr(iQ,999,CommonSet.PrePrintPort,StrWk,15,0);
            PrintDBStr(iQ,999,CommonSet.PrePrintPort,'',13,0);
            PrintDBStr(iQ,999,CommonSet.PrePrintPort,'',13,0);

            Str((iSumA/100):10:2,StrWk);
            StrWk:='������ ����� �� �����:'+StrWk+' ���';
            PrintDBStr(iQ,999,CommonSet.PrePrintPort,StrWk,15,0);
            PrintDBStr(iQ,999,CommonSet.PrePrintPort,'',13,0);
            PrintDBStr(iQ,999,CommonSet.PrePrintPort,'',13,0);

            StrWk:='����� �� �����       :      0,00 ���';
            PrintDBStr(iQ,999,CommonSet.PrePrintPort,StrWk,15,0);
            PrintDBStr(iQ,999,CommonSet.PrePrintPort,'',13,0);

            PrintDBStr(iQ,999,CommonSet.PrePrintPort,'-',13,0);

            prAddPrintQH(999,iQ,FormatDateTime('dd.mm hh:nn:sss',now)+', ������� - '+IntToStr(CommonSet.Station)+','+CommonSet.PrePrintPort);

            quPrint.Active:=False;
          end;
        end;

      finally

       //�������� � ������ ����� ��� ���������� ��������� - ��� �� �������

        prSaveToAll.ParamByName('ID_TAB').AsInteger:=TabCh.Id;
        prSaveToAll.ParamByName('OPERTYPE').AsString:='Sale';
        prSaveToAll.ParamByName('CHECKNUM').AsInteger:=0;
        prSaveToAll.ParamByName('SKLAD').AsInteger:=0; //�������� �����������
        prSaveToAll.ParamByName('STATION').AsInteger:=CommonSet.Station; //����� �������
        prSaveToAll.ParamByName('ID_PERSONCLOSE').AsInteger:=Person.Id; //��� ������
        prSaveToAll.ParamByName('SPBAR').AsString:=''; //��� ��������� �����, ��� ��� ���� �� ���� ������
        prSaveToAll.ExecProc;

        if Tab.DType=3 then prWritePCSum(Tab.DBar,rv(CalcBonus(Tab.Id,Tab.DPercent)),prSaveToAll.ParamByName('ID_H').AsInteger);

         // ������ ����
        quDelTab.Active:=False;
        quDelTab.ParamByName('Id').AsInteger:=TabCh.Id;

        trDel.StartTransaction;
        quDelTab.Active:=True;
        trDel.Commit;
        //��� ������ �� ���� �.�. ����� =0
      end;
//      quTabs.Refresh;
      fmMainCashRn.CreateViewPers(True);
      Button5.Enabled:=True; //������ ������� ��������
      bPressB5Main:=False;

      TiRefresh.Enabled:=True; //���������� �������������� ����������
      exit;
    end;
    }

    //������ �������� iAvans:=3; ����� ������ < ����� ���� ��� iAvans:=2 -- ��� �� ������

    //����� ������������� ����� ������������ ����� �� ���� ������
    for i:=1 to 10 do
      for k:=1 to 10 do aCheckPlat[i,k]:=0;  //������

    for i:=1 to 10 do aCheckBn[i]:=0; //������ �� ������������

    //��������� ������������ �� ������������
    quCheckOrgItog.Active:=False;
    quCheckOrgItog.ParamByName('IDTAB').AsInteger:=TabCh.Id;
    quCheckOrgItog.Active:=True;

    quCheckOrgItog.First;
    while not quCheckOrgItog.Eof do
    begin
      aCheckPlat[quCheckOrgItogIORG.AsInteger,1]:=quCheckOrgItogRSUM.AsFloat; // 1 - ����� � ������
      aCheckPlat[quCheckOrgItogIORG.AsInteger,2]:=quCheckOrgItogDSUM.AsFloat; // 2 - ����� ������
      quCheckOrgItog.Next;
    end;

    quCheckOrgAvans.Active:=False;
    quCheckOrgAvans.ParamByName('IDTAB').AsInteger:=TabCh.Id;
    quCheckOrgAvans.Active:=True;

    rSumA:=Abs(quCheckOrgAvansRSUM.AsFloat);

    quCheckOrgAvans.Active:=False;

    //����������� �����  iSumA
    for i:=1 to 10 do
    begin
      if rSumA<=aCheckPlat[i,1] then
      begin
        aCheckPlat[i,3]:=rSumA; //1-����� � ������, 2-�������, 3-�����, 4 - �����, 5- ���������� �����
        rSumA:=0;
      end else
      begin
        aCheckPlat[i,3]:=aCheckPlat[i,1]; //1-����� � ������, 2-�������, 3-�����, 4 - �����, 5- ���������� �����
        rSumA:=rSumA-aCheckPlat[i,1];
      end;
      if rSumA=0 then Break;
    end;

    quCheckOrgItog.FullRefresh; //��������� ����� � ������ � ��������� ���� ITSUM

    with fmCash do
    begin
      TabCh.Summa:=RoundSpec(TabCh.Summa,TabCh.rDiscDop);

      Caption:='������';
      Label5.Caption:='���. '+TabCh.Name+'  ���� - '+TabCh.NumTable;
      Label6.Caption:='����� - '+Floattostr(TabCh.Summa)+' �.';
      cEdit1.EditValue:=TabCh.Summa;
      cEdit2.EditValue:=TabCh.Summa;
      cEdit3.EditValue:=0;
      cEdit2.SelectAll;
    end;

    fmCash.ShowModal;
    if fmCash.ModalResult=mrOk then
    begin
      try
        bCheckOk:=False;
        bPrintCheck:=False; //��� �� 2-��� �������
        bPrintCheck:=True;
        PrintFCheck:=True; //��� ���� � ������� ������� ��������� ������
        iPos:=1;


        //��������� ��������� �����
        { ��������� ���� �� ������������
        iPC:=0; //�� ��������� ��������� ���� ���

        if fmCash.CEdit6.value>0 then
        begin
          prFindPCardBalans(SalePC.BarCode,SalePC.rBalans,SalePC.rBalansDay,SalePC.DLimit,SalePC.CliName,SalePC.CliType);
          if SalePC.rBalansDay>=fmCash.CEdit6.value then
          begin
            if fmCash.CEdit6.value=fmCash.CEdit1.value then iPc:=2 else iPc:=1;  //���� ������ ������������, ���� ���������� , �� ��������
           // iPc:=2  ������������ �������
          end;
        end;
        }
        //����������� ,������
        for i:=1 to 10 do aCheckPlat[i,5]:=aCheckBn[i];

        //����������� ����� c ������ ������ � �������
        rSumDt:=Abs(RV(fmCash.CEdit6.value));
        for i:=1 to 10 do
        begin
          if rSumDt<=(aCheckPlat[i,1]-aCheckPlat[i,3]-aCheckPlat[i,5]) then
          begin
            aCheckPlat[i,4]:=rSumDt; //1-����� � ������, 2-�������, 3-�����, 4 - �����, 5- ���������� �����
            rSumDt:=0;
          end else
          begin
            aCheckPlat[i,4]:=(aCheckPlat[i,1]-aCheckPlat[i,3]-aCheckPlat[i,5]); //1-����� � ������, 2-�������, 3-�����, 4 - �����, 5- ���������� �����
            rSumDt:=rSumDt-((aCheckPlat[i,1]-aCheckPlat[i,3])-aCheckPlat[i,5]);
          end;
          if rSumDt=0 then Break;
        end;

        //������� ������� �� ������������
        for i:=1 to 10 do
          if aCheckPlat[i,1]>0 then prWriteLog(' ORG:'+its(i)+' RSUM:'+fts(aCheckPlat[i,1])+' DSUM:'+fts(aCheckPlat[i,2])+' ASUM:'+fts(aCheckPlat[i,3])+' DTSUM:'+fts(aCheckPlat[i,4])+' BNSUM'+fts(aCheckPlat[i,5]));

        quCheckOrgItog.First;
        while not quCheckOrgItog.Eof do
        begin
          BnStr:=''; //�� ������ ���� �� ������� �������� ���-�� ��������
          iPayType:=0; //��� ����������� ����� ��� ������� ������� �����������
          iOrg:=quCheckOrgItogIORG.AsInteger;

          if quCheckOrgItogRSUM.AsFloat>0 then //�� ������ ����������� ����� �������� ���
          begin
            prGetCashPar(quCheckOrgItogIORG.AsInteger); //����������� ��� CommonSet �� �����

            //������� ������������ �� �����������
            quCheckOrg.Active:=False;
            quCheckOrg.ParamByName('IDTAB').AsInteger:=TabCh.Id;
            quCheckOrg.ParamByName('IORG').AsInteger:=quCheckOrgItogIORG.AsInteger;
            quCheckOrg.Active:=True;

            iPC:=0; //�� ��������� ��������� ���� ���

            //������ �� ��������� ������
            if aCheckPlat[iOrg,4]>0 then    //1-����� � ������, 2-�������, 3-�����, 4 - �����, 5- ���������� �����
            begin
              iPC:=1;
              if aCheckPlat[iOrg,4]>=(aCheckPlat[iOrg,1]-aCheckPlat[iOrg,3]) then iPC:=2; //��������� ��������� �������
            end;

            //� �����
            if (CommonSet.Fis>0)and((aCheckPlat[iOrg,1]-aCheckPlat[iOrg,3]-aCheckPlat[iOrg,4])>0.001) then iFisCheck:=1 else iFisCheck:=0;

            //������� ���
            if (CommonSet.Fis>0)and(CommonSet.SpecChar=0)and(iFisCheck=1)and(iPC<2) then
            begin

           //�������� ������� �������� �����
              CashDriver;
              GetNums;

//              CutDoc;
              OpenNFDoc;

              if SpecVal.SPH1>'' then PrintNFStr(SpecVal.SPH1);
              if SpecVal.SPH2>'' then PrintNFStr(SpecVal.SPH2);
              if SpecVal.SPH3>'' then PrintNFStr(SpecVal.SPH3);
              if SpecVal.SPH4>'' then PrintNFStr(SpecVal.SPH4);
              if SpecVal.SPH5>'' then PrintNFStr(SpecVal.SPH5);

              Case Operation of
              0: begin
    //             prWriteLog('!!CheckStart;'+IntToStr((Nums.iCheckNum+1))+';'+CurPos.Articul+';'+CurPos.Bar+';'+FloatToStr(CurPos.Quant)+';'+FloatToStr(CurPos.Price)+';'+FloatToStr(CurPos.DSum)+';');
                   prWriteLog('!!CheckStart;'+IntToStr((Nums.iCheckNum+1))+';'+IntToStr(Person.Id)+';'+Person.Name+';');
                   Event_RegEx(4,Nums.iCheckNum,0,0,'','',0,0,0,0,0,0); //������ ����

                   if CheckStart = False then
                   begin
                     TestStatus('CheckStart',sMessage);
                     fmAttention.Label1.Caption:=sMessage;
                     prWriteLog('~~AttentionShow;'+sMessage);
                     fmAttention.ShowModal;
                     CheckCancel;

                     Button5.Enabled:=True; //������ ������� ��������

                     quTabs.Edit;
                     quTabsISTATUS.AsInteger:=iCurStatus;
                     quTabs.Post;
                     quTabs.Refresh;

                     bPressB5Main:=False;
                     TiRefresh.Enabled:=True; //���������� �������������� ����������
                     exit;
                   end;
                 end;
              1: begin
                   prWriteLog('!!CheckRetStart;'+IntToStr((Nums.iCheckNum+1))+';'+IntToStr(Person.Id)+';'+Person.Name+';');
                   Event_RegEx(46,Nums.iCheckNum,0,0,'','',0,0,0,0,0,0);
                   if CheckRetStart = False then
                   begin
                     TestStatus('CheckRetStart',sMessage);
                     fmAttention.Label1.Caption:=sMessage;
                     prWriteLog('~~AttentionShow;'+sMessage);
                     fmAttention.ShowModal;
                     CheckCancel;

                     quTabs.Edit;
                     quTabsISTATUS.AsInteger:=iCurStatus;
                     quTabs.Post;
                     quTabs.Refresh;

                     Button5.Enabled:=True; //������ ������� ��������
                     bPressB5Main:=False;
                     TiRefresh.Enabled:=True; //���������� �������������� ����������
                     exit;
                   end;
                 end;
              end;

              rDiscont:=0;
              if (aCheckPlat[iOrg,3]=0)and(aCheckPlat[iOrg,4]=0) then   //����������� ������� ��� ������� � ������
              begin
                rSum:=0;
                quCheckOrg.First; iPos:=1;
                while not quCheckOrg.Eof do
                begin      //��������� �������
                  PosCh.Name:=quCheckOrgNAME.AsString;
                  PosCh.Code:=quCheckOrgSIFR.AsString;
                  PosCh.AddName:='';
                  PosCh.Price:=RoundEx(quCheckOrgPRICE.AsFloat*100);
                  PosCh.Count:=RoundEx(quCheckOrgQUANTITY.AsFloat*1000);
                  PosCh.Sum:=RoundEx(quCheckOrgPRICE.AsFloat*quCheckOrgQUANTITY.AsFloat*100)/100;
                  PosCh.RSum:=quCheckOrgSUMMA.AsFloat;
                  PosCh.DSum:=quCheckOrgDISCOUNTSUM.AsFloat;
                  if TabCh.rDiscDop>0 then    //�������� ���.������ �� ������ ������� > �����
                    if PosCh.RSum>=1 then
                    begin
                      PosCh.RSum:=PosCh.RSum-TabCh.rDiscDop;
                      rDiscDop:=TabCh.rDiscDop;
                      TabCh.rDiscDop:=0;
                    end;

                  rDiscont:=rDiscont+quCheckOrgDISCOUNTSUM.AsFloat; //������ ������ �� ���� ��������
                  rSum:=rSum+quCheckOrgSUMMA.AsFloat;

                  Event_RegEx(6,Nums.iCheckNum,iPos,quCheckOrgSIFR.AsInteger,'',PosCh.Name,PosCh.Price,quCheckOrgQUANTITY.AsFloat,PosCh.Sum,TabCh.Summa,quCheckOrgDISCOUNTPROC.AsFloat,quCheckOrgDISCOUNTSUM.AsFloat);

                  quCheckOrg.Next;
                  if not quCheckOrg.Eof then
                  begin
                    while quCheckOrgITYPE.AsInteger=1 do
                    begin  //������������
                      if quCheckOrg.Eof then break;
                      if quCheckOrgSifr.AsInteger>0 then
                        if taModif.Locate('SIFR',quCheckOrgSifr.AsInteger,[]) then
                          PosCh.AddName:=PosCh.AddName+'|   '+taModifNAME.AsString;
                      quCheckOrg.Next;
                    end;
                    delete(PosCh.AddName,1,1);
                  end;

                  prWriteLog('~!CheckAddPos; � ���� '+IntToStr((Nums.iCheckNum+1))+'; ��� '+PosCh.Code+'; ���-�� '+FloatToStr(PosCh.Count)+'; ���� '+FloatToStr(PosCh.Price)+'; ����� '+FloatToStr(PosCh.Sum)+';');

                  if PosCh.RSum>=0.01 then   //������� ������� �������� ������
                  begin
                    CheckAddPos(iSumPos);  inc(iPos);
                    while TestStatus('CheckAddPos',sMessage)=False do
                    begin
                      fmAttention.Label1.Caption:=sMessage;
                      prWriteLog('~~AttentionShow;'+sMessage);
                      fmAttention.ShowModal;
                      Nums.iRet:=InspectSt(Nums.sRet);
                    end;
                  end else prWriteLog('~!CheckAddPosBad; � ���� '+IntToStr((Nums.iCheckNum+1))+'; ��� '+PosCh.Code+'; ���-�� '+FloatToStr(PosCh.Count)+'; ���� '+FloatToStr(PosCh.Price)+'; ����� '+FloatToStr(PosCh.Sum)+';');
                end;
              end else //���� ���� ������  ��� ��������� ����� �� ��� ����� �������
              begin
                if aCheckPlat[iOrg,3]>0 then
                begin
                  PosCh.Name:='� ������ �� �����';
                  PosCh.Code:='';
//                  iSumA:=iSumA*(-1);
                  Str((aCheckPlat[iOrg,1]-aCheckPlat[iOrg,3]-aCheckPlat[iOrg,4]):10:2,StrWk);
                  PosCh.AddName:='|����� ����� �����:    '+StrWk+'|������ ����� �� �����:';
                  Str((aCheckPlat[iOrg,3]):10:2,StrWk);
                  PosCh.AddName:=PosCh.AddName+StrWk+'| ';
                  PosCh.Price:=RoundEx(rSum*100); //� ��������
                  PosCh.Count:=1000; //� �������
                  PosCh.Sum:=RoundEx(rSum*100)/100;

                  Event_RegEx(6,Nums.iCheckNum,1,0,'',PosCh.Name,PosCh.Price,1,PosCh.Sum,TabCh.Summa,0,0);

                  CheckAddPos(iSumPos);
                  while TestStatus('CheckAddPos',sMessage)=False do
                  begin
                    fmAttention.Label1.Caption:=sMessage;
                    prWriteLog('~~AttentionShow;'+sMessage);
                    fmAttention.ShowModal;
                    Nums.iRet:=InspectSt(Nums.sRet);
                  end;

                end;

                if iPC<>0 then  //�������� ��������� ������
                begin
                  PosCh.Name:='� ������ ';
                  PosCh.Code:='';

                  Str(rv(aCheckPlat[iOrg,1]-aCheckPlat[iOrg,3]-aCheckPlat[iOrg,4]):10:2,StrWk);
                  PosCh.AddName:='|����� ����� ����:    '+StrWk+'|';
                  Str(rv(aCheckPlat[iOrg,4]):10:2,StrWk);
                  PosCh.AddName:=PosCh.AddName+'�������� '+Copy(SalePC.CliName,1,20)+':'+StrWk+'| ';
                  Str(rv(SalePC.rBalans-fmCash.cEdit6.value):10:2,StrWk);
                  PosCh.AddName:=PosCh.AddName+'������ �� �����:'+ StrWk+'| ';
                  Str(rv(SalePC.rBalansDay-fmCash.cEdit6.value):10:2,StrWk);
                  PosCh.AddName:=PosCh.AddName+'������� �������� ������:'+ StrWk+'| ';

                  PosCh.Price:=RoundEx(rv(aCheckPlat[iOrg,1]-aCheckPlat[iOrg,3]-aCheckPlat[iOrg,4])*100); //� ��������
                  PosCh.Count:=1000; //� �������
                  PosCh.Sum:=RoundEx(rv(aCheckPlat[iOrg,1]-aCheckPlat[iOrg,3]-aCheckPlat[iOrg,4])*100)/100; //�� ������������ � CheckAddPos(iSumPos);

                  Event_RegEx(6,Nums.iCheckNum,1,0,'',PosCh.Name,PosCh.Price,1,PosCh.Sum,TabCh.Summa,0,0);

                  CheckAddPos(iSumPos);
                  while TestStatus('CheckAddPos',sMessage)=False do
                  begin
                    fmAttention.Label1.Caption:=sMessage;
                    prWriteLog('~~AttentionShow;'+sMessage);
                    fmAttention.ShowModal;
                    Nums.iRet:=InspectSt(Nums.sRet);
                  end;

                  rSum:=PosCh.Sum;
                end;

              end;

              //������������ ���������� - ������ ����, ������, ������;
              CheckTotal(iSumTotal);
              prWriteLog('!!AfterCheckTotal;'+IntToStr((Nums.iCheckNum+1))+';'+inttostr(iSumTotal)+';');

              iSumTotalFis:=iSumTotal;

              Event_RegEx(26,Nums.iCheckNum,iPos,0,'','',0,0,0,iSumTotalFis/100,0,0);

              while TestStatus('CheckTotal',sMessage)=False do
              begin
                fmAttention.Label1.Caption:=sMessage;
                prWriteLog('~~AttentionShow;'+sMessage);
                fmAttention.ShowModal;
                Nums.iRet:=InspectSt(Nums.sRet);
              end;

              rDiscont:=aCheckPlat[iOrg,2];

              iSumIt:=0; iSumDisc:=0;
              if (abs(rDiscont)>=0.01) and (CommonSet.DiscToPrice=0) then //������� ����� ������ ��� ������� ���� � ����� ������ � ���� ��������
              begin
                CheckDiscount(rDiscont,iSumDisc,iSumIt); //� ������ iSumIt=0 ������
                prWriteLog('!!AfterCheckDisc;'+IntToStr((Nums.iCheckNum+1))+';'+FloatToStr(rDiscont)+';'+IntToStr(iSumDisc)+';'+IntToStr(iSumIt)+';');

                Event_RegEx(35,Nums.iCheckNum,0,0,'','',0,0,0,iSumTotalFis/100,Tab.DPercent,rDiscont);

                while TestStatus('CheckDiscount',sMessage)=False do
                begin
                  fmAttention.Label1.Caption:=sMessage;
                  prWriteLog('~~AttentionShow;'+sMessage);
                  fmAttention.ShowModal;
                  Nums.iRet:=InspectSt(Nums.sRet);
                end;
              end;
              if (abs(rDiscont)>=0.01) and (CommonSet.DiscToPrice=1) then //������� ����� ������ ��� ������� ���� � ����� ������ � ���� �������
              begin
                rDiscont:=rDiscont+rDiscDop;
                Str(rDiscont:8:2,StrWk);
                StrWk:='� �.�. ������������� ������ '+StrWk;
                PrintNFStr(StrWk);
              end;

              if TabCh.rDiscDop>0.001 then
              begin
                CheckDiscount(TabCh.rDiscDop,iSumDisc,iSumIt);
                iSumTotal:=iSumIt;
                prWriteLog('!!AfterCheckDopDisc;'+IntToStr((Nums.iCheckNum+1))+';'+FloatToStr(TabCh.rDiscDop)+';'+IntToStr(iSumDisc)+';'+IntToStr(iSumIt)+';');
                while TestStatus('CheckDiscount',sMessage)=False do
                begin
                  fmAttention.Label1.Caption:=sMessage;
                  fmAttention.ShowModal;
                  Nums.iRet:=InspectSt(Nums.sRet);
                  prWriteLog('~~AttentionShow;'+sMessage+';'+'InspectSt(Nums.sRet)='+IntToStr(Nums.iRet));
                end;
              end;

              //�������������� �������

              CheckTotal(iSumIt);
              prWriteLog('!!AfterCheckTotal;'+IntToStr((Nums.iCheckNum+1))+';'+inttostr(iSumTotal)+';');
              while TestStatus('CheckTotal',sMessage)=False do
              begin
                fmAttention.Label1.Caption:=sMessage;
                prWriteLog('~~AttentionShow;'+sMessage);
                fmAttention.ShowModal;
                Nums.iRet:=InspectSt(Nums.sRet);
              end;

              //�������� iSumTotal
              if iSumTotal=0 then iSumTotal:=RoundEx(rSum*100); //rSum - ��� ����� ��� ������
              iSumDisc:=RoundEx((rDiscont+TabCh.rDiscDop)*100);

              prWriteLog('!!_0;iSumTotalFis '+its(iSumTotalFis)+'; iSumDisc '+its(iSumDisc));

    //          if (iSumDisc=0)and(abs(rDiscont+TabCh.rDiscDop)>=0.01) then iSumDisc:=RoundEx((rDiscont+TabCh.rDiscDop)*100);
    //          prWriteLog('!!_1;iSumTotalFis '+its(iSumTotalFis)+'; iSumDisc '+its(iSumDisc));

              if iSumIt=0 then iSumIt:=iSumTotalFis-iSumDisc;

              prWriteLog('!!_2;rSum '+fts(rSum)+'; iSumIt '+its(iSumIt));

              if abs(iSumIt-(iSumTotalFis-iSumDisc))>500000 then  // 5 ������
              begin
                prWriteLog('!!AfterCheckRasch;'+IntToStr((Nums.iCheckNum+1))+'; ����. ����� '+FloatToStr(rSum*100)+'; ����. ����� '+INtToStr(iSumIt)+';');
                prClearCheck;
              end
              else
              begin
                if aCheckPlat[iOrg,5]>0 then
                begin
                  CheckRasch(1,RoundEx(aCheckPlat[iOrg,5]*100),'',iSumIt); //������
                end;
                if (aCheckPlat[iOrg,1]-aCheckPlat[iOrg,3]-aCheckPlat[iOrg,4]-aCheckPlat[iOrg,5])>0 then
                begin
                  CheckRasch(0,RoundEx((aCheckPlat[iOrg,1]-aCheckPlat[iOrg,3]-aCheckPlat[iOrg,4]-aCheckPlat[iOrg,5])*100),'',iSumIt) //���
                end;

      //          if iCashType=0 then CheckRasch(0,RoundEx(fmCash.cEdit2.EditValue*100),'',iSumIt) //���
      //          else CheckRasch(1,RoundEx(fmCash.cEdit1.EditValue*100),'',iSumIt); //������

                prWriteLog('!!AfterCheckRasch;'+IntToStr((Nums.iCheckNum+1))+';'+FloatToStr(aCheckPlat[iOrg,5])+';'+FloatToStr((aCheckPlat[iOrg,1]-aCheckPlat[iOrg,3]-aCheckPlat[iOrg,4]-aCheckPlat[iOrg,5]))+';'+INtToStr(iSumIt)+';');
                while TestStatus('CheckRasch',sMessage)=False do
                begin
                  fmAttention.Label1.Caption:=sMessage;
                  prWriteLog('~~AttentionShow;'+sMessage);
                  fmAttention.ShowModal;
                  Nums.iRet:=InspectSt(Nums.sRet);
                end;

                prWriteLog('!!CheckClose;'+IntToStr((Nums.iCheckNum+1))+';');

                Case Operation of
                0: begin
                     Event_RegEx(5,Nums.iCheckNum,0,0,'','',0,0,(aCheckPlat[iOrg,1]-aCheckPlat[iOrg,3]-aCheckPlat[iOrg,4]),(aCheckPlat[iOrg,1]-aCheckPlat[iOrg,3]-aCheckPlat[iOrg,4]),0,0); //����� ����
                   end;
                1: begin
                     Event_RegEx(47,Nums.iCheckNum,0,0,'','',0,0,(aCheckPlat[iOrg,1]-aCheckPlat[iOrg,3]-aCheckPlat[iOrg,4]),(aCheckPlat[iOrg,1]-aCheckPlat[iOrg,3]-aCheckPlat[iOrg,4]),0,0);
                   end;
                end;

                CheckClose;

                delay(300); // ������� ������� ������ ���� �������� ����� ���� ��� �������.

                while TestStatus('CheckClose',sMessage)=False do
                begin
                  fmAttention.Label1.Caption:=sMessage;
                  prWriteLog('~~AttentionShow;'+sMessage);
                  fmAttention.ShowModal;
                  Nums.iRet:=InspectSt(Nums.sRet);
                end;

              //�������� ������ ����� ������
                iRet:=InspectSt(StrWk);
                iCurCheck:=Nums.iCheckNum+1; //��� ���-�� ������� ���

                if iRet=0 then
                begin
                  Label3.Caption:='������ ���: ��� ��.';
                  if CashDate(StrWk) then Label2.Caption:='�������� ����: '+StrWk;
                  if GetNums then Label4.Caption:='��� � '+Nums.CheckNum;
              //��� ����������� �������� CommonSet.CashChNum - ����� �������� ��� � ���
                  WriteCheckNum;
                end
                else
                begin
                  Label3.Caption:='������ ���: ������ ('+IntToStr(iRet)+')';

                  while TestStatus('InspectSt',sMessage)=False do
                  begin
                    fmAttention.Label1.Caption:=sMessage;    //������ 3: 000D
                    prWriteLog('~~AttentionShow;'+sMessage);
                    fmAttention.ShowModal;
                    Nums.iRet:=InspectSt(Nums.sRet);
                  end;
                end;

                bCheckOk:=True;

                if CommonSet.ControlNums=1 then
                begin
                  if (Nums.iCheckNum <> iCurCheck) then
                  begin //���������� ������������������ �����
                      //������� ���������� ������������
                    prWriteLog('!!ErrorNumCheck;new-'+IntToStr(Nums.iCheckNum+1)+';old-'+IntToStr(iCurCheck)+';');
                    prClearCheck;
                  end;
                end;

                PrintBn(BnStr);
                PrintBn(BnStr);
                BnStr:='';
                inc(CommonSet.CashChNum); //����� �������� 1-��
                WriteCheckNum;
                prWriteChNumToBase(iOrg);
              end;

    {          if abs(rDiscont)<=0.01 then
              begin
                if TabCh.DBar>'' then //������ ���� - ����� ������ = 0  - �������� �� �������� �����
                begin
                  If FindDiscount(TabCh.DBar) then
                  begin
                    if Tab.DType=3 then
                    begin


                    end;
                  end;
                end;
              end;
              }
            end else //����� ������������ ��� ��� ����� �������� ������������, �� ��� �� �������� ����� .. ������� c ��������� ������
            begin
              if (CommonSet.Fis=0)and(CommonSet.SpecChar=0)and(pos('DB',CommonSet.PrePrintPort)=0)and(Pos('fis',CommonSet.SpecBoxX)=0) then //������ ������������ ����� �� ��������� �������
              begin
                try
                  OpenMoneyBox;
                  delay(20);
                  prDevOpen(CommonSet.PrePrintPort,0);
                  BufPr.iC:=0;
                  StrP:=CommonSet.PrePrintPort;

                  prSetFont(StrP,13,0); PrintStr(StrP,' '+CommonSet.DepartName); PrintStr(StrP,'');

                  if CommonSet.PreLine1>'' then
                  begin
                    if fDifStr(1,CommonSet.PreLine1)>'' then PrintStr(StrP,fDifStr(1,CommonSet.PreLine1));
                    if fDifStr(2,CommonSet.PreLine1)>'' then PrintStr(StrP,fDifStr(2,CommonSet.PreLine1));
                  end;

                  prSetFont(StrP,14,0); PrintStr(StrP,'  �������� ��� �'+IntToStr(CommonSet.CashChNum));
                  prSetFont(StrP,13,0); PrintStr(StrP,'');

                  PrintStr(StrP,'��������: '+TabCh.Name);
                  PrintStr(StrP,'����: '+TabCh.NumTable);
                  prSetFont(StrP,13,0);PrintStr(StrP,'������: '+IntToStr(TabCh.Quests));
                  PrintStr(StrP,'������: '+FormatDateTime('dd.mm.yyyy hh:nn:ss',TabCh.OpenTime));
                  prSetFont(StrP,13,0);StrWk:='                                        ';
                  PrintStr(StrP,StrWk);
                  prSetFont(StrP,15,0);StrWk:=' ��������      ���-��   ����   �����';
                  PrintStr(StrP,StrWk);
                  prSetFont(StrP,13,0);StrWk:='                                        ';
                  PrintStr(StrP,StrWk);

                 //�������
                  rDiscont:=0;

                  taStreams.Active:=False;
                  taStreams.Active:=True;
                  taStreams.First;
                  while not taStreams.Eof do
                  begin
                    rSum:=0;

                    quCheckOrg.First;
                    while not quCheckOrg.Eof do
                    begin      //��������� �������
                      PosCh.Name:=quCheckOrgNAME.AsString;
                      PosCh.Code:=quCheckOrgCODE.AsString;
                      PosCh.AddName:='';
                      PosCh.Stream:=quCheckOrgSTREAM.AsInteger;

                      if PosCh.Stream=taStreamsID.AsInteger then
                      begin
                        rDiscont:=rDiscont+quCheckOrgDISCOUNTSUM.AsFloat;
                        rSum:=rSum+quCheckOrgSUMMA.AsFloat;

                        StrWk:= Copy(PosCh.Name,1,18);
                        while Length(StrWk)<18 do StrWk:=StrWk+' ';

                        if abs(frac(quCheckOrgQUANTITY.AsFloat*100))>0 then nd:=3  //���� ������� ����� �� �������
                        else
                          if abs(frac(quCheckOrgQUANTITY.AsFloat*10))>0 then nd:=2  //���� ������� ����� �������� �� �������
                          else nd:=1;

                        Str(quCheckOrgQUANTITY.AsFloat:5:nd,StrWk1);
                        StrWk:=StrWk+' '+StrWk1;
                        Str(quCheckOrgPRICE.AsFloat:7:2,StrWk1);
                        StrWk:=StrWk+' '+StrWk1;
                        Str(quCheckOrgSUMMA.AsFloat:8:2,StrWk1);
                        StrWk:=StrWk+' '+StrWk1+'�';
                        prSetFont(StrP,15,0);
                        PrintStr(StrP,StrWk);

                        quCheckOrg.Next;
                        if not quCheckOrg.Eof then
                        begin
                          while quCheckOrgITYPE.AsInteger=1 do
                          begin  //������������
                            if quCheckOrg.Eof then break;
                            if quCheckOrgSifr.AsInteger>0 then
                              if taModif.Locate('SIFR',quCheckOrgSifr.AsInteger,[]) then
                              begin
                                PrintStr(StrP,'   '+Copy(taModifNAME.AsString,1,29));
                              end;
                            quCheckOrg.Next;
                          end;
                        end;
                      end else
                      quCheckOrg.Next;
                    end;
                    if rSum<>0 then
                    begin
                      prSetFont(StrP,13,0);
                      StrWk:='                    ';
                      PrintStr(StrP,StrWk);
                      //�������� ������� �� ������
                      Str(rSum:8:2,StrWk1);
                      StrWk:='����� �� '+taStreamsNAMESTREAM.AsString+'  '+StrWk1+'�';
                      prSetFont(StrP,15,0);
                      PrintStr(StrP,StrWk);
                      PrintStr(StrP,'');
                    end;

                    taStreams.Next;
                  end;

                  prSetFont(StrP,13,0);
                  StrWk:='                                          ';
                  PrintStr(StrP,StrWk);
                  prSetFont(StrP,15,0);
                  Str(TabCh.Summa:8:2,StrWk1);
                  StrWk:=' �����                    '+StrWk1+' ���';
                  PrintStr(StrP,StrWk);

                  Case Operation of
                  0:begin
                      PrintStr(StrP,'�������                            ');
                      if fmCash.cEdit5.EditValue>0 then
                      begin
                        PrintStr(StrP,prDefFormatStr(40,'��������� �����:',fmCash.cEdit5.EditValue));
                      end;
                      if fmCash.cEdit2.EditValue>0 then
                      begin
                        PrintStr(StrP,prDefFormatStr(40,'��������:',fmCash.cEdit2.EditValue));
                      end;
                      if (fmCash.cEdit5.EditValue+fmCash.cEdit2.EditValue)>TabCh.Summa then
                      begin
                        PrintStr(StrP,prDefFormatStr(40,'�����:',(fmCash.cEdit5.EditValue+fmCash.cEdit2.EditValue)-TabCh.Summa));
                      end;
                    end;
                  1:begin
                      PrintStr(StrP,'�������                            ');
                      if fmCash.cEdit5.EditValue>0 then
                      begin
                        PrintStr(StrP,prDefFormatStr(40,'��������� �����:',fmCash.cEdit5.EditValue));
                      end;
                      if (rSum-fmCash.cEdit5.EditValue)>0 then
                      begin
                        PrintStr(StrP,prDefFormatStr(40,'��������:',(rSum-fmCash.cEdit5.EditValue)));
                      end;
                    end;
                  end;

                  if rDiscont>0.02 then
                  begin
                    prSetFont(StrP,15,0);
                    PrintStr(StrP,'');
                    prSetFont(StrP,10,0);
                    rDProc:=RoundEx((rDiscont/(TabCh.Summa+rDiscont)*100)*100)/100;
                    Str(rDProc:5:2,StrWk1);
                    Str(rDiscont:8:2,StrWk);
                    StrWk:=' � �.�. ������ - '+StrWk+'�.';
                    PrintStr(StrP,StrWk);
                  end;

                  PrintStr(StrP,'������: '+Person.Name);
                  PrintStr(StrP,'�������: _____________________');

                  if CommonSet.LastLine1>'' then
                  begin
                    PrintStr(StrP,'');
                    if fDifStr(1,CommonSet.LastLine1)>'' then PrintStr(StrP,fDifStr(1,CommonSet.LastLine1));
                    if fDifStr(2,CommonSet.LastLine1)>'' then PrintStr(StrP,fDifStr(2,CommonSet.LastLine1));
                  end;



                  prWrBuf(StrP);
                  prCutDoc(StrP,0);

               {   kDelay:=(BufPr.iC div 40)-10;
                  if kDelay<0 then kDelay:=0;
                  delay(CommonSet.ComDelay*5+trunc(kDelay*CommonSet.ComDelay/2));  }
                finally
                  delay(50);
                  taStreams.Active:=False;
                  prDevClose(StrP,0);
                end;
              end;

              if ((CommonSet.Fis=0)or(iPC=2))and(CommonSet.SpecChar=0)and(pos('DB',CommonSet.SpecBoxX)>0) then //������ �� ����� ������
              begin
                if pos('fis',CommonSet.SpecBox)=0 then OpenMoneyBox
                else
                begin
                  try
                    StrWk:='COM'+IntToStr(CommonSet.CashPort);
                    CommonSet.CashNum:=CommonSet.CashNum*(-1);
                    CashOpen(PChar(StrWk));
                    CashDriver;
                    CashClose;
                    CommonSet.CashNum:=CommonSet.CashNum*(-1);
                  except
                  end;
                end;

                with dmC1 do
                begin
                 try
                  quPrint.Active:=False;
                  quPrint.Active:=True;
                  iQ:=GetId('PQH'); //����������� ������� �� 1-�

                  rSum:=0;

                  if CommonSet.PreLine1>'' then
                  begin
                    if fDifStr(1,CommonSet.PreLine1)>'' then PrintDBStr(iQ,999,CommonSet.PrePrintPort,fDifStr(1,CommonSet.PreLine1),10,0);
                    if fDifStr(2,CommonSet.PreLine1)>'' then PrintDBStr(iQ,999,CommonSet.PrePrintPort,fDifStr(2,CommonSet.PreLine1),10,0);
                  end;

                  PrintDBStr(iQ,999,CommonSet.PrePrintPort,' ',10,0);
                  PrintDBStr(iQ,999,CommonSet.PrePrintPort,'  �������� ��� �'+IntToStr(CommonSet.CashChNum),14,0);
                  PrintDBStr(iQ,999,CommonSet.PrePrintPort,' ',10,0);
                  PrintDBStr(iQ,999,CommonSet.PrePrintPort,'��������: '+TabCh.Name,13,0);
                  PrintDBStr(iQ,999,CommonSet.PrePrintPort,'����: '+TabCh.NumTable,13,0);
                  if Tab.DBar>'' then
                  begin
                    Str(Tab.DPercent:5:2,StrWk);
                    PrintDBStr(iQ,999,CommonSet.PrePrintPort,'�����: '+Tab.DName+' ('+StrWk+'%)',13,0);
                  end;
                  PrintDBStr(iQ,999,CommonSet.PrePrintPort,'������: '+IntToStr(TabCh.Quests),13,0);
                  PrintDBStr(iQ,999,CommonSet.PrePrintPort,'������: '+FormatDateTime('dd.mm.yyyy hh:nn:ss',TabCh.OpenTime),13,0);
                  PrintDBStr(iQ,999,CommonSet.PrePrintPort,'-',13,0);
                  PrintDBStr(iQ,999,CommonSet.PrePrintPort,' ��������      ���-��   ����   �����',15,0);
                  PrintDBStr(iQ,999,CommonSet.PrePrintPort,'-',13,0);

                  rDiscont:=0;
                 {
                  quCheck.First;
                  while not quCheck.Eof do
                  begin      //��������� �������

                    PosCh.Name:=quCheckNAME.AsString;
                    PosCh.Code:=quCheckCODE.AsString;
                    PosCh.AddName:='';

                    rDiscont:=rDiscont+quCheckDISCOUNTSUM.AsFloat;

                    StrWk:= Copy(PosCh.Name,1,19);
                    while Length(StrWk)<19 do StrWk:=StrWk+' ';

                    if abs(frac(quCheckQUANTITY.AsFloat*100))>0 then nd:=3  //���� ������� ����� �� �������
                    else
                      if abs(frac(quCheckQUANTITY.AsFloat*10))>0 then nd:=2  //���� ������� ����� �������� �� �������
                      else nd:=1;

                    Str(quCheckQUANTITY.AsFloat:5:nd,StrWk1);
                    StrWk:=StrWk+' '+StrWk1;
                    Str(quCheckPRICE.AsFloat:7:2,StrWk1);
                    StrWk:=StrWk+' '+StrWk1;
                    Str((quCheckPRICE.AsFloat*quCheckQUANTITY.AsFloat):8:2,StrWk1);
                    StrWk:=StrWk+StrWk1+'�';

                    PrintDBStr(iQ,999,CommonSet.PrePrintPort,StrWk,15,0);

                    rSum:=rSum+quCheckSUMMA.AsFloat;

                    quCheck.Next;

                    if not quCheck.Eof then
                    begin
                      while quCheckITYPE.AsInteger=1 do
                      begin  //������������
                        if quCheck.Eof then break;
                        if quCheckSifr.AsInteger>0 then
                          if taModif.Locate('SIFR',quCheckSifr.AsInteger,[]) then
                          begin
                            PrintDBStr(iQ,999,CommonSet.PrePrintPort,'   '+Copy(taModifNAME.AsString,1,29),15,0);
                          end;
                        quCheck.Next;
                      end;
                    end;
                  end;
                  }

                  taStreams.Active:=False;
                  taStreams.Active:=True;
                  taStreams.First;
                  while not taStreams.Eof do
                  begin
                    rSum:=0;

                    quCheckOrg.First;
                    while not quCheckOrg.Eof do
                    begin      //��������� �������
                      PosCh.Stream:=quCheckOrgSTREAM.AsInteger;

                      if (PosCh.Stream=taStreamsID.AsInteger) then
                      begin
                        if (quCheckOrgITYPE.AsInteger=0) then
                        begin
                          PosCh.Name:=quCheckOrgNAME.AsString;
                          PosCh.Code:=quCheckOrgCODE.AsString;
                          PosCh.AddName:='';

                          rDiscont:=rDiscont+quCheckOrgDISCOUNTSUM.AsFloat;
                          rSum:=rSum+quCheckOrgSUMMA.AsFloat;

                          StrWk:= Copy(PosCh.Name,1,18);
                          while Length(StrWk)<18 do StrWk:=StrWk+' ';

                          if abs(frac(quCheckOrgQUANTITY.AsFloat*100))>0 then nd:=3  //���� ������� ����� �� �������
                          else
                            if abs(frac(quCheckOrgQUANTITY.AsFloat*10))>0 then nd:=2  //���� ������� ����� �������� �� �������
                            else nd:=1;

                          Str(quCheckOrgQUANTITY.AsFloat:5:nd,StrWk1);
                          StrWk:=StrWk+' '+StrWk1;
                          Str(quCheckOrgPRICE.AsFloat:7:2,StrWk1);
                          StrWk:=StrWk+' '+StrWk1;
                          Str(quCheckOrgSUMMA.AsFloat:8:2,StrWk1);
                          StrWk:=StrWk+' '+StrWk1+'�';

  //                      prSetFont(StrP,15,0); PrintStr(StrP,StrWk);
                          PrintDBStr(iQ,999,CommonSet.PrePrintPort,StrWk,15,0);
                          quCheckOrg.Next;
                          if not quCheckOrg.Eof then
                          begin
                            while quCheckOrgITYPE.AsInteger=1 do
                            begin  //������������
                              if quCheckOrg.Eof then break;
                              if quCheckOrgSifr.AsInteger>0 then
                                if taModif.Locate('SIFR',quCheckOrgSifr.AsInteger,[]) then
                                begin
  //                              PrintStr(StrP,'   '+Copy(taModifNAME.AsString,1,29));
                                  PrintDBStr(iQ,999,CommonSet.PrePrintPort,'   '+Copy(taModifNAME.AsString,1,29),15,0);
                                end;
                              quCheckOrg.Next;
                            end;
                          end;
                        end else quCheckOrg.Next;
                      end else
                      quCheckOrg.Next;
                    end;
                    if rSum<>0 then
                    begin
    //                  prSetFont(StrP,13,0);
                      StrWk:='                    ';
  //                    PrintStr(StrP,StrWk);

                      PrintDBStr(iQ,999,CommonSet.PrePrintPort,StrWk,13,0);
                      //�������� ������� �� ������
                      Str(rSum:8:2,StrWk1);
                      StrWk:='����� �� '+taStreamsNAMESTREAM.AsString+'  '+StrWk1+'�';
  //                    prSetFont(StrP,15,0);
  //                    PrintStr(StrP,StrWk);
  //                    PrintStr(StrP,'');

                      PrintDBStr(iQ,999,CommonSet.PrePrintPort,StrWk,15,0);
                      PrintDBStr(iQ,999,CommonSet.PrePrintPort,'  ',15,0);
                    end;

                    taStreams.Next;
                  end;

                  PrintDBStr(iQ,999,CommonSet.PrePrintPort,'-',13,0);
                  Str(TabCh.Summa:8:2,StrWk1);
                  StrWk:=' �����                    '+StrWk1+' ���';
                  PrintDBStr(iQ,999,CommonSet.PrePrintPort,StrWk,15,0);

                  Case Operation of
                  0:begin
                      PrintDBStr(iQ,999,CommonSet.PrePrintPort,'�������                            ',15,0);
                      if fmCash.cEdit5.EditValue>0 then
                      begin
                        PrintDBStr(iQ,999,CommonSet.PrePrintPort,prDefFormatStr(40,'��������� �����:',fmCash.cEdit5.EditValue),15,0);
                      end;
                      if fmCash.cEdit6.EditValue>0 then
                      begin
                        PrintDBStr(iQ,999,CommonSet.PrePrintPort,prDefFormatStr(40,'��������� �����:',fmCash.cEdit6.EditValue),15,0);
                      end;
                      if fmCash.cEdit2.EditValue>0 then
                      begin
                        PrintDBStr(iQ,999,CommonSet.PrePrintPort,prDefFormatStr(40,'��������:',fmCash.cEdit2.EditValue),15,0);
                      end;
                      if (fmCash.cEdit5.EditValue+fmCash.cEdit6.EditValue+fmCash.cEdit2.EditValue)>TabCh.Summa then
                      begin
                        PrintDBStr(iQ,999,CommonSet.PrePrintPort,prDefFormatStr(40,'�����:',(fmCash.cEdit5.EditValue+fmCash.cEdit2.EditValue+fmCash.cEdit6.EditValue)-TabCh.Summa),15,0);
                      end;
                    end;
                  1:begin
                      PrintDBStr(iQ,999,CommonSet.PrePrintPort,'�������                            ',15,0);
                      if fmCash.cEdit5.EditValue>0 then
                      begin
                        PrintDBStr(iQ,999,CommonSet.PrePrintPort,prDefFormatStr(40,'��������� �����:',fmCash.cEdit5.EditValue),15,0);
                      end;
                      if fmCash.cEdit6.EditValue>0 then
                      begin
                        PrintDBStr(iQ,999,CommonSet.PrePrintPort,prDefFormatStr(40,'��������� �����:',fmCash.cEdit6.EditValue),15,0);
                      end;
                      if (rSum-fmCash.cEdit5.EditValue-fmCash.cEdit6.EditValue)>0 then
                      begin
                        PrintDBStr(iQ,999,CommonSet.PrePrintPort,prDefFormatStr(40,'��������:',(rSum-fmCash.cEdit5.EditValue-fmCash.cEdit6.EditValue)),15,0);
                      end;
                    end;
                  end;

                  if rDiscont>0.02 then
                  begin
                    PrintDBStr(iQ,999,CommonSet.PrePrintPort,'',15,0);
                    rDProc:=RoundEx((rDiscont/(TabCh.Summa+rDiscont)*100)*100)/100;
                    Str(rDProc:5:2,StrWk1);
                    Str(rDiscont:8:2,StrWk);
                    StrWk:=' � �.�. ������ - '+StrWk+'�.';
                    PrintDBStr(iQ,999,CommonSet.PrePrintPort,StrWk,10,0);
                  end;

                  if iPC=2 then
                  begin
                    PrintDBStr(iQ,999,CommonSet.PrePrintPort,'',10,0);
                    Str(rv(fmCash.cEdit6.value):10:2,StrWk);
                    PrintDBStr(iQ,999,CommonSet.PrePrintPort,'�������� '+Copy(SalePC.CliName,1,20)+':'+StrWk,10,0);
                    Str(rv(SalePC.rBalans-fmCash.cEdit6.value):10:2,StrWk);
                    PrintDBStr(iQ,999,CommonSet.PrePrintPort,'������ �� �����:'+ StrWk,10,0);
                    Str(rv(SalePC.rBalansDay-fmCash.cEdit6.value):10:2,StrWk);
                    PrintDBStr(iQ,999,CommonSet.PrePrintPort,'������� �������� ������:'+ StrWk,10,0);
                    PrintDBStr(iQ,999,CommonSet.PrePrintPort,'',10,0);
                  end;

                  PrintDBStr(iQ,999,CommonSet.PrePrintPort,'������: '+Person.Name,10,0);
                  PrintDBStr(iQ,999,CommonSet.PrePrintPort,'�������: _____________________',10,0);

                  if CommonSet.LastLine1>'' then
                  begin
                    PrintDBStr(iQ,999,CommonSet.PrePrintPort,'',10,0);
                    if fDifStr(1,CommonSet.LastLine1)>'' then PrintDBStr(iQ,999,CommonSet.PrePrintPort,fDifStr(1,CommonSet.LastLine1),10,0);
                    if fDifStr(2,CommonSet.LastLine1)>'' then PrintDBStr(iQ,999,CommonSet.PrePrintPort,fDifStr(2,CommonSet.LastLine1),10,0);
                  end;

                  prAddPrintQH(999,iQ,FormatDateTime('dd.mm hh:nn:sss',now)+', ������� - '+IntToStr(CommonSet.Station)+','+CommonSet.PrePrintPort);
                  quPrint.Active:=False;
                 finally
                  bCheckOk:=True;
                 end;
                end;
              end;
              if (CommonSet.SpecChar=1)and(CommonSet.CashNum<0) then //������ ������������ �����
              begin
                if pos('fis',CommonSet.SpecBox)=0 then OpenMoneyBox
                else
                begin
                  try
                    StrWk:='COM'+IntToStr(CommonSet.CashPort);
                    CommonSet.CashNum:=CommonSet.CashNum*(-1);
                    CashOpen(PChar(StrWk));
                    CashDriver;
                    CashClose;
                    CommonSet.CashNum:=CommonSet.CashNum*(-1);
                  except
                  end;
                end;

                if Pos('COM',CommonSet.PrePrintPort)>0 then
                begin
                  try
                    prDevOpen(CommonSet.PrePrintPort,0);
                    StrP:=CommonSet.PrePrintPort;

                    BufPr.iC:=0;

                    prSetFont(StrP,10,0);
                    PrintStr(StrP,SpecVal.SPH1);
                    PrintStr(StrP,SpecVal.SPH2);
                    PrintStr(StrP,SpecVal.SPH3);
                    PrintStr(StrP,SpecVal.SPH4);
                    PrintStr(StrP,SpecVal.SPH5);

  //                PrintStr(StrP,'�������� N: 0718668         N ���: 00012');
                    StrWk:=IntToStr(TabCh.Id); //����� ���������
                    while Length(StrWk)<5 do StrWk:='0'+StrWk;
                    PrintStr(StrP,'�������� N: '+CommonSet.CashSer+'         N ���: '+StrWk);
                    PrintStr(StrP,FormatDateTime('dd-mm-yyyy                         hh:nn',now));

  //                PrintStr(StrP,'������: ������              ��� N: 00012');
                    StrWk:='������: '+Copy(Person.Name,1,19);
                    while Length(StrWk)<28 do StrWk:=StrWk+' ';
                    StrWk:=StrWk+'��� N: ';
                    StrWk1:=IntToStr(CommonSet.CashChNum); StrWk1:=Copy(StrWk1,1,5);
                    while Length(StrWk1)<5 do StrWk1:='0'+StrWk1;
                    StrWk:=StrWk+StrWk1;
                    PrintStr(StrP,StrWk);
                    PrintStr(StrP,'������:                          �����: ');
                    PrintStr(StrP,its(TabCh.Id));

                    rSum:=0;

                    //����� ������ ������������ ����� �������

                    quCheckOrg.First;
                    while not quCheckOrg.Eof do
                    begin      //��������� �������
                      StrWk:=quCheckOrgCODE.AsString;

                      StrWk:=Copy(quCheckOrgNAME.AsString,1,40);
                    //  PrintStr(StrP,StrWk);

                      if quCheckOrgQUANTITY.AsFloat<>0 then  //���� ��������
                        Str((quCheckOrgSUMMA.AsFloat/quCheckOrgQUANTITY.AsFloat):7:2,StrWk)
                      else
                        Str(quCheckOrgPRICE.AsFloat:7:2,StrWk);

                      StrWk:=DelSp(StrWk);

                      Str(quCheckOrgQUANTITY.AsFloat:7:3,StrWk1);
                      StrWk1:=DelSp(StrWk1);
                      if Pos('.',StrWk)>0 then
                      begin
                        if StrWk1[Length(StrWk1)]='0' then delete(StrWk1,Length(StrWk1),1);
                        if StrWk1[Length(StrWk1)]='0' then delete(StrWk1,Length(StrWk1),1);
                      end;

                      StrWk:=StrWk+' � '+StrWk1+' ��.';

                   //   PrintStr(StrP,prDefFormatStr(40,StrWk,quCheckOrgSUMMA.AsFloat));

                      rSum:=rSum+quCheckOrgSUMMA.AsFloat;

                      quCheckOrg.Next;
                    end;
                    PrintStr(StrP,'����� 1');
                    PrintStr(StrP,prDefFormatStr(40,fts(rSum)+' X 1 ��.',rSum));
  //                  PrintStr(StrP,prDefFormatStr(40,'����� 1',rSum));
  //                  PrintStr(StrP,'�����:                                  ');
                    PrintStr(StrP,prDefFormatStr(40,'�����:',rSum));

                    Case Operation of
                    0:begin
                        PrintStr(StrP,'�������                                 ');
                        if fmCash.cEdit5.EditValue>0 then
                        begin
                          PrintStr(StrP,prDefFormatStr(40,'��������� �����:',fmCash.cEdit5.EditValue));
                        end;
                        if fmCash.cEdit2.EditValue>0 then
                        begin
                          PrintStr(StrP,prDefFormatStr(40,'��������:',fmCash.cEdit2.EditValue));
                        end;
                        if (fmCash.cEdit5.EditValue+fmCash.cEdit2.EditValue)>rSum then
                        begin
                          PrintStr(StrP,prDefFormatStr(40,'�����:',(fmCash.cEdit5.EditValue+fmCash.cEdit2.EditValue)-rSum));
                        end;
                      end;
                    1:begin
                        PrintStr(StrP,'�������                                 ');
                        if fmCash.cEdit5.EditValue>0 then
                        begin
                          PrintStr(StrP,prDefFormatStr(40,'��������� �����:',fmCash.cEdit5.EditValue));
                        end;
                        if (rSum-fmCash.cEdit5.EditValue)>0 then
                        begin
                          PrintStr(StrP,prDefFormatStr(40,'��������:',(rSum-fmCash.cEdit5.EditValue)));
                        end;
                      end;
                    end;

                    CutDocPrSpec;
                    prWrBuf(StrP);

                  finally
                    prDevClose(StrP,0);
                  end;
                end;
                if Pos('DB',CommonSet.PrePrintPort)>0 then
                begin
                  with dmC1 do
                  begin
                    quPrint.Active:=False;
                    quPrint.Active:=True;
                    iQ:=GetId('PQH'); //����������� ������� �� 1-�

                    PrintDBStr(iQ,999,'Check',SpecVal.SPH1,10,0);
                    PrintDBStr(iQ,999,'Check',SpecVal.SPH2,10,0);
                    PrintDBStr(iQ,999,'Check',SpecVal.SPH3,10,0);
                    PrintDBStr(iQ,999,'Check',SpecVal.SPH4,10,0);
                    PrintDBStr(iQ,999,'Check',SpecVal.SPH5,10,0);
  //                PrintDBStr(iQ,999,'Check','�������� N: 0718668         N ���: 00012');
                    StrWk:=IntToStr(TabCh.Id); //����� ���������
                    while Length(StrWk)<5 do StrWk:='0'+StrWk;
                    PrintDBStr(iQ,999,'Check','�������� N: '+CommonSet.CashSer+'         N ���: '+StrWk,10,0);
                    PrintDBStr(iQ,999,'Check',FormatDateTime('dd-mm-yyyy                         hh:nn',now),10,0);

  //                PrintDBStr(iQ,999,'Check','������: ������              ��� N: 00012');
                    StrWk:='������: '+Copy(Person.Name,1,19);
                    while Length(StrWk)<28 do StrWk:=StrWk+' ';
                    StrWk:=StrWk+'��� N: ';
                    StrWk1:=IntToStr(CommonSet.CashChNum); StrWk1:=Copy(StrWk1,1,5);
                    while Length(StrWk1)<5 do StrWk1:='0'+StrWk1;
                    StrWk:=StrWk+StrWk1;
                    PrintDBStr(iQ,999,'Check',StrWk,10,0);
                    PrintDBStr(iQ,999,'Check','������:                          �����: ',10,0);
                    PrintDBStr(iQ,999,'Check',its(TabCh.Id),10,0);

                    rSum:=0;

                    quCheckOrg.First;
                    while not quCheckOrg.Eof do
                    begin      //��������� �������
                      StrWk:=quCheckOrgCODE.AsString;
  //                  PrintDBStr(iQ,999,'Check',StrWk);

                      StrWk:=Copy(quCheckOrgNAME.AsString,1,40);
  //                  PrintDBStr(iQ,999,'Check',StrWk);

  //                  '100.50 X 3.3 ��.                 =120.00'
                      if quCheckOrgQUANTITY.AsFloat<>0 then  //���� ��������
                        Str((quCheckOrgSUMMA.AsFloat/quCheckOrgQUANTITY.AsFloat):7:2,StrWk)
                      else
                        Str(quCheckOrgPRICE.AsFloat:7:2,StrWk);

                      StrWk:=DelSp(StrWk);

                      Str(quCheckOrgQUANTITY.AsFloat:7:3,StrWk1);
                      StrWk1:=DelSp(StrWk1);
                      if Pos('.',StrWk)>0 then
                      begin
                        if StrWk1[Length(StrWk1)]='0' then delete(StrWk1,Length(StrWk1),1);
                        if StrWk1[Length(StrWk1)]='0' then delete(StrWk1,Length(StrWk1),1);
                      end;

                      StrWk:=StrWk+' � '+StrWk1+' ��.';

                      rSum:=rSum+quCheckOrgSUMMA.AsFloat;

                      quCheckOrg.Next;
                    end;

                    PrintDBStr(iQ,999,'Check','����� 1',10,0);
                    PrintDBStr(iQ,999,'Check',prDefFormatStr(40,fts(rSum)+' X 1 ��.',rSum),10,0);
                    PrintDBStr(iQ,999,'Check',prDefFormatStr(40,'�����: ',rSum),10,0);
  //                  PrintDBStr(iQ,999,'Check','�����:                                  ',10,0);
                    Case Operation of
                    0:begin
                        PrintDBStr(iQ,999,'Check','�������                                 ',10,0);
                        if fmCash.cEdit5.EditValue>0 then
                        begin
                          PrintDBStr(iQ,999,'Check',prDefFormatStr(40,'��������� �����:',fmCash.cEdit5.EditValue),10,0);
                        end;
                        if fmCash.cEdit2.EditValue>0 then
                        begin
                          PrintDBStr(iQ,999,'Check',prDefFormatStr(40,'��������:',fmCash.cEdit2.EditValue),10,0);
                        end;
                        if (fmCash.cEdit5.EditValue+fmCash.cEdit2.EditValue)>rSum then
                        begin
                          PrintDBStr(iQ,999,'Check',prDefFormatStr(40,'�����:',(fmCash.cEdit5.EditValue+fmCash.cEdit2.EditValue)-rSum),10,0);
                        end;
                      end;
                    1:begin
                        PrintDBStr(iQ,999,'Check','�������                                 ',10,0);
                        if fmCash.cEdit5.EditValue>0 then
                        begin
                          PrintDBStr(iQ,999,'Check',prDefFormatStr(40,'��������� �����:',fmCash.cEdit5.EditValue),10,0);
                        end;
                        if (rSum-fmCash.cEdit5.EditValue)>0 then
                        begin
                          PrintDBStr(iQ,999,'Check',prDefFormatStr(40,'��������:',(rSum-fmCash.cEdit5.EditValue)),10,0);
                        end;
                      end;
                    end;

                    prAddPrintQH(999,iQ,FormatDateTime('dd.mm hh:nn:sss',now)+',������� - '+IntToStr(CommonSet.Station)+','+CommonSet.PrePrintPort);

                    PrintNFStr('������: '+Person.Name);
                    PrintNFStr('�������: _____________________');

                    quPrint.Active:=False;
                  end;
                end;
              end;

              if ((iFisCheck=0)or(iPC=2)or(CommonSet.Fis=0))and(pos('fis',CommonSet.SpecBoxX)>0) then //������ ������������ ����� �� ����������
              begin
                TestPrint:=1;
                While PrintQu and (TestPrint<=MaxDelayPrint) do
                begin
//                showmessage('������� �����, ��������� �������.');
                  prWriteLog('������������� ������ ���� � �������.');
                  delay(1000);
                  inc(TestPrint);
                end;
                try
                  PrintFCheck:=True; //��� ���� � ������� ������� ��������� ������

                  CashDriver;

//                  CutDoc;

                  OpenNFDoc;

                  if SpecVal.SPH1>'' then PrintNFStr(SpecVal.SPH1);
                  if SpecVal.SPH2>'' then PrintNFStr(SpecVal.SPH2);
                  if SpecVal.SPH3>'' then PrintNFStr(SpecVal.SPH3);
                  if SpecVal.SPH4>'' then PrintNFStr(SpecVal.SPH4);
                  if SpecVal.SPH5>'' then PrintNFStr(SpecVal.SPH5);

                  PrintNFStr(' ');
                  PrintNFStr('       ��� �'+IntToStr(CommonSet.CashChNum));
                  PrintNFStr(' ');
                  PrintNFStr('��������: '+TabCh.Name);
                  PrintNFStr('����: '+TabCh.NumTable);
                  PrintNFStr('������: '+FormatDateTime('dd.mm.yyyy hh:nn:ss',TabCh.OpenTime));
                  StrWk:='-----------------------------------';
                  PrintNFStr(StrWk);
                  StrWk:=' ��������     ���-��  ����   ����� ';
                  PrintNFStr(StrWk);
                  StrWk:='-----------------------------------';
                  PrintNFStr(StrWk);

                  quCheckOrg.First;
                  while not quCheckOrg.Eof do
                  begin      //��������� �������

                    PosCh.Name:=quCheckOrgNAME.AsString;
                    PosCh.Code:=quCheckOrgCODE.AsString;
                    PosCh.AddName:='';

                    StrWk:= Copy(PosCh.Name,1,36);
                    while Length(StrWk)<36 do StrWk:=StrWk+' ';
                    PrintNFStr(StrWk); //������� ����� �������� - �������� �������

//                    rDiscont:=rDiscont+quCheckOrgDISCOUNTSUM.AsFloat; �� ����

                    if abs(frac(quCheckOrgQUANTITY.AsFloat*100))>0 then nd:=3  //���� ������� ����� �� �������
                    else
                      if abs(frac(quCheckOrgQUANTITY.AsFloat*10))>0 then nd:=2  //���� ������� ����� �������� �� �������
                      else nd:=1;

                    Str(quCheckOrgQUANTITY.AsFloat:5:nd,StrWk1);
                    StrWk:='          '+StrWk1;
                    Str(quCheckOrgPRICE.AsFloat:7:2,StrWk1);
                    StrWk:=StrWk+' '+StrWk1;
                    Str(quCheckOrgSUMMA.AsFloat:8:2,StrWk1);
                    StrWk:=StrWk+' '+StrWk1+'���';
                    PrintNFStr(StrWk);

                    quCheckOrg.Next;
                    if not quCheckOrg.Eof then
                    begin
                      while quCheckOrgITYPE.AsInteger=1 do
                      begin  //������������
                        if quCheckOrg.Eof then break;
                        if quCheckOrgSifr.AsInteger>0 then
                        if taModif.Locate('SIFR',quCheckOrgSifr.AsInteger,[]) then
                        begin
                          StrWk:='   '+Copy(taModifNAME.AsString,1,29);
                          PrintNFStr(StrWk);
                        end;
                        quCheckOrg.Next;
                      end;
                    end;
                  end;

                  StrWk:='-----------------------------------';
                  PrintNFStr(StrWk);
                  Str((aCheckPlat[iOrg,1]):8:2,StrWk1);
                  StrWk:=' �����                '+StrWk1+' ���';
                  PrintNFStr(StrWk);

                  Case Operation of
                  0:begin
                      PrintNFStr('�������                                 ');
                      if aCheckPlat[iOrg,5]>0 then
                      begin
                        PrintNFStr(prDefFormatStr(32,'��������� �����:',aCheckPlat[iOrg,5]));
                      end;
                      if aCheckPlat[iOrg,4]>0 then
                      begin
                        PrintNFStr(prDefFormatStr(32,'��������� �����:',aCheckPlat[iOrg,4]));
                      end;
                      if aCheckPlat[iOrg,3]>0 then
                      begin
                        PrintNFStr(prDefFormatStr(32,'������� ������:',aCheckPlat[iOrg,3]));
                      end;
                      if (aCheckPlat[iOrg,1]-aCheckPlat[iOrg,3]-aCheckPlat[iOrg,4]-aCheckPlat[iOrg,5])>0 then
                      begin
                        PrintNFStr(prDefFormatStr(32,'��������:',aCheckPlat[iOrg,1]-aCheckPlat[iOrg,3]-aCheckPlat[iOrg,4]-aCheckPlat[iOrg,5]));
                      end;
//                      if (fmCash.cEdit5.EditValue+fmCash.cEdit2.EditValue)>rSum then
//                      begin
//                        PrintNFStr(prDefFormatStr(32,'�����:',(fmCash.cEdit5.EditValue+fmCash.cEdit6.EditValue+fmCash.cEdit2.EditValue)-rSum));
//                      end;
                    end;
                  1:begin
                      PrintNFStr('�������                                 ');
                      if aCheckPlat[iOrg,5]>0 then
                      begin
                        PrintNFStr(prDefFormatStr(32,'��������� �����:',aCheckPlat[iOrg,5]));
                      end;
                      if aCheckPlat[iOrg,4]>0 then
                      begin
                        PrintNFStr(prDefFormatStr(32,'��������� �����:',aCheckPlat[iOrg,4]));
                      end;
                      if (aCheckPlat[iOrg,1]-aCheckPlat[iOrg,3]-aCheckPlat[iOrg,4]-aCheckPlat[iOrg,5])>0 then
                      begin
                        PrintNFStr(prDefFormatStr(32,'��������:',(aCheckPlat[iOrg,1]-aCheckPlat[iOrg,3]-aCheckPlat[iOrg,4]-aCheckPlat[iOrg,5])));
                      end;
                    end;
                  end;

                  rDiscont:=aCheckPlat[iOrg,2];

                  if rDiscont>0.02 then
                  begin
                    PrintNFStr('');
                    rDProc:=rv(rDiscont/(aCheckPlat[iOrg,1]+aCheckPlat[iOrg,2])*100);
                    Str(rDProc:5:2,StrWk1);
                    Str(rDiscont:8:2,StrWk);
                    StrWk:=' � �.�. ������ - '+StrWk1+'%  '+StrWk+'�.';
                    PrintNFStr(StrWk);
                  end;

                  if iPC=2 then
                  begin
                    PrintNFStr(' ');
                    Str(rv(aCheckPlat[iOrg,4]):10:2,StrWk);
                    PrintNFStr('�������� '+Copy(SalePC.CliName,1,20)+':'+StrWk);
                    Str(rv(SalePC.rBalans-fmCash.cEdit6.value):10:2,StrWk);
                    PrintNFStr('������ �� �����:'+ StrWk);
                    Str(rv(SalePC.rBalansDay-fmCash.cEdit6.value):10:2,StrWk);
                    PrintNFStr('������� �������� ������:'+ StrWk);
                    PrintNFStr(' ');
                  end;

                  PrintNFStr('������: '+Person.Name);
                  PrintNFStr('�������: _____________________');
                  PrintNFStr(' ');
                  PrintNFStr(' ');


                  if CommonSet.LastLine1>'' then
                  begin
                    PrintNFStr('');
                    if fDifStr(1,CommonSet.LastLine1)>'' then PrintNFStr(fDifStr(1,CommonSet.LastLine1));
                    if fDifStr(2,CommonSet.LastLine1)>'' then PrintNFStr(fDifStr(2,CommonSet.LastLine1));
                  end;

                  CloseNFDoc;
                  if CommonSet.TypeFis<>'sp101' then
                  begin
                    CutDoc;
                  end;
                finally
                  bCheckOk:=True;
                  PrintFCheck:=False; //��� ���� � ������� ������� ��������� ������
                  inc(CommonSet.CashChNum); //����� �������� 1-��
                  WriteCheckNum;
                  prWriteChNumToBase(iOrg);
                end;

                {
                if pos('fisprim',CommonSet.PrePrintPort)>0 then
                begin
                  TestPrint:=1;
                  While PrintQu and (TestPrint<=MaxDelayPrint) do
                  begin
  //                showmessage('������� �����, ��������� �������.');
                    prWriteLog('������������� ������ ���� � �������.');
                    delay(1000);
                    inc(TestPrint);
                  end;
                  try
                    PrintFCheck:=True; //��� ���� � ������� ������� ��������� ������

                    CashDriver;

                    OpenNFDoc;

                    PrintNFStr(' '+CommonSet.DepartName);

                    if CommonSet.PreLine1>'' then
                    begin
                      if fDifStr(1,CommonSet.PreLine1)>'' then PrintNFStr(fDifStr(1,CommonSet.PreLine1));
                      if fDifStr(2,CommonSet.PreLine1)>'' then PrintNFStr(fDifStr(2,CommonSet.PreLine1));
                    end;

                    PrintNFStr(' ');
                    SelectF(14);
                    PrintNFStr('     ��� �'+IntToStr(CommonSet.CashChNum));
                    SelectF(10);
                    PrintNFStr(' ');
                    PrintNFStr('��������: '+TabCh.Name);
                    PrintNFStr('����: '+TabCh.NumTable);
                    PrintNFStr('������: '+FormatDateTime('dd.mm.yyyy hh:nn:ss',TabCh.OpenTime));
                    StrWk:='----------------------------------------';
                    PrintNFStr(StrWk);
                    StrWk:=' ��������     ���-��   ����    �����  ';
                    PrintNFStr(StrWk);
                    StrWk:='----------------------------------------';
                    PrintNFStr(StrWk);

                    rDiscont:=0;

                    quCheckOrg.First;
                    while not quCheckOrg.Eof do
                    begin      //��������� �������

                      PosCh.Name:=quCheckOrgNAME.AsString;
                      PosCh.Code:=quCheckOrgCODE.AsString;
                      PosCh.AddName:='';

                      StrWk:= Copy(PosCh.Name,1,36);
                      while Length(StrWk)<36 do StrWk:=StrWk+' ';
                      PrintNFStr(StrWk); //������� ����� �������� - �������� �������

                      rDiscont:=rDiscont+quCheckOrgDISCOUNTSUM.AsFloat;

                      Str(quCheckOrgQUANTITY.AsFloat:7:3,StrWk1);
                      StrWk:='          '+StrWk1;
                      Str(quCheckOrgPRICE.AsFloat:7:2,StrWk1);
                      StrWk:=StrWk+' '+StrWk1;
                      Str(quCheckOrgSUMMA.AsFloat:8:2,StrWk1);
                      StrWk:=StrWk+' '+StrWk1+'���';
                      PrintNFStr(StrWk);

                      quCheckOrg.Next;
                      if not quCheckOrg.Eof then
                      begin
                        while quCheckOrgITYPE.AsInteger=1 do
                        begin  //������������
                          if quCheckOrg.Eof then break;
                          if quCheckOrgSifr.AsInteger>0 then
                          if taModif.Locate('SIFR',quCheckOrgSifr.AsInteger,[]) then
                          begin
                            StrWk:='   '+Copy(taModifNAME.AsString,1,32);
                            PrintNFStr(StrWk);
                          end;
                          quCheckOrg.Next;
                        end;
                      end;
                    end;

                    StrWk:='----------------------------------------';
                    PrintNFStr(StrWk);
                    Str(TabCh.Summa:8:2,StrWk1);
                    StrWk:=' �����                '+StrWk1+' ���';
                    PrintNFStr(StrWk);

                    Case Operation of
                    0:begin
                        PrintNFStr('�������                                 ');
                        if fmCash.cEdit5.EditValue>0 then
                        begin
                          PrintNFStr(prDefFormatStr(32,'��������� �����:',fmCash.cEdit5.EditValue));
                        end;
                        if fmCash.cEdit2.EditValue>0 then
                        begin
                          PrintNFStr(prDefFormatStr(32,'��������:',fmCash.cEdit2.EditValue));
                        end;
                        if fmCash.cEdit6.EditValue>0 then
                        begin
                          PrintNFStr(prDefFormatStr(32,'��������� �����:',fmCash.cEdit6.EditValue));
                        end;
                        if (fmCash.cEdit5.EditValue+fmCash.cEdit2.EditValue)>rSum then
                        begin
                          PrintNFStr(prDefFormatStr(32,'�����:',(fmCash.cEdit5.EditValue+fmCash.cEdit6.EditValue+fmCash.cEdit2.EditValue)-rSum));
                        end;
                      end;
                    1:begin
                        PrintNFStr('�������                                 ');
                        if fmCash.cEdit5.EditValue>0 then
                        begin
                          PrintNFStr(prDefFormatStr(32,'��������� �����:',fmCash.cEdit5.EditValue));
                        end;
                        if fmCash.cEdit6.EditValue>0 then
                        begin
                          PrintNFStr(prDefFormatStr(32,'��������� �����:',fmCash.cEdit6.EditValue));
                        end;
                        if (rSum-fmCash.cEdit5.EditValue)>0 then
                        begin
                          PrintNFStr(prDefFormatStr(32,'��������:',(rSum-fmCash.cEdit5.EditValue-fmCash.cEdit6.EditValue)));
                        end;
                      end;
                    end;

                    if rDiscont>0.02 then
                    begin
                      PrintNFStr('');
                      rDProc:=RoundEx((rDiscont/(TabCh.Summa+rDiscont)*100)*100)/100;
                      Str(rDProc:5:1,StrWk1);
                      Str(rDiscont:8:2,StrWk);
                      StrWk:=' � �.�. ������ - '+StrWk1+'%  '+StrWk+'�.';
                      PrintNFStr(StrWk);
                    end;

                    if iPC=2 then
                    begin
                      PrintNFStr(' ');
                      Str(rv(fmCash.cEdit6.value):10:2,StrWk);
                      PrintNFStr('�������� '+Copy(SalePC.CliName,1,20)+':'+StrWk);
                      Str(rv(SalePC.rBalans-fmCash.cEdit6.value):10:2,StrWk);
                      PrintNFStr('������ �� �����:'+ StrWk);
                      Str(rv(SalePC.rBalansDay-fmCash.cEdit6.value):10:2,StrWk);
                      PrintNFStr('������� �������� ������:'+ StrWk);
                      PrintNFStr(' ');
                    end;

                    PrintNFStr('������: '+Person.Name);
                    PrintNFStr('�������: _____________________');


                    if CommonSet.LastLine1>'' then
                    begin
                      PrintNFStr('');
                      if fDifStr(1,CommonSet.LastLine1)>'' then PrintNFStr(fDifStr(1,CommonSet.LastLine1));
                      if fDifStr(2,CommonSet.LastLine1)>'' then PrintNFStr(fDifStr(2,CommonSet.LastLine1));
                    end;

                    PrintNFStr(' ');// PrintNFStr(' '); // PrintNFStr(' '); PrintNFStr(' ');PrintNFStr(' ');

                    CloseNFDoc;
                    if CommonSet.TypeFis<>'sp101' then CutDoc;
                  finally
                    bCheckOk:=True;
                    PrintFCheck:=False; //��� ���� � ������� ������� ��������� ������
                  end;
                end;}
              end;

            end;
            taModif.Active:=False;
            quCheckOrg.Active:=False;

            //����� ����� ���������� � table_all
            if bCheckOk then
            begin
              prSaveToAll.ParamByName('ID_TAB').AsInteger:=TabCh.Id;

              Case Operation of    //��� ���� �� ������� ������ � ����� � ��������� ������� ��
            0:begin
                if iPC>0 then
                begin
                  FormLog('SalePC',IntToStr(Person.Id)+' '+quTabsID_Personal.AsString+' '+quTabsNUMTABLE.AsString+' '+quTabsNUMZ.AsString);
                  prSaveToAll.ParamByName('OPERTYPE').AsString:='SalePC';
                end else
                begin
                  if iPayType=0 then  //
                  begin
                    FormLog('Sale',IntToStr(Person.Id)+' '+quTabsID_Personal.AsString+' '+quTabsNUMTABLE.AsString+' '+quTabsNUMZ.AsString);
                    prSaveToAll.ParamByName('OPERTYPE').AsString:='Sale';
                  end else
                  begin
                    FormLog('SaleBank',IntToStr(Person.Id)+' '+quTabsID_Personal.AsString+' '+quTabsNUMTABLE.AsString+' '+quTabsNUMZ.AsString);
                    prSaveToAll.ParamByName('OPERTYPE').AsString:='SaleBank';
                  end;
                end;
              end;
            1:begin
                if iPayType=0 then
                begin
                  FormLog('Ret',IntToStr(Person.Id)+' '+quTabsID_Personal.AsString+' '+quTabsNUMTABLE.AsString+' '+quTabsNUMZ.AsString);
                  prSaveToAll.ParamByName('OPERTYPE').AsString:='Ret';
                end else
                begin
                  FormLog('RetBank',IntToStr(Person.Id)+' '+quTabsID_Personal.AsString+' '+quTabsNUMTABLE.AsString+' '+quTabsNUMZ.AsString);
                  prSaveToAll.ParamByName('OPERTYPE').AsString:='RetBank';
                 end;
                end;
              end;

              if (CommonSet.Fis<=0)or(CommonSet.SpecChar=1) then
              begin
                Label4.Caption:='����� �'+IntToStr(CommonSet.CashZ)+'  ��� � '+IntToStr(CommonSet.CashChNum);
                prSaveToAll.ParamByName('CHECKNUM').AsInteger:=CommonSet.CashChNum;
              end
              else prSaveToAll.ParamByName('CHECKNUM').AsInteger:=Nums.iCheckNum;

              prSaveToAll.ParamByName('SKLAD').AsInteger:=0; //�������� �����������
              prSaveToAll.ParamByName('STATION').AsInteger:=CommonSet.Station; //����� �������
              prSaveToAll.ParamByName('ID_PERSONCLOSE').AsInteger:=Person.Id; //��� ������
              prSaveToAll.ParamByName('SPBAR').AsString:=SalePC.BarCode;

              prSaveToAll.ParamByName('CASHNUM').AsInteger:=CommonSet.CashNum;
              prSaveToAll.ParamByName('CHECKTYPE').AsInteger:=iFisCheck;
              prSaveToAll.ParamByName('TABSUM').AsFloat:=aCheckPlat[iOrg,1];
              prSaveToAll.ParamByName('DSUM').AsFloat:=aCheckPlat[iOrg,2];
              prSaveToAll.ParamByName('ASUM').AsFloat:=aCheckPlat[iOrg,3];
              prSaveToAll.ParamByName('DTSUM').AsFloat:=aCheckPlat[iOrg,4];
              prSaveToAll.ParamByName('BNSUM').AsFloat:=aCheckPlat[iOrg,5];
              prSaveToAll.ParamByName('RSUM').AsFloat:=aCheckPlat[iOrg,1]-aCheckPlat[iOrg,3]-aCheckPlat[iOrg,4]-aCheckPlat[iOrg,5];
              prSaveToAll.ParamByName('IORG').AsInteger:=iOrg;
              prSaveToAll.ParamByName('IDATE').AsInteger:=Trunc(date);
              prSaveToAll.ExecProc;

              // cashnum , checktype , tabsum , dsum , asum , dtsum , bnsum , rsum , iorg, idate

              IdH:=prSaveToAll.ParamByName('ID_H').AsInteger;

              //���� ���
              if aCheckPlat[iOrg,5]>0 then // ������
              begin
                idC:=GetId('CS');
                taCashSailSel.Active:=False;
                taCashSailSel.ParamByName('IDH').AsInteger:=IDC;
                taCashSailSel.Active:=True;

                taCashSailSel.Append;
                taCashSailSelID.AsInteger:=IdC;
                taCashSailSelCASHNUM.AsInteger:=CommonSet.CashNum;
                if CommonSet.Fis=0 then
                begin
                  taCashSailSelZNUM.AsInteger:=CommonSet.CashZ;
                  taCashSailSelCHECKNUM.AsInteger:=CommonSet.CashChNum;
                end
                else
                begin
                  taCashSailSelZNUM.AsInteger:=Nums.ZNum;
                  taCashSailSelCHECKNUM.AsInteger:=Nums.iCheckNum;
                end;

                taCashSailSelTAB_ID.AsInteger:=IdH;

                Case Operation of
                0: taCashSailSelTABSUM.AsFloat:=aCheckPlat[iOrg,5];
                1: taCashSailSelTABSUM.AsFloat:=(-1)*aCheckPlat[iOrg,5];
                end;

                taCashSailSelCLIENTSUM.AsFloat:=aCheckPlat[iOrg,5];
                taCashSailSelCHDATE.AsDateTime:=now;
                taCashSailSelCASHERID.AsInteger:=Person.Id;
                taCashSailSelWAITERID.AsInteger:=TabCh.Id_Personal;
                taCashSailSelPAYTYPE.AsInteger:=1;
                taCashSailSelPAYID.AsInteger:=iPayType;
                taCashSailSel.Post;

                taCashSailSel.Active:=False;
              end;
              if (aCheckPlat[iOrg,1]-aCheckPlat[iOrg,3]-aCheckPlat[iOrg,4]-aCheckPlat[iOrg,5])>0 then //��� ���
              begin
                idC:=GetId('CS');
                taCashSailSel.Active:=False;
                taCashSailSel.ParamByName('IDH').AsInteger:=IDC;
                taCashSailSel.Active:=True;

                taCashSailSel.Append;
                taCashSailSelID.AsInteger:=IdC;
                taCashSailSelCASHNUM.AsInteger:=CommonSet.CashNum;
                if CommonSet.Fis=0 then
                begin
                  taCashSailSelZNUM.AsInteger:=CommonSet.CashZ;
                  taCashSailSelCHECKNUM.AsInteger:=CommonSet.CashChNum;
                end
                else
                begin
                  taCashSailSelZNUM.AsInteger:=Nums.ZNum;
                  taCashSailSelCHECKNUM.AsInteger:=Nums.iCheckNum;
                end;

                taCashSailSelTAB_ID.AsInteger:=IdH;

                Case Operation of
                0: taCashSailSelTABSUM.AsFloat:=(aCheckPlat[iOrg,1]-aCheckPlat[iOrg,3]-aCheckPlat[iOrg,4]-aCheckPlat[iOrg,5]);
                1: taCashSailSelTABSUM.AsFloat:=(-1)*(aCheckPlat[iOrg,1]-aCheckPlat[iOrg,3]-aCheckPlat[iOrg,4]-aCheckPlat[iOrg,5]);
                end;

                taCashSailSelCLIENTSUM.AsFloat:=(aCheckPlat[iOrg,1]-aCheckPlat[iOrg,3]-aCheckPlat[iOrg,4]-aCheckPlat[iOrg,5]);
                taCashSailSelCHDATE.AsDateTime:=now;
                taCashSailSelCASHERID.AsInteger:=Person.Id;
                taCashSailSelWAITERID.AsInteger:=TabCh.Id_Personal;
                taCashSailSelPAYTYPE.AsInteger:=0;
                taCashSailSelPAYID.AsInteger:=0;
                taCashSailSel.Post;

                taCashSailSel.Active:=False;
              end;

            end;
            TabCh.rDiscDop:=0;
          end;
          quCheckOrgItog.Next;
        end;

        //�������� �����
        if (Tab.DType=3) then
        begin
          Case Operation of    //��� ���� �� ������� ������ � ����� � ��������� ������� ��
          0: prWritePCSum(Tab.DBar,rv(CalcBonus(Tab.Id,Tab.DPercent)),IdH); //��� �������
          1: prWritePCSum(Tab.DBar,rv(CalcBonus(Tab.Id,Tab.DPercent)*(-1)),IdH);      //��� ��������
          end;
        end;

        // ��������� �������� �� ��������� �����
        Case Operation of    //��� ���� �� ������� ������ � ����� � ��������� ������� ��
        0: if iPC>0 then prWritePCSum(SalePC.BarCode,rv(fmCash.CEdit6.Value*(-1)),IdH); //- ��� �������
        1: if iPC>0 then prWritePCSum(SalePC.BarCode,rv(fmCash.CEdit6.Value),IdH);      //+ ��� ��������
        end;

       // ������ �������� ����
        quDelTab.Active:=False;
        quDelTab.ParamByName('Id').AsInteger:=TabCh.Id;

        trDel.StartTransaction;
        quDelTab.Active:=True;
        trDel.Commit;

      finally
        bPrintCheck:=False; //��� �� �������� �������
        PrintFCheck:=False; //��� ���� � ������� ������� ��������� ������
      end;
    end else
    begin //������ ������� �� ������ ������� �����
      quTabs.Edit;
      quTabsISTATUS.AsInteger:=iCurStatus;
      quTabs.Post;
      quTabs.Refresh;
    end;
    if bExitPers then
    begin
      bExitPers:=False;
      Operation:=0;
      Label5.Caption:='�������� - ������� ��������.';
      DestrViewPers;
      fmPre.ShowModal;
    end
    else
    begin
      fmMainCashRn.CreateViewPers(True);
      Operation:=0;
      Label5.Caption:='�������� - ������� ��������.';
    end;
  end;
end;

procedure TfmMainCashRn.cxButton2Click(Sender: TObject);
Var StrWk:String;
    iRet:Integer;
    iNumSm,iCountDoc,iMin,iMax:Integer;
    rSumNal,rSumRet,rSumBNal,rSumBRet,rSumDisc:Real;
//    rSum1,rSum2,rSum3:Real;
    l:ShortInt;
    StrP:String;
    iQ:INteger;
begin
  if not CanDo('prXRep') then begin Showmessage('��� ����.'); exit; end;
  prWriteLog('!!XRep;'+IntToStr(Person.Id)+';'+Person.Name+';');

  Event_RegEx(209,0,0,0,'','',0,0,0,0,0,0);

  with dmC do
  begin
    quOrgsSt.Active:=False;
    quOrgsSt.ParamByName('IST').AsInteger:=CommonSet.Station;
    quOrgsSt.Active:=True;

    if quOrgsSt.RecordCount=1 then prGetCashPar(quOrgsStIORG.AsInteger)
    else
    begin
      fmSelOrg.ShowModal;
      prGetCashPar(quOrgsStIORG.AsInteger);
    end;

    if (CommonSet.fis>0)and(CommonSet.SpecChar=0) then //����������� ����������
    begin
      if OpenZ then
      begin
        OpenNFDoc;

        if SpecVal.SPH1>'' then PrintNFStr(SpecVal.SPH1);
        if SpecVal.SPH2>'' then PrintNFStr(SpecVal.SPH2);
        if SpecVal.SPH3>'' then PrintNFStr(SpecVal.SPH3);
        if SpecVal.SPH4>'' then PrintNFStr(SpecVal.SPH4);
        if SpecVal.SPH5>'' then PrintNFStr(SpecVal.SPH5);

        if GetX=False then Showmessage('������ ��� ��������� X-������.');
//        CutDoc;
      end
      else  //�������� �����
      begin
        iRet:=InspectSt(StrWk);
        if iRet=0 then
        begin
          if ZOpen=False then Showmessage('������ ��� �������� �����.')
          else
          begin
            CutDoc;
            OpenNFDoc;

            if SpecVal.SPH1>'' then PrintNFStr(SpecVal.SPH1);
            if SpecVal.SPH2>'' then PrintNFStr(SpecVal.SPH2);
            if SpecVal.SPH3>'' then PrintNFStr(SpecVal.SPH3);
            if SpecVal.SPH4>'' then PrintNFStr(SpecVal.SPH4);
            if SpecVal.SPH5>'' then PrintNFStr(SpecVal.SPH5);

            if GetX=False then Showmessage('������ ��� ��������� X-������.');
//            CutDoc;

          end;
        end
        else
        begin
          Label3.Caption:='������ ���: ������ ('+IntToStr(iRet)+')';
          Showmessage('��������� ������ ��� - '+''''+StrWk+''''+'  ������ ('+IntToStr(iRet)+')');
        end;
      end;
    end else
    begin //��� ����� ������������ ������ �����?

      //������ ������� ��� ���� ���������
      with dmC do
      with dmC1 do
      begin
        iNumSm:=Nums.ZNum;
        iCountDoc:=Nums.iCheckNum;

        //������� ���, ������
        rSumNal:=0;
        rSumBNal:=0;
        quRSum.Active:=False;
        quRSum.ParamByName('ICASHNUM').AsInteger:=CommonSet.CashNum;
        quRSum.ParamByName('IZNUM').AsInteger:=iNumSm;
        quRSum.Active:=True;
        quRSum.First;
        while not quRSum.Eof do
        begin
          if quRSumPAYTYPE.AsInteger=0 then rSumNal:=quRSumSUMR.AsFloat;
          if quRSumPAYTYPE.AsInteger>0 then rSumBNal:=rSumBNal+quRSumSUMR.AsFloat;
          quRSum.Next;
        end;
        quRSum.Active:=False;

        //�������� ���, ������
        rSumRet:=0;
        rSumBRet:=0;
        quRSumRet.Active:=False;
        quRSumRet.ParamByName('ICASHNUM').AsInteger:=CommonSet.CashNum;
        quRSumRet.ParamByName('IZNUM').AsInteger:=iNumSm;
        quRSumRet.Active:=True;
        quRSumRet.First;
        while not quRSumRet.Eof do
        begin
          if quRSumRetPAYTYPE.AsInteger=0 then rSumRet:=quRSumRetSUMR.AsFloat;
          if quRSumRetPAYTYPE.AsInteger>0 then rSumBRet:=rSumBRet+quRSumRetSUMR.AsFloat;
          quRSumRet.Next;
        end;
        quRSumRet.Active:=False;

        rSumRet:=(-1)*rSumRet;
        rSumBRet:=(-1)*rSumBRet;

        iMax:=0;
        iMin:=0;
        quMaxMin.Active:=False;
        quMaxMin.ParamByName('ICASHNUM').AsInteger:=CommonSet.CashNum;
        quMaxMin.ParamByName('IZNUM').AsInteger:=iNumSm;
        quMaxMin.Active:=True;
        if quMaxMin.RecordCount>0 then
        begin
          quMaxMin.First;
          iMin:=quMaxMinIBEG.AsInteger;
          iMax:=quMaxMinIEND.AsInteger;
        end;
        quMaxMin.Active:=False;

        rSumDisc:=0;
        if iMax>0 then
        begin
          rSumDisc:=0;
        end else
        begin
          quDSum.Active:=False;
          quDSum.ParamByName('IMIN').AsInteger:=iMin;
          quDSum.ParamByName('IMAX').AsInteger:=iMax;
          quDSum.Active:=True;
          if quDSum.RecordCount>0 then
          begin
            quDSum.First;
            rSumDisc:=quDSumDSUM.AsFloat;
          end;
        end;

            //������� ��������
        if (Pos('COM',CommonSet.SpecBoxX)>0)or(Pos('LPT',CommonSet.SpecBoxX)>0) then
        begin
          try
            l:=40;

            prDevOpen(CommonSet.SpecBoxX,0);

            BufPr.iC:=0;

            StrP:=CommonSet.SpecBoxX;
            prSetFont(StrP,10,0);
            PrintStr(StrP,SpecVal.SPH1);
            PrintStr(StrP,SpecVal.SPH2);
            PrintStr(StrP,SpecVal.SPH3);
            PrintStr(StrP,SpecVal.SPH4);
            PrintStr(StrP,SpecVal.SPH5);
            StrWk:=IntToStr(iMax+1); //����� ���������
            while Length(StrWk)<5 do StrWk:='0'+StrWk;
            if CommonSet.SpecChar=1 then
              PrintStr(StrP,'�������� N: '+CommonSet.CashSer+'         N ���: '+StrWk);
            PrintStr(StrP,FormatDateTime('dd-mm-yyyy                         hh:nn',now));
            PrintStr(StrP,' ');
            StrWk:='            X-����� N '+IntToStr(iNumSm);
            PrintStr(StrP,StrWk);
            PrintStr(StrP,' ');
            PrintStr(StrP,'��������:');
            PrintStr(StrP,prDefFormatStr1(l,'�������',rSumNal));
            PrintStr(StrP,prDefFormatStr1(l,'�����. �������',0));
            PrintStr(StrP,prDefFormatStr1(l,'�������',rSumRet));
            PrintStr(StrP,' ');
            PrintStr(StrP,'������:');
            PrintStr(StrP,prDefFormatStr1(l,'�������',0));
            PrintStr(StrP,prDefFormatStr1(l,'�����. �������',0));
            PrintStr(StrP,' ');
            PrintStr(StrP,'����. �����:');
            PrintStr(StrP,prDefFormatStr1(l,'�������',rSumBNal));
            PrintStr(StrP,prDefFormatStr1(l,'�����. �������',rSumBRet));
            PrintStr(StrP,' ');
            PrintStr(StrP,'����. �����2:');
            PrintStr(StrP,prDefFormatStr1(l,'�������',0));
            PrintStr(StrP,prDefFormatStr1(l,'�����. �������',0));
            PrintStr(StrP,' ');
            PrintStr(StrP,'����. �����3:');
            PrintStr(StrP,prDefFormatStr1(l,'�������',0));
            PrintStr(StrP,prDefFormatStr1(l,'�����. �������',0));
            PrintStr(StrP,' ');
            PrintStr(StrP,'����. �����4:');
            PrintStr(StrP,prDefFormatStr1(l,'�������',0));
            PrintStr(StrP,prDefFormatStr1(l,'�����. �������',0));
            PrintStr(StrP,' ');
            PrintStr(StrP,'================= ����� ================');
            PrintStr(StrP,prDefFormatStr1(l,'�������',rSumNal+rSumBNal));
            PrintStr(StrP,prDefFormatStr1(l,'� �.�. ����:',0));
            PrintStr(StrP,prDefFormatStr1(l,' ������',rSumDisc));
            PrintStr(StrP,prDefFormatStr1(l,'�����. �������',0));
            PrintStr(StrP,prDefFormatStr1(l,'� �.�. ����:',0));
            PrintStr(StrP,prDefFormatStr1(l,'�������',rSumRet+rSumBRet));
            PrintStr(StrP,prDefFormatStr1(l,'� �.�. ����:',0));
            PrintStr(StrP,prDefFormatStr1(l,'������������:',0));
            PrintStr(StrP,prDefFormatStr1(l,'����������:',0));
            PrintStr(StrP,' ');
            PrintStr(StrP,prDefFormatStr2(l,'��������� ����������:',iCountDoc+4));
            PrintStr(StrP,prDefFormatStr2(l,'� �.�. �����:',iCountDoc));
            PrintStr(StrP,prDefFormatStr2(l,'� �.�. �����. �����:',0));
            PrintStr(StrP,prDefFormatStr2(l,'� �.�. ������. ���. ���. �����:',0));
            PrintStr(StrP,' ');
            PrintStr(StrP,prDefFormatStr2(l,'�������:',iCountDoc));
            PrintStr(StrP,prDefFormatStr2(l,'�����. �������:',0));
            PrintStr(StrP,prDefFormatStr2(l,'�������:',0));
            PrintStr(StrP,prDefFormatStr2(l,'������������:',0));
            PrintStr(StrP,prDefFormatStr2(l,'����������:',0));

            prWrBuf(StrP);

            if CommonSet.SpecChar=1 then CutDocPrSpec
            else prCutDoc(StrP,0);

          finally
            prDevClose(StrP,0);
          end;
        end; //if Pos('COM',CommonSet.SpecBoxX)>0 then

        if Pos('DB',CommonSet.SpecBoxX)>0 then
        begin
          try
          //��������� �������
            with dmC1 do
            begin

              CommonSet.PrePrintPort:=prGetPrePrintPort;

              quPrint.Active:=False;
              quPrint.Active:=True;
              iQ:=GetId('PQH'); //����������� ������� �� 1-�
              l:=40;

              StrP:=CommonSet.SpecBoxX;

              PrintDBStr(iQ,999,StrP,SpecVal.SPH1,10,0);
              PrintDBStr(iQ,999,StrP,SpecVal.SPH2,10,0);
              PrintDBStr(iQ,999,StrP,SpecVal.SPH3,10,0);
              PrintDBStr(iQ,999,StrP,SpecVal.SPH4,10,0);
              PrintDBStr(iQ,999,StrP,SpecVal.SPH5,10,0);
              StrWk:=IntToStr(iMax+1); //����� ���������
              while Length(StrWk)<5 do StrWk:='0'+StrWk;
              if CommonSet.SpecChar=1 then
              PrintDBStr(iQ,999,StrP,'�������� N: '+CommonSet.CashSer+'         N ���: '+StrWk,10,0);
              PrintDBStr(iQ,999,StrP,FormatDateTime('dd-mm-yyyy                         hh:nn',now),10,0);
              PrintDBStr(iQ,999,StrP,' ',10,0);
              StrWk:='            X-����� N '+IntToStr(iNumSm);
              PrintDBStr(iQ,999,StrP,StrWk,10,0);
              PrintDBStr(iQ,999,StrP,' ',10,0);
              PrintDBStr(iQ,999,StrP,'��������:',10,0);
              PrintDBStr(iQ,999,StrP,prDefFormatStr1(l,'�������',rSumNal),10,0);
              PrintDBStr(iQ,999,StrP,prDefFormatStr1(l,'�����. �������',0),10,0);
              PrintDBStr(iQ,999,StrP,prDefFormatStr1(l,'�������',rSumRet),10,0);
              PrintDBStr(iQ,999,StrP,' ',10,0);
              PrintDBStr(iQ,999,StrP,'������:',10,0);
              PrintDBStr(iQ,999,StrP,prDefFormatStr1(l,'�������',0),10,0);
              PrintDBStr(iQ,999,StrP,prDefFormatStr1(l,'�����. �������',0),10,0);
              PrintDBStr(iQ,999,StrP,' ',10,0);
              PrintDBStr(iQ,999,StrP,'����. �����:',10,0);
              PrintDBStr(iQ,999,StrP,prDefFormatStr1(l,'�������',rSumBNal),10,0);
              PrintDBStr(iQ,999,StrP,prDefFormatStr1(l,'�����. �������',rSumBRet),10,0);
              PrintDBStr(iQ,999,StrP,' ',10,0);
              PrintDBStr(iQ,999,StrP,'����. �����2:',10,0);
              PrintDBStr(iQ,999,StrP,prDefFormatStr1(l,'�������',0),10,0);
              PrintDBStr(iQ,999,StrP,prDefFormatStr1(l,'�����. �������',0),10,0);
              PrintDBStr(iQ,999,StrP,' ',10,0);
              PrintDBStr(iQ,999,StrP,'����. �����3:',10,0);
              PrintDBStr(iQ,999,StrP,prDefFormatStr1(l,'�������',0),10,0);
              PrintDBStr(iQ,999,StrP,prDefFormatStr1(l,'�����. �������',0),10,0);
              PrintDBStr(iQ,999,StrP,' ',10,0);
              PrintDBStr(iQ,999,StrP,'����. �����4:',10,0);
              PrintDBStr(iQ,999,StrP,prDefFormatStr1(l,'�������',0),10,0);
              PrintDBStr(iQ,999,StrP,prDefFormatStr1(l,'�����. �������',0),10,0);
              PrintDBStr(iQ,999,StrP,' ',10,0);
              PrintDBStr(iQ,999,StrP,'================= ����� ================',10,0);
              PrintDBStr(iQ,999,StrP,prDefFormatStr1(l,'�������',rSumNal+rSumBNal),10,0);
              PrintDBStr(iQ,999,StrP,prDefFormatStr1(l,'� �.�. ����:',0),10,0);
              PrintDBStr(iQ,999,StrP,prDefFormatStr1(l,' ������',rSumDisc),10,0);
              PrintDBStr(iQ,999,StrP,prDefFormatStr1(l,'�����. �������',0),10,0);
              PrintDBStr(iQ,999,StrP,prDefFormatStr1(l,'� �.�. ����:',0),10,0);
              PrintDBStr(iQ,999,StrP,prDefFormatStr1(l,'�������',rSumRet+rSumBRet),10,0);
              PrintDBStr(iQ,999,StrP,prDefFormatStr1(l,'� �.�. ����:',0),10,0);
              PrintDBStr(iQ,999,StrP,prDefFormatStr1(l,'������������:',0),10,0);
              PrintDBStr(iQ,999,StrP,prDefFormatStr1(l,'����������:',0),10,0);
              PrintDBStr(iQ,999,StrP,' ',10,0);
              PrintDBStr(iQ,999,StrP,prDefFormatStr2(l,'��������� ����������:',iCountDoc+4),10,0);
              PrintDBStr(iQ,999,StrP,prDefFormatStr2(l,'� �.�. �����:',iCountDoc),10,0);
              PrintDBStr(iQ,999,StrP,prDefFormatStr2(l,'� �.�. �����. �����:',0),10,0);
              PrintDBStr(iQ,999,StrP,prDefFormatStr2(l,'� �.�. ������. ���. ���. �����:',0),10,0);
              PrintDBStr(iQ,999,StrP,' ',10,0);
              PrintDBStr(iQ,999,StrP,prDefFormatStr2(l,'�������:',iCountDoc),10,0);
              PrintDBStr(iQ,999,StrP,prDefFormatStr2(l,'�����. �������:',0),10,0);
              PrintDBStr(iQ,999,StrP,prDefFormatStr2(l,'�������:',0),10,0);
              PrintDBStr(iQ,999,StrP,prDefFormatStr2(l,'������������:',0),10,0);
              PrintDBStr(iQ,999,StrP,prDefFormatStr2(l,'����������:',0),10,0);

              prAddPrintQH(999,iQ,FormatDateTime('dd.mm hh:nn:sss',now)+', ������� - '+IntToStr(CommonSet.Station)+','+CommonSet.SpecBoxX);
              quPrint.Active:=False;
            end;
          finally
          end;
        end; //if Pos('COM',CommonSet.SpecBoxX)>0 then

        if Pos('fis',CommonSet.SpecBoxX)>0 then
        begin
          TestPrint:=1;
          While PrintQu and (TestPrint<=MaxDelayPrint) do
          begin
  //        showmessage('������� �����, ��������� �������.');
            prWriteLog('������������� ������ ����� � �������.');
            delay(1000);
            inc(TestPrint);
          end;
          try
            PrintFCheck:=True; //��� ���� � ������� ������� ��������� ������

            l:=40;
            if CommonSet.SpecBoxX='fisshtrih' then l:=32;

            OpenNFDoc;
//            CutDoc;

            PrintNFStr(SpecVal.SPH1);
            PrintNFStr(SpecVal.SPH2);
            PrintNFStr(SpecVal.SPH3);
            PrintNFStr(SpecVal.SPH4);
            PrintNFStr(SpecVal.SPH5);
            StrWk:=IntToStr(iMax+1); //����� ���������
            while Length(StrWk)<5 do StrWk:='0'+StrWk;
            PrintNFStr('�������� N: '+CommonSet.CashSer+' N ���: '+StrWk);
            PrintNFStr(FormatDateTime('dd-mm-yyyy                 hh:nn',now));
            PrintNFStr(' ');
            StrWk:='            X-����� N '+IntToStr(iNumSm);
            PrintNFStr(StrWk);
            PrintNFStr(' ');
            PrintNFStr('��������:');
            PrintNFStr(prDefFormatStr1(l,'�������',rSumNal));
            PrintNFStr(prDefFormatStr1(l,'�����. �������',0));
            PrintNFStr(prDefFormatStr1(l,'�������',rSumRet));
            PrintNFStr(' ');
            PrintNFStr('������:');
            PrintNFStr(prDefFormatStr1(l,'�������',0));
            PrintNFStr(prDefFormatStr1(l,'�����. �������',0));
            PrintNFStr(' ');
            PrintNFStr('����. �����:');
            PrintNFStr(prDefFormatStr1(l,'�������',rSumBNal));
            PrintNFStr(prDefFormatStr1(l,'�����. �������',rSumBRet));
            PrintNFStr(' ');
            PrintNFStr('����. �����2:');
            PrintNFStr(prDefFormatStr1(l,'�������',0));
            PrintNFStr(prDefFormatStr1(l,'�����. �������',0));
            PrintNFStr(' ');
            PrintNFStr('����. �����3:');
            PrintNFStr(prDefFormatStr1(l,'�������',0));
            PrintNFStr(prDefFormatStr1(l,'�����. �������',0));
            PrintNFStr(' ');
            PrintNFStr('����. �����4:');
            PrintNFStr(prDefFormatStr1(l,'�������',0));
            PrintNFStr(prDefFormatStr1(l,'�����. �������',0));
            PrintNFStr(' ');
            PrintNFStr('================= ����� ================');
            PrintNFStr(prDefFormatStr1(l,'�������',rSumNal+rSumBNal));
            PrintNFStr(prDefFormatStr1(l,'� �.�. ����:',0));
            PrintNFStr(prDefFormatStr1(l,' ������',rSumDisc));
            PrintNFStr(prDefFormatStr1(l,'�����. �������',0));
            PrintNFStr(prDefFormatStr1(l,'� �.�. ����:',0));
            PrintNFStr(prDefFormatStr1(l,'�������',rSumRet+rSumBRet));
            PrintNFStr(prDefFormatStr1(l,'� �.�. ����:',0));
            PrintNFStr(prDefFormatStr1(l,'������������:',0));
            PrintNFStr(prDefFormatStr1(l,'����������:',0));
            PrintNFStr(' ');
            PrintNFStr(prDefFormatStr2(l,'��������� ����������:',iCountDoc+4));
            PrintNFStr(prDefFormatStr2(l,'� �.�. �����:',iCountDoc));
            PrintNFStr(prDefFormatStr2(l,'� �.�. �����. �����:',0));
            PrintNFStr(prDefFormatStr2(l,'� �.�. ������. ���. ���. �����:',0));
            PrintNFStr(' ');
            PrintNFStr(prDefFormatStr2(l,'�������:',iCountDoc));
            PrintNFStr(prDefFormatStr2(l,'�����. �������:',0));
            PrintNFStr(prDefFormatStr2(l,'�������:',0));
            PrintNFStr(prDefFormatStr2(l,'������������:',0));
            PrintNFStr(prDefFormatStr2(l,'����������:',0));

            PrintNFStr(' ');//PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');

            CloseNFDoc;
            if CommonSet.TypeFis<>'sp101' then CutDoc;
          finally
            PrintFCheck:=False; //��� ���� � ������� ������� ��������� ������
          end;
        end; //if Pos('fis',CommonSet.SpecBoxX)>0 then

      end;
    end;
    quOrgsSt.Active:=False;
  end;
end;

procedure TfmMainCashRn.cxButton3Click(Sender: TObject);
Var iRet,iSum,iSumBN,iSumR:INteger;
    StrWk:String;
    iNumSm,iCountDoc,iMin,iMax:Integer;
    rSumNal,rSumRet,rSumBNal,rSumBRet,rSumDisc:Real;
    l:ShortInt;
    StrP:String;
    iQ:INteger;
begin
  //�������� �����
  if not CanDo('prZRep') then begin Showmessage('��� ����.'); exit; end;

  if MessageDlg('���������� �������� �����?',mtConfirmation, [mbYes, mbNo], 0) <> mrYes then Exit;

  //�������� �� ������� �������� �������
  if IsOpenZ then
  begin
    if not CanDo('prZRepAll') then
    begin
      Showmessage('��������� ���� ������ - ���� �������� ������.');
      exit;
    end;
  end;

  prWriteLog('!!ZRep;'+IntToStr(Person.Id)+';'+Person.Name+';');

  Event_RegEx(210,0,0,0,'','',0,0,0,0,0,0);

  with dmC do
  begin
    quOrgsSt.Active:=False;
    quOrgsSt.ParamByName('IST').AsInteger:=CommonSet.Station;
    quOrgsSt.Active:=True;

    if quOrgsSt.RecordCount=1 then prGetCashPar(quOrgsStIORG.AsInteger)
    else
    begin
      fmSelOrg.ShowModal;
      prGetCashPar(quOrgsStIORG.AsInteger);
    end;
  end;

  if (CommonSet.Fis>0)and(CommonSet.SpecChar=0) then //����������� ����������
  begin
    if OpenZ then //��� �������� ����� � �������� ������� �� �����
    begin

//      CutDoc;
      OpenNFDoc;

      if SpecVal.SPH1>'' then PrintNFStr(SpecVal.SPH1);
      if SpecVal.SPH2>'' then PrintNFStr(SpecVal.SPH2);
      if SpecVal.SPH3>'' then PrintNFStr(SpecVal.SPH3);
      if SpecVal.SPH4>'' then PrintNFStr(SpecVal.SPH4);
      if SpecVal.SPH5>'' then PrintNFStr(SpecVal.SPH5);

      //�������� ��� �������
      ZClose;
      iRet:=InspectSt(StrWk);
      if iRet=0 then
      begin
        Label3.Caption:='������ ���: ��� ��.';
        if CashDate(StrWk) then Label2.Caption:='�������� ����: '+StrWk;
        inc(CommonSet.CashZ);

        GetSerial;
        CashDate(Nums.sDate);
        if GetNums then CommonSet.CashZ:=Nums.ZNum;
        GetRes; //�������

        WriteStatus;
        if CommonSet.IncNumTab>0 then CommonSet.IncNumTab:=1;//��������� ������� ������� ������
        WriteCheckNum;

        acRecalcCount.Execute; //����� currest
      end
      else
      begin
        Label3.Caption:='������ ���: ������ ('+IntToStr(iRet)+')';
        WriteHistoryFR('������ ���: ������ ('+IntToStr(iRet)+')');
      end;
    end
    else
    begin
      Showmessage('����� ��� �������.');
    end;
  end;
  If (CommonSet.CashNum<0)and(CommonSet.SpecChar=1) then    //������ ����������
  begin
    try
      CommonSet.CashNum:=(-1)*CommonSet.CashNum; //������ � ���������� �����
      fmMessTxt:=tfmMessTxt.Create(Application);
      fmMessTxt.Button1.Enabled:=False;
      fmMessTxt.Button2.Enabled:=False;
      fmMessTxt.Show;
      with fmMessTxt do
      begin
        Memo1.Lines.Add('������������� ��.');
        if CommonSet.CashPort>0 then
        begin
          StrWk:='COM'+IntToStr(CommonSet.CashPort);
          if CashOpen(PChar(StrWk)) then   //dll �������
          begin
            Memo1.Lines.Add('���������� - Ok');

            Nums.bOpen:=False;
            if CheckOpen then CheckCancel;
            Nums.iRet:=InspectSt(Nums.sRet);
            Memo1.Lines.Add(Nums.sRet);

            if Nums.iRet=0 then Nums.bOpen:=True
            else
            begin
              if MessageDlg('��� ���������� ������ ���������� ������� �����. �������� ��������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
              begin
                if OpenZ then
                begin
                  ZClose;//��� �������� ����� � �������� ������� �� �����
                  acRecalcCount.Execute; //����� currest
                end;
              end;
            end;

//             Nums.bOpen:=True;  //��������
            if Nums.bOpen then
            begin
              Memo1.Lines.Add('����� ... ���� ��������� ������ c ��.');
              CashDate(Nums.sDate);
              GetNums; //������ �����
              GetRes; //������� � ����� �����
              WriteStatus;

              Memo1.Lines.Add('�� - Ok');
              Memo1.Lines.Add('  ������������ ������.');
              with dmC do
              begin
                taFis.Active:=False;
                taFis.ParamByName('IST').AsInteger:=CommonSet.Station;
                taFis.Active:=True;
                Memo1.Lines.Add('  ������������ ��.');
                Memo1.Lines.Add('    ����� ... ���� ������.');

                taFis.First;
                while not taFis.Eof do
                begin
                  if abs(taFisRSUM.AsFloat)>=0.005 then
                  begin
                      //��������� ���
                    if Pos('Sale',taFisOPERTYPE.AsString)>0 then CheckStart;
                    if Pos('Ret',taFisOPERTYPE.AsString)>0 then CheckRetStart;

                    while TestStatus('CheckStart',sMessage)=False do
                    begin
                      fmAttention.Label1.Caption:=sMessage;
                      prWriteLog('~~AttentionShow;'+sMessage);
                      fmAttention.ShowModal;
                      Nums.iRet:=InspectSt(Nums.sRet);
                    end;

                    PosCh.Name:='����� 1';
                    PosCh.Code:=taFisID_TAB.AsString;
                    PosCh.AddName:='';
                    PosCh.Price:=RoundEx(abs(taFisRSUM.AsFloat)*100);
                    PosCh.Count:=1000;
                    PosCh.Sum:=abs(taFisRSUM.AsFloat);

                    CheckAddPos(iSum);

                    while TestStatus('CheckAddPos',sMessage)=False do
                    begin
                      fmAttention.Label1.Caption:=sMessage;
                      prWriteLog('~~AttentionShow;'+sMessage);
                      fmAttention.ShowModal;
                      Nums.iRet:=InspectSt(Nums.sRet);
                    end;

                       //������������ ���������� - ������ ����, ������, ������;
                    CheckTotal(iSum);

                    while TestStatus('CheckTotal',sMessage)=False do
                    begin
                      fmAttention.Label1.Caption:=sMessage;
                      prWriteLog('~~AttentionShow;'+sMessage);
                      fmAttention.ShowModal;
                      Nums.iRet:=InspectSt(Nums.sRet);
                    end;

                    if pos('Bank',taFisOPERTYPE.AsString)>0 then
                    begin
                      iSumBN:=0;
                      iSumR:=RoundEx(abs(taFisRSUM.AsFloat)*100);
                      taFisBN.Active:=False;
                      taFisBN.ParamByName('IDH').AsInteger:=taFisID_TAB.AsInteger;
                      taFisBN.Active:=True;

                      if taFisBN.RecordCount>0 then iSumBN:=RoundEx(abs(taFisBNTABSUM.AsFloat)*100);
                      taFisBN.Active:=False;

                      if iSumBN>=iSumR then iSumBN:=iSumR;
                      if iSumBN>0 then CheckRasch(1,iSumBN,'',iSum); //������
                      if (iSumR-iSumBN)>0 then CheckRasch(0,(iSumR-iSumBN),'',iSum); //���������� ���
                    end
                    else CheckRasch(0,RoundEx(abs(taFisRSUM.AsFloat)*100),'',iSum); //���

                    while TestStatus('CheckRasch',sMessage)=False do
                    begin
                      fmAttention.Label1.Caption:=sMessage;
                      prWriteLog('~~AttentionShow;'+sMessage);
                      fmAttention.ShowModal;
                      Nums.iRet:=InspectSt(Nums.sRet);
                    end;

                    CheckClose;
                    while TestStatus('CheckClose',sMessage)=False do
                    begin
                      fmAttention.Label1.Caption:=sMessage;
                      prWriteLog('~~AttentionShow;'+sMessage);
                      fmAttention.ShowModal;
                      Nums.iRet:=InspectSt(Nums.sRet);
                    end;
                  end;

                  quSetPrint.ParamByName('IDH').AsInteger:=taFisID_TAB.AsInteger;
                  quSetPrint.ExecQuery;

                  taFis.Next;
                end;
                taFis.Active:=False;

              end;
              Memo1.Lines.Add('    ������ ���������.');
              Memo1.Lines.Add('  �������� �����.');

              if OpenZ then
              begin
                ZClose;//��� �������� ����� � �������� ������� �� �����
                acRecalcCount.Execute; //����� currest
              end;
              Memo1.Lines.Add('  ������������ ��.');

            end else
            begin
              Memo1.Lines.Add('������ ��.');
            end;
          end else Memo1.Lines.Add('�� - ������ ������������� ����������.');
        end;
      end;

      Delay(1000);
      fmMessTxt.Close;
    finally
      CommonSet.CashNum:=(-1)*CommonSet.CashNum;
      fmMessTxt.Release;
    end;
  end;

  If ((CommonSet.Fis>0)and(CommonSet.SpecChar=1))or((CommonSet.Fis=0)and(CommonSet.SpecChar=0)) then //������������
  begin  //��� ������������ ������ �������
    with dmC do
    with dmC1 do
    begin
      iNumSm:=Nums.ZNum;
      iCountDoc:=Nums.iCheckNum;

      //������� ���, ������
      rSumNal:=0;
      rSumBNal:=0;
      quRSum.Active:=False;
      quRSum.ParamByName('ICASHNUM').AsInteger:=CommonSet.CashNum;
      quRSum.ParamByName('IZNUM').AsInteger:=iNumSm;
      quRSum.Active:=True;
      quRSum.First;
      while not quRSum.Eof do
      begin
        if quRSumPAYTYPE.AsInteger=0 then rSumNal:=quRSumSUMR.AsFloat;
        if quRSumPAYTYPE.AsInteger>0 then rSumBNal:=rSumBNal+quRSumSUMR.AsFloat;
        quRSum.Next;
      end;
      quRSum.Active:=False;

      //�������� ���, ������
      rSumRet:=0;
      rSumBRet:=0;
      quRSumRet.Active:=False;
      quRSumRet.ParamByName('ICASHNUM').AsInteger:=CommonSet.CashNum;
      quRSumRet.ParamByName('IZNUM').AsInteger:=iNumSm;
      quRSumRet.Active:=True;
      quRSumRet.First;
      while not quRSumRet.Eof do
      begin
        if quRSumRetPAYTYPE.AsInteger=0 then rSumRet:=quRSumRetSUMR.AsFloat;
        if quRSumRetPAYTYPE.AsInteger>0 then rSumBRet:=rSumBRet+quRSumRetSUMR.AsFloat;
        quRSumRet.Next;
      end;
      quRSumRet.Active:=False;

      rSumRet:=(-1)*rSumRet;
      rSumBRet:=(-1)*rSumBRet;

      iMax:=0;
      iMin:=0;
      quMaxMin.Active:=False;
      quMaxMin.ParamByName('ICASHNUM').AsInteger:=CommonSet.CashNum;
      quMaxMin.ParamByName('IZNUM').AsInteger:=iNumSm;
      quMaxMin.Active:=True;
      if quMaxMin.RecordCount>0 then
      begin
        quMaxMin.First;
        iMin:=quMaxMinIBEG.AsInteger;
        iMax:=quMaxMinIEND.AsInteger;
      end;
      quMaxMin.Active:=False;

      rSumDisc:=0;
      if iMax>0 then
      begin
        rSumDisc:=0;
      end else
      begin
        quDSum.Active:=False;
        quDSum.ParamByName('IMIN').AsInteger:=iMin;
        quDSum.ParamByName('IMAX').AsInteger:=iMax;
        quDSum.Active:=True;
        if quDSum.RecordCount>0 then
        begin
          quDSum.First;
          rSumDisc:=quDSumDSUM.AsFloat;
        end;
      end;

          //������� ��������
      if (Pos('COM',CommonSet.SpecBoxX)>0)or(Pos('LPT',CommonSet.SpecBoxX)>0) then
      begin
        try
          l:=40;
          prDevOpen(CommonSet.SpecBoxX,0);
          BufPr.iC:=0;

          StrP:=CommonSet.SpecBoxX;
          prSetFont(StrP,10,0);
          PrintStr(StrP,SpecVal.SPH1);
          PrintStr(StrP,SpecVal.SPH2);
          PrintStr(StrP,SpecVal.SPH3);
          PrintStr(StrP,SpecVal.SPH4);
          PrintStr(StrP,SpecVal.SPH5);
          StrWk:=IntToStr(iMax+1); //����� ���������
          while Length(StrWk)<5 do StrWk:='0'+StrWk;
//          PrintStr(StrP,'�������� N: '+CommonSet.CashSer+'         N ���: '+StrWk);   ������� ������������ - ��������� �� ����
          PrintStr(StrP,FormatDateTime('dd-mm-yyyy                         hh:nn',now));
          PrintStr(StrP,' ');
          StrWk:='            Z-����� N '+IntToStr(iNumSm);
          PrintStr(StrP,StrWk);
          PrintStr(StrP,' ');
          PrintStr(StrP,'��������:');
          PrintStr(StrP,prDefFormatStr1(l,'�������',rSumNal));
          PrintStr(StrP,prDefFormatStr1(l,'�����. �������',0));
          PrintStr(StrP,prDefFormatStr1(l,'�������',rSumRet));
          PrintStr(StrP,' ');
          PrintStr(StrP,'������:');
          PrintStr(StrP,prDefFormatStr1(l,'�������',0));
          PrintStr(StrP,prDefFormatStr1(l,'�����. �������',0));
          PrintStr(StrP,' ');
          PrintStr(StrP,'����. �����:');
          PrintStr(StrP,prDefFormatStr1(l,'�������',rSumBNal));
          PrintStr(StrP,prDefFormatStr1(l,'�����. �������',rSumBRet));
          PrintStr(StrP,' ');
          PrintStr(StrP,'����. �����2:');
          PrintStr(StrP,prDefFormatStr1(l,'�������',0));
          PrintStr(StrP,prDefFormatStr1(l,'�����. �������',0));
          PrintStr(StrP,' ');
          PrintStr(StrP,'����. �����3:');
          PrintStr(StrP,prDefFormatStr1(l,'�������',0));
          PrintStr(StrP,prDefFormatStr1(l,'�����. �������',0));
          PrintStr(StrP,' ');
          PrintStr(StrP,'����. �����4:');
          PrintStr(StrP,prDefFormatStr1(l,'�������',0));
          PrintStr(StrP,prDefFormatStr1(l,'�����. �������',0));
          PrintStr(StrP,' ');
          PrintStr(StrP,'================= ����� ================');
          PrintStr(StrP,prDefFormatStr1(l,'�������',rSumNal+rSumBNal));
          PrintStr(StrP,prDefFormatStr1(l,'� �.�. ����:',0));
          PrintStr(StrP,prDefFormatStr1(l,' ������',rSumDisc));
          PrintStr(StrP,prDefFormatStr1(l,'�����. �������',0));
          PrintStr(StrP,prDefFormatStr1(l,'� �.�. ����:',0));
          PrintStr(StrP,prDefFormatStr1(l,'�������',rSumRet+rSumBRet));
          PrintStr(StrP,prDefFormatStr1(l,'� �.�. ����:',0));
          PrintStr(StrP,prDefFormatStr1(l,'������������:',0));
          PrintStr(StrP,prDefFormatStr1(l,'����������:',0));
          PrintStr(StrP,' ');
          PrintStr(StrP,prDefFormatStr2(l,'��������� ����������:',iCountDoc+4));
          PrintStr(StrP,prDefFormatStr2(l,'� �.�. �����:',iCountDoc));
          PrintStr(StrP,prDefFormatStr2(l,'� �.�. �����. �����:',0));
          PrintStr(StrP,prDefFormatStr2(l,'� �.�. ������. ���. ���. �����:',0));
          PrintStr(StrP,' ');
          PrintStr(StrP,prDefFormatStr2(l,'�������:',iCountDoc));
          PrintStr(StrP,prDefFormatStr2(l,'�����. �������:',0));
          PrintStr(StrP,prDefFormatStr2(l,'�������:',0));
          PrintStr(StrP,prDefFormatStr2(l,'������������:',0));
          PrintStr(StrP,prDefFormatStr2(l,'����������:',0));

          prWrBuf(StrP);
          prCutDoc(StrP,0);
        finally
          prDevClose(StrP,0);
        end;
      end; //if Pos('COM',CommonSet.SpecBoxX)>0 then

      if Pos('DB',CommonSet.SpecBoxX)>0 then
      begin
        try
          l:=40;
          StrP:=CommonSet.SpecBoxX;

          with dmC1 do
          begin
            quPrint.Active:=False;
            quPrint.Active:=True;
            iQ:=GetId('PQH'); //����������� ������� �� 1-�

            PrintDBStr(iQ,999,StrP,SpecVal.SPH1,10,0);
            PrintDBStr(iQ,999,StrP,SpecVal.SPH2,10,0);
            PrintDBStr(iQ,999,StrP,SpecVal.SPH3,10,0);
            PrintDBStr(iQ,999,StrP,SpecVal.SPH4,10,0);
            PrintDBStr(iQ,999,StrP,SpecVal.SPH5,10,0);
            StrWk:=IntToStr(iMax+1); //����� ���������
            while Length(StrWk)<5 do StrWk:='0'+StrWk;
//          PrintDBStr(iQ,999,StrP,'�������� N: '+CommonSet.CashSer+'         N ���: '+StrWk);   ������� ������������ - ��������� �� ����
            PrintDBStr(iQ,999,StrP,FormatDateTime('dd-mm-yyyy                         hh:nn',now),10,0);
            PrintDBStr(iQ,999,StrP,' ',10,0);
            StrWk:='            Z-����� N '+IntToStr(iNumSm);
            PrintDBStr(iQ,999,StrP,StrWk,10,0);
            PrintDBStr(iQ,999,StrP,' ',10,0);
            PrintDBStr(iQ,999,StrP,'��������:',10,0);
            PrintDBStr(iQ,999,StrP,prDefFormatStr1(l,'�������',rSumNal),10,0);
            PrintDBStr(iQ,999,StrP,prDefFormatStr1(l,'�����. �������',0),10,0);
            PrintDBStr(iQ,999,StrP,prDefFormatStr1(l,'�������',rSumRet),10,0);
            PrintDBStr(iQ,999,StrP,' ',10,0);
            PrintDBStr(iQ,999,StrP,'������:',10,0);
            PrintDBStr(iQ,999,StrP,prDefFormatStr1(l,'�������',0),10,0);
            PrintDBStr(iQ,999,StrP,prDefFormatStr1(l,'�����. �������',0),10,0);
            PrintDBStr(iQ,999,StrP,' ',10,0);
            PrintDBStr(iQ,999,StrP,'����. �����:',10,0);
            PrintDBStr(iQ,999,StrP,prDefFormatStr1(l,'�������',rSumBNal),10,0);
            PrintDBStr(iQ,999,StrP,prDefFormatStr1(l,'�����. �������',rSumBRet),10,0);
            PrintDBStr(iQ,999,StrP,' ',10,0);
            PrintDBStr(iQ,999,StrP,'����. �����2:',10,0);
            PrintDBStr(iQ,999,StrP,prDefFormatStr1(l,'�������',0),10,0);
            PrintDBStr(iQ,999,StrP,prDefFormatStr1(l,'�����. �������',0),10,0);
            PrintDBStr(iQ,999,StrP,' ',10,0);
            PrintDBStr(iQ,999,StrP,'����. �����3:',10,0);
            PrintDBStr(iQ,999,StrP,prDefFormatStr1(l,'�������',0),10,0);
            PrintDBStr(iQ,999,StrP,prDefFormatStr1(l,'�����. �������',0),10,0);
            PrintDBStr(iQ,999,StrP,' ',10,0);
            PrintDBStr(iQ,999,StrP,'����. �����4:',10,0);
            PrintDBStr(iQ,999,StrP,prDefFormatStr1(l,'�������',0),10,0);
            PrintDBStr(iQ,999,StrP,prDefFormatStr1(l,'�����. �������',0),10,0);
            PrintDBStr(iQ,999,StrP,' ',10,0);
            PrintDBStr(iQ,999,StrP,'================= ����� ================',10,0);
            PrintDBStr(iQ,999,StrP,prDefFormatStr1(l,'�������',rSumNal+rSumBNal),10,0);
            PrintDBStr(iQ,999,StrP,prDefFormatStr1(l,'� �.�. ����:',0),10,0);
            PrintDBStr(iQ,999,StrP,prDefFormatStr1(l,' ������',rSumDisc),10,0);
            PrintDBStr(iQ,999,StrP,prDefFormatStr1(l,'�����. �������',0),10,0);
            PrintDBStr(iQ,999,StrP,prDefFormatStr1(l,'� �.�. ����:',0),10,0);
            PrintDBStr(iQ,999,StrP,prDefFormatStr1(l,'�������',rSumRet+rSumBRet),10,0);
            PrintDBStr(iQ,999,StrP,prDefFormatStr1(l,'� �.�. ����:',0),10,0);
            PrintDBStr(iQ,999,StrP,prDefFormatStr1(l,'������������:',0),10,0);
            PrintDBStr(iQ,999,StrP,prDefFormatStr1(l,'����������:',0),10,0);
            PrintDBStr(iQ,999,StrP,' ',10,0);
            PrintDBStr(iQ,999,StrP,prDefFormatStr2(l,'��������� ����������:',iCountDoc+4),10,0);
            PrintDBStr(iQ,999,StrP,prDefFormatStr2(l,'� �.�. �����:',iCountDoc),10,0);
            PrintDBStr(iQ,999,StrP,prDefFormatStr2(l,'� �.�. �����. �����:',0),10,0);
            PrintDBStr(iQ,999,StrP,prDefFormatStr2(l,'� �.�. ������. ���. ���. �����:',0),10,0);
            PrintDBStr(iQ,999,StrP,' ',10,0);
            PrintDBStr(iQ,999,StrP,prDefFormatStr2(l,'�������:',iCountDoc),10,0);
            PrintDBStr(iQ,999,StrP,prDefFormatStr2(l,'�����. �������:',0),10,0);
            PrintDBStr(iQ,999,StrP,prDefFormatStr2(l,'�������:',0),10,0);
            PrintDBStr(iQ,999,StrP,prDefFormatStr2(l,'������������:',0),10,0);
            PrintDBStr(iQ,999,StrP,prDefFormatStr2(l,'����������:',0),10,0);

            prAddPrintQH(999,iQ,FormatDateTime('dd.mm hh:nn:sss',now)+', ������� - '+IntToStr(CommonSet.Station)+','+CommonSet.SpecBoxX);
            quPrint.Active:=False;
          end;
        finally
        end;
      end; //if Pos('COM',CommonSet.SpecBoxX)>0 then

      if Pos('fis',CommonSet.SpecBoxX)>0 then
      begin
        TestPrint:=1;
        While PrintQu and (TestPrint<=MaxDelayPrint) do
        begin
//        showmessage('������� �����, ��������� �������.');
          prWriteLog('������������� ������ ����� � �������.');
          delay(1000);
          inc(TestPrint);
        end;
        try
          PrintFCheck:=True; //��� ���� � ������� ������� ��������� ������

          l:=40;
          if CommonSet.SpecBoxX='fisshtrih' then l:=32;

          OpenNFDoc;

          PrintNFStr(SpecVal.SPH1);
          PrintNFStr(SpecVal.SPH2);
          PrintNFStr(SpecVal.SPH3);
          PrintNFStr(SpecVal.SPH4);
          PrintNFStr(SpecVal.SPH5);
          StrWk:=IntToStr(iMax+1); //����� ���������
          while Length(StrWk)<5 do StrWk:='0'+StrWk;
//          PrintNFStr('�������� N: '+CommonSet.CashSer+' N ���: '+StrWk);  ������� ������������ - ��������� �� ����
          PrintNFStr(FormatDateTime('dd-mm-yyyy                 hh:nn',now));
          PrintNFStr(' ');
          StrWk:='            Z-����� N '+IntToStr(iNumSm);
          PrintNFStr(StrWk);
          PrintNFStr(' ');
          PrintNFStr('��������:');
          PrintNFStr(prDefFormatStr1(l,'�������',rSumNal));
          PrintNFStr(prDefFormatStr1(l,'�����. �������',0));
          PrintNFStr(prDefFormatStr1(l,'�������',rSumRet));
          PrintNFStr(' ');
          PrintNFStr('������:');
          PrintNFStr(prDefFormatStr1(l,'�������',0));
          PrintNFStr(prDefFormatStr1(l,'�����. �������',0));
          PrintNFStr(' ');
          PrintNFStr('����. �����:');
          PrintNFStr(prDefFormatStr1(l,'�������',rSumBNal));
          PrintNFStr(prDefFormatStr1(l,'�����. �������',rSumBRet));
          PrintNFStr(' ');
          PrintNFStr('����. �����2:');
          PrintNFStr(prDefFormatStr1(l,'�������',0));
          PrintNFStr(prDefFormatStr1(l,'�����. �������',0));
          PrintNFStr(' ');
          PrintNFStr('����. �����3:');
          PrintNFStr(prDefFormatStr1(l,'�������',0));
          PrintNFStr(prDefFormatStr1(l,'�����. �������',0));
          PrintNFStr(' ');
          PrintNFStr('����. �����4:');
          PrintNFStr(prDefFormatStr1(l,'�������',0));
          PrintNFStr(prDefFormatStr1(l,'�����. �������',0));
          PrintNFStr(' ');
          PrintNFStr('================= ����� ================');
          PrintNFStr(prDefFormatStr1(l,'�������',rSumNal+rSumBNal));
          PrintNFStr(prDefFormatStr1(l,'� �.�. ����:',0));
          PrintNFStr(prDefFormatStr1(l,' ������',rSumDisc));
          PrintNFStr(prDefFormatStr1(l,'�����. �������',0));
          PrintNFStr(prDefFormatStr1(l,'� �.�. ����:',0));
          PrintNFStr(prDefFormatStr1(l,'�������',rSumRet+rSumBRet));
          PrintNFStr(prDefFormatStr1(l,'� �.�. ����:',0));
          PrintNFStr(prDefFormatStr1(l,'������������:',0));
          PrintNFStr(prDefFormatStr1(l,'����������:',0));
          PrintNFStr(' ');
          PrintNFStr(prDefFormatStr2(l,'��������� ����������:',iCountDoc+4));
          PrintNFStr(prDefFormatStr2(l,'� �.�. �����:',iCountDoc));
          PrintNFStr(prDefFormatStr2(l,'� �.�. �����. �����:',0));
          PrintNFStr(prDefFormatStr2(l,'� �.�. ������. ���. ���. �����:',0));
          PrintNFStr(' ');
          PrintNFStr(prDefFormatStr2(l,'�������:',iCountDoc));
          PrintNFStr(prDefFormatStr2(l,'�����. �������:',0));
          PrintNFStr(prDefFormatStr2(l,'�������:',0));
          PrintNFStr(prDefFormatStr2(l,'������������:',0));
          PrintNFStr(prDefFormatStr2(l,'����������:',0));

          PrintNFStr(' '); // PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');

          CloseNFDoc;
          if CommonSet.TypeFis<>'sp101' then CutDoc;

//          CutDoc;
        finally
          PrintFCheck:=False; //��� ���� � ������� ������� ��������� ������
        end;
      end; //if Pos('fis',CommonSet.SpecBoxX)>0 then
    end;

  end;

  If (CommonSet.Fis=0)or(CommonSet.SpecChar=1) then
  begin
    inc(CommonSet.CashZ);
    inc(Nums.ZNum);
    CommonSet.CashChNum:=0;
    Nums.iCheckNum:=0;
    CommonSet.PreCheckNum:=1;
    if CommonSet.IncNumTab>0 then CommonSet.IncNumTab:=1;//��������� ������� ������� ������
    WriteCheckNum;
    Label4.Caption:='����� �'+IntToStr(CommonSet.CashZ)+'  ��� � '+IntToStr(CommonSet.CashChNum);
    prWriteChNumToBase(CommonSet.iOrg);
  end;
end;

procedure TfmMainCashRn.cxButton4Click(Sender: TObject);
begin
  fmPCardsRn.ShowModal;
end;

procedure TfmMainCashRn.acCashRepExecute(Sender: TObject);
begin
  if Panel4.Visible then
  Panel4.Visible:=False
  else
  begin
    if CanDo('prXRep') then cxButton2.Visible:=True
    else cxButton2.Visible:=False;

    if CanDo('prZRep') then cxButton3.Visible:=True
    else cxButton3.Visible:=False;

    if CanDo('prPrintCheck') then
    begin
      cxButton5.Visible:=True;
      cxButton7.Visible:=True;
      cxButton9.Visible:=True;
      cxButton13.Visible:=True;
    end
    else
    begin
      cxButton5.Visible:=False;
      cxButton7.Visible:=False;
      cxButton9.Visible:=False;
      cxButton13.Visible:=False;
    end;

    Panel4.Visible:=True;

    if cxButton2.Visible then cxButton2.SetFocus;
  end;
end;

procedure TfmMainCashRn.cxButton1Click(Sender: TObject);
begin
  acCashRep.Execute;
end;

procedure TfmMainCashRn.acExitExecute(Sender: TObject);
begin
  if bPrintCheck then
  begin
    bExitPers:=True;
    prWriteLog('������� ������ ��� ������ ����.');
    exit;
  end;
  bPrintCheck:=False;
  DestrViewPers;
  fmPre.ShowModal;
end;

procedure TfmMainCashRn.cxButton5Click(Sender: TObject);
begin
  if not CanDo('prRetCheck') then exit;

  if Operation =0 then Operation:=1
  else Operation := 0;

  Case Operation of
  0: begin
       Label5.Caption:='�������� - ������� ��������.';
     end;
  1: begin
       Label5.Caption:='�������� - �������.';
     end;
  end;
end;

procedure TfmMainCashRn.RxClock1Click(Sender: TObject);
begin
  if CanDo('prExit') then
    if MessageDlg('�� ������������� ������ �������� ���������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      FormLog('Close',IntToStr(Tab.Id_Personal));
      Close;
    end;
end;

procedure TfmMainCashRn.acOpenExecute(Sender: TObject);
Var StrPers, StrStatus:String;
    DSum,rSum:Real;
begin
  //���������� ������������ �����
  if bOpenTable then exit;
  with dmC do
  begin

    bOpenTable:=True; // ��������� �����

    if not CanDo('prView') then begin bOpenTable:=False; exit; end;

    if quTabs.RecordCount=0 then  begin bOpenTable:=False; exit; end;

    if not FindTab(quTabsID.AsInteger) then
    begin
      TabView.BeginUpdate;
      quTabs.FullRefresh;
      TabView.EndUpdate;
      bOpenTable:=False;
      exit;
    end;

    quTabs.Refresh;  //���� ��� �� �������� ������ -  �������� �� ������ ������ ������ ������� ������

    if quTabsISTATUS.AsInteger=2 then begin bOpenTable:=False; showmessage('���� �������������. �������� � �������������� ���������.');  exit; end;

    TiRefresh.Enabled:=False; //���������� �������������� ����������

    Tab.Id_Personal:=quTabsID_PERSONAL.AsInteger;

    quFindPers.Active:=False;
    quFindPers.ParamByName('IDP').AsInteger:=Tab.Id_Personal;
    quFindPers.Active:=True;
    Tab.Name:=quFindPersNAME.AsString;
    quFindPers.Active:=False;

    Tab.OpenTime:=quTabsBEGTIME.AsDateTime;
    Tab.NumTable:=quTabsNUMTABLE.AsString;
    Tab.Quests:=quTabsQUESTS.AsInteger;
    Tab.iStatus:=quTabsISTATUS.AsInteger;
    Tab.DBar:=quTabsDISCONT.AsString;
    Tab.DBar1:=quTabsDISCONT.AsString;
    Tab.Id:=quTabsID.AsInteger;
    Tab.Summa:=quTabsTABSUM.AsFloat;
    Tab.iNumZ:=quTabsNUMZ.AsInteger;

    //�������� ������� ����� ������ 0 - ���   1-����   2 - ������

    quTabs.Edit;
    quTabsISTATUS.AsInteger:=2;
    quTabs.Post;
    quTabs.Refresh;

    //�������� ���������� ������

    try
      fmSpec.ViewSpec.BeginUpdate;
      fmSpec.ViewMod.BeginUpdate;
      //������� �� ������ ������
      prWriteLog('--OpenTab;'+IntToStr(Tab.Id)+';'+Tab.NumTable+';'+Person.Name+';'+IntToStr(Tab.Id_Personal)+';');

      prOpenTab.ParamByName('ID_TAB').AsInteger:=Tab.Id;
      prOpenTab.ParamByName('ID_STATION').AsInteger:=CommonSet.Station;
      prOpenTab.ExecProc;
      DSUM:=RoundEx(prOpenTab.ParamByName('DSUMALL').AsDouble*100)/100;
      Check.Max:=prOpenTab.ParamByName('IDMAX').AsInteger;

      prWriteLog('--SelSpec;'+IntToStr(Tab.Id)+';'+Tab.NumTable+';'+Person.Name+';'+IntToStr(Tab.Id_Personal)+';'+IntToStr(Check.Max)+';');

      rSum:=0;
      quCurSpec.Active:=False;
      quCurSpec.ParamByName('IDT').AsInteger:=Tab.Id;
      quCurSpec.ParamByName('STATION').AsInteger:=CommonSet.Station;
      quCurSpec.Active:=True;
      quCurSpec.First;
      while not quCurSpec.Eof do
      begin
        rSum:=rSum+quCurSpecSUMMA.AsFloat;
        prWriteLog('-----pos;'+IntToStr(quCurSpecID_TAB.AsInteger)+';'+IntToStr(quCurSpecID.AsInteger)+';'+IntTostr(quCurSpecID_PERSONAL.AsInteger)+';'+quCurSpecNAME.AsString+';'+IntToStr(quCurSpecSIFR.AsInteger)+';'+FloatToStr(quCurSpecSUMMA.AsFloat)+';');
        quCurSpec.Next;
      end;
      prWriteLog('--EndSpec;'+IntToStr(Tab.Id)+';'+Tab.NumTable+';'+Person.Name+';'+IntToStr(Tab.Id_Personal)+';'+IntToStr(Check.Max)+';');

      quCurMod.Active:=False;
      quCurMod.ParamByName('IDT').AsInteger:=quCurSpecID_TAB.AsInteger;
      quCurMod.ParamByName('IDP').AsInteger:=quCurSpecID.AsInteger;
      quCurMod.ParamByName('STATION').AsInteger:=CommonSet.Station;
      quCurMod.Active:=True;

      quCurSpec.First;
      quCurMod.First;

      //����� �������� �������� �� ������������ �������� ����� �� �����
      if RoundEx(rSum*100)<>RoundEx(Tab.Summa*100) then
      begin
        //����� �� ��������
        prWriteLog('--SumSpec; New-'+IntToStr(RoundEx(rSum*100))+';Old-'+IntToStr(RoundEx(Tab.Summa*100))+';');
        if MessageDlg('���������� ����������� ���� ��������� � ������������ ������. �� ��������� �����?',
        mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
  //        showmessage('������ �������� ������.');
          bOpenTable:=False; // ���� ������� �����

          TiRefresh.Enabled:=True; //���������� �������������� ����������

          TabView.BeginUpdate;
          if quTabs.Locate('ID',Tab.Id,[]) then
          begin
            quTabs.Edit;
            quTabsISTATUS.AsInteger:=Tab.iStatus;
            quTabs.Post;
            quTabs.Refresh;
          end;
          TabView.EndUpdate;

          exit;
        end;
      end;

      with fmSpec do
      begin
        StrPers:=Tab.Name;
        StrStatus:='';
        Label8.Caption:='����� � '+INtToStr(Tab.iNumZ);

        SetStatus(StrStatus);

        Label7.Caption:=StrPers;
        BEdit1.Text:=Tab.NumTable;
        if Tab.Quests>=0 then SEdit1.EditValue:=Tab.Quests else SEdit1.EditValue:=0;
        Label10.Caption:=FormatdateTime('dd.mm.yyyy hh:nn',Tab.OpenTime);
        Label11.Caption:=StrStatus;
        Label17.Caption:='';

        If FindDiscount(Tab.DBar) then
        begin
          // ��������� ������
          Str(Tab.DPercent:5:2,StrWk);
          if Tab.DType=3 then Label17.Caption:='�������� ������ - '+ Tab.DName+' ('+StrWk+'%)'
          else Label17.Caption:='������������ ������ - '+ Tab.DName+' ('+StrWk+'%)';
        end;

        if DSum<>0 then
        begin
          ViewSpecDPROC.Visible:=True;
          ViewSpecDSum.Visible:=True;
        end
        else
        begin
          ViewSpecDPROC.Visible:=False;
          ViewSpecDSum.Visible:=False;
        end;

        dmC1.quSaleT.Locate('ID',dmC.quTabsSALET.AsInteger,[]);
        fmSpec.cxButton17.Caption:=Copy(dmC1.quSaleTNAMECS.AsString,1,15);
        fmSpec.cxButton17.Tag:=dmC1.quSaleTID.AsInteger;
      end;
    finally
      bOpenTable:=False; // ���� ������� �����
      fmSpec.ViewSpec.EndUpdate;
      fmSpec.ViewMod.EndUpdate;
    end;
    fmSpec.ShowModal;
  end;
//  delay(100);
  CreateViewPers(bChangeView); //�������� ��������� ���-���� ����� ��������� ������ �������� �������
end;

procedure TfmMainCashRn.TabViewDblClick(Sender: TObject);
begin
//Button2.SetFocus;
//Button2.OnClick(Self);
  TiRefresh.Enabled:=False;
  TiRefresh.Enabled:=True;
end;

procedure TfmMainCashRn.acPrintPreExecute(Sender: TObject);
  //�������� ����
Var strWk,StrWk1:String;
    rDiscont,rDProc,rSumQ:Real;
    TabCh:TTab;
    iQ,iQuest:Integer;
    b35,bQuest:Boolean;
    StrP:String;
    BonusSum,BonusLim:Real;
    rBalansDay,DLimit:Real;
    CliName,CliType:String;
    nd:Smallint;

  function LenStr(S1,S2:String):String;
  begin
    if b35 then Result:=S2 else Result:=S1;
  end;

begin
  if bPressB5Main then exit;

  prDisButton;
  with dmC do
  begin
    if quTabs.RecordCount=0 then begin prEnButton; exit; end;
    if not FindTab(quTabsID.AsInteger) then begin prEnButton; exit; end;

    TabView.BeginUpdate;
    quTabs.Refresh; //���� ������� ��� �� ������� �� ������ ������� - ������ ���-�� �����������
    TabView.EndUpdate;


    if quTabsISTATUS.AsInteger=2 then begin showmessage('���� �������������. �������� � �������������� ���������.');  prEnButton; exit; end;

    if (quTabsISTATUS.AsInteger=1) and (CommonSet.PreCheckCount=1) then
    begin
      showmessage('��������� ������ ����� ���������.');
      prEnButton;
      exit;
    end;

    inc(CommonSet.PreCheckNum);
    WriteCheckNum;

    CommonSet.PrePrintPort:=prGetPrePrintPort;

    Tab.Id_Personal:=quTabsID_PERSONAL.AsInteger;
    TabCh.Id_Personal:=quTabsID_PERSONAL.AsInteger;
    quFindPers.Active:=False;
    quFindPers.ParamByName('IDP').AsInteger:=Tab.Id_Personal;
    quFindPers.Active:=True;
    Tab.Name:=quFindPersNAME.AsString;
    TabCh.Name:=quFindPersNAME.AsString;

    quFindPers.Active:=False;

    TabCh.OpenTime:=quTabsBEGTIME.AsDateTime;
    TabCh.NumTable:=quTabsNUMTABLE.AsString;
    TabCh.Quests:=quTabsQUESTS.AsInteger;
    TabCh.iStatus:=quTabsISTATUS.AsInteger;
    TabCh.DBar:=quTabsDISCONT.AsString;
    TabCh.Id:=quTabsID.AsInteger;
    TabCh.Summa:=quTabsTABSUM.AsFloat;
    TabCh.iNumZ:=quTabsNUMZ.AsInteger;

    Tab.OpenTime:=quTabsBEGTIME.AsDateTime;
    Tab.NumTable:=quTabsNUMTABLE.AsString;
    Tab.Quests:=quTabsQUESTS.AsInteger;
    Tab.iStatus:=quTabsISTATUS.AsInteger;
    Tab.DBar:=quTabsDISCONT.AsString;
    Tab.DBar1:=quTabsDISCONT.AsString;
    Tab.Id:=quTabsID.AsInteger;
    Tab.Summa:=quTabsTABSUM.AsFloat;
    Tab.iNumZ:=quTabsNUMZ.AsInteger;

    prWriteLog('--PrePrintM;'+IntToStr(Tab.Id)+';'+Tab.NumTable+';'+Person.Name+';'+IntToStr(Tab.Id_Personal)+';'+FloatToStr(Tab.Summa)+';'+INtToStr(Tab.iNumZ)+';'+Tab.DBar);

    FindDiscount(TabCh.DBar); //��� ������������ Tab.DBar, Tab.DPercent, Tab.DName,

    quCheck.Active:=False;
    quCheck.ParamByName('IDTAB').Value:=TabCh.Id;
    quCheck.Active:=True;

    bQuest:=False; iQuest:=1; //����� ������ �������� �� ����� ������
    quCheck.First;
    if quCheck.RecordCount>0 then iQuest:=quCheckQUEST.AsInteger;
    while not quCheck.Eof do
    begin
      if iQuest<>quCheckQUEST.AsInteger then
      begin
        bQuest:=True;
        Break;
      end;
      quCheck.Next;
    end;


    taModif.Active:=True;

    bPrintCheck:=False;
    bPrintCheck:=True;

    if (CommonSet.PrePrintPort>'')and(CommonSet.PrePrintPort<>'0')and(CommonSet.PrePrintPort<>'G')and(Pos('DB',CommonSet.PrePrintPort)=0)and(Pos('fis',CommonSet.PrePrintPort)=0) then
    begin //���������� ���� COM1 ��� LPT ��������
      try
        prDevOpen(CommonSet.PrePrintPort,0);
        StrP:=CommonSet.PrePrintPort;

        if CommonSet.PrePrintPort[1]='Z' then
        begin
          b35:=True;
          if not fTestPaper then fmWarnPaper.ShowModal;
        end
        else b35:=False;

        BufPr.iC:=0;

        prSetFont(StrP,13,0); PrintStr(StrP,' '+CommonSet.DepartName);

        if CommonSet.PreLine>'' then
        begin
          if fDifStr(1,CommonSet.PreLine)>'' then PrintStr(StrP,fDifStr(1,CommonSet.PreLine));
          if fDifStr(2,CommonSet.PreLine)>'' then PrintStr(StrP,fDifStr(2,CommonSet.PreLine));
        end;

        PrintStr(StrP,LenStr('                                          ','                                   '));

        prSetFont(StrP,14,0);PrintStr(StrP,'   ���� �'+IntToStr(TabCh.iNumZ));
        prSetFont(StrP,13,0);PrintStr(StrP,LenStr('                                          ','                                   '));
        PrintStr(StrP,'��������: '+TabCh.Name);
        PrintStr(StrP,'����: '+TabCh.NumTable);
        if TabCh.DBar>'' then
        begin
          Str(Tab.DPercent:5:2,StrWk);
          PrintStr(StrP,'�����: '+Tab.DName+' ('+StrWk+'%)');
        end;
        prSetFont(StrP,13,0);
        PrintStr(StrP,'������: '+IntToStr(TabCh.Quests));
        PrintStr(StrP,'������: '+FormatDateTime('dd.mm.yyyy hh:nn:ss',TabCh.OpenTime));

        prSetFont(StrP,13,0); StrWk:=LenStr('                                          ','                                   '); PrintStr(StrP,StrWk);

        prSetFont(StrP,15,0); StrWk:=LenStr(' ��������      ���-��   ����   �����',' ��������    ���-��   ����   �����'); PrintStr(StrP,StrWk);
        prSetFont(StrP,13,0); StrWk:=LenStr('                                          ','                                   '); PrintStr(StrP,StrWk);

        rDiscont:=0;


        rSumQ:=0;
        quCheck.First;
        if quCheck.RecordCount>0 then iQuest:=quCheckQUEST.AsInteger;
        while not quCheck.Eof do
        begin      //��������� �������
          if iQuest<>quCheckQUEST.AsInteger then   //�������� �����
          begin
            prSetFont(StrP,15,0); StrWk:=''; PrintStr(StrP,StrWk);
            prSetFont(StrP,13,0); StrWk:='  ����� �� ����� '+its(iQuest)+'   '+vts(rSumQ); PrintStr(StrP,StrWk);
            prSetFont(StrP,15,0); StrWk:=''; PrintStr(StrP,StrWk);

            iQuest:=quCheckQUEST.AsInteger;
            rSumQ:=0;
          end;
          rSumQ:=rSumQ+quCheckSUMMA.AsFloat;

          PosCh.Name:=quCheckNAME.AsString;
          PosCh.Code:=quCheckCODE.AsString;
          PosCh.AddName:='';

          rDiscont:=rDiscont+quCheckDISCOUNTSUM.AsFloat;

          if not b35 then
          begin
            StrWk:= Copy(PosCh.Name,1,19);
            while Length(StrWk)<19 do StrWk:=StrWk+' ';
          end else
          begin
            StrWk:= Copy(PosCh.Name,1,15);
            while Length(StrWk)<15 do StrWk:=StrWk+' ';
          end;

          if abs(frac(quCheckQUANTITY.AsFloat*100))>0 then nd:=3  //���� ������� ����� �� �������
          else
            if abs(frac(quCheckQUANTITY.AsFloat*10))>0 then nd:=2  //���� ������� ����� �������� �� �������
            else nd:=1;

          Str(quCheckQUANTITY.AsFloat:5:nd,StrWk1);
          StrWk:=StrWk+' '+StrWk1;
          if not b35 then Str(quCheckPRICE.AsFloat:7:2,StrWk1)
          else Str(quCheckPRICE.AsFloat:4:0,StrWk1);
          StrWk:=StrWk+' '+StrWk1;
//          Str(quCheckSUMMA.AsFloat:8:2,StrWk1);
          Str((quCheckPRICE.AsFloat*quCheckQUANTITY.AsFloat):8:2,StrWk1);
          StrWk:=StrWk+StrWk1;
          prSetFont(StrP,15,0);
          PrintStr(StrP,StrWk);

          quCheck.Next;
          if not quCheck.Eof then
          begin
            while quCheckITYPE.AsInteger=1 do
            begin  //������������
              if quCheck.Eof then break;
              if quCheckSifr.AsInteger>0 then
                if taModif.Locate('SIFR',quCheckSifr.AsInteger,[]) then
                begin
                  PrintStr(StrP,'   '+Copy(taModifNAME.AsString,1,29));
                end;
              quCheck.Next;
            end;
          end;
        end;
        if bQuest then
        begin
          prSetFont(StrP,15,0); StrWk:=''; PrintStr(StrP,StrWk);
          prSetFont(StrP,13,0); StrWk:='  ����� �� ����� '+its(iQuest)+'   '+vts(rSumQ); PrintStr(StrP,StrWk);
          prSetFont(StrP,15,0); StrWk:=''; PrintStr(StrP,StrWk);
        end;

        prSetFont(StrP,13,0); StrWk:=LenStr('                                          ','                                   '); PrintStr(StrP,StrWk);

        prSetFont(StrP,15,0);
        Str((TabCh.Summa+rDiscont):8:2,StrWk1);
        StrWk:=LenStr(' �����                      '+StrWk1+' ���',' �����                 '+StrWk1+' ���');  PrintStr(StrP,StrWk);

        if rDiscont>0.02 then
        begin
//          prSetFont(StrP,15);
//          PrintStr(StrP,'');
          prSetFont(StrP,15,0);
          rDProc:=RoundEx((rDiscont/(TabCh.Summa+rDiscont)*100)*100)/100;
          Str(rDProc:5:2,StrWk1);
          Str((rDiscont*(-1)):8:2,StrWk);

          Str(Tab.DPercent:4:1,StrWk1);
//          StrWk:=' � �.�. ������ - '+StrWk1+'%  '+StrWk+'�.';
          StrWk:=LenStr(' ������    '+StrWk1+'%            '+StrWk+' ���',' ������    '+StrWk1+'%       '+StrWk+' ���'); PrintStr(StrP,StrWk);
        end;

        if Tab.DType=3 then
        begin
          BonusSum:=rv(CalcBonus(Tab.Id,Tab.DPercent));
          prFindPCardBalans(Tab.DBar,BonusLim,rBalansDay,DLimit,CliName,CliType);

          StrWk:=CliName; PrintStr(StrP,StrWk);
          StrWk:='����������� ������ - '+FlToS(rv(BonusLim))+' ���'; PrintStr(StrP,StrWk);

          Str(Tab.DPercent:4:1,StrWk);
          StrWk:='�������� ������ - ('+StrWk+'%)';  PrintStr(StrP,StrWk);
          StrWk:='����� ��������� - '+FlToS(BonusSum)+' ���';  PrintStr(StrP,StrWk);
        end;

        prSetFont(StrP,15,0);
        Str((TabCh.Summa):8:2,StrWk1);
        StrWk:=LenStr(' ����� � ������             '+StrWk1+' ���',' ����� � ������        '+StrWk1+' ���'); PrintStr(StrP,StrWk);
        if CommonSet.LastLine>'' then
        begin
          PrintStr(StrP,'');
          if fDifStr(1,CommonSet.LastLine)>'' then PrintStr(StrP,fDifStr(1,CommonSet.LastLine));
          if fDifStr(2,CommonSet.LastLine)>'' then PrintStr(StrP,fDifStr(2,CommonSet.LastLine));
          PrintStr(StrP,' ');
        end;

        prWrBuf(StrP);
      finally
        prCutDoc(StrP,0);
        if CommonSet.PrePrintPort[1]='Z' then prRelease; //��������� ���.�������
        prDevClose(StrP,0);
      end;
    end;
    if CommonSet.PrePrintPort='G' then
    begin

      quCheck.Filtered:=False;
      quCheck.Filter:='ITYPE=0';
      quCheck.Filtered:=True;

      rDiscont:=0;

      quCheck.First;
      while not quCheck.Eof do
      begin      //��������� �������
        rDiscont:=rDiscont+quCheckDISCOUNTSUM.AsFloat;
        quCheck.Next;
      end;
      quCheck.First;


      frRepMain.LoadFromFile(CurDir + 'PreCheck1.frf');

      frVariables.Variable['Waiter']:=TabCh.Name;
      frVariables.Variable['TabNum']:=TabCh.NumTable;
      frVariables.Variable['OpenTime']:=FormatDateTime('dd.mm.yyyy hh:nn:ss',TabCh.OpenTime);
      frVariables.Variable['Quests']:=IntToStr(TabCh.Quests);
      frVariables.Variable['ZNum']:=IntToStr(TabCh.Id);
      frVariables.Variable['PreNum']:=IntToStr(CommonSet.PreCheckNum-1);

      if rDiscont>0.2 then
      begin
        str(rDiscont:8:2,StrWk);
        frVariables.Variable['Discount']:='� �.�. ������ '+StrWk;
      end else frVariables.Variable['Discount']:='';
      frRepMain.ReportName:='����.';
      frRepMain.PrepareReport;
//      frRepMain.ShowPreparedReport;
      frRepMain.PrintPreparedReportDlg;
    end;
    if pos('DB',CommonSet.PrePrintPort)>0 then
    begin
        //��������� �������
      with dmC1 do
      begin
        quPrint.Active:=False;
        quPrint.Active:=True;
        iQ:=GetId('PQH'); //����������� ������� �� 1-�

        PrintDBStr(iQ,999,CommonSet.PrePrintPort,' '+CommonSet.DepartName,13,0);

        if CommonSet.PreLine>'' then
        begin
          if fDifStr(1,CommonSet.PreLine)>'' then PrintDBStr(iQ,999,CommonSet.PrePrintPort,fDifStr(1,CommonSet.PreLine),13,0);
          if fDifStr(2,CommonSet.PreLine)>'' then PrintDBStr(iQ,999,CommonSet.PrePrintPort,fDifStr(2,CommonSet.PreLine),13,0);
        end;

        PrintDBStr(iQ,999,CommonSet.PrePrintPort,'',13,0);
        PrintDBStr(iQ,999,CommonSet.PrePrintPort,'   ���� �'+IntToStr(TabCh.iNumZ),14,0);
        PrintDBStr(iQ,999,CommonSet.PrePrintPort,'',13,0);
        PrintDBStr(iQ,999,CommonSet.PrePrintPort,'��������: '+TabCh.Name,13,0);
        PrintDBStr(iQ,999,CommonSet.PrePrintPort,'����: '+TabCh.NumTable,13,0);
        if TabCh.DBar>'' then
        begin
          Str(Tab.DPercent:5:2,StrWk);
          PrintDBStr(iQ,999,CommonSet.PrePrintPort,'�����: '+Tab.DName+' ('+StrWk+'%)',13,0);
        end;
        PrintDBStr(iQ,999,CommonSet.PrePrintPort,'������: '+IntToStr(TabCh.Quests),13,0);
        PrintDBStr(iQ,999,CommonSet.PrePrintPort,'������: '+FormatDateTime('dd.mm.yyyy hh:nn:ss',TabCh.OpenTime),13,0);
        PrintDBStr(iQ,999,CommonSet.PrePrintPort,'-',13,0);
        PrintDBStr(iQ,999,CommonSet.PrePrintPort,' ��������      ���-��   ����   �����',15,0);
        PrintDBStr(iQ,999,CommonSet.PrePrintPort,'-',13,0);

        rDiscont:=0;

        rSumQ:=0;
        quCheck.First;
        if quCheck.RecordCount>0 then iQuest:=quCheckQUEST.AsInteger;
        while not quCheck.Eof do
        begin      //��������� �������
          if iQuest<>quCheckQUEST.AsInteger then   //�������� �����
          begin
            PrintDBStr(iQ,999,CommonSet.PrePrintPort,' ',15,0);
            PrintDBStr(iQ,999,CommonSet.PrePrintPort,'  ����� �� ����� '+its(iQuest)+'   '+vts(rSumQ),13,0);
            PrintDBStr(iQ,999,CommonSet.PrePrintPort,' ',15,0);

            iQuest:=quCheckQUEST.AsInteger;
            rSumQ:=0;
          end;
          rSumQ:=rSumQ+quCheckSUMMA.AsFloat;


          PosCh.Name:=quCheckNAME.AsString;
          PosCh.Code:=quCheckCODE.AsString;
          PosCh.AddName:='';

          rDiscont:=rDiscont+quCheckDISCOUNTSUM.AsFloat;

          if pos('DBfis2',CommonSet.PrePrintPort)>0 then
          begin  //�� ���������� �����   - ��� �� 2-� ������ �� 36 �������� - ���� �������� �������
            StrWk:= Copy(PosCh.Name,1,36);
            while Length(StrWk)<36 do StrWk:=StrWk+' ';
            PrintDBStr(iQ,999,CommonSet.PrePrintPort,StrWk,15,0);

            if abs(frac(quCheckQUANTITY.AsFloat*100))>0 then nd:=3  //���� ������� ����� �� �������
            else
              if abs(frac(quCheckQUANTITY.AsFloat*10))>0 then nd:=2  //���� ������� ����� �������� �� �������
              else nd:=1;

            Str(quCheckQUANTITY.AsFloat:5:nd,StrWk1);
            StrWk:=' '+StrWk1;
            Str(quCheckPRICE.AsFloat:7:2,StrWk1);
            StrWk:=StrWk+' '+StrWk1;
            Str(quCheckSUMMA.AsFloat:8:2,StrWk1);
            StrWk:=StrWk+' '+StrWk1+'�';
            while Length(StrWk)<36 do StrWk:=' '+StrWk;
            PrintDBStr(iQ,999,CommonSet.PrePrintPort,StrWk,15,0);

          end else //��� �� ����������� ������ 40�������
          begin
            StrWk:= Copy(PosCh.Name,1,19);
            while Length(StrWk)<19 do StrWk:=StrWk+' ';


            if abs(frac(quCheckQUANTITY.AsFloat*100))>0 then nd:=3  //���� ������� ����� �� �������
            else
              if abs(frac(quCheckQUANTITY.AsFloat*10))>0 then nd:=2  //���� ������� ����� �������� �� �������
              else nd:=1;

            Str(quCheckQUANTITY.AsFloat:5:nd,StrWk1);

            StrWk:=StrWk+' '+StrWk1;
            Str(quCheckPRICE.AsFloat:7:2,StrWk1);
            StrWk:=StrWk+' '+StrWk1;
            Str((quCheckPRICE.AsFloat*quCheckQUANTITY.AsFloat):8:2,StrWk1);
            StrWk:=StrWk+StrWk1+'�';

            PrintDBStr(iQ,999,CommonSet.PrePrintPort,StrWk,15,0);
          end;
          quCheck.Next;
          if not quCheck.Eof then
          begin
            while quCheckITYPE.AsInteger=1 do
            begin  //������������
              if quCheck.Eof then break;
              if quCheckSifr.AsInteger>0 then
                if taModif.Locate('SIFR',quCheckSifr.AsInteger,[]) then
                begin
                  PrintDBStr(iQ,999,CommonSet.PrePrintPort,'   '+Copy(taModifNAME.AsString,1,29),15,0);
                end;
              quCheck.Next;
            end;
          end;
        end;
        if bQuest then
        begin
          PrintDBStr(iQ,999,CommonSet.PrePrintPort,' ',15,0);
          PrintDBStr(iQ,999,CommonSet.PrePrintPort,'  ����� �� ����� '+its(iQuest)+'   '+vts(rSumQ),13,0);
          PrintDBStr(iQ,999,CommonSet.PrePrintPort,' ',15,0);
        end;

        PrintDBStr(iQ,999,CommonSet.PrePrintPort,'-',13,0);

        Str((TabCh.Summa+rDiscont):8:2,StrWk1);
        StrWk:=' �����                      '+StrWk1+' ���';
        PrintDBStr(iQ,999,CommonSet.PrePrintPort,StrWk,15,0);

        if rDiscont>0.02 then
        begin
//          rDProc:=RoundEx((rDiscont/(TabCh.Summa+rDiscont)*100)*100)/100;
//          Str(rDProc:5:2,StrWk1);
          Str(Tab.DPercent:4:1,StrWk1);
          Str((rDiscont*(-1)):8:2,StrWk);
//          StrWk:=' ������                     '+StrWk+' ���';
          StrWk:=LenStr(' ������    '+StrWk1+'%            '+StrWk+' ���',' ������    '+StrWk1+'%       '+StrWk+' ���');

          PrintDBStr(iQ,999,CommonSet.PrePrintPort,StrWk,15,0);
        end;

        if Tab.DType=3 then
        begin
          BonusSum:=rv(CalcBonus(Tab.Id,Tab.DPercent));
          prFindPCardBalans(Tab.DBar,BonusLim,rBalansDay,DLimit,CliName,CliType);

          StrWk:=CliName; PrintDBStr(iQ,999,CommonSet.PrePrintPort,StrWk,15,0);
          StrWk:='����������� ������ - '+FlToS(rv(BonusLim))+' ���'; PrintDBStr(iQ,999,CommonSet.PrePrintPort,StrWk,15,0);;

          Str(Tab.DPercent:4:1,StrWk);
          StrWk:='�������� ������ - ('+StrWk+'%)';  PrintDBStr(iQ,999,CommonSet.PrePrintPort,StrWk,15,0);
          StrWk:='����� ��������� - '+FlToS(BonusSum)+' ���' ;  PrintDBStr(iQ,999,CommonSet.PrePrintPort,StrWk,15,0);
        end;

        Str((TabCh.Summa):8:2,StrWk1);
        StrWk:=' ����� � ������             '+StrWk1+' ���';
        PrintDBStr(iQ,999,CommonSet.PrePrintPort,StrWk,15,0);

        if CommonSet.LastLine>'' then
        begin
          PrintDBStr(iQ,999,CommonSet.PrePrintPort,'',15,0);

          if fDifStr(1,CommonSet.LastLine)>'' then PrintDBStr(iQ,999,CommonSet.PrePrintPort,fDifStr(1,CommonSet.LastLine),15,0);
          if fDifStr(2,CommonSet.LastLine)>'' then PrintDBStr(iQ,999,CommonSet.PrePrintPort,fDifStr(2,CommonSet.LastLine),15,0);
        end;

        prAddPrintQH(999,iQ,FormatDateTime('dd.mm hh:nn:sss',now)+', ������� - '+IntToStr(CommonSet.Station)+','+CommonSet.PrePrintPort);
        quPrint.Active:=False;
      end;
    end;
    if CommonSet.PrePrintPort='fisshtrih' then
    begin
      TestPrint:=1;
      While PrintQu and (TestPrint<=MaxDelayPrint) do
      begin
//        showmessage('������� �����, ��������� �������.');
        prWriteLog('������������� ������ ����� � �������.');
        delay(1000);
        inc(TestPrint);
      end;
      try
        PrintFCheck:=True; //��� ���� � ������� ������� ��������� ������

        PrintNFStr(' '+CommonSet.DepartName);

        if CommonSet.PreLine>'' then
        begin
          if fDifStr(1,CommonSet.PreLine)>'' then PrintNFStr(fDifStr(1,CommonSet.PreLine));
          if fDifStr(2,CommonSet.PreLine)>'' then PrintNFStr(fDifStr(2,CommonSet.PreLine));
        end;

        PrintNFStr(' ');
        PrintNFStr('       ���� �'+IntToStr(TabCh.iNumZ));
        PrintNFStr(' ');
        PrintNFStr('��������: '+TabCh.Name);
        PrintNFStr('����: '+TabCh.NumTable);
        if Tab.DBar>'' then
        begin
          Str(Tab.DPercent:5:2,StrWk);
          PrintNFStr('�����: '+Tab.DName+' ('+StrWk+'%)');
        end;
        PrintNFStr('������: '+IntToStr(TabCh.Quests));
        PrintNFStr('������: '+FormatDateTime('dd.mm.yyyy hh:nn:ss',TabCh.OpenTime));

        StrWk:='-----------------------------------';
        PrintNFStr(StrWk);
        StrWk:=' ��������     ���-��  ����   ����� ';
        PrintNFStr(StrWk);
        StrWk:='-----------------------------------';
        PrintNFStr(StrWk);

        rDiscont:=0;

        rSumQ:=0;
        quCheck.First;
        if quCheck.RecordCount>0 then iQuest:=quCheckQUEST.AsInteger;
        while not quCheck.Eof do
        begin      //��������� �������
          if iQuest<>quCheckQUEST.AsInteger then   //�������� �����
          begin
            StrWk:=' '; PrintNFStr(StrWk);
            StrWk:='  ����� �� ����� '+its(iQuest)+'   '+vts(rSumQ); PrintNFStr(StrWk);
            StrWk:=' '; PrintNFStr(StrWk);

            iQuest:=quCheckQUEST.AsInteger;
            rSumQ:=0;
          end;
          rSumQ:=rSumQ+quCheckSUMMA.AsFloat;

          PosCh.Name:=quCheckNAME.AsString;
          PosCh.Code:=quCheckCODE.AsString;
          PosCh.AddName:='';

          StrWk:= Copy(PosCh.Name,1,36);
          while Length(StrWk)<36 do StrWk:=StrWk+' ';
          PrintNFStr(StrWk); //������� ����� �������� - �������� �������

          rDiscont:=rDiscont+quCheckDISCOUNTSUM.AsFloat;

          if abs(frac(quCheckQUANTITY.AsFloat*100))>0 then nd:=3  //���� ������� ����� �� �������
          else
            if abs(frac(quCheckQUANTITY.AsFloat*10))>0 then nd:=2  //���� ������� ����� �������� �� �������
            else nd:=1;

          Str(quCheckQUANTITY.AsFloat:5:nd,StrWk1);
          StrWk:='          '+StrWk1;
          Str(quCheckPRICE.AsFloat:7:2,StrWk1);
          StrWk:=StrWk+' '+StrWk1;
          Str(quCheckSUMMA.AsFloat:8:2,StrWk1);
          StrWk:=StrWk+' '+StrWk1+'���';
          PrintNFStr(StrWk);

          quCheck.Next;
          if not quCheck.Eof then
          begin
            while quCheckITYPE.AsInteger=1 do
            begin  //������������
              if quCheck.Eof then break;
              if quCheckSifr.AsInteger>0 then
                if taModif.Locate('SIFR',quCheckSifr.AsInteger,[]) then
                begin
                  StrWk:='   '+Copy(taModifNAME.AsString,1,29);
                  PrintNFStr(StrWk);
                end;
              quCheck.Next;
            end;
          end;
        end;
        if bQuest then
        begin
          StrWk:=' '; PrintNFStr(StrWk);
          StrWk:='  ����� �� ����� '+its(iQuest)+'   '+vts(rSumQ); PrintNFStr(StrWk);
          StrWk:=' '; PrintNFStr(StrWk);
        end;

        StrWk:='-----------------------------------';
        PrintNFStr(StrWk);
        Str(TabCh.Summa:8:2,StrWk1);
        StrWk:=' �����                '+StrWk1+' ���';
        PrintNFStr(StrWk);

        if rDiscont>0.02 then
        begin
          PrintNFStr('');
//          rDProc:=RoundEx((rDiscont/(TabCh.Summa+rDiscont)*100)*100)/100;
//          Str(rDProc:5:2,StrWk1);
          Str(Tab.DPercent:4:1,StrWk1);
          Str(rDiscont:8:2,StrWk);
          StrWk:=' � �.�. ������ - '+StrWk1+'%  '+StrWk+'�.';
          PrintNFStr(StrWk);
        end;

        if Tab.DType=3 then
        begin
          BonusSum:=rv(CalcBonus(Tab.Id,Tab.DPercent));
          prFindPCardBalans(Tab.DBar,BonusLim,rBalansDay,DLimit,CliName,CliType);

          StrWk:=CliName; PrintNFStr(StrWk);
          StrWk:='����������� ������ - '+FlToS(rv(BonusLim))+' ���'; PrintNFStr(StrWk);

          Str(Tab.DPercent:4:1,StrWk);
          StrWk:='�������� ������ - ('+StrWk+'%)';  PrintNFStr(StrWk);
          StrWk:='����� ��������� - '+FlToS(BonusSum)+' ���' ;  PrintNFStr(StrWk);
        end;

        if CommonSet.LastLine>'' then
        begin
          PrintNFStr('');
          if fDifStr(1,CommonSet.LastLine)>'' then PrintNFStr(fDifStr(1,CommonSet.LastLine));
          if fDifStr(2,CommonSet.LastLine)>'' then PrintNFStr(fDifStr(2,CommonSet.LastLine));
        end;

        if CommonSet.TypeFis='fprint' then CutDoc
        else
        begin
          PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');
          CutDoc;
        end;  
      finally
        PrintFCheck:=False; //��� ���� � ������� ������� ��������� ������
      end;
    end;
    if CommonSet.PrePrintPort='fisprim' then
    begin
      TestPrint:=1;
      While PrintQu and (TestPrint<=MaxDelayPrint) do
      begin
//        showmessage('������� �����, ��������� �������.');
        prWriteLog('������������� ������ ����� � �������.');
        delay(1000);
        inc(TestPrint);
      end;
      try
        PrintFCheck:=True; //��� ���� � ������� ������� ��������� ������
        OpenNFDoc;
        SelectF(13);PrintNFStr(' '+CommonSet.DepartName);

        if CommonSet.PreLine>'' then
        begin
          if fDifStr(1,CommonSet.PreLine)>'' then PrintNFStr(fDifStr(1,CommonSet.PreLine));
          if fDifStr(2,CommonSet.PreLine)>'' then PrintNFStr(fDifStr(2,CommonSet.PreLine));
        end;

        PrintNFStr('');
        SelectF(14); PrintNFStr('   ���� �'+IntToStr(TabCh.iNumZ));
        SelectF(13); PrintNFStr(' '); PrintNFStr('��������: '+TabCh.Name);
        SelectF(3); PrintNFStr('����: '+TabCh.NumTable);
        if Tab.DBar>'' then
        begin
          Str(Tab.DPercent:5:2,StrWk);
          PrintNFStr('�����: '+TabCh.DName+' ('+StrWk+'%)');
        end;
        PrintNFStr('������: '+IntToStr(TabCh.Quests));
        PrintNFStr('������: '+FormatDateTime('dd.mm.yyyy hh:nn:ss',TabCh.OpenTime));

        SelectF(3); StrWk:='                                                       ';
        PrintNFStr(StrWk);
        SelectF(15); StrWk:=' ��������     ���-��  ����   ����� ';
        PrintNFStr(StrWk);
        SelectF(3); StrWk:='                                                       ';
        PrintNFStr(StrWk);

        rDiscont:=0;

        rSumQ:=0;
        quCheck.First;
        if quCheck.RecordCount>0 then iQuest:=quCheckQUEST.AsInteger;
        while not quCheck.Eof do
        begin      //��������� �������
          if iQuest<>quCheckQUEST.AsInteger then   //�������� �����
          begin
            SelectF(15); StrWk:=' '; PrintNFStr(StrWk);
            SelectF(3); StrWk:='  ����� �� ����� '+its(iQuest)+'   '+vts(rSumQ); PrintNFStr(StrWk);
            SelectF(15); StrWk:=' '; PrintNFStr(StrWk);

            iQuest:=quCheckQUEST.AsInteger;
            rSumQ:=0;
          end;
          rSumQ:=rSumQ+quCheckSUMMA.AsFloat;

          PosCh.Name:=quCheckNAME.AsString;
          PosCh.Code:=quCheckCODE.AsString;
          PosCh.AddName:='';

          StrWk:= Copy(PosCh.Name,1,29);
          while Length(StrWk)<29 do StrWk:=StrWk+' ';

          if abs(frac(quCheckQUANTITY.AsFloat*100))>0 then nd:=3  //���� ������� ����� �� �������
          else
            if abs(frac(quCheckQUANTITY.AsFloat*10))>0 then nd:=2  //���� ������� ����� �������� �� �������
            else nd:=1;

          Str(quCheckQUANTITY.AsFloat:5:nd,StrWk1);
          StrWk:=StrWk+' '+StrWk1;
          Str(quCheckPRICE.AsFloat:7:2,StrWk1);
          StrWk:=StrWk+' '+StrWk1;
          Str(quCheckSUMMA.AsFloat:8:2,StrWk1);
          StrWk:=StrWk+' '+StrWk1+'���';
          SelectF(3);
          PrintNFStr(StrWk);

          rDiscont:=rDiscont+quCheckDISCOUNTSUM.AsFloat;

          quCheck.Next;
          if not quCheck.Eof then
          begin
            while quCheckITYPE.AsInteger=1 do
            begin  //������������
              if quCheck.Eof then break;
              if quCheckSifr.AsInteger>0 then
                if taModif.Locate('SIFR',quCheckSifr.AsInteger,[]) then
                begin
                  StrWk:='   '+Copy(taModifNAME.AsString,1,29);
                  SelectF(0); PrintNFStr(StrWk);
                end;
              quCheck.Next;
            end;
          end;
        end;
        if bQuest then
        begin
          SelectF(15); StrWk:=' '; PrintNFStr(StrWk);
          SelectF(3); StrWk:='  ����� �� ����� '+its(iQuest)+'   '+vts(rSumQ); PrintNFStr(StrWk);
          SelectF(15); StrWk:=' '; PrintNFStr(StrWk);
        end;

        SelectF(3); StrWk:='                                                       ';
        PrintNFStr(StrWk);
        SelectF(10); PrintNFStr(' ');
        Str(TabCh.Summa:8:2,StrWk1);
        StrWk:=' �����                '+StrWk1+' ���';
        SelectF(15); PrintNFStr(StrWk);

        if rDiscont>0.02 then
        begin
          SelectF(10); PrintNFStr(' ');
//          rDProc:=RoundEx((rDiscont/(TabCh.Summa+rDiscont)*100)*100)/100;
//          Str(rDProc:5:2,StrWk1);
          Str(Tab.DPercent:4:1,StrWk1);
          Str(rDiscont:8:2,StrWk);
          StrWk:=' � �.�. ������      - '+StrWk+'�.';
          PrintNFStr(StrWk);
        end;

        if Tab.DType=3 then
        begin
          BonusSum:=rv(CalcBonus(Tab.Id,Tab.DPercent));
          prFindPCardBalans(Tab.DBar,BonusLim,rBalansDay,DLimit,CliName,CliType);

          StrWk:=CliName; PrintNFStr(StrWk);
          StrWk:='����������� ������ - '+FlToS(rv(BonusLim))+' ���'; PrintNFStr(StrWk);

          Str(Tab.DPercent:4:1,StrWk);
          StrWk:='�������� ������ - ('+StrWk+'%)';  PrintNFStr(StrWk);
          StrWk:='����� ��������� - '+FlToS(BonusSum)+' ���' ;  PrintNFStr(StrWk);
        end;

        if CommonSet.LastLine>'' then
        begin
          PrintNFStr(' ');
          if fDifStr(1,CommonSet.LastLine)>'' then PrintNFStr(fDifStr(1,CommonSet.LastLine));
          if fDifStr(2,CommonSet.LastLine)>'' then PrintNFStr(fDifStr(2,CommonSet.LastLine));
        end;

        PrintNFStr(' ');//PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');
        CloseNFDoc;
        if CommonSet.TypeFis<>'sp101' then CutDoc;
      finally
        PrintFCheck:=False; //��� ���� � ������� ������� ��������� ������
      end;
    end;

   //��������� ������ ����� �����

    TabView.BeginUpdate;
 //   trUpdate.StartTransaction;
    quTabs.Edit;
    quTabsISTATUS.AsInteger:=1;
    quTabs.Post;
//    trUpdate.Commit;
    TabView.EndUpdate;
    quTabs.Locate('ID',Tab.Id,[]);
//      fmMainCashRn.CreateViewPers;
    quTabs.Refresh;



    taModif.Active:=False;

    quCheck.Filtered:=False;
    quCheck.Active:=False;

    bPrintCheck:=False;
    if bExitPers then
    begin
      bExitPers:=False;
      DestrViewPers;
      fmPre.ShowModal;
      prEnButton;
      exit;
    end;

  end;
  prEnButton;
end;


procedure TfmMainCashRn.acCashPCardExecute(Sender: TObject);
//Var StrWk:String;
//Var rSum:Real;
Var iCurStatus:Integer;
    rBalans,rBalansDay,DLimit:Real;
    CliName,CliType:String;
    i,k:Integer;
    rSum,rSumD:Real;
    TabCh:TTab;
    StrWk1,StrP:string;
    kDelay:Integer;
begin
//  fmDiscount_Shape.ShowModal;
  if not CanDo('prPrintBNCheck') then exit;
  with dmC do
  begin
    if quTabs.RecordCount=0 then exit;
    if not FindTab(quTabsID.AsInteger) then exit;

    if quTabsISTATUS.AsInteger=2 then begin showmessage('���� �������������. �������� � �������������� ���������.');  exit; end;

    iCurStatus:=quTabsISTATUS.AsInteger;

    TabCh.OpenTime:=quTabsBEGTIME.AsDateTime;
    TabCh.NumTable:=quTabsNUMTABLE.AsString;
    TabCh.Quests:=quTabsQUESTS.AsInteger;
    TabCh.iStatus:=quTabsISTATUS.AsInteger;
    TabCh.DBar:=quTabsDISCONT.AsString;
    TabCh.Id:=quTabsID.AsInteger;
    TabCh.Summa:=quTabsTABSUM.AsFloat;
    TabCh.iNumZ:=quTabsNUMZ.AsInteger;

    Tab.OpenTime:=quTabsBEGTIME.AsDateTime;
    Tab.NumTable:=quTabsNUMTABLE.AsString;
    Tab.Quests:=quTabsQUESTS.AsInteger;
    Tab.iStatus:=quTabsISTATUS.AsInteger;
    Tab.DBar:=quTabsDISCONT.AsString;
    Tab.DBar1:=quTabsDISCONT.AsString;
    Tab.Id:=quTabsID.AsInteger;
    Tab.Summa:=quTabsTABSUM.AsFloat;
    Tab.iNumZ:=quTabsNUMZ.AsInteger;


    quTabs.Edit;
    quTabsISTATUS.AsInteger:=2;
    quTabs.Post;
    quTabs.Refresh;

    fmDiscount_Shape.cxTextEdit1.Text:='';
    fmDiscount_Shape.Caption:='��������� ��������� �����';
    fmDiscount_Shape.Label1.Caption:='��������� ��������� �����';
    fmDiscount_Shape.cxButton13.Visible:=True;
    fmDiscount_Shape.cxCheckBox1.Checked:=False;
    fmDiscount_Shape.cxCheckBox1.Enabled:=True;
    fmDiscount_Shape.cxCheckBox1.Visible:=True;

    if not CanDo('prPrintPCCheck') then
    begin
      fmDiscount_Shape.cxButton13.Visible:=False;
      fmDiscount_Shape.cxCheckBox1.Checked:=True;
      fmDiscount_Shape.cxCheckBox1.Enabled:=False;
    end;

    fmDiscount_Shape.ShowModal;
    if fmDiscount_Shape.ModalResult=mrOk then
    begin

      //������� ��������� ������ - ��������� ����� - �.�. �� ������
//    showmessage('');

      if fmDiscount_Shape.cxCheckBox1.Checked=False then
      begin
        TiRefresh.Enabled:=False; //���������� �������������� ����������

        if fmDiscount_Shape.cxTextEdit1.Text>'' then
        begin
          DiscountBar:=SOnlyDigit(fmDiscount_Shape.cxTextEdit1.Text);
          If FindPCard(DiscountBar) then
          begin
            bPrintCheck:=True; //������� ������ ����

            if (quTabsSALET.AsInteger<=1) and (Tab.SaleT>1) then
            begin
              quTabs.Edit;
              quTabsSALET.AsInteger:=Tab.SaleT;
              quTabs.Post;
              quTabs.Refresh;
            end;

            FormLog('SalePC',IntToStr(Person.Id)+' '+quTabsID_Personal.AsString+' '+quTabsNUMTABLE.AsString+' '+quTabsNUMZ.AsString);


            quCheck.Active:=False;
            quCheck.ParamByName('IDTAB').Value:=TabCh.Id;
            quCheck.Active:=True;

            rSum:=0;
            rSumD:=0;

            quCheck.First;
            while not quCheck.Eof do
            begin
              rSum:=rSum+quCheckSUMMA.AsFloat;
              rSumD:=rSumD+quCheckDISCOUNTSUM.AsFloat;

              quCheck.Next;
            end;


            taModif.Active:=True;

//������ �������� �� ��
            if Pos('fis',CommonSet.PrePrintPort)>0 then
            begin
              TestPrint:=1;
              While PrintQu and (TestPrint<=MaxDelayPrint) do
              begin
//        showmessage('������� �����, ��������� �������.');
                prWriteLog('������������� ������ ����� � �������.');
                delay(1000);
                inc(TestPrint);
              end;
              try
                PrintFCheck:=True; //��� ���� � ������� ������� ��������� ������
                OpenNFDoc;
                SelectF(13); PrintNFStr(' '+CommonSet.DepartName);

                if CommonSet.PreLine>'' then
                begin
                  if fDifStr(1,CommonSet.PreLine)>'' then PrintNFStr(fDifStr(1,CommonSet.PreLine));
                  if fDifStr(2,CommonSet.PreLine)>'' then PrintNFStr(fDifStr(2,CommonSet.PreLine));
                end;

                PrintNFStr('');

                SelectF(14); PrintNFStr('    ������ �'+IntToStr(Tab.iNumZ));
                SelectF(13); PrintNFStr('');
//                PrintNFStr('��������: '+Tab.Name); PrintNFStr('����: '+Tab.NumTable);
//                SelectF(3); PrintNFStr('������: '+IntToStr(Tab.Quests));

                if Tab.DBar>'' then
                begin
                  Str(Tab.DPercent:5:2,StrWk);
                  PrintNFStr('�����: '+Tab.DName+' ('+StrWk+'%)');
                end;
//                PrintNFStr('������: '+IntToStr(Tab.Quests));
                PrintNFStr('������: '+FormatDateTime('dd.mm.yyyy hh:nn:ss',Now));

                SelectF(3); StrWk:=S35('                                                       ','-----------------------------------');
                PrintNFStr(StrWk);
                SelectF(15); StrWk:=S35(' ��������          ���-��   ����   ����� ',' ��������      ���-�� ���� �����');
                PrintNFStr(StrWk);
                SelectF(3); StrWk:=S35('                                                       ','-----------------------------------');
                PrintNFStr(StrWk);

                quCheck.First;
                while not quCheck.Eof do
                begin
                  If CommonSet.l35=0 then
                  begin
                    StrWk:= Copy(quCheckName.AsString,1,29);
                    while Length(StrWk)<29 do StrWk:=StrWk+' ';
                    Str(quCheckQuantity.AsFloat:5:1,StrWk1);
                    StrWk:=StrWk+' '+StrWk1;
                    Str(quCheckPrice.AsFloat:7:2,StrWk1);
                    StrWk:=StrWk+' '+StrWk1;
                    Str(quCheckSumma.AsFloat:8:2,StrWk1);
                    StrWk:=StrWk+' '+StrWk1+'���';
                  end else
                  begin
                    StrWk:= Copy(quCheckName.AsString,1,15);
                    while Length(StrWk)<15 do StrWk:=StrWk+' ';
                    Str(quCheckQuantity.AsFloat:4:1,StrWk1);
                    StrWk:=StrWk+StrWk1;
                    Str(quCheckPrice.AsFloat:6:2,StrWk1);
                    StrWk:=StrWk+StrWk1;
                    Str(quCheckSumma.AsFloat:7:2,StrWk1);
                    StrWk:=StrWk+StrWk1+'���';
                  end;
                  SelectF(13);
                  PrintNFStr(StrWk);

                  quCheck.Next;

                  if not quCheck.Eof then
                  begin
                    while quCheckITYPE.AsInteger=1 do
                    begin  //������������
                      if quCheck.Eof then break;
                      if quCheckSifr.AsInteger>0 then
                        if taModif.Locate('SIFR',quCheckSifr.AsInteger,[]) then
                        begin
                          SelectF(13);
                          PrintNFStr('   '+Copy(taModifNAME.AsString,1,29));
                        end;
                      quCheck.Next;
                    end;
                  end;
                end;

                SelectF(3); StrWk:=S35('                                                       ','-----------------------------------');
                PrintNFStr(StrWk);
                SelectF(10); PrintNFStr(' ');
                Str(rSum:8:2,StrWk1);
                StrWk:=' �����                 '+StrWk1+' ���';
                SelectF(15); PrintNFStr(StrWk);

                if rSumD>0.02 then
                begin
                  SelectF(3);
                  StrWk:=S35('                                                       ','-----------------------------------');
                  PrintNFStr(StrWk);

                  Str(rSumD:8:2,StrWk);
                  rSumD:=RoundEx((rSumD/(rSum+rSumD)*100)*100)/100;
                  Str(rSumD:5:2,StrWk1);
                  StrWk:=' � �.�. ������ - '+StrWk1+'%  '+StrWk+'�.';

                  SelectF(10); PrintNFStr(StrWk);
                end;

                StrWk:='�������� ��.������ - '+Copy(Tab.PName,1,19);
                SelectF(15); PrintNFStr(StrWk);

                if CommonSet.LastLine>'' then
                begin
                  PrintNFStr(' ');
                  if fDifStr(1,CommonSet.LastLine)>'' then PrintNFStr(fDifStr(1,CommonSet.LastLine));
                  if fDifStr(2,CommonSet.LastLine)>'' then PrintNFStr(fDifStr(2,CommonSet.LastLine));
                end;

                PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');
                CloseNFDoc;
                CutDoc;
              finally
                PrintFCheck:=False; //��� ���� � ������� ������� ��������� ������
              end;
            end;


            if (pos('COM',CommonSet.PrePrintPort)>0)or(pos('LPT',CommonSet.PrePrintPort)>0) then //������ ������������ ����� �� ��������� �������
            begin
              TestPrint:=1;
              While PrintQu and (TestPrint<=MaxDelayPrint) do
              begin
//        showmessage('������� �����, ��������� �������.');
                prWriteLog('������������� ������ ����� � �������.');
                delay(1000);
                inc(TestPrint);
              end;
              try
                PrintFCheck:=True; //��� ���� � ������� ������� ��������� ������

                BufPr.iC:=0;

                StrP:=CommonSet.PrePrintPort;
                prDevOpen(CommonSet.PrePrintPort,0);
                PrintStr(StrP,' '+CommonSet.DepartName);

                prSetFont(StrP,0,0);

                if CommonSet.PreLine>'' then
                begin
                  if fDifStr(1,CommonSet.PreLine)>'' then PrintStr(StrP,fDifStr(1,CommonSet.PreLine));
                  if fDifStr(2,CommonSet.PreLine)>'' then PrintStr(StrP,fDifStr(2,CommonSet.PreLine));
                end;

                PrintStr(StrP,'');

                PrintStr(StrP,'    ������ �'+IntToStr(CommonSet.CashChNum));
                PrintStr(StrP,''); PrintStr(StrP,'');

                if Tab.DBar>'' then
                begin
                  Str(Tab.DPercent:5:2,StrWk);
                  PrintStr(StrP,'�����: '+Tab.DName+' ('+StrWk+'%)');
                end;
//                PrintStr(StrP,'������: '+IntToStr(Tab.Quests));
                PrintStr(StrP,'������: '+FormatDateTime('dd.mm.yyyy hh:nn:ss',Tab.OpenTime));

                StrWk:='-------------------------------------------------------';
                PrintStr(StrP,StrWk);
                StrWk:=' ��������          ���-��   ����   ����� ';
                PrintStr(StrP,StrWk);
                StrWk:='-------------------------------------------------------';
                PrintStr(StrP,StrWk);

                quCheck.First;
                while not quCheck.Eof do
                begin
                  StrWk:= Copy(quCheckName.AsString,1,29);
                  while Length(StrWk)<29 do StrWk:=StrWk+' ';
                  Str(quCheckQuantity.AsFloat:5:1,StrWk1);
                  StrWk:=StrWk+' '+StrWk1;
                  Str(quCheckPrice.AsFloat:7:2,StrWk1);
                  StrWk:=StrWk+' '+StrWk1;
                  Str(quCheckSumma.AsFloat:8:2,StrWk1);
                  StrWk:=StrWk+' '+StrWk1+'���';
                  PrintStr(StrP,StrWk);

                  quCheck.Next;

                  if not quCheck.Eof then
                  begin
                    while quCheckITYPE.AsInteger=1 do
                    begin  //������������
                      if quCheck.Eof then break;
                      if quCheckSifr.AsInteger>0 then
                        if taModif.Locate('SIFR',quCheckSifr.AsInteger,[]) then
                        begin
                          PrintStr(StrP,'   '+Copy(taModifNAME.AsString,1,29));
                        end;
                      quCheck.Next;
                    end;
                  end;
                end;

                StrWk:='-------------------------------------------------------';
                PrintStr(StrP,StrWk);
                PrintStr(StrP,' ');
                Str(rSum:8:2,StrWk1);
                StrWk:=' �����                      '+StrWk1+' ���';
                PrintStr(StrP,StrWk);

                if rSumD>0.02 then
                begin
                  StrWk:='-------------------------------------------------------';
                  PrintStr(StrP,StrWk);

                  Str(rSumD:8:2,StrWk);
                  rSumD:=RoundEx((rSumD/(rSum+rSumD)*100)*100)/100;
                  Str(rSumD:5:2,StrWk1);
                  StrWk:=' � �.�. ������ - '+StrWk1+'%  '+StrWk+'�.';

                  PrintStr(StrP,StrWk);
                end;

                StrWk:='�������� ��.������ - '+Copy(Tab.PName,1,19);
                PrintStr(StrP,StrWk);

                if CommonSet.LastLine>'' then
                begin
                  PrintStr(StrP,' ');
                  if fDifStr(1,CommonSet.LastLine)>'' then PrintStr(StrP,fDifStr(1,CommonSet.LastLine));
                  if fDifStr(2,CommonSet.LastLine)>'' then PrintStr(StrP,fDifStr(2,CommonSet.LastLine));
                end;

                kDelay:=(BufPr.iC div 40)-10;
                if kDelay<0 then kDelay:=0;

                prWrBuf(StrP);
                delay(CommonSet.ComDelay*5+trunc(kDelay*CommonSet.ComDelay/2));
                if CommonSet.SpecChar=1 then CutDocPrSpec
                else prCutDoc(StrP,0);

              finally
                PrintFCheck:=False; //��� ���� � ������� ������� ��������� ������
                prDevClose(StrP,0);
                inc(CommonSet.CashChNum); //����� �������� 1-��
                WriteCheckNum;
              end;
            end;


            taModif.Active:=False;

            //��������� ������������ �� ������������

            //����� ������������� ����� ������������ ����� �� ���� ������
            for i:=1 to 10 do
              for k:=1 to 10 do aCheckPlat[i,k]:=0;  //������

            quCheckOrgItog.Active:=False;
            quCheckOrgItog.ParamByName('IDTAB').AsInteger:=quTabsID.AsInteger;
            quCheckOrgItog.Active:=True;

            quCheckOrgItog.First;
            while not quCheckOrgItog.Eof do
            begin
              aCheckPlat[quCheckOrgItogIORG.AsInteger,1]:=quCheckOrgItogRSUM.AsFloat; // 1 - ����� � ������
              aCheckPlat[quCheckOrgItogIORG.AsInteger,2]:=quCheckOrgItogDSUM.AsFloat; // 2 - ����� ������

//              quOrgsSt.Active:=False;
//              quOrgsSt.ParamByName('IST').AsInteger:=CommonSet.Station;
//              quOrgsSt.Active:=True;
//              quOrgsSt.First;

              prSaveToAll.ParamByName('ID_TAB').AsInteger:=quTabsID.AsInteger;
              prSaveToAll.ParamByName('OPERTYPE').AsString:='SalePC';
              prSaveToAll.ParamByName('CHECKNUM').AsInteger:=Nums.iCheckNum;
              prSaveToAll.ParamByName('SKLAD').AsInteger:=0; //�������� �����������
              prSaveToAll.ParamByName('SPBAR').AsString:=DiscountBar; //��� �����
              prSaveToAll.ParamByName('STATION').AsInteger:=CommonSet.Station; //����� �������
              prSaveToAll.ParamByName('ID_PERSONCLOSE').AsInteger:=Person.Id; //��� ������

              prSaveToAll.ParamByName('CASHNUM').AsInteger:=0;
              prSaveToAll.ParamByName('CHECKTYPE').AsInteger:=0;
              prSaveToAll.ParamByName('TABSUM').AsFloat:=aCheckPlat[quCheckOrgItogIORG.AsInteger,1];
              prSaveToAll.ParamByName('DSUM').AsFloat:=aCheckPlat[quCheckOrgItogIORG.AsInteger,2];
              prSaveToAll.ParamByName('ASUM').AsFloat:=0;
              prSaveToAll.ParamByName('DTSUM').AsFloat:=aCheckPlat[quCheckOrgItogIORG.AsInteger,1];
              prSaveToAll.ParamByName('BNSUM').AsFloat:=0;
              prSaveToAll.ParamByName('RSUM').AsFloat:=0;
              prSaveToAll.ParamByName('IORG').AsInteger:=quCheckOrgItogIORG.AsInteger;
              prSaveToAll.ParamByName('IDATE').AsInteger:=Trunc(date);

              prSaveToAll.ExecProc;

//              quOrgsSt.Active:=False;

              quCheckOrgItog.Next;
            end;
            quCheckOrgItog.Active:=False;

          // ������ ����
            quDelTab.Active:=False;
            quDelTab.ParamByName('Id').AsInteger:=quTabsID.AsInteger;

            trDel.StartTransaction;
            quDelTab.Active:=True;
            trDel.Commit;

            //��c��� ��� � cashsail �� ���� �.�. ��� ������ ����
            CreateViewPers(True);


            bPrintCheck:=False; //������� ������ ����
          end else
          begin
            //��������� ������� ��������� ������
            FormLog('BadPC',IntToStr(Person.Id)+' '+quTabsID_Personal.AsString+' '+quTabsNUMTABLE.AsString+' '+quTabsNUMZ.AsString);

            quTabs.Edit;
            quTabsISTATUS.AsInteger:=iCurStatus;
            quTabs.Post;
            quTabs.Refresh;

          end;
        end else
        begin
          quTabs.Edit;
          quTabsISTATUS.AsInteger:=iCurStatus;
          quTabs.Post;
          quTabs.Refresh;
        end;

        TiRefresh.Enabled:=True; //���������� �������������� ����������
      end else
      begin
        quTabs.Edit;
        quTabsISTATUS.AsInteger:=iCurStatus;
        quTabs.Post;
        quTabs.Refresh;

        if fmDiscount_Shape.cxTextEdit1.Text>'' then
        begin
          DiscountBar:=SOnlyDigit(fmDiscount_Shape.cxTextEdit1.Text);
          prFindPCardBalans(DiscountBar,rBalans,rBalansDay,DLimit,CliName,CliType);

          Showmessage('     �������� - '+CliName+#$0D+'     ��� - '+CliType+#$0D+'     ������ - '+fts(rv(rBalans))+#$0D+'     ������ ������� - '+fts(rv(rBalansDay))+#$0D+'     ������� ����� - '+fts(rv(DLimit)));
        end;
      end;
    end
    else
    begin
      quTabs.Edit;
      quTabsISTATUS.AsInteger:=iCurStatus;
      quTabs.Post;
      quTabs.Refresh;
    end;
  end;
end;

procedure TfmMainCashRn.TimerDelayPrintQTimer(Sender: TObject);
begin
//
  TimerDelayPrintQ.Enabled:=False;
  prWriteLogPS('������ ������� ������ ��������.('+INtToStr(CommonSet.iDelay div 1000)+'�)');
  TimerPrintQ.Interval:=CommonSet.iDelay;
  TimerPrintQ.Enabled:=True;
end;

procedure TfmMainCashRn.TimerPrintQTimer(Sender: TObject);
begin
  TimerPrintQ.Enabled:=False;
 {
  try
    if PrintFCheck then prWriteLogPS('----- stop')
    else prWriteLogPS('----- go');
    if not PrintFCheck then
    begin
      PrintQu:=True;
      try

        dmC1.prPrint;

      finally
        delay(10);
        PrintQu:=False;
      end;
    end;
  finally
    TimerPrintQ.Enabled:=True;
  end;}
end;

procedure TfmMainCashRn.cxButton6Click(Sender: TObject);
begin
  //��� ������
with dmC do
  begin
    Label1.Caption:='��� �������� ������.';

    PersView.BeginUpdate;
    quPers.Active:=False;
    dsPers.DataSet:=nil;

    if CanDo('prViewAll') then
    begin
      quPers.SelectSQL.Clear;
      quPers.SelectSQL.Add('select t.Id_personal, p.Name, count(*) as CountTab, sum(TabSum) as TotalSum from tables t');
      quPers.SelectSQL.Add('left join rpersonal p on p.Id=t.Id_Personal');
      quPers.SelectSQL.Add('group by t.Id_personal, p.Name');
    end
    else
    begin
      quPers.SelectSQL.Clear;
      quPers.SelectSQL.Add('select t.Id_personal, p.Name, count(*) as CountTab, sum(TabSum) as TotalSum from tables t');
      quPers.SelectSQL.Add('left join rpersonal p on p.Id=t.Id_Personal');
      quPers.SelectSQL.Add('Where Id_personal='+IntToStr(Person.Id));
      quPers.SelectSQL.Add('group by t.Id_personal, p.Name');
    end;

    quPers.Active:=True;
    dsPers.DataSet:=quPers;
    PersView.EndUpdate;


    TabView.BeginUpdate;
    quTabs.Active:=False;
    dsTabs.DataSet:=nil;

    if CanDo('prViewAll') then
    begin
      quTabs.SelectSQL.Clear;
      quTabs.SelectSQL.Add('SELECT t.ID, t.ID_PERSONAL, t.NUMTABLE, t.QUESTS, t.TABSUM, t.BEGTIME,');
      quTabs.SelectSQL.Add('t.ISTATUS, t.DISCONT, dc.Name, t.NUMZ, t.SALET');
      quTabs.SelectSQL.Add('FROM TABLES t');
      quTabs.SelectSQL.Add('left join DISCCARD dc On dc.BARCODE=t.DISCONT');
      quTabs.SelectSQL.Add('ORDER BY ID_PERSONAL, t.BEGTIME');
    end
    else
    begin
      quTabs.SelectSQL.Clear;
      quTabs.SelectSQL.Add('SELECT t.ID, t.ID_PERSONAL, t.NUMTABLE, t.QUESTS, t.TABSUM, t.BEGTIME,');
      quTabs.SelectSQL.Add('t.ISTATUS, t.DISCONT, dc.Name, t.NUMZ, t.SALET');
      quTabs.SelectSQL.Add('FROM TABLES t');
      quTabs.SelectSQL.Add('left join DISCCARD dc On dc.BARCODE=t.DISCONT');
      quTabs.SelectSQL.Add('Where Id_personal='+IntToStr(Person.Id));
      quTabs.SelectSQL.Add('ORDER BY ID_PERSONAL, t.BEGTIME');
    end;

//t.BEGTIME>'27.01.2008 01:00'

    quTabs.Active:=True;
    dsTabs.DataSet:=quTabs;
    TabView.EndUpdate;

    PersView.Focused:=True;
    quPers.Last;
//    delay(10);

    while not quPers.Bof do
    begin
      PersView.Controller.FocusedRow.Expand(True);
      quPers.Prior;
//      delay(10);
    end;
  end;
end;

procedure TfmMainCashRn.cxButton7Click(Sender: TObject);
begin
  if not CanDo('prSberRep') then begin Showmessage('��� ����.'); exit; end;
  if CommonSet.BNManual=2 then
  begin //��� ����
    try
//      fmSber:=tFmSber.Create(Application);
      iMoneyBn:=0; //� ��������
      fmSber.Label1.Visible:=False;
      fmSber.cxButton9.Enabled:=True;
      fmSber.cxButton10.Enabled:=True;
      fmSber.cxButton11.Enabled:=True;
      fmSber.cxButton6.Enabled:=True;

      fmSber.cxButton2.Enabled:=False;
      fmSber.cxButton3.Enabled:=False;
      fmSber.cxButton4.Enabled:=False;
      fmSber.cxButton5.Enabled:=False;
      fmSber.cxButton7.Enabled:=False;
      fmSber.ShowModal;
    finally
//      fmSber.Release;
    end;
  end;

  if CommonSet.BNManual=3 then //���
  begin //��� ����
    try
      SberTabStop:=3;

      iMoneyBn:=0; //� ��������
      fmVTB.Label1.Visible:=False;

      fmVTB.cxButton9.Enabled:=True;
      fmVTB.cxButton9.TabStop:=True;
      fmVTB.cxButton9.TabOrder:=1;
      fmVTB.cxButton9.Default:=True;
      fmVTB.cxButton9.Default:=True;

      fmVTB.cxButton6.Enabled:=True;
      fmVTB.cxButton6.TabOrder:=4;
      fmVTB.cxButton6.TabStop:=True;

      fmVTB.cxButton2.Enabled:=False;
      fmVTB.cxButton2.TabStop:=False;

      fmVTB.cxButton8.Enabled:=False;
      fmVTB.cxButton8.TabStop:=False;

      fmVTB.cxButton13.Enabled:=True;
      fmVTB.cxButton13.TabOrder:=5;
      fmVTB.cxButton13.TabStop:=True;

      fmVTB.cxButton1.TabOrder:=6;

      BnStr:='';

      fmVTB.ShowModal;
    finally

    end;
  end;
end;

procedure TfmMainCashRn.cxButton8Click(Sender: TObject);
Var // StrWk:String;
    WId:INteger;
    rBn,rBnSum,rSum:Real;
    StrP:String;
    iQ:INteger;

Const s40:String[40]='----------------------------------------';
begin
  //������� �� ����������
  with dmC do
  begin
    if (CommonSet.CashNum>0)and(CommonSet.SpecChar=0) then CommonSet.CashZ:=Nums.ZNum;  //��� ���������� ���������� ������ 

    quRealW.Active:=False;
    quRealW.ParamByName('CASNUM').AsInteger:=CommonSet.CashNum;
    quRealW.ParamByName('ZNUM').AsInteger:=CommonSet.CashZ;
    quRealW.Active:=True;

//:CASNUM and cs.ZNUM=:ZNUM

    fmMessTxt:=tfmMessTxt.Create(Application);
    with fmMessTxt do
    begin
      Memo1.Lines.Add('������� �� �����������');
      Memo1.Lines.Add('�������� ����: '+FormatDateTime('dd.mm.yyyy',date));
      Memo1.Lines.Add('�����: '+INtToStr(CommonSet.CashZ));
      Memo1.Lines.Add('');
      Memo1.Lines.Add('           '+FormatDateTime('dd.mm.yyyy hh:nn',now));
      Memo1.Lines.Add(s40);
      Memo1.Lines.Add('   ������                    �����      ');
      Memo1.Lines.Add(s40);
      Memo1.Lines.Add('');
    end;

    wId:=0;
    rBn:=0;
    rBnSum:=0;
    rSum:=0;
    
    quRealW.First;
    while not quRealW.Eof do
    begin
      with fmMessTxt do
      begin
        if WId<>quRealWWAITERID.AsInteger then
        begin //����� ����������
          if WId>0 then //��� ��������� �������� ����������
          begin
            if rBn<>0 then
            begin
              Memo1.Lines.Add(prDefFormatStr(40,'����� ��������� �����:',rBn));
            end;
          end;

          rBn:=0;
          Memo1.Lines.Add(s40);
          Memo1.Lines.Add(quRealWPNAME.AsString);
          Memo1.Lines.Add('');
          if quRealWPAYTYPE.AsInteger=0 then
          begin
             Memo1.Lines.Add(prDefFormatStr(40,'��������: ',quRealWRSUM.AsFloat));
             Memo1.Lines.Add('');
             rSum:=rSum+quRealWRSUM.AsFloat;
          end
          else
          begin
            Memo1.Lines.Add('��������� ����� ');
            Memo1.Lines.Add(prDefFormatStr(40,quRealWCRNAME.AsString,quRealWRSUM.AsFloat));
            rBn:=rBn+quRealWRSUM.AsFloat;
            rBnSum:=rBnSum+quRealWRSUM.AsFloat;
          end;

          WId:=quRealWWAITERID.AsInteger;
        end else
        begin
          if quRealWPAYTYPE.AsInteger=0 then
          begin
            Memo1.Lines.Add(prDefFormatStr(40,'��������: ',quRealWRSUM.AsFloat));
            Memo1.Lines.Add('');
            rSum:=rSum+quRealWRSUM.AsFloat;
          end
          else
          begin
            if rBn=0 then Memo1.Lines.Add('��������� ����� ');
            Memo1.Lines.Add(prDefFormatStr(40,quRealWCRNAME.AsString,quRealWRSUM.AsFloat));
            rBn:=rBn+quRealWRSUM.AsFloat;
            rBnSum:=rBnSum+quRealWRSUM.AsFloat;
          end;
        end;
      end;
      quRealW.Next;
    end;
    if WId>0 then //��� ��������� �������� ����������
    begin
      if rBn<>0 then
      begin
        fmMessTxt.Memo1.Lines.Add(prDefFormatStr(40,'����� ��������� �����:',rBn));
      end;
    end;
    fmMessTxt.Memo1.Lines.Add(s40);
    fmMessTxt.Memo1.Lines.Add(s40);
    fmMessTxt.Memo1.Lines.Add('�����:');
    fmMessTxt.Memo1.Lines.Add(prDefFormatStr(40,'��������:',rSum));
    fmMessTxt.Memo1.Lines.Add(prDefFormatStr(40,'��������� �����:',rBnSum));
    fmMessTxt.Memo1.Lines.Add(prDefFormatStr(40,'�����:',rBnSum+rSum));
    fmMessTxt.Memo1.Lines.Add(s40);




    //    delay(5000);
    fmMessTxt.Button1.Enabled:=True;
    fmMessTxt.Button2.Enabled:=True;
    fmMessTxt.Button1.Visible:=True;
    fmMessTxt.Button2.Visible:=True;

    fmMessTxt.ShowModal;

    if fmMessTxt.ModalResult=mrOk then
    begin
      //������
      if (CommonSet.PrePrintPort>'')and(CommonSet.PrePrintPort<>'0')and(CommonSet.PrePrintPort<>'G')and(Pos('DB',CommonSet.PrePrintPort)=0)and(Pos('fis',CommonSet.PrePrintPort)=0) then
      begin //���������� ���� COM1 ��������
        try
          prDevOpen(CommonSet.PrePrintPort,0);
          BufPr.iC:=0;

          StrP:=CommonSet.PrePrintPort;
          prSetFont(StrP,10,0);
          PrintStr(StrP,'������� �� �����������');
          PrintStr(StrP,'�������� ����: '+FormatDateTime('dd.mm.yyyy',date));
          PrintStr(StrP,'�����: '+INtToStr(CommonSet.CashZ));
          PrintStr(StrP,'');
          PrintStr(StrP,'           '+FormatDateTime('dd.mm.yyyy hh:nn',now));
          PrintStr(StrP,s40);
          PrintStr(StrP,'   ������                    �����      ');
          PrintStr(StrP,s40);
          PrintStr(StrP,'');

          wId:=0;
          rBn:=0;
          rBnSum:=0;
          rSum:=0;

          quRealW.First;
          while not quRealW.Eof do
          begin
            if WId<>quRealWWAITERID.AsInteger then
            begin //����� ����������
              if WId>0 then //��� ��������� �������� ����������
              begin
                if rBn<>0 then
                begin
                  PrintStr(StrP,prDefFormatStr(40,'����� ��������� �����:',rBn));
                end;
              end;

              rBn:=0;
              PrintStr(StrP,s40);
              PrintStr(StrP,quRealWPNAME.AsString);
              PrintStr(StrP,'');
              if quRealWPAYTYPE.AsInteger=0 then
              begin
                PrintStr(StrP,prDefFormatStr(40,'��������: ',quRealWRSUM.AsFloat));
                PrintStr(StrP,'');
                rSum:=rSum+quRealWRSUM.AsFloat;
              end
              else
              begin
                PrintStr(StrP,'��������� ����� ');
                PrintStr(StrP,prDefFormatStr(40,quRealWCRNAME.AsString,quRealWRSUM.AsFloat));
                rBn:=rBn+quRealWRSUM.AsFloat;
                rBnSum:=rBnSum+quRealWRSUM.AsFloat;
              end;

              WId:=quRealWWAITERID.AsInteger;
            end else
            begin
              if quRealWPAYTYPE.AsInteger=0 then
              begin
                PrintStr(StrP,prDefFormatStr(40,'��������: ',quRealWRSUM.AsFloat));
                PrintStr(StrP,'');
                rSum:=rSum+quRealWRSUM.AsFloat;
              end
              else
              begin
                if rBn=0 then PrintStr(StrP,'��������� ����� ');
                PrintStr(StrP,prDefFormatStr(40,quRealWCRNAME.AsString,quRealWRSUM.AsFloat));
                rBn:=rBn+quRealWRSUM.AsFloat;
                rBnSum:=rBnSum+quRealWRSUM.AsFloat;
              end;
            end;
            quRealW.Next;
          end;
          if WId>0 then //��� ��������� �������� ����������
          begin
            if rBn<>0 then
            begin
              PrintStr(StrP,prDefFormatStr(40,'����� ��������� �����:',rBn));
            end;
          end;
          PrintStr(StrP,s40);
          PrintStr(StrP,s40);
          PrintStr(StrP,'�����:');
          PrintStr(StrP,prDefFormatStr(40,'��������:',rSum));
          PrintStr(StrP,prDefFormatStr(40,'��������� �����:',rBnSum));
          PrintStr(StrP,prDefFormatStr(40,'�����:',rBnSum+rSum));
          PrintStr(StrP,s40);

          prWrBuf(StrP);
          prCutDoc(StrP,0);
        finally
          prDevClose(StrP,0);
        end;
      end;
//������  DB
      if Pos('DB',CommonSet.PrePrintPort)>0 then
      begin //���������� ���� COM1 ��������
        try
          StrP:=CommonSet.PrePrintPort;

          with dmC1 do
          begin
            quPrint.Active:=False;
            quPrint.Active:=True;
            iQ:=GetId('PQH'); //����������� ������� �� 1-�

            PrintDBStr(iQ,999,CommonSet.PrePrintPort,'������� �� �����������',10,0);
            PrintDBStr(iQ,999,CommonSet.PrePrintPort,'�������� ����: '+FormatDateTime('dd.mm.yyyy',date),10,0);
            PrintDBStr(iQ,999,CommonSet.PrePrintPort,'�����: '+INtToStr(CommonSet.CashZ),10,0);
            PrintDBStr(iQ,999,CommonSet.PrePrintPort,' ',10,0);
            PrintDBStr(iQ,999,CommonSet.PrePrintPort,'           '+FormatDateTime('dd.mm.yyyy hh:nn',now),10,0);
            PrintDBStr(iQ,999,CommonSet.PrePrintPort,s40,10,0);
            PrintDBStr(iQ,999,CommonSet.PrePrintPort,'   ������                    �����      ',10,0);
            PrintDBStr(iQ,999,CommonSet.PrePrintPort,s40,10,0);
            PrintDBStr(iQ,999,CommonSet.PrePrintPort,' ',10,0);

            wId:=0;
            rBn:=0;
            rBnSum:=0;
            rSum:=0;

            quRealW.First;
            while not quRealW.Eof do
            begin
              if WId<>quRealWWAITERID.AsInteger then
              begin //����� ����������
                if WId>0 then //��� ��������� �������� ����������
                begin
                  if rBn<>0 then
                  begin
                    PrintStr(StrP,prDefFormatStr(40,'����� ��������� �����:',rBn));
                  end;
                end;

                rBn:=0;
                PrintDBStr(iQ,999,CommonSet.PrePrintPort,s40,10,0);
                PrintDBStr(iQ,999,CommonSet.PrePrintPort,quRealWPNAME.AsString,10,0);
                PrintDBStr(iQ,999,CommonSet.PrePrintPort,' ',10,0);

                if quRealWPAYTYPE.AsInteger=0 then
                begin
                  PrintDBStr(iQ,999,CommonSet.PrePrintPort,prDefFormatStr(40,'��������: ',quRealWRSUM.AsFloat),10,0);
                  PrintDBStr(iQ,999,CommonSet.PrePrintPort,' ',10,0);

                  rSum:=rSum+quRealWRSUM.AsFloat;
                end
                else
                begin
                  PrintDBStr(iQ,999,CommonSet.PrePrintPort,'��������� ����� ',10,0);
                  PrintDBStr(iQ,999,CommonSet.PrePrintPort,prDefFormatStr(40,quRealWCRNAME.AsString,quRealWRSUM.AsFloat),10,0);

                  rBn:=rBn+quRealWRSUM.AsFloat;
                  rBnSum:=rBnSum+quRealWRSUM.AsFloat;
                end;

                WId:=quRealWWAITERID.AsInteger;
              end else
              begin
                if quRealWPAYTYPE.AsInteger=0 then
                begin
                  PrintDBStr(iQ,999,CommonSet.PrePrintPort,prDefFormatStr(40,'��������: ',quRealWRSUM.AsFloat),10,0);
                  PrintDBStr(iQ,999,CommonSet.PrePrintPort,' ',10,0);

                  rSum:=rSum+quRealWRSUM.AsFloat;
                end
                else
                begin
                  if rBn=0 then
                    PrintDBStr(iQ,999,CommonSet.PrePrintPort,'��������� ����� ',10,0);

                  PrintDBStr(iQ,999,CommonSet.PrePrintPort,prDefFormatStr(40,quRealWCRNAME.AsString,quRealWRSUM.AsFloat),10,0);

                  rBn:=rBn+quRealWRSUM.AsFloat;
                  rBnSum:=rBnSum+quRealWRSUM.AsFloat;
                end;
              end;
              quRealW.Next;
            end;
            if WId>0 then //��� ��������� �������� ����������
            begin
              if rBn<>0 then
              begin
                PrintDBStr(iQ,999,CommonSet.PrePrintPort,prDefFormatStr(40,'����� ��������� �����:',rBn),10,0);
              end;
            end;

            PrintDBStr(iQ,999,CommonSet.PrePrintPort,s40,10,0);
            PrintDBStr(iQ,999,CommonSet.PrePrintPort,s40,10,0);
            PrintDBStr(iQ,999,CommonSet.PrePrintPort,'�����:',10,0);
            PrintDBStr(iQ,999,CommonSet.PrePrintPort,prDefFormatStr(40,'��������:',rSum),10,0);
            PrintDBStr(iQ,999,CommonSet.PrePrintPort,prDefFormatStr(40,'��������� �����:',rBnSum),10,0);
            PrintDBStr(iQ,999,CommonSet.PrePrintPort,prDefFormatStr(40,'�����:',rBnSum+rSum),10,0);
            PrintDBStr(iQ,999,CommonSet.PrePrintPort,s40,10,0);

            prAddPrintQH(999,iQ,FormatDateTime('dd.mm hh:nn:sss',now)+', ������� - '+IntToStr(CommonSet.Station)+','+CommonSet.PrePrintPort);
            quPrint.Active:=False;
          end;
        finally
        end;
      end;
    end;

    fmMessTxt.Release;
    quRealW.Active:=False;
  end;
end;

procedure TfmMainCashRn.cxButton9Click(Sender: TObject);
begin
  if CommonSet.Fis>0 then CashDriver
  else
  begin
    if CommonSet.SpecChar=1 then
    begin
      if pos('fis',CommonSet.SpecBox)=0 then dmC.OpenMoneyBox
      else
      begin
        try
          StrWk:='COM'+IntToStr(CommonSet.CashPort);
          CommonSet.CashNum:=CommonSet.CashNum*(-1);
          CashOpen(PChar(StrWk));
          prWriteLog('�� - '+INtToStr(CommonSet.CashNum)+' '+StrWk);
          CashDriver;
          CashClose;
          CommonSet.CashNum:=CommonSet.CashNum*(-1);
        except
        end;
      end;
    end else dmC.OpenMoneyBox;
  end;
end;

procedure TfmMainCashRn.TiRefreshTimer(Sender: TObject);
Var iTab:Integer;
begin
  //������
  with dmC do
  begin
//    cxButton1.Enabled:=False;
    iTab:=0;
    if quTabs.RecordCount>0 then iTab:=quTabsID.AsInteger;

    PersView.BeginUpdate;
    TabView.BeginUpdate;

    quPers.FullRefresh;
    quTabs.FullRefresh;
    if iTab>0 then quTabs.Locate('ID',iTab,[]);

    PersView.EndUpdate;
    TabView.EndUpdate;

    PersView.ViewData.Expand(False);

 //   cxButton1.Enabled:=True;
 //   delay(10);
  end;
end;

procedure TfmMainCashRn.acRecalcCountExecute(Sender: TObject);
Var rSum1,rSum2,rSum3:Real;
    Id:Integer;
begin
  if GetSums(rSum1,rSum2,rSum3) then
  begin
    with dmC1 do
    with dmC do
    begin
      quSums.Active:=False;
      quSums.Active:=True;
      Id:=quSums.RecordCount+1;
      quSums.Append;
      quSumsID.AsInteger:=Id;
      quSumsSUM1.AsFloat:=rSum1;
      quSumsSUM2.AsFloat:=rSum2;
      quSumsSUM3.AsFloat:=rSum3;
      quSums.Post;
      quSums.Active:=False;
    end;
  end;
end;

procedure TfmMainCashRn.Action1Execute(Sender: TObject);
//Var iQ:Integer;     //���� �������
begin
{  with dmC1 do
  begin
    quPrint.Active:=False;
    quPrint.Active:=True;
    iQ:=GetId('PQH'); //����������� ������� �� 1-�

    PrintDBStr(iQ,2,CommonSet.Pgroup2,'012345_00',0,0);
    PrintDBStr(iQ,2,CommonSet.Pgroup2,'012345_00',1,0);
    PrintDBStr(iQ,2,CommonSet.Pgroup2,'012345_00',2,0);
    PrintDBStr(iQ,2,CommonSet.Pgroup2,'012345_00',3,0);
    PrintDBStr(iQ,2,CommonSet.Pgroup2,'012345_00',4,0);
    PrintDBStr(iQ,2,CommonSet.Pgroup2,'012345_00',5,0);
    PrintDBStr(iQ,2,CommonSet.Pgroup2,'012345_00',6,0);
    PrintDBStr(iQ,2,CommonSet.Pgroup2,'012345_00',6,0);
    PrintDBStr(iQ,2,CommonSet.Pgroup2,'012345_00',8,0);
    PrintDBStr(iQ,2,CommonSet.Pgroup2,'012345_00',9,0);
    PrintDBStr(iQ,2,CommonSet.Pgroup2,'012345_00',10,0);
    PrintDBStr(iQ,2,CommonSet.Pgroup2,'012345_00',11,0);
    PrintDBStr(iQ,2,CommonSet.Pgroup2,'012345_00',12,0);
    PrintDBStr(iQ,2,CommonSet.Pgroup2,'012345_00',13,0);
    PrintDBStr(iQ,2,CommonSet.Pgroup2,'012345_00',14,0);
    PrintDBStr(iQ,2,CommonSet.Pgroup2,'012345_00',15,0);
    PrintDBStr(iQ,2,CommonSet.Pgroup2,'012345_00',16,0);

    prAddPrintQH(2,iQ,FormatDateTime('dd.mm hh:nn:sss',now)+', ������� - '+IntToStr(CommonSet.Station)+','+CommonSet.Pgroup2);

    quPrint.Active:=False;
  end;              }
  ShowMessage('Ok');
end;

procedure TfmMainCashRn.TabViewCellClick(Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
//Var iX,iY:INteger;
begin
  TiRefresh.Enabled:=False;
  TiRefresh.Enabled:=True;
  if CommonSet.ViewTab=1 then
  begin
//    iX:=ACellViewInfo.BorderBounds[bBottom].BottomRight.X;
//    iY:=ACellViewInfo.BorderBounds[bBottom].BottomRight.Y;
//    ShowMessage('x='+INtToStr(iX)+'   y='+INtToStr(iY));
//    fmViewTab.Left:=iX+20;
//    fmViewTab.Top:=iY+120;
    if fmMainCashRn.Tag=0 then //1024�768
    begin
      fmViewTab.Left:=336;
      fmViewTab.Top:=510;
    end;
    if fmMainCashRn.Tag=1 then //800x600
    begin
      fmViewTab.Left:=220;
      fmViewTab.Top:=365;
    end;

    acViewTab.Execute;
  end;
end;

procedure TfmMainCashRn.cxButton10Click(Sender: TObject);
Var rNal,rSum:Real;
    StrWk:String;
begin
  if not CanDo('prInCass') then begin Showmessage('��� ����.'); exit; end;
  prWriteLog('!!InCass;'+IntToStr(Person.Id)+';'+Person.Name+';');
  Event_RegEx(206,0,0,0,'','',0,0,0,0,0,0);

  try
    fmCashInOut:=TfmCashInOut.Create(Application);
    if GetCashReg(rNal) then fmCashInOut.cxCurrencyEdit1.Value:=rNal
    else fmCashInOut.cxCurrencyEdit1.Value:=0;
    fmCashInOut.cxCurrencyEdit2.Value:=0;

    fmCashInOut.ShowModal;
    if fmCashInOut.ModalResult=mrOk then
    begin
    
      rSum:=fmCashInOut.cxCurrencyEdit2.Value;
      if rSum>=0.01 then
      begin
        if CommonSet.TypeFis='sp101' then InCass(rSum*(-1)) else InCass(rSum);
        //�������� ����������

        if CommonSet.PrePrintPort='fisshtrih' then
        begin
          TestPrint:=1;
          While PrintQu and (TestPrint<=MaxDelayPrint) do
          begin
//        showmessage('������� �����, ��������� �������.');
            prWriteLog('������������� ������ ����� � �������.');
            delay(1000);
            inc(TestPrint);
          end;
          try
            PrintFCheck:=True; //��� ���� � ������� ������� ��������� ������

            PrintNFStr(' ');
            PrintNFStr(FormatDateTime('dd.mm.yyyy            hh:nn:ss',Now));
            PrintNFStr(' ');
            PrintNFStr('������� �����          ����� '+INtToStr(CommonSet.CashNum));
            PrintNFStr('���������: '+Person.Name);
            PrintNFStr(' ');
            Str(rNal:8:2,StrWk);
            PrintNFStr('����� � �����     ���:'+StrWk);
            PrintNFStr(' ');
            Str(rSum:8:2,StrWk);
            PrintNFStr('������ �����      ���:'+StrWk);
            PrintNFStr(' ');
            Str((rNal-rSum):8:2,StrWk);
            PrintNFStr('�������           ���:'+StrWk);
            PrintNFStr(' ');
            PrintNFStr(' ');
            PrintNFStr('���� _______________________');
            PrintNFStr(' ');
            PrintNFStr(' ');
            PrintNFStr('������ _____________________');


            if CommonSet.TypeFis='fprint' then CutDoc
            else
            begin
              PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');
              CutDoc;
            end;  
          finally
            PrintFCheck:=False; //��� ���� � ������� ������� ��������� ������
          end;
        end;
      end else
        ShowMessage('����� ������� ����');
    end;
    fmCashInOut.Release;
  except
  end;
end;

procedure TfmMainCashRn.TimerCloseTimer(Sender: TObject);
begin
  Close;
end;

procedure TfmMainCashRn.acViewTabExecute(Sender: TObject);
begin
  //���������� ������������ �����
  with dmC do
  begin
    if quTabs.RecordCount=0 then  begin bOpenTable:=False; exit; end;

    if not FindTab(quTabsID.AsInteger) then
    begin
      TabView.BeginUpdate;
      quTabs.FullRefresh;
      TabView.EndUpdate;
      bOpenTable:=False;
      exit;
    end;

    //���������
    quSpecSel.Active:=False;
    quSpecSel.ParamByName('IDTAB').AsInteger:=quTabsID.AsInteger;
    quSpecSel.Active:=True;
    delay(100);
    fmViewTab.Show;
  end;
end;

procedure TfmMainCashRn.cxButton11Click(Sender: TObject);
begin
  with dmC do
  begin
    if quTabs.RecordCount=0 then  exit;

    if not FindTab(quTabsID.AsInteger) then
    begin
      TabView.BeginUpdate;
      quTabs.FullRefresh;
      TabView.EndUpdate;
      exit;
    end;

    if taMes.Active=False then taMes.Active:=True;
    taMes.FullRefresh;

    TiRefresh.Enabled:=False; //���������� �������������� ����������

    Tab.Id_Personal:=quTabsID_PERSONAL.AsInteger;

    quFindPers.Active:=False;
    quFindPers.ParamByName('IDP').AsInteger:=Tab.Id_Personal;
    quFindPers.Active:=True;
    Tab.Name:=quFindPersNAME.AsString;
    quFindPers.Active:=False;

    Tab.OpenTime:=quTabsBEGTIME.AsDateTime;
    Tab.NumTable:=quTabsNUMTABLE.AsString;
    Tab.Quests:=quTabsQUESTS.AsInteger;
    Tab.iStatus:=quTabsISTATUS.AsInteger;
    Tab.DBar:=quTabsDISCONT.AsString;
    Tab.DBar1:=quTabsDISCONT.AsString;
    Tab.Id:=quTabsID.AsInteger;
    Tab.Summa:=quTabsTABSUM.AsFloat;
    Tab.iNumZ:=quTabsNUMZ.AsInteger;

    fmSendMessage.Label1.Caption:='����� � '+its(Tab.iNumZ)+',   � ����� '+Tab.NumTable+',    ������ '+its(Tab.Quests);
    fmSendMessage.Label2.Caption:='�������� - '+Tab.Name+',     ����� '+vts(Tab.Summa);

    fmSendMessage.cxIComboBox1.ItemIndex:=0;
  end;
  fmSendMessage.ShowModal;
  if fmSendMessage.ModalResult=mrOk then
  begin
    with dmC do
    begin
      CloseTe(taServP);

      taServP.Append;
      taServPName.AsString:=taMesMESSAG.AsString;
      taServPCode.AsString:='';
      taServPQuant.AsFloat:=0;
      taServPStream.AsInteger:=fmSendMessage.cxIComboBox1.ItemIndex+1;
      taServPiType.AsInteger:=1; // ����������� ��� ���������
      taServP.Post;

      PrintServCh('��������� ');// ���� ������� (1) - �� ������� ������ �� ����

      taServP.Active:=False;
    end;
  end;
  TiRefresh.Enabled:=True;
end;

procedure TfmMainCashRn.PersViewCellClick(Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  TiRefresh.Enabled:=False;
  TiRefresh.Enabled:=True;
end;

procedure TfmMainCashRn.cxButton12Click(Sender: TObject);
begin
 //��������
  with dmC do
  begin
    try
      fmStopListCash.ViewSl.BeginUpdate;
      quMenuSL.Active:=False;
      quMenuSL.Active:=True;
    finally
      fmStopListCash.ViewSl.EndUpdate;
    end;
  end;
  fmStopListCash.ShowModal;
end;

procedure TfmMainCashRn.cxButton13Click(Sender: TObject);
begin
  fmCopyCh.cxTextEdit1.Text:=its(Nums.ZNum)+','+its(Nums.iCheckNum);
  fmCopyCh.ShowModal;
end;

procedure TfmMainCashRn.cxButton19Click(Sender: TObject);
Var StrWk:String;
    rSum,rSumR:Real;
begin
  with dmC do
  begin
    prWriteLog('!!OutPutMoney;');

    if Prizma then
    begin
      prWriteLog('!!Inkass');
      Event_RegEx(206,0,0,0,'','',0,0,0,0,0,0);
    end;

    with dmC do
    begin
      quOrgsSt.Active:=False;
      quOrgsSt.ParamByName('IST').AsInteger:=CommonSet.Station;
      quOrgsSt.Active:=True;

      if quOrgsSt.RecordCount=1 then prGetCashPar(quOrgsStIORG.AsInteger)
      else
      begin
        fmSelOrg.ShowModal;
        prGetCashPar(quOrgsStIORG.AsInteger);
      end;

      if (CommonSet.fis>0)and(CommonSet.SpecChar=0) then //����������� ����������
      begin
        GetCashReg(rSumR);
        { ���� ���������� ������
        if rSumR<0 then               //������������ ����� ���� �� �������� - ������� ����� ������
        begin
          fmInOutM.Label8.Caption:='';
        end else
        begin
          Str(rSumR:12:2,StrWk);
          fmInOutM.Label8.Caption:=StrWk;
        end;
        }
        Str(rSumR:12:2,StrWk);
        fmInOutM.Label8.Caption:=StrWk;

        fmInOutM.Caption:='������� ����� � �����.';

        fmInOutM.ShowModal; // bMoneyCalc:=False - ��� ��������
        if fmInOutM.ModalResult=mrOk then
        begin
          //���� � ���� �������� �����
          rSum:=fmInOutM.CurrencyEdit1.EditValue;
          if rSum<>0 then InCass((-1)*rSum); //������ ���� ������������� ��� �������

          try
            OpenNFDoc;
            SelectF(13);
            PrintNFStr(' ');
            PrintNFStr(FormatDateTime('dd.mm.yyyy            hh:nn:ss',Now));
            PrintNFStr(' ');
            PrintNFStr('������� �����        ����� '+INtToStr(CommonSet.CashNum));
            PrintNFStr('���������: '+Person.Name);
            PrintNFStr(' ');
            Str(rSumR:8:2,StrWk);
            PrintNFStr('����� � �����     ���:'+StrWk);
            PrintNFStr(' ');
            Str(rSum:8:2,StrWk);
            PrintNFStr('������ �����     ���:'+StrWk);
            PrintNFStr(' ');
            Str((rSumR-rSum):8:2,StrWk);
            PrintNFStr('�������           ���:'+StrWk);
            PrintNFStr(' ');
            PrintNFStr(' ');
            PrintNFStr('���� _______________________');
            PrintNFStr(' ');
            PrintNFStr(' ');
            PrintNFStr('������ _____________________');

            PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');
            CloseNFDoc;
            CutDoc;
          finally
          end;
        end;
      end;
    end;
  end;
end;


procedure TfmMainCashRn.cxButton18Click(Sender: TObject);
Var StrWk:String;
    rSum,rSumR:Real;
begin
  with dmC do
  begin
    prWriteLog('!!InPutMoney;');

    if Prizma then
    begin
      prWriteLog('!!InPutMoney');
      Event_RegEx(206,0,0,0,'','',0,0,0,0,0,0);
    end;

    with dmC do
    begin
      quOrgsSt.Active:=False;
      quOrgsSt.ParamByName('IST').AsInteger:=CommonSet.Station;
      quOrgsSt.Active:=True;

      if quOrgsSt.RecordCount=1 then prGetCashPar(quOrgsStIORG.AsInteger)
      else
      begin
        fmSelOrg.ShowModal;
        prGetCashPar(quOrgsStIORG.AsInteger);
      end;

      if (CommonSet.fis>0)and(CommonSet.SpecChar=0) then //����������� ����������
      begin
        GetCashReg(rSumR);
        { ���� ���������� ����� ������
        if rSumR<0 then              //������������ ����� ���� �� �������� - ������� ����� ������
        begin
          fmInOutM.Label8.Caption:='';
        end else
        begin
          Str(rSumR:12:2,StrWk);
          fmInOutM.Label8.Caption:=StrWk;
        end;
        }

        Str(rSumR:12:2,StrWk);
        fmInOutM.Label8.Caption:=StrWk;

        fmInOutM.Caption:='�������� ����� � �����.';

        fmInOutM.ShowModal; // bMoneyCalc:=False - ��� ��������
        if fmInOutM.ModalResult=mrOk then
        begin
          //���� � ���� �������� �����
          rSum:=fmInOutM.CurrencyEdit1.EditValue;
          if rSum<>0 then InCass(rSum);

          try
            OpenNFDoc;
            SelectF(13);
            PrintNFStr(' ');
            PrintNFStr(FormatDateTime('dd.mm.yyyy            hh:nn:ss',Now));
            PrintNFStr(' ');
    //        PrintNFStr('������� �����          ����� '+INtToStr(CommonSet.CashNum));
            PrintNFStr('�������� �����        ����� '+INtToStr(CommonSet.CashNum));
            PrintNFStr('���������: '+Person.Name);
            PrintNFStr(' ');
            Str(rSumR:8:2,StrWk);
            PrintNFStr('����� � �����     ���:'+StrWk);
            PrintNFStr(' ');
            Str(rSum:8:2,StrWk);
            PrintNFStr('������� �����     ���:'+StrWk);
            PrintNFStr(' ');
            Str((rSumR+rSum):8:2,StrWk);
            PrintNFStr('�������           ���:'+StrWk);
            PrintNFStr(' ');
            PrintNFStr(' ');
            PrintNFStr('���� _______________________');
            PrintNFStr(' ');
            PrintNFStr(' ');
            PrintNFStr('������ _____________________');

            PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');
            CloseNFDoc;
            CutDoc;
          finally
          end;
        end;
      end;
    end;
  end;
end;

end.
