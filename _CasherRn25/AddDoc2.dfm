object fmAddDoc2: TfmAddDoc2
  Left = 546
  Top = 333
  Width = 1336
  Height = 532
  Caption = #1044#1086#1082#1091#1084#1077#1085#1090#1099' ('#1074#1086#1079#1074#1088#1072#1090')'
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Image7: TImage
    Left = 4
    Top = 121
    Width = 17
    Height = 15
    Center = True
    Picture.Data = {
      07544269746D617026040000424D260400000000000036000000280000001200
      0000120000000100180000000000F00300000000000000000000000000000000
      0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCEC7C6A5
      B6A584AE8484AA8494A694B5B2ADFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBDC7B55AAE5A29AE2931BE3929C342
      31BE4A52B26394A694FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
      FFFFFFFFFFFFBDC7B5429E3110A2084ABA4AA5CFA552CB6B29CB4A29CB4A42BA
      5294A694FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFD6CFCE63A2
      521092006BBA63CED7CED6D3D6BDD3BD8CD39C39CB5A21C73952AE5AFFFFFFFF
      FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFBDC3B5318E18188A005AAE4AC6
      D7C6DEDBDEC6D3C6D6D3D69CCFA529C73929BA3194A68CFFFFFFFFFFFFFFFFFF
      0000FFFFFFFFFFFFFFFFFFBDC3B5529E4221860021920842AA3194CF945AC35A
      A5CFADCED3CE4AC75218BA187BAA7BFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
      FFFFFFC6C7BD94BE8484BA73C6DFBD73BE6B189A1018A6108CCB8CDEDFDE52C3
      5210B21084AE7BFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFCEC7C6ADC7
      A5ADCFA5F7FBF7E7EFDE6BB65A63BA5AD6E3CEDEE3D64ABA4231AE29A5B69CFF
      FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFD6CBC6BDCBBDADCFA5CEE3C6FF
      FFFFFFFFFFFFFBFFF7F7F7A5D39C4AB63973B66BFFFFFFFFFFFFFFFFFFFFFFFF
      0000FFFFFFFFFFFFFFFFFFFFFFFFC6C7C6BDCFB5B5D3A5C6DBB5D6E7CECEE7CE
      A5D3946BBA5A73B66BBDC7B5FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFC6C7C6C6CBBDB5CFADADCB9C9CC78C94BE849CBE94C6C7
      BDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFD6CBC6CEC7C6C6CBC6C6CBBDC6CBC6D6CFCEFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000}
    Proportional = True
    Transparent = True
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1328
    Height = 129
    Align = alTop
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 1
    object Label1: TLabel
      Left = 24
      Top = 16
      Width = 65
      Height = 13
      Caption = #1044#1086#1082#1091#1084#1077#1085#1090' '#8470
      Transparent = True
    end
    object Label2: TLabel
      Left = 24
      Top = 40
      Width = 33
      Height = 13
      Caption = #1057#1063#1060#1050
      Transparent = True
    end
    object Label3: TLabel
      Left = 248
      Top = 40
      Width = 11
      Height = 13
      Caption = #1086#1090
      Transparent = True
    end
    object Label4: TLabel
      Left = 24
      Top = 72
      Width = 58
      Height = 13
      Caption = #1050#1086#1085#1090#1088#1072#1075#1077#1085#1090
      Transparent = True
    end
    object Label5: TLabel
      Left = 24
      Top = 96
      Width = 82
      Height = 13
      Caption = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103
      Transparent = True
    end
    object Label12: TLabel
      Left = 248
      Top = 16
      Width = 11
      Height = 13
      Caption = #1086#1090
      Transparent = True
    end
    object Label15: TLabel
      Left = 264
      Top = 96
      Width = 201
      Height = 13
      AutoSize = False
      Caption = #1056#1086#1079#1085#1080#1095#1085#1072#1103' '#1094#1077#1085#1072
      Transparent = True
    end
    object Label6: TLabel
      Left = 336
      Top = 72
      Width = 37
      Height = 13
      Caption = #1086#1090' '#1082#1086#1075#1086
      Transparent = True
    end
    object Label17: TLabel
      Left = 448
      Top = 100
      Width = 85
      Height = 13
      Caption = #1054#1089#1086#1073#1099#1081' '#1087#1088#1080#1079#1085#1072#1082
    end
    object cxTextEdit1: TcxTextEdit
      Left = 112
      Top = 12
      Properties.MaxLength = 15
      TabOrder = 0
      Text = 'cxTextEdit1'
      Width = 121
    end
    object cxDateEdit1: TcxDateEdit
      Left = 272
      Top = 12
      Style.BorderStyle = ebsOffice11
      TabOrder = 1
      Width = 121
    end
    object cxTextEdit2: TcxTextEdit
      Left = 112
      Top = 36
      Properties.MaxLength = 15
      TabOrder = 2
      Text = 'cxTextEdit2'
      Width = 121
    end
    object cxDateEdit2: TcxDateEdit
      Left = 272
      Top = 36
      Style.BorderStyle = ebsOffice11
      TabOrder = 3
      Width = 121
    end
    object cxButtonEdit1: TcxButtonEdit
      Left = 112
      Top = 68
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
      Style.BorderStyle = ebsOffice11
      TabOrder = 4
      Text = 'cxButtonEdit1'
      OnKeyPress = cxButtonEdit1KeyPress
      Width = 193
    end
    object cxLookupComboBox1: TcxLookupComboBox
      Left = 112
      Top = 92
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Caption = #1053#1072#1079#1074#1072#1085#1080#1077
          FieldName = 'NAMEMH'
        end>
      Properties.ListOptions.AnsiSort = True
      Properties.ListSource = dmO.dsMHAll
      Properties.OnChange = cxLookupComboBox1PropertiesChange
      Style.BorderStyle = ebsOffice11
      Style.LookAndFeel.Kind = lfOffice11
      Style.PopupBorderStyle = epbsDefault
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 5
      Width = 145
    end
    object cxButtonEdit2: TcxButtonEdit
      Left = 396
      Top = 68
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.OnButtonClick = cxButtonEdit2PropertiesButtonClick
      Style.BorderStyle = ebsOffice11
      TabOrder = 6
      Text = 'cxButtonEdit2'
      OnKeyPress = cxButtonEdit2KeyPress
      Width = 217
    end
    object cxSpinEdit1: TcxSpinEdit
      Left = 548
      Top = 96
      Properties.MaxValue = 255.000000000000000000
      Style.BorderStyle = ebsOffice11
      TabOrder = 7
      Width = 65
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 479
    Width = 1328
    Height = 19
    Color = clWhite
    Panels = <
      item
        Width = 300
      end
      item
        Width = 50
      end>
  end
  object Panel2: TPanel
    Left = 0
    Top = 438
    Width = 1328
    Height = 41
    Align = alBottom
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 2
    object cxButton1: TcxButton
      Left = 32
      Top = 8
      Width = 113
      Height = 25
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100'  Ctrl+S'
      Default = True
      TabOrder = 0
      OnClick = cxButton1Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 184
      Top = 8
      Width = 113
      Height = 25
      Caption = #1042#1099#1093#1086#1076'   F10'
      TabOrder = 1
      OnClick = cxButton2Click
      Glyph.Data = {
        5E040000424D5E04000000000000360000002800000012000000130000000100
        18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
        8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
        0000CED3D6848684848684848684848684848684848684848684848684848684
        848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
        7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
        FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
        FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
        007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
        FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
        00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
        494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
        00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000}
      LookAndFeel.Kind = lfOffice11
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 129
    Width = 145
    Height = 309
    Align = alLeft
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 3
    object cxLabel1: TcxLabel
      Left = 8
      Top = 16
      Cursor = crHandPoint
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1087#1086#1079#1080#1094#1080#1102'  Ins'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = 4227072
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel1Click
    end
    object cxLabel2: TcxLabel
      Left = 8
      Top = 72
      Cursor = crHandPoint
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1087#1086#1079#1080#1094#1080#1102'  F8'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clRed
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel2Click
    end
    object cxLabel3: TcxLabel
      Left = 8
      Top = 152
      Cursor = crHandPoint
      Caption = #1042' '#1094#1077#1085#1072#1093' '#1087#1086#1089#1083'. '#1087#1088#1080#1093#1086#1076#1072
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clBlack
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel3Click
    end
    object cxLabel4: TcxLabel
      Left = 7
      Top = 200
      Cursor = crHandPoint
      Caption = #1059#1088#1072#1074#1085#1103#1090#1100' '#1094#1077#1085#1099
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clBlack
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel4Click
    end
    object cxLabel5: TcxLabel
      Left = 7
      Top = 176
      Cursor = crHandPoint
      Caption = #1055#1072#1088#1090#1080#1080' '#1090#1086#1074#1072#1088#1072
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clBlack
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel5Click
    end
    object cxLabel6: TcxLabel
      Left = 7
      Top = 224
      Cursor = crHandPoint
      Caption = #1055#1088#1086#1074#1077#1088#1082#1072' '#1087#1072#1088#1090#1080#1081
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clBlack
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel6Click
    end
    object cxLabel7: TcxLabel
      Left = 8
      Top = 32
      Cursor = crHandPoint
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1089#1087#1080#1089#1086#1082' Ctrl+Ins'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = 4227072
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel7Click
    end
    object cxLabel8: TcxLabel
      Left = 8
      Top = 88
      Cursor = crHandPoint
      Caption = #1054#1095#1080#1089#1090#1080#1090#1100' Ctrl+F8'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clRed
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel8Click
    end
  end
  object PageControl1: TPageControl
    Left = 156
    Top = 136
    Width = 553
    Height = 289
    ActivePage = TabSheet1
    Style = tsFlatButtons
    TabOrder = 4
    object TabSheet1: TTabSheet
      Caption = #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1103
      object GridDoc2: TcxGrid
        Left = 0
        Top = 0
        Width = 545
        Height = 258
        Align = alClient
        TabOrder = 0
        LookAndFeel.Kind = lfOffice11
        object ViewDoc2: TcxGridDBTableView
          PopupMenu = PopupMenu1
          OnDblClick = cxLabel5Click
          OnDragDrop = ViewDoc2DragDrop
          OnDragOver = ViewDoc2DragOver
          NavigatorButtons.ConfirmDelete = False
          OnEditing = ViewDoc2Editing
          OnEditKeyDown = ViewDoc2EditKeyDown
          OnEditKeyPress = ViewDoc2EditKeyPress
          DataController.DataSource = dsSpec
          DataController.Summary.DefaultGroupSummaryItems = <
            item
              Format = '0.00'
              Kind = skSum
              Position = spFooter
              FieldName = 'RNds'
              Column = ViewDoc2RNds
            end
            item
              Format = '0.00'
              Kind = skSum
              Position = spFooter
              FieldName = 'Sum1'
              Column = ViewDoc2Sum1
            end
            item
              Format = '0.00'
              Kind = skSum
              Position = spFooter
              FieldName = 'Sum2'
              Column = ViewDoc2Sum2
            end
            item
              Format = '0.00'
              Kind = skSum
              Position = spFooter
              FieldName = 'SumNac'
              Column = ViewDoc2SumNac
            end
            item
              Format = '0.0'
              Kind = skAverage
              Position = spFooter
              FieldName = 'ProcNac'
              Column = ViewDoc2ProcNac
            end
            item
              Format = '0.00'
              Kind = skSum
              Position = spFooter
              FieldName = 'Sum0'
              Column = ViewDoc2Sum0
            end>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = '0.00'
              Kind = skSum
              FieldName = 'RNds'
              Column = ViewDoc2RNds
            end
            item
              Format = '0.00'
              Kind = skSum
              FieldName = 'Sum1'
              Column = ViewDoc2Sum1
            end
            item
              Format = '0.00'
              Kind = skSum
              FieldName = 'Sum2'
              Column = ViewDoc2Sum2
            end
            item
              Format = '0.00'
              Kind = skSum
              FieldName = 'SumNac'
              Column = ViewDoc2SumNac
            end
            item
              Format = ',0.0%;-,0.0%'
              Kind = skAverage
              FieldName = 'ProcNac'
              Column = ViewDoc2ProcNac
            end
            item
              Format = '0.00'
              Kind = skSum
              FieldName = 'Sum0'
              Column = ViewDoc2Sum0
            end>
          DataController.Summary.SummaryGroups = <>
          OptionsData.Deleting = False
          OptionsData.Inserting = False
          OptionsSelection.MultiSelect = True
          OptionsView.Footer = True
          OptionsView.GroupFooters = gfAlwaysVisible
          OptionsView.Indicator = True
          object ViewDoc2Num: TcxGridDBColumn
            Caption = #8470' '#1087#1087
            DataBinding.FieldName = 'Num'
            Width = 49
          end
          object ViewDoc2IdGoods: TcxGridDBColumn
            Caption = #1050#1086#1076
            DataBinding.FieldName = 'IdGoods'
            Width = 60
          end
          object ViewDoc2CType: TcxGridDBColumn
            Caption = #1058#1080#1087' '#1087#1086#1079#1080#1094#1080#1080
            DataBinding.FieldName = 'CType'
            PropertiesClassName = 'TcxImageComboBoxProperties'
            Properties.Items = <
              item
                Description = #1058#1086#1074#1072#1088
                ImageIndex = 0
                Value = 1
              end
              item
                Description = #1059#1089#1083#1091#1075#1080
                ImageIndex = 0
                Value = 2
              end
              item
                Description = #1040#1074#1072#1085#1089#1099
                ImageIndex = 0
                Value = 3
              end
              item
                Description = #1058#1072#1088#1072
                ImageIndex = 0
                Value = 4
              end>
            Styles.Content = dmO.cxStyle5
          end
          object ViewDoc2NameG: TcxGridDBColumn
            Caption = #1053#1072#1079#1074#1072#1085#1080#1077
            DataBinding.FieldName = 'NameG'
            Width = 185
          end
          object ViewDoc2IM: TcxGridDBColumn
            DataBinding.FieldName = 'IM'
            Visible = False
            Options.Editing = False
          end
          object ViewDoc2SM: TcxGridDBColumn
            Caption = #1045#1076'.'#1080#1079#1084'.'
            DataBinding.FieldName = 'SM'
            PropertiesClassName = 'TcxButtonEditProperties'
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.OnButtonClick = ViewDoc2SMPropertiesButtonClick
            Width = 53
          end
          object ViewDoc2Quant: TcxGridDBColumn
            Caption = #1050#1086#1083'-'#1074#1086
            DataBinding.FieldName = 'Quant'
          end
          object ViewDoc2Price1: TcxGridDBColumn
            Caption = #1062#1077#1085#1072' '#1079#1072#1082#1091#1087'. c '#1053#1044#1057
            DataBinding.FieldName = 'Price1'
            PropertiesClassName = 'TcxCurrencyEditProperties'
            Properties.DisplayFormat = ',0.00;-,0.00'
          end
          object ViewDoc2Sum1: TcxGridDBColumn
            Caption = #1057#1091#1084#1084#1072' '#1079#1072#1082#1091#1087'. '#1089' '#1053#1044#1057
            DataBinding.FieldName = 'Sum1'
            PropertiesClassName = 'TcxCurrencyEditProperties'
            Properties.DisplayFormat = ',0.00;-,0.00'
            Styles.Content = dmO.cxStyle25
          end
          object ViewDoc2Price2: TcxGridDBColumn
            Caption = #1062#1077#1085#1072' '#1091#1095#1077#1090#1085#1072#1103
            DataBinding.FieldName = 'Price2'
            PropertiesClassName = 'TcxCurrencyEditProperties'
            Properties.DisplayFormat = ',0.00;-,0.00'
          end
          object ViewDoc2Sum2: TcxGridDBColumn
            Caption = #1057#1091#1084#1084#1072' '#1091#1095#1077#1090#1085#1072#1103
            DataBinding.FieldName = 'Sum2'
            PropertiesClassName = 'TcxCurrencyEditProperties'
            Properties.DisplayFormat = ',0.00;-,0.00'
            Styles.Content = dmO.cxStyle25
          end
          object ViewDoc2SumNac: TcxGridDBColumn
            Caption = #1057#1091#1084#1084#1072' '#1085#1072#1094#1077#1085#1082#1080
            DataBinding.FieldName = 'SumNac'
            PropertiesClassName = 'TcxCurrencyEditProperties'
            Properties.DisplayFormat = ',0.00;-,0.00'
            Options.Editing = False
            Styles.Content = dmO.cxStyle25
          end
          object ViewDoc2ProcNac: TcxGridDBColumn
            Caption = '% '#1085#1072#1094#1077#1085#1082#1080
            DataBinding.FieldName = 'ProcNac'
            Options.Editing = False
          end
          object ViewDoc2INds: TcxGridDBColumn
            DataBinding.FieldName = 'INds'
            Visible = False
            Options.Editing = False
          end
          object ViewDoc2SNds: TcxGridDBColumn
            Caption = #1053#1044#1057
            DataBinding.FieldName = 'SNds'
            Options.Editing = False
            Width = 46
          end
          object ViewDoc2RNds: TcxGridDBColumn
            Caption = #1053#1044#1057' '#1074' '#1090'.'#1095'.'
            DataBinding.FieldName = 'RNds'
            PropertiesClassName = 'TcxCurrencyEditProperties'
          end
          object ViewDoc2Price0: TcxGridDBColumn
            Caption = #1062#1077#1085#1072' '#1079#1072#1082#1091#1087'. '#1073#1077#1079' '#1053#1044#1057
            DataBinding.FieldName = 'Price0'
          end
          object ViewDoc2Sum0: TcxGridDBColumn
            Caption = #1057#1091#1084#1084#1072' '#1079#1072#1082#1091#1087'. '#1073#1077#1079' '#1053#1044#1057
            DataBinding.FieldName = 'Sum0'
          end
        end
        object LevelDoc2: TcxGridLevel
          GridView = ViewDoc2
        end
      end
    end
  end
  object FormPlacement1: TFormPlacement
    Active = False
    Left = 448
    Top = 160
  end
  object dsSpec: TDataSource
    DataSet = taSpecOut
    Left = 344
    Top = 268
  end
  object prCalcPrice: TpFIBStoredProc
    Transaction = dmO.trSelect
    Database = dmO.OfficeRnDb
    SQL.Strings = (
      
        'EXECUTE PROCEDURE PR_CALCPRICE (?INPRICE, ?IDGOOD, ?PRICE0, ?PRI' +
        'CE1)')
    StoredProcName = 'PR_CALCPRICE'
    Left = 272
    Top = 272
  end
  object amDocOut: TActionManager
    Left = 216
    Top = 208
    StyleName = 'XP Style'
    object acSave: TAction
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
      ShortCut = 16467
      OnExecute = acSaveExecute
    end
    object acExit: TAction
      Caption = #1042#1099#1093#1086#1076
      ShortCut = 121
      OnExecute = acExitExecute
    end
    object acAddPos: TAction
      Caption = 'acAddPos'
      ShortCut = 45
      OnExecute = acAddPosExecute
    end
    object acAddList: TAction
      Caption = 'acAddList'
      ShortCut = 16429
      OnExecute = acAddListExecute
    end
    object acDelPos: TAction
      Caption = 'acDelPos'
      ShortCut = 119
      OnExecute = acDelPosExecute
    end
    object acDelAll: TAction
      Caption = 'acDelAll'
      OnExecute = acDelAllExecute
    end
    object acMovePos: TAction
      Caption = #1044#1074#1080#1078#1077#1085#1080#1077' '#1090#1086#1074#1072#1088#1072
      ShortCut = 32885
      OnExecute = acMovePosExecute
    end
  end
  object PopupMenu1: TPopupMenu
    Images = dmO.imState
    Left = 427
    Top = 230
    object N2: TMenuItem
      Action = acMovePos
      Hint = #1044#1074#1080#1078#1077#1085#1080#1077' '#1090#1086#1074#1072#1088#1072
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object MenuItem1: TMenuItem
      Caption = #1069#1082#1089#1087#1086#1088#1090' '#1074' Excel'
      ImageIndex = 47
      OnClick = MenuItem1Click
    end
  end
  object taSpecOut: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 560
    Top = 247
    object taSpecOutNum: TIntegerField
      FieldName = 'Num'
    end
    object taSpecOutIdGoods: TIntegerField
      FieldName = 'IdGoods'
    end
    object taSpecOutNameG: TStringField
      FieldName = 'NameG'
      Size = 200
    end
    object taSpecOutIM: TIntegerField
      FieldName = 'IM'
    end
    object taSpecOutSM: TStringField
      FieldName = 'SM'
      Size = 50
    end
    object taSpecOutQuant: TFloatField
      FieldName = 'Quant'
      OnChange = taSpecOutQuantChange
      DisplayFormat = '0.000'
    end
    object taSpecOutPrice0: TFloatField
      FieldName = 'Price0'
      OnChange = taSpecOutPrice0Change
      DisplayFormat = '0.00'
    end
    object taSpecOutSum0: TFloatField
      FieldName = 'Sum0'
      OnChange = taSpecOutSum0Change
      DisplayFormat = '0.00'
    end
    object taSpecOutPrice1: TFloatField
      FieldName = 'Price1'
      OnChange = taSpecOutPrice1Change
      DisplayFormat = '0.00'
    end
    object taSpecOutSum1: TFloatField
      FieldName = 'Sum1'
      OnChange = taSpecOutSum1Change
      DisplayFormat = '0.00'
    end
    object taSpecOutPrice2: TFloatField
      FieldName = 'Price2'
      OnChange = taSpecOutPrice2Change
      DisplayFormat = '0.00'
    end
    object taSpecOutSum2: TFloatField
      FieldName = 'Sum2'
      OnChange = taSpecOutSum2Change
      DisplayFormat = '0.00'
    end
    object taSpecOutINds: TIntegerField
      FieldName = 'INds'
    end
    object taSpecOutSNds: TStringField
      FieldName = 'SNds'
      Size = 30
    end
    object taSpecOutSumNac: TFloatField
      FieldName = 'SumNac'
      DisplayFormat = '0.00'
    end
    object taSpecOutProcNac: TFloatField
      FieldName = 'ProcNac'
      DisplayFormat = '0.0'
    end
    object taSpecOutKM: TFloatField
      FieldName = 'KM'
      DisplayFormat = '0.000'
    end
    object taSpecOutCType: TSmallintField
      FieldName = 'CType'
    end
    object taSpecOutRNds: TFloatField
      FieldName = 'RNds'
      DisplayFormat = '0.00'
    end
  end
end
