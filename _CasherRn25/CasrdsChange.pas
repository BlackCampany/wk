unit CasrdsChange;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SpeedBar, ExtCtrls, cxStyles, cxCustomData, cxGraphics,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxCalc,
  cxCalendar, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxControls, cxGridCustomView, cxGrid,
  FIBDataSet, pFIBDataSet, ActnList, XPStyleActnCtrls, ActnMan;

type
  TfmCardsChange = class(TForm)
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    GrCaChange: TcxGrid;
    ViewCaChange: TcxGridDBTableView;
    LevelCaChange: TcxGridLevel;
    quCaChange: TpFIBDataSet;
    dsquCaChange: TDataSource;
    quCaChangeIDCARD: TFIBIntegerField;
    quCaChangeIDCHANGEGR: TFIBIntegerField;
    quCaChangeNAME: TFIBStringField;
    quCaChangeFIFO: TFIBSmallIntField;
    quCaChangeCOMMENT: TFIBStringField;
    ViewCaChangeIDCHANGEGR: TcxGridDBColumn;
    ViewCaChangeNAME: TcxGridDBColumn;
    ViewCaChangeFIFO: TcxGridDBColumn;
    ViewCaChangeCOMMENT: TcxGridDBColumn;
    amCaChange: TActionManager;
    acAddPos: TAction;
    acDelPos: TAction;
    procedure SpeedItem1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure acAddPosExecute(Sender: TObject);
    procedure acDelPosExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmCardsChange: TfmCardsChange;

implementation

uses dmOffice, SelChangeGr;

{$R *.dfm}

procedure TfmCardsChange.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmCardsChange.FormCreate(Sender: TObject);
begin
  GrCaChange.Align:=AlClient;
end;

procedure TfmCardsChange.FormShow(Sender: TObject);
begin
  if CanDo('prEditCardChange') then
  begin
    acAddPOs.Enabled:=True;
    acDelPOs.Enabled:=True;
    ViewCaChange.OptionsData.Editing:=True;
  end else
  begin
    acAddPOs.Enabled:=False;
    acDelPOs.Enabled:=False;
    ViewCaChange.OptionsData.Editing:=False;
  end;
end;

procedure TfmCardsChange.acAddPosExecute(Sender: TObject);
begin
  fmSelChangeGr.quSelChangeGr.Active:=False;
  fmSelChangeGr.quSelChangeGr.Active:=True;
  fmSelChangeGr.ShowModal;
end;

procedure TfmCardsChange.acDelPosExecute(Sender: TObject);
begin
  if quCaChange.Active then
    if quCaChange.RecordCount>0 then quCaChange.Delete;
end;

end.
