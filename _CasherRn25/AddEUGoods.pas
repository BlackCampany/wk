unit AddEUGoods;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, ComCtrls, StdCtrls, cxButtons,
  ExtCtrls, cxCalc, cxControls, cxContainer, cxEdit, cxTextEdit,
  cxMaskEdit, cxDropDownEdit, cxCalendar;

type
  TfmAddEUGoods = class(TForm)
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    StatusBar1: TStatusBar;
    cxDateEdit1: TcxDateEdit;
    cxDateEdit2: TcxDateEdit;
    cxCalcEdit1: TcxCalcEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    procedure cxButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAddEUGoods: TfmAddEUGoods;

implementation

uses dmOffice, Un1, AddGoods;

{$R *.dfm}

procedure TfmAddEUGoods.cxButton1Click(Sender: TObject);
Var iBeg,iEnd:Integer;
    bErr:Boolean;
begin
  iBeg:=prDateToI(cxDateEdit1.Date);
  iEnd:=prDateToI(cxDateEdit2.Date);
  if iBeg>iEnd then begin showmessage('������������ ��������. ���������� �������� ������.'); exit; end;
  with fmAddGood do
  begin
    ViewEU.BeginUpdate;
    bErr:=False;
    tEU.First;
    while not tEU.Eof do
    begin
      if (iBeg<=tEUIDATEE.AsInteger) and (iBeg>=tEUIDATEB.AsInteger) then begin bErr:=True; Break; end;
      if (iEnd<=tEUIDATEE.AsInteger) and (iEnd>=tEUIDATEB.AsInteger) then begin bErr:=True; Break; end;
      tEU.Next;
    end;
    tEU.First;
    if bErr then showmessage('����������� ���������� �����������. ���������� �������� ������.')
    else
    begin //��� ������ ����� ���������
      tEU.Append;
      tEUIDATEB.AsInteger:=iBeg;
      tEUIDATEE.AsInteger:=iEnd;
      tEUto100g.AsFloat:=fmAddEUGoods.cxCalcEdit1.EditValue;
      tEU.Post;
    end;
    ViewEU.EndUpdate;
  end;
end;

end.
