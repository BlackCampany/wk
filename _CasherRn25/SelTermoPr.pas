unit SelTermoPr;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxLookAndFeelPainters, Menus, StdCtrls, cxButtons, cxGroupBox,
  cxRadioGroup, cxControls, cxContainer, cxEdit, cxTextEdit, ExtCtrls,WinSpool;

type
  TfmTermoPr = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    cxTextEdit1: TcxTextEdit;
    Label2: TLabel;
    cxRadioGroup1: TcxRadioGroup;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    PrintDialog1: TPrintDialog;
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmTermoPr: TfmTermoPr;

implementation

uses Un1;

{$R *.dfm}

procedure TfmTermoPr.cxButton1Click(Sender: TObject);
begin
  CommonSet.TPrintN:=cxTextEdit1.Text;
  CommonSet.TPrintCode:=cxRadioGroup1.ItemIndex+1;
  writeini;
end;

procedure TfmTermoPr.cxButton2Click(Sender: TObject);
begin
  close;
end;

procedure TfmTermoPr.cxButton3Click(Sender: TObject);
begin
  PrintDialog1.Execute;
//  cxTextEdit1.Text:=PrintDialog1.;
end;

end.
