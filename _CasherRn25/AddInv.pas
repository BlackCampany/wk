unit AddInv;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, cxGraphics, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxCurrencyEdit, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxControls, cxGridCustomView, cxGrid,
  cxLabel, ExtCtrls, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox, cxMaskEdit, cxCalendar, cxContainer, cxTextEdit,
  StdCtrls, ComCtrls, cxButtons, Placemnt, DBClient, cxButtonEdit,
  ActnList, XPStyleActnCtrls, ActnMan, cxSpinEdit, cxImageComboBox, cxMemo,
  cxGroupBox, cxRadioGroup, FR_DSet, FR_DBSet, FR_Class,
  pFIBDataSet, FIBDatabase, pFIBDatabase, cxCheckBox, dxmdaset,
  cxProgressBar, FIBDataSet;

type
  TfmAddInv = class(TForm)
    Panel2: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Label1: TLabel;
    Label5: TLabel;
    Label12: TLabel;
    Label15: TLabel;
    cxTextEdit1: TcxTextEdit;
    cxDateEdit1: TcxDateEdit;
    cxLookupComboBox1: TcxLookupComboBox;
    Panel3: TPanel;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    FormPlacement1: TFormPlacement;
    taSpec: TClientDataSet;
    dsSpec: TDataSource;
    taSpecNum: TIntegerField;
    taSpecIdGoods: TIntegerField;
    taSpecNameG: TStringField;
    taSpecIM: TIntegerField;
    taSpecSM: TStringField;
    taSpecQuant: TFloatField;
    taSpecPriceIn: TFloatField;
    taSpecSumIn: TFloatField;
    taSpecPriceUch: TFloatField;
    taSpecSumUch: TFloatField;
    taSpecQuantFact: TFloatField;
    taSpecPriceInF: TFloatField;
    taSpecSumInF: TFloatField;
    taSpecPriceUchF: TFloatField;
    taSpecSumUchF: TFloatField;
    taSpecQuantDif: TFloatField;
    taSpecSumInDif: TFloatField;
    taSpecSumUchDif: TFloatField;
    cxLabel7: TcxLabel;
    cxLabel8: TcxLabel;
    cxLabel9: TcxLabel;
    amInv: TActionManager;
    acAddPos: TAction;
    taSpecKm: TFloatField;
    Label2: TLabel;
    taSpecTCard: TIntegerField;
    taSpecId_Group: TIntegerField;
    Panel5: TPanel;
    Panel4: TPanel;
    Memo1: TcxMemo;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    GridInv: TcxGrid;
    ViewInv: TcxGridDBTableView;
    ViewInvNum: TcxGridDBColumn;
    ViewInvIdGoods: TcxGridDBColumn;
    ViewInvNameG: TcxGridDBColumn;
    ViewInvTCard: TcxGridDBColumn;
    ViewInvIdGroup: TcxGridDBColumn;
    ViewInvNameGr: TcxGridDBColumn;
    ViewInvIM: TcxGridDBColumn;
    ViewInvSM: TcxGridDBColumn;
    ViewInvQuant: TcxGridDBColumn;
    ViewInvPriceIn: TcxGridDBColumn;
    ViewInvSumIn: TcxGridDBColumn;
    ViewInvQuantFact: TcxGridDBColumn;
    ViewInvPriceInF: TcxGridDBColumn;
    ViewInvSumInF: TcxGridDBColumn;
    ViewInvQuantDif: TcxGridDBColumn;
    ViewInvSumInDif: TcxGridDBColumn;
    LevelInv: TcxGridLevel;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    GridBC: TcxGrid;
    ViewBC: TcxGridDBTableView;
    ViewBCID: TcxGridDBColumn;
    ViewBCCODEB: TcxGridDBColumn;
    ViewBCNAMEB: TcxGridDBColumn;
    ViewBCQUANT: TcxGridDBColumn;
    ViewBCIDCARD: TcxGridDBColumn;
    ViewBCNAMEC: TcxGridDBColumn;
    ViewBCSB: TcxGridDBColumn;
    ViewBCQUANTC: TcxGridDBColumn;
    ViewBCPRICEIN: TcxGridDBColumn;
    ViewBCSUMIN: TcxGridDBColumn;
    ViewBCIM: TcxGridDBColumn;
    ViewBCSM: TcxGridDBColumn;
    LevelBC: TcxGridLevel;
    acSaveInv: TAction;
    Label3: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    cxButton3: TcxButton;
    taSpecC: TClientDataSet;
    taSpecCNum: TIntegerField;
    taSpecCIdGoods: TIntegerField;
    taSpecCNameG: TStringField;
    taSpecCIM: TIntegerField;
    taSpecCSM: TStringField;
    taSpecCQuant: TFloatField;
    taSpecCPriceIn: TFloatField;
    taSpecCSumIn: TFloatField;
    taSpecCPriceUch: TFloatField;
    taSpecCSumUch: TFloatField;
    taSpecCQuantFact: TFloatField;
    taSpecCPriceInF: TFloatField;
    taSpecCSumInF: TFloatField;
    taSpecCPriceUchF: TFloatField;
    taSpecCSumUchF: TFloatField;
    taSpecCQuantDif: TFloatField;
    taSpecCSumInDif: TFloatField;
    taSpecCSumUchDif: TFloatField;
    taSpecCKm: TFloatField;
    taSpecCTCard: TIntegerField;
    taSpecCId_Group: TIntegerField;
    taSpecCNameGr: TStringField;
    dsSpecC: TDataSource;
    GridInvC: TcxGrid;
    ViewInvC: TcxGridDBTableView;
    LevelInvC: TcxGridLevel;
    RepInv: TfrReport;
    frdsSpec: TfrDBDataSet;
    cxRadioButton1: TcxRadioButton;
    cxRadioButton2: TcxRadioButton;
    cxRadioButton3: TcxRadioButton;
    acAddList: TAction;
    acDelPos: TAction;
    acDelAll: TAction;
    taSpecNoCalc: TSmallintField;
    ViewInvNoCalc: TcxGridDBColumn;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    ViewInvCNum: TcxGridDBColumn;
    ViewInvCIdGoods: TcxGridDBColumn;
    ViewInvCNameG: TcxGridDBColumn;
    ViewInvCIM: TcxGridDBColumn;
    ViewInvCSM: TcxGridDBColumn;
    ViewInvCQuant: TcxGridDBColumn;
    ViewInvCPriceIn: TcxGridDBColumn;
    ViewInvCSumIn: TcxGridDBColumn;
    ViewInvCQuantFact: TcxGridDBColumn;
    ViewInvCPriceInF: TcxGridDBColumn;
    ViewInvCSumInF: TcxGridDBColumn;
    ViewInvCQuantDif: TcxGridDBColumn;
    ViewInvCSumInDif: TcxGridDBColumn;
    ViewInvCTCard: TcxGridDBColumn;
    ViewInvCIdGroup: TcxGridDBColumn;
    ViewInvCNameGr: TcxGridDBColumn;
    frdsSpecC: TfrDBDataSet;
    acCalc1: TAction;
    cxCheckBox1: TcxCheckBox;
    acExit: TAction;
    taSpecSumInTO: TFloatField;
    taSpecSumInTODif: TFloatField;
    ViewInvSumInTO: TcxGridDBColumn;
    ViewInvSumInTODif: TcxGridDBColumn;
    taSpecCSumInTO: TFloatField;
    ViewInvCSumInTO: TcxGridDBColumn;
    acEquals: TAction;
    Label7: TLabel;
    cxTextEdit2: TcxTextEdit;
    taSpecNameGr: TStringField;
    taSpecNameGr2: TStringField;
    ViewInvNameGr2: TcxGridDBColumn;
    acRefreshGr: TAction;
    taSpecCSumInTODif: TFloatField;
    ViewInvCSumInTODif: TcxGridDBColumn;
    cxCheckBox2: TcxCheckBox;
    N3: TMenuItem;
    Excel1: TMenuItem;
    taR: TdxMemData;
    taRCode: TIntegerField;
    taRPrice: TFloatField;
    acMovePos: TAction;
    PBar1: TcxProgressBar;
    cxTextEdit3: TcxTextEdit;
    acFindWord: TAction;
    acFastSave: TAction;
    ViewInvCCType: TcxGridDBColumn;
    taSpecCType: TSmallintField;
    ViewInvCType: TcxGridDBColumn;
    taSpecCCType: TSmallintField;
    Label8: TLabel;
    cxComboBox1: TcxComboBox;
    cxRadioButton4: TcxRadioButton;
    cxRadioButton5: TcxRadioButton;
    taSpec1: TdxMemData;
    taSpec1Num: TIntegerField;
    taSpec1IdGoods: TIntegerField;
    taSpec1NameG: TStringField;
    taSpec1IM: TSmallintField;
    taSpec1SM: TStringField;
    taSpec1Quant: TFloatField;
    taSpec1PriceIn: TFloatField;
    taSpec1SumIn: TFloatField;
    taSpec1PriceIn0: TFloatField;
    taSpec1SumIn0: TFloatField;
    taSpec1PriceUch: TFloatField;
    taSpec1SumUch: TFloatField;
    taSpec1PriceInF: TFloatField;
    taSpec1SumInF: TFloatField;
    taSpec1PriceInF0: TFloatField;
    taSpec1SumInF0: TFloatField;
    taSpec1PriceUchF: TFloatField;
    taSpec1SumUchF: TFloatField;
    taSpec1QuantDif: TFloatField;
    taSpec1SumInDif: TFloatField;
    taSpec1SumInDif0: TFloatField;
    taSpec1SumUchDif: TFloatField;
    taSpec1Km: TFloatField;
    taSpec1TCard: TSmallintField;
    taSpec1NoCalc: TSmallintField;
    taSpec1SumInTO: TFloatField;
    taSpec1SumInTODif: TFloatField;
    taSpec1CType: TSmallintField;
    taSpecC1: TdxMemData;
    taSpecC1Num: TIntegerField;
    taSpecC1IdGoods: TIntegerField;
    taSpecC1NameG: TStringField;
    taSpecC1IM: TSmallintField;
    taSpecC1SM: TStringField;
    taSpecC1Quant: TFloatField;
    taSpecC1PriceIn: TFloatField;
    taSpecC1SumIn: TFloatField;
    taSpecC1PriceIn0: TFloatField;
    taSpecC1SumIn0: TFloatField;
    taSpecC1PriceUch: TFloatField;
    taSpecC1SumUch: TFloatField;
    taSpec1QuantFact: TFloatField;
    taSpecC1QuantFact: TFloatField;
    taSpecC1PriceInF: TFloatField;
    taSpecC1SumInF: TFloatField;
    taSpecC1PriceInF0: TFloatField;
    taSpecC1SumInF0: TFloatField;
    taSpecC1PriceUchF: TFloatField;
    taSpecC1SumUchF: TFloatField;
    taSpecC1QuantDif: TFloatField;
    taSpecC1SumInDif: TFloatField;
    taSpecC1SumInDif0: TFloatField;
    taSpecC1SumUchDif: TFloatField;
    taSpecC1Km: TFloatField;
    taSpecC1TCard: TSmallintField;
    taSpecC1SumInTO: TFloatField;
    taSpecC1SumInTODif: TFloatField;
    taSpecC1CType: TFloatField;
    taSpecC1NDSProc: TFloatField;
    taSpecC1SumNDS: TFloatField;
    taSpecCPriceIn0: TFloatField;
    taSpecCSumIn0: TFloatField;
    taSpecCPriceInF0: TFloatField;
    taSpecCSumInF0: TFloatField;
    taSpecCSumInDif0: TFloatField;
    taSpecCNDSProc: TFloatField;
    taSpecCSumNDS: TFloatField;
    taSpecPriceIn0: TFloatField;
    taSpecSumIn0: TFloatField;
    taSpecPriceInF0: TFloatField;
    taSpecSumInF0: TFloatField;
    taSpecSumInDif0: TFloatField;
    taSpec1NDSProc: TFloatField;
    taSpec1SumNDS: TFloatField;
    taSpecNDSProc: TFloatField;
    taSpecSumNDS: TFloatField;
    ViewInvPriceIn0: TcxGridDBColumn;
    ViewInvSumIn0: TcxGridDBColumn;
    ViewInvPriceInF0: TcxGridDBColumn;
    ViewInvSumInF0: TcxGridDBColumn;
    ViewInvSumInDif0: TcxGridDBColumn;
    ViewInvNDSProc: TcxGridDBColumn;
    ViewInvSumNDS: TcxGridDBColumn;
    ViewInvCPriceIn0: TcxGridDBColumn;
    ViewInvCSumIn0: TcxGridDBColumn;
    ViewInvCPriceInF0: TcxGridDBColumn;
    ViewInvCSumInF0: TcxGridDBColumn;
    ViewInvCSumInDif0: TcxGridDBColumn;
    ViewInvCNDSProc: TcxGridDBColumn;
    ViewInvCSumNDS: TcxGridDBColumn;
    ViewBCPRICEIN0: TcxGridDBColumn;
    ViewBCSUMIN0: TcxGridDBColumn;
    taSpec1IdGroup: TIntegerField;
    taRPrice0: TFloatField;
    taSpecC1IdGroup: TIntegerField;
    Label9: TLabel;
    N4: TMenuItem;
    acDetPosDocInv: TAction;
    acGetSumTOPos: TAction;
    PopupMenu2: TPopupMenu;
    N5: TMenuItem;
    quS: TpFIBDataSet;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acAddPosExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxLabel1Click(Sender: TObject);
    procedure taSpecBeforePost(DataSet: TDataSet);
    procedure ViewInvEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure ViewInvEditKeyPress(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
    procedure cxLabel7Click(Sender: TObject);
    procedure ViewInvDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure ViewInvDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure cxLabel8Click(Sender: TObject);
    procedure cxLabel2Click(Sender: TObject);
    procedure cxLabel9Click(Sender: TObject);
    procedure acSaveInvExecute(Sender: TObject);
    procedure ViewInvEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure taSpecQuantFactChange(Sender: TField);
    procedure taSpecPriceInFChange(Sender: TField);
    procedure taSpecSumInFChange(Sender: TField);
    procedure taSpecPriceUchFChange(Sender: TField);
    procedure taSpecSumUchFChange(Sender: TField);
    procedure Label3Click(Sender: TObject);
    procedure Label4Click(Sender: TObject);
    procedure Label6Click(Sender: TObject);
    procedure taSpecCBeforePost(DataSet: TDataSet);
    procedure cxButton3Click(Sender: TObject);
    procedure acAddListExecute(Sender: TObject);
    procedure acDelPosExecute(Sender: TObject);
    procedure acDelAllExecute(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure Memo1DblClick(Sender: TObject);
    procedure acCalc1Execute(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure taSpecCQuantFactChange(Sender: TField);
    procedure acEqualsExecute(Sender: TObject);
    procedure acRefreshGrExecute(Sender: TObject);
    procedure Excel1Click(Sender: TObject);
    procedure acMovePosExecute(Sender: TObject);
    procedure acFindWordExecute(Sender: TObject);
    procedure cxTextEdit3PropertiesChange(Sender: TObject);
    procedure cxTextEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure acFastSaveExecute(Sender: TObject);
    procedure RepInvUserFunction(const Name: String; p1, p2, p3: Variant;
      var Val: Variant);
    procedure taSpec1BeforePost(DataSet: TDataSet);
    procedure taSpecC1BeforePost(DataSet: TDataSet);
    procedure taSpec1PriceInFChange(Sender: TField);
    procedure taSpec1SumInFChange(Sender: TField);
    procedure taSpec1PriceInF0Change(Sender: TField);
    procedure taSpec1SumInF0Change(Sender: TField);
    procedure taSpecSumInTOChange(Sender: TField);
    procedure taSpecCSumInTOChange(Sender: TField);
    procedure acDetPosDocInvExecute(Sender: TObject);
    procedure acGetSumTOPosExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure prButton(bSt:Boolean);
  end;

Procedure prCalcBlInv(sBeg,sName:String;IdPos,iCode,iDate,iTCard,IdSkl:Integer;rQ:Real;IdM:INteger;taCalcB,taSpecC,taSpec:tdxMemData;Memo1:TcxMemo;iT:Byte);

var
  fmAddInv: TfmAddInv;
  bAdd:Boolean = False;
  iCol:INteger;

implementation

uses Un1, dmOffice, FCards, Goods, DMOReps, DocInv, Message, MainRnOffice,
  SelPerSkl, CardsMove, sumprops, DetPosSum;

{$R *.dfm}

procedure  TfmAddInv.prButton(bSt:Boolean);
begin
  cxButton1.Enabled:=bSt;
  cxButton2.Enabled:=bSt;
  delay(10);
end;



Procedure prCalcBlInv(sBeg,sName:String;IdPos,iCode,iDate,iTCard,IdSkl:Integer;rQ:Real;IdM:INteger;taCalcB,taSpecC,taSpec:tdxMemData;Memo1:TcxMemo;iT:Byte);
Var iTC,iPCount:INteger;
    QT1:TpFIBDataSet;
    rQ1,kBrutto,kM,MassaB:Real;
    iMain,iMax:INteger;
    sM:String;
    Par:Variant;
    rQb:Real;
//    NameGr:String;
//    Id_Group:INteger;
    Qr,Qf,Pr1,Pr11,Pr2,Pr22,Sum1,Sum11,Sum2,Sum21:Real;
    bCalc:Boolean;
    Pr0,PrF0,Sum0,SumF0:Real;

  Procedure VarsToZero;
  begin
    Qr:=0;Qf:=0;Pr1:=0;Pr11:=0;Pr2:=0;Pr22:=0;Sum1:=0;Sum11:=0;Sum2:=0;Sum21:=0;
    Pr0:=0;PrF0:=0;Sum0:=0;SumF0:=0;
  end;

  Procedure VarsToSpec;
  begin
    Qr:=taSpecC.FieldbyName('Quant').AsFloat;
    Qf:=taSpecC.FieldbyName('QuantFact').AsFloat;
    Pr1:=taSpecC.FieldbyName('PriceIn').AsFloat;
    Pr11:=taSpecC.FieldbyName('PriceUch').AsFloat;
    Pr2:=taSpecC.FieldbyName('PriceInF').AsFloat;
    Pr22:=taSpecC.FieldbyName('PriceUchF').AsFloat;
    Sum1:=taSpecC.FieldbyName('SumIn').AsFloat;
    Sum11:=taSpecC.FieldbyName('SumUch').AsFloat;
    Sum2:=taSpecC.FieldbyName('SumInF').AsFloat;
    Sum21:=taSpecC.FieldbyName('SumUchF').AsFloat;

    Pr0:=taSpecC.FieldbyName('PriceIn0').AsFloat;
    PrF0:=taSpecC.FieldbyName('PriceInF0').AsFloat;
    Sum0:=taSpecC.FieldbyName('SumIn0').AsFloat;
    SumF0:=taSpecC.FieldbyName('SumInF0').AsFloat;
  end;

begin
  sBeg:=sBeg+'    ';

  with dmO do
//  with fmAddInv do
  begin
    //���
    if iTCard=1 then bCalc:=True else bCalc:=False;
    if fCalcBlasCard(IdSkl,iCode) then bCalc:=False; //���� �� ������������ �� ��� �����

    if bCalc then
    begin
      iTC:=0; iPCount:=0; MassaB:=1;
      quFindTCard.Active:=False;
      quFindTCard.ParamByName('IDCARD').AsInteger:=iCode;
      quFindTCard.ParamByName('IDATE').AsDateTime:=iDate;
      quFindTCard.Active:=True;
      if quFindTCard.RecordCount>0 then
      begin
        iTC:=quFindTCardID.AsInteger;
        iPCount:=quFindTCardPCOUNT.AsInteger;
        MassaB:=quFindTCardPVES.AsFloat/1000;
        if prFindMT(IdM)=1 then MassaB:=1; // ���� ���� ����� �������
        prFindSM(IdM,sM,iMain); //��� �������� �������� ������� ���������
        prWH(sBeg+sName+' ('+INtToStr(iCode)+'). �� ����. ���-��: '+FloatToStr(RoundEx(rQ*1000)/1000)+' '+sM,Memo1);
      end else
      begin
        inc(iErr);
        sErr:=sErr+IntToStr(iCode)+' '+sName+';';
        prWH(SBeg+'������: �� �� �������.( ���-'+INtToStr(iCode)+')',Memo1);
      end;
      quFindTCard.Active:=False;
      if iTC>0 then
      begin
        QT1:=TpFIBDataSet.Create(Owner);
        QT1.Active:=False;
        QT1.Database:=OfficeRnDb;
        QT1.Transaction:=trSel;
        QT1.SelectSQL.Clear;
        QT1.SelectSQL.Add('SELECT CS.ID,CS.IDCARD,CS.CURMESSURE,CS.NETTO,CS.BRUTTO,CS.KNB,');
        QT1.SelectSQL.Add('CD.NAME,CD.TCARD');
        QT1.SelectSQL.Add('FROM OF_CARDSTSPEC CS');
        QT1.SelectSQL.Add('left join of_cards cd on cd.ID=CS.IDCARD');
        QT1.SelectSQL.Add('where IDC='+IntToStr(iCode)+' and IDT='+IntToStr(iTC));
        QT1.SelectSQL.Add('ORDER BY CS.ID');
        QT1.Active:=True;

        QT1.First;
        while not QT1.Eof do
        begin
          rQ1:=QT1.FieldByName('NETTO').AsFloat;
          kM:=prFindKM(QT1.FieldByName('CURMESSURE').AsInteger);
          rQ1:=rQ1*kM;//��������� � �������� �������
          prFindSM(QT1.FieldByName('CURMESSURE').AsInteger,sM,iMain); //��� �������� �������� ������� ���������
          //���� ������ �� ������ ����
          kBrutto:=prFindBrutto(QT1.FieldByName('IDCARD').AsInteger,iDate);
          rQ1:=rQ1*(100+kBrutto)/100; //��� ����� � ������
          rQ1:=rQ1/iPCount; //��� ����� �� 1-� ������ ���� �������� �� �������

          rQ1:=rQ1*rQ/MassaB; //��� ����� �� ��� ���-�� ������

//          prWH(sBeg+QT1.FieldByName('NAME').AsString+' ('+QT1.FieldByName('IDCARD').AsString+'). �� ����. ���-��: '+FloatToStr(RoundEx(rQ1*1000)/1000)+' '+sM);
          prCalcBlInv(sBeg,QT1.FieldByName('NAME').AsString,IdPos,QT1.FieldByName('IDCARD').AsInteger,iDate,QT1.FieldByName('TCARD').AsInteger,IdSkl,rQ1,iMain,dmORep.taCalcB1,fmAddInv.taSpecC1,fmAddInv.taSpec1,Memo1,iT);

          QT1.Next;
        end;
        QT1.Active:=False;
        QT1.Free;
      end;
    end else
    begin
      //��������� � �������� ������� �� ������ ������ ����� ������ ��� - �� ����
      kM:=prFindKM(IdM);
      prFindSM(IdM,sM,iMain); //��� �������� �������� ������� ���������
      rQ:=rQ*kM;              //������ ������� - ��� ������ �� �����������

      prWH(sBeg+sName+' ('+IntToStr(iCode)+'). �����.  ���-��: '+FloatToStr(RoundEx(rQ*1000)/1000)+' '+sM,Memo1);

      //������ ���� ��� � �������� � � ������
//      prCalcSave;
      par := VarArrayCreate([0,1], varInteger);
      par[0]:=IdPos;
      par[1]:=iCode;
      if taCalcB.Locate('ID;IDCARD',par,[]) then
      begin
        rQb:=taCalcB.fieldByName('QUANTC').AsFloat+rQ;
        taCalcB.edit;
        taCalcB.fieldByName('QUANTC').AsFloat:=rQb;
        taCalcB.fieldByName('PRICEIN').AsFloat:=0;
        taCalcB.fieldByName('SUMIN').AsFloat:=0;
        taCalcB.fieldByName('PRICEIN0').AsFloat:=0;
        taCalcB.fieldByName('SUMIN0').AsFloat:=0;
        taCalcB.fieldByName('IM').AsInteger:=iMain;
        taCalcB.fieldByName('SM').AsString:=sM;
        taCalcB.fieldByName('SB').AsString:='';
        taCalcB.Post;
      end else
      begin
        taCalcB.Append;
        taCalcB.fieldByName('ID').AsInteger:=IdPos;
        taCalcB.fieldByName('CODEB').AsInteger:=taSpec.FieldbyName('IdGoods').AsInteger;
        taCalcB.fieldByName('NAMEB').AsString:=taSpec.FieldbyName('NameG').AsString;
        taCalcB.fieldByName('QUANT').AsFloat:=taSpec.FieldbyName('QuantFact').AsFloat;
        taCalcB.fieldByName('PRICEOUT').AsFloat:=0;
        taCalcB.fieldByName('SUMOUT').AsFloat:=0;
        taCalcB.fieldByName('IDCARD').AsInteger:=iCode;
        taCalcB.fieldByName('NAMEC').AsString:=sName;
        taCalcB.fieldByName('QUANTC').AsFloat:=rQ;
        taCalcB.fieldByName('PRICEIN').AsFloat:=0;
        taCalcB.fieldByName('SUMIN').AsFloat:=0;
        taCalcB.fieldByName('PRICEIN0').AsFloat:=0;
        taCalcB.fieldByName('SUMIN0').AsFloat:=0;
        taCalcB.fieldByName('IM').AsInteger:=iMain;
        taCalcB.fieldByName('SM').AsString:=sM;
        taCalcB.fieldByName('SB').AsString:='';
        taCalcB.Post;
      end;

      if taSpecC.Locate('IdGoods',iCode,[]) then
      begin //�������������
        VarsToSpec; //������� �������� ��������
        taSpecC.Edit;
        if iT=0 then //����������� ������� (�� ����� � �������������� ���������)
        begin
          taSpecC.FieldbyName('QuantFact').AsFloat:=rQ+Qf;
          taSpecC.FieldbyName('SumInF').AsFloat:=(rQ+Qf)*Pr2;
          taSpecC.FieldbyName('SumInF0').AsFloat:=(rQ+Qf)*PrF0;
        end;
        if iT=1 then //��� ����� � �������������� ���������
        begin
          taSpecC.FieldbyName('Quant').AsFloat:=Qr-rQ; //�������� ���������� �����
          taSpecC.FieldbyName('SumIn').AsFloat:=(Qr-rQ)*Pr1;
          taSpecC.FieldbyName('SumIn0').AsFloat:=(Qr-rQ)*Pr0;
        end;
        taSpecC.Post;
      end else
      begin
        VarsToZero;

        iMax:=1;
        if not taSpecC.Eof then
        begin
          taSpecC.Last;
          iMax:=taSpecC.FieldbyName('Num').AsInteger+1;
        end;

        taSpecC.Append;
        taSpecC.FieldbyName('Num').AsInteger:=iMax;

        taSpecC.FieldbyName('IdGoods').AsInteger:=iCode;
        taSpecC.FieldbyName('NameG').AsString:=sName;
        taSpecC.FieldbyName('IM').AsInteger:=iMain;
        taSpecC.FieldbyName('SM').AsString:=sM;

//        prFindGroup(iCode,NameGr,Id_Group); //�������� ������
//        taSpecC.FieldbyName('Id_Group').AsInteger:=Id_Group;
//        taSpecC.FieldbyName('NameGr').AsString:=NameGr;

        if iT=0 then
        begin
          taSpecC.FieldbyName('Quant').AsFloat:=0;
          taSpecC.FieldbyName('QuantFact').AsFloat:=rQ+Qf;
        end;

        if iT=1 then
        begin
          taSpecC.FieldbyName('Quant').AsFloat:=-rQ;
          taSpecC.FieldbyName('QuantFact').AsFloat:=0;
        end;

        taSpecC.FieldbyName('PriceIn').AsFloat:=0;
        taSpecC.FieldbyName('SumIn').AsFloat:=0;
        taSpecC.FieldbyName('PriceUch').AsFloat:=0;
        taSpecC.FieldbyName('SumUch').AsFloat:=0;
        taSpecC.FieldbyName('PriceIn0').AsFloat:=0;
        taSpecC.FieldbyName('SumIn0').AsFloat:=0;

        taSpecC.FieldbyName('PriceInF').AsFloat:=0;
        taSpecC.FieldbyName('SumInF').AsFloat:=0;
        taSpecC.FieldbyName('PriceUchF').AsFloat:=0;
        taSpecC.FieldbyName('SumUchF').AsFloat:=0;
        taSpecC.FieldbyName('PriceInF0').AsFloat:=0;
        taSpecC.FieldbyName('SumInF0').AsFloat:=0;

        taSpecC.FieldbyName('Km').AsFloat:=1;
        taSpecC.FieldbyName('TCard').AsInteger:=0;
        taSpecC.Post;

      end;
    end;
  end;
end;



procedure TfmAddInv.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  PageControl1.Align:=alClient;
  ViewInv.RestoreFromIniFile(CurDir+GridIni);
  ViewInvC.RestoreFromIniFile(CurDir+GridIni);
end;

procedure TfmAddInv.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if cxButton1.Enabled=True then
  begin
    if MessageDlg('�� ������������� ������ ����� �� ���������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      ViewInv.StoreToIniFile(CurDir+GridIni,False);
      ViewInvC.StoreToIniFile(CurDir+GridIni,False);
      Action := caHide;
    end
    else  Action := caNone;
  end else
  begin
    if cxButton2.Enabled=True then Action := caHide
    else  Action := caNone;
  end;
end;

procedure TfmAddInv.acAddPosExecute(Sender: TObject);
Var iMax:INteger;
begin
//�������� �������
  if cxButton1.Enabled then
  begin
    iMax:=1;
    ViewInv.BeginUpdate;

    taSpec1.First;
    if not taSpec1.Eof then
    begin
      taSpec1.Last;
      iMax:=taSpec1Num.AsInteger+1;
    end;

    taSpec1.Append;
    taSpec1Num.AsInteger:=iMax;
    taSpec1IdGoods.AsInteger:=0;
    taSpec1NameG.AsString:='';
    taSpec1IM.AsInteger:=0;
    taSpec1SM.AsString:='';
    taSpec1Quant.AsFloat:=0;
    taSpec1PriceIn0.AsFloat:=0;
    taSpec1SumIn0.AsFloat:=0;
    taSpec1PriceIn.AsFloat:=0;
    taSpec1SumIn.AsFloat:=0;
    taSpec1PriceUch.AsFloat:=0;
    taSpec1SumUch.AsFloat:=0;
    taSpec1QuantFact.AsFloat:=0;
    taSpec1PriceInF0.AsFloat:=0;
    taSpec1SumInF0.AsFloat:=0;
    taSpec1PriceInF.AsFloat:=0;
    taSpec1SumInF.AsFloat:=0;
    taSpec1PriceUchF.AsFloat:=0;
    taSpec1SumUchF.AsFloat:=0;
    taSpec1Km.AsFloat:=0;
    taSpec1TCard.AsInteger:=0;
    taSpec1NoCalc.AsInteger:=0;
    taSpec1CType.AsInteger:=1;
    taSpec1QuantDif.AsFloat:=0;
    taSpec1SumInDif.AsFloat:=0;
    taSpec1SumInDif0.AsFloat:=0;
    taSpec1SumUchDif.AsFloat:=0;
    taSpec1SumInTO.AsFloat:=0;
    taSpec1SumInTODif.AsFloat:=0;
    taSpec1NDSProc.AsFloat:=0;
    taSpec1SumNDS.AsFloat:=0;
    taSpec1IdGroup.AsInteger:=0;
    taSpec1NDSProc.AsFloat:=0;
    taSpec1.Post;
    ViewInv.EndUpdate;
    GridInv.SetFocus;

    ViewInvNameG.Options.Editing:=True;
    ViewInvNameG.Focused:=True;

    prRowFocus(ViewInv);
  end;
end;

procedure TfmAddInv.FormShow(Sender: TObject);
begin
  Memo1.Clear;
  PageControl1.ActivePageIndex:=0;
  iCol:=0;
  ViewInvNameG.Options.Editing:=False;
  cxTextEdit3.Text:='';
end;

procedure TfmAddInv.cxLabel1Click(Sender: TObject);
begin
  acAddPos.Execute;
end;

procedure TfmAddInv.taSpecBeforePost(DataSet: TDataSet);
begin
  taSpecQuantDif.AsFloat:=taSpecQuantFact.AsFloat-taSpecQuant.AsFloat;
  taSpecSumInDif.AsFloat:=taSpecSumInF.AsFloat-taSpecSumIn.AsFloat;
  taSpecSumUchDif.AsFloat:=taSpecSumUchF.AsFloat-taSpecSumUch.AsFloat;
//  taSpecSumInTODif.AsFloat:=taSpecSumInTO.AsFloat-taSpecSumIn.AsFloat;
  taSpecSumInTODif.AsFloat:=taSpecSumInF.AsFloat-taSpecSumInTO.AsFloat;
end;

procedure TfmAddInv.ViewInvEditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
Var iCode:Integer;
    Km,rProc:Real;
    sName:String;
begin
  with dmO do
  begin

    if (Key=$0D) then
    begin
      if ViewInv.Controller.FocusedColumn.Name='ViewInvIdGoods' then
      begin
        iCode:=VarAsType(AEdit.EditingValue, varInteger);

        quFCard.Active:=False;
        quFCard.SelectSQL.Clear;
        quFCard.SelectSQL.Add('SELECT first 50 ID,NAME,IMESSURE,PARENT,TCARD,INDS,LASTPRICEOUT,CATEGORY');
        quFCard.SelectSQL.Add('FROM OF_CARDS');
        quFCard.SelectSQL.Add('where ID='+IntToStr(iCode));
        quFCard.SelectSQL.Add('and IACTIVE>0');
        quFCard.SelectSQL.Add('and PARENT in (SELECT ID_CLASSIF FROM RCLASSIF WHERE RIGHTS=0 AND ID_PERSONAL='+its(Person.Id)+')');
        quFCard.Active:=True;

        if quFCard.RecordCount=1 then
        begin
          ViewInv.BeginUpdate;
          taSpec1.Edit;
          taSpec1IdGoods.AsInteger:=iCode;
          taSpec1NameG.AsString:=quFCardNAME.AsString;
          taSpec1IM.AsInteger:=quFCardIMESSURE.AsInteger;
          taSpec1SM.AsString:=prFindKNM(quFCardIMESSURE.AsInteger,Km);
          taSpec1Km.AsFloat:=Km;
          taSpec1TCard.AsInteger:=quFCardTCARD.AsInteger;
          taSpec1NoCalc.AsInteger:=0;

          taSpec1IdGroup.AsInteger:=quFCardPARENT.AsInteger;
//          taSpecNameGr.AsString:=prFindGrN(quFCardPARENT.AsInteger);

          taSpec1Quant.AsFloat:=0;
          taSpec1PriceIn.AsFloat:=0;
          taSpec1SumIn.AsFloat:=0;
          taSpec1PriceUch.AsFloat:=0;
          taSpec1SumUch.AsFloat:=0;
          taSpec1QuantFact.AsFloat:=0;
          taSpec1PriceInF.AsFloat:=0;
          taSpec1SumInF.AsFloat:=0;
          taSpec1PriceUchF.AsFloat:=0;
          taSpec1SumUchF.AsFloat:=0;
          taSpec1CType.AsInteger:=quFCardCATEGORY.AsInteger;
          taSpec1PriceIn0.AsFloat:=0;
          taSpec1SumIn0.AsFloat:=0;
          taSpec1PriceInF0.AsFloat:=0;
          taSpec1SumInF0.AsFloat:=0;
          prGetNDS(quFCardINDS.AsInteger,rProc);
          taSpec1NDSProc.AsFloat:=rProc;
          taSpec1.Post;

          ViewInv.EndUpdate;
        end;
      end;
      if ViewInv.Controller.FocusedColumn.Name='ViewInvNameG' then
      begin
        sName:=VarAsType(AEdit.EditingValue, varString);

        fmFCards.ViewFCards.BeginUpdate;
        quFCard.Active:=False;
        quFCard.SelectSQL.Clear;
        quFCard.SelectSQL.Add('SELECT first 100 ID,NAME,IMESSURE,PARENT,TCARD,INDS,LASTPRICEOUT,CATEGORY');
        quFCard.SelectSQL.Add('FROM OF_CARDS');
        quFCard.SelectSQL.Add('where UPPER(NAME) like ''%'+AnsiUpperCase(sName)+'%''');
        quFCard.SelectSQL.Add('and IACTIVE>0');
        quFCard.SelectSQL.Add('and PARENT in (SELECT ID_CLASSIF FROM RCLASSIF WHERE RIGHTS=0 AND ID_PERSONAL='+its(Person.Id)+')');
        quFCard.SelectSQL.Add('Order by NAME');

        quFCard.Active:=True;

        bAdd:=True;

        quFCard.Locate('NAME',sName,[loCaseInsensitive, loPartialKey]);
        prRowFocus(fmFCards.ViewFCards);
        fmFCards.ViewFCards.EndUpdate;

        //�������� ����� ������ � ����� ������
        fmFCards.ShowModal;
        if fmFCards.ModalResult=mrOk then
        begin
          if quFCard.RecordCount>0 then
          begin
            ViewInv.BeginUpdate;
            taSpec1.Edit;
            taSpec1IdGoods.AsInteger:=quFCardID.AsInteger;
            taSpec1NameG.AsString:=quFCardNAME.AsString;
            taSpec1IM.AsInteger:=quFCardIMESSURE.AsInteger;
            taSpec1SM.AsString:=prFindKNM(quFCardIMESSURE.AsInteger,Km);
            taSpec1Km.AsFloat:=Km;
            taSpec1TCard.AsInteger:=quFCardTCARD.AsInteger;
            taSpec1NoCalc.AsInteger:=0;
            taSpec1IdGroup.AsInteger:=quFCardPARENT.AsInteger;
//            taSpec1NameGr.AsString:=prFindGrN(quFCardPARENT.AsInteger);

            taSpec1Quant.AsFloat:=0;
            taSpec1PriceIn.AsFloat:=0;
            taSpec1SumIn.AsFloat:=0;
            taSpec1PriceUch.AsFloat:=0;
            taSpec1SumUch.AsFloat:=0;
            taSpec1QuantFact.AsFloat:=0;
            taSpec1PriceInF.AsFloat:=0;
            taSpec1SumInF.AsFloat:=0;
            taSpec1PriceUchF.AsFloat:=0;
            taSpec1SumUchF.AsFloat:=0;
            taSpec1CType.AsInteger:=quFCardCATEGORY.AsInteger;

            taSpec1PriceIn0.AsFloat:=0;
            taSpec1SumIn0.AsFloat:=0;
            taSpec1PriceInF0.AsFloat:=0;
            taSpec1SumInF0.AsFloat:=0;
            prGetNDS(quFCardINDS.AsInteger,rProc);
            taSpec1NDSProc.AsFloat:=rProc;

            taSpec1.Post;
            ViewInv.EndUpdate;
            AEdit.SelectAll;
          end;
        end;
        bAdd:=False;
      end;
      ViewInvQuantFact.Focused:=True;
    end else
      if ViewInv.Controller.FocusedColumn.Name='ViewInvIdGoods' then
        if fTestKey(Key)=False then
          if taSpec.State in [dsEdit,dsInsert] then taSpec.Cancel;
  end;
end;

procedure TfmAddInv.ViewInvEditKeyPress(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
Var Km,rProc:Real;
    sName:String;
begin
  if bAdd then exit;
  with dmO do
  begin
    if ViewInv.Controller.FocusedColumn.Name='ViewInvNameG' then
    begin
      sName:=VarAsType(AEdit.EditingValue ,varString)+Key;
//      Label2.Caption:=sName;
      if length(sName)>2 then
      begin
        quFCard.Active:=False;
        quFCard.SelectSQL.Clear;
        quFCard.SelectSQL.Add('SELECT  first 2 ID,NAME,IMESSURE,PARENT,TCARD,INDS,LASTPRICEOUT,CATEGORY');
        quFCard.SelectSQL.Add('FROM OF_CARDS');
        quFCard.SelectSQL.Add('where UPPER(NAME) like ''%'+AnsiUpperCase(sName)+'%''');
        quFCard.SelectSQL.Add('and IACTIVE>0');
        quFCard.SelectSQL.Add('and PARENT in (SELECT ID_CLASSIF FROM RCLASSIF WHERE RIGHTS=0 AND ID_PERSONAL='+its(Person.Id)+')');
        quFCard.SelectSQL.Add('Order by NAME');
        quFCard.Active:=True;

        if quFCard.RecordCount=1 then
        begin
          ViewInv.BeginUpdate;
          taSpec1.Edit;
          taSpec1IdGoods.AsInteger:=quFCardID.AsInteger;
          taSpec1NameG.AsString:=quFCardNAME.AsString;
          taSpec1IM.AsInteger:=quFCardIMESSURE.AsInteger;
          taSpec1SM.AsString:=prFindKNM(quFCardIMESSURE.AsInteger,Km);
          taSpec1Km.AsFloat:=Km;
          taSpec1TCard.AsInteger:=quFCardTCARD.AsInteger;
          taSpec1NoCalc.AsInteger:=0;
          taSpec1IdGroup.AsInteger:=quFCardPARENT.AsInteger;
//          taSpec1NameGr.AsString:=prFindGrN(quFCardPARENT.AsInteger);

          taSpec1Quant.AsFloat:=0;
          taSpec1PriceIn.AsFloat:=0;
          taSpec1SumIn.AsFloat:=0;
          taSpec1PriceUch.AsFloat:=0;
          taSpec1SumUch.AsFloat:=0;
          taSpec1QuantFact.AsFloat:=0;
          taSpec1PriceInF.AsFloat:=0;
          taSpec1SumInF.AsFloat:=0;
          taSpec1PriceUchF.AsFloat:=0;
          taSpec1SumUchF.AsFloat:=0;
          taSpec1CType.AsInteger:=quFCardCATEGORY.AsInteger;

          taSpec1PriceIn0.AsFloat:=0;
          taSpec1SumIn0.AsFloat:=0;
          taSpec1PriceInF0.AsFloat:=0;
          taSpec1SumInF0.AsFloat:=0;
          prGetNDS(quFCardINDS.AsInteger,rProc);
          taSpec1NDSProc.AsFloat:=rProc;
          taSpec1.Post;
          ViewInv.EndUpdate;
          AEdit.SelectAll;
          ViewInvNameG.Options.Editing:=False;
          ViewInvNameG.Focused:=True;
          Key:=#0;
        end;
      end;
    end;
  end;//}
end;

procedure TfmAddInv.cxLabel7Click(Sender: TObject);
begin
//  bAddSpecInv:=True;
//  try  fmGoods.Close; delay(10); except end;
//  fmGoods.Show;
  acAddList.Execute;
end;

procedure TfmAddInv.ViewInvDragOver(Sender, Source: TObject; X, Y: Integer;
  State: TDragState; var Accept: Boolean);
begin
  Accept:=False;
  if bDInv then  Accept:=True;
end;

procedure TfmAddInv.ViewInvDragDrop(Sender, Source: TObject; X,
  Y: Integer);
Var iCo:Integer;
    i,j: Integer;
    iNum: Integer;
    Rec:TcxCustomGridRecord;
    Km:Real;
    iMax:Integer;
begin
  if bDInv then
  begin
    ResetAddVars;
    iCo:=fmGoods.ViewGoods.Controller.SelectedRecordCount;
    if iCo>0 then
    begin
      if MessageDlg('�� ������������� ������ �������� ��������� ������� ('+IntToStr(iCo)+' ��.) � ��������������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        with dmO do
        begin
          Memo1.Clear;
          Memo1.Lines.Add('����� .. ���� ���������� �������.');
          ViewInv.BeginUpdate;

          iMax:=1;
          taSpec1.First;
          if not taSpec1.Eof then
          begin
            taSpec1.Last;
            iMax:=taSpec1Num.AsInteger+1;
          end;

          for i:=0 to fmGoods.ViewGoods.Controller.SelectedRecordCount-1 do
          begin
            Rec:=fmGoods.ViewGoods.Controller.SelectedRecords[i];

            for j:=0 to Rec.ValueCount-1 do
            begin
              if fmGoods.ViewGoods.Columns[j].Name='ViewGoodsID' then break;
            end;

            iNum:=Rec.Values[j];
          //��� ��� - ����������
            with dmO do
            begin
              if quCardsSel.Locate('ID',iNum,[]) then
              begin
                try
                  if taSpec1.Locate('IdGoods',iNum,[])=False then
                  begin
                    taSpec1.Append;
                    taSpec1Num.AsInteger:=iMax;
                    taSpec1IdGoods.AsInteger:=quCardsSelID.AsInteger;
                    taSpec1NameG.AsString:=quCardsSelNAME.AsString;
                    taSpec1IM.AsInteger:=quCardsSelIMESSURE.AsInteger;
                    taSpec1SM.AsString:=prFindKNM(quCardsSelIMESSURE.AsInteger,Km);
                    taSpec1Quant.AsFloat:=0;
                    taSpec1PriceIn.AsFloat:=0;
                    taSpec1SumIn.AsFloat:=0;
                    taSpec1PriceUch.AsFloat:=0;
                    taSpec1SumUch.AsFloat:=0;
                    taSpec1QuantFact.AsFloat:=0;
                    taSpec1PriceInF.AsFloat:=0;
                    taSpec1SumInF.AsFloat:=0;
                    taSpec1PriceUchF.AsFloat:=0;
                    taSpec1SumUchF.AsFloat:=0;
                    taSpec1Km.AsFloat:=Km;
                    taSpec1TCard.AsInteger:=quCardsSelTCard.AsInteger;
                    taSpec1NoCalc.AsInteger:=0;
                    taSpec1IdGroup.AsInteger:=quCardsSelPARENT.AsInteger;
//                    taSpec1NameGr.AsString:=prFindGrN(quCardsSelPARENT.AsInteger);
                    taSpec1CType.AsInteger:=quCardsSelCATEGORY.AsInteger;
                    taSpec1PriceIn0.AsFloat:=0;
                    taSpec1SumIn0.AsFloat:=0;
                    taSpec1PriceInF0.AsFloat:=0;
                    taSpec1SumInF0.AsFloat:=0;
                    taSpec1NDSProc.AsFloat:=quCardsSelPROC.AsFloat;
                    taSpec1.Post;
                    delay(10);
                    inc(iMax);
                  end;
                except
                end;
              end;
            end;
          end;
          ViewInv.EndUpdate;
          Memo1.Lines.Add('���������� ��.');
        end;
      end;
    end;
  end;
end;

procedure TfmAddInv.cxLabel8Click(Sender: TObject);
Var iMax:INteger;
    bAdd:Boolean;
//    S1,S2:String;
    iM,iC:INteger;
begin
  with dmO do
  begin
    if MessageDlg('���������� ����� ������ ���������� ��� ������ ��������������. ��������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      prButton(False);

      Memo1.Clear;
      Memo1.Lines.Add('����� ... ���� ������������ � ���������� ������.'); delay(10);

      ViewInv.BeginUpdate;
      iMax:=1;
      taSpec1.First;
      if not taSpec1.Eof then
      begin
        taSpec1.Last;
        iMax:=taSpec1Num.AsInteger+1;
      end;

      quAllCards.Active:=False;
      quAllCards.Active:=True;

      PBar1.Properties.Min:=0;
      PBar1.Properties.Max:=quAllCards.RecordCount;
      PBar1.Position:=0;
      PBar1.Visible:=True;

      if quAllCards.RecordCount>100 then iM:=Trunc(quAllCards.RecordCount/100)
      else iM:=1;

      iC:=0;

      quAllCards.First;
      while not quAllCards.Eof do
      begin
//        if quAllCardsTCARD.AsInteger=1 then
//        begin
          //���� ���� �������������� �� ����� �� ��������
        quGDS.Active:=False;
        quGDS.ParamByName('IDCARD').AsInteger:=quAllCardsID.AsInteger;
        quGDS.ParamByName('IDSKL').AsInteger:=cxLookupComboBox1.EditValue;
        quGDS.Active:=True;
        if quGDS.RecordCount>0 then bAdd:=True else bAdd:=False;
        quGDS.Active:=False;
//        end;
        if bAdd then
        begin
          if taSpec1.Locate('IdGoods',quAllCardsID.AsInteger,[])=False then
          begin
//            prFind2group(quAllCardsPARENT.AsInteger,S1,S2);

            taSpec1.Append;
            taSpec1Num.AsInteger:=iMax;
            taSpec1IdGoods.AsInteger:=quAllCardsID.AsInteger;
            taSpec1NameG.AsString:=quAllCardsNAME.AsString;
            taSpec1IM.AsInteger:=quAllCardsIMESSURE.AsInteger;
            taSpec1SM.AsString:=quAllCardsNAMESHORT.AsString;
            taSpec1Quant.AsFloat:=0;
            taSpec1PriceIn.AsFloat:=0;
            taSpec1SumIn.AsFloat:=0;
            taSpec1PriceUch.AsFloat:=0;
            taSpec1SumUch.AsFloat:=0;
            taSpec1QuantFact.AsFloat:=0;
            taSpec1PriceInF.AsFloat:=0;
            taSpec1SumInF.AsFloat:=0;
            taSpec1PriceUchF.AsFloat:=0;
            taSpec1SumUchF.AsFloat:=0;
            taSpec1Km.AsFloat:=quAllCardsKOEF.AsFloat;
            taSpec1TCard.AsInteger:=quAllCardsTCARD.AsInteger;
            taSpec1NoCalc.AsInteger:=0;
            taSpec1IdGroup.AsInteger:=quAllCardsPARENT.AsInteger;
//            taSpec1NameGr.AsString:=S1;
//            taSpec1NameGr2.AsString:=S2;
            taSpec1CType.AsInteger:=quAllCardsCATEGORY.AsInteger;
            taSpec1PriceIn0.AsFloat:=0;
            taSpec1SumIn0.AsFloat:=0;
            taSpec1PriceInF0.AsFloat:=0;
            taSpec1SumInF0.AsFloat:=0;
            taSpec1NDSProc.AsFloat:=quAllCardsPROC.AsFloat;
            taSpec1.Post;
            inc(iMax);
          end;
        end;
        quAllCards.Next; delay(10);
        inc(iC);
        if iC mod iM = 0 then
        begin
          PBar1.Position:=iC;
          delay(10);
        end;
      end;
      Delay(100);
      PBar1.Visible:=False;
      quAllCards.Active:=False;
      Memo1.Lines.Add('���������� ��.');
      ViewInv.EndUpdate;

      prButton(True);
    end;
  end;
end;

procedure TfmAddInv.cxLabel2Click(Sender: TObject);
begin
  acDelPos.Execute;
end;

procedure TfmAddInv.cxLabel9Click(Sender: TObject);
begin
{  if MessageDlg('�������� ��������������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
    taSpec.First;
    while not taSpec.Eof do taSpec.Delete;
  end;}
  acDelAll.Execute;
end;

procedure TfmAddInv.acSaveInvExecute(Sender: TObject);
Var Idh,iM1:Integer;
    StrWk:String;
    rQf,Km:Real;
    iDate,Ids,iMax:Integer;
    Qr,Qf,Pr1,Pr11,Pr2,Pr22,Sum1,Sum11,Sum2,Sum21,Sum3,Pr0,Sum0,PrF0,SumF0:Real;
    rQ1:Real;
    IdM:Integer;
    rSumTO,rPrice,rPriceUch,rPrice0:Real;
    bCalc:Boolean;
    IdSkl:Integer;
// rMessure:Real;
    QZer:Byte;
    iM,iC:INteger;
    SumTR,SumTF,SumTD:Real;
    iSS:INteger;

  Procedure VarsToZero;
  begin
    Qr:=0;Qf:=0;Pr1:=0;Pr11:=0;Pr2:=0;Pr22:=0;Sum1:=0;Sum11:=0;Sum2:=0;Sum21:=0; Pr0:=0; Sum0:=0;  PrF0:=0; SumF0:=0;
  end;

  Procedure VarsToSpec;
  begin
    Qr:=taSpecCQuant.AsFloat;
    Qf:=taSpecCQuantFact.AsFloat;
    Pr1:=taSpecCPriceIn.AsFloat;
    Pr11:=taSpecCPriceUch.AsFloat;
    Pr2:=taSpecCPriceInF.AsFloat;
    Pr22:=taSpecCPriceUchF.AsFloat;
    Sum1:=taSpecCSumIn.AsFloat;
    Sum11:=taSpecCSumUch.AsFloat;
    Sum2:=taSpecCSumInF.AsFloat;
    Sum21:=taSpecCSumUchF.AsFloat;
    Pr0:=taSpecCPriceIn0.AsFloat;
    Sum0:=taSpecCSumIn0.AsFloat;
    PrF0:=taSpecCPriceInF0.AsFloat;
    SumF0:=taSpecCSumInF0.AsFloat;
  end;

begin
  //��������� ��������������
  if cxButton1.Enabled=False then exit;

  Memo1.Clear;
  prButton(False);

  prWH('����� ... ���� ���������� ������.',Memo1); delay(10);

  iDate:=Trunc(date);
  if cxDateEdit1.Date>3000 then iDate:=Trunc(cxDateEdit1.Date);

  with dmO do
  with dmORep do
  begin
    if taSpec1.State in [dsEdit,dsInsert] then taSpec1.Post;

    //������� ������ ������
    // ������ ����� ���������� ��������� � �������� ������������ �� ������ ������.

    prAllViewOff;
    IdSkl:=cxLookupComboBox1.EditValue;
    iSS:=prISS(IdSkl);

    //�������� ���������
    prWH('  ���������.',Memo1); delay(10);

    IDH:=cxTextEdit1.Tag;
    if cxTextEdit1.Tag=0 then
    begin
      IDH:=GetId('InvH');
      cxTextEdit1.Tag:=IDH;
      if cxTextEdit1.Text=prGetNum(3,0) then prGetNum(3,1); //��������
    end;

    quDocsInvId.Active:=False;

    quDocsInvId.ParamByName('IDH').AsInteger:=IDH;
    quDocsInvId.Active:=True;

    quDocsInvId.First;
    if quDocsInvId.RecordCount=0 then quDocsInvId.Append else quDocsInvId.Edit;

    quDocsInvIdID.AsInteger:=IDH;
    quDocsInvIdDATEDOC.AsDateTime:=cxDateEdit1.Date;
    quDocsInvIdNUMDOC.AsString:=cxTextEdit1.Text;
    quDocsInvIdIDSKL.AsInteger:=cxLookupComboBox1.EditValue;
    quDocsInvIdIACTIVE.AsInteger:=0;
    quDocsInvIdITYPE.AsInteger:=0;
    quDocsInvIdSUM1.AsFloat:=0;
    quDocsInvIdSUM11.AsFloat:=0;
    quDocsInvIdSUM2.AsFloat:=0;
    quDocsInvIdSUM21.AsFloat:=0;
    quDocsInvIdSUM3.AsFloat:=0;
    quDocsInvIdSUM31.AsFloat:=0;
    quDocsInvIdCOMMENT.AsString:=cxTextEdit2.Text;
    quDocsInvIdIDETAL.AsInteger:=cxComboBox1.ItemIndex+1;
    quDocsInvId.Post;

    cxTextEdit1.Tag:=IDH;

    prWH('   ���������� ������ � �����.',Memo1);

    //�������� ������������ ������ � �����
    quSpecINv.Active:=False;
    quSpecInv.ParamByName('IDH').AsInteger:=IDH;
    quSpecInv.Active:=True;

    quSpecInv.First;
    while not quSpecInv.Eof do quSpecInv.Delete;

    PBar1.Properties.Min:=0;
    PBar1.Properties.Max:=taSpec1.RecordCount;
    PBar1.Position:=0;
    PBar1.Visible:=True;

    if taSpec1.RecordCount>100 then iM:=Trunc(taSpec1.RecordCount/100)
    else iM:=1;

    iC:=0;

    //�������� ������ ����� ��� ����

    taSpec1.First;
    while not taSpec1.Eof do
    begin
      if (abs(taSpec1Quant.AsFloat)>=0.0001) or (abs(taSpec1QuantFact.AsFloat)>=0.0001) or (abs(taSpec1SumInTO.AsFloat)>=0.0001) then
      begin //������� �� ��������� ������
        quSpecInv.Append;
        quSpecInvIDHEAD.AsInteger:=IDH;
        quSpecInvID.AsInteger:=GetId('InvS');
        quSpecInvNUM.AsInteger:=taSpec1Num.AsInteger;
        quSpecInvIDCARD.AsInteger:=taSpec1IdGoods.AsInteger;
        quSpecInvQUANT.AsFloat:=taSpec1Quant.AsFloat;
        quSpecInvSUMIN.AsFloat:=taSpec1SumIn.AsFloat;
        quSpecInvSUMINR.AsFloat:=taSpec1SumInTO.AsFloat;
        quSpecInvSUMUCH.AsFloat:=taSpec1SumUch.AsFloat;
        quSpecInvIDMESSURE.AsInteger:=taSpec1IM.AsInteger;
        quSpecInvKM.AsFloat:=taSpec1Km.AsFloat;
        quSpecInvQUANTFACT.AsFloat:=taSpec1QuantFact.AsFloat;
        quSpecInvSUMINFACT.AsFloat:=taSpec1SumInF.AsFloat;
        quSpecInvSUMUCHFACT.AsFloat:=taSpec1SumUchF.AsFloat;
        quSpecInvTCARD.AsInteger:=taSpec1TCard.AsInteger;
        quSpecInvIDGROUP.AsInteger:=0;
        quSpecInvNOCALC.AsInteger:=taSpec1NoCalc.AsInteger;
        quSpecInvSUMIN0.AsFloat:=taSpec1SumIn0.AsFloat;
        quSpecInvSUMINFACT0.AsFloat:=taSpec1SumInF0.AsFloat;
        quSpecInvNDSPROC.AsFloat:=taSpec1NDSProc.AsFloat;
        quSpecInvNDSSUM.AsFloat:=taSpec1SumNDS.AsFloat;
        quSpecInvIDGROUP.AsInteger:=taSpec1IdGroup.AsInteger;
        quSpecInv.Post;
      end;
      taSpec1.Next; delay(10);

      inc(iC);
      if iC mod iM = 0 then
      begin
        PBar1.Position:=iC;
        delay(10);
      end;
    end;

    Delay(100);
    PBar1.Visible:=False;

    quSpecINv.Active:=False;

    prWH('   �������� ������.',Memo1); delay(10);

    CloseTe(taSpecC1); //����
    CloseTe(taCalcB1); //���� �����

    PBar1.Properties.Min:=0;
    PBar1.Properties.Max:=taSpec1.RecordCount;
    PBar1.Position:=0;
    PBar1.Visible:=True;

    if taSpec1.RecordCount>100 then iM:=Trunc(taSpec1.RecordCount/100)
    else iM:=1;

    iC:=0;

    // ������ ���-�� ������� � ������������ ����
    prWH('   ������ ���-��.',Memo1);

    taSpec1.First;
    while not taSpec1.Eof do
    begin
      if (abs(taSpec1Quant.AsFloat)>=0.0001) or (abs(taSpec1QuantFact.AsFloat)>=0.0001) or (abs(taSpec1SumInTO.AsFloat)>=0.0001) then
      begin //������� �� ��������� ������
        prWH('   ���.: '+taSpec1Num.AsString+'('+taSpec1IdGoods.AsString+')',Memo1); delay(10);
        QZer:=0;
        if (taSpec1TCard.AsInteger=1)and(taSpec1NoCalc.AsInteger=0)and(taSpec1CType.AsInteger=1)
          then bCalc:=True else bCalc:=False;

        if fCalcBlasCard(IdSkl,taSpec1IdGoods.AsInteger) then bCalc:=False; //���� �� ������������ �� ��� �����

        //prCalcBlInv

        if bCalc then
        begin
          //���� ���-�� � ������� �� ��������� � ��������
          prFindSM(taSpec1IM.AsInteger,StrWk,IdM);

          if taSpec1Quant.AsFloat>=0 then   //�������� ������ ����  - ������� ��������� � ��� ������� �����
          begin     // 1,2,3,4,6
            rQ1:=(taSpec1QuantFact.AsFloat-taSpec1Quant.AsFloat)*taSpec1Km.AsFloat;
            if rQ1<0 then bCalc:=False //�������� ������ ����� - ���������, ��� ������� �����
            else //�������
              prCalcBlInv('    ',taSpec1NameG.AsString,taSpec1Num.AsInteger,taSpec1IdGoods.AsInteger,iDate,taSpec1TCard.AsInteger,IdSkl,rQ1,IdM,dmORep.taCalcB1,fmAddInv.taSpecC1,fmAddInv.taSpec1,Memo1,0);
          end else                         //�������� ������ ����
          begin   // 5,7
            if taSpec1QuantFact.AsFloat>0 then  // ��������� ������ �� ��� ������ 0  �� �����
            begin
              prCalcBlInv('    ',taSpec1NameG.AsString,taSpec1Num.AsInteger,taSpec1IdGoods.AsInteger,iDate,taSpec1TCard.AsInteger,IdSkl,(taSpec1QuantFact.AsFloat*taSpec1Km.AsFloat),IdM,dmORep.taCalcB1,fmAddInv.taSpecC1,fmAddInv.taSpec1,Memo1,0);
            end;
            // �������� ���������� ������������� ���-�� ��������
            prCalcBlInv('    ',taSpec1NameG.AsString,taSpec1Num.AsInteger,taSpec1IdGoods.AsInteger,iDate,taSpec1TCard.AsInteger,IdSkl,(taSpec1Quant.AsFloat*taSpec1Km.AsFloat*(-1)),IdM,dmORep.taCalcB1,fmAddInv.taSpecC1,fmAddInv.taSpec1,Memo1,1);
            // ������ ����� ��������� � �������� � ������� ���-�� ���� � 0
            qZer:=1;
            bCalc:=False; // � ��� ������� �����
          end;
        end;

        if bCalc=False then //��� ��� ������� �����
        begin

          if taSpecC1.Locate('IdGoods',taSpec1IdGoods.AsInteger,[]) then
          begin //�������������
            VarsToSpec;
            taSpecC1.Edit;
          end else
          begin

            iMax:=1;
            if not taSpecC1.Eof then
            begin
              taSpecC1.Last;
              iMax:=taSpecC1Num.AsInteger+1;
            end;

            VarsToZero;
            taSpecC1.Append;  //��� ����������
            taSpecC1Num.AsInteger:=iMax;
          end;

          Km:=taSpec1Km.AsFloat;
          if Km=0 then Km:=1;

          prFindSM(taSpec1IM.AsInteger,StrWk,iM1); //��������� ��� � �������� �� ���
   //����� ������� - ���� ����� ��������� = ���������
   //������ ������� ����

          taSpecC1IdGoods.AsInteger:=taSpec1IdGoods.AsInteger;
          taSpecC1NameG.AsString:=taSpec1NameG.AsString;
          taSpecC1IM.AsInteger:=iM1;
          taSpecC1SM.AsString:=StrWk;
          taSpecC1Quant.AsFloat:=taSpec1Quant.AsFloat*Km+Qr;

          taSpecC1PriceIn.AsFloat:=taSpec1PriceIn.AsFloat/Km;    //���������
          taSpecC1PriceIn0.AsFloat:=taSpec1PriceIn0.AsFloat/Km;    //���������
          taSpecC1PriceUch.AsFloat:=taSpec1PriceUch.AsFloat/Km;

          taSpecC1SumIn.AsFloat:=RoundVal((taSpec1Quant.AsFloat*Km+Qr)*taSpec1PriceIn.AsFloat/Km);
          taSpecC1SumIn0.AsFloat:=RoundVal((taSpec1Quant.AsFloat*Km+Qr)*taSpec1PriceIn0.AsFloat/Km);
          taSpecC1SumUch.AsFloat:=RoundVal((taSpec1Quant.AsFloat*Km+Qr)*taSpec1PriceUch.AsFloat/Km);

          if qZer=0 then //����������� �������
          begin
            taSpecC1QuantFact.AsFloat:=taSpec1QuantFact.AsFloat*Km+Qf;
            taSpecC1PriceInF.AsFloat:=taSpec1PriceInF.AsFloat/Km; //�����������
            taSpecC1PriceInF0.AsFloat:=taSpec1PriceInF0.AsFloat/Km; //�����������
            taSpecC1PriceUchF.AsFloat:=taSpec1PriceUchF.AsFloat/Km;
            taSpecC1SumInF.AsFloat:=RoundVal((taSpec1QuantFact.AsFloat*Km+Qf)*taSpec1PriceInF.AsFloat/Km);
            taSpecC1SumUchF.AsFloat:=RoundVal((taSpec1QuantFact.AsFloat*Km+Qf)*taSpec1PriceUchF.AsFloat/Km);
            taSpecC1SumInF0.AsFloat:=RoundVal((taSpec1QuantFact.AsFloat*Km+Qf)*taSpec1PriceInF0.AsFloat/Km);
          end;

          if qZer=1 then //������������� ���-�� ����� � 0-�
          begin
            taSpecC1QuantFact.AsFloat:=0;
            taSpecC1PriceInF0.AsFloat:=0; //�����������
            taSpecC1PriceInF.AsFloat:=0; //�����������
            taSpecC1PriceUchF.AsFloat:=0;
            taSpecC1SumInF.AsFloat:=0;
            taSpecC1SumInF0.AsFloat:=0;
            taSpecC1SumUchF.AsFloat:=0;
          end;

          taSpecC1Km.AsFloat:=1;
          taSpecC1TCard.AsInteger:=taSpec1TCard.AsInteger;
          taSpecC1CType.AsInteger:=taSpec1CType.AsInteger;
          taSpecC1NDSProc.AsFloat:=taSpecCNDSProc.AsFloat;
          taSpecC1.Post;

        end;
      end;
      taSpec1.Next;

      inc(iC);
      if iC mod iM = 0 then
      begin
        PBar1.Position:=iC;
        delay(10);
      end;
    end;

    //������ ������������� �������

    Delay(100);
    PBar1.Visible:=False;
    Delay(100);


    PBar1.Properties.Min:=0;
    PBar1.Properties.Max:=taSpecC1.RecordCount;
    PBar1.Position:=0;
    PBar1.Visible:=True;

    if taSpecC1.RecordCount>100 then iM:=Trunc(taSpecC1.RecordCount/100)
    else iM:=1;

    iC:=0;

    prWH('   ������ ����.',Memo1);

  //��������� �� ��
    taSpecC1.First;  //�������� ���� ���� �������� � �� ��� ������������� �������� ��� �����, ��� ��������
    while not taSpecC1.Eof do
    begin
      if taSpecC1Km.AsFloat<>0 then
      begin
        prCalcSumRemn.ParamByName('IDCARD').AsInteger:=taSpecC1IdGoods.AsInteger;
        prCalcSumRemn.ParamByName('IDSKL').AsInteger:=cxLookupComboBox1.EditValue;
        prCalcSumRemn.ParamByName('DDATE').AsDate:=cxDateEdit1.Date;
        prCalcSumRemn.ParamByName('ISS').AsInteger:=iSS;
        prCalcSumRemn.ExecProc;
        rSumTO:=prCalcSumRemn.ParamByName('REMNSUM').AsFloat;   // ��������� ����� ������������
        rPrice:=taSpecC1PriceInF.AsFloat;
        rPrice0:=taSpecC1PriceInF0.AsFloat;

        if (rPrice<0.01)or(rPrice0<0.01) then
        begin
          if taSpec1.Locate('IdGoods',taSpecC1IdGoods.AsInteger,[]) then
          begin
            rPrice:=taSpec1PriceInF.AsFloat/taSpec1Km.AsFloat;
            rPrice0:=taSpec1PriceInF0.AsFloat/taSpec1Km.AsFloat;
          end;
          if (rPrice<0.01)or(rPrice0<0.01) then
            prCalcLastPricePos(taSpecC1IdGoods.AsInteger,cxLookupComboBox1.EditValue,Trunc(cxDateEdit1.Date),rPrice,rPriceUch,rPrice0);
        end;

        taSpecC1.Edit;
        taSpecC1SumInTO.AsFloat:=rSumTO;
        taSpecC1PriceInF.AsFloat:=rPrice; //���� ������ , ���� ���� �� ��� � ������� ��
        taSpecC1PriceInF0.AsFloat:=rPrice0; //���� ������ , ���� ���� �� ��� � ������� ��
        taSpecC1SumInF.AsFloat:=RV(taSpecC1QuantFact.AsFloat*rPrice);
        taSpecC1SumInF0.AsFloat:=RV(taSpecC1QuantFact.AsFloat*rPrice0);
        taSpecC1.Post;
      end;
      taSpecC1.Next;

      inc(iC);
      if iC mod iM = 0 then
      begin
        PBar1.Position:=iC;
        delay(10);
      end;
    end;

    Delay(100);
    PBar1.Visible:=False;

  {  taSpecC.First;  //�������� ���� ���� �������� � �� ��� ������������� �������� ��� �����, ��� ��������
    while not taSpecC.Eof do
    begin
      rQ:=prCalcRemn(taSpecCIdGoods.AsInteger,Trunc(cxDateEdit1.Date),cxLookupComboBox1.EditValue);
      if abs(rQ)>0 then
      begin
        rSum:=prCalcRemnSum(taSpecCIdGoods.AsInteger,Trunc(cxDateEdit1.Date),cxLookupComboBox1.EditValue,rQ);
        rQ:=rQ*taSpecCKm.AsFloat; //�� �������� ����� � �������
//        rSum:=rSum*taSpecCKm.AsFloat; //����� � ������� ����� //�������� �.�. ����� ����� � ����� ��.���.
        taSpecC.Edit;
        taSpecCQuant.AsFloat:=rQ;
        taSpecCSumIn.AsFloat:=rSum;
        taSpecCSumUch.AsFloat:=rSum;
        taSpecCPriceIn.AsFloat:=RoundEx(rSum/rQ*100)/100;
        taSpecCPriceUch.AsFloat:=RoundEx(rSum/rQ*100)/100;
        if taSpecCSumInF.AsFloat=0 then
        begin
          taSpecCSumInF.AsFloat:=RoundEx(rSum/rQ*100*taSpecCQuantFact.AsFloat)/100;
          taSpecCPriceInF.AsFloat:=RoundEx(rSum/rQ*100)/100
        end;
        if taSpecCSumUchF.AsFloat=0 then
        begin
          taSpecCSumUchF.AsFloat:=RoundEx(rSum/rQ*100*taSpecCQuantFact.AsFloat)/100;
          taSpecCPriceUchF.AsFloat:=RoundEx(rSum/rQ*100)/100
        end;
        taSpecC.Post;
      end;
      taSpecC.Next;
    end; }

    //������������� ����

    prWH('   ������ ������������� ����.',Memo1);

    PBar1.Properties.Min:=0;
    PBar1.Properties.Max:=taCalcB1.RecordCount;
    PBar1.Position:=0;
    PBar1.Visible:=True;

    if taCalcB1.RecordCount>100 then iM:=Trunc(taCalcB1.RecordCount/100)
    else iM:=1;

    iC:=0;

    taCalcB1.First;
    while not taCalcB1.Eof do
    begin
      if taSpecC1.Locate('IdGoods',taCalcB1IDCARD.AsInteger,[]) then
      begin
        taCalcB1.Edit;
        taCalcB1PRICEIN.AsFloat:=taSpecC1PriceIn.AsFloat;
        taCalcB1SUMIN.AsFloat:=taCalcB1QUANTC.AsFloat*taSpecC1PriceIn.AsFloat;
        taCalcB1PRICEIN0.AsFloat:=taSpecC1PriceIn0.AsFloat;
        taCalcB1SUMIN0.AsFloat:=taCalcB1QUANTC.AsFloat*taSpecC1PriceIn0.AsFloat;
        taCalcB1.Post;
      end;
      taCalcB1.Next;

      inc(iC);
      if iC mod iM = 0 then
      begin
        PBar1.Position:=iC;
        delay(10);
      end;
    end;

    Delay(100);
    PBar1.Visible:=False;

    //����������
    prWH('   ���������� ������.',Memo1); delay(10);

    PBar1.Properties.Min:=0;
    PBar1.Properties.Max:=taSpecC1.RecordCount;
    PBar1.Position:=0;
    PBar1.Visible:=True;

    if taSpecC1.RecordCount>100 then iM:=Trunc(taSpecC1.RecordCount/100)
    else iM:=1;

    iC:=0;

    //�������� ������������ ������ � �����
    quSpecINvC.Active:=False;
    quSpecInvC.ParamByName('IDH').AsInteger:=IDH;
    quSpecInvC.Active:=True;

    quSpecInvC.First;
    while not quSpecInvC.Eof do quSpecInvC.Delete;

    Sum1:=0;  Sum11:=0; Sum2:=0;  Sum21:=0; Sum3:=0;
    SumTR:=0; SumTF:=0; SumTD:=0;
    Sum0:=0;  SumF0:=0;

    taSpecC1.First;
    while not taSpecC1.Eof do
    begin
      rQf:=taSpecC1QuantFact.AsFloat;
      if abs(rQf)<0.00000001 then rQf:=0.000001;
      quSpecInvC.Append;
      quSpecInvCIDHEAD.AsInteger:=IDH;
      quSpecInvCID.AsInteger:=taSpecC1Num.AsInteger;
      quSpecInvCNUM.AsInteger:=taSpecC1Num.AsInteger;
      quSpecInvCIDCARD.AsInteger:=taSpecC1IdGoods.AsInteger;
      quSpecInvCQUANT.AsFloat:=taSpecC1Quant.AsFloat;
      quSpecInvCSUMIN.AsFloat:=taSpecC1SumIn.AsFloat;
      quSpecInvCSUMINR.AsFloat:=taSpecC1SumInTO.AsFloat;
      quSpecInvCSUMUCH.AsFloat:=taSpecC1SumUch.AsFloat;
      quSpecInvCIDMESSURE.AsInteger:=taSpecC1IM.AsInteger;
      quSpecInvCKM.AsFloat:=taSpecC1Km.AsFloat;
      quSpecInvCQUANTFACT.AsFloat:=rQf;
      quSpecInvCSUMINFACT.AsFloat:=taSpecC1SumInF.AsFloat;
      quSpecInvCSUMUCHFACT.AsFloat:=taSpecC1SumUchF.AsFloat;
      quSpecInvCTCARD.AsInteger:=taSpecC1TCard.AsInteger;
      quSpecInvCIDGROUP.AsInteger:=0;
      quSpecInvCSUMIN0.AsFloat:=taSpecC1SumIn0.AsFloat;
      quSpecInvCSUMINFACT0.AsFloat:=taSpecC1SumInF0.AsFloat;
      quSpecInvCNDSPROC.AsFloat:=taSpecC1NDSProc.AsFloat;
      quSpecInvCNDSSUM.AsFloat:=taSpecC1SumNDS.AsFloat;
      quSpecInvC.Post;

      if taSpecC1CType.AsInteger=4 then
      begin //����
        SumTR:=SumTR+taSpecC1SumIn.AsFloat;
        SumTF:=SumTF+taSpecC1SumInF.AsFloat;
        SumTD:=SumTF-SumTR;

      end else //������
      begin
        Sum1:=Sum1+taSpecC1SumIn.AsFloat;
        Sum11:=Sum11+taSpecC1SumUch.AsFloat;
        Sum2:=Sum2+taSpecC1SumInF.AsFloat;
        Sum21:=Sum21+taSpecC1SumUchF.AsFloat;
        Sum3:=Sum3+taSpecC1SumInTO.AsFloat;
        Sum0:=Sum0+taSpecC1SumIn0.AsFloat;
        SumF0:=SumF0+taSpecC1SumInF0.AsFloat;
      end;

      taSpecC1.Next; delay(10);

      inc(iC);
      if iC mod iM = 0 then
      begin
        PBar1.Position:=iC;
        delay(10);
      end;
    end;
    quSpecINvC.Active:=False;

    Delay(100);
    PBar1.Visible:=False;

    prWH('   ���������� �����.',Memo1);

    quSpecInvBC.Active:=False;
    quSpecInvBC.ParamByName('IDH').AsInteger:=IdH;
    quSpecInvBC.Active:=True;

    while not quSpecInvBC.Eof do quSpecInvBC.Delete;

    PBar1.Properties.Min:=0;
    PBar1.Properties.Max:=taCalcB1.RecordCount;
    PBar1.Position:=0;
    PBar1.Visible:=True;

    if taCalcB1.RecordCount>100 then iM:=Trunc(taCalcB1.RecordCount/100)
    else iM:=1;

    iC:=0;

    IdS:=1;
    taCalcB1.First;
    while not taCalcB1.Eof do
    begin
      quSpecInvBC.Append;
      quSpecInvBCIDHEAD.AsInteger:=Idh;
      quSpecInvBCIDB.AsInteger:=taCalcB1ID.AsInteger;
      quSpecInvBCID.AsInteger:=Ids; //��� ��� ������
      quSpecInvBCCODEB.AsInteger:=taCalcB1CODEB.AsInteger;
      quSpecInvBCNAMEB.AsString:=taCalcB1NAMEB.AsString;
      quSpecInvBCQUANT.AsFloat:=taCalcB1QUANT.AsFloat;
      quSpecInvBCPRICEOUT.AsFloat:=taCalcB1PRICEOUT.AsFloat;
      quSpecInvBCSUMOUT.AsFloat:=taCalcB1SUMOUT.AsFloat;
      quSpecInvBCIDCARD.AsInteger:=taCalcB1IDCARD.AsInteger;
      quSpecInvBCNAMEC.AsString:=taCalcB1NAMEC.AsString;
      quSpecInvBCQUANTC.AsFloat:=taCalcB1QUANTC.AsFloat;
      quSpecInvBCPRICEIN.AsFloat:=taCalcB1PRICEIN.AsFloat;
      quSpecInvBCSUMIN.AsFloat:=taCalcB1SUMIN.AsFloat;
      quSpecInvBCIM.AsInteger:=taCalcB1IM.AsInteger;
      quSpecInvBCSM.AsString:=taCalcB1SM.AsString;
      quSpecInvBCSB.AsString:=taCalcB1SB.AsString;
      quSpecInvBCPRICEIN0.AsFloat:=taCalcB1PRICEIN0.AsFloat;
      quSpecInvBCSUMIN0.AsFloat:=taCalcB1SUMIN0.AsFloat;
      quSpecInvBC.Post;

      inc(Ids);
      taCalcB1.Next;

      inc(iC);
      if iC mod iM = 0 then
      begin
        PBar1.Position:=iC;
        delay(10);
      end;
    end;

    Delay(100);
    PBar1.Visible:=False;

    quSpecInvBC.Active:=False;

    quDocsInvId.Edit;
    quDocsInvIdSUM11.AsFloat:=Sum11;
    quDocsInvIdSUM21.AsFloat:=Sum21;
    quDocsInvIdSUM3.AsFloat:=Sum3;
    quDocsInvIdSUMTARAR.AsFloat:=SumTR;
    quDocsInvIdSUMTARAF.AsFloat:=SumTF;
    quDocsInvIdSUMTARAD.AsFloat:=SumTD;

    if iSS<>2 then
    begin
      quDocsInvIdSUM1.AsFloat:=Sum1;
      quDocsInvIdSUM2.AsFloat:=Sum2;
    end else
    begin
      quDocsInvIdSUM1.AsFloat:=Sum0;
      quDocsInvIdSUM2.AsFloat:=SumF0;
    end;

    quDocsInvId.Post;

    quDocsInvId.Active:=False;

    fmDocsInv.ViewDocsInv.BeginUpdate;
    quDocsInvSel.FullRefresh;
    quDocsInvSel.Locate('ID',IDH,[]);
    fmDocsInv.ViewDocsInv.EndUpdate;

    prAllViewOn;
  end;
  prWH('���������� ��.',Memo1);
  prButton(True);
end;

procedure TfmAddInv.ViewInvEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  iCol:=0;
  if ViewInv.Controller.FocusedColumn.Name='ViewInvQuantFact' then iCol:=1;
  if ViewInv.Controller.FocusedColumn.Name='ViewInvPriceInF' then iCol:=2;
  if ViewInv.Controller.FocusedColumn.Name='ViewInvSumInF' then iCol:=3;
  if ViewInv.Controller.FocusedColumn.Name='ViewInvPriceUchF' then iCol:=4;
  if ViewInv.Controller.FocusedColumn.Name='ViewInvSumUchF' then iCol:=5;
  if ViewInv.Controller.FocusedColumn.Name='ViewInvPriceInF0' then iCol:=6;
  if ViewInv.Controller.FocusedColumn.Name='ViewInvSumInF0' then iCol:=7;
  if ViewInv.Controller.FocusedColumn.Name='ViewInvSumInTO' then iCol:=8;
end;

procedure TfmAddInv.taSpecQuantFactChange(Sender: TField);
begin
  //���������� �����
  if iCol=1 then
  begin
    taSpecSumInF.AsFloat:=taSpecPriceInF.AsFloat*taSpecQuantFact.AsFloat;
    taSpecSumUchF.AsFloat:=taSpecPriceUchF.AsFloat*taSpecQuantFact.AsFloat;
  end;
end;

procedure TfmAddInv.taSpecPriceInFChange(Sender: TField);
begin
  //���������� ����
  if iCol=2 then
  begin
    taSpecSumInF.AsFloat:=taSpecPriceInF.AsFloat*taSpecQuantFact.AsFloat;
//    taSpecSumUchF.AsFloat:=taSpecPriceUchF.AsFloat*taSpecQuantFact.AsFloat;
  end;
end;

procedure TfmAddInv.taSpecSumInFChange(Sender: TField);
begin
  if iCol=3 then  //���������� �����
  begin
    if abs(taSpecQuantFact.AsFloat)>0 then taSpecPriceInF.AsFloat:=RoundEx(taSpecSumInF.AsFloat/taSpecQuantFact.AsFloat*100)/100;
  end;
end;

procedure TfmAddInv.taSpecPriceUchFChange(Sender: TField);
begin
  //���������� ���� �������
  if iCol=4 then
  begin
//    taSpecSumInF.AsFloat:=taSpecPriceInF.AsFloat*taSpecQuantFact.AsFloat;
    taSpecSumUchF.AsFloat:=taSpecPriceUchF.AsFloat*taSpecQuantFact.AsFloat;
  end;
end;

procedure TfmAddInv.taSpecSumUchFChange(Sender: TField);
begin
  if iCol=5 then  //���������� �����
  begin
    if abs(taSpecQuantFact.AsFloat)>0 then taSpecPriceUchF.AsFloat:=RoundEx(taSpecSumUchF.AsFloat/taSpecQuantFact.AsFloat*100)/100;
  end;
end;

procedure TfmAddInv.Label3Click(Sender: TObject);
Var rQ:Real;
    rSum,rSumTO,rSum0:Real;
    rPriceIn,rPriceUch,rPriceIn0:Real;
    iC,iM:INteger;
    iSS:INteger;
    rMessure:Real;
begin //������ ��������
  iCol:=0;
  with dmO do
  with dmORep do
  begin
    prButton(False);

    iSS:=prISS(cxLookupComboBox1.EditValue);

    Memo1.Clear;
    Memo1.Lines.Add('����� ... ���� ������ ��������.'); delay(10);

    try
      ViewInv.BeginUpdate;
      ViewInvC.BeginUpdate;
      ViewBC.BeginUpdate;


      taR.Active:=False;
      taR.Active:=True;
      taR.First; while not taR.Eof do taR.Delete;

      PBar1.Properties.Min:=0;
      PBar1.Properties.Max:=taSpec1.RecordCount;
      PBar1.Position:=0;
      PBar1.Visible:=True;

//    if quAllCards.RecordCount>100 then iM:=Trunc(quAllCards.RecordCount/100)
//    else iM:=1;

      if taSpec1.RecordCount>100 then iM:=Trunc(taSpec1.RecordCount/100)
      else iM:=1;

      iC:=0;

      taSpec1.First;
      while not taSpec1.Eof do
      begin
        if taR.Locate('Code',taSpec1IdGoods.AsInteger,[]) then
        begin   //��� ��� � �������  �� ���� ������� ��� ����� ���� - ����� �����
          rQ:=0;
          rSumTO:=0;
//        rSum:=0;
          rPriceIn:=taRPrice.AsFloat;
          rPriceIn0:=taRPrice0.AsFloat;
        end else
        begin   //��� ������ � �������
          rQ:=prCalcRemn(taSpec1IdGoods.AsInteger,Trunc(cxDateEdit1.Date),cxLookupComboBox1.EditValue);
          rSum:=prCalcRemnSumF(taSpec1IdGoods.AsInteger,Trunc(cxDateEdit1.Date),cxLookupComboBox1.EditValue,rQ,rSum0);

          //rSum - ��� �������������� ���� , �.�. �������������� �� ��������� �������
          rPriceIn:=0;
          rPriceIn0:=0;

          prCalcSumRemn.ParamByName('IDCARD').AsInteger:=taSpec1IdGoods.AsInteger;
          prCalcSumRemn.ParamByName('IDSKL').AsInteger:=cxLookupComboBox1.EditValue;
          prCalcSumRemn.ParamByName('DDATE').AsDate:=cxDateEdit1.Date;
          prCalcSumRemn.ParamByName('ISS').AsInteger:=iSS;
          prCalcSumRemn.ExecProc;
          rSumTO:=prCalcSumRemn.ParamByName('REMNSUM').AsFloat;
          //rSumTO - ��� �������� ���� , ���� ��� ��������������

          if taSpec1Km.AsFloat<>0 then rQ:=rQ/taSpec1Km.AsFloat; //�� �������� ����� � �������

          if abs(rQ)>0.0001 then  //�������� �� ����
          begin
            rPriceIn:=RoundVal(rSum/rQ);
            rPriceIn0:=RoundVal(rSum0/rQ);

            taR.Append;
            taRCode.AsInteger:=taSpec1IdGoods.AsInteger;
            taRPrice.AsFloat:=rPriceIn;
            taRPrice0.AsFloat:=rPriceIn0;
            taR.Post;
          end;
        end;

        if (rPriceIn=0)or(rPriceIn0=0) then
        begin //���� ���� ���� ������� - ����� ���-�� ������ - ����� ����� �� ��
          prCalcLastPrice1.ParamByName('IDGOOD').AsInteger:=taSpec1IdGoods.AsInteger;
          prCalcLastPrice1.ParamByName('ISKL').AsInteger:=cxLookupComboBox1.EditValue;
          prCalcLastPrice1.ExecProc;

          rPriceIn:=prCalcLastPrice1.ParamByName('PRICEIN').AsFloat;
          rPriceIn0:=prCalcLastPrice1.ParamByName('PRICEIN0').AsFloat;

          rMessure:=prCalcLastPrice1.ParamByName('KOEF').AsFloat;
          if (rMessure<>0)and(rMessure<>1) then
          begin
            rPriceIn:=rPriceIn/rMessure;
            rPriceIn0:=rPriceIn0/rMessure;
          end;
        end;


        taSpec1.Edit;
        taSpec1SumInTO.AsFloat:=RoundVal(rSumTO);

        if abs(rQ)>0.0001 then //�������� �� ����
        begin
          taSpec1Quant.AsFloat:=rQ;

          rPriceUch:=rPriceIn;

          taSpec1SumIn.AsFloat:=RoundVal(rPriceIn*rQ);  //�������� �������������� ���� ��������� � ��������
          taSpec1SumIn0.AsFloat:=RoundVal(rPriceIn0*rQ);  //�������� �������������� ���� ��������� � ��������
          taSpec1SumUch.AsFloat:=RoundVal(rPriceIn*rQ);

          taSpec1PriceIn.AsFloat:=rPriceIn;
          taSpec1PriceIn0.AsFloat:=rPriceIn0;
          taSpec1PriceUch.AsFloat:=rPriceUch;
          if (taSpec1SumInF.AsFloat=0)or(cxCheckBox1.Checked=True) then
          begin
            taSpec1SumInF.AsFloat:=RoundVal(rPriceIn*taSpec1QuantFact.AsFloat);
            taSpec1PriceInF.AsFloat:=rPriceIn;
            taSpec1SumInF0.AsFloat:=RoundVal(rPriceIn0*taSpec1QuantFact.AsFloat);
            taSpec1PriceInF0.AsFloat:=rPriceIn0;
          end;
          if (taSpec1SumUchF.AsFloat=0)or(cxCheckBox1.Checked=True) then
          begin
            taSpec1SumUchF.AsFloat:=RoundVal(rPriceIn*taSpec1QuantFact.AsFloat);
            taSpec1PriceUchF.AsFloat:=rPriceIn;
          end;
        end else
        begin
          taSpec1Quant.AsFloat:=0;
          taSpec1SumIn.AsFloat:=0;
          taSpec1SumIn0.AsFloat:=0;
//        taSpec1SumInTO.AsFloat:=0;
          taSpec1SumUch.AsFloat:=0;

          taSpec1PriceIn.AsFloat:=rPriceIn;    //���� ��� ����� ���� ��������� ���� �� ������� ��� ��������� ��������
          taSpec1PriceIn0.AsFloat:=rPriceIn0;    //���� ��� ����� ���� ��������� ���� �� ������� ��� ��������� ��������
          taSpec1PriceUch.AsFloat:=rPriceIn;

          if (taSpec1SumInF.AsFloat=0)or(cxCheckBox1.Checked=True) then
          begin
            if rPriceIn=0 then
            begin
              prCalcLastPricePos(taSpec1IdGoods.AsInteger,cxLookupComboBox1.EditValue,Trunc(cxDateEdit1.Date),rPriceIn,rPriceUch,rPriceIn0);

              taSpec1PriceInF.AsFloat:=rPriceIn*taSpec1Km.AsFloat;
              taSpec1PriceInF0.AsFloat:=rPriceIn0*taSpec1Km.AsFloat;

              taSpec1SumInF.AsFloat:=RoundVal(rPriceIn*taSpec1Km.AsFloat*taSpec1QuantFact.AsFloat);
              taSpec1SumInF0.AsFloat:=RoundVal(rPriceIn0*taSpec1Km.AsFloat*taSpec1QuantFact.AsFloat);

              taSpec1PriceUchF.AsFloat:=rPriceUch*taSpec1Km.AsFloat;
              taSpec1SumUchF.AsFloat:=RoundVal(rPriceUch*taSpec1Km.AsFloat*taSpec1QuantFact.AsFloat);
            end
            else
            begin
              taSpec1PriceInF.AsFloat:=rPriceIn;
              taSpec1SumInF.AsFloat:=RoundVal(rPriceIn*taSpec1QuantFact.AsFloat);

              taSpec1PriceInF0.AsFloat:=rPriceIn0;
              taSpec1SumInF0.AsFloat:=RoundVal(rPriceIn0*taSpec1QuantFact.AsFloat);

              taSpec1PriceUchF.AsFloat:=rPriceUch;
              taSpec1SumUchF.AsFloat:=RoundVal(rPriceUch*taSpec1QuantFact.AsFloat);
            end;
          end;
        end;
        taSpec1.Post;

        taSpec1.Next;
        inc(iC);
        if iC mod iM = 0 then
        begin
          PBar1.Position:=iC;
          delay(10);
        end;
      end;
      Delay(100);
      PBar1.Visible:=False;

      taR.Active:=False;
    finally
      ViewInv.EndUpdate;
      ViewInvC.EndUpdate;
      ViewBC.EndUpdate;
    end;

    Memo1.Lines.Add('������ ��.'); delay(10);

    prButton(True);
  end;
end;

procedure TfmAddInv.Label4Click(Sender: TObject);
begin
  prButton(False);

  Memo1.Clear;
  Memo1.Lines.Add('����� ... ���� ���������� ��������.'); delay(10);

  ViewInv.BeginUpdate;
  ViewInvC.BeginUpdate;
  ViewBC.BeginUpdate;

  taSpec1.First;
  while not taSpec1.Eof do
  begin
    taSpec1.Edit;

    taSpec1QuantFact.AsFloat:=taSpec1Quant.AsFloat;
    taSpec1PriceInF.AsFloat:=taSpec1PriceIn.AsFloat;
    taSpec1SumInF.AsFloat:=taSpec1SumIn.AsFloat;
    taSpec1PriceUchF.AsFloat:=taSpec1PriceUch.AsFloat;
    taSpec1SumUchF.AsFloat:=taSpec1SumUch.AsFloat;
    taSpec1PriceInF0.AsFloat:=taSpec1PriceIn0.AsFloat;
    taSpec1SumInF0.AsFloat:=taSpec1SumIn0.AsFloat;

    taSpec1.Post;


    taSpec1.Next;
  end;
  ViewInv.EndUpdate;
  ViewInvC.EndUpdate;
  ViewBC.EndUpdate;
  Memo1.Lines.Add('��.'); delay(10);

  prButton(True);
end;

procedure TfmAddInv.Label6Click(Sender: TObject);
begin
  prButton(False);

  Memo1.Clear;
  Memo1.Lines.Add('����� ... '); delay(10);

  taSpec1.First;
  while not taSpec1.Eof do
  begin
    taSpec1.Edit;

    taSpec1QuantFact.AsFloat:=0;
    taSpec1PriceInF.AsFloat:=0;
    taSpec1PriceInF0.AsFloat:=0;
    taSpec1SumInF.AsFloat:=0;
    taSpec1SumInF0.AsFloat:=0;
    taSpec1PriceUchF.AsFloat:=0;
    taSpec1SumUchF.AsFloat:=0;

    taSpec1.Post;


    taSpec1.Next;
  end;
  Memo1.Lines.Add('��.'); delay(10);

  prButton(True);
end;

procedure TfmAddInv.taSpecCBeforePost(DataSet: TDataSet);
begin
  taSpecCQuantDif.AsFloat:=taSpecCQuantFact.AsFloat-taSpecCQuant.AsFloat;
  taSpecCSumInDif.AsFloat:=taSpecCSumInF.AsFloat-taSpecCSumIn.AsFloat;
  taSpecCSumUchDif.AsFloat:=taSpecCSumUchF.AsFloat-taSpecCSumUch.AsFloat;
  taSpecCSumInTODif.AsFloat:=taSpecCSumInF.AsFloat-taSpecCSumInTO.AsFloat;
end;

procedure TfmAddInv.cxButton3Click(Sender: TObject);
Var StrWk:String;
    i:Integer;
    StrF:String;
    iSS:INteger;
    IdSkl:INteger;
begin
  taSpec.IndexName:='taSpecIndex1';
  taSpecC.IndexName:='taSpecCIndex1';

  IdSkl:=cxLookupComboBox1.EditValue;
  iSS:=prISS(IdSkl);

  if cxRadioButton3.Checked then
  begin
    if PageControl1.ActivePageIndex=0 then prNExportExel5(ViewInv);
    if PageControl1.ActivePageIndex=1 then prNExportExel5(ViewInvC);
    if PageControl1.ActivePageIndex=2 then prNExportExel5(ViewBC);
  end;
  if cxRadioButton1.Checked then
  begin
    if PageControl1.ActivePageIndex=0 then
    begin
      taSpec.Filter:=ViewInv.DataController.Filter.FilterText;
      taSpec.Filtered:=True;

      if cxCheckBox2.Checked then
      begin
        if iSS=2 then
        begin
          RepInv.LoadFromFile(CurDir + 'InvRep1s0.frf');
          frVariables.Variable['SumDoc']:=dmORep.quDocsInvSelSUM3.AsFloat;
          frVariables.Variable['SumFact']:=dmORep.quDocsInvSelSUM2.AsFloat;
          frVariables.Variable['SumDif']:=dmORep.quDocsInvSelSUM2.AsFloat-dmORep.quDocsInvSelSUM3.AsFloat;
        end else
        begin
          RepInv.LoadFromFile(CurDir + 'InvRep1s.frf');
          frVariables.Variable['SumDoc']:=dmORep.quDocsInvSelSUM3.AsFloat;
          frVariables.Variable['SumFact']:=dmORep.quDocsInvSelSUM2.AsFloat;
          frVariables.Variable['SumDif']:=dmORep.quDocsInvSelSUM2.AsFloat-dmORep.quDocsInvSelSUM3.AsFloat;
        end;
      end
      else RepInv.LoadFromFile(CurDir + 'InvRep1.frf');

      frVariables.Variable['DocNum']:=cxTextEdit1.Text;
      frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date);
      frVariables.Variable['DocStore']:=cxLookupComboBox1.Text;

      StrWk:=ViewInv.DataController.Filter.FilterCaption;
      while Pos('AND',StrWk)>0 do
      begin
        i:=Pos('AND',StrWk);
        StrWk[i]:=' ';
        StrWk[i+1]:='�';
        StrWk[i+2]:=' ';
      end;
      while Pos('and',StrWk)>0 do
      begin
        i:=Pos('and',StrWk);
        StrWk[i]:=' ';
        StrWk[i+1]:='�';
        StrWk[i+2]:=' ';
      end;

      frVariables.Variable['DocPars']:=StrWk;

      RepInv.ReportName:='��������� �������������� (��).';
      RepInv.PrepareReport;
      RepInv.ShowPreparedReport;

      taSpec.Filter:='';
      taSpec.Filtered:=False;
    end;
    if PageControl1.ActivePageIndex=1 then
    begin
      taSpecC.Filter:=ViewInv.DataController.Filter.FilterText;
      taSpecC.Filtered:=True;

      if cxCheckBox2.Checked then
      begin
        if iSS=2 then
        begin
          RepInv.LoadFromFile(CurDir + 'InvRep11s0.frf');
          frVariables.Variable['SumDoc']:=dmORep.quDocsInvSelSUM3.AsFloat;
          frVariables.Variable['SumFact']:=dmORep.quDocsInvSelSUM2.AsFloat;
          frVariables.Variable['SumDif']:=dmORep.quDocsInvSelSUM2.AsFloat-dmORep.quDocsInvSelSUM3.AsFloat;
        end else
        begin
          RepInv.LoadFromFile(CurDir + 'InvRep11s.frf');
          frVariables.Variable['SumDoc']:=dmORep.quDocsInvSelSUM3.AsFloat;
          frVariables.Variable['SumFact']:=dmORep.quDocsInvSelSUM2.AsFloat;
          frVariables.Variable['SumDif']:=dmORep.quDocsInvSelSUM2.AsFloat-dmORep.quDocsInvSelSUM3.AsFloat;
        end;
      end
      else
      begin
        if iSS=2 then
        begin
          RepInv.LoadFromFile(CurDir + 'InvRep11_0.frf');
        end else
        begin
          RepInv.LoadFromFile(CurDir + 'InvRep11.frf');
        end;
      end;

      frVariables.Variable['DocNum']:=cxTextEdit1.Text;
      frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date);
      frVariables.Variable['DocStore']:=cxLookupComboBox1.Text;

      StrWk:=ViewInv.DataController.Filter.FilterCaption;
      while Pos('AND',StrWk)>0 do
      begin
        i:=Pos('AND',StrWk);
        StrWk[i]:=' ';
        StrWk[i+1]:='�';
        StrWk[i+2]:=' ';
      end;
      while Pos('and',StrWk)>0 do
      begin
        i:=Pos('and',StrWk);
        StrWk[i]:=' ';
        StrWk[i+1]:='�';
        StrWk[i+2]:=' ';
      end;

      frVariables.Variable['DocPars']:=StrWk;

      RepInv.ReportName:='��������� �������������� (�).';
      RepInv.PrepareReport;
      RepInv.ShowPreparedReport;

      taSpecC.Filter:='';
      taSpecC.Filtered:=False;
    end;
  end;
  if cxRadioButton2.Checked then
  begin
    if PageControl1.ActivePageIndex=0 then
    begin
      taSpec.Filter:=ViewInv.DataController.Filter.FilterText;
      taSpec.Filtered:=True;

      RepInv.LoadFromFile(CurDir + 'InvRep2.frf');

      frVariables.Variable['DocNum']:=cxTextEdit1.Text;
      frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date);
      frVariables.Variable['DocStore']:=cxLookupComboBox1.Text;

      StrWk:=ViewInv.DataController.Filter.FilterCaption;
      while Pos('AND',StrWk)>0 do
      begin
        i:=Pos('AND',StrWk);
        StrWk[i]:=' ';
        StrWk[i+1]:='�';
        StrWk[i+2]:=' ';
      end;
      while Pos('and',StrWk)>0 do
      begin
        i:=Pos('and',StrWk);
        StrWk[i]:=' ';
        StrWk[i+1]:='�';
        StrWk[i+2]:=' ';
      end;

      frVariables.Variable['DocPars']:=StrWk;

      RepInv.ReportName:='��������� �������������� (��).';
      RepInv.PrepareReport;
      RepInv.ShowPreparedReport;

      taSpec.Filter:='';
      taSpec.Filtered:=False;
    end;
    if PageControl1.ActivePageIndex=1 then
    begin
      taSpecC.Filter:=ViewInv.DataController.Filter.FilterText;
      taSpecC.Filtered:=True;

      RepInv.LoadFromFile(CurDir + 'InvRep22.frf');

      frVariables.Variable['DocNum']:=cxTextEdit1.Text;
      frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date);
      frVariables.Variable['DocStore']:=cxLookupComboBox1.Text;

      StrWk:=ViewInv.DataController.Filter.FilterCaption;
      while Pos('AND',StrWk)>0 do
      begin
        i:=Pos('AND',StrWk);
        StrWk[i]:=' ';
        StrWk[i+1]:='�';
        StrWk[i+2]:=' ';
      end;
      while Pos('and',StrWk)>0 do
      begin
        i:=Pos('and',StrWk);
        StrWk[i]:=' ';
        StrWk[i+1]:='�';
        StrWk[i+2]:=' ';
      end;

      frVariables.Variable['DocPars']:=StrWk;

      RepInv.ReportName:='��������� �������������� (�).';
      RepInv.PrepareReport;
      RepInv.ShowPreparedReport;

      taSpecC.Filter:='';
      taSpecC.Filtered:=False;
    end;
  end;
  if cxRadioButton4.Checked then
  begin
    if PageControl1.ActivePageIndex=0 then
    begin
      taSpec.Filter:=ViewInv.DataController.Filter.FilterText;
      taSpec.Filtered:=True;

      if cxCheckBox2.Checked then
      begin
        RepInv.LoadFromFile(CurDir + 'InvRep1s.frf');
        frVariables.Variable['SumDoc']:=dmORep.quDocsInvSelSUM3.AsFloat;
        frVariables.Variable['SumFact']:=dmORep.quDocsInvSelSUM2.AsFloat;
        frVariables.Variable['SumDif']:=dmORep.quDocsInvSelSUM2.AsFloat-dmORep.quDocsInvSelSUM3.AsFloat;
      end
      else RepInv.LoadFromFile(CurDir + 'InvRep1Uni.frf');

      frVariables.Variable['DocNum']:=cxTextEdit1.Text;
      frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date);
      frVariables.Variable['DocStore']:=cxLookupComboBox1.Text;

      StrWk:=ViewInv.DataController.Filter.FilterCaption;
      while Pos('AND',StrWk)>0 do
      begin
        i:=Pos('AND',StrWk);
        StrWk[i]:=' ';
        StrWk[i+1]:='�';
        StrWk[i+2]:=' ';
      end;
      while Pos('and',StrWk)>0 do
      begin
        i:=Pos('and',StrWk);
        StrWk[i]:=' ';
        StrWk[i+1]:='�';
        StrWk[i+2]:=' ';
      end;

      frVariables.Variable['DocPars']:=StrWk;

      RepInv.ReportName:='��������� �������������� (��).';
      RepInv.PrepareReport;
      RepInv.ShowPreparedReport;

      taSpec.Filter:='';
      taSpec.Filtered:=False;
    end;
    if PageControl1.ActivePageIndex=1 then
    begin
      taSpecC.Filter:=ViewInv.DataController.Filter.FilterText;
      taSpecC.Filtered:=True;

      if cxCheckBox2.Checked then
      begin
        RepInv.LoadFromFile(CurDir + 'InvRep11s.frf');
        frVariables.Variable['SumDoc']:=dmORep.quDocsInvSelSUM3.AsFloat;
        frVariables.Variable['SumFact']:=dmORep.quDocsInvSelSUM2.AsFloat;
        frVariables.Variable['SumDif']:=dmORep.quDocsInvSelSUM2.AsFloat-dmORep.quDocsInvSelSUM3.AsFloat;
      end

      else RepInv.LoadFromFile(CurDir + 'InvRep11Uni.frf');

      frVariables.Variable['DocNum']:=cxTextEdit1.Text;
      frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date);
      frVariables.Variable['DocStore']:=cxLookupComboBox1.Text;

      StrWk:=ViewInv.DataController.Filter.FilterCaption;
      while Pos('AND',StrWk)>0 do
      begin
        i:=Pos('AND',StrWk);
        StrWk[i]:=' ';
        StrWk[i+1]:='�';
        StrWk[i+2]:=' ';
      end;
      while Pos('and',StrWk)>0 do
      begin
        i:=Pos('and',StrWk);
        StrWk[i]:=' ';
        StrWk[i+1]:='�';
        StrWk[i+2]:=' ';
      end;

      frVariables.Variable['DocPars']:=StrWk;

      RepInv.ReportName:='��������� �������������� (�).';
      RepInv.PrepareReport;
      RepInv.ShowPreparedReport;

      taSpecC.Filter:='';
      taSpecC.Filtered:=False;
    end;
  end;
  if cxRadioButton5.Checked then
  begin
    if PageControl1.ActivePageIndex=0 then
    begin
      StrF:=ViewInv.DataController.Filter.FilterText;
      if StrF='' then StrF:='QuantDif<>0';
      taSpec.Filter:=StrF;
      taSpec.Filtered:=True;

      if cxCheckBox2.Checked then
      begin
        RepInv.LoadFromFile(CurDir + 'InvRep1s.frf');
        frVariables.Variable['SumDoc']:=dmORep.quDocsInvSelSUM3.AsFloat;
        frVariables.Variable['SumFact']:=dmORep.quDocsInvSelSUM2.AsFloat;
        frVariables.Variable['SumDif']:=dmORep.quDocsInvSelSUM2.AsFloat-dmORep.quDocsInvSelSUM3.AsFloat;
      end
      else RepInv.LoadFromFile(CurDir + 'InvRep1UniSl.frf');

      frVariables.Variable['DocNum']:=cxTextEdit1.Text;
      frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date);
      frVariables.Variable['DocStore']:=cxLookupComboBox1.Text;

      StrWk:=ViewInv.DataController.Filter.FilterCaption;
      while Pos('AND',StrWk)>0 do
      begin
        i:=Pos('AND',StrWk);
        StrWk[i]:=' ';
        StrWk[i+1]:='�';
        StrWk[i+2]:=' ';
      end;
      while Pos('and',StrWk)>0 do
      begin
        i:=Pos('and',StrWk);
        StrWk[i]:=' ';
        StrWk[i+1]:='�';
        StrWk[i+2]:=' ';
      end;

      frVariables.Variable['DocPars']:=StrWk;

      RepInv.ReportName:='��������� �������������� (��).';
      RepInv.PrepareReport;
      RepInv.ShowPreparedReport;

      taSpec.Filter:='';
      taSpec.Filtered:=False;
    end;
    if PageControl1.ActivePageIndex=1 then
    begin
      StrF:=ViewInv.DataController.Filter.FilterText;
      if StrF='' then StrF:='QuantDif<>0';
      taSpecC.Filter:=StrF;
      taSpecC.Filtered:=True;

      if cxCheckBox2.Checked then
      begin
        RepInv.LoadFromFile(CurDir + 'InvRep11s.frf');
        frVariables.Variable['SumDoc']:=dmORep.quDocsInvSelSUM3.AsFloat;
        frVariables.Variable['SumFact']:=dmORep.quDocsInvSelSUM2.AsFloat;
        frVariables.Variable['SumDif']:=dmORep.quDocsInvSelSUM2.AsFloat-dmORep.quDocsInvSelSUM3.AsFloat;
      end
      else
      begin
        if iSS=2 then
        begin
          RepInv.LoadFromFile(CurDir + 'InvRep11UniSl0.frf');
        end else
        begin
          RepInv.LoadFromFile(CurDir + 'InvRep11UniSl.frf');
        end;
      end;

      frVariables.Variable['DocNum']:=cxTextEdit1.Text;
      frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date);
      frVariables.Variable['DocStore']:=cxLookupComboBox1.Text;

      StrWk:=ViewInv.DataController.Filter.FilterCaption;
      while Pos('AND',StrWk)>0 do
      begin
        i:=Pos('AND',StrWk);
        StrWk[i]:=' ';
        StrWk[i+1]:='�';
        StrWk[i+2]:=' ';
      end;
      while Pos('and',StrWk)>0 do
      begin
        i:=Pos('and',StrWk);
        StrWk[i]:=' ';
        StrWk[i+1]:='�';
        StrWk[i+2]:=' ';
      end;

      frVariables.Variable['DocPars']:=StrWk;

      RepInv.ReportName:='��������� �������������� (�).';
      RepInv.PrepareReport;
      RepInv.ShowPreparedReport;

      taSpecC.Filter:='';
      taSpecC.Filtered:=False;
    end;
  end;
  taSpec.IndexName:='taSpecIndex2';
end;

procedure TfmAddInv.acAddListExecute(Sender: TObject);
begin
  bAddSpecInv:=True;
  fmGoods.Show;
end;

procedure TfmAddInv.acDelPosExecute(Sender: TObject);
begin
  if taSpec1.RecordCount>0 then taSpec1.Delete;
end;

procedure TfmAddInv.acDelAllExecute(Sender: TObject);
begin
  if MessageDlg('�������� ��������������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
    CloseTe(taSpec1);
  end;
end;

procedure TfmAddInv.N1Click(Sender: TObject);
begin
//�� ������������
  ViewInv.BeginUpdate;
  taSpec1.First;
  while not taSpec1.Eof do
  begin
    taSpec1.Edit;
    taSpec1NoCalc.AsInteger:=1;
    taSpec1.Post;
    taSpec1.Next;
  end;
  taSpec1.First;
  ViewInv.EndUpdate;
end;

procedure TfmAddInv.N2Click(Sender: TObject);
begin
//��� ������������
  ViewInv.BeginUpdate;
  taSpec1.First;
  while not taSpec1.Eof do
  begin
    taSpec1.Edit;
    taSpec1NoCalc.AsInteger:=0;
    taSpec1.Post;
    taSpec1.Next;
  end;
  taSpec1.First;
  ViewInv.EndUpdate;
end;

procedure TfmAddInv.Memo1DblClick(Sender: TObject);
begin
  fmMessage:=tfmMessage.Create(Application);
  fmMessage.Memo1.Lines:=Memo1.Lines;
  fmMessage.ShowModal;
  fmMessage.Release;
end;

procedure TfmAddInv.acCalc1Execute(Sender: TObject);
begin
  Memo1.Clear;
  taSpec1.First;
  while not taSpec1.Eof do
  begin
    if taSpecC1.Locate('IdGoods',taSpec1IdGoods.AsInteger,[]) then
    begin
      if taSpec1SumUch.AsFloat<>taSpecC1SumUch.AsFloat then
      begin
        Memo1.Lines.Add(taSpec1Num.AsString+'  '+taSpecC1Num.AsString+'  '+taSpec1IdGoods.AsString+'  '+FloatToStr(taSpec1SumUch.AsFloat-taSpecC1SumUch.AsFloat));
      end;
    end;
    taSpec1.Next;
  end;
end;

procedure TfmAddInv.cxButton2Click(Sender: TObject);
begin
  acExit.Execute;
end;

procedure TfmAddInv.acExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmAddInv.taSpecCQuantFactChange(Sender: TField);
begin
  taSpecCSumInF.AsFloat:=RoundVal(taSpecCPriceInF.AsFloat*taSpecCQuantFact.AsFloat);
end;

procedure TfmAddInv.acEqualsExecute(Sender: TObject);
begin
  taSpec1.First;
  while not taSpec1.Eof do
  begin
    if taSpecC1.Locate('IdGoods',taSpec1IdGoods.AsInteger,[]) then
    begin
//      if RoundVal(taSpecC1SumInF.AsFloat)<>RoundVal(taSpec1SumInF.AsFloat) then
      if abs(taSpecC1SumInF.AsFloat-taSpec1SumInF.AsFloat)>0.002 then
      begin
        Memo1.Lines.Add(IntToStr(taSpec1IdGoods.AsInteger)+' '+fts(abs(taSpecC1SumInF.AsFloat-taSpec1SumInF.AsFloat)));
      end;
    end else Memo1.Lines.Add(IntToStr(taSpec1IdGoods.AsInteger));
    taSpec1.Next;
    delay(10);
  end;
end;

procedure TfmAddInv.acRefreshGrExecute(Sender: TObject);
Var S1,S2:String;
    IGr:Integer;
begin
  //�������� ������ ���� �������� ���������� � ������ ������
  with dmORep do
  with dmO do
  begin
    if taSpec.Active=True then
    begin
      taSpec.First;
      while not taSpec.Eof do
      begin
        prFindGroup(taSpecIdGoods.AsInteger,S1,iGr);
        prFind2group(iGr,S1,S2);

        taSpec.Edit;
        taSpecId_Group.AsInteger:=iGr;
        taSpecNameGr.AsString:=S1;
        taSpecNameGr2.AsString:=S2;
        taSpec.Post;

        taSpec.Next; delay(10);
      end;
    end;
  end;
end;

procedure TfmAddInv.Excel1Click(Sender: TObject);
begin
  prNExportExel5(ViewInv);
end;

procedure TfmAddInv.acMovePosExecute(Sender: TObject);
Var iDateB,iDateE,iM:INteger;
    IdCard:INteger;
    NameC:String;
    iMe,RecC:INteger;
begin
  //�������� �� ������
  with dmO do
  begin
    RecC:=0;
    IdCard:=0;
    iMe:=0;

    if PageControl1.ActivePageIndex=0 then
    begin
      if cxButton1.Enabled then
      begin
        RecC:=taSpec1.RecordCount;
        IdCard:=taSpec1IdGoods.AsInteger;
        NameC:=taSpec1NameG.AsString;
        iMe:=taSpec1IM.AsInteger;
      end else
      begin
        RecC:=taSpec.RecordCount;
        IdCard:=taSpecIdGoods.AsInteger;
        NameC:=taSpecNameG.AsString;
        iMe:=taSpecIM.AsInteger;
      end;
    end;

    if PageControl1.ActivePageIndex=1 then
    begin
      if cxButton1.Enabled then
      begin
        RecC:=taSpecC1.RecordCount;
        IdCard:=taSpecC1IdGoods.AsInteger;
        NameC:=taSpecC1NameG.AsString;
        iMe:=taSpecC1IM.AsInteger;
      end else
      begin
        RecC:=taSpecC.RecordCount;
        IdCard:=taSpecCIdGoods.AsInteger;
        NameC:=taSpecCNameG.AsString;
        iMe:=taSpecCIM.AsInteger;
      end;
    end;

    if RecC>0 then
    begin
      //�� ������
      fmSelPerSkl:=tfmSelPerSkl.Create(Application);
      with fmSelPerSkl do
      begin
        cxDateEdit1.Date:=CommonSet.DateFrom;
        quMHAll1.Active:=False;
        quMHAll1.ParamByName('IDPERSON').AsInteger:=Person.Id;
        quMHAll1.Active:=True;
        quMHAll1.First;
        if CommonSet.IdStore>0 then cxLookupComboBox1.EditValue:=CommonSet.IdStore
        else  cxLookupComboBox1.EditValue:=quMHAll1ID.AsInteger;
        cxRadioButton1.Visible:=True;
        cxRadioButton2.Visible:=True;
      end;
      fmSelPerSkl.ShowModal;
      if fmSelPerSkl.ModalResult=mrOk then
      begin
        iDateB:=Trunc(fmSelPerSkl.cxDateEdit1.Date);
        iDateE:=Trunc(fmSelPerSkl.cxDateEdit2.Date)+1;
//        if fmSelPerSkl.cxRadioButton1.Checked then iType:=0 else iType:=1;

        CommonSet.IdStore:=fmSelPerSkl.cxLookupComboBox1.EditValue;
        CommonSet.NameStore:=fmSelPerSkl.cxLookupComboBox1.Text;
        CommonSet.DateFrom:=iDateB;
        CommonSet.DateTo:=iDateE+1;

        CardSel.Id:=IdCard;
        CardSel.Name:=NameC;
        prFindSM(iMe,CardSel.mesuriment,iM);
//        CardSel.NameGr:=ClassTree.Selected.Text;

        prPreShowMove(IdCard,iDateB,iDateE,fmSelPerSkl.cxLookupComboBox1.EditValue,NameC);
        fmSelPerSkl.Release;

//        fmCardsMove.PageControl1.ActivePageIndex:=2;
        fmCardsMove.ShowModal;

      end else fmSelPerSkl.Release;
    end;
  end;
end;

procedure TfmAddInv.acFindWordExecute(Sender: TObject);
begin
  cxTextEdit3.SetFocus;
  cxTextEdit3.SelectAll;
end;

procedure TfmAddInv.cxTextEdit3PropertiesChange(Sender: TObject);
begin
  if cxButton1.Enabled then
  begin
    if taSpec1.Active then
    begin
      taSpec1.Locate('NameG',cxTextEdit3.Text,[loCaseInsensitive, loPartialKey]);
      prRowFocus(ViewInv);
    end;
  end else
  begin
    if taSpec.Active then
    begin
      taSpec.Locate('NameG',cxTextEdit3.Text,[loCaseInsensitive, loPartialKey]);
      prRowFocus(ViewInv);
    end;
  end;
end;

procedure TfmAddInv.cxTextEdit3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
Var iCode:Integer;  
begin
  if (Key=$0D) then
  begin
    iCode:=StrToIntDef(cxTextEdit3.Text,0);
    if iCode>0 then
    begin
      if cxButton1.Enabled then
      begin
        if taSpec1.Active then
          taSpec1.Locate('IdGoods',iCode,[]);
      end else
      begin
        if taSpec.Active then
           taSpec.Locate('IdGoods',iCode,[]);
      end;
      prRowFocus(ViewInv);
    end;
    GridInv.SetFocus;
  end;
end;

procedure TfmAddInv.acFastSaveExecute(Sender: TObject);
Var Idh:Integer;
    iM,iC,iSS:INteger;
    Sum1,Sum11,Sum2,Sum21,Sum3,Pr0,Sum0,PrF0,SumF0,SumTR,SumTF,SumTD,rQf:Real;

begin
  //��������� �������������� ������
  if cxButton1.Enabled=False then exit;

  Memo1.Clear;
  prButton(False);

  prWH('����� ... ���� ���������� ������.',Memo1); delay(10);

  with dmO do
  with dmORep do
  begin
    if taSpec1.State in [dsEdit,dsInsert] then taSpec1.Post;

    prAllViewOff;
    //�������� ���������

    iSS:=prISS(cxLookupComboBox1.EditValue);

    prWH('  ���������.',Memo1); delay(10);

    IDH:=cxTextEdit1.Tag;
    if cxTextEdit1.Tag=0 then
    begin
      IDH:=GetId('InvH');
      cxTextEdit1.Tag:=IDH;
      if cxTextEdit1.Text=prGetNum(3,0) then prGetNum(3,1); //��������
    end;

    quDocsInvId.Active:=False;

    quDocsInvId.ParamByName('IDH').AsInteger:=IDH;
    quDocsInvId.Active:=True;

    quDocsInvId.First;
    if quDocsInvId.RecordCount=0 then quDocsInvId.Append else quDocsInvId.Edit;

    quDocsInvIdID.AsInteger:=IDH;
    quDocsInvIdDATEDOC.AsDateTime:=cxDateEdit1.Date;
    quDocsInvIdNUMDOC.AsString:=cxTextEdit1.Text;
    quDocsInvIdIDSKL.AsInteger:=cxLookupComboBox1.EditValue;
    quDocsInvIdIACTIVE.AsInteger:=0;
    quDocsInvIdITYPE.AsInteger:=0;
    quDocsInvIdSUM1.AsFloat:=0;
    quDocsInvIdSUM11.AsFloat:=0;
    quDocsInvIdSUM2.AsFloat:=0;
    quDocsInvIdSUM21.AsFloat:=0;
    quDocsInvIdSUM3.AsFloat:=0;
    quDocsInvIdSUM31.AsFloat:=0;
    quDocsInvIdCOMMENT.AsString:=cxTextEdit2.Text;
    quDocsInvId.Post;

    cxTextEdit1.Tag:=IDH;

    prWH('   ���������� ������ � �����.',Memo1);

    //�������� ������������ ������ � �����
    quSpecINv.Active:=False;
    quSpecInv.ParamByName('IDH').AsInteger:=IDH;
    quSpecInv.Active:=True;

    quSpecInv.First;
    while not quSpecInv.Eof do quSpecInv.Delete;

    PBar1.Properties.Min:=0;
    PBar1.Properties.Max:=taSpec1.RecordCount;
    PBar1.Position:=0;
    PBar1.Visible:=True;

    if taSpec1.RecordCount>100 then iM:=Trunc(taSpec1.RecordCount/100)
    else iM:=1;

    iC:=0;

    taSpec1.First;
    while not taSpec1.Eof do
    begin
      if (abs(taSpec1Quant.AsFloat)>=0.0001) or (abs(taSpec1QuantFact.AsFloat)>=0.0001) or (abs(taSpec1SumInTO.AsFloat)>=0.0001) then
      begin //������� �� ��������� ������
        quSpecInv.Append;
        quSpecInvIDHEAD.AsInteger:=IDH;
        quSpecInvID.AsInteger:=GetId('InvS');
        quSpecInvNUM.AsInteger:=taSpec1Num.AsInteger;
        quSpecInvIDCARD.AsInteger:=taSpec1IdGoods.AsInteger;
        quSpecInvQUANT.AsFloat:=taSpec1Quant.AsFloat;
        quSpecInvSUMIN.AsFloat:=taSpec1SumIn.AsFloat;
        quSpecInvSUMINR.AsFloat:=taSpec1SumInTO.AsFloat;
        quSpecInvSUMUCH.AsFloat:=taSpec1SumUch.AsFloat;
        quSpecInvIDMESSURE.AsInteger:=taSpec1IM.AsInteger;
        quSpecInvKM.AsFloat:=taSpec1Km.AsFloat;
        quSpecInvQUANTFACT.AsFloat:=taSpec1QuantFact.AsFloat;
        quSpecInvSUMINFACT.AsFloat:=taSpec1SumInF.AsFloat;
        quSpecInvSUMUCHFACT.AsFloat:=taSpec1SumUchF.AsFloat;
        quSpecInvTCARD.AsInteger:=taSpec1TCard.AsInteger;
        quSpecInvIDGROUP.AsInteger:=0;
        quSpecInvNOCALC.AsInteger:=taSpec1NoCalc.AsInteger;
        quSpecInvSUMIN0.AsFloat:=taSpec1SumIn0.AsFloat;
        quSpecInvSUMINFACT0.AsFloat:=taSpec1SumInF0.AsFloat;
        quSpecInvNDSPROC.AsFloat:=taSpec1NDSProc.AsFloat;
        quSpecInvNDSSUM.AsFloat:=taSpec1SumNDS.AsFloat;
        quSpecInvIDGROUP.AsInteger:=taSpec1IdGroup.AsInteger;
        quSpecInv.Post;
      end;
      taSpec1.Next; delay(10);

      inc(iC);
      if iC mod iM = 0 then
      begin
        PBar1.Position:=iC;
        delay(10);
      end;
    end;

    Delay(100);
    PBar1.Visible:=False;

    quSpecINv.Active:=False;


    iC:=0;

    //�������� ������������ ������ � �����
    quSpecINvC.Active:=False;
    quSpecInvC.ParamByName('IDH').AsInteger:=IDH;
    quSpecInvC.Active:=True;

    quSpecInvC.First;
    while not quSpecInvC.Eof do quSpecInvC.Delete;

    Sum1:=0;  Sum11:=0; Sum2:=0;  Sum21:=0; Sum3:=0;
    SumTR:=0; SumTF:=0; SumTD:=0;
    Sum0:=0;  SumF0:=0;

    taSpecC1.First;
    while not taSpecC1.Eof do
    begin
      rQf:=taSpecC1QuantFact.AsFloat;
      if abs(rQf)<0.00000001 then rQf:=0.000001;
      quSpecInvC.Append;
      quSpecInvCIDHEAD.AsInteger:=IDH;
      quSpecInvCID.AsInteger:=taSpecC1Num.AsInteger;
      quSpecInvCNUM.AsInteger:=taSpecC1Num.AsInteger;
      quSpecInvCIDCARD.AsInteger:=taSpecC1IdGoods.AsInteger;
      quSpecInvCQUANT.AsFloat:=taSpecC1Quant.AsFloat;
      quSpecInvCSUMIN.AsFloat:=taSpecC1SumIn.AsFloat;
      quSpecInvCSUMINR.AsFloat:=taSpecC1SumInTO.AsFloat;
      quSpecInvCSUMUCH.AsFloat:=taSpecC1SumUch.AsFloat;
      quSpecInvCIDMESSURE.AsInteger:=taSpecC1IM.AsInteger;
      quSpecInvCKM.AsFloat:=taSpecC1Km.AsFloat;
      quSpecInvCQUANTFACT.AsFloat:=rQf;
      quSpecInvCSUMINFACT.AsFloat:=taSpecC1SumInF.AsFloat;
      quSpecInvCSUMUCHFACT.AsFloat:=taSpecC1SumUchF.AsFloat;
      quSpecInvCTCARD.AsInteger:=taSpecC1TCard.AsInteger;
      quSpecInvCIDGROUP.AsInteger:=0;
      quSpecInvCSUMIN0.AsFloat:=taSpecC1SumIn0.AsFloat;
      quSpecInvCSUMINFACT0.AsFloat:=taSpecC1SumInF0.AsFloat;
      quSpecInvCNDSPROC.AsFloat:=taSpecC1NDSProc.AsFloat;
      quSpecInvCNDSSUM.AsFloat:=taSpecC1SumNDS.AsFloat;
      quSpecInvC.Post;

      if taSpecC1CType.AsInteger=4 then
      begin //����
        SumTR:=SumTR+taSpecC1SumIn.AsFloat;
        SumTF:=SumTF+taSpecC1SumInF.AsFloat;
        SumTD:=SumTF-SumTR;

      end else //������
      begin
        Sum1:=Sum1+taSpecC1SumIn.AsFloat;
        Sum11:=Sum11+taSpecC1SumUch.AsFloat;
        Sum2:=Sum2+taSpecC1SumInF.AsFloat;
        Sum21:=Sum21+taSpecC1SumUchF.AsFloat;
        Sum3:=Sum3+taSpecC1SumInTO.AsFloat;
        Sum0:=Sum0+taSpecC1SumIn0.AsFloat;
        SumF0:=SumF0+taSpecC1SumInF0.AsFloat;
      end;

      taSpecC1.Next; delay(10);

      inc(iC);
      if iC mod iM = 0 then
      begin
        PBar1.Position:=iC;
        delay(10);
      end;
    end;
    quSpecINvC.Active:=False;

    Delay(100);
    PBar1.Visible:=False;

    quDocsInvId.Edit;
    quDocsInvIdSUM11.AsFloat:=Sum11;
    quDocsInvIdSUM21.AsFloat:=Sum21;
    quDocsInvIdSUM3.AsFloat:=Sum3;
    quDocsInvIdSUMTARAR.AsFloat:=SumTR;
    quDocsInvIdSUMTARAF.AsFloat:=SumTF;
    quDocsInvIdSUMTARAD.AsFloat:=SumTD;

    if iSS<>2 then
    begin
      quDocsInvIdSUM1.AsFloat:=Sum1;
      quDocsInvIdSUM2.AsFloat:=Sum2;
    end else
    begin
      quDocsInvIdSUM1.AsFloat:=Sum0;
      quDocsInvIdSUM2.AsFloat:=SumF0;
    end;

    quDocsInvId.Post;

    quDocsInvId.Active:=False;

    fmDocsInv.ViewDocsInv.BeginUpdate;
    quDocsInvSel.FullRefresh;
    quDocsInvSel.Locate('ID',IDH,[]);
    fmDocsInv.ViewDocsInv.EndUpdate;

    prAllViewOn;
  end;
  prWH('���������� ��.',Memo1);
  prButton(True);
end;

procedure TfmAddInv.RepInvUserFunction(const Name: String; p1, p2,
  p3: Variant; var Val: Variant);
Var Str1:String;
begin
//  if AnsiCompareText('SUMSTR', Name) = 0 then val.My_Convertion_Routine(frParser.Calc(p1));
  Str1:=MoneyToString(frParser.Calc(p1),True,False);
  if AnsiCompareText('SUMSTR', Name) = 0 then val:=Str1;
  if AnsiCompareText('DIGITSTR', Name) = 0 then
  begin
    if POS('���',Str1)>0 then Str1:=Copy(Str1,1,POS('���',Str1)-1);
    val:=Str1;
  end;
end;

procedure TfmAddInv.taSpec1BeforePost(DataSet: TDataSet);
begin
  taSpec1QuantDif.AsFloat:=taSpec1QuantFact.AsFloat-taSpec1Quant.AsFloat;
  taSpec1SumInDif.AsFloat:=taSpec1SumInF.AsFloat-taSpec1SumIn.AsFloat;
  taSpec1SumUchDif.AsFloat:=taSpec1SumUchF.AsFloat-taSpec1SumUch.AsFloat;
  taSpec1SumInTODif.AsFloat:=taSpec1SumInF.AsFloat-taSpec1SumInTO.AsFloat;
  taSpec1SumInDif0.AsFloat:=taSpec1SumInF0.AsFloat-taSpec1SumIn0.AsFloat;
  taSpec1SumNDS.AsFloat:=taSpec1SumInF.AsFloat-taSpec1SumInF0.AsFloat;
end;

procedure TfmAddInv.taSpecC1BeforePost(DataSet: TDataSet);
begin
  taSpecC1QuantDif.AsFloat:=taSpecC1QuantFact.AsFloat-taSpecC1Quant.AsFloat;
  taSpecC1SumInDif.AsFloat:=taSpecC1SumInF.AsFloat-taSpecC1SumIn.AsFloat;
  taSpecC1SumUchDif.AsFloat:=taSpecC1SumUchF.AsFloat-taSpecC1SumUch.AsFloat;
  taSpecC1SumInTODif.AsFloat:=taSpecC1SumInF.AsFloat-taSpecC1SumInTO.AsFloat;
  taSpecC1SumInDif0.AsFloat:=taSpecC1SumInF0.AsFloat-taSpecC1SumIn0.AsFloat;
  taSpecC1SumNDS.AsFloat:=taSpecC1SumInF.AsFloat-taSpecC1SumInF0.AsFloat;
end;

procedure TfmAddInv.taSpec1PriceInFChange(Sender: TField);
begin
  //���������� ����
  if iCol=2 then
  begin
    taSpec1SumInF.AsFloat := taSpec1PriceInF.AsFloat*taSpec1QuantFact.AsFloat;
    taSpec1SumInF0.AsFloat := rv(taSpec1SumInF.AsFloat-(taSpec1NDSProc.AsFloat*taSpec1SumInF.AsFloat/(100+taSpec1NDSProc.AsFloat)));
    taSpec1PriceInF0.AsFloat := 0;
    if taSpec1QuantFact.AsFloat<>0 then
    begin
      taSpec1PriceInF0.AsFloat:=rv(taSpec1SumInF0.AsFloat/taSpec1QuantFact.AsFloat);
    end;
  end;
end;

procedure TfmAddInv.taSpec1SumInFChange(Sender: TField);
begin
  if iCol=3 then  //���������� �����
  begin
    taSpec1PriceInF.AsFloat:=0;
    taSpec1PriceInF0.AsFloat:=0;
    if taSpec1QuantFact.AsFloat<>0 then
    begin
      taSpec1PriceInF.AsFloat:=RoundEx(taSpec1SumInF.AsFloat/taSpec1QuantFact.AsFloat*100)/100;
      taSpec1SumInF0.AsFloat := rv(taSpec1SumInF.AsFloat-(taSpec1NDSProc.AsFloat*taSpec1SumInF.AsFloat/(100+taSpec1NDSProc.AsFloat)));
      taSpec1PriceInF0.AsFloat:=rv(taSpec1SumInF0.AsFloat/taSpec1QuantFact.AsFloat);
    end;
  end;
end;

procedure TfmAddInv.taSpec1PriceInF0Change(Sender: TField);
begin
  //���������� ���� ��� ���
  if iCol=6 then
  begin
    taSpec1SumInF0.AsFloat := taSpec1PriceInF0.AsFloat*taSpec1QuantFact.AsFloat;
    taSpec1SumInF.AsFloat := rv(taSpec1SumInF0.AsFloat+(taSpec1NDSProc.AsFloat*taSpec1SumInF0.AsFloat/100));

    if taSpec1QuantFact.AsFloat<>0 then
    begin
      taSpec1PriceInF.AsFloat:=rv(taSpec1SumInF.AsFloat/taSpec1QuantFact.AsFloat);
    end;

  end;
end;

procedure TfmAddInv.taSpec1SumInF0Change(Sender: TField);
begin
  //���������� ���� ��� ���
  if iCol=7 then
  begin
    taSpec1PriceInF.AsFloat:=0;
    taSpec1PriceInF0.AsFloat:=0;
    if taSpec1QuantFact.AsFloat<>0 then
    begin
      taSpec1PriceInF0.AsFloat:=RoundEx(taSpec1SumInF0.AsFloat/taSpec1QuantFact.AsFloat*100)/100;
      taSpec1SumInF.AsFloat := rv(taSpec1SumInF0.AsFloat+(taSpec1NDSProc.AsFloat*taSpec1SumInF0.AsFloat/100));
      taSpec1PriceInF.AsFloat:=rv(taSpec1SumInF.AsFloat/taSpec1QuantFact.AsFloat);
    end;
  end;
end;

procedure TfmAddInv.taSpecSumInTOChange(Sender: TField);
begin
  //���������� ���� ��� ���
  if iCol=8 then
  begin
    taSpec1SumInTODif.AsFloat:=taSpec1SumInF.AsFloat-taSpec1SumInTO.AsFloat;
  end;
end;

procedure TfmAddInv.taSpecCSumInTOChange(Sender: TField);
begin
  taSpecCSumInTODif.AsFloat:=taSpecCSumInF.AsFloat-taSpecCSumInTO.AsFloat;
end;

procedure TfmAddInv.acDetPosDocInvExecute(Sender: TObject);
Var iDateB,iDateE:INteger;
    IdCard:INteger;
    NameC:String;
    RecC:INteger;
    iMH:INteger;
begin
  //����������� �� ����������
  with dmO do
  with dmORep do
  begin
    RecC:=0;
    IdCard:=0;

    if PageControl1.ActivePageIndex=0 then
    begin
      if cxButton1.Enabled then
      begin
        RecC:=taSpec1.RecordCount;
        IdCard:=taSpec1IdGoods.AsInteger;
        NameC:=taSpec1NameG.AsString;
      end else
      begin
        RecC:=taSpec.RecordCount;
        IdCard:=taSpecIdGoods.AsInteger;
        NameC:=taSpecNameG.AsString;
      end;
    end;

    if PageControl1.ActivePageIndex=1 then
    begin
      if cxButton1.Enabled then
      begin
        RecC:=taSpecC1.RecordCount;
        IdCard:=taSpecC1IdGoods.AsInteger;
        NameC:=taSpecC1NameG.AsString;
      end else
      begin
        RecC:=taSpecC.RecordCount;
        IdCard:=taSpecCIdGoods.AsInteger;
        NameC:=taSpecCNameG.AsString;
      end;
    end;

    if RecC>0 then
    begin
      //�� ������
      fmSelPerSkl:=tfmSelPerSkl.Create(Application);
      with fmSelPerSkl do
      begin
        cxDateEdit1.Date:=CommonSet.DateFrom;
        quMHAll1.Active:=False;
        quMHAll1.ParamByName('IDPERSON').AsInteger:=Person.Id;
        quMHAll1.Active:=True;
        quMHAll1.First;
        if CommonSet.IdStore>0 then cxLookupComboBox1.EditValue:=CommonSet.IdStore
        else  cxLookupComboBox1.EditValue:=quMHAll1ID.AsInteger;
        cxRadioButton1.Visible:=True;
        cxRadioButton2.Visible:=True;
      end;
      fmSelPerSkl.ShowModal;
      if fmSelPerSkl.ModalResult=mrOk then
      begin
        iDateB:=Trunc(fmSelPerSkl.cxDateEdit1.Date);
        iDateE:=Trunc(fmSelPerSkl.cxDateEdit2.Date);
        if fmSelPerSkl.cxCheckBox1.Checked=False then iDateE:=Trunc(Date);

//        if fmSelPerSkl.cxRadioButton1.Checked then iType:=0 else iType:=1;
        // ���� ������� �� ���������� �������� �� ���� ��� ������ - ����� ��������� ����������� ����

        //������ �� ����� � ������

        CommonSet.IdStore:=fmSelPerSkl.cxLookupComboBox1.EditValue;
        CommonSet.NameStore:=fmSelPerSkl.cxLookupComboBox1.Text;
        CommonSet.DateFrom:=iDateB;
        CommonSet.DateTo:=iDateE;

        iMH:=fmSelPerSkl.cxLookupComboBox1.EditValue;

        fmSelPerSkl.Release;

        fmDetPosSum.Caption:='����������� ���������� �� ������� - '+its(IdCard)+' '+NameC+' �� ������ � '+ds1(iDateB)+' �� '+ds1(iDateE);
        fmDetPosSum.Show;
        fmDetPosSum.ViewPosDetDoc.BeginUpdate;
        CloseTe(fmDetPosSum.teDocLn);
        prGetDetPosDoc(IdCard,iDateB,iDateE,iMh,NameC,fmDetPosSum.Memo1,fmDetPosSum.teDocLn);
        fmDetPosSum.ViewPosDetDoc.EndUpdate;

      end else fmSelPerSkl.Release;
    end;
  end;
end;

procedure TfmAddInv.acGetSumTOPosExecute(Sender: TObject);
Var iSS,IdSkl,iCode:INteger;
    BDate:TDateTime;
begin
  //������ ����� �� �� ����� �������
{        prCalcSumRemn.ParamByName('IDCARD').AsInteger:=taSpecC1IdGoods.AsInteger;
        prCalcSumRemn.ParamByName('IDSKL').AsInteger:=cxLookupComboBox1.EditValue;
        prCalcSumRemn.ParamByName('DDATE').AsDate:=cxDateEdit1.Date;
        prCalcSumRemn.ParamByName('ISS').AsInteger:=iSS;
        prCalcSumRemn.ExecProc;
        rSumTO:=prCalcSumRemn.ParamByName('REMNSUM').AsFloat;   // ��������� ����� ������������
}

  IdSkl:=cxLookupComboBox1.EditValue;
  iCode:=taSpecC1IdGoods.AsInteger;
  iSS:=prISS(IdSkl);

  if taSpecC1.RecordCount=0 then exit;
  
  Memo1.Clear;
  Memo1.Lines.Add('������ ������� ����� �� �� �� 1-� �������.');
  Memo1.Lines.Add('�� - '+its(IdSkl));
  Memo1.Lines.Add('��� ������ - '+its(iCode));
  Memo1.Lines.Add('�� ���� - '+ds1(cxDateEdit1.Date));
  Memo1.Lines.Add('���� (ISS) - '+its(iSS));

  quS.Active:=False;
  quS.SelectSQL.Clear;
  quS.SelectSQL.Add('  SELECT first 1 dh.DATEDOC,sp.SUMINFACT,sp.SUMUCHFACT,sp.SUMINFACT0 FROM OF_DOCSPECINVC sp');
  quS.SelectSQL.Add('  left join of_docheadinv dh on sp.IDHEAD=dh.ID');
  quS.SelectSQL.Add('  where IDCARD='+its(iCode)+' and dh.DATEDOC<='''+ds1(cxDateEdit1.Date)+''' and dh.IDSKL='+its(IdSkl)+' and dh.IACTIVE=1');
  quS.SelectSQL.Add('  order by dh.DATEDOC desc');
  quS.Active:=True;

  if quS.RecordCount>0 then  bDate:=quS.FieldByName('DATEDOC').AsDateTime else bDate:=StrToDate('01.01.2000');

  quS.Active:=False;

  Memo1.Lines.Add('���� ������ ������� - '+ds1(bDate));

  // �������
  quS.Active:=False;
  quS.SelectSQL.Clear;
  quS.SelectSQL.Add('  SELECT SUM(sp.SUMIN)as SUMIN, SUM(sp.SUM0) as SUMIN0');
  quS.SelectSQL.Add('  FROM OF_DOCSPECIN sp');
  quS.SelectSQL.Add('  left join of_docheadin dh on dh.ID=sp.IDHEAD');
  quS.SelectSQL.Add('  where sp.IDCARD='+its(iCode)+' and dh.DATEDOC>'''+ds1(bDate)+''' and dh.DATEDOC<='''+ds1(cxDateEdit1.Date)+''' and dh.IDSKL='+its(IdSkl)+' and dh.IACTIVE=1');
  quS.Active:=True;

  if quS.RecordCount>0 then
  begin
    Memo1.Lines.Add('------');
    Memo1.Lines.Add('����� ����. � ��� - '+fts(quS.FieldByName('SUMIN').AsFloat));
    Memo1.Lines.Add('����� ����. ��� ��� - '+fts(quS.FieldByName('SUMIN0').AsFloat));
  end else Memo1.Lines.Add('������� ���.');
  quS.Active:=False;

  //���������� ������
  quS.Active:=False;
  quS.SelectSQL.Clear;
  quS.SelectSQL.Add('  SELECT SUM(sp.SUMIN) as SUMIN');
  quS.SelectSQL.Add('  FROM OF_DOCSPECVN sp');
  quS.SelectSQL.Add('  left join of_docheadvn dh on dh.ID=sp.IDHEAD');
  quS.SelectSQL.Add('  where sp.IDCARD='+its(iCode)+' and dh.DATEDOC>'''+ds1(bDate)+''' and dh.DATEDOC<='''+ds1(cxDateEdit1.Date)+''' and dh.IDSKL_TO='+its(IdSkl)+' and dh.IACTIVE=1');
  quS.Active:=True;

  if quS.RecordCount>0 then
  begin
    Memo1.Lines.Add('------');
    Memo1.Lines.Add('����� ����������� ������� - '+fts(quS.FieldByName('SUMIN').AsFloat));
  end else Memo1.Lines.Add('����������� ������� ���.');
  quS.Active:=False;

  //���� �����������
  quS.Active:=False;
  quS.SelectSQL.Clear;
  quS.SelectSQL.Add('  SELECT SUM(sp.SUMIN)as SUMIN, SUM(sp.SUMIN0) as SUMIN0');
  quS.SelectSQL.Add('  FROM OF_DOCSPECACTSIN sp');
  quS.SelectSQL.Add('  left join of_docheadacts dh on dh.ID=sp.IDHEAD');
  quS.SelectSQL.Add('  where sp.IDCARD='+its(iCode)+' and dh.DATEDOC>'''+ds1(bDate)+''' and dh.DATEDOC<='''+ds1(cxDateEdit1.Date)+''' and dh.IDSKL='+its(IdSkl)+' and dh.IACTIVE=1');
  quS.Active:=True;

  if quS.RecordCount>0 then
  begin
    Memo1.Lines.Add('------');
    Memo1.Lines.Add('������ �� ����� �������. � ��� - '+fts(quS.FieldByName('SUMIN').AsFloat));
    Memo1.Lines.Add('������ �� ����� �������. ��� ��� - '+fts(quS.FieldByName('SUMIN0').AsFloat));
  end else Memo1.Lines.Add('������� �� ����� ����������� ���.');
  quS.Active:=False;

  //������������
  quS.Active:=False;
  quS.SelectSQL.Clear;
  quS.SelectSQL.Add('  SELECT SUM(sp.SUMIN)as SUMIN, SUM(sp.SUMIN0) as SUMIN0');
  quS.SelectSQL.Add('  FROM OF_DOCSPECCOMPLB sp');
  quS.SelectSQL.Add('  left join of_docheadcompl dh on dh.ID=sp.IDHEAD');
  quS.SelectSQL.Add('  where sp.IDCARD='+its(iCode)+' and dh.DATEDOC>'''+ds1(bDate)+''' and dh.DATEDOC<='''+ds1(cxDateEdit1.Date)+''' and dh.IDSKL='+its(IdSkl)+' and dh.IACTIVE=1');
  quS.Active:=True;

  if quS.RecordCount>0 then
  begin
    Memo1.Lines.Add('------');
    Memo1.Lines.Add('������ �� �����. � ��� - '+fts(quS.FieldByName('SUMIN').AsFloat));
    Memo1.Lines.Add('������ �� �����. ��� ��� - '+fts(quS.FieldByName('SUMIN0').AsFloat));
  end else Memo1.Lines.Add('������� �� ������������ ���.');
  quS.Active:=False;

  // ����������
  quS.Active:=False;
  quS.SelectSQL.Clear;
  quS.SelectSQL.Add('  SELECT SUM(sp.SUMIN)as SUMIN, SUM(sp.SUMIN0) as SUMIN0');
  quS.SelectSQL.Add('  FROM of_docspecoutc sp');
  quS.SelectSQL.Add('  left join of_docheadoutb dh on dh.ID=sp.IDHEAD');
  quS.SelectSQL.Add('  where sp.ARTICUL='+its(iCode)+' and dh.DATEDOC>'''+ds1(bDate)+''' and dh.DATEDOC<='''+ds1(cxDateEdit1.Date)+''' and dh.IDSKL='+its(IdSkl)+' and dh.IACTIVE=1');
  quS.Active:=True;

  if quS.RecordCount>0 then
  begin
    Memo1.Lines.Add('------');
    Memo1.Lines.Add('���������� � ��� - '+fts(quS.FieldByName('SUMIN').AsFloat));
    Memo1.Lines.Add('���������� ��� ��� - '+fts(quS.FieldByName('SUMIN0').AsFloat));
  end else Memo1.Lines.Add('���������� ���.');
  quS.Active:=False;


  // ���������� �� �������
  quS.Active:=False;
  quS.SelectSQL.Clear;
  quS.SelectSQL.Add('  SELECT SUM(sp.SUMIN)as SUMIN, SUM(sp.SUMIN0) as SUMIN0');
  quS.SelectSQL.Add('  FROM of_docspecoutr sp');
  quS.SelectSQL.Add('  left join of_docheadoutr dh on dh.ID=sp.IDHEAD');
  quS.SelectSQL.Add('  where sp.IDCARD='+its(iCode)+' and dh.DATEDOC>'''+ds1(bDate)+''' and dh.DATEDOC<='''+ds1(cxDateEdit1.Date)+''' and dh.IDSKL='+its(IdSkl)+' and dh.IACTIVE=1');
  quS.Active:=True;

  if quS.RecordCount>0 then
  begin
    Memo1.Lines.Add('------');
    Memo1.Lines.Add('���������� �� ������� � ��� - '+fts(quS.FieldByName('SUMIN').AsFloat));
    Memo1.Lines.Add('���������� �� ������� ��� ��� - '+fts(quS.FieldByName('SUMIN0').AsFloat));
  end else Memo1.Lines.Add('���������� �� ������� ���.');
  quS.Active:=False;


  // �������
  quS.Active:=False;
  quS.SelectSQL.Clear;
  quS.SelectSQL.Add('  SELECT SUM(sp.SUMIN)as SUMIN, SUM(sp.SUMIN0) as SUMIN0');
  quS.SelectSQL.Add('  FROM of_docspecout sp');
  quS.SelectSQL.Add('  left join of_docheadout dh on dh.ID=sp.IDHEAD');
  quS.SelectSQL.Add('  where sp.IDCARD='+its(iCode)+' and dh.DATEDOC>'''+ds1(bDate)+''' and dh.DATEDOC<='''+ds1(cxDateEdit1.Date)+''' and dh.IDSKL='+its(IdSkl)+' and dh.IACTIVE=1');
  quS.Active:=True;

  if quS.RecordCount>0 then
  begin
    Memo1.Lines.Add('------');
    Memo1.Lines.Add('������� � ��� - '+fts(quS.FieldByName('SUMIN').AsFloat));
    Memo1.Lines.Add('������� ��� ��� - '+fts(quS.FieldByName('SUMIN0').AsFloat));
  end else Memo1.Lines.Add('�������� ���.');
  quS.Active:=False;


  // ��. ������
  quS.Active:=False;
  quS.SelectSQL.Clear;
  quS.SelectSQL.Add('  SELECT SUM(sp.SUMIN)as SUMIN');
  quS.SelectSQL.Add('  FROM OF_DOCSPECVN sp');
  quS.SelectSQL.Add('  left join of_docheadvn dh on dh.ID=sp.IDHEAD');
  quS.SelectSQL.Add('  where sp.IDCARD='+its(iCode)+' and dh.DATEDOC>'''+ds1(bDate)+''' and dh.DATEDOC<='''+ds1(cxDateEdit1.Date)+''' and dh.IDSKL_FROM='+its(IdSkl)+' and dh.IACTIVE=1');
  quS.Active:=True;

  if quS.RecordCount>0 then
  begin
    Memo1.Lines.Add('------');
    Memo1.Lines.Add('��. ������ � ��� - '+fts(quS.FieldByName('SUMIN').AsFloat));
//    Memo1.Lines.Add('��. ������ ��� ��� - '+fts(quS.FieldByName('SUMIN0').AsFloat));
  end else Memo1.Lines.Add('��. ������� ���.');
  quS.Active:=False;


  // ���� ����������� ������
  quS.Active:=False;
  quS.SelectSQL.Clear;
  quS.SelectSQL.Add('  SELECT SUM(sp.SUMIN)as SUMIN, SUM(sp.SUMIN0) as SUMIN0');
  quS.SelectSQL.Add('  FROM OF_DOCSPECACTSOUT sp');
  quS.SelectSQL.Add('  left join of_docheadacts dh on dh.ID=sp.IDHEAD');
  quS.SelectSQL.Add('  where sp.IDCARD='+its(iCode)+' and dh.DATEDOC>'''+ds1(bDate)+''' and dh.DATEDOC<='''+ds1(cxDateEdit1.Date)+''' and dh.IDSKL='+its(IdSkl)+' and dh.IACTIVE=1');
  quS.Active:=True;

  if quS.RecordCount>0 then
  begin
    Memo1.Lines.Add('------');
    Memo1.Lines.Add('���� ����������� ������ � ��� - '+fts(quS.FieldByName('SUMIN').AsFloat));
    Memo1.Lines.Add('���� ����������� ������ ��� ��� - '+fts(quS.FieldByName('SUMIN0').AsFloat));
  end else Memo1.Lines.Add('���� ����������� - ������� ���.');
  quS.Active:=False;

  // ������������ ������
  quS.Active:=False;
  quS.SelectSQL.Clear;
  quS.SelectSQL.Add('  SELECT SUM(sp.SUMIN)as SUMIN, SUM(sp.SUMIN0) as SUMIN0');
  quS.SelectSQL.Add('  FROM of_docspeccomplc sp');
  quS.SelectSQL.Add('  left join of_docheadcompl dh on dh.ID=sp.IDHEAD');
  quS.SelectSQL.Add('  where sp.IDCARD='+its(iCode)+' and dh.DATEDOC>'''+ds1(bDate)+''' and dh.DATEDOC<='''+ds1(cxDateEdit1.Date)+''' and dh.IDSKL='+its(IdSkl)+' and dh.IACTIVE=1');
  quS.Active:=True;

  if quS.RecordCount>0 then
  begin
    Memo1.Lines.Add('------');
    Memo1.Lines.Add('������������ ������ � ��� - '+fts(quS.FieldByName('SUMIN').AsFloat));
    Memo1.Lines.Add('������������ ������ ��� ��� - '+fts(quS.FieldByName('SUMIN0').AsFloat));
  end else Memo1.Lines.Add('������������ - ������� ���.');
  quS.Active:=False;

{
  /* �� ������ */
  SELECT SUM(sp.SUMIN) as SUMIN
  FROM OF_DOCSPECVN sp
  left join of_docheadvn dh on dh.ID=sp.IDHEAD
  where sp.IDCARD=:IDCARD and dh.DATEDOC>:DDATEBEG and dh.DATEDOC<=:DDATE and dh.IDSKL_TO=:IDSKL and dh.IACTIVE=1
  INTO :RSUM;

  if (RSUM is null) then RSUM=0;
  RESSUM=:RESSUM+:RSUM;
  RESSUM0=:RESSUM0+:RSUM;/* ������� ��� ��. �������� ���������, �� ����� � ��� */

  /* ���� ����������� */
  SELECT SUM(sp.SUMIN),SUM(sp.SUMIN0) as SUMIN
  FROM OF_DOCSPECACTSIN sp
  left join of_docheadacts dh on dh.ID=sp.IDHEAD
  where sp.IDCARD=:IDCARD and dh.DATEDOC>:DDATEBEG and dh.DATEDOC<=:DDATE and dh.IDSKL=:IDSKL and dh.IACTIVE=1
  INTO :RSUM,:RSUM0;

  if (RSUM is null) then RSUM=0;
  if (RSUM0 is null) then RSUM0=0;
  RESSUM=:RESSUM+:RSUM;
  RESSUM0=:RESSUM0+:RSUM0;

  /* ������������ */
  SELECT SUM(sp.SUMIN),SUM(sp.SUMIN0) as SUMIN
  FROM OF_DOCSPECCOMPLB sp
  left join of_docheadcompl dh on dh.ID=sp.IDHEAD
  where sp.IDCARD=:IDCARD and dh.DATEDOC>:DDATEBEG and dh.DATEDOC<=:DDATE and dh.IDSKL=:IDSKL and dh.IACTIVE=1
  INTO :RSUM,:RSUM0;

  if (RSUM is null) then RSUM=0;
  if (RSUM0 is null) then RSUM0=0;
  RESSUM=:RESSUM+:RSUM;
  RESSUM0=:RESSUM0+:RSUM0;

  /* ���������� */
  SELECT SUM(sp.SUMIN),SUM(sp.SUMIN0) as SUMIN
  FROM of_docspecoutc sp
  left join of_docheadoutb dh on dh.ID=sp.IDHEAD
  where sp.ARTICUL=:IDCARD and dh.DATEDOC>:DDATEBEG and dh.DATEDOC<=:DDATE and dh.IDSKL=:IDSKL and dh.IACTIVE=1
  INTO :RSUM,:RSUM0;

  if (RSUM is null) then RSUM=0;
  if (RSUM0 is null) then RSUM0=0;
  RESSUM=:RESSUM-:RSUM;
  RESSUM0=:RESSUM0-:RSUM0;

  /* ���������� �� �������*/
  SELECT SUM(sp.SUMIN),SUM(sp.SUMIN0) as SUMIN
  FROM of_docspecoutr sp
  left join of_docheadoutr dh on dh.ID=sp.IDHEAD
  where sp.IDCARD=:IDCARD and dh.DATEDOC>:DDATEBEG and dh.DATEDOC<=:DDATE and dh.IDSKL=:IDSKL and dh.IACTIVE=1
  INTO :RSUM,:RSUM0;

  if (RSUM is null) then RSUM=0;
  if (RSUM0 is null) then RSUM0=0;
  RESSUM=:RESSUM-:RSUM;
  RESSUM0=:RESSUM0-:RSUM0;

  /* ������� */
  SELECT SUM(sp.SUMIN),SUM(sp.SUMIN0) as SUMIN
  FROM of_docspecout sp
  left join of_docheadout dh on dh.ID=sp.IDHEAD
  where sp.IDCARD=:IDCARD and dh.DATEDOC>:DDATEBEG and dh.DATEDOC<=:DDATE and dh.IDSKL=:IDSKL and dh.IACTIVE=1
  INTO :RSUM,:RSUM0;

  if (RSUM is null) then RSUM=0;
  if (RSUM0 is null) then RSUM0=0;
  RESSUM=:RESSUM-:RSUM;
  RESSUM0=:RESSUM0-:RSUM0;

  /* �� ������ */
  SELECT SUM(sp.SUMIN) as SUMIN
  FROM OF_DOCSPECVN sp
  left join of_docheadvn dh on dh.ID=sp.IDHEAD
  where sp.IDCARD=:IDCARD and dh.DATEDOC>:DDATEBEG and dh.DATEDOC<=:DDATE and dh.IDSKL_FROM=:IDSKL and dh.IACTIVE=1
  INTO :RSUM;

  if (RSUM is null) then RSUM=0;
  RESSUM=:RESSUM-:RSUM;
  RESSUM0=:RESSUM0-:RSUM; /* ������� ��� ��. �������� ���������, �� ����� � ��� */

  /* ���� ����������� ������*/
  SELECT SUM(sp.SUMIN),SUM(sp.SUMIN0) as SUMIN
  FROM OF_DOCSPECACTSOUT sp
  left join of_docheadacts dh on dh.ID=sp.IDHEAD
  where sp.IDCARD=:IDCARD and dh.DATEDOC>:DDATEBEG and dh.DATEDOC<=:DDATE and dh.IDSKL=:IDSKL and dh.IACTIVE=1
  INTO :RSUM,:RSUM0;

  if (RSUM is null) then RSUM=0;
  if (RSUM0 is null) then RSUM0=0;
  RESSUM=:RESSUM-:RSUM;
  RESSUM0=:RESSUM0-:RSUM0;

  /* ������������ ������*/
  SELECT SUM(sp.SUMIN),SUM(sp.SUMIN0) as SUMIN
  FROM of_docspeccomplc sp
  left join of_docheadcompl dh on dh.ID=sp.IDHEAD
  where sp.IDCARD=:IDCARD and dh.DATEDOC>:DDATEBEG and dh.DATEDOC<=:DDATE and dh.IDSKL=:IDSKL and dh.IACTIVE=1
  INTO :RSUM,:RSUM0;

  if (RSUM is null) then RSUM=0;
  if (RSUM0 is null) then RSUM0=0;
  RESSUM=:RESSUM-:RSUM;
  RESSUM0=:RESSUM0-:RSUM0;

  /* �������������� �� ������� ������� ���� - �������� ������� ����� ������ � ������
  SELECT SUM(sp.SUMINFACT-sp.SUMIN) as SUMIN
  FROM of_docspecinvc sp
  left join of_docheadinv dh on dh.ID=sp.IDHEAD
  where sp.IDCARD=:IDCARD and dh.DATEDOC<=:DDATE and dh.IDSKL=:IDSKL and dh.IACTIVE=1
  INTO :RSUM;

  if (RSUM is null) then RSUM=0;
  RESSUM=:RESSUM+:RSUM; */

  if (ISS=2) then
  begin
    REMNSUM=:RESSUM0; /* ��� ��� */
  end
  else
  begin
    REMNSUM=:RESSUM;
  end

  SUSPEND;
END

}

  Memo1.Lines.Add('------');
  Memo1.Lines.Add('');
  Memo1.Lines.Add('������ ��.');
end;

end.
