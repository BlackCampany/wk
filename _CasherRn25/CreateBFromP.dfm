object fmCreateBFromP: TfmCreateBFromP
  Left = 658
  Top = 166
  BorderStyle = bsDialog
  Caption = #1057#1086#1079#1076#1072#1090#1100' '
  ClientHeight = 357
  ClientWidth = 538
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 538
    Height = 81
    Align = alTop
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 20
      Width = 72
      Height = 13
      Caption = #1050#1086#1083'-'#1074#1086' '#1087#1072#1088#1090#1080#1080
    end
    object Label2: TLabel
      Left = 16
      Top = 48
      Width = 26
      Height = 13
      Caption = #1044#1072#1090#1072
    end
    object cxCalcEdit1: TcxCalcEdit
      Left = 104
      Top = 16
      EditValue = 0.000000000000000000
      Style.BorderStyle = ebsOffice11
      Style.Shadow = False
      TabOrder = 0
      Width = 113
    end
    object cxButton1: TcxButton
      Left = 272
      Top = 8
      Width = 101
      Height = 33
      Caption = 'Ok'
      ModalResult = 1
      TabOrder = 1
      Colors.Default = 16776176
      Colors.Normal = 16776176
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 392
      Top = 8
      Width = 101
      Height = 33
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 2
      Colors.Default = 16776176
      Colors.Normal = 16776176
      LookAndFeel.Kind = lfOffice11
    end
    object cxDateEdit1: TcxDateEdit
      Left = 104
      Top = 40
      Style.BorderStyle = ebsOffice11
      TabOrder = 3
      Width = 113
    end
  end
  object GridF: TcxGrid
    Left = 8
    Top = 92
    Width = 521
    Height = 249
    TabOrder = 1
    LookAndFeel.Kind = lfOffice11
    object ViewF: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dsquInputF
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.MultiSelect = True
      OptionsView.GroupByBox = False
      object ViewFIDC: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1073#1083#1102#1076#1072
        DataBinding.FieldName = 'IDC'
        Styles.Content = dmO.cxStyle25
        Width = 46
      end
      object ViewFSHORTNAME: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'SHORTNAME'
        Styles.Content = dmO.cxStyle15
        Width = 135
      end
      object ViewFRECEIPTNUM: TcxGridDBColumn
        Caption = #8470' '#1088#1077#1094#1077#1087#1090#1091#1088#1099
        DataBinding.FieldName = 'RECEIPTNUM'
        Width = 48
      end
      object ViewFPCOUNT: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086' '#1087#1086#1088#1094#1080#1081
        DataBinding.FieldName = 'PCOUNT'
        Width = 39
      end
      object ViewFPVES: TcxGridDBColumn
        Caption = #1042#1077#1089' '#1087#1086#1088#1094#1080#1080' '#1074' '#1075#1088#1072#1084#1084#1072#1093
        DataBinding.FieldName = 'PVES'
        Width = 47
      end
      object ViewFNAMESHORT: TcxGridDBColumn
        Caption = #1045#1076'. '#1080#1079#1084'.'
        DataBinding.FieldName = 'NAMESHORT'
        Width = 44
      end
      object ViewFNETTO2: TcxGridDBColumn
        Caption = #1053#1077#1090#1090#1086
        DataBinding.FieldName = 'NETTO2'
        Styles.Content = dmO.cxStyle1
        Width = 56
      end
      object ViewFSGR: TcxGridDBColumn
        Caption = #1043#1088#1091#1087#1087#1072
        DataBinding.FieldName = 'SGR'
        Visible = False
        Width = 128
      end
      object ViewFSSGR: TcxGridDBColumn
        Caption = #1055#1086#1076#1075#1088#1091#1087#1087#1072
        DataBinding.FieldName = 'SSGR'
        Visible = False
        Width = 126
      end
    end
    object LevelF: TcxGridLevel
      GridView = ViewF
    end
  end
  object quInputF: TpFIBDataSet
    SelectSQL.Strings = (
      
        'SELECT cts.IDC,ct.ID as IDCT,c.PARENT,ct.SHORTNAME,ct.RECEIPTNUM' +
        ',ct.PCOUNT,ct.PVES,cts.CURMESSURE,me.NAMESHORT,cts.NETTO,cts.NET' +
        'TO1,cts.BRUTTO'
      ',cts.NETTO2,me.KOEF'
      'FROM OF_CARDSTSPEC cts'
      ''
      'left join OF_CARDST ct on ct.ID=cts.IDT'
      'left join of_messur me on me.ID=cts.CURMESSURE'
      'left join of_cards c on c.ID=cts.IDC'
      ''
      'where cts.IDCARD=:IDC and ct.DATEE>=:CURDATE'
      'and c.PARENT>0')
    Transaction = dmO.trSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    Left = 60
    Top = 128
    poAskRecordCount = True
    object quInputFIDC: TFIBIntegerField
      FieldName = 'IDC'
    end
    object quInputFSHORTNAME: TFIBStringField
      FieldName = 'SHORTNAME'
      Size = 100
      EmptyStrToNull = True
    end
    object quInputFRECEIPTNUM: TFIBStringField
      FieldName = 'RECEIPTNUM'
      Size = 30
      EmptyStrToNull = True
    end
    object quInputFPCOUNT: TFIBIntegerField
      FieldName = 'PCOUNT'
    end
    object quInputFPVES: TFIBFloatField
      FieldName = 'PVES'
    end
    object quInputFCURMESSURE: TFIBIntegerField
      FieldName = 'CURMESSURE'
    end
    object quInputFNAMESHORT: TFIBStringField
      FieldName = 'NAMESHORT'
      Size = 50
      EmptyStrToNull = True
    end
    object quInputFNETTO: TFIBFloatField
      FieldName = 'NETTO'
      DisplayFormat = '0.###'
    end
    object quInputFPARENT: TFIBIntegerField
      FieldName = 'PARENT'
    end
    object quInputFSGR: TStringField
      FieldKind = fkCalculated
      FieldName = 'SGR'
      Size = 50
      Calculated = True
    end
    object quInputFSSGR: TStringField
      FieldKind = fkCalculated
      FieldName = 'SSGR'
      Size = 50
      Calculated = True
    end
    object quInputFIDCT: TFIBIntegerField
      FieldName = 'IDCT'
    end
    object quInputFNETTO1: TFIBFloatField
      FieldName = 'NETTO1'
    end
    object quInputFBRUTTO: TFIBFloatField
      FieldName = 'BRUTTO'
    end
    object quInputFNETTO2: TFIBFloatField
      FieldName = 'NETTO2'
    end
    object quInputFKOEF: TFIBFloatField
      FieldName = 'KOEF'
    end
  end
  object dsquInputF: TDataSource
    DataSet = quInputF
    Left = 120
    Top = 128
  end
end
