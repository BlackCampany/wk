unit PerA_Close;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxLookAndFeelPainters, StdCtrls, cxButtons, ExtCtrls, ComCtrls,
  DB, ADODB, cxControls, cxContainer, cxEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxImageComboBox, cxDBEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, Menus, cxGraphics;

type
  TfmPerA_Close = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Button1: TcxButton;
    Button2: TcxButton;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    ComboBox1: TcxLookupComboBox;
    Edit1: TcxTextEdit;
    procedure FormCreate(Sender: TObject);
    procedure Edit1Enter(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmPerA_Close: TfmPerA_Close;
  CountEnter:Integer;

implementation

uses Un1, ClosePer;

{$R *.dfm}

procedure TfmPerA_Close.FormCreate(Sender: TObject);
begin
  Left:=1;
  Top:=1;
  CountEnter:=0;
end;

procedure TfmPerA_Close.Edit1Enter(Sender: TObject);
begin
  Text:='';
end;

procedure TfmPerA_Close.Button1Click(Sender: TObject);
Var StrWk:String;
begin
  strwk:=Edit1.Text;
  with fmClosePer do
  begin
    quPer.Locate('ID',ComboBox1.EditValue,[]);
    if quPerCheck.AsString=Edit1.Text then
    begin //������� ������
      Person.Id:=quPerId.AsInteger;
      Person.Name:=quPerName.AsString;
      quPer.Active:=False;
      fmPerA_Close.ModalResult:=1;
    end
    else
    begin
      inc(CountEnter);
      if CountEnter>2 then close;
      showmessage('������ ������������. �������� '+IntToStr(3-CountEnter)+' �������.');
      Edit1.Text:='';
      Edit1.SetFocus;
    end;
  end;
end;

procedure TfmPerA_Close.Button2Click(Sender: TObject);
begin
  fmClosePer.quPer.Active:=False;
  Close;
end;

procedure TfmPerA_Close.FormShow(Sender: TObject);
begin
  with fmClosePer do
  begin
    if RnDb.Connected then
    begin
      quPer.Active:=True;
      ComboBox1.EditValue:=Person.Id;
      StatusBar1.Panels[0].Text:=CommonSet.OfficeDb;
      Edit1.SetFocus;
      Edit1.SelectAll;
    end;
  end;
end;

end.
