unit DC;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, dxfBackGround, ExtCtrls, Menus, cxLookAndFeelPainters,
  StdCtrls, cxButtons, cxMaskEdit, cxDropDownEdit, cxCalc, cxControls,
  cxContainer, cxEdit, cxTextEdit, cxCheckBox, cxCalendar;

type
  TfmAddDC = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    dxfBackGround1: TdxfBackGround;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxTextEdit1: TcxTextEdit;
    cxTextEdit2: TcxTextEdit;
    cxCalcEdit1: TcxCalcEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    CheckBox1: TcxCheckBox;
    Label4: TLabel;
    cxTextEdit3: TcxTextEdit;
    Label5: TLabel;
    cxDateEdit1: TcxDateEdit;
    Label6: TLabel;
    cxTextEdit4: TcxTextEdit;
    cxDateEditDATECARD: TcxDateEdit;
    Label7: TLabel;
    Label8: TLabel;
    cxTextEditMT: TcxTextEdit;
    Panel2: TPanel;
    Label9: TLabel;
    cxTextEditEMAIL: TcxTextEdit;
    cxTextEditAdres: TcxTextEdit;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    cxCalcEditCS: TcxCalcEdit;
    cxCalcEditCB: TcxCalcEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
   sBarOld:String;
    { Public declarations }
  end;

var
  fmAddDC: TfmAddDC;

implementation

uses DmRnDisc;

{$R *.dfm}

procedure TfmAddDC.FormCreate(Sender: TObject);
begin
  cxTextEdit1.Text:='';
  cxTextEdit2.Text:='';
  cxCalcEdit1.Value:=0;
end;

procedure TfmAddDC.FormClose(Sender: TObject; var Action: TCloseAction);
Var sBar:String;
begin
 if ModalResult=mrOk then
 begin
  sBar:=Copy(cxTextEdit2.Text,1,30);
  while pos(' ',sBar)>0 do delete(sBar,pos(' ',sBar),1);

  if SBAR <> SBAROLD then
  begin
   with dmCDisc do
   begin

      quFindDc.Active:=False;
      quFindDc.ParamByName('SBAR').AsString:=sBar;
      quFindDc.Active:=True;

      if quFindDc.RecordCount>0 then
      begin
        showmessage('���������� ����� � ����� ����� ��� ����������. ���������� ����������.');
        Action:=caNone;
      end;
      quFindDc.Active:=False;
   end;
  end;
 end;
end;

end.
