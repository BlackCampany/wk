unit ImportDK;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, cxButtons, StdCtrls, ExtCtrls,
  cxControls, cxContainer, cxEdit, cxTextEdit, cxMemo, cxMaskEdit,
  cxDropDownEdit, cxCalendar, DB, cxCheckBox,
  IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, IdFTP, FileUtil,
  ShellApi,EasyCompression, DBClient,pFIBDataSet, FMTBcd, SqlExpr;



type

  TDiscontRecord = record
     Parent_ID:Integer;
     Parent_NAME:String;
     BARCODE:String;
     NAME:String;
     PERCENT:Real;
     CLIENTINDEX:Integer;
     IACTIVE:Integer;
     PHONE:String;
     BERTHDAY:TDateTime;
     COMMENT:String;
     ChildSmall:Integer;
     ChildBig:Integer;
     DATECARD:TDateTime;
     PHONE_MOBILE:String;
     EMAIL:String;
     ADRES:String;
     SUMMA:Real;
  end;

 TClassifRecord = record
     IDCL:Integer;
     NAMECL:String;
     TYPECL:Integer;
     IDPARENT:Integer;
  end;

  TfmImportDK = class(TForm)
    Memo1: TcxMemo;
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Timer1: TTimer;
    Label3: TLabel;
    IdFTP1: TIdFTP;
    cxButton3: TcxButton;
    SQLStoredProc1: TSQLStoredProc;


    procedure cxButton2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure prStart;
    Function prTr(NameF:String):Boolean;
    Function prTrFiles:Boolean;

  end;

//TSmallintField; TIntegerField; TStringField;

Procedure WrMess(S:String);
Function WrRec(taT:TpFIBDataSet):String;
Function GetSubStrFromLn():String;


var
  fmImportDK: TfmImportDK;
  fs: TECLFileStream;
  NameFileDisccard: string;
  NameFileClassif: string;
  StrDisRec:String;

implementation

uses Un1, u2fdk, Attention, DmRnDisc;

{$R *.dfm}

Function TfmImportDK.prTrFiles:Boolean;
var
  FList,Dest,PList,DList: TStrings;
  i,k,m,iPos:INteger;
  F:TextFile;
  bExit:Boolean;
  StrWk,StrWk1,StrWk2,StrWk3:String;
  BadPath:Boolean;

  Label ExitProc;
begin
  Result:=True;
  WrMess('  �������� �������� ������ �� FTP �������� ��������.');
  bExit:=False;

  //�������� ������ ���� ����� ��� ��������
  FList := TStringList.Create; //� ������ ��� ��������
  DList := TStringList.Create; //��� ����� ��� ����������

  //�������� ����������� �� ���
  Dest := TStringList.Create;

  //�������� ����� �������� �� ��� �� ���
  pList := TStringList.Create;
  StrWk:=CommonSet.FtpExport;
  While StrWk>'' do
  begin
    if Pos(r,strwk)>0 then
    begin
      StrWk1:=Copy(StrWk,1,Pos(r,strwk)-1);
      Delete(StrWk,1,Pos(r,strwk));
    end else
    begin
      StrWk1:=StrWk;
      StrWk:='';
    end;
    PList.Add(StrWk1);
  end;

  try
    //�������� ��� ��� ������
    try
      IDFtp1.DisConnect;
      IdFtp1.Host:=CommonSet.HostIp;
      IdFtp1.Username:=CommonSet.HostUser;
      IdFtp1.Password:=CommonSet.HostPassw;
      IdFTP1.Connect;
      IDFtp1.List( Dest );
    except
      Result:=False;
      bExit:=True;
    end;

    if bExit then goto ExitProc;

    //�������� ������ ������
    if FileExists(CurDir+'FileList.txt') then
    begin
      try
        Assignfile(f,CurDir+'FileList.txt');
        reset(f);
        while not EOF(f) do
        begin
          ReadLn(f,StrWk);
          iPos:=Pos('XXXXXX',StrWk);
          if iPos>0 then //����������� ���� ���� cn200750.btr
          begin
            StrWk1:=Copy(StrWk,1,iPos-1);

            StrWk2:=FormatDateTime('yyyy',Date);
            StrWk1:=StrWk1+StrWk2;
            StrWk2:=FormatDateTime('mm',Date);
            StrWk3:='01';
            if StrWk2='01' then StrWk3:='10';
            if StrWk2='02' then StrWk3:='20';
            if StrWk2='03' then StrWk3:='30';
            if StrWk2='04' then StrWk3:='40';
            if StrWk2='05' then StrWk3:='50';
            if StrWk2='06' then StrWk3:='60';
            if StrWk2='07' then StrWk3:='70';
            if StrWk2='08' then StrWk3:='80';
            if StrWk2='09' then StrWk3:='90';
            if StrWk2='10' then StrWk3:='a0';
            if StrWk2='11' then StrWk3:='b0';
            if StrWk2='12' then StrWk3:='c0';
            StrWk1:=StrWk1+StrWk3;
            StrWk:=StrWk1+'.btr';
          end;

          if StrWk>'' then
            if FileExists(StrWk) then
            begin
              FList.Add(StrWk); // ���������� � ������ �������� ����� ��� �������� � ������
              While Pos('\',StrWk)>0 do Delete(StrWk,1,Pos('\',StrWk));
{              if pos('.',StrWk)>0 then
              begin
                iPos:=Length(StrWk);
                Delete(StrWk,pos('.',StrWk),iPos-pos('.',StrWk)+1); //������ ����������
              end;                                                                     }
              StrWk:=StrWk+TrExt;
              DList.Add(StrWk); //���������� �������� ���  �����
            end;
        end;
      finally
        CloseFile(F);
      end;
    end;

//    FList.Add(NameF); // ���������� � ������ �������� ����� ��� ��������

{    FindRes:=FindFirst(CommonSet.PathExport+'*.*',32 ,SR);  //32 faArhive
    While FindRes=0 do // ���� �� ������� ����� (��������), �� ��������� ����
    begin
      FList.Add(SR.Name); // ���������� � ������ �������� ���������� ��������
      FindRes:=FindNext(SR); // ����������� ������ �� �������� ��������
    end;
    FindClose(SR); // ��������� �����
}
    //������ ������ ������������ -
    //1. �������� - 100 ��
    //2. �������� ����� �� ������ ������ - �������� �������� �� 10 �.
    //3. ����������
    //4. ��������
    //5. ��� ������ ���������� � �����
    //6. ���� ���� ������  - ����� ������� ���������� ����� ���� �� �����.

    //1.
    delay(100);
    For i:=0 to FList.Count-1 do
    begin
      WrMess(' ���� - '+FList.Strings[i]);
      {$I-}
      Assignfile(f,FList.Strings[i]);
      FileMode := 2;  {Set file access to read/Write }
      Reset(F);
      CloseFile(F);
      {$I+}
      if IOResult = 0 then
      begin
        WrMess(' ������ - ��.');
        try
          fs := TECLFileStream.Create(CurDir+DList.Strings[i],fmCreate,odInac,levelValue[9]);
          fs.LoadFromFile(FList.Strings[i]);
          fs.Free;
          WrMess(' ������ - ��.');

          For k:=0 to PList.Count-1 do
          begin
            StrWk:=PList.Strings[k];
            m:=0;
            BadPath:=False;
            While StrWk>'' do
            begin
              if Pos('\',strwk)>0 then
              begin
                StrWk1:=Copy(StrWk,1,Pos('\',strwk)-1);
                Delete(StrWk,1,Pos('\',strwk));
              end else
              begin
                StrWk1:=StrWk;
                StrWk:='';
              end;
              if strwk1>'' then
              begin
                WrMess('    ���� ��� �������� - '+StrWk1);
                try
                  IdFTP1.ChangeDir(StrWk1);
                  inc(m);
                  WrMess('    ���� ��.'+StrWk1);
                except
                  Result:=False;
                  WrMess('    ������ ��� �������� - '+StrWk1);
                  BadPath:=True;
                end;
              end;
            end;

            if not BadPath then //���� ���������� ����.
            begin
              try
                WrMess('      �������� ����� - '+DList.Strings[i]);
                IDFtp1.Put(DList.Strings[i], DList.Strings[i], False );
//                WrMess('      �������� Ok.');
              except
                Result:=False;
                WrMess('      ������ ��� �������� ����� - '+FList.Strings[i]+TrExt);
              end;
            end;

            While m>0 do
            begin
              IdFTP1.ChangeDir('..');//�� ������� ���������� - ��  ������� ���������.
              m:=m-1;
            end;
          end;

          //������� ������
          Assignfile(f,CurDir+DList.Strings[i]);
          erase(f);

{     //���������� ������

           if TrGood then //���������� ���� � �����
            MoveFile(CurDir+FList.Strings[i], CommonSet.PathArh+FList.Strings[i]);
}
        except
          Result:=False;
          WrMess(' ������ ��� ������.');
        end;
      end
      else
      begin
        Result:=False;
        WrMess(' � ������� ��������.');
      end;
      delay(100);
    end;
    ExitProc:
  finally
    IDFtp1.DisConnect;
    FList.Free;
    DList.Free;
    Dest.Free;
    PList.Free;
    WrMess('  �������� �������� ���������.');
  end;
end;



Function TfmImportDK.prTr(NameF:String):Boolean;
var
  FList,Dest,PList : TStrings;
  i,k,m:INteger;
//  F:TextFile;
  bExit:Boolean;
  StrWk,StrWk1:String;
  BadPath:Boolean;

  Label ExitProc;
begin
  Result:=True;
  WrMess('  �������� �������� ������ �� FTP.');
  bExit:=False;

  //�������� ������ ���� ����� ��� ��������
  FList := TStringList.Create;

  //�������� ����������� �� ���
  Dest := TStringList.Create;

  //�������� ����� �������� �� ��� �� ���
  pList := TStringList.Create;
  StrWk:=CommonSet.FtpExport;
  While StrWk>'' do
  begin
    if Pos(r,strwk)>0 then
    begin
      StrWk1:=Copy(StrWk,1,Pos(r,strwk)-1);
      Delete(StrWk,1,Pos(r,strwk));
    end else
    begin
      StrWk1:=StrWk;
      StrWk:='';
    end;
    PList.Add(StrWk1);
  end;

  try
    //�������� ��� ��� ������
    try
      IDFtp1.DisConnect;
      IdFtp1.Host:=CommonSet.HostIp;
      IdFtp1.Username:=CommonSet.HostUser;
      IdFtp1.Password:=CommonSet.HostPassw;
      IdFTP1.Connect;
      IDFtp1.List( Dest );
    except
      Result:=False;
      bExit:=True;
    end;

    if bExit then goto ExitProc;

    FList.Add(NameF); // ���������� � ������ �������� ����� ��� ��������

{    FindRes:=FindFirst(CommonSet.PathExport+'*.*',32 ,SR);  //32 faArhive
    While FindRes=0 do // ���� �� ������� ����� (��������), �� ��������� ����
    begin
      FList.Add(SR.Name); // ���������� � ������ �������� ���������� ��������
      FindRes:=FindNext(SR); // ����������� ������ �� �������� ��������
    end;
    FindClose(SR); // ��������� �����
}
    //������ ������ ������������ -
    //1. �������� - 100 ��
    //2. �������� ����� �� ������ ������ - �������� �������� �� 10 �. - ���
    //3. ���������� - ���
    //4. ��������
    //5. ��� ������ ���������� � ����� -���
    //6. ���� ���� ������  - ����� ������� ���������� ����� ���� �� �����.

    //1.
    delay(100);
    For i:=0 to FList.Count-1 do
    begin
      WrMess(' ���� - '+FList.Strings[i]);

      try
//        TrGood:=true; // ������������ ��� ��� ���������� ������

        For k:=0 to PList.Count-1 do
        begin
          StrWk:=PList.Strings[k];
          m:=0;
          BadPath:=False;
          While StrWk>'' do
          begin
            if Pos('\',strwk)>0 then
            begin
              StrWk1:=Copy(StrWk,1,Pos('\',strwk)-1);
              Delete(StrWk,1,Pos('\',strwk));
            end else
            begin
              StrWk1:=StrWk;
              StrWk:='';
            end;
            if strwk1>'' then
            begin
              WrMess('    ���� ��� �������� - '+StrWk1);
              try
                IdFTP1.ChangeDir(StrWk1);
                inc(m);
                WrMess('    ���� ��.'+StrWk1);
              except
                Result:=False;
                WrMess('    ������ ��� �������� - '+StrWk1);
                BadPath:=True;
              end;
            end;
          end;

          if not BadPath then //���� ���������� ����.
          begin
            try
              WrMess('      �������� ����� - '+FList.Strings[i]);
              IDFtp1.Put(CommonSet.PathExport+FList.Strings[i], FList.Strings[i], False );
//              WrMess('      �������� Ok.');
            except
              Result:=False;
//              TrGood:=False;
              WrMess('      ������ ��� �������� ����� - '+FList.Strings[i]+TrExt);
            end;
          end;

          While m>0 do
          begin
            IdFTP1.ChangeDir('..');//�� ������� ���������� - ��  ������� ���������.
            m:=m-1;
          end;
        end;
      except
        Result:=False;
      end;
    end;
    delay(100);
    ExitProc:
  finally
    IDFtp1.DisConnect;
    FList.Free;
    Dest.Free;
    PList.Free;
  end;
end;


Function WrRec(taT:TpFIBDataSet):String;
Var StrWk,StrT,StrF,StrVal:String;
    i:Integer;
begin
  StrWk:='';
  for i:=0 to taT.FieldDefs.Count-1 do
  begin
    StrF:=taT.FieldDefs.Items[i].Name;
    StrT:=taT.FieldDefs.Items[i].FieldClass.ClassName;
    StrVal:=taT.FieldByName(StrF).AsString+';';
    StrWk:=StrWk+StrVal;
  end;
  Result:=StrWk;
end;

Function GetSubStrFromLn():String;
Var StrWk:String;
 begin
  StrWk:=Copy(StrDisRec,1,Pos(';',StrDisRec)-1);
  Delete(StrDisRec,1,Pos(';',StrDisRec));
  Result:=StrWk;
end;

procedure TfmImportDK.prStart;
Var StrWk,NAMECL,BARCODE_D:String;
    F: TextFile;
    countStrok:Integer;
    sPost:String;
    bTransp:Boolean;
    DisRec: TDiscontRecord;
 //   ClRec: TClassifRecord;

 begin
  Memo1.Clear;
  WrMess('������.');

  bTransp:=True;
//������� ��������� NameFileClassif
  StrWk:=CommonSet.PathImport+NameFileClassif;  //CommonSet.PathExport+CommonSet.FileExp;
  WrMess(' ��������  ���� : '+StrWk);
  if not FileExists(StrWk) then
   begin
//    Result:=False;
    WrMess(' ��� ����� : '+StrWk);
    exit;
   end;

  try
   Application.ProcessMessages;
   AssignFile(F, StrWk);
   reset(F);

   WrMess('���� ����� ������...');

   with dmCDisc do
   begin
    while not EOF(F) do
    begin
        ReadLn(F,StrDisRec);
        if StrDisRec>'' then
        begin

         prAddRClassif.ParamByName('IDCL').AsInteger := StrToINtDef(GetSubStrFromLn(),0);
         NAMECL := GetSubStrFromLn();
         prAddRClassif.ParamByName('NAMECL').AsString := NAMECL;
         prAddRClassif.ParamByName('TYPECL').AsInteger := StrToINtDef(GetSubStrFromLn(),0);
         prAddRClassif.ParamByName('IDPARENT').AsInteger := StrToINtDef(GetSubStrFromLn(),0);
         prAddRClassif.ExecProc;
        end;

        WrMess('    ���������: '+NAMECL );


 //        WrMess('    �������� �����: '+IntToStr(countStrok));
         delay(10);

    end; //EOF(F)
   end;  //with dmCDisc do

 //   Flush(f);  { ensures that the text was actually written to file }


   WrMess('    ������������� Ok.');


  finally
    CloseFile(F);
    WrMess('');
    WrMess('  ���� ������.' +StrWk);
  end;


  StrWk:=CommonSet.PathImport+NameFileDisccard;  //CommonSet.PathExport+CommonSet.FileExp;
  WrMess(' ��������  ���� : '+StrWk);
  if not FileExists(StrWk) then
   begin
//    Result:=False;
    WrMess(' ��� ����� : '+StrWk);
    exit;
   end;

   try
    Application.ProcessMessages;
    AssignFile(F, StrWk);
    reset(F);

    WrMess('���� ����� ������...');

    with dmCDisc do
    begin
     while not EOF(F) do
      begin
        ReadLn(F,StrDisRec);
        if StrDisRec>'' then
        begin

         BARCODE_D := GetSubStrFromLn();
         If Length(BARCODE_D) > 5 Then
         begin

          prAddRDISCCARD.ParamByName('BARCODE_D').AsString := BARCODE_D;
          prAddRDISCCARD.ParamByName('NAME_D').AsString := GetSubStrFromLn();
          prAddRDISCCARD.ParamByName('PERCENT_D').AsFLOAT := StrToFloatDef(GetSubStrFromLn(),0);
          prAddRDISCCARD.ParamByName('CLIENTINDEX_D').AsInteger := StrToINtDef(GetSubStrFromLn(),0);
          prAddRDISCCARD.ParamByName('IACTIVE_D').AsInteger := StrToINtDef(GetSubStrFromLn(),0);
          prAddRDISCCARD.ParamByName('PHONE_D').AsString := GetSubStrFromLn();
          prAddRDISCCARD.ParamByName('BERTHDAY_D').AsDate := StrToDateDef(GetSubStrFromLn(),0);
          prAddRDISCCARD.ParamByName('COMMENT_D').AsString := GetSubStrFromLn();
          prAddRDISCCARD.ParamByName('CHILDSMALL_D').AsInteger := StrToINtDef(GetSubStrFromLn(),0);
          prAddRDISCCARD.ParamByName('CHILDBIG_D').AsInteger := StrToINtDef(GetSubStrFromLn(),0);
          prAddRDISCCARD.ParamByName('DATECARD_D').AsDate := StrToDateDef(GetSubStrFromLn(),0);
          prAddRDISCCARD.ParamByName('PHONE_MOBILE_D').AsString := GetSubStrFromLn();
          prAddRDISCCARD.ParamByName('EMAIL_D').AsString := GetSubStrFromLn();
          prAddRDISCCARD.ParamByName('ADRES_D').AsString := GetSubStrFromLn();

          prAddRDISCCARD.ExecProc;


//         DisRec.SUMMA := StrToFloatDef(GetSubStrFromLn(),0);


          WrMess('    ���������: '+ BARCODE_D);


 //        WrMess('    �������� �����: '+IntToStr(countStrok));
          delay(10);

         end;
        end;
      end; //EOF(F)
    end;  //with dmCDisc do

 //   Flush(f);  { ensures that the text was actually written to file }


     WrMess('    ���������� ����� Ok.');


    finally
    CloseFile(F);
    WrMess('');
    WrMess('  ���� ������.');
  end;
{
  if bTransp then prTr(NameF) else
  begin
    fmAttention.ShowModal;
  end;
 }
  WrMess('���������.');
end;

Procedure WrMess(S:String);
Var StrWk:String;
begin
  try
    if fmImportDK.Memo1.Lines.Count > 500 then fmImportDK.Memo1.Clear;
    delay(10);
    if S>'' then StrWk:=FormatDateTime('dd.mm hh:nn:ss:zzz',now)+'  '+S
    else StrWk:=S;
    WriteHistory(StrWk);
    fmImportDK.Memo1.Lines.Add(StrWk);
  except
    try
      WriteHistory(StrWk);
      StrWk:=FormatDateTime('dd.mm hh:nn:ss:zzz',now)+'  ���� ������ �������.';
      WriteHistory(StrWk);
    except
    end;
  end;
end;



procedure TfmImportDK.cxButton2Click(Sender: TObject);
begin
{
  CommonSet.TrStr:='';
  if cxCheckBox1.Checked then CommonSet.TrStr:=CommonSet.TrStr+'1' else CommonSet.TrStr:=CommonSet.TrStr+'0';

  WriteIni;
}
  Close;
end;

procedure TfmImportDK.FormCreate(Sender: TObject);
begin
  CurDir := ExtractFilePath(ParamStr(0));
  ReadIni;
  Memo1.Clear;
  iCountSec:=0;
  Label3.Caption:='';
  NameFileDisccard := 'Disccard.txt';
  NameFileClassif := 'Classif.txt';

{  if (CommonSet.TrDocAutoStart='Yes')or(CommonSet.TrDocAutoStart='YES')
  or (CommonSet.TrDocAutoStart='Y') then Timer1.Enabled:=True;
}
end;

procedure TfmImportDK.Timer1Timer(Sender: TObject);
begin
  //�� ��������� �� 2-� �������
  Timer1.Enabled:=False;

{  if TestExport then
  begin
    prTransp;  //���� �����-�� �����
  end;}
  inc(iCountSec);
  if iCountSec>=CommonSet.PeriodPing then //������ �� �������� - ������ ������ ��� (��� ���������� �������� ������ 20 ���)
  begin
//    prTranspImp;  //���� �����-�� �����
//    prImpToCrystal;
    prStart;
//    prTrFiles;

    iCountSec:=0;

//    if CommonSet.CountStart>0 then dec(CommonSet.CountStart);
//    if CommonSet.CountStart=0 then Close;
  end;
  Timer1.Enabled:=True;
end;

procedure TfmImportDK.cxButton1Click(Sender: TObject);
begin
  prStart;
//  prTrFiles;
end;

procedure TfmImportDK.cxButton3Click(Sender: TObject);
var
  FList,Dest,PList,DList: TStrings;
  i,k,m:INteger;
  F:TextFile;
  bExit:Boolean;
  StrWk,StrWk1:String;
  BadPath:Boolean;

  Label ExitProc;
begin
  Memo1.Clear;
  Memo1.Lines.Add('  �������� �������� ������ �� FTP �������� ��������.');
  bExit:=False;

  //�������� ������ ���� ����� ��� ��������
  FList := TStringList.Create; //� ������ ��� ��������
  DList := TStringList.Create; //��� ����� ��� ����������

  //�������� ����������� �� ���
  Dest := TStringList.Create;

  //�������� ����� �������� �� ��� �� ���
  pList := TStringList.Create;
  StrWk:=CommonSet.FtpExport;
  While StrWk>'' do
  begin
    if Pos(r,strwk)>0 then
    begin
      StrWk1:=Copy(StrWk,1,Pos(r,strwk)-1);
      Delete(StrWk,1,Pos(r,strwk));
    end else
    begin
      StrWk1:=StrWk;
      StrWk:='';
    end;
    PList.Add(StrWk1);
  end;

  try
    //�������� ��� ��� ������
    try
      IDFtp1.DisConnect;
      IdFtp1.Host:=CommonSet.HostIp;
      IdFtp1.Username:=CommonSet.HostUser;
      IdFtp1.Password:=CommonSet.HostPassw;
      IdFTP1.Connect;
      IDFtp1.List( Dest );
    except
      bExit:=True;
    end;

    if bExit then goto ExitProc;

//    StrWk:=CommonSet.PathExport+CommonSet.FileExp;
    StrWk:=CommonSet.PathExport+NameFileDisccard;

    if FileExists(StrWk) then
    begin
      FList.Add(StrWk); // ���������� � ������ �������� ����� ��� �������� � ������
      While Pos('\',StrWk)>0 do Delete(StrWk,1,Pos('\',StrWk));
      if CommonSet.Compress=1 then StrWk:=StrWk+TrExt;
      DList.Add(StrWk); //���������� �������� ���  �����
    end;

    //������ ������ ������������ -
    //1. �������� - 100 ��
    //2. �������� ����� �� ������ ������ - �������� �������� �� 10 �.
    //3. ����������
    //4. ��������
    //5. ��� ������ ���������� � �����
    //6. ���� ���� ������  - ����� ������� ���������� ����� ���� �� �����.

    //1.
    delay(100);
    For i:=0 to FList.Count-1 do
    begin
      Memo1.Lines.Add(' ���� - '+FList.Strings[i]);
      {$I-}
      Assignfile(f,FList.Strings[i]);
      FileMode := 2;  {Set file access to read/Write }
      Reset(F);
      CloseFile(F);
      {$I+}
      if IOResult = 0 then
      begin
        Memo1.Lines.Add(' ������ - ��.');
        try
          if CommonSet.Compress=1 then
          begin
            fs := TECLFileStream.Create(CurDir+DList.Strings[i],fmCreate,odInac,levelValue[9]);
            fs.LoadFromFile(FList.Strings[i]);
            fs.Free;
            Memo1.Lines.Add(' ������ - ��.');
          end;

          For k:=0 to PList.Count-1 do
          begin
            StrWk:=PList.Strings[k];
            m:=0;
            BadPath:=False;
            While StrWk>'' do
            begin
              if Pos('\',strwk)>0 then
              begin
                StrWk1:=Copy(StrWk,1,Pos('\',strwk)-1);
                Delete(StrWk,1,Pos('\',strwk));
              end else
              begin
                StrWk1:=StrWk;
                StrWk:='';
              end;
              if strwk1>'' then
              begin
                Memo1.Lines.Add('    ���� ��� �������� - '+StrWk1);
                try
                  IdFTP1.ChangeDir(StrWk1);
                  inc(m);
                  Memo1.Lines.Add('    ���� ��.'+StrWk1);
                except
                  Memo1.Lines.Add('    ������ ��� �������� - '+StrWk1);
                  BadPath:=True;
                end;
              end;
            end;

            if not BadPath then //���� ���������� ����.
            begin
              try
                Memo1.Lines.Add('      �������� ����� - '+DList.Strings[i]);
                if CommonSet.Compress=1 then
                begin
                  IDFtp1.Put(DList.Strings[i], DList.Strings[i], False );
                end
                else
                begin
                  StrWk:=FList.Strings[i];
                  While Pos('\',StrWk)>0 do Delete(StrWk,1,Pos('\',StrWk));
                  IDFtp1.Put(FList.Strings[i], StrWk, False );
                end;
//                Memo1.Lines.Add('      �������� Ok.');
              except
                Memo1.Lines.Add('      ������ ��� �������� ����� - '+FList.Strings[i]+TrExt);
              end;
            end;

            While m>0 do
            begin
              IdFTP1.ChangeDir('..');//�� ������� ���������� - ��  ������� ���������.
              m:=m-1;
            end;
          end;

          //������� ������
          if CommonSet.Compress=1 then
          begin
            Assignfile(f,CurDir+DList.Strings[i]);
            erase(f);
          end;

{     //���������� ������

           if TrGood then //���������� ���� � �����
            MoveFile(CurDir+FList.Strings[i], CommonSet.PathArh+FList.Strings[i]);
}
        except
//          Memo1.Lines.Add(' ������ ��� ������.');
        end;
      end
      else
      begin
        Memo1.Lines.Add(' � ������� ��������.');
      end;
      delay(100);
    end;
    ExitProc:
  finally
    IDFtp1.DisConnect;
    FList.Free;
    DList.Free;
    Dest.Free;
    PList.Free;
    Memo1.Lines.Add('  �������� �������� ���������.');
  end;
end;


end.
