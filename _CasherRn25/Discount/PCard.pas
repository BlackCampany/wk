unit PCard;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, SpeedBar, ExtCtrls, Placemnt, cxImageComboBox,
  ActnList, XPStyleActnCtrls, ActnMan, Menus;

type
  TfmPCard = class(TForm)
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItemIns: TSpeedItem;
    SpeedItemExit: TSpeedItem;
    SpeedItemEdit: TSpeedItem;
    SpeedItemDel: TSpeedItem;
    SpeedItemAktiv: TSpeedItem;
    SpeedItemNoAktiv: TSpeedItem;
    ViewPCards: TcxGridDBTableView;
    LevelPCards: TcxGridLevel;
    GridPCards: TcxGrid;
    FormPlacement1: TFormPlacement;
    ViewPCardsBARCODE: TcxGridDBColumn;
    ViewPCardsPLATTYPE: TcxGridDBColumn;
    ViewPCardsIACTIVE: TcxGridDBColumn;
    ViewPCardsCLINAME: TcxGridDBColumn;
    ViewPCardsNAME: TcxGridDBColumn;
    ViewPCardsDATEFROM: TcxGridDBColumn;
    ViewPCardsDATETO: TcxGridDBColumn;
    ViewPCardsCOMMENT: TcxGridDBColumn;
    ViewPCardsVIDOPL: TcxGridDBColumn;
    SpeedItem1: TSpeedItem;
    amPC: TActionManager;
    acInputMoney: TAction;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    acBalansPC: TAction;
    N3: TMenuItem;
    acHistoryPC: TAction;
    SpeedItem2: TSpeedItem;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SpeedItemExitClick(Sender: TObject);
    procedure SpeedItemAktivClick(Sender: TObject);
    procedure SpeedItemNoAktivClick(Sender: TObject);
    procedure SpeedItemInsClick(Sender: TObject);
    procedure SpeedItemEditClick(Sender: TObject);
    procedure SpeedItem7Click(Sender: TObject);
    procedure SpeedItemDelClick(Sender: TObject);
    procedure acInputMoneyExecute(Sender: TObject);
    procedure acBalansPCExecute(Sender: TObject);
    procedure ViewPCardsDblClick(Sender: TObject);
    procedure acHistoryPCExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmPCard: TfmPCard;

implementation

uses Un1, AddPlCard, prdb, DmRnDisc, InputMoney, PCHistori;

{$R *.dfm}

procedure TfmPCard.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  GridPCards.Align:=AlClient;
  ViewPCards.RestoreFromIniFile(CurDir+GridIni);
end;

procedure TfmPCard.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewPCards.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmPCard.SpeedItemExitClick(Sender: TObject);
begin
  close;
end;

procedure TfmPCard.SpeedItemAktivClick(Sender: TObject);
begin
  with dmPC do
  begin
    if not taPCard.Eof then
    begin
      taPCard.Edit;
      taPCardIACTIVE.AsInteger:=1;
      taPCard.Post;

      taPCard.Refresh;
    end;
  end;
end;

procedure TfmPCard.SpeedItemNoAktivClick(Sender: TObject);
begin
  with dmPC do
  begin
    if not taPCard.Eof then
    begin
      taPCard.Edit;
      taPCardIACTIVE.AsInteger:=0;
      taPCard.Post;
      taPCard.Refresh;
    end;
  end;
end;

procedure TfmPCard.SpeedItemInsClick(Sender: TObject);
Var StrWk:String;
begin
  //���������� ��
  with dmPC do
  begin
    with fmAddPlCard do
    begin
      if taTypePl.Active=False then taTypePl.Active:=True;
      if taCategSaleSel.Active=False then taCategSaleSel.Active:=True;

      cxTextEdit1.Text:=taPCardCLINAME.AsString;
      cxTextEdit2.Text:=taPCardBARCODE.AsString;
      if taPCardIACTIVE.AsInteger=1 then CheckBox1.Checked:=True
      else CheckBox1.Checked:=False;

      cxLookupComboBox1.EditValue:=taPCardPLATTYPE.AsInteger;

      cxComboBoxTypeOpl.ItemIndex:=taPCardTypeOpl.AsInteger-1;
      cxCalcEdit2.EditValue:=0;
      cxTextEditBALANCECARD.Text := taPCardBARCODE.AsString;
      cxCheckBoxGol.Checked := False;
      cxCalcEdit1.EditValue:= 0;

      cxLookupComboBoxCategSale.EditValue:=taPCardSALET.AsInteger;

      Caption:='���������� ��������� �����.';
    end;
    fmAddPlCard.ShowModal;
    if fmAddPlCard.ModalResult=mrOk then
    begin
      StrWk:=fmAddPlCard.cxTextEdit2.Text;
      if taPCard.Locate('BARCODE',StrWk,[]) then
      begin
        ShowMessage('��������� ����� � ����� ����� ��� ���� !!! ���������� ���������.');
      end else
      begin
        fmPCard.ViewPCards.BeginUpdate;
        try
//          if taPCard.Active=False then taPCard.Active:=True;
          taPCard.Append;
          taPCardBARCODE.AsString:=StrWk;
          taPCardPLATTYPE.AsInteger:=fmAddPlCard.cxLookupComboBox1.EditValue;
          if fmAddPlCard.CheckBox1.Checked=True then taPCardIACTIVE.AsInteger:=1
          else taPCardIACTIVE.AsInteger:=0;
          taPCardCLINAME.asstring:=fmAddPlCard.cxTextEdit1.Text;

          taPCardTypeOpl.AsInteger:=fmAddPlCard.cxComboBoxTypeOpl.ItemIndex+1;
          taPCardBALANCECARD.AsString := fmAddPlCard.cxTextEditBALANCECARD.Text;
          taPCardDAYLIMIT.AsFloat := fmAddPlCard.cxCalcEdit1.EditValue;
          taPCardSALET.AsInteger := fmAddPlCard.cxLookupComboBoxCategSale.EditValue;

          taPCard.Post;

          taPCard.Refresh;
          taPCard.Locate('BARCODE',StrWk,[]);
        finally
          fmPCard.ViewPCards.EndUpdate;
          prRowFocus(ViewPCards);
        end;
      end;
    end;
  end;
end;

procedure TfmPCard.SpeedItemEditClick(Sender: TObject);
Var StrWk:String;
    sBar:String;
    CliName,CliType:String;
    rBalans,rBalansDay,DLimit:Real;
begin
  with dmPC do
  begin
    with fmAddPlCard do
    begin
      if taTypePl.Active=False then taTypePl.Active:=True;
      if taCategSaleSel.Active=False then taCategSaleSel.Active:=True;

      sBar:=taPCardBARCODE.AsString;

      prFindPCardBalans(sBar,rBalans,rBalansDay,DLimit,CliName,CliType);

      cxTextEdit1.Text:=taPCardCLINAME.AsString;
      cxTextEdit2.Text:=taPCardBARCODE.AsString;
      if taPCardIACTIVE.AsInteger=1 then CheckBox1.Checked:=True
      else CheckBox1.Checked:=False;

      cxLookupComboBox1.EditValue:=taPCardPLATTYPE.AsInteger;
      cxComboBoxTypeOpl.ItemIndex:=taPCardTypeOpl.AsInteger-1;
      cxCalcEdit2.EditValue := rBalans;
      cxTextEditBALANCECARD.Text:= taPCardBALANCECARD.AsString;
      if cxTextEditBALANCECARD.Text = '' then
       cxTextEditBALANCECARD.Text := taPCardBARCODE.AsString;

      cxCheckBoxGol.Checked := False;
      if cxTextEdit2.Text <> cxTextEditBALANCECARD.Text then
       cxCheckBoxGol.Checked := True;

      cxLookupComboBoxCategSale.EditValue:=taPCardSALET.AsInteger;

      cxCalcEdit1.EditValue  := taPCardDAYLIMIT.AsFloat;

      Caption:='�������������� ��������� �����.';
    end;
    fmAddPlCard.ShowModal;
    if fmAddPlCard.ModalResult=mrOk then
    begin
      StrWk:=fmAddPlCard.cxTextEdit2.Text;
      if StrWk<>sBar then
      begin
        if taPCard.Locate('BARCODE',StrWk,[]) then
        begin
          ShowMessage('��������� ����� � ����� ����� ��� ���� !!! ���������� ���������.');
          exit;
        end;
      end;

      if taPCard.Locate('BARCODE',sBar,[]) then
      begin
        taPCard.Edit;
        taPCardBARCODE.AsString:=StrWk;
        taPCardPLATTYPE.AsInteger:=fmAddPlCard.cxLookupComboBox1.EditValue;

        if fmAddPlCard.CheckBox1.Checked=True then taPCardIACTIVE.AsInteger:=1
        else taPCardIACTIVE.AsInteger:=0;
        taPCardCLINAME.asstring:=fmAddPlCard.cxTextEdit1.Text;
        taPCardTypeOpl.AsInteger:=fmAddPlCard.cxComboBoxTypeOpl.ItemIndex+1;
        taPCardBALANCECARD.AsString := fmAddPlCard.cxTextEditBALANCECARD.Text;
        taPCardDAYLIMIT.AsFloat := fmAddPlCard.cxCalcEdit1.EditValue;
        taPCardSALET.AsInteger := fmAddPlCard.cxLookupComboBoxCategSale.EditValue;

        taPCard.Post;

        taPCard.Refresh;
      end;
    end;
  end;

end;

procedure TfmPCard.SpeedItem7Click(Sender: TObject);
begin
  //����� �� ����, ������



end;

procedure TfmPCard.SpeedItemDelClick(Sender: TObject);
Var sBar:String;
begin
  //������� ��������� �����
  with dmPC do
  begin
    if taPCard.RecordCount>0 then
    begin
      if ViewPCards.Controller.SelectedRowCount=1 then
      begin
        if MessageDlg('�� ������������� ������ ������� ��������� ����� '+taPCardBARCODE.AsString+'('+taPCardCLINAME.AsString+') ?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          sBar:=taPCardBARCODE.AsString;
          if prTestMovePC(sBar) then
          begin
            showmessage('�������� ���������� - �� �������� ���� ��������.');
          end else
          begin

            taPCard.Delete;


          end;
        end;
      end;
    end;
  end;
end;

procedure TfmPCard.acInputMoneyExecute(Sender: TObject);
Var sBar:String;
begin
  //�������� ����� �� ��
  if not CanDo('prEditBalancePC') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  with dmPC do
  begin
    if taPCard.RecordCount>0 then
    begin
      if fmPCard.ViewPCards.Controller.SelectedRowCount=1 then
      begin
        if taPCardTYPEOPL.AsInteger=2 then
        begin
          if taPCardIACTIVE.AsInteger=0 then
          begin
            ShowMessage('�������� ����� �� �� �������� �������� ���������.');
          end else
          begin
            sBar:=taPCardBARCODE.AsString;
            fmInputMoney.cxCalcEdit1.Value:=0;
            fmInputMoney.cxDateEdit1.Date:=Date;
            fmInputMoney.Label1.Caption:=taPCardCLINAME.AsString;
            fmInputMoney.ShowModal;
            if fmInputMoney.ModalResult=mrOk then
            begin
//EXECUTE PROCEDURE PRSAVEPC1 (?PCBAR, ?PCSUM, ?STATION, ?IDPERSON, ?PERSNAME)
              prInpMoney.ParamByName('PCBAR').AsString:=sBar;
              prInpMoney.ParamByName('PCSUM').AsFloat:=fmInputMoney.cxCalcEdit1.Value;
              prInpMoney.ParamByName('STATION').AsINteger:=CommonSet.Station;
              prInpMoney.ParamByName('IDPERSON').AsINteger:=Person.Id;
              prInpMoney.ParamByName('PERSNAME').AsString:=Person.Name;
              prInpMoney.ParamByName('DATEOP').AsDateTime:=fmInputMoney.cxDateEdit1.Date;

              prInpMoney.ExecProc;
            end;
          end;
        end else
        begin
          showmessage('�� ������ ��� ������ �������� �� �����������������.');
        end;
      end;
    end;
  end;
end;

procedure TfmPCard.acBalansPCExecute(Sender: TObject);
Var sBar:String;
    CliName,CliType:String;
    rBalans,rBalansDay,DLimit:Real;
begin
  if not CanDo('prViewBalancePC') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  with dmPC do
  begin
    if taPCard.RecordCount>0 then
    begin
      if ViewPCards.Controller.SelectedRowCount=1 then
      begin
        if taPCardTYPEOPL.AsInteger>=2 then
        begin
          sBar:=taPCardBARCODE.AsString;
          prFindPCardBalans(sBar,rBalans,rBalansDay,DLimit,CliName,CliType);

          Showmessage('     ��������  '+CliName+#$0D+'     ���  '+CliType+#$0D+'     ������  '+fts(rv(rBalans))+#$0D+'     ������ �������  '+fts(rv(rBalansDay))+#$0D+'     ������� �����  '+fts(rv((-1)*DLimit)));
        end;
      end;
    end;
  end;
end;

procedure TfmPCard.ViewPCardsDblClick(Sender: TObject);
begin
  with dmPC do
  begin
    if taPCard.RecordCount>0 then
    begin
      if ViewPCards.Controller.SelectedRowCount=1 then
      begin
        if taPCardTYPEOPL.AsInteger>=2 then
        begin
          if CanDo('prViewBalancePC') then acBalansPC.Execute
          else SpeedItemEdit.Click;
        end;
      end;
    end;
  end;
end;

procedure TfmPCard.acHistoryPCExecute(Sender: TObject);
var  CliName,CliType:String;
     rBalans,rBalansDay,DLimit:Real;
begin
  with dmPC do
  begin
    if taPCard.RecordCount>0 then
    begin
      if ViewPCards.Controller.SelectedRowCount=1 then
      begin
        fmPCHist.Caption:='������� �� ��������� ����� '+taPCardBARCODE.AsString+' �� ������ � '+FormatDateTime('dd.mm.yyyy hh:nn',TrebSel.DateFrom)+' �� '+FormatDateTime('dd.mm.yyyy hh:nn',TrebSel.DateTo);
        prFindPCardBalans(taPCardBARCODE.AsString,rBalans,rBalansDay,DLimit,CliName,CliType);
        fmPCHist.Label1.Caption:='��������  '+CliName+'     ���  '+CliType+'     ������  '+fts(rv(rBalans))+'     ������ �������  '+fts(rv(rBalansDay))+'     ������� �����  '+fts(rv((-1)*DLimit));
        fmPCHist.StatusBar1.Panels[1].Text:=taPCardBARCODE.AsString;
        fmPCHist.Label2.Caption:=taPCardBARCODE.AsString;
        with dmPC do
        begin
          fmPCHist.StatusBar1.Panels[1].Text:=taPCardBARCODE.AsString;
          fmPCHist.Label2.Caption:=taPCardBARCODE.AsString;
          fmPCHist.ViewPCHist.BeginUpdate;
          try
            quPCHist.Active:=False;
            quPCHist.ParamByName('DateB').AsDateTime:=TrebSel.DateFrom;
            quPCHist.ParamByName('DateE').AsDateTime:=Trunc(TrebSel.DateTo+1);
            quPCHist.ParamByName('BARCODE').AsString:=taPCardBARCODE.AsString;
            quPCHist.Active:=True;
          finally
            fmPCHist.ViewPCHist.EndUpdate;
          end;
        end;
        fmPCHist.Show;
      end;
    end;
  end;
end;

end.
