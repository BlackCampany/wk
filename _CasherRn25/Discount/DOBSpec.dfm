object fmDOBSpec: TfmDOBSpec
  Left = 166
  Top = 132
  Width = 781
  Height = 598
  Caption = #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1103' '#1088#1072#1089#1093#1086#1076#1072' '#1073#1083#1102#1076
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 545
    Width = 773
    Height = 19
    Panels = <
      item
        Width = 300
      end
      item
        Width = 50
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 504
    Width = 773
    Height = 41
    Align = alBottom
    BevelInner = bvLowered
    Color = 12582911
    TabOrder = 1
    object cxButton1: TcxButton
      Left = 240
      Top = 8
      Width = 113
      Height = 25
      Caption = #1042#1099#1093#1086#1076
      TabOrder = 0
      OnClick = cxButton1Click
      Glyph.Data = {
        5E040000424D5E04000000000000360000002800000012000000130000000100
        18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
        8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
        0000CED3D6848684848684848684848684848684848684848684848684848684
        848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
        7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
        FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
        FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
        007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
        FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
        00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
        494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
        00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000}
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 56
      Top = 8
      Width = 129
      Height = 25
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1088#1072#1089#1095#1077#1090
      TabOrder = 1
      OnClick = cxButton2Click
      Glyph.Data = {
        EE030000424DEE03000000000000360000002800000012000000110000000100
        180000000000B8030000C40E0000C40E00000000000000000000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6A5A6A57371
        737371735A595A5A595A4A494A4A494A393C39313031313031313031A5A6A5CE
        D3D6CED3D6CED3D60000CED3D6CED3D6A5A6A5633400CECFCEE7E7E7C6C3C6C6
        C3C6CECFCEF7F3F7EFEFEFE7E7E7A5A6A5313031313031A5A6A5CED3D6CED3D6
        0000CED3D6633400633400633400DEDBDEE7E7E7E7E7E7E7E7E7E7E7E7E7E7E7
        E7E7E7E7E7E7DEDBDECE8E42633400313031313031CED3D60000CED3D6633400
        CE8E42633400DEA67BDEA67BDEA67BDEA67BDEA67BCE8E42CE8E42CE8E42CE8E
        42CE8E426334007B5900313031CED3D60000CED3D6633400DEA67B633400DEA6
        7BDEA67BDEA67BDEA67BDEA67BDEA67BCE8E42CE8E42CE8E42CE8E42633400CE
        8E42313031CED3D60000CED3D6633400DEA67B633400DEA67BDEA67BDEA67BDE
        A67BDEA67BDEA67BDEA67BCE8E42CE8E42CE8E42633400CE8E42393C39CED3D6
        0000CED3D6633400DEA67B633400AD3C29633400633400633400633400633400
        633400633400633400CE8E42633400CE8E424A494ACED3D60000CED3D6633400
        DEA67B6334009C9A9CADFFFF9CFBFF9CFBFF9CFBFF9CFBFF9CFBFF9CFBFF9CFB
        FF633400633400CE8E424A494ACED3D60000CED3D6633400DEA67B633400ADFF
        FFCECFCEA5A6A5A5A6A5A5A6A5A5A6A5A5A6A5A5A6A5C6C3C69CFBFF633400CE
        8E424A494ACED3D60000CED3D6633400DEA67B7B5900ADFFFFADFFFFADFFFFAD
        FFFFADFFFFADFFFFADFFFFADFFFFADFFFF9CFBFF7B5900CE8E424A494ACED3D6
        0000CED3D6633400DEA67B7B5900ADFFFFCECFCEA5A6A5A5A6A5A5A6A5A5A6A5
        A5A6A5A5A6A5C6C3C69CFBFF7B5900CE8E424A494ACED3D60000CED3D6633400
        DEA67B9C9A9CADFFFFADFFFFADFFFFADFFFFADFFFFADFFFFADFFFFADFFFFADFF
        FF9CFBFF9C9A9CCE8E425A595ACED3D60000CED3D6633400DEA67BA5A6A5CED3
        D6CECFCECE8E42CE8E42A5A6A5A5A6A5A5A6A5A5A6A5C6C3C69CFBFFA5A6A5DE
        A67B636563CED3D60000CED3D6A5A6A56334007B5900CED3D6CED3D6ADFFFFAD
        FFFFADFFFFADFFFFADFFFFADFFFFADFFFFADFFFF7B5900633400A5A6A5CED3D6
        0000CED3D6CED3D6CED3D6A5A6A5633400633400633400633400633400633400
        6334006334006334006334009C9A9CCED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000}
      LookAndFeel.Kind = lfOffice11
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 81
    Width = 185
    Height = 423
    Align = alLeft
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 2
    object Label1: TLabel
      Left = 8
      Top = 144
      Width = 134
      Height = 13
      Cursor = crHandPoint
      Caption = '1. '#1055#1088#1086#1074#1077#1088#1082#1072' '#1089#1086#1086#1090#1074#1077#1090#1089#1090#1074#1080#1103
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
      Transparent = True
      OnClick = Label1Click
    end
    object Label2: TLabel
      Left = 8
      Top = 168
      Width = 155
      Height = 13
      Cursor = crHandPoint
      Caption = '2. '#1056#1072#1089#1095#1077#1090' '#1082#1086#1083'-'#1074#1072' '#1076#1083#1103' '#1089#1087#1080#1089#1072#1085#1080#1103
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
      Transparent = True
      OnClick = Label2Click
    end
    object Label4: TLabel
      Left = 8
      Top = 240
      Width = 132
      Height = 13
      Cursor = crHandPoint
      Caption = '4. '#1047#1072#1087#1086#1083#1085#1080#1090#1100' '#1090#1077#1082'. '#1086#1089#1090#1072#1090#1082#1080
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
      Transparent = True
      OnClick = Label4Click
    end
    object Label5: TLabel
      Left = 8
      Top = 264
      Width = 123
      Height = 13
      Cursor = crHandPoint
      Caption = '7. '#1055#1088#1086#1074#1077#1088#1082#1072' '#1087#1086' '#1087#1072#1088#1090#1080#1103#1084
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
      Transparent = True
      OnClick = Label5Click
    end
    object Label6: TLabel
      Left = 8
      Top = 192
      Width = 157
      Height = 13
      Cursor = crHandPoint
      Caption = '3. '#1056#1072#1089#1095#1077#1090' '#1089#1077#1073#1077#1089#1090#1086#1080#1084#1086#1089#1090#1080' '#1073#1083#1102#1076
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
      Transparent = True
      OnClick = Label6Click
    end
    object Label11: TLabel
      Left = 32
      Top = 24
      Width = 97
      Height = 13
      Cursor = crHandPoint
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1087#1086#1079#1080#1094#1080#1102
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
      Transparent = True
      OnClick = Label11Click
    end
    object Label12: TLabel
      Left = 32
      Top = 48
      Width = 90
      Height = 13
      Cursor = crHandPoint
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1087#1086#1079#1080#1094#1080#1102
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
      Transparent = True
    end
    object Image1: TImage
      Left = 12
      Top = 25
      Width = 17
      Height = 15
      Center = True
      Picture.Data = {
        07544269746D617076010000424D760100000000000036000000280000000A00
        00000A000000010018000000000040010000C40E0000C40E0000000000000000
        0000FFFFFFFFFFFFFFFFFFFF6600FF6600FF6600FF6600FFFFFFFFFFFFFFFFFF
        0000FFFFFFFFFFFFFFFFFFFF6600FF6600FF6600FF6600FFFFFFFFFFFFFFFFFF
        0000FFFFFFFFFFFFFFFFFFFF6600FF6600FF6600FF6600FFFFFFFFFFFFFFFFFF
        0000FF6600FF6600FF6600FF6600FF6600FF6600FF6600FF6600FF6600FF6600
        0000FF6600FF6600FF6600FF6600FF6600FF6600FF6600FF6600FF6600FF6600
        0000FF9966FF9966FF9966FF9966FF9966FF9966FF9966FF9966FF9966FF9966
        0000CCCC99CCCC99CCCC99CCCC99CCCC99CCCC99CCCC99CCCC99CCCC99CCCC99
        0000FFFFFFFFFFFFFFFFFFFF9966CCCC99CCCC99FF9966FFFFFFFFFFFFFFFFFF
        0000FFFFFFFFFFFFFFFFFFFF9966CCCC99CCCC99FF9966FFFFFFFFFFFFFFFFFF
        0000FFFFFFFFFFFFFFFFFFFF9966FF9966FF9966FF9966FFFFFFFFFFFFFFFFFF
        0000}
      Proportional = True
      Transparent = True
    end
    object Image2: TImage
      Left = 12
      Top = 49
      Width = 17
      Height = 15
      Center = True
      Picture.Data = {
        07544269746D617076010000424D760100000000000036000000280000000D00
        0000080000000100180000000000400100000000000000000000000000000000
        0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFF633AF3633AF3633AF3633A
        F3633AF3633AF3633AF3633AF3633AF3633AF3633AF3FFFFFF00FFFFFF633AF3
        A777E8A777E8A777E8A777E8A777E8A777E8A777E8A777E8A777E8633AF3FFFF
        FF00FFFFFF633AF3C3A7FFC3A7FFC3A7FFC3A7FFC3A7FFC3A7FFC3A7FFC3A7FF
        C3A7FFA777E8FFFFFF00FFFFFFA777E8A777E8A777E8A777E8A777E8A777E8A7
        77E8A777E8A777E8A777E8A777E8FFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FF00}
      Proportional = True
      Transparent = True
    end
    object Label15: TLabel
      Left = 88
      Top = 216
      Width = 72
      Height = 13
      Cursor = crHandPoint
      Caption = #1055#1077#1095#1072#1090#1100' '#1086#1090#1095#1077#1090#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
      Transparent = True
      OnClick = Label15Click
    end
  end
  object Panel3: TPanel
    Left = 185
    Top = 81
    Width = 588
    Height = 423
    Align = alClient
    BevelInner = bvLowered
    Caption = 'Panel3'
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 349
      Width = 584
      Height = 72
      Align = alBottom
      BevelInner = bvLowered
      Color = clWhite
      TabOrder = 0
      object Memo1: TcxMemo
        Left = 88
        Top = 2
        Lines.Strings = (
          'Memo1')
        ParentFont = False
        Properties.OEMConvert = True
        Properties.ReadOnly = True
        Properties.ScrollBars = ssVertical
        Properties.WordWrap = False
        Style.Font.Charset = ANSI_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'MS Sans Serif'
        Style.Font.Pitch = fpFixed
        Style.Font.Style = []
        Style.LookAndFeel.Kind = lfOffice11
        Style.IsFontAssigned = True
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 0
        OnDblClick = Memo1DblClick
        Height = 68
        Width = 494
      end
    end
    object cxSplitter1: TcxSplitter
      Left = 2
      Top = 346
      Width = 584
      Height = 3
      Cursor = crVSplit
      AlignSplitter = salBottom
      Control = Panel4
      Color = 16744448
      ParentColor = False
    end
    object PageControl1: TPageControl
      Left = 2
      Top = 2
      Width = 584
      Height = 344
      ActivePage = TabSheet1
      Align = alClient
      Style = tsFlatButtons
      TabOrder = 2
      object TabSheet1: TTabSheet
        Caption = #1041#1083#1102#1076#1072
        object GridB: TcxGrid
          Left = 0
          Top = 0
          Width = 576
          Height = 313
          Align = alClient
          TabOrder = 0
          LookAndFeel.Kind = lfOffice11
          object ViewB: TcxGridDBTableView
            NavigatorButtons.ConfirmDelete = False
            DataController.DataSource = dmO.dsDobSpec
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <
              item
                Format = '0.00'
                Kind = skSum
                FieldName = 'RSUM'
                Column = ViewBRSUM
              end
              item
                Format = '0.00'
                Kind = skSum
                FieldName = 'DSUM'
                Column = ViewBDSUM
              end>
            DataController.Summary.SummaryGroups = <>
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsData.Inserting = False
            OptionsView.Footer = True
            OptionsView.GroupByBox = False
            OptionsView.Indicator = True
            object ViewBSIFR: TcxGridDBColumn
              Caption = #1050#1086#1076' '#1073#1083#1102#1076#1072
              DataBinding.FieldName = 'SIFR'
              Options.Editing = False
            end
            object ViewBNAMEB: TcxGridDBColumn
              Caption = #1053#1072#1079#1074#1072#1085#1080#1077' '#1073#1083#1102#1076#1072
              DataBinding.FieldName = 'NAMEB'
              Options.Editing = False
              Width = 200
            end
            object ViewBCODEB: TcxGridDBColumn
              Caption = #1057#1074#1103#1079#1091#1102#1097#1080#1081' '#1082#1086#1076
              DataBinding.FieldName = 'CODEB'
              PropertiesClassName = 'TcxButtonEditProperties'
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.ReadOnly = False
              Properties.OnButtonClick = ViewBCODEBPropertiesButtonClick
            end
            object ViewBKB: TcxGridDBColumn
              Caption = #1050' '#1087#1077#1088#1077#1076#1072#1095#1080
              DataBinding.FieldName = 'KB'
              PropertiesClassName = 'TcxCalcEditProperties'
              Properties.AssignedValues.DisplayFormat = True
              Properties.ImmediatePost = True
            end
            object ViewBQUANT: TcxGridDBColumn
              Caption = #1050#1086#1083'-'#1074#1086
              DataBinding.FieldName = 'QUANT'
            end
            object ViewBPRICER: TcxGridDBColumn
              Caption = #1062#1077#1085#1072
              DataBinding.FieldName = 'PRICER'
            end
            object ViewBDSUM: TcxGridDBColumn
              Caption = #1057#1091#1084#1084#1072' '#1089#1082#1080#1076#1082#1080
              DataBinding.FieldName = 'DSUM'
              Width = 69
            end
            object ViewBRSUM: TcxGridDBColumn
              Caption = #1057#1091#1084#1084#1072' '#1088#1077#1072#1083#1080#1079#1072#1094#1080#1080
              DataBinding.FieldName = 'RSUM'
              Styles.Content = dmO.cxStyle25
            end
            object ViewBIDCARD: TcxGridDBColumn
              Caption = #1040#1088#1090#1080#1082#1091#1083
              DataBinding.FieldName = 'IDCARD'
              PropertiesClassName = 'TcxButtonEditProperties'
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.OnButtonClick = ViewBCODEBPropertiesButtonClick
            end
            object ViewBNAME: TcxGridDBColumn
              Caption = #1053#1072#1079#1074#1072#1085#1080#1077
              DataBinding.FieldName = 'NAME'
              PropertiesClassName = 'TcxButtonEditProperties'
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.OnButtonClick = ViewBCODEBPropertiesButtonClick
              Width = 200
            end
            object ViewBNAMESHORT: TcxGridDBColumn
              Caption = #1045#1076'.'#1080#1079#1084'.'
              DataBinding.FieldName = 'NAMESHORT'
              Options.Editing = False
              Width = 100
            end
            object ViewBTCARD: TcxGridDBColumn
              Caption = #1058#1080#1087
              DataBinding.FieldName = 'TCARD'
              PropertiesClassName = 'TcxImageComboBoxProperties'
              Properties.Images = dmO.imState
              Properties.Items = <
                item
                  Value = 0
                end
                item
                  ImageIndex = 11
                  Value = 1
                end>
              Options.Editing = False
            end
            object ViewBKM: TcxGridDBColumn
              DataBinding.FieldName = 'KM'
              Visible = False
              Options.Editing = False
            end
          end
          object LevelB: TcxGridLevel
            GridView = ViewB
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = #1058#1086#1074#1072#1088#1099
        ImageIndex = 1
        object GridC: TcxGrid
          Left = 0
          Top = 0
          Width = 576
          Height = 313
          Align = alClient
          TabOrder = 0
          LookAndFeel.Kind = lfOffice11
          object ViewC: TcxGridDBTableView
            PopupMenu = PopupMenu1
            NavigatorButtons.ConfirmDelete = False
            DataController.DataSource = dmORep.dsCalc
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <
              item
                Format = '0.00'
                Kind = skSum
                FieldName = 'SumIn'
                Column = ViewCSumIn
              end>
            DataController.Summary.SummaryGroups = <>
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsView.Footer = True
            OptionsView.GroupByBox = False
            OptionsView.Indicator = True
            object ViewCArticul: TcxGridDBColumn
              Caption = #1040#1088#1090#1080#1082#1091#1083
              DataBinding.FieldName = 'Articul'
            end
            object ViewCName: TcxGridDBColumn
              Caption = #1053#1072#1079#1074#1072#1085#1080#1077
              DataBinding.FieldName = 'Name'
            end
            object ViewCsM: TcxGridDBColumn
              Caption = #1045#1076'.'#1080#1079#1084'.'
              DataBinding.FieldName = 'sM'
            end
            object ViewCQuant: TcxGridDBColumn
              Caption = #1050#1086#1083'-'#1074#1086' '#1088#1072#1089#1095'.'
              DataBinding.FieldName = 'Quant'
            end
            object ViewCQuantFact: TcxGridDBColumn
              Caption = #1054#1089#1090#1072#1090#1086#1082
              DataBinding.FieldName = 'QuantFact'
            end
            object ViewCQuantDiff: TcxGridDBColumn
              Caption = #1042#1089#1077#1075#1086
              DataBinding.FieldName = 'QuantDiff'
            end
            object ViewCSumIn: TcxGridDBColumn
              Caption = #1057#1091#1084#1084#1072' '#1087#1088#1080#1093'.'
              DataBinding.FieldName = 'SumIn'
            end
          end
          object LevelC: TcxGridLevel
            GridView = ViewC
          end
        end
      end
      object TabSheet3: TTabSheet
        Caption = #1050#1072#1083#1100#1082#1091#1083#1103#1094#1080#1103
        ImageIndex = 2
        object GridBC: TcxGrid
          Left = 0
          Top = 0
          Width = 576
          Height = 313
          Align = alClient
          TabOrder = 0
          LookAndFeel.Kind = lfOffice11
          object ViewBC: TcxGridDBTableView
            OnDblClick = acCalcBBExecute
            NavigatorButtons.ConfirmDelete = False
            OnCustomDrawCell = ViewBCCustomDrawCell
            DataController.DataSource = dmORep.dsCalcB
            DataController.Summary.DefaultGroupSummaryItems = <
              item
                Format = '0.00'
                Kind = skSum
                Position = spFooter
                FieldName = 'SUMIN'
                Column = ViewBCSUMIN
              end
              item
                Format = '0.00'
                Kind = skSum
                Position = spFooter
                FieldName = 'PRICEIN'
                Column = ViewBCPRICEIN
              end
              item
                Format = '0.00'
                Kind = skAverage
                Position = spFooter
                FieldName = 'PRICEOUT'
                Column = ViewBCPRICEOUT
              end
              item
                Format = '0.00'
                Kind = skAverage
                Position = spFooter
                FieldName = 'SUMOUT'
                Column = ViewBCSUMOUT
              end
              item
                Format = '0.000'
                Kind = skSum
                Position = spFooter
                FieldName = 'QUANTC'
                Column = ViewBCQUANTC
              end>
            DataController.Summary.FooterSummaryItems = <
              item
                Format = '0.00'
                Kind = skSum
                FieldName = 'SUMIN'
                Column = ViewBCSUMIN
              end
              item
                Format = '0.00'
                Kind = skSum
                FieldName = 'PRICEIN'
                Column = ViewBCPRICEIN
              end>
            DataController.Summary.SummaryGroups = <>
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsView.Footer = True
            OptionsView.GroupFooters = gfAlwaysVisible
            OptionsView.Indicator = True
            object ViewBCID: TcxGridDBColumn
              Caption = #1050#1086#1076' '#1087#1086#1079#1080#1094#1080#1080
              DataBinding.FieldName = 'ID'
            end
            object ViewBCCODEB: TcxGridDBColumn
              Caption = #1050#1086#1076' '#1073#1083#1102#1076#1072
              DataBinding.FieldName = 'CODEB'
            end
            object ViewBCNAMEB: TcxGridDBColumn
              Caption = #1053#1072#1079#1074#1072#1085#1080#1077' '#1073#1083#1102#1076#1072
              DataBinding.FieldName = 'NAMEB'
              Width = 200
            end
            object ViewBCQUANT: TcxGridDBColumn
              Caption = #1050#1086#1083'-'#1074#1086' '#1073#1083#1102#1076#1072
              DataBinding.FieldName = 'QUANT'
            end
            object ViewBCPRICEOUT: TcxGridDBColumn
              Caption = #1062#1077#1085#1072' '#1087#1088#1086#1076#1072#1078#1080
              DataBinding.FieldName = 'PRICEOUT'
            end
            object ViewBCSUMOUT: TcxGridDBColumn
              Caption = #1057#1091#1084#1084#1072' '#1087#1088#1086#1076#1072#1078#1080
              DataBinding.FieldName = 'SUMOUT'
            end
            object ViewBCIDCARD: TcxGridDBColumn
              Caption = #1050#1086#1076' '#1090#1086#1074#1072#1088#1072
              DataBinding.FieldName = 'IDCARD'
            end
            object ViewBCNAMEC: TcxGridDBColumn
              Caption = #1053#1072#1079#1074#1072#1085#1080#1077' '#1090#1086#1074#1072#1088#1072
              DataBinding.FieldName = 'NAMEC'
              Width = 200
            end
            object ViewBCSB: TcxGridDBColumn
              Caption = #1058#1080#1087
              DataBinding.FieldName = 'SB'
            end
            object ViewBCQUANTC: TcxGridDBColumn
              Caption = #1050#1086#1083'-'#1074#1086
              DataBinding.FieldName = 'QUANTC'
            end
            object ViewBCPRICEIN: TcxGridDBColumn
              Caption = #1062#1077#1085#1072' '#1079#1072#1082#1091#1087#1072
              DataBinding.FieldName = 'PRICEIN'
            end
            object ViewBCSUMIN: TcxGridDBColumn
              Caption = #1057#1091#1084#1084#1072' '#1079#1072#1082#1091#1087#1072
              DataBinding.FieldName = 'SUMIN'
            end
            object ViewBCIM: TcxGridDBColumn
              Caption = #1050#1086#1076' '#1077#1076'.'#1080#1079#1084'.'
              DataBinding.FieldName = 'IM'
            end
            object ViewBCSM: TcxGridDBColumn
              Caption = #1045#1076'.'#1080#1079#1084'.'
              DataBinding.FieldName = 'SM'
              Width = 50
            end
          end
          object LevelBC: TcxGridLevel
            GridView = ViewBC
          end
        end
      end
    end
  end
  object Panel5: TPanel
    Left = 0
    Top = 0
    Width = 773
    Height = 81
    Align = alTop
    BevelInner = bvLowered
    Color = 12582911
    TabOrder = 4
    object Label7: TLabel
      Left = 16
      Top = 16
      Width = 34
      Height = 13
      Caption = #1053#1086#1084#1077#1088
      Transparent = True
    end
    object Label8: TLabel
      Left = 192
      Top = 16
      Width = 26
      Height = 13
      Caption = #1044#1072#1090#1072
      Transparent = True
    end
    object Label9: TLabel
      Left = 16
      Top = 48
      Width = 82
      Height = 13
      Caption = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103
      Transparent = True
    end
    object Label10: TLabel
      Left = 368
      Top = 16
      Width = 50
      Height = 13
      Caption = #1054#1087#1077#1088#1072#1094#1080#1103
      Transparent = True
    end
    object cxTextEdit1: TcxTextEdit
      Left = 64
      Top = 12
      Style.BorderStyle = ebsOffice11
      Style.Shadow = True
      TabOrder = 0
      Text = 'cxTextEdit1'
      Width = 97
    end
    object cxDateEdit1: TcxDateEdit
      Left = 224
      Top = 12
      EditValue = 39083d
      Style.BorderStyle = ebsOffice11
      Style.Shadow = True
      TabOrder = 1
      Width = 105
    end
    object cxLookupComboBox1: TcxLookupComboBox
      Left = 112
      Top = 44
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Caption = #1053#1072#1079#1074#1072#1085#1080#1077
          FieldName = 'NAMEMH'
        end>
      Properties.ListOptions.AnsiSort = True
      Properties.ListSource = dmO.dsMHAll
      Style.BorderStyle = ebsOffice11
      Style.LookAndFeel.Kind = lfOffice11
      Style.Shadow = True
      Style.PopupBorderStyle = epbsDefault
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 2
      Width = 217
    end
    object cxTextEdit2: TcxTextEdit
      Left = 432
      Top = 12
      Style.BorderStyle = ebsOffice11
      Style.Shadow = True
      TabOrder = 3
      Text = 'cxTextEdit2'
      Width = 113
    end
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 3000
    OnTimer = Timer1Timer
    Left = 336
    Top = 128
  end
  object FormPlacement1: TFormPlacement
    Active = False
    Left = 448
    Top = 128
  end
  object amDOB: TActionManager
    Left = 377
    Top = 200
    StyleName = 'XP Style'
    object acCalcB: TAction
      Caption = 'acCalcB'
      OnExecute = acCalcBExecute
    end
    object acCalcBB: TAction
      Caption = 'acCalcBB'
      OnExecute = acCalcBBExecute
    end
    object acExpExcel1: TAction
      Caption = #1069#1082#1089#1087#1086#1088#1090' '#1074' Excel'
      OnExecute = acExpExcel1Execute
    end
    object acTest1: TAction
      Caption = 'acTest1'
      ShortCut = 16433
      OnExecute = acTest1Execute
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 503
    Top = 190
    object Excel1: TMenuItem
      Action = acExpExcel1
      Bitmap.Data = {
        56080000424D560800000000000036000000280000001A0000001A0000000100
        18000000000020080000C40E0000C40E00000000000000000000C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0004000004000004000004000C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C00000C0C0C0004000
        80808080808080808080808080808080808080808000400040E02040E020FFFF
        FFC8D0D4A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0808080C0
        C0C0C0C0C0C0C0C00000C0C0C0C0C0C000400040E02000C02000C02000C02000
        C02000400040E02040E020FFFFFF004000C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4A4A0A0A4A0A0404040C0C0C0C0C0C00000C0C0C0C0C0C0
        C0C0C000400000C02000C02000C02000400000C02040E020FFFFFF00400000C0
        20C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4A4A0A0A4A0A040
        2020808080C0C0C00000C0C0C0C0C0C0C0C0C0A4A0A000400000C02000400000
        C02040E020FFFFFF004000004000004000C8D0D4A4A0A0A4A0A0A4A0A0A4A0A0
        A4A0A0A4A0A0808080C8D0D4808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C000400040E02040E020FFFFFF004000F0FBFFF0FBFFF0FB
        FFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFA4A0A0C8D0D4C8D0D440
        2020808080C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C000400040E02040E020FF
        FFFF004000808080004000C8D0D4C0DCC0C0DCC0C0DCC0C0DCC0C0DCC0C0DCC0
        C0DCC0C8D0D4F0FBFFA4A0A0C8D0D4402020808080C0C0C00000C0C0C0C0C0C0
        C0C0C000400000C02040E020FFFFFF00400080E0A000C020808080004000FFFF
        FFFFFFFFFFFFFFF0FBFFC0DCC0C0DCC0F0FBFFC0DCC0F0FBFF808080C8D0D440
        2020808080C0C0C00000C0C0C0C0C0C000400000C02040E020FFFFFF004000FF
        FFFF00400000C02000C020808080004000FFFFFFFFFFFFC8D0D4F0FBFFC0DCC0
        C0DCC0C0DCC0F0FBFFC0DCC0A4A0A0404020808080C0C0C00000C0C0C0004000
        004000004000004000FFFFFFFFFFFFFFFFFFFFFFFF0040000040000040000040
        00C8D0D4F0FBFFC0DCC0C8D0D4C8D0D4F0FBFFC0DCC0F0FBFFFFFFFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFC0DCC0FFFFFFF0FBFFFFFFFFFFFFFFF0FBFFF0FBFFF0FBFF
        FFFFFFC0DCC0F0FBFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0F0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FB
        FFF0FBFFFFFFFFFFFFFFF0FBFFF0FBFFFFFFFFC0DCC0F0FBFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0F0
        FBFFF0FBFFF0FBFFC0DCC0C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4F0FBFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0C0DCC0C0DCC0F0FBFFF0FBFFC0DCC0FFFF
        FFC0DCC0F0FBFFC0DCC0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0C0
        DCC0F0FBFFF0FBFFF0FBFFF0FBFFFFFFFFF0FBFFF0FBFFC0DCC0F0FBFFFFFFFF
        FFFFFFFFFFFFFFFFFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0C0DCC0C8D0D4F0FBFFF0FBFFC8D0D4FFFF
        FFC8D0D4F0FBFFC0DCC0F0FBFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0F0
        FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFC8D0D4F0FBFFFFFFFF
        FFFFFFFFFFFFFFFFFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFF0FBFFC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4C8D0D4C8D0D4C8D0D4F0FBFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFF0FBFFF0
        FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080C0DCC0C8D0D4C8D0D4F0FBFFC8D0D4C0DCC0C0DCC0C8D0D4F0FB
        FFC8D0D4C0DCC0F0FBFFC8D0D4F0FBFFC8D0D4C8D0D4F0FBFFA4A0A080808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C080808080E0E040E0E040E0E080
        E0E040E0E080E0E080E0E040E0E080E0E040E0E040E0E080E0E040E0E080E0E0
        40E0E040E0E080E0E000E0E0408080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C080808080E0E080E0E0C0DCC080E0E0C0DCC080E0E080E0E0C0DCC080E0
        E0C0DCC080E0E080E0E0C0DCC080E0E0C0DCC080E0E080E0E0C0DCC000808000
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080F0FBFFF0FBFFF0FBFF80
        E0E0F0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFF
        F0FBFFF0FBFFF0FBFFF0FBFF00C0C0002040808080C0C0C00000C0C0C0C0C0C0
        C0C0C08080808080808080808080808080808080808080808080808080808080
        8080808080808080808080808080808080808080808080808080808080808000
        2040C0C0C0C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C00000}
    end
  end
end
