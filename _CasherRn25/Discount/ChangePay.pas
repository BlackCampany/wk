unit ChangePay;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons, ExtCtrls,
  RXCtrls, cxControls, cxContainer, cxEdit, cxTextEdit, cxStyles,
  cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, DB, cxDBData,
  cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxClasses, cxGridCustomView, cxGrid;

type
  TFmChagePay = class(TForm)
    Panel2: TPanel;
    Panel1: TPanel;
    cxButtonOk: TcxButton;
    cxButtonCancel: TcxButton;
    RxLabel1: TRxLabel;
    cxID_TAB: TcxTextEdit;
    cxOPERTYPE: TcxTextEdit;
    RxLabel2: TRxLabel;
    RxLabel3: TRxLabel;
    GridBN: TcxGrid;
    ViewBN: TcxGridDBTableView;
    ViewBNNAME: TcxGridDBColumn;
    LevelBN: TcxGridLevel;
    procedure cxButtonCancelClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmChagePay: TFmChagePay;

implementation

{$R *.dfm}

procedure TFmChagePay.cxButtonCancelClick(Sender: TObject);
begin
 close;
end;

end.
