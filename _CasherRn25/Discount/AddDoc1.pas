unit AddDoc1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, cxGraphics, cxCurrencyEdit, cxDropDownEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxButtonEdit,
  cxMaskEdit, cxCalendar, cxControls, cxContainer, cxEdit, cxTextEdit,
  StdCtrls, ExtCtrls, Menus, cxLookAndFeelPainters, cxButtons, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, DB, cxDBData, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxLabel, dxfLabel, Placemnt, FIBDataSet,
  pFIBDataSet, DBClient, FIBQuery, pFIBQuery, pFIBStoredProc,
  XPStyleActnCtrls, ActnList, ActnMan;

type
  TfmAddDoc1 = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Label1: TLabel;
    cxTextEdit1: TcxTextEdit;
    cxDateEdit1: TcxDateEdit;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    cxTextEdit2: TcxTextEdit;
    cxDateEdit2: TcxDateEdit;
    cxButtonEdit1: TcxButtonEdit;
    cxLookupComboBox1: TcxLookupComboBox;
    cxCurrencyEdit1: TcxCurrencyEdit;
    cxCurrencyEdit2: TcxCurrencyEdit;
    Label13: TLabel;
    Label14: TLabel;
    Panel2: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Panel3: TPanel;
    ViewDoc1: TcxGridDBTableView;
    LevelDoc1: TcxGridLevel;
    GridDoc1: TcxGrid;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    cxLabel3: TcxLabel;
    Label15: TLabel;
    FormPlacement1: TFormPlacement;
    taSpec: TClientDataSet;
    dsSpec: TDataSource;
    taSpecNum: TIntegerField;
    taSpecIdGoods: TIntegerField;
    taSpecNameG: TStringField;
    taSpecIM: TIntegerField;
    taSpecSM: TStringField;
    taSpecQuant: TFloatField;
    taSpecPrice1: TCurrencyField;
    taSpecSum1: TCurrencyField;
    taSpecPrice2: TCurrencyField;
    taSpecSum2: TCurrencyField;
    taSpecINds: TIntegerField;
    taSpecSNds: TStringField;
    taSpecRNds: TCurrencyField;
    taSpecSumNac: TCurrencyField;
    taSpecProcNac: TFloatField;
    prCalcPrice: TpFIBStoredProc;
    cxLabel4: TcxLabel;
    ViewDoc1Num: TcxGridDBColumn;
    ViewDoc1IdGoods: TcxGridDBColumn;
    ViewDoc1NameG: TcxGridDBColumn;
    ViewDoc1IM: TcxGridDBColumn;
    ViewDoc1SM: TcxGridDBColumn;
    ViewDoc1Quant: TcxGridDBColumn;
    ViewDoc1Price1: TcxGridDBColumn;
    ViewDoc1Sum1: TcxGridDBColumn;
    ViewDoc1Price2: TcxGridDBColumn;
    ViewDoc1Sum2: TcxGridDBColumn;
    ViewDoc1INds: TcxGridDBColumn;
    ViewDoc1SNds: TcxGridDBColumn;
    ViewDoc1RNds: TcxGridDBColumn;
    ViewDoc1SumNac: TcxGridDBColumn;
    ViewDoc1ProcNac: TcxGridDBColumn;
    cxLabel5: TcxLabel;
    cxLabel6: TcxLabel;
    taSpecKm: TFloatField;
    amDocIn: TActionManager;
    acAddPos: TAction;
    acDelPos: TAction;
    acAddList: TAction;
    acDelAll: TAction;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure cxLookupComboBox1PropertiesChange(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxLabel1Click(Sender: TObject);
    procedure ViewDoc1DragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure ViewDoc1DragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure taSpecQuantChange(Sender: TField);
    procedure taSpecPrice1Change(Sender: TField);
    procedure taSpecSum1Change(Sender: TField);
    procedure taSpecPrice2Change(Sender: TField);
    procedure taSpecSum2Change(Sender: TField);
    procedure cxLabel3Click(Sender: TObject);
    procedure cxLabel2Click(Sender: TObject);
    procedure cxLabel4Click(Sender: TObject);
    procedure ViewDoc1Editing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cxLabel5Click(Sender: TObject);
    procedure ViewDoc1EditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure ViewDoc1EditKeyPress(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
    procedure acAddPosExecute(Sender: TObject);
    procedure acDelPosExecute(Sender: TObject);
    procedure acAddListExecute(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure acDelAllExecute(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure cxLabel6Click(Sender: TObject);
    procedure cxDateEdit1PropertiesChange(Sender: TObject);
    procedure ViewDoc1SMPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAddDoc1: TfmAddDoc1;
  iCol:Integer = 0;
  bAdd:Boolean = False;

implementation

uses Un1, dmOffice, Clients, Goods, FCards, DocsIn, CurMessure, OMessure;

{$R *.dfm}

procedure TfmAddDoc1.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  GridDoc1.Align:=alClient;
  ViewDoc1.RestoreFromIniFile(CurDir+GridIni);
end;

procedure TfmAddDoc1.cxLookupComboBox1PropertiesChange(Sender: TObject);
begin
  with dmO do
  begin
    CurVal.IdMH:=cxLookupComboBox1.EditValue;
    CurVal.NAMEMH:=cxLookupComboBox1.Text;
    if quMHAll.Locate('ID',CurVal.IdMH,[]) then
    begin
      Label15.Caption:='��. ����: '+quMHAllNAMEPRICE.AsString;
      Label15.Tag:=quMHAllDEFPRICE.AsInteger;
    end else
    begin
      Label15.Caption:='';
      Label15.Tag:=0;
    end;  
  end;
end;

procedure TfmAddDoc1.cxButtonEdit1PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  if dmO.taClients.Active=False then dmO.taClients.Active:=True;
  if fmClients.Visible then fmClients.Close;
  fmClients.ShowModal;
  if fmClients.ModalResult=mrOk then
  begin
    cxButtonEdit1.Tag:=dmO.taClientsID.AsInteger;
    cxButtonEdit1.Text:=dmO.taClientsNAMECL.AsString;

  end;
end;

procedure TfmAddDoc1.cxLabel1Click(Sender: TObject);
begin
  acAddList.Execute;
end;

procedure TfmAddDoc1.ViewDoc1DragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept:=False;
  if bDrIn then  Accept:=True;
end;

procedure TfmAddDoc1.ViewDoc1DragDrop(Sender, Source: TObject; X,
  Y: Integer);
Var iCo:Integer;
    i,j: Integer;
    iNum: Integer;
    Rec:TcxCustomGridRecord;
    iMax:Integer;
begin
//
  if bDrIn then
  begin
    ResetAddVars;
    iCo:=fmGoods.ViewGoods.Controller.SelectedRecordCount;
    if iCo>0 then
    begin
      if MessageDlg('�� ������������� ������ ����������� ��������� ������� ('+IntToStr(iCo)+' ��.) � ������������ ���������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        iMax:=1;
        fmAddDoc1.taSpec.First;
        if not fmAddDoc1.taSpec.Eof then
        begin
          fmAddDoc1.taSpec.Last;
          iMax:=fmAddDoc1.taSpecNum.AsInteger+1;
        end;

        with dmO do
        begin
          ViewDoc1.BeginUpdate;
          for i:=0 to fmGoods.ViewGoods.Controller.SelectedRecordCount-1 do
          begin
            Rec:=fmGoods.ViewGoods.Controller.SelectedRecords[i];

            for j:=0 to Rec.ValueCount-1 do
            begin
              if fmGoods.ViewGoods.Columns[j].Name='ViewGoodsID' then break;
            end;

            iNum:=Rec.Values[j];
          //��� ��� - ����������

            if quCardsSel.Locate('ID',iNum,[]) then
            begin
              with fmAddDoc1 do
              begin
                taSpec.Append;
                taSpecNum.AsInteger:=iMax;
                taSpecIdGoods.AsInteger:=quCardsSelID.AsInteger;
                taSpecNameG.AsString:=quCardsSelNAME.AsString;
                taSpecIM.AsInteger:=quCardsSelIMESSURE.AsInteger;
                taSpecSM.AsString:=quCardsSelNAMESHORT.AsString;
                taSpecQuant.AsFloat:=1;
                taSpecPrice1.AsCurrency:=0;
                taSpecSum1.AsCurrency:=0;
                taSpecPrice2.AsCurrency:=0;
                taSpecSum2.AsCurrency:=0;
                taSpecINds.AsInteger:=quCardsSelINDS.AsInteger;
                taSpecSNds.AsString:=quCardsSelNAMENDS.AsString;
                taSpecRNds.AsCurrency:=0;
                taSpecSumNac.AsCurrency:=0;
                taSpecProcNac.AsFloat:=0;
                taSpec.Post;
                inc(iMax);
              end;
            end;
          end;
          ViewDoc1.EndUpdate;
        end;
      end;
    end;
  end;
end;

procedure TfmAddDoc1.taSpecQuantChange(Sender: TField);
begin
  //���������� �����
  if iCol=1 then
  begin
    taSpecSum1.AsCurrency:=taSpecPrice1.AsCurrency*taSpecQuant.AsFloat;
    taSpecSum2.AsCurrency:=taSpecPrice2.AsCurrency*taSpecQuant.AsFloat;
    taSpecSumNac.AsCurrency:=taSpecSum2.AsCurrency-taSpecSum1.AsCurrency;
    if taSpecSum1.AsCurrency>0.01 then
    begin
      taSpecProcNac.AsFloat:=(taSpecSum2.AsCurrency-taSpecSum1.AsCurrency)/taSpecSum1.AsCurrency*100;
    end;
    taSpecRNds.AsCurrency:=RoundEx(taSpecSum1.AsCurrency*vNds[taSpecINds.AsInteger]/(100+vNds[taSpecINds.AsInteger])*100)/100;
  end;
end;

procedure TfmAddDoc1.taSpecPrice1Change(Sender: TField);
begin
  //���������� ����
  if iCol=2 then
  begin
    taSpecSum1.AsCurrency:=taSpecPrice1.AsCurrency*taSpecQuant.AsFloat;
//  taSpecSum2.AsCurrency:=taSpecPrice2.AsCurrency*taSpecQuant.AsFloat;
    taSpecSumNac.AsCurrency:=taSpecSum2.AsCurrency-taSpecSum1.AsCurrency;
    if taSpecSum1.AsCurrency>0.01 then
    begin
      taSpecProcNac.AsFloat:=(taSpecSum2.AsCurrency-taSpecSum1.AsCurrency)/taSpecSum1.AsCurrency*100;
    end;
    taSpecRNds.AsCurrency:=RoundEx(taSpecSum1.AsCurrency*vNds[taSpecINds.AsInteger]/(100+vNds[taSpecINds.AsInteger])*100)/100;
  end;
end;

procedure TfmAddDoc1.taSpecSum1Change(Sender: TField);
begin
  if iCol=3 then
  begin
    if abs(taSpecQuant.AsFloat)>0 then taSpecPrice1.AsCurrency:=RoundEx(taSpecSum1.AsCurrency/taSpecQuant.AsFloat*100)/100;
    taSpecSumNac.AsCurrency:=taSpecSum2.AsCurrency-taSpecSum1.AsCurrency;
    if taSpecSum1.AsCurrency>0.01 then
    begin
      taSpecProcNac.AsFloat:=(taSpecSum2.AsCurrency-taSpecSum1.AsCurrency)/taSpecSum1.AsCurrency*100;
    end;
    taSpecRNds.AsCurrency:=RoundEx(taSpecSum1.AsCurrency*vNds[taSpecINds.AsInteger]/(100+vNds[taSpecINds.AsInteger])*100)/100;
  end;
end;

procedure TfmAddDoc1.taSpecPrice2Change(Sender: TField);
begin
  //���������� ����
  if iCol=4 then
  begin
//  taSpecSum1.AsCurrency:=taSpecPrice1.AsCurrency*taSpecQuant.AsFloat;
    taSpecSum2.AsCurrency:=taSpecPrice2.AsCurrency*taSpecQuant.AsFloat;
    taSpecSumNac.AsCurrency:=taSpecSum2.AsCurrency-taSpecSum1.AsCurrency;
    if taSpecSum1.AsCurrency>0.01 then
    begin
      taSpecProcNac.AsFloat:=(taSpecSum2.AsCurrency-taSpecSum1.AsCurrency)/taSpecSum1.AsCurrency*100;
    end;  
  end;
end;

procedure TfmAddDoc1.taSpecSum2Change(Sender: TField);
begin
  if iCol=5 then
  begin
    if abs(taSpecQuant.AsFloat)>0 then taSpecPrice2.AsCurrency:=RoundEx(taSpecSum2.AsCurrency/taSpecQuant.AsFloat*100)/100;
    taSpecSumNac.AsCurrency:=taSpecSum2.AsCurrency-taSpecSum1.AsCurrency;
    if taSpecSum1.AsCurrency>0.01 then
    begin
      taSpecProcNac.AsFloat:=(taSpecSum2.AsCurrency-taSpecSum1.AsCurrency)/taSpecSum1.AsCurrency*100;
    end;
  end;
end;

procedure TfmAddDoc1.cxLabel3Click(Sender: TObject);
Var rPrice:Currency;
begin
  //����������� ����
  taSpec.First;
  while not taSpec.Eof do
  begin
//?INPRICE, ?IDGOOD, ?PRICE0, ?PRICE1
    prCalcPrice.ParamByName('INPRICE').AsInteger:=Label15.Tag;
    prCalcPrice.ParamByName('IDGOOD').AsInteger:=taSpecIdGoods.AsInteger;
    prCalcPrice.ParamByName('PRICE0').AsCurrency:=taSpecPrice1.AsCurrency;
    prCalcPrice.ParamByName('PRICE1').AsCurrency:=0;
    prCalcPrice.ExecProc;

    rPrice:=prCalcPrice.ParamByName('PRICEOUT').AsCurrency;

    taSpec.Edit;
    taSpecPrice2.AsCurrency:=rPrice;
    taSpecSum2.AsCurrency:=rPrice*taSpecQuant.AsFloat;
    taSpecSumNac.AsCurrency:=rPrice*taSpecQuant.AsFloat-taSpecSum1.AsCurrency;
    if taSpecSum1.AsCurrency>0.01 then
    begin
      taSpecProcNac.AsFloat:=(rPrice*taSpecQuant.AsFloat-taSpecSum1.AsCurrency)/taSpecSum1.AsCurrency*100;
    end;
    taSpec.Post;

    taSpec.Next;
    delay(10);
  end;
  taSpec.First;
end;

procedure TfmAddDoc1.cxLabel2Click(Sender: TObject);
begin
  acDelPos.Execute;
end;

procedure TfmAddDoc1.cxLabel4Click(Sender: TObject);
begin
  //�������� ����
  ViewDoc1.BeginUpdate;
  taSpec.First;
  while not taSpec.Eof do
  begin
    taSpec.Edit;
    taSpecPrice2.AsCurrency:=taSpecPrice1.AsCurrency;
    taSpecSum2.AsCurrency:=taSpecSum1.AsCurrency;
    taSpecSumNac.AsCurrency:=0;
    taSpecProcNac.AsFloat:=0;
    taSpec.Post;

    taSpec.Next;
    delay(10);
  end;
  taSpec.First;
  ViewDoc1.EndUpdate;
end;

procedure TfmAddDoc1.ViewDoc1Editing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  iCol:=0;
  if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1Quant' then iCol:=1;
  if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1Price1' then iCol:=2;
  if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1Sum1' then iCol:=3;
  if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1Price2' then iCol:=4;
  if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1Sum2' then iCol:=5;


end;

procedure TfmAddDoc1.FormShow(Sender: TObject);
begin
  iCol:=0;
  if cxTextEdit1.Text='' then
  begin
    cxTextEdit1.SetFocus;
    cxTextEdit1.SelectAll;
  end else GridDoc1.SetFocus;

  ViewDoc1NameG.Options.Editing:=False;
end;

procedure TfmAddDoc1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewDoc1.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmAddDoc1.cxLabel5Click(Sender: TObject);
begin
  acAddPos.Execute;
end;

procedure TfmAddDoc1.ViewDoc1EditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
Var iCode:Integer;
    Km:Real;
    sName:String;
begin
  with dmO do
  begin
    if (Key=$0D) then
    begin
      if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1IdGoods' then
      begin
        iCode:=VarAsType(AEdit.EditingValue, varInteger);

        quFCard.Active:=False;
        quFCard.SelectSQL.Clear;
        quFCard.SelectSQL.Add('SELECT ID,NAME,IMESSURE,PARENT,TCARD,INDS');
        quFCard.SelectSQL.Add('FROM OF_CARDS');
        quFCard.SelectSQL.Add('where ID='+IntToStr(iCode));
        quFCard.Active:=True;

        if quFCard.RecordCount=1 then
        begin
          ViewDoc1.BeginUpdate;
          taSpec.Edit;
          taSpecIdGoods.AsInteger:=iCode;
          taSpecNameG.AsString:=quFCardNAME.AsString;
          taSpecIM.AsInteger:=quFCardIMESSURE.AsInteger;
          taSpecSM.AsString:=prFindKNM(quFCardIMESSURE.AsInteger,Km);
          taSpecKm.AsFloat:=Km;
          taSpecQuant.AsFloat:=0;
          taSpecPrice1.AsFloat:=0;
          taSpecSum1.AsFloat:=0;
          taSpecPrice2.AsFloat:=0;
          taSpecSum2.AsFloat:=0;
          taSpecINds.AsInteger:=0;
          taSpecSNds.AsString:='';
          if taNDS.Active=False then taNDS.Active:=True;
          if taNds.Locate('ID',quFCardINDS.AsInteger,[]) then
          begin
            taSpecINds.AsInteger:=quFCardINDS.AsInteger;
            taSpecSNds.AsString:=taNDSNAMENDS.AsString;
          end;

          taSpecRNds.AsFloat:=0;
          taSpecSumNac.AsFloat:=0;
          taSpecProcNac.AsFloat:=0;
          taSpec.Post;

          ViewDoc1.EndUpdate;
        end;
      end;
      if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1NameG' then
      begin
        sName:=VarAsType(AEdit.EditingValue, varString);

        quFCard.Active:=False;
        quFCard.SelectSQL.Clear;
        quFCard.SelectSQL.Add('SELECT ID,NAME,IMESSURE,PARENT,TCARD,INDS');
        quFCard.SelectSQL.Add('FROM OF_CARDS');
        quFCard.SelectSQL.Add('where UPPER(NAME) like ''%'+AnsiUpperCase(sName)+'%''');
        quFCard.SelectSQL.Add('Order by NAME');

        quFCard.Active:=True;

        bAdd:=True;
        //�������� ����� ������ � ����� ������
        fmFCards.ShowModal;
        if fmFCards.ModalResult=mrOk then
        begin
          if quFCard.RecordCount>0 then
          begin
            ViewDoc1.BeginUpdate;
            taSpec.Edit;
            taSpecIdGoods.AsInteger:=quFCardID.AsInteger;
            taSpecNameG.AsString:=quFCardNAME.AsString;
            taSpecIM.AsInteger:=quFCardIMESSURE.AsInteger;
            taSpecSM.AsString:=prFindKNM(quFCardIMESSURE.AsInteger,Km);
            taSpecKm.AsFloat:=Km;
            taSpecQuant.AsFloat:=0;
            taSpecPrice1.AsFloat:=0;
            taSpecSum1.AsFloat:=0;
            taSpecPrice2.AsFloat:=0;
            taSpecSum2.AsFloat:=0;
            taSpecINds.AsInteger:=0;
            taSpecSNds.AsString:='';
            taSpecRNds.AsFloat:=0;
            if taNDS.Active=False then taNDS.Active:=True;
            if taNds.Locate('ID',quFCardINDS.AsInteger,[]) then
            begin
              taSpecINds.AsInteger:=quFCardINDS.AsInteger;
              taSpecSNds.AsString:=taNDSNAMENDS.AsString;
            end;
            taSpecSumNac.AsFloat:=0;
            taSpecProcNac.AsFloat:=0;
            taSpec.Post;
            ViewDoc1.EndUpdate;
            AEdit.SelectAll;
          end;
        end;
        bAdd:=False;
      end;
    end else
      if ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1IdGoods' then
        if fTestKey(Key)=False then
          if taSpec.State in [dsEdit,dsInsert] then taSpec.Cancel;
  end;
end;

procedure TfmAddDoc1.ViewDoc1EditKeyPress(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
Var Km:Real;
    sName:String;
begin
  if bAdd then exit;
  with dmO do
  begin
    if (ViewDoc1.Controller.FocusedColumn.Name='ViewDoc1NameG') then
    begin
      sName:=VarAsType(AEdit.EditingValue ,varString)+Key;
//      Label2.Caption:=sName;
      if sName>'' then
      begin
        quFCard.Active:=False;
        quFCard.SelectSQL.Clear;
        quFCard.SelectSQL.Add('SELECT ID,NAME,IMESSURE,PARENT,TCARD,INDS');
        quFCard.SelectSQL.Add('FROM OF_CARDS');
        quFCard.SelectSQL.Add('where UPPER(NAME) like ''%'+AnsiUpperCase(sName)+'%''');
        quFCard.SelectSQL.Add('Order by NAME');
        quFCard.Active:=True;

        if quFCard.RecordCount=1 then
        begin
          ViewDoc1.BeginUpdate;
          taSpec.Edit;
          taSpecIdGoods.AsInteger:=quFCardID.AsInteger;
          taSpecNameG.AsString:=quFCardNAME.AsString;
          taSpecIM.AsInteger:=quFCardIMESSURE.AsInteger;
          taSpecSM.AsString:=prFindKNM(quFCardIMESSURE.AsInteger,Km);
          taSpecKm.AsFloat:=Km;
          taSpecQuant.AsFloat:=0;
          taSpecPrice1.AsFloat:=0;
          taSpecSum1.AsFloat:=0;
          taSpecPrice2.AsFloat:=0;
          taSpecSum2.AsFloat:=0;
          taSpecINds.AsInteger:=0;
          taSpecSNds.AsString:='';
          if taNDS.Active=False then taNDS.Active:=True;
          if taNds.Locate('ID',quFCardINDS.AsInteger,[]) then
          begin
            taSpecINds.AsInteger:=quFCardINDS.AsInteger;
            taSpecSNds.AsString:=taNDSNAMENDS.AsString;
          end;
          taSpecRNds.AsFloat:=0;
          taSpecSumNac.AsFloat:=0;
          taSpecProcNac.AsFloat:=0;
          taSpec.Post;
          ViewDoc1.EndUpdate;
          AEdit.SelectAll;
          ViewDoc1NameG.Options.Editing:=False;
          ViewDoc1NameG.Focused:=True;
          Key:=#0;
        end;
      end;
    end;
  end;//}
end;

procedure TfmAddDoc1.acAddPosExecute(Sender: TObject);
Var iMax:Integer;
begin
  //�������� �������
  iMax:=1;
  ViewDoc1.BeginUpdate;

  taSpec.First;
  if not taSpec.Eof then
  begin
    taSpec.Last;
    iMax:=taSpecNum.AsInteger+1;
  end;

  taSpec.Append;
  taSpecNum.AsInteger:=iMax;
  taSpecIdGoods.AsInteger:=0;
  taSpecNameG.AsString:='';
  taSpecIM.AsInteger:=0;
  taSpecSM.AsString:='';
  taSpecKm.AsFloat:=0;
  taSpecQuant.AsFloat:=0;
  taSpecPrice1.AsFloat:=0;
  taSpecSum1.AsFloat:=0;
  taSpecPrice2.AsFloat:=0;
  taSpecSum2.AsFloat:=0;
  taSpecINds.AsInteger:=0;
  taSpecSNds.AsString:='';
  taSpecRNds.AsFloat:=0;
  taSpecSumNac.AsFloat:=0;
  taSpecProcNac.AsFloat:=0;
  taSpec.Post;
  ViewDoc1.EndUpdate;
  GridDoc1.SetFocus;

  ViewDoc1NameG.Options.Editing:=True;
  ViewDoc1NameG.Focused:=True;

end;

procedure TfmAddDoc1.acDelPosExecute(Sender: TObject);
begin
  //������� �������
  if taSpec.RecordCount>0 then
  begin
    taSpec.Delete;
  end;
{  if MessageDlg('�� ������������� ������ ������� �������  � '+taSpecNum.AsString+'  '+taSpecNameG.AsString+' ?',
     mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
    taSpec.Delete;
  end;}
end;

procedure TfmAddDoc1.acAddListExecute(Sender: TObject);
begin
  bAddSpecIn:=True;
  if fmGoods.Visible then fmGoods.Close;
  Delay(100);
  fmGoods.Show;
end;

procedure TfmAddDoc1.cxButton1Click(Sender: TObject);
Var IdH:Integer;
    rSum1,rSum2:Real;
begin
  //��������
  with dmO do
  begin
    IdH:=cxTextEdit1.Tag;
    if taSpec.State in [dsEdit,dsInsert] then taSpec.Post;
    if cxDateEdit1.Date<3000 then  cxDateEdit1.Date:=Date;
    if cxDateEdit2.Date<3000 then  cxDateEdit2.Date:=Date;

    //������� ������ ������
    ViewDoc1.BeginUpdate;

    if cxTextEdit1.Tag=0 then IDH:=GetId('DocIn');

    quDocsInId.Active:=False;
    quDocsInId.ParamByName('IDH').AsInteger:=IDH;
    quDocsInId.Active:=True;

    quDocsInId.First;
    if quDocsInId.RecordCount=0 then quDocsInId.Append else quDocsInId.Edit;

    quDocsInIdID.AsInteger:=IDH;
    quDocsInIdDATEDOC.AsDateTime:=Trunc(cxDateEdit1.Date);
    quDocsInIdNUMDOC.AsString:=cxTextEdit1.Text;
    quDocsInIdDATESF.AsDateTime:=Trunc(cxDateEdit2.Date);
    quDocsInIdNUMSF.AsString:=cxTextEdit2.Text;
    quDocsInIdIDCLI.AsInteger:=cxButtonEdit1.Tag;
    quDocsInIdIDSKL.AsInteger:=cxLookupComboBox1.EditValue;
    quDocsInIdSUMIN.AsFloat:=0;
    quDocsInIdSUMUCH.AsFloat:=0;
    quDocsInIdSUMTAR.AsFloat:=0;
    quDocsInIdSUMNDS0.AsFloat:=sNDS[1];
    quDocsInIdSUMNDS1.AsFloat:=sNDS[2];
    quDocsInIdSUMNDS2.AsFloat:=sNDS[3];
    quDocsInIdPROCNAC.AsFloat:=0;
    quDocsInIdIACTIVE.AsInteger:=0;
    quDocsInId.Post;

    cxTextEdit1.Tag:=IDH;

    //�������� ������������
    quSpecInSel.Active:=False;
    quSpecInSel.ParamByName('IDHD').AsInteger:=IDH;
    quSpecInSel.Active:=True;

    quSpecInSel.First; //������� ������
    while not quSpecInSel.Eof do quSpecInSel.Delete;

    sNDS[1]:=0;sNDS[2]:=0;sNDS[3]:=0;
    rSum1:=0; rSum2:=0;
    taSpec.First;
    while not taSpec.Eof do
    begin
      quSpecInSel.Append;
      quSpecInSelIDHEAD.AsInteger:=IDH;
      quSpecInSelID.AsInteger:=GetId('SpecIn');
      quSpecInSelNUM.AsInteger:=taSpecNum.AsInteger;
      quSpecInSelIDCARD.AsInteger:=taSpecIdGoods.AsInteger;
      quSpecInSelQUANT.AsFloat:=taSpecQuant.AsFloat;
      quSpecInSelPRICEIN.AsFloat:=taSpecPrice1.AsFloat;
      quSpecInSelSUMIN.AsFloat:=taSpecSum1.AsFloat;
      quSpecInSelPRICEUCH.AsFloat:=taSpecPrice2.AsFloat;
      quSpecInSelSUMUCH.AsFloat:=taSpecSum2.AsFloat;
      quSpecInSelIDNDS.AsInteger:=taSpecINds.AsInteger;
      quSpecInSelSUMNDS.AsFloat:=taSpecRNds.AsFloat;
      quSpecInSelIDM.AsInteger:=taSpecIM.AsInteger;
      quSpecInSel.Post;
      sNDS[taSpecINds.AsInteger]:=sNDS[taSpecINds.AsInteger]+taSpecRNds.AsFloat;
      rSum1:=rSum1+taSpecSum1.AsFloat; rSum2:=rSum2+taSpecSum2.AsFloat;

      taSpec.Next;
    end;

    quDocsInId.Edit;
    quDocsInIdSUMIN.AsFloat:=rSum1;
    quDocsInIdSUMUCH.AsFloat:=rSum2;
    quDocsInIdSUMTAR.AsFloat:=0;
    quDocsInIdSUMNDS0.AsFloat:=sNDS[1];
    quDocsInIdSUMNDS1.AsFloat:=sNDS[2];
    quDocsInIdSUMNDS2.AsFloat:=sNDS[3];
    quDocsInIdPROCNAC.AsFloat:=0;
    quDocsInIdIACTIVE.AsInteger:=0;
    quDocsInId.Post;

    quDocsInId.Active:=False;

    fmDocsIn.ViewDocsIn.BeginUpdate;
    quDocsInSel.FullRefresh;
    quDocsInSel.Locate('ID',IDH,[]);
    fmDocsIn.ViewDocsIn.EndUpdate;

    ViewDoc1.EndUpdate;
    GridDoc1.SetFocus;
  end;
end;

procedure TfmAddDoc1.acDelAllExecute(Sender: TObject);
begin
  if taSpec.RecordCount>0 then
  begin
    if MessageDlg('�� ������������� ������ �������� ������������ ?',
     mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      taSpec.First; while not taSpec.Eof do taSpec.Delete;
    end;
  end;
end;

procedure TfmAddDoc1.N1Click(Sender: TObject);
Type tRecSpec = record
     Num:INteger;
     IdGoods:Integer;
     NameG:String;
     IM:Integer;
     SM:String;
     Quant,Price1,Price2,Sum1,Sum2:Real;
     iNds:INteger;
     sNds:String;
     rNds,SumNac,ProcNac,Km:Real;
     end;
Var RecSpec:TRecSpec;
    iMax:Integer;
begin
  //���������� �������
  if not taSpec.Eof then
  begin
    RecSpec.Num:=taSpecNum.AsInteger;
    RecSpec.IdGoods:=taSpecIdGoods.AsInteger;
    RecSpec.Quant:=taSpecQuant.AsFloat;
    RecSpec.Price1:=taSpecPrice1.AsFloat;
    RecSpec.Sum1:=taSpecSum1.AsFloat;
    RecSpec.Price2:=taSpecPrice2.AsFloat;
    RecSpec.Sum2:=taSpecSum2.AsFloat;
    RecSpec.iNds:=taSpecINds.AsInteger;
    RecSpec.rNds:=taSpecRNds.AsFloat;
    RecSpec.IM:=taSpecIM.AsInteger;
    RecSpec.NameG:=taSpecNameG.AsString;
    RecSpec.SM:=taSpecSM.AsString;
    RecSpec.sNds:=taSpecsNds.AsString;
    RecSpec.SumNac:=taSpecSumNac.AsFloat;
    RecSpec.ProcNac:=taSpecProcNac.AsFloat;
    RecSpec.Km:=taSpecKm.AsFloat;

    iMax:=1;
    ViewDoc1.BeginUpdate;

    taSpec.First;
    if not taSpec.Eof then
    begin
      taSpec.Last;
      iMax:=taSpecNum.AsInteger+1;
    end;

    taSpec.Append;
    taSpecNum.AsInteger:=iMax;
    taSpecIdGoods.AsInteger:=RecSpec.IdGoods;
    taSpecNameG.AsString:=RecSpec.NameG;
    taSpecIM.AsInteger:=RecSpec.IM;
    taSpecSM.AsString:=RecSpec.SM;
    taSpecKm.AsFloat:=RecSpec.Km;
    taSpecQuant.AsFloat:=RecSpec.Quant;
    taSpecPrice1.AsFloat:=RecSpec.Price1;
    taSpecSum1.AsFloat:=RecSpec.Sum1;
    taSpecPrice2.AsFloat:=RecSpec.Price2;
    taSpecSum2.AsFloat:=RecSpec.Sum2;
    taSpecINds.AsInteger:=RecSpec.iNds;
    taSpecSNds.AsString:=RecSpec.sNds;
    taSpecRNds.AsFloat:=RecSpec.rNds;
    taSpecSumNac.AsFloat:=RecSpec.SumNac;
    taSpecProcNac.AsFloat:=RecSpec.ProcNac;
    taSpec.Post;
    ViewDoc1.EndUpdate;
    GridDoc1.SetFocus;
  end;
end;

procedure TfmAddDoc1.cxLabel6Click(Sender: TObject);
begin
  acDelall.Execute;
end;

procedure TfmAddDoc1.cxDateEdit1PropertiesChange(Sender: TObject);
begin
  cxDateEdit2.EditValue:=cxDateEdit1.EditValue;
end;

procedure TfmAddDoc1.ViewDoc1SMPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
Var rK:Real;
    iM:Integer;
    Sm:String;
begin
  with dmO do
  begin
    if taSpecIm.AsInteger>0 then
    begin
      bAddSpec:=True;
      fmMessure.ShowModal;
      if fmMessure.ModalResult=mrOk then
      begin
        iM:=iMSel; iMSel:=0;
        Sm:=prFindKNM(iM,rK); //�������� ����� �������� � ����� �����

        taSpec.Edit;
        taSpecIM.AsInteger:=iM;
        taSpecKm.AsFloat:=rK;
        taSpecSM.AsString:=Sm;
        taSpec.Post;
      end;
    end;
  end;
end;

procedure TfmAddDoc1.cxButton2Click(Sender: TObject);
begin
  Close;
end;

end.
