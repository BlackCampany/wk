unit LogSpec;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxEdit, DB, cxDBData, ExtCtrls, SpeedBar, cxGridLevel, cxClasses,
  cxControls, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, Placemnt, cxDataStorage, StdCtrls, ActnList,
  XPStyleActnCtrls, ActnMan, FR_Class, FR_DSet, FR_DBSet;

type
  TfmLogSpec = class(TForm)
    StatusBar1: TStatusBar;
    ViewLog: TcxGridDBTableView;
    LevelLog: TcxGridLevel;
    GridLog: TcxGrid;
    SpeedBar1: TSpeedBar;
    FormPlacement1: TFormPlacement;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    ViewLogDATEOP: TcxGridDBColumn;
    ViewLogID_PERSONAL: TcxGridDBColumn;
    ViewLogNAMEOP: TcxGridDBColumn;
    ViewLogCONTENT: TcxGridDBColumn;
    ViewLogNAMEP: TcxGridDBColumn;
    ViewLogOPERNAME: TcxGridDBColumn;
    ViewLogCONTNAME: TcxGridDBColumn;
    ActionManager1: TActionManager;
    acPrint: TAction;
    SpeedItem2: TSpeedItem;
    frRepLog: TfrReport;
    dsRep6: TfrDBDataSet;
    SpeedItem3: TSpeedItem;
    asPeriod: TAction;
    SpeedItem4: TSpeedItem;
    ViewLogZAKAZ: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SpeedItem1Click(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure asPeriodExecute(Sender: TObject);
    procedure OnChangePeriod();
    procedure SpeedItem4Click(Sender: TObject);
  private
    { Private declarations }
  public
    DateFrom: TDateTime;           //���
    DateTo: TDateTime;             //���
    { Public declarations }
  end;

var
  fmLogSpec: TfmLogSpec;

implementation

uses Un1, Dm, MainRep, Period;

{$R *.dfm}

procedure TfmLogSpec.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  ViewLog.RestoreFromIniFile(CurDir+GridIni);
end;

procedure TfmLogSpec.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  dmC.taSpecAllSel.Active:=False;
  ViewLog.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmLogSpec.SpeedItem1Click(Sender: TObject);
begin
  close;
end;

procedure TfmLogSpec.acPrintExecute(Sender: TObject);
Var StrWk:String;
    i:Integer;
begin
  //������
{  StrWk:=ViewLog.DataController.Filter.FilterText;}

  ViewLog.BeginUpdate;
  with dmC do
  begin
    quRepLog.Filter:=ViewLog.DataController.Filter.FilterText;
    quRepLog.Filtered:=True;
    frRepLog.LoadFromFile(CurDir + 'PersLog.frf');

 //���   frVariables.Variable['sPeriod']:='c '+FormatDateTime('dd.mm.yyyy hh:nn',CommonSet.DateFrom)+' �� '+FormatDateTime('dd.mm.yyyy hh:nn',CommonSet.DateTo);
    frVariables.Variable['sPeriod']:='c '+FormatDateTime('dd.mm.yyyy hh:nn',DateFrom)+' �� '+FormatDateTime('dd.mm.yyyy hh:nn',DateTo);
    StrWk:=ViewLog.DataController.Filter.FilterCaption;
    while Pos('AND',StrWk)>0 do
    begin
      i:=Pos('AND',StrWk);
      StrWk[i]:=' ';
      StrWk[i+1]:='�';
      StrWk[i+2]:=' ';
    end;
    while Pos('and',StrWk)>0 do
    begin
      i:=Pos('and',StrWk);
      StrWk[i]:=' ';
      StrWk[i+1]:='�';
      StrWk[i+2]:=' ';
    end;

    frVariables.Variable['sDop']:=StrWk;

    frRepLog.ReportName:='�������� �������� ���������.';
    frRepLog.PrepareReport;
    frRepLog.ShowPreparedReport;

    quRepLog.Filter:='';
    quRepLog.Filtered:=False;
  end;
  ViewLog.EndUpdate;
end;

procedure TfmLogSpec.Button1Click(Sender: TObject);
begin
  showmessage(ViewLog.DataController.Filter.FilterText);
end;

procedure TfmLogSpec.OnChangePeriod();
Var DateBeg,DateEnd:TDateTime;
begin
    Datebeg:=TrebSel.DateFrom;
    DateEnd:=TrebSel.DateTo;

    with dmC do
    begin //������ ��������� ����������
//����� ������ ���������
      quPersonal2.Active:=False;
      quPersonal2.Active:=True;

//����� ������ ����
      quMenuSel.Active:=False;
      quMenuSel.Active:=True;

//
      quRepLog.Active:=False;
//      quRepLog.ParamByName('DATEB').AsString:=FormatDateTime('mm/dd/yyyy hh:nn',DateBeg);
//      quRepLog.ParamByName('DATEE').AsString:=FormatDateTime('mm/dd/yyyy hh:nn',DateEnd);
      quRepLog.ParamByName('DATEB').AsDateTime:=DateBeg;
      quRepLog.ParamByName('DATEE').AsDateTime:=DateEnd;
      quRepLog.Active:=True;

//   fmTabs.Caption:='����������� ������ �� ������ � '+FormatDateTime('dd.mm.yyyy hh:mm',TrebSel.DateFrom)+' �� '+FormatDateTime('dd.mm.yyyy hh:mm',TrebSel.DateTo);
    end;
end;

procedure TfmLogSpec.asPeriodExecute(Sender: TObject);
begin
  try
     fmPeriod:=TfmPeriod.Create(Application); //��� ���� ������������

    fmPeriod.ShowModal;
    if fmPeriod.ModalResult=mrOk then
    begin
      OnChangePeriod();
    end;
  finally
    fmPeriod.Release;
  end;
end;

procedure TfmLogSpec.SpeedItem4Click(Sender: TObject);
begin
   prNExportExel4(ViewLog);
end;

end.
