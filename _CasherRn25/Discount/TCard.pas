unit TCard;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxImageComboBox, Menus,
  cxLookAndFeelPainters, cxMaskEdit, cxSpinEdit, StdCtrls, cxContainer,
  cxTextEdit, cxButtons, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxControls,
  cxGridCustomView, cxGrid, ExtCtrls, cxDropDownEdit, cxCalc, cxLabel,
  Placemnt, cxDBLookupComboBox, FIBDataSet, pFIBDataSet, FIBDatabase,
  pFIBDatabase, cxButtonEdit, cxCalendar, DBClient, cxBlobEdit, FR_DSet,
  FR_DBSet, FR_Class, ActnList, XPStyleActnCtrls, ActnMan, cxCheckBox;

type
  TTK = Record
  Add,Edit:Boolean
  end;

  TfmTCard = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    GTCards: TcxGrid;
    ViewTCards: TcxGridDBTableView;
    LTCards: TcxGridLevel;
    cxButton4: TcxButton;
    cxButton5: TcxButton;
    Panel2: TPanel;
    Panel3: TPanel;
    cxTextEdit1: TcxTextEdit;
    cxTextEdit2: TcxTextEdit;
    Label1: TLabel;
    Label2: TLabel;
    cxTextEdit3: TcxTextEdit;
    Label3: TLabel;
    cxTextEdit4: TcxTextEdit;
    Label4: TLabel;
    cxSpinEdit1: TcxSpinEdit;
    Label5: TLabel;
    cxCalcEdit1: TcxCalcEdit;
    Label6: TLabel;
    cxButton2: TcxButton;
    Image1: TImage;
    cxLabel1: TcxLabel;
    Image2: TImage;
    cxLabel2: TcxLabel;
    GTSpec: TcxGrid;
    ViewTSpec: TcxGridDBTableView;
    LTSpec: TcxGridLevel;
    ViewTCardsIDCARD: TcxGridDBColumn;
    ViewTCardsID: TcxGridDBColumn;
    ViewTCardsDATEB: TcxGridDBColumn;
    ViewTCardsDATEE: TcxGridDBColumn;
    ViewTCardsSHORTNAME: TcxGridDBColumn;
    ViewTCardsRECEIPTNUM: TcxGridDBColumn;
    ViewTCardsPOUTPUT: TcxGridDBColumn;
    ViewTCardsPCOUNT: TcxGridDBColumn;
    ViewTCardsPVES: TcxGridDBColumn;
    ViewTSpecIDCARD: TcxGridDBColumn;
    ViewTSpecNETTO: TcxGridDBColumn;
    ViewTSpecBRUTTO: TcxGridDBColumn;
    ViewTSpecNAME: TcxGridDBColumn;
    ViewTSpecSM: TcxGridDBColumn;
    cxButton6: TcxButton;
    Timer1: TTimer;
    cxButton3: TcxButton;
    cxButton1: TcxButton;
    FormPlacement1: TFormPlacement;
    ViewTSpecIdM: TcxGridDBColumn;
    ViewTCardsSDATEE: TcxGridDBColumn;
    Label7: TLabel;
    cxDateEdit1: TcxDateEdit;
    Image3: TImage;
    cxLabel3: TcxLabel;
    cxButton7: TcxButton;
    Label8: TLabel;
    PopupMenuTCard: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    TCS: TClientDataSet;
    TCH: TClientDataSet;
    TCHIDCARD: TIntegerField;
    TCHID: TIntegerField;
    TCHDATEB: TDateField;
    TCHDATEE: TDateField;
    TCHSHORTNAME: TStringField;
    TCHRECEIPTNUM: TStringField;
    TCHPOUTPUT: TStringField;
    TCHPCOUNT: TIntegerField;
    TCHPVES: TFloatField;
    TCSIDC: TIntegerField;
    TCSIDT: TIntegerField;
    TCSID: TIntegerField;
    TCSIDCARD: TIntegerField;
    TCSCURMESSURE: TIntegerField;
    TCSNETTO: TFloatField;
    TCSBRUTTO: TFloatField;
    TCSKNB: TFloatField;
    dsTCH: TDataSource;
    dsTCS: TDataSource;
    cxButton8: TcxButton;
    cxButton9: TcxButton;
    frRepTTK: TfrReport;
    frtaTTK: TfrDBDataSet;
    taTTK: TClientDataSet;
    taTTKNum: TIntegerField;
    taTTKName: TStringField;
    taTTKSM: TStringField;
    taTTKQuantB: TFloatField;
    taTTKQuantN: TFloatField;
    taTTKQuantN10: TFloatField;
    taTTKQuantN20: TFloatField;
    amTCard: TActionManager;
    acIns: TAction;
    acEdit: TAction;
    acDel: TAction;
    taTSpec: TClientDataSet;
    taTSpecNum: TIntegerField;
    taTSpecIdCard: TIntegerField;
    taTSpecIdM: TIntegerField;
    taTSpecSM: TStringField;
    taTSpecKm: TFloatField;
    taTSpecName: TStringField;
    taTSpecNetto: TFloatField;
    taTSpecBrutto: TFloatField;
    taTSpecKnb: TFloatField;
    dstaTSpec: TDataSource;
    ViewTSpecNum: TcxGridDBColumn;
    acSave: TAction;
    TCSNAME: TStringField;
    cxCheckBox1: TcxCheckBox;
    cxButton10: TcxButton;
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxTextEdit2PropertiesChange(Sender: TObject);
    procedure cxTextEdit3PropertiesChange(Sender: TObject);
    procedure cxTextEdit4PropertiesChange(Sender: TObject);
    procedure cxSpinEdit1PropertiesChange(Sender: TObject);
    procedure cxCalcEdit1PropertiesChange(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
    procedure ViewTCardsFocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure cxButton5Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxLabel1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ViewTSpecNAMESHORTPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxLabel2Click(Sender: TObject);
    procedure Label7Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton7Click(Sender: TObject);
    procedure cxLabel3Click(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure cxButton9Click(Sender: TObject);
    procedure cxButton8Click(Sender: TObject);
    procedure acInsExecute(Sender: TObject);
    procedure acEditExecute(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure ViewTSpecEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure acSaveExecute(Sender: TObject);
    procedure taTSpecNettoChange(Sender: TField);
    procedure ViewTSpecSMPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxButton10Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure  prSetVal;
    procedure  prSetOn;
    procedure  prSaveTForce;
    procedure prTestAndSave;
  end;

var
  fmTCard: TfmTCard;
  TK: TTK;
  bClearTK:Boolean = False;

implementation

uses dmOffice, Un1, PeriodUni, Goods, FindResult, CurMessure, GoodsSel,
  CalcSeb, TBuff, Refer, FCards, OMessure;

{$R *.dfm}

procedure TfmTCard.prTestAndSave;
Var bSave:Boolean;
    StrWk:String;
    k:Integer;
begin
  bSave:=False;
  with dmO do
  begin
    ViewTSpec.BeginUpdate;

    quTSpec.Active:=False;
    quTSpec.ParamByName('IDC').AsInteger:=quTCardsIDCARD.AsInteger;
    quTSpec.ParamByName('IDTC').AsInteger:=quTCardsID.AsInteger;
    quTSpec.Active:=True;

    if quTSpec.RecordCount<>taTSpec.RecordCount then bSave:=True;

    if bSave=False then
    begin
      quTSpec.First; taTSpec.First;  k:=0;
      while not quTSpec.Eof do
      begin
        if taTSpecIdCard.AsInteger<>quTSpecIdCard.AsInteger then begin bSave:=True; k:=1; end;
        if taTSpecIdM.AsInteger<>quTSpecCURMESSURE.AsInteger then begin bSave:=True; k:=2; end;
        if taTSpecKm.AsFloat<>quTSpecKNB.AsFloat then begin bSave:=True; k:=3; end;
        if taTSpecNetto.AsFloat<>quTSpecNETTO.AsFloat then begin bSave:=True; k:=4; end;

        if bSave then
        begin
          StrWk:=taTSpecIdCard.AsString+' '+taTSpecName.AsString+' _'+IntToStr(k);
          break;
        end;

        quTSpec.Next; taTSpec.Next;
      end;
    end;
    
    quTSpec.Active:=False;
    taTSpec.Last;

    ViewTSpec.EndUpdate;
  end;

  if bSave then
    if MessageDlg('�� ����������('+StrWk+'), ���������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then prSaveTForce;
end;

procedure  TfmTCard.prSetOn;
begin
  cxButton1.Enabled:=True;
end;

procedure  TfmTCard.prSetVal;
var kBrutto:Real;
    iDate:Integer;
    iMax:INteger;
begin
  with dmO do
  begin
//    showmessage('��������� ������������?');


    cxTextEdit2.Text:=quTCardsSHORTNAME.AsString;
    cxTextEdit3.Text:=quTCardsRECEIPTNUM.AsString;
    cxTextEdit4.Text:=quTCardsPOUTPUT.AsString;
    cxSpinEdit1.EditValue:=quTCardsPCOUNT.AsInteger;
    cxCalcEdit1.EditValue:=quTCardsPVES.AsFloat;

    taTSpec.Active:=False;
    taTSpec.CreateDataSet;

    ViewTSpec.BeginUpdate;

    quTSpec.Active:=False;
    quTSpec.ParamByName('IDC').AsInteger:=quTCardsIDCARD.AsInteger;
    quTSpec.ParamByName('IDTC').AsInteger:=quTCardsID.AsInteger;
    quTSpec.Active:=True;


    iMax:=1;
    quTSpec.First;
    while not quTSpec.Eof do
    begin
      taTSpec.Append;
      taTSpecNum.AsInteger:=iMax;
      taTSpecIdCard.AsInteger:=quTSpecIDCARD.AsInteger;
      taTSpecIdM.AsInteger:=quTSpecCURMESSURE.AsInteger;
      taTSpecSM.AsString:=quTSpecNAMESHORT.AsString;
      taTSpecKm.AsFloat:=quTSpecKOEF.AsFloat;
      taTSpecName.AsString:=quTSpecNAME.AsString;
      taTSpecNetto.AsFloat:=quTSpecNETTO.AsFloat;
      taTSpecBrutto.AsFloat:=quTSpecBRUTTO.asfloat;
      taTSpecKnb.AsFloat:=0;
      taTSpec.Post;

      quTSpec.Next;   inc(iMax);
    end;
    quTSpec.Active:=False;

    ViewTSpecBRUTTO.Caption:='������ �� '+FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date);
    iDate:=prDateToI(cxDateEdit1.Date);

    taTSpec.First;
    while not taTSpec.Eof do
    begin
      //���������� ������
      kBrutto:=0;
      quFindEU.Active:=False;
      quFindEU.ParamByName('GOODSID').AsInteger:=taTSpecIDCARD.AsInteger;
      quFindEU.ParamByName('DATEB').AsInteger:=iDate;
      quFindEU.ParamByName('DATEE').AsInteger:=iDate;
      quFindEU.Active:=True;

      if quFindEU.RecordCount>0 then
      begin
        quFindEU.First;
        kBrutto:=quFindEUTO100GRAMM.AsFloat;
      end;

      quFindEU.Active:=False;

      taTSpec.Edit;
      taTSpecBRUTTO.AsFloat:=taTSpecNETTO.AsFloat*(100+kBrutto)/100;
      taTSpecKNB.AsFloat:=kBrutto;
      taTSpec.Post;

      taTSpec.Next;
    end;

    ViewTSpec.EndUpdate;
    cxButton1.Enabled:=False;

    ViewTSpec.OptionsData.Editing:=False;
    cxLabel1.Enabled:=False;
    cxLabel2.Enabled:=False;
    Image1.Enabled:=False;
    Image2.Enabled:=False;
    cxButton10.Enabled:=False;

  end;
end;

procedure TfmTCard.cxButton2Click(Sender: TObject);
begin
  prTestAndSave;
  close;
end;

procedure TfmTCard.Timer1Timer(Sender: TObject);
begin
  if bClearTK=True then begin StatusBar1.Panels[0].Text:='';bClearTK:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearTK:=True;
end;

procedure TfmTCard.cxButton4Click(Sender: TObject);
Var IId:Integer;
    DateB,DateE:TDateTime;
    iDate,iC:Integer;
begin
  //���������� ��
  if not CanDo('prAddTK') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  with dmO do
  begin
    //��� ���� �������� �� ���������� �������
    if quTCards.RecordCount>0 then if cxButton10.Enabled then prTestAndSave;

    fmPeriodUni.DateTimePicker1.Date:=Date;
    fmPeriodUni.DateTimePicker2.Date:=iMaxDate;
    fmPeriodUni.ShowModal;
    if fmPeriodUni.ModalResult=mrOk then
    begin
      DateB:=Trunc(fmPeriodUni.DateTimePicker1.Date);
      DateE:=Trunc(fmPeriodUni.DateTimePicker2.Date)+1;

      //�������� �� �������������
      prTestUseTC(cxTextEdit1.Tag,iDate,iC);
      if Trunc(DateB)<=iDate then
      begin //� ������ �������� ���� ������������� - �������� ����������
        Showmessage('�� ���� � �������� (���� - '+FormatDateTime('dd.mm.yyyy',iDate)+', ��� ����� - '+IntToStr(iC)+'). ������� ������ ������ �������� !!!');
        exit;
      end;

      if quTCards.RecordCount>0 then
      begin //�������� ���� ��������� �������� ���� ���������� �� ��������� �� �������
        quTCards.Last;
        prSetVal;
        if quTCardsDATEB.AsDateTime>=DateB then
        begin
          showmessage('������ ������ ��� ���������, ���������� ����������.');
          exit;
        end;
        if quTCardsDATEE.AsDateTime>=iMaxDate then
        begin
          quTCards.Edit;
          quTCardsDATEE.AsDateTime:=Trunc(DateB-1);
          quTCards.Post;
        end;
      end;

      iId:=GetId('TCards');

      quTCards.Append;
      quTCardsIDCARD.AsInteger:=cxTextEdit1.Tag;
      quTCardsID.AsInteger:=IId;
      quTCardsDATEB.AsDateTime:=Trunc(DateB);
      quTCardsDATEE.AsDateTime:=Trunc(DateE-1); //��� ������������
      quTCardsSHORTNAME.AsString:=cxTextEdit1.Text;
      quTCardsRECEIPTNUM.AsString:='';
      quTCardsPOUTPUT.AsString:='';
      quTCardsPCOUNT.AsInteger:=1;
      quTCardsPVES.AsFloat:=0;
      quTCards.Post;

      if quCardsSelTCARD.AsInteger=0 then
      begin
        quCardsSel.Edit;
        quCardsSelTCARD.AsInteger:=1;
        quCardsSel.Post;
        quCardsSel.Refresh;
      end;

      if Panel3.Visible=False then Panel3.Visible:=True;
      prSetVal;
//      prSetOn;

    end;
  end;
end;

procedure TfmTCard.cxButton1Click(Sender: TObject);
begin
  cxButton1.Enabled:=False;
  with dmO do
  begin
    quTCards.Edit;
    quTCardsSHORTNAME.AsString:=cxTextEdit2.Text;
    quTCardsRECEIPTNUM.AsString:=cxTextEdit3.Text;
    quTCardsPOUTPUT.AsString:=cxTextEdit4.Text;
    quTCardsPCOUNT.AsInteger:=cxSpinEdit1.EditValue;
    quTCardsPVES.AsFloat:=cxCalcEdit1.EditValue;
    quTCards.Post;
  end;
end;

procedure TfmTCard.cxTextEdit2PropertiesChange(Sender: TObject);
begin
  prSetOn;

end;

procedure TfmTCard.cxTextEdit3PropertiesChange(Sender: TObject);
begin
  prSetOn;

end;

procedure TfmTCard.cxTextEdit4PropertiesChange(Sender: TObject);
begin
  prSetOn;

end;

procedure TfmTCard.cxSpinEdit1PropertiesChange(Sender: TObject);
begin
  prSetOn;
end;

procedure TfmTCard.cxCalcEdit1PropertiesChange(Sender: TObject);
begin
  prSetOn;
end;

procedure TfmTCard.cxButton6Click(Sender: TObject);
begin
  // ������������� ������
  if not CanDo('prEditTKPeriod') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  with dmO do
  begin
    CommonSet.DateFrom:=Trunc(quTCardsDATEB.AsDateTime);
    CommonSet.DateTo:=Trunc(quTCardsDATEE.AsDateTime)+1;

    fmPeriodUni.DateTimePicker1.Date:=CommonSet.DateFrom;
    fmPeriodUni.DateTimePicker2.Date:=CommonSet.DateTo-1;
    fmPeriodUni.ShowModal;
    if fmPeriodUni.ModalResult=mrOk then
    begin
      CommonSet.DateFrom:=Trunc(fmPeriodUni.DateTimePicker1.Date);
      CommonSet.DateTo:=Trunc(fmPeriodUni.DateTimePicker2.Date)+1;

      quTCards.Edit;
      quTCardsDATEB.AsDateTime:=Trunc(CommonSet.DateFrom);
      quTCardsDATEE.AsDateTime:=Trunc(CommonSet.DateTo-1); //��� ������������
      quTCards.Post;
    end;
  end;
end;

procedure TfmTCard.ViewTCardsFocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord;
  ANewItemRecordFocusingChanged: Boolean);
begin
//  showmessage('1');
  if Visible then prSetVal;
end;

procedure TfmTCard.cxButton5Click(Sender: TObject);
Var i,iDate,iC:Integer;
begin
  //������� ��
  if not CanDo('prDelTK') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  with dmO do
  begin
    if quTCards.RecordCount>0 then
    begin
          //�������� �� ��������� ��������
      prTestUseTC(quTCardsIDCARD.AsInteger,iDate,iC);
      if Trunc(quTCardsDATEB.AsDateTime)<=iDate then
      begin //� ������ �������� ���� ������������� - �������� ����������
        Showmessage('�� ���� � �������� (���� - '+FormatDateTime('dd.mm.yyyy',iDate)+', ��� ����� - '+IntToStr(iC)+'). �������� ���������� !!!');
        exit;
      end;

    if MessageDlg('�� ������������� ������ ������� �� ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      quTCards.Delete;
      if quTCards.RecordCount=0 then
      begin

        if taTSpec.Active then
        begin
          taTSpec.First;
          while not taTSpec.Eof do taTSpec.Delete;
        end;  

        Panel3.Visible:=False;

        //����� ������� ���� ����� �������� � ��������� ��
        if quCardsSel.locate('ID',cxTextEdit1.Tag,[]) = False then
        begin
          quFind.Active:=False;
          quFind.SelectSQL.Clear;
          quFind.SelectSQL.Add('SELECT ID,PARENT,NAME');
          quFind.SelectSQL.Add('FROM OF_CARDS');
          quFind.SelectSQL.Add('where ID ='+IntToStr(cxTextEdit1.Tag));
          quFind.Active:=True;

          if quFind.RecordCount>0 then
          begin
            for i:=0 to fmGoods.ClassTree.Items.Count-1 Do
            if Integer(fmGoods.ClassTree.Items[i].Data) = quFindPARENT.AsInteger Then
            begin
              fmGoods.ClassTree.Items[i].Expand(False);
              fmGoods.ClassTree.Repaint;
              fmGoods.ClassTree.Items[i].Selected:=True;
              Break;
            End;
            delay(10);
            quCardsSel.First;
            if quCardsSel.locate('ID',cxTextEdit1.Tag,[]) then
            begin
              quCardsSel.Edit;
              quCardsSelTCARD.AsInteger:=0;
              quCardsSel.Post;
              quCardsSel.Refresh;
            end;
          end;

        end else
        begin
          quCardsSel.Edit;
          quCardsSelTCARD.AsInteger:=0;
          quCardsSel.Post;
          quCardsSel.Refresh;
        end;
      end else
      begin
        quTCards.Last;

        quTCards.Edit;
        quTCardsDATEE.AsDateTime:=iMaxDate;
        quTCards.Post;

        quTCards.Refresh;

        prSetVal;
      end;
    end;
    end;
  end;
end;

procedure TfmTCard.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  ViewTSpec.RestoreFromIniFile(CurDir+GridIni);

end;

procedure TfmTCard.cxLabel1Click(Sender: TObject);
begin
  //�������� �������
  with dmO do
  begin
    if quTCards.RecordCount>0 then
    begin
      bAddTSpec:=True;
      quCardsSel1.Active:=False;
      quCardsSel1.Active:=True;
      fmGoodsSel.Show;
    end else showmessage('�������� ������� ��.');
  end;
end;

procedure TfmTCard.FormShow(Sender: TObject);
begin
  cxDateEdit1.Date:=Date;
  cxLabel3.Tag:=0;
end;

procedure TfmTCard.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewTSpec.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmTCard.ViewTSpecNAMESHORTPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
//Var rN,rB,rK,rKOld:Real;
//    Id:Integer;
begin
{  with dmO do
  begin
    if quTSpec.RecordCount>0 then
    begin
      quMessureSel.Active:=False;
      quMessureSel.ParamByName('IDM').AsInteger:=quTSpecCURMESSURE.AsInteger;
      quMessureSel.Active:=True;

      quMessureSel.Locate('ID',quTSpecCURMESSURE.AsInteger,[]);

      fmCurMessure.ShowModal;
      if fmCurMessure.ModalResult=mrOk then
      begin
        rN:=quTSpecNETTO.AsFloat;
        rB:=quTSpecBRUTTO.AsFloat;
        rK:=quMessureSelKOEF.AsFloat;
        rKOld:=quTSpecKOEF.AsFloat;
        Id:=quTSpecID.AsInteger;

        quTSpec.Edit;
        quTSpecCURMESSURE.AsInteger:=quMessureSelID.AsInteger;
        if (rK=0) or (rN=0) then
        begin
          quTSpecNETTO.AsFloat:=0;
          quTSpecBRUTTO.AsFloat:=0;
        end else
        begin
          quTSpecNETTO.AsFloat:=RoundEx(rN*rKOld/rK*1000)/1000;
          quTSpecBRUTTO.AsFloat:=(rB/rN)*RoundEx(rN*rKOld/rK*1000)/1000;
        end;
//        quTSpecNAMESHORT.AsString:=quMessureSelNAMESHORT.AsString;
        quTSpec.Post;
        quTSpec.FullRefresh;
        quTSpec.Locate('ID',ID,[]);
      end;
      quMessureSel.Active:=False;
    end;
  end;}
end;

procedure TfmTCard.cxLabel2Click(Sender: TObject);
begin
  with dmO do
  begin
    if taTSpec.RecordCount>0 then
    begin
      if MessageDlg('�� ������������� ������ ������� "'+taTSpecNAME.AsString+'" ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        taTSpec.Delete;
      end;
    end;
  end;
end;

procedure TfmTCard.Label7Click(Sender: TObject);
Var kBrutto:Real;
    iDate:INteger;
begin
//  showmessage('2');
//  prSetVal;
  with dmO do
  begin
    ViewTSpec.BeginUpdate;

    ViewTSpecBRUTTO.Caption:='������ �� '+FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date);
    iDate:=prDateToI(cxDateEdit1.Date);

    taTSpec.First;
    while not taTSpec.Eof do
    begin
      //���������� ������
      kBrutto:=0;
      quFindEU.Active:=False;
      quFindEU.ParamByName('GOODSID').AsInteger:=taTSpecIDCARD.AsInteger;
      quFindEU.ParamByName('DATEB').AsInteger:=iDate;
      quFindEU.ParamByName('DATEE').AsInteger:=iDate;
      quFindEU.Active:=True;

      if quFindEU.RecordCount>0 then
      begin
        quFindEU.First;
        kBrutto:=quFindEUTO100GRAMM.AsFloat;
      end;

      quFindEU.Active:=False;

      taTSpec.Edit;
      taTSpecBRUTTO.AsFloat:=taTSpecNETTO.AsFloat*(100+kBrutto)/100;
      taTSpecKNB.AsFloat:=kBrutto;
      taTSpec.Post;

      taTSpec.Next;
    end;

    ViewTSpec.EndUpdate;
  end;
end;

procedure TfmTCard.cxButton3Click(Sender: TObject);
begin
//�������������
  fmCalcSeb:=tfmCalcSeb.Create(Application);
  fmCalcSeb.Label4.Caption:=cxTextEdit1.Text;
  fmCalcSeb.Label1.Caption:=FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date);
  fmCalcSeb.ShowModal;
  fmCalcSeb.quMHAll.Active:=False;
  fmCalcSeb.taSeb.Active:=False;

  fmCalcSeb.Release;
end;

procedure TfmTCard.cxButton7Click(Sender: TObject);
Var iDate,iC:Integer;
begin
  //�������� ���������
  prTestUseTC(dmO.quTCardsIDCARD.AsInteger,iDate,iC);
  Label8.Caption:=IntToStr(iDate)+'   '+IntToStr(iC);
end;

procedure TfmTCard.cxLabel3Click(Sender: TObject);
begin
  acEdit.Execute;
end;

procedure TfmTCard.N1Click(Sender: TObject);
begin
  //���������� ��
  with dmO do
  begin
    if quTCards.RecordCount=0 then exit;

    TCH.Active:=False;
    TCH.FileName:=CurDir+'TCH.cds';
    if FileExists(CurDir+'TCH.cds') then TCH.Active:=True
    else TCH.CreateDataSet;

    TCS.Active:=False;
    TCS.FileName:=CurDir+'TCS.cds';
    if FileExists(CurDir+'TCS.cds') then TCS.Active:=True
    else TCS.CreateDataSet;

    if TCH.Locate('ID',quTCardsID.AsInteger,[])=False
    then
    begin
      TCH.Append;
      TCHIDCARD.AsInteger:=quTCardsIDCARD.AsInteger;
      TCHID.AsInteger:=quTCardsID.AsInteger;
      TCHDATEB.AsDateTime:=quTCardsDATEB.AsDateTime;
      TCHDATEE.AsDateTime:=quTCardsDATEE.AsDateTime;
      TCHSHORTNAME.AsString:=quTCardsSHORTNAME.AsString;
      TCHRECEIPTNUM.AsString:=quTCardsRECEIPTNUM.AsString;
      TCHPOUTPUT.AsString:=quTCardsPOUTPUT.AsString;
      TCHPCOUNT.AsInteger:=quTCardsPCOUNT.AsInteger;
      TCHPVES.AsFloat:=quTCardsPVES.AsFloat;
      TCH.Post;

      taTSpec.First;
      while not taTSpec.Eof do
      begin
        TCS.Append;
        TCSIDC.AsInteger:=quTCardsIDCARD.AsInteger;
        TCSIDT.AsInteger:=quTCardsID.AsInteger;
        TCSID.AsInteger:=taTSpecNum.AsInteger;
        TCSIDCARD.AsInteger:=taTSpecIDCARD.AsInteger;
        TCSCURMESSURE.AsInteger:=taTSpecIdM.AsInteger;
        TCSNETTO.AsFloat:=taTSpecNETTO.AsFloat;
        TCSBRUTTO.AsFloat:=taTSpecBRUTTO.AsFloat;
        TCSKNB.AsFloat:=taTSpecKm.AsFloat;
        TCSNAME.AsString:=taTSpecName.AsString;
        TCS.Post;

        taTSpec.Next;
      end;

    end else
    begin
      showmessage('�� "'+quTCardsSHORTNAME.AsString+'"  ��� ���� � ������.');
    end;
  end;

  TCH.Active:=False;
  TCS.Active:=False;
end;

procedure TfmTCard.N2Click(Sender: TObject);
Var IId:Integer;
    DateB,DateE:TDateTime;
    iDate,iC,iCurDate:Integer;
    bAdd:Boolean;
    kBrutto:Real;
    Km:Real;
    iMax:Integer;
begin
  // ��������
  TCH.Active:=False;
  TCH.FileName:=CurDir+'TCH.cds';
  if FileExists(CurDir+'TCH.cds') then TCH.Active:=True
  else TCH.CreateDataSet;

  TCS.Active:=False;
  TCS.FileName:=CurDir+'TCS.cds';
  if FileExists(CurDir+'TCS.cds') then TCS.Active:=True
  else TCS.CreateDataSet;

  fmTBuff:=TfmTBuff.Create(Application);

  fmTBuff.LevelTH.Visible:=True;
  fmTBuff.LevelTS.Visible:=True;
  fmTBuff.LevelD.Visible:=False;
  fmTBuff.LevelDSpec.Visible:=False;

  fmTBuff.ShowModal;
  if fmTBuff.ModalResult=mrOk then
  begin //���������
    if TCH.RecordCount>0 then
    begin
      if CanDo('prAddTK') then
      begin
        with dmO do
        begin
          fmPeriodUni.DateTimePicker1.Date:=TCHDATEB.AsDateTime;
          fmPeriodUni.DateTimePicker2.Date:=iMaxDate;
          fmPeriodUni.ShowModal;
          if fmPeriodUni.ModalResult=mrOk then
          begin
            DateB:=Trunc(fmPeriodUni.DateTimePicker1.Date);
            DateE:=Trunc(fmPeriodUni.DateTimePicker2.Date)+1;

           //�������� �� �������������

            prTestUseTC(cxTextEdit1.Tag,iDate,iC);
            if Trunc(DateB)>iDate then
            begin //� ������ �������� �� ���� ������������� - �������� ����������

              bAdd:=True;
              if quTCards.RecordCount>0 then
              begin //�������� ���� ��������� �������� ���� ���������� �� ��������� �� �������
                quTCards.Last;

                if quTCardsDATEB.AsDateTime<DateB then
                begin
                  if quTCardsDATEE.AsDateTime>=iMaxDate then
                  begin
                    quTCards.Edit;
                    quTCardsDATEE.AsDateTime:=Trunc(DateB-1);
                    quTCards.Post;
                  end else
                  begin
                    if quTCardsDATEE.AsDateTime>=DateB then
                    begin
                      showmessage('������ ������ ��� ���������, ���������� ����������.');
                      bAdd:=False;
                    end;
                  end;
                end else
                begin
                  showmessage('������ ������ ��� ���������, ���������� ����������.');
                  bAdd:=False;
                end;
              end;

              if bAdd then
              begin
                iId:=GetId('TCards');

                quTCards.Append;
                quTCardsIDCARD.AsInteger:=cxTextEdit1.Tag;
                quTCardsID.AsInteger:=IId;
                quTCardsDATEB.AsDateTime:=Trunc(DateB);
                quTCardsDATEE.AsDateTime:=Trunc(DateE-1); //��� ������������
                quTCardsSHORTNAME.AsString:=cxTextEdit1.Text;
                quTCardsRECEIPTNUM.AsString:='';
                quTCardsPOUTPUT.AsString:='';
                quTCardsPCOUNT.AsInteger:=TCHPCOUNT.AsInteger;
                quTCardsPVES.AsFloat:=TCHPVES.AsFloat;
                quTCards.Post;

                if quCardsSelTCARD.AsInteger=0 then  //�������� �������� ����
                begin
                  quCardsSel.Edit;
                  quCardsSelTCARD.AsInteger:=1;
                  quCardsSel.Post;
                  quCardsSel.Refresh;
                end;

                if Panel3.Visible=False then Panel3.Visible:=True;

                cxTextEdit2.Text:=quTCardsSHORTNAME.AsString;
                cxTextEdit3.Text:=quTCardsRECEIPTNUM.AsString;
                cxTextEdit4.Text:=quTCardsPOUTPUT.AsString;
                cxSpinEdit1.EditValue:=quTCardsPCOUNT.AsInteger;
                cxCalcEdit1.EditValue:=quTCardsPVES.AsFloat;

                //�������� ��� ������������  �� TCS
                ViewTSpec.BeginUpdate;

                taTSpec.Active:=False;
                taTSpec.CreateDataSet;

                iMax:=0;
                TCS.First;
                while not TCS.Eof do
                begin
                  if TCSIDT.AsInteger=TCHID.AsInteger then
                  begin

                  //��� ������� �� ���� �� ������� ��� ����� ������
                 //���� ������� ��������� ���� � ��������� ������ � ��� �� ������ ���������
                    if prCanSel(cxTextEdit1.Tag,TCSIDCARD.asInteger) then
                    begin
                      kBrutto:=0;
                      iCurDate:=prDateToI(cxDateEdit1.Date);
                      quFindEU.Active:=False;
                      quFindEU.ParamByName('GOODSID').AsInteger:=TCSIDCARD.asInteger;
                      quFindEU.ParamByName('DATEB').AsInteger:=iCurDate;
                      quFindEU.ParamByName('DATEE').AsInteger:=iCurDate;
                      quFindEU.Active:=True;

                      if quFindEU.RecordCount>0 then
                      begin
                        quFindEU.First;
                        kBrutto:=quFindEUTO100GRAMM.AsFloat;
                      end;

                      quFindEU.Active:=False;

                      iMax:=iMax+1;
                      taTSpec.Append;
                      taTSpecNum.AsInteger:=iMax;
                      taTSpecIDCARD.AsInteger:=TCSIDCARD.asInteger;
                      taTSpecIDM.AsInteger:=TCSCURMESSURE.AsInteger;
                      taTSpecSM.AsString:=prFindKNM(TCSCURMESSURE.AsInteger,Km);
                      taTSpecKm.AsFloat:=km;
                      taTSpecNETTO.AsFloat:=TCSNETTO.AsFloat;
                      taTSpecBRUTTO.AsFloat:=TCSNETTO.AsFloat*(100+kBrutto)/100;
                      taTSpecKNB.AsFloat:=kBrutto;
                      taTSpecName.AsString:=TCSNAME.AsString;
                      taTSpec.Post;
                    end;
{                    else  //������ �� ��������  ��� ��������������
                    begin
                      showmessage('���������� ����������: ����������� ������.');
                    end; }

                  end;
                  TCS.Next;
                end;
                taTSpec.Last;
                ViewTSpec.EndUpdate;

                acEdit.Execute; //����� � ����� ��������������
              end;
            end else
            begin //� ������ �������� ���� ������������� - �������� ����������
              Showmessage('�� ���� � �������� (���� - '+FormatDateTime('dd.mm.yyyy',iDate)+', ��� ����� - '+IntToStr(iC)+'). ������� ������ ������ �������� !!!');
              exit;
            end;
          end;
        end;
      end else showmessage('��� ����.');
    end;
  end;
  fmTBuff.Release;

  TCH.Active:=False;
  TCS.Active:=False;
end;

procedure TfmTCard.cxButton9Click(Sender: TObject);
begin
  with dmO do
  begin
    if quTCards.RecordCount=0 then exit;
    fmComm:=TfmComm.Create(Application);
    fmComm.Memo1.Text:=quTCardsTEHNO.AsString;
    fmComm.Memo2.Text:=quTCardsOFORM.AsString;
    fmComm.ShowModal;
    if fmComm.ModalResult=mrOk then
    begin
      quTCards.Edit;
      quTCardsTEHNO.AsString:=fmComm.Memo1.Text;
      quTCardsOFORM.AsString:=fmComm.Memo2.Text;
      quTCards.Post;
    end;
    fmComm.Release;
  end;
end;

procedure TfmTCard.cxButton8Click(Sender: TObject);
Var kp,iMax:Integer;
begin
  //������ ���
  with dmO do
  begin
    if quTCards.RecordCount=0 then exit;

    taTTK.Active:=False;
    taTTK.CreateDataSet;

    kp:=cxSpinEdit1.EditValue; if kp=0 then kp:=1;
    iMax:=1;
    
    taTSpec.First;
    while not taTSpec.Eof do
    begin
      taTTK.Append;
      taTTKNum.AsInteger:=iMax;
      taTTKName.AsString:=taTSpecNAME.AsString;
      if taTSpecIdM.AsInteger=1 then
      begin //��������� � ������ ��������� ���� � �����������
        taTTKSM.AsString:='�.';
        taTTKQuantB.AsFloat:=taTSpecBRUTTO.AsFloat/kp*1000; //���� �� ���� ������
        taTTKQuantN.AsFloat:=taTSpecNETTO.AsFloat/kp*1000;
      end else
      begin
        taTTKSM.AsString:=taTSpecSM.AsString;
        taTTKQuantB.AsFloat:=taTSpecBRUTTO.AsFloat/kp; //���� �� ���� ������
        taTTKQuantN.AsFloat:=taTSpecNETTO.AsFloat/kp;
      end;
      taTTKQuantN10.AsFloat:=taTSpecNETTO.AsFloat/kp*taTSpecKm.AsFloat*10; //���� � �������� � �� 10 ������
      taTTKQuantN20.AsFloat:=quTSpecNETTO.AsFloat/kp*taTSpecKm.AsFloat*20; //���� � �������� � �� 20 ������
      taTTK.Post;

      taTSpec.Next;
      inc(iMax);
    end;

    frRepTTK.LoadFromFile(CurDir + 'ttk.frf');

    frVariables.Variable['Num']:=quTCardsID.AsString;
    frVariables.Variable['Name']:=cxTextEdit1.Text;
    frVariables.Variable['MBlud']:=cxCalcEdit1.EditValue;
    frVariables.Variable['TP']:=quTCardsTEHNO.AsString;
    frVariables.Variable['TREB']:=quTCardsOFORM.AsString;

    frRepTTK.ReportName:='���.';
    frRepTTK.PrepareReport;
    frRepTTK.ShowPreparedReport;

    taTTK.Active:=False;
  end;
end;

procedure TfmTCard.acInsExecute(Sender: TObject);
Var iMax:Integer;
begin
//��������
  if ViewTSpec.OptionsData.Editing then
  begin //��������� ��������� ������
    iMax:=1;

    taTSpec.First;
    if not taTSpec.Eof then
    begin
      taTSpec.Last;
      iMax:=taTSpecNum.AsInteger+1;
    end;

    taTSpec.Append;
    taTSpecNum.AsInteger:=iMax;
    taTSpecIDCARD.AsInteger:=0;
    taTSpecIdM.AsInteger:=-1;
    taTSpecSM.AsString:='';
    taTSpecKm.AsFloat:=0;
    taTSpecName.AsString:='';
    taTSpecNETTO.AsFloat:=0;
    taTSpecBRUTTO.AsFloat:=0;
    taTSpecKNB.AsFloat:=0;
    taTSpec.Post;
  end;
  GTSpec.SetFocus;
end;

procedure TfmTCard.acEditExecute(Sender: TObject);
Var iDate,iC:Integer;
begin
  //�������� �� ����������� ��������������
  with dmO do
  begin
    prTestUseTC(quTCardsIDCARD.AsInteger,iDate,iC);
    if Trunc(quTCardsDATEB.AsDateTime)<=iDate then
    begin //� ������ �������� ���� ������������� - �������� ����������
      Showmessage('�� ���� � �������� (���� - '+FormatDateTime('dd.mm.yyyy',iDate)+', ��� ����� - '+IntToStr(iC)+'). �������������� ���������� !!!');
      exit;
    end
    else
    begin
      fmTCard.ViewTSpec.OptionsData.Editing:=True;
      cxLabel1.Enabled:=True;
      cxLabel2.Enabled:=True;
      Image1.Enabled:=True;
      Image2.Enabled:=True;
      StatusBar1.Panels[1].Text:='��������������, ��� ���������� Ctrl+S';
      cxButton10.Enabled:=True;
    end;
  end;
end;

procedure TfmTCard.acDelExecute(Sender: TObject);
begin
  if ViewTSpec.OptionsData.Editing then //��������� ��������� ������
  with dmO do
  begin
    if taTSpec.RecordCount>0 then
    begin
      taTSpec.Delete;
    end;
  end;
end;

procedure TfmTCard.ViewTSpecEditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
Var iCode:Integer;
    Km:Real;
    sName:String;
begin
with dmO do
  begin
    if (Key=$0D) then
    begin
      if ViewTSpec.Controller.FocusedColumn.Name='ViewTSpecIDCARD' then
      begin
        iCode:=VarAsType(AEdit.EditingValue, varInteger);

        quFCard.Active:=False;
        quFCard.SelectSQL.Clear;
        quFCard.SelectSQL.Add('SELECT ID,NAME,IMESSURE,PARENT,TCARD,INDS');
        quFCard.SelectSQL.Add('FROM OF_CARDS');
        quFCard.SelectSQL.Add('where ID='+IntToStr(iCode));
        quFCard.Active:=True;

        if quFCard.RecordCount=1 then
        begin
          if prCanSel(quCardsSelId.asInteger,iCode) then
          begin
            taTSpec.Edit;
            taTSpecIDCARD.AsInteger:=iCode;
            taTSpecName.AsString:=quFCardNAME.AsString;
            taTSpecNETTO.AsFloat:=0;
            taTSpecBRUTTO.AsFloat:=0;
            taTSpecKNB.AsFloat:=0;

            if cxCheckBox1.Checked then
            begin
              taTSpecIdM.AsInteger:=4;
              taTSpecSM.AsString:='�.';
              taTSpecKm.AsFloat:=0.001;
            end else
            begin
              taTSpecIdM.AsInteger:=quFCardIMESSURE.AsInteger;
              taTSpecSM.AsString:=prFindKNM(quFCardIMESSURE.AsInteger,Km);
              taTSpecKm.AsFloat:=Km;
            end;

            taTSpec.Post;

          end else
          begin
            Showmessage('����������� ������, ���������� ����������.');
          end;
        end;
      end;
      if ViewTSpec.Controller.FocusedColumn.Name='ViewTSpecNAME' then
      begin
        if AEdit.EditingValue <> null then
        begin
          sName:=VarAsType(AEdit.EditingValue, varString);

          quFCard.Active:=False;
          quFCard.SelectSQL.Clear;
          quFCard.SelectSQL.Add('SELECT ID,NAME,IMESSURE,PARENT,TCARD,INDS');
          quFCard.SelectSQL.Add('FROM OF_CARDS');
          quFCard.SelectSQL.Add('where UPPER(NAME) like ''%'+AnsiUpperCase(sName)+'%''');
          quFCard.SelectSQL.Add('Order by NAME');

          quFCard.Active:=True;

//        bAdd:=True;
        //�������� ����� ������ � ����� ������
          fmFCards.ShowModal;
          if fmFCards.ModalResult=mrOk then
          begin
            if quFCard.RecordCount>0 then
            begin
              if prCanSel(quCardsSelId.asInteger,quFCardID.AsInteger) then
              begin
                taTSpec.Edit;
                taTSpecIDCARD.AsInteger:=quFCardID.AsInteger;

                if cxCheckBox1.Checked then
                begin
                  taTSpecIdM.AsInteger:=4;
                  taTSpecSM.AsString:='�.';
                  taTSpecKm.AsFloat:=0.001;
                end else
                begin
                  taTSpecIdM.AsInteger:=quFCardIMESSURE.AsInteger;
                  taTSpecSM.AsString:=prFindKNM(quFCardIMESSURE.AsInteger,Km);
                  taTSpecKm.AsFloat:=Km;
                end;

                taTSpecName.AsString:=quFCardNAME.AsString;
                taTSpecNETTO.AsFloat:=0;
                taTSpecBRUTTO.AsFloat:=0;
                taTSpecKNB.AsFloat:=0;
                taTSpec.Post;
              end else
              begin
                Showmessage('����������� ������, ���������� ����������.');
              end;
              AEdit.SelectAll;
            end;
          end;
//        bAdd:=False;
        end;
      end;
    end else
      if ViewTSpec.Controller.FocusedColumn.Name='ViewTSpecIDCARD' then
        if fTestKey(Key)=False then
          if taTSpec.State in [dsEdit,dsInsert] then taTSpec.Post;
  end;
end;

procedure TfmTCard.acSaveExecute(Sender: TObject);
begin
  //���������� ��
  prSaveTForce;
end;

procedure  TfmTCard.prSaveTForce;
begin
  if ViewTSpec.OptionsData.Editing then //��������� ��������� ������
  with dmO do
  begin
    ViewTSpec.BeginUpdate;

    quTSpec.Active:=False;
    quTSpec.ParamByName('IDC').AsInteger:=quTCardsIDCARD.AsInteger;
    quTSpec.ParamByName('IDTC').AsInteger:=quTCardsID.AsInteger;
    quTSpec.Active:=True;

    while not quTSpec.Eof do quTSpec.Delete;

    taTSpec.First;
    while not taTSpec.eof do
    begin
      quTSpec.Append;
      quTSpecIDC.AsInteger:=quTCardsIDCARD.AsInteger;
      quTSpecIDT.AsInteger:=quTCardsID.AsInteger;
      quTSpecID.AsInteger:=taTSpecNum.AsInteger;
      quTSpecIDCARD.AsInteger:=taTSpecIdCard.AsInteger;
      quTSpecCURMESSURE.AsInteger:=taTSpecIdM.AsInteger;
      quTSpecNETTO.AsFloat:=taTSpecNetto.AsFloat;
      quTSpecBRUTTO.AsFloat:=taTSpecBrutto.AsFloat;
      //��� ����� ��������� km �.�. ��� ����� ����� ��� kbrutto
      quTSpecKNB.AsFloat:=taTSpecKm.AsFloat; //���� ���� ����� �� ���������
      quTSpec.Post;

      taTSpec.Next;
    end;

    StatusBar1.Panels[1].Text:='C��������� ��.';

    ViewTSpec.EndUpdate;
  end;
end;

procedure TfmTCard.taTSpecNettoChange(Sender: TField);
begin
  taTSpecBRUTTO.AsFloat:=taTSpecNETTO.AsFloat*(100+taTSpecKNB.AsFloat)/100;
end;

procedure TfmTCard.ViewTSpecSMPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
Var rK:Real;
    iM:Integer;
    Sm:String;
begin
  with dmO do
  begin
    if taTSpecIDM.AsInteger>0 then
    begin
      bAddSpec:=True;
      fmMessure.ShowModal;
      if fmMessure.ModalResult=mrOk then
      begin
        iM:=iMSel; iMSel:=0;
        Sm:=prFindKNM(iM,rK); //�������� ����� �������� � ����� �����

        taTSpec.Edit;
        taTSpecIdM.AsInteger:=iM;
        taTSpecKm.AsFloat:=rK;
        taTSpecSM.AsString:=Sm;
        taTSpec.Post;
      end;
    end;
  end;
end;

procedure TfmTCard.cxButton10Click(Sender: TObject);
begin
  prSaveTForce;
end;

end.
