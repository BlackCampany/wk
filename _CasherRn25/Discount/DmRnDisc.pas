unit DmRnDisc;

interface

uses
  SysUtils, Classes, DB, FIBDataSet, pFIBDataSet, FIBDatabase, pFIBDatabase,
  cxStyles, ImgList, Controls, FIBQuery, pFIBQuery, pFIBStoredProc,
  frexpimg, frRtfExp, frTXTExp, frXMLExl, frOLEExl, FR_E_HTML2, FR_E_HTM,
  FR_E_CSV, FR_E_RTF, FR_Class, FR_E_TXT, cxGridDBTableView;

type
  TdmCDisc = class(TDataModule)
    CasherRnDb: TpFIBDatabase;
    trSelect: TpFIBTransaction;
    trUpdate: TpFIBTransaction;
    trDel: TpFIBTransaction;
    quPer: TpFIBDataSet;
    quPerID: TFIBIntegerField;
    quPerID_PARENT: TFIBIntegerField;
    quPerNAME: TFIBStringField;
    quPerUVOLNEN: TFIBBooleanField;
    quPerCheck: TFIBStringField;
    quPerMODUL1: TFIBBooleanField;
    quPerMODUL2: TFIBBooleanField;
    quPerMODUL3: TFIBBooleanField;
    quPerMODUL4: TFIBBooleanField;
    quPerMODUL5: TFIBBooleanField;
    quPerMODUL6: TFIBBooleanField;
    dsPer: TDataSource;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    cxStyle14: TcxStyle;
    cxStyle15: TcxStyle;
    cxStyle16: TcxStyle;
    cxStyle17: TcxStyle;
    cxStyle18: TcxStyle;
    cxStyle19: TcxStyle;
    cxStyle20: TcxStyle;
    cxStyle21: TcxStyle;
    cxStyle22: TcxStyle;
    cxStyle23: TcxStyle;
    imState: TImageList;
    quCanDo: TpFIBDataSet;
    quCanDoID_PERSONAL: TFIBIntegerField;
    quCanDoNAME: TFIBStringField;
    quCanDoPREXEC: TFIBBooleanField;
    trCanDo: TpFIBTransaction;
    prAddRClassif: TpFIBStoredProc;
    prDelRClassif: TpFIBStoredProc;
    prEditRClassif: TpFIBStoredProc;
    cxStyle24: TcxStyle;
    cxStyle25: TcxStyle;
    prGetId: TpFIBStoredProc;
    frTextExport1: TfrTextExport;
    frRTFExport1: TfrRTFExport;
    frCSVExport1: TfrCSVExport;
    frHTMExport1: TfrHTMExport;
    frHTML2Export1: TfrHTML2Export;
    frOLEExcelExport1: TfrOLEExcelExport;
    frXMLExcelExport1: TfrXMLExcelExport;
    frTextAdvExport1: TfrTextAdvExport;
    frRtfAdvExport1: TfrRtfAdvExport;
    frJPEGExport1: TfrJPEGExport;
    quClassif: TpFIBDataSet;
    quDiscSel: TpFIBDataSet;
    quDiscSelBARCODE: TFIBStringField;
    quDiscSelNAME: TFIBStringField;
    quDiscSelPERCENT: TFIBFloatField;
    quDiscSelCLIENTINDEX: TFIBIntegerField;
    quDiscSelIACTIVE: TFIBSmallIntField;
    dsDiscSel: TDataSource;
    quMaxIdM: TpFIBDataSet;
    quMaxIdMMAXID: TFIBIntegerField;
    taClassif: TpFIBDataSet;
    taClassifTYPE_CLASSIF: TFIBIntegerField;
    taClassifID: TFIBIntegerField;
    taClassifIACTIVE: TFIBSmallIntField;
    taClassifID_PARENT: TFIBIntegerField;
    taClassifNAME: TFIBStringField;
    taClassifIEDIT: TFIBSmallIntField;
    taClassEdit: TpFIBDataSet;
    taClassEditTYPE_CLASSIF: TFIBIntegerField;
    taClassEditID: TFIBIntegerField;
    taClassEditIACTIVE: TFIBSmallIntField;
    taClassEditID_PARENT: TFIBIntegerField;
    taClassEditNAME: TFIBStringField;
    taClassEditIEDIT: TFIBSmallIntField;
    quFindChild: TpFIBDataSet;
    quFindChildCOUNTREC: TFIBIntegerField;
    quFindDC: TpFIBDataSet;
    quFindDCBARCODE: TFIBStringField;
    taTypePl_old: TpFIBDataSet;
    taTypePl_oldID: TFIBIntegerField;
    taTypePl_oldNAME: TFIBStringField;
    taTypePl_oldDATEFROM: TFIBDateField;
    taTypePl_oldDATETO: TFIBDateField;
    taTypePl_oldCOMMENT: TFIBStringField;
    dsTypePl_old: TDataSource;
    quMaxIdTPL_old: TpFIBDataSet;
    quMaxIdTPL_oldMAXID: TFIBIntegerField;
    quFindTPL_old: TpFIBDataSet;
    quFindTPL_oldCOUNTREC: TFIBIntegerField;
    dsTypePl1_old: TDataSource;
    quRep1: TpFIBDataSet;
    quRep1DISCONT: TFIBStringField;
    quRep1NAME: TFIBStringField;
    quRep1PERCENT: TFIBFloatField;
    quRep1SSUM: TFIBFloatField;
    quDiscDetail: TpFIBDataSet;
    quDiscDetailID_PERSONAL: TFIBIntegerField;
    quDiscDetailNUMTABLE: TFIBStringField;
    quDiscDetailQUESTS: TFIBIntegerField;
    quDiscDetailENDTIME: TFIBDateTimeField;
    quDiscDetailDISCONT: TFIBStringField;
    quDiscDetailNAME: TFIBStringField;
    quDiscDetailPERCENT: TFIBFloatField;
    quDiscDetailTABSUM: TFIBFloatField;
    quDiscDetailNAME1: TFIBStringField;
    dsDiscDetail: TDataSource;
    quPCDet: TpFIBDataSet;
    quPCDetID: TFIBIntegerField;
    quPCDetID_PERSONAL: TFIBIntegerField;
    quPCDetNUMTABLE: TFIBStringField;
    quPCDetQUESTS: TFIBIntegerField;
    quPCDetENDTIME: TFIBDateTimeField;
    quPCDetDISCONT: TFIBStringField;
    quPCDetCLINAME: TFIBStringField;
    quPCDetTABSUM: TFIBFloatField;
    quPCDetNAME: TFIBStringField;
    quPCDetSIFR: TFIBIntegerField;
    quPCDetPRICE: TFIBFloatField;
    quPCDetQUANTITY: TFIBFloatField;
    quPCDetDISCOUNTPROC: TFIBFloatField;
    quPCDetDISCOUNTSUM: TFIBFloatField;
    quPCDetSUMMA: TFIBFloatField;
    quPCDetNAME1: TFIBStringField;
    dsPCDet: TDataSource;
    quPCDetNAMETYPE: TFIBStringField;
    quPCDetCOMMENT: TFIBStringField;
    quPCDetDATETO: TFIBDateField;
    quDiscSelPHONE: TFIBStringField;
    quDiscSelBERTHDAY: TFIBDateField;
    quDiscSelCOMMENT: TFIBStringField;
    quPCDetDISCONT1: TFIBStringField;
    quPCDetDCNAME: TFIBStringField;
    quDiscDetailID: TFIBIntegerField;
    quPCCli: TpFIBDataSet;
    quPCCliDISCONT: TFIBStringField;
    quPCCliDCNAME: TFIBStringField;
    quPCCliSUM: TFIBFloatField;
    quPCCliSUM1: TFIBFloatField;
    quPCCliSITOG: TFIBFloatField;
    dsPCCli: TDataSource;
    dsRep1: TDataSource;
    quRep1SUMPOS: TFIBFloatField;
    quRep1ITSUM: TFIBFloatField;
    quDiscDetailITSUM: TFIBFloatField;
    quDiscDetailSUMPOS: TFIBFloatField;
    quDiscSelBARNUM: TIntegerField;
    quDiscSelDATECARD: TFIBDateField;
    quDiscSelPHONE_MOBILE: TFIBStringField;
    quDiscSelEMAIL: TFIBStringField;
    quDiscSelADRES: TFIBStringField;
    quDiscSelChildSmall: TFIBIntegerField;
    quDiscSelChildBig: TFIBIntegerField;
    quDiscSpisok: TpFIBDataSet;
    FIBStringField1: TFIBStringField;
    FIBStringField2: TFIBStringField;
    FIBFloatField1: TFIBFloatField;
    FIBIntegerField1: TFIBIntegerField;
    FIBSmallIntField1: TFIBSmallIntField;
    FIBStringField3: TFIBStringField;
    FIBDateField1: TFIBDateField;
    FIBStringField4: TFIBStringField;
    IntegerField1: TIntegerField;
    FIBDateField2: TFIBDateField;
    FIBStringField5: TFIBStringField;
    FIBStringField6: TFIBStringField;
    FIBStringField7: TFIBStringField;
    FIBIntegerField2: TFIBIntegerField;
    FIBIntegerField3: TFIBIntegerField;
    dsDiscSpisok: TDataSource;
    quDiscSelMMDD: TStringField;
    quDiscAll: TpFIBDataSet;
    quDiscAllBARCODE: TFIBStringField;
    quDiscAllNAME: TFIBStringField;
    quDiscAllPERCENT: TFIBFloatField;
    quDiscAllCLIENTINDEX: TFIBIntegerField;
    quDiscAllIACTIVE: TFIBSmallIntField;
    quDiscAllPHONE: TFIBStringField;
    quDiscAllBERTHDAY: TFIBDateField;
    quDiscAllCOMMENT: TFIBStringField;
    quDiscAllDATECARD: TFIBDateField;
    quDiscAllPHONE_MOBILE: TFIBStringField;
    quDiscAllEMAIL: TFIBStringField;
    taTypePl_oldCEHPIT: TFIBSmallIntField;
    quFindDCFind: TpFIBDataSet;
    quFindDCFindNAME: TFIBStringField;
    quFindDCFindCLIENTINDEX: TFIBIntegerField;
    quFindDCFindBARCODE: TFIBStringField;
    dsFindDCFind: TDataSource;
    quRep1PHONE: TFIBStringField;
    quRep1BERTHDAY: TFIBDateField;
    quRep1EMAIL: TFIBStringField;
    quRep1ADRES: TFIBStringField;
    quRep1MONTHDAY: TFIBSmallIntField;
    quDiskImport: TpFIBDataSet;
    quDiskImportBARCODE: TFIBStringField;
    quDiskImportNAME: TFIBStringField;
    quDiskImportPERCENT: TFIBFloatField;
    quDiskImportCLIENTINDEX: TFIBIntegerField;
    quDiskImportIACTIVE: TFIBSmallIntField;
    quDiskImportPHONE: TFIBStringField;
    quDiskImportBERTHDAY: TFIBDateField;
    quDiskImportCOMMENT: TFIBStringField;
    quDiskImportChildSmall: TFIBIntegerField;
    quDiskImportChildBig: TFIBIntegerField;
    quDiskImportDATECARD: TFIBDateField;
    quDiskImportPHONE_MOBILE: TFIBStringField;
    quDiskImportEMAIL: TFIBStringField;
    quDiskImportADRES: TFIBStringField;
    quDiskImportSUMMA: TFIBFloatField;
    quClassifImport: TpFIBDataSet;
    quClassifImportTYPE_CLASSIF: TFIBIntegerField;
    quClassifImportID: TFIBIntegerField;
    quClassifImportID_PARENT: TFIBIntegerField;
    quClassifImportNAME: TFIBStringField;
    prAddRDISCCARD: TpFIBStoredProc;
    dsPCard_old: TDataSource;
    taPCard_old: TpFIBDataSet;
    taPCard_oldBARCODE: TFIBStringField;
    taPCard_oldPLATTYPE: TFIBIntegerField;
    taPCard_oldIACTIVE: TFIBSmallIntField;
    taPCard_oldCLINAME: TFIBStringField;
    taPCard_oldNAME: TFIBStringField;
    taPCard_oldDATEFROM: TFIBDateField;
    taPCard_oldDATETO: TFIBDateField;
    taPCard_oldCOMMENT: TFIBStringField;
    taPCard_oldTYPEOPL: TFIBSmallIntField;
    taPCard_oldBALANS: TFIBFloatField;
    taPCard_oldDAYLIMIT: TFIBFloatField;
    taPCard_oldBALANCECARD: TFIBStringField;
    taPCard_oldVIDOPL: TStringField;
    taCategSaleSel: TpFIBDataSet;
    taCategSaleSelID: TFIBIntegerField;
    taCategSaleSelNAMECS: TFIBStringField;
    dsCategSaleSel: TDataSource;
    quCheckDC: TpFIBDataSet;
    quCheckDCDISCONT: TFIBStringField;
    quMenuTree: TpFIBDataSet;
    taDiscStopGr: TpFIBDataSet;
    taDiscStopGrID: TFIBIntegerField;
    taDiscStopGrDISCMAX: TFIBFloatField;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure quDiscSelCalcFields(DataSet: TDataSet);
    procedure taPCard_oldCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

Function CanDo(Name_F:String):Boolean;
function prGetDiscGr(IdGr:INteger):Real;

var
  dmCDisc: TdmCDisc;

implementation

uses Un1;

{$R *.dfm}


function prGetDiscGr(IdGr:INteger):Real;
begin
  Result:=100;
  with dmCDisc do
  begin
    taDiscStopGr.Active:=False;
    taDiscStopGr.ParamByName('IDGR').AsInteger:=IdGr;
    if taDiscStopGr.RecordCount>0 then Result:=100-taDiscStopGrDISCMAX.AsFloat;
    taDiscStopGr.Active:=False;
  end;
end;


procedure TdmCDisc.DataModuleCreate(Sender: TObject);
begin
  CasherRnDb.Connected:=False;
  CasherRnDb.DBName:=DBName;
  try
    CasherRnDb.Open;
  except
  end;
end;

procedure TdmCDisc.DataModuleDestroy(Sender: TObject);
begin
  try
    CasherRnDb.Close;
  except
  end;
end;

Function CanDo(Name_F:String):Boolean;
begin
  result:=True;
  with dmCDisc do
  begin
    quCanDo.Active:=False;
    quCanDo.ParamByName('Person_id').Value:=Person.Id;
    quCanDo.ParamByName('Name_F').Value:=Name_F;
    quCanDo.Active:=True;
    if quCanDoprExec.AsBoolean=True then Result:=False;
  end;
end;


procedure TdmCDisc.quDiscSelCalcFields(DataSet: TDataSet);
Var StrB:String;
begin
  StrB:=quDiscSelBARCODE.AsString;
  if length(StrB)>4 then delete(StrB,1,4);
  while pos('=',StrB)>0 do delete(StrB,1,pos('=',StrB));
  quDiscSelBARNUM.AsInteger:=StrToINtDef(StrB,0);
  quDiscSelMMDD.AsString:=FormatDateTime('mm.dd',quDiscSelBERTHDAY.AsDateTime);
end;

procedure TdmCDisc.taPCard_oldCalcFields(DataSet: TDataSet);
begin
{
 if taPCardTYPEOPL.AsInteger = 1 then
  taPCardVIDOPL.AsString:='���������� �����'
 else if taPCardTYPEOPL.AsInteger = 2 then
  taPCardVIDOPL.AsString:='��������� �����'
 else if taPCardTYPEOPL.AsInteger = 3 then
  taPCardVIDOPL.AsString:='���������/������ �����'
 else
  taPCardVIDOPL.AsString:='������!!!!!';
}
end;

end.
