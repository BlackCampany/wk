unit Categories;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Placemnt, ComCtrls, SpeedBar, ExtCtrls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  RXSplit, cxCurrencyEdit, cxImageComboBox, cxTextEdit, ActnList,
  XPStyleActnCtrls, ActnMan;

type
  TfmCateg = class(TForm)
    FormPlacement1: TFormPlacement;
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    RxSplitter1: TRxSplitter;
    ViewCateg: TcxGridDBTableView;
    LevelCateg: TcxGridLevel;
    GridCateg: TcxGrid;
    ViewCategSIFR: TcxGridDBColumn;
    ViewCategNAME: TcxGridDBColumn;
    ViewCategTAX_GROUP: TcxGridDBColumn;
    ViewCategIACTIVE: TcxGridDBColumn;
    ViewCategIEDIT: TcxGridDBColumn;
    am2: TActionManager;
    acAdd: TAction;
    acEdit: TAction;
    acDel: TAction;
    acExit: TAction;
    acPrint: TAction;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    Timer1: TTimer;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure acAddExecute(Sender: TObject);
    procedure acEditExecute(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmCateg: TfmCateg;

implementation

uses Un1, dmRnEdit, AddCateg;

{$R *.dfm}

procedure TfmCateg.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  GridCateg.Align:=AlClient;
  ViewCateg.RestoreFromIniFile(CurDir+GridIni);
  delay(10);
//  dmC.taSpecAllSel.Active:=False;
//  ViewLog.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmCateg.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewCateg.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmCateg.FormShow(Sender: TObject);
begin
  with dmC do
  begin
    ViewCateg.BeginUpdate;
    quCateg.Active:=False;
    quCateg.Active:=True;
    ViewCateg.EndUpdate;
  end;
end;

procedure TfmCateg.acAddExecute(Sender: TObject);
Var Id:Integer;
begin
  //���������� ���������
  if not CanDo('prAddCateg') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  with dmC do
  begin
    fmAddCateg:=TFmAddCateg.create(Application);
    fmAddCateg.Caption:='���������� ��������� ����.';
    fmAddCateg.cxTextEdit1.Text:='';
    fmAddCateg.ShowModal;
    if fmAddCateg.ModalResult=mrOk then
    begin
      try
//        showmessage('�������� ���������');
        quMaxIdCateg.Active:=False;
        quMaxIdCateg.Active:=True;
        Id:=quMaxIdCategMAXID.AsInteger+1;
        quMaxIdCateg.Active:=False;

        trUpdate.StartTransaction;

        quCateg.Append;
        quCategSIFR.AsInteger:=Id;
        quCategNAME.AsString:=fmAddCateg.cxTextEdit1.Text;
        quCategTAX_GROUP.AsInteger:=0;
        quCategIACTIVE.AsInteger:=1;
        quCategIEDIT.AsInteger:=0;
        quCateg.Post;

        trUpdate.Commit;
      except
      end;
    end;
    fmAddCateg.Release;
  end;
end;

procedure TfmCateg.acEditExecute(Sender: TObject);
begin
  //������������� ���������
  if not CanDo('prEditCateg') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  with dmC do
  begin
    fmAddCateg:=TFmAddCateg.create(Application);
    fmAddCateg.Caption:='�������������� ��������� - '+quCategNAME.AsString;
    fmAddCateg.cxTextEdit1.Text:=quCategNAME.AsString;
    fmAddCateg.ShowModal;
    if fmAddCateg.ModalResult=mrOk then
    begin
      try
//        showmessage('���������� ���������');
        trUpdate.StartTransaction;

        quCateg.Edit;
        quCategNAME.AsString:=fmAddCateg.cxTextEdit1.Text;
        quCateg.Post;

        trUpdate.Commit;
        quCateg.Refresh;

      except
      end;
    end;
    fmAddCateg.Release;
  end;
end;

procedure TfmCateg.acDelExecute(Sender: TObject);
begin
  //������� ���������
  if not CanDo('prDelCateg') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  if MessageDlg('�� ������������� ������ ������� ��������� - '+dmC.quCategNAME.AsString+' ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
    try
      with dmC do
      begin
        trUpdate.StartTransaction;
        quCateg.Delete;
        trUpdate.Commit;
      end;
    except
    end;
  end;
end;

procedure TfmCateg.acExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmCateg.acPrintExecute(Sender: TObject);
begin
  //������
  if not CanDo('prPrintCateg') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
end;

procedure TfmCateg.Timer1Timer(Sender: TObject);
begin
  if bClear1=True then begin StatusBar1.Panels[0].Text:=''; bClear1:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClear1:=True;
end;

end.
