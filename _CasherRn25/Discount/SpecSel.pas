unit SpecSel;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxEdit, DB, cxDBData, ExtCtrls, SpeedBar, cxGridLevel, cxClasses,
  cxControls, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, Placemnt, cxDataStorage, cxImageComboBox;

type
  TfmSpecSel = class(TForm)
    StatusBar1: TStatusBar;
    ViewSpecSel: TcxGridDBTableView;
    LevelSpecSel: TcxGridLevel;
    GridSpecSel: TcxGrid;
    SpeedBar1: TSpeedBar;
    FormPlacement1: TFormPlacement;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    ViewSpecSelID: TcxGridDBColumn;
    ViewSpecSelSIFR: TcxGridDBColumn;
    ViewSpecSelPRICE: TcxGridDBColumn;
    ViewSpecSelQUANTITY: TcxGridDBColumn;
    ViewSpecSelDISCOUNTPROC: TcxGridDBColumn;
    ViewSpecSelDISCOUNTSUM: TcxGridDBColumn;
    ViewSpecSelSUMMA: TcxGridDBColumn;
    ViewSpecSelITYPE: TcxGridDBColumn;
    ViewSpecSelNAMEMM: TcxGridDBColumn;
    ViewSpecSelNAMEMD: TcxGridDBColumn;
    ViewSpecSelNAME: TcxGridDBColumn;
    ViewSpecSelSTREAM: TcxGridDBColumn;
    ViewSpecSelNAMESTREAM: TcxGridDBColumn;
    SpeedItem2: TSpeedItem;
    LevelSpecCash: TcxGridLevel;
    ViewSpecCash: TcxGridDBTableView;
    SpeedItem3: TSpeedItem;
    ViewSpecCashCASHNUM: TcxGridDBColumn;
    ViewSpecCashZNUM: TcxGridDBColumn;
    ViewSpecCashCHECKNUM: TcxGridDBColumn;
    ViewSpecCashTAB_ID: TcxGridDBColumn;
    ViewSpecCashTABSUM: TcxGridDBColumn;
    ViewSpecCashCLIENTSUM: TcxGridDBColumn;
    ViewSpecCashCASHERID: TcxGridDBColumn;
    ViewSpecCashCASHER: TcxGridDBColumn;
    ViewSpecCashWAITERID: TcxGridDBColumn;
    ViewSpecCashWAITER: TcxGridDBColumn;
    ViewSpecCashCHDATE: TcxGridDBColumn;
    ViewSpecCashPAYTYPE: TcxGridDBColumn;
    ViewSpecCashPAYID: TcxGridDBColumn;
    ViewSpecCashPAYBAR: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SpeedItem1Click(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmSpecSel: TfmSpecSel;

implementation

uses Un1, Dm;

{$R *.dfm}

procedure TfmSpecSel.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  ViewSpecSel.RestoreFromIniFile(CurDir+GridIni);
end;

procedure TfmSpecSel.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  dmC.taSpecAllSel.Active:=False;
  ViewSpecSel.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmSpecSel.SpeedItem1Click(Sender: TObject);
begin
  close;
end;

procedure TfmSpecSel.SpeedItem2Click(Sender: TObject);
begin
//������� � ������
//  with dmC do prExportExel(ViewSpecSel,dsSpecAllSel,taSpecAllSel);
  prNExportExel4(ViewSpecSel)
end;

procedure TfmSpecSel.SpeedItem3Click(Sender: TObject);
begin
   //��������� ����
  if LevelSpecSel.Visible=True then
  begin
    LevelSpecSel.Visible:=False;
//    SpeedItem2.Enabled:=False;
    LevelSpecCash.Visible:=True;
  end else
  begin
    LevelSpecSel.Visible:=True;
    LevelSpecCash.Visible:=False;
  end;

end;

end.
