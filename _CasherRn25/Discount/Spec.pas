unit Spec;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Placemnt, cxGridBandedTableView, cxGridDBBandedTableView, StdCtrls,
  cxLookAndFeelPainters, cxButtons, ActnList, XPStyleActnCtrls, ActnMan,
  DBClient, ExtCtrls, VaClasses, VaComm, VaControls, VaDisplay, VaSystem,
  dxfBackGround, cxContainer, cxTextEdit, Grids, DBGrids, cxDataStorage,
  cxCurrencyEdit, cxCalc, cxImageComboBox, Menus, FR_Class, FR_DSet,
  FR_DBSet, cxMaskEdit, cxButtonEdit, cxSpinEdit;

type
  TfmSpec = class(TForm)
    LevelSpec: TcxGridLevel;
    GridSpec: TcxGrid;
    FormPlacement1: TFormPlacement;
    ViewSpec: TcxGridDBBandedTableView;
    ViewSpecPRICE: TcxGridDBBandedColumn;
    ViewSpecQUANTITY: TcxGridDBBandedColumn;
    ViewSpecDPROC: TcxGridDBBandedColumn;
    ViewSpecDSUM: TcxGridDBBandedColumn;
    ViewSpecSUMMA: TcxGridDBBandedColumn;
    ViewSpecISTATUS: TcxGridDBBandedColumn;
    ViewSpecNAME: TcxGridDBBandedColumn;
    ViewSpecCODE: TcxGridDBBandedColumn;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Button1: TcxButton;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Button5: TcxButton;
    cxButton3: TcxButton;
    amSpec: TActionManager;
    acMenu: TAction;
    acChangeQuantity: TAction;
    acDelPos: TAction;
    cxButton4: TcxButton;
    acMovePos: TAction;
    acPrePrint: TAction;
    Label17: TLabel;
    ViewMod: TcxGridDBTableView;
    LevelMod: TcxGridLevel;
    GridMod: TcxGrid;
    cxButton5: TcxButton;
    acModify: TAction;
    ViewModName: TcxGridDBColumn;
    ViewModQuantity: TcxGridDBColumn;
    cxButton6: TcxButton;
    Panel1: TPanel;
    Label18: TLabel;
    Timer1: TTimer;
    Action1: TAction;
    Action2: TAction;
    Action5: TAction;
    Action7: TAction;
    Action8: TAction;
    Action9: TAction;
    Action10: TAction;
    acAddMod: TAction;
    acDelMod: TAction;
    Button2: TButton;
    dxfBackGround1: TdxfBackGround;
    cxButton7: TcxButton;
    acKod: TAction;
    Edit1: TcxTextEdit;
    acDiscount: TAction;
    acExitN: TAction;
    cxButton8: TcxButton;
    cxButton9: TcxButton;
    frRepSp: TfrReport;
    frquCheck: TfrDBDataSet;
    cxButton10: TcxButton;
    Label4: TLabel;
    BEdit1: TcxButtonEdit;
    SEdit1: TcxSpinEdit;
    cxButton11: TcxButton;
    cxButton12: TcxButton;
    cxButton13: TcxButton;
    acNumTab: TAction;
    procedure FormCreate(Sender: TObject);
    procedure acMenuExecute(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure acChangeQuantityExecute(Sender: TObject);
    procedure acDelPosExecute(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure acMovePosExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure acPrePrintExecute(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure acModifyExecute(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure Action1Execute(Sender: TObject);
    procedure Action2Execute(Sender: TObject);
    procedure acExitNExecute(Sender: TObject);
    procedure Action4Execute(Sender: TObject);
    procedure Action5Execute(Sender: TObject);
    procedure Action6Execute(Sender: TObject);
    procedure Action7Execute(Sender: TObject);
    procedure Action8Execute(Sender: TObject);
    procedure Action9Execute(Sender: TObject);
    procedure Action10Execute(Sender: TObject);
    procedure acAddModExecute(Sender: TObject);
    procedure acDelModExecute(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure acKodExecute(Sender: TObject);
    procedure cxButton7Click(Sender: TObject);
    procedure acDiscountExecute(Sender: TObject);
    procedure ViewSpecFocusedRecordChanged(Sender: TcxCustomGridTableView;
      APrevFocusedRecord, AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure cxButton8Click(Sender: TObject);
    procedure cxButton9Click(Sender: TObject);
    procedure cxButton10Click(Sender: TObject);
    procedure cxButton11Click(Sender: TObject);
    procedure cxButton13Click(Sender: TObject);
    procedure acNumTabExecute(Sender: TObject);
    procedure BEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure BEdit1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure BEdit1Click(Sender: TObject);
    procedure cxButton12Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }

    Procedure SaveSpecN;
    Procedure FormServChN;
    Procedure CreateViewPers1;
    Function TestSave:Boolean;
    Procedure SaveStatusTab(TabId,TabiStatus,iQuests:Integer; sNumTab:String);
    Procedure SetStatus(Var S:String);

  end;

Function prFindKey(iC:Integer;Var sName,sCode:String;Var rPrice:Real;Var Sifr,iType,iLinkM,iLimit,iStream:Integer):Boolean;

var
  fmSpec: TfmSpec;
  bSaveSpec:Boolean = False;

implementation

uses Un1, Dm, Menu, Calc, MessQ, MainCashRn, MessDel, PosMove, Modif,
  UnCash, u2fdk, Discont, fmDiscountShape, uDB1, Attention, CashEnd;

{$R *.dfm}

Procedure TfmSpec.SetStatus(Var S:String);
begin
  S:='';
  if Tab.iStatus=0 then
  begin
    S:='� ��������������';
    Button1.Enabled:=True;
    cxButton1.Enabled:=True;
    cxButton4.Enabled:=True;
    cxButton2.Enabled:=True;
    cxButton5.Enabled:=True;
    cxButton6.Enabled:=True;
    cxButton7.Enabled:=True;
    cxButton10.Enabled:=True;
    cxButton11.Enabled:=True;
    cxButton13.Enabled:=True;
    bEdit1.Properties.ReadOnly:=False;
    bEdit1.Enabled:=True;
    sEdit1.Properties.ReadOnly:=False;
  end;
  if Tab.iStatus=1 then
  begin
    S:='�������';
    Button1.Enabled:=False;
    cxButton1.Enabled:=False;
    cxButton4.Enabled:=False;
    cxButton2.Enabled:=False;
    cxButton5.Enabled:=False;
    cxButton6.Enabled:=False;
    cxButton7.Enabled:=False;
    cxButton10.Enabled:=False;
    cxButton11.Enabled:=False;
    cxButton13.Enabled:=False;
    bEdit1.Properties.ReadOnly:=True;
    bEdit1.Enabled:=False;
    sEdit1.Properties.ReadOnly:=True;
  end;
end;



Procedure TfmSpec.SaveStatusTab(TabId,TabiStatus,iQuests:Integer; sNumTab:String);
begin
  with dmC do
  begin
//      trUpdate.StartTransaction;
      fmMainCashRn.TabView.BeginUpdate;
      if quTabs.Locate('ID',TabId,[]) then
      begin
        quTabs.Edit;
        quTabsISTATUS.AsInteger:=TabiStatus;
        quTabsNUMTABLE.AsString:=sNumTab;
        quTabsQUESTS.AsInteger:=iQuests;
        quTabsTABSUM.AsFloat:=Tab.Summa;
        quTabs.Post;
//      trUpdate.Commit;
        quTabs.Locate('ID',TabId,[]);
        quTabs.Refresh;
      end;  
      fmMainCashRn.TabView.EndUpdate;

      Tab.NumTable:=sNumTab;
      Tab.Quests:=iQuests;

{

    if quTabs.Locate('ID',TabId,[]) then
    begin
//      TabView.BeginUpdate;
      trUpdate.StartTransaction;
      quTabs.Edit;
      quTabsISTATUS.AsInteger:=TabiStatus;
      quTabsNUMTABLE.AsString:=sNumTab;
      quTabsQUESTS.AsInteger:=iQuests;
      quTabs.Post;
      trUpdate.Commit;
//      TabView.EndUpdate;
      quTabs.Locate('ID',TabId,[]);
      quTabs.Refresh;
      Tab.NumTable:=sNumTab;
      Tab.Quests:=iQuests;
    end;}
  end;
end;


Function TfmSpec.TestSave:Boolean;
Var rSum:Real;
begin
  Result:=True;
  with dmC do
  begin
    quTestSave.Active:=False;
    quTestSave.ParamByName('IDTAB').AsInteger:=Tab.Id;
    quTestSave.Active:=True;

    rSum:= quTestSaveTABSUM.AsFloat;
    quTestSave.Active:=False;
    if rSum<>Tab.Summa then
    begin
      Result:=False;
      prWriteLog('---TestSaveBad;'+IntToStr(Tab.Id)+';'+Tab.NumTable+'; Spec-'+FloatToStr(rSum)+';CurSpec-'+FloatToStr(Tab.Summa)+';');
    end else
    begin
      prWriteLog('---TestSaveGood;'+IntToStr(Tab.Id)+';'+Tab.NumTable+'; Spec-'+FloatToStr(rSum)+';CurSpec-'+FloatToStr(Tab.Summa)+';');
    end;
  end;
end;

Procedure TfmSpec.CreateViewPers1;
begin
  bChangeView:=True;
end;


Procedure TfmSpec.FormServChN;
begin
    //�������� ���� ��������
  with dmC do
  begin
    taServP.Active:=False;
    taServP.CreateDataSet;

    quCurModAll.Active:=False;
    quCurModAll.ParamByName('IDT').AsInteger:=Tab.Id;
    quCurModAll.Active:=True;

    //���� ������� ��� ����������

    prWriteLog('--SaveSpec;'+IntToStr(Tab.Id)+';'+Tab.NumTable+';'+Person.Name+';'+IntToStr(Tab.Id_Personal)+';');

    quCurSpec.First;
    while not quCurSpec.Eof do
    begin
      if quCurSpecIStatus.AsInteger=0 then
      begin
        prWriteLog('-----Addpos;'+IntToStr(quCurSpecID_TAB.AsInteger)+';'+IntToStr(quCurSpecID.AsInteger)+';'+IntTostr(quCurSpecID_PERSONAL.AsInteger)+';'+quCurSpecNAME.AsString+';'+IntToStr(quCurSpecSIFR.AsInteger)+';'+FloatToStr(quCurSpecSUMMA.AsFloat)+';');
      end;
      quCurSpec.Next;
    end;
    prWriteLog('--EndSpec;'+IntToStr(Tab.Id)+';'+Tab.NumTable+';'+Person.Name+';'+IntToStr(Tab.Id_Personal)+';');

    //��������
    quCurSpec.First;
    while not quCurSpec.Eof do
    begin
      if quCurSpecIStatus.AsInteger=0 then
      begin
        //�������� ��� ������ �����
        taServP.Append;
        taServPName.AsString:=quCurSpecName.AsString;
        taServPCode.AsString:=quCurSpecCode.AsString;
        taServPQuant.AsFloat:=quCurSpecQuantity.AsFloat;
        taServPStream.AsInteger:=quCurSpecStream.AsInteger;
        taServPiType.AsInteger:=0; //�����
        taServP.Post;

        //��������� ������ �� ������� �� ������
        while (quCurModAllID_POS.AsInteger<quCurSpecID.AsInteger) and (quCurModAll.Eof=False) do quCurModAll.Next;
        while (quCurModAllID_POS.AsInteger=quCurSpecID.AsInteger) and (quCurModAll.Eof=False) do
        begin
          taServP.Append;
          taServPName.AsString:=quCurModAllNAME.AsString;
          taServPCode.AsString:='';
          taServPQuant.AsFloat:=0;
          taServPStream.AsInteger:=quCurSpecStream.AsInteger;
          taServPiType.AsInteger:=1; //�����������
          taServP.Post;

          quCurModAll.Next;
        end;
      end;
      quCurSpec.Next;
    end;
    //�������� �������
    if Operation=0 then PrintServCh('�����');// ���� ������� (1) - �� ������� ������ �� ����
    taServP.Active:=False;
    quCurModAll.Active:=False;
  end;
end;


Procedure TfmSpec.SaveSpecN; //
begin
  //�������� ���� ��������
  with dmC do
  begin
    FormServChN;

    //������� ������ ������ ������������
    quDelSpec.ParamByName('IDTAB').AsInteger:=Tab.Id;
    quDelSpec.ExecQuery;

   //��������
    prSaveTab.ParamByName('ID_TAB').AsInteger:=Tab.Id;
    prSaveTab.ParamByName('ID_PERSONAL').AsInteger:=Tab.Id_Personal;
    prSaveTab.ParamByName('NUMTABLE').AsString:=Tab.NumTable;
    prSaveTab.ParamByName('QUESTS').AsInteger:=Tab.Quests;
    prSaveTab.ParamByName('TABSUM').AsDouble:=Tab.Summa;
    prSaveTab.ParamByName('BEGTIME').AsDateTime:=Tab.OpenTime;
    prSaveTab.ParamByName('ISTATUS').AsInteger:=Tab.iStatus;
    prSaveTab.ParamByName('DBAR').AsString:=Tab.DBar;
    prSaveTab.ParamByName('ID_STATION').AsInteger:=CommonSet.Station;
    prSaveTab.ExecProc;

    //�������� ������ ����
    quSetStatus.ParamByName('ID_TAB').AsInteger:=Tab.Id;
    quSetStatus.ParamByName('ID_STATION').AsInteger:=CommonSet.Station;
    quSetStatus.ExecQuery;

    quCurSpec.FullRefresh;

  end;
end;

Function prFindKey(iC:Integer;Var sName,sCode:String;Var rPrice:Real;Var Sifr,iType,iLinkM,iLimit,iStream:Integer):Boolean;
begin
  result:=False;
  if CommonSet.bTouch then exit; //��� ����� ������� ������ �� ��������� ���� ��� - ���������.
  with dmC do
  begin
    taCF_All.Active:=False;
    taCF_All.Active:=True;
    if taCF_All.locate('KEY_CODE',iC,[]) then
    begin
      case taCF_AllKEY_STATUS.AsInteger of
        0:begin //��������
          end;
        1:begin //������ �������
            Sifr:=taCF_AllID_CLASSIF.AsInteger-10000;
            iType:=2;
            Result:=True;
          end;
        2:begin //������
            quMenuSelKey.Active:=False;
            quMenuSelKey.Active:=True;
            if quMenuSelKey.Locate('SIFR',taCF_AllSIFR.AsInteger,[]) then
            begin
              Sifr:= quMenuSelKeySIFR.AsInteger;
              sName:=quMenuSelKeyNAME.AsString;
              rPrice:=quMenuSelKeyPRICE.AsFloat;
              sCode:=quMenuSelKeyCODE.AsString;
              iLinkM:=quMenuSelKeyLINK.AsInteger;
              iLimit:=RoundEx(quMenuSelKeyLIMITPRICE.AsFloat);
              iStream:=quMenuSelKeySTREAM.AsInteger;
              iType:=1;
              Result:=True;
            end;
            quMenuSelKey.Active:=False;
          end;
        3:begin //������
          end;
        else
        end;
    end;
    taCF_All.Active:=False;
  end;
end;

procedure TfmSpec.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  if CommonSet.CashNum<=0 then
  begin
    cxButton12.Visible:=False;
    cxButton12.Enabled:=False;
  end;
end;

procedure TfmSpec.acMenuExecute(Sender: TObject);
//Var rQ:Real;
begin
  with dmC do
  begin
    bBludoSel:=False;
    fmMenuClass.ShowModal;
  end;
end;

procedure TfmSpec.Button1Click(Sender: TObject);
begin
  if Tab.iStatus>0 then exit;
  prWriteLog('---Menu;'+IntToStr(Tab.Id)+';'+Tab.NumTable+';'+Person.Name+';'+IntToStr(Tab.Id_Personal)+';');
  bSaveSpec:=False; //�� �������� ������� ��������
  acMenu.Execute;
  GridSpec.SetFocus;
end;

procedure TfmSpec.acChangeQuantityExecute(Sender: TObject);
Var rDif:Real;
    rQ:Real;
begin
  if Tab.iStatus>0 then
  begin
    Label18.Caption:='����� � ������� ��������. �������������� ���������.';
    exit;
  end;
  with dmC do
  begin
    bSaveSpec:=False; //�� �������� ������� ��������
    if quCurSpec.Eof then exit;
    prWriteLog('---Quantity;'+quCurSpecSifr.AsString+';'+quCurSpecName.AsString+';'+quCurSpecQuantity.AsString+';'+Person.Name+';'+IntToStr(Tab.Id_Personal)+';');
    bAddPosFromSpec:=True;
    fmCalc.CalcEdit1.EditValue:=quCurSpecQuantity.AsFloat;
    fmCalc.ShowModal;
    if fmCalc.ModalResult=mrOk then
    begin
      rQ:=RoundEx(fmCalc.CalcEdit1.EditValue*1000)/1000;

      if quCurSpecIStatus.AsInteger=0 then
      begin
        CalcDiscontN(quCurSpecSifr.AsInteger,quCurSpecId.AsInteger,-1,quCurSpecSTREAM.AsInteger,quCurSpecPrice.AsFloat,rQ,0,Tab.DBar,Now,Check.DProc,Check.DSum);

        quCurSpec.Edit;
        quCurSpecQuantity.AsFloat:=rQ;
        quCurSpecDProc.AsFloat:=Check.DProc;
        quCurSpecDSum.AsFloat:=Check.DSum;
        quCurSpecSumma.AsFloat:=RoundEx(quCurSpecPrice.AsFloat*rQ*100)/100-Check.DSum;
        quCurSpec.Post;
      end
      else
      begin
        rDif:=rQ-quCurSpecQuantity.AsFloat;
        if rDif>0 then
        begin
          Check.Id_personal:=quCurSpecId_Personal.AsInteger;
          Check.NumTable:=quCurSpecNumTable.AsString;
          Check.Sifr:=quCurSpecSifr.AsInteger;
          Check.Price:=quCurSpecPrice.AsFloat;
          Check.Quantity:=quCurSpecQuantity.AsFloat;
          Check.Summa:=quCurSpecSumma.AsFloat;
          Check.DProc:=quCurSpecDProc.AsFloat;
          Check.DSum:=quCurSpecDSum.AsFloat;
          Check.Name:=quCurSpecName.AsString;
          Check.Code:=quCurSpecCode.AsString;
          Check.LimitM:=quCurSpecLimitM.AsInteger;
          Check.LinkM:=quCurSpecLinkM.AsInteger;
          Check.Stream:=quCurSpecStream.AsInteger;

          CalcDiscontN(Check.Sifr,Check.Max+1,-1,Check.Stream,Check.Price,rDif,0,Tab.DBar,Now,Check.DProc,Check.DSum);

          quCurSpec.Append;
          quCurSpecSTATION.AsInteger:=CommonSet.Station;
          quCurSpecID_TAB.AsInteger:=Tab.Id;
          quCurSpecId_Personal.AsInteger:=Check.Id_personal;
          quCurSpecNumTable.AsString:=Check.NumTable;
          quCurSpecId.AsInteger:=Check.Max+1;
          quCurSpecSifr.AsInteger:=Check.Sifr;
          quCurSpecPrice.AsFloat:=Check.Price;
          quCurSpecQuantity.AsFloat:=rDif;
          quCurSpecSumma.AsFloat:=roundEx(rDif*Check.Price*100)/100-Check.DSum;
          quCurSpecDProc.AsFloat:=Check.DProc;
          quCurSpecDSum.AsFloat:=Check.DSum;
          quCurSpecIStatus.AsInteger:=0;
          quCurSpecName.AsString:=Check.Name;
          quCurSpecCode.AsString:=Check.Code;
          quCurSpecLinkM.AsInteger:=Check.LinkM;
          quCurSpecLimitM.AsInteger:=Check.LimitM;
          quCurSpecStream.AsInteger:=Check.Stream;
          quCurSpec.Post;
          inc(Check.Max);

          if quCurSpecLinkM.AsInteger>0 then
          begin //������������
            acModify.Execute;
          end;
        end;
      end;
    end;
    bAddPosFromSpec:=False;
  end;
  GridSpec.SetFocus;
end;


procedure TfmSpec.acDelPosExecute(Sender: TObject);
  //������� �������
Var StrWk:String;
    IdHead, iSklad, iNum:Integer;
    rSum,rSumD:Real;
    bDel:Boolean;
    rQ:Real;
begin
  with dmC do
  begin
    if quCurSpec.Eof then exit;
    if quCurSpecIStatus.AsInteger=0 then //��� �� �������� - ��� �����������
    begin
      quCurMod.First;
      while not quCurMod.Eof do quCurMod.Delete;

      quCurSpec.Delete;
      exit;
    end;

    if not CanDo('prPosDel') then begin Label18.Caption:='��� ����.'; exit; end;
    fmMessDel:=TfmMessDel.Create(Application);
    if not CanDo('prPosDelNoSkl') then fmMessDel.cxButton1.Enabled:=False;
    if not CanDo('prPosDelWithSkl') then fmMessDel.cxButton2.Enabled:=False;

    fmMessDel.Label1.Caption:=quCurSpecName.AsString+' '+quCurSpecCode.AsString;
    Str(quCurSpecPrice.AsCurrency:10:2,StrWk);
//    fmMessDel.Label2.Caption:='����� '+StrWk+'  ���-�� '; //+quCurSpecQuantity.asstring;
    fmMessDel.Label2.Caption:=  '���� '+StrWk+'  ���-�� '; //+quCurSpecQuantity.asstring;
    fmMessDel.CalcEdit1.Visible:=True;
    fmMessDel.CalcEdit1.EditValue:=quCurSpecQuantity.AsFloat;

    fmMessDel.ShowModal;

    iSklad:=0;
    bDel:=False;
    rQ:=RoundEx(fmMessDel.CalcEdit1.EditValue*1000)/1000;

    if (fmMessDel.ModalResult=mrYes)and(rQ>0) then
    begin
      iSklad:=0;
      bDel:=True;
      //�� ��������� ������ ��� ����������� �� �����
      FormLog('DelPos0',IntToStr(Person.Id)+' '+quCurSpecId.AsString+' '+quCurSpecSifr.AsString+' '+StrWk);
    end;
    if (fmMessDel.ModalResult=mrNo)and(rQ>0) then
    begin
      iSklad:=1;
      bDel:=True;
      //��� �������� ����� ����������� ������ ��� �� ������
      FormLog('DelPos1',IntToStr(Person.Id)+' '+quCurSpecId.AsString+' '+quCurSpecSifr.AsString+' '+StrWk);
    end;
      //����� ������ ������������.
    if bDel then
    begin
      idHead:=GetId('TH');

      //���� ���������
      taTabAll.Active:=False;
      taTabAll.ParamByName('TID').AsInteger:=idHead;
      taTabAll.Active:=True;

      trUpdTab.StartTransaction;
      taTabAll.Append;
      taTabAllID.AsInteger:=IdHead;
      taTabAllID_PERSONAL.AsInteger:=Tab.Id_Personal;
      taTabAllNUMTABLE.AsString:=Tab.NumTable;
      taTabAllQUESTS.AsInteger:=Tab.Quests;
      taTabAllTABSUM.AsFloat:=RoundEx(quCurSpecSUMMA.AsFloat*rQ/quCurSpecQuantity.AsFloat*100)/100;
      taTabAllBEGTIME.AsDateTime:=Tab.OpenTime;
      taTabAllENDTIME.AsDateTime:=now;
      taTabAllDISCONT.AsString:=Tab.DBar;
      taTabAllOPERTYPE.AsString:='Del';
      taTabAllCHECKNUM.AsInteger:=0;
      taTabAllSKLAD.AsInteger:=iSklad; //�� ��������� ��� ���
      taTabAllSTATION.AsInteger:=CommonSet.Station;
      taTabAll.Post;
      trUpdTab.Commit;

      taTabAll.Active:=False;

      //���� ������������ 1-�������
      taSpecAll.Active:=False;
      taSpecAll.ParamByName('TID').AsInteger:=idHead;
      taSpecAll.Active:=True;

      trUpdTab.StartTransaction;
      iNum:=1;
      taSpecAll.Append;
      taSpecAllID_TAB.AsInteger:=IdHead;
      taSpecAllID.AsInteger:=iNum;
      taSpecAllID_PERSONAL.AsInteger:=Tab.Id_Personal;
      taSpecAllNUMTABLE.AsString:=Tab.NumTable;
      taSpecAllSIFR.AsInteger:=quCurSpecSifr.AsInteger;
      taSpecAllPRICE.AsFloat:=quCurSpecPrice.AsFloat;
      taSpecAllQUANTITY.AsFloat:=rQ;
      taSpecAllDISCOUNTPROC.AsFloat:=quCurSpecDProc.AsFloat;
      taSpecAllDISCOUNTSUM.AsFloat:=RoundEx(quCurSpecDSum.AsFloat*rQ/quCurSpecQuantity.AsFloat*100)/100;
      taSpecAllSUMMA.AsFloat:=RoundEx(quCurSpecSUMMA.AsFloat*rQ/quCurSpecQuantity.AsFloat*100)/100;
      taSpecAllISTATUS.AsInteger:=1;
      taSpecAllITYPE.AsInteger:=0;//�����
      taSpecAll.Post;
      inc(iNum);

      ViewSpec.BeginUpdate;
      ViewMod.BeginUpdate;

      quCurMod.First;
      while not quCurMod.Eof do
      begin
        taSpecAll.Append;
        taSpecAllID_TAB.AsInteger:=IdHead;
        taSpecAllID.AsInteger:=iNum;
        taSpecAllID_PERSONAL.AsInteger:=Tab.Id_Personal;
        taSpecAllNUMTABLE.AsString:=Tab.NumTable;
        taSpecAllSIFR.AsInteger:=quCurModSifr.AsInteger;
        taSpecAllPRICE.AsFloat:=0;
        taSpecAllQUANTITY.AsFloat:=rQ;
        taSpecAllDISCOUNTPROC.AsFloat:=0;
        taSpecAllDISCOUNTSUM.AsFloat:=0;
        taSpecAllSUMMA.AsFloat:=0;
        taSpecAllISTATUS.AsInteger:=1;
        taSpecAllITYPE.AsInteger:=1;//�����������
        taSpecAll.Post;
        inc(iNum);

        quCurMod.Next;
      end;
      trUpdTab.Commit;
      taSpecAll.Active:=False;

      //�������� ������ ��� �� ������
      taServP.Active:=False;
      taServP.CreateDataSet;

      taServP.Append;
      taServPName.AsString:=quCurSpecName.AsString;
      taServPCode.AsString:=quCurSpecCode.AsString;
      taServPQuant.AsFloat:=rQ;
      taServPStream.AsInteger:=quCurSpecStream.AsInteger;
      taServPiType.AsInteger:=0; //�����
      taServP.Post;

      PrintServCh('������');

      if rQ=quCurSpecQuantity.AsFloat then
      begin
        while not quCurMod.Eof do quCurMod.delete;
        quCurSpec.Delete;
      end else
      begin
        quCurMod.First;
        while not quCurMod.Eof do
        begin
          quCurMod.Edit;
          quCurModQUANTITY.AsFloat:=quCurSpecQuantity.AsFloat-rQ;
          quCurMod.Post;

          quCurMod.Next;
        end;

        rSumD:=quCurSpecDSum.AsFloat-RoundEx(quCurSpecDSum.AsFloat*rQ/quCurSpecQuantity.AsFloat*100)/100;
        rSum:=quCurSpecSUMMA.AsFloat-RoundEx(quCurSpecSUMMA.AsFloat*rQ/quCurSpecQuantity.AsFloat*100)/100;

        quCurSpec.Edit;
        quCurSpecQUANTITY.AsFloat:=quCurSpecQuantity.AsFloat-rQ;
        quCurSpecDSum.AsFloat:=rSumD;
        quCurSpecSUMMA.AsFloat:=rSum;
        quCurSpec.Post;
      end;

      rSum:=0;
      quCurSpec.First;
      while not quCurSpec.Eof do
      begin
        rSum:=rSum+quCurSpecSumma.AsFloat;
        quCurSpec.Next;
      end;
      Tab.Summa:=rSum;
      SaveSpecN; // � ������� ������ �����
      if TestSave=False then
      begin
        SaveSpecN;
        if TestSave=False then SaveSpecN;
      end;

      quCurSpec.First;

      ViewMod.EndUpdate;
      ViewSpec.EndUpdate;

      CreateViewPers1;
    end;
  end;
  fmMessDel.Release;
end;

procedure TfmSpec.cxButton2Click(Sender: TObject);
begin
  if Tab.iStatus>0 then exit;
  if dmC.quCurSpec.Eof then exit;
  prWriteLog('---Del;'+dmC.quCurSpecSifr.AsString+';'+dmC.quCurSpecName.AsString+';');
  acDelPos.Execute;
  GridSpec.SetFocus;
end;

procedure TfmSpec.acMovePosExecute(Sender: TObject);
Var StrWk:String;
    IdHead, MaxId, Id, Id_Pos:INteger;
    rQ,rSum:Real;
begin
  //����������� �������
  if not CanDo('prPosMove') then  begin Label18.Caption:='��� ����.'; exit; end;

  with dmC do
  begin
    if quCurSpec.Eof then exit;

    if quCurSpecIStatus.AsInteger=0 then begin Label18.Caption:='���������� ����� ������ ����������� �������.'; exit; end;
    if Tab.iStatus>0 then begin Label18.Caption:='����� �� �������� ����� ������� �� �������������.'; exit; end;

    fmPosMove:=TfmPosMove.Create(Application);
    with dmC do
    begin
      fmPosMove.Label1.Caption:=quCurSpecName.AsString+' '+quCurSpecCode.AsString;
      Str(quCurSpecSumma.AsCurrency:10:2,StrWk);
      fmPosMove.Label2.Caption:='����� '+StrWk+'  ���-�� '+quCurSpecQuantity.asstring;
      fmPosMove.CalcEdit1.EditValue:=quCurSpecQuantity.AsFloat;

      fmPosMove.ShowModal;
      if fmPosMove.ModalResult=mrOk then
      begin
        Str(quCurSpecSumma.AsCurrency:10:2,StrWk);
        rQ:=quCurSpecQuantity.AsFloat;
        if (fmPosMove.CalcEdit1.EditValue<=rQ)and(fmPosMove.CalcEdit1.EditValue>0) then
        begin
          if fmPosMove.cxRadioGroup1.ItemIndex=0 then
          begin
            FormLog('MovePos0',IntToStr(Person.Id)+' '+quCurSpecId.AsString+' '+quCurSpecSifr.AsString+' '+StrWk);
            //������� ����� ����
            FormLog('CreateTab',IntToStr(Tab.Id_Personal)+' '+fmPosMove.TextEdit1.Text);

            taTabs.Active:=False;
            taTabs.Active:=True;

            IdHead:=GetId('TabH');

            trUpdTab.StartTransaction;

            taTabs.Append;
            taTabsId.AsInteger:=IdHead;
            taTabsID_PERSONAL.AsInteger:=Tab.Id_Personal;
            taTabsNUMTABLE.AsString:=fmPosMove.TextEdit1.Text;
            taTabsQUESTS.AsInteger:=1;
            taTabsTABSUM.AsFloat:=RoundEx(fmPosMove.CalcEdit1.EditValue*quCurSpecPrice.AsFloat*100)/100;
            taTabsBEGTIME.AsDateTime:=Now;
            taTabsISTATUS.AsInteger:=0; // ����������� c������ � ���.
            taTabsDISCONT.AsString:=''; //��� ������ ������
            taTabs.Post;

            trUpdTab.Commit;

            taTabs.Active:=False;

            Id:=1;
            taSpec.Active:=False;
            taSpec.Active:=True;
            trUpdTab.StartTransaction;

            taSpec.Append;
            taSpecID_TAB.AsInteger:=IdHead;
            taSpecID_PERSONAL.AsInteger:=Tab.Id_Personal;
            taSpecNUMTABLE.AsString:=fmPosMove.TextEdit1.Text;
            taSpecID.AsInteger:=Id;//1
            taSpecID_POS.AsInteger:=0;//�����
            taSpecSIFR.AsInteger:=quCurSpecSifr.AsInteger;
            taSpecPRICE.AsFloat:=quCurSpecPrice.AsFloat;
            taSpecQUANTITY.AsFloat:=fmPosMove.CalcEdit1.EditValue;
            taSpecSUMMA.AsFloat:=RoundEx(fmPosMove.CalcEdit1.EditValue*quCurSpecPrice.AsFloat*100)/100;
            taSpecDISCOUNTPROC.AsFloat:=0;
            taSpecDISCOUNTSUM.AsFloat:=0;
            taSpecISTATUS.AsInteger:=1; // ����� �� ���� ������, ������ ��� ������ �� ����
            taSpecLIMITM.AsInteger:=quCurSpecLIMITM.AsInteger;
            taSpecLINKM.AsInteger:=quCurSpecLINKM.AsInteger;
            taSpecITYPE.AsInteger:=0; //�����
            taSpec.Post;

            inc(Id);

            ViewMod.BeginUpdate;
            quCurMod.First;
            while not quCurMod.Eof do
            begin
              taSpec.Append;

              taSpecID_TAB.AsInteger:=IdHead;
              taSpecID_PERSONAL.AsInteger:=Tab.Id_Personal;
              taSpecNUMTABLE.AsString:=fmPosMove.TextEdit1.Text;
              taSpecID.AsInteger:=Id;
              taSpecID_POS.AsInteger:=1;//1 �� � ����� ���� � ����� �������������� ��������
              taSpecSIFR.AsInteger:=quCurModSifr.AsInteger;
              taSpecPRICE.AsFloat:=0;
              taSpecQUANTITY.AsFloat:=fmPosMove.CalcEdit1.EditValue;
              taSpecSUMMA.AsFloat:=0;
              taSpecDISCOUNTPROC.AsFloat:=0;
              taSpecDISCOUNTSUM.AsFloat:=0;
              taSpecISTATUS.AsInteger:=1;
              taSpecLIMITM.AsInteger:=0;
              taSpecLINKM.AsInteger:=0;
              taSpecITYPE.AsInteger:=1; //������������
              taSpec.Post;

              inc(id);
              quCurMod.Next;
            end;
            quCurMod.First;
            ViewMod.EndUpdate;

            trUpdTab.Commit;
            taSpec.Active:=False;
          end; //����� ������� ��� �����
          //� ���������
          if fmPosMove.cxRadioGroup1.ItemIndex=1 then
          begin
            FormLog('MovePos1',IntToStr(Person.Id)+' '+quCurSpecId.AsString+' '+quCurSpecSifr.AsString+' '+StrWk);

            IdHead:=quTabsPersID.AsInteger;
            taSpec1.Active:=False;
            taSpec1.ParamByName('IDTAB').AsInteger:=IdHead;
            taSpec1.Active:=True;

            rSum:=0; MaxId:=0;
            taSpec1.First;
            while not taSpec1.Eof do
            begin
              if taSpec1Id.AsInteger>MaxId then MaxId:=taSpec1Id.AsInteger;
              rSum:=rSum+taSpec1SUMMA.AsFloat;
              taSpec1.Next;
            end;

            inc(MaxId);

            taTabs.Active:=False;
            taTabs.Active:=True;

            if taTabs.Locate('ID',IDHead,[]) then
            begin
              trUpdTab.StartTransaction;
              taTabs.Edit;
              taTabsTABSUM.AsFloat:=rSum+RoundEx(fmPosMove.CalcEdit1.EditValue*quCurSpecPrice.AsFloat*100)/100;
              taTabs.Post;
              trUpdTab.Commit;
              taTabs.Active:=False;

              trUpdTab.StartTransaction;
              taSpec1.Append;
              taSpec1ID_TAB.AsInteger:=IdHead;
              taSpec1ID_PERSONAL.AsInteger:=Tab.Id_Personal;
              taSpec1NUMTABLE.AsString:=fmPosMove.TextEdit1.Text;
              taSpec1ID.AsInteger:=MaxId;
              taSpec1ID_POS.AsInteger:=0; //�����
              taSpec1SIFR.AsInteger:=quCurSpecSifr.AsInteger;
              taSpec1PRICE.AsFloat:=quCurSpecPrice.AsFloat;
              taSpec1QUANTITY.AsFloat:=fmPosMove.CalcEdit1.EditValue;
              taSpec1SUMMA.AsFloat:=RoundEx(fmPosMove.CalcEdit1.EditValue*quCurSpecPrice.AsFloat*100)/100;
              taSpec1DISCOUNTPROC.AsFloat:=0;
              taSpec1DISCOUNTSUM.AsFloat:=0;
              taSpec1ISTATUS.AsInteger:=1; // ����� �� ���� ������, ������ ��� ������ �� ����
              taSpec1LIMITM.AsInteger:=quCurSpecLIMITM.AsInteger;
              taSpec1LINKM.AsInteger:=quCurSpecLINKM.AsInteger;
              taSpec1ITYPE.AsInteger:=0; //�����

              taSpec1.Post;

              inc(MaxId);

              Id_Pos:=MaxId-1;
              ViewMod.BeginUpdate;
              quCurMod.First;
              while not quCurMod.Eof do
              begin
                taSpec1.Append;
                taSpec1ID_TAB.AsInteger:=IdHead;
                taSpec1ID_PERSONAL.AsInteger:=Tab.Id_Personal;
                taSpec1NUMTABLE.AsString:=fmPosMove.TextEdit1.Text;
                taSpec1ID.AsInteger:=MaxId;
                taSpec1ID_POS.AsInteger:=ID_POS;
                taSpec1SIFR.AsInteger:=quCurModSifr.AsInteger;
                taSpec1PRICE.AsFloat:=0;
                taSpec1QUANTITY.AsFloat:=fmPosMove.CalcEdit1.EditValue;
                taSpec1SUMMA.AsFloat:=0;
                taSpec1DISCOUNTPROC.AsFloat:=0;
                taSpec1DISCOUNTSUM.AsFloat:=0;
                taSpec1ISTATUS.AsInteger:=1;
                taSpec1LIMITM.AsInteger:=0;
                taSpec1LINKM.AsInteger:=0;
                taSpec1ITYPE.AsInteger:=1; //������������
                taSpec1.Post;

                inc(Maxid);
                quCurMod.Next;
              end;
              quCurMod.First;
              ViewMod.EndUpdate;

              trUpdTab.Commit;
            end;
            taSpec1.Active:=False;
          end;
        end;

        //����������� ������� �����


        rQ:=RoundEx((rQ-fmPosMove.CalcEdit1.EditValue)*1000)/1000; //� ��� �� ���-��
        if rQ=0 then
        begin
          quCurSpec.Delete;
          while not quCurMod.Eof do quCurMod.Delete;
        end
        else
        begin
          //��� ����� ��������� � ��������� �.�. �������� ���
          CalcDiscontN(quCurSpecSifr.AsInteger,quCurSpecId.AsInteger,-1,quCurSpecSTREAM.AsInteger,quCurSpecPrice.AsFloat,rQ,0,Tab.DBar,Now,Check.DProc,Check.DSum);

          quCurSpec.Edit;
          quCurSpecQuantity.AsFloat:=rQ;
          quCurSpecDProc.AsFloat:=Check.DProc;
          quCurSpecDSum.AsFloat:=Check.DSum;
          quCurSpecSumma.AsFloat:=RoundEx(rQ*quCurSpecPrice.AsFloat*100)/100-Check.DSum;
          quCurSpec.Post;

          quCurMod.First;
          while not quCurMod.Eof do
          begin
            quCurMod.Edit;
            quCurModQUANTITY.AsFloat:=rQ;
            quCurMod.Post;

            quCurMod.Next;
          end;
        end;

       //����� ��������� ����� �� �������� ������������

        ViewSpec.BeginUpdate;
        Tab.Summa:=0;
        quCurSpec.First;
        while not quCurSpec.Eof do
        begin
          Tab.Summa:=Tab.Summa+quCurSpecSumma.AsFloat;
          quCurSpec.Next;
        end;

        SaveSpecN;
        if TestSave=False then
        begin
          SaveSpecN;
          if TestSave=False then SaveSpecN;
        end;

        quCurSpec.First;
        ViewSpec.EndUpdate;
        //��������� �������� ���
        CreateViewPers1;
      end;
    end;
  end;
  fmPosMove.Release;
end;

procedure TfmSpec.FormShow(Sender: TObject);
begin
  if not CanDo('prPosMove') then cxButton4.Visible:=False
  else cxButton4.Visible:=True;
  cxButton12.Visible:=True;
  cxButton12.Enabled:=True;
  if not CanDo('prPrintCheck') then cxButton12.Visible:=False
  else begin
 {   if CommonSet.CashNum<=0 then
    begin
      cxButton12.Visible:=False;
      cxButton12.Enabled:=False;
    end else cxButton12.Visible:=True;}
  end;
  if CommonSet.SpecChar=1 then  //�������� ��� ������������ ������ �� ������������, ���� �� ������������ ������ ���
  begin
    cxButton12.Visible:=False;
    cxButton12.Enabled:=False;
  end;
  Edit1.Text:='';
  DiscountBar:='';
  GridSpec.SetFocus;
  Top:=40;
  Left:=10;
  bPrintCheck:=False;
end;

procedure TfmSpec.cxButton4Click(Sender: TObject);
begin
  if Tab.iStatus>0 then exit;
  if dmC.quCurSpec.Eof then exit;
  prWriteLog('---Remove;'+dmC.quCurSpecSifr.AsString+';'+dmC.quCurSpecName.AsString+';');
  acMovePos.Execute;
  GridSpec.SetFocus;
end;

procedure TfmSpec.acPrePrintExecute(Sender: TObject);
Var rSum,rSumD:Real;
    StrWk,StrWk1:String;
    bEdit:Boolean;
    iQ:Integer;
begin
  if (Tab.iStatus=1)and(CommonSet.PreCheckCount=1) then
  begin
    showmessage('��������� ������ ����� ���������.');
    exit;
  end;
  cxButton12.Enabled:=False;

// �������� ����� � ���������� �������
  bSaveSpec:=False; //�� �������� ������� ��������
  bPrintPre:=True;
  prWriteLog('---PrePrint;'+IntToStr(Tab.Id)+';'+Tab.NumTable+';'+Person.Name+';'+IntToStr(Tab.Id_Personal)+';');
  ViewSpec.BeginUpdate;
  ViewMod.BeginUpdate;
//  Action10.Enabled:=False;
  rSum:=0;
  rSumD:=0;
  bEdit:=False;

  with dmC do
  begin
    quCurSpec.First;
    while not quCurSpec.Eof do
    begin
      rSum:=rSum+quCurSpecSumma.AsFloat;
      rSumD:=rSumD+quCurSpecDSum.AsFloat;
      if quCurSpecIStatus.AsInteger=0 then bEdit:=True;
      quCurSpec.Next;
    end;

    if rSum<>Tab.Summa then bEdit:=True;

    Tab.iStatus:=1; //���� � ��������� ��������

    //�������� ������ ���������
    SaveStatusTab(Tab.Id,Tab.iStatus,sEdit1.Value,BEdit1.Text);

    if bEdit then //���� ��������� ������������
    begin
      Tab.Summa:=rSum;
      SaveSpecN; //� ������� ������ �����
      if TestSave=False then
      begin
        SaveSpecN;
        if TestSave=False then SaveSpecN;
      end;
    end;

    SetStatus(StrWk); //���������� �� ������
    Label11.Caption:=StrWk;

    inc(CommonSet.PreCheckNum);
    WriteCheckNum;
    FindDiscount(Tab.DBar); //��� ������������ Tab.DBar, Tab.DPercent, Tab.DName

    //������ �������� �� ��
    if pos('fis',CommonSet.PrePrintPort)>0 then
    begin
      if CommonSet.PrePrintPort='fisprim' then
      begin
        OpenNFDoc;
        SelectF(13); PrintNFStr(' '+CommonSet.DepartName);
        PrintNFStr('');

        SelectF(14); PrintNFStr('        ����');
        SelectF(13); PrintNFStr(''); PrintNFStr('');
        PrintNFStr('��������: '+Tab.Name); PrintNFStr('����: '+Tab.NumTable);
        SelectF(3); PrintNFStr('������: '+IntToStr(Tab.Quests));

        if Tab.DBar>'' then
        begin
          Str(Tab.DPercent:5:2,StrWk);
          PrintNFStr('�����: '+Tab.DName+' ('+StrWk+'%)');
        end;
        PrintNFStr('������: '+FormatDateTime('dd.mm.yyyy hh:nn:ss',Tab.OpenTime));

        SelectF(3); StrWk:='                                                       ';
        PrintNFStr(StrWk);
        SelectF(15); StrWk:=' ��������          ���-��   ����   ����� ';
        PrintNFStr(StrWk);
        SelectF(3); StrWk:='                                                       ';
        PrintNFStr(StrWk);

        quCurModAll.Active:=False;
        quCurModAll.ParamByName('IDT').AsInteger:=Tab.Id;
        quCurModAll.Active:=True;

        quCurSpec.First;
        while not quCurSpec.Eof do
        begin
          StrWk:= Copy(quCurSpecName.AsString,1,29);
          while Length(StrWk)<29 do StrWk:=StrWk+' ';
          Str(quCurSpecQuantity.AsFloat:5:1,StrWk1);
          StrWk:=StrWk+' '+StrWk1;
          Str(quCurSpecPrice.AsFloat:7:2,StrWk1);
          StrWk:=StrWk+' '+StrWk1;
          Str(quCurSpecSumma.AsFloat:8:2,StrWk1);
          StrWk:=StrWk+' '+StrWk1+'���';
          SelectF(3);
          PrintNFStr(StrWk);

          while (quCurModAllID_POS.AsInteger<quCurSpecID.AsInteger) and (quCurModAll.Eof=False) do quCurModAll.Next;
          while (quCurModAllID_POS.AsInteger=quCurSpecID.AsInteger) and (quCurModAll.Eof=False) do
          begin
            StrWk:=Copy(quCurModAllNAME.AsString,1,29);
            StrWk:='   '+StrWk;
            SelectF(0);
            PrintNFStr(StrWk);

            quCurModAll.Next;
          end;

          quCurSpec.Next;
        end;

        SelectF(3); StrWk:='                                                       ';
        PrintNFStr(StrWk);
        SelectF(10); PrintNFStr(' ');
        Str(rSum:8:2,StrWk1);
        StrWk:=' �����                      '+StrWk1+' ���';
        SelectF(15); PrintNFStr(StrWk);

        if rSumD>0.02 then
        begin
          SelectF(3);
          StrWk:='                                                       ';
          PrintNFStr(StrWk);

          Str(rSumD:8:2,StrWk);
          rSumD:=RoundEx((rSumD/(rSum+rSumD)*100)*100)/100;
          Str(rSumD:5:2,StrWk1);
          StrWk:=' � �.�. ������ - '+StrWk1+'%  '+StrWk+'�.';

          SelectF(10); PrintNFStr(StrWk);
        end;

        PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');
        CloseNFDoc;
        CutDoc;
      end;
      if CommonSet.PrePrintPort='fisshtrih' then
      begin
        PrintNFStr(' '+CommonSet.DepartName); PrintNFStr('');

        PrintNFStr('             ����.');
        PrintNFStr(' ');
        PrintNFStr('��������: '+Tab.Name);
        PrintNFStr('����: '+Tab.NumTable);
        if Tab.DBar>'' then
        begin
          Str(Tab.DPercent:5:2,StrWk);
          PrintNFStr('�����: '+Tab.DName+' ('+StrWk+'%)');
        end;  
        PrintNFStr('������: '+IntToStr(Tab.Quests));
        PrintNFStr('������: '+FormatDateTime('dd.mm.yyyy hh:nn:ss',Tab.OpenTime));

        StrWk:='-----------------------------------';
        PrintNFStr(StrWk);
        StrWk:=' ��������     ���-��  ����   ����� ';
        PrintNFStr(StrWk);
        StrWk:='-----------------------------------';
        PrintNFStr(StrWk);

        quCurModAll.Active:=False;
        quCurModAll.ParamByName('IDT').AsInteger:=Tab.Id;
        quCurModAll.Active:=True;

        quCurSpec.First;
        while not quCurSpec.Eof do
        begin
          StrWk:= Copy(quCurSpecName.AsString,1,36);
          while Length(StrWk)<36 do StrWk:=StrWk+' ';
          PrintNFStr(StrWk); //������� ����� �������� - �������� �������

          Str(quCurSpecQuantity.AsFloat:5:1,StrWk1);
          StrWk:='          '+StrWk1;
          Str(quCurSpecPrice.AsFloat:7:2,StrWk1);
          StrWk:=StrWk+' '+StrWk1;
          Str(quCurSpecSumma.AsFloat:8:2,StrWk1);
          StrWk:=StrWk+' '+StrWk1+'���';
          PrintNFStr(StrWk);

          while (quCurModAllID_POS.AsInteger<quCurSpecID.AsInteger) and (quCurModAll.Eof=False) do quCurModAll.Next;
          while (quCurModAllID_POS.AsInteger=quCurSpecID.AsInteger) and (quCurModAll.Eof=False) do
          begin
            StrWk:=Copy(quCurModAllNAME.AsString,1,30);
            StrWk:='     '+StrWk;
            PrintNFStr(StrWk);

            quCurModAll.Next;
          end;

          quCurSpec.Next;
        end;

        StrWk:='-----------------------------------';
        PrintNFStr(StrWk);

        Str(rSum:8:2,StrWk1);
        StrWk:=' �����                 '+StrWk1+' ���';
        PrintNFStr(StrWk);

        if rSumD>0.02 then
        begin
          StrWk:='-----------------------------------';
          PrintNFStr(StrWk);

          Str(rSumD:8:2,StrWk);
          rSumD:=RoundEx((rSumD/(rSum+rSumD)*100)*100)/100;
          Str(rSumD:5:2,StrWk1);
          StrWk:=' � �.�. ������ - '+StrWk1+'%  '+StrWk+'�.';
          PrintNFStr(StrWk);
        end;
        PrintNFStr('');PrintNFStr('');PrintNFStr('');PrintNFStr('');PrintNFStr('');PrintNFStr('');PrintNFStr('');
        CutDoc;
      end;
    end
    else
    if (CommonSet.PrePrintPort<>'0')and(CommonSet.PrePrintPort<>'G')and(pos('DB',CommonSet.PrePrintPort)=0) then
    begin
      try
        prOpenDevPrint(CommonSet.PrePrintPort);

        SelFont(13); PrintStr(' '+CommonSet.DepartName); PrintStr(' ');
        SelFont(14); PrintStr('       ����');
        SelFont(13); PrintStr(' ');
        PrintStr('��������: '+Tab.Name);
        PrintStr('����: '+Tab.NumTable);
        if Tab.DBar>'' then
        begin
          Str(Tab.DPercent:5:2,StrWk);
          PrintStr('�����: '+Tab.DName+' ('+StrWk+'%)');
        end;
        SelFont(13); PrintStr('������: '+IntToStr(Tab.Quests));
        PrintStr('������: '+FormatDateTime('dd.mm.yyyy hh:nn:ss',Tab.OpenTime));

        SelFont(13); StrWk:='                                          ';
        PrintStr(StrWk);
        SelFont(15); StrWk:=' ��������          ���-��   ����   ����� ';
        PrintStr(StrWk);
        SelFont(13); StrWk:='                                          ';
        PrintStr(StrWk);

        quCurModAll.Active:=False;
        quCurModAll.ParamByName('IDT').AsInteger:=Tab.Id;
        quCurModAll.Active:=True;

        quCurSpec.First;
        while not quCurSpec.Eof do
        begin
          StrWk:= Copy(quCurSpecName.AsString,1,19);
          while Length(StrWk)<19 do StrWk:=StrWk+' ';
          Str(quCurSpecQuantity.AsFloat:5:1,StrWk1);
          StrWk:=StrWk+' '+StrWk1;
          Str(quCurSpecPrice.AsFloat:7:2,StrWk1);
          StrWk:=StrWk+' '+StrWk1;
          Str((quCurSpecQuantity.AsFloat*quCurSpecPrice.AsFloat):8:2,StrWk1);
          StrWk:=StrWk+StrWk1+'�';
          SelFont(15);
          PrintStr(StrWk);


          while (quCurModAllID_POS.AsInteger<quCurSpecID.AsInteger) and (quCurModAll.Eof=False) do quCurModAll.Next;
          while (quCurModAllID_POS.AsInteger=quCurSpecID.AsInteger) and (quCurModAll.Eof=False) do
          begin
            StrWk:=Copy(quCurModAllNAME.AsString,1,29);
            StrWk:='   '+StrWk;
            SelFont(10);
            PrintStr(StrWk);

            quCurModAll.Next;
          end;

          quCurSpec.Next;
        end;

        SelFont(13);
        StrWk:='                                          ';
        PrintStr(StrWk);

        SelFont(15);
        Str((rSum+rSumD):8:2,StrWk1);
        StrWk:=' �����                      '+StrWk1+' ���';
        PrintStr(StrWk);

        if rSumD>0.02 then
        begin
          Str(((-1)*rSumD):8:2,StrWk);
          rSumD:=RoundEx((rSumD/(rSum+rSumD)*100)*100)/100;
          Str(rSumD:5:2,StrWk1);
//          StrWk:=' � �.�. ������ - '+StrWk1+'%  '+StrWk+'�.';
          StrWk:=' ������                    -'+StrWk+' ���';
          PrintStr(StrWk);
        end;

        Str(rSum:8:2,StrWk1);
        StrWk:=' ����� � ������             '+StrWk1+' ���';
        PrintStr(StrWk);

        CutDocPr;
      finally
//        Delay(100);
        DevPrint.Close;
      end;
    end;

    if CommonSet.PrePrintPort='G' then
    begin
      quCheck.Active:=False;
      quCheck.ParamByName('IDTAB').Value:=Tab.Id;
      quCheck.Active:=True;

      quCheck.Filtered:=False;
      quCheck.Filter:='ITYPE=0';
      quCheck.Filtered:=True;

      frRepSp.LoadFromFile(CurDir + 'PreCheck1.frf');

      frVariables.Variable['Waiter']:=Tab.Name;
      frVariables.Variable['TabNum']:=Tab.NumTable;
      frVariables.Variable['OpenTime']:=FormatDateTime('dd.mm.yyyy hh:nn:ss',Tab.OpenTime);
      frVariables.Variable['Quests']:=IntToStr(Tab.Quests);
      frVariables.Variable['ZNum']:=IntToStr(Tab.Id);
      frVariables.Variable['PreNum']:=IntToStr(CommonSet.PreCheckNum-1);

      if rSumD>0.2 then
      begin
        str(rSumD:8:2,StrWk);
        frVariables.Variable['Discount']:='� �.�. ������ '+StrWk;
      end else frVariables.Variable['Discount']:='';

      frRepSp.ReportName:='����.';
      frRepSp.PrepareReport;

//      frRepSp.ShowPreparedReport;
      frRepSp.PrintPreparedReportDlg;

      quCheck.Active:=False;
    end;
    if pos('DB',CommonSet.PrePrintPort)>0 then //���� � ����  DB1 ������; DB2 POSIFLEX AURA PP7000; DB3 STAR TSP600
    begin                    //� ������ �������� ����� ������ DB, � � ������ ������� ��� ��� - 1COM1,2COM1,3COM1 - c ���������� �������
        //��������� �������
      with dmC1 do
      begin
        quPrint.Active:=False;
        quPrint.Active:=True;
        iQ:=GetId1('PQH0'); //��� ���������� �������� �� 1

        PrintDBStr(iQ,0,CommonSet.PrePrintPort,' '+CommonSet.DepartName,13,0);
        PrintDBStr(iQ,0,CommonSet.PrePrintPort,'',13,0);
        PrintDBStr(iQ,0,CommonSet.PrePrintPort,'       ����',14,0);
        PrintDBStr(iQ,0,CommonSet.PrePrintPort,'',13,0);
        PrintDBStr(iQ,0,CommonSet.PrePrintPort,'��������: '+Tab.Name,13,0);
        PrintDBStr(iQ,0,CommonSet.PrePrintPort,'����: '+Tab.NumTable,13,0);
        if Tab.DBar>'' then
        begin
          Str(Tab.DPercent:5:2,StrWk);
          PrintDBStr(iQ,0,CommonSet.PrePrintPort,'�����: '+Tab.DName+' ('+StrWk+'%)',13,0);
        end;
        PrintDBStr(iQ,0,CommonSet.PrePrintPort,'������: '+FormatDateTime('dd.mm.yyyy hh:nn:ss',Tab.OpenTime),13,0);
        PrintDBStr(iQ,0,CommonSet.PrePrintPort,'-',13,0);
        PrintDBStr(iQ,0,CommonSet.PrePrintPort,' ��������          ���-��   ����   ����� ',15,0);
        PrintDBStr(iQ,0,CommonSet.PrePrintPort,'-',13,0);

        quCurModAll.Active:=False;
        quCurModAll.ParamByName('IDT').AsInteger:=Tab.Id;
        quCurModAll.Active:=True;

        quCurSpec.First;
        while not quCurSpec.Eof do
        begin
          if pos('DBfis2',CommonSet.PrePrintPort)>0 then
          begin  //�� ���������� �����   - ��� �� 2-� ������ �� 36 �������� - ���� �������� �������
            StrWk:= Copy(quCurSpecName.AsString,1,36);
            while Length(StrWk)<36 do StrWk:=StrWk+' ';
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,StrWk,15,0);

            Str(quCurSpecQuantity.AsFloat:5:1,StrWk1);
            StrWk:=' '+StrWk1;
            Str(quCurSpecPrice.AsFloat:7:2,StrWk1);
            StrWk:=StrWk+' '+StrWk1;
            Str(quCurSpecSumma.AsFloat:8:2,StrWk1);
            StrWk:=StrWk+' '+StrWk1+'�';
            while Length(StrWk)<36 do StrWk:=' '+StrWk;

            PrintDBStr(iQ,0,CommonSet.PrePrintPort,StrWk,15,0);
          end else
          begin
            StrWk:= Copy(quCurSpecName.AsString,1,19);
            while Length(StrWk)<19 do StrWk:=StrWk+' ';
            Str(quCurSpecQuantity.AsFloat:5:1,StrWk1);
            StrWk:=StrWk+' '+StrWk1;
            Str(quCurSpecPrice.AsFloat:7:2,StrWk1);
            StrWk:=StrWk+' '+StrWk1;
            Str((quCurSpecQuantity.AsFloat*quCurSpecPrice.AsFloat):8:2,StrWk1);
            StrWk:=StrWk+StrWk1+'�';

            PrintDBStr(iQ,0,CommonSet.PrePrintPort,StrWk,15,0);
          end;

          while (quCurModAllID_POS.AsInteger<quCurSpecID.AsInteger) and (quCurModAll.Eof=False) do quCurModAll.Next;
          while (quCurModAllID_POS.AsInteger=quCurSpecID.AsInteger) and (quCurModAll.Eof=False) do
          begin
            StrWk:=Copy(quCurModAllNAME.AsString,1,29);
            StrWk:='   '+StrWk;

            PrintDBStr(iQ,0,CommonSet.PrePrintPort,StrWk,10,0);
            quCurModAll.Next;
          end;

          quCurSpec.Next;
        end;

        PrintDBStr(iQ,0,CommonSet.PrePrintPort,'-',13,0);

        Str((rSum+rSumD):8:2,StrWk1);
        StrWk:=' �����                      '+StrWk1+' ���';
        PrintDBStr(iQ,0,CommonSet.PrePrintPort,StrWk,15,0);

        if rSumD>0.02 then
        begin
          Str((rSumD*(-1)):8:2,StrWk);
          StrWk:=' ������                     '+StrWk+' ���';
          PrintDBStr(iQ,0,CommonSet.PrePrintPort,StrWk,15,0);
        end;

        Str((rSum):8:2,StrWk1);
        StrWk:=' ����� � ������             '+StrWk1+' ���';
        PrintDBStr(iQ,0,CommonSet.PrePrintPort,StrWk,15,0);

        GetId1('PQH'); //����������� ������� �� 1-�
        quPrint.Active:=False;
      end;
    end;
    quCurSpec.First;
    ViewSpec.EndUpdate;
    ViewMod.EndUpdate;
//    delay(100);

    GridSpec.SetFocus;

    CreateViewPers1; //��� ������ ��������� ���� ����������� ����������
//  quTabs.Refresh;


    bPrintPre:=False;
//    Action10.Enabled:=True;

//    bCloseSpec:=True;

    if bCloseSpec then
    begin
      bCloseSpec:=False;
      prWriteLog('---ExitFromPrePrint;'+IntToStr(Tab.Id)+';'+Tab.NumTable+';'+Person.Name+';'+IntToStr(Tab.Id_Personal)+';');;
      prWriteLog('');
      prClearCur.ParamByName('ID_TAB').AsInteger:=Tab.Id;
      prClearCur.ParamByName('ID_STATION').AsInteger:=CommonSet.Station;
      prClearCur.ExecProc;
      close;
    end;
  end;
  cxButton12.Enabled:=True;
end;

procedure TfmSpec.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var bCh:Byte;
    iShift,iType,Sifr,iLinkM,iLimit,iStream:Integer;
    StrWk:String;
    iKey:Integer;
    sName,sCode:String;
    rPrice:Real;
begin
  bCh:=ord(Key);

  if Tab.iStatus=1 then exit; // � �������� �������������� ���������

  if (Chr(bCh) in ['0','1','2','3','4','5','6','7','8','9',';','?']=False)
  and (bCh <> 13) then
  begin  //��� ��
    exit;
  end;

  //121,27,18
  iShift:=0;
  if Shift=[ssShift]  then iShift:=1;
  if Shift=[ssCtrl]   then iShift:=2;
  if Shift=[ssAlt]  then iShift:=3;

  iKey:=bCh+1000*iShift;

  with dmC do
  begin
    if not CommonSet.bTouch then //���� �� ��� �� ����� ����� ��������� �� ������� �������
    begin
      if prFindKey(iKey,sName,sCode,rPrice,Sifr,iType,iLinkM,iLimit,iStream) then
      begin
        if iType=1 then //�����
        begin
          CalcDiscontN(Sifr,Check.Max+1,-1,iStream,rPrice,1,0,Tab.DBar,Now,Check.DProc,Check.DSum);

          quCurSpec.Append;
          quCurSpecSTATION.AsInteger:=CommonSet.Station;
          quCurSpecID_TAB.AsInteger:=Tab.Id;
          quCurSpecId_Personal.AsInteger:=Person.Id;
          quCurSpecNumTable.AsString:=Tab.NumTable;
          quCurSpecId.AsInteger:=Check.Max+1;
          quCurSpecSifr.AsInteger:=Sifr;
          quCurSpecPrice.AsFloat:=rPrice;
          quCurSpecQuantity.AsFloat:=1;
          quCurSpecSumma.AsFloat:=rPrice-Check.DSum;
          quCurSpecDProc.AsFloat:=Check.DProc;
          quCurSpecDSum.AsFloat:=Check.DSum;
          quCurSpecIStatus.AsInteger:=0;
          quCurSpecName.AsString:=sName;
          quCurSpecCode.AsString:=sCode;
          quCurSpecLinkM.AsInteger:=iLinkM;
          if iLimit>0 then quCurSpecLimitM.AsInteger:=iLimit
          else quCurSpecLimitM.AsInteger:=99;
          quCurSpecStream.AsInteger:=iStream;
          quCurSpec.Post;
          inc(Check.Max);

          if iLinkM>0 then
          begin //������������
            acModify.Execute;
          end;
          GridSpec.SetFocus;
        end;
        if iType=2 then //������ �������
        begin
          quMenu.Active:=False;
          quMenu.ParamByName('Id_Parent').Value:=Sifr; //������ ����
          quMenu.Active:=True;
          fmMenuClass.ShowModal;
          if fmMenuClass.ModalResult=mrOk then
          begin
            fmCalc.CalcEdit1.EditValue:=1;
            fmCalc.ShowModal;
            if (fmCalc.ModalResult=mrOk)and(fmCalc.CalcEdit1.EditValue>0)  then
            begin
              CalcDiscontN(quMenuSIFR.AsInteger,Check.Max+1,Sifr,quMenuSTREAM.AsInteger,quMenuPRICE.AsFloat,fmCalc.CalcEdit1.EditValue,0,Tab.DBar,Now,Check.DProc,Check.DSum);

              quCurSpec.Append;
              quCurSpecSTATION.AsInteger:=CommonSet.Station;
              quCurSpecID_TAB.AsInteger:=Tab.Id;
              quCurSpecId_Personal.AsInteger:=Person.Id;
              quCurSpecNumTable.AsString:=Tab.NumTable;
              quCurSpecId.AsInteger:=Check.Max+1;
              quCurSpecSifr.AsInteger:=quMenuSIFR.AsInteger;
              quCurSpecPrice.AsFloat:=quMenuPRICE.AsFloat;
              quCurSpecQuantity.AsFloat:=RoundEx(fmCalc.CalcEdit1.EditValue*1000)/1000;
              quCurSpecSumma.AsFloat:=RoundEx(quMenuPRICE.AsFloat*fmCalc.CalcEdit1.EditValue*100)/100-Check.DSum;
              quCurSpecDProc.AsFloat:=Check.DProc;
              quCurSpecDSum.AsFloat:=Check.DSum;
              quCurSpecIStatus.AsInteger:=0;
              quCurSpecName.AsString:=quMenuNAME.AsString;
              quCurSpecCode.AsString:=quMenuCODE.AsString;
              quCurSpecLinkM.AsInteger:=quMenuLINK.AsInteger;
              if quMenuLIMITPRICE.AsFloat>0 then
              quCurSpecLimitM.AsInteger:=RoundEx(quMenuLIMITPRICE.AsFloat)
              else quCurSpecLimitM.AsInteger:=99;
              quCurSpecStream.AsInteger:=quMenuSTREAM.AsInteger;
              quCurSpec.Post;
              inc(Check.Max);

              if dmC.quMenuLINK.AsInteger>0 then
              begin //������������
                acModify.Execute;
              end;
              GridSpec.SetFocus;
            end;
          end;
        end;
        DiscountBar:=''
      end; //FindKey - ��� ���� �����������
    end
    else //���, ��, ��
    begin
      if (bCh = 13)and(Length(DiscountBar)>0) then
      begin //���� ����������
//      if (Length(DiscountBar)<=5)or(Length(DiscountBar)=13) then
        if DiscountBar[1]<>';' then //��� �� ������� - �� ������� ��� �������
        begin  //��� ��� ��
           //����� �� ����
          quMenuSel.Active:=False;
          quMenuSel.SelectSQL.Clear;
          quMenuSel.SelectSQL.Add('SELECT SIFR,NAME,PRICE,CODE,LIMITPRICE,LINK,STREAM,BARCODE,PARENT');
          quMenuSel.SelectSQL.Add('FROM MENU');
//          if Length(DiscountBar)=4 then
          quMenuSel.SelectSQL.Add('WHERE SIFR='''+DiscountBar+'''');
//          if Length(DiscountBar)=13 then
          quMenuSel.SelectSQL.Add('or BARCODE='''+DiscountBar+'''');
          quMenuSel.SelectSQL.Add('and TREETYPE=''F''');

          quMenuSel.Active:=True;

          if (quMenuSel.RecordCount>0) then
          begin
            fmCalc.CalcEdit1.EditValue:=1;
            fmCalc.ShowModal;
            if (fmCalc.ModalResult=mrOk)and(fmCalc.CalcEdit1.EditValue>0)and(quMenuSelPRICE.AsFloat>0)
            then begin
              CalcDiscontN(quMenuSelSIFR.AsInteger,Check.Max+1,-1,quMenuSelSTREAM.AsInteger,quMenuSelPRICE.AsFloat,fmCalc.CalcEdit1.EditValue,0,Tab.DBar,Now,Check.DProc,Check.DSum);

              quCurSpec.Append;
              quCurSpecSTATION.AsInteger:=CommonSet.Station;
              quCurSpecID_TAB.AsInteger:=Tab.Id;
              quCurSpecId_Personal.AsInteger:=Person.Id;
              quCurSpecNumTable.AsString:=Tab.NumTable;
              quCurSpecId.AsInteger:=Check.Max+1;
              quCurSpecSifr.AsInteger:=quMenuSelSIFR.AsInteger;
              quCurSpecPrice.AsFloat:=quMenuSelPRICE.AsFloat;
              quCurSpecQuantity.AsFloat:=fmCalc.CalcEdit1.EditValue;
              quCurSpecSumma.AsFloat:=RoundEx(quMenuSelPRICE.AsFloat*fmCalc.CalcEdit1.EditValue*100)/100-Check.DSum;
              quCurSpecDProc.AsFloat:=Check.DProc;
              quCurSpecDSum.AsFloat:=Check.DSum;

              quCurSpecIStatus.AsInteger:=0;
              quCurSpecName.AsString:=quMenuSelNAME.AsString;
              quCurSpecCode.AsString:=quMenuSelCODE.AsString;
              quCurSpecLinkM.AsInteger:=quMenuSelLINK.AsInteger;
              if quMenuSelLIMITPRICE.AsFloat>0 then
              quCurSpecLimitM.AsInteger:=RoundEx(quMenuSelLIMITPRICE.AsFloat)
              else quCurSpecLimitM.AsInteger:=99;
              quCurSpecStream.AsInteger:=quMenuSelSTREAM.AsInteger;
              quCurSpec.Post;

              inc(Check.Max);

              if quMenuSelLINK.AsInteger>0 then
              begin //������������
                acModify.Execute;
              end;
              GridSpec.SetFocus;
            end;
          end
          else Label18.Caption:='����� �� K��� (��) �� �������.';
        end
        else  //�������
        begin
          If FindDiscount(DiscountBar) then
          begin
            // ��������� ������
            Str(Tab.DPercent:5:2,StrWk);
            Label17.Caption:='������������ ������ - '+ Tab.DName+' ('+StrWk+'%)';
            //������ ������ �� ��� �������
            ViewSpec.BeginUpdate;
            quCurSpec.First;
            While not quCurSpec.Eof do
            begin
              CalcDiscontN(quCurSpecSifr.AsInteger,quCurSpecId.AsInteger,-1,quCurSpecSTREAM.AsInteger,quCurSpecPrice.AsFloat,quCurSpecQuantity.AsFloat,0,Tab.DBar,Now,Check.DProc,Check.DSum);
              if Check.DSum<>0 then
              begin
                quCurSpec.Edit;
                quCurSpecSumma.AsFloat:=RoundEx(quCurSpecPrice.AsFloat*quCurSpecQuantity.AsFloat*100)/100-Check.DSum;
                quCurSpecDProc.AsFloat:=Check.DProc;
                quCurSpecDSum.AsFloat:=Check.DSum;
                quCurSpec.Post;

                ViewSpecDPROC.Visible:=True;
                ViewSpecDSum.Visible:=True;
              end;
              quCurSpec.Next;
            end;
            quCurSpec.First;
            ViewSpec.EndUpdate;
          end;
        end;
        DiscountBar:='';
        Edit1.Text:='';
        GridSpec.SetFocus;
      end
      else //�����
        DiscountBar:=DiscountBar+Chr(bCh);
    end;
  end;
end;

procedure TfmSpec.acModifyExecute(Sender: TObject);
begin
  //������������
  with dmC do
  begin
    quModif.Active:=False;
    quModif.ParamByName('PARENT').AsInteger:=quCurSpecLINKM.AsInteger;
    quModif.Active:=True;
    fmModif:=TFmModif.Create(Application);
    //������� �����
    MaxMod:=quCurSpecLimitM.AsInteger;
    CountMod:=quCurMod.RecordCount;

    fmModif.Label1.Caption:='�������� � ���������� '+INtToStr(MaxMod-CountMod)+' ������������.';

    fmModif.ShowModal;
    fmModif.Release;
    quModif.Active:=False;
  end;
end;

procedure TfmSpec.cxButton5Click(Sender: TObject);
begin
  prWriteLog('---AddMod;');
  acAddMod.Execute;
end;

procedure TfmSpec.cxButton6Click(Sender: TObject);
begin
  prWriteLog('---DelMod;');
  acDelMod.Execute;
end;

procedure TfmSpec.Timer1Timer(Sender: TObject);
begin
  if MessClearSpec then
  begin
    Label18.Caption:='';
    MessClearSpec:=False;
  end;
  if Label18.Caption>'' then
  MessClearSpec:=True;
end;

procedure TfmSpec.Action1Execute(Sender: TObject);
begin
//
end;

procedure TfmSpec.Action2Execute(Sender: TObject);
begin
//
end;

procedure TfmSpec.acExitNExecute(Sender: TObject);
Var rSum:Real;
    bEdit,bEditH:Boolean;
//    Id:Integer;
begin
  ViewSpec.BeginUpdate;
  ViewMod.BeginUpdate;
//  dsCheck.DataSet:=nil;
  rSum:=0;
  bEdit:=False;
  bEditH:=False;
  with dmC do
  begin
    quCurSpec.First;
    while not quCurSpec.Eof do
    begin
      rSum:=rSum+quCurSpecSumma.AsFloat;
      if quCurSpecIStatus.AsInteger=0 then bEdit:=True;
      quCurSpec.Next;
    end;

    if rSum<>Tab.Summa then bEdit:=True;
    if Tab.DBar<>Tab.DBar1 then bEdit:=True;

    if (Tab.NumTable<>BEdit1.Text)or(Tab.Quests<>SEdit1.EditValue) then bEditH:=True;


    if bEdit or bEditH then
    begin
      fmMess:=tfmMess.Create(Application);
      fmMess.ShowModal;
      if fmMess.ModalResult=mrOk then
      begin
//        FormLog('SaveTab',IntToStr(Tab.Id)+' '+IntToStr(Tab.Id_Personal)+' '+Tab.NumTable);

        Tab.Summa:=rSum;

        if bEditH then SaveStatusTab(Tab.Id,Tab.iStatus,sEdit1.Value,BEdit1.Text);

        if bEdit then
        begin
          SaveSpecN;
          if TestSave=False then
          begin
            SaveSpecN;
            if TestSave=False then SaveSpecN;
          end;

//        fmMainCashRn.CreateViewPers;
//      quTabs.Refresh;
          CreateViewPers1;
        end;
//        Delay(100);
      end else FormLog('ExitNoSave',IntToStr(Tab.Id_Personal));

      fmMess.Release;
    end
    else //� ��������������� ������ ���, �� ������������ ������� ��� ����� ����.
    begin
      with dmC do
      begin
        prClearCur.ParamByName('ID_TAB').AsInteger:=Tab.Id;
        prClearCur.ParamByName('ID_STATION').AsInteger:=CommonSet.Station;
        prClearCur.ExecProc;
      end;
    end;
  end;
//  dsCheck.DataSet:=quCurSpec;
  ViewMod.EndUpdate;
  ViewSpec.EndUpdate;
end;

procedure TfmSpec.Action4Execute(Sender: TObject);
begin
//
end;

procedure TfmSpec.Action5Execute(Sender: TObject);
begin
//
end;

procedure TfmSpec.Action6Execute(Sender: TObject);
begin
//
end;

procedure TfmSpec.Action7Execute(Sender: TObject);
begin
//
end;

procedure TfmSpec.Action8Execute(Sender: TObject);
begin
//
end;

procedure TfmSpec.Action9Execute(Sender: TObject);
begin
//
end;

procedure TfmSpec.Action10Execute(Sender: TObject);
begin
  if bPrintPre then
  begin
    prWriteLog('---ExitFlagAfterPrePrint;'+IntToStr(Tab.Id)+';'+Tab.NumTable+';'+Person.Name+';'+IntToStr(Tab.Id_Personal)+';');;
    prWriteLog('');
    bCloseSpec:=True;  //����� ������ � ����� ��������� ������ �����
    exit;
  end;

  if bPrintCheck then
  begin //���� ������, � ����� ���� ����� ����� - ������ ���������
//    prWriteLog('---ExitFlagAfterRasch;'+IntToStr(Tab.Id)+';'+Tab.NumTable+';'+Person.Name+';'+IntToStr(Tab.Id_Personal)+';');;
//    prWriteLog('');
//    bCloseSpec:=True;  //����� ������ � ����� ��������� ������ �����
    exit;
  end;

  if bSaveSpec then exit; //�� �������� �������
  bSaveSpec:=True;

  prWriteLog('---Exit;'+IntToStr(Tab.Id)+';'+Tab.NumTable+';'+Person.Name+';'+IntToStr(Tab.Id_Personal)+';');;
  prWriteLog('');
  if Tab.iStatus=0 then
  begin
    acExitN.Execute; //��� ��� ���� c ������� ������ ����� � �.�.
  end
  else //������� ������������ ��� ����� ����
  begin
    with dmC do
    begin
      prClearCur.ParamByName('ID_TAB').AsInteger:=Tab.Id;
      prClearCur.ParamByName('ID_STATION').AsInteger:=CommonSet.Station;
      prClearCur.ExecProc;
    end;
  end;
  bSaveSpec:=False;
  close;
end;

procedure TfmSpec.acAddModExecute(Sender: TObject);
begin
  with dmC do
  begin
    if quCurSpec.Eof then exit;

    if Tab.iStatus>0 then
    begin
      Label18.Caption:='������ ������ �� ��������� ��������������.';
      exit;
    end;
    if quCurSpecIStatus.AsInteger=1 then
    begin
      Label18.Caption:='����� ��������, ��������� ������������ �����������.';
      exit; //������������� ����� ������ ������������� �������
    end;
    if quCurSpecLinkM.AsInteger=0 then
    begin
      Label18.Caption:='����� ��� �������������.';
      exit;
    end;
  end;
  acModify.Execute;
  GridSpec.SetFocus;
end;

procedure TfmSpec.acDelModExecute(Sender: TObject);
begin
  with dmC do
  begin
    if quCurSpec.Eof then exit;
    with dmC do
    begin
      if not quCurMod.Eof then
      begin
        if quCurSpecIStatus.AsInteger=0 then quCurMod.Delete
        else Label18.Caption:='����� ��������, ��������� ������������ �����������.';
      end;
    end;  
  end;
end;

procedure TfmSpec.Button2Click(Sender: TObject);
//Var StrWk:String;
begin
{  DevPrint.DeviceName:='COM1';
  DevPrint.Open;

  SelFont(1);
  StrWk:='����� ������ 1';
  PrintStr(StrWk);

  SelFont(2);
  StrWk:='����� ������ 2';
  PrintStr(StrWk);

  SelFont(3);
  StrWk:='����� ������ 3';
  PrintStr(StrWk);

  SelFont(0);
  StrWk:='����� ������   0';
  PrintStr(StrWk);
  SelFont(0);
  StrWk:='����� ������   0';
  PrintStr(StrWk);
  SelFont(0);
  StrWk:='����� ������   0';
  PrintStr(StrWk);
  SelFont(0);
  StrWk:='����� ������   0';
  PrintStr(StrWk);
  SelFont(0);
  StrWk:='����� ������   0';
  PrintStr(StrWk);
  SelFont(0);
  StrWk:='����� ������   0';
  PrintStr(StrWk);
  SelFont(0);
  StrWk:='����� ������   0';
  PrintStr(StrWk);
  SelFont(0);
  StrWk:='����� ������   0';
  PrintStr(StrWk);
  SelFont(0);
  StrWk:='����� ������   0';
  PrintStr(StrWk);
  SelFont(0);
  StrWk:='����� ������   0';
  PrintStr(StrWk);
  SelFont(0);
  StrWk:='����� ������   0';
  PrintStr(StrWk);
  SelFont(0);
  StrWk:='����� ������   0';
  PrintStr(StrWk);
  SelFont(0);
  StrWk:='����� ������   0';
  PrintStr(StrWk);
  SelFont(0);
  StrWk:='����� ������   0';
  PrintStr(StrWk);
  SelFont(0);
  StrWk:='����� ������   0';
  PrintStr(StrWk);
  SelFont(0);
  StrWk:='����� ������   0';
  PrintStr(StrWk);
  SelFont(0);
  StrWk:='����� ������   0';
  PrintStr(StrWk);
  SelFont(0);
  StrWk:='����� ������   0';
  PrintStr(StrWk);
  SelFont(0);
  StrWk:='����� ������   0';
  PrintStr(StrWk);
  SelFont(0);
  StrWk:='����� ������   0';
  PrintStr(StrWk);
  SelFont(0);
  StrWk:='����� ������   0';
  PrintStr(StrWk);
  StrWk:='����� ������   0';
  PrintStr(StrWk);
  StrWk:='����� ������   0';
  PrintStr(StrWk);
  StrWk:='����� ������   0';
  PrintStr(StrWk);
  StrWk:='����� ������   0';
  PrintStr(StrWk);
  StrWk:='����� ������   0';
  PrintStr(StrWk);
  StrWk:='����� ������   0';
  PrintStr(StrWk);
  StrWk:='����� ������   0';
  PrintStr(StrWk);
  StrWk:='����� ������   0';
  PrintStr(StrWk);
  StrWk:='����� ������   0';
  PrintStr(StrWk);
  StrWk:='����� ������   0';
  PrintStr(StrWk);
  StrWk:='����� ������   0';
  PrintStr(StrWk);
  StrWk:='����� ������   0';
  PrintStr(StrWk);
  StrWk:='����� ������   0';
  PrintStr(StrWk);
  StrWk:='����� ������   0';
  PrintStr(StrWk);
  StrWk:='����� ������   0';
  PrintStr(StrWk);
  StrWk:='����� ������   0';
  PrintStr(StrWk);
  StrWk:='����� ������   0';
  PrintStr(StrWk);
  StrWk:='����� ������   0';
  PrintStr(StrWk);
  StrWk:='����� ������   0';
  PrintStr(StrWk);
  StrWk:='����� ������   0';
  PrintStr(StrWk);

  StrWk:='';
  PrintStr(StrWk);
  StrWk:='';
  PrintStr(StrWk);
  StrWk:='';
  PrintStr(StrWk);
  StrWk:='';
  PrintStr(StrWk);
  StrWk:='';
  PrintStr(StrWk);
  CutDocPr;

  delay(300);
  DevPrint.Close;}
end;

procedure TfmSpec.acKodExecute(Sender: TObject);
Var sKod:String;
begin
//
  fmCalc.CalcEdit1.EditValue:=0;
  fmCalc.ShowModal;
  if (fmCalc.ModalResult=mrOk)and(Trunc(fmCalc.CalcEdit1.EditValue)>0)  then
  begin
    sKod:=IntToStr(Trunc(fmCalc.CalcEdit1.EditValue));
    with dmC do
    begin
           //����� �� ����
      quMenuSel.Active:=False;
      quMenuSel.SelectSQL.Clear;
      quMenuSel.SelectSQL.Add('SELECT SIFR,NAME,PARENT,PRICE,CODE,LIMITPRICE,LINK,STREAM,BARCODE,PARENT');
      quMenuSel.SelectSQL.Add('FROM MENU');

      if (Length(sKod)<=6) then
      quMenuSel.SelectSQL.Add('WHERE SIFR='+sKod);
      if Length(sKod)=13 then
      quMenuSel.SelectSQL.Add('WHERE BARCODE='''+sKod+'''');
      quMenuSel.Active:=True;

      if quMenuSel.RecordCount>0 then
      begin
        CalcDiscontN(quMenuSIFR.AsInteger,Check.Max+1,quMenuSelPARENT.AsInteger,quMenuSelSTREAM.AsInteger,quMenuSelPRICE.AsFloat,1,0,Tab.DBar,Now,Check.DProc,Check.DSum);

        quCurSpec.Append;
        quCurSpecSTATION.AsInteger:=CommonSet.Station;
        quCurSpecID_TAB.AsInteger:=Tab.Id;
        quCurSpecId_Personal.AsInteger:=Person.Id;
        quCurSpecNumTable.AsString:=Tab.NumTable;
        quCurSpecId.AsInteger:=Check.Max+1;
        quCurSpecSifr.AsInteger:=quMenuSelSIFR.AsInteger;
        quCurSpecPrice.AsFloat:=quMenuSelPRICE.AsFloat;
        quCurSpecQuantity.AsFloat:=1;
        quCurSpecSumma.AsFloat:=roundEx(quMenuSelPRICE.AsFloat*100)/100-Check.DSum;
        quCurSpecDProc.AsFloat:=Check.DProc;
        quCurSpecDSum.AsFloat:=Check.DSum;
        quCurSpecIStatus.AsInteger:=0;
        quCurSpecName.AsString:=quMenuSelNAME.AsString;
        quCurSpecCode.AsString:=quMenuSelCODE.AsString;
        quCurSpecLinkM.AsInteger:=quMenuSelLINK.AsInteger;
        if quMenuSelLIMITPRICE.AsFloat>0 then
        quCurSpecLimitM.AsInteger:=RoundEx(quMenuSelLIMITPRICE.AsFloat)
        else quCurSpecLimitM.AsInteger:=99;
        quCurSpecStream.AsInteger:=quMenuSelSTREAM.AsInteger;
        quCurSpec.Post;
        inc(Check.Max);

        if quMenuSelLINK.AsInteger>0 then
        begin //������������
          acModify.Execute;
        end;
        GridSpec.SetFocus;
      end
        else Label18.Caption:='����� �� K��� (��) �� �������.';
    end;
  end;
end;

procedure TfmSpec.cxButton7Click(Sender: TObject);
begin
  if Tab.iStatus>0 then exit;
  prWriteLog('---Kod;'+IntToStr(Tab.Id)+';'+Tab.NumTable+';'+Person.Name+';'+IntToStr(Tab.Id_Personal)+';');
  acKod.Execute;
  GridSpec.SetFocus;
end;

procedure TfmSpec.acDiscountExecute(Sender: TObject);
begin
  Edit1.SetFocus;
  DiscountBar:=';';
end;

procedure TfmSpec.ViewSpecFocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord;
  ANewItemRecordFocusingChanged: Boolean);
begin
  with dmC do
  begin
    quCurMod.Active:=False;
    quCurMod.ParamByName('IDT').AsInteger:=quCurSpecID_TAB.AsInteger;
    quCurMod.ParamByName('IDP').AsInteger:=quCurSpecID.AsInteger;
    quCurMod.ParamByName('STATION').AsInteger:=CommonSet.Station;
    quCurMod.Active:=True;
  end;
end;

procedure TfmSpec.cxButton8Click(Sender: TObject);
begin
  bSaveSpec:=False; //�� �������� ������� ��������
  with dmC do
  begin
    if not quCurSpec.Bof then quCurSpec.Prior;
  end;
end;

procedure TfmSpec.cxButton9Click(Sender: TObject);
begin
  bSaveSpec:=False; //�� �������� ������� ��������
  with dmC do
  begin
    if not quCurSpec.Eof then
    begin
      quCurSpec.Next;
      if quCurSpec.Eof then
      begin
        quCurSpec.Prior;
        quCurSpec.Next;
      end;
    end;
  end;
end;

procedure TfmSpec.cxButton10Click(Sender: TObject);
Var StrWk:String;
begin
  if Tab.iStatus>0 then
  begin
    Label18.Caption:='����� � ������� ��������. �������������� ���������.';
    exit;
  end;
  with dmC do
  begin
    if quCurSpec.RecordCount>0 then
    begin
      fmDiscount_Shape.cxTextEdit1.Text:='';
      fmDiscount_Shape.Caption:='��������� ���������� �����';
      fmDiscount_Shape.Label1.Caption:='��������� ���������� �����';
      fmDiscount_Shape.ShowModal;
      if fmDiscount_Shape.ModalResult=mrOk then
      begin
        //������� ������� ������
//      showmessage('');

        if fmDiscount_Shape.cxTextEdit1.Text>'' then
        begin
          DiscountBar:=SOnlyDigit(fmDiscount_Shape.cxTextEdit1.Text);
          WriteHistory('��:"'+DiscountBar+'"');

          If FindDiscount(DiscountBar) then
          begin
            // ��������� ������
            Str(Tab.DPercent:5:2,StrWk);
            Label17.Caption:='������������ ������ - '+ Tab.DName+' ('+StrWk+'%)';
            //������ ������ �� ��� �������
            ViewSpec.BeginUpdate;
            quCurSpec.First;
            While not quCurSpec.Eof do
            begin
              CalcDiscontN(quCurSpecSifr.AsInteger,quCurSpecId.AsInteger,-1,quCurSpecSTREAM.AsInteger,quCurSpecPrice.AsFloat,quCurSpecQuantity.AsFloat,0,Tab.DBar,Now,Check.DProc,Check.DSum);
//              if Check.DSum<>0 then //�������� ������� ������
//              begin
                quCurSpec.Edit;
                quCurSpecSumma.AsFloat:=RoundEx(quCurSpecPrice.AsFloat*quCurSpecQuantity.AsFloat*100)/100-Check.DSum;
                quCurSpecDProc.AsFloat:=Check.DProc;
                quCurSpecDSum.AsFloat:=Check.DSum;
                quCurSpec.Post;

                ViewSpecDPROC.Visible:=True;
                ViewSpecDSum.Visible:=True;
//              end;
              quCurSpec.Next;
            end;
            quCurSpec.First;
            ViewSpec.EndUpdate;
          end;
        end else //������ �������� - ��� ������ ������
        begin
            //������ ������ �� ��� �������
          DiscountBar:='';
          Check.DProc:=0;
          Check.DSum:=0;
          Tab.DBar:='';
          Tab.DPercent:=0;
          Tab.DName:='';
          Label17.Caption:='';

          ViewSpec.BeginUpdate;
          quCurSpec.First;
          While not quCurSpec.Eof do
          begin
            quCurSpec.Edit;
            quCurSpecSumma.AsFloat:=RoundEx(quCurSpecPrice.AsFloat*quCurSpecQuantity.AsFloat*100)/100;
            quCurSpecDProc.AsFloat:=0;
            quCurSpecDSum.AsFloat:=0;
            quCurSpec.Post;

            quCurSpec.Next;
          end;
          //�������

          Label7.Caption:='';
          ViewSpecDPROC.Visible:=False;
          ViewSpecDSum.Visible:=False;

          quCurSpec.First;
          ViewSpec.EndUpdate;
        end;
      end;
    end;
  end;
end;

procedure TfmSpec.cxButton11Click(Sender: TObject);
begin
  SEdit1.EditValue:=SEdit1.EditValue+1;
end;

procedure TfmSpec.cxButton13Click(Sender: TObject);
begin
  if SEdit1.EditValue>1 then SEdit1.EditValue:=SEdit1.EditValue-1;
end;

procedure TfmSpec.acNumTabExecute(Sender: TObject);
Var bTab:Boolean;
    iCount:Integer;
begin
  fmCalc.CalcEdit1.Text:=BEdit1.Text;
  bTab:=False;
  while not bTab do
  begin
    fmCalc.ShowModal;
    if fmCalc.ModalResult=mrOk then
    begin
      with dmC do
      begin
        quTabExists.Active:=False;
        quTabExists.ParamByName('Id_Personal').AsInteger:=Tab.Id_Personal;
        quTabExists.ParamByName('NumTab').AsString:=fmCalc.CalcEdit1.Text;
        quTabExists.Active:=True;
        iCount:=quTabExistsCOUNT.AsInteger;
        if iCount>0 then showmessage('������ ����� ��� ���������. �������� ������.')
        else bTab:=true;
      end;
    end else
    begin
      bTab:=True;
      fmCalc.CalcEdit1.Text:=BEdit1.Text;
    end;
  end;
  BEdit1.Text:=fmCalc.CalcEdit1.Text;
end;

procedure TfmSpec.BEdit1PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  acNumTab.Execute;
end;

procedure TfmSpec.BEdit1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  acNumTab.Execute;
end;

procedure TfmSpec.BEdit1Click(Sender: TObject);
begin
  acNumTab.Execute;
end;

procedure TfmSpec.cxButton12Click(Sender: TObject);
Var bEdit,bEditH:Boolean;
    rSum,rSumD:Real;

    strWk,StrWk1:String;
    iRet,IdH,IdC:Integer;
    bCheckOk:Boolean;
    rDiscont,rDProc:Real;
    iSumPos,iSumTotal,iSumDisc,iSumIt:INteger;
    TabCh:TTab;
    iAvans,iSumA,iQ:INteger;

 procedure prClearCheck;
 begin
   fmAttention.Label1.Caption:='���� ��������� ���� � ��. ��������� ������������ ����!';
   fmAttention.Label2.Caption:='';
   fmAttention.Label3.Caption:='';

   fmAttention.ShowModal;
   Nums.iRet:=InspectSt(Nums.sRet);
   prWriteLog('~~AttentionShow;'+'���� ��������� ���� � ��. ��������� ������������ ����!');

   fmAttention.Label2.Caption:='����� ���������� ������ ������� "�����"';
   fmAttention.Label3.Caption:='��������� ������������ ��������� ��������.';

   prWriteLog('~~CheckCancel;');
   CheckCancel;
    //��������� �����
   bCheckOk:=False;
 end;

begin
//������
  with dmC do
  begin

    if (CommonSet.MustPrePrint=1)and(Tab.iStatus=0)then
    begin
      ShowMessage('�������� ������� ����.');
      exit;
    end;

    //�������� � ������� ������ �����

    ViewSpec.BeginUpdate;
    ViewMod.BeginUpdate;
    rSum:=0;
    rSumD:=0;
    bEdit:=False;
    bEditH:=False;

    quCurSpec.First;
    while not quCurSpec.Eof do
    begin
      rSum:=rSum+quCurSpecSumma.AsFloat;
      rSumD:=rSumD+quCurSpecDSum.AsFloat;
      if quCurSpecIStatus.AsInteger=0 then bEdit:=True;
      quCurSpec.Next;
    end;

    if rSum<>Tab.Summa then bEdit:=True;
    if Tab.DBar<>Tab.DBar1 then bEdit:=True;
    if (Tab.NumTable<>BEdit1.Text)or(Tab.Quests<>SEdit1.EditValue) then bEditH:=True;

    if bEdit or bEditH then
    begin
      Tab.Summa:=rSum;

//      if bEditH then SaveStatusTab(Tab.Id,Tab.iStatus,sEdit1.Value,BEdit1.Text);
      //������, ���� ���� ��������� ��� ����������� ������ ������
      SaveStatusTab(Tab.Id,Tab.iStatus,sEdit1.Value,BEdit1.Text);

      if bEdit then
      begin
        SaveSpecN;
        if TestSave=False then
        begin
          SaveSpecN;
          if TestSave=False then SaveSpecN;
        end;
      end;

      CreateViewPers1;
    end;

    ViewSpec.EndUpdate;
    ViewMod.EndUpdate;

   //��������, ������ ��� � ��������

    if More24H(iRet) then
    begin
      fmAttention.Label1.Caption:='������ ����� 24 �����. ���������� ������� �����.';
      prWriteLog('~~AttentionShow;'+'������ ����� 24 �����. ���������� ������� �����.');
      fmAttention.ShowModal;
      Exit;   //������� , �� ���� �� ������ �� ���������� ������ �� ����
    end;

    if not CanDo('prPrintCheck') then begin  exit;end;

    TabCh.Id_Personal:=Tab.Id_Personal;
    TabCh.Name:=Tab.Name;

    TabCh.OpenTime:=Tab.OpenTime;
    TabCh.NumTable:=Tab.NumTable;
    TabCh.Quests:=sEdit1.Value;
    TabCh.iStatus:=Tab.iStatus;
    TabCh.DBar:=Tab.DBar;
    TabCh.Id:=Tab.Id;

    //���� ��������� �������� �� ������ �������� iAvans
    // 1 ����� ������� ������ ����� ����  ---- ������������ ������
    // 2 ����� ������ = ����� ����  ----- TabCh.Summa=0
    // 3 ����� ������ < ����� ����  ----- �� ���������� ����� ���� �������� ���

    quCheck.Active:=False;
    quCheck.ParamByName('IDTAB').Value:=TabCh.Id;
    quCheck.Active:=True;

    rSum:=0; iSumA:=0;
    iAvans:=0;
    quCheck.First;
    while not quCheck.Eof do
    begin
      rSum:=rSum+quCheckSUMMA.AsFloat;
      if quCheckPRICE.AsFloat<0 then iSumA:=RoundEx(quCheckSUMMA.AsFloat*100);
      quCheck.Next;
    end;
    if iSumA<0 then //������ �� ���� � ������
    begin
      if rSum>0 then iAvans:=3; // 3 ����� ������ < ����� ����  ----- �� ���������� ����� ���� �������� ���
      if rSum=0 then iAvans:=2; // 2 ����� ������ = ����� ����
      if rSum<0 then iAvans:=1; // 1 ����� ������� ������ ����� ����  ---- ������������ ������
      if not CanDo('prAvansCheck') then
      begin
        fmAttention.Label1.Caption:='��� ���� ��� ������ � �������� !!!.';
        prWriteLog('~~AttentionShow;'+'��� ���� ��� ������ � �������� !!!.');
        fmAttention.ShowModal;

        exit;
      end;
    end;

    TabCh.Summa:=rSum;

    if TabCh.Summa<0.01 then
    begin
      if iAvans<2 then  //��������, ��� �������� ������
      begin
        fmAttention.Label1.Caption:='��� � ������� � ������������� ������ �������� ������ !!!.';
        prWriteLog('~~AttentionShow;'+'��� � ������� � ������������� ������ �������� ������ !!!.');
        fmAttention.ShowModal;
        Exit;   //������� , �� ���� �� ������ �� ���������� ������ �� ����
      end;
    end;

    if (Operation=1) and (iAvans>1) then
    begin
      fmAttention.Label1.Caption:='������ �������� ��� �������� (������)!!!.';
      prWriteLog('~~AttentionShow;'+'������ �������� ��� �������� (������)!!!.');
      fmAttention.ShowModal;
      Exit;   //������� , �� ���� �� ������ �� ���������� ������ �� ����
    end;

    taModif.Active:=True;

    if iAvans=2 then // 2 ����� ������ = ����� ����  ������� ������ ������������ ��������
    begin
      PosCh.Name:='';
      PosCh.Code:='';
      PosCh.Price:=0; //� ��������
      PosCh.Count:=0; //� �������
      PosCh.Sum:=0;

      try
        if CommonSet.PrePrintPort='fisprim' then
        begin
          OpenNFDoc;

          SelectF(13);PrintNFStr(' '+CommonSet.DepartName); PrintNFStr('');
          SelectF(14); PrintNFStr('       ������');
          SelectF(13); PrintNFStr('');
          PrintNFStr('��������: '+TabCh.Name);
          PrintNFStr('����: '+TabCh.NumTable);
          SelectF(13);PrintNFStr('������: '+IntToStr(TabCh.Quests));
          PrintNFStr('������: '+FormatDateTime('dd.mm.yyyy hh:nn:ss',TabCh.OpenTime));
          SelectF(13);StrWk:='                                          ';
          PrintNFStr(StrWk);
          SelectF(15);StrWk:=' ��������          ���-��   ����   ����� ';
          PrintNFStr(StrWk);
          SelectF(13);StrWk:='                                          ';
          PrintNFStr(StrWk);
          SelectF(15); PrintNFStr(' ');PrintNFStr(' ');

          iSumA:=iSumA*(-1);
          Str((rSum+iSumA/100):10:2,StrWk);
          StrWk:='����� ����� �����:    '+StrWk+' ���';
          PrintNFStr(StrWk); PrintNFStr(' ');PrintNFStr(' ');

          Str((iSumA/100):10:2,StrWk);
          StrWk:='������ ����� �� �����:'+StrWk+' ���';
          PrintNFStr(StrWk); PrintNFStr(' ');PrintNFStr(' ');

          StrWk:='����� �� �����       :      0,00 ���';
          PrintNFStr(StrWk); PrintNFStr(' ');

          SelectF(13);StrWk:='                                          ';
          PrintNFStr(StrWk);
          SelectF(11);
          PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');
          CloseNFDoc;
          CutDoc;
        end;
        if pos('DB',CommonSet.PrePrintPort)>0 then
        begin
        //��������� �������
          with dmC1 do
          begin
            quPrint.Active:=False;
            quPrint.Active:=True;
            iQ:=GetId1('PQH0'); //��� ���������� ���������� - ����������� � ����� ������

            PrintDBStr(iQ,0,CommonSet.PrePrintPort,' '+CommonSet.DepartName,13,0);
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,'',13,0);
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,'       ������',14,0);
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,'',13,0);
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,'��������: '+TabCh.Name,13,0);
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,'����: '+TabCh.NumTable,13,0);
            if Tab.DBar>'' then
            begin
              Str(Tab.DPercent:5:2,StrWk);
              PrintDBStr(iQ,0,CommonSet.PrePrintPort,'�����: '+Tab.DName+' ('+StrWk+'%)',13,0);
            end;
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,'������: '+IntToStr(TabCh.Quests),13,0);
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,'������: '+FormatDateTime('dd.mm.yyyy hh:nn:ss',TabCh.OpenTime),13,0);
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,'-',13,0);
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,' ��������      ���-��   ����   �����',15,0);
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,'-',13,0);
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,'',13,0);
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,'',13,0);


            iSumA:=iSumA*(-1);
            Str((rSum+iSumA/100):10:2,StrWk);
            StrWk:='����� ����� �����:    '+StrWk+' ���';
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,StrWk,15,0);
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,'',13,0);
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,'',13,0);

            Str((iSumA/100):10:2,StrWk);
            StrWk:='������ ����� �� �����:'+StrWk+' ���';
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,StrWk,15,0);
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,'',13,0);
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,'',13,0);

            StrWk:='����� �� �����       :      0,00 ���';
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,StrWk,15,0);
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,'',13,0);

            PrintDBStr(iQ,0,CommonSet.PrePrintPort,'-',13,0);

            GetId1('PQH'); //����������� ������� �� 1-�
            quPrint.Active:=False;
          end;
        end;

      finally

       //�������� � ������ ����� ��� ���������� ��������� - ��� �� �������

        prSaveToAll.ParamByName('ID_TAB').AsInteger:=TabCh.Id;
        prSaveToAll.ParamByName('OPERTYPE').AsString:='Sale';
        prSaveToAll.ParamByName('CHECKNUM').AsInteger:=0;
        prSaveToAll.ParamByName('SKLAD').AsInteger:=0; //�������� �����������
        prSaveToAll.ParamByName('STATION').AsInteger:=CommonSet.Station; //����� �������
        prSaveToAll.ExecProc;

         // ������ ����
        quDelTab.Active:=False;
        quDelTab.ParamByName('Id').AsInteger:=TabCh.Id;

        trDel.StartTransaction;
        quDelTab.Active:=True;
        trDel.Commit;
        //��� � cashsaile ������ �� ���� �.�. ����� =0
      end;

      fmMainCashRn.CreateViewPers(True);
      exit;
    end;

    //������ �������� ������  iAvans:=3; ����� ������ < ����� ����
    with fmCash do
    begin
      fmCash.Caption:='�������� ������';
      fmCash.Label5.Caption:='���. '+TabCh.Name+'  ���� - '+TabCh.NumTable;
      fmCash.Label6.Caption:='����� - '+Floattostr(TabCh.Summa)+' �.';
      fmCash.cEdit1.EditValue:=TabCh.Summa;
      fmCash.cEdit2.EditValue:=TabCh.Summa;
      fmCash.cEdit3.EditValue:=0;
      fmCash.cEdit2.SelectAll;

      if CommonSet.CashNum>0 then
      begin
        iRet:=InspectSt(StrWk);
        if iRet=0 then
        begin
          fmCash.Label7.Caption:='������ ���: ��� ��.';
        end
        else //��� ������  - ����� ���� ������ ��� ���������� ���
        begin
          fmCash.Label7.Caption:='������ ���: ������ ('+IntToStr(iRet)+') '+StrWk;
          fmMainCashRn.Label3.Caption:='������ ���: ������ ('+IntToStr(iRet)+')';

          fmAttention.Label1.Caption:=StrWk;
          prWriteLog('~~AttentionShow;'+StrWk);
          fmAttention.ShowModal;

          while TestStatus('InspectSt',sMessage)=False do
          begin
            fmAttention.Label1.Caption:=sMessage;
            prWriteLog('~~AttentionShow;'+sMessage);
            fmAttention.ShowModal;
            Exit;   //�������  �  ���� �� ������ �� ���������� ������ �� ����
          end;
        end;
        //���� ����� �� ���� ��� � �����

        fmCash.Label7.Caption:='������ ���: ��� ��.';
        fmMainCashRn.Label3.Caption:='������ ���: ��� ��.';
      end;
    end;

    fmCash.ShowModal;
    if fmCash.ModalResult=mrOk then
    begin
      if PrintQu then
      begin
        showmessage('������� �����, ��������� �������.');
        prWriteLog('������� �����, ��������� �������.');
        exit;
      end;

      try
        bCheckOk:=False;
//        bPrintCheck:=False; //��� �� 2-��� �������
        bPrintCheck:=True;
        PrintCheck:=True; //��� ���� � ������� ������� ��������� ������

      //������� ���
        if CommonSet.CashNum>0 then
        begin

       //�������� ������� �������� �����
          CashDriver;

          Case Operation of
          0: begin
//             prWriteLog('!!CheckStart;'+IntToStr((Nums.iCheckNum+1))+';'+CurPos.Articul+';'+CurPos.Bar+';'+FloatToStr(CurPos.Quant)+';'+FloatToStr(CurPos.Price)+';'+FloatToStr(CurPos.DSum)+';');
               prWriteLog('!!CheckStart;'+IntToStr((Nums.iCheckNum+1))+';'+IntToStr(Person.Id)+';'+Person.Name+';');
               CheckStart;
               while TestStatus('CheckStart',sMessage)=False do
               begin
                 fmAttention.Label1.Caption:=sMessage;
                 prWriteLog('~~AttentionShow;'+sMessage);
                 fmAttention.ShowModal;
                 Nums.iRet:=InspectSt(Nums.sRet);
               end;
             end;
          1: begin
               prWriteLog('!!CheckRetStart;'+IntToStr((Nums.iCheckNum+1))+';'+IntToStr(Person.Id)+';'+Person.Name+';');
               CheckRetStart;
               while TestStatus('CheckRetStart',sMessage)=False do
               begin
                 fmAttention.Label1.Caption:=sMessage;
                 prWriteLog('~~AttentionShow;'+sMessage);
                 fmAttention.ShowModal;
                 Nums.iRet:=InspectSt(Nums.sRet);
               end;
             end;
          end;


          //�������

          rDiscont:=0;
          if iAvans=0 then
          begin
            rSum:=0;
            quCheck.First;
            while not quCheck.Eof do
            begin      //��������� �������
              PosCh.Name:=quCheckNAME.AsString;
              PosCh.Code:=quCheckSIFR.AsString;
              PosCh.AddName:='';
              PosCh.Price:=RoundEx(quCheckPRICE.AsFloat*100);
              PosCh.Count:=RoundEx(quCheckQUANTITY.AsFloat*1000);
              PosCh.Sum:=RoundEx(quCheckPRICE.AsFloat*quCheckQUANTITY.AsFloat*100)/100;

              rDiscont:=rDiscont+quCheckDISCOUNTSUM.AsFloat;
              rSum:=rSum+quCheckSUMMA.AsFloat;

              quCheck.Next;
              if not quCheck.Eof then
              begin
                while quCheckITYPE.AsInteger=1 do
                begin  //������������
                  if quCheck.Eof then break;
                  if taModif.Locate('SIFR',quCheckSifr.AsInteger,[]) then
                    PosCh.AddName:=PosCh.AddName+'|   '+taModifNAME.AsString;
                  quCheck.Next;
                end;
                delete(PosCh.AddName,1,1);
              end;

              prWriteLog('~!CheckAddPos; � ���� '+IntToStr((Nums.iCheckNum+1))+'; ��� '+PosCh.Code+'; ���-�� '+FloatToStr(PosCh.Count)+'; ���� '+FloatToStr(PosCh.Price)+'; ����� '+FloatToStr(PosCh.Sum)+';');
              if PosCh.Sum>=0.01 then   //������� ������� �������� ������
              begin
                CheckAddPos(iSumPos);
                while TestStatus('CheckAddPos',sMessage)=False do
                begin
                  fmAttention.Label1.Caption:=sMessage;
                  prWriteLog('~~AttentionShow;'+sMessage);
                  fmAttention.ShowModal;
                  Nums.iRet:=InspectSt(Nums.sRet);
                end;
              end else prWriteLog('~!CheckAddPosBad; � ���� '+IntToStr((Nums.iCheckNum+1))+'; ��� '+PosCh.Code+'; ���-�� '+FloatToStr(PosCh.Count)+'; ���� '+FloatToStr(PosCh.Price)+'; ����� '+FloatToStr(PosCh.Sum)+';');
            end;
          end else //���� ���� ������ �� ��� ����� �������
          begin
            PosCh.Name:='� ������ �� �����';
            PosCh.Code:='';
            iSumA:=iSumA*(-1);
            Str((rSum+iSumA/100):10:2,StrWk);
            PosCh.AddName:='|����� ����� �����:    '+StrWk+'|������ ����� �� �����:';
            Str((iSumA/100):10:2,StrWk);
            PosCh.AddName:=PosCh.AddName+StrWk+'| ';
            PosCh.Price:=RoundEx(rSum*100); //� ��������
            PosCh.Count:=1000; //� �������
            PosCh.Sum:=RoundEx(rSum*100)/100;

            CheckAddPos(iSumPos);
            while TestStatus('CheckAddPos',sMessage)=False do
            begin
              fmAttention.Label1.Caption:=sMessage;
              prWriteLog('~~AttentionShow;'+sMessage);
              fmAttention.ShowModal;
              Nums.iRet:=InspectSt(Nums.sRet);
            end;
          end;

          //������������ ���������� - ������ ����, ������, ������;
          CheckTotal(iSumTotal);
          prWriteLog('!!AfterCheckTotal;'+IntToStr((Nums.iCheckNum+1))+';'+inttostr(iSumTotal)+';');
          while TestStatus('CheckTotal',sMessage)=False do
          begin
            fmAttention.Label1.Caption:=sMessage;
            prWriteLog('~~AttentionShow;'+sMessage);
            fmAttention.ShowModal;
            Nums.iRet:=InspectSt(Nums.sRet);
          end;

          iSumIt:=0; iSumDisc:=0;
          if rDiscont>0 then
          begin
            CheckDiscount(rDiscont,iSumDisc,iSumIt); //� ������ iSumIt=0 ������
            prWriteLog('!!AfterCheckDisc;'+IntToStr((Nums.iCheckNum+1))+';'+FloatToStr(rDiscont)+';'+IntToStr(iSumDisc)+';'+IntToStr(iSumIt)+';');
            while TestStatus('CheckDiscount',sMessage)=False do
            begin
              fmAttention.Label1.Caption:=sMessage;
              prWriteLog('~~AttentionShow;'+sMessage);
              fmAttention.ShowModal;
              Nums.iRet:=InspectSt(Nums.sRet);
            end;
          end;

//        ��������, ���� ���� ����������� - ��������� ���
        //�������� iSumTotal
          if iSumIt=0 then iSumIt:=iSumTotal-iSumDisc;
          if abs(rSum-(iSumIt/100))>0.01 then
//          if abs(rSum-(iSumIt/100))>10000000 then
            begin
            prWriteLog('!!AfterCheckRasch;'+IntToStr((Nums.iCheckNum+1))+'; ����. ����� '+FloatToStr(rSum)+'; ����. ����� '+INtToStr(iSumIt)+';');
            prClearCheck;
          end
          else
          begin
            if iCashType=0 then CheckRasch(0,RoundEx(fmCash.cEdit2.EditValue*100),'',iSumIt) //���
            else CheckRasch(1,RoundEx(fmCash.cEdit1.EditValue*100),'',iSumIt); //������

            prWriteLog('!!AfterCheckRasch;'+IntToStr((Nums.iCheckNum+1))+';'+FloatToStr(fmCash.cEdit2.EditValue)+';'+INtToStr(iSumIt)+';');
            while TestStatus('CheckRasch',sMessage)=False do
            begin
              fmAttention.Label1.Caption:=sMessage;
              prWriteLog('~~AttentionShow;'+sMessage);
              fmAttention.ShowModal;
              Nums.iRet:=InspectSt(Nums.sRet);
            end;

            prWriteLog('!!CheckClose;'+IntToStr((Nums.iCheckNum+1))+';');
            CheckClose;
            while TestStatus('CheckClose',sMessage)=False do
            begin
              fmAttention.Label1.Caption:=sMessage;
              prWriteLog('~~AttentionShow;'+sMessage);
              fmAttention.ShowModal;
              Nums.iRet:=InspectSt(Nums.sRet);
            end;

          //�������� ������ ����� ������
            iRet:=InspectSt(StrWk);
            if iRet=0 then
            begin
              fmMainCashRn.Label3.Caption:='������ ���: ��� ��.';
              if CashDate(StrWk) then fmMainCashRn.Label2.Caption:='�������� ����: '+StrWk;
              if GetNums then fmMainCashRn.Label4.Caption:='��� � '+Nums.CheckNum;
          //��� ����������� �������� CommonSet.CashChNum - ����� �������� ��� � ���
              WriteCheckNum;
            end
            else
            begin
              fmMainCashRn.Label3.Caption:='������ ���: ������ ('+IntToStr(iRet)+')';

              while TestStatus('InspectSt',sMessage)=False do
              begin
                fmAttention.Label1.Caption:=sMessage;
                prWriteLog('~~AttentionShow;'+sMessage);
                fmAttention.ShowModal;
                Nums.iRet:=InspectSt(Nums.sRet);
              end;
            end;

            bCheckOk:=True;
          end;
        end else //����� ������������, �� ��� �� �������� ����� .. ������� c ��������� ������
        begin
          if CommonSet.PrePrintPort<>'0' then
        //�� ����������� ��� STAR
          begin
            try
              prOpenDevPrint(CommonSet.PrePrintPort);

              SelFont(13);PrintStr(' '+CommonSet.DepartName); PrintStr('');

              SelFont(14); PrintStr('       ������');
              SelFont(13); PrintStr('');
              PrintStr('��������: '+TabCh.Name);
              PrintStr('����: '+TabCh.NumTable);
              SelFont(13);PrintStr('������: '+IntToStr(TabCh.Quests));
              PrintStr('������: '+FormatDateTime('dd.mm.yyyy hh:nn:ss',TabCh.OpenTime));

              SelFont(13);StrWk:='                                          ';
              PrintStr(StrWk);
              SelFont(15);StrWk:=' ��������      ���-��   ����   �����';
              PrintStr(StrWk);
              SelFont(13);StrWk:='                                          ';
              PrintStr(StrWk);

             //�������
              rDiscont:=0;

              taStreams.Active:=False;
              taStreams.Active:=True;
              taStreams.First;
              while not taStreams.Eof do
              begin
                rSum:=0;

                quCheck.First;
                while not quCheck.Eof do
                begin      //��������� �������

                  PosCh.Name:=quCheckNAME.AsString;
                  PosCh.Code:=quCheckCODE.AsString;
                  PosCh.AddName:='';
                  PosCh.Stream:=quCheckSTREAM.AsInteger;

                  if PosCh.Stream=taStreamsID.AsInteger then
                  begin
                    rDiscont:=rDiscont+quCheckDISCOUNTSUM.AsFloat;
                    rSum:=rSum+quCheckSUMMA.AsFloat;

                    StrWk:= Copy(PosCh.Name,1,18);
                    while Length(StrWk)<18 do StrWk:=StrWk+' ';
                    Str(quCheckQUANTITY.AsFloat:5:1,StrWk1);
                    StrWk:=StrWk+' '+StrWk1;
                    Str(quCheckPRICE.AsFloat:7:2,StrWk1);
                    StrWk:=StrWk+' '+StrWk1;
                    Str(quCheckSUMMA.AsFloat:8:2,StrWk1);
                    StrWk:=StrWk+' '+StrWk1+'�';
                    SelFont(15);
                    PrintStr(StrWk);


                    quCheck.Next;
                    if not quCheck.Eof then
                    begin
                      while quCheckITYPE.AsInteger=1 do
                      begin  //������������
                        if quCheck.Eof then break;
                        if taModif.Locate('SIFR',quCheckSifr.AsInteger,[]) then
                        begin
                          PrintStr('   '+Copy(taModifNAME.AsString,1,29));
                        end;
                        quCheck.Next;
                      end;
                    end;
                  end else
                  quCheck.Next;
                end;
                if rSum<>0 then
                begin
                  SelFont(13);
                  StrWk:='                    ';
                  PrintStr(StrWk);
                  //�������� ������� �� ������
                  Str(rSum:8:2,StrWk1);
                  StrWk:='����� �� '+taStreamsNAMESTREAM.AsString+'  '+StrWk1+'�';

                  SelFont(15);
                  PrintStr(StrWk);
                  PrintStr('');
                end;


                taStreams.Next;
              end;

              SelFont(13);
              StrWk:='                                          ';
              PrintStr(StrWk);

              SelFont(15);
              Str(TabCh.Summa:8:2,StrWk1);
              StrWk:=' �����                      '+StrWk1+' ���';
              PrintStr(StrWk);

              if rDiscont>0.02 then
              begin
                SelFont(15);
                PrintStr('');
                SelFont(10);
                rDProc:=RoundEx((rDiscont/(TabCh.Summa+rDiscont)*100)*100)/100;
                Str(rDProc:5:2,StrWk1);
                Str(rDiscont:8:2,StrWk);
//                StrWk:=' � �.�. ������ - '+StrWk1+'%  '+StrWk+'�.';
                StrWk:=' � �.�. ������ - '+StrWk+'�.';
                PrintStr(StrWk);
              end;
              CutDocPr;
            finally
              taStreams.Active:=False;
//              Delay(100);
              DevPrint.Close;
            end;
          end;
        end;
        taModif.Active:=False;

        if CommonSet.CashNum<=0 then bCheckOk:=True;
        if bCheckOk then
        begin
          inc(CommonSet.CashChNum); //����� �������� 1-��
          WriteCheckNum;

          prSaveToAll.ParamByName('ID_TAB').AsInteger:=TabCh.Id;

          Case Operation of
        0:begin
            if iCashType=0 then
            begin
              FormLog('Sale',IntToStr(Person.Id)+' '+quTabsID_Personal.AsString+' '+quTabsNUMTABLE.AsString);
              prSaveToAll.ParamByName('OPERTYPE').AsString:='Sale';
            end else
            begin
              FormLog('SaleBank',IntToStr(Person.Id)+' '+quTabsID_Personal.AsString+' '+quTabsNUMTABLE.AsString);
              prSaveToAll.ParamByName('OPERTYPE').AsString:='SaleBank';
            end;
          end;
        1:begin
            if iCashType=0 then
            begin
              FormLog('Ret',IntToStr(Person.Id)+' '+quTabsID_Personal.AsString+' '+quTabsNUMTABLE.AsString);
              prSaveToAll.ParamByName('OPERTYPE').AsString:='Ret';
            end else
            begin
              FormLog('RetBank',IntToStr(Person.Id)+' '+quTabsID_Personal.AsString+' '+quTabsNUMTABLE.AsString);
              prSaveToAll.ParamByName('OPERTYPE').AsString:='RetBank';
             end;
            end;
          end;

          if CommonSet.CashNum<=0 then
          begin
            fmMainCashRn.Label4.Caption:='����� �'+IntToStr(CommonSet.CashZ)+'  ��� � '+IntToStr(CommonSet.CashChNum);
            prSaveToAll.ParamByName('CHECKNUM').AsInteger:=CommonSet.CashChNum;
          end
          else prSaveToAll.ParamByName('CHECKNUM').AsInteger:=Nums.iCheckNum;

          prSaveToAll.ParamByName('SKLAD').AsInteger:=0; //�������� �����������
          prSaveToAll.ParamByName('STATION').AsInteger:=CommonSet.Station; //����� �������
          prSaveToAll.ExecProc;

          IdH:=prSaveToAll.ParamByName('ID_H').AsInteger;

         // ������ ����
          quDelTab.Active:=False;
          quDelTab.ParamByName('Id').AsInteger:=TabCh.Id;

          trDel.StartTransaction;
          quDelTab.Active:=True;
          trDel.Commit;

        //���� ���
          taCashSail.Active:=True;
//        trUpdCS.StartTransaction;

          idC:=GetId('CS');
          taCashSail.Append;
          taCashSailID.AsInteger:=IdC;
          taCashSailCASHNUM.AsInteger:=CommonSet.CashNum;
          if CommonSet.CashNum<0 then
          begin
            taCashSailZNUM.AsInteger:=CommonSet.CashZ;
            taCashSailCHECKNUM.AsInteger:=CommonSet.CashChNum;
          end
          else
          begin
            taCashSailZNUM.AsInteger:=Nums.ZNum;
            taCashSailCHECKNUM.AsInteger:=Nums.iCheckNum;
          end;

          taCashSailTAB_ID.AsInteger:=IdH;

          Case Operation of
          0: taCashSailTABSUM.AsFloat:=TabCh.Summa;
          1: taCashSailTABSUM.AsFloat:=(-1)*TabCh.Summa;
          end;

          taCashSailCLIENTSUM.AsFloat:=fmCash.cEdit2.EditValue;
          taCashSailCHDATE.AsDateTime:=now;
          taCashSailCASHERID.AsInteger:=Person.Id;
          taCashSailWAITERID.AsInteger:=TabCh.Id_Personal;
          taCashSailPAYTYPE.AsInteger:=iCashType;

          taCashSail.Post;

//        trUpdCS.Commit;
          taCashSail.Active:=False;
        end;
      finally
        bPrintCheck:=False; //��� �� �������� �������
        PrintCheck:=False; //��� ���� � ������� ������� ��������� ������

        prWriteLog('---CloseSpec ;'+IntToStr(Tab.Id)+';'+Tab.NumTable+';'+Person.Name+';'+IntToStr(Tab.Id_Personal)+';');;
        prWriteLog('');

        prClearCur.ParamByName('ID_TAB').AsInteger:=Tab.Id;
        prClearCur.ParamByName('ID_STATION').AsInteger:=CommonSet.Station;
        prClearCur.ExecProc;

        CreateViewPers1;
        Operation:=0;
        fmMainCashRn.Label5.Caption:='�������� - ������� ��������.';
        PrintCheck:=False; //��� ���� � ������� ������� ��������� ������

        close;
      end;
    end;

  end;
end;

end.
