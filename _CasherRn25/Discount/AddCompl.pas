unit AddCompl;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, cxGraphics, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxCurrencyEdit, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxControls, cxGridCustomView, cxGrid,
  cxLabel, ExtCtrls, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox, cxMaskEdit, cxCalendar, cxContainer, cxTextEdit,
  StdCtrls, ComCtrls, cxButtons, Placemnt, DBClient, cxButtonEdit,
  ActnList, XPStyleActnCtrls, ActnMan, cxSpinEdit, cxImageComboBox, cxMemo,
  cxGroupBox, cxRadioGroup, FR_DSet, FR_DBSet, FR_Class,
  pFIBDataSet, FIBDatabase, pFIBDatabase, cxCheckBox;

type
  TfmAddCompl = class(TForm)
    Panel2: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Label1: TLabel;
    Label5: TLabel;
    Label12: TLabel;
    Label15: TLabel;
    cxTextEdit1: TcxTextEdit;
    cxDateEdit1: TcxDateEdit;
    cxLookupComboBox1: TcxLookupComboBox;
    Panel3: TPanel;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    FormPlacement1: TFormPlacement;
    taSpec: TClientDataSet;
    dsSpec: TDataSource;
    taSpecNum: TIntegerField;
    taSpecIdGoods: TIntegerField;
    taSpecNameG: TStringField;
    taSpecIM: TIntegerField;
    taSpecSM: TStringField;
    taSpecQuant: TFloatField;
    taSpecPriceIn: TFloatField;
    taSpecSumIn: TFloatField;
    taSpecPriceUch: TFloatField;
    taSpecSumUch: TFloatField;
    cxLabel7: TcxLabel;
    cxLabel8: TcxLabel;
    cxLabel9: TcxLabel;
    amCom: TActionManager;
    acAddPos: TAction;
    taSpecKm: TFloatField;
    Label2: TLabel;
    taSpecTCard: TIntegerField;
    Panel5: TPanel;
    Panel4: TPanel;
    Memo1: TcxMemo;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    GridCom: TcxGrid;
    ViewCom: TcxGridDBTableView;
    ViewComNum: TcxGridDBColumn;
    ViewComIdGoods: TcxGridDBColumn;
    ViewComNameG: TcxGridDBColumn;
    ViewComTCard: TcxGridDBColumn;
    ViewComIM: TcxGridDBColumn;
    ViewComSM: TcxGridDBColumn;
    ViewComQuant: TcxGridDBColumn;
    ViewComPriceIn: TcxGridDBColumn;
    ViewComSumIn: TcxGridDBColumn;
    ViewComPriceUch: TcxGridDBColumn;
    ViewComSumUch: TcxGridDBColumn;
    LevelCom: TcxGridLevel;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    GridComBC: TcxGrid;
    ViewComBC: TcxGridDBTableView;
    ViewComBCID: TcxGridDBColumn;
    ViewComBCCODEB: TcxGridDBColumn;
    ViewComBCNAMEB: TcxGridDBColumn;
    ViewComBCQUANT: TcxGridDBColumn;
    ViewComBCIDCARD: TcxGridDBColumn;
    ViewComBCNAMEC: TcxGridDBColumn;
    ViewComBCSB: TcxGridDBColumn;
    ViewComBCQUANTC: TcxGridDBColumn;
    ViewComBCPRICEIN: TcxGridDBColumn;
    ViewComBCSUMIN: TcxGridDBColumn;
    ViewComBCIM: TcxGridDBColumn;
    ViewComBCSM: TcxGridDBColumn;
    LevelComBC: TcxGridLevel;
    acSaveInv: TAction;
    Label3: TLabel;
    cxButton3: TcxButton;
    taSpecC: TClientDataSet;
    taSpecCNum: TIntegerField;
    taSpecCIdGoods: TIntegerField;
    taSpecCNameG: TStringField;
    taSpecCIM: TIntegerField;
    taSpecCSM: TStringField;
    taSpecCQuant: TFloatField;
    taSpecCPriceIn: TFloatField;
    taSpecCSumIn: TFloatField;
    taSpecCPriceUch: TFloatField;
    taSpecCSumUch: TFloatField;
    taSpecCQuantFact: TFloatField;
    taSpecCPriceInF: TFloatField;
    taSpecCSumInF: TFloatField;
    taSpecCPriceUchF: TFloatField;
    taSpecCSumUchF: TFloatField;
    taSpecCQuantDif: TFloatField;
    taSpecCSumInDif: TFloatField;
    taSpecCSumUchDif: TFloatField;
    taSpecCKm: TFloatField;
    taSpecCTCard: TIntegerField;
    taSpecCId_Group: TIntegerField;
    taSpecCNameGr: TStringField;
    dsSpecC: TDataSource;
    GridComC: TcxGrid;
    ViewComC: TcxGridDBTableView;
    LevelComC: TcxGridLevel;
    RepComplSpec: TfrReport;
    frdsSpec: TfrDBDataSet;
    acAddList: TAction;
    acDelPos: TAction;
    acDelAll: TAction;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    ViewComCNum: TcxGridDBColumn;
    ViewComCIdGoods: TcxGridDBColumn;
    ViewComCNameG: TcxGridDBColumn;
    ViewComCIM: TcxGridDBColumn;
    ViewComCSM: TcxGridDBColumn;
    ViewComCQuant: TcxGridDBColumn;
    ViewComCPriceIn: TcxGridDBColumn;
    ViewComCSumIn: TcxGridDBColumn;
    ViewComCQuantFact: TcxGridDBColumn;
    ViewComCPriceInF: TcxGridDBColumn;
    ViewComCSumInF: TcxGridDBColumn;
    ViewComCQuantDif: TcxGridDBColumn;
    ViewComCSumInDif: TcxGridDBColumn;
    ViewComCTCard: TcxGridDBColumn;
    ViewComCId_Group: TcxGridDBColumn;
    ViewComCNameGr: TcxGridDBColumn;
    frdsSpecC: TfrDBDataSet;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acAddPosExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxLabel1Click(Sender: TObject);
    procedure ViewComEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure ViewComEditKeyPress(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
    procedure cxLabel7Click(Sender: TObject);
    procedure ViewComDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure ViewComDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure cxLabel2Click(Sender: TObject);
    procedure cxLabel9Click(Sender: TObject);
    procedure acSaveInvExecute(Sender: TObject);
    procedure ViewComEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure Label3Click(Sender: TObject);
    procedure acAddListExecute(Sender: TObject);
    procedure acDelPosExecute(Sender: TObject);
    procedure acDelAllExecute(Sender: TObject);
    procedure Memo1DblClick(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxLookupComboBox1PropertiesChange(Sender: TObject);
    procedure taSpecQuantChange(Sender: TField);
    procedure taSpecPriceInChange(Sender: TField);
    procedure taSpecSumInChange(Sender: TField);
    procedure taSpecPriceUchChange(Sender: TField);
    procedure taSpecSumUchChange(Sender: TField);
  private
    { Private declarations }
  public
    { Public declarations }
    Procedure prCalcCompl(sBeg,sName:String;IdPos,iCode,iDate,iTCard:Integer;rQ:Real;IdM:INteger;taCalcB:TClientDataSet;Memo1:TcxMemo);
    Procedure VarsToZero;
    Procedure VarsToSpec;
  end;

var
  fmAddCompl: TfmAddCompl;
  bAdd:Boolean = False;
  iCol,iMax:INteger;
  Qr,Qf,Pr1,Pr11,Pr2,Pr22,Sum1,Sum11,Sum2,Sum21:Real;

implementation

uses Un1, dmOffice, FCards, Goods, DMOReps, DocInv, Message;

{$R *.dfm}

Procedure TfmAddCompl.VarsToZero;
begin
  Qr:=0;Qf:=0;Pr1:=0;Pr11:=0;Pr2:=0;Pr22:=0;Sum1:=0;Sum11:=0;Sum2:=0;Sum21:=0;
end;

Procedure TfmAddCompl.VarsToSpec;
begin
  Qr:=taSpecCQuant.AsFloat;
  Qf:=taSpecCQuantFact.AsFloat;
  Pr1:=taSpecCPriceIn.AsFloat;
  Pr11:=taSpecCPriceUch.AsFloat;
  Pr2:=taSpecCPriceInF.AsFloat;
  Pr22:=taSpecCPriceUchF.AsFloat;
  Sum1:=taSpecCSumIn.AsFloat;
  Sum11:=taSpecCSumUch.AsFloat;
  Sum2:=taSpecCSumInF.AsFloat;
  Sum21:=taSpecCSumUchF.AsFloat;
end;


Procedure TfmAddCompl.prCalcCompl(sBeg,sName:String;IdPos,iCode,iDate,iTCard:Integer;rQ:Real;IdM:INteger;taCalcB:TClientDataSet;Memo1:TcxMemo);
{Var iTC,iPCount:INteger;
    QT1:TpFIBDataSet;
    rQ1,kBrutto,kM,MassaB:Real;
    iMain:INteger;
    sM:String;
    Par:Variant;
    rQb:Real;
    NameGr:String;
    Id_Group:INteger;
}
begin
{  sBeg:=sBeg+'    ';

  with dmO do
  begin
    //���
    if iTCard=1 then
    begin
      iTC:=0; iPCount:=0; MassaB:=1;
      quFindTCard.Active:=False;
      quFindTCard.ParamByName('IDCARD').AsInteger:=iCode;
      quFindTCard.ParamByName('IDATE').AsDateTime:=iDate;
      quFindTCard.Active:=True;
      if quFindTCard.RecordCount>0 then
      begin
        iTC:=quFindTCardID.AsInteger;
        iPCount:=quFindTCardPCOUNT.AsInteger;
        MassaB:=quFindTCardPVES.AsFloat/1000;
        if prFindMT(IdM)=1 then MassaB:=1;
        prFindSM(IdM,sM,iMain); //��� �������� �������� ������� ���������
        Memo1.Lines.Add(sBeg+sName+' ('+INtToStr(iCode)+'). �� ����. ���-��: '+FloatToStr(RoundEx(rQ*1000)/1000)+' '+sM);
      end else
      begin
        inc(iErr);
        Memo1.Lines.Add(SBeg+'������: �� �� �������.')
      end;
      quFindTCard.Active:=False;
      if iTC>0 then
      begin
        QT1:=TpFIBDataSet.Create(Owner);
        QT1.Active:=False;
        QT1.Database:=OfficeRnDb;
        QT1.Transaction:=trSel;
        QT1.SelectSQL.Clear;
        QT1.SelectSQL.Add('SELECT CS.ID,CS.IDCARD,CS.CURMESSURE,CS.NETTO,CS.BRUTTO,CS.KNB,');
        QT1.SelectSQL.Add('CD.NAME,CD.TCARD');
        QT1.SelectSQL.Add('FROM OF_CARDSTSPEC CS');
        QT1.SelectSQL.Add('left join of_cards cd on cd.ID=CS.IDCARD');
        QT1.SelectSQL.Add('where IDC='+IntToStr(iCode)+' and IDT='+IntToStr(iTC));
        QT1.SelectSQL.Add('ORDER BY CS.ID');
        QT1.Active:=True;

        QT1.First;
        while not QT1.Eof do
        begin
          rQ1:=QT1.FieldByName('NETTO').AsFloat;
          kM:=prFindKM(QT1.FieldByName('CURMESSURE').AsInteger);
          rQ1:=rQ1*kM;//��������� � �������� �������
          prFindSM(QT1.FieldByName('CURMESSURE').AsInteger,sM,iMain); //��� �������� �������� ������� ���������
          //���� ������ �� ������ ����
          kBrutto:=prFindBrutto(QT1.FieldByName('IDCARD').AsInteger,iDate);
          rQ1:=rQ1*(100+kBrutto)/100; //��� ����� � ������
          rQ1:=rQ1/iPCount; //��� ����� �� 1-� ������ ���� �������� �� �������
          rQ1:=rQ1*rQ/MassaB; //��� ����� �� ��� ���-�� ������

//          Memo1.Lines.Add(sBeg+QT1.FieldByName('NAME').AsString+' ('+QT1.FieldByName('IDCARD').AsString+'). �� ����. ���-��: '+FloatToStr(RoundEx(rQ1*1000)/1000)+' '+sM);
          prCalcBlInv(sBeg,QT1.FieldByName('NAME').AsString,IdPos,QT1.FieldByName('IDCARD').AsInteger,iDate,QT1.FieldByName('TCARD').AsInteger,rQ1,iMain,dmORep.taCalcB,Memo1);

          QT1.Next;
        end;
        QT1.Active:=False;
        QT1.Free;
      end;
    end else
    begin
      //��������� � �������� ������� �� ������ ������ ����� ������ ��� - �� ����
      kM:=prFindKM(IdM);
      prFindSM(IdM,sM,iMain); //��� �������� �������� ������� ���������
      rQ:=rQ*kM;              //������ ������� - ��� ������ �� �����������

      Memo1.Lines.Add(sBeg+sName+' ('+IntToStr(iCode)+'). �����.  ���-��: '+FloatToStr(RoundEx(rQ*1000)/1000)+' '+sM);

      //������ ���� ��� � �������� � � ������
//      prCalcSave;
      par := VarArrayCreate([0,1], varInteger);
      par[0]:=IdPos;
      par[1]:=iCode;
      if taCalcB.Locate('ID;IDCARD',par,[]) then
      begin
        rQb:=taCalcB.fieldByName('QUANTC').AsFloat+rQ;
        taCalcB.edit;
        taCalcB.fieldByName('QUANTC').AsFloat:=rQb;
        taCalcB.fieldByName('PRICEIN').AsFloat:=0;
        taCalcB.fieldByName('SUMIN').AsFloat:=0;
        taCalcB.fieldByName('IM').AsInteger:=iMain;
        taCalcB.fieldByName('SM').AsString:=sM;
        taCalcB.fieldByName('SB').AsString:='';
        taCalcB.Post;
      end else
      begin
        taCalcB.Append;
        taCalcB.fieldByName('ID').AsInteger:=IdPos;
        taCalcB.fieldByName('CODEB').AsInteger:=taSpecIdGoods.AsInteger;
        taCalcB.fieldByName('NAMEB').AsString:=taSpecNameG.AsString;
        taCalcB.fieldByName('QUANT').AsFloat:=taSpecQuantFact.AsFloat;
        taCalcB.fieldByName('PRICEOUT').AsFloat:=0;
        taCalcB.fieldByName('SUMOUT').AsFloat:=0;
        taCalcB.fieldByName('IDCARD').AsInteger:=iCode;
        taCalcB.fieldByName('NAMEC').AsString:=sName;
        taCalcB.fieldByName('QUANTC').AsFloat:=rQ;
        taCalcB.fieldByName('PRICEIN').AsFloat:=0;
        taCalcB.fieldByName('SUMIN').AsFloat:=0;
        taCalcB.fieldByName('IM').AsInteger:=iMain;
        taCalcB.fieldByName('SM').AsString:=sM;
        taCalcB.fieldByName('SB').AsString:='';
        taCalcB.Post;
      end;

      if taSpecC.Locate('IdGoods',iCode,[]) then
      begin //�������������
        VarsToSpec;
        taSpecC.Edit;
      end else
      begin
        VarsToZero;
        taSpecC.Append;
        taSpecCNum.AsInteger:=iMax;
        inc(iMax);
      end;

      taSpecCIdGoods.AsInteger:=iCode;
      taSpecCNameG.AsString:=sName;
      taSpecCIM.AsInteger:=iMain;
      taSpecCSM.AsString:=sM;

      prFindGroup(iCode,NameGr,Id_Group); //�������� ������

      taSpecCId_Group.AsInteger:=Id_Group;
      taSpecCNameGr.AsString:=NameGr;

      taSpecCQuant.AsFloat:=0;
      taSpecCPriceIn.AsFloat:=0;
      taSpecCSumIn.AsFloat:=0;
      taSpecCPriceUch.AsFloat:=0;
      taSpecCSumUch.AsFloat:=0;

      taSpecCQuantFact.AsFloat:=rQ+Qf;
      taSpecCPriceInF.AsFloat:=0;
      taSpecCSumInF.AsFloat:=0;
      taSpecCPriceUchF.AsFloat:=0;
      taSpecCSumUchF.AsFloat:=0;
      taSpecCKm.AsFloat:=1;
      taSpecCTCard.AsInteger:=0;
      taSpecC.Post;

    end;
  end;}
end;



procedure TfmAddCompl.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  PageControl1.Align:=alClient;
  ViewCom.RestoreFromIniFile(CurDir+GridIni);
  ViewComC.RestoreFromIniFile(CurDir+GridIni);
  ViewComBC.RestoreFromIniFile(CurDir+GridIni);
end;

procedure TfmAddCompl.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  bAddSpecCompl:=False;
  ViewCom.StoreToIniFile(CurDir+GridIni,False);
  ViewComC.StoreToIniFile(CurDir+GridIni,False);
  ViewComBC.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmAddCompl.acAddPosExecute(Sender: TObject);
Var iMax:INteger;
begin
//�������� �������
  iMax:=1;
  ViewCom.BeginUpdate;

  taSpec.First;
  if not taSpec.Eof then
  begin
    taSpec.Last;
    iMax:=taSpecNum.AsInteger+1;
  end;

  taSpec.Append;
  taSpecNum.AsInteger:=iMax;
  taSpecIdGoods.AsInteger:=0;
  taSpecNameG.AsString:='';
  taSpecIM.AsInteger:=0;
  taSpecSM.AsString:='';
  taSpecQuant.AsFloat:=0;
  taSpecPriceIn.AsFloat:=0;
  taSpecSumIn.AsFloat:=0;
  taSpecPriceUch.AsFloat:=0;
  taSpecSumUch.AsFloat:=0;
  taSpecKm.AsFloat:=0;
  taSpecTCard.AsInteger:=0;
  taSpec.Post;
  ViewCom.EndUpdate;

  ViewComNameG.Options.Editing:=True;
  ViewComNameG.Focused:=True;
  
end;

procedure TfmAddCompl.FormShow(Sender: TObject);
begin
  Memo1.Clear;
  PageControl1.ActivePageIndex:=0;
  iCol:=0;
end;

procedure TfmAddCompl.cxLabel1Click(Sender: TObject);
begin
  acAddPos.Execute;
end;

procedure TfmAddCompl.ViewComEditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
Var iCode:Integer;
    Km:Real;
    sName:String;
begin
  with dmO do
  begin

    if (Key=$0D) then
    begin
      if ViewCom.Controller.FocusedColumn.Name='ViewComIdGoods' then
      begin
        iCode:=VarAsType(AEdit.EditingValue, varInteger);

        quFCard.Active:=False;
        quFCard.SelectSQL.Clear;
        quFCard.SelectSQL.Add('SELECT ID,NAME,IMESSURE,PARENT,TCARD,INDS');
        quFCard.SelectSQL.Add('FROM OF_CARDS');
        quFCard.SelectSQL.Add('where ID='+IntToStr(iCode));
        quFCard.Active:=True;

        if quFCard.RecordCount=1 then
        begin
          ViewCom.BeginUpdate;
          taSpec.Edit;
          taSpecIdGoods.AsInteger:=iCode;
          taSpecNameG.AsString:=quFCardNAME.AsString;
          taSpecIM.AsInteger:=quFCardIMESSURE.AsInteger;
          taSpecSM.AsString:=prFindKNM(quFCardIMESSURE.AsInteger,Km);
          taSpecKm.AsFloat:=Km;
          taSpecTCard.AsInteger:=quFCardTCARD.AsInteger;

          taSpecQuant.AsFloat:=0;
          taSpecPriceIn.AsFloat:=0;
          taSpecSumIn.AsFloat:=0;
          taSpecPriceUch.AsFloat:=0;
          taSpecSumUch.AsFloat:=0;

          taSpec.Post;

          ViewCom.EndUpdate;
        end;
      end;
      if ViewCom.Controller.FocusedColumn.Name='ViewComNameG' then
      begin
        sName:=VarAsType(AEdit.EditingValue, varString);

        quFCard.Active:=False;
        quFCard.SelectSQL.Clear;
        quFCard.SelectSQL.Add('SELECT ID,NAME,IMESSURE,PARENT,TCARD,INDS');
        quFCard.SelectSQL.Add('FROM OF_CARDS');
        quFCard.SelectSQL.Add('where UPPER(NAME) like ''%'+AnsiUpperCase(sName)+'%''');
        quFCard.SelectSQL.Add('Order by NAME');

        quFCard.Active:=True;

        bAdd:=True;
        //�������� ����� ������ � ����� ������
        fmFCards.ShowModal;
        if fmFCards.ModalResult=mrOk then
        begin
          if quFCard.RecordCount>0 then
          begin
            ViewCom.BeginUpdate;
            taSpec.Edit;
            taSpecIdGoods.AsInteger:=quFCardID.AsInteger;
            taSpecNameG.AsString:=quFCardNAME.AsString;
            taSpecIM.AsInteger:=quFCardIMESSURE.AsInteger;
            taSpecSM.AsString:=prFindKNM(quFCardIMESSURE.AsInteger,Km);
            taSpecKm.AsFloat:=Km;
            taSpecTCard.AsInteger:=quFCardTCARD.AsInteger;

            taSpecQuant.AsFloat:=0;
            taSpecPriceIn.AsFloat:=0;
            taSpecSumIn.AsFloat:=0;
            taSpecPriceUch.AsFloat:=0;
            taSpecSumUch.AsFloat:=0;

            taSpec.Post;
            ViewCom.EndUpdate;
            AEdit.SelectAll;
          end;
        end;
        bAdd:=False;
      end;
    end else
      if ViewCom.Controller.FocusedColumn.Name='ViewComIdGoods' then
        if fTestKey(Key)=False then
          if taSpec.State in [dsEdit,dsInsert] then taSpec.Cancel;
  end;
end;

procedure TfmAddCompl.ViewComEditKeyPress(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
Var Km:Real;
    sName:String;
begin
  if bAdd then exit;
  with dmO do
  begin
    if ViewCom.Controller.FocusedColumn.Name='ViewComNameG' then
    begin
      sName:=VarAsType(AEdit.EditingValue ,varString)+Key;
//      Label2.Caption:=sName;
      if sName>'' then
      begin
        quFCard.Active:=False;
        quFCard.SelectSQL.Clear;
        quFCard.SelectSQL.Add('SELECT ID,NAME,IMESSURE,PARENT,TCARD,INDS');
        quFCard.SelectSQL.Add('FROM OF_CARDS');
        quFCard.SelectSQL.Add('where UPPER(NAME) like ''%'+AnsiUpperCase(sName)+'%''');
        quFCard.SelectSQL.Add('Order by NAME');
        quFCard.Active:=True;

        if quFCard.RecordCount=1 then
        begin
          ViewCom.BeginUpdate;
          taSpec.Edit;
          taSpecIdGoods.AsInteger:=quFCardID.AsInteger;
          taSpecNameG.AsString:=quFCardNAME.AsString;
          taSpecIM.AsInteger:=quFCardIMESSURE.AsInteger;
          taSpecSM.AsString:=prFindKNM(quFCardIMESSURE.AsInteger,Km);
          taSpecKm.AsFloat:=Km;
          taSpecTCard.AsInteger:=quFCardTCARD.AsInteger;

          taSpecQuant.AsFloat:=0;
          taSpecPriceIn.AsFloat:=0;
          taSpecSumIn.AsFloat:=0;
          taSpecPriceUch.AsFloat:=0;
          taSpecSumUch.AsFloat:=0;

          taSpec.Post;
          ViewCom.EndUpdate;
          AEdit.SelectAll;
          ViewComNameG.Options.Editing:=False;
          ViewComNameG.Focused:=True;
          Key:=#0;
        end;
      end;
    end;
  end;//}
end;

procedure TfmAddCompl.cxLabel7Click(Sender: TObject);
begin
  acAddList.Execute;
end;

procedure TfmAddCompl.ViewComDragOver(Sender, Source: TObject; X, Y: Integer;
  State: TDragState; var Accept: Boolean);
begin
  Accept:=False;
  if bDCompl then  Accept:=True;
end;

procedure TfmAddCompl.ViewComDragDrop(Sender, Source: TObject; X,
  Y: Integer);
Var iCo:Integer;
    i,j: Integer;
    iNum: Integer;
    Rec:TcxCustomGridRecord;
    Km:Real;
    iMax:Integer;
begin
  if bDCompl then
  begin
    ResetAddVars;
    iCo:=fmGoods.ViewGoods.Controller.SelectedRecordCount;
    if iCo>1 then
    begin
      if MessageDlg('�� ������������� ������ �������� ��������� ������� ('+IntToStr(iCo)+' ��.) � ��������������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        with dmO do
        begin
          Memo1.Clear;
          Memo1.Lines.Add('����� .. ���� ���������� �������.');
          ViewCom.BeginUpdate;

          iMax:=1;
          taSpec.First;
          if not taSpec.Eof then
          begin
            taSpec.Last;
            iMax:=taSpecNum.AsInteger+1;
          end;

          for i:=0 to fmGoods.ViewGoods.Controller.SelectedRecordCount-1 do
          begin
            Rec:=fmGoods.ViewGoods.Controller.SelectedRecords[i];

            for j:=0 to Rec.ValueCount-1 do
            begin
              if fmGoods.ViewGoods.Columns[j].Name='ViewGoodsID' then break;
            end;

            iNum:=Rec.Values[j];
          //��� ��� - ����������
            with dmO do
            begin
              if quCardsSel.Locate('ID',iNum,[]) then
              begin
                try
                  if taSpec.Locate('IdGoods',iNum,[])=False then
                  begin
                    taSpec.Append;
                    taSpecNum.AsInteger:=iMax;
                    taSpecIdGoods.AsInteger:=quCardsSelID.AsInteger;
                    taSpecNameG.AsString:=quCardsSelNAME.AsString;
                    taSpecIM.AsInteger:=quCardsSelIMESSURE.AsInteger;
                    taSpecSM.AsString:=prFindKNM(quCardsSelIMESSURE.AsInteger,Km);
                    taSpecQuant.AsFloat:=0;
                    taSpecPriceIn.AsFloat:=0;
                    taSpecSumIn.AsFloat:=0;
                    taSpecPriceUch.AsFloat:=0;
                    taSpecSumUch.AsFloat:=0;
                    taSpecKm.AsFloat:=Km;
                    taSpecTCard.AsInteger:=quCardsSelTCard.AsInteger;
                    taSpec.Post;
                    delay(10);
                    inc(iMax);
                  end;
                except
                end;
              end;
            end;
          end;
          ViewCom.EndUpdate;
          Memo1.Lines.Add('���������� ��.');
        end;
      end;
    end;
  end;
end;

procedure TfmAddCompl.cxLabel2Click(Sender: TObject);
begin
  acDelPos.Execute;
end;

procedure TfmAddCompl.cxLabel9Click(Sender: TObject);
begin
  acDelAll.Execute;
end;

procedure TfmAddCompl.acSaveInvExecute(Sender: TObject);
Var Idh:Integer;
//    StrWk:String;
//    rQf,rQ,rSum:Real;
    iDate:Integer;
begin
  //��������� ������������
  Memo1.Clear;
  Memo1.Lines.Add('����� ... ���� ���������� ������.'); delay(10);

  iDate:=Trunc(date);
  if cxDateEdit1.Date>3000 then iDate:=Trunc(cxDateEdit1.Date);

  with dmO do
  with dmORep do
  begin
    if taSpec.State in [dsEdit,dsInsert] then taSpec.Post;

    //������� ������ ������
    ViewCom.BeginUpdate;
    ViewComC.BeginUpdate;
    ViewComBC.BeginUpdate;


    Memo1.Lines.Add('   �������� ������.'); delay(10);
    taSpecC.Active:=False; //����
    taSpecC.CreateDataSet;

    taCalcB.Active:=False; //���� �����
    taCalcB.CreateDataSet;

    iMax:=1;

    taSpec.First;
    while not taSpec.Eof do
    begin
{      if (taSpecTCard.AsInteger>0)and(taSpecNoCalc.AsInteger=0)  then
      begin  //���� �������������
//        showmessage('��������� ���� � ��������������, �������� ����������.');

        prCalcBlInv('    ',taSpecNameG.AsString,taSpecNum.AsInteger,taSpecIdGoods.AsInteger,iDate,taSpecTCard.AsInteger,taSpecQuantFact.AsFloat,taSpecIM.AsInteger,dmORep.taCalcB,Memo1);
      end else //��� ��� ������� �����
      begin

        if taSpecC.Locate('IdGoods',taSpecIdGoods.AsInteger,[]) then
        begin //�������������
          VarsToSpec;
          taSpecC.Edit;
        end else
        begin
          VarsToZero;
          taSpecC.Append;
          taSpecCNum.AsInteger:=iMax;
          inc(iMax);
        end;

        prFindSM(taSpecIM.AsInteger,StrWk,iM); //��������� ��� � �������� �� ���
        taSpecCIdGoods.AsInteger:=taSpecIdGoods.AsInteger;
        taSpecCNameG.AsString:=taSpecNameG.AsString;
        taSpecCIM.AsInteger:=iM;
        taSpecCSM.AsString:=StrWk;
        taSpecCQuant.AsFloat:=taSpecQuant.AsFloat*taSpecKm.AsFloat+Qr;
        taSpecCPriceIn.AsFloat:=0;
        taSpecCPriceUch.AsFloat:=0;
        taSpecCSumIn.AsFloat:=taSpecSumIn.AsFloat+Sum1;
        taSpecCSumUch.AsFloat:=taSpecSumUch.AsFloat+Sum11;
        if abs(taSpecQuant.AsFloat*taSpecKm.AsFloat+Qr)>0 then
        begin
          taSpecCPriceIn.AsFloat:=RoundEx((taSpecSumIn.AsFloat+Sum1)/(taSpecQuant.AsFloat*taSpecKm.AsFloat+Qr)*100)/100;
          taSpecCPriceUch.AsFloat:=RoundEx((taSpecSumUch.AsFloat+Sum11)/(taSpecQuant.AsFloat*taSpecKm.AsFloat+Qr)*100)/100;
        end;
        taSpecCQuantFact.AsFloat:=taSpecQuantFact.AsFloat*taSpecKm.AsFloat+Qf;
        taSpecCPriceInF.AsFloat:=0;
        taSpecCPriceUchF.AsFloat:=0;
        taSpecCSumInF.AsFloat:=taSpecSumInF.AsFloat+Sum2;
        taSpecCSumUchF.AsFloat:=taSpecSumUchF.AsFloat+Sum21;
        if abs(taSpecQuantFact.AsFloat*taSpecKm.AsFloat+Qf)>0 then
        begin
          taSpecCPriceInF.AsFloat:=RoundEx((taSpecSumInF.AsFloat+Sum2)/(taSpecQuantFact.AsFloat*taSpecKm.AsFloat+Qf)*100)/100;
          taSpecCPriceUchF.AsFloat:=RoundEx((taSpecSumUchF.AsFloat+Sum21)/(taSpecQuantFact.AsFloat*taSpecKm.AsFloat+Qf)*100)/100;
        end;
        taSpecCKm.AsFloat:=1;
        taSpecCTCard.AsInteger:=taSpecTCard.AsInteger;
        taSpecCId_Group.AsInteger:=taSpecId_Group.AsInteger;
        taSpecCNameGr.AsString:=taSpecNameGr.AsString;
        taSpecC.Post;
      end;}
      taSpec.Next;
    end;
    //������ �������������

    Memo1.Lines.Add('   �������� ����.'); delay(10);

    taSpecC.First;
    while not taSpecC.Eof do
    begin
{      rQ:=prCalcRemn(taSpecCIdGoods.AsInteger,Trunc(cxDateEdit1.Date),cxLookupComboBox1.EditValue);
      if abs(rQ)>0 then
      begin
        rSum:=prCalcRemnSum(taSpecCIdGoods.AsInteger,Trunc(cxDateEdit1.Date),cxLookupComboBox1.EditValue,rQ);
        rQ:=rQ*taSpecCKm.AsFloat; //�� �������� ����� � �������
//        rSum:=rSum*taSpecCKm.AsFloat; //����� � ������� ����� //�������� �.�. ����� ����� � ����� ��.���.
        taSpecC.Edit;
        taSpecCQuant.AsFloat:=rQ;
        taSpecCSumIn.AsFloat:=rSum;
        taSpecCSumUch.AsFloat:=rSum;
        taSpecCPriceIn.AsFloat:=RoundEx(rSum/rQ*100)/100;
        taSpecCPriceUch.AsFloat:=RoundEx(rSum/rQ*100)/100;
        if taSpecCSumInF.AsFloat=0 then
        begin
          taSpecCSumInF.AsFloat:=RoundEx(rSum/rQ*100*taSpecCQuantFact.AsFloat)/100;
          taSpecCPriceInF.AsFloat:=RoundEx(rSum/rQ*100)/100
        end;
        if taSpecCSumUchF.AsFloat=0 then
        begin
          taSpecCSumUchF.AsFloat:=RoundEx(rSum/rQ*100*taSpecCQuantFact.AsFloat)/100;
          taSpecCPriceUchF.AsFloat:=RoundEx(rSum/rQ*100)/100
        end;
        taSpecC.Post;
      end;}
      taSpecC.Next;
    end;

    //������������� ����
    taCalcB.First;
    while not taCalcB.Eof do
    begin
{      if taSpecC.Locate('IdGoods',taCalcBIDCARD.AsInteger,[]) then
      begin
        taCalcB.Edit;
        taCalcBPRICEIN.AsFloat:=taSpecCPriceIn.AsFloat;
        taCalcBSUMIN.AsFloat:=taCalcBQUANTC.AsFloat*taSpecCPriceIn.AsFloat;
        taCalcB.Post;
      end;}
      taCalcB.Next;
    end;

    //����������
    Memo1.Lines.Add('   ���������� ������ � �����.'); delay(10);

    IDH:=cxTextEdit1.Tag;
    if cxTextEdit1.Tag=0 then IDH:=GetId('HeadCompl');

    quDocsComlRec.Active:=False;

    quDocsComlRec.ParamByName('IDH').AsInteger:=IDH;
    quDocsComlRec.Active:=True;

    quDocsComlRec.First;
    if quDocsComlRec.RecordCount=0 then quDocsComlRec.Append else quDocsComlRec.Edit;

    quDocsComlRecID.AsInteger:=IDH;
    quDocsComlRecDATEDOC.AsDateTime:=iDate;
    quDocsComlRecNUMDOC.AsString:=cxTextEdit1.Text;
    quDocsComlRecIDSKL.AsInteger:=cxLookupComboBox1.EditValue;
    quDocsComlRecIACTIVE.AsInteger:=0;
    quDocsComlRecOPER.AsString:='';
    quDocsComlRecSUMIN.AsFloat:=0;
    quDocsComlRecSUMUCH.AsFloat:=0;
    quDocsComlRecSUMTAR.AsFloat:=0;
    quDocsComlRecPROCNAC.AsFloat:=0;
    quDocsComlRec.Post;

    cxTextEdit1.Tag:=IDH;



    //�������� ������������ ������ � �����
    quSpecCompl.Active:=False;
    quSpecCompl.ParamByName('IDH').AsInteger:=IDH;
    quSpecCompl.Active:=True;

    quSpecCompl.First;
    while not quSpecCompl.Eof do quSpecCompl.Delete;


    taSpec.First;
    while not taSpec.Eof do
    begin
      quSpecCompl.Append;
      quSpecComplIDHEAD.AsInteger:=IDH;
      quSpecComplID.AsInteger:=taSpecNum.AsInteger;
      quSpecComplIDCARD.AsInteger:=taSpecIdGoods.AsInteger;
      quSpecComplQUANT.AsFloat:=taSpecQuant.AsFloat;
      quSpecComplIDM.AsInteger:=taSpecIM.AsInteger;
      quSpecComplKM.AsFloat:=taSpecKm.AsFloat;
      quSpecComplPRICEIN.AsFloat:=taSpecPriceIn.AsFloat;
      quSpecComplSUMIN.AsFloat:=taSpecSumIn.AsFloat;
      quSpecComplPRICEINUCH.AsFloat:=taSpecPriceUch.AsFloat;
      quSpecComplSUMINUCH.AsFloat:=taSpecSumUch.AsFloat;
      quSpecComplTCARD.AsInteger:=taSpecTCard.AsInteger;
      quSpecCompl.Post;

      taSpec.Next; delay(10);
    end;

    quSpecCompl.Active:=False;

    Memo1.Lines.Add('   ���������� ������.'); delay(10);
   {
    //�������� ������������ ������
    quSpecComplC.Active:=False;
    quSpecComplC.ParamByName('IDH').AsInteger:=IDH;
    quSpecComplC.Active:=True;

    quSpecComplC.First;
    while not quSpecComplC.Eof do quSpecComplC.Delete;

    Sum1:=0;Sum11:=0;Sum2:=0;Sum21:=0;

    taSpecC.First;
    while not taSpecC.Eof do
    begin
      rQf:=taSpecCQuantFact.AsFloat;
      if abs(rQf)<0.00000001 then rQf:=0.000001;
      quSpecComplC.Append;
      quSpecComplCIDHEAD.AsInteger:=IDH;
      quSpecComplCID.AsInteger:=taSpecCNum.AsInteger;
      quSpecComplCNUM.AsInteger:=taSpecCNum.AsInteger;
      quSpecComplCIDCARD.AsInteger:=taSpecCIdGoods.AsInteger;
      quSpecComplCQUANT.AsFloat:=taSpecCQuant.AsFloat;
      quSpecComplCSUMIN.AsFloat:=taSpecCSumIn.AsFloat;
      quSpecComplCSUMUCH.AsFloat:=taSpecCSumUch.AsFloat;
      quSpecComplCIDMESSURE.AsInteger:=taSpecCIM.AsInteger;
      quSpecComplCKM.AsFloat:=taSpecCKm.AsFloat;
      quSpecComplCQUANTFACT.AsFloat:=rQf;
      quSpecComplCSUMINFACT.AsFloat:=taSpecCSumInF.AsFloat;
      quSpecComplCSUMUCHFACT.AsFloat:=taSpecCSumUchF.AsFloat;
      quSpecComplCTCARD.AsInteger:=taSpecCTCard.AsInteger;
      quSpecComplCIDGROUP.AsInteger:=taSpecCId_Group.AsInteger;
      quSpecComplC.Post;

      Sum1:=Sum1+taSpecCSumIn.AsFloat; Sum11:=Sum11+taSpecCSumUch.AsFloat;
      Sum2:=Sum2+taSpecCSumInF.AsFloat; Sum21:=Sum21+taSpecCSumUchF.AsFloat;

      taSpecC.Next; delay(10);
    end;
    quSpecComplC.Active:=False;

    Memo1.Lines.Add('   ���������� �����.'); delay(10);

    quSpecComplBC.Active:=False;
    quSpecComplBC.ParamByName('IDH').AsInteger:=IdH;
    quSpecComplBC.Active:=True;

    while not quSpecComplBC.Eof do quSpecComplBC.Delete;

    IdS:=1;
    taCalcB.First;
    while not taCalcB.Eof do
    begin
      quSpecComplBC.Append;
      quSpecComplBCIDHEAD.AsInteger:=Idh;
      quSpecComplBCIDB.AsInteger:=taCalcBID.AsInteger;
      quSpecComplBCID.AsInteger:=Ids; //��� ��� ������
      quSpecComplBCCODEB.AsInteger:=taCalcBCODEB.AsInteger;
      quSpecComplBCNAMEB.AsString:=taCalcBNAMEB.AsString;
      quSpecComplBCQUANT.AsFloat:=taCalcBQUANT.AsFloat;
      quSpecComplBCPRICEOUT.AsFloat:=taCalcBPRICEOUT.AsFloat;
      quSpecComplBCSUMOUT.AsFloat:=taCalcBSUMOUT.AsFloat;
      quSpecComplBCIDCARD.AsInteger:=taCalcBIDCARD.AsInteger;
      quSpecComplBCNAMEC.AsString:=taCalcBNAMEC.AsString;
      quSpecComplBCQUANTC.AsFloat:=taCalcBQUANTC.AsFloat;
      quSpecComplBCPRICEIN.AsFloat:=taCalcBPRICEIN.AsFloat;
      quSpecComplBCSUMIN.AsFloat:=taCalcBSUMIN.AsFloat;
      quSpecComplBCIM.AsInteger:=taCalcBIM.AsInteger;
      quSpecComplBCSM.AsString:=taCalcBSM.AsString;
      quSpecComplBCSB.AsString:=taCalcBSB.AsString;
      quSpecComplBC.Post;

      inc(Ids);
      taCalcB.Next;
    end;

    quSpecComplBC.Active:=False;

    quDocsComlRec.Edit;
    quDocsComlRecSUM1.AsFloat:=Sum1;
    quDocsComlRecSUM11.AsFloat:=Sum11;
    quDocsComlRecSUM2.AsFloat:=Sum2;
    quDocsComlRecSUM21.AsFloat:=Sum21;
    quDocsComlRec.Post;

    quDocsComlRec.Active:=False;

    fmDocsInv.ViewDocsInv.BeginUpdate;
    quDocsInvSel.FullRefresh;
    quDocsInvSel.Locate('ID',IDH,[]);
    fmDocsInv.ViewDocsInv.EndUpdate;
    }
    ViewCom.EndUpdate;
    ViewComC.EndUpdate;
    ViewComBC.EndUpdate;
  end;
  Memo1.Lines.Add('���������� ��.'); delay(10);
end;

procedure TfmAddCompl.ViewComEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  iCol:=0;
  if ViewCom.Controller.FocusedColumn.Name='ViewComQuant' then iCol:=1;
  if ViewCom.Controller.FocusedColumn.Name='ViewComPriceIn' then iCol:=2;
  if ViewCom.Controller.FocusedColumn.Name='ViewComSumIn' then iCol:=3;
  if ViewCom.Controller.FocusedColumn.Name='ViewComPriceUch' then iCol:=4;
  if ViewCom.Controller.FocusedColumn.Name='ViewComSumUch' then iCol:=5;
end;

procedure TfmAddCompl.Label3Click(Sender: TObject);
{Var rQ:Real;
    rSum:Real;}
begin //������ ��������
{  iCol:=0;
  with dmO do
  with dmORep do
  begin
    Memo1.Clear;
    Memo1.Lines.Add('����� ... ���� ������ ��������.'); delay(10);

    taSpec.First;
    while not taSpec.Eof do
    begin
      rQ:=prCalcRemn(taSpecIdGoods.AsInteger,Trunc(cxDateEdit1.Date),cxLookupComboBox1.EditValue);
      if abs(rQ)>0 then
      begin
        rSum:=prCalcRemnSum(taSpecIdGoods.AsInteger,Trunc(cxDateEdit1.Date),cxLookupComboBox1.EditValue,rQ);
        rQ:=rQ*taSpecKm.AsFloat; //�� �������� ����� � �������
//        rSum:=rSum*taSpecKm.AsFloat; //����� � ������� ����� //�������� �.�. ����� ����� � ����� ��.���.
        taSpec.Edit;
        taSpecQuant.AsFloat:=rQ;
        taSpecSumIn.AsFloat:=rSum;
        taSpecSumUch.AsFloat:=rSum;
        taSpecPriceIn.AsFloat:=RoundEx(rSum/rQ*100)/100;
        taSpecPriceUch.AsFloat:=RoundEx(rSum/rQ*100)/100;
        if taSpecSumInF.AsFloat=0 then
        begin
          taSpecSumInF.AsFloat:=RoundEx(rSum/rQ*100*taSpecQuantFact.AsFloat)/100;
          taSpecPriceInF.AsFloat:=RoundEx(rSum/rQ*100)/100
        end;
        if taSpecSumUchF.AsFloat=0 then
        begin
          taSpecSumUchF.AsFloat:=RoundEx(rSum/rQ*100*taSpecQuantFact.AsFloat)/100;
          taSpecPriceUchF.AsFloat:=RoundEx(rSum/rQ*100)/100
        end;
        taSpec.Post;
      end;
      taSpec.Next;
    end;
    Memo1.Lines.Add('������ ��.'); delay(10);
  end;}
end;

procedure TfmAddCompl.acAddListExecute(Sender: TObject);
begin
  bAddSpecCompl:=True;
  fmGoods.Show;
end;

procedure TfmAddCompl.acDelPosExecute(Sender: TObject);
begin
  if taSpec.RecordCount>0 then taSpec.Delete;
end;

procedure TfmAddCompl.acDelAllExecute(Sender: TObject);
begin
  if MessageDlg('�������� ��������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
    taSpec.First;
    while not taSpec.Eof do taSpec.Delete;
  end;
end;

procedure TfmAddCompl.Memo1DblClick(Sender: TObject);
begin
  fmMessage:=tfmMessage.Create(Application);
  fmMessage.Memo1.Lines:=Memo1.Lines;
  fmMessage.ShowModal;
  fmMessage.Release;
end;

procedure TfmAddCompl.cxButton2Click(Sender: TObject);
begin
  Close;
end;

procedure TfmAddCompl.cxLookupComboBox1PropertiesChange(Sender: TObject);
begin
  with dmO do
  begin
    CurVal.IdMH:=cxLookupComboBox1.EditValue;
    CurVal.NAMEMH:=cxLookupComboBox1.Text;
    quMHAll.FullRefresh;
    if quMHAll.Locate('ID',CurVal.IdMH,[]) then
    begin
      Label15.Caption:='��. ����: '+quMHAllNAMEPRICE.AsString;
      Label15.Tag:=quMHAllDEFPRICE.AsInteger;
    end else
    begin
      Label15.Caption:='';
      Label15.Tag:=0;
    end;  
  end;
end;

procedure TfmAddCompl.taSpecQuantChange(Sender: TField);
begin
  //���������� �����
  if iCol=1 then
  begin
    taSpecSumIn.AsCurrency:=taSpecPriceIn.AsCurrency*taSpecQuant.AsFloat;
    taSpecSumUch.AsCurrency:=taSpecPriceUch.AsCurrency*taSpecQuant.AsFloat;
  end;
end;

procedure TfmAddCompl.taSpecPriceInChange(Sender: TField);
begin
  //���������� ����
 if iCol=2 then
  begin
    taSpecSumIn.AsCurrency:=taSpecPriceIn.AsCurrency*taSpecQuant.AsFloat;
  end;
end;

procedure TfmAddCompl.taSpecSumInChange(Sender: TField);
begin
  if iCol=3 then  //���������� �����
  begin
    if abs(taSpecQuant.AsFloat)>0 then taSpecPriceIn.AsCurrency:=RoundEx(taSpecSumIn.AsCurrency/taSpecQuant.AsFloat*100)/100;
  end;
end;

procedure TfmAddCompl.taSpecPriceUchChange(Sender: TField);
begin
  //���������� ���� �������
  if iCol=4 then
  begin
    taSpecSumUch.AsCurrency:=taSpecPriceUch.AsCurrency*taSpecQuant.AsFloat;
  end;
end;

procedure TfmAddCompl.taSpecSumUchChange(Sender: TField);
begin
  if iCol=5 then  //���������� �����
  begin
    if abs(taSpecQuant.AsFloat)>0 then taSpecPriceUch.AsCurrency:=RoundEx(taSpecSumUch.AsCurrency/taSpecQuant.AsFloat*100)/100;
  end;
end;

end.
