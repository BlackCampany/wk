object fmFCards: TfmFCards
  Left = 354
  Top = 247
  BorderStyle = bsDialog
  Caption = #1042#1099#1073#1086#1088
  ClientHeight = 290
  ClientWidth = 425
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 344
    Top = 128
    Width = 32
    Height = 13
    Caption = 'Label1'
    Transparent = True
  end
  object GridFCards: TcxGrid
    Left = 0
    Top = 0
    Width = 329
    Height = 290
    Align = alLeft
    TabOrder = 0
    LookAndFeel.Kind = lfOffice11
    object ViewFCards: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dmO.dsFCard
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      object ViewFCardsID: TcxGridDBColumn
        Caption = #1050#1086#1076
        DataBinding.FieldName = 'ID'
        Styles.Content = dmO.cxStyle1
        Width = 52
      end
      object ViewFCardsNAME: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'NAME'
        Styles.Content = dmO.cxStyle25
        Width = 192
      end
      object ViewFCardsTCARD: TcxGridDBColumn
        Caption = #1058#1050
        DataBinding.FieldName = 'TCARD'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Images = dmO.imState
        Properties.Items = <
          item
            Description = #1085#1077#1090' '#1058#1050
            Value = 0
          end
          item
            Description = #1077#1089#1090#1100' '#1058#1050
            ImageIndex = 11
            Value = 1
          end>
        Width = 58
      end
    end
    object LevelFCards: TcxGridLevel
      GridView = ViewFCards
    end
  end
  object cxButton1: TcxButton
    Left = 336
    Top = 16
    Width = 75
    Height = 25
    Caption = 'Ok'
    Default = True
    ModalResult = 1
    TabOrder = 1
    OnClick = cxButton1Click
    LookAndFeel.Kind = lfOffice11
  end
  object cxButton2: TcxButton
    Left = 336
    Top = 56
    Width = 75
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1072
    TabOrder = 2
    OnClick = cxButton2Click
    LookAndFeel.Kind = lfOffice11
  end
  object ActionManager1: TActionManager
    Left = 128
    Top = 48
    StyleName = 'XP Style'
    object Action1: TAction
      Caption = 'Action1'
      ShortCut = 27
      OnExecute = Action1Execute
    end
  end
end
