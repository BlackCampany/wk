unit RepPCCli;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, Placemnt, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxControls, cxGridCustomView, cxGrid, SpeedBar, ExtCtrls, ComCtrls,
  cxCurrencyEdit,ComObj, ActiveX, Excel2000, OleServer, ExcelXP;

type
  TfmPCCli = class(TForm)
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem2: TSpeedItem;
    GridPCCli: TcxGrid;
    ViewPCCli: TcxGridDBTableView;
    LevelPCCli: TcxGridLevel;
    FormPlacement1: TFormPlacement;
    ViewPCCliDISCONT: TcxGridDBColumn;
    ViewPCCliDCNAME: TcxGridDBColumn;
    ViewPCCliSUM: TcxGridDBColumn;
    ViewPCCliSUM1: TcxGridDBColumn;
    ViewPCCliSITOG: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SpeedItem4Click(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;


var
  fmPCCli: TfmPCCli;

implementation

uses Un1, DmRnDisc, Period;

{$R *.dfm}

procedure TfmPCCli.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  GridPCCli.Align:=AlClient;
  ViewPCCli.RestoreFromIniFile(CurDir+GridIni);
end;

procedure TfmPCCli.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewPCCli.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmPCCli.SpeedItem4Click(Sender: TObject);
begin
  close;
end;

procedure TfmPCCli.SpeedItem3Click(Sender: TObject);
begin
  fmPeriod:=TfmPeriod.Create(Application);
  fmPeriod.DateTimePicker1.Date:=TrebSel.DateFrom;
  fmPeriod.DateTimePicker2.Date:=TrebSel.DateTo;
  fmPeriod.DateTimePicker3.Visible:=True;
  fmPeriod.DateTimePicker3.Time:=Frac(TrebSel.DateFrom);
  fmPeriod.DateTimePicker4.Visible:=True;
  fmPeriod.DateTimePicker4.Time:=Frac(TrebSel.DateTo);

  fmPeriod.cxCheckBox1.Visible:=False;

  fmPeriod.ShowModal;
  if fmPeriod.ModalResult=mrOk then
  begin
    with dmCDisc do
    begin
      fmPeriod.Release;

      ViewPCCli.BeginUpdate;
      quPCCli.Active:=False;
      quPCCli.ParamByName('DateB').AsDateTime:=TrebSel.DateFrom;
      quPCCli.ParamByName('DateE').AsDateTime:=TrebSel.DateTo;
      quPCCli.Active:=True;
      ViewPCCli.EndUpdate;

      fmPCCli.Caption:='���������� �� �� �� �������� �� ������ � '+FormatDateTime('dd.mm.yyyy hh:nn',TrebSel.DateFrom)+' �� '+FormatDateTime('dd.mm.yyyy hh:nn',TrebSel.DateTo);
      exit;
    end;
  end;
  fmPeriod.Release;
end;

procedure TfmPCCli.SpeedItem2Click(Sender: TObject);
begin
  with dmCDisc do prNExportExel4(ViewPCCli);
end;

end.
