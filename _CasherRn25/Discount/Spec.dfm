object fmSpec: TfmSpec
  Left = 289
  Top = 51
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = '\'
  ClientHeight = 636
  ClientWidth = 727
  Color = 9737289
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 8
    Width = 75
    Height = 13
    Caption = #1057#1086#1089#1090#1072#1074' '#1079#1072#1082#1072#1079#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 12189625
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object Label2: TLabel
    Left = 531
    Top = 24
    Width = 46
    Height = 13
    Caption = #1054#1090#1074'.'#1083#1080#1094#1086
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 16514043
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object Label3: TLabel
    Left = 531
    Top = 104
    Width = 45
    Height = 13
    Caption = #1047#1072#1082#1072#1079' '#8470
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 16514043
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object Label5: TLabel
    Left = 531
    Top = 48
    Width = 38
    Height = 13
    Caption = #1054#1090#1082#1088#1099#1090
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 16514043
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object Label6: TLabel
    Left = 531
    Top = 64
    Width = 34
    Height = 13
    Caption = #1057#1090#1072#1090#1091#1089
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 16514043
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object Label7: TLabel
    Left = 603
    Top = 24
    Width = 12
    Height = 13
    Caption = '....'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 16514043
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object Label10: TLabel
    Left = 603
    Top = 48
    Width = 12
    Height = 13
    Caption = '....'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 16514043
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object Label11: TLabel
    Left = 603
    Top = 64
    Width = 12
    Height = 13
    Caption = '....'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 16514043
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object Label12: TLabel
    Left = 587
    Top = 24
    Width = 3
    Height = 13
    Caption = ':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 16514043
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object Label15: TLabel
    Left = 587
    Top = 48
    Width = 3
    Height = 13
    Caption = ':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 16514043
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object Label16: TLabel
    Left = 587
    Top = 64
    Width = 3
    Height = 13
    Caption = ':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 16514043
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object Label17: TLabel
    Left = 152
    Top = 8
    Width = 370
    Height = 13
    Alignment = taRightJustify
    AutoSize = False
    Caption = '.....'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold, fsUnderline]
    ParentFont = False
    Transparent = True
  end
  object Label4: TLabel
    Left = 531
    Top = 160
    Width = 35
    Height = 13
    Caption = #1043#1086#1089#1090#1077#1081
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 16514043
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object Edit1: TcxTextEdit
    Left = 544
    Top = 352
    BeepOnEnter = False
    TabOrder = 13
    Width = 89
  end
  object GridSpec: TcxGrid
    Left = 16
    Top = 26
    Width = 505
    Height = 383
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    LookAndFeel.Kind = lfFlat
    object ViewSpec: TcxGridDBBandedTableView
      NavigatorButtons.ConfirmDelete = False
      FilterBox.CustomizeDialog = False
      OnFocusedRecordChanged = ViewSpecFocusedRecordChanged
      DataController.DataSource = dmC.dsCurSpec
      DataController.KeyFieldNames = 'ID'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SUMMA'
          Column = ViewSpecSUMMA
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnGrouping = False
      OptionsCustomize.ColumnSorting = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsSelection.UnselectFocusedRecordOnExit = False
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      Styles.Selection = dmC.cxStyle16
      Styles.Footer = dmC.cxStyle17
      Styles.Header = dmC.cxStyle17
      Styles.BandBackground = dmC.cxStyle17
      Styles.BandHeader = dmC.cxStyle17
      Bands = <
        item
          Width = 276
        end
        item
          Width = 207
        end>
      object ViewSpecCODE: TcxGridDBBandedColumn
        Caption = #1050#1086#1076
        DataBinding.FieldName = 'SIFR'
        Options.Editing = False
        Options.Sorting = False
        Width = 96
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 1
      end
      object ViewSpecNAME: TcxGridDBBandedColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'NAME'
        Options.Editing = False
        Options.Sorting = False
        Styles.Content = dmC.cxStyle18
        Width = 226
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object ViewSpecPRICE: TcxGridDBBandedColumn
        Caption = #1062#1077#1085#1072
        DataBinding.FieldName = 'PRICE'
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Options.Editing = False
        Options.Sorting = False
        Styles.Content = dmC.cxStyle18
        Width = 70
        Position.BandIndex = 1
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object ViewSpecQUANTITY: TcxGridDBBandedColumn
        Caption = #1050#1086#1083'-'#1074#1086
        DataBinding.FieldName = 'QUANTITY'
        PropertiesClassName = 'TcxCalcEditProperties'
        Options.Editing = False
        Styles.Content = dmC.cxStyle18
        Width = 61
        Position.BandIndex = 1
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object ViewSpecSUMMA: TcxGridDBBandedColumn
        Caption = #1057#1091#1084#1084#1072' '#1080#1090#1086#1075#1086
        DataBinding.FieldName = 'SUMMA'
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Options.Editing = False
        Styles.Content = dmC.cxStyle18
        Width = 83
        Position.BandIndex = 1
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object ViewSpecDPROC: TcxGridDBBandedColumn
        Caption = #1055#1088#1086#1094#1077#1085#1090' '#1089#1082#1080#1076#1082#1080
        DataBinding.FieldName = 'DPROC'
        Options.Editing = False
        Styles.Content = dmC.cxStyle12
        Position.BandIndex = 1
        Position.ColIndex = 0
        Position.RowIndex = 1
      end
      object ViewSpecDSUM: TcxGridDBBandedColumn
        Caption = #1057#1091#1084#1084#1072' '#1089#1082#1080#1076#1082#1080
        DataBinding.FieldName = 'DSUM'
        Options.Editing = False
        Styles.Content = dmC.cxStyle12
        Position.BandIndex = 1
        Position.ColIndex = 1
        Position.RowIndex = 1
      end
      object ViewSpecISTATUS: TcxGridDBBandedColumn
        Caption = #1057#1090#1072#1090#1091#1089
        DataBinding.FieldName = 'ISTATUS'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Images = dmC.imState
        Properties.Items = <
          item
            Description = #1074' '#1088#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1085#1080#1080
            ImageIndex = 2
            Value = 0
          end
          item
            Description = #1079#1072#1082#1072#1079#1072#1085#1086
            ImageIndex = 0
            Value = 1
          end>
        Options.Editing = False
        Width = 190
        Position.BandIndex = 0
        Position.ColIndex = 1
        Position.RowIndex = 1
      end
    end
    object LevelSpec: TcxGridLevel
      GridView = ViewSpec
    end
  end
  object Button1: TcxButton
    Left = 16
    Top = 544
    Width = 89
    Height = 60
    Caption = #1044#1086#1073#1072#1074#1080#1090#1100
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    TabStop = False
    OnClick = Button1Click
    Colors.Default = 11457198
    Colors.Normal = 11457198
    Colors.Hot = 7384944
    Colors.Pressed = 5411922
    Layout = blGlyphTop
    LookAndFeel.Kind = lfUltraFlat
  end
  object cxButton1: TcxButton
    Left = 208
    Top = 544
    Width = 89
    Height = 60
    Action = acChangeQuantity
    Caption = #1048#1079#1084'. '#1082#1086#1083'-'#1074#1086
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    TabStop = False
    Colors.Default = 11457198
    Colors.Normal = 11457198
    Colors.Hot = 7384944
    Colors.Pressed = 5411922
    Layout = blGlyphTop
    LookAndFeel.Kind = lfUltraFlat
  end
  object cxButton2: TcxButton
    Left = 432
    Top = 544
    Width = 89
    Height = 60
    Caption = #1059#1076#1083'.'#1041#1083#1102#1076#1086
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 3
    TabStop = False
    OnClick = cxButton2Click
    Colors.Default = 11457198
    Colors.Normal = 11457198
    Colors.Hot = 7384944
    Colors.Pressed = 5411922
    Layout = blGlyphTop
    LookAndFeel.Kind = lfUltraFlat
  end
  object Button5: TcxButton
    Left = 544
    Top = 335
    Width = 161
    Height = 42
    Action = acPrePrint
    Caption = #1057#1095#1077#1090
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 4
    TabStop = False
    Colors.Default = clWhite
    Colors.Normal = clWhite
    Colors.Hot = clMoneyGreen
    Colors.Pressed = clMoneyGreen
    Layout = blGlyphTop
    LookAndFeel.Kind = lfUltraFlat
  end
  object cxButton3: TcxButton
    Left = 536
    Top = 544
    Width = 177
    Height = 60
    Action = Action10
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 5
    TabStop = False
    Colors.Default = 11457198
    Colors.Normal = 11457198
    Colors.Hot = 7384944
    Colors.Pressed = 5411922
    Glyph.Data = {
      5E040000424D5E04000000000000360000002800000012000000130000000100
      18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
      CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
      8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
      0000CED3D6848684848684848684848684848684848684848684848684848684
      848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
      7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
      FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
      00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
      75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
      0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
      FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
      CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
      007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
      00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
      75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
      0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
      FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
      CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
      00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
      494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
      0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
      00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
      CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
      0000}
    Layout = blGlyphTop
    LookAndFeel.Kind = lfUltraFlat
  end
  object cxButton4: TcxButton
    Left = 304
    Top = 544
    Width = 89
    Height = 60
    Caption = #1055#1077#1088#1077#1085#1077#1089#1090#1080
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 6
    TabStop = False
    OnClick = cxButton4Click
    Colors.Default = 11457198
    Colors.Normal = 11457198
    Colors.Hot = 7384944
    Colors.Pressed = 5411922
    Layout = blGlyphTop
    LookAndFeel.Kind = lfUltraFlat
  end
  object GridMod: TcxGrid
    Left = 16
    Top = 424
    Width = 385
    Height = 105
    TabOrder = 7
    LookAndFeel.Kind = lfFlat
    object ViewMod: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dmC.dsCurMod
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnGrouping = False
      OptionsCustomize.ColumnMoving = False
      OptionsCustomize.ColumnSorting = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsSelection.UnselectFocusedRecordOnExit = False
      OptionsView.GroupByBox = False
      Styles.Header = dmC.cxStyle17
      object ViewModName: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'NAME'
        Styles.Content = dmC.cxStyle18
        Width = 322
      end
      object ViewModQuantity: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086
        DataBinding.FieldName = 'QUANTITY'
        Visible = False
        Styles.Content = dmC.cxStyle18
        Width = 74
      end
    end
    object LevelMod: TcxGridLevel
      GridView = ViewMod
    end
  end
  object cxButton5: TcxButton
    Left = 432
    Top = 424
    Width = 89
    Height = 49
    BiDiMode = bdRightToLeftNoAlign
    Caption = #1044#1086#1073'. '#1052#1086#1076#1080#1092'.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentBiDiMode = False
    ParentFont = False
    TabOrder = 8
    TabStop = False
    OnClick = cxButton5Click
    Colors.Default = 11457198
    Colors.Normal = 11457198
    Colors.Hot = 7384944
    Colors.Pressed = 5411922
    Layout = blGlyphTop
    LookAndFeel.Kind = lfUltraFlat
  end
  object cxButton6: TcxButton
    Left = 432
    Top = 480
    Width = 89
    Height = 49
    BiDiMode = bdRightToLeftNoAlign
    Caption = #1059#1076#1083'. '#1052#1086#1076#1080#1092'.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentBiDiMode = False
    ParentFont = False
    TabOrder = 9
    TabStop = False
    OnClick = cxButton6Click
    Colors.Default = 11457198
    Colors.Normal = 11457198
    Colors.Hot = 7384944
    Colors.Pressed = 5411922
    Layout = blGlyphTop
    LookAndFeel.Kind = lfUltraFlat
  end
  object Panel1: TPanel
    Left = 0
    Top = 609
    Width = 727
    Height = 27
    Align = alBottom
    BevelInner = bvLowered
    Color = 8881987
    TabOrder = 10
    object Label18: TLabel
      Left = 16
      Top = 8
      Width = 13
      Height = 13
      Caption = '...'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object Button2: TButton
    Left = 312
    Top = 520
    Width = 121
    Height = 25
    Caption = #1055#1088#1086#1073#1072' '#1087#1077#1095#1072#1090#1080
    TabOrder = 11
    Visible = False
    OnClick = Button2Click
  end
  object cxButton7: TcxButton
    Left = 112
    Top = 544
    Width = 89
    Height = 60
    Caption = #1055#1086' '#1082#1086#1076#1091
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 12
    TabStop = False
    OnClick = cxButton7Click
    Colors.Default = 11457198
    Colors.Normal = 11457198
    Colors.Hot = 7384944
    Colors.Pressed = 5411922
    Layout = blGlyphTop
    LookAndFeel.Kind = lfUltraFlat
  end
  object cxButton8: TcxButton
    Left = 544
    Top = 224
    Width = 65
    Height = 33
    BiDiMode = bdRightToLeftNoAlign
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentBiDiMode = False
    ParentFont = False
    TabOrder = 14
    TabStop = False
    OnClick = cxButton8Click
    Colors.Default = 11457198
    Colors.Normal = 11457198
    Colors.Hot = 7384944
    Colors.Pressed = 5411922
    Glyph.Data = {
      22090000424D2209000000000000360400002800000023000000230000000100
      080000000000EC04000000000000000000000001000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000C0DCC000F0CA
      A6000020400000206000002080000020A0000020C0000020E000004000000040
      20000040400000406000004080000040A0000040C0000040E000006000000060
      20000060400000606000006080000060A0000060C0000060E000008000000080
      20000080400000806000008080000080A0000080C0000080E00000A0000000A0
      200000A0400000A0600000A0800000A0A00000A0C00000A0E00000C0000000C0
      200000C0400000C0600000C0800000C0A00000C0C00000C0E00000E0000000E0
      200000E0400000E0600000E0800000E0A00000E0C00000E0E000400000004000
      20004000400040006000400080004000A0004000C0004000E000402000004020
      20004020400040206000402080004020A0004020C0004020E000404000004040
      20004040400040406000404080004040A0004040C0004040E000406000004060
      20004060400040606000406080004060A0004060C0004060E000408000004080
      20004080400040806000408080004080A0004080C0004080E00040A0000040A0
      200040A0400040A0600040A0800040A0A00040A0C00040A0E00040C0000040C0
      200040C0400040C0600040C0800040C0A00040C0C00040C0E00040E0000040E0
      200040E0400040E0600040E0800040E0A00040E0C00040E0E000800000008000
      20008000400080006000800080008000A0008000C0008000E000802000008020
      20008020400080206000802080008020A0008020C0008020E000804000008040
      20008040400080406000804080008040A0008040C0008040E000806000008060
      20008060400080606000806080008060A0008060C0008060E000808000008080
      20008080400080806000808080008080A0008080C0008080E00080A0000080A0
      200080A0400080A0600080A0800080A0A00080A0C00080A0E00080C0000080C0
      200080C0400080C0600080C0800080C0A00080C0C00080C0E00080E0000080E0
      200080E0400080E0600080E0800080E0A00080E0C00080E0E000C0000000C000
      2000C0004000C0006000C0008000C000A000C000C000C000E000C0200000C020
      2000C0204000C0206000C0208000C020A000C020C000C020E000C0400000C040
      2000C0404000C0406000C0408000C040A000C040C000C040E000C0600000C060
      2000C0604000C0606000C0608000C060A000C060C000C060E000C0800000C080
      2000C0804000C0806000C0808000C080A000C080C000C080E000C0A00000C0A0
      2000C0A04000C0A06000C0A08000C0A0A000C0A0C000C0A0E000C0C00000C0C0
      2000C0C04000C0C06000C0C08000C0C0A000F0FBFF00A4A0A000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFF
      FFFFFF000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFF
      FFFFFFFFFFFFFF00A4A4A4A4A4A40700FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFF
      FFFFFFFFFFFFFFFFFFFFFF00A4A4A4A4A4A40700FFFFFFFFFFFFFFFFFFFFFFFF
      FF00FFFFFFFFFFFFFFFFFFFFFFFFFF00A4A4A4A4A4A40700FFFFFFFFFFFFFFFF
      FFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFF00A4A4A4A4A4A40700FFFFFFFF
      FFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFF00A4A4A4A4A4A40700
      FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFF00A4A4A4A4
      A4A40700FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFF00
      A4A4A4A4A4A40700FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFF
      FFFFFF00A4A4A4A4A4A40700FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFF
      FFFFFFFFFFFFFF00A4A4A4A4A4A40700FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFF
      FFFFFFFFFFFFFFFFFFFFFF00A4A4A4A4A4A40700FFFFFFFFFFFFFFFFFFFFFFFF
      FF00FFFFFFFFFFFFFFFFFFFFFFFFFF00A4A4A4A4A4A40700FFFFFFFFFFFFFFFF
      FFFFFFFFFF00FFFFFFFFFFFFFFFF000000000000A4A4A4A4A4A4070000000000
      00FFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFF00A4A4A4A4A4A4A4A4A4A4A4A4A4
      A4A407A400FFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFF00A4A4A4A4A4A4A4A4
      A4A4A4A4A407A400FFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFF00A4A4A4
      A4A4A4A4A4A4A4A407A400FFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFF
      FF00A4A4A4A4A4A4A4A4A407A400FFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFF
      FFFFFFFFFFFF00A4A4A4A4A4A4A407A400FFFFFFFFFFFFFFFFFFFFFFFF00FFFF
      FFFFFFFFFFFFFFFFFFFFFF00A4A4A4A4A407A400FFFFFFFFFFFFFFFFFFFFFFFF
      FF00FFFFFFFFFFFFFFFFFFFFFFFFFFFF00A4A4A407A400FFFFFFFFFFFFFFFFFF
      FFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00A407A400FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00A400FFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFF00}
    Layout = blGlyphTop
    LookAndFeel.Kind = lfUltraFlat
  end
  object cxButton9: TcxButton
    Left = 640
    Top = 224
    Width = 65
    Height = 33
    BiDiMode = bdRightToLeftNoAlign
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentBiDiMode = False
    ParentFont = False
    TabOrder = 15
    TabStop = False
    OnClick = cxButton9Click
    Colors.Default = 11457198
    Colors.Normal = 11457198
    Colors.Hot = 7384944
    Colors.Pressed = 5411922
    Glyph.Data = {
      22090000424D2209000000000000360400002800000023000000230000000100
      080000000000EC04000000000000000000000001000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000C0DCC000F0CA
      A6000020400000206000002080000020A0000020C0000020E000004000000040
      20000040400000406000004080000040A0000040C0000040E000006000000060
      20000060400000606000006080000060A0000060C0000060E000008000000080
      20000080400000806000008080000080A0000080C0000080E00000A0000000A0
      200000A0400000A0600000A0800000A0A00000A0C00000A0E00000C0000000C0
      200000C0400000C0600000C0800000C0A00000C0C00000C0E00000E0000000E0
      200000E0400000E0600000E0800000E0A00000E0C00000E0E000400000004000
      20004000400040006000400080004000A0004000C0004000E000402000004020
      20004020400040206000402080004020A0004020C0004020E000404000004040
      20004040400040406000404080004040A0004040C0004040E000406000004060
      20004060400040606000406080004060A0004060C0004060E000408000004080
      20004080400040806000408080004080A0004080C0004080E00040A0000040A0
      200040A0400040A0600040A0800040A0A00040A0C00040A0E00040C0000040C0
      200040C0400040C0600040C0800040C0A00040C0C00040C0E00040E0000040E0
      200040E0400040E0600040E0800040E0A00040E0C00040E0E000800000008000
      20008000400080006000800080008000A0008000C0008000E000802000008020
      20008020400080206000802080008020A0008020C0008020E000804000008040
      20008040400080406000804080008040A0008040C0008040E000806000008060
      20008060400080606000806080008060A0008060C0008060E000808000008080
      20008080400080806000808080008080A0008080C0008080E00080A0000080A0
      200080A0400080A0600080A0800080A0A00080A0C00080A0E00080C0000080C0
      200080C0400080C0600080C0800080C0A00080C0C00080C0E00080E0000080E0
      200080E0400080E0600080E0800080E0A00080E0C00080E0E000C0000000C000
      2000C0004000C0006000C0008000C000A000C000C000C000E000C0200000C020
      2000C0204000C0206000C0208000C020A000C020C000C020E000C0400000C040
      2000C0404000C0406000C0408000C040A000C040C000C040E000C0600000C060
      2000C0604000C0606000C0608000C060A000C060C000C060E000C0800000C080
      2000C0804000C0806000C0808000C080A000C080C000C080E000C0A00000C0A0
      2000C0A04000C0A06000C0A08000C0A0A000C0A0C000C0A0E000C0C00000C0C0
      2000C0C04000C0C06000C0C08000C0C0A000F0FBFF00A4A0A000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFF00A400FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFF00A407A400FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFF
      FFFFFFFFFFFFFFFF00A4A4A407A400FFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFF
      FFFFFFFFFFFFFFFFFFFFFF00A4A4A4A4A407A400FFFFFFFFFFFFFFFFFFFFFFFF
      FF00FFFFFFFFFFFFFFFFFFFFFFFF00A4A4A4A4A4A4A407A400FFFFFFFFFFFFFF
      FFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFF00A4A4A4A4A4A4A4A4A407A400FFFF
      FFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFF00A4A4A4A4A4A4A4A4A4A4A4
      07A400FFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFF00A4A4A4A4A4A4A4A4
      A4A4A4A4A407A400FFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFF00A4A4A4A4A4
      A4A4A4A4A4A4A4A4A4A407A400FFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFF0000
      00000000A4A4A4A4A4A407000000000000FFFFFFFFFFFFFFFF00FFFFFFFFFFFF
      FFFFFFFFFFFFFF00A4A4A4A4A4A40700FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFF
      FFFFFFFFFFFFFFFFFFFFFF00A4A4A4A4A4A40700FFFFFFFFFFFFFFFFFFFFFFFF
      FF00FFFFFFFFFFFFFFFFFFFFFFFFFF00A4A4A4A4A4A40700FFFFFFFFFFFFFFFF
      FFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFF00A4A4A4A4A4A40700FFFFFFFF
      FFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFF00A4A4A4A4A4A40700
      FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFF00A4A4A4A4
      A4A40700FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFF00
      A4A4A4A4A4A40700FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFF
      FFFFFF00A4A4A4A4A4A40700FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFF
      FFFFFFFFFFFFFF00A4A4A4A4A4A40700FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFF
      FFFFFFFFFFFFFFFFFFFFFF00A4A4A4A4A4A40700FFFFFFFFFFFFFFFFFFFFFFFF
      FF00FFFFFFFFFFFFFFFFFFFFFFFFFF00A4A4A4A4A4A40700FFFFFFFFFFFFFFFF
      FFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000FFFFFFFF
      FFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFF00}
    Layout = blGlyphTop
    LookAndFeel.Kind = lfUltraFlat
  end
  object cxButton10: TcxButton
    Left = 544
    Top = 288
    Width = 161
    Height = 41
    Caption = #1057#1082#1080#1076#1082#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 16
    TabStop = False
    OnClick = cxButton10Click
    Colors.Default = 11192063
    Colors.Normal = 11192063
    Colors.Hot = 7446527
    Colors.Pressed = 7446527
    Layout = blGlyphTop
    LookAndFeel.Kind = lfUltraFlat
  end
  object BEdit1: TcxButtonEdit
    Left = 584
    Top = 100
    Properties.Buttons = <
      item
        Default = True
        Kind = bkEllipsis
      end>
    Properties.OnButtonClick = BEdit1PropertiesButtonClick
    Style.LookAndFeel.Kind = lfOffice11
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 17
    Text = 'BEdit1'
    OnClick = BEdit1Click
    Width = 121
  end
  object SEdit1: TcxSpinEdit
    Left = 584
    Top = 156
    Properties.MaxValue = 1000.000000000000000000
    Properties.MinValue = 1.000000000000000000
    Style.BorderStyle = ebsOffice11
    TabOrder = 18
    Value = 1
    Width = 49
  end
  object cxButton11: TcxButton
    Left = 648
    Top = 134
    Width = 49
    Height = 27
    BiDiMode = bdRightToLeftNoAlign
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentBiDiMode = False
    ParentFont = False
    TabOrder = 19
    TabStop = False
    OnClick = cxButton11Click
    Colors.Default = clWhite
    Colors.Normal = clWhite
    Colors.Hot = clMoneyGreen
    Colors.Pressed = clMoneyGreen
    Glyph.Data = {
      76010000424D760100000000000036000000280000000A0000000A0000000100
      18000000000040010000C40E0000C40E00000000000000000000FFFFFFFFFFFF
      FFFFFFFF6600FF6600FF6600FF6600FFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
      FFFFFFFF6600FF6600FF6600FF6600FFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
      FFFFFFFF6600FF6600FF6600FF6600FFFFFFFFFFFFFFFFFF0000FF6600FF6600
      FF6600FF6600FF6600FF6600FF6600FF6600FF6600FF66000000FF6600FF6600
      FF6600FF6600FF6600FF6600FF6600FF6600FF6600FF66000000FF9966FF9966
      FF9966FF9966FF9966FF9966FF9966FF9966FF9966FF99660000CCCC99CCCC99
      CCCC99CCCC99CCCC99CCCC99CCCC99CCCC99CCCC99CCCC990000FFFFFFFFFFFF
      FFFFFFFF9966CCCC99CCCC99FF9966FFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
      FFFFFFFF9966CCCC99CCCC99FF9966FFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
      FFFFFFFF9966FF9966FF9966FF9966FFFFFFFFFFFFFFFFFF0000}
    Layout = blGlyphTop
    LookAndFeel.Kind = lfUltraFlat
  end
  object cxButton12: TcxButton
    Left = 544
    Top = 399
    Width = 161
    Height = 42
    Caption = #1056#1072#1089#1095#1077#1090
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 20
    TabStop = False
    OnClick = cxButton12Click
    Colors.Default = 12621940
    Colors.Normal = 12621940
    Colors.Hot = 10843723
    Colors.Pressed = 10843723
    Layout = blGlyphTop
    LookAndFeel.Kind = lfUltraFlat
  end
  object cxButton13: TcxButton
    Left = 648
    Top = 166
    Width = 49
    Height = 27
    BiDiMode = bdRightToLeftNoAlign
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentBiDiMode = False
    ParentFont = False
    TabOrder = 21
    TabStop = False
    OnClick = cxButton13Click
    Colors.Default = clWhite
    Colors.Normal = clWhite
    Colors.Hot = clMoneyGreen
    Colors.Pressed = clMoneyGreen
    Glyph.Data = {
      76010000424D760100000000000036000000280000000D000000080000000100
      1800000000004001000000000000000000000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFF00FFFFFF633AF3633AF3633AF3633AF3633AF3633AF363
      3AF3633AF3633AF3633AF3633AF3FFFFFF00FFFFFF633AF3A777E8A777E8A777
      E8A777E8A777E8A777E8A777E8A777E8A777E8633AF3FFFFFF00FFFFFF633AF3
      C3A7FFC3A7FFC3A7FFC3A7FFC3A7FFC3A7FFC3A7FFC3A7FFC3A7FFA777E8FFFF
      FF00FFFFFFA777E8A777E8A777E8A777E8A777E8A777E8A777E8A777E8A777E8
      A777E8A777E8FFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00}
    Layout = blGlyphTop
    LookAndFeel.Kind = lfUltraFlat
  end
  object FormPlacement1: TFormPlacement
    Active = False
    Left = 408
    Top = 240
  end
  object amSpec: TActionManager
    Left = 232
    Top = 248
    StyleName = 'XP Style'
    object acMenu: TAction
      Caption = 'acMenu'
      ShortCut = 107
      OnExecute = acMenuExecute
    end
    object acChangeQuantity: TAction
      Caption = 'acChangeQuantity'
      ShortCut = 115
      OnExecute = acChangeQuantityExecute
    end
    object acDelPos: TAction
      Caption = 'acDelPos'
      ShortCut = 109
      OnExecute = acDelPosExecute
    end
    object acMovePos: TAction
      Caption = 'acMovePos'
      ShortCut = 117
      OnExecute = acMovePosExecute
    end
    object acPrePrint: TAction
      Caption = 'acPrePrint'
      ShortCut = 114
      OnExecute = acPrePrintExecute
    end
    object acModify: TAction
      Caption = 'acModify'
      OnExecute = acModifyExecute
    end
    object Action1: TAction
      Caption = 'Action1'
      ShortCut = 112
      OnExecute = Action1Execute
    end
    object Action2: TAction
      Caption = 'Action2'
      ShortCut = 113
      OnExecute = Action2Execute
    end
    object Action5: TAction
      Caption = 'Action5'
      ShortCut = 116
      OnExecute = Action5Execute
    end
    object Action7: TAction
      Caption = 'Action7'
      ShortCut = 118
      OnExecute = Action7Execute
    end
    object Action8: TAction
      Caption = 'Action8'
      ShortCut = 119
      OnExecute = Action8Execute
    end
    object Action9: TAction
      Caption = 'Action9'
      ShortCut = 120
      OnExecute = Action9Execute
    end
    object Action10: TAction
      Caption = #1042#1099#1093#1086#1076
      ShortCut = 121
      OnExecute = Action10Execute
    end
    object acAddMod: TAction
      Caption = #1044#1086#1073'.'#1052#1086#1076#1080#1092'.'
      ShortCut = 122
      OnExecute = acAddModExecute
    end
    object acDelMod: TAction
      Caption = #1059#1076#1083'. '#1052#1086#1076#1080#1092'.'
      ShortCut = 123
      OnExecute = acDelModExecute
    end
    object acKod: TAction
      Caption = #1055#1086' '#1082#1086#1076#1091
      OnExecute = acKodExecute
    end
    object acDiscount: TAction
      Caption = 'acDiscount'
      ShortCut = 186
      OnExecute = acDiscountExecute
    end
    object acExitN: TAction
      Caption = 'acExitN'
      OnExecute = acExitNExecute
    end
    object acNumTab: TAction
      Caption = #1053#1086#1084#1077#1088' '#1079#1072#1082#1072#1079#1072
      OnExecute = acNumTabExecute
    end
  end
  object Timer1: TTimer
    Interval = 3000
    OnTimer = Timer1Timer
    Left = 408
    Top = 184
  end
  object dxfBackGround1: TdxfBackGround
    BkColor.BeginColor = 10131969
    BkColor.EndColor = 16711314
    BkColor.FillStyle = fsVert
    BkAnimate.Speed = 700
    Left = 336
    Top = 264
  end
  object frRepSp: TfrReport
    InitialZoom = pzDefault
    PreviewButtons = [pbZoom, pbLoad, pbSave, pbPrint, pbFind, pbHelp, pbExit]
    ShowPrintDialog = False
    ShowProgress = False
    RebuildPrinter = False
    Left = 168
    Top = 136
    ReportForm = {19000000}
  end
  object frquCheck: TfrDBDataSet
    DataSet = dmC.quCheck
    OpenDataSource = False
    Left = 253
    Top = 141
  end
end
