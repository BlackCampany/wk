unit InputMoney;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxControls, cxContainer, cxEdit, cxTextEdit, cxMaskEdit,
  cxDropDownEdit, cxCalc, StdCtrls, ExtCtrls, Menus, cxLookAndFeelPainters,
  cxButtons, cxCalendar;

type
  TfmInputMoney = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    cxCalcEdit1: TcxCalcEdit;
    Label2: TLabel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Label3: TLabel;
    cxDateEdit1: TcxDateEdit;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmInputMoney: TfmInputMoney;

implementation

{$R *.dfm}

procedure TfmInputMoney.FormShow(Sender: TObject);
begin
  cxCalcEdit1.SelectAll;
  cxCalcEdit1.SetFocus;
end;

end.
