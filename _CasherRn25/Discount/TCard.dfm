object fmTCard: TfmTCard
  Left = 282
  Top = 113
  Width = 585
  Height = 578
  Caption = #1058#1077#1093#1085#1086#1083#1086#1075#1080#1095#1077#1089#1082#1072#1103' '#1082#1072#1088#1090#1072
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Image1: TImage
    Left = 436
    Top = 297
    Width = 17
    Height = 15
    Center = True
    Picture.Data = {
      07544269746D617076010000424D760100000000000036000000280000000A00
      00000A000000010018000000000040010000C40E0000C40E0000000000000000
      0000FFFFFFFFFFFFFFFFFFFF6600FF6600FF6600FF6600FFFFFFFFFFFFFFFFFF
      0000FFFFFFFFFFFFFFFFFFFF6600FF6600FF6600FF6600FFFFFFFFFFFFFFFFFF
      0000FFFFFFFFFFFFFFFFFFFF6600FF6600FF6600FF6600FFFFFFFFFFFFFFFFFF
      0000FF6600FF6600FF6600FF6600FF6600FF6600FF6600FF6600FF6600FF6600
      0000FF6600FF6600FF6600FF6600FF6600FF6600FF6600FF6600FF6600FF6600
      0000FF9966FF9966FF9966FF9966FF9966FF9966FF9966FF9966FF9966FF9966
      0000CCCC99CCCC99CCCC99CCCC99CCCC99CCCC99CCCC99CCCC99CCCC99CCCC99
      0000FFFFFFFFFFFFFFFFFFFF9966CCCC99CCCC99FF9966FFFFFFFFFFFFFFFFFF
      0000FFFFFFFFFFFFFFFFFFFF9966CCCC99CCCC99FF9966FFFFFFFFFFFFFFFFFF
      0000FFFFFFFFFFFFFFFFFFFF9966FF9966FF9966FF9966FFFFFFFFFFFFFFFFFF
      0000}
    Proportional = True
    Transparent = True
    OnClick = cxLabel1Click
  end
  object Image2: TImage
    Left = 436
    Top = 321
    Width = 17
    Height = 15
    Center = True
    Picture.Data = {
      07544269746D617076010000424D760100000000000036000000280000000D00
      0000080000000100180000000000400100000000000000000000000000000000
      0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFF633AF3633AF3633AF3633A
      F3633AF3633AF3633AF3633AF3633AF3633AF3633AF3FFFFFF00FFFFFF633AF3
      A777E8A777E8A777E8A777E8A777E8A777E8A777E8A777E8A777E8633AF3FFFF
      FF00FFFFFF633AF3C3A7FFC3A7FFC3A7FFC3A7FFC3A7FFC3A7FFC3A7FFC3A7FF
      C3A7FFA777E8FFFFFF00FFFFFFA777E8A777E8A777E8A777E8A777E8A777E8A7
      77E8A777E8A777E8A777E8A777E8FFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FF00}
    Proportional = True
    Transparent = True
    OnClick = cxLabel2Click
  end
  object Label7: TLabel
    Left = 432
    Top = 344
    Width = 74
    Height = 13
    Cursor = crHandPoint
    Caption = #1041#1088#1091#1090#1090#1086' '#1085#1072' '#1076#1072#1090#1091
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsUnderline]
    ParentFont = False
    OnClick = Label7Click
  end
  object Image3: TImage
    Left = 436
    Top = 265
    Width = 17
    Height = 15
    Center = True
    Picture.Data = {
      07544269746D61709A020000424D9A0200000000000036000000280000000B00
      0000110000000100180000000000640200000000000000000000000000000000
      0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFF000000FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFF000000FFFFFF000000000000FFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF0000000000000000000000
      00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000000000FF
      6600000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000
      FF6600FF9966FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF
      FFFFFFFF000000FF6600FF9966000000000000FFFFFFFFFFFFFFFFFFFFFFFF00
      0000FFFFFFFFFFFF000000FF9966FF6600FFFFFF000000FFFFFFFFFFFFFFFFFF
      FFFFFF000000FFFFFFFFFFFFFFFFFF000000FF9966FF6600000000000000FFFF
      FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFF000000FF6600FF9966FFFFFF00
      0000FFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFF000000FF6600
      000000000000000000FFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFF0000
      000000000000BD00FFFF000000FFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFF0000000000BD00FFFF00FFFF000000FFFFFF000000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFF0000000000BD0000BD0000BD000000FFFFFF000000FFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000FFFFFFFFFFFF00
      0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFF000000}
    Proportional = True
    Transparent = True
    OnClick = cxLabel1Click
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 577
    Height = 257
    Align = alTop
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 1
    object Label6: TLabel
      Left = 208
      Top = 16
      Width = 49
      Height = 13
      AutoSize = False
      Caption = #1050#1086#1076' '
    end
    object GTCards: TcxGrid
      Left = 8
      Top = 40
      Width = 193
      Height = 209
      PopupMenu = PopupMenuTCard
      TabOrder = 0
      TabStop = False
      LookAndFeel.Kind = lfOffice11
      object ViewTCards: TcxGridDBTableView
        NavigatorButtons.ConfirmDelete = False
        OnFocusedRecordChanged = ViewTCardsFocusedRecordChanged
        DataController.DataSource = dmO.dsTCards
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsCustomize.ColumnFiltering = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.GroupByBox = False
        object ViewTCardsIDCARD: TcxGridDBColumn
          Caption = #1050#1086#1076' '#1082#1072#1088#1090#1099
          DataBinding.FieldName = 'IDCARD'
          Visible = False
        end
        object ViewTCardsID: TcxGridDBColumn
          Caption = #1050#1086#1076' '
          DataBinding.FieldName = 'ID'
          Width = 39
        end
        object ViewTCardsDATEB: TcxGridDBColumn
          Caption = #1044#1072#1090#1072' '#1085#1072#1095'.'
          DataBinding.FieldName = 'DATEB'
        end
        object ViewTCardsDATEE: TcxGridDBColumn
          Caption = #1044#1072#1090#1072' '#1086#1082#1086#1085#1095'.'
          DataBinding.FieldName = 'DATEE'
          Visible = False
          Width = 68
        end
        object ViewTCardsSHORTNAME: TcxGridDBColumn
          DataBinding.FieldName = 'SHORTNAME'
          Visible = False
        end
        object ViewTCardsRECEIPTNUM: TcxGridDBColumn
          DataBinding.FieldName = 'RECEIPTNUM'
          Visible = False
        end
        object ViewTCardsPOUTPUT: TcxGridDBColumn
          DataBinding.FieldName = 'POUTPUT'
          Visible = False
        end
        object ViewTCardsPCOUNT: TcxGridDBColumn
          DataBinding.FieldName = 'PCOUNT'
          Visible = False
        end
        object ViewTCardsPVES: TcxGridDBColumn
          DataBinding.FieldName = 'PVES'
          Visible = False
        end
        object ViewTCardsSDATEE: TcxGridDBColumn
          Caption = #1044#1072#1090#1072' '#1086#1082#1086#1085#1095'.'
          DataBinding.FieldName = 'SDATEE'
          Width = 67
        end
      end
      object LTCards: TcxGridLevel
        GridView = ViewTCards
      end
    end
    object cxButton4: TcxButton
      Left = 8
      Top = 8
      Width = 41
      Height = 22
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100' '#1058#1050
      TabOrder = 1
      TabStop = False
      OnClick = cxButton4Click
      Glyph.Data = {
        76010000424D760100000000000036000000280000000A0000000A0000000100
        18000000000040010000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFF6600FF6600FF6600FF6600FFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
        FFFFFFFF6600FF6600FF6600FF6600FFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
        FFFFFFFF6600FF6600FF6600FF6600FFFFFFFFFFFFFFFFFF0000FF6600FF6600
        FF6600FF6600FF6600FF6600FF6600FF6600FF6600FF66000000FF6600FF6600
        FF6600FF6600FF6600FF6600FF6600FF6600FF6600FF66000000FF9966FF9966
        FF9966FF9966FF9966FF9966FF9966FF9966FF9966FF99660000CCCC99CCCC99
        CCCC99CCCC99CCCC99CCCC99CCCC99CCCC99CCCC99CCCC990000FFFFFFFFFFFF
        FFFFFFFF9966CCCC99CCCC99FF9966FFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
        FFFFFFFF9966CCCC99CCCC99FF9966FFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
        FFFFFFFF9966FF9966FF9966FF9966FFFFFFFFFFFFFFFFFF0000}
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton5: TcxButton
      Left = 96
      Top = 8
      Width = 22
      Height = 22
      Hint = #1059#1076#1072#1083#1080#1090#1100' '#1058#1050
      TabOrder = 2
      TabStop = False
      OnClick = cxButton5Click
      Glyph.Data = {
        76010000424D760100000000000036000000280000000D000000080000000100
        1800000000004001000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFF00FFFFFF633AF3633AF3633AF3633AF3633AF3633AF363
        3AF3633AF3633AF3633AF3633AF3FFFFFF00FFFFFF633AF3A777E8A777E8A777
        E8A777E8A777E8A777E8A777E8A777E8A777E8633AF3FFFFFF00FFFFFF633AF3
        C3A7FFC3A7FFC3A7FFC3A7FFC3A7FFC3A7FFC3A7FFC3A7FFC3A7FFA777E8FFFF
        FF00FFFFFFA777E8A777E8A777E8A777E8A777E8A777E8A777E8A777E8A777E8
        A777E8A777E8FFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00}
      LookAndFeel.Kind = lfOffice11
    end
    object Panel3: TPanel
      Left = 208
      Top = 40
      Width = 361
      Height = 209
      BevelInner = bvLowered
      Color = clWhite
      TabOrder = 3
      object Label1: TLabel
        Left = 8
        Top = 16
        Width = 99
        Height = 13
        Caption = #1050#1086#1088#1086#1090#1082#1086#1077' '#1085#1072#1079#1074#1072#1085#1080#1077
      end
      object Label2: TLabel
        Left = 8
        Top = 48
        Width = 91
        Height = 13
        Caption = #1053#1086#1084#1077#1088' '#1088#1077#1094#1077#1087#1090#1091#1088#1099
      end
      object Label3: TLabel
        Left = 8
        Top = 72
        Width = 117
        Height = 13
        Caption = #1042#1099#1093#1086#1076' 1 '#1087#1086#1088#1094'. '#1087#1086' '#1084#1077#1085#1102
      end
      object Label4: TLabel
        Left = 8
        Top = 112
        Width = 217
        Height = 13
        Caption = #1063#1080#1089#1083#1086' '#1087#1086#1088#1094'. '#1085#1072' '#1082#1086#1090#1086#1088#1086#1077' '#1089#1076#1077#1083#1072#1085#1072' '#1079#1072#1082#1083#1072#1076#1082#1072
      end
      object Label5: TLabel
        Left = 8
        Top = 136
        Width = 119
        Height = 13
        Caption = #1052#1072#1089#1089#1072' '#1086#1076#1085#1086#1081' '#1087#1086#1088#1094#1080#1080'  '#1075'.'
      end
      object Label8: TLabel
        Left = 312
        Top = 200
        Width = 32
        Height = 13
        Caption = 'Label8'
        Visible = False
      end
      object cxTextEdit2: TcxTextEdit
        Left = 128
        Top = 12
        TabStop = False
        ParentFont = False
        Properties.MaxLength = 100
        Properties.OnChange = cxTextEdit2PropertiesChange
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'MS Sans Serif'
        Style.Font.Style = [fsBold]
        Style.Shadow = True
        Style.IsFontAssigned = True
        TabOrder = 0
        Text = 'cxTextEdit2'
        Width = 225
      end
      object cxTextEdit3: TcxTextEdit
        Left = 144
        Top = 44
        TabStop = False
        Properties.OnChange = cxTextEdit3PropertiesChange
        Style.Shadow = True
        TabOrder = 1
        Text = 'cxTextEdit3'
        Width = 89
      end
      object cxTextEdit4: TcxTextEdit
        Left = 144
        Top = 68
        TabStop = False
        Properties.OnChange = cxTextEdit4PropertiesChange
        Style.Shadow = True
        TabOrder = 2
        Text = 'cxTextEdit4'
        Width = 89
      end
      object cxSpinEdit1: TcxSpinEdit
        Left = 232
        Top = 108
        TabStop = False
        Properties.OnChange = cxSpinEdit1PropertiesChange
        Style.LookAndFeel.Kind = lfOffice11
        Style.Shadow = True
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 3
        Value = 1
        Width = 89
      end
      object cxCalcEdit1: TcxCalcEdit
        Left = 232
        Top = 132
        TabStop = False
        EditValue = 0.000000000000000000
        Properties.OnChange = cxCalcEdit1PropertiesChange
        Style.LookAndFeel.Kind = lfOffice11
        Style.Shadow = True
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 4
        Width = 89
      end
      object cxButton1: TcxButton
        Left = 208
        Top = 168
        Width = 113
        Height = 33
        Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
        Default = True
        Enabled = False
        TabOrder = 5
        OnClick = cxButton1Click
        LookAndFeel.Kind = lfOffice11
      end
      object cxButton8: TcxButton
        Left = 40
        Top = 168
        Width = 113
        Height = 33
        Caption = #1055#1077#1095#1072#1090#1100' '#1058#1058#1050
        TabOrder = 6
        TabStop = False
        OnClick = cxButton8Click
        Glyph.Data = {
          86070000424D86070000000000003600000028000000180000001A0000000100
          1800000000005007000000000000000000000000000000000000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0BDB2
          B3B0A7FFFFFFFFFFFFD7D7D7D7D7D7D7D7D7FFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFB0AEA8989896838280898883A9A7A0B4B3AA93928D85848096948EAFADA5
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFA1A09BAEAEADC9C9C8ABABAA84848371706D767573A2A2A1A4
          A4A376767472716D93918CACAAA3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFB8B7B0A3A3A0D8D8D8E7E7E7D5D5D5C7C7C7A9A9A98585
          85A0A09FA8A8A8BABABAD7D7D7C3C3C38686856E6D6B8B8A85FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFA5A4A0C6C6C5F6F6F6F8F8F8E2E2E2D2D2D2
          C6C6C6AEAEAE787878777777929292A4A4A4B3B3B3C9C9C9E2E2E2D9D9D9A1A1
          A07D7C7AB0AEA8FFFFFFFFFFFFFFFFFFFFFFFFADADABE7E7E7FDFDFDFEFEFEF4
          F4F4E3E3E3D4D4D4C3C3C3B7B7B79E9E9E888888828282929292AAAAAABDBDBD
          C6C6C6BDBDBDA1A1A181817FBEBDB6FFFFFFFFFFFFFFFFFFBCBBB9FDFDFDFFFF
          FFFFFFFFFFFFFFF3F3F3D7D7D7C9C9C9BEBEBEB6B6B6C4C4C4D0D0D0C6C6C6AB
          ABAB9B9B9B9090905353537E7E7E7C7C7B9B9A96FFFFFFFFFFFFFFFFFFFFFFFF
          BCBCBAFFFFFFFFFFFFFBFBFBE9E9E9D7D7D7D4D4D4D7D7D7D1D1D1C8C8C8C0C0
          C0C3C3C3CECECEDBDBDBD9D9D9BBBBBB8D8D8D9F9D9E878686979692FFFFFFFF
          FFFFFFFFFFFFFFFFBDBDBBFDFDFDE9E9E9D8D8D8DEDEDEE4E4E4E0E0E0DBDBDB
          D6D6D6CFCFCFC9C9C9C2C2C2BBBBBBBABABAC1C1C1C9C9C9C5C1C448FF737D94
          87979692FFFFFFFFFFFFFFFFFFFFFFFFBEBEBBEAEAEAE5E5E5EEEEEEEBEBEBE3
          E3E3E7E7E7F1F1F1EAEAEADCDCDCCFCFCFC6C6C6BFBFBFB6B6B6B0B0B0B2B2B2
          B4B3B3A7B7AE8C9A93979592FFFFFFFFFFFFFFFFFFFFFFFFBFBEBDFEFEFEF7F7
          F7EEEEEEEBEBEBF2F2F2F9F9F9E6E6E6D8D8D8DCDCDCDCDCDCD5D5D5CCCCCCC0
          C0C0B7B7B7B3B3B3B2B2B2B6B2B4B2ADAFA09F9DFFFFFFFFFFFFFFFFFFFFFFFF
          C0C0C0FBFBFBF8F8F8F7F7F7FBFBFBF7F7F7DFDFDFD6D6D6E6E6E6E5E5E5E1E1
          E1DDDDDDD7D7D7D0D0D0C6C6C6BEBEBEB8B8B8B3B3B3B0B0B0B8B7B4FFFFFFFF
          FFFFFFFFFFFFFFFFDCDCDBDCDCDBF7F7F7F4F4F4E5E5E5D2D2D2D1D1D1E0E0E0
          E9E8E8F1F1F1F9F8F8FCFCFCFEFEFEF4F4F4EBEBEBDADADABABABAB3B3B3B1B1
          B0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE0DFDDCFCFCED5D6D7D7
          D8DAD7D9DBCED0D2CED0D2D5D5D7DCDDDDE9E9E9F3F4F4FFFFFFFBFBFBDADADA
          B3B3B3BABAB9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFDDDCDBEEECEBE6E1DADFD9CFD4D0CAC8C7C4BCBCBCB3B4B7B4B6BABEC1C2D3
          D3D3C3C2C2C1C1C0DBDBD9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFE7D5C6F4E5C7F4E0BEF2DDBBECD8B7E4D2B4D9C8
          B0CCBDABAFA9A6B2B2B2E0DFDDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2D7C3FFF5D3FFEBC7FFE9C1
          FFE6B8FFE3B1FFE3AFFED9AAAD9B95D7D7D5FFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE5E1E0F7E1CEFF
          F4D9FFECCDFFEAC7FFE7C0FFE4B8FFE6B4FCD9ACA79B98FFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFDED3D0FEF0DFFFF4DCFFEFD3FFECCDFFE9C6FFE6C0FFEABBF3D2ADB1A8A6FF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFE7D5D0FFFBECFFF4E1FFF1DBFFEED4FFEBCDFFE9C6FFEE
          C1DBBEA6DCDADAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFE6E3E3F7EBE5FFFEF4FFF5E7FFF3E1FFF1DA
          FFEED3FFEDCDFFEFC8C0A89EDCDADAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE4D9D9FFFEFDFFFEF9FF
          F9EFFFF6E8FFF3E1FFF0D9FFF4D6FAE5C8C2B5B3F3F3F3FFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEAD7
          D7FFFEFEFFFFFEFFFEF8FFFDF2FFFBECFFFCE9FFFBE0D9C2B9E9E5E5FFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFEFEEEAF5EEEEF1E8E8EDE1E1EAD9D8E5D1CEE5CCC7E3CBC6FFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFF}
        LookAndFeel.Kind = lfOffice11
      end
      object cxButton9: TcxButton
        Left = 256
        Top = 44
        Width = 91
        Height = 25
        Caption = #1058#1077#1093#1085#1086#1083#1086#1075#1080#1103
        TabOrder = 7
        TabStop = False
        OnClick = cxButton9Click
        LookAndFeel.Kind = lfOffice11
      end
    end
    object cxTextEdit1: TcxTextEdit
      Left = 264
      Top = 9
      TabStop = False
      ParentFont = False
      Properties.MaxLength = 200
      Properties.ReadOnly = True
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsBold]
      Style.Shadow = True
      Style.IsFontAssigned = True
      TabOrder = 4
      Text = 'cxTextEdit1'
      Width = 305
    end
    object cxButton6: TcxButton
      Left = 56
      Top = 8
      Width = 22
      Height = 22
      Hint = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100' '#1058#1050
      TabOrder = 5
      TabStop = False
      Visible = False
      OnClick = cxButton6Click
      Glyph.Data = {
        9A020000424D9A0200000000000036000000280000000B000000110000000100
        1800000000006402000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF
        FF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
        0000FFFFFF000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFF000000FFFFFF000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000FFFFFF000000000000000000000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000000000FF6600000000000000
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000FF6600FF9966FFFF
        FF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF000000FF
        6600FF9966000000000000FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
        000000FF9966FF6600FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFF000000FFFF
        FFFFFFFFFFFFFF000000FF9966FF6600000000000000FFFFFFFFFFFFFFFFFF00
        0000FFFFFFFFFFFFFFFFFF000000FF6600FF9966FFFFFF000000FFFFFFFFFFFF
        FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFF000000FF66000000000000000000
        00FFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFF0000000000000000BD00
        FFFF000000FFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000
        0000BD00FFFF00FFFF000000FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FF0000000000BD0000BD0000BD000000FFFFFF000000FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFF000000000000000000FFFFFFFFFFFF000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000}
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton7: TcxButton
      Left = 160
      Top = 8
      Width = 22
      Height = 22
      Hint = #1059#1076#1072#1083#1080#1090#1100' '#1058#1050
      TabOrder = 6
      TabStop = False
      Visible = False
      OnClick = cxButton7Click
      LookAndFeel.Kind = lfOffice11
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 525
    Width = 577
    Height = 19
    Panels = <
      item
        Width = 300
      end
      item
        Width = 50
      end>
  end
  object Panel2: TPanel
    Left = 0
    Top = 257
    Width = 425
    Height = 268
    Align = alLeft
    BevelInner = bvLowered
    TabOrder = 0
    object GTSpec: TcxGrid
      Left = 2
      Top = 2
      Width = 421
      Height = 264
      Align = alClient
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
      object ViewTSpec: TcxGridDBTableView
        NavigatorButtons.ConfirmDelete = False
        OnEditKeyDown = ViewTSpecEditKeyDown
        DataController.DataSource = dstaTSpec
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <
          item
            Format = '0.000'
            Kind = skSum
            FieldName = 'BRUTTO'
            Column = ViewTSpecBRUTTO
          end
          item
            Format = '0.000'
            Kind = skSum
            FieldName = 'NETTO'
            Column = ViewTSpecNETTO
          end>
        DataController.Summary.SummaryGroups = <>
        OptionsCustomize.ColumnFiltering = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Inserting = False
        OptionsView.Footer = True
        OptionsView.GroupByBox = False
        OptionsView.Indicator = True
        object ViewTSpecNum: TcxGridDBColumn
          Caption = #8470
          DataBinding.FieldName = 'Num'
          Width = 29
        end
        object ViewTSpecIDCARD: TcxGridDBColumn
          Caption = #1050#1086#1076' '
          DataBinding.FieldName = 'IdCard'
          Width = 45
        end
        object ViewTSpecNAME: TcxGridDBColumn
          Caption = #1053#1072#1079#1074#1072#1085#1080#1077
          DataBinding.FieldName = 'Name'
          Width = 127
        end
        object ViewTSpecIdM: TcxGridDBColumn
          Caption = #1045#1076'.'#1080#1079#1084'.'
          DataBinding.FieldName = 'IdM'
          PropertiesClassName = 'TcxButtonEditProperties'
          Properties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Visible = False
          Width = 50
        end
        object ViewTSpecSM: TcxGridDBColumn
          Caption = #1045#1076'.'#1080#1079#1084'.'
          DataBinding.FieldName = 'SM'
          PropertiesClassName = 'TcxButtonEditProperties'
          Properties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.OnButtonClick = ViewTSpecSMPropertiesButtonClick
          Width = 51
        end
        object ViewTSpecNETTO: TcxGridDBColumn
          Caption = #1053#1077#1090#1090#1086
          DataBinding.FieldName = 'Netto'
          Width = 68
        end
        object ViewTSpecBRUTTO: TcxGridDBColumn
          Caption = #1041#1088#1091#1090#1090#1086
          DataBinding.FieldName = 'Brutto'
          Width = 70
        end
      end
      object LTSpec: TcxGridLevel
        GridView = ViewTSpec
      end
    end
  end
  object cxButton2: TcxButton
    Left = 440
    Top = 480
    Width = 113
    Height = 25
    Caption = #1042#1099#1093#1086#1076
    TabOrder = 2
    TabStop = False
    OnClick = cxButton2Click
    Glyph.Data = {
      5E040000424D5E04000000000000360000002800000012000000130000000100
      18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
      CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
      8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
      0000CED3D6848684848684848684848684848684848684848684848684848684
      848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
      7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
      FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
      00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
      75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
      0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
      FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
      CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
      007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
      00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
      75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
      0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
      FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
      CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
      00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
      494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
      0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
      00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
      CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
      0000}
    LookAndFeel.Kind = lfOffice11
  end
  object cxLabel1: TcxLabel
    Left = 456
    Top = 296
    Cursor = crHandPoint
    Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1087#1086#1079#1080#1094#1080#1102
    Enabled = False
    ParentFont = False
    Properties.Orientation = cxoLeftTop
    Properties.PenWidth = 3
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = 10485760
    Style.Font.Height = -11
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = [fsUnderline]
    Style.IsFontAssigned = True
    StyleHot.TextStyle = [fsBold, fsUnderline]
    Transparent = True
    OnClick = cxLabel1Click
  end
  object cxLabel2: TcxLabel
    Left = 456
    Top = 320
    Cursor = crHandPoint
    Caption = #1059#1076#1072#1083#1080#1090#1100' '#1087#1086#1079#1080#1094#1080#1102' Del'
    Enabled = False
    ParentFont = False
    Properties.Orientation = cxoLeftTop
    Properties.PenWidth = 3
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = 10485760
    Style.Font.Height = -11
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = [fsUnderline]
    Style.IsFontAssigned = True
    StyleHot.TextStyle = [fsBold, fsUnderline]
    Transparent = True
    OnClick = cxLabel2Click
  end
  object cxButton3: TcxButton
    Left = 440
    Top = 440
    Width = 113
    Height = 25
    Caption = #1057#1077#1073#1077#1089#1090#1086#1080#1084#1086#1089#1090#1100
    TabOrder = 6
    TabStop = False
    OnClick = cxButton3Click
    LookAndFeel.Kind = lfOffice11
  end
  object cxDateEdit1: TcxDateEdit
    Left = 432
    Top = 360
    TabStop = False
    TabOrder = 7
    Width = 121
  end
  object cxLabel3: TcxLabel
    Left = 456
    Top = 264
    Cursor = crHandPoint
    Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100' F4'
    ParentFont = False
    Properties.Orientation = cxoLeftTop
    Properties.PenWidth = 3
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = 10485760
    Style.Font.Height = -11
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = [fsUnderline]
    Style.IsFontAssigned = True
    StyleHot.TextStyle = [fsBold, fsUnderline]
    Transparent = True
    OnClick = cxLabel3Click
  end
  object cxCheckBox1: TcxCheckBox
    Left = 432
    Top = 400
    Caption = #1075
    State = cbsChecked
    TabOrder = 9
    Width = 33
  end
  object cxButton10: TcxButton
    Left = 464
    Top = 400
    Width = 89
    Height = 25
    Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
    Enabled = False
    TabOrder = 10
    OnClick = cxButton10Click
    Glyph.Data = {
      EE030000424DEE03000000000000360000002800000012000000110000000100
      180000000000B8030000C40E0000C40E00000000000000000000CED3D6CED3D6
      CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6A5A6A57371
      737371735A595A5A595A4A494A4A494A393C39313031313031313031A5A6A5CE
      D3D6CED3D6CED3D60000CED3D6CED3D6A5A6A5633400CECFCEE7E7E7C6C3C6C6
      C3C6CECFCEF7F3F7EFEFEFE7E7E7A5A6A5313031313031A5A6A5CED3D6CED3D6
      0000CED3D6633400633400633400DEDBDEE7E7E7E7E7E7E7E7E7E7E7E7E7E7E7
      E7E7E7E7E7E7DEDBDECE8E42633400313031313031CED3D60000CED3D6633400
      CE8E42633400DEA67BDEA67BDEA67BDEA67BDEA67BCE8E42CE8E42CE8E42CE8E
      42CE8E426334007B5900313031CED3D60000CED3D6633400DEA67B633400DEA6
      7BDEA67BDEA67BDEA67BDEA67BDEA67BCE8E42CE8E42CE8E42CE8E42633400CE
      8E42313031CED3D60000CED3D6633400DEA67B633400DEA67BDEA67BDEA67BDE
      A67BDEA67BDEA67BDEA67BCE8E42CE8E42CE8E42633400CE8E42393C39CED3D6
      0000CED3D6633400DEA67B633400AD3C29633400633400633400633400633400
      633400633400633400CE8E42633400CE8E424A494ACED3D60000CED3D6633400
      DEA67B6334009C9A9CADFFFF9CFBFF9CFBFF9CFBFF9CFBFF9CFBFF9CFBFF9CFB
      FF633400633400CE8E424A494ACED3D60000CED3D6633400DEA67B633400ADFF
      FFCECFCEA5A6A5A5A6A5A5A6A5A5A6A5A5A6A5A5A6A5C6C3C69CFBFF633400CE
      8E424A494ACED3D60000CED3D6633400DEA67B7B5900ADFFFFADFFFFADFFFFAD
      FFFFADFFFFADFFFFADFFFFADFFFFADFFFF9CFBFF7B5900CE8E424A494ACED3D6
      0000CED3D6633400DEA67B7B5900ADFFFFCECFCEA5A6A5A5A6A5A5A6A5A5A6A5
      A5A6A5A5A6A5C6C3C69CFBFF7B5900CE8E424A494ACED3D60000CED3D6633400
      DEA67B9C9A9CADFFFFADFFFFADFFFFADFFFFADFFFFADFFFFADFFFFADFFFFADFF
      FF9CFBFF9C9A9CCE8E425A595ACED3D60000CED3D6633400DEA67BA5A6A5CED3
      D6CECFCECE8E42CE8E42A5A6A5A5A6A5A5A6A5A5A6A5C6C3C69CFBFFA5A6A5DE
      A67B636563CED3D60000CED3D6A5A6A56334007B5900CED3D6CED3D6ADFFFFAD
      FFFFADFFFFADFFFFADFFFFADFFFFADFFFFADFFFF7B5900633400A5A6A5CED3D6
      0000CED3D6CED3D6CED3D6A5A6A5633400633400633400633400633400633400
      6334006334006334006334009C9A9CCED3D6CED3D6CED3D60000CED3D6CED3D6
      CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D60000}
    LookAndFeel.Kind = lfOffice11
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 3000
    OnTimer = Timer1Timer
    Left = 96
    Top = 296
  end
  object FormPlacement1: TFormPlacement
    Active = False
    Left = 344
    Top = 320
  end
  object PopupMenuTCard: TPopupMenu
    Left = 152
    Top = 72
    object N1: TMenuItem
      Caption = #1050#1086#1087#1080#1088#1086#1074#1072#1090#1100
      OnClick = N1Click
    end
    object N2: TMenuItem
      Caption = #1042#1089#1090#1072#1074#1080#1090#1100
      OnClick = N2Click
    end
  end
  object TCH: TClientDataSet
    Aggregates = <>
    FileName = 'TCH.cds'
    Params = <>
    Left = 24
    Top = 72
    object TCHIDCARD: TIntegerField
      FieldName = 'IDCARD'
    end
    object TCHID: TIntegerField
      FieldName = 'ID'
    end
    object TCHDATEB: TDateField
      FieldName = 'DATEB'
    end
    object TCHDATEE: TDateField
      FieldName = 'DATEE'
    end
    object TCHSHORTNAME: TStringField
      FieldName = 'SHORTNAME'
      Size = 100
    end
    object TCHRECEIPTNUM: TStringField
      FieldName = 'RECEIPTNUM'
      Size = 30
    end
    object TCHPOUTPUT: TStringField
      FieldName = 'POUTPUT'
      Size = 30
    end
    object TCHPCOUNT: TIntegerField
      FieldName = 'PCOUNT'
    end
    object TCHPVES: TFloatField
      FieldName = 'PVES'
    end
  end
  object TCS: TClientDataSet
    Aggregates = <>
    FileName = 'C:\_CasherRn\BIN\TCS.cds'
    FieldDefs = <
      item
        Name = 'IDC'
        DataType = ftInteger
      end
      item
        Name = 'IDT'
        DataType = ftInteger
      end
      item
        Name = 'ID'
        DataType = ftInteger
      end
      item
        Name = 'IDCARD'
        DataType = ftInteger
      end
      item
        Name = 'CURMESSURE'
        DataType = ftInteger
      end
      item
        Name = 'NETTO'
        DataType = ftFloat
      end
      item
        Name = 'BRUTTO'
        DataType = ftFloat
      end
      item
        Name = 'KNB'
        DataType = ftFloat
      end
      item
        Name = 'NAME'
        DataType = ftString
        Size = 70
      end>
    IndexDefs = <
      item
        Name = 'TCSIndex1'
        Fields = 'IDC;IDT;ID'
      end>
    IndexFieldNames = 'IDC;IDT;ID'
    Params = <>
    StoreDefs = True
    Left = 24
    Top = 136
    object TCSIDC: TIntegerField
      FieldName = 'IDC'
    end
    object TCSIDT: TIntegerField
      FieldName = 'IDT'
    end
    object TCSID: TIntegerField
      FieldName = 'ID'
    end
    object TCSIDCARD: TIntegerField
      FieldName = 'IDCARD'
    end
    object TCSCURMESSURE: TIntegerField
      FieldName = 'CURMESSURE'
    end
    object TCSNETTO: TFloatField
      FieldName = 'NETTO'
    end
    object TCSBRUTTO: TFloatField
      FieldName = 'BRUTTO'
    end
    object TCSKNB: TFloatField
      FieldName = 'KNB'
    end
    object TCSNAME: TStringField
      FieldName = 'NAME'
      Size = 70
    end
  end
  object dsTCH: TDataSource
    DataSet = TCH
    Left = 88
    Top = 72
  end
  object dsTCS: TDataSource
    DataSet = TCS
    Left = 88
    Top = 136
  end
  object frRepTTK: TfrReport
    InitialZoom = pzDefault
    PreviewButtons = [pbZoom, pbLoad, pbSave, pbPrint, pbFind, pbHelp, pbExit]
    RebuildPrinter = False
    Left = 40
    Top = 353
    ReportForm = {19000000}
  end
  object frtaTTK: TfrDBDataSet
    DataSet = taTTK
    Left = 40
    Top = 417
  end
  object taTTK: TClientDataSet
    Aggregates = <>
    FileName = 'ttk.cds'
    Params = <>
    Left = 240
    Top = 297
    object taTTKNum: TIntegerField
      FieldName = 'Num'
    end
    object taTTKName: TStringField
      FieldName = 'Name'
      Size = 150
    end
    object taTTKSM: TStringField
      FieldName = 'SM'
      Size = 15
    end
    object taTTKQuantB: TFloatField
      FieldName = 'QuantB'
      DisplayFormat = '0.###'
    end
    object taTTKQuantN: TFloatField
      FieldName = 'QuantN'
      DisplayFormat = '0.###'
    end
    object taTTKQuantN10: TFloatField
      FieldName = 'QuantN10'
      DisplayFormat = '0.###'
    end
    object taTTKQuantN20: TFloatField
      FieldName = 'QuantN20'
      DisplayFormat = '0.###'
    end
  end
  object amTCard: TActionManager
    Left = 240
    Top = 361
    StyleName = 'XP Style'
    object acIns: TAction
      Caption = #1042#1089#1090#1072#1074#1080#1090#1100
      ShortCut = 45
      OnExecute = acInsExecute
    end
    object acEdit: TAction
      Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100
      ShortCut = 115
      OnExecute = acEditExecute
    end
    object acDel: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ShortCut = 46
      OnExecute = acDelExecute
    end
    object acSave: TAction
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
      ShortCut = 16467
      OnExecute = acSaveExecute
    end
  end
  object taTSpec: TClientDataSet
    Aggregates = <>
    FileName = 'ttkspec.cds'
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'taTSpecIndex1'
        Fields = 'Num'
      end>
    IndexName = 'taTSpecIndex1'
    Params = <>
    StoreDefs = True
    Left = 168
    Top = 297
    object taTSpecNum: TIntegerField
      FieldName = 'Num'
    end
    object taTSpecIdCard: TIntegerField
      FieldName = 'IdCard'
    end
    object taTSpecIdM: TIntegerField
      FieldName = 'IdM'
    end
    object taTSpecSM: TStringField
      FieldName = 'SM'
    end
    object taTSpecKm: TFloatField
      FieldName = 'Km'
    end
    object taTSpecName: TStringField
      FieldName = 'Name'
      Size = 200
    end
    object taTSpecNetto: TFloatField
      FieldName = 'Netto'
      OnChange = taTSpecNettoChange
      DisplayFormat = '0.000'
    end
    object taTSpecBrutto: TFloatField
      FieldName = 'Brutto'
      DisplayFormat = '0.000'
    end
    object taTSpecKnb: TFloatField
      FieldName = 'Knb'
    end
  end
  object dstaTSpec: TDataSource
    DataSet = taTSpec
    Left = 168
    Top = 353
  end
end
