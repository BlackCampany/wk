unit FCards;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, Menus, cxLookAndFeelPainters,
  StdCtrls, cxButtons, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ActnList, XPStyleActnCtrls, ActnMan,
  cxImageComboBox;

type
  TfmFCards = class(TForm)
    ViewFCards: TcxGridDBTableView;
    LevelFCards: TcxGridLevel;
    GridFCards: TcxGrid;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    ViewFCardsID: TcxGridDBColumn;
    ViewFCardsNAME: TcxGridDBColumn;
    Label1: TLabel;
    ActionManager1: TActionManager;
    Action1: TAction;
    ViewFCardsTCARD: TcxGridDBColumn;
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Action1Execute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmFCards: TfmFCards;

implementation

uses dmOffice;

{$R *.dfm}

procedure TfmFCards.cxButton1Click(Sender: TObject);
begin
  ModalResult:=mrOk;
end;

procedure TfmFCards.cxButton2Click(Sender: TObject);
begin
  Close;
end;

procedure TfmFCards.FormShow(Sender: TObject);
begin
  Label1.Caption:='����� - '+INtToStr(dmO.quFCard.RecordCount);
end;

procedure TfmFCards.Action1Execute(Sender: TObject);
begin
  ModalResult:=mrNone;
  Close;
end;

end.
