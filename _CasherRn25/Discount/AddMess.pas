unit AddMess;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, dxfBackGround, ExtCtrls, Menus, cxLookAndFeelPainters, StdCtrls,
  cxButtons, cxMaskEdit, cxDropDownEdit, cxCalc, cxControls, cxContainer,
  cxEdit, cxTextEdit, cxRadioGroup;

type
  TfmAddMess = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    dxfBackGround1: TdxfBackGround;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxTextEdit1: TcxTextEdit;
    cxTextEdit2: TcxTextEdit;
    cxCalcEdit1: TcxCalcEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    cxRButton1: TcxRadioButton;
    cxRButton2: TcxRadioButton;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAddMess: TfmAddMess;

implementation

{$R *.dfm}

end.
