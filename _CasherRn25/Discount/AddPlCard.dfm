object fmAddPlCard: TfmAddPlCard
  Left = 528
  Top = 138
  Width = 425
  Height = 485
  Caption = 'fmAddPlCard'
  Color = clGray
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 20
    Width = 50
    Height = 13
    Caption = #1053#1072#1079#1074#1072#1085#1080#1077
    Transparent = True
  end
  object Label2: TLabel
    Left = 16
    Top = 52
    Width = 53
    Height = 13
    Caption = #1050#1086#1076' '#1082#1072#1088#1090#1099
    Transparent = True
  end
  object Label3: TLabel
    Left = 16
    Top = 84
    Width = 82
    Height = 13
    Caption = #1058#1080#1087' '#1087#1083#1072#1090'. '#1082#1072#1088#1090#1099
    Transparent = True
  end
  object Label4: TLabel
    Left = 16
    Top = 132
    Width = 53
    Height = 13
    Caption = #1042#1080#1076' '#1082#1072#1088#1090#1099
    Transparent = True
  end
  object Panel3: TPanel
    Left = 0
    Top = 168
    Width = 417
    Height = 169
    Color = clGradientInactiveCaption
    TabOrder = 7
    object LabelBALANCECARD: TLabel
      Left = 16
      Top = 100
      Width = 103
      Height = 13
      Caption = #1050#1086#1076' '#1075#1086#1083#1086#1074#1085#1086#1081' '#1082#1072#1088#1090#1099
      Transparent = True
    end
    object LabelDAYLIMIT: TLabel
      Left = 8
      Top = 44
      Width = 79
      Height = 13
      Caption = #1044#1085#1077#1074#1085#1086#1081' '#1083#1080#1084#1080#1090
      Transparent = True
    end
    object LabelBalans: TLabel
      Left = 8
      Top = 20
      Width = 71
      Height = 13
      Caption = #1041#1072#1083#1072#1085#1089' '#1082#1072#1088#1090#1099
      Transparent = True
    end
    object Label5: TLabel
      Left = 16
      Top = 140
      Width = 65
      Height = 13
      Caption = #1058#1080#1087' '#1087#1083#1072#1090#1077#1078#1072
      Transparent = True
    end
    object cxCheckBoxGol: TcxCheckBox
      Left = 8
      Top = 68
      Caption = #1059#1095#1077#1090' '#1073#1072#1083#1072#1085#1089#1072' '#1085#1072' '#1075#1086#1083#1086#1074#1085#1086#1081' '#1082#1072#1088#1090#1077
      State = cbsChecked
      TabOrder = 0
      Transparent = True
      OnClick = cxCheckBoxGolClick
      Width = 241
    end
    object cxTextEditBALANCECARD: TcxTextEdit
      Left = 136
      Top = 100
      TabOrder = 1
      Text = 'cxTextEditBALANCECARD'
      Width = 129
    end
    object cxLookupComboBoxCategSale: TcxLookupComboBox
      Left = 136
      Top = 136
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Caption = #1053#1072#1079#1074#1072#1085#1080#1077
          FieldName = 'NAMECS'
        end>
      Properties.ListSource = dmCDisc.dsCategSaleSel
      EditValue = 0
      TabOrder = 2
      Width = 193
    end
    object cxPCHist: TcxButton
      Left = 288
      Top = 52
      Width = 109
      Height = 25
      Caption = #1048#1089#1090#1086#1088#1080#1103
      Default = True
      ModalResult = 1
      TabOrder = 3
      OnClick = cxPCHistClick
      Colors.Default = 16765650
      Colors.Normal = 16765650
      Colors.Pressed = 16744448
      LookAndFeel.Kind = lfFlat
    end
    object cxCalcEdit1: TcxCalcEdit
      Left = 136
      Top = 40
      EditValue = 0.000000000000000000
      Properties.Alignment.Horz = taRightJustify
      Properties.DisplayFormat = '0.00'
      Style.BorderStyle = ebsOffice11
      Style.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 4
      Width = 93
    end
    object cxCalcEdit2: TcxCalcEdit
      Left = 136
      Top = 16
      EditValue = 0.000000000000000000
      Properties.Alignment.Horz = taRightJustify
      Properties.DisplayFormat = '0.00'
      Properties.ReadOnly = True
      Style.BorderStyle = ebsOffice11
      Style.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 5
      Width = 93
    end
    object cxButton3: TcxButton
      Left = 288
      Top = 16
      Width = 109
      Height = 25
      Caption = #1042#1085#1077#1089#1077#1085#1080#1077' '#1076#1077#1085#1077#1075
      Default = True
      ModalResult = 1
      TabOrder = 6
      Visible = False
      OnClick = cxButton3Click
      Colors.Default = 16765650
      Colors.Normal = 16765650
      Colors.Pressed = 16744448
      LookAndFeel.Kind = lfFlat
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 432
    Width = 417
    Height = 19
    Panels = <
      item
        Width = 300
      end
      item
        Width = 50
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 365
    Width = 417
    Height = 67
    Align = alBottom
    BevelInner = bvLowered
    Color = 16747660
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 144
      Top = 24
      Width = 89
      Height = 25
      Caption = #1054#1082
      Default = True
      ModalResult = 1
      TabOrder = 0
      Colors.Default = 16765650
      Colors.Normal = 16765650
      Colors.Pressed = 16744448
      LookAndFeel.Kind = lfFlat
    end
    object cxButton2: TcxButton
      Left = 248
      Top = 24
      Width = 89
      Height = 25
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      OnClick = cxButton2Click
      Colors.Default = 16765650
      Colors.Normal = 16765650
      Colors.Pressed = 16744448
      Glyph.Data = {
        5E040000424D5E04000000000000360000002800000012000000130000000100
        18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
        8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
        0000CED3D6848684848684848684848684848684848684848684848684848684
        848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
        7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
        FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
        FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
        007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
        FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
        00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
        494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
        00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000}
      LookAndFeel.Kind = lfFlat
    end
  end
  object cxTextEdit1: TcxTextEdit
    Left = 88
    Top = 16
    TabOrder = 1
    Text = 'cxTextEdit1'
    Width = 241
  end
  object cxTextEdit2: TcxTextEdit
    Left = 120
    Top = 48
    Properties.OnEditValueChanged = cxTextEdit2PropertiesEditValueChanged
    TabOrder = 2
    Text = 'cxTextEdit2'
    Width = 129
  end
  object CheckBox1: TcxCheckBox
    Left = 8
    Top = 344
    Caption = #1040#1082#1090#1080#1074#1085#1072#1103
    State = cbsChecked
    TabOrder = 3
    Transparent = True
    Width = 121
  end
  object cxLookupComboBox1: TcxLookupComboBox
    Left = 120
    Top = 80
    Properties.KeyFieldNames = 'ID'
    Properties.ListColumns = <
      item
        Caption = #1058#1080#1087' '#1055#1083'.'#1050#1072#1088#1090#1099
        FieldName = 'NAME'
      end>
    Properties.ListSource = dmPC.dsTypePl
    EditValue = 0
    TabOrder = 4
    Width = 209
  end
  object cxComboBoxTypeOpl: TcxComboBox
    Left = 120
    Top = 128
    Properties.Items.Strings = (
      #1050#1088#1077#1076#1080#1090#1086#1074#1072#1103' '#1082#1072#1088#1090#1072
      #1044#1077#1073#1077#1090#1086#1074#1072#1103' '#1082#1072#1088#1090#1072
      #1044#1077#1073#1077#1090#1086#1074#1072#1103'/'#1057#1082#1080#1076#1082#1072' '#1082#1072#1088#1090#1072)
    TabOrder = 5
    Text = 'cxComboBoxTypeOpl'
    OnClick = cxComboBoxTypeOplClick
    Width = 209
  end
  object dxfBackGround1: TdxfBackGround
    BkColor.BeginColor = 16759739
    BkColor.EndColor = 16747660
    BkColor.FillStyle = fsVert
    BkAnimate.Speed = 700
    Left = 336
    Top = 8
  end
end
