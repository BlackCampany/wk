unit prdb;

interface

uses
  SysUtils, Classes, FIBDatabase, pFIBDatabase, FIBQuery, pFIBQuery,
  pFIBStoredProc,
  DB, FIBDataSet, pFIBDataSet;

type
  TdmPC = class(TDataModule)
    PCDb: TpFIBDatabase;
    trSelPC: TpFIBTransaction;
    trUpdPC: TpFIBTransaction;
    prGetLim: TpFIBStoredProc;
    prWritePC: TpFIBStoredProc;
    dsPCard: TDataSource;
    taPCard: TpFIBDataSet;
    taTypePl: TpFIBDataSet;
    taTypePlID: TFIBIntegerField;
    taTypePlNAME: TFIBStringField;
    taTypePlDATEFROM: TFIBDateField;
    taTypePlDATETO: TFIBDateField;
    taTypePlCOMMENT: TFIBStringField;
    taTypePlCEHPIT: TFIBSmallIntField;
    dsTypePl: TDataSource;
    trDelPC: TpFIBTransaction;
    quMaxIdTPL: TpFIBDataSet;
    quMaxIdTPLMAXID: TFIBIntegerField;
    quFindTPL: TpFIBDataSet;
    quFindTPLCOUNTREC: TFIBIntegerField;
    taCategSaleSel: TpFIBDataSet;
    taCategSaleSelID: TFIBIntegerField;
    taCategSaleSelNAMECS: TFIBStringField;
    dsCategSaleSel: TDataSource;
    dsPCHist: TDataSource;
    quPCHist: TpFIBDataSet;
    quPCHistBARCODE: TFIBStringField;
    quPCHistDATEOP: TFIBDateField;
    quPCHistTYPEOP: TFIBSmallIntField;
    quPCHistSTATION: TFIBIntegerField;
    quPCHistIDPERSON: TFIBIntegerField;
    quPCHistPERSNAME: TFIBStringField;
    quPCHistDBNUM: TFIBSmallIntField;
    quPCHistBARCLI: TFIBStringField;
    quPCHistRSUM: TFIBFloatField;
    quFindPC: TpFIBDataSet;
    quFindPCCLINAME: TFIBStringField;
    taPCardBARCODE: TFIBStringField;
    taPCardPLATTYPE: TFIBIntegerField;
    taPCardIACTIVE: TFIBSmallIntField;
    taPCardCLINAME: TFIBStringField;
    taPCardTYPEOPL: TFIBSmallIntField;
    taPCardBALANS: TFIBFloatField;
    taPCardDAYLIMIT: TFIBFloatField;
    taPCardBALANCECARD: TFIBStringField;
    taPCardSALET: TFIBIntegerField;
    taPCardNAME: TFIBStringField;
    taPCardDATEFROM: TFIBDateField;
    taPCardDATETO: TFIBDateField;
    taPCardCOMMENT: TFIBStringField;
    taPCardVIDOPL: TStringField;
    quFindMovePC: TpFIBDataSet;
    quFindMovePCBARCODE: TFIBStringField;
    quFindMovePCDATEOP: TFIBDateField;
    quFindMovePCID: TFIBIntegerField;
    quFindMovePCRSUM: TFIBFloatField;
    quFindMovePCTYPEOP: TFIBSmallIntField;
    quFindMovePCSTATION: TFIBIntegerField;
    quFindMovePCIDPERSON: TFIBIntegerField;
    quFindMovePCDATEACTION: TFIBDateField;
    quFindMovePCPERSNAME: TFIBStringField;
    quFindMovePCDBNUM: TFIBSmallIntField;
    quFindMovePCBARCLI: TFIBStringField;
    quFindMovePCIDTAB: TFIBIntegerField;
    prInpMoney: TpFIBStoredProc;
    trSel1: TpFIBTransaction;
    trUpd1: TpFIBTransaction;
    quPCHistCLIBALANCE: TFIBStringField;
    quPCHistCLINAME1: TFIBStringField;
    quBalansPC: TpFIBDataSet;
    quBalansPCBARCODE: TFIBStringField;
    quBalansPCSUM: TFIBFloatField;
    dsquBalansPC: TDataSource;
    quBalansPCPLATTYPE: TFIBIntegerField;
    quBalansPCIACTIVE: TFIBSmallIntField;
    quBalansPCCLINAME: TFIBStringField;
    quBalansPCTYPEOPL: TFIBSmallIntField;
    quBalansPCDAYLIMIT: TFIBFloatField;
    quBalansPCBALANCECARD: TFIBStringField;
    quBalansPCSALET: TFIBSmallIntField;
    quBalansPCNAME: TFIBStringField;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure taPCardCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

Procedure prFindPCardBalans(sBar:String; Var rBalans,rBalansDay,DLimit:Real; Var CliName,CliType:String );
Procedure prWritePCSum(sBar:String;PCSUM:Real;IDT:INteger);
function prFindPC(sBar:String):Boolean;
function prTestMovePC(sBar:String):Boolean;

var
  dmPC: TdmPC;

implementation

uses Un1;

{$R *.dfm}

function prTestMovePC(sBar:String):Boolean;
begin
  with dmPC do
  begin
    Result:=False;
    quFindMovePC.Active:=False;
    quFindMovePC.ParamByName('SBAR').AsString:=sBar;
    quFindMovePC.Active:=True;
    if quFindMovePC.RecordCount>0 then Result:=True;
    quFindMovePC.Active:=False;
  end;
end;


function prFindPC(sBar:String):Boolean;
begin
  with dmPC do
  begin
    Result:=False;
    quFindPC.Active:=False;
    quFindPC.ParamByName('SBAR').AsString:=sBar;
    quFindPC.Active:=True;
    if quFindPC.RecordCount>0 then Result:=True;
    quFindPC.Active:=False;
  end;
end;

Procedure prWritePCSum(sBar:String;PCSUM:Real;IDT:INteger);
begin
  with dmPC do
  begin
    prWritePC.ParamByName('PCBAR').AsString:=sBar;
    prWritePC.ParamByName('PCSUM').AsFloat:=PCSUM;
    prWritePC.ParamByName('STATION').AsINteger:=CommonSet.Station;
    prWritePC.ParamByName('IDPERSON').AsINteger:=Person.Id;
    prWritePC.ParamByName('PERSNAME').AsString:=Person.Name;
    prWritePC.ParamByName('ID_TAB').AsINteger:=IDT;

    //ECUTE PROCEDURE PRSAVEPC (?PCBAR, ?PCSUM, ?STATION, ?IDPERSON, ?PERSNAME, ID_TAB)
    prWritePC.ExecProc;
  end;
end;


Procedure prFindPCardBalans(sBar:String; Var rBalans,rBalansDay,DLimit:Real; Var CliName,CliType:String );
begin
  with dmPC do
  begin
    prGetLim.ParamByName('PCBAR').AsString:=sBar;
    prGetLim.ExecProc;
    delay(33);
    rBalans:=prGetLim.ParamByName('BALANS').Value;
    rBalansDay:=prGetLim.ParamByName('BALANSDAY').Value;
    DLimit:=prGetLim.ParamByName('DLIMIT').Value;
    CliName:=prGetLim.ParamByName('CLINAME').Value;
    CliType:=prGetLim.ParamByName('CLITYPE').Value;
  end;
end;


procedure TdmPC.DataModuleCreate(Sender: TObject);
begin
{  PCDb.Connected:=False;
  PCDb.DBName:=DBNamePC;
  try
    PCDb.Open;
  except
  end;}
end;

procedure TdmPC.DataModuleDestroy(Sender: TObject);
begin
  PCDb.Close;
end;

procedure TdmPC.taPCardCalcFields(DataSet: TDataSet);
begin
 if taPCardTYPEOPL.AsInteger = 1 then
  taPCardVIDOPL.AsString:='���������� �����'
 else if taPCardTYPEOPL.AsInteger = 2 then
  taPCardVIDOPL.AsString:='��������� �����'
 else if taPCardTYPEOPL.AsInteger = 3 then
  taPCardVIDOPL.AsString:='���������/������ �����'
 else
  taPCardVIDOPL.AsString:='������!!!!!';

end;

end.

