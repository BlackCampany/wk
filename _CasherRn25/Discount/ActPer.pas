unit ActPer;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SpeedBar, ExtCtrls, ComCtrls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Placemnt, cxImageComboBox, XPStyleActnCtrls, ActnList, ActnMan, Menus,
  FR_DSet, FR_DBSet, FR_Class;

type
  TfmDocsActs = class(TForm)
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    Timer1: TTimer;
    FormPlacement1: TFormPlacement;
    GridActs: TcxGrid;
    ViewActs: TcxGridDBTableView;
    LevelActs: TcxGridLevel;
    ViewActsID: TcxGridDBColumn;
    ViewActsDATEDOC: TcxGridDBColumn;
    ViewActsNUMDOC: TcxGridDBColumn;
    ViewActsIDSKL: TcxGridDBColumn;
    ViewActsSUMIN: TcxGridDBColumn;
    ViewActsSUMUCH: TcxGridDBColumn;
    ViewActsNAMEMH: TcxGridDBColumn;
    ViewActsIACTIVE: TcxGridDBColumn;
    amDocsIn: TActionManager;
    acPeriod: TAction;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    SpeedItem6: TSpeedItem;
    acAddDoc1: TAction;
    acEditAct: TAction;
    acViewAct: TAction;
    acDelDoc1: TAction;
    acOnDoc1: TAction;
    acOffDoc1: TAction;
    SpeedItem7: TSpeedItem;
    SpeedItem8: TSpeedItem;
    acVid: TAction;
    SpeedItem9: TSpeedItem;
    LevelCardsActs: TcxGridLevel;
    ViewCardsActs: TcxGridDBTableView;
    ViewCardsActsNAME: TcxGridDBColumn;
    ViewCardsActsNAMESHORT: TcxGridDBColumn;
    ViewCardsActsIDCARD: TcxGridDBColumn;
    ViewCardsActsQUANT: TcxGridDBColumn;
    ViewCardsActsPRICEIN: TcxGridDBColumn;
    ViewCardsActsSUMIN: TcxGridDBColumn;
    ViewCardsActsPRICEUCH: TcxGridDBColumn;
    ViewCardsActsSUMUCH: TcxGridDBColumn;
    ViewCardsActsIDNDS: TcxGridDBColumn;
    ViewCardsActsSUMNDS: TcxGridDBColumn;
    ViewCardsActsDATEDOC: TcxGridDBColumn;
    ViewCardsActsNUMDOC: TcxGridDBColumn;
    ViewCardsActsNAMECL: TcxGridDBColumn;
    ViewCardsActsNAMEMH: TcxGridDBColumn;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    acPrint1: TAction;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    acCopy: TAction;
    acInsertD: TAction;
    procedure SpeedItem1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acPeriodExecute(Sender: TObject);
    procedure acAddDoc1Execute(Sender: TObject);
    procedure acEditActExecute(Sender: TObject);
    procedure acViewActExecute(Sender: TObject);
    procedure ViewActsDblClick(Sender: TObject);
    procedure acDelDoc1Execute(Sender: TObject);
    procedure acOnDoc1Execute(Sender: TObject);
    procedure acOffDoc1Execute(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure acVidExecute(Sender: TObject);
    procedure SpeedItem1Click0(Sender: TObject);
    procedure acCopyExecute(Sender: TObject);
    procedure acInsertDExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmDocsActs: TfmDocsActs;
  bClearActs:Boolean = false;

implementation

uses Un1, dmOffice, PeriodUni, AddDoc1, DMOReps, TBuff, AddAct;

{$R *.dfm}

procedure TfmDocsActs.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmDocsActs.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  Timer1.Enabled:=True;
  GridActs.Align:=AlClient;
  ViewActs.RestoreFromIniFile(CurDir+GridIni);
  ViewCardsActs.RestoreFromIniFile(CurDir+GridIni);
  StatusBar1.Color:=$00FFCACA;
end;

procedure TfmDocsActs.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewActs.StoreToIniFile(CurDir+GridIni,False);
  ViewCardsActs.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmDocsActs.acPeriodExecute(Sender: TObject);
begin
//  ������
  fmPeriodUni.DateTimePicker1.Date:=CommonSet.DateFrom;
//  fmPeriodUni.DateTimePicker2.Date:=CommonSet.DateTo-1;
  fmPeriodUni.ShowModal;
  if fmPeriodUni.ModalResult=mrOk then
  begin
    CommonSet.DateFrom:=Trunc(fmPeriodUni.DateTimePicker1.Date);
    CommonSet.DateTo:=Trunc(fmPeriodUni.DateTimePicker2.Date)+1;

    with dmO do
    with dmORep do
    begin
      if LevelActs.Visible then
      begin
        if CommonSet.DateTo>=iMaxDate then fmDocsActs.Caption:='���� ����������� � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
        else fmDocsActs.Caption:='���� ����������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);

        ViewActs.BeginUpdate;
        quDocsActs.Active:=False;
        quDocsActs.ParamByName('DATEB').AsDate:=CommonSet.DateFrom;
        quDocsActs.ParamByName('DATEE').AsDate:=CommonSet.DateTo;
        quDocsActs.Active:=True;
        ViewActs.EndUpdate;
      end else
      begin
        if CommonSet.DateTo>=iMaxDate then fmDocsActs.Caption:='���� ����������� �� ������� � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
        else fmDocsActs.Caption:='���� ����������� �� ������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);

        ViewCardsActs.BeginUpdate;
{        quDocsInCard.Active:=False;
        quDocsInCard.ParamByName('DATEB').AsDate:=CommonSet.DateFrom;
        quDocsInCard.ParamByName('DATEE').AsDate:=CommonSet.DateTo;
        quDocsInCard.Active:=True;}
        ViewCardsActs.EndUpdate;
      end;
    end;
  end;
end;

procedure TfmDocsActs.acAddDoc1Execute(Sender: TObject);
begin
  //�������� ��������
  if not CanDo('prAddAct') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmO do
  with dmORep do
  begin
    fmAddAct.Caption:='���� �����������: ����� ��������.';
    fmAddAct.cxTextEdit1.Text:='';
    fmAddAct.cxTextEdit1.Properties.ReadOnly:=False;
    fmAddAct.cxTextEdit1.Tag:=0; //������� ���������� ��������� 
    fmAddAct.cxDateEdit1.Date:=Date;
    fmAddAct.cxDateEdit1.Properties.ReadOnly:=False;

    if quMHAll.Active=False then quMHAll.Active:=True;
    quMHAll.FullRefresh;

    fmAddAct.cxLookupComboBox1.EditValue:=0;
    fmAddAct.cxLookupComboBox1.Text:='';
    fmAddAct.cxLookupComboBox1.Properties.ReadOnly:=False;

    if CurVal.IdMH<>0 then
    begin
      fmAddAct.cxLookupComboBox1.EditValue:=CurVal.IdMH;
      fmAddAct.cxLookupComboBox1.Text:=CurVal.NAMEMH;
    end else
    begin
       quMHAll.First;
       if not quMHAll.Eof then
       begin
         CurVal.IdMH:=quMHAllID.AsInteger;
         CurVal.NAMEMH:=quMHAllNAMEMH.AsString;
         fmAddAct.cxLookupComboBox1.EditValue:=CurVal.IdMH;
         fmAddAct.cxLookupComboBox1.Text:=CurVal.NAMEMH;
       end;
    end;
    if quMHAll.Locate('ID',CurVal.IdMH,[]) then
    begin
      fmAddAct.Label15.Caption:='��. ����: '+quMHAllNAMEPRICE.AsString;
      fmAddAct.Label15.Tag:=quMHAllDEFPRICE.AsInteger;
    end else
    begin
      fmAddAct.Label15.Caption:='��. ����: ';
      fmAddAct.Label15.Tag:=0;
    end;

    fmAddAct.cxLabel1.Enabled:=True;
    fmAddAct.cxLabel2.Enabled:=True;
    fmAddAct.cxLabel7.Enabled:=True;
    fmAddAct.cxLabel9.Enabled:=True;
    fmAddAct.Label3.Enabled:=True;

    fmAddAct.cxButton1.Enabled:=True;
    fmAddAct.taSpecO.Active:=False;
    fmAddAct.taSpecO.CreateDataSet;
    fmAddAct.taSpecI.Active:=False;
    fmAddAct.taSpecI.CreateDataSet;

    fmAddAct.ViewAO.OptionsData.Editing:=True;
    fmAddAct.ViewAI.OptionsData.Editing:=True;

    fmAddAct.ShowModal;
  end;
end;

procedure TfmDocsActs.acEditActExecute(Sender: TObject);
Var IDH:INteger;
//    rSum1,rSum2:Real;
begin
  //�������������
  if not CanDo('prEditAct') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmO do
  with dmORep do
  begin
    if quDocsActs.RecordCount>0 then //���� ��� �������������
    begin
      if quDocsActsIACTIVE.AsInteger=0 then
      begin
        fmAddAct.Caption:='���� �����������: ��������������.';
        fmAddAct.cxTextEdit1.Text:=quDocsActsNUMDOC.AsString;
        fmAddAct.cxTextEdit1.Properties.ReadOnly:=False;
        fmAddAct.cxTextEdit1.Tag:=quDocsActsID2.AsInteger; //������� ���������� ��������� ���� 0

        fmAddAct.cxDateEdit1.Date:=quDocsActsDATEDOC.AsDateTime;
        fmAddAct.cxDateEdit1.Properties.ReadOnly:=False;

        if quMHAll.Active=False then quMHAll.Active:=True;
        quMHAll.FullRefresh;

        fmAddAct.cxLookupComboBox1.EditValue:=quDocsActsIDSKL.asinteger;
        fmAddAct.cxLookupComboBox1.Text:=quDocsActsNAMEMH.AsString;
        fmAddAct.cxLookupComboBox1.Properties.ReadOnly:=False;

        CurVal.IdMH:=quDocsActsIDSKL.asinteger;
        CurVal.NAMEMH:=quDocsActsNAMEMH.AsString;

        fmAddAct.cxLabel1.Enabled:=True;
        fmAddAct.cxLabel2.Enabled:=True;
        fmAddAct.cxLabel7.Enabled:=True;
        fmAddAct.cxLabel9.Enabled:=True;
        fmAddAct.Label3.Enabled:=True;

        fmAddAct.cxButton1.Enabled:=True;

        fmAddAct.taSpecO.Active:=False;
        fmAddAct.taSpecO.CreateDataSet;
        fmAddAct.taSpecI.Active:=False;
        fmAddAct.taSpecI.CreateDataSet;

        IDH:=quDocsActsID2.AsInteger;

        quSpecAO.Active:=False;
        quSpecAO.ParamByName('IDH').AsInteger:=IDH;
        quSpecAO.Active:=True;

        quSpecAO.First;
        while not quSpecAO.Eof do
        begin
          with fmAddAct do
          begin
            taSpecO.Append;
            taSpecONum.AsInteger:=quSpecAOID.AsInteger;
            taSpecOIdGoods.AsInteger:=quSpecAOIDCARD.AsInteger;
            taSpecONameG.AsString:=quSpecAONAME.AsString;
            taSpecOIM.AsInteger:=quSpecAOIDM.AsInteger;
            taSpecOSM.AsString:=quSpecAONAMESHORT.AsString;
            taSpecOQuant.AsFloat:=quSpecAOQUANT.AsFloat;
            taSpecOPriceIn.AsFloat:=quSpecAOPRICEIN.AsFloat;
            taSpecOSumIn.AsFloat:=quSpecAOSUMIN.AsFloat;
            taSpecOPriceUch.AsFloat:=quSpecAOPRICEINUCH.AsFloat;
            taSpecOSumUch.AsFloat:=quSpecAOSUMINUCH.AsFloat;
            taSpecOKm.AsFloat:=quSpecAOKM.AsFloat;
            taSpecO.Post;
          end;
          quSpecAO.Next;
        end;

        quSpecAO.Active:=False;

        quSpecAI.Active:=False;
        quSpecAI.ParamByName('IDH').AsInteger:=IDH;
        quSpecAI.Active:=True;

        quSpecAI.First;
        while not quSpecAI.Eof do
        begin
          with fmAddAct do
          begin
            taSpecI.Append;
            taSpecINum.AsInteger:=quSpecAIID.AsInteger;
            taSpecIIdGoods.AsInteger:=quSpecAIIDCARD.AsInteger;
            taSpecINameG.AsString:=quSpecAINAME.AsString;
            taSpecIIM.AsInteger:=quSpecAIIDM.AsInteger;
            taSpecISM.AsString:=quSpecAINAMESHORT.AsString;
            taSpecIQuant.AsFloat:=quSpecAIQUANT.AsFloat;
            taSpecIPriceIn.AsFloat:=quSpecAIPRICEIN.AsFloat;
            taSpecISumIn.AsFloat:=quSpecAISUMIN.AsFloat;
            taSpecIPriceUch.AsFloat:=quSpecAIPRICEINUCH.AsFloat;
            taSpecISumUch.AsFloat:=quSpecAISUMINUCH.AsFloat;
            taSpecIKm.AsFloat:=quSpecAIKM.AsFloat;
            taSpecI.Post;
          end;
          quSpecAI.Next;
        end;

        quSpecAI.Active:=False;

        fmAddAct.ViewAO.OptionsData.Editing:=True;
        fmAddAct.ViewAI.OptionsData.Editing:=True;

        fmAddAct.ShowModal;

      end else
      begin
        showmessage('������������� �������������� �������� ������.');
      end;
    end else
    begin
      showmessage('�������� �������� ��� ��������������.');
    end;
  end;
end;

procedure TfmDocsActs.acViewActExecute(Sender: TObject);
Var IDH:INteger;
//    rSum1,rSum2:Real;
begin
  //�������������
  if not CanDo('prViewAct') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmO do
  with dmORep do
  begin
    if quDocsActs.RecordCount>0 then //���� ��� ��������
    begin
      fmAddAct.Caption:='���� �����������: ��������.';
      fmAddAct.cxTextEdit1.Text:=quDocsActsNUMDOC.AsString;
      fmAddAct.cxTextEdit1.Properties.ReadOnly:=True;
      fmAddAct.cxTextEdit1.Tag:=quDocsActsID2.AsInteger; //������� ���������� ��������� ���� 0

      fmAddAct.cxDateEdit1.Date:=quDocsActsDATEDOC.AsDateTime;
      fmAddAct.cxDateEdit1.Properties.ReadOnly:=True;

      if quMHAll.Active=False then quMHAll.Active:=True;
      quMHAll.FullRefresh;

      fmAddAct.cxLookupComboBox1.EditValue:=quDocsActsIDSKL.asinteger;
      fmAddAct.cxLookupComboBox1.Text:=quDocsActsNAMEMH.AsString;
      fmAddAct.cxLookupComboBox1.Properties.ReadOnly:=True;

      CurVal.IdMH:=quDocsActsIDSKL.asinteger;
      CurVal.NAMEMH:=quDocsActsNAMEMH.AsString;

      fmAddAct.cxLabel1.Enabled:=False;
      fmAddAct.cxLabel2.Enabled:=False;
      fmAddAct.cxLabel7.Enabled:=False;
      fmAddAct.cxLabel9.Enabled:=False;
      fmAddAct.Label3.Enabled:=False;

      fmAddAct.cxButton1.Enabled:=False;

      fmAddAct.taSpecO.Active:=False;
      fmAddAct.taSpecO.CreateDataSet;
      fmAddAct.taSpecI.Active:=False;
      fmAddAct.taSpecI.CreateDataSet;

      IDH:=quDocsActsID2.AsInteger;

      quSpecAO.Active:=False;
      quSpecAO.ParamByName('IDH').AsInteger:=IDH;
      quSpecAO.Active:=True;

      quSpecAO.First;
      while not quSpecAO.Eof do
      begin
        with fmAddAct do
        begin
          taSpecO.Append;
          taSpecONum.AsInteger:=quSpecAOID.AsInteger;
          taSpecOIdGoods.AsInteger:=quSpecAOIDCARD.AsInteger;
          taSpecONameG.AsString:=quSpecAONAME.AsString;
          taSpecOIM.AsInteger:=quSpecAOIDM.AsInteger;
          taSpecOSM.AsString:=quSpecAONAMESHORT.AsString;
          taSpecOQuant.AsFloat:=quSpecAOQUANT.AsFloat;
          taSpecOPriceIn.AsFloat:=quSpecAOPRICEIN.AsFloat;
          taSpecOSumIn.AsFloat:=quSpecAOSUMIN.AsFloat;
          taSpecOPriceUch.AsFloat:=quSpecAOPRICEINUCH.AsFloat;
          taSpecOSumUch.AsFloat:=quSpecAOSUMINUCH.AsFloat;
          taSpecOKm.AsFloat:=quSpecAOKM.AsFloat;
          taSpecO.Post;
        end;
        quSpecAO.Next;
      end;

      quSpecAO.Active:=False;

      quSpecAI.Active:=False;
      quSpecAI.ParamByName('IDH').AsInteger:=IDH;
      quSpecAI.Active:=True;

      quSpecAI.First;
      while not quSpecAI.Eof do
      begin
        with fmAddAct do
        begin
          taSpecI.Append;
          taSpecINum.AsInteger:=quSpecAIID.AsInteger;
          taSpecIIdGoods.AsInteger:=quSpecAIIDCARD.AsInteger;
          taSpecINameG.AsString:=quSpecAINAME.AsString;
          taSpecIIM.AsInteger:=quSpecAIIDM.AsInteger;
          taSpecISM.AsString:=quSpecAINAMESHORT.AsString;
          taSpecIQuant.AsFloat:=quSpecAIQUANT.AsFloat;
          taSpecIPriceIn.AsFloat:=quSpecAIPRICEIN.AsFloat;
          taSpecISumIn.AsFloat:=quSpecAISUMIN.AsFloat;
          taSpecIPriceUch.AsFloat:=quSpecAIPRICEINUCH.AsFloat;
          taSpecISumUch.AsFloat:=quSpecAISUMINUCH.AsFloat;
          taSpecIKm.AsFloat:=quSpecAIKM.AsFloat;
          taSpecI.Post;
        end;
        quSpecAI.Next;
      end;

      quSpecAI.Active:=False;

      fmAddAct.ViewAO.OptionsData.Editing:=False;
      fmAddAct.ViewAI.OptionsData.Editing:=False;

      fmAddAct.ShowModal;
    end else
    begin
      showmessage('�������� �������� ��� ���������.');
    end;
  end;
end;

procedure TfmDocsActs.ViewActsDblClick(Sender: TObject);
begin
  //������� �������
  with dmORep do
  begin
    if quDocsActsIACTIVE.AsInteger=0 then acEditAct.Execute //��������������
    else acViewAct.Execute; //��������
  end;
end;

procedure TfmDocsActs.acDelDoc1Execute(Sender: TObject);
begin
  //������� ��������
  if not CanDo('prDelAct') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmO do
  with dmORep do
  begin
    if quDocsActs.RecordCount>0 then //���� ��� �������������
    begin
      if quDocsActsIACTIVE.AsInteger=0 then
      begin
        if MessageDlg('�� ������������� ������ ������� �������� �'+quDocsActsNUMDOC.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          quDocsActs.Delete;
        end;
      end else
      begin
        showmessage('������� �������������� �������� ������.');
      end;
    end else
    begin
      showmessage('�������� �������� ��� ��������.');
    end;
  end;
end;

procedure TfmDocsActs.acOnDoc1Execute(Sender: TObject);
Var IdH:INteger;
    rSumO,rSumI:Real;
    PriceSp,PriceUch,rSumIn,rSumUch,rQs,rQ,rQp,rMessure:Real;

begin
//������������
  if not CanDo('prOnAct') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmO do
  with dmORep do
  begin
    if quDocsActs.RecordCount>0 then //���� ��� ��������
    begin
      if MessageDlg('������������ �������� �'+quDocsActsNUMDOC.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        IDH:=quDocsActsID2.AsInteger;
        rSumO:=0; rSumI:=0;

        quSpecAO.Active:=False;
        quSpecAO.ParamByName('IDH').AsInteger:=IDH;
        quSpecAO.Active:=True;

        //������ ���� ����������� �������, � �� ���������� ����� ����� ������������� ���������, ���� � ������ �� ��� ���
        quSpecAO.First;
        while not quSpecAO.Eof do
        begin
          PriceSp:=0;
          rSumIn:=0;
          rSumUch:=0;
          rQs:=quSpecAOQuant.AsFloat*quSpecAOKm.AsFloat; //�������� � ��������
          prSelPartIn(quSpecAOIDCARD.AsInteger,quDocsActsIDSKL.AsInteger,0);

          quSelPartIn.First;
          if rQs>0 then
          begin
            while (not quSelPartIn.Eof) and (rQs>0) do
            begin
              //���� �� ���� ������� ���� �����, ��������� �������� ���
              rQp:=quSelPartInQREMN.AsFloat;
              if rQs<=rQp then  rQ:=rQs//��������� ������ ������ ���������
                          else  rQ:=rQp;
              rQs:=rQs-rQ;

              PriceSp:=quSelPartInPRICEIN.AsFloat;
              PriceUch:=quSelPartInPRICEOUT.AsFloat;
              rSumIn:=rSumIn+PriceSp*rQ;
              rSumUch:=rSumUch+PriceUch*rQ;
              quSelPartIn.Next;
            end;

            if rQs>0 then //�������� ������������� ������
            begin
              if PriceSp=0 then
              begin //��� ���� ���������� ������� � ���������� ����������
                prCalcLastPrice1.ParamByName('IDGOOD').AsInteger:=quSpecAOIDCARD.AsInteger;
                prCalcLastPrice1.ExecProc;
                PriceSp:=prCalcLastPrice1.ParamByName('PRICEIN').AsCurrency;
                rMessure:=prCalcLastPrice1.ParamByName('KOEF').AsFloat;
                if (rMessure<>0)and(rMessure<>1) then PriceSp:=PriceSp/rMessure;
              end;
              rSumIn:=rSumIn+PriceSp*rQs;
            end;
          end;
          quSelPartIn.Active:=False;

          //�������� ���������
          quSpecAO.Edit;
          if quSpecAOQuant.AsFloat<>0 then
          begin
            quSpecAOSUMIN.AsCurrency:=rSumIn;
            quSpecAOSUMINUCH.AsCurrency:=rSumUch;
            quSpecAOPRICEIN.AsCurrency:=rSumIn/quSpecAOQuant.AsFloat;
            quSpecAOPRICEINUCH.AsCurrency:=rSumUch/quSpecAOQuant.AsFloat;
          end else
          begin
            quSpecAOSUMIN.AsCurrency:=0;
            quSpecAOSUMINUCH.AsCurrency:=0;
            quSpecAOPRICEIN.AsCurrency:=0;
            quSpecAOPRICEINUCH.AsCurrency:=0;
          end;
          quSpecAO.Post;

          quSpecAO.Next; delay(10);
        end;

        quSpecAO.First;
        while not quSpecAO.Eof do
        begin
          rSumO:=rSumO+quSpecAOSumIn.AsFloat;
          quSpecAO.Next;
        end;

        quSpecAI.Active:=False;
        quSpecAI.ParamByName('IDH').AsInteger:=IDH;
        quSpecAI.Active:=True;

        quSpecAI.First;
        while not quSpecAI.Eof do
        begin
          rSumI:=rSumI+quSpecAISumIn.AsFloat;
          quSpecAI.Next;
        end;
        if rSumO=rSumI then
        begin //������ ���� � ��������
          //������
          if prTOFind(Trunc(quDocsActsDATEDOC.AsDateTime),quDocsActsIDSKL.AsInteger)=1
          then
          begin //�� ����
            if MessageDlg('������� ��� �������� ������ �� �� '+quDocsActsNAMEMH.AsString+'  � '+FormatDateTime('dd.mm.yyyy',quDocsActsDATEDOC.AsDateTime)+'?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
              prTODel(Trunc(quDocsActsDATEDOC.AsDateTime),quDocsActsIDSKL.AsInteger)
            else
            begin
              showmessage('��������� ������� ��������� ����������...');
              exit;
            end;
          end;
          //������� ������ - ����� ������

          quSpecAO.First;
          while not quSpecAO.Eof do
          begin

            PriceSp:=0;
            PriceUch:=0;
            rQs:=quSpecAOQUANT.AsFloat*quSpecAOKM.AsFloat; //�������� � ��������
            prSelPartIn(quSpecAOIDCARD.AsInteger,quDocsActsIDSKL.AsInteger,0);

            quSelPartIn.First;
            if rQs>0 then
            begin
              while (not quSelPartIn.Eof) and (rQs>0) do
              begin
               //���� �� ���� ������� ���� �����, ��������� �������� ���
                rQp:=quSelPartInQREMN.AsFloat;
                if rQs<=rQp then  rQ:=rQs//��������� ������ ������ ���������
                            else  rQ:=rQp;
                rQs:=rQs-rQ;

                PriceSp:=quSelPartInPRICEIN.AsFloat;
                PriceUch:=quSelPartInPRICEOUT.AsFloat;

                prAddPartOut.ParamByName('ARTICUL').AsInteger:=quSpecAOIDCARD.AsInteger;
                prAddPartOut.ParamByName('IDDATE').AsInteger:=Trunc(quDocsActsDATEDOC.AsDateTime);
                prAddPartOut.ParamByName('IDSTORE').AsInteger:=quDocsActsIDSKL.AsInteger;
                prAddPartOut.ParamByName('IDPARTIN').AsInteger:=quSelPartInID.AsInteger;
                prAddPartOut.ParamByName('IDDOC').AsInteger:=quDocsActsID2.AsInteger;
                prAddPartOut.ParamByName('IDCLI').AsInteger:=quSelPartInIDCLI.AsInteger;
                prAddPartOut.ParamByName('DTYPE').AsInteger:=5;
                prAddPartOut.ParamByName('QUANT').AsFloat:=rQ;
                prAddPartOut.ParamByName('PRICEIN').AsFloat:=PriceSp;
                prAddPartOut.ParamByName('SUMOUT').AsFloat:=PriceUch*rQ;
                prAddPartout.ExecProc;

                quSelPartIn.Next;
              end;

              if rQs>0 then //�������� ������������� ������, �������� � ������, �� � ������� ��������� ������ ���, ��� � �������������
              begin
                if PriceSp=0 then
                begin //��� ���� ���������� ������� � ���������� ����������
                  prCalcLastPrice1.ParamByName('IDGOOD').AsInteger:=quSpecAOIDCARD.AsInteger;
                  prCalcLastPrice1.ExecProc;
                  PriceSp:=prCalcLastPrice1.ParamByName('PRICEIN').AsCurrency;
                  rMessure:=prCalcLastPrice1.ParamByName('KOEF').AsFloat;
                  if (rMessure<>0)and(rMessure<>1) then PriceSp:=PriceSp/rMessure;

                  prAddPartOut.ParamByName('ARTICUL').AsInteger:=quSpecAOIDCARD.AsInteger;
                  prAddPartOut.ParamByName('IDDATE').AsInteger:=Trunc(quDocsActsDATEDOC.AsDateTime);
                  prAddPartOut.ParamByName('IDSTORE').AsInteger:=quDocsActsIDSKL.AsInteger;
                  prAddPartOut.ParamByName('IDPARTIN').AsInteger:=-1;
                  prAddPartOut.ParamByName('IDDOC').AsInteger:=quDocsActsID2.AsInteger;
                  prAddPartOut.ParamByName('IDCLI').AsInteger:=0;
                  prAddPartOut.ParamByName('DTYPE').AsInteger:=5;
                  prAddPartOut.ParamByName('QUANT').AsFloat:=rQs;
                  prAddPartOut.ParamByName('PRICEIN').AsFloat:=PriceSp;
                  prAddPartOut.ParamByName('SUMOUT').AsFloat:=PriceUch*rQs;
                  prAddPartout.ExecProc;

                end;
              end;
            end;
            quSelPartIn.Active:=False;

            quSpecAO.Next;
          end;

          //������
          quSpecAI.First;
          while not quSpecAI.Eof do
          begin
            rQs:=quSpecAIQUANT.AsFloat*quSpecAIKM.AsFloat; //�������� � ��������

            prAddPartIn1.ParamByName('IDSKL').AsInteger:=quDocsActsIDSKL.AsInteger;
            prAddPartIn1.ParamByName('IDDOC').AsInteger:=quDocsActsID2.AsInteger;
            prAddPartIn1.ParamByName('DTYPE').AsInteger:=5;
            prAddPartIn1.ParamByName('IDATE').AsInteger:=Trunc(quDocsActsDATEDOC.AsDateTime);
            prAddPartIn1.ParamByName('IDCARD').AsInteger:=quSpecAIIDCARD.AsInteger;
            prAddPartIn1.ParamByName('IDCLI').AsInteger:=(-1)*quDocsActsIDSKL.AsInteger;
            prAddPartIn1.ParamByName('QUANT').AsFloat:=rQs;

            PriceSp:=0;
            PriceUch:=0;
            if quSpecAIKM.AsFloat<>0 then
            begin
              PriceSp:=quSpecAIPRICEIN.AsFloat/quSpecAIKM.AsFloat;   //���� � ������� ��
              PriceUch:=quSpecAIPRICEINUCH.AsFloat/quSpecAIKM.AsFloat; //���� � ������� ��
            end;

            prAddPartIn1.ParamByName('PRICEIN').AsFloat:=PriceSp;
            prAddPartIn1.ParamByName('PRICEUCH').AsFloat:=PriceUch;
            prAddPartIn1.ExecProc;

            quSpecAI.Next;
          end;

          //�������� ������ � �����
          quDocsActs.Edit;
          quDocsActsIACTIVE.AsInteger:=1;
          quDocsActsSUMIN.AsFloat:=rSumO;
          quDocsActs.Post;
          quDocsActs.Refresh;

        end else showmessage('������������ �������� ������ - ����� ������� �� ��������� � ������ �������.');

        quSpecAO.Active:=False;
        quSpecAI.Active:=False;
      end;
    end;
  end;
end;

procedure TfmDocsActs.acOffDoc1Execute(Sender: TObject);
Var iCountPartOut:Integer;
    bStart:Boolean;
begin
//��������
  if not CanDo('prOffAct') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmO do
  with dmORep do
  begin
    if quDocsActs.RecordCount>0 then //���� ��� ������������
    begin
      if quDocsActsIACTIVE.AsInteger=1 then
      begin
        if MessageDlg('�������� �������� �'+quDocsActsNUMDOC.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin

          if prTOFind(Trunc(quDocsActsDATEDOC.AsDateTime),quDocsActsIDSKL.AsInteger)=1 then
          begin //�� ����
            if MessageDlg('������� ��� �������� ������ �� �� '+quDocsActsNAMEMH.AsString+'  � '+FormatDateTime('dd.mm.yyyy',quDocsActsDATEDOC.AsDateTime)+'?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
              prTODel(Trunc(quDocsActsDATEDOC.AsDateTime),quDocsActsIDSKL.AsInteger)
            else
            begin
              showmessage('��������� ������� ��������� ����������...');
              exit;
            end;
          end;

         // 1 - ��������� ���� �� �������� � ��������� ������� ������� ���������, �� ������� �  ���������� �����
         // ���� ������ ��
          prFindPartOut.ParamByName('IDDOC').AsInteger:=quDocsActsID2.AsInteger;
          prFindPartOut.ParamByName('DTYPE').AsInteger:=5;
          prFindPartOut.ExecProc;
          iCountPartOut:=prFindPartOut.ParamByName('RESULT').Value;
          if iCountPartOut>0 then
          begin
            if MessageDlg('� ������� ��������� ��������� '+IntToStr(iCountPartOut)+' ��������� ������. �������� ��������.',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
            begin
//               bStart:=True;
//              showmessage('�� ������� ����������� ������������� � '+FormatDateTime('dd.mm.yyyy',quDocsActsDATEDOC.AsDateTime)+' �����.');
              bStart:=False;
            end else
            begin
              bStart:=False;
            end;
          end else bStart:=True;
          if bStart then
          begin
            // 1 - �������� ��������� ������
            // 2 - ������� ��������� ��������� ������ �� ��������� c ���������� ��������������
            prPartInDel.ParamByName('IDDOC').AsInteger:=quDocsActsID2.AsInteger;
            prPartInDel.ParamByName('DTYPE').AsInteger:=5;
            prPartInDel.ExecProc;

            //������� ��������� ������ �� ��������� � ���������� ��������������
            prDelPartOut.ParamByName('IDDOC').AsInteger:=quDocsActsID2.AsInteger;
            prDelPartOut.ParamByName('DTYPE').AsInteger:=5;
            prDelPartOut.ExecProc;

            // 4 - �������� ������
            quDocsActs.Edit;
            quDocsActsIACTIVE.AsInteger:=0;
            quDocsActs.Post;
            quDocsActs.Refresh;
          end;
        end;
      end;
    end;
  end;
end;

procedure TfmDocsActs.Timer1Timer(Sender: TObject);
begin
  if bClearActs=True then begin StatusBar1.Panels[0].Text:=''; bClearActs:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearActs:=True;
end;

procedure TfmDocsActs.acVidExecute(Sender: TObject);
begin
  //���
  with dmO do
  with dmORep do
  begin
{    if LevelDocsIn.Visible then
    begin
      if CommonSet.DateTo>=iMaxDate then fmDocsIn.Caption:='������� �� ������� � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
      else fmDocsIn.Caption:='������� �� ������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);

      LevelDocsIn.Visible:=False;
      LevelCards.Visible:=True;

      ViewCardsActs.BeginUpdate;
      quDocsInCard.Active:=False;
      quDocsInCard.ParamByName('DATEB').AsDate:=CommonSet.DateFrom;
      quDocsInCard.ParamByName('DATEE').AsDate:=CommonSet.DateTo;
      quDocsInCard.Active:=True;
      ViewCardsActs.EndUpdate;

      SpeedItem3.Visible:=False;
      SpeedItem4.Visible:=False;
      SpeedItem5.Visible:=False;
      SpeedItem6.Visible:=False;
      SpeedItem7.Visible:=False;
      SpeedItem8.Visible:=False;

    end else
    begin
      if CommonSet.DateTo>=iMaxDate then fmDocsIn.Caption:='���� ����������� � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
      else fmDocsIn.Caption:='���� ����������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);

      LevelDocsIn.Visible:=True;
      LevelCards.Visible:=False;

      ViewActs.BeginUpdate;
      quDocsActs.Active:=False;
      quDocsActs.ParamByName('DATEB').AsDate:=CommonSet.DateFrom;
      quDocsActs.ParamByName('DATEE').AsDate:=CommonSet.DateTo;
      quDocsActs.Active:=True;
      ViewActs.EndUpdate;

      SpeedItem3.Visible:=True;
      SpeedItem4.Visible:=True;
      SpeedItem5.Visible:=True;
      SpeedItem6.Visible:=True;
      SpeedItem7.Visible:=True;
      SpeedItem8.Visible:=True;

    end;}
  end;
end;

procedure TfmDocsActs.SpeedItem1Click0(Sender: TObject);
begin
  Close;
end;

procedure TfmDocsActs.acCopyExecute(Sender: TObject);
//var Par:Variant;
begin
  //����������
  with dmO do
  with dmORep do
  begin
{    if quDocsActs.RecordCount=0 then exit;

    taHeadDoc.Active:=False;
    taHeadDoc.FileName:=CurDir+'HeadDoc.cds';
    if FileExists(CurDir+'HeadDoc.cds') then taHeadDoc.Active:=True
    else taHeadDoc.CreateDataSet;

    taSpecDoc.Active:=False;
    taSpecDoc.FileName:=CurDir+'SpecDoc.cds';
    if FileExists(CurDir+'SpecDoc.cds') then taSpecDoc.Active:=True
    else taSpecDoc.CreateDataSet;

    par := VarArrayCreate([0,1], varInteger);
    par[0]:=1;
    par[1]:=quDocsActsID.AsInteger;
    if taHeadDoc.Locate('IType;Id',par,[])=False then
    begin
      taHeadDoc.Append;
      taHeadDocIType.AsInteger:=1;
      taHeadDocId.AsInteger:=quDocsActsID.AsInteger;
      taHeadDocDateDoc.AsInteger:=Trunc(quDocsActsDATEDOC.AsDateTime);
      taHeadDocNumDoc.AsString:=quDocsActsNUMDOC.AsString;
      taHeadDocIdCli.AsInteger:=quDocsActsIDCLI.AsInteger;
      taHeadDocNameCli.AsString:=Copy(quDocsActsNAMECL.AsString,1,70);
      taHeadDocIdSkl.AsInteger:=quDocsActsIDSKL.AsInteger;
      taHeadDocNameSkl.AsString:=Copy(quDocsActsNAMEMH.AsString,1,70);
      taHeadDocSumIN.AsFloat:=quDocsActsSUMIN.AsFloat;
      taHeadDocSumUch.AsFloat:=quDocsActsSUMUCH.AsFloat;
      taHeadDoc.Post;

      quSpecInSel.Active:=False;
      quSpecInSel.ParamByName('IDHD').AsInteger:=quDocsActsID.AsInteger;
      quSpecInSel.Active:=True;

      quSpecInSel.First;
      while not quSpecInSel.Eof do
      begin
        taSpecDoc.Append;
        taSpecDocIType.AsInteger:=1;
        taSpecDocIdHead.AsInteger:=quDocsActsID.AsInteger;
        taSpecDocNum.AsInteger:=quSpecInSelNUM.AsInteger;
        taSpecDocIdCard.AsInteger:=quSpecInSelIDCARD.AsInteger;
        taSpecDocQuant.AsFloat:=quSpecInSelQUANT.AsFloat;
        taSpecDocPriceIn.AsFloat:=quSpecInSelPRICEIN.AsFloat;
        taSpecDocSumIn.AsFloat:=quSpecInSelSUMIN.AsFloat;
        taSpecDocPriceUch.AsFloat:=quSpecInSelPRICEUCH.AsFloat;
        taSpecDocSumUch.AsFloat:=quSpecInSelSUMUCH.AsFloat;
        taSpecDocIdNds.AsInteger:=quSpecInSelIDNDS.AsInteger;
        taSpecDocSumNds.AsFloat:=quSpecInSelSUMNDS.AsFloat;
        taSpecDocNameC.AsString:=Copy(quSpecInSelNAMEC.AsString,1,30);
        taSpecDocSm.AsString:=quSpecInSelSM.AsString;
        taSpecDocIdM.AsInteger:=quSpecInSelIDM.AsInteger;
        taSpecDoc.Post;

        quSpecInSel.Next;
      end;
    end else
    begin
      showmessage('�������� ��� ���� � ������.');
    end;
    taHeadDoc.Active:=False;
    taSpecDoc.Active:=False;}
  end;
end;

procedure TfmDocsActs.acInsertDExecute(Sender: TObject);
{Var IId:Integer;
    DateB,DateE:TDateTime;
    iDate,iC,iCurDate:Integer;
    bAdd:Boolean;
    kBrutto:Real;}
begin
  // ��������
  with dmO do
  with dmORep do
  begin
{    taHeadDoc.Active:=False;
    taHeadDoc.FileName:=CurDir+'HeadDoc.cds';
    if FileExists(CurDir+'HeadDoc.cds') then taHeadDoc.Active:=True
    else taHeadDoc.CreateDataSet;

    taSpecDoc.Active:=False;
    taSpecDoc.FileName:=CurDir+'SpecDoc.cds';
    if FileExists(CurDir+'SpecDoc.cds') then taSpecDoc.Active:=True
    else taSpecDoc.CreateDataSet;

    fmTBuff:=TfmTBuff.Create(Application);

    fmTBuff.LevelTH.Visible:=False;
    fmTBuff.LevelTS.Visible:=False;
    fmTBuff.LevelD.Visible:=True;
    fmTBuff.LevelDSpec.Visible:=True;

    fmTBuff.ShowModal;
    if fmTBuff.ModalResult=mrOk then
    begin //���������
      fmTBuff.Release;
      if taHeadDoc.RecordCount>0 then
      begin
        if CanDo('prAddAct') then
        begin
          fmAddDoc1.Caption:='���������: ����� ��������.';
          fmAddDoc1.cxTextEdit1.Text:='';
          fmAddDoc1.cxTextEdit1.Tag:=0;
          fmAddDoc1.cxTextEdit1.Properties.ReadOnly:=False;
          fmAddDoc1.cxTextEdit2.Text:='';
          fmAddDoc1.cxTextEdit2.Properties.ReadOnly:=False;
          fmAddDoc1.cxDateEdit1.Date:=Date;
          fmAddDoc1.cxDateEdit1.Properties.ReadOnly:=False;
          fmAddDoc1.cxDateEdit2.Date:=Date;
          fmAddDoc1.cxDateEdit2.Properties.ReadOnly:=False;
          fmAddDoc1.cxCurrencyEdit1.EditValue:=0;
          fmAddDoc1.cxCurrencyEdit2.EditValue:=0;

          fmAddDoc1.cxButtonEdit1.Tag:=taHeadDocIdCli.AsInteger;
          fmAddDoc1.cxButtonEdit1.EditValue:=taHeadDocIdCli.AsInteger;
          fmAddDoc1.cxButtonEdit1.Text:=taHeadDocNameCli.AsString;
          fmAddDoc1.cxButtonEdit1.Properties.ReadOnly:=False;

          fmAddDoc1.cxLookupComboBox1.EditValue:=taHeadDocIdSkl.AsInteger;
          fmAddDoc1.cxLookupComboBox1.Text:=taHeadDocNameSkl.AsString;
          fmAddDoc1.cxLookupComboBox1.Properties.ReadOnly:=False;

          if quMHAll.Active=False then quMHAll.Active:=True;
          quMHAll.FullRefresh;

          if quMHAll.Locate('ID',CurVal.IdMH,[]) then
          begin
            fmAddDoc1.Label15.Caption:='��. ����: '+quMHAllNAMEPRICE.AsString;
            fmAddDoc1.Label15.Tag:=quMHAllDEFPRICE.AsInteger;
          end else
          begin
            fmAddDoc1.Label15.Caption:='��. ����: ';
            fmAddDoc1.Label15.Tag:=0;
          end;

          fmAddDoc1.cxLabel1.Enabled:=True;
          fmAddDoc1.cxLabel2.Enabled:=True;
          fmAddDoc1.cxLabel3.Enabled:=True;
          fmAddDoc1.cxLabel4.Enabled:=True;
          fmAddDoc1.cxLabel5.Enabled:=True;
          fmAddDoc1.cxLabel6.Enabled:=True;
          fmAddDoc1.N1.Enabled:=True;

          fmAddDoc1.ViewDoc1.OptionsData.Editing:=True;
          fmAddDoc1.ViewDoc1.OptionsData.Deleting:=True;

          fmAddDoc1.cxButton1.Enabled:=True;

          fmAddDoc1.taSpec.Active:=False;
          fmAddDoc1.taSpec.CreateDataSet;

          taSpecDoc.First;
          while not taSpecDoc.Eof do
          begin
            if (taSpecDocIType.AsInteger=taHeadDocIType.AsInteger)and(taSpecDocIdHead.AsInteger=taHeadDocId.AsInteger) then
            begin
              with fmAddDoc1 do
              begin
                taSpec.Append;
                taSpecNum.AsInteger:=taSpecDocNum.AsInteger;
                taSpecIdGoods.AsInteger:=taSpecDocIdCard.AsInteger;
                taSpecNameG.AsString:=taSpecDocNameC.AsString;
                taSpecIM.AsInteger:=taSpecDocIdM.AsInteger;
                taSpecSM.AsString:=taSpecDocSm.AsString;
                taSpecQuant.AsFloat:=taSpecDocQuant.AsFloat;
                taSpecPrice1.AsCurrency:=taSpecDocPriceIn.AsCurrency;
                taSpecSum1.AsCurrency:=taSpecDocSumIn.AsCurrency;
                taSpecPrice2.AsCurrency:=taSpecDocPriceUch.AsCurrency;
                taSpecSum2.AsCurrency:=taSpecDocSumUch.AsCurrency;
                taSpecINds.AsInteger:=taSpecDocIdNds.AsInteger;
                taSpecSNds.AsString:='���';
                taSpecRNds.AsCurrency:=taSpecDocSumNds.AsCurrency;
                taSpecSumNac.AsCurrency:=taSpecDocSumUch.AsCurrency-taSpecDocSumIn.AsCurrency;
                taSpecProcNac.AsCurrency:=0;
                if taSpecDocSumIn.AsCurrency<>0 then
                taSpecProcNac.AsCurrency:=RoundEx((taSpecDocSumUch.AsCurrency-taSpecDocSumIn.AsCurrency)/taSpecDocSumIn.AsCurrency*10000)/100;
                taSpec.Post;
              end;
            end;
            taSpecDoc.Next;
          end;

          taHeadDoc.Active:=False;
          taSpecDoc.Active:=False;

          fmAddDoc1.ShowModal;

        end else showmessage('��� ����.');
      end;
    end else
    begin
      fmTBuff.Release;
      taHeadDoc.Active:=False;
      taSpecDoc.Active:=False;
    end;}
  end;
end;

end.
