object dmC1: TdmC1
  OldCreateOrder = False
  Left = 126
  Top = 299
  Height = 274
  Width = 898
  object CasherRnDb1: TpFIBDatabase
    DBName = 'localhost:C:\_CasherRn\DB2\CASHERRN.GDB'
    DBParams.Strings = (
      'user_name=SYSDBA'
      'lc_ctype=WIN1251'
      'password=masterkey'
      'sql_role_name=SYSDBA')
    DefaultTransaction = trPrintSel
    DefaultUpdateTransaction = trPrintUpd
    SQLDialect = 3
    Timeout = 0
    SynchronizeTime = False
    DesignDBOptions = []
    AliasName = 'CasherRnDb1'
    WaitForRestoreConnect = 0
    Left = 40
    Top = 15
  end
  object quPrint: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE PRINTQUERY'
      'SET '
      '    STREAM = :STREAM,'
      '    PTYPE = :PTYPE,'
      '    FTYPE = :FTYPE,'
      '    STR = :STR,'
      '    PAGECODE = :PAGECODE,'
      '    CTYPE = :CTYPE,'
      '    BRING = :BRING'
      'WHERE'
      '    IDQUERY = :OLD_IDQUERY'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    PRINTQUERY'
      'WHERE'
      '        IDQUERY = :OLD_IDQUERY'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO PRINTQUERY('
      '    IDQUERY,'
      '    ID,'
      '    STREAM,'
      '    PTYPE,'
      '    FTYPE,'
      '    STR,'
      '    PAGECODE,'
      '    CTYPE,'
      '    BRING'
      ')'
      'VALUES('
      '    :IDQUERY,'
      '    :ID,'
      '    :STREAM,'
      '    :PTYPE,'
      '    :FTYPE,'
      '    :STR,'
      '    :PAGECODE,'
      '    :CTYPE,'
      '    :BRING'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    IDQUERY,'
      '    ID,'
      '    STREAM,'
      '    PTYPE,'
      '    FTYPE,'
      '    STR,'
      '    PAGECODE,'
      '    CTYPE,'
      '    BRING'
      'FROM'
      '    PRINTQUERY '
      ''
      ' WHERE '
      '        PRINTQUERY.IDQUERY = :OLD_IDQUERY'
      '    and PRINTQUERY.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    IDQUERY,'
      '    ID,'
      '    STREAM,'
      '    PTYPE,'
      '    FTYPE,'
      '    STR,'
      '    PAGECODE,'
      '    CTYPE,'
      '    BRING'
      'FROM'
      '    PRINTQUERY ')
    Transaction = trPrintSel
    Database = CasherRnDb1
    UpdateTransaction = trPrintUpd
    AutoCommit = True
    Left = 184
    Top = 16
    object quPrintIDQUERY: TFIBIntegerField
      FieldName = 'IDQUERY'
    end
    object quPrintID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quPrintSTREAM: TFIBIntegerField
      FieldName = 'STREAM'
    end
    object quPrintPTYPE: TFIBStringField
      FieldName = 'PTYPE'
      Size = 10
      EmptyStrToNull = True
    end
    object quPrintFTYPE: TFIBSmallIntField
      FieldName = 'FTYPE'
    end
    object quPrintSTR: TFIBStringField
      FieldName = 'STR'
      Size = 40
      EmptyStrToNull = True
    end
    object quPrintPAGECODE: TFIBSmallIntField
      FieldName = 'PAGECODE'
    end
    object quPrintCTYPE: TFIBSmallIntField
      FieldName = 'CTYPE'
    end
    object quPrintBRING: TFIBSmallIntField
      FieldName = 'BRING'
    end
  end
  object quPrintQu: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE PRINTQUERY'
      'SET '
      '    STREAM = :STREAM,'
      '    PTYPE = :PTYPE,'
      '    FTYPE = :FTYPE,'
      '    STR = :STR,'
      '    PAGECODE = :PAGECODE,'
      '    CTYPE = :CTYPE,'
      '    BRING = :BRING'
      'WHERE'
      '    IDQUERY = :OLD_IDQUERY'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    PRINTQUERY'
      'WHERE'
      '        IDQUERY = :OLD_IDQUERY'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO PRINTQUERY('
      '    IDQUERY,'
      '    ID,'
      '    STREAM,'
      '    PTYPE,'
      '    FTYPE,'
      '    STR,'
      '    PAGECODE,'
      '    CTYPE,'
      '    BRING'
      ')'
      'VALUES('
      '    :IDQUERY,'
      '    :ID,'
      '    :STREAM,'
      '    :PTYPE,'
      '    :FTYPE,'
      '    :STR,'
      '    :PAGECODE,'
      '    :CTYPE,'
      '    :BRING'
      ')')
    RefreshSQL.Strings = (
      'SELECT IDQUERY,ID,STREAM,PTYPE,FTYPE,STR,PAGECODE,CTYPE,BRING'
      'FROM PRINTQUERY'
      'where(  STREAM=:ISTREAM'
      '     ) and (     PRINTQUERY.IDQUERY = :OLD_IDQUERY'
      '    and PRINTQUERY.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT IDQUERY,ID,STREAM,PTYPE,FTYPE,STR,PAGECODE,CTYPE,BRING'
      'FROM PRINTQUERY'
      'where STREAM=:ISTREAM'
      'Order by IDQUERY,ID')
    Transaction = trQuPrint
    Database = CasherRnDb1
    UpdateTransaction = trPrintUpd
    AutoCommit = True
    Left = 248
    Top = 16
    poAskRecordCount = True
    object quPrintQuIDQUERY: TFIBIntegerField
      FieldName = 'IDQUERY'
    end
    object quPrintQuID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quPrintQuSTREAM: TFIBIntegerField
      FieldName = 'STREAM'
    end
    object quPrintQuPTYPE: TFIBStringField
      FieldName = 'PTYPE'
      Size = 10
      EmptyStrToNull = True
    end
    object quPrintQuFTYPE: TFIBSmallIntField
      FieldName = 'FTYPE'
    end
    object quPrintQuSTR: TFIBStringField
      FieldName = 'STR'
      Size = 60
      EmptyStrToNull = True
    end
    object quPrintQuPAGECODE: TFIBSmallIntField
      FieldName = 'PAGECODE'
    end
    object quPrintQuCTYPE: TFIBSmallIntField
      FieldName = 'CTYPE'
    end
    object quPrintQuBRING: TFIBSmallIntField
      FieldName = 'BRING'
    end
  end
  object quDelQu: TpFIBQuery
    Transaction = trDelQu
    Database = CasherRnDb1
    SQL.Strings = (
      'DELETE from PRINTQUERY '
      'where IDQUERY=:IDQ'
      '    ')
    Left = 304
    Top = 16
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object trQuPrint: TpFIBTransaction
    DefaultDatabase = CasherRnDb1
    TimeoutAction = TARollback
    Left = 248
    Top = 72
  end
  object trDelQu: TpFIBTransaction
    DefaultDatabase = CasherRnDb1
    TimeoutAction = TARollback
    Left = 304
    Top = 72
  end
  object trPrintSel: TpFIBTransaction
    DefaultDatabase = CasherRnDb1
    TimeoutAction = TARollback
    Left = 120
    Top = 16
  end
  object trPrintUpd: TpFIBTransaction
    DefaultDatabase = CasherRnDb1
    TimeoutAction = TARollback
    Left = 120
    Top = 72
  end
  object prGetId1: TpFIBStoredProc
    Transaction = trSelGetId
    Database = CasherRnDb1
    SQL.Strings = (
      'EXECUTE PROCEDURE PR_GETID (?ITYPE)')
    StoredProcName = 'PR_GETID'
    Left = 96
    Top = 136
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object trSelGetId: TpFIBTransaction
    DefaultDatabase = CasherRnDb1
    TimeoutAction = TARollback
    Left = 32
    Top = 136
  end
  object prSetRefresh: TpFIBStoredProc
    Transaction = trRefresh
    Database = dmC.CasherRnDb
    SQL.Strings = (
      'EXECUTE PROCEDURE PR_SETREFRESH (?STATION)')
    StoredProcName = 'PR_SETREFRESH'
    Left = 480
    Top = 24
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object trRefresh: TpFIBTransaction
    DefaultDatabase = dmC.CasherRnDb
    TimeoutAction = TARollback
    Left = 408
    Top = 24
  end
  object quRefresh: TpFIBDataSet
    Transaction = dmC.trSelect
    Database = dmC.CasherRnDb
    UpdateTransaction = dmC.trUpdate
    Left = 480
    Top = 80
    poAskRecordCount = True
  end
end
