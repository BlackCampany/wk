unit PCHistori;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, Placemnt, ComCtrls, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxControls, cxGridCustomView, cxGrid, SpeedBar, ExtCtrls, FR_Class,
  FR_DSet, FR_DBSet, ComObj, ActiveX, Excel2000, OleServer, ExcelXP,
  StdCtrls;

type
  TfmPCHist = class(TForm)
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    GridPCHist: TcxGrid;
    ViewPCHist: TcxGridDBTableView;
    LevelPCHist: TcxGridLevel;
    StatusBar1: TStatusBar;
    FormPlacement1: TFormPlacement;
    SpeedItem1: TSpeedItem;
    frRep1: TfrReport;
    frquPCDet: TfrDBDataSet;
    SpeedItem2: TSpeedItem;
    ViewPCHistBARCODE: TcxGridDBColumn;
    ViewPCHistDATEOP: TcxGridDBColumn;
    ViewPCHistTYPEOP: TcxGridDBColumn;
    ViewPCHistSTATION: TcxGridDBColumn;
    ViewPCHistIDPERSON: TcxGridDBColumn;
    ViewPCHistPERSNAME: TcxGridDBColumn;
    ViewPCHistBARCLI: TcxGridDBColumn;
    ViewPCHistRSUM: TcxGridDBColumn;
    ViewPCHistCLIBALANCE: TcxGridDBColumn;
    ViewPCHistCLINAME1: TcxGridDBColumn;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SpeedItem4Click(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function IsOLEObjectInstalled(Name: String): boolean;

var
  fmPCHist: TfmPCHist;

implementation

uses Un1, DmRnDisc,prdb, Period, MainDiscount, PCDetail;

{$R *.dfm}

function IsOLEObjectInstalled(Name: String): boolean;
var
  ClassID: TCLSID;
  Rez : HRESULT;
begin

// L��� CLSID OLE-������
  Rez := CLSIDFromProgID(PWideChar(WideString(Name)), ClassID);
  if Rez = S_OK then
 // +���� ������
    Result := true
  else
    Result := false;
end;


procedure TfmPCHist.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  GridPCHist.Align:=AlClient;
  ViewPCHist.RestoreFromIniFile(CurDir+GridIni);
end;

procedure TfmPCHist.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewPCHist.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmPCHist.SpeedItem4Click(Sender: TObject);
begin
  Close;
end;

procedure TfmPCHist.SpeedItem3Click(Sender: TObject);
begin
//
//
  fmPeriod:=TfmPeriod.Create(Application);
  fmPeriod.DateTimePicker1.Date:=TrebSel.DateFrom;
  fmPeriod.DateTimePicker2.Date:=TrebSel.DateTo;
  fmPeriod.DateTimePicker3.Visible:=True;
  fmPeriod.DateTimePicker3.Time:=Frac(TrebSel.DateFrom);
  fmPeriod.DateTimePicker4.Visible:=True;
  fmPeriod.DateTimePicker4.Time:=Frac(TrebSel.DateTo);

  fmPeriod.cxCheckBox1.Visible:=False;

  fmPeriod.ShowModal;
  if fmPeriod.ModalResult=mrOk then
  begin
    with dmPC do
    begin
      fmPeriod.Release;

      fmPCHist.ViewPCHist.BeginUpdate;
      try
        quPCHist.Active:=False;
        quPCHist.ParamByName('DateB').AsDateTime:=TrebSel.DateFrom;
        quPCHist.ParamByName('DateE').AsDateTime:=Trunc(TrebSel.DateTo+1);
        quPCHist.ParamByName('BARCODE').AsString:=fmPCHist.StatusBar1.Panels[1].Text;
        quPCHist.Active:=True;
      finally
        fmPCHist.ViewPCHist.EndUpdate;
      end;
      fmPCHist.Caption:='������� �� ��������� ����� '+StatusBar1.Panels[1].Text+' �� ������ � '+FormatDateTime('dd.mm.yyyy hh:nn',TrebSel.DateFrom)+' �� '+FormatDateTime('dd.mm.yyyy hh:nn',TrebSel.DateTo);
      exit;
    end;
  end;
  fmPeriod.Release;
end;

procedure TfmPCHist.SpeedItem1Click(Sender: TObject);
Var StrWk:String;
    i:Integer;
begin
  with dmPC do
  begin
    quPCHist.Filter:=ViewPCHist.DataController.Filter.FilterText;
    quPCHist.Filtered:=True;

    frRep1.LoadFromFile(CurDir + 'PCDet.frf');

    frVariables.Variable['sPeriod']:='c '+FormatDateTime('dd.mm.yyyy hh:nn',TrebSel.DateFrom)+' �� '+FormatDateTime('dd.mm.yyyy hh:nn',TrebSel.DateTo);

    StrWk:=ViewPCHist.DataController.Filter.FilterCaption;
    while Pos('AND',StrWk)>0 do
    begin
      i:=Pos('AND',StrWk);
      StrWk[i]:=' ';
      StrWk[i+1]:='�';
      StrWk[i+2]:=' ';
    end;
    while Pos('and',StrWk)>0 do
    begin
      i:=Pos('and',StrWk);
      StrWk[i]:=' ';
      StrWk[i+1]:='�';
      StrWk[i+2]:=' ';
    end;

    frVariables.Variable['sDop']:=StrWk;

    frRep1.ReportName:='��������� ����� (���������������� �����).';
    frRep1.PrepareReport;
    frRep1.ShowPreparedReport;

    quPCHist.Filter:='';
    quPCHist.Filtered:=False;
  end;

end;

procedure TfmPCHist.SpeedItem2Click(Sender: TObject);
begin
  with dmCDisc do prNExportExel4(ViewPCHist);
end;

end.
