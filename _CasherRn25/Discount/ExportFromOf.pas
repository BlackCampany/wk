unit ExportFromOf;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons, ExtCtrls,
  dxfProgressBar, cxControls, cxContainer, cxEdit, cxTextEdit, cxMaskEdit,
  cxDropDownEdit, cxCalendar, DB, FIBDataSet, pFIBDataSet;

type
  TfmExport = class(TForm)
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    cxDateEdit1: TcxDateEdit;
    cxDateEdit2: TcxDateEdit;
    ProgressBar1: TdxfProgressBar;
    quExport: TpFIBDataSet;
    quExportIDHEAD: TFIBIntegerField;
    quExportIDNDS: TFIBIntegerField;
    quExportDATEDOC: TFIBDateField;
    quExportNUMDOC: TFIBStringField;
    quExportDATESF: TFIBDateField;
    quExportNUMSF: TFIBStringField;
    quExportIDCLI: TFIBIntegerField;
    quExportIDSKL: TFIBIntegerField;
    quExportNAMECL: TFIBStringField;
    quExportINN: TFIBStringField;
    quExportSUMIN: TFIBFloatField;
    quExportSUMUCH: TFIBFloatField;
    quExportSUMNDS: TFIBFloatField;
    Label4: TLabel;
    quCountD: TpFIBDataSet;
    quCountDCOUNTDOC: TFIBIntegerField;
    quExportEXPNAME: TFIBStringField;
    ProgressBar2: TdxfProgressBar;
    quExpOut: TpFIBDataSet;
    quCountDOut: TpFIBDataSet;
    quCountDOutCOUNTDOC: TFIBIntegerField;
    quExpOutIDHEAD: TFIBIntegerField;
    quExpOutIDNDS: TFIBIntegerField;
    quExpOutDATEDOC: TFIBDateField;
    quExpOutNUMDOC: TFIBStringField;
    quExpOutDATESF: TFIBDateField;
    quExpOutNUMSF: TFIBStringField;
    quExpOutIDCLI: TFIBIntegerField;
    quExpOutIDSKL: TFIBIntegerField;
    quExpOutEXPNAME: TFIBStringField;
    quExpOutNAMECL: TFIBStringField;
    quExpOutINN: TFIBStringField;
    quExpOutSUMIN: TFIBFloatField;
    quExpOutSUMUCH: TFIBFloatField;
    quExpOutSUMNDS: TFIBFloatField;
    quListR: TpFIBDataSet;
    quListRDATEDOC: TFIBDateField;
    quListRIDSKL: TFIBIntegerField;
    quAvIn: TpFIBDataSet;
    quAvOut: TpFIBDataSet;
    quAvInRSUMA: TFIBFloatField;
    quAvOutRSUMA: TFIBFloatField;
    quListREXPNAME: TFIBStringField;
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmExport: TfmExport;

implementation

uses Un1, u2fdk, dmOffice;

{$R *.dfm}

procedure TfmExport.cxButton1Click(Sender: TObject);
begin
  close;
end;

procedure TfmExport.cxButton2Click(Sender: TObject);
Var StrWk,FileN:String;
    F: TextFile;
		SUM_POST,SUM_10,NDS_10,SUM_18,NDS_18,NSP,SUM_SKLAD,SUM_TARA:Real;
    SUM_REALIN,SUM_DELIN,SUM_REAL,SUM_REALBN,SUM_AVOUT,SUM_AVIN,SUM_USLOUT:Real;
    Idh,i,iMax:INteger;
    bW:Boolean;

{		SUM_POST    = ����� ����������;
		SUM_10      = ����� ���������� ��� ��� 10;
		NDS_10      = ��� 10 ����������;
		SUM_18      = ����� ���������� ��� ��� 18;
		NDS_18      = ��� 18 ����������;
		NSP         = 0;
		SUM_SKLAD   = ����� �������;
		SUM_TARA    = ����� ����;

    SUM_REALIN  = ������������� ����������
    SUM_DELIN   = ������������� ���������
    SUM_REAL    = ����� ����������
    SUM_REALBN  = ����� ���������� ������
    SUM_AVOUT   = ����� ������ ���������
    SUM_AVIN   = ����� ������ ����������
    SUM_USLOUT   = ����� ���������� �����

}



begin
  //������� �������
  try
    Application.ProcessMessages;
    FileN:=CommonSet.PathExport+'expout.txt';
    AssignFile(F, FileN);
 //     if Not FileExists(FileN) then Rewrite(F)
//      else                           Append(F);
    Rewrite(F);

    //������
    Label4.Caption:='�����, ���� ��������� ������ (������)..'; Delay(10);
    quCountD.Active:=False;
    quCountD.ParamByName('DATEB').AsDateTime:=Trunc(cxDateEdit1.Date);
    quCountD.ParamByName('DATEE').AsDateTime:=Trunc(cxDateEdit2.Date)+1;
    quCountD.Active:=True;
    iMax:=quCountDCOUNTDOC.AsInteger;
    quCountD.Active:=False;

    ProgressBar2.Position:=0;
    ProgressBar2.Visible:=True;

    if iMax>0 then
    begin

      quExport.Active:=False;
      quExport.ParamByName('DATEB').AsDateTime:=Trunc(cxDateEdit1.Date);
      quExport.ParamByName('DATEE').AsDateTime:=Trunc(cxDateEdit2.Date)+1;
      quExport.Active:=True;

      Label4.Caption:='�����, ���� ������������ ������ (������)..'; Delay(10);
      ProgressBar1.Position:=0;
      ProgressBar1.Visible:=True;


      Idh:=0; i:=0;
      SUM_POST:=0;SUM_10:=0;NDS_10:=0;SUM_18:=0;NDS_18:=0;NSP:=0;SUM_SKLAD:=0;SUM_TARA:=0;
      bW:=False;

      quExport.First;
      while not quExport.Eof do
      begin
        if quExportIDHEAD.AsInteger<>Idh then
        begin //��������� ���������
          if Idh<>0 then
          begin //����� ����������
            StrWk:=StrWk+FlToS(SUM_POST)+';'+FlToS(SUM_10)+';'+FlToS(NDS_10)+';'+FlToS(SUM_18)+';'+FlToS(NDS_18)+';'+FlToS(NSP)+';'+FlToS(SUM_SKLAD)+';'+FlToS(SUM_TARA)+';';
            WriteLn(F,AnsiToOemConvert(StrWk));
          end;
          SUM_POST:=0;SUM_10:=0;NDS_10:=0;SUM_18:=0;NDS_18:=0;NSP:=0;SUM_SKLAD:=0;SUM_TARA:=0;
          StrWk:='���;'+quExportEXPNAME.AsString+';'+FormatDateTime('dd.mm.yyyy',quExportDATEDOC.AsDateTime)+';'+quExportNUMDOC.AsString+';'+quExportNAMECL.AsString+';'+quExportINN.AsString+';'+FormatDateTime('dd.mm.yyyy',quExportDATESF.AsDateTime)+';'+quExportNUMSF.AsString+';';
          IdH:=quExportIDHEAD.AsInteger;
          bW:=True;
          inc(i);
          ProgressBar1.Position:=i*100 div iMax; Delay(10);
        end;
        SUM_POST:=SUM_POST+quExportSUMIN.AsFloat;
        SUM_SKLAD:=SUM_SKLAD+quExportSUMUCH.AsFloat;
        if  quExportIDNDS.AsInteger=2 then //��� 10
        begin
          SUM_10:=SUM_10+quExportSUMIN.AsFloat-quExportSUMNDS.AsFloat;
          NDS_10:=NDS_10+quExportSUMNDS.AsFloat;
        end;
        if  quExportIDNDS.AsInteger=3 then //��� 18
        begin
          SUM_18:=SUM_18+quExportSUMIN.AsFloat-quExportSUMNDS.AsFloat;
          NDS_18:=NDS_18+quExportSUMNDS.AsFloat;
        end;
        quExport.Next;
      end;
      if bW then
      begin //����� ����������
        StrWk:=StrWk+FlToS(SUM_POST)+';'+FlToS(SUM_10)+';'+FlToS(NDS_10)+';'+FlToS(SUM_18)+';'+FlToS(NDS_18)+';'+FlToS(NSP)+';'+FlToS(SUM_SKLAD)+';'+FlToS(SUM_TARA)+';';
        WriteLn(F,AnsiToOemConvert(StrWk));
      end;
      ProgressBar1.Position:=100; delay(100);
      Flush(F);
    end;
    ProgressBar2.Position:=70; delay(50);

    //������ - �������
    Label4.Caption:='�����, ���� ��������� ������ (�������)..'; Delay(10);
    quCountDOut.Active:=False;
    quCountDOut.ParamByName('DATEB').AsDateTime:=Trunc(cxDateEdit1.Date);
    quCountDOut.ParamByName('DATEE').AsDateTime:=Trunc(cxDateEdit2.Date)+1;
    quCountDOut.Active:=True;
    iMax:=quCountDOutCOUNTDOC.AsInteger;
    quCountDOut.Active:=False;

    ProgressBar2.Position:=0;
    ProgressBar2.Visible:=True;

    if iMax>0 then
    begin

      quExpOut.Active:=False;
      quExpOut.ParamByName('DATEB').AsDateTime:=Trunc(cxDateEdit1.Date);
      quExpOut.ParamByName('DATEE').AsDateTime:=Trunc(cxDateEdit2.Date)+1;
      quExpOut.Active:=True;

      Label4.Caption:='�����, ���� ������������ ������ (�������)..'; Delay(10);
      ProgressBar1.Position:=0;
      ProgressBar1.Visible:=True;


      Idh:=0; i:=0;
      SUM_POST:=0;SUM_10:=0;NDS_10:=0;SUM_18:=0;NDS_18:=0;NSP:=0;SUM_SKLAD:=0;SUM_TARA:=0;
      bW:=False;

      quExpOut.First;
      while not quExpOut.Eof do
      begin
        if quExpOutIDHEAD.AsInteger<>Idh then
        begin //��������� ���������
          if Idh<>0 then
          begin //����� ����������
            StrWk:=StrWk+FlToS(SUM_POST)+';'+FlToS(SUM_10)+';'+FlToS(NDS_10)+';'+FlToS(SUM_18)+';'+FlToS(NDS_18)+';'+FlToS(NSP)+';'+FlToS(SUM_SKLAD)+';'+FlToS(SUM_TARA)+';';
            WriteLn(F,AnsiToOemConvert(StrWk));
          end;
          SUM_POST:=0;SUM_10:=0;NDS_10:=0;SUM_18:=0;NDS_18:=0;NSP:=0;SUM_SKLAD:=0;SUM_TARA:=0;
          StrWk:='���;'+quExpOutEXPNAME.AsString+';'+FormatDateTime('dd.mm.yyyy',quExpOutDATEDOC.AsDateTime)+';'+quExpOutNUMDOC.AsString+';'+quExpOutNAMECL.AsString+';'+quExpOutINN.AsString+';'+FormatDateTime('dd.mm.yyyy',quExpOutDATESF.AsDateTime)+';'+quExpOutNUMSF.AsString+';';
          IdH:=quExpOutIDHEAD.AsInteger;
          bW:=True;
          inc(i);
          ProgressBar1.Position:=i*100 div iMax; Delay(10);
        end;
        SUM_POST:=SUM_POST+quExpOutSUMIN.AsFloat;
        SUM_SKLAD:=SUM_SKLAD+quExpOutSUMUCH.AsFloat;
        if  quExpOutIDNDS.AsInteger=2 then //��� 10
        begin
          SUM_10:=SUM_10+quExpOutSUMIN.AsFloat-quExpOutSUMNDS.AsFloat;
          NDS_10:=NDS_10+quExpOutSUMNDS.AsFloat;
        end;
        if  quExpOutIDNDS.AsInteger=3 then //��� 18
        begin
          SUM_18:=SUM_18+quExpOutSUMIN.AsFloat-quExpOutSUMNDS.AsFloat;
          NDS_18:=NDS_18+quExpOutSUMNDS.AsFloat;
        end;
        quExpOut.Next;
      end;
      if bW then
      begin //����� ����������
        StrWk:=StrWk+FlToS(SUM_POST)+';'+FlToS(SUM_10)+';'+FlToS(NDS_10)+';'+FlToS(SUM_18)+';'+FlToS(NDS_18)+';'+FlToS(NSP)+';'+FlToS(SUM_SKLAD)+';'+FlToS(SUM_TARA)+';';
        WriteLn(F,AnsiToOemConvert(StrWk));
      end;
      ProgressBar1.Position:=100; delay(100);
      Flush(F);
    end;
    ProgressBar2.Position:=80; delay(50);

    //����������
{
    SUM_REALIN  = ������������� ����������
    SUM_DELIN   = ������������� ���������
    SUM_REAL    = ����� ����������
    SUM_REALBN  = ����� ���������� ������
    SUM_AVOUT   = ����� ������ ���������
    SUM_AVIN   = ����� ������ ����������
    SUM_USLOUT   = ����� ���������� �����

}
    with dmO do
    begin
      quListR.Active:=False;
      quListR.ParamByName('DATEB').AsDateTime:=Trunc(cxDateEdit1.Date);
      quListR.ParamByName('DATEE').AsDateTime:=Trunc(cxDateEdit2.Date)+1;
      quListR.Active:=True;
      iMax:=quListR.RecordCount;
      i:=0;
      Label4.Caption:='�����, ���� ������������ ������ (����������)..'; Delay(10);
      ProgressBar1.Position:=0;
      ProgressBar1.Visible:=True;


      quListR.First;
      while not quListR.Eof do
      begin
        //������� �������� �� ��� �� ������
        SUM_REALIN:=0;
        SUM_DELIN:=0;
        SUM_REAL:=0;
        SUM_REALBN:=0;
        SUM_AVOUT:=0;
        SUM_AVIN:=0;
        SUM_USLOUT:=0;

        //������ �� �����
        taTOOutB.Active:=False;
        taTOOutB.ParamByName('DATEB').AsDate:=Trunc(quListRDATEDOC.AsDateTime);
        taTOOutB.ParamByName('IDSKL').AsInteger:=quListRIDSKL.AsInteger;
        taTOOutB.Active:=True;

        taTOOutB.First;
        while not taTOOutB.Eof do
        begin
          if taTOOutBOPER.AsString='Sale' then
          begin
            SUM_REALIN:=SUM_REALIN+taTOOutBSUMIN.AsFloat;
            SUM_REAL:=SUM_REAL+taTOOutBSUMUCH.AsFloat;
          end;
          if taTOOutBOPER.AsString='SaleBank' then
          begin
            SUM_REALIN:=SUM_REALIN+taTOOutBSUMIN.AsFloat;
            SUM_REALBN:=SUM_REALBN+taTOOutBSUMUCH.AsFloat;
          end;
          if taTOOutBOPER.AsString='Ret' then
          begin
            SUM_REALIN:=SUM_REALIN-abs(taTOOutBSUMIN.AsFloat);
            SUM_REAL:=SUM_REAL-abs(taTOOutBSUMUCH.AsFloat); 
          end;
          if taTOOutBOPER.AsString='RetBank' then
          begin
            SUM_REALIN:=SUM_REALIN-abs(taTOOutBSUMIN.AsFloat);
            SUM_REALBN:=SUM_REALBN-abs(taTOOutBSUMUCH.AsFloat);
          end;
          if taTOOutBOPER.AsString='SalePC' then
          begin
            SUM_REALIN:=SUM_REALIN+taTOOutBSUMIN.AsFloat;
          end;
          if taTOOutBOPER.AsString='Del' then
          begin
            SUM_DELIN:=SUM_DELIN+taTOOutBSUMIN.AsFloat;
          end;

          taTOOutB.Next;
        end;
        taTOOutB.Active:=False;

        //������ �� ����� - ������
        taTOOutBCat.Active:=False;
        taTOOutBCat.ParamByName('DATEB').AsDate:=Trunc(quListRDATEDOC.AsDateTime);
        taTOOutBCat.ParamByName('ICAT').AsInteger:=2;
        taTOOutBCat.ParamByName('IDSKL').AsInteger:=quListRIDSKL.AsInteger;
        taTOOutBCat.Active:=True;

        if (taTOOutBCatRSUM.AsFloat<>0) then
        begin
          SUM_USLOUT:=SUM_USLOUT+taTOOutBCatRSUM.AsFloat;
//          SUM_REAL:=SUM_REAL-SUM_USLOUT;
        end;
        taTOOutBCat.Active:=False;


        //������ �� ����� - ������ ����������
        quAvIn.Active:=False;
        quAvIn.ParamByName('DATEB').AsDate:=Trunc(quListRDATEDOC.AsDateTime);
        quAvIn.ParamByName('IDSKL').AsInteger:=quListRIDSKL.AsInteger;
        quAvIn.Active:=True;

        if quAvInRSUMA.AsFloat<>0 then
        begin
          SUM_AVIN:=SUM_AVIN+quAvInRSUMA.AsFloat;
//          SUM_REAL:=SUM_REAL-SUM_AVIN;
        end;
        quAvIn.Active:=True;

        //������ �� ����� - ������ �������
        quAvOut.Active:=False;
        quAvOut.ParamByName('DATEB').AsDate:=Trunc(quListRDATEDOC.AsDateTime);
        quAvOut.ParamByName('IDSKL').AsInteger:=quListRIDSKL.AsInteger;
        quAvOut.Active:=True;

        if quAvOutRSUMA.AsFloat<>0 then
        begin
          SUM_AVOut:=SUM_AVOut+(-1)*quAvOutRSUMA.AsFloat;
//          SUM_REAL:=SUM_REAL+SUM_AVOUT;
        end;
        quAvOut.Active:=True;

        SUM_REALIN:=RoundEx(SUM_REALIN*100)/100;
        SUM_DELIN:=RoundEx(SUM_DELIN*100)/100;
        SUM_REAL:=RoundEx(SUM_REAL*100)/100;
        SUM_REALBN:=RoundEx(SUM_REALBN*100)/100;
        SUM_AVOUT:=RoundEx(SUM_AVOUT*100)/100;
        SUM_AVIN:=RoundEx(SUM_AVIN*100)/100;
        SUM_USLOUT:=RoundEx(SUM_USLOUT*100)/100;


        StrWk:='���;'+quListREXPNAME.AsString+';'+FormatDateTime('dd.mm.yyyy',quListRDATEDOC.AsDateTime)+';;;;;;'+FlToS(SUM_REALIN)+';'+FlToS(SUM_DELIN)+';'+FlToS(SUM_REAL)+';'+FlToS(SUM_REALBN)+';'+FlToS(SUM_AVIN)+';'+FlToS(SUM_AVOUT)+';'+FlToS(SUM_USLOUT)+';';
        WriteLn(F,AnsiToOemConvert(StrWk));
        Flush(F);

        quListR.Next;
        inc(i);
        ProgressBar1.Position:=i*100 div iMax; Delay(10);
      end;
      quListR.Active:=False;
    end;

  finally
    ProgressBar1.Position:=100; Delay(10);
    ProgressBar2.Position:=100;
    Label4.Caption:='������������ ������ ���������.'; Delay(10);
    quExport.Active:=False;
    quExpOut.Active:=False;
    CloseFile(F);
  end;
end;

procedure TfmExport.FormCreate(Sender: TObject);
begin
  Label4.Caption:='';
end;

end.
