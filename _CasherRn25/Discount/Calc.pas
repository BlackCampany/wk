unit Calc;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxControls, cxContainer, cxEdit, cxTextEdit, cxMaskEdit,
  cxDropDownEdit, cxCalc, ExtCtrls, cxLookAndFeelPainters, StdCtrls,
  cxButtons, ActnList, XPStyleActnCtrls, ActnMan, Menus;

type
  TfmCalc = class(TForm)
    Panel1: TPanel;
    CalcEdit1: TcxCalcEdit;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    cxButton5: TcxButton;
    cxButton6: TcxButton;
    cxButton7: TcxButton;
    cxButton8: TcxButton;
    cxButton9: TcxButton;
    cxButton10: TcxButton;
    cxButton11: TcxButton;
    cxButton12: TcxButton;
    cxButton13: TcxButton;
    cxButton14: TcxButton;
    cxButton15: TcxButton;
    amCalc: TActionManager;
    Action1: TAction;
    Action2: TAction;
    Action3: TAction;
    Action4: TAction;
    Action5: TAction;
    Action6: TAction;
    Action7: TAction;
    Action8: TAction;
    Action9: TAction;
    Action10: TAction;
    Action11: TAction;
    Action12: TAction;
    Action13: TAction;
    Action14: TAction;
    Action15: TAction;
    acExit: TAction;
    cxButton16: TcxButton;
    acAddToSpec: TAction;
    cxButton17: TcxButton;
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
    procedure cxButton7Click(Sender: TObject);
    procedure cxButton8Click(Sender: TObject);
    procedure cxButton9Click(Sender: TObject);
    procedure cxButton10Click(Sender: TObject);
    procedure cxButton11Click(Sender: TObject);
    procedure Action1Execute(Sender: TObject);
    procedure Action2Execute(Sender: TObject);
    procedure Action3Execute(Sender: TObject);
    procedure Action4Execute(Sender: TObject);
    procedure Action5Execute(Sender: TObject);
    procedure Action6Execute(Sender: TObject);
    procedure Action7Execute(Sender: TObject);
    procedure Action8Execute(Sender: TObject);
    procedure Action9Execute(Sender: TObject);
    procedure Action10Execute(Sender: TObject);
    procedure Action11Execute(Sender: TObject);
    procedure Action12Execute(Sender: TObject);
    procedure Action13Execute(Sender: TObject);
    procedure Action14Execute(Sender: TObject);
    procedure Action15Execute(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure acAddToSpecExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmCalc: TfmCalc;
  bFirst:Boolean;

implementation

uses Dm, Un1;

{$R *.dfm}

procedure TfmCalc.cxButton1Click(Sender: TObject);
begin
  Action1.Execute;
end;

procedure TfmCalc.cxButton2Click(Sender: TObject);
begin
  Action2.Execute;
end;

procedure TfmCalc.cxButton3Click(Sender: TObject);
begin
  Action3.Execute;
end;

procedure TfmCalc.cxButton4Click(Sender: TObject);
begin
  Action4.Execute;
end;

procedure TfmCalc.cxButton5Click(Sender: TObject);
begin
  Action5.Execute;
end;

procedure TfmCalc.cxButton6Click(Sender: TObject);
begin
  Action6.Execute;
end;

procedure TfmCalc.cxButton7Click(Sender: TObject);
begin
  Action7.Execute;
end;

procedure TfmCalc.cxButton8Click(Sender: TObject);
begin
  Action8.Execute;
end;

procedure TfmCalc.cxButton9Click(Sender: TObject);
begin
  Action9.Execute;
end;

procedure TfmCalc.cxButton10Click(Sender: TObject);
begin
  Action10.Execute;
end;

procedure TfmCalc.cxButton11Click(Sender: TObject);
begin
  Action11.Execute;
end;

procedure TfmCalc.Action1Execute(Sender: TObject);
begin
  if pos(',',CalcEdit1.Text)=0 then CalcEdit1.EditValue:=CalcEdit1.EditValue*10+1
  else CalcEdit1.Text:=CalcEdit1.Text+'1';
  if bFirst then
  begin
    CalcEdit1.EditValue:=1;
    bFirst:=False;
  end;
end;

procedure TfmCalc.Action2Execute(Sender: TObject);
begin
  if pos(',',CalcEdit1.Text)=0 then CalcEdit1.EditValue:=CalcEdit1.EditValue*10+2
  else CalcEdit1.Text:=CalcEdit1.Text+'2';
  if bFirst then
  begin
    CalcEdit1.EditValue:=2;
    bFirst:=False;
  end;
end;

procedure TfmCalc.Action3Execute(Sender: TObject);
begin
  if pos(',',CalcEdit1.Text)=0 then CalcEdit1.EditValue:=CalcEdit1.EditValue*10+3
  else CalcEdit1.Text:=CalcEdit1.Text+'3';
  if bFirst then
  begin
    CalcEdit1.EditValue:=3;
    bFirst:=False;
  end;
end;

procedure TfmCalc.Action4Execute(Sender: TObject);
begin
  if pos(',',CalcEdit1.Text)=0 then CalcEdit1.EditValue:=CalcEdit1.EditValue*10+4
  else CalcEdit1.Text:=CalcEdit1.Text+'4';
  if bFirst then
  begin
    CalcEdit1.EditValue:=4;
    bFirst:=False;
  end;
end;

procedure TfmCalc.Action5Execute(Sender: TObject);
begin
  if pos(',',CalcEdit1.Text)=0 then CalcEdit1.EditValue:=CalcEdit1.EditValue*10+5
  else CalcEdit1.Text:=CalcEdit1.Text+'5';
  if bFirst then
  begin
    CalcEdit1.EditValue:=5;
    bFirst:=False;
  end;
end;

procedure TfmCalc.Action6Execute(Sender: TObject);
begin
  if pos(',',CalcEdit1.Text)=0 then CalcEdit1.EditValue:=CalcEdit1.EditValue*10+6
  else CalcEdit1.Text:=CalcEdit1.Text+'6';
  if bFirst then
  begin
    CalcEdit1.EditValue:=6;
    bFirst:=False;
  end;
end;

procedure TfmCalc.Action7Execute(Sender: TObject);
begin
  if pos(',',CalcEdit1.Text)=0 then CalcEdit1.EditValue:=CalcEdit1.EditValue*10+7
  else CalcEdit1.Text:=CalcEdit1.Text+'7';
  if bFirst then
  begin
    CalcEdit1.EditValue:=7;
    bFirst:=False;
  end;
end;

procedure TfmCalc.Action8Execute(Sender: TObject);
begin
  if pos(',',CalcEdit1.Text)=0 then CalcEdit1.EditValue:=CalcEdit1.EditValue*10+8
  else CalcEdit1.Text:=CalcEdit1.Text+'8';
  if bFirst then
  begin
    CalcEdit1.EditValue:=8;
    bFirst:=False;
  end;
end;

procedure TfmCalc.Action9Execute(Sender: TObject);
begin
  if pos(',',CalcEdit1.Text)=0 then CalcEdit1.EditValue:=CalcEdit1.EditValue*10+9
  else CalcEdit1.Text:=CalcEdit1.Text+'9';
  if bFirst then
  begin
    CalcEdit1.EditValue:=9;
    bFirst:=False;
  end;
end;

procedure TfmCalc.Action10Execute(Sender: TObject);
begin
  if pos(',',CalcEdit1.Text)=0 then CalcEdit1.EditValue:=CalcEdit1.EditValue*10
  else CalcEdit1.Text:=CalcEdit1.Text+'0';
  if bFirst then
  begin
    CalcEdit1.EditValue:=0;
    bFirst:=False;
  end;
end;

procedure TfmCalc.Action11Execute(Sender: TObject);
begin
  if pos(',',CalcEdit1.Text)=0 then CalcEdit1.Text:=CalcEdit1.Text+',';
  if bFirst then
  begin
    bFirst:=False;
  end;
end;

procedure TfmCalc.Action12Execute(Sender: TObject);
begin
  CalcEdit1.Value:=0;
  if bFirst then
  begin
    CalcEdit1.EditValue:=0;
    bFirst:=False;
  end;
end;

procedure TfmCalc.Action13Execute(Sender: TObject);
begin
  CalcEdit1.EditValue:=CalcEdit1.EditValue+1;
  if bFirst then
  begin
    bFirst:=False;
  end;
end;

procedure TfmCalc.Action14Execute(Sender: TObject);
Var rQ:Real;
begin
  rQ:=CalcEdit1.EditValue-1;
  if rQ >=0 then  CalcEdit1.EditValue:=rQ;
  if bFirst then
  begin
    bFirst:=False;
  end;
end;

procedure TfmCalc.Action15Execute(Sender: TObject);
begin
  CalcEdit1.EditValue:=0;
  if bFirst then
  begin
    bFirst:=False;
  end;
end;

procedure TfmCalc.acExitExecute(Sender: TObject);
begin
  ModalResult:=mrCancel;
end;

procedure TfmCalc.FormShow(Sender: TObject);
begin
  bFirst:=True;
  if bAddPosFromMenu or bAddPosFromSpec then cxButton16.Visible:=True
  else cxButton16.Visible:=False;
end;

procedure TfmCalc.acAddToSpecExecute(Sender: TObject);
Var rQ:Real;
begin
//��������� � ������������ ����� ���� ������� ���-��
  with dmC do
  begin
    rQ:=RoundEx(CalcEdit1.EditValue*1000)/1000;

    if bAddPosFromMenu then
    begin
      CalcDiscontN(quMenuSIFR.AsInteger,Check.Max+1,quMenuPARENT.AsInteger,quMenuSTREAM.AsInteger,quMenuPRICE.AsFloat,rQ,0,Tab.DBar,Now,Check.DProc,Check.DSum);

      quCurSpec.Append;
      quCurSpecSTATION.AsInteger:=CommonSet.Station;
      quCurSpecID_TAB.AsInteger:=Tab.Id;
      quCurSpecId_Personal.AsInteger:=Person.Id;
      quCurSpecNumTable.AsString:=Tab.NumTable;
      quCurSpecId.AsInteger:=Check.Max+1;
      quCurSpecSifr.AsInteger:=quMenuSIFR.AsInteger;
      quCurSpecPrice.AsFloat:=quMenuPRICE.AsFloat;
      quCurSpecQuantity.AsFloat:=rQ;
      quCurSpecSumma.AsFloat:=roundEx(quMenuPRICE.AsFloat*rQ*100)/100-Check.DSum;
      quCurSpecDProc.AsFloat:=Check.DProc;
      quCurSpecDSum.AsFloat:=Check.DSum;
      quCurSpecIStatus.AsInteger:=0;
      quCurSpecName.AsString:=quMenuNAME.AsString;
      quCurSpecCode.AsString:=quMenuCODE.AsString;
      quCurSpecLinkM.AsInteger:=quMenuLINK.AsInteger;
      if quMenuLIMITPRICE.AsFloat>0 then
        quCurSpecLimitM.AsInteger:=RoundEx(quMenuLIMITPRICE.AsFloat)
      else quCurSpecLimitM.AsInteger:=99;
      quCurSpecStream.AsInteger:=quMenuSTREAM.AsInteger;
      quCurSpec.Post;
      inc(Check.Max);

{     //�� ������������ ��� �� ����� ���� � ���� ������ ����
      if quMenuLINK.AsInteger>0 then
      begin //������������
        acMod1.Execute;
      end;}
    end;
    if bAddPosFromSpec then
    begin
      Check.Id_personal:=quCurSpecId_Personal.AsInteger;
      Check.NumTable:=quCurSpecNumTable.AsString;
      Check.Sifr:=quCurSpecSifr.AsInteger;
      Check.Price:=quCurSpecPrice.AsFloat;
      Check.Summa:=quCurSpecSumma.AsFloat;
      Check.DProc:=quCurSpecDProc.AsFloat;
      Check.DSum:=quCurSpecDSum.AsFloat;
      Check.Name:=quCurSpecName.AsString;
      Check.Code:=quCurSpecCode.AsString;
      Check.LimitM:=quCurSpecLimitM.AsInteger;
      Check.LinkM:=quCurSpecLinkM.AsInteger;
      Check.Stream:=quCurSpecStream.AsInteger;

      CalcDiscontN(Check.Sifr,Check.Max+1,-1,Check.Stream,Check.Price,rQ,0,Tab.DBar,Now,Check.DProc,Check.DSum);

      quCurSpec.Append;
      quCurSpecSTATION.AsInteger:=CommonSet.Station;
      quCurSpecID_TAB.AsInteger:=Tab.Id;
      quCurSpecId_Personal.AsInteger:=Check.Id_personal;
      quCurSpecNumTable.AsString:=Check.NumTable;
      quCurSpecId.AsInteger:=Check.Max+1;
      quCurSpecSifr.AsInteger:=Check.Sifr;
      quCurSpecPrice.AsFloat:=Check.Price;
      quCurSpecQuantity.AsFloat:=rQ;
      quCurSpecSumma.AsFloat:=roundEx(rQ*Check.Price*100)/100-Check.DSum;
      quCurSpecDProc.AsFloat:=Check.DProc;
      quCurSpecDSum.AsFloat:=Check.DSum;
      quCurSpecIStatus.AsInteger:=0;
      quCurSpecName.AsString:=Check.Name;
      quCurSpecCode.AsString:=Check.Code;
      quCurSpecLinkM.AsInteger:=Check.LinkM;
      quCurSpecLimitM.AsInteger:=Check.LimitM;
      quCurSpecStream.AsInteger:=Check.Stream;
      quCurSpec.Post;
      inc(Check.Max);
    end;
  end;
end;

end.
