unit Tabs;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  ComCtrls, ExtCtrls, SpeedBar, Placemnt, ActnList, XPStyleActnCtrls,
  ActnMan, cxDataStorage, cxCurrencyEdit, cxImageComboBox,
  ComObj, ActiveX, Excel2000, OleServer, ExcelXP, cxCheckBox, FR_DSet,
  FR_DBSet, FR_Class, StdCtrls;

type
  TfmTabs = class(TForm)
    StatusBar1: TStatusBar;
    ViewTabs: TcxGridDBTableView;
    LevelTabs: TcxGridLevel;
    GridTabs: TcxGrid;
    SpeedBar1: TSpeedBar;
    FormPlacement1: TFormPlacement;
    amTabs: TActionManager;
    acPeriod: TAction;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    acSpecSel: TAction;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    LevelSpec: TcxGridLevel;
    ViewSpecReps: TcxGridDBTableView;
    ViewSpecRepsID_TAB: TcxGridDBColumn;
    ViewSpecRepsID: TcxGridDBColumn;
    ViewSpecRepsID_PERSONAL: TcxGridDBColumn;
    ViewSpecRepsNUMTABLE: TcxGridDBColumn;
    ViewSpecRepsSIFR: TcxGridDBColumn;
    ViewSpecRepsPRICE: TcxGridDBColumn;
    ViewSpecRepsQUANTITY: TcxGridDBColumn;
    ViewSpecRepsDISCOUNTPROC: TcxGridDBColumn;
    ViewSpecRepsDISCOUNTSUM: TcxGridDBColumn;
    ViewSpecRepsSUMMA: TcxGridDBColumn;
    ViewSpecRepsITYPE: TcxGridDBColumn;
    ViewSpecRepsNAMEMM: TcxGridDBColumn;
    ViewSpecRepsNAMEMD: TcxGridDBColumn;
    ViewSpecRepsNAMEPERS: TcxGridDBColumn;
    ViewSpecRepsQUESTS: TcxGridDBColumn;
    ViewSpecRepsTABSUM: TcxGridDBColumn;
    ViewSpecRepsBEGTIME: TcxGridDBColumn;
    ViewSpecRepsENDTIME: TcxGridDBColumn;
    ViewSpecRepsDISCONT: TcxGridDBColumn;
    ViewSpecRepsOPERTYPE: TcxGridDBColumn;
    ViewSpecRepsCHECKNUM: TcxGridDBColumn;
    ViewSpecRepsSKLAD: TcxGridDBColumn;
    SpeedItem4: TSpeedItem;
    ViewSpecRepsName: TcxGridDBColumn;
    aPrintCheck: TAction;
    ViewSpecRepsSTREAM: TcxGridDBColumn;
    ViewSpecRepsNAMESTREAM: TcxGridDBColumn;
    SpeedItem5: TSpeedItem;
    acExportEx: TAction;
    ViewSpecRepsSTATION: TcxGridDBColumn;
    ViewSpecRepsINEED: TcxGridDBColumn;
    ViewSpecRepsQUANTITY1: TcxGridDBColumn;
    ViewSpecRepsSUMMA1: TcxGridDBColumn;
    SpeedItem6: TSpeedItem;
    frRepTabs: TfrReport;
    dsCheck: TfrDBDataSet;
    CheckEdit: TCheckBox;
    ViewTabsID: TcxGridDBColumn;
    ViewTabsID_PERSONAL: TcxGridDBColumn;
    ViewTabsNUMTABLE: TcxGridDBColumn;
    ViewTabsQUESTS: TcxGridDBColumn;
    ViewTabsTABSUM: TcxGridDBColumn;
    ViewTabsBEGTIME: TcxGridDBColumn;
    ViewTabsENDTIME: TcxGridDBColumn;
    ViewTabsDISCONT: TcxGridDBColumn;
    ViewTabsOPERTYPE: TcxGridDBColumn;
    ViewTabsCHECKNUM: TcxGridDBColumn;
    ViewTabsSKLAD: TcxGridDBColumn;
    ViewTabsSTATION: TcxGridDBColumn;
    ViewTabsNAME: TcxGridDBColumn;
    ViewTabsDISNAME: TcxGridDBColumn;
    ViewTabsNUMZ: TcxGridDBColumn;
    SIChangePay: TSpeedItem;
    ViewTabsCASHNUM: TcxGridDBColumn;
    ViewSpecRepsCASHNUM: TcxGridDBColumn;
    ViewTabsCASHERNAME: TcxGridDBColumn;
    ViewTabsPLATCARDNAME: TcxGridDBColumn;
    ViewSpecRepsPLATCARDNAME: TcxGridDBColumn;
    ViewTabsPLATTYPENAME: TcxGridDBColumn;
    ViewSpecRepsPLATTYPENAME: TcxGridDBColumn;
    ViewSpecRepsNUMZ: TcxGridDBColumn;
    ViewTabsPERCENT: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acPeriodExecute(Sender: TObject);
    procedure acSpecSelExecute(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
    procedure SpeedItem4Click(Sender: TObject);
    procedure aPrintCheckExecute(Sender: TObject);
    procedure SpeedItem5Click(Sender: TObject);
    procedure SpeedItem6Click(Sender: TObject);
    procedure CheckEditClick(Sender: TObject);
    procedure OnChangePeriod();
    procedure SIChangePayClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmTabs: TfmTabs;

implementation

uses Dm, Un1, PeriodUni, SpecSel, UnCash, Period, ChangePay;

{$R *.dfm}
{$I dll.inc}

procedure TfmTabs.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  ViewTabs.RestoreFromIniFile(CurDir+GridIni);
  ViewSpecReps.RestoreFromIniFile(CurDir+GridIni);
  SIChangePay.Visible := CheckEdit.Checked;

end;

procedure TfmTabs.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  dmC.taTabsAllSel.Active:=False;
  ViewTabs.StoreToIniFile(CurDir+GridIni,False);
  ViewSpecReps.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmTabs.acPeriodExecute(Sender: TObject);
begin
  try
    if CheckEdit.Checked then abort;

    fmPeriod:=TfmPeriod.Create(Application); //��� ���� ������������

    fmPeriod.ShowModal;
    if fmPeriod.ModalResult=mrOk then
    begin
      OnChangePeriod();
    end;
  finally
    fmPeriod.Release;
  end;
end;

procedure TfmTabs.acSpecSelExecute(Sender: TObject);
begin
  with dmC do
  begin
    taSpecAllSel.Active:=False;
    taSpecAllSel.ParamByName('IDHEAD').AsInteger:=taTabsAllSelID.AsInteger;
    taSpecAllSel.Active:=True;

    taSelCashAll.Active:=False;
    taSelCashAll.ParamByName('IDHEAD').AsInteger:=taTabsAllSelID.AsInteger;
    taSelCashAll.Active:=True;

    fmSpecSel.ShowModal;

  end;
end;

procedure TfmTabs.SpeedItem3Click(Sender: TObject);
begin
  close;
end;

procedure TfmTabs.SpeedItem4Click(Sender: TObject);
begin
  //��������� ����
  if LevelTabs.Visible=True then
  begin
    LevelTabs.Visible:=False;
    SpeedItem2.Enabled:=False;
    SpeedItem6.Enabled:=False;  //���
    ViewSpecRepsQUANTITY1.Options.Editing := CheckEdit.Checked;   //oea
    ViewSpecRepsINEED.Options.Editing := CheckEdit.Checked;       //oea
    LevelSpec.Visible:=True;
    SIChangePay.Enabled:=False;  //���

  end else
  begin
    LevelTabs.Visible:=True;
    SpeedItem2.Enabled:=True;
    SpeedItem6.Enabled:=True;  //���
    LevelSpec.Visible:=False;
    SIChangePay.Enabled:=True;  //���
  end;
end;



procedure TfmTabs.aPrintCheckExecute(Sender: TObject);
Var iCo:Integer;
    rSum:Real;
    Rec:TcxCustomGridRecord;
    i,j,iSum: Integer;

    {
Var sGr:String;
    iGr:Integer;
    iCo:Integer;
    iNum: Integer;
}

begin
  //������ �����
  iCo:=ViewTabs.Controller.SelectedRecordCount;
  if iCo>0 then
  begin
    if MessageDlg('���������� ���� ('+IntToStr(iCo)+')?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      OpenDll('������','AERF',PChar('COM1'),0);
      with dmC do
      begin
        //ViewTabsTABSUM
        for i:=0 to ViewTabs.Controller.SelectedRecordCount-1 do
        begin
          Rec:=ViewTabs.Controller.SelectedRecords[i];

          for j:=0 to Rec.ValueCount-1 do
          begin
            if ViewTabs.Columns[j].Name='ViewTabsTABSUM' then break;
          end;

          rSum:=Rec.Values[j];

          if rSum>0 then
          begin
            iSum:=RoundEx(rSum*100);
            StartReceipt(0,1,'','','');
            ItemReceiptPlus('����� �� ����� ','12345','��.','','',iSum,1000,1);
            TotalReceipt;
            TenderReceipt(0,iSum,'');
            CloseReceipt;
          end;
        end;
      end;
      XReport;
      delay(1000);
      CloseDll;
    end;
  end;

{

iCo:=ViewMenuCr.Controller.SelectedRecordCount;
    if iCo>0 then
    begin
      if MessageDlg('�� ������������� ������ ����������� ��������� ������� ('+IntToStr(iCo)+' ��.) � ������ "'+sGr+'"?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        with dmC do
        begin
          taMenu.Active:=False;
          taMenu.Active:=True;

          for i:=0 to ViewMenuCr.Controller.SelectedRecordCount-1 do
          begin
            Rec:=ViewMenuCr.Controller.SelectedRecords[i];

            for j:=0 to Rec.ValueCount-1 do
            begin
              if ViewMenuCr.Columns[j].Name='ViewMenuCrSIFR' then break;
            end;

            iNum:=Rec.Values[j];
          //��� ��� - ����������

            if taMenu.Locate('SIFR',iNum,[]) then
            begin
              trUpd2.StartTransaction;
              taMenu.Edit;
              taMenuPARENT.AsInteger:=iGr;
              taMenu.Post;
              trUpd2.Commit;
            end;
          end;
          taMenu.Active:=False;
          quMenuSel.FullRefresh;
        end;
      end;
    end;
}


end;

procedure TfmTabs.SpeedItem5Click(Sender: TObject);
begin
//  with dmC do
//  begin
    if LevelTabs.Visible=True then
    begin //ViewTabs
//      prExportExel(ViewTabs,dsTabsAllSel,taTabsAllSel);
      prNExportExel4(ViewTabs);

    end else //levelspec
    begin
//      prExportExel(ViewSpecReps,dsSelSpecAll,quSelSpecAll);
      prNExportExel4(ViewSpecReps);
    end;
//  end;
end;

procedure TfmTabs.SpeedItem6Click(Sender: TObject);
begin
  frRepTabs.LoadFromFile(CurDir + 'PreCheckTabs.frf');

   with dmC do
  begin

    quCheckTabs.Active:=False;
    quCheckTabs.ParamByName('IDTAB').AsInteger:=taTabsAllSelID.AsInteger;
    quCheckTabs.Active:=True;


    frVariables.Variable['Depart']:=CommonSet.DepartName;

    frRepTabs.PrepareReport;
    frRepTabs.ShowPreparedReport;
    
  end;

end;

procedure TfmTabs.CheckEditClick(Sender: TObject);
begin
  ViewSpecRepsQUANTITY1.Options.Editing := CheckEdit.Checked;
  ViewSpecRepsINEED.Options.Editing := CheckEdit.Checked;       
  if CheckEdit.Checked then
   begin
      TrebSel.DateFrom:=Trunc(Date)-1 + StrtoTimeDef(CommonSet.ZTimeShift,0);
      TrebSel.DateTo:=Trunc(Date) + StrtoTimeDef(CommonSet.ZTimeShift,0);
      OnChangePeriod();
   end;
   SIChangePay.Visible := CheckEdit.Checked;

end;

procedure TfmTabs.OnChangePeriod();
begin
  with dmC do
  begin
        taTabsAllSel.Active:=False;
        taTabsAllSel.ParamByName('DBEG').AsDateTime:=TrebSel.DateFrom;
        taTabsAllSel.ParamByName('DEND').AsDateTime:=TrebSel.DateTo;
        taTabsAllSel.Active:=True;
        taTabsAllSel.Last;

        quSelSpecAll.Active:=False;
        quSelSpecAll.ParamByName('DBEG').AsDateTime:=TrebSel.DateFrom;
        quSelSpecAll.ParamByName('DEND').AsDateTime:=TrebSel.DateTo;
        quSelSpecAll.Active:=True;
   end;

   fmTabs.Caption:='����������� ������ �� ������ � '+FormatDateTime('dd.mm.yyyy hh:mm',TrebSel.DateFrom)+' �� '+FormatDateTime('dd.mm.yyyy hh:mm',TrebSel.DateTo);
end;

procedure TfmTabs.SIChangePayClick(Sender: TObject);
begin
  try
   FmChagePay:=TFmChagePay.Create(Application);
   with dmC do
   begin
    FmChagePay.cxID_TAB.Text := taTabsAllSelID.AsString;
    FmChagePay.cxOPERTYPE.Text := taTabsAllSelOPERTYPE.AsString;
    if taTabsAllSelOPERTYPE.AsString = 'SaleBank' then
      FmChagePay.GridBN.Visible:=True
    else
      FmChagePay.GridBN.Visible:=False;

    FmChagePay.ShowModal;
    if FmChagePay.ModalResult=mrOk then
    begin
    end;

   end;
  finally
   FmChagePay.Release;
  end;



end;

end.
