unit repDiscCli;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxCurrencyEdit, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxControls, cxGridCustomView, cxGrid, SpeedBar, ExtCtrls, Placemnt,
  ComObj, ActiveX, Excel2000, OleServer, ExcelXP, FR_DSet, FR_DBSet,
  FR_Class;

type
  TfmDiscCli = class(TForm)
    StatusBar1: TStatusBar;
    FormPlacement1: TFormPlacement;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem2: TSpeedItem;
    GridDCCli: TcxGrid;
    ViewDCCli: TcxGridDBTableView;
    LevelDCCli: TcxGridLevel;
    ViewDCCliNAME: TcxGridDBColumn;
    ViewDCCliDISCONT: TcxGridDBColumn;
    ViewDCCliPERCENT: TcxGridDBColumn;
    ViewDCCliSSUM: TcxGridDBColumn;
    ViewDCCliSUMPOS: TcxGridDBColumn;
    ViewDCCliITSUM: TcxGridDBColumn;
    SpeedItem1: TSpeedItem;
    frRepDC: TfrReport;
    frDbRep1: TfrDBDataSet;
    ViewDCCliPHONE: TcxGridDBColumn;
    ViewDCCliBERTHDAY: TcxGridDBColumn;
    ViewDCCliEMAIL: TcxGridDBColumn;
    ViewDCCliADRES: TcxGridDBColumn;
    SpeedItem5: TSpeedItem;
    ViewDCCliMONTHDAY: TcxGridDBColumn;
    procedure SpeedItem4Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SpeedItem3Click(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure SpeedItem5Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmDiscCli: TfmDiscCli;

implementation

uses Un1, DmRnDisc, Period, MainDiscount;

{$R *.dfm}

procedure TfmDiscCli.SpeedItem4Click(Sender: TObject);
begin
  close;
end;

procedure TfmDiscCli.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  GridDCCli.Align:=AlClient;
  ViewDCCli.RestoreFromIniFile(CurDir+GridIni);
end;

procedure TfmDiscCli.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewDCCli.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmDiscCli.SpeedItem3Click(Sender: TObject);
begin
  fmPeriod:=TfmPeriod.Create(Application);
  fmPeriod.DateTimePicker1.Date:=TrebSel.DateFrom;
  fmPeriod.DateTimePicker2.Date:=TrebSel.DateTo;
  fmPeriod.DateTimePicker3.Visible:=True;
  fmPeriod.DateTimePicker3.Time:=Frac(TrebSel.DateFrom);
  fmPeriod.DateTimePicker4.Visible:=True;
  fmPeriod.DateTimePicker4.Time:=Frac(TrebSel.DateTo);

  fmPeriod.cxCheckBox1.Visible:=False;

  fmPeriod.ShowModal;
  if fmPeriod.ModalResult=mrOk then
  begin
    with dmCDisc do
    begin
      fmPeriod.Release;
      ViewDCCli.BeginUpdate;
      quRep1.Active:=False;
      quRep1.ParamByName('DateB').AsDateTime:=TrebSel.DateFrom;
      quRep1.ParamByName('DateE').AsDateTime:=TrebSel.DateTo;
      quRep1.Active:=True;
      fmDiscCli.Caption:='������ �� �� �� ������ � '+FormatDateTime('dd.mm.yyyy hh:nn',TrebSel.DateFrom)+' �� '+FormatDateTime('dd.mm.yyyy hh:nn',TrebSel.DateTo);
      fmDiscCli.Show;
      ViewDCCli.EndUpdate;

      exit;
    end;
  end;
  fmPeriod.Release;
end;

procedure TfmDiscCli.SpeedItem2Click(Sender: TObject);
begin
  with dmCDisc do prNExportExel4(ViewDCCli);
end;

procedure TfmDiscCli.SpeedItem1Click(Sender: TObject);
begin
//������
  with dmCDisc do
  begin
    StatusBar1.Panels[0].Text:='����� ���� ������������.'; delay(10);
    ViewDCCli.BeginUpdate;

  //  dsRep1.DataSet:=nil;

    quRep1.Filter:=ViewDCCli.DataController.Filter.FilterText;
    quRep1.Filtered:=True;


    frRepDC.LoadFromFile(CurDir + 'DiscSum.frf');

    frVariables.Variable['sPeriod']:='c '+FormatDateTime('dd.mm.yyyy hh:nn',TrebSel.DateFrom)+' �� '+FormatDateTime('dd.mm.yyyy hh:nn',TrebSel.DateTo);
//      frVariables.Variable['Depart']:=CommonSet.DepartName;

    frRepDC.ReportName:='������ �� ���������� ������.';
    frRepDC.PrepareReport;
    frRepDC.ShowPreparedReport;

    quRep1.Filter:='';
    quRep1.Filtered:=False;

//    quRep1.First;
//    dsRep1.DataSet:=quRep1;

    ViewDCCli.EndUpdate;
    StatusBar1.Panels[0].Text:='������������ ��.';
  end;
end;

procedure TfmDiscCli.SpeedItem5Click(Sender: TObject);
begin
//������
  with dmCDisc do
  begin
    StatusBar1.Panels[0].Text:='����� ���� ������������.'; delay(10);
    ViewDCCli.BeginUpdate;

  //  dsRep1.DataSet:=nil;

    quRep1.Filter:=ViewDCCli.DataController.Filter.FilterText;
    quRep1.Filtered:=True;


    frRepDC.LoadFromFile(CurDir + 'DiscSum1.frf');

    frVariables.Variable['sPeriod']:='c '+FormatDateTime('dd.mm.yyyy hh:nn',TrebSel.DateFrom)+' �� '+FormatDateTime('dd.mm.yyyy hh:nn',TrebSel.DateTo);
//      frVariables.Variable['Depart']:=CommonSet.DepartName;

    frRepDC.ReportName:='������ �� ���������� ������.';
    frRepDC.PrepareReport;
    frRepDC.ShowPreparedReport;

    quRep1.Filter:='';
    quRep1.Filtered:=False;

//    quRep1.First;
//    dsRep1.DataSet:=quRep1;

    ViewDCCli.EndUpdate;
    StatusBar1.Panels[0].Text:='������������ ��.';
  end;

end;

end.
