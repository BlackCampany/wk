unit MainAppend;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, cxLookAndFeelPainters, cxButtons, ExtCtrls,
  FIBDatabase, pFIBDatabase, DBTables, DB, FIBDataSet, pFIBDataSet,
  ComCtrls;

type
  TfmMainAppend = class(TForm)
    Memo1: TMemo;
    cxButton1: TcxButton;
    Timer1: TTimer;
    dmC: TpFIBDatabase;
    trSelect: TpFIBTransaction;
    trUpdate: TpFIBTransaction;
    taCheck: TTable;
    Session1: TSession;
    taCheckUNI: TIntegerField;
    taCheckSys_Num: TIntegerField;
    taCheckCnum: TSmallintField;
    taCheckLogicDate: TDateField;
    taCheckRealDate: TDateField;
    taCheckOpenTime: TStringField;
    taCheckCloseTime: TStringField;
    taCheckCover: TSmallintField;
    taCheckCashier: TSmallintField;
    taCheckWaiter: TSmallintField;
    taCheckUnit: TStringField;
    taCheckDepart: TStringField;
    taCheckTotal: TFloatField;
    taCheckBaseKurs: TFloatField;
    taCheckDeleted: TSmallintField;
    taCheckManager: TSmallintField;
    taCheckCharge: TFloatField;
    taCheckTable: TStringField;
    taCheckOpenDate: TDateField;
    taCheckCount: TSmallintField;
    taCheckNacKurs: TFloatField;
    taCheckTotalR: TFloatField;
    taCheckCoverR: TSmallintField;
    taCheckTaxSum: TFloatField;
    taCheckTaxSumR: TFloatField;
    taCheckTaxRate: TFloatField;
    taCheckTaxRateR: TFloatField;
    taCheckBonus: TFloatField;
    taCheckBonusCard: TFloatField;
    taRCheck: TTable;
    taDCheck: TTable;
    taDCheckUNI: TIntegerField;
    taDCheckSys_Num: TIntegerField;
    taDCheckCnum: TSmallintField;
    taDCheckSifr: TSmallintField;
    taDCheckSum: TFloatField;
    taDCheckCardCod: TFloatField;
    taDCheckPerson: TSmallintField;
    taDCheckSumR: TFloatField;
    taPCheck: TTable;
    taVCheck: TTable;
    taCashSail: TpFIBDataSet;
    Timer2: TTimer;
    ProgressBar1: TProgressBar;
    taCashSailCASHNUM: TFIBIntegerField;
    taCashSailZNUM: TFIBIntegerField;
    taCashSailCHECKNUM: TFIBIntegerField;
    taCashSailTAB_ID: TFIBIntegerField;
    taCashSailTABSUM: TFIBFloatField;
    taCashSailCASHERID: TFIBIntegerField;
    taCashSailWAITERID: TFIBIntegerField;
    taCashSailCHDATE: TFIBDateTimeField;
    taCashSailBEGTIME: TFIBDateTimeField;
    taCashSailNUMTABLE: TFIBStringField;
    taCashSailQUESTS: TFIBIntegerField;
    taCashSailID: TFIBIntegerField;
    taSpec: TpFIBDataSet;
    taSpecID_TAB: TFIBIntegerField;
    taSpecID: TFIBIntegerField;
    taSpecID_PERSONAL: TFIBIntegerField;
    taSpecNUMTABLE: TFIBStringField;
    taSpecSIFR: TFIBIntegerField;
    taSpecPRICE: TFIBFloatField;
    taSpecQUANTITY: TFIBFloatField;
    taSpecDISCOUNTPROC: TFIBFloatField;
    taSpecDISCOUNTSUM: TFIBFloatField;
    taSpecSUMMA: TFIBFloatField;
    taSpecISTATUS: TFIBIntegerField;
    taSpecITYPE: TFIBSmallIntField;
    taRCheckUNI: TIntegerField;
    taRCheckSys_Num: TIntegerField;
    taRCheckCnum: TSmallintField;
    taRCheckSifr: TSmallintField;
    taRCheckQnt: TFloatField;
    taRCheckPrice: TFloatField;
    taRCheckComp: TStringField;
    taRCheckRealPrice: TFloatField;
    taRCheckQntR: TFloatField;
    taRCheckNalog: TFloatField;
    taCashSailDISCONT: TFIBStringField;
    taVCheckUNI: TAutoIncField;
    taVCheckLogicDate: TDateField;
    taVCheckRealDate: TDateField;
    taVCheckTime: TStringField;
    taVCheckSifr: TSmallintField;
    taVCheckComp: TSmallintField;
    taVCheckQnt: TFloatField;
    taVCheckPrice: TFloatField;
    taVCheckReason: TSmallintField;
    taVCheckManager: TSmallintField;
    taVCheckWaiter: TSmallintField;
    taVCheckTable: TStringField;
    taVCheckUnit: TStringField;
    taVCheckDepart: TStringField;
    taVCheckTabNum: TFloatField;
    taDel: TpFIBDataSet;
    taDelID: TFIBIntegerField;
    taDelID_PERSONAL: TFIBIntegerField;
    taDelNUMTABLE: TFIBStringField;
    taDelQUESTS: TFIBIntegerField;
    taDelTABSUM: TFIBFloatField;
    taDelBEGTIME: TFIBDateTimeField;
    taDelENDTIME: TFIBDateTimeField;
    taDelDISCONT: TFIBStringField;
    taDelOPERTYPE: TFIBStringField;
    taDelCHECKNUM: TFIBIntegerField;
    taDelSKLAD: TFIBSmallIntField;
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    Procedure prExportCash;
  end;

var
  fmMainAppend: TfmMainAppend;
  iProc:Integer;
  iCurR:Integer;

implementation

uses Un1;

{$R *.dfm}

Procedure TfmMainAppend.prExportCash;
Var iNum,iNumR,iNumD,iNumV:Integer;
    IdBeg:Integer;
    StrWk:String;
    rDSum:Real;
begin
  Memo1.Lines.Add('�������� ���� FB.');
  try
    dmC.Connected:=False;
    dmC.DatabaseName:=DBName;
    dmC.Connected:=True;
    if dmC.Connected=True then Memo1.Lines.Add('���������� ��.');
    Memo1.Lines.Add('�������� ���� Db.');
    Session1.Active:=False;
    Session1.AddPassword('767186508400000');
    Session1.Active:=True;

    taCheck.Active:=False;
    taCheck.DatabaseName:=CommonSet.PathRkData;
    taCheck.Active:=True;

    taRCheck.Active:=False;
    taRCheck.DatabaseName:=CommonSet.PathRkData;
    taRCheck.Active:=True;

    taPCheck.Active:=False;
    taPCheck.DatabaseName:=CommonSet.PathRkData;
    taPCheck.Active:=True;

    taDCheck.Active:=False;
    taDCheck.DatabaseName:=CommonSet.PathRkData;
    taDCheck.Active:=True;

    taVCheck.Active:=False;
    taVCheck.DatabaseName:=CommonSet.PathRkData;
    taVCheck.Active:=True;

    Memo1.Lines.Add('���� Db ��.');

    //���������
    Memo1.Lines.Add('�����. ���� �������� ������.');

    //����
    iNum:=1;
    iNumR:=1;
    iNumD:=1;
    iNumV:=1;
    IdBeg:=1;
    taCheck.First;
    if not taCheck.Eof then
    begin
      taCheck.Last;
      iNum:=taCheckUNI.AsInteger+1;
      iDBeg:=taCheckSys_Num.AsInteger+1;
    end;
    taRCheck.First;
    if not taRCheck.Eof then
    begin
      taRCheck.Last;
      iNumR:=taRCheckUNI.AsInteger+1;
    end;
    taDCheck.First;
    if not taDCheck.Eof then
    begin
      taDCheck.Last;
      iNumD:=taDCheckUNI.AsInteger+1;
    end;
    taVCheck.First;
    if not taVCheck.Eof then
    begin
      taVCheck.Last;
      iNumV:=taVCheckTabNum.AsInteger+1;
    end;

    taCashSail.Active:=False;
    taCashSail.ParamByName('IDBEG').AsInteger:=IdBeg;
    taCashSail.Active:=True;

    taCashSail.First;
    ProgressBar1.Position:=0;
    ProgressBar1.Visible:=True;
    Timer2.Enabled:=True;
    iProc:=taCashSail.RecordCount;
    iCurR:=1;
    while not taCashSail.Eof do
    begin
      taCheck.Append;
      taCheckUNI.AsInteger:=iNum;
      taCheckSys_Num.AsInteger:=taCashSailID.AsInteger;
      taCheckCnum.AsInteger:=taCashSailCHECKNUM.AsInteger;
      taCheckLogicDate.AsDateTime:=Trunc(taCashSailCHDATE.AsDateTime);
      taCheckRealDate.AsDateTime:=Trunc(taCashSailCHDATE.AsDateTime);
      taCheckOpenTime.AsString:=FormatDateTime('hh:nn',taCashSailBEGTIME.AsDateTime);
      taCheckCloseTime.AsString:=FormatDateTime('hh:nn',taCashSailCHDATE.AsDateTime);
      taCheckCover.AsInteger:=taCashSailQUESTS.AsInteger;
      taCheckCashier.AsInteger:=taCashSailCASHERID.AsInteger;
      taCheckWaiter.AsInteger:=taCashSailWAITERID.AsInteger;
      StrWk:=IntToStr(taCashSailCASHNUM.AsInteger);
      If Length(StrWk)<2 then StrWk:='0'+StrWk;
      taCheckUnit.AsString:=Copy(StrWk,1,2);
      StrWk:=CommonSet.DepartId;
      If Length(StrWk)<2 then StrWk:='0'+StrWk;
      taCheckDepart.AsString:=Copy(StrWk,1,2);
      taCheckTotal.AsFloat:=taCashSailTABSUM.AsFloat;
      taCheckBaseKurs.AsFloat:=1;
      taCheckDeleted.AsInteger:=0;
      taCheckManager.AsInteger:=0;
      StrWk:=taCashSailNUMTABLE.AsString;
      taCheckTable.AsString:=Copy(StrWk,1,4);
      taCheckOpenDate.AsDateTime:=Trunc(taCashSailCHDATE.AsDateTime);
      taCheckCount.AsInteger:=29701;
      taCheckNacKurs.AsFloat:=1;
      taCheckTotalR.AsFloat:=taCashSailTABSUM.AsFloat;
      taCheckCoverR.AsInteger:=taCashSailQUESTS.AsInteger;
      taCheckTaxSum.AsFloat:=0;
      taCheckTaxSumR.AsFloat:=0;
      taCheckTaxRate.AsFloat:=0;
      taCheckTaxRateR.AsFloat:=0;
      taCheckBonus.AsFloat:=0;
      taCheckBonusCard.AsFloat:=0;
      taCheck.Post;

      rDSum:=0;
      taSpec.Active:=False;
      taSpec.ParamByName('IDHEAD').AsInteger:=taCashSailTAB_ID.AsInteger;
      taSpec.Active:=True;
      taSpec.First;
      while not taSpec.Eof do
      begin
        taRCheck.Append;
        taRCheckUNI.AsInteger:=iNumR;
        taRCheckSys_Num.AsInteger:=taCashSailID.AsInteger;
        taRCheckCnum.AsInteger:=taCashSailCHECKNUM.AsInteger;
        taRCheckSifr.AsInteger:=taSpecSIFR.AsInteger;
        taRCheckQnt.AsFloat:=taSpecQUANTITY.AsFloat;
        taRCheckPrice.AsFloat:=taSpecPRICE.AsFloat;
        if taSpecITYPE.AsInteger=0 then taRCheckComp.AsInteger:=1
        else taRCheckComp.AsInteger:=0;
        if taSpecQUANTITY.AsFloat<>0 then taRCheckRealPrice.AsFloat:=taSpecSUMMA.AsFloat/taSpecQUANTITY.AsFloat
        else taRCheckRealPrice.AsFloat:=taSpecPRICE.AsFloat;
        taRCheckQntR.AsFloat:=taSpecQUANTITY.AsFloat;
        taRCheckNalog.AsFloat:=0;
        taRCheck.Post;
        inc(iNumR);

        rDSum:=rDSum+taSpecDISCOUNTSUM.AsFloat;
        taSpec.Next;
      end;
      taSpec.Active:=False;

      if rDSum<>0 then
      begin
        taDCheck.Append;
        taDCheckUNI.AsInteger:=iNumD;
        taDCheckSys_Num.AsInteger:=taCashSailID.AsInteger;
        taDCheckCnum.AsInteger:=taCashSailCHECKNUM.AsInteger;
        taDCheckSifr.AsInteger:=1;
        taDCheckSum.AsFloat:=-1*rDSum;
        StrWk:=taCashSailDISCONT.AsString;
        if length(StrWk)>=5 then StrWk:=Copy(StrWk,length(StrWk)-4,5);
        taDCheckCardCod.AsFloat:=StrToIntDef(StrWk,0);
        taDCheckPerson.AsInteger:=-1;
        taDCheckSumR.AsFloat:=-1*rDSum;
        taDCheck.Post;
        inc(iNumD);
      end;


      Inc(iNum);
      inc(iCurR);
      taCashSail.Next;
    end;
    //�� �������� ����������
    // ������ ����� ��������� VCheck
    // �������� ��� ���������
    taDel.Active:=False;
    taDel.ParamByName('IDBEG').AsInteger:=iNumV;
    taDel.Active:=True;
    taDel.First;
    while not taDel.Eof do
    begin
      taSpec.Active:=False;
      taSpec.ParamByName('IDHEAD').AsInteger:=taDelID.AsInteger;
      taSpec.Active:=True;
      taSpec.First;
      while not taSpec.Eof do
      begin
        taVCheck.Append;
        taVCheckLogicDate.AsDateTime:=Trunc(taDelENDTIME.AsDateTime);
        taVCheckRealDate.AsDateTime:=Trunc(taDelENDTIME.AsDateTime);
        taVCheckTime.AsString:=FormatDateTime('hh:nn',taDelENDTIME.AsDateTime);
        taVCheckSifr.AsInteger:=taSpecSIFR.AsInteger;
        taVCheckComp.AsInteger:=taSpecITYPE.AsInteger; //0-����� 1-���
        taVCheckQnt.AsFloat:=taSpecQUANTITY.AsFloat;
        taVCheckPrice.AsFloat:=taSpecPRICE.AsFloat;
        if taDelSKLAD.AsInteger=0 then taVCheckReason.AsInteger:=2;  //��������� ��������
        if taDelSKLAD.AsInteger=1 then taVCheckReason.AsInteger:=1;  //�� ��������� ��������
        taVCheckManager.AsInteger:=taDelID_PERSONAL.AsInteger;
        taVCheckWaiter.AsInteger:=taDelID_PERSONAL.AsInteger;
        if length(taDelNUMTABLE.AsString)>3 then
        taVCheckTable.AsString:=Copy(taDelNUMTABLE.AsString,1,4)
        else taVCheckTable.AsString:=taDelNUMTABLE.AsString;
        taVCheckUnit.AsString:=Copy(CommonSet.sUnit,1,2);
        StrWk:=CommonSet.DepartId;
        If Length(StrWk)<2 then StrWk:='0'+StrWk;
        taVCheckDepart.AsString:=Copy(StrWk,1,2);
        taVCheckTabNum.AsInteger:=taDelID.AsInteger;
        taVCheck.Post;

        taSpec.Next;
      end;
      taSpec.Active:=False;

      taDel.Next;
    end;
    taDel.Active:=False;

    Timer2.Enabled:=False;
    Memo1.Lines.Add('�������� ��������� ��.');
    ProgressBar1.Visible:=False;
  finally
    cxButton1.Enabled:=True;
//    Memo1.Lines.Add('�������� ���� RK.');
    taCheck.Active:=False;
    taRCheck.Active:=False;
    taPCheck.Active:=False;
    taDCheck.Active:=False;
    taVCheck.Active:=False;

    Memo1.Lines.Add('�������� ���� FB.');
    dmC.Connected:=False;
  end;
end;

procedure TfmMainAppend.FormCreate(Sender: TObject);
begin
  CurDir := ExtractFilePath(ParamStr(0));
  ReadIni;
  Memo1.Clear;
  Timer1.Enabled:=True;
end;

procedure TfmMainAppend.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled:=False;
  cxButton1.Enabled:=False;
  prExportCash;
end;

procedure TfmMainAppend.cxButton1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmMainAppend.Timer2Timer(Sender: TObject);
begin
  ProgressBar1.Position:=round(iCurR/iProc*100);
  delay(10);
end;

end.
