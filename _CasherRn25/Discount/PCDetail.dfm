object fmPCDet: TfmPCDet
  Left = 304
  Top = 147
  Width = 696
  Height = 480
  Caption = #1055#1083#1072#1090#1077#1078#1085#1099#1077' '#1082#1072#1088#1090#1099' ('#1076#1077#1090#1072#1083#1100#1085#1086')'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object SpeedBar1: TSpeedBar
    Left = 0
    Top = 0
    Width = 688
    Height = 46
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Options = [sbAllowDrag, sbFlatBtns]
    BtnOffsetHorz = 4
    BtnOffsetVert = 4
    BtnWidth = 60
    BtnHeight = 40
    BevelInner = bvLowered
    TabOrder = 0
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object SpeedItem3: TSpeedItem
      BtnCaption = #1055#1077#1088#1080#1086#1076
      Caption = #1055#1077#1088#1080#1086#1076
      Glyph.Data = {
        BA030000424DBA030000000000003600000028000000140000000F0000000100
        18000000000084030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00800000800000
        8000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFF008000008000008000FFFFFFFFFFFF008000000000000000FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFF000000000000008000FFFFFFFFFFFF008000000000FFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FF000000008000FFFFFFFFFFFF008000000000FFFFFFFFFFFFFFFFFF000000FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFF000000
        008000FFFFFFFFFFFF008000000000FFFFFFFFFFFF000000008000FFFFFFFFFF
        FF000000000000FFFFFFFFFFFF008000000000FFFFFFFFFFFF000000008000FF
        FFFFFFFFFF008000000000FFFFFF00000000800000E100000000000000000000
        00000000000000000000E100008000000000FFFFFF000000008000FFFFFFFFFF
        FF00800000000000000000800000E10000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000E100008000000000000000008000FFFFFFFFFFFF008000
        000000FFFFFF00000000800000E1000000000000000000000000000000000000
        0000E100008000000000FFFFFF000000008000FFFFFFFFFFFF008000000000FF
        FFFFFFFFFF000000008000FFFFFFFFFFFF000000000000FFFFFFFFFFFF008000
        000000FFFFFFFFFFFF000000008000FFFFFFFFFFFF008000000000FFFFFFFFFF
        FFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFF
        FFFFFFFFFF000000008000FFFFFFFFFFFF008000000000FFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FF000000008000FFFFFFFFFFFF008000000000000000FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000
        008000FFFFFFFFFFFF008000008000008000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF008000008000008000FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      Hint = #1055#1077#1088#1080#1086#1076'|'
      Spacing = 1
      Left = 84
      Top = 4
      Visible = True
      OnClick = SpeedItem3Click
      SectionName = 'Untitled (0)'
    end
    object SpeedItem4: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Glyph.Data = {
        5E040000424D5E04000000000000360000002800000012000000130000000100
        18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
        8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
        0000CED3D6848684848684848684848684848684848684848684848684848684
        848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
        7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
        FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
        FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
        007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
        FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
        00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
        494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
        00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000}
      Hint = #1042#1099#1093#1086#1076'|'
      Spacing = 1
      Left = 454
      Top = 4
      Visible = True
      OnClick = SpeedItem4Click
      SectionName = 'Untitled (0)'
    end
    object SpeedItem1: TSpeedItem
      BtnCaption = #1055#1077#1095#1072#1090#1100
      Caption = #1055#1077#1095#1072#1090#1100
      Glyph.Data = {
        EE030000424DEE03000000000000360000002800000012000000110000000100
        180000000000B8030000C40E0000C40E00000000000000000000D6DBDED6DBDE
        D6DBDED6DBDED6DBDED6DBDED6DBDED6DBDED6DBDED6DBDED6DBDED6DBDED6DB
        DED6DBDED6DBDED6DBDED6DBDED6DBDE0000D6DBDED6DBDED6DBDE4A4D4A4A4D
        4A4A4D4A4A4D4A4A4D4A4A4D4A4A4D4A4A4D4A4A4D4A4A4D4A4A4D4AD6DBDED6
        DBDED6DBDED6DBDE0000D6DBDED6DBDE4A4D4AD6DBD6D6DBD6D6DBD6D6DBD6D6
        DBD6D6DBD6D6DBD6D6DBD6D6DBD64A4D4AD6DBD64A4D4AD6DBDED6DBDED6DBDE
        0000D6DBDE4A4D4A4A4D4A4A4D4A4A4D4A4A4D4A4A4D4A4A4D4A4A4D4A4A4D4A
        4A4D4A4A4D4A4A4D4A4A4D4AD6DBD64A4D4AD6DBDED6DBDE0000D6DBDE4A4D4A
        D6DBD6D6DBD6D6DBD6D6DBD6D6DBD6D6DBD64AFFFF4AFFFF4AFFFFD6DBD6D6DB
        D64A4D4A4A4D4A4A4D4AD6DBDED6DBDE0000D6DBDE4A4D4AD6DBD6D6DBD6D6DB
        D6D6DBD6D6DBD6D6DBD6ADAAADADAAADADAAADD6DBD6D6DBD64A4D4AD6DBD64A
        4D4AD6DBDED6DBDE0000D6DBDE4A4D4A4A4D4A4A4D4A4A4D4A4A4D4A4A4D4A4A
        4D4A4A4D4A4A4D4A4A4D4A4A4D4A4A4D4A4A4D4AD6DBD6D6DBD64A4D4AD6DBDE
        0000D6DBDE4A4D4AD6DBD6D6DBD6D6DBD6D6DBD6D6DBD6D6DBD6D6DBD6D6DBD6
        D6DBD6D6DBD64A4D4AD6DBD64A4D4AD6DBD64A4D4AD6DBDE0000D6DBDED6DBDE
        4A4D4A4A4D4A4A4D4A4A4D4A4A4D4A4A4D4A4A4D4A4A4D4A4A4D4A4A4D4AD6DB
        D64A4D4AD6DBD64A4D4A4A4D4AD6DBDE0000D6DBDED6DBDED6DBDE4A4D4AFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4A4D4A4A4D4A4A4D4A4A
        4D4AD6DBDED6DBDE0000D6DBDED6DBDED6DBDED6DBDE4A4D4AFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF4A4D4AD6DBDED6DBDED6DBDED6DBDED6DBDE
        0000D6DBDED6DBDED6DBDED6DBDE4A4D4A4A4D4A4A4D4A4A4D4A4A4D4A4A4D4A
        4A4D4A4A4D4A4A4D4AD6DBDED6DBDED6DBDED6DBDED6DBDE0000D6DBDED6DBDE
        D6DBDED6DBDE4A4D4AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4A4D
        4AD6DBDED6DBDED6DBDED6DBDED6DBDE0000D6DBDED6DBDED6DBDED6DBDE4A4D
        4A4A4D4A4A4D4A4A4D4A4A4D4A4A4D4A4A4D4A4A4D4A4A4D4A4A4D4AD6DBDED6
        DBDED6DBDED6DBDE0000D6DBDED6DBDED6DBDED6DBDED6DBDE4A4D4AFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4A4D4AD6DBDED6DBDED6DBDE
        0000D6DBDED6DBDED6DBDED6DBDED6DBDED6DBDE4A4D4A4A4D4A4A4D4A4A4D4A
        4A4D4A4A4D4A4A4D4A4A4D4A4A4D4AD6DBDED6DBDED6DBDE0000D6DBDED6DBDE
        D6DBDED6DBDED6DBDED6DBDED6DBDED6DBDED6DBDED6DBDED6DBDED6DBDED6DB
        DED6DBDED6DBDED6DBDED6DBDED6DBDE0000}
      Hint = #1055#1077#1095#1072#1090#1100'|'
      Spacing = 1
      Left = 24
      Top = 4
      Visible = True
      OnClick = SpeedItem1Click
      SectionName = 'Untitled (0)'
    end
    object SpeedItem2: TSpeedItem
      Glyph.Data = {
        56080000424D560800000000000036000000280000001A0000001A0000000100
        18000000000020080000C40E0000C40E00000000000000000000C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0004000004000004000004000C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C00000C0C0C0004000
        80808080808080808080808080808080808080808000400040E02040E020FFFF
        FFC8D0D4A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0808080C0
        C0C0C0C0C0C0C0C00000C0C0C0C0C0C000400040E02000C02000C02000C02000
        C02000400040E02040E020FFFFFF004000C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4A4A0A0A4A0A0404040C0C0C0C0C0C00000C0C0C0C0C0C0
        C0C0C000400000C02000C02000C02000400000C02040E020FFFFFF00400000C0
        20C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4A4A0A0A4A0A040
        2020808080C0C0C00000C0C0C0C0C0C0C0C0C0A4A0A000400000C02000400000
        C02040E020FFFFFF004000004000004000C8D0D4A4A0A0A4A0A0A4A0A0A4A0A0
        A4A0A0A4A0A0808080C8D0D4808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C000400040E02040E020FFFFFF004000F0FBFFF0FBFFF0FB
        FFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFA4A0A0C8D0D4C8D0D440
        2020808080C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C000400040E02040E020FF
        FFFF004000808080004000C8D0D4C0DCC0C0DCC0C0DCC0C0DCC0C0DCC0C0DCC0
        C0DCC0C8D0D4F0FBFFA4A0A0C8D0D4402020808080C0C0C00000C0C0C0C0C0C0
        C0C0C000400000C02040E020FFFFFF00400080E0A000C020808080004000FFFF
        FFFFFFFFFFFFFFF0FBFFC0DCC0C0DCC0F0FBFFC0DCC0F0FBFF808080C8D0D440
        2020808080C0C0C00000C0C0C0C0C0C000400000C02040E020FFFFFF004000FF
        FFFF00400000C02000C020808080004000FFFFFFFFFFFFC8D0D4F0FBFFC0DCC0
        C0DCC0C0DCC0F0FBFFC0DCC0A4A0A0404020808080C0C0C00000C0C0C0004000
        004000004000004000FFFFFFFFFFFFFFFFFFFFFFFF0040000040000040000040
        00C8D0D4F0FBFFC0DCC0C8D0D4C8D0D4F0FBFFC0DCC0F0FBFFFFFFFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFC0DCC0FFFFFFF0FBFFFFFFFFFFFFFFF0FBFFF0FBFFF0FBFF
        FFFFFFC0DCC0F0FBFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0F0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FB
        FFF0FBFFFFFFFFFFFFFFF0FBFFF0FBFFFFFFFFC0DCC0F0FBFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0F0
        FBFFF0FBFFF0FBFFC0DCC0C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4F0FBFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0C0DCC0C0DCC0F0FBFFF0FBFFC0DCC0FFFF
        FFC0DCC0F0FBFFC0DCC0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0C0
        DCC0F0FBFFF0FBFFF0FBFFF0FBFFFFFFFFF0FBFFF0FBFFC0DCC0F0FBFFFFFFFF
        FFFFFFFFFFFFFFFFFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0C0DCC0C8D0D4F0FBFFF0FBFFC8D0D4FFFF
        FFC8D0D4F0FBFFC0DCC0F0FBFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0F0
        FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFC8D0D4F0FBFFFFFFFF
        FFFFFFFFFFFFFFFFFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFF0FBFFC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4C8D0D4C8D0D4C8D0D4F0FBFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFF0FBFFF0
        FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080C0DCC0C8D0D4C8D0D4F0FBFFC8D0D4C0DCC0C0DCC0C8D0D4F0FB
        FFC8D0D4C0DCC0F0FBFFC8D0D4F0FBFFC8D0D4C8D0D4F0FBFFA4A0A080808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C080808080E0E040E0E040E0E080
        E0E040E0E080E0E080E0E040E0E080E0E040E0E040E0E080E0E040E0E080E0E0
        40E0E040E0E080E0E000E0E0408080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C080808080E0E080E0E0C0DCC080E0E0C0DCC080E0E080E0E0C0DCC080E0
        E0C0DCC080E0E080E0E0C0DCC080E0E0C0DCC080E0E080E0E0C0DCC000808000
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080F0FBFFF0FBFFF0FBFF80
        E0E0F0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFF
        F0FBFFF0FBFFF0FBFFF0FBFF00C0C0002040808080C0C0C00000C0C0C0C0C0C0
        C0C0C08080808080808080808080808080808080808080808080808080808080
        8080808080808080808080808080808080808080808080808080808080808000
        2040C0C0C0C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C00000}
      Hint = #1069#1082#1089#1087#1086#1088#1090' '#1074' Excel'
      Spacing = 1
      Left = 234
      Top = 4
      Visible = True
      OnClick = SpeedItem2Click
      SectionName = 'Untitled (0)'
    end
  end
  object GridPCDet: TcxGrid
    Left = 8
    Top = 56
    Width = 657
    Height = 361
    TabOrder = 1
    LookAndFeel.Kind = lfOffice11
    object ViewPCDet: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dmCDisc.dsPCDet
      DataController.Summary.DefaultGroupSummaryItems = <
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'SUMMA'
          Column = ViewPCDetSUMMA
        end
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'DISCOUNTSUM'
          Column = ViewPCDetDISCOUNTSUM
        end>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SUMMA'
          Column = ViewPCDetSUMMA
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'DISCOUNTSUM'
          Column = ViewPCDetDISCOUNTSUM
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.Footer = True
      OptionsView.GroupFooters = gfAlwaysVisible
      OptionsView.Indicator = True
      object ViewPCDetCLINAME: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'CLINAME'
        Styles.Content = dmCDisc.cxStyle17
        Width = 117
      end
      object ViewPCDetDISCONT: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1044#1050
        DataBinding.FieldName = 'DISCONT'
      end
      object ViewPCDetSIFR: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1090#1086#1074#1072#1088#1072
        DataBinding.FieldName = 'SIFR'
        Styles.Content = dmCDisc.cxStyle1
      end
      object ViewPCDetNAME1: TcxGridDBColumn
        Caption = #1041#1083#1102#1076#1086
        DataBinding.FieldName = 'NAME1'
        Styles.Content = dmCDisc.cxStyle4
      end
      object ViewPCDetPRICE: TcxGridDBColumn
        Caption = #1062#1077#1085#1072
        DataBinding.FieldName = 'PRICE'
      end
      object ViewPCDetQUANTITY: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086
        DataBinding.FieldName = 'QUANTITY'
      end
      object ViewPCDetSUMMA: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072
        DataBinding.FieldName = 'SUMMA'
        Styles.Content = dmCDisc.cxStyle5
      end
      object ViewPCDetNUMTABLE: TcxGridDBColumn
        Caption = #1053#1086#1084#1077#1088' '#1079#1072#1082#1072#1079#1072
        DataBinding.FieldName = 'NUMTABLE'
      end
      object ViewPCDetQUESTS: TcxGridDBColumn
        Caption = #1043#1086#1089#1090#1077#1081
        DataBinding.FieldName = 'QUESTS'
      end
      object ViewPCDetENDTIME: TcxGridDBColumn
        Caption = #1047#1072#1082#1088#1099#1090
        DataBinding.FieldName = 'ENDTIME'
      end
      object ViewPCDetTABSUM: TcxGridDBColumn
        DataBinding.FieldName = 'TABSUM'
        Visible = False
      end
      object ViewPCDetNAME: TcxGridDBColumn
        Caption = #1055#1077#1088#1089#1086#1085#1072#1083
        DataBinding.FieldName = 'NAME'
      end
      object ViewPCDetDISCOUNTSUM: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1089#1082#1080#1076#1082#1080
        DataBinding.FieldName = 'DISCOUNTSUM'
        Styles.Content = dmCDisc.cxStyle12
      end
      object ViewPCDetDISCOUNTPROC: TcxGridDBColumn
        Caption = '% '#1089#1082#1080#1076#1082#1080
        DataBinding.FieldName = 'DISCOUNTPROC'
        Styles.Content = dmCDisc.cxStyle12
      end
      object ViewPCDetNAMETYPE: TcxGridDBColumn
        Caption = #1058#1080#1087' '#1087#1083'. '#1082#1072#1088#1090#1099
        DataBinding.FieldName = 'NAMETYPE'
      end
      object ViewPCDetCOMMENT: TcxGridDBColumn
        Caption = #1050#1086#1084#1084#1077#1085#1090#1072#1088#1080#1081
        DataBinding.FieldName = 'COMMENT'
      end
      object ViewPCDetDATETO: TcxGridDBColumn
        Caption = #1044#1077#1081#1089#1090#1074#1091#1077#1090' '#1076#1086
        DataBinding.FieldName = 'DATETO'
      end
      object ViewPCDetDCNAME: TcxGridDBColumn
        Caption = #1050#1083#1080#1077#1085#1090
        DataBinding.FieldName = 'DCNAME'
        Options.Editing = False
        Styles.Content = dmCDisc.cxStyle5
      end
    end
    object LevelPCDet: TcxGridLevel
      GridView = ViewPCDet
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 427
    Width = 688
    Height = 19
    Panels = <
      item
        Width = 300
      end
      item
        Width = 50
      end>
  end
  object FormPlacement1: TFormPlacement
    Active = False
    Left = 496
    Top = 144
  end
  object frRep1: TfrReport
    InitialZoom = pzDefault
    PreviewButtons = [pbZoom, pbLoad, pbSave, pbPrint, pbFind, pbHelp, pbExit]
    RebuildPrinter = False
    Left = 72
    Top = 168
    ReportForm = {19000000}
  end
  object frquPCDet: TfrDBDataSet
    DataSet = dmCDisc.quPCDet
    Left = 152
    Top = 168
  end
end
