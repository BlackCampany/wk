object fmCurMessure: TfmCurMessure
  Left = 349
  Top = 263
  BorderStyle = bsSingle
  Caption = #1042#1099#1073#1077#1088#1080#1090#1077' '#1077#1076#1080#1085#1080#1094#1091' '#1080#1079#1084#1077#1088#1077#1085#1080#1103
  ClientHeight = 215
  ClientWidth = 256
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object GM: TcxGrid
    Left = 8
    Top = 8
    Width = 241
    Height = 200
    TabOrder = 0
    LookAndFeel.Kind = lfOffice11
    object VM: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      OnCellClick = VMCellClick
      DataController.DataSource = dmO.dsMessureSel
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      object VMID: TcxGridDBColumn
        DataBinding.FieldName = 'ID'
        Visible = False
      end
      object VMID_PARENT: TcxGridDBColumn
        DataBinding.FieldName = 'ID_PARENT'
        Visible = False
      end
      object VMNAMESHORT: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'NAMESHORT'
        Width = 119
      end
      object VMKOEF: TcxGridDBColumn
        Caption = #1050#1086#1101#1092#1092#1080#1094#1080#1077#1085#1090
        DataBinding.FieldName = 'KOEF'
        Width = 60
      end
    end
    object LM: TcxGridLevel
      GridView = VM
    end
  end
end
