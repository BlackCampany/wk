unit TOSel;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxTextEdit, cxImageComboBox,
  ExtCtrls, Placemnt, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxControls, cxGridCustomView, cxGrid,
  ComCtrls, SpeedBar, ActnList, XPStyleActnCtrls, ActnMan, cxCalendar,
  cxCurrencyEdit, cxContainer, cxMemo, FR_Class;

type
  TfmTO = class(TForm)
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    StatusBar1: TStatusBar;
    GridTO: TcxGrid;
    ViewTO: TcxGridDBTableView;
    LevelTO: TcxGridLevel;
    FormPlacement1: TFormPlacement;
    Timer1: TTimer;
    amTO: TActionManager;
    acExit: TAction;
    acPerSkl: TAction;
    acExportEx: TAction;
    ViewTOIDATE: TcxGridDBColumn;
    ViewTOREMNIN: TcxGridDBColumn;
    ViewTOREMNOUT: TcxGridDBColumn;
    ViewTOPOSTIN: TcxGridDBColumn;
    ViewTOPOSTOUT: TcxGridDBColumn;
    ViewTOVNIN: TcxGridDBColumn;
    ViewTOVNOUT: TcxGridDBColumn;
    ViewTOINV: TcxGridDBColumn;
    ViewTOQREAL: TcxGridDBColumn;
    Memo1: TcxMemo;
    acCreate: TAction;
    SpeedItem4: TSpeedItem;
    acPrintTO: TAction;
    SpeedItem5: TSpeedItem;
    acTODelPer: TAction;
    SpeedItem6: TSpeedItem;
    procedure FormCreate(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure acExportExExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acPerSklExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure acCreateExecute(Sender: TObject);
    procedure acPrintTOExecute(Sender: TObject);
    procedure acTODelPerExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmTO: TfmTO;

implementation

uses Un1, dmOffice, SelPerSkl, DMOReps;

{$R *.dfm}

procedure TfmTO.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  Timer1.Enabled:=True;
  GridTO.Align:=AlClient;
  ViewTO.RestoreFromIniFile(CurDir+GridIni);
end;

procedure TfmTO.acExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmTO.acExportExExecute(Sender: TObject);
begin
  // ������� � ������
  prExportExel(ViewTO,dmO.dsTO,dmO.quTO);
end;

procedure TfmTO.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewTO.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmTO.acPerSklExecute(Sender: TObject);
begin
//������
  fmSelPerSkl:=tfmSelPerSkl.Create(Application);
  with fmSelPerSkl do
  begin
    cxDateEdit1.Date:=CommonSet.DateFrom;
    quMHAll1.Active:=False;
    quMHAll1.Active:=True;
    quMHAll1.First;
    cxLookupComboBox1.EditValue:=quMHAll1ID.AsInteger;
    if CommonSet.IdStore>0 then  cxLookupComboBox1.EditValue:=CommonSet.IdStore;
  end;
  fmSelPerSkl.ShowModal;
  if fmSelPerSkl.ModalResult=mrOk then
  begin
    CommonSet.DateFrom:=Trunc(fmSelPerSkl.cxDateEdit1.Date);
    CommonSet.DateTo:=Trunc(fmSelPerSkl.cxDateEdit2.Date)+1;
    CommonSet.IdStore:=fmSelPerSkl.cxLookupComboBox1.EditValue;
    CommonSet.NameStore:=fmSelPerSkl.cxLookupComboBox1.Text;

    if CommonSet.DateTo>=iMaxDate then fmTO.Caption:='�������� ������ �� �� '+CommonSet.NameStore+' � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
    else fmTO.Caption:='�������� ������ �� �� '+CommonSet.NameStore+' �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);
    fmSelPerSkl.quMHAll1.Active:=False;
    fmSelPerSkl.Release;
    with dmO do
    begin
      quTO.Active:=False;
      quTO.ParamByName('DATEB').AsInteger:=Trunc(CommonSet.DateFrom);
      quTO.ParamByName('DATEE').AsInteger:=Trunc(CommonSet.DateTo);
      quTO.ParamByName('IDSKL').AsInteger:=CommonSet.IdStore;
      quTO.Active:=True;
    end;
  end else
  begin
    fmSelPerSkl.quMHAll1.Active:=False;
    fmSelPerSkl.Release;
  end;
end;

procedure TfmTO.FormShow(Sender: TObject);
begin
  Memo1.Clear;
end;

procedure TfmTO.acCreateExecute(Sender: TObject);
Var iDateB,iDateE,IdSkl,iR,iD:INteger;
    rSumB,rPostIn,rPostOut,rVnIn,rVnOut,rInv,rQReal:Real;
    StrWk:String;
begin
  //������������ �� �� ������
  if not CanDo('prCreateTO') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  with dmO do
  with dmORep do
  begin
    // ������
    fmSelPerSkl:=tfmSelPerSkl.Create(Application);
    with fmSelPerSkl do
    begin
      cxDateEdit1.Date:=CommonSet.DateFrom;
      quMHAll1.Active:=False;
      quMHAll1.Active:=True;
      quMHAll1.First;
      cxLookupComboBox1.EditValue:=quMHAll1ID.AsInteger;
      if CommonSet.IdStore>0 then  cxLookupComboBox1.EditValue:=CommonSet.IdStore;
    end;
    fmSelPerSkl.ShowModal;
    if fmSelPerSkl.ModalResult=mrOk then
    begin
      iDateB:=Trunc(fmSelPerSkl.cxDateEdit1.Date);
      iDateE:=Trunc(fmSelPerSkl.cxDateEdit2.Date)+1;
      IdSkl:=fmSelPerSkl.cxLookupComboBox1.EditValue;

      WriteHistory('������������ �� � '+FormatDateTime('dd.mm.yyyy',iDateB)+' �� '+FormatDateTime('dd.mm.yyyy',iDateE)+'. ��������: '+Person.Name);

      if iDateE>=iMaxDate then  iDateE:=Trunc(Date+1);
      fmSelPerSkl.quMHAll1.Active:=False;
      fmSelPerSkl.Release;

      //�������
      Memo1.Clear;
      Memo1.Lines.Add('������ �������.  ������ � '+FormatDateTime('dd.mm.yyyy',iDateB)+' �� '+FormatDateTime('dd.mm.yyyy',iDateE)); Delay(10);

      for iD:=iDateB to iDateE do
      begin
        //������� �� ������
        Memo1.Lines.Add('  ������� �� '+FormatDateTime('dd.mm.yyyy',iD)); Delay(10);
        iR:=prTOFindRemB(iD,IdSkl,rSumB);

        if iR<>0 then
          if MessageDlg('������� �� ������ �� ���������. ���������� ������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then exit;

        Str(rSumB:12:2,StrWk);
        Memo1.Lines.Add('    ������� �� ������: '+StrWk); Delay(10);

        //�������� �� ��������
        iR:=prTOFind(iD,IdSkl);
        if iR<>0 then //���� ������� ���������
        begin
          StrWk:='    �� �� '+FormatDateTime('dd.mm.yyyy',iD)+' ��� ����. ������� ��� �� � '+FormatDateTime('dd.mm.yyyy',iD)+'?';
          if MessageDlg(StrWk,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
          begin
            if not CanDo('prDelTO') then
            begin
              Memo1.Lines.Add('��� ���� �� �������� ��. ������ �� ��������.'); Delay(10);
              exit;
            end;
            //��������
            Memo1.Lines.Add('    ���� �������� �� � '+FormatDateTime('dd.mm.yyyy',iD)); Delay(10);
            prTODel(iD,IdSkl);
            Memo1.Lines.Add('    �������� ��.'); Delay(10);
          end else
          begin
            Memo1.Lines.Add('������ �� ��������.'); Delay(10);
            exit;
          end;
        end;

        //������
        Memo1.Lines.Add('    ������.'); Delay(10);
        rPostIn:=RoundEx(prTOPostIn(iD,IdSkl)*100)/100;
        //������
        Memo1.Lines.Add('    �������.'); Delay(10);
        rPostOut:=RoundEx(prTOPostOut(iD,IdSkl)*100)/100;
        //�� ������
        Memo1.Lines.Add('    ��. ������.'); Delay(10);
        rVnIn:=RoundEx(prTOVnIn(iD,IdSkl)*100)/100;
        //�� ������
        Memo1.Lines.Add('    ��. ������.'); Delay(10);
        rVnOut:=RoundEx(prTOVnOut(iD,IdSkl)*100)/100;
        //��������������
        Memo1.Lines.Add('    ��������������.'); Delay(10);
        rInv:=RoundEx(prTOInv(iD,IdSkl)*100)/100;
        //����������
        Memo1.Lines.Add('    ������.'); Delay(10);
        rQReal:=RoundEx(prTOReal(iD,IdSkl)*100)/100;

        quTOSel.Active:=False;
        quTOSel.ParamByName('DATEB').AsInteger:=iD;
        quTOSel.ParamByName('DATEE').AsInteger:=iD+1;
        quTOSel.ParamByName('IDSKL').AsInteger:=IdSkl;
        quTOSel.Active:=True;

        quTOSel.Append;
        quTOSelIDATE.AsInteger:=iD;
        quTOSelIDSTORE.AsInteger:=IdSkl;
        quTOSelREMNIN.AsFloat:=rSumB;
        quTOSelPOSTIN.AsFloat:=rPostIn;
        quTOSelPOSTOUT.AsFloat:=rPostOut;
        quTOSelVNIN.AsFloat:=rVnIn;
        quTOSelVNOUT.AsFloat:=rVnOut;
        quTOSelINV.AsFloat:=rInv;
        quTOSelQREAL.AsFloat:=rQReal;
        quTOSelREMNOUT.AsFloat:=rSumB+rPostIn-rPostOut+rVnIn-rVnOut+rInv-rQReal;
        quTOSel.Post;

        quTOSel.Active:=False;

      end;
      Memo1.Lines.Add('������ ��������.'); Delay(10);

      quTO.Active:=False;
      quTO.Active:=True;
    end else
    begin
      fmSelPerSkl.quMHAll1.Active:=False;
      fmSelPerSkl.Release;
    end;
  end;
end;

procedure TfmTO.acPrintTOExecute(Sender: TObject);
Var rSum:Real;
    StrWk,StrWk1:String;
begin
  //������
  with dmO do
  with dmORep do
  begin
    Memo1.Clear;
    Memo1.Lines.Add('����� ���� ������������ ������ ��������� ������ (��).'); delay(10);
    taTORep.Active:=False;
    taTORep.CreateDataSet;

    rSum:=quTOREMNIN.AsFloat;

    //������
    taTOPostIn.Active:=False;
    taTOpostIn.ParamByName('DATEB').AsDate:=quTOIDATE.AsInteger;
    taTOpostIn.ParamByName('IDSKL').AsInteger:=quTOIDSTORE.AsInteger;
    taTOPostIn.Active:=True;

    taTOPostIn.First;
    while not taTOPostIn.Eof do
    begin
      taTORep.Append;
      taTORepsType.AsString:='������';
      taTORepsDocType.AsString:='������ �� �����������';
      taTORepsCliName.AsString:=taTOPostInNAMECL.AsString;
      taTORepsDate.AsString:=FormatDateTime('dd.mm.yyyy',taTOPostInDATEDOC.AsDateTime);
      taTORepsDocNum.AsString:=taTOPostInNUMDOC.AsString;
      taTOReprSum.AsFloat:=taTOPostInSUMIN.AsFloat;
      taTOReprSum1.AsFloat:=taTOPostInSUMIN.AsFloat;
      taTOReprSumO.AsFloat:=taTOPostInSUMUCH.AsFloat;
      taTOReprSumO1.AsFloat:=taTOPostInSUMUCH.AsFloat;
      taTORep.Post;

      rSum:=rSum+taTOPostInSUMIN.AsFloat;

      taTOPostIn.Next;
    end;
    taTOPostIn.Active:=False;

    //���������� ������
    taTOVnIn.Active:=False;
    taTOVnIn.ParamByName('DATEB').AsDate:=quTOIDATE.AsInteger;
    taTOVnIn.ParamByName('IDSKL').AsInteger:=quTOIDSTORE.AsInteger;
    taTOVnIn.Active:=True;

    taTOVnIn.First;
    while not taTOVnIn.Eof do
    begin
      taTORep.Append;
      taTORepsType.AsString:='������';
      taTORepsDocType.AsString:='���������� ������';
      taTORepsCliName.AsString:=taTOVnInNAMEMH.AsString;
      taTORepsDate.AsString:=FormatDateTime('dd.mm.yyyy',taTOVnInDATEDOC.AsDateTime);
      taTORepsDocNum.AsString:=taTOVnInNUMDOC.AsString;
      taTOReprSum.AsFloat:=taTOVnInSUMIN.AsFloat;
      taTOReprSum1.AsFloat:=taTOVnInSUMIN.AsFloat;
      taTOReprSumO.AsFloat:=taTOVnInSUMUCH1.AsFloat;
      taTOReprSumO1.AsFloat:=taTOVnInSUMUCH1.AsFloat;
      taTORep.Post;

      rSum:=rSum+taTOVnInSUMIN.AsFloat;

      taTOVnIn.Next;
    end;
    taTOVnIn.Active:=False;

    //��������������

    taTOInv.Active:=False;
    taTOInv.ParamByName('DATEB').AsDate:=quTOIDATE.AsInteger;
    taTOInv.ParamByName('IDSKL').AsInteger:=quTOIDSTORE.AsInteger;
    taTOInv.Active:=True;

    taTOInv.First;
    while not taTOInv.Eof do
    begin
      if (taTOInvSUM2.AsFloat-taTOInvSUM1.AsFloat)>0 then
      begin
        taTORep.Append;
        taTORepsType.AsString:='������';
        taTORepsDocType.AsString:='��������������';
        taTORepsCliName.AsString:='��������������  �';
        taTORepsDate.AsString:=FormatDateTime('dd.mm.yyyy',quTOIDATE.AsInteger);
        taTORepsDocNum.AsString:=taTOInvNUMDOC.AsString;
        taTOReprSum.AsFloat:=(taTOInvSUM2.AsFloat-taTOInvSUM1.AsFloat);
        taTOReprSum1.AsFloat:=(taTOInvSUM2.AsFloat-taTOInvSUM1.AsFloat);
        taTOReprSumO.AsFloat:=(taTOInvSUM21.AsFloat-taTOInvSUM11.AsFloat);
        taTOReprSumO1.AsFloat:=(taTOInvSUM21.AsFloat-taTOInvSUM11.AsFloat);
        taTORep.Post;

        rSum:=rSum+(taTOInvSUM2.AsFloat-taTOInvSUM1.AsFloat);
      end;
      if (taTOInvSUM2.AsFloat-taTOInvSUM1.AsFloat)<0 then
      begin
        taTORep.Append;
        taTORepsType.AsString:='������';
        taTORepsDocType.AsString:='��������������';
        taTORepsCliName.AsString:='��������������  �';
        taTORepsDate.AsString:=FormatDateTime('dd.mm.yyyy',quTOIDATE.AsInteger);
        taTORepsDocNum.AsString:=taTOInvNUMDOC.AsString;
        taTOReprSum.AsFloat:=(taTOInvSUM2.AsFloat-taTOInvSUM1.AsFloat)*(-1);
        taTOReprSum1.AsFloat:=(taTOInvSUM2.AsFloat-taTOInvSUM1.AsFloat)*(-1);
        taTOReprSumO.AsFloat:=(taTOInvSUM21.AsFloat-taTOInvSUM11.AsFloat)*(-1);
        taTOReprSumO1.AsFloat:=(taTOInvSUM21.AsFloat-taTOInvSUM11.AsFloat)*(-1);
        taTORep.Post;

        rSum:=rSum-(taTOInvSUM2.AsFloat-taTOInvSUM1.AsFloat);
      end;

      taTOInv.Next;
    end;
    taTOInv.Active:=False;

    //������ - �������
    taTOPostOut.Active:=False;
    taTOPostOut.ParamByName('DATEB').AsDate:=quTOIDATE.AsInteger;
    taTOPostOut.ParamByName('IDSKL').AsInteger:=quTOIDSTORE.AsInteger;
    taTOPostOut.Active:=True;

    taTOPostOut.First;
    while not taTOPostOut.Eof do
    begin
      taTORep.Append;
      taTORepsType.AsString:='������';
      taTORepsDocType.AsString:='�������� �����������';
      taTORepsCliName.AsString:=taTOPostOutNAMECL.AsString;
      taTORepsDate.AsString:=FormatDateTime('dd.mm.yyyy',taTOPostOutDATEDOC.AsDateTime);
      taTORepsDocNum.AsString:=taTOPostOutNUMDOC.AsString;
      taTOReprSum.AsFloat:=taTOPostOutSUMIN.AsFloat;
      taTOReprSum1.AsFloat:=taTOPostOutSUMIN.AsFloat;
      taTOReprSumO.AsFloat:=taTOPostOutSUMUCH.AsFloat;
      taTOReprSumO1.AsFloat:=taTOPostOutSUMUCH.AsFloat;
      taTORep.Post;

      rSum:=rSum-taTOPostOutSUMIN.AsFloat;

      taTOPostOut.Next;
    end;
    taTOPostOut.Active:=False;

    //���������� ������
    taTOVnOut.Active:=False;
    taTOVnOut.ParamByName('DATEB').AsDate:=quTOIDATE.AsInteger;
    taTOVnOut.ParamByName('IDSKL').AsInteger:=quTOIDSTORE.AsInteger;
    taTOVnOut.Active:=True;

    taTOVnOut.First;
    while not taTOVnOut.Eof do
    begin
      taTORep.Append;
      taTORepsType.AsString:='������';
      taTORepsDocType.AsString:='���������� ������';
      taTORepsCliName.AsString:=taTOVnOutNAMEMH.AsString;
      taTORepsDate.AsString:=FormatDateTime('dd.mm.yyyy',taTOVnOutDATEDOC.AsDateTime);
      taTORepsDocNum.AsString:=taTOVnOutNUMDOC.AsString;
      taTOReprSum.AsFloat:=taTOVnOutSUMIN.AsFloat;
      taTOReprSum1.AsFloat:=taTOVnOutSUMIN.AsFloat;
      taTOReprSumO.AsFloat:=taTOVnOutSUMUCH.AsFloat;
      taTOReprSumO1.AsFloat:=taTOVnOutSUMUCH.AsFloat;
      taTORep.Post;

      rSum:=rSum-taTOVnOutSUMIN.AsFloat;

      taTOVnOut.Next;
    end;
    taTOVnOut.Active:=False;


    //������ �� �����
    taTOOutB.Active:=False;
    taTOOutB.ParamByName('DATEB').AsDate:=quTOIDATE.AsInteger;
    taTOOutB.ParamByName('IDSKL').AsInteger:=quTOIDSTORE.AsInteger;
    taTOOutB.Active:=True;

    taTOOutB.First;
    while not taTOOutB.Eof do
    begin
      StrWk:=taTOOutBOPER.AsString;
      StrWk1:='�����';
      if taTOOutBOPER.AsString='Sale' then StrWk:='������� ��������';
      if taTOOutBOPER.AsString='SaleBank' then StrWk:='������� �� ���������� �����';
      if taTOOutBOPER.AsString='Ret' then StrWk:='������� ��������';
      if taTOOutBOPER.AsString='RetBank' then StrWk:='������� �� ���������� �����';

      if taTOOutBOPER.AsString='SalePC' then
      begin
        StrWk:='������� ��. ������';
        StrWk1:='��������';
      end;
      if taTOOutBOPER.AsString='Del' then
      begin
        StrWk:='�������� �� ���������';
        StrWk1:='��������';
      end;

      taTORep.Append;
      taTORepsType.AsString:='������';
      taTORepsDocType.AsString:=StrWk1;
      taTORepsCliName.AsString:=StrWk;
      taTORepsDate.AsString:=FormatDateTime('dd.mm.yyyy',taTOOutBDATEDOC.AsDateTime);
      taTORepsDocNum.AsString:=taTOOutBNUMDOC.AsString;
      taTOReprSum.AsFloat:=taTOOutBSUMIN.AsFloat;
      taTOReprSum1.AsFloat:=taTOOutBSUMIN.AsFloat;
      taTOReprSumO.AsFloat:=taTOOutBSUMUCH.AsFloat;
      taTOReprSumO1.AsFloat:=taTOOutBSUMUCH.AsFloat;
      taTORep.Post;

      rSum:=rSum-taTOPostOutSUMIN.AsFloat;

      taTOOutB.Next;
    end;
    taTOOutB.Active:=False;

    //������ �� ����� - ������
    taTOOutBCat.Active:=False;
    taTOOutBCat.ParamByName('DATEB').AsDate:=quTOIDATE.AsInteger;
    taTOOutBCat.ParamByName('ICAT').AsInteger:=2;
    taTOOutBCat.ParamByName('IDSKL').AsInteger:=quTOIDSTORE.AsInteger;
    taTOOutBCat.Active:=True;

    taTOOutCCat.Active:=False;
    taTOOutCCat.ParamByName('DATEB').AsDate:=quTOIDATE.AsInteger;
    taTOOutCCat.ParamByName('ICAT').AsInteger:=2;
    taTOOutCCat.ParamByName('IDSKL').AsInteger:=quTOIDSTORE.AsInteger;
    taTOOutCCat.Active:=True;

    if (taTOOutBCatRSUM.AsFloat<>0)or(taTOOutCCatRSUM.AsFloat<>0) then
    begin
      taTORep.Append;
      taTORepsType.AsString:='������';
      taTORepsDocType.AsString:='�����';
      taTORepsCliName.AsString:='���������� ����� � �.�.';
      taTORepsDate.AsString:='';
      taTORepsDocNum.AsString:='';
      taTOReprSum.AsFloat:=taTOOutCCatRSUM.AsFloat;
      taTOReprSum1.AsFloat:=0;
      taTOReprSumO.AsFloat:=taTOOutBCatRSUM.AsFloat;
      taTOReprSumO1.AsFloat:=0;
      taTORep.Post;
    end;
    taTOOutBCat.Active:=False;
    taTOOutCCat.Active:=False;

    //������ �� ����� - ������
    taTOOutBCat.Active:=False;
    taTOOutBCat.ParamByName('DATEB').AsDate:=quTOIDATE.AsInteger;
    taTOOutBCat.ParamByName('ICAT').AsInteger:=3;
    taTOOutBCat.ParamByName('IDSKL').AsInteger:=quTOIDSTORE.AsInteger;
    taTOOutBCat.Active:=True;

    taTOOutCCat.Active:=False;
    taTOOutCCat.ParamByName('DATEB').AsDate:=quTOIDATE.AsInteger;
    taTOOutCCat.ParamByName('ICAT').AsInteger:=3;
    taTOOutCCat.ParamByName('IDSKL').AsInteger:=quTOIDSTORE.AsInteger;
    taTOOutCCat.Active:=True;

    if (taTOOutBCatRSUM.AsFloat<>0)or(taTOOutCCatRSUM.AsFloat<>0) then
    begin
      taTORep.Append;
      taTORepsType.AsString:='������';
      taTORepsDocType.AsString:='�����';
      if taTOOutBCatRSUM.AsFloat>0 then taTORepsCliName.AsString:='������ (������) � �.�.'
                                   else taTORepsCliName.AsString:='������ (�������) � �.�.';
      taTORepsDate.AsString:='';
      taTORepsDocNum.AsString:='';
      taTOReprSum.AsFloat:=taTOOutCCatRSUM.AsFloat;
      taTOReprSum1.AsFloat:=0;
      taTOReprSumO.AsFloat:=taTOOutBCatRSUM.AsFloat;
      taTOReprSumO1.AsFloat:=0;
      taTORep.Post;
    end;
    taTOOutBCat.Active:=False;
    taTOOutCCat.Active:=False;

    Memo1.Lines.Add('������������ ��.'); delay(10);

    frRep1.LoadFromFile(CurDir + 'to.frf');

    frVariables.Variable['sPeriod']:=FormatDateTime('dd.mm.yyyy',quTOIDATE.AsInteger);
    frVariables.Variable['Depart']:=quTONAMEMH.AsString;
    frVariables.Variable['RemnB']:=quTOREMNIN.AsFloat;
    frVariables.Variable['RemnE']:=quTOREMNOUT.AsFloat;

    frRep1.ReportName:='�������� �����.';
    frRep1.PrepareReport;
    frRep1.ShowPreparedReport;

    taTORep.Active:=False;
  end;
end;

procedure TfmTO.acTODelPerExecute(Sender: TObject);
Var iDateB,IdSkl:INteger;
begin
  //������� �� ������
  if not CanDo('prDelTO') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  with dmO do
  with dmORep do
  begin
    // ������
    fmSelPerSkl:=tfmSelPerSkl.Create(Application);
    with fmSelPerSkl do
    begin
      cxDateEdit1.Date:=quTOIDATE.AsInteger;
      quMHAll1.Active:=False;
      quMHAll1.Active:=True;
      quMHAll1.First;
      cxLookupComboBox1.EditValue:=quTOIDSTORE.AsInteger;
    end;
    fmSelPerSkl.ShowModal;
    if fmSelPerSkl.ModalResult=mrOk then
    begin
      iDateB:=Trunc(fmSelPerSkl.cxDateEdit1.Date);
      IdSkl:=fmSelPerSkl.cxLookupComboBox1.EditValue;

      fmSelPerSkl.quMHAll1.Active:=False;
      fmSelPerSkl.Release;

      //�������
      Memo1.Clear;
      Memo1.Lines.Add('������ ��������.  ������ � '+FormatDateTime('dd.mm.yyyy',iDateB)+'.'); Delay(10);
      prTODel(iDateB,IdSkl);
      Memo1.Lines.Add('    �������� ��.'); Delay(10);

      quTO.Active:=False;
      quTO.Active:=True;
    end else
    begin
      fmSelPerSkl.quMHAll1.Active:=False;
      fmSelPerSkl.Release;
    end;
  end;
end;

end.
