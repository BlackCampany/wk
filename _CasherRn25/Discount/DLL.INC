{ stdcall - USE C/C++ Caller}

{ Here and down use GetLastDllError function if ERROR (NON ZERO)}

// Init DLL
Function OpenDLL(OpName,Psw,DevName:PChar; FlagOem:Integer): Integer; stdcall; far; external 'azimuth.dll' name 'OpenDLL';
Function OpenTCPDLL( OpName,Psw,ServerName:PChar; Port, FlagOem:Integer ): Integer; stdcall; far; external 'azimuth.dll' name 'OpenTCPDLL';
Function OpenDLLPlus( OpName,Psw,DevName,BaudRate:PChar; FlagOem:Integer ): Integer; stdcall; far; external 'azimuth.dll' name 'OpenDLLPlus';
// Close DLL
Function CloseDLL: Integer; stdcall; far; external 'azimuth.dll' name 'CloseDLL';
// Start seans function
Function StartSeans: Integer; stdcall; far; external 'azimuth.dll' name 'StartSeans';
// Shift open function
// Here and doun use GetLastDllError function if ERROR
Function ShiftOpen(Buf:PChar): Integer; stdcall; far; external 'azimuth.dll' name 'ShiftOpen';
Function ShiftOpenEx( _CurDate,_CurTime,Buf:PChar ): Integer; export; stdcall;far; external 'azimuth.dll' name 'ShiftOpenEx';

// Shif close function (Z-Report)
Function ShiftClose: Integer; stdcall; far; external 'azimuth.dll' name 'ShiftClose';
// Shif close function (Z-Report)
Function ShiftCloseEx: Integer; stdcall; far; external 'azimuth.dll' name 'ShiftCloseEx';
// X-Report
Function XReport: Integer; stdcall; far; external 'azimuth.dll' name 'XReport';
// From Cash function
Function FromCash(Sum:Integer): Integer; stdcall; far; external 'azimuth.dll' name 'FromCash';
// From Cash function
Function FromCashEx(Sum:PChar; FreeField : PChar): Integer; stdcall; far; external 'azimuth.dll' name 'FromCashEx';
// To Cash function
Function ToCash(Sum:Integer): Integer; stdcall; far; external 'azimuth.dll' name 'ToCash';
// To Cash function
Function ToCashEx(Sum:PChar; FreeField : PChar): Integer; stdcall; far; external 'azimuth.dll' name 'ToCashEx';
// From Cash function
Function FromCashPlus(_Sum:Integer; FreeField : PChar  ): Integer; stdcall; far; external 'azimuth.dll' name 'FromCashPlus';
// To Cash function
Function ToCashPlus(_Sum:Integer; FreeField : PChar  ): Integer; stdcall; far; external 'azimuth.dll' name 'ToCashPlus';

{ ------------ FISCAL DOCUMENT FUNCTIONS ---------------}

// Open fiscal document functions
Function OpenFiscalDoc(DocType,PayType,FlipFOffs,PageNum,HCopyNum,VCopyNum: Byte;
                       LOffs,VGap: Word ;LGap: Byte;Sum :Integer): Integer; stdcall; far; external 'azimuth.dll' name 'OpenFiscalDoc';
// Open fiscal document functions
Function OpenFiscalDocEx(DocType,PayType,FlipFOffs,PageNum,HCopyNum,VCopyNum: Byte;
                       LOffs,VGap: Word ;LGap: Byte;Sum :PChar): Integer; stdcall; far; external 'azimuth.dll' name 'OpenFiscalDocEx';

// Set fix fields positions
Function AddPosField(SerNoLine, SerNoCol :Word; SerNoFont :Byte;
                     DocNoLine, DocNoCol :Word; DocNoFont :Byte;
                     DateLine , DateCol  :Word; DateFont  :Byte;
                     TimeLine , TimeCol  :Word; TimeFont  :Byte;
                     InnLine  , InnCol   :Word; InnFont   :Byte;
                     OperLine , OperCol  :Word; OperFont  :Byte;
                     SumLine  , SumCol   :Word; SumFont   :Byte): Integer; stdcall; far; external 'azimuth.dll' name 'AddPosField';
// Add one free field
Function AddFreeField(Line,Col :Word;Font,PrintMode,JourNo :Byte;Info:PChar): Integer; stdcall; far; external 'azimuth.dll' name 'AddFreeField';
// Close Free Doc (Abort) if ERROR Print
Function CloseFreeDoc: Integer; stdcall; far; external 'azimuth.dll' name 'CloseFreeDoc';
// Print free doc as Slip
Function PrintFiscalSlip: Integer; stdcall; far; external 'azimuth.dll' name 'PrintFiscalSlip';
// Print free doc as Receipt
Function PrintFiscalReceipt: Integer; stdcall; far; external 'azimuth.dll' name 'PrintFiscalReceipt';
// Print electronic journal (if use)
Function PrintEjournal: Integer; stdcall; far; external 'azimuth.dll' name 'PrintEjournal';


// Open fiscal document functions
Function OpenFiscalDoc77(DocType,PayType,FlipFOffs,PageNum,HCopyNum,VCopyNum: Byte;
                       LOffs,VGap: Word ;LGap: Byte;Sum :Integer): Integer; stdcall; far; external 'azimuth.dll' name 'OpenFiscalDoc77';
// Open fiscal document functions
Function OpenFiscalDocEx77(DocType,PayType,FlipFOffs,PageNum,HCopyNum,VCopyNum: Byte;
                       LOffs,VGap: Word ;LGap: Byte;Sum :PChar): Integer; stdcall; far; external 'azimuth.dll' name 'OpenFiscalDocEx77';

// Set fix fields positions
Function AddPosField77(SerNoLine, SerNoCol :Word; SerNoFont :Byte;
                     DocNoLine, DocNoCol :Word; DocNoFont :Byte;
                     DateLine , DateCol  :Word; DateFont  :Byte;
                     TimeLine , TimeCol  :Word; TimeFont  :Byte;
                     InnLine  , InnCol   :Word; InnFont   :Byte;
                     OperationLine , OperationCol  :Word; OperationFont  :Byte;
                     KPKLine , KPKCol  :Word; KPKFont  :Byte;
                     OperLine , OperCol  :Word; OperFont  :Byte;
                     SumLine  , SumCol   :Word; SumFont   :Byte): Integer; stdcall; far; external 'azimuth.dll' name 'AddPosField77';
// Add one free field
Function AddFreeField77(Line,Col :Word;Font,PrintMode,JourNo :Byte;Info:PChar): Integer; stdcall; far; external 'azimuth.dll' name 'AddFreeField77';
// Close Free Doc (Abort) if ERROR Print
Function CloseFreeDoc77: Integer; stdcall; far; external 'azimuth.dll' name 'CloseFreeDoc77';
// Print free doc as Slip
Function PrintFiscalSlip77: Integer; stdcall; far; external 'azimuth.dll' name 'PrintFiscalSlip77';


// Get Receipt Numbers (use GetField Num = 5,6,7)
Function GetNumbers: Integer; stdcall; far; external 'azimuth.dll' name 'GetNumbers';
// Get Electron reportc(use GetField Num = 5,6,7 etc)
Function GetEReport (ReportNum, Param : Byte): Integer; stdcall; far; external 'azimuth.dll' name 'GetEReport';
// Get Resource information (use GetField Num = 5,6,7,8,9)
Function GetResource: Integer; stdcall; far; external 'azimuth.dll' name 'GetResource';
// Get Serial Number (use GetField Num = 5)
Function GetSerialNum: Integer; stdcall; far; external 'azimuth.dll' name 'GetSerialNum';
// Get Fiscal numbers (use GetField Num = 5,6,7,8,9)
Function GetFiscalNums: Integer; stdcall; far; external 'azimuth.dll' name 'GetFiscalNums';
// Set communication password for accsess
Function SetPassword(Psw:PChar): Integer; stdcall; far; external 'azimuth.dll' name 'SetPassword';
// Set date time
Function SetDate: Integer; stdcall; far; external 'azimuth.dll' name 'SetDate';
// Set date time
Function SetDateEx(CurDate,CurTime:PChar): Integer; export; stdcall; far; external 'azimuth.dll' name 'SetDateEx';
// Set prezenter functionality
Function SetPrezenter(_IsRetrak,_IsPezenter,_IsSet:Byte): Integer; export; stdcall; far; external 'azimuth.dll' name 'SetPrezenter';

// Get date time
Function GetDate: Integer; stdcall; far; external 'azimuth.dll' name 'GetDate';
//  Set Header Doc (4 lines)
Function SetHeader(H1,H2,H3,H4:PChar): Integer; stdcall; far; external 'azimuth.dll' name 'SetHeader';
//  Set NEW Header Doc (6 lines)
Function SetHeaderNew(H1,H2,H3,H4,H5,H6:PChar): Integer; stdcall; far; external 'azimuth.dll' name 'SetHeaderNew';
// Set Tail Doc
Function SetTail(T1,T2,T3,T4:PChar): Integer; stdcall; far; external 'azimuth.dll' name 'SetTail';
//Set new operation names
Function SetOperNames( N1,N2,N3,N4,N5,N6:PChar; NCmd:Integer ): Integer; stdcall; far; external 'azimuth.dll' name 'SetOperNames';
// Get electronic journal to HOST
Function GetMony: Integer; stdcall; far; external 'azimuth.dll' name 'GetMony';
// Change service type
Function ChangeService( ServiceType:Byte; OperatorPosition:PChar ): Integer; stdcall; far; external 'azimuth.dll' name 'ChangeService';

// Print free NOTFISCAL information
Function FreeDoc(Infomation:PChar;Len:DWord): Integer; stdcall; far; external 'azimuth.dll' name 'FreeDoc';
// Cut parer using FreeDoc function
Function FreeDocCut: Integer; stdcall; far; external 'azimuth.dll' name 'FreeDocCut';

// Open free NOTFISCAL doc
Function OpenFDoc: Integer; stdcall; far; external 'azimuth.dll' name 'OpenFDoc';
// Print free NOTFISCAL information (in Dos code)
Function PrintFDoc(Information:PChar;Len: DWord): Integer; stdcall; far; external 'azimuth.dll' name 'PrintFDoc';
// Print free NOTFISCAL information with OEM convert (Windows-Dos)
Function PrintOEMDoc(Information:PChar;Len: DWord): Integer; stdcall; far; external 'azimuth.dll' name 'PrintOEMDoc';
// Print free NOTFISCAL line with OEM convert (Windows-Dos) and CRLF
Function PrintOEMCRLFDoc(Information:PChar;Len: DWord): Integer; stdcall; far; external 'azimuth.dll' name 'PrintOEMCRLFDoc';
// FontSelect free NOTFISCAL doc
Function FontSelectFDoc(B:Byte): Integer; export; stdcall; far; external 'azimuth.dll' name 'FontSelectFDoc';
// SlipSelect free NOTFISCAL doc
Function SlipSelectDoc: Integer;stdcall; far; external 'azimuth.dll' name 'SlipSelectDoc';
// SlipEject free NOTFISCAL doc
Function SlipEjectDoc: Integer;stdcall; far; external 'azimuth.dll' name 'SlipEjectDoc';
// Barcode print free NOTFISCAL doc
Function PrintBarcodeFDoc(BType,BWith,BHight,HRIFont,HRIMode,BarCodeLen:Byte; BarCode:PChar): Integer; export; stdcall; far; external 'azimuth.dll' name 'PrintBarcodeFDoc';
// Close free NOTFISCAL doc
Function CloseFDoc: Integer;stdcall; far; external 'azimuth.dll' name 'CloseFDoc';
// Close free NOTFISCAL doc
Function CloseFDocEx(Information:PChar;Len: DWord): Integer;stdcall; far; external 'azimuth.dll' name 'CloseFDocEx';
// Close free NOTFISCAL doc whihout chec�ing previous state
Function CloseFDocPlus: Integer;stdcall; far; external 'azimuth.dll' name 'CloseFDocPlus';
// Select slip
Function SlipSelectFDoc: Integer; export; stdcall; far; external 'azimuth.dll' name 'SlipSelectFDoc';
// Eject slip
Function SlipEjectFDoc: Integer; export; stdcall; far; external 'azimuth.dll' name 'SlipEjectFDoc';
// Cut
Function CutFDoc: Integer; export; stdcall; far; external 'azimuth.dll' name 'CutFDoc';
// Print header (free NOTFISCAL doc)
Function PrintHFDoc(H1,H2,H3,H4,H5,H6:PChar; IsCut:Byte): Integer; export; stdcall; far; external 'azimuth.dll' name 'PrintHFDoc';

// Do ALL command
Function GRunCommand(CommandNum:Byte;CommandI:PChar): Integer; stdcall; far; external 'azimuth.dll' name 'GRunCommand';
// Create departments structure
Function AddDept (_DepNum:Integer; _DepName:PChar ): Integer; stdcall; far; external 'azimuth.dll' name 'AddDept';
// Destroy departments structure if ERROR SetDep
Function CloseDept: Integer; stdcall; far; external 'azimuth.dll' name 'CloseDept';
// Upload departments structure
Function SetDept: Integer; stdcall; far; external 'azimuth.dll' name 'SetDept';
// Download departments structure
Function GetDept: Integer; stdcall; far; external 'azimuth.dll' name 'GetDept';

// Create articles structure
Function AddArt( _ArtNum,_ArtFlag:Integer; _ArtName:PChar ): Integer; stdcall; far; external 'azimuth.dll' name 'AddArt';
// Destroy articles  structure if ERROR SetArt
Function CloseArt: Integer; stdcall; far; external 'azimuth.dll' name 'CloseArt';
// Upload articles  structure
Function SetArt(_DepNum:Integer): Integer; stdcall; far; external 'azimuth.dll' name 'SetArt';
// Download articles  structure
Function GetArt(_DepNum:Integer): Integer; stdcall; far; external 'azimuth.dll' name 'GetArt';
// Clear department structute
function ClearDept(DepNo:Byte): Integer; export; stdcall;far; external 'azimuth.dll' name 'ClearDept';
// Clear departments structute
function ClearAllDept : Integer; export; stdcall;far; external 'azimuth.dll' name 'ClearAllDept';

// Get last DLL Error after FALSE
Function GetLastDllError: DWord;stdcall; far; external 'azimuth.dll' name 'GetLastDllError';
// Get fields count
Function GetFldsCount: Integer;stdcall; far; external 'azimuth.dll' name 'GetFldsCount';
// Get field as Lint for mony functions
Function GetFldInt(Num:Byte): Integer;stdcall; far; external 'azimuth.dll' name 'GetFldInt';
// Get field as Float for mony functions
Function GetFldFloat(Num:Byte): Extended;stdcall; far; external 'azimuth.dll' name 'GetFldFloat';
// Get field as string for all functions
Function GetFldStr(Num:Byte;Field:PChar):PChar;stdcall; far; external 'azimuth.dll' name 'GetFldStr';
// Get field as Word for all functions
Function GetFldWord(Num:Byte): Integer;stdcall; far; external 'azimuth.dll' name 'GetFldWord';
 // Get field as string for all functions
Function GetFldByte(Num:Byte): Integer;stdcall; far; external 'azimuth.dll' name 'GetFldByte';
 // Get command as string for all functions
Function GetCommand(PtrCommand:PChar): PChar;stdcall; far; external 'azimuth.dll' name 'GetCommand';
// Get code of command
Function GetLastCommandNum: DWord;export; stdcall; far; external 'azimuth.dll' name 'GetLastComandNum';

 // Get answer as string for all functions
Function GetAnswer(PtrCommand:PChar): PChar;stdcall; far; external 'azimuth.dll' name 'GetAnswer';
 // Get size of answer  string for all functions
Function GetAnswerSize: Word;stdcall; far; external 'azimuth.dll' name 'GetAnswerSize';
// Change ID Char
Function SetIDChar(CharID:Char): Integer; export; stdcall; far; external 'azimuth.dll' name 'SetIDChar';
// Auto ID enable
Function SetAutoID(IsIDAuto:Byte): Integer; export; stdcall; far; external 'azimuth.dll' name 'SetAutoID';
// GetSerial Interface
Function GetParameters: Integer; export; stdcall; far; external 'azimuth.dll' name 'GetParameters';
 // Return Serial answer code
Function GetSerialAnswer: Integer;stdcall; far; external 'azimuth.dll' name 'GetSerialAnswer';
 // Get last answer -1 ANSWER ERROR, 0 NOT COMPLETE, 1 COMPLETE
Function GetLastAnswer: Integer;stdcall; far; external 'azimuth.dll' name 'GetLastAnswer';
// Get version
Function GetDllVer(DllVer:PChar): PChar;stdcall; far; external 'azimuth.dll' name 'GetDllVer';
// Get FW version
Function GetFWVer(FWVer:PChar): PChar;stdcall; far; external 'azimuth.dll' name 'GetFWVer';
// Set doc parameters
Function SetParamDoc(ParamDoc1,ParamDoc2,TimeoutSlip:Word): Integer; stdcall; far; external 'azimuth.dll' name 'SetParamDoc';
// Get doc parameters
Function GetParamDoc: Integer; stdcall; far; external 'azimuth.dll' name 'GetParamDoc';
// Set Answer timeout sec
Function SetCommTimeout( WaitRXTime,WaitTXTime:Integer): Integer; stdcall; far; external 'azimuth.dll' name 'SetCommTimeout';
// Set Answer timeout msec
Function SetCommTimeoutMs( WaitRXTime,WaitTXTime:Integer): Integer; stdcall; far; external 'azimuth.dll' name 'SetCommTimeoutMs';
// Cash Driver OPEN
Function CashDriverOpen:Integer; stdcall; far; external 'azimuth.dll' name 'CashDriverOpen';
// Set Cash Driver pulses (On and Off)
Function SetDrawerParam(_OnTime,_OffTime:Byte; _ParamDoc1:Word): Integer; export; stdcall; far; external 'azimuth.dll' name 'SetDrawerParam';
// Get driver parameters
Function GetDrawerParam: Integer; export; stdcall; far; external 'azimuth.dll' name 'GetDrawerParam';
// Show Display Info
Function ShowDisplay(Info:PChar;Len:Integer):Integer; stdcall; far; external 'azimuth.dll' name 'ShowDisplay';
// Init Display
Function InitDisplay:Integer; stdcall; far; external 'azimuth.dll' name 'InitDisplay';

// Set Taxes
Function SetTaxes( TIndex,TType:Byte; TName,TValue,TMin:PChar ):Integer; stdcall; far; external 'azimuth.dll' name 'SetTaxes';
// Get Taxes
Function GetTaxes( TIndex:Byte ):Integer; stdcall; far; external 'azimuth.dll' name 'GetTaxes';
// Get Taxes
Function GetTaxesPlus( TIndex:Byte; FreeField:PChar ):Integer; stdcall; far; external 'azimuth.dll' name 'GetTaxesPlus';
// Graph logo
Function DownloadGraphHeader(FName:PChar) :Integer; stdcall; far; external 'azimuth.dll' name 'DownloadGraphHeader';

//******************* AS 71-73 with departments
Function OpenFiscalDocPlus( _DocType,_FlipFOffs,_PageNum,_HCopyNum,_VCopyNum: Byte;
                   _LOffs,_VGap: Word ;
                   _LGap,_DepartNum,_ArticlesNum: Byte;
                   _Sum :Integer ): Integer;  stdcall; far; external 'azimuth.dll' name 'OpenFiscalDocPlus';

//******************* AS 71-73 with departments
Function OpenFiscalDocPlusEx( _DocType,_FlipFOffs,_PageNum,_HCopyNum,_VCopyNum: Byte;
                   _LOffs,_VGap: Word ;
                   _LGap,_DepartNum,_ArticlesNum: Byte;
                   _Sum :PChar ): Integer;  stdcall; far; external 'azimuth.dll' name 'OpenFiscalDocPlusEx';

Function AddPosFieldPlus( _SerNoLine,  _SerNoCol  :Word; _SerNoFont  :Byte;
                          _DocNoLine,  _DocNoCol  :Word; _DocNoFont  :Byte;
                          _OperNoLine, _OperNoCol :Word; _OperNoFont :Byte;
                          _DateLine ,  _DateCol   :Word; _DateFont   :Byte;
                          _TimeLine ,  _TimeCol   :Word; _TimeFont   :Byte;
                          _InnLine  ,  _InnCol    :Word; _InnFont    :Byte;
                          _OperLine ,  _OperCol   :Word; _OperFont   :Byte;
                          _DepLine  ,  _DepCol    :Word; _DepFont    :Byte;
                          _ArtLine  ,  _ArtCol    :Word; _ArtFont    :Byte;
                          _SumLine  ,  _SumCol    :Word; _SumFont    :Byte ): Integer;  stdcall; far; external 'azimuth.dll' name 'AddPosFieldPlus';

// first part of AddPosFieldPlus function (for V Fox Pro)
Function AddPosFieldPlus1( _SerNoLine,  _SerNoCol  :Word; _SerNoFont  :Byte;
                           _DocNoLine,  _DocNoCol  :Word; _DocNoFont  :Byte;
                           _OperNoLine, _OperNoCol :Word; _OperNoFont :Byte;
                           _DateLine ,  _DateCol   :Word; _DateFont   :Byte;
                           _TimeLine ,  _TimeCol   :Word; _TimeFont   :Byte ): Integer;  stdcall; far; external 'azimuth.dll' name 'AddPosFieldPlus1';

// second part of AddPosFieldPlus function (for V Fox Pro)
Function AddPosFieldPlus2( _InnLine  ,  _InnCol    :Word; _InnFont    :Byte;
                           _OperLine ,  _OperCol   :Word; _OperFont   :Byte;
                           _DepLine  ,  _DepCol    :Word; _DepFont    :Byte;
                           _ArtLine  ,  _ArtCol    :Word; _ArtFont    :Byte;
                           _SumLine  ,  _SumCol    :Word; _SumFont    :Byte ): Integer;  stdcall; far; external 'azimuth.dll' name 'AddPosFieldPlus2';

Function AddFreeFieldPlus( _Line,_Col :Word;
                           _Font,_PrintMode,_JourNo :Byte;
                           _Info:PChar): Integer;  stdcall; far; external 'azimuth.dll' name 'AddFreeFieldPlus';
Function AddPayFieldPlus( _Line,_Col :Word;
                          _Font,_PayMode:Byte;
                          _Sum:Integer ): Integer;  stdcall; far; external 'azimuth.dll' name 'AddPayFieldPlus';

Function AddPayFieldPlusEx( _Line,_Col :Word;
                          _Font,_PayMode:Byte;
                          _Sum:PChar ): Integer;  stdcall; far; external 'azimuth.dll' name 'AddPayFieldPlusEx';

Function AddSubDepFieldPlus( _SubDepNum:Byte; _Sum:Integer): Integer;  stdcall; far; external 'azimuth.dll' name 'AddSubDepFieldPlus';

Function AddSubDepFieldPlusex( _SubDepNum:Byte; _Sum:PChar): Integer;  stdcall; far; external 'azimuth.dll' name 'AddSubDepFieldPlusEx';

Function CloseFreeDocPlus: Integer;  stdcall; far; external 'azimuth.dll' name 'CloseFreeDocPlus';
Function PrintFiscalSlipPlus: Integer;  stdcall; far; external 'azimuth.dll' name 'PrintFiscalSlipPlus';
Function PrintFiscalReceiptPlus: Integer;  stdcall; far; external 'azimuth.dll' name 'PrintFiscalReceiptPlus';

Function StartReceipt( _DocType, _Copies : Byte;
                       _TableNo,
                       _PlaceNo,
                       _AccountNo : PChar ):Integer;  stdcall; far; external 'azimuth.dll' name 'StartReceipt';

Function StartReceiptPlus( _DocType, _Copies : Byte;
                       _TableNo,
                       _PlaceNo,
                       _AccountNo, FreeField : PChar ):Integer;  stdcall; far; external 'azimuth.dll' name 'StartReceiptPlus';

Function CommentReceipt( Buf:PChar ): Integer; export; stdcall; far; external 'azimuth.dll' name 'CommentReceipt';
// Price = 100 = 1 rub (.xx)
// Count = 1000 = 1 (.xxx)
Function ItemReceipt( _WareName,
                      _WareCode,
                      _Measure,
                      _SecID    : PChar;
                      _Price,
                      _Count    : Integer;
                      _WareType : Byte ):Integer;  stdcall; far; external 'azimuth.dll' name 'ItemReceipt';

Function ItemReceiptPlus( _WareName,
                      _WareCode,
                      _Measure,
                      _SecID, FreeField : PChar;
                      _Price,
                      _Count    : Integer;
                      _WareType : Byte ):Integer;  stdcall; far; external 'azimuth.dll' name 'ItemReceiptPlus';

Function ItemReceiptEx( _WareName,
                      _WareCode,
                      _Measure,
                      _SecID, FreeField : PChar;
                      _Price    : PChar;
                      _Count    : Integer;
                      _WareType : Byte ):Integer;  stdcall; far; external 'azimuth.dll' name 'ItemReceiptEx';

Function ItemReceiptExx( _WareName,
                      _WareCode,
                      _Measure,
                      _SecID, FreeField : PChar;
                      _Price,
                      _Count    : PChar;
                      _WareType : Byte ):Integer;  stdcall; far; external 'azimuth.dll' name 'ItemReceiptExx';

Function ItemDepReceipt( _SecID,
                         _WareName  : Byte;
                         _Price,
                         _Count    : Integer;
                         _Measure,
                         _WareCode : PChar ):Integer;  stdcall; far; external 'azimuth.dll' name 'ItemDepReceipt';

Function ItemDepReceiptPlus( _SecID,
                         _WareName  : Byte;
                         _Price,
                         _Count    : Integer;
                         _Measure,
                         _WareCode,
                         FreeField  : PChar ):Integer;  stdcall; far; external 'azimuth.dll' name 'ItemDepReceiptPlus';

Function ItemDepReceiptEx( _SecID,
                           _WareName  : Byte;
                           _Price     : PChar;
                           _Count     : Integer;
                           _Measure,
                           _WareCode,
                           FreeField  : PChar ):Integer;  stdcall; far; external 'azimuth.dll' name 'ItemDepReceiptEx';

Function ItemDepReceiptExx( _SecID,
                           _WareName  : Byte;
                           _Price,
                           _Count     : PChar;
                           _Measure,
                           _WareCode,
                           FreeField  : PChar ):Integer;  stdcall; far; external 'azimuth.dll' name 'ItemDepReceiptExx';

Function TotalReceipt:Integer;  stdcall; far; external 'azimuth.dll' name 'TotalReceipt';
Function TotalReceiptPlus(FreeField : PChar):Integer;  stdcall; far; external 'azimuth.dll' name 'TotalReceiptPlus';

Function SubTotalReceipt:Integer;  stdcall; far; external 'azimuth.dll' name 'SubTotalReceipt';
Function SubTotalReceiptPlus(FreeField : PChar):Integer;  stdcall; far; external 'azimuth.dll' name 'SubTotalReceiptPlus';

Function TenderReceipt(_PayType:Byte; _TenderSum:Integer; _CardName:PChar):Integer;  stdcall; far; external 'azimuth.dll' name 'TenderReceipt';
Function TenderReceiptPlus(_PayType:Byte; _TenderSum:Integer; _CardName,FreeField:PChar):Integer;  stdcall; far; external 'azimuth.dll' name 'TenderReceiptPlus';
Function TenderReceiptEx(_PayType:Byte; _TenderSum:PChar; _CardName,FreeField:PChar):Integer;  stdcall; far; external 'azimuth.dll' name 'TenderReceiptEx';
// Sum = 100 = 1 rub (.xx)
// Percent = 1000 = 1% (.xxx)
Function ComissionReceipt(_OType:Byte; _Percent,_Sum:Integer):Integer; stdcall; far; external 'azimuth.dll' name 'ComissionReceipt';
Function ComissionReceiptPlus(_OType:Byte; _Percent,_Sum:Integer;FreeField:PChar):Integer; stdcall; far; external 'azimuth.dll' name 'ComissionReceiptPlus';
Function ComissionReceiptEx(_OType:Byte; _Percent:Integer; _Sum:PChar;FreeField:PChar):Integer; stdcall; far; external 'azimuth.dll' name 'ComissionReceiptEx';
Function ComissionReceiptExx(_OType:Byte; _Percent, _Sum, FreeField:PChar):Integer; export; stdcall;far; external 'azimuth.dll' name 'ComissionReceiptExx';

Function CloseReceipt:Integer;  stdcall; far; external 'azimuth.dll' name 'CloseReceipt';
Function CancelReceipt:Integer;  stdcall; far; external 'azimuth.dll' name 'CancelReceipt';
Function BarcodeReceipt( _BarcodeType,_HRI,_Font,_Height,_Width:Byte; _Barcode:PChar ):Integer;  stdcall; far; external 'azimuth.dll' name 'BarcodeReceipt';
Function TaxReceipt(_TaxIndex:Byte):Integer;  stdcall; far; external 'azimuth.dll' name 'TaxReceipt';
Function TaxReceiptPlus(_TaxIndex:Byte; FreeField:PChar):Integer;  stdcall; far; external 'azimuth.dll' name 'TaxReceiptPlus';

// Change Operator Name
Function ChangeOpName( OpName:PChar ) :Integer; stdcall; far; external 'azimuth.dll' name 'ChangeOpName';

Function DllComWrite(P:PChar;Count:Integer):Integer;  stdcall; far; external 'azimuth.dll' name 'DllComWrite';
Function DllComWritePlus(P:PChar;Count:Integer):Integer;  stdcall; far; external 'azimuth.dll' name 'DllComWritePlus';
Function WriteComm(P:PChar;Count:Integer):Integer;  stdcall; far; external 'azimuth.dll' name 'WriteComm';
Function ReadComm(P:PChar;Count:Integer):Integer;  stdcall; far; external 'azimuth.dll' name 'ReadComm';
Function GetCommStatus( var cbRX,cbTX:Integer): Integer;export; stdcall; far; external 'azimuth.dll' name 'GetCommStatus';
Function GetFiscalInfo:Integer;  stdcall; far; external 'azimuth.dll' name 'GetFiscalInfo';
Function GetErrorMessage(P:PChar): PChar; stdcall; far; external 'azimuth.dll' name 'GetErrorMessage';
Function GetErrorMessageNo(P:PChar;ErrNo:DWord): PChar; stdcall; far; external 'azimuth.dll' name 'GetErrorMessageNo';
Function SetCommParam(DCB:PChar): Integer;stdcall; far; external 'azimuth.dll' name 'SetCommParam';
Function GetStatus : Integer;stdcall; far; external 'azimuth.dll' name 'GetStatus';
Function GetStatusPlus : Integer;stdcall; far; external 'azimuth.dll' name 'GetStatusPlus';
Function GetStatusNo(No:Byte) : Integer;stdcall; far; external 'azimuth.dll' name 'GetStatusNo';
Function GetStatusNoPlus(No:Byte) : Integer;stdcall; far; external 'azimuth.dll' name 'GetStatusNoPlus';

// NUM [1..6] BitNum[0..7] GetStatusNum = 0 - error
Function GetStatusNum(Num : Integer) : Integer;stdcall; far; external 'azimuth.dll' name 'GetStatusNum';
// CheckStatusNum = -1 - error
Function CheckStatusNum(Num,BitNum : Integer)  : Integer;stdcall; far; external 'azimuth.dll' name 'CheckStatusNum';

Function EKLActivization : Integer;stdcall; far; external 'azimuth.dll' name 'EKLActivization';
Function EKLClose: Integer;stdcall; far; external 'azimuth.dll' name 'EKLClose';
Function EKLActivizationReport : Integer;stdcall; far; external 'azimuth.dll' name 'EKLActivizationReport';
Function EKLEJournalTotal(EJournalNum:Integer) : Integer;stdcall; far; external 'azimuth.dll' name 'EKLEJournalTotal';
Function EKLKPKReport(KPKNum:PChar) : Integer;stdcall; far; external 'azimuth.dll' name 'EKLKPKReport';
Function EKLEJournalReport(EJournalNum:Integer): Integer;stdcall; far; external 'azimuth.dll' name 'EKLEJournalReport';
Function EKLShiftsNumReport(ReportType,_StartNo,_EndNo:Integer ) : Integer;stdcall; far; external 'azimuth.dll' name 'EKLShiftsNumReport';
Function EKLShiftsDateReport(ReportType:Integer;_StartDate,_EndDate:PChar): Integer;stdcall; far; external 'azimuth.dll' name 'EKLShiftsDateReport';
Function ChangeBaudrate( BaudRate:PChar ): Integer;stdcall; far; external 'azimuth.dll' name 'ChangeBaudrate';
Function SetPayment( _Index: Byte;
                     _PName:PChar;
                     _IsSecondLine,_IsChange:Integer;
                     _CurrencyIndex,_PermOperation : Byte;
                     _CrossCourse:PChar): Integer;stdcall; far; external 'azimuth.dll' name 'SetPayment';
Function GetPayment(PaymentNum:Byte): Integer;stdcall; far; external 'azimuth.dll' name 'GetPayment';
Function WriteCMOS( _Offs : Byte; _Info : PChar): Integer;stdcall; far; external 'azimuth.dll' name 'WriteCMOS';
Function ReadCMOS(_Offs,_Num  : Byte): Integer;stdcall; far; external 'azimuth.dll' name 'ReadCMOS';
Function ExceptToLog(Msg:PChar): Integer;stdcall; far; external 'azimuth.dll' name 'ExceptToLog';
Function StartMonitor: Integer; stdcall; far; external 'azimuth.dll' name 'StartMonitor';
Function TerminateMonitor: Integer; stdcall; far; external 'azimuth.dll' name 'TerminateMonitor';
Function DownLoadHex(MemoryChar:Char;StartL, CountO:Word; BufferP:PByteArray):Boolean;stdcall; far; external 'azimuth.dll' name 'DownLoadHex';
Function FreeDocCutPlus(Count: Byte):Integer; stdcall; far; external 'azimuth.dll' name 'FreeDocCutPlus';

Procedure LibEnable(IsEnable: Byte); stdcall; far; external 'azimuth.dll' name 'LibEnable';
Procedure LogEnable(IsEnable: Byte); stdcall; far; external 'azimuth.dll' name 'LogEnable';
Procedure CashDriverEnable(IsEnable:Byte); export; stdcall;far; external 'azimuth.dll' name 'CashDriverEnable';

Function EJPrint: Integer; export; stdcall;far; external 'azimuth.dll' name 'EJPrint';
Function EJPrintEx(ShiftNum:Word): Integer; export; stdcall;far; external 'azimuth.dll' name 'EJPrintEx';
Function EJErase: Integer; export; stdcall;far; external 'azimuth.dll' name 'EJErase';
Function EJTimeReport(_StartTime,_EndTime:PChar): Integer; export; stdcall;far; external 'azimuth.dll' name 'EJTimeReport';
Function EJDateTimeReport(_StartTime,_EndTime, _StartDate:PChar): Integer; export; stdcall;far; external 'azimuth.dll' name 'EJDateTimeReport';
Function EJNoReport(_StartNo,_EndNo:Word): Integer; export; stdcall;far; external 'azimuth.dll' name 'EJNoReport';
Function EJNoDoc(DocNo:Word): Integer; export; stdcall;far; external 'azimuth.dll' name 'EJNoDoc';
Function EJNoReportEx(_StartNo,_EndNo:Word; _OverCount:Byte): Integer; export; stdcall;far; external 'azimuth.dll' name 'EJNoReportEx';
Function EJNoDocEx(DocNo:Word; _OverCount:Byte): Integer; export; stdcall;far; external 'azimuth.dll' name 'EJNoDocEx';

Function GetEJParam: Integer; export; stdcall;far; external 'azimuth.dll' name 'GetEJParam';

Function Fiscalization( _OldPass, _NewPass, _NewRegNo, _NewCode:PChar; _Group, _TotalInFlash : Byte ): Integer; export; stdcall;far; external 'azimuth.dll' name 'Fiscalization';
Function GetDateReport(ReportType:Byte; _Pass, _StartDate, _EndDate : PChar ): Integer; export; stdcall;far; external 'azimuth.dll' name 'GetDateReport';
Function GetNumReport( ReportType:Byte; _Pass: PChar; _StartNo, _EndNo : Word ): Integer; export; stdcall;far; external 'azimuth.dll' name 'GetNumReport';

Function SetInterfaceParam( _sBaudRate:PChar; _sIs5Wires,_sIsDateTime:Byte ):Integer; export; stdcall;far; external 'azimuth.dll' name 'SetInterfaceParam';

Function StartReceiptNF :Integer; export; stdcall;far; external 'azimuth.dll' name 'StartReceiptNF';
Function CloseReceiptNF :Integer; export; stdcall;far; external 'azimuth.dll' name 'CloseReceiptNF';
Function LineReceiptNF(Line: PChar) :Integer; export; stdcall;far; external 'azimuth.dll' name 'LineReceiptNF';
Function LinesReceiptNF(Line1,Line2,Line3,Line4,Line5,Line6,Line7,Line8,Line9,Line10: PChar):Integer; export; stdcall;far; external 'azimuth.dll' name 'LinesReceiptNF';

Function GetCounters:Integer; export; stdcall;far; external 'azimuth.dll' name 'GetCounters';
Function GetLastDoc(KPKNum:Pchar):Integer; export; stdcall;far; external 'azimuth.dll' name 'GetLastDoc';
Function GetLastKPK:Integer; export; stdcall;far; external 'azimuth.dll' name 'GetLastKPK';

procedure SetTCPDelay(DelayTCP:Integer);  export; stdcall;far; external 'azimuth.dll' name 'SetTCPDelay';
function GetTCPDelay:Integer;  export; stdcall;far; external 'azimuth.dll' name 'GetTCPDelay';

Function Sertification( Serial : PChar):Integer; export; stdcall; far; external 'azimuth.dll' name 'Sertification';
