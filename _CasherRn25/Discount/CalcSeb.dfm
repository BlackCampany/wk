object fmCalcSeb: TfmCalcSeb
  Left = 337
  Top = 227
  Width = 546
  Height = 476
  Caption = #1057#1077#1073#1077#1089#1090#1086#1080#1084#1086#1089#1090#1100
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 538
    Height = 113
    Align = alTop
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 0
    object Label2: TLabel
      Left = 24
      Top = 8
      Width = 33
      Height = 13
      Caption = #1041#1083#1102#1076#1086
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
      Transparent = True
    end
    object Label3: TLabel
      Left = 24
      Top = 32
      Width = 74
      Height = 13
      Caption = #1041#1088#1091#1090#1090#1086' '#1085#1072' '#1076#1072#1090#1091
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
      Transparent = True
    end
    object Label4: TLabel
      Left = 112
      Top = 8
      Width = 161
      Height = 13
      AutoSize = False
      Caption = 'Label4'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label1: TLabel
      Left = 112
      Top = 32
      Width = 39
      Height = 13
      Caption = 'Label1'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object cxButton2: TcxButton
      Left = 432
      Top = 48
      Width = 83
      Height = 25
      Caption = #1042#1099#1093#1086#1076
      TabOrder = 0
      OnClick = cxButton2Click
      Colors.Default = 16776176
      Colors.Normal = 16776176
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton1: TcxButton
      Left = 432
      Top = 8
      Width = 83
      Height = 25
      Caption = #1056#1072#1089#1095#1080#1090#1072#1090#1100
      TabOrder = 1
      OnClick = cxButton1Click
      Colors.Default = 16776176
      Colors.Normal = 16776176
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton3: TcxButton
      Left = 24
      Top = 64
      Width = 65
      Height = 41
      TabOrder = 2
      OnClick = cxButton3Click
      Glyph.Data = {
        56080000424D560800000000000036000000280000001A0000001A0000000100
        18000000000020080000C40E0000C40E00000000000000000000C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0004000004000004000004000C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C00000C0C0C0004000
        80808080808080808080808080808080808080808000400040E02040E020FFFF
        FFC8D0D4A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0808080C0
        C0C0C0C0C0C0C0C00000C0C0C0C0C0C000400040E02000C02000C02000C02000
        C02000400040E02040E020FFFFFF004000C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4A4A0A0A4A0A0404040C0C0C0C0C0C00000C0C0C0C0C0C0
        C0C0C000400000C02000C02000C02000400000C02040E020FFFFFF00400000C0
        20C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4A4A0A0A4A0A040
        2020808080C0C0C00000C0C0C0C0C0C0C0C0C0A4A0A000400000C02000400000
        C02040E020FFFFFF004000004000004000C8D0D4A4A0A0A4A0A0A4A0A0A4A0A0
        A4A0A0A4A0A0808080C8D0D4808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C000400040E02040E020FFFFFF004000F0FBFFF0FBFFF0FB
        FFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFA4A0A0C8D0D4C8D0D440
        2020808080C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C000400040E02040E020FF
        FFFF004000808080004000C8D0D4C0DCC0C0DCC0C0DCC0C0DCC0C0DCC0C0DCC0
        C0DCC0C8D0D4F0FBFFA4A0A0C8D0D4402020808080C0C0C00000C0C0C0C0C0C0
        C0C0C000400000C02040E020FFFFFF00400080E0A000C020808080004000FFFF
        FFFFFFFFFFFFFFF0FBFFC0DCC0C0DCC0F0FBFFC0DCC0F0FBFF808080C8D0D440
        2020808080C0C0C00000C0C0C0C0C0C000400000C02040E020FFFFFF004000FF
        FFFF00400000C02000C020808080004000FFFFFFFFFFFFC8D0D4F0FBFFC0DCC0
        C0DCC0C0DCC0F0FBFFC0DCC0A4A0A0404020808080C0C0C00000C0C0C0004000
        004000004000004000FFFFFFFFFFFFFFFFFFFFFFFF0040000040000040000040
        00C8D0D4F0FBFFC0DCC0C8D0D4C8D0D4F0FBFFC0DCC0F0FBFFFFFFFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFC0DCC0FFFFFFF0FBFFFFFFFFFFFFFFF0FBFFF0FBFFF0FBFF
        FFFFFFC0DCC0F0FBFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0F0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FB
        FFF0FBFFFFFFFFFFFFFFF0FBFFF0FBFFFFFFFFC0DCC0F0FBFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0F0
        FBFFF0FBFFF0FBFFC0DCC0C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4F0FBFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0C0DCC0C0DCC0F0FBFFF0FBFFC0DCC0FFFF
        FFC0DCC0F0FBFFC0DCC0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0C0
        DCC0F0FBFFF0FBFFF0FBFFF0FBFFFFFFFFF0FBFFF0FBFFC0DCC0F0FBFFFFFFFF
        FFFFFFFFFFFFFFFFFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0C0DCC0C8D0D4F0FBFFF0FBFFC8D0D4FFFF
        FFC8D0D4F0FBFFC0DCC0F0FBFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0F0
        FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFC8D0D4F0FBFFFFFFFF
        FFFFFFFFFFFFFFFFFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFF0FBFFC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4C8D0D4C8D0D4C8D0D4F0FBFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFF0FBFFF0
        FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080C0DCC0C8D0D4C8D0D4F0FBFFC8D0D4C0DCC0C0DCC0C8D0D4F0FB
        FFC8D0D4C0DCC0F0FBFFC8D0D4F0FBFFC8D0D4C8D0D4F0FBFFA4A0A080808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C080808080E0E040E0E040E0E080
        E0E040E0E080E0E080E0E040E0E080E0E040E0E040E0E080E0E040E0E080E0E0
        40E0E040E0E080E0E000E0E0408080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C080808080E0E080E0E0C0DCC080E0E0C0DCC080E0E080E0E0C0DCC080E0
        E0C0DCC080E0E080E0E0C0DCC080E0E0C0DCC080E0E080E0E0C0DCC000808000
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080F0FBFFF0FBFFF0FBFF80
        E0E0F0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFF
        F0FBFFF0FBFFF0FBFFF0FBFF00C0C0002040808080C0C0C00000C0C0C0C0C0C0
        C0C0C08080808080808080808080808080808080808080808080808080808080
        8080808080808080808080808080808080808080808080808080808080808000
        2040C0C0C0C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C00000}
      LookAndFeel.Kind = lfOffice11
    end
    object cxRadioButton1: TcxRadioButton
      Left = 304
      Top = 16
      Width = 113
      Height = 17
      Caption = #1042' '#1094#1077#1085#1072#1093' '#1055'.'#1055'.'
      Checked = True
      TabOrder = 3
      TabStop = True
    end
    object cxRadioButton2: TcxRadioButton
      Left = 304
      Top = 40
      Width = 113
      Height = 17
      Caption = #1055#1086' '#1089#1077#1073'-'#1090#1080' (FiFo)'
      TabOrder = 4
    end
  end
  object GrSeb: TcxGrid
    Left = 11
    Top = 128
    Width = 510
    Height = 217
    TabOrder = 1
    LookAndFeel.Kind = lfOffice11
    object ViewSeb: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dsSeb
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SUM1'
          Column = ViewSebSUM1
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnGrouping = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      object ViewSebIDCARD: TcxGridDBColumn
        Caption = #1050#1086#1076
        DataBinding.FieldName = 'IDCARD'
        Width = 38
      end
      object ViewSebNAME: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'NAME'
        Width = 154
      end
      object ViewSebNAMESHORT: TcxGridDBColumn
        Caption = #1045#1076'.'#1080#1079#1084'.'
        DataBinding.FieldName = 'NAMESHORT'
        Width = 49
      end
      object ViewSebCURMESSURE: TcxGridDBColumn
        Caption = #1045#1076'.'#1080#1079#1084'.'
        DataBinding.FieldName = 'CURMESSURE'
        Visible = False
      end
      object ViewSebNETTO: TcxGridDBColumn
        Caption = #1053#1077#1090#1090#1086
        DataBinding.FieldName = 'NETTO'
      end
      object ViewSebBRUTTO: TcxGridDBColumn
        Caption = #1041#1088#1091#1090#1090#1086
        DataBinding.FieldName = 'BRUTTO'
      end
      object ViewSebKOEF: TcxGridDBColumn
        DataBinding.FieldName = 'KOEF'
        Visible = False
      end
      object ViewSebKNB: TcxGridDBColumn
        DataBinding.FieldName = 'KNB'
        Visible = False
      end
      object ViewSebPRICE1: TcxGridDBColumn
        Caption = #1062#1077#1085#1072' '
        DataBinding.FieldName = 'PRICE1'
      end
      object ViewSebSUM1: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '
        DataBinding.FieldName = 'SUM1'
        Styles.Content = dmO.cxStyle25
        Width = 63
      end
      object ViewSebPRICE2: TcxGridDBColumn
        Caption = #1062#1077#1085#1072' '#1087#1086' '#1087#1072#1088#1090#1080#1103#1084
        DataBinding.FieldName = 'PRICE2'
        Visible = False
      end
      object ViewSebSUM2: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1087#1086' '#1087#1072#1088#1090#1080#1103#1084
        DataBinding.FieldName = 'SUM2'
        Visible = False
      end
    end
    object LevelSeb: TcxGridLevel
      GridView = ViewSeb
    end
  end
  object Memo1: TcxMemo
    Left = 0
    Top = 368
    Align = alBottom
    Lines.Strings = (
      'Memo1')
    TabOrder = 2
    OnDblClick = Memo1DblClick
    Height = 74
    Width = 538
  end
  object taSeb: TClientDataSet
    Aggregates = <>
    FileName = 'CalcSeb.cds'
    Params = <>
    Left = 216
    Top = 144
    object taSebIDCARD: TIntegerField
      FieldName = 'IDCARD'
    end
    object taSebCURMESSURE: TIntegerField
      FieldName = 'CURMESSURE'
    end
    object taSebNETTO: TFloatField
      FieldName = 'NETTO'
      DisplayFormat = '0.000'
    end
    object taSebBRUTTO: TFloatField
      FieldName = 'BRUTTO'
      DisplayFormat = '0.000'
    end
    object taSebNAME: TStringField
      FieldName = 'NAME'
      Size = 200
    end
    object taSebNAMESHORT: TStringField
      FieldName = 'NAMESHORT'
      Size = 50
    end
    object taSebKOEF: TFloatField
      FieldName = 'KOEF'
    end
    object taSebKNB: TFloatField
      FieldName = 'KNB'
    end
    object taSebPRICE1: TCurrencyField
      FieldName = 'PRICE1'
      DisplayFormat = '0.00'
    end
    object taSebSUM1: TCurrencyField
      FieldName = 'SUM1'
      DisplayFormat = '0.00'
    end
    object taSebPRICE2: TCurrencyField
      FieldName = 'PRICE2'
      DisplayFormat = '0.00'
    end
    object taSebSUM2: TCurrencyField
      FieldName = 'SUM2'
      DisplayFormat = '0.00'
    end
  end
  object dsSeb: TDataSource
    DataSet = taSeb
    Left = 216
    Top = 200
  end
  object quMHAll: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT MH.ID,MH.PARENT,MH.ITYPE,MH.NAMEMH,'
      '       MH.DEFPRICE,PT.NAMEPRICE'
      'FROM OF_MH MH'
      'left JOIN OF_PRICETYPE PT ON PT.ID=MH.DEFPRICE'
      'WHERE MH.ITYPE=1'
      'Order by MH.ID')
    Transaction = dmO.trSel1
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    Left = 296
    Top = 144
    object quMHAllID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quMHAllPARENT: TFIBIntegerField
      FieldName = 'PARENT'
    end
    object quMHAllITYPE: TFIBIntegerField
      FieldName = 'ITYPE'
    end
    object quMHAllNAMEMH: TFIBStringField
      FieldName = 'NAMEMH'
      Size = 100
      EmptyStrToNull = True
    end
    object quMHAllDEFPRICE: TFIBIntegerField
      FieldName = 'DEFPRICE'
    end
    object quMHAllNAMEPRICE: TFIBStringField
      FieldName = 'NAMEPRICE'
      Size = 100
      EmptyStrToNull = True
    end
  end
  object dsMHAll: TDataSource
    DataSet = quMHAll
    Left = 296
    Top = 200
  end
  object taSpecC: TClientDataSet
    Aggregates = <>
    FileName = 'SpecSeb.cds'
    FieldDefs = <
      item
        Name = 'Num'
        DataType = ftInteger
      end
      item
        Name = 'IdGoods'
        DataType = ftInteger
      end
      item
        Name = 'NameG'
        DataType = ftString
        Size = 200
      end
      item
        Name = 'IM'
        DataType = ftInteger
      end
      item
        Name = 'SM'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Quant'
        DataType = ftFloat
      end
      item
        Name = 'PriceIn'
        DataType = ftFloat
      end
      item
        Name = 'SumIn'
        DataType = ftFloat
      end
      item
        Name = 'PriceUch'
        DataType = ftFloat
      end
      item
        Name = 'SumUch'
        DataType = ftFloat
      end
      item
        Name = 'Km'
        DataType = ftFloat
      end>
    IndexDefs = <
      item
        Name = 'taSpecCIndex1'
        Fields = 'IDGoods'
        Options = [ixCaseInsensitive]
      end>
    IndexName = 'taSpecCIndex1'
    Params = <>
    StoreDefs = True
    Left = 112
    Top = 168
    object taSpecCNum: TIntegerField
      FieldName = 'Num'
    end
    object taSpecCIdGoods: TIntegerField
      FieldName = 'IdGoods'
    end
    object taSpecCNameG: TStringField
      FieldName = 'NameG'
      Size = 200
    end
    object taSpecCIM: TIntegerField
      FieldName = 'IM'
    end
    object taSpecCSM: TStringField
      FieldName = 'SM'
    end
    object taSpecCQuant: TFloatField
      FieldName = 'Quant'
      DisplayFormat = '0.000'
    end
    object taSpecCPriceIn: TFloatField
      FieldName = 'PriceIn'
      DisplayFormat = '0.00'
    end
    object taSpecCSumIn: TFloatField
      FieldName = 'SumIn'
      DisplayFormat = '0.00'
    end
    object taSpecCPriceUch: TFloatField
      FieldName = 'PriceUch'
      DisplayFormat = '0.00'
    end
    object taSpecCSumUch: TFloatField
      FieldName = 'SumUch'
      DisplayFormat = '0.00'
    end
    object taSpecCKm: TFloatField
      FieldName = 'Km'
    end
  end
end
