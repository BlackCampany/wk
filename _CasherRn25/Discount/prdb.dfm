object dmPC: TdmPC
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 642
  Top = 148
  Height = 425
  Width = 623
  object PCDb: TpFIBDatabase
    DBName = 'localhost:C:\_CasherRn\DB2\CASHERRN.GDB'
    DBParams.Strings = (
      'user_name=SYSDBA'
      'lc_ctype=WIN1251'
      'password=masterkey'
      'sql_role_name=SYSDBA')
    SQLDialect = 3
    Timeout = 0
    SynchronizeTime = False
    DesignDBOptions = []
    AliasName = 'PCDb'
    WaitForRestoreConnect = 0
    Left = 20
    Top = 15
  end
  object trSelPC: TpFIBTransaction
    DefaultDatabase = PCDb
    TimeoutAction = TARollback
    Left = 20
    Top = 68
  end
  object trUpdPC: TpFIBTransaction
    DefaultDatabase = PCDb
    TimeoutAction = TARollback
    Left = 20
    Top = 120
  end
  object prGetLim: TpFIBStoredProc
    Transaction = trSelPC
    Database = PCDb
    SQL.Strings = (
      'EXECUTE PROCEDURE PR_GETBNLIM (?PCBAR)')
    StoredProcName = 'PR_GETBNLIM'
    Left = 24
    Top = 296
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object prWritePC: TpFIBStoredProc
    Transaction = trSelPC
    Database = PCDb
    SQL.Strings = (
      
        'EXECUTE PROCEDURE PRSAVEPC (?PCBAR, ?PCSUM, ?STATION, ?IDPERSON,' +
        ' ?PERSNAME, ?ID_TAB)')
    StoredProcName = 'PRSAVEPC'
    Left = 24
    Top = 240
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object dsPCard: TDataSource
    DataSet = taPCard
    Left = 120
    Top = 80
  end
  object taPCard: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE PLATCARD'
      'SET '
      '    PLATTYPE = :PLATTYPE,'
      '    IACTIVE = :IACTIVE,'
      '    CLINAME = :CLINAME,'
      '    TYPEOPL = :TYPEOPL,'
      '    BALANS = :BALANS,'
      '    DAYLIMIT = :DAYLIMIT,'
      '    BALANCECARD = :BALANCECARD,'
      '    SALET = :SALET'
      'WHERE'
      '    BARCODE = :OLD_BARCODE'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    PLATCARD'
      'WHERE'
      '        BARCODE = :OLD_BARCODE'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO PLATCARD('
      '    BARCODE,'
      '    PLATTYPE,'
      '    IACTIVE,'
      '    CLINAME,'
      '    TYPEOPL,'
      '    BALANS,'
      '    DAYLIMIT,'
      '    BALANCECARD,'
      '    SALET'
      ')'
      'VALUES('
      '    :BARCODE,'
      '    :PLATTYPE,'
      '    :IACTIVE,'
      '    :CLINAME,'
      '    :TYPEOPL,'
      '    :BALANS,'
      '    :DAYLIMIT,'
      '    :BALANCECARD,'
      '    :SALET'
      ')')
    RefreshSQL.Strings = (
      'SELECT pc.BARCODE,'
      ' pc.PLATTYPE,'
      ' pc.IACTIVE,'
      ' pc.CLINAME,'
      ' pc.TYPEOPL,'
      ' pc.BALANS,'
      ' pc.DAYLIMIT,'
      ' pc.BALANCECARD,'
      ' pc.SALET,'
      ' pt.NAME,'
      ' pt.DATEFROM,'
      ' pt.DATETO,'
      ' pt.COMMENT'
      ''
      'FROM PLATCARD pc'
      'Left join PLATTYPE pt on pc.PLATTYPE=pt.ID'
      ''
      ''
      ' WHERE '
      '        PC.BARCODE = :OLD_BARCODE'
      '    ')
    SelectSQL.Strings = (
      'SELECT pc.BARCODE,'
      ' pc.PLATTYPE,'
      ' pc.IACTIVE,'
      ' pc.CLINAME,'
      ' pc.TYPEOPL,'
      ' pc.BALANS,'
      ' pc.DAYLIMIT,'
      ' pc.BALANCECARD,'
      ' pc.SALET,'
      ' pt.NAME,'
      ' pt.DATEFROM,'
      ' pt.DATETO,'
      ' pt.COMMENT'
      ''
      'FROM PLATCARD pc'
      'Left join PLATTYPE pt on pc.PLATTYPE=pt.ID'
      ''
      'Order by plattype,BARCODE')
    OnCalcFields = taPCardCalcFields
    Transaction = trSel1
    Database = PCDb
    UpdateTransaction = trUpd1
    AutoCommit = True
    Left = 120
    Top = 20
    poAskRecordCount = True
    object taPCardBARCODE: TFIBStringField
      FieldName = 'BARCODE'
      Size = 30
      EmptyStrToNull = True
    end
    object taPCardPLATTYPE: TFIBIntegerField
      FieldName = 'PLATTYPE'
    end
    object taPCardIACTIVE: TFIBSmallIntField
      FieldName = 'IACTIVE'
    end
    object taPCardCLINAME: TFIBStringField
      FieldName = 'CLINAME'
      Size = 50
      EmptyStrToNull = True
    end
    object taPCardTYPEOPL: TFIBSmallIntField
      FieldName = 'TYPEOPL'
    end
    object taPCardBALANS: TFIBFloatField
      FieldName = 'BALANS'
    end
    object taPCardDAYLIMIT: TFIBFloatField
      FieldName = 'DAYLIMIT'
    end
    object taPCardBALANCECARD: TFIBStringField
      FieldName = 'BALANCECARD'
      Size = 30
      EmptyStrToNull = True
    end
    object taPCardSALET: TFIBIntegerField
      FieldName = 'SALET'
    end
    object taPCardNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 100
      EmptyStrToNull = True
    end
    object taPCardDATEFROM: TFIBDateField
      FieldName = 'DATEFROM'
    end
    object taPCardDATETO: TFIBDateField
      FieldName = 'DATETO'
    end
    object taPCardCOMMENT: TFIBStringField
      FieldName = 'COMMENT'
      Size = 300
      EmptyStrToNull = True
    end
    object taPCardVIDOPL: TStringField
      FieldKind = fkCalculated
      FieldName = 'VIDOPL'
      Size = 30
      Calculated = True
    end
  end
  object taTypePl: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE PLATTYPE'
      'SET '
      '    NAME = :NAME,'
      '    DATEFROM = :DATEFROM,'
      '    DATETO = :DATETO,'
      '    COMMENT = :COMMENT,'
      '    CEHPIT  = :CEHPIT'
      ''
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    PLATTYPE'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO PLATTYPE('
      '    ID,'
      '    NAME,'
      '    DATEFROM,'
      '    DATETO,'
      '    COMMENT,'
      '    CEHPIT'
      ''
      ')'
      'VALUES('
      '    :ID,'
      '    :NAME,'
      '    :DATEFROM,'
      '    :DATETO,'
      '    :COMMENT,'
      '    :CEHPIT'
      ''
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    NAME,'
      '    DATEFROM,'
      '    DATETO,'
      '    COMMENT,'
      '    CEHPIT'
      ''
      'FROM'
      '    PLATTYPE '
      ''
      ' WHERE '
      '        PLATTYPE.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    NAME,'
      '    DATEFROM,'
      '    DATETO,'
      '    COMMENT,'
      '    CEHPIT'
      'FROM'
      '    PLATTYPE ')
    Transaction = trSel1
    Database = PCDb
    UpdateTransaction = trUpd1
    Left = 188
    Top = 20
    object taTypePlID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taTypePlNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 100
      EmptyStrToNull = True
    end
    object taTypePlDATEFROM: TFIBDateField
      DefaultExpression = '01.01.2006'
      FieldName = 'DATEFROM'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object taTypePlDATETO: TFIBDateField
      DefaultExpression = '01.01.2006'
      FieldName = 'DATETO'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object taTypePlCOMMENT: TFIBStringField
      FieldName = 'COMMENT'
      Size = 300
      EmptyStrToNull = True
    end
    object taTypePlCEHPIT: TFIBSmallIntField
      DefaultExpression = '0'
      FieldName = 'CEHPIT'
    end
  end
  object dsTypePl: TDataSource
    DataSet = taTypePl
    Left = 188
    Top = 76
  end
  object trDelPC: TpFIBTransaction
    DefaultDatabase = PCDb
    TimeoutAction = TARollback
    Left = 24
    Top = 176
  end
  object quMaxIdTPL: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT Max(ID) as MAXID'
      'FROM PLATTYPE'
      ' ')
    Transaction = trSel1
    Database = PCDb
    UpdateTransaction = trUpdPC
    Left = 252
    Top = 16
    object quMaxIdTPLMAXID: TFIBIntegerField
      FieldName = 'MAXID'
    end
  end
  object quFindTPL: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT Count(*) as CountRec from PLATCARD'
      'WHERE PLATTYPE=:PLATTYPE')
    Transaction = trSel1
    Database = PCDb
    UpdateTransaction = trUpdPC
    Left = 252
    Top = 80
    object quFindTPLCOUNTREC: TFIBIntegerField
      FieldName = 'COUNTREC'
    end
  end
  object taCategSaleSel: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE CLASSIF'
      'SET '
      '    TYPE_CLASSIF = :TYPE_CLASSIF,'
      '    ID = :ID,'
      '    IACTIVE = :IACTIVE,'
      '    ID_PARENT = :ID_PARENT,'
      '    NAME = :NAME,'
      '    IEDIT = :IEDIT'
      'WHERE'
      '    TYPE_CLASSIF = :OLD_TYPE_CLASSIF'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    CLASSIF'
      'WHERE'
      '        TYPE_CLASSIF = :OLD_TYPE_CLASSIF'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO CLASSIF('
      '    TYPE_CLASSIF,'
      '    ID,'
      '    IACTIVE,'
      '    ID_PARENT,'
      '    NAME,'
      '    IEDIT'
      ')'
      'VALUES('
      '    :TYPE_CLASSIF,'
      '    :ID,'
      '    :IACTIVE,'
      '    :ID_PARENT,'
      '    :NAME,'
      '    :IEDIT'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    TYPE_CLASSIF,'
      '    ID,'
      '    IACTIVE,'
      '    ID_PARENT,'
      '    NAME,'
      '    IEDIT'
      'FROM'
      '    CLASSIF '
      ''
      ' WHERE '
      '        CLASSIF.TYPE_CLASSIF = :OLD_TYPE_CLASSIF'
      '    and CLASSIF.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    NAMECS'
      'FROM'
      '    CATEGSALE ')
    Transaction = trSel1
    Database = PCDb
    AutoCommit = True
    Left = 552
    Top = 32
    object taCategSaleSelID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taCategSaleSelNAMECS: TFIBStringField
      FieldName = 'NAMECS'
      EmptyStrToNull = True
    end
  end
  object dsCategSaleSel: TDataSource
    DataSet = taCategSaleSel
    Left = 552
    Top = 80
  end
  object dsPCHist: TDataSource
    DataSet = quPCHist
    Left = 332
    Top = 80
  end
  object quPCHist: TpFIBDataSet
    SelectSQL.Strings = (
      
        'SELECT PH.BARCODE,PH.DATEOP,PH.TYPEOP,PH.STATION,PH.IDPERSON,PH.' +
        'PERSNAME,PH.DBNUM,'
      'PH.BARCLI,PH.RSUM,'
      'PC.CLINAME as CLIBALANCE,'
      'PC.CLINAME as CLINAME1'
      ''
      'FROM PLATHIST PH'
      'left join PLATCARD PC on PC.BARCODE=PH.BARCODE'
      'left join PLATCARD PC1 on PC1.BARCODE=PH.BARCLI'
      ''
      ''
      'where PH.DATEOP>=:DATEB'
      'and PH.DATEOP<:DATEE'
      'and PH.BARCLI=:BARCODE'
      ''
      'Order by PH.DATEOP'
      ''
      '')
    Transaction = trSel1
    Database = PCDb
    Left = 332
    Top = 20
    poAskRecordCount = True
    object quPCHistBARCODE: TFIBStringField
      FieldName = 'BARCODE'
      Size = 30
      EmptyStrToNull = True
    end
    object quPCHistDATEOP: TFIBDateField
      FieldName = 'DATEOP'
    end
    object quPCHistTYPEOP: TFIBSmallIntField
      FieldName = 'TYPEOP'
    end
    object quPCHistSTATION: TFIBIntegerField
      FieldName = 'STATION'
    end
    object quPCHistIDPERSON: TFIBIntegerField
      FieldName = 'IDPERSON'
    end
    object quPCHistPERSNAME: TFIBStringField
      FieldName = 'PERSNAME'
      Size = 50
      EmptyStrToNull = True
    end
    object quPCHistDBNUM: TFIBSmallIntField
      FieldName = 'DBNUM'
    end
    object quPCHistBARCLI: TFIBStringField
      FieldName = 'BARCLI'
      Size = 30
      EmptyStrToNull = True
    end
    object quPCHistRSUM: TFIBFloatField
      FieldName = 'RSUM'
      DisplayFormat = '0.00'
    end
    object quPCHistCLIBALANCE: TFIBStringField
      FieldName = 'CLIBALANCE'
      Size = 50
      EmptyStrToNull = True
    end
    object quPCHistCLINAME1: TFIBStringField
      FieldName = 'CLINAME1'
      Size = 50
      EmptyStrToNull = True
    end
  end
  object quFindPC: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE PLATCARD'
      'SET '
      '    PLATTYPE = :PLATTYPE,'
      '    IACTIVE = :IACTIVE,'
      '    CLINAME = :CLINAME,'
      '    TYPEOPL = :TYPEOPL,'
      '    DAYLIMIT = :DAYLIMIT,'
      '    BALANCECARD = :BALANCECARD'
      'WHERE'
      '    BARCODE = :OLD_BARCODE'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    PLATCARD'
      'WHERE'
      '        BARCODE = :OLD_BARCODE'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO PLATCARD('
      '    BARCODE,'
      '    PLATTYPE,'
      '    IACTIVE,'
      '    CLINAME,'
      '    TYPEOPL,'
      '    BALANCECARD  '
      ')'
      'VALUES('
      '    :BARCODE,'
      '    :PLATTYPE,'
      '    :IACTIVE,'
      '    :CLINAME,'
      '    :TYPEOPL, '
      '    :BALANCECARD'
      ')')
    RefreshSQL.Strings = (
      'SELECT pc.BARCODE,'
      ' pc.PLATTYPE,'
      ' pc.IACTIVE,'
      ' pc.CLINAME,'
      ' pc.TYPEOPL,'
      ' pc.BALANS,'
      ' pc.DAYLIMIT,'
      ' pc.BALANCECARD,'
      ''
      ' pc.CLINAME as BALANCECARDNAME,'
      ''
      ' pt.NAME,'
      ' pt.DATEFROM,'
      ' pt.DATETO,'
      ' pt.COMMENT'
      ''
      'FROM PLATCARD pc'
      'Left join PLATTYPE pt on pc.PLATTYPE=pt.ID'
      ''
      ' WHERE '
      '        PLATCARD.BARCODE = :OLD_BARCODE')
    SelectSQL.Strings = (
      'SELECT first 1 pc.CLINAME'
      'FROM PLATCARD pc'
      'where BARCODE=:SBAR')
    OnCalcFields = taPCardCalcFields
    Transaction = trSel1
    Database = PCDb
    UpdateTransaction = trUpdPC
    Left = 464
    Top = 164
    poAskRecordCount = True
    object quFindPCCLINAME: TFIBStringField
      FieldName = 'CLINAME'
      Size = 50
      EmptyStrToNull = True
    end
  end
  object quFindMovePC: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE PLATCARD'
      'SET '
      '    PLATTYPE = :PLATTYPE,'
      '    IACTIVE = :IACTIVE,'
      '    CLINAME = :CLINAME,'
      '    TYPEOPL = :TYPEOPL,'
      '    DAYLIMIT = :DAYLIMIT,'
      '    BALANCECARD = :BALANCECARD'
      'WHERE'
      '    BARCODE = :OLD_BARCODE'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    PLATCARD'
      'WHERE'
      '        BARCODE = :OLD_BARCODE'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO PLATCARD('
      '    BARCODE,'
      '    PLATTYPE,'
      '    IACTIVE,'
      '    CLINAME,'
      '    TYPEOPL,'
      '    BALANCECARD  '
      ')'
      'VALUES('
      '    :BARCODE,'
      '    :PLATTYPE,'
      '    :IACTIVE,'
      '    :CLINAME,'
      '    :TYPEOPL, '
      '    :BALANCECARD'
      ')')
    RefreshSQL.Strings = (
      'SELECT pc.BARCODE,'
      ' pc.PLATTYPE,'
      ' pc.IACTIVE,'
      ' pc.CLINAME,'
      ' pc.TYPEOPL,'
      ' pc.BALANS,'
      ' pc.DAYLIMIT,'
      ' pc.BALANCECARD,'
      ''
      ' pc.CLINAME as BALANCECARDNAME,'
      ''
      ' pt.NAME,'
      ' pt.DATEFROM,'
      ' pt.DATETO,'
      ' pt.COMMENT'
      ''
      'FROM PLATCARD pc'
      'Left join PLATTYPE pt on pc.PLATTYPE=pt.ID'
      ''
      ' WHERE '
      '        PLATCARD.BARCODE = :OLD_BARCODE')
    SelectSQL.Strings = (
      'SELECT first 1'
      '    BARCODE,'
      '    DATEOP,'
      '    ID,'
      '    RSUM,'
      '    TYPEOP,'
      '    STATION,'
      '    IDPERSON,'
      '    DATEACTION,'
      '    PERSNAME,'
      '    DBNUM,'
      '    BARCLI,'
      '    IDTAB'
      'FROM'
      '    PLATHIST '
      'where BARCLI=:SBAR')
    OnCalcFields = taPCardCalcFields
    Transaction = trSel1
    Database = PCDb
    UpdateTransaction = trUpdPC
    Left = 544
    Top = 164
    poAskRecordCount = True
    object quFindMovePCBARCODE: TFIBStringField
      FieldName = 'BARCODE'
      Size = 30
      EmptyStrToNull = True
    end
    object quFindMovePCDATEOP: TFIBDateField
      FieldName = 'DATEOP'
    end
    object quFindMovePCID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quFindMovePCRSUM: TFIBFloatField
      FieldName = 'RSUM'
    end
    object quFindMovePCTYPEOP: TFIBSmallIntField
      FieldName = 'TYPEOP'
    end
    object quFindMovePCSTATION: TFIBIntegerField
      FieldName = 'STATION'
    end
    object quFindMovePCIDPERSON: TFIBIntegerField
      FieldName = 'IDPERSON'
    end
    object quFindMovePCDATEACTION: TFIBDateField
      FieldName = 'DATEACTION'
    end
    object quFindMovePCPERSNAME: TFIBStringField
      FieldName = 'PERSNAME'
      Size = 50
      EmptyStrToNull = True
    end
    object quFindMovePCDBNUM: TFIBSmallIntField
      FieldName = 'DBNUM'
    end
    object quFindMovePCBARCLI: TFIBStringField
      FieldName = 'BARCLI'
      Size = 30
      EmptyStrToNull = True
    end
    object quFindMovePCIDTAB: TFIBIntegerField
      FieldName = 'IDTAB'
    end
  end
  object prInpMoney: TpFIBStoredProc
    Transaction = trSelPC
    Database = PCDb
    SQL.Strings = (
      
        'EXECUTE PROCEDURE PRSAVEPC1 (?PCBAR, ?PCSUM, ?STATION, ?IDPERSON' +
        ', ?PERSNAME, ?DATEOP)')
    StoredProcName = 'PRSAVEPC1'
    Left = 112
    Top = 244
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object trSel1: TpFIBTransaction
    DefaultDatabase = PCDb
    TimeoutAction = TARollback
    Left = 108
    Top = 168
  end
  object trUpd1: TpFIBTransaction
    DefaultDatabase = PCDb
    TimeoutAction = TARollback
    Left = 168
    Top = 168
  end
  object quBalansPC: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT'
      'ph.BARCODE,'
      'pc.PLATTYPE,'
      'pc.IACTIVE,'
      'pc.CLINAME,'
      'pc.TYPEOPL,'
      'pc.DAYLIMIT,'
      'pc.BALANCECARD,'
      'pc.SALET,'
      'pt.NAME,'
      'SUM(ph.RSUM)'
      'FROM PLATHIST ph'
      ''
      'left join PLATCARD pc on pc.BARCODE=ph.BARCODE'
      'Left join PLATTYPE pt on pt.ID=pc.PLATTYPE'
      ''
      'where pc.TYPEOPL>=2'
      ''
      'group by ph.BARCODE,'
      'pc.PLATTYPE,'
      'pc.IACTIVE,'
      'pc.CLINAME,'
      'pc.TYPEOPL,'
      'pc.DAYLIMIT,'
      'pc.BALANCECARD,'
      'pc.SALET,'
      'pt.NAME')
    Transaction = trSel1
    Database = PCDb
    Left = 272
    Top = 148
    poAskRecordCount = True
    object quBalansPCBARCODE: TFIBStringField
      FieldName = 'BARCODE'
      Size = 30
      EmptyStrToNull = True
    end
    object quBalansPCSUM: TFIBFloatField
      FieldName = 'SUM'
    end
    object quBalansPCPLATTYPE: TFIBIntegerField
      FieldName = 'PLATTYPE'
    end
    object quBalansPCIACTIVE: TFIBSmallIntField
      FieldName = 'IACTIVE'
    end
    object quBalansPCCLINAME: TFIBStringField
      FieldName = 'CLINAME'
      Size = 50
      EmptyStrToNull = True
    end
    object quBalansPCTYPEOPL: TFIBSmallIntField
      FieldName = 'TYPEOPL'
    end
    object quBalansPCDAYLIMIT: TFIBFloatField
      FieldName = 'DAYLIMIT'
    end
    object quBalansPCBALANCECARD: TFIBStringField
      FieldName = 'BALANCECARD'
      Size = 30
      EmptyStrToNull = True
    end
    object quBalansPCSALET: TFIBSmallIntField
      FieldName = 'SALET'
    end
    object quBalansPCNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 100
      EmptyStrToNull = True
    end
  end
  object dsquBalansPC: TDataSource
    DataSet = quBalansPC
    Left = 272
    Top = 200
  end
end
