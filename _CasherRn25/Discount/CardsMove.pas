unit CardsMove;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SpeedBar, ExtCtrls, ComCtrls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  cxCalendar, ActnList, XPStyleActnCtrls, ActnMan, StdCtrls, Placemnt;

type
  TfmCardsMove = class(TForm)
    PageControl1: TPageControl;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    ViewPartIn: TcxGridDBTableView;
    LevelPartIn: TcxGridLevel;
    GridPartIn: TcxGrid;
    GridPartOut: TcxGrid;
    ViewPartOut: TcxGridDBTableView;
    LevelPartOut: TcxGridLevel;
    GridMove: TcxGrid;
    ViewMove: TcxGridDBTableView;
    LevelMove: TcxGridLevel;
    ViewPartInID: TcxGridDBColumn;
    ViewPartInNAMEMH: TcxGridDBColumn;
    ViewPartInIDDOC: TcxGridDBColumn;
    ViewPartInNUMDOC: TcxGridDBColumn;
    ViewPartInARTICUL: TcxGridDBColumn;
    ViewPartInIDCLI: TcxGridDBColumn;
    ViewPartInNAMECL: TcxGridDBColumn;
    ViewPartInDTYPE: TcxGridDBColumn;
    ViewPartInQPART: TcxGridDBColumn;
    ViewPartInQREMN: TcxGridDBColumn;
    ViewPartInPRICEIN: TcxGridDBColumn;
    ViewPartInPRICEOUT: TcxGridDBColumn;
    ViewPartInIDATE: TcxGridDBColumn;
    ViewPartInSUMIN: TcxGridDBColumn;
    ViewPartInSUMREMN: TcxGridDBColumn;
    ViewPartOutARTICUL: TcxGridDBColumn;
    ViewPartOutIDDATE: TcxGridDBColumn;
    ViewPartOutIDSTORE: TcxGridDBColumn;
    ViewPartOutNAMEMH: TcxGridDBColumn;
    ViewPartOutIDPARTIN: TcxGridDBColumn;
    ViewPartOutIDDOC: TcxGridDBColumn;
    ViewPartOutNUMDOC: TcxGridDBColumn;
    ViewPartOutIDCLI: TcxGridDBColumn;
    ViewPartOutNAMECL: TcxGridDBColumn;
    ViewPartOutDTYPE: TcxGridDBColumn;
    ViewPartOutQUANT: TcxGridDBColumn;
    ViewPartOutPRICEIN: TcxGridDBColumn;
    ViewPartOutSUMOUT: TcxGridDBColumn;
    amCM: TActionManager;
    acExit: TAction;
    acPeriod: TAction;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    Edit1: TEdit;
    ViewMoveARTICUL: TcxGridDBColumn;
    ViewMoveIDATE: TcxGridDBColumn;
    ViewMoveIDSTORE: TcxGridDBColumn;
    ViewMoveNAMEMH: TcxGridDBColumn;
    ViewMoveRB: TcxGridDBColumn;
    ViewMovePOSTIN: TcxGridDBColumn;
    ViewMovePOSTOUT: TcxGridDBColumn;
    ViewMoveVNIN: TcxGridDBColumn;
    ViewMoveVNOUT: TcxGridDBColumn;
    ViewMoveINV: TcxGridDBColumn;
    ViewMoveQREAL: TcxGridDBColumn;
    ViewMoveRE: TcxGridDBColumn;
    FormPlacement1: TFormPlacement;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acExitExecute(Sender: TObject);
    procedure acPeriodExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmCardsMove: TfmCardsMove;

implementation

uses dmOffice, SelPerSkl, Un1, DMOReps;

{$R *.dfm}

procedure TfmCardsMove.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  PageControl1.Align:=AlClient;
  PageControl1.ActivePageIndex:=0;
  ViewPartIn.RestoreFromIniFile(CurDir+GridIni);
  ViewPartOut.RestoreFromIniFile(CurDir+GridIni);
  ViewMove.RestoreFromIniFile(CurDir+GridIni);
end;

procedure TfmCardsMove.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  with dmO do
  with dmORep do
  begin
    quCPartIn.Active:=False;
    quCPartOut.Active:=False;
    quCMove.Active:=False;
    taCMove.Active:=False;
  end;
  ViewPartIn.StoreToIniFile(CurDir+GridIni,False);
  ViewPartOut.StoreToIniFile(CurDir+GridIni,False);
  ViewMove.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmCardsMove.acExitExecute(Sender: TObject);
begin
  close;
end;

procedure TfmCardsMove.acPeriodExecute(Sender: TObject);
Var iDateB,iDateE:INteger;
begin
  //�� ������
  fmSelPerSkl:=tfmSelPerSkl.Create(Application);
  with fmSelPerSkl do
  begin
    cxDateEdit1.Date:=CommonSet.DateFrom;
    quMHAll1.Active:=False;
    quMHAll1.Active:=True;
    quMHAll1.First;
    if CommonSet.IdStore>0 then cxLookupComboBox1.EditValue:=CommonSet.IdStore
    else  cxLookupComboBox1.EditValue:=quMHAll1ID.AsInteger;
//    Label3.Visible:=False;
//    cxLookupComboBox1.Visible:=False;
  end;
  fmSelPerSkl.ShowModal;
  if fmSelPerSkl.ModalResult=mrOk then
  begin
    iDateB:=Trunc(fmSelPerSkl.cxDateEdit1.Date);
    iDateE:=Trunc(fmSelPerSkl.cxDateEdit2.Date)+1;

    prPreShowMove(fmCardsMove.Tag,iDateB,iDateE,0,fmCardsMove.Edit1.Text);
  end;
  fmSelPerSkl.Release;
end;

end.
