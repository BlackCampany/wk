unit AddPlCard;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons, ExtCtrls,
  ComCtrls, dxfBackGround, cxMaskEdit, cxDropDownEdit, cxCalc, cxCheckBox,
  cxControls, cxContainer, cxEdit, cxTextEdit, cxGraphics, cxLookupEdit,
  cxDBLookupEdit, cxDBLookupComboBox;

type
  TfmAddPlCard = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Label1: TLabel;
    cxTextEdit1: TcxTextEdit;
    Label2: TLabel;
    cxTextEdit2: TcxTextEdit;
    CheckBox1: TcxCheckBox;
    Label3: TLabel;
    dxfBackGround1: TdxfBackGround;
    cxLookupComboBox1: TcxLookupComboBox;
    cxComboBoxTypeOpl: TcxComboBox;
    Label4: TLabel;
    Panel3: TPanel;
    LabelBALANCECARD: TLabel;
    LabelDAYLIMIT: TLabel;
    LabelBalans: TLabel;
    cxCheckBoxGol: TcxCheckBox;
    cxTextEditBALANCECARD: TcxTextEdit;
    cxLookupComboBoxCategSale: TcxLookupComboBox;
    Label5: TLabel;
    cxPCHist: TcxButton;
    cxCalcEdit1: TcxCalcEdit;
    cxCalcEdit2: TcxCalcEdit;
    cxButton3: TcxButton;
    procedure cxButton2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxCheckBoxGolEditing(Sender: TObject; var CanEdit: Boolean);
    procedure cxComboBoxTypeOplClick(Sender: TObject);
    procedure cxCheckBoxGolClick(Sender: TObject);
    procedure cxTextEdit2PropertiesEditValueChanged(Sender: TObject);
    procedure cxPCHistClick(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
  private
    procedure VisibleField();
    { Private declarations }
  public
    { Public declarations }
  end;


var
  fmAddPlCard: TfmAddPlCard;

implementation

uses Un1,prdb, DmRnDisc, PCDetail, PCHistori, InputMoney, PCard;

{$R *.dfm}

procedure TfmAddPlCard.cxButton2Click(Sender: TObject);
begin
  Close;
end;

procedure TfmAddPlCard.VisibleField();
var VF : Boolean;
begin
   VF := False;

   if cxComboBoxTypeOpl.ItemIndex > 0 then
   begin
    VF := True;
   end;
   cxCalcEdit2.visible := VF;
   cxCheckBoxGol.Visible := VF;
   cxCalcEdit1.Visible := VF;

   cxTextEditBALANCECARD.Visible := (cxCheckBoxGol.Checked and VF);

   LabelBalans.Visible := cxCalcEdit2.visible;
   LabelBALANCECARD.Visible :=cxTextEditBALANCECARD.Visible;
   LabelDAYLIMIT.Visible := cxCalcEdit1.Visible;
end;

procedure TfmAddPlCard.FormCreate(Sender: TObject);
begin

 VisibleField();

end;

procedure TfmAddPlCard.cxCheckBoxGolEditing(Sender: TObject;
  var CanEdit: Boolean);
begin
 VisibleField();
end;

procedure TfmAddPlCard.cxComboBoxTypeOplClick(Sender: TObject);
begin
 VisibleField();
end;

procedure TfmAddPlCard.cxCheckBoxGolClick(Sender: TObject);
begin
 VisibleField();
end;

procedure TfmAddPlCard.cxTextEdit2PropertiesEditValueChanged(
  Sender: TObject);
begin
 if cxCheckBoxGol.EditValue = False then
  begin
   cxTextEditBALANCECARD.Text := cxTextEdit2.EditValue;
  end;

end;

procedure TfmAddPlCard.cxPCHistClick(Sender: TObject);
var  CliName,CliType:String;
     rBalans,rBalansDay,DLimit:Real;
begin
  fmPCHist.Caption:='������� �� ��������� ����� '+cxTextEdit2.Text+' �� ������ � '+FormatDateTime('dd.mm.yyyy hh:nn',TrebSel.DateFrom)+' �� '+FormatDateTime('dd.mm.yyyy hh:nn',TrebSel.DateTo);
  prFindPCardBalans(cxTextEdit2.Text,rBalans,rBalansDay,DLimit,CliName,CliType);
  fmPCHist.Label1.Caption:='��������  '+CliName+'     ���  '+CliType+'     ������  '+fts(rv(rBalans))+'     ������ �������  '+fts(rv(rBalansDay))+'     ������� �����  '+fts(rv((-1)*DLimit));
  with dmPC do
  begin
    fmPCHist.StatusBar1.Panels[1].Text:=cxTextEdit2.Text;
    fmPCHist.Label2.Caption:=cxTextEdit2.Text;
    fmPCHist.ViewPCHist.BeginUpdate;
    try
      quPCHist.Active:=False;
      quPCHist.ParamByName('DateB').AsDateTime:=TrebSel.DateFrom;
      quPCHist.ParamByName('DateE').AsDateTime:=Trunc(TrebSel.DateTo+1);
      quPCHist.ParamByName('BARCODE').AsString:=cxTextEdit2.Text;
      quPCHist.Active:=True;
    finally
      fmPCHist.ViewPCHist.EndUpdate;
    end;
  end;
  fmPCHist.Show;
end;

procedure TfmAddPlCard.cxButton3Click(Sender: TObject);
//Var sBar:String;
begin
{  //�������� ����� �� ��
  if not CanDo('prEditBalancePC') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  with dmPC do
  begin
    if taPCard.RecordCount>0 then
    begin
      if fmPCard.ViewPCards.Controller.SelectedRowCount=1 then
      begin
        if taPCardTYPEOPL.AsInteger=2 then
        begin
          sBar:=taPCardBARCODE.AsString;
          fmInputMoney.cxCalcEdit1.Value:=0;
          fmInputMoney.cxDateEdit1.Date:=Date;
          fmInputMoney.Label1.Caption:=taPCardCLINAME.AsString;
          fmInputMoney.ShowModal;
          if fmInputMoney.ModalResult=mrOk then
          begin
//EXECUTE PROCEDURE PRSAVEPC1 (?PCBAR, ?PCSUM, ?STATION, ?IDPERSON, ?PERSNAME)
            prInpMoney.ParamByName('PCBAR').AsString:=sBar;
            prInpMoney.ParamByName('PCSUM').AsFloat:=fmInputMoney.cxCalcEdit1.Value;
            prInpMoney.ParamByName('STATION').AsINteger:=CommonSet.Station;
            prInpMoney.ParamByName('IDPERSON').AsINteger:=Person.Id;
            prInpMoney.ParamByName('PERSNAME').AsString:=Person.Name;
            prInpMoney.ParamByName('DAYEOP').AsDate:=fmInputMoney.cxDateEdit1.Date;

            prInpMoney.ExecProc;
          end;
        end else
        begin
          showmessage('�� ������ ��� ������ �������� �� �����������������.');
        end;
      end;
    end;
  end;}
end;

end.
