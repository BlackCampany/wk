unit TypePl;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Placemnt, ComCtrls, ExtCtrls, SpeedBar, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  cxImageComboBox, cxDropDownEdit, cxCheckBox;

type
  TfmTypePl = class(TForm)
    StatusBar1: TStatusBar;
    FormPlacement1: TFormPlacement;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItemADD: TSpeedItem;
    SpeedItemEdit: TSpeedItem;
    SpeedItemDEL: TSpeedItem;
    SpeedItemExit: TSpeedItem;
    GridTPl: TcxGrid;
    ViewTPl: TcxGridDBTableView;
    LevelTPl: TcxGridLevel;
    ViewTPlID: TcxGridDBColumn;
    ViewTPlNAME: TcxGridDBColumn;
    ViewTPlDATEFROM: TcxGridDBColumn;
    ViewTPlDATETO: TcxGridDBColumn;
    ViewTPlCOMMENT: TcxGridDBColumn;
    ViewTPlCEHPIT: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SpeedItemExitClick(Sender: TObject);
    procedure SpeedItemADDClick(Sender: TObject);
    procedure SpeedItemEditClick(Sender: TObject);
    procedure SpeedItemDELClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmTypePl: TfmTypePl;

implementation

uses Un1, AddTypePl, prdb, dmRnDisc;

{$R *.dfm}

procedure TfmTypePl.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  GridTPl.Align:=AlClient;
  ViewTPl.RestoreFromIniFile(CurDir+GridIni);
  delay(10);
end;

procedure TfmTypePl.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewTPl.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmTypePl.SpeedItemExitClick(Sender: TObject);
begin
  close;
end;

procedure TfmTypePl.SpeedItemADDClick(Sender: TObject);
Var Id:Integer;
begin
  //�������� ��� ��������� ����
  if not CanDo('prAddTPl') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  with dmPC do
  begin
    fmAddTypePl:=TfmAddTypePl.create(Application);
    fmAddTypePl.Caption:='���������� ���� ��������� �����';
    fmAddTypePl.cxTextEdit1.Text:='';
    fmAddTypePl.cxTextEdit2.Text:='';
    fmAddTypePl.cxDateEdit1.Date:=Date;
    fmAddTypePl.cxDateEdit2.Date:=Date+365;

    if not taTypePl.eof then
    begin
      fmAddTypePl.cxTextEdit1.Text:=taTypePlNAME.AsString;
      fmAddTypePl.cxTextEdit2.Text:=taTypePlCOMMENT.AsString;
      fmAddTypePl.cxDateEdit1.Date:=taTypePlDATEFROM.AsDateTime;
      fmAddTypePl.cxDateEdit2.Date:=taTypePlDATETO.AsDateTime;
    end;

    fmAddTypePl.ShowModal;

    if fmAddTypePl.ModalResult=mrOk then
    begin
      try

        quMaxIdTpl.Active:=False;
        quMaxIdTpl.Active:=True;
        Id:=quMaxIdTplMAXID.AsInteger+1;
        quMaxIdTpl.Active:=False;

        trUpdPC.StartTransaction;
        taTypePl.Append;
        taTypePlID.AsInteger:=Id;
        taTypePlNAME.AsString:=Copy(fmAddTypePl.cxTextEdit1.Text,1,100);
        taTypePlCOMMENT.AsString:=Copy(fmAddTypePl.cxTextEdit2.Text,1,300);
        taTypePlDATEFROM.AsDateTime:=Trunc(fmAddTypePl.cxDateEdit1.Date);
        taTypePlDATETO.AsDateTime:=Trunc(fmAddTypePl.cxDateEdit2.Date);
        taTypePl.Post;
        trUpdPC.Commit;

      except
      end;
    end;
    fmAddTypePl.Release;
  end;
end;

procedure TfmTypePl.SpeedItemEditClick(Sender: TObject);
begin
  //������������� ��� ��������� ����
  if not CanDo('prEditTPl') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  with dmPC do
  begin
    if taTypePl.eof then exit;

    fmAddTypePl:=TfmAddTypePl.create(Application);
    fmAddTypePl.Caption:='�������������� ���� ��������� �����';

    fmAddTypePl.cxTextEdit1.Text:=taTypePlNAME.AsString;
    fmAddTypePl.cxTextEdit2.Text:=taTypePlCOMMENT.AsString;
    fmAddTypePl.cxDateEdit1.Date:=taTypePlDATEFROM.AsDateTime;
    fmAddTypePl.cxDateEdit2.Date:=taTypePlDATETO.AsDateTime;
    fmAddTypePl.cxCheckBox1.Checked:=taTypePlCEHPIT.AsBoolean;

    fmAddTypePl.ShowModal;
    if fmAddTypePl.ModalResult=mrOk then
    begin
      try

        trUpdPC.StartTransaction;
        taTypePl.Edit;
        taTypePlNAME.AsString:=Copy(fmAddTypePl.cxTextEdit1.Text,1,100);
        taTypePlCOMMENT.AsString:=Copy(fmAddTypePl.cxTextEdit2.Text,1,300);
        taTypePlDATEFROM.AsDateTime:=Trunc(fmAddTypePl.cxDateEdit1.Date);
        taTypePlDATETO.AsDateTime:=Trunc(fmAddTypePl.cxDateEdit2.Date);
        taTypePlCEHPIT.AsBoolean:=fmAddTypePl.cxCheckBox1.Checked;
        taTypePl.Post;
        trUpdPC.Commit;

        taTypePl.Refresh;
      except
      end;
    end;
    fmAddTypePl.Release;
  end;
end;

procedure TfmTypePl.SpeedItemDELClick(Sender: TObject);
begin
  //��������
  if not CanDo('prDelTPl') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  with dmPC do
  begin
    if taTypePl.eof then exit;

    if MessageDlg('�� ������������� ������ ������� ��� �� "'+taTypePlNAME.AsString+'"',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      quFindTPl.Active:=False;
      quFindTPl.ParamByName('PLATTYPE').AsInteger:=taTypePlId.AsInteger;
      quFindTPl.Active:=True;

      if quFindTPLCOUNTREC.AsInteger=0 then
      begin
        try
          trUpdPC.StartTransaction;
          taTypePl.Delete;
          trUpdPC.Commit;

          taTypePl.Refresh;
        except
        end;
      end else
      begin
        ShowMessage('�������� ����������, �� ��� ���� ������.');
      end;  
      quFindTPl.Active:=False;
    end;
  end;
end;

end.
