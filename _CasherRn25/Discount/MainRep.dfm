object fmMainRep: TfmMainRep
  Left = 156
  Top = 143
  Width = 844
  Height = 265
  Action = acRealMH
  Caption = #1056#1077#1072#1083#1080#1079#1072#1094#1080#1103' '#1087#1086' '#1052#1061' ('#1087#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1072')'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCanResize = FormCanResize
  OnClick = acRealMHExecute
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 212
    Width = 836
    Height = 19
    Panels = <
      item
        Width = 400
      end>
  end
  object ActionMainMenuBar1: TActionMainMenuBar
    Left = 0
    Top = 0
    Width = 836
    Height = 24
    UseSystemFont = False
    ActionManager = am
    Caption = 'ActionMainMenuBar1'
    ColorMap.HighlightColor = 15660791
    ColorMap.BtnSelectedColor = clBtnFace
    ColorMap.UnusedColor = 15660791
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Spacing = 0
  end
  object SpeedBar1: TSpeedBar
    Left = 0
    Top = 24
    Width = 836
    Height = 48
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Options = [sbAllowDrag, sbFlatBtns]
    BtnOffsetHorz = 4
    BtnOffsetVert = 4
    BtnWidth = 60
    BtnHeight = 40
    BevelInner = bvLowered
    TabOrder = 2
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object SpeedItem1: TSpeedItem
      Action = acTabs
      BtnCaption = #1047#1072#1082#1072#1079#1099
      Caption = #1054#1092#1086#1088#1084#1083#1077#1085#1085#1099#1077' '#1079#1072#1082#1072#1079#1099
      Glyph.Data = {
        AA030000424DAA03000000000000360000002800000011000000110000000100
        18000000000074030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FF00FFFFFFFFFFFFDEB284D6AE7BD6AA73D6A66BD69E63CE9A5ACE9652CE924A
        CE924ACE924ACE964ACE964ACE9652FFFFFFFFFFFF00FFFFFFFFFFFFDEB284FF
        FFFFFFFFFFFFFBF7000000FFF7EFF7F3EFF7EFE7100C08F7EBDEF7E7DEEFE3D6
        CE9652FFFFFFFFFFFF00FFFFFFFFFFFFDEB284D6AE7BD6AA73D6A66B000000CE
        9A5ACE9652CE924A100C08CE924ACE964ACE964ACE9652FFFFFFFFFFFF00FFFF
        FFFFFFFFDEB284FFFFFFFFFFFFFFFBF7000000FFF7EFF7F3EFF7EFE7100C08F7
        EBDEF7E7DEEFE3D6CE9652FFFFFFFFFFFF00FFFFFFFFFFFFDEB284D6AE7BD6AA
        73D6A66B000000CE9A5ACE9652CE924A100C08CE924ACE964ACE964ACE9652FF
        FFFFFFFFFF00FFFFFFFFFFFFDEB284FFFFFFFFFFFFFFFBF7000000FFF7EFF7F3
        EFF7EFE7100C08F7EBDEF7E7DEEFE3D6CE9652FFFFFFFFFFFF00FFFFFFFFFFFF
        DEB284D6AE7BD6AA73D6A66B000000CE9A5ACE9652CE924A100C08CE924ACE96
        4ACE964ACE9652FFFFFFFFFFFF00FFFFFFFFFFFFDEB284FFFFFFFFFFFFFFFBF7
        000000FFF7EFF7F3EFF7EFE7100C08F7EBDEF7E7DEEFE3D6CE9652FFFFFFFFFF
        FF00FFFFFFFFFFFFDEB284D6AE7BD6AA73D6A66B000000CE9A5ACE9652CE924A
        100C08CE924ACE964ACE964ACE9652FFFFFFFFFFFF00FFFFFFFFFFFFDEB284FF
        FFFFFFFFFFFFFBF7000000FFF7EFF7F3EFF7EFE7100C08F7EBDEF7E7DEEFE3D6
        CE9652FFFFFFFFFFFF00FFFFFFFFFFFFDEB284D6AE7BD6AA73D6A66B000000CE
        9A5ACE9652CE924A100C08CE924ACE964ACE964ACE9652FFFFFFFFFFFF00FFFF
        FFFFFFFFDEB284FFFFFFFFFFFFFFFBF7000000FFF7EFF7F3EFF7EFE7100C08F7
        EBDEF7E7DEEFE3D6CE9652FFFFFFFFFFFF00FFFFFFFFFFFFDEB284D6AE7BD6AA
        73D6A66BD69E63CE9A5ACE9652CE924ACE924ACE924ACE964ACE964ACE9652FF
        FFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFF00}
      Hint = #1054#1092#1086#1088#1084#1083#1077#1085#1085#1099#1077' '#1079#1072#1082#1072#1079#1099
      Spacing = 1
      Left = 14
      Top = 4
      Visible = True
      OnClick = acTabsExecute
      SectionName = 'Untitled (0)'
    end
    object SpeedItem2: TSpeedItem
      Action = acExit
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Glyph.Data = {
        5E040000424D5E04000000000000360000002800000012000000130000000100
        18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
        8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
        0000CED3D6848684848684848684848684848684848684848684848684848684
        848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
        7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
        FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
        FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
        007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
        FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
        00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
        494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
        00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000}
      Hint = #1042#1099#1093#1086#1076
      Spacing = 1
      Left = 344
      Top = 4
      Visible = True
      OnClick = acExitExecute
      SectionName = 'Untitled (0)'
    end
  end
  object am: TActionManager
    ActionBars = <
      item
        Items = <
          item
            Items = <
              item
                Action = acExit
                Caption = #1042#1099#1093#1086#1076
                ImageIndex = 10
              end>
            Caption = #1057#1080#1089#1090#1077#1084#1072
          end
          item
            Items = <
              item
                Items = <
                  item
                    Action = acRep1
                    ImageIndex = 9
                  end
                  item
                    Action = acSaleCateg
                  end
                  item
                    Action = acSaleCategShort
                  end
                  item
                    Action = acSaleStation
                  end
                  item
                    Action = AcRealCehPit
                  end
                  item
                    Action = AcCategWater
                  end>
                Caption = #1056#1077#1072#1083#1080#1079#1072#1094#1080#1103' '#1087#1088#1086#1076#1091#1082#1094#1080#1080
              end
              item
                Action = acSaleWaiters
                ImageIndex = 12
              end
              item
                Action = acRealMH
              end
              item
                Action = acSaleMHStation
              end
              item
                Items = <
                  item
                    Action = acCaherShort
                    Caption = #1050#1088#1072#1090#1082#1086
                  end
                  item
                    Action = acCaherShortS
                    Caption = #1050#1088#1072#1090#1082#1086#1057
                  end
                  item
                    Action = acCashReal
                    Caption = #1055#1086#1076#1088#1086#1073#1085#1086
                  end>
                Caption = #1042#1099#1088#1091#1095#1082#1072' '#1087#1086' '#1082#1072#1089#1089#1072#1084
              end
              item
                Action = acCaher
                ImageIndex = 14
              end
              item
                Action = acSaleMH
              end
              item
                Action = acRealPerDay
                ImageIndex = 18
              end
              item
                Caption = '-'
              end
              item
                Action = acHour
                Caption = #1055#1086#1095#1072#1089#1086#1074#1072#1103' '#1089#1090#1072#1090#1080#1089#1090#1080#1082#1072' ('#1074#1099#1088#1091#1095#1082#1072')'
                ImageIndex = 15
              end
              item
                Action = acHourOpenSchet
                ImageIndex = 15
              end
              item
                Action = acWeek
                ImageIndex = 16
              end
              item
                Items = <
                  item
                    Action = AcSaleDay
                  end
                  item
                    Action = AcBalans
                  end>
                Caption = #1058#1086#1074#1072#1088#1086#1086#1073#1086#1088#1086#1090
              end
              item
                Action = AcSkidki
              end
              item
                Action = acSaleWaitersBlud
              end
              item
                Action = acSpisokPersonal
              end
              item
                Items = <
                  item
                    Action = AcPCCli
                  end>
                Caption = #1055#1083#1072#1090#1077#1078#1085#1099#1077' '#1082#1072#1088#1090#1099
              end
              item
                Action = AcSkidkiFull
              end>
            Caption = #1054#1090#1095#1077#1090#1099
          end
          item
            Items = <
              item
                Action = acOutGr
                ImageIndex = 9
              end
              item
                Action = acOutKat
                ImageIndex = 11
              end
              item
                Caption = '-'
              end
              item
                Action = acDel
                ImageIndex = 3
              end
              item
                Caption = '-'
              end
              item
                Action = acRealDay
                ImageIndex = 16
              end>
            Caption = #1056#1072#1089#1093#1086#1076
          end
          item
            Items = <
              item
                Action = acTabs
              end
              item
                Action = acTabsDay
              end>
            Caption = #1047#1072#1082#1072#1079#1099
          end
          item
            Items = <
              item
                Action = acProtocol
              end>
            Caption = #1055#1088#1086#1090#1086#1082#1086#1083
          end>
        ActionBar = ActionMainMenuBar1
      end>
    Images = dmC.imState
    Left = 640
    Top = 80
    StyleName = 'XP Style'
    object acExit: TAction
      Caption = 'acExit'
      OnExecute = acExitExecute
    end
    object acRep1: TAction
      Caption = #1056#1077#1072#1083#1080#1079#1072#1094#1080#1103' '#1087#1086' '#1075#1088#1091#1087#1087#1072#1084
      ImageIndex = 9
      OnExecute = acRep1Execute
    end
    object acCaherShort: TAction
      Caption = #1042#1099#1088#1091#1095#1082#1072' '#1087#1086' '#1082#1072#1089#1089#1072#1084' '#1050#1088#1072#1090#1082#1086
      OnExecute = acCaherShortExecute
    end
    object acCaherShortS: TAction
      Caption = #1042#1099#1088#1091#1095#1082#1072' '#1087#1086' '#1082#1072#1089#1089#1072#1084' '#1050#1088#1072#1090#1082#1086#1057
      OnExecute = acCaherShortSExecute
    end
    object acCashReal: TAction
      Caption = #1042#1099#1088#1091#1095#1082#1072' '#1087#1086' '#1082#1072#1089#1089#1072#1084' '#1055#1086#1076#1088#1086#1073#1085#1086
      OnExecute = acCashRealExecute
    end
    object acSaleWaiters: TAction
      Caption = #1056#1077#1072#1083#1080#1079#1072#1094#1080#1103' '#1087#1086' '#1086#1092#1080#1094#1080#1072#1085#1090#1072#1084' ('#1074#1099#1088#1091#1095#1082#1072')'
      OnExecute = acSaleWaitersExecute
    end
    object acSaleCateg: TAction
      Caption = #1056#1077#1072#1083#1080#1079#1072#1094#1080#1103' '#1087#1086' '#1082#1072#1090#1077#1075#1086#1088#1080#1103#1084' '#1055#1086#1076#1088#1086#1073#1085#1086
      OnExecute = acSaleCategExecute
    end
    object acSaleCategShort: TAction
      Caption = #1056#1077#1072#1083#1080#1079#1072#1094#1080#1103' '#1087#1086' '#1082#1072#1090#1077#1075#1086#1088#1080#1103#1084' '#1050#1088#1072#1090#1082#1086
      OnExecute = acSaleCategShortExecute
    end
    object acSaleStation: TAction
      Caption = #1056#1077#1072#1083#1080#1079#1072#1094#1080#1103' '#1087#1086' '#1089#1090#1072#1085#1094#1080#1103#1084
      OnExecute = acSaleStationExecute
    end
    object acTabs: TAction
      Caption = #1054#1092#1086#1088#1084#1083#1077#1085#1085#1099#1077' '#1079#1072#1082#1072#1079#1099
      OnExecute = acTabsExecute
    end
    object acTabsDay: TAction
      Caption = #1054#1090#1082#1088#1099#1090#1099#1077' '#1079#1072#1082#1072#1079#1099
      OnExecute = acTabsDayExecute
    end
    object acProtocol: TAction
      Caption = #1055#1088#1086#1090#1086#1082#1086#1083' '#1076#1077#1081#1089#1090#1074#1080#1081' '#1087#1077#1088#1089#1086#1085#1072#1083#1072
      ImageIndex = 13
      OnExecute = acProtocolExecute
    end
    object acCaher: TAction
      Caption = #1042#1099#1088#1091#1095#1082#1072' '#1087#1086' '#1082#1072#1089#1089#1080#1088#1072#1084
      ImageIndex = 12
      OnExecute = acCaherExecute
    end
    object acHour: TAction
      Caption = #1055#1086#1095#1072#1089#1086#1074#1072#1103' '#1074#1099#1088#1091#1095#1082#1072'.'
      ImageIndex = 15
      OnExecute = acHourExecute
    end
    object acHourOpenSchet: TAction
      Caption = #1055#1086#1095#1072#1089#1086#1074#1072#1103' '#1074#1099#1088#1091#1095#1082#1072' '#1087#1086' '#1086#1090#1082#1088#1099#1090#1080#1102' '#1089#1095#1077#1090#1072'.'
      ImageIndex = 15
      OnExecute = acHourOpenSchetExecute
    end
    object acWeek: TAction
      Caption = #1042#1099#1088#1091#1095#1082#1072' '#1087#1086#1076#1085#1103#1084' '#1085#1077#1076#1077#1083#1080
      ImageIndex = 16
      OnExecute = acWeekExecute
    end
    object acOutGr: TAction
      Caption = #1056#1072#1089#1093#1086#1076' '#1087#1086' '#1075#1088#1091#1087#1087#1072#1084
      OnExecute = acOutGrExecute
    end
    object acOutKat: TAction
      Caption = #1056#1072#1089#1093#1086#1076' '#1087#1086' '#1082#1072#1090#1077#1075#1086#1088#1080#1103#1084
      OnExecute = acOutKatExecute
    end
    object acDel: TAction
      Caption = #1059#1076#1072#1083#1077#1085#1085#1099#1077
      OnExecute = acDelExecute
    end
    object acRealMH: TAction
      Caption = #1056#1077#1072#1083#1080#1079#1072#1094#1080#1103' '#1087#1086' '#1052#1061' ('#1087#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1072')'
      OnExecute = acRealMHExecute
    end
    object acSaleMH: TAction
      Caption = #1042#1099#1088#1091#1095#1082#1072' '#1087#1086' '#1052#1061' ('#1087#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1072')'
      OnExecute = acSaleMHExecute
    end
    object acRealPerDay: TAction
      Caption = #1056#1077#1072#1083#1080#1079#1072#1094#1080#1103' '#1079#1072' '#1087#1077#1088#1080#1086#1076' '#1076#1085#1103' ('#1074#1099#1088#1091#1095#1082#1072')'
      ImageIndex = 18
      OnExecute = acRealPerDayExecute
    end
    object acRealDay: TAction
      Caption = #1056#1077#1072#1083#1080#1079#1072#1094#1080#1103' '#1087#1086' '#1076#1085#1103#1084
      ImageIndex = 16
      OnExecute = acRealDayExecute
    end
    object acSaleMHStation: TAction
      Caption = #1056#1077#1072#1083#1080#1079#1072#1094#1080#1103' '#1087#1086' '#1075#1088#1091#1087#1087#1072#1084' '#1089#1090#1072#1085#1094#1080#1081
      OnExecute = acSaleMHStationExecute
    end
    object AcSaleDay: TAction
      Caption = #1058#1086#1074#1072#1088#1086#1086#1073#1086#1088#1086#1090' '#1087#1086' '#1076#1085#1103#1084
      OnExecute = AcSaleDayExecute
    end
    object acSaleWaitersBlud: TAction
      Caption = #1055#1088#1086#1076#1072#1078#1080' '#1086#1092#1080#1094#1080#1072#1085#1090#1086#1074
      OnExecute = acSaleWaitersBludExecute
    end
    object acSpisokPersonal: TAction
      Caption = #1057#1087#1080#1089#1086#1082' '#1087#1077#1088#1089#1086#1085#1072#1083#1072
      OnExecute = acSpisokPersonalExecute
    end
    object AcBalans: TAction
      Caption = #1041#1072#1083#1072#1085#1089
      OnExecute = AcBalansExecute
    end
    object AcPCCli: TAction
      Caption = #1056#1077#1072#1083#1080#1079#1072#1094#1080#1103' '#1087#1086' '#1055#1050' '#1087#1086' '#1082#1083#1080#1077#1085#1090#1072#1084
      OnExecute = AcPCCliExecute
    end
    object AcRealCehPit: TAction
      Caption = #1056#1077#1072#1083#1080#1079#1072#1094#1080#1103' '#1062#1077#1093#1086#1074#1086#1077' '#1087#1080#1090#1072#1085#1080#1077
      OnExecute = AcRealCehPitExecute
    end
    object AcSkidki: TAction
      Caption = #1057#1082#1080#1076#1082#1080' '#1080' '#1085#1072#1094#1077#1085#1082#1080
      OnExecute = AcSkidkiExecute
    end
    object AcCategWater: TAction
      Caption = #1050#1072#1090#1077#1075#1086#1088#1080#1080' '#1073#1083#1102#1076' '#1087#1086' '#1086#1092#1080#1094#1080#1072#1085#1090#1072#1084
      OnExecute = AcCategWaterExecute
    end
    object AcSkidkiFull: TAction
      Caption = #1057#1082#1080#1076#1082#1080' '#1087#1086#1083#1085#1099#1081' '#1086#1090#1095#1077#1090
      OnExecute = AcSkidkiFullExecute
    end
  end
  object dsRep1: TfrDBDataSet
    CloseDataSource = True
    DataSet = dmC.quRep1
    Left = 184
    Top = 32
  end
  object Timer1: TTimer
    Interval = 3000
    OnTimer = Timer1Timer
    Left = 560
    Top = 24
  end
  object dsRep2: TfrDBDataSet
    CloseDataSource = True
    DataSet = dmC.quRep2
    Left = 240
    Top = 32
  end
  object frRepMain2: TfrReport
    InitialZoom = pzDefault
    PreviewButtons = [pbZoom, pbLoad, pbSave, pbPrint, pbFind, pbHelp, pbExit]
    RebuildPrinter = False
    Left = 88
    Top = 32
    ReportForm = {19000000}
  end
  object dsRep3: TfrDBDataSet
    CloseDataSource = True
    DataSet = dmC.quRep3
    Left = 288
    Top = 32
  end
  object dsRep4: TfrDBDataSet
    DataSet = TaSW
    Left = 720
    Top = 120
  end
  object dsRep5: TfrDBDataSet
    DataSet = dmC.quRep5
    Left = 408
    Top = 32
  end
  object taHour: TClientDataSet
    Active = True
    Aggregates = <>
    FileName = 'Hours.cds'
    Params = <>
    Left = 576
    Top = 80
    Data = {
      1C0200009619E0BD0100000018000000070007000000030000000E01034E756D
      040001000000000006506572696F640100490000000100055749445448020002
      00140006436865636B7304000100000000000651756573747304000100000000
      00045253756D080004000000010007535542545950450200490006004D6F6E65
      79000450726F630100490000000100055749445448020002001E00055250726F
      63080004000000000001000A4348414E47455F4C4F4704008200150000000100
      0000000000000400000002000000000000000400000003000000000000000400
      0000040000000000000004000000050000000000000004000000060000000000
      000004000000070000000000000004000000040020010000000BCFEEEDE5E4E5
      EBFCEDE8EA0000000000000000000000000000000001000400200200000007C2
      F2EEF0EDE8EA0700000007000000295C8FC2F528BC3F123D3D3D3D3D3D3D3D3D
      3D3D202035322C33380400200300000005D1F0E5E4E002000000020000009A99
      99999999B93F113D3D3D3D3D3D3D3D3D3D202034372C36320400200400000007
      D7E5F2E2E5F0E300000000000000000000000000000000010004002005000000
      07CFFFF2EDE8F6E0000000000000000000000000000000000100040020060000
      0007D1F3E1E1EEF2E00000000000000000000000000000000001000400200700
      00000BC2EEF1EAF0E5F1E5EDFCE5000000000000000000000000000000000100}
    object taHourNum: TIntegerField
      FieldName = 'Num'
    end
    object taHourPeriod: TStringField
      FieldName = 'Period'
    end
    object taHourChecks: TIntegerField
      FieldName = 'Checks'
    end
    object taHourQuests: TIntegerField
      FieldName = 'Quests'
    end
    object taHourRSum: TCurrencyField
      FieldName = 'RSum'
    end
    object taHourProc: TStringField
      FieldName = 'Proc'
      Size = 30
    end
    object taHourRProc: TFloatField
      FieldName = 'RProc'
    end
  end
  object dsHour: TfrDBDataSet
    DataSet = taHour
    Left = 408
    Top = 88
  end
  object dsRep1_: TfrDBDataSet
    CloseDataSource = True
    DataSet = dmC.quRep1_
    Left = 208
    Top = 88
  end
  object dsRep2_: TfrDBDataSet
    CloseDataSource = True
    DataSet = dmC.quRep2_
    Left = 272
    Top = 88
  end
  object dsRepDel: TfrDBDataSet
    DataSet = dmC.quRepDel
    Left = 312
    Top = 88
  end
  object dsRep7: TfrDBDataSet
    CloseDataSource = True
    DataSet = dmC.quRep7
    Left = 472
    Top = 32
  end
  object dsRep8: TfrDBDataSet
    CloseDataSource = True
    DataSet = dmC.quRep8
    Left = 472
    Top = 80
  end
  object taRDT: TClientDataSet
    Aggregates = <>
    FileName = 'RDayTime.cds'
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'taRDTSifr'
        Fields = 'Sifr'
        Options = [ixPrimary, ixUnique]
      end
      item
        Name = 'taRDTName'
        Fields = 'NAMEGR;NAME'
        Options = [ixCaseInsensitive]
      end>
    IndexName = 'taRDTName'
    Params = <>
    StoreDefs = True
    Left = 24
    Top = 96
    object taRDTSifr: TIntegerField
      FieldName = 'Sifr'
    end
    object taRDTNAME: TStringField
      DisplayWidth = 100
      FieldName = 'NAME'
      Size = 100
    end
    object taRDTPARENT: TIntegerField
      FieldName = 'PARENT'
    end
    object taRDTNAMEGR: TStringField
      FieldName = 'NAMEGR'
      Size = 100
    end
    object taRDTSUMQUANT: TFloatField
      FieldName = 'SUMQUANT'
    end
    object taRDTSUMSUM: TFloatField
      FieldName = 'SUMSUM'
    end
    object taRDTSUMDISC: TFloatField
      FieldName = 'SUMDISC'
    end
  end
  object dsRDT: TDataSource
    DataSet = taRDT
    Left = 80
    Top = 96
  end
  object frtaRDT: TfrDBDataSet
    DataSet = taRDT
    OpenDataSource = False
    Left = 136
    Top = 96
  end
  object frRepMain1: TfrReport
    InitialZoom = pzDefault
    PreviewButtons = [pbZoom, pbLoad, pbSave, pbPrint, pbFind, pbHelp, pbExit]
    RebuildPrinter = False
    Left = 136
    Top = 32
    ReportForm = {19000000}
  end
  object dsRep10: TfrDBDataSet
    CloseDataSource = True
    DataSet = dmC.quRep10
    Left = 520
    Top = 112
  end
  object dsRepDay: TfrDBDataSet
    DataSet = taDay
    Left = 352
    Top = 128
  end
  object taDay: TClientDataSet
    Aggregates = <>
    FileName = 'taDay.cds'
    FieldDefs = <
      item
        Name = 'ZNUM'
        DataType = ftInteger
      end
      item
        Name = 'KolCh'
        DataType = ftInteger
      end
      item
        Name = 'Quests'
        DataType = ftInteger
      end
      item
        Name = 'Summa'
        DataType = ftFloat
      end
      item
        Name = 'SUMDISC'
        DataType = ftFloat
      end
      item
        Name = 'Date'
        DataType = ftDate
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 312
    Top = 128
    object taDayZNUM: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'ZNUM'
      Calculated = True
    end
    object taDayKolCh: TIntegerField
      FieldName = 'KolCh'
    end
    object taDayQuests: TIntegerField
      FieldName = 'Quests'
    end
    object taDaySumma: TFloatField
      FieldName = 'Summa'
    end
    object taDaySUMDISC: TFloatField
      FieldName = 'SUMDISC'
    end
    object taDayDate: TDateField
      FieldName = 'Date'
    end
  end
  object dsRep31: TfrDBDataSet
    CloseDataSource = True
    DataSet = dmC.quRep31
    Left = 272
    Top = 56
  end
  object dsRep32: TfrDBDataSet
    CloseDataSource = True
    DataSet = dmC.quRep32
    Left = 312
    Top = 56
  end
  object dsRepSK: TfrDBDataSet
    CloseDataSource = True
    DataSet = dmC.quRepSK
    Left = 208
    Top = 128
  end
  object dsRep33: TfrDBDataSet
    CloseDataSource = True
    DataSet = dmC.quRep33
    Left = 392
    Top = 128
  end
  object dsRep81: TfrDBDataSet
    CloseDataSource = True
    DataSet = dmC.quRep81
    Left = 472
    Top = 112
  end
  object dsRep1_1: TfrDBDataSet
    CloseDataSource = True
    Left = 184
    Top = 64
  end
  object dsRep312: TfrDBDataSet
    CloseDataSource = True
    DataSet = dmC.quRep312
    Left = 256
    Top = 128
  end
  object dsRep313: TfrDBDataSet
    CloseDataSource = True
    DataSet = dmC.quRep313
    Left = 256
    Top = 160
  end
  object taRepKS: TClientDataSet
    Aggregates = <>
    FileName = 'taRepKS.cds'
    FieldDefs = <
      item
        Name = 'CASHNUM'
        DataType = ftInteger
      end
      item
        Name = 'OPERTYPE'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'SUMSUM'
        DataType = ftFloat
      end
      item
        Name = 'QUESTS'
        DataType = ftInteger
      end
      item
        Name = 'KolCheck'
        DataType = ftInteger
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 24
    Top = 160
    object taRepKSCASHNUM: TIntegerField
      FieldName = 'CASHNUM'
    end
    object taRepKSOPERTYPE: TStringField
      FieldName = 'OPERTYPE'
      Size = 10
    end
    object taRepKSSUMSUM: TFloatField
      FieldName = 'SUMSUM'
    end
    object taRepKSQUESTS: TIntegerField
      FieldName = 'QUESTS'
    end
    object taRepKSKolCheck: TIntegerField
      FieldName = 'KolCheck'
    end
  end
  object DSRepKS: TDataSource
    DataSet = taRepKS
    Left = 72
    Top = 160
  end
  object dstaRepKS: TfrDBDataSet
    CloseDataSource = True
    DataSet = taRepKS
    Left = 112
    Top = 160
  end
  object dsRep22_1: TfrDBDataSet
    CloseDataSource = True
    DataSource = DSRep22
    Left = 160
    Top = 168
  end
  object DSRep22: TDataSource
    DataSet = dmC.quRep22
    Left = 160
    Top = 136
  end
  object dsRep32_1: TfrDBDataSet
    CloseDataSource = True
    DataSet = dmC.quRep32_1
    Left = 344
    Top = 64
  end
  object dsRep32_2: TfrDBDataSet
    CloseDataSource = True
    DataSet = dmC.quRep32_2
    Left = 360
    Top = 88
  end
  object dsRep32_3: TfrDBDataSet
    CloseDataSource = True
    DataSet = dmC.quRep32_3
    Left = 376
    Top = 96
  end
  object dsRep2_2: TfrDBDataSet
    CloseDataSource = True
    DataSet = dmC.quRep2_2
    Left = 312
    Top = 168
  end
  object dsSkidki: TfrDBDataSet
    CloseDataSource = True
    DataSet = taSkidki
    Left = 600
    Top = 168
  end
  object taSkidki: TClientDataSet
    Aggregates = <>
    FileName = 'Skidka.cds'
    FieldDefs = <
      item
        Name = 'DISCPERCENT'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'SumD'
        DataType = ftFloat
      end
      item
        Name = 'KOLCH'
        DataType = ftInteger
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 632
    Top = 128
    object taSkidkiDISCPERCENT: TStringField
      FieldName = 'DISCPERCENT'
    end
    object taSkidkiSumD: TFloatField
      FieldName = 'SumD'
    end
    object taSkidkiKOLCH: TIntegerField
      FieldName = 'KOLCH'
    end
  end
  object dsRep82: TfrDBDataSet
    CloseDataSource = True
    DataSet = dmC.quRep82
    Left = 472
    Top = 144
  end
  object dsRep2_3: TfrDBDataSet
    CloseDataSource = True
    DataSet = dmC.quRep2_3
    Left = 352
    Top = 168
  end
  object TaSW: TdxMemData
    Active = True
    Indexes = <>
    SortOptions = []
    Left = 720
    Top = 88
    object TaSWID_PERSONAL: TIntegerField
      FieldName = 'ID_PERSONAL'
    end
    object TaSWCOUNT: TFloatField
      FieldName = 'COUNT'
    end
    object TaSWNAME: TStringField
      FieldName = 'NAME'
      Size = 200
    end
    object TaSWSUMSUM: TFloatField
      FieldName = 'SUMSUM'
    end
    object TaSWSUMQ: TFloatField
      FieldName = 'SUMQ'
    end
  end
end
