unit MainRep;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ToolWin, ActnMan, ActnCtrls, ActnMenus,
  XPStyleActnCtrls, ActnList, ExtCtrls, SpeedBar, FR_Class, FR_DSet,
  FR_DBSet, DB, DBClient, FR_Desgn, frexpimg, frRtfExp, frXMLExl, frOLEExl,
  frHTMExp, FR_E_HTML2, FR_E_CSV, FR_E_RTF, FR_E_TXT, frTXTExp, FR_BarC,
  FR_Chart,Math, dxmdaset;

type
  TfmMainRep = class(TForm)
    StatusBar1: TStatusBar;
    am: TActionManager;
    ActionMainMenuBar1: TActionMainMenuBar;
    SpeedBar1: TSpeedBar;
    acExit: TAction;
    acRep1: TAction;
    dsRep1: TfrDBDataSet;
    Timer1: TTimer;
    acCashReal: TAction;
    acSaleWaiters: TAction;
    acSaleCateg: TAction;
    dsRep2: TfrDBDataSet;
    frRepMain2: TfrReport;
    dsRep3: TfrDBDataSet;
    dsRep4: TfrDBDataSet;
    acTabs: TAction;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    acProtocol: TAction;
    acCaher: TAction;
    dsRep5: TfrDBDataSet;
    acHour: TAction;
    taHour: TClientDataSet;
    taHourNum: TIntegerField;
    taHourPeriod: TStringField;
    taHourChecks: TIntegerField;
    taHourQuests: TIntegerField;
    taHourRSum: TCurrencyField;
    taHourProc: TStringField;
    taHourRProc: TFloatField;
    dsHour: TfrDBDataSet;
    acWeek: TAction;
    acOutGr: TAction;
    acOutKat: TAction;
    acDel: TAction;
    dsRep1_: TfrDBDataSet;
    dsRep2_: TfrDBDataSet;
    dsRepDel: TfrDBDataSet;
    acRealMH: TAction;
    dsRep7: TfrDBDataSet;
    acSaleMH: TAction;
    dsRep8: TfrDBDataSet;
    acRealPerDay: TAction;
    taRDT: TClientDataSet;
    dsRDT: TDataSource;
    frtaRDT: TfrDBDataSet;
    acRealDay: TAction;
    SpeedItem2: TSpeedItem;
    frRepMain1: TfrReport;
    acSaleMHStation: TAction;
    dsRep10: TfrDBDataSet;
    AcSaleDay: TAction;
    dsRepDay: TfrDBDataSet;
    taDay: TClientDataSet;
    taDayZNUM: TIntegerField;
    taDayKolCh: TIntegerField;
    taDayQuests: TIntegerField;
    taDaySumma: TFloatField;
    taDaySUMDISC: TFloatField;
    taDayDate: TDateField;
    acCaherShort: TAction;
    dsRep31: TfrDBDataSet;
    dsRep32: TfrDBDataSet;
    acSaleWaitersBlud: TAction;
    acSpisokPersonal: TAction;
    dsRepSK: TfrDBDataSet;
    acCaherShortS: TAction;
    dsRep33: TfrDBDataSet;
    dsRep81: TfrDBDataSet;
    AcBalans: TAction;
    dsRep1_1: TfrDBDataSet;
    dsRep312: TfrDBDataSet;
    dsRep313: TfrDBDataSet;
    acTabsDay: TAction;
    taRepKS: TClientDataSet;
    DSRepKS: TDataSource;
    taRepKSCASHNUM: TIntegerField;
    taRepKSOPERTYPE: TStringField;
    taRepKSSUMSUM: TFloatField;
    taRDTSifr: TIntegerField;
    taRDTNAME: TStringField;
    taRDTPARENT: TIntegerField;
    taRDTNAMEGR: TStringField;
    taRDTSUMQUANT: TFloatField;
    taRDTSUMSUM: TFloatField;
    taRDTSUMDISC: TFloatField;
    dstaRepKS: TfrDBDataSet;
    acSaleStation: TAction;
    dsRep22_1: TfrDBDataSet;
    DSRep22: TDataSource;
    dsRep32_1: TfrDBDataSet;
    AcPCCli: TAction;
    AcRealCehPit: TAction;
    taRepKSQUESTS: TIntegerField;
    taRepKSKolCheck: TIntegerField;
    dsRep32_2: TfrDBDataSet;
    dsRep32_3: TfrDBDataSet;
    acSaleCategShort: TAction;
    dsRep2_2: TfrDBDataSet;
    AcSkidki: TAction;
    dsSkidki: TfrDBDataSet;
    taSkidki: TClientDataSet;
    taSkidkiDISCPERCENT: TStringField;
    taSkidkiSumD: TFloatField;
    taSkidkiKOLCH: TIntegerField;
    dsRep82: TfrDBDataSet;
    AcCategWater: TAction;
    dsRep2_3: TfrDBDataSet;
    acHourOpenSchet: TAction;
    AcSkidkiFull: TAction;
    TaSW: TdxMemData;
    TaSWID_PERSONAL: TIntegerField;
    TaSWCOUNT: TFloatField;
    TaSWNAME: TStringField;
    TaSWSUMSUM: TFloatField;
    TaSWSUMQ: TFloatField;
    procedure FormCanResize(Sender: TObject; var NewWidth,
      NewHeight: Integer; var Resize: Boolean);
    procedure FormShow(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure acRep1Execute(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure acCashRealExecute(Sender: TObject);
    procedure acSaleWaitersExecute(Sender: TObject);
    procedure acSaleCategExecute(Sender: TObject);
    procedure acTabsExecute(Sender: TObject);
    procedure acProtocolExecute(Sender: TObject);
    procedure acCaherExecute(Sender: TObject);
    procedure acHourExecute(Sender: TObject);
    procedure acWeekExecute(Sender: TObject);
    procedure acOutGrExecute(Sender: TObject);
    procedure acOutKatExecute(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure acRealMHExecute(Sender: TObject);
    procedure acSaleMHExecute(Sender: TObject);
    procedure acRealPerDayExecute(Sender: TObject);
    procedure acRealDayExecute(Sender: TObject);
    procedure acSaleMHStationExecute(Sender: TObject);
    procedure AcSaleDayExecute(Sender: TObject);
    procedure acCaherShortExecute(Sender: TObject);
    procedure acSaleWaitersBludExecute(Sender: TObject);
    procedure acSpisokPersonalExecute(Sender: TObject);
    procedure acCaherShortSExecute(Sender: TObject);
    procedure AcBalansExecute(Sender: TObject);
    procedure acTabsDayExecute(Sender: TObject);
    procedure acSaleStationExecute(Sender: TObject);
    procedure AcPCCliExecute(Sender: TObject);
    procedure AcRealCehPitExecute(Sender: TObject);
    procedure acSaleCategShortExecute(Sender: TObject);
    procedure AcSkidkiExecute(Sender: TObject);
    procedure AcCategWaterExecute(Sender: TObject);
    procedure acHourOpenSchetExecute(Sender: TObject);
    procedure AcSkidkiFullExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmMainRep: TfmMainRep;

implementation

uses Dm, Un1, Passw, Period, Tabs, LogSpec, Period2, RepGridDayTime,
  RepRealDays, Period4,RepWatersBlud,cxDBCheckListBox,cxCheckBox,
  cxCheckListBox,cxLookAndFeelPainters,
  TabsDay, RepPCCliRep, RepSkidkiFull;

{$R *.dfm}

procedure TfmMainRep.FormCanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
  NewHeight:=125;
end;

procedure TfmMainRep.FormShow(Sender: TObject);
begin
  Left:=0;
  Top:=0;
  Width:=1024;

  with dmC do
  begin
    quPer.Active:=False;
    quPer.SelectSQL.Clear;
    quPer.SelectSQL.Add('select * from rpersonal');
    quPer.SelectSQL.Add('where uvolnen=1 and modul4=0');
    quPer.SelectSQL.Add('order by Name');
//    quPer.Active:=True; ����������� �� show fmPerA
  end;
  fmPerA:=TfmPerA.Create(Application);
  fmPerA.ShowModal;
  if fmPerA.ModalResult=mrOk then
  begin
    Caption:=Caption+'   : '+Person.Name;
    WriteIni; //�������� �������� �������
    fmPerA.Release;
  end
  else
  begin
    fmPerA.Release;
    delay(100);
    close;
    delay(100);
  end;
end;

procedure TfmMainRep.acExitExecute(Sender: TObject);
begin
  close;
end;

procedure TfmMainRep.FormCreate(Sender: TObject);
begin
  CurDir := ExtractFilePath(ParamStr(0));
  ReadIni;
  StrPre:='';
  TrebSel.DateFrom:=Trunc(Date) + StrtoTimeDef(CommonSet.ZTimeShift,0);
  TrebSel.DateTo:=Trunc(Date)+1 + StrtoTimeDef(CommonSet.ZTimeShift,0);
end;

procedure TfmMainRep.acRep1Execute(Sender: TObject);
Var DateBeg,DateEnd:TDateTime;
var I: Integer;
Var PARENTSP: String;
var ArrayParent:Array[1..500] of Integer;
//var StateCheckBox: TcxCheckBoxState;
begin

// ���������� �� �������
  if not CanDo('SaleGroup') then  StatusBar1.Panels[0].Text:='��� ����.';

  PARENTSP:='';

  fmPeriod4:=TfmPeriod4.Create(Application);
 // ������ ����� ����
  dmC.quMenuGR.Active:=False;
  dmC.quMenuGR.Active:=True;
  fmPeriod4.cxCheckListBox1.Items.Clear;
  dmC.quMenuGR.First;
  i:=1;
  while not dmC.quMenuGR.Eof do
  begin
    with fmPeriod4.cxCheckListBox1.Items.Add do
     begin
      Text := dmC.quMenuGRNAME.AsString;
      ArrayParent[i]:=dmC.quMenuGRSIFR.AsInteger;
 //     Enabled := False;
 //     State := cbsChecked;
      i:=i+ 1;
     end;
     dmC.quMenuGR.Next;
  end;

  dmC.quMenuGR.Active:=False;

    fmPeriod4.ShowModal;
    if fmPeriod4.ModalResult=mrOk then
    begin
     for i:=0 to fmPeriod4.cxCheckListBox1.Count-1 do
     //cbsChecked
     if fmPeriod4.cxCheckListBox1.Items[i].State = cbsChecked then
      begin
       if PARENTSP<> '' then PARENTSP:=PARENTSP+',';
       PARENTSP:=PARENTSP+IntToStr(ArrayParent[i+1]);
      end;

     with dmC do
     begin

      quMenuGR.Active:=False;

      Datebeg:=TrebSel.DateFrom;
      DateEnd:=TrebSel.DateTo;

      quRep1.Active:=False;

// ��������� SQL ������
      with quRep1.SQLs.SelectSQL do
      begin
        Clear;
        Append('SELECT');
        Append('b.SIFR,M.CODE,M.NAME,M.PARENT,MM.NAME AS NAMEGR,SUM(b.QUANTITY) AS SUMQUANT, SUM(b.SUMMA) AS SUMSUM, SUM(b.DISCOUNTSUM) AS SUMDISC');
        Append('FROM');
        Append('SPEC_ALL b');
        Append('LEFT JOIN MENU M ON M.SIFR=B.SIFR');
        Append('LEFT JOIN MENU MM ON M.PARENT=MM.SIFR');
        Append('LEFT JOIN TABLES_ALL T ON T.ID=b.ID_TAB');

        Append('LEFT JOIN PLATCARD PC ON T.DISCONT1=PC.BARCODE');
        Append('LEFT JOIN PLATTYPE PT ON PT.ID=PC.PLATTYPE');

        Append('WHERE T.ENDTIME>=:DateB AND T.ENDTIME<=:DATEE');
        Append('AND b.ITYPE=0');
        Append('AND b.ISTATUS=1');
        Append('AND T.SKLAD=0');
//        Append('and ((T.OPERTYPE like ''Sale%'')  or (T.OPERTYPE like ''Ret%''))');
        Append('and (((T.OPERTYPE like ''Sale%'') and (T.OPERTYPE <> ''SalePC''))  or (T.OPERTYPE like ''Ret%'') or ((T.OPERTYPE = ''SalePC'') and (PT.CEHPIT = 0)))');

        if PARENTSP<> '' then
        begin
         Append('AND M.PARENT IN ('+PARENTSP+')');
        end;

        Append('GROUP BY b.SIFR,M.CODE,M.NAME,M.PARENT,MM.NAME');
        Append('ORDER BY MM.NAME,M.NAME');

      end;
// ������ ������
      quRep1.ParamByName('DateB').AsDateTime:=Datebeg;
      quRep1.ParamByName('DateE').AsDateTime:=DateEnd;
      frRepMain1.LoadFromFile(CurDir + 'SaleGroup.frf');

      frVariables.Variable['sPeriod']:='c '+FormatDateTime('dd.mm.yyyy hh.nn',DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy hh.nn',DateEnd);
      frVariables.Variable['Depart']:=CommonSet.DepartName;
      frVariables.Variable['CountRec']:=IntToStr(quRep1.RecordCount);
      frVariables.Variable['NameRep']:='���������� �� �������.';

      frRepMain1.ReportName:='���������� �� �������. ('+IntToStr(quRep1.RecordCount)+')';
      frRepMain1.PrepareReport;
      frRepMain1.ShowPreparedReport;
//      quRep1.Active:=False;
     end;
    end;
  fmPeriod4.Release;
 
end;

procedure TfmMainRep.acSaleCategExecute(Sender: TObject);
Var DateBeg,DateEnd:TDateTime;
begin
// ���������� �� ��������� ��������
  if not CanDo('SaleCateg') then  StatusBar1.Panels[0].Text:='��� ����.';
  fmPeriod:=TfmPeriod.Create(Application);

  fmPeriod.ShowModal;
  if fmPeriod.ModalResult=mrOk then
  begin
    Datebeg:=TrebSel.DateFrom;
    DateEnd:=TrebSel.DateTo;

    with dmC do
    begin
      quRep2.Active:=False;
// ��������� SQL ������
      with quRep2.SQLs.SelectSQL do
      begin
        Clear;
        Append('SELECT');
        Append('b.SIFR,M.CODE,M.NAME,C.NAME AS NAMECA,SUM(b.QUANTITY) AS SUMQUANT, SUM(b.SUMMA) AS SUMSUM, SUM(b.DISCOUNTSUM) AS SUMDISC');
        Append('FROM');
        Append('SPEC_ALL b');
        Append('LEFT JOIN MENU M ON M.SIFR=B.SIFR');
        Append('LEFT JOIN CATEGORY C ON M.CATEG=C.SIFR');
        Append('LEFT JOIN TABLES_ALL T ON T.ID=b.ID_TAB');

        Append('LEFT JOIN PLATCARD PC ON T.DISCONT1=PC.BARCODE');
        Append('LEFT JOIN PLATTYPE PT ON PT.ID=PC.PLATTYPE');

        Append('WHERE T.ENDTIME>=:DateB AND T.ENDTIME<=:DATEE');
        Append('AND b.ITYPE=0');
        Append('AND b.ISTATUS=1');
        Append('AND T.SKLAD=0');
        Append('and (((T.OPERTYPE like ''Sale%'') and (T.OPERTYPE <> ''SalePC''))  or (T.OPERTYPE like ''Ret%'') or ((T.OPERTYPE = ''SalePC'') and (PT.CEHPIT = 0)))');

        Append('GROUP BY b.SIFR,M.CODE,M.NAME,C.NAME');
        Append('ORDER BY C.NAME,M.NAME');

      end;
// ������ ������

      quRep2.ParamByName('DateB').AsDateTime:=Datebeg;
      quRep2.ParamByName('DateE').AsDateTime:=DateEnd;

      frRepMain2.LoadFromFile(CurDir + 'SaleCateg.frf');

      frVariables.Variable['sPeriod']:='c '+FormatDateTime('dd.mm.yyyy hh.nn',DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy hh.nn',DateEnd);
      frVariables.Variable['Depart']:=CommonSet.DepartName;

      frRepMain2.ReportName:='���������� �� ����������.';
      frRepMain2.PrepareReport;
      frRepMain2.ShowPreparedReport;
    end;
  end;
  fmPeriod.Release;
end;

procedure TfmMainRep.acSaleCategShortExecute(Sender: TObject);
Var DateBeg,DateEnd:TDateTime;
begin
// ���������� �� ��������� ������
  if not CanDo('SaleCateg') then  StatusBar1.Panels[0].Text:='��� ����.';

  fmPeriod:=TfmPeriod.Create(Application);

  fmPeriod.ShowModal;
  if fmPeriod.ModalResult=mrOk then
  begin
    Datebeg:=TrebSel.DateFrom;
    DateEnd:=TrebSel.DateTo;

    with dmC do
    begin
      quRep2_2.Active:=False;
// ��������� SQL ������
      with quRep2_2.SQLs.SelectSQL do
      begin
        Clear;
        Append('SELECT');
        Append('G.NAME AS NAMECA,SUM(b.QUANTITY) AS SUMQUANT, SUM(b.SUMMA) AS SUMSUM, SUM(b.DISCOUNTSUM) AS SUMDISC');
        Append('FROM');
        Append('SPEC_ALL b');
        Append('LEFT JOIN MENU M ON M.SIFR=B.SIFR');
        Append('LEFT JOIN CATEGORY G ON M.CATEG=G.SIFR');
        Append('LEFT JOIN TABLES_ALL T ON T.ID=b.ID_TAB');

        Append('LEFT JOIN PLATCARD PC ON T.DISCONT1=PC.BARCODE');
        Append('LEFT JOIN PLATTYPE PT ON PT.ID=PC.PLATTYPE');

        Append('WHERE T.ENDTIME>=:DateB AND T.ENDTIME<=:DATEE');
        Append('AND b.ITYPE=0');
        Append('AND b.ISTATUS=1');
        Append('AND T.SKLAD=0');
        Append('and (((T.OPERTYPE like ''Sale%'') and (T.OPERTYPE <> ''SalePC''))  or (T.OPERTYPE like ''Ret%'') or ((T.OPERTYPE = ''SalePC'') and (PT.CEHPIT = 0)))');

        Append('GROUP BY G.NAME');
        Append('ORDER BY G.NAME');

      end;
// ������ ������

      quRep2_2.ParamByName('DateB').AsDateTime:=Datebeg;
      quRep2_2.ParamByName('DateE').AsDateTime:=DateEnd;

      frRepMain2.LoadFromFile(CurDir + 'SaleCategShort.frf');

      frVariables.Variable['sPeriod']:='c '+FormatDateTime('dd.mm.yyyy hh.nn',DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy hh.nn',DateEnd);
      frVariables.Variable['Depart']:=CommonSet.DepartName;

      frRepMain2.ReportName:='����� ���������.';
      frRepMain2.PrepareReport;
      frRepMain2.ShowPreparedReport;
    end;
  end;
  fmPeriod.Release;
end;

procedure TfmMainRep.acSaleStationExecute(Sender: TObject);
Var DateBeg,DateEnd:TDateTime;
begin
// ���������� �� ��������
  if not CanDo('SaleStation') then  StatusBar1.Panels[0].Text:='��� ����.';
  fmPeriod:=TfmPeriod.Create(Application);

  fmPeriod.ShowModal;
  if fmPeriod.ModalResult=mrOk then
  begin
    Datebeg:=TrebSel.DateFrom;
    DateEnd:=TrebSel.DateTo;

    with dmC do
    begin
      quRep22.Active:=False;
      quRep22.ParamByName('DateB').AsDateTime:=Datebeg;
      quRep22.ParamByName('DateE').AsDateTime:=DateEnd;

      frRepMain2.LoadFromFile(CurDir + 'SaleStation.frf');

      frVariables.Variable['sPeriod']:='c '+FormatDateTime('dd.mm.yyyy hh.nn',DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy hh.nn',DateEnd);
      frVariables.Variable['Depart']:=CommonSet.DepartName;

      frRepMain2.ReportName:='���������� �� ��������.';
      frRepMain2.PrepareReport;
      frRepMain2.ShowPreparedReport;
    end;
  end;
  fmPeriod.Release;

end;

procedure TfmMainRep.AcRealCehPitExecute(Sender: TObject);
Var DateBeg,DateEnd:TDateTime;
begin
// ���������� ������� �������
  if not CanDo('SaleCehPit') then  StatusBar1.Panels[0].Text:='��� ����.';
  fmPeriod:=TfmPeriod.Create(Application);

  fmPeriod.ShowModal;
  if fmPeriod.ModalResult=mrOk then
  begin
    Datebeg:=TrebSel.DateFrom;
    DateEnd:=TrebSel.DateTo;

    with dmC do
    begin
      quRep1.Active:=False;
// ��������� SQL ������
      with quRep1.SQLs.SelectSQL do
      begin
        Clear;

        Append('SELECT');
        Append('b.SIFR,M.CODE,M.NAME,M.PARENT,MM.NAME AS NAMEGR,SUM(b.QUANTITY) AS SUMQUANT, SUM(b.SUMMA) AS SUMSUM, SUM(b.DISCOUNTSUM) AS SUMDISC');
        Append('FROM');
        Append('SPEC_ALL b');
        Append('LEFT JOIN MENU M ON M.SIFR=B.SIFR');
        Append('LEFT JOIN MENU MM ON M.PARENT=MM.SIFR');
        Append('LEFT JOIN TABLES_ALL T ON T.ID=b.ID_TAB');

        Append('LEFT JOIN PLATCARD PC ON T.DISCONT1=PC.BARCODE');
        Append('LEFT JOIN PLATTYPE PT ON PT.ID=PC.PLATTYPE');

        Append('WHERE T.ENDTIME>=:DateB AND T.ENDTIME<=:DATEE');
        Append('AND b.ITYPE=0');
        Append('AND b.ISTATUS=1');
        Append('AND T.SKLAD=0');
        Append('and ((T.OPERTYPE = ''SalePC'') and (PT.CEHPIT = 1))');

        Append('GROUP BY b.SIFR,M.CODE,M.NAME,M.PARENT,MM.NAME');
        Append('ORDER BY MM.NAME,M.NAME');

      end;
// ������ ������

      quRep1.ParamByName('DateB').AsDateTime:=Datebeg;
      quRep1.ParamByName('DateE').AsDateTime:=DateEnd;

      frRepMain2.LoadFromFile(CurDir + 'SaleGroup.frf');

      frVariables.Variable['sPeriod']:='c '+FormatDateTime('dd.mm.yyyy hh.nn',DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy hh.nn',DateEnd);
      frVariables.Variable['Depart']:=CommonSet.DepartName;
      frVariables.Variable['NameRep']:='���������� ������� �������.';


      frRepMain2.ReportName:='���������� ������� �������.';
      frRepMain2.PrepareReport;
      frRepMain2.ShowPreparedReport;
    end;
  end;
  fmPeriod.Release;

end;


procedure TfmMainRep.Timer1Timer(Sender: TObject);
begin
  if bClearStatusBar=True then begin StatusBar1.Panels[0].Text:=''; Delay(10); bClearStatusBar:=False end;
  if StatusBar1.Panels[0].Text>'' then bClearStatusBar:=True;
end;

procedure TfmMainRep.acCaherShortExecute(Sender: TObject);
Var DateBeg,DateEnd:TDateTime;
begin
// ������� �� ������ ������
  if not CanDo('CashReal') then  StatusBar1.Panels[0].Text:='��� ����.';
  fmPeriod:=TfmPeriod.Create(Application);

  fmPeriod.ShowModal;
  if fmPeriod.ModalResult=mrOk then
  begin
    Datebeg:=TrebSel.DateFrom;
    DateEnd:=TrebSel.DateTo;

    with dmC do
    begin
    // ����� ���������� �����
      quRep32.Active:=False;
      quRep32.ParamByName('DateB').AsDateTime:=Datebeg;
      quRep32.ParamByName('DateE').AsDateTime:=DateEnd;

    // ����� �� ��������
      quRep31.Active:=False;
      quRep31.ParamByName('DateB').AsDateTime:=Datebeg;
      quRep31.ParamByName('DateE').AsDateTime:=DateEnd;

    // ������� ���������� ����� � ������ �� ����� �����
      quRep32_1.Active:=False;
      quRep32_1.ParamByName('DateB').AsDateTime:=Datebeg;
      quRep32_1.ParamByName('DateE').AsDateTime:=DateEnd;


      frRepMain2.LoadFromFile(CurDir + 'SaleCashShort.frf');

      frVariables.Variable['sPeriod']:='c '+FormatDateTime('dd.mm.yyyy hh.nn',DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy hh.nn',DateEnd);
      frVariables.Variable['Depart']:=CommonSet.DepartName;

      frRepMain2.ReportName:='���������� �� ������.';
      frRepMain2.PrepareReport;
      frRepMain2.ShowPreparedReport;
    end;
  end;
  fmPeriod.Release;

end;


procedure TfmMainRep.acCashRealExecute(Sender: TObject);
Var DateBeg,DateEnd:TDateTime;
    Bnal,nal: Real;
begin
// ������� �� ������
  if not CanDo('CashReal') then  StatusBar1.Panels[0].Text:='��� ����.';
  fmPeriod:=TfmPeriod.Create(Application);

  fmPeriod.ShowModal;
  if fmPeriod.ModalResult=mrOk then
  begin
    Datebeg:=TrebSel.DateFrom;
    DateEnd:=TrebSel.DateTo;

    with dmC do
    begin
      quRep3.Active:=False;
      quRep3.ParamByName('DateB').AsDateTime:=Datebeg;
      quRep3.ParamByName('DateE').AsDateTime:=DateEnd;

      quRep32.Active:=False;
      quRep32.ParamByName('DateB').AsDateTime:=Datebeg;
      quRep32.ParamByName('DateE').AsDateTime:=DateEnd;
      
      //��������� ����� ��� � ������
      quRep3.Active:=True;
      nal  := 0;
      Bnal := 0;
      quRep3.First;
      while not quRep3.Eof do
      begin
        if quRep3PAYTYPE.Value = 0 then
          nal  := nal + quRep3SUMSUM.Value
        else
          Bnal  := Bnal + quRep3SUMSUM.Value;

        quRep3.Next;
      end;
      quRep3.Active:=False;

 //
      frRepMain2.LoadFromFile(CurDir + 'SaleCash.frf');

      frVariables.Variable['sPeriod']:='c '+FormatDateTime('dd.mm.yyyy hh.nn',DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy hh.nn',DateEnd);
      frVariables.Variable['Depart']:=CommonSet.DepartName;
      frVariables.Variable['nal']:=nal;
      frVariables.Variable['Bnal']:=Bnal;

      frRepMain2.ReportName:='���������� �� ������.';
      frRepMain2.PrepareReport;
      frRepMain2.ShowPreparedReport;
    end;
  end;
  fmPeriod.Release;
end;

procedure TfmMainRep.acSaleWaitersExecute(Sender: TObject);
Var DateBeg,DateEnd:TDateTime;
    rSum,rSumSt:Real;
    StrWk,StrWk1:String;
begin
// ���������� �� ����������
  if not CanDo('SaleWaiter') then  StatusBar1.Panels[0].Text:='��� ����.';
  fmPeriod:=TfmPeriod.Create(Application); //��� ���� ������������

  fmPeriod.ShowModal;
  if fmPeriod.ModalResult=mrOk then
  begin
    Datebeg:=TrebSel.DateFrom;
    DateEnd:=TrebSel.DateTo;

    with dmC do
    begin
      quRep4.Active:=False;
      quRep4.ParamByName('DateB').AsDateTime:=Datebeg;
      quRep4.ParamByName('DateE').AsDateTime:=DateEnd;
      quRep4.Active:=True;

      quRep41.Active:=False;
      quRep41.ParamByName('DateB').AsDateTime:=Datebeg;
      quRep41.ParamByName('DateE').AsDateTime:=DateEnd;
      quRep41.Active:=True;

      quRep4.First;
      while not quRep4.eof do
      begin
        TaSW.Append;
        TaSWID_PERSONAL.AsInteger:=quRep4ID_PERSONAL.AsInteger;
        TaSWNAME.AsString:=quRep4NAME.AsString;
        TaSWCOUNT.AsFloat:=quRep4COUNT.AsFloat;
        TaSWSUMQ.AsFloat:=quRep4SUMQ.AsFloat;
        TaSWSUMSUM.AsFloat:=0;
        if quRep41.Locate('ID_PERSONAL',quRep4ID_PERSONAL.AsInteger,[]) then
         begin
          TaSWSUMSUM.AsFloat:=quRep41SUMSUM.AsFloat;
         end;

        TaSW.Post;
        quRep4.Next;
     end;

      if fmPeriod.cxCheckBox1.Checked then
      begin //�������� �� �������� �������
        try
          quRep4.Active:=True;

          prOpenDevPrint(CommonSet.PrePrintPort);

          SelFont(13);
          PrintStr(' '+CommonSet.DepartName);
          PrintStr('');

          SelFont(14);
          PrintStr('���������� �� ����������.');
          SelFont(13);
          PrintStr('');
          PrintStr('�� ������ � '+FormatDateTime('dd.mm.yyyy hh:mm',Datebeg));
          PrintStr('�� '+FormatDateTime('dd.mm.yyyy hh:mm',DateEnd));

          SelFont(13);
          StrWk:='                                          ';
          PrintStr(StrWk);
          SelFont(15);
          StrWk:=' ��������               �����     ������  ';
            //       1234 �������� ������ 18 999.999 9999.99
          PrintStr(StrWk);
          SelFont(13);
          StrWk:='                                          ';
          PrintStr(StrWk);

          rSum:=0; //����� ������
          rSumSt:=0;  //����� ������
          SelFont(15);

          quRep4.First;
          while not quRep4.Eof do
          begin
            rSum:=rSum+quRep4SUMSUM.AsFloat;
            rSumSt:=rSumSt+quRep4SUMQ.AsFloat;
            //��������
            StrWk1:=Copy(quRep4NAME.AsString,1,25);
            while Length(StrWk1)<25 do StrWk1:=StrWk1+' ';
            StrWk:=StrWk+StrWk1+' ';
            //�����
            Str(quRep4SUMSUM.AsFloat:9:2,StrWk1);
            while Length(StrWk1)<9 do StrWk1:=StrWk1+' ';
            StrWk:=StrWk+StrWk1+' ';
            //������
            Str(quRep4SUMQ.AsFloat:5,StrWk1);
            while Length(StrWk1)<5 do StrWk1:=StrWk1+' ';
            StrWk:=StrWk+StrWk1;

            PrintStr(StrWk);

            quRep4.Next;
          end;

          PrintStr('');
          SelFont(13);
          Str(rSumSt:8:2,StrWk);
          Str(rSum:8:2,StrWk1);

          SelFont(13);
          StrWk:='                                          ';
          PrintStr(StrWk);

          StrWk:='�����               '+StrWk1+' '+StrWk;
          PrintStr(StrWk);
          PrintStr('');

          CutDocPr;
        finally
          quRep4.Active:=False;
          Delay(100);
          DevPrint.Close;
        end;

      end else
      begin //�������� �� �������

        frRepMain1.LoadFromFile(CurDir + 'SaleWaiters.frf');

        frVariables.Variable['sPeriod']:='c '+FormatDateTime('dd.mm.yyyy hh.nn',DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy hh.nn',DateEnd);
        frVariables.Variable['Depart']:=CommonSet.DepartName;

        frRepMain1.ReportName:='���������� �� ����������.';
        frRepMain1.PrepareReport;
        frRepMain1.ShowPreparedReport;
      end;
    end;
  end;
  fmPeriod.Release;
end;

procedure TfmMainRep.acTabsExecute(Sender: TObject);
begin
  with dmC do
  begin
    taTabsAllSel.Active:=False;
    taTabsAllSel.ParamByName('DBEG').AsDateTime:=TrebSel.DateFrom;
    taTabsAllSel.ParamByName('DEND').AsDateTime:=TrebSel.DateTo;
    taTabsAllSel.Active:=True;
    taTabsAllSel.Last;

    quSelSpecAll.Active:=False;
    quSelSpecAll.ParamByName('DBEG').AsDateTime:=TrebSel.DateFrom;
    quSelSpecAll.ParamByName('DEND').AsDateTime:=TrebSel.DateTo;
    quSelSpecAll.Active:=True;
  end;
  fmTabs.Caption:='����������� ������ �� ������ � '+FormatDateTime('dd.mm.yyyy hh.nn',TrebSel.DateFrom)+' �� '+FormatDateTime('dd.mm.yyyy hh.nn',TrebSel.DateTo);
  fmTabs.show;
end;

procedure TfmMainRep.acProtocolExecute(Sender: TObject);
Var DateBeg,DateEnd:TDateTime;
begin
// �������� ��������
  if not CanDo('SaleWaiter') then  StatusBar1.Panels[0].Text:='��� ����.';
  fmPeriod:=TfmPeriod.Create(Application);

  fmPeriod.ShowModal;
  if fmPeriod.ModalResult=mrOk then
  begin
    Datebeg:=TrebSel.DateFrom;
    DateEnd:=TrebSel.DateTo;

    with dmC do
    begin //������ ��������� ����������
//����� ������ ���������
      quPersonal2.Active:=False;
      quPersonal2.Active:=True;

//����� ������ ����
      quMenuSel.Active:=False;
      quMenuSel.Active:=True;

//
      quRepLog.Active:=False;
//      quRepLog.ParamByName('DATEB').AsString:=FormatDateTime('mm/dd/yyyy hh:nn',DateBeg);
//      quRepLog.ParamByName('DATEE').AsString:=FormatDateTime('mm/dd/yyyy hh:nn',DateEnd);
      quRepLog.ParamByName('DATEB').AsDateTime:=DateBeg;
      quRepLog.ParamByName('DATEE').AsDateTime:=DateEnd;
      quRepLog.Active:=True;



      fmLogSpec.DateFrom:= Datebeg;      //���
      fmLogSpec.DateTo:= DateEnd;        //���
      fmLogSpec.Show;


    end;
  end;
  fmPeriod.Release;
end;

procedure TfmMainRep.acCaherExecute(Sender: TObject);
Var DateBeg,DateEnd:TDateTime;
begin
// ���������� �� ��������
  if not CanDo('SaleCasher') then  StatusBar1.Panels[0].Text:='��� ����.';
  fmPeriod:=TfmPeriod.Create(Application);
  fmPeriod.ShowModal;
  if fmPeriod.ModalResult=mrOk then
  begin
    Datebeg:=TrebSel.DateFrom;
    DateEnd:=TrebSel.DateTo;

    with dmC do
    begin
      quRep5.Active:=False;
      quRep5.ParamByName('DateB').AsDateTime:=Datebeg;
      quRep5.ParamByName('DateE').AsDateTime:=DateEnd;

      frRepMain1.LoadFromFile(CurDir + 'SaleCashers.frf');

      frVariables.Variable['sPeriod']:='c '+FormatDateTime('dd.mm.yyyy hh.nn',DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy hh.nn',DateEnd);
      frVariables.Variable['Depart']:=CommonSet.DepartName;

      frRepMain1.ReportName:='���������� �� ��������.';
      frRepMain1.PrepareReport;
      frRepMain1.ShowPreparedReport;
    end;
  end;
  fmPeriod.Release;
end;

procedure TfmMainRep.acHourExecute(Sender: TObject);
type THStatREc = record
     Checks,Quests:INteger;
     rSum:Currency;
     end;

Var DateBeg,DateEnd:TDateTime;
    HStat:Array[1..24] of THStatrec;
    n:Integer;
    rSumAll:Currency;
    p,i:Real;
    StrWk:String;


Function CalcTimeToH(tt:TDateTime):ShortInt;
begin
  tt:=Frac(tt);
  Result:=StrToINtDef(FormatDateTime('hh',tt),0);
end;

begin
  //��������� ������� - �������
  if not CanDo('SaleCasher') then  StatusBar1.Panels[0].Text:='��� ����.';
  fmPeriod:=TfmPeriod.Create(Application);
  fmPeriod.ShowModal;
  if fmPeriod.ModalResult=mrOk then
  begin
    Datebeg:=TrebSel.DateFrom;
    DateEnd:=TrebSel.DateTo;

    with dmC do
    begin
      //������� ������������� ������
      for n:=1 to 24 do
      begin
        hStat[n].Checks:=0; hStat[n].Quests:=0; hStat[n].rSum:=0;
      end;

      quRep6.Active:=False;
      quRep6.ParamByName('DateB').AsDateTime:=Datebeg;
      quRep6.ParamByName('DateE').AsDateTime:=DateEnd;
      quRep6.Active:=True;

      quRep6.First;
      while not quRep6.eof do
      begin
        n:=CalcTimeToH(quRep6CHDATE.AsDateTime)+1; //+1 ���� ���� ����� �������� � ������� �� 1 - ����� �������
        inc(hStat[n].Checks);
        hStat[n].Quests:=hStat[n].Quests+quRep6QUESTS.AsInteger;
        hStat[n].rSum:=hStat[n].rSum+quRep6TABSUM.AsFloat;
        quRep6.Next;
        delay(10);
      end;
      //������ ������ ����� ��� ������������ ��������     ======
      rSumAll:=0;
      for n:=1 to 24 do rSumAll:=rSumAll+hStat[n].rSum;


      //������ ���� �������� - �������� �������
      taHour.Active:=False;
      taHour.CreateDataSet;

      for n:=10 to 24 do //��� �� � ������ ����� ���� ����� ���������
      begin

        if rSumAll>0 then p:=round(hStat[n].rSum/rSumAll*10000)/100
        else p:=0;

        StrWk:='';i:=0; while i<p do begin StrWk:=StrWk+'=';i:=i+5; end;

        taHour.Append;
        taHourNum.AsInteger:=n;
        taHourPeriod.AsString:=IntToStr(n-1)+':00  '+INtToStr(n-1)+':59';
        taHourChecks.AsInteger:=hStat[n].Checks;
        taHourQuests.AsInteger:=hStat[n].Quests;
        taHourRSum.AsCurrency:=hStat[n].rSum;
        if p>0 then taHourProc.AsString:=StrWk+'  '+FloatToStr(p)
        else taHourProc.AsString:='';
        taHour.Post;
      end;
      for n:=1 to 9 do //��� �� � ��� ����� � 0 �� 8:00
      begin
        if rSumAll>0 then p:=round(hStat[n].rSum/rSumAll*10000)/100
        else p:=0;

        StrWk:='';i:=0; while i<p do begin StrWk:=StrWk+'=';i:=i+5; end;

        taHour.Append;
        taHourNum.AsInteger:=n;
        taHourPeriod.AsString:=IntToStr(n-1)+':00  '+INtToStr(n-1)+':59';
        taHourChecks.AsInteger:=hStat[n].Checks;
        taHourQuests.AsInteger:=hStat[n].Quests;
        taHourRSum.AsCurrency:=hStat[n].rSum;
        if p>0 then taHourProc.AsString:=StrWk+'  '+FloatToStr(p)
        else taHourProc.AsString:='';
        taHour.Post;
      end;



      frRepMain1.LoadFromFile(CurDir + 'SaleHour.frf');

      frVariables.Variable['sPeriod']:='c '+FormatDateTime('dd.mm.yyyy hh.nn',DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy hh.nn',DateEnd);
      frVariables.Variable['Depart']:=CommonSet.DepartName;

      frRepMain1.ReportName:='��������� ����������.';
      frRepMain1.PrepareReport;
      frRepMain1.ShowPreparedReport;
    end;
  end;
  fmPeriod.Release;
end;

procedure TfmMainRep.acHourOpenSchetExecute(Sender: TObject);
type THStatREc = record
     Checks,Quests:INteger;
     rSum:Currency;
     end;

Var DateBeg,DateEnd:TDateTime;
    HStat:Array[1..24] of THStatrec;
    n:Integer;
    rSumAll:Currency;
    p,i:Real;
    StrWk:String;


Function CalcTimeToH(tt:TDateTime):ShortInt;
begin
  tt:=Frac(tt);
  Result:=StrToINtDef(FormatDateTime('hh',tt),0);
end;

begin
  //��������� ������� �� �������� ����� - �������
  if not CanDo('SaleCasher') then  StatusBar1.Panels[0].Text:='��� ����.';
  fmPeriod:=TfmPeriod.Create(Application);
  fmPeriod.ShowModal;
  if fmPeriod.ModalResult=mrOk then
  begin
    Datebeg:=TrebSel.DateFrom;
    DateEnd:=TrebSel.DateTo;

    with dmC do
    begin
      //������� ������������� ������
      for n:=1 to 24 do
      begin
        hStat[n].Checks:=0; hStat[n].Quests:=0; hStat[n].rSum:=0;
      end;

      quRep6_1.Active:=False;
      quRep6_1.ParamByName('DateB').AsDateTime:=Datebeg;
      quRep6_1.ParamByName('DateE').AsDateTime:=DateEnd;
      quRep6_1.Active:=True;

      quRep6_1.First;
      while not quRep6_1.eof do
      begin
        n:=CalcTimeToH(quRep6_1CHDATE.AsDateTime)+1; //+1 ���� ���� ����� �������� � ������� �� 1 - ����� �������
        inc(hStat[n].Checks);
        hStat[n].Quests:=hStat[n].Quests+quRep6_1QUESTS.AsInteger;
        hStat[n].rSum:=hStat[n].rSum+quRep6_1TABSUM.AsFloat;
        quRep6_1.Next;
        delay(10);
      end;
      //������ ������ ����� ��� ������������ ��������     ======
      rSumAll:=0;
      for n:=1 to 24 do rSumAll:=rSumAll+hStat[n].rSum;


      //������ ���� �������� - �������� �������
      taHour.Active:=False;
      taHour.CreateDataSet;

      for n:=10 to 24 do //��� �� � ������ ����� ���� ����� ���������
      begin

        if rSumAll>0 then p:=round(hStat[n].rSum/rSumAll*10000)/100
        else p:=0;

        StrWk:='';i:=0; while i<p do begin StrWk:=StrWk+'=';i:=i+5; end;

        taHour.Append;
        taHourNum.AsInteger:=n;
        taHourPeriod.AsString:=IntToStr(n-1)+':00  '+INtToStr(n-1)+':59';
        taHourChecks.AsInteger:=hStat[n].Checks;
        taHourQuests.AsInteger:=hStat[n].Quests;
        taHourRSum.AsCurrency:=hStat[n].rSum;
        if p>0 then taHourProc.AsString:=StrWk+'  '+FloatToStr(p)
        else taHourProc.AsString:='';
        taHour.Post;
      end;
      for n:=1 to 9 do //��� �� � ��� ����� � 0 �� 8:00
      begin
        if rSumAll>0 then p:=round(hStat[n].rSum/rSumAll*10000)/100
        else p:=0;

        StrWk:='';i:=0; while i<p do begin StrWk:=StrWk+'=';i:=i+5; end;

        taHour.Append;
        taHourNum.AsInteger:=n;
        taHourPeriod.AsString:=IntToStr(n-1)+':00  '+INtToStr(n-1)+':59';
        taHourChecks.AsInteger:=hStat[n].Checks;
        taHourQuests.AsInteger:=hStat[n].Quests;
        taHourRSum.AsCurrency:=hStat[n].rSum;
        if p>0 then taHourProc.AsString:=StrWk+'  '+FloatToStr(p)
        else taHourProc.AsString:='';
        taHour.Post;
      end;



      frRepMain1.LoadFromFile(CurDir + 'SaleHour.frf');

      frVariables.Variable['sPeriod']:='c '+FormatDateTime('dd.mm.yyyy hh.nn',DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy hh.nn',DateEnd);
      frVariables.Variable['Depart']:=CommonSet.DepartName;

      frRepMain1.ReportName:='��������� ����������.';
      frRepMain1.PrepareReport;
      frRepMain1.ShowPreparedReport;
    end;
  end;
  fmPeriod.Release;

end;

procedure TfmMainRep.acWeekExecute(Sender: TObject);
  //������� �� ���� ������
type THStatREc = record
     Checks,Quests:INteger;
     rSum:Currency;
     end;

Var DateBeg,DateEnd:TDateTime;
    HStat:Array[1..7] of THStatrec;
    n:Integer;
    rSumAll:Currency;
    p,i:Real;
    StrWk,StrWk1:String;


Function CalcDayW(tt:TDateTime):ShortInt;
Var StrW:String;
begin
  StrW:=FormatDateTime('ddd	',tt);
  Result:=1;
  if Pos('��',StrW)>0 then Result:=1;
  if Pos('��',StrW)>0 then Result:=2;
  if Pos('��',StrW)>0 then Result:=3;
  if Pos('��',StrW)>0 then Result:=4;
  if Pos('��',StrW)>0 then Result:=5;
  if Pos('��',StrW)>0 then Result:=6;
  if Pos('��',StrW)>0 then Result:=7;
end;

begin
  //������� �� ���� ������ - �������
  if not CanDo('SaleCasher') then  StatusBar1.Panels[0].Text:='��� ����.';
  fmPeriod:=TfmPeriod.Create(Application);
  fmPeriod.ShowModal;
  if fmPeriod.ModalResult=mrOk then
  begin
    Datebeg:=TrebSel.DateFrom;
    DateEnd:=TrebSel.DateTo;

    with dmC do
    begin
      //������� ������������� ������
      for n:=1 to 7 do
      begin
        hStat[n].Checks:=0; hStat[n].Quests:=0; hStat[n].rSum:=0;
      end;

      quRep6.Active:=False;
      quRep6.ParamByName('DateB').AsDateTime:=Datebeg;
      quRep6.ParamByName('DateE').AsDateTime:=DateEnd;
      quRep6.Active:=True;

      quRep6.First;
      while not quRep6.eof do
      begin
        n:=CalcDayW(quRep6CHDATE.AsDateTime-StrtoTimeDef(CommonSet.ZTimeShift,0));
        inc(hStat[n].Checks);
        hStat[n].Quests:=hStat[n].Quests+quRep6QUESTS.AsInteger;
        hStat[n].rSum:=hStat[n].rSum+quRep6TABSUM.AsFloat;
        quRep6.Next;
        delay(10);
      end;
      //������ ������ ����� ��� ������������ ��������     ======
      rSumAll:=0;
      for n:=1 to 7 do rSumAll:=rSumAll+hStat[n].rSum;


      //������ ���� �������� - �������� �������
      taHour.Active:=False;
      taHour.CreateDataSet;

      for n:=1 to 7 do
      begin

        if rSumAll>0 then p:=round(hStat[n].rSum/rSumAll*10000)/100
        else p:=0;

        StrWk:='';i:=0; while i<p do begin StrWk:=StrWk+'=';i:=i+5; end;

        StrWk1:='';
        Case n of
        1:StrWk1:='�����������';
        2:StrWk1:='�������';
        3:StrWk1:='�����';
        4:StrWk1:='�������';
        5:StrWk1:='�������';
        6:StrWk1:='�������';
        7:StrWk1:='�����������';
        end;

        taHour.Append;
        taHourNum.AsInteger:=n;
        taHourPeriod.AsString:=StrWk1;
        taHourChecks.AsInteger:=hStat[n].Checks;
        taHourQuests.AsInteger:=hStat[n].Quests;
        taHourRSum.AsCurrency:=hStat[n].rSum;
        if p>0 then taHourProc.AsString:=StrWk+'  '+FloatToStr(p)
        else taHourProc.AsString:='';
        taHour.Post;
      end;

      frRepMain1.LoadFromFile(CurDir + 'SaleWeek.frf');

      frVariables.Variable['sPeriod']:='c '+FormatDateTime('dd.mm.yyyy hh.nn',DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy hh.nn',DateEnd);
      frVariables.Variable['Depart']:=CommonSet.DepartName;

      frRepMain1.ReportName:='������� �� ���� ������.';
      frRepMain1.PrepareReport;
      frRepMain1.ShowPreparedReport;
    end;
  end;
  fmPeriod.Release;
end;

procedure TfmMainRep.acOutGrExecute(Sender: TObject);
Var DateBeg,DateEnd:TDateTime;
begin
// ���������� �� �������
  if not CanDo('OutGroup') then  StatusBar1.Panels[0].Text:='��� ����.';
  fmPeriod:=TfmPeriod.Create(Application);
  fmPeriod.ShowModal;
  if fmPeriod.ModalResult=mrOk then
  begin
    Datebeg:=TrebSel.DateFrom;
    DateEnd:=TrebSel.DateTo;

    with dmC do
    begin
      quRep1_.Active:=False;
      quRep1_.ParamByName('DateB').AsDateTime:=Datebeg;
      quRep1_.ParamByName('DateE').AsDateTime:=DateEnd;

      frRepMain1.LoadFromFile(CurDir + 'OutGroup.frf');

      frVariables.Variable['sPeriod']:='c '+FormatDateTime('dd.mm.yyyy hh.nn',DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy hh.nn',DateEnd);
      frVariables.Variable['Depart']:=CommonSet.DepartName;

      frRepMain1.ReportName:='������ �� �������.';
      frRepMain1.PrepareReport;
      frRepMain1.ShowPreparedReport;
    end;
  end;
  fmPeriod.Release;
end;

procedure TfmMainRep.acOutKatExecute(Sender: TObject);
Var DateBeg,DateEnd:TDateTime;
begin
// ���������� �� ���������
  if not CanDo('OutCateg') then  StatusBar1.Panels[0].Text:='��� ����.';
  fmPeriod:=TfmPeriod.Create(Application);
  fmPeriod.ShowModal;
  if fmPeriod.ModalResult=mrOk then
  begin
    Datebeg:=TrebSel.DateFrom;
    DateEnd:=TrebSel.DateTo;

    with dmC do
    begin
      quRep2_.Active:=False;
      quRep2_.ParamByName('DateB').AsDateTime:=Datebeg;
      quRep2_.ParamByName('DateE').AsDateTime:=DateEnd;

      frRepMain2.LoadFromFile(CurDir + 'OutCateg.frf');

      frVariables.Variable['sPeriod']:='c '+FormatDateTime('dd.mm.yyyy hh.nn',DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy hh.nn',DateEnd);
      frVariables.Variable['Depart']:=CommonSet.DepartName;

      frRepMain2.ReportName:='������ �� ����������.';
      frRepMain2.PrepareReport;
      frRepMain2.ShowPreparedReport;
    end;
  end;
  fmPeriod.Release;
end;

procedure TfmMainRep.acDelExecute(Sender: TObject);
Var DateBeg,DateEnd:TDateTime;
begin
  //���������
  if not CanDo('DelRep') then  StatusBar1.Panels[0].Text:='��� ����.';
  fmPeriod:=TfmPeriod.Create(Application);
  fmPeriod.ShowModal;
  if fmPeriod.ModalResult=mrOk then
  begin
    Datebeg:=TrebSel.DateFrom;
    DateEnd:=TrebSel.DateTo;

    with dmC do
    begin
      quRepDel.Active:=False;
      quRepDel.ParamByName('DateB').AsDateTime:=Datebeg;
      quRepDel.ParamByName('DateE').AsDateTime:=DateEnd;

      frRepMain2.LoadFromFile(CurDir + 'DelRep.frf');

      frVariables.Variable['sPeriod']:='c '+FormatDateTime('dd.mm.yyyy hh.nn',DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy hh.nn',DateEnd);
      frVariables.Variable['Depart']:=CommonSet.DepartName;

      frRepMain2.ReportName:='���������.';
      frRepMain2.PrepareReport;
      frRepMain2.ShowPreparedReport;
    end;
  end;
  fmPeriod.Release;
end;

procedure TfmMainRep.acRealMHExecute(Sender: TObject);
Var DateBeg,DateEnd:TDateTime;
    rSum,rSumSt:Real;
    StrWk,StrWk1:String;
    iStream:Integer;
begin
//������� �� ������ �������� (������������)
  if not CanDo('SaleMH') then  StatusBar1.Panels[0].Text:='��� ����.';
  fmPeriod:=TfmPeriod.Create(Application);
  fmPeriod.ShowModal;
  if fmPeriod.ModalResult=mrOk then
  begin
    Datebeg:=TrebSel.DateFrom;
    DateEnd:=TrebSel.DateTo;

    with dmC do
    begin
      quRep7.Active:=False;
      quRep7.ParamByName('DateB').AsDateTime:=Datebeg;
      quRep7.ParamByName('DateE').AsDateTime:=DateEnd;

      if fmPeriod.cxCheckBox1.Checked then
      begin //�������� �� �������� �������
        try
          quRep7.Active:=True;

          prOpenDevPrint(CommonSet.PrePrintPort);

          SelFont(13);
          PrintStr(' '+CommonSet.DepartName);
          PrintStr('');

          SelFont(14);
          PrintStr('���������� �� ��.');
          SelFont(13);
          PrintStr('');
          PrintStr('�� ������ � '+FormatDateTime('dd.mm.yyyy hh:mm',Datebeg));
          PrintStr('�� '+FormatDateTime('dd.mm.yyyy hh:mm',DateEnd));

          SelFont(13);
          StrWk:='                                          ';
          PrintStr(StrWk);
          SelFont(15);
          StrWk:=' ���      ��������       ���-��   ����� ';
            //       1234 �������� ������ 18 999.999 9999.99
          PrintStr(StrWk);
          SelFont(13);
          StrWk:='                                          ';
          PrintStr(StrWk);

          rSum:=0;
          rSumSt:=0;
          iStream:=-1; //������ ����� ���� �� ����� ���� �������� ������ � ������ ������
          SelFont(15);

          quRep7.First;
          while not quRep7.Eof do
          begin
            if iStream<>quRep7STREAM.AsInteger then
            begin
              //���� �� ������ ����� -1 �� ������� ����
              if iStream>=0 then
              begin
                PrintStr('');
                SelFont(13);
                Str(rSumSt:8:2,StrWk);
                StrWk:='����� �� �� : '+StrWk;
                PrintStr(StrWk);
                PrintStr('');
                rSumSt:=0;
              end;

              iStream:=quRep7STREAM.AsInteger;
              PrintStr('');
              SelFont(13);
              StrWk:=quRep7NAMESTREAM.AsString;
              PrintStr(StrWk);
              PrintStr('');
              SelFont(15);
            end;

            rSum:=rSum+quRep7SUMSUM.AsFloat;
            rSumSt:=rSumSt+quRep7SUMSUM.AsFloat;
            //���
            StrWk:=IntToStr(quRep7SIFR.AsInteger);
            while Length(StrWk)<5 do StrWk:=' '+StrWk;
            StrWk:=StrWk+' ';//' 0000 '
            //��������
            StrWk1:=Copy(quRep7NAME.AsString,1,18);
            while Length(StrWk1)<18 do StrWk1:=StrWk1+' ';
            StrWk:=StrWk+StrWk1+' ';
            //���-��
            Str(quRep7SUMQUANT.AsFloat:7:3,StrWk1);
            while Length(StrWk1)<7 do StrWk1:=StrWk1+' ';
            StrWk:=StrWk+StrWk1+' ';
            //�����
            Str(quRep7SUMSUM.AsFloat:7:2,StrWk1);
            while Length(StrWk1)<7 do StrWk1:=StrWk1+' ';
            StrWk:=StrWk+StrWk1;

            PrintStr(StrWk);

            quRep7.Next;
          end;

          PrintStr('');
          SelFont(13);
          Str(rSumSt:8:2,StrWk);
          StrWk:='����� �� �� : '+StrWk;
          PrintStr(StrWk);
          PrintStr('');


          SelFont(13);
          StrWk:='                                          ';
          PrintStr(StrWk);

          SelFont(15);
          Str(rSum:8:2,StrWk1);
          StrWk:=' �����                      '+StrWk1+' ���';
          PrintStr(StrWk);

          CutDocPr;
        finally
          quRep7.Active:=False;
          Delay(100);
          DevPrint.Close;
        end;

      end else
      begin //�������� �� �������
        frRepMain1.LoadFromFile(CurDir + 'SaleMH.frf');

        frVariables.Variable['sPeriod']:='c '+FormatDateTime('dd.mm.yyyy hh.nn',DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy hh.nn',DateEnd);
        frVariables.Variable['Depart']:=CommonSet.DepartName;

        frRepMain1.ReportName:='���������� �� ������ �������� (������������).';
        frRepMain1.PrepareReport;
        frRepMain1.ShowPreparedReport;
      end;
    end;
  end;
  fmPeriod.Release;
end;

procedure TfmMainRep.acSaleMHExecute(Sender: TObject);
Var DateBeg,DateEnd:TDateTime;
    rSum,rSumD:Real;
    StrWk,StrWk1:String;
begin
  //������� �� ������ �������� (������������)
  if not CanDo('CashRealMH') then  StatusBar1.Panels[0].Text:='��� ����.';
  fmPeriod:=TfmPeriod.Create(Application);
  fmPeriod.ShowModal;
  if fmPeriod.ModalResult=mrOk then
  begin
    Datebeg:=TrebSel.DateFrom;
    DateEnd:=TrebSel.DateTo;

    with dmC do
    begin
      quRep8.Active:=False;
      quRep8.ParamByName('DateB').AsDateTime:=Datebeg;
      quRep8.ParamByName('DateE').AsDateTime:=DateEnd;

      quRep81.Active:=False;
      quRep81.ParamByName('DateB').AsDateTime:=Datebeg;
      quRep81.ParamByName('DateE').AsDateTime:=DateEnd;

      if fmPeriod.cxCheckBox1.Checked then
      begin //�������� �� �������� �������
        try
          quRep8.Active:=True;

          prOpenDevPrint(CommonSet.PrePrintPort);

          SelFont(13);
          PrintStr(' '+CommonSet.DepartName);
          PrintStr('');

          SelFont(14);
          PrintStr('������� �� ��.');
          SelFont(13);
          PrintStr('');
          PrintStr('�� ������ � '+FormatDateTime('dd.mm.yyyy hh:mm',Datebeg));
          PrintStr('�� '+FormatDateTime('dd.mm.yyyy hh:mm',DateEnd));

          SelFont(13);
          StrWk:='                                          ';
          PrintStr(StrWk);
          SelFont(15);
          StrWk:=' ����� ��������      �����   � �.�.������ ';
            //   ' ���                99999.99   99999.99   '
          PrintStr(StrWk);
          SelFont(13);
          StrWk:='                                          ';
          PrintStr(StrWk);

          SelFont(15);

          rSum:=0; rSumD:=0;

          quRep8.First;
          while not quRep8.Eof do
          begin

            rSum:=rSum+quRep8SUMSUM.AsFloat;
            rSumD:=rSumD+quRep8SUMDISC.AsFloat;
            //��������
            StrWk:=' '+Copy(quRep8NAMESTREAM.AsString,1,19);
            while Length(StrWk)<19 do StrWk:=StrWk+' ';

            //�����
            Str(quRep8SUMSUM.AsFloat:8:2,StrWk1);
            while Length(StrWk1)<8 do StrWk1:=StrWk1+' ';
            StrWk:=StrWk+StrWk1+' ';

            //������
            Str(quRep8SUMDISC.AsFloat:8:2,StrWk1);
            while Length(StrWk1)<8 do StrWk1:=StrWk1+' ';
            StrWk:=StrWk+StrWk1+' ';

            PrintStr(StrWk);

            quRep8.Next;
          end;

          SelFont(13);
          StrWk:='                                          ';
          PrintStr(StrWk);

          SelFont(15);
          Str(rSum:8:2,StrWk1);
          StrWk:=' �����              '+StrWk1+' ';
          Str(rSumD:8:2,StrWk1);
          StrWk:=StrWk+StrWk1;

          PrintStr(StrWk);

          CutDocPr;
        finally
          quRep8.Active:=False;
          Delay(100);
          DevPrint.Close;
        end;

      end else
      begin //�������� �� �������
        frRepMain2.LoadFromFile(CurDir + 'SaleMHSum.frf');

        frVariables.Variable['sPeriod']:='c '+FormatDateTime('dd.mm.yyyy hh.nn',DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy hh.nn',DateEnd);
        frVariables.Variable['Depart']:=CommonSet.DepartName;

        frRepMain2.ReportName:='������� �� ������ �������� (������������).';
        frRepMain2.PrepareReport;
        frRepMain2.ShowPreparedReport;
      end;
    end;
  end;
  fmPeriod.Release;
end;

procedure TfmMainRep.acRealPerDayExecute(Sender: TObject);
Var DateBeg,DateEnd:TDateTime;
    iDateb,iDateE,iDateCur:Integer;
    rProc,rQ,rS,rD:Real;
begin
  //���������� �� ������� �� ������ ���
  if not CanDo('SaleDayTime') then  StatusBar1.Panels[0].Text:='��� ����.';
  fmPeriod2:=TfmPeriod2.Create(Application);
  fmPeriod2.DateTimePicker1.Date:=Trunc(TrebSel.DateFrom);
  fmPeriod2.DateTimePicker2.Date:=Trunc(TrebSel.DateTo);
//  fmPeriod2.DateTimePicker3.Visible:=True;
//  fmPeriod2.DateTimePicker4.Visible:=True;
  fmPeriod2.DateTimePicker3.Time:=TrebSel.TimeFrom;
  fmPeriod2.DateTimePicker4.Time:=TrebSel.TimeTo;

  fmPeriod2.ShowModal; 
  if fmPeriod2.ModalResult=mrOk then
  begin
    Datebeg:=Trunc(fmPeriod2.DateTimePicker1.Date);
    DateEnd:=Trunc(fmPeriod2.DateTimePicker2.Date);

    with dmC do
    begin
      fmMainrep.taRDT.Active:=False;
      fmMainrep.taRDT.CreateDataSet;
      fmRDTime.Caption:='���������� �� ������ c '+FormatDateTime('dd.mm.yyyy hh.nn',DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy hh.nn',DateEnd)+'   c '+FormatDateTime('hh:nn',TrebSel.TimeFrom)+' �� '+FormatDateTime('hh:nn',TrebSel.TimeTo);
      fmRDTime.ProgressBar1.Visible:=True;
      fmRDTime.ProgressBar1.Position:=0;
      fmRDTime.Show;
      delay(10);
      fmRDTime.ViewRDT.BeginUpdate;
      iDateB:=Trunc(DateBeg);
      iDateE:=Trunc(DateEnd);
      rProc:=100/(iDateE-iDateB);
      for iDateCur:=iDateB to iDateE do
      begin
        quRep1.Active:=False;
        quRep1.ParamByName('DateB').AsDateTime:=iDateCur+Frac(TrebSel.TimeFrom);
        quRep1.ParamByName('DateE').AsDateTime:=iDateCur+Frac(TrebSel.TimeTo);
        quRep1.Active:=True;
        qurep1.First;
        while not quRep1.Eof do
        begin
          if taRDT.Locate('Sifr',quRep1SIFR.AsInteger,[]) then
          begin //����� ����� ��� ����
            rQ:=taRDTSUMQUANT.AsFloat;
            rS:=taRDTSUMSUM.AsFloat;
            rD:=taRDTSUMDISC.AsFloat;

            taRDT.Edit;
            taRDTSUMQUANT.AsFloat:=rQ+quRep1SUMQUANT.AsFloat;
            taRDTSUMSUM.AsFloat:=rS+quRep1SUMSUM.AsFloat;
            taRDTSUMDISC.AsFloat:=rD+quRep1SUMDISC.AsFloat;
            taRDT.Post;
          end else
          begin  // ������ ��� ���.
            taRDT.Append;
            taRDTSifr.AsInteger:=quRep1SIFR.AsInteger;
            taRDTNAME.AsString:=quRep1NAME.AsString;
            taRDTPARENT.AsInteger:=quRep1PARENT.AsInteger;
            taRDTNAMEGR.AsString:=quRep1NAMEGR.AsString;
            taRDTSUMQUANT.AsFloat:=quRep1SUMQUANT.AsFloat;
            taRDTSUMSUM.AsFloat:=quRep1SUMSUM.AsFloat;
            taRDTSUMDISC.AsFloat:=quRep1SUMDISC.AsFloat;
            taRDT.Post;
          end;
          quRep1.Next;
        end;

        fmRDTime.ProgressBar1.Position:=Round((iDateCur-iDateB)*rProc);
        delay(100);
      end;

      quRep1.Active:=False;
      fmRDTime.ViewRDT.EndUpdate;
      fmRDTime.ProgressBar1.Position:=100;
      delay(200);
      fmRDTime.ProgressBar1.Visible:=False;
    end;
  end;
  fmPeriod2.Release;
end;

procedure TfmMainRep.acRealDayExecute(Sender: TObject);
Var DateBeg,DateEnd:TDateTime;
    bSt:Boolean;
begin
  //���������� �� ����
  if not CanDo('RealDay') then  begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end; 
  fmPeriod2:=TfmPeriod2.Create(Application);
  fmPeriod2.DateTimePicker1.Date:=Trunc(TrebSel.DateFrom);
  fmPeriod2.DateTimePicker2.Date:=Trunc(TrebSel.DateTo);
  fmPeriod2.DateTimePicker3.Visible:=False;
  fmPeriod2.DateTimePicker4.Visible:=False;
  fmPeriod2.Label3.Visible:=False;
  fmPeriod2.Label4.Visible:=False;
//  fmPeriod2.DateTimePicker3.Time:=TrebSel.TimeFrom;
//  fmPeriod2.DateTimePicker4.Time:=TrebSel.TimeTo;

  bSt:=False;
  fmPeriod2.ShowModal;
  if fmPeriod2.ModalResult=mrOk then  bSt:=True;
  fmPeriod2.Release;
  if bSt then
  begin
    Datebeg:=Trunc(fmPeriod2.DateTimePicker1.Date);
    DateEnd:=Trunc(fmPeriod2.DateTimePicker2.Date);
    fmRealDays.Caption:='���������� �� ���� �� ������ c '+FormatDateTime('dd.mm.yyyy',DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy',DateEnd);

    with dmC do
    begin
      quRep9.Active:=False;
      quRep9.ParamByName('DATEB').AsDate:=Datebeg;
      quRep9.ParamByName('DATEE').AsDate:=DateEnd; //��� ������ <, ��  TrebSel.DateTo ��� +1
      quRep9.Active:=True;

      fmRealDays.Show;
    end;
  end;
end;

procedure TfmMainRep.acSaleMHStationExecute(Sender: TObject);
Var DateBeg,DateEnd:TDateTime;
begin
// ���������� ������� �������
  if not CanDo('SaleMHSt') then  StatusBar1.Panels[0].Text:='��� ����.';
  fmPeriod:=TfmPeriod.Create(Application);
  fmPeriod.ShowModal;
  if fmPeriod.ModalResult=mrOk then
  begin
    Datebeg:=TrebSel.DateFrom;
    DateEnd:=TrebSel.DateTo;

    with dmC do
    begin
      quRep10.Active:=False;
      quRep10.ParamByName('DateB').AsDateTime:=Datebeg;
      quRep10.ParamByName('DateE').AsDateTime:=DateEnd;

      frRepMain1.LoadFromFile(CurDir + 'SaleMHSt.frf');

      frVariables.Variable['sPeriod']:='c '+FormatDateTime('dd.mm.yyyy hh.nn',DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy hh.nn',DateEnd);
      frVariables.Variable['Depart']:=CommonSet.DepartName;

      frRepMain1.ReportName:='���������� �� ������� �������.';
      frRepMain1.PrepareReport;
      frRepMain1.ShowPreparedReport;
    end;
  end;
  fmPeriod.Release;
end;

procedure TfmMainRep.AcSaleDayExecute(Sender: TObject);
Var DateBeg,DateEnd:TDateTime;
    Znum,KolCh,Quests:Integer;
    rS,rD:Real;
    DateZ,EndDate: String;
    par:Variant;

begin
// ������� �� ����
  if not CanDo('SaleDay') then  StatusBar1.Panels[0].Text:='��� ����.';
  fmPeriod:=TfmPeriod.Create(Application);
  fmPeriod.ShowModal;
  if fmPeriod.ModalResult=mrOk then
  begin
    Datebeg:=TrebSel.DateFrom;
    DateEnd:=TrebSel.DateTo;

    with dmC do
    begin
      fmMainrep.taDay.Active:=False;
      fmMainrep.taDay.CreateDataSet;

//������ ����� � CASHSAIL
      quRepDay1.Active:=False;
      quRepDay1.ParamByName('DateB').AsDateTime:=Datebeg;
      quRepDay1.ParamByName('DateE').AsDateTime:=DateEnd;
      quRepDay1.Active:=True;

//���� z ������ � CASHSAIL
      quRepDay2.Active:=False;
      quRepDay2.ParamByName('DateB').AsDateTime:=Datebeg;
      quRepDay2.ParamByName('DateE').AsDateTime:=DateEnd;
      quRepDay2.Active:=True;

 // ������ �����
      quRepDay.Active:=False;
      quRepDay.ParamByName('DateB').AsDateTime:=Datebeg;
      quRepDay.ParamByName('DateE').AsDateTime:=DateEnd;
      quRepDay.Active:=True;

      Znum:=0;
      KolCh:=0;
      rS:=0; rD:=0;
      Quests:=0;
      EndDate := '';
      par := VarArrayCreate([0,1], varInteger);

      quRepDay.First;
      while not quRepDay.Eof do
      begin

                //��� �� ������ ������
        DateZ:= FormatDateTime('dd.mm.yyyy',quRepDayENDTIME.AsDateTime);

        par[0]:=quRepDayID_TAB.AsInteger;
        if quRepDay1.Locate('Tab_ID',par,[]) then
         begin
          par[0]:=quRepDay1CASHNUM.AsInteger;
          par[1]:=quRepDay1ZNUM.AsInteger;
          if quRepDay2.Locate('CASHNUM;ZNUM',par,[]) then DateZ:= FormatDateTime('dd.mm.yyyy',quRepDay2DATEZ.AsDateTime);

         end;

        if EndDate <> DateZ then
          begin
            if EndDate <> '' then
             begin  // ������ ��� ���.
              taDay.Append;
  //            taDayZnum.AsInteger:=Znum;
              taDayKolCh.AsInteger:=KolCh;
              taDayQuests.AsInteger:=Quests;
              taDaySumma.AsFloat:=rS;
              taDaySUMDISC.AsFloat:=rD;
              taDayDate.AsString:=EndDate;
              taDay.Post;
             end;
            EndDate := DateZ;
 //           Znum := quRepDayZNUM.AsInteger;
//            EndDate:= quRepDayEndTime.AsDateTime;
            KolCh:=0;
            rS:=0; rD:=0;
            Quests:=0;
          end;

         KolCh:=KolCh + 1;
         Quests:=Quests+quRepDayQuests.AsInteger;
         rS:=rS+quRepDaySumma.AsFloat;
         rD:=rD+quRepDaySUMDISC.AsFloat;
         quRepDay.Next;

      end;
      if EndDate <> '' then
       begin
        taDay.Append;
   //     taDayZnum.AsInteger:=Znum;
        taDayKolCh.AsInteger:=KolCh;
        taDayQuests.AsInteger:=Quests;
        taDaySumma.AsFloat:=rS;
        taDaySUMDISC.AsFloat:=rD;
        taDayDate.AsString:=EndDate;
        taDay.Post;
        end;


      quRepDay.Active:=False;

      frRepMain2.LoadFromFile(CurDir + 'SaleDay.frf');

      frVariables.Variable['sPeriod']:='c '+FormatDateTime('dd.mm.yyyy hh.nn',DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy hh.nn',DateEnd);
      frVariables.Variable['Depart']:=CommonSet.DepartName;

      frRepMain2.ReportName:='������� �� ����.';
      frRepMain2.PrepareReport;
      frRepMain2.ShowPreparedReport;
    end;
  end;
  fmPeriod.Release;
end;
//��� ))



procedure TfmMainRep.acSaleWaitersBludExecute(Sender: TObject);
begin
   with dmC do
  begin
{
    taTabsAllSel.Active:=False;
    taTabsAllSel.ParamByName('DBEG').AsDateTime:=TrebSel.DateFrom;
    taTabsAllSel.ParamByName('DEND').AsDateTime:=TrebSel.DateTo;
    taTabsAllSel.Active:=True;
    taTabsAllSel.Last;
 }
 
  end;
  fmRepWatersBlud.Caption:=' ������� ���������� �� ������ � '+FormatDateTime('dd.mm.yyyy hh.nn',TrebSel.DateFrom)+' �� '+FormatDateTime('dd.mm.yyyy hh.nn',TrebSel.DateTo);
  fmRepWatersBlud.show;
end;

procedure TfmMainRep.acSpisokPersonalExecute(Sender: TObject);
begin
// ������ ��������
//  if not CanDo('SaleMHSt') then  StatusBar1.Panels[0].Text:='��� ����.';

  with dmC do
  begin
   quRepSK.Active:=False;
//   quRep10.ParamByName('DateB').AsDateTime:=Datebeg;
//  quRep10.ParamByName('DateE').AsDateTime:=DateEnd;

    frRepMain1.LoadFromFile(CurDir + 'SpisokPersonal.frf');

 //   frVariables.Variable['sPeriod']:='c '+FormatDateTime('dd.mm.yyyy hh.nn',DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy hh.nn',DateEnd);
    frVariables.Variable['Depart']:=CommonSet.DepartName;

    frRepMain1.ReportName:='������ ���������.';
    frRepMain1.PrepareReport;
    frRepMain1.ShowPreparedReport;
  end;

end;

procedure TfmMainRep.acCaherShortSExecute(Sender: TObject);
Var DateBeg,DateEnd:TDateTime;
Var Bnal,sumb,sumr : Real;
Var par:Variant;
Var SUMSUM : Real;
Var QUESTS,KolCheck,CASHNUM,IDSifr : Integer;
Var OPERTYPE : String;

begin
// ������� �� ������ ������ ����
  if not CanDo('CashReal') then  StatusBar1.Panels[0].Text:='��� ����.';
  fmPeriod:=TfmPeriod.Create(Application);

  fmPeriod.ShowModal;
  if fmPeriod.ModalResult=mrOk then
  begin
    Datebeg:=TrebSel.DateFrom;
    DateEnd:=TrebSel.DateTo;

    with dmC do
    begin
    // ������� ������ ������

      quRep33.Active:=False;
      quRep33.ParamByName('DateB').AsDateTime:=Datebeg;
      quRep33.ParamByName('DateE').AsDateTime:=DateEnd;

     // ����� ���������� ����� �� ��������
      quRep32_2.Active:=False;
      quRep32_2.ParamByName('DateB').AsDateTime:=Datebeg;
      quRep32_2.ParamByName('DateE').AsDateTime:=DateEnd;

      // ������� ���������� ����� � ������ �� ����� ����� �����
      quRep32_3.Active:=False;
      quRep32_3.ParamByName('DateB').AsDateTime:=Datebeg;
      quRep32_3.ParamByName('DateE').AsDateTime:=DateEnd;

    // ������� ������ ��
      quRep331.Active:=False;
      quRep331.ParamByName('DateB').AsDateTime:=Datebeg;
      quRep331.ParamByName('DateE').AsDateTime:=DateEnd;

      quRep33.Active:=True;
      quRep331.Active:=True;

      taRepKS.Active:=False;
      taRepKS.CreateDataSet;

      Bnal := 0;
      par := VarArrayCreate([0,1], varVariant);

      quRep33.First;
      while not quRep33.Eof do
      begin
       par[0]:=quRep33CASHNUM.AsInteger;
       par[1]:=quRep33OPERTYPE.AsString;
       if quRep33CASHNUM.AsInteger <>  0 then
       Begin
        if not taRepKS.Locate('CASHNUM;OPERTYPE',par,[]) then
//        if (OPERTYPE <> quRep33OPERTYPE.AsString) or (CASHNUM <> quRep33CASHNUM.AsInteger) then
         Begin
          taRepKS.Append;
          taRepKSCASHNUM.AsInteger:=quRep33CASHNUM.AsInteger;
          taRepKSOPERTYPE.AsString:=quRep33OPERTYPE.AsString;

          taRepKSSUMSUM.AsFloat:= 0;
          taRepKSQUESTS.AsInteger:= 0;
          taRepKSKolCheck.AsInteger:= 0;
         end
        else
         Begin
          taRepKS.Edit;
         end;

        if quRep33OPERTYPE.AsString = 'SaleBank' then
         Begin
          taRepKSSUMSUM.AsFloat := taRepKSSUMSUM.AsFloat + RoundTo(quRep33SUMMA.AsFloat,-2);
         end
        else
         Begin
          taRepKSSUMSUM.AsFloat := taRepKSSUMSUM.AsFloat + RoundTo(quRep33SUMSUM.AsFloat,-2);
         end;

        taRepKSQUESTS.AsInteger:= taRepKSQUESTS.AsInteger + quRep33QUESTS.AsInteger;
        taRepKSKolCheck.AsInteger:= taRepKSKolCheck.AsInteger + 1;
        taRepKS.Post;

       end;

       quRep33.Next;
      end;

      quRep331.First;
      while not quRep331.Eof do
      begin

 //       Bnal  := quRep331SUMSUM.Value;

        par[0]:=quRep331CASHNUM.AsInteger;
        par[1]:='SaleBank';
        sumb := quRep331SUMSUM.Value;

//T.OPERTYPE

        if taRepKS.Locate('CASHNUM;OPERTYPE',par,[]) then
         begin
          if sumb <> taRepKSSUMSUM.Value then
           begin

            taRepKS.Edit;
            sumr := taRepKSSUMSUM.Value - sumb;
            taRepKSSUMSUM.Value := sumb;
            taRepKS.Post;

            par[1]:='Sale';
            taRepKS.Locate('CASHNUM;OPERTYPE',par,[]);
            taRepKS.Edit;
            taRepKSSUMSUM.Value :=taRepKSSUMSUM.Value + sumr;
            taRepKS.Post;

           end;
         end;

        quRep331.Next;
      end;

      taRepKS.IndexFieldNames := 'CASHNUM;OPERTYPE';

      quRep33.Active:=False;
      quRep331.Active:=False;
      taRepKS.Active:=False;

      frRepMain2.LoadFromFile(CurDir + 'SaleCashShortS.frf');
      frVariables.Variable['sPeriod']:='c '+FormatDateTime('dd.mm.yyyy hh.nn',DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy hh.nn',DateEnd);
      frVariables.Variable['Depart']:=CommonSet.DepartName;


      frRepMain2.ReportName:='���������� �� ������ "C".';
      frRepMain2.PrepareReport;
      frRepMain2.ShowPreparedReport;
    end;
  end;
  fmPeriod.Release;

end;

procedure TfmMainRep.AcBalansExecute(Sender: TObject);
Var DateBeg,DateEnd:TDateTime;
    rS,rD:Real;
    par,KolCh,Quests:Variant;

begin
// ������
  if not CanDo('Balans') then  StatusBar1.Panels[0].Text:='��� ����.';
  fmPeriod:=TfmPeriod.Create(Application);
  fmPeriod.ShowModal;
  if fmPeriod.ModalResult=mrOk then
  begin
    Datebeg:=TrebSel.DateFrom;
    DateEnd:=TrebSel.DateTo;

    with dmC do
    begin
      fmMainrep.taDay.Active:=False;
      fmMainrep.taDay.CreateDataSet;

//������������ �� ������ ��������
      quRep8.Active:=False;
      quRep8.ParamByName('DateB').AsDateTime:=Datebeg;
      quRep8.ParamByName('DateE').AsDateTime:=DateEnd;

// ������� �� ����� ������
      quRep312.Active:=False;
      quRep312.ParamByName('DateB').AsDateTime:=Datebeg;
      quRep312.ParamByName('DateE').AsDateTime:=DateEnd;

// ������� �� ��������� ������
      quRep82.Active:=False;
      quRep82.ParamByName('DateB').AsDateTime:=Datebeg;
      quRep82.ParamByName('DateE').AsDateTime:=DateEnd;

// ���������� ������ � �����
      quRep313.Active:=False;
      quRep313.ParamByName('DateB').AsDateTime:=Datebeg;
      quRep313.ParamByName('DateE').AsDateTime:=DateEnd;

 // ������ �����
      quRepDay.Active:=False;
      quRepDay.ParamByName('DateB').AsDateTime:=Datebeg;
      quRepDay.ParamByName('DateE').AsDateTime:=DateEnd;
      quRepDay.Active:=True;


 {
      rS:=0; rD:=0;

      KolCh:=VarArrayCreate([0,1], varInteger);
      Quests:=VarArrayCreate([0,1], varInteger);
      par := VarArrayCreate([0,1], varInteger);

      KolCh[0]:=0; KolCh[1]:=0;
      Quests[0]:=0; Quests[1]:=0;

      quRepDay.First;
      while not quRepDay.Eof do
      begin
       if quRepDaySumma.AsFloat
                //��� �� ������ ������
//        DateZ:= FormatDateTime('dd.mm.yyyy',quRepDayENDTIME.AsDateTime);

 //       par[0]:=quRepDayID_TAB.AsInteger;
 //       if quRepDay1.Locate('Tab_ID',par,[]) then
//         begin
//          par[0]:=quRepDay1CASHNUM.AsInteger;
//          par[1]:=quRepDay1ZNUM.AsInteger;
//          if quRepDay2.Locate('CASHNUM;ZNUM',par,[]) then DateZ:= FormatDateTime('dd.mm.yyyy',quRepDay2DATEZ.AsDateTime);

//         end;

        if EndDate <> DateZ then
          begin
            if EndDate <> '' then
             begin  // ������ ��� ���.
              taDay.Append;
  //            taDayZnum.AsInteger:=Znum;
              taDayKolCh.AsInteger:=KolCh;
              taDayQuests.AsInteger:=Quests;
              taDaySumma.AsFloat:=rS;
              taDaySUMDISC.AsFloat:=rD;
              taDayDate.AsString:=EndDate;
              taDay.Post;
             end;
            EndDate := DateZ;
 //           Znum := quRepDayZNUM.AsInteger;
//            EndDate:= quRepDayEndTime.AsDateTime;
            KolCh:=0;
            rS:=0; rD:=0;
            Quests:=0;
          end;

         KolCh:=KolCh + 1;
         Quests:=Quests+quRepDayQuests.AsInteger;
         rS:=rS+quRepDaySumma.AsFloat;
         rD:=rD+quRepDaySUMDISC.AsFloat;


         quRepDay.Next;

      end;

      quRepDay.Active:=False;
 }
 
      frRepMain2.LoadFromFile(CurDir + 'SaleBalans.frf');

      frVariables.Variable['sPeriod']:='c '+FormatDateTime('dd.mm.yyyy hh.nn',DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy hh.nn',DateEnd);
      frVariables.Variable['Depart']:=CommonSet.DepartName;

      frRepMain2.ReportName:='������.';
      frRepMain2.PrepareReport;
      frRepMain2.ShowPreparedReport;
    end;
  end;
  fmPeriod.Release;

end;

procedure TfmMainRep.acTabsDayExecute(Sender: TObject);
begin
  with dmC do
  begin
    taTabsSel.Active:=False;
    taTabsSel.Active:=True;
    taTabsSel.Last;

    quSelSpec.Active:=False;
    quSelSpec.Active:=True;
  end;
  fmTabsDay.Caption:='�������� ������';
  fmTabsDay.show;

end;

procedure TfmMainRep.AcPCCliExecute(Sender: TObject);
begin
  with dmC do
  begin
    quPCCli.Active:=False;
    quPCCli.ParamByName('DateB').AsDateTime:=TrebSel.DateFrom;
    quPCCli.ParamByName('DateE').AsDateTime:=TrebSel.DateTo;
    quPCCli.Active:=True;
  end;

  fmPCCliRep.Caption:='���������� �� �� �� �������� �� ������ � '+FormatDateTime('dd.mm.yyyy hh:nn',TrebSel.DateFrom)+' �� '+FormatDateTime('dd.mm.yyyy hh:nn',TrebSel.DateTo);
  fmPCCliRep.Show;

end;


procedure TfmMainRep.AcSkidkiExecute(Sender: TObject);
Var DateBeg,DateEnd:TDateTime;
    par:Variant;

begin
// ������ � �������
  if not CanDo('CashSkidki') then  StatusBar1.Panels[0].Text:='��� ����.';
  fmPeriod:=TfmPeriod.Create(Application);

  fmPeriod.ShowModal;
  if fmPeriod.ModalResult=mrOk then
  begin
    Datebeg:=TrebSel.DateFrom;
    DateEnd:=TrebSel.DateTo;

    with dmC do
    begin

  // ����� ������
     quRepSkidk1.Active:=False;
     quRepSkidk1.ParamByName('DateB').AsDateTime:=Datebeg;
     quRepSkidk1.ParamByName('DateE').AsDateTime:=DateEnd;
     quRepSkidk1.Active:=True;

     taSkidki.Active:=False;
     taSkidki.CreateDataSet;

     quRepSkidk1.First;
     while not quRepSkidk1.Eof do
     begin
       taSkidki.Append;
       taSkidkiKOLCH.AsInteger:=0;
       taSkidkiDISCPERCENT.AsString:=quRepSkidk1DISCPERCENT.AsString;
       taSkidkiSumD.AsFloat:=quRepSkidk1SumD.AsFloat;
       taSkidki.Post;

       quRepSkidk1.Next;
     end;

  // ���-�� ����� ������
      quRepSkidk2.Active:=False;
      quRepSkidk2.ParamByName('DateB').AsDateTime:=Datebeg;
      quRepSkidk2.ParamByName('DateE').AsDateTime:=DateEnd;
      quRepSkidk2.Active:=True;

      par := VarArrayCreate([0,1], varInteger);

     quRepSkidk2.First;
     while not quRepSkidk2.Eof do
     begin

        par[0]:=quRepSkidk2DISCPERCENT.AsInteger;
        if taSkidki.Locate('DISCPERCENT',par,[]) then
         begin
           taSkidki.Edit;
           taSkidkiKOLCH.AsInteger:=quRepSkidk2KOLCH.AsInteger;
           taSkidki.Post;
         end;
         quRepSkidk2.Next;

     end;

      frRepMain2.LoadFromFile(CurDir + 'SaleCashSkidki.frf');

      frVariables.Variable['sPeriod']:='c '+FormatDateTime('dd.mm.yyyy hh.nn',DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy hh.nn',DateEnd);
      frVariables.Variable['Depart']:=CommonSet.DepartName;

      frRepMain2.ReportName:='������ � �������.';
      frRepMain2.PrepareReport;
      frRepMain2.ShowPreparedReport;
    end;
  end;
  fmPeriod.Release;

end;

procedure TfmMainRep.AcCategWaterExecute(Sender: TObject);
Var DateBeg,DateEnd:TDateTime;
begin
// ��������� ���� �� ����������

  if not CanDo('AcCategWater') then  StatusBar1.Panels[0].Text:='��� ����.';

  fmPeriod:=TfmPeriod.Create(Application);

  fmPeriod.ShowModal;
  if fmPeriod.ModalResult=mrOk then
  begin
    Datebeg:=TrebSel.DateFrom;
    DateEnd:=TrebSel.DateTo;

    with dmC do
    begin
      quRep2_3.Active:=False;
// ������ ������

      quRep2_3.ParamByName('DateB').AsDateTime:=Datebeg;
      quRep2_3.ParamByName('DateE').AsDateTime:=DateEnd;

      frRepMain2.LoadFromFile(CurDir + 'CategWater.frf');

      frVariables.Variable['sPeriod']:='c '+FormatDateTime('dd.mm.yyyy hh.nn',DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy hh.nn',DateEnd);
      frVariables.Variable['Depart']:=CommonSet.DepartName;

      frRepMain2.ReportName:='��������� ���� �� ����������.';
      frRepMain2.PrepareReport;
      frRepMain2.ShowPreparedReport;
    end;
  end;
  fmPeriod.Release;

end;


procedure TfmMainRep.AcSkidkiFullExecute(Sender: TObject);
begin
  with dmC do
  begin
    quSkidkiFull.Active:=False;
    quSkidkiFull.ParamByName('DateB').AsDateTime:=TrebSel.DateFrom;
    quSkidkiFull.ParamByName('DateE').AsDateTime:=TrebSel.DateTo;
    quSkidkiFull.Active:=True;
  end;

  fmRepSkidkiFull.Caption:='������ �� ������ � '+FormatDateTime('dd.mm.yyyy hh:nn',TrebSel.DateFrom)+' �� '+FormatDateTime('dd.mm.yyyy hh:nn',TrebSel.DateTo);
  fmRepSkidkiFull.Show;

end;

end.
