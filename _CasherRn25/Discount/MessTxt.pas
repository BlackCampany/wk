unit MessTxt;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons, ExtCtrls,
  cxControls, cxContainer, cxEdit, cxTextEdit, cxMemo;

type
  TfmMessTxt = class(TForm)
    Panel1: TPanel;
    Button1: TcxButton;
    Memo1: TcxMemo;
    Button2: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmMessTxt: TfmMessTxt;

implementation

{$R *.dfm}

procedure TfmMessTxt.FormCreate(Sender: TObject);
begin
  Memo1.Align:=AlClient;
  Memo1.Clear;
end;

procedure TfmMessTxt.Button1Click(Sender: TObject);
begin
  Close;
end;

end.
