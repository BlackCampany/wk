unit DocsOut;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SpeedBar, ExtCtrls, ComCtrls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Placemnt, cxImageComboBox, XPStyleActnCtrls, ActnList, ActnMan;

type
  TfmDocsOut = class(TForm)
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    Timer1: TTimer;
    FormPlacement1: TFormPlacement;
    GridDocsOut: TcxGrid;
    ViewDocsOut: TcxGridDBTableView;
    LevelDocsOut: TcxGridLevel;
    ViewDocsOutID: TcxGridDBColumn;
    ViewDocsOutDATEDOC: TcxGridDBColumn;
    ViewDocsOutNUMDOC: TcxGridDBColumn;
    ViewDocsOutDATESF: TcxGridDBColumn;
    ViewDocsOutNUMSF: TcxGridDBColumn;
    ViewDocsOutIDCLI: TcxGridDBColumn;
    ViewDocsOutIDSKL: TcxGridDBColumn;
    ViewDocsOutSUMIN: TcxGridDBColumn;
    ViewDocsOutSUMUCH: TcxGridDBColumn;
    ViewDocsOutSUMTAR: TcxGridDBColumn;
    ViewDocsOutSUMNDS0: TcxGridDBColumn;
    ViewDocsOutSUMNDS1: TcxGridDBColumn;
    ViewDocsOutSUMNDS2: TcxGridDBColumn;
    ViewDocsOutPROCNAC: TcxGridDBColumn;
    ViewDocsOutNAMECL: TcxGridDBColumn;
    ViewDocsOutNAMEMH: TcxGridDBColumn;
    ViewDocsOutIACTIVE: TcxGridDBColumn;
    amDocsOut: TActionManager;
    acPeriod: TAction;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    SpeedItem6: TSpeedItem;
    acAddDoc1: TAction;
    acEditDoc1: TAction;
    acViewDoc1: TAction;
    acDelDoc1: TAction;
    acOnDoc1: TAction;
    acOffDoc1: TAction;
    SpeedItem7: TSpeedItem;
    SpeedItem8: TSpeedItem;
    procedure SpeedItem1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acPeriodExecute(Sender: TObject);
    procedure acAddDoc1Execute(Sender: TObject);
    procedure acEditDoc1Execute(Sender: TObject);
    procedure acViewDoc1Execute(Sender: TObject);
    procedure ViewDocsOutDblClick(Sender: TObject);
    procedure acDelDoc1Execute(Sender: TObject);
    procedure acOnDoc1Execute(Sender: TObject);
    procedure acOffDoc1Execute(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmDocsOut: TfmDocsOut;
  bClearDocOut:Boolean = false;

implementation

uses Un1, dmOffice, PeriodUni, AddDoc2, SelPartIn1, DMOReps;

{$R *.dfm}

procedure TfmDocsOut.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmDocsOut.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  Timer1.Enabled:=True;
  GridDocsOut.Align:=AlClient;
  ViewDocsOut.RestoreFromIniFile(CurDir+GridIni);
  //���� ����� ������� �������� ������ ���
  with dmO do
  begin
    if taNDS.Active=False then taNDS.Active:=True;
    taNDS.First;
    while not taNDS.Eof do
    begin
      if taNDSID.AsInteger=1 then ViewDocsOutSUMNDS0.Caption:=taNDSNAMENDS.AsString;
      if taNDSID.AsInteger=2 then ViewDocsOutSUMNDS1.Caption:=taNDSNAMENDS.AsString;
      if taNDSID.AsInteger=3 then ViewDocsOutSUMNDS2.Caption:=taNDSNAMENDS.AsString;
      taNDS.Next;
    end;
  end;
  StatusBar1.Color:=$00FFCACA;
end;

procedure TfmDocsOut.FormClose(Sender: TObject; var Action: TCloseAction);
begin
   ViewDocsOut.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmDocsOut.acPeriodExecute(Sender: TObject);
begin
//  ������
  fmPeriodUni.DateTimePicker1.Date:=CommonSet.DateFrom;
//  fmPeriodUni.DateTimePicker2.Date:=CommonSet.DateTo-1;
  fmPeriodUni.ShowModal;
  if fmPeriodUni.ModalResult=mrOk then
  begin
    CommonSet.DateFrom:=Trunc(fmPeriodUni.DateTimePicker1.Date);
    CommonSet.DateTo:=Trunc(fmPeriodUni.DateTimePicker2.Date)+1;

    if CommonSet.DateTo>=iMaxDate then fmDocsOut.Caption:='��������� ��������� � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
    else fmDocsOut.Caption:='��������� ��������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);
    with dmO do
    begin
      ViewDocsOut.BeginUpdate;
      quDocsOutSel.Active:=False;
      quDocsOutSel.ParamByName('DATEB').AsDate:=CommonSet.DateFrom;
      quDocsOutSel.ParamByName('DATEE').AsDate:=CommonSet.DateTo;
      quDocsOutSel.Active:=True;
      ViewDocsOut.EndUpdate;
    end;
  end;
end;

procedure TfmDocsOut.acAddDoc1Execute(Sender: TObject);
Var IDH:INteger;
    rSum1,rSum2:Real;
begin
  //�������� ��������

  if not CanDo('prAddDocOut') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmO do
  begin
    fmAddDoc2.Caption:='���������: ����� ��������.';
    fmAddDoc2.cxTextEdit1.Text:='';
    fmAddDoc2.cxTextEdit1.Properties.ReadOnly:=False;
    fmAddDoc2.cxTextEdit2.Text:='';
    fmAddDoc2.cxTextEdit2.Properties.ReadOnly:=False;
    fmAddDoc2.cxDateEdit1.Date:=Date;
    fmAddDoc2.cxDateEdit1.Properties.ReadOnly:=False;
    fmAddDoc2.cxDateEdit2.Date:=Date;
    fmAddDoc2.cxDateEdit2.Properties.ReadOnly:=False;
    fmAddDoc2.cxCurrencyEdit1.EditValue:=0;
    fmAddDoc2.cxCurrencyEdit2.EditValue:=0;

    if taNDS.Active=False then taNDS.Active:=True;
    taNds.First;
    if not taNDS.Eof then fmAddDoc2.Label7.Caption:=taNDSNAMENDS.AsString; vNds[1]:=taNDSPROC.AsFloat; taNds.Next;
    if not taNDS.Eof then fmAddDoc2.Label9.Caption:=taNDSNAMENDS.AsString; vNds[2]:=taNDSPROC.AsFloat; taNds.Next;
    if not taNDS.Eof then fmAddDoc2.Label10.Caption:=taNDSNAMENDS.AsString; vNds[3]:=taNDSPROC.AsFloat;
    taNds.First;
    fmAddDoc2.Label11.Caption:='0.00�.';
    fmAddDoc2.Label13.Caption:='0.00�.';
    fmAddDoc2.Label14.Caption:='0.00�.';

    fmAddDoc2.cxButtonEdit1.Tag:=0;
    fmAddDoc2.cxButtonEdit1.EditValue:=0;
    fmAddDoc2.cxButtonEdit1.Text:='';
    fmAddDoc2.cxButtonEdit1.Properties.ReadOnly:=False;

    if quMHAll.Active=False then quMHAll.Active:=True;
    quMHAll.FullRefresh;

    fmAddDoc2.cxLookupComboBox1.EditValue:=0;
    fmAddDoc2.cxLookupComboBox1.Text:='';
    fmAddDoc2.cxLookupComboBox1.Properties.ReadOnly:=False;

    if CurVal.IdMH<>0 then
    begin
      fmAddDoc2.cxLookupComboBox1.EditValue:=CurVal.IdMH;
      fmAddDoc2.cxLookupComboBox1.Text:=CurVal.NAMEMH;
    end else
    begin
       quMHAll.First;
       if not quMHAll.Eof then
       begin
         CurVal.IdMH:=quMHAllID.AsInteger;
         CurVal.NAMEMH:=quMHAllNAMEMH.AsString;
         fmAddDoc2.cxLookupComboBox1.EditValue:=CurVal.IdMH;
         fmAddDoc2.cxLookupComboBox1.Text:=CurVal.NAMEMH;
       end;
    end;
    if quMHAll.Locate('ID',CurVal.IdMH,[]) then
    begin
      fmAddDoc2.Label15.Caption:='��. ����: '+quMHAllNAMEPRICE.AsString;
      fmAddDoc2.Label15.Tag:=quMHAllDEFPRICE.AsInteger;
    end else
    begin
      fmAddDoc2.Label15.Caption:='��. ����: ';
      fmAddDoc2.Label15.Tag:=0;
    end;

    fmAddDoc2.cxLabel1.Enabled:=True;
    fmAddDoc2.cxLabel2.Enabled:=True;
    fmAddDoc2.cxLabel3.Enabled:=True;
    fmAddDoc2.cxLabel4.Enabled:=True;
    fmAddDoc2.cxLabel5.Enabled:=True;
    fmAddDoc2.cxLabel6.Enabled:=True;


    fmAddDoc2.cxButton1.Enabled:=True;
    fmAddDoc2.taSpecOut.Active:=False;
    fmAddDoc2.taSpecOut.CreateDataSet;

    fmAddDoc2.ShowModal;
    if fmAddDoc2.ModalResult=mrOk then
    begin
      //��������
      IDH:=GetId('DocOut');
      quSpecOutSel.Active:=False;
      quSpecOutSel.ParamByName('IDHD').AsInteger:=IDH;
      quSpecOutSel.Active:=True;
      with fmAddDoc2 do
      begin
        sNDS[1]:=0;sNDS[2]:=0;sNDS[3]:=0;
        rSum1:=0; rSum2:=0;

        //������� ��������� � �������� ������
        quDocsOutSel.Append;
        quDocsOutSelID.AsInteger:=IDH;
        quDocsOutSelDATEDOC.AsDateTime:=Trunc(cxDateEdit1.Date);
        quDocsOutSelNUMDOC.AsString:=cxTextEdit1.Text;
        quDocsOutSelDATESF.AsDateTime:=Trunc(cxDateEdit2.Date);
        quDocsOutSelNUMSF.AsString:=cxTextEdit2.Text;
        quDocsOutSelIDCLI.AsInteger:=cxButtonEdit1.Tag;
        quDocsOutSelIDSKL.AsInteger:=cxLookupComboBox1.EditValue;
        quDocsOutSelSUMIN.AsFloat:=rSum1;
        quDocsOutSelSUMUCH.AsFloat:=rSum2;
        quDocsOutSelSUMTAR.AsFloat:=0;
        quDocsOutSelSUMNDS0.AsFloat:=sNDS[1];
        quDocsOutSelSUMNDS1.AsFloat:=sNDS[2];
        quDocsOutSelSUMNDS2.AsFloat:=sNDS[3];
        quDocsOutSelPROCNAC.AsFloat:=0;
        quDocsOutSelIACTIVE.AsInteger:=0;
        quDocsOutSel.Post;

        //����� ������������
        taSpecOut.First;
        while not taSpecOut.Eof do
        begin
          quSpecOutSel.Append;
          quSpecOutSelIDHEAD.AsInteger:=IDH;
          quSpecOutSelID.AsInteger:=GetId('SpecIn');
          quSpecOutSelNUM.AsInteger:=taSpecOutNum.AsInteger;
          quSpecOutSelIDCARD.AsInteger:=taSpecOutIdGoods.AsInteger;
          quSpecOutSelQUANT.AsFloat:=taSpecOutQuant.AsFloat;
          quSpecOutSelPRICEIN.AsFloat:=taSpecOutPrice1.AsFloat;
          quSpecOutSelSUMIN.AsFloat:=taSpecOutSum1.AsFloat;
          quSpecOutSelPRICEUCH.AsFloat:=taSpecOutPrice2.AsFloat;
          quSpecOutSelSUMUCH.AsFloat:=taSpecOutSum2.AsFloat;
          quSpecOutSelIDNDS.AsInteger:=taSpecOutINds.AsInteger;
          quSpecOutSelSUMNDS.AsFloat:=taSpecOutRNds.AsFloat;
          quSpecOutSelIDM.AsInteger:=taSpecOutIM.AsInteger;
          quSpecOutSel.Post;
          sNDS[taSpecOutINds.AsInteger]:=sNDS[taSpecOutINds.AsInteger]+taSpecOutRNds.AsFloat;
          rSum1:=rSum1+taSpecOutSum1.AsFloat; rSum2:=rSum2+taSpecOutSum2.AsFloat;
          taSpecOut.Next;
        end;
        //������ ��������� ���������
        taSpecOut.Active:=False;

        quDocsOutSel.Edit;
        quDocsOutSelSUMIN.AsFloat:=rSum1;
        quDocsOutSelSUMUCH.AsFloat:=rSum2;
        quDocsOutSelSUMNDS0.AsFloat:=sNDS[1];
        quDocsOutSelSUMNDS1.AsFloat:=sNDS[2];
        quDocsOutSelSUMNDS2.AsFloat:=sNDS[3];
        quDocsOutSel.Post;

        quDocsOutSel.Refresh;
      end;
      quSpecOutSel.Active:=False;
    end;
  end;
end;

procedure TfmDocsOut.acEditDoc1Execute(Sender: TObject);
Var IDH:INteger;
    rSum1,rSum2:Real;
begin
  //�������������
  if not CanDo('prEditDocOut') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmO do
  begin
    if quDocsOutSel.RecordCount>0 then //���� ��� �������������
    begin
      if quDocsOutSelIACTIVE.AsInteger=0 then
      begin
        fmAddDoc2.Caption:='���������: ��������������.';
        fmAddDoc2.cxTextEdit1.Text:=quDocsOutSelNUMDOC.AsString;
        fmAddDoc2.cxTextEdit1.Properties.ReadOnly:=False;
        fmAddDoc2.cxTextEdit2.Text:=quDocsOutSelNUMSF.AsString;
        fmAddDoc2.cxTextEdit2.Properties.ReadOnly:=False;
        fmAddDoc2.cxDateEdit1.Date:=quDocsOutSelDATEDOC.AsDateTime;
        fmAddDoc2.cxDateEdit1.Properties.ReadOnly:=False;
        fmAddDoc2.cxDateEdit2.Date:=quDocsOutSelDATESF.AsDateTime;
        fmAddDoc2.cxDateEdit2.Properties.ReadOnly:=False;
        fmAddDoc2.cxCurrencyEdit1.EditValue:=quDocsOutSelSUMIN.AsCurrency;
        fmAddDoc2.cxCurrencyEdit2.EditValue:=quDocsOutSelSUMUCH.AsCurrency;

        if taNDS.Active=False then taNDS.Active:=True;
        taNds.First;
        if not taNDS.Eof then fmAddDoc2.Label7.Caption:=taNDSNAMENDS.AsString; vNds[1]:=taNDSPROC.AsFloat; taNds.Next;
        if not taNDS.Eof then fmAddDoc2.Label9.Caption:=taNDSNAMENDS.AsString; vNds[2]:=taNDSPROC.AsFloat; taNds.Next;
        if not taNDS.Eof then fmAddDoc2.Label10.Caption:=taNDSNAMENDS.AsString; vNds[3]:=taNDSPROC.AsFloat;
        taNds.First;
        fmAddDoc2.Label11.Caption:=quDocsOutSelSUMNDS0.AsString;
        fmAddDoc2.Label13.Caption:=quDocsOutSelSUMNDS1.AsString;
        fmAddDoc2.Label14.Caption:=quDocsOutSelSUMNDS2.AsString;

        fmAddDoc2.cxButtonEdit1.Tag:=quDocsOutSelIDCLI.AsInteger;
        fmAddDoc2.cxButtonEdit1.EditValue:=quDocsOutSelIDCLI.AsInteger;
        fmAddDoc2.cxButtonEdit1.Text:=quDocsOutSelNAMECL.AsString;
        fmAddDoc2.cxButtonEdit1.Properties.ReadOnly:=False;

        if quMHAll.Active=False then quMHAll.Active:=True;
        quMHAll.FullRefresh;

        fmAddDoc2.cxLookupComboBox1.EditValue:=quDocsOutSelIDSKL.AsInteger;
        fmAddDoc2.cxLookupComboBox1.Text:=quDocsOutSelNAMEMH.AsString;
        fmAddDoc2.cxLookupComboBox1.Properties.ReadOnly:=False;

        CurVal.IdMH:=quDocsOutSelIDSKL.AsInteger;
        CurVal.NAMEMH:=quDocsOutSelNAMEMH.AsString;

        if quMHAll.Locate('ID',CurVal.IdMH,[]) then
        begin
          fmAddDoc2.Label15.Caption:='��. ����: '+quMHAllNAMEPRICE.AsString;
          fmAddDoc2.Label15.Tag:=quMHAllDEFPRICE.AsInteger;
        end else
        begin
          fmAddDoc2.Label15.Caption:='��. ����: ';
          fmAddDoc2.Label15.Tag:=0;
        end;

        fmAddDoc2.cxLabel1.Enabled:=True;
        fmAddDoc2.cxLabel2.Enabled:=True;
        fmAddDoc2.cxLabel3.Enabled:=True;
        fmAddDoc2.cxLabel4.Enabled:=True;
        fmAddDoc2.cxLabel5.Enabled:=True;
        fmAddDoc2.cxLabel6.Enabled:=True;
        fmAddDoc2.cxButton1.Enabled:=True;

        fmAddDoc2.taSpecOut.Active:=False;
        fmAddDoc2.taSpecOut.CreateDataSet;

        IDH:=quDocsOutSelID.AsInteger;

        quSpecOutSel.Active:=False;
        quSpecOutSel.ParamByName('IDHD').AsInteger:=IDH;
        quSpecOutSel.Active:=True;

        quSpecOutSel.First;
        while not quSpecOutSel.Eof do
        begin
          with fmAddDoc2 do
          begin
            taSpecOut.Append;
            taSpecOutNum.AsInteger:=quSpecOutSelNUM.AsInteger;
            taSpecOutIdGoods.AsInteger:=quSpecOutSelIDCARD.AsInteger;
            taSpecOutNameG.AsString:=quSpecOutSelNAMEC.AsString;
            taSpecOutIM.AsInteger:=quSpecOutSelIDM.AsInteger;
            taSpecOutSM.AsString:=quSpecOutSelSM.AsString;
            taSpecOutQuant.AsFloat:=quSpecOutSelQUANT.AsFloat;
            taSpecOutPrice1.AsCurrency:=quSpecOutSelPRICEIN.AsCurrency;
            taSpecOutSum1.AsCurrency:=quSpecOutSelSUMIN.AsCurrency;
            taSpecOutPrice2.AsCurrency:=quSpecOutSelPRICEUCH.AsCurrency;
            taSpecOutSum2.AsCurrency:=quSpecOutSelSUMUCH.AsCurrency;
            taSpecOutINds.AsInteger:=quSpecOutSelIDNDS.AsInteger;
            taSpecOutSNds.AsString:=quSpecOutSelNAMENDS.AsString;
            taSpecOutRNds.AsCurrency:=quSpecOutSelSUMNDS.AsCurrency;
            taSpecOutSumNac.AsCurrency:=quSpecOutSelSUMUCH.AsCurrency-quSpecOutSelSUMIN.AsCurrency;
            taSpecOutProcNac.AsCurrency:=0;
            if quSpecOutSelSUMIN.AsCurrency<>0 then
              taSpecOutProcNac.AsCurrency:=RoundEx((quSpecOutSelSUMUCH.AsCurrency-quSpecOutSelSUMIN.AsCurrency)/quSpecOutSelSUMIN.AsCurrency*10000)/100;
            taSpecOut.Post;
          end;
          quSpecOutSel.Next;
        end;

        fmAddDoc2.ShowModal;
        if fmAddDoc2.ModalResult=mrOk then
        begin //��������
          quSpecOutSel.Active:=False;
          quSpecOutSel.ParamByName('IDHD').AsInteger:=IDH;
          quSpecOutSel.Active:=True;

          quSpecOutSel.First; //������� ������
          while not quSpecOutSel.Eof do quSpecOutSel.Delete;

          with fmAddDoc2 do
          begin
            sNDS[1]:=0;sNDS[2]:=0;sNDS[3]:=0;
            rSum1:=0; rSum2:=0;
            //������� ������������
            taSpecOut.First;
            while not taSpecOut.Eof do
            begin
              quSpecOutSel.Append;
              quSpecOutSelIDHEAD.AsInteger:=IDH;
              quSpecOutSelID.AsInteger:=GetId('SpecIn');
              quSpecOutSelNUM.AsInteger:=taSpecOutNum.AsInteger;
              quSpecOutSelIDCARD.AsInteger:=taSpecOutIdGoods.AsInteger;
              quSpecOutSelQUANT.AsFloat:=taSpecOutQuant.AsFloat;
              quSpecOutSelPRICEIN.AsFloat:=taSpecOutPrice1.AsFloat;
              quSpecOutSelSUMIN.AsFloat:=taSpecOutSum1.AsFloat;
              quSpecOutSelPRICEUCH.AsFloat:=taSpecOutPrice2.AsFloat;
              quSpecOutSelSUMUCH.AsFloat:=taSpecOutSum2.AsFloat;
              quSpecOutSelIDNDS.AsInteger:=taSpecOutINds.AsInteger;
              quSpecOutSelSUMNDS.AsFloat:=taSpecOutRNds.AsFloat;
              quSpecOutSelIDM.AsInteger:=taSpecOutIM.AsInteger;
              quSpecOutSel.Post;
              sNDS[taSpecOutINds.AsInteger]:=sNDS[taSpecOutINds.AsInteger]+taSpecOutRNds.AsFloat;
              rSum1:=rSum1+taSpecOutSum1.AsFloat; rSum2:=rSum2+taSpecOutSum2.AsFloat;

              taSpecOut.Next;
            end;

            taSpecOut.Active:=False;
        //������ ���������
            quDocsOutSel.Edit;
            quDocsOutSelID.AsInteger:=IDH;
            quDocsOutSelDATEDOC.AsDateTime:=Trunc(cxDateEdit1.Date);
            quDocsOutSelNUMDOC.AsString:=cxTextEdit1.Text;
            quDocsOutSelDATESF.AsDateTime:=Trunc(cxDateEdit2.Date);
            quDocsOutSelNUMSF.AsString:=cxTextEdit2.Text;
            quDocsOutSelIDCLI.AsInteger:=cxButtonEdit1.Tag;
            quDocsOutSelIDSKL.AsInteger:=cxLookupComboBox1.EditValue;
            quDocsOutSelSUMIN.AsFloat:=rSum1;
            quDocsOutSelSUMUCH.AsFloat:=rSum2;
            quDocsOutSelSUMTAR.AsFloat:=0;
            quDocsOutSelSUMNDS0.AsFloat:=sNDS[1];
            quDocsOutSelSUMNDS1.AsFloat:=sNDS[2];
            quDocsOutSelSUMNDS2.AsFloat:=sNDS[3];
            quDocsOutSelPROCNAC.AsFloat:=0;
            quDocsOutSelIACTIVE.AsInteger:=0;
            quDocsOutSel.Post;
            delay(100);
            quDocsOutSel.Refresh;
          end;
          quSpecOutSel.Active:=False;
        end;
      end else
      begin
        showmessage('������������� �������������� �������� ������.');
      end;
    end else
    begin
      showmessage('�������� �������� ��� ��������������.');
    end;
  end;
end;

procedure TfmDocsOut.acViewDoc1Execute(Sender: TObject);
Var IDH:INteger;
begin
  //��������
  if not CanDo('prViewDocOut') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmO do
  begin
    if quDocsOutSel.RecordCount>0 then //���� ��� �������������
    begin
      fmAddDoc2.Caption:='���������: ��������.';
      fmAddDoc2.cxTextEdit1.Text:=quDocsOutSelNUMDOC.AsString;
      fmAddDoc2.cxTextEdit1.Properties.ReadOnly:=True;
      fmAddDoc2.cxTextEdit2.Text:=quDocsOutSelNUMSF.AsString;
      fmAddDoc2.cxTextEdit2.Properties.ReadOnly:=True;
      fmAddDoc2.cxDateEdit1.Date:=quDocsOutSelDATEDOC.AsDateTime;
      fmAddDoc2.cxDateEdit1.Properties.ReadOnly:=True;
      fmAddDoc2.cxDateEdit2.Date:=quDocsOutSelDATESF.AsDateTime;
      fmAddDoc2.cxDateEdit2.Properties.ReadOnly:=True;
      fmAddDoc2.cxCurrencyEdit1.EditValue:=quDocsOutSelSUMIN.AsCurrency;
      fmAddDoc2.cxCurrencyEdit2.EditValue:=quDocsOutSelSUMUCH.AsCurrency;


      if taNDS.Active=False then taNDS.Active:=True;
      taNds.First;
      if not taNDS.Eof then fmAddDoc2.Label7.Caption:=taNDSNAMENDS.AsString; vNds[1]:=taNDSPROC.AsFloat; taNds.Next;
      if not taNDS.Eof then fmAddDoc2.Label9.Caption:=taNDSNAMENDS.AsString; vNds[2]:=taNDSPROC.AsFloat; taNds.Next;
      if not taNDS.Eof then fmAddDoc2.Label10.Caption:=taNDSNAMENDS.AsString; vNds[3]:=taNDSPROC.AsFloat;
      taNds.First;
      fmAddDoc2.Label11.Caption:=quDocsOutSelSUMNDS0.AsString;
      fmAddDoc2.Label13.Caption:=quDocsOutSelSUMNDS1.AsString;
      fmAddDoc2.Label14.Caption:=quDocsOutSelSUMNDS2.AsString;

      fmAddDoc2.cxButtonEdit1.Tag:=quDocsOutSelIDCLI.AsInteger;
      fmAddDoc2.cxButtonEdit1.EditValue:=quDocsOutSelIDCLI.AsInteger;
      fmAddDoc2.cxButtonEdit1.Text:=quDocsOutSelNAMECL.AsString;
      fmAddDoc2.cxButtonEdit1.Properties.ReadOnly:=True;

      quMHAll.Active:=False; quMHAll.Active:=True;

      fmAddDoc2.cxLookupComboBox1.EditValue:=quDocsOutSelIDSKL.AsInteger;
      fmAddDoc2.cxLookupComboBox1.Text:=quDocsOutSelNAMEMH.AsString;
      fmAddDoc2.cxLookupComboBox1.Properties.ReadOnly:=True;

      CurVal.IdMH:=quDocsOutSelIDSKL.AsInteger;
      CurVal.NAMEMH:=quDocsOutSelNAMEMH.AsString;

      if quMHAll.Locate('ID',CurVal.IdMH,[]) then
      begin
        fmAddDoc2.Label15.Caption:='��. ����: '+quMHAllNAMEPRICE.AsString;
        fmAddDoc2.Label15.Tag:=quMHAllDEFPRICE.AsInteger;
      end else
      begin
        fmAddDoc2.Label15.Caption:='��. ����: ';
        fmAddDoc2.Label15.Tag:=0;
      end;

      fmAddDoc2.cxLabel1.Enabled:=False;
      fmAddDoc2.cxLabel2.Enabled:=False;
      fmAddDoc2.cxLabel3.Enabled:=False;
      fmAddDoc2.cxLabel4.Enabled:=False;
      fmAddDoc2.cxLabel5.Enabled:=False;
      fmAddDoc2.cxLabel6.Enabled:=False;
      fmAddDoc2.cxButton1.Enabled:=False;

      fmAddDoc2.taSpecOut.Active:=False;
      fmAddDoc2.taSpecOut.CreateDataSet;

      IDH:=quDocsOutSelID.AsInteger;

      quSpecOutSel.Active:=False;
      quSpecOutSel.ParamByName('IDHD').AsInteger:=IDH;
      quSpecOutSel.Active:=True;

      quSpecOutSel.First;
      while not quSpecOutSel.Eof do
      begin
        with fmAddDoc2 do
        begin
          taSpecOut.Append;
          taSpecOutNum.AsInteger:=quSpecOutSelNUM.AsInteger;
          taSpecOutIdGoods.AsInteger:=quSpecOutSelIDCARD.AsInteger;
          taSpecOutNameG.AsString:=quSpecOutSelNAMEC.AsString;
          taSpecOutIM.AsInteger:=quSpecOutSelIDM.AsInteger;
          taSpecOutSM.AsString:=quSpecOutSelSM.AsString;
          taSpecOutQuant.AsFloat:=quSpecOutSelQUANT.AsFloat;
          taSpecOutPrice1.AsCurrency:=quSpecOutSelPRICEIN.AsCurrency;
          taSpecOutSum1.AsCurrency:=quSpecOutSelSUMIN.AsCurrency;
          taSpecOutPrice2.AsCurrency:=quSpecOutSelPRICEUCH.AsCurrency;
          taSpecOutSum2.AsCurrency:=quSpecOutSelSUMUCH.AsCurrency;
          taSpecOutINds.AsInteger:=quSpecOutSelIDNDS.AsInteger;
          taSpecOutSNds.AsString:=quSpecOutSelNAMENDS.AsString;
          taSpecOutRNds.AsCurrency:=quSpecOutSelSUMNDS.AsCurrency;
          taSpecOutSumNac.AsCurrency:=quSpecOutSelSUMUCH.AsCurrency-quSpecOutSelSUMIN.AsCurrency;
          taSpecOutProcNac.AsCurrency:=0;
          if quSpecOutSelSUMIN.AsCurrency<>0 then
            taSpecOutProcNac.AsCurrency:=RoundEx((quSpecOutSelSUMUCH.AsCurrency-quSpecOutSelSUMIN.AsCurrency)/quSpecOutSelSUMIN.AsCurrency*10000)/100;
          taSpecOut.Post;
        end;
        quSpecOutSel.Next;
      end;

      fmAddDoc2.ShowModal;
      fmAddDoc2.taSpecOut.Active:=False;
    end else
    begin
      showmessage('�������� �������� ��� ���������.');
    end;
  end;
end;

procedure TfmDocsOut.ViewDocsOutDblClick(Sender: TObject);
begin
  //������� �������
  with dmO do
  begin
    if quDocsOutSelIACTIVE.AsInteger=0 then acEditDoc1.Execute //��������������
    else acViewDoc1.Execute; //��������
  end;
end;

procedure TfmDocsOut.acDelDoc1Execute(Sender: TObject);
//Var IDH:INteger;
begin
  //������� ��������
  if not CanDo('prDelDocOut') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmO do
  begin
    if quDocsOutSel.RecordCount>0 then //���� ��� �������������
    begin
      if quDocsOutSelIACTIVE.AsInteger=0 then
      begin
        if MessageDlg('�� ������������� ������ ������� ��������� �'+quDocsOutSelNUMDOC.AsString+' �� '+quDocsOutSelNAMECL.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
//          IDH:=quDocsOutSelID.AsInteger;
          quDocsOutSel.Delete;

{          quSpecOutSel.Active:=False;
          quSpecOutSel.ParamByName('IDHD').AsInteger:=IDH;
          quSpecOutSel.Active:=True;

          quSpecOutSel.First; //������
          while not quSpecOutSel.Eof do quSpecOutSel.Delete;
          quSpecOutSel.Active:=False;
}          
        end;
      end else
      begin
        showmessage('������� �������������� �������� ������.');
      end;
    end else
    begin
      showmessage('�������� �������� ��� ��������.');
    end;
  end;
end;

procedure TfmDocsOut.acOnDoc1Execute(Sender: TObject);
Var IdH:INteger;
    rQP,rQs,PriceSp,k,rQ,PriceSpOut:Real;
begin
//������������
  if not CanDo('prOnDocOut') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmO do
  with dmORep do
  begin
    if quDocsOutSel.RecordCount>0 then //���� ��� ������������
    begin
      if quDocsOutSelIACTIVE.AsInteger=0 then
      begin
        if MessageDlg('������������ �������� �'+quDocsOutSelNUMDOC.AsString+' �� '+quDocsOutSelNAMECL.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          if prTOFind(Trunc(quDocsOutSelDATEDOC.AsDateTime),quDocsOutSelIDSKL.AsInteger)=1 then
          begin //�� ����
            if MessageDlg('������� ��� �������� ������ �� �� '+quDocsOutSelNAMEMH.AsString+' � '+quDocsOutSelNUMDOC.AsString+'?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
              prTODel(Trunc(quDocsOutSelDATEDOC.AsDateTime),quDocsOutSelIDSKL.AsInteger)
            else
            begin
              showmessage('��������� ������� ��������� ����������...');
              exit;
            end;
          end;


         // 1 - ������� ��������� ������
         // 2 - �������� ��������������
         // 3 - �������� ������

          IDH:=quDocsOutSelID.AsInteger;
          if quDocsOutSelIDSKL.AsInteger>0 then
          begin
            taPartTest.Active:=False;
            taPartTest.CreateDataSet;

            quSpecOutSel.Active:=False;
            quSpecOutSel.ParamByName('IDHD').AsInteger:=IDH;
            quSpecOutSel.Active:=True;

            quSpecOutSel.First;
            while not quSpecOutSel.Eof do
            begin
              k:=prFindKM(quSpecOutSelIM.AsInteger);
              rQs:=quSpecOutSelQUANT.AsFloat*k; //20*0,5=10
              PriceSp:=quSpecOutSelPRICEIN.AsCurrency/k; //20/0,5=40
              PriceSpOut:=quSpecOutSelPRICEUCH.AsCurrency/k; //20/0,5=40

              prSelPartIn(quSpecOutSelIDCARD.AsInteger,quDocsOutSelIDSKL.AsInteger,quDocsOutSelIDCLI.AsInteger);

              quSelPartIn.First;
              while (not quSelPartIn.Eof) and (rQs>0) do
              begin
                //���� �� ���� ������� ���� �����, ��������� �������� ���
                if (RoundEx(quSelPartInPRICEIN.AsFloat*100)/100)=PriceSp then
                begin //��� ��� ������
                  rQp:=quSelPartInQREMN.AsFloat;
                  if rQs<=rQp then  rQ:=rQs//��������� ������ ������ ���������
                              else  rQ:=rQp;
                  rQs:=rQs-rQ;
                  //��������� ��������� ������
                  //����������� ��������� ������
//?ARTICUL, ?IDDATE, ?IDSTORE, ?IDPARTIN, ?IDDOC, ?IDCLI, ?DTYPE, ?QUANT, ?PRICEIN, ?SUMOUT)
                  prAddPartOut.ParamByName('ARTICUL').AsInteger:=quSpecOutSelIDCARD.AsInteger;
                  prAddPartOut.ParamByName('IDDATE').AsInteger:=Trunc(quDocsOutSelDATEDOC.AsDateTime);
                  prAddPartOut.ParamByName('IDSTORE').AsInteger:=quDocsOutSelIDSKL.AsInteger;
                  prAddPartOut.ParamByName('IDPARTIN').AsInteger:=quSelPartInID.AsInteger;
                  prAddPartOut.ParamByName('IDDOC').AsInteger:=quDocsOutSelID.AsInteger;
                  if quDocsOutSelIDCLI.AsInteger>0 then
                    prAddPartOut.ParamByName('IDCLI').AsInteger:=quDocsOutSelIDCLI.AsInteger
                    else
                    prAddPartOut.ParamByName('IDCLI').AsInteger:=quSelPartInIDCLI.AsInteger;
                  prAddPartOut.ParamByName('DTYPE').AsInteger:=1;
                  prAddPartOut.ParamByName('QUANT').AsFloat:=rQ;
                  prAddPartOut.ParamByName('PRICEIN').AsFloat:=PriceSp;
                  prAddPartOut.ParamByName('SUMOUT').AsFloat:=PriceSpOut*rQ;
                  prAddPartout.ExecProc;

                end;
                quSelPartIn.Next;
              end;
              quSelPartIn.Active:=False;

              //��������� ��������
              if rQs>0 then //�������� ������������� ������
              begin
                //��������� ��� ����� ���������
                prAddPartOut.ParamByName('ARTICUL').AsInteger:=quSpecOutSelIDCARD.AsInteger;
                prAddPartOut.ParamByName('IDDATE').AsInteger:=Trunc(quDocsOutSelDATEDOC.AsDateTime);
                prAddPartOut.ParamByName('IDSTORE').AsInteger:=quDocsOutSelIDSKL.AsInteger;
                prAddPartOut.ParamByName('IDPARTIN').AsInteger:=-1;
                prAddPartOut.ParamByName('IDDOC').AsInteger:=quDocsOutSelID.AsInteger;
                if quDocsOutSelIDCLI.AsInteger>0 then
                  prAddPartOut.ParamByName('IDCLI').AsInteger:=quDocsOutSelIDCLI.AsInteger
                  else
                  prAddPartOut.ParamByName('IDCLI').AsInteger:=0;
                prAddPartOut.ParamByName('DTYPE').AsInteger:=1;
                prAddPartOut.ParamByName('QUANT').AsFloat:=rQs;
                prAddPartOut.ParamByName('PRICEIN').AsFloat:=PriceSp;
                prAddPartOut.ParamByName('SUMOUT').AsFloat:=PriceSpOut*rQs;
                prAddPartout.ExecProc;

                taPartTest.Append;
                taPartTestNum.AsInteger:=quSpecOutSelNUM.AsInteger;
                taPartTestIdGoods.AsInteger:=quSpecOutSelIDCARD.AsInteger;
                taPartTestNameG.AsString:=quSpecOutSelNAMEC.AsString;
                taPartTestIM.AsInteger:=quSpecOutSelIM.AsInteger;
                taPartTestSM.AsString:=quSpecOutSelSM.AsString;
                taPartTestQuant.AsFloat:=quSpecOutSelQUANT.AsFloat;
                taPartTestPrice1.AsCurrency:=quSpecOutSelPRICEIN.AsCurrency;
                taPartTestiRes.AsInteger:=0;
                taPartTest.Post;

              end else
              begin
                taPartTest.Append;
                taPartTestNum.AsInteger:=quSpecOutSelNUM.AsInteger;
                taPartTestIdGoods.AsInteger:=quSpecOutSelIDCARD.AsInteger;
                taPartTestNameG.AsString:=quSpecOutSelNAMEC.AsString;
                taPartTestIM.AsInteger:=quSpecOutSelIM.AsInteger;
                taPartTestSM.AsString:=quSpecOutSelSM.AsString;
                taPartTestQuant.AsFloat:=quSpecOutSelQUANT.AsFloat;
                taPartTestPrice1.AsCurrency:=quSpecOutSelPRICEIN.AsCurrency;
                taPartTestiRes.AsInteger:=1;
                taPartTest.Post;
              end;

              quSpecOutSel.Next;
            end;
            quSpecOutSel.Active:=False;

            // 3 - �������� ������
            quDocsOutSel.Edit;
            quDocsOutSelIACTIVE.AsInteger:=1;
            quDocsOutSel.Post;
            quDocsOutSel.Refresh;


            fmPartIn1:=tfmPartIn1.Create(Application);
            fmPartIn1.Label5.Caption:=quDocsOutSelNAMEMH.AsString;
            fmPartIn1.Label6.Caption:=quDocsOutSelNAMECL.AsString;
            fmPartIn1.ShowModal;
            fmPartIn1.Release;
            taPartTest.Active:=False;
          end else showmessage('���������� ����� �������� ���������.');
        end;
      end;
    end;
  end;
end;

procedure TfmDocsOut.acOffDoc1Execute(Sender: TObject);
begin
//��������
  if not CanDo('prOffDocOut') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmO do
  begin
    if quDocsOutSel.RecordCount>0 then //���� ��� ����������
    begin
      if quDocsOutSelIACTIVE.AsInteger=1 then
      begin
        if MessageDlg('�������� �������� �'+quDocsOutSelNUMDOC.AsString+' �� '+quDocsOutSelNAMECL.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          if prTOFind(Trunc(quDocsOutSelDATEDOC.AsDateTime),quDocsOutSelIDSKL.AsInteger)=1 then
          begin //�� ����
            if MessageDlg('������� ��� �������� ������ �� �� '+quDocsOutSelNAMEMH.AsString+' � '+quDocsOutSelNUMDOC.AsString+'?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
              prTODel(Trunc(quDocsOutSelDATEDOC.AsDateTime),quDocsOutSelIDSKL.AsInteger)
            else
            begin
              showmessage('��������� ������� ��������� ����������...');
              exit;
            end;
          end;

          //������� ��� �������� ������
//          ?IDDOC, ?DTYPE
          prDelPartOut.ParamByName('IDDOC').AsInteger:=quDocsOutSelID.AsInteger;
          prDelPartOut.ParamByName('DTYPE').AsInteger:=1;
          prDelPartOut.ExecProc;

          quDocsOutSel.Edit;
          quDocsOutSelIACTIVE.AsInteger:=0;
          quDocsOutSel.Post;
          quDocsOutSel.Refresh;
        end;
      end;
    end;
  end;
end;

procedure TfmDocsOut.Timer1Timer(Sender: TObject);
begin
  if bClearDocOut=True then begin StatusBar1.Panels[0].Text:=''; bClearDocOut:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearDocOut:=True;
end;

end.
