unit AddClass;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, dxfBackGround, ExtCtrls, Menus, cxLookAndFeelPainters, StdCtrls,
  cxButtons, cxMaskEdit, cxDropDownEdit, cxCalc, cxControls, cxContainer,
  cxEdit, cxTextEdit;

type
  TfmAddClass = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    dxfBackGround1: TdxfBackGround;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxTextEdit1: TcxTextEdit;
    Label1: TLabel;
    Label4: TLabel;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAddClass: TfmAddClass;

implementation

{$R *.dfm}

procedure TfmAddClass.FormShow(Sender: TObject);
begin
  cxTextEdit1.SetFocus;
  cxTextEdit1.SelectAll;
end;

end.
