unit FindResultDC;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons, ExtCtrls,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxImageComboBox;

type
  TfmFindDC = class(TForm)
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    ViewFind: TcxGridDBTableView;
    LevelFind: TcxGridLevel;
    GridFind: TcxGrid;
    ViewFindID: TcxGridDBColumn;
    ViewFindNAME: TcxGridDBColumn;
    ViewFMenu: TcxGridDBTableView;
    ViewFMenuSIFR: TcxGridDBColumn;
    ViewFMenuNAME: TcxGridDBColumn;
    ViewFMenuPRICE: TcxGridDBColumn;
    ViewFMenuCODE: TcxGridDBColumn;
    ViewFMenuCONSUMMA: TcxGridDBColumn;
    procedure ViewFindCellDblClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure ViewFMenuCellDblClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmFindDC: TfmFindDC;

implementation

uses DmRnDisc;

{$R *.dfm}

procedure TfmFindDC.ViewFindCellDblClick(Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  ModalResult:=mrOk;
end;

procedure TfmFindDC.FormShow(Sender: TObject);
begin
  GridFind.SetFocus;
end;

procedure TfmFindDC.ViewFMenuCellDblClick(Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  ModalResult:=mrOk;
end;

end.
