unit Discount;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Placemnt, ComCtrls, SpeedBar, ExtCtrls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  RXSplit, cxCurrencyEdit, cxImageComboBox, cxTextEdit, XPStyleActnCtrls,
  ActnList, ActnMan, Menus, ToolWin, ActnCtrls, ActnMenus, StdCtrls,
  FR_Class, FR_DSet, FR_DBSet, DBClient, FR_BarC, cxContainer, cxTreeView,
  cxLookAndFeelPainters, cxButtons;

type
  TfmRnDisc = class(TForm)
    FormPlacement1: TFormPlacement;
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    RxSplitter1: TRxSplitter;
    ViewDiscCard: TcxGridDBTableView;
    LevelDiscCard: TcxGridLevel;
    GridDiscCard: TcxGrid;
    ViewDiscCardNAME: TcxGridDBColumn;
    ViewDiscCardBARCODE: TcxGridDBColumn;
    ViewDiscCardIACTIVE: TcxGridDBColumn;
    am3: TActionManager;
    acEditMGr: TAction;
    acAddMGr: TAction;
    acExit: TAction;
    PopupMenu1: TPopupMenu;
    acAddMGr1: TMenuItem;
    N1: TMenuItem;
    acAddMSubGr: TAction;
    Timer1: TTimer;
    acAddMSubGr1: TMenuItem;
    N2: TMenuItem;
    N6: TMenuItem;
    acDelMGr: TAction;
    ActionMainMenuBar1: TActionMainMenuBar;
    acAddM: TAction;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    acEditM: TAction;
    acDelM: TAction;
    SpeedItem5: TSpeedItem;
    acMOn: TAction;
    acMOff: TAction;
    SpeedItem6: TSpeedItem;
    acPrint: TAction;
    ViewDiscCardPERCENT: TcxGridDBColumn;
    ViewDiscCardIACTIVE1: TcxGridDBColumn;
    SpeedItem7: TSpeedItem;
    ViewDiscCardPHONE: TcxGridDBColumn;
    ViewDiscCardBERTHDAY: TcxGridDBColumn;
    ViewDiscCardCOMMENT: TcxGridDBColumn;
    DiscTree: TcxTreeView;
    acImport: TAction;
    ViewDiscCardBARNUM: TcxGridDBColumn;
    SpeedItem8: TSpeedItem;
    AcSpisok: TAction;
    frRepDisk: TfrReport;
    dsRep1: TfrDBDataSet;
    ViewDiscCardDATECARD: TcxGridDBColumn;
    ViewDiscCardPHONE_MOBILE: TcxGridDBColumn;
    ViewDiscCardEMAIL: TcxGridDBColumn;
    ViewDiscCardADRES: TcxGridDBColumn;
    ViewDiscCardChildSmall: TcxGridDBColumn;
    ViewDiscCardChildBig: TcxGridDBColumn;
    SpeedItem9: TSpeedItem;
    ViewDiscCardMMDD: TcxGridDBColumn;
    Panel2: TPanel;
    Label1: TLabel;
    cxTextEdit1: TcxTextEdit;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    acAddPlatCard: TAction;
    PopupMenu2: TPopupMenu;
    N3: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure DiscTreeExpanding(Sender: TObject; Node: TTreeNode;
      var AllowExpansion: Boolean);
    procedure DiscTreeChange(Sender: TObject; Node: TTreeNode);
    procedure acExitExecute(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure acAddMGrExecute(Sender: TObject);
    procedure acAddMSubGrExecute(Sender: TObject);
    procedure acDelMGrExecute(Sender: TObject);
    procedure acEditMGrExecute(Sender: TObject);
    procedure acAddMExecute(Sender: TObject);
    procedure ViewDiscCardCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure acEditMExecute(Sender: TObject);
    procedure acDelMExecute(Sender: TObject);
    procedure acMOnExecute(Sender: TObject);
    procedure acMOffExecute(Sender: TObject);
    procedure ViewDiscCardStartDrag(Sender: TObject;
      var DragObject: TDragObject);
    procedure DiscTreeDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure DiscTreeDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure SpeedItem7Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure acImportExecute(Sender: TObject);
    procedure EditRecordquDiscSel(Sender: TObject);
    procedure AcSpisokExecute(Sender: TObject);
    procedure SpeedItem9Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure acAddPlatCardExecute(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmRnDisc: TfmRnDisc;
  bDr:Boolean=False;

implementation

uses Un1, dmRnEdit, AddCateg, AddM, DmRnDisc, DC, Period, DiscDetail, FindResultDC,
  prdb, AddPlCard, PCard;

{$R *.dfm}

procedure TfmRnDisc.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  GridDiscCard.Align:=AlClient;
  ViewDiscCard.RestoreFromIniFile(CurDir+GridIni);
  ClassifExpand(nil,DiscTree,dmCDisc.quClassif,Person.Id,5);
  with dmCDisc do
  begin
    quClassif.First;
    quDiscSel.Active:=False;
    quDiscSel.ParamByName('PARENTID').AsInteger:=quClassif.fieldbyName('Id').AsInteger;
    quDiscSel.Active:=True;
  end;
  cxTextEdit1.Text:='';

  delay(10);
end;

procedure TfmRnDisc.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewDiscCard.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmRnDisc.DiscTreeExpanding(Sender: TObject; Node: TTreeNode;
  var AllowExpansion: Boolean);
begin
  if Node.getFirstChild.Data = nil then
  begin
    Node.DeleteChildren;
    ClassifExpand(Node,DiscTree,dmCDisc.quClassif,Person.Id,5);
  end;
end;

procedure TfmRnDisc.DiscTreeChange(Sender: TObject; Node: TTreeNode);
begin
  if bMenuList then
  begin
    with dmCDisc do
    begin
      ViewDiscCard.BeginUpdate;

      quDiscSel.Active:=False;
      quDiscSel.ParamByName('PARENTID').AsInteger:=Integer(DiscTree.Selected.Data);
      quDiscSel.Active:=True;

      ViewDiscCard.EndUpdate;
    end;
  end;
end;

procedure TfmRnDisc.acExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmRnDisc.acAddMGrExecute(Sender: TObject);
//procedure TfmMenuCr.acAddMGr1Click(Sender: TObject);
Var Id,i:Integer;
begin
  //�������� ������
  if not CanDo('prAddDiscGr') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  with dmCDisc do
  begin
    Id:=0;
    fmAddCateg:=TFmAddCateg.create(Application);
    fmAddCateg.Caption:='���������� ������ ���������� ����.';
    fmAddCateg.cxTextEdit1.Text:='';
    fmAddCateg.ShowModal;
    if fmAddCateg.ModalResult=mrOk then
    begin
      try
//        showmessage('�������� ������ ����');

        quMaxIdM.Active:=False;
        quMaxIdM.ParamByName('ITYPE').AsInteger:=5;
        quMaxIdM.Active:=True;
        Id:=quMaxIdMMAXID.AsInteger+1;
        quMaxIdM.Active:=False;


        taClassif.Active:=False;
        taClassif.Active:=True;

        taClassif.Append;
        taClassifTYPE_CLASSIF.AsInteger:=5; //���������� �����
        taClassifID.AsInteger:=Id;
        taClassifIACTIVE.AsInteger:=1;
        taClassifID_PARENT.AsInteger:=0;
        taClassifNAME.AsString:=Copy(fmAddCateg.cxTextEdit1.Text,1,40);
        taClassifIEDIT.AsInteger:=0;
        taClassif.Post;

        //����� ���������� ������ � ������ �����
        quDiscSel.Active:=False;
        quDiscSel.ParamByName('PARENTID').AsInteger:=Id;
        quDiscSel.Active:=True;
      except
      end;
    end;
    fmAddCateg.Release;
    if Id>0 then //������ �������� - ����� �� ���������
    begin
      bMenuList:=False;

      DiscTree.Items.Clear;
      ClassifExpand(nil,DiscTree,dmCDisc.quClassif,Person.Id,5);

      for i:=0 to DiscTree.Items.Count-1 do
      if Integer(DiscTree.Items.Item[i].Data) = Id then
      begin
        DiscTree.Items[i].Selected:=True;
        DiscTree.Repaint;
      end;

      bMenuList:=True;
    end;
  end;
end;

procedure TfmRnDisc.Timer1Timer(Sender: TObject);
begin
  if bClear3=True then begin StatusBar1.Panels[0].Text:=''; bClear3:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClear3:=True;
end;


procedure TfmRnDisc.acAddMSubGrExecute(Sender: TObject);
Var Id,IdGr:Integer;
    TreeNode : TTreeNode;
begin
// �������� ���������
  if not CanDo('prAddDiscSubGr') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  if (DiscTree.Items.Count=0)  then
  begin
    showmessage('�������� ������.');
    exit;
  end;
  if (DiscTree.Selected=nil)  then
  begin
    showmessage('�������� ������,���������.');
    exit;
  end;

  with dmCDisc do
  begin
    Id:=0;
    IdGr:=Integer(DiscTree.Selected.data);

    fmAddCateg:=TFmAddCateg.create(Application);
    fmAddCateg.Caption:='���������� � ������ "'+DiscTree.Selected.Text+'" ����� ���������.';
    fmAddCateg.cxTextEdit1.Text:='';
    fmAddCateg.ShowModal;
    if fmAddCateg.ModalResult=mrOk then
    begin
      try
//        showmessage('�������� ������ �������������');

        quMaxIdM.Active:=False;
        quMaxIdM.ParamByName('ITYPE').AsInteger:=5;
        quMaxIdM.Active:=True;
        Id:=quMaxIdMMAXID.AsInteger+1;
        quMaxIdM.Active:=False;

        taClassif.Active:=False;
        taClassif.Active:=True;

        taClassif.Append;
        taClassifTYPE_CLASSIF.AsInteger:=5; //���������� �����
        taClassifID.AsInteger:=Id;
        taClassifIACTIVE.AsInteger:=1;
        taClassifID_PARENT.AsInteger:=IdGr;
        taClassifNAME.AsString:=Copy(fmAddCateg.cxTextEdit1.Text,1,40);
        taClassifIEDIT.AsInteger:=0;
        taClassif.Post;

        //����� �� ���������� ������ � ������ �����
{        quDiscSel.Active:=False;
        quDiscSel.ParamByName('PARENTID').AsInteger:=Id;
        quDiscSel.Active:=True;
}
      except
      end;
    end;
    if Id>0 then //������ ���� �������� - ����� �� �������� � ������
    begin
      bMenuList:=False;

      TreeNode:=DiscTree.Items.AddChildObject(DiscTree.Selected,Copy(fmAddCateg.cxTextEdit1.Text,1,40), Pointer(Id));
      TreeNode.ImageIndex:=8;
      TreeNode.SelectedIndex:=7;
//      ���������� ������ �� �����
//      DiscTree.Items[DiscTree.Items.Count-1].Selected:=True;

      bMenuList:=True;
    end;
    fmAddCateg.Release;
  end;
end;

procedure TfmRnDisc.acDelMGrExecute(Sender: TObject);
begin
  //������� ������
  if not CanDo('prDelDiscGr') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  if (DiscTree.Items.Count=0)  then
  begin
//    showmessage('�������� ������.');
    exit;
  end;
  if (DiscTree.Selected=nil)  then
  begin
    showmessage('�������� ������,���������.');
    exit;
  end;

  with dmCDisc do
  begin
    if MessageDlg('�� ������������� ������ ������� ������ - "'+DiscTree.Selected.Text+'"?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      if quDiscSel.RecordCount>0 then
      begin
        showmessage('������ �� �����, �������� ����������!!!');
        exit;
      end;

      quFindChild.Active:=False;
      quFindChild.ParamByName('PARENTID').AsInteger:=Integer(DiscTree.Selected.Data);
      quFindChild.ParamByName('ITYPE').AsInteger:=5;
      quFindChild.Active:=True;

      if quFindChildCOUNTREC.AsInteger>0 then
      begin
        showmessage('������ �� �����, �������� ����������!!!');
        quFindChild.Active:=False;
        exit;
      end;
      quFindChild.Active:=False;
      //���� ����� �� ���� �� �������� ��������

      //������ � ����

      taClassEdit.Active:=False;
      taClassEdit.ParamByName('ITYPE').AsInteger:=5;
      taClassEdit.ParamByName('IID').AsInteger:=Integer(DiscTree.Selected.Data);
      taClassEdit.Active:=True;

      taClassEdit.First;

      if not taClassEdit.Eof then taClassEdit.Delete;

      //������ � ������
      DiscTree.Selected.Delete;

      //��� ����� ���������� ������ �������� - ����� ������� ������� �� ����
      quDiscSel.Active:=False;
      quDiscSel.ParamByName('PARENTID').AsInteger:=Integer(DiscTree.Selected.Data);
      quDiscSel.Active:=True;
    end;
  end;
end;

procedure TfmRnDisc.acEditMGrExecute(Sender: TObject);
Var Id:Integer;
begin
  //�������������� ������
  if not CanDo('prEditDiscGr') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  if (DiscTree.Items.Count=0)  then
  begin
   // showmessage('�������� ������.');
    exit;
  end;
  if (DiscTree.Selected=nil)  then
  begin
    showmessage('�������� ������,���������.');
    exit;
  end;
  with dmCDisc do
  begin
    Id:=Integer(DiscTree.Selected.Data);
    fmAddCateg:=TFmAddCateg.create(Application);
    fmAddCateg.Caption:='��������� �������� ������.';
    fmAddCateg.cxTextEdit1.Text:=DiscTree.Selected.Text;
    fmAddCateg.ShowModal;
    if fmAddCateg.ModalResult=mrOk then
    begin
      try
        taClassEdit.Active:=False;
        taClassEdit.ParamByName('ITYPE').AsInteger:=5;
        taClassEdit.ParamByName('IID').AsInteger:=Id;
        taClassEdit.Active:=True;
        taClassEdit.First;
        if not taClassEdit.Eof then
        begin
          taClassEdit.Edit;
          taClassEditNAME.AsString:=Copy(fmAddCateg.cxTextEdit1.Text,1,40);
          taClassEdit.Post;
        end;
      except
      end;
      DiscTree.Selected.Text:=Copy(fmAddCateg.cxTextEdit1.Text,1,40);
    end;
    fmAddCateg.Release;
  end;
end;

procedure TfmRnDisc.acAddMExecute(Sender: TObject);
Var sBar:String;
    sPref,sPosf:String;
    iNum,iL:Integer;
begin
  //���������� ��
  if not CanDo('prAddDC') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
//  IdGr:=Integer(DiscTree.Selected.Data);

  if (DiscTree.Selected=nil)  then
  begin
    showmessage('�������� ������,���������.');
    exit;
  end;


  with dmCDisc do
  begin
    fmAddDC:=TFmAddDC.create(Application);
    fmAddDC.Caption:='���������� ���������� �����.';

    fmAddDC.sBarOld := '';
    fmAddDC.cxTextEdit1.Text:='';
    fmAddDC.cxTextEdit2.Text:='';

    fmAddDC.cxTextEdit3.Text:='';
    fmAddDC.cxTextEdit4.Text:='';
    fmAddDC.cxDateEdit1.Date:=Date;

    fmAddDC.cxCalcEdit1.Value:=0;
    fmAddDc.CheckBox1.Checked:=True;

    fmAddDC.cxTextEditMT.Text:='';
    fmAddDC.cxTextEditEMAIL.Text:='';
    fmAddDC.cxTextEditAdres.Text:='';
    fmAddDC.cxDateEditDATECARD.Date:=Date;
    fmAddDC.cxCalcEditCS.Text:='0';
    fmAddDC.cxCalcEditCB.Text:='0';


    if not quDiscSel.Eof then
    begin
      sBar:=quDiscSelBARCODE.AsString;
      if Length(sBar)>7 then
      begin
        sPref:=Copy(sBar,1,Length(sBar)-7);;
        sPosf:='';

        sBar:=Copy(sBar,Length(sBar)-6,7);
        if (sBar[7] in ['0','1','2','3','4','5','6','7','8','9'])=False then
        begin
          sPosf:=sBar[7];
          delete(sBar,7,1);
        end;
        iL:=Length(sBar);
        while sBar[1]='0' do delete(sBar,1,1);
        iNum:=StrToIntDef(sBar,0);
        if iNum>0 then
        begin
          inc(iNum);
          sBar:=INtToStr(iNum);
          while Length(sBar)<iL do sBar:='0'+sBar;
          sBar:=sPref+sBar+sPosf;
        end;
      end;


      fmAddDC.cxTextEdit1.Text:=quDiscSelNAME.AsString;
      fmAddDC.cxTextEdit2.Text:=sBar;
      fmAddDC.cxCalcEdit1.Value:=quDiscSelPERCENT.AsFloat;
      fmAddDC.cxTextEdit3.Text:=quDiscSelPHONE.AsString;
      fmAddDC.cxTextEdit4.Text:=quDiscSelCOMMENT.AsString;

//      fmAddDC.cxDateEdit1.Date:=quDiscSelBERTHDAY.AsDateTime;
    end;

    fmAddDC.ShowModal;
    if fmAddDC.ModalResult=mrOk then
    begin
      sBar:=Copy(fmAddDC.cxTextEdit2.Text,1,30);
      while pos(' ',sBar)>0 do delete(sBar,pos(' ',sBar),1);

      quFindDc.Active:=False;
      quFindDc.ParamByName('SBAR').AsString:=sBar;
      quFindDc.Active:=True;

      if quFindDc.RecordCount=0 then
      begin
        trUpdate.StartTransaction;
        quDiscSel.Append;
        quDiscSelBARCODE.AsString:=sBar;
        quDiscSelCLIENTINDEX.AsInteger:=Integer(DiscTree.Selected.Data);

        EditRecordquDiscSel(Sender);

      end else
      begin
        showmessage('���������� ����� � ����� ����� ��� ����������. ���������� ����������.');
      end;
      quFindDc.Active:=False;
    end;
    fmAddDC.Release;
  end;
end;

procedure TfmRnDisc.ViewDiscCardCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
Var iType,i:Integer;
begin
  if AViewInfo.GridRecord.Selected then exit;

  iType:=0;
  for i:=0 to ViewDiscCard.ColumnCount-1 do
  begin
    if ViewDiscCard.Columns[i].Name='ViewDiscCardIACTIVE1' then
    begin
      iType:=VarAsType(AViewInfo.GridRecord.DisplayTexts[i], varInteger);
      break;
    end;
  end;

  if iType=0  then
  begin
    ACanvas.Canvas.Brush.Color := clWhite;
    ACanvas.Canvas.Font.Color := clGray;
  end;
end;

procedure TfmRnDisc.EditRecordquDiscSel(Sender: TObject);
Var sBar:String;
begin

  with dmCDisc do
  begin

      sBar:=Copy(fmAddDC.cxTextEdit2.Text,1,30);
      while pos(' ',sBar)>0 do delete(sBar,pos(' ',sBar),1);
      quDiscSelBARCODE.AsString:=sBar;

      quDiscSelNAME.AsString:=Copy(fmAddDC.cxTextEdit1.Text,1,100);
      quDiscSelPERCENT.Value:=fmAddDC.cxCalcEdit1.Value;
      if fmAddDc.CheckBox1.Checked=True then quDiscSelIACTIVE.AsInteger:=1
      else quDiscSelIACTIVE.AsInteger:=0;

      quDiscSelPHONE.AsString:=fmAddDC.cxTextEdit3.Text;
      quDiscSelCOMMENT.AsString:=fmAddDC.cxTextEdit4.Text;

      if fmAddDC.cxDateEdit1.EditValue = Null then
  //       quDiscSelBERTHDAY.AsString:=''
      else
        quDiscSelBERTHDAY.AsDateTime:=fmAddDC.cxDateEdit1.Date;

      if fmAddDC.cxDateEditDATECARD.EditValue = Null then
   //      quDiscSelDATECARD.AsString:=''
      else
        quDiscSelDATECARD.AsDateTime:=fmAddDC.cxDateEditDATECARD.Date;


      quDiscSelPHONE_MOBILE.AsString:=Copy(fmAddDC.cxTextEditMT.Text,1,15);
      quDiscSelEMAIL.AsString:=Copy(fmAddDC.cxTextEditEMAIL.Text,1,50);
      quDiscSelAdres.AsString:=Copy(fmAddDC.cxTextEditAdres.Text,1,100);

      if fmAddDC.cxCalcEditCS.EditValue = Null then
       quDiscSelChildSmall.AsInteger:=0
      else
        quDiscSelChildSmall.AsInteger:=fmAddDC.cxCalcEditCS.EditValue;

      if fmAddDC.cxCalcEditCB.EditValue = Null then
       quDiscSelChildBig.AsInteger:=0
      else
        quDiscSelChildBig.AsInteger:=fmAddDC.cxCalcEditCB.EditValue;

      quDiscSel.Post;
      trUpdate.Commit;
      quDiscSel.Refresh;
    end;
end;


procedure TfmRnDisc.acEditMExecute(Sender: TObject);
//Var sBar:String;
begin
//�������������
  if not CanDo('prEditDC') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
//  IdGr:=Integer(DiscTree.Selected.Data);

  if (DiscTree.Selected=nil)  then
  begin
    showmessage('�������� ������,���������.');
    exit;
  end;

  with dmCDisc do
  begin

    if quDiscSel.Eof then
    begin
      exit;
    end;

    fmAddDC:=TFmAddDC.create(Application);
    fmAddDC.Caption:='�������������� ���������� �����.';
    fmAddDC.cxTextEdit1.Text:='';
    fmAddDC.cxTextEdit2.Text:='';
    fmAddDC.cxCalcEdit1.Value:=0;
    fmAddDc.CheckBox1.Checked:=True;

    if not quDiscSel.Eof then
    begin
      fmAddDC.cxTextEdit1.Text:=quDiscSelNAME.AsString;
      fmAddDC.cxTextEdit2.Text:=quDiscSelBARCODE.AsString;
      fmAddDC.SBAROLD:=quDiscSelBARCODE.AsString;


      quCheckDC.Active:=False;
      quCheckDC.ParamByName('SBAR').AsString:=quDiscSelBARCODE.AsString;
      quCheckDC.Active:=True;

      if quCheckDC.RecordCount>0 then
      begin
       fmAddDC.cxTextEdit2.Properties.ReadOnly:=True; //������������� ��� ������ - ������ ����� �������
      end;
      quCheckDC.Active:=False;

      fmAddDC.cxCalcEdit1.Value:=quDiscSelPERCENT.AsFloat;
      if quDiscSelIACTIVE.AsInteger=1 then fmAddDc.CheckBox1.Checked:=True
      else fmAddDc.CheckBox1.Checked:=False;
      fmAddDC.cxTextEdit3.Text:=quDiscSelPHONE.AsString;
      fmAddDC.cxTextEdit4.Text:=quDiscSelCOMMENT.AsString;

      if quDiscSelBERTHDAY.AsDateTime = 0 then
 //      fmAddDC.cxDateEdit1.EditValue:=''
      else
       fmAddDC.cxDateEdit1.Date:=quDiscSelBERTHDAY.AsDateTime;

      if quDiscSelDATECARD.AsDateTime = 0 then
 //     fmAddDC.cxDateEditDATECARD.EditValue:=''
      else
       fmAddDC.cxDateEditDATECARD.Date:=quDiscSelDATECARD.AsDateTime;

      fmAddDC.cxTextEditMT.Text:=quDiscSelPHONE_MOBILE.AsString;
      fmAddDC.cxTextEditEMAIL.Text:=quDiscSelEMAIL.AsString;
      fmAddDC.cxTextEditAdres.Text:=quDiscSelAdres.AsString;
      fmAddDC.cxCalcEditCS.Text:=quDiscSelChildSmall.AsString;
      fmAddDC.cxCalcEditCB.Text:=quDiscSelChildBig.AsString;

    end;

    fmAddDC.ShowModal;
    if fmAddDC.ModalResult=mrOk then
    begin
      trUpdate.StartTransaction;
      quDiscSel.Edit;

      EditRecordquDiscSel(Sender);

    end;
    fmAddDC.Release;
  end;
end;

procedure TfmRnDisc.acDelMExecute(Sender: TObject);
begin
  //������� ��
  if not CanDo('prDelDC') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;

  if dmCDisc.quDiscSel.Eof then exit;
  if MessageDlg('�� ������������� ������ ������� ��: "'+dmCDisc.quDiscSelNAME.AsString+'"',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
    with dmCDisc do
    begin
      quDiscSel.Delete;
      quDiscSel.Refresh;
    end;
  end;
end;

procedure TfmRnDisc.acMOnExecute(Sender: TObject);
begin
// �������
  if not CanDo('prOnDisc') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  if dmCDisc.quDiscSel.Eof then exit;
  with dmCDisc do
  begin
    trUpdate.StartTransaction;
    quDiscSel.Edit;
    quDiscSelIACTIVE.AsInteger:=1;
    quDiscSel.Post;
    trUpdate.Commit;

    quDiscSel.Refresh;
    quDiscSel.Next;
  end;
end;

procedure TfmRnDisc.acMOffExecute(Sender: TObject);
begin
// ���������
  if not CanDo('prOffDisc') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  if dmCDisc.quDiscSel.Eof then exit;
  with dmCDisc do
  begin
    trUpdate.StartTransaction;
    quDiscSel.Edit;
    quDiscSelIACTIVE.AsInteger:=0;
    quDiscSel.Post;
    trUpdate.Commit;

    quDiscSel.Refresh;
    quDiscSel.Next;
  end;
end;

procedure TfmRnDisc.ViewDiscCardStartDrag(Sender: TObject;
  var DragObject: TDragObject);
begin
  if not CanDo('prMoveDC') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  bDr:=True;
end;

procedure TfmRnDisc.DiscTreeDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept:=False;
  if bDr then  Accept:=True;
end;

procedure TfmRnDisc.DiscTreeDragDrop(Sender, Source: TObject; X,
  Y: Integer);
Var sGr:String;
    iGr:Integer;
    iCo:Integer;
    i,j: Integer;
    Rec:TcxCustomGridRecord;
    sBar:String;
begin
  if bDr then
  begin
    bDr:=False;
    sGr:=DiscTree.DropTarget.Text;
    iGr:=Integer(DiscTree.DropTarget.data);
    iCo:=ViewDiscCard.Controller.SelectedRecordCount;
    if iCo>0 then
    begin
      if MessageDlg('�� ������������� ������ ����������� ��������� ������� ('+IntToStr(iCo)+' ��.) � ������ "'+sGr+'"?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        with dmCDisc do
        begin
          for i:=0 to ViewDiscCard.Controller.SelectedRecordCount-1 do
          begin
            Rec:=ViewDiscCard.Controller.SelectedRecords[i];

            for j:=0 to Rec.ValueCount-1 do
            begin
              if ViewDiscCard.Columns[j].Name='ViewDiscCardBARCODE' then break;
            end;

            sBar:=Rec.Values[j];
            //��� ������ - ����������

            if quDiscSel.Locate('BARCODE',sBar,[]) then
            begin
              trUpdate.StartTransaction;
              quDiscSel.Edit;
              quDiscSelCLIENTINDEX.AsInteger:=iGr;
              quDiscSel.Post;
              trUpdate.Commit;
            end;
          end;
          quDiscSel.FullRefresh;
        end;
      end;
    end;
  end;
end;

procedure TfmRnDisc.SpeedItem7Click(Sender: TObject);
begin
  //������ ��������
  with dmCDisc do
  begin
    if not quDiscSel.Eof then
    begin
      quDiscDetail.Active:=False;
      quDiscDetail.ParamByName('DateB').AsDateTime:=TrebSel.DateFrom;
      quDiscDetail.ParamByName('DateE').AsDateTime:=TrebSel.DateTo;
      quDiscDetail.ParamByName('SBAR').AsString:=quDiscSelBARCODE.AsString;
      quDiscDetail.Active:=True;

      fmDiscDet.Caption:='�������� �� ����� "'+quDiscSelName.AsString+'" ���-'+quDiscSelBARCODE.AsString+' �� ������ � '+FormatDateTime('dd.mm.yyyy hh:nn',TrebSel.DateFrom)+' �� '+FormatDateTime('dd.mm.yyyy hh:nn',TrebSel.DateTo);
      fmDiscDet.ShowModal;
    end;
  end;
end;

procedure TfmRnDisc.FormShow(Sender: TObject);
begin
  DiscTree.SetFocus;
end;

procedure TfmRnDisc.acImportExecute(Sender: TObject);
begin
  // ������� �� ListDc.dbf



end;

procedure TfmRnDisc.AcSpisokExecute(Sender: TObject);
begin

  with dmCDisc do
    begin
      quDiscSpisok.Active:=False;
     quDiscSpisok.ParamByName('PARENTID').AsInteger:=Integer(DiscTree.Selected.Data);


      frRepDisk.LoadFromFile(CurDir + 'DiscSpisok.frf');

//      frVariables.Variable['Depart']:=CommonSet.DepartName;

      frRepDisk.ReportName:='������ ���������� ����.';
      frRepDisk.PrepareReport;
      frRepDisk.ShowPreparedReport;
    end;


end;

procedure TfmRnDisc.SpeedItem9Click(Sender: TObject);
begin
//������� � ������
//  with dmCDisc do prExportExel(ViewDiscCard,dsDiscSel,quDiscSel);
  with dmCDisc do prNExportExel4(ViewDiscCard);

end;

procedure TfmRnDisc.cxButton1Click(Sender: TObject);
Var i:INteger;
begin
  if cxTextEdit1.Text>'' then
  begin
    with dmCDisc do
    begin
      quFindDCFind.Active:=False;
      quFindDCFind.SelectSQL.Clear;
      quFindDCFind.SelectSQL.Add('SELECT BARCODE,CLIENTINDEX,NAME');
      quFindDCFind.SelectSQL.Add('FROM DISCCARD');
      quFindDCFind.SelectSQL.Add('where UPPER(NAME)like ''%'+AnsiUpperCase(cxTextEdit1.Text)+'%''');
      quFindDCFind.Active:=True;

      fmFindDC.LevelFind.Visible:=True;

      fmFindDC.ShowModal;
      if fmFindDC.ModalResult=mrOk then
      begin
        if quFindDCFind.RecordCount>0 then
        begin
          for i:=0 to DiscTree.Items.Count-1 Do
          if Integer(DiscTree.Items[i].Data) = quFindDCFindCLIENTINDEX.AsInteger Then
          begin
            DiscTree.Items[i].Expand(False);
            DiscTree.Repaint;
            DiscTree.Items[i].Selected:=True;
            Break;
          End;
          delay(10);
          quDiscSel.First;
          quDiscSel.locate('BARCODE',quFindDCFindBARCODE.AsString,[]);
        end;
      end;
      cxTextEdit1.Text:='';
      delay(10);
      GridDiscCard.SetFocus;
    end;
  end;

end;

procedure TfmRnDisc.cxButton2Click(Sender: TObject);
var i:INteger;
begin
  if cxTextEdit1.Text>'' then
  begin
    with dmCDisc do
    begin
      quFindDCFind.Active:=False;
      quFindDCFind.SelectSQL.Clear;
      quFindDCFind.SelectSQL.Add('SELECT BARCODE,CLIENTINDEX,NAME');
      quFindDCFind.SelectSQL.Add('FROM DISCCARD');
      quFindDCFind.SelectSQL.Add('where BARCODE = '+AnsiUpperCase(cxTextEdit1.Text));
      quFindDCFind.Active:=True;

      fmFindDC.LevelFind.Visible:=True;

      fmFindDC.ShowModal;
      if fmFindDC.ModalResult=mrOk then
      begin
        if quFindDCFind.RecordCount>0 then
        begin
          for i:=0 to DiscTree.Items.Count-1 Do
          if Integer(DiscTree.Items[i].Data) = quFindDCFindCLIENTINDEX.AsInteger Then
          begin
            DiscTree.Items[i].Expand(False);
            DiscTree.Repaint;
            DiscTree.Items[i].Selected:=True;
            Break;
          End;
          delay(10);
          quDiscSel.First;
          quDiscSel.locate('BARCODE',quFindDCFindBARCODE.AsString,[]);
        end;
      end;
      cxTextEdit1.Text:='';
      delay(10);
      GridDiscCard.SetFocus;
    end;
  end;
end;

procedure TfmRnDisc.acAddPlatCardExecute(Sender: TObject);
Var SBar:String;
    CliName,CliType:String;
    rBalans,rBalansDay,DLimit:Real;
begin
//  showmessage('�������� � ��������� �����');
  with dmPC do
  with dmCDisc do
  begin
    if quDiscSel.RecordCount>0 then
    begin
      if ViewDiscCard.Controller.SelectedRecordCount>0 then
      begin
        sBar:=quDiscSelBARCODE.AsString;
        if prFindPC(sBar)=False then
        begin
          with fmAddPlCard do
          begin
            if taTypePl.Active=False then taTypePl.Active:=True;
            if taCategSaleSel.Active=False then taCategSaleSel.Active:=True;
            taCategSaleSel.First;

            cxTextEdit1.Text:=quDiscSelNAME.AsString;
            cxTextEdit2.Text:=quDiscSelBARCODE.AsString;
            CheckBox1.Checked:=True;

            taTypePl.First;
            if taTypePl.RecordCount>0 then cxLookupComboBox1.EditValue:=taTypePlID.AsInteger;

            cxComboBoxTypeOpl.ItemIndex:=0;
            cxCalcEdit2.EditValue:=0;
            cxTextEditBALANCECARD.Text:=quDiscSelBARCODE.AsString;
            cxCheckBoxGol.Checked := False;
            cxCalcEdit1.EditValue:=0;

            cxLookupComboBoxCategSale.EditValue:=taCategSaleSelID.AsInteger;

            Caption:='���������� ��������� �����.';
            cxTextEdit2.Properties.ReadOnly:=True;
          end;
          fmAddPlCard.ShowModal;
          if fmAddPlCard.ModalResult=mrOk then
          begin
            sBar:=fmAddPlCard.cxTextEdit2.Text;
            if prFindPC(sBar)=False then
            begin
              fmPCard.ViewPCards.BeginUpdate;
              try
                if taPCard.Active=False then taPCard.Active:=True;
                taPCard.Append;
                taPCardBARCODE.AsString:=sBar;
                taPCardPLATTYPE.AsInteger:=fmAddPlCard.cxLookupComboBox1.EditValue;
                if fmAddPlCard.CheckBox1.Checked=True then taPCardIACTIVE.AsInteger:=1
                else taPCardIACTIVE.AsInteger:=0;
                taPCardCLINAME.asstring:=fmAddPlCard.cxTextEdit1.Text;

                taPCardTypeOpl.AsInteger:=fmAddPlCard.cxComboBoxTypeOpl.ItemIndex+1;
                taPCardBALANCECARD.AsString := fmAddPlCard.cxTextEditBALANCECARD.Text;
                taPCardDAYLIMIT.AsFloat := fmAddPlCard.cxCalcEdit1.EditValue;
                taPCardSALET.AsInteger := fmAddPlCard.cxLookupComboBoxCategSale.EditValue;

                taPCard.Post;

                taPCard.Refresh;
                taPCard.Locate('BARCODE',sBar,[]);

              finally
                fmPCard.ViewPCards.EndUpdate;
                prRowFocus(fmPCard.ViewPCards);
              end;
            end;
          end;
        end else
        begin
          if MessageDlg('��������� ����� �� ���� ��� ��������. �������������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
          begin
            fmPCard.ViewPCards.BeginUpdate;
            try
              if taPCard.Active=False then taPCard.Active:=True;
              if taPCard.Locate('BARCODE',sBar,[]) then
              begin
                with fmAddPlCard do
                begin
                  if taTypePl.Active=False then taTypePl.Active:=True;
                  if taCategSaleSel.Active=False then taCategSaleSel.Active:=True;
                  taCategSaleSel.First;

                  prFindPCardBalans(sBar,rBalans,rBalansDay,DLimit,CliName,CliType);


                  cxTextEdit1.Text:=taPCardCLINAME.AsString;
                  cxTextEdit2.Text:=taPCardBARCODE.AsString;
                  if taPCardIACTIVE.AsInteger=1 then CheckBox1.Checked:=True
                  else CheckBox1.Checked:=False;

                  cxLookupComboBox1.EditValue:=taPCardPLATTYPE.AsInteger;
                  cxComboBoxTypeOpl.ItemIndex:=taPCardTypeOpl.AsInteger-1;
                  cxCalcEdit2.EditValue := rBalans;
                  cxTextEditBALANCECARD.Text:= taPCardBALANCECARD.AsString;
                  if cxTextEditBALANCECARD.Text = '' then
                  cxTextEditBALANCECARD.Text := taPCardBARCODE.AsString;

                  cxCheckBoxGol.Checked := False;
                  if cxTextEdit2.Text <> cxTextEditBALANCECARD.Text then
                  cxCheckBoxGol.Checked := True;

                  if taPCardSALET.AsInteger>0 then cxLookupComboBoxCategSale.EditValue:=taPCardSALET.AsInteger
                  else cxLookupComboBoxCategSale.EditValue:=taCategSaleSelID.AsInteger;

                  fmAddPlCard.cxCalcEdit1.EditValue := taPCardDAYLIMIT.AsFloat;

                  Caption:='�������������� ��������� �����.';

                  cxTextEdit2.Properties.ReadOnly:=True;
                end;
                fmAddPlCard.ShowModal;
                if fmAddPlCard.ModalResult=mrOk then
                begin
                  taPCard.Edit;
                  taPCardBARCODE.AsString:=StrWk;
                  taPCardPLATTYPE.AsInteger:=fmAddPlCard.cxLookupComboBox1.EditValue;

                  if fmAddPlCard.CheckBox1.Checked=True then taPCardIACTIVE.AsInteger:=1
                  else taPCardIACTIVE.AsInteger:=0;
                  taPCardCLINAME.asstring:=fmAddPlCard.cxTextEdit1.Text;
                  taPCardTypeOpl.AsInteger:=fmAddPlCard.cxComboBoxTypeOpl.ItemIndex+1;
                  taPCardBALANCECARD.AsString := fmAddPlCard.cxTextEditBALANCECARD.Text;
                  taPCardDAYLIMIT.AsFloat := fmAddPlCard.cxCalcEdit1.EditValue;
                  taPCardSALET.AsInteger := fmAddPlCard.cxLookupComboBoxCategSale.EditValue;

                  taPCard.Post;

                  taPCard.Refresh;
                end;
              end;
            finally
              fmPCard.ViewPCards.EndUpdate;
              prRowFocus(fmPCard.ViewPCards);
            end;
          end;
        end;
      end;
    end;
  end;
end;

end.
