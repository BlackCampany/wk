unit Menu;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, cxLookAndFeelPainters, StdCtrls, cxButtons, Placemnt,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit, DB,
  cxDBData, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  cxGridCardView, cxGridDBCardView, ActnList, XPStyleActnCtrls, ActnMan,
  Menus, cxDataStorage, cxTextEdit;

type
  TfmMenuClass = class(TForm)
    FormPlacement1: TFormPlacement;
    Panel1: TPanel;
    Button1: TcxButton;
    Button2: TcxButton;
    Button3: TcxButton;
    LevelM: TcxGridLevel;
    GridM: TcxGrid;
    ViewM: TcxGridDBCardView;
    ViewMINFO: TcxGridDBCardViewRow;
    ViewMTREETYPE: TcxGridDBCardViewRow;
    ViewMSPRICE: TcxGridDBCardViewRow;
    am1: TActionManager;
    acUpUp: TAction;
    acUp: TAction;
    acExit: TAction;
    acOpenSelMenu: TAction;
    acSelOk: TAction;
    Label1: TLabel;
    acMod1: TAction;
    procedure cxButton2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ViewMDblClick(Sender: TObject);
    procedure ViewMCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure acUpUpExecute(Sender: TObject);
    procedure acUpExecute(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure acOpenSelMenuExecute(Sender: TObject);
    procedure ViewMCellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure acMod1Execute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmMenuClass: TfmMenuClass;

implementation

uses Dm, Un1, Calc, Modif;

{$R *.dfm}

procedure TfmMenuClass.cxButton2Click(Sender: TObject);
begin
  close;
end;

procedure TfmMenuClass.FormCreate(Sender: TObject);
begin
  FormPlacement1.Active:=False;
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  ViewM.RestoreFromIniFile(CurDir+GridIni,False);
end;

procedure TfmMenuClass.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  ViewM.StoreToIniFile(CurDir+GridIni);
end;

procedure TfmMenuClass.ViewMDblClick(Sender: TObject);
begin
//  acOpenSelMenu.Execute;
end;

procedure TfmMenuClass.ViewMCustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);
var
  ARec: TRect;
  ATextToDraw: string;
  sType:String;
begin
  if (AViewInfo is TcxGridCardRowDataViewInfo) then
    ATextToDraw := AViewInfo.GridRecord.DisplayTexts[AViewInfo.Item.Index]
  else
    ATextToDraw := VarAsType(AViewInfo.Item.Caption, varString);

  ARec := AViewInfo.Bounds;

  sType := VarAsType(AViewInfo.GridRecord.DisplayTexts[1], varString);
  if sType='T'  then
  begin
    ACanvas.Canvas.Brush.Color:=$00CDAD98;
  end
  else
  begin
//  ACanvas.Canvas.Brush.Bitmap := ABitmap;
  end;
  ACanvas.Canvas.FillRect(ARec);
  SetBkMode(ACanvas.Canvas.Handle, TRANSPARENT);
  ACanvas.DrawText(ATextToDraw, AViewInfo.Bounds, 0, True);
  ADone := True; // }
end;

procedure TfmMenuClass.acUpUpExecute(Sender: TObject);
begin
  with dmC do
  begin
    ViewM.BeginUpdate;
    quMenu.Active:=False;
    quMenu.ParamByName('Id_Parent').Value:=0; //������
    quMenu.Active:=True;
    quMenu.First;
    ViewM.EndUpdate;
    GridM.SetFocus;
  end;
end;

procedure TfmMenuClass.acUpExecute(Sender: TObject);
begin
  with dmC do
  begin
    ViewM.BeginUpdate;
    taMenu.Active:=True;
    if taMenu.Locate('SIFR',quMenuPARENT.AsInteger,[]) and (quMenuPARENT.AsInteger<>0) then
    begin
      quMenu.Active:=False;
      quMenu.ParamByName('Id_Parent').Value:=taMenuPARENT.AsInteger; //������
      quMenu.Active:=True;
      quMenu.First;
    end;
    taMenu.Active:=False;
    ViewM.EndUpdate;
    GridM.SetFocus;
  end;
end;

procedure TfmMenuClass.acExitExecute(Sender: TObject);
begin
  close;
end;

procedure TfmMenuClass.FormShow(Sender: TObject);
begin
  GridM.SetFocus;
end;

procedure TfmMenuClass.acOpenSelMenuExecute(Sender: TObject);
Var iParent:Integer;
    rQ:Real;
begin
  with dmC do
  begin
    if quMenuTREETYPE.AsString='T' then
    begin
      iParent:=quMenuSIFR.AsInteger;
      ViewM.BeginUpdate;
      dsMenu.DataSet:=Nil;

      quMenu.Active:=False;
      quMenu.ParamByName('Id_Parent').Value:=iParent; //������
      quMenu.Active:=True;
      quMenu.First;
//      delay(200);   //� ������� ��������� - ����� ���� First

      dsMenu.DataSet:=quMenu;
      ViewM.EndUpdate;
    end
    else //��������� ������� � �����
    begin
//    ModalResult:=mrOk;
//1      bBludoSel:=True;
//1      Close;
      if  (quCurSpecSifr.AsInteger=quMenuSIFR.AsInteger)and(quCurSpecIStatus.AsInteger=0) then
      begin //������ +1
        rQ:=quCurSpecQuantity.AsFloat+1;

        CalcDiscontN(quMenuSIFR.AsInteger,Check.Max,quMenuPARENT.AsInteger,quMenuSTREAM.AsInteger,quMenuPRICE.AsFloat,rQ,0,Tab.DBar,Now,Check.DProc,Check.DSum);

        quCurSpec.Edit;
        quCurSpecQuantity.AsFloat:=rQ;
        quCurSpecSumma.AsFloat:=roundEx(quMenuPRICE.AsFloat*rQ*100)/100-Check.DSum;
        quCurSpecDProc.AsFloat:=Check.DProc;
        quCurSpecDSum.AsFloat:=Check.DSum;
        quCurSpecIStatus.AsInteger:=0;
        quCurSpecName.AsString:=quMenuNAME.AsString;
        quCurSpecCode.AsString:=quMenuCODE.AsString;
        quCurSpecLinkM.AsInteger:=quMenuLINK.AsInteger;
        if quMenuLIMITPRICE.AsFloat>0 then
         quCurSpecLimitM.AsInteger:=RoundEx(quMenuLIMITPRICE.AsFloat)
        else quCurSpecLimitM.AsInteger:=99;
        quCurSpecStream.AsInteger:=quMenuSTREAM.AsInteger;
        quCurSpec.Post;

      end
      else //�����
      begin
        rQ:=1;
        if quMenuDESIGNSIFR.AsInteger>0 then
        begin
          bAddPosFromMenu:= True;
          fmCalc.CalcEdit1.EditValue:=1;
          fmCalc.ShowModal;
          if (fmCalc.ModalResult=mrOk)and(fmCalc.CalcEdit1.EditValue>0.00001)  then
          begin
            rQ:=RoundEx(fmCalc.CalcEdit1.EditValue*1000)/1000;
          end else rQ:=0;
          bAddPosFromMenu:= False;
        end;

        if rQ>0 then
        begin
          CalcDiscontN(quMenuSIFR.AsInteger,Check.Max+1,quMenuPARENT.AsInteger,quMenuSTREAM.AsInteger,quMenuPRICE.AsFloat,rQ,0,Tab.DBar,Now,Check.DProc,Check.DSum);

          quCurSpec.Append;
          quCurSpecSTATION.AsInteger:=CommonSet.Station;
          quCurSpecID_TAB.AsInteger:=Tab.Id;
          quCurSpecId_Personal.AsInteger:=Person.Id;
          quCurSpecNumTable.AsString:=Tab.NumTable;
          quCurSpecId.AsInteger:=Check.Max+1;
          quCurSpecSifr.AsInteger:=quMenuSIFR.AsInteger;
          quCurSpecPrice.AsFloat:=quMenuPRICE.AsFloat;
          quCurSpecQuantity.AsFloat:=rQ;
          quCurSpecSumma.AsFloat:=roundEx(quMenuPRICE.AsFloat*rQ*100)/100-Check.DSum;
          quCurSpecDProc.AsFloat:=Check.DProc;
          quCurSpecDSum.AsFloat:=Check.DSum;
          quCurSpecIStatus.AsInteger:=0;
          quCurSpecName.AsString:=quMenuNAME.AsString;
          quCurSpecCode.AsString:=quMenuCODE.AsString;
          quCurSpecLinkM.AsInteger:=quMenuLINK.AsInteger;
          if quMenuLIMITPRICE.AsFloat>0 then
           quCurSpecLimitM.AsInteger:=RoundEx(quMenuLIMITPRICE.AsFloat)
          else quCurSpecLimitM.AsInteger:=99;
          quCurSpecStream.AsInteger:=quMenuSTREAM.AsInteger;
          quCurSpec.Post;
          inc(Check.Max);

          if quMenuLINK.AsInteger>0 then
          begin //������������
            acMod1.Execute;
          end;
        end;  
      end;
//      GridSpec.SetFocus;
    end;
  end;
end;

procedure TfmMenuClass.ViewMCellClick(Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
//Var iParent:Integer;
begin
  AHandled:=True;
  acOpenSelMenu.Execute;
{  with dmC do
  begin
    if quMenuTREETYPE.AsString='T' then
    begin
      iParent:=quMenuSIFR.AsInteger;
      ViewM.BeginUpdate;
      dsMenu.DataSet:=Nil;
      Label1.Caption:=FormatDateTime('hh:nn:ss',now);

      quMenu.Active:=False;
      quMenu.ParamByName('Id_Parent').Value:=iParent; //������
      quMenu.Active:=True;
      quMenu.First;
      delay(200);

      dsMenu.DataSet:=quMenu;
      ViewM.EndUpdate;
    end
    else //��������� ������� � �����
    begin
//    ModalResult:=mrOk;
      bBludoSel:=True;
      Close;
    end;
  end;}
end;

procedure TfmMenuClass.acMod1Execute(Sender: TObject);
begin
  //������������
  with dmC do
  begin
    quModif.Active:=False;
    quModif.ParamByName('PARENT').AsInteger:=quCurSpecLINKM.AsInteger;
    quModif.Active:=True;
    fmModif:=TFmModif.Create(Application);
    //������� �����
    MaxMod:=quCurSpecLimitM.AsInteger;
    CountMod:=quCurMod.RecordCount;

    fmModif.Label1.Caption:='�������� � ���������� '+INtToStr(MaxMod-CountMod)+' ������������.';

    fmModif.ShowModal;
    fmModif.Release;
    quModif.Active:=False;
  end;
end;

end.
