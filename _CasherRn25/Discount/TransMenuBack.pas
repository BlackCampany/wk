unit TransMenuBack;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Placemnt, ComCtrls, SpeedBar, ExtCtrls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  RXSplit, cxCurrencyEdit, cxImageComboBox, cxTextEdit, XPStyleActnCtrls,
  ActnList, ActnMan, Menus, ToolWin, ActnCtrls, ActnMenus, StdCtrls,
  FR_Class, FR_DSet, FR_DBSet, DBClient, FR_BarC, FIBDataSet, pFIBDataSet,
  FIBDatabase, pFIBDatabase, ImgList;

type
  TfmMenuCr = class(TForm)
    FormPlacement1: TFormPlacement;
    StatusBar1: TStatusBar;
    MenuTree: TTreeView;
    RxSplitter1: TRxSplitter;
    ViewMenuCr: TcxGridDBTableView;
    LevelMenuCr: TcxGridLevel;
    GridMenuCr: TcxGrid;
    ViewMenuCrSIFR: TcxGridDBColumn;
    ViewMenuCrNAME: TcxGridDBColumn;
    ViewMenuCrPRICE: TcxGridDBColumn;
    ViewMenuCrCODE: TcxGridDBColumn;
    ViewMenuCrLIMITPRICE: TcxGridDBColumn;
    ViewMenuCrCATEG: TcxGridDBColumn;
    ViewMenuCrPARENT: TcxGridDBColumn;
    ViewMenuCrLINK: TcxGridDBColumn;
    ViewMenuCrSTREAM: TcxGridDBColumn;
    ViewMenuCrLACK: TcxGridDBColumn;
    ViewMenuCrALTNAME: TcxGridDBColumn;
    ViewMenuCrNALOG: TcxGridDBColumn;
    ViewMenuCrBARCODE: TcxGridDBColumn;
    ViewMenuCrIMAGE: TcxGridDBColumn;
    ViewMenuCrCONSUMMA: TcxGridDBColumn;
    ViewMenuCrMINREST: TcxGridDBColumn;
    ViewMenuCrPRNREST: TcxGridDBColumn;
    ViewMenuCrCOOKTIME: TcxGridDBColumn;
    ViewMenuCrDISPENSER: TcxGridDBColumn;
    ViewMenuCrDISPKOEF: TcxGridDBColumn;
    ViewMenuCrACCESS: TcxGridDBColumn;
    ViewMenuCrFLAGS: TcxGridDBColumn;
    ViewMenuCrTARA: TcxGridDBColumn;
    ViewMenuCrCNTPRICE: TcxGridDBColumn;
    ViewMenuCrBACKBGR: TcxGridDBColumn;
    ViewMenuCrFONTBGR: TcxGridDBColumn;
    ViewMenuCrIACTIVE: TcxGridDBColumn;
    ViewMenuCrIEDIT: TcxGridDBColumn;
    am3: TActionManager;
    acExit: TAction;
    Timer1: TTimer;
    ActionMainMenuBar1: TActionMainMenuBar;
    ViewMenuCrNAMECA: TcxGridDBColumn;
    ViewMenuCrNAMEMO: TcxGridDBColumn;
    ViewMenuCrIACTIVE1: TcxGridDBColumn;
    CasherRnDb: TpFIBDatabase;
    trSelM: TpFIBTransaction;
    trUpdM: TpFIBTransaction;
    quMenuTree: TpFIBDataSet;
    quMenuSel: TpFIBDataSet;
    quMenuSelSIFR: TFIBIntegerField;
    quMenuSelNAME: TFIBStringField;
    quMenuSelPRICE: TFIBFloatField;
    quMenuSelCODE: TFIBStringField;
    quMenuSelTREETYPE: TFIBStringField;
    quMenuSelLIMITPRICE: TFIBFloatField;
    quMenuSelCATEG: TFIBSmallIntField;
    quMenuSelPARENT: TFIBSmallIntField;
    quMenuSelLINK: TFIBSmallIntField;
    quMenuSelSTREAM: TFIBSmallIntField;
    quMenuSelLACK: TFIBSmallIntField;
    quMenuSelDESIGNSIFR: TFIBSmallIntField;
    quMenuSelALTNAME: TFIBStringField;
    quMenuSelNALOG: TFIBFloatField;
    quMenuSelBARCODE: TFIBStringField;
    quMenuSelIMAGE: TFIBSmallIntField;
    quMenuSelCONSUMMA: TFIBFloatField;
    quMenuSelMINREST: TFIBSmallIntField;
    quMenuSelPRNREST: TFIBSmallIntField;
    quMenuSelCOOKTIME: TFIBSmallIntField;
    quMenuSelDISPENSER: TFIBSmallIntField;
    quMenuSelDISPKOEF: TFIBSmallIntField;
    quMenuSelACCESS: TFIBSmallIntField;
    quMenuSelFLAGS: TFIBSmallIntField;
    quMenuSelTARA: TFIBSmallIntField;
    quMenuSelCNTPRICE: TFIBSmallIntField;
    quMenuSelBACKBGR: TFIBFloatField;
    quMenuSelFONTBGR: TFIBFloatField;
    quMenuSelIACTIVE: TFIBSmallIntField;
    quMenuSelIEDIT: TFIBSmallIntField;
    quMenuSelNAMECA: TFIBStringField;
    quMenuSelNAMEMO: TFIBStringField;
    dsMenuSel: TDataSource;
    taMenu: TpFIBDataSet;
    taMenuSIFR: TFIBIntegerField;
    taMenuNAME: TFIBStringField;
    taMenuPRICE: TFIBFloatField;
    taMenuCODE: TFIBStringField;
    taMenuTREETYPE: TFIBStringField;
    taMenuLIMITPRICE: TFIBFloatField;
    taMenuCATEG: TFIBSmallIntField;
    taMenuPARENT: TFIBSmallIntField;
    taMenuLINK: TFIBSmallIntField;
    taMenuSTREAM: TFIBSmallIntField;
    taMenuLACK: TFIBSmallIntField;
    taMenuDESIGNSIFR: TFIBSmallIntField;
    taMenuALTNAME: TFIBStringField;
    taMenuNALOG: TFIBFloatField;
    taMenuBARCODE: TFIBStringField;
    taMenuIMAGE: TFIBSmallIntField;
    taMenuCONSUMMA: TFIBFloatField;
    taMenuMINREST: TFIBSmallIntField;
    taMenuPRNREST: TFIBSmallIntField;
    taMenuCOOKTIME: TFIBSmallIntField;
    taMenuDISPENSER: TFIBSmallIntField;
    taMenuDISPKOEF: TFIBSmallIntField;
    taMenuACCESS: TFIBSmallIntField;
    taMenuFLAGS: TFIBSmallIntField;
    taMenuTARA: TFIBSmallIntField;
    taMenuCNTPRICE: TFIBSmallIntField;
    taMenuBACKBGR: TFIBFloatField;
    taMenuFONTBGR: TFIBFloatField;
    taMenuIACTIVE: TFIBSmallIntField;
    taMenuIEDIT: TFIBSmallIntField;
    quMenuFindParent: TpFIBDataSet;
    quMenuFindParentCOUNTREC: TFIBIntegerField;
    quMaxIdM: TpFIBDataSet;
    quMaxIdMMAXID: TFIBIntegerField;
    quMaxIdMe: TpFIBDataSet;
    quMaxIdMeMAXID: TFIBIntegerField;
    imState: TImageList;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    cxStyle14: TcxStyle;
    cxStyle15: TcxStyle;
    cxStyle16: TcxStyle;
    cxStyle17: TcxStyle;
    cxStyle18: TcxStyle;
    cxStyle19: TcxStyle;
    cxStyle20: TcxStyle;
    cxStyle21: TcxStyle;
    cxStyle22: TcxStyle;
    cxStyle23: TcxStyle;
    cxStyle24: TcxStyle;
    cxStyle25: TcxStyle;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure MenuTreeExpanding(Sender: TObject; Node: TTreeNode;
      var AllowExpansion: Boolean);
    procedure MenuTreeChange(Sender: TObject; Node: TTreeNode);
    procedure acExitExecute(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure ViewMenuCrCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure ViewMenuCrStartDrag(Sender: TObject;
      var DragObject: TDragObject);
    procedure ViewMenuCrDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure ViewMenuCrDragDrop(Sender, Source: TObject; X, Y: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmMenuCr: TfmMenuCr;
//  bDrM:Boolean=False;
  bClearMenu:Boolean = False;

implementation

uses Un1, dmRnEdit, AddCateg, AddM, dmOffice, Goods, uTransM;

{$R *.dfm}

procedure TfmMenuCr.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  GridMenuCr.Align:=AlClient;
  ViewMenuCr.RestoreFromIniFile(CurDir+GridIni);

  CasherRnDb.Connected:=False;
  CasherRnDb.DBName:=DBName;
  try
    CasherRnDb.Open;

    bMenuList:=False;
    CardsExpandLevel(nil,MenuTree,quMenuTree);
    quMenuTree.First;
    bMenuList:=True;

    quMenuSel.Active:=False;
    quMenuSel.ParamByName('PARENTID').AsInteger:=quMenuTree.fieldbyName('Id').AsInteger;
    quMenuSel.Active:=True;
  except
    CasherRnDb.Connected:=False;
  end;
end;

procedure TfmMenuCr.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CasherRnDb.Connected:=False;
  ViewMenuCr.StoreToIniFile(CurDir+GridIni,False);
  Release;
end;

procedure TfmMenuCr.MenuTreeExpanding(Sender: TObject; Node: TTreeNode;
  var AllowExpansion: Boolean);
begin
  if Node.getFirstChild.Data = nil then
  begin
    Node.DeleteChildren;
    CardsExpandLevel(Node,MenuTree,quMenuTree);
  end;
end;

procedure TfmMenuCr.MenuTreeChange(Sender: TObject; Node: TTreeNode);
begin
  if bMenuList then
  begin
    ViewMenuCr.BeginUpdate;

    quMenuSel.Active:=False;
    quMenuSel.ParamByName('PARENTID').AsInteger:=Integer(MenuTree.Selected.Data);
    quMenuSel.Active:=True;

    ViewMenuCr.EndUpdate;
  end;
end;

procedure TfmMenuCr.acExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmMenuCr.Timer1Timer(Sender: TObject);
begin
  if bClearMenu=True then begin StatusBar1.Panels[0].Text:=''; bClearMenu:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearMenu:=True;
end;

procedure TfmMenuCr.ViewMenuCrCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
Var iType,i:Integer;
begin
  if AViewInfo.GridRecord.Selected then exit;

  iType:=0;
  for i:=0 to ViewMenuCr.ColumnCount-1 do
  begin
    if ViewMenuCr.Columns[i].Name='ViewMenuCrIACTIVE1' then
    begin
      iType:=VarAsType(AViewInfo.GridRecord.DisplayTexts[i], varInteger);
      break;
    end;
  end;

  if iType=0  then
  begin
    ACanvas.Canvas.Brush.Color := clWhite;
    ACanvas.Canvas.Font.Color := clGray;
  end;

end;

procedure TfmMenuCr.ViewMenuCrStartDrag(Sender: TObject;
  var DragObject: TDragObject);
begin
  if not CanDo('prTransMenu') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  bDM:=True;
end;

procedure TfmMenuCr.ViewMenuCrDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept:=False;
  if bDM then  Accept:=True;
end;

procedure TfmMenuCr.ViewMenuCrDragDrop(Sender, Source: TObject; X,
  Y: Integer);
Var iCo:Integer;
    i,j: Integer;
    iNum: Integer;
    Rec,Rec1:TcxCustomGridRecord;
    iD:INteger;
    sBar:String;
    AHitTest: TcxCustomGridHitTest;
    bAdd:Boolean;
    K:Real;
begin
  if bDM then
  begin
    ResetAddVars;
    iCo:=fmGoods.ViewGoods.Controller.SelectedRecordCount;
    if iCo>1 then
    begin
      if MessageDlg('�� ������������� ������ �������� ��������� ������� ('+IntToStr(iCo)+' ��.) � ���� ���������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        with dmO do
        begin
          ViewMenuCr.BeginUpdate;
          for i:=0 to fmGoods.ViewGoods.Controller.SelectedRecordCount-1 do
          begin
            Rec:=fmGoods.ViewGoods.Controller.SelectedRecords[i];

            for j:=0 to Rec.ValueCount-1 do
            begin
              if fmGoods.ViewGoods.Columns[j].Name='ViewGoodsID' then break;
            end;

            iNum:=Rec.Values[j];
          //��� ��� - ����������
            with dmO do
            begin
              if quCardsSel.Locate('ID',iNum,[]) then
              begin
                try
                  quMaxIdMe.Active:=False;
                  quMaxIdMe.Active:=True;
                  Id:=quMaxIdMeMAXID.AsInteger+1;
                  quMaxIdMe.Active:=False;

                  sBar:=IntToStr(iD);
                  while length(sBar)<10 do sBar:='0'+sBar;
                  sBar:='25'+sBar;
                  sBar:=ToStandart(sBar);

                  trUpdM.StartTransaction;
                  quMenusel.Append;
                  quMenuSelSIFR.AsInteger:=Id;
                  quMenuSelNAME.AsString:=Copy(quCardsSelNAME.AsString,1,100);
                  quMenuSelPRICE.AsFloat:=0;
                 //��� ����
                  quMenuSelCODE.AsString:=IntToStr(iNum);
                  quMenuSelCONSUMMA.AsFloat:=1;
                  quMenuSelTREETYPE.AsString:='F';
                  quMenuSelLIMITPRICE.AsFloat:=0;

                  quMenuSelCATEG.AsInteger:=0;
                  quMenuSelPARENT.AsInteger:=Integer(MenuTree.Selected.Data);
                  quMenuSelLINK.AsInteger:=0;
                  quMenuSelSTREAM.AsInteger:=0;
                  quMenuSelLACK.AsInteger:=0;
                  quMenuSelDESIGNSIFR.AsInteger:=0;
                  quMenuSelALTNAME.AsString:='';
                  quMenuSelNALOG.AsFloat:=0;
                  quMenuSelBARCODE.AsString:=sBar;
                  quMenuSelIMAGE.AsInteger:=0;
                  quMenuSelMINREST.AsFloat:=0;
                  quMenuSelPRNREST.AsInteger:=0;
                  quMenuSelCOOKTIME.AsInteger:=0;
                  quMenuSelDISPENSER.AsInteger:=0;
                  quMenuSelDISPKOEF.AsInteger:=0;
                  quMenuSelACCESS.AsInteger:=0;
                  quMenuSelFLAGS.AsInteger:=0;
                  quMenuSelTARA.AsInteger:=0;
                  quMenuSelCNTPRICE.AsInteger:=0;
                  quMenuSelBACKBGR.AsFloat:=0;
                  quMenuSelFONTBGR.AsFloat:=0;
                  quMenuSelIACTIVE.AsInteger:=1;
                  quMenuSelIEDIT.AsInteger:=0;
                  quMenuSel.Post;
                  trUpdM.Commit;
                except
                end;
                quMenuSel.Refresh;
              end;
            end;
          end;
          ViewMenuCr.EndUpdate;
        end;
      end;
    end;
    if iCo=1 then //������ ���� ��������� �������
    begin
      if Sender is TcxGridSite then
      begin
        bAdd:=False;
        k:=1;
        AHitTest := TcxGridSite(Sender).ViewInfo.GetHitTest(X, Y);
        if AHitTest.HitTestCode=107 then //������
        begin
          Rec1:=TcxGridRecordHitTest(AHitTest).GridRecord;
          fmTransM:=tfmTransM.Create(Application);
          for j:=0 to Rec1.ValueCount-1 do
          begin
            if ViewMenuCr.Columns[j].Name='ViewMenuCrSIFR' then break;
          end;
          iNum:=Rec1.Values[j];
          if quMenuSel.Locate('SIFR',iNum,[]) then
          begin
            with fmTransM do
            begin
              cxTextEdit1.Text:=quMenuSelNAME.AsString;
              cxCalcEdit1.EditValue:=StrToINtDef(quMenuSelCODE.AsString,0);
              cxCalcEdit2.EditValue:=quMenuSelCONSUMMA.AsFloat;

              cxTextEdit2.Text:=dmO.quCardsSelNAME.AsString;
              cxCalcEdit3.EditValue:=dmO.quCardsSelID.AsInteger;
              cxCalcEdit4.EditValue:=1;
            end;
            fmTransM.ShowModal;
            if fmTransM.ModalResult=mrYes then
            begin //������
              trUpdM.StartTransaction;
              quMenusel.Edit;
              //��� ����
              quMenuSelNAME.AsString:=Copy(fmTransM.cxTextEdit2.Text,1,100);
              quMenuSelCODE.AsString:=INtToStr(Trunc(fmTransM.cxCalcEdit3.EditValue));
              quMenuSelCONSUMMA.AsFloat:=fmTransM.cxCalcEdit4.EditValue;
              quMenuSel.Post;
              trUpdM.Commit;
              quMenuSel.Refresh;
            end;
            if fmTransM.ModalResult=mrOk then //����������
            begin
              bAdd:=True;
              k:=fmTransM.cxCalcEdit4.EditValue;
            end;
          end else bAdd:=True;
          fmTransM.Release;
        end;
        if AHitTest.HitTestCode=0 then  bAdd:=True; //���� ����������
        if bAdd then   //����������
        begin
          quMaxIdMe.Active:=False;
          quMaxIdMe.Active:=True;
          Id:=quMaxIdMeMAXID.AsInteger+1;
          quMaxIdMe.Active:=False;

          sBar:=IntToStr(iD);
          while length(sBar)<10 do sBar:='0'+sBar;
          sBar:='25'+sBar;
          sBar:=ToStandart(sBar);

          trUpdM.StartTransaction;
          quMenusel.Append;
          quMenuSelSIFR.AsInteger:=Id;
          quMenuSelNAME.AsString:=Copy(dmO.quCardsSelNAME.AsString,1,100);
          quMenuSelPRICE.AsFloat:=0;
          //��� ����
          quMenuSelCODE.AsString:=IntToStr(dmO.quCardsSelID.AsInteger);
          quMenuSelCONSUMMA.AsFloat:=K;
          quMenuSelTREETYPE.AsString:='F';
          quMenuSelLIMITPRICE.AsFloat:=0;

          quMenuSelCATEG.AsInteger:=0;
          quMenuSelPARENT.AsInteger:=Integer(MenuTree.Selected.Data);
          quMenuSelLINK.AsInteger:=0;
          quMenuSelSTREAM.AsInteger:=0;
          quMenuSelLACK.AsInteger:=0;
          quMenuSelDESIGNSIFR.AsInteger:=0;
          quMenuSelALTNAME.AsString:='';
          quMenuSelNALOG.AsFloat:=0;
          quMenuSelBARCODE.AsString:=sBar;
          quMenuSelIMAGE.AsInteger:=0;
          quMenuSelMINREST.AsFloat:=0;
          quMenuSelPRNREST.AsInteger:=0;
          quMenuSelCOOKTIME.AsInteger:=0;
          quMenuSelDISPENSER.AsInteger:=0;
          quMenuSelDISPKOEF.AsInteger:=0;
          quMenuSelACCESS.AsInteger:=0;
          quMenuSelFLAGS.AsInteger:=0;
          quMenuSelTARA.AsInteger:=0;
          quMenuSelCNTPRICE.AsInteger:=0;
          quMenuSelBACKBGR.AsFloat:=0;
          quMenuSelFONTBGR.AsFloat:=0;
          quMenuSelIACTIVE.AsInteger:=1;
          quMenuSelIEDIT.AsInteger:=0;
          quMenuSel.Post;
          trUpdM.Commit;
          quMenuSel.Refresh;
        end;
      end;
    end;
  end;
end;

end.
