unit dmOffice;

interface

uses
  SysUtils, Classes, frexpimg, frRtfExp, frTXTExp, frXMLExl, frOLEExl,
  FR_E_HTML2, FR_E_HTM, FR_E_CSV, FR_E_RTF, FR_Class, FR_E_TXT, FIBQuery,
  pFIBQuery, pFIBStoredProc, DB, FIBDataSet, ImgList, Controls, cxStyles,
  pFIBDataSet, FIBDatabase, pFIBDatabase,DBClient,cxMemo,Variants;

type
  TdmO = class(TDataModule)
    OfficeRnDb: TpFIBDatabase;
    trSelect: TpFIBTransaction;
    trUpdate: TpFIBTransaction;
    trDel: TpFIBTransaction;
    quPer: TpFIBDataSet;
    quPerID: TFIBIntegerField;
    quPerID_PARENT: TFIBIntegerField;
    quPerNAME: TFIBStringField;
    quPerUVOLNEN: TFIBBooleanField;
    quPerCheck: TFIBStringField;
    quPerMODUL1: TFIBBooleanField;
    quPerMODUL2: TFIBBooleanField;
    quPerMODUL3: TFIBBooleanField;
    quPerMODUL4: TFIBBooleanField;
    quPerMODUL5: TFIBBooleanField;
    quPerMODUL6: TFIBBooleanField;
    dsPer: TDataSource;
    cxRepos1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    cxStyle14: TcxStyle;
    cxStyle15: TcxStyle;
    cxStyle16: TcxStyle;
    cxStyle17: TcxStyle;
    cxStyle18: TcxStyle;
    cxStyle19: TcxStyle;
    cxStyle20: TcxStyle;
    cxStyle21: TcxStyle;
    cxStyle22: TcxStyle;
    cxStyle23: TcxStyle;
    cxStyle24: TcxStyle;
    cxStyle25: TcxStyle;
    imState: TImageList;
    quCanDo: TpFIBDataSet;
    quCanDoID_PERSONAL: TFIBIntegerField;
    quCanDoNAME: TFIBStringField;
    quCanDoPREXEC: TFIBBooleanField;
    trCanDo: TpFIBTransaction;
    prGetId: TpFIBStoredProc;
    frTextExport1: TfrTextExport;
    frRTFExport1: TfrRTFExport;
    frCSVExport1: TfrCSVExport;
    frHTMExport1: TfrHTMExport;
    frHTML2Export1: TfrHTML2Export;
    frOLEExcelExport1: TfrOLEExcelExport;
    frXMLExcelExport1: TfrXMLExcelExport;
    frTextAdvExport1: TfrTextAdvExport;
    frRtfAdvExport1: TfrRtfAdvExport;
    frJPEGExport1: TfrJPEGExport;
    quMessTree: TpFIBDataSet;
    taMess: TpFIBDataSet;
    taMessID: TFIBIntegerField;
    taMessID_PARENT: TFIBIntegerField;
    taMessNAMESHORT: TFIBStringField;
    taMessNAMEFULL: TFIBStringField;
    taMessKOEF: TFIBFloatField;
    prCanDelMess: TpFIBStoredProc;
    quClassTree: TpFIBDataSet;
    taClass: TpFIBDataSet;
    taClassID: TFIBIntegerField;
    taClassID_PARENT: TFIBIntegerField;
    taClassITYPE: TFIBSmallIntField;
    taClassNAMECL: TFIBStringField;
    prCanDelClass: TpFIBStoredProc;
    taPriceT: TpFIBDataSet;
    taPriceTID: TFIBIntegerField;
    taPriceTNAMEPRICE: TFIBStringField;
    dsPriceT: TDataSource;
    quMHTree: TpFIBDataSet;
    taMH: TpFIBDataSet;
    taMHID: TFIBIntegerField;
    taMHPARENT: TFIBIntegerField;
    taMHITYPE: TFIBIntegerField;
    taMHNAMEMH: TFIBStringField;
    taMHDEFPRICE: TFIBIntegerField;
    quMHSel: TpFIBDataSet;
    quMHSelID: TFIBIntegerField;
    quMHSelPARENT: TFIBIntegerField;
    quMHSelITYPE: TFIBIntegerField;
    quMHSelNAMEMH: TFIBStringField;
    quMHSelDEFPRICE: TFIBIntegerField;
    dsMHSel: TDataSource;
    quMHSelNAMEPRICE: TFIBStringField;
    quMHParent: TpFIBDataSet;
    quPriceTSel: TpFIBDataSet;
    quPriceTSelID: TFIBIntegerField;
    quPriceTSelNAMEPRICE: TFIBStringField;
    dsPriceTSel: TDataSource;
    quCardsSel: TpFIBDataSet;
    dsCardsSel: TDataSource;
    quCardsSelID: TFIBIntegerField;
    quCardsSelPARENT: TFIBIntegerField;
    quCardsSelNAME: TFIBStringField;
    quCardsSelTTYPE: TFIBIntegerField;
    quCardsSelIMESSURE: TFIBIntegerField;
    quCardsSelINDS: TFIBIntegerField;
    quCardsSelMINREST: TFIBFloatField;
    quCardsSelLASTPRICEIN: TFIBFloatField;
    quCardsSelLASTPRICEOUT: TFIBFloatField;
    quCardsSelLASTPOST: TFIBIntegerField;
    quCardsSelIACTIVE: TFIBIntegerField;
    quCardsSelNAMESHORT: TFIBStringField;
    quCardsSelNAMENDS: TFIBStringField;
    quCardsSelPROC: TFIBFloatField;
    taNDS: TpFIBDataSet;
    taNDSID: TFIBIntegerField;
    taNDSNAMENDS: TFIBStringField;
    taNDSPROC: TFIBFloatField;
    dsMess: TDataSource;
    dsNDS: TDataSource;
    quGdsFind: TpFIBDataSet;
    quBarFind: TpFIBDataSet;
    quBarFindBAR: TFIBStringField;
    quBarFindGOODSID: TFIBIntegerField;
    quBars: TpFIBDataSet;
    quBarsBAR: TFIBStringField;
    quBarsGOODSID: TFIBIntegerField;
    quBarsQUANT: TFIBFloatField;
    quBarsBARFORMAT: TFIBSmallIntField;
    quBarsPRICE: TFIBFloatField;
    trUpdCards: TpFIBTransaction;
    trUpdBars: TpFIBTransaction;
    quGoodsEU: TpFIBDataSet;
    quGoodsEUGOODSID: TFIBIntegerField;
    quGoodsEUIDATEB: TFIBIntegerField;
    quGoodsEUIDATEE: TFIBIntegerField;
    quGoodsEUTO100GRAMM: TFIBFloatField;
    dsGoodsEU: TDataSource;
    trUpdEU: TpFIBTransaction;
    quFind: TpFIBDataSet;
    dsFind: TDataSource;
    quFindID: TFIBIntegerField;
    quFindPARENT: TFIBIntegerField;
    quFindNAME: TFIBStringField;
    taClients: TpFIBDataSet;
    taClientsID: TFIBIntegerField;
    taClientsNAMECL: TFIBStringField;
    taClientsFULLNAMECL: TFIBStringField;
    taClientsINN: TFIBStringField;
    taClientsPHONE: TFIBStringField;
    taClientsMOL: TFIBStringField;
    taClientsCOMMENT: TFIBStringField;
    taClientsINDS: TFIBIntegerField;
    taClientsIACTIVE: TFIBSmallIntField;
    dsClients: TDataSource;
    cxStyle26: TcxStyle;
    quDocsInSel: TpFIBDataSet;
    trDocsSel: TpFIBTransaction;
    trDocsUpd: TpFIBTransaction;
    dsDocsInSel: TDataSource;
    quDocsInSelID: TFIBIntegerField;
    quDocsInSelDATEDOC: TFIBDateField;
    quDocsInSelNUMDOC: TFIBStringField;
    quDocsInSelDATESF: TFIBDateField;
    quDocsInSelNUMSF: TFIBStringField;
    quDocsInSelIDCLI: TFIBIntegerField;
    quDocsInSelIDSKL: TFIBIntegerField;
    quDocsInSelSUMIN: TFIBFloatField;
    quDocsInSelSUMUCH: TFIBFloatField;
    quDocsInSelSUMTAR: TFIBFloatField;
    quDocsInSelSUMNDS0: TFIBFloatField;
    quDocsInSelSUMNDS1: TFIBFloatField;
    quDocsInSelSUMNDS2: TFIBFloatField;
    quDocsInSelPROCNAC: TFIBFloatField;
    quDocsInSelNAMECL: TFIBStringField;
    quDocsInSelNAMEMH: TFIBStringField;
    quDocsInSelIACTIVE: TFIBIntegerField;
    quSpecInSel: TpFIBDataSet;
    quSpecInSelIDHEAD: TFIBIntegerField;
    quSpecInSelID: TFIBIntegerField;
    quSpecInSelNUM: TFIBIntegerField;
    quSpecInSelIDCARD: TFIBIntegerField;
    quSpecInSelQUANT: TFIBFloatField;
    quSpecInSelPRICEIN: TFIBFloatField;
    quSpecInSelSUMIN: TFIBFloatField;
    quSpecInSelPRICEUCH: TFIBFloatField;
    quSpecInSelSUMUCH: TFIBFloatField;
    quSpecInSelIDNDS: TFIBIntegerField;
    quSpecInSelSUMNDS: TFIBFloatField;
    quCardsSelTCARD: TFIBIntegerField;
    quTCards: TpFIBDataSet;
    quTCardsIDCARD: TFIBIntegerField;
    quTCardsID: TFIBIntegerField;
    quTCardsDATEB: TFIBDateField;
    quTCardsDATEE: TFIBDateField;
    quTCardsSHORTNAME: TFIBStringField;
    quTCardsRECEIPTNUM: TFIBStringField;
    quTCardsPOUTPUT: TFIBStringField;
    quTCardsPCOUNT: TFIBIntegerField;
    quTCardsPVES: TFIBFloatField;
    dsTCards: TDataSource;
    quTSpec: TpFIBDataSet;
    dsTSpec: TDataSource;
    quTSpecIDC: TFIBIntegerField;
    quTSpecIDT: TFIBIntegerField;
    quTSpecID: TFIBIntegerField;
    quTSpecIDCARD: TFIBIntegerField;
    quTSpecCURMESSURE: TFIBIntegerField;
    quTSpecNETTO: TFIBFloatField;
    quTSpecBRUTTO: TFIBFloatField;
    quTSpecNAME: TFIBStringField;
    quTSpecNAMESHORT: TFIBStringField;
    quFindEU: TpFIBDataSet;
    quFindEUGOODSID: TFIBIntegerField;
    quFindEUIDATEB: TFIBIntegerField;
    quFindEUIDATEE: TFIBIntegerField;
    quFindEUTO100GRAMM: TFIBFloatField;
    trSElMessure: TpFIBTransaction;
    quMessureSel: TpFIBDataSet;
    dsMessureSel: TDataSource;
    quMessureSelID: TFIBIntegerField;
    quMessureSelID_PARENT: TFIBIntegerField;
    quMessureSelNAMESHORT: TFIBStringField;
    quMessureSelKOEF: TFIBFloatField;
    quTSpecKOEF: TFIBFloatField;
    quTSpecKNB: TFIBFloatField;
    taClientsKPP: TFIBStringField;
    taClientsADDRES: TFIBStringField;
    quCardsSel1: TpFIBDataSet;
    dsCardsSel1: TDataSource;
    quCardsSel1ID: TFIBIntegerField;
    quCardsSel1PARENT: TFIBIntegerField;
    quCardsSel1NAME: TFIBStringField;
    quCardsSel1TTYPE: TFIBIntegerField;
    quCardsSel1IMESSURE: TFIBIntegerField;
    quCardsSel1INDS: TFIBIntegerField;
    quCardsSel1MINREST: TFIBFloatField;
    quCardsSel1LASTPRICEIN: TFIBFloatField;
    quCardsSel1LASTPRICEOUT: TFIBFloatField;
    quCardsSel1LASTPOST: TFIBIntegerField;
    quCardsSel1IACTIVE: TFIBIntegerField;
    quCardsSel1TCARD: TFIBIntegerField;
    quCardsSel1NAMESHORT: TFIBStringField;
    quCardsSel1NAMENDS: TFIBStringField;
    quCardsSel1PROC: TFIBFloatField;
    quS: TpFIBDataSet;
    quSTCARD: TFIBIntegerField;
    quST: TpFIBDataSet;
    FIBIntegerField1: TFIBIntegerField;
    trSel: TpFIBTransaction;
    quTCardsSDATEE: TStringField;
    quSpecInSelNAMEC: TFIBStringField;
    quSpecInSelSM: TFIBStringField;
    quSpecInSelNAMENDS: TFIBStringField;
    quSpecInSelIDM: TFIBIntegerField;
    quSpecInSelIM: TFIBIntegerField;
    prDelPart: TpFIBStoredProc;
    prAddPartIn: TpFIBStoredProc;
    prFindPartOut: TpFIBStoredProc;
    trSel1: TpFIBTransaction;
    prPartInDel: TpFIBStoredProc;
    taParams: TpFIBDataSet;
    taParamsID: TFIBIntegerField;
    taParamsIDATEB: TFIBIntegerField;
    taParamsIDATEE: TFIBIntegerField;
    taParamsIDSTORE: TFIBIntegerField;
    quMoveSel: TpFIBDataSet;
    quMoveSelID: TFIBIntegerField;
    quMoveSelPARENT: TFIBIntegerField;
    quMoveSelNAME: TFIBStringField;
    quMoveSelTTYPE: TFIBIntegerField;
    quMoveSelIMESSURE: TFIBIntegerField;
    quMoveSelINDS: TFIBIntegerField;
    quMoveSelMINREST: TFIBFloatField;
    quMoveSelIACTIVE: TFIBIntegerField;
    quMoveSelTCARD: TFIBIntegerField;
    quMoveSelNAMECL: TFIBStringField;
    quMoveSelNAMEMESSURE: TFIBStringField;
    quMoveSelKOEF: TFIBFloatField;
    quMoveSelPOSTIN: TFIBFloatField;
    quMoveSelPOSTOUT: TFIBFloatField;
    quMoveSelVNIN: TFIBFloatField;
    quMoveSelVNOUT: TFIBFloatField;
    quMoveSelINV: TFIBFloatField;
    quMoveSelQREAL: TFIBFloatField;
    quMoveSelItog: TFloatField;
    dsMoveSel: TDataSource;
    quDocsOutSel: TpFIBDataSet;
    quDocsOutSelID: TFIBIntegerField;
    quDocsOutSelDATEDOC: TFIBDateField;
    quDocsOutSelNUMDOC: TFIBStringField;
    quDocsOutSelDATESF: TFIBDateField;
    quDocsOutSelNUMSF: TFIBStringField;
    quDocsOutSelIDCLI: TFIBIntegerField;
    quDocsOutSelIDSKL: TFIBIntegerField;
    quDocsOutSelSUMIN: TFIBFloatField;
    quDocsOutSelSUMUCH: TFIBFloatField;
    quDocsOutSelSUMTAR: TFIBFloatField;
    quDocsOutSelSUMNDS0: TFIBFloatField;
    quDocsOutSelSUMNDS1: TFIBFloatField;
    quDocsOutSelSUMNDS2: TFIBFloatField;
    quDocsOutSelPROCNAC: TFIBFloatField;
    quDocsOutSelIACTIVE: TFIBIntegerField;
    quDocsOutSelNAMECL: TFIBStringField;
    quDocsOutSelNAMEMH: TFIBStringField;
    dsDocsOutSel: TDataSource;
    quSpecOutSel: TpFIBDataSet;
    quSpecOutSelIDHEAD: TFIBIntegerField;
    quSpecOutSelID: TFIBIntegerField;
    quSpecOutSelNUM: TFIBIntegerField;
    quSpecOutSelIDCARD: TFIBIntegerField;
    quSpecOutSelQUANT: TFIBFloatField;
    quSpecOutSelPRICEIN: TFIBFloatField;
    quSpecOutSelSUMIN: TFIBFloatField;
    quSpecOutSelPRICEUCH: TFIBFloatField;
    quSpecOutSelSUMUCH: TFIBFloatField;
    quSpecOutSelIDNDS: TFIBIntegerField;
    quSpecOutSelSUMNDS: TFIBFloatField;
    quSpecOutSelIDM: TFIBIntegerField;
    quSpecOutSelNAMEC: TFIBStringField;
    quSpecOutSelIM: TFIBIntegerField;
    quSpecOutSelSM: TFIBStringField;
    quSpecOutSelNAMENDS: TFIBStringField;
    prCalcLastPrice: TpFIBStoredProc;
    quM: TpFIBDataSet;
    quMID: TFIBIntegerField;
    quMID_PARENT: TFIBIntegerField;
    quMNAMESHORT: TFIBStringField;
    quMKOEF: TFIBFloatField;
    quSelPartIn: TpFIBDataSet;
    quSelPartInID: TFIBIntegerField;
    quSelPartInIDSTORE: TFIBIntegerField;
    quSelPartInIDDOC: TFIBIntegerField;
    quSelPartInARTICUL: TFIBIntegerField;
    quSelPartInIDCLI: TFIBIntegerField;
    quSelPartInDTYPE: TFIBIntegerField;
    quSelPartInQPART: TFIBFloatField;
    quSelPartInQREMN: TFIBFloatField;
    quSelPartInPRICEIN: TFIBFloatField;
    quSelPartInPRICEOUT: TFIBFloatField;
    quSelPartInIDATE: TFIBIntegerField;
    dsSelPartIn: TDataSource;
    quSelPartInSDATE: TStringField;
    prAddPartOut: TpFIBStoredProc;
    prDelPartOut: TpFIBStoredProc;
    prCalcLastPrice1: TpFIBStoredProc;
    quFindCard: TpFIBDataSet;
    quFindCardID: TFIBIntegerField;
    quFindCardPARENT: TFIBIntegerField;
    quFindCardNAME: TFIBStringField;
    quFindCardTTYPE: TFIBIntegerField;
    quFindCardIMESSURE: TFIBIntegerField;
    quFindCardINDS: TFIBIntegerField;
    quFindCardMINREST: TFIBFloatField;
    quFindCardLASTPRICEIN: TFIBFloatField;
    quFindCardLASTPRICEOUT: TFIBFloatField;
    quFindCardLASTPOST: TFIBIntegerField;
    quFindCardIACTIVE: TFIBIntegerField;
    quFindCardTCARD: TFIBIntegerField;
    quDocsOutB: TpFIBDataSet;
    quDocsOutBID: TFIBIntegerField;
    quDocsOutBDATEDOC: TFIBDateField;
    quDocsOutBNUMDOC: TFIBStringField;
    quDocsOutBDATESF: TFIBDateField;
    quDocsOutBNUMSF: TFIBStringField;
    quDocsOutBIDCLI: TFIBIntegerField;
    quDocsOutBIDSKL: TFIBIntegerField;
    quDocsOutBSUMIN: TFIBFloatField;
    quDocsOutBSUMUCH: TFIBFloatField;
    quDocsOutBSUMTAR: TFIBFloatField;
    quDocsOutBSUMNDS0: TFIBFloatField;
    quDocsOutBSUMNDS1: TFIBFloatField;
    quDocsOutBSUMNDS2: TFIBFloatField;
    quDocsOutBPROCNAC: TFIBFloatField;
    quDocsOutBIACTIVE: TFIBIntegerField;
    quDocsOutBNAMEMH: TFIBStringField;
    dsDocsOutB: TDataSource;
    quDOBHEAD: TpFIBDataSet;
    quDOBHEADID: TFIBIntegerField;
    quDOBHEADDATEDOC: TFIBDateField;
    quDOBHEADNUMDOC: TFIBStringField;
    quDOBHEADDATESF: TFIBDateField;
    quDOBHEADNUMSF: TFIBStringField;
    quDOBHEADIDCLI: TFIBIntegerField;
    quDOBHEADIDSKL: TFIBIntegerField;
    quDOBHEADSUMIN: TFIBFloatField;
    quDOBHEADSUMUCH: TFIBFloatField;
    quDOBHEADSUMTAR: TFIBFloatField;
    quDOBHEADSUMNDS0: TFIBFloatField;
    quDOBHEADSUMNDS1: TFIBFloatField;
    quDOBHEADSUMNDS2: TFIBFloatField;
    quDOBHEADPROCNAC: TFIBFloatField;
    quDOBHEADIACTIVE: TFIBIntegerField;
    quDOBHEADOPER: TFIBStringField;
    quDOBSPEC: TpFIBDataSet;
    quDOBSPECIDHEAD: TFIBIntegerField;
    quDOBSPECID: TFIBIntegerField;
    quDOBSPECIDCARD: TFIBIntegerField;
    quDOBSPECQUANT: TFIBFloatField;
    quDOBSPECIDM: TFIBIntegerField;
    quDOBSPECSIFR: TFIBIntegerField;
    quDOBSPECNAMEB: TFIBStringField;
    quDOBSPECCODEB: TFIBStringField;
    quDOBSPECKB: TFIBFloatField;
    quDOBSPECPRICER: TFIBFloatField;
    quDOBSPECDSUM: TFIBFloatField;
    quDOBSPECRSUM: TFIBFloatField;
    quDOBSPECKM: TFIBFloatField;
    quDocsOutBOPER: TFIBStringField;
    taDobSpec: TpFIBDataSet;
    taDobSpecIDHEAD: TFIBIntegerField;
    taDobSpecID: TFIBIntegerField;
    taDobSpecIDCARD: TFIBIntegerField;
    taDobSpecQUANT: TFIBFloatField;
    taDobSpecIDM: TFIBIntegerField;
    taDobSpecSIFR: TFIBIntegerField;
    taDobSpecNAMEB: TFIBStringField;
    taDobSpecCODEB: TFIBStringField;
    taDobSpecKB: TFIBFloatField;
    taDobSpecPRICER: TFIBFloatField;
    taDobSpecDSUM: TFIBFloatField;
    taDobSpecRSUM: TFIBFloatField;
    taDobSpecKM: TFIBFloatField;
    taDobSpecNAME: TFIBStringField;
    taDobSpecTCARD: TFIBIntegerField;
    taDobSpecNAMESHORT: TFIBStringField;
    dsDobSpec: TDataSource;
    quFindTCard: TpFIBDataSet;
    quFindTCardID: TFIBIntegerField;
    quFindTCardSHORTNAME: TFIBStringField;
    quFindTCardRECEIPTNUM: TFIBStringField;
    quFindTCardPOUTPUT: TFIBStringField;
    quFindTCardPCOUNT: TFIBIntegerField;
    quFindTCardPVES: TFIBFloatField;
    quRemn: TpFIBDataSet;
    quRemnREMN: TFIBFloatField;
    trSel2: TpFIBTransaction;
    taDobSpec1: TpFIBDataSet;
    taDobSpec1IDHEAD: TFIBIntegerField;
    taDobSpec1ID: TFIBIntegerField;
    taDobSpec1ARTICUL: TFIBIntegerField;
    taDobSpec1IDM: TFIBIntegerField;
    taDobSpec1SM: TFIBStringField;
    taDobSpec1KM: TFIBFloatField;
    taDobSpec1QUANT: TFIBFloatField;
    taDobSpec1NAME: TFIBStringField;
    taDobSpec1SUMIN: TFIBFloatField;
    quMHAll: TpFIBDataSet;
    quMHAllID: TFIBIntegerField;
    quMHAllPARENT: TFIBIntegerField;
    quMHAllITYPE: TFIBIntegerField;
    quMHAllNAMEMH: TFIBStringField;
    quMHAllDEFPRICE: TFIBIntegerField;
    quMHAllNAMEPRICE: TFIBStringField;
    dsMHAll: TDataSource;
    taMessITYPE: TFIBSmallIntField;
    quMITYPE: TFIBSmallIntField;
    taCateg: TpFIBDataSet;
    taCategID: TFIBIntegerField;
    taCategNAMECAT: TFIBStringField;
    dsCateg: TDataSource;
    quCardsSelCATEGORY: TFIBIntegerField;
    quCardsSel1CATEGORY: TFIBIntegerField;
    taDobSpec2: TpFIBDataSet;
    taDobSpec2IDHEAD: TFIBIntegerField;
    taDobSpec2IDB: TFIBIntegerField;
    taDobSpec2ID: TFIBIntegerField;
    taDobSpec2CODEB: TFIBIntegerField;
    taDobSpec2NAMEB: TFIBStringField;
    taDobSpec2QUANT: TFIBFloatField;
    taDobSpec2PRICEOUT: TFIBFloatField;
    taDobSpec2SUMOUT: TFIBFloatField;
    taDobSpec2IDCARD: TFIBIntegerField;
    taDobSpec2NAMEC: TFIBStringField;
    taDobSpec2QUANTC: TFIBFloatField;
    taDobSpec2PRICEIN: TFIBFloatField;
    taDobSpec2SUMIN: TFIBFloatField;
    taDobSpec2IM: TFIBIntegerField;
    taDobSpec2SM: TFIBStringField;
    taDobSpec2SB: TFIBStringField;
    quTO: TpFIBDataSet;
    dsTO: TDataSource;
    quTOIDATE: TFIBIntegerField;
    quTOIDSTORE: TFIBIntegerField;
    quTOREMNIN: TFIBFloatField;
    quTOREMNOUT: TFIBFloatField;
    quTOPOSTIN: TFIBFloatField;
    quTOPOSTOUT: TFIBFloatField;
    quTOVNIN: TFIBFloatField;
    quTOVNOUT: TFIBFloatField;
    quTOINV: TFIBFloatField;
    quTOQREAL: TFIBFloatField;
    quTOSel: TpFIBDataSet;
    quTOSelIDATE: TFIBIntegerField;
    quTOSelIDSTORE: TFIBIntegerField;
    quTOSelREMNIN: TFIBFloatField;
    quTOSelREMNOUT: TFIBFloatField;
    quTOSelPOSTIN: TFIBFloatField;
    quTOSelPOSTOUT: TFIBFloatField;
    quTOSelVNIN: TFIBFloatField;
    quTOSelVNOUT: TFIBFloatField;
    quTOSelINV: TFIBFloatField;
    quTOSelQREAL: TFIBFloatField;
    quTODel: TpFIBQuery;
    taTOPostIn: TpFIBDataSet;
    taTOPostInID: TFIBIntegerField;
    taTOPostInDATEDOC: TFIBDateField;
    taTOPostInNUMDOC: TFIBStringField;
    taTOPostInNAMECL: TFIBStringField;
    taTOPostInDATESF: TFIBDateField;
    taTOPostInNUMSF: TFIBStringField;
    taTOPostInIDCLI: TFIBIntegerField;
    taTOPostInSUMIN: TFIBFloatField;
    taTOPostInSUMUCH: TFIBFloatField;
    taTOPostInSUMTAR: TFIBFloatField;
    taTOPostOut: TpFIBDataSet;
    taTOPostOutID: TFIBIntegerField;
    taTOPostOutDATEDOC: TFIBDateField;
    taTOPostOutNUMDOC: TFIBStringField;
    taTOPostOutNAMECL: TFIBStringField;
    taTOPostOutDATESF: TFIBDateField;
    taTOPostOutNUMSF: TFIBStringField;
    taTOPostOutIDCLI: TFIBIntegerField;
    taTOPostOutSUMIN: TFIBFloatField;
    taTOPostOutSUMUCH: TFIBFloatField;
    taTOPostOutSUMTAR: TFIBFloatField;
    taTOOutB: TpFIBDataSet;
    taTOOutBID: TFIBIntegerField;
    taTOOutBDATEDOC: TFIBDateField;
    taTOOutBNUMDOC: TFIBStringField;
    taTOOutBDATESF: TFIBDateField;
    taTOOutBNUMSF: TFIBStringField;
    taTOOutBIDCLI: TFIBIntegerField;
    taTOOutBIDSKL: TFIBIntegerField;
    taTOOutBSUMIN: TFIBFloatField;
    taTOOutBSUMUCH: TFIBFloatField;
    taTOOutBSUMTAR: TFIBFloatField;
    taTOOutBOPER: TFIBStringField;
    quSelCardsCat: TpFIBDataSet;
    quSelCardsCatCATEGORY: TFIBIntegerField;
    quTONAMEMH: TFIBStringField;
    quSelPartIn1: TpFIBDataSet;
    quSelPartIn1ID: TFIBIntegerField;
    quSelPartIn1IDSTORE: TFIBIntegerField;
    quSelPartIn1IDDOC: TFIBIntegerField;
    quSelPartIn1ARTICUL: TFIBIntegerField;
    quSelPartIn1IDCLI: TFIBIntegerField;
    quSelPartIn1DTYPE: TFIBIntegerField;
    quSelPartIn1QPART: TFIBFloatField;
    quSelPartIn1QREMN: TFIBFloatField;
    quSelPartIn1PRICEIN: TFIBFloatField;
    quSelPartIn1PRICEOUT: TFIBFloatField;
    quSelPartIn1IDATE: TFIBIntegerField;
    prGdsMove: TpFIBStoredProc;
    taTOOutBCat: TpFIBDataSet;
    taTOOutBCatRSUM: TFIBFloatField;
    taTOOutCCat: TpFIBDataSet;
    taTOOutCCatRSUM: TFIBFloatField;
    quRemnDate: TpFIBDataSet;
    dsRemnDate: TDataSource;
    quRemnDateID: TFIBIntegerField;
    quRemnDatePARENT: TFIBIntegerField;
    quRemnDateNAME: TFIBStringField;
    quRemnDateTTYPE: TFIBIntegerField;
    quRemnDateIMESSURE: TFIBIntegerField;
    quRemnDateINDS: TFIBIntegerField;
    quRemnDateMINREST: TFIBFloatField;
    quRemnDateIACTIVE: TFIBIntegerField;
    quRemnDateTCARD: TFIBIntegerField;
    quRemnDateNAMECL: TFIBStringField;
    quRemnDateNAMEMESSURE: TFIBStringField;
    quRemnDateKOEF: TFIBFloatField;
    quRemnDateREMN: TFIBFloatField;
    quCPartIn: TpFIBDataSet;
    quCPartOut: TpFIBDataSet;
    quCMove: TpFIBDataSet;
    dsquCPartIn: TDataSource;
    dsquCPartOut: TDataSource;
    quCPartInID: TFIBIntegerField;
    quCPartInIDSTORE: TFIBIntegerField;
    quCPartInNAMEMH: TFIBStringField;
    quCPartInIDDOC: TFIBIntegerField;
    quCPartInNUMDOC: TFIBStringField;
    quCPartInARTICUL: TFIBIntegerField;
    quCPartInIDCLI: TFIBIntegerField;
    quCPartInNAMECL: TFIBStringField;
    quCPartInDTYPE: TFIBIntegerField;
    quCPartInQPART: TFIBFloatField;
    quCPartInQREMN: TFIBFloatField;
    quCPartInPRICEIN: TFIBFloatField;
    quCPartInPRICEOUT: TFIBFloatField;
    quCPartInIDATE: TFIBIntegerField;
    quCPartOutARTICUL: TFIBIntegerField;
    quCPartOutIDDATE: TFIBIntegerField;
    quCPartOutIDSTORE: TFIBIntegerField;
    quCPartOutNAMEMH: TFIBStringField;
    quCPartOutIDPARTIN: TFIBIntegerField;
    quCPartOutIDDOC: TFIBIntegerField;
    quCPartOutNUMDOC: TFIBStringField;
    quCPartOutIDCLI: TFIBIntegerField;
    quCPartOutNAMECL: TFIBStringField;
    quCPartOutDTYPE: TFIBIntegerField;
    quCPartOutQUANT: TFIBFloatField;
    quCPartOutPRICEIN: TFIBFloatField;
    quCPartOutSUMOUT: TFIBFloatField;
    quCPartInSUMIN: TFIBFloatField;
    quCPartInSUMREMN: TFIBFloatField;
    quCPartOutSUMIN: TFIBFloatField;
    quCMoveARTICUL: TFIBIntegerField;
    quCMoveIDATE: TFIBIntegerField;
    quCMoveIDSTORE: TFIBIntegerField;
    quCMoveNAMEMH: TFIBStringField;
    quCMovePOSTIN: TFIBFloatField;
    quCMovePOSTOUT: TFIBFloatField;
    quCMoveVNIN: TFIBFloatField;
    quCMoveVNOUT: TFIBFloatField;
    quCMoveINV: TFIBFloatField;
    quCMoveQREAL: TFIBFloatField;
    quCMoveRB: TFloatField;
    quCMoveRE: TFloatField;
    quCRemn: TpFIBDataSet;
    quCRemnREMN: TFIBFloatField;
    quFindUse: TpFIBDataSet;
    quFindUseMAXDATE: TFIBDateField;
    quFCard: TpFIBDataSet;
    quFCardID: TFIBIntegerField;
    quFCardIMESSURE: TFIBIntegerField;
    quFCardNAME: TFIBStringField;
    dsFCard: TDataSource;
    quFCardPARENT: TFIBIntegerField;
    quFCardTCARD: TFIBIntegerField;
    quFindGrName: TpFIBDataSet;
    quFindGrNameID: TFIBIntegerField;
    quFindGrNameID_PARENT: TFIBIntegerField;
    quFindGrNameITYPE: TFIBSmallIntField;
    quFindGrNameNAMECL: TFIBStringField;
    quAllCards: TpFIBDataSet;
    quAllCardsID: TFIBIntegerField;
    quAllCardsNAME: TFIBStringField;
    quAllCardsIMESSURE: TFIBIntegerField;
    quAllCardsTCARD: TFIBIntegerField;
    quAllCardsPARENT: TFIBIntegerField;
    quAllCardsNAMESHORT: TFIBStringField;
    quAllCardsKOEF: TFIBFloatField;
    quAllCardsNAMECL: TFIBStringField;
    quCalcRemnSum: TpFIBDataSet;
    quCalcRemnSumID: TFIBIntegerField;
    quCalcRemnSumIDSTORE: TFIBIntegerField;
    quCalcRemnSumIDDOC: TFIBIntegerField;
    quCalcRemnSumARTICUL: TFIBIntegerField;
    quCalcRemnSumIDCLI: TFIBIntegerField;
    quCalcRemnSumDTYPE: TFIBIntegerField;
    quCalcRemnSumQPART: TFIBFloatField;
    quCalcRemnSumQREMN: TFIBFloatField;
    quCalcRemnSumPRICEIN: TFIBFloatField;
    quCalcRemnSumPRICEOUT: TFIBFloatField;
    quCalcRemnSumIDATE: TFIBIntegerField;
    quDocsInId: TpFIBDataSet;
    quDocsInIdID: TFIBIntegerField;
    quDocsInIdDATEDOC: TFIBDateField;
    quDocsInIdNUMDOC: TFIBStringField;
    quDocsInIdDATESF: TFIBDateField;
    quDocsInIdNUMSF: TFIBStringField;
    quDocsInIdIDCLI: TFIBIntegerField;
    quDocsInIdIDSKL: TFIBIntegerField;
    quDocsInIdSUMIN: TFIBFloatField;
    quDocsInIdSUMUCH: TFIBFloatField;
    quDocsInIdSUMTAR: TFIBFloatField;
    quDocsInIdSUMNDS0: TFIBFloatField;
    quDocsInIdSUMNDS1: TFIBFloatField;
    quDocsInIdSUMNDS2: TFIBFloatField;
    quDocsInIdPROCNAC: TFIBFloatField;
    quDocsInIdIACTIVE: TFIBIntegerField;
    quFCardINDS: TFIBIntegerField;
    quDocsInCard: TpFIBDataSet;
    quDocsInCardNAME: TFIBStringField;
    quDocsInCardNAMESHORT: TFIBStringField;
    quDocsInCardIDCARD: TFIBIntegerField;
    quDocsInCardQUANT: TFIBFloatField;
    quDocsInCardPRICEIN: TFIBFloatField;
    quDocsInCardSUMIN: TFIBFloatField;
    quDocsInCardPRICEUCH: TFIBFloatField;
    quDocsInCardSUMUCH: TFIBFloatField;
    quDocsInCardIDNDS: TFIBIntegerField;
    quDocsInCardSUMNDS: TFIBFloatField;
    quDocsInCardIDM: TFIBIntegerField;
    quDocsInCardDATEDOC: TFIBDateField;
    quDocsInCardNUMDOC: TFIBStringField;
    quDocsInCardNAMECL: TFIBStringField;
    quDocsInCardNAMEMH: TFIBStringField;
    dsDocsInCard: TDataSource;
    prDelPartInv: TpFIBStoredProc;
    prAddPartIn1: TpFIBStoredProc;
    taTOInv: TpFIBDataSet;
    taTOInvNUMDOC: TFIBStringField;
    taTOInvSUM1: TFIBFloatField;
    taTOInvSUM11: TFIBFloatField;
    taTOInvSUM2: TFIBFloatField;
    taTOInvSUM21: TFIBFloatField;
    quTCardsTEHNO: TFIBStringField;
    quTCardsOFORM: TFIBStringField;
    quFindTCARD2: TFIBIntegerField;
    taTOVnIn: TpFIBDataSet;
    taTOVnInID: TFIBIntegerField;
    taTOVnInDATEDOC: TFIBDateField;
    taTOVnInNUMDOC: TFIBStringField;
    taTOVnInIDSKL_FROM: TFIBIntegerField;
    taTOVnInNAMEMH: TFIBStringField;
    taTOVnInIDSKL_TO: TFIBIntegerField;
    taTOVnInSUMIN: TFIBFloatField;
    taTOVnInSUMUCH: TFIBFloatField;
    taTOVnInSUMUCH1: TFIBFloatField;
    taTOVnOut: TpFIBDataSet;
    taTOVnOutID: TFIBIntegerField;
    taTOVnOutDATEDOC: TFIBDateField;
    taTOVnOutNUMDOC: TFIBStringField;
    taTOVnOutIDSKL_FROM: TFIBIntegerField;
    taTOVnOutNAMEMH: TFIBStringField;
    taTOVnOutIDSKL_TO: TFIBIntegerField;
    taTOVnOutSUMIN: TFIBFloatField;
    taTOVnOutSUMUCH: TFIBFloatField;
    taTOVnOutSUMUCH1: TFIBFloatField;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure quTCardsCalcFields(DataSet: TDataSet);
    procedure quMoveSelCalcFields(DataSet: TDataSet);
    procedure quSelPartInCalcFields(DataSet: TDataSet);
    procedure taDobSpecQUANTChange(Sender: TField);
    procedure taDobSpecPRICERChange(Sender: TField);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

Function CanDo(Name_F:String):Boolean;
Function GetId(ta:String):Integer;
procedure prSelPartIn(Articul,IdStore,iCli:Integer);
function prFindKM(Idm:Integer):Real; //���� �����������
function prFindKNM(Idm:Integer;Var km:Real):String; //���� ����������� � �������� ������� ��
//function prFindCardKNM(IdC:Integer;Var km:Real):String; //���� ����������� � �������� ������� ��

function prFindBrutto(IdC:Integer;rDate:TDateTime):Real;
Procedure prFindSM(Idm:Integer;Var SM:String; Var IM:Integer); //�������� ��������
function prFindMT(Idm:Integer):Integer;
Procedure prFindGroup(Idc:Integer;Var NameGr:String; Var Id_Group:Integer); //�������� ������

Function prCalcRemn(iCode,iDate,iSkl:Integer):Real;
Procedure prCalcBl(sBeg,sName:String;IdPos,iCode,iDate,iTCard:Integer;rQ:Real;IdM:INteger;taCalc,taCalcB:TClientDataSet;Memo1:TcxMemo);

//��� ���� ��� �� �����
Procedure prCalcBlPrice(iCode,iDate:Integer;rQ:Real;IdM:INteger;taCalc:TClientDataSet;Var rSum:Real);
Procedure prCalcBlPrice1(sBeg:String;iCode,iDate:Integer;rQ:Real;IdM:INteger;taCalc:TClientDataSet;Memo1:TcxMemo;Var rSumOut:Real);

Function prTOFindRemB(iDateB,IdSkl:Integer; Var rSum:Real):Integer;
Function prTOFind(iDateB,IdSkl:Integer):Integer;
Procedure prTODel(iDateB,IdSkl:Integer);
Function prTOPostIn(iD,IdSkl:Integer):Real;
Function prTOPostOut(iD,IdSkl:Integer):Real;
Function prTOVnIn(iD,IdSkl:Integer):Real;
Function prTOVnOut(iD,IdSkl:Integer):Real;
Function prTOInv(iD,IdSkl:Integer):Real;
Function prTOReal(iD,IdSkl:Integer):Real;
Function prTypeC(IdC:Integer):Integer;
procedure prPreShowMove(Id:INteger;DateB,DateE:TDateTime;IdSkl:INteger;NameC:String);
procedure prTestUseTC(IdC:Integer; Var iDate,iC:INteger);
Function prFindGrN(IdC:INteger):String;
Function prCalcRemnSum(iCode,iDate,iSkl:Integer;rQ:Real):Real;

var
  dmO: TdmO;
  rQb,rQe:Real;

implementation

uses Un1, DMOReps, CardsMove;

{$R *.dfm}

Procedure prFindGroup(Idc:Integer;Var NameGr:String; Var Id_Group:Integer); //�������� ������
begin
  with dmORep do
  begin
    Id_Group:=0;
    NameGr:='';

    quSelNameCl.Active:=False;
    quSelNameCl.ParamByName('IDC').AsInteger:=Idc;
    quSelNameCl.Active:=True;

    quSelNameCl.First;
    if quSelNameCl.RecordCount>0 then
    begin
      Id_Group:=quSelNameClPARENT.AsInteger;
      NameGr:=quSelNameClNAMECL.AsString;
    end;

    quSelNameCl.Active:=False;
  end;
end;

Function prCalcRemnSum(iCode,iDate,iSkl:Integer;rQ:Real):Real;
Var rSumCur,rQCur,rPriceCur:Real;
    k:Integer;
begin
  Result:=0;
  k:=1;
  if rQ<0 then k:=-1;
  rQCur:=rQ*k;
  rSumCur:=0;
  with dmO do
  begin

  { ������� - ����� ��������� 10-� ������ � ������� ���� (���� ���������) � �� ��� �������
SELECT FIRST 10 ID,IDSTORE,IDDOC,ARTICUL,IDCLI,DTYPE,QPART,QREMN,PRICEIN,PRICEOUT,IDATE
FROM OF_PARTIN
where IDSTORE=:IDSKL
and ARTICUL=:IDCARD
and IDATE<=:IDATE
ORDER by IDATE desc}

    quCalcRemnSum.Active:=False;
    quCalcRemnSum.ParamByName('IDSKL').AsInteger:=iSkl;
    quCalcRemnSum.ParamByName('IDCARD').AsInteger:=iCode;
    quCalcRemnSum.ParamByName('IDATE').AsInteger:=iDate;
    quCalcRemnSum.Active:=True;

    if quCalcRemnSum.RecordCount>0 then
    begin
      rPriceCur:=0;
      quCalcRemnSum.First;
      while (quCalcRemnSum.Eof=False)and(rQCur>0) do
      begin
        if k=-1 then
        begin //��� ������������� ��������� ����� ������ ���� ���������� ������� �� ���� ���������� �� ���-�� ������
          rSumCur:=rQCur*quCalcRemnSumPRICEIN.AsFloat;
          rQCur:=0;
        end else
        begin
          if quCalcRemnSumQPART.AsFloat>=rQCur then
          begin
            rSumCur:=rSumCur+rQCur*quCalcRemnSumPRICEIN.AsFloat;
            rQCur:=0;
          end else
          begin
            rSumCur:=rSumCur+quCalcRemnSumQPART.AsFloat*quCalcRemnSumPRICEIN.AsFloat;
            rQCur:=rQCur-quCalcRemnSumQPART.AsFloat;
            rPriceCur:=quCalcRemnSumPRICEIN.AsFloat;
          end;
        end;
        quCalcRemnSum.Next;
      end;
      if rQCur>0 then rSumCur:=rSumCur+rQCur*rPriceCur; //���� ��� ���-�� �������� �� �� ���� ��������� 10-� ������ (����� ������������)

      result:=rSumCur*k;
    end;
    quCalcRemnSum.Active:=False;
  end;
end;



Function prFindGrN(IdC:INteger):String;
begin
  with dmO do
  begin
    quFindGrName.Active:=False;
    quFindGrName.ParamByName('IDCL').AsInteger:=IdC;
    quFindGrName.Active:=True;
    if quFindGrName.RecordCount>0 then Result:=quFindGrNameNAMECL.AsString
    else Result:='';
    quFindGrName.Active:=False;
  end;
end;

procedure prTestUseTC(IdC:Integer; Var iDate,iC:INteger);
var QT1:TpFIBDataSet;
    iDD,iCC,iDD1,iCC1:Integer;
begin
  //���������� ���� ���������� ������������� ������ ����. ����� � � ����� �����������
//  iDD:=0;
  iCC:=IdC;
  //��� �������� � ��������
  with dmO do
  begin
    quFindUse.Active:=False;
    quFindUse.ParamByName('IDC').AsInteger:=IdC;
    quFindUse.Active:=True;
    iDD:=Trunc(quFindUseMaxDate.AsDateTime);
    quFindUse.Active:=False;

    //��� � �������� �����������
    QT1:=TpFIBDataSet.Create(Owner);
    QT1.Active:=False;
    QT1.Database:=OfficeRnDb;
    QT1.Transaction:=trSel;
    QT1.SelectSQL.Clear;
    QT1.SelectSQL.Add('SELECT IDC FROM OF_CARDSTSPEC');
    QT1.SelectSQL.Add('where IDCARD='+IntToStr(IdC));
    QT1.Active:=True;

    QT1.First;
    while not QT1.Eof do
    begin
      prTestUseTC(QT1.FieldByName('IDC').AsInteger,iDD1,iCC1);
      if iDD1>iDD then
      begin
        iDD:=iDD1; iCC:=iCC1;
      end;
      QT1.Next;
    end;
    QT1.Active:=False;
    QT1.Free;
  end;
  iDate:=iDD;
  iC:=iCC;
end;

procedure prPreShowMove(Id:INteger;DateB,DateE:TDateTime;IdSkl:INteger;NameC:String);
Var iDateB,iDateE:INteger;
begin
  fmCardsMove.Tag:=Id;
  fmCardsMove.Edit1.Text:=NameC;
  iDateB:=Trunc(DateB);
  iDateE:=Trunc(DateE);
  rQb:=0; rQe:=0;
  with dmO do
  begin
    quCPartIn.Active:=False;
    quCPartIn.SelectSQL.Clear;
    quCPartIn.SelectSQL.Add('SELECT pi.ID,pi.IDSTORE,mh.NAMEMH,pi.IDDOC,dh.NUMDOC,pi.ARTICUL,pi.IDCLI,cl.NAMECL,pi.DTYPE,pi.QPART,pi.QREMN,');
    quCPartIn.SelectSQL.Add('pi.PRICEIN,pi.PRICEOUT,pi.IDATE,(pi.PRICEIN*pi.QPART) as SUMIN,(pi.PRICEIN*pi.QREMN) as SUMREMN ');
    quCPartIn.SelectSQL.Add('FROM OF_PARTIN PI');
    quCPartIn.SelectSQL.Add('left join OF_MH mh on mh.ID=pi.IDSTORE');
    quCPartIn.SelectSQL.Add('left join OF_CLIENTS cl on cl.ID=pi.IDCLI');
    quCPartIn.SelectSQL.Add('left join of_docheadin dh on dh.ID=pi.IDDOC');
    quCPartIn.SelectSQL.Add('where pi.ARTICUL='+IntToStr(Id));
    quCPartIn.SelectSQL.Add('and pi.IDATE>='+IntToStr(iDateB));
    if iDateE<(iMaxDate-1) then
      quCPartIn.SelectSQL.Add('and pi.IDATE<'+IntToStr(iDateE+1));
    if IdSkl>0 then
      quCPartIn.SelectSQL.Add('and pi.IDSTORE='+IntToStr(IdSkl));
    quCPartIn.SelectSQL.Add('ORDER BY pi.IDATE');
    quCPartIn.Active:=True;


    quCPartOut.Active:=False;
    quCPartOut.SelectSQL.Clear;
    quCPartOut.SelectSQL.Add('SELECT po.ARTICUL,po.IDDATE,po.IDSTORE,mh.NAMEMH,po.IDPARTIN,po.IDDOC,dh.NUMDOC,po.IDCLI,cl.NAMECL,po.DTYPE,');
    quCPartOut.SelectSQL.Add('po.QUANT,po.PRICEIN,po.SUMOUT,(po.PRICEIN*po.QUANT) as SUMIN');
    quCPartOut.SelectSQL.Add('FROM OF_PARTOUT po');
    quCPartOut.SelectSQL.Add('left join OF_MH mh on mh.ID=po.IDSTORE');
    quCPartOut.SelectSQL.Add('left join OF_CLIENTS cl on cl.ID=po.IDCLI');
    quCPartOut.SelectSQL.Add('left join of_docheadoutb dh on dh.ID=po.IDDOC');
    quCPartOut.SelectSQL.Add('where po.ARTICUL='+IntToStr(Id));
    quCPartOut.SelectSQL.Add('and po.IDDATE>='+IntToStr(iDateB));
    if iDateE<(iMaxDate-1) then
      quCPartOut.SelectSQL.Add('and po.IDDATE<'+IntToStr(iDateE));
    if IdSkl>0 then
      quCPartOut.SelectSQL.Add('and po.IDSTORE='+IntToStr(IdSkl));
    quCPartOut.SelectSQL.Add('ORDER BY po.IDDATE');
    quCPartOut.Active:=True;

    quCRemn.Active:=False;
    quCRemn.SelectSQL.Clear;
    quCRemn.SelectSQL.Add('SELECT SUM(POSTIN-POSTOUT+VNIN-VNOUT+INV-QREAL) as REMN');
    quCRemn.SelectSQL.Add('FROM OF_GDSMOVE');
    quCRemn.SelectSQL.Add('where ARTICUL='+IntToStr(Id));
    quCRemn.SelectSQL.Add('and IDATE<'+IntToStr(iDateB));
    if IdSkl>0 then
      quCRemn.SelectSQL.Add('and IDSTORE='+IntToStr(IdSkl));
    quCRemn.Active:=True;
    quCRemn.First;
    if quCRemn.RecordCount>0 then rQb:=quCRemnREMN.AsFloat;
    quCRemn.Active:=False;


    quCMove.Active:=False;
    quCMove.SelectSQL.Clear;
    quCMove.SelectSQL.Add('SELECT m.ARTICUL,m.IDATE,m.IDSTORE,mh.NAMEMH,m.POSTIN,m.POSTOUT,m.VNIN,m.VNOUT,m.INV,m.QREAL');
    quCMove.SelectSQL.Add('FROM OF_GDSMOVE m');
    quCMove.SelectSQL.Add('left join OF_MH mh on mh.ID=m.IDSTORE');
    quCMove.SelectSQL.Add('where m.ARTICUL='+IntToStr(Id));
    quCMove.SelectSQL.Add('and m.IDATE>='+IntToStr(iDateB));
    if iDateE<(iMaxDate-1) then
      quCMove.SelectSQL.Add('and m.IDATE<'+IntToStr(iDateE));
    if IdSkl>0 then
      quCMove.SelectSQL.Add('and m.IDSTORE='+IntToStr(IdSkl));
    quCMove.SelectSQL.Add('ORDER BY m.IDATE');
    quCMove.Active:=True;

    with dmORep do
    begin
      taCMove.Active:=False;
      taCMove.CreateDataSet;

      quCMove.First;
      while not quCMove.Eof do
      begin
        rQe:=rQb+quCMovePOSTIN.AsFloat-quCMovePOSTOUT.AsFloat+quCMoveVNIN.AsFloat-quCMoveVNOUT.AsFloat+quCMoveINV.AsFloat-quCMoveQREAL.AsFloat;
        taCMove.Append;
        taCMoveARTICUL.AsInteger:=quCMoveARTICUL.AsInteger;
        taCMoveIDATE.AsInteger:=quCMoveIDATE.AsInteger;
        taCMoveIDSTORE.AsInteger:=quCMoveIDSTORE.AsInteger;
        taCMoveNAMEMH.AsString:=quCMoveNAMEMH.AsString;
        taCMoveRB.AsFloat:=rQb;
        taCMovePOSTIN.AsFloat:=quCMovePOSTIN.AsFloat;
        taCMovePOSTOUT.AsFloat:=quCMovePOSTOUT.AsFloat;
        taCMoveVNIN.AsFloat:=quCMoveVNIN.AsFloat;
        taCMoveVNOUT.AsFloat:=quCMoveVNOUT.AsFloat;
        taCMoveINV.AsFloat:=quCMoveINV.AsFloat;
        taCMoveQREAL.AsFloat:=quCMoveQREAL.AsFloat;
        taCMoveRE.AsFloat:=rQe;
        taCMove.Post;

        rQb:=rQe;
        
        quCMove.Next;
      end;
      taCMove.First;
    end;

    quCMove.Active:=False;

    if iDateE>=iMaxDate then fmCardsMove.Caption:='�������� �� ������ "'+NameC+'" � '+FormatDateTiMe('dd.mm.yyyy',iDateB)
    else fmCardsMove.Caption:='�������� �� ������ "'+NameC+'" �� ������ � '+FormatDateTiMe('dd.mm.yyyy',iDateB)+' �� '+FormatDateTiMe('dd.mm.yyyy',iDateE-1);

  end;
end;


Function prTypeC(IdC:Integer):Integer;
begin
  Result:=1;
  with dmO do
  begin
    quSelCardsCat.Active:=False;
    quSelCardsCat.ParamByName('IDCARD').AsInteger:=IdC;
    quSelCardsCat.Active:=True;
    quSelCardsCat.First;
    if quSelCardsCat.RecordCount>0 then
    begin
      Result:=quSelCardsCatCATEGORY.AsInteger;
    end;
    quSelCardsCat.Active:=False;
  end;
end;

Function prTOReal(iD,IdSkl:Integer):Real;
Var rSum:Real;
begin
  with dmO do
  begin
    taTOOutB.Active:=False;
    taTOOutB.ParamByName('DATEB').AsDate:=iD;
    taTOOutB.ParamByName('IDSKL').AsInteger:=IdSkl;
    taTOOutB.Active:=True;

    rSum:=0;
    taTOOutB.First;
    while not taTOOutB.Eof do
    begin
      rSum:=rSum+taTOOutBSUMIN.AsFloat;
      taTOOutB.Next;
    end;
    taTOOutB.Active:=False;

  end;
  Result:=rSum;
end;

Function prTOVnIn(iD,IdSkl:Integer):Real;
Var rSum:Real;
begin
  with dmO do
  begin
    taTOVnIn.Active:=False;
    taTOVnIn.ParamByName('DATEB').AsDate:=iD;
    taTOVnIn.ParamByName('IDSKL').AsInteger:=IdSkl;
    taTOVnIn.Active:=True;

    rSum:=0;
    taTOVnIn.First;
    while not taTOVnIn.Eof do
    begin
      rSum:=rSum+taTOVnInSUMIN.AsFloat;
      taTOVnIn.Next;
    end;
    taTOVnIn.Active:=False;
  end;
  
  Result:=rSum;
end;

Function prTOVnOut(iD,IdSkl:Integer):Real;
Var rSum:Real;
begin
  with dmO do
  begin
    taTOVnOut.Active:=False;
    taTOVnOut.ParamByName('DATEB').AsDate:=iD;
    taTOVnOut.ParamByName('IDSKL').AsInteger:=IdSkl;
    taTOVnOut.Active:=True;

    rSum:=0;
    taTOVnOut.First;
    while not taTOVnOut.Eof do
    begin
      rSum:=rSum+taTOVnOutSUMIN.AsFloat;
      taTOVnOut.Next;
    end;
    taTOVnOut.Active:=False;
  end;
  Result:=rSum;
end;

Function prTOInv(iD,IdSkl:Integer):Real;
Var rSum:Real;
begin       //��������������
  with dmO do
  begin
    taTOInv.Active:=False;
    taTOInv.ParamByName('DATEB').AsDate:=iD;
    taTOInv.ParamByName('IDSKL').AsInteger:=IdSkl;
    taTOInv.Active:=True;

    rSum:=0;
    taTOInv.First;
    while not taTOInv.Eof do
    begin
      rSum:=rSum+taTOInvSUM2.AsFloat-taTOInvSUM1.AsFloat;
      taTOInv.Next;
    end;
    taTOInv.Active:=False;
  end;
  Result:=rSum;
end;

Function prTOPostOut(iD,IdSkl:Integer):Real;
Var rSum:Real;
begin
  with dmO do
  begin
    taTOPostOut.Active:=False;
    taTOpostOut.ParamByName('DATEB').AsDate:=iD;
    taTOpostOut.ParamByName('IDSKL').AsInteger:=IdSkl;
    taTOPostOut.Active:=True;

    rSum:=0;
    taTOPostOut.First;
    while not taTOPostOut.Eof do
    begin
      rSum:=rSum+taTOPostOutSUMIN.AsFloat;
      taTOPostOut.Next;
    end;
    taTOPostOut.Active:=False;
  end;
  Result:=rSum;
end;


Function prTOPostIn(iD,IdSkl:Integer):Real;
Var rSum:Real;
begin
  with dmO do
  begin
    taTOPostIn.Active:=False;
    taTOpostIn.ParamByName('DATEB').AsDate:=iD;
    taTOpostIn.ParamByName('IDSKL').AsInteger:=IdSkl;
    taTOPostIn.Active:=True;

    rSum:=0;
    taTOPostIn.First;
    while not taTOPostIn.Eof do
    begin
      rSum:=rSum+taTOPostInSUMIN.AsFloat;
      taTOPostIn.Next;
    end;
    taTOPostIn.Active:=False;
  end;
  Result:=rSum;
end;

Procedure prTODel(iDateB,IdSkl:Integer);
begin
  with dmO do
  begin
    WriteHistory('�������� �� � '+FormatDateTime('dd.mm.yyyy',iDateB)+'. ��������: '+Person.Name);
    quTODel.ParamByName('DATEB').AsInteger:=iDateB;
    quTODel.ParamByName('IDSKL').AsInteger:=IdSkl;
    quTODel.ExecQuery;
  end;
end;


Function prTOFind(iDateB,IdSkl:Integer):Integer;
begin
  with dmO do
  begin
    Result:=0;

    quTOSel.Active:=False;
    quTOSel.ParamByName('DATEB').AsInteger:=iDateB;
    quTOSel.ParamByName('DATEE').AsInteger:=iDateB+1;
    quTOSel.ParamByName('IDSKL').AsInteger:=IdSkl;
    quTOSel.Active:=True;
    if quTOSel.RecordCount>0 then Result:=1;
    quTOSel.Active:=False;
  end;
end;


Function prTOFindRemB(iDateB,IdSkl:Integer; Var rSum:Real):Integer;
begin
  with dmO do
  begin
    Result:=1;
    rSum:=0;

    quTOSel.Active:=False;
    quTOSel.ParamByName('DATEB').AsInteger:=iDateB-1;
    quTOSel.ParamByName('DATEE').AsInteger:=iDateB;
    quTOSel.ParamByName('IDSKL').AsInteger:=IdSkl;
    quTOSel.Active:=True;
    if quTOSel.RecordCount>0 then
    begin
      Result:=0;
      rSum:=quTOSelREMNOUT.AsFloat;
    end;
    quTOSel.Active:=False;
  end;
end;

Procedure prCalcBlPrice1(sBeg:String;iCode,iDate:Integer;rQ:Real;IdM:INteger;taCalc:TClientDataSet;Memo1:TcxMemo;Var rSumOut:Real);
Var iTC,iPCount:INteger;
    QT1:TpFIBDataSet;
    rQ1,kBrutto,kM,MassaB:Real;
    IdC,iMain:INteger;
    sM,StrWk,StrWk1,StrWk2,StrWk3,StrWk4:String;
    rSum:Real;
begin
  sBeg:=sBeg+'    ';
  with dmO do
  begin
    //���
    rSumOut:=0; MassaB:=1;
    iTC:=0; iPCount:=0;
    quFindTCard.Active:=False;
    quFindTCard.ParamByName('IDCARD').AsInteger:=iCode;
    quFindTCard.ParamByName('IDATE').AsDateTime:=iDate;
    quFindTCard.Active:=True;
    if quFindTCard.RecordCount>0 then
    begin
      iTC:=quFindTCardID.AsInteger;
      iPCount:=quFindTCardPCOUNT.AsInteger;
      MassaB:=quFindTCardPVES.AsFloat/1000;
      if prFindMT(IdM)=1 then MassaB:=1;
    end else
    begin
      inc(iErr);
      Memo1.Lines.Add(SBeg+'������: �� �� �������.')
    end;
    quFindTCard.Active:=False;
    if iTC>0 then
    begin
      QT1:=TpFIBDataSet.Create(Owner);
      QT1.Active:=False;
      QT1.Database:=OfficeRnDb;
      QT1.Transaction:=trSel;
      QT1.SelectSQL.Clear;
      QT1.SelectSQL.Add('SELECT CS.ID,CS.IDCARD,CS.CURMESSURE,CS.NETTO,CS.BRUTTO,CS.KNB,');
      QT1.SelectSQL.Add('CD.NAME,CD.TCARD');
      QT1.SelectSQL.Add('FROM OF_CARDSTSPEC CS');
      QT1.SelectSQL.Add('left join of_cards cd on cd.ID=CS.IDCARD');
      QT1.SelectSQL.Add('where IDC='+IntToStr(iCode)+' and IDT='+IntToStr(iTC));
      QT1.SelectSQL.Add('ORDER BY CS.ID');
      QT1.Active:=True;

      QT1.First;
      while not QT1.Eof do
      begin
        //����� �����
        rQ1:=QT1.FieldByName('NETTO').AsFloat;

        //��������� � �������� �������
        kM:=prFindKM(QT1.FieldByName('CURMESSURE').AsInteger);
        prFindSM(QT1.FieldByName('CURMESSURE').AsInteger,sM,iMain); //��� �������� �������� ������� ���������
        rQ1:=rQ1*kM; // 0,050

        //���� ������ �� ������ ����
        IdC:=QT1.FieldByName('IDCARD').AsInteger;
        kBrutto:=prFindBrutto(IdC,iDate);
        rQ1:=rQ1*(100+kBrutto)/100; //��� ����� � ������   //0,050
        rQ1:=rQ1/iPCount; //��� ����� �� 1-� ������ ���� �������� �� ������� //0,050
        rQ1:=rQ1*rQ/MassaB; //��� ����� �� ��� ���-�� ������ rQ/����� ������ 0,050*0,6/0,05

        StrWk:=QT1.FieldByName('NAME').AsString;
        While Length(StrWk)<20 do StrWk:=StrWk+' ';
        StrWk1:=QT1.FieldByName('IDCARD').AsString;
        While Length(StrWk1)<5 do StrWk1:=' '+StrWk1;
        Str((RoundEx(rQ1*1000)/1000):8:3,StrWk2);

        if QT1.FieldByName('TCARD').AsInteger=1 then
        begin  //���� ��

          Memo1.Lines.Add(sBeg+StrWk+' ('+StrWk1+'). �� ����. ���-��: '+StrWk2+' '+sM);
          prCalcBlPrice1(sBeg,QT1.FieldByName('IDCARD').AsInteger,iDate,rQ1,iMain,taCalc,Memo1,rSum);
        end else
        begin //��� ��
          rSum:=0;
          if taCalc.Locate('Articul',QT1.FieldByName('IDCARD').AsInteger,[]) then
            if taCalc.FieldByName('Quant').AsFloat<>0 then
              rSum:=taCalc.FieldByName('SumIn').AsFloat/taCalc.FieldByName('Quant').AsFloat*rQ1;
          Str((RoundEx(rSum*100)/100):8:2,StrWk3);
          Str((RoundEx(rSum/rQ1*100)/100):8:2,StrWk4);
          Memo1.Lines.Add(sBeg+StrWk+' ('+StrWk1+'). �� ���.  ���-��: '+StrWk2+' '+sM+'    �����: '+StrWk3+ '�'+'    ����: '+StrWk4+ '�');
        end;
        rSumOut:=rSumOut+rSum;
        QT1.Next;
      end;
      QT1.Active:=False;
      QT1.Free;
    end;
  end;
end;



Procedure prCalcBlPrice(iCode,iDate:Integer;rQ:Real;IdM:INteger;taCalc:TClientDataSet;Var rSum:Real);
Var iTC,iPCount:INteger;
    QT1:TpFIBDataSet;
    rQ1,kBrutto,kM:Real;
    IdC,iMain:INteger;
    sM:String;
    rSum1,MassaB:Real;
begin
  rSum:=0; MassaB:=1;
  with dmO do
  begin
    //���
    iTC:=0; iPCount:=0;
    quFindTCard.Active:=False;
    quFindTCard.ParamByName('IDCARD').AsInteger:=iCode;
    quFindTCard.ParamByName('IDATE').AsDateTime:=iDate;
    quFindTCard.Active:=True;
    if quFindTCard.RecordCount>0 then
    begin
      iTC:=quFindTCardID.AsInteger;
      iPCount:=quFindTCardPCOUNT.AsInteger;
      MassaB:=quFindTCardPVES.AsFloat/1000;
      if prFindMT(IdM)=1 then MassaB:=1;
    end;
    quFindTCard.Active:=False;
    if iTC>0 then
    begin
      QT1:=TpFIBDataSet.Create(Owner);
      QT1.Active:=False;
      QT1.Database:=OfficeRnDb;
      QT1.Transaction:=trSel;
      QT1.SelectSQL.Clear;
      QT1.SelectSQL.Add('SELECT CS.ID,CS.IDCARD,CS.CURMESSURE,CS.NETTO,CS.BRUTTO,CS.KNB,');
      QT1.SelectSQL.Add('CD.NAME,CD.TCARD');
      QT1.SelectSQL.Add('FROM OF_CARDSTSPEC CS');
      QT1.SelectSQL.Add('left join of_cards cd on cd.ID=CS.IDCARD');
      QT1.SelectSQL.Add('where IDC='+IntToStr(iCode)+' and IDT='+IntToStr(iTC));
      QT1.SelectSQL.Add('ORDER BY CS.ID');
      QT1.Active:=True;

      QT1.First;
      while not QT1.Eof do
      begin
        //����� �����
        rQ1:=QT1.FieldByName('NETTO').AsFloat;

        //��������� � �������� �������
        kM:=prFindKM(QT1.FieldByName('CURMESSURE').AsInteger);
        prFindSM(QT1.FieldByName('CURMESSURE').AsInteger,sM,iMain); //��� �������� �������� ������� ���������
        rQ1:=rQ1*kM;

        //���� ������ �� ������ ����
        IdC:=QT1.FieldByName('IDCARD').AsInteger;
        kBrutto:=prFindBrutto(IdC,iDate);
        rQ1:=rQ1*(100+kBrutto)/100; //��� ����� � ������
        rQ1:=rQ1/iPCount; //��� ����� �� 1-� ������ ���� �������� �� �������
        rQ1:=rQ1*rQ/MassaB; //��� ����� �� ��� ���-�� ������

        if QT1.FieldByName('TCARD').AsInteger=1 then
        begin  //���� ��
//          prCalcBl(sBeg,QT1.FieldByName('IDCARD').AsInteger,iDate,rQ,taCalc,Memo1);
          prCalcBlPrice(QT1.FieldByName('IDCARD').AsInteger,iDate,rQ1,iMain,taCalc,rSum1);
          rSum:=rSum+rSum1;
        end else
        begin //��� ��
          if taCalc.Locate('Articul',QT1.FieldByName('IDCARD').AsInteger,[]) then
            if taCalc.FieldByName('Quant').AsFloat<>0 then rSum:=rSum+taCalc.FieldByName('SumIn').AsFloat/taCalc.FieldByName('Quant').AsFloat*rQ1;
        end;
        QT1.Next;
      end;
      QT1.Active:=False;
      QT1.Free;
    end;
  end;
end;
                                                               //��� � ��������
Procedure prCalcBl(sBeg,sName:String;IdPos,iCode,iDate,iTCard:Integer;rQ:Real;IdM:INteger;taCalc,taCalcB:TClientDataSet;Memo1:TcxMemo);
Var iTC,iPCount:INteger;
    QT1:TpFIBDataSet;
    rQ1,kBrutto,kM,MassaB:Real;
    iMain:INteger;
    sM:String;
    Par:Variant;

Procedure prCalcSave;
Var rQb:Real;
begin
  par := VarArrayCreate([0,1], varInteger);
  par[0]:=IdPos;
  par[1]:=iCode;
  if taCalcB.Locate('ID;IDCARD',par,[]) then
  begin
    rQb:=taCalcB.fieldByName('QUANTC').AsFloat+rQ;
    taCalcB.edit;
    taCalcB.fieldByName('QUANTC').AsFloat:=rQb;
    taCalcB.fieldByName('PRICEIN').AsFloat:=0;
    taCalcB.fieldByName('SUMIN').AsFloat:=0;
    taCalcB.fieldByName('IM').AsInteger:=iMain;
    taCalcB.fieldByName('SM').AsString:=sM;
    taCalcB.fieldByName('SB').AsString:='';
    taCalcB.Post;
  end else
  begin
    with dmO do
    begin
      taCalcB.Append;
      taCalcB.fieldByName('ID').AsInteger:=IdPos;
      taCalcB.fieldByName('CODEB').AsInteger:=taDobSpecIDCARD.AsInteger;
      taCalcB.fieldByName('NAMEB').AsString:=taDobSpecNAME.AsString;
      taCalcB.fieldByName('QUANT').AsFloat:=taDobSpecQUANT.AsFloat;
      taCalcB.fieldByName('PRICEOUT').AsFloat:=taDobSpecPRICER.AsFloat;
      taCalcB.fieldByName('SUMOUT').AsFloat:=taDobSpecRSUM.AsFloat;
      taCalcB.fieldByName('IDCARD').AsInteger:=iCode;
      taCalcB.fieldByName('NAMEC').AsString:=sName;
      taCalcB.fieldByName('QUANTC').AsFloat:=rQ;
      taCalcB.fieldByName('PRICEIN').AsFloat:=0;
      taCalcB.fieldByName('SUMIN').AsFloat:=0;
      taCalcB.fieldByName('IM').AsInteger:=iMain;
      taCalcB.fieldByName('SM').AsString:=sM;
      taCalcB.fieldByName('SB').AsString:='';
      taCalcB.Post;
    end;
  end;

  if taCalc.Locate('Articul',iCode,[]) then
  begin
    rQ:=taCalc.fieldByName('Quant').AsFloat+rQ;
    taCalc.Edit;
    taCalc.fieldByName('Quant').AsFloat:=rQ;
    taCalc.fieldByName('QuantFact').AsFloat:=0;
    taCalc.fieldByName('QuantDiff').AsFloat:=0;
    taCalc.Post;
  end else
  begin
    taCalc.Append;
    taCalc.fieldByName('Articul').AsInteger:=iCode;
    taCalc.fieldByName('Name').AsString:=sName;
    taCalc.fieldByName('Idm').AsInteger:=iMain;
    taCalc.fieldByName('sM').AsString:=sM;
    taCalc.fieldByName('Km').AsFloat:=1;
    taCalc.fieldByName('Quant').AsFloat:=rQ;
    taCalc.fieldByName('QuantFact').AsFloat:=0;
    taCalc.fieldByName('QuantDiff').AsFloat:=0;
    taCalc.fieldByName('SumIn').AsFloat:=0;
    taCalc.fieldByName('SumUch').AsFloat:=0;
    taCalc.Post;
  end;

end;

begin
  sBeg:=sBeg+'    ';

  with dmO do
  begin
    //���
    if iTCard=1 then
    begin
      iTC:=0; iPCount:=0; MassaB:=1;
      quFindTCard.Active:=False;
      quFindTCard.ParamByName('IDCARD').AsInteger:=iCode;
      quFindTCard.ParamByName('IDATE').AsDateTime:=iDate;
      quFindTCard.Active:=True;
      if quFindTCard.RecordCount>0 then
      begin
        iTC:=quFindTCardID.AsInteger;
        iPCount:=quFindTCardPCOUNT.AsInteger;
        MassaB:=quFindTCardPVES.AsFloat/1000;
        if prFindMT(IdM)=1 then MassaB:=1;
        prFindSM(IdM,sM,iMain); //��� �������� �������� ������� ���������
        Memo1.Lines.Add(sBeg+sName+' ('+INtToStr(iCode)+'). �� ����. ���-��: '+FloatToStr(RoundEx(rQ*1000)/1000)+' '+sM);
      end else
      begin
        inc(iErr);
        Memo1.Lines.Add(SBeg+'������: �� �� �������.')
      end;
      quFindTCard.Active:=False;
      if iTC>0 then
      begin
        QT1:=TpFIBDataSet.Create(Owner);
        QT1.Active:=False;
        QT1.Database:=OfficeRnDb;
        QT1.Transaction:=trSel;
        QT1.SelectSQL.Clear;
        QT1.SelectSQL.Add('SELECT CS.ID,CS.IDCARD,CS.CURMESSURE,CS.NETTO,CS.BRUTTO,CS.KNB,');
        QT1.SelectSQL.Add('CD.NAME,CD.TCARD');
        QT1.SelectSQL.Add('FROM OF_CARDSTSPEC CS');
        QT1.SelectSQL.Add('left join of_cards cd on cd.ID=CS.IDCARD');
        QT1.SelectSQL.Add('where IDC='+IntToStr(iCode)+' and IDT='+IntToStr(iTC));
        QT1.SelectSQL.Add('ORDER BY CS.ID');
        QT1.Active:=True;

        QT1.First;
        while not QT1.Eof do
        begin
          rQ1:=QT1.FieldByName('NETTO').AsFloat;
          kM:=prFindKM(QT1.FieldByName('CURMESSURE').AsInteger);
          rQ1:=rQ1*kM;//��������� � �������� �������
          prFindSM(QT1.FieldByName('CURMESSURE').AsInteger,sM,iMain); //��� �������� �������� ������� ���������
          //���� ������ �� ������ ����
          kBrutto:=prFindBrutto(QT1.FieldByName('IDCARD').AsInteger,iDate);
          rQ1:=rQ1*(100+kBrutto)/100; //��� ����� � ������
          rQ1:=rQ1/iPCount; //��� ����� �� 1-� ������ ���� �������� �� �������
          rQ1:=rQ1*rQ/MassaB; //��� ����� �� ��� ���-�� ������

//          Memo1.Lines.Add(sBeg+QT1.FieldByName('NAME').AsString+' ('+QT1.FieldByName('IDCARD').AsString+'). �� ����. ���-��: '+FloatToStr(RoundEx(rQ1*1000)/1000)+' '+sM);
          prCalcBl(sBeg,QT1.FieldByName('NAME').AsString,IdPos,QT1.FieldByName('IDCARD').AsInteger,iDate,QT1.FieldByName('TCARD').AsInteger,rQ1,iMain,taCalc,taCalcB,Memo1);

          QT1.Next;
        end;
        QT1.Active:=False;
        QT1.Free;
      end;
    end else
    begin
      //��������� � �������� ������� �� ������ ������ ����� ������ ��� - �� ����
      kM:=prFindKM(IdM);
      prFindSM(IdM,sM,iMain); //��� �������� �������� ������� ���������
      rQ:=rQ*kM;              //������ ������� - ��� ������ �� �����������

      Memo1.Lines.Add(sBeg+sName+' ('+IntToStr(iCode)+'). �����.  ���-��: '+FloatToStr(RoundEx(rQ*1000)/1000)+' '+sM);

      //������ ���� ��� � �������� � � ������
      prCalcSave;
    end;
  end;
end;

Function GetId(ta:String):Integer;
Var iType:Integer;
begin
  with dmO do
  begin
    iType:=0;
    if ta='Pe' then iType:=1; //��������
    if ta='Cl' then iType:=2; //�������������
    if ta='Trh' then iType:=3; //��������� ������
    if ta='Trs' then iType:=4; //������������ ������
    if ta='TabH' then iType:=5; //��������� ������� (������)
    if ta='CS' then iType:=6; //��������� ������� (������)
    if ta='TH' then iType:=7; //��������� ������� (������) all
    if ta='MESS' then iType:=100; //������� ���������
    if ta='Class' then iType:=101; //������������� ������� � �����
    if ta='MH' then iType:=102; //����� �������� � ������������
    if ta='GD' then iType:=103; //�������
    if ta='Cli' then iType:=104; //�����������
    if ta='DocIn' then iType:=105; //��������� ��������� ����������
    if ta='SpecIn' then iType:=106; //������������ ��������� ����������
    if ta='TCards' then iType:=107; //��������������� �����
    if ta='PartIn' then iType:=108; //��������� ������
    if ta='InvH' then iType:=109; //��������� ��������������
    if ta='InvS' then iType:=110; //������������ ��������������
    if ta='Pars' then iType:=111; //��������� ������
    if ta='DocOut' then iType:=112; //��������� ��������� ����������
    if ta='SpecOut' then iType:=113; //������������ ��������� ����������
    if ta='DocOutB' then iType:=114; //��������� ��������� ���������� ����
    if ta='SpecOutB' then iType:=115; //������������ ��������� ���������� ����
    if ta='SpecOutC' then iType:=116; //������������ ��������� ���������� ��������
    if ta='HeadCompl' then iType:=117; //��������� ������������
    if ta='DocAct' then iType:=118; //��������� ����� �����������
    if ta='DocVn' then iType:=119; //��������� ����������
    if ta='SpecVn' then iType:=120; //��������� ����������

    prGetId.ParamByName('ITYPE').Value:=iType;
    prGetId.ExecProc;
    result:=prGetId.ParamByName('RESULT').Value;
  end;
end;


procedure TdmO.DataModuleCreate(Sender: TObject);
begin
  OfficeRnDb.Connected:=False;
  OfficeRnDb.DBName:=CommonSet.OfficeDb;
  try
    OfficeRnDb.Open;
  except
  end;
end;

procedure TdmO.DataModuleDestroy(Sender: TObject);
begin
  try
    OfficeRnDb.Close;
  except
  end;
end;

Function CanDo(Name_F:String):Boolean;
begin
  result:=True;
  with dmO do
  begin
    quCanDo.Active:=False;
    quCanDo.ParamByName('Person_id').Value:=Person.Id;
    quCanDo.ParamByName('Name_F').Value:=Name_F;
    quCanDo.Active:=True;
    if quCanDoprExec.AsBoolean=True then
     Result:=False;
  end;
end;


procedure TdmO.quTCardsCalcFields(DataSet: TDataSet);
begin
  if Trunc(quTCardsDATEE.AsDateTime)>=iMaxDate then quTCardsSDATEE.AsString:=' '
  else quTCardsSDATEE.AsString:=FormatDateTime('dd.mm.yyyy',Trunc(quTCardsDATEE.AsDateTime));
end;

procedure TdmO.quMoveSelCalcFields(DataSet: TDataSet);
begin
  quMoveSelItog.AsFloat:=quMoveSelPOSTIN.AsFloat-quMoveSelPOSTOUT.AsFloat+quMoveSelVNIN.AsFloat-quMoveSelVNOUT.AsFloat+quMoveSelINV.AsFloat-quMoveSelQREAL.AsFloat;
end;

procedure TdmO.quSelPartInCalcFields(DataSet: TDataSet);
begin
  quSelPartInSDATE.AsString:=FormatdateTime('dd.mm.yyyy',quSelPartInIDATE.AsInteger);
end;

procedure prSelPartIn(Articul,IdStore,iCli:Integer);
begin
  with dmO do
  begin
    quSelPartIn.Active:=False;
    quSelpartIn.SelectSQL.Clear;
    quSelPartIn.SelectSQL.Add('SELECT ID,IDSTORE,IDDOC,ARTICUL,IDCLI,DTYPE,QPART,QREMN,PRICEIN,PRICEOUT,IDATE');
    quSelPartIn.SelectSQL.Add('FROM OF_PARTIN');
    if iCli>0 then quSelPartIn.SelectSQL.Add('where QREMN>0 and IDSTORE='+IntToStr(IdStore)+' and ARTICUL='+IntToStr(Articul)+' and IDCLI='+IntToStr(iCli))
              else quSelPartIn.SelectSQL.Add('where QREMN>0 and IDSTORE='+IntToStr(IdStore)+' and ARTICUL='+IntToStr(Articul));
    quSelPartIn.SelectSQL.Add('Order by IDATE');

    quSelPartIn.Active:=true;
  end;
end;

function prFindMT(Idm:Integer):Integer;
begin
  with dmO do
  begin
    quM.Active:=False;
    quM.ParamByName('IDM').AsInteger:=Idm;
    quM.Active:=True;
    result:=quMITYPE.AsInteger;
  end;
end;


function prFindKM(Idm:Integer):Real;
begin
  with dmO do
  begin
    quM.Active:=False;
    quM.ParamByName('IDM').AsInteger:=Idm;
    quM.Active:=True;
    result:=quMKOEF.AsFloat;
  end;
end;

function prFindKNM(Idm:Integer;Var km:Real):String;
begin
  with dmO do
  begin
    quM.Active:=False;
    quM.ParamByName('IDM').AsInteger:=Idm;
    quM.Active:=True;
    Km:=quMKOEF.AsFloat;
    Result:=quMNAMESHORT.AsString;
  end;
end;


function prFindBrutto(IdC:Integer;rDate:TDateTime):Real;
Var iDate:Integer;
begin
  with dmO do
  begin
    Result:=0;
    try
      iDate:=prDateToI(rDate);
      quFindEU.Active:=False;
      quFindEU.ParamByName('GOODSID').AsInteger:=iDc;
      quFindEU.ParamByName('DATEB').AsInteger:=iDate;
      quFindEU.ParamByName('DATEE').AsInteger:=iDate;
      quFindEU.Active:=True;

      if quFindEU.RecordCount>0 then
      begin
        quFindEU.First;
        Result:=quFindEUTO100GRAMM.AsFloat;
      end;
    finally
      quFindEU.Active:=False;
    end;
  end;
end;

Procedure prFindSM(Idm:Integer;Var SM:String; Var IM:Integer); //�������� ��������
begin
  IM:=Idm;
  with dmO do
  begin
    try
      quM.Active:=False;
      quM.ParamByName('IDM').AsInteger:=IM;
      quM.Active:=True;
      if quMID_PARENT.AsInteger=0 then SM:=quMNAMESHORT.asstring
      else begin
        IM:=quMID_PARENT.AsInteger;
        quM.Active:=False;
        quM.ParamByName('IDM').AsInteger:=IM;
        quM.Active:=True;
        SM:=quMNAMESHORT.asstring;
      end;
    finally
      quM.Active:=False;
    end;
  end;
end;

Function prCalcRemn(iCode,iDate,iSkl:Integer):Real;
begin
  Result:=0;
  with dmO do
  begin
    quRemn.Active:=False;
    quRemn.ParamByName('IDCARD').AsInteger:=iCode;
    quRemn.ParamByName('IDATE').AsInteger:=iDate+1;
    quRemn.ParamByName('ISKL').AsInteger:=iSkl;
    quRemn.Active:=True;
    if quRemn.RecordCount>0 then
    begin
      quRemn.First;
      Result:=quRemnREMN.AsFloat;
    end;
    quRemn.Active:=False;
  end;
end;

procedure TdmO.taDobSpecQUANTChange(Sender: TField);
begin
  taDobSpecRSUM.AsFloat:=taDobSpecQUANT.AsFloat*taDobSpecPRICER.AsFloat;
end;

procedure TdmO.taDobSpecPRICERChange(Sender: TField);
begin
  taDobSpecRSUM.AsFloat:=taDobSpecQUANT.AsFloat*taDobSpecPRICER.AsFloat;
end;

end.
