unit OMessure;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, cxControls, cxSplitter, ExtCtrls, StdCtrls,
  DB, ImgList, ToolWin, Menus,Buttons,ActnList, ActnMan, ActnCtrls,
  ActnMenus, XPStyleActnCtrls, ActnColorMaps;

type
  TfmMessure = class(TForm)
    StatusBar1: TStatusBar;
    XPColorMap1: TXPColorMap;
    acMess: TActionManager;
    ActionMainMenuBar1: TActionMainMenuBar;
    Action1: TAction;
    Action2: TAction;
    Action3: TAction;
    Action4: TAction;
    Action5: TAction;
    Action8: TAction;
    Action9: TAction;
    MessTree: TTreeView;
    Timer1: TTimer;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    procedure Action1Execute(Sender: TObject);
    procedure Action2Execute(Sender: TObject);
    procedure Action3Execute(Sender: TObject);
    procedure Action5Execute(Sender: TObject);
    procedure Action6Execute(Sender: TObject);
    procedure Action7Execute(Sender: TObject);
    procedure Action8Execute(Sender: TObject);
    procedure Action9Execute(Sender: TObject);
    procedure MessTreeExpanded(Sender: TObject; Node: TTreeNode);
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure MessTreeDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmMessure: TfmMessure;
  bClearMess:Boolean = False;
  bAddSpec:Boolean = False;
  iMSel:Integer;

implementation

uses Un1, dmOffice, AddMess;

{$R *.dfm}

procedure TfmMessure.Action1Execute(Sender: TObject);
Var TreeNode : TTreeNode;
    iId,i:Integer;
    StrWk:String;
    k:Real;
begin
// �������� ��������
  if not CanDo('prAddMess') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  fmAddMess.Caption:='���������� �������� ��.���.';
  fmAddMess.label4.Caption:='���������� �������� ��.���.';
  fmAddMess.cxTextEdit1.Text:='';
  fmAddMess.cxTextEdit2.Text:='';
  fmAddMess.cxCalcEdit1.Value:=1;
  fmAddMess.cxCalcEdit1.Properties.ReadOnly:=True;
  fmAddMess.cxRButton1.Visible:=True;
  fmAddMess.cxRButton2.Visible:=True;
  fmAddMess.cxRButton1.Checked:=True;
  fmAddMess.cxRButton2.Checked:=False;

  fmAddMess.ShowModal;
  if fmAddMess.ModalResult=mrOk then
  begin
    with dmO do
    begin
      iId:=GetId('MESS');

      taMess.Active:=False;
      taMess.Active:=True;
      taMess.Append;
      taMessID.AsInteger:=iId;
      taMessID_PARENT.AsInteger:=0;
      taMessNAMESHORT.AsString:=Copy(fmAddMess.cxTextEdit1.Text,1,50);
      taMessNAMEFULL.AsString:=Copy(fmAddMess.cxTextEdit2.Text,1,100);
      taMessKOEF.AsFloat:=1;
      if fmAddMess.cxRButton1.Checked then taMessITYPE.AsInteger:=1 else taMessITYPE.AsInteger:=0;
      taMess.Post;

      k:=RoundEx(taMessKOEF.AsFloat*1000)/1000;
      StrWk:=taMess.FieldByName('NAMEFULL').AsString+' ('+taMess.FieldByName('NAMESHORT').AsString+' x'+FloatToStr(k)+')';

      MessTree.Items.BeginUpdate;
      TreeNode:=MessTree.Items.AddChildObject(nil,StrWk,Pointer(iId));
      TreeNode.ImageIndex:=26;
      TreeNode.SelectedIndex:=25;
      MessTree.Items.AddChildObject(TreeNode,'', nil);
      MessTree.Items.EndUpdate;

      for i:=0 To MessTree.Items.Count-1 do
        if Integer(MessTree.Items[i].Data) = iId then
        begin
          MessTree.Items[i].Expand(False);
          MessTree.Items[i].Selected:=True;
          MessTree.Repaint;
          Break;
        end;

      taMess.Active:=False;
    end;
  end;
end;

procedure TfmMessure.Action2Execute(Sender: TObject);
Var CurNode : TTreeNode;
    iId:Integer;
    StrWk:String;
    k:Real;
begin
// �������������
  if not CanDo('prEditMess') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  iId:=Integer(MessTree.Selected.Data);
  CurNode:=MessTree.Selected;
  with dmO do
  begin
    taMess.Active:=False;
    taMess.Active:=True;
    if taMess.Locate('ID',iId,[]) then
    begin
      if taMessITYPE.AsInteger=1 then
      begin
        fmAddMess.cxRButton1.Checked:=True;
        fmAddMess.cxRButton2.Checked:=False;
      end else
      begin
        fmAddMess.cxRButton1.Checked:=False;
        fmAddMess.cxRButton2.Checked:=True;
      end;
      if taMessID_PARENT.AsInteger<>0 then
      begin
        fmAddMess.Caption:='�������������� �������������� ��.���.';
        fmAddMess.label4.Caption:='�������������� �������������� ��.���.';
        fmAddMess.cxTextEdit1.Text:=taMessNAMESHORT.AsString;
        fmAddMess.cxTextEdit2.Text:=taMessNAMEFULL.AsString;
        fmAddMess.cxCalcEdit1.Value:=RoundEx(taMessKOEF.AsFloat*1000)/1000;
        fmAddMess.cxCalcEdit1.Properties.ReadOnly:=False;
      end else
      begin
        fmAddMess.Caption:='�������������� �������� ��.���.';
        fmAddMess.label4.Caption:='�������������� �������� ��.���.';
        fmAddMess.cxTextEdit1.Text:=taMessNAMESHORT.AsString;
        fmAddMess.cxTextEdit2.Text:=taMessNAMEFULL.AsString;
        fmAddMess.cxCalcEdit1.Value:=1;
        fmAddMess.cxCalcEdit1.Properties.ReadOnly:=True;
      end;
      fmAddMess.ShowModal;
      if fmAddMess.ModalResult=mrOk then
      begin
        taMess.Edit;
        taMessNAMESHORT.AsString:=Copy(fmAddMess.cxTextEdit1.Text,1,50);
        taMessNAMEFULL.AsString:=Copy(fmAddMess.cxTextEdit2.Text,1,100);
        taMessKOEF.AsFloat:=RoundEx(fmAddMess.cxCalcEdit1.Value*1000)/1000;
        if fmAddMess.cxRButton1.Checked then taMessITYPE.AsInteger:=1 else taMessITYPE.AsInteger:=0;
        taMess.Post;

        k:=RoundEx(taMessKOEF.AsFloat*1000)/1000;
        StrWk:=taMess.FieldByName('NAMEFULL').AsString+' ('+taMess.FieldByName('NAMESHORT').AsString+' x'+FloatToStr(k)+')';

        CurNode.Text:=strWk;
      end;
    end;
    taMess.Active:=False;
  end;
end;

procedure TfmMessure.Action3Execute(Sender: TObject);
Var CurNode : TTreeNode;
    iRes:Integer;
begin
//�������
  if not CanDo('prDelMess') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  CurNode:=MessTree.Selected;
  with dmO do
  begin
    if MessageDlg('�� ������������� ������ ������� ������� ��������� "'+CurNode.Text+'"', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      prCanDelMess.ParamByName('IDM').AsInteger:=Integer(CurNode.Data);
      prCanDelMess.ExecProc;
      iRes:=prCanDelMess.ParamByName('RESULT').AsInteger;

      if iRes=0 then
      begin //�������� ���������
        taMess.Active:=False;
        taMess.Active:=True;
        if taMess.Locate('ID',Integer(CurNode.Data),[]) then
        begin
          taMess.Delete;
          CurNode.Delete;
          MessTree.Repaint;
        end;
        taMess.Active:=False;
      end;

      if iRes=1 then
      begin //�������� ��������� - ���� ������ � ������ ������ ���������
        showmessage('�������� ���������! ���� �������������� ������� ���������.');
      end;

    end;
  end;
end;

procedure TfmMessure.Action5Execute(Sender: TObject);
Var TreeNode : TTreeNode;
    CurNode : TTreeNode;
    iId,i,iParent,iType:Integer;
    StrWk:String;
    k:Real;
begin
//�������� ��������������
  if not CanDo('prAddDopMess') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  iParent:=Integer(MessTree.Selected.Data);
  CurNode:=MessTree.Selected;
  with dmO do
  begin
    taMess.Active:=False;
    taMess.Active:=True;
    if taMess.Locate('ID',iParent,[]) then
    begin
      if taMessID_PARENT.AsInteger<>0 then
      begin
        //����� �� �� �������� ��
        showmessage('�������� �������� ������� ���������.');
      end else
      begin
        iType:=taMessITYPE.AsInteger;
        fmAddMess.Caption:='���������� �������������� ��.���.';
        fmAddMess.label4.Caption:='���������� �������������� ��.���.';
        fmAddMess.cxTextEdit1.Text:='';
        fmAddMess.cxTextEdit2.Text:='';
        fmAddMess.cxCalcEdit1.Value:=1;
        fmAddMess.cxCalcEdit1.Properties.ReadOnly:=False;
        if iType=1 then
        begin
          fmAddMess.cxRButton1.Checked:=True;
          fmAddMess.cxRButton2.Checked:=False;
        end else
        begin
          fmAddMess.cxRButton1.Checked:=False;
          fmAddMess.cxRButton2.Checked:=True;
        end;

        fmAddMess.ShowModal;
        if fmAddMess.ModalResult=mrOk then
        begin
          iId:=GetId('MESS');

          taMess.Append;
          taMessID.AsInteger:=iId;
          taMessID_PARENT.AsInteger:=iParent;
          taMessNAMESHORT.AsString:=Copy(fmAddMess.cxTextEdit1.Text,1,50);
          taMessNAMEFULL.AsString:=Copy(fmAddMess.cxTextEdit2.Text,1,100);
          taMessKOEF.AsFloat:=RoundEx(fmAddMess.cxCalcEdit1.Value*1000)/1000;
          if fmAddMess.cxRButton1.Checked then taMessITYPE.AsInteger:=1 else taMessITYPE.AsInteger:=0;
          taMess.Post;

          k:=RoundEx(taMessKOEF.AsFloat*1000)/1000;
          StrWk:=taMess.FieldByName('NAMEFULL').AsString+' ('+taMess.FieldByName('NAMESHORT').AsString+' x'+FloatToStr(k)+')';

          MessTree.Items.BeginUpdate;
          TreeNode:=MessTree.Items.AddChildObject(CurNode,StrWk,Pointer(iId));
          TreeNode.ImageIndex:=26;
          TreeNode.SelectedIndex:=25;
          MessTree.Items.AddChildObject(TreeNode,'', nil);
          MessTree.Items.EndUpdate;

          for i:=0 To MessTree.Items.Count-1 do
            if Integer(MessTree.Items[i].Data) = iId then
            begin
              MessTree.Items[i].Expand(False);
              MessTree.Items[i].Selected:=True;
              MessTree.Repaint;
              Break;
            end;

        end;
      end;
    end;
    taMess.Active:=False;
  end;
end;

procedure TfmMessure.Action6Execute(Sender: TObject);
begin
//
end;

procedure TfmMessure.Action7Execute(Sender: TObject);
begin
//
end;

procedure TfmMessure.Action8Execute(Sender: TObject);
begin
  Close;
end;

procedure TfmMessure.Action9Execute(Sender: TObject);
begin
  //
end;

procedure TfmMessure.MessTreeExpanded(Sender: TObject; Node: TTreeNode);
begin
  if Node.getFirstChild.Data = nil then
  begin
    Node.DeleteChildren;
    MessExpandLevel(Node,MessTree,dmO.quMessTree);
  end;
end;

procedure TfmMessure.FormCreate(Sender: TObject);
begin
  MessExpandLevel(nil,MessTree,dmO.quMessTree);
  MessTree.FullExpand;
  Timer1.Enabled:=True;
end;

procedure TfmMessure.Timer1Timer(Sender: TObject);
begin
  if bClearMess=True then begin StatusBar1.Panels[0].Text:=''; bClearMess:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearMess:=True;
end;

procedure TfmMessure.MessTreeDblClick(Sender: TObject);
begin
  if bAddSpec then
  begin
    bAddSpec:=False;
    iMSel:=Integer(MessTree.Selected.Data);
    ModalResult:=mrOk;
  end;  
end;

end.
