unit Balans;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SpeedBar, ExtCtrls, ComCtrls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  cxImageComboBox, ActnList, XPStyleActnCtrls, ActnMan, Placemnt;

type
  TfmBalans = class(TForm)
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem4: TSpeedItem;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    ViewBal: TcxGridDBTableView;
    LevBal: TcxGridLevel;
    GrBal: TcxGrid;
    ViewBalBARCODE: TcxGridDBColumn;
    ViewBalIACTIVE: TcxGridDBColumn;
    ViewBalCLINAME: TcxGridDBColumn;
    ViewBalTYPEOPL: TcxGridDBColumn;
    ViewBalSUM: TcxGridDBColumn;
    ViewBalNAME: TcxGridDBColumn;
    amBal: TActionManager;
    acHistory: TAction;
    SpeedItem3: TSpeedItem;
    FormPlacement1: TFormPlacement;
    procedure FormCreate(Sender: TObject);
    procedure SpeedItem4Click(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
    procedure acHistoryExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmBalans: TfmBalans;

implementation

uses prdb, DC, DmRnDisc, Un1, PCHistori;

{$R *.dfm}

procedure TfmBalans.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  GrBal.Align:=AlClient;
end;

procedure TfmBalans.SpeedItem4Click(Sender: TObject);
begin
  Close;
end;

procedure TfmBalans.SpeedItem2Click(Sender: TObject);
begin
  prNExportExel5(ViewBal);
end;

procedure TfmBalans.acHistoryExecute(Sender: TObject);
var  CliName,CliType:String;
     rBalans,rBalansDay,DLimit:Real;
begin
  with dmPC do
  begin
    if quBalansPC.RecordCount>0 then
    begin
      if ViewBal.Controller.SelectedRowCount=1 then
      begin
        fmPCHist.Caption:='������� �� ��������� ����� '+quBalansPCBARCODE.AsString+' �� ������ � '+FormatDateTime('dd.mm.yyyy hh:nn',TrebSel.DateFrom)+' �� '+FormatDateTime('dd.mm.yyyy hh:nn',TrebSel.DateTo);
        prFindPCardBalans(quBalansPCBARCODE.AsString,rBalans,rBalansDay,DLimit,CliName,CliType);
        fmPCHist.Label1.Caption:='��������  '+CliName+'     ���  '+CliType+'     ������  '+fts(rv(rBalans))+'     ������ �������  '+fts(rv(rBalansDay))+'     ������� �����  '+fts(rv((-1)*DLimit));
        fmPCHist.StatusBar1.Panels[1].Text:=quBalansPCBARCODE.AsString;
        fmPCHist.Label2.Caption:=quBalansPCBARCODE.AsString;
        with dmPC do
        begin
          fmPCHist.ViewPCHist.BeginUpdate;
          try
            quPCHist.Active:=False;
            quPCHist.ParamByName('DateB').AsDateTime:=TrebSel.DateFrom;
            quPCHist.ParamByName('DateE').AsDateTime:=Trunc(TrebSel.DateTo+1);
            quPCHist.ParamByName('BARCODE').AsString:=quBalansPCBARCODE.AsString;
            quPCHist.Active:=True;
          finally
            fmPCHist.ViewPCHist.EndUpdate;
          end;
        end;
        fmPCHist.Show;
      end;
    end;
  end;
end;

end.
