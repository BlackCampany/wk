unit CashEnd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxLookAndFeelPainters, StdCtrls, cxButtons, cxControls,
  cxContainer, cxEdit, cxTextEdit, cxCurrencyEdit, ExtCtrls, Placemnt,
  ActnList, XPStyleActnCtrls, ActnMan, dxfBackGround, dxfLabel, cxMaskEdit,
  cxDropDownEdit, cxCalc, Menus;

type
  TfmCash = class(TForm)
    cEdit1: TcxCurrencyEdit;
    CEdit2: TcxCurrencyEdit;
    CEdit3: TcxCurrencyEdit;
    Button5: TcxButton;
    cxButton1: TcxButton;
    FormPlacement1: TFormPlacement;
    Timer1: TTimer;
    am2: TActionManager;
    acExit: TAction;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Panel1: TPanel;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    cxButton5: TcxButton;
    cxButton6: TcxButton;
    cxButton7: TcxButton;
    cxButton8: TcxButton;
    cxButton9: TcxButton;
    cxButton10: TcxButton;
    cxButton11: TcxButton;
    cxButton12: TcxButton;
    cxButton13: TcxButton;
    cxButton14: TcxButton;
    Label4: TLabel;
    cxButton15: TcxButton;
    dxfBackGround1: TdxfBackGround;
    dxfLabel1: TdxfLabel;
    dxfLabel2: TdxfLabel;
    dxfLabel3: TdxfLabel;
    CEdit4: TcxCalcEdit;
    cxButton16: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure cxCurrencyEdit2PropertiesEditValueChanged(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton11Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
    procedure cxButton7Click(Sender: TObject);
    procedure cxButton8Click(Sender: TObject);
    procedure cxButton9Click(Sender: TObject);
    procedure cxButton10Click(Sender: TObject);
    procedure cxButton13Click(Sender: TObject);
    procedure cxButton15Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxButton14Click(Sender: TObject);
    procedure cxButton12Click(Sender: TObject);
    procedure cxButton16Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmCash: TfmCash;
  bFirst:Boolean;

implementation

uses Un1, CredCards, BnSber, UnCash, Dm;

{$R *.dfm}

procedure TfmCash.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  fmCash.ClientWidth:=690;
  if not CommonSet.bTouch then
  begin
    Panel1.Visible:=False;
    fmCash.ClientWidth:=500;
  end;
  if CommonSet.BNButton=0 then cxButton16.Enabled:=False else cxButton16.Enabled:=True; 
end;

procedure TfmCash.Timer1Timer(Sender: TObject);
begin
  if MessClear then
  begin
    Label4.Caption:='';
    MessClear:=False;
  end;
  if Label4.Caption>'' then MessClear:=True;
end;

procedure TfmCash.cxCurrencyEdit2PropertiesEditValueChanged(
  Sender: TObject);
begin

//��� ��� ������������ ���������

  CEdit3.EditValue:=roundEx((CEdit2.EditValue-CEdit1.EditValue)*100)/100;
  if CEdit3.EditValue<0 then
  begin
    Label4.Caption:='��������� ����� ������������ !';
    if fmCash.Visible then
    begin
      CEdit2.SetFocus;
      CEdit2.SelectAll;
    end;
  end
  else
  begin
    if fmCash.Visible and Button5.Enabled  then Button5.SetFocus;
  end;
end;

procedure TfmCash.Button5Click(Sender: TObject);
begin
  Button5.Enabled:=False;
  cEdit3.EditValue:=cEdit2.EditValue-CEdit1.EditValue;
  if CEdit3.EditValue<0 then
  begin
    Label4.Caption:='��������� ����� ������������ !';
    if fmCash.Visible then
    begin
      CEdit2.SetFocus;
      CEdit2.SelectAll;
    end;
  end
  else
  begin
    if fmCash.Visible then
    begin
      CEdit2.SetFocus;
    end;
    iCashType:=0; //������ ��������
    ModalResult:=mrOk;
  end;
end;

procedure TfmCash.acExitExecute(Sender: TObject);
begin
  if fmCash.Visible then
  begin
    CEdit2.SetFocus;
  end;
  ModalResult:=mrCancel;
end;

procedure TfmCash.cxButton2Click(Sender: TObject);
begin
  if bFirst then CEdit4.Value:=1
  else
    if pos(',',CEdit4.Text)=0 then CEdit4.EditValue:=CEdit4.EditValue*10+1
    else CEdit4.Text:=CEdit4.Text+'1';
  bFirst:=False;
end;

procedure TfmCash.cxButton11Click(Sender: TObject);
begin
  if bFirst then CEdit4.Value:=0
  else
    if pos(',',CEdit4.Text)=0 then CEdit4.EditValue:=CEdit4.EditValue*10
    else CEdit4.Text:=CEdit4.Text+'0';
  bFirst:=False;
end;

procedure TfmCash.cxButton3Click(Sender: TObject);
begin
  if bFirst then CEdit4.Value:=2
  else
    if pos(',',CEdit4.Text)=0 then CEdit4.EditValue:=CEdit4.EditValue*10+2
    else CEdit4.Text:=CEdit4.Text+'2';
  bFirst:=False;
end;

procedure TfmCash.cxButton4Click(Sender: TObject);
begin
  if bFirst then CEdit4.Value:=3
  else
    if pos(',',CEdit4.Text)=0 then CEdit4.EditValue:=CEdit4.EditValue*10+3
    else CEdit4.Text:=CEdit4.Text+'3';
  bFirst:=False;
end;

procedure TfmCash.cxButton5Click(Sender: TObject);
begin
  if bFirst then CEdit4.Value:=4
  else
    if pos(',',CEdit4.Text)=0 then CEdit4.EditValue:=CEdit4.EditValue*10+4
    else CEdit4.Text:=CEdit4.Text+'4';
  bFirst:=False;
end;

procedure TfmCash.cxButton6Click(Sender: TObject);
begin
  if bFirst then CEdit4.Value:=5
  else
    if pos(',',CEdit4.Text)=0 then CEdit4.EditValue:=CEdit4.EditValue*10+5
    else CEdit4.Text:=CEdit4.Text+'5';
  bFirst:=False;
end;

procedure TfmCash.cxButton7Click(Sender: TObject);
begin
  if bFirst then CEdit4.Value:=6
  else
    if pos(',',CEdit4.Text)=0 then CEdit4.EditValue:=CEdit4.EditValue*10+6
    else CEdit4.Text:=CEdit4.Text+'6';
  bFirst:=False;
end;

procedure TfmCash.cxButton8Click(Sender: TObject);
begin
  if bFirst then CEdit4.Value:=7
  else
    if pos(',',CEdit4.Text)=0 then CEdit4.EditValue:=CEdit4.EditValue*10+7
    else CEdit4.Text:=CEdit4.Text+'7';
  bFirst:=False;
end;

procedure TfmCash.cxButton9Click(Sender: TObject);
begin
  if bFirst then CEdit4.Value:=8
  else
    if pos(',',CEdit4.Text)=0 then CEdit4.EditValue:=CEdit4.EditValue*10+8
    else CEdit4.Text:=CEdit4.Text+'8';
  bFirst:=False;
end;

procedure TfmCash.cxButton10Click(Sender: TObject);
begin
  if bFirst then CEdit4.Value:=9
  else
    if pos(',',CEdit4.Text)=0 then CEdit4.EditValue:=CEdit4.EditValue*10+9
    else CEdit4.Text:=CEdit4.Text+'9';
  bFirst:=False;
end;

procedure TfmCash.cxButton13Click(Sender: TObject);
begin
  CEdit4.Value:=0;
end;

procedure TfmCash.cxButton15Click(Sender: TObject);
begin
  if cEdit4.EditValue <> 0 then
  begin
    cEdit2.EditValue:=cEdit4.EditValue;
  end;
end;

procedure TfmCash.FormShow(Sender: TObject);
begin
  cEdit4.EditValue:=0;
  bFirst:=True;
  Button5.Enabled:=True;
end;

procedure TfmCash.cxButton14Click(Sender: TObject);
begin
  cEdit4.EditValue:=Trunc(cEdit4.EditValue*100/10)/100;
end;

procedure TfmCash.cxButton12Click(Sender: TObject);
begin
  if pos(',',CEdit4.Text)=0 then CEdit4.Text:=CEdit4.Text+',';
  bFirst:=False;
end;

procedure TfmCash.cxButton16Click(Sender: TObject);
Var rMoney:Real;
//    StrWk:String;
begin
//    rMoney = ������ ���������� ����� � ������ � ������
    iPayType:=0;

    rMoney:=cEdit1.Value;
    if CommonSet.BNManual=1 then //������ ����� �������
    begin
      //������� ���� ������ ���� ����� � �����.
      try
        fmCredCards:=TfmCredCards.Create(Application);
        fmCredCards.CurrencyEdit1.EditValue:=rMoney;
        fmCredCards.Panel2.Visible:=False;
        fmCredCards.Edit1.Visible:=False;
        fmCredCards.GridBN.Visible:=True;
        fmCredCards.Button1.Visible:=True;
        fmCredCards.ShowModal;
        if fmCredCards.ModalResult<>mrOk then rMoney:=0; //���� ������ �� �����=0

      finally
        fmCredCards.Release;
      end;

      if rMoney<>0 then
      begin
        iCashType:=1; //������ �����������
        iPayType:=dmC.taCredCardID.AsInteger;
        ModalResult:=mrOk;
      end;

    end
    else  //�������������� ����� �������
    begin
      if CommonSet.BNManual=0 then
      begin
        //������� ���� ���������� ����� � �����.
        //������� ���� ������ ���� ����� � �����.
        try
          fmCredCards:=TfmCredCards.Create(Application);
          fmCredCards.CurrencyEdit1.EditValue:=rMoney;
          fmCredCards.Panel2.Visible:=True;
          fmCredCards.Edit1.Visible:=True;
          fmCredCards.GridBN.Visible:=False;
          fmCredCards.Button1.Visible:=False;
          bnBar:='';

          fmCredCards.ShowModal;
          if fmCredCards.ModalResult<>mrOk then rMoney:=0; //���� ������ �� �����=0


        finally
          fmCredCards.Release;
        end;

        if rMoney<>0 then
        begin
          iCashType:=1; //������ �����������
          ModalResult:=mrOk;
        end;
      end;
      if CommonSet.BNManual=2 then
      begin //��� ����
        try
          fmSber:=tFmSber.Create(Application);

          iMoneyBn:=RoundEx(rMoney*100); //� ��������

          fmSber.Label1.Visible:=True;
          fmSber.cxButton9.Enabled:=False;
          fmSber.cxButton10.Enabled:=False;
          fmSber.cxButton11.Enabled:=False;
          fmSber.cxButton6.Enabled:=False;

          if Operation=0 then //�������
          begin
            fmSber.cxButton2.Enabled:=True;
            fmSber.cxButton3.Enabled:=True;
            fmSber.cxButton4.Enabled:=True;
            fmSber.cxButton5.Enabled:=False;
            fmSber.cxButton7.Enabled:=False;
          end;
          if Operation=1 then //�������
          begin
            fmSber.cxButton2.Enabled:=False;
            fmSber.cxButton3.Enabled:=False;
            fmSber.cxButton4.Enabled:=False;
            fmSber.cxButton5.Enabled:=True;
            fmSber.cxButton7.Enabled:=True;
          end;

          BnStr:='';

          fmSber.ShowModal;

//����� ����� ������ ������ ����� ���������� �� ����������
          prBnPrint;
{          if  BnStr>'' then
          begin
            OpenNFDoc;
            while BnStr>'' do
            begin
              if (ord(BnStr[1])=$0D)or(ord(BnStr[1])=$0A) then Delete(BnStr,1,1)
              else
              begin
                if ord(BnStr[1])=$01 then
                begin
                  CutDoc;
                  Delete(BnStr,1,1);
                end
                else
                begin
                  if Pos(Chr($0D),BnStr)>0 then
                  begin
                   StrWk:=Copy(BnStr,1,Pos(Chr($0D),BnStr)-1);
                   Delete(BnStr,1,Pos(Chr($0D),BnStr));
                  end
                  else
                  begin
                    if Pos(Chr($01),BnStr)>0 then
                    begin
                      StrWk:=Copy(BnStr,1,Pos(Chr($01),BnStr)-1);
                      Delete(BnStr,1,Pos(Chr($01),BnStr)-1);
                    end
                    else
                    begin
                      StrWk:=BnStr;
                      BnStr:='';
                    end;
                  end;
                  PrintNFStr(StrWk);
                end;
              end;
            end;
            CloseNFDoc;
          end;}

          if fmSber.ModalResult<>mrOk then
          begin
//            rMoney:=0; //���� ������ �� �����=0
            iMoneyBn:=0;
          end;
          if iMoneyBn<>0 then
          begin
            iCashType:=1; //������ �����������
//            rMoney:=iMoneyBn/100;
            ModalResult:=mrOk;
          end;

        finally
          fmSber.Release;
        end;
      end;
    end;

end;

end.
