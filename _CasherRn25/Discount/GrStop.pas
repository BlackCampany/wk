unit GrStop;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, cxControls, cxContainer, cxTreeView, Menus;

type
  TfmGrDStop = class(TForm)
    Tree1: TcxTreeView;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    StatusBar1: TStatusBar;
    procedure FormCreate(Sender: TObject);
    procedure Tree1Expanding(Sender: TObject; Node: TTreeNode;
      var AllowExpansion: Boolean);
    procedure N1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmGrDStop: TfmGrDStop;

implementation

uses Un1, DmRnDisc, SetMaxDiscGr;

{$R *.dfm}

procedure TfmGrDStop.FormCreate(Sender: TObject);
begin
  Tree1.Align:=AlClient;
end;

procedure TfmGrDStop.Tree1Expanding(Sender: TObject; Node: TTreeNode;
  var AllowExpansion: Boolean);
begin
  Tree1.Items.BeginUpdate;
  if Node.getFirstChild.Data = nil then
  begin
    Node.DeleteChildren;
    CardsExpandLevel1(Node,Tree1,dmCDisc.quMenuTree);
  end;
  Tree1.Items.EndUpdate;
end;

procedure TfmGrDStop.N1Click(Sender: TObject);
Var IdGr:INteger;
    rMax:Real;
    StrWk:String;
begin
//�������
// �������� ���������
  if not CanDo('prSetMaxDiscGr') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  if (Tree1.Items.Count=0)  then
  begin
    showmessage('�������� ������.');
    exit;
  end;
  if (Tree1.Selected=nil)  then
  begin
    showmessage('�������� ������,���������.');
    exit;
  end;

  with dmCDisc do
  begin
    IdGr:=Integer(Tree1.Selected.data);
    rMax:=prGetDiscGr(IdGr);
    fmSetMaxDisc.Label2.Caption:=Tree1.Selected.Text;
    fmSetMaxDisc.cxCalcEdit1.Value:=rMax;
    fmSetMaxDisc.ShowModal;
    if fmSetMaxDisc.ModalResult=mrOk then
    begin
      rMax:=fmSetMaxDisc.cxCalcEdit1.EditValue;
      StrWk:=Tree1.Selected.Text;
      Delete(StrWk,Pos('(',StrWk)+1,Length(StrWk)-Pos('(',StrWk));
      StrWk:=StrWk+' '+fts(rMax)+' )';
      Tree1.Selected.Text:=StrWk;
      
      with dmCDisc do
      begin
        taDiscStopGr.Active:=False;
        taDiscStopGr.ParamByName('IDGR').AsInteger:=IdGr;
        taDiscStopGr.Active:=True;
        if taDiscStopGr.RecordCount>0 then
        begin
          taDiscStopGr.Edit;
          taDiscStopGrDISCMAX.AsFloat:=100-rMax;
          taDiscStopGr.Post;
        end else
        begin
          taDiscStopGr.Append;
          taDiscStopGrID.AsInteger:=IdGr;
          taDiscStopGrDISCMAX.AsFloat:=100-rMax;
          taDiscStopGr.Post;
        end;
        taDiscStopGr.Active:=False;
      end;
    end;
  end;
end;

end.
