unit ImportDK;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, cxButtons, StdCtrls, ExtCtrls,
  cxControls, cxContainer, cxEdit, cxTextEdit, cxMemo, cxMaskEdit,
  cxDropDownEdit, cxCalendar, DB, BseMain, BtrMain, cxCheckBox,
  IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, IdFTP, FileUtil,
  ShellApi,EasyCompression, DBClient;



type
  TfmImportDK = class(TForm)
    Memo1: TcxMemo;
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Timer1: TTimer;
    Label3: TLabel;
    IdFTP1: TIdFTP;
    cxButton3: TcxButton;
    procedure cxButton2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cxButton3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure prStart;
    Function prTr(NameF:String):Boolean;
    Function prTrFiles:Boolean;
  end;

//TSmallintField; TIntegerField; TStringField;

Procedure WrMess(S:String);
Function WrRec(taT:TtbTable):String;

var
  fmImportDK: TfmImportDK;
  fs: TECLFileStream;

implementation

uses Un1, u2fdk, Attention;

{$R *.dfm}

Function TfmImportDK.prTrFiles:Boolean;
var
  FList,Dest,PList,DList: TStrings;
  i,k,m,iPos:INteger;
  F:TextFile;
  bExit:Boolean;
  StrWk,StrWk1,StrWk2,StrWk3:String;
  BadPath:Boolean;

  Label ExitProc;
begin
  Result:=True;
  WrMess('  �������� �������� ������ �� FTP �������� ��������.');
  bExit:=False;

  //�������� ������ ���� ����� ��� ��������
  FList := TStringList.Create; //� ������ ��� ��������
  DList := TStringList.Create; //��� ����� ��� ����������

  //�������� ����������� �� ���
  Dest := TStringList.Create;

  //�������� ����� �������� �� ��� �� ���
  pList := TStringList.Create;
  StrWk:=CommonSet.FtpExport;
  While StrWk>'' do
  begin
    if Pos(r,strwk)>0 then
    begin
      StrWk1:=Copy(StrWk,1,Pos(r,strwk)-1);
      Delete(StrWk,1,Pos(r,strwk));
    end else
    begin
      StrWk1:=StrWk;
      StrWk:='';
    end;
    PList.Add(StrWk1);
  end;

  try
    //�������� ��� ��� ������
    try
      IDFtp1.DisConnect;
      IdFtp1.Host:=CommonSet.HostIp;
      IdFtp1.Username:=CommonSet.HostUser;
      IdFtp1.Password:=CommonSet.HostPassw;
      IdFTP1.Connect;
      IDFtp1.List( Dest );
    except
      Result:=False;
      bExit:=True;
    end;

    if bExit then goto ExitProc;

    //�������� ������ ������
    if FileExists(CurDir+'FileList.txt') then
    begin
      try
        Assignfile(f,CurDir+'FileList.txt');
        reset(f);
        while not EOF(f) do
        begin
          ReadLn(f,StrWk);
          iPos:=Pos('XXXXXX',StrWk);
          if iPos>0 then //����������� ���� ���� cn200750.btr
          begin
            StrWk1:=Copy(StrWk,1,iPos-1);

            StrWk2:=FormatDateTime('yyyy',Date);
            StrWk1:=StrWk1+StrWk2;
            StrWk2:=FormatDateTime('mm',Date);
            StrWk3:='01';
            if StrWk2='01' then StrWk3:='10';
            if StrWk2='02' then StrWk3:='20';
            if StrWk2='03' then StrWk3:='30';
            if StrWk2='04' then StrWk3:='40';
            if StrWk2='05' then StrWk3:='50';
            if StrWk2='06' then StrWk3:='60';
            if StrWk2='07' then StrWk3:='70';
            if StrWk2='08' then StrWk3:='80';
            if StrWk2='09' then StrWk3:='90';
            if StrWk2='10' then StrWk3:='a0';
            if StrWk2='11' then StrWk3:='b0';
            if StrWk2='12' then StrWk3:='c0';
            StrWk1:=StrWk1+StrWk3;
            StrWk:=StrWk1+'.btr';
          end;

          if StrWk>'' then
            if FileExists(StrWk) then
            begin
              FList.Add(StrWk); // ���������� � ������ �������� ����� ��� �������� � ������
              While Pos('\',StrWk)>0 do Delete(StrWk,1,Pos('\',StrWk));
{              if pos('.',StrWk)>0 then
              begin
                iPos:=Length(StrWk);
                Delete(StrWk,pos('.',StrWk),iPos-pos('.',StrWk)+1); //������ ����������
              end;                                                                     }
              StrWk:=StrWk+TrExt;
              DList.Add(StrWk); //���������� �������� ���  �����
            end;
        end;
      finally
        CloseFile(F);
      end;
    end;

//    FList.Add(NameF); // ���������� � ������ �������� ����� ��� ��������

{    FindRes:=FindFirst(CommonSet.PathExport+'*.*',32 ,SR);  //32 faArhive
    While FindRes=0 do // ���� �� ������� ����� (��������), �� ��������� ����
    begin
      FList.Add(SR.Name); // ���������� � ������ �������� ���������� ��������
      FindRes:=FindNext(SR); // ����������� ������ �� �������� ��������
    end;
    FindClose(SR); // ��������� �����
}
    //������ ������ ������������ -
    //1. �������� - 100 ��
    //2. �������� ����� �� ������ ������ - �������� �������� �� 10 �.
    //3. ����������
    //4. ��������
    //5. ��� ������ ���������� � �����
    //6. ���� ���� ������  - ����� ������� ���������� ����� ���� �� �����.

    //1.
    delay(100);
    For i:=0 to FList.Count-1 do
    begin
      WrMess(' ���� - '+FList.Strings[i]);
      {$I-}
      Assignfile(f,FList.Strings[i]);
      FileMode := 2;  {Set file access to read/Write }
      Reset(F);
      CloseFile(F);
      {$I+}
      if IOResult = 0 then
      begin
        WrMess(' ������ - ��.');
        try
          fs := TECLFileStream.Create(CurDir+DList.Strings[i],fmCreate,odInac,levelValue[9]);
          fs.LoadFromFile(FList.Strings[i]);
          fs.Free;
          WrMess(' ������ - ��.');

          For k:=0 to PList.Count-1 do
          begin
            StrWk:=PList.Strings[k];
            m:=0;
            BadPath:=False;
            While StrWk>'' do
            begin
              if Pos('\',strwk)>0 then
              begin
                StrWk1:=Copy(StrWk,1,Pos('\',strwk)-1);
                Delete(StrWk,1,Pos('\',strwk));
              end else
              begin
                StrWk1:=StrWk;
                StrWk:='';
              end;
              if strwk1>'' then
              begin
                WrMess('    ���� ��� �������� - '+StrWk1);
                try
                  IdFTP1.ChangeDir(StrWk1);
                  inc(m);
                  WrMess('    ���� ��.'+StrWk1);
                except
                  Result:=False;
                  WrMess('    ������ ��� �������� - '+StrWk1);
                  BadPath:=True;
                end;
              end;
            end;

            if not BadPath then //���� ���������� ����.
            begin
              try
                WrMess('      �������� ����� - '+DList.Strings[i]);
                IDFtp1.Put(DList.Strings[i], DList.Strings[i], False );
//                WrMess('      �������� Ok.');
              except
                Result:=False;
                WrMess('      ������ ��� �������� ����� - '+FList.Strings[i]+TrExt);
              end;
            end;

            While m>0 do
            begin
              IdFTP1.ChangeDir('..');//�� ������� ���������� - ��  ������� ���������.
              m:=m-1;
            end;
          end;

          //������� ������
          Assignfile(f,CurDir+DList.Strings[i]);
          erase(f);

{     //���������� ������

           if TrGood then //���������� ���� � �����
            MoveFile(CurDir+FList.Strings[i], CommonSet.PathArh+FList.Strings[i]);
}
        except
          Result:=False;
          WrMess(' ������ ��� ������.');
        end;
      end
      else
      begin
        Result:=False;
        WrMess(' � ������� ��������.');
      end;
      delay(100);
    end;
    ExitProc:
  finally
    IDFtp1.DisConnect;
    FList.Free;
    DList.Free;
    Dest.Free;
    PList.Free;
    WrMess('  �������� �������� ���������.');
  end;
end;



Function TfmImportDK.prTr(NameF:String):Boolean;
var
  FList,Dest,PList : TStrings;
  i,k,m:INteger;
//  F:TextFile;
  bExit:Boolean;
  StrWk,StrWk1:String;
  BadPath:Boolean;

  Label ExitProc;
begin
  Result:=True;
  WrMess('  �������� �������� ������ �� FTP.');
  bExit:=False;

  //�������� ������ ���� ����� ��� ��������
  FList := TStringList.Create;

  //�������� ����������� �� ���
  Dest := TStringList.Create;

  //�������� ����� �������� �� ��� �� ���
  pList := TStringList.Create;
  StrWk:=CommonSet.FtpExport;
  While StrWk>'' do
  begin
    if Pos(r,strwk)>0 then
    begin
      StrWk1:=Copy(StrWk,1,Pos(r,strwk)-1);
      Delete(StrWk,1,Pos(r,strwk));
    end else
    begin
      StrWk1:=StrWk;
      StrWk:='';
    end;
    PList.Add(StrWk1);
  end;

  try
    //�������� ��� ��� ������
    try
      IDFtp1.DisConnect;
      IdFtp1.Host:=CommonSet.HostIp;
      IdFtp1.Username:=CommonSet.HostUser;
      IdFtp1.Password:=CommonSet.HostPassw;
      IdFTP1.Connect;
      IDFtp1.List( Dest );
    except
      Result:=False;
      bExit:=True;
    end;

    if bExit then goto ExitProc;

    FList.Add(NameF); // ���������� � ������ �������� ����� ��� ��������

{    FindRes:=FindFirst(CommonSet.PathExport+'*.*',32 ,SR);  //32 faArhive
    While FindRes=0 do // ���� �� ������� ����� (��������), �� ��������� ����
    begin
      FList.Add(SR.Name); // ���������� � ������ �������� ���������� ��������
      FindRes:=FindNext(SR); // ����������� ������ �� �������� ��������
    end;
    FindClose(SR); // ��������� �����
}
    //������ ������ ������������ -
    //1. �������� - 100 ��
    //2. �������� ����� �� ������ ������ - �������� �������� �� 10 �. - ���
    //3. ���������� - ���
    //4. ��������
    //5. ��� ������ ���������� � ����� -���
    //6. ���� ���� ������  - ����� ������� ���������� ����� ���� �� �����.

    //1.
    delay(100);
    For i:=0 to FList.Count-1 do
    begin
      WrMess(' ���� - '+FList.Strings[i]);

      try
//        TrGood:=true; // ������������ ��� ��� ���������� ������

        For k:=0 to PList.Count-1 do
        begin
          StrWk:=PList.Strings[k];
          m:=0;
          BadPath:=False;
          While StrWk>'' do
          begin
            if Pos('\',strwk)>0 then
            begin
              StrWk1:=Copy(StrWk,1,Pos('\',strwk)-1);
              Delete(StrWk,1,Pos('\',strwk));
            end else
            begin
              StrWk1:=StrWk;
              StrWk:='';
            end;
            if strwk1>'' then
            begin
              WrMess('    ���� ��� �������� - '+StrWk1);
              try
                IdFTP1.ChangeDir(StrWk1);
                inc(m);
                WrMess('    ���� ��.'+StrWk1);
              except
                Result:=False;
                WrMess('    ������ ��� �������� - '+StrWk1);
                BadPath:=True;
              end;
            end;
          end;

          if not BadPath then //���� ���������� ����.
          begin
            try
              WrMess('      �������� ����� - '+FList.Strings[i]);
              IDFtp1.Put(CommonSet.PathExport+FList.Strings[i], FList.Strings[i], False );
//              WrMess('      �������� Ok.');
            except
              Result:=False;
//              TrGood:=False;
              WrMess('      ������ ��� �������� ����� - '+FList.Strings[i]+TrExt);
            end;
          end;

          While m>0 do
          begin
            IdFTP1.ChangeDir('..');//�� ������� ���������� - ��  ������� ���������.
            m:=m-1;
          end;
        end;
      except
        Result:=False;
      end;
    end;  
    delay(100);
    ExitProc:
  finally
    IDFtp1.DisConnect;
    FList.Free;
    Dest.Free;
    PList.Free;
  end;
end;


Function WrRec(taT:TtbTable):String;
Var StrWk,StrT,StrF,StrVal:String;
    i:Integer;
begin
  StrWk:=taT.TableName+';';
  for i:=0 to taT.FieldDefs.Count-1 do
  begin
    StrF:=taT.FieldDefs.Items[i].Name;
    StrT:=taT.FieldDefs.Items[i].FieldClass.ClassName;
    StrVal:=taT.FieldByName(StrF).AsString+';';
    StrWk:=StrWk+StrVal;
  end;
  Result:=StrWk;
end;

procedure TfmImportDK.prStart;
Var StrWk,NameF:String;
//    i:Integer;
    F: TextFile;
    DateB,DateE:TDateTime;
    sPost,sDep:String;
    bTransp:Boolean;
    par:Variant;
    DateSale:TDateTime;
    iDep:Integer;
    rSumP,rSum0,rSum10,rSum20,nds10,nds20:Real;
begin
  Memo1.Clear;
  WrMess('������.');

  bTransp:=True;

  DateB:=Trunc(DateEdit1.Date);
  DateE:=Trunc(DateEdit2.Date);

  StrWk:=CommonSet.FileExp;
  WrMess('  ������ ���� : '+StrWk);
  try
    Application.ProcessMessages;
    NameF:=StrWk;
    StrWk:=CommonSet.PathExport+NameF;  //CommonSet.PathExport+CommonSet.FileExp;
    AssignFile(F, StrWk);
    Rewrite(F);

    WrMess('  ��������� ������.');

    //������ ����� ������
    taDeparts.Active:=False;
    taDeparts.DatabaseName:=CommonSet.PathCryst;
    taDeparts.Active:=True;

    WrMess('');
    WrMess('    ��������� ��������� �� ������ � '+FormatDateTime('dd.mm.yyyy',DateB)+' �� '+FormatDateTime('dd.mm.yyyy',DateE));

    btVend.Active:=False;
    btVend.DatabaseName:=CommonSet.PathCryst;;
    btVend.Active:=True;

    btIndivid.Active:=False;
    btIndivid.DatabaseName:=CommonSet.PathCryst;
    btIndivid.Active:=True;



    btTTnIn.Active:=False;
    btTTnIn.DatabaseName:=CommonSet.PathCryst;
    btTTnIn.Active:=True;

{    taDeparts.First;
    while not taDeparts.Eof do
    begin
      taDeparts.Next;
    end;
}
    taDeparts.First;
    while not taDeparts.Eof do
    begin
      sDep:=taDepartsEnglishFullName.AsString;
      if sDep='' then sDep:=IntToStr(taDepartsID.AsInteger);

      btTTnIn.CancelRange;
      btTTnIn.SetRange([taDepartsID.AsInteger,DateB],[taDepartsID.AsInteger,DateE]);
      btTTnIn.First;

      while not btTTnIn.Eof do
      begin
        if btTTnInAcStatus.AsInteger=3 then //��������� ������������
        begin
          sPost:=';;';
          if btTTnIn.FieldByName('IndexPost').AsInteger=1 then
          begin //��
            if btVend.FindKey([btTTnIn.FieldByName('CodePost').AsInteger]) then
            begin
          //��������� � ��������� ����������
      //      WriteLn(F,WrRec(btVend));
              sPost:=btVendName.AsString+';'+btVendINN.AsString+';';
            end;
          end else
          begin //2 ��
            if btIndivid.FindKey([btTTnIn.FieldByName('CodePost').AsInteger]) then
            begin
      //      WriteLn(F,WrRec(btIndivid));
              sPost:=btIndividName.AsString+';'+btIndividINN.AsString+';';
            end;
          end;

          rSumP:=RoundV(btTTnInDolgPost.AsFloat);
          rSum0:=RoundV(btTTnInNotNDSTovar.AsFloat);
          rSum10:=RoundV(btTTnInOutNDS10.AsFloat);
          rSum20:=RoundV(btTTnInOutNDS20.AsFloat);
          nds10:=RoundV(btTTnInNDS10.AsFloat);
          nds20:=RoundV(btTTnInNDS20.AsFloat);
          if (rSumP<>(rSum0+rSum10+rSum20+nds10+nds20))or(rSum0<>0) then
          begin //����� �� ����
            if rSum0<0.2 then rSum0:=0;
            if rSum10>0 then
            begin
              nds10:=rSumP-(rSum0+rSum10+rSum20+nds20);
            end else //��� � ���10
            begin
              if rSum20>0 then
              begin
                nds20:=rSumP-(rSum0+rSum10+rSum20+nds10);
              end else
              begin
                rSum0:=rSumP;
                rSum10:=0;
                rSum20:=0;
                nds10:=0;
                nds20:=0;
              end;
            end;
          end;


          StrWk:='���;';
          StrWk:=StrWk+sDep+';';
          StrWk:=StrWk+FormatDateTime('dd/mm/yyyy',btTTnInDateInvoice.AsDateTime)+';';
          StrWk:=StrWk+btTTnInNumber.AsString+';';
          StrWk:=StrWk+sPost;
          StrWk:=StrWk+RV(rSumP+btTTnInSummaTara.AsFloat)+';';
          StrWk:=StrWk+RV(btTTnInSummaTara.AsFloat)+';';
          StrWk:=StrWk+RV(btTTnInAmortTara.AsFloat)+';';
          StrWk:=StrWk+RV(btTTnInTransport.AsFloat)+';';
          StrWk:=StrWk+RV(btTTnInSummaTovar.AsFloat)+';';
          StrWk:=StrWk+fts(btTTnInOthodiPoluch.AsFloat)+';';
          StrWk:=StrWk+RV(rSum0)+';';
          StrWk:=StrWk+RV(rSum10)+';';
          StrWk:=StrWk+RV(nds10)+';';
          StrWk:=StrWk+RV(rSum20)+';';
          StrWk:=StrWk+RV(nds20)+';';
          StrWk:=StrWk+RV(btTTnInNDSTransport.AsFloat)+';';
          StrWk:=StrWk+INtToStr(btTTnInID.AsInteger)+';';

          while pos('.',StrWk)>0 do StrWk[pos('.',StrWk)]:='/';
          while pos(',',StrWk)>0 do StrWk[pos(',',StrWk)]:='.';

          WriteLn(F,AnsiToOemConvert(StrWk));
        end
        else
        begin
          if CommonSet.StatusMess=1 then
          begin
            if btTTnInAcStatus.AsInteger=0 then //��������� ��������������
            begin
              WrMess('---!!! ����� � '+sDep+'. ��������� � '+btTTnInNumber.AsString+' �� '+FormatDateTime('dd/mm/yyyy',btTTnInDateInvoice.AsDateTime)+' - ��������������.');
              bTransp:=False;
            end;  
          end;
        end;

        btTtnIn.Next;
        delay(10);
      end;

      taDeparts.Next;
    end;


    btTTnIn.Active:=False;

    WrMess('    ��������� ��������� Ok.');

    WrMess('');
    WrMess('    ��������� ��������� �� ������ � '+FormatDateTime('dd.mm.yyyy',DateB)+' �� '+FormatDateTime('dd.mm.yyyy',DateE));

    btTTnOut.Active:=False;
    btTTnOut.DatabaseName:=CommonSet.PathCryst;
    btTTnOut.Active:=True;

    taDeparts.First;
    while not taDeparts.Eof do
    begin
      sDep:=taDepartsEnglishFullName.AsString;
      if sDep='' then sDep:=IntToStr(taDepartsID.AsInteger);

      btTTnOut.CancelRange;
      btTTnOut.SetRange([taDepartsID.AsInteger,DateB],[taDepartsID.AsInteger,DateE]);
      btTTnOut.First;

      while not btTTnOut.Eof do
      begin
        if btTTnOutAcStatus.AsInteger=3 then //��������� ������������
        begin
          sPost:=';;';
          if btTTnOut.FieldByName('IndexPoluch').AsInteger=1 then
          begin //��
            if btVend.FindKey([btTTnOut.FieldByName('CodePoluch').AsInteger]) then
            begin
          //��������� � ��������� ����������
      //      WriteLn(F,WrRec(btVend));
              sPost:=btVendName.AsString+';'+btVendINN.AsString+';';
            end;
          end else
          begin //2 ��
            if btIndivid.FindKey([btTTnOut.FieldByName('CodePoluch').AsInteger]) then
            begin
      //      WriteLn(F,WrRec(btIndivid));
              sPost:=btIndividName.AsString+';'+btIndividINN.AsString+';';
            end;
          end;

          rSumP:=RoundV(btTTnOutSummaTovar.AsFloat);
          rSum0:=RoundV(btTTnOutOutLgtNDS.AsFloat);
          rSum10:=RoundV(btTTnOutOutNDS10.AsFloat);
          rSum20:=RoundV(btTTnOutOutNDS20.AsFloat);
          nds10:=RoundV(btTTnOutNDS10.AsFloat);
          nds20:=RoundV(btTTnOutNDS20.AsFloat);
          if rSumP<>(rSum0+rSum10+rSum20+nds10+nds20) then
          begin //����� �� ����
            if rSum0<0.2 then rSum0:=0;
            if rSum10>0 then
            begin
              nds10:=rSumP-(rSum0+rSum10+rSum20+nds20);
            end else //��� � ���10
            begin
              if rSum20>0 then
              begin
                nds20:=rSumP-(rSum0+rSum10+rSum20+nds10);
              end else
              begin
                rSum0:=rSumP;
                rSum10:=0;
                rSum20:=0;
                nds10:=0;
                nds20:=0;
              end;
            end;
          end;

          StrWk:='���;';
          StrWk:=StrWk+sDep+';';
          StrWk:=StrWk+FormatDateTime('dd/mm/yyyy',btTTnOutDateInvoice.AsDateTime)+';';
          StrWk:=StrWk+btTTnOutNumber.AsString+';';
          StrWk:=StrWk+sPost;
          StrWk:=StrWk+RV(btTTnOutSummaTovarSpis.AsFloat)+';';
          StrWk:=StrWk+RV(btTTnOutSummaTaraSpis.AsFloat)+';';
          StrWk:=StrWk+RV(rSumP)+';';
          StrWk:=StrWk+RV(rSum0)+';';
          StrWk:=StrWk+RV(rSum10)+';';
          StrWk:=StrWk+RV(nds10)+';';
          StrWk:=StrWk+RV(rSum20)+';';
          StrWk:=StrWk+RV(nds20)+';';
          StrWk:=StrWk+IntToStr(btTTnOutProvodType.AsInteger)+';';
          StrWk:=StrWk+INtToStr(btTTnOutID.AsInteger)+';';

          while pos('.',StrWk)>0 do StrWk[pos('.',StrWk)]:='/';
          while pos(',',StrWk)>0 do StrWk[pos(',',StrWk)]:='.';

          WriteLn(F,AnsiToOemConvert(StrWk));
        end
        else
        begin
          if CommonSet.StatusMess=1 then
          begin
            WrMess('---!!! ����� � '+sDep+'. ��������� � '+btTTnOutNumber.AsString+' �� '+FormatDateTime('dd/mm/yyyy',btTTnOutDateInvoice.AsDateTime)+' - ��������������.');
            bTransp:=False;
          end;  
        end;

        btTTnOut.Next;
        delay(10);
      end;


      taDeparts.Next;
    end;

    btTTnOut.Active:=False;

    WrMess('    ��������� ��������� Ok.');


    WrMess('');
    WrMess('    ���������� ��������� �� ������ � '+FormatDateTime('dd.mm.yyyy',DateB)+' �� '+FormatDateTime('dd.mm.yyyy',DateE));

    btTTNSelf.Active:=False;
    btTTNSelf.DatabaseName:=CommonSet.PathCryst;
    btTTNSelf.Active:=True;

    taDeparts.First;
    while not taDeparts.Eof do
    begin

      sDep:=taDepartsEnglishFullName.AsString;
      if sDep='' then sDep:=IntToStr(taDepartsID.AsInteger);

      btTTNSelf.CancelRange;
      btTTNSelf.SetRange([taDepartsID.AsInteger,DateB],[taDepartsID.AsInteger,DateE]);
      btTTNSelf.First;

      while not btTTNSelf.Eof do
      begin
        if (btTTNSelfAcStatusP.AsInteger=3)and(btTTNSelfAcStatusR.AsInteger=3) then //��������� ������������
        begin
          sPost:=';;';

          StrWk:='���;'+FormatDateTime('dd/mm/yyyy',btTTNSelfDateInvoice.AsDateTime)+';'+btTTNSelfNumber.AsString;
          StrWk:=StrWk+';'+sDep+';'+IntToStr(btTTNSelfFlowDepart.AsInteger);
          StrWk:=StrWk+';'+rv(btTTNSelfSummaTovarSpis.AsFloat)+';'+rv(btTTNSelfSummaTovarMove.AsFloat);
          StrWk:=StrWk+';'+rv(btTTNSelfSummaTara.AsFloat)+';'+IntToStr(btTTNSelfProvodTypeR.asInteger)+';';

          while pos('.',StrWk)>0 do StrWk[pos('.',StrWk)]:='/';
          while pos(',',StrWk)>0 do StrWk[pos(',',StrWk)]:='.';

          WriteLn(F,AnsiToOemConvert(StrWk));
        end
        else
        begin
          if CommonSet.StatusMess=1 then
          begin
            WrMess('---!!! ����� � '+sDep+'. ��������� � '+btTTNSelfNumber.AsString+' �� '+FormatDateTime('dd/mm/yyyy',btTTNSelfDateInvoice.AsDateTime)+' - ��������������.');
            bTransp:=False;
          end;  
        end;

        btTTNSelf.Next;
        delay(10);
      end;

      taDeparts.Next;
    end;

    btTTNSelf.Active:=False;

    WrMess('    ���������� ��������� Ok.');

    WrMess('');
    WrMess('    ���� ���������� �� ������ � '+FormatDateTime('dd.mm.yyyy',DateB)+' �� '+FormatDateTime('dd.mm.yyyy',DateE));

    tbTTNOvr.Active:=False;
    tbTTNOvr.DatabaseName:=CommonSet.PathCryst;
    tbTTNOvr.Active:=True;


    taDeparts.First;
    while not taDeparts.Eof do
    begin
      sDep:=taDepartsEnglishFullName.AsString;
      if sDep='' then sDep:=IntToStr(taDepartsID.AsInteger);

      tbTTNOvr.CancelRange;
      tbTTNOvr.SetRange([taDepartsID.AsInteger,DateB],[taDepartsID.AsInteger,DateE]);
      tbTTNOvr.First;

      while not tbTTNOvr.Eof do
      begin

        if tbTTNOvrAcStatus.AsInteger=3 then //��������� ������������
        begin

          StrWk:='���;'+FormatDateTime('dd/mm/yyyy',tbTTNOvrDateAct.AsDateTime)+';'+tbTTNOvrNumber.AsString;
          StrWk:=StrWk+';'+sDep;
          StrWk:=StrWk+';'+rv(tbTTNOvrMatOldSum.AsFloat)+';'+rv(tbTTNOvrMatNewSum.AsFloat)+';';

          while pos('.',StrWk)>0 do StrWk[pos('.',StrWk)]:='/';
          while pos(',',StrWk)>0 do StrWk[pos(',',StrWk)]:='.';

          WriteLn(F,AnsiToOemConvert(StrWk));
        end
        else
        begin
          if CommonSet.StatusMess=1 then
          begin
            WrMess('---!!! ����� � '+sDep+'. ��� � '+tbTTNOvrNumber.AsString+' �� '+FormatDateTime('dd/mm/yyyy',tbTTNOvrDateAct.AsDateTime)+' - �������������.');
            bTransp:=False;
          end;  
        end;

        tbTTNOvr.Next;
        delay(10);
      end;

      taDeparts.Next;
    end;

    tbTTNOvr.Active:=False;
    WrMess('    ���� ���������� Ok.');


    WrMess('');
    WrMess('    ���������� �� ������ � '+FormatDateTime('dd.mm.yyyy',DateB)+' �� '+FormatDateTime('dd.mm.yyyy',DateE));

    btSale.Active:=False;
    btSale.DatabaseName:=CommonSet.PathCryst;
    btSale.Active:=True;

    btSale.CancelRange;
    btSale.SetRange([DateB],[DateE]);
    btSale.First;

    btSaleBn.Active:=False;
    btSaleBn.DatabaseName:=CommonSet.PathCryst;
    btSaleBn.Active:=True;

    btSaleBn.CancelRange;
    btSaleBn.SetRange([DateB],[DateE]);
    btSaleBn.First;


{   taZRep.Active:=False;
    taZRep.DatabaseName:=CommonSet.PathCryst;
    taZRep.Active:=True;

    taZRep.CancelRange;
    taZRep.SetRange([DateB],[DateE]);
    taZRep.First;
    }

    taSale.Active:=False;
    taSale.CreateDataSet;

    par := VarArrayCreate([0,2], varInteger);

    {
    taZRep.First;
    while not taZRep.Eof do
    begin
      taSale.Append;
      taSaleDateSale.AsInteger:=Trunc(taZRepZDate.AsDateTime);
      taSaleCassaNumber.AsInteger:=taZRepCashNumber.AsInteger;
      taSaleOtdel.AsInteger:=4;
      taSaleSumma.AsCurrency:=btSaleSumma.AsCurrency;
      taSaleSummaCor.AsCurrency:=btSaleSummaCor.AsCurrency;
      taSaleSNDS10.AsCurrency:=btSaleSummaNDSx10.AsCurrency;
      taSaleSNDS20.AsCurrency:=btSaleSummaNDSx20.AsCurrency;
      taSaleCorNDS10.AsCurrency:=btSaleCorNDSx10.AsCurrency;
      taSaleCorNDS20.AsCurrency:=btSaleCorNDSx20.AsCurrency;
      taSaleBnSumma.AsCurrency:=0;
      taSaleBnSummaCor.AsCurrency:=0;
      taSaleSBNNDS10.AsCurrency:=0;
      taSaleSBNNDS20.AsCurrency:=0;
      taSaleBNCorNDS10.AsCurrency:=0;
      taSaleBNCorNDS20.AsCurrency:=0;

      taSale.Post;

      taZRep.Next;
    end;

    }

    while not btSale.Eof do
    begin
      iDep:=0;
      sDep:=btSaleOtdel.AsString;
      if taDeparts.Locate('ID',btSaleOtdel.AsInteger,[]) then
      begin
        iDep:=StrToIntDef(taDepartsEnglishFullName.AsString,0);
        if iDep>0 then sDep:=IntToStr(iDep);
      end;
      if iDep=0 then iDep:=btSaleOtdel.AsInteger;

      par[0]:=Trunc(btSaleDateSale.AsDateTime);
      par[1]:=btSaleCassaNumber.AsInteger;
      par[2]:=iDep;

      if taSale.Locate('DateSale;CassaNumber;Otdel',par,[]) then
      begin
        taSale.Edit;
        taSaleSumma.AsCurrency:=taSaleSumma.AsCurrency+btSaleSumma.AsCurrency;
        taSaleSummaCor.AsCurrency:=taSaleSummaCor.AsCurrency+btSaleSummaCor.AsCurrency;
        taSaleSNDS10.AsCurrency:=taSaleSNDS10.AsCurrency+btSaleSummaNDSx10.AsCurrency;
        taSaleSNDS20.AsCurrency:=taSaleSNDS20.AsCurrency+btSaleSummaNDSx20.AsCurrency;
        taSaleCorNDS10.AsCurrency:=taSaleCorNDS10.AsCurrency+btSaleCorNDSx10.AsCurrency;
        taSaleCorNDS20.AsCurrency:=taSaleCorNDS20.AsCurrency+btSaleCorNDSx20.AsCurrency;
        taSale.Post;
      end else
      begin
        taSale.Append;
        taSaleDateSale.AsInteger:=Trunc(btSaleDateSale.AsDateTime);
        taSaleCassaNumber.AsInteger:=btSaleCassaNumber.AsInteger;
        taSaleOtdel.AsInteger:=iDep;
        taSaleSumma.AsCurrency:=btSaleSumma.AsCurrency;
        taSaleSummaCor.AsCurrency:=btSaleSummaCor.AsCurrency;
        taSaleSNDS10.AsCurrency:=btSaleSummaNDSx10.AsCurrency;
        taSaleSNDS20.AsCurrency:=btSaleSummaNDSx20.AsCurrency;
        taSaleCorNDS10.AsCurrency:=btSaleCorNDSx10.AsCurrency;
        taSaleCorNDS20.AsCurrency:=btSaleCorNDSx20.AsCurrency;
        taSaleBnSumma.AsCurrency:=0;
        taSaleBnSummaCor.AsCurrency:=0;
        taSaleSBNNDS10.AsCurrency:=0;
        taSaleSBNNDS20.AsCurrency:=0;
        taSaleBNCorNDS10.AsCurrency:=0;
        taSaleBNCorNDS20.AsCurrency:=0;

        taSale.Post;
      end;

      btSale.Next;
      delay(10);
    end;

    btSaleBn.First;
    while not btSaleBn.Eof do
    begin

      iDep:=0;
      sDep:=btSaleOtdel.AsString;
      if taDeparts.Locate('ID',btSaleBnOtdel.AsInteger,[]) then
      begin
        iDep:=StrToIntDef(taDepartsEnglishFullName.AsString,0);
        if iDep>0 then sDep:=IntToStr(iDep);
      end;
      if iDep=0 then iDep:=btSaleBnOtdel.AsInteger;

      par[0]:=Trunc(btSaleBnDateSale.AsDateTime);
      par[1]:=btSaleBnCassaNumber.AsInteger;
      par[2]:=iDep;


      if taSale.Locate('DateSale;CassaNumber;Otdel',par,[]) then
      begin
        taSale.Edit;
        taSaleBnSumma.AsCurrency:=taSaleBnSumma.AsCurrency+btSaleBnSumma.AsCurrency;
        taSaleBnSummaCor.AsCurrency:=taSaleBnSummaCor.AsCurrency+btSaleBnSummaCor.AsCurrency;
        taSaleSBNNDS10.AsCurrency:=taSaleSBNNDS10.AsCurrency+btSaleBnSummaNDSx10.AsCurrency;
        taSaleSBNNDS20.AsCurrency:=taSaleSBNNDS20.AsCurrency+btSaleBnSummaNDSx20.AsCurrency;
        taSaleBNCorNDS10.AsCurrency:=taSaleBNCorNDS10.AsCurrency+btSaleBnCorNDSx10.AsCurrency;
        taSaleBNCorNDS20.AsCurrency:=taSaleBNCorNDS20.AsCurrency+btSaleBnCorNDSx20.AsCurrency;
        taSale.Post;
      end else
      begin
        taSale.Append;
        taSaleDateSale.AsInteger:=Trunc(btSaleBnDateSale.AsDateTime);
        taSaleCassaNumber.AsInteger:=btSaleBnCassaNumber.AsInteger;
        taSaleOtdel.AsInteger:=iDep;
        taSaleSumma.AsCurrency:=0;
        taSaleSummaCor.AsCurrency:=0;
        taSaleSNDS10.AsCurrency:=0;
        taSaleSNDS20.AsCurrency:=0;
        taSaleCorNDS10.AsCurrency:=0;
        taSaleCorNDS20.AsCurrency:=0;
        taSaleBnSumma.AsCurrency:=taSaleBnSumma.AsCurrency+btSaleBnSumma.AsCurrency;
        taSaleBnSummaCor.AsCurrency:=taSaleBnSummaCor.AsCurrency+btSaleBnSummaCor.AsCurrency;
        taSaleSBNNDS10.AsCurrency:=taSaleSBNNDS10.AsCurrency+btSaleBnSummaNDSx10.AsCurrency;
        taSaleSBNNDS20.AsCurrency:=taSaleSBNNDS20.AsCurrency+btSaleBnSummaNDSx20.AsCurrency;
        taSaleBNCorNDS10.AsCurrency:=taSaleBNCorNDS10.AsCurrency+btSaleBnCorNDSx10.AsCurrency;
        taSaleBNCorNDS20.AsCurrency:=taSaleBNCorNDS20.AsCurrency+btSaleBnCorNDSx20.AsCurrency;

        taSale.Post;
      end;

      btSaleBn.Next;
    end;

    taSale.First;
    while not taSale.Eof do
    begin

      DateSale:=taSaleDateSale.AsInteger;

      StrWk:='���;'+FormatDateTime('dd/mm/yyyy',DateSale)+';';
      StrWk:=StrWk+CommonSet.CashPref+IntToStr(taSaleCassaNumber.AsInteger)+';';
      StrWk:=StrWk+IntToStr(taSaleOtdel.AsInteger)+';'+rv(taSaleSumma.AsFloat)+';'+rv(taSaleSummaCor.AsFloat)+';';
      StrWk:=StrWk+rv(taSaleBnSumma.AsFloat)+';'+rv(taSaleBnSummaCor.AsFloat)+';';
      StrWk:=StrWk+rv(taSaleSNDS10.AsFloat)+';'+rv(taSaleCorNDS10.AsFloat)+';';
      StrWk:=StrWk+rv(taSaleSNDS20.AsFloat)+';'+rv(taSaleCorNDS20.AsFloat)+';';
      StrWk:=StrWk+rv(taSaleSBNNDS10.AsFloat)+';'+rv(taSaleBnCorNDS10.AsFloat)+';';
      StrWk:=StrWk+rv(taSaleSBNNDS20.AsFloat)+';'+rv(taSaleBnCorNDS20.AsFloat)+';';

      while pos('.',StrWk)>0 do StrWk[pos('.',StrWk)]:='/';
      while pos(',',StrWk)>0 do StrWk[pos(',',StrWk)]:='.';

      WriteLn(F,AnsiToOemConvert(StrWk));

      taSale.Next;
    end;

    taSale.Active:=False;
    btSale.Active:=False;
    btSaleBn.Active:=False;
//    taZRep.Active:=False;

    WrMess('    ���������� Ok.');

    btVend.Active:=False;
    btIndivid.Active:=False;
    taDeparts.Active:=False;

    WrMess('');
    WrMess('  ��������� ���������.');

    finally
    CloseFile(F);
  end;
  if bTransp then prTr(NameF) else
  begin
    fmAttention.ShowModal;
  end;
  WrMess('���������.');
end;

Procedure WrMess(S:String);
Var StrWk:String;
begin
  try
    if fmMainFrom.Memo1.Lines.Count > 500 then fmMainFrom.Memo1.Clear;
    delay(10);
    if S>'' then StrWk:=FormatDateTime('dd.mm hh:nn:ss:zzz',now)+'  '+S
    else StrWk:=S;
    WriteHistory(StrWk);
    fmMainFrom.Memo1.Lines.Add(StrWk);
  except
    try
      WriteHistory(StrWk);
      StrWk:=FormatDateTime('dd.mm hh:nn:ss:zzz',now)+'  ���� ������ �������.';
      WriteHistory(StrWk);
    except
    end;
  end;
end;



procedure TfmImportDK.cxButton2Click(Sender: TObject);
begin
  CommonSet.TrStr:='';
  if cxCheckBox1.Checked then CommonSet.TrStr:=CommonSet.TrStr+'1' else CommonSet.TrStr:=CommonSet.TrStr+'0';
  if cxCheckBox2.Checked then CommonSet.TrStr:=CommonSet.TrStr+'1' else CommonSet.TrStr:=CommonSet.TrStr+'0';
  if cxCheckBox3.Checked then CommonSet.TrStr:=CommonSet.TrStr+'1' else CommonSet.TrStr:=CommonSet.TrStr+'0';
  if cxCheckBox4.Checked then CommonSet.TrStr:=CommonSet.TrStr+'1' else CommonSet.TrStr:=CommonSet.TrStr+'0';
  if cxCheckBox5.Checked then CommonSet.TrStr:=CommonSet.TrStr+'1' else CommonSet.TrStr:=CommonSet.TrStr+'0';
  if cxCheckBox6.Checked then CommonSet.TrStr:=CommonSet.TrStr+'1' else CommonSet.TrStr:=CommonSet.TrStr+'0';
  if cxCheckBox7.Checked then CommonSet.TrStr:=CommonSet.TrStr+'1' else CommonSet.TrStr:=CommonSet.TrStr+'0';
  if cxCheckBox8.Checked then CommonSet.TrStr:=CommonSet.TrStr+'1' else CommonSet.TrStr:=CommonSet.TrStr+'0';
  if cxCheckBox9.Checked then CommonSet.TrStr:=CommonSet.TrStr+'1' else CommonSet.TrStr:=CommonSet.TrStr+'0';

  WriteIni;

  Close;
end;

procedure TfmImportDK.FormCreate(Sender: TObject);
begin
  CurDir := ExtractFilePath(ParamStr(0));
  ReadIni;
  Memo1.Clear;
  iCountSec:=0;
  DateEdit1.Date:=Date-CommonSet.TrDocDay;
  DateEdit2.Date:=Date;
  Label3.Caption:='';
//  if CommonSet.TrStr[1]='1' then cxCheckBox1.Checked:=True else cxCheckBox1.Checked:=False;
//  if CommonSet.TrStr[2]='1' then cxCheckBox2.Checked:=True else cxCheckBox2.Checked:=False;
//  if CommonSet.TrStr[3]='1' then cxCheckBox3.Checked:=True else cxCheckBox3.Checked:=False;
//  if CommonSet.TrStr[4]='1' then cxCheckBox4.Checked:=True else cxCheckBox4.Checked:=False;
//  if CommonSet.TrStr[5]='1' then cxCheckBox5.Checked:=True else cxCheckBox5.Checked:=False;
//  if CommonSet.TrStr[6]='1' then cxCheckBox6.Checked:=True else cxCheckBox6.Checked:=False;
//  if CommonSet.TrStr[7]='1' then cxCheckBox7.Checked:=True else cxCheckBox7.Checked:=False;
//  if CommonSet.TrStr[8]='1' then cxCheckBox8.Checked:=True else cxCheckBox8.Checked:=False;
//  if CommonSet.TrStr[9]='1' then cxCheckBox9.Checked:=True else cxCheckBox9.Checked:=False;

  if (CommonSet.TrDocAutoStart='Yes')or(CommonSet.TrDocAutoStart='YES')
  or (CommonSet.TrDocAutoStart='Y') then Timer1.Enabled:=True;
end;

procedure TfmImportDK.Timer1Timer(Sender: TObject);
begin
  //�� ��������� �� 2-� �������
  Timer1.Enabled:=False;

{  if TestExport then
  begin
    prTransp;  //���� �����-�� �����
  end;}
  inc(iCountSec);
  if iCountSec>=CommonSet.PeriodPing then //������ �� �������� - ������ ������ ��� (��� ���������� �������� ������ 20 ���)
  begin
//    prTranspImp;  //���� �����-�� �����
//    prImpToCrystal;
    prStart;
//    prTrFiles;
    
    iCountSec:=0;

    if CommonSet.CountStart>0 then dec(CommonSet.CountStart);
    if CommonSet.CountStart=0 then Close;
  end;
  Timer1.Enabled:=True;
end;

procedure TfmImportDK.cxButton1Click(Sender: TObject);
begin
  prStart;
//  prTrFiles;
end;

procedure TfmImportDK.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  //��� �������� ������� ��������� ���������� ����������
//  ShellExecute(handle,'open','proga.exe',parametr,'', SW_HIDE);
  if FileExists(CurDir+'PowerOff.exe') then
  begin
    ShellExecute(handle, 'open', 'PowerOff.exe','','', SW_SHOWNORMAL);
  end;
end;

procedure TfmImportDK.cxButton3Click(Sender: TObject);
var
  FList,Dest,PList,DList: TStrings;
  i,k,m:INteger;
  F:TextFile;
  bExit:Boolean;
  StrWk,StrWk1:String;
  BadPath:Boolean;

  Label ExitProc;
begin
  Memo1.Clear;
  Memo1.Lines.Add('  �������� �������� ������ �� FTP �������� ��������.');
  bExit:=False;

  //�������� ������ ���� ����� ��� ��������
  FList := TStringList.Create; //� ������ ��� ��������
  DList := TStringList.Create; //��� ����� ��� ����������

  //�������� ����������� �� ���
  Dest := TStringList.Create;

  //�������� ����� �������� �� ��� �� ���
  pList := TStringList.Create;
  StrWk:=CommonSet.FtpExport;
  While StrWk>'' do
  begin
    if Pos(r,strwk)>0 then
    begin
      StrWk1:=Copy(StrWk,1,Pos(r,strwk)-1);
      Delete(StrWk,1,Pos(r,strwk));
    end else
    begin
      StrWk1:=StrWk;
      StrWk:='';
    end;
    PList.Add(StrWk1);
  end;

  try
    //�������� ��� ��� ������
    try
      IDFtp1.DisConnect;
      IdFtp1.Host:=CommonSet.HostIp;
      IdFtp1.Username:=CommonSet.HostUser;
      IdFtp1.Password:=CommonSet.HostPassw;
      IdFTP1.Connect;
      IDFtp1.List( Dest );
    except
      bExit:=True;
    end;

    if bExit then goto ExitProc;

    StrWk:=CommonSet.PathExport+CommonSet.FileExp;

    if FileExists(StrWk) then
    begin
      FList.Add(StrWk); // ���������� � ������ �������� ����� ��� �������� � ������
      While Pos('\',StrWk)>0 do Delete(StrWk,1,Pos('\',StrWk));
      if CommonSet.Compress=1 then StrWk:=StrWk+TrExt;
      DList.Add(StrWk); //���������� �������� ���  �����
    end;

    //������ ������ ������������ -
    //1. �������� - 100 ��
    //2. �������� ����� �� ������ ������ - �������� �������� �� 10 �.
    //3. ����������
    //4. ��������
    //5. ��� ������ ���������� � �����
    //6. ���� ���� ������  - ����� ������� ���������� ����� ���� �� �����.

    //1.
    delay(100);
    For i:=0 to FList.Count-1 do
    begin
      Memo1.Lines.Add(' ���� - '+FList.Strings[i]);
      {$I-}
      Assignfile(f,FList.Strings[i]);
      FileMode := 2;  {Set file access to read/Write }
      Reset(F);
      CloseFile(F);
      {$I+}
      if IOResult = 0 then
      begin
        Memo1.Lines.Add(' ������ - ��.');
        try
          if CommonSet.Compress=1 then
          begin
            fs := TECLFileStream.Create(CurDir+DList.Strings[i],fmCreate,odInac,levelValue[9]);
            fs.LoadFromFile(FList.Strings[i]);
            fs.Free;
            Memo1.Lines.Add(' ������ - ��.');
          end;

          For k:=0 to PList.Count-1 do
          begin
            StrWk:=PList.Strings[k];
            m:=0;
            BadPath:=False;
            While StrWk>'' do
            begin
              if Pos('\',strwk)>0 then
              begin
                StrWk1:=Copy(StrWk,1,Pos('\',strwk)-1);
                Delete(StrWk,1,Pos('\',strwk));
              end else
              begin
                StrWk1:=StrWk;
                StrWk:='';
              end;
              if strwk1>'' then
              begin
                Memo1.Lines.Add('    ���� ��� �������� - '+StrWk1);
                try
                  IdFTP1.ChangeDir(StrWk1);
                  inc(m);
                  Memo1.Lines.Add('    ���� ��.'+StrWk1);
                except
                  Memo1.Lines.Add('    ������ ��� �������� - '+StrWk1);
                  BadPath:=True;
                end;
              end;
            end;

            if not BadPath then //���� ���������� ����.
            begin
              try
                Memo1.Lines.Add('      �������� ����� - '+DList.Strings[i]);
                if CommonSet.Compress=1 then
                begin
                  IDFtp1.Put(DList.Strings[i], DList.Strings[i], False );
                end
                else
                begin
                  StrWk:=FList.Strings[i];
                  While Pos('\',StrWk)>0 do Delete(StrWk,1,Pos('\',StrWk));
                  IDFtp1.Put(FList.Strings[i], StrWk, False );
                end;
//                Memo1.Lines.Add('      �������� Ok.');
              except
                Memo1.Lines.Add('      ������ ��� �������� ����� - '+FList.Strings[i]+TrExt);
              end;
            end;

            While m>0 do
            begin
              IdFTP1.ChangeDir('..');//�� ������� ���������� - ��  ������� ���������.
              m:=m-1;
            end;
          end;

          //������� ������
          if CommonSet.Compress=1 then
          begin
            Assignfile(f,CurDir+DList.Strings[i]);
            erase(f);
          end;

{     //���������� ������

           if TrGood then //���������� ���� � �����
            MoveFile(CurDir+FList.Strings[i], CommonSet.PathArh+FList.Strings[i]);
}
        except
//          Memo1.Lines.Add(' ������ ��� ������.');
        end;
      end
      else
      begin
        Memo1.Lines.Add(' � ������� ��������.');
      end;
      delay(100);
    end;
    ExitProc:
  finally
    IDFtp1.DisConnect;
    FList.Free;
    DList.Free;
    Dest.Free;
    PList.Free;
    Memo1.Lines.Add('  �������� �������� ���������.');
  end;
end;


end.
