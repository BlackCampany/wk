unit MoveSel;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, Placemnt, ExtCtrls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  SpeedBar, ActnList, XPStyleActnCtrls, ActnMan, cxTextEdit,
  cxImageComboBox, cxCurrencyEdit, cxContainer, cxMemo;

type
  TfmMoveSel = class(TForm)
    StatusBar1: TStatusBar;
    Timer1: TTimer;
    FormPlacement1: TFormPlacement;
    ViewMoveSel: TcxGridDBTableView;
    LevelMoveSel: TcxGridLevel;
    GridMoveSel: TcxGrid;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    amMoveSel: TActionManager;
    acExit: TAction;
    ViewMoveSelID: TcxGridDBColumn;
    ViewMoveSelPARENT: TcxGridDBColumn;
    ViewMoveSelNAME: TcxGridDBColumn;
    ViewMoveSelIMESSURE: TcxGridDBColumn;
    ViewMoveSelMINREST: TcxGridDBColumn;
    ViewMoveSelTCARD: TcxGridDBColumn;
    ViewMoveSelNAMECL: TcxGridDBColumn;
    ViewMoveSelNAMEMESSURE: TcxGridDBColumn;
    ViewMoveSelKOEF: TcxGridDBColumn;
    ViewMoveSelPOSTIN: TcxGridDBColumn;
    ViewMoveSelPOSTOUT: TcxGridDBColumn;
    ViewMoveSelVNIN: TcxGridDBColumn;
    ViewMoveSelVNOUT: TcxGridDBColumn;
    ViewMoveSelINV: TcxGridDBColumn;
    ViewMoveSelQREAL: TcxGridDBColumn;
    ViewMoveSelItog: TcxGridDBColumn;
    Panel1: TPanel;
    acPerSkl: TAction;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    acExportEx: TAction;
    LevelRemn: TcxGridLevel;
    ViewRemn: TcxGridDBTableView;
    ViewRemnID: TcxGridDBColumn;
    ViewRemnPARENT: TcxGridDBColumn;
    ViewRemnNAME: TcxGridDBColumn;
    ViewRemnIMESSURE: TcxGridDBColumn;
    ViewRemnMINREST: TcxGridDBColumn;
    ViewRemnTCARD: TcxGridDBColumn;
    ViewRemnNAMECL: TcxGridDBColumn;
    ViewRemnNAMEMESSURE: TcxGridDBColumn;
    ViewRemnKOEF: TcxGridDBColumn;
    ViewRemnREMN: TcxGridDBColumn;
    Memo1: TcxMemo;
    procedure FormCreate(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Timer1Timer(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure acPerSklExecute(Sender: TObject);
    procedure acExportExExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ViewRemnDblClick(Sender: TObject);
    procedure ViewMoveSelDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmMoveSel: TfmMoveSel;
  bClearMoveSel: Boolean = False;

implementation

uses Un1, dmOffice, SelPerSkl, CardsMove;

{$R *.dfm}

procedure TfmMoveSel.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  Timer1.Enabled:=True;
  GridMoveSel.Align:=AlClient;
  ViewMoveSel.RestoreFromIniFile(CurDir+GridIni);
  ViewRemn.RestoreFromIniFile(CurDir+GridIni);
end;

procedure TfmMoveSel.SpeedItem1Click(Sender: TObject);
begin
  close;
end;

procedure TfmMoveSel.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewMoveSel.StoreToIniFile(CurDir+GridIni,False);
  ViewRemn.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmMoveSel.Timer1Timer(Sender: TObject);
begin
  if bClearMoveSel=True then begin StatusBar1.Panels[0].Text:=''; bClearMoveSel:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearMoveSel:=True;
end;

procedure TfmMoveSel.acExitExecute(Sender: TObject);
begin
  close;
end;

procedure TfmMoveSel.acPerSklExecute(Sender: TObject);
Var IdPar,iDate:INteger;
begin
  Memo1.Clear;
  Memo1.Lines.Add('����� , ���� ������������.');
  if LevelMoveSel.Visible then
  begin
    //�������� �� ������ �� ������
    fmSelPerSkl:=tfmSelPerSkl.Create(Application);
    with fmSelPerSkl do
    begin
      cxDateEdit1.Date:=CommonSet.DateFrom;
      quMHAll1.Active:=False;
      quMHAll1.Active:=True;
      quMHAll1.First;
      cxLookupComboBox1.EditValue:=CommonSet.IdStore;
    end;
    fmSelPerSkl.ShowModal;
    if fmSelPerSkl.ModalResult=mrOk then
    begin
      Cursor:=crHourGlass; Delay(10);
      CommonSet.DateFrom:=Trunc(fmSelPerSkl.cxDateEdit1.Date);
      CommonSet.DateTo:=Trunc(fmSelPerSkl.cxDateEdit2.Date)+1;
      CommonSet.IdStore:=fmSelPerSkl.cxLookupComboBox1.EditValue;
      CommonSet.NameStore:=fmSelPerSkl.cxLookupComboBox1.Text;

      if CommonSet.DateTo>=iMaxDate then fmMoveSel.Caption:='�������� �� �� '+CommonSet.NameStore+' � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
      else fmMoveSel.Caption:='�������� �� �� '+CommonSet.NameStore+' �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);
      fmSelPerSkl.quMHAll1.Active:=False;
      fmSelPerSkl.Release;
      with dmO do
      begin
        idPar:=GetId('Pars');
        taParams.Active:=False;
        taParams.Active:=True;
        taParams.Append;
        taParamsID.AsInteger:=idPar;
        taParamsIDATEB.AsInteger:=Trunc(CommonSet.DateFrom);
        taParamsIDATEE.AsInteger:=Trunc(CommonSet.DateTo-1);
        taParamsIDSTORE.AsInteger:=CommonSet.IdStore;
        taParams.Post;
        taParams.Active:=False;

        ViewMovesel.BeginUpdate;
        quMoveSel.Active:=False;
        quMoveSel.Active:=True;
        ViewMovesel.EndUpdate;
      end;
    end else
    begin
      fmSelPerSkl.quMHAll1.Active:=False;
      fmSelPerSkl.Release;
    end;
  end;
  if Levelremn.Visible then
  begin
    fmSelPerSkl:=tfmSelPerSkl.Create(Application);
    with fmSelPerSkl do
    begin
      if CommonSet.DateTo>=iMaxDate then CommonSet.DateTo:=Date;
      cxDateEdit1.Date:=CommonSet.DateTo;
      quMHAll1.Active:=False;
      quMHAll1.Active:=True;
      quMHAll1.First;
      cxLookupComboBox1.EditValue:=quMHAll1ID.AsInteger;
      cxCheckBox1.Visible:=False;
      Label1.Caption:='�� �����';
    end;
    fmSelPerSkl.ShowModal;
    if fmSelPerSkl.ModalResult=mrOk then
    begin
//    CommonSet.DateFrom:=Trunc(fmSelPerSkl.cxDateEdit1.Date);
      CommonSet.DateTo:=Trunc(fmSelPerSkl.cxDateEdit1.Date)+1;
      Cursor:=crHourGlass; Delay(10);
      iDate:=Trunc(fmSelPerSkl.cxDateEdit1.Date);
      CommonSet.IdStore:=fmSelPerSkl.cxLookupComboBox1.EditValue;
      CommonSet.NameStore:=fmSelPerSkl.cxLookupComboBox1.Text;

      fmMoveSel.Caption:='������� �� �� '+CommonSet.NameStore+' �� ����� '+FormatDateTiMe('dd.mm.yyyy',iDate);
      fmSelPerSkl.quMHAll1.Active:=False;
      fmSelPerSkl.Release;
      with dmO do
      begin
        idPar:=GetId('Pars');
        taParams.Active:=False;
        taParams.Active:=True;
        taParams.Append;
        taParamsID.AsInteger:=idPar;
        taParamsIDATEB.AsInteger:=iDate;
        taParamsIDATEE.AsInteger:=iDate;
        taParamsIDSTORE.AsInteger:=CommonSet.IdStore;
        taParams.Post;
        taParams.Active:=False;

        quRemnDate.Active:=False;
        quRemnDate.Active:=True;

      end;
    end else
    begin
      fmSelPerSkl.quMHAll1.Active:=False;
      fmSelPerSkl.Release;
    end;
  end;
  Cursor:=crDefault; Delay(10);
  Memo1.Lines.Add('������������ ��.');
end;

procedure TfmMoveSel.acExportExExecute(Sender: TObject);
begin
  // ������� � ������
  if LevelMoveSel.Visible then prExportExel(ViewMoveSel,dmO.dsMoveSel,dmO.quMoveSel);
  if LevelRemn.Visible then prExportExel(ViewRemn,dmO.dsRemnDate,dmO.quRemnDate);
end;

procedure TfmMoveSel.FormShow(Sender: TObject);
begin
  Memo1.Clear;
end;

procedure TfmMoveSel.ViewRemnDblClick(Sender: TObject);
begin
  //�������� ������
  with dmO do if not quRemnDate.Eof then prPreShowMove(quRemnDateID.AsInteger,0,CommonSet.DateTo,CommonSet.IdStore,quRemnDateNAME.AsString);
  fmCardsMove.PageControl1.ActivePageIndex:=2;
  fmCardsMove.ShowModal;
end;

procedure TfmMoveSel.ViewMoveSelDblClick(Sender: TObject);
begin
  //�������� ������
  with dmO do if not quMoveSel.Eof then prPreShowMove(quMoveSelID.AsInteger,CommonSet.DateFrom,CommonSet.DateTo,CommonSet.IdStore,quMoveSelNAME.AsString);
  fmCardsMove.PageControl1.ActivePageIndex:=2;
  fmCardsMove.ShowModal;
end;

end.
