unit AddDoc3;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, cxGraphics, cxCurrencyEdit, cxDropDownEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxButtonEdit,
  cxMaskEdit, cxCalendar, cxControls, cxContainer, cxEdit, cxTextEdit,
  StdCtrls, ExtCtrls, Menus, cxLookAndFeelPainters, cxButtons, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, DB, cxDBData, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxLabel, dxfLabel, Placemnt, FIBDataSet,
  pFIBDataSet, DBClient, FIBQuery, pFIBQuery, pFIBStoredProc,
  XPStyleActnCtrls, ActnList, ActnMan;

type
  TfmAddDoc3 = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Label1: TLabel;
    cxTextEdit1: TcxTextEdit;
    cxDateEdit1: TcxDateEdit;
    Label4: TLabel;
    Label5: TLabel;
    Label12: TLabel;
    cxLookupComboBox1: TcxLookupComboBox;
    Panel2: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Panel3: TPanel;
    ViewDoc3: TcxGridDBTableView;
    LevelDoc3: TcxGridLevel;
    GridDoc3: TcxGrid;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    cxLabel3: TcxLabel;
    Label15: TLabel;
    FormPlacement1: TFormPlacement;
    taSpec: TClientDataSet;
    dsSpec: TDataSource;
    taSpecNum: TIntegerField;
    taSpecIdGoods: TIntegerField;
    taSpecNameG: TStringField;
    taSpecIM: TIntegerField;
    taSpecSM: TStringField;
    taSpecQuant: TFloatField;
    taSpecPrice1: TCurrencyField;
    taSpecSum1: TCurrencyField;
    taSpecPrice2: TCurrencyField;
    taSpecSum2: TCurrencyField;
    taSpecSumNac: TCurrencyField;
    taSpecProcNac: TFloatField;
    prCalcPrice: TpFIBStoredProc;
    cxLabel4: TcxLabel;
    ViewDoc3Num: TcxGridDBColumn;
    ViewDoc3IdGoods: TcxGridDBColumn;
    ViewDoc3NameG: TcxGridDBColumn;
    ViewDoc3IM: TcxGridDBColumn;
    ViewDoc3SM: TcxGridDBColumn;
    ViewDoc3Quant: TcxGridDBColumn;
    ViewDoc3Price1: TcxGridDBColumn;
    ViewDoc3Sum1: TcxGridDBColumn;
    ViewDoc3Price2: TcxGridDBColumn;
    ViewDoc3Sum2: TcxGridDBColumn;
    ViewDoc3SumNac: TcxGridDBColumn;
    ViewDoc3ProcNac: TcxGridDBColumn;
    cxLabel5: TcxLabel;
    cxLabel6: TcxLabel;
    taSpecKm: TFloatField;
    amDocVn: TActionManager;
    acAddPos: TAction;
    acDelPos: TAction;
    acAddList: TAction;
    acDelAll: TAction;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    taSpecPrice3: TCurrencyField;
    taSpecSum3: TCurrencyField;
    ViewDoc3Price3: TcxGridDBColumn;
    ViewDoc3Sum3: TcxGridDBColumn;
    cxLookupComboBox2: TcxLookupComboBox;
    Label2: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure cxLookupComboBox1PropertiesChange(Sender: TObject);
    procedure cxLabel1Click(Sender: TObject);
    procedure ViewDoc3DragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure ViewDoc3DragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure taSpecQuantChange(Sender: TField);
    procedure taSpecPrice1Change(Sender: TField);
    procedure taSpecSum1Change(Sender: TField);
    procedure taSpecPrice2Change(Sender: TField);
    procedure taSpecSum2Change(Sender: TField);
    procedure cxLabel3Click(Sender: TObject);
    procedure cxLabel2Click(Sender: TObject);
    procedure cxLabel4Click(Sender: TObject);
    procedure ViewDoc3Editing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cxLabel5Click(Sender: TObject);
    procedure ViewDoc3EditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure ViewDoc3EditKeyPress(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
    procedure acAddPosExecute(Sender: TObject);
    procedure acDelPosExecute(Sender: TObject);
    procedure acAddListExecute(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure acDelAllExecute(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure cxLabel6Click(Sender: TObject);
    procedure ViewDoc3SMPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxLookupComboBox2PropertiesChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAddDoc3: TfmAddDoc3;
  iCol:Integer = 0;
  bAdd:Boolean = False;

implementation

uses Un1, dmOffice, Clients, Goods, FCards, DocsIn, CurMessure, OMessure,
  DMOReps, DocsVn;

{$R *.dfm}

procedure TfmAddDoc3.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  GridDoc3.Align:=alClient;
  ViewDoc3.RestoreFromIniFile(CurDir+GridIni);
end;

procedure TfmAddDoc3.cxLookupComboBox1PropertiesChange(Sender: TObject);
begin
  with dmO do
  begin
    CurVal.IdMH:=cxLookupComboBox1.EditValue;
    CurVal.NAMEMH:=cxLookupComboBox1.Text;
    if quMHAll.Locate('ID',CurVal.IdMH,[]) then
    begin
      Label15.Caption:='��. ����: '+quMHAllNAMEPRICE.AsString;
      Label15.Tag:=quMHAllDEFPRICE.AsInteger;
    end else
    begin
      Label15.Caption:='';
      Label15.Tag:=0;
    end;
  end;
end;

procedure TfmAddDoc3.cxLabel1Click(Sender: TObject);
begin
  acAddList.Execute;
end;

procedure TfmAddDoc3.ViewDoc3DragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept:=False;
  if bDrIn then  Accept:=True;
end;

procedure TfmAddDoc3.ViewDoc3DragDrop(Sender, Source: TObject; X,
  Y: Integer);
Var iCo:Integer;
    i,j: Integer;
    iNum: Integer;
    Rec:TcxCustomGridRecord;
    iMax:Integer;
    Km:Real;
begin
//
  if bDrIn then
  begin
    ResetAddVars;
    iCo:=fmGoods.ViewGoods.Controller.SelectedRecordCount;
    if iCo>0 then
    begin
      if MessageDlg('�� ������������� ������ ����������� ��������� ������� ('+IntToStr(iCo)+' ��.) � ������������ ���������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        iMax:=1;
        fmAddDoc3.taSpec.First;
        if not fmAddDoc3.taSpec.Eof then
        begin
          fmAddDoc3.taSpec.Last;
          iMax:=fmAddDoc3.taSpecNum.AsInteger+1;
        end;

        with dmO do
        begin
          ViewDoc3.BeginUpdate;
          for i:=0 to fmGoods.ViewGoods.Controller.SelectedRecordCount-1 do
          begin
            Rec:=fmGoods.ViewGoods.Controller.SelectedRecords[i];

            for j:=0 to Rec.ValueCount-1 do
            begin
              if fmGoods.ViewGoods.Columns[j].Name='ViewGoodsID' then break;
            end;

            iNum:=Rec.Values[j];
          //��� ��� - ����������

            if quCardsSel.Locate('ID',iNum,[]) then
            begin
              with fmAddDoc3 do
              begin
                taSpec.Append;
                taSpecNum.AsInteger:=iMax;
                taSpecIdGoods.AsInteger:=quCardsSelID.AsInteger;
                taSpecNameG.AsString:=quCardsSelNAME.AsString;
                taSpecIM.AsInteger:=quCardsSelIMESSURE.AsInteger;
                taSpecSM.AsString:=prFindKNM(quCardsSelIMESSURE.AsInteger,Km);
                taSpecQuant.AsFloat:=1;
                taSpecPrice1.AsCurrency:=0;
                taSpecSum1.AsCurrency:=0;
                taSpecPrice2.AsCurrency:=0;
                taSpecSum2.AsCurrency:=0;
                taSpecPrice3.AsCurrency:=0;
                taSpecSum3.AsCurrency:=0;
                taSpecSumNac.AsCurrency:=0;
                taSpecProcNac.AsFloat:=0;
                taSpecKm.AsFloat:=Km;
                taSpec.Post;
                inc(iMax);
              end;
            end;
          end;
          ViewDoc3.EndUpdate;
        end;
      end;
    end;
  end;
end;

procedure TfmAddDoc3.taSpecQuantChange(Sender: TField);
begin
  //���������� �����
  if iCol=1 then
  begin
    taSpecSum1.AsCurrency:=taSpecPrice1.AsCurrency*taSpecQuant.AsFloat;
    taSpecSum2.AsCurrency:=taSpecPrice2.AsCurrency*taSpecQuant.AsFloat;
    taSpecSumNac.AsCurrency:=taSpecSum2.AsCurrency-taSpecSum1.AsCurrency;
    if taSpecSum1.AsCurrency>0.01 then
    begin
      taSpecProcNac.AsFloat:=(taSpecSum2.AsCurrency-taSpecSum1.AsCurrency)/taSpecSum1.AsCurrency*100;
    end;
  end;
end;

procedure TfmAddDoc3.taSpecPrice1Change(Sender: TField);
begin
  //���������� ����
  if iCol=2 then
  begin
    taSpecSum1.AsCurrency:=taSpecPrice1.AsCurrency*taSpecQuant.AsFloat;
//  taSpecSum2.AsCurrency:=taSpecPrice2.AsCurrency*taSpecQuant.AsFloat;
    taSpecSumNac.AsCurrency:=taSpecSum2.AsCurrency-taSpecSum1.AsCurrency;
    if taSpecSum1.AsCurrency>0.01 then
    begin
      taSpecProcNac.AsFloat:=(taSpecSum2.AsCurrency-taSpecSum1.AsCurrency)/taSpecSum1.AsCurrency*100;
    end;
  end;
end;

procedure TfmAddDoc3.taSpecSum1Change(Sender: TField);
begin
  if iCol=3 then
  begin
    if abs(taSpecQuant.AsFloat)>0 then taSpecPrice1.AsCurrency:=RoundEx(taSpecSum1.AsCurrency/taSpecQuant.AsFloat*100)/100;
    taSpecSumNac.AsCurrency:=taSpecSum2.AsCurrency-taSpecSum1.AsCurrency;
    if taSpecSum1.AsCurrency>0.01 then
    begin
      taSpecProcNac.AsFloat:=(taSpecSum2.AsCurrency-taSpecSum1.AsCurrency)/taSpecSum1.AsCurrency*100;
    end;
  end;
end;

procedure TfmAddDoc3.taSpecPrice2Change(Sender: TField);
begin
  //���������� ����
  if iCol=4 then
  begin
//  taSpecSum1.AsCurrency:=taSpecPrice1.AsCurrency*taSpecQuant.AsFloat;
    taSpecSum2.AsCurrency:=taSpecPrice2.AsCurrency*taSpecQuant.AsFloat;
    taSpecSumNac.AsCurrency:=taSpecSum2.AsCurrency-taSpecSum1.AsCurrency;
    if taSpecSum1.AsCurrency>0.01 then
    begin
      taSpecProcNac.AsFloat:=(taSpecSum2.AsCurrency-taSpecSum1.AsCurrency)/taSpecSum1.AsCurrency*100;
    end;  
  end;
end;

procedure TfmAddDoc3.taSpecSum2Change(Sender: TField);
begin
  if iCol=5 then
  begin
    if abs(taSpecQuant.AsFloat)>0 then taSpecPrice2.AsCurrency:=RoundEx(taSpecSum2.AsCurrency/taSpecQuant.AsFloat*100)/100;
    taSpecSumNac.AsCurrency:=taSpecSum2.AsCurrency-taSpecSum1.AsCurrency;
    if taSpecSum1.AsCurrency>0.01 then
    begin
      taSpecProcNac.AsFloat:=(taSpecSum2.AsCurrency-taSpecSum1.AsCurrency)/taSpecSum1.AsCurrency*100;
    end;
  end;
end;

procedure TfmAddDoc3.cxLabel3Click(Sender: TObject);
Var PriceSp,PriceUch,rSumIn,rSumUch,rQs,rQ,rQp,rMessure:Real;
begin
  //����������� ����
  with dmO do
  begin
    taSpec.First;
    while not taSpec.Eof do
    begin
      PriceSp:=0;
      rSumIn:=0;
      rSumUch:=0;
      rQs:=taSpecQuant.AsFloat*taSpecKm.AsFloat; //�������� � ��������
      prSelPartIn(taSpecIdGoods.AsInteger,cxLookupComboBox2.EditValue,0);

      quSelPartIn.First;
      if rQs>0 then
      begin
        while (not quSelPartIn.Eof) and (rQs>0) do
        begin
          //���� �� ���� ������� ���� �����, ��������� �������� ���
          rQp:=quSelPartInQREMN.AsFloat;
          if rQs<=rQp then  rQ:=rQs//��������� ������ ������ ���������
                            else  rQ:=rQp;
          rQs:=rQs-rQ;

          PriceSp:=quSelPartInPRICEIN.AsFloat;
          PriceUch:=quSelPartInPRICEOUT.AsFloat;
          rSumIn:=rSumIn+PriceSp*rQ;
          rSumUch:=rSumUch+PriceUch*rQ;
          quSelPartIn.Next;
        end;

        if rQs>0 then //�������� ������������� ������, �������� � ������, �� � ������� ��������� ������ ���, ��� � �������������
        begin
          if PriceSp=0 then
          begin //��� ���� ���������� ������� � ���������� ����������
            prCalcLastPrice1.ParamByName('IDGOOD').AsInteger:=taSpecIdGoods.AsInteger;
            prCalcLastPrice1.ExecProc;
            PriceSp:=prCalcLastPrice1.ParamByName('PRICEIN').AsCurrency;
            rMessure:=prCalcLastPrice1.ParamByName('KOEF').AsFloat;
            if (rMessure<>0)and(rMessure<>1) then PriceSp:=PriceSp/rMessure;
          end;
          rSumIn:=rSumIn+PriceSp*rQs;
        end;
      end;
      quSelPartIn.Active:=False;

      //�������� ���������
      taSpec.Edit;
      if taSpecQuant.AsFloat<>0 then
      begin
        taSpecSum1.AsCurrency:=rSumIn;
        taSpecSum2.AsCurrency:=rSumUch;
        taSpecPrice1.AsCurrency:=rSumIn/taSpecQuant.AsFloat;
        taSpecPrice2.AsCurrency:=rSumUch/taSpecQuant.AsFloat;
      end else
      begin
        taSpecSum1.AsCurrency:=0;
        taSpecSum2.AsCurrency:=0;
        taSpecPrice1.AsCurrency:=0;
        taSpecPrice2.AsCurrency:=0;
      end;
      taSpec.Post;

      taSpec.Next;
      delay(10);
    end;
    taSpec.First;
  end;
end;

procedure TfmAddDoc3.cxLabel2Click(Sender: TObject);
begin
  acDelPos.Execute;
end;

procedure TfmAddDoc3.cxLabel4Click(Sender: TObject);
begin
  //�������� ����
  ViewDoc3.BeginUpdate;
  taSpec.First;
  while not taSpec.Eof do
  begin
    taSpec.Edit;
    taSpecPrice2.AsCurrency:=taSpecPrice1.AsCurrency;
    taSpecSum2.AsCurrency:=taSpecSum1.AsCurrency;
    taSpecPrice3.AsCurrency:=taSpecPrice1.AsCurrency;
    taSpecSum3.AsCurrency:=taSpecSum1.AsCurrency;
    taSpecSumNac.AsCurrency:=0;
    taSpecProcNac.AsFloat:=0;
    taSpec.Post;

    taSpec.Next;
    delay(10);
  end;
  taSpec.First;
  ViewDoc3.EndUpdate;
end;

procedure TfmAddDoc3.ViewDoc3Editing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  iCol:=0;
  if ViewDoc3.Controller.FocusedColumn.Name='ViewDoc3Quant' then iCol:=1;
  if ViewDoc3.Controller.FocusedColumn.Name='ViewDoc3Price1' then iCol:=2;
  if ViewDoc3.Controller.FocusedColumn.Name='ViewDoc3Sum1' then iCol:=3;
  if ViewDoc3.Controller.FocusedColumn.Name='ViewDoc3Price2' then iCol:=4;
  if ViewDoc3.Controller.FocusedColumn.Name='ViewDoc3Sum2' then iCol:=5;


end;

procedure TfmAddDoc3.FormShow(Sender: TObject);
begin
  iCol:=0;
  if cxTextEdit1.Text='' then
  begin
    cxTextEdit1.SetFocus;
    cxTextEdit1.SelectAll;
  end else GridDoc3.SetFocus;

  ViewDoc3NameG.Options.Editing:=False;
end;

procedure TfmAddDoc3.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewDoc3.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmAddDoc3.cxLabel5Click(Sender: TObject);
begin
  acAddPos.Execute;
end;

procedure TfmAddDoc3.ViewDoc3EditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
Var iCode:Integer;
    Km:Real;
    sName:String;
begin
  with dmO do
  begin
    if (Key=$0D) then
    begin
      if ViewDoc3.Controller.FocusedColumn.Name='ViewDoc3IdGoods' then
      begin
        iCode:=VarAsType(AEdit.EditingValue, varInteger);

        quFCard.Active:=False;
        quFCard.SelectSQL.Clear;
        quFCard.SelectSQL.Add('SELECT ID,NAME,IMESSURE,PARENT,TCARD,INDS');
        quFCard.SelectSQL.Add('FROM OF_CARDS');
        quFCard.SelectSQL.Add('where ID='+IntToStr(iCode));
        quFCard.Active:=True;

        if quFCard.RecordCount=1 then
        begin
          ViewDoc3.BeginUpdate;
          taSpec.Edit;
          taSpecIdGoods.AsInteger:=iCode;
          taSpecNameG.AsString:=quFCardNAME.AsString;
          taSpecIM.AsInteger:=quFCardIMESSURE.AsInteger;
          taSpecSM.AsString:=prFindKNM(quFCardIMESSURE.AsInteger,Km);
          taSpecKm.AsFloat:=Km;
          taSpecQuant.AsFloat:=0;
          taSpecPrice1.AsFloat:=0;
          taSpecSum1.AsFloat:=0;
          taSpecPrice2.AsFloat:=0;
          taSpecSum2.AsFloat:=0;
          taSpecSumNac.AsFloat:=0;
          taSpecProcNac.AsFloat:=0;
          taSpec.Post;

          ViewDoc3.EndUpdate;
        end;
      end;
      if ViewDoc3.Controller.FocusedColumn.Name='ViewDoc3NameG' then
      begin
        sName:=VarAsType(AEdit.EditingValue, varString);

        quFCard.Active:=False;
        quFCard.SelectSQL.Clear;
        quFCard.SelectSQL.Add('SELECT ID,NAME,IMESSURE,PARENT,TCARD,INDS');
        quFCard.SelectSQL.Add('FROM OF_CARDS');
        quFCard.SelectSQL.Add('where UPPER(NAME) like ''%'+AnsiUpperCase(sName)+'%''');
        quFCard.SelectSQL.Add('Order by NAME');

        quFCard.Active:=True;

        bAdd:=True;
        //�������� ����� ������ � ����� ������
        fmFCards.ShowModal;
        if fmFCards.ModalResult=mrOk then
        begin
          if quFCard.RecordCount>0 then
          begin
            ViewDoc3.BeginUpdate;
            taSpec.Edit;
            taSpecIdGoods.AsInteger:=quFCardID.AsInteger;
            taSpecNameG.AsString:=quFCardNAME.AsString;
            taSpecIM.AsInteger:=quFCardIMESSURE.AsInteger;
            taSpecSM.AsString:=prFindKNM(quFCardIMESSURE.AsInteger,Km);
            taSpecKm.AsFloat:=Km;
            taSpecQuant.AsFloat:=0;
            taSpecPrice1.AsFloat:=0;
            taSpecSum1.AsFloat:=0;
            taSpecPrice2.AsFloat:=0;
            taSpecSum2.AsFloat:=0;
            taSpecSumNac.AsFloat:=0;
            taSpecProcNac.AsFloat:=0;
            taSpec.Post;
            ViewDoc3.EndUpdate;
            AEdit.SelectAll;
          end;
        end;
        bAdd:=False;
      end;
    end else
      if ViewDoc3.Controller.FocusedColumn.Name='ViewDoc3IdGoods' then
        if fTestKey(Key)=False then
          if taSpec.State in [dsEdit,dsInsert] then taSpec.Cancel;
  end;
end;

procedure TfmAddDoc3.ViewDoc3EditKeyPress(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
Var Km:Real;
    sName:String;
begin
  if bAdd then exit;
  with dmO do
  begin
    if (ViewDoc3.Controller.FocusedColumn.Name='ViewDoc3NameG') then
    begin
      sName:=VarAsType(AEdit.EditingValue ,varString)+Key;
//      Label2.Caption:=sName;
      if sName>'' then
      begin
        quFCard.Active:=False;
        quFCard.SelectSQL.Clear;
        quFCard.SelectSQL.Add('SELECT ID,NAME,IMESSURE,PARENT,TCARD,INDS');
        quFCard.SelectSQL.Add('FROM OF_CARDS');
        quFCard.SelectSQL.Add('where UPPER(NAME) like ''%'+AnsiUpperCase(sName)+'%''');
        quFCard.SelectSQL.Add('Order by NAME');
        quFCard.Active:=True;

        if quFCard.RecordCount=1 then
        begin
          ViewDoc3.BeginUpdate;
          taSpec.Edit;
          taSpecIdGoods.AsInteger:=quFCardID.AsInteger;
          taSpecNameG.AsString:=quFCardNAME.AsString;
          taSpecIM.AsInteger:=quFCardIMESSURE.AsInteger;
          taSpecSM.AsString:=prFindKNM(quFCardIMESSURE.AsInteger,Km);
          taSpecKm.AsFloat:=Km;
          taSpecQuant.AsFloat:=0;
          taSpecPrice1.AsFloat:=0;
          taSpecSum1.AsFloat:=0;
          taSpecPrice2.AsFloat:=0;
          taSpecSum2.AsFloat:=0;
          taSpecSumNac.AsFloat:=0;
          taSpecProcNac.AsFloat:=0;
          taSpec.Post;
          ViewDoc3.EndUpdate;
          AEdit.SelectAll;
          ViewDoc3NameG.Options.Editing:=False;
          ViewDoc3NameG.Focused:=True;
          Key:=#0;
        end;
      end;
    end;
  end;//}
end;

procedure TfmAddDoc3.acAddPosExecute(Sender: TObject);
Var iMax:Integer;
begin
  //�������� �������
  iMax:=1;
  ViewDoc3.BeginUpdate;

  taSpec.First;
  if not taSpec.Eof then
  begin
    taSpec.Last;
    iMax:=taSpecNum.AsInteger+1;
  end;

  taSpec.Append;
  taSpecNum.AsInteger:=iMax;
  taSpecIdGoods.AsInteger:=0;
  taSpecNameG.AsString:='';
  taSpecIM.AsInteger:=0;
  taSpecSM.AsString:='';
  taSpecKm.AsFloat:=0;
  taSpecQuant.AsFloat:=0;
  taSpecPrice1.AsFloat:=0;
  taSpecSum1.AsFloat:=0;
  taSpecPrice2.AsFloat:=0;
  taSpecSum2.AsFloat:=0;
  taSpecSumNac.AsFloat:=0;
  taSpecProcNac.AsFloat:=0;
  taSpec.Post;
  ViewDoc3.EndUpdate;
  GridDoc3.SetFocus;

  ViewDoc3NameG.Options.Editing:=True;
  ViewDoc3NameG.Focused:=True;

end;

procedure TfmAddDoc3.acDelPosExecute(Sender: TObject);
begin
  //������� �������
  if taSpec.RecordCount>0 then
  begin
    taSpec.Delete;
  end;
{  if MessageDlg('�� ������������� ������ ������� �������  � '+taSpecNum.AsString+'  '+taSpecNameG.AsString+' ?',
     mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
    taSpec.Delete;
  end;}
end;

procedure TfmAddDoc3.acAddListExecute(Sender: TObject);
begin
  bAddSpecVn:=True;
  if fmGoods.Visible then fmGoods.Close;
  fmGoods.Show;
end;

procedure TfmAddDoc3.cxButton1Click(Sender: TObject);
Var IdH:Integer;
    rSum1,rSum2,rSum3:Real;
begin
  //��������
  with dmORep do
  begin
    IdH:=cxTextEdit1.Tag;
    if taSpec.State in [dsEdit,dsInsert] then taSpec.Post;
    if cxDateEdit1.Date<3000 then  cxDateEdit1.Date:=Date;

    //������� ������ ������
    ViewDoc3.BeginUpdate;

    if cxTextEdit1.Tag=0 then IDH:=GetId('DocVn');

    quDocsVnId.Active:=False;
    quDocsVnId.ParamByName('IDH').AsInteger:=IDH;
    quDocsVnId.Active:=True;

    quDocsVnId.First;
    if quDocsVnId.RecordCount=0 then quDocsVnId.Append else quDocsVnId.Edit;

    quDocsVnIdID.AsInteger:=IDH;
    quDocsVnIdDATEDOC.AsDateTime:=Trunc(cxDateEdit1.Date);
    quDocsVnIdNUMDOC.AsString:=cxTextEdit1.Text;
    quDocsVnIdIDSKL_FROM.AsInteger:=cxLookupComboBox2.EditValue;
    quDocsVnIdIDSKL_TO.AsInteger:=cxLookupComboBox1.EditValue;
    quDocsVnIdSUMIN.AsFloat:=0;
    quDocsVnIdSUMUCH.AsFloat:=0;
    quDocsVnIdSUMUCH1.AsFloat:=0;
    quDocsVnIdSUMTAR.AsFloat:=0;
    quDocsVnIdPROCNAC.AsFloat:=0;
    quDocsVnIdIACTIVE.AsInteger:=0;
    quDocsVnId.Post;

    cxTextEdit1.Tag:=IDH;

    //�������� ������������
    quSpecVnSel.Active:=False;
    quSpecVnSel.ParamByName('IDHD').AsInteger:=IDH;
    quSpecVnSel.Active:=True;

    quSpecVnSel.First; //������� ������
    while not quSpecVnSel.Eof do quSpecVnSel.Delete;

    rSum1:=0; rSum2:=0; rSum3:=0;
    taSpec.First;
    while not taSpec.Eof do
    begin
      quSpecVnSel.Append;
      quSpecVnSelIDHEAD.AsInteger:=IDH;
      quSpecVnSelID.AsInteger:=GetId('SpecVn');
      quSpecVnSelNUM.AsInteger:=taSpecNum.AsInteger;
      quSpecVnSelIDCARD.AsInteger:=taSpecIdGoods.AsInteger;
      quSpecVnSelQUANT.AsFloat:=taSpecQuant.AsFloat;
      quSpecVnSelPRICEIN.AsFloat:=taSpecPrice1.AsFloat;
      quSpecVnSelSUMIN.AsFloat:=taSpecSum1.AsFloat;
      quSpecVnSelPRICEUCH.AsFloat:=taSpecPrice2.AsFloat;
      quSpecVnSelSUMUCH.AsFloat:=taSpecSum2.AsFloat;
      quSpecVnSelPRICEUCH1.AsFloat:=taSpecPrice3.AsFloat;
      quSpecVnSelSUMUCH1.AsFloat:=taSpecSum3.AsFloat;
      quSpecVnSelIDM.AsInteger:=taSpecIM.AsInteger;
      quSpecVnSelKM.AsFloat:=taSpecKM.AsFloat;
      quSpecVnSel.Post;
      rSum1:=rSum1+taSpecSum1.AsFloat; rSum2:=rSum2+taSpecSum2.AsFloat; rSum3:=rSum3+taSpecSum3.AsFloat;

      taSpec.Next;
    end;

    quDocsVnId.Edit;
    quDocsVnIdSUMIN.AsFloat:=rSum1;
    quDocsVnIdSUMUCH.AsFloat:=rSum2;
    quDocsVnIdSUMUCH1.AsFloat:=rSum3;
    quDocsVnIdSUMTAR.AsFloat:=0;
    quDocsVnIdPROCNAC.AsFloat:=0;
    quDocsVnIdIACTIVE.AsInteger:=0;
    quDocsVnId.Post;

    quDocsVnId.Active:=False;

    fmDocsVn.ViewDocsVn.BeginUpdate;
    quDocsVnSel.FullRefresh;
    quDocsVnSel.Locate('ID',IDH,[]);
    fmDocsVn.ViewDocsVn.EndUpdate;

    ViewDoc3.EndUpdate;
    GridDoc3.SetFocus;
  end;
end;

procedure TfmAddDoc3.acDelAllExecute(Sender: TObject);
begin
  if taSpec.RecordCount>0 then
  begin
    if MessageDlg('�� ������������� ������ �������� ������������ ?',
     mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      taSpec.First; while not taSpec.Eof do taSpec.Delete;
    end;
  end;
end;

procedure TfmAddDoc3.N1Click(Sender: TObject);
Type tRecSpec = record
     Num:INteger;
     IdGoods:Integer;
     NameG:String;
     IM:Integer;
     SM:String;
     Quant,Price1,Price2,Sum1,Sum2:Real;
     iNds:INteger;
     sNds:String;
     rNds,SumNac,ProcNac,Km:Real;
     end;
Var RecSpec:TRecSpec;
    iMax:Integer;
begin
  //���������� �������
  if not taSpec.Eof then
  begin
    RecSpec.Num:=taSpecNum.AsInteger;
    RecSpec.IdGoods:=taSpecIdGoods.AsInteger;
    RecSpec.Quant:=taSpecQuant.AsFloat;
    RecSpec.Price1:=taSpecPrice1.AsFloat;
    RecSpec.Sum1:=taSpecSum1.AsFloat;
    RecSpec.Price2:=taSpecPrice2.AsFloat;
    RecSpec.Sum2:=taSpecSum2.AsFloat;
    RecSpec.IM:=taSpecIM.AsInteger;
    RecSpec.NameG:=taSpecNameG.AsString;
    RecSpec.SM:=taSpecSM.AsString;
    RecSpec.SumNac:=taSpecSumNac.AsFloat;
    RecSpec.ProcNac:=taSpecProcNac.AsFloat;
    RecSpec.Km:=taSpecKm.AsFloat;

    iMax:=1;
    ViewDoc3.BeginUpdate;

    taSpec.First;
    if not taSpec.Eof then
    begin
      taSpec.Last;
      iMax:=taSpecNum.AsInteger+1;
    end;

    taSpec.Append;
    taSpecNum.AsInteger:=iMax;
    taSpecIdGoods.AsInteger:=RecSpec.IdGoods;
    taSpecNameG.AsString:=RecSpec.NameG;
    taSpecIM.AsInteger:=RecSpec.IM;
    taSpecSM.AsString:=RecSpec.SM;
    taSpecKm.AsFloat:=RecSpec.Km;
    taSpecQuant.AsFloat:=RecSpec.Quant;
    taSpecPrice1.AsFloat:=RecSpec.Price1;
    taSpecSum1.AsFloat:=RecSpec.Sum1;
    taSpecPrice2.AsFloat:=RecSpec.Price2;
    taSpecSum2.AsFloat:=RecSpec.Sum2;
    taSpecSumNac.AsFloat:=RecSpec.SumNac;
    taSpecProcNac.AsFloat:=RecSpec.ProcNac;
    taSpec.Post;
    ViewDoc3.EndUpdate;
    GridDoc3.SetFocus;
  end;
end;

procedure TfmAddDoc3.cxLabel6Click(Sender: TObject);
begin
  acDelall.Execute;
end;

procedure TfmAddDoc3.ViewDoc3SMPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
Var rK:Real;
    iM:Integer;
    Sm:String;
begin
  with dmO do
  begin
    if taSpecIm.AsInteger>0 then
    begin
      bAddSpec:=True;
      fmMessure.ShowModal;
      if fmMessure.ModalResult=mrOk then
      begin
        iM:=iMSel; iMSel:=0;
        Sm:=prFindKNM(iM,rK); //�������� ����� �������� � ����� �����

        taSpec.Edit;
        taSpecIM.AsInteger:=iM;
        taSpecKm.AsFloat:=rK;
        taSpecSM.AsString:=Sm;
        taSpec.Post;
      end;
    end;
  end;
end;

procedure TfmAddDoc3.cxLookupComboBox2PropertiesChange(Sender: TObject);
begin
  with dmO do
  begin
    CurVal.IdMH:=cxLookupComboBox2.EditValue;
    CurVal.NAMEMH:=cxLookupComboBox2.Text;
    if quMHAll.Locate('ID',CurVal.IdMH,[]) then
    begin
      Label2.Caption:='��. ����: '+quMHAllNAMEPRICE.AsString;
      Label2.Tag:=quMHAllDEFPRICE.AsInteger;
    end else
    begin
      Label2.Caption:='';
      Label2.Tag:=0;
    end;
  end;
end;

end.
