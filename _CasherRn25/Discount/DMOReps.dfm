object dmORep: TdmORep
  OldCreateOrder = False
  Left = 456
  Top = 402
  Height = 583
  Width = 901
  object taPartTest: TClientDataSet
    Aggregates = <>
    FileName = 'PartTest.cds'
    Params = <>
    Left = 24
    Top = 16
    object taPartTestNum: TIntegerField
      FieldName = 'Num'
    end
    object taPartTestIdGoods: TIntegerField
      FieldName = 'IdGoods'
    end
    object taPartTestNameG: TStringField
      FieldName = 'NameG'
      Size = 200
    end
    object taPartTestIM: TIntegerField
      FieldName = 'IM'
    end
    object taPartTestSM: TStringField
      FieldName = 'SM'
      Size = 50
    end
    object taPartTestQuant: TFloatField
      FieldName = 'Quant'
      DisplayFormat = '0.000'
      EditFormat = '0.000'
    end
    object taPartTestPrice1: TCurrencyField
      FieldName = 'Price1'
      DisplayFormat = ',0.00;-,0.00'
      EditFormat = ',0.00;-,0.00'
    end
    object taPartTestiRes: TSmallintField
      FieldName = 'iRes'
    end
  end
  object dsPartTest: TDataSource
    DataSet = taPartTest
    Left = 24
    Top = 72
  end
  object taCalc: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 89
    Top = 16
    object taCalcArticul: TIntegerField
      FieldName = 'Articul'
    end
    object taCalcName: TStringField
      FieldName = 'Name'
      Size = 100
    end
    object taCalcIdm: TIntegerField
      FieldName = 'Idm'
    end
    object taCalcsM: TStringField
      FieldName = 'sM'
    end
    object taCalcKm: TFloatField
      FieldName = 'Km'
    end
    object taCalcQuant: TFloatField
      FieldName = 'Quant'
      DisplayFormat = '0.000'
    end
    object taCalcQuantFact: TFloatField
      FieldName = 'QuantFact'
      DisplayFormat = '0.000'
    end
    object taCalcQuantDiff: TFloatField
      FieldName = 'QuantDiff'
      DisplayFormat = '0.000'
    end
    object taCalcSumUch: TFloatField
      FieldName = 'SumUch'
      DisplayFormat = '0.00'
    end
    object taCalcSumIn: TFloatField
      FieldName = 'SumIn'
      DisplayFormat = '0.00'
    end
  end
  object dsCalc: TDataSource
    DataSet = taCalc
    Left = 89
    Top = 72
  end
  object frRep1: TfrReport
    InitialZoom = pzDefault
    PreviewButtons = [pbZoom, pbLoad, pbSave, pbPrint, pbFind, pbHelp, pbExit]
    RebuildPrinter = False
    Left = 248
    Top = 16
    ReportForm = {19000000}
  end
  object frTextExport1: TfrTextExport
    ScaleX = 1.000000000000000000
    ScaleY = 1.000000000000000000
    Left = 968
    Top = 560
  end
  object frRTFExport1: TfrRTFExport
    ScaleX = 1.300000000000000000
    ScaleY = 1.000000000000000000
    Left = 968
    Top = 504
  end
  object frCSVExport1: TfrCSVExport
    ScaleX = 1.000000000000000000
    ScaleY = 1.000000000000000000
    Delimiter = ';'
    Left = 968
    Top = 344
  end
  object frHTMExport1: TfrHTMExport
    ScaleX = 1.000000000000000000
    ScaleY = 1.000000000000000000
    Left = 968
    Top = 200
  end
  object frHTML2Export1: TfrHTML2Export
    Scale = 1.000000000000000000
    Navigator.Position = []
    Navigator.Font.Charset = DEFAULT_CHARSET
    Navigator.Font.Color = clWindowText
    Navigator.Font.Height = -11
    Navigator.Font.Name = 'MS Sans Serif'
    Navigator.Font.Style = []
    Navigator.InFrame = False
    Navigator.WideInFrame = False
    Left = 968
    Top = 248
  end
  object frOLEExcelExport1: TfrOLEExcelExport
    HighQuality = False
    CellsAlign = False
    CellsBorders = False
    CellsFillColor = False
    CellsFontColor = False
    CellsFontName = False
    CellsFontSize = False
    CellsFontStyle = False
    CellsMerged = False
    CellsWrapWords = False
    ExportPictures = False
    PageBreaks = False
    AsText = False
    Left = 968
    Top = 448
  end
  object frXMLExcelExport1: TfrXMLExcelExport
    Left = 968
    Top = 400
  end
  object frTextAdvExport1: TfrTextAdvExport
    ScaleWidth = 1.000000000000000000
    ScaleHeight = 1.000000000000000000
    Borders = True
    Pseudogrpahic = False
    PageBreaks = True
    OEMCodepage = False
    EmptyLines = True
    LeadSpaces = True
    PrintAfter = False
    PrinterDialog = True
    UseSavedProps = True
    Left = 968
    Top = 296
  end
  object frRtfAdvExport1: TfrRtfAdvExport
    OpenAfterExport = True
    Wysiwyg = True
    Creator = 'FastReport http://www.fast-report.com'
    Left = 968
    Top = 152
  end
  object frBMPExport1: TfrBMPExport
    Left = 968
    Top = 8
  end
  object frJPEGExport1: TfrJPEGExport
    Left = 968
    Top = 56
  end
  object frTIFFExport1: TfrTIFFExport
    Left = 968
    Top = 104
  end
  object taCalcB: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'ID'
        DataType = ftInteger
      end
      item
        Name = 'CODEB'
        DataType = ftInteger
      end
      item
        Name = 'NAMEB'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'QUANT'
        DataType = ftFloat
      end
      item
        Name = 'PRICEOUT'
        DataType = ftFloat
      end
      item
        Name = 'SUMOUT'
        DataType = ftFloat
      end
      item
        Name = 'IDCARD'
        DataType = ftInteger
      end
      item
        Name = 'NAMEC'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'QUANTC'
        DataType = ftFloat
      end
      item
        Name = 'PRICEIN'
        DataType = ftFloat
      end
      item
        Name = 'SUMIN'
        DataType = ftFloat
      end
      item
        Name = 'IM'
        DataType = ftInteger
      end
      item
        Name = 'SM'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'SB'
        DataType = ftString
        Size = 10
      end>
    IndexDefs = <
      item
        Name = 'taCalcBIndex'
        Fields = 'ID'
        Options = [ixPrimary]
      end>
    IndexFieldNames = 'ID'
    Params = <>
    StoreDefs = True
    Left = 161
    Top = 16
    object taCalcBID: TIntegerField
      FieldName = 'ID'
    end
    object taCalcBCODEB: TIntegerField
      FieldName = 'CODEB'
    end
    object taCalcBNAMEB: TStringField
      FieldName = 'NAMEB'
      Size = 100
    end
    object taCalcBQUANT: TFloatField
      FieldName = 'QUANT'
      DisplayFormat = '0.000'
    end
    object taCalcBPRICEOUT: TFloatField
      FieldName = 'PRICEOUT'
      DisplayFormat = '0.00'
    end
    object taCalcBSUMOUT: TFloatField
      FieldName = 'SUMOUT'
      DisplayFormat = '0.00'
    end
    object taCalcBIDCARD: TIntegerField
      FieldName = 'IDCARD'
    end
    object taCalcBNAMEC: TStringField
      FieldName = 'NAMEC'
      Size = 100
    end
    object taCalcBQUANTC: TFloatField
      FieldName = 'QUANTC'
      DisplayFormat = '0.000'
    end
    object taCalcBPRICEIN: TFloatField
      FieldName = 'PRICEIN'
      DisplayFormat = '0.00'
    end
    object taCalcBSUMIN: TFloatField
      FieldName = 'SUMIN'
      DisplayFormat = '0.00'
    end
    object taCalcBIM: TIntegerField
      FieldName = 'IM'
    end
    object taCalcBSM: TStringField
      FieldName = 'SM'
    end
    object taCalcBSB: TStringField
      FieldName = 'SB'
      Size = 10
    end
  end
  object dsCalcB: TDataSource
    DataSet = taCalcB
    Left = 160
    Top = 72
  end
  object frdsCalcB: TfrDBDataSet
    DataSource = dsCalcB
    Left = 320
    Top = 16
  end
  object taTORep: TClientDataSet
    Aggregates = <>
    FileName = 'to.cds'
    FieldDefs = <
      item
        Name = 'sType'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'sDocType'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'sCliName'
        DataType = ftString
        Size = 200
      end
      item
        Name = 'sDate'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'sDocNum'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'rSum'
        DataType = ftCurrency
      end
      item
        Name = 'rSum1'
        DataType = ftCurrency
      end
      item
        Name = 'rSumO'
        DataType = ftCurrency
      end
      item
        Name = 'rSumO1'
        DataType = ftCurrency
      end>
    IndexDefs = <
      item
        Name = 'taTORepIndex1'
        Fields = 'sType;sDocType'
      end>
    IndexName = 'taTORepIndex1'
    Params = <>
    StoreDefs = True
    Left = 32
    Top = 144
    object taTORepsType: TStringField
      FieldName = 'sType'
    end
    object taTORepsDocType: TStringField
      FieldName = 'sDocType'
      Size = 50
    end
    object taTORepsCliName: TStringField
      FieldName = 'sCliName'
      Size = 200
    end
    object taTORepsDate: TStringField
      FieldName = 'sDate'
      Size = 10
    end
    object taTORepsDocNum: TStringField
      FieldName = 'sDocNum'
    end
    object taTOReprSum: TCurrencyField
      FieldName = 'rSum'
    end
    object taTOReprSum1: TCurrencyField
      FieldName = 'rSum1'
    end
    object taTOReprSumO: TCurrencyField
      FieldName = 'rSumO'
    end
    object taTOReprSumO1: TCurrencyField
      FieldName = 'rSumO1'
    end
  end
  object dsTORep: TDataSource
    DataSet = taTORep
    Left = 32
    Top = 200
  end
  object frdsTORep: TfrDBDataSet
    DataSource = dsTORep
    Left = 320
    Top = 72
  end
  object taCMove: TClientDataSet
    Aggregates = <>
    FileName = 'CMove.cds'
    Params = <>
    Left = 104
    Top = 144
    object taCMoveARTICUL: TIntegerField
      FieldName = 'ARTICUL'
    end
    object taCMoveIDATE: TIntegerField
      FieldName = 'IDATE'
    end
    object taCMoveIDSTORE: TIntegerField
      FieldName = 'IDSTORE'
    end
    object taCMoveNAMEMH: TStringField
      FieldName = 'NAMEMH'
      Size = 200
    end
    object taCMoveRB: TFloatField
      FieldName = 'RB'
      DisplayFormat = '0.000'
    end
    object taCMovePOSTIN: TFloatField
      FieldName = 'POSTIN'
      DisplayFormat = '0.000'
    end
    object taCMovePOSTOUT: TFloatField
      FieldName = 'POSTOUT'
      DisplayFormat = '0.000'
    end
    object taCMoveVNIN: TFloatField
      FieldName = 'VNIN'
      DisplayFormat = '0.000'
    end
    object taCMoveVNOUT: TFloatField
      FieldName = 'VNOUT'
      DisplayFormat = '0.000'
    end
    object taCMoveINV: TFloatField
      FieldName = 'INV'
      DisplayFormat = '0.000'
    end
    object taCMoveQREAL: TFloatField
      FieldName = 'QREAL'
      DisplayFormat = '0.000'
    end
    object taCMoveRE: TFloatField
      FieldName = 'RE'
      DisplayFormat = '0.000'
    end
  end
  object dstaCMove: TDataSource
    DataSet = taCMove
    Left = 104
    Top = 200
  end
  object quDocsInvSel: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHEADINV'
      'SET '
      '    DATEDOC = :DATEDOC,'
      '    NUMDOC = :NUMDOC,'
      '    IDSKL = :IDSKL,'
      '    IACTIVE = :IACTIVE,'
      '    ITYPE = :ITYPE,'
      '    SUM1 = :SUM1,'
      '    SUM11 = :SUM11,'
      '    SUM2 = :SUM2,'
      '    SUM21 = :SUM21'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHEADINV'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHEADINV('
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    IDSKL,'
      '    IACTIVE,'
      '    ITYPE,'
      '    SUM1,'
      '    SUM11,'
      '    SUM2,'
      '    SUM21'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATEDOC,'
      '    :NUMDOC,'
      '    :IDSKL,'
      '    :IACTIVE,'
      '    :ITYPE,'
      '    :SUM1,'
      '    :SUM11,'
      '    :SUM2,'
      '    :SUM21'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT IH.ID,IH.DATEDOC,IH.NUMDOC,IH.IDSKL,IH.IACTIVE,IH.ITYPE,I' +
        'H.SUM1,'
      
        '       IH.SUM11,IH.SUM2,IH.SUM21,(IH.SUM2-IH.SUM1) as SumDifIn,(' +
        'IH.SUM21-IH.SUM11) as SumDifUch,'
      '       mh.NAMEMH'
      'FROM OF_DOCHEADINV IH'
      'left join OF_MH mh on mh.ID=IH.IDSKL'
      ''
      'Where(  IH.DATEDOC>=:DATEB and IH.DATEDOC<:DATEE'
      '     ) and (     IH.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT IH.ID,IH.DATEDOC,IH.NUMDOC,IH.IDSKL,IH.IACTIVE,IH.ITYPE,I' +
        'H.SUM1,'
      
        '       IH.SUM11,IH.SUM2,IH.SUM21,(IH.SUM2-IH.SUM1) as SumDifIn,(' +
        'IH.SUM21-IH.SUM11) as SumDifUch,'
      '       mh.NAMEMH'
      'FROM OF_DOCHEADINV IH'
      'left join OF_MH mh on mh.ID=IH.IDSKL'
      ''
      'Where IH.DATEDOC>=:DATEB and IH.DATEDOC<:DATEE')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 432
    Top = 16
    poAskRecordCount = True
    object quDocsInvSelID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quDocsInvSelDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object quDocsInvSelNUMDOC: TFIBStringField
      FieldName = 'NUMDOC'
      Size = 15
      EmptyStrToNull = True
    end
    object quDocsInvSelIDSKL: TFIBIntegerField
      FieldName = 'IDSKL'
    end
    object quDocsInvSelNAMEMH: TFIBStringField
      FieldName = 'NAMEMH'
      Size = 100
      EmptyStrToNull = True
    end
    object quDocsInvSelIACTIVE: TFIBIntegerField
      FieldName = 'IACTIVE'
    end
    object quDocsInvSelITYPE: TFIBIntegerField
      FieldName = 'ITYPE'
    end
    object quDocsInvSelSUM1: TFIBFloatField
      FieldName = 'SUM1'
      DisplayFormat = '0.00'
    end
    object quDocsInvSelSUM11: TFIBFloatField
      FieldName = 'SUM11'
      DisplayFormat = '0.00'
    end
    object quDocsInvSelSUM2: TFIBFloatField
      FieldName = 'SUM2'
      DisplayFormat = '0.00'
    end
    object quDocsInvSelSUM21: TFIBFloatField
      FieldName = 'SUM21'
      DisplayFormat = '0.00'
    end
    object quDocsInvSelSUMDIFIN: TFIBFloatField
      FieldName = 'SUMDIFIN'
      DisplayFormat = '0.00'
    end
    object quDocsInvSelSUMDIFUCH: TFIBFloatField
      FieldName = 'SUMDIFUCH'
      DisplayFormat = '0.00'
    end
  end
  object dsquDocsInvSel: TDataSource
    DataSet = quDocsInvSel
    Left = 432
    Top = 72
  end
  object quDocsInvId: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHEADINV'
      'SET '
      '    DATEDOC = :DATEDOC,'
      '    NUMDOC = :NUMDOC,'
      '    IDSKL = :IDSKL,'
      '    IACTIVE = :IACTIVE,'
      '    ITYPE = :ITYPE,'
      '    SUM1 = :SUM1,'
      '    SUM11 = :SUM11,'
      '    SUM2 = :SUM2,'
      '    SUM21 = :SUM21'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHEADINV'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHEADINV('
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    IDSKL,'
      '    IACTIVE,'
      '    ITYPE,'
      '    SUM1,'
      '    SUM11,'
      '    SUM2,'
      '    SUM21'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATEDOC,'
      '    :NUMDOC,'
      '    :IDSKL,'
      '    :IACTIVE,'
      '    :ITYPE,'
      '    :SUM1,'
      '    :SUM11,'
      '    :SUM2,'
      '    :SUM21'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT IH.ID,IH.DATEDOC,IH.NUMDOC,IH.IDSKL,IH.IACTIVE,IH.ITYPE,I' +
        'H.SUM1,IH.SUM11,IH.SUM2,IH.SUM21'
      'FROM OF_DOCHEADINV IH'
      'Where(  IH.ID=:IDH'
      '     ) and (     IH.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT IH.ID,IH.DATEDOC,IH.NUMDOC,IH.IDSKL,IH.IACTIVE,IH.ITYPE,I' +
        'H.SUM1,IH.SUM11,IH.SUM2,IH.SUM21'
      'FROM OF_DOCHEADINV IH'
      'Where IH.ID=:IDH')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 520
    Top = 72
    poAskRecordCount = True
    object quDocsInvIdID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quDocsInvIdDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object quDocsInvIdNUMDOC: TFIBStringField
      FieldName = 'NUMDOC'
      Size = 15
      EmptyStrToNull = True
    end
    object quDocsInvIdIDSKL: TFIBIntegerField
      FieldName = 'IDSKL'
    end
    object quDocsInvIdIACTIVE: TFIBIntegerField
      FieldName = 'IACTIVE'
    end
    object quDocsInvIdITYPE: TFIBIntegerField
      FieldName = 'ITYPE'
    end
    object quDocsInvIdSUM1: TFIBFloatField
      FieldName = 'SUM1'
    end
    object quDocsInvIdSUM11: TFIBFloatField
      FieldName = 'SUM11'
    end
    object quDocsInvIdSUM2: TFIBFloatField
      FieldName = 'SUM2'
    end
    object quDocsInvIdSUM21: TFIBFloatField
      FieldName = 'SUM21'
    end
  end
  object quSpecInv: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCSPECINV'
      'SET '
      '    NUM = :NUM,'
      '    IDCARD = :IDCARD,'
      '    QUANT = :QUANT,'
      '    SUMIN = :SUMIN,'
      '    SUMUCH = :SUMUCH,'
      '    IDMESSURE = :IDMESSURE,'
      '    QUANTFACT = :QUANTFACT,'
      '    SUMINFACT = :SUMINFACT,'
      '    SUMUCHFACT = :SUMUCHFACT,'
      '    KM = :KM,'
      '    TCARD = :TCARD,'
      '    IDGROUP = :IDGROUP,'
      '    NOCALC = :NOCALC'
      'WHERE'
      '    IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCSPECINV'
      'WHERE'
      '        IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCSPECINV('
      '    IDHEAD,'
      '    ID,'
      '    NUM,'
      '    IDCARD,'
      '    QUANT,'
      '    SUMIN,'
      '    SUMUCH,'
      '    IDMESSURE,'
      '    QUANTFACT,'
      '    SUMINFACT,'
      '    SUMUCHFACT,'
      '    KM,'
      '    TCARD,'
      '    IDGROUP,'
      '    NOCALC'
      ')'
      'VALUES('
      '    :IDHEAD,'
      '    :ID,'
      '    :NUM,'
      '    :IDCARD,'
      '    :QUANT,'
      '    :SUMIN,'
      '    :SUMUCH,'
      '    :IDMESSURE,'
      '    :QUANTFACT,'
      '    :SUMINFACT,'
      '    :SUMUCHFACT,'
      '    :KM,'
      '    :TCARD,'
      '    :IDGROUP,'
      '    :NOCALC'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT s.IDHEAD,s.ID,s.NUM,s.IDCARD,s.QUANT,s.SUMIN,s.SUMUCH,s.I' +
        'DMESSURE,'
      
        's.QUANTFACT,s.SUMINFACT,s.SUMUCHFACT,s.KM,s.TCARD,s.IDGROUP,s.NO' +
        'CALC,'
      'me.NAMESHORT,cl.NAMECL,ca.NAME'
      'FROM OF_DOCSPECINV s'
      'left join OF_MESSUR me on me.ID=s.IDMESSURE'
      'left join OF_CLASSIF cl on cl.ID=s.IDGROUP'
      'left join of_cards ca on ca.ID=s.IDCARD'
      'where(  IDHEAD=:IDH'
      '     ) and (     S.IDHEAD = :OLD_IDHEAD'
      '    and S.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT s.IDHEAD,s.ID,s.NUM,s.IDCARD,s.QUANT,s.SUMIN,s.SUMUCH,s.I' +
        'DMESSURE,'
      
        's.QUANTFACT,s.SUMINFACT,s.SUMUCHFACT,s.KM,s.TCARD,s.IDGROUP,s.NO' +
        'CALC,'
      'me.NAMESHORT,cl.NAMECL,ca.NAME'
      'FROM OF_DOCSPECINV s'
      'left join OF_MESSUR me on me.ID=s.IDMESSURE'
      'left join OF_CLASSIF cl on cl.ID=s.IDGROUP'
      'left join of_cards ca on ca.ID=s.IDCARD'
      'where IDHEAD=:IDH')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 520
    Top = 16
    poAskRecordCount = True
    object quSpecInvIDHEAD: TFIBIntegerField
      FieldName = 'IDHEAD'
    end
    object quSpecInvID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quSpecInvNUM: TFIBIntegerField
      FieldName = 'NUM'
    end
    object quSpecInvIDCARD: TFIBIntegerField
      FieldName = 'IDCARD'
    end
    object quSpecInvQUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quSpecInvSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quSpecInvSUMUCH: TFIBFloatField
      FieldName = 'SUMUCH'
    end
    object quSpecInvIDMESSURE: TFIBIntegerField
      FieldName = 'IDMESSURE'
    end
    object quSpecInvQUANTFACT: TFIBFloatField
      FieldName = 'QUANTFACT'
    end
    object quSpecInvSUMINFACT: TFIBFloatField
      FieldName = 'SUMINFACT'
    end
    object quSpecInvSUMUCHFACT: TFIBFloatField
      FieldName = 'SUMUCHFACT'
    end
    object quSpecInvKM: TFIBFloatField
      FieldName = 'KM'
    end
    object quSpecInvTCARD: TFIBSmallIntField
      FieldName = 'TCARD'
    end
    object quSpecInvIDGROUP: TFIBIntegerField
      FieldName = 'IDGROUP'
    end
    object quSpecInvNAMESHORT: TFIBStringField
      FieldName = 'NAMESHORT'
      Size = 50
      EmptyStrToNull = True
    end
    object quSpecInvNAMECL: TFIBStringField
      FieldName = 'NAMECL'
      Size = 150
      EmptyStrToNull = True
    end
    object quSpecInvNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 200
      EmptyStrToNull = True
    end
    object quSpecInvNOCALC: TFIBSmallIntField
      FieldName = 'NOCALC'
    end
  end
  object quSpecInvC: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCSPECINVC'
      'SET '
      '    NUM = :NUM,'
      '    IDCARD = :IDCARD,'
      '    QUANT = :QUANT,'
      '    SUMIN = :SUMIN,'
      '    SUMUCH = :SUMUCH,'
      '    IDMESSURE = :IDMESSURE,'
      '    QUANTFACT = :QUANTFACT,'
      '    SUMINFACT = :SUMINFACT,'
      '    SUMUCHFACT = :SUMUCHFACT,'
      '    KM = :KM,'
      '    TCARD = :TCARD,'
      '    IDGROUP = :IDGROUP'
      'WHERE'
      '    IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCSPECINVC'
      'WHERE'
      '        IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCSPECINVC('
      '    IDHEAD,'
      '    ID,'
      '    NUM,'
      '    IDCARD,'
      '    QUANT,'
      '    SUMIN,'
      '    SUMUCH,'
      '    IDMESSURE,'
      '    QUANTFACT,'
      '    SUMINFACT,'
      '    SUMUCHFACT,'
      '    KM,'
      '    TCARD,'
      '    IDGROUP'
      ')'
      'VALUES('
      '    :IDHEAD,'
      '    :ID,'
      '    :NUM,'
      '    :IDCARD,'
      '    :QUANT,'
      '    :SUMIN,'
      '    :SUMUCH,'
      '    :IDMESSURE,'
      '    :QUANTFACT,'
      '    :SUMINFACT,'
      '    :SUMUCHFACT,'
      '    :KM,'
      '    :TCARD,'
      '    :IDGROUP'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT s.IDHEAD,s.ID,s.NUM,s.IDCARD,s.QUANT,s.SUMIN,s.SUMUCH,s.I' +
        'DMESSURE,'
      's.QUANTFACT,s.SUMINFACT,s.SUMUCHFACT,s.KM,s.TCARD,s.IDGROUP,'
      'me.NAMESHORT,cl.NAMECL,ca.NAME'
      'FROM OF_DOCSPECINVC s'
      'left join OF_MESSUR me on me.ID=s.IDMESSURE'
      'left join OF_CLASSIF cl on cl.ID=s.IDGROUP'
      'left join of_cards ca on ca.ID=s.IDCARD'
      'where(  IDHEAD=:IDH'
      '     ) and (     S.IDHEAD = :OLD_IDHEAD'
      '    and S.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT s.IDHEAD,s.ID,s.NUM,s.IDCARD,s.QUANT,s.SUMIN,s.SUMUCH,s.I' +
        'DMESSURE,'
      's.QUANTFACT,s.SUMINFACT,s.SUMUCHFACT,s.KM,s.TCARD,s.IDGROUP,'
      'me.NAMESHORT,cl.NAMECL,ca.NAME'
      'FROM OF_DOCSPECINVC s'
      'left join OF_MESSUR me on me.ID=s.IDMESSURE'
      'left join OF_CLASSIF cl on cl.ID=s.IDGROUP'
      'left join of_cards ca on ca.ID=s.IDCARD'
      'where IDHEAD=:IDH')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 592
    Top = 16
    poAskRecordCount = True
    object quSpecInvCIDHEAD: TFIBIntegerField
      FieldName = 'IDHEAD'
    end
    object quSpecInvCID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quSpecInvCNUM: TFIBIntegerField
      FieldName = 'NUM'
    end
    object quSpecInvCIDCARD: TFIBIntegerField
      FieldName = 'IDCARD'
    end
    object quSpecInvCQUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quSpecInvCSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quSpecInvCSUMUCH: TFIBFloatField
      FieldName = 'SUMUCH'
    end
    object quSpecInvCIDMESSURE: TFIBIntegerField
      FieldName = 'IDMESSURE'
    end
    object quSpecInvCQUANTFACT: TFIBFloatField
      FieldName = 'QUANTFACT'
    end
    object quSpecInvCSUMINFACT: TFIBFloatField
      FieldName = 'SUMINFACT'
    end
    object quSpecInvCSUMUCHFACT: TFIBFloatField
      FieldName = 'SUMUCHFACT'
    end
    object quSpecInvCKM: TFIBFloatField
      FieldName = 'KM'
    end
    object quSpecInvCTCARD: TFIBSmallIntField
      FieldName = 'TCARD'
    end
    object quSpecInvCIDGROUP: TFIBIntegerField
      FieldName = 'IDGROUP'
    end
    object quSpecInvCNAMESHORT: TFIBStringField
      FieldName = 'NAMESHORT'
      Size = 50
      EmptyStrToNull = True
    end
    object quSpecInvCNAMECL: TFIBStringField
      FieldName = 'NAMECL'
      Size = 150
      EmptyStrToNull = True
    end
    object quSpecInvCNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 200
      EmptyStrToNull = True
    end
  end
  object quSelPrib: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT cl.NAMECL,ca.PARENT,vr.CODEB,ca.NAME,'
      
        'SUM(vr.QUANT) as QUANT ,SUM(vr.SUMOUT)as SUMOUT,SUM(vr.SUMIN)as ' +
        'SUMIN'
      'FROM OF_VREALB vr'
      'left join OF_CARDS ca on ca.ID=vr.CODEB'
      'left join of_classif cl on cl.ID=ca.PARENT'
      'WHERE vr.DATEDOC>=:DATEB and vr.DATEDOC<:DATEE'
      'and vr.IDSKL=:IDSTORE'
      'GROUP by cl.NAMECL,ca.PARENT,vr.CODEB,ca.NAME'
      'ORDER by cl.NAMECL')
    Transaction = trSelRep
    Database = dmO.OfficeRnDb
    Left = 280
    Top = 136
    poAskRecordCount = True
    object quSelPribNAMECL: TFIBStringField
      FieldName = 'NAMECL'
      Size = 150
      EmptyStrToNull = True
    end
    object quSelPribPARENT: TFIBIntegerField
      FieldName = 'PARENT'
    end
    object quSelPribCODEB: TFIBIntegerField
      FieldName = 'CODEB'
    end
    object quSelPribNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 200
      EmptyStrToNull = True
    end
    object quSelPribQUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quSelPribSUMOUT: TFIBFloatField
      FieldName = 'SUMOUT'
    end
    object quSelPribSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
  end
  object quSelPribGr: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT cl.NAMECL,ca.PARENT,'
      
        'SUM(vr.QUANT) as QUANT ,SUM(vr.SUMOUT)as SUMOUT,SUM(vr.SUMIN)as ' +
        'SUMIN'
      'FROM OF_VREALB vr'
      'left join OF_CARDS ca on ca.ID=vr.CODEB'
      'left join of_classif cl on cl.ID=ca.PARENT'
      'WHERE vr.DATEDOC>=:DATEB and vr.DATEDOC<:DATEE'
      'and vr.IDSKL=:IDSTORE'
      'GROUP by cl.NAMECL,ca.PARENT')
    Transaction = trSelRep
    Database = dmO.OfficeRnDb
    Left = 352
    Top = 136
    poAskRecordCount = True
    object quSelPribGrNAMECL: TFIBStringField
      FieldName = 'NAMECL'
      Size = 150
      EmptyStrToNull = True
    end
    object quSelPribGrPARENT: TFIBIntegerField
      FieldName = 'PARENT'
    end
    object quSelPribGrQUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quSelPribGrSUMOUT: TFIBFloatField
      FieldName = 'SUMOUT'
    end
    object quSelPribGrSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
  end
  object trSelRep: TpFIBTransaction
    DefaultDatabase = dmO.OfficeRnDb
    TimeoutAction = TARollback
    Left = 208
    Top = 136
  end
  object quDocOutBPrint: TpFIBDataSet
    SelectSQL.Strings = (
      
        'SELECT vr.IDB,vr.CODEB,vr.QUANT,vr.SUMOUT,vr.IDSKL,vr.SUMIN,ds.N' +
        'AMEB,ds.PRICER'
      'FROM OF_VREALB_DOC vr'
      
        'left join of_docspecoutb ds on ((ds.IDHEAD=:IDH)and(ds.IDCARD=vr' +
        '.CODEB))')
    Transaction = trSelRep
    Database = dmO.OfficeRnDb
    Left = 280
    Top = 192
    poAskRecordCount = True
    object quDocOutBPrintIDB: TFIBIntegerField
      FieldName = 'IDB'
    end
    object quDocOutBPrintCODEB: TFIBIntegerField
      FieldName = 'CODEB'
    end
    object quDocOutBPrintQUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quDocOutBPrintSUMOUT: TFIBFloatField
      FieldName = 'SUMOUT'
    end
    object quDocOutBPrintIDSKL: TFIBIntegerField
      FieldName = 'IDSKL'
    end
    object quDocOutBPrintSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quDocOutBPrintNAMEB: TFIBStringField
      FieldName = 'NAMEB'
      Size = 100
      EmptyStrToNull = True
    end
    object quDocOutBPrintPRICER: TFIBFloatField
      FieldName = 'PRICER'
    end
  end
  object quSelInSum: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT ARTICUL,SUM(QPART*PRICEIN) as SUMIN,SUM(QPART) as QSUM'
      'FROM OF_PARTIN'
      'where IDATE>=:DATEB and IDATE<:DATEE and IDSTORE=:IDSKL'
      'Group by ARTICUL')
    Transaction = trSelRep
    Database = dmO.OfficeRnDb
    Left = 432
    Top = 136
    object quSelInSumARTICUL: TFIBIntegerField
      FieldName = 'ARTICUL'
    end
    object quSelInSumSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quSelInSumQSUM: TFIBFloatField
      FieldName = 'QSUM'
    end
  end
  object quSelNamePos: TpFIBDataSet
    SelectSQL.Strings = (
      
        'SELECT cl.NAMECL,ca.PARENT,ca.NAME,ME.NAMESHORT as SM,ME.KOEF,ca' +
        '.IMESSURE'
      'FROM OF_CARDS ca'
      'left join of_classif cl on cl.ID=ca.PARENT'
      'left join OF_MESSUR ME on ME.ID=ca.IMESSURE'
      'where ca.ID=:IDCARD')
    Transaction = trSelRep
    Database = dmO.OfficeRnDb
    Left = 504
    Top = 136
    poAskRecordCount = True
    object quSelNamePosNAMECL: TFIBStringField
      FieldName = 'NAMECL'
      Size = 150
      EmptyStrToNull = True
    end
    object quSelNamePosPARENT: TFIBIntegerField
      FieldName = 'PARENT'
    end
    object quSelNamePosNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 200
      EmptyStrToNull = True
    end
    object quSelNamePosSM: TFIBStringField
      FieldName = 'SM'
      Size = 50
      EmptyStrToNull = True
    end
    object quSelNamePosKOEF: TFIBFloatField
      FieldName = 'KOEF'
    end
    object quSelNamePosIMESSURE: TFIBIntegerField
      FieldName = 'IMESSURE'
    end
  end
  object quSelOutSum: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT ARTICUL,SUM(QUANT*PRICEIN) as SUMOUT,SUM(QUANT) as QSUM'
      'FROM OF_PARTOUT'
      'where IDDATE>=:DATEB and IDDATE<:DATEE and IDSTORE=:IDSKL'
      'Group by ARTICUL')
    Transaction = trSelRep
    Database = dmO.OfficeRnDb
    Left = 432
    Top = 200
    poAskRecordCount = True
    object quSelOutSumARTICUL: TFIBIntegerField
      FieldName = 'ARTICUL'
    end
    object quSelOutSumSUMOUT: TFIBFloatField
      FieldName = 'SUMOUT'
    end
    object quSelOutSumQSUM: TFIBFloatField
      FieldName = 'QSUM'
    end
  end
  object quSelNameCl: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT ca.PARENT, cl.NAMECL FROM OF_CARDS ca'
      'left join OF_Classif cl on cl.ID=ca.PARENT'
      'where ca.ID=:IDC')
    Transaction = dmO.trSelect
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    Left = 576
    Top = 136
    poAskRecordCount = True
    object quSelNameClPARENT: TFIBIntegerField
      FieldName = 'PARENT'
    end
    object quSelNameClNAMECL: TFIBStringField
      FieldName = 'NAMECL'
      Size = 150
      EmptyStrToNull = True
    end
  end
  object quSpecInvBC: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCSPECINVBC'
      'SET '
      '    CODEB = :CODEB,'
      '    NAMEB = :NAMEB,'
      '    QUANT = :QUANT,'
      '    PRICEOUT = :PRICEOUT,'
      '    SUMOUT = :SUMOUT,'
      '    IDCARD = :IDCARD,'
      '    NAMEC = :NAMEC,'
      '    QUANTC = :QUANTC,'
      '    PRICEIN = :PRICEIN,'
      '    SUMIN = :SUMIN,'
      '    IM = :IM,'
      '    SM = :SM,'
      '    SB = :SB'
      'WHERE'
      '    IDHEAD = :OLD_IDHEAD'
      '    and IDB = :OLD_IDB'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCSPECINVBC'
      'WHERE'
      '        IDHEAD = :OLD_IDHEAD'
      '    and IDB = :OLD_IDB'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCSPECINVBC('
      '    IDHEAD,'
      '    IDB,'
      '    ID,'
      '    CODEB,'
      '    NAMEB,'
      '    QUANT,'
      '    PRICEOUT,'
      '    SUMOUT,'
      '    IDCARD,'
      '    NAMEC,'
      '    QUANTC,'
      '    PRICEIN,'
      '    SUMIN,'
      '    IM,'
      '    SM,'
      '    SB'
      ')'
      'VALUES('
      '    :IDHEAD,'
      '    :IDB,'
      '    :ID,'
      '    :CODEB,'
      '    :NAMEB,'
      '    :QUANT,'
      '    :PRICEOUT,'
      '    :SUMOUT,'
      '    :IDCARD,'
      '    :NAMEC,'
      '    :QUANTC,'
      '    :PRICEIN,'
      '    :SUMIN,'
      '    :IM,'
      '    :SM,'
      '    :SB'
      ')')
    RefreshSQL.Strings = (
      'SELECT IDHEAD,IDB,ID,CODEB,NAMEB,QUANT,PRICEOUT,SUMOUT,'
      '       IDCARD,NAMEC,QUANTC,PRICEIN,SUMIN,IM,SM,SB'
      'FROM OF_DOCSPECINVBC'
      'where(  IDHEAD=:IDH'
      '     ) and (     OF_DOCSPECINVBC.IDHEAD = :OLD_IDHEAD'
      '    and OF_DOCSPECINVBC.IDB = :OLD_IDB'
      '    and OF_DOCSPECINVBC.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT IDHEAD,IDB,ID,CODEB,NAMEB,QUANT,PRICEOUT,SUMOUT,'
      '       IDCARD,NAMEC,QUANTC,PRICEIN,SUMIN,IM,SM,SB'
      'FROM OF_DOCSPECINVBC'
      'where IDHEAD=:IDH')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 664
    Top = 16
    poAskRecordCount = True
    object quSpecInvBCIDHEAD: TFIBIntegerField
      FieldName = 'IDHEAD'
    end
    object quSpecInvBCIDB: TFIBIntegerField
      FieldName = 'IDB'
    end
    object quSpecInvBCID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quSpecInvBCCODEB: TFIBIntegerField
      FieldName = 'CODEB'
    end
    object quSpecInvBCNAMEB: TFIBStringField
      FieldName = 'NAMEB'
      Size = 100
      EmptyStrToNull = True
    end
    object quSpecInvBCQUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quSpecInvBCPRICEOUT: TFIBFloatField
      FieldName = 'PRICEOUT'
    end
    object quSpecInvBCSUMOUT: TFIBFloatField
      FieldName = 'SUMOUT'
    end
    object quSpecInvBCIDCARD: TFIBIntegerField
      FieldName = 'IDCARD'
    end
    object quSpecInvBCNAMEC: TFIBStringField
      FieldName = 'NAMEC'
      Size = 100
      EmptyStrToNull = True
    end
    object quSpecInvBCQUANTC: TFIBFloatField
      FieldName = 'QUANTC'
    end
    object quSpecInvBCPRICEIN: TFIBFloatField
      FieldName = 'PRICEIN'
    end
    object quSpecInvBCSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quSpecInvBCIM: TFIBIntegerField
      FieldName = 'IM'
    end
    object quSpecInvBCSM: TFIBStringField
      FieldName = 'SM'
      EmptyStrToNull = True
    end
    object quSpecInvBCSB: TFIBStringField
      FieldName = 'SB'
      EmptyStrToNull = True
    end
  end
  object quDocsCompl: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHEADCOMPL'
      'SET '
      '    DATEDOC = :DATEDOC,'
      '    NUMDOC = :NUMDOC,'
      '    IDSKL = :IDSKL,'
      '    SUMIN = :SUMIN,'
      '    SUMUCH = :SUMUCH,'
      '    SUMTAR = :SUMTAR,'
      '    PROCNAC = :PROCNAC,'
      '    IACTIVE = :IACTIVE,'
      '    OPER = :OPER'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHEADCOMPL'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHEADCOMPL('
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    IDSKL,'
      '    SUMIN,'
      '    SUMUCH,'
      '    SUMTAR,'
      '    PROCNAC,'
      '    IACTIVE,'
      '    OPER'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATEDOC,'
      '    :NUMDOC,'
      '    :IDSKL,'
      '    :SUMIN,'
      '    :SUMUCH,'
      '    :SUMTAR,'
      '    :PROCNAC,'
      '    :IACTIVE,'
      '    :OPER'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT dh.ID,dh.DATEDOC,dh.NUMDOC,dh.IDSKL,dh.SUMIN,dh.SUMUCH,dh' +
        '.SUMTAR,'
      'dh.PROCNAC,dh.IACTIVE,dh.OPER,mh.NAMEMH'
      'FROM OF_DOCHEADCOMPL dh'
      'left join OF_MH mh on mh.ID=dh.IDSKL'
      'Where(  dh.DATEDOC>=:DATEB and dh.DATEDOC<:DATEE'
      '     ) and (     DH.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT dh.ID,dh.DATEDOC,dh.NUMDOC,dh.IDSKL,dh.SUMIN,dh.SUMUCH,dh' +
        '.SUMTAR,'
      'dh.PROCNAC,dh.IACTIVE,dh.OPER,mh.NAMEMH'
      'FROM OF_DOCHEADCOMPL dh'
      'left join OF_MH mh on mh.ID=dh.IDSKL'
      'Where dh.DATEDOC>=:DATEB and dh.DATEDOC<:DATEE')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 560
    Top = 216
    poAskRecordCount = True
    object quDocsComplID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quDocsComplDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object quDocsComplNUMDOC: TFIBStringField
      FieldName = 'NUMDOC'
      Size = 15
      EmptyStrToNull = True
    end
    object quDocsComplIDSKL: TFIBIntegerField
      FieldName = 'IDSKL'
    end
    object quDocsComplSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
      DisplayFormat = '0.00'
    end
    object quDocsComplSUMUCH: TFIBFloatField
      FieldName = 'SUMUCH'
      DisplayFormat = '0.00'
    end
    object quDocsComplSUMTAR: TFIBFloatField
      FieldName = 'SUMTAR'
      DisplayFormat = '0.00'
    end
    object quDocsComplPROCNAC: TFIBFloatField
      FieldName = 'PROCNAC'
      DisplayFormat = '0.0'
    end
    object quDocsComplIACTIVE: TFIBIntegerField
      FieldName = 'IACTIVE'
    end
    object quDocsComplOPER: TFIBStringField
      FieldName = 'OPER'
      EmptyStrToNull = True
    end
    object quDocsComplNAMEMH: TFIBStringField
      FieldName = 'NAMEMH'
      Size = 100
      EmptyStrToNull = True
    end
  end
  object dsquDocsCompl: TDataSource
    DataSet = quDocsCompl
    Left = 560
    Top = 272
  end
  object quDocsComlRec: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHEADCOMPL'
      'SET '
      '    DATEDOC = :DATEDOC,'
      '    NUMDOC = :NUMDOC,'
      '    IDSKL = :IDSKL,'
      '    SUMIN = :SUMIN,'
      '    SUMUCH = :SUMUCH,'
      '    SUMTAR = :SUMTAR,'
      '    PROCNAC = :PROCNAC,'
      '    IACTIVE = :IACTIVE,'
      '    OPER = :OPER'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHEADCOMPL'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHEADCOMPL('
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    IDSKL,'
      '    SUMIN,'
      '    SUMUCH,'
      '    SUMTAR,'
      '    PROCNAC,'
      '    IACTIVE,'
      '    OPER'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATEDOC,'
      '    :NUMDOC,'
      '    :IDSKL,'
      '    :SUMIN,'
      '    :SUMUCH,'
      '    :SUMTAR,'
      '    :PROCNAC,'
      '    :IACTIVE,'
      '    :OPER'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT ID,DATEDOC,NUMDOC,IDSKL,SUMIN,SUMUCH,SUMTAR,PROCNAC,IACTI' +
        'VE,OPER'
      'FROM OF_DOCHEADCOMPL'
      'Where(  ID=:IDH'
      '     ) and (     OF_DOCHEADCOMPL.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT ID,DATEDOC,NUMDOC,IDSKL,SUMIN,SUMUCH,SUMTAR,PROCNAC,IACTI' +
        'VE,OPER'
      'FROM OF_DOCHEADCOMPL'
      'Where ID=:IDH')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 648
    Top = 216
    poAskRecordCount = True
    object quDocsComlRecID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quDocsComlRecDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object quDocsComlRecNUMDOC: TFIBStringField
      FieldName = 'NUMDOC'
      Size = 15
      EmptyStrToNull = True
    end
    object quDocsComlRecIDSKL: TFIBIntegerField
      FieldName = 'IDSKL'
    end
    object quDocsComlRecSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quDocsComlRecSUMUCH: TFIBFloatField
      FieldName = 'SUMUCH'
    end
    object quDocsComlRecSUMTAR: TFIBFloatField
      FieldName = 'SUMTAR'
    end
    object quDocsComlRecPROCNAC: TFIBFloatField
      FieldName = 'PROCNAC'
    end
    object quDocsComlRecIACTIVE: TFIBIntegerField
      FieldName = 'IACTIVE'
    end
    object quDocsComlRecOPER: TFIBStringField
      FieldName = 'OPER'
      EmptyStrToNull = True
    end
  end
  object quSpecCompl: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCSPECCOMPLB'
      'SET '
      '    IDCARD = :IDCARD,'
      '    QUANT = :QUANT,'
      '    IDM = :IDM,'
      '    KM = :KM,'
      '    PRICEIN = :PRICEIN,'
      '    SUMIN = :SUMIN,'
      '    PRICEINUCH = :PRICEINUCH,'
      '    SUMINUCH = :SUMINUCH,'
      '    TCARD = :TCARD'
      'WHERE'
      '    IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCSPECCOMPLB'
      'WHERE'
      '        IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCSPECCOMPLB('
      '    IDHEAD,'
      '    ID,'
      '    IDCARD,'
      '    QUANT,'
      '    IDM,'
      '    KM,'
      '    PRICEIN,'
      '    SUMIN,'
      '    PRICEINUCH,'
      '    SUMINUCH,'
      '    TCARD'
      ')'
      'VALUES('
      '    :IDHEAD,'
      '    :ID,'
      '    :IDCARD,'
      '    :QUANT,'
      '    :IDM,'
      '    :KM,'
      '    :PRICEIN,'
      '    :SUMIN,'
      '    :PRICEINUCH,'
      '    :SUMINUCH,'
      '    :TCARD'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT s.IDHEAD,s.ID,s.IDCARD,s.QUANT,s.IDM,s.KM,s.PRICEIN,s.SUM' +
        'IN,s.PRICEINUCH,s.SUMINUCH,s.TCARD,'
      'me.NAMESHORT,ca.NAME'
      'FROM OF_DOCSPECCOMPLB s'
      'left join OF_MESSUR me on me.ID=s.IDM'
      'left join of_cards ca on ca.ID=s.IDCARD'
      'where(  IDHEAD=:IDH'
      '     ) and (     S.IDHEAD = :OLD_IDHEAD'
      '    and S.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT s.IDHEAD,s.ID,s.IDCARD,s.QUANT,s.IDM,s.KM,s.PRICEIN,s.SUM' +
        'IN,s.PRICEINUCH,s.SUMINUCH,s.TCARD,'
      'me.NAMESHORT,ca.NAME'
      'FROM OF_DOCSPECCOMPLB s'
      'left join OF_MESSUR me on me.ID=s.IDM'
      'left join of_cards ca on ca.ID=s.IDCARD'
      'where s.IDHEAD=:IDH')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 736
    Top = 216
    poAskRecordCount = True
    object quSpecComplIDHEAD: TFIBIntegerField
      FieldName = 'IDHEAD'
    end
    object quSpecComplID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quSpecComplIDCARD: TFIBIntegerField
      FieldName = 'IDCARD'
    end
    object quSpecComplQUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quSpecComplIDM: TFIBIntegerField
      FieldName = 'IDM'
    end
    object quSpecComplKM: TFIBFloatField
      FieldName = 'KM'
    end
    object quSpecComplPRICEIN: TFIBFloatField
      FieldName = 'PRICEIN'
    end
    object quSpecComplSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quSpecComplPRICEINUCH: TFIBFloatField
      FieldName = 'PRICEINUCH'
    end
    object quSpecComplSUMINUCH: TFIBFloatField
      FieldName = 'SUMINUCH'
    end
    object quSpecComplTCARD: TFIBSmallIntField
      FieldName = 'TCARD'
    end
    object quSpecComplNAMESHORT: TFIBStringField
      FieldName = 'NAMESHORT'
      Size = 50
      EmptyStrToNull = True
    end
    object quSpecComplNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 200
      EmptyStrToNull = True
    end
  end
  object quSpecComplC: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCSPECCOMPLC'
      'SET '
      '    IDCARD = :IDCARD,'
      '    QUANT = :QUANT,'
      '    IDM = :IDM,'
      '    KM = :KM,'
      '    PRICEIN = :PRICEIN,'
      '    SUMIN = :SUMIN,'
      '    PRICEINUCH = :PRICEINUCH,'
      '    SUMINUCH = :SUMINUCH'
      'WHERE'
      '    IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCSPECCOMPLC'
      'WHERE'
      '        IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCSPECCOMPLC('
      '    IDHEAD,'
      '    ID,'
      '    IDCARD,'
      '    QUANT,'
      '    IDM,'
      '    KM,'
      '    PRICEIN,'
      '    SUMIN,'
      '    PRICEINUCH,'
      '    SUMINUCH'
      ')'
      'VALUES('
      '    :IDHEAD,'
      '    :ID,'
      '    :IDCARD,'
      '    :QUANT,'
      '    :IDM,'
      '    :KM,'
      '    :PRICEIN,'
      '    :SUMIN,'
      '    :PRICEINUCH,'
      '    :SUMINUCH'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT s.IDHEAD,s.ID,s.IDCARD,s.QUANT,s.IDM,s.KM,s.PRICEIN,s.SUM' +
        'IN,s.PRICEINUCH,s.SUMINUCH,'
      'me.NAMESHORT,ca.NAME'
      'FROM OF_DOCSPECCOMPLC s'
      'left join OF_MESSUR me on me.ID=s.IDM'
      'left join of_cards ca on ca.ID=s.IDCARD'
      'where(  s.IDHEAD=:IDH'
      '     ) and (     S.IDHEAD = :OLD_IDHEAD'
      '    and S.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT s.IDHEAD,s.ID,s.IDCARD,s.QUANT,s.IDM,s.KM,s.PRICEIN,s.SUM' +
        'IN,s.PRICEINUCH,s.SUMINUCH,'
      'me.NAMESHORT,ca.NAME'
      'FROM OF_DOCSPECCOMPLC s'
      'left join OF_MESSUR me on me.ID=s.IDM'
      'left join of_cards ca on ca.ID=s.IDCARD'
      'where s.IDHEAD=:IDH')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 736
    Top = 272
    poAskRecordCount = True
    object quSpecComplCIDHEAD: TFIBIntegerField
      FieldName = 'IDHEAD'
    end
    object quSpecComplCID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quSpecComplCIDCARD: TFIBIntegerField
      FieldName = 'IDCARD'
    end
    object quSpecComplCQUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quSpecComplCIDM: TFIBIntegerField
      FieldName = 'IDM'
    end
    object quSpecComplCKM: TFIBFloatField
      FieldName = 'KM'
    end
    object quSpecComplCPRICEIN: TFIBFloatField
      FieldName = 'PRICEIN'
    end
    object quSpecComplCSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quSpecComplCPRICEINUCH: TFIBFloatField
      FieldName = 'PRICEINUCH'
    end
    object quSpecComplCSUMINUCH: TFIBFloatField
      FieldName = 'SUMINUCH'
    end
    object quSpecComplCNAMESHORT: TFIBStringField
      FieldName = 'NAMESHORT'
      Size = 50
      EmptyStrToNull = True
    end
    object quSpecComplCNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 200
      EmptyStrToNull = True
    end
  end
  object quSpecComplCB: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCSPECCOMPLCB'
      'SET '
      '    CODEB = :CODEB,'
      '    NAMEB = :NAMEB,'
      '    QUANT = :QUANT,'
      '    IDCARD = :IDCARD,'
      '    NAMEC = :NAMEC,'
      '    QUANTC = :QUANTC,'
      '    PRICEIN = :PRICEIN,'
      '    SUMIN = :SUMIN,'
      '    IM = :IM,'
      '    SM = :SM,'
      '    SB = :SB'
      'WHERE'
      '    IDHEAD = :OLD_IDHEAD'
      '    and IDB = :OLD_IDB'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCSPECCOMPLCB'
      'WHERE'
      '        IDHEAD = :OLD_IDHEAD'
      '    and IDB = :OLD_IDB'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCSPECCOMPLCB('
      '    IDHEAD,'
      '    IDB,'
      '    ID,'
      '    CODEB,'
      '    NAMEB,'
      '    QUANT,'
      '    IDCARD,'
      '    NAMEC,'
      '    QUANTC,'
      '    PRICEIN,'
      '    SUMIN,'
      '    IM,'
      '    SM,'
      '    SB'
      ')'
      'VALUES('
      '    :IDHEAD,'
      '    :IDB,'
      '    :ID,'
      '    :CODEB,'
      '    :NAMEB,'
      '    :QUANT,'
      '    :IDCARD,'
      '    :NAMEC,'
      '    :QUANTC,'
      '    :PRICEIN,'
      '    :SUMIN,'
      '    :IM,'
      '    :SM,'
      '    :SB'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT IDHEAD,IDB,ID,CODEB,NAMEB,QUANT,IDCARD,NAMEC,QUANTC,PRICE' +
        'IN,SUMIN,IM,SM,SB'
      'FROM OF_DOCSPECCOMPLCB '
      'where(  IDHEAD=:IDH'
      '     ) and (     OF_DOCSPECCOMPLCB.IDHEAD = :OLD_IDHEAD'
      '    and OF_DOCSPECCOMPLCB.IDB = :OLD_IDB'
      '    and OF_DOCSPECCOMPLCB.ID = :OLD_ID'
      '     )'
      '    '
      '')
    SelectSQL.Strings = (
      
        'SELECT IDHEAD,IDB,ID,CODEB,NAMEB,QUANT,IDCARD,NAMEC,QUANTC,PRICE' +
        'IN,SUMIN,IM,SM,SB'
      'FROM OF_DOCSPECCOMPLCB '
      'where IDHEAD=:IDH'
      '')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 736
    Top = 328
    poAskRecordCount = True
    object quSpecComplCBIDHEAD: TFIBIntegerField
      FieldName = 'IDHEAD'
    end
    object quSpecComplCBIDB: TFIBIntegerField
      FieldName = 'IDB'
    end
    object quSpecComplCBID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quSpecComplCBCODEB: TFIBIntegerField
      FieldName = 'CODEB'
    end
    object quSpecComplCBNAMEB: TFIBStringField
      FieldName = 'NAMEB'
      Size = 100
      EmptyStrToNull = True
    end
    object quSpecComplCBQUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quSpecComplCBIDCARD: TFIBIntegerField
      FieldName = 'IDCARD'
    end
    object quSpecComplCBNAMEC: TFIBStringField
      FieldName = 'NAMEC'
      Size = 100
      EmptyStrToNull = True
    end
    object quSpecComplCBQUANTC: TFIBFloatField
      FieldName = 'QUANTC'
    end
    object quSpecComplCBPRICEIN: TFIBFloatField
      FieldName = 'PRICEIN'
    end
    object quSpecComplCBSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quSpecComplCBIM: TFIBIntegerField
      FieldName = 'IM'
    end
    object quSpecComplCBSM: TFIBStringField
      FieldName = 'SM'
      EmptyStrToNull = True
    end
    object quSpecComplCBSB: TFIBStringField
      FieldName = 'SB'
      EmptyStrToNull = True
    end
  end
  object taHeadDoc: TClientDataSet
    Aggregates = <>
    FileName = 'HeadDoc.cds'
    Params = <>
    Left = 32
    Top = 264
    object taHeadDocIType: TSmallintField
      FieldName = 'IType'
    end
    object taHeadDocId: TIntegerField
      FieldName = 'Id'
    end
    object taHeadDocDateDoc: TIntegerField
      FieldName = 'DateDoc'
    end
    object taHeadDocNumDoc: TStringField
      FieldName = 'NumDoc'
      Size = 15
    end
    object taHeadDocIdCli: TIntegerField
      FieldName = 'IdCli'
    end
    object taHeadDocNameCli: TStringField
      FieldName = 'NameCli'
      Size = 70
    end
    object taHeadDocIdSkl: TIntegerField
      FieldName = 'IdSkl'
    end
    object taHeadDocNameSkl: TStringField
      FieldName = 'NameSkl'
      Size = 70
    end
    object taHeadDocSumIN: TFloatField
      FieldName = 'SumIN'
      DisplayFormat = '0.00'
    end
    object taHeadDocSumUch: TFloatField
      FieldName = 'SumUch'
      DisplayFormat = '0.00'
    end
  end
  object taSpecDoc: TClientDataSet
    Aggregates = <>
    FileName = 'SpecDoc.cds'
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'taSpecDocIndex1'
        Fields = 'IType;IdHead;Num'
      end>
    IndexFieldNames = 'IType;IdHead;Num'
    MasterSource = dsHeadDoc
    PacketRecords = 0
    Params = <>
    StoreDefs = True
    Left = 104
    Top = 264
    object taSpecDocIType: TSmallintField
      FieldName = 'IType'
    end
    object taSpecDocIdHead: TIntegerField
      FieldName = 'IdHead'
    end
    object taSpecDocNum: TIntegerField
      FieldName = 'Num'
    end
    object taSpecDocIdCard: TIntegerField
      FieldName = 'IdCard'
    end
    object taSpecDocQuant: TFloatField
      FieldName = 'Quant'
      DisplayFormat = '0.###'
    end
    object taSpecDocPriceIn: TFloatField
      FieldName = 'PriceIn'
      DisplayFormat = '0.00'
    end
    object taSpecDocSumIn: TFloatField
      FieldName = 'SumIn'
      DisplayFormat = '0.00'
    end
    object taSpecDocPriceUch: TFloatField
      FieldName = 'PriceUch'
      DisplayFormat = '0.00'
    end
    object taSpecDocSumUch: TFloatField
      FieldName = 'SumUch'
      DisplayFormat = '0.00'
    end
    object taSpecDocIdNds: TIntegerField
      FieldName = 'IdNds'
    end
    object taSpecDocSumNds: TFloatField
      FieldName = 'SumNds'
    end
    object taSpecDocNameC: TStringField
      FieldName = 'NameC'
      Size = 30
    end
    object taSpecDocSm: TStringField
      FieldName = 'Sm'
      Size = 15
    end
    object taSpecDocIdM: TIntegerField
      FieldName = 'IdM'
    end
    object taSpecDocKm: TFloatField
      FieldName = 'Km'
    end
    object taSpecDocPriceUch1: TFloatField
      FieldName = 'PriceUch1'
    end
    object taSpecDocSumUch1: TFloatField
      FieldName = 'SumUch1'
    end
  end
  object dsHeadDoc: TDataSource
    DataSet = taHeadDoc
    Left = 32
    Top = 320
  end
  object dsSpecDoc: TDataSource
    DataSet = taSpecDoc
    Left = 104
    Top = 320
  end
  object quDocsActs: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHEADACTS'
      'SET '
      '    DATEDOC = :DATEDOC,'
      '    NUMDOC = :NUMDOC,'
      '    IDSKL = :IDSKL,'
      '    SUMIN = :SUMIN,'
      '    SUMUCH = :SUMUCH,'
      '    IACTIVE = :IACTIVE,'
      '    OPER = :OPER'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHEADACTS'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHEADACTS('
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    IDSKL,'
      '    SUMIN,'
      '    SUMUCH,'
      '    IACTIVE,'
      '    OPER'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATEDOC,'
      '    :NUMDOC,'
      '    :IDSKL,'
      '    :SUMIN,'
      '    :SUMUCH,'
      '    :IACTIVE,'
      '    :OPER'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT AH.ID,AH.DATEDOC,AH.NUMDOC,AH.IDSKL,AH.SUMIN,AH.SUMUCH,AH' +
        '.IACTIVE,AH.OPER'
      'FROM OF_DOCHEADACTS AH'
      'left join OF_MH mh on mh.ID=AH.IDSKL'
      'Where(  AH.DATEDOC>=:DATEB and AH.DATEDOC<:DATEE'
      '     ) and (     AH.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT AH.ID,AH.DATEDOC,AH.NUMDOC,AH.IDSKL,AH.SUMIN,AH.SUMUCH,AH' +
        '.IACTIVE,AH.OPER,mh.NAMEMH'
      'FROM OF_DOCHEADACTS AH'
      'left join OF_MH mh on mh.ID=AH.IDSKL'
      'Where AH.DATEDOC>=:DATEB and AH.DATEDOC<:DATEE')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 216
    Top = 264
    poAskRecordCount = True
    object quDocsActsID2: TFIBIntegerField
      FieldName = 'ID'
    end
    object quDocsActsDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object quDocsActsNUMDOC: TFIBStringField
      FieldName = 'NUMDOC'
      Size = 15
      EmptyStrToNull = True
    end
    object quDocsActsIDSKL: TFIBIntegerField
      FieldName = 'IDSKL'
    end
    object quDocsActsSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
      DisplayFormat = '0.00'
    end
    object quDocsActsSUMUCH: TFIBFloatField
      FieldName = 'SUMUCH'
      DisplayFormat = '0.00'
    end
    object quDocsActsIACTIVE: TFIBIntegerField
      FieldName = 'IACTIVE'
    end
    object quDocsActsOPER: TFIBStringField
      FieldName = 'OPER'
      EmptyStrToNull = True
    end
    object quDocsActsNAMEMH: TFIBStringField
      FieldName = 'NAMEMH'
      Size = 100
      EmptyStrToNull = True
    end
  end
  object dsquDocsActs: TDataSource
    DataSet = quDocsActs
    Left = 216
    Top = 312
  end
  object quDocsActsId: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHEADACTS'
      'SET '
      '    DATEDOC = :DATEDOC,'
      '    NUMDOC = :NUMDOC,'
      '    IDSKL = :IDSKL,'
      '    SUMIN = :SUMIN,'
      '    SUMUCH = :SUMUCH,'
      '    IACTIVE = :IACTIVE,'
      '    OPER = :OPER'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHEADACTS'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHEADACTS('
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    IDSKL,'
      '    SUMIN,'
      '    SUMUCH,'
      '    IACTIVE,'
      '    OPER'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATEDOC,'
      '    :NUMDOC,'
      '    :IDSKL,'
      '    :SUMIN,'
      '    :SUMUCH,'
      '    :IACTIVE,'
      '    :OPER'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT AH.ID,AH.DATEDOC,AH.NUMDOC,AH.IDSKL,AH.SUMIN,AH.SUMUCH,AH' +
        '.IACTIVE,AH.OPER'
      'FROM OF_DOCHEADACTS AH'
      'left join OF_MH mh on mh.ID=AH.IDSKL'
      'Where(  AH.ID=:IDH'
      '     ) and (     AH.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT AH.ID,AH.DATEDOC,AH.NUMDOC,AH.IDSKL,AH.SUMIN,AH.SUMUCH,AH' +
        '.IACTIVE,AH.OPER'
      'FROM OF_DOCHEADACTS AH'
      'left join OF_MH mh on mh.ID=AH.IDSKL'
      'Where AH.ID=:IDH')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 296
    Top = 264
    poAskRecordCount = True
    object quDocsActsIdID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quDocsActsIdDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object quDocsActsIdNUMDOC: TFIBStringField
      FieldName = 'NUMDOC'
      Size = 15
      EmptyStrToNull = True
    end
    object quDocsActsIdIDSKL: TFIBIntegerField
      FieldName = 'IDSKL'
    end
    object quDocsActsIdSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quDocsActsIdSUMUCH: TFIBFloatField
      FieldName = 'SUMUCH'
    end
    object quDocsActsIdIACTIVE: TFIBIntegerField
      FieldName = 'IACTIVE'
    end
    object quDocsActsIdOPER: TFIBStringField
      FieldName = 'OPER'
      EmptyStrToNull = True
    end
  end
  object quDocsVnSel: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHEADVN'
      'SET '
      '    DATEDOC = :DATEDOC,'
      '    NUMDOC = :NUMDOC,'
      '    IDSKL_FROM = :IDSKL_FROM,'
      '    IDSKL_TO = :IDSKL_TO,'
      '    SUMIN = :SUMIN,'
      '    SUMUCH = :SUMUCH,'
      '    SUMUCH1 = :SUMUCH1,'
      '    SUMTAR = :SUMTAR,'
      '    PROCNAC = :PROCNAC,'
      '    IACTIVE = :IACTIVE'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHEADVN'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHEADVN('
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    IDSKL_FROM,'
      '    IDSKL_TO,'
      '    SUMIN,'
      '    SUMUCH,'
      '    SUMUCH1,'
      '    SUMTAR,'
      '    PROCNAC,'
      '    IACTIVE'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATEDOC,'
      '    :NUMDOC,'
      '    :IDSKL_FROM,'
      '    :IDSKL_TO,'
      '    :SUMIN,'
      '    :SUMUCH,'
      '    :SUMUCH1,'
      '    :SUMTAR,'
      '    :PROCNAC,'
      '    :IACTIVE'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT dh.ID,dh.DATEDOC,dh.NUMDOC,dh.IDSKL_FROM,dh.IDSKL_TO,dh.S' +
        'UMIN,'
      '       dh.SUMUCH,dh.SUMUCH1,dh.SUMTAR,dh.PROCNAC,dh.IACTIVE,'
      '       mh_f.NAMEMH, mh_t.NAMEMH'
      'FROM OF_DOCHEADVN dh'
      'left join OF_MH mh_f on mh_f.ID=dh.IDSKL_FROM'
      'left join OF_MH mh_t on mh_t.ID=dh.IDSKL_TO'
      ''
      'Where(  dh.DATEDOC>=:DATEB and dh.DATEDOC<:DATEE'
      '     ) and (     DH.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT dh.ID,dh.DATEDOC,dh.NUMDOC,dh.IDSKL_FROM,dh.IDSKL_TO,dh.S' +
        'UMIN,'
      '       dh.SUMUCH,dh.SUMUCH1,dh.SUMTAR,dh.PROCNAC,dh.IACTIVE,'
      '       mh_f.NAMEMH, mh_t.NAMEMH'
      'FROM OF_DOCHEADVN dh'
      'left join OF_MH mh_f on mh_f.ID=dh.IDSKL_FROM'
      'left join OF_MH mh_t on mh_t.ID=dh.IDSKL_TO'
      ''
      'Where dh.DATEDOC>=:DATEB and dh.DATEDOC<:DATEE')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 296
    Top = 368
    poAskRecordCount = True
    object quDocsVnSelID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quDocsVnSelDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object quDocsVnSelNUMDOC: TFIBStringField
      FieldName = 'NUMDOC'
      Size = 15
      EmptyStrToNull = True
    end
    object quDocsVnSelIDSKL_FROM: TFIBIntegerField
      FieldName = 'IDSKL_FROM'
    end
    object quDocsVnSelIDSKL_TO: TFIBIntegerField
      FieldName = 'IDSKL_TO'
    end
    object quDocsVnSelSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
      DisplayFormat = '0.00'
    end
    object quDocsVnSelSUMUCH: TFIBFloatField
      FieldName = 'SUMUCH'
      DisplayFormat = '0.00'
    end
    object quDocsVnSelSUMUCH1: TFIBFloatField
      FieldName = 'SUMUCH1'
      DisplayFormat = '0.00'
    end
    object quDocsVnSelSUMTAR: TFIBFloatField
      FieldName = 'SUMTAR'
    end
    object quDocsVnSelPROCNAC: TFIBFloatField
      FieldName = 'PROCNAC'
    end
    object quDocsVnSelIACTIVE: TFIBIntegerField
      FieldName = 'IACTIVE'
    end
    object quDocsVnSelNAMEMH: TFIBStringField
      FieldName = 'NAMEMH'
      Size = 100
      EmptyStrToNull = True
    end
    object quDocsVnSelNAMEMH1: TFIBStringField
      FieldName = 'NAMEMH1'
      Size = 100
      EmptyStrToNull = True
    end
  end
  object dsDocsVnSel: TDataSource
    DataSet = quDocsVnSel
    Left = 296
    Top = 416
  end
  object quSpecVnSel: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCSPECVN'
      'SET '
      '    NUM = :NUM,'
      '    IDCARD = :IDCARD,'
      '    QUANT = :QUANT,'
      '    PRICEIN = :PRICEIN,'
      '    SUMIN = :SUMIN,'
      '    PRICEUCH = :PRICEUCH,'
      '    SUMUCH = :SUMUCH,'
      '    PRICEUCH1 = :PRICEUCH1,'
      '    SUMUCH1 = :SUMUCH1,'
      '    IDM = :IDM,'
      '    KM = :KM'
      'WHERE'
      '    IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCSPECVN'
      'WHERE'
      '        IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCSPECVN('
      '    IDHEAD,'
      '    ID,'
      '    NUM,'
      '    IDCARD,'
      '    QUANT,'
      '    PRICEIN,'
      '    SUMIN,'
      '    PRICEUCH,'
      '    SUMUCH,'
      '    PRICEUCH1,'
      '    SUMUCH1,'
      '    IDM,'
      '    KM'
      ')'
      'VALUES('
      '    :IDHEAD,'
      '    :ID,'
      '    :NUM,'
      '    :IDCARD,'
      '    :QUANT,'
      '    :PRICEIN,'
      '    :SUMIN,'
      '    :PRICEUCH,'
      '    :SUMUCH,'
      '    :PRICEUCH1,'
      '    :SUMUCH1,'
      '    :IDM,'
      '    :KM'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT SP.IDHEAD,SP.ID,SP.NUM,SP.IDCARD,SP.QUANT,SP.PRICEIN,SP.S' +
        'UMIN,'
      'SP.PRICEUCH,SP.SUMUCH,SP.PRICEUCH1,SP.SUMUCH1,SP.IDM,SP.KM,'
      'C.NAME as NAMEC,M.NAMESHORT as SM'
      'FROM OF_DOCSPECVN SP'
      'left join OF_CARDS C on C.ID=SP.IDCARD'
      'left join OF_MESSUR M on M.ID=SP.IDM'
      ''
      'where(  SP.IDHEAD=:IDHD'
      '     ) and (     SP.IDHEAD = :OLD_IDHEAD'
      '    and SP.ID = :OLD_ID'
      '     )'
      '    '
      '')
    SelectSQL.Strings = (
      
        'SELECT SP.IDHEAD,SP.ID,SP.NUM,SP.IDCARD,SP.QUANT,SP.PRICEIN,SP.S' +
        'UMIN,'
      'SP.PRICEUCH,SP.SUMUCH,SP.PRICEUCH1,SP.SUMUCH1,SP.IDM,SP.KM,'
      'C.NAME as NAMEC,M.NAMESHORT as SM'
      'FROM OF_DOCSPECVN SP'
      'left join OF_CARDS C on C.ID=SP.IDCARD'
      'left join OF_MESSUR M on M.ID=SP.IDM'
      ''
      'where SP.IDHEAD=:IDHD'
      ''
      'ORDER BY SP.NUM')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 296
    Top = 464
    object quSpecVnSelIDHEAD: TFIBIntegerField
      FieldName = 'IDHEAD'
    end
    object quSpecVnSelID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quSpecVnSelNUM: TFIBIntegerField
      FieldName = 'NUM'
    end
    object quSpecVnSelIDCARD: TFIBIntegerField
      FieldName = 'IDCARD'
    end
    object quSpecVnSelQUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quSpecVnSelPRICEIN: TFIBFloatField
      FieldName = 'PRICEIN'
    end
    object quSpecVnSelSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quSpecVnSelPRICEUCH: TFIBFloatField
      FieldName = 'PRICEUCH'
    end
    object quSpecVnSelSUMUCH: TFIBFloatField
      FieldName = 'SUMUCH'
    end
    object quSpecVnSelPRICEUCH1: TFIBFloatField
      FieldName = 'PRICEUCH1'
    end
    object quSpecVnSelSUMUCH1: TFIBFloatField
      FieldName = 'SUMUCH1'
    end
    object quSpecVnSelIDM: TFIBIntegerField
      FieldName = 'IDM'
    end
    object quSpecVnSelNAMEC: TFIBStringField
      FieldName = 'NAMEC'
      Size = 200
      EmptyStrToNull = True
    end
    object quSpecVnSelSM: TFIBStringField
      FieldName = 'SM'
      Size = 50
      EmptyStrToNull = True
    end
    object quSpecVnSelKM: TFIBFloatField
      FieldName = 'KM'
    end
  end
  object quDocsVnId: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHEADVN'
      'SET '
      '    DATEDOC = :DATEDOC,'
      '    NUMDOC = :NUMDOC,'
      '    IDSKL_FROM = :IDSKL_FROM,'
      '    IDSKL_TO = :IDSKL_TO,'
      '    SUMIN = :SUMIN,'
      '    SUMUCH = :SUMUCH,'
      '    SUMUCH1 = :SUMUCH1,'
      '    SUMTAR = :SUMTAR,'
      '    PROCNAC = :PROCNAC,'
      '    IACTIVE = :IACTIVE'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHEADVN'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHEADVN('
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    IDSKL_FROM,'
      '    IDSKL_TO,'
      '    SUMIN,'
      '    SUMUCH,'
      '    SUMUCH1,'
      '    SUMTAR,'
      '    PROCNAC,'
      '    IACTIVE'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATEDOC,'
      '    :NUMDOC,'
      '    :IDSKL_FROM,'
      '    :IDSKL_TO,'
      '    :SUMIN,'
      '    :SUMUCH,'
      '    :SUMUCH1,'
      '    :SUMTAR,'
      '    :PROCNAC,'
      '    :IACTIVE'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    IDSKL_FROM,'
      '    IDSKL_TO,'
      '    SUMIN,'
      '    SUMUCH,'
      '    SUMUCH1,'
      '    SUMTAR,'
      '    PROCNAC,'
      '    IACTIVE'
      'FROM'
      '    OF_DOCHEADVN '
      'Where(  ID=:IDH'
      '     ) and (     OF_DOCHEADVN.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    IDSKL_FROM,'
      '    IDSKL_TO,'
      '    SUMIN,'
      '    SUMUCH,'
      '    SUMUCH1,'
      '    SUMTAR,'
      '    PROCNAC,'
      '    IACTIVE'
      'FROM'
      '    OF_DOCHEADVN '
      'Where ID=:IDH')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 216
    Top = 368
    poAskRecordCount = True
    object quDocsVnIdID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quDocsVnIdDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object quDocsVnIdNUMDOC: TFIBStringField
      FieldName = 'NUMDOC'
      Size = 15
      EmptyStrToNull = True
    end
    object quDocsVnIdIDSKL_FROM: TFIBIntegerField
      FieldName = 'IDSKL_FROM'
    end
    object quDocsVnIdIDSKL_TO: TFIBIntegerField
      FieldName = 'IDSKL_TO'
    end
    object quDocsVnIdSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quDocsVnIdSUMUCH: TFIBFloatField
      FieldName = 'SUMUCH'
    end
    object quDocsVnIdSUMUCH1: TFIBFloatField
      FieldName = 'SUMUCH1'
    end
    object quDocsVnIdSUMTAR: TFIBFloatField
      FieldName = 'SUMTAR'
    end
    object quDocsVnIdPROCNAC: TFIBFloatField
      FieldName = 'PROCNAC'
    end
    object quDocsVnIdIACTIVE: TFIBIntegerField
      FieldName = 'IACTIVE'
    end
  end
  object quSpecAO: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCSPECACTSOUT'
      'SET '
      '    IDCARD = :IDCARD,'
      '    QUANT = :QUANT,'
      '    IDM = :IDM,'
      '    KM = :KM,'
      '    PRICEIN = :PRICEIN,'
      '    SUMIN = :SUMIN,'
      '    PRICEINUCH = :PRICEINUCH,'
      '    SUMINUCH = :SUMINUCH,'
      '    TCARD = :TCARD'
      'WHERE'
      '    IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCSPECACTSOUT'
      'WHERE'
      '        IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCSPECACTSOUT('
      '    IDHEAD,'
      '    ID,'
      '    IDCARD,'
      '    QUANT,'
      '    IDM,'
      '    KM,'
      '    PRICEIN,'
      '    SUMIN,'
      '    PRICEINUCH,'
      '    SUMINUCH,'
      '    TCARD'
      ')'
      'VALUES('
      '    :IDHEAD,'
      '    :ID,'
      '    :IDCARD,'
      '    :QUANT,'
      '    :IDM,'
      '    :KM,'
      '    :PRICEIN,'
      '    :SUMIN,'
      '    :PRICEINUCH,'
      '    :SUMINUCH,'
      '    :TCARD'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT so.IDHEAD,so.ID,so.IDCARD,so.QUANT,so.IDM,so.KM,so.PRICEI' +
        'N,so.SUMIN,so.PRICEINUCH,so.SUMINUCH,so.TCARD,'
      'me.NAMESHORT,ca.NAME'
      'FROM OF_DOCSPECACTSOUT so'
      'left join OF_MESSUR me on me.ID=so.IDM'
      'left join of_cards ca on ca.ID=so.IDCARD'
      'where(  IDHEAD=:IDH'
      '     ) and (     SO.IDHEAD = :OLD_IDHEAD'
      '    and SO.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT so.IDHEAD,so.ID,so.IDCARD,so.QUANT,so.IDM,so.KM,so.PRICEI' +
        'N,so.SUMIN,so.PRICEINUCH,so.SUMINUCH,so.TCARD,'
      'me.NAMESHORT,ca.NAME'
      'FROM OF_DOCSPECACTSOUT so'
      'left join OF_MESSUR me on me.ID=so.IDM'
      'left join of_cards ca on ca.ID=so.IDCARD'
      'where IDHEAD=:IDH'
      'Order by so.ID')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 376
    Top = 264
    poAskRecordCount = True
    object quSpecAOIDHEAD: TFIBIntegerField
      FieldName = 'IDHEAD'
    end
    object quSpecAOID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quSpecAOIDCARD: TFIBIntegerField
      FieldName = 'IDCARD'
    end
    object quSpecAOQUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quSpecAOIDM: TFIBIntegerField
      FieldName = 'IDM'
    end
    object quSpecAOKM: TFIBFloatField
      FieldName = 'KM'
    end
    object quSpecAOPRICEIN: TFIBFloatField
      FieldName = 'PRICEIN'
    end
    object quSpecAOSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quSpecAOPRICEINUCH: TFIBFloatField
      FieldName = 'PRICEINUCH'
    end
    object quSpecAOSUMINUCH: TFIBFloatField
      FieldName = 'SUMINUCH'
    end
    object quSpecAOTCARD: TFIBSmallIntField
      FieldName = 'TCARD'
    end
    object quSpecAONAMESHORT: TFIBStringField
      FieldName = 'NAMESHORT'
      Size = 50
      EmptyStrToNull = True
    end
    object quSpecAONAME: TFIBStringField
      FieldName = 'NAME'
      Size = 200
      EmptyStrToNull = True
    end
  end
  object quSpecAI: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCSPECACTSIN'
      'SET '
      '    IDCARD = :IDCARD,'
      '    QUANT = :QUANT,'
      '    IDM = :IDM,'
      '    KM = :KM,'
      '    PRICEIN = :PRICEIN,'
      '    SUMIN = :SUMIN,'
      '    PRICEINUCH = :PRICEINUCH,'
      '    SUMINUCH = :SUMINUCH,'
      '    TCARD = :TCARD'
      'WHERE'
      '    IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCSPECACTSIN'
      'WHERE'
      '        IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCSPECACTSIN('
      '    IDHEAD,'
      '    ID,'
      '    IDCARD,'
      '    QUANT,'
      '    IDM,'
      '    KM,'
      '    PRICEIN,'
      '    SUMIN,'
      '    PRICEINUCH,'
      '    SUMINUCH,'
      '    TCARD'
      ')'
      'VALUES('
      '    :IDHEAD,'
      '    :ID,'
      '    :IDCARD,'
      '    :QUANT,'
      '    :IDM,'
      '    :KM,'
      '    :PRICEIN,'
      '    :SUMIN,'
      '    :PRICEINUCH,'
      '    :SUMINUCH,'
      '    :TCARD'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT so.IDHEAD,so.ID,so.IDCARD,so.QUANT,so.IDM,so.KM,so.PRICEI' +
        'N,so.SUMIN,so.PRICEINUCH,so.SUMINUCH,so.TCARD,'
      'me.NAMESHORT,ca.NAME'
      'FROM OF_DOCSPECACTSIN so'
      'left join OF_MESSUR me on me.ID=so.IDM'
      'left join of_cards ca on ca.ID=so.IDCARD'
      'where(  IDHEAD=:IDH'
      '     ) and (     SO.IDHEAD = :OLD_IDHEAD'
      '    and SO.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT so.IDHEAD,so.ID,so.IDCARD,so.QUANT,so.IDM,so.KM,so.PRICEI' +
        'N,so.SUMIN,so.PRICEINUCH,so.SUMINUCH,so.TCARD,'
      'me.NAMESHORT,ca.NAME'
      'FROM OF_DOCSPECACTSIN so'
      'left join OF_MESSUR me on me.ID=so.IDM'
      'left join of_cards ca on ca.ID=so.IDCARD'
      'where IDHEAD=:IDH'
      'Order by so.ID')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 376
    Top = 320
    poAskRecordCount = True
    object quSpecAIIDHEAD: TFIBIntegerField
      FieldName = 'IDHEAD'
    end
    object quSpecAIID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quSpecAIIDCARD: TFIBIntegerField
      FieldName = 'IDCARD'
    end
    object quSpecAIQUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quSpecAIIDM: TFIBIntegerField
      FieldName = 'IDM'
    end
    object quSpecAIKM: TFIBFloatField
      FieldName = 'KM'
    end
    object quSpecAIPRICEIN: TFIBFloatField
      FieldName = 'PRICEIN'
    end
    object quSpecAISUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quSpecAIPRICEINUCH: TFIBFloatField
      FieldName = 'PRICEINUCH'
    end
    object quSpecAISUMINUCH: TFIBFloatField
      FieldName = 'SUMINUCH'
    end
    object quSpecAITCARD: TFIBSmallIntField
      FieldName = 'TCARD'
    end
    object quSpecAINAMESHORT: TFIBStringField
      FieldName = 'NAMESHORT'
      Size = 50
      EmptyStrToNull = True
    end
    object quSpecAINAME: TFIBStringField
      FieldName = 'NAME'
      Size = 200
      EmptyStrToNull = True
    end
  end
end
