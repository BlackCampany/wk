unit uDB1;

interface

uses
  SysUtils, Classes, FIBDatabase, pFIBDatabase, FIBQuery, pFIBQuery, DB,
  FIBDataSet, pFIBDataSet, pFIBStoredProc;

type
  TdmC1 = class(TDataModule)
    CasherRnDb1: TpFIBDatabase;
    quPrint: TpFIBDataSet;
    quPrintIDQUERY: TFIBIntegerField;
    quPrintID: TFIBIntegerField;
    quPrintSTREAM: TFIBIntegerField;
    quPrintPTYPE: TFIBStringField;
    quPrintFTYPE: TFIBSmallIntField;
    quPrintSTR: TFIBStringField;
    quPrintPAGECODE: TFIBSmallIntField;
    quPrintCTYPE: TFIBSmallIntField;
    quPrintBRING: TFIBSmallIntField;
    quPrintQu: TpFIBDataSet;
    quPrintQuIDQUERY: TFIBIntegerField;
    quPrintQuID: TFIBIntegerField;
    quPrintQuSTREAM: TFIBIntegerField;
    quPrintQuPTYPE: TFIBStringField;
    quPrintQuFTYPE: TFIBSmallIntField;
    quPrintQuSTR: TFIBStringField;
    quPrintQuPAGECODE: TFIBSmallIntField;
    quPrintQuCTYPE: TFIBSmallIntField;
    quPrintQuBRING: TFIBSmallIntField;
    quDelQu: TpFIBQuery;
    trQuPrint: TpFIBTransaction;
    trDelQu: TpFIBTransaction;
    trPrintSel: TpFIBTransaction;
    trPrintUpd: TpFIBTransaction;
    prGetId1: TpFIBStoredProc;
    trSelGetId: TpFIBTransaction;
    prSetRefresh: TpFIBStoredProc;
    trRefresh: TpFIBTransaction;
    quRefresh: TpFIBDataSet;
  private
    { Private declarations }
  public
    { Public declarations }
    Procedure PrintDBStr(iQ,iStream:INteger;sPt,PrintS:String;iFont,iRing:Integer);
  end;

Function GetId1(ta:String):Integer;
Procedure SetRefresh;
Function  GetRefresh:Boolean;

var
  dmC1: TdmC1;

implementation

uses Un1;

{$R *.dfm}

Function  GetRefresh:Boolean;
begin
  with dmC1 do
  begin
    result:=False;



  end;
end;


Procedure SetRefresh;
begin
  with dmC1 do
  begin
    prSetRefresh.ParamByName('STATION').AsInteger:=Commonset.Station;
    prSetRefresh.ExecProc;
  end;
end;


Procedure TdmC1.PrintDBStr(iQ,iStream:INteger;sPt,PrintS:String;iFont,iRing:Integer);
Var iQs:Integer;
begin
//  Delay(20);
  iQs:=GetId1('PQS');

//  Delay(20);
  quPrint.Append;
  quPrintIDQUERY.AsInteger:=iQ;
  quPrintID.AsInteger:=iQs;
  quPrintSTREAM.AsInteger:=iStream;
  quPrintPTYPE.AsString:=sPt;
  quPrintFTYPE.AsInteger:=iFont;
  quPrintSTR.AsString:=PrintS;
  quPrintCTYPE.AsInteger:=0;
  quPrintBRING.AsInteger:=iRing;
  quPrint.Post;
end;

Function GetId1(ta:String):Integer;
Var iType:Integer;
begin
  with dmC1 do
  begin
    iType:=0;
    if ta='Pe' then iType:=1; //��������
    if ta='Cl' then iType:=2; //�������������
    if ta='Trh' then iType:=3; //��������� ������
    if ta='Trs' then iType:=4; //������������ ������
    if ta='TabH' then iType:=5; //��������� ������� (������)
    if ta='CS' then iType:=6; //��������� ������� (������)
    if ta='TH' then iType:=7; //��������� ������� (������) all

    if ta='PQH' then iType:=9; //����� ������� ������
    if ta='PQS' then iType:=10; //����� ������ ������
    if ta='PQH0' then iType:=11; //����� ������� ��� � ���������

    delay(10);
    prGetId1.ParamByName('ITYPE').Value:=iType;
    prGetId1.ExecProc;
    result:=prGetId1.ParamByName('RESULT').Value;
  end;
end;


end.
