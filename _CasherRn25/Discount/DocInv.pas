unit DocInv;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SpeedBar, ExtCtrls, ComCtrls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Placemnt, cxImageComboBox, XPStyleActnCtrls, ActnList, ActnMan,
  cxCalendar, cxContainer, cxTextEdit, cxMemo;

type
  TfmDocsInv = class(TForm)
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    Timer1: TTimer;
    FormPlacement1: TFormPlacement;
    GridDocsInv: TcxGrid;
    ViewDocsInv: TcxGridDBTableView;
    LevelDocsInv: TcxGridLevel;
    amDocsInv: TActionManager;
    acPeriod: TAction;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    SpeedItem6: TSpeedItem;
    acAddDoc1: TAction;
    acEditDoc1: TAction;
    acViewDoc1: TAction;
    acDelDoc1: TAction;
    acOnDoc1: TAction;
    acOffDoc1: TAction;
    SpeedItem7: TSpeedItem;
    SpeedItem8: TSpeedItem;
    acVid: TAction;
    SpeedItem9: TSpeedItem;
    LevelCardsInv: TcxGridLevel;
    ViewCardsInv: TcxGridDBTableView;
    ViewCardsInvNAME: TcxGridDBColumn;
    ViewCardsInvNAMESHORT: TcxGridDBColumn;
    ViewCardsInvIDCARD: TcxGridDBColumn;
    ViewCardsInvQUANT: TcxGridDBColumn;
    ViewCardsInvPRICEIN: TcxGridDBColumn;
    ViewCardsInvSUMIN: TcxGridDBColumn;
    ViewCardsInvPRICEUCH: TcxGridDBColumn;
    ViewCardsInvSUMUCH: TcxGridDBColumn;
    ViewCardsInvIDNDS: TcxGridDBColumn;
    ViewCardsInvSUMNDS: TcxGridDBColumn;
    ViewCardsInvDATEDOC: TcxGridDBColumn;
    ViewCardsInvNUMDOC: TcxGridDBColumn;
    ViewCardsInvNAMECL: TcxGridDBColumn;
    ViewCardsInvNAMEMH: TcxGridDBColumn;
    ViewDocsInvID: TcxGridDBColumn;
    ViewDocsInvDATEDOC: TcxGridDBColumn;
    ViewDocsInvNUMDOC: TcxGridDBColumn;
    ViewDocsInvIDSKL: TcxGridDBColumn;
    ViewDocsInvNAMEMH: TcxGridDBColumn;
    ViewDocsInvIACTIVE: TcxGridDBColumn;
    ViewDocsInvITYPE: TcxGridDBColumn;
    ViewDocsInvSUM1: TcxGridDBColumn;
    ViewDocsInvSUM11: TcxGridDBColumn;
    ViewDocsInvSUM2: TcxGridDBColumn;
    ViewDocsInvSUM21: TcxGridDBColumn;
    ViewDocsInvSUMDIFIN: TcxGridDBColumn;
    ViewDocsInvSUMDIFUCH: TcxGridDBColumn;
    Panel1: TPanel;
    Memo1: TcxMemo;
    procedure SpeedItem1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acPeriodExecute(Sender: TObject);
    procedure acAddDoc1Execute(Sender: TObject);
    procedure acEditDoc1Execute(Sender: TObject);
    procedure acViewDoc1Execute(Sender: TObject);
    procedure ViewDocsInvDblClick(Sender: TObject);
    procedure acDelDoc1Execute(Sender: TObject);
    procedure acOnDoc1Execute(Sender: TObject);
    procedure acOffDoc1Execute(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure acVidExecute(Sender: TObject);
    procedure SpeedItem1Click0(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmDocsInv: TfmDocsInv;
  bClearDocIn:Boolean = false;

implementation

uses Un1, dmOffice, PeriodUni, AddDoc1, AddInv, DMOReps;

{$R *.dfm}

procedure TfmDocsInv.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmDocsInv.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  Timer1.Enabled:=True;
  GridDocsInv.Align:=AlClient;
  ViewDocsInv.RestoreFromIniFile(CurDir+GridIni);
  ViewCardsInv.RestoreFromIniFile(CurDir+GridIni);
  StatusBar1.Color:=$00FFCACA;
  Memo1.Clear;
end;

procedure TfmDocsInv.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewDocsInv.StoreToIniFile(CurDir+GridIni,False);
  ViewCardsInv.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmDocsInv.acPeriodExecute(Sender: TObject);
begin
//  ������
  fmPeriodUni.DateTimePicker1.Date:=CommonSet.DateFrom;
//  fmPeriodUni.DateTimePicker2.Date:=CommonSet.DateTo-1;
  fmPeriodUni.ShowModal;
  if fmPeriodUni.ModalResult=mrOk then
  begin
    CommonSet.DateFrom:=Trunc(fmPeriodUni.DateTimePicker1.Date);
    CommonSet.DateTo:=Trunc(fmPeriodUni.DateTimePicker2.Date)+1;

    with dmO do
    with dmORep do
    begin
      if LevelDocsInv.Visible then
      begin
        if CommonSet.DateTo>=iMaxDate then fmDocsInv.Caption:='�������������� � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
        else fmDocsInv.Caption:='�������������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);

        ViewDocsInv.BeginUpdate;
        quDocsInvSel.Active:=False;
        quDocsInvSel.ParamByName('DATEB').AsDate:=CommonSet.DateFrom;
        quDocsInvSel.ParamByName('DATEE').AsDate:=CommonSet.DateTo;
        quDocsInvSel.Active:=True;
        ViewDocsInv.EndUpdate;
      end else
      begin
        if CommonSet.DateTo>=iMaxDate then fmDocsInv.Caption:='������� �� ������� � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
        else fmDocsInv.Caption:='������� �� ������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);

        ViewCardsInv.BeginUpdate;
{       quDocsInCard.Active:=False;
        quDocsInCard.ParamByName('DATEB').AsDate:=CommonSet.DateFrom;
        quDocsInCard.ParamByName('DATEE').AsDate:=CommonSet.DateTo;
        quDocsInCard.Active:=True;}
        ViewCardsInv.EndUpdate;
      end;
    end;
  end;
end;

procedure TfmDocsInv.acAddDoc1Execute(Sender: TObject);
//Var IDH:INteger;
//    rSum1,rSum2:Real;
begin
  //�������� ��������

  if not CanDo('prAddDocInv') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmO do
  with dmORep do
  begin
    fmAddInv.Caption:='��������������: ����� ��������.';
    fmAddInv.cxTextEdit1.Text:='';
    fmAddInv.cxTextEdit1.Properties.ReadOnly:=False;
    fmAddInv.cxTextEdit1.Tag:=0; //������� ���������� ��������� 
    fmAddInv.cxDateEdit1.Date:=Date;
    fmAddInv.cxDateEdit1.Properties.ReadOnly:=False;

    if quMHAll.Active=False then quMHAll.Active:=True;
    quMHAll.FullRefresh;

    fmAddInv.cxLookupComboBox1.EditValue:=0;
    fmAddInv.cxLookupComboBox1.Text:='';
    fmAddInv.cxLookupComboBox1.Properties.ReadOnly:=False;

    if CurVal.IdMH<>0 then
    begin
      fmAddInv.cxLookupComboBox1.EditValue:=CurVal.IdMH;
      fmAddInv.cxLookupComboBox1.Text:=CurVal.NAMEMH;
    end else
    begin
       quMHAll.First;
       if not quMHAll.Eof then
       begin
         CurVal.IdMH:=quMHAllID.AsInteger;
         CurVal.NAMEMH:=quMHAllNAMEMH.AsString;
         fmAddInv.cxLookupComboBox1.EditValue:=CurVal.IdMH;
         fmAddInv.cxLookupComboBox1.Text:=CurVal.NAMEMH;
       end;
    end;
    if quMHAll.Locate('ID',CurVal.IdMH,[]) then
    begin
      fmAddInv.Label15.Caption:='��. ����: '+quMHAllNAMEPRICE.AsString;
      fmAddInv.Label15.Tag:=quMHAllDEFPRICE.AsInteger;
    end else
    begin
      fmAddInv.Label15.Caption:='��. ����: ';
      fmAddInv.Label15.Tag:=0;
    end;

    fmAddInv.cxLabel1.Enabled:=True;
    fmAddInv.cxLabel2.Enabled:=True;

    fmAddInv.cxButton1.Enabled:=True;
    fmAddInv.taSpec.Active:=False;
    fmAddInv.taSpec.CreateDataSet;
    fmAddInv.taSpecC.Active:=False;
    fmAddInv.taSpecC.CreateDataSet;
    taCalcB.Active:=False;
    taCalcB.CreateDataSet;

    fmAddInv.ShowModal;
  end;
end;

procedure TfmDocsInv.acEditDoc1Execute(Sender: TObject);
Var IDH:INteger;
begin
  //�������������
  if not CanDo('prEditDocInv') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmO do
  with dmORep do
  begin
    if quDocsInvSel.RecordCount>0 then //���� ��� �������������
    begin
      if quDocsInvSelIACTIVE.AsInteger=0 then
      begin
        Memo1.Clear;
        Memo1.Lines.Add('����� ���� �������� ���������.'); Delay(10);

        fmAddInv.Caption:='��������������: ��������������.';
        fmAddInv.cxTextEdit1.Text:=quDocsInvSelNUMDOC.AsString;
        fmAddInv.cxTextEdit1.Properties.ReadOnly:=False;
        fmAddInv.cxTextEdit1.Tag:=quDocsInvSelId.AsInteger;
        fmAddInv.cxDateEdit1.Date:=quDocsInvSelDATEDOC.AsDateTime;
        fmAddInv.cxDateEdit1.Properties.ReadOnly:=False;

        if quMHAll.Active=False then quMHAll.Active:=True;
        quMHAll.FullRefresh;

        fmAddInv.cxLookupComboBox1.EditValue:=quDocsInvSelIDSKL.AsInteger;
        fmAddInv.cxLookupComboBox1.Text:=quDocsInvSelNAMEMH.AsString;
        fmAddInv.cxLookupComboBox1.Properties.ReadOnly:=False;

        CurVal.IdMH:=quDocsInvSelIDSKL.AsInteger;
        CurVal.NAMEMH:=quDocsInvSelNAMEMH.AsString;

        if quMHAll.Locate('ID',CurVal.IdMH,[]) then
        begin
          fmAddInv.Label15.Caption:='��. ����: '+quMHAllNAMEPRICE.AsString;
          fmAddInv.Label15.Tag:=quMHAllDEFPRICE.AsInteger;
        end else
        begin
          fmAddInv.Label15.Caption:='��. ����: ';
          fmAddInv.Label15.Tag:=0;
        end;

        fmAddInv.cxLabel1.Enabled:=True;
        fmAddInv.cxLabel2.Enabled:=True;
        fmAddInv.cxLabel7.Enabled:=True;
        fmAddInv.cxLabel8.Enabled:=True;
        fmAddInv.cxLabel9.Enabled:=True;
        fmAddInv.cxButton1.Enabled:=True;
        fmAddInv.Label3.Enabled:=True;
        fmAddInv.Label4.Enabled:=True;
        fmAddInv.Label6.Enabled:=True;


        fmAddInv.ViewInv.OptionsData.Editing:=True;

        fmAddInv.taSpec.Active:=False;
        fmAddInv.taSpec.CreateDataSet;
        fmAddInv.taSpecC.Active:=False;
        fmAddInv.taSpecC.CreateDataSet;

        IDH:=quDocsInvSelID.AsInteger;

        Memo1.Lines.Add('   ������ � �����.'); Delay(10);

        quSpecInv.Active:=False;
        quSpecInv.ParamByName('IDH').AsInteger:=IDH;
        quSpecInv.Active:=True;

        quSpecInv.First;
        while not quSpecInv.Eof do
        begin
          with fmAddInv do
          begin
            taSpec.Append;
            taSpecNum.AsInteger:=quSpecInvNUM.AsInteger;
            taSpecIdGoods.AsInteger:=quSpecInvIDCARD.AsInteger;
            taSpecNameG.AsString:=quSpecInvNAME.AsString;
            taSpecIM.AsInteger:=quSpecInvIDMESSURE.AsInteger;
            taSpecSM.AsString:=quSpecInvNAMESHORT.AsString;
            taSpecQuant.AsFloat:=quSpecInvQUANT.AsFloat;
            taSpecSumIn.AsFloat:=quSpecInvSUMIN.AsFloat;
            taSpecSumUch.AsFloat:=quSpecInvSUMUCH.AsFloat;
            taSpecPriceIn.AsFloat:=0; taSpecPriceUch.AsFloat:=0;
            if quSpecInvQUANT.AsFloat<>0 then
            begin
              taSpecPriceIn.AsFloat:=quSpecInvSUMIN.AsFloat/quSpecInvQUANT.AsFloat;
              taSpecPriceUch.AsFloat:=quSpecInvSUMUCH.AsFloat/quSpecInvQUANT.AsFloat;
            end;
            taSpecQuantFact.AsFloat:=quSpecInvQUANTFACT.AsFloat;
            taSpecSumInF.AsFloat:=quSpecInvSUMINFACT.AsFloat;
            taSpecSumUchF.AsFloat:=quSpecInvSUMUCHFACT.AsFloat;
            taSpecPriceInF.AsFloat:=0; taSpecPriceUchF.AsFloat:=0;
            if quSpecInvQUANTFACT.AsFloat<>0 then
            begin
              taSpecPriceInF.AsFloat:=quSpecInvSUMINFACT.AsFloat/quSpecInvQUANTFACT.AsFloat;
              taSpecPriceUchF.AsFloat:=quSpecInvSUMUCHFACT.AsFloat/quSpecInvQUANTFACT.AsFloat;
            end;

            taSpecKm.AsFloat:=quSpecInvKM.AsFloat;
            taSpecTCard.AsInteger:=quSpecInvTCARD.AsInteger;
            taSpecNoCalc.AsInteger:=quSpecInvNOCALC.AsInteger;
            taSpecId_Group.AsInteger:=quSpecInvIDGROUP.AsInteger;
            taSpecNameGr.AsString:=quSpecInvNAMECL.AsString;
            taSpec.Post;
          end;
          quSpecInv.Next;
        end;

        Memo1.Lines.Add('   ������.'); Delay(10);

        quSpecInvC.Active:=False;
        quSpecInvC.ParamByName('IDH').AsInteger:=IDH;
        quSpecInvC.Active:=True;

        quSpecInvC.First;
        while not quSpecInvC.Eof do
        begin
          with fmAddInv do
          begin
            taSpecC.Append;
            taSpecCNum.AsInteger:=quSpecInvCNUM.AsInteger;
            taSpecCIdGoods.AsInteger:=quSpecInvCIDCARD.AsInteger;
            taSpecCNameG.AsString:=quSpecInvCNAME.AsString;
            taSpecCIM.AsInteger:=quSpecInvCIDMESSURE.AsInteger;
            taSpecCSM.AsString:=quSpecInvCNAMESHORT.AsString;
            taSpecCQuant.AsFloat:=quSpecInvCQUANT.AsFloat;
            taSpecCSumIn.AsFloat:=quSpecInvCSUMIN.AsFloat;
            taSpecCSumUch.AsFloat:=quSpecInvCSUMUCH.AsFloat;
            taSpecCPriceIn.AsFloat:=0; taSpecCPriceUch.AsFloat:=0;
            if quSpecInvCQUANT.AsFloat<>0 then
            begin
              taSpecCPriceIn.AsFloat:=quSpecInvCSUMIN.AsFloat/quSpecInvCQUANT.AsFloat;
              taSpecCPriceUch.AsFloat:=quSpecInvCSUMUCH.AsFloat/quSpecInvCQUANT.AsFloat;
            end;
            taSpecCQuantFact.AsFloat:=quSpecInvCQUANTFACT.AsFloat;
            taSpecCSumInF.AsFloat:=quSpecInvCSUMINFACT.AsFloat;
            taSpecCSumUchF.AsFloat:=quSpecInvCSUMUCHFACT.AsFloat;
            taSpecCPriceInF.AsFloat:=0; taSpecCPriceUchF.AsFloat:=0;
            if quSpecInvCQUANTFACT.AsFloat<>0 then
            begin
              taSpecCPriceInF.AsFloat:=quSpecInvCSUMINFACT.AsFloat/quSpecInvCQUANTFACT.AsFloat;
              taSpecCPriceUchF.AsFloat:=quSpecInvCSUMUCHFACT.AsFloat/quSpecInvCQUANTFACT.AsFloat;
            end;

            taSpecCKm.AsFloat:=quSpecInvCKM.AsFloat;
            taSpecCTCard.AsInteger:=quSpecInvCTCARD.AsInteger;
            taSpecCId_Group.AsInteger:=quSpecInvCIDGROUP.AsInteger;
            taSpecCNameGr.AsString:=quSpecInvCNAMECL.AsString;
            taSpecC.Post;
          end;
          quSpecInvC.Next;
        end;
        Memo1.Lines.Add('   �����������.'); Delay(10);

        taCalcB.Active:=False;
        taCalcB.CreateDataSet;

        quSpecInvBC.Active:=False;
        quSpecInvBC.ParamByName('IDH').AsInteger:=IDH;
        quSpecInvBC.Active:=True;

        quSpecInvBC.First;
        while not quSpecInvBC.Eof do
        begin
          taCalcB.Append;
          taCalcBID.AsInteger:=quSpecInvBCIDB.AsInteger;
          taCalcBCODEB.AsInteger:=quSpecInvBCCODEB.AsInteger;
          taCalcBNAMEB.AsString:=quSpecInvBCNAMEB.AsString;
          taCalcBQUANT.AsFloat:=quSpecInvBCQUANT.AsFloat;
          taCalcBPRICEOUT.AsFloat:=quSpecInvBCPRICEOUT.AsFloat;
          taCalcBSUMOUT.AsFloat:=quSpecInvBCSUMOUT.AsFloat;
          taCalcBIDCARD.AsInteger:=quSpecInvBCIDCARD.AsInteger;
          taCalcBNAMEC.AsString:=quSpecInvBCNAMEC.AsString;
          taCalcBQUANTC.AsFloat:=quSpecInvBCQUANTC.AsFloat;
          taCalcBPRICEIN.AsFloat:=quSpecInvBCPRICEIN.AsFloat;
          taCalcBSUMIN.AsFloat:=quSpecInvBCSUMIN.AsFloat;
          taCalcBIM.AsInteger:=quSpecInvBCIM.AsInteger;
          taCalcBSM.AsString:=quSpecInvBCSM.AsString;
          taCalcBSB.AsString:=quSpecInvBCSB.AsString;
          taCalcB.Post;

          quSpecInvBC.Next;
        end;

        Memo1.Lines.Add('   �������� ��.'); Delay(10);
        fmAddInv.ShowModal;
      end else
      begin
        showmessage('������������� �������������� �������� ������.');
      end;
    end else
    begin
      showmessage('�������� �������� ��� ��������������.');
    end;//}
  end;
end;

procedure TfmDocsInv.acViewDoc1Execute(Sender: TObject);
Var IDH:INteger;
begin
  //��������
  if not CanDo('prViewDocInv') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  with dmO do
  with dmORep do
  begin
    if quDocsInvSel.RecordCount>0 then //���� ��� �������������
    begin
        Memo1.Clear;
        Memo1.Lines.Add('����� ���� �������� ���������.'); Delay(10);

        fmAddInv.Caption:='��������������: ��������.';
        fmAddInv.cxTextEdit1.Text:=quDocsInvSelNUMDOC.AsString;
        fmAddInv.cxTextEdit1.Properties.ReadOnly:=True;
        fmAddInv.cxTextEdit1.Tag:=quDocsInvSelId.AsInteger;
        fmAddInv.cxDateEdit1.Date:=quDocsInvSelDATEDOC.AsDateTime;
        fmAddInv.cxDateEdit1.Properties.ReadOnly:=True;

        if quMHAll.Active=False then quMHAll.Active:=True;
        quMHAll.FullRefresh;

        fmAddInv.cxLookupComboBox1.EditValue:=quDocsInvSelIDSKL.AsInteger;
        fmAddInv.cxLookupComboBox1.Text:=quDocsInvSelNAMEMH.AsString;
        fmAddInv.cxLookupComboBox1.Properties.ReadOnly:=True;

        CurVal.IdMH:=quDocsInvSelIDSKL.AsInteger;
        CurVal.NAMEMH:=quDocsInvSelNAMEMH.AsString;

        if quMHAll.Locate('ID',CurVal.IdMH,[]) then
        begin
          fmAddInv.Label15.Caption:='��. ����: '+quMHAllNAMEPRICE.AsString;
          fmAddInv.Label15.Tag:=quMHAllDEFPRICE.AsInteger;
        end else
        begin
          fmAddInv.Label15.Caption:='��. ����: ';
          fmAddInv.Label15.Tag:=0;
        end;

        fmAddInv.cxLabel1.Enabled:=False;
        fmAddInv.cxLabel2.Enabled:=False;
        fmAddInv.cxLabel7.Enabled:=False;
        fmAddInv.cxLabel8.Enabled:=False;
        fmAddInv.cxLabel9.Enabled:=False;
        fmAddInv.cxButton1.Enabled:=False;

        fmAddInv.Label3.Enabled:=False;
        fmAddInv.Label4.Enabled:=False;
        fmAddInv.Label6.Enabled:=False;


        fmAddInv.ViewInv.OptionsData.Editing:=False;


        fmAddInv.taSpec.Active:=False;
        fmAddInv.taSpec.CreateDataSet;
        fmAddInv.taSpecC.Active:=False;
        fmAddInv.taSpecC.CreateDataSet;

        IDH:=quDocsInvSelID.AsInteger;

        Memo1.Lines.Add('   ������ � �����.'); Delay(10);

        quSpecInv.Active:=False;
        quSpecInv.ParamByName('IDH').AsInteger:=IDH;
        quSpecInv.Active:=True;

        quSpecInv.First;
        while not quSpecInv.Eof do
        begin
          with fmAddInv do
          begin
            taSpec.Append;
            taSpecNum.AsInteger:=quSpecInvNUM.AsInteger;
            taSpecIdGoods.AsInteger:=quSpecInvIDCARD.AsInteger;
            taSpecNameG.AsString:=quSpecInvNAME.AsString;
            taSpecIM.AsInteger:=quSpecInvIDMESSURE.AsInteger;
            taSpecSM.AsString:=quSpecInvNAMESHORT.AsString;
            taSpecQuant.AsFloat:=quSpecInvQUANT.AsFloat;
            taSpecSumIn.AsFloat:=quSpecInvSUMIN.AsFloat;
            taSpecSumUch.AsFloat:=quSpecInvSUMUCH.AsFloat;
            taSpecPriceIn.AsFloat:=0; taSpecPriceUch.AsFloat:=0;
            if quSpecInvQUANT.AsFloat<>0 then
            begin
              taSpecPriceIn.AsFloat:=quSpecInvSUMIN.AsFloat/quSpecInvQUANT.AsFloat;
              taSpecPriceUch.AsFloat:=quSpecInvSUMUCH.AsFloat/quSpecInvQUANT.AsFloat;
            end;
            taSpecQuantFact.AsFloat:=quSpecInvQUANTFACT.AsFloat;
            taSpecSumInF.AsFloat:=quSpecInvSUMINFACT.AsFloat;
            taSpecSumUchF.AsFloat:=quSpecInvSUMUCHFACT.AsFloat;
            taSpecPriceInF.AsFloat:=0; taSpecPriceUchF.AsFloat:=0;
            if quSpecInvQUANTFACT.AsFloat<>0 then
            begin
              taSpecPriceInF.AsFloat:=quSpecInvSUMINFACT.AsFloat/quSpecInvQUANTFACT.AsFloat;
              taSpecPriceUchF.AsFloat:=quSpecInvSUMUCHFACT.AsFloat/quSpecInvQUANTFACT.AsFloat;
            end;

            taSpecKm.AsFloat:=quSpecInvKM.AsFloat;
            taSpecTCard.AsInteger:=quSpecInvTCARD.AsInteger;
            taSpecNoCalc.AsInteger:=quSpecInvNOCALC.AsInteger;
            taSpecId_Group.AsInteger:=quSpecInvIDGROUP.AsInteger;
            taSpecNameGr.AsString:=quSpecInvNAMECL.AsString;
            taSpec.Post;
          end;
          quSpecInv.Next;
        end;

        Memo1.Lines.Add('   ������.'); Delay(10);

        quSpecInvC.Active:=False;
        quSpecInvC.ParamByName('IDH').AsInteger:=IDH;
        quSpecInvC.Active:=True;

        quSpecInvC.First;
        while not quSpecInvC.Eof do
        begin
          with fmAddInv do
          begin
            taSpecC.Append;
            taSpecCNum.AsInteger:=quSpecInvCNUM.AsInteger;
            taSpecCIdGoods.AsInteger:=quSpecInvCIDCARD.AsInteger;
            taSpecCNameG.AsString:=quSpecInvCNAME.AsString;
            taSpecCIM.AsInteger:=quSpecInvCIDMESSURE.AsInteger;
            taSpecCSM.AsString:=quSpecInvCNAMESHORT.AsString;
            taSpecCQuant.AsFloat:=quSpecInvCQUANT.AsFloat;
            taSpecCSumIn.AsFloat:=quSpecInvCSUMIN.AsFloat;
            taSpecCSumUch.AsFloat:=quSpecInvCSUMUCH.AsFloat;
            taSpecCPriceIn.AsFloat:=0; taSpecCPriceUch.AsFloat:=0;
            if quSpecInvCQUANT.AsFloat<>0 then
            begin
              taSpecCPriceIn.AsFloat:=quSpecInvCSUMIN.AsFloat/quSpecInvCQUANT.AsFloat;
              taSpecCPriceUch.AsFloat:=quSpecInvCSUMUCH.AsFloat/quSpecInvCQUANT.AsFloat;
            end;
            taSpecCQuantFact.AsFloat:=quSpecInvCQUANTFACT.AsFloat;
            taSpecCSumInF.AsFloat:=quSpecInvCSUMINFACT.AsFloat;
            taSpecCSumUchF.AsFloat:=quSpecInvCSUMUCHFACT.AsFloat;
            taSpecCPriceInF.AsFloat:=0; taSpecCPriceUchF.AsFloat:=0;
            if quSpecInvCQUANTFACT.AsFloat<>0 then
            begin
              taSpecCPriceInF.AsFloat:=quSpecInvCSUMINFACT.AsFloat/quSpecInvCQUANTFACT.AsFloat;
              taSpecCPriceUchF.AsFloat:=quSpecInvCSUMUCHFACT.AsFloat/quSpecInvCQUANTFACT.AsFloat;
            end;

            taSpecCKm.AsFloat:=quSpecInvCKM.AsFloat;
            taSpecCTCard.AsInteger:=quSpecInvCTCARD.AsInteger;
            taSpecCId_Group.AsInteger:=quSpecInvCIDGROUP.AsInteger;
            taSpecCNameGr.AsString:=quSpecInvCNAMECL.AsString;
            taSpecC.Post;
          end;
          quSpecInvC.Next;
        end;
        Memo1.Lines.Add('   �����������.'); Delay(10);

        taCalcB.Active:=False;
        taCalcB.CreateDataSet;

        quSpecInvBC.Active:=False;
        quSpecInvBC.ParamByName('IDH').AsInteger:=IDH;
        quSpecInvBC.Active:=True;

        quSpecInvBC.First;
        while not quSpecInvBC.Eof do
        begin
          taCalcB.Append;
          taCalcBID.AsInteger:=quSpecInvBCIDB.AsInteger;
          taCalcBCODEB.AsInteger:=quSpecInvBCCODEB.AsInteger;
          taCalcBNAMEB.AsString:=quSpecInvBCNAMEB.AsString;
          taCalcBQUANT.AsFloat:=quSpecInvBCQUANT.AsFloat;
          taCalcBPRICEOUT.AsFloat:=quSpecInvBCPRICEOUT.AsFloat;
          taCalcBSUMOUT.AsFloat:=quSpecInvBCSUMOUT.AsFloat;
          taCalcBIDCARD.AsInteger:=quSpecInvBCIDCARD.AsInteger;
          taCalcBNAMEC.AsString:=quSpecInvBCNAMEC.AsString;
          taCalcBQUANTC.AsFloat:=quSpecInvBCQUANTC.AsFloat;
          taCalcBPRICEIN.AsFloat:=quSpecInvBCPRICEIN.AsFloat;
          taCalcBSUMIN.AsFloat:=quSpecInvBCSUMIN.AsFloat;
          taCalcBIM.AsInteger:=quSpecInvBCIM.AsInteger;
          taCalcBSM.AsString:=quSpecInvBCSM.AsString;
          taCalcBSB.AsString:=quSpecInvBCSB.AsString;
          taCalcB.Post;

          quSpecInvBC.Next;
        end;

        Memo1.Lines.Add('   �������� ��.'); Delay(10);
        fmAddInv.ShowModal;
    end else
    begin
      showmessage('�������� �������� ��� ��������������.');
    end;//}
  end;
end;

procedure TfmDocsInv.ViewDocsInvDblClick(Sender: TObject);
begin
  //������� �������
  with dmO do
  begin
    if quDocsInSelIACTIVE.AsInteger=0 then acEditDoc1.Execute //��������������
    else acViewDoc1.Execute; //��������
  end;
end;

procedure TfmDocsInv.acDelDoc1Execute(Sender: TObject);
//Var IDH:INteger;
begin
  //������� ��������
  if not CanDo('prDelDocInv') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmORep do
  begin
    if quDocsInvSel.RecordCount>0 then //���� ��� �������������
    begin
      if quDocsInvSelIACTIVE.AsInteger=0 then
      begin
        if MessageDlg('�� ������������� ������ ������� �������������� �'+quDocsInvSelNUMDOC.AsString+' �� '+FormatDateTime('dd.mm.yyyy',quDocsInvSelDATEDOC.asDateTime),mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
//          IDH:=quDocsInvSelID.AsInteger;
          quDocsInvSel.Delete;

{          quSpecInv.Active:=False;
          quSpecInv.ParamByName('IDH').AsInteger:=IDH;
          quSpecInv.Active:=True;

          quSpecInv.First; //������
          while not quSpecInv.Eof do quSpecInv.Delete;
          quSpecInv.Active:=False;

          quSpecInvC.Active:=False;
          quSpecInvC.ParamByName('IDH').AsInteger:=IDH;
          quSpecInvC.Active:=True;

          quSpecInvC.First; //������
          while not quSpecInvC.Eof do quSpecInvC.Delete;
          quSpecInvC.Active:=False;
}
        end;
      end else
      begin
        showmessage('������� �������������� �������� ������.');
      end;
    end else
    begin
      showmessage('�������� �������� ��� ��������.');
    end;
  end;
end;

procedure TfmDocsInv.acOnDoc1Execute(Sender: TObject);
Var IdH:INteger;
    rQ,rQs,rQInv,rQp:Real;
begin
//������������
  if not CanDo('prOnDocInv') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmO do
  with dmORep do
  begin
    if quDocsInvSel.RecordCount>0 then //���� ��� ������������
    begin
      if quDocsInvSelIACTIVE.AsInteger=0 then
      begin
        if MessageDlg('������������ �������� �'+quDocsInvSelNUMDOC.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          if prTOFind(Trunc(quDocsInvSelDATEDOC.AsDateTime),quDocsInvSelIDSKL.AsInteger)=1 then
          begin //�� ����
            if MessageDlg('������� ��� �������� ������ �� �� '+quDocsInvSelNAMEMH.AsString+' � '+FormatDateTime('dd.mm.yyyy',quDocsInvSelDATEDOC.AsDateTime)+'?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
              prTODel(Trunc(quDocsInvSelDATEDOC.AsDateTime),quDocsInvSelIDSKL.AsInteger)
            else
            begin
              showmessage('��������� ������� ��������� ����������...');
              exit;
            end;
          end;

         // 1 - ������� ��������� ������ �� ��������� (�� ������ ������)
         // 2 - �������� ����� ������
         // 3 - �������� ��������������
         // 4 - �������� ������

          Memo1.Clear;
          Memo1.Lines.Add('����� ... ���� ��������� ���������.'); Delay(10);
          IDH:=quDocsInvSelID.AsInteger;
         // 1 - ������� ��������� ��������� ������ �� ��������� (�� ������ ������) ��� �������� ������ ��������� ��������� ������
         // � ��� ��������� ��������������
          prDelPartInv.ParamByName('IDDOC').AsInteger:=IDH;
          prDelPartInv.ParamByName('DTYPE').AsInteger:=3;
          prDelPartInv.ExecProc;

          quSpecInvC.Active:=False;
          quSpecInvC.ParamByName('IDH').AsInteger:=IDH;
          quSpecInvC.Active:=True;

          quSpecInvC.First;
          while not quSpecInvC.Eof do
          begin
            rQInv:=quSpecInvCQUANTFACT.AsFloat-quSpecInvCQUANT.AsFloat;
            if rQInv=0 then rQInv:=0.000001;  //�.�. �� ������ � ������� ���-���, �� � � ������� ��������.
            if rQInv>0 then  //��� = 0 ���� �� ����� ����������� � �� ����� ����������
            begin //�������-������ �� ������
// EXECUTE PROCEDURE PR_ADDPARTIN1 (?IDSKL, ?IDDOC, ?DTYPE, ?IDATE, ?IDCARD, ?IDCLI, ?QUANT, ?PRICEIN, ?PRICEUCH)
              prAddPartIn1.ParamByName('IDSKL').AsInteger:=quDocsInvSelIDSKL.AsInteger;
              prAddPartIn1.ParamByName('IDDOC').AsInteger:=IDH;
              prAddPartIn1.ParamByName('DTYPE').AsInteger:=3;
              prAddPartIn1.ParamByName('IDATE').AsInteger:=Trunc(quDocsInvSelDATEDOC.AsDateTime);
              prAddPartIn1.ParamByName('IDCARD').AsInteger:=quSpecInvCIDCARD.AsInteger;
              prAddPartIn1.ParamByName('IDCLI').AsInteger:=0;
              prAddPartIn1.ParamByName('QUANT').AsFloat:=rQInv;
              prAddPartIn1.ParamByName('PRICEIN').AsFloat:=(quSpecInvCSUMINFACT.AsFloat-quSpecInvCSUMIN.AsFloat)/rQInv;
              prAddPartIn1.ParamByName('PRICEUCH').AsFloat:=(quSpecInvCSUMUCHFACT.AsFloat-quSpecInvCSUMUCH.AsFloat)/rQInv;
              prAddPartIn1.ExecProc;

            end else
            begin //���������-������ �� ������
              //������� �������� ������ �� �������
              //����� �������� ��� �������� � �������

              // ����������� ��� �� ����� ��������������

              rQs:=rQInv*(-1);
              prSelPartIn(quSpecInvCIDCARD.AsInteger,quDocsInvSelIDSKL.AsInteger,0);

              quSelPartIn.First;

              while (not quSelPartIn.Eof) and (rQs>0) do
              begin
                //���� �� ���� ������� ���� �����, ��������� �������� ���
                rQp:=quSelPartInQREMN.AsFloat;
                if rQs<=rQp then  rQ:=rQs//��������� ������ ������ ���������
                            else  rQ:=rQp;
                rQs:=rQs-rQ;

                Memo1.Lines.Add('���. ������: ���  '+quSpecInvCIDCARD.AsString+' '+quSpecInvCName.AsString+', ����� '+quDocsInvSelIDSKL.AsString+', ��� ��. ������ '+quSelPartInID.AsString+', ���-�� '+FloatToStr(rQ));

                prAddPartOut.ParamByName('ARTICUL').AsInteger:=quSpecInvCIDCARD.AsInteger;
                prAddPartOut.ParamByName('IDDATE').AsInteger:=Trunc(quDocsInvSelDATEDOC.AsDateTime);
                prAddPartOut.ParamByName('IDSTORE').AsInteger:=quDocsInvSelIDSKL.AsInteger;
                prAddPartOut.ParamByName('IDPARTIN').AsInteger:=quSelPartInID.AsInteger;
                prAddPartOut.ParamByName('IDDOC').AsInteger:=IDH;
                prAddPartOut.ParamByName('IDCLI').AsInteger:=0;
                prAddPartOut.ParamByName('DTYPE').AsInteger:=3;
                prAddPartOut.ParamByName('QUANT').AsFloat:=rQ;
                prAddPartOut.ParamByName('PRICEIN').AsFloat:=(quSpecInvCSUMIN.AsFloat-quSpecInvCSUMINFACT.AsFloat)*(-1)/rQInv;
                prAddPartOut.ParamByName('SUMOUT').AsFloat:=(quSpecInvCSUMUCH.AsFloat-quSpecInvCSUMUCHFACT.AsFloat)*rQ*(-1)/rQInv;
                prAddPartout.ExecProc;

                quSelPartIn.Next;
              end;

              quSelPartIn.Active:=False;

              //��������� �������� � ������������� ������
              if rQs>0 then //�������� ������������� ������, �������� � ������
              begin
                  //��������� ��� ����� ���������
                Memo1.Lines.Add('�����. ������: ���  '+quSpecInvCIDCARD.AsString+' '+quSpecInvCName.AsString+', ����� '+quDocsInvSelIDSKL.AsString+', ��� ��. ������ '+quSelPartInID.AsString+', ���-�� '+FloatToStr(rQs));

                prAddPartOut.ParamByName('ARTICUL').AsInteger:=quSpecInvCIDCARD.AsInteger;
                prAddPartOut.ParamByName('IDDATE').AsInteger:=Trunc(quDocsInvSelDATEDOC.AsDateTime);
                prAddPartOut.ParamByName('IDSTORE').AsInteger:=quDocsInvSelIDSKL.AsInteger;
                prAddPartOut.ParamByName('IDPARTIN').AsInteger:=-1;
                prAddPartOut.ParamByName('IDDOC').AsInteger:=IDH;
                prAddPartOut.ParamByName('IDCLI').AsInteger:=0;
                prAddPartOut.ParamByName('DTYPE').AsInteger:=3;
                prAddPartOut.ParamByName('QUANT').AsFloat:=rQs;
                prAddPartOut.ParamByName('PRICEIN').AsFloat:=(quSpecInvCSUMIN.AsFloat-quSpecInvCSUMINFACT.AsFloat)*(-1)/rQInv;
                prAddPartOut.ParamByName('SUMOUT').AsFloat:=(quSpecInvCSUMUCH.AsFloat-quSpecInvCSUMUCHFACT.AsFloat)*rQs*(-1)/rQInv;
                prAddPartout.ExecProc;
              end;
            end;

            quSpecInvC.Next;
          end;

         // �������� ������
          quDocsInvSel.Edit;
          quDocsInvSelIACTIVE.AsInteger:=1;
          quDocsInvSel.Post;
          quDocsInvSel.Refresh;

          Memo1.Lines.Add('��������� ���������.'); Delay(10);

        end;
      end;
    end;

  end;
end;

procedure TfmDocsInv.acOffDoc1Execute(Sender: TObject);
Var iCountPartOut:Integer;
    bStart:Boolean;
begin
//��������
  if not CanDo('prOffDocInv') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmO do
  with dmORep do
  begin
    if quDocsInvSel.RecordCount>0 then //���� ��� ������������
    begin
      if quDocsInvSelIACTIVE.AsInteger=1 then
      begin
        if MessageDlg('�������� �������� �'+quDocsInvSelNUMDOC.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          if prTOFind(Trunc(quDocsInvSelDATEDOC.AsDateTime),quDocsInvSelIDSKL.AsInteger)=1 then
          begin //�� ����
            if MessageDlg('������� ��� �������� ������ �� �� '+quDocsInvSelNAMEMH.AsString+' � '+FormatDateTime('dd.mm.yyyy',quDocsInvSelDATEDOC.AsDateTime)+'?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
              prTODel(Trunc(quDocsInvSelDATEDOC.AsDateTime),quDocsInvSelIDSKL.AsInteger)
            else
            begin
              showmessage('��������� ������� ��������� ����������...');
              exit;
            end;
          end;

         // 1 - ��������� ���� �� �������� �� ������� ���������� �����
         // ���� ������ ��
          Memo1.Clear;
          Memo1.Lines.Add('����� ... ���� ��������� ���������.'); Delay(10);

          prFindPartOut.ParamByName('IDDOC').AsInteger:=quDocsInvSelID.AsInteger;
          prFindPartOut.ParamByName('DTYPE').AsInteger:=3;
          prFindPartOut.ExecProc;
          iCountPartOut:=prFindPartOut.ParamByName('RESULT').Value;
          if iCountPartOut>0 then
          begin
            if MessageDlg('� ������� ��������� ��������� '+IntToStr(iCountPartOut)+' ��������� ������. �������� ��������.',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
            begin
//               bStart:=True;
//              showmessage('�� ������� ����������� ������������� � '+FormatDateTime('dd.mm.yyyy',quDocsInvSelDATEDOC.AsDateTime)+' �����.');
              bStart:=False;
            end else
            begin
              bStart:=False;
            end;
          end else bStart:=True;
          if bStart then
          begin
            // 1 - �������� ��������� ������
            // 2 - ������� ��������� ������ �� ���������
            // 3 - �������� ��������������
            prPartInDel.ParamByName('IDDOC').AsInteger:=quDocsInvSelID.AsInteger;
            prPartInDel.ParamByName('DTYPE').AsInteger:=3;
            prPartInDel.ExecProc;

            prDelPartOut.ParamByName('IDDOC').AsInteger:=quDocsInvSelID.AsInteger;
            prDelPartOut.ParamByName('DTYPE').AsInteger:=3;
            prDelPartOut.ExecProc;

            // 4 - �������� ������
            quDocsInvSel.Edit;
            quDocsInvSelIACTIVE.AsInteger:=0;
            quDocsInvSel.Post;
            quDocsInvSel.Refresh;
          end;
          Memo1.Lines.Add('��������� ���������.'); Delay(10);
        end;
      end;
    end;
  end;
end;

procedure TfmDocsInv.Timer1Timer(Sender: TObject);
begin
  if bClearDocIn=True then begin StatusBar1.Panels[0].Text:=''; bClearDocIn:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearDocIn:=True;
end;

procedure TfmDocsInv.acVidExecute(Sender: TObject);
begin
  //���
  with dmO do
  with dmORep do
  begin
    if LevelDocsInv.Visible then
    begin
    end;
  end;
end;

procedure TfmDocsInv.SpeedItem1Click0(Sender: TObject);
begin
  Close;
end;

end.
