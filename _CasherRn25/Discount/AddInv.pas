unit AddInv;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, cxGraphics, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxCurrencyEdit, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxControls, cxGridCustomView, cxGrid,
  cxLabel, ExtCtrls, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox, cxMaskEdit, cxCalendar, cxContainer, cxTextEdit,
  StdCtrls, ComCtrls, cxButtons, Placemnt, DBClient, cxButtonEdit,
  ActnList, XPStyleActnCtrls, ActnMan, cxSpinEdit, cxImageComboBox, cxMemo,
  cxGroupBox, cxRadioGroup, FR_DSet, FR_DBSet, FR_Class,
  pFIBDataSet, FIBDatabase, pFIBDatabase, cxCheckBox;

type
  TfmAddInv = class(TForm)
    Panel2: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Label1: TLabel;
    Label5: TLabel;
    Label12: TLabel;
    Label15: TLabel;
    cxTextEdit1: TcxTextEdit;
    cxDateEdit1: TcxDateEdit;
    cxLookupComboBox1: TcxLookupComboBox;
    Panel3: TPanel;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    FormPlacement1: TFormPlacement;
    taSpec: TClientDataSet;
    dsSpec: TDataSource;
    taSpecNum: TIntegerField;
    taSpecIdGoods: TIntegerField;
    taSpecNameG: TStringField;
    taSpecIM: TIntegerField;
    taSpecSM: TStringField;
    taSpecQuant: TFloatField;
    taSpecPriceIn: TFloatField;
    taSpecSumIn: TFloatField;
    taSpecPriceUch: TFloatField;
    taSpecSumUch: TFloatField;
    taSpecQuantFact: TFloatField;
    taSpecPriceInF: TFloatField;
    taSpecSumInF: TFloatField;
    taSpecPriceUchF: TFloatField;
    taSpecSumUchF: TFloatField;
    taSpecQuantDif: TFloatField;
    taSpecSumInDif: TFloatField;
    taSpecSumUchDif: TFloatField;
    cxLabel7: TcxLabel;
    cxLabel8: TcxLabel;
    cxLabel9: TcxLabel;
    amInv: TActionManager;
    acAddPos: TAction;
    taSpecKm: TFloatField;
    Label2: TLabel;
    taSpecTCard: TIntegerField;
    taSpecId_Group: TIntegerField;
    taSpecNameGr: TStringField;
    Panel5: TPanel;
    Panel4: TPanel;
    Memo1: TcxMemo;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    GridInv: TcxGrid;
    ViewInv: TcxGridDBTableView;
    ViewInvNum: TcxGridDBColumn;
    ViewInvIdGoods: TcxGridDBColumn;
    ViewInvNameG: TcxGridDBColumn;
    ViewInvTCard: TcxGridDBColumn;
    ViewInvId_Group: TcxGridDBColumn;
    ViewInvNameGr: TcxGridDBColumn;
    ViewInvIM: TcxGridDBColumn;
    ViewInvSM: TcxGridDBColumn;
    ViewInvQuant: TcxGridDBColumn;
    ViewInvPriceIn: TcxGridDBColumn;
    ViewInvSumIn: TcxGridDBColumn;
    ViewInvPriceUch: TcxGridDBColumn;
    ViewInvSumUch: TcxGridDBColumn;
    ViewInvQuantFact: TcxGridDBColumn;
    ViewInvPriceInF: TcxGridDBColumn;
    ViewInvSumInF: TcxGridDBColumn;
    ViewInvPriceUchF: TcxGridDBColumn;
    ViewInvSumUchF: TcxGridDBColumn;
    ViewInvQuantDif: TcxGridDBColumn;
    ViewInvSumInDif: TcxGridDBColumn;
    ViewInvSumUchDif: TcxGridDBColumn;
    LevelInv: TcxGridLevel;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    GridBC: TcxGrid;
    ViewBC: TcxGridDBTableView;
    ViewBCID: TcxGridDBColumn;
    ViewBCCODEB: TcxGridDBColumn;
    ViewBCNAMEB: TcxGridDBColumn;
    ViewBCQUANT: TcxGridDBColumn;
    ViewBCIDCARD: TcxGridDBColumn;
    ViewBCNAMEC: TcxGridDBColumn;
    ViewBCSB: TcxGridDBColumn;
    ViewBCQUANTC: TcxGridDBColumn;
    ViewBCPRICEIN: TcxGridDBColumn;
    ViewBCSUMIN: TcxGridDBColumn;
    ViewBCIM: TcxGridDBColumn;
    ViewBCSM: TcxGridDBColumn;
    LevelBC: TcxGridLevel;
    acSaveInv: TAction;
    Label3: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    cxButton3: TcxButton;
    taSpecC: TClientDataSet;
    taSpecCNum: TIntegerField;
    taSpecCIdGoods: TIntegerField;
    taSpecCNameG: TStringField;
    taSpecCIM: TIntegerField;
    taSpecCSM: TStringField;
    taSpecCQuant: TFloatField;
    taSpecCPriceIn: TFloatField;
    taSpecCSumIn: TFloatField;
    taSpecCPriceUch: TFloatField;
    taSpecCSumUch: TFloatField;
    taSpecCQuantFact: TFloatField;
    taSpecCPriceInF: TFloatField;
    taSpecCSumInF: TFloatField;
    taSpecCPriceUchF: TFloatField;
    taSpecCSumUchF: TFloatField;
    taSpecCQuantDif: TFloatField;
    taSpecCSumInDif: TFloatField;
    taSpecCSumUchDif: TFloatField;
    taSpecCKm: TFloatField;
    taSpecCTCard: TIntegerField;
    taSpecCId_Group: TIntegerField;
    taSpecCNameGr: TStringField;
    dsSpecC: TDataSource;
    GridInvC: TcxGrid;
    ViewInvC: TcxGridDBTableView;
    LevelInvC: TcxGridLevel;
    RepInv: TfrReport;
    frdsSpec: TfrDBDataSet;
    cxRadioButton1: TcxRadioButton;
    cxRadioButton2: TcxRadioButton;
    cxRadioButton3: TcxRadioButton;
    acAddList: TAction;
    acDelPos: TAction;
    acDelAll: TAction;
    taSpecNoCalc: TSmallintField;
    ViewInvNoCalc: TcxGridDBColumn;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    ViewInvCNum: TcxGridDBColumn;
    ViewInvCIdGoods: TcxGridDBColumn;
    ViewInvCNameG: TcxGridDBColumn;
    ViewInvCIM: TcxGridDBColumn;
    ViewInvCSM: TcxGridDBColumn;
    ViewInvCQuant: TcxGridDBColumn;
    ViewInvCPriceIn: TcxGridDBColumn;
    ViewInvCSumIn: TcxGridDBColumn;
    ViewInvCQuantFact: TcxGridDBColumn;
    ViewInvCPriceInF: TcxGridDBColumn;
    ViewInvCSumInF: TcxGridDBColumn;
    ViewInvCQuantDif: TcxGridDBColumn;
    ViewInvCSumInDif: TcxGridDBColumn;
    ViewInvCTCard: TcxGridDBColumn;
    ViewInvCId_Group: TcxGridDBColumn;
    ViewInvCNameGr: TcxGridDBColumn;
    frdsSpecC: TfrDBDataSet;
    acCalc1: TAction;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acAddPosExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxLabel1Click(Sender: TObject);
    procedure taSpecBeforePost(DataSet: TDataSet);
    procedure ViewInvEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure ViewInvEditKeyPress(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
    procedure cxLabel7Click(Sender: TObject);
    procedure ViewInvDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure ViewInvDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure cxLabel8Click(Sender: TObject);
    procedure cxLabel2Click(Sender: TObject);
    procedure cxLabel9Click(Sender: TObject);
    procedure acSaveInvExecute(Sender: TObject);
    procedure ViewInvEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure taSpecQuantFactChange(Sender: TField);
    procedure taSpecPriceInFChange(Sender: TField);
    procedure taSpecSumInFChange(Sender: TField);
    procedure taSpecPriceUchFChange(Sender: TField);
    procedure taSpecSumUchFChange(Sender: TField);
    procedure Label3Click(Sender: TObject);
    procedure Label4Click(Sender: TObject);
    procedure Label6Click(Sender: TObject);
    procedure taSpecCBeforePost(DataSet: TDataSet);
    procedure cxButton3Click(Sender: TObject);
    procedure acAddListExecute(Sender: TObject);
    procedure acDelPosExecute(Sender: TObject);
    procedure acDelAllExecute(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure Memo1DblClick(Sender: TObject);
    procedure acCalc1Execute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    Procedure prCalcBlInv(sBeg,sName:String;IdPos,iCode,iDate,iTCard:Integer;rQ:Real;IdM:INteger;taCalcB:TClientDataSet;Memo1:TcxMemo);
    Procedure VarsToZero;
    Procedure VarsToSpec;
  end;

var
  fmAddInv: TfmAddInv;
  bAdd:Boolean = False;
  iCol,iMax:INteger;
  Qr,Qf,Pr1,Pr11,Pr2,Pr22,Sum1,Sum11,Sum2,Sum21:Real;

implementation

uses Un1, dmOffice, FCards, Goods, DMOReps, DocInv, Message;

{$R *.dfm}

Procedure TfmAddInv.VarsToZero;
begin
  Qr:=0;Qf:=0;Pr1:=0;Pr11:=0;Pr2:=0;Pr22:=0;Sum1:=0;Sum11:=0;Sum2:=0;Sum21:=0;
end;

Procedure TfmAddInv.VarsToSpec;
begin
  Qr:=taSpecCQuant.AsFloat;
  Qf:=taSpecCQuantFact.AsFloat;
  Pr1:=taSpecCPriceIn.AsFloat;
  Pr11:=taSpecCPriceUch.AsFloat;
  Pr2:=taSpecCPriceInF.AsFloat;
  Pr22:=taSpecCPriceUchF.AsFloat;
  Sum1:=taSpecCSumIn.AsFloat;
  Sum11:=taSpecCSumUch.AsFloat;
  Sum2:=taSpecCSumInF.AsFloat;
  Sum21:=taSpecCSumUchF.AsFloat;
end;


Procedure TfmAddInv.prCalcBlInv(sBeg,sName:String;IdPos,iCode,iDate,iTCard:Integer;rQ:Real;IdM:INteger;taCalcB:TClientDataSet;Memo1:TcxMemo);
Var iTC,iPCount:INteger;
    QT1:TpFIBDataSet;
    rQ1,kBrutto,kM,MassaB:Real;
    iMain:INteger;
    sM:String;
    Par:Variant;
    rQb:Real;
    NameGr:String;
    Id_Group:INteger;

begin
  sBeg:=sBeg+'    ';

  with dmO do
  begin
    //���
    if iTCard=1 then
    begin
      iTC:=0; iPCount:=0; MassaB:=1;
      quFindTCard.Active:=False;
      quFindTCard.ParamByName('IDCARD').AsInteger:=iCode;
      quFindTCard.ParamByName('IDATE').AsDateTime:=iDate;
      quFindTCard.Active:=True;
      if quFindTCard.RecordCount>0 then
      begin
        iTC:=quFindTCardID.AsInteger;
        iPCount:=quFindTCardPCOUNT.AsInteger;
        MassaB:=quFindTCardPVES.AsFloat/1000;
        if prFindMT(IdM)=1 then MassaB:=1;
        prFindSM(IdM,sM,iMain); //��� �������� �������� ������� ���������
        Memo1.Lines.Add(sBeg+sName+' ('+INtToStr(iCode)+'). �� ����. ���-��: '+FloatToStr(RoundEx(rQ*1000)/1000)+' '+sM);
      end else
      begin
        inc(iErr);
        Memo1.Lines.Add(SBeg+'������: �� �� �������.')
      end;
      quFindTCard.Active:=False;
      if iTC>0 then
      begin
        QT1:=TpFIBDataSet.Create(Owner);
        QT1.Active:=False;
        QT1.Database:=OfficeRnDb;
        QT1.Transaction:=trSel;
        QT1.SelectSQL.Clear;
        QT1.SelectSQL.Add('SELECT CS.ID,CS.IDCARD,CS.CURMESSURE,CS.NETTO,CS.BRUTTO,CS.KNB,');
        QT1.SelectSQL.Add('CD.NAME,CD.TCARD');
        QT1.SelectSQL.Add('FROM OF_CARDSTSPEC CS');
        QT1.SelectSQL.Add('left join of_cards cd on cd.ID=CS.IDCARD');
        QT1.SelectSQL.Add('where IDC='+IntToStr(iCode)+' and IDT='+IntToStr(iTC));
        QT1.SelectSQL.Add('ORDER BY CS.ID');
        QT1.Active:=True;

        QT1.First;
        while not QT1.Eof do
        begin
          rQ1:=QT1.FieldByName('NETTO').AsFloat;
          kM:=prFindKM(QT1.FieldByName('CURMESSURE').AsInteger);
          rQ1:=rQ1*kM;//��������� � �������� �������
          prFindSM(QT1.FieldByName('CURMESSURE').AsInteger,sM,iMain); //��� �������� �������� ������� ���������
          //���� ������ �� ������ ����
          kBrutto:=prFindBrutto(QT1.FieldByName('IDCARD').AsInteger,iDate);
          rQ1:=rQ1*(100+kBrutto)/100; //��� ����� � ������
          rQ1:=rQ1/iPCount; //��� ����� �� 1-� ������ ���� �������� �� �������
          rQ1:=rQ1*rQ/MassaB; //��� ����� �� ��� ���-�� ������

//          Memo1.Lines.Add(sBeg+QT1.FieldByName('NAME').AsString+' ('+QT1.FieldByName('IDCARD').AsString+'). �� ����. ���-��: '+FloatToStr(RoundEx(rQ1*1000)/1000)+' '+sM);
          prCalcBlInv(sBeg,QT1.FieldByName('NAME').AsString,IdPos,QT1.FieldByName('IDCARD').AsInteger,iDate,QT1.FieldByName('TCARD').AsInteger,rQ1,iMain,dmORep.taCalcB,Memo1);

          QT1.Next;
        end;
        QT1.Active:=False;
        QT1.Free;
      end;
    end else
    begin
      //��������� � �������� ������� �� ������ ������ ����� ������ ��� - �� ����
      kM:=prFindKM(IdM);
      prFindSM(IdM,sM,iMain); //��� �������� �������� ������� ���������
      rQ:=rQ*kM;              //������ ������� - ��� ������ �� �����������

      Memo1.Lines.Add(sBeg+sName+' ('+IntToStr(iCode)+'). �����.  ���-��: '+FloatToStr(RoundEx(rQ*1000)/1000)+' '+sM);

      //������ ���� ��� � �������� � � ������
//      prCalcSave;
      par := VarArrayCreate([0,1], varInteger);
      par[0]:=IdPos;
      par[1]:=iCode;
      if taCalcB.Locate('ID;IDCARD',par,[]) then
      begin
        rQb:=taCalcB.fieldByName('QUANTC').AsFloat+rQ;
        taCalcB.edit;
        taCalcB.fieldByName('QUANTC').AsFloat:=rQb;
        taCalcB.fieldByName('PRICEIN').AsFloat:=0;
        taCalcB.fieldByName('SUMIN').AsFloat:=0;
        taCalcB.fieldByName('IM').AsInteger:=iMain;
        taCalcB.fieldByName('SM').AsString:=sM;
        taCalcB.fieldByName('SB').AsString:='';
        taCalcB.Post;
      end else
      begin
        taCalcB.Append;
        taCalcB.fieldByName('ID').AsInteger:=IdPos;
        taCalcB.fieldByName('CODEB').AsInteger:=taSpecIdGoods.AsInteger;
        taCalcB.fieldByName('NAMEB').AsString:=taSpecNameG.AsString;
        taCalcB.fieldByName('QUANT').AsFloat:=taSpecQuantFact.AsFloat;
        taCalcB.fieldByName('PRICEOUT').AsFloat:=0;
        taCalcB.fieldByName('SUMOUT').AsFloat:=0;
        taCalcB.fieldByName('IDCARD').AsInteger:=iCode;
        taCalcB.fieldByName('NAMEC').AsString:=sName;
        taCalcB.fieldByName('QUANTC').AsFloat:=rQ;
        taCalcB.fieldByName('PRICEIN').AsFloat:=0;
        taCalcB.fieldByName('SUMIN').AsFloat:=0;
        taCalcB.fieldByName('IM').AsInteger:=iMain;
        taCalcB.fieldByName('SM').AsString:=sM;
        taCalcB.fieldByName('SB').AsString:='';
        taCalcB.Post;
      end;

      if taSpecC.Locate('IdGoods',iCode,[]) then
      begin //�������������
        VarsToSpec;
        taSpecC.Edit;
      end else
      begin
        VarsToZero;
        taSpecC.Append;
        taSpecCNum.AsInteger:=iMax;
        inc(iMax);
      end;

      taSpecCIdGoods.AsInteger:=iCode;
      taSpecCNameG.AsString:=sName;
      taSpecCIM.AsInteger:=iMain;
      taSpecCSM.AsString:=sM;

      prFindGroup(iCode,NameGr,Id_Group); //�������� ������

      taSpecCId_Group.AsInteger:=Id_Group;
      taSpecCNameGr.AsString:=NameGr;

      taSpecCQuant.AsFloat:=0;
      taSpecCPriceIn.AsFloat:=0;
      taSpecCSumIn.AsFloat:=0;
      taSpecCPriceUch.AsFloat:=0;
      taSpecCSumUch.AsFloat:=0;

      taSpecCQuantFact.AsFloat:=rQ+Qf;
      taSpecCPriceInF.AsFloat:=0;
      taSpecCSumInF.AsFloat:=0;
      taSpecCPriceUchF.AsFloat:=0;
      taSpecCSumUchF.AsFloat:=0;
      taSpecCKm.AsFloat:=1;
      taSpecCTCard.AsInteger:=0;
      taSpecC.Post;

    end;
  end;
end;



procedure TfmAddInv.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  PageControl1.Align:=alClient;
  ViewInv.RestoreFromIniFile(CurDir+GridIni);
  ViewInvC.RestoreFromIniFile(CurDir+GridIni);
end;

procedure TfmAddInv.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewInv.StoreToIniFile(CurDir+GridIni,False);
  ViewInvC.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmAddInv.acAddPosExecute(Sender: TObject);
Var iMax:INteger;
begin
//�������� �������
  iMax:=1;
  ViewInv.BeginUpdate;

  taSpec.First;
  if not taSpec.Eof then
  begin
    taSpec.Last;
    iMax:=taSpecNum.AsInteger+1;
  end;

  taSpec.Append;
  taSpecNum.AsInteger:=iMax;
  taSpecIdGoods.AsInteger:=0;
  taSpecNameG.AsString:='';
  taSpecIM.AsInteger:=0;
  taSpecSM.AsString:='';
  taSpecQuant.AsFloat:=0;
  taSpecPriceIn.AsFloat:=0;
  taSpecSumIn.AsFloat:=0;
  taSpecPriceUch.AsFloat:=0;
  taSpecSumUch.AsFloat:=0;
  taSpecQuantFact.AsFloat:=0;
  taSpecPriceInF.AsFloat:=0;
  taSpecSumInF.AsFloat:=0;
  taSpecPriceUchF.AsFloat:=0;
  taSpecSumUchF.AsFloat:=0;
  taSpecKm.AsFloat:=0;
  taSpecTCard.AsInteger:=0;
  taSpecNoCalc.AsInteger:=0;
  taSpecId_Group.AsInteger:=0;
  taSpecNameGr.AsString:='';
  taSpec.Post;
  ViewInv.EndUpdate;
  GridInv.SetFocus;
end;

procedure TfmAddInv.FormShow(Sender: TObject);
begin
  Memo1.Clear;
  PageControl1.ActivePageIndex:=0;
  iCol:=0;
end;

procedure TfmAddInv.cxLabel1Click(Sender: TObject);
begin
  acAddPos.Execute;
end;

procedure TfmAddInv.taSpecBeforePost(DataSet: TDataSet);
begin
  taSpecQuantDif.AsFloat:=taSpecQuantFact.AsFloat-taSpecQuant.AsFloat;
  taSpecSumInDif.AsFloat:=taSpecSumInF.AsFloat-taSpecSumIn.AsFloat;
  taSpecSumUchDif.AsFloat:=taSpecSumUchF.AsFloat-taSpecSumUch.AsFloat;
end;

procedure TfmAddInv.ViewInvEditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
Var iCode:Integer;
    Km:Real;
    sName:String;
begin
  with dmO do
  begin

    if (Key=$0D) then
    begin
      if ViewInv.Controller.FocusedColumn.Name='ViewInvIdGoods' then
      begin
        iCode:=VarAsType(AEdit.EditingValue, varInteger);

        quFCard.Active:=False;
        quFCard.SelectSQL.Clear;
        quFCard.SelectSQL.Add('SELECT ID,NAME,IMESSURE,PARENT,TCARD,INDS');
        quFCard.SelectSQL.Add('FROM OF_CARDS');
        quFCard.SelectSQL.Add('where ID='+IntToStr(iCode));
        quFCard.Active:=True;

        if quFCard.RecordCount=1 then
        begin
          ViewInv.BeginUpdate;
          taSpec.Edit;
          taSpecIdGoods.AsInteger:=iCode;
          taSpecNameG.AsString:=quFCardNAME.AsString;
          taSpecIM.AsInteger:=quFCardIMESSURE.AsInteger;
          taSpecSM.AsString:=prFindKNM(quFCardIMESSURE.AsInteger,Km);
          taSpecKm.AsFloat:=Km;
          taSpecTCard.AsInteger:=quFCardTCARD.AsInteger;
          taSpecNoCalc.AsInteger:=0;

          taSpecId_Group.AsInteger:=quFCardPARENT.AsInteger;
          taSpecNameGr.AsString:=prFindGrN(quFCardPARENT.AsInteger);

          taSpecQuant.AsFloat:=0;
          taSpecPriceIn.AsFloat:=0;
          taSpecSumIn.AsFloat:=0;
          taSpecPriceUch.AsFloat:=0;
          taSpecSumUch.AsFloat:=0;
          taSpecQuantFact.AsFloat:=0;
          taSpecPriceInF.AsFloat:=0;
          taSpecSumInF.AsFloat:=0;
          taSpecPriceUchF.AsFloat:=0;
          taSpecSumUchF.AsFloat:=0;

          taSpec.Post;

          ViewInv.EndUpdate;
        end;
      end;
      if ViewInv.Controller.FocusedColumn.Name='ViewInvNameG' then
      begin
        sName:=VarAsType(AEdit.EditingValue, varString);

        quFCard.Active:=False;
        quFCard.SelectSQL.Clear;
        quFCard.SelectSQL.Add('SELECT ID,NAME,IMESSURE,PARENT,TCARD,INDS');
        quFCard.SelectSQL.Add('FROM OF_CARDS');
        quFCard.SelectSQL.Add('where UPPER(NAME) like ''%'+AnsiUpperCase(sName)+'%''');
        quFCard.SelectSQL.Add('Order by NAME');

        quFCard.Active:=True;

        bAdd:=True;
        //�������� ����� ������ � ����� ������
        fmFCards.ShowModal;
        if fmFCards.ModalResult=mrOk then
        begin
          if quFCard.RecordCount>0 then
          begin
            ViewInv.BeginUpdate;
            taSpec.Edit;
            taSpecIdGoods.AsInteger:=quFCardID.AsInteger;
            taSpecNameG.AsString:=quFCardNAME.AsString;
            taSpecIM.AsInteger:=quFCardIMESSURE.AsInteger;
            taSpecSM.AsString:=prFindKNM(quFCardIMESSURE.AsInteger,Km);
            taSpecKm.AsFloat:=Km;
            taSpecTCard.AsInteger:=quFCardTCARD.AsInteger;
            taSpecNoCalc.AsInteger:=0;
            taSpecId_Group.AsInteger:=quFCardPARENT.AsInteger;
            taSpecNameGr.AsString:=prFindGrN(quFCardPARENT.AsInteger);

            taSpecQuant.AsFloat:=0;
            taSpecPriceIn.AsFloat:=0;
            taSpecSumIn.AsFloat:=0;
            taSpecPriceUch.AsFloat:=0;
            taSpecSumUch.AsFloat:=0;
            taSpecQuantFact.AsFloat:=0;
            taSpecPriceInF.AsFloat:=0;
            taSpecSumInF.AsFloat:=0;
            taSpecPriceUchF.AsFloat:=0;
            taSpecSumUchF.AsFloat:=0;

            taSpec.Post;
            ViewInv.EndUpdate;
            AEdit.SelectAll;
          end;
        end;
        bAdd:=False;
      end;
    end else
      if ViewInv.Controller.FocusedColumn.Name='ViewInvIdGoods' then
        if fTestKey(Key)=False then
          if taSpec.State in [dsEdit,dsInsert] then taSpec.Cancel;
  end;
end;

procedure TfmAddInv.ViewInvEditKeyPress(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
Var Km:Real;
    sName:String;
begin
  if bAdd then exit;
  with dmO do
  begin
    if ViewInv.Controller.FocusedColumn.Name='ViewInvNameG' then
    begin
      sName:=VarAsType(AEdit.EditingValue ,varString)+Key;
//      Label2.Caption:=sName;
      if sName>'' then
      begin
        quFCard.Active:=False;
        quFCard.SelectSQL.Clear;
        quFCard.SelectSQL.Add('SELECT ID,NAME,IMESSURE,PARENT,TCARD,INDS');
        quFCard.SelectSQL.Add('FROM OF_CARDS');
        quFCard.SelectSQL.Add('where UPPER(NAME) like ''%'+AnsiUpperCase(sName)+'%''');
        quFCard.SelectSQL.Add('Order by NAME');
        quFCard.Active:=True;

        if quFCard.RecordCount=1 then
        begin
          ViewInv.BeginUpdate;
          taSpec.Edit;
          taSpecIdGoods.AsInteger:=quFCardID.AsInteger;
          taSpecNameG.AsString:=quFCardNAME.AsString;
          taSpecIM.AsInteger:=quFCardIMESSURE.AsInteger;
          taSpecSM.AsString:=prFindKNM(quFCardIMESSURE.AsInteger,Km);
          taSpecKm.AsFloat:=Km;
          taSpecTCard.AsInteger:=quFCardTCARD.AsInteger;
          taSpecNoCalc.AsInteger:=0;
          taSpecId_Group.AsInteger:=quFCardPARENT.AsInteger;
          taSpecNameGr.AsString:=prFindGrN(quFCardPARENT.AsInteger);

          taSpecQuant.AsFloat:=0;
          taSpecPriceIn.AsFloat:=0;
          taSpecSumIn.AsFloat:=0;
          taSpecPriceUch.AsFloat:=0;
          taSpecSumUch.AsFloat:=0;
          taSpecQuantFact.AsFloat:=0;
          taSpecPriceInF.AsFloat:=0;
          taSpecSumInF.AsFloat:=0;
          taSpecPriceUchF.AsFloat:=0;
          taSpecSumUchF.AsFloat:=0;

          taSpec.Post;
          ViewInv.EndUpdate;
          AEdit.SelectAll;
          Key:=#0;
        end;
      end;
    end;
  end;//}
end;

procedure TfmAddInv.cxLabel7Click(Sender: TObject);
begin
//  bAddSpecInv:=True;
//  try  fmGoods.Close; delay(10); except end;
//  fmGoods.Show;
  acAddList.Execute;
end;

procedure TfmAddInv.ViewInvDragOver(Sender, Source: TObject; X, Y: Integer;
  State: TDragState; var Accept: Boolean);
begin
  Accept:=False;
  if bDInv then  Accept:=True;
end;

procedure TfmAddInv.ViewInvDragDrop(Sender, Source: TObject; X,
  Y: Integer);
Var iCo:Integer;
    i,j: Integer;
    iNum: Integer;
    Rec:TcxCustomGridRecord;
    Km:Real;
    iMax:Integer;
begin
  if bDInv then
  begin
    ResetAddVars;
    iCo:=fmGoods.ViewGoods.Controller.SelectedRecordCount;
    if iCo>1 then
    begin
      if MessageDlg('�� ������������� ������ �������� ��������� ������� ('+IntToStr(iCo)+' ��.) � ��������������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        with dmO do
        begin
          Memo1.Clear;
          Memo1.Lines.Add('����� .. ���� ���������� �������.');
          ViewInv.BeginUpdate;

          iMax:=1;
          taSpec.First;
          if not taSpec.Eof then
          begin
            taSpec.Last;
            iMax:=taSpecNum.AsInteger+1;
          end;

          for i:=0 to fmGoods.ViewGoods.Controller.SelectedRecordCount-1 do
          begin
            Rec:=fmGoods.ViewGoods.Controller.SelectedRecords[i];

            for j:=0 to Rec.ValueCount-1 do
            begin
              if fmGoods.ViewGoods.Columns[j].Name='ViewGoodsID' then break;
            end;

            iNum:=Rec.Values[j];
          //��� ��� - ����������
            with dmO do
            begin
              if quCardsSel.Locate('ID',iNum,[]) then
              begin
                try
                  if taSpec.Locate('IdGoods',iNum,[])=False then
                  begin
                    taSpec.Append;
                    taSpecNum.AsInteger:=iMax;
                    taSpecIdGoods.AsInteger:=quCardsSelID.AsInteger;
                    taSpecNameG.AsString:=quCardsSelNAME.AsString;
                    taSpecIM.AsInteger:=quCardsSelIMESSURE.AsInteger;
                    taSpecSM.AsString:=prFindKNM(quCardsSelIMESSURE.AsInteger,Km);
                    taSpecQuant.AsFloat:=0;
                    taSpecPriceIn.AsFloat:=0;
                    taSpecSumIn.AsFloat:=0;
                    taSpecPriceUch.AsFloat:=0;
                    taSpecSumUch.AsFloat:=0;
                    taSpecQuantFact.AsFloat:=0;
                    taSpecPriceInF.AsFloat:=0;
                    taSpecSumInF.AsFloat:=0;
                    taSpecPriceUchF.AsFloat:=0;
                    taSpecSumUchF.AsFloat:=0;
                    taSpecKm.AsFloat:=Km;
                    taSpecTCard.AsInteger:=quCardsSelTCard.AsInteger;
                    taSpecNoCalc.AsInteger:=0;
                    taSpecId_Group.AsInteger:=quCardsSelPARENT.AsInteger;
                    taSpecNameGr.AsString:=prFindGrN(quCardsSelPARENT.AsInteger);
                    taSpec.Post;
                    delay(10);
                    inc(iMax);
                  end;
                except
                end;
              end;
            end;
          end;
          ViewInv.EndUpdate;
          Memo1.Lines.Add('���������� ��.');
        end;
      end;
    end;
  end;
end;

procedure TfmAddInv.cxLabel8Click(Sender: TObject);
Var iMax:INteger;
begin
  with dmO do
  begin
    if MessageDlg('���������� ����� ������ ���������� ��� ������ ��������������. ��������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      Memo1.Clear;
      Memo1.Lines.Add('����� ... ���� ������������ � ���������� ������.'); delay(10);

      ViewInv.BeginUpdate;
      iMax:=1;
      taSpec.First;
      if not taSpec.Eof then
      begin
        taSpec.Last;
        iMax:=taSpecNum.AsInteger+1;
      end;

      quAllCards.Active:=False;
      quAllCards.Active:=True;
      quAllCards.First;
      while not quAllCards.Eof do
      begin
        if taSpec.Locate('IdGoods',quAllCardsID.AsInteger,[])=False then
        begin
          taSpec.Append;
          taSpecNum.AsInteger:=iMax;
          taSpecIdGoods.AsInteger:=quAllCardsID.AsInteger;
          taSpecNameG.AsString:=quAllCardsNAME.AsString;
          taSpecIM.AsInteger:=quAllCardsIMESSURE.AsInteger;
          taSpecSM.AsString:=quAllCardsNAMESHORT.AsString;
          taSpecQuant.AsFloat:=0;
          taSpecPriceIn.AsFloat:=0;
          taSpecSumIn.AsFloat:=0;
          taSpecPriceUch.AsFloat:=0;
          taSpecSumUch.AsFloat:=0;
          taSpecQuantFact.AsFloat:=0;
          taSpecPriceInF.AsFloat:=0;
          taSpecSumInF.AsFloat:=0;
          taSpecPriceUchF.AsFloat:=0;
          taSpecSumUchF.AsFloat:=0;
          taSpecKm.AsFloat:=quAllCardsKOEF.AsFloat;
          taSpecTCard.AsInteger:=0;
          taSpecNoCalc.AsInteger:=0;
          taSpecId_Group.AsInteger:=quAllCardsPARENT.AsInteger;
          taSpecNameGr.AsString:=quAllCardsNAMECL.AsString;
          taSpec.Post;
          inc(iMax);
        end;
        quAllCards.Next; delay(10);
      end;
      quAllCards.Active:=False;
      Memo1.Lines.Add('���������� ��.');
      ViewInv.EndUpdate;
    end;
  end;
end;

procedure TfmAddInv.cxLabel2Click(Sender: TObject);
begin
  acDelPos.Execute;
end;

procedure TfmAddInv.cxLabel9Click(Sender: TObject);
begin
{  if MessageDlg('�������� ��������������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
    taSpec.First;
    while not taSpec.Eof do taSpec.Delete;
  end;}
  acDelAll.Execute;
end;

procedure TfmAddInv.acSaveInvExecute(Sender: TObject);
Var Idh,iM:Integer;
    StrWk:String;
    rQf:Real;
    iDate,Ids:Integer;
//    rQ,rSum:Real;
begin
  //��������� ��������������
  Memo1.Clear;
  Memo1.Lines.Add('����� ... ���� ���������� ������.'); delay(10);

  iDate:=Trunc(date);
  if cxDateEdit1.Date>3000 then iDate:=Trunc(cxDateEdit1.Date);

  with dmO do
  with dmORep do
  begin
    if taSpec.State in [dsEdit,dsInsert] then taSpec.Post;

    //������� ������ ������
    ViewInv.BeginUpdate;
    ViewInvC.BeginUpdate;
    ViewBC.BeginUpdate;


    Memo1.Lines.Add('   �������� ������.'); delay(10);
    taSpecC.Active:=False; //����
    taSpecC.CreateDataSet;

    taCalcB.Active:=False; //���� �����
    taCalcB.CreateDataSet;

    iMax:=1;

    taSpec.First;
    while not taSpec.Eof do
    begin
      //��� ����� ���������� taSpec � ������, ���� ������� ������������� � ���� ���
      if taSpecPriceIn.AsFloat=0 then
      begin
        taSpec.Edit;
        taSpecPriceIn.AsFloat:=taSpecPriceInF.AsFloat;
        taSpecSumIn.AsFloat:=taSpecQuant.AsFloat*taSpecPriceInF.AsFloat;
        taSpec.Post;
      end;

      if (taSpecTCard.AsInteger>0)and(taSpecNoCalc.AsInteger=0)  then
      begin  //���� �������������
        prCalcBlInv('    ',taSpecNameG.AsString,taSpecNum.AsInteger,taSpecIdGoods.AsInteger,iDate,taSpecTCard.AsInteger,taSpecQuantFact.AsFloat,taSpecIM.AsInteger,dmORep.taCalcB,Memo1);
      end else //��� ��� ������� �����
      begin

        if taSpecC.Locate('IdGoods',taSpecIdGoods.AsInteger,[]) then
        begin //�������������
          VarsToSpec;
          taSpecC.Edit;
        end else
        begin
          VarsToZero;
          taSpecC.Append;
          taSpecCNum.AsInteger:=iMax;
          inc(iMax);
        end;

        prFindSM(taSpecIM.AsInteger,StrWk,iM); //��������� ��� � �������� �� ���
        taSpecCIdGoods.AsInteger:=taSpecIdGoods.AsInteger;
        taSpecCNameG.AsString:=taSpecNameG.AsString;
        taSpecCIM.AsInteger:=iM;
        taSpecCSM.AsString:=StrWk;
        taSpecCQuant.AsFloat:=taSpecQuant.AsFloat*taSpecKm.AsFloat+Qr;
        taSpecCPriceIn.AsFloat:=0;
        taSpecCPriceUch.AsFloat:=0;
        taSpecCSumIn.AsFloat:=taSpecSumIn.AsFloat+Sum1;
        taSpecCSumUch.AsFloat:=taSpecSumUch.AsFloat+Sum11;
        if abs(taSpecQuant.AsFloat*taSpecKm.AsFloat+Qr)>0 then
        begin
          taSpecCPriceIn.AsFloat:=RoundEx((taSpecSumIn.AsFloat+Sum1)/(taSpecQuant.AsFloat*taSpecKm.AsFloat+Qr)*100)/100;
          taSpecCPriceUch.AsFloat:=RoundEx((taSpecSumUch.AsFloat+Sum11)/(taSpecQuant.AsFloat*taSpecKm.AsFloat+Qr)*100)/100;
        end;
        taSpecCQuantFact.AsFloat:=taSpecQuantFact.AsFloat*taSpecKm.AsFloat+Qf;
        taSpecCPriceInF.AsFloat:=0;
        taSpecCPriceUchF.AsFloat:=0;
        taSpecCSumInF.AsFloat:=taSpecSumInF.AsFloat+Sum2;
        taSpecCSumUchF.AsFloat:=taSpecSumUchF.AsFloat+Sum21;
        if abs(taSpecQuantFact.AsFloat*taSpecKm.AsFloat+Qf)>0 then
        begin
          taSpecCPriceInF.AsFloat:=RoundEx((taSpecSumInF.AsFloat+Sum2)/(taSpecQuantFact.AsFloat*taSpecKm.AsFloat+Qf)*100)/100;
          taSpecCPriceUchF.AsFloat:=RoundEx((taSpecSumUchF.AsFloat+Sum21)/(taSpecQuantFact.AsFloat*taSpecKm.AsFloat+Qf)*100)/100;
        end;
        taSpecCKm.AsFloat:=1;
        taSpecCTCard.AsInteger:=taSpecTCard.AsInteger;
        taSpecCId_Group.AsInteger:=taSpecId_Group.AsInteger;
        taSpecCNameGr.AsString:=taSpecNameGr.AsString;
        taSpecC.Post;
      end;
      taSpec.Next;
    end;
    //������ �������������

    Memo1.Lines.Add('   �������� ����.'); delay(10);

    taSpecC.First;
    while not taSpecC.Eof do
    begin
{      rQ:=prCalcRemn(taSpecCIdGoods.AsInteger,Trunc(cxDateEdit1.Date),cxLookupComboBox1.EditValue);
      if abs(rQ)>0 then
      begin
        rSum:=prCalcRemnSum(taSpecCIdGoods.AsInteger,Trunc(cxDateEdit1.Date),cxLookupComboBox1.EditValue,rQ);
        rQ:=rQ*taSpecCKm.AsFloat; //�� �������� ����� � �������
//        rSum:=rSum*taSpecCKm.AsFloat; //����� � ������� ����� //�������� �.�. ����� ����� � ����� ��.���.
        taSpecC.Edit;
        taSpecCQuant.AsFloat:=rQ;
        taSpecCSumIn.AsFloat:=rSum;
        taSpecCSumUch.AsFloat:=rSum;
        taSpecCPriceIn.AsFloat:=RoundEx(rSum/rQ*100)/100;
        taSpecCPriceUch.AsFloat:=RoundEx(rSum/rQ*100)/100;
        if taSpecCSumInF.AsFloat=0 then
        begin
          taSpecCSumInF.AsFloat:=RoundEx(rSum/rQ*100*taSpecCQuantFact.AsFloat)/100;
          taSpecCPriceInF.AsFloat:=RoundEx(rSum/rQ*100)/100
        end;
        if taSpecCSumUchF.AsFloat=0 then
        begin
          taSpecCSumUchF.AsFloat:=RoundEx(rSum/rQ*100*taSpecCQuantFact.AsFloat)/100;
          taSpecCPriceUchF.AsFloat:=RoundEx(rSum/rQ*100)/100
        end;
        taSpecC.Post;
      end;}
      taSpecC.Next;
    end;

    //������������� ����
    taCalcB.First;
    while not taCalcB.Eof do
    begin
      if taSpecC.Locate('IdGoods',taCalcBIDCARD.AsInteger,[]) then
      begin
        taCalcB.Edit;
        taCalcBPRICEIN.AsFloat:=taSpecCPriceIn.AsFloat;
        taCalcBSUMIN.AsFloat:=taCalcBQUANTC.AsFloat*taSpecCPriceIn.AsFloat;
        taCalcB.Post;
      end;
      taCalcB.Next;
    end;

    //����������
    Memo1.Lines.Add('   ���������� ������ � �����.'); delay(10);

    IDH:=cxTextEdit1.Tag;
    if cxTextEdit1.Tag=0 then IDH:=GetId('InvH');

    quDocsInvId.Active:=False;

    quDocsInvId.ParamByName('IDH').AsInteger:=IDH;
    quDocsInvId.Active:=True;

    quDocsInvId.First;
    if quDocsInvId.RecordCount=0 then quDocsInvId.Append else quDocsInvId.Edit;

    quDocsInvIdID.AsInteger:=IDH;
    quDocsInvIdDATEDOC.AsDateTime:=cxDateEdit1.Date;
    quDocsInvIdNUMDOC.AsString:=cxTextEdit1.Text;
    quDocsInvIdIDSKL.AsInteger:=cxLookupComboBox1.EditValue;
    quDocsInvIdIACTIVE.AsInteger:=0;
    quDocsInvIdITYPE.AsInteger:=0;
    quDocsInvIdSUM1.AsFloat:=0;
    quDocsInvIdSUM11.AsFloat:=0;
    quDocsInvIdSUM2.AsFloat:=0;
    quDocsInvIdSUM21.AsFloat:=0;
    quDocsInvId.Post;

    cxTextEdit1.Tag:=IDH;

    //�������� ������������ ������ � �����
    quSpecINv.Active:=False;
    quSpecInv.ParamByName('IDH').AsInteger:=IDH;
    quSpecInv.Active:=True;

    quSpecInv.First;
    while not quSpecInv.Eof do quSpecInv.Delete;


    taSpec.First;
    while not taSpec.Eof do
    begin
      quSpecInv.Append;
      quSpecInvIDHEAD.AsInteger:=IDH;
      quSpecInvID.AsInteger:=GetId('InvS');
      quSpecInvNUM.AsInteger:=taSpecNum.AsInteger;
      quSpecInvIDCARD.AsInteger:=taSpecIdGoods.AsInteger;
      quSpecInvQUANT.AsFloat:=taSpecQuant.AsFloat;
      quSpecInvSUMIN.AsFloat:=taSpecSumIn.AsFloat;
      quSpecInvSUMUCH.AsFloat:=taSpecSumUch.AsFloat;
      quSpecInvIDMESSURE.AsInteger:=taSpecIM.AsInteger;
      quSpecInvKM.AsFloat:=taSpecKm.AsFloat;
      quSpecInvQUANTFACT.AsFloat:=taSpecQuantFact.AsFloat;
      quSpecInvSUMINFACT.AsFloat:=taSpecSumInF.AsFloat;
      quSpecInvSUMUCHFACT.AsFloat:=taSpecSumUchF.AsFloat;
      quSpecInvTCARD.AsInteger:=taSpecTCard.AsInteger;
      quSpecInvIDGROUP.AsInteger:=taSpecId_Group.AsInteger;
      quSpecInvNOCALC.AsInteger:=taSpecNoCalc.AsInteger;
      quSpecInv.Post;

      taSpec.Next; delay(10);
    end;

    quSpecINv.Active:=False;

    Memo1.Lines.Add('   ���������� ������.'); delay(10);

    //�������� ������������ ������ � �����
    quSpecINvC.Active:=False;
    quSpecInvC.ParamByName('IDH').AsInteger:=IDH;
    quSpecInvC.Active:=True;

    quSpecInvC.First;
    while not quSpecInvC.Eof do quSpecInvC.Delete;

    Sum1:=0;Sum11:=0;Sum2:=0;Sum21:=0;

    taSpecC.First;
    while not taSpecC.Eof do
    begin
      rQf:=taSpecCQuantFact.AsFloat;
      if abs(rQf)<0.00000001 then rQf:=0.000001;
      quSpecInvC.Append;
      quSpecInvCIDHEAD.AsInteger:=IDH;
      quSpecInvCID.AsInteger:=taSpecCNum.AsInteger;
      quSpecInvCNUM.AsInteger:=taSpecCNum.AsInteger;
      quSpecInvCIDCARD.AsInteger:=taSpecCIdGoods.AsInteger;
      quSpecInvCQUANT.AsFloat:=taSpecCQuant.AsFloat;
      quSpecInvCSUMIN.AsFloat:=taSpecCSumIn.AsFloat;
      quSpecInvCSUMUCH.AsFloat:=taSpecCSumUch.AsFloat;
      quSpecInvCIDMESSURE.AsInteger:=taSpecCIM.AsInteger;
      quSpecInvCKM.AsFloat:=taSpecCKm.AsFloat;
      quSpecInvCQUANTFACT.AsFloat:=rQf;
      quSpecInvCSUMINFACT.AsFloat:=taSpecCSumInF.AsFloat;
      quSpecInvCSUMUCHFACT.AsFloat:=taSpecCSumUchF.AsFloat;
      quSpecInvCTCARD.AsInteger:=taSpecCTCard.AsInteger;
      quSpecInvCIDGROUP.AsInteger:=taSpecCId_Group.AsInteger;
      quSpecInvC.Post;

      Sum1:=Sum1+taSpecCSumIn.AsFloat; Sum11:=Sum11+taSpecCSumUch.AsFloat;
      Sum2:=Sum2+taSpecCSumInF.AsFloat; Sum21:=Sum21+taSpecCSumUchF.AsFloat;

      taSpecC.Next; delay(10);
    end;
    quSpecINvC.Active:=False;

    Memo1.Lines.Add('   ���������� �����.'); delay(10);

    quSpecInvBC.Active:=False;
    quSpecInvBC.ParamByName('IDH').AsInteger:=IdH;
    quSpecInvBC.Active:=True;

    while not quSpecInvBC.Eof do quSpecInvBC.Delete;

    IdS:=1;
    taCalcB.First;
    while not taCalcB.Eof do
    begin
      quSpecInvBC.Append;
      quSpecInvBCIDHEAD.AsInteger:=Idh;
      quSpecInvBCIDB.AsInteger:=taCalcBID.AsInteger;
      quSpecInvBCID.AsInteger:=Ids; //��� ��� ������
      quSpecInvBCCODEB.AsInteger:=taCalcBCODEB.AsInteger;
      quSpecInvBCNAMEB.AsString:=taCalcBNAMEB.AsString;
      quSpecInvBCQUANT.AsFloat:=taCalcBQUANT.AsFloat;
      quSpecInvBCPRICEOUT.AsFloat:=taCalcBPRICEOUT.AsFloat;
      quSpecInvBCSUMOUT.AsFloat:=taCalcBSUMOUT.AsFloat;
      quSpecInvBCIDCARD.AsInteger:=taCalcBIDCARD.AsInteger;
      quSpecInvBCNAMEC.AsString:=taCalcBNAMEC.AsString;
      quSpecInvBCQUANTC.AsFloat:=taCalcBQUANTC.AsFloat;
      quSpecInvBCPRICEIN.AsFloat:=taCalcBPRICEIN.AsFloat;
      quSpecInvBCSUMIN.AsFloat:=taCalcBSUMIN.AsFloat;
      quSpecInvBCIM.AsInteger:=taCalcBIM.AsInteger;
      quSpecInvBCSM.AsString:=taCalcBSM.AsString;
      quSpecInvBCSB.AsString:=taCalcBSB.AsString;
      quSpecInvBC.Post;

      inc(Ids);
      taCalcB.Next;
    end;

    quSpecInvBC.Active:=False;

    quDocsInvId.Edit;
    quDocsInvIdSUM1.AsFloat:=Sum1;
    quDocsInvIdSUM11.AsFloat:=Sum11;
    quDocsInvIdSUM2.AsFloat:=Sum2;
    quDocsInvIdSUM21.AsFloat:=Sum21;
    quDocsInvId.Post;

    quDocsInvId.Active:=False;

    fmDocsInv.ViewDocsInv.BeginUpdate;
    quDocsInvSel.FullRefresh;
    quDocsInvSel.Locate('ID',IDH,[]);
    fmDocsInv.ViewDocsInv.EndUpdate;

    ViewInv.EndUpdate;
    ViewInvC.EndUpdate;
    ViewBC.EndUpdate;
  end;
  Memo1.Lines.Add('���������� ��.'); delay(10);
end;

procedure TfmAddInv.ViewInvEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  iCol:=0;
  if ViewInv.Controller.FocusedColumn.Name='ViewInvQuantFact' then iCol:=1;
  if ViewInv.Controller.FocusedColumn.Name='ViewInvPriceInF' then iCol:=2;
  if ViewInv.Controller.FocusedColumn.Name='ViewInvSumInF' then iCol:=3;
  if ViewInv.Controller.FocusedColumn.Name='ViewInvPriceUchF' then iCol:=4;
  if ViewInv.Controller.FocusedColumn.Name='ViewInvSumUchF' then iCol:=5;
end;

procedure TfmAddInv.taSpecQuantFactChange(Sender: TField);
begin
  //���������� �����
  if iCol=1 then
  begin
    taSpecSumInF.AsCurrency:=taSpecPriceInF.AsCurrency*taSpecQuantFact.AsFloat;
    taSpecSumUchF.AsCurrency:=taSpecPriceUchF.AsCurrency*taSpecQuantFact.AsFloat;
  end;
end;

procedure TfmAddInv.taSpecPriceInFChange(Sender: TField);
begin
  //���������� ����
  if iCol=2 then
  begin
    taSpecSumInF.AsCurrency:=taSpecPriceInF.AsCurrency*taSpecQuantFact.AsFloat;
//    taSpecSumUchF.AsCurrency:=taSpecPriceUchF.AsCurrency*taSpecQuantFact.AsFloat;
  end;
end;

procedure TfmAddInv.taSpecSumInFChange(Sender: TField);
begin
  if iCol=3 then  //���������� �����
  begin
    if abs(taSpecQuantFact.AsFloat)>0 then taSpecPriceInF.AsCurrency:=RoundEx(taSpecSumInF.AsCurrency/taSpecQuantFact.AsFloat*100)/100;
  end;
end;

procedure TfmAddInv.taSpecPriceUchFChange(Sender: TField);
begin
  //���������� ���� �������
  if iCol=4 then
  begin
//    taSpecSumInF.AsCurrency:=taSpecPriceInF.AsCurrency*taSpecQuantFact.AsFloat;
    taSpecSumUchF.AsCurrency:=taSpecPriceUchF.AsCurrency*taSpecQuantFact.AsFloat;
  end;
end;

procedure TfmAddInv.taSpecSumUchFChange(Sender: TField);
begin
  if iCol=5 then  //���������� �����
  begin
    if abs(taSpecQuantFact.AsFloat)>0 then taSpecPriceUchF.AsCurrency:=RoundEx(taSpecSumUchF.AsCurrency/taSpecQuantFact.AsFloat*100)/100;
  end;
end;

procedure TfmAddInv.Label3Click(Sender: TObject);
Var rQ:Real;
    rSum:Real;
begin //������ ��������
  iCol:=0;
  with dmO do
  with dmORep do
  begin
    Memo1.Clear;
    Memo1.Lines.Add('����� ... ���� ������ ��������.'); delay(10);

    taSpec.First;
    while not taSpec.Eof do
    begin
      rQ:=prCalcRemn(taSpecIdGoods.AsInteger,Trunc(cxDateEdit1.Date),cxLookupComboBox1.EditValue);
      if abs(rQ)>0 then
      begin
        rSum:=prCalcRemnSum(taSpecIdGoods.AsInteger,Trunc(cxDateEdit1.Date),cxLookupComboBox1.EditValue,rQ);
        if taSpecKm.AsFloat<>0 then rQ:=rQ/taSpecKm.AsFloat; //�� �������� ����� � �������
//        rSum:=rSum*taSpecKm.AsFloat; //����� � ������� ����� //�������� �.�. ����� ����� � ����� ��.���.
        taSpec.Edit;
        taSpecQuant.AsFloat:=rQ;
        taSpecSumIn.AsFloat:=rSum;
        taSpecSumUch.AsFloat:=rSum;
        taSpecPriceIn.AsFloat:=RoundEx(rSum/rQ*100)/100;
        taSpecPriceUch.AsFloat:=RoundEx(rSum/rQ*100)/100;
        if taSpecSumInF.AsFloat=0 then
        begin
          taSpecSumInF.AsFloat:=RoundEx(rSum/rQ*100*taSpecQuantFact.AsFloat)/100;
          taSpecPriceInF.AsFloat:=RoundEx(rSum/rQ*100)/100
        end;
        if taSpecSumUchF.AsFloat=0 then
        begin
          taSpecSumUchF.AsFloat:=RoundEx(rSum/rQ*100*taSpecQuantFact.AsFloat)/100;
          taSpecPriceUchF.AsFloat:=RoundEx(rSum/rQ*100)/100
        end;
        taSpec.Post;
      end;
      taSpec.Next;
    end;
    Memo1.Lines.Add('������ ��.'); delay(10);
  end;
end;

procedure TfmAddInv.Label4Click(Sender: TObject);
begin
  Memo1.Clear;
  Memo1.Lines.Add('����� ... ���� ���������� ��������.'); delay(10);

  taSpec.First;
  while not taSpec.Eof do
  begin
    taSpec.Edit;

    taSpecQuantFact.AsFloat:=taSpecQuant.AsFloat;
    taSpecPriceInF.AsFloat:=taSpecPriceIn.AsFloat;
    taSpecSumInF.AsFloat:=taSpecSumIn.AsFloat;
    taSpecPriceUchF.AsFloat:=taSpecPriceUch.AsFloat;
    taSpecSumUchF.AsFloat:=taSpecSumUch.AsFloat;

    taSpec.Post;


    taSpec.Next;
  end;
  Memo1.Lines.Add('��.'); delay(10);
end;

procedure TfmAddInv.Label6Click(Sender: TObject);
begin
  Memo1.Clear;
  Memo1.Lines.Add('����� ... ���� ���������� ��������.'); delay(10);

  taSpec.First;
  while not taSpec.Eof do
  begin
    taSpec.Edit;

    taSpecQuantFact.AsFloat:=0;
    taSpecPriceInF.AsFloat:=0;
    taSpecSumInF.AsFloat:=0;
    taSpecPriceUchF.AsFloat:=0;
    taSpecSumUchF.AsFloat:=0;

    taSpec.Post;


    taSpec.Next;
  end;
  Memo1.Lines.Add('��.'); delay(10);
end;

procedure TfmAddInv.taSpecCBeforePost(DataSet: TDataSet);
begin
  taSpecCQuantDif.AsFloat:=taSpecCQuantFact.AsFloat-taSpecCQuant.AsFloat;
  taSpecCSumInDif.AsFloat:=taSpecCSumInF.AsFloat-taSpecCSumIn.AsFloat;
  taSpecCSumUchDif.AsFloat:=taSpecCSumUchF.AsFloat-taSpecCSumUch.AsFloat;
end;

procedure TfmAddInv.cxButton3Click(Sender: TObject);
Var StrWk:String;
    i:Integer;
begin
  taSpec.IndexName:='taSpecIndex1';
  if cxRadioButton3.Checked then
  begin
    if PageControl1.ActivePageIndex=0 then prExportExel1(ViewInv,dsSpec,taSpec);
    if PageControl1.ActivePageIndex=1 then prExportExel1(ViewInvC,dsSpecC,taSpecC);
  end;
  if cxRadioButton1.Checked then
  begin
    if PageControl1.ActivePageIndex=0 then
    begin
      taSpec.Filter:=ViewInv.DataController.Filter.FilterText;
      taSpec.Filtered:=True;

      RepInv.LoadFromFile(CurDir + 'InvRep1.frf');

      frVariables.Variable['DocNum']:=cxTextEdit1.Text;
      frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date);
      frVariables.Variable['DocStore']:=cxLookupComboBox1.Text;

      StrWk:=ViewInv.DataController.Filter.FilterCaption;
      while Pos('AND',StrWk)>0 do
      begin
        i:=Pos('AND',StrWk);
        StrWk[i]:=' ';
        StrWk[i+1]:='�';
        StrWk[i+2]:=' ';
      end;
      while Pos('and',StrWk)>0 do
      begin
        i:=Pos('and',StrWk);
        StrWk[i]:=' ';
        StrWk[i+1]:='�';
        StrWk[i+2]:=' ';
      end;

      frVariables.Variable['DocPars']:=StrWk;

      RepInv.ReportName:='��������� �������������� (��).';
      RepInv.PrepareReport;
      RepInv.ShowPreparedReport;

      taSpec.Filter:='';
      taSpec.Filtered:=False;
    end;
    if PageControl1.ActivePageIndex=1 then
    begin
      taSpecC.Filter:=ViewInv.DataController.Filter.FilterText;
      taSpecC.Filtered:=True;

      RepInv.LoadFromFile(CurDir + 'InvRep11.frf');

      frVariables.Variable['DocNum']:=cxTextEdit1.Text;
      frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date);
      frVariables.Variable['DocStore']:=cxLookupComboBox1.Text;

      StrWk:=ViewInv.DataController.Filter.FilterCaption;
      while Pos('AND',StrWk)>0 do
      begin
        i:=Pos('AND',StrWk);
        StrWk[i]:=' ';
        StrWk[i+1]:='�';
        StrWk[i+2]:=' ';
      end;
      while Pos('and',StrWk)>0 do
      begin
        i:=Pos('and',StrWk);
        StrWk[i]:=' ';
        StrWk[i+1]:='�';
        StrWk[i+2]:=' ';
      end;

      frVariables.Variable['DocPars']:=StrWk;

      RepInv.ReportName:='��������� �������������� (�).';
      RepInv.PrepareReport;
      RepInv.ShowPreparedReport;

      taSpecC.Filter:='';
      taSpecC.Filtered:=False;
    end;
  end;
  if cxRadioButton2.Checked then
  begin
    if PageControl1.ActivePageIndex=0 then
    begin
      taSpec.Filter:=ViewInv.DataController.Filter.FilterText;
      taSpec.Filtered:=True;

      RepInv.LoadFromFile(CurDir + 'InvRep2.frf');

      frVariables.Variable['DocNum']:=cxTextEdit1.Text;
      frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date);
      frVariables.Variable['DocStore']:=cxLookupComboBox1.Text;

      StrWk:=ViewInv.DataController.Filter.FilterCaption;
      while Pos('AND',StrWk)>0 do
      begin
        i:=Pos('AND',StrWk);
        StrWk[i]:=' ';
        StrWk[i+1]:='�';
        StrWk[i+2]:=' ';
      end;
      while Pos('and',StrWk)>0 do
      begin
        i:=Pos('and',StrWk);
        StrWk[i]:=' ';
        StrWk[i+1]:='�';
        StrWk[i+2]:=' ';
      end;

      frVariables.Variable['DocPars']:=StrWk;

      RepInv.ReportName:='��������� �������������� (��).';
      RepInv.PrepareReport;
      RepInv.ShowPreparedReport;

      taSpec.Filter:='';
      taSpec.Filtered:=False;
    end;
    if PageControl1.ActivePageIndex=1 then
    begin
      taSpecC.Filter:=ViewInv.DataController.Filter.FilterText;
      taSpecC.Filtered:=True;

      RepInv.LoadFromFile(CurDir + 'InvRep22.frf');

      frVariables.Variable['DocNum']:=cxTextEdit1.Text;
      frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date);
      frVariables.Variable['DocStore']:=cxLookupComboBox1.Text;

      StrWk:=ViewInv.DataController.Filter.FilterCaption;
      while Pos('AND',StrWk)>0 do
      begin
        i:=Pos('AND',StrWk);
        StrWk[i]:=' ';
        StrWk[i+1]:='�';
        StrWk[i+2]:=' ';
      end;
      while Pos('and',StrWk)>0 do
      begin
        i:=Pos('and',StrWk);
        StrWk[i]:=' ';
        StrWk[i+1]:='�';
        StrWk[i+2]:=' ';
      end;

      frVariables.Variable['DocPars']:=StrWk;

      RepInv.ReportName:='��������� �������������� (�).';
      RepInv.PrepareReport;
      RepInv.ShowPreparedReport;

      taSpecC.Filter:='';
      taSpecC.Filtered:=False;
    end;
  end;
  taSpec.IndexName:='taSpecIndex2';
end;

procedure TfmAddInv.acAddListExecute(Sender: TObject);
begin
  bAddSpecInv:=True;
  fmGoods.Show;
end;

procedure TfmAddInv.acDelPosExecute(Sender: TObject);
begin
  if taSpec.RecordCount>0 then taSpec.Delete;
end;

procedure TfmAddInv.acDelAllExecute(Sender: TObject);
begin
  if MessageDlg('�������� ��������������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
    taSpec.First;
    while not taSpec.Eof do taSpec.Delete;
  end;
end;

procedure TfmAddInv.N1Click(Sender: TObject);
begin
//�� ������������
  ViewInv.BeginUpdate;
  taSpec.First;
  while not taSpec.Eof do
  begin
    taSpec.Edit;
    taSpecNoCalc.AsInteger:=1;
    taSpec.Post;
    taSpec.Next;
  end;
  taSpec.First;
  ViewInv.EndUpdate;
end;

procedure TfmAddInv.N2Click(Sender: TObject);
begin
//��� ������������
  ViewInv.BeginUpdate;
  taSpec.First;
  while not taSpec.Eof do
  begin
    taSpec.Edit;
    taSpecNoCalc.AsInteger:=0;
    taSpec.Post;
    taSpec.Next;
  end;
  taSpec.First;
  ViewInv.EndUpdate;
end;

procedure TfmAddInv.Memo1DblClick(Sender: TObject);
begin
  fmMessage:=tfmMessage.Create(Application);
  fmMessage.Memo1.Lines:=Memo1.Lines;
  fmMessage.ShowModal;
  fmMessage.Release;
end;

procedure TfmAddInv.acCalc1Execute(Sender: TObject);
begin
  Memo1.Clear;
  taSpec.First;
  while not taSpec.Eof do
  begin
    if taSpecC.Locate('IdGoods',taSpecIdGoods.AsInteger,[]) then
    begin
      if taSpecSumUch.AsFloat<>taSpecCSumUch.AsFloat then
      begin
        Memo1.Lines.Add(taSpecNum.AsString+'  '+taSpecCNum.AsString+'  '+taSpecIdGoods.AsString+'  '+FloatToStr(taSpecSumUch.AsFloat-taSpecCSumUch.AsFloat));
      end;
    end;
    taSpec.Next;
  end;
end;

end.
