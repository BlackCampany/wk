unit CalcSeb;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  StdCtrls, cxButtons, ExtCtrls, DBClient, cxContainer, cxTextEdit,
  cxMaskEdit, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox, cxCalendar, FIBDataSet, pFIBDataSet, cxRadioGroup,
  cxMemo;

type
  TfmCalcSeb = class(TForm)
    Panel2: TPanel;
    GrSeb: TcxGrid;
    ViewSeb: TcxGridDBTableView;
    LevelSeb: TcxGridLevel;
    Label2: TLabel;
    Label3: TLabel;
    cxButton2: TcxButton;
    cxButton1: TcxButton;
    taSeb: TClientDataSet;
    dsSeb: TDataSource;
    taSebIDCARD: TIntegerField;
    taSebCURMESSURE: TIntegerField;
    taSebNETTO: TFloatField;
    taSebBRUTTO: TFloatField;
    taSebNAME: TStringField;
    taSebNAMESHORT: TStringField;
    taSebKOEF: TFloatField;
    taSebKNB: TFloatField;
    taSebPRICE1: TCurrencyField;
    taSebSUM1: TCurrencyField;
    taSebPRICE2: TCurrencyField;
    taSebSUM2: TCurrencyField;
    ViewSebIDCARD: TcxGridDBColumn;
    ViewSebCURMESSURE: TcxGridDBColumn;
    ViewSebNETTO: TcxGridDBColumn;
    ViewSebBRUTTO: TcxGridDBColumn;
    ViewSebNAME: TcxGridDBColumn;
    ViewSebNAMESHORT: TcxGridDBColumn;
    ViewSebKOEF: TcxGridDBColumn;
    ViewSebKNB: TcxGridDBColumn;
    ViewSebPRICE1: TcxGridDBColumn;
    ViewSebSUM1: TcxGridDBColumn;
    ViewSebPRICE2: TcxGridDBColumn;
    ViewSebSUM2: TcxGridDBColumn;
    Label4: TLabel;
    quMHAll: TpFIBDataSet;
    quMHAllID: TFIBIntegerField;
    quMHAllPARENT: TFIBIntegerField;
    quMHAllITYPE: TFIBIntegerField;
    quMHAllNAMEMH: TFIBStringField;
    quMHAllDEFPRICE: TFIBIntegerField;
    quMHAllNAMEPRICE: TFIBStringField;
    dsMHAll: TDataSource;
    Label1: TLabel;
    cxButton3: TcxButton;
    cxRadioButton1: TcxRadioButton;
    cxRadioButton2: TcxRadioButton;
    Memo1: TcxMemo;
    taSpecC: TClientDataSet;
    taSpecCNum: TIntegerField;
    taSpecCIdGoods: TIntegerField;
    taSpecCNameG: TStringField;
    taSpecCIM: TIntegerField;
    taSpecCSM: TStringField;
    taSpecCQuant: TFloatField;
    taSpecCPriceIn: TFloatField;
    taSpecCSumIn: TFloatField;
    taSpecCPriceUch: TFloatField;
    taSpecCSumUch: TFloatField;
    taSpecCKm: TFloatField;
    procedure FormCreate(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure Memo1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function prCalcSebBludo:Real;
    Procedure prCalcQSeb(sBeg,sName:String;iCode,iDate,iTCard:Integer;rQ:Real;IdM:INteger;Memo1:TcxMemo);
  end;

var
  fmCalcSeb: TfmCalcSeb;
  iMax:Integer;

implementation

uses dmOffice, TCard, Un1, Message;

{$R *.dfm}

function TfmCalcSeb.prCalcSebBludo:Real;
Var sBeg:String;
    rPrice:Real;
    rMessure:Real;
    rSum:Real;
    StrWk:String;
begin
  with dmO do
  begin
    sBeg:='';
    rSum:=0;
    taSpecC.Active:=False;
    taSpecC.CreateDataSet;
    //���������� ���-�� �������� ������� �����                                                                           //��� ����� �.�.  ������ ������ �����������
    prCalcQSeb(sBeg,quTSpecNAME.AsString,quTSpecIDCARD.AsInteger,Trunc(Date),1,(quTSpecNETTO.AsFloat*quTSpecKOEF.AsFloat),quTSpecCURMESSURE.AsInteger,Memo1);

    Memo1.Lines.Add('');
    taSpecC.First;
    while not taSpecC.Eof do
    begin
      prCalcLastPrice1.ParamByName('IDGOOD').AsInteger:=taSpecCIdGoods.AsInteger;
      prCalcLastPrice1.ExecProc;
      rPrice:=prCalcLastPrice1.ParamByName('PRICEIN').AsCurrency;
      rMessure:=prCalcLastPrice1.ParamByName('KOEF').AsFloat;
      if rMessure<>1 then //���� �������� ��������������
      begin
        if rMessure<>0 then rPrice:=rPrice/rMessure;
      end;
      rSum:=rSum+rPrice*taSpecCQuant.AsFloat;

      StrWk:='        '+taSpecCNameG.AsString+' ('+INtToStr(taSpecCIdGoods.AsInteger)+') ���-��: '+FloatToStr(RoundEx(taSpecCQuant.AsFloat*1000)/1000)+'  ���� ��: '+FloatToStr(RoundEx(rPrice*100)/100)+'  �����: '+FloatToStr(RoundEx(taSpecCQuant.AsFloat*rPrice*100)/100);
      Memo1.Lines.Add(StrWk);
//sName+' ('+INtToStr(iCode)+'). �� ����. ���-��: '+FloatToStr(RoundEx(rQ*1000)/1000)+' '+sM);
      taSpecC.Next;
    end;
    taSpecC.Active:=False;
    Result:=rSum;
    Memo1.Lines.Add('');
  end;
end;


Procedure TfmCalcSeb.prCalcQSeb(sBeg,sName:String;iCode,iDate,iTCard:Integer;rQ:Real;IdM:INteger;Memo1:TcxMemo);
Var iTC,iPCount:INteger;
    QT1:TpFIBDataSet;
    rQ1,kBrutto,kM,MassaB:Real;
    iMain:INteger;
    sM:String;
    Qr:Real;
begin
  sBeg:=sBeg+'    ';
  iMax:=1;
  with dmO do
  begin
    //���
    if iTCard=1 then
    begin
      iTC:=0; iPCount:=0; MassaB:=1;
      quFindTCard.Active:=False;
      quFindTCard.ParamByName('IDCARD').AsInteger:=iCode;
      quFindTCard.ParamByName('IDATE').AsDateTime:=iDate;
      quFindTCard.Active:=True;
      if quFindTCard.RecordCount>0 then
      begin
        iTC:=quFindTCardID.AsInteger;
        iPCount:=quFindTCardPCOUNT.AsInteger;
        MassaB:=quFindTCardPVES.AsFloat/1000;
        if prFindMT(IdM)=1 then MassaB:=1;
        prFindSM(IdM,sM,iMain); //��� �������� �������� ������� ���������
        Memo1.Lines.Add(sBeg+sName+' ('+INtToStr(iCode)+'). �� ����. ���-��: '+FloatToStr(RoundEx(rQ*1000)/1000)+' '+sM);
      end else
      begin
        inc(iErr);
        Memo1.Lines.Add(SBeg+'������: �� �� �������.')
      end;
      quFindTCard.Active:=False;
      if iTC>0 then
      begin
        QT1:=TpFIBDataSet.Create(Owner);
        QT1.Active:=False;
        QT1.Database:=OfficeRnDb;
        QT1.Transaction:=trSel;
        QT1.SelectSQL.Clear;
        QT1.SelectSQL.Add('SELECT CS.ID,CS.IDCARD,CS.CURMESSURE,CS.NETTO,CS.BRUTTO,CS.KNB,');
        QT1.SelectSQL.Add('CD.NAME,CD.TCARD');
        QT1.SelectSQL.Add('FROM OF_CARDSTSPEC CS');
        QT1.SelectSQL.Add('left join of_cards cd on cd.ID=CS.IDCARD');
        QT1.SelectSQL.Add('where IDC='+IntToStr(iCode)+' and IDT='+IntToStr(iTC));
        QT1.SelectSQL.Add('ORDER BY CS.ID');
        QT1.Active:=True;

        QT1.First;
        while not QT1.Eof do
        begin
          rQ1:=QT1.FieldByName('NETTO').AsFloat;
          kM:=prFindKM(QT1.FieldByName('CURMESSURE').AsInteger);
          rQ1:=rQ1*kM;//��������� � �������� �������
          prFindSM(QT1.FieldByName('CURMESSURE').AsInteger,sM,iMain); //��� �������� �������� ������� ���������
          //���� ������ �� ������ ����
          kBrutto:=prFindBrutto(QT1.FieldByName('IDCARD').AsInteger,iDate);
          rQ1:=rQ1*(100+kBrutto)/100; //��� ����� � ������
          rQ1:=rQ1/iPCount; //��� ����� �� 1-� ������ ���� �������� �� �������
          rQ1:=rQ1*rQ/MassaB; //��� ����� �� ��� ���-�� ������

//          Memo1.Lines.Add(sBeg+QT1.FieldByName('NAME').AsString+' ('+QT1.FieldByName('IDCARD').AsString+'). �� ����. ���-��: '+FloatToStr(RoundEx(rQ1*1000)/1000)+' '+sM);
          prCalcQSeb(sBeg,QT1.FieldByName('NAME').AsString,QT1.FieldByName('IDCARD').AsInteger,iDate,QT1.FieldByName('TCARD').AsInteger,rQ1,iMain,Memo1);

          QT1.Next;
        end;
        QT1.Active:=False;
        QT1.Free;
      end;
    end else
    begin
      //��������� � �������� ������� �� ������ ������ ����� ������ ��� - �� ����
      kM:=prFindKM(IdM);
      prFindSM(IdM,sM,iMain); //��� �������� �������� ������� ���������
      rQ:=rQ*kM;              //������ ������� - ��� ������ �� �����������

      Memo1.Lines.Add(sBeg+sName+' ('+IntToStr(iCode)+'). �����.  ���-��: '+FloatToStr(RoundEx(rQ*1000)/1000)+' '+sM);

      //������ ���� ��� � �������� � � ������
      if taSpecC.Locate('IdGoods',iCode,[]) then
      begin //�������������
        Qr:=taSpecCQuant.AsFloat;
        taSpecC.Edit;
      end else
      begin
        Qr:=0;
        taSpecC.Append;
        taSpecCNum.AsInteger:=iMax;
        inc(iMax);
      end;

      taSpecCIdGoods.AsInteger:=iCode;
      taSpecCNameG.AsString:=sName;
      taSpecCIM.AsInteger:=iMain;
      taSpecCSM.AsString:=sM;

      taSpecCQuant.AsFloat:=rQ+Qr;
      taSpecCPriceIn.AsFloat:=0;
      taSpecCSumIn.AsFloat:=0;
      taSpecCPriceUch.AsFloat:=0;
      taSpecCSumUch.AsFloat:=0;
      taSpecCKm.AsFloat:=1;
      taSpecC.Post;

    end;
  end;
end;

procedure TfmCalcSeb.FormCreate(Sender: TObject);
begin
  GrSeb.Align:=AlClient;
  Memo1.Clear;
end;

procedure TfmCalcSeb.cxButton2Click(Sender: TObject);
begin
  close;
end;

procedure TfmCalcSeb.cxButton1Click(Sender: TObject);
Var rPrice,rMessure,k,rSum:Real;
    bBludo:Boolean;
begin
  with dmO do
  begin
    Memo1.Clear;
    if cxRadioButton1.Checked then
    begin
      taSeb.Active:=False;
      taSeb.CreateDataSet;
      dsTSpec.DataSet:=Nil;

      with fmTCard do
      begin
        taTSpec.First;
        while not taTSpec.Eof do
        begin
        //��������� � �� ����� �� ���???
          bBludo:=False;
          quFindCard.Active:=False;
          quFindCard.ParamByName('IDCARD').AsInteger:=taTSpecIDCARD.AsInteger;
          quFindCard.Active:=True;
          if quFindCardTCARD.AsInteger=1 then bBludo:=True;
          quFindCard.Active:=False;

          if bBludo=False then
          begin
            prCalcLastPrice1.ParamByName('IDGOOD').AsInteger:=taTSpecIDCARD.AsInteger;
            prCalcLastPrice1.ExecProc;
            rPrice:=prCalcLastPrice1.ParamByName('PRICEIN').AsCurrency;
            rMessure:=prCalcLastPrice1.ParamByName('KOEF').AsFloat;
            if (rMessure<>0) then
            begin
              if taTSpecKm.AsFloat<>rMessure then
              begin
                k:=taTSpecKm.AsFloat/rMessure;
              end else k:=1;
            end else k:=1;
            rSum:=RoundEx(taTSpecBRUTTO.AsFloat*rPrice*k*100)/100;
          end else
          begin //��� ����� - ���� �������� ������������� �����
            rSum:=prCalcSebBludo;
            if taTSpecBRUTTO.AsFloat<>0 then rPrice:=rSum/taTSpecBRUTTO.AsFloat
            else rPrice:=0;
          end;

          taSeb.Append;
          taSebIDCARD.AsInteger:=taTSpecIDCARD.AsInteger;
          taSebCURMESSURE.AsInteger:=taTSpecIdM.AsInteger;
          taSebNETTO.AsFloat:=taTSpecNETTO.AsFloat;
          taSebBRUTTO.AsFloat:=taTSpecBRUTTO.AsFloat;
          taSebNAME.AsString:=taTSpecNAME.AsString;
          taSebNAMESHORT.AsString:=taTSpecSM.AsString;
          taSebKOEF.AsFloat:=taTSpecKm.AsFloat;
          taSebKNB.AsFloat:=taTSpecBrutto.AsFloat;
          taSebPRICE1.AsCurrency:=rPrice;
          taSebSUM1.AsCurrency:=rSum;
          taSebPRICE2.AsCurrency:=0;
          taSebSUM2.AsCurrency:=0;
          taSeb.Post;

          taTSpec.Next;
        end;
      end;
    end;
  end;
end;

procedure TfmCalcSeb.cxButton3Click(Sender: TObject);
begin
//� ������
  prExportExel1(ViewSeb,dsSeb,taSeb);
end;

procedure TfmCalcSeb.Memo1DblClick(Sender: TObject);
begin
  fmMessage:=tfmMessage.Create(Application);
  fmMessage.Memo1.Lines:=Memo1.Lines;
  fmMessage.ShowModal;
  fmMessage.Release;
end;

end.
