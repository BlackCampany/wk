unit RepSkidkiFull;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ComCtrls, cxCurrencyEdit, cxTextEdit,
  ExtCtrls, SpeedBar, Placemnt, Grids, DBGrids,
  ComObj, ActiveX, Excel2000, OleServer, ExcelXP, FR_Class, FR_DSet,
  FR_DBSet,cxLookAndFeelPainters;

type
  TfmRepSkidkiFull = class(TForm)
    GridSkidki: TcxGrid;
    ViewSkidki: TcxGridDBTableView;
    LevelSkidki: TcxGridLevel;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    frRepWB: TfrReport;
    FormPlacement1: TFormPlacement;
    dsRepWB1: TfrDBDataSet;
    ViewSkidkiID: TcxGridDBColumn;
    ViewSkidkiQUESTS: TcxGridDBColumn;
    ViewSkidkiTABSUM: TcxGridDBColumn;
    ViewSkidkiBEGTIME: TcxGridDBColumn;
    ViewSkidkiENDTIME: TcxGridDBColumn;
    ViewSkidkiDISCONT: TcxGridDBColumn;
    ViewSkidkiOPERTYPE: TcxGridDBColumn;
    ViewSkidkiSTATION: TcxGridDBColumn;
    ViewSkidkiNUMZ: TcxGridDBColumn;
    ViewSkidkiPERSONALNAME: TcxGridDBColumn;
    ViewSkidkiDISNAME: TcxGridDBColumn;
    ViewSkidkiDSUM: TcxGridDBColumn;
    ViewSkidkiDTIME: TcxGridDBColumn;
    ViewSkidkiSPERS: TcxGridDBColumn;
    ViewSkidkiSUMFULL: TcxGridDBColumn;
    ViewSkidkiPLATCARDNAME: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SpeedItem2Click(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function IsOLEObjectInstalled(Name: String): boolean;

var
  fmRepSkidkiFull: TfmRepSkidkiFull;

implementation

uses Un1, Dm, Period2, Period, spisok,cxDBCheckListBox,cxCheckBox;

{$R *.dfm}

function IsOLEObjectInstalled(Name: String): boolean;
var
  ClassID: TCLSID;
  Rez : HRESULT;
begin

// ���� CLSID OLE-�������
  Rez := CLSIDFromProgID(PWideChar(WideString(Name)), ClassID);
  if Rez = S_OK then
 // ������ ������
    Result := true
  else
    Result := false;
end;



procedure TfmRepSkidkiFull.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  ViewSkidki.RestoreFromIniFile(CurDir+GridIni);
end;

procedure TfmRepSkidkiFull.SpeedItem1Click(Sender: TObject);
begin
 close;
end;

procedure TfmRepSkidkiFull.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  dmC.quSkidkiFull.Active:=False;
  ViewSkidki.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmRepSkidkiFull.SpeedItem2Click(Sender: TObject);
begin
 fmPeriod:=TfmPeriod.Create(Application); //��� ���� ������������

  fmPeriod.ShowModal;
  if fmPeriod.ModalResult=mrOk then
  begin
    ViewSkidki.BeginUpdate;
    fmRepSkidkiFull.Caption:='������ �� ������ c '+FormatDateTime('dd.mm.yyyy',TrebSel.DateFrom)+' �� '+FormatDateTime('dd.mm.yyyy',TrebSel.DateTo);

    with dmC do
    begin
      quSkidkiFull.Active:=False;
      quSkidkiFull.ParamByName('DATEB').AsDateTime:=TrebSel.DateFrom;
      quSkidkiFull.ParamByName('DATEE').AsDateTime:=TrebSel.DateTo;
      quSkidkiFull.Active:=True;

      fmRepSkidkiFull.Show;
    end;
    ViewSkidki.EndUpdate;
  end;

end;

procedure TfmRepSkidkiFull.SpeedItem3Click(Sender: TObject);
begin
  prNExportExel4(ViewSkidki);
end;

end.
