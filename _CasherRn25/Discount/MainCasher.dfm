object fmMainCasher: TfmMainCasher
  Left = 0
  Top = 2
  Width = 1025
  Height = 738
  Caption = #1056#1072#1073#1086#1095#1077#1077' '#1084#1077#1089#1090#1086' '#1082#1072#1089#1089#1080#1088#1072
  Color = 9788977
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 32
    Top = 16
    Width = 63
    Height = 16
    Caption = #1064#1090#1088#1080#1093#1082#1086#1076
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 15456197
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 32
    Top = 72
    Width = 55
    Height = 16
    Caption = #1040#1088#1090#1080#1082#1091#1083
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 15456197
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 216
    Top = 16
    Width = 66
    Height = 16
    Caption = #1053#1072#1079#1074#1072#1085#1080#1077
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 15456197
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 536
    Top = 16
    Width = 78
    Height = 16
    Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 15456197
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label5: TLabel
    Left = 632
    Top = 16
    Width = 33
    Height = 16
    Caption = #1062#1077#1085#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 15456197
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label6: TLabel
    Left = 632
    Top = 72
    Width = 43
    Height = 16
    Caption = #1057#1091#1084#1084#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 15456197
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label7: TLabel
    Left = 528
    Top = 480
    Width = 130
    Height = 20
    Caption = #1042#1089#1077#1075#1086' '#1082' '#1086#1087#1083#1072#1090#1077
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 15456197
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label11: TLabel
    Left = 32
    Top = 144
    Width = 75
    Height = 16
    Caption = #1063#1077#1082' '#8470' 102'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 15456197
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold, fsUnderline]
    ParentFont = False
  end
  object Label12: TLabel
    Left = 32
    Top = 528
    Width = 78
    Height = 16
    Caption = #1042#1074#1077#1076#1080#1090#1077' '#1064#1050
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 15456197
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object cxTextEdit1: TcxTextEdit
    Left = 32
    Top = 32
    ParentFont = False
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -19
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = []
    Style.IsFontAssigned = True
    TabOrder = 0
    Text = '1234567890123'
    Width = 177
  end
  object cxTextEdit2: TcxTextEdit
    Left = 32
    Top = 88
    ParentFont = False
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -19
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = [fsBold]
    Style.IsFontAssigned = True
    TabOrder = 1
    Text = '10002'
    Width = 121
  end
  object cxMemo1: TcxMemo
    Left = 216
    Top = 32
    Lines.Strings = (
      #1050#1056#1059#1055#1040' '#1055#1064#1045#1053'.'#1055#1054#1051#1058#1040#1042#1057#1050#1040#1071' '#8470'3 '
      '500'#1043#1056)
    ParentFont = False
    Properties.Alignment = taCenter
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -16
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = [fsBold]
    Style.IsFontAssigned = True
    TabOrder = 2
    Height = 89
    Width = 313
  end
  object cxCalcEdit1: TcxCalcEdit
    Left = 536
    Top = 32
    EditValue = 2.000000000000000000
    ParentFont = False
    Properties.Alignment.Horz = taCenter
    Properties.Alignment.Vert = taVCenter
    Properties.QuickClose = True
    Style.BorderStyle = ebsUltraFlat
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -19
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = []
    Style.LookAndFeel.Kind = lfFlat
    Style.LookAndFeel.NativeStyle = False
    Style.ButtonStyle = btsFlat
    Style.ButtonTransparency = ebtHideInactive
    Style.IsFontAssigned = True
    StyleDisabled.LookAndFeel.Kind = lfFlat
    StyleDisabled.LookAndFeel.NativeStyle = False
    StyleFocused.LookAndFeel.Kind = lfFlat
    StyleFocused.LookAndFeel.NativeStyle = False
    StyleHot.LookAndFeel.Kind = lfFlat
    StyleHot.LookAndFeel.NativeStyle = False
    TabOrder = 3
    Width = 89
  end
  object cxCurrencyEdit1: TcxCurrencyEdit
    Left = 632
    Top = 32
    EditValue = 99999.990000000000000000
    ParentFont = False
    Properties.Alignment.Horz = taCenter
    Properties.Alignment.Vert = taVCenter
    Properties.DisplayFormat = '0. '#1088'00;-,0. '#1088'00'
    Properties.UseThousandSeparator = True
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -19
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = []
    Style.IsFontAssigned = True
    TabOrder = 4
    Width = 113
  end
  object cxCurrencyEdit2: TcxCurrencyEdit
    Left = 632
    Top = 88
    AutoSize = False
    EditValue = 99999.010000000000000000
    ParentFont = False
    Properties.Alignment.Horz = taCenter
    Properties.Alignment.Vert = taVCenter
    Properties.DisplayFormat = '0. '#1088'00;-,0. '#1088'00'
    Properties.UseThousandSeparator = True
    Style.Font.Charset = RUSSIAN_CHARSET
    Style.Font.Color = 5412867
    Style.Font.Height = -19
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Pitch = fpVariable
    Style.Font.Style = []
    Style.IsFontAssigned = True
    TabOrder = 5
    Height = 33
    Width = 113
  end
  object cxGrid1: TcxGrid
    Left = 32
    Top = 168
    Width = 713
    Height = 289
    TabOrder = 6
    object cxGrid1DBTableView1: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object cxCurrencyEdit3: TcxCurrencyEdit
    Left = 528
    Top = 512
    AutoSize = False
    EditValue = 99999.010000000000000000
    ParentFont = False
    Properties.Alignment.Horz = taCenter
    Properties.Alignment.Vert = taVCenter
    Properties.DisplayFormat = '0. '#1088'00;-,0. '#1088'00'
    Properties.UseThousandSeparator = True
    Style.Font.Charset = RUSSIAN_CHARSET
    Style.Font.Color = 7405793
    Style.Font.Height = -48
    Style.Font.Name = 'Arial Narrow'
    Style.Font.Pitch = fpVariable
    Style.Font.Style = []
    Style.IsFontAssigned = True
    TabOrder = 7
    Height = 73
    Width = 217
  end
  object RxClock1: TRxClock
    Left = 872
    Top = 656
    Width = 129
    Height = 41
    Color = 6175776
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 14927276
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Panel1: TPanel
    Left = 784
    Top = 24
    Width = 209
    Height = 97
    BevelInner = bvLowered
    Color = 6175776
    TabOrder = 9
    object Label8: TLabel
      Left = 16
      Top = 16
      Width = 47
      Height = 13
      Caption = #1057#1084#1077#1085#1072' '#8470
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 15456197
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label9: TLabel
      Left = 16
      Top = 40
      Width = 68
      Height = 13
      Caption = #1056#1072#1073#1086#1095#1072#1103' '#1076#1072#1090#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 15456197
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label10: TLabel
      Left = 16
      Top = 64
      Width = 37
      Height = 13
      Caption = #1050#1072#1089#1089#1080#1088
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 15456197
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
  end
  object cxTextEdit3: TcxTextEdit
    Left = 32
    Top = 548
    ParentFont = False
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -24
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = []
    Style.IsFontAssigned = True
    TabOrder = 10
    Width = 353
  end
  object Panel2: TPanel
    Left = 872
    Top = 600
    Width = 129
    Height = 41
    BevelInner = bvLowered
    Color = 10329423
    TabOrder = 11
    object Label13: TLabel
      Left = 8
      Top = 13
      Width = 99
      Height = 16
      Caption = #1056#1077#1078#1080#1084' '#1082#1072#1089#1089#1080#1088#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
  end
  object cxButton1: TcxButton
    Left = 784
    Top = 144
    Width = 65
    Height = 49
    Caption = 'cxButton1'
    TabOrder = 12
    Colors.Normal = clWhite
  end
  object cxButton2: TcxButton
    Left = 856
    Top = 144
    Width = 65
    Height = 49
    Caption = 'cxButton2'
    TabOrder = 13
    Colors.Normal = clWhite
  end
  object cxButton3: TcxButton
    Left = 928
    Top = 144
    Width = 65
    Height = 49
    Caption = 'cxButton3'
    TabOrder = 14
    Colors.Normal = clWhite
  end
  object cxButton4: TcxButton
    Left = 784
    Top = 200
    Width = 65
    Height = 49
    Caption = 'cxButton1'
    TabOrder = 15
    Colors.Normal = clWhite
  end
  object cxButton5: TcxButton
    Left = 856
    Top = 200
    Width = 65
    Height = 49
    Caption = 'cxButton1'
    TabOrder = 16
    Colors.Normal = clWhite
  end
  object cxButton6: TcxButton
    Left = 928
    Top = 200
    Width = 65
    Height = 49
    Caption = 'cxButton1'
    TabOrder = 17
    Colors.Normal = clWhite
  end
  object cxButton7: TcxButton
    Left = 784
    Top = 256
    Width = 65
    Height = 49
    Caption = 'cxButton1'
    TabOrder = 18
    Colors.Normal = 16745090
  end
  object cxButton8: TcxButton
    Left = 856
    Top = 256
    Width = 65
    Height = 49
    Caption = 'cxButton1'
    TabOrder = 19
    Colors.Normal = 16745090
  end
  object cxButton9: TcxButton
    Left = 928
    Top = 256
    Width = 65
    Height = 49
    Caption = 'cxButton1'
    TabOrder = 20
    Colors.Normal = 16745090
  end
  object cxButton10: TcxButton
    Left = 784
    Top = 312
    Width = 65
    Height = 49
    Caption = 'cxButton1'
    TabOrder = 21
    Colors.Normal = 16745090
  end
  object cxButton11: TcxButton
    Left = 856
    Top = 312
    Width = 65
    Height = 49
    Caption = 'cxButton2'
    TabOrder = 22
    Colors.Normal = 16745090
  end
  object cxButton12: TcxButton
    Left = 928
    Top = 312
    Width = 65
    Height = 49
    Caption = 'cxButton3'
    TabOrder = 23
    Colors.Normal = 16745090
  end
  object cxButton13: TcxButton
    Left = 784
    Top = 368
    Width = 65
    Height = 49
    Caption = 'cxButton1'
    TabOrder = 24
    Colors.Normal = 8421631
  end
  object cxButton14: TcxButton
    Left = 856
    Top = 368
    Width = 65
    Height = 49
    Caption = 'cxButton1'
    TabOrder = 25
    Colors.Normal = 8421631
  end
  object cxButton15: TcxButton
    Left = 928
    Top = 368
    Width = 65
    Height = 49
    Caption = 'cxButton1'
    TabOrder = 26
    Colors.Normal = 8421631
  end
  object cxButton16: TcxButton
    Left = 784
    Top = 424
    Width = 65
    Height = 49
    Caption = 'cxButton1'
    TabOrder = 27
    Colors.Normal = 8421631
  end
  object cxButton17: TcxButton
    Left = 856
    Top = 424
    Width = 65
    Height = 49
    Caption = 'cxButton1'
    TabOrder = 28
    Colors.Normal = 8421631
  end
  object cxButton18: TcxButton
    Left = 928
    Top = 424
    Width = 65
    Height = 49
    Caption = 'cxButton1'
    TabOrder = 29
    Colors.Normal = 8421631
  end
end
