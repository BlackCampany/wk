unit RepWatersBlud;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ComCtrls, cxCurrencyEdit, cxTextEdit,
  ExtCtrls, SpeedBar, Placemnt, Grids, DBGrids,
  ComObj, ActiveX, Excel2000, OleServer, ExcelXP, FR_Class, FR_DSet,
  FR_DBSet,cxLookAndFeelPainters;

type
  TfmRepWatersBlud = class(TForm)
    GridWater: TcxGrid;
    ViewWater: TcxGridDBTableView;
    LevelWater: TcxGridLevel;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    frRepWB: TfrReport;
    FormPlacement1: TFormPlacement;
    ViewWaterID_PERSONAL: TcxGridDBColumn;
    ViewWaterSIFR: TcxGridDBColumn;
    ViewWaterPRICE: TcxGridDBColumn;
    ViewWaterPERSONAL: TcxGridDBColumn;
    ViewWaterNAME: TcxGridDBColumn;
    ViewWaterQSUM: TcxGridDBColumn;
    ViewWaterSSUM: TcxGridDBColumn;
    SpeedItem4: TSpeedItem;
    dsRepWB1: TfrDBDataSet;
    SpeedItem5: TSpeedItem;
    ViewWaterDSUM: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SpeedItem2Click(Sender: TObject);
    procedure SpeedItem4Click(Sender: TObject);
    procedure SpeedItem5Click(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function IsOLEObjectInstalled(Name: String): boolean;

var
  fmRepWatersBlud: TfmRepWatersBlud;

implementation

uses Un1, Dm, Period2, Period, spisok,cxDBCheckListBox,cxCheckBox;

{$R *.dfm}

function IsOLEObjectInstalled(Name: String): boolean;
var
  ClassID: TCLSID;
  Rez : HRESULT;
begin

// ���� CLSID OLE-�������
  Rez := CLSIDFromProgID(PWideChar(WideString(Name)), ClassID);
  if Rez = S_OK then
 // ������ ������
    Result := true
  else
    Result := false;
end;



procedure TfmRepWatersBlud.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  ViewWater.RestoreFromIniFile(CurDir+GridIni);
end;

procedure TfmRepWatersBlud.SpeedItem1Click(Sender: TObject);
begin
 close;
end;

procedure TfmRepWatersBlud.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  dmC.quRepWB.Active:=False;
  ViewWater.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmRepWatersBlud.SpeedItem2Click(Sender: TObject);
begin
 fmPeriod:=TfmPeriod.Create(Application); //��� ���� ������������

  fmPeriod.ShowModal;
  if fmPeriod.ModalResult=mrOk then
  begin
    ViewWater.BeginUpdate;
    fmRepWatersBlud.Caption:='������� ���������� �� ������ c '+FormatDateTime('dd.mm.yyyy',TrebSel.DateFrom)+' �� '+FormatDateTime('dd.mm.yyyy',TrebSel.DateTo);

    with dmC do
    begin
      quRepWB.Active:=False;
      quRepWB.ParamByName('DATEB').AsDateTime:=TrebSel.DateFrom;
      quRepWB.ParamByName('DATEE').AsDateTime:=TrebSel.DateTo;
      quRepWB.Active:=True;

      fmRepWatersBlud.Show;
    end;
    ViewWater.EndUpdate;
  end;

end;

procedure TfmRepWatersBlud.SpeedItem4Click(Sender: TObject);
var I: Integer;
Var ff: String;
begin
//    StatusBar1.Panels[0].Text:='����� ���� ������������.'; delay(10);
  with dmC do
  begin

    ViewWater.BeginUpdate;
 //   dsRepWB.DataSet:=nil;
 //   ff:=ViewWater.DataController.Filter.FilterText;
{
    if ViewWater.DataController.Filter.Root.IsItemList then
      with ViewWater.DataController.Filter do
       for i:=0 to Root.Count-1 do
       begin
        ff := ff + Root.Items[i].Criteria.FilterText;
       end;
 }

    quRepWB.Filter:= ViewWater.DataController.Filter.FilterText;
    quRepWB.Filtered:=True;

    frRepWB.LoadFromFile(CurDir + 'RepWatersBlud.frf');
      frVariables.Variable['sPeriod']:='c '+FormatDateTime('dd.mm.yyyy hh.nn',TrebSel.DateFrom)+' �� '+FormatDateTime('dd.mm.yyyy hh.nn',TrebSel.DateTo);
      frVariables.Variable['Depart']:=CommonSet.DepartName;

    frRepWB.ReportName:='������� ����������.';
    frRepWB.PrepareReport;
    frRepWB.ShowPreparedReport;


    quRepWB.Filter:='';
    quRepWB.Filtered:=False;

    quRepWB.Active :=True;
  //  quRepWB.First;
 //   dsRepWB.DataSet:=quRepWB;

    ViewWater.EndUpdate;
 //   StatusBar1.Panels[0].Text:='������������ ��.';
  end;

end;

procedure TfmRepWatersBlud.SpeedItem5Click(Sender: TObject);
var I,i1: Integer;
Var Filter: String;
var ArrayParent:Array[1..5000] of Integer;
var ArraySel: Variant;

var AItemList: TcxFilterCriteriaItemList;

begin
  fmSpisok:=TfmSpisok.Create(Application);

 // ������ ����
  dmC.quRepWBSpisok.ParamByName('DateB').AsDateTime:=TrebSel.DateFrom;
  dmC.quRepWBSpisok.ParamByName('DateE').AsDateTime:=TrebSel.DateTo;

  dmC.quRepWBSpisok.Active:=False;
  dmC.quRepWBSpisok.Active:=True;
  fmSpisok.cxCheckListBox.Items.Clear;
  dmC.quRepWBSpisok.First;
  i:=1;
  while not dmC.quRepWBSpisok.Eof do
  begin
    with fmSpisok.cxCheckListBox.Items.Add do
     begin
      Text := dmC.quRepWBSpisokNAME.AsString;
      ArrayParent[i]:=dmC.quRepWBSpisokSIFR.AsInteger;
 //     Enabled := False;
 //     State := cbsChecked;
      i:=i+ 1;
     end;
     dmC.quRepWBSpisok.Next;
  end;

  dmC.quRepWBSpisok.Active:=False;

  fmSpisok.ShowModal;
  if fmSpisok.ModalResult=mrOk then
  begin
     Filter:='';
     i1 := 0;
     ArraySel := VarArrayCreate([0, 10], varVariant);

{1234 }
     for i:=0 to fmSpisok.cxCheckListBox.Count-1 do
     if fmSpisok.cxCheckListBox.Items[i].State = cbsChecked then
      begin
       if Filter<> '' then Filter:=Filter+',';
       Filter:=Filter+IntToStr(ArrayParent[i+1]);
       ArraySel[i1] := ArrayParent[i+1];
       i1 :=i1 + 1;
      end;
{}
     if Filter<> '' then
     WITH ViewWater.DataController.Filter DO
      begin
       BeginUpdate;
       try
        Root.Clear;
        Root.BoolOperatorKind:=fboOR;
//        Root.AddItem(ViewWaterSIFR, foInList, ArraySel[0], Filter);
//        Root.AddItem(ViewWaterSIFR, foEqual, ArraySel[0], ArraySel[0]);
        for i := 0 to i1-1 do
        begin

        Root.AddItem(ViewWaterSIFR, foEqual, ArraySel[i], ArraySel[i]);
//         AItemList := Root.AddItemList(fboOr);
//         AItemList.AddItem(ViewWaterSIFR, foEqual, ArraySel[i], ArraySel[i]);
        end;

       finally
        EndUpdate;
        Active:=True;
       end;
      end;

  end;

end;

procedure TfmRepWatersBlud.SpeedItem3Click(Sender: TObject);
begin
  prNExportExel4(ViewWater);
end;

end.
