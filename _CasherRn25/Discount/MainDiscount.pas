unit MainDiscount;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ToolWin, ActnMan, ActnCtrls, ActnMenus,
  XPStyleActnCtrls, ActnList, ExtCtrls, SpeedBar, FR_DSet, FR_DBSet,
  pFIBDataSet,FR_Class,IniFiles;

type

  TfmMainDiscount = class(TForm)
    StatusBar1: TStatusBar;
    am1: TActionManager;
    ActionMainMenuBar1: TActionMainMenuBar;
    acExit: TAction;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    acDiscCards: TAction;
    acPlatCards: TAction;
    acTypePl: TAction;
    acDiscRep: TAction;
    acRealPCRep: TAction;
    frRep1: TfrReport;
    acPCCli: TAction;
    acEditBar: TAction;
    acEditIndex: TAction;
    acExportDK: TAction;
    AcImportDK: TAction;
    acGrDStop: TAction;
    SpeedItem2: TSpeedItem;
    acRemnDeb: TAction;
    procedure FormCanResize(Sender: TObject; var NewWidth,
      NewHeight: Integer; var Resize: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure acDiscCardsExecute(Sender: TObject);
    procedure acPlatCardsExecute(Sender: TObject);
    procedure acTypePlExecute(Sender: TObject);
    procedure acDiscRepExecute(Sender: TObject);
    procedure acRealPCRepExecute(Sender: TObject);
    procedure acPCCliExecute(Sender: TObject);
    procedure acEditBarExecute(Sender: TObject);
    procedure acEditIndexExecute(Sender: TObject);
    procedure acExportDKExecute(Sender: TObject);
    procedure AcImportDKExecute(Sender: TObject);
    procedure acGrDStopExecute(Sender: TObject);
    procedure acRemnDebExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

{  Function WrRec(taT:TpFIBDataSet):String;}

var
  fmMainDiscount: TfmMainDiscount;

implementation

uses Un1, dmRnDisc, Discount, TypePl, PCard, Period, PCDetail,
  PerA_Disc, RepPCCli, repDiscCli, ImportDK, ExportDK, prdb, GrStop,
  Balans;

{$R *.dfm}

procedure TfmMainDiscount.FormCanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
  NewHeight:=125
end;

procedure TfmMainDiscount.FormCreate(Sender: TObject);
begin
  CurDir := ExtractFilePath(ParamStr(0));
  ReadIni;
  TrebSel.DateFrom:=Trunc(Date-30);
  TrebSel.DateTo:=Trunc(Date);
end;

procedure TfmMainDiscount.FormResize(Sender: TObject);
begin
  StatusBar1.Width:=Width;
end;

procedure TfmMainDiscount.FormShow(Sender: TObject);
begin
  Left:=0;
  Top:=0;
  Width:=1024;

  with dmCDisc do
  begin
    quPer.Active:=False;
    quPer.SelectSQL.Clear;
    quPer.SelectSQL.Add('select * from rpersonal');
    quPer.SelectSQL.Add('where uvolnen=1 and modul2=0');
    quPer.SelectSQL.Add('order by Name');
//    quPer.Active:=True; ����������� �� show fmPerA
  end;
//���  fmPerA_Disc:=TfmPerA_Disc.Create(Application);
  fmPerA_Disc:=TfmPerA_Disc.Create(Application);
  fmPerA_Disc.ShowModal;
  if fmPerA_Disc.ModalResult=mrOk then
  begin
    Caption:=Caption+'   : '+Person.Name;
    WriteIni; //�������� �������� �������
    fmPerA_Disc.Release;

    bMenuList:=False;
    fmRnDisc:=tfmRnDisc.Create(Application);

 //   fmRnDisc.Show;
    bMenuList:=True;

    with dmPC do
    begin
      PCDb.Connected:=False;
      PCDb.DBName:=DBNamePC;

      try
        PCDb.Open;

        taTypePl.Active:=False;
        taTypePl.Active:=True;

      except
        showmessage('������ �������� ����.');
      end;
    end;
  end
  else
  begin
    fmPerA_Disc.Release;
    delay(100);
    close;
    delay(100);
  end;
end;

procedure TfmMainDiscount.acExitExecute(Sender: TObject);
begin
  close;
end;

procedure TfmMainDiscount.acDiscCardsExecute(Sender: TObject);
begin

 if not CanDo('dsViewDC') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
 fmRnDisc.Show;
//���������� �����
end;

procedure TfmMainDiscount.acPlatCardsExecute(Sender: TObject);
begin
//��������� �����
 if not CanDo('dsViewPC') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  with dmPC do
  begin
    taPCard.Active:=False;
    taPCard.Active:=True;
  end;
  fmPCard.Show;
end;

procedure TfmMainDiscount.acTypePlExecute(Sender: TObject);
begin
//���� ��������� ����
  if not CanDo('dsViewTipPC') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  fmTypePl.Show;
end;

procedure TfmMainDiscount.acDiscRepExecute(Sender: TObject);
begin
  //������ �� ���������� ������
//  if not CanDo('SaleGroup') then  StatusBar1.Panels[0].Text:='��� ����.';

  with dmCDisc do
  begin
    quRep1.Active:=False;
    quRep1.ParamByName('DateB').AsDateTime:=TrebSel.DateFrom;
    quRep1.ParamByName('DateE').AsDateTime:=TrebSel.DateTo;
    quRep1.Active:=True;
  end;

  fmDiscCli.Caption:='������ �� �� �� ������ � '+FormatDateTime('dd.mm.yyyy hh:nn',TrebSel.DateFrom)+' �� '+FormatDateTime('dd.mm.yyyy hh:nn',TrebSel.DateTo);
  fmDiscCli.Show;



{  fmPeriod:=TfmPeriod.Create(Application);
  fmPeriod.DateTimePicker1.Date:=Trunc(TrebSel.DateFrom);
  fmPeriod.DateTimePicker2.Date:=Trunc(TrebSel.DateTo);
  fmPeriod.DateTimePicker3.Visible:=True;
  fmPeriod.DateTimePicker3.Time:=Frac(TrebSel.DateFrom);
  fmPeriod.DateTimePicker4.Visible:=True;
  fmPeriod.DateTimePicker4.Time:=Frac(TrebSel.DateTo);
  fmPeriod.cxCheckBox1.Visible:=False;

  fmPeriod.ShowModal;
  if fmPeriod.ModalResult=mrOk then
  begin

    with dmCDisc do
    begin
      quRep1.Active:=False;
      quRep1.ParamByName('DateB').AsDateTime:=TrebSel.DateFrom;
      quRep1.ParamByName('DateE').AsDateTime:=TrebSel.DateTo;


      frRep1.LoadFromFile(CurDir + 'DiscSum.frf');

      frVariables.Variable['sPeriod']:='c '+FormatDateTime('dd.mm.yyyy hh:nn',TrebSel.DateFrom)+' �� '+FormatDateTime('dd.mm.yyyy hh:nn',TrebSel.DateTo);
//      frVariables.Variable['Depart']:=CommonSet.DepartName;

      frRep1.ReportName:='������ �� ���������� ������.';
      frRep1.PrepareReport;
      frRep1.ShowPreparedReport;
    end;
  end;
  fmPeriod.Release;             }

end;

procedure TfmMainDiscount.acRealPCRepExecute(Sender: TObject);
begin
  with dmCDisc do
  begin
    quPCDet.Active:=False;
    quPCDet.ParamByName('DateB').AsDateTime:=TrebSel.DateFrom;
    quPCDet.ParamByName('DateE').AsDateTime:=TrebSel.DateTo;
    quPCDet.Active:=True;
  end;

  fmPCDet.Caption:='��������� ����� �� ������ � '+FormatDateTime('dd.mm.yyyy hh:nn',TrebSel.DateFrom)+' �� '+FormatDateTime('dd.mm.yyyy hh:nn',TrebSel.DateTo);
  fmPCDet.Show;
end;

procedure TfmMainDiscount.acPCCliExecute(Sender: TObject);
begin
  with dmCDisc do
  begin
    quPCCli.Active:=False;
    quPCCli.ParamByName('DateB').AsDateTime:=TrebSel.DateFrom;
    quPCCli.ParamByName('DateE').AsDateTime:=TrebSel.DateTo;
    quPCCli.Active:=True;
  end;

  fmPCCli.Caption:='���������� �� �� �� �������� �� ������ � '+FormatDateTime('dd.mm.yyyy hh:nn',TrebSel.DateFrom)+' �� '+FormatDateTime('dd.mm.yyyy hh:nn',TrebSel.DateTo);
  fmPCCli.Show;
end;

procedure TfmMainDiscount.acEditBarExecute(Sender: TObject);
Var StrWk:String;
begin
  //�������� � �������� ��������� ������ = ����� 3-��� � 11-��� �����
  with dmCDisc do
  begin
    if MessageDlg('�������� = ?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      quDiscAll.Active:=False;
      quDiscAll.Active:=True;

      quDiscAll.First;
      trUpdate.StartTransaction;
      while not quDiscAll.Eof do
      begin
        StrWk:=quDiscAllBARCODE.AsString;
        if pos('=',StrWk)=0 then
        begin
          if Length(StrWk)>11 then
          begin
            insert('=',StrWk,12);
            insert('=',StrWk,4);

            quDiscAll.Edit;
            quDiscAllBARCODE.AsString:=StrWk;
            quDiscAll.Post;
          end;
        end;

        quDiscAll.Next;
      end;
      trUpdate.Commit;
      quDiscAll.Active:=False;
    end;
  end;
end;

procedure TfmMainDiscount.acEditIndexExecute(Sender: TObject);
Var StrWk:String;
    iCode:INteger;
begin
  //�������� � �������� ��������� ������ = ����� 3-��� � 11-��� �����
  with dmCDisc do
  begin
    if MessageDlg('�������� ClientIndex ?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      quDiscAll.Active:=False;
      quDiscAll.Active:=True;

      quDiscAll.First;
      trUpdate.StartTransaction;
      while not quDiscAll.Eof do
      begin
        if quDiscAllCLIENTINDEX.AsInteger=0 then
        begin
          StrWk:=quDiscAllBARCODE.AsString;
          while pos('=',StrWk)>0 do delete(StrWk,1,pos('=',StrWk));

          iCode:=StrToIntDef(StrWk,0);
          if iCode>0 then
          begin
            quDiscAll.Edit;
            quDiscAllCLIENTINDEX.AsInteger:=iCode;
            quDiscAll.Post;
          end;
        end;
        quDiscAll.Next;
      end;
      trUpdate.Commit;
      quDiscAll.Active:=False;
    end;
  end;
end;
{
Function WrRec(taT:TpFIBDataSet):String;
 Var StrWk,StrT,StrF,StrVal:String;
     i:Integer;
 begin
   StrWk:='';
   for i:=0 to taT.FieldDefs.Count-1 do
   begin
     StrF:=taT.FieldDefs.Items[i].Name;
     StrT:=taT.FieldDefs.Items[i].FieldClass.ClassName;
     StrVal:=taT.FieldByName(StrF).AsString+';';
     StrWk:=StrWk+StrVal;
   end;
   Result:=StrWk;
 end;
}
procedure TfmMainDiscount.acExportDKExecute(Sender: TObject);
begin
 if not CanDo('dsExportDK') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
 fmExportDK.Show;

end;

 {
   trUpdate.StartTransaction;
    quDiscSel.Edit;
    quDiscSelIACTIVE.AsInteger:=1;
    quDiscSel.Post;
    trUpdate.Commit;
  }
procedure TfmMainDiscount.AcImportDKExecute(Sender: TObject);
begin
 if not CanDo('dsImportDK') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
 fmImportDK.Show;

end;

Procedure ReadIniDiscont;
Var f:TIniFile;
//    s:String;
begin

  f:=TIniFile.create(CurDir+'Discont.ini');

//  KolBaseExport :=f.ReadInteger('Common','KolBaseExport',1);
//  MinLenghBarcode :=f.ReadInteger('Common','MinLenghBarcode',1);

//  DBName:=f.ReadString('Config_','DBName','C:\Database\Ust\Ust.GDB');

//  while pos(' ',CommonSet.CashSer)>0 do delete(CommonSet.CashSer,pos(' ',CommonSet.CashSer),1);

  f.Free;

 end;


procedure TfmMainDiscount.acGrDStopExecute(Sender: TObject);
begin
//
  fmGrDStop.Tree1.Items.BeginUpdate;
  CardsExpandLevel1(nil,fmGrDStop.Tree1,dmCDisc.quMenuTree);
  fmGrDStop.Tree1.FullExpand;
  fmGrDStop.Tree1.FullCollapse;
  fmGrDStop.Tree1.Items.EndUpdate;

  fmGrDStop.Show;
end;

procedure TfmMainDiscount.acRemnDebExecute(Sender: TObject);
begin
  //������� �� ��������� ������
  if not CanDo('prViewBalancePC') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  with dmPC do
  begin
    fmBalans.Show;
    fmBalans.ViewBal.BeginUpdate;
    quBalansPC.Active:=False;
    quBalansPC.Active:=True;
    fmBalans.ViewBal.EndUpdate;
  end;
end;

end.
