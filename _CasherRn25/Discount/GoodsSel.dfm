object fmGoodsSel: TfmGoodsSel
  Left = 287
  Top = 108
  Width = 696
  Height = 509
  Caption = #1058#1086#1074#1072#1088#1099
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 456
    Width = 688
    Height = 19
    Panels = <
      item
        Width = 300
      end
      item
        Width = 50
      end>
  end
  object ActionMainMenuBar1: TActionMainMenuBar
    Left = 0
    Top = 0
    Width = 688
    Height = 27
    UseSystemFont = False
    ActionManager = amG
    Caption = 'ActionMainMenuBar1'
    ColorMap.HighlightColor = 15660791
    ColorMap.BtnSelectedColor = clBtnFace
    ColorMap.UnusedColor = 15660791
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Spacing = 0
  end
  object Panel1: TPanel
    Left = 0
    Top = 75
    Width = 169
    Height = 340
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 2
    object ClassTree: TcxTreeView
      Left = 0
      Top = 0
      Width = 169
      Height = 340
      Align = alClient
      DragMode = dmAutomatic
      PopupMenu = PopupMenu1
      Style.BorderStyle = cbsOffice11
      Style.LookAndFeel.Kind = lfOffice11
      StyleDisabled.BorderStyle = cbsOffice11
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.BorderStyle = cbsOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.BorderStyle = cbsOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 0
      OnDragDrop = ClassTreeDragDrop
      OnDragOver = ClassTreeDragOver
      Images = dmO.imState
      ReadOnly = True
      OnChange = ClassTreeChange
      OnExpanding = ClassTreeExpanding
    end
  end
  object SpeedBar1: TSpeedBar
    Left = 0
    Top = 27
    Width = 688
    Height = 48
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Options = [sbAllowDrag, sbFlatBtns, sbTransparentBtns]
    BtnOffsetHorz = 4
    BtnOffsetVert = 4
    BtnWidth = 60
    BtnHeight = 40
    BevelInner = bvLowered
    Color = 16764134
    TabOrder = 3
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object SpeedItem1: TSpeedItem
      Action = Action1
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = 'SpeedItem1'
      Glyph.Data = {
        5E040000424D5E04000000000000360000002800000012000000130000000100
        18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
        8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
        0000CED3D6848684848684848684848684848684848684848684848684848684
        848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
        7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
        FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
        FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
        007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
        FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
        00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
        494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
        00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000}
      Spacing = 1
      Left = 424
      Top = 4
      Visible = True
      OnClick = Action1Execute
      SectionName = 'Untitled (0)'
    end
    object SpeedItem2: TSpeedItem
      Action = acAddGoods
      BtnCaption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1090#1086#1074#1072#1088
      Caption = 'SpeedItem2'
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C9F019F019F019F019F019F019F019F01
        9F011F7C1F7C1F7C1F7C1F7C1F7C9F019F019F019F019F019F019F019F019F01
        9F019F011F7C1F7C1F7C1F7C1F7C9F019F019F019F019F019F019F019F019F01
        9F019F011F7C1F7C1F7C1F7C1F7C9F019F019F019F019F019F019F019F019F01
        9F019F011F7C1F7C1F7C1F7C1F7C1F021F021F021F021F021F021F021F021F02
        1F021F021F7C1F7C1F7C1F7C1F7C1F021F021F021F021F021F021F021F021F02
        1F021F021F7C1F7C1F7C1F7C1F7C7F327F327F327F327F327F327F327F327F32
        7F327F321F7C1F7C1F7C6003C001E00060037F327F327F327F327F327F327F32
        7F327F321F7C1F7C1F7CE000F75FF75FE000394F394F394F394F394F394F394F
        394F7F321F7C6003C001C001F75FF75FC001E0006003394F394F394F394F394F
        394F7F321F7CC001E313F75FF75FF75FF75F6003E0007F327F327F327F327F32
        7F321F7C1F7CC001FF67FF67E313F75FE313E313C0011F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7CE31360036003ED3BE313C001C001E3131F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C6003ED3BED3BC0011F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7CED3BE313E313ED3B1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      Spacing = 1
      Left = 4
      Top = 4
      Visible = True
      OnClick = acAddGoodsExecute
      SectionName = 'Untitled (0)'
    end
    object SpeedItem3: TSpeedItem
      Action = acEditGoods
      BtnCaption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100' '#1090#1086#1074#1072#1088
      Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100' '#1090#1086#1074#1072#1088
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C00001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C000000001F7C1F7C1F7C9F019F019F019F019F019F019F01
        9F011F7C1F7C1F7C0000000000001F7C9F019F019F019F019F019F019F019F01
        9F019F011F7C1F7C42082129C24100009F019F019F019F019F019F019F019F01
        9F019F011F7C1F7C10426277036FC56A4A299F019F019F019F019F019F019F01
        9F019F011F7C1F7CD65AA17B4277E46E00001F021F021F021F021F021F021F02
        1F021F021F7C1F7C1F7C1042817B2373C46A4A291F021F021F021F021F021F02
        1F021F021F7C1F7C1F7CD65AC07F6277036F00007F327F327F327F327F327F32
        7F327F321F7C1F7C1F7C1F7C1042A17B4277E46E4A297F327F327F327F327F32
        7F327F321F7C1F7C1F7C1F7CD65AE07F817B23730000394F394F394F394F394F
        394F7F321F7C1F7C1F7C1F7C1F7C1042C07F000000000000394F394F394F394F
        394F7F321F7C1F7C1F7C1F7C1F7CD65ABD77D65A104200007F327F327F327F32
        7F321F7C1F7C1F7C1F7C1F7C1F7C1F7C10423967524A10424A291F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7CD65ABD77D65A104200001F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1042104210421F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      Hint = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100' '#1090#1086#1074#1072#1088
      Spacing = 1
      Left = 64
      Top = 4
      Visible = True
      OnClick = acEditGoodsExecute
      SectionName = 'Untitled (0)'
    end
    object SpeedItem4: TSpeedItem
      Action = acDelGoods
      BtnCaption = #1059#1076#1072#1083#1080#1090#1100' '#1090#1086#1074#1072#1088'.'
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1090#1086#1074#1072#1088'.'
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C9F019F019F019F019F019F019F01
        9F011F7C1F7C1F7C1F7C1F7C1F7C1F7C9F019F019F019F019F019F019F019F01
        9F019F011F7C1F7C1F7C1F7C1F7C1F7C9F019F019F019F019F019F019F019F01
        9F019F011F7C1F7C1F7C1F7C1F7C1F7C9F019F019F019F019F019F019F019F01
        9F019F011F7C1F7C1F7C1F7C1F7C1F7C1F021F021F021F021F021F021F021F02
        1F021F021F7C1F7C1F7C1F7C1F7C1F7C1F021F021F021F021F021F021F021F02
        1F021F021F7C1F7C1F7C1F7C1F7C1F7C7F327F327F327F327F327F327F327F32
        7F327F321F7C1F7C1F7C1F7C1F7C1F7C7F327F327F327F327F327F327F327F32
        7F327F321F7C1F7C1F7C007C007CC67CC67CC67CC67C394F394F394F394F394F
        394F7F321F7C1F7CC67CCE7DCE7DB57EB57EB57EB57E007C394F394F394F394F
        394F7F321F7C1F7C007CCE7DCE7DB57EB57EB57EB57EC67C7F327F327F327F32
        7F321F7C1F7C1F7C1F7CC67CC67CC67CC67C007C007C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      Hint = #1059#1076#1072#1083#1080#1090#1100' '#1090#1086#1074#1072#1088'.'
      Spacing = 1
      Left = 144
      Top = 4
      Visible = True
      OnClick = acDelGoodsExecute
      SectionName = 'Untitled (0)'
    end
  end
  object cxSplitter1: TcxSplitter
    Left = 169
    Top = 75
    Width = 4
    Height = 340
    Cursor = crHSplit
    NativeBackground = False
    Control = Panel1
    Color = clGreen
    ParentColor = False
  end
  object GrGoodsSel: TcxGrid
    Left = 192
    Top = 80
    Width = 473
    Height = 329
    TabOrder = 5
    LookAndFeel.Kind = lfOffice11
    object ViewGoodsSel: TcxGridDBTableView
      DragMode = dmAutomatic
      PopupMenu = PopupMenu2
      OnDblClick = ViewGoodsSelDblClick
      OnStartDrag = ViewGoodsSelStartDrag
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dmO.dsCardsSel1
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.MultiSelect = True
      OptionsView.Indicator = True
      object ViewGoodsSelID: TcxGridDBColumn
        Caption = #1050#1086#1076
        DataBinding.FieldName = 'ID'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taCenter
        HeaderAlignmentHorz = taCenter
        Styles.Content = dmO.cxStyle1
      end
      object ViewGoodsSelPARENT: TcxGridDBColumn
        DataBinding.FieldName = 'PARENT'
        Visible = False
      end
      object ViewGoodsSelNAME: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'NAME'
        Styles.Content = dmO.cxStyle18
        Width = 200
      end
      object ViewGoodsSelTTYPE: TcxGridDBColumn
        Caption = #1058#1080#1087' '#1064#1050
        DataBinding.FieldName = 'TTYPE'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Images = dmO.imState
        Properties.Items = <
          item
            Description = #1064#1090#1091#1095#1085'.'
            Value = 0
          end
          item
            Description = #1042#1077#1089'.'
            Value = 1
          end>
      end
      object ViewGoodsSelTCARD: TcxGridDBColumn
        Caption = #1058#1077#1093#1085'.'#1082#1072#1088#1090#1072
        DataBinding.FieldName = 'TCARD'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Images = dmO.imState
        Properties.Items = <
          item
            Value = 0
          end
          item
            ImageIndex = 11
            Value = 1
          end>
      end
      object ViewGoodsSelIMESSURE: TcxGridDBColumn
        DataBinding.FieldName = 'IMESSURE'
        Visible = False
      end
      object ViewGoodsSelNAMESHORT: TcxGridDBColumn
        Caption = #1045#1076'.'#1048#1079#1084'.'
        DataBinding.FieldName = 'NAMESHORT'
      end
      object ViewGoodsSelNAMENDS: TcxGridDBColumn
        Caption = #1053#1044#1057
        DataBinding.FieldName = 'NAMENDS'
      end
      object ViewGoodsSelPROC: TcxGridDBColumn
        DataBinding.FieldName = 'PROC'
        Visible = False
      end
      object ViewGoodsSelINDS: TcxGridDBColumn
        DataBinding.FieldName = 'INDS'
        Visible = False
      end
      object ViewGoodsSelMINREST: TcxGridDBColumn
        Caption = #1052#1080#1085'. '#1086#1089#1090#1072#1090#1086#1082
        DataBinding.FieldName = 'MINREST'
        PropertiesClassName = 'TcxCalcEditProperties'
        Properties.DisplayFormat = '0.###'
      end
      object ViewGoodsSelLASTPRICEIN: TcxGridDBColumn
        DataBinding.FieldName = 'LASTPRICEIN'
        Visible = False
      end
      object ViewGoodsSelLASTPRICEOUT: TcxGridDBColumn
        DataBinding.FieldName = 'LASTPRICEOUT'
        Visible = False
      end
      object ViewGoodsSelLASTPOST: TcxGridDBColumn
        DataBinding.FieldName = 'LASTPOST'
        Visible = False
      end
      object ViewGoodsSelIACTIVE: TcxGridDBColumn
        Caption = #1057#1090#1072#1090#1091#1089
        DataBinding.FieldName = 'IACTIVE'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Images = dmO.imState
        Properties.Items = <
          item
            Description = #1040#1082#1090#1080#1074#1085#1072#1103
            ImageIndex = 0
            Value = 1
          end
          item
            Description = #1053#1077#1072#1082#1090#1080#1074#1085#1072#1103
            ImageIndex = 1
            Value = 0
          end>
      end
    end
    object LevelGoodsSel: TcxGridLevel
      GridView = ViewGoodsSel
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 415
    Width = 688
    Height = 41
    Align = alBottom
    BevelInner = bvLowered
    Color = 16764134
    TabOrder = 6
    object Label1: TLabel
      Left = 16
      Top = 16
      Width = 35
      Height = 13
      Caption = #1055#1086#1080#1089#1082' '
    end
    object cxTextEdit1: TcxTextEdit
      Left = 64
      Top = 12
      TabOrder = 0
      Text = 'cxTextEdit1'
      Width = 225
    end
    object cxButton1: TcxButton
      Left = 304
      Top = 12
      Width = 75
      Height = 21
      Caption = #1055#1086' '#1085#1072#1079#1074#1072#1085#1080#1102
      Default = True
      TabOrder = 1
      OnClick = cxButton1Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 392
      Top = 12
      Width = 75
      Height = 21
      Caption = #1055#1086' '#1082#1086#1076#1091
      TabOrder = 2
      OnClick = cxButton2Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton3: TcxButton
      Left = 480
      Top = 12
      Width = 75
      Height = 21
      Caption = #1055#1086' '#1064#1050
      TabOrder = 3
      OnClick = cxButton3Click
      LookAndFeel.Kind = lfOffice11
    end
  end
  object amG: TActionManager
    ActionBars = <
      item
        Items = <
          item
            Action = Action1
            ImageIndex = 10
          end
          item
            Items = <
              item
                Items = <
                  item
                    Action = Action5
                    ImageIndex = 28
                  end
                  item
                    Action = Action6
                    ImageIndex = 28
                  end
                  item
                    Action = Action7
                    ImageIndex = 27
                  end
                  item
                    Caption = '-'
                  end
                  item
                    Action = Action8
                    ImageIndex = 29
                  end>
                Action = Action3
                ImageIndex = 9
              end
              item
                Items = <
                  item
                    Action = acAddGoods
                    ImageIndex = 32
                    ShortCut = 45
                  end
                  item
                    Action = acEditGoods
                    ImageIndex = 30
                    ShortCut = 115
                  end
                  item
                    Caption = '-'
                  end
                  item
                    Action = acDelGoods
                    ImageIndex = 31
                    ShortCut = 46
                  end>
                Caption = #1058#1086#1074#1072#1088#1099
                ImageIndex = 34
              end>
            Action = Action2
          end>
        ActionBar = ActionMainMenuBar1
      end>
    Images = dmO.imState
    Left = 408
    Top = 216
    StyleName = 'XP Style'
    object Action1: TAction
      Caption = #1042#1099#1093#1086#1076
      ImageIndex = 10
      OnExecute = Action1Execute
    end
    object Action2: TAction
      Caption = #1044#1077#1081#1089#1090#1074#1080#1103
      OnExecute = Action2Execute
    end
    object Action3: TAction
      Caption = #1050#1083#1072#1089#1089#1080#1092#1080#1082#1072#1090#1086#1088
      ImageIndex = 9
      OnExecute = Action3Execute
    end
    object Action4: TAction
      Caption = #1058#1086#1074#1072#1088#1099', '#1087#1088#1086#1076#1091#1082#1090#1099', '#1073#1083#1102#1076#1072
    end
    object Action5: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1075#1088#1091#1087#1087#1091
      ImageIndex = 28
      OnExecute = Action5Execute
    end
    object Action6: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1087#1086#1076#1075#1088#1091#1087#1087#1091
      ImageIndex = 28
      OnExecute = Action6Execute
    end
    object Action7: TAction
      Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100
      ImageIndex = 27
      OnExecute = Action7Execute
    end
    object Action8: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ImageIndex = 29
      OnExecute = Action8Execute
    end
    object acAddGoods: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1090#1086#1074#1072#1088
      ImageIndex = 38
      ShortCut = 45
      OnExecute = acAddGoodsExecute
    end
    object acEditGoods: TAction
      Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100' '#1090#1086#1074#1072#1088
      ImageIndex = 37
      ShortCut = 115
      OnExecute = acEditGoodsExecute
    end
    object acDelGoods: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1090#1086#1074#1072#1088'.'
      ImageIndex = 39
      ShortCut = 46
      OnExecute = acDelGoodsExecute
    end
    object Action9: TAction
      Caption = #1055#1077#1088#1077#1084#1077#1089#1090#1080#1090#1100' '#1074' '#1075#1088#1091#1087#1087#1091
      ShortCut = 117
      OnExecute = Action9Execute
    end
    object acTCard: TAction
      Caption = #1058#1077#1093#1085#1086#1083#1086#1075#1080#1095#1077#1089#1082#1072#1103' '#1082#1072#1088#1090#1072
      ImageIndex = 11
      Visible = False
      OnExecute = acTCardExecute
    end
  end
  object FormPlacement1: TFormPlacement
    Active = False
    Left = 560
    Top = 120
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 3000
    OnTimer = Timer1Timer
    Left = 472
    Top = 152
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    PopupMenus = <>
    Left = 528
    Top = 264
  end
  object PopupMenu1: TPopupMenu
    Images = dmO.imState
    Left = 80
    Top = 139
    object N1: TMenuItem
      Action = Action5
    end
    object N2: TMenuItem
      Action = Action6
    end
    object N3: TMenuItem
      Action = Action7
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object N5: TMenuItem
      Action = Action8
    end
  end
  object PopupMenu2: TPopupMenu
    Images = dmO.imState
    Left = 320
    Top = 192
    object N6: TMenuItem
      Action = acAddGoods
    end
    object N7: TMenuItem
      Action = acEditGoods
    end
    object N8: TMenuItem
      Caption = '-'
    end
    object N9: TMenuItem
      Action = acDelGoods
    end
  end
end
