unit Period4;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, RXCtrls, ComCtrls, cxLookAndFeelPainters, StdCtrls,
  cxButtons, Menus, cxControls, cxContainer, cxEdit, cxCheckBox,
  cxCheckListBox, FR_DSet, FR_DBSet, cxDBCheckListBox, DB, FIBDataSet,
  pFIBDataSet, cxTreeView;

type
  TfmPeriod4 = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    DateTimePicker1: TDateTimePicker;
    DateTimePicker2: TDateTimePicker;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    DateTimePicker3: TDateTimePicker;
    DateTimePicker4: TDateTimePicker;
    cxCheckBox1: TcxCheckBox;
    cxCheckListBox1: TcxCheckListBox;
    dsMenuGR: TDataSource;
    PanelPeriod: TPanel;
    RxLabel4: TRxLabel;
    RxLabel2: TRxLabel;
    cxTreeView1: TcxTreeView;
    TreeView1: TTreeView;
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmPeriod4: TfmPeriod4;

implementation

uses Un1,dm;

{$R *.dfm}

procedure TfmPeriod4.cxButton1Click(Sender: TObject);
begin
  TrebSel.DateFrom:=Trunc(DateTimePicker1.Date)+Frac(DateTimePicker3.Time);
  TrebSel.DateTo:=Trunc(DateTimePicker2.Date)+Frac(DateTimePicker4.Time);
end;

procedure TfmPeriod4.cxButton2Click(Sender: TObject);
begin
  close;
end;

procedure TfmPeriod4.FormCreate(Sender: TObject);
Var TimeShift:TDateTime;
begin
  DateTimePicker1.Date:=Trunc(TrebSel.DateFrom);
  DateTimePicker2.Date:=Trunc(TrebSel.DateTo); 
  DateTimePicker3.Visible:=True;
  DateTimePicker4.Visible:=True;

  TimeShift:=StrToTimeDef(CommonSet.ZTimeShift,0.25);
  DateTimePicker3.Time:=frac(TimeShift);
  DateTimePicker4.Time:=frac(TimeShift);
end;

end.
