object fmMDG: TfmMDG
  Left = 349
  Top = 137
  BorderStyle = bsDialog
  Caption = #1043#1088#1091#1087#1087#1099' '#1076#1085#1077#1074#1085#1086#1075#1086' '#1084#1077#1085#1102
  ClientHeight = 174
  ClientWidth = 323
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 24
    Width = 50
    Height = 13
    Caption = #1053#1072#1079#1074#1072#1085#1080#1077
    Transparent = True
  end
  object Label2: TLabel
    Left = 16
    Top = 56
    Width = 70
    Height = 13
    Caption = #8470' '#1087#1086' '#1087#1086#1088#1103#1076#1082#1091
    Transparent = True
  end
  object Panel1: TPanel
    Left = 0
    Top = 115
    Width = 323
    Height = 59
    Align = alBottom
    BevelInner = bvLowered
    Color = 11403099
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 48
      Top = 16
      Width = 89
      Height = 25
      Caption = #1054#1082
      ModalResult = 1
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 192
      Top = 16
      Width = 91
      Height = 25
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      LookAndFeel.Kind = lfOffice11
    end
  end
  object cxTextEdit1: TcxTextEdit
    Left = 96
    Top = 20
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 1
    Text = 'cxTextEdit1'
    Width = 201
  end
  object cxSpinEdit1: TcxSpinEdit
    Left = 96
    Top = 52
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 2
    Width = 121
  end
  object dxfBackGround1: TdxfBackGround
    BkColor.BeginColor = 15335378
    BkColor.EndColor = 11403099
    BkColor.FillStyle = fsVert
    BkAnimate.Speed = 700
    Left = 280
    Top = 40
  end
end
