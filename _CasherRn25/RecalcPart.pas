unit RecalcPart;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, cxMemo, StdCtrls, cxButtons,
  cxControls, cxContainer, cxEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit,
  cxCalendar, ComCtrls, FIBQuery, pFIBQuery, pFIBStoredProc, FIBDatabase,
  pFIBDatabase, DB, FIBDataSet, pFIBDataSet, dxfProgressBar, cxGraphics,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox;

type
  TfmRecalcPer2 = class(TForm)
    StatusBar1: TStatusBar;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Memo1: TcxMemo;
    PBar1: TdxfProgressBar;
    trS: TpFIBTransaction;
    trU: TpFIBTransaction;
    quC: TpFIBDataSet;
    quCID: TFIBIntegerField;
    cxLookupComboBox2: TcxLookupComboBox;
    Label1: TLabel;
    PRNORMPARTIN: TpFIBStoredProc;
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure prWr(StrWk:String);
  end;

var
  fmRecalcPer2: TfmRecalcPer2;

implementation

uses Un1, dmOffice, DMOReps, DocCompl, AddCompl, ActPer, DocsVn, DocsOut,
  DocOutB, DOBSpec, DocInv, DocOutR;

{$R *.dfm}

procedure TfmRecalcPer2.prWr(StrWk:String);
Var StrWk1:String;
begin
  StrWk1:=FormatDateTime('mm.dd hh:nn:sss',now)+' '+StrWk;
  Memo1.Lines.Add(StrWk1);
end;


procedure TfmRecalcPer2.cxButton1Click(Sender: TObject);
Var iC,iP,iL:Integer;
    rQ:Real;
begin
  Memo1.Clear;
  prWr('������.');
  prWr('  ����� ���� ��������.');
  cxButton1.Enabled:=False;
  cxButton2.Enabled:=False;
  try
    quC.Active:=False;
    quC.Active:=True;

    PBar1.Position:=0;
    PBar1.Max:=100;
    iP:=quC.RecordCount div 100;
    PBar1.Visible:=True;

    iC:=0; iL:=0;
    quC.First;
    while not quC.Eof do
    begin
      rQ:=prCalcRemn(quCID.AsInteger,Trunc(Date),cxLookupComboBox2.EditValue);

//      EXECUTE PROCEDURE PR_NORMPARTIN (?IDSKL, ?IDATE, ?IDCARD, ?QUANT)
      PRNORMPARTIN.ParamByName('IDSKL').AsInteger:=cxLookupComboBox2.EditValue;
      PRNORMPARTIN.ParamByName('IDATE').AsInteger:=Trunc(Date);
      PRNORMPARTIN.ParamByName('IDCARD').AsInteger:=quCID.AsInteger;
      PRNORMPARTIN.ParamByName('QUANT').AsFloat:=rQ;
      PRNORMPARTIN.ExecProc;
      delay(10);

      quC.Next; inc(iC);
      if iP>0 then
      begin
        if iC mod iP = 0 then
        begin
          if iL<100 then inc(iL);
          PBar1.Position:=iL; delay(10);
        end;
      end;  
    end;
  finally
    quC.Active:=False;
    PBar1.Position:=PBar1.Max; delay(100);
    PBar1.Visible:=False;

    cxButton1.Enabled:=True;
    cxButton2.Enabled:=True;
    prWr('�������� ��������.');
  end;
end;

procedure TfmRecalcPer2.cxButton2Click(Sender: TObject);
begin
  close;
end;

end.
