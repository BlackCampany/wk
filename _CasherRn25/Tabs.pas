unit Tabs;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  ComCtrls, ExtCtrls, SpeedBar, Placemnt, ActnList, XPStyleActnCtrls,
  ActnMan, cxDataStorage, cxCurrencyEdit, cxImageComboBox,
  ComObj, ActiveX, Excel2000, OleServer, ExcelXP, cxCheckBox;

type
  TfmTabs = class(TForm)
    StatusBar1: TStatusBar;
    ViewTabs: TcxGridDBTableView;
    LevelTabs: TcxGridLevel;
    GridTabs: TcxGrid;
    ViewTabsID: TcxGridDBColumn;
    ViewTabsID_PERSONAL: TcxGridDBColumn;
    ViewTabsNUMTABLE: TcxGridDBColumn;
    ViewTabsQUESTS: TcxGridDBColumn;
    ViewTabsTABSUM: TcxGridDBColumn;
    ViewTabsBEGTIME: TcxGridDBColumn;
    ViewTabsENDTIME: TcxGridDBColumn;
    ViewTabsDISCONT: TcxGridDBColumn;
    ViewTabsOPERTYPE: TcxGridDBColumn;
    ViewTabsCHECKNUM: TcxGridDBColumn;
    ViewTabsSKLAD: TcxGridDBColumn;
    ViewTabsNAME: TcxGridDBColumn;
    SpeedBar1: TSpeedBar;
    FormPlacement1: TFormPlacement;
    ViewTabsCASHNUM: TcxGridDBColumn;
    ViewTabsZNUM: TcxGridDBColumn;
    ViewTabsCHECKNUM1: TcxGridDBColumn;
    ViewTabsTABSUM1: TcxGridDBColumn;
    ViewTabsCLIENTSUM: TcxGridDBColumn;
    ViewTabsCHDATE: TcxGridDBColumn;
    amTabs: TActionManager;
    acPeriod: TAction;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    acSpecSel: TAction;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    LevelSpec: TcxGridLevel;
    ViewSpecReps: TcxGridDBTableView;
    ViewSpecRepsID_TAB: TcxGridDBColumn;
    ViewSpecRepsID: TcxGridDBColumn;
    ViewSpecRepsID_PERSONAL: TcxGridDBColumn;
    ViewSpecRepsNUMTABLE: TcxGridDBColumn;
    ViewSpecRepsSIFR: TcxGridDBColumn;
    ViewSpecRepsPRICE: TcxGridDBColumn;
    ViewSpecRepsQUANTITY: TcxGridDBColumn;
    ViewSpecRepsDISCOUNTPROC: TcxGridDBColumn;
    ViewSpecRepsDISCOUNTSUM: TcxGridDBColumn;
    ViewSpecRepsSUMMA: TcxGridDBColumn;
    ViewSpecRepsITYPE: TcxGridDBColumn;
    ViewSpecRepsNAMEMM: TcxGridDBColumn;
    ViewSpecRepsNAMEMD: TcxGridDBColumn;
    ViewSpecRepsNAMEPERS: TcxGridDBColumn;
    ViewSpecRepsQUESTS: TcxGridDBColumn;
    ViewSpecRepsTABSUM: TcxGridDBColumn;
    ViewSpecRepsBEGTIME: TcxGridDBColumn;
    ViewSpecRepsENDTIME: TcxGridDBColumn;
    ViewSpecRepsDISCONT: TcxGridDBColumn;
    ViewSpecRepsOPERTYPE: TcxGridDBColumn;
    ViewSpecRepsCHECKNUM: TcxGridDBColumn;
    ViewSpecRepsSKLAD: TcxGridDBColumn;
    ViewSpecRepsCASHNUM: TcxGridDBColumn;
    ViewSpecRepsZNUM: TcxGridDBColumn;
    ViewSpecRepsCLIENTSUM: TcxGridDBColumn;
    ViewSpecRepsCHDATE: TcxGridDBColumn;
    SpeedItem4: TSpeedItem;
    ViewSpecRepsName: TcxGridDBColumn;
    aPrintCheck: TAction;
    ViewSpecRepsSTREAM: TcxGridDBColumn;
    ViewSpecRepsNAMESTREAM: TcxGridDBColumn;
    SpeedItem5: TSpeedItem;
    acExportEx: TAction;
    ViewTabsSTATION: TcxGridDBColumn;
    ViewSpecRepsSTATION: TcxGridDBColumn;
    ViewSpecRepsINEED: TcxGridDBColumn;
    ViewSpecRepsQUANTITY1: TcxGridDBColumn;
    ViewSpecRepsSUMMA1: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acPeriodExecute(Sender: TObject);
    procedure acSpecSelExecute(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
    procedure SpeedItem4Click(Sender: TObject);
    procedure aPrintCheckExecute(Sender: TObject);
    procedure SpeedItem5Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmTabs: TfmTabs;

implementation

uses Dm, Un1, PeriodUni, SpecSel, UnCash, Period;

{$R *.dfm}
{$I dll.inc}

procedure TfmTabs.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  ViewTabs.RestoreFromIniFile(CurDir+GridIni);
  ViewSpecReps.RestoreFromIniFile(CurDir+GridIni);
end;

procedure TfmTabs.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  dmC.taTabsAllSel.Active:=False;
  ViewTabs.StoreToIniFile(CurDir+GridIni,False);
  ViewSpecReps.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmTabs.acPeriodExecute(Sender: TObject);
begin
  try
    fmPeriod:=TfmPeriod.Create(Application); //��� ���� ������������

    fmPeriod.ShowModal;
    if fmPeriod.ModalResult=mrOk then
    begin
      with dmC do
      begin
        taTabsAllSel.Active:=False;
        taTabsAllSel.ParamByName('DBEG').AsDateTime:=TrebSel.DateFrom;
        taTabsAllSel.ParamByName('DEND').AsDateTime:=TrebSel.DateTo;
        taTabsAllSel.Active:=True;
        taTabsAllSel.Last;

        quSelSpecAll.Active:=False;
        quSelSpecAll.ParamByName('DBEG').AsDateTime:=TrebSel.DateFrom;
        quSelSpecAll.ParamByName('DEND').AsDateTime:=TrebSel.DateTo;
        quSelSpecAll.Active:=True;
      end;

      fmTabs.Caption:='����������� ������ �� ������ � '+FormatDateTime('dd.mm.yyyy hh:mm',TrebSel.DateFrom)+' �� '+FormatDateTime('dd.mm.yyyy hh:mm',TrebSel.DateTo);
    end;
  finally
    fmPeriod.Release;
  end;  
end;

procedure TfmTabs.acSpecSelExecute(Sender: TObject);
begin
  with dmC do
  begin
    taSpecAllSel.Active:=False;
    taSpecAllSel.ParamByName('IDHEAD').AsInteger:=taTabsAllSelID.AsInteger;
    taSpecAllSel.Active:=True;

    fmSpecSel.ShowModal;

  end;
end;

procedure TfmTabs.SpeedItem3Click(Sender: TObject);
begin
  close;
end;

procedure TfmTabs.SpeedItem4Click(Sender: TObject);
begin
  //��������� ����
  if LevelTabs.Visible=True then
  begin
    LevelTabs.Visible:=False;
    SpeedItem2.Enabled:=False;
    LevelSpec.Visible:=True;
  end else
  begin
    LevelTabs.Visible:=True;
    SpeedItem2.Enabled:=True;
    LevelSpec.Visible:=False;
  end;
end;

procedure TfmTabs.aPrintCheckExecute(Sender: TObject);
Var iCo:Integer;
    rSum:Real;
    Rec:TcxCustomGridRecord;
    i,j,iSum: Integer;

    {
Var sGr:String;
    iGr:Integer;
    iCo:Integer;
    iNum: Integer;
}

begin
  //������ �����
  iCo:=ViewTabs.Controller.SelectedRecordCount;
  if iCo>0 then
  begin
    if MessageDlg('���������� ���� ('+IntToStr(iCo)+')?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      OpenDll('������','AERF',PChar('COM1'),0);
      with dmC do
      begin
        //ViewTabsTABSUM
        for i:=0 to ViewTabs.Controller.SelectedRecordCount-1 do
        begin
          Rec:=ViewTabs.Controller.SelectedRecords[i];

          for j:=0 to Rec.ValueCount-1 do
          begin
            if ViewTabs.Columns[j].Name='ViewTabsTABSUM' then break;
          end;

          rSum:=Rec.Values[j];

          if rSum>0 then
          begin
            iSum:=RoundEx(rSum*100);
            StartReceipt(0,1,'','','');
            ItemReceiptPlus('����� �� ����� ','12345','��.','','',iSum,1000,1);
            TotalReceipt;
            TenderReceipt(0,iSum,'');
            CloseReceipt;
          end;
        end;
      end;
      XReport;
      delay(1000);
      CloseDll;
    end;
  end;

{

iCo:=ViewMenuCr.Controller.SelectedRecordCount;
    if iCo>0 then
    begin
      if MessageDlg('�� ������������� ������ ����������� ��������� ������� ('+IntToStr(iCo)+' ��.) � ������ "'+sGr+'"?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        with dmC do
        begin
          taMenu.Active:=False;
          taMenu.Active:=True;

          for i:=0 to ViewMenuCr.Controller.SelectedRecordCount-1 do
          begin
            Rec:=ViewMenuCr.Controller.SelectedRecords[i];

            for j:=0 to Rec.ValueCount-1 do
            begin
              if ViewMenuCr.Columns[j].Name='ViewMenuCrSIFR' then break;
            end;

            iNum:=Rec.Values[j];
          //��� ��� - ����������

            if taMenu.Locate('SIFR',iNum,[]) then
            begin
              trUpd2.StartTransaction;
              taMenu.Edit;
              taMenuPARENT.AsInteger:=iGr;
              taMenu.Post;
              trUpd2.Commit;
            end;
          end;
          taMenu.Active:=False;
          quMenuSel.FullRefresh;
        end;
      end;
    end;
}


end;

procedure TfmTabs.SpeedItem5Click(Sender: TObject);
begin
  with dmC do
  begin
//    StatusBar1.Panels[0].Text:='����� ���� ������������.'; delay(10);
    if LevelTabs.Visible=True then
    begin //ViewTabs
//      StatusBar1.Panels[0].Text:='����� ���� ������������.'; delay(10);
      prExportExel(ViewTabs,dsTabsAllSel,taTabsAllSel);
//      StatusBar1.Panels[0].Text:='������������ ��.';
    end else //levelspec
    begin
//      StatusBar1.Panels[0].Text:='����� ���� ������������.'; delay(10);
      prExportExel(ViewSpecReps,dsSelSpecAll,quSelSpecAll);
//      StatusBar1.Panels[0].Text:='������������ ��.';
    end;
  end;
end;

end.
