unit SetStatusBZ;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxLookAndFeelPainters, Menus, StdCtrls, cxButtons, cxControls,
  cxContainer, cxEdit, cxGroupBox, cxRadioGroup, ExtCtrls;

type
  TfmSetStatusBZ = class(TForm)
    cxRadioGroup1: TcxRadioGroup;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    Panel1: TPanel;
    cxButton4: TcxButton;
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmSetStatusBZ: TfmSetStatusBZ;

implementation

{$R *.dfm}

procedure TfmSetStatusBZ.cxButton1Click(Sender: TObject);
begin
  cxButton4.Tag:=1;
  ModalResult:=mrOk;
end;

procedure TfmSetStatusBZ.cxButton2Click(Sender: TObject);
begin
  cxButton4.Tag:=2;
  ModalResult:=mrOk;
end;

procedure TfmSetStatusBZ.cxButton3Click(Sender: TObject);
begin
  cxButton4.Tag:=3;
  ModalResult:=mrOk;
end;

end.
