unit PCardList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons,
  ExtCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid;

type
  TfmPCardList = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    ViewPCL: TcxGridDBTableView;
    LevelPCL: TcxGridLevel;
    GridPCL: TcxGrid;
    ViewPCLCLINAME: TcxGridDBColumn;
    ViewPCLDATEFROM: TcxGridDBColumn;
    ViewPCLDATETO: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmPCardList: TfmPCardList;

implementation

uses Dm;

{$R *.dfm}

procedure TfmPCardList.FormCreate(Sender: TObject);
begin
  GridPCL.Align:=AlClient;
end;

end.
