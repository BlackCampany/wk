unit CreateBFromP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons, ExtCtrls,
  cxControls, cxContainer, cxEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit,
  cxCalc, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, DB, cxDBData, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid,
  FIBDataSet, pFIBDataSet, cxCalendar;

type
  TfmCreateBFromP = class(TForm)
    Panel2: TPanel;
    Label1: TLabel;
    cxCalcEdit1: TcxCalcEdit;
    quInputF: TpFIBDataSet;
    quInputFIDC: TFIBIntegerField;
    quInputFSHORTNAME: TFIBStringField;
    quInputFRECEIPTNUM: TFIBStringField;
    quInputFPCOUNT: TFIBIntegerField;
    quInputFPVES: TFIBFloatField;
    quInputFCURMESSURE: TFIBIntegerField;
    quInputFNAMESHORT: TFIBStringField;
    quInputFNETTO: TFIBFloatField;
    quInputFPARENT: TFIBIntegerField;
    quInputFSGR: TStringField;
    quInputFSSGR: TStringField;
    dsquInputF: TDataSource;
    GridF: TcxGrid;
    ViewF: TcxGridDBTableView;
    ViewFIDC: TcxGridDBColumn;
    ViewFSHORTNAME: TcxGridDBColumn;
    ViewFRECEIPTNUM: TcxGridDBColumn;
    ViewFPCOUNT: TcxGridDBColumn;
    ViewFPVES: TcxGridDBColumn;
    ViewFNAMESHORT: TcxGridDBColumn;
    ViewFNETTO2: TcxGridDBColumn;
    ViewFSGR: TcxGridDBColumn;
    ViewFSSGR: TcxGridDBColumn;
    LevelF: TcxGridLevel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Label2: TLabel;
    cxDateEdit1: TcxDateEdit;
    quInputFIDCT: TFIBIntegerField;
    quInputFNETTO1: TFIBFloatField;
    quInputFBRUTTO: TFIBFloatField;
    quInputFNETTO2: TFIBFloatField;
    quInputFKOEF: TFIBFloatField;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmCreateBFromP: TfmCreateBFromP;

implementation

{$R *.dfm}

procedure TfmCreateBFromP.FormCreate(Sender: TObject);
begin
  GridF.Align:=AlClient;
end;

end.
