object fmAddBGU: TfmAddBGU
  Left = 377
  Top = 503
  BorderStyle = bsDialog
  Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1074' '#1089#1087#1088#1072#1074#1086#1095#1085#1080#1082' '#1101#1085#1077#1088#1075#1077#1090#1080#1095#1077#1089#1082#1086#1081' '#1094#1077#1085#1085#1086#1089#1090#1080
  ClientHeight = 234
  ClientWidth = 574
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 8
    Width = 49
    Height = 13
    Caption = #1043#1088#1091#1087#1087#1072
  end
  object Label13: TLabel
    Left = 272
    Top = 104
    Width = 31
    Height = 13
    Caption = #1041#1077#1083#1082#1080
    Transparent = True
  end
  object Label14: TLabel
    Left = 272
    Top = 128
    Width = 31
    Height = 13
    Caption = #1046#1080#1088#1099
    Transparent = True
  end
  object Label15: TLabel
    Left = 272
    Top = 152
    Width = 51
    Height = 13
    Caption = #1059#1075#1083#1077#1074#1086#1076#1099
    Transparent = True
  end
  object Label16: TLabel
    Left = 272
    Top = 180
    Width = 70
    Height = 26
    Caption = #1069#1085'.'#1094#1077#1085#1085#1086#1089#1090#1100#13#10#1050#1050#1072#1083'. '#1085#1072' 100'#1075
    Transparent = True
  end
  object Panel1: TPanel
    Left = 12
    Top = 28
    Width = 237
    Height = 193
    BevelInner = bvLowered
    TabOrder = 0
    object GrABGU: TcxGrid
      Left = 2
      Top = 2
      Width = 233
      Height = 189
      Align = alClient
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
      object ViewABGU: TcxGridDBTableView
        NavigatorButtons.ConfirmDelete = False
        DataController.DataSource = dstaBGUGr
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsView.GroupByBox = False
        object ViewABGUID: TcxGridDBColumn
          Caption = #1050#1086#1076
          DataBinding.FieldName = 'ID'
          Width = 52
        end
        object ViewABGUNAMEGR: TcxGridDBColumn
          Caption = #1053#1072#1079#1074#1072#1085#1080#1077
          DataBinding.FieldName = 'NAMEGR'
          Width = 155
        end
      end
      object LevABGU: TcxGridLevel
        GridView = ViewABGU
      end
    end
  end
  object cxCalcEdit5: TcxCalcEdit
    Left = 344
    Top = 180
    EditValue = 0.000000000000000000
    Style.Shadow = True
    TabOrder = 1
    Width = 65
  end
  object cxCalcEdit4: TcxCalcEdit
    Left = 344
    Top = 148
    EditValue = 0.000000000000000000
    Style.Shadow = True
    TabOrder = 2
    Width = 65
  end
  object cxCalcEdit3: TcxCalcEdit
    Left = 344
    Top = 124
    EditValue = 0.000000000000000000
    Style.Shadow = True
    TabOrder = 3
    Width = 65
  end
  object cxCalcEdit2: TcxCalcEdit
    Left = 344
    Top = 100
    EditValue = 0.000000000000000000
    Style.Shadow = True
    TabOrder = 4
    Width = 65
  end
  object cxTextEdit1: TcxTextEdit
    Left = 268
    Top = 40
    TabOrder = 5
    Text = 'cxTextEdit1'
    Width = 281
  end
  object cxButton1: TcxButton
    Left = 440
    Top = 100
    Width = 109
    Height = 45
    Caption = #1044#1086#1073'. '#1074' '#1089#1087#1088#1072#1074#1086#1095#1085#1080#1082
    TabOrder = 6
    OnClick = cxButton1Click
    LookAndFeel.Kind = lfOffice11
  end
  object cxButton2: TcxButton
    Left = 440
    Top = 164
    Width = 109
    Height = 45
    Caption = #1042#1099#1093#1086#1076
    ModalResult = 2
    TabOrder = 7
    LookAndFeel.Kind = lfOffice11
  end
  object taBGUGr: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_BGUGR'
      'SET '
      '    NAMEGR = :NAMEGR'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_BGUGR'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_BGUGR('
      '    ID,'
      '    NAMEGR'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAMEGR'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    NAMEGR'
      'FROM'
      '    OF_BGUGR '
      ''
      ' WHERE '
      '        OF_BGUGR.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    NAMEGR'
      'FROM'
      '    OF_BGUGR ')
    Transaction = dmO.trSel3
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdEU
    AutoCommit = True
    Left = 40
    Top = 64
    object taBGUGrID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taBGUGrNAMEGR: TFIBStringField
      FieldName = 'NAMEGR'
      Size = 50
      EmptyStrToNull = True
    end
  end
  object dstaBGUGr: TDataSource
    DataSet = taBGUGr
    Left = 40
    Top = 116
  end
  object quMaxBGU: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT max(ID) as MaxId FROM OF_BGU')
    Transaction = dmO.trSel3
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    AutoCommit = True
    ReceiveEvents.Strings = (
      'Select max(ID) as MaxId from OF_BGUOBR')
    Left = 116
    Top = 71
    poAskRecordCount = True
    object quMaxBGUMAXID: TFIBIntegerField
      FieldName = 'MAXID'
    end
  end
end
