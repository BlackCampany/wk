unit Period3;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, cxGraphics, cxDropDownEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxControls,
  cxContainer, cxEdit, cxTextEdit, cxMaskEdit, cxCalendar, StdCtrls,
  cxButtons, ExtCtrls, DB, FIBDataSet, pFIBDataSet, cxCheckBox, cxSpinEdit,
  cxTimeEdit, FIBDatabase, pFIBDatabase, FIBQuery, pFIBQuery,
  pFIBStoredProc, cxMemo;

type
  TfmSelPerSkl1 = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    Label3: TLabel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxDateEdit1: TcxDateEdit;
    cxDateEdit2: TcxDateEdit;
    cxLookupComboBox1: TcxLookupComboBox;
    quMHAll1: TpFIBDataSet;
    quMHAll1ID: TFIBIntegerField;
    quMHAll1PARENT: TFIBIntegerField;
    quMHAll1ITYPE: TFIBIntegerField;
    quMHAll1NAMEMH: TFIBStringField;
    quMHAll1DEFPRICE: TFIBIntegerField;
    quMHAll1NAMEPRICE: TFIBStringField;
    dsMHAll1: TDataSource;
    cxTimeEdit1: TcxTimeEdit;
    Label2: TLabel;
    cxTimeEdit2: TcxTimeEdit;
    Memo1: TcxMemo;
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxLookupComboBox1PropertiesChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmSelPerSkl1: TfmSelPerSkl1;

implementation

uses dmOffice, Un1, DMOReps, InputMess, dbCash;

{$R *.dfm}

procedure TfmSelPerSkl1.cxButton2Click(Sender: TObject);
begin
  close;
end;

procedure TfmSelPerSkl1.cxButton1Click(Sender: TObject);
Var CurDate:TDateTime;
    DateB,DateE,iCurDate:Integer;
    NumDB:Integer;
    iTag:INteger;
    iFind:INteger;
begin
  CommonSet.DateFrom:=Trunc(cxDateEdit1.Date);
  CommonSet.DateTo:=Trunc(cxDateEdit2.Date);

  DateB:=Trunc(cxDateEdit1.Date);
  DateE:=Trunc(cxDateEdit2.Date);

  NumDB:=cxLookupComboBox1.EditValue;

//  Label4.Caption:='���������� ����� ������.';delay(10);

  Memo1.Clear;
  Memo1.Lines.Add('���������� ����� ������.');delay(10);

  cxButton1.Enabled:=False;
  cxButton2.Enabled:=False;

  with dmO do
  with dmORep do
  with dmCash do
  begin
    for iCurDate:=DateB to DateE-1 do
    begin
      CurDate:=iCurDate+cxTimeEdit1.Time;

      iTag:=0;

      iFind:=prFindReal(NumDB,Trunc(CurDate),0);//���� ��������� , ��� �������� �� ���������

      if iFind>0 then
      begin
        quTestInput.Active:=False;
        quTestInput.ParamByName('IDB').AsInteger:=NumDB;
        quTestInput.ParamByName('DDATE').AsDateTime:=iCurDate;
        quTestInput.Active:=True;

        fmInputMess:=TfmInputMess.Create(Application);

        if iFind=1 then fmInputMess.cxButton1.Enabled:=False;

        fmInputMess.GrInpMess.Tag:=0;
        fmInputMess.ShowModal;
        iTag:= fmInputMess.GrInpMess.Tag;
        fmInputMess.Release;
      end;

      if iTag=1 then
      begin //�������
        if MessageDlg('����� ��������� ���������, ��� � ����� ����������� ����� �� ��������. ���������� ��������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          quTestInput.First;
          while not quTestInput.Eof do quTestInput.Delete;
          quDocsOutB.FullRefresh;
        end;
      end;
      if iTag=2 then
      begin //����������


      end;
      if iTag=3 then
      begin //��������
        Memo1.Lines.Add('����� ������ ����������.');delay(10);
//        Label4.Caption:='����� ������ ����������.';delay(10);
        cxButton1.Enabled:=True;
        cxButton2.Enabled:=True;
        exit;
      end;

      if quTestInput.Active then quTestInput.Active:=False;

      prCloseCashDbDate(DBName,CurDate,NumDB,Memo1);

    end;
  end;
  Memo1.Lines.Add('����� ������ ��������.');delay(500);

//  Label4.Caption:='����� ������ ��������.';Delay(10);
  cxButton1.Enabled:=True;
  cxButton2.Enabled:=True;
  delay(500);
  ModalResult:=mrOk;
end;

procedure TfmSelPerSkl1.cxLookupComboBox1PropertiesChange(Sender: TObject);
begin
  try
    if dmORep.quDB.Locate('ID',cxLookupComboBox1.EditValue,[]) then
      DBName:=dmORep.quDBPATHDB.AsString;
  except
  end;    
end;

procedure TfmSelPerSkl1.FormShow(Sender: TObject);
begin
  Memo1.Clear;
end;

end.
