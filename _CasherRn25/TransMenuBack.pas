unit TransMenuBack;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Placemnt, ComCtrls, SpeedBar, ExtCtrls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  RXSplit, cxCurrencyEdit, cxImageComboBox, cxTextEdit, XPStyleActnCtrls,
  ActnList, ActnMan, Menus, ToolWin, ActnCtrls, ActnMenus, StdCtrls,
  FR_Class, FR_DSet, FR_DBSet, DBClient, FR_BarC, FIBDataSet, pFIBDataSet,
  FIBDatabase, pFIBDatabase, ImgList, cxLookAndFeelPainters, cxButtons,
  cxContainer, cxMaskEdit, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox, cxTreeView, FIBQuery, pFIBQuery, pFIBStoredProc,
  cxMemo, dxmdaset;

type
  TfmMenuCr = class(TForm)
    FormPlacement1: TFormPlacement;
    StatusBar1: TStatusBar;
    RxSplitter1: TRxSplitter;
    am3: TActionManager;
    acExit: TAction;
    Timer1: TTimer;
    ActionMainMenuBar1: TActionMainMenuBar;
    CasherRnDb: TpFIBDatabase;
    trSelM: TpFIBTransaction;
    trUpdM: TpFIBTransaction;
    quMenuTree: TpFIBDataSet;
    quMenuSel: TpFIBDataSet;
    quMenuSelSIFR: TFIBIntegerField;
    quMenuSelNAME: TFIBStringField;
    quMenuSelPRICE: TFIBFloatField;
    quMenuSelCODE: TFIBStringField;
    quMenuSelTREETYPE: TFIBStringField;
    quMenuSelLIMITPRICE: TFIBFloatField;
    quMenuSelCATEG: TFIBSmallIntField;
    quMenuSelPARENT: TFIBSmallIntField;
    quMenuSelLINK: TFIBSmallIntField;
    quMenuSelSTREAM: TFIBSmallIntField;
    quMenuSelLACK: TFIBSmallIntField;
    quMenuSelDESIGNSIFR: TFIBSmallIntField;
    quMenuSelALTNAME: TFIBStringField;
    quMenuSelNALOG: TFIBFloatField;
    quMenuSelBARCODE: TFIBStringField;
    quMenuSelIMAGE: TFIBSmallIntField;
    quMenuSelCONSUMMA: TFIBFloatField;
    quMenuSelMINREST: TFIBSmallIntField;
    quMenuSelPRNREST: TFIBSmallIntField;
    quMenuSelCOOKTIME: TFIBSmallIntField;
    quMenuSelDISPENSER: TFIBSmallIntField;
    quMenuSelDISPKOEF: TFIBSmallIntField;
    quMenuSelACCESS: TFIBSmallIntField;
    quMenuSelFLAGS: TFIBSmallIntField;
    quMenuSelTARA: TFIBSmallIntField;
    quMenuSelCNTPRICE: TFIBSmallIntField;
    quMenuSelBACKBGR: TFIBFloatField;
    quMenuSelFONTBGR: TFIBFloatField;
    quMenuSelIACTIVE: TFIBSmallIntField;
    quMenuSelIEDIT: TFIBSmallIntField;
    quMenuSelNAMECA: TFIBStringField;
    quMenuSelNAMEMO: TFIBStringField;
    dsMenuSel: TDataSource;
    quMenuFindParent: TpFIBDataSet;
    quMenuFindParentCOUNTREC: TFIBIntegerField;
    quMaxIdM: TpFIBDataSet;
    quMaxIdMMAXID: TFIBIntegerField;
    quMaxIdMe: TpFIBDataSet;
    quMaxIdMeMAXID: TFIBIntegerField;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    cxStyle14: TcxStyle;
    cxStyle15: TcxStyle;
    cxStyle16: TcxStyle;
    cxStyle17: TcxStyle;
    cxStyle18: TcxStyle;
    cxStyle19: TcxStyle;
    cxStyle20: TcxStyle;
    cxStyle21: TcxStyle;
    cxStyle22: TcxStyle;
    cxStyle23: TcxStyle;
    cxStyle24: TcxStyle;
    cxStyle25: TcxStyle;
    quFindB: TpFIBDataSet;
    dsFindB: TDataSource;
    quFindBSIFR: TFIBIntegerField;
    quFindBNAME: TFIBStringField;
    quFindBPRICE: TFIBFloatField;
    quFindBCODE: TFIBStringField;
    quFindBPARENT: TFIBSmallIntField;
    quFindBCONSUMMA: TFIBFloatField;
    quModGrMo: TpFIBDataSet;
    quModGrMoSIFR: TFIBIntegerField;
    quModGrMoNAME: TFIBStringField;
    quModGrMoPARENT: TFIBIntegerField;
    quModGrMoPRICE: TFIBFloatField;
    quModGrMoREALPRICE: TFIBFloatField;
    quModGrMoIACTIVE: TFIBSmallIntField;
    quModGrMoIEDIT: TFIBSmallIntField;
    quMods: TpFIBDataSet;
    quModsSIFR: TFIBIntegerField;
    quModsNAME: TFIBStringField;
    quModsPARENT: TFIBIntegerField;
    quModsPRICE: TFIBFloatField;
    quModsREALPRICE: TFIBFloatField;
    quModsIACTIVE: TFIBSmallIntField;
    quModsIEDIT: TFIBSmallIntField;
    quModsCODEB: TFIBIntegerField;
    quModsKB: TFIBFloatField;
    dsMods: TDataSource;
    quMaxIdMod: TpFIBDataSet;
    quMaxIdModMAXID: TFIBIntegerField;
    acEditM: TAction;
    quStream: TpFIBDataSet;
    quStreamID: TFIBIntegerField;
    quStreamNAMESTREAM: TFIBStringField;
    dsquStream: TDataSource;
    imState: TImageList;
    Panel1: TPanel;
    Panel3: TPanel;
    MenuTree: TTreeView;
    GridMenuCr: TcxGrid;
    ViewMenuCr: TcxGridDBTableView;
    ViewMenuCrIACTIVE1: TcxGridDBColumn;
    ViewMenuCrSIFR: TcxGridDBColumn;
    ViewMenuCrNAME: TcxGridDBColumn;
    ViewMenuCrPRICE: TcxGridDBColumn;
    ViewMenuCrCODE: TcxGridDBColumn;
    ViewMenuCrCONSUMMA: TcxGridDBColumn;
    ViewMenuCrSTREAM: TcxGridDBColumn;
    ViewMenuCrALTNAME: TcxGridDBColumn;
    ViewMenuCrBARCODE: TcxGridDBColumn;
    ViewMenuCrIACTIVE: TcxGridDBColumn;
    ViewMenuCrNAMECA: TcxGridDBColumn;
    ViewMenuCrNAMEMO: TcxGridDBColumn;
    ViewMenuCrLIMITPRICE: TcxGridDBColumn;
    ViewMods: TcxGridDBTableView;
    ViewModsSIFR: TcxGridDBColumn;
    ViewModsNAME: TcxGridDBColumn;
    ViewModsCODEB: TcxGridDBColumn;
    ViewModsKB: TcxGridDBColumn;
    LevelMenuCr: TcxGridLevel;
    LevelMods: TcxGridLevel;
    Panel4: TPanel;
    Label2: TLabel;
    cxLookupComboBox1: TcxLookupComboBox;
    Panel2: TPanel;
    Label1: TLabel;
    cxTextEdit1: TcxTextEdit;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    SpeedBar1: TSpeedBar;
    acPeriodDM: TAction;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    ViewDM: TcxGridDBTableView;
    LevelDM: TcxGridLevel;
    GridDM: TcxGrid;
    TreeMD: TcxTreeView;
    quMenuDayList: TpFIBDataSet;
    quMenuDayListID: TFIBSmallIntField;
    quMenuDayListNAME: TFIBStringField;
    quMenuDayListMDATE: TFIBDateField;
    quMenuDayListIACTIVE: TFIBIntegerField;
    dsquMenuDayList: TDataSource;
    ViewDMNAME: TcxGridDBColumn;
    ViewDMMDATE: TcxGridDBColumn;
    acAddMD: TAction;
    prGetId: TpFIBStoredProc;
    quMDGID: TpFIBDataSet;
    quMDGIDIDH: TFIBIntegerField;
    quMDGIDID: TFIBIntegerField;
    quMDGIDNAMEGR: TFIBStringField;
    quMDGIDPRIOR: TFIBIntegerField;
    quMDGIDPARENT: TFIBIntegerField;
    quMDGroups: TpFIBDataSet;
    quMDGroupsIDH: TFIBIntegerField;
    quMDGroupsID: TFIBIntegerField;
    quMDGroupsNAMEGR: TFIBStringField;
    quMDGroupsPRIOR: TFIBIntegerField;
    quMDGroupsPARENT: TFIBIntegerField;
    trSel1: TpFIBTransaction;
    trUpd1: TpFIBTransaction;
    SpeedItem2: TSpeedItem;
    acEditMD: TAction;
    acDelMD: TAction;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    quMenuDay: TpFIBDataSet;
    quMenuDayIDH: TFIBIntegerField;
    quMenuDayID: TFIBIntegerField;
    quMenuDayNAME: TFIBStringField;
    quMenuDayPRIOR: TFIBIntegerField;
    quMenuDayPARENT: TFIBIntegerField;
    acAddMDGr: TAction;
    acAddMDSGr: TAction;
    acEditMDGr: TAction;
    acDelMDGr: TAction;
    quMaxMDG: TpFIBDataSet;
    quMaxMDGID: TFIBIntegerField;
    quMDS: TpFIBDataSet;
    dsquMDS: TDataSource;
    trSel2: TpFIBTransaction;
    trUpd2: TpFIBTransaction;
    quMDSIDH: TFIBIntegerField;
    quMDSIDGR: TFIBIntegerField;
    quMDSSIFR: TFIBIntegerField;
    quMDSPRI: TFIBIntegerField;
    quMDSNAME: TFIBStringField;
    quMDSPRICE: TFIBFloatField;
    quMDSALTNAME: TFIBStringField;
    SpeedItem5: TSpeedItem;
    SpeedItem6: TSpeedItem;
    SpeedItem7: TSpeedItem;
    SpeedItem8: TSpeedItem;
    quMDFindParent: TpFIBDataSet;
    quMDFindParentCOUNTREC: TFIBIntegerField;
    PopupMenu1: TPopupMenu;
    acAddMGr1: TMenuItem;
    acAddMSubGr1: TMenuItem;
    N1: TMenuItem;
    N2: TMenuItem;
    N6: TMenuItem;
    quMenuId: TpFIBDataSet;
    quMenuIdSIFR: TFIBIntegerField;
    quMenuIdNAME: TFIBStringField;
    quMenuIdPRICE: TFIBFloatField;
    quMenuIdCODE: TFIBStringField;
    quMenuIdTREETYPE: TFIBStringField;
    quMenuIdLIMITPRICE: TFIBFloatField;
    quMenuIdCATEG: TFIBSmallIntField;
    quMenuIdPARENT: TFIBSmallIntField;
    quMenuIdLINK: TFIBSmallIntField;
    quMenuIdSTREAM: TFIBSmallIntField;
    quMenuIdLACK: TFIBSmallIntField;
    quMenuIdDESIGNSIFR: TFIBSmallIntField;
    quMenuIdALTNAME: TFIBStringField;
    quMenuIdNALOG: TFIBFloatField;
    quMenuIdBARCODE: TFIBStringField;
    quMenuIdIMAGE: TFIBSmallIntField;
    quMenuIdCONSUMMA: TFIBFloatField;
    quMenuIdMINREST: TFIBSmallIntField;
    quMenuIdPRNREST: TFIBSmallIntField;
    quMenuIdCOOKTIME: TFIBSmallIntField;
    quMenuIdDISPENSER: TFIBSmallIntField;
    quMenuIdDISPKOEF: TFIBSmallIntField;
    quMenuIdACCESS: TFIBSmallIntField;
    quMenuIdFLAGS: TFIBSmallIntField;
    quMenuIdTARA: TFIBSmallIntField;
    quMenuIdCNTPRICE: TFIBSmallIntField;
    quMenuIdBACKBGR: TFIBFloatField;
    quMenuIdFONTBGR: TFIBFloatField;
    quMenuIdIACTIVE: TFIBSmallIntField;
    quMenuIdIEDIT: TFIBSmallIntField;
    quMenuIdDATEB: TFIBIntegerField;
    quMenuIdDATEE: TFIBIntegerField;
    quMenuIdDAYWEEK: TFIBStringField;
    quMenuIdTIMEB: TFIBTimeField;
    quMenuIdTIMEE: TFIBTimeField;
    quMenuIdALLTIME: TFIBSmallIntField;
    quMenuGrMD: TpFIBDataSet;
    quMenuGrMDSIFR: TFIBIntegerField;
    quMenuGrMDNAME: TFIBStringField;
    quMenuGrMDPRICE: TFIBFloatField;
    quMenuGrMDCODE: TFIBStringField;
    quMenuGrMDTREETYPE: TFIBStringField;
    quMenuGrMDLIMITPRICE: TFIBFloatField;
    quMenuGrMDCATEG: TFIBSmallIntField;
    quMenuGrMDPARENT: TFIBSmallIntField;
    quMenuGrMDLINK: TFIBSmallIntField;
    quMenuGrMDSTREAM: TFIBSmallIntField;
    quMenuGrMDLACK: TFIBSmallIntField;
    quMenuGrMDDESIGNSIFR: TFIBSmallIntField;
    quMenuGrMDALTNAME: TFIBStringField;
    quMenuGrMDNALOG: TFIBFloatField;
    quMenuGrMDBARCODE: TFIBStringField;
    quMenuGrMDIMAGE: TFIBSmallIntField;
    quMenuGrMDCONSUMMA: TFIBFloatField;
    quMenuGrMDMINREST: TFIBSmallIntField;
    quMenuGrMDPRNREST: TFIBSmallIntField;
    quMenuGrMDCOOKTIME: TFIBSmallIntField;
    quMenuGrMDDISPENSER: TFIBSmallIntField;
    quMenuGrMDDISPKOEF: TFIBSmallIntField;
    quMenuGrMDACCESS: TFIBSmallIntField;
    quMenuGrMDFLAGS: TFIBSmallIntField;
    quMenuGrMDTARA: TFIBSmallIntField;
    quMenuGrMDCNTPRICE: TFIBSmallIntField;
    quMenuGrMDBACKBGR: TFIBFloatField;
    quMenuGrMDFONTBGR: TFIBFloatField;
    quMenuGrMDIACTIVE: TFIBSmallIntField;
    quMenuGrMDIEDIT: TFIBSmallIntField;
    quMenuGrMDDATEB: TFIBIntegerField;
    quMenuGrMDDATEE: TFIBIntegerField;
    quMenuGrMDDAYWEEK: TFIBStringField;
    quMenuGrMDTIMEB: TFIBTimeField;
    quMenuGrMDTIMEE: TFIBTimeField;
    quMenuGrMDALLTIME: TFIBSmallIntField;
    quMaxIdMeGr: TpFIBDataSet;
    quMaxIdMeGrMAXID: TFIBIntegerField;
    PopupMenu2: TPopupMenu;
    acEditM1: TMenuItem;
    PopupMenu3: TPopupMenu;
    acCopyDM: TAction;
    N3: TMenuItem;
    quMDSSel: TpFIBDataSet;
    quMDSEdit: TpFIBDataSet;
    trSel3: TpFIBTransaction;
    trUpd3: TpFIBTransaction;
    acRecalcPrice: TAction;
    Panel5: TPanel;
    GridMenuDay: TcxGrid;
    ViewMenuDay: TcxGridDBTableView;
    ViewMenuDaySIFR: TcxGridDBColumn;
    ViewMenuDayNAME: TcxGridDBColumn;
    ViewMenuDayPRICE: TcxGridDBColumn;
    ViewMenuDayALTNAME: TcxGridDBColumn;
    ViewMenuDayPRI: TcxGridDBColumn;
    LevelMenuDay: TcxGridLevel;
    Memo1: TcxMemo;
    SpeedItem9: TSpeedItem;
    PopupMenu4: TPopupMenu;
    N4: TMenuItem;
    ViewMenuDayCODE: TcxGridDBColumn;
    ViewMenuDayCONSUMMA: TcxGridDBColumn;
    quMDSCODE: TFIBStringField;
    quMDSCONSUMMA: TFIBFloatField;
    acDelPos: TAction;
    SpeedItem10: TSpeedItem;
    SpeedItem11: TSpeedItem;
    quMDSSTREAM: TFIBSmallIntField;
    quMenuRec: TpFIBDataSet;
    quMenuRecSIFR: TFIBIntegerField;
    quMenuRecNAME: TFIBStringField;
    quMenuRecPRICE: TFIBFloatField;
    quMenuRecCODE: TFIBStringField;
    quMenuRecTREETYPE: TFIBStringField;
    quMenuRecLIMITPRICE: TFIBFloatField;
    quMenuRecCATEG: TFIBSmallIntField;
    quMenuRecPARENT: TFIBSmallIntField;
    quMenuRecLINK: TFIBSmallIntField;
    quMenuRecSTREAM: TFIBSmallIntField;
    quMenuRecLACK: TFIBSmallIntField;
    quMenuRecDESIGNSIFR: TFIBSmallIntField;
    quMenuRecALTNAME: TFIBStringField;
    quMenuRecNALOG: TFIBFloatField;
    quMenuRecBARCODE: TFIBStringField;
    quMenuRecIMAGE: TFIBSmallIntField;
    quMenuRecCONSUMMA: TFIBFloatField;
    quMenuRecMINREST: TFIBSmallIntField;
    quMenuRecPRNREST: TFIBSmallIntField;
    quMenuRecCOOKTIME: TFIBSmallIntField;
    quMenuRecDISPENSER: TFIBSmallIntField;
    quMenuRecDISPKOEF: TFIBSmallIntField;
    quMenuRecACCESS: TFIBSmallIntField;
    quMenuRecFLAGS: TFIBSmallIntField;
    quMenuRecTARA: TFIBSmallIntField;
    quMenuRecCNTPRICE: TFIBSmallIntField;
    quMenuRecBACKBGR: TFIBFloatField;
    quMenuRecFONTBGR: TFIBFloatField;
    quMenuRecIACTIVE: TFIBSmallIntField;
    quMenuRecIEDIT: TFIBSmallIntField;
    quMenuRecDATEB: TFIBIntegerField;
    quMenuRecDATEE: TFIBIntegerField;
    quMenuRecDAYWEEK: TFIBStringField;
    quMenuRecTIMEB: TFIBTimeField;
    quMenuRecTIMEE: TFIBTimeField;
    quMenuRecALLTIME: TFIBSmallIntField;
    quMDSSelIDH: TFIBIntegerField;
    quMDSSelIDGR: TFIBIntegerField;
    quMDSSelSIFR: TFIBIntegerField;
    quMDSSelPRI: TFIBIntegerField;
    quMDSSelPRICE: TFIBFloatField;
    quMDSEditIDH: TFIBIntegerField;
    quMDSEditIDGR: TFIBIntegerField;
    quMDSEditSIFR: TFIBIntegerField;
    quMDSEditPRI: TFIBIntegerField;
    quMDSEditPRICE: TFIBFloatField;
    acCalcAllMenu: TAction;
    N5: TMenuItem;
    quMDSAll: TpFIBDataSet;
    quMDSAllIDH: TFIBIntegerField;
    quMDSAllIDGR: TFIBIntegerField;
    quMDSAllSIFR: TFIBIntegerField;
    quMDSAllPRI: TFIBIntegerField;
    quMDSAllNAME: TFIBStringField;
    quMDSAllPRICE: TFIBFloatField;
    quMDSAllALTNAME: TFIBStringField;
    quMDSAllCODE: TFIBStringField;
    quMDSAllCONSUMMA: TFIBFloatField;
    quMDSAllSTREAM: TFIBSmallIntField;
    acPrintMenu: TAction;
    SpeedItem12: TSpeedItem;
    quMDRep: TpFIBDataSet;
    quMDRepIDH: TFIBIntegerField;
    quMDRepIDGR: TFIBIntegerField;
    quMDRepSIFR: TFIBIntegerField;
    quMDRepPRI: TFIBIntegerField;
    quMDRepNAMEGR: TFIBStringField;
    quMDRepPRIOR: TFIBIntegerField;
    quMDRepNAME: TFIBStringField;
    quMDRepPRICE: TFIBFloatField;
    quMDRepALTNAME: TFIBStringField;
    frRepMD: TfrReport;
    frquMDRep: TfrDBDataSet;
    acMOL: TAction;
    SpeedItem13: TSpeedItem;
    quMDSAllQUANT: TFIBFloatField;
    quMDSQUANT: TFIBFloatField;
    ViewMenuDayQUANT: TcxGridDBColumn;
    acPrintQuant: TAction;
    SpeedItem14: TSpeedItem;
    teQ: TdxMemData;
    teQiCode: TIntegerField;
    teQName: TStringField;
    teQQuant: TFloatField;
    frteC1: TfrDBDataSet;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure MenuTreeExpanding(Sender: TObject; Node: TTreeNode;
      var AllowExpansion: Boolean);
    procedure MenuTreeChange(Sender: TObject; Node: TTreeNode);
    procedure acExitExecute(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure ViewMenuCrCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure ViewMenuCrStartDrag(Sender: TObject;
      var DragObject: TDragObject);
    procedure ViewMenuCrDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure ViewMenuCrDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxLookupComboBox1PropertiesChange(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure ViewModsDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure ViewModsDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure ViewModsStartDrag(Sender: TObject;
      var DragObject: TDragObject);
    procedure acEditMExecute(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure acPeriodDMExecute(Sender: TObject);
    procedure acAddMDExecute(Sender: TObject);
    procedure acEditMDExecute(Sender: TObject);
    procedure acDelMDExecute(Sender: TObject);
    procedure ViewDMFocusedRecordChanged(Sender: TcxCustomGridTableView;
      APrevFocusedRecord, AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure acAddMDGrExecute(Sender: TObject);
    procedure acAddMDSGrExecute(Sender: TObject);
    procedure acEditMDGrExecute(Sender: TObject);
    procedure acDelMDGrExecute(Sender: TObject);
    procedure TreeMDChange(Sender: TObject; Node: TTreeNode);
    procedure TreeMDExpanding(Sender: TObject; Node: TTreeNode;
      var AllowExpansion: Boolean);
    procedure ViewMenuDayDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure ViewMenuDayDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure MenuTreeDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure MenuTreeDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure acCopyDMExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure acRecalcPriceExecute(Sender: TObject);
    procedure Memo1DblClick(Sender: TObject);
    procedure acDelPosExecute(Sender: TObject);
    procedure acCalcAllMenuExecute(Sender: TObject);
    procedure acPrintMenuExecute(Sender: TObject);
    procedure acMOLExecute(Sender: TObject);
    procedure acPrintQuantExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    Procedure RefreshMods;
    Procedure RefreshModTree;
  end;

function prFindDMGR:INteger;  

var
  fmMenuCr: TfmMenuCr;
//  bDrM:Boolean=False;
  bClearMenu:Boolean = False;
  bCreate:Boolean = False;

implementation

uses Un1, dmRnEdit, AddCateg, AddM, dmOffice, Goods, uTransM, FindResult,
  DMOReps, of_EditPosMenu, PeriodUni, AddMDL, AddMDG, RecalcPrice, Message,
  RMol;

{$R *.dfm}

function prFindDMGR:INteger;  
Var Id:INteger;
begin
  with fmMenuCr do
  begin
    quMenuGrMD.Active:=False;
    quMenuGrMD.Active:=True;
    if quMenuGrMD.RecordCount>0 then
    begin
      quMenuGrMD.First;
      Result:=quMenuGrMDSIFR.AsInteger;
    end else //����� ������ ��� - ������
    begin
      quMaxIdMeGr.Active:=False;
      quMaxIdMeGr.Active:=True;
      Id:=quMaxIdMeGrMAXID.AsInteger+1;
      quMaxIdMeGr.Active:=False;

      quMenuGrMD.Append;
      quMenuGrMDSIFR.AsInteger:=Id;
      quMenuGrMDNAME.AsString:='������� ����';
      quMenuGrMDPRICE.AsFloat:=0;
      quMenuGrMDCODE.AsString:='0';
      quMenuGrMDCONSUMMA.AsFloat:=0;
      quMenuGrMDTREETYPE.AsString:='T';
      quMenuGrMDLIMITPRICE.AsFloat:=0;
      quMenuGrMDCATEG.AsInteger:=0;
      quMenuGrMDPARENT.AsInteger:=0;
      quMenuGrMDLINK.AsInteger:=0;
      quMenuGrMDSTREAM.AsInteger:=0;
      quMenuGrMDLACK.AsInteger:=0;
      quMenuGrMDDESIGNSIFR.AsInteger:=0;
      quMenuGrMDALTNAME.AsString:='';
      quMenuGrMDNALOG.AsFloat:=0;
      quMenuGrMDBARCODE.AsString:='';
      quMenuGrMDIMAGE.AsInteger:=0;
      quMenuGrMDMINREST.AsFloat:=0;
      quMenuGrMDPRNREST.AsInteger:=0;
      quMenuGrMDCOOKTIME.AsInteger:=0;
      quMenuGrMDDISPENSER.AsInteger:=0;
      quMenuGrMDDISPKOEF.AsInteger:=0;
      quMenuGrMDACCESS.AsInteger:=0;
      quMenuGrMDFLAGS.AsInteger:=0;
      quMenuGrMDTARA.AsInteger:=0;
      quMenuGrMDCNTPRICE.AsInteger:=0;
      quMenuGrMDBACKBGR.AsFloat:=0;
      quMenuGrMDFONTBGR.AsFloat:=0;
      quMenuGrMDIACTIVE.AsInteger:=1;
      quMenuGrMDIEDIT.AsInteger:=0;
      quMenuGrMD.Post;

      Result:=Id;
    end;

    quMenuGrMD.Active:=False;
  end;
end;

Procedure TfmMenuCr.RefreshMods;
begin
  quMods.Active:=False;
  quMods.ParamByName('PARENTID').AsInteger:=Integer(MenuTree.Selected.Data);
  quMods.Active:=True;
  quMods.First;
end;

Procedure TfmMenuCr.RefreshModTree;
Var TreeNode : TTreeNode;
begin
  MenuTree.Items.Clear;

  quModGrMo.Active:=False;
  quModGrMo.Active:=True;
  quModGrMo.First;
  while not quModGrMo.Eof do
  begin
    TreeNode:=MenuTree.Items.AddChildObject(nil, quModGrMoNAME.AsString, Pointer(quModGrMoSIFR.AsInteger));
    TreeNode.ImageIndex:=8;
    TreeNode.SelectedIndex:=7;

    quModGrMo.Next;
  end;
  if MenuTree.Items.Count>0 then
  begin
    MenuTree.Items[0].Selected:=True;
    MenuTree.Repaint;
    delay(10);
  end;
end;



procedure TfmMenuCr.FormCreate(Sender: TObject);
begin
  bCreate:=True;
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;

  Panel3.Align:=AlClient;
  Panel4.Align:=AlClient;

  cxButton4.Caption:='�� ��� ������� ����';
  cxButton4.Tag:=1;

  GridMenuCr.Align:=AlClient;

  Panel5.Align:=AlClient;

  GridMenuDay.Align:=AlClient;

  ViewMenuCr.RestoreFromIniFile(CurDir+GridIni);

  cxTextEdit1.Text:='';

  with dmORep do
  begin
    if quDB.Active then
    begin
      cxLookupComboBox1.EditValue:=quDBID.AsInteger;
      cxLookupComboBox1.Text:=quDBNAMEDB.AsString;
      DBName:=dmORep.quDBPATHDB.AsString;
    end;
  end;

  CasherRnDb.Connected:=False;
  CasherRnDb.DBName:=DBName;
  try
    CasherRnDb.Open;

    bMenuList:=False;
    MenuTree.Items.Clear;
    CardsExpandLevel(nil,MenuTree,quMenuTree);

    MenuTree.FullExpand;
    MenuTree.FullCollapse;

    quMenuTree.First;
    bMenuList:=True;

    quMenuSel.Active:=False;
    quMenuSel.ParamByName('PARENTID').AsInteger:=quMenuTree.fieldbyName('Id').AsInteger;
    quMenuSel.Active:=True;
  except
    CasherRnDb.Connected:=False;
  end;
  bCreate:=False;
end;

procedure TfmMenuCr.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CasherRnDb.Connected:=False;
  ViewMenuCr.StoreToIniFile(CurDir+GridIni,False);
  Release;
end;

procedure TfmMenuCr.MenuTreeExpanding(Sender: TObject; Node: TTreeNode;
  var AllowExpansion: Boolean);
begin
  if Node.getFirstChild.Data = nil then
  begin
    Node.DeleteChildren;
    CardsExpandLevel(Node,MenuTree,quMenuTree);
  end;
end;

procedure TfmMenuCr.MenuTreeChange(Sender: TObject; Node: TTreeNode);
begin
  if bMenuList then
  begin
    if cxButton3.Tag=0 then
    begin
      ViewMenuCr.BeginUpdate;

      quMenuSel.Active:=False;
      quMenuSel.ParamByName('PARENTID').AsInteger:=Integer(MenuTree.Selected.Data);
      quMenuSel.Active:=True;

      ViewMenuCr.EndUpdate;
    end;
    if cxButton3.Tag=1 then
    begin
      ViewMods.BeginUpdate;
      RefreshMods;
      ViewMods.EndUpdate;
    end;
  end;
end;

procedure TfmMenuCr.acExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmMenuCr.Timer1Timer(Sender: TObject);
begin
  if bClearMenu=True then begin StatusBar1.Panels[0].Text:=''; bClearMenu:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearMenu:=True;
end;

procedure TfmMenuCr.ViewMenuCrCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
Var iType,i:Integer;
begin
  if AViewInfo.GridRecord.Selected then exit;

  iType:=0;
  for i:=0 to ViewMenuCr.ColumnCount-1 do
  begin
    if ViewMenuCr.Columns[i].Name='ViewMenuCrIACTIVE1' then
    begin
      iType:=VarAsType(AViewInfo.GridRecord.DisplayTexts[i], varInteger);
      break;
    end;
  end;

  if iType=0  then
  begin
    ACanvas.Canvas.Brush.Color := clWhite;
    ACanvas.Canvas.Font.Color := clGray;
  end;

end;

procedure TfmMenuCr.ViewMenuCrStartDrag(Sender: TObject;
  var DragObject: TDragObject);
begin
//  if not CanDo('prTransMenu') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
//  bDM:=True;
  if not CanDo('prMoveMenu') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  bDrMove:=True;
end;

procedure TfmMenuCr.ViewMenuCrDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept:=False;
  if bDM then  Accept:=True;
end;

procedure TfmMenuCr.ViewMenuCrDragDrop(Sender, Source: TObject; X,
  Y: Integer);
Var iCo:Integer;
    i,j: Integer;
    iNum: Integer;
    Rec,Rec1:TcxCustomGridRecord;
    iD:INteger;
    sBar:String;
    AHitTest: TcxCustomGridHitTest;
    bAdd:Boolean;
    K:Real;
begin
  if bDM then
  begin
    ResetAddVars;
    iCo:=fmGoods.ViewGoods.Controller.SelectedRecordCount;
    if iCo>1 then
    begin
      if MessageDlg('�� ������������� ������ �������� ��������� ������� ('+IntToStr(iCo)+' ��.) � ���� ���������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        with dmO do
        begin
          ViewMenuCr.BeginUpdate;
          for i:=0 to fmGoods.ViewGoods.Controller.SelectedRecordCount-1 do
          begin
            Rec:=fmGoods.ViewGoods.Controller.SelectedRecords[i];

            for j:=0 to Rec.ValueCount-1 do
            begin
              if fmGoods.ViewGoods.Columns[j].Name='ViewGoodsID' then break;
            end;

            iNum:=Rec.Values[j];
          //��� ��� - ����������
            with dmO do
            begin
              if quCardsSel.Locate('ID',iNum,[]) then
              begin
                try
                  fmMenuCr.prGetId.ParamByName('ITYPE').Value:=8;  //��� ����� � ��
                  fmMenuCr.prGetId.ExecProc;
                  Id:=fmMenuCr.prGetId.ParamByName('RESULT').Value;

                  {
                  quMaxIdMe.Active:=False;
                  quMaxIdMe.Active:=True;
                  Id:=quMaxIdMeMAXID.AsInteger+1;
                  quMaxIdMe.Active:=False;
                  }

                  sBar:=IntToStr(iD);
                  while length(sBar)<10 do sBar:='0'+sBar;
                  sBar:='25'+sBar;
                  sBar:=ToStandart(sBar);

                  trUpdM.StartTransaction;
                  quMenusel.Append;
                  quMenuSelSIFR.AsInteger:=Id;
                  quMenuSelNAME.AsString:=Copy(quCardsSelNAME.AsString,1,100);
                  quMenuSelPRICE.AsFloat:=quCardsSelLASTPRICEOUT.AsFloat;
                 //��� ����
                  quMenuSelCODE.AsString:=IntToStr(iNum);
                  quMenuSelCONSUMMA.AsFloat:=1;
                  quMenuSelTREETYPE.AsString:='F';
                  quMenuSelLIMITPRICE.AsFloat:=0;

                  quMenuSelCATEG.AsInteger:=0;
                  quMenuSelPARENT.AsInteger:=Integer(MenuTree.Selected.Data);
                  quMenuSelLINK.AsInteger:=0;
                  quMenuSelSTREAM.AsInteger:=0;
                  quMenuSelLACK.AsInteger:=0;
                  quMenuSelDESIGNSIFR.AsInteger:=0;
                  quMenuSelALTNAME.AsString:=prFTCardVes(iNum);
                  quMenuSelNALOG.AsFloat:=0;
                  quMenuSelBARCODE.AsString:=sBar;
                  quMenuSelIMAGE.AsInteger:=0;
                  quMenuSelMINREST.AsFloat:=0;
                  quMenuSelPRNREST.AsInteger:=0;
                  quMenuSelCOOKTIME.AsInteger:=0;
                  quMenuSelDISPENSER.AsInteger:=0;
                  quMenuSelDISPKOEF.AsInteger:=0;
                  quMenuSelACCESS.AsInteger:=0;
                  quMenuSelFLAGS.AsInteger:=0;
                  quMenuSelTARA.AsInteger:=0;
                  quMenuSelCNTPRICE.AsInteger:=0;
                  quMenuSelBACKBGR.AsFloat:=0;
                  quMenuSelFONTBGR.AsFloat:=0;
                  quMenuSelIACTIVE.AsInteger:=1;
                  quMenuSelIEDIT.AsInteger:=0;
                  quMenuSel.Post;
                  trUpdM.Commit;

                  with dmO do
                  begin
                  // EXECUTE PROCEDURE PR_ADDKB (?IDDB, ?ITYPE, ?SIFR, ?CODEB, ?KB)
                    prAddKb.ParamByName('IDDB').AsInteger:=cxLookupComboBox1.EditValue;
                    prAddKb.ParamByName('ITYPE').AsInteger:=1;
                    prAddKb.ParamByName('SIFR').AsInteger:=Id;
                    prAddKb.ParamByName('CODEB').AsInteger:=iNum;
                    prAddKb.ParamByName('KB').AsFloat:=1;
                    prAddKb.ExecProc;
                  end;

                except
                end;
                quMenuSel.Refresh;
              end;
            end;
          end;
          ViewMenuCr.EndUpdate;
        end;
      end;
    end;
    if iCo=1 then //������ ���� ��������� �������
    begin
      if Sender is TcxGridSite then
      begin
        bAdd:=False;
        k:=1;
        AHitTest := TcxGridSite(Sender).ViewInfo.GetHitTest(X, Y);
        if AHitTest.HitTestCode=107 then //������
        begin
          Rec1:=TcxGridRecordHitTest(AHitTest).GridRecord;
          fmTransM:=tfmTransM.Create(Application);
          for j:=0 to Rec1.ValueCount-1 do
          begin
            if ViewMenuCr.Columns[j].Name='ViewMenuCrSIFR' then break;
          end;
          iNum:=Rec1.Values[j];
          if quMenuSel.Locate('SIFR',iNum,[]) then
          begin
            with fmTransM do
            begin
              cxTextEdit1.Text:=quMenuSelNAME.AsString;
              cxCalcEdit1.EditValue:=StrToINtDef(quMenuSelCODE.AsString,0);
              cxCalcEdit2.EditValue:=quMenuSelCONSUMMA.AsFloat;

              cxTextEdit2.Text:=dmO.quCardsSelNAME.AsString;
              cxCalcEdit3.EditValue:=dmO.quCardsSelID.AsInteger;
              cxCalcEdit4.EditValue:=1;
            end;
            fmTransM.ShowModal;
            if fmTransM.ModalResult=mrYes then
            begin //������
              trUpdM.StartTransaction;
              quMenusel.Edit;
              //��� ����
              quMenuSelNAME.AsString:=Copy(fmTransM.cxTextEdit2.Text,1,100);
              quMenuSelCODE.AsString:=INtToStr(Trunc(fmTransM.cxCalcEdit3.EditValue));
              quMenuSelCONSUMMA.AsFloat:=fmTransM.cxCalcEdit4.EditValue;
              quMenuSelALTNAME.AsString:=prFTCardVes(Trunc(fmTransM.cxCalcEdit3.EditValue));
              quMenuSel.Post;
              trUpdM.Commit;


              with dmO do
              begin
               // EXECUTE PROCEDURE PR_ADDKB (?IDDB, ?ITYPE, ?SIFR, ?CODEB, ?KB)
                prAddKb.ParamByName('IDDB').AsInteger:=cxLookupComboBox1.EditValue;
                prAddKb.ParamByName('ITYPE').AsInteger:=1;
                prAddKb.ParamByName('SIFR').AsInteger:=quMenuSelSifr.AsInteger;
                prAddKb.ParamByName('CODEB').AsInteger:=Trunc(fmTransM.cxCalcEdit3.EditValue);
                prAddKb.ParamByName('KB').AsFloat:=fmTransM.cxCalcEdit4.EditValue;
                prAddKb.ExecProc;
              end;

              quMenuSel.Refresh;
            end;
            if fmTransM.ModalResult=mrOk then //����������
            begin
              bAdd:=True;
              k:=fmTransM.cxCalcEdit4.EditValue;
            end;
          end else bAdd:=True;
          fmTransM.Release;
        end;
        if AHitTest.HitTestCode=0 then  bAdd:=True; //���� ����������
        if bAdd then   //����������
        begin
          quMaxIdMe.Active:=False;
          quMaxIdMe.Active:=True;
          Id:=quMaxIdMeMAXID.AsInteger+1;
          quMaxIdMe.Active:=False;

          sBar:=IntToStr(iD);
          while length(sBar)<10 do sBar:='0'+sBar;
          sBar:='25'+sBar;
          sBar:=ToStandart(sBar);

          trUpdM.StartTransaction;
          quMenusel.Append;
          quMenuSelSIFR.AsInteger:=Id;
          quMenuSelNAME.AsString:=Copy(dmO.quCardsSelNAME.AsString,1,100);
          quMenuSelPRICE.AsFloat:=dmO.quCardsSelLASTPRICEOUT.AsFloat;
          //��� ����
          quMenuSelCODE.AsString:=IntToStr(dmO.quCardsSelID.AsInteger);
          quMenuSelCONSUMMA.AsFloat:=K;
          quMenuSelTREETYPE.AsString:='F';
          quMenuSelLIMITPRICE.AsFloat:=0;

          quMenuSelCATEG.AsInteger:=0;
          quMenuSelPARENT.AsInteger:=Integer(MenuTree.Selected.Data);
          quMenuSelLINK.AsInteger:=0;
          quMenuSelSTREAM.AsInteger:=0;
          quMenuSelLACK.AsInteger:=0;
          quMenuSelDESIGNSIFR.AsInteger:=0;
          quMenuSelALTNAME.AsString:=prFTCardVes(dmO.quCardsSelID.AsInteger);
          quMenuSelNALOG.AsFloat:=0;
          quMenuSelBARCODE.AsString:=sBar;
          quMenuSelIMAGE.AsInteger:=0;
          quMenuSelMINREST.AsFloat:=0;
          quMenuSelPRNREST.AsInteger:=0;
          quMenuSelCOOKTIME.AsInteger:=0;
          quMenuSelDISPENSER.AsInteger:=0;
          quMenuSelDISPKOEF.AsInteger:=0;
          quMenuSelACCESS.AsInteger:=0;
          quMenuSelFLAGS.AsInteger:=0;
          quMenuSelTARA.AsInteger:=0;
          quMenuSelCNTPRICE.AsInteger:=0;
          quMenuSelBACKBGR.AsFloat:=0;
          quMenuSelFONTBGR.AsFloat:=0;
          quMenuSelIACTIVE.AsInteger:=1;
          quMenuSelIEDIT.AsInteger:=0;
          quMenuSel.Post;
          trUpdM.Commit;

          with dmO do
          begin
               // EXECUTE PROCEDURE PR_ADDKB (?IDDB, ?ITYPE, ?SIFR, ?CODEB, ?KB)
            prAddKb.ParamByName('IDDB').AsInteger:=cxLookupComboBox1.EditValue;
            prAddKb.ParamByName('ITYPE').AsInteger:=1;
            prAddKb.ParamByName('SIFR').AsInteger:=Id;
            prAddKb.ParamByName('CODEB').AsInteger:=dmO.quCardsSelID.AsInteger;
            prAddKb.ParamByName('KB').AsFloat:=K;
            prAddKb.ExecProc;
          end;

          quMenuSel.Refresh;
        end;
      end;
    end;
  end;
end;

procedure TfmMenuCr.cxButton1Click(Sender: TObject);
Var i:INteger;
begin
  if cxTextEdit1.Text>'' then
  begin
    quFindB.Active:=False;
    quFindB.SelectSQL.Clear;
    quFindB.SelectSQL.Add('SELECT SIFR,NAME,PRICE,CODE,PARENT,CONSUMMA');
    quFindB.SelectSQL.Add('FROM MENU ');
    quFindB.SelectSQL.Add('where UPPER(NAME)like ''%'+AnsiUpperCase(cxTextEdit1.Text)+'%''');
    quFindB.Active:=True;

    fmFind.LevelFCli.Visible:=False;
    fmFind.LevelFind.Visible:=False;
    fmFind.LevelFMenu.Visible:=True;

    fmFind.ShowModal;
    if fmFind.ModalResult=mrOk then
    begin
      if quFindB.RecordCount>0 then
      begin
        bMenuList:=False;
        ViewMenuCr.BeginUpdate;

        for i:=0 to MenuTree.Items.Count-1 Do
        if Integer(MenuTree.Items[i].Data) = quFindBPARENT.AsInteger Then
        begin
          MenuTree.Items[i].Expand(False);
          MenuTree.Repaint;
          MenuTree.Items[i].Selected:=True;
          Break;
        End;
        delay(10);

        quMenuSel.Active:=False;
        quMenuSel.ParamByName('PARENTID').AsInteger:=quFindBPARENT.AsInteger;
        quMenuSel.Active:=True;

        quMenuSel.First;
        quMenuSel.locate('SIFR',quFindBSIFR.AsInteger,[]);

        ViewMenuCr.EndUpdate;
        bMenuList:=True;

        prRowFocus(ViewMenuCr);

      end;
    end;

    fmFind.LevelFind.Visible:=True;
    fmFind.LevelFMenu.Visible:=False;

    cxTextEdit1.Text:='';
    delay(10);
    GridMenuCr.SetFocus;
  end;
end;

procedure TfmMenuCr.cxButton2Click(Sender: TObject);
Var i:INteger;
    iCode:INteger;
begin
  iCode:=StrToIntDef(cxTextEdit1.Text,0);
  if iCode>0 then
  begin
    with dmO do
    begin
      quFindB.Active:=False;
      quFindB.SelectSQL.Clear;
      quFindB.SelectSQL.Add('SELECT SIFR,NAME,PRICE,CODE,PARENT,CONSUMMA');
      quFindB.SelectSQL.Add('FROM MENU ');
      quFindB.SelectSQL.Add('where SIFR = '+IntToStr(iCode));
      quFindB.Active:=True;

      fmFind.LevelFCli.Visible:=False;
      fmFind.LevelFind.Visible:=False;
      fmFind.LevelFMenu.Visible:=True;

      fmFind.ShowModal;
      if fmFind.ModalResult=mrOk then
      begin
        if quFindB.RecordCount>0 then
        begin
          bMenuList:=False;
          ViewMenuCr.BeginUpdate;

          for i:=0 to MenuTree.Items.Count-1 Do
          if Integer(MenuTree.Items[i].Data) = quFindBPARENT.AsInteger Then
          begin
            MenuTree.Items[i].Expand(False);
            MenuTree.Repaint;
            MenuTree.Items[i].Selected:=True;
            Break;
          End;
          delay(10);

          quMenuSel.Active:=False;
          quMenuSel.ParamByName('PARENTID').AsInteger:=quFindBPARENT.AsInteger;
          quMenuSel.Active:=True;

          quMenuSel.First;
          quMenuSel.locate('SIFR',quFindBSIFR.AsInteger,[]);

          ViewMenuCr.EndUpdate;
          bMenuList:=True;

          prRowFocus(ViewMenuCr);

        end;
      end;

      fmFind.LevelFind.Visible:=True;
      fmFind.LevelFMenu.Visible:=False;

      cxTextEdit1.Text:='';
      delay(10);
      GridMenuCr.SetFocus;
    end;
  end;
end;

procedure TfmMenuCr.cxLookupComboBox1PropertiesChange(Sender: TObject);
begin
//
  if bCreate then exit;
  StatusBar1.Panels[0].Text:='�����, ���� �������� ���� ...'; delay(10);
  cxButton3.Caption:='�� ������������';
  cxButton3.Tag:=0;
  fmMenuCr.Caption:='���� (�����)';

  // ������� �� ������ �������� ����
  cxButton4.Tag:=1;
  cxButton4.Caption:='�� ��� ������� ����';
  Panel3.Visible:=True;
  Panel4.Visible:=False;

  LevelMenuCr.Visible:=True;
  LevelMods.Visible:=False;

  ViewMenuCr.BeginUpdate;

  if dmORep.quDB.Locate('ID',cxLookupComboBox1.EditValue,[]) then
    DBName:=dmORep.quDBPATHDB.AsString;

  CasherRnDb.Connected:=False;
  CasherRnDb.DBName:=DBName;
  try
    CasherRnDb.Open;

    bMenuList:=False;

    MenuTree.Visible:=False;
    MenuTree.Items.Clear;
    CardsExpandLevel(nil,MenuTree,quMenuTree);
    MenuTree.FullExpand;
    MenuTree.FullCollapse;
    MenuTree.Visible:=True;

    quMenuTree.First;

    quMenuSel.Active:=False;
    quMenuSel.ParamByName('PARENTID').AsInteger:=quMenuTree.fieldbyName('Id').AsInteger;
    quMenuSel.Active:=True;

    bMenuList:=True;

  except
    CasherRnDb.Connected:=False;
  end;

  ViewMenuCr.EndUpdate;
  StatusBar1.Panels[0].Text:='��.'; delay(10);
end;

procedure TfmMenuCr.cxButton3Click(Sender: TObject);
begin
  if not CasherRnDb.Connected then exit;
  StatusBar1.Panels[0].Text:='����� ���� ������������ ������ ...'; delay(10);
  if cxButton3.Tag=0 then
  begin
    //������������ �� ������������
    cxButton3.Caption:='�� �����';
    cxButton3.Tag:=1;
    fmMenuCr.Caption:='���� (������������)';

    LevelMenuCr.Visible:=False;
    LevelMods.Visible:=True;

    ViewMods.BeginUpdate;
    try
      bMenuList:=False;
      MenuTree.Items.Clear;

      RefreshModTree;
      if quModGrMo.RecordCount>0 then  RefreshMods;

      bMenuList:=True;
    finally
      ViewMods.EndUpdate;
    end;

  end else
  begin
    //������������ ������� �� �����
    cxButton3.Caption:='�� ������������';
    cxButton3.Tag:=0;
    fmMenuCr.Caption:='���� (�����)';

    LevelMenuCr.Visible:=True;
    LevelMods.Visible:=False;

    ViewMenuCr.BeginUpdate;
    try
      bMenuList:=False;

      MenuTree.Items.Clear;
      CardsExpandLevel(nil,MenuTree,quMenuTree);
      MenuTree.Visible:=False;
      MenuTree.FullExpand;
      MenuTree.FullCollapse;
      MenuTree.Visible:=True;

      quMenuTree.First;

      quMenuSel.Active:=False;
      quMenuSel.ParamByName('PARENTID').AsInteger:=quMenuTree.fieldbyName('Id').AsInteger;
      quMenuSel.Active:=True;

      bMenuList:=True;
    finally
      ViewMenuCr.EndUpdate;
    end;
  end;
  StatusBar1.Panels[0].Text:='��.'; delay(10);
end;

procedure TfmMenuCr.ViewModsDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept:=False;
  if bDM then  Accept:=True;
end;

procedure TfmMenuCr.ViewModsDragDrop(Sender, Source: TObject; X,
  Y: Integer);
Var iCo:Integer;
    i,j: Integer;
    iNum: Integer;
    Rec,Rec1:TcxCustomGridRecord;
    iD:INteger;
    sBar:String;
    AHitTest: TcxCustomGridHitTest;
    bAdd:Boolean;
    K:Real;
begin
  if bDM then
  begin
    ResetAddVars;
    iCo:=fmGoods.ViewGoods.Controller.SelectedRecordCount;
    if iCo>1 then
    begin
      if MessageDlg('�� ������������� ������ �������� ��������� ������� ('+IntToStr(iCo)+' ��.) � ���� (������������) ���������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        with dmO do
        begin
          ViewMods.BeginUpdate;
          for i:=0 to fmGoods.ViewGoods.Controller.SelectedRecordCount-1 do
          begin
            Rec:=fmGoods.ViewGoods.Controller.SelectedRecords[i];

            for j:=0 to Rec.ValueCount-1 do
            begin
              if fmGoods.ViewGoods.Columns[j].Name='ViewGoodsID' then break;
            end;

            iNum:=Rec.Values[j];
          //��� ��� - ����������
            with dmO do
            begin
              if quCardsSel.Locate('ID',iNum,[]) then
              begin
                try
                  quMaxIdMod.Active:=False;
                  quMaxIdMod.Active:=True;
                  Id:=quMaxIdModMAXID.AsInteger+1;
                  quMaxIdMod.Active:=False;

                  sBar:=IntToStr(iD);
                  while length(sBar)<10 do sBar:='0'+sBar;
                  sBar:='25'+sBar;
                  sBar:=ToStandart(sBar);
                                         

                  trUpdM.StartTransaction;
                  quMods.Append;
                  quModsSIFR.AsInteger:=Id;
                  quModsNAME.AsString:=Copy(quCardsSelNAME.AsString,1,100);
                  quModsPRICE.AsFloat:=0;
                  quModsREALPRICE.AsFloat:=quCardsSelLASTPRICEOUT.AsFloat;
                 //��� ����
                  quModsCODEB.AsInteger:=iNum;
                  quModsKB.AsFloat:=1;
                  quModsPARENT.AsInteger:=Integer(MenuTree.Selected.Data);
                  quModsIACTIVE.AsInteger:=1;
                  quModsIEDIT.AsInteger:=0;
                  quMods.Post;
                  trUpdM.Commit;


                  with dmO do
                  begin
                    // EXECUTE PROCEDURE PR_ADDKB (?IDDB, ?ITYPE, ?SIFR, ?CODEB, ?KB)
                    prAddKb.ParamByName('IDDB').AsInteger:=cxLookupComboBox1.EditValue;
                    prAddKb.ParamByName('ITYPE').AsInteger:=2;
                    prAddKb.ParamByName('SIFR').AsInteger:=Id;
                    prAddKb.ParamByName('CODEB').AsInteger:=iNum;
                    prAddKb.ParamByName('KB').AsFloat:=1;
                    prAddKb.ExecProc;
                  end;

                except
                end;
                quMods.Refresh;
              end;
            end;
          end;
          ViewMods.EndUpdate;
        end;
      end;
    end;
    if iCo=1 then //������ ���� ��������� �������
    begin
      if Sender is TcxGridSite then
      begin
        bAdd:=False;
        k:=1;
        AHitTest := TcxGridSite(Sender).ViewInfo.GetHitTest(X, Y);
        if AHitTest.HitTestCode=107 then //������
        begin
          Rec1:=TcxGridRecordHitTest(AHitTest).GridRecord;
          fmTransM:=tfmTransM.Create(Application);
          for j:=0 to Rec1.ValueCount-1 do
          begin
            if ViewMods.Columns[j].Name='ViewModsSIFR' then break;
          end;
          iNum:=Rec1.Values[j];
          if quMods.Locate('SIFR',iNum,[]) then
          begin
            with fmTransM do
            begin
              cxTextEdit1.Text:=quModsNAME.AsString;
              cxCalcEdit1.EditValue:=StrToINtDef(quModsCODEB.AsString,0);
              cxCalcEdit2.EditValue:=quModsKB.AsFloat;

              cxTextEdit2.Text:=dmO.quCardsSelNAME.AsString;
              cxCalcEdit3.EditValue:=dmO.quCardsSelID.AsInteger;
              cxCalcEdit4.EditValue:=1;
            end;
            fmTransM.ShowModal;
            if fmTransM.ModalResult=mrYes then
            begin //������
              trUpdM.StartTransaction;
              quMods.Edit;
              //��� ����
              quModsNAME.AsString:=Copy(fmTransM.cxTextEdit2.Text,1,100);
              quModsCODEB.AsInteger:=Trunc(fmTransM.cxCalcEdit3.EditValue);
              quModsKB.AsFloat:=fmTransM.cxCalcEdit4.EditValue;
              quMods.Post;
              trUpdM.Commit;
              quMods.Refresh;

              with dmO do
              begin
                // EXECUTE PROCEDURE PR_ADDKB (?IDDB, ?ITYPE, ?SIFR, ?CODEB, ?KB)
                prAddKb.ParamByName('IDDB').AsInteger:=cxLookupComboBox1.EditValue;
                prAddKb.ParamByName('ITYPE').AsInteger:=2;
                prAddKb.ParamByName('SIFR').AsInteger:=quModsSIFR.AsInteger;
                prAddKb.ParamByName('CODEB').AsInteger:=Trunc(fmTransM.cxCalcEdit3.EditValue);
                prAddKb.ParamByName('KB').AsFloat:=fmTransM.cxCalcEdit4.EditValue;
                prAddKb.ExecProc;
              end;

            end;
            if fmTransM.ModalResult=mrOk then //����������
            begin
              bAdd:=True;
              k:=fmTransM.cxCalcEdit4.EditValue;
            end;
          end else bAdd:=True;
          fmTransM.Release;
        end;
        if AHitTest.HitTestCode=0 then  bAdd:=True; //���� ����������
        if bAdd then   //����������
        begin
          quMaxIdMod.Active:=False;
          quMaxIdMod.Active:=True;
          Id:=quMaxIdModMAXID.AsInteger+1;
          quMaxIdMod.Active:=False;

          sBar:=IntToStr(iD);
          while length(sBar)<10 do sBar:='0'+sBar;
          sBar:='25'+sBar;
          sBar:=ToStandart(sBar);

          trUpdM.StartTransaction;
          quMods.Append;
          quModsSIFR.AsInteger:=Id;
          quModsNAME.AsString:=Copy(dmO.quCardsSelNAME.AsString,1,100);
          quModsPRICE.AsFloat:=0;
          quModsREALPRICE.AsFloat:=dmO.quCardsSelLASTPRICEOUT.AsFloat;
          quModsCODEB.AsInteger:=dmO.quCardsSelID.AsInteger;
          quModsKB.AsFloat:=K;
          quModsPARENT.AsInteger:=Integer(MenuTree.Selected.Data);
          quModsIACTIVE.AsInteger:=1;
          quModsIEDIT.AsInteger:=0;
          quMods.Post;

          trUpdM.Commit;
          quMods.Refresh;

          with dmO do
          begin
                    // EXECUTE PROCEDURE PR_ADDKB (?IDDB, ?ITYPE, ?SIFR, ?CODEB, ?KB)
            prAddKb.ParamByName('IDDB').AsInteger:=cxLookupComboBox1.EditValue;
            prAddKb.ParamByName('ITYPE').AsInteger:=2;
            prAddKb.ParamByName('SIFR').AsInteger:=Id;
            prAddKb.ParamByName('CODEB').AsInteger:=dmO.quCardsSelID.AsInteger;
            prAddKb.ParamByName('KB').AsFloat:=K;
            prAddKb.ExecProc;
          end;

        end;
      end;
    end;
  end;
end;

procedure TfmMenuCr.ViewModsStartDrag(Sender: TObject;
  var DragObject: TDragObject);
begin
  if not CanDo('prTransMenu') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  bDM:=True;
end;

procedure TfmMenuCr.acEditMExecute(Sender: TObject);
begin
// ����������� ������� ����
  if Panel3.Visible then
  begin
  if LevelMenuCr.Visible then
  begin
    if quMenuSel.RecordCount>0 then
    begin
      try
        fmEditPosM:=TfmEditPosM.Create(Application);
        with fmEditPosM do
        begin
          quStream.Active:=False;
          quStream.Active:=True;
          cxTextEdit1.Text:=quMenuSelNAME.AsString;
          cxCurrencyEdit1.Value:=quMenuSelPRICE.AsFloat;
          cxLookupComboBox1.EditValue:=quMenuSelSTREAM.AsInteger;
          cxCalcEdit1.Value:=StrToIntDef(quMenuSelCODE.AsString,0);
          cxCalcEdit2.Value:=quMenuSelCONSUMMA.AsFloat;
          cxTextEdit3.Text:=quMenuSelALTNAME.AsString;
        end;
        fmEditPosM.ShowModal;
        if fmEditPosM.ModalResult=mrOk then
        begin
          trUpdM.StartTransaction;
          quMenuSel.Edit;
          quMenuSelNAME.AsString:=fmEditPosM.cxTextEdit1.Text;
          quMenuSelPRICE.AsFloat:=fmEditPosM.cxCurrencyEdit1.Value;
          quMenuSelSTREAM.AsInteger:=fmEditPosM.cxLookupComboBox1.EditValue;
          quMenuSelCODE.AsString:=IntToStr(Trunc(fmEditPosM.cxCalcEdit1.Value));
          quMenuSelCONSUMMA.AsFloat:=fmEditPosM.cxCalcEdit2.Value;
          quMenuSelALTNAME.AsString:=fmEditPosM.cxTextEdit3.Text;
          quMenuSel.Post;
          trUpdM.Commit;

          with dmO do
          begin
               // EXECUTE PROCEDURE PR_ADDKB (?IDDB, ?ITYPE, ?SIFR, ?CODEB, ?KB)
            prAddKb.ParamByName('IDDB').AsInteger:=cxLookupComboBox1.EditValue;
            prAddKb.ParamByName('ITYPE').AsInteger:=1;
            prAddKb.ParamByName('SIFR').AsInteger:=quMenuSelSifr.AsInteger;
            prAddKb.ParamByName('CODEB').AsInteger:=Trunc(fmEditPosM.cxCalcEdit1.Value);
            prAddKb.ParamByName('KB').AsFloat:=fmEditPosM.cxCalcEdit2.Value;
            prAddKb.ExecProc;
          end;

          quMenuSel.Refresh;
        end;

      finally
        quStream.Active:=False;
        fmEditPosM.Release;
      end;
    end;
  end;
  if LevelMods.Visible then
  begin
    if quMods.RecordCount>0 then
    begin
      try
        fmEditPosM:=TfmEditPosM.Create(Application);
        with fmEditPosM do
        begin
          quStream.Active:=False;
          quStream.Active:=True;
          cxTextEdit1.Text:=quModsNAME.AsString;
          cxCurrencyEdit1.Value:=0;
          cxLookupComboBox1.EditValue:=0;
          cxCalcEdit1.Value:=StrToIntDef(quModsCODEB.AsString,0);
          cxCalcEdit2.Value:=quModsKB.AsFloat;
        end;
        fmEditPosM.ShowModal;
        if fmEditPosM.ModalResult=mrOk then
        begin
          trUpdM.StartTransaction;
          quMods.Edit;
          quModsNAME.AsString:=fmEditPosM.cxTextEdit1.Text;
          quModsCODEB.AsString:=IntToStr(Trunc(fmEditPosM.cxCalcEdit1.Value));
          quModsKB.AsFloat:=fmEditPosM.cxCalcEdit2.Value;
          quMods.Post;
          trUpdM.Commit;

          quMods.Refresh;

          with dmO do
          begin
            // EXECUTE PROCEDURE PR_ADDKB (?IDDB, ?ITYPE, ?SIFR, ?CODEB, ?KB)
            prAddKb.ParamByName('IDDB').AsInteger:=cxLookupComboBox1.EditValue;
            prAddKb.ParamByName('ITYPE').AsInteger:=2;
            prAddKb.ParamByName('SIFR').AsInteger:=quModsSIFR.AsInteger;
            prAddKb.ParamByName('CODEB').AsInteger:=Trunc(fmEditPosM.cxCalcEdit1.Value);
            prAddKb.ParamByName('KB').AsFloat:=fmEditPosM.cxCalcEdit2.Value;
            prAddKb.ExecProc;
          end;

        end;

      finally
        quStream.Active:=False;
        fmEditPosM.Release;
      end;
    end;
  end;
  end;

  if Panel4.Visible then   //�� �������� ����
  begin
    if quMDS.RecordCount>0 then
    begin
      try
        fmEditPosM:=TfmEditPosM.Create(Application);
        with fmEditPosM do
        begin
          quStream.Active:=False;
          quStream.Active:=True;
          cxTextEdit1.Text:=quMDSNAME.AsString;
          cxCurrencyEdit1.Value:=quMDSPRICE.AsFloat;
          cxLookupComboBox1.EditValue:=quMDSSTREAM.AsInteger;
          cxCalcEdit1.Value:=StrToIntDef(quMDSCODE.AsString,0);
          cxCalcEdit2.Value:=quMDSCONSUMMA.AsFloat;
          cxTextEdit3.Text:=quMDSALTNAME.AsString;
        end;
        fmEditPosM.ShowModal;
        if fmEditPosM.ModalResult=mrOk then
        begin
          quMenuRec.Active:=False;
          quMenuRec.ParamByName('ICODE').AsInteger:=quMDSSIFR.AsINteger;
          quMenuRec.Active:=True;
          if quMenuRec.RecordCount>0 then
          begin
            quMenuRec.First;

            quMenuRec.Edit;
            quMenuRecNAME.AsString:=fmEditPosM.cxTextEdit1.Text;
            quMenuRecPRICE.AsFloat:=fmEditPosM.cxCurrencyEdit1.Value;
            quMenuRecSTREAM.AsInteger:=fmEditPosM.cxLookupComboBox1.EditValue;
            quMenuRecCODE.AsString:=IntToStr(Trunc(fmEditPosM.cxCalcEdit1.Value));
            quMenuRecCONSUMMA.AsFloat:=fmEditPosM.cxCalcEdit2.Value;
            quMenuRecALTNAME.AsString:=fmEditPosM.cxTextEdit3.Text;
            quMenuRec.Post;

            with dmO do
            begin
               // EXECUTE PROCEDURE PR_ADDKB (?IDDB, ?ITYPE, ?SIFR, ?CODEB, ?KB)
              prAddKb.ParamByName('IDDB').AsInteger:=cxLookupComboBox1.EditValue; //����
              prAddKb.ParamByName('ITYPE').AsInteger:=1;
              prAddKb.ParamByName('SIFR').AsInteger:=quMDSSIFR.AsINteger;
              prAddKb.ParamByName('CODEB').AsInteger:=Trunc(fmEditPosM.cxCalcEdit1.Value);
              prAddKb.ParamByName('KB').AsFloat:=fmEditPosM.cxCalcEdit2.Value;
              prAddKb.ExecProc;
            end;

            if quMDSPRICE.AsFloat<>fmEditPosM.cxCurrencyEdit1.Value then
            begin
              quMDS.Edit;
              quMDSPRICE.AsFloat:=rv(fmEditPosM.cxCurrencyEdit1.Value);
              quMDS.Post;
            end;

            quMDS.Refresh;

          end;
          quMenuRec.Active:=False;

        end;
      finally
        quStream.Active:=False;
        fmEditPosM.Release;
      end;
    end;
  end;  
end;

procedure TfmMenuCr.cxButton4Click(Sender: TObject);
begin
  if cxButton4.Tag=1 then
  begin
    cxButton4.Tag:=2;
    cxButton4.Caption:='�� ��� ����';
    Panel3.Visible:=False;
    Panel4.Visible:=True;

    fmMenuCr.Caption:='������� ���� �� ������ � '+ds1(CommonSet.DateFrom)+' �� '+ds1(CommonSet.DateTo);

    ViewDM.BeginUpdate;
    quMenuDayList.Active:=False;
    quMenuDayList.ParamByName('DATEB').AsDateTime:=CommonSet.DateFrom;
    quMenuDayList.ParamByName('DATEE').AsDateTime:=CommonSet.DateTo;
    quMenuDayList.Active:=True;
    ViewDM.EndUpdate;

    if quMenuDayList.RecordCount>0 then
    begin
      bRefresh:=False;
      ViewMenuDay.BeginUpdate;
      MenuTree.Tag:=quMenuDayListID.AsInteger;
      GridMenuDay.Tag:=Trunc(quMenuDayListMDATE.AsDateTime);
      MenuExpandLevel(nil,TreeMD,quMenuDay,quMenuDayListID.AsInteger);

      TreeMD.FullExpand;
      TreeMD.FullCollapse;

      ViewMenuDay.EndUpdate;
      bRefresh:=True;
    end;

  end else
  begin
    cxButton4.Tag:=1;
    cxButton4.Caption:='�� ��� ������� ����';
    Panel3.Visible:=True;
    Panel4.Visible:=False;

    fmMenuCr.Caption:='Me�� ';
  end;
end;

procedure TfmMenuCr.acPeriodDMExecute(Sender: TObject);
begin
  //������
  fmPeriodUni.DateTimePicker1.Date:=CommonSet.DateFrom;
  fmPeriodUni.ShowModal;
  if fmPeriodUni.ModalResult=mrOk then
  begin
    CommonSet.DateFrom:=Trunc(fmPeriodUni.DateTimePicker1.Date);
    CommonSet.DateTo:=Trunc(fmPeriodUni.DateTimePicker2.Date)+1;

    fmMenuCr.Caption:='������� ���� �� ������ � '+ds1(CommonSet.DateFrom)+' �� '+ds1(CommonSet.DateTo);

    ViewDM.BeginUpdate;
    quMenuDayList.Active:=False;
    quMenuDayList.ParamByName('DATEB').AsDateTime:=CommonSet.DateFrom;
    quMenuDayList.ParamByName('DATEE').AsDateTime:=CommonSet.DateTo;
    quMenuDayList.Active:=True;
    ViewDM.EndUpdate;
  end;
end;

procedure TfmMenuCr.acAddMDExecute(Sender: TObject);
Var IDH,IdhGr:INteger;
begin
  //�������� ������� ����
 // if not CanDo('prAddMDL') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;

  IdHGr:=0;
  if quMenuDayList.RecordCount>0 then  IdHGr:=quMenuDayListID.AsInteger;

  fmAddMDL:=tfmAddMDL.Create(Application);
  fmAddMDL.cxTextEdit1.Text:='';
  fmAddMDL.cxDateEdit1.Date:=Date+1;
  fmAddMDL.ShowModal;
  if fmAddMDL.ModalResult=mrOk then
  begin
    prGetId.ParamByName('ITYPE').Value:=12;
    prGetId.ExecProc;
    IDH:=prGetId.ParamByName('RESULT').Value;

    quMenuDayList.Append;
    quMenuDayListID.AsInteger:=IDH;
    quMenuDayListNAME.AsString:=fmAddMDL.cxTextEdit1.Text;
    quMenuDayListMDATE.AsDateTime:=Trunc(fmAddMDL.cxDateEdit1.Date);
    quMenuDayListIACTIVE.AsInteger:=0;
    quMenuDayList.Post;

    //�������� ����� ������
    if IdHGr>0 then
    begin
      quMDGroups.Active:=False;
      quMDGroups.ParamByName('IDH').AsInteger:=IdHGr;
      quMDGroups.Active:=True;

      quMDGID.Active:=False;
      quMDGID.ParamByName('IDH').AsInteger:=IDH;
      quMDGID.Active:=True;

      while not quMDGID.Eof do quMDGID.Delete;   //���� ��� ������ ��� ������ �� ������ ������

      quMDGroups.First;
      while not quMDGroups.Eof do
      begin
        quMDGID.Append;
        quMDGIDIDH.AsInteger:=IDH;
        quMDGIDID.AsInteger:=quMDGroupsID.AsInteger;
        quMDGIDNAMEGR.AsString:=quMDGroupsNAMEGR.AsString;
        quMDGIDPRIOR.AsInteger:=quMDGroupsPRIOR.AsInteger;
        quMDGIDPARENT.AsInteger:=quMDGroupsPARENT.AsInteger;
        quMDGID.Post;

        quMDGroups.Next;
      end;

      quMDGID.Active:=False;
      quMDGroups.Active:=False;
    end;

    quMenuDayList.Refresh;
  end;
  fmAddMDL.Release;
end;

procedure TfmMenuCr.acEditMDExecute(Sender: TObject);
begin
  //������������� ������� ����
  if quMenuDayList.RecordCount>0 then
  begin
    fmAddMDL:=tfmAddMDL.Create(Application);
    fmAddMDL.cxTextEdit1.Text:=quMenuDayListNAME.AsString;
    fmAddMDL.cxDateEdit1.Date:=quMenuDayListMDATE.AsDateTime;
    fmAddMDL.ShowModal;
    if fmAddMDL.ModalResult=mrOk then
    begin
      quMenuDayList.Edit;
      quMenuDayListNAME.AsString:=fmAddMDL.cxTextEdit1.Text;
      quMenuDayListMDATE.AsDateTime:=Trunc(fmAddMDL.cxDateEdit1.Date);
      quMenuDayList.Post;
    end;
    fmAddMDL.Release;
    quMenuDayList.Refresh;
  end;
end;

procedure TfmMenuCr.acDelMDExecute(Sender: TObject);
begin
  //�������� �������� ����
  if quMenuDayList.RecordCount>0 then
  begin
    if MessageDlg('������� ������� ���� '+quMenuDayListNAME.AsString+' ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      quMDGID.Active:=False;
      quMDGID.ParamByName('IDH').AsInteger:=quMenuDayListID.AsInteger;
      quMDGID.Active:=True;
      while not quMDGID.Eof do quMDGID.Delete;   //���� ��� ������ ��� ������ �� ������ ������
      quMDGID.Active:=False;

      quMenuDayList.Delete;
    end;
  end;
end;

procedure TfmMenuCr.ViewDMFocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord;
  ANewItemRecordFocusingChanged: Boolean);
begin
  if quMenuDayList.RecordCount>0 then
  begin
    bRefresh:=False;

    TreeMD.Update;

    ViewMenuDay.BeginUpdate;
    MenuTree.Tag:=quMenuDayListID.AsInteger;
    GridMenuDay.Tag:=Trunc(quMenuDayListMDATE.AsDateTime);
    MenuExpandLevel(nil,TreeMD,quMenuDay,quMenuDayListID.AsInteger);

    TreeMD.FullExpand;
    TreeMD.FullCollapse;

    if (TreeMD.Selected<>nil) then
    begin
      //��� ����� ���������� ������ �������� - ����� ������� ������� �� ����
      ViewMenuDay.BeginUpdate;
      quMDS.Active:=False;
      quMDS.ParamByName('IDH').AsInteger:=quMenuDayListID.AsINteger;
      quMDS.ParamByName('ID').AsInteger:=Integer(TreeMD.Selected.Data);
      quMDS.Active:=True;
      ViewMenuDay.EndUpdate;
    end else
    begin
      ViewMenuDay.BeginUpdate;
      quMDS.Active:=False;
      quMDS.ParamByName('IDH').AsInteger:=quMenuDayListID.AsINteger;
      quMDS.ParamByName('ID').AsInteger:=0;
      quMDS.Active:=True;
      ViewMenuDay.EndUpdate;
    end;


    ViewMenuDay.EndUpdate;

    bRefresh:=True;
  end;
end;

procedure TfmMenuCr.acAddMDGrExecute(Sender: TObject);
Var Id,i:Integer;
begin
  if quMenuDayList.RecordCount>0 then
  begin
    fmMDG:=tfmMDG.Create(Application);
    fmMDG.Caption:='������ �������� ���� - ����������.';
    fmMDG.cxTextEdit1.Text:='����� ������';
    fmMDG.cxSpinEdit1.Value:=1;
    fmMDG.ShowModal;
    if fmMDG.ModalResult=mrOk then
    begin
      quMaxMDG.Active:=False;
      quMaxMDG.ParamByName('IDH').AsInteger:=quMenuDayListID.AsINteger;
      quMaxMDG.Active:=True;

      Id:=1;
      if quMaxMDG.RecordCount>0 then
      begin
        Id:=quMaxMDGID.AsInteger+1;
      end;
      quMaxMDG.Active:=False;

      quMDGID.Active:=False;
      quMDGID.ParamByName('IDH').AsInteger:=quMenuDayListID.AsINteger;
      quMDGID.Active:=True;

      quMDGID.Append;
      quMDGIDIDH.AsInteger:=quMenuDayListID.AsINteger;
      quMDGIDID.AsInteger:=Id;
      quMDGIDNAMEGR.AsString:=fmMDG.cxTextEdit1.Text;
      quMDGIDPRIOR.AsInteger:=fmMDG.cxSpinEdit1.Value;
      quMDGIDPARENT.AsInteger:=0;
      quMDGID.Post;

      quMDGID.Active:=False;

      bRefresh:=False;

      MenuExpandLevel(nil,TreeMD,quMenuDay,quMenuDayListID.AsINteger);

      for i:=0 to TreeMD.Items.Count-1 do
      if Integer(TreeMD.Items.Item[i].Data) = Id then
      begin
        TreeMD.Items[i].Selected:=True;
        TreeMD.Repaint;
      end;

      bRefresh:=True;

      if (TreeMD.Selected<>nil) then
      begin
        //��� ����� ���������� ������ �������� - ����� ������� ������� �� ����
        ViewMenuDay.BeginUpdate;
        quMDS.Active:=False;
        quMDS.ParamByName('IDH').AsInteger:=quMenuDayListID.AsINteger;
        quMDS.ParamByName('ID').AsInteger:=Integer(TreeMD.Selected.Data);
        quMDS.Active:=True;
        ViewMenuDay.EndUpdate;
      end;

    end;

    fmMDG.Release;
  end;
end;

procedure TfmMenuCr.acAddMDSGrExecute(Sender: TObject);
Var Id,IdGr:Integer;
    TreeNode : TTreeNode;
begin
// �������� ���������
  if (TreeMD.Items.Count=0)  then
  begin
    showmessage('�������� ������.');
    exit;
  end;
  if (TreeMD.Selected=nil)  then
  begin
    showmessage('�������� ������,���������.');
    exit;
  end;

  if quMenuDayList.RecordCount>0 then
  begin
    IdGr:=Integer(TreeMD.Selected.data);

    fmMDG:=TfmMDG.create(Application);
    fmMDG.Caption:='���������� � ������ "'+TreeMD.Selected.Text+'" ����� ���������.';
    fmMDG.cxTextEdit1.Text:='����� ���������';
    fmMDG.cxSpinEdit1.Value:=1;
    fmMDG.ShowModal;
    if fmMDG.ModalResult=mrOk then
    begin
      quMaxMDG.Active:=False;
      quMaxMDG.ParamByName('IDH').AsInteger:=quMenuDayListID.AsINteger;
      quMaxMDG.Active:=True;

      Id:=1;
      if quMaxMDG.RecordCount>0 then
      begin
        Id:=quMaxMDGID.AsInteger+1;
      end;
      quMaxMDG.Active:=False;

      quMDGID.Active:=False;
      quMDGID.ParamByName('IDH').AsInteger:=quMenuDayListID.AsINteger;
      quMDGID.Active:=True;

      quMDGID.Append;
      quMDGIDIDH.AsInteger:=quMenuDayListID.AsINteger;
      quMDGIDID.AsInteger:=Id;
      quMDGIDNAMEGR.AsString:=fmMDG.cxTextEdit1.Text;
      quMDGIDPRIOR.AsInteger:=fmMDG.cxSpinEdit1.Value;
      quMDGIDPARENT.AsInteger:=IdGr;
      quMDGID.Post;

      quMDGID.Active:=False;

      bRefresh:=False;

      TreeNode:=TreeMD.Items.AddChildObject(TreeMD.Selected,fmMDG.cxTextEdit1.Text, Pointer(Id));
      TreeNode.ImageIndex:=8;
      TreeNode.SelectedIndex:=7;

      bRefresh:=True;

      if (TreeMD.Selected<>nil) then
      begin
        //��� ����� ���������� ������ �������� - ����� ������� ������� �� ����
        ViewMenuDay.BeginUpdate;
        quMDS.Active:=False;
        quMDS.ParamByName('IDH').AsInteger:=quMenuDayListID.AsINteger;
        quMDS.ParamByName('ID').AsInteger:=Integer(TreeMD.Selected.Data);
        quMDS.Active:=True;
        ViewMenuDay.EndUpdate;
      end;

    end;
    fmMDG.Release;
  end;
end;

procedure TfmMenuCr.acEditMDGrExecute(Sender: TObject);
Var Id,iPrior,i:Integer;
begin
  //�������������� ������
  if (TreeMD.Items.Count=0)  then
  begin
   // showmessage('�������� ������.');
    exit;
  end;
  if (TreeMD.Selected=nil)  then
  begin
    showmessage('�������� ������,���������.');
    exit;
  end;
  if quMenuDayList.RecordCount>0 then
  begin
    Id:=Integer(TreeMD.Selected.Data);

    quMDGID.Active:=False;
    quMDGID.ParamByName('IDH').AsInteger:=quMenuDayListID.AsINteger;
    quMDGID.Active:=True;
    if quMDGID.Locate('ID',Id,[]) then
    begin
      iPrior:=quMDGIDPRIOR.AsInteger;

      fmMDG:=TfmMDG.create(Application);
      fmMDG.Caption:='������ �������� ���� - ���������.';
      fmMDG.cxTextEdit1.Text:=quMDGIDNAMEGR.AsString;
      fmMDG.cxSpinEdit1.Value:=quMDGIDPRIOR.AsInteger;
      fmMDG.ShowModal;
      if fmMDG.ModalResult=mrOk then
      begin
        try
          quMDGID.Edit;
          quMDGIDNAMEGR.AsString:=fmMDG.cxTextEdit1.Text;
          quMDGIDPRIOR.AsInteger:=fmMDG.cxSpinEdit1.Value;
          quMDGID.Post;
        except
        end;

        if iPrior<>fmMDG.cxSpinEdit1.Value then
        begin
          bRefresh:=False;

          MenuExpandLevel(nil,TreeMD,quMenuDay,quMenuDayListID.AsINteger);

          for i:=0 to TreeMD.Items.Count-1 do
          if Integer(TreeMD.Items.Item[i].Data) = Id then
          begin
            TreeMD.Items[i].Selected:=True;
            TreeMD.Repaint;
          end;

          bRefresh:=True;

          if (TreeMD.Selected<>nil) then
          begin
            //��� ����� ���������� ������ �������� - ����� ������� ������� �� ����
            ViewMenuDay.BeginUpdate;
            quMDS.Active:=False;
            quMDS.ParamByName('IDH').AsInteger:=quMenuDayListID.AsINteger;
            quMDS.ParamByName('ID').AsInteger:=Integer(TreeMD.Selected.Data);
            quMDS.Active:=True;
            ViewMenuDay.EndUpdate;
          end;

        end else TreeMD.Selected.Text:=fmMDG.cxTextEdit1.Text;
      end;
    end;
    quMDGID.Active:=False;

    fmMDG.Release;
  end;
end;

procedure TfmMenuCr.acDelMDGrExecute(Sender: TObject);
begin
  //������� ������
  if TreeMD.Focused then
  begin
    if (TreeMD.Items.Count=0) then
    begin
      exit;
    end;
    if (TreeMD.Selected=nil) then
    begin
      showmessage('�������� ������,���������.');
      exit;
    end;

    if quMenuDayList.RecordCount>0 then
    begin
      if MessageDlg('�� ������������� ������ ������� ������ - "'+TreeMD.Selected.Text+'"?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        if quMDS.Active then
        begin
          if quMDS.RecordCount>0 then
          begin
            showmessage('������ �� �����, �������� ����������!!!');
            exit;
          end;  
        end;

        quMDFindParent.Active:=False;
        quMDFindParent.ParamByName('PARENTID').AsInteger:=Integer(TreeMD.Selected.Data);
        quMDFindParent.Active:=True;

        if quMDFindParentCOUNTREC.AsInteger>0 then
        begin
          showmessage('������ �� �����, �������� ����������!!!');
          quMDFindParent.Active:=False;
          exit;
        end;
        quMDFindParent.Active:=False;

        //���� ����� �� ���� �� �������� ��������

        //������ �� ����

        quMDGID.Active:=False;
        quMDGID.ParamByName('IDH').AsInteger:=quMenuDayListID.AsINteger;
        quMDGID.Active:=True;

        if quMDGID.Locate('ID',Integer(TreeMD.Selected.Data),[]) then
        begin
          quMDGID.Delete;
        end;

        //������ � ������
        TreeMD.Selected.Delete;

        if (TreeMD.Selected<>nil) then
        begin
          //��� ����� ���������� ������ �������� - ����� ������� ������� �� ����
          ViewMenuDay.BeginUpdate;
          quMDS.Active:=False;
          quMDS.ParamByName('IDH').AsInteger:=quMenuDayListID.AsINteger;
          quMDS.ParamByName('ID').AsInteger:=Integer(TreeMD.Selected.Data);
          quMDS.Active:=True;
          ViewMenuDay.EndUpdate;
        end;
      end;
    end;
  end;
end;

procedure TfmMenuCr.TreeMDChange(Sender: TObject; Node: TTreeNode);
begin
  if bRefresh then
  begin
    if quMenuDayList.RecordCount>0 then
    begin
      if (TreeMD.Selected<>nil) then
      begin
        ViewMenuDay.BeginUpdate;
        quMDS.Active:=False;
        quMDS.ParamByName('IDH').AsInteger:=quMenuDayListID.AsINteger;
        quMDS.ParamByName('ID').AsInteger:=Integer(TreeMD.Selected.Data);
        quMDS.Active:=True;
        ViewMenuDay.EndUpdate;
      end;  
    end;
  end;
end;

procedure TfmMenuCr.TreeMDExpanding(Sender: TObject; Node: TTreeNode;
  var AllowExpansion: Boolean);
begin
  if quMenuDayList.RecordCount>0 then
  begin
    if Node.getFirstChild.Data = nil then
    begin
      Node.DeleteChildren;
      MenuExpandLevel(Node,TreeMD,quMenuDay,quMenuDayListID.AsINteger);
    end;
  end;
end;

procedure TfmMenuCr.ViewMenuDayDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept:=False;
  if bDM then  Accept:=True;
end;

procedure TfmMenuCr.ViewMenuDayDragDrop(Sender, Source: TObject; X,
  Y: Integer);
Var iCo:Integer;
    i,j: Integer;
    iNum: Integer;
    Rec:TcxCustomGridRecord;
    iCodeM,Id:INteger;
    iMax:Integer;
    sBar:String;
    iGr:INteger;
begin
  if bDM then
  begin
    ResetAddVars;
    iCo:=fmGoods.ViewGoods.Controller.SelectedRecordCount;
    if (TreeMD.Selected=nil) then
    begin
      showmessage('�������� ������.');
      exit;
    end;

    if iCo>0 then
    begin
      if MessageDlg('�� ������������� ������ �������� ��������� ������� ('+IntToStr(iCo)+' ��.) � ���� ���������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        if quMenuDayList.RecordCount>0 then
        begin
          ViewMenuCr.BeginUpdate;
          for i:=0 to fmGoods.ViewGoods.Controller.SelectedRecordCount-1 do
          begin
            Rec:=fmGoods.ViewGoods.Controller.SelectedRecords[i];

            for j:=0 to Rec.ValueCount-1 do
            begin
              if fmGoods.ViewGoods.Columns[j].Name='ViewGoodsID' then break;
            end;

            iNum:=Rec.Values[j];
          //��� ��� - ����������
            with dmO do
            begin
              if quCardsSel.Locate('ID',iNum,[]) then
              begin
                iCodeM:=prFindCM(cxLookupComboBox1.EditValue,iNum);
                if iCodeM>0 then //������ � ����� �����
                begin
                  quFindB.Active:=False;
                  quFindB.SelectSQL.Clear;
                  quFindB.SelectSQL.Add('SELECT SIFR,NAME,PRICE,CODE,PARENT,CONSUMMA');
                  quFindB.SelectSQL.Add('FROM MENU ');
                  quFindB.SelectSQL.Add('where SIFR = '+IntToStr(iCodeM));
                  quFindB.Active:=True;
                  if quFindB.RecordCount>0 then
                  begin //� �������� ���� ����� - ����� � �������
                    if quMDS.Locate('SIFR',iCodeM,[])=False then
                    begin
                      iMax:=1;
                      if quMDS.RecordCount>0 then
                      begin
                        quMDS.Last;
                        iMax:=quMDSPRI.AsInteger+1;
                      end;

                      quMDS.Append;
                      quMDSIDH.AsInteger:=quMenuDayListID.AsINteger;
                      quMDSIDGR.AsInteger:=Integer(TreeMD.Selected.Data);
                      quMDSSIFR.AsInteger:=iCodeM;
                      quMDSPRI.AsInteger:=iMax;
                      quMDSPRICE.AsFloat:=quCardsSelLASTPRICEOUT.AsFloat;
                      quMDS.Post;

                      //������ � �������� � �������� ����

                      quMenuRec.Active:=False;
                      quMenuRec.ParamByName('ICODE').AsInteger:=iCodeM;
                      quMenuRec.Active:=True;
                      if quMenuRec.RecordCount>0 then
                      begin
                        quMenuRec.First;

                        quMenuRec.Edit;
                        quMenuRecNAME.AsString:=Copy(quCardsSelNAME.AsString,1,100);
                        quMenuRecALTNAME.AsString:=prFTCardVes(iNum);
                        quMenuRec.Post;
                      end;
                      quMenuRec.Active:=False;

                      quMDS.Refresh;
                    end;
                  end else
                  begin
                    //���� ����� �� ����� - ������ ���� �������

                    quDelCardM.ParamByName('IDC').AsInteger:=iCodeM;
                    quDelCardM.ParamByName('IDB').AsInteger:=cxLookupComboBox1.EditValue;
                    quDelCardM.ExecQuery;

                  end;
                  quFindB.Active:=False;
                end else
                begin

                try
                  quMaxIdMe.Active:=False;
                  quMaxIdMe.Active:=True;
                  Id:=quMaxIdMeMAXID.AsInteger+1;
                  quMaxIdMe.Active:=False;

                  sBar:=IntToStr(iD);
                  while length(sBar)<10 do sBar:='0'+sBar;
                  sBar:='25'+sBar;
                  sBar:=ToStandart(sBar);

                  //���� ����� ����  - ������ ������� ����
                  iGr:=prFindDMGR; //���� ��� ������ ������� ����

                  quMenuId.Active:=False;
                  quMenuId.ParamByName('SIFR').AsInteger:=Id;
                  quMenuId.Active:=True;

                  quMenuId.Append;
                  quMenuIdSIFR.AsInteger:=Id;
                  quMenuIdNAME.AsString:=Copy(quCardsSelNAME.AsString,1,100);
                  quMenuIdPRICE.AsFloat:=quCardsSelLASTPRICEOUT.AsFloat;

                  quMenuIdCODE.AsString:=IntToStr(iNum);
                  quMenuIdCONSUMMA.AsFloat:=1;
                  quMenuIdTREETYPE.AsString:='F';
                  quMenuIdLIMITPRICE.AsFloat:=0;

                  quMenuIdCATEG.AsInteger:=0;
                  quMenuIdPARENT.AsInteger:=iGr;
                  quMenuIdLINK.AsInteger:=0;
                  quMenuIdSTREAM.AsInteger:=0;
                  quMenuIdLACK.AsInteger:=0;
                  quMenuIdDESIGNSIFR.AsInteger:=0;
                  quMenuIdALTNAME.AsString:=prFTCardVes(iNum);
                  quMenuIdNALOG.AsFloat:=0;
                  quMenuIdBARCODE.AsString:=sBar;
                  quMenuIdIMAGE.AsInteger:=0;
                  quMenuIdMINREST.AsFloat:=0;
                  quMenuIdPRNREST.AsInteger:=0;
                  quMenuIdCOOKTIME.AsInteger:=0;
                  quMenuIdDISPENSER.AsInteger:=0;
                  quMenuIdDISPKOEF.AsInteger:=0;
                  quMenuIdACCESS.AsInteger:=0;
                  quMenuIdFLAGS.AsInteger:=0;
                  quMenuIdTARA.AsInteger:=0;
                  quMenuIdCNTPRICE.AsInteger:=0;
                  quMenuIdBACKBGR.AsFloat:=0;
                  quMenuIdFONTBGR.AsFloat:=0;
                  quMenuIdIACTIVE.AsInteger:=1;
                  quMenuIdIEDIT.AsInteger:=0;
                  quMenuId.Post;

                  quMenuId.Active:=False;

                  with dmO do
                  begin
                  // EXECUTE PROCEDURE PR_ADDKB (?IDDB, ?ITYPE, ?SIFR, ?CODEB, ?KB)
                    prAddKb.ParamByName('IDDB').AsInteger:=cxLookupComboBox1.EditValue;
                    prAddKb.ParamByName('ITYPE').AsInteger:=1;
                    prAddKb.ParamByName('SIFR').AsInteger:=Id;
                    prAddKb.ParamByName('CODEB').AsInteger:=iNum;
                    prAddKb.ParamByName('KB').AsFloat:=1;
                    prAddKb.ExecProc;
                  end;

                  //� �������� ���� ������� - ����� ��� � �������

                  iMax:=1;
                  if quMDS.RecordCount>0 then
                  begin
                    quMDS.Last;
                    iMax:=quMDSPRI.AsInteger+1;
                  end;

                  quMDS.Append;
                  quMDSIDH.AsInteger:=quMenuDayListID.AsINteger;
                  quMDSIDGR.AsInteger:=Integer(TreeMD.Selected.Data);
                  quMDSSIFR.AsInteger:=Id;
                  quMDSPRI.AsInteger:=iMax;
                  quMDSPRICE.AsFloat:=quCardsSelLASTPRICEOUT.AsFloat;
                  quMDS.Post;

                  quMDS.Refresh;

                except
                end;

                end;
              end;
            end;
          end;
          ViewMenuCr.EndUpdate;
        end else
          showmessage('�������� � �������� ������� ����.');
      end;
    end;
  end;
end;

procedure TfmMenuCr.MenuTreeDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept:=False;
  if bDrMove then Accept:=True;
end;

procedure TfmMenuCr.MenuTreeDragDrop(Sender, Source: TObject; X,
  Y: Integer);
Var sGr:String;
    iGr:Integer;
    iCo:Integer;
    i,j: Integer;
    iNum: Integer;
    Rec:TcxCustomGridRecord;

begin
  if bDrMove then
  begin
    bDrMove:=False;
    sGr:=MenuTree.DropTarget.Text;
    iGr:=Integer(MenuTree.DropTarget.data);
    iCo:=ViewMenuCr.Controller.SelectedRecordCount;
    if iCo>0 then
    begin
      if MessageDlg('�� ������������� ������ ����������� ��������� ������� ('+IntToStr(iCo)+' ��.) � ������ "'+sGr+'"?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
 //       with dmC do
  //      begin

        for i:=0 to ViewMenuCr.Controller.SelectedRecordCount-1 do
        begin
          Rec:=ViewMenuCr.Controller.SelectedRecords[i];

          for j:=0 to Rec.ValueCount-1 do
          begin
            if ViewMenuCr.Columns[j].Name='ViewMenuCrSIFR' then break;
          end;

          iNum:=Rec.Values[j];
          //��� ��� - ����������

          if quMenuSel.Locate('SIFR',iNum,[]) then
          begin
            trUpdM.StartTransaction;
            quMenuSel.Edit;
            quMenuSelPARENT.AsInteger:=iGr;
            quMenuSel.Post;
            trUpdM.Commit;
          end;
        end;
        ViewMenuCr.BeginUpdate;
        quMenuSel.FullRefresh;
        ViewMenuCr.EndUpdate;

        prRowFocus(ViewMenuCr);
      end;
    end;
  end;
end;

procedure TfmMenuCr.acCopyDMExecute(Sender: TObject);
Var IDH,IdhGr:INteger;
begin
  //���������� ������� ����
  with dmORep do
  begin
    IdHGr:=0;
    if quMenuDayList.RecordCount>0 then  IdHGr:=quMenuDayListID.AsInteger;

    if IdHGr=0 then begin ShowMessage('�������� ������� ���� ��� �����������.'); exit; end
    else
    begin
      fmAddMDL:=tfmAddMDL.Create(Application);
      fmAddMDL.cxTextEdit1.Text:='';
      fmAddMDL.cxDateEdit1.Date:=Date+1;
      fmAddMDL.ShowModal;
      if fmAddMDL.ModalResult=mrOk then
      begin
        prGetId.ParamByName('ITYPE').Value:=12;
        prGetId.ExecProc;
        IDH:=prGetId.ParamByName('RESULT').Value;

        quMenuDayList.Append;
        quMenuDayListID.AsInteger:=IDH;
        quMenuDayListNAME.AsString:=fmAddMDL.cxTextEdit1.Text;
        quMenuDayListMDATE.AsDateTime:=Trunc(fmAddMDL.cxDateEdit1.Date);
        quMenuDayListIACTIVE.AsInteger:=0;
        quMenuDayList.Post;

        //�������� ����� ������
        if IdHGr>0 then
        begin
          quMDGroups.Active:=False;
          quMDGroups.ParamByName('IDH').AsInteger:=IdHGr;
          quMDGroups.Active:=True;

          quMDGID.Active:=False;
          quMDGID.ParamByName('IDH').AsInteger:=IDH;
          quMDGID.Active:=True;

          while not quMDGID.Eof do quMDGID.Delete;   //���� ��� ������ ��� ������ �� ������ ������

          quMDGroups.First;
          while not quMDGroups.Eof do
          begin
            quMDGID.Append;
            quMDGIDIDH.AsInteger:=IDH;
            quMDGIDID.AsInteger:=quMDGroupsID.AsInteger;
            quMDGIDNAMEGR.AsString:=quMDGroupsNAMEGR.AsString;
            quMDGIDPRIOR.AsInteger:=quMDGroupsPRIOR.AsInteger;
            quMDGIDPARENT.AsInteger:=quMDGroupsPARENT.AsInteger;
            quMDGID.Post;

            //�������� ������ - ����� �������� ��� � ������� �� ��������� ������

            quMDSSel.Active:=False;
            quMDSSel.ParamByName('IDH').AsInteger:=IdHGr;
            quMDSSel.ParamByName('ID').AsInteger:=quMDGroupsID.AsInteger;
            quMDSSel.Active:=True;

            quMDSEdit.Active:=False;
            quMDSEdit.ParamByName('IDH').AsInteger:=IdH;
            quMDSEdit.ParamByName('ID').AsInteger:=quMDGroupsID.AsInteger;
            quMDSEdit.Active:=True;

            quMDSEdit.First;
            while not quMDSEdit.Eof do quMDSEdit.Delete;

            quMDSSel.First;
            while not quMDSSel.Eof do
            begin
              quMDSEdit.Append;
              quMDSEditIDH.AsInteger:=IDH;
              quMDSEditIDGR.AsInteger:=quMDGroupsID.AsInteger;
              quMDSEditSIFR.AsInteger:=quMDSSelSIFR.AsInteger;
              quMDSEditPRI.AsInteger:=quMDSSelPRI.AsInteger;
              quMDSEditPRICE.AsFloat:=quMDSSelPRICE.AsFloat;
              quMDSEdit.Post;

              quMDSSel.Next;
            end;

            quMDSEdit.Active:=False;
            quMDSSel.Active:=False;

            quMDGroups.Next;
          end;

          quMDGID.Active:=False;
          quMDGroups.Active:=False;
        end;
        quMenuDayList.Refresh;
      end;
      fmAddMDL.Release;
    end;
  end;
end;

procedure TfmMenuCr.FormShow(Sender: TObject);
begin
  Memo1.Clear;
end;

procedure TfmMenuCr.acRecalcPriceExecute(Sender: TObject);
Var iL:INteger;
    i,j: Integer;
    iNum,iShifr: Integer;
    Rec:TcxCustomGridRecord;
    rK:Real;
    sName:String;
    rNac:Real;
    iT:INteger;
    rPrSS,rPrOut:Real;
    iRound:Real;
    iUstCost:INteger;
begin
  iL:=ViewMenuDay.Controller.SelectedRecordCount;
  if iL>0 then
  begin
    if MessageDlg('����������� ��������� ���� �� ��������� �������� ('+IntToStr(iL)+' ��.)?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      fmRecalcPrice:=TfmRecalcPrice.Create(Application);
      fmRecalcPrice.ShowModal;
      if fmRecalcPrice.ModalResult=mrOk then
      begin
        delay(100);
        Memo1.Clear;
        Memo1.Lines.Add('����� ���� ������ ��� ...'); delay(10);
        with dmO do
        with dmORep do
        begin
          for i:=0 to ViewMenuDay.Controller.SelectedRecordCount-1 do
          begin
            iNum:=0; rK:=0; iShifr:=0;

            Rec:=ViewMenuDay.Controller.SelectedRecords[i];

            for j:=0 to Rec.ValueCount-1 do
            begin
              if ViewMenuDay.Columns[j].Name='ViewMenuDaySIFR' then iShifr:=Rec.Values[j];
              if ViewMenuDay.Columns[j].Name='ViewMenuDayCODE' then iNum:=StrToINtDef(Rec.Values[j],0);
              if ViewMenuDay.Columns[j].Name='ViewMenuDayCONSUMMA' then rK:=StrToFloatDef(Rec.Values[j],0);
              if ViewMenuDay.Columns[j].Name='ViewMenuDayNAME' then sName:=Rec.Values[j];
            end;

            Memo1.Lines.Add(' ');  delay(10);
            Memo1.Lines.Add('  ���. '+sName+', ��� - '+its(iShifr)+', ����. ��� - '+its(iNum));  delay(10);

            quFindCard.Active:=False;
            quFindCard.ParamByName('IDCARD').AsInteger:=iNum;
            quFindCard.Active:=True;
            if (quFindCard.RecordCount>0)and(rK>0) then
            begin
              rNac:=fmRecalcPrice.cxCalcEdit1.Value;
              iT:=fmRecalcPrice.cxRadioGroup1.ItemIndex;

              CloseTe(teC0);

              teC0.Append;
              teC0ID.AsInteger:=1;
              teC0IDCARD.AsInteger:=iNum;
              teC0NAME.AsString:=quFindCardNAME.AsString;
              teC0TCARD.AsInteger:=quFindCardTCARD.AsInteger;
              teC0QUANT.AsFloat:=1;
              teC0IDM.AsInteger:=quFindCardIMESSURE.AsInteger;
              teC0PRICER.AsFloat:=0;
              teC0RSUM.AsFloat:=0;
              teC0KM.AsInteger:=1;
              teC0.Post;

              //������ ������������� �����
              rPrSS:=rK*prCalcBlPriceIn(iNum,iT,0,Trunc(date),Memo1,iUstCost);

              if (iUstCost=1)or(iT=2) then
              begin
                Memo1.Lines.Add('          �����. �������� - '+fts(rK)+'  ( c ������ �� '+fts(rv(rPrSS))+')'); delay(10);
                Memo1.Lines.Add('          ������� - '+fts(rv(rNac))+'%'); delay(10);

                rPrOut:=rv(rPrSS*(rNac+100)/100); //���������� �� ������

                Memo1.Lines.Add('      ���� ������� - '+fts(rPrOut));  delay(10);

                iRound:=fmRecalcPrice.cxComboBox1.ItemIndex;
                if iRound=0 then rPrOut:=RoundEx(rPrOut/10)*10; //���������� �� 10-��� ������
                if iRound=1 then rPrOut:=RoundEx(rPrOut); //���������� �� ������
                if iRound=2 then rPrOut:=RoundEx(rPrOut*10)/10; //���������� �� 10-��� ������

                if quMDS.Locate('SIFR',iShifr,[]) then
                begin
                  quMDS.Edit;
                  quMDSPRICE.AsFloat:=rPrOut;
                  quMDS.Post;
                end;

                Memo1.Lines.Add(' ');  delay(10);
              end else begin Memo1.Lines.Add('      ��� ������������ ���� - ������ ����������.'); delay(10); end;
            end else Memo1.Lines.Add('      ������������ ���������� - ������ ����������.'); delay(10);
          end;
        end;
        Memo1.Lines.Add('������ ��� ��������.'); delay(10);
      end;
      fmRecalcPrice.Release;
    end;
  end;
end;

procedure TfmMenuCr.Memo1DblClick(Sender: TObject);
begin
  fmMessage:=tfmMessage.Create(Application);
  fmMessage.Memo1.Lines:=Memo1.Lines;
  fmMessage.ShowModal;
  fmMessage.Release;
end;

procedure TfmMenuCr.acDelPosExecute(Sender: TObject);
begin
  if quMDS.RecordCount>0 then
  begin
    if MessageDlg('������� ��� '+IntToStr(quMDSSIFR.AsInteger)+' '+quMDSNAME.AsString+' ?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      quMDS.Delete;
    end;
  end;
end;

procedure TfmMenuCr.acCalcAllMenuExecute(Sender: TObject);
Var iNum,iShifr: Integer;
    rK:Real;
    sName:String;
    rNac:Real;
    iT:INteger;
    rPrSS,rPrOut:Real;
    iRound:Real;
    IdhGr:INteger;
    iUstCost:INteger;
begin
  IdHGr:=0;
  if quMenuDayList.RecordCount>0  then  IdHGr:=quMenuDayListID.AsInteger;

  if IdHGr=0 then begin ShowMessage('�������� ������� ���� ��� �������.'); exit; end
  else
  begin
    if MessageDlg('����������� ��������� ���� �� ���� - '+quMenuDayListNAME.AsString+' �� '+quMenuDayListMDATE.AsString+' ?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      fmRecalcPrice:=TfmRecalcPrice.Create(Application);
      fmRecalcPrice.ShowModal;
      if fmRecalcPrice.ModalResult=mrOk then
      begin
        delay(100);
        Memo1.Clear;
        Memo1.Lines.Add('����� ���� ������ ��� ...'); delay(10);
        with dmO do
        with dmORep do
        begin
          quMDSAll.Active:=False;
          quMDSAll.ParamByName('IDH').AsInteger:=IdHGr;
          quMDSAll.Active:=True;

          quMDSAll.First;
          while not quMDSAll.Eof do
          begin
            iNum:=quMDSAllCODE.AsInteger;
            rK:=quMDSAllCONSUMMA.AsFloat;
            iShifr:=quMDSAllSIFR.AsInteger;
            sName:=quMDSAllNAME.AsString;

            Memo1.Lines.Add(' ');  delay(10);
            Memo1.Lines.Add('  ���. '+sName+', ��� - '+its(iShifr)+', ����. ��� - '+its(iNum));  delay(10);

            quFindCard.Active:=False;
            quFindCard.ParamByName('IDCARD').AsInteger:=iNum;
            quFindCard.Active:=True;
            if (quFindCard.RecordCount>0)and(rK>0) then
            begin
              rNac:=fmRecalcPrice.cxCalcEdit1.Value;
              iT:=fmRecalcPrice.cxRadioGroup1.ItemIndex;

              CloseTe(teC0);

              teC0.Append;
              teC0ID.AsInteger:=1;
              teC0IDCARD.AsInteger:=iNum;
              teC0NAME.AsString:=quFindCardNAME.AsString;
              teC0TCARD.AsInteger:=quFindCardTCARD.AsInteger;
              teC0QUANT.AsFloat:=1;
              teC0IDM.AsInteger:=quFindCardIMESSURE.AsInteger;
              teC0PRICER.AsFloat:=0;
              teC0RSUM.AsFloat:=0;
              teC0KM.AsInteger:=1;
              teC0.Post;

              //������ ������������� �����
              rPrSS:=rK*prCalcBlPriceIn(iNum,iT,0,Trunc(date),Memo1,iUstCost);

              if (iUstCost=1)or(iT=2) then
              begin
                Memo1.Lines.Add('          �����. �������� - '+fts(rK)+'  ( c ������ �� '+fts(rv(rPrSS))+')'); delay(10);
                Memo1.Lines.Add('          ������� - '+fts(rv(rNac))+'%'); delay(10);

                rPrOut:=rv(rPrSS*(rNac+100)/100); //���������� �� ������

                Memo1.Lines.Add('      ���� ������� - '+fts(rPrOut));  delay(10);

                iRound:=fmRecalcPrice.cxComboBox1.ItemIndex;
                if iRound=0 then rPrOut:=RoundEx(rPrOut/10)*10; //���������� �� 10-��� ������
                if iRound=1 then rPrOut:=RoundEx(rPrOut); //���������� �� ������
                if iRound=2 then rPrOut:=RoundEx(rPrOut*10)/10; //���������� �� 10-��� ������

                quMDSAll.Edit;
                quMDSAllPRICE.AsFloat:=rPrOut;
                quMDSAll.Post;

                Memo1.Lines.Add(' ');  delay(10);
              end else
              begin
                Memo1.Lines.Add('      ��� ������������ ���� - ������ ����������.'); delay(10);
                ShowMessage('��� ������������ ���� - ������ ����������.');
                Break;
              end;
            end else Memo1.Lines.Add('      ������������ ���������� - ������ ����������.'); delay(10);

            quMDSAll.Next;
          end;

          quMDS.FullRefresh;
        end;
        Memo1.Lines.Add('������ ��� ��������.'); delay(10);
      end;
      fmRecalcPrice.Release;
    end;
  end;
end;


procedure TfmMenuCr.acPrintMenuExecute(Sender: TObject);
Var StrWk:String;
    IdHGr:INteger;
begin
//������
  IdHGr:=0;
  if quMenuDayList.RecordCount>0  then  IdHGr:=quMenuDayListID.AsInteger;

  if IdHGr=0 then begin ShowMessage('�������� ������� ���� ��� ������.'); exit; end
  else
  begin
    quMDRep.Active:=False;
    quMDRep.ParamByName('IDH').AsInteger:=IdHGr;
    quMDRep.Active:=True;

    strwk:=FormatDateTime('mm',GridMenuDay.Tag);
    if StrWk='01' then StrWk:=' ������ ';
    if StrWk='02' then StrWk:=' ������� ';
    if StrWk='03' then StrWk:=' ����� ';
    if StrWk='04' then StrWk:=' ������ ';
    if StrWk='05' then StrWk:=' ��� ';
    if StrWk='06' then StrWk:=' ���� ';
    if StrWk='07' then StrWk:=' ���� ';
    if StrWk='08' then StrWk:=' ������� ';
    if StrWk='09' then StrWk:=' �������� ';
    if StrWk='10' then StrWk:=' ������� ';
    if StrWk='11' then StrWk:=' ������ ';
    if StrWk='12' then StrWk:=' ������� ';

    frRepMD.LoadFromFile(CurDir + 'MenuDayOf.frf');
    frRepMD.ReportName:='����.';
    frVariables.Variable['MDate']:=FormatDateTime('�� dd'+StrWk+'yyyy �',GridMenuDay.Tag);

    frVariables.Variable['Tehnolog']:=Pers.Tehno;
    frVariables.Variable['Director']:=Pers.Dir;
    frVariables.Variable['ZavPr']:=Pers.ZavPr;
    frVariables.Variable['Buh']:=Pers.Buh;

    frRepMD.PrepareReport;
    frRepMD.ShowPreparedReport;
  end;
end;

procedure TfmMenuCr.acMOLExecute(Sender: TObject);
begin
  fmRMol:=TfmRMol.Create(Application);
  fmRMol.cxTextEdit1.Text:=Pers.Dir;
  fmRMol.cxTextEdit2.Text:=Pers.ZavPr;
  fmRMol.cxTextEdit3.Text:=Pers.Tehno;
  fmRMol.cxTextEdit4.Text:=Pers.Buh;
  fmRMol.ShowModal;
  if fmRMol.ModalResult=mrOk then
  begin
    Pers.Dir:=fmRMol.cxTextEdit1.Text;
    Pers.ZavPr:=fmRMol.cxTextEdit2.Text;
    Pers.Tehno:=fmRMol.cxTextEdit3.Text;
    Pers.Buh:=fmRMol.cxTextEdit4.Text;

    WriteIniDM;
  end;
  fmRMol.Release;
end;

procedure TfmMenuCr.acPrintQuantExecute(Sender: TObject);
Var iNum,iShifr: Integer;
    rK:Real;
    sName:String;
    IdhGr:INteger;
    rQuant:Real;
begin
  //������ ���-�� ������������ �� ��� ����
  IdHGr:=0;
  if quMenuDayList.RecordCount>0  then  IdHGr:=quMenuDayListID.AsInteger;

  if IdHGr=0 then begin ShowMessage('�������� ������� ���� ��� ������� ������������ ���-�� ���������.'); exit; end
  else
  begin
    if MessageDlg('��������� ����������� ���-�� ��������� �� ���� - '+quMenuDayListNAME.AsString+' �� '+quMenuDayListMDATE.AsString+' ?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      delay(100);
      Memo1.Clear;
      Memo1.Lines.Add('����� ���� ������ ...'); delay(10);
      with dmO do
      with dmORep do
      begin
        quMDSAll.Active:=False;
        quMDSAll.ParamByName('IDH').AsInteger:=IdHGr;
        quMDSAll.Active:=True;

        CloseTe(teC0);
        CloseTe(teC1);
        CloseTe(teC2);

        quMDSAll.First;
        while not quMDSAll.Eof do
        begin
          iNum:=quMDSAllCODE.AsInteger;
          rK:=quMDSAllCONSUMMA.AsFloat;
          iShifr:=quMDSAllSIFR.AsInteger;
          sName:=quMDSAllNAME.AsString;
          rQuant:=quMDSAllQUANT.AsFloat;

          if rQuant>0 then
          begin
            Memo1.Lines.Add(' ');  delay(10);
            Memo1.Lines.Add('  ���. '+sName+', ��� - '+its(iShifr)+', ����. ��� - '+its(iNum));  delay(10);

            quFindCard.Active:=False;
            quFindCard.ParamByName('IDCARD').AsInteger:=iNum;
            quFindCard.Active:=True;
            if (quFindCard.RecordCount>0)and(rK>0) then
            begin
              teC0.Append;
              teC0ID.AsInteger:=1;
              teC0IDCARD.AsInteger:=iNum;
              teC0NAME.AsString:=quFindCardNAME.AsString;
              teC0TCARD.AsInteger:=quFindCardTCARD.AsInteger;
              teC0QUANT.AsFloat:=rQuant*rK;
              teC0IDM.AsInteger:=quFindCardIMESSURE.AsInteger;
              teC0PRICER.AsFloat:=0;
              teC0RSUM.AsFloat:=0;
              teC0KM.AsInteger:=1;
              teC0.Post;

              prCalcBl('    ',teC0NAME.AsString,0,0,iNum,Trunc(date),teC0TCARD.AsInteger,teC0QUANT.AsFloat,teC0IDM.AsInteger,teC0,teC1,teC2,Memo1,True,1,0,0);

            end else Memo1.Lines.Add('      ������������ ���������� - ������ �� ����� ����������.'); delay(10);
          end;
          quMDSAll.Next;
        end;

        quMDS.FullRefresh;
      end;
      Memo1.Lines.Add('������ ������������ ���-�� ��������� ��������.'); delay(10);
      Memo1.Lines.Add('����� ... ���� ������������ ������ ...'); delay(10);

      //������
      with dmO do
      with dmORep do
      begin

        frRepMD.LoadFromFile(CurDir + 'KolProd.frf');

        frVariables.Variable['Depart']:=CommonSet.DepartName;
        frVariables.Variable['DOCDATE']:=Date;

        frRepMD.ReportName:='���-�� ���������.';
        frRepMD.PrepareReport;
        frRepMD.ShowPreparedReport;
      end;
    end;
  end;
end;

end.
