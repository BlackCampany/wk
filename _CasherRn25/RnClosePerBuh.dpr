program RnClosePerBuh;

uses
  Forms,
  Un1 in 'Un1.pas',
  ClosePerBuh in 'ClosePerBuh.pas' {fmClosePerBuh},
  PerACloseBuh in 'PerACloseBuh.pas' {fmPerA_CloseBuh};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfmClosePerBuh, fmClosePerBuh);
  Application.CreateForm(TfmPerA_CloseBuh, fmPerA_CloseBuh);
  Application.Run;
end.
