object fmMessure: TfmMessure
  Left = 382
  Top = 207
  Width = 318
  Height = 435
  Caption = #1045#1076#1080#1085#1080#1094#1099' '#1080#1079#1084#1077#1088#1077#1085#1080#1103
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 382
    Width = 310
    Height = 19
    Panels = <
      item
        Width = 300
      end
      item
        Width = 50
      end>
  end
  object ActionMainMenuBar1: TActionMainMenuBar
    Left = 0
    Top = 0
    Width = 310
    Height = 27
    UseSystemFont = False
    ActionManager = acMess
    AnimateDuration = 300
    AnimationStyle = asUnFold
    Caption = 'ActionMainMenuBar1'
    ColorMap = XPColorMap1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Spacing = 0
  end
  object MessTree: TTreeView
    Left = 0
    Top = 27
    Width = 310
    Height = 355
    Align = alClient
    BevelInner = bvNone
    BevelOuter = bvNone
    Images = dmO.imState
    Indent = 19
    PopupMenu = PopupMenu1
    ReadOnly = True
    TabOrder = 2
    OnDblClick = MessTreeDblClick
    OnExpanded = MessTreeExpanded
    OnKeyDown = MessTreeKeyDown
  end
  object XPColorMap1: TXPColorMap
    HighlightColor = 15660791
    BtnSelectedColor = clBtnFace
    UnusedColor = 15660791
    Left = 248
    Top = 96
  end
  object acMess: TActionManager
    ActionBars = <
      item
        Items = <
          item
            Action = Action8
            ImageIndex = 10
          end
          item
            Items = <
              item
                Action = Action1
                ImageIndex = 28
              end
              item
                Action = Action5
                ImageIndex = 28
              end
              item
                Action = Action2
                ImageIndex = 27
              end
              item
                Action = Action4
              end
              item
                Action = Action3
                ImageIndex = 29
              end>
            Action = Action9
          end>
        ActionBar = ActionMainMenuBar1
      end
      item
      end>
    Images = dmO.imState
    Left = 248
    Top = 40
    StyleName = 'XP Style'
    object Action1: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1086#1089#1085#1086#1074#1085#1091#1102
      ImageIndex = 28
      OnExecute = Action1Execute
    end
    object Action5: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1076#1086#1087#1086#1083#1085#1080#1090#1077#1083#1100#1085#1091#1102
      ImageIndex = 28
      OnExecute = Action5Execute
    end
    object Action4: TAction
      Caption = '-'
    end
    object Action2: TAction
      Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100
      ImageIndex = 27
      OnExecute = Action2Execute
    end
    object Action3: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ImageIndex = 29
      OnExecute = Action3Execute
    end
    object Action8: TAction
      Caption = #1042#1099#1093#1086#1076
      ImageIndex = 10
      OnExecute = Action8Execute
    end
    object Action9: TAction
      Caption = #1044#1077#1081#1089#1090#1074#1080#1103
      OnExecute = Action9Execute
    end
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 3000
    OnTimer = Timer1Timer
    Left = 168
    Top = 40
  end
  object PopupMenu1: TPopupMenu
    Images = dmO.imState
    Left = 64
    Top = 120
    object N1: TMenuItem
      Action = Action1
      ShortCut = 45
    end
    object N2: TMenuItem
      Action = Action5
      ShortCut = 16429
    end
    object N3: TMenuItem
      Action = Action2
      ShortCut = 115
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object N5: TMenuItem
      Action = Action3
      ShortCut = 46
    end
  end
end
