unit MainAdm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, Menus, ImgList, XPStyleActnCtrls, ActnList, ActnMan,
  ToolWin, ActnCtrls, ActnMenus, ActnColorMaps, ExtCtrls, StdCtrls, Buttons,
  Placemnt, SpeedBar,DB;

type
  TfmMainAdm = class(TForm)
    StatusBar1: TStatusBar;
    AM1: TActionManager;
    ImageList1: TImageList;
    XPColorMap1: TXPColorMap;
    ActionMainMenuBar1: TActionMainMenuBar;
    System: TAction;
    Nastroiki: TAction;
    Exit: TAction;
    CentersOtv: TAction;
    aCreateP: TAction;
    aCreateD: TAction;
    delP: TAction;
    DelD: TAction;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    SpeedItem6: TSpeedItem;
    acDelPeriod: TAction;
    procedure FormResize(Sender: TObject);
    procedure FormCanResize(Sender: TObject; var NewWidth,
      NewHeight: Integer; var Resize: Boolean);
    procedure N6Click(Sender: TObject);
    procedure SystemExecute(Sender: TObject);
    procedure NastroikiExecute(Sender: TObject);
    procedure ExitExecute(Sender: TObject);
    procedure CentersOtvExecute(Sender: TObject);
    procedure aCreatePExecute(Sender: TObject);
    procedure aCreateDExecute(Sender: TObject);
    procedure delPExecute(Sender: TObject);
    procedure DelDExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure acDelPeriodExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmMainAdm: TfmMainAdm;

implementation

uses Right, Dm, Un1, Passw, Period;

{$R *.dfm}

procedure TfmMainAdm.FormResize(Sender: TObject);
begin
  StatusBar1.Width:=Width;
end;

procedure TfmMainAdm.FormCanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
  NewHeight:=125;
end;

procedure TfmMainAdm.N6Click(Sender: TObject);
begin
  Close;
end;

procedure TfmMainAdm.SystemExecute(Sender: TObject);
begin
  Showmessage('�������');
end;

procedure TfmMainAdm.NastroikiExecute(Sender: TObject);
begin
//  JvSpeedBar1.Visible:=True;
  fmRight.Show;

 // Showmessage('���������');
end;

procedure TfmMainAdm.ExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmMainAdm.CentersOtvExecute(Sender: TObject);
begin
  Showmessage('������ ���������������');
end;

procedure TfmMainAdm.aCreatePExecute(Sender: TObject);
Var iNode,ID_Parent:Integer;
    TreeNode,TreeParent : TTreeNode;
begin
 //�������� ������������
  with fmRight do
  begin
    PageControl1.ActivePageIndex:=0;
    with dmC do
    begin
      if cxButton2.Enabled then
      begin
        showmessage('��������� �� ���������. ������� ������ "���������" ��� ����������, ��� ������ "������" ��� ������ ���������.');
      end
      else
      begin
        if taPersonalId_Parent.AsInteger>0 then
        begin
          showmessage('�������� ��������� ��� ������ ������������.');
        end
        else
        begin
          id_Parent:=taPersonalId.AsInteger;
          TreeParent:=TreePersonal.Selected;
          delay(10);
          //�������� � ��������
          taPersonal.Append;
          taPersonalID.AsInteger:=GetId('Pe');
          taPersonalID_PARENT.AsInteger:=ID_Parent;
          taPersonalNAME.AsString:='����� ������������';
          taPersonalUVOLNEN.AsString:='T';
          taPersonalPASSW.AsString:='';
          taPersonalModul1.AsInteger:=1;
          taPersonalModul2.AsInteger:=1;
          taPersonalModul3.AsInteger:=0;
          taPersonalModul4.AsInteger:=1;
          taPersonalModul5.AsInteger:=1;
          taPersonalModul6.AsInteger:=1;
          taPersonalBARCODE.AsString:='';
          taPersonal.Post;
          delay(10);

          taRClassif.Active:=False;
          taRClassif.Active:=True;

          //���������� ����� � RFunction
          prChangeRFunction.ParamByName('IDPERSONAL').AsInteger:=taPersonalId.AsInteger;
          prChangeRFunction.ParamByName('IDPOST').AsInteger:=ID_Parent;
          prChangeRFunction.ExecProc;
          delay(10);

          iNode:=taPersonalId.AsInteger;

          TreePersonal.Items.BeginUpdate;
          TreeNode:=TreePersonal.Items.AddChildObject(TreeParent, '����� ������������', Pointer(iNode));
          TreeNode.ImageIndex:=1;
          TreeNode.SelectedIndex:=3;
          TreePersonal.Items.AddChildObject(TreeNode,'', nil);
          TreePersonal.Items.EndUpdate;

          TreePersonal.Refresh;
          TreePersonal.Selected:=TreeNode;

          delay(10);
          Edit1.Text:='����� ������������';
        end;
      end;
    end;
  end;
end;

procedure TfmMainAdm.aCreateDExecute(Sender: TObject);
Var iNode:Integer;
    TreeNode : TTreeNode;
//    Bm: TBookmarkStr;
begin
 //�������� ���������
  with fmRight do
  begin
    PageControl1.ActivePageIndex:=0;
    with dmC do
    begin
      if cxButton2.Enabled then
      begin
        showmessage('��������� �� ���������. ������� ������ "���������" ��� ����������, ��� ������ "������" ��� ������ ���������.');
      end
      else
      begin
        taPersonal.Append;
        taPersonalID.AsInteger:=GetId('Pe');
        taPersonalID_PARENT.AsInteger:=0;
        taPersonalNAME.AsString:='����� ���������';
        taPersonalUVOLNEN.AsString:='F';
        taPersonalPASSW.AsString:='';
        taPersonalModul1.AsInteger:=1;
        taPersonalModul2.AsInteger:=1;
        taPersonalModul3.AsInteger:=1;
        taPersonalModul4.AsInteger:=1;
        taPersonalModul5.AsInteger:=1;
        taPersonalModul6.AsInteger:=1;

        taPersonal.Post;

//        trUpdate.CommitRetaining;
//        taPersonal.FullRefresh;

{        bm:=taPersonal.Bookmark;
        delay(10);
        taPersonal.Active:=False;
        taPersonal.Active:=True;
        taPersonal.Bookmark:=bm;
}
        iNode:=taPersonalId.AsInteger;

        taRClassif.Active:=False;
        taRClassif.Active:=True;

        TreePersonal.Items.BeginUpdate;
        TreeNode:=TreePersonal.Items.AddChildObject(Nil, '����� ���������', Pointer(iNode));
        TreeNode.ImageIndex:=0;
        TreeNode.SelectedIndex:=2;
        TreePersonal.Items.AddChildObject(TreeNode,'', nil);
        TreePersonal.Items.EndUpdate;
        TreePersonal.Refresh;
        TreePersonal.Selected:=TreeNode;

        delay(10);

        Edit1.Text:='����� ���������';

      end;
    end;
  end;
end;

procedure TfmMainAdm.delPExecute(Sender: TObject);
begin
//������� ������������
  with dmC do
  begin
    if taPersonalID_Parent.AsInteger > 0 then
    begin
      //�������� ����������� �������� ������������.
      prDelPersonal.ParamByName('IDPERSONAL').Value:=taPersonalId.AsInteger;
      prDelPersonal.ExecProc;
      if prDelPersonal.ParamByName('RESULT').Value>0 then
      begin
        showmessage('������� ������������ ������� ������. �� ���� ���� ������ � ������.');
      end
      else
      begin
        taPersonal.Delete;
        with fmRight do
        begin
          TreePersonal.Items.BeginUpdate;
          TreePersonal.Selected.Delete;
          TreePersonal.Items.EndUpdate;

          TreePersonal.Refresh;

          taPersonal.Locate('ID',Integer(TreePersonal.Selected.Data),[]);
          if taPersonalId_Parent.AsInteger=0 then
          begin
            Edit1.Text:=taPersonalName.AsString;
            Edit2.Text:='';
            Edit3.Text:='';
            CheckBox1.Checked:=False; CheckBox2.Checked:=False; CheckBox3.Checked:=False; CheckBox4.Checked:=False;
            Edit2.Enabled:=False; Edit3.Enabled:=False; CheckBox1.Enabled:=False; CheckBox2.Enabled:=False; CheckBox3.Enabled:=False; CheckBox4.Enabled:=False;
          end
          else
          begin
            Edit2.Enabled:=True; Edit3.Enabled:=True; CheckBox1.Enabled:=True; CheckBox2.Enabled:=True; CheckBox3.Enabled:=True; CheckBox4.Enabled:=True;

            Edit1.Text:=taPersonalName.AsString;
            Edit2.Text:=taPersonalPassw.AsString;
            while Length(Edit2.Text)<15 do Edit2.Text:=Edit2.Text+' ';
            Edit3.Text:=Edit2.Text;
            if taPersonalModul1.AsInteger=0 then CheckBox1.Checked:=True else CheckBox1.Checked:=False;
            if taPersonalModul2.AsInteger=0 then CheckBox2.Checked:=True else CheckBox2.Checked:=False;
            if taPersonalModul3.AsInteger=0 then CheckBox3.Checked:=True else CheckBox3.Checked:=False;
            if taPersonalModul4.AsInteger=0 then CheckBox5.Checked:=True else CheckBox5.Checked:=False;
            if taPersonalModul5.AsInteger=0 then CheckBox6.Checked:=True else CheckBox6.Checked:=False;
            if taPersonalModul6.AsInteger=0 then CheckBox7.Checked:=True else CheckBox7.Checked:=False;

            if taPersonalUvolnen.AsInteger=0 then CheckBox4.Checked:=True else CheckBox4.Checked:=False;
          end;
        end;
      end;
    end
    else
    begin
      showmessage('�������� ������������.');
    end;
  end;
end;

procedure TfmMainAdm.DelDExecute(Sender: TObject);
begin
  //������� ���������
  with dmC do
  begin
    if taPersonalID_Parent.AsInteger = 0 then
    begin //���������
      //�������� ������� �������������.
      prExistPersonal.ParamByName('IDPARENT').Value:=taPersonalId.AsInteger;
      prExistPersonal.ExecProc;
      if prExistPersonal.ParamByName('RESULT').Value>0 then
      begin
        showmessage('���������� ������������ � ������ ����������. �������� ����������.');
      end
      else
      begin
        taPersonal.Delete;
        with fmRight do
        begin
          TreePersonal.Items.BeginUpdate;
          TreePersonal.Selected.Delete;
          TreePersonal.Items.EndUpdate;

          TreePersonal.Refresh;

          taPersonal.Locate('ID',Integer(TreePersonal.Selected.Data),[]);
          if taPersonalId_Parent.AsInteger=0 then
          begin
            Edit1.Text:=taPersonalName.AsString;
            Edit2.Text:='';
            Edit3.Text:='';
            CheckBox1.Checked:=False; CheckBox2.Checked:=False; CheckBox3.Checked:=False; CheckBox4.Checked:=False;
            Edit2.Enabled:=False; Edit3.Enabled:=False; CheckBox1.Enabled:=False; CheckBox2.Enabled:=False; CheckBox3.Enabled:=False; CheckBox4.Enabled:=False;
          end
          else
          begin
            Edit2.Enabled:=True; Edit3.Enabled:=True; CheckBox1.Enabled:=True; CheckBox2.Enabled:=True; CheckBox3.Enabled:=True; CheckBox4.Enabled:=True;

            Edit1.Text:=taPersonalName.AsString;
            Edit2.Text:=taPersonalPassw.AsString;
            while Length(Edit2.Text)<15 do Edit2.Text:=Edit2.Text+' ';
            Edit3.Text:=Edit2.Text;

            if taPersonalModul1.AsInteger=0 then CheckBox1.Checked:=True else CheckBox1.Checked:=False;
            if taPersonalModul2.AsInteger=0 then CheckBox2.Checked:=True else CheckBox2.Checked:=False;
            if taPersonalModul3.AsInteger=0 then CheckBox3.Checked:=True else CheckBox3.Checked:=False;
            if taPersonalModul4.AsInteger=0 then CheckBox5.Checked:=True else CheckBox5.Checked:=False;
            if taPersonalModul5.AsInteger=0 then CheckBox6.Checked:=True else CheckBox6.Checked:=False;
            if taPersonalModul6.AsInteger=0 then CheckBox7.Checked:=True else CheckBox7.Checked:=False;

            if taPersonalUvolnen.AsInteger=0 then CheckBox4.Checked:=True else CheckBox4.Checked:=False;
          end;
        end;
      end;
    end
    else
    begin
      showmessage('�������� ���������.');
    end;
  end;
end;

procedure TfmMainAdm.FormShow(Sender: TObject);
begin
  fmPerA:=tfmPerA.Create(Application);
  fmPerA.ShowModal;
  if fmPerA.ModalResult=mrOk then
  begin
    Caption:=Caption+'   : '+Person.Name;
    WriteIni; //�������� �������� �������
  end
  else
  begin
    delay(100);
    close;
    delay(100);
  end;
  fmPerA.Release;

  fmRight.Show;
end;

procedure TfmMainAdm.FormCreate(Sender: TObject);
begin
  CurDir := ExtractFilePath(ParamStr(0));
  ReadIni;
  TrebSel.DateFrom:=Date-2;
  TrebSel.DateTo:=Date-3;
end;

procedure TfmMainAdm.acDelPeriodExecute(Sender: TObject);
Var Datebeg,DateEnd:TDateTime;
begin
  //������� ������
  if not CanDo('DelRealRn') then  StatusBar1.Panels[0].Text:='��� ����.';
  fmPeriod:=TfmPeriod.Create(Application);
  fmPeriod.DateTimePicker1.Date:=TrebSel.DateFrom;
  fmPeriod.DateTimePicker2.Date:=TrebSel.DateTo;
  fmPeriod.DateTimePicker3.Visible:=True;
  fmPeriod.DateTimePicker4.Visible:=True;
  fmPeriod.DateTimePicker3.Time:=0.083334;
  fmPeriod.DateTimePicker4.Time:=0.083334;
  fmPeriod.cxCheckBox1.Visible:=False;

  fmPeriod.ShowModal;
  if fmPeriod.ModalResult=mrOk then
  begin
//    showmessage('Ok');
//    Datebeg:=Trunc(fmPeriod.DateTimePicker1.Date);
//    DateEnd:=Trunc(fmPeriod.DateTimePicker2.Date)+1;
//    Datebeg:=TrebSel.DateFrom;
//    DateEnd:=TrebSel.DateTo;

    Datebeg:=TrebSel.DateFrom+Frac(fmPeriod.DateTimePicker3.Time);
    DateEnd:=TrebSel.DateTo+Frac(fmPeriod.DateTimePicker4.Time);

    with dmC do
    begin
{      quDelTabAll.Active:=False;
      quDelTabAll.ParamByName('DateB').AsDateTime:=Datebeg;
      quDelTabAll.ParamByName('DateE').AsDateTime:=DateEnd;
      quDelTabAll.Active:=True;

      quDelCS.Active:=False;
      quDelCS.ParamByName('DateB').AsDateTime:=Datebeg;
      quDelCS.ParamByName('DateE').AsDateTime:=DateEnd;
      quDelCS.Active:=True;

      quDelTabAll.ParamByName('DateB').AsDateTime:=Datebeg;
      quDelTabAll.ParamByName('DateE').AsDateTime:=DateEnd;
      quDelTabAll.ExecQuery;

      quDelCS.ParamByName('DateB').AsDateTime:=Datebeg;
      quDelCS.ParamByName('DateE').AsDateTime:=DateEnd;
      quDelCS.ExecQuery;
}
      showmessage('�������� �����������.');
    end;
  end;
  fmPeriod.Release;
end;

end.
