object fmPosMove: TfmPosMove
  Left = 407
  Top = 264
  BorderStyle = bsDialog
  Caption = #1055#1077#1088#1077#1085#1077#1089#1090#1080
  ClientHeight = 500
  ClientWidth = 351
  Color = 16449787
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel2: TBevel
    Left = 16
    Top = 8
    Width = 321
    Height = 53
  end
  object Label1: TLabel
    Left = 32
    Top = 16
    Width = 39
    Height = 13
    Caption = 'Label1'
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    Transparent = True
  end
  object Label2: TLabel
    Left = 32
    Top = 40
    Width = 39
    Height = 13
    Caption = 'Label2'
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    Transparent = True
  end
  object Bevel1: TBevel
    Left = 24
    Top = 432
    Width = 305
    Height = 5
  end
  object Label3: TLabel
    Left = 20
    Top = 120
    Width = 51
    Height = 16
    Caption = #1050#1086#1083'-'#1074#1086
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    Transparent = True
  end
  object cxButton3: TcxButton
    Left = 25
    Top = 444
    Width = 304
    Height = 45
    Caption = #1054#1090#1084#1077#1085#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ModalResult = 2
    ParentFont = False
    TabOrder = 0
    Colors.Default = 16759807
    Colors.Normal = 16759807
    Colors.Pressed = 11403438
    Glyph.Data = {
      5E040000424D5E04000000000000360000002800000012000000130000000100
      18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
      CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
      8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
      0000CED3D6848684848684848684848684848684848684848684848684848684
      848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
      7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
      FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
      00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
      75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
      0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
      FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
      CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
      007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
      00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
      75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
      0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
      FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
      CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
      00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
      494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
      0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
      00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
      CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
      0000}
    LookAndFeel.Kind = lfFlat
  end
  object CalcEdit1: TcxCalcEdit
    Left = 80
    Top = 108
    EditValue = 0.000000000000000000
    ParentFont = False
    Properties.ReadOnly = True
    Style.BorderStyle = ebsUltraFlat
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clNavy
    Style.Font.Height = -19
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = [fsBold]
    Style.LookAndFeel.Kind = lfFlat
    Style.ButtonStyle = btsDefault
    Style.ButtonTransparency = ebtHideInactive
    Style.IsFontAssigned = True
    StyleDisabled.LookAndFeel.Kind = lfFlat
    StyleFocused.LookAndFeel.Kind = lfFlat
    StyleHot.LookAndFeel.Kind = lfFlat
    TabOrder = 1
    OnEnter = CalcEdit1Enter
    Width = 85
  end
  object cxRadioGroup1: TcxRadioGroup
    Left = 16
    Top = 148
    Caption = ' '#1055#1088#1080#1105#1084#1085#1080#1082' '
    ParentFont = False
    Properties.Columns = 2
    Properties.Items = <
      item
        Caption = #1042' '#1085#1086#1074#1099#1081
      end
      item
        Caption = #1042' '#1089#1091#1097#1077#1089#1090#1074#1091#1102#1097#1080#1081
      end>
    Properties.OnChange = cxRadioGroup1PropertiesChange
    ItemIndex = 0
    Style.BorderStyle = ebsFlat
    Style.LookAndFeel.Kind = lfUltraFlat
    StyleDisabled.LookAndFeel.Kind = lfUltraFlat
    StyleFocused.LookAndFeel.Kind = lfUltraFlat
    StyleHot.LookAndFeel.Kind = lfUltraFlat
    TabOrder = 2
    Height = 53
    Width = 321
  end
  object cxButton1: TcxButton
    Left = 25
    Top = 380
    Width = 304
    Height = 45
    Caption = #1055#1077#1088#1077#1085#1077#1089#1090#1080
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ModalResult = 1
    ParentFont = False
    TabOrder = 3
    Colors.Default = 16759807
    Colors.Normal = 16759807
    Colors.Pressed = 11403438
    LookAndFeel.Kind = lfFlat
  end
  object Panel1: TPanel
    Left = 16
    Top = 208
    Width = 321
    Height = 41
    BevelInner = bvLowered
    Color = 16449787
    TabOrder = 4
    object Label4: TLabel
      Left = 24
      Top = 16
      Width = 78
      Height = 13
      Caption = #1053#1086#1084#1077#1088' '#1089#1090#1086#1083#1072
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Transparent = True
    end
    object TextEdit1: TcxTextEdit
      Left = 144
      Top = 8
      ParentFont = False
      Properties.Alignment.Horz = taRightJustify
      Properties.MaxLength = 8
      Style.BorderColor = 8454143
      Style.BorderStyle = ebsUltraFlat
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clNavy
      Style.Font.Height = -13
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsBold]
      Style.LookAndFeel.Kind = lfUltraFlat
      Style.IsFontAssigned = True
      StyleDisabled.LookAndFeel.Kind = lfUltraFlat
      StyleFocused.LookAndFeel.Kind = lfUltraFlat
      StyleHot.LookAndFeel.Kind = lfUltraFlat
      TabOrder = 0
      Text = '0'
      OnEnter = TextEdit1Enter
      Width = 124
    end
  end
  object Panel2: TPanel
    Left = 16
    Top = 208
    Width = 321
    Height = 165
    BevelInner = bvLowered
    Color = 16449787
    TabOrder = 5
    Visible = False
    object Grid: TcxGrid
      Left = 2
      Top = 2
      Width = 317
      Height = 161
      Align = alClient
      TabOrder = 0
      object ViewMove: TcxGridDBCardView
        NavigatorButtons.ConfirmDelete = False
        DataController.DataSource = dmC.dsTabsPers
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        LayoutDirection = ldVertical
        OptionsBehavior.ImmediateEditor = False
        OptionsCustomize.CardSizing = False
        OptionsCustomize.RowFiltering = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsSelection.UnselectFocusedRecordOnExit = False
        OptionsView.CardWidth = 93
        OptionsView.SeparatorColor = 16713471
        OptionsView.SeparatorWidth = 3
        Styles.Background = dmC.cxStyle20
        Styles.CardBorder = dmC.cxStyle19
        object ViewMoveNUMTABLE: TcxGridDBCardViewRow
          Caption = #1057#1090#1086#1083
          DataBinding.FieldName = 'NUMTABLE'
          Styles.Content = dmC.cxStyle13
        end
        object ViewMoveTABSUM: TcxGridDBCardViewRow
          Caption = #1057#1091#1084#1084#1072
          DataBinding.FieldName = 'TABSUM'
          Styles.Content = dmC.cxStyle12
        end
      end
      object Level: TcxGridLevel
        GridView = ViewMove
      end
    end
  end
  object cxImageComboBox1: TcxImageComboBox
    Left = 196
    Top = 107
    EditValue = 1
    ParentFont = False
    Properties.ButtonGlyph.Data = {
      16080000424D16080000000000003600000028000000170000001C0000000100
      180000000000E007000000000000000000000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB5B2AD6B7D8494AAB5ADB6BDBDBE
      BDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
      0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC6C3BD528AA57BAEC6
      4292BD4296BD529EC673AEC694BACEA5BEC6BDC3C6C6C7C6D6C7C6FFFFFFFFFF
      FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA5
      AEB5318AAD7BB2C63986AD317DA53986AD4A96BD52A2C652A6CE5AAED66BBAD6
      84C3DEA5CBDEADBEC6C6BAB5FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFA5B2BD318EB57BB6CE2996C6218ABD3992BD529ABD4292B542
      8EB54292B53996BD399AC639A6CE4A9EBDBDBAB5FFFFFF000000FFFFFFFFFFFF
      FFFFFFFFFFFFEFDBCEE7CFC6DECBBD8CA2AD2996BD7BBACE299ED6219AD652B6
      DE5AB6DE39AADE42AED639A6D631A2CE299EC6299AC6318AADC6C3C6FFFFFF00
      0000FFFFFFFFFFFFFFFFFFD6C3BDA5968C8C827B8C868463828C299AC684BECE
      29A2D642AEDE6BC3E742B6DE52BAE752BEE742BAE729B6E718B2E710B6EF21A6
      CEC6C7C6FFFFFF000000FFFFFFFFFFFFADA69C847D7B525152393C394A49424A
      718429A2CE84BED642B2DE6BC7E752BEE763C7EF63CBEF4AC3EF29BAEF10B6EF
      10BAEF10BEF721AED6C6C7C6FFFFFF000000FFFFFFA596947B7D7B6B696B6365
      637371734A494A31596329A6CE94C3D66BAAC66BA6BD73B2CE73BED652BADE29
      B6E718BAEF18BEEF10C3F710C7F721B2DEC6C7CEFFFFFF000000C6BAB57B7D7B
      7B7D7B6361637B797BB5B2B57B797B4A718421AAD69CCBD68CCBDE63BAD639AA
      CE31A6CE31A2C639A6C642AACE42AECE42B2D631BADE31AACEC6CBCEFFFFFF00
      0000C6B6AD7B7D7B7B7D7B5A5D5A737573CECFCE9496944A6D7B42A2BD5A757B
      7BA6B55AA6BD39A6C631AAD621B2DE18B6E718BAE721BAE721BADE29B6DE39A2
      BDCECBCEFFFFFF000000D6C3BD7B7D7B7B797B5A5D5A5A595AB5B2B59C9A9C42
      494A7B929C5282945A96AD6B9EB584AABD73AEC67BB6C67BBACE7BBED66BBED6
      5ABEDE4AC3DE42B2CEC6C7C6FFFFFF000000E7CFC68C86847B797B5A595A4A49
      4A737173B5B6B531303139383952595A4A595A5A656B5A696B4A798C528EA54A
      92AD639AA573BAD66BC7DE84CBDEA5BEC6CEC3BDFFFFFF000000FFFFFFADA29C
      7375735A595A4245424A4D4AADAAAD393C392124214241424A494A4245426365
      63525552525552636563A59E9CD6D3CEBDCBCEB5C3C6FFFFFFFFFFFFFFFFFF00
      0000FFFFFFFFFFFFADA29C736D6B4A4D4A4A5152738AAD5A6D8C313431181C18
      393839424542393C394A494A948A84FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFEFD3C6B5AAAD6B7D945A86CE63
      8EDE6382B52934422120215A595A6B6963BDAEA5FFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFF9CBA
      F784B2F76B9AEF5A86D6638EDE6B8ECE424D5A847973DECFC6FFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
      FFFFFFB5C3E78CBAFF8CBEFF8CBAFF73A6F7638EE773A2EF94A6CEE7CBBDFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
      0000FFFFFFFFFFFFE7D7CEA5BEF794BEFF8CBAFF8CBAFF8CB6FF6B9EF7739EEF
      A5BEE7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFF000000FFFFFFFFFFFFDED3DEA5C3FF94BEFF94BEFF94BEFF94
      BEFF7BAAF76B9AEF94AEE7EFD7CEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFCECFDEADCFFF9CC7
      FF9CC7FF9CC7FF9CC3FF8CB6FF6B9AEF5A7DB5B5AAA5FFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
      D6D3E7BDD7FFA5CBFFA5CFFFADCFFFA5CFFF94BAFF6B96E75A82C652515AFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
      0000FFFFFFFFFFFFD6D7E7C6DFFFB5D7FFBDD7FFBDDBFFBDDBFF94B6EF394D6B
      31416B212429B5AAA5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFF000000FFFFFFFFFFFFDEDBEFDEEBFFCEE3FFD6E7FFDEEBFFCE
      E7FF8CB2E7313439000000101410AD9E9CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFBDC3D6C6D3
      EFDEEFFFEFF7FFCEE3FF7B96BD313031080808181818ADA2A5FFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFEFE3DE
      7B757B393C4239454A5A697B7B92AD6B82A5525963393C39212021313029D6C3
      BDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
      0000FFFFFFEFDFD6A59E9C7371735A595A424142313431101410212421525152
      4245427B7573F7E3DEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFD6CBC6A59E9C7371734A4D4A29
      2C29211C1863615AB5A6A5E7DBCEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFF7E3DEDECFC6CEBEBDCEC3BDF7DFDEF7EBDEFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000}
    Properties.DropDownRows = 9
    Properties.Items = <
      item
        Description = #1043#1086#1089#1090#1100' 1'
        ImageIndex = 0
        Value = 1
      end
      item
        Description = #1043#1086#1089#1090#1100' 2'
        ImageIndex = 0
        Value = 2
      end
      item
        Description = #1043#1086#1089#1090#1100' 3'
        ImageIndex = 0
        Value = 3
      end
      item
        Description = #1043#1086#1089#1090#1100' 4'
        ImageIndex = 0
        Value = 4
      end
      item
        Description = #1043#1086#1089#1090#1100' 5'
        ImageIndex = 0
        Value = 5
      end
      item
        Description = #1043#1086#1089#1090#1100' 6'
        ImageIndex = 0
        Value = 6
      end
      item
        Description = #1043#1086#1089#1090#1100' 7'
        ImageIndex = 0
        Value = 7
      end
      item
        Description = #1043#1086#1089#1090#1100' 8'
        ImageIndex = 0
        Value = 8
      end
      item
        Description = #1043#1086#1089#1090#1100' 9 '
        ImageIndex = 0
        Value = 9
      end>
    Style.BorderStyle = ebsOffice11
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -19
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = []
    Style.LookAndFeel.Kind = lfOffice11
    Style.ButtonStyle = btsDefault
    Style.ButtonTransparency = ebtAlways
    Style.IsFontAssigned = True
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 6
    Visible = False
    Width = 121
  end
  object cxRadioButton1: TcxRadioButton
    Left = 16
    Top = 76
    Width = 113
    Height = 17
    Caption = #1055#1086#1079#1080#1094#1080#1102
    Checked = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 7
    TabStop = True
    OnClick = cxRadioButton1Click
    Transparent = True
  end
  object cxRadioButton2: TcxRadioButton
    Left = 196
    Top = 76
    Width = 113
    Height = 17
    Caption = #1043#1086#1089#1090#1103
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 8
    OnClick = cxRadioButton2Click
    Transparent = True
  end
end
