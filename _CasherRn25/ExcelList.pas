unit ExcelList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, SpeedBar, ComCtrls, ActnList, XPStyleActnCtrls,
  ActnMan, DB, dxmdaset, cxPivotGridCustomDataSet,
  cxPivotGridSummaryDataSet, cxStyles, cxCustomData, cxGraphics, cxFilter,
  cxData, cxDataStorage, cxEdit, cxDBData, cxGridLevel, cxClasses,
  cxControls, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxDBPivotGrid, cxCustomPivotGrid;

type
  TfmExcelList = class(TForm)
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    amExcelList: TActionManager;
    acExportExcel: TAction;
    acExit: TAction;
    dsSummary: TcxPivotGridSummaryDataSet;
    dsdsSummary: TDataSource;
    ViewEx: TcxGridDBTableView;
    LevelEx: TcxGridLevel;
    GridEx: TcxGrid;
    procedure acExitExecute(Sender: TObject);
    procedure acExportExcelExecute(Sender: TObject);
    procedure dsSummaryCreateField(Sender: TcxPivotGridCustomDataSet;
      APivotGridField: TcxPivotGridField; ADataSetField: TField);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure CreateFooterSummary(ATableView: TcxGridTableView; PG:TcxDBPivotGrid);
  end;

var
  fmExcelList: TfmExcelList;

implementation

uses Un1;

{$R *.dfm}

procedure TfmExcelList.CreateFooterSummary(ATableView: TcxGridTableView; PG:TcxDBPivotGrid);

  procedure CreateFooterSummaryCell(AGridColumn: TcxGridColumn);
  begin
    AGridColumn.Summary.FooterKind := skSum;
    AGridColumn.Summary.FooterFormat := ',.00';
  end;

  function ColumnByCaption(ACaption: string): TcxGridColumn;
  var
    I: Integer;
  begin
    Result := nil;
    for I := 0 to ATableView.ColumnCount - 1 do
      if ATableView.Columns[I].Caption = ACaption then
      begin
        Result := ATableView.Columns[I];
        Break;
      end;
  end;

var
  I: Integer;
begin
  for I := 0 to PG.FieldCount - 1 do
  begin
    if (PG.Fields[I].Area = faData) then
      if not PG.Fields[I].DataBinding.ValueTypeClass.IsString then
      begin
        CreateFooterSummaryCell(ColumnByCaption(PG.Fields[I].Caption));
      end;
  end;

 { �������� �� ��� ������ ����� � ������� ������, �� �������� ������ �� �������.

 for I := 0 to ATableView.ColumnCount - 1 do
    if ATableView.Columns[I]. = ACaption then
      begin
        Result := ATableView.Columns[I];
        Break;
      end;}
end;


procedure TfmExcelList.acExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmExcelList.acExportExcelExecute(Sender: TObject);
begin
//��� ��� �������
  prNExportExel5(ViewEx);
end;

procedure TfmExcelList.dsSummaryCreateField(
  Sender: TcxPivotGridCustomDataSet; APivotGridField: TcxPivotGridField;
  ADataSetField: TField);
begin
  if ADataSetField is TStringField then ADataSetField.Size:=TcxDBPivotGridField(APivotGridField).DataBinding.DBField.Size;
end;

end.
