unit DMOReps;

interface

uses
  SysUtils, Classes, DB, DBClient, frexpimg, frRtfExp, frTXTExp, frXMLExl,
  frOLEExl, FR_E_HTML2, FR_E_HTM, FR_E_CSV, FR_E_RTF, FR_Class, FR_E_TXT,
  FR_DSet, FR_DBSet, FIBDataSet, pFIBDataSet, FIBDatabase, pFIBDatabase,
  FIBQuery, pFIBQuery, pFIBStoredProc,Dialogs,cxMemo, dxmdaset, Un1;

type
  TdmORep = class(TDataModule)
    taPartTest: TClientDataSet;
    taPartTestNum: TIntegerField;
    taPartTestIdGoods: TIntegerField;
    taPartTestNameG: TStringField;
    taPartTestIM: TIntegerField;
    taPartTestSM: TStringField;
    taPartTestQuant: TFloatField;
    taPartTestPrice1: TCurrencyField;
    taPartTestiRes: TSmallintField;
    dsPartTest: TDataSource;
    taCalc: TClientDataSet;
    taCalcArticul: TIntegerField;
    taCalcName: TStringField;
    taCalcIdm: TIntegerField;
    taCalcsM: TStringField;
    taCalcKm: TFloatField;
    taCalcQuant: TFloatField;
    taCalcQuantFact: TFloatField;
    taCalcQuantDiff: TFloatField;
    dsCalc: TDataSource;
    frRep1: TfrReport;
    frTextExport1: TfrTextExport;
    frRTFExport1: TfrRTFExport;
    frCSVExport1: TfrCSVExport;
    frHTMExport1: TfrHTMExport;
    frHTML2Export1: TfrHTML2Export;
    frOLEExcelExport1: TfrOLEExcelExport;
    frXMLExcelExport1: TfrXMLExcelExport;
    frTextAdvExport1: TfrTextAdvExport;
    frRtfAdvExport1: TfrRtfAdvExport;
    frBMPExport1: TfrBMPExport;
    frJPEGExport1: TfrJPEGExport;
    frTIFFExport1: TfrTIFFExport;
    taCalcSumUch: TFloatField;
    taCalcSumIn: TFloatField;
    taCalcB: TClientDataSet;
    taCalcBCODEB: TIntegerField;
    taCalcBNAMEB: TStringField;
    taCalcBQUANT: TFloatField;
    taCalcBPRICEOUT: TFloatField;
    taCalcBSUMOUT: TFloatField;
    taCalcBID: TIntegerField;
    taCalcBIDCARD: TIntegerField;
    taCalcBNAMEC: TStringField;
    taCalcBQUANTC: TFloatField;
    taCalcBPRICEIN: TFloatField;
    taCalcBSUMIN: TFloatField;
    taCalcBIM: TIntegerField;
    taCalcBSM: TStringField;
    dsCalcB: TDataSource;
    frdsCalcB: TfrDBDataSet;
    taCalcBSB: TStringField;
    taTORep: TClientDataSet;
    dsTORep: TDataSource;
    taTORepsType: TStringField;
    taTORepsDocType: TStringField;
    taTORepsCliName: TStringField;
    taTORepsDate: TStringField;
    taTORepsDocNum: TStringField;
    taTOReprSum: TCurrencyField;
    taTOReprSum1: TCurrencyField;
    frdsTORep: TfrDBDataSet;
    taTOReprSumO: TCurrencyField;
    taTOReprSumO1: TCurrencyField;
    taCMove: TClientDataSet;
    dstaCMove: TDataSource;
    taCMoveARTICUL: TIntegerField;
    taCMoveIDATE: TIntegerField;
    taCMoveIDSTORE: TIntegerField;
    taCMoveNAMEMH: TStringField;
    taCMoveRB: TFloatField;
    taCMovePOSTIN: TFloatField;
    taCMovePOSTOUT: TFloatField;
    taCMoveVNIN: TFloatField;
    taCMoveVNOUT: TFloatField;
    taCMoveINV: TFloatField;
    taCMoveQREAL: TFloatField;
    taCMoveRE: TFloatField;
    quDocsInvSel: TpFIBDataSet;
    quDocsInvSelID: TFIBIntegerField;
    quDocsInvSelDATEDOC: TFIBDateField;
    quDocsInvSelNUMDOC: TFIBStringField;
    quDocsInvSelIDSKL: TFIBIntegerField;
    quDocsInvSelNAMEMH: TFIBStringField;
    quDocsInvSelIACTIVE: TFIBIntegerField;
    quDocsInvSelITYPE: TFIBIntegerField;
    quDocsInvSelSUM1: TFIBFloatField;
    quDocsInvSelSUM11: TFIBFloatField;
    quDocsInvSelSUM2: TFIBFloatField;
    quDocsInvSelSUM21: TFIBFloatField;
    quDocsInvSelSUMDIFIN: TFIBFloatField;
    quDocsInvSelSUMDIFUCH: TFIBFloatField;
    dsquDocsInvSel: TDataSource;
    quDocsInvId: TpFIBDataSet;
    quDocsInvIdID: TFIBIntegerField;
    quDocsInvIdDATEDOC: TFIBDateField;
    quDocsInvIdNUMDOC: TFIBStringField;
    quDocsInvIdIDSKL: TFIBIntegerField;
    quDocsInvIdIACTIVE: TFIBIntegerField;
    quDocsInvIdITYPE: TFIBIntegerField;
    quDocsInvIdSUM1: TFIBFloatField;
    quDocsInvIdSUM11: TFIBFloatField;
    quDocsInvIdSUM2: TFIBFloatField;
    quDocsInvIdSUM21: TFIBFloatField;
    quSpecInv: TpFIBDataSet;
    quSpecInvIDHEAD: TFIBIntegerField;
    quSpecInvID: TFIBIntegerField;
    quSpecInvNUM: TFIBIntegerField;
    quSpecInvIDCARD: TFIBIntegerField;
    quSpecInvQUANT: TFIBFloatField;
    quSpecInvSUMIN: TFIBFloatField;
    quSpecInvSUMUCH: TFIBFloatField;
    quSpecInvIDMESSURE: TFIBIntegerField;
    quSpecInvQUANTFACT: TFIBFloatField;
    quSpecInvSUMINFACT: TFIBFloatField;
    quSpecInvSUMUCHFACT: TFIBFloatField;
    quSpecInvKM: TFIBFloatField;
    quSpecInvTCARD: TFIBSmallIntField;
    quSpecInvIDGROUP: TFIBIntegerField;
    quSpecInvNAMESHORT: TFIBStringField;
    quSpecInvNAMECL: TFIBStringField;
    quSpecInvNAME: TFIBStringField;
    quSpecInvC: TpFIBDataSet;
    quSpecInvCIDHEAD: TFIBIntegerField;
    quSpecInvCID: TFIBIntegerField;
    quSpecInvCNUM: TFIBIntegerField;
    quSpecInvCIDCARD: TFIBIntegerField;
    quSpecInvCQUANT: TFIBFloatField;
    quSpecInvCSUMIN: TFIBFloatField;
    quSpecInvCSUMUCH: TFIBFloatField;
    quSpecInvCIDMESSURE: TFIBIntegerField;
    quSpecInvCQUANTFACT: TFIBFloatField;
    quSpecInvCSUMINFACT: TFIBFloatField;
    quSpecInvCSUMUCHFACT: TFIBFloatField;
    quSpecInvCKM: TFIBFloatField;
    quSpecInvCTCARD: TFIBSmallIntField;
    quSpecInvCIDGROUP: TFIBIntegerField;
    quSpecInvCNAMESHORT: TFIBStringField;
    quSpecInvCNAMECL: TFIBStringField;
    quSpecInvCNAME: TFIBStringField;
    trSelRep: TpFIBTransaction;
    quDocOutBPrint: TpFIBDataSet;
    quDocOutBPrintIDB: TFIBIntegerField;
    quDocOutBPrintCODEB: TFIBIntegerField;
    quDocOutBPrintQUANT: TFIBFloatField;
    quDocOutBPrintSUMOUT: TFIBFloatField;
    quDocOutBPrintIDSKL: TFIBIntegerField;
    quDocOutBPrintSUMIN: TFIBFloatField;
    quDocOutBPrintNAMEB: TFIBStringField;
    quDocOutBPrintPRICER: TFIBFloatField;
    quSelInSum: TpFIBDataSet;
    quSelNamePos: TpFIBDataSet;
    quSelNamePosNAMECL: TFIBStringField;
    quSelNamePosPARENT: TFIBIntegerField;
    quSelNamePosNAME: TFIBStringField;
    quSelNamePosSM: TFIBStringField;
    quSelNamePosKOEF: TFIBFloatField;
    quSelNamePosIMESSURE: TFIBIntegerField;
    quSelOutSum: TpFIBDataSet;
    quSpecInvNOCALC: TFIBSmallIntField;
    quSelNameCl: TpFIBDataSet;
    quSelNameClPARENT: TFIBIntegerField;
    quSelNameClNAMECL: TFIBStringField;
    quSpecInvBC: TpFIBDataSet;
    quSpecInvBCIDHEAD: TFIBIntegerField;
    quSpecInvBCIDB: TFIBIntegerField;
    quSpecInvBCID: TFIBIntegerField;
    quSpecInvBCCODEB: TFIBIntegerField;
    quSpecInvBCNAMEB: TFIBStringField;
    quSpecInvBCQUANT: TFIBFloatField;
    quSpecInvBCPRICEOUT: TFIBFloatField;
    quSpecInvBCSUMOUT: TFIBFloatField;
    quSpecInvBCIDCARD: TFIBIntegerField;
    quSpecInvBCNAMEC: TFIBStringField;
    quSpecInvBCQUANTC: TFIBFloatField;
    quSpecInvBCPRICEIN: TFIBFloatField;
    quSpecInvBCSUMIN: TFIBFloatField;
    quSpecInvBCIM: TFIBIntegerField;
    quSpecInvBCSM: TFIBStringField;
    quSpecInvBCSB: TFIBStringField;
    quDocsCompl: TpFIBDataSet;
    dsquDocsCompl: TDataSource;
    quDocsComplID: TFIBIntegerField;
    quDocsComplDATEDOC: TFIBDateField;
    quDocsComplNUMDOC: TFIBStringField;
    quDocsComplIDSKL: TFIBIntegerField;
    quDocsComplSUMIN: TFIBFloatField;
    quDocsComplSUMUCH: TFIBFloatField;
    quDocsComplSUMTAR: TFIBFloatField;
    quDocsComplPROCNAC: TFIBFloatField;
    quDocsComplIACTIVE: TFIBIntegerField;
    quDocsComplOPER: TFIBStringField;
    quDocsComplNAMEMH: TFIBStringField;
    quDocsComlRec: TpFIBDataSet;
    quSpecCompl: TpFIBDataSet;
    quSpecComplC: TpFIBDataSet;
    quSpecComplCB: TpFIBDataSet;
    quDocsComlRecID: TFIBIntegerField;
    quDocsComlRecDATEDOC: TFIBDateField;
    quDocsComlRecNUMDOC: TFIBStringField;
    quDocsComlRecIDSKL: TFIBIntegerField;
    quDocsComlRecSUMIN: TFIBFloatField;
    quDocsComlRecSUMUCH: TFIBFloatField;
    quDocsComlRecSUMTAR: TFIBFloatField;
    quDocsComlRecPROCNAC: TFIBFloatField;
    quDocsComlRecIACTIVE: TFIBIntegerField;
    quDocsComlRecOPER: TFIBStringField;
    quSpecComplIDHEAD: TFIBIntegerField;
    quSpecComplID: TFIBIntegerField;
    quSpecComplIDCARD: TFIBIntegerField;
    quSpecComplQUANT: TFIBFloatField;
    quSpecComplIDM: TFIBIntegerField;
    quSpecComplKM: TFIBFloatField;
    quSpecComplPRICEIN: TFIBFloatField;
    quSpecComplSUMIN: TFIBFloatField;
    quSpecComplPRICEINUCH: TFIBFloatField;
    quSpecComplSUMINUCH: TFIBFloatField;
    quSpecComplTCARD: TFIBSmallIntField;
    quSpecComplNAMESHORT: TFIBStringField;
    quSpecComplNAME: TFIBStringField;
    quSpecComplCIDHEAD: TFIBIntegerField;
    quSpecComplCID: TFIBIntegerField;
    quSpecComplCIDCARD: TFIBIntegerField;
    quSpecComplCQUANT: TFIBFloatField;
    quSpecComplCIDM: TFIBIntegerField;
    quSpecComplCKM: TFIBFloatField;
    quSpecComplCPRICEIN: TFIBFloatField;
    quSpecComplCSUMIN: TFIBFloatField;
    quSpecComplCPRICEINUCH: TFIBFloatField;
    quSpecComplCSUMINUCH: TFIBFloatField;
    quSpecComplCNAMESHORT: TFIBStringField;
    quSpecComplCNAME: TFIBStringField;
    quSpecComplCBIDHEAD: TFIBIntegerField;
    quSpecComplCBIDB: TFIBIntegerField;
    quSpecComplCBID: TFIBIntegerField;
    quSpecComplCBCODEB: TFIBIntegerField;
    quSpecComplCBNAMEB: TFIBStringField;
    quSpecComplCBQUANT: TFIBFloatField;
    quSpecComplCBIDCARD: TFIBIntegerField;
    quSpecComplCBNAMEC: TFIBStringField;
    quSpecComplCBQUANTC: TFIBFloatField;
    quSpecComplCBPRICEIN: TFIBFloatField;
    quSpecComplCBSUMIN: TFIBFloatField;
    quSpecComplCBIM: TFIBIntegerField;
    quSpecComplCBSM: TFIBStringField;
    quSpecComplCBSB: TFIBStringField;
    taHeadDoc: TClientDataSet;
    taSpecDoc: TClientDataSet;
    taHeadDocIType: TSmallintField;
    taHeadDocDateDoc: TIntegerField;
    taHeadDocNumDoc: TStringField;
    taHeadDocIdCli: TIntegerField;
    taHeadDocNameCli: TStringField;
    taHeadDocIdSkl: TIntegerField;
    taHeadDocNameSkl: TStringField;
    taHeadDocSumIN: TFloatField;
    taHeadDocSumUch: TFloatField;
    taHeadDocId: TIntegerField;
    taSpecDocIType: TSmallintField;
    taSpecDocIdHead: TIntegerField;
    taSpecDocNum: TIntegerField;
    taSpecDocIdCard: TIntegerField;
    taSpecDocQuant: TFloatField;
    taSpecDocPriceIn: TFloatField;
    taSpecDocSumIn: TFloatField;
    taSpecDocPriceUch: TFloatField;
    taSpecDocSumUch: TFloatField;
    taSpecDocIdNds: TIntegerField;
    taSpecDocSumNds: TFloatField;
    taSpecDocNameC: TStringField;
    taSpecDocSm: TStringField;
    taSpecDocIdM: TIntegerField;
    dsHeadDoc: TDataSource;
    dsSpecDoc: TDataSource;
    quDocsActs: TpFIBDataSet;
    dsquDocsActs: TDataSource;
    quDocsActsId: TpFIBDataSet;
    quDocsActsID2: TFIBIntegerField;
    quDocsActsDATEDOC: TFIBDateField;
    quDocsActsNUMDOC: TFIBStringField;
    quDocsActsIDSKL: TFIBIntegerField;
    quDocsActsSUMIN: TFIBFloatField;
    quDocsActsSUMUCH: TFIBFloatField;
    quDocsActsIACTIVE: TFIBIntegerField;
    quDocsActsOPER: TFIBStringField;
    quDocsActsIdID: TFIBIntegerField;
    quDocsActsIdDATEDOC: TFIBDateField;
    quDocsActsIdNUMDOC: TFIBStringField;
    quDocsActsIdIDSKL: TFIBIntegerField;
    quDocsActsIdSUMIN: TFIBFloatField;
    quDocsActsIdSUMUCH: TFIBFloatField;
    quDocsActsIdIACTIVE: TFIBIntegerField;
    quDocsActsIdOPER: TFIBStringField;
    quDocsActsNAMEMH: TFIBStringField;
    quDocsVnSel: TpFIBDataSet;
    dsDocsVnSel: TDataSource;
    quSpecVnSel: TpFIBDataSet;
    quDocsVnId: TpFIBDataSet;
    quDocsVnSelID: TFIBIntegerField;
    quDocsVnSelDATEDOC: TFIBDateField;
    quDocsVnSelNUMDOC: TFIBStringField;
    quDocsVnSelIDSKL_FROM: TFIBIntegerField;
    quDocsVnSelIDSKL_TO: TFIBIntegerField;
    quDocsVnSelSUMIN: TFIBFloatField;
    quDocsVnSelSUMUCH: TFIBFloatField;
    quDocsVnSelSUMUCH1: TFIBFloatField;
    quDocsVnSelSUMTAR: TFIBFloatField;
    quDocsVnSelPROCNAC: TFIBFloatField;
    quDocsVnSelIACTIVE: TFIBIntegerField;
    quDocsVnSelNAMEMH: TFIBStringField;
    quDocsVnSelNAMEMH1: TFIBStringField;
    quDocsVnIdID: TFIBIntegerField;
    quDocsVnIdDATEDOC: TFIBDateField;
    quDocsVnIdNUMDOC: TFIBStringField;
    quDocsVnIdIDSKL_FROM: TFIBIntegerField;
    quDocsVnIdIDSKL_TO: TFIBIntegerField;
    quDocsVnIdSUMIN: TFIBFloatField;
    quDocsVnIdSUMUCH: TFIBFloatField;
    quDocsVnIdSUMUCH1: TFIBFloatField;
    quDocsVnIdSUMTAR: TFIBFloatField;
    quDocsVnIdPROCNAC: TFIBFloatField;
    quDocsVnIdIACTIVE: TFIBIntegerField;
    quSpecVnSelIDHEAD: TFIBIntegerField;
    quSpecVnSelID: TFIBIntegerField;
    quSpecVnSelNUM: TFIBIntegerField;
    quSpecVnSelIDCARD: TFIBIntegerField;
    quSpecVnSelQUANT: TFIBFloatField;
    quSpecVnSelPRICEIN: TFIBFloatField;
    quSpecVnSelSUMIN: TFIBFloatField;
    quSpecVnSelPRICEUCH: TFIBFloatField;
    quSpecVnSelSUMUCH: TFIBFloatField;
    quSpecVnSelPRICEUCH1: TFIBFloatField;
    quSpecVnSelSUMUCH1: TFIBFloatField;
    quSpecVnSelIDM: TFIBIntegerField;
    quSpecVnSelNAMEC: TFIBStringField;
    quSpecVnSelSM: TFIBStringField;
    quSpecVnSelKM: TFIBFloatField;
    taSpecDocKm: TFloatField;
    taSpecDocPriceUch1: TFloatField;
    taSpecDocSumUch1: TFloatField;
    quSpecAO: TpFIBDataSet;
    quSpecAOIDHEAD: TFIBIntegerField;
    quSpecAOID: TFIBIntegerField;
    quSpecAOIDCARD: TFIBIntegerField;
    quSpecAOQUANT: TFIBFloatField;
    quSpecAOIDM: TFIBIntegerField;
    quSpecAOKM: TFIBFloatField;
    quSpecAOPRICEIN: TFIBFloatField;
    quSpecAOSUMIN: TFIBFloatField;
    quSpecAOPRICEINUCH: TFIBFloatField;
    quSpecAOSUMINUCH: TFIBFloatField;
    quSpecAOTCARD: TFIBSmallIntField;
    quSpecAONAMESHORT: TFIBStringField;
    quSpecAONAME: TFIBStringField;
    quSpecAI: TpFIBDataSet;
    quSpecAIIDHEAD: TFIBIntegerField;
    quSpecAIID: TFIBIntegerField;
    quSpecAIIDCARD: TFIBIntegerField;
    quSpecAIQUANT: TFIBFloatField;
    quSpecAIIDM: TFIBIntegerField;
    quSpecAIKM: TFIBFloatField;
    quSpecAIPRICEIN: TFIBFloatField;
    quSpecAISUMIN: TFIBFloatField;
    quSpecAIPRICEINUCH: TFIBFloatField;
    quSpecAISUMINUCH: TFIBFloatField;
    quSpecAITCARD: TFIBSmallIntField;
    quSpecAINAMESHORT: TFIBStringField;
    quSpecAINAME: TFIBStringField;
    quSpecAIPROCPRICE: TFIBFloatField;
    quDocOutBPrintNAME: TFIBStringField;
    quDocsActsCOMMENT: TFIBStringField;
    quDocsActsIdCOMMENT: TFIBStringField;
    quOperT: TpFIBDataSet;
    quOperTABR: TFIBStringField;
    quOperTID: TFIBIntegerField;
    quOperTDOCTYPE: TFIBSmallIntField;
    quOperTNAMEOPER: TFIBStringField;
    quOperTNAMEDOC: TFIBStringField;
    dsquOperT: TDataSource;
    taDocType: TpFIBDataSet;
    taDocTypeID: TFIBSmallIntField;
    taDocTypeNAMEDOC: TFIBStringField;
    dstaDocType: TDataSource;
    taOperT: TpFIBDataSet;
    taOperTABR: TFIBStringField;
    taOperTID: TFIBIntegerField;
    taOperD: TpFIBDataSet;
    taOperDABR: TFIBStringField;
    taOperDID: TFIBIntegerField;
    taOperDDOCTYPE: TFIBSmallIntField;
    taOperDNAMEOPER: TFIBStringField;
    dstaOperD: TDataSource;
    taSpecDoc1: TClientDataSet;
    taSpecDoc1IType: TSmallintField;
    taSpecDoc1IdHead: TIntegerField;
    taSpecDoc1Num: TIntegerField;
    taSpecDoc1IdCard: TIntegerField;
    taSpecDoc1Quant: TFloatField;
    taSpecDoc1PriceIn: TFloatField;
    taSpecDoc1SumIn: TFloatField;
    taSpecDoc1PriceUch: TFloatField;
    taSpecDoc1SumUch: TFloatField;
    taSpecDoc1IdNDS: TIntegerField;
    taSpecDoc1SumNds: TFloatField;
    taSpecDoc1NameC: TStringField;
    taSpecDoc1SM: TStringField;
    taSpecDoc1IDM: TIntegerField;
    taSpecDoc1Km: TFloatField;
    taSpecDoc1PriceUch1: TFloatField;
    taSpecDoc1SumUch1: TFloatField;
    taSpecDocProcPrice: TFloatField;
    prAddPartOutT: TpFIBStoredProc;
    prAddPartIn1T: TpFIBStoredProc;
    quSpecVnGrPos: TpFIBDataSet;
    FIBIntegerField1: TFIBIntegerField;
    FIBIntegerField2: TFIBIntegerField;
    FIBIntegerField3: TFIBIntegerField;
    FIBIntegerField4: TFIBIntegerField;
    FIBFloatField1: TFIBFloatField;
    FIBFloatField2: TFIBFloatField;
    FIBFloatField3: TFIBFloatField;
    FIBFloatField4: TFIBFloatField;
    FIBFloatField5: TFIBFloatField;
    FIBFloatField6: TFIBFloatField;
    FIBFloatField7: TFIBFloatField;
    FIBIntegerField5: TFIBIntegerField;
    FIBStringField1: TFIBStringField;
    FIBStringField2: TFIBStringField;
    FIBFloatField8: TFIBFloatField;
    prTestSpecPart: TpFIBStoredProc;
    prResetSum: TpFIBStoredProc;
    quCardPO: TpFIBDataSet;
    quCardPOQUANT: TFIBFloatField;
    quCardPORSUM: TFIBFloatField;
    prCalcSumRemn: TpFIBStoredProc;
    quDocsRSel: TpFIBDataSet;
    quDocsRSelID: TFIBIntegerField;
    quDocsRSelDATEDOC: TFIBDateField;
    quDocsRSelNUMDOC: TFIBStringField;
    quDocsRSelDATESF: TFIBDateField;
    quDocsRSelNUMSF: TFIBStringField;
    quDocsRSelIDCLI: TFIBIntegerField;
    quDocsRSelIDSKL: TFIBIntegerField;
    quDocsRSelSUMIN: TFIBFloatField;
    quDocsRSelSUMUCH: TFIBFloatField;
    quDocsRSelSUMTAR: TFIBFloatField;
    quDocsRSelSUMNDS0: TFIBFloatField;
    quDocsRSelSUMNDS1: TFIBFloatField;
    quDocsRSelSUMNDS2: TFIBFloatField;
    quDocsRSelPROCNAC: TFIBFloatField;
    quDocsRSelIACTIVE: TFIBIntegerField;
    quDocsRSelNAMECL: TFIBStringField;
    quDocsRSelNAMEMH: TFIBStringField;
    dsquDocsRSel: TDataSource;
    quDocsRId: TpFIBDataSet;
    quDocsRIdID: TFIBIntegerField;
    quDocsRIdDATEDOC: TFIBDateField;
    quDocsRIdNUMDOC: TFIBStringField;
    quDocsRIdDATESF: TFIBDateField;
    quDocsRIdNUMSF: TFIBStringField;
    quDocsRIdIDCLI: TFIBIntegerField;
    quDocsRIdIDSKL: TFIBIntegerField;
    quDocsRIdSUMIN: TFIBFloatField;
    quDocsRIdSUMUCH: TFIBFloatField;
    quDocsRIdSUMTAR: TFIBFloatField;
    quDocsRIdSUMNDS0: TFIBFloatField;
    quDocsRIdSUMNDS1: TFIBFloatField;
    quDocsRIdSUMNDS2: TFIBFloatField;
    quDocsRIdPROCNAC: TFIBFloatField;
    quDocsRIdIACTIVE: TFIBIntegerField;
    quSpecRSel: TpFIBDataSet;
    quSpecRDay: TpFIBDataSet;
    quSpecRDayIDCARD: TFIBIntegerField;
    quSpecRDayIDM: TFIBIntegerField;
    quSpecRDayKM: TFIBFloatField;
    quSpecRDayQUANT: TFIBFloatField;
    quSpecRDaySUMR: TFIBFloatField;
    quSpecRDayNAME: TFIBStringField;
    quSpecRDayNAMESHORT: TFIBStringField;
    quDocsInvIdSUM3: TFIBFloatField;
    quDocsInvIdSUM31: TFIBFloatField;
    quDocsInvSelSUM3: TFIBFloatField;
    quDocsInvSelSUM31: TFIBFloatField;
    quDocsInvSelSUMDIFINF: TFIBFloatField;
    quSpecInvSUMINR: TFIBFloatField;
    quSpecInvPRICER: TFIBFloatField;
    quSpecInvCSUMINR: TFIBFloatField;
    taHeadDocComment: TStringField;
    taDelCard: TpFIBDataSet;
    taDelCardID: TFIBIntegerField;
    taDelCardSIFR: TFIBIntegerField;
    taDelCardNAME: TFIBStringField;
    taDelCardTTYPE: TFIBIntegerField;
    taDelCardIMESSURE: TFIBIntegerField;
    prRECALCGDS: TpFIBStoredProc;
    trD: TpFIBTransaction;
    quClass: TpFIBDataSet;
    quClassID: TFIBIntegerField;
    quClassNAME: TFIBStringField;
    quClassPARENT: TFIBIntegerField;
    quClassIMESSURE: TFIBIntegerField;
    quClassNAMECL: TFIBStringField;
    quFindPart: TpFIBDataSet;
    quFindPartID: TFIBIntegerField;
    quFindPartIDSTORE: TFIBIntegerField;
    quFindPartIDDOC: TFIBIntegerField;
    quFindPartARTICUL: TFIBIntegerField;
    quFindPartIDCLI: TFIBIntegerField;
    quFindPartDTYPE: TFIBIntegerField;
    quFindPartQPART: TFIBFloatField;
    quFindPartQREMN: TFIBFloatField;
    quFindPartPRICEIN: TFIBFloatField;
    quFindPartIDATE: TFIBIntegerField;
    quFindPartNAMECL: TFIBStringField;
    quMainClass: TpFIBDataSet;
    quMainClassID: TFIBIntegerField;
    quFindPartNAMEMH: TFIBStringField;
    pr_CalcLastPrice: TpFIBStoredProc;
    quMovePer: TpFIBDataSet;
    quMovePerPOSTIN: TFIBFloatField;
    quMovePerPOSTOUT: TFIBFloatField;
    quMovePerVNIN: TFIBFloatField;
    quMovePerVNOUT: TFIBFloatField;
    quMovePerINV: TFIBFloatField;
    quMovePerQREAL: TFIBFloatField;
    quSelInSumIDATE: TFIBIntegerField;
    quSelInSumQPART: TFIBFloatField;
    quSelInSumPRICEIN: TFIBFloatField;
    quSelOutSumIDDATE: TFIBIntegerField;
    quSelOutSumQUANT: TFIBFloatField;
    quSelOutSumPRICEIN: TFIBFloatField;
    quSelOutSumSUMOUT: TFIBFloatField;
    quDocsInvSelCOMMENT: TFIBStringField;
    quDocsInvIdCOMMENT: TFIBStringField;
    quFindSebReal: TpFIBDataSet;
    quFindSebRealIDM: TFIBIntegerField;
    quFindSebRealKM: TFIBFloatField;
    quFindSebRealPRICEIN: TFIBFloatField;
    dsquFindSebReal: TDataSource;
    quFindCl: TpFIBDataSet;
    quFindClID_PARENT: TFIBIntegerField;
    quFindClNAMECL: TFIBStringField;
    quFindMassa: TpFIBDataSet;
    quFindMassaPOUTPUT: TFIBStringField;
    quOperTTPRIB: TFIBSmallIntField;
    quOperTTSPIS: TFIBSmallIntField;
    quFindTSpis: TpFIBDataSet;
    quFindTSpisTSPIS: TFIBSmallIntField;
    quSpecRDaySUMIN: TFIBFloatField;
    quDB: TpFIBDataSet;
    quDBID: TFIBIntegerField;
    quDBNAMEDB: TFIBStringField;
    quDBPATHDB: TFIBStringField;
    dsquDB: TDataSource;
    quClosePartIn: TpFIBQuery;
    trU: TpFIBTransaction;
    quPODoc: TpFIBDataSet;
    quPODocSUMIN: TFIBFloatField;
    quFindCli: TpFIBDataSet;
    quFindCliID: TFIBIntegerField;
    quFindCliNAMECL: TFIBStringField;
    quSpecRSel1: TpFIBDataSet;
    quSpecRSel1IDHEAD: TFIBIntegerField;
    quSpecRSel1ID: TFIBIntegerField;
    quSpecRSel1IDCARD: TFIBIntegerField;
    quSpecRSel1QUANT: TFIBFloatField;
    quSpecRSel1IDM: TFIBIntegerField;
    quSpecRSel1KM: TFIBFloatField;
    quSpecRSel1PRICEIN: TFIBFloatField;
    quSpecRSel1SUMIN: TFIBFloatField;
    quSpecRSel1PRICER: TFIBFloatField;
    quSpecRSel1SUMR: TFIBFloatField;
    quSpecRSel1TCARD: TFIBSmallIntField;
    quSpecRSel1IDNDS: TFIBIntegerField;
    quSpecRSel1SUMNDS: TFIBFloatField;
    quSpecRSel1NAMEC: TFIBStringField;
    quSpecRSel1SM: TFIBStringField;
    quSpecRSel1NAMENDS: TFIBStringField;
    quSpecRSel1OPER: TFIBSmallIntField;
    taCardM: TdxMemData;
    taCardMIdDoc: TIntegerField;
    taCardMTypeD: TIntegerField;
    taCardMDateD: TDateField;
    taCardMsFrom: TStringField;
    taCardMsTo: TStringField;
    taCardMiM: TIntegerField;
    taCardMsM: TStringField;
    taCardMQuant: TFloatField;
    taCardMQRemn: TFloatField;
    taCardMComment: TStringField;
    dstaCardM: TDataSource;
    quTestInput: TpFIBDataSet;
    dsquTestInput: TDataSource;
    quTestInputID: TFIBIntegerField;
    quTestInputDATEDOC: TFIBDateField;
    quTestInputNUMDOC: TFIBStringField;
    quTestInputIDCLI: TFIBIntegerField;
    quTestInputIDSKL: TFIBIntegerField;
    quTestInputSUMIN: TFIBFloatField;
    quTestInputIACTIVE: TFIBIntegerField;
    quTestInputOPER: TFIBStringField;
    quTestInputCOMMENT: TFIBStringField;
    quTestInputNAMEMH: TFIBStringField;
    quDocsRSelIDFROM: TFIBIntegerField;
    quDocsRSelNAMECLFROM: TFIBStringField;
    quDocsRIdIDFROM: TFIBIntegerField;
    quSpecVnSelCATEGORY: TFIBIntegerField;
    quSpecInvCATEGORY: TFIBIntegerField;
    quSpecInvCCATEGORY: TFIBIntegerField;
    quDocsInvSelSUMTARAR: TFIBFloatField;
    quDocsInvSelSUMTARAF: TFIBFloatField;
    quDocsInvSelSUMTARAD: TFIBFloatField;
    quDocsInvIdSUMTARAR: TFIBFloatField;
    quDocsInvIdSUMTARAF: TFIBFloatField;
    quDocsInvIdSUMTARAD: TFIBFloatField;
    taTOReprSumPr: TFloatField;
    taTORepPriceType: TStringField;
    taTORepsSumPr: TStringField;
    quDocsInvSelIDETAL: TFIBSmallIntField;
    quDocsInvIdIDETAL: TFIBSmallIntField;
    quDocsComplIDSKLTO: TFIBIntegerField;
    quDocsComplIDFROM: TFIBIntegerField;
    quDocsComplTOREAL: TFIBSmallIntField;
    quDocsComlRecIDFROM: TFIBIntegerField;
    quDocsComlRecTOREAL: TFIBSmallIntField;
    quDocsComlRecIDSKLTO: TFIBIntegerField;
    teCalcB1: TdxMemData;
    teCalcB1ID: TIntegerField;
    teCalcB1CODEB: TIntegerField;
    teCalcB1NAMEB: TStringField;
    teCalcB1QUANT: TFloatField;
    teCalcB1PRICEOUT: TFloatField;
    teCalcB1SUMOUT: TFloatField;
    teCalcB1IDCARD: TIntegerField;
    teCalcB1NAMEC: TStringField;
    teCalcB1QUANTC: TFloatField;
    teCalcB1PRICEIN: TFloatField;
    teCalcB1SUMIN: TFloatField;
    teCalcB1IM: TIntegerField;
    teCalcB1SM: TStringField;
    teCalcB1SB: TStringField;
    dsteCalcB1: TDataSource;
    quDocsRCard: TpFIBDataSet;
    dsquDocsRCard: TDataSource;
    quSpecRSel2: TpFIBDataSet;
    teCalcB1PRICEIN0: TFloatField;
    teCalcB1SUMIN0: TFloatField;
    quSpecComplPRICEIN0: TFIBFloatField;
    quSpecComplSUMIN0: TFIBFloatField;
    quSpecComplCBPRICEIN0: TFIBFloatField;
    quSpecComplCBSUMIN0: TFIBFloatField;
    quSpecComplCPRICEIN0: TFIBFloatField;
    quSpecComplCSUMIN0: TFIBFloatField;
    quFindSebRealPRICEIN0: TFIBFloatField;
    quSpecRSel1PRICEIN0: TFIBFloatField;
    quSpecRSel1SUMIN0: TFIBFloatField;
    quSpecAOPRICEIN0: TFIBFloatField;
    quSpecAOSUMIN0: TFIBFloatField;
    quSpecAOINDS: TFIBIntegerField;
    quSpecAORNDS: TFIBFloatField;
    quSpecAONAMENDS: TFIBStringField;
    quSpecAIPRICEIN0: TFIBFloatField;
    quSpecAISUMIN0: TFIBFloatField;
    quSpecAIINDS: TFIBIntegerField;
    quSpecAIRNDS: TFIBFloatField;
    quSpecAINAMENDS: TFIBStringField;
    taCalcB1: TdxMemData;
    taCalcB1ID: TIntegerField;
    taCalcB1CODEB: TIntegerField;
    taCalcB1NAMEB: TStringField;
    taCalcB1QUANT: TFloatField;
    taCalcB1PRICEOUT: TFloatField;
    taCalcB1SUMOUT: TFloatField;
    taCalcB1IDCARD: TIntegerField;
    taCalcB1NAMEC: TStringField;
    taCalcB1QUANTC: TFloatField;
    taCalcB1PRICEIN: TFloatField;
    taCalcB1SUMIN: TFloatField;
    taCalcB1IM: TIntegerField;
    taCalcB1SB: TStringField;
    taCalcB1SM: TStringField;
    quSpecInvSUMIN0: TFIBFloatField;
    quSpecInvSUMINFACT0: TFIBFloatField;
    quSpecInvNDSPROC: TFIBFloatField;
    quSpecInvNDSSUM: TFIBFloatField;
    quSpecInvCSUMIN0: TFIBFloatField;
    quSpecInvCSUMINFACT0: TFIBFloatField;
    quSpecInvCNDSPROC: TFIBFloatField;
    quSpecInvCNDSSUM: TFIBFloatField;
    taCalcB1PRICEIN0: TFloatField;
    taCalcB1SUMIN0: TFloatField;
    quSpecInvBCPRICEIN0: TFIBFloatField;
    quSpecInvBCSUMIN0: TFIBFloatField;
    quSpecRSel1SUMOUT0: TFIBFloatField;
    quSpecRSel1SUMNDSOUT: TFIBFloatField;
    taTORepsSumNDS: TStringField;
    quDocOutBPrintNAMESHORT: TFIBStringField;
    quDocsRSelBZTYPE: TFIBSmallIntField;
    quDocsRSelBZSTATUS: TFIBSmallIntField;
    quDocsRSelBZSUMR: TFIBFloatField;
    quDocsRSelBZSUMF: TFIBFloatField;
    quDocsRSelBZSUMS: TFIBFloatField;
    quDocsRSelIEDIT: TFIBSmallIntField;
    quDocsRSelPERSEDIT: TFIBStringField;
    quDocsRSelIPERSEDIT: TFIBIntegerField;
    quSetSync: TpFIBQuery;
    quDocsRIdIEDIT: TFIBSmallIntField;
    quDocsRIdPERSEDIT: TFIBStringField;
    quDocsRIdIPERSEDIT: TFIBIntegerField;
    quGetSync: TpFIBDataSet;
    trSetSync: TpFIBTransaction;
    trGetSync: TpFIBTransaction;
    quGetSyncID: TFIBIntegerField;
    quGetSyncDOCTYPE: TFIBSmallIntField;
    quGetSyncIDSKL: TFIBIntegerField;
    quGetSyncIPERS: TFIBIntegerField;
    quGetSyncTIMEEDIT: TFIBDateTimeField;
    quGetSyncSTYPEEDIT: TFIBStringField;
    quGetSyncIDDOC: TFIBIntegerField;
    quDocsRIdBZTYPE: TFIBSmallIntField;
    quDocsRIdBZSTATUS: TFIBSmallIntField;
    quDocsRIdBZSUMR: TFIBFloatField;
    quDocsRIdBZSUMF: TFIBFloatField;
    quDocsRIdBZSUMS: TFIBFloatField;
    quDocsRSelIDHCB: TFIBIntegerField;
    quSpecRToCB: TpFIBDataSet;
    quSpecRToCBIDHEAD: TFIBIntegerField;
    quSpecRToCBID: TFIBIntegerField;
    quSpecRToCBIDCARD: TFIBIntegerField;
    quSpecRToCBQUANT: TFIBFloatField;
    quSpecRToCBIDM: TFIBIntegerField;
    quSpecRToCBKM: TFIBFloatField;
    quSpecRToCBPRICEIN: TFIBFloatField;
    quSpecRToCBSUMIN: TFIBFloatField;
    quSpecRToCBPRICER: TFIBFloatField;
    quSpecRToCBSUMR: TFIBFloatField;
    quSpecRToCBTCARD: TFIBSmallIntField;
    quSpecRToCBIDNDS: TFIBIntegerField;
    quSpecRToCBSUMNDS: TFIBFloatField;
    quSpecRToCBOPER: TFIBSmallIntField;
    quSpecRToCBPRICEIN0: TFIBFloatField;
    quSpecRToCBSUMIN0: TFIBFloatField;
    quSpecRToCBSUMOUT0: TFIBFloatField;
    quSpecRToCBSUMNDSOUT: TFIBFloatField;
    quSpecRToCBBZQUANTR: TFIBFloatField;
    quSpecRToCBBZQUANTF: TFIBFloatField;
    quSpecRToCBBZQUANTS: TFIBFloatField;
    quSpecRToCBCODEZAK: TFIBIntegerField;
    quSpecRToCBCATEGORY: TFIBIntegerField;
    quSpecRToCBPROC: TFIBFloatField;
    quAlgClass: TpFIBDataSet;
    dsquAlgClass: TDataSource;
    quAlgClassID: TFIBIntegerField;
    quAlgClassNAMECLA: TFIBStringField;
    quMakers: TpFIBDataSet;
    quMakersID: TFIBIntegerField;
    quMakersNAMEM: TFIBStringField;
    quMakersINNM: TFIBStringField;
    quMakersKPPM: TFIBStringField;
    dsquMakers: TDataSource;
    dsquAlgClass1: TDataSource;
    dsquMakers1: TDataSource;
    dsquCliLic: TDataSource;
    quCardsAlg: TpFIBDataSet;
    quCardsAlgID: TFIBIntegerField;
    quCardsAlgPARENT: TFIBIntegerField;
    quCardsAlgNAME: TFIBStringField;
    quCardsAlgTTYPE: TFIBIntegerField;
    quCardsAlgIMESSURE: TFIBIntegerField;
    quCardsAlgINDS: TFIBIntegerField;
    quCardsAlgMINREST: TFIBFloatField;
    quCardsAlgLASTPRICEIN: TFIBFloatField;
    quCardsAlgLASTPRICEOUT: TFIBFloatField;
    quCardsAlgLASTPOST: TFIBIntegerField;
    quCardsAlgIACTIVE: TFIBIntegerField;
    quCardsAlgTCARD: TFIBIntegerField;
    quCardsAlgCATEGORY: TFIBIntegerField;
    quCardsAlgSPISSTORE: TFIBStringField;
    quCardsAlgBB: TFIBFloatField;
    quCardsAlgGG: TFIBFloatField;
    quCardsAlgU1: TFIBFloatField;
    quCardsAlgU2: TFIBFloatField;
    quCardsAlgEE: TFIBFloatField;
    quCardsAlgRCATEGORY: TFIBIntegerField;
    quCardsAlgCOMMENT: TFIBStringField;
    quCardsAlgCTO: TFIBIntegerField;
    quCardsAlgCODEZAK: TFIBIntegerField;
    quCardsAlgALGCLASS: TFIBIntegerField;
    quCardsAlgALGMAKER: TFIBIntegerField;
    quCardsAlgNAMECLA: TFIBStringField;
    quCardsAlgNAMEM: TFIBStringField;
    quCardsAlgINNM: TFIBStringField;
    quCardsAlgKPPM: TFIBStringField;
    quInLnAlg: TpFIBDataSet;
    quInLnAlgIDHEAD: TFIBIntegerField;
    quInLnAlgID: TFIBIntegerField;
    quInLnAlgNUM: TFIBIntegerField;
    quInLnAlgIDCARD: TFIBIntegerField;
    quInLnAlgQUANT: TFIBFloatField;
    quInLnAlgPRICEIN: TFIBFloatField;
    quInLnAlgSUMIN: TFIBFloatField;
    quInLnAlgPRICEUCH: TFIBFloatField;
    quInLnAlgSUMUCH: TFIBFloatField;
    quInLnAlgIDNDS: TFIBIntegerField;
    quInLnAlgSUMNDS: TFIBFloatField;
    quInLnAlgIDM: TFIBIntegerField;
    quInLnAlgKM: TFIBFloatField;
    quInLnAlgPRICE0: TFIBFloatField;
    quInLnAlgSUM0: TFIBFloatField;
    quInLnAlgNDSPROC: TFIBFloatField;
    quInLnAlgDATEDOC: TFIBDateField;
    quInLnAlgNUMDOC: TFIBStringField;
    quInLnAlgIDCLI: TFIBIntegerField;
    quInLnAlgIDSKL: TFIBIntegerField;
    quInLnAlgIACTIVE: TFIBIntegerField;
    quInLnAlgFULLNAMECL: TFIBStringField;
    quInLnAlgINN: TFIBStringField;
    quInLnAlgKPP: TFIBStringField;
    quCliLicMax: TpFIBDataSet;
    quCliLicMaxICLI: TFIBIntegerField;
    quCliLicMaxIDATEB: TFIBIntegerField;
    quCliLicMaxDDATEB: TFIBDateField;
    quCliLicMaxIDATEE: TFIBIntegerField;
    quCliLicMaxDDATEE: TFIBDateField;
    quCliLicMaxSER: TFIBStringField;
    quCliLicMaxSNUM: TFIBStringField;
    quCliLicMaxORGAN: TFIBStringField;
    quCardsAlgVOL: TFIBFloatField;
    quDocsRCard1: TpFIBDataSet;
    quDocsRCard1IDCARD: TFIBIntegerField;
    quDocsRCard1NAME: TFIBStringField;
    quDocsRCard1IDCLI: TFIBIntegerField;
    quDocsRCard1IDSKL: TFIBIntegerField;
    quDocsRCard1MHTO: TFIBStringField;
    quDocsRCard1NAMECL: TFIBStringField;
    quDocsRCard1QUANT: TFIBFloatField;
    quDocsRCard1BZQUANTF: TFIBFloatField;
    quDocsRCard1GRNAME: TFIBStringField;
    quDocsRCard2: TpFIBDataSet;
    quDocsRCard2IDCARD: TFIBIntegerField;
    quDocsRCard2NAME: TFIBStringField;
    quDocsRCard2GRNAME: TFIBStringField;
    quDocsRCard2IDSKL: TFIBIntegerField;
    quDocsRCard2MHTO: TFIBStringField;
    quDocsRCard2QUANT: TFIBFloatField;
    quDocsRCard2BZQUANTF: TFIBFloatField;
    quDocsRCard2NAMESHORT: TFIBStringField;
    quDocsRCli: TpFIBDataSet;
    quDocsRCliIDCLI: TFIBIntegerField;
    quDocsRCliNAMECL: TFIBStringField;
    quDocsRCard2PARENT: TFIBIntegerField;
    quDocsRCliGr: TpFIBDataSet;
    quDocsRCliGrPARENT: TFIBIntegerField;
    quDocsRCliGrIDCLI: TFIBIntegerField;
    quDocsRCliGrBZQUANTF: TFIBFloatField;
    quTara: TpFIBDataSet;
    quTaraID: TFIBIntegerField;
    quTaraPARENT: TFIBIntegerField;
    quTaraNAME: TFIBStringField;
    quTaraTTYPE: TFIBIntegerField;
    quTaraIMESSURE: TFIBIntegerField;
    quTaraINDS: TFIBIntegerField;
    quTaraMINREST: TFIBFloatField;
    quTaraLASTPRICEIN: TFIBFloatField;
    quTaraLASTPRICEOUT: TFIBFloatField;
    quTaraLASTPOST: TFIBIntegerField;
    quTaraIACTIVE: TFIBIntegerField;
    quTaraTCARD: TFIBIntegerField;
    quTaraCATEGORY: TFIBIntegerField;
    quTaraSPISSTORE: TFIBStringField;
    quTaraBB: TFIBFloatField;
    quTaraGG: TFIBFloatField;
    quTaraU1: TFIBFloatField;
    quTaraU2: TFIBFloatField;
    quTaraEE: TFIBFloatField;
    quTaraRCATEGORY: TFIBIntegerField;
    quTaraCOMMENT: TFIBStringField;
    quTaraCTO: TFIBIntegerField;
    quTaraCODEZAK: TFIBIntegerField;
    quTaraALGCLASS: TFIBIntegerField;
    quTaraALGMAKER: TFIBIntegerField;
    quTaraVOL: TFIBFloatField;
    dsquTara: TDataSource;
    trSelTara: TpFIBTransaction;
    quSpecRToCBNAME: TFIBStringField;
    quCliLicMaxID: TFIBIntegerField;
    quCliLic: TpFIBDataSet;
    quCliLicID: TFIBIntegerField;
    quCliLicICLI: TFIBIntegerField;
    quCliLicIDATEB: TFIBIntegerField;
    quCliLicDDATEB: TFIBDateField;
    quCliLicIDATEE: TFIBIntegerField;
    quCliLicDDATEE: TFIBDateField;
    quCliLicSER: TFIBStringField;
    quCliLicSNUM: TFIBStringField;
    quCliLicORGAN: TFIBStringField;
    quAlcoDH: TpFIBDataSet;
    quAlcoDS1: TpFIBDataSet;
    quAlcoDS2: TpFIBDataSet;
    dsquAlcoDH: TDataSource;
    dsquAlcoDS1: TDataSource;
    dsquAlcoDS2: TDataSource;
    quAlcoDHIDORG: TFIBIntegerField;
    quAlcoDHID: TFIBIntegerField;
    quAlcoDHIDATEB: TFIBIntegerField;
    quAlcoDHIDATEE: TFIBIntegerField;
    quAlcoDHDDATEB: TFIBDateField;
    quAlcoDHDDATEE: TFIBDateField;
    quAlcoDHSPERS: TFIBStringField;
    quAlcoDHIACTIVE: TFIBSmallIntField;
    quAlcoDHIT1: TFIBSmallIntField;
    quAlcoDHIT2: TFIBSmallIntField;
    quAlcoDHINUMCORR: TFIBSmallIntField;
    quAlcoDHNAME: TFIBStringField;
    quAlcoDHINN: TFIBStringField;
    quAlcoDS1IDH: TFIBIntegerField;
    quAlcoDS1ID: TFIBIntegerField;
    quAlcoDS1IDEP: TFIBIntegerField;
    quAlcoDS1INUM: TFIBIntegerField;
    quAlcoDS1IVID: TFIBIntegerField;
    quAlcoDS1IPROD: TFIBIntegerField;
    quAlcoDS1RQB: TFIBFloatField;
    quAlcoDS1RQIN1: TFIBFloatField;
    quAlcoDS1RQIN2: TFIBFloatField;
    quAlcoDS1RQIN3: TFIBFloatField;
    quAlcoDS1RQINIT1: TFIBFloatField;
    quAlcoDS1RQIN4: TFIBFloatField;
    quAlcoDS1RQIN5: TFIBFloatField;
    quAlcoDS1RQIN6: TFIBFloatField;
    quAlcoDS1RQINIT: TFIBFloatField;
    quAlcoDS1RQOUT1: TFIBFloatField;
    quAlcoDS1RQOUT2: TFIBFloatField;
    quAlcoDS1RQOUT3: TFIBFloatField;
    quAlcoDS1RQOUT4: TFIBFloatField;
    quAlcoDS1RQOUTIT: TFIBFloatField;
    quAlcoDS1RQE: TFIBFloatField;
    quAlcoDS1NAMESHOP: TFIBStringField;
    quAlcoDS1NAMEVID: TFIBStringField;
    quAlcoDS1NAMEPRODUCER: TFIBStringField;
    quAlcoDS1INN: TFIBStringField;
    quAlcoDS1KPP: TFIBStringField;
    quAlcoDS2IDH: TFIBIntegerField;
    quAlcoDS2ID: TFIBIntegerField;
    quAlcoDS2IDEP: TFIBIntegerField;
    quAlcoDS2IVID: TFIBIntegerField;
    quAlcoDS2IPROD: TFIBIntegerField;
    quAlcoDS2IPOST: TFIBIntegerField;
    quAlcoDS2ILIC: TFIBIntegerField;
    quAlcoDS2DOCDATE: TFIBDateField;
    quAlcoDS2DOCNUM: TFIBStringField;
    quAlcoDS2GTD: TFIBStringField;
    quAlcoDS2RQIN: TFIBFloatField;
    quAlcoDS2NAMESHOP: TFIBStringField;
    quAlcoDS2NAMEPRODUCER: TFIBStringField;
    quAlcoDS2INN: TFIBStringField;
    quAlcoDS2KPP: TFIBStringField;
    quAlcoDS2NAMECLI: TFIBStringField;
    quAlcoDS2INNCLI: TFIBStringField;
    quAlcoDS2KPPCLI: TFIBStringField;
    quAlcoDS2SER: TFIBStringField;
    quAlcoDS2SNUM: TFIBStringField;
    quAlcoDS2DDATEB: TFIBDateField;
    quAlcoDS2DDATEE: TFIBDateField;
    quAlcoDS2ORGAN: TFIBStringField;
    quAlcoDS2NAMEVID: TFIBStringField;
    quDelDS1: TpFIBQuery;
    quDelDS2: TpFIBQuery;
    quFindDH: TpFIBDataSet;
    quFindDHIDORG: TFIBIntegerField;
    quFindDHID: TFIBIntegerField;
    quFindDHIDATEB: TFIBIntegerField;
    quFindDHIDATEE: TFIBIntegerField;
    quFindDHDDATEB: TFIBDateField;
    quFindDHDDATEE: TFIBDateField;
    quFindDHSPERS: TFIBStringField;
    quFindDHIACTIVE: TFIBSmallIntField;
    quFindDHIT1: TFIBSmallIntField;
    quFindDHIT2: TFIBSmallIntField;
    quFindDHINUMCORR: TFIBSmallIntField;
    quAlcoDS2IDDH: TFIBIntegerField;
    quSpecRSel1CATEGORY: TFIBIntegerField;
    quDetPosD: TpFIBDataSet;
    quLog: TpFIBQuery;
    trLog: TpFIBTransaction;
    quDocComplAuto: TpFIBDataSet;
    quDocComplAutoID: TFIBIntegerField;
    quDocComplAutoDATEDOC: TFIBDateField;
    quDocComplAutoNUMDOC: TFIBStringField;
    quDocComplAutoIDSKL: TFIBIntegerField;
    quDocComplAutoSUMIN: TFIBFloatField;
    quDocComplAutoSUMUCH: TFIBFloatField;
    quDocComplAutoSUMTAR: TFIBFloatField;
    quDocComplAutoPROCNAC: TFIBFloatField;
    quDocComplAutoIACTIVE: TFIBIntegerField;
    quDocComplAutoOPER: TFIBStringField;
    quDocComplAutoIDSKLTO: TFIBIntegerField;
    quDocComplAutoIDFROM: TFIBIntegerField;
    quDocComplAutoTOREAL: TFIBSmallIntField;
    quDocComplAutoNAMEMH: TFIBStringField;
    quUPL: TpFIBDataSet;
    dsquUPL: TDataSource;
    quUPLID: TFIBIntegerField;
    quUPLIMH: TFIBIntegerField;
    quUPLNAME: TFIBStringField;
    quUPLIACTIVE: TFIBSmallIntField;
    quUPLNAMEMH: TFIBStringField;
    quUPLMH: TpFIBDataSet;
    dsquUPLMH: TDataSource;
    quUPLMHID: TFIBIntegerField;
    quUPLMHIMH: TFIBIntegerField;
    quUPLMHNAME: TFIBStringField;
    quUPLMHIACTIVE: TFIBSmallIntField;
    quDocsRIdUPL: TFIBIntegerField;
    quDocsRSelUPL: TFIBIntegerField;
    quUPLRec: TpFIBDataSet;
    quUPLRecID: TFIBIntegerField;
    quUPLRecIMH: TFIBIntegerField;
    quUPLRecNAME: TFIBStringField;
    quUPLRecIACTIVE: TFIBSmallIntField;
    quDocsPrice: TpFIBDataSet;
    dsquDocsPrice: TDataSource;
    quDocsPriceID: TFIBIntegerField;
    quDocsPriceIDATE: TFIBIntegerField;
    quDocsPriceDDATE: TFIBDateField;
    quDocsPriceCOMMENT: TFIBStringField;
    quDocPrId: TpFIBDataSet;
    quDocPrIdID: TFIBIntegerField;
    quDocPrIdIDATE: TFIBIntegerField;
    quDocPrIdDDATE: TFIBDateField;
    quDocPrIdCOMMENT: TFIBStringField;
    quSpecPrSel: TpFIBDataSet;
    quSpecPrSelIDH: TFIBIntegerField;
    quSpecPrSelID: TFIBIntegerField;
    quSpecPrSelICODE: TFIBIntegerField;
    quSpecPrSelOLDPRICE: TFIBFloatField;
    quSpecPrSelNEWPRICE: TFIBFloatField;
    quSpecPrSelIM: TFIBIntegerField;
    quSpecPrSelKM: TFIBFloatField;
    quSpecPrSelNAME: TFIBStringField;
    quSpecPrSelNAMESHORT: TFIBStringField;
    quDelSpecPrice: TpFIBQuery;
    quFOldPrice: TpFIBDataSet;
    quFOldPriceNEWPRICE: TFIBFloatField;
    quFPricePP: TpFIBDataSet;
    quFPricePPID: TFIBIntegerField;
    quFPricePPIDSTORE: TFIBIntegerField;
    quFPricePPIDDOC: TFIBIntegerField;
    quFPricePPARTICUL: TFIBIntegerField;
    quFPricePPIDCLI: TFIBIntegerField;
    quFPricePPDTYPE: TFIBIntegerField;
    quFPricePPQPART: TFIBFloatField;
    quFPricePPQREMN: TFIBFloatField;
    quFPricePPPRICEIN: TFIBFloatField;
    quFPricePPPRICEOUT: TFIBFloatField;
    quFPricePPIDATE: TFIBIntegerField;
    quFPricePPPRICEIN0: TFIBFloatField;
    quDBAUTOCLOSE: TFIBSmallIntField;
    quTestInputSUMUCH: TFIBFloatField;
    quInLnAlgGTD: TFIBStringField;
    PRNORMPARTIN: TpFIBStoredProc;
    quRepAkciz: TpFIBDataSet;
    teRepAkciz: TdxMemData;
    teRepAkciziCode: TIntegerField;
    teRepAkcizQuantDoc: TFloatField;
    teRepAkcizNameC: TStringField;
    teRepAkcizSM: TStringField;
    teRepAkcizrSumIn: TFloatField;
    teRepAkcizrSumIn0: TFloatField;
    teRepAkcizrSumOut: TFloatField;
    teRepAkcizrSumOut0: TFloatField;
    teRepAkcizrSumNDSOut: TFloatField;
    quQD: TpFIBDataSet;
    quQDIDCARD: TFIBIntegerField;
    quQDSETIDCARDS: TFIBIntegerField;
    quQDMAXQUANT: TFIBFloatField;
    teRepAkciziCodeDoc: TIntegerField;
    teRepAkcizQuant: TFloatField;
    frteRepAkciz: TfrDBDataSet;
    quQDNAMEC: TFIBStringField;
    quQDIMESSURE: TFIBIntegerField;
    quQDSM: TFIBStringField;
    quRepAkcizIDCARD: TFIBIntegerField;
    quRepAkcizIDM: TFIBIntegerField;
    quRepAkcizKM: TFIBFloatField;
    quRepAkcizQUANT: TFIBFloatField;
    quRepAkcizSUMIN: TFIBFloatField;
    quRepAkcizSUMR: TFIBFloatField;
    quRepAkcizSUMNDS: TFIBFloatField;
    quRepAkcizSUMIN0: TFIBFloatField;
    quRepAkcizSUMOUT0: TFIBFloatField;
    quRepAkcizSUMNDSOUT: TFIBFloatField;
    quRepAkcizQUANTDOC: TFIBFloatField;
    quSpecRSelIDHEAD: TFIBIntegerField;
    quSpecRSelID: TFIBIntegerField;
    quSpecRSelIDCARD: TFIBIntegerField;
    quSpecRSelQUANT: TFIBFloatField;
    quSpecRSelIDM: TFIBIntegerField;
    quSpecRSelKM: TFIBFloatField;
    quSpecRSelPRICEIN0: TFIBFloatField;
    quSpecRSelSUMIN0: TFIBFloatField;
    quSpecRSelPRICEIN: TFIBFloatField;
    quSpecRSelSUMIN: TFIBFloatField;
    quSpecRSelPRICER: TFIBFloatField;
    quSpecRSelSUMR: TFIBFloatField;
    quSpecRSelTCARD: TFIBSmallIntField;
    quSpecRSelIDNDS: TFIBIntegerField;
    quSpecRSelSUMNDS: TFIBFloatField;
    quSpecRSelSUMOUT0: TFIBFloatField;
    quSpecRSelSUMNDSOUT: TFIBFloatField;
    quSpecRSelNAMEC: TFIBStringField;
    quSpecRSelCATEGORY: TFIBIntegerField;
    quSpecRSelSM: TFIBStringField;
    quSpecRSelNAMENDS: TFIBStringField;
    quSpecRSelOPER: TFIBSmallIntField;
    quSpecRSelPROC: TFIBFloatField;
    quSpecRSelOUTNDSSUM: TFIBFloatField;
    quSpecRSelID1: TFIBIntegerField;
    quSpecRSelNAMECTO: TFIBStringField;
    quSpecRSelINDS: TFIBIntegerField;
    quSpecRSelBZQUANTR: TFIBFloatField;
    quSpecRSelBZQUANTF: TFIBFloatField;
    quSpecRSelBZQUANTS: TFIBFloatField;
    quSpecRSelBZQUANTSHOP: TFIBFloatField;
    quSpecRSelQUANTDOC: TFIBFloatField;
    quSpecRSel2IDHEAD: TFIBIntegerField;
    quSpecRSel2ID: TFIBIntegerField;
    quSpecRSel2IDCARD: TFIBIntegerField;
    quSpecRSel2QUANT: TFIBFloatField;
    quSpecRSel2IDM: TFIBIntegerField;
    quSpecRSel2KM: TFIBFloatField;
    quSpecRSel2PRICEIN0: TFIBFloatField;
    quSpecRSel2SUMIN0: TFIBFloatField;
    quSpecRSel2PRICEIN: TFIBFloatField;
    quSpecRSel2SUMIN: TFIBFloatField;
    quSpecRSel2PRICER: TFIBFloatField;
    quSpecRSel2SUMR: TFIBFloatField;
    quSpecRSel2TCARD: TFIBSmallIntField;
    quSpecRSel2IDNDS: TFIBIntegerField;
    quSpecRSel2SUMNDS: TFIBFloatField;
    quSpecRSel2SUMOUT0: TFIBFloatField;
    quSpecRSel2SUMNDSOUT: TFIBFloatField;
    quSpecRSel2NAMEC: TFIBStringField;
    quSpecRSel2CATEGORY: TFIBIntegerField;
    quSpecRSel2SM: TFIBStringField;
    quSpecRSel2NAMENDS: TFIBStringField;
    quSpecRSel2OPER: TFIBSmallIntField;
    quSpecRSel2PROC: TFIBFloatField;
    quSpecRSel2OUTNDSSUM: TFIBFloatField;
    quSpecRSel2ID1: TFIBIntegerField;
    quSpecRSel2NAMECTO: TFIBStringField;
    quSpecRSel2CODE: TFIBIntegerField;
    quSpecRSel2RKREP: TFIBFloatField;
    quSpecRSel2INDS: TFIBIntegerField;
    quSpecRSel2BZQUANTR: TFIBFloatField;
    quSpecRSel2BZQUANTF: TFIBFloatField;
    quSpecRSel2BZQUANTS: TFIBFloatField;
    quSpecRSel2BZQUANTSHOP: TFIBFloatField;
    quSpecRSel2QUANTDOC: TFIBFloatField;
    quSpecRSel2RKREP1: TFIBFloatField;
    quSpecRSel2RKREP2: TFIBFloatField;
    quSpecRSel2NAMEKREP: TFIBStringField;
    quSpecRSel2RVAL: TFIBFloatField;
    quSpecRSel2DVOL: TFIBFloatField;
    quSpecRSel2SERT: TFIBBlobField;
    quSpecRSel2VES: TFIBFloatField;
    quDrv: TpFIBDataSet;
    dsquDrv: TDataSource;
    quDrvID: TFIBIntegerField;
    quDrvNAME1: TFIBStringField;
    quDrvNAME2: TFIBStringField;
    quDrvNAME3: TFIBStringField;
    quDrvSDOC: TFIBStringField;
    quDrvCARNAME: TFIBStringField;
    quDrvCARNUM: TFIBStringField;
    quDrvFIO: TStringField;
    quDocsRSelIDRV: TFIBIntegerField;
    quDrvFIO_CAR: TStringField;
    dsquDrv1: TDataSource;
    quDocsRIdIDRV: TFIBIntegerField;
    quDocsRCardIDCARD: TFIBIntegerField;
    quDocsRCardNAME: TFIBStringField;
    quDocsRCardIDM: TFIBIntegerField;
    quDocsRCardNAMESHORT: TFIBStringField;
    quDocsRCardKM: TFIBFloatField;
    quDocsRCardCATEGORY: TFIBIntegerField;
    quDocsRCardRCATEGORY: TFIBIntegerField;
    quDocsRCardNAMECAT: TFIBStringField;
    quDocsRCardDATEDOC: TFIBDateField;
    quDocsRCardIDCLI: TFIBIntegerField;
    quDocsRCardIDSKL: TFIBIntegerField;
    quDocsRCardIDFROM: TFIBIntegerField;
    quDocsRCardMHTO: TFIBStringField;
    quDocsRCardNAMECL: TFIBStringField;
    quDocsRCardIDATE: TFIBIntegerField;
    quDocsRCardKVART: TFIBSmallIntField;
    quDocsRCardWEEKOFM: TFIBIntegerField;
    quDocsRCardDAYWEEK: TFIBSmallIntField;
    quDocsRCardQUANT: TFIBFloatField;
    quDocsRCardSUMIN: TFIBFloatField;
    quDocsRCardSUMR: TFIBFloatField;
    quDocsRCardQUANTDOC: TFIBFloatField;
    quDocsRCardVOL: TFIBFloatField;
    quDocsRCardALGCLASS: TFIBIntegerField;
    quDocsRCardRKREP: TFIBFloatField;
    quDocsRCardOPER: TIntegerField;
    quDocsRCardRNac: TFloatField;
    quDocsRCardNUMDOC: TFIBStringField;
    quAcloOrg: TpFIBDataSet;
    quAcloOrgID: TFIBIntegerField;
    quAcloOrgPARENT: TFIBIntegerField;
    quAcloOrgITYPE: TFIBIntegerField;
    quAcloOrgNAMEMH: TFIBStringField;
    quAcloOrgDEFPRICE: TFIBIntegerField;
    quAcloOrgEXPNAME: TFIBStringField;
    quAcloOrgNUMSPIS: TFIBSmallIntField;
    quAcloOrgISS: TFIBSmallIntField;
    quAcloOrgGLN: TFIBStringField;
    quAcloOrgPRIOR: TFIBSmallIntField;
    quAcloOrgFULLNAME: TFIBStringField;
    quAcloOrgINN: TFIBStringField;
    quAcloOrgKPP: TFIBStringField;
    quAcloOrgDIR1: TFIBStringField;
    quAcloOrgDIR2: TFIBStringField;
    quAcloOrgDIR3: TFIBStringField;
    quAcloOrgGB1: TFIBStringField;
    quAcloOrgGB2: TFIBStringField;
    quAcloOrgGB3: TFIBStringField;
    quAcloOrgCITY: TFIBStringField;
    quAcloOrgSTREET: TFIBStringField;
    quAcloOrgHOUSE: TFIBStringField;
    quAcloOrgKORP: TFIBStringField;
    quAcloOrgPOSTINDEX: TFIBStringField;
    quAcloOrgPHONE: TFIBStringField;
    quAcloOrgSERLIC: TFIBStringField;
    quAcloOrgNUMLIC: TFIBStringField;
    quAcloOrgORGAN: TFIBStringField;
    quAcloOrgDATEB: TFIBDateField;
    quAcloOrgDATEE: TFIBDateField;
    quAcloOrgIP: TFIBIntegerField;
    quAcloOrgIPREG: TFIBStringField;
    quDocsRIdIDCLITRANS: TFIBIntegerField;
    quSpecRDayQUANTDOC: TFIBFloatField;
    quChangeGr: TpFIBDataSet;
    quChangeGrID: TFIBIntegerField;
    quChangeGrNAME: TFIBStringField;
    quChangeGrFIFO: TFIBSmallIntField;
    quChange: TpFIBDataSet;
    dsquChange: TDataSource;
    quChangeIDGR: TFIBIntegerField;
    quChangeIDCARD: TFIBIntegerField;
    quChangeKOEF: TFIBFloatField;
    quChangePRIOR: TFIBIntegerField;
    quChangeIDATEB: TFIBIntegerField;
    quChangeIDATEE: TFIBIntegerField;
    quChangeIDM: TFIBIntegerField;
    quChangeKM: TFIBFloatField;
    quChangeNAME: TFIBStringField;
    quChangeNAMESHORT: TFIBStringField;
    quChangeEDIZM: TStringField;
    quChangeIMESSURE: TFIBIntegerField;
    quDocsComplNAMEMH1: TFIBStringField;
    quDocsRSelIDCLITRANS: TFIBIntegerField;
    procedure quDocsRCardCalcFields(DataSet: TDataSet);
    procedure quDrvCalcFields(DataSet: TDataSet);
    procedure quChangeCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function fTestPartDoc(rSum:Real;Idh,dType:Integer):Integer;
function fRecalcSumDoc(Idh,dType,ResT:Integer;Memo1:TcxMemo):Real;
procedure prCalcLastPricePos(IdCard,IdSkl,IDate:Integer;Var PriceIn,PriceUch,PriceIn0:Real);
procedure prFind2group(IdCl:Integer;Var S1,S2:String);
function prFindMassaTC(IdCard:Integer):String;
function FindTSpis(sOp:String):Integer;
procedure prSetSync(sDocT,sOp:String;IDH,ISKL:INteger);
procedure prFormQuDocsR;
function prGetOrg(iOrg,iSh:INteger):TOrg;
procedure prGetDetPosDoc(iCode,iDateB,iDateE,iMH:INteger;SName:String;Memo1:TcxMemo;taM:TdxMemData);
procedure prLog(iTD,Idh,iOp,iSkl:INteger);
function prFindUPL(iUpl:INteger):String;
function prFOldUPrice(iCode,iDate:INteger):Real;
function prFPricePP(iCode:INteger):Real;
function prFindReal(IDB,iDate,iDel:INteger):Integer; //���� �� ���������� �� ������ �� ������ ���� �� ����

var
  dmORep: TdmORep;


implementation

uses dmOffice;

{$R *.dfm}

function prFindReal(IDB,iDate,iDel:INteger):Integer; //���� �� ���������� �� ������ �� ������ ���� �� ����
Var bDel:Boolean;
begin
  with dmORep do
  begin
    quTestInput.Active:=False;
    quTestInput.ParamByName('IDB').AsInteger:=IDB;
    quTestInput.ParamByName('DDATE').AsDateTime:=iDate;
    quTestInput.Active:=True;

    bDel:=True;

    Result:=0;

    if quTestInput.RecordCount>0 then Result:=2;

    quTestInput.First;
    while not quTestInput.Eof do
    begin
      if quTestInputIACTIVE.AsInteger=1 then
      begin
        bDel:=False;
        break;
      end;
      quTestInput.Next;
    end;
    quTestInput.First;

    if bDel=False then //���� �������������� ��������� - ������ �� ������
    begin
      Result:=1;
    end else
    begin
      //���� ��������� ���� � ��� ���������� �� ����� � ������� �����
      if iDel=1 then //������� ���� ����
      begin
        if quTestInput.RecordCount>0 then
        begin //���� ���-��
          quTestInput.First;
          while not quTestInput.Eof do quTestInput.Delete;
        end;

        Result:=0;
      end;
    end;
    quTestInput.Active:=False;
  end;
end;


function prFOldUPrice(iCode,iDate:INteger):Real;
begin
  with dmORep do
  begin
    Result:=0;
    quFOldPrice.Active:=False;
    quFOldPrice.ParamByName('ICODE').AsInteger:=iCode;
    quFOldPrice.ParamByName('IDATE').AsInteger:=iDate;
    quFOldPrice.Active:=True;
    quFOldPrice.First;
    if quFOldPrice.RecordCount>0 then Result:=quFOldPriceNEWPRICE.AsFloat;
    quFOldPrice.Active:=False;
  end;
end;

function prFPricePP(iCode:INteger):Real;
begin
  with dmORep do
  begin
    Result:=0;
    quFPricePP.Active:=False;
    quFPricePP.ParamByName('ICODE').AsInteger:=iCode;
    quFPricePP.Active:=True;
    quFPricePP.First;
    if quFPricePP.RecordCount>0 then Result:=quFPricePPPRICEIN.AsFloat;
    quFPricePP.Active:=False;
  end;
end;


function prFindUPL(iUpl:INteger):String;
begin
  with dmORep do
  begin
    Result:='';
    quUPLRec.Active:=False;
    quUPLRec.ParamByName('IUPL').AsInteger:=iUpl;
    quUPLRec.Active:=True;
    if quUPLRec.RecordCount>0 then
    begin
      quUPLRec.First;
      Result:=quUPLRecNAME.AsString;
    end;
    quUPLRec.Active:=False;
  end;
end;

procedure prLog(iTD,Idh,iOp,iSkl:INteger);
Var sOp:String;
begin
  with dmORep do
  begin
    sOp:='';
    if iOp=0 then sOp:='�����';
    if iOp=1 then sOp:='�������������';
    if iOp=2 then sOp:='������������';
    if iOp=3 then sOp:='��������';

    try
      quLog.SQL.Clear;
      quLog.SQL.Add('insert into OF_DOCHIST (ITD,IDHEAD,DATEEDIT,IDPERS,SPERS,IOP,SOP,ISKL)');
      quLog.SQL.Add('values ('+its(iTD)+','+its(Idh)+','''+formatdatetime('dd.mm.yyyy hh:nn',now)+''','+its(Person.Id)+','''+Person.WinName+'/'+Person.Name+''','+its(iOp)+','''+sOp+''','+its(iSkl)+')');
      quLog.ExecQuery;
    except

    end;
{
insert into OF_DOCHIST (ITD,IDHEAD,DOCDATE,DOCNUM,DATEEDIT,IDPERS,SPERS,IOP,SOP)
values (1,11111,'23.07.2012','25004','23.07.2012 18:00',1001,'Admin\Admin',1,'�������������')
}

  end;
end;

procedure prGetDetPosDoc(iCode,iDateB,iDateE,iMH:INteger;SName:String;Memo1:TcxMemo;taM:TdxMemData);
Var rQ:Real;


  procedure prWr1(S:String);
  begin
    if Memo1<>nil then Memo1.Lines.Add(S);
    delay(10);
  end;

begin
  with dmORep do
  begin
    Memo1.Clear;
    prWr1('����� ... ���� ������������ ������.');

    prWr1('  �������.');
    quDetPosD.Active:=False;
    quDetPosD.SelectSQL.Clear;
    quDetPosD.SelectSQL.Add('SELECT sp.IDHEAD, sp.QUANT, sp.PRICEIN, sp.SUMIN, sp.PRICE0, sp.SUM0, sp.KM, dh.DATEDOC, dh.NUMDOC');
    quDetPosD.SelectSQL.Add('FROM OF_DOCSPECIN sp');
    quDetPosD.SelectSQL.Add('left join of_docheadin dh on dh.ID=sp.IDHEAD');
    quDetPosD.SelectSQL.Add('where');
    quDetPosD.SelectSQL.Add('sp.IDCARD='+its(iCode));
    quDetPosD.SelectSQL.Add('and dh.DATEDOC>='''+ds1(iDateB)+'''');
    quDetPosD.SelectSQL.Add('and dh.DATEDOC<'''+ds1(iDateE+1)+'''');
    quDetPosD.SelectSQL.Add('and dh.IDSKL='+its(iMH));
    quDetPosD.SelectSQL.Add('and dh.IACTIVE=1');
    quDetPosD.SelectSQL.Add('');


    quDetPosD.Active:=True;
    quDetPosD.First;
    while not quDetPosD.Eof do
    begin
      taM.Append;
      taM.FieldByName('ITypeD').AsInteger:=1; //�������
      taM.FieldByName('IdDoc').AsInteger:=quDetPosD.FieldByName('IDHEAD').AsInteger;
      taM.FieldByName('NumDoc').AsString:=quDetPosD.FieldByName('NUMDOC').AsString;
      taM.FieldByName('IDateD').AsInteger:=Trunc(quDetPosD.FieldByName('DATEDOC').AsDateTime);
      taM.FieldByName('DateDoc').AsDateTime:=quDetPosD.FieldByName('DATEDOC').AsDateTime;
      taM.FieldByName('QuantIn').AsFloat:=quDetPosD.FieldByName('QUANT').AsFloat*quDetPosD.FieldByName('KM').AsFloat;
      taM.FieldByName('QuantOut').AsFloat:=0;
      taM.FieldByName('PriceIn0').AsFloat:=quDetPosD.FieldByName('PRICE0').AsFloat;
      taM.FieldByName('SumIn0').AsFloat:=quDetPosD.FieldByName('SUM0').AsFloat;
      taM.FieldByName('PriceIn').AsFloat:=quDetPosD.FieldByName('PRICEIN').AsFloat;
      taM.FieldByName('SumIn').AsFloat:=quDetPosD.FieldByName('SUMIN').AsFloat;
      taM.FieldByName('PriceOut').AsFloat:=0;
      taM.FieldByName('SumOut').AsFloat:=0;
      taM.FieldByName('PriceOut0').AsFloat:=0;
      taM.FieldByName('SumOut0').AsFloat:=0;
      taM.Post;

      quDetPosD.Next;
    end;
    quDetPosD.Active:=False;


    prWr1('  �� ������.');
    quDetPosD.Active:=False;
    quDetPosD.SelectSQL.Clear;
    quDetPosD.SelectSQL.Add('SELECT sp.IDHEAD, sp.QUANT, sp.PRICEIN, sp.SUMIN, sp.KM, dh.DATEDOC, dh.NUMDOC');
    quDetPosD.SelectSQL.Add('FROM OF_DOCSPECVN sp');
    quDetPosD.SelectSQL.Add('left join of_docheadvn dh on dh.ID=sp.IDHEAD');
    quDetPosD.SelectSQL.Add('where');
    quDetPosD.SelectSQL.Add('sp.IDCARD='+its(iCode));
    quDetPosD.SelectSQL.Add('and dh.DATEDOC>='''+ds1(iDateB)+'''');
    quDetPosD.SelectSQL.Add('and dh.DATEDOC<'''+ds1(iDateE+1)+'''');
    quDetPosD.SelectSQL.Add('and dh.IDSKL_TO='+its(iMH));
    quDetPosD.SelectSQL.Add('and dh.IACTIVE=1');
    quDetPosD.SelectSQL.Add('');

    quDetPosD.Active:=True;
    quDetPosD.First;
    while not quDetPosD.Eof do
    begin
      taM.Append;
      taM.FieldByName('ITypeD').AsInteger:=4;
      taM.FieldByName('IdDoc').AsInteger:=quDetPosD.FieldByName('IDHEAD').AsInteger;
      taM.FieldByName('NumDoc').AsString:=quDetPosD.FieldByName('NUMDOC').AsString;
      taM.FieldByName('IDateD').AsInteger:=Trunc(quDetPosD.FieldByName('DATEDOC').AsDateTime);
      taM.FieldByName('DateDoc').AsDateTime:=quDetPosD.FieldByName('DATEDOC').AsDateTime;
      taM.FieldByName('QuantIn').AsFloat:=quDetPosD.FieldByName('QUANT').AsFloat*quDetPosD.FieldByName('KM').AsFloat;
      taM.FieldByName('QuantOut').AsFloat:=0;
      taM.FieldByName('PriceIn0').AsFloat:=quDetPosD.FieldByName('PRICEIN').AsFloat;
      taM.FieldByName('SumIn0').AsFloat:=quDetPosD.FieldByName('SUMIN').AsFloat;
      taM.FieldByName('PriceIn').AsFloat:=quDetPosD.FieldByName('PRICEIN').AsFloat;
      taM.FieldByName('SumIn').AsFloat:=quDetPosD.FieldByName('SUMIN').AsFloat;
      taM.FieldByName('PriceOut').AsFloat:=0;
      taM.FieldByName('SumOut').AsFloat:=0;
      taM.FieldByName('PriceOut0').AsFloat:=0;
      taM.FieldByName('SumOut0').AsFloat:=0;
      taM.Post;

      quDetPosD.Next;
    end;
    quDetPosD.Active:=False;


    prWr1('  ���� ����������� ������.');
    quDetPosD.Active:=False;
    quDetPosD.SelectSQL.Clear;
    quDetPosD.SelectSQL.Add('SELECT sp.IDHEAD, sp.QUANT, sp.PRICEIN, sp.SUMIN, sp.PRICEIN0, sp.SUMIN0, sp.KM, dh.DATEDOC, dh.NUMDOC');
    quDetPosD.SelectSQL.Add('FROM OF_DOCSPECACTSIN sp');
    quDetPosD.SelectSQL.Add('left join of_docheadacts dh on dh.ID=sp.IDHEAD');
    quDetPosD.SelectSQL.Add('where');
    quDetPosD.SelectSQL.Add('sp.IDCARD='+its(iCode));
    quDetPosD.SelectSQL.Add('and dh.DATEDOC>='''+ds1(iDateB)+'''');
    quDetPosD.SelectSQL.Add('and dh.DATEDOC<'''+ds1(iDateE+1)+'''');
    quDetPosD.SelectSQL.Add('and dh.IDSKL='+its(iMH));
    quDetPosD.SelectSQL.Add('and dh.IACTIVE=1');
    quDetPosD.SelectSQL.Add('');

    quDetPosD.Active:=True;
    quDetPosD.First;
    while not quDetPosD.Eof do
    begin
      taM.Append;
      taM.FieldByName('ITypeD').AsInteger:=5;
      taM.FieldByName('IdDoc').AsInteger:=quDetPosD.FieldByName('IDHEAD').AsInteger;
      taM.FieldByName('NumDoc').AsString:=quDetPosD.FieldByName('NUMDOC').AsString;
      taM.FieldByName('IDateD').AsInteger:=Trunc(quDetPosD.FieldByName('DATEDOC').AsDateTime);
      taM.FieldByName('DateDoc').AsDateTime:=quDetPosD.FieldByName('DATEDOC').AsDateTime;
      taM.FieldByName('QuantIn').AsFloat:=quDetPosD.FieldByName('QUANT').AsFloat*quDetPosD.FieldByName('KM').AsFloat;
      taM.FieldByName('QuantOut').AsFloat:=0;
      taM.FieldByName('PriceIn0').AsFloat:=quDetPosD.FieldByName('PRICEIN0').AsFloat;
      taM.FieldByName('SumIn0').AsFloat:=quDetPosD.FieldByName('SUMIN0').AsFloat;
      taM.FieldByName('PriceIn').AsFloat:=quDetPosD.FieldByName('PRICEIN').AsFloat;
      taM.FieldByName('SumIn').AsFloat:=quDetPosD.FieldByName('SUMIN').AsFloat;
      taM.FieldByName('PriceOut').AsFloat:=0;
      taM.FieldByName('SumOut').AsFloat:=0;
      taM.FieldByName('PriceOut0').AsFloat:=0;
      taM.FieldByName('SumOut0').AsFloat:=0;
      taM.Post;

      quDetPosD.Next;
    end;
    quDetPosD.Active:=False;

    prWr1('  ������������ ������.');
    quDetPosD.Active:=False;
    quDetPosD.SelectSQL.Clear;
    quDetPosD.SelectSQL.Add('SELECT sp.IDHEAD, sp.QUANT, sp.PRICEIN, sp.SUMIN, sp.PRICEIN0, sp.SUMIN0, sp.KM, dh.DATEDOC, dh.NUMDOC');
    quDetPosD.SelectSQL.Add('FROM OF_DOCSPECCOMPLB sp');
    quDetPosD.SelectSQL.Add('left join of_docheadcompl dh on dh.ID=sp.IDHEAD');
    quDetPosD.SelectSQL.Add('where');
    quDetPosD.SelectSQL.Add('sp.IDCARD='+its(iCode));
    quDetPosD.SelectSQL.Add('and dh.DATEDOC>='''+ds1(iDateB)+'''');
    quDetPosD.SelectSQL.Add('and dh.DATEDOC<'''+ds1(iDateE+1)+'''');
    quDetPosD.SelectSQL.Add('and dh.IDSKL='+its(iMH));
    quDetPosD.SelectSQL.Add('and dh.IACTIVE=1');
    quDetPosD.SelectSQL.Add('');

    quDetPosD.Active:=True;
    quDetPosD.First;
    while not quDetPosD.Eof do
    begin
      taM.Append;
      taM.FieldByName('ITypeD').AsInteger:=6;
      taM.FieldByName('IdDoc').AsInteger:=quDetPosD.FieldByName('IDHEAD').AsInteger;
      taM.FieldByName('NumDoc').AsString:=quDetPosD.FieldByName('NUMDOC').AsString;
      taM.FieldByName('IDateD').AsInteger:=Trunc(quDetPosD.FieldByName('DATEDOC').AsDateTime);
      taM.FieldByName('DateDoc').AsDateTime:=quDetPosD.FieldByName('DATEDOC').AsDateTime;
      taM.FieldByName('QuantIn').AsFloat:=quDetPosD.FieldByName('QUANT').AsFloat*quDetPosD.FieldByName('KM').AsFloat;
      taM.FieldByName('QuantOut').AsFloat:=0;
      taM.FieldByName('PriceIn0').AsFloat:=quDetPosD.FieldByName('PRICEIN0').AsFloat;
      taM.FieldByName('SumIn0').AsFloat:=quDetPosD.FieldByName('SUMIN0').AsFloat;
      taM.FieldByName('PriceIn').AsFloat:=quDetPosD.FieldByName('PRICEIN').AsFloat;
      taM.FieldByName('SumIn').AsFloat:=quDetPosD.FieldByName('SUMIN').AsFloat;
      taM.FieldByName('PriceOut').AsFloat:=0;
      taM.FieldByName('SumOut').AsFloat:=0;
      taM.FieldByName('PriceOut0').AsFloat:=0;
      taM.FieldByName('SumOut0').AsFloat:=0;
      taM.Post;

      quDetPosD.Next;
    end;
    quDetPosD.Active:=False;


    prWr1('  ��������������.');
    quDetPosD.Active:=False;
    quDetPosD.SelectSQL.Clear;
    quDetPosD.SelectSQL.Add('SELECT sp.IDHEAD, sp.QUANTFACT, sp.SUMINFACT, sp.SUMINFACT0, sp.KM, dh.DATEDOC, dh.NUMDOC');
    quDetPosD.SelectSQL.Add('FROM of_docspecinvc sp');
    quDetPosD.SelectSQL.Add('left join of_docheadinv dh on dh.ID=sp.IDHEAD');
    quDetPosD.SelectSQL.Add('where');
    quDetPosD.SelectSQL.Add('sp.IDCARD='+its(iCode));
    quDetPosD.SelectSQL.Add('and dh.DATEDOC>='''+ds1(iDateB)+'''');
    quDetPosD.SelectSQL.Add('and dh.DATEDOC<'''+ds1(iDateE+1)+'''');
    quDetPosD.SelectSQL.Add('and dh.IDSKL='+its(iMH));
    quDetPosD.SelectSQL.Add('and dh.IACTIVE=1');
    quDetPosD.SelectSQL.Add('');

    quDetPosD.Active:=True;
    quDetPosD.First;
    while not quDetPosD.Eof do
    begin
      rQ:=quDetPosD.FieldByName('QUANTFACT').AsFloat*quDetPosD.FieldByName('KM').AsFloat;

      taM.Append;
      taM.FieldByName('ITypeD').AsInteger:=3;
      taM.FieldByName('IdDoc').AsInteger:=quDetPosD.FieldByName('IDHEAD').AsInteger;
      taM.FieldByName('NumDoc').AsString:=quDetPosD.FieldByName('NUMDOC').AsString;
      taM.FieldByName('IDateD').AsInteger:=Trunc(quDetPosD.FieldByName('DATEDOC').AsDateTime);
      taM.FieldByName('DateDoc').AsDateTime:=quDetPosD.FieldByName('DATEDOC').AsDateTime;
      taM.FieldByName('QuantIn').AsFloat:=rQ;
      taM.FieldByName('QuantOut').AsFloat:=0;

      taM.FieldByName('PriceOut0').AsFloat:=0;
      taM.FieldByName('PriceOut').AsFloat:=0;
      taM.FieldByName('SumOut0').AsFloat:=0;
      taM.FieldByName('SumOut').AsFloat:=0;

      if abs(rQ)>0.001 then
      begin
        taM.FieldByName('PriceIn0').AsFloat:=rv(quDetPosD.FieldByName('SUMINFACT0').AsFloat/rQ);
        taM.FieldByName('PriceIn').AsFloat:=rv(quDetPosD.FieldByName('SUMINFACT').AsFloat/rQ);
      end else
      begin
        taM.FieldByName('PriceIn0').AsFloat:=0;
        taM.FieldByName('PriceIn').AsFloat:=0;
      end;

      taM.FieldByName('SumIn0').AsFloat:=quDetPosD.FieldByName('SUMINFACT0').AsFloat;
      taM.FieldByName('SumIn').AsFloat:=quDetPosD.FieldByName('SUMINFACT').AsFloat;
      taM.Post;

      quDetPosD.Next;
    end;
    quDetPosD.Active:=False;


    prWr1('  ����������.');
    quDetPosD.Active:=False;
    quDetPosD.SelectSQL.Clear;
    quDetPosD.SelectSQL.Add('SELECT sp.IDHEAD, sp.QUANT, sp.SUMIN, sp.SUMIN0, sp.KM, dh.DATEDOC, dh.NUMDOC');
    quDetPosD.SelectSQL.Add('FROM of_docspecoutc sp');
    quDetPosD.SelectSQL.Add('left join of_docheadoutb dh on dh.ID=sp.IDHEAD');
    quDetPosD.SelectSQL.Add('where');
    quDetPosD.SelectSQL.Add('sp.ARTICUL='+its(iCode));
    quDetPosD.SelectSQL.Add('and dh.DATEDOC>='''+ds1(iDateB)+'''');
    quDetPosD.SelectSQL.Add('and dh.DATEDOC<'''+ds1(iDateE+1)+'''');
    quDetPosD.SelectSQL.Add('and dh.IDSKL='+its(iMH));
    quDetPosD.SelectSQL.Add('and dh.IACTIVE=1');
    quDetPosD.SelectSQL.Add('');

    quDetPosD.Active:=True;
    quDetPosD.First;
    while not quDetPosD.Eof do
    begin
      rQ:=quDetPosD.FieldByName('QUANT').AsFloat*quDetPosD.FieldByName('KM').AsFloat;

      taM.Append;
      taM.FieldByName('ITypeD').AsInteger:=2;
      taM.FieldByName('IdDoc').AsInteger:=quDetPosD.FieldByName('IDHEAD').AsInteger;
      taM.FieldByName('NumDoc').AsString:=quDetPosD.FieldByName('NUMDOC').AsString;
      taM.FieldByName('IDateD').AsInteger:=Trunc(quDetPosD.FieldByName('DATEDOC').AsDateTime);
      taM.FieldByName('DateDoc').AsDateTime:=quDetPosD.FieldByName('DATEDOC').AsDateTime;
      taM.FieldByName('QuantIn').AsFloat:=0;
      taM.FieldByName('QuantOut').AsFloat:=rQ;

      taM.FieldByName('PriceIn0').AsFloat:=0;
      taM.FieldByName('PriceIn').AsFloat:=0;
      taM.FieldByName('SumIn0').AsFloat:=0;
      taM.FieldByName('SumIn').AsFloat:=0;

      if abs(rQ)>0.001 then
      begin
        taM.FieldByName('PriceOut0').AsFloat:=rv(quDetPosD.FieldByName('SUMIN0').AsFloat/rQ);
        taM.FieldByName('PriceOut').AsFloat:=rv(quDetPosD.FieldByName('SUMIN').AsFloat/rQ);
      end else
      begin
        taM.FieldByName('PriceOut0').AsFloat:=0;
        taM.FieldByName('PriceOut').AsFloat:=0;
      end;

      taM.FieldByName('SumOut0').AsFloat:=quDetPosD.FieldByName('SUMIN0').AsFloat;
      taM.FieldByName('SumOut').AsFloat:=quDetPosD.FieldByName('SUMIN').AsFloat;
      taM.Post;

      quDetPosD.Next;
    end;
    quDetPosD.Active:=False;


    prWr1('  ���������� �� �������');
    quDetPosD.Active:=False;
    quDetPosD.SelectSQL.Clear;
    quDetPosD.SelectSQL.Add('SELECT sp.IDHEAD, sp.QUANT, sp.PRICEIN, sp.SUMIN, sp.PRICEIN0, sp.SUMIN0, sp.KM, dh.DATEDOC, dh.NUMDOC');
    quDetPosD.SelectSQL.Add('FROM of_docspecoutr sp');
    quDetPosD.SelectSQL.Add('left join of_docheadoutr dh on dh.ID=sp.IDHEAD');
    quDetPosD.SelectSQL.Add('where');
    quDetPosD.SelectSQL.Add('sp.IDCARD='+its(iCode));
    quDetPosD.SelectSQL.Add('and dh.DATEDOC>='''+ds1(iDateB)+'''');
    quDetPosD.SelectSQL.Add('and dh.DATEDOC<'''+ds1(iDateE+1)+'''');
    quDetPosD.SelectSQL.Add('and dh.IDSKL='+its(iMH));
    quDetPosD.SelectSQL.Add('and dh.IACTIVE=1');
    quDetPosD.SelectSQL.Add('');

    quDetPosD.Active:=True;
    quDetPosD.First;
    while not quDetPosD.Eof do
    begin
      rQ:=quDetPosD.FieldByName('QUANT').AsFloat*quDetPosD.FieldByName('KM').AsFloat;

      taM.Append;
      taM.FieldByName('ITypeD').AsInteger:=8;
      taM.FieldByName('IdDoc').AsInteger:=quDetPosD.FieldByName('IDHEAD').AsInteger;
      taM.FieldByName('NumDoc').AsString:=quDetPosD.FieldByName('NUMDOC').AsString;
      taM.FieldByName('IDateD').AsInteger:=Trunc(quDetPosD.FieldByName('DATEDOC').AsDateTime);
      taM.FieldByName('DateDoc').AsDateTime:=quDetPosD.FieldByName('DATEDOC').AsDateTime;
      taM.FieldByName('QuantIn').AsFloat:=0;
      taM.FieldByName('QuantOut').AsFloat:=rQ;

      taM.FieldByName('PriceIn0').AsFloat:=0;
      taM.FieldByName('PriceIn').AsFloat:=0;
      taM.FieldByName('SumIn0').AsFloat:=0;
      taM.FieldByName('SumIn').AsFloat:=0;

      taM.FieldByName('PriceOut0').AsFloat:=quDetPosD.FieldByName('PRICEIN0').AsFloat;
      taM.FieldByName('PriceOut').AsFloat:=quDetPosD.FieldByName('PRICEIN').AsFloat;
      taM.FieldByName('SumOut0').AsFloat:=quDetPosD.FieldByName('SUMIN0').AsFloat;
      taM.FieldByName('SumOut').AsFloat:=quDetPosD.FieldByName('SUMIN').AsFloat;
      taM.Post;

      quDetPosD.Next;
    end;
    quDetPosD.Active:=False;

    prWr1('  ������� ����������');
    quDetPosD.Active:=False;
    quDetPosD.SelectSQL.Clear;
    quDetPosD.SelectSQL.Add('SELECT sp.IDHEAD, sp.QUANT, sp.PRICEIN, sp.SUMIN, sp.PRICEIN0, sp.SUMIN0, dh.DATEDOC, dh.NUMDOC');
    quDetPosD.SelectSQL.Add('FROM of_docspecout sp');
    quDetPosD.SelectSQL.Add('left join of_docheadout dh on dh.ID=sp.IDHEAD');
    quDetPosD.SelectSQL.Add('where');
    quDetPosD.SelectSQL.Add('sp.IDCARD='+its(iCode));
    quDetPosD.SelectSQL.Add('and dh.DATEDOC>='''+ds1(iDateB)+'''');
    quDetPosD.SelectSQL.Add('and dh.DATEDOC<'''+ds1(iDateE+1)+'''');
    quDetPosD.SelectSQL.Add('and dh.IDSKL='+its(iMH));
    quDetPosD.SelectSQL.Add('and dh.IACTIVE=1');
    quDetPosD.SelectSQL.Add('');

    quDetPosD.Active:=True;
    quDetPosD.First;
    while not quDetPosD.Eof do
    begin
      rQ:=quDetPosD.FieldByName('QUANT').AsFloat;

      taM.Append;
      taM.FieldByName('ITypeD').AsInteger:=7;
      taM.FieldByName('IdDoc').AsInteger:=quDetPosD.FieldByName('IDHEAD').AsInteger;
      taM.FieldByName('NumDoc').AsString:=quDetPosD.FieldByName('NUMDOC').AsString;
      taM.FieldByName('IDateD').AsInteger:=Trunc(quDetPosD.FieldByName('DATEDOC').AsDateTime);
      taM.FieldByName('DateDoc').AsDateTime:=quDetPosD.FieldByName('DATEDOC').AsDateTime;
      taM.FieldByName('QuantIn').AsFloat:=0;
      taM.FieldByName('QuantOut').AsFloat:=rQ;

      taM.FieldByName('PriceIn0').AsFloat:=0;
      taM.FieldByName('PriceIn').AsFloat:=0;
      taM.FieldByName('SumIn0').AsFloat:=0;
      taM.FieldByName('SumIn').AsFloat:=0;

      taM.FieldByName('PriceOut0').AsFloat:=quDetPosD.FieldByName('PRICEIN0').AsFloat;
      taM.FieldByName('PriceOut').AsFloat:=quDetPosD.FieldByName('PRICEIN').AsFloat;
      taM.FieldByName('SumOut0').AsFloat:=quDetPosD.FieldByName('SUMIN0').AsFloat;
      taM.FieldByName('SumOut').AsFloat:=quDetPosD.FieldByName('SUMIN').AsFloat;
      taM.Post;

      quDetPosD.Next;
    end;
    quDetPosD.Active:=False;


    prWr1('  �� ������.');
    quDetPosD.Active:=False;
    quDetPosD.SelectSQL.Clear;
    quDetPosD.SelectSQL.Add('SELECT sp.IDHEAD, sp.QUANT, sp.PRICEIN, sp.SUMIN, sp.KM, dh.DATEDOC, dh.NUMDOC');
    quDetPosD.SelectSQL.Add('FROM OF_DOCSPECVN sp');
    quDetPosD.SelectSQL.Add('left join of_docheadvn dh on dh.ID=sp.IDHEAD');
    quDetPosD.SelectSQL.Add('where');
    quDetPosD.SelectSQL.Add('sp.IDCARD='+its(iCode));
    quDetPosD.SelectSQL.Add('and dh.DATEDOC>='''+ds1(iDateB)+'''');
    quDetPosD.SelectSQL.Add('and dh.DATEDOC<'''+ds1(iDateE+1)+'''');
    quDetPosD.SelectSQL.Add('and dh.IDSKL_FROM='+its(iMH));
    quDetPosD.SelectSQL.Add('and dh.IACTIVE=1');
    quDetPosD.SelectSQL.Add('');

    quDetPosD.Active:=True;
    quDetPosD.First;
    while not quDetPosD.Eof do
    begin
      taM.Append;
      taM.FieldByName('ITypeD').AsInteger:=4;
      taM.FieldByName('IdDoc').AsInteger:=quDetPosD.FieldByName('IDHEAD').AsInteger;
      taM.FieldByName('NumDoc').AsString:=quDetPosD.FieldByName('NUMDOC').AsString;
      taM.FieldByName('IDateD').AsInteger:=Trunc(quDetPosD.FieldByName('DATEDOC').AsDateTime);
      taM.FieldByName('DateDoc').AsDateTime:=quDetPosD.FieldByName('DATEDOC').AsDateTime;
      taM.FieldByName('QuantIn').AsFloat:=0;
      taM.FieldByName('QuantOut').AsFloat:=quDetPosD.FieldByName('QUANT').AsFloat*quDetPosD.FieldByName('KM').AsFloat;
      taM.FieldByName('PriceOut0').AsFloat:=quDetPosD.FieldByName('PRICEIN').AsFloat;
      taM.FieldByName('SumOut0').AsFloat:=quDetPosD.FieldByName('SUMIN').AsFloat;
      taM.FieldByName('PriceOut').AsFloat:=quDetPosD.FieldByName('PRICEIN').AsFloat;
      taM.FieldByName('SumOut').AsFloat:=quDetPosD.FieldByName('SUMIN').AsFloat;
      taM.FieldByName('PriceIn').AsFloat:=0;
      taM.FieldByName('SumIn').AsFloat:=0;
      taM.FieldByName('PriceIn0').AsFloat:=0;
      taM.FieldByName('SumIn0').AsFloat:=0;
      taM.Post;

      quDetPosD.Next;
    end;
    quDetPosD.Active:=False;

    prWr1('  ���� ����������� ������.');
    quDetPosD.Active:=False;
    quDetPosD.SelectSQL.Clear;
    quDetPosD.SelectSQL.Add('SELECT sp.IDHEAD, sp.QUANT, sp.PRICEIN, sp.SUMIN, sp.PRICEIN0, sp.SUMIN0, sp.KM, dh.DATEDOC, dh.NUMDOC');
    quDetPosD.SelectSQL.Add('FROM OF_DOCSPECACTSOUT sp');
    quDetPosD.SelectSQL.Add('left join of_docheadacts dh on dh.ID=sp.IDHEAD');
    quDetPosD.SelectSQL.Add('where');
    quDetPosD.SelectSQL.Add('sp.IDCARD='+its(iCode));
    quDetPosD.SelectSQL.Add('and dh.DATEDOC>='''+ds1(iDateB)+'''');
    quDetPosD.SelectSQL.Add('and dh.DATEDOC<'''+ds1(iDateE+1)+'''');
    quDetPosD.SelectSQL.Add('and dh.IDSKL='+its(iMH));
    quDetPosD.SelectSQL.Add('and dh.IACTIVE=1');
    quDetPosD.SelectSQL.Add('');

    quDetPosD.Active:=True;
    quDetPosD.First;
    while not quDetPosD.Eof do
    begin
      taM.Append;
      taM.FieldByName('ITypeD').AsInteger:=5;
      taM.FieldByName('IdDoc').AsInteger:=quDetPosD.FieldByName('IDHEAD').AsInteger;
      taM.FieldByName('NumDoc').AsString:=quDetPosD.FieldByName('NUMDOC').AsString;
      taM.FieldByName('IDateD').AsInteger:=Trunc(quDetPosD.FieldByName('DATEDOC').AsDateTime);
      taM.FieldByName('DateDoc').AsDateTime:=quDetPosD.FieldByName('DATEDOC').AsDateTime;
      taM.FieldByName('QuantOut').AsFloat:=quDetPosD.FieldByName('QUANT').AsFloat*quDetPosD.FieldByName('KM').AsFloat;
      taM.FieldByName('QuantIn').AsFloat:=0;
      taM.FieldByName('PriceOut0').AsFloat:=quDetPosD.FieldByName('PRICEIN0').AsFloat;
      taM.FieldByName('SumOut0').AsFloat:=quDetPosD.FieldByName('SUMIN0').AsFloat;
      taM.FieldByName('PriceOut').AsFloat:=quDetPosD.FieldByName('PRICEIN').AsFloat;
      taM.FieldByName('SumOut').AsFloat:=quDetPosD.FieldByName('SUMIN').AsFloat;
      taM.FieldByName('PriceIn').AsFloat:=0;
      taM.FieldByName('SumIn').AsFloat:=0;
      taM.FieldByName('PriceIn0').AsFloat:=0;
      taM.FieldByName('SumIn0').AsFloat:=0;
      taM.Post;

      quDetPosD.Next;
    end;
    quDetPosD.Active:=False;


    prWr1('  ������������ ������.');
    quDetPosD.Active:=False;
    quDetPosD.SelectSQL.Clear;
    quDetPosD.SelectSQL.Add('SELECT sp.IDHEAD, sp.QUANT, sp.PRICEIN, sp.SUMIN, sp.PRICEIN0, sp.SUMIN0, sp.KM, dh.DATEDOC, dh.NUMDOC');
    quDetPosD.SelectSQL.Add('FROM of_docspeccomplc sp');
    quDetPosD.SelectSQL.Add('left join of_docheadcompl dh on dh.ID=sp.IDHEAD');
    quDetPosD.SelectSQL.Add('where');
    quDetPosD.SelectSQL.Add('sp.IDCARD='+its(iCode));
    quDetPosD.SelectSQL.Add('and dh.DATEDOC>='''+ds1(iDateB)+'''');
    quDetPosD.SelectSQL.Add('and dh.DATEDOC<'''+ds1(iDateE+1)+'''');
    quDetPosD.SelectSQL.Add('and dh.IDSKL='+its(iMH));
    quDetPosD.SelectSQL.Add('and dh.IACTIVE=1');
    quDetPosD.SelectSQL.Add('');

    quDetPosD.Active:=True;
    quDetPosD.First;
    while not quDetPosD.Eof do
    begin
      taM.Append;
      taM.FieldByName('ITypeD').AsInteger:=6;
      taM.FieldByName('IdDoc').AsInteger:=quDetPosD.FieldByName('IDHEAD').AsInteger;
      taM.FieldByName('NumDoc').AsString:=quDetPosD.FieldByName('NUMDOC').AsString;
      taM.FieldByName('IDateD').AsInteger:=Trunc(quDetPosD.FieldByName('DATEDOC').AsDateTime);
      taM.FieldByName('DateDoc').AsDateTime:=quDetPosD.FieldByName('DATEDOC').AsDateTime;
      taM.FieldByName('QuantOut').AsFloat:=quDetPosD.FieldByName('QUANT').AsFloat*quDetPosD.FieldByName('KM').AsFloat;
      taM.FieldByName('QuantIn').AsFloat:=0;
      taM.FieldByName('PriceOut0').AsFloat:=quDetPosD.FieldByName('PRICEIN0').AsFloat;
      taM.FieldByName('SumOut0').AsFloat:=quDetPosD.FieldByName('SUMIN0').AsFloat;
      taM.FieldByName('PriceOut').AsFloat:=quDetPosD.FieldByName('PRICEIN').AsFloat;
      taM.FieldByName('SumOut').AsFloat:=quDetPosD.FieldByName('SUMIN').AsFloat;
      taM.FieldByName('PriceIn').AsFloat:=0;
      taM.FieldByName('SumIn').AsFloat:=0;
      taM.FieldByName('PriceIn0').AsFloat:=0;
      taM.FieldByName('SumIn0').AsFloat:=0;
      taM.Post;

      quDetPosD.Next;
    end;
    quDetPosD.Active:=False;


    prWr1('������������ ���������.');
  end;
end;

function prGetOrg(iOrg,iSh:INteger):TOrg;
Var rOrg:TOrg;
begin
  with dmORep do
  begin
    rOrg.Name:='';
    rOrg.FullName:='';
    rOrg.iShop:=iSh;
    rOrg.sAdr:='';
    rOrg.iDep:=0;
    rOrg.IdOrg:=iOrg;
    rOrg.Inn:='';
    rOrg.KPP:='';
    rOrg.sMail:='';
    rOrg.sTel:='';
    rOrg.CodeCountry:='';
    rOrg.CodeReg:='';
    rOrg.Sity:='';
    rOrg.Street:='';
    rOrg.House:='';
    rOrg.Korp:='';
    rOrg.Dir1:='';
    rOrg.Dir2:='';
    rOrg.Dir3:='';
    rOrg.gb1:='';
    rOrg.gb2:='';
    rOrg.gb3:='';
    rOrg.LicVid:='';
    rOrg.LicSer:='';
    rOrg.LicNum:='';
    rOrg.dDateB:=date;
    rOrg.dDateE:=date;
    rOrg.ip:=0;
    //
    quAcloOrg.Active:=False;
    quAcloOrg.ParamByName('IMH').Value:=iSh;
    quAcloOrg.Active:=True;
    if quAcloOrg.RecordCount>0 then
    begin
      quAcloOrg.First;

      rOrg.Name:=quAcloOrgNAMEMH.AsString;
      rOrg.FullName:=quAcloOrgFULLNAME.AsString;
      rOrg.iShop:=1;
      rOrg.sAdr:=quAcloOrgPOSTINDEX.AsString+' '+quAcloOrgCITY.AsString+' '+quAcloOrgSTREET.AsString+' '+quAcloOrgHOUSE.AsString+' '+quAcloOrgKORP.AsString;
      rOrg.iDep:=quAcloOrgID.AsInteger;
      rOrg.IdOrg:=iOrg;
      rOrg.Inn:=quAcloOrgINN.AsString;
      rOrg.KPP:=quAcloOrgKPP.AsString;
      rOrg.sMail:='';
      rOrg.sTel:=quAcloOrgPHONE.AsString;
      rOrg.CodeCountry:='643';
      rOrg.CodeReg:='66';
      rOrg.Sity:=quAcloOrgCITY.AsString;
      rOrg.Street:=quAcloOrgSTREET.AsString;
      rOrg.House:=quAcloOrgHOUSE.AsString;
      rOrg.Korp:=quAcloOrgKORP.AsString;
      rOrg.Dir1:=quAcloOrgDIR1.AsString;
      rOrg.Dir2:=quAcloOrgDIR2.AsString;
      rOrg.Dir3:=quAcloOrgDIR3.AsString;
      rOrg.gb1:=quAcloOrgGB1.AsString;
      rOrg.gb2:=quAcloOrgGB2.AsString;
      rOrg.gb3:=quAcloOrgGB3.AsString;
      rOrg.LicVid:='��������� ������� ����������� ���������';
      rOrg.LicSer:=quAcloOrgSERLIC.AsString;
      rOrg.LicNum:=quAcloOrgNUMLIC.AsString;
      rOrg.dDateB:=quAcloOrgDATEB.AsDateTime;
      rOrg.dDateE:=quAcloOrgDATEE.AsDateTime;
      rOrg.ip:=quAcloOrgIP.AsInteger;
    end;
    quAcloOrg.Active:=False;

    //

    Result:=rOrg;
  end;
end;


procedure prFormQuDocsR;
begin
  with dmORep do
  begin
    quDocsRSel.Active:=False;
    quDocsRSel.SelectSQL.Clear;
    quDocsRSel.SelectSQL.Add('');

    quDocsRSel.SelectSQL.Add('SELECT dh.ID,dh.IDHCB,dh.DATEDOC,dh.NUMDOC,dh.DATESF,dh.NUMSF,dh.IDCLI,dh.IDSKL,dh.SUMIN,');
    quDocsRSel.SelectSQL.Add('       dh.SUMUCH,dh.SUMTAR,dh.SUMNDS0,dh.SUMNDS1,dh.SUMNDS2,dh.PROCNAC,dh.IACTIVE,');
    quDocsRSel.SelectSQL.Add('       dh.BZTYPE,dh.BZSTATUS,dh.BZSUMR,dh.BZSUMF,dh.BZSUMS,dh.IEDIT,dh.PERSEDIT,dh.IPERSEDIT,');
    quDocsRSel.SelectSQL.Add('       cl.NAMECL,mh.NAMEMH,dh.IDFROM,cl1.NAMECL as NAMECLFROM,dh.UPL,dh.IDRV,dh.IDCLITRANS'); //,dh.IDCLITRANS
    quDocsRSel.SelectSQL.Add('FROM OF_DOCHEADOUTR dh');
    quDocsRSel.SelectSQL.Add('left join OF_CLIENTS cl on cl.ID=dh.IDCLI');
    quDocsRSel.SelectSQL.Add('left join OF_CLIENTS cl1 on cl1.ID=dh.IDFROM');
    quDocsRSel.SelectSQL.Add('left join OF_MH mh on mh.ID=dh.IDSKL');

    quDocsRSel.SelectSQL.Add('Where dh.DATEDOC>='''+ds1(CommonSet.DateFrom)+''' and dh.DATEDOC<='''+ds1(CommonSet.DateTo)+'''');
    quDocsRSel.SelectSQL.Add('and dh.IDSKL in (SELECT IDMH FROM RPERSONALMH where IDPERSON='+its(Person.Id)+')');
    if Person.sCli>'' then
    begin
      quDocsRSel.SelectSQL.Add('and dh.IDCLI in ('+Person.sCli+')');
    end;

    quDocsRSel.Active:=True;
  end;
end;

procedure prSetSync(sDocT,sOp:String;IDH,ISKL:INteger);
begin
  with dmORep do
  begin
    quSetSync.SQL.Clear;
    if sDocT='DocR' then
    begin
      quSetSync.SQL.Add('insert into OF_SYNC (DOCTYPE,IDSKL,IPERS,TIMEEDIT,STYPEEDIT,IDDOC)');
      quSetSync.SQL.Add('values (8,'+its(ISKL)+','+its(Person.Id)+','''+formatdatetime('dd.mm.yyyy hh:nn',now)+''','''+sOp+''','+its(IDH)+')');

//insert into OF_SYNC (DOCTYPE,IDSKL,IPERS,TIMEEDIT,STYPEEDIT,IDDOC)
//values (4,5,6,'23.07.2012 18:00','OPN',10000)

    end;
    quSetSync.ExecQuery;
  end;
end;

function FindTSpis(sOp:String):Integer;
begin
  Result:=1;
  with dmORep do
  begin
    quFindTSpis.Active:=False;
    quFindTSpis.ParamByName('ABR').AsString:=sOp;
    quFindTSpis.Active:=True;

    if quFindTSpis.RecordCount>0 then Result:=quFindTSpisTSPIS.AsInteger;

    quFindTSpis.Active:=False;
  end;
end;


function prFindMassaTC(IdCard:Integer):String;
begin
  Result:='';
  with dmORep do
  begin
    quFindMassa.Active:=False;
    quFindMassa.ParamByName('IDCARD').AsInteger:=IdCard;
    quFindMassa.Active:=True;

    if quFindMassa.RecordCount>0 then Result:=quFindMassaPOUTPUT.AsString;

    quFindMassa.Active:=False;
  end;
end;

procedure prFind2group(IdCl:Integer;Var S1,S2:String);
Var Id:INteger;
begin
  with dmORep do
  begin
    S1:=''; S2:=''; Id:=IdCl;
    while iD<>0 do
    begin
      quFindCl.Active:=False;
      quFindCl.ParamByName('IDCL').AsInteger:=Id;
      quFindCl.Active:=True;
      S2:=S1;
      S1:=Copy(quFindClNAMECL.AsString,1,50);
      Id:=quFindClID_PARENT.AsInteger;
      quFindCl.Active:=False;
    end;
  end;
end;  

procedure prCalcLastPricePos(IdCard,IdSkl,IDate:Integer;Var PriceIn,PriceUch,PriceIn0:Real);
begin
  with dmORep do
  begin
//    EXECUTE PROCEDURE PR_CALCLASTPRICE2 (?IDGOOD, ?IDSKL, ?IDATE)
    pr_CalcLastPrice.ParamByName('IDGOOD').AsInteger:=IdCard;
    pr_CalcLastPrice.ParamByName('IDSKL').AsInteger:=IdSkl;
    pr_CalcLastPrice.ParamByName('IDATE').AsInteger:=IDate;
    pr_CalcLastPrice.ExecProc;
    PriceIn0:=pr_CalcLastPrice.ParamByName('PRICEIN0').AsFloat;
    PriceIn:=pr_CalcLastPrice.ParamByName('PRICEIN').AsFloat;
    PriceUch:=pr_CalcLastPrice.ParamByName('PRICEUCH').AsFloat;

    //���� � ������� ��
  end;
end;

function fRecalcSumDoc(Idh,dType,ResT:Integer;Memo1:tcxMemo):Real;
Var rQ,rSum:Real;
begin
  Result:=0;
  with dmORep do
  with dmO do
  begin
    if ResT=2 then
    begin //����������� ������ � ����������
      prResetSum.ParamByName('IDDOC').AsInteger:=IDH;
      prResetSum.ParamByName('DTYPE').AsInteger:=dType;
      prResetSum.ExecProc;
      Result:=prResetSum.ParamByName('RESULT').AsInteger;
    end;
    if ResT=1 then //����������� �� ����������� ����� �� �������������
    begin
      if dType=1 then //������
      begin //������ �� ����� �.�. ��� ������������ ������������ � ��������� �������
            { ����� ���� ��������� ���������� ������� � ���������� ���-��� � �� ����� ����
            , � ��� ��� ��������� ������ �� ������ ������� ��� �� � ���������� ����������� ������.
            ���� ���� ������� � 0 � ���-��, � -900 � �����, � 0=KM
            ����� �������� ������ �������� �������� � �����������
            }
      end;
      if dType=2 then //����������
      begin
        taDobSpec1.Active:=False;
        taDobSpec1.ParamByName('IDH').AsInteger:=IDH;
        taDobSpec1.Active:=True;
        taDobSpec1.First;
        while not taDobSpec1.Eof do
        begin
          quCardPO.Active:=False;
          quCardPO.ParamByName('IDCARD').AsInteger:=taDobSpec1ARTICUL.AsInteger;
          quCardPO.ParamByName('IDH').AsInteger:=IDH;
          quCardPO.ParamByName('DT').AsInteger:=dType;
          quCardPO.Active:=True;
          if quCardPO.RecordCount>0 then
          begin
            rQ:=quCardPOQUANT.AsFloat;
            rSum:=quCardPORSUM.AsFloat;

            if abs(rQ-taDobSpec1KM.AsFloat*taDobSpec1QUANT.AsFloat)<0.001 then
            begin
              //���-�� ��������� - ������������ �����
              if abs(rSum-taDobSpec1SUMIN.AsFloat)>0.01 then
              begin
                prWH('       ���. ����� - ���: '+IntToStr(taDobSpec1ARTICUL.AsInteger),Memo1);
                prWH('          c���� �� - '+FloatToStr(taDobSpec1SUMIN.AsFloat),Memo1);
                prWH('          c���� ����� - '+FloatToStr(rSum),Memo1);

                taDobSpec1.Edit;
                taDobSpec1SUMIN.AsFloat:=rSum;
                taDobSpec1.Post;
                delay(20);
                
              end;
            end else prWH('       ����������� ���-�� ����. ������ - '+IntToStr(taDobSpec1ARTICUL.AsInteger),Memo1);
          end else prWH('       ����������� ����. ������ - '+IntToStr(taDobSpec1ARTICUL.AsInteger),Memo1);

          quCardPO.Active:=False;

          taDobSpec1.Next;
        end;
        taDobSpec1.Active:=False;
      end;
      if dType=4 then //�����
      begin

      end;
      if dType=5 then //��� �����������
      begin

      end;
      if dType=6 then //������������
      begin

      end;
      if dType=7 then //�������
      begin
  {      quSpecOutSel.Active:=False;
        quSpecOutSel.ParamByName('IDHD').AsInteger:=IDH;
        quSpecOutSel.Active:=True;

        quSpecOutSel.First;
        while not quSpecOutSel.Eof do
        begin



          quSpecOutSel.Next;
        end;
        quSpecOutSel.Active:=False;
        }
      end;


      //��� ���� �� ������ ������
      prResetSum.ParamByName('IDDOC').AsInteger:=IDH;
      prResetSum.ParamByName('DTYPE').AsInteger:=dType;
      prResetSum.ExecProc;
      Result:=prResetSum.ParamByName('RESULT').AsInteger;
    end;
  end;
end;

function fTestPartDoc(rSum:Real;Idh,dType:Integer):Integer;
begin
  with dmORep do
  begin
    prTestSpecPart.ParamByName('IDDOC').AsInteger:=IDH;
    prTestSpecPart.ParamByName('DTYPE').AsInteger:=dType;
    prTestSpecPart.ParamByName('DSUM').AsFloat:=rSum;
    prTestSpecPart.ExecProc;
    Result:=prTestSpecPart.ParamByName('RESULT').AsInteger;
  end;
end;


procedure TdmORep.quDocsRCardCalcFields(DataSet: TDataSet);
begin
  quDocsRCardRNac.AsFloat:=quDocsRCardSUMR.AsFloat-quDocsRCardSUMIN.AsFloat;
  if quDocsRCardQUANT.AsFloat>=0 then quDocsRCardOPER.AsInteger:=1 else quDocsRCardOPER.AsInteger:=2;
end;

procedure TdmORep.quDrvCalcFields(DataSet: TDataSet);
Var StrFIO,StrI,StrO:String;
begin
  StrFIO:=quDrvNAME1.AsString;
  StrI:=quDrvNAME2.AsString;
  StrO:=quDrvNAME3.AsString;
  if Length(StrI)>0 then StrFIO:=StrFIO+' '+StrI[1]+'.';
  if Length(StrO)>0 then StrFIO:=StrFIO+StrO[1]+'.';
  quDrvFIO.AsString:=StrFIO;
  quDrvFIO_CAR.AsString:=StrFIO+' ('+quDrvCARNAME.AsString+' "'+quDrvCARNUM.AsString+'")';
end;


procedure TdmORep.quChangeCalcFields(DataSet: TDataSet);
Var SM:String;
    IM:INteger;
begin
  prFindSM(quChangeIMESSURE.AsInteger,SM,IM); //�������� ��������
  quChangeEDIZM.AsString:=SM;
end;

end.




