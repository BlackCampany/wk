unit Dm;

interface

uses
  SysUtils, Classes, DB, ADODB, ImgList, Controls, FIBDatabase,
  pFIBDatabase, FIBDataSet, pFIBDataSet, FIBQuery, pFIBQuery,
  pFIBStoredProc, cxStyles, Graphics, frOLEExl, frXMLExl, frHTMExp,
  FR_E_CSV, FR_E_RTF, FR_E_TXT, frTXTExp, frexpimg, FR_Class, frRtfExp,
  FR_Chart, FR_E_HTML2, DBClient, VaComm, VaSystem, VaClasses, FR_DSet,
  FR_DBSet, Dialogs, dxmdaset;

type
  TdmC = class(TDataModule)
    dsPersonal: TDataSource;
    dsClassif: TDataSource;
    dsFuncList: TDataSource;
    imState: TImageList;
    CasherRnDb: TpFIBDatabase;
    trSelect: TpFIBTransaction;
    trUpdate: TpFIBTransaction;
    quPer: TpFIBDataSet;
    quPerID: TFIBIntegerField;
    quPerID_PARENT: TFIBIntegerField;
    quPerNAME: TFIBStringField;
    quPerUVOLNEN: TFIBBooleanField;
    quPerCheck: TFIBStringField;
    quPerMODUL1: TFIBBooleanField;
    quPerMODUL2: TFIBBooleanField;
    quPerMODUL3: TFIBBooleanField;
    quPerMODUL4: TFIBBooleanField;
    quPerMODUL5: TFIBBooleanField;
    quPerMODUL6: TFIBBooleanField;
    dsPer: TDataSource;
    taPersonal: TpFIBDataSet;
    taPersonalID: TFIBIntegerField;
    taPersonalID_PARENT: TFIBIntegerField;
    taPersonalNAME: TFIBStringField;
    taPersonalUVOLNEN: TFIBBooleanField;
    taPersonalPASSW: TFIBStringField;
    taPersonalMODUL1: TFIBBooleanField;
    taPersonalMODUL2: TFIBBooleanField;
    taPersonalMODUL3: TFIBBooleanField;
    taPersonalMODUL4: TFIBBooleanField;
    taPersonalMODUL5: TFIBBooleanField;
    taPersonalMODUL6: TFIBBooleanField;
    taRClassif: TpFIBDataSet;
    quFuncList: TpFIBDataSet;
    taRClassifID_PERSONAL: TFIBIntegerField;
    taRClassifID_CLASSIF: TFIBIntegerField;
    taRClassifRIGHTS: TFIBIntegerField;
    quFuncListID_PERSONAL: TFIBIntegerField;
    quFuncListNAME: TFIBStringField;
    quFuncListPREXEC: TFIBBooleanField;
    quFuncListCOMMENT: TFIBStringField;
    quClassif: TpFIBDataSet;
    quPersonal1: TpFIBDataSet;
    prChangeRFunction: TpFIBStoredProc;
    prExistPersonal: TpFIBStoredProc;
    prDelPersonal: TpFIBStoredProc;
    taFuncList: TpFIBDataSet;
    taFuncListNAME: TFIBStringField;
    taFuncListCOMMENT: TFIBStringField;
    trDel: TpFIBTransaction;
    quCanDo: TpFIBDataSet;
    prGetId: TpFIBStoredProc;
    taStates: TpFIBDataSet;
    taStatesID: TFIBIntegerField;
    taStatesNAME: TFIBStringField;
    taStatesBARCODE: TFIBStringField;
    taStatesKEY_POSITION: TFIBIntegerField;
    taStatesID_DEVICE: TFIBIntegerField;
    taCF_Operations: TpFIBDataSet;
    taOperations: TpFIBDataSet;
    taOperationsID: TFIBIntegerField;
    taOperationsNAME: TFIBStringField;
    taOperationsID_DEVICE: TFIBIntegerField;
    taClassif: TpFIBDataSet;
    taClassifID: TFIBIntegerField;
    taClassifID_PARENT: TFIBIntegerField;
    taClassifTYPE_CLASSIF: TFIBIntegerField;
    taClassifNAME: TFIBStringField;
    dsCF_Operations: TDataSource;
    taCF_OperationsID_OPERATIONS: TFIBIntegerField;
    taCF_OperationsKEY_CODE: TFIBIntegerField;
    taCF_OperationsKEY_STATUS: TFIBIntegerField;
    taCF_OperationsKEY_CHAR: TFIBStringField;
    taCF_OperationsID_STATES: TFIBIntegerField;
    taCF_OperationsBARCODE: TFIBStringField;
    taCF_OperationsKEY_POSITION: TFIBIntegerField;
    taCF_OperationsID_CLASSIF: TFIBIntegerField;
    taCF_OperationsARTICUL: TFIBStringField;
    taCF_OperationsID_DEPARTS: TFIBIntegerField;
    taCF_OperationsBAR: TFIBStringField;
    taCF_OperationsNAME: TFIBStringField;
    taCF_OperationsNAME1: TFIBStringField;
    dsOperations: TDataSource;
    dsStates: TDataSource;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    quClassifR: TpFIBDataSet;
    quClassifRTYPE_CLASSIF: TFIBIntegerField;
    quClassifRID: TFIBIntegerField;
    quClassifRIACTIVE: TFIBSmallIntField;
    quClassifRID_PARENT: TFIBIntegerField;
    quClassifRNAME: TFIBStringField;
    quClassifRIEDIT: TFIBSmallIntField;
    taCF_OperationsNAMECL: TFIBStringField;
    taCF_All: TpFIBDataSet;
    taCF_AllID_OPERATIONS: TFIBIntegerField;
    taCF_AllKEY_CODE: TFIBIntegerField;
    taCF_AllKEY_STATUS: TFIBIntegerField;
    taCF_AllKEY_CHAR: TFIBStringField;
    taCF_AllID_STATES: TFIBIntegerField;
    taCF_AllBARCODE: TFIBStringField;
    taCF_AllKEY_POSITION: TFIBIntegerField;
    taCF_AllID_CLASSIF: TFIBIntegerField;
    taCF_AllARTICUL: TFIBStringField;
    taCF_AllID_DEPARTS: TFIBIntegerField;
    taCF_AllBAR: TFIBStringField;
    taCF_OperationsNAMECS: TFIBStringField;
    taCF_OperationsNAMEDEP: TFIBStringField;
    taClassifIACTIVE: TFIBSmallIntField;
    taClassifIEDIT: TFIBSmallIntField;
    prSetDiscCardStop: TpFIBStoredProc;
    taPersonalBARCODE: TFIBStringField;
    taCateg: TpFIBDataSet;
    taCategSIFR: TFIBIntegerField;
    taCategNAME: TFIBStringField;
    taCategTAX_GROUP: TFIBIntegerField;
    taCategIACTIVE: TFIBSmallIntField;
    taCategIEDIT: TFIBSmallIntField;
    taMenu: TpFIBDataSet;
    taModify: TpFIBDataSet;
    taMenuSIFR: TFIBIntegerField;
    taMenuNAME: TFIBStringField;
    taMenuPRICE: TFIBFloatField;
    taMenuCODE: TFIBStringField;
    taMenuTREETYPE: TFIBStringField;
    taMenuLIMITPRICE: TFIBFloatField;
    taMenuCATEG: TFIBSmallIntField;
    taMenuPARENT: TFIBSmallIntField;
    taMenuLINK: TFIBSmallIntField;
    taMenuSTREAM: TFIBSmallIntField;
    taMenuLACK: TFIBSmallIntField;
    taMenuDESIGNSIFR: TFIBSmallIntField;
    taMenuALTNAME: TFIBStringField;
    taMenuNALOG: TFIBFloatField;
    taMenuBARCODE: TFIBStringField;
    taMenuIMAGE: TFIBSmallIntField;
    taMenuCONSUMMA: TFIBFloatField;
    taMenuMINREST: TFIBSmallIntField;
    taMenuPRNREST: TFIBSmallIntField;
    taMenuCOOKTIME: TFIBSmallIntField;
    taMenuDISPENSER: TFIBSmallIntField;
    taMenuDISPKOEF: TFIBSmallIntField;
    taMenuACCESS: TFIBSmallIntField;
    taMenuFLAGS: TFIBSmallIntField;
    taMenuTARA: TFIBSmallIntField;
    taMenuCNTPRICE: TFIBSmallIntField;
    taMenuBACKBGR: TFIBFloatField;
    taMenuFONTBGR: TFIBFloatField;
    taMenuIACTIVE: TFIBSmallIntField;
    taMenuIEDIT: TFIBSmallIntField;
    taModifySIFR: TFIBIntegerField;
    taModifyNAME: TFIBStringField;
    taModifyPARENT: TFIBIntegerField;
    taModifyPRICE: TFIBFloatField;
    taModifyREALPRICE: TFIBFloatField;
    taModifyIACTIVE: TFIBSmallIntField;
    taModifyIEDIT: TFIBSmallIntField;
    prAfterImportRk: TpFIBStoredProc;
    quPers: TpFIBDataSet;
    quPersID_PERSONAL: TFIBIntegerField;
    quPersNAME: TFIBStringField;
    quPersCOUNTTAB: TFIBIntegerField;
    quPerssCountTab: TStringField;
    dsPers: TDataSource;
    quTabs: TpFIBDataSet;
    dsTabs: TDataSource;
    quTabsID_PERSONAL: TFIBIntegerField;
    quTabsNUMTABLE: TFIBStringField;
    quTabsQUESTS: TFIBIntegerField;
    quTabsTABSUM: TFIBFloatField;
    quTabsBEGTIME: TFIBDateTimeField;
    quTabsISTATUS: TFIBSmallIntField;
    quPersTOTALSUM: TFIBFloatField;
    quTabsSSTAT: TStringField;
    quTabsSTIME: TStringField;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    quSpec: TpFIBDataSet;
    dsSpec: TDataSource;
    quSpecID_PERSONAL: TFIBIntegerField;
    quSpecNUMTABLE: TFIBStringField;
    quSpecID: TFIBIntegerField;
    quSpecSIFR: TFIBIntegerField;
    quSpecPRICE: TFIBFloatField;
    quSpecQUANTITY: TFIBFloatField;
    quSpecDISCOUNTPROC: TFIBFloatField;
    quSpecDISCOUNTSUM: TFIBFloatField;
    quSpecSUMMA: TFIBFloatField;
    quSpecISTATUS: TFIBIntegerField;
    cxStyle14: TcxStyle;
    cxStyle15: TcxStyle;
    cxStyle16: TcxStyle;
    quMenu: TpFIBDataSet;
    quMenuSIFR: TFIBIntegerField;
    quMenuNAME: TFIBStringField;
    quMenuPARENT: TFIBSmallIntField;
    quMenuCODE: TFIBStringField;
    quMenuPRICE: TFIBFloatField;
    quMenuTREETYPE: TFIBStringField;
    dsMenu: TDataSource;
    quMenuINFO: TStringField;
    quMenuSPRICE: TStringField;
    cxStyle17: TcxStyle;
    cxStyle18: TcxStyle;
    quCheck: TpFIBDataSet;
    quCheckID_PERSONAL: TFIBIntegerField;
    quCheckNUMTABLE: TFIBStringField;
    quCheckID: TFIBIntegerField;
    quCheckSIFR: TFIBIntegerField;
    quCheckPRICE: TFIBFloatField;
    quCheckQUANTITY: TFIBFloatField;
    quCheckDISCOUNTPROC: TFIBFloatField;
    quCheckDISCOUNTSUM: TFIBFloatField;
    quCheckSUMMA: TFIBFloatField;
    quCheckISTATUS: TFIBIntegerField;
    quCheckNAME: TFIBStringField;
    quCheckCODE: TFIBStringField;
    quPersonal1ID: TFIBIntegerField;
    quPersonal1NAME: TFIBStringField;
    dsPersonal1: TDataSource;
    quPersonal1ID_PARENT: TFIBIntegerField;
    quPersonal1UVOLNEN: TFIBBooleanField;
    quPersonal1PASSW: TFIBStringField;
    quPersonal1MODUL1: TFIBBooleanField;
    quPersonal1MODUL2: TFIBBooleanField;
    quPersonal1MODUL3: TFIBBooleanField;
    quPersonal1MODUL4: TFIBBooleanField;
    quPersonal1MODUL5: TFIBBooleanField;
    quPersonal1MODUL6: TFIBBooleanField;
    quPersonal: TpFIBDataSet;
    FIBIntegerField1: TFIBIntegerField;
    FIBStringField1: TFIBStringField;
    FIBIntegerField2: TFIBIntegerField;
    FIBBooleanField1: TFIBBooleanField;
    FIBStringField2: TFIBStringField;
    FIBBooleanField2: TFIBBooleanField;
    FIBBooleanField3: TFIBBooleanField;
    FIBBooleanField4: TFIBBooleanField;
    FIBBooleanField5: TFIBBooleanField;
    FIBBooleanField6: TFIBBooleanField;
    FIBBooleanField7: TFIBBooleanField;
    quTabsDISCONT: TFIBStringField;
    quTabExists: TpFIBDataSet;
    quTabExistsCOUNT: TFIBIntegerField;
    taTabs: TpFIBDataSet;
    taTabsID_PERSONAL: TFIBIntegerField;
    taTabsNUMTABLE: TFIBStringField;
    taTabsQUESTS: TFIBIntegerField;
    taTabsTABSUM: TFIBFloatField;
    taTabsBEGTIME: TFIBDateTimeField;
    taTabsENDTIME: TFIBDateTimeField;
    taTabsISTATUS: TFIBSmallIntField;
    taTabsDISCONT: TFIBStringField;
    quDelTab: TpFIBDataSet;
    taSpec: TpFIBDataSet;
    taSpecID_PERSONAL: TFIBIntegerField;
    taSpecNUMTABLE: TFIBStringField;
    taSpecID: TFIBIntegerField;
    taSpecSIFR: TFIBIntegerField;
    taSpecPRICE: TFIBFloatField;
    taSpecQUANTITY: TFIBFloatField;
    taSpecDISCOUNTPROC: TFIBFloatField;
    taSpecDISCOUNTSUM: TFIBFloatField;
    taSpecSUMMA: TFIBFloatField;
    taSpecISTATUS: TFIBIntegerField;
    quCanDoID_PERSONAL: TFIBIntegerField;
    quCanDoNAME: TFIBStringField;
    quCanDoPREXEC: TFIBBooleanField;
    prFormLog: TpFIBStoredProc;
    quTabsID: TFIBIntegerField;
    quSpecID_TAB: TFIBIntegerField;
    taTabsID: TFIBIntegerField;
    taSpecID_TAB: TFIBIntegerField;
    trSelTab: TpFIBTransaction;
    trUpdTab: TpFIBTransaction;
    quTabsPers: TpFIBDataSet;
    quTabsPersID: TFIBIntegerField;
    quTabsPersID_PERSONAL: TFIBIntegerField;
    quTabsPersNUMTABLE: TFIBStringField;
    quTabsPersQUESTS: TFIBIntegerField;
    quTabsPersTABSUM: TFIBFloatField;
    quTabsPersDISCONT: TFIBStringField;
    dsTabsPers: TDataSource;
    cxStyle19: TcxStyle;
    cxStyle20: TcxStyle;
    taSpec1: TpFIBDataSet;
    taSpec1ID_TAB: TFIBIntegerField;
    taSpec1ID: TFIBIntegerField;
    taSpec1ID_PERSONAL: TFIBIntegerField;
    taSpec1NUMTABLE: TFIBStringField;
    taSpec1SIFR: TFIBIntegerField;
    taSpec1PRICE: TFIBFloatField;
    taSpec1QUANTITY: TFIBFloatField;
    taSpec1DISCOUNTPROC: TFIBFloatField;
    taSpec1DISCOUNTSUM: TFIBFloatField;
    taSpec1SUMMA: TFIBFloatField;
    taSpec1ISTATUS: TFIBIntegerField;
    taDiscCard: TpFIBDataSet;
    taDiscCardBARCODE: TFIBStringField;
    taDiscCardNAME: TFIBStringField;
    taDiscCardPERCENT: TFIBFloatField;
    quModif: TpFIBDataSet;
    quModifSIFR: TFIBIntegerField;
    quModifNAME: TFIBStringField;
    quModifPARENT: TFIBIntegerField;
    quModifPRICE: TFIBFloatField;
    quModifREALPRICE: TFIBFloatField;
    quModifIACTIVE: TFIBSmallIntField;
    quModifIEDIT: TFIBSmallIntField;
    dsModif: TDataSource;
    quCheckLIMITM: TFIBIntegerField;
    quCheckLINKM: TFIBIntegerField;
    quMenuLINK: TFIBSmallIntField;
    quMenuLIMITPRICE: TFIBFloatField;
    taSpecLIMITM: TFIBIntegerField;
    taSpecLINKM: TFIBIntegerField;
    taSpecITYPE: TFIBSmallIntField;
    taSpec1LIMITM: TFIBIntegerField;
    taSpec1LINKM: TFIBIntegerField;
    taSpec1ITYPE: TFIBSmallIntField;
    cxStyle21: TcxStyle;
    quCheckITYPE: TFIBSmallIntField;
    taModif: TpFIBDataSet;
    taModifSIFR: TFIBIntegerField;
    taModifNAME: TFIBStringField;
    taModifPARENT: TFIBIntegerField;
    taModifPRICE: TFIBFloatField;
    taModifREALPRICE: TFIBFloatField;
    taModifIACTIVE: TFIBSmallIntField;
    taModifIEDIT: TFIBSmallIntField;
    quMenuSel: TpFIBDataSet;
    quMenuSelSIFR: TFIBIntegerField;
    quMenuSelNAME: TFIBStringField;
    quMenuSelPRICE: TFIBFloatField;
    quMenuSelCODE: TFIBStringField;
    quMenuSelLIMITPRICE: TFIBFloatField;
    quMenuSelLINK: TFIBSmallIntField;
    quMenuSelSTREAM: TFIBSmallIntField;
    quMenuSelBARCODE: TFIBStringField;
    taCashSail: TpFIBDataSet;
    prSaveToAll: TpFIBStoredProc;
    taCashSailID: TFIBIntegerField;
    taCashSailCASHNUM: TFIBIntegerField;
    taCashSailZNUM: TFIBIntegerField;
    taCashSailCHECKNUM: TFIBIntegerField;
    taCashSailTAB_ID: TFIBIntegerField;
    taCashSailTABSUM: TFIBFloatField;
    taCashSailCLIENTSUM: TFIBFloatField;
    taCashSailCASHERID: TFIBIntegerField;
    taCashSailWAITERID: TFIBIntegerField;
    taCashSailCHDATE: TFIBDateTimeField;
    quCheckSTREAM: TFIBSmallIntField;
    quMenuSTREAM: TFIBSmallIntField;
    quMenuSelKey: TpFIBDataSet;
    quMenuSelKeySIFR: TFIBIntegerField;
    quMenuSelKeyNAME: TFIBStringField;
    quMenuSelKeyPRICE: TFIBFloatField;
    quMenuSelKeyCODE: TFIBStringField;
    dsMenuSelKey: TDataSource;
    taCF_OperationsSIFR: TFIBIntegerField;
    taCF_AllSIFR: TFIBIntegerField;
    quMenuSelKeyLINK: TFIBSmallIntField;
    quMenuSelKeyLIMITPRICE: TFIBFloatField;
    quMenuSelKeySTREAM: TFIBSmallIntField;
    quRep1: TpFIBDataSet;
    quRep1SIFR: TFIBIntegerField;
    quRep1SUMQUANT: TFIBFloatField;
    quRep1SUMSUM: TFIBFloatField;
    quRep1SUMDISC: TFIBFloatField;
    quRep1NAME: TFIBStringField;
    quRep1PARENT: TFIBSmallIntField;
    quRep1CODE: TFIBStringField;
    quRep1NAMEGR: TFIBStringField;
    quRep2: TpFIBDataSet;
    quRep2SIFR: TFIBIntegerField;
    quRep2CODE: TFIBStringField;
    quRep2NAME: TFIBStringField;
    quRep2NAMECA: TFIBStringField;
    quRep2SUMQUANT: TFIBFloatField;
    quRep2SUMSUM: TFIBFloatField;
    quRep2SUMDISC: TFIBFloatField;
    quRep3: TpFIBDataSet;
    quRep3CASHNUM: TFIBIntegerField;
    quRep3ZNUM: TFIBIntegerField;
    quRep3SUMSUM: TFIBFloatField;
    quRep4: TpFIBDataSet;
    quRep4WAITERID: TFIBIntegerField;
    quRep4NAME: TFIBStringField;
    quRep4SUMSUM: TFIBFloatField;
    quRep4SUMQ: TFIBBCDField;
    taTabAll: TpFIBDataSet;
    taTabAllID: TFIBIntegerField;
    taTabAllID_PERSONAL: TFIBIntegerField;
    taTabAllNUMTABLE: TFIBStringField;
    taTabAllQUESTS: TFIBIntegerField;
    taTabAllTABSUM: TFIBFloatField;
    taTabAllBEGTIME: TFIBDateTimeField;
    taTabAllENDTIME: TFIBDateTimeField;
    taTabAllDISCONT: TFIBStringField;
    taTabAllOPERTYPE: TFIBStringField;
    taTabAllCHECKNUM: TFIBIntegerField;
    taTabAllSKLAD: TFIBSmallIntField;
    taSpecAll: TpFIBDataSet;
    taSpecAllID_TAB: TFIBIntegerField;
    taSpecAllID: TFIBIntegerField;
    taSpecAllID_PERSONAL: TFIBIntegerField;
    taSpecAllNUMTABLE: TFIBStringField;
    taSpecAllSIFR: TFIBIntegerField;
    taSpecAllPRICE: TFIBFloatField;
    taSpecAllQUANTITY: TFIBFloatField;
    taSpecAllDISCOUNTPROC: TFIBFloatField;
    taSpecAllDISCOUNTSUM: TFIBFloatField;
    taSpecAllSUMMA: TFIBFloatField;
    taSpecAllISTATUS: TFIBIntegerField;
    taSpecAllITYPE: TFIBSmallIntField;
    taTabsAllSel: TpFIBDataSet;
    taTabsAllSelID: TFIBIntegerField;
    taTabsAllSelID_PERSONAL: TFIBIntegerField;
    taTabsAllSelNUMTABLE: TFIBStringField;
    taTabsAllSelQUESTS: TFIBIntegerField;
    taTabsAllSelTABSUM: TFIBFloatField;
    taTabsAllSelBEGTIME: TFIBDateTimeField;
    taTabsAllSelENDTIME: TFIBDateTimeField;
    taTabsAllSelDISCONT: TFIBStringField;
    taTabsAllSelOPERTYPE: TFIBStringField;
    taTabsAllSelCHECKNUM: TFIBIntegerField;
    taTabsAllSelSKLAD: TFIBSmallIntField;
    taTabsAllSelNAME: TFIBStringField;
    dsTabsAllSel: TDataSource;
    taTabsAllSelCASHNUM: TFIBIntegerField;
    taTabsAllSelZNUM: TFIBIntegerField;
    taTabsAllSelCHECKNUM1: TFIBIntegerField;
    taTabsAllSelTABSUM1: TFIBFloatField;
    taTabsAllSelCLIENTSUM: TFIBFloatField;
    taTabsAllSelCHDATE: TFIBDateTimeField;
    taSpecAllSel: TpFIBDataSet;
    taSpecAllSelID_TAB: TFIBIntegerField;
    taSpecAllSelID: TFIBIntegerField;
    taSpecAllSelID_PERSONAL: TFIBIntegerField;
    taSpecAllSelNUMTABLE: TFIBStringField;
    taSpecAllSelSIFR: TFIBIntegerField;
    taSpecAllSelPRICE: TFIBFloatField;
    taSpecAllSelQUANTITY: TFIBFloatField;
    taSpecAllSelDISCOUNTPROC: TFIBFloatField;
    taSpecAllSelDISCOUNTSUM: TFIBFloatField;
    taSpecAllSelSUMMA: TFIBFloatField;
    taSpecAllSelISTATUS: TFIBIntegerField;
    taSpecAllSelITYPE: TFIBSmallIntField;
    taSpecAllSelNAMEMM: TFIBStringField;
    taSpecAllSelNAMEMD: TFIBStringField;
    taSpecAllSelNAME: TStringField;
    dsSpecAllSel: TDataSource;
    quRepLog: TpFIBDataSet;
    dsRepLog: TDataSource;
    quRepLogDATEOP: TFIBDateTimeField;
    quRepLogID_PERSONAL: TFIBIntegerField;
    quRepLogNAMEOP: TFIBStringField;
    quRepLogCONTENT: TFIBStringField;
    quRepLogNAMEP: TFIBStringField;
    quRepLogOPERNAME: TFIBStringField;
    quRepLogCONTNAME: TStringField;
    quRep5: TpFIBDataSet;
    quRep5CASHERID: TFIBIntegerField;
    quRep5NAME: TFIBStringField;
    quRep5SUMSUM: TFIBFloatField;
    quRep5SUMQ: TFIBBCDField;
    quRep6: TpFIBDataSet;
    quRep6CHDATE: TFIBDateTimeField;
    quRep6TABSUM: TFIBFloatField;
    quRep6QUESTS: TFIBIntegerField;
    quFindPers: TpFIBDataSet;
    quFindPersNAME: TFIBStringField;
    prOpenTab: TpFIBStoredProc;
    quCurSpec: TpFIBDataSet;
    quCurMod: TpFIBDataSet;
    dsCurSpec: TDataSource;
    dsCurMod: TDataSource;
    quCurSpecID_TAB: TFIBIntegerField;
    quCurSpecID: TFIBIntegerField;
    quCurSpecID_PERSONAL: TFIBIntegerField;
    quCurSpecNUMTABLE: TFIBStringField;
    quCurSpecSIFR: TFIBIntegerField;
    quCurSpecPRICE: TFIBFloatField;
    quCurSpecQUANTITY: TFIBFloatField;
    quCurSpecSUMMA: TFIBFloatField;
    quCurSpecDPROC: TFIBFloatField;
    quCurSpecDSUM: TFIBFloatField;
    quCurSpecISTATUS: TFIBSmallIntField;
    quCurSpecNAME: TFIBStringField;
    quCurSpecCODE: TFIBStringField;
    quCurSpecLINKM: TFIBIntegerField;
    quCurSpecLIMITM: TFIBIntegerField;
    quCurSpecSTREAM: TFIBIntegerField;
    quCurModID_TAB: TFIBIntegerField;
    quCurModID_POS: TFIBIntegerField;
    quCurModID: TFIBIntegerField;
    quCurModSIFR: TFIBIntegerField;
    quCurModNAME: TFIBStringField;
    quCurModQUANTITY: TFIBFloatField;
    trCurSpecSel: TpFIBTransaction;
    trCurSpecUpd: TpFIBTransaction;
    trCurModSel: TpFIBTransaction;
    trCurModUpd: TpFIBTransaction;
    trOpenTab: TpFIBTransaction;
    quSetStatus: TpFIBQuery;
    quCurModAll: TpFIBDataSet;
    trModSel: TpFIBTransaction;
    quCurModAllID_TAB: TFIBIntegerField;
    quCurModAllID_POS: TFIBIntegerField;
    quCurModAllID: TFIBIntegerField;
    quCurModAllSIFR: TFIBIntegerField;
    quCurModAllNAME: TFIBStringField;
    quCurModAllQUANTITY: TFIBFloatField;
    prSaveTab: TpFIBStoredProc;
    trSaveTab: TpFIBTransaction;
    prClearCur: TpFIBStoredProc;
    trClearCur: TpFIBTransaction;
    quMenuSelPARENT: TFIBSmallIntField;
    taSpecID_POS: TFIBIntegerField;
    taSpec1ID_POS: TFIBIntegerField;
    quTabsPersBEGTIME: TFIBDateTimeField;
    quTabsPersISTATUS: TFIBSmallIntField;
    trCheck: TpFIBTransaction;
    quRep3ZDATE: TFIBDateTimeField;
    quRep1_: TpFIBDataSet;
    quRep2_: TpFIBDataSet;
    quRep1_SIFR: TFIBIntegerField;
    quRep1_CODE: TFIBStringField;
    quRep1_NAME: TFIBStringField;
    quRep1_PARENT: TFIBSmallIntField;
    quRep1_NAMEGR: TFIBStringField;
    quRep1_SUMQUANT: TFIBFloatField;
    quRep1_SUMSUM: TFIBFloatField;
    quRep1_SUMDISC: TFIBFloatField;
    quRep3ZDATE1: TFIBDateTimeField;
    frRtfAdvExport1: TfrRtfAdvExport;
    frJPEGExport1: TfrJPEGExport;
    frTIFFExport1: TfrTIFFExport;
    frBMPExport1: TfrBMPExport;
    frTextAdvExport1: TfrTextAdvExport;
    frTextExport1: TfrTextExport;
    frRTFExport1: TfrRTFExport;
    frCSVExport1: TfrCSVExport;
    frHTMLTableExport1: TfrHTMLTableExport;
    frXMLExcelExport1: TfrXMLExcelExport;
    frOLEExcelExport1: TfrOLEExcelExport;
    frHTML2Export1: TfrHTML2Export;
    frChartObject1: TfrChartObject;
    quRep2_SIFR: TFIBIntegerField;
    quRep2_CODE: TFIBStringField;
    quRep2_NAME: TFIBStringField;
    quRep2_NAMECA: TFIBStringField;
    quRep2_SUMQUANT: TFIBFloatField;
    quRep2_SUMSUM: TFIBFloatField;
    quRep2_SUMDISC: TFIBFloatField;
    quRepDel: TpFIBDataSet;
    quRepDelSKLAD: TFIBSmallIntField;
    quRepDelSIFR: TFIBIntegerField;
    quRepDelNAME: TFIBStringField;
    quRepDelSQUANTITY: TFIBFloatField;
    quRepDelSSUMMA: TFIBFloatField;
    quRepDelSDISCOUNTSUM: TFIBFloatField;
    cxStyle22: TcxStyle;
    cxStyle23: TcxStyle;
    quCurSpecMod: TpFIBDataSet;
    quCurSpecModID_TAB: TFIBIntegerField;
    quCurSpecModID_POS: TFIBIntegerField;
    quCurSpecModID: TFIBIntegerField;
    quCurSpecModSIFR: TFIBIntegerField;
    quCurSpecModNAME: TFIBStringField;
    quCurSpecModQUANTITY: TFIBFloatField;
    dsCurSpecMod: TDataSource;
    quHotKey: TpFIBDataSet;
    trSelHK: TpFIBTransaction;
    trUpdHK: TpFIBTransaction;
    quHotKeyCASHNUM: TFIBIntegerField;
    quHotKeyIROW: TFIBSmallIntField;
    quHotKeyICOL: TFIBSmallIntField;
    quHotKeySIFR: TFIBIntegerField;
    quHotKeyNAME: TFIBStringField;
    quHotKeyPRICE: TFIBFloatField;
    cxStyle24: TcxStyle;
    quMenuHK: TpFIBDataSet;
    trSelMenuHK: TpFIBTransaction;
    quMenuHKSIFR: TFIBIntegerField;
    quMenuHKNAME: TFIBStringField;
    quMenuHKPRICE: TFIBFloatField;
    quMenuHKCODE: TFIBStringField;
    quMenuHKTREETYPE: TFIBStringField;
    quMenuHKLIMITPRICE: TFIBFloatField;
    quMenuHKCATEG: TFIBSmallIntField;
    quMenuHKLINK: TFIBSmallIntField;
    quMenuHKSTREAM: TFIBSmallIntField;
    quMenuHKLACK: TFIBSmallIntField;
    quMenuHKDESIGNSIFR: TFIBSmallIntField;
    quMenuHKALTNAME: TFIBStringField;
    quMenuHKNALOG: TFIBFloatField;
    quMenuHKBARCODE: TFIBStringField;
    quMenuHKIMAGE: TFIBSmallIntField;
    quMenuHKCONSUMMA: TFIBFloatField;
    quMenuHKMINREST: TFIBSmallIntField;
    quMenuHKPRNREST: TFIBSmallIntField;
    quMenuHKCOOKTIME: TFIBSmallIntField;
    quMenuHKDISPENSER: TFIBSmallIntField;
    quMenuHKDISPKOEF: TFIBSmallIntField;
    quMenuHKACCESS: TFIBSmallIntField;
    quMenuHKFLAGS: TFIBSmallIntField;
    quMenuHKTARA: TFIBSmallIntField;
    quMenuHKCNTPRICE: TFIBSmallIntField;
    quMenuHKBACKBGR: TFIBFloatField;
    quMenuHKFONTBGR: TFIBFloatField;
    quMenuHKIACTIVE: TFIBSmallIntField;
    quMenuHKIEDIT: TFIBSmallIntField;
    taCurMod: TpFIBDataSet;
    trCurMod: TpFIBTransaction;
    taCurModID_TAB: TFIBIntegerField;
    taCurModID_POS: TFIBIntegerField;
    taCurModID: TFIBIntegerField;
    taCurModSIFR: TFIBIntegerField;
    taCurModNAME: TFIBStringField;
    taCurModQUANTITY: TFIBFloatField;
    quCashSail: TpFIBDataSet;
    trSelCS: TpFIBTransaction;
    trUpdCS: TpFIBTransaction;
    quCashSailID: TFIBIntegerField;
    quCashSailCASHNUM: TFIBIntegerField;
    quCashSailZNUM: TFIBIntegerField;
    quCashSailCHECKNUM: TFIBIntegerField;
    quCashSailTAB_ID: TFIBIntegerField;
    quCashSailTABSUM: TFIBFloatField;
    quCashSailCLIENTSUM: TFIBFloatField;
    quCashSailCASHERID: TFIBIntegerField;
    quCashSailWAITERID: TFIBIntegerField;
    quCashSailCHDATE: TFIBDateTimeField;
    quPCard: TpFIBDataSet;
    quPCardCLINAME: TFIBStringField;
    quPCardDATEFROM: TFIBDateField;
    quPCardDATETO: TFIBDateField;
    DevPrint: TVaComm;
    VaWaitMessage1: TVaWaitMessage;
    trSel1: TpFIBTransaction;
    trSel2: TpFIBTransaction;
    quRep7: TpFIBDataSet;
    quRep7SIFR: TFIBIntegerField;
    quRep7CODE: TFIBStringField;
    quRep7NAME: TFIBStringField;
    quRep7STREAM: TFIBSmallIntField;
    quRep7NAMESTREAM: TFIBStringField;
    quRep7SUMQUANT: TFIBFloatField;
    quRep7SUMSUM: TFIBFloatField;
    quRep7SUMDISC: TFIBFloatField;
    quRep8: TpFIBDataSet;
    quRep8STREAM: TFIBSmallIntField;
    quRep8NAMESTREAM: TFIBStringField;
    quRep8SUMSUM: TFIBFloatField;
    quRep8SUMDISC: TFIBFloatField;
    quMenuDESIGNSIFR: TFIBSmallIntField;
    trCalcDisc: TpFIBTransaction;
    prCalcDisc: TpFIBStoredProc;
    taStreams: TpFIBDataSet;
    taStreamsID: TFIBIntegerField;
    taStreamsNAMESTREAM: TFIBStringField;
    trStreams: TpFIBTransaction;
    quCurSum: TpFIBDataSet;
    quCurSumRSUM: TFIBFloatField;
    trDiscCard: TpFIBTransaction;
    trMenu: TpFIBTransaction;
    quDelCS: TpFIBQuery;
    quDelTabAll: TpFIBQuery;
    quCurSpecSTATION: TFIBIntegerField;
    quDelSpec: TpFIBQuery;
    quTestSave: TpFIBDataSet;
    quTestSaveTABSUM: TFIBFloatField;
    quCurSpecModSTATION: TFIBIntegerField;
    taCurModSTATION: TFIBIntegerField;
    quCashSailPAYTYPE: TFIBSmallIntField;
    taCashSailPAYTYPE: TFIBSmallIntField;
    quRep3PAYTYPE: TFIBSmallIntField;
    taDiscPref: TpFIBDataSet;
    taDiscPrefBARCODE: TFIBStringField;
    taDiscPrefNAME: TFIBStringField;
    taDiscPrefPERCENT: TFIBFloatField;
    taDiscPrefCLIENTINDEX: TFIBIntegerField;
    taDiscPrefIACTIVE: TFIBSmallIntField;
    prAddBNTR: TpFIBStoredProc;
    taCredCard: TpFIBDataSet;
    dsCredCard: TDataSource;
    taCredCardID: TFIBSmallIntField;
    taCredCardNAME: TFIBStringField;
    taCredCardCLIENTINDEX: TFIBIntegerField;
    taCredCardLIMITSUM: TFIBFloatField;
    taCredCardCANRETURN: TFIBIntegerField;
    trAddBn: TpFIBTransaction;
    trSelCredCard: TpFIBTransaction;
    quSelSpecAll: TpFIBDataSet;
    quSelSpecAllID_TAB: TFIBIntegerField;
    quSelSpecAllID: TFIBIntegerField;
    quSelSpecAllID_PERSONAL: TFIBIntegerField;
    quSelSpecAllNUMTABLE: TFIBStringField;
    quSelSpecAllSIFR: TFIBIntegerField;
    quSelSpecAllPRICE: TFIBFloatField;
    quSelSpecAllQUANTITY: TFIBFloatField;
    quSelSpecAllDISCOUNTPROC: TFIBFloatField;
    quSelSpecAllDISCOUNTSUM: TFIBFloatField;
    quSelSpecAllSUMMA: TFIBFloatField;
    quSelSpecAllITYPE: TFIBSmallIntField;
    quSelSpecAllNAMEMM: TFIBStringField;
    quSelSpecAllNAMEMD: TFIBStringField;
    quSelSpecAllNAMEPERS: TFIBStringField;
    quSelSpecAllQUESTS: TFIBIntegerField;
    quSelSpecAllTABSUM: TFIBFloatField;
    quSelSpecAllBEGTIME: TFIBDateTimeField;
    quSelSpecAllENDTIME: TFIBDateTimeField;
    quSelSpecAllDISCONT: TFIBStringField;
    quSelSpecAllOPERTYPE: TFIBStringField;
    quSelSpecAllCHECKNUM: TFIBIntegerField;
    quSelSpecAllSKLAD: TFIBSmallIntField;
    quSelSpecAllCASHNUM: TFIBIntegerField;
    quSelSpecAllZNUM: TFIBIntegerField;
    quSelSpecAllCLIENTSUM: TFIBFloatField;
    quSelSpecAllCHDATE: TFIBDateTimeField;
    dsSelSpecAll: TDataSource;
    cxStyle25: TcxStyle;
    quSelSpecAllName: TStringField;
    quRep9: TpFIBDataSet;
    quRep9DATER: TFIBDateField;
    quRep9SIFR: TFIBIntegerField;
    quRep9PRICE: TFIBFloatField;
    quRep9NAME: TFIBStringField;
    quRep9PARENT: TFIBSmallIntField;
    quRep9NAMEGROUP: TFIBStringField;
    quRep9CATEG: TFIBSmallIntField;
    quRep9NAMECAT: TFIBStringField;
    quRep9QSUM: TFIBFloatField;
    quRep9SSUM: TFIBFloatField;
    dsRep9: TDataSource;
    quCurModSTATION: TFIBIntegerField;
    frRep1: TfrReport;
    frServP: TfrDBDataSet;
    prSaveToAllPC: TpFIBStoredProc;
    taTabAllDISCONT1: TFIBStringField;
    quTabsNAME: TFIBStringField;
    cxStyle26: TcxStyle;
    trSelId: TpFIBTransaction;
    taSpecAllSelSTREAM: TFIBSmallIntField;
    taSpecAllSelNAMESTREAM: TFIBStringField;
    quSelSpecAllSTREAM: TFIBSmallIntField;
    quSelSpecAllNAMESTREAM: TFIBStringField;
    quRep10: TpFIBDataSet;
    quRep10SIFR: TFIBIntegerField;
    quRep10CODE: TFIBStringField;
    quRep10NAME: TFIBStringField;
    quRep10STREAM: TFIBSmallIntField;
    quRep10NAMESTREAM: TFIBStringField;
    quRep10STATION: TFIBIntegerField;
    quRep10SUMQUANT: TFIBFloatField;
    quRep10SUMSUM: TFIBFloatField;
    quRep10SUMDISC: TFIBFloatField;
    taTabsAllSelSTATION: TFIBIntegerField;
    quSelSpecAllSTATION: TFIBIntegerField;
    quSelSpecAllINEED: TFIBSmallIntField;
    taTabAllSTATION: TFIBIntegerField;
    taFis: TpFIBDataSet;
    taFisID_TAB: TFIBIntegerField;
    taFisOPERTYPE: TFIBStringField;
    taFisNAME: TFIBStringField;
    taFisRSUM: TFIBFloatField;
    quSetPrint: TpFIBQuery;
    taCashSailPAYID: TFIBIntegerField;
    quRealW: TpFIBDataSet;
    quRealWWAITERID: TFIBIntegerField;
    quRealWPAYTYPE: TFIBSmallIntField;
    quRealWPAYID: TFIBIntegerField;
    quRealWRSUM: TFIBFloatField;
    quRealWPNAME: TFIBStringField;
    quRealWCRNAME: TFIBStringField;
    quSelSpecAllQUANTITY1: TFIBFloatField;
    quSelSpecAllSUMMA1: TFloatField;
    cxStyle27: TcxStyle;
    taCashSailSel: TpFIBDataSet;
    taCashSailSelID: TFIBIntegerField;
    taCashSailSelCASHNUM: TFIBIntegerField;
    taCashSailSelZNUM: TFIBIntegerField;
    taCashSailSelCHECKNUM: TFIBIntegerField;
    taCashSailSelTAB_ID: TFIBIntegerField;
    taCashSailSelTABSUM: TFIBFloatField;
    taCashSailSelCLIENTSUM: TFIBFloatField;
    taCashSailSelCASHERID: TFIBIntegerField;
    taCashSailSelWAITERID: TFIBIntegerField;
    taCashSailSelCHDATE: TFIBDateTimeField;
    taCashSailSelPAYTYPE: TFIBSmallIntField;
    taCashSailSelPAYID: TFIBIntegerField;
    taFisBN: TpFIBDataSet;
    taFisBNTABSUM: TFIBFloatField;
    quTabsNUMZ: TFIBIntegerField;
    taMes: TpFIBDataSet;
    taMesID: TFIBIntegerField;
    taMesMESSAG: TFIBStringField;
    dstaMes: TDataSource;
    taTabAllNUMZ: TFIBIntegerField;
    taTabsNUMZ: TFIBIntegerField;
    taDiscCardIACTIVE: TFIBSmallIntField;
    quTabsPersNUMZ: TFIBIntegerField;
    taTabsSTATION: TFIBSmallIntField;
    cxStyle100: TcxStyle;
    cxStyle118: TcxStyle;
    quOpenZ: TpFIBDataSet;
    quOpenZID: TFIBIntegerField;
    quOpenZID_PERSONAL: TFIBIntegerField;
    quOpenZNUMTABLE: TFIBStringField;
    quOpenZQUESTS: TFIBIntegerField;
    quOpenZTABSUM: TFIBFloatField;
    quOpenZBEGTIME: TFIBDateTimeField;
    quOpenZENDTIME: TFIBDateTimeField;
    quOpenZISTATUS: TFIBSmallIntField;
    quOpenZDISCONT: TFIBStringField;
    quOpenZNUMZ: TFIBIntegerField;
    quOpenZSTATION: TFIBSmallIntField;
    quClassifID_CLASSIF: TFIBIntegerField;
    quClassifRIGHTS: TFIBIntegerField;
    quClassifID_PARENT: TFIBIntegerField;
    quClassifNAME: TFIBStringField;
    prDisc_Hist: TpFIBStoredProc;
    taTabAllDELT: TFIBIntegerField;
    quTabsSALET: TFIBSmallIntField;
    quSpecSel: TpFIBDataSet;
    dsquSpecSel: TDataSource;
    quSpecSelID: TFIBIntegerField;
    quSpecSelSIFR: TFIBIntegerField;
    quSpecSelPRICE: TFIBFloatField;
    quSpecSelQUANTITY: TFIBFloatField;
    quSpecSelDISCOUNTPROC: TFIBFloatField;
    quSpecSelDISCOUNTSUM: TFIBFloatField;
    quSpecSelSUMMA: TFIBFloatField;
    quSpecSelNAME: TFIBStringField;
    cxStyle28: TcxStyle;
    quCurSpecQUEST: TFIBSmallIntField;
    cxStyle29: TcxStyle;
    quSelCard: TpFIBQuery;
    quPCardsList: TpFIBDataSet;
    quPCardsListBARCODE: TFIBStringField;
    quPCardsListPLATTYPE: TFIBIntegerField;
    quPCardsListIACTIVE: TFIBSmallIntField;
    quPCardsListCLINAME: TFIBStringField;
    quPCardsListDATEFROM: TFIBDateField;
    quPCardsListDATETO: TFIBDateField;
    quPCardsListVISIBLE: TFIBSmallIntField;
    dsquPCardsList: TDataSource;
    quCheckID_TAB: TFIBIntegerField;
    quCheckQUEST: TFIBSmallIntField;
    quSpecID_POS: TFIBIntegerField;
    quSpecLIMITM: TFIBIntegerField;
    quSpecLINKM: TFIBIntegerField;
    quSpecITYPE: TFIBSmallIntField;
    quSpecQUEST: TFIBSmallIntField;
    taSpecQUEST: TFIBSmallIntField;
    taSpec1QUEST: TFIBSmallIntField;
    cxStyle30: TcxStyle;
    cxStyle31: TcxStyle;
    taTabAllSALET: TFIBSmallIntField;
    taServP: TdxMemData;
    taServPName: TStringField;
    taServPCode: TStringField;
    taServPQuant: TFloatField;
    taServPStream: TSmallintField;
    taServPiType: TSmallintField;
    taDiscCardCLIENTINDEX: TFIBIntegerField;
    taDiscCardTYPEOPL: TFIBIntegerField;
    prCalcBonus: TpFIBStoredProc;
    trCalcBonus: TpFIBTransaction;
    quPCardSALET: TFIBIntegerField;
    taTabsSALET: TFIBSmallIntField;
    quTabsP1: TpFIBDataSet;
    quTabsP1ID: TFIBIntegerField;
    quTabsP1ID_PERSONAL: TFIBIntegerField;
    quTabsP1NUMTABLE: TFIBStringField;
    quTabsP1QUESTS: TFIBIntegerField;
    quTabsP1TABSUM: TFIBFloatField;
    quTabsP1BEGTIME: TFIBDateTimeField;
    quTabsP1ISTATUS: TFIBSmallIntField;
    quTabsP1DISCONT: TFIBStringField;
    quTabsP1NUMZ: TFIBIntegerField;
    trSel4: TpFIBTransaction;
    quTestGr: TpFIBDataSet;
    quTestGrPARENT: TFIBSmallIntField;
    quTestGrNAMEGR: TFIBStringField;
    quTestGrMAINGR: TFIBSmallIntField;
    quTestGrQUANT: TFIBFloatField;
    taServPiServ: TIntegerField;
    quCurModAllISERV: TFIBIntegerField;
    taServPIPos: TIntegerField;
    quListTabs: TpFIBDataSet;
    quMenuMINREST: TFIBSmallIntField;
    quMenuPRNREST: TFIBSmallIntField;
    quMenuBACKBGR: TFIBFloatField;
    quMenuSL: TpFIBDataSet;
    quMenuSLSIFR: TFIBIntegerField;
    quMenuSLNAME: TFIBStringField;
    quMenuSLPRICE: TFIBFloatField;
    quMenuSLCODE: TFIBStringField;
    quMenuSLTREETYPE: TFIBStringField;
    quMenuSLLIMITPRICE: TFIBFloatField;
    quMenuSLCATEG: TFIBSmallIntField;
    quMenuSLPARENT: TFIBSmallIntField;
    quMenuSLLINK: TFIBSmallIntField;
    quMenuSLSTREAM: TFIBSmallIntField;
    quMenuSLLACK: TFIBSmallIntField;
    quMenuSLDESIGNSIFR: TFIBSmallIntField;
    quMenuSLALTNAME: TFIBStringField;
    quMenuSLNALOG: TFIBFloatField;
    quMenuSLBARCODE: TFIBStringField;
    quMenuSLIMAGE: TFIBSmallIntField;
    quMenuSLCONSUMMA: TFIBFloatField;
    quMenuSLMINREST: TFIBSmallIntField;
    quMenuSLPRNREST: TFIBSmallIntField;
    quMenuSLCOOKTIME: TFIBSmallIntField;
    quMenuSLDISPENSER: TFIBSmallIntField;
    quMenuSLDISPKOEF: TFIBSmallIntField;
    quMenuSLACCESS: TFIBSmallIntField;
    quMenuSLFLAGS: TFIBSmallIntField;
    quMenuSLTARA: TFIBSmallIntField;
    quMenuSLCNTPRICE: TFIBSmallIntField;
    quMenuSLBACKBGR: TFIBFloatField;
    quMenuSLFONTBGR: TFIBFloatField;
    quMenuSLIACTIVE: TFIBSmallIntField;
    quMenuSLIEDIT: TFIBSmallIntField;
    quMenuSLDATEB: TFIBIntegerField;
    quMenuSLDATEE: TFIBIntegerField;
    quMenuSLDAYWEEK: TFIBStringField;
    quMenuSLTIMEB: TFIBTimeField;
    quMenuSLTIMEE: TFIBTimeField;
    quMenuSLALLTIME: TFIBSmallIntField;
    dsquMenuSL: TDataSource;
    cxStyle32: TcxStyle;
    prADDMOVESL: TpFIBStoredProc;
    trUpdMove: TpFIBTransaction;
    quMenuSelPRNREST: TFIBSmallIntField;
    quTestRemn: TpFIBDataSet;
    quTestRemnSTATION: TFIBIntegerField;
    quTestRemnID_TAB: TFIBIntegerField;
    quTestRemnID: TFIBIntegerField;
    quTestRemnSIFR: TFIBIntegerField;
    quTestRemnPRICE: TFIBFloatField;
    quTestRemnQUANTITY: TFIBFloatField;
    quTestRemnSUMMA: TFIBFloatField;
    quTestRemnNAME: TFIBStringField;
    quTestRemnBACKBGR: TFIBFloatField;
    quPServ: TpFIBDataSet;
    trSelPServ: TpFIBTransaction;
    quPServISTATION: TFIBIntegerField;
    quPServISTREAM: TFIBIntegerField;
    quPServNAMESTREAM: TFIBStringField;
    quPServSPORT: TFIBStringField;
    quPServRING: TFIBSmallIntField;
    taTabAllID_PERSONALCLOSE: TFIBIntegerField;
    dsquCheckOrgItog: TDataSource;
    quCheckOrgItog: TpFIBDataSet;
    quSelCashPar: TpFIBDataSet;
    quSelCashParISTATION: TFIBIntegerField;
    quSelCashParCASHNUM: TFIBIntegerField;
    quSelCashParFIS: TFIBSmallIntField;
    quSelCashParIORG: TFIBIntegerField;
    quSelCashParCASHSER: TFIBStringField;
    quSelCashParDEPARTNAME: TFIBStringField;
    quSelCashParCASHPORT: TFIBIntegerField;
    quSelCashParCASHCHNUM: TFIBIntegerField;
    quSelCashParCASHZ: TFIBIntegerField;
    quSelCashParCOMDELAY: TFIBIntegerField;
    quSelCashParROUNDSUM: TFIBIntegerField;
    quSelCashParPRELINE: TFIBStringField;
    quSelCashParPRELINE1: TFIBStringField;
    quSelCashParLASTLINE: TFIBStringField;
    quSelCashParLASTLINE1: TFIBStringField;
    quCheckOrgItogIORG: TFIBIntegerField;
    quCheckOrgItogNAME: TFIBStringField;
    quCheckOrgItogRSUM: TFIBFloatField;
    quCheckOrg: TpFIBDataSet;
    quCheckOrgID_TAB: TFIBIntegerField;
    quCheckOrgID: TFIBIntegerField;
    quCheckOrgID_PERSONAL: TFIBIntegerField;
    quCheckOrgNUMTABLE: TFIBStringField;
    quCheckOrgSIFR: TFIBIntegerField;
    quCheckOrgPRICE: TFIBFloatField;
    quCheckOrgQUANTITY: TFIBFloatField;
    quCheckOrgDISCOUNTPROC: TFIBFloatField;
    quCheckOrgDISCOUNTSUM: TFIBFloatField;
    quCheckOrgSUMMA: TFIBFloatField;
    quCheckOrgISTATUS: TFIBIntegerField;
    quCheckOrgNAME: TFIBStringField;
    quCheckOrgCODE: TFIBStringField;
    quCheckOrgSTREAM: TFIBSmallIntField;
    quCheckOrgLIMITM: TFIBIntegerField;
    quCheckOrgLINKM: TFIBIntegerField;
    quCheckOrgITYPE: TFIBSmallIntField;
    quCheckOrgQUEST: TFIBSmallIntField;
    quCheckOrgIORG: TFIBIntegerField;
    quOrgsSt: TpFIBDataSet;
    quOrgsStISTATION: TFIBIntegerField;
    quOrgsStCASHNUM: TFIBIntegerField;
    quOrgsStFIS: TFIBSmallIntField;
    quOrgsStIORG: TFIBIntegerField;
    quOrgsStCASHSER: TFIBStringField;
    quOrgsStDEPARTNAME: TFIBStringField;
    quOrgsStCASHPORT: TFIBIntegerField;
    quOrgsStCASHCHNUM: TFIBIntegerField;
    quOrgsStCASHZ: TFIBIntegerField;
    quOrgsStCOMDELAY: TFIBIntegerField;
    quOrgsStROUNDSUM: TFIBIntegerField;
    quOrgsStPRELINE: TFIBStringField;
    quOrgsStPRELINE1: TFIBStringField;
    quOrgsStLASTLINE: TFIBStringField;
    quOrgsStLASTLINE1: TFIBStringField;
    dsquOrgsSt: TDataSource;
    quSelCashParSPECBOXX: TFIBStringField;
    quSelCashParSPH1: TFIBStringField;
    quSelCashParSPH2: TFIBStringField;
    quSelCashParSPH3: TFIBStringField;
    quSelCashParSPH4: TFIBStringField;
    quSelCashParSPH5: TFIBStringField;
    quCheckOrgItogDSUM: TFIBFloatField;
    quCheckOrgItogITSUM: TFloatField;
    quCheckOrgAvans: TpFIBDataSet;
    quCheckOrgAvansRSUM: TFIBFloatField;
    quListTabsNAMETAB: TFIBStringField;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure quFuncListBeforePost(DataSet: TDataSet);
    procedure quPersCalcFields(DataSet: TDataSet);
    procedure quTabsCalcFields(DataSet: TDataSet);
    procedure quMenuCalcFields(DataSet: TDataSet);
    procedure taSpecAllSelCalcFields(DataSet: TDataSet);
    procedure quRepLogCalcFields(DataSet: TDataSet);
    procedure quSelSpecAllCalcFields(DataSet: TDataSet);
    procedure quSelSpecAllQUANTITY1Change(Sender: TField);
    procedure quSelSpecAllINEEDChange(Sender: TField);
    procedure quCheckOrgItogCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
//    Procedure prSendRing;
//    Function SelFont(iNum:Integer):Boolean;
    Procedure PrintStr(StrP,S:String);
//    Procedure CutDocPr;
    Procedure CutDocPrSpec;
//    Procedure prOpenDevPrint(StrP:string); //
    Procedure OpenMoneyBox;

    Procedure prDevOpen(StrP:String;iLog:ShortInt);
    Procedure prDevClose(StrP:String;iLog:ShortInt);
    Procedure prCutDoc(StrP:String;iLog:ShortInt); //�������
    Procedure prRing(StrP:String); //��������
    Procedure prSetFont(StrP:String;iFont:INteger;iLog:ShortInt); //��������� �����

    Procedure prPrintStr(StrP,S:String); //  


    procedure PrintBn(BS:String);
    function fTestPaper:Boolean;
    procedure prRelease;
    procedure prTest;

  end;

Function GetId(ta:String):Integer;
Function CanDo(Name_F:String):Boolean;
Function FindPers(sP:String):Boolean;
Procedure CalcDiscont(Sifr,Id:Integer;Price,Quantity,Summa:Real;Discount:String;CurDateTime:TDateTime; Var DiscProc,DiscSum:Real);
Procedure FormLog(NAMEOP,CONTENT:String);
Function FindDiscount(sBar:String):Boolean;
Procedure CalcDiscontN(Sifr,Id,Parent,Stream:Integer;Price,Quantity,Summa:Real;Discount:String;CurDateTime:TDateTime; Var DiscProc,DiscSum:Real);
Function FindPCard(sBar:String):Boolean;
Procedure PrintServCh(sOp:String);
Function IsOpenZ:Boolean;
procedure prDiscHist(DateZ:TDateTime;NumZ:INteger;Proc,DSum:Real);
Procedure prWrBuf(StrP:String);
function FindCardT(Sifr:Integer):INteger;
function CalcBonus(IdH:INteger;DISCPROC:Real):Real;
procedure prFormMenuList(iParent:INteger);
procedure prAddMove(Oper,Code:Integer;rQ:Real);
function prTestRemn(Var StrMess:String):Boolean;
function prGetPrePrintPort:String;


var
  dmC: TdmC;
  ABitmap:TBitMap;

implementation

uses MainAdm, Un1, Passw, UnCash, u2fdk, uDB1;

{$R *.dfm}

Procedure TdmC.prTest;
begin
  if TVAComm(FindComponent('DevPrint'))<>nil then prDevOpen(CommonSet.SpecBoxX,0);
end;


function prGetPrePrintPort:String;
begin
  Result:='0';
  with dmC do
  begin
    quPServ.Active:=False;
    quPServ.ParamByName('IST').AsInteger:=CommonSet.Station;
    quPServ.Active:=True;

    quPServ.First;
    while not quPServ.Eof do
    begin
      if quPServISTREAM.AsInteger=999 then begin Result:=quPServSPORT.AsString; break; end;
      quPServ.Next;
    end;
    quPServ.Active:=False;
    if Result='' then Result:='0';
  end;
end;

function prTestRemn(Var StrMess:String):Boolean;
begin
  with dmC do
  begin
    Result:=True;
    StrMess:='';

    quTestRemn.Active:=False;
    quTestRemn.ParamByName('STATION').AsInteger:=CommonSet.Station;
    quTestRemn.ParamByName('IDT').AsInteger:=Tab.Id;
    quTestRemn.Active:=True;
    if quTestRemn.RecordCount>0 then
    begin
      Result:=False;

      StrMess:='������������ ������� : '+#13;

      quTestRemn.First;
      while not quTestRemn.Eof do
      begin
        StrMess:=StrMess+quTestRemnNAME.AsString+' ('+fts(quTestRemnBACKBGR.AsFloat-quTestRemnQUANTITY.AsFloat)+')'+#13;
        quTestRemn.Next;
      end;
    end;
    quTestRemn.Active:=False;
  end;
end;


procedure prFormMenuList(iParent:INteger);
Var sW,sT,sD:String;
    iWeek:Integer;
begin
  with dmC do
  begin
    sW:=FormatDateTime('ddd',Date);

    iWeek:=1;
    if Pos('��',sW)>0 then iWeek:=1;
    if Pos('��',sW)>0 then iWeek:=2;
    if Pos('��',sW)>0 then iWeek:=3;
    if Pos('��',sW)>0 then iWeek:=4;
    if Pos('��',sW)>0 then iWeek:=5;
    if Pos('��',sW)>0 then iWeek:=6;
    if Pos('��',sW)>0 then iWeek:=7;

    if Pos('Mon',sW)>0 then iWeek:=1;
    if Pos('Tue',sW)>0 then iWeek:=2;
    if Pos('Wed',sW)>0 then iWeek:=3;
    if Pos('Thu',sW)>0 then iWeek:=4;
    if Pos('Fri',sW)>0 then iWeek:=5;
    if Pos('Sat',sW)>0 then iWeek:=6;
    if Pos('Sun',sW)>0 then iWeek:=7;

    sW:=its(iWeek);

    iWeek:=Trunc(date);
    sD:=its(iWeek);

    sT:=FormatDateTime('hh:nn',now);

    quMenu.Active:=False;
    quMenu.SelectSQL.Clear;
    quMenu.SelectSQL.Add('Select Sifr, Name, Parent, Code, Price, TreeType, Link, LimitPrice, Stream, Designsifr, MINREST, PRNREST, BACKBGR');
    quMenu.SelectSQL.Add('from menu');
    quMenu.SelectSQL.Add('where Parent='+its(iParent));
    quMenu.SelectSQL.Add('and IACTIVE=1');
    quMenu.SelectSQL.Add('and ((ALLTIME=1)');
    quMenu.SelectSQL.Add('or ((ALLTIME=0)and(DAYWEEK like ''%'+sW+'%'')and(DATEB<='+sD+')and(DATEE>='+sD+')and(TIMEB<='''+sT+''')and(TIMEE>='''+sT+''')))');
    quMenu.SelectSQL.Add('and ');
    quMenu.SelectSQL.Add('((Sifr in ( select id_classif-10000 from rclassif where id_personal='+its(Person.Id)+' and rights=0) and TreeType=''T'')');
    quMenu.SelectSQL.Add('or (TreeType=''F''))');
    quMenu.SelectSQL.Add('Order by TreeType,Name');

{Select Sifr, Name, Parent, Code, Price, TreeType, Link, LimitPrice, Stream, Designsifr
from menu
where Parent=30006
and IACTIVE=1
and ((ALLTIME=1)
or ((ALLTIME=0)and(DAYWEEK like '%1%')and(DATEB<=40520)and(DATEE>=40520)and(TIMEB<='10:01')and(TIMEE>='23:30')))
Order by TreeType,Name
}
    quMenu.Active:=True;
  end;
end;

function CalcBonus(IdH:INteger;DISCPROC:Real):Real;
Var StrWk:String;
    iWeek:Integer;
    BonusSum:Real;
begin
//  BonusSum:=0;

  with dmC do
  begin

    StrWk:=FormatDateTime('ddd',Date);

    iWeek:=1;
    if Pos('��',StrWk)>0 then iWeek:=1;
    if Pos('��',StrWk)>0 then iWeek:=2;
    if Pos('��',StrWk)>0 then iWeek:=3;
    if Pos('��',StrWk)>0 then iWeek:=4;
    if Pos('��',StrWk)>0 then iWeek:=5;
    if Pos('��',StrWk)>0 then iWeek:=6;
    if Pos('��',StrWk)>0 then iWeek:=7;

    if Pos('Mon',StrWk)>0 then iWeek:=1;
    if Pos('Tue',StrWk)>0 then iWeek:=2;
    if Pos('Wed',StrWk)>0 then iWeek:=3;
    if Pos('Thu',StrWk)>0 then iWeek:=4;
    if Pos('Fri',StrWk)>0 then iWeek:=5;
    if Pos('Sat',StrWk)>0 then iWeek:=6;
    if Pos('Sun',StrWk)>0 then iWeek:=7;


    prCalcBonus.ParamByName('IDH').AsInteger:=IdH;
    prCalcBonus.ParamByName('DISCPROC').AsFloat:=DISCPROC;
    prCalcBonus.ParamByName('CURTIME').AsFloat:=frac(now);
    prCalcBonus.ParamByName('DAYWEEK').AsInteger:=iWeek;

    prCalcBonus.ExecProc;

    BonusSum:=prCalcBonus.ParamByName('BONUSSUM').AsFloat;

    prWriteLog('--CalcBonus;'+its(IdH)+';'+fts(DISCPROC)+';'+its(iWeek)+';'+fts(BonusSum));

//?IDH, ?DISCPROC, ?CURTIME, ?DAYWEEK)

    Result:=BonusSum;
  end;
end;

function FindCardT(Sifr:Integer):INteger;
begin
  with dmC do
  begin
    Result:=0;
    quSelCard.ParamByName('ID').AsInteger:=Sifr;
    quSelCard.ExecQuery;
    if quSelCard.RecordCount>0 then
    begin
      if quSelCard.FieldByName('DESIGNSIFR').AsInteger>0 then Result:=1;
    end;
  end;
end;

Procedure prWrBuf(StrP:String);
begin
  if pos('COM',StrP)>0 then
  begin
    dmC.DevPrint.WriteBuf(BufPr,BufPr.iC);
  end;
end;


procedure prDiscHist(DateZ:TDateTime;NumZ:INteger;Proc,DSum:Real);
begin
  //EXECUTE PROCEDURE PR_DISCHIST (?DATEZ, ?NUMZ, ?IDPERS, ?SPERS, ?PROC, ?DSUM, ?DTIME)
  with dmC do
  begin
    prDisc_Hist.ParamByName('DATEZ').AsDate:=DateZ;
    prDisc_Hist.ParamByName('NUMZ').AsInteger:=NumZ;
    prDisc_Hist.ParamByName('IDPERS').AsINteger:=Person.Id;
    prDisc_Hist.ParamByName('SPERS').AsString:=Person.Name;
    prDisc_Hist.ParamByName('PROC').AsFloat:=Proc;
    prDisc_Hist.ParamByName('DSUM').AsFloat:=DSum;
    prDisc_Hist.ExecProc;
  end;
end;

Function IsOpenZ:Boolean;
begin
  if CommonSet.TestOpenTBeforeClose=0 then result:=False
  else
  begin
    result:=False;
    with dmC do
    begin
      quOpenZ.Active:=False;
      quOpenZ.Active:=True;
      if quOpenZ.RecordCount>0 then Result:=True;
      quOpenZ.Active:=False;
    end;
  end;
end;


procedure TdmC.prRelease;
Var Buff:Array[1..3] of Char;
begin
  Buff[1]:=#$1B;
  Buff[2]:=#$71;
  DevPrint.WriteBuf(Buff,2);
  delay(10);
end;

function TdmC.fTestPaper:Boolean;
Var Buff,Buff1:Array[1..3] of Char;
    bRet:Byte;
begin
  result:=True;
  Buff[1]:=#$10;
  Buff[2]:=#$04;
  Buff[3]:=#$05;
  DevPrint.WriteBuf(Buff,3);
  delay(100);
  DevPrint.ReadBuf(Buff1,1);
  delay(10);
  bRet:=ord(Buff1[1]);
  if bRet=18 then Result:=True;
  if bRet=114 then Result:=False;
end;

procedure TdmC.PrintBn(BS:String);
Var Strb,Strb1:String;
begin
  if BS>'' then //�������� ������ ������ � ���
  begin
    Strb:=BS;
    while Strb>'' do
    begin
      while Strb[1]='|' do
      begin
        Delete(Strb,1,1);
        PrintNfStr(' ');
      end;
      if Pos('|',Strb)>0 then
      begin
        Strb1:=Copy(Strb,1,pos('|',Strb)-1);
        Delete(Strb,1,pos('|',Strb));
        PrintNfStr(Strb1);
      end else
      begin
        if Strb>'' then PrintNfStr(Strb);
        Strb:='';
      end;
    end;
    PrintNfStr(' ');
    PrintNfStr(' ');
    PrintNfStr(' ');
    PrintNfStr(' ');
    PrintNfStr(' ');
    PrintNfStr(' ');
    delay(100);
    CutDoc;
  end;
end;

Procedure TdmC.prPrintStr(StrP,S:String);
Var Buff:Array[1..100] of Char;
    i,iCount:Integer;
begin
  prWriteLogPS('   ������ ������ ('+StrP+') "'+S+'"');
  if pos('fis',StrP)>0 then PrintNFStr(S);

  if (StrP[1]='S')or(StrP[1]='s')or(StrP[1]='B')or(StrP[1]='b') then
  begin
    if S='-' then s:='----------------------------------------'
  end else
  begin
    if S='-' then s:='                                        '
  end;
  if pos('COM',StrP)>0 then
  begin

    S:=AnsiToOemConvert(S);
    iCount:=Length(S);

//    for i:=1 to iCount do Buff[i]:=S[i];
    for i:=1 to iCount do prInBuf(S[i]);

    Buff[iCount+1]:=#$0A;
    prInBuf(#$0A);

    try
//      DevPrint.WriteBuf(Buff,iCount+1);
    except
    end;
//    delay(CommonSet.ComDelay);
  end;
  if pos('LPT',StrP)>0 then
  begin
    if (StrP[1]='S')or(StrP[1]='s') then
    begin
      if bPrintOpen then prWritePrinter(AnsiToOemConvert(S)+#$0D+#$0A);
    end else
    begin
      if bPrintOpen then prWritePrinter(AnsiToOemConvert(S)+#$0D+#$0A);
    end;
  end;
end;


Procedure TdmC.prSetFont(StrP:String;iFont:INteger;iLog:ShortInt); //��������� �����
Var iFl,iFl1:Byte;
    Buff:Array[1..100] of Char;
    iTypeP:INteger;
begin
  if iLog=1 then prWriteLogPS('   ��������� ����� '+IntToStr(iFont)+'('+StrP+')');
  if pos('fis',StrP)>0 then SelectF(iFont);
  if pos('COM',StrP)>0 then
  begin
    iTypeP:=0;
    if (StrP[1]='S')or(StrP[1]='s') then iTypeP:=1;

    if iTypeP=0 then //������ �� ���������
    begin
      iFl:=$00;
      Case iFont of
      0: begin iFl:=$01 end;  //�������
      1: begin iFl:=$21 end;  //������� ������
      2: begin iFl:=$11 end;  //������� ������
      3: begin iFl:=$81 end;  //�������������
      4: begin iFl:=$31 end;  //������� ��� � ���

      5: begin iFl:=$09 end;  //�������
      6: begin iFl:=$29 end;  //������� ������
      7: begin iFl:=$19 end;  //������� ������
      8: begin iFl:=$89 end;  //�������������
      9: begin iFl:=$39 end;  //������� ��� � ���

      10: begin iFl:=$00 end; //�������
      11: begin iFl:=$20 end; //������� ������
      12: begin iFl:=$10 end; //������� ������
      13: begin iFl:=$80 end; //�������������
      14: begin iFl:=$30 end; //������� ��� � ���

      15: begin iFl:=$08 end; //�������
      16: begin iFl:=$28 end; //������� ������
      17: begin iFl:=$18 end; //������� ������
      18: begin iFl:=$88 end; //�������������
      19: begin iFl:=$38 end; //������� ��� � ���

      end;

      Buff[1]:=#$1B;
      Buff[2]:=#$21;
      Buff[3]:=chr(iFl);

      prInBuf(Buff[1]);
      prInBuf(Buff[2]);
      prInBuf(Buff[3]);

      try
//        DevPrint.WriteBuf(Buff,3);
      except
      end;
    end;

    if iTypeP=1 then //Star600
    begin
      iFl:=$30;  iFl1:=$30;
      Case iFont of
      10: begin iFl:=$30; iFl1:=$30; end; //�������
      13: begin iFl:=$30; iFl1:=$30; end; //�������������
      14: begin iFl:=$31; iFl1:=$31; end; //������� ��� � ���
      15: begin iFl:=$30; iFl1:=$30; end; //�������
      end;

      Buff[1]:=#$1B;
      Buff[2]:=#$69; // 30 30
      Buff[3]:=chr(iFl);
      Buff[4]:=chr(iFl1);

      prInBuf(Buff[1]);
      prInBuf(Buff[2]);
      prInBuf(Buff[3]);
      prInBuf(Buff[4]);

      try
//        DevPrint.WriteBuf(Buff,4);
      except
      end;
    end;
  end;

  if pos('LPT',StrP)>0 then
  begin
    iTypeP:=0;
    if (StrP[1]='S')or(StrP[1]='s') then iTypeP:=1;
    if (StrP[1]='B')or(StrP[1]='b') then iTypeP:=2;
    if iTypeP=1 then //Star600
    begin
      iFl:=$30;  iFl1:=$30;
      Case iFont of
      10: begin iFl:=$30; iFl1:=$30; end; //�������
      13: begin iFl:=$30; iFl1:=$30; end; //�������������
      14: begin iFl:=$31; iFl1:=$31; end; //������� ��� � ���
      15: begin iFl:=$30; iFl1:=$30; end; //�������
      end;

      if bPrintOpen then prWritePrinter(#$1B+#$69+chr(iFl)+chr(iFl1));
    end;
    if iTypeP=2 then //Star600
    begin
      iFl:=$00;
      Case iFont of
      0: begin iFl:=$01 end;  //�������
      1: begin iFl:=$21 end;  //������� ������
      2: begin iFl:=$11 end;  //������� ������
      3: begin iFl:=$81 end;  //�������������
      4: begin iFl:=$31 end;  //������� ��� � ���

      5: begin iFl:=$09 end;  //�������
      6: begin iFl:=$29 end;  //������� ������
      7: begin iFl:=$19 end;  //������� ������
      8: begin iFl:=$89 end;  //�������������
      9: begin iFl:=$39 end;  //������� ��� � ���

      10: begin iFl:=$00 end; //�������
      11: begin iFl:=$20 end; //������� ������
      12: begin iFl:=$10 end; //������� ������
      13: begin iFl:=$80 end; //�������������
      14: begin iFl:=$30 end; //������� ��� � ���

      15: begin iFl:=$08 end; //�������
      16: begin iFl:=$28 end; //������� ������
      17: begin iFl:=$18 end; //������� ������
      18: begin iFl:=$88 end; //�������������
      19: begin iFl:=$38 end; //������� ��� � ���
      end;

      if bPrintOpen then prWritePrinter(#$1B+#$21+chr(iFl));
    end;
  end;
end;


Procedure TdmC.prRing(StrP:String); //��������
Var Buff:Array[1..100] of Char;
begin
  if pos('COM',StrP)>0 then
  begin
    if (StrP[1]='S')or(StrP[1]='s') then
    begin

    end else
    begin
      Buff[1]:=#$1B;
      Buff[2]:=#$70;
      Buff[3]:=#$00;
      Buff[4]:=#$FF;
      Buff[5]:=#$FF;
      try
        DevPrint.WriteBuf(Buff,5);
        delay(500);
      except
      end;
    end;
  end;
end;


Procedure TdmC.prCutDoc(StrP:String;iLog:ShortInt); //�������
//  Memo1.Lines.Add('�����');
Var Buff:Array[1..14] of Char;
//    i:Integer;
    iTypeP:INteger;
//    kDelay:INteger;
begin
//  SelFont(0);
  if iLog=1 then prWriteLogPS(' �����.('+StrP+')');
  iTypeP:=0; //��� �� ���������
  if pos('fis',StrP)>0 then
  begin
    PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');
    CutDoc;
  end;

  if pos('COM',StrP)>0 then
  begin
    if (StrP[1]='S')or(StrP[1]='s') then iTypeP:=1;
    if (StrP[1]='A')or(StrP[1]='a') then iTypeP:=2;

    if (iTypeP=2)or(iTypeP=0) then //������ � ��� ���������
    begin
      {Buff[1]:=#$0A;

      try
        for i:=1 to 7 do DevPrint.WriteBuf(Buff,1); //�������
      except
      end;

      Buff[1]:=#$1B;
      Buff[2]:=#$69;
      try
        DevPrint.WriteBuf(Buff,2);
      except
      end;
      }


      delay(CommonSet.ComDelay*2); //��� ����� �� ��������

{      kDelay:=(BufPr.iC div 40)-10;
      if kDelay<0 then kDelay:=0;
      delay(CommonSet.ComDelay*5+trunc(kDelay*CommonSet.ComDelay/2));
}
      prSetFont(StrP,0,0);
      PrintStr(StrP,' ');
      PrintStr(StrP,' ');

      Buff[1]:=#$20;
      Buff[2]:=#$0A;

      Buff[3]:=#$20;
      Buff[4]:=#$0A;

      Buff[5]:=#$20;
      Buff[6]:=#$0A;

      Buff[7]:=#$20;
      Buff[8]:=#$0A;

      Buff[9]:=#$20;
      Buff[10]:=#$0A;

      Buff[11]:=#$1B;
      Buff[12]:=#$69;

      Buff[13]:=#$00;
      Buff[14]:=#$00;

      try
        DevPrint.WriteBuf(Buff,12);
        delay(CommonSet.ComDelay);
      except
      end;

//      PrintStr(StrP,' ');
    end;
    if iTypeP=1 then //Star 600
    begin
      Buff[1]:=#$1B;
      Buff[2]:=#$64;
      Buff[3]:=#$33;
      try
        DevPrint.WriteBuf(Buff,3);
        delay(100);
      except
      end;
    end;
  end;
  if pos('LPT',StrP)>0 then
  begin
    if (StrP[1]='S')or(StrP[1]='s') then iTypeP:=1;
    if (StrP[1]='B')or(StrP[1]='b') then iTypeP:=2;
    if iTypeP=1 then //Star 600
    begin
      if bPrintOpen then prWritePrinter(#$1B+#$64+#$33);
    end else
    begin    //��� ������ ��� ����������
      {
      if iTypeP=2 then
      begin
        if bPrintOpen then
        begin
          for i:=1 to 7 do prWritePrinter(#$0A);
          prWritePrinter(#$1B+#$69);
        end;
      end else
        if bPrintOpen then
        begin
          prWritePrinter(#$1B+#$69);
        end;}
    end;
  end;
//  BufPr.iC:=0;
end;


Procedure TdmC.prDevClose(StrP:String;iLog:ShortInt);
Var pName:String;
    kDelay:INteger;
begin //�������� ����������
  if iLog=1 then prWriteLogPS(' ��������.('+StrP+')');
  if pos('fis',StrP)>0 then CloseNFDoc;
  if pos('COM',StrP)>0 then
  begin
    try
      kDelay:=(BufPr.iC div 40)-10;
      if kDelay<0 then kDelay:=0;
      delay(CommonSet.ComDelay*5+trunc(kDelay*CommonSet.ComDelay/2));
      DevPrint.Close;
//      delay(CommonSet.ComDelay);
    except
    end;
  end;
  if pos('LPT',StrP)>0 then
  begin
    pName:=Copy(StrP,POS('LPT',StrP)+3,length(StrP)-POS('LPT',StrP)+2);
//    Memo1.Lines.Add(FormatDateTime('dd.mm.yyyy hh.nn.ss',now)+'  '+pName);
    prClosePrinter(pName);
  end;
  BufPr.iC:=0;
end;


Procedure TdmC.prDevOpen(StrP:String;iLog:ShortInt);
Var Buff:Array[1..3] of Char;
    bAxiohm,bEpson:Boolean;
    PName:String;
begin  //�������� ����������
//  Memo1.Lines.Add('����-'+StrP);
  BufPr.iC:=0;
  
  if iLog=1 then prWriteLogPS('   �������� - '+StrP);
  if pos('fis',StrP)>0 then OpenNFDoc;

  if pos('COM',StrP)>0 then
  begin
    try
      bAxiohm:=False;
      bEpson:=False;
      if (StrP[1]='A')or(StrP[1]='Z') then
      begin
        bAxiohm:=True;
        Delete(StrP,1,1);
      end;
      if (StrP[1]='E')or(StrP[1]='e') then
      begin
        bEpson:=True;
        Delete(StrP,1,1); //���� � �������� ����� ������ E - �� Epson ��������
      end;

      if (StrP[1]='S')then Delete(StrP,1,1);

      while pos(' ',StrP)>0 do delete(StrP,pos(' ',StrP),1);

      DevPrint.DeviceName:=StrP;
      DevPrint.Open;

   //��� ����������� ��� ������� - � ������ ��������� ����� �� �����
      if bAxiohm then
      begin
        Buff[1]:=#$1B;
        Buff[2]:=#$74;
        Buff[3]:=#$07;
        DevPrint.WriteBuf(Buff,3);
      end;
    //��� ����������� ��� Epsona - � ������ ��������� ����� �� �����
      if bEpson then
      begin
        Buff[1]:=#$1B;
        Buff[2]:=#$74;
        Buff[3]:=#$11;
        DevPrint.WriteBuf(Buff,3);
      end;

    except
      if iLog=1 then prWriteLogPS('   ������ �������� - '+StrP);
    end;
  end;
  if pos('LPT',StrP)>0 then
  begin
    pName:=Copy(StrP,POS('LPT',StrP)+3,length(StrP)-POS('LPT',StrP)+2);
    if prOpenPrinter(pName) then bPrintOpen:=True else bPrintOpen:=False;
  end;
end;



Procedure TdmC.OpenMoneyBox;
Var Buff:Array[1..5] of Char;
begin
  if CommonSet.SpecBox<>'0' then
  begin
    try
      DevPrint.Close;
      DevPrint.DeviceName:=CommonSet.SpecBox;
      DevPrint.Open;

      Buff[1]:=#$1B;   //�������������
      Buff[2]:=#$40;
      DevPrint.WriteBuf(Buff,2);
      delay(50);

      Buff[1]:=#$1B;   //�������������
      Buff[2]:=#$70;
      Buff[3]:=#$00;
      Buff[4]:=#$32;
      Buff[5]:=#$32;

      DevPrint.WriteBuf(Buff,5);
      delay(50);

    finally
      DevPrint.Close;
    end;
  end;  
end;
{

  DevPrint.Close;
  DevPrint.DeviceName:='COM'+INtToStr(cxSpinEdit4.Value);
  DevPrint.Open;
  Delay(100);

  Buff[1]:=#$1B;   //�������������
  Buff[2]:=#$40;
  DevPrint.WriteBuf(Buff,2);
  delay(100);

  Buff[1]:=#$1B;   //�������������
  Buff[2]:=#$70;
  Buff[3]:=#$00;
  Buff[4]:=#$32;
  Buff[5]:=#$32;

  DevPrint.WriteBuf(Buff,5);

  delay(100);

  DevPrint.Close;

}
{
Procedure TdmC.prOpenDevPrint(StrP:string);
Var Buff:Array[1..3] of Char;
    bAxiohm,bEpson:Boolean;
    PName:String;
begin
  if pos('fis',StrP)>0 then OpenNFDoc;

  if pos('COM',StrP)>0 then
  begin
    try
      bAxiohm:=False;
      bEpson:=False;
      if (StrP[1]='A') or(StrP[1]='Z') then
      begin
        bAxiohm:=True;
        Delete(StrP,1,1);
      end;
      if (StrP[1]='E')or(StrP[1]='e') then
      begin
        bEpson:=True;
        Delete(StrP,1,1); //���� � �������� ����� ������ E - �� Epson ��������
      end;

      if (StrP[1]='S')then Delete(StrP,1,1);

      while pos(' ',StrP)>0 do delete(StrP,pos(' ',StrP),1);

      DevPrint.DeviceName:=StrP;
      DevPrint.Open;

   //��� ����������� ��� ������� - � ������ ��������� ����� �� �����
      if bAxiohm then
      begin
        Buff[1]:=#$1B;
        Buff[2]:=#$74;
        Buff[3]:=#$07;
        DevPrint.WriteBuf(Buff,3);
      end;
  //��� ����������� ��� Epsona - � ������ ��������� ����� �� �����
      if bEpson then
      begin
        Buff[1]:=#$1B;
        Buff[2]:=#$74;
        Buff[3]:=#$11;
        DevPrint.WriteBuf(Buff,3);
      end;
    except
    end;
  end;
  if pos('LPT',StrP)>0 then
  begin
    pName:=Copy(StrP,POS('LPT',StrP)+3,length(StrP)-POS('LPT',StrP)+2);
    if prOpenPrinter(pName) then bPrintOpen:=True else bPrintOpen:=False;
  end;
end;
}
Procedure TdmC.CutDocPrSpec;
Var Buff:Array[1..7] of Char;
    Buff1:Array[1..43] of Char;
    i:Integer;
    StrP:String;
begin
  prSetFont(CommonSet.PrePrintPort,10,0);
  StrP:=CommonSet.PrePrintPort;
  PrintStr(StrP,SpecVal.SPE1);

///  delay(100);  //��� ����� �� ��������

{  Buff[1]:=#$1B;   //�������������
  Buff[2]:=#$40;
  DevPrint.WriteBuf(Buff,2);
  delay(100);}

  Buff[1]:=#$1B;   //����� �������������� ������� ��������
  Buff[2]:=#$5C;
//  Buff[3]:=#$E9;
  Buff[3]:=#$C4;
  Buff[4]:=#$00;

  for i:=1 to 4 do prINBuf(Buff[i]);

//  DevPrint.WriteBuf(Buff,4);
///  delay(100);


  Buff1[1]:=#$1B;  //������ ������� �����
  Buff1[2]:=#$2A;
  Buff1[3]:=#$01;
  Buff1[4]:=#$26;
  Buff1[5]:=#$00;

  Buff1[6]:=#$01; Buff1[7]:=#$01;
  Buff1[8]:=#$02; Buff1[9]:=#$02;
  Buff1[10]:=#$04;Buff1[11]:=#$04;
  Buff1[12]:=#$0A;Buff1[13]:=#$0A;
  Buff1[14]:=#$14;Buff1[15]:=#$14;
  Buff1[16]:=#$18;Buff1[17]:=#$18;
  Buff1[18]:=#$18;Buff1[19]:=#$18;
  Buff1[20]:=#$18;Buff1[21]:=#$18;
  Buff1[22]:=#$18;Buff1[23]:=#$18;
  Buff1[24]:=#$1B;Buff1[25]:=#$1B;
  Buff1[26]:=#$3F;Buff1[27]:=#$7F;
  Buff1[28]:=#$7C;Buff1[29]:=#$7C;
  Buff1[30]:=#$58;Buff1[31]:=#$58;
  Buff1[32]:=#$58;Buff1[33]:=#$58;
  Buff1[34]:=#$14;Buff1[35]:=#$14;
  Buff1[36]:=#$0A;Buff1[37]:=#$0A;
  Buff1[38]:=#$05;Buff1[39]:=#$05;
  Buff1[40]:=#$02;Buff1[41]:=#$02;
  Buff1[42]:=#$01;Buff1[43]:=#$01;

//0	0	0	0	0	0	0	0	1	1	1	1	1	1	1	1	1	1	1	1	3	7	7	7	5	5	5	5	1	1	0	0	0	0	0	0	0	0
//1	1	2	2	4	4	A	A	4	4	8	8	8	8	8	8	8	8	B	B	F	F	C	C	8	8	8	8	4	4	A	A	5	5	2	2	1	1

  for i:=1 to 43 do prINBuf(Buff1[i]);

  try
//    DevPrint.WriteBuf(Buff1,43);
  except
  end;
///  delay(200);

  Buff[1]:=#$1B;     //����� ����
  Buff[2]:=#$4A;
  Buff[3]:=#$00;

  for i:=1 to 3 do prINBuf(Buff[i]);

//  DevPrint.WriteBuf(Buff,3);
///  delay(100);

  Buff[1]:=#$1B;     //����� ������
  Buff[2]:=#$5C;
  Buff[3]:=#$C4;
//  Buff[3]:=#$E9;
  Buff[4]:=#$00;

  for i:=1 to 4 do prINBuf(Buff[i]);

//  DevPrint.WriteBuf(Buff,4);
//  delay(100);

  Buff1[1]:=#$1B;     //������ ������ �����
  Buff1[2]:=#$2A;
  Buff1[3]:=#$01;
  Buff1[4]:=#$26;
  Buff1[5]:=#$00;

  Buff1[6]:=#$80; Buff1[7]:=#$80;
  Buff1[8]:=#$40; Buff1[9]:=#$40;
  Buff1[10]:=#$20;Buff1[11]:=#$20;
  Buff1[12]:=#$50;Buff1[13]:=#$52;
  Buff1[14]:=#$2A;Buff1[15]:=#$2A;
  Buff1[16]:=#$1A;Buff1[17]:=#$1A;
  Buff1[18]:=#$1A;Buff1[19]:=#$1A;
  Buff1[20]:=#$3E;Buff1[21]:=#$3E;
  Buff1[22]:=#$FE;Buff1[23]:=#$FC;
  Buff1[24]:=#$D8;Buff1[25]:=#$D8;
  Buff1[26]:=#$18;Buff1[27]:=#$18;
  Buff1[28]:=#$18;Buff1[29]:=#$18;
  Buff1[30]:=#$18;Buff1[31]:=#$18;
  Buff1[32]:=#$18;Buff1[33]:=#$18;
  Buff1[34]:=#$28;Buff1[35]:=#$28;
  Buff1[36]:=#$50;Buff1[37]:=#$50;
  Buff1[38]:=#$A0;Buff1[39]:=#$A0;
  Buff1[40]:=#$40;Buff1[41]:=#$40;
  Buff1[42]:=#$80;Buff1[43]:=#$80;

//8	8	4	4	2	2	5	5	2	2	1	1	1	1	3	3	F	F	D	D	1	1	1	1	1	1	1	1	2	2	5	5	A	A	4	4	8	8
//0	0	0	0	0	0	0	2	A	A	A	A	A	A	E	C	C	C	8	8	8	8	8	8	8	8	8	8	8	8	0	0	0	0	0	0	0	0

  for i:=1 to 43 do prINBuf(Buff1[i]);

  try
//    DevPrint.WriteBuf(Buff1,43);
  except
  end;
//  delay(200);

  Buff[1]:=#$0A; prINBuf(Buff[1]);

//  DevPrint.WriteBuf(Buff,1); //������� ��� ������
  prSetFont(StrP,10,0); PrintStr(StrP,SpecVal.SPE2);

  try
///    for i:=1 to 7 do DevPrint.WriteBuf(Buff,1); //������� ��� ������
    for i:=1 to 6 do prINBuf(Buff[1]); //������� ��� ������
  except
  end;

//  delay(100);  //��� ����� �� ��������

{  Buff[1]:=#$1B;   //�������������
  Buff[2]:=#$40;
  DevPrint.WriteBuf(Buff,2);
  delay(100);}

  Buff[1]:=#$1B;
  Buff[2]:=#$69;
  for i:=1 to 2 do prINBuf(Buff[i]);

  try
///    DevPrint.WriteBuf(Buff,2);
  except
  end;
//  delay(100);  //��� ����� �� ��������
//  BufPr.iC:=0;
end;

{
Procedure TdmC.CutDocPr;
Var Buff:Array[1..20] of Char;
//    i:Integer;
begin
//  prSetFont(0);

  delay(100);  //��� ����� �� ��������

  Buff[1]:=#$20;
  Buff[2]:=#$0A;

  Buff[3]:=#$20;
  Buff[4]:=#$0A;

  Buff[5]:=#$20;
  Buff[6]:=#$0A;

  Buff[7]:=#$20;
  Buff[8]:=#$0A;

  Buff[9]:=#$20;
  Buff[10]:=#$0A;

  Buff[11]:=#$1B;
  Buff[12]:=#$69;

  Buff[13]:=#$00;
  Buff[14]:=#$00;


  try
    DevPrint.WriteBuf(Buff,12);
  except
  end;
end;
}

Procedure TdmC.PrintStr(StrP,S:String);
Var Buff:Array[1..100] of Char;
    i,iCount:Integer;
begin
  if pos('fis',StrP)>0 then PrintNFStr(S);

  if (StrP[1]='S')or(StrP[1]='s')or(StrP[1]='B')or(StrP[1]='b') then
  begin
    if S='-' then s:='----------------------------------------'
  end else
  begin
    if S='-' then s:='                                        '
  end;
  if pos('COM',StrP)>0 then
  begin

    S:=AnsiToOemConvert(S);
    iCount:=Length(S);
//    for i:=1 to iCount do Buff[i]:=S[i];
    for i:=1 to iCount do prINBuf(S[i]);
    Buff[iCount+1]:=#$0A; prINBuf(#$0A);

    try
//      DevPrint.WriteBuf(Buff,iCount+1);
    except
    end;
//    delay(CommonSet.ComDelay);
  end;
  if pos('LPT',StrP)>0 then
  begin
    if (StrP[1]='S')or(StrP[1]='s') then
    begin
      if bPrintOpen then prWritePrinter(AnsiToOemConvert(S)+#$0D+#$0A);
    end else
    begin
      if bPrintOpen then prWritePrinter(AnsiToOemConvert(S)+#$0D+#$0A);
    end;
  end;
end;







{Function TdmC.SelFont(iNum:Integer):Boolean;
Var iFl:Byte;
Var Buff:Array[1..100] of Char;
begin
  Result:=True;
  iFl:=$00;
  Case iNum of
  0: begin iFl:=$01 end;  //�������
  1: begin iFl:=$21 end;  //������� ������
  2: begin iFl:=$11 end;  //������� ������
  3: begin iFl:=$81 end;  //�������������
  4: begin iFl:=$31 end;  //������� ��� � ���

  5: begin iFl:=$09 end;  //�������
  6: begin iFl:=$29 end;  //������� ������
  7: begin iFl:=$19 end;  //������� ������
  8: begin iFl:=$89 end;  //�������������
  9: begin iFl:=$39 end;  //������� ��� � ���

  10: begin iFl:=$00 end; //�������
  11: begin iFl:=$20 end; //������� ������
  12: begin iFl:=$10 end; //������� ������
  13: begin iFl:=$80 end; //�������������
  14: begin iFl:=$30 end; //������� ��� � ���

  15: begin iFl:=$08 end; //�������
  16: begin iFl:=$28 end; //������� ������
  17: begin iFl:=$18 end; //������� ������
  18: begin iFl:=$88 end; //�������������
  19: begin iFl:=$38 end; //������� ��� � ���

  end;

  Buff[1]:=#$1B;
  Buff[2]:=#$21;
  Buff[3]:=chr(iFl);
  try
    DevPrint.WriteBuf(Buff,3);
  except
  end;
end;
}
{
Procedure TdmC.prSendRing;
Var Buff:Array[1..100] of Char;
begin
  Buff[1]:=#$1B;
  Buff[2]:=#$70;
  Buff[3]:=#$00;
  Buff[4]:=#$FF;
  Buff[5]:=#$FF;
  try
    DevPrint.WriteBuf(Buff,5);
//    delay(500);
  except
  end;
end;
}

Procedure PrintServCh(sOp:String);
Var iCount,n:Integer;
    StrWk,StrWk1:String;
    StrP,StrPN:String;
    bRing:Boolean;
    iQ,iRing:INteger;
    iLine:INteger;
    bHeadRet:Boolean;
    iNullStr:INteger;
begin
  with dmC do
  begin

  if taServP.Active=False then
  begin
    prWriteLog('!!PrintService; error taServP');
    exit; //������� ������ ���� �������
  end;

  delay(10);
  taServP.First;
  prWriteLog('!!PrintService;'+sOp+' ����� - '+IntToStr(taServP.RecordCount));

  if bFF then sOp:=Copy(sOp,1,6);//������ ���� ������ - ��� � �������� �������
  //������� �� �������

  quPServ.Active:=False;
  quPServ.ParamByName('IST').AsInteger:=CommonSet.Station;
  quPServ.Active:=True;

  quPServ.First;
  while not quPServ.Eof do
  begin
    n:=quPServISTREAM.AsInteger;
    StrP:=quPServSPORT.AsString;
    StrPN:=quPServNAMESTREAM.AsString;
    if quPServRING.AsInteger=1 then bRing:=True else bRing:=False;

    iCount:=0;
    taServP.First;
    while not taServP.eof do
    begin
      if taServPStream.AsInteger=n then inc(iCount);
      taServP.Next;
    end;
    if iCount>0 then //���� ��� �������� �� ���� ������
    begin
      prWriteLog('!!PrintService; ����� '+IntToStr(n)+', ����� '+IntToStr(iCount));

      if StrP='fisprim' then
      begin
        OpenNFDoc;

        SelectF(14);

        if bFF then   PrintNFStr(sOp+' ('+StrPN+')'+' ��� : '+IntToStr(Nums.iCheckNum))
        else PrintNFStr('    '+sOp+' ('+StrPN+')');
        SelectF(13);
        PrintNFStr(' ');
        PrintNFStr('��������: '+Tab.Name);
        PrintNFStr('����: '+Tab.NumTable+'  ������: '+IntToStr(Tab.Quests));
        PrintNFStr('����� ������: '+FormatDateTime('dd.mm.yyyy hh:nn:ss',Now));

        SelectF(3); StrWk:='                                                       ';
        PrintNFStr(StrWk);
        SelectF(15); StrWk:='        ��������                ���-��   ';
        PrintNFStr(StrWk);
        SelectF(3); StrWk:='                                                       ';
        PrintNFStr(StrWk);

        taServP.First;
        while not taServP.eof do
        begin
          if taServPStream.AsInteger=n then
          begin
            if taServPIType.AsInteger=0 then
            begin
              StrWk:=' ';
              StrWk:=StrWk + Copy(taServPName.AsString,1,28);

              while Length(StrWk)<29 do StrWk:=StrWk+' ';
              Str(taServPQuant.AsFloat:7:3,StrWk1);
//              StrWk:=StrWk+' '+StrWk1+' ��';
              StrWk:=StrWk+' '+StrWk1;
              SelectF(15);
              PrintNFStr(StrWk);
            end
            else
            begin
              StrWk:='   '+Copy(taServPName.AsString,1,35);
              while Length(StrWk)<40 do StrWk:=StrWk+' ';
              SelectF(10);
              PrintNFStr(StrWk);
            end;
          end;
          taServP.Next;
        end;
        SelectF(3); StrWk:='                                                       ';
        PrintNFStr(StrWk);
        SelectF(13);
        PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');
        CloseNFDoc;
        if CommonSet.TypeFis<>'sp101' then CutDoc;
//        CutDoc;
      end;
      if StrP='fisshtrih' then
      begin
        OpenNFDoc;
        if bFF then   PrintNFStr(sOp+' ('+StrPN+')'+' ��� : '+IntToStr(Nums.iCheckNum))
        else PrintNFStr('    '+sOp+' ('+StrPN+')');
        PrintNFStr(' ');
        PrintNFStr('��������: '+Tab.Name);
        PrintNFStr('����: '+Tab.NumTable+'  ������: '+IntToStr(Tab.Quests));
        PrintNFStr('����� ������: '+FormatDateTime('dd.mm.yyyy hh:nn:ss',Now));
        StrWk:='-----------------------------------';
        PrintNFStr(StrWk);
        StrWk:='        ��������          ���-��   ';
        PrintNFStr(StrWk);
        StrWk:='-----------------------------------';
        PrintNFStr(StrWk);

        taServP.First;
        while not taServP.eof do
        begin
          if taServPStream.AsInteger=n then
          begin
            if taServPIType.AsInteger=0 then
            begin
              StrWk:=' ';
              StrWk:=StrWk + Copy(taServPName.AsString,1,25);

              while Length(StrWk)<25 do StrWk:=StrWk+' ';
              Str(taServPQuant.AsFloat:7:3,StrWk1);
              StrWk:=StrWk+' '+StrWk1;
              PrintNFStr(StrWk);
            end
            else
            begin
              StrWk:='   '+Copy(taServPName.AsString,1,33);
              while Length(StrWk)<36 do StrWk:=StrWk+' ';
              PrintNFStr(StrWk);
            end;
          end;
          taServP.Next;
        end;
        StrWk:='-----------------------------------';
        PrintNFStr(StrWk);
        if CommonSet.TypeFis='fprint' then CutDoc
        else
        begin
          PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');
          CutDoc;
        end;
      end;
      if Pos('COM',StrP)>0 then
      begin  //����������� COM1
        try
          prDevOpen(StrP,0);  //���� ������ �� �������� �����������
          BufPr.iC:=0;

          if bRing then prRing(StrP);

          prSetFont(StrP,14,0);
          if bFF then   PrintStr(StrP,sOp+' ('+StrPN+')'+' ��� : '+IntToStr(Nums.iCheckNum))
          else PrintStr(StrP,'    '+sOp+' ('+StrPN+')');

          prSetFont(StrP,13,0); PrintStr(StrP,'');
          PrintStr(StrP,'��������: '+Tab.Name);
          PrintStr(StrP,'����: '+Tab.NumTable+'  ������: '+IntToStr(Tab.Quests));
          PrintStr(StrP,'����� ������: '+FormatDateTime('dd.mm.yyyy hh:nn:ss',Now));

          prSetFont(StrP,13,0); StrWk:='                                          ';
          PrintStr(StrP,StrWk);
          prSetFont(StrP,10,0); StrWk:='      ��������                  ���-��   ';
          PrintStr(StrP,StrWk);
          prSetFont(StrP,13,0); StrWk:='                                          ';
          PrintStr(StrP,StrWk);

          taServP.First;
          while not taServP.eof do
          begin
            if taServPStream.AsInteger=n then
            begin
              if taServPIType.AsInteger=0 then
              begin
                StrWk:=' ';
                StrWk:=StrWk + Copy(taServPName.AsString,1,28);

                while Length(StrWk)<29 do StrWk:=StrWk+' ';
                Str(taServPQuant.AsFloat:7:3,StrWk1);
                StrWk:=StrWk+' '+StrWk1;

                prSetFont(StrP,15,0);
                PrintStr(StrP,StrWk);
              end
              else
              begin
                StrWk:='   '+Copy(taServPName.AsString,1,35);
                while Length(StrWk)<40 do StrWk:=StrWk+' ';
                prSetFont(StrP,10,0);
                PrintStr(StrP,StrWk);
              end;
            end;
            taServP.Next;
          end;
          prSetFont(StrP,13,0);
          StrWk:='                                          ';
          PrintStr(StrP,StrWk);
          prWrBuf(StrP);
          prCutDoc(StrP,0);
        finally
          prDevClose(StrP,0);
        end;
      end;
      if StrP='G' then //������� �4/3
      begin
        taServP.Filtered:=False;
        taServP.Filter:='ITYPE=0 AND STREAM='+INtToStr(n);
        taServP.Filtered:=True;

        frRep1.LoadFromFile(CurDir + 'ServCheck1.frf');

        frVariables.Variable['Waiter']:=Tab.Name;
        frVariables.Variable['TabNum']:=Tab.NumTable;
        frVariables.Variable['OpenTime']:=FormatDateTime('dd.mm.yyyy hh:nn:ss',Now);
        frVariables.Variable['Quests']:=IntToStr(Tab.Quests);
        frVariables.Variable['ZNum']:=IntToStr(Tab.Id);
        frVariables.Variable['Stream']:=sOp+' ('+StrPN+')';

        frRep1.ReportName:='������ ���.';
        frRep1.PrepareReport;

//        frRep1.ShowPreparedReport;
        frRep1.PrintPreparedReportDlg;
        taServP.Filtered:=False;
      end;
      if Pos('DB',StrP)>0 then //���� � ����  DB1 ������; DB2 POSIFLEX AURA PP7000; DB3 STAR TSP600
      begin                    //� ������ �������� ����� ������ DB, � � ������ ������� ��� ��� - 1COM1,2COM1,3COM1 - c ���������� �������
        //��������� �������
        with dmC1 do
        begin
          quPrint.Active:=False;
          quPrint.Active:=True;
          iQ:=GetId('PQH'); //����������� ������� �� 1-� c����
          if bRing then iRing:=1 else iRing:=0;

          prWriteLog('!!PrintDB; ����� - '+IntToStr(n)+'('+StrP+')'+' ��.'+IntToStr(iQ));

          iNullStr:=CommonSet.AddStrsServis;  //������ ������ ����� �� ������ ��������
          while iNullStr>0 do PrintDBStr(iQ,n,StrP,'',13,iRing);

          if Pos('DB33',StrP)>0 then  //210 ����� �� 33 ������� ������������ ������
          begin

            PrintDBStr(iQ,n,StrP,sOp+' ('+StrPN+')',4,iRing);
            PrintDBStr(iQ,n,StrP,'',13,iRing);
            PrintDBStr(iQ,n,StrP,'����: '+Tab.NumTable+'  ������: '+IntToStr(Tab.Quests),4,iRing);
            PrintDBStr(iQ,n,StrP,'��������: '+Tab.Name,2,iRing);
            PrintDBStr(iQ,n,StrP,'����� ������: '+FormatDateTime('dd.mm.yyyy hh:nn:ss',Now),13,iRing);
            PrintDBStr(iQ,n,StrP,'_                               _',13,iRing);
            PrintDBStr(iQ,n,StrP,'      ��������          ���-��   ',10,iRing);
            PrintDBStr(iQ,n,StrP,'_                               _',13,iRing);

            iLine:=0;
            bHeadRet:=False;

            taServP.First;
            while not taServP.eof do
            begin
              if taServPStream.AsInteger=n then
              begin
                if taServPIType.AsInteger=0 then
                begin
                  if iLine>0 then //� ����� �� �������� ��������� ������ ���
                  begin
                    if (taServPiServ.AsInteger=1) or bHeadRet then //�� ����� ��������
                    begin
                      PrintDBStr(iQ,n,StrP,'_                               _',13,iRing);
                      prAddPrintQH(n,iQ,FormatDateTime('dd.mm hh:nn:sss',now)+', ������� - '+IntToStr(CommonSet.Station)+','+StrP);
                      iQ:=GetId('PQH'); //����������� ������� �� 1-� c����
                      if bRing then iRing:=1 else iRing:=0;

                      PrintDBStr(iQ,n,StrP,sOp+' ('+StrPN+')',4,iRing);
                      PrintDBStr(iQ,n,StrP,'',13,iRing);
                      PrintDBStr(iQ,n,StrP,'����: '+Tab.NumTable+'  ������: '+IntToStr(Tab.Quests),4,iRing);
                      PrintDBStr(iQ,n,StrP,'��������: '+Tab.Name,4,iRing);
                      PrintDBStr(iQ,n,StrP,'����� ������: '+FormatDateTime('dd.mm.yyyy hh:nn:ss',Now),13,iRing);
                      PrintDBStr(iQ,n,StrP,'_                               _',13,iRing);
                      PrintDBStr(iQ,n,StrP,'      ��������          ���-��   ',10,iRing);
                      PrintDBStr(iQ,n,StrP,'_                               _',13,iRing);

                      if taServPiServ.AsInteger=1 then bHeadRet:=True else bHeadRet:=False; //����� ���� ������� ����� ������� �� ����� ������ ���
                    end;
                  end else
                  begin
                    if taServPiServ.AsInteger=1 then bHeadRet:=True;
                  end;

                  StrWk:=' ';
                  StrWk:=StrWk + Copy(taServPName.AsString,1,26);
                  while Length(StrWk)<26 do StrWk:=StrWk+' ';
                  Str(taServPQuant.AsFloat:5:1,StrWk1);
                  StrWk:=StrWk+' '+StrWk1;
                  PrintDBStr(iQ,n,StrP,StrWk,15,iRing);

                  inc(iLine);

                end else
                begin
                  StrWk:='   '+Copy(taServPName.AsString,1,30);
                  while Length(StrWk)<33 do StrWk:=StrWk+' ';
                  PrintDBStr(iQ,n,StrP,StrWk,10,iRing);
                end;
              end;
              taServP.Next;
            end;
            PrintDBStr(iQ,n,StrP,'_                               _',13,iRing);
          end
          else
          begin
            PrintDBStr(iQ,n,StrP,sOp+' ('+StrPN+')',14,iRing);
            PrintDBStr(iQ,n,StrP,'',13,iRing);
            PrintDBStr(iQ,n,StrP,'��������: '+Tab.Name,14,iRing);
            PrintDBStr(iQ,n,StrP,'����: '+Tab.NumTable+'  ������: '+IntToStr(Tab.Quests),14,iRing);
            PrintDBStr(iQ,n,StrP,'����� ������: '+FormatDateTime('dd.mm.yyyy hh:nn:ss',Now),13,iRing);
            PrintDBStr(iQ,n,StrP,'----------------------------------------',10,iRing);
            PrintDBStr(iQ,n,StrP,'      ��������                  ���-��',10,iRing);
            PrintDBStr(iQ,n,StrP,'----------------------------------------',10,iRing);

            iLine:=0;
            bHeadRet:=False;

            taServP.First;
            while not taServP.eof do
            begin
              if taServPStream.AsInteger=n then
              begin
                if taServPIType.AsInteger=0 then
                begin
                  if pos('DBfis2',StrP)>0 then
                  begin  //�� ���������� �����   - ��� �� 2-� ������ �� 36 �������� - ���� �������� �������
                    if iLine>0 then //� ����� �� �������� ��������� ������ ���
                    begin
                      if (taServPiServ.AsInteger=1) or bHeadRet  then //�� ����� ��������
                      begin
                        PrintDBStr(iQ,n,StrP,'----------------------------------------',10,iRing);
                        prAddPrintQH(n,iQ,FormatDateTime('dd.mm hh:nn:sss',now)+', ������� - '+IntToStr(CommonSet.Station)+','+StrP);
                        iQ:=GetId('PQH'); //����������� ������� �� 1-� c����
                        if bRing then iRing:=1 else iRing:=0;


                        PrintDBStr(iQ,n,StrP,sOp+' ('+StrPN+')',14,iRing);
                        PrintDBStr(iQ,n,StrP,'',13,iRing);
                        PrintDBStr(iQ,n,StrP,'��������: '+Tab.Name,13,iRing);
                        PrintDBStr(iQ,n,StrP,'����: '+Tab.NumTable+'  ������: '+IntToStr(Tab.Quests),13,iRing);
                        PrintDBStr(iQ,n,StrP,'����� ������: '+FormatDateTime('dd.mm.yyyy hh:nn:ss',Now),13,iRing);
                        PrintDBStr(iQ,n,StrP,'----------------------------------------',10,iRing);
                        PrintDBStr(iQ,n,StrP,'      ��������                  ���-��',10,iRing);
                        PrintDBStr(iQ,n,StrP,'----------------------------------------',10,iRing);

                        if taServPiServ.AsInteger=1 then bHeadRet:=True else bHeadRet:=False; //����� ���� ������� ����� ������� �� ����� ������ ���
                      end;
                    end else
                    begin
                      if taServPiServ.AsInteger=1 then bHeadRet:=True;
                    end;

                    StrWk:=' ';
                    StrWk:=StrWk + Copy(taServPName.AsString,1,36);
                    while Length(StrWk)<36 do StrWk:=StrWk+' ';
                    PrintDBStr(iQ,n,StrP,StrWk,15,iRing);
                    Str(taServPQuant.AsFloat:7:3,StrWk1);
                    StrWk:=StrWk1;
                    while Length(StrWk)<36 do StrWk:=' '+StrWk;
                    PrintDBStr(iQ,n,StrP,StrWk,15,iRing);
                  end else     //����������� ������� �� 40 ��������
                  begin
                    if iLine>0 then //� ����� �� �������� ��������� ������ ���
                    begin
                      if (taServPiServ.AsInteger=1) or bHeadRet  then //�� ����� �������� ��������� ������ ��� �� ��� �������
                      begin
                        PrintDBStr(iQ,n,StrP,'----------------------------------------',10,iRing);
                        prAddPrintQH(n,iQ,FormatDateTime('dd.mm hh:nn:sss',now)+', ������� - '+IntToStr(CommonSet.Station)+','+StrP);
                        iQ:=GetId('PQH'); //����������� ������� �� 1-� c����
                        if bRing then iRing:=1 else iRing:=0;

                        PrintDBStr(iQ,n,StrP,sOp+' ('+StrPN+')',14,iRing);
                        PrintDBStr(iQ,n,StrP,'',13,iRing);
                        PrintDBStr(iQ,n,StrP,'��������: '+Tab.Name,13,iRing);
                        PrintDBStr(iQ,n,StrP,'����: '+Tab.NumTable+'  ������: '+IntToStr(Tab.Quests),13,iRing);
                        PrintDBStr(iQ,n,StrP,'����� ������: '+FormatDateTime('dd.mm.yyyy hh:nn:ss',Now),13,iRing);
                        PrintDBStr(iQ,n,StrP,'----------------------------------------',10,iRing);
                        PrintDBStr(iQ,n,StrP,'      ��������                  ���-��',10,iRing);
                        PrintDBStr(iQ,n,StrP,'----------------------------------------',10,iRing);

                        if taServPiServ.AsInteger=1 then bHeadRet:=True else bHeadRet:=False; //����� ���� ������� ����� ������� �� ����� ������ ���

                      end;
                    end else
                    begin
                      if taServPiServ.AsInteger=1 then bHeadRet:=True;
                    end;


                    StrWk:=' ';
                    StrWk:=StrWk + Copy(taServPName.AsString,1,27);
                    while Length(StrWk)<29 do StrWk:=StrWk+' ';
                    Str(taServPQuant.AsFloat:7:3,StrWk1);
                    StrWk:=StrWk+' '+StrWk1;
                    PrintDBStr(iQ,n,StrP,StrWk,15,iRing);
                  end;

                  inc(iLine);
                end else
                begin
                  StrWk:='   '+Copy(taServPName.AsString,1,35);
                  while Length(StrWk)<40 do StrWk:=StrWk+' ';
                  PrintDBStr(iQ,n,StrP,StrWk,10,iRing);
                end;
              end;
              taServP.Next;
            end;
            PrintDBStr(iQ,n,StrP,'----------------------------------------',10,iRing);
          end;
          prAddPrintQH(n,iQ,FormatDateTime('dd.mm hh:nn:sss',now)+', ������� - '+IntToStr(CommonSet.Station)+','+StrP);
          quPrint.Active:=False;
        end;
      end;
    end;

    quPServ.Next;
  end;
  //�� ������� ����������
  // ������� ������ ����� ���������� �� ������ �� ����������� ������� - ����� �����

  quPServ.Active:=False;

  end; //with
end;


Function FindPCard(sBar:String):Boolean;
begin
  result:=False;
  with dmC do
  begin
    quPCard.Active:=False;
    quPCard.ParamByName('SBAR').AsString:=sBar;
    quPCard.Active:=True;
    quPCard.First;
    if quPCard.RecordCount>0 then
    begin
      if (quPCardDATEFROM.AsDateTime<=now)and(quPCardDATETO.AsDateTime>=now) then
      begin
        Tab.PBar:=sBar;
        Tab.PName:=quPCardCLINAME.AsString;
        Tab.SaleT:=quPCardSALET.AsInteger;

        result:=True;
      end;
    end;
    quPCard.Active:=False;
  end;
end;


{

Function FindDiscount(sBar:String):Boolean;
Var bPrefFind:Boolean;
    StrWk:String;
    iL:Integer;
    StrWk1:String;
begin
  result:=False;
  Check.Discount:='';
  Check.DiscProc:=0;
  Check.DiscName:='';

  if Length(sBar)>30 then sBar:=Copy(sBar,1,30);

  with dmC do
  begin    //������� ��� ������������ �� ��������� ����� �� ����� ����
    bPrefFind:=False;

    taDiscPre.Active:=False;
    taDiscPre.Active:=True;
    taDiscPre.First;
    while not taDiscPre.Eof do
    begin
      StrWk:=taDiscPreBARCODE.AsString;
      iL:=Length(StrWk);
      if Length(sBar)>=iL then
      begin
        StrWk1:=Copy(sBar,1,iL);
        if StrWk=StrWk1 then
        begin
          Result:=True;
          bPrefFind:=True;
          Check.Discount:=sBar;
          Check.DiscProc:=taDiscPrePERCENT.AsFloat;
          Check.DiscName:=taDiscPreNAME.AsString;
        end;
      end;
      taDiscPre.Next;
    end;
    taDiscPre.Active:=False;

    if not bPrefFind then
    begin //�� �������� �� ����� - ���� � �������� ����
      taDiscFind.Active:=False;
      taDiscFind.ParamByName('SBAR').AsString:=sBar;
      taDiscFind.Active:=True;
      if taDiscFind.RecordCount>0 then
      begin
        Result:=True;
        Check.Discount:=sBar;
        Check.DiscProc:=taDiscFindPERCENT.AsFloat;
        Check.DiscName:=taDiscFindNAME.AsString;
      end;
      taDiscFind.Active:=False;
    end;
  end;
end;

}


Function FindDiscount(sBar:String):Boolean;
Var bPrefFind:Boolean;
    StrWk:String;
    iL:Integer;
    StrWk1:String;
begin
  result:=False;
  Tab.DBar:='';
  Tab.DPercent:=0;
  Tab.DName:='';
  Tab.DType:=0;

  if SBar='' then exit;
  with dmC do
  begin

    if Length(sBar)>30 then sBar:=Copy(sBar,1,30);
    bPrefFind:=False;

    taDiscPref.Active:=False;
    taDiscPref.Active:=True;
    taDiscPref.First;
    while not taDiscPref.Eof do
    begin
      StrWk:=taDiscPrefBARCODE.AsString;
      delete(StrWk,1,1); //������ ! -  ���� �������� 
      iL:=Length(StrWk);
      if Length(sBar)>=iL then
      begin
        StrWk1:=Copy(sBar,1,iL);
        if StrWk=StrWk1 then
        begin
          Result:=True;
          bPrefFind:=True;

          Tab.DBar:=sBar;
          Tab.DPercent:=taDiscPrefPERCENT.AsFloat;
          Tab.DName:=taDiscPrefNAME.AsString;
        end;
      end;
      taDiscPref.Next;
    end;
    taDiscPref.Active:=False;

    if not bPrefFind then
    begin //�� �������� �� ����� - ���� � �������� ����

//and ID in (SELECT ID_CLASSIF FROM RCLASSIF WHERE RIGHTS=0 AND ID_PERSONAL= :PERSONID)

      taDiscCard.Active:=False;
      taDiscCard.ParamByName('DBAR').AsString:=sBar;
      taDiscCard.ParamByName('PERSONID').AsInteger:=Person.Id;
      taDiscCard.Active:=True;
      taDiscCard.First;
      if taDiscCard.RecordCount>0 then
      begin
        if taDiscCardIACTIVE.AsInteger=1 then
        begin
          Tab.DBar:=taDiscCardBARCODE.AsString;
          Tab.DPercent:=taDiscCardPERCENT.AsFloat;
          Tab.DName:=taDiscCardNAME.AsString;

          if taDiscCardTYPEOPL.AsInteger=3 then Tab.DType:=3;

          result:=True;
        end;
      end;
      taDiscCard.Active:=False;
    end;
  end;
end;


Procedure FormLog(NAMEOP,CONTENT:String);
begin
  with dmC do
  begin
    prFormLog.ParamByName('IDPERSONAL').AsInteger:=Person.Id;
    prFormLog.ParamByName('NAMEOP').AsString:=NAMEOP;
    prFormLog.ParamByName('CONTENT').AsString:=CONTENT;
    prFormLog.ExecProc;
  end;
end;

Procedure CalcDiscont(Sifr,Id:Integer;Price,Quantity,Summa:Real;Discount:String;CurDateTime:TDateTime; Var DiscProc,DiscSum:Real);
begin
  DiscProc:=0;
  DiscSum:=0;

  if FindDiscount(Discount) then
  begin
    DiscProc:=Tab.DPercent;
    DiscSum:=Price*Quantity*Tab.DPercent/100;
  end;
end;

Procedure CalcDiscontN(Sifr,Id,Parent,Stream:Integer;Price,Quantity,Summa:Real;Discount:String;CurDateTime:TDateTime; Var DiscProc,DiscSum:Real);
Var StrWk:String;
    iWeek:INteger;
begin
  DiscProc:=0;
  DiscSum:=0;


  if FindDiscount(Discount) or (CommonSet.DiscAuto=1) then
  begin
    if Tab.DType<>3 then
    begin
      DiscProc:=Tab.DPercent;
      DiscSum:=RoundEx(Price*Quantity*Tab.DPercent/100*100)/100;

//(?ART, ?POSNUM, ?PARENT, ?PRINTGROUP, ?PRICE, ?QUANTITY, ?SUMMA, ?DISCPROC, ?CURTIME, ?CURDATE, ?DAYWEEK)
      with dmC do
      begin

        StrWk:=FormatDateTime('ddd',Date);

        iWeek:=1;
        if Pos('��',StrWk)>0 then iWeek:=1;
        if Pos('��',StrWk)>0 then iWeek:=2;
        if Pos('��',StrWk)>0 then iWeek:=3;
        if Pos('��',StrWk)>0 then iWeek:=4;
        if Pos('��',StrWk)>0 then iWeek:=5;
        if Pos('��',StrWk)>0 then iWeek:=6;
        if Pos('��',StrWk)>0 then iWeek:=7;

        if Pos('Mon',StrWk)>0 then iWeek:=1;
        if Pos('Tue',StrWk)>0 then iWeek:=2;
        if Pos('Wed',StrWk)>0 then iWeek:=3;
        if Pos('Thu',StrWk)>0 then iWeek:=4;
        if Pos('Fri',StrWk)>0 then iWeek:=5;
        if Pos('Sat',StrWk)>0 then iWeek:=6;
        if Pos('Sun',StrWk)>0 then iWeek:=7;


        prCalcDisc.ParamByName('ART').AsInteger:=Sifr;
        prCalcDisc.ParamByName('POSNUM').AsInteger:=Id;
        prCalcDisc.ParamByName('PARENT').AsInteger:=Parent; //���� �� �� ���� ��������� ��������
        prCalcDisc.ParamByName('STREAM').AsInteger:=Stream; //������ ������
        prCalcDisc.ParamByName('PRICE').AsFloat:=Price;
        prCalcDisc.ParamByName('QUANTITY').AsFloat:=Quantity;
        prCalcDisc.ParamByName('SUMMA').AsFloat:=Summa; //��� ���� ���-�� ������ ����� ������
        prCalcDisc.ParamByName('DISCPROC').AsFloat:=DiscProc;
        prCalcDisc.ParamByName('CURTIME').AsFloat:=Now - Trunc(Now); //�����
        prCalcDisc.ParamByName('CURDATE').AsDateTime:=Date;
        prCalcDisc.ParamByName('DAYWEEK').AsInteger:=iWeek; //���� ������

        prCalcDisc.ExecProc;

        DiscProc:=prCalcDisc.ParamByName('DISCOUNTPROC').AsFloat;
        DiscSum:=prCalcDisc.ParamByName('DISCOUNTSUM').AsFloat;

        prWriteLog('--CalcDisc;'+IntToStr(Tab.Id)+';'+IntToStr(Sifr)+';'+IntToStr(Stream)+';'+IntToStr(Tab.Id_Personal)+';'+FloatToStr(DiscProc)+';'+FloatToStr(RoundVal(DiscSum))+';'+Discount);

      // ��� �������� ����� �������� ��� ���������� - ���������� ���� ������ ����� ������ ���������
      // ��������
      // cSum:=RoundVal(quMenuPRICE.AsFloat*rQ-Check.DSum);
      // cSumD:=RoundVal(quMenuPRICE.AsFloat*rQ)-cSum;

      end;
    end;
  end else
  begin
//    prWriteLog('--CalcDiscBad;'+Discount);
  end;
end;


Function FindPers(sP:String):Boolean;
begin
  result:=False;
  with dmC do
  begin
    taPersonal.Active:=true;
    if taPersonal.Locate('BARCODE',sP,[]) then
    begin
      if (taPersonalUVOLNEN.AsInteger=1) and (taPersonalMODUL3.AsInteger=0) then
      begin
        Person.Id:=taPersonalID.AsInteger;
        Person.Name:=taPersonalNAME.AsString;
        Result:=True;
      end;
    end;
    tapersonal.Active:=False;
  end;
end;


Function CanDo(Name_F:String):Boolean;
begin
  result:=True;
  with dmC do
  begin
    quCanDo.Active:=False;
    quCanDo.ParamByName('Person_id').Value:=Person.Id;
    quCanDo.ParamByName('Name_F').Value:=Name_F;
    quCanDo.Active:=True;
    if quCanDoprExec.AsBoolean=True then Result:=False;
  end;
end;

Function GetId(ta:String):Integer;
Var iType:Integer;
begin
  with dmC do
  begin
    iType:=0;
    if ta='Pe' then iType:=1; //��������
    if ta='Cl' then iType:=2; //�������������
    if ta='Trh' then iType:=3; //��������� ������
    if ta='Trs' then iType:=4; //������������ ������
    if ta='TabH' then iType:=5; //��������� ������� (������)
    if ta='CS' then iType:=6; //��������� ������� (������)
    if ta='TH' then iType:=7; //��������� ������� (������) all

    if ta='PQH' then iType:=9; //����� ������� ������
    if ta='PQS' then iType:=10; //����� ������ ������
    if ta='PQH0' then iType:=11; //����� ������� ������� ������

    prGetId.ParamByName('ITYPE').Value:=iType;
    prGetId.ExecProc;
    result:=prGetId.ParamByName('RESULT').Value;
  end;
end;


procedure TdmC.DataModuleCreate(Sender: TObject);
begin
  with dmC do
  begin
    CasherRnDb.Connected:=False;
    CasherRnDb.DBName:=DBName;
    try
      CasherRnDb.Open;

      taPersonal.Active:=True;
      taRClassif.Active:=True;
      quFuncList.Active:=True;
      taFuncList.Active:=True;
    except
      fmPerA.StatusBar1.Panels[0].Text:='���� �� ����������� - '+DBName;
    end;
  end;
  ABitmap := TBitmap.Create;
  ABitmap.LoadFromFile(CurDir+'Pict2.bmp');
end;

procedure TdmC.DataModuleDestroy(Sender: TObject);
begin
  taPersonal.Active:=False;
  taRClassif.Active:=False;
  quFuncList.Active:=False;
  taFuncList.Active:=False;

{  if trUpdate.Active then trUpdate.Commit;
  if trDel.Active then trDel.Commit;
  if trSelect.Active then trSelect.Commit;
}
  CasherRnDb.Close;
  ABitmap.Free;
end;

procedure TdmC.quFuncListBeforePost(DataSet: TDataSet);
begin
  if taFuncList.Locate('Name',quFuncListNAME.AsString,[]) then
  begin
    taFuncList.Edit;
    taFuncListCOMMENT.AsString:=quFuncListCOMMENT.AsString;
    taFuncList.Post;
  end;
end;

procedure TdmC.quPersCalcFields(DataSet: TDataSet);
Var StrWk:String;
begin
  Str(quPersTOTALSUM.AsFloat:8:2,StrWk);
  quPerssCountTab.AsString:='�������� ������� - '+IntToStr(quPersCOUNTTAB.AsInteger)+ ',  �� ����� '+StrWk+' ���.';
end;

procedure TdmC.quTabsCalcFields(DataSet: TDataSet);
Var StrWk:String;
begin
  StrWk:='';
  if  quTabsISTATUS.AsInteger=0 then strWk:='(���.)';
  if  quTabsISTATUS.AsInteger=1 then strWk:='(��.)';
  if  quTabsISTATUS.AsInteger=2 then strWk:='(����)';

  quTabsSSTAT.AsString:=quTabsNUMTABLE.AsString+' '+StrWk;

  quTabsSTIME.AsString:=FormatdateTime('hh:mm',quTabsBEGTIME.AsDateTime);
end;

procedure TdmC.quMenuCalcFields(DataSet: TDataSet);
Var StrWk,StrWk1:String;
begin
  Str(quMenuPRICE.AsFloat:8:2,StrWk);
  While Pos(' ',StrWk)>0 do delete(StrWk,Pos(' ',StrWk),1);
  StrWk1:=quMenuNAME.AsString;
  if quMenuTREETYPE.AsString='T' then
  begin
 //   while Length(StrWk1)<50 do StrWk1:=StrWk1+' ';
    quMenuINFO.AsString:=StrWk1;
    quMenuSPRICE.AsString:='';
  end else
  begin
//    StrWk1:=StrWk1+'  ('+StrWk+'�.)';
//    while Length(StrWk1)<50 do StrWk1:=StrWk1+' ';
//    quMenuINFO.AsString:=StrWk1;
//    quMenuSPRICE.AsString:='���� - '+StrWk+'�.';

    quMenuINFO.AsString:=StrWk1+'                                                            |'+quMenuSIFR.AsString;
    if quMenuPRNREST.AsInteger=1  then
    begin
      quMenuSPRICE.AsString:='���� - '+StrWk+'�.'+'(���. '+fts(quMenuBACKBGR.AsFloat)+' )'+'                                                            |'+quMenuSIFR.AsString;
    end else
    begin
      quMenuSPRICE.AsString:='���� - '+StrWk+'�.'+'                                                            |'+quMenuSIFR.AsString;
    end;
  end;
end;

procedure TdmC.taSpecAllSelCalcFields(DataSet: TDataSet);
begin
  if taSpecAllSelITYPE.AsInteger=0 then
    taSpecAllSelNAME.AsString:=taSpecAllSelNAMEMM.AsString
  else
    taSpecAllSelNAME.AsString:=taSpecAllSelNAMEMD.AsString;
end;

procedure TdmC.quRepLogCalcFields(DataSet: TDataSet);
Var StrWk,StrWk1:String;
    StrF:Array[1..5] of String;

Procedure DecodeStr;
var n:Integer;
begin
  for n:=1 to 5 do
  begin
    StrF[n]:='';

    if StrWk1>'' then
    begin
      if pos(' ',StrWk1)>0 then
      begin
        StrF[n]:=Copy(StrWk1,1,pos(' ',StrWk1)-1);
        delete(StrWk1,1,pos(' ',StrWk1));
      end
      else
      begin
        StrF[n]:=StrWk1;
        StrWk1:='';
      end;
    end;
  end;
end;


begin
  StrWk:='';
  StrWk1:=quRepLogCONTENT.AsString;
  DecodeStr;
  if  quRepLogNAMEOP.AsString='CreateTab' then
  begin
    StrWk:='��� - '+StrF[1]+'. ����� ����� - '+StrF[2];
  end;
  if  quRepLogNAMEOP.AsString='Sale' then
  begin
    StrWk:='������ - '+StrF[1]+'. �������� - '+StrF[2]+'. ����� ����� - '+StrF[3];
  end;
  if  quRepLogNAMEOP.AsString='MoveTab' then
  begin
    StrWk:='��� - '+StrF[1]+'. �� ���� - '+StrF[2]+'. ���� - '+StrF[3]+'. ���� - '+StrF[4]+'. ���� - '+StrF[5];
  end;
  if  quRepLogNAMEOP.AsString='RetPre' then
  begin
    StrWk:='��� - '+StrF[1]+'. �������� - '+StrF[2]+'. ����� ����� - '+StrF[3];
  end;
  if  quRepLogNAMEOP.AsString='Close' then
  begin
    StrWk:='��� - '+StrF[1]+'.';
  end;
  if  quRepLogNAMEOP.AsString='DelPos1' then
  begin
    StrWk:='��� - '+StrF[1]+'. ����� - '+StrF[3]+'. ����� - '+StrF[4];
  end;
  if  quRepLogNAMEOP.AsString='DelPos0' then
  begin
    StrWk:='��� - '+StrF[1]+'. ����� - '+StrF[3]+'. ����� - '+StrF[4];
  end;
  if  quRepLogNAMEOP.AsString='DelTab0' then
  begin
    StrWk:='��� - '+StrF[1]+'. �������� - '+StrF[2]+'. ����� ����� - '+StrF[3];
  end;
  if  quRepLogNAMEOP.AsString='DelTab1' then
  begin
    StrWk:='��� - '+StrF[1]+'. �������� - '+StrF[2]+'. ����� ����� - '+StrF[3];
  end;
  if  quRepLogNAMEOP.AsString='MovePos0' then
  begin
    StrWk:='��� - '+StrF[1]+'. ����� - '+StrF[3]+'. ����� - '+StrF[4];
  end;
  if  quRepLogNAMEOP.AsString='MovePos1' then
  begin
    StrWk:='��� - '+StrF[1]+'. ����� - '+StrF[3]+'. ����� - '+StrF[4];
  end;
  if  quRepLogNAMEOP.AsString='Ret' then
  begin
    StrWk:='��� - '+StrF[1]+'. �������� - '+StrF[2]+'. ����� ����� - '+StrF[3];
  end;
  if  quRepLogNAMEOP.AsString='SaveTab' then
  begin
    StrWk:='��� - '+StrF[2]+'. ����� ����� - '+StrF[3];
  end;


  quRepLogCONTNAME.AsString:=StrWk;;
end;

procedure TdmC.quSelSpecAllCalcFields(DataSet: TDataSet);
begin
  if quSelSpecAllITYPE.AsInteger=0 then quSelSpecAllName.AsString:=quSelSpecAllNAMEMM.AsString
  else quSelSpecAllName.AsString:=quSelSpecAllNAMEMD.AsString;
                                                                                                                                                                               // /100*100)       
  quSelSpecAllSUMMA1.AsFloat:=quSelSpecAllQuantity1.AsFloat*quSelSpecAllPRICE.AsFloat-RoundEx(quSelSpecAllQuantity1.AsFloat*quSelSpecAllPRICE.AsFloat*quSelSpecAllDISCOUNTPROC.AsFloat)/100;
end;

procedure TdmC.quSelSpecAllQUANTITY1Change(Sender: TField);
begin
  quSelSpecAllSUMMA1.AsFloat:=quSelSpecAllQuantity1.AsFloat*quSelSpecAllPRICE.AsFloat-RoundEx(quSelSpecAllQuantity1.AsFloat*quSelSpecAllPRICE.AsFloat*quSelSpecAllDISCOUNTPROC.AsFloat)/100;
end;

procedure TdmC.quSelSpecAllINEEDChange(Sender: TField);
begin
  if quSelSpecAllINEED.AsInteger=0 then
  begin
    quSelSpecAllQuantity1.AsFloat:=0;
    quSelSpecAllSUMMA1.AsFloat:=quSelSpecAllQuantity1.AsFloat*quSelSpecAllPRICE.AsFloat-RoundEx(quSelSpecAllQuantity1.AsFloat*quSelSpecAllPRICE.AsFloat*quSelSpecAllDISCOUNTPROC.AsFloat)/100;
  end else
  begin
    quSelSpecAllQuantity1.AsFloat:=quSelSpecAllQuantity.AsFloat;
    quSelSpecAllSUMMA1.AsFloat:=quSelSpecAllQuantity1.AsFloat*quSelSpecAllPRICE.AsFloat-RoundEx(quSelSpecAllQuantity1.AsFloat*quSelSpecAllPRICE.AsFloat*quSelSpecAllDISCOUNTPROC.AsFloat)/100;
  end;
end;


procedure prAddMove(Oper,Code:Integer;rQ:Real);
begin
  with dmC do
  begin

  //EXECUTE PROCEDURE PR_ADDMOVESL (?SIFR, ?PERSON, ?QUANT, ?OPER)

    quMenuSel.Active:=False;
    quMenuSel.SelectSQL.Clear;
    quMenuSel.SelectSQL.Add('SELECT SIFR,NAME,PARENT,PRICE,CODE,LIMITPRICE,LINK,STREAM,BARCODE,PARENT,PRNREST');
    quMenuSel.SelectSQL.Add('FROM MENU');
    quMenuSel.SelectSQL.Add('WHERE SIFR='+its(Code));
    quMenuSel.Active:=True;
    if quMenuSel.RecordCount>0 then
    begin
      if quMenuSelPRNREST.AsInteger=1 then
      begin
        prADDMOVESL.ParamByName('SIFR').AsInteger:=Code;
        prADDMOVESL.ParamByName('PERSON').AsString:=Person.Name;
        prADDMOVESL.ParamByName('QUANT').AsFloat:=rQ;
        prADDMOVESL.ParamByName('OPER').AsInteger:=Oper;
        prADDMOVESL.ExecProc;
      end;
    end;
    quMenuSel.Active:=False;

    delay(10);
  end;
end;


procedure TdmC.quCheckOrgItogCalcFields(DataSet: TDataSet);
begin
  quCheckOrgItogITSUM.AsFloat:=quCheckOrgItogRSUM.AsFloat-aCheckPlat[quCheckOrgItogIORG.AsInteger,3];
end;

end.
