unit UpLica;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, Placemnt, SpeedBar, ExtCtrls,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  cxImageComboBox, XPStyleActnCtrls, ActnList, ActnMan;

type
  TfmUpLica = class(TForm)
    ViewUPL: TcxGridDBTableView;
    LevelUPL: TcxGridLevel;
    GrUPL: TcxGrid;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    FormPlacement1: TFormPlacement;
    ViewUPLID: TcxGridDBColumn;
    ViewUPLIMH: TcxGridDBColumn;
    ViewUPLNAME: TcxGridDBColumn;
    ViewUPLIACTIVE: TcxGridDBColumn;
    ViewUPLNAMEMH: TcxGridDBColumn;
    acUPL: TActionManager;
    acAddUPL: TAction;
    acEditUPL: TAction;
    acDelUPL: TAction;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SpeedItem1Click(Sender: TObject);
    procedure acAddUPLExecute(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
    procedure acEditUPLExecute(Sender: TObject);
    procedure acDelUPLExecute(Sender: TObject);
    procedure ViewUPLDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmUpLica: TfmUpLica;

implementation

uses Un1, DMOReps, dmOffice, AddUPL;

{$R *.dfm}

procedure TfmUpLica.FormCreate(Sender: TObject);
begin
  GrUPL.Align:=AlClient;
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  ViewUPL.RestoreFromIniFile(CurDir+GridIni);
end;

procedure TfmUpLica.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewUPL.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmUpLica.SpeedItem1Click(Sender: TObject);
begin
  close;
end;

procedure TfmUpLica.acAddUPLExecute(Sender: TObject);
begin
  if not CanDo('prAddUPL') then begin ShowMessage('��� ����.'); exit; end;
  fmAddUPL:=tfmAddUPL.Create(Application);
  with dmO do
  begin
    if quMHAll.Active=False then
    begin
       quMHAll.ParamByName('IDPERSON').AsInteger:=Person.Id;
       quMHAll.Active:=True;
    end;
    quMHAll.FullRefresh;
    quMHAll.First;
    fmAddUPL.cxLookupComboBox2.EditValue:=quMHAllID.AsInteger;
    fmAddUPL.cxTextEdit1.Text:='';
  end;
  fmAddUPL.ShowModal;
  if fmAddUPL.ModalResult=mrOk then
  begin
    with dmORep do
    begin
      quUPL.Append;
      quUPLID.AsInteger:=getid('UPL');
      quUPLIMH.AsInteger:=fmAddUPL.cxLookupComboBox2.EditValue;
      quUPLNAME.AsString:=fmAddUPL.cxTextEdit1.Text;
      quUPLIACTIVE.AsInteger:=1;
      quUPL.Post;

      quUPL.Refresh;
    end;
  end;
  fmAddUPL.Release;
end;

procedure TfmUpLica.SpeedItem3Click(Sender: TObject);
begin
  prNExportExel5(ViewUPL);
end;

procedure TfmUpLica.acEditUPLExecute(Sender: TObject);
begin
  if not CanDo('prEditUPL') then begin ShowMessage('��� ����.'); exit; end;
  with dmORep do
  with dmO do
  begin
    if quUPL.RecordCount>0 then
    begin
      fmAddUPL:=tfmAddUPL.Create(Application);
      if quMHAll.Active=False then
      begin
         quMHAll.ParamByName('IDPERSON').AsInteger:=Person.Id;
         quMHAll.Active:=True;
      end;
      quMHAll.FullRefresh;
      quMHAll.Locate('ID',quUPLIMH.AsInteger,[]);
      fmAddUPL.cxLookupComboBox2.EditValue:=quMHAllID.AsInteger;
      fmAddUPL.cxTextEdit1.Text:=quUPLNAME.AsString;

      fmAddUPL.ShowModal;
      if fmAddUPL.ModalResult=mrOk then
      begin
        quUPL.Edit;
        quUPLIMH.AsInteger:=fmAddUPL.cxLookupComboBox2.EditValue;
        quUPLNAME.AsString:=fmAddUPL.cxTextEdit1.Text;
        quUPLIACTIVE.AsInteger:=1;
        quUPL.Post;

        quUPL.Refresh;
      end;
      fmAddUPL.Release;
    end;
  end;

end;

procedure TfmUpLica.acDelUPLExecute(Sender: TObject);
begin
  if not CanDo('prDelUPL') then begin ShowMessage('��� ����.'); exit; end;
  with dmORep do
  with dmO do
  begin
    if quUPL.RecordCount>0 then
    begin
      if MessageDlg('����� ������� �������������� ���� '+quUPLNAME.AsString+'?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        quUPL.Delete;
      end;
    end;
  end;
end;

procedure TfmUpLica.ViewUPLDblClick(Sender: TObject);
begin
  if not CanDo('prEditUPL') then begin ShowMessage('��� ����.'); exit; end;
  with dmORep do
  with dmO do
  begin
    if quUPL.RecordCount>0 then
    begin
      if quUPLIACTIVE.AsInteger=1 then
      begin
        if MessageDlg('������� ���������� '+quUPLNAME.AsString+'?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          quUPL.Edit;
          quUPLIACTIVE.AsInteger:=0;
          quUPL.Post;

          quUPL.Refresh;
        end;
      end else
      begin
        if MessageDlg('������� �������� '+quUPLNAME.AsString+'?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          quUPL.Edit;
          quUPLIACTIVE.AsInteger:=1;
          quUPL.Post;

          quUPL.Refresh;
        end;
      end;
    end;
  end;
end;

end.
