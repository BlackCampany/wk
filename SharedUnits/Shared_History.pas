//==============================================================================
// ����� ������ ��� ������ ��������� ���� � �� ELOG
// ���� �������: 10.12.2014
//
// ����� �������������� ������� FDConnection
//  _Log.Connection:=FDConnection;  //�������� ������� � ��, ����� ��� ��� �������� � ��
//==============================================================================

unit Shared_History;

interface

uses SysUtils, Classes, Types, Forms, Windows, Data.DB, ActiveX, Shared_Functions, ExtCtrls, Graphics,
  Vcl.Imaging.jpeg,
  FireDAC.Stan.Intf,FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client, FireDAC.UI.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Phys, FireDAC.Phys.MSSQL, FireDAC.Phys.MSSQLDef, FireDAC.VCLUI.Wait,
  FireDAC.Comp.UI, FireDAC.Phys.ODBCBase;

type
  THistoryWriter = class
  private
    FConnection: TFDConnection;
    FQuery: TFDQuery;
    FCurrentUserName: string;
    FCurrentComputerName: string;
    FCurrentIDPersonal: Integer;
    FCurrentNamePersonal: string;

    //��������� ���������� � ��
    function CheckDBConnection:Boolean;
    function GetConnection: TFDConnection;
    procedure SetConnection(const Value: TFDConnection);
  public
    constructor Create;
    destructor Destroy; override;

    property Connection: TFDConnection read GetConnection write SetConnection;
    property IDPersonal: Integer read FCurrentIDPersonal write FCurrentIDPersonal;
    property NamePersonal: String read FCurrentNamePersonal write FCurrentNamePersonal;

    //<--��������� ��� ������ � ��������
    //��������� ��������� �������
    function AddHistoryHeader(IDH,ISHOP,IDOCTYPE,IOPER: Integer): Integer;
    //��������� ������������ �������
    function AddHistorySpecification(IDHEAD,ISHOP,IDOPER,IDFIELD: Integer; NAMEFIELD,NAMESQLFIELD,CHANGE_FROM,CHANGE_TO: String): Integer;
    //-->
  end;

var
  _Hist: THistoryWriter;

implementation

uses Math;

constructor THistoryWriter.Create;
begin
  FQuery := TFDQuery.Create(nil);

  FCurrentUserName:=GetCurrentUserName;
  FCurrentComputerName:=GetCurrentComputerName;
end;

destructor THistoryWriter.Destroy;
begin
  FreeAndNil(FQuery);
end;

procedure THistoryWriter.SetConnection(const Value: TFDConnection);
begin
  FConnection := Value;
  FQuery.Connection := FConnection;
  if Assigned(FConnection) then begin
    FQuery.Transaction := FConnection.Transaction;
    FQuery.UpdateTransaction := FConnection.UpdateTransaction;
  end;
end;

function THistoryWriter.GetConnection: TFDConnection;
begin
  Result:=FConnection;
end;

function THistoryWriter.CheckDBConnection:Boolean;
begin
  Result:=False;
  if Assigned(FQuery.Connection)=false then Exit;

  if FQuery.Connection.Connected=false then begin
    FQuery.Connection.Connected:=true;
  end;
  Result:=FQuery.Connection.Connected;
end;

function THistoryWriter.AddHistoryHeader(IDH,ISHOP,IDOCTYPE,IOPER: Integer): Integer;
var str: string;
begin
  if CheckDBConnection=false then Exit;

  FQuery.Active:=False;
  FQuery.SQL.Clear;
  FQuery.SQL.Add('DECLARE @RC bigint');
  FQuery.SQL.Add('EXECUTE @RC = ELOG.dbo.prHistoryAddHeader :ISHOP,:IDOCTYPE,:IDH,:IOPER,:RPERSON,:PERSON,:DATEOPER');
  FQuery.SQL.Add('SELECT	''RC'' = @RC');
  FQuery.ParamByName('IDH').Value:=IDH;
  FQuery.ParamByName('ISHOP').Value:=ISHOP;
  FQuery.ParamByName('IDOCTYPE').Value:=IDOCTYPE;
  FQuery.ParamByName('IOPER').Value:=IOPER;
  FQuery.ParamByName('RPERSON').Value:=FCurrentComputerName+' '+FCurrentUserName+' ('+trim(FCurrentNamePersonal)+')';
  FQuery.ParamByName('PERSON').Value:=FCurrentIDPersonal;
  FQuery.ParamByName('DATEOPER').Value:=Now;
  FQuery.Active:=True;

  Result:=FQuery.FieldByName('RC').AsInteger;
end;

function THistoryWriter.AddHistorySpecification(IDHEAD,ISHOP,IDOPER,IDFIELD: Integer; NAMEFIELD,NAMESQLFIELD,CHANGE_FROM,CHANGE_TO: String): Integer;
var str: string;
begin
  if CheckDBConnection=false then Exit;

  if (IDOPER = 1) and (CHANGE_FROM <> CHANGE_TO) then  //�������� �� �� ������ ��������������, ������ ��� �����������������
  begin
    FQuery.Active:=False;
    FQuery.SQL.Clear;
    FQuery.SQL.Add('EXECUTE ELOG.dbo.prHistoryAddSpecification :IDHEAD,:ISHOP,:IDOPER,:IDFIELD,:NAMEFIELD,:NAMESQLFIELD,:CHANGE_FROM,:CHANGE_TO');
    FQuery.ParamByName('IDHEAD').Value := IDHEAD;
    FQuery.ParamByName('ISHOP').Value := ISHOP;
    FQuery.ParamByName('IDOPER').Value := IDOPER;
    FQuery.ParamByName('IDFIELD').Value := IDFIELD;
    FQuery.ParamByName('NAMEFIELD').Value := NAMEFIELD;
    FQuery.ParamByName('NAMESQLFIELD').Value := NAMESQLFIELD;
    FQuery.ParamByName('CHANGE_FROM').Value := CHANGE_FROM;
    if IDOPER = 1 then
      FQuery.ParamByName('CHANGE_TO').Value := CHANGE_TO
    else
      FQuery.ParamByName('CHANGE_TO').Value := 0;

    FQuery.ExecSQL;
  end;
end;

initialization
  CoInitialize(nil);
  _Hist:=THistoryWriter.Create;
finalization
  FreeAndNil(_Hist);
  CoUninitialize;
end.


