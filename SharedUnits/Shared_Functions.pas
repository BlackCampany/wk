unit Shared_Functions;

interface

uses Types, Classes, Windows, Forms, dxmdaset, Math, DB, Winsock, ComCtrls, ZLib,
{$IFDEF VER150} //Delphi 7
  SysUtils, Masks, IdHashMessageDigest,
{$ELSE} //Delphi XE
  cxTL, System.SysUtils, System.Masks, IdHashMessageDigest,
{$ENDIF}
  ActnList;

const TAB=#9;

function RV( X: Double ): Double;
function ds(d:TDateTime):String;
function ds1(d:TDateTime):String;
function ds2(d:TDateTime):String;
function its(i:Integer):String; overload;
function its(i:Int64):String; overload;
function fs(rSum:Real):String;
function s2f(const S: string): Extended; //������ � ������� �����
procedure CloseTe(taT:TdxMemData);
function RoundEx(X: Double): Integer;
function RoundPr(X: Double; Precision: Integer = 0): Double;
function RoundHi( X: Double ): Integer;

//����� ��������� � �������������
procedure Delay(MSecs: Longint);
//���������� ��� �������� ������������
function GetCurrentUserName: string;
//���������� ��� �������� ����������
function GetCurrentComputerName: string;
//���������� IP ������ ��� ������, ���� �� ���������
function GetCurrentIP:String;
//��������� ������ ���������
function GetFileVersion(const FileName: string): string;
//�������� OriginalFileName
function GetOriginalFileName(const FileName: string): string;
//�������� ProgramVersion
function GetProgramVersion(const FileName: string): string;
//������� ������ Enter, ������ ��������� 2 ������� #$0D � #$0A
function TrimRightEnter(const S: string): string;
//����� ������� ��� ������ �������
procedure _StartTimer;
//������������� ������, ���������� ����� ����������
function _StopTimer: Integer;
//������ ���� ������
Function FirstDayOfMonth(const DateTime: TDateTime):TDateTime;
//��������� ���� ������
function LastDayOfMonth(const DateTime: TDateTime):TDateTime;
//���������� ���� � ����������
function ShowMsg(MessageText: string; Caption: string = ''; Buttons: Integer = MB_OK + MB_ICONINFORMATION): Integer;
//���������� ���� � ����������, � ���������
function ShowMsgTimeOut(MessageText: string; timeOut: integer = 30000; Caption: string = ''; Buttons: Integer = MB_OK + MB_ICONINFORMATION): Integer;
//��������� ������ Items, �������� � ComboBox, ������ � ���������
procedure FillItemsFromDataSet(Items: TStrings; DataSet: TDataSet; ID: TIntegerField;  Name: TStringField);
//���������� �������� ����� �� ������ Items
function GetItemKey(Items: TStrings; ItemIndex: Integer): Integer;
//������� ����� ������ �� �����
function LocateItemIndexByKey(Items: TStrings; ID: Integer): integer;
//������ ���������� ����������� �� �� �������, �������� ���������� %SystemRoot% �� C:\Windows
function ExpandEnvVar(const Value: string): string;
//��������� � ������ �������� �������� ����������, ��� ��������� � ����������� ���������� MSSQL
function GetConnectionString(UdlFileName, ApplicationName: string):string;
//���������� ������������ SQL �������
function GetSQLServerName(ConnectionString: string):string;
// ��������� ������ � �������� �������
Function Access_Resourse(PathResurse, Username, Password: String): Boolean;
// ��������� ������ ������ �� ��������
procedure FindFiles(StartFolder, Mask: string; List: TStrings; ScanSubFolders: Boolean = True);
//��������� �����
function SplitString(Str: String; Delimiter: Char = ';'):TStrings;
//��������� �������� �� ������ ������ ���������� �����
function IsNumber(const s: String): Boolean;
//�������� MD5 ��� �����
function GetMD5_File(AFilename: String): String;
//�������� MD5 ��� ������
function GetMD5_Stream(AStream: TStream): String;
//������ � ���������� �������
Procedure CompressionStream(streamInOut: TMemoryStream; CompressionLevel:TCompressionLevel = clDefault);
Procedure DeCompressionStream(streamInOut: TMemoryStream);
//���������� ��������� ��������� ��-��� IDE Delphi ��� ���
function RunningFromIDE: Boolean;

var
  SysDecimalSeparator: string;
  FTimer : Int64;

implementation

function RV( X: Double ): Double;
var  ScaledFractPart:Integer;
     Temp : Double;
begin
  ScaledFractPart := Trunc(X*100);
  if X>=0 then Temp := Trunc(Frac(X*100)*1000000)+1 else  Temp := Trunc(Frac(X*100)*1000000)-1;
  if Temp >=  500000 then ScaledFractPart := ScaledFractPart + 1;
  if Temp <= -500000 then ScaledFractPart := ScaledFractPart - 1;
  Result:= ScaledFractPart/100;
end;

function ds(d:TDateTime):String;
begin
  result:=FormatDateTime('yyyymmdd',d);
end;

function ds1(d:TDateTime):String;
begin
  result:=FormatDateTime('dd.mm.yyyy',d);
end;

function ds2(d:TDateTime):String;
const
   Mes: array[1..12] of string = ('������', '�������', '�����', '������', '���', '����', '����', '�������',
                                  '��������', '�������', '������', '�������');
var
   Year, Month, Day: Word;
begin
   DecodeDate(d, Year, Month, Day);
   if (Year=0)or(Month=0)or(Day=0) then
     Result:=''   //�������� ������ ����, �� ����� ������� � ����������
   else
     Result := IntToStr(day) + ' ' + Mes[Month] +' ' + IntToStr(Year)+' �.';
end;

function its(i:Integer):String;
begin
  result:=IntToStr(i);
end;

function its(i:Int64):String;
begin
  result:=IntToStr(i);
end;

function fs(rSum:Real):String;
Var s:String;
begin
  s:=FloatToStr(rSum);
  while pos(',',s)>0 do s[pos(',',s)]:='.';
  Result:=s;
end;

function s2f(const S: string): Extended;
var StrWk:string;
begin
  if Trim(S)='' then
    Result:=0
  else
  begin
    StrWk:=Trim(S);
    while Pos('.',StrWk)>0  do StrWk[Pos('.',StrWk)]:=',';

    Result := StrToFloat(StrWk);
  end;
end;

procedure Delay(MSecs: Longint);
var
  FirstTickCount, Now: Longint;
begin
  FirstTickCount := GetTickCount;
  repeat
    Application.ProcessMessages;
    { allowing access to other controls, etc. }
    Now := GetTickCount;
  until (Now - FirstTickCount >= MSecs) or (Now < FirstTickCount);
end;

function GetCurrentUserName: string;
const
  cnMaxUserNameLen = 254;
var
  sUserName: string;
  dwUserNameLen: DWORD;
begin
  dwUserNameLen := cnMaxUserNameLen - 1;
  SetLength(sUserName, cnMaxUserNameLen);
  GetUserName(PChar(sUserName), dwUserNameLen);
  SetLength(sUserName, dwUserNameLen-1);

  Result := sUserName;
end;

function GetCurrentComputerName: string;
var
  i: LongWord;
begin
  SetLength(Result, MAX_COMPUTERNAME_LENGTH + 1);
  i := Length(Result);
  if GetComputerName(@Result[1], i) then begin
    SetLength(Result, i);
  end;
end;

//���������� IP ������ ��� ������, ���� �� ���������
function GetCurrentIP:String;
type
  TaPInAddr = Array[0..10] of PInAddr;
  PaPInAddr = ^TaPInAddr;
var
  phe: PHostEnt;
  pptr: PaPInAddr;
  Buffer: Array[0..63] of AnsiChar;
  I: Integer;
  GInitData: TWSAData;
  IPs: TStringList;
begin
  //������ ��������� ���������� ��� Delphi XE7
  IPs := TStringList.Create;
  WSAStartup($101, GInitData);
  GetHostName(Buffer, SizeOf(Buffer));
  phe := GetHostByName(buffer);
  if phe = nil then IPs.Add('No IP found')
  else
  begin
    pPtr := PaPInAddr(phe^.h_addr_list);
    I := 0;
    while pPtr^[I] <> nil do
    begin
      IPs.Add(String(inet_ntoa(pptr^[I]^)));
      Inc(I);
    end;
  end;
  WSACleanup;
  IPs.Delimiter:=',';
  Result := IPs.CommaText;
end;

//��������� ������ ���������
function GetFileVersion(const FileName: string): string;
type
  PDWORD = ^DWORD;
  PLangAndCodePage = ^TLangAndCodePage;
  TLangAndCodePage = packed record
    wLanguage: WORD;
    wCodePage: WORD;
  end;
  PLangAndCodePageArray = ^TLangAndCodePageArray;
  TLangAndCodePageArray = array[0..0] of TLangAndCodePage;
var
  loc_InfoBufSize: DWORD;
  loc_InfoBuf: PChar;
  loc_VerBufSize: DWORD;
  loc_VerBuf: PChar;
  cbTranslate: DWORD;
  lpTranslate: PDWORD;
  i: DWORD;
begin
  Result := '';
  if (Length(FileName) = 0) or (not Fileexists(FileName)) then
    Exit;
  loc_InfoBufSize := GetFileVersionInfoSize(PChar(FileName), loc_InfoBufSize);
  if loc_InfoBufSize > 0 then begin
    loc_VerBuf := nil;
    loc_InfoBuf := AllocMem(loc_InfoBufSize);
    try
      if not GetFileVersionInfo(PChar(FileName), 0, loc_InfoBufSize, loc_InfoBuf) then
        exit;
      if not VerQueryValue(loc_InfoBuf, '\\VarFileInfo\\Translation', Pointer(lpTranslate), DWORD(cbTranslate)) then
        exit;
      for i := 0 to (cbTranslate div SizeOf(TLangAndCodePage)) - 1 do begin
        if VerQueryValue(
          loc_InfoBuf,
          PChar(Format(
          'StringFileInfo\0%x0%x\FileVersion', [
          PLangAndCodePageArray(lpTranslate)[i].wLanguage,
            PLangAndCodePageArray(lpTranslate)[i].wCodePage])),
            Pointer(loc_VerBuf),
          DWORD(loc_VerBufSize)
          ) then
        begin
          Result := loc_VerBuf;
          Break;
        end;
      end;
    finally
      FreeMem(loc_InfoBuf, loc_InfoBufSize);
    end;
  end;
end;

//�������� OriginalFileName
function GetOriginalFileName(const FileName: string): string;
type
  PLandCodepage = ^TLandCodepage;
  TLandCodepage = record
    wLanguage,
    wCodePage: word;
  end;
var
  dummy, len: cardinal;
  buf, pntr: pointer;
  lang: string;
begin
  len := GetFileVersionInfoSize(PChar(FileName), dummy);
  if len = 0 then
    RaiseLastOSError;
  GetMem(buf, len);
  try
    if not GetFileVersionInfo(PChar(FileName), 0, len, buf) then
      RaiseLastOSError;

    if not VerQueryValue(buf, '\VarFileInfo\Translation\', pntr, len) then
      RaiseLastOSError;

    lang := Format('%.4x%.4x', [PLandCodepage(pntr)^.wLanguage, PLandCodepage(pntr)^.wCodePage]);

//    if VerQueryValue(buf, PChar('\StringFileInfo\' + lang + '\CompanyName'), pntr, len){ and (@len <> nil)} then
//      result.CompanyName := PChar(pntr);
//    if VerQueryValue(buf, PChar('\StringFileInfo\' + lang + '\FileDescription'), pntr, len){ and (@len <> nil)} then
//      result.FileDescription := PChar(pntr);
//    if VerQueryValue(buf, PChar('\StringFileInfo\' + lang + '\FileVersion'), pntr, len){ and (@len <> nil)} then
//      result.FileVersion := PChar(pntr);
//    if VerQueryValue(buf, PChar('\StringFileInfo\' + lang + '\InternalName'), pntr, len){ and (@len <> nil)} then
//      result.InternalName := PChar(pntr);
//    if VerQueryValue(buf, PChar('\StringFileInfo\' + lang + '\LegalCopyright'), pntr, len){ and (@len <> nil)} then
//      result.LegalCopyright := PChar(pntr);
//    if VerQueryValue(buf, PChar('\StringFileInfo\' + lang + '\LegalTrademarks'), pntr, len){ and (@len <> nil)} then
//      result.LegalTrademarks := PChar(pntr);
    if VerQueryValue(buf, PChar('\StringFileInfo\' + lang + '\OriginalFileName'), pntr, len){ and (@len <> nil)} then
      Result := PChar(pntr);
//    if VerQueryValue(buf, PChar('\StringFileInfo\' + lang + '\ProductName'), pntr, len){ and (@len <> nil)} then
//      result.ProductName := PChar(pntr);
//    if VerQueryValue(buf, PChar('\StringFileInfo\' + lang + '\ProductVersion'), pntr, len){ and (@len <> nil)} then
//      result.ProductVersion := PChar(pntr);
//    if VerQueryValue(buf, PChar('\StringFileInfo\' + lang + '\Comments'), pntr, len){ and (@len <> nil)} then
//      result.Comments := PChar(pntr);
//    if VerQueryValue(buf, PChar('\StringFileInfo\' + lang + '\PrivateBuild'), pntr, len){ and (@len <> nil)} then
//      result.PrivateBuild := PChar(pntr);
//    if VerQueryValue(buf, PChar('\StringFileInfo\' + lang + '\SpecialBuild'), pntr, len){ and (@len <> nil)} then
//      result.SpecialBuild := PChar(pntr);
  finally
    FreeMem(buf);
  end;
end;

//�������� ProgramVersion
function GetProgramVersion(const FileName: string): string;
type
  PLandCodepage = ^TLandCodepage;
  TLandCodepage = record
    wLanguage,
    wCodePage: word;
  end;
var
  dummy, len: cardinal;
  buf, pntr: pointer;
  lang: string;
begin
  len := GetFileVersionInfoSize(PChar(FileName), dummy);
  if len = 0 then
    RaiseLastOSError;
  GetMem(buf, len);
  try
    if not GetFileVersionInfo(PChar(FileName), 0, len, buf) then
      RaiseLastOSError;

    if not VerQueryValue(buf, '\VarFileInfo\Translation\', pntr, len) then
      RaiseLastOSError;

    lang := Format('%.4x%.4x', [PLandCodepage(pntr)^.wLanguage, PLandCodepage(pntr)^.wCodePage]);

    if VerQueryValue(buf, PChar('\StringFileInfo\' + lang + '\ProgramVersion'), pntr, len){ and (@len <> nil)} then
      Result := PChar(pntr);
  finally
    FreeMem(buf);
  end;
end;

//������� ������ Enter, ������ ��������� 2 ������� #$0D � #$0A
function TrimRightEnter(const S: string): string;
var
  I: Integer;
begin
  I := Length(S);
  if (I > 0) and (S[I] = #$0A) then Dec(I);
  if (I > 0) and (S[I] = #$0D) then Dec(I);
  Result := Copy(S, 1, I);
end;

procedure _StartTimer;
begin
  FTimer := GetTickCount;
end;

function _StopTimer: Integer;
begin
  Result := GetTickCount - FTimer;
end;

//������ ���� ������
Function FirstDayOfMonth(const DateTime: TDateTime):TDateTime;
var
  y, m, d: Word;
begin
  DecodeDate(DateTime, y, m, d);
  Result := EncodeDate(y, m, 1);
end;

//��������� ���� ������
function LastDayOfMonth(const DateTime: TDateTime):TDateTime;
var
  y, m, d: Word;
begin
  DecodeDate(DateTime, y, m, d);
  m := m + 1;
  if m >12 then
  begin
    y := y + 1;
    m := 1;
  end;
  Result := EncodeDate(y, m, 1) - 1;
end;

// ���������� ���� � ����������
function ShowMsg(MessageText: string; Caption: string = '';
  Buttons: Integer = MB_OK + MB_ICONINFORMATION): Integer;
var
  Capt: string;
begin
  if trim(Caption) = '' then
    Capt := '��������'
  else
    Capt := Caption;
{$WARNINGS OFF}
  Result := Application.MessageBox(pchar(MessageText + '   '), pchar(Capt), Buttons);
{$WARNINGS ON}
end;

function MessageBoxTimeOut(hWnd: HWND; lpText: PChar; lpCaption: PChar; uType: UINT; wLanguageId: WORD; dwMilliseconds: DWORD): Integer; stdcall; external user32 name 'MessageBoxTimeoutW';
function ShowMsgTimeOut(MessageText: string; timeOut: integer = 30000; Caption: string = ''; Buttons: Integer = MB_OK + MB_ICONINFORMATION): Integer;
var
  Capt: string;
//  iRet: Integer;
//  iFlags: Integer;
begin
  //http://delphi.about.com/od/adptips2004/a/bltip1004_5.htm

  if trim(Caption) = '' then
    Capt := '��������'
  else
    Capt := Caption;

   Result:=MessageBoxTimeout(Application.Handle, pchar(MessageText), pchar(Capt), Buttons, 0, timeOut) ;

{  //Test
   iFlags := MB_YESNO or MB_SETFOREGROUND or MB_SYSTEMMODAL or MB_ICONINFORMATION;
   iRet := MessageBoxTimeout(Application.Handle, 'Test a timeout of 5 seconds.', 'MessageBoxTimeout Test', iFlags, 0, 5000) ;
   case iRet of
     IDYES:
       ShowMsg('Yes') ;
     IDNO:
       ShowMsg('No') ;
     MB_TIMEDOUT:
       ShowMsg('TimedOut') ;
   end;
}
end;

procedure CloseTe(taT:TdxMemData);
begin
  taT.Close;
  if taT.Active then
  begin
    taT.First;
    while not taT.Eof do taT.Delete;
    taT.Active:=False;
  end;
  taT.Open;
  if taT.Active then
  begin
    taT.First;
    while not taT.Eof do taT.Delete;
  end;
end;

function RoundEx(X: Double): Integer;
var  ScaledFractPart:Integer;
     Temp : Double;
begin
 ScaledFractPart := Trunc(X);
 Temp := Trunc(Frac(X)*1000000)+1;
 if Temp >=  500000 then ScaledFractPart := ScaledFractPart + 1;
 if Temp <= -500000 then ScaledFractPart := ScaledFractPart - 1;
 RoundEx := ScaledFractPart;
end;

function RoundPr(X: Double; Precision: Integer = 0): Double;
var prec: Integer;
begin
  if Precision>0 then begin
    prec:=Trunc(Power(10,Precision));  //10^Precision
    Result:=RoundEx(X*prec)/prec;
  end else
    Result:=RoundEx(X);
end;

function RoundHi( X: Double ): Integer;
var  ScaledFractPart:Integer;
begin
 ScaledFractPart := Trunc(X);
 if Frac(X)>0.0001 then ScaledFractPart := ScaledFractPart + 1;
 RoundHi:=ScaledFractPart;
end;

//��������� ������ Items, �������� � ComboBox, ������ � ���������
procedure FillItemsFromDataSet(Items: TStrings; DataSet: TDataSet; ID: TIntegerField;  Name: TStringField);
begin
  Items.Clear;
  DataSet.First;
  while not DataSet.Eof do begin
    Items.AddObject(Name.AsString, TObject(ID.AsInteger));  //��������� �������� � ���� � ������
    DataSet.Next;
  end;
  DataSet.First;
end;

//���������� �������� ����� �� ������ Items
function GetItemKey(Items: TStrings; ItemIndex: Integer): Integer;
begin
  Result:=0;
  if ItemIndex<0 then Exit;
  Result:=Integer(TObject(Items.Objects[ItemIndex]));
end;

//������� ����� ������ �� �����
function LocateItemIndexByKey(Items: TStrings; ID: Integer): integer;
var i: integer;
begin
  Result:=-1;
  for i :=0 to Items.Count-1 do begin
    if ID=Integer(TObject(Items.Objects[i])) then begin
      Result:=i;
      Break;
    end;
  end;
end;

//������ ���������� ����������� �� �� �������, �������� ���������� %SystemRoot% �� C:\Windows
function ExpandEnvVar(const Value: string): string;
var
  Buffer: array [0..MAX_PATH] of Char;
begin
  ExpandEnvironmentStrings(PChar(Value), Buffer, MAX_PATH - 1);
  Result := Buffer;
end;

//��������� � ������ �������� �������� ����������, ��� ��������� � ����������� ���������� MSSQL
function GetConnectionString(UdlFileName, ApplicationName: string):string;
Var i, indexProvider: integer;
    UDLFile: TStrings;
    Params: TStrings;
    {$IFDEF VER150} //Delphi 7
      f : File;
      WStrBuff : WideString;
      CountWideChar : Integer;
      StrRes  : String;
    {$ENDIF}
begin
  Result:='';
  if FileExists(UdlFileName)=False then Exit;

  UDLFile := TStringList.Create;
  Params := TStringList.Create;
  try
    {$IFDEF VER150} //Delphi 7
      //Delphi 7 �� ����� ������ UDLFile.LoadFromFile(UdlFileName) � ������� UTF16, �� ����� ������ ���
      AssignFile(f, UdlFileName);
      Reset(f, SizeOf(WideChar));
      CountWideChar := FileSize(f);
      SetLength(WStrBuff, CountWideChar);
      BlockRead(f, PWideChar(WStrBuff)^, CountWideChar);
      StrRes := WideCharLenToString(PWideChar(WStrBuff), CountWideChar);
      UDLFile.Text:=StrRes;
      Finalize(WStrBuff);
      CloseFile(f);

      indexProvider:=-1;
      for i := 0 to UDLFile.Count-1 do begin
        if AnsiLowerCase(Copy(UDLFile.Strings[i],1,8))=AnsiLowerCase('Provider') then begin
          indexProvider:=i;
          Break;
        end;
      end;
      if indexProvider>=0 then begin
        Result:=UDLFile.Strings[i];
        if Pos('Application Name',Result)=0 then
          Result:=Result+';Application Name='+ApplicationName;
      end;
    {$ELSE} //Delphi XE
      UDLFile.LoadFromFile(UdlFileName);
      indexProvider:=-1;
      for i := 0 to UDLFile.Count-1 do begin
        if AnsiLowerCase(Copy(UDLFile.Strings[i],1,8))=AnsiLowerCase('Provider') then begin
          indexProvider:=i;
          Break;
        end;
      end;
      if indexProvider>=0 then begin
        Params.Delimiter:=';';
        Params.StrictDelimiter := True;
        Params.DelimitedText:=UDLFile.Strings[i];
        Params.Values['Application Name']:=ApplicationName;
        //����� �������� �������������� �� Windows
//        Params.Values['Integrated Security']:='SSPI';
//        Params.Values['Persist Security Info']:='False';
//        Params.Values['User ID']:='';
//        Params.Values['Password']:='';
        Result:=Params.DelimitedText;
      end;
    {$ENDIF}

    //���� ��������� UDL ���� �� �������, �� ���������� ������ ������� �� ����
    if Result='' then Result:='FILE NAME='+UdlFileName;
  finally
    Params.Free;
    UDLFile.Free;
  end;
end;

//���������� ������������ SQL �������
function GetSQLServerName(ConnectionString: string):string;
Var Params: TStrings;
begin
  Result:='';
  Params := TStringList.Create;
  try
    {$IFDEF VER150} //Delphi 7
      Result:='';
    {$ELSE} //Delphi XE
      Params.Delimiter:=';';
      Params.StrictDelimiter := True;
      Params.DelimitedText:=ConnectionString;
      Result:=Params.Values['Data Source'];
      if Result='' then
      Result:=Params.Values['Server'];
    {$ENDIF}
  finally
    Params.Free;
  end;
end;

// ��������� ������ � �������� �������
Function Access_Resourse(PathResurse, Username, Password: String): Boolean;
var ShareName: string;
    Res: Integer;
    lpNetResource: TNetResource;
Const r = ERROR_SESSION_CREDENTIAL_CONFLICT;
begin
  ShareName := ExcludeTrailingPathDelimiter(PathResurse);
  ZeroMemory(@lpNetResource, SizeOf(lpNetResource)); //�������� ������ �������� ���������
  lpNetResource.dwType := RESOURCETYPE_ANY;
  lpNetResource.lpLocalName := nil;
  lpNetResource.lpProvider := nil;
  lpNetResource.lpRemoteName := PChar(ShareName);
  res := WNetAddConnection2(lpNetResource, PChar(Password), PChar(UserName), 0);
  // ���� ���������� ��� ����������� ������� ������������
  // �� ��������� ������ ��� ������������� ����� � ������
  if res = ERROR_SESSION_CREDENTIAL_CONFLICT then
     res := WNetAddConnection2(lpNetResource, Nil, Nil, 0);
  if Res = 0 then
     Result := True
  else
  begin
    Result := False;
    //ShowMessage('������ �������� �������� ������� - ' + PathResurse);
  end;
End;

// ��������� ������ ������ �� ��������
procedure FindFiles(StartFolder, Mask: string; List: TStrings; ScanSubFolders: Boolean = True);
var
  SearchRec: TSearchRec;
  FindResult: Integer;
begin
  List.BeginUpdate;
  try
{$IFDEF VER150} //Delphi 7
    StartFolder := IncludeTrailingBackslash(StartFolder);
{$ELSE} //Delphi XE
    StartFolder := IncludeTrailingPathDelimiter(StartFolder);
{$ENDIF}
    FindResult := FindFirst(StartFolder + '*.*', faAnyFile, SearchRec);
    try
      while FindResult = 0 do
        with SearchRec do
        begin
          if (Attr and faDirectory) <> 0 then
          begin
            if ScanSubFolders and (Name <> '.') and (Name <> '..') then
              FindFiles(StartFolder + Name, Mask, List, ScanSubFolders);
          end
          else
          begin
            if MatchesMask(Name, Mask) then
              List.Add(StartFolder + Name);
          end;
          FindResult := FindNext(SearchRec);
        end;
    finally
      FindClose(SearchRec);
    end;
  finally
    List.EndUpdate;
  end;
end;

//��������� �����
function SplitString(Str: String; Delimiter: Char = ';'):TStrings;
Var List: TStrings;
begin
  List:=TStringList.Create;
  List.Delimiter:=Delimiter;
{$IFDEF VER150} //Delphi 7
{$ELSE} //Delphi XE
  List.StrictDelimiter:=True;
{$ENDIF}
  List.DelimitedText:=Trim(Str);
  Result:=List;
end;

//��������� �������� �� ������ ������ ���������� �����
function IsNumber(const s: String): Boolean;
var i, len: Integer;
begin
  len := Length(s);
  if len > 0 then begin
    Result := True;
    for i := 1 to len do begin
{$IFDEF VER150} //Delphi 7
      if not (s[i] in ['0'..'9']) then begin
        Result := False;
        break;
      end;
{$ELSE} //Delphi XE
      if not CharInSet(s[i], ['0'..'9']) then begin
        Result := False;
        break;
      end;
{$ENDIF}
    end;
  end
  else
    Result := False;
end;

function GetMD5_File(AFilename: String): String;
var
  fs: TFileStream;
begin
  with TIdHashMessageDigest5.Create do
  begin
    fs := TFileStream.Create(AFilename, fmOpenRead or fmShareDenyNone);
    try
{$IFDEF VER150} //Delphi 7
      Result := AsHex(HashValue(fs)) ;
{$ELSE} //Delphi XE
      Result := HashStreamAsHex(fs);
{$ENDIF}
    finally
      fs.Free;
    end;
    Free;
  end;
end;

function GetMD5_Stream(AStream: TStream): String;
begin
  with TIdHashMessageDigest5.Create do
  begin
    AStream.Position:=0;
{$IFDEF VER150} //Delphi 7
      Result := AsHex(HashValue(AStream)) ;
{$ELSE} //Delphi XE
      Result := HashStreamAsHex(AStream);
{$ENDIF}
  end;
end;

//������ � ���������� �������
Procedure CompressionStream(streamInOut: TMemoryStream; CompressionLevel:TCompressionLevel = clDefault);
var streamPack     : TMemoryStream;
    Compression    : TCompressionStream;
begin
  streamPack := TMemoryStream.Create;
  try
    //<--������� streamInOut � streamPack
    streamInOut.Position := 0;
    Compression := TCompressionStream.Create(CompressionLevel, streamPack);
    try
      Compression.CopyFrom(streamInOut, streamInOut.Size);
    finally
      Compression.Free;
    end;
    //-->
    //<--�������� streamPack � streamInOut
    streamPack.Position:=0;
    streamInOut.Clear;          //�������� streamInOut
    streamInOut.CopyFrom(streamPack, streamPack.Size);
    streamInOut.Position:=0;
    //-->
  finally
    streamPack.Free;
  end;
end;

Procedure DeCompressionStream(streamInOut: TMemoryStream);
var streamPack     : TMemoryStream;
    Decompression  : TDecompressionStream;
begin
  streamPack   := TMemoryStream.Create;
  try
    //<--������������� streamInOut � streamPack
    streamInOut.Position:=0;
    Decompression := TDeCompressionStream.Create(streamInOut);
    try
      streamPack.CopyFrom(Decompression, Decompression.Size);
    finally
      Decompression.Free;
    end;
    //-->
    //<--�������� streamPack � streamInOut
    streamPack.Position:=0;
    streamInOut.Clear;          //�������� streamInOut
    streamInOut.CopyFrom(streamPack, streamPack.Size);
    streamInOut.Position:=0;
    //-->
  finally
    streamPack.Free;
  end;
end;

function RunningFromIDE: Boolean;
begin
  //������ ������ ����� ����� ����������.
  //1. ��������� �� ����� ��������, �� ������ ����� Delphi
  //2. ������ �� ������� ������ � ����� VirtualBox ����, ��� ������ �� ��� ��������� (������ ��������� �� ��������).
  {$IFDEF LINUX}
    Result := False;
  {$ELSE}
    Result := (DebugHook > 0);
  {$ENDIF}
end;

initialization
{$IFDEF VER150} //Delphi 7
  SysDecimalSeparator:=DecimalSeparator; //����������� ������� � ����� ������ ����� �� Windows
{$ELSE} //Delphi XE
  SysDecimalSeparator:=System.SysUtils.FormatSettings.DecimalSeparator; //����������� ������� � ����� ������ ����� �� Windows
{$ENDIF}
end.
