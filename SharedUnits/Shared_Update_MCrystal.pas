unit Shared_Update_MCrystal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Forms,
  Registry, Data.DB,
  FireDAC.Stan.Intf,FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client, FireDAC.UI.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Phys, FireDAC.Phys.MSSQL, FireDAC.Phys.MSSQLDef, FireDAC.VCLUI.Wait,
  FireDAC.Comp.UI, FireDAC.Phys.ODBCBase;

type
  TUpdater = class
  private
    FConnection: TFDConnection;
    FQuery: TFDQuery;

    FOriginalFileName: String;
    FCurrentVersion: String;
    FCurrentUserName: String;
    FCurrentComputerName: String;

    FLastBuildID: Integer;
    FLastBuildDDate: TDateTime;
    FLastBuildFileName: string;
    FLastBuildVersion: string;
    FLastBuildMD5: string;

    //��������� ���������� � ��
    function CheckDBConnection:Boolean;
    function GetConnection: TFDConnection;
    procedure SetConnection(const Value: TFDConnection);

    //�������� ��� ����� ��������� �� ��������
    function GetProgramFileNameFromRegistry:String;
    //��������� ��� ����� ��������� � �������
    procedure SetProgramFileNameToRegistry(ProgramFileName: String);
    //�������� ���������� � ��������� �������� ����� �� ���� ������
    function GetLastBuildFromDataBase:Boolean;
    //��������� ����������� ����� �����
    function CheckMd5InFile(FileName, Md5: string): boolean;
    //��������� �� �� ����� ���� ���������
    function DownloadLastBuildFromDataBase: Boolean;
  public
    constructor Create;
    destructor Destroy; override;
    property Connection: TFDConnection read GetConnection write SetConnection;

    //�������� ID ��������� ������ ��������� �� ��
    function GetLastIDBuild(OriginalFileName, UserName, CompName: String):Integer;
    //�������� ��������� ���������� ���������
    procedure CheckUpdate(var NeedRestart: Boolean; var RestartFileName: String);
    //���������� True ���� ����� ����� ������
    function CheckNewVersion(var LastBuildVersion: string): boolean;
  end;

var
  _Updater: TUpdater;

implementation

uses Shared_Logs_MCrystal, Shared_Functions;

constructor TUpdater.Create;
begin
  FQuery := TFDQuery.Create(nil);

  FOriginalFileName:=GetOriginalFileName(Application.ExeName);
  //FCurrentVersion:=GetFileVersion(Application.ExeName);
  FCurrentVersion:=GetProgramVersion(Application.ExeName);
  FCurrentUserName:=GetCurrentUserName;
  FCurrentComputerName:=GetCurrentComputerName;
end;

destructor TUpdater.Destroy;
begin
  FreeAndNil(FQuery);
  inherited;
end;

procedure TUpdater.SetConnection(const Value: TFDConnection);
begin
  FConnection := Value;
  FQuery.Connection := FConnection;
  if Assigned(FConnection) then begin
    FQuery.Transaction := FConnection.Transaction;
    FQuery.UpdateTransaction := FConnection.UpdateTransaction;
  end;
end;

function TUpdater.GetConnection: TFDConnection;
begin
  Result:=FConnection;
end;

function TUpdater.CheckDBConnection:Boolean;
begin
  Result:=False;
  if Assigned(FQuery.Connection)=false then Exit;

  if FQuery.Connection.Connected=false then begin
    FQuery.Connection.Connected:=true;
  end;
  Result:=FQuery.Connection.Connected;
end;

procedure TUpdater.CheckUpdate(var NeedRestart: Boolean; var RestartFileName: String);
var
  CurrentFileName: string;
  RegistryFileName: string;
  LastBuildExe: string;
  CompareVersion: Integer;
  NeedUpdate: Boolean;
begin
  NeedRestart:=False;
  RestartFileName:='';
  CurrentFileName:=Application.ExeName;
  if GetLastBuildFromDataBase then begin
    //�������� ������ �� ��
    CompareVersion:=AnsiCompareStr(FCurrentVersion, FLastBuildVersion);
    NeedUpdate:=False;
    if CompareVersion<0 then begin            //FCurrentVersion < FLastBuildVersion
      NeedUpdate:=True;
    end;
    if CompareVersion=0 then begin            //FCurrentVersion = FLastBuildVersion
      SetProgramFileNameToRegistry(CurrentFileName);
      Exit;   //������ �� ��������� ������, �������� �� ������ ������ ���������
    end;
    if CompareVersion>0 then begin            //FCurrentVersion > FLastBuildVersion
      Exit;   //������ �� ��������� ������, �������� �� ����� ����� ������
    end;

    if NeedUpdate then begin
      //������ ����������
      if DownloadLastBuildFromDataBase=False then begin
        //�������� �� �������
        Exit;
      end;
    end;

    LastBuildExe:=ExtractFilePath(CurrentFileName)+FLastBuildFileName;
    if FileExists(LastBuildExe) then begin
      //������� ����� ������, ����� ��������������� �� ����� ������
      NeedRestart:=True;
      RestartFileName:=LastBuildExe;
    end else begin
      //�������� ������� ����� ������ ���������, �� ������-�� �� ����������
      Exit;    //�������� �� �������
    end;
  end else begin
    //�� �������� ������ �� ��, ������� ��������� ����������� ���� �� ��������
    RegistryFileName:=ExtractFilePath(CurrentFileName)+GetProgramFileNameFromRegistry;
    if FileExists(RegistryFileName)=False then begin
      //��������� ����������� ��������� �� �������
      Exit;    //�������� �� �������
    end;
    if AnsiUpperCase(CurrentFileName)<>AnsiUpperCase(RegistryFileName) then begin
      //������� ��������� �� ����� ��������� �����������, ����� ��������������� �� ����� ������
      NeedRestart:=True;
      RestartFileName:=RegistryFileName;
    end else begin
      //������� ��������� ����� ��������� �����������
      Exit;   //������ �� ��������� ������, �������� �� ������ ������ ���������
    end;
  end;
end;

//�������� ��� ����� ��������� �� ��������
function TUpdater.GetProgramFileNameFromRegistry:String;
var Reg: TRegistry;
    Key: string;
begin
  Result:='';
  Reg := TRegistry.Create;
  try
    Reg.RootKey := HKEY_CURRENT_USER;
    Key := 'Software\'+Trim(FOriginalFileName)+'\Update';
    if Reg.OpenKeyReadOnly(Key) then
    begin
      if Reg.ValueExists('ProgramFileName') then
        Result := Reg.ReadString('ProgramFileName');
      Reg.CloseKey;
    end;
  finally
    Reg.Free
  end;
end;

//��������� ��� ����� ��������� � �������
procedure TUpdater.SetProgramFileNameToRegistry(ProgramFileName: String);
var Reg: TRegistry;
    Key: string;
begin
  Reg := TRegistry.Create;
  try
    Reg.RootKey := HKEY_CURRENT_USER;
    Key := 'Software\'+Trim(FOriginalFileName)+'\Update';
    if Reg.OpenKey(Key, True) then
    begin
      Reg.WriteString('ProgramFileName',ProgramFileName);
      Reg.CloseKey;
    end;
  finally
    Reg.Free
  end;
end;

//�������� ��������� ������ ��������� �� ��
function TUpdater.GetLastIDBuild(OriginalFileName, UserName, CompName: String):Integer;
//var IDPROG: Integer;
begin
  if CheckDBConnection=false then Exit(0);

  FQuery.Active:=False;
  FQuery.SQL.Clear;
  FQuery.SQL.Add('DECLARE @RC int');
  FQuery.SQL.Add('EXECUTE @RC = ELOG.dbo.GetLastIDUpdateBuild :UpdateProgramName, :UserName, :CompName');
  FQuery.SQL.Add('SELECT @RC AS res');
  FQuery.ParamByName('UpdateProgramName').Value:=OriginalFileName;
  FQuery.ParamByName('UserName').Value:=UserName;
  FQuery.ParamByName('CompName').Value:=CompName;
  FQuery.Active:=True;
  if FQuery.FieldByName('res').IsNull then
    Result:=0
  else
    Result:=FQuery.FieldByName('res').AsInteger;

//  //<--�������� ID ��������� �� ������������� �����
//  FQuery.Active:=False;
//  FQuery.SQL.Clear;
//  FQuery.SQL.Add('SELECT TOP 1 ID FROM ELOG.dbo.UpdatePrograms WHERE Name=:OriginalFileName');
//  FQuery.ParamByName('OriginalFileName').Value:=OriginalFileName;
//  FQuery.Active:=True;
//  if FQuery.FieldByName('ID').IsNull then
//    Exit(0)
//  else
//    IDPROG:=FQuery.FieldByName('ID').AsInteger;
//  FQuery.Active:=False;
//  //-->
//
//  //<--�������� ID ���������� ����� �� ID ���������
//  FQuery.Active:=False;
//  FQuery.SQL.Clear;
//  FQuery.SQL.Add('SELECT TOP 1 ID FROM ELOG.dbo.UpdateBuilds');
//  FQuery.SQL.Add('WHERE IActive=1');
//  FQuery.SQL.Add('  AND IDUpProg=:IDPROG');
//  FQuery.SQL.Add('ORDER BY DDate DESC');
//  FQuery.ParamByName('IDPROG').Value:=IDPROG;
//  FQuery.Active:=True;
//  if FQuery.FieldByName('ID').IsNull then
//    Exit(0)
//  else
//    Result:=FQuery.FieldByName('ID').AsInteger;
//  FQuery.Active:=False;
//  //-->
end;

//�������� ���������� � ��������� �������� ����� �� ���� ������
function TUpdater.GetLastBuildFromDataBase:Boolean;
begin
  FLastBuildID:=GetLastIDBuild(FOriginalFileName, FCurrentUserName, FCurrentComputerName);
  if FLastBuildID=0 then Exit(False);

  FQuery.Active:=False;
  FQuery.SQL.Clear;
  FQuery.SQL.Add('SELECT TOP 1');
  FQuery.SQL.Add('  ID');
  FQuery.SQL.Add(' ,DDate');
  FQuery.SQL.Add(' ,FileName');
  FQuery.SQL.Add(' ,Version');
  FQuery.SQL.Add(' ,Md5');
  FQuery.SQL.Add('FROM ELOG.dbo.UpdateBuilds');
  FQuery.SQL.Add('WHERE ID=:IDBuild');
  FQuery.ParamByName('IDBuild').Value:=FLastBuildID;
  FQuery.Active:=True;

  if FQuery.RecordCount=0 then Exit(False);

  FLastBuildID:=FQuery.FieldByName('ID').AsInteger;
  FLastBuildDDate:=FQuery.FieldByName('DDate').AsDateTime;
  FLastBuildFileName:=FQuery.FieldByName('FileName').AsString;
  FLastBuildVersion:=FQuery.FieldByName('Version').AsString;
  FLastBuildMD5:=FQuery.FieldByName('Md5').AsString;
  FQuery.Active:=False;
  Result:=True;
end;

//��������� �� �� ����� ���� ���������
function TUpdater.DownloadLastBuildFromDataBase: Boolean;
var
  LastBuildExe: string;
  FlagFileName: string;
  streamMemory: TMemoryStream;
  F: TextFile;
begin
  if FLastBuildID=0 then Exit(False);

  LastBuildExe:=ExtractFilePath(Application.ExeName)+FLastBuildFileName;
  if FileExists(LastBuildExe) then begin
    if CheckMd5InFile(LastBuildExe, FLastBuildMD5) then begin
      Exit(True);    //���������� �� ���������
    end else begin
      //����������� ����� �� �������, ��������
    end;
  end;

  FlagFileName:=ExtractFilePath(Application.ExeName)+'Updater.txt';
  AssignFile(F, FlagFileName);
  try
    //<--��������� ��������� ���� ����� ������������ �� ����������� ����������
    try
      Rewrite(F);
    except
      Exit(False);  //�������� ������ ��� �������� �����, �������� ���������� ��� �����������
    end;
    WriteLn(F,FormatDateTime('dd.mm.yyyy hh:nn:ss', Now)+'  ��������� ����������');
    WriteLn(F,FormatDateTime('dd.mm.yyyy hh:nn:ss', Now)+'  �������� ���������: '+FOriginalFileName);
    WriteLn(F,FormatDateTime('dd.mm.yyyy hh:nn:ss', Now)+'  ������� ������ ���������: '+FCurrentVersion);
    WriteLn(F,FormatDateTime('dd.mm.yyyy hh:nn:ss', Now)+'  ������������: '+FCurrentUserName);
    WriteLn(F,FormatDateTime('dd.mm.yyyy hh:nn:ss', Now)+'  ���������: '+FCurrentComputerName);
    WriteLn(F,FormatDateTime('dd.mm.yyyy hh:nn:ss', Now)+'  ID ����� ������ ���������: '+IntToStr(FLastBuildID));
    WriteLn(F,FormatDateTime('dd.mm.yyyy hh:nn:ss', Now)+'  ���� ����� ������ ���������: '+FormatDateTime('dd.mm.yyyy hh:nn:ss',FLastBuildDDate));
    WriteLn(F,FormatDateTime('dd.mm.yyyy hh:nn:ss', Now)+'  ��� ����� ����� ������ ���������: '+FLastBuildFileName);
    WriteLn(F,FormatDateTime('dd.mm.yyyy hh:nn:ss', Now)+'  ������ ����� ������ ���������: '+FLastBuildVersion);
    Flush(F);
    //-->

    FQuery.Active:=False;
    FQuery.SQL.Clear;
    FQuery.SQL.Add('SELECT');
    FQuery.SQL.Add('  ID');
    FQuery.SQL.Add(' ,FileName');
    FQuery.SQL.Add(' ,Blob');
    FQuery.SQL.Add(' ,Md5');
    FQuery.SQL.Add('FROM ELOG.dbo.UpdateBuilds');
    FQuery.SQL.Add('WHERE ID=:ID');
    FQuery.ParamByName('ID').Value:=FLastBuildID;
    FQuery.Active:=True;
    try
      if FQuery.RecordCount=0 then Exit(False);

      streamMemory:=TMemoryStream.Create;
      try
        TBLOBField(FQuery.FieldByName('Blob')).SaveToStream(streamMemory);
        //<--������������� �����
        try
          DeCompressionStream(streamMemory);
        except
          WriteLn(F,FormatDateTime('dd.mm.yyyy hh:nn:ss', Now)+'  ���������� �� ������, ������ ������������!');
          Exit(False);   //������ ������������
        end;
        //-->
        //<--���� ���� ����������, ����� ��� �������
        if FileExists(LastBuildExe) then
          if DeleteFile(LastBuildExe)=False then begin
            WriteLn(F,FormatDateTime('dd.mm.yyyy hh:nn:ss', Now)+'  ���������� �� ������, c����������� ���� �������� �� �������!');
            Exit(False);  //������������ ���� �������� �� �������
          end;
        //-->
        //<--��������� � ����
        streamMemory.SaveToFile(LastBuildExe);
        if CheckMd5InFile(LastBuildExe, FQuery.FieldByName('Md5').AsString) then begin
          //����������� ����� ����� ���������
          Result:=True;
          WriteLn(F,FormatDateTime('dd.mm.yyyy hh:nn:ss', Now)+'  ���������� ������ �������!');
        end else begin
          //����������� ����� ����� �� ���������
          Result:=False;
          WriteLn(F,FormatDateTime('dd.mm.yyyy hh:nn:ss', Now)+'  ���������� �� ������, ����������� ����� ����� �� ���������!');
          DeleteFile(LastBuildExe);  //������� ������������ ����
        end;
        //-->
      finally
        streamMemory.Free;
      end;
    finally
      FQuery.Active:=False;
    end;
  finally
    //����� ���������� ��������� ����
    WriteLn(F,FormatDateTime('dd.mm.yyyy hh:nn:ss', Now)+'  ��������� ���������� ���������� ���������.');
    CloseFile(F);
  end;
end;

function TUpdater.CheckMd5InFile(FileName, Md5: string): boolean;
var md5_file: string;
begin
  if FileExists(FileName)=false then Exit(False);

  md5_file:=GetMD5_File(FileName);
  if AnsiUpperCase(Trim(md5_file))=AnsiUpperCase(Trim(Md5)) then begin
    Result:=True;
  end else begin
    Result:=False;
  end;
end;

function TUpdater.CheckNewVersion(var LastBuildVersion: string): boolean;
var CurrentFileName: string;
    CompareVersion: Integer;
begin
  Result:=False;
  LastBuildVersion:='';
  CurrentFileName:=Application.ExeName;
  if GetLastBuildFromDataBase then begin
    //�������� ������ �� ��
    CompareVersion:=AnsiCompareStr(FCurrentVersion, FLastBuildVersion);
    if CompareVersion<0 then begin            //FCurrentVersion < FLastBuildVersion
      LastBuildVersion:=FLastBuildVersion;
      Result:=True;
    end;
  end;
end;

initialization
  _Updater:=TUpdater.Create;
finalization
  FreeAndNil(_Updater);
end.
