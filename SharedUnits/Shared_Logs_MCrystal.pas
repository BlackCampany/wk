//==============================================================================
// ����� ������ ��� ������ ��������� ���� � �� ��� ������ MCrystal
// ���� �������: 10.12.2014
//
// ����� �������������� ������� Connection
//  _Log.Connection:=FDConnection;  //�������� ������� � ��, ����� ��� ��� �������� � ��
//==============================================================================

unit Shared_Logs_MCrystal;

interface

uses SysUtils, Classes, Types, Forms, Windows, ADODB, ActiveX, Shared_Functions, ExtCtrls, JPEG, Graphics,
  FireDAC.Stan.Intf,FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client, FireDAC.UI.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Phys, FireDAC.Phys.MSSQL, FireDAC.Phys.MSSQLDef, FireDAC.VCLUI.Wait,
  FireDAC.Comp.UI, FireDAC.Phys.ODBCBase;

type
  TLogWriter = class
  private
    FCacheLogEnabled: Boolean;
    FCacheLogMaxCount: Integer;
    FCacheLog: TStrings;
    FExemplar: Integer;
    FTimer: TTimer;
    FTimerEnabled: Boolean;
    FScreenShotOnError: Boolean;
    FConnection: TFDConnection;
    FDTrans: TFDTransaction;
    FDTransUpdate: TFDTransaction;
    FDPhysMSSQLDriverLink: TFDPhysMSSQLDriverLink;
    FDGUIxWaitCursor: TFDGUIxWaitCursor;
    FQuery: TFDQuery;
    FCurrentUserName: string;
    FCurrentComputerName: string;
    FCurrentIDPersonal: Integer;

    //��������� ���������� � ��
    function CheckDBConnection:Boolean;
    function GetConnection: TFDCustomConnection;
    procedure SetConnection(const Value: TFDCustomConnection);
    function GetTimesInterval: Integer;
    procedure SetTimesInterval(const Value: Integer);
    function GetTimesEnabled: Boolean;
    procedure SetTimesEnabled(const Value: Boolean);
    procedure Timer(Sender: TObject);
  public
    constructor Create;
    destructor Destroy; override;
    property Connection: TFDCustomConnection read GetConnection write SetConnection;
    property CacheLogEnabled: boolean read FCacheLogEnabled write FCacheLogEnabled;
    property CacheLogMaxCount: integer read FCacheLogMaxCount write FCacheLogMaxCount;
    property Exemplar: Integer read FExemplar write FExemplar;
    property IDPersonal: Integer read FCurrentIDPersonal write FCurrentIDPersonal;
    property TimesInterval: Integer read GetTimesInterval write SetTimesInterval;
    property TimesEnabled: Boolean read GetTimesEnabled write SetTimesEnabled;
    property ScreenShotOnError: boolean read FScreenShotOnError write FScreenShotOnError;

    //���������� �������� � �� ������� ProgLogs, ��������� �������� ��������� spProgLogsAddMessage
    procedure WriteMessageToLog(aMessage: string);
    //���������� �������� � �� ������� ProgErrors, ��������� �������� ��������� spProgErrorsAddMessage
    procedure WriteMessageToErrors(aMessage: string);
    //��������� ������������ ��� � ��
    procedure SaveCacheLog;
  end;

var
  _Log: TLogWriter;

implementation

uses Math;

function GetScreenShot:TMemoryStream;
var
  c: TCanvas;
  r: TRect;
  Stream: TMemoryStream;
  Jpeg: TJpegImage;
  Bitmap: TBitmap;
begin
  try
    c:=TCanvas.Create;
    Bitmap:=TBitmap.Create;
    Jpeg:=TJpegImage.Create;
    Stream:=TMemoryStream.Create;
    c.Handle:=GetWindowDC(GetDesktopWindow);   // �������� handle �������� �����
    try
      r := Rect(0, 0, Screen.DesktopWidth, Screen.DesktopHeight);  // ���������� ��� �������
      Bitmap.Width := Screen.DesktopWidth;
      Bitmap.Height := Screen.DesktopHeight;
      Bitmap.Canvas.CopyRect(r, c, r);         //� �������� � Bitmap ����������� ������

      Jpeg.Assign(Bitmap);                     //�������� ����������� � ����� TJpegImage
      Jpeg.SaveToStream(Stream);               //�������� Jpeg � ������
      Stream.Seek(0,0);
    finally
      ReleaseDC(0, c.Handle);
      c.Free;
      Bitmap.Free;
      Jpeg.Free;
    end;
    Result:=Stream;
  except
    Result:=nil;
  end;
end;

constructor TLogWriter.Create;
begin
  FExemplar:=0;

  FConnection := TFDConnection.Create(nil);
  FConnection.LoginPrompt := False;
  FConnection.Transaction := FDTrans;
  FConnection.UpdateTransaction := FDTransUpdate;
  FQuery := TFDQuery.Create(nil);
  FQuery.Connection := FConnection;
  FQuery.Transaction := FDTrans;
  FQuery.UpdateTransaction := FDTransUpdate;

  FCacheLogMaxCount:=100;
  FCacheLog:=TStringList.Create;
  FCacheLogEnabled:=True;

  FTimerEnabled:=True;
  FTimer:=TTimer.Create(nil);
  FTimer.Interval:=5000;
  FTimer.Enabled:=False;
  FTimer.OnTimer:=Timer;

  FScreenShotOnError:=False;

  FCurrentUserName:=GetCurrentUserName;
  FCurrentComputerName:=GetCurrentComputerName;
end;

destructor TLogWriter.Destroy;
begin
  SaveCacheLog; //��������� ��� ����� ������������, ����� ���-�� ��������

  FreeAndNil(FTimer);
  FreeAndNil(FCacheLog);
  FreeAndNil(FQuery);
  FreeAndNil(FConnection);

  inherited;
end;

procedure TLogWriter.SetConnection(const Value: TFDCustomConnection);
begin
  FConnection.ConnectionString := Value.ConnectionString;
  FQuery.Connection:=FConnection;

  FTimer.Enabled:=FTimerEnabled;
end;

function TLogWriter.GetConnection: TFDCustomConnection;
begin
  Result:=FQuery.Connection;
end;

function TLogWriter.GetTimesInterval: Integer;
begin
  Result:=FTimer.Interval;
end;

procedure TLogWriter.SetTimesInterval(const Value: Integer);
begin
  FTimer.Interval:=Value;
end;

function TLogWriter.GetTimesEnabled: Boolean;
begin
  Result:=FTimerEnabled;
end;

procedure TLogWriter.SetTimesEnabled(const Value: Boolean);
begin
  FTimerEnabled:=Value;
  FTimer.Enabled:=Value;
end;

procedure TLogWriter.Timer(Sender: TObject);
begin
  SaveCacheLog;
end;

function TLogWriter.CheckDBConnection:Boolean;
begin
  Result:=False;
  if Assigned(FQuery.Connection)=false then Exit;

  if FQuery.Connection.Connected=false then begin
    FQuery.Connection.Connected:=true;
  end;
  Result:=FQuery.Connection.Connected;
end;

procedure TLogWriter.SaveCacheLog;
var str: string;
begin
  //���� ��� ������, �� �������
  if FCacheLog.Count=0 then Exit;

  if CheckDBConnection=false then Exit;

  FTimer.Enabled:=False;

  str:= TrimRightEnter(FCacheLog.Text);  //������� ������ Enter
  FCacheLog.Clear;

  FQuery.Active:=False;
  FQuery.SQL.Clear;
  FQuery.SQL.Add('exec dbo.spProgLogsAddMessage :ProgName, :Message, :UserName, :CompName, :IDPersonal');
  FQuery.ParamByName('ProgName').Value:=ExtractFileName(ParamStr(0));
  FQuery.ParamByName('Message').AsWideMemo:=str;
  FQuery.ParamByName('UserName').Value:=FCurrentUserName;
  FQuery.ParamByName('CompName').Value:=FCurrentComputerName;
  FQuery.ParamByName('IDPersonal').Value:=FCurrentIDPersonal;

  try
    FQuery.ExecSQL;
  except
    FCacheLog.Insert(0,str);  //���� ������, �� �� ������������ ������ ���������� ������� � ���
  end;

  FTimer.Enabled:=FTimerEnabled;
end;

procedure TLogWriter.WriteMessageToLog(aMessage: string);
begin
  FCacheLog.Append(aMessage);

  //���� ��� �� ������������, �� ����� ��������� � ��
  if FCacheLogEnabled=false then begin
    SaveCacheLog;
    Exit;
  end;

  if FCacheLog.Count>=FCacheLogMaxCount then begin
    SaveCacheLog;
  end;
end;

procedure TLogWriter.WriteMessageToErrors(aMessage: string);
var Stream: TMemoryStream;
begin
  if CheckDBConnection=false then Exit;

  if ScreenShotOnError=False then begin
    //������ ���������� ��������� � ���� ��� ���������
    FQuery.Active:=False;
    FQuery.SQL.Clear;
    FQuery.SQL.Add('exec dbo.spProgErrorsAddMessage :ProgName, :Message, :UserName, :CompName, :IDPersonal');
    FQuery.ParamByName('ProgName').Value:=ExtractFileName(ParamStr(0));
    FQuery.ParamByName('Message').AsWideMemo:=aMessage;
    FQuery.ParamByName('UserName').Value:=FCurrentUserName;
    FQuery.ParamByName('CompName').Value:=FCurrentComputerName;
    FQuery.ParamByName('IDPersonal').Value:=FCurrentIDPersonal;
    FQuery.ExecSQL;
  end else begin
    //������ ���������� ��������� � ���� �� ����������
    FQuery.Active:=False;
    FQuery.SQL.Clear;
    FQuery.SQL.Add('exec dbo.spProgErrorsAddMessageWithScreen :ProgName, :Message, :Screen, :UserName, :CompName, :IDPersonal');
    FQuery.ParamByName('ProgName').Value:=ExtractFileName(ParamStr(0));
    FQuery.ParamByName('Message').AsWideMemo:=aMessage;
    FQuery.ParamByName('UserName').Value:=FCurrentUserName;
    FQuery.ParamByName('CompName').Value:=FCurrentComputerName;
    FQuery.ParamByName('IDPersonal').Value:=FCurrentIDPersonal;
    Stream:=GetScreenShot;               //�������� �������� � �����
    if Assigned(Stream) then begin
      FQuery.ParamByName('Screen').LoadFromStream(Stream, TDataType.ftBlob);   //���������� �������� �� ������ � ���� ����� �����
      Stream.Free;
    end;
    FQuery.ExecSQL;
  end;
end;

initialization
  CoInitialize(nil);
  _Log:=TLogWriter.Create;
finalization
  FreeAndNil(_Log);
  CoUninitialize;
end.


