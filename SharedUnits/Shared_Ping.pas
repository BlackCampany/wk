unit Shared_Ping;

interface

uses  Windows, SysUtils, Classes, WinSock;

function PingTest(InetAddress : string; iTimeOut:Integer; var sWr:String) : boolean;

implementation

type
  ip_option_information = packed record  // ���������� ��������� IP (�������������� ��������� � ������ ����� ������ � RFC791.
    Ttl : byte;// ����� ����� (������������ traceroute-��)
    Tos : byte;// ��� ������������, ������ 0
    Flags : byte;// ����� ��������� IP, ������ 0
    OptionsSize : byte;// ������ ������ � ���������, ������ 0, �������� 40
    OptionsData : Pointer;// ��������� �� ������
  end;

  icmp_echo_reply = packed record
    Address : u_long;      // ����� �����������
    Status : u_long;     // IP_STATUS (��. ����)
    RTTime : u_long;     // ����� ����� ���-�������� � ���-������� � �������������
    DataSize : u_short;      // ������ ������������ ������
    Reserved : u_short;      // ���������������
    Data : Pointer;  // ��������� �� ������������ ������
    Options : ip_option_information; // ���������� �� ��������� IP
  end;

  PIPINFO = ^ip_option_information;
  PVOID = Pointer;

  function IcmpCreateFile() : THandle; stdcall; external 'ICMP.DLL' name 'IcmpCreateFile';
  function IcmpCloseHandle(IcmpHandle : THandle) : BOOL; stdcall; external 'ICMP.DLL'  name 'IcmpCloseHandle';
  function IcmpSendEcho(  IcmpHandle : THandle;    // handle, ������������ IcmpCreateFile()
                          DestAddress : u_long;    // ����� ���������� (� ������� �������)
                          RequestData : PVOID;     // ��������� �� ���������� ������
                          RequestSize : Word;      // ������ ���������� ������
                          RequestOptns : PIPINFO;  // ��������� �� ���������� ��������� ip_option_information (����� ���� nil)
                          ReplyBuffer : PVOID;     // ��������� �� �����, ���������� ������.
                          ReplySize : DWORD;       // ������ ������ �������
                          Timeout : DWORD          // ����� �������� ������ � �������������
                         ) : DWORD; stdcall; external 'ICMP.DLL' name 'IcmpSendEcho';

function PingTest(InetAddress : string; iTimeOut:Integer; var sWr:String) : boolean;
var Handle : THandle;
    pingBuffer : array [0..31] of Char;
    pIpe : ^icmp_echo_reply;
    pHostEn : PHostEnt;
    wVersionRequested : WORD;
    lwsaData : WSAData;
    error : DWORD;
    destAddress : In_Addr;

    DestIP, ReplyIP: string;
begin
  sWr:='';
  result := false;

  Handle := IcmpCreateFile();
  if Handle = INVALID_HANDLE_VALUE then Exit;

  GetMem( pIpe, sizeof(icmp_echo_reply) + sizeof(pingBuffer));
  pIpe.Data := @pingBuffer;
  pIpe.DataSize := sizeof(pingBuffer);
  wVersionRequested := MakeWord(1,1);
  try
    error := WSAStartup(wVersionRequested,lwsaData);
    if (error <> 0) then begin
      sWr:='Error in call to WSAStartup(). Error code: '+IntToStr(error);
      Exit;
    end;

    pHostEn := GetHostByName(PAnsiChar(AnsiString(InetAddress)));
    error := GetLastError();
    if (error <> 0) then begin
      sWr:='Error in call to gethostbyname(). Error code: '+IntToStr(error);
      Exit;
    end;

    destAddress := PInAddr(pHostEn^.h_addr_list^)^;
    DestIP:=String(inet_ntoa(destAddress));
    // �������� ping-�����
    sWr:='Pinging '+pHostEn^.h_name+' ['+DestIP+'] with '+IntToStr(sizeof(pingBuffer))+' bytes of data:';
    IcmpSendEcho(Handle, destAddress.S_addr, @pingBuffer, sizeof(pingBuffer), Nil, pIpe, sizeof(icmp_echo_reply) + sizeof(pingBuffer), iTimeOut);

    error := GetLastError();
    if (error <> 0) then begin
      sWr:=sWr+#13#10+'Error in call to IcmpSendEcho(). Error code: '+IntToStr(error);
      Exit;
    end;

    // ������� ��������� �� ����������� ������
    ReplyIP:=IntToStr(LoByte(LoWord(pIpe^.Address)))+'.'+
             IntToStr(HiByte(LoWord(pIpe^.Address)))+'.'+
             IntToStr(LoByte(HiWord(pIpe^.Address)))+'.'+
             IntToStr(HiByte(HiWord(pIpe^.Address)));
    sWr:=sWr+#13#10+'Reply from '+ReplyIP;
    sWr:=sWr+#13#10+'Reply time: '+IntToStr(pIpe.RTTime)+' ms';

    //���� ����� �� ����, ������ ����� �������, �������� ��������� ��� �� ��� IP �������
    if (DestIP=ReplyIP) {and (pIpe.RTTime<200)} then Result:=True;
  finally
    IcmpCloseHandle(Handle);
    WSACleanup();
    FreeMem(pIpe);
  end;
end;

end.
