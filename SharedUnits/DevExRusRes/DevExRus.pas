unit DevExRus;

interface

implementation

{$R DevExRus.lang}

uses SysUtils, Classes, cxLocalization, Windows;

var
  cxLocalizer: TcxLocalizer;

procedure __DevExRus__;
var
  rs: TResourceStream;
begin
  try
    rs := TResourceStream.Create(HInstance, 'DEVEXRUS', RT_RCDATA);
    try
      cxLocalizer.LoadFromStream(rs);
      cxLocalizer.LanguageIndex := 1;
      cxLocalizer.Translate;
    finally
      FreeAndNil(rs);
    end;
  except
    //
  end;
end;

initialization
  cxLocalizer := TcxLocalizer.Create(nil);
  if Assigned(cxLocalizer) then
    __DevExRus__;

finalization
  if Assigned(cxLocalizer) then
    FreeAndNil(cxLocalizer);

end.
