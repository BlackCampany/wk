//==============================================================================
// ����� ������ ��� ������ ��������� ���� � �� ELOG
// ���� �������: 10.12.2014
//
// ����� �������������� ������� FDConnection
//  _Log.Connection:=FDConnection;  //�������� ������� � ��, ����� ��� ��� �������� � ��
//==============================================================================

unit Shared_Logs;

interface

uses SysUtils, Classes, Types, Forms, Windows, Data.DB, ActiveX, Shared_Functions, ExtCtrls, Graphics,
  Vcl.Imaging.jpeg,
  FireDAC.Stan.Intf,FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client, FireDAC.UI.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Phys, FireDAC.Phys.MSSQL, FireDAC.Phys.MSSQLDef, FireDAC.VCLUI.Wait,
  FireDAC.Comp.UI, FireDAC.Phys.ODBCBase;

type
  TLogWriter = class
  private
    FCacheLogEnabled: Boolean;
    FCacheLogMaxCount: Integer;
    FCacheLog: TStrings;
    FExemplar: Integer;
    FTimer: TTimer;
    FTimerEnabled: Boolean;
    FScreenShotOnError: Boolean;

    FConnection: TFDConnection;
    FQuery: TFDQuery;
    FCurrentUserName: string;
    FCurrentComputerName: string;
    FCurrentIDPersonal: Integer;

    //��������� ���������� � ��
    function CheckDBConnection:Boolean;
    function GetConnection: TFDConnection;
    procedure SetConnection(const Value: TFDConnection);
    function GetTimesInterval: Integer;
    procedure SetTimesInterval(const Value: Integer);
    function GetTimesEnabled: Boolean;
    procedure SetTimesEnabled(const Value: Boolean);
    procedure Timer(Sender: TObject);
  public
    constructor Create;
    destructor Destroy; override;

    property Connection: TFDConnection read GetConnection write SetConnection;
    property CacheLogEnabled: boolean read FCacheLogEnabled write FCacheLogEnabled;
    property CacheLogMaxCount: integer read FCacheLogMaxCount write FCacheLogMaxCount;
    property Exemplar: Integer read FExemplar write FExemplar;
    property IDPersonal: Integer read FCurrentIDPersonal write FCurrentIDPersonal;
    property TimesInterval: Integer read GetTimesInterval write SetTimesInterval;
    property TimesEnabled: Boolean read GetTimesEnabled write SetTimesEnabled;
    property ScreenShotOnError: boolean read FScreenShotOnError write FScreenShotOnError;

    //<--��������� ��� ������ �����
    //���������� �������� � �� ELog ������� ProgLogs, ��������� �������� ��������� spProgLogsAddMessage
    procedure WriteMessageToLog(aMessage: string);
    //-->

    //<--��������� ��� ������ � EDI
    //���������� �������� � �� ELog ������� EDILog, ��������� �������� ��������� spEDILogAddMessage
    procedure WriteMessageToEDILog(aMessage, aDOCNUM: String);
    //���������� �������� � �� ELog ������� EDILog_PRICAT, ��������� �������� ��������� spEDILog_PRICATAddMessage
    procedure WriteMessageToEDILog_PRICAT(aMessage: String; aIDH: Int64);
    //-->

    //<--��������� ��� ������ � ��������
    //���������� �������� � �� ELog ������� ProgErrors, ��������� �������� ��������� spProgErrorsAddMessage
    procedure WriteMessageToErrors(aMessage: string);
    //��������� ������������ ��� � ��
    procedure SaveCacheLog;
    //-->
  end;

var
  _Log: TLogWriter;

implementation

uses Math;

function GetScreenShot:TMemoryStream;
var
  c: TCanvas;
  r: TRect;
  Stream: TMemoryStream;
  Jpeg: TJpegImage;
  Bitmap: TBitmap;
begin
  try
    c:=TCanvas.Create;
    Bitmap:=TBitmap.Create;
    Jpeg:=TJpegImage.Create;
    Stream:=TMemoryStream.Create;
    c.Handle:=GetWindowDC(GetDesktopWindow);   // �������� handle �������� �����
    try
      r := Rect(0, 0, Screen.DesktopWidth, Screen.DesktopHeight);  // ���������� ��� �������
      Bitmap.Width := Screen.DesktopWidth;
      Bitmap.Height := Screen.DesktopHeight;
      Bitmap.Canvas.CopyRect(r, c, r);         //� �������� � Bitmap ����������� ������

      Jpeg.Assign(Bitmap);                     //�������� ����������� � ����� TJpegImage
      Jpeg.SaveToStream(Stream);               //�������� Jpeg � ������
      Stream.Seek(0,0);
    finally
      ReleaseDC(0, c.Handle);
      c.Free;
      Bitmap.Free;
      Jpeg.Free;
    end;
    Result:=Stream;
  except
    Result:=nil;
  end;
end;

constructor TLogWriter.Create;
begin
  FExemplar:=0;

  FQuery := TFDQuery.Create(nil);

  FCacheLogMaxCount:=100;
  FCacheLog:=TStringList.Create;
  FCacheLogEnabled:=True;

  FTimerEnabled:=True;
  FTimer:=TTimer.Create(nil);
  FTimer.Interval:=5000;
  FTimer.Enabled:=False;
  FTimer.OnTimer:=Timer;

  FScreenShotOnError:=False;

  FCurrentUserName:=GetCurrentUserName;
  FCurrentComputerName:=GetCurrentComputerName;
end;

destructor TLogWriter.Destroy;
begin
  SaveCacheLog; //��������� ��� ����� ������������, ����� ���-�� ��������

  FreeAndNil(FTimer);
  FreeAndNil(FCacheLog);
  FreeAndNil(FQuery);

  inherited;
end;

procedure TLogWriter.SetConnection(const Value: TFDConnection);
begin
  FConnection := Value;
  FQuery.Connection := FConnection;
  if Assigned(FConnection) then begin
    FQuery.Transaction := FConnection.Transaction;
    FQuery.UpdateTransaction := FConnection.UpdateTransaction;
  end;

  FTimer.Enabled:=FTimerEnabled;
end;

function TLogWriter.GetConnection: TFDConnection;
begin
  Result:=FConnection;
end;

function TLogWriter.GetTimesInterval: Integer;
begin
  Result:=FTimer.Interval;
end;

procedure TLogWriter.SetTimesInterval(const Value: Integer);
begin
  FTimer.Interval:=Value;
end;

function TLogWriter.GetTimesEnabled: Boolean;
begin
  Result:=FTimerEnabled;
end;

procedure TLogWriter.SetTimesEnabled(const Value: Boolean);
begin
  FTimerEnabled:=Value;
  FTimer.Enabled:=Value;
end;

procedure TLogWriter.Timer(Sender: TObject);
begin
  SaveCacheLog;
end;

function TLogWriter.CheckDBConnection:Boolean;
begin
  Result:=False;
  if Assigned(FQuery.Connection)=false then Exit;

  if FQuery.Connection.Connected=false then begin
    FQuery.Connection.Connected:=true;
  end;
  Result:=FQuery.Connection.Connected;
end;

procedure TLogWriter.SaveCacheLog;
var str: string;
begin
  //���� ��� ������, �� �������
  if FCacheLog.Count=0 then Exit;

  if CheckDBConnection=false then Exit;

  FTimer.Enabled:=False;

  str:= TrimRightEnter(FCacheLog.Text);  //������� ������ Enter
  FCacheLog.Clear;

  FQuery.Active:=False;
  FQuery.SQL.Clear;
  FQuery.SQL.Add('exec ELOG.dbo.spProgLogsAddMessage :IdProg, :Message, :Exemplar, :UserName, :CompName, :IDPersonal');
  FQuery.ParamByName('IdProg').Value:=ExtractFileName(ParamStr(0));
  //FQuery.ParamByName('Message').Value:=str;  //��� �������� � FireDAC � ������ ������ ����� 8002 �������� - [FireDAC][Phys][ODBC]-345. Data too large for variable [BodyCommit]. Max len = [8002], actual len = [87603] Hint: set the TFDParam.Size to a greater value.
  FQuery.ParamByName('Message').AsWideMemo:=str;  //����� ������ ���
  FQuery.ParamByName('Exemplar').Value:=FExemplar;
  FQuery.ParamByName('UserName').Value:=FCurrentUserName;
  FQuery.ParamByName('CompName').Value:=FCurrentComputerName;
  FQuery.ParamByName('IDPersonal').Value:=FCurrentIDPersonal;

  try
    FQuery.ExecSQL;
  except
    FCacheLog.Insert(0,str);  //���� ������, �� �� ������������ ������ ���������� ������� � ���
  end;

  FTimer.Enabled:=FTimerEnabled;
end;

procedure TLogWriter.WriteMessageToLog(aMessage: string);
begin
  FCacheLog.Append(aMessage);

  //���� ��� �� ������������, �� ����� ��������� � ��
  if FCacheLogEnabled=false then begin
    SaveCacheLog;
    Exit;
  end;

  if FCacheLog.Count>=FCacheLogMaxCount then begin
    SaveCacheLog;
  end;
end;

procedure TLogWriter.WriteMessageToEDILog(aMessage, aDOCNUM: String);
begin
  if CheckDBConnection=false then Exit;

  FQuery.Active:=False;
  //��������� ��������� � ����� ����
  FQuery.SQL.Clear;
  FQuery.SQL.Add('exec ELOG.dbo.spEDILogAddMessage :DOCNUM, :Message');
  FQuery.ParamByName('DOCNUM').Value:=aDOCNUM;
  FQuery.ParamByName('Message').AsWideMemo:=aMessage;
  FQuery.ExecSQL;
end;

procedure TLogWriter.WriteMessageToEDILog_PRICAT(aMessage: String; aIDH: Int64);
begin
  if CheckDBConnection=false then Exit;

  FQuery.Active:=False;
  //��������� ��������� � ����� ����
  FQuery.SQL.Clear;
  FQuery.SQL.Add('exec ELOG.dbo.spEDILog_PRICATAddMessage :IDH, :Message');
  FQuery.ParamByName('IDH').Value:=aIDH;
  FQuery.ParamByName('Message').AsWideMemo:=aMessage;
  FQuery.ExecSQL;
end;

procedure TLogWriter.WriteMessageToErrors(aMessage: string);
var Stream: TMemoryStream;
begin
  if CheckDBConnection=false then Exit;

  if ScreenShotOnError=False then begin
    //������ ���������� ��������� � ���� ��� ���������
    FQuery.Active:=False;
    FQuery.SQL.Clear;
    FQuery.SQL.Add('exec ELOG.dbo.spProgErrorsAddMessage :IdProg, :Message, :Exemplar, :UserName, :CompName, :IDPersonal');
    FQuery.ParamByName('IdProg').Value:=ExtractFileName(ParamStr(0));
    FQuery.ParamByName('Message').AsWideMemo:=aMessage;
    FQuery.ParamByName('Exemplar').Value:=FExemplar;
    FQuery.ParamByName('UserName').Value:=FCurrentUserName;
    FQuery.ParamByName('CompName').Value:=FCurrentComputerName;
    FQuery.ParamByName('IDPersonal').Value:=FCurrentIDPersonal;
    FQuery.ExecSQL;
  end else begin
    //������ ���������� ��������� � ���� �� ����������
    FQuery.Active:=False;
    FQuery.SQL.Clear;
    FQuery.SQL.Add('exec ELOG.dbo.spProgErrorsAddMessageWithScreen :IdProg, :Message, :Screen, :Exemplar, :UserName, :CompName, :IDPersonal');
    FQuery.ParamByName('IdProg').Value:=ExtractFileName(ParamStr(0));
    FQuery.ParamByName('Message').AsWideMemo:=aMessage;
    FQuery.ParamByName('Exemplar').Value:=FExemplar;
    FQuery.ParamByName('UserName').Value:=FCurrentUserName;
    FQuery.ParamByName('CompName').Value:=FCurrentComputerName;
    FQuery.ParamByName('IDPersonal').Value:=FCurrentIDPersonal;
    Stream:=GetScreenShot;               //�������� �������� � �����
    if Assigned(Stream) then begin
      FQuery.ParamByName('Screen').LoadFromStream(Stream, TFieldType.ftBlob);   //���������� �������� �� ������ � ���� ����� �����
      Stream.Free;
    end;
    FQuery.ExecSQL;
  end;
end;

initialization
  CoInitialize(nil);
  _Log:=TLogWriter.Create;
finalization
  FreeAndNil(_Log);
  CoUninitialize;
end.


