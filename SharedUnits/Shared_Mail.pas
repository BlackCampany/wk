unit Shared_Mail;

interface

uses System.SysUtils, System.Classes, Vcl.SvcMgr, Winapi.Windows,
  mimemess, mimepart, smtpsend, ssl_openssl, IniFiles, IdSMTP, IdMessage, IdAttachmentFile;

type
  TMailConfig = record
    Host: String;
    Login: String;
    Pass: String;
    From: String;
  private
    procedure SaveToIni(iniFileName: String; Section: String = 'SendMail');
  public
    procedure LoadFromIni(iniFileName: String; Section: String = 'SendMail');
  end;

function SendMail(pMailConfig: TMailConfig; pSubject, pTo: string; pBodyText: TStrings; pBodyAsHtml: boolean; pAttachmentFiles: TStrings):Boolean;
function SendMailIndy(pMailConfig: TMailConfig; pSubject, pTo: string; pBodyText: TStrings; pBodyAsHtml: boolean; pAttachmentFiles: TStrings):Boolean;

implementation

function SendMail(pMailConfig: TMailConfig; pSubject, pTo: string; pBodyText: TStrings; pBodyAsHtml: boolean; pAttachmentFiles: TStrings):Boolean;
var tmpMsg : TMimeMess;
    tmpMIMEPart : TMimePart;
    fHost, fFrom, fLogin, fPass: String;
    i: integer;
    attfile: string;
begin
  Result:=False;

  fHost:=pMailConfig.Host;
  fLogin:=pMailConfig.Login;
  fPass:=pMailConfig.Pass;
  fFrom:=pMailConfig.From;

  tmpMsg := TMimeMess.Create;
  try
    //Headers
    tmpMsg.Header.Subject := pSubject;
    tmpMsg.Header.From := fFrom;
    tmpMsg.Header.ToList.Add(pTo);
    //tmpMsg.Header.Priority := MP_high;

    //MIMe Parts
    tmpMIMEPart := tmpMsg.AddPartMultipart('alternate',nil);

    //����� ������
    if Assigned(pBodyText) then
      if pBodyAsHtml then
        tmpMsg.AddPartHTML(pBodyText, tmpMIMEPart)
      else
        tmpMsg.AddPartText(pBodyText, tmpMIMEPart);

    //���������� � ������ ����
    if Assigned(pAttachmentFiles) then
      if pAttachmentFiles.Count>0 then
        for i := 0 to pAttachmentFiles.Count-1 do begin
          attfile:=pAttachmentFiles.Strings[i];
          if Trim(attfile)='' then Continue;
          if FileExists(attfile)=False then Continue;
          tmpMsg.AddPartBinaryFromFile(attfile,tmpMIMEPart);
        end;

    //����������
    tmpMsg.EncodeMessage;
    if smtpsend.SendToRaw(fFrom, pTo, fHost, tmpMsg.Lines, fLogin, fPass) then
      Result:=True;
  finally
    tmpMsg.Free;
  end;
end;

function SendMailIndy(pMailConfig: TMailConfig; pSubject, pTo: string; pBodyText: TStrings; pBodyAsHtml: boolean; pAttachmentFiles: TStrings):Boolean;
var tmpMsg : TIdMessage;
    IdSMTP : TIdSMTP;
    fHost, fFrom, fLogin, fPass: String;
    i: integer;
    attfile: string;
begin
  Result:=False;

  fHost:=pMailConfig.Host;
  fLogin:=pMailConfig.Login;
  fPass:=pMailConfig.Pass;
  fFrom:=pMailConfig.From;

  tmpMsg := TIdMessage.Create(nil);
  IdSMTP := TIdSMTP.Create(nil);
  try
    tmpMsg.CharSet := 'UTF-8';
    tmpMsg.From.Text := fFrom;
    tmpMsg.Sender.Text := fFrom;
    tmpMsg.Subject:=pSubject;
    tmpMsg.Recipients.EMailAddresses := pTo;

    //����� ������
    if Assigned(pBodyText) then
      tmpMsg.Body.Text:=pBodyText.Text;

    //���������� � ������ ����
    if Assigned(pAttachmentFiles) then
      if pAttachmentFiles.Count>0 then
        for i := 0 to pAttachmentFiles.Count-1 do begin
          attfile:=pAttachmentFiles.Strings[i];
          if Trim(attfile)='' then Continue;
          if FileExists(attfile)=False then Continue;
          TidAttachmentFile.Create(tmpMsg.MessageParts,attfile);
        end;

    IdSMTP.Host:=fHost;
    IdSMTP.Username:=fLogin;
    IdSMTP.Password:=fPass;
    IdSMTP.Connect;
    if IdSMTP.Connected then begin
      IdSMTP.Send(tmpMsg);
      Result:=True;
    end;
  finally
    tmpMsg.Free;
    IdSMTP.Free;
  end;
end;

procedure TMailConfig.SaveToIni(iniFileName: String; Section: String = 'SendMail');
var iniFile:TIniFile;
begin
  iniFile:=TIniFile.Create(iniFileName);
  try
    //<--��������� ���������
    iniFile.WriteString(section,'Host',String(Host));
    iniFile.WriteString(section,'Login',String(Login));
    iniFile.WriteString(section,'Pass',String(Pass));
    iniFile.WriteString(section,'From',String(From));
    //-->

    iniFile.UpdateFile;
  except
  end;
  iniFile.Free;
end;

procedure TMailConfig.LoadFromIni(iniFileName: String; Section: String = 'SendMail');
var iniFile:TIniFile;
begin
  iniFile:=TIniFile.Create(iniFileName);

  //<--��������� ���������
  Host:=iniFile.ReadString(Section,'Host','192.168.0.111');
  Login:=iniFile.ReadString(Section,'Login','Order@Eliseysm.ru');
  Pass:=iniFile.ReadString(Section,'Pass','z7241z');
  From:=iniFile.ReadString(Section,'From','dev5@Eliseysm.ru');
  //-->

  iniFile.Free;

  if FileExists(iniFileName)=False then
    SaveToIni(iniFileName, Section);  //������� ����, ���� ��� ���
end;

end.
