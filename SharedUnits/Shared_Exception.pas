//==============================================================================
// ����� ������ ��� ������������ ���������� � ������ � �� ELOG
// ���� �������: 02.12.2014
//
// ��� ������ � �� ���������� ������ Shared_Logs
// _Log.Connection:=msConnection;
// ���.��������:
//   UnhandledExceptionsOnly: Boolean;  - ������������ ������ ������������� � try..Except ������
//   HideCallstackItemNoLine: Boolean;  - �� ����� ������� ������� ������ ��� ��������� ������
//   ShowMessageError: Boolean;         - ���������� ��������� �� ������
//   Lines: TSrings;                    - ���� ��������� Memo1.Lines, �� ������� ������ � ������� � Memo1
//
//==============================================================================

unit Shared_Exception;

interface

uses
  Windows, SysUtils, Classes, ActiveX,
  JclDebug, JclHookExcept, AppEvnts, Forms, Shared_Logs, Shared_Functions;


type
  TExceptionHandler = class
  private
    FAppEvents: TApplicationEvents;
    FOnException: TExceptionEvent;
    FUnhandledExceptionsOnly: Boolean;
    FHideCallstackItemNoLine: Boolean;
    FShowMessageError: Boolean;
    FLines: TStrings;
    FProgramBuild: string;
    FProgramVersion: string;

    procedure SetUnhandledExceptionsOnly(const Value: Boolean);
    procedure InternalLogException(Sender: TObject; E: Exception);
    procedure DeleteLinesWithNoLine(Str: TStringList);
  public
    constructor Create;
    destructor Destroy; override;

    // function that takes exception message and logs it with stack trace
    procedure LogExceptionWithStackTrace(const E: Exception);

    property OnException: TExceptionEvent read FOnException write FOnException;
    property UnhandledExceptionsOnly: Boolean read FUnhandledExceptionsOnly write SetUnhandledExceptionsOnly;
    property HideCallstackItemNoLine: Boolean read FHideCallstackItemNoLine write FHideCallstackItemNoLine;
    property ShowMessageError: Boolean read FShowMessageError write FShowMessageError;
    property Lines: TStrings read FLines write FLines;
    property ProgramVersion: String read FProgramVersion write FProgramVersion;
  end;

var
  _ExceptionHandler: TExceptionHandler;

implementation

procedure HookGlobalException(ExceptObj: TObject; ExceptAddr: Pointer; OSException: Boolean);
begin
  _ExceptionHandler.LogExceptionWithStackTrace(Exception(ExceptObj));
end;

{ TExceptionHandler }

constructor TExceptionHandler.Create;
begin
  FAppEvents := TApplicationEvents.Create(nil);
  UnhandledExceptionsOnly := True;
  HideCallstackItemNoLine := True;
  ShowMessageError := True;

  FProgramBuild := GetFileVersion(ParamStr(0));
end;

destructor TExceptionHandler.Destroy;
begin
  FreeAndNil(FAppEvents);

  inherited;
end;

procedure TExceptionHandler.LogExceptionWithStackTrace(const E: Exception);
var
  Str: TStringList;
  Xml: TStringList;
  s: string;
begin
  //<--���������� ��� ��������� ������ �� ���������, �� ��������� �������
  if E.ClassName='EcxEditValidationError' then begin ShowMsg(E.Message); Exit; end; //��� ������ ��������� ���� � ������ ����� ����� �� �����, � �������� ����� -
  if E.Message='�� ������� ����� ������ ��� ����������. ��������� �������� ����� ���� �������� �� ������� �� ���������� ������' then begin ShowMsg(E.Message); Exit; end;
  //-->

  Str := TStringList.Create;
  Xml := TStringList.Create;
  try
    //������� ����
    //<--������� ��������� �� ������, ����� ����� ������� ��������
    //if ShowMessageError then
    //  ShowException(E, nil);
    //-->

    JclLastExceptStackListToStrings(Str, false, false, false, false);

    if FHideCallstackItemNoLine then
      DeleteLinesWithNoLine(Str);

    Xml.Append('<exception>');
      Xml.Append('  <error>'+E.Message+' [ClassName='+E.ClassName+']'+'</error>');
      if Length(FProgramVersion)>0 then begin
        //s:=FProgramVersion+' ('+FProgramBuild+')';
        s:=FProgramVersion;
      end else begin
        s:=FProgramBuild;
      end;
      Xml.Append('  <version>'+s+'</version>');
      Xml.Append('  <callstack><![CDATA['+Str.Text+']]></callstack>');
    Xml.Append('</exception>');

    //����� ��������� � ��
    _Log.WriteMessageToErrors(Xml.Text);

    //���� Memo �������, ������� � � ����
    Str.Insert(0, E.Message);
    Str.Insert(1, '������ '+FProgramVersion);
    if Assigned(FLines) then
      FLines.Append(Str.Text);

    //<--������� ��������� �� ������, ����� ���� ��� ������� ��������
    if ShowMessageError then
      ShowException(E, nil);
    //-->
  finally
    FreeAndNil(Str);
    FreeAndNil(Xml);
  end;
end;

procedure TExceptionHandler.InternalLogException(Sender: TObject; E: Exception);
begin
  LogExceptionWithStackTrace(E);

  if Assigned(FOnException) then
    FOnException(Sender, E);
end;

procedure TExceptionHandler.SetUnhandledExceptionsOnly(const Value: Boolean);
begin
  FUnhandledExceptionsOnly := Value;

  if FUnhandledExceptionsOnly then begin
    JclRemoveExceptNotifier(HookGlobalException);
    FAppEvents.OnException := InternalLogException;
  end else begin
    JclAddExceptNotifier(HookGlobalException);
    FAppEvents.OnException := nil;
  end;
end;

procedure TExceptionHandler.DeleteLinesWithNoLine(Str: TStringList);
var i: Integer;
begin
  for i:= Str.Count-1 downto 0 do begin
    if Pos('(Line ', Str[i])=0 then Str.Delete(i);
  end;
end;

initialization
  // Disable stack tracking in dynamically loaded modules (it makes stack tracking code a bit faster)
  Include(JclStackTrackingOptions, stStaticModuleList);
  // Enable raw mode (default mode uses stack frames which aren't always generated by the compiler)
  Include(JclStackTrackingOptions, stRawMode);

  // create the exception handler here
  _ExceptionHandler := TExceptionHandler.Create;

  // Initialize Exception tracking
  JclStartExceptionTracking;
  JclHookExceptions;
finalization
  JclUnhookExceptions;
  JclStopExceptionTracking;
  FreeAndNil(_ExceptionHandler);
end.
