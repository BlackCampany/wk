unit Shared_TableViewer;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, dxSkinsCore, dxSkinCaramel, dxSkinOffice2010Silver, dxSkinOffice2013White, dxSkinscxPCPainter;

type
  TfmTableViewer = class(TForm)
    cxGridDBTableView: TcxGridDBTableView;
    cxGridLevel: TcxGridLevel;
    cxGrid: TcxGrid;
    dsDataSet: TDataSource;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    procedure Init(aDataSet: TDataSet; aCaption: String);
  public
    { Public declarations }
  end;

//var
//  fmTableViewer: TfmTableViewer;

procedure _ShowTable(aDataSet: TDataSet; aInNewWindow: boolean = False; aCaption: String = '');

implementation

{$R *.dfm}

procedure _ShowTable(aDataSet: TDataSet; aInNewWindow: boolean = False; aCaption: String = '');
var
  AForm: TfmTableViewer;
  i: integer;
begin
  if aInNewWindow=false then begin //���� �� � ����� ����
    //<--��������� ������� �� ��� ����� � ������ ���������
    for i := 0 to Screen.FormCount-1 do begin
      if Screen.Forms[i].ClassName='TfmTableViewer' then begin
        AForm:=TfmTableViewer(Screen.Forms[i]);
        if AForm.dsDataSet.DataSet=aDataSet then begin
          AForm.Init(aDataSet, aCaption);
          AForm.SetFocus;
          Application.ProcessMessages;
          Exit;
        end;
      end;
    end;
    //-->
  end;

  AForm := TfmTableViewer.Create(nil);
  try
    AForm.Init(aDataSet, aCaption);
    AForm.Show;
    Application.ProcessMessages;
  finally
    //AForm.Free;  //����������� ��� �������� �����
  end;
end;

procedure TfmTableViewer.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;     //���������� ����� ��� ��������
end;

procedure TfmTableViewer.Init(aDataSet: TDataSet; aCaption: String);
begin
  dsDataSet.DataSet:=aDataSet;
  cxGridDBTableView.ClearItems;
  cxGridDBTableView.DataController.CreateAllItems;
  if aCaption='' then
    Caption:='TableViewer: DataSet='+aDataSet.Name
  else
    Caption:=aCaption;
end;

end.
