unit Shared_SkinHelper;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, dxSkinsCore, dxSkinsDefaultPainters, cxLookAndFeels, dxSkinsForm,
  cxControls, cxContainer, cxEdit, cxGroupBox, cxRadioGroup, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ExtCtrls, dxSkinsLookAndFeelPainter,
  dxBar, dxRibbon, dxNavBar, dxNavBarViewsFact, cxPC, dxDockControl;

type
  TdxControlsSkinHelper = class
  private
    FPrevRootLookAndFeelChanged: TcxLookAndFeelChangedEvent;
    function NeedProcess: Boolean;
  protected
    procedure ProcessScreen;
    procedure ProcessForm(AForm: TCustomForm);
    procedure ProcessControl(AControl: TControl); virtual;
    procedure ActiveFormChanged(Sender: TObject);
    procedure RootLookAndFeelChanged(Sender: TcxLookAndFeel; AChangedValues: TcxLookAndFeelValues);
  public
    procedure AssignEventHandlers;
    procedure ResetEventHandlers;
  end;

implementation

var
  ControlsSkinHelper: TdxControlsSkinHelper;

procedure RegisterControlsSkinHelper;
begin
  ControlsSkinHelper := TdxControlsSkinHelper.Create;
  ControlsSkinHelper.AssignEventHandlers;
end;

procedure UnregisterControlsSkinHelper;
begin
  if ControlsSkinHelper <> nil then
  begin
    ControlsSkinHelper.ResetEventHandlers;
    FreeAndNil(ControlsSkinHelper);
  end;
end;

{ TdxControlsSkinHelper }

function TdxControlsSkinHelper.NeedProcess: Boolean;
begin
  Result := RootLookAndFeel.Painter.InheritsFrom(TdxSkinLookAndFeelPainter);
end;

type
  TcxCustomTabControlAccess = class(TcxCustomTabControl);

procedure TdxControlsSkinHelper.ProcessControl(AControl: TControl);
var
  NavBarSkinView: Integer;
begin
  // Ribbon - Requires you to change the ColorSchemeName property
  if AControl is TdxCustomRibbon then
    TdxCustomRibbon(AControl).ColorSchemeName := RootLookAndFeel.SkinName

  // TcxPageControl, TcxTabControl - Require you to select the Skin style
  else if AControl is TcxCustomTabControl then
    TcxCustomTabControlAccess(AControl).Style := 11 // Style = Skin

  // NavBar - Requires you to select the SkinExplorerBarView or SkinNavigationPaneView View
  else if AControl is TdxNavBar then
  begin
    NavBarSkinView := dxNavBarViewsFactory.IndexOfName('SkinNavigationPaneView');
    if NavBarSkinView <> -1 then
      TdxNavBar(AControl).View := NavBarSkinView;
  end;

  if Supports(AControl, IcxLookAndFeelContainer) then
    (AControl as IcxLookAndFeelContainer).GetLookAndFeel.SkinName := RootLookAndFeel.SkinName;
end;

type
  TdxDockingControllerAccess = class(TdxDockingController);

procedure TdxControlsSkinHelper.ProcessForm(AForm: TCustomForm);
var
  I: Integer;
  ABarManager: TdxBarManager;
//  ADockingManager: TdxDockingManager;
begin
  if AForm = nil then
    Exit;

  // ExpressBars
  ABarManager := GetBarManagerByForm(AForm);
  if ABarManager <> nil then
  begin
    ABarManager.LookAndFeel.SkinName := RootLookAndFeel.SkinName;
    ABarManager.Style := bmsUseLookAndFeel;
  end;

  // ExpressDocking Library
//  ADockingManager := TdxDockingControllerAccess(dxDockingController).FindManager(AForm);
//  if ADockingManager <> nil then
//    ADockingManager.ViewStyle := vsUseLookAndFeel;
  
  for I  := 0 to AForm.ControlCount - 1 do
    ProcessControl(AForm.Controls[I]);
end;

procedure TdxControlsSkinHelper.ProcessScreen;
var
  I: Integer;
begin
  for I := 0 to Screen.FormCount - 1 do begin
    try
      ProcessForm(Screen.Forms[I]);
    Except
    end;
  end;
end;

procedure TdxControlsSkinHelper.RootLookAndFeelChanged(
  Sender: TcxLookAndFeel; AChangedValues: TcxLookAndFeelValues);
begin
  if NeedProcess then
    ProcessScreen;
  if Assigned(FPrevRootLookAndFeelChanged) then
    FPrevRootLookAndFeelChanged(Sender, AChangedValues);
end;

procedure TdxControlsSkinHelper.ResetEventHandlers;
begin
  RootLookAndFeel.OnChanged := FPrevRootLookAndFeelChanged;
end;

procedure TdxControlsSkinHelper.ActiveFormChanged(Sender: TObject);
begin
  if NeedProcess then
    ProcessForm(TScreen(Sender).ActiveCustomForm);
end;

procedure TdxControlsSkinHelper.AssignEventHandlers;
begin
  FPrevRootLookAndFeelChanged := RootLookAndFeel.OnChanged;
  RootLookAndFeel.OnChanged := RootLookAndFeelChanged;
  Screen.OnActiveFormChange := ActiveFormChanged;
end;

initialization
  RegisterControlsSkinHelper;

finalization
  UnregisterControlsSkinHelper;

end.
