unit Shared_ExportGrid;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  cxGridExportLink, cxGrid, System.Win.ComObj, Excel2010,
  System.UITypes, Vcl.StdCtrls, cxExport, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, cxProgressBar,
  cxLabel, Menus, cxButtons, dxSkinsCore, dxSkinCaramel, dxSkinOffice2010Silver, dxSkinOffice2013White;

type

  { TfrmExportProgress }

  TfmExportProgress = class(TForm, IcxExportProgress)
    btnCancel: TcxButton;
    lbTitle: TcxLabel;
    pbProgress: TcxProgressBar;
    SaveDialog: TSaveDialog;

    procedure btnCancelClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    // IcxExportProgress
    procedure OnProgress(Sender: TObject; Percent: Integer);
  end;

type
  TSupportedExportType = (exNone, exHTML, exXML, exExcel, exCSV, exText, exText_TAB, exXLSX);

function Export_SaveGridToFile(aFilename: String; AExportType: TSupportedExportType; cxGrid: TcxGrid): Boolean;
function Export_SaveGridToFileWithSaveDialog(aInitialDir: string; cxGrid: TcxGrid): Boolean;
procedure Export_OpenFileInExcel(aFilename: String; AExportType: TSupportedExportType);

implementation

{$R *.dfm}

const
  sConfirmMessage = '�� ������ �������� �������� ��������?';

function Export_SaveGridToFile(aFilename: String; AExportType: TSupportedExportType; cxGrid: TcxGrid): Boolean;
var
  AProgressDialog: TfmExportProgress;
begin
  AProgressDialog := TfmExportProgress.Create(Application);
  try
    AProgressDialog.Show;
    case AExportType of
      //AExpand - ����������� ��� ������, ���� True ��� ������ �����������, ���� False
      //ASaveAll - ����������� ��� ������, ���� True ��� ������ ����������, ���� False
      //AUseNativeFormat - ���� True, ��� ������� �������� ������������� ������������ ����� ����� � ������ Excel:
      //   TcxCurrencyEdit->Currency, TcxCalcEditProperties->Number, TcxDateEdit->Date, TcxTimeEdit->Time, TcxSpinEdit->Number
      exHTML:
        ExportGridToHTML (afilename, cxGrid, False, True, 'html', AProgressDialog);
      exXML:
        ExportGridToXML  (afilename, cxGrid, False, True, 'xml', AProgressDialog);
      exExcel:
        ExportGridToExcel(afilename, cxGrid, False, True, True, 'xls', AProgressDialog);
      exCSV:
        ExportGridToText (afilename, cxGrid, False, True, ';', '"', '"', 'csv', AProgressDialog, TEncoding.UTF8);
      exText:
        ExportGridToText (afilename, cxGrid, False, True, 'txt', AProgressDialog);
      exText_TAB:
        ExportGridToCSV  (afilename, cxGrid, False, True, #9, 'txt', AProgressDialog, TEncoding.UTF8); //����������� TAB
      exXLSX:
        ExportGridToXLSX (afilename, cxGrid, False, True, True, 'xlsx', AProgressDialog);
    end;
    Result:=FileExists(aFilename);
  finally
    AProgressDialog.Free;
  end;
end;

function Export_SaveGridToFileWithSaveDialog(aInitialDir: string; cxGrid: TcxGrid): Boolean;
var
  AProgressDialog: TfmExportProgress;
  aExportType: TSupportedExportType;
  aFilename: string;
  aFileExt: string;
begin
  Result:=False;
  AProgressDialog := TfmExportProgress.Create(Application);
  try
    AProgressDialog.SaveDialog.InitialDir:=aInitialDir;
    if AProgressDialog.SaveDialog.Execute then begin
      aFilename:=AProgressDialog.SaveDialog.FileName;
      aFileExt:=AnsiLowerCase(ExtractFileExt(aFilename));
      aExportType:=exNone;
      if aFileExt='.xlsx' then aExportType:=exXLSX;
      if aFileExt='.xls' then aExportType:=exExcel;
      if aFileExt='.xml' then aExportType:=exXML;
      if aFileExt='.html' then aExportType:=exHTML;
      if aFileExt='.csv' then aExportType:=exCSV;
      if aFileExt='.txt' then begin
        if AProgressDialog.SaveDialog.FilterIndex=5 then
          aExportType:=exText_TAB
        else
          aExportType:=exText;
      end;
      //���� ��� �� ����������, ������ ������������� ��� �� ���������
      if aExportType=exNone then begin
        aExportType:=exXLSX;
        ChangeFileExt(aFilename, '.xlsx');
      end;
      Result:=Export_SaveGridToFile(aFilename, aExportType, cxGrid);
      if Result then
        Export_OpenFileInExcel(aFilename, aExportType);
    end;
  finally
    AProgressDialog.Free;
  end;
end;

procedure Export_OpenFileInExcel(aFilename: String; AExportType: TSupportedExportType);
var ExcelApp: Variant;
    ArrayData:Variant;
begin
  //���������������� �������
  if AExportType in [exNone, exText, exText_TAB] then Exit;

  ExcelApp := CreateOleObject('Excel.Application');
  ExcelApp.Application.EnableEvents := false;
  ExcelApp.DisplayAlerts := false;
  ExcelApp.Visible := true;
  ArrayData := VarArrayOf([1]);

  case AExportType of
    exHTML: ExcelApp.WorkBooks.Add(aFilename);
    exXML: ExcelApp.Workbooks.OpenXML(aFilename, ArrayData);
    exExcel: ExcelApp.WorkBooks.Add(aFilename);
    exCSV: ExcelApp.WorkBooks.Add(aFilename);
    exText: ;
    exText_TAB: ;
    exXLSX: ExcelApp.WorkBooks.Add(aFilename);
  end;
  ExcelApp.DisplayAlerts := true;
  ExcelApp.Visible := true;
end;

{ TfrmExportProgress }

procedure TfmExportProgress.btnCancelClick(Sender: TObject);
begin
  if MessageDlg(sConfirmMessage, mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    btnCancel.Enabled := False;
end;

procedure TfmExportProgress.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  btnCancel.Click;
  CanClose := not btnCancel.Enabled;
end;

procedure TfmExportProgress.OnProgress(Sender: TObject; Percent: Integer);
begin
  if (Percent mod 1)=0 then begin
    pbProgress.Position := Percent;
    Sleep(10);
    Application.ProcessMessages;
  end;
  if not btnCancel.Enabled then
    Abort;
end;

end.
