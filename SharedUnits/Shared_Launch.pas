unit Shared_Launch;

interface

uses
  Windows, Classes, Forms, ShellApi, SysUtils;

type
  TShowMode = ( smNormal, smMaximized, smMinimized, smHide );

const
  ShowWindowModes: array[ TShowMode ] of Integer =(sw_Normal, sw_ShowMaximized, sw_ShowMinimized, sw_Hide);

type
  TLauncher = class;

  TLaunchThread = class( TThread )
  private
    FLauncher: TLauncher;
  protected
    procedure Execute; override;
  public
    constructor Create( ALauncher: TLauncher );
  end;

  TLaunchErrorEvent = procedure( Sender: TObject; ErrorCode: DWord ) of object;

  TWaitType = ( wtFullStop, wtProcessMessages );

  TLauncher = class( TComponent )
  private
    FHInstance: THandle;
    FAction: string;
    FFileName: string;
    FParameters: string;
    FShowMode: TShowMode;
    FStartDir: string;
    FTimeout: Integer;
    FWaitType: TWaitType;
    FWaitUntilFinished: Boolean;
    FOnFinished: TNotifyEvent;
    FOnTimeout: TNotifyEvent;
    FOnError: TLaunchErrorEvent;
    FExitCode: DWord;
    FLastErrorCode: DWord;
    FHProcess: THandle;
    FRunning: Boolean;
    FBackgroundThread: TLaunchThread;
  protected
    procedure StartProcess;

    procedure Finished; dynamic;
    procedure DoTimeout; dynamic;
    procedure LaunchError; dynamic;

    procedure WaitForProcessAndProcessMsgs; virtual;
    procedure WaitForProcessFullStop; virtual;
    procedure WaitForProcessFromThread; virtual;
  public
    constructor Create( AOwner: TComponent ); override;
    destructor Destroy; override;

    procedure Launch; virtual;
    function Execute: DWord; virtual;

    function GetErrorMsg( ErrorCode: DWord ): string;

    property HInstance: THandle
      read FHInstance;

    property ExitCode: DWord
      read FExitCode;

    property HProcess: THandle
      read FHProcess;

    property Running: Boolean
      read FRunning;
  published
    property Action: string
      read FAction
      write FAction;

    property FileName: string
      read FFileName
      write FFileName;

    property Parameters: string
      read FParameters
      write FParameters;

    property ShowMode: TShowMode
      read FShowMode
      write FShowMode
      default smNormal;

    property StartDir: string
      read FStartDir
      write FStartDir;

    property Timeout: Integer
      read FTimeout
      write FTimeout;

    property WaitType: TWaitType
      read FWaitType
      write FWaitType
      default wtFullStop;

    property WaitUntilFinished: Boolean
      read FWaitUntilFinished
      write FWaitUntilFinished
      default False;

    property OnFinished: TNotifyEvent
      read FOnFinished
      write FOnFinished;

    property OnError: TLaunchErrorEvent
      read FOnError
      write FOnError;

    property OnTimeout: TNotifyEvent
      read FOnTimeout
      write FOnTimeout;
  end;

// ��������� ��� ������� ������� ���������
procedure ExecuteProgramm(const FileName, Params: WideString; Wait_Flag: WordBool);

implementation

resourcestring
  sLaunchOutOfMemory            = '�� ������� ������ ��� ����������� ���� ���������'; //'Out of memory or executable file is corrupt'
  sLaunchFileNotFound           = '���� �� ������'; //'File was not found'
  sLaunchPathNotFound           = '���� �� ������'; //'Path was not found'
  sLaunchSharingViolation       = '��������� ������ ��� ������ ����'; //'Sharing violation or netword error'
  sLaunchSeparateDataSeg        = '���������� ����������� ��������� ��������� ������ ��� ������ ������'; //'A library required separate data segments for each task'
  sLaunchInsufficientMemory     = '������������ ������ ��� ������� ����������'; //'Insufficient memory to start application'
  sLaunchIncorrectWindowsVer    = '������������ ������ Windows'; //'Incorrect version of Windows'
  sLaunchInvalidEXE             = '���� �� �������� ����������� Windows ��� ��������� ������ � .EXE �����'; //'File is not a Windows application or there was an error in the .EXE image'
  sLaunchIncorrectOS            = '���������� ���� ����������� ��� ������ ������������ �������'; //'Application was designed for a different operating system'
  sLaunchForMSDos4              = '���������� ���� ����������� MS-DOS 4.0'; //'Application was designed for MS-DOS 4.0'
  sLaunchUnknownType            = '����������� ��� ������������ �����'; //'Unknown executable file type'
  sLaunchLoadRealMode           = 'Cannot load a real-mode application';
  sLaunchNonReadOnlyDataSeg     = 'Cannot load a second instance of an executable file containing multiple, non-read-only data segments';
  sLaunchCompressedEXE          = '�� ������� ��������� ������ ����������� ����'; //'Cannot load a compressed executable file'
  sLaunchInvalidDLL             = 'A dynamic-link library (DLL) file is invalid';
  sLaunchWin32                  = '���������� ������� Windows 32-������� ����������'; //'Application requires Windows 32-bit extensions'
  sLaunchNoAssociation          = 'No association for specified file type';

// ��������� ��� ������� ������� ���������
procedure ExecuteProgramm(const FileName, Params: WideString; Wait_Flag: WordBool);
var
  Launcher: TLauncher;
begin
  Launcher := TLauncher.Create(nil);
  Launcher.StartDir := ExtractFileDir(FileName);
  Launcher.FileName := FileName;
  Launcher.Parameters := Params;
  Launcher.WaitUntilFinished := Wait_Flag;
  Launcher.Launch;
  Launcher.Free;
end;


{============================}
{= TLaunchThread Methods ==}
{============================}

constructor TLaunchThread.Create( ALauncher: TLauncher );
begin
  inherited Create( False );
  FLauncher := ALauncher;
  FreeOnTerminate := True;
end;


procedure TLaunchThread.Execute;
begin
  if FLauncher <> nil then
    FLauncher.StartProcess;
end;


{=========================}
{== TLauncher Methods ==}
{=========================}

constructor TLauncher.Create( AOwner: TComponent );
begin
  inherited;
  FShowMode := smNormal;
  FHInstance := 0;
  FAction := 'Open';
  {&RCI}
  FHProcess := 0;
  FExitCode := 0;
  FTimeout := -1{INFINITE};

  FRunning := False;
  FWaitType := wtFullStop;
  FWaitUntilFinished := False;
  {&RV}
end;


destructor TLauncher.Destroy;
begin
  if FRunning and not FWaitUntilFinished and ( FBackgroundThread <> nil ) and not FBackgroundThread.Terminated then
  begin
    FBackgroundThread.Terminate;
    Sleep( 200 );
  end;

  inherited;
end;


procedure TLauncher.Finished;
begin
  if Assigned( FOnFinished ) then
    FOnFinished( Self );
end;


function TLauncher.GetErrorMsg( ErrorCode: DWord ): string;
begin
  case ErrorCode of
    0: Result := sLaunchOutOfMemory;
    2: Result := sLaunchFileNotFound;
    3: Result := sLaunchPathNotFound;
    5: Result := sLaunchSharingViolation;
    6: Result := sLaunchSeparateDataSeg;
    8: Result := sLaunchInsufficientMemory;
    10: Result := sLaunchIncorrectWindowsVer;
    11: Result := sLaunchInvalidEXE;
    12: Result := sLaunchIncorrectOS;
    13: Result := sLaunchForMSDos4;
    14: Result := sLaunchUnknownType;
    15: Result := sLaunchLoadRealMode;
    16: Result := sLaunchNonReadOnlyDataSeg;
    19: Result := sLaunchCompressedEXE;
    20: Result := sLaunchInvalidDLL;
    21: Result := sLaunchWin32;
    31: Result := sLaunchNoAssociation;
    else
      Result := 'Unknown Error';
  end;
end;


procedure TLauncher.LaunchError;
begin
  if Assigned( FOnError ) then
    FOnError( Self, FLastErrorCode );
end;


procedure TLauncher.DoTimeout;
begin
  if Assigned( FOnTimeout ) then
    FOnTimeout( Self );
end;


procedure TLauncher.WaitForProcessAndProcessMsgs;
begin
  repeat
    case MsgWaitForMultipleObjects( 1, FHProcess, False, Cardinal( FTimeout ),
                                    QS_POSTMESSAGE or QS_SENDMESSAGE or QS_ALLPOSTMESSAGE ) of
      WAIT_OBJECT_0:
      begin
        GetExitCodeProcess( FHProcess, FExitCode );
        Finished;
        Break;
      end;

      WAIT_OBJECT_0 + 1:
      begin
//        Application.ProcessMessages;
      end;

      WAIT_TIMEOUT:
      begin
        DoTimeout;
        Break;
      end;
    end;

  until False;
end;


procedure TLauncher.WaitForProcessFullStop;
begin
  case WaitForSingleObject( FHProcess, Cardinal( FTimeout ) ) of
    WAIT_FAILED:
    begin
      FLastErrorCode := GetLastError;
      LaunchError;
    end;

    WAIT_OBJECT_0:
    begin
      GetExitCodeProcess( FHProcess, FExitCode );
      Finished;
    end;

    WAIT_TIMEOUT:
      DoTimeout;
  end;
end;


procedure TLauncher.WaitForProcessFromThread;
var
  Done: Boolean;
  TimeoutCount: Cardinal;
begin
  Done := False;
  TimeoutCount := 0;
  repeat
    case WaitForSingleObject( FHProcess, Cardinal( 100 ) ) of
      WAIT_FAILED:
      begin
        FLastErrorCode := GetLastError;
        FBackgroundThread.Synchronize( LaunchError );
        Done := True;
      end;

      WAIT_OBJECT_0:
      begin
        GetExitCodeProcess( FHProcess, FExitCode );
        FBackgroundThread.Synchronize( Finished );
        Done := True;
      end;

      WAIT_TIMEOUT:
      begin
        Inc( TimeoutCount, 100 );
        if TimeoutCount >= Cardinal( FTimeout ) then
        begin
          FBackgroundThread.Synchronize( DoTimeout );
          Done := True;
        end;
      end;
    end; { case }
  until Done or FBackgroundThread.Terminated;
end;


procedure TLauncher.StartProcess;
var
  ShellInfo: TShellExecuteInfo;
begin
  FLastErrorCode := 0;
  FHInstance := 0;
  FHProcess := 0;
  FExitCode := 0;

  FillChar( ShellInfo, SizeOf( TShellExecuteInfo ), 0 );
  ShellInfo.cbSize := SizeOf( TShellExecuteInfo );
  ShellInfo.fMask := SEE_MASK_NOCLOSEPROCESS or SEE_MASK_FLAG_NO_UI or SEE_MASK_FLAG_DDEWAIT;
  ShellInfo.Wnd := HWnd_Desktop;
  ShellInfo.lpVerb := PChar( FAction );
  ShellInfo.lpFile := PChar( FFileName );
  ShellInfo.lpParameters := PChar( FParameters );
  ShellInfo.lpDirectory := PChar( FStartDir );
  ShellInfo.nShow := ShowWindowModes[ FShowMode ];

  if ShellExecuteEx( @ShellInfo ) then
  begin
    FHInstance := ShellInfo.hInstApp;
    FHProcess := ShellInfo.hProcess;
    FRunning := True;

    try
      if FWaitUntilFinished then
      begin
        if FWaitType = wtFullStop then
          WaitForProcessFullStop
        else
          WaitForProcessAndProcessMsgs;
      end
      else
        WaitForProcessFromThread;
    finally
      CloseHandle( FHProcess );
      FRunning := False;
    end;
  end
  else
  begin
    FLastErrorCode := ShellInfo.hInstApp;
    if FWaitUntilFinished then
      LaunchError
    else
      FBackgroundThread.Synchronize( LaunchError );
  end;
end; {= TLauncher.StartProcess =}



procedure TLauncher.Launch;
begin
  if not FRunning and ( FFileName <> '' ) then
  begin
    if FWaitUntilFinished then
      StartProcess
    else
    begin
      FBackgroundThread := TLaunchThread.Create( Self );
      repeat
        Sleep( 10 );
      until FRunning or ( FLastErrorCode <> 0 );
    end;
  end;
end;


function TLauncher.Execute: DWord;
var
  ShellInfo: TShellExecuteInfo;
begin
  if FFileName = '' then
  begin
    Result := 0;
    Exit;
  end;

  FHInstance := 0;
  FHProcess := 0;
  FExitCode := 0;

  FillChar( ShellInfo, SizeOf( TShellExecuteInfo ), 0 );
  ShellInfo.cbSize := SizeOf( TShellExecuteInfo );
  ShellInfo.fMask := SEE_MASK_NOCLOSEPROCESS or SEE_MASK_FLAG_NO_UI or SEE_MASK_FLAG_DDEWAIT;
  ShellInfo.Wnd := HWnd_Desktop;
  ShellInfo.lpVerb := PChar( FAction );
  ShellInfo.lpFile := PChar( FFileName );
  ShellInfo.lpParameters := PChar( FParameters );
  ShellInfo.lpDirectory := PChar( FStartDir );
  ShellInfo.nShow := ShowWindowModes[ FShowMode ];

  if ShellExecuteEx( @ShellInfo ) then
    Result := 0
  else
    Result := ShellInfo.hInstApp;
end;


end.
