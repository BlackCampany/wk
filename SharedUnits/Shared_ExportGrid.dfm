object fmExportProgress: TfmExportProgress
  Left = 408
  Top = 291
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = #1069#1082#1089#1087#1086#1088#1090'...'
  ClientHeight = 86
  ClientWidth = 441
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  Padding.Left = 3
  Padding.Top = 3
  Padding.Right = 3
  Padding.Bottom = 3
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCloseQuery = FormCloseQuery
  DesignSize = (
    441
    86)
  PixelsPerInch = 96
  TextHeight = 13
  object pbProgress: TcxProgressBar
    AlignWithMargins = True
    Left = 6
    Top = 26
    Align = alTop
    Properties.PeakValue = 100.000000000000000000
    TabOrder = 0
    Width = 429
  end
  object lbTitle: TcxLabel
    AlignWithMargins = True
    Left = 6
    Top = 6
    Margins.Bottom = 0
    Align = alTop
    Caption = #1055#1086#1078#1072#1083#1091#1081#1089#1090#1072', '#1087#1086#1076#1086#1078#1076#1080#1090#1077', '#1087#1086#1082#1072' '#1076#1072#1085#1085#1099#1077' '#1101#1082#1089#1087#1086#1088#1090#1080#1088#1091#1102#1090#1089#1103'...'
    Transparent = True
  end
  object btnCancel: TcxButton
    Left = 175
    Top = 55
    Width = 91
    Height = 25
    Anchors = [akBottom]
    Cancel = True
    Caption = '&'#1054#1090#1084#1077#1085#1072
    TabOrder = 2
    OnClick = btnCancelClick
  end
  object SaveDialog: TSaveDialog
    DefaultExt = 'xlsx'
    Filter = 
      #1050#1085#1080#1075#1072' Excel (*.xlsx)|*.xlsx|'#1050#1085#1080#1075#1072' Excel 97-2003 (*.xls)|*.xls|XM' +
      'L-'#1076#1072#1085#1085#1099#1077' (*.xml)|*.xml|'#1042#1077#1073'-'#1089#1090#1088#1072#1085#1080#1094#1072' (*.html)|*.html|'#1058#1077#1082#1089#1090' '#1089' '#1090#1072#1073#1091 +
      #1083#1103#1094#1080#1077#1081' (*.txt)|*.txt|'#1058#1077#1082#1089#1090' '#1070#1085#1080#1082#1086#1076' (*.txt)|*.txt|CSV '#1092#1072#1081#1083' (*.csv)' +
      '|*.csv'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofPathMustExist, ofEnableSizing]
    Left = 61
    Top = 36
  end
end
