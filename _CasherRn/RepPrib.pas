unit RepPrib;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxTextEdit, cxImageComboBox,
  DBClient, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxControls, cxGridCustomView, cxGrid,
  cxContainer, cxMemo, ExtCtrls, SpeedBar, ComCtrls, Placemnt, FR_DSet,
  FR_DBSet, FR_Class, FIBDataSet, pFIBDataSet, cxProgressBar, dxPSGlbl,
  dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon, dxPScxGridLnk,
  Menus, cxDBLookupComboBox, dxmdaset;

type
  TfmRepPrib = class(TForm)
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    Panel1: TPanel;
    Memo1: TcxMemo;
    GridPrib: TcxGrid;
    ViewPrib: TcxGridDBTableView;
    LevelPrib: TcxGridLevel;
    taPrib1: TClientDataSet;
    dsPrib: TDataSource;
    taPrib1CodeB: TIntegerField;
    taPrib1Name: TStringField;
    taPrib1Parent: TIntegerField;
    taPrib1Quant: TFloatField;
    taPrib1SumOut: TFloatField;
    taPrib1SumIn: TFloatField;
    taPrib1PriceIn: TFloatField;
    taPrib1PriceOut: TFloatField;
    taPrib1Pr1: TFloatField;
    taPrib1Pr2: TFloatField;
    taPrib1Pr3: TFloatField;
    taPrib1Diff: TFloatField;
    taPrib1DiffSum: TFloatField;
    ViewPribCodeB: TcxGridDBColumn;
    ViewPribName: TcxGridDBColumn;
    ViewPribParent: TcxGridDBColumn;
    ViewPribQuant: TcxGridDBColumn;
    ViewPribSumOut: TcxGridDBColumn;
    ViewPribSumIn: TcxGridDBColumn;
    ViewPribPriceIn: TcxGridDBColumn;
    ViewPribPriceOut: TcxGridDBColumn;
    ViewPribPr1: TcxGridDBColumn;
    ViewPribPr2: TcxGridDBColumn;
    ViewPribPr3: TcxGridDBColumn;
    ViewPribDiff: TcxGridDBColumn;
    ViewPribDiffSum: TcxGridDBColumn;
    FormPlacement1: TFormPlacement;
    frRepPrib: TfrReport;
    frtaPrib: TfrDBDataSet;
    SpeedItem4: TSpeedItem;
    taPrib1PrNac: TFloatField;
    ViewPribPrNac: TcxGridDBColumn;
    taPrib1NameGr1: TStringField;
    taPrib1NameGr2: TStringField;
    ViewPribNameGr1: TcxGridDBColumn;
    ViewPribNameGr2: TcxGridDBColumn;
    taPrib1Oper: TSmallintField;
    ViewPribOper: TcxGridDBColumn;
    quSelPrib: TpFIBDataSet;
    quSelPribGr: TpFIBDataSet;
    quSelPribGrNAMECL: TFIBStringField;
    quSelPribGrPARENT: TFIBIntegerField;
    quSelPribGrQUANT: TFIBFloatField;
    quSelPribGrSUMOUT: TFIBFloatField;
    quSelPribGrSUMIN: TFIBFloatField;
    PBar1: TcxProgressBar;
    PrintPrib: TdxComponentPrinter;
    PrintPribLink1: TdxGridReportLink;
    SpeedItem5: TSpeedItem;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    SpeedItem6: TSpeedItem;
    quSelPribPARENT: TFIBIntegerField;
    quSelPribCODEB: TFIBIntegerField;
    quSelPribNAME: TFIBStringField;
    quSelPribQUANT: TFIBFloatField;
    quSelPribSUMOUT: TFIBFloatField;
    quSelPribSUMIN: TFIBFloatField;
    taPrib1SaleMH: TIntegerField;
    ViewPribSaleMH: TcxGridDBColumn;
    quSelPribIDCLI: TFIBIntegerField;
    ViewPribCategory: TcxGridDBColumn;
    quSelPribSSALET: TFIBStringField;
    taPrib1SSALET: TStringField;
    ViewPribSSALET: TcxGridDBColumn;
    quSelPribNAMECAT: TFIBStringField;
    taPrib1SCategory: TStringField;
    quSelPribIDSKL: TFIBIntegerField;
    taPrib1IdSkl: TIntegerField;
    teMH: TdxMemData;
    teMHID: TIntegerField;
    teMHNAMEMH: TStringField;
    taPrib1NameMH: TStringField;
    ViewPribNameMH: TcxGridDBColumn;
    taPrib: TdxMemData;
    taPribCodeB: TIntegerField;
    taPribName: TStringField;
    taPribParent: TIntegerField;
    taPribQuant: TFloatField;
    taPribSumOut: TFloatField;
    taPribSumIn: TFloatField;
    taPribPriceIn: TFloatField;
    taPribPriceOut: TFloatField;
    taPribPr1: TFloatField;
    taPribPr2: TFloatField;
    taPribPr3: TFloatField;
    taPribDiff: TFloatField;
    taPribDiffSum: TFloatField;
    taPribNameGr1: TStringField;
    taPribNameGr2: TStringField;
    taPribOper: TSmallintField;
    taPribSaleMH: TIntegerField;
    taPribSSALET: TStringField;
    taPribSCategory: TStringField;
    taPribIdSkl: TIntegerField;
    taPribNameMH: TStringField;
    taPribPrNac: TFloatField;
    taPribCliTo: TStringField;
    ViewPribCliTo: TcxGridDBColumn;
    taPribSum1: TFloatField;
    ViewPribSum1: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
    procedure SpeedItem4Click(Sender: TObject);
    procedure taPrib1CalcFields(DataSet: TDataSet);
    procedure SpeedItem5Click(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure SpeedItem6Click(Sender: TObject);
    procedure ViewPribDataControllerSummaryAfterSummary(
      ASender: TcxDataSummary);
    procedure taPribCalcFields(DataSet: TDataSet);
    procedure ViewPribDataControllerGroupingChanged(Sender: TObject);
    procedure ViewPribDataControllerDetailCollapsed(
      ADataController: TcxCustomDataController; ARecordIndex: Integer);
    procedure ViewPribDataControllerDetailExpanding(
      ADataController: TcxCustomDataController; ARecordIndex: Integer;
      var AAllow: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure  prFormPrib;
  end;

var
  fmRepPrib: TfmRepPrib;

implementation

uses Un1, SelPerSkl, dmOffice, DMOReps, SummaryReal, SelPerSkl2;

{$R *.dfm}

procedure  TfmRepPrib.prFormPrib;
Var rSumOut,rSumMarg:Real;
    iGr:Integer;
    S1,S2:String;
    iC,iB:Integer;
    sSkl,sMProd,sCat,sSalet:String;
    sSklg,sMProdg,sCatg,sSaletg:String;
    S3,S4,S5,S6,s7,s8,s9,s10:String;
    sCli,sCliG:String; 
begin
  with dmO do
  with dmORep do
  begin
    fmRepPrib.Memo1.Clear;
    fmRepPrib.Memo1.Lines.Add('����� .. ���� ������������ ������.'); delay(10);

    PBar1.Position:=0;
    PBar1.Visible:=True;


    ViewPrib.BeginUpdate;

    CloseTe(taPrib);

    if (Prib.Oper=1)or(Prib.OperAll) then
    begin
      fmRepPrib.Memo1.Lines.Add('  ���������� �� ������.'); delay(10);

      if Prib.MHUnion then begin sSkl:='0 as IDSKL'; sSklg:=''; end else begin sSkl:='vr.IDSKL'; sSklg:=',vr.IDSKL'; end;
      if Prib.MProdUnion then begin sMProd:='0 as IDCLI'; sMProdg:=''; end else begin sMProd:='vr.IDCLI'; sMProdg:=',vr.IDCLI'; end;
      if Prib.CatUnion then begin sCat:=''''' as NAMECAT'; sCatg:=''; end else begin sCat:='cat.NAMECAT'; sCatg:=',cat.NAMECAT'; end;
      if Prib.SaletUnion then begin sSalet:=''''' as SSALET'; sSaletg:=''; end else begin sSalet:='vr.SSALET'; sSaletg:=',vr.SSALET'; end;

      quSelPrib.Active:=False;
      quSelPrib.SelectSQL.Clear;
      quSelPrib.SelectSQL.Add('SELECT ca.PARENT,'+sSkl+','+sMProd+',vr.CODEB,ca.NAME,'+sCat+','+sSalet+',SUM(vr.QUANT) as QUANT ,SUM(vr.SUMOUT)as SUMOUT,SUM(vr.SUMIN)as SUMIN');
      quSelPrib.SelectSQL.Add('FROM OF_VREALB vr');
      quSelPrib.SelectSQL.Add('left join OF_CARDS ca on ca.ID=vr.CODEB');
      quSelPrib.SelectSQL.Add('left join OF_CARDSRCATEGORY cat on cat.ID=ca.RCATEGORY');
      quSelPrib.SelectSQL.Add('WHERE vr.DATEDOC>='''+FormatDateTime(sFormatDate,CommonSet.DateFrom)+''' and vr.DATEDOC<'''+FormatDateTime(sFormatDate,CommonSet.DateTo)+'''');

//      if Prib.MHAll=False then quSelPrib.SelectSQL.Add('and vr.IDSKL='+IntToStr(Prib.MHId));
      if Prib.MHAll=False then quSelPrib.SelectSQL.Add('and vr.IDSKL in ('+Prib.sMHId+')');
      if Prib.MProdAll=False then quSelPrib.SelectSQL.Add('and vr.IDCLI='+IntToStr(Prib.MProd));
      if Prib.CatAll=False then quSelPrib.SelectSQL.Add('and ca.RCATEGORY='+IntToStr(Prib.Cat));
      if Prib.SaletAll=False then quSelPrib.SelectSQL.Add('and vr.SSALET like ''%'+Prib.Salet+'%''');

      quSelPrib.SelectSQL.Add('GROUP by ca.PARENT'+sSklg+sMProdg+',vr.CODEB,ca.NAME'+sCatg+sSaletg);
      quSelPrib.Active:=True;

      fmRepPrib.Memo1.Lines.Add('  ���������� �� �������.'); delay(10);

{      quSelPribGr.Active:=False;
      quSelPribGr.ParamByName('DATEB').AsDateTime:=CommonSet.DateFrom;
      quSelPribGr.ParamByName('DATEE').AsDateTime:=CommonSet.DateTo;
      quSelPribGr.ParamByName('IDSTORE').AsInteger:=CommonSet.IdStore;
      quSelPribGr.Active:=True;
}
      quSelPribGr.Active:=False;
      quSelPribGr.SelectSQL.Clear;
      quSelPribGr.SelectSQL.Add('SELECT cl.NAMECL,ca.PARENT,');
      quSelPribGr.SelectSQL.Add('SUM(vr.QUANT) as QUANT ,SUM(vr.SUMOUT)as SUMOUT,SUM(vr.SUMIN)as SUMIN');
      quSelPribGr.SelectSQL.Add('FROM OF_VREALB vr');
      quSelPribGr.SelectSQL.Add('left join OF_CARDS ca on ca.ID=vr.CODEB');
      quSelPribGr.SelectSQL.Add('left join of_classif cl on cl.ID=ca.PARENT');
      quSelPribGr.SelectSQL.Add('WHERE vr.DATEDOC>='''+ds1(CommonSet.DateFrom)+''' and vr.DATEDOC<'''+ds1(CommonSet.DateTo)+'''');
      if Prib.MHAll=False then quSelPribGr.SelectSQL.Add('and vr.IDSKL in ('+Prib.sMHId+')');
      quSelPribGr.SelectSQL.Add('GROUP by cl.NAMECL,ca.PARENT');
      quSelPribGr.Active:=True;

      iB:=Trunc(quSelPrib.RecordCount/80*10);
      if iB<1 then iB:=1;
      iC:=0;

      fmRepPrib.Memo1.Lines.Add('  ��������� ������.'); delay(10);

      fmRepPrib.Memo1.Lines.Add('  �������.'); delay(10);

      quSelPrib.First;
      while not quSelPrib.Eof do
      begin

        prFind2group(quSelPribPARENT.AsInteger,S1,S2);

        taPrib.Append;
        taPribCodeB.AsInteger:=quSelPribCODEB.AsInteger;
        taPribName.AsString:=quSelPribNAME.AsString;
        taPribParent.AsInteger:=quSelPribPARENT.AsInteger;
        taPribNameGr1.AsString:=S1;
        taPribNameGr2.AsString:=S2;
        taPribOper.AsInteger:=1; //�������
        taPribQuant.AsFloat:=quSelPribQUANT.AsFloat;
        taPribSumOut.AsFloat:=quSelPribSUMOUT.AsFloat;
        taPribSumIn.AsFloat:=quSelPribSUMIN.AsFloat;
        taPribPriceIn.AsFloat:=0;
        taPribPriceOut.AsFloat:=0;
        taPribPr1.AsFloat:=0;
        taPribPr2.AsFloat:=0;
        taPribPr3.AsFloat:=0;
        taPribDiff.AsFloat:=0;
        taPribDiffSum.AsFloat:=quSelPribSUMOUT.AsFloat-quSelPribSUMIN.AsFloat;
        taPribSaleMH.AsInteger:=quSelPribIDCLI.AsInteger;
        taPribSCategory.AsString:=quSelPribNAMECAT.AsString;
        taPribSSALET.AsString:=quSelPribSSALET.AsString;

        if abs(quSelPribQUANT.AsFloat)>0 then
        begin
          taPribPriceIn.AsFloat:=RoundEx(quSelPribSUMIN.AsFloat/quSelPribQUANT.AsFloat*100)/100;
          taPribPriceOut.AsFloat:=RoundEx(quSelPribSUMOUT.AsFloat/quSelPribQUANT.AsFloat*100)/100;
          taPribDiff.AsFloat:=RoundEx(quSelPribSUMOUT.AsFloat/quSelPribQUANT.AsFloat*100)/100-RoundEx(quSelPribSUMIN.AsFloat/quSelPribQUANT.AsFloat*100)/100;
        end;
        if abs(quSelPribSUMOUT.AsFloat)>0 then
        begin
          taPribPr2.AsFloat:=RoundEx(quSelPribSUMIN.AsFloat/quSelPribSUMOUT.AsFloat*1000)/10;
        end;

        iGr:=quSelPribPARENT.AsInteger;
        if quSelPribGr.Locate('PARENT',iGr,[]) then
        begin
          rSumOut:=quSelPribGrSUMOUT.AsFloat;
          rSumMarg:=quSelPribGrSUMOUT.AsFloat-quSelPribGrSUMIN.AsFloat;
          if rSumOut<>0 then
          begin
            taPribPr1.AsFloat:=RoundEx(quSelPribSUMOUT.AsFloat/rSumOut*1000)/10;
          end;
          if rSumMarg<>0 then
          begin
            taPribPr3.AsFloat:=RoundEx((quSelPribSUMOUT.AsFloat-quSelPribSUMIN.AsFloat)/rSumMarg*1000)/10;
          end;
        end;

        taPribIdSkl.AsInteger:=quSelPribIdSkl.AsInteger;
        taPribCliTo.AsString:='';

        taPrib.Post;

        inc(iC); if iC mod iB = 0 then begin PBar1.Position:= iC div iB; Delay(10); end;

        quSelPrib.Next;
      end;
      quSelPrib.Active:=False;
      quSelPribGr.Active:=False;
    end;

    PBar1.Position:=80; Delay(10);

    if (Prib.Oper=2)or(Prib.OperAll) then
    begin

      fmRepPrib.Memo1.Lines.Add('  ��������.'); delay(10);

      if Prib.MHUnion then begin sSkl:='0 as IDSKL'; sSklg:=''; end else begin sSkl:='vr.IDSKL'; sSklg:=',vr.IDSKL'; end;
      if Prib.CatUnion then begin sCat:=''''' as NAMECAT'; sCatg:=''; end else begin sCat:='cat.NAMECAT'; sCatg:=',cat.NAMECAT'; end;

      quSelPrib.Active:=False;
      quSelPrib.SelectSQL.Clear;
      quSelPrib.SelectSQL.Add('SELECT ca.PARENT,'+sSkl+',0 as IDCLI,vr.CODEB,ca.NAME,'+sCat+','''' as SSALET,SUM(vr.QUANT) as QUANT ,SUM(vr.SUMOUT)as SUMOUT,SUM(vr.SUMIN)as SUMIN');
      quSelPrib.SelectSQL.Add('FROM OF_VREALB2 vr');
      quSelPrib.SelectSQL.Add('left join OF_CARDS ca on ca.ID=vr.CODEB');
      quSelPrib.SelectSQL.Add('left join OF_CARDSRCATEGORY cat on cat.ID=ca.RCATEGORY');
      quSelPrib.SelectSQL.Add('WHERE vr.DATEDOC>='''+FormatDateTime(sFormatDate,CommonSet.DateFrom)+''' and vr.DATEDOC<'''+FormatDateTime(sFormatDate,CommonSet.DateTo)+'''');

      if Prib.MHAll=False then quSelPrib.SelectSQL.Add('and vr.IDSKL in ('+Prib.sMHId+')');
      if Prib.CatAll=False then quSelPrib.SelectSQL.Add('and ca.RCATEGORY='+IntToStr(Prib.Cat));

      quSelPrib.SelectSQL.Add('GROUP by ca.PARENT'+sSklg+',vr.CODEB,ca.NAME'+sCatg);
      quSelPrib.Active:=True;

      if taPrib.Active=False then CloseTe(taPrib);

      quSelPrib.First;
      while not quSelPrib.Eof do
      begin

        prFind2group(quSelPribPARENT.AsInteger,S1,S2);

        taPrib.Append;
        taPribCodeB.AsInteger:=quSelPribCODEB.AsInteger;
        taPribName.AsString:=quSelPribNAME.AsString;
        taPribParent.AsInteger:=quSelPribPARENT.AsInteger;
        taPribNameGr1.AsString:=S1;
        taPribNameGr2.AsString:=S2;
        taPribOper.AsInteger:=2;
        taPribQuant.AsFloat:=quSelPribQUANT.AsFloat;
        taPribSumOut.AsFloat:=quSelPribSUMOUT.AsFloat;
        taPribSumIn.AsFloat:=quSelPribSUMIN.AsFloat;
        taPribPriceIn.AsFloat:=0;
        taPribPriceOut.AsFloat:=0;
        taPribPr1.AsFloat:=0;
        taPribPr2.AsFloat:=0;
        taPribPr3.AsFloat:=0;
        taPribDiff.AsFloat:=0;
        taPribDiffSum.AsFloat:=quSelPribSUMOUT.AsFloat-quSelPribSUMIN.AsFloat;
        taPribSaleMH.AsInteger:=quSelPribIDCLI.AsInteger;
        taPribSCategory.AsString:=quSelPribNAMECAT.AsString;
        taPribSSALET.AsString:=quSelPribSSALET.AsString;

        if abs(quSelPribQUANT.AsFloat)>0 then
        begin
          taPribPriceIn.AsFloat:=RoundEx(quSelPribSUMIN.AsFloat/quSelPribQUANT.AsFloat*100)/100;
          taPribPriceOut.AsFloat:=RoundEx(quSelPribSUMOUT.AsFloat/quSelPribQUANT.AsFloat*100)/100;
          taPribDiff.AsFloat:=RoundEx(quSelPribSUMOUT.AsFloat/quSelPribQUANT.AsFloat*100)/100-RoundEx(quSelPribSUMIN.AsFloat/quSelPribQUANT.AsFloat*100)/100;
        end;
        if abs(quSelPribSUMOUT.AsFloat)>0 then
        begin
          taPribPr2.AsFloat:=RoundEx(quSelPribSUMIN.AsFloat/quSelPribSUMOUT.AsFloat*1000)/10;
        end;

        taPribIdSkl.AsInteger:=quSelPribIdSkl.AsInteger;
        taPribCliTo.AsString:='';
        
        taPrib.Post;

        quSelPrib.Next;
      end;
      quSelPrib.Active:=False;
    end;

    PBar1.Position:=85; Delay(10);

    if (Prib.Oper=3)or(Prib.OperAll) then
    begin
      fmRepPrib.Memo1.Lines.Add('  ������� �������.'); delay(10);

      if Prib.MHUnion then begin sSkl:='0 as IDSKL'; sSklg:=''; end else begin sSkl:='vr.IDSKL'; sSklg:=',vr.IDSKL'; end;
      if Prib.CatUnion then begin sCat:=''''' as NAMECAT'; sCatg:=''; end else begin sCat:='cat.NAMECAT'; sCatg:=',cat.NAMECAT'; end;

      quSelPrib.Active:=False;
      quSelPrib.SelectSQL.Clear;
      quSelPrib.SelectSQL.Add('SELECT ca.PARENT,'+sSkl+',0 as IDCLI,vr.CODEB,ca.NAME,'+sCat+','''' as SSALET,SUM(vr.QUANT) as QUANT ,SUM(vr.SUMOUT)as SUMOUT,SUM(vr.SUMIN)as SUMIN');
      quSelPrib.SelectSQL.Add('FROM OF_VREALB3 vr');
      quSelPrib.SelectSQL.Add('left join OF_CARDS ca on ca.ID=vr.CODEB');
      quSelPrib.SelectSQL.Add('left join OF_CARDSRCATEGORY cat on cat.ID=ca.RCATEGORY');
      quSelPrib.SelectSQL.Add('WHERE vr.DATEDOC>='''+FormatDateTime(sFormatDate,CommonSet.DateFrom)+''' and vr.DATEDOC<'''+FormatDateTime(sFormatDate,CommonSet.DateTo)+'''');

      if Prib.MHAll=False then quSelPrib.SelectSQL.Add('and vr.IDSKL in ('+Prib.sMHId+')');
      if Prib.CatAll=False then quSelPrib.SelectSQL.Add('and ca.RCATEGORY='+IntToStr(Prib.Cat));

      quSelPrib.SelectSQL.Add('GROUP by ca.PARENT'+sSklg+',vr.CODEB,ca.NAME'+sCatg);
      quSelPrib.Active:=True;

      if taPrib.Active=False then CloseTe(taPrib);

      quSelPrib.First;
      while not quSelPrib.Eof do
      begin

        prFind2group(quSelPribPARENT.AsInteger,S1,S2);

        taPrib.Append;
        taPribCodeB.AsInteger:=quSelPribCODEB.AsInteger;
        taPribName.AsString:=quSelPribNAME.AsString;
        taPribParent.AsInteger:=quSelPribPARENT.AsInteger;
        taPribNameGr1.AsString:=S1;
        taPribNameGr2.AsString:=S2;
        taPribOper.AsInteger:=3;
        taPribQuant.AsFloat:=quSelPribQUANT.AsFloat;
        taPribSumOut.AsFloat:=quSelPribSUMOUT.AsFloat;
        taPribSumIn.AsFloat:=quSelPribSUMIN.AsFloat;
        taPribPriceIn.AsFloat:=0;
        taPribPriceOut.AsFloat:=0;
        taPribPr1.AsFloat:=0;
        taPribPr2.AsFloat:=0;
        taPribPr3.AsFloat:=0;
        taPribDiff.AsFloat:=0;
        taPribDiffSum.AsFloat:=quSelPribSUMOUT.AsFloat-quSelPribSUMIN.AsFloat;
        taPribSaleMH.AsInteger:=quSelPribIDCLI.AsInteger;
        taPribSCategory.AsString:=quSelPribNAMECAT.AsString;
        taPribSSALET.AsString:=quSelPribSSALET.AsString;

        if abs(quSelPribQUANT.AsFloat)>0 then
        begin
          taPribPriceIn.AsFloat:=RoundEx(quSelPribSUMIN.AsFloat/quSelPribQUANT.AsFloat*100)/100;
          taPribPriceOut.AsFloat:=RoundEx(quSelPribSUMOUT.AsFloat/quSelPribQUANT.AsFloat*100)/100;
          taPribDiff.AsFloat:=RoundEx(quSelPribSUMOUT.AsFloat/quSelPribQUANT.AsFloat*100)/100-RoundEx(quSelPribSUMIN.AsFloat/quSelPribQUANT.AsFloat*100)/100;
        end;
        if abs(quSelPribSUMOUT.AsFloat)>0 then
        begin
          taPribPr2.AsFloat:=RoundEx(quSelPribSUMIN.AsFloat/quSelPribSUMOUT.AsFloat*1000)/10;
        end;

        taPribIdSkl.AsInteger:=quSelPribIdSkl.AsInteger;
        taPribCliTo.AsString:='';
        taPrib.Post;

        quSelPrib.Next;
      end;
      quSelPrib.Active:=False;
    end;

    PBar1.Position:=90; Delay(10);

    if (Prib.Oper=4)or(Prib.OperAll) then
    begin
      fmRepPrib.Memo1.Lines.Add('  ������ ������.'); delay(10);

      if Prib.MHUnion then begin sSkl:='0 as IDSKL'; sSklg:=''; end else begin sSkl:='vr.IDSKL'; sSklg:=',vr.IDSKL'; end;
      if Prib.CatUnion then begin sCat:=''''' as NAMECAT'; sCatg:=''; end else begin sCat:='cat.NAMECAT'; sCatg:=',cat.NAMECAT'; end;

      quSelPrib.Active:=False;
      quSelPrib.SelectSQL.Clear;
      quSelPrib.SelectSQL.Add('SELECT ca.PARENT,'+sSkl+',0 as IDCLI,vr.CODEB,ca.NAME,'+sCat+','''' as SSALET,SUM(vr.QUANT) as QUANT ,SUM(vr.SUMOUT)as SUMOUT,SUM(vr.SUMIN)as SUMIN');
      quSelPrib.SelectSQL.Add('FROM OF_VREALB4 vr');
      quSelPrib.SelectSQL.Add('left join OF_CARDS ca on ca.ID=vr.CODEB');
      quSelPrib.SelectSQL.Add('left join OF_CARDSRCATEGORY cat on cat.ID=ca.RCATEGORY');
      quSelPrib.SelectSQL.Add('WHERE vr.DATEDOC>='''+FormatDateTime(sFormatDate,CommonSet.DateFrom)+''' and vr.DATEDOC<'''+FormatDateTime(sFormatDate,CommonSet.DateTo)+'''');

      if Prib.MHAll=False then quSelPrib.SelectSQL.Add('and vr.IDSKL in ('+Prib.sMHId+')');
      if Prib.CatAll=False then quSelPrib.SelectSQL.Add('and ca.RCATEGORY='+IntToStr(Prib.Cat));

      quSelPrib.SelectSQL.Add('GROUP by ca.PARENT'+sSklg+',vr.CODEB,ca.NAME'+sCatg);
      quSelPrib.Active:=True;

      if taPrib.Active=False then CloseTe(taPrib);

      quSelPrib.First;
      while not quSelPrib.Eof do
      begin

        prFind2group(quSelPribPARENT.AsInteger,S1,S2);

        taPrib.Append;
        taPribCodeB.AsInteger:=quSelPribCODEB.AsInteger;
        taPribName.AsString:=quSelPribNAME.AsString;
        taPribParent.AsInteger:=quSelPribPARENT.AsInteger;
        taPribNameGr1.AsString:=S1;
        taPribNameGr2.AsString:=S2;
        taPribOper.AsInteger:=4;
        taPribQuant.AsFloat:=quSelPribQUANT.AsFloat;
        taPribSumOut.AsFloat:=quSelPribSUMOUT.AsFloat;
        taPribSumIn.AsFloat:=quSelPribSUMIN.AsFloat;
        taPribPriceIn.AsFloat:=0;
        taPribPriceOut.AsFloat:=0;
        taPribPr1.AsFloat:=0;
        taPribPr2.AsFloat:=0;
        taPribPr3.AsFloat:=0;
        taPribDiff.AsFloat:=0;
        taPribDiffSum.AsFloat:=quSelPribSUMOUT.AsFloat-quSelPribSUMIN.AsFloat;
        taPribSaleMH.AsInteger:=quSelPribIDCLI.AsInteger;
        taPribSCategory.AsString:=quSelPribNAMECAT.AsString;
        taPribSSALET.AsString:=quSelPribSSALET.AsString;

        if abs(quSelPribQUANT.AsFloat)>0 then
        begin
          taPribPriceIn.AsFloat:=RoundEx(quSelPribSUMIN.AsFloat/quSelPribQUANT.AsFloat*100)/100;
          taPribPriceOut.AsFloat:=RoundEx(quSelPribSUMOUT.AsFloat/quSelPribQUANT.AsFloat*100)/100;
          taPribDiff.AsFloat:=RoundEx(quSelPribSUMOUT.AsFloat/quSelPribQUANT.AsFloat*100)/100-RoundEx(quSelPribSUMIN.AsFloat/quSelPribQUANT.AsFloat*100)/100;
        end;
        if abs(quSelPribSUMOUT.AsFloat)>0 then
        begin
          taPribPr2.AsFloat:=RoundEx(quSelPribSUMIN.AsFloat/quSelPribSUMOUT.AsFloat*1000)/10;
        end;

        taPribIdSkl.AsInteger:=quSelPribIdSkl.AsInteger;
        taPribCliTo.AsString:='';
        taPrib.Post;

        quSelPrib.Next;
      end;
      quSelPrib.Active:=False;
    end;

    PBar1.Position:=95; Delay(10);

    if (Prib.Oper=5)or(Prib.OperAll) then
    begin
      fmRepPrib.Memo1.Lines.Add('  ���������� �� �������.'); delay(10);

      if Prib.MHUnion then begin sSkl:='0 as IDSKL'; sSklg:=''; end else begin sSkl:='dh.IDSKL'; sSklg:=',dh.IDSKL'; end;
      if Prib.CatUnion then begin sCat:=''''' as NAMECAT'; sCatg:=''; end else begin sCat:='cat.NAMECAT'; sCatg:=',cat.NAMECAT'; end;
      if Prib.OperUnionCli then begin sCli:='0 as IDCLI'; sCliG:=''; end else begin sCli:='dh.IDCLI'; sClig:=',dh.IDCLI'; end;

      quSelPrib.Active:=False;
      quSelPrib.SelectSQL.Clear;
//      quSelPrib.SelectSQL.Add('SELECT ca.PARENT,'+sSkl+',0 as IDCLI,vr.IDCARD as CODEB,ca.NAME,'+sCat+','''' as SSALET,SUM(vr.QUANT*vr.KM) as QUANT ,SUM(vr.SUMR)as SUMOUT,SUM(vr.SUMIN)as SUMIN');
      quSelPrib.SelectSQL.Add('SELECT ca.PARENT,'+sSkl+','+sCli+',vr.IDCARD as CODEB,ca.NAME,'+sCat+','''' as SSALET,SUM(vr.QUANT*vr.KM) as QUANT ,SUM(vr.SUMR)as SUMOUT,SUM(vr.SUMIN)as SUMIN');
      quSelPrib.SelectSQL.Add('FROM OF_DOCSPECOUTR vr');
      quSelPrib.SelectSQL.Add('left join OF_CARDS ca on ca.ID=vr.IDCARD');
      quSelPrib.SelectSQL.Add('left join of_docheadoutr dh  on  dh.ID=vr.IDHEAD');
      quSelPrib.SelectSQL.Add('left join OF_CARDSRCATEGORY cat on cat.ID=ca.RCATEGORY');
      quSelPrib.SelectSQL.Add('WHERE dh.DATEDOC>='''+FormatDateTime(sFormatDate,CommonSet.DateFrom)+''' and dh.DATEDOC<'''+FormatDateTime(sFormatDate,CommonSet.DateTo)+'''');

      if Prib.MHAll=False then quSelPrib.SelectSQL.Add('and dh.IDSKL in ('+Prib.sMHId+')');
      if Prib.CatAll=False then quSelPrib.SelectSQL.Add('and ca.RCATEGORY='+IntToStr(Prib.Cat));

      quSelPrib.SelectSQL.Add(' and dh.IACTIVE=1 and vr.QUANT>0');

      quSelPrib.SelectSQL.Add('GROUP by ca.PARENT'+sSklg+sCliG+',vr.IDCARD,ca.NAME'+sCatg);

      quSelPrib.Active:=True;

      if taPrib.Active=False then CloseTe(taPrib);

      quSelPrib.First;
      while not quSelPrib.Eof do
      begin

        prFind2group(quSelPribPARENT.AsInteger,S1,S2);

        taPrib.Append;
        taPribCodeB.AsInteger:=quSelPribCODEB.AsInteger;
        taPribName.AsString:=quSelPribNAME.AsString;
        taPribParent.AsInteger:=quSelPribPARENT.AsInteger;
        taPribNameGr1.AsString:=S1;
        taPribNameGr2.AsString:=S2;
        taPribOper.AsInteger:=5;
        taPribQuant.AsFloat:=quSelPribQUANT.AsFloat;
        taPribSumOut.AsFloat:=quSelPribSUMOUT.AsFloat;
        taPribSumIn.AsFloat:=quSelPribSUMIN.AsFloat;
        taPribPriceIn.AsFloat:=0;
        taPribPriceOut.AsFloat:=0;
        taPribPr1.AsFloat:=0;
        taPribPr2.AsFloat:=0;
        taPribPr3.AsFloat:=0;
        taPribDiff.AsFloat:=0;
        taPribDiffSum.AsFloat:=quSelPribSUMOUT.AsFloat-quSelPribSUMIN.AsFloat;
        taPribSaleMH.AsInteger:=quSelPribIDCLI.AsInteger;
        taPribSCategory.AsString:=quSelPribNAMECAT.AsString;
        taPribSSALET.AsString:=quSelPribSSALET.AsString;

        if abs(quSelPribQUANT.AsFloat)>0 then
        begin
          taPribPriceIn.AsFloat:=RoundEx(quSelPribSUMIN.AsFloat/quSelPribQUANT.AsFloat*100)/100;
          taPribPriceOut.AsFloat:=RoundEx(quSelPribSUMOUT.AsFloat/quSelPribQUANT.AsFloat*100)/100;
          taPribDiff.AsFloat:=RoundEx(quSelPribSUMOUT.AsFloat/quSelPribQUANT.AsFloat*100)/100-RoundEx(quSelPribSUMIN.AsFloat/quSelPribQUANT.AsFloat*100)/100;
        end;
        if abs(quSelPribSUMOUT.AsFloat)>0 then
        begin
          taPribPr2.AsFloat:=RoundEx(quSelPribSUMIN.AsFloat/quSelPribSUMOUT.AsFloat*1000)/10;
        end;

        taPribIdSkl.AsInteger:=quSelPribIdSkl.AsInteger;

        if Prib.OperUnionCli=False then
        begin
          prFindCl(quSelPribIDCLI.AsInteger,S1,S2,S3,S4,S5,S6,s7,s8,s9,s10);
          taPribCliTo.AsString:=S1;
        end else taPribCliTo.AsString:='';

        taPrib.Post;

        quSelPrib.Next;
      end;
      quSelPrib.Active:=False;
    end;

    PBar1.Position:=100; Delay(10);
    PBar1.Visible:=False;

    ViewPrib.EndUpdate;

    fmRepPrib.Memo1.Lines.Add('������������ ��.'); delay(10);
    quSelPrib.Active:=False;
    quSelPribGr.Active:=False;
  end;
end;


procedure TfmRepPrib.FormCreate(Sender: TObject);
begin
  GridPrib.Align:=AlClient;
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
//  ViewPrib.RestoreFromIniFile(CurDir+GridIni);
  taPrib.Active:=False;
//  taPrib.FileName:=CurDir+'Prib.cds';

  Prib.MHId:=0;
  Prib.sMHId:='';
  Prib.MHAll:=False;
  Prib.MHUnion:=False;
  Prib.MProd:=0;
  Prib.MProdAll:=False;
  Prib.MProdUnion:=False;
  Prib.Cat:=0;
  Prib.CatAll:=False;
  Prib.CatUnion:=False;
  Prib.Salet:='';
  Prib.SaletAll:=False;
  Prib.SaletUnion:=False;
  Prib.Oper:=1;
  Prib.OperAll:=False;
  Prib.OperUnion:=False;
  Prib.OperUnionCli:=False;
end;

procedure TfmRepPrib.FormClose(Sender: TObject; var Action: TCloseAction);
begin
//  ViewPrib.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmRepPrib.FormShow(Sender: TObject);
begin
  Memo1.Clear;
end;

procedure TfmRepPrib.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmRepPrib.SpeedItem2Click(Sender: TObject);
begin
  //����� �� �������
  fmSelPerSkl2:=tfmSelPerSkl2.Create(Application);
  with fmSelPerSkl2 do
  begin
    cxDateEdit1.Date:=CommonSet.DateFrom;
    quMHAll1.Active:=False; quMHAll1.Active:=True; quMHAll1.First;
    if Prib.MHId>0 then cxLookupComboBox1.EditValue:=Prib.MHId else cxLookupComboBox1.EditValue:=quMHAll1ID.AsInteger;
    if Prib.sMHId>'' then fmSelPerSkl2.cxCheckComboBox1.Text:=Prib.sMHId;

    CheckBox1.Checked:=Prib.MHAll;
    CheckBox11.Checked:=Prib.MHUnion;

    quMProd.Active:=False; quMProd.Active:=True;  quMProd.First;
    if Prib.MProd>0 then cxLookupComboBox2.EditValue:=Prib.MProd else cxLookupComboBox2.EditValue:=quMProdID.AsInteger;
    CheckBox2.Checked:=Prib.MProdAll;
    CheckBox22.Checked:=Prib.MProdUnion;

    quCateg.Active:=False; quCateg.Active:=True; quCateg.First;
    if Prib.Cat>0 then cxLookupComboBox3.EditValue:=Prib.Cat else cxLookupComboBox3.EditValue:=quCategID.AsInteger;
    CheckBox3.Checked:=Prib.CatAll;
    CheckBox33.Checked:=Prib.CatUnion;

    quSalet.Active:=False; quSalet.Active:=True; quSalet.First;
    cxLookupComboBox4.EditValue:=quSaletSalet.AsInteger;
    CheckBox4.Checked:=Prib.SaletAll;
    CheckBox44.Checked:=Prib.SaletUnion;

    cxLookupComboBox5.EditValue:=Prib.Oper; 
    CheckBox5.Checked:=Prib.OperAll;
    CheckBox55.Checked:=Prib.OperUnion;
  end;
  fmSelPerSkl2.ShowModal;
  if (fmSelPerSkl2.ModalResult=mrOk)and(fmSelPerSkl2.cxCheckComboBox1.Text<>'�� �������') then
  begin
    CommonSet.DateFrom:=Trunc(fmSelPerSkl2.cxDateEdit1.Date);
    CommonSet.DateTo:=Trunc(fmSelPerSkl2.cxDateEdit2.Date)+1;
    CommonSet.IdStore:=fmSelPerSkl2.cxLookupComboBox1.EditValue;
    CommonSet.NameStore:=fmSelPerSkl2.cxLookupComboBox1.Text;

    if CommonSet.DateTo>=iMaxDate then fmRepPrib.Caption:='����� �� ������� �� �� '+CommonSet.NameStore+' � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
    else fmRepPrib.Caption:='����� �� ������� �� �� '+CommonSet.NameStore+' �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);

    teMH.Active:=False;
    teMH.Active:=True;

    fmSelPerSkl2.quMHAll1.First;
    while not fmSelPerSkl2.quMHAll1.Eof do
    begin
      teMH.Append;
      teMHID.AsInteger:=fmSelPerSkl2.quMHAll1ID.AsInteger;
      teMHNAMEMH.AsString:=fmSelPerSkl2.quMHAll1NAMEMH.AsString;
      teMH.Post;

      fmSelPerSkl2.quMHAll1.Next;
    end;

    fmSelPerSkl2.quMHAll1.Active:=False;
    fmSelPerSkl2.Release;

    ViewPrib.BeginUpdate;
    prFormPrib;
    ViewPrib.EndUpdate;

  end else
  begin
    fmSelPerSkl2.quMHAll1.Active:=False;
    fmSelPerSkl2.Release;
  end;
end;

procedure TfmRepPrib.SpeedItem3Click(Sender: TObject);
begin
  prNExportExel5(ViewPrib);
end;

procedure TfmRepPrib.SpeedItem4Click(Sender: TObject);
Var StrWk:String;
    i:Integer;
begin
//������
  ViewPrib.BeginUpdate;
  taPrib.Filter:=ViewPrib.DataController.Filter.FilterText;
  taPrib.Filtered:=True;

  frRepPrib.LoadFromFile(CurDir + 'Prib.frf');

  if CommonSet.DateTo>=iMaxDate then frVariables.Variable['sPeriod']:=' � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
  else frVariables.Variable['sPeriod']:=' �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);

  frVariables.Variable['DocStore']:=CommonSet.NameStore;

  StrWk:=ViewPrib.DataController.Filter.FilterCaption;
  while Pos('AND',StrWk)>0 do
  begin
    i:=Pos('AND',StrWk);
    StrWk[i]:=' ';
    StrWk[i+1]:='�';
    StrWk[i+2]:=' ';
  end;
  while Pos('and',StrWk)>0 do
  begin
    i:=Pos('and',StrWk);
    StrWk[i]:=' ';
    StrWk[i+1]:='�';
    StrWk[i+2]:=' ';
  end;

  frVariables.Variable['DocPars']:=StrWk;

  frRepPrib.ReportName:='����� � �������.';
  frRepPrib.PrepareReport;
  frRepPrib.ShowPreparedReport;

  taPrib.Filter:='';
  taPrib.Filtered:=False;
  ViewPrib.EndUpdate;
end;

procedure TfmRepPrib.taPrib1CalcFields(DataSet: TDataSet);
begin
  taPribPrNac.AsFloat:=0;
  if taPribSumIn.AsFloat<>0 then  taPribPrNac.AsFloat:=RoundVal((taPribSumOut.AsFloat-taPribSumIn.AsFloat)/taPribSumIn.AsFloat*100);
  taPribNameMH.AsString:='';
  if teMH.Locate('ID',taPribIdSkl.AsInteger,[]) then taPribNameMH.AsString:=teMHNAMEMH.AsString;
end;

procedure TfmRepPrib.SpeedItem5Click(Sender: TObject);
Var StrWk:String;
begin
  StrWk:=fmRepPrib.Caption;
  PrintPribLink1.ReportTitle.Text:=StrWk;
  PrintPrib.Preview(True,nil);
end;

procedure TfmRepPrib.N1Click(Sender: TObject);
begin
//��������
  ViewPrib.BeginUpdate;
  ViewPrib.DataController.Groups.FullCollapse;
  ViewPrib.EndUpdate;
end;

procedure TfmRepPrib.N2Click(Sender: TObject);
begin
//��������
  ViewPrib.BeginUpdate;
  ViewPrib.DataController.Groups.FullExpand;
  ViewPrib.EndUpdate;
end;

procedure TfmRepPrib.SpeedItem6Click(Sender: TObject);
begin
// ����������� ���������
  fmSummary:=tfmSummary.Create(Application);
  fmSummary.PivotGrid1.Visible:=True;
  fmSummary.PivotGrid2.Visible:=False;
  fmSummary.Caption:=fmRepPrib.Caption;
  fmSummary.ShowModal;
  fmSummary.Release;
end;

procedure TfmRepPrib.ViewPribDataControllerSummaryAfterSummary(
  ASender: TcxDataSummary);
begin
  try
    prCalcSumNac(3,4,5,ViewPrib);
    prCalcDefGroup(5,6,15,-1,ViewPrib);
  except
  end;
end;

procedure TfmRepPrib.taPribCalcFields(DataSet: TDataSet);
begin
  taPribPrNac.AsFloat:=0;
  if taPribSumIn.AsFloat<>0 then  taPribPrNac.AsFloat:=RoundVal((taPribSumOut.AsFloat-taPribSumIn.AsFloat)/taPribSumIn.AsFloat*100);
  taPribNameMH.AsString:='';
  if teMH.Locate('ID',taPribIdSkl.AsInteger,[]) then taPribNameMH.AsString:=teMHNAMEMH.AsString;
  taPribSum1.AsFloat:=0;
  if taPribSumOut.AsFloat<>0 then taPribSum1.AsFloat:=rv(taPribDiffSum.AsFloat/taPribSumOut.AsFloat);
end;

procedure TfmRepPrib.ViewPribDataControllerGroupingChanged(
  Sender: TObject);
begin
// delay(10);
end;

procedure TfmRepPrib.ViewPribDataControllerDetailCollapsed(
  ADataController: TcxCustomDataController; ARecordIndex: Integer);
begin
//  delay(10);
end;

procedure TfmRepPrib.ViewPribDataControllerDetailExpanding(
  ADataController: TcxCustomDataController; ARecordIndex: Integer;
  var AAllow: Boolean);
begin
//  delay(10);
end;

end.
