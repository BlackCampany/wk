unit Drivers;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, StdCtrls, ExtCtrls, SpeedBar,
  XPStyleActnCtrls, ActnList, ActnMan, cxImageComboBox;

type
  TfmDrivers = class(TForm)
    StatusBar1: TStatusBar;
    ViewDrv: TcxGridDBTableView;
    LevelDrv: TcxGridLevel;
    GrDrv: TcxGrid;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    amDrv: TActionManager;
    acAddDrv: TAction;
    acEditDrv: TAction;
    acDelDrv: TAction;
    Timer1: TTimer;
    ViewDrvID: TcxGridDBColumn;
    ViewDrvNAME1: TcxGridDBColumn;
    ViewDrvNAME2: TcxGridDBColumn;
    ViewDrvNAME3: TcxGridDBColumn;
    ViewDrvSDOC: TcxGridDBColumn;
    ViewDrvCARNAME: TcxGridDBColumn;
    ViewDrvCARNUM: TcxGridDBColumn;
    ViewDrvFIO: TcxGridDBColumn;
    procedure acAddDrvExecute(Sender: TObject);
    procedure acEditDrvExecute(Sender: TObject);
    procedure acDelDrvExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedItem4Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmDrivers: TfmDrivers;
  bClearPr:Boolean = False;

implementation

uses dmOffice, DMOReps, AddDRV, Un1;

{$R *.dfm}

procedure TfmDrivers.acAddDrvExecute(Sender: TObject);
begin
  //��������

  if not CanDo('prAddDrv') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;

  with dmORep do
  begin
    ViewDrv.BeginUpdate;

    fmAddDrv:=tfmAddDrv.Create(Application);
    fmAddDrv.Caption:='���������� ��������.';
    fmAddDrv.cxTextEdit1.Text:='';
    fmAddDrv.cxTextEdit2.Text:='';
    fmAddDrv.cxTextEdit3.Text:='';
    fmAddDrv.cxTextEdit4.Text:='';
    fmAddDrv.cxTextEdit5.Text:='';
    fmAddDrv.cxTextEdit6.Text:='';

    fmAddDrv.ShowModal;
    if fmAddDrv.ModalResult=mrOk then
    begin
      quDrv.Append;
      quDrvNAME1.AsString:=fmAddDrv.cxTextEdit1.Text;
      quDrvNAME2.AsString:=fmAddDrv.cxTextEdit2.Text;
      quDrvNAME3.AsString:=fmAddDrv.cxTextEdit3.Text;
      quDrvSDOC.AsString:=fmAddDrv.cxTextEdit4.Text;
      quDrvCARNAME.AsString:=fmAddDrv.cxTextEdit5.Text;
      quDrvCARNUM.AsString:=fmAddDrv.cxTextEdit6.Text;
      quDrv.Post;
    end;
    fmAddDrv.Release;
    ViewDrv.EndUpdate;
    delay(100);
    quDrv.Refresh;
  end;
end;

procedure TfmDrivers.acEditDrvExecute(Sender: TObject);
begin
//�������������
  if not CanDo('prEditDrv') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;

  with dmORep do
  begin
    if quDrv.RecordCount>0 then
    begin
      ViewDrv.BeginUpdate;

      fmAddDrv:=tfmAddDrv.Create(Application);
      fmAddDrv.Caption:='���������� ��������.';
      fmAddDrv.cxTextEdit1.Text:=quDrvNAME1.AsString;
      fmAddDrv.cxTextEdit2.Text:=quDrvNAME2.AsString;
      fmAddDrv.cxTextEdit3.Text:=quDrvNAME3.AsString;
      fmAddDrv.cxTextEdit4.Text:=quDrvSDOC.AsString;
      fmAddDrv.cxTextEdit5.Text:=quDrvCARNAME.AsString;
      fmAddDrv.cxTextEdit6.Text:=quDrvCARNUM.AsString;

      fmAddDrv.ShowModal;
      if fmAddDrv.ModalResult=mrOk then
      begin
        quDrv.Edit;
        quDrvNAME1.AsString:=fmAddDrv.cxTextEdit1.Text;
        quDrvNAME2.AsString:=fmAddDrv.cxTextEdit2.Text;
        quDrvNAME3.AsString:=fmAddDrv.cxTextEdit3.Text;
        quDrvSDOC.AsString:=fmAddDrv.cxTextEdit4.Text;
        quDrvCARNAME.AsString:=fmAddDrv.cxTextEdit5.Text;
        quDrvCARNUM.AsString:=fmAddDrv.cxTextEdit6.Text;
        quDrv.Post;
      end;
      fmAddDrv.Release;
      ViewDrv.EndUpdate;
      delay(100);
      quDrv.Refresh;
    end;
  end;
end;

procedure TfmDrivers.acDelDrvExecute(Sender: TObject);
begin
//�������
  if not CanDo('prDelDrv') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;

  with dmORep do
  begin
    if MessageDlg('�� ������������� ������ ��������: "'+quDrvFIO.AsString+'" ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      ViewDrv.BeginUpdate;
      quDrv.Delete;
      ViewDrv.EndUpdate;
    end;
  end;
end;

procedure TfmDrivers.FormCreate(Sender: TObject);
begin
  GrDrv.Align:=AlClient;
end;

procedure TfmDrivers.SpeedItem4Click(Sender: TObject);
begin
  Close;
end;

procedure TfmDrivers.Timer1Timer(Sender: TObject);
begin
  if bClearPr=True then begin StatusBar1.Panels[0].Text:=''; bClearPr:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearPr:=True;
end;

end.
