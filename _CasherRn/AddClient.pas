unit AddClient;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons, ExtCtrls,
  cxControls, cxContainer, cxEdit, cxTextEdit, cxMemo, cxCheckBox,
  cxMaskEdit, cxSpinEdit;

type
  TfmAddClients = class(TForm)
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Label1: TLabel;
    cxTextEdit1: TcxTextEdit;
    Label2: TLabel;
    cxTextEdit2: TcxTextEdit;
    Label3: TLabel;
    cxTextEdit3: TcxTextEdit;
    Label4: TLabel;
    cxTextEdit4: TcxTextEdit;
    Label5: TLabel;
    cxTextEdit5: TcxTextEdit;
    Label6: TLabel;
    cxMemo1: TcxMemo;
    cxCheckBox1: TcxCheckBox;
    Label7: TLabel;
    cxTextEdit6: TcxTextEdit;
    Label8: TLabel;
    cxTextEdit7: TcxTextEdit;
    Label9: TLabel;
    cxSpinEdit1: TcxSpinEdit;
    Label10: TLabel;
    cxTextEdit8: TcxTextEdit;
    Label11: TLabel;
    cxTextEdit9: TcxTextEdit;
    Label12: TLabel;
    cxTextEdit10: TcxTextEdit;
    Label13: TLabel;
    cxTextEdit11: TcxTextEdit;
    Label14: TLabel;
    cxTextEdit12: TcxTextEdit;
    Label15: TLabel;
    cxTextEdit13: TcxTextEdit;
    Label16: TLabel;
    cxTextEdit14: TcxTextEdit;
    Label17: TLabel;
    cxTextEdit15: TcxTextEdit;
    Label18: TLabel;
    cxTextEdit16: TcxTextEdit;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAddClients: TfmAddClients;

implementation

{$R *.dfm}

procedure TfmAddClients.FormShow(Sender: TObject);
begin
  cxTextEdit1.SetFocus;
  cxTextEdit1.SelectAll;
end;

end.
