unit SpecSel;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxEdit, DB, cxDBData, ExtCtrls, SpeedBar, cxGridLevel, cxClasses,
  cxControls, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, Placemnt, cxDataStorage, cxImageComboBox;

type
  TfmSpecSel = class(TForm)
    StatusBar1: TStatusBar;
    ViewSpecSel: TcxGridDBTableView;
    LevelSpecSel: TcxGridLevel;
    GridSpecSel: TcxGrid;
    SpeedBar1: TSpeedBar;
    FormPlacement1: TFormPlacement;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    ViewSpecSelID: TcxGridDBColumn;
    ViewSpecSelSIFR: TcxGridDBColumn;
    ViewSpecSelPRICE: TcxGridDBColumn;
    ViewSpecSelQUANTITY: TcxGridDBColumn;
    ViewSpecSelDISCOUNTPROC: TcxGridDBColumn;
    ViewSpecSelDISCOUNTSUM: TcxGridDBColumn;
    ViewSpecSelSUMMA: TcxGridDBColumn;
    ViewSpecSelITYPE: TcxGridDBColumn;
    ViewSpecSelNAMEMM: TcxGridDBColumn;
    ViewSpecSelNAMEMD: TcxGridDBColumn;
    ViewSpecSelNAME: TcxGridDBColumn;
    ViewSpecSelSTREAM: TcxGridDBColumn;
    ViewSpecSelNAMESTREAM: TcxGridDBColumn;
    SpeedItem2: TSpeedItem;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SpeedItem1Click(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmSpecSel: TfmSpecSel;

implementation

uses Un1, Dm;

{$R *.dfm}

procedure TfmSpecSel.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  ViewSpecSel.RestoreFromIniFile(CurDir+GridIni);
end;

procedure TfmSpecSel.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  dmC.taSpecAllSel.Active:=False;
  ViewSpecSel.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmSpecSel.SpeedItem1Click(Sender: TObject);
begin
  close;
end;

procedure TfmSpecSel.SpeedItem2Click(Sender: TObject);
begin
//������� � ������
  with dmC do prExportExel(ViewSpecSel,dsSpecAllSel,taSpecAllSel);
end;

end.
