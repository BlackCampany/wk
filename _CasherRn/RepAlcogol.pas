unit RepAlcogol;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxImageComboBox, cxProgressBar,
  cxContainer, cxTextEdit, cxMemo, ExtCtrls, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxControls, cxGridCustomView, cxGrid, SpeedBar, Placemnt, dxmdaset;

type
  TfmAlg = class(TForm)
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    GridAlg: TcxGrid;
    ViewAlg1: TcxGridDBTableView;
    LevelAlg1: TcxGridLevel;
    Panel1: TPanel;
    Memo1: TcxMemo;
    PBar1: TcxProgressBar;
    LevelAlg2: TcxGridLevel;
    ViewAlg2: TcxGridDBTableView;
    FormPlacement1: TFormPlacement;
    teAlg1: TdxMemData;
    dsteAlg1: TDataSource;
    dsteAlg2: TDataSource;
    teAlg1sVid: TStringField;
    teAlg1iVid: TIntegerField;
    teAlg1sIVid: TStringField;
    teAlg1Prod: TStringField;
    teAlg1ProdInn: TStringField;
    teAlg1ProdKpp: TStringField;
    teAlg1QuantB: TFloatField;
    teAlg1QuantIn: TFloatField;
    teAlg1QuantRet: TFloatField;
    teAlg1QuantR: TFloatField;
    teAlg1QuantE: TFloatField;
    teAlg1Col1_1: TStringField;
    teAlg1Col1_2: TStringField;
    teAlg1QuantIn1: TFloatField;
    teAlg1Col1_3: TStringField;
    teAlg1Col1_4: TStringField;
    teAlg1QuantIn2: TFloatField;
    teAlg1Col1_5: TStringField;
    teAlg1Col2_1: TStringField;
    teAlg1Col2_2: TStringField;
    teAlg1QuantOut: TFloatField;
    teAlg1ProdId: TIntegerField;
    ViewAlg1sVid: TcxGridDBColumn;
    ViewAlg1sIVid: TcxGridDBColumn;
    ViewAlg1Prod: TcxGridDBColumn;
    ViewAlg1ProdInn: TcxGridDBColumn;
    ViewAlg1ProdKpp: TcxGridDBColumn;
    ViewAlg1QuantB: TcxGridDBColumn;
    ViewAlg1QuantIn: TcxGridDBColumn;
    ViewAlg1Col1_1: TcxGridDBColumn;
    ViewAlg1Col1_2: TcxGridDBColumn;
    ViewAlg1QuantIn1: TcxGridDBColumn;
    ViewAlg1Col1_3: TcxGridDBColumn;
    ViewAlg1Col1_4: TcxGridDBColumn;
    ViewAlg1Col1_5: TcxGridDBColumn;
    ViewAlg1QuantIn2: TcxGridDBColumn;
    ViewAlg1QuantR: TcxGridDBColumn;
    ViewAlg1Col2_1: TcxGridDBColumn;
    ViewAlg1QuantRet: TcxGridDBColumn;
    ViewAlg1Col2_2: TcxGridDBColumn;
    ViewAlg1QuantOut: TcxGridDBColumn;
    ViewAlg1QuantE: TcxGridDBColumn;
    teAlg2: TdxMemData;
    teAlg2sVid: TStringField;
    teAlg2iVid: TIntegerField;
    teAlg2sIVid: TStringField;
    teAlg2ProdId: TIntegerField;
    teAlg2Prod: TStringField;
    teAlg2ProdInn: TStringField;
    teAlg2ProdKpp: TStringField;
    teAlg2iCli: TIntegerField;
    teAlg2CliName: TStringField;
    teAlg2CliInn: TStringField;
    teAlg2CliKpp: TStringField;
    teAlg2LicSerN: TStringField;
    teAlg2LicOrg: TStringField;
    teAlg2DocDate: TDateField;
    teAlg2DocNum: TStringField;
    teAlg2DocId: TIntegerField;
    teAlg2DocGTD: TStringField;
    teAlg2Quant: TFloatField;
    ViewAlg2sVid: TcxGridDBColumn;
    ViewAlg2sIVid: TcxGridDBColumn;
    ViewAlg2Prod: TcxGridDBColumn;
    ViewAlg2ProdInn: TcxGridDBColumn;
    ViewAlg2ProdKpp: TcxGridDBColumn;
    ViewAlg2CliName: TcxGridDBColumn;
    ViewAlg2CliInn: TcxGridDBColumn;
    ViewAlg2CliKpp: TcxGridDBColumn;
    ViewAlg2LicSerN: TcxGridDBColumn;
    ViewAlg2LicOrg: TcxGridDBColumn;
    ViewAlg2DocDate: TcxGridDBColumn;
    ViewAlg2DocNum: TcxGridDBColumn;
    ViewAlg2DocGTD: TcxGridDBColumn;
    ViewAlg2Quant: TcxGridDBColumn;
    teAlg2LicDateB: TStringField;
    teAlg2LicDateE: TStringField;
    ViewAlg2LicDateB: TcxGridDBColumn;
    ViewAlg2LicDateE: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAlg: TfmAlg;

implementation

uses Un1, u2fdk;

{$R *.dfm}

procedure TfmAlg.FormCreate(Sender: TObject);
begin
  GridAlg.Align:=AlClient;

  FormPlacement1.IniFileName:=CurDir+Person.Name+'\'+GridIni;
  FormPlacement1.Active:=True;
end;

procedure TfmAlg.SpeedItem1Click(Sender: TObject);
begin
  bStopAlg:=True;
  Close;
end;

procedure TfmAlg.SpeedItem3Click(Sender: TObject);
begin
  if LevelAlg1.Visible then prNExportExel5(ViewAlg1);
  if LevelAlg2.Visible then prNExportExel5(ViewAlg2);
end;

end.
