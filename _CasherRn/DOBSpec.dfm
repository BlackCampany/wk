object fmDOBSpec: TfmDOBSpec
  Left = 751
  Top = 111
  Width = 1073
  Height = 681
  Caption = #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1103' '#1088#1072#1089#1093#1086#1076#1072' '#1073#1083#1102#1076
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 628
    Width = 1065
    Height = 19
    Panels = <
      item
        Width = 300
      end
      item
        Width = 50
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 587
    Width = 1065
    Height = 41
    Align = alBottom
    BevelInner = bvLowered
    Color = 12582911
    TabOrder = 1
    object cxButton1: TcxButton
      Left = 240
      Top = 8
      Width = 125
      Height = 25
      Caption = #1042#1099#1093#1086#1076
      TabOrder = 0
      OnClick = cxButton1Click
      Glyph.Data = {
        5E040000424D5E04000000000000360000002800000012000000130000000100
        18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
        8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
        0000CED3D6848684848684848684848684848684848684848684848684848684
        848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
        7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
        FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
        FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
        007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
        FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
        00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
        494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
        00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000}
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 56
      Top = 8
      Width = 129
      Height = 25
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1088#1072#1089#1095#1077#1090
      TabOrder = 1
      OnClick = cxButton2Click
      Glyph.Data = {
        EE030000424DEE03000000000000360000002800000012000000110000000100
        180000000000B8030000C40E0000C40E00000000000000000000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6A5A6A57371
        737371735A595A5A595A4A494A4A494A393C39313031313031313031A5A6A5CE
        D3D6CED3D6CED3D60000CED3D6CED3D6A5A6A5633400CECFCEE7E7E7C6C3C6C6
        C3C6CECFCEF7F3F7EFEFEFE7E7E7A5A6A5313031313031A5A6A5CED3D6CED3D6
        0000CED3D6633400633400633400DEDBDEE7E7E7E7E7E7E7E7E7E7E7E7E7E7E7
        E7E7E7E7E7E7DEDBDECE8E42633400313031313031CED3D60000CED3D6633400
        CE8E42633400DEA67BDEA67BDEA67BDEA67BDEA67BCE8E42CE8E42CE8E42CE8E
        42CE8E426334007B5900313031CED3D60000CED3D6633400DEA67B633400DEA6
        7BDEA67BDEA67BDEA67BDEA67BDEA67BCE8E42CE8E42CE8E42CE8E42633400CE
        8E42313031CED3D60000CED3D6633400DEA67B633400DEA67BDEA67BDEA67BDE
        A67BDEA67BDEA67BDEA67BCE8E42CE8E42CE8E42633400CE8E42393C39CED3D6
        0000CED3D6633400DEA67B633400AD3C29633400633400633400633400633400
        633400633400633400CE8E42633400CE8E424A494ACED3D60000CED3D6633400
        DEA67B6334009C9A9CADFFFF9CFBFF9CFBFF9CFBFF9CFBFF9CFBFF9CFBFF9CFB
        FF633400633400CE8E424A494ACED3D60000CED3D6633400DEA67B633400ADFF
        FFCECFCEA5A6A5A5A6A5A5A6A5A5A6A5A5A6A5A5A6A5C6C3C69CFBFF633400CE
        8E424A494ACED3D60000CED3D6633400DEA67B7B5900ADFFFFADFFFFADFFFFAD
        FFFFADFFFFADFFFFADFFFFADFFFFADFFFF9CFBFF7B5900CE8E424A494ACED3D6
        0000CED3D6633400DEA67B7B5900ADFFFFCECFCEA5A6A5A5A6A5A5A6A5A5A6A5
        A5A6A5A5A6A5C6C3C69CFBFF7B5900CE8E424A494ACED3D60000CED3D6633400
        DEA67B9C9A9CADFFFFADFFFFADFFFFADFFFFADFFFFADFFFFADFFFFADFFFFADFF
        FF9CFBFF9C9A9CCE8E425A595ACED3D60000CED3D6633400DEA67BA5A6A5CED3
        D6CECFCECE8E42CE8E42A5A6A5A5A6A5A5A6A5A5A6A5C6C3C69CFBFFA5A6A5DE
        A67B636563CED3D60000CED3D6A5A6A56334007B5900CED3D6CED3D6ADFFFFAD
        FFFFADFFFFADFFFFADFFFFADFFFFADFFFFADFFFF7B5900633400A5A6A5CED3D6
        0000CED3D6CED3D6CED3D6A5A6A5633400633400633400633400633400633400
        6334006334006334006334009C9A9CCED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000}
      LookAndFeel.Kind = lfOffice11
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 81
    Width = 185
    Height = 506
    Align = alLeft
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 2
    object Label1: TLabel
      Left = 8
      Top = 144
      Width = 134
      Height = 13
      Cursor = crHandPoint
      Caption = '1. '#1055#1088#1086#1074#1077#1088#1082#1072' '#1089#1086#1086#1090#1074#1077#1090#1089#1090#1074#1080#1103
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
      Transparent = True
      OnClick = Label1Click
    end
    object Label2: TLabel
      Left = 8
      Top = 168
      Width = 155
      Height = 13
      Cursor = crHandPoint
      Caption = '2. '#1056#1072#1089#1095#1077#1090' '#1082#1086#1083'-'#1074#1072' '#1076#1083#1103' '#1089#1087#1080#1089#1072#1085#1080#1103
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
      Transparent = True
      OnClick = Label2Click
    end
    object Label4: TLabel
      Left = 8
      Top = 240
      Width = 132
      Height = 13
      Cursor = crHandPoint
      Caption = '4. '#1047#1072#1087#1086#1083#1085#1080#1090#1100' '#1090#1077#1082'. '#1086#1089#1090#1072#1090#1082#1080
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
      Transparent = True
      OnClick = Label4Click
    end
    object Label5: TLabel
      Left = 8
      Top = 264
      Width = 123
      Height = 13
      Cursor = crHandPoint
      Caption = '5. '#1055#1088#1086#1074#1077#1088#1082#1072' '#1087#1086' '#1087#1072#1088#1090#1080#1103#1084
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
      Transparent = True
      OnClick = Label5Click
    end
    object Label6: TLabel
      Left = 8
      Top = 192
      Width = 157
      Height = 13
      Cursor = crHandPoint
      Caption = '3. '#1056#1072#1089#1095#1077#1090' '#1089#1077#1073#1077#1089#1090#1086#1080#1084#1086#1089#1090#1080' '#1073#1083#1102#1076
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
      Transparent = True
      OnClick = Label6Click
    end
    object Label11: TLabel
      Left = 16
      Top = 24
      Width = 117
      Height = 13
      Cursor = crHandPoint
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1087#1086#1079#1080#1094#1080#1102'  Ins'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 4227072
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
      Transparent = True
      OnClick = Label11Click
    end
    object Label12: TLabel
      Left = 16
      Top = 80
      Width = 108
      Height = 13
      Cursor = crHandPoint
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1087#1086#1079#1080#1094#1080#1102'  F8'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
      Transparent = True
      OnClick = Label12Click
    end
    object Label15: TLabel
      Left = 88
      Top = 216
      Width = 72
      Height = 13
      Cursor = crHandPoint
      Caption = #1055#1077#1095#1072#1090#1100' '#1086#1090#1095#1077#1090#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
      Transparent = True
      OnClick = Label15Click
    end
    object Label3: TLabel
      Left = 16
      Top = 40
      Width = 130
      Height = 13
      Cursor = crHandPoint
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1089#1087#1080#1089#1086#1082'  Ctrl+Ins'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 4227072
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
      Transparent = True
      OnClick = Label3Click
    end
    object Label13: TLabel
      Left = 16
      Top = 96
      Width = 86
      Height = 13
      Cursor = crHandPoint
      Caption = #1054#1095#1080#1089#1090#1080#1090#1100'  Ctrl+F8'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
      Transparent = True
      OnClick = Label13Click
    end
  end
  object Panel3: TPanel
    Left = 185
    Top = 81
    Width = 880
    Height = 506
    Align = alClient
    BevelInner = bvLowered
    Caption = 'Panel3'
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 432
      Width = 876
      Height = 72
      Align = alBottom
      BevelInner = bvLowered
      Color = clWhite
      TabOrder = 0
      object Memo1: TcxMemo
        Left = 48
        Top = 2
        Lines.Strings = (
          'Memo1')
        ParentFont = False
        Properties.OEMConvert = True
        Properties.ReadOnly = True
        Properties.ScrollBars = ssVertical
        Properties.WordWrap = False
        Style.Font.Charset = ANSI_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'MS Sans Serif'
        Style.Font.Pitch = fpFixed
        Style.Font.Style = []
        Style.LookAndFeel.Kind = lfOffice11
        Style.IsFontAssigned = True
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 0
        OnDblClick = Memo1DblClick
        Height = 68
        Width = 534
      end
    end
    object cxSplitter1: TcxSplitter
      Left = 2
      Top = 429
      Width = 876
      Height = 3
      Cursor = crVSplit
      AlignSplitter = salBottom
      Control = Panel4
      Color = 16744448
      ParentColor = False
    end
    object PageControl1: TPageControl
      Left = 2
      Top = 2
      Width = 876
      Height = 427
      ActivePage = TabSheet2
      Align = alClient
      Style = tsFlatButtons
      TabOrder = 2
      object TabSheet1: TTabSheet
        Caption = #1041#1083#1102#1076#1072
        object GridB: TcxGrid
          Left = 0
          Top = 0
          Width = 868
          Height = 396
          Align = alClient
          PopupMenu = PopupMenu2
          TabOrder = 0
          LookAndFeel.Kind = lfOffice11
          object ViewB: TcxGridDBTableView
            NavigatorButtons.ConfirmDelete = False
            OnEditing = ViewBEditing
            OnEditKeyDown = ViewBEditKeyDown
            OnEditKeyPress = ViewBEditKeyPress
            DataController.DataSource = dsteSpecB
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <
              item
                Format = '0.00'
                Kind = skSum
                FieldName = 'RSUM'
                Column = ViewBRSUM
              end
              item
                Format = '0.00'
                Kind = skSum
                FieldName = 'DSUM'
                Column = ViewBDSUM
              end>
            DataController.Summary.SummaryGroups = <>
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsData.Inserting = False
            OptionsSelection.MultiSelect = True
            OptionsView.Footer = True
            OptionsView.GroupByBox = False
            OptionsView.Indicator = True
            object ViewBID: TcxGridDBColumn
              Caption = #8470' '#1087'.'#1087'.'
              DataBinding.FieldName = 'ID'
            end
            object ViewBSIFR: TcxGridDBColumn
              Caption = #1050#1086#1076' '#1073#1083#1102#1076#1072
              DataBinding.FieldName = 'SIFR'
              Options.Editing = False
            end
            object ViewBNAMEB: TcxGridDBColumn
              Caption = #1053#1072#1079#1074#1072#1085#1080#1077' '#1073#1083#1102#1076#1072
              DataBinding.FieldName = 'NAMEB'
              Options.Editing = False
              Width = 200
            end
            object ViewBCODEB: TcxGridDBColumn
              Caption = #1057#1074#1103#1079#1091#1102#1097#1080#1081' '#1082#1086#1076
              DataBinding.FieldName = 'CODEB'
              PropertiesClassName = 'TcxButtonEditProperties'
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.ReadOnly = False
              Properties.OnButtonClick = ViewBCODEBPropertiesButtonClick
            end
            object ViewBKB: TcxGridDBColumn
              Caption = #1050' '#1087#1077#1088#1077#1076#1072#1095#1080
              DataBinding.FieldName = 'KB'
              PropertiesClassName = 'TcxCalcEditProperties'
              Properties.AssignedValues.DisplayFormat = True
              Properties.ImmediatePost = True
            end
            object ViewBQUANT: TcxGridDBColumn
              Caption = #1050#1086#1083'-'#1074#1086
              DataBinding.FieldName = 'QUANT'
            end
            object ViewBPRICER: TcxGridDBColumn
              Caption = #1062#1077#1085#1072
              DataBinding.FieldName = 'PRICER'
            end
            object ViewBDSUM: TcxGridDBColumn
              Caption = #1057#1091#1084#1084#1072' '#1089#1082#1080#1076#1082#1080
              DataBinding.FieldName = 'DSUM'
              Width = 69
            end
            object ViewBRSUM: TcxGridDBColumn
              Caption = #1057#1091#1084#1084#1072' '#1088#1077#1072#1083#1080#1079#1072#1094#1080#1080
              DataBinding.FieldName = 'RSUM'
              Styles.Content = dmO.cxStyle25
            end
            object ViewBIDCARD: TcxGridDBColumn
              Caption = #1040#1088#1090#1080#1082#1091#1083
              DataBinding.FieldName = 'IDCARD'
            end
            object ViewBNAME: TcxGridDBColumn
              Caption = #1053#1072#1079#1074#1072#1085#1080#1077
              DataBinding.FieldName = 'NAME'
              Width = 200
            end
            object ViewBNAMESHORT: TcxGridDBColumn
              Caption = #1045#1076'.'#1080#1079#1084'.'
              DataBinding.FieldName = 'NAMESHORT'
              PropertiesClassName = 'TcxButtonEditProperties'
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.OnButtonClick = ViewBNAMESHORTPropertiesButtonClick
              Width = 73
            end
            object ViewBTCARD: TcxGridDBColumn
              Caption = #1058#1080#1087
              DataBinding.FieldName = 'TCARD'
              PropertiesClassName = 'TcxImageComboBoxProperties'
              Properties.Images = dmO.imState
              Properties.Items = <
                item
                  Value = 0
                end
                item
                  ImageIndex = 11
                  Value = 1
                end>
              Options.Editing = False
            end
            object ViewBKM: TcxGridDBColumn
              DataBinding.FieldName = 'KM'
              Visible = False
              Options.Editing = False
            end
            object ViewBSSALET: TcxGridDBColumn
              Caption = #1058#1080#1087' '#1087#1088#1086#1076#1072#1078#1080
              DataBinding.FieldName = 'SSALET'
              Options.Editing = False
            end
          end
          object LevelB: TcxGridLevel
            GridView = ViewB
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = #1058#1086#1074#1072#1088#1099
        ImageIndex = 1
        object GridC: TcxGrid
          Left = 0
          Top = 0
          Width = 868
          Height = 396
          Align = alClient
          TabOrder = 0
          LookAndFeel.Kind = lfOffice11
          object ViewC: TcxGridDBTableView
            PopupMenu = PopupMenu1
            NavigatorButtons.ConfirmDelete = False
            DataController.DataSource = dsteCalc
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <
              item
                Format = '0.00'
                Kind = skSum
                FieldName = 'SumIn'
                Column = ViewCSumIn
              end
              item
                Format = '0.00'
                Kind = skSum
                FieldName = 'SumIn0'
                Column = ViewCSumIn0
              end>
            DataController.Summary.SummaryGroups = <>
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsSelection.MultiSelect = True
            OptionsView.Footer = True
            OptionsView.GroupByBox = False
            OptionsView.Indicator = True
            object ViewCArticul: TcxGridDBColumn
              Caption = #1040#1088#1090#1080#1082#1091#1083
              DataBinding.FieldName = 'Articul'
            end
            object ViewCName: TcxGridDBColumn
              Caption = #1053#1072#1079#1074#1072#1085#1080#1077
              DataBinding.FieldName = 'Name'
              Width = 225
            end
            object ViewCsM: TcxGridDBColumn
              Caption = #1045#1076'.'#1080#1079#1084'.'
              DataBinding.FieldName = 'sM'
              Width = 67
            end
            object ViewCQuant: TcxGridDBColumn
              Caption = #1050#1086#1083'-'#1074#1086' '#1088#1072#1089#1095'.'
              DataBinding.FieldName = 'Quant'
            end
            object ViewCQuantFact: TcxGridDBColumn
              Caption = #1054#1089#1090#1072#1090#1086#1082
              DataBinding.FieldName = 'QuantFact'
            end
            object ViewCQuantDiff: TcxGridDBColumn
              Caption = #1042#1089#1077#1075#1086
              DataBinding.FieldName = 'QuantDiff'
            end
            object ViewCSumIn: TcxGridDBColumn
              Caption = #1057#1091#1084#1084#1072' '#1087#1088#1080#1093'. c '#1053#1044#1057
              DataBinding.FieldName = 'SumIn'
              Width = 79
            end
            object ViewCSumIn0: TcxGridDBColumn
              Caption = #1057#1091#1084#1084#1072' '#1087#1088#1080#1093'. '#1073#1077#1079' '#1053#1044#1057
              DataBinding.FieldName = 'SumIn0'
            end
          end
          object LevelC: TcxGridLevel
            GridView = ViewC
          end
        end
      end
      object TabSheet3: TTabSheet
        Caption = #1050#1072#1083#1100#1082#1091#1083#1103#1094#1080#1103
        ImageIndex = 2
        object GridBC: TcxGrid
          Left = 0
          Top = 0
          Width = 868
          Height = 396
          Align = alClient
          TabOrder = 0
          LookAndFeel.Kind = lfOffice11
          object ViewBC: TcxGridDBTableView
            PopupMenu = PopupMenu3
            OnDblClick = acCalcBBExecute
            NavigatorButtons.ConfirmDelete = False
            OnCustomDrawCell = ViewBCCustomDrawCell
            DataController.DataSource = dsteCalcB
            DataController.Summary.DefaultGroupSummaryItems = <
              item
                Format = '0.00'
                Kind = skSum
                Position = spFooter
                FieldName = 'SUMIN'
                Column = ViewBCSUMIN
              end
              item
                Format = '0.00'
                Kind = skSum
                Position = spFooter
                FieldName = 'PRICEIN'
                Column = ViewBCPRICEIN
              end
              item
                Format = '0.00'
                Kind = skAverage
                Position = spFooter
                FieldName = 'PRICEOUT'
                Column = ViewBCPRICEOUT
              end
              item
                Format = '0.00'
                Kind = skAverage
                Position = spFooter
                FieldName = 'SUMOUT'
                Column = ViewBCSUMOUT
              end
              item
                Format = '0.000'
                Kind = skSum
                Position = spFooter
                FieldName = 'QUANTC'
                Column = ViewBCQUANTC
              end>
            DataController.Summary.FooterSummaryItems = <
              item
                Format = '0.00'
                Kind = skSum
                FieldName = 'SUMIN'
                Column = ViewBCSUMIN
              end
              item
                Format = '0.00'
                Kind = skSum
                FieldName = 'PRICEIN'
                Column = ViewBCPRICEIN
              end
              item
                Format = '0.00'
                Kind = skSum
                FieldName = 'PRICEIN0'
                Column = ViewBCPRICEIN0
              end
              item
                Format = '0.00'
                Kind = skSum
                FieldName = 'SUMIN0'
                Column = ViewBCSUMIN0
              end>
            DataController.Summary.SummaryGroups = <>
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsSelection.MultiSelect = True
            OptionsView.Footer = True
            OptionsView.GroupFooters = gfAlwaysVisible
            OptionsView.Indicator = True
            object ViewBCID: TcxGridDBColumn
              Caption = #1050#1086#1076' '#1087#1086#1079#1080#1094#1080#1080
              DataBinding.FieldName = 'ID'
            end
            object ViewBCCODEB: TcxGridDBColumn
              Caption = #1050#1086#1076' '#1073#1083#1102#1076#1072
              DataBinding.FieldName = 'CODEB'
            end
            object ViewBCNAMEB: TcxGridDBColumn
              Caption = #1053#1072#1079#1074#1072#1085#1080#1077' '#1073#1083#1102#1076#1072
              DataBinding.FieldName = 'NAMEB'
              Width = 200
            end
            object ViewBCQUANT: TcxGridDBColumn
              Caption = #1050#1086#1083'-'#1074#1086' '#1073#1083#1102#1076#1072
              DataBinding.FieldName = 'QUANT'
            end
            object ViewBCPRICEOUT: TcxGridDBColumn
              Caption = #1062#1077#1085#1072' '#1087#1088#1086#1076#1072#1078#1080
              DataBinding.FieldName = 'PRICEOUT'
            end
            object ViewBCSUMOUT: TcxGridDBColumn
              Caption = #1057#1091#1084#1084#1072' '#1087#1088#1086#1076#1072#1078#1080
              DataBinding.FieldName = 'SUMOUT'
            end
            object ViewBCIDCARD: TcxGridDBColumn
              Caption = #1050#1086#1076' '#1090#1086#1074#1072#1088#1072
              DataBinding.FieldName = 'IDCARD'
            end
            object ViewBCNAMEC: TcxGridDBColumn
              Caption = #1053#1072#1079#1074#1072#1085#1080#1077' '#1090#1086#1074#1072#1088#1072
              DataBinding.FieldName = 'NAMEC'
              Width = 200
            end
            object ViewBCSB: TcxGridDBColumn
              Caption = #1058#1080#1087
              DataBinding.FieldName = 'SB'
            end
            object ViewBCQUANTC: TcxGridDBColumn
              Caption = #1050#1086#1083'-'#1074#1086
              DataBinding.FieldName = 'QUANTC'
            end
            object ViewBCPRICEIN: TcxGridDBColumn
              Caption = #1062#1077#1085#1072' '#1079#1072#1082#1091#1087#1072' '#1089' '#1053#1044#1057
              DataBinding.FieldName = 'PRICEIN'
            end
            object ViewBCSUMIN: TcxGridDBColumn
              Caption = #1057#1091#1084#1084#1072' '#1079#1072#1082#1091#1087#1072' c '#1053#1044#1057
              DataBinding.FieldName = 'SUMIN'
            end
            object ViewBCIM: TcxGridDBColumn
              Caption = #1050#1086#1076' '#1077#1076'.'#1080#1079#1084'.'
              DataBinding.FieldName = 'IM'
            end
            object ViewBCSM: TcxGridDBColumn
              Caption = #1045#1076'.'#1080#1079#1084'.'
              DataBinding.FieldName = 'SM'
              Width = 50
            end
            object ViewBCPRICEIN0: TcxGridDBColumn
              Caption = #1062#1077#1085#1072' '#1079#1072#1082#1091#1087#1072' '#1073#1077#1079' '#1053#1044#1057
              DataBinding.FieldName = 'PRICEIN0'
            end
            object ViewBCSUMIN0: TcxGridDBColumn
              Caption = #1057#1091#1084#1084#1072' '#1079#1072#1082#1091#1087#1072' '#1073#1077#1079' '#1053#1044#1057
              DataBinding.FieldName = 'SUMIN0'
            end
          end
          object LevelBC: TcxGridLevel
            GridView = ViewBC
          end
        end
      end
    end
  end
  object Panel5: TPanel
    Left = 0
    Top = 0
    Width = 1065
    Height = 81
    Align = alTop
    BevelInner = bvLowered
    Color = 12582911
    TabOrder = 4
    object Label7: TLabel
      Left = 16
      Top = 16
      Width = 34
      Height = 13
      Caption = #1053#1086#1084#1077#1088
      Transparent = True
    end
    object Label8: TLabel
      Left = 192
      Top = 16
      Width = 26
      Height = 13
      Caption = #1044#1072#1090#1072
      Transparent = True
    end
    object Label9: TLabel
      Left = 16
      Top = 48
      Width = 82
      Height = 13
      Caption = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103
      Transparent = True
    end
    object Label10: TLabel
      Left = 368
      Top = 16
      Width = 50
      Height = 13
      Caption = #1054#1087#1077#1088#1072#1094#1080#1103
      Transparent = True
    end
    object Label14: TLabel
      Left = 360
      Top = 48
      Width = 63
      Height = 13
      Caption = #1057#1086#1076#1077#1088#1078#1072#1085#1080#1077
      Transparent = True
    end
    object cxTextEdit1: TcxTextEdit
      Left = 64
      Top = 12
      Style.BorderStyle = ebsOffice11
      Style.Shadow = True
      TabOrder = 0
      Text = 'cxTextEdit1'
      Width = 97
    end
    object cxDateEdit1: TcxDateEdit
      Left = 224
      Top = 12
      EditValue = 39083d
      Style.BorderStyle = ebsOffice11
      Style.Shadow = True
      TabOrder = 1
      Width = 105
    end
    object cxLookupComboBox1: TcxLookupComboBox
      Left = 112
      Top = 44
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Caption = #1053#1072#1079#1074#1072#1085#1080#1077
          FieldName = 'NAMEMH'
        end>
      Properties.ListOptions.AnsiSort = True
      Properties.ListSource = dmO.dsMHAll
      Style.BorderStyle = ebsOffice11
      Style.LookAndFeel.Kind = lfOffice11
      Style.Shadow = True
      Style.PopupBorderStyle = epbsDefault
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 2
      Width = 217
    end
    object cxLookupComboBox2: TcxLookupComboBox
      Left = 432
      Top = 10
      Properties.KeyFieldNames = 'ABR'
      Properties.ListColumns = <
        item
          Caption = #1040#1073#1073#1088#1077#1074#1080#1072#1090#1091#1088#1072
          FieldName = 'ABR'
        end
        item
          Caption = #1053#1072#1079#1074#1072#1085#1080#1077
          FieldName = 'NAMEOPER'
        end>
      Properties.ListSource = dmORep.dstaOperD
      Style.BorderStyle = ebsOffice11
      Style.Shadow = True
      TabOrder = 3
      Width = 145
    end
    object cxTextEdit2: TcxTextEdit
      Left = 432
      Top = 44
      Style.BorderStyle = ebsOffice11
      Style.Shadow = True
      TabOrder = 4
      Text = 'cxTextEdit2'
      Width = 361
    end
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 3000
    OnTimer = Timer1Timer
    Left = 336
    Top = 192
  end
  object FormPlacement1: TFormPlacement
    Active = False
    Left = 432
    Top = 320
  end
  object amDOB: TActionManager
    Left = 377
    Top = 256
    StyleName = 'XP Style'
    object acCalcB: TAction
      Caption = 'acCalcB'
      OnExecute = acCalcBExecute
    end
    object acCalcBB: TAction
      Caption = 'acCalcBB'
      OnExecute = acCalcBBExecute
    end
    object acExpExcel1: TAction
      Caption = #1069#1082#1089#1087#1086#1088#1090' '#1074' Excel'
      OnExecute = acExpExcel1Execute
    end
    object acTest1: TAction
      Caption = 'acTest1'
      ShortCut = 16433
      OnExecute = acTest1Execute
    end
    object acInsPos: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1087#1086#1079#1080#1094#1080#1102
      ShortCut = 45
      OnExecute = acInsPosExecute
    end
    object acAddList: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1089#1087#1080#1089#1086#1082
      ShortCut = 16429
      OnExecute = acAddListExecute
    end
    object acDelPos: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1087#1086#1079#1080#1094#1080#1102
      ShortCut = 119
      OnExecute = acDelPosExecute
    end
    object acClear: TAction
      Caption = #1054#1095#1080#1089#1090#1080#1090#1100
      ShortCut = 16503
      OnExecute = acClearExecute
    end
    object acMovePos: TAction
      Caption = #1044#1074#1080#1078#1077#1085#1080#1077' '#1087#1086' '#1090#1086#1074#1072#1088#1091
      ShortCut = 32885
      OnExecute = acMovePosExecute
    end
    object acDelPos1: TAction
      ShortCut = 24695
      OnExecute = acDelPos1Execute
    end
    object acSaveB1: TAction
      Caption = 'acSaveB1'
      ShortCut = 24689
      OnExecute = acSaveB1Execute
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 511
    Top = 294
    object Excel1: TMenuItem
      Action = acExpExcel1
      Bitmap.Data = {
        56080000424D560800000000000036000000280000001A0000001A0000000100
        18000000000020080000C40E0000C40E00000000000000000000C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0004000004000004000004000C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C00000C0C0C0004000
        80808080808080808080808080808080808080808000400040E02040E020FFFF
        FFC8D0D4A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0808080C0
        C0C0C0C0C0C0C0C00000C0C0C0C0C0C000400040E02000C02000C02000C02000
        C02000400040E02040E020FFFFFF004000C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4A4A0A0A4A0A0404040C0C0C0C0C0C00000C0C0C0C0C0C0
        C0C0C000400000C02000C02000C02000400000C02040E020FFFFFF00400000C0
        20C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4A4A0A0A4A0A040
        2020808080C0C0C00000C0C0C0C0C0C0C0C0C0A4A0A000400000C02000400000
        C02040E020FFFFFF004000004000004000C8D0D4A4A0A0A4A0A0A4A0A0A4A0A0
        A4A0A0A4A0A0808080C8D0D4808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C000400040E02040E020FFFFFF004000F0FBFFF0FBFFF0FB
        FFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFA4A0A0C8D0D4C8D0D440
        2020808080C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C000400040E02040E020FF
        FFFF004000808080004000C8D0D4C0DCC0C0DCC0C0DCC0C0DCC0C0DCC0C0DCC0
        C0DCC0C8D0D4F0FBFFA4A0A0C8D0D4402020808080C0C0C00000C0C0C0C0C0C0
        C0C0C000400000C02040E020FFFFFF00400080E0A000C020808080004000FFFF
        FFFFFFFFFFFFFFF0FBFFC0DCC0C0DCC0F0FBFFC0DCC0F0FBFF808080C8D0D440
        2020808080C0C0C00000C0C0C0C0C0C000400000C02040E020FFFFFF004000FF
        FFFF00400000C02000C020808080004000FFFFFFFFFFFFC8D0D4F0FBFFC0DCC0
        C0DCC0C0DCC0F0FBFFC0DCC0A4A0A0404020808080C0C0C00000C0C0C0004000
        004000004000004000FFFFFFFFFFFFFFFFFFFFFFFF0040000040000040000040
        00C8D0D4F0FBFFC0DCC0C8D0D4C8D0D4F0FBFFC0DCC0F0FBFFFFFFFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFC0DCC0FFFFFFF0FBFFFFFFFFFFFFFFF0FBFFF0FBFFF0FBFF
        FFFFFFC0DCC0F0FBFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0F0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FB
        FFF0FBFFFFFFFFFFFFFFF0FBFFF0FBFFFFFFFFC0DCC0F0FBFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0F0
        FBFFF0FBFFF0FBFFC0DCC0C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4F0FBFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0C0DCC0C0DCC0F0FBFFF0FBFFC0DCC0FFFF
        FFC0DCC0F0FBFFC0DCC0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0C0
        DCC0F0FBFFF0FBFFF0FBFFF0FBFFFFFFFFF0FBFFF0FBFFC0DCC0F0FBFFFFFFFF
        FFFFFFFFFFFFFFFFFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0C0DCC0C8D0D4F0FBFFF0FBFFC8D0D4FFFF
        FFC8D0D4F0FBFFC0DCC0F0FBFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0F0
        FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFC8D0D4F0FBFFFFFFFF
        FFFFFFFFFFFFFFFFFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFF0FBFFC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4C8D0D4C8D0D4C8D0D4F0FBFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFF0FBFFF0
        FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080C0DCC0C8D0D4C8D0D4F0FBFFC8D0D4C0DCC0C0DCC0C8D0D4F0FB
        FFC8D0D4C0DCC0F0FBFFC8D0D4F0FBFFC8D0D4C8D0D4F0FBFFA4A0A080808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C080808080E0E040E0E040E0E080
        E0E040E0E080E0E080E0E040E0E080E0E040E0E040E0E080E0E040E0E080E0E0
        40E0E040E0E080E0E000E0E0408080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C080808080E0E080E0E0C0DCC080E0E0C0DCC080E0E080E0E0C0DCC080E0
        E0C0DCC080E0E080E0E0C0DCC080E0E0C0DCC080E0E080E0E0C0DCC000808000
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080F0FBFFF0FBFFF0FBFF80
        E0E0F0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFF
        F0FBFFF0FBFFF0FBFFF0FBFF00C0C0002040808080C0C0C00000C0C0C0C0C0C0
        C0C0C08080808080808080808080808080808080808080808080808080808080
        8080808080808080808080808080808080808080808080808080808080808000
        2040C0C0C0C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C00000}
    end
  end
  object CasherRnDb2: TpFIBDatabase
    DBName = '192.168.26.150:C:\_SoftUr\DBBEL\casherrn.gdb'
    DBParams.Strings = (
      'user_name=SYSDBA'
      'lc_ctype=WIN1251'
      'password=masterkey'
      'sql_role_name=SYSDBA')
    DefaultTransaction = trSelCode
    DefaultUpdateTransaction = trSelCode
    SQLDialect = 3
    Timeout = 0
    SynchronizeTime = False
    DesignDBOptions = []
    AliasName = 'CasherRnDb'
    WaitForRestoreConnect = 20
    Left = 224
    Top = 240
  end
  object trSelCode: TpFIBTransaction
    DefaultDatabase = CasherRnDb2
    TimeoutAction = TARollback
    Left = 220
    Top = 307
  end
  object quFindCode: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT PRICE,'
      '       CODE,'
      '       CONSUMMA,'
      '       IACTIVE'
      'FROM MENU'
      'where SIFR=:ISIFR'
      'order by IACTIVE')
    Transaction = trSelCode
    Database = CasherRnDb2
    Left = 308
    Top = 259
    poAskRecordCount = True
    object quFindCodePRICE: TFIBFloatField
      FieldName = 'PRICE'
    end
    object quFindCodeCODE: TFIBStringField
      FieldName = 'CODE'
      Size = 10
      EmptyStrToNull = True
    end
    object quFindCodeCONSUMMA: TFIBFloatField
      FieldName = 'CONSUMMA'
    end
    object quFindCodeIACTIVE: TFIBSmallIntField
      FieldName = 'IACTIVE'
    end
  end
  object PopupMenu2: TPopupMenu
    Left = 599
    Top = 294
    object MenuItem1: TMenuItem
      Bitmap.Data = {
        56080000424D560800000000000036000000280000001A0000001A0000000100
        18000000000020080000C40E0000C40E00000000000000000000C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0004000004000004000004000C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C00000C0C0C0004000
        80808080808080808080808080808080808080808000400040E02040E020FFFF
        FFC8D0D4A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0808080C0
        C0C0C0C0C0C0C0C00000C0C0C0C0C0C000400040E02000C02000C02000C02000
        C02000400040E02040E020FFFFFF004000C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4A4A0A0A4A0A0404040C0C0C0C0C0C00000C0C0C0C0C0C0
        C0C0C000400000C02000C02000C02000400000C02040E020FFFFFF00400000C0
        20C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4A4A0A0A4A0A040
        2020808080C0C0C00000C0C0C0C0C0C0C0C0C0A4A0A000400000C02000400000
        C02040E020FFFFFF004000004000004000C8D0D4A4A0A0A4A0A0A4A0A0A4A0A0
        A4A0A0A4A0A0808080C8D0D4808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C000400040E02040E020FFFFFF004000F0FBFFF0FBFFF0FB
        FFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFA4A0A0C8D0D4C8D0D440
        2020808080C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C000400040E02040E020FF
        FFFF004000808080004000C8D0D4C0DCC0C0DCC0C0DCC0C0DCC0C0DCC0C0DCC0
        C0DCC0C8D0D4F0FBFFA4A0A0C8D0D4402020808080C0C0C00000C0C0C0C0C0C0
        C0C0C000400000C02040E020FFFFFF00400080E0A000C020808080004000FFFF
        FFFFFFFFFFFFFFF0FBFFC0DCC0C0DCC0F0FBFFC0DCC0F0FBFF808080C8D0D440
        2020808080C0C0C00000C0C0C0C0C0C000400000C02040E020FFFFFF004000FF
        FFFF00400000C02000C020808080004000FFFFFFFFFFFFC8D0D4F0FBFFC0DCC0
        C0DCC0C0DCC0F0FBFFC0DCC0A4A0A0404020808080C0C0C00000C0C0C0004000
        004000004000004000FFFFFFFFFFFFFFFFFFFFFFFF0040000040000040000040
        00C8D0D4F0FBFFC0DCC0C8D0D4C8D0D4F0FBFFC0DCC0F0FBFFFFFFFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFC0DCC0FFFFFFF0FBFFFFFFFFFFFFFFF0FBFFF0FBFFF0FBFF
        FFFFFFC0DCC0F0FBFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0F0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FB
        FFF0FBFFFFFFFFFFFFFFF0FBFFF0FBFFFFFFFFC0DCC0F0FBFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0F0
        FBFFF0FBFFF0FBFFC0DCC0C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4F0FBFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0C0DCC0C0DCC0F0FBFFF0FBFFC0DCC0FFFF
        FFC0DCC0F0FBFFC0DCC0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0C0
        DCC0F0FBFFF0FBFFF0FBFFF0FBFFFFFFFFF0FBFFF0FBFFC0DCC0F0FBFFFFFFFF
        FFFFFFFFFFFFFFFFFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0C0DCC0C8D0D4F0FBFFF0FBFFC8D0D4FFFF
        FFC8D0D4F0FBFFC0DCC0F0FBFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0F0
        FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFC8D0D4F0FBFFFFFFFF
        FFFFFFFFFFFFFFFFFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFF0FBFFC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4C8D0D4C8D0D4C8D0D4F0FBFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFF0FBFFF0
        FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080C0DCC0C8D0D4C8D0D4F0FBFFC8D0D4C0DCC0C0DCC0C8D0D4F0FB
        FFC8D0D4C0DCC0F0FBFFC8D0D4F0FBFFC8D0D4C8D0D4F0FBFFA4A0A080808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C080808080E0E040E0E040E0E080
        E0E040E0E080E0E080E0E040E0E080E0E040E0E040E0E080E0E040E0E080E0E0
        40E0E040E0E080E0E000E0E0408080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C080808080E0E080E0E0C0DCC080E0E0C0DCC080E0E080E0E0C0DCC080E0
        E0C0DCC080E0E080E0E0C0DCC080E0E0C0DCC080E0E080E0E0C0DCC000808000
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080F0FBFFF0FBFFF0FBFF80
        E0E0F0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFF
        F0FBFFF0FBFFF0FBFFF0FBFF00C0C0002040808080C0C0C00000C0C0C0C0C0C0
        C0C0C08080808080808080808080808080808080808080808080808080808080
        8080808080808080808080808080808080808080808080808080808080808000
        2040C0C0C0C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C00000}
      Caption = #1069#1082#1089#1087#1086#1088#1090' '#1074' Excel'
      OnClick = MenuItem1Click
    end
    object MenuItem2: TMenuItem
      Caption = '-'
    end
    object N2: TMenuItem
      Caption = #1054#1073#1085#1091#1083#1080#1090#1100' '#1094#1077#1085#1099' '#1087#1088#1086#1076#1072#1078#1080
      OnClick = N2Click
    end
  end
  object teCalcB: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 560
    Top = 152
    object teCalcBID: TIntegerField
      FieldName = 'ID'
    end
    object teCalcBCODEB: TIntegerField
      FieldName = 'CODEB'
    end
    object teCalcBNAMEB: TStringField
      FieldName = 'NAMEB'
      Size = 30
    end
    object teCalcBQUANT: TFloatField
      FieldName = 'QUANT'
      DisplayFormat = '0.000'
    end
    object teCalcBPRICEOUT: TFloatField
      FieldName = 'PRICEOUT'
      DisplayFormat = '0.00'
    end
    object teCalcBSUMOUT: TFloatField
      FieldName = 'SUMOUT'
      DisplayFormat = '0.00'
    end
    object teCalcBIDCARD: TIntegerField
      FieldName = 'IDCARD'
    end
    object teCalcBNAMEC: TStringField
      FieldName = 'NAMEC'
      Size = 30
    end
    object teCalcBQUANTC: TFloatField
      FieldName = 'QUANTC'
      DisplayFormat = '0.000'
    end
    object teCalcBPRICEIN: TFloatField
      FieldName = 'PRICEIN'
      DisplayFormat = '0.00'
    end
    object teCalcBSUMIN: TFloatField
      FieldName = 'SUMIN'
      DisplayFormat = '0.00'
    end
    object teCalcBIM: TIntegerField
      FieldName = 'IM'
    end
    object teCalcBSM: TStringField
      FieldName = 'SM'
      Size = 30
    end
    object teCalcBSB: TStringField
      FieldName = 'SB'
    end
    object teCalcBPRICEIN0: TFloatField
      FieldName = 'PRICEIN0'
      DisplayFormat = '0.00'
    end
    object teCalcBSUMIN0: TFloatField
      FieldName = 'SUMIN0'
      DisplayFormat = '0.00'
    end
  end
  object dsteCalcB: TDataSource
    DataSet = teCalcB
    Left = 567
    Top = 206
  end
  object teSpecB: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 415
    Top = 150
    object teSpecBID: TIntegerField
      FieldName = 'ID'
    end
    object teSpecBIDCARD: TIntegerField
      FieldName = 'IDCARD'
    end
    object teSpecBQUANT: TFloatField
      FieldName = 'QUANT'
    end
    object teSpecBIDM: TIntegerField
      FieldName = 'IDM'
    end
    object teSpecBSIFR: TIntegerField
      FieldName = 'SIFR'
    end
    object teSpecBNAMEB: TStringField
      FieldName = 'NAMEB'
      Size = 100
    end
    object teSpecBCODEB: TStringField
      FieldName = 'CODEB'
      Size = 10
    end
    object teSpecBKB: TFloatField
      FieldName = 'KB'
    end
    object teSpecBPRICER: TFloatField
      FieldName = 'PRICER'
      OnChange = teSpecBPRICERChange
      DisplayFormat = '0.00'
    end
    object teSpecBDSUM: TCurrencyField
      FieldName = 'DSUM'
      OnChange = teSpecBDSUMChange
      DisplayFormat = '0.00'
    end
    object teSpecBRSUM: TCurrencyField
      FieldName = 'RSUM'
      OnChange = teSpecBRSUMChange
      DisplayFormat = '0.00'
    end
    object teSpecBNAME: TStringField
      FieldName = 'NAME'
      Size = 200
    end
    object teSpecBNAMESHORT: TStringField
      FieldName = 'NAMESHORT'
    end
    object teSpecBTCARD: TSmallintField
      FieldName = 'TCARD'
    end
    object teSpecBKM: TFloatField
      FieldName = 'KM'
    end
    object teSpecBSALET: TIntegerField
      FieldName = 'SALET'
    end
    object teSpecBSSALET: TStringField
      FieldName = 'SSALET'
      Size = 30
    end
  end
  object teCalc: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 487
    Top = 150
    object teCalcArticul: TIntegerField
      FieldName = 'Articul'
    end
    object teCalcName: TStringField
      FieldName = 'Name'
      Size = 100
    end
    object teCalcIdM: TIntegerField
      FieldName = 'IdM'
    end
    object teCalcsM: TStringField
      FieldName = 'sM'
    end
    object teCalcKm: TFloatField
      FieldName = 'Km'
    end
    object teCalcQuant: TFloatField
      FieldName = 'Quant'
      DisplayFormat = '0.000'
    end
    object teCalcQuantFact: TFloatField
      FieldName = 'QuantFact'
      DisplayFormat = '0.000'
    end
    object teCalcQuantDiff: TFloatField
      FieldName = 'QuantDiff'
      DisplayFormat = '0.000'
    end
    object teCalcSumUch: TFloatField
      FieldName = 'SumUch'
      DisplayFormat = '0.00'
    end
    object teCalcSumIn: TFloatField
      FieldName = 'SumIn'
      DisplayFormat = '0.00'
    end
    object teCalcSumIn0: TFloatField
      FieldName = 'SumIn0'
      DisplayFormat = '0.00'
    end
  end
  object dsteSpecB: TDataSource
    DataSet = teSpecB
    Left = 415
    Top = 206
  end
  object dsteCalc: TDataSource
    DataSet = teCalc
    Left = 487
    Top = 206
  end
  object frRepDOB: TfrReport
    InitialZoom = pzDefault
    PreviewButtons = [pbZoom, pbLoad, pbSave, pbPrint, pbFind, pbHelp, pbExit]
    RebuildPrinter = False
    Left = 232
    Top = 144
    ReportForm = {19000000}
  end
  object frdsteCalcB: TfrDBDataSet
    DataSource = dsteCalcB
    Left = 304
    Top = 144
  end
  object PopupMenu3: TPopupMenu
    Left = 675
    Top = 294
    object MenuItem3: TMenuItem
      Bitmap.Data = {
        56080000424D560800000000000036000000280000001A0000001A0000000100
        18000000000020080000C40E0000C40E00000000000000000000C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0004000004000004000004000C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C00000C0C0C0004000
        80808080808080808080808080808080808080808000400040E02040E020FFFF
        FFC8D0D4A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0808080C0
        C0C0C0C0C0C0C0C00000C0C0C0C0C0C000400040E02000C02000C02000C02000
        C02000400040E02040E020FFFFFF004000C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4A4A0A0A4A0A0404040C0C0C0C0C0C00000C0C0C0C0C0C0
        C0C0C000400000C02000C02000C02000400000C02040E020FFFFFF00400000C0
        20C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4A4A0A0A4A0A040
        2020808080C0C0C00000C0C0C0C0C0C0C0C0C0A4A0A000400000C02000400000
        C02040E020FFFFFF004000004000004000C8D0D4A4A0A0A4A0A0A4A0A0A4A0A0
        A4A0A0A4A0A0808080C8D0D4808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C000400040E02040E020FFFFFF004000F0FBFFF0FBFFF0FB
        FFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFA4A0A0C8D0D4C8D0D440
        2020808080C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C000400040E02040E020FF
        FFFF004000808080004000C8D0D4C0DCC0C0DCC0C0DCC0C0DCC0C0DCC0C0DCC0
        C0DCC0C8D0D4F0FBFFA4A0A0C8D0D4402020808080C0C0C00000C0C0C0C0C0C0
        C0C0C000400000C02040E020FFFFFF00400080E0A000C020808080004000FFFF
        FFFFFFFFFFFFFFF0FBFFC0DCC0C0DCC0F0FBFFC0DCC0F0FBFF808080C8D0D440
        2020808080C0C0C00000C0C0C0C0C0C000400000C02040E020FFFFFF004000FF
        FFFF00400000C02000C020808080004000FFFFFFFFFFFFC8D0D4F0FBFFC0DCC0
        C0DCC0C0DCC0F0FBFFC0DCC0A4A0A0404020808080C0C0C00000C0C0C0004000
        004000004000004000FFFFFFFFFFFFFFFFFFFFFFFF0040000040000040000040
        00C8D0D4F0FBFFC0DCC0C8D0D4C8D0D4F0FBFFC0DCC0F0FBFFFFFFFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFC0DCC0FFFFFFF0FBFFFFFFFFFFFFFFF0FBFFF0FBFFF0FBFF
        FFFFFFC0DCC0F0FBFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0F0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FB
        FFF0FBFFFFFFFFFFFFFFF0FBFFF0FBFFFFFFFFC0DCC0F0FBFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0F0
        FBFFF0FBFFF0FBFFC0DCC0C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4F0FBFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0C0DCC0C0DCC0F0FBFFF0FBFFC0DCC0FFFF
        FFC0DCC0F0FBFFC0DCC0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0C0
        DCC0F0FBFFF0FBFFF0FBFFF0FBFFFFFFFFF0FBFFF0FBFFC0DCC0F0FBFFFFFFFF
        FFFFFFFFFFFFFFFFFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0C0DCC0C8D0D4F0FBFFF0FBFFC8D0D4FFFF
        FFC8D0D4F0FBFFC0DCC0F0FBFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0F0
        FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFC8D0D4F0FBFFFFFFFF
        FFFFFFFFFFFFFFFFFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFF0FBFFC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4C8D0D4C8D0D4C8D0D4F0FBFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFF0FBFFF0
        FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080C0DCC0C8D0D4C8D0D4F0FBFFC8D0D4C0DCC0C0DCC0C8D0D4F0FB
        FFC8D0D4C0DCC0F0FBFFC8D0D4F0FBFFC8D0D4C8D0D4F0FBFFA4A0A080808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C080808080E0E040E0E040E0E080
        E0E040E0E080E0E080E0E040E0E080E0E040E0E040E0E080E0E040E0E080E0E0
        40E0E040E0E080E0E000E0E0408080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C080808080E0E080E0E0C0DCC080E0E0C0DCC080E0E080E0E0C0DCC080E0
        E0C0DCC080E0E080E0E0C0DCC080E0E0C0DCC080E0E080E0E0C0DCC000808000
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080F0FBFFF0FBFFF0FBFF80
        E0E0F0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFF
        F0FBFFF0FBFFF0FBFFF0FBFF00C0C0002040808080C0C0C00000C0C0C0C0C0C0
        C0C0C08080808080808080808080808080808080808080808080808080808080
        8080808080808080808080808080808080808080808080808080808080808000
        2040C0C0C0C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C00000}
      Caption = #1069#1082#1089#1087#1086#1088#1090' '#1074' Excel'
      OnClick = MenuItem3Click
    end
  end
end
