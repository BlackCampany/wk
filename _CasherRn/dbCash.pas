unit dbCash;

interface

uses
  SysUtils, Classes, DB, FIBDataSet, pFIBDataSet, FIBDatabase,
  pFIBDatabase, FIBQuery, pFIBQuery, pFIBStoredProc,cxMemo;

type
  TdmCash = class(TDataModule)
    CasherRnDb: TpFIBDatabase;
    prSetStream: TpFIBStoredProc;
    trSelM: TpFIBTransaction;
    trUpdM: TpFIBTransaction;
    quSelMH: TpFIBDataSet;
    quSelMHSTORE: TFIBIntegerField;
    quSpecAll: TpFIBDataSet;
    quSpecAllOPERTYPE: TFIBStringField;
    quSpecAllSIFR: TFIBIntegerField;
    quSpecAllNAME: TFIBStringField;
    quSpecAllCODE: TFIBStringField;
    quSpecAllCONSUMMA: TFIBFloatField;
    quSpecAllQSUM: TFIBFloatField;
    quSpecAllDSUM: TFIBFloatField;
    quSpecAllRSUM: TFIBFloatField;
    quSpecAllDISCONT1: TFIBStringField;
    quSpecAllCLINAME: TFIBStringField;
    quSpecAllITYPE: TFIBSmallIntField;
    quSpecAllNAME1: TFIBStringField;
    quSpecAllCODEB: TFIBIntegerField;
    quSpecAllKB: TFIBFloatField;
    quSpecAllSALET: TFIBSmallIntField;
    quSpecAllCNTPRICE: TFIBSmallIntField;
    quSpecAllTCAS: TFIBStringField;
    quSpecAllMCAS: TFIBStringField;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure prCloseCashDbDate(DBName:String;CurDate:TDateTime;NumDB:INteger;Memo1:TcxMemo);
  end;


var
  dmCash: TdmCash;

implementation

uses dmOffice, DMOReps, Un1;

{$R *.dfm}

procedure TdmCash.prCloseCashDbDate(DBName:String;CurDate:TDateTime;NumDB:INteger;Memo1:TcxMemo);
Var sOper:String;
    IDH,IDS:Integer;
    rSum,Km:Real;
    iCode,iM:INteger;
    bHead:Boolean;
    sPC,sPCDB:String;

 procedure prMemo(S:String);
 begin
   prWHAUTO(S,Memo1);
   {
   if Memo1<>nil then
   begin
     Memo1.Lines.Add(S);
     delay(10);
   end;}
 end;

begin
  with dmO do
  with dmORep do
  begin
    CasherRnDb.Connected:=False;
    CasherRnDb.DatabaseName:=DBNAME;
    try
      CasherRnDb.Connected:=True;
    except
      prMemo('������������ ����: '+DBNAME+'. ����� ������ ����������.');
    end;

    if CasherRnDb.Connected then
    begin
      //��������� ������

      prMemo('      ���� : '+DBNAME+'. ��.');

//    sOper:=''; //��������� ��������

      prSetStream.ParamByName('DATEB').AsDateTime:=CurDate;
      prSetStream.ParamByName('DATEE').AsDateTime:=CurDate+1;
      prSetStream.ExecProc;
      delay(100);

      prMemo('    - SetStream  ��.');

      if prFindReal(NumDB,Trunc(CurDate),1)<>1 then //��������� ������� ���������� � ��������� ����������
      begin
        quSelMH.Active:=False;
        quSelMH.Active:=True;
        quSelMH.First;
        while not quSelMH.Eof do
        begin
          prMemo('      �� '+quSelMHSTORE.AsString);

          rSum:=0; IdH:=0; bHead:=False; sOper:=''; sPC:='PCCard';
          quSpecAll.Active:=False;
          quSpecAll.ParamByName('DATEB').AsDateTime:=CurDate;
          quSpecAll.ParamByName('DATEE').AsDateTime:=CurDate+1;
          quSpecAll.ParamByName('IDSKL').AsInteger:=quSelMHSTORE.AsInteger;
          quSpecAll.Active:=True;

          IDS:=1;
          quSpecAll.First;
          while not quSpecAll.Eof do
          begin
            if quSpecAllOPERTYPE.AsString='SalePC' then
            begin
              if CommonSet.SelPCard=1 then sPCDB:=quSpecAllDISCONT1.AsString
              else sPCDB:='PC';
              if sPCDB<>sPC then
              begin //������ SalePC �������� ���������� - ����� ����� �������� ��������
                if bHead then
                begin //��������� ��� ���� ����� ��������� �����
                  if abs(rSum)>0 then
                  begin
                    quDobHead.Edit;
                    quDOBHEADSUMUCH.AsFloat:=rSum;
                    quDobHead.Post;
                  end;
                end;

                sOper:=quSpecAllOPERTYPE.AsString;
                sPC:=sPCDB;

                IDH:=GetId('DocOutB');
                IDS:=1;
                quDOBHEAD.Active:=False;
                quDOBHEAD.ParamByName('IDH').AsInteger:=IDH;
                quDOBHEAD.Active:=True;

                quDOBHEAD.Append;
                quDOBHEADID.AsInteger:=IDH;
                quDOBHEADDATEDOC.AsDateTime:=Trunc(CurDate);
                quDOBHEADNUMDOC.AsString:=IntToStr(IDH);
                quDOBHEADDATESF.AsDateTime:=Trunc(CurDate);
                quDOBHEADNUMSF.AsString:=IntToStr(IDH);
                quDOBHEADIDCLI.AsInteger:=NumDB;
                quDOBHEADIDSKL.AsInteger:=quSelMHSTORE.AsInteger;
                quDOBHEADSUMIN.AsFloat:=0;
                quDOBHEADSUMUCH.AsFloat:=0;
                quDOBHEADSUMTAR.AsFloat:=0;
                quDOBHEADSUMNDS0.AsFloat:=0;
                quDOBHEADSUMNDS1.AsFloat:=0;
                quDOBHEADSUMNDS2.AsFloat:=0;
                quDOBHEADPROCNAC.AsFloat:=0;
                quDOBHEADIACTIVE.AsInteger:=0;
                quDOBHEADOPER.AsString:=sOper;
                quDOBHEADCOMMENT.AsString:=quSpecAllCLINAME.AsString;
                quDOBHEAD.Post;

                prMemo('   ���. '+ds1(CurDate)+' '+its(IDH)+' '+sOper+' '+quSpecAllCLINAME.AsString);

                quDOBSpec.Active:=False;
                quDobSpec.ParamByName('IDH').AsInteger:=IDH;
                quDobSpec.Active:=True;

                bHead:=True;
                rSum:=0; //��������� �����
              end;
            end
            else
            begin
              if quSpecAllOPERTYPE.AsString<>sOper then
              begin //��������� ��������� � ��������� sOper
                if bHead then
                begin //��������� ��� ���� ����� ��������� �����
                  if abs(rSum)>0 then
                  begin
                    quDobHead.Edit;
                    quDOBHEADSUMUCH.AsFloat:=rSum;
                    quDobHead.Post;
                  end;
                end;

                sOper:=quSpecAllOPERTYPE.AsString;

                IDH:=GetId('DocOutB');
                IDS:=1;
                quDOBHEAD.Active:=False;
                quDOBHEAD.ParamByName('IDH').AsInteger:=IDH;
                quDOBHEAD.Active:=True;

                quDOBHEAD.Append;
                quDOBHEADID.AsInteger:=IDH;
                quDOBHEADDATEDOC.AsDateTime:=Trunc(CurDate);
                quDOBHEADNUMDOC.AsString:=IntToStr(IDH);
                quDOBHEADDATESF.AsDateTime:=Trunc(CurDate);
                quDOBHEADNUMSF.AsString:=IntToStr(IDH);
                quDOBHEADIDCLI.AsInteger:=NumDB;
                quDOBHEADIDSKL.AsInteger:=quSelMHSTORE.AsInteger;
                quDOBHEADSUMIN.AsFloat:=0;
                quDOBHEADSUMUCH.AsFloat:=0;
                quDOBHEADSUMTAR.AsFloat:=0;
                quDOBHEADSUMNDS0.AsFloat:=0;
                quDOBHEADSUMNDS1.AsFloat:=0;
                quDOBHEADSUMNDS2.AsFloat:=0;
                quDOBHEADPROCNAC.AsFloat:=0;
                quDOBHEADIACTIVE.AsInteger:=0;
                quDOBHEADOPER.AsString:=sOper;
                quDOBHEADCOMMENT.AsString:='';
                try  //�� ������ ������
                  if dmORep.quDB.Active then
                   quDOBHEADCOMMENT.AsString:=dmORep.quDBNAMEDB.AsString;
                except
                end;
                quDOBHEAD.Post;

                prMemo('   ���. '+ds1(CurDate)+' '+its(IDH)+' '+sOper+' '+quSpecAllCLINAME.AsString);

                quDOBSpec.Active:=False;
                quDobSpec.ParamByName('IDH').AsInteger:=IDH;
                quDobSpec.Active:=True;

                bHead:=True;
                rSum:=0; //��������� �����
              end;
            end;

            //�������� ������������
//            IDS:=GetId('SpecOutB');

            iM:=0; Km:=0; iCode:=0;

            if quSpecAllITYPE.AsInteger=0 then iCode:=StrToIntDef(quSpecAllCODE.AsString,0); //�����
            if quSpecAllITYPE.AsInteger=1 then iCode:=StrToIntDef(quSpecAllCODEB.AsString,0); //������������
            if iCode>0 then
            begin
              quFindCard.Active:=False;
              quFindCard.ParamByName('IDCARD').AsInteger:=iCode;
              quFindCard.Active:=True;
              if quFindCard.RecordCount>0 then
              begin
                iM:=quFindCardIMESSURE.AsInteger;
                if iM>0 then
                begin
                  quM.Active:=False;
                  quM.ParamByName('IDM').AsInteger:=iM;
                  quM.Active:=True;
                  if quM.RecordCount>0 then KM:=quMKOEF.AsFloat;
                  quM.Active:=False;
                end;
              end else iCode:=0;
              quFindCard.Active:=False;
            end;

            quDobSpec.Append;
            quDOBSPECIDHEAD.AsInteger:=IDH;
            quDOBSPECID.AsInteger:=IDS;
            quDOBSPECSIFR.AsInteger:=quSpecAllSIFR.AsInteger;
            if quSpecAllITYPE.AsInteger=0 then
            begin
              quDOBSPECNAMEB.AsString:=quSpecAllNAME.AsString;
              quDOBSPECCODEB.AsString:=quSpecAllCODE.AsString;
              quDOBSPECKB.AsFloat:=quSpecAllCONSUMMA.AsFloat;
            end else
            begin
              quDOBSPECNAMEB.AsString:=quSpecAllNAME1.AsString;
              quDOBSPECCODEB.AsString:=quSpecAllCODEB.AsString;
              quDOBSPECKB.AsFloat:=quSpecAllKB.AsFloat;
            end;
            quDOBSPECDSUM.AsFloat:=quSpecAllDSUM.AsFloat;
            quDOBSPECRSUM.AsFloat:=quSpecAllRSUM.AsFloat;
            quDOBSPECQUANT.AsFloat:=quSpecAllQSUM.AsFloat;
            quDOBSPECIDCARD.AsInteger:=iCode;
            quDOBSPECIDM.AsInteger:=iM;
            quDOBSPECKM.AsFloat:=kM;
            quDOBSPECPRICER.AsFloat:=0;
            if abs(quSpecAllQSUM.AsFloat)>0 then quDOBSPECPRICER.AsFloat:=RoundEx(quSpecAllRSUM.AsFloat/quSpecAllQSUM.AsFloat*100)/100;

            quDOBSPECSALET.AsInteger:=1;
            quDOBSPECSSALET.AsString:='������� �������';

            if quSpecAllSALET.AsInteger>1 then
            begin
              quDOBSPECSALET.AsInteger:=quSpecAllSALET.AsInteger;
              quDOBSPECSSALET.AsString:=quSpecAllTCAS.AsString;
            end else
            begin
              if quSpecAllCNTPRICE.AsInteger>1 then
              begin
                quDOBSPECSALET.AsInteger:=quSpecAllCNTPRICE.AsInteger;
                quDOBSPECSSALET.AsString:=quSpecAllMCAS.AsString;
              end;
            end;

            quDobSpec.Post;

            inc(IDS);
            rSum:=rSum+quSpecAllRSUM.AsFloat;

            quSpecAll.Next;
          end;
          if bHead then
          begin //��������� ��� ���� ����� ��������� �����
            if abs(rSum)>0 then
            begin
              quDobHead.Edit;
              quDOBHEADSUMUCH.AsFloat:=rSum;
              quDobHead.Post;
            end;
          end;
          quDobHead.Active:=False;
          quDobSpec.Active:=False;

          quSelMH.Next;
        end;

        quSelMH.Active:=False;
      end else prMemo('      ������ �� ������ - ��� ���� �������� ���������.');

      CasherRnDb.Connected:=False;
      prMemo('      ���� �������.');
    end;
  end;
end;


end.
