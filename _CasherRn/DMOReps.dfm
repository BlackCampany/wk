object dmORep: TdmORep
  OldCreateOrder = False
  Left = 1139
  Top = 214
  Height = 988
  Width = 1288
  object taPartTest: TClientDataSet
    Aggregates = <>
    FileName = 'PartTest.cds'
    Params = <>
    Left = 24
    Top = 16
    object taPartTestNum: TIntegerField
      FieldName = 'Num'
    end
    object taPartTestIdGoods: TIntegerField
      FieldName = 'IdGoods'
    end
    object taPartTestNameG: TStringField
      FieldName = 'NameG'
      Size = 200
    end
    object taPartTestIM: TIntegerField
      FieldName = 'IM'
    end
    object taPartTestSM: TStringField
      FieldName = 'SM'
      Size = 50
    end
    object taPartTestQuant: TFloatField
      FieldName = 'Quant'
      DisplayFormat = '0.000'
      EditFormat = '0.000'
    end
    object taPartTestPrice1: TCurrencyField
      FieldName = 'Price1'
      DisplayFormat = ',0.00;-,0.00'
      EditFormat = ',0.00;-,0.00'
    end
    object taPartTestiRes: TSmallintField
      FieldName = 'iRes'
    end
  end
  object dsPartTest: TDataSource
    DataSet = taPartTest
    Left = 24
    Top = 64
  end
  object taCalc: TClientDataSet
    Aggregates = <>
    FileName = 'Calc.cds'
    Params = <>
    Left = 89
    Top = 16
    object taCalcArticul: TIntegerField
      FieldName = 'Articul'
    end
    object taCalcName: TStringField
      FieldName = 'Name'
      Size = 100
    end
    object taCalcIdm: TIntegerField
      FieldName = 'Idm'
    end
    object taCalcsM: TStringField
      FieldName = 'sM'
    end
    object taCalcKm: TFloatField
      FieldName = 'Km'
    end
    object taCalcQuant: TFloatField
      FieldName = 'Quant'
      DisplayFormat = '0.000'
    end
    object taCalcQuantFact: TFloatField
      FieldName = 'QuantFact'
      DisplayFormat = '0.000'
    end
    object taCalcQuantDiff: TFloatField
      FieldName = 'QuantDiff'
      DisplayFormat = '0.000'
    end
    object taCalcSumUch: TFloatField
      FieldName = 'SumUch'
      DisplayFormat = '0.00'
    end
    object taCalcSumIn: TFloatField
      FieldName = 'SumIn'
      DisplayFormat = '0.00'
    end
  end
  object dsCalc: TDataSource
    DataSet = taCalc
    Left = 89
    Top = 64
  end
  object frRep1: TfrReport
    InitialZoom = pzDefault
    PreviewButtons = [pbZoom, pbLoad, pbSave, pbPrint, pbFind, pbHelp, pbExit]
    RebuildPrinter = False
    Left = 312
    Top = 16
    ReportForm = {19000000}
  end
  object frTextExport1: TfrTextExport
    ScaleX = 1.000000000000000000
    ScaleY = 1.000000000000000000
    Left = 1248
    Top = 356
  end
  object frRTFExport1: TfrRTFExport
    ScaleX = 1.300000000000000000
    ScaleY = 1.000000000000000000
    Left = 1188
    Top = 508
  end
  object frCSVExport1: TfrCSVExport
    ScaleX = 1.000000000000000000
    ScaleY = 1.000000000000000000
    Delimiter = ';'
    Left = 1188
    Top = 348
  end
  object frHTMExport1: TfrHTMExport
    ScaleX = 1.000000000000000000
    ScaleY = 1.000000000000000000
    Left = 1188
    Top = 204
  end
  object frHTML2Export1: TfrHTML2Export
    Scale = 1.000000000000000000
    Navigator.Position = []
    Navigator.Font.Charset = DEFAULT_CHARSET
    Navigator.Font.Color = clWindowText
    Navigator.Font.Height = -11
    Navigator.Font.Name = 'MS Sans Serif'
    Navigator.Font.Style = []
    Navigator.InFrame = False
    Navigator.WideInFrame = False
    Left = 1188
    Top = 252
  end
  object frOLEExcelExport1: TfrOLEExcelExport
    HighQuality = False
    CellsAlign = False
    CellsBorders = False
    CellsFillColor = False
    CellsFontColor = False
    CellsFontName = False
    CellsFontSize = False
    CellsFontStyle = False
    CellsMerged = False
    CellsWrapWords = False
    ExportPictures = False
    PageBreaks = False
    AsText = False
    Left = 1188
    Top = 452
  end
  object frXMLExcelExport1: TfrXMLExcelExport
    Left = 1188
    Top = 404
  end
  object frTextAdvExport1: TfrTextAdvExport
    ScaleWidth = 1.000000000000000000
    ScaleHeight = 1.000000000000000000
    Borders = True
    Pseudogrpahic = False
    PageBreaks = True
    OEMCodepage = False
    EmptyLines = True
    LeadSpaces = True
    PrintAfter = False
    PrinterDialog = True
    UseSavedProps = True
    Left = 1188
    Top = 300
  end
  object frRtfAdvExport1: TfrRtfAdvExport
    OpenAfterExport = True
    Wysiwyg = True
    Creator = 'FastReport http://www.fast-report.com'
    Left = 1188
    Top = 156
  end
  object frBMPExport1: TfrBMPExport
    Left = 1188
    Top = 12
  end
  object frJPEGExport1: TfrJPEGExport
    Left = 1188
    Top = 60
  end
  object frTIFFExport1: TfrTIFFExport
    Left = 1188
    Top = 108
  end
  object taCalcB: TClientDataSet
    Aggregates = <>
    AutoCalcFields = False
    FileName = 'CalcB.cds'
    FieldDefs = <
      item
        Name = 'ID'
        DataType = ftInteger
      end
      item
        Name = 'CODEB'
        DataType = ftInteger
      end
      item
        Name = 'NAMEB'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'QUANT'
        DataType = ftFloat
      end
      item
        Name = 'PRICEOUT'
        DataType = ftFloat
      end
      item
        Name = 'SUMOUT'
        DataType = ftFloat
      end
      item
        Name = 'IDCARD'
        DataType = ftInteger
      end
      item
        Name = 'NAMEC'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'QUANTC'
        DataType = ftFloat
      end
      item
        Name = 'PRICEIN'
        DataType = ftFloat
      end
      item
        Name = 'SUMIN'
        DataType = ftFloat
      end
      item
        Name = 'IM'
        DataType = ftInteger
      end
      item
        Name = 'SM'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'SB'
        DataType = ftString
        Size = 10
      end>
    IndexDefs = <
      item
        Name = 'taCalcBIndex'
        Fields = 'ID'
        Options = [ixPrimary]
      end>
    IndexFieldNames = 'ID'
    Params = <>
    StoreDefs = True
    Left = 153
    Top = 20
    object taCalcBID: TIntegerField
      FieldName = 'ID'
    end
    object taCalcBCODEB: TIntegerField
      FieldName = 'CODEB'
    end
    object taCalcBNAMEB: TStringField
      FieldName = 'NAMEB'
      Size = 30
    end
    object taCalcBQUANT: TFloatField
      FieldName = 'QUANT'
      DisplayFormat = '0.000'
    end
    object taCalcBPRICEOUT: TFloatField
      FieldName = 'PRICEOUT'
      DisplayFormat = '0.00'
    end
    object taCalcBSUMOUT: TFloatField
      FieldName = 'SUMOUT'
      DisplayFormat = '0.00'
    end
    object taCalcBIDCARD: TIntegerField
      FieldName = 'IDCARD'
    end
    object taCalcBNAMEC: TStringField
      FieldName = 'NAMEC'
      Size = 30
    end
    object taCalcBQUANTC: TFloatField
      FieldName = 'QUANTC'
      DisplayFormat = '0.000'
    end
    object taCalcBPRICEIN: TFloatField
      FieldName = 'PRICEIN'
      DisplayFormat = '0.00'
    end
    object taCalcBSUMIN: TFloatField
      FieldName = 'SUMIN'
      DisplayFormat = '0.00'
    end
    object taCalcBIM: TIntegerField
      FieldName = 'IM'
    end
    object taCalcBSM: TStringField
      FieldName = 'SM'
    end
    object taCalcBSB: TStringField
      FieldName = 'SB'
      Size = 10
    end
  end
  object dsCalcB: TDataSource
    DataSet = taCalcB1
    Left = 152
    Top = 76
  end
  object frdsCalcB: TfrDBDataSet
    DataSource = dsCalcB
    Left = 368
    Top = 16
  end
  object taTORep: TClientDataSet
    Aggregates = <>
    FileName = 'to.cds'
    FieldDefs = <
      item
        Name = 'sType'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'sDocType'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'sCliName'
        DataType = ftString
        Size = 200
      end
      item
        Name = 'sDate'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'sDocNum'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'rSum'
        DataType = ftCurrency
      end
      item
        Name = 'rSum1'
        DataType = ftCurrency
      end
      item
        Name = 'rSumO'
        DataType = ftCurrency
      end
      item
        Name = 'rSumO1'
        DataType = ftCurrency
      end
      item
        Name = 'rSumPr'
        DataType = ftFloat
      end
      item
        Name = 'PriceType'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'sSumPr'
        DataType = ftString
        Size = 15
      end
      item
        Name = 'sSumNDS'
        DataType = ftString
        Size = 20
      end>
    IndexDefs = <
      item
        Name = 'taTORepIndex1'
        Fields = 'sType;sDocType'
      end>
    IndexName = 'taTORepIndex1'
    Params = <>
    StoreDefs = True
    Left = 24
    Top = 128
    object taTORepsType: TStringField
      FieldName = 'sType'
    end
    object taTORepsDocType: TStringField
      FieldName = 'sDocType'
      Size = 50
    end
    object taTORepsCliName: TStringField
      FieldName = 'sCliName'
      Size = 200
    end
    object taTORepsDate: TStringField
      FieldName = 'sDate'
      Size = 10
    end
    object taTORepsDocNum: TStringField
      FieldName = 'sDocNum'
    end
    object taTOReprSum: TCurrencyField
      FieldName = 'rSum'
    end
    object taTOReprSum1: TCurrencyField
      FieldName = 'rSum1'
    end
    object taTOReprSumO: TCurrencyField
      FieldName = 'rSumO'
    end
    object taTOReprSumO1: TCurrencyField
      FieldName = 'rSumO1'
    end
    object taTOReprSumPr: TFloatField
      FieldName = 'rSumPr'
    end
    object taTORepPriceType: TStringField
      FieldName = 'PriceType'
    end
    object taTORepsSumPr: TStringField
      FieldName = 'sSumPr'
      Size = 15
    end
    object taTORepsSumNDS: TStringField
      FieldKind = fkCalculated
      FieldName = 'sSumNDS'
      Calculated = True
    end
  end
  object dsTORep: TDataSource
    DataSet = taTORep
    Left = 24
    Top = 184
  end
  object frdsTORep: TfrDBDataSet
    DataSource = dsTORep
    Left = 328
    Top = 72
  end
  object taCMove: TClientDataSet
    Aggregates = <>
    FileName = 'CMove.cds'
    Params = <>
    Left = 80
    Top = 128
    object taCMoveARTICUL: TIntegerField
      FieldName = 'ARTICUL'
    end
    object taCMoveIDATE: TIntegerField
      FieldName = 'IDATE'
    end
    object taCMoveIDSTORE: TIntegerField
      FieldName = 'IDSTORE'
    end
    object taCMoveNAMEMH: TStringField
      FieldName = 'NAMEMH'
      Size = 200
    end
    object taCMoveRB: TFloatField
      FieldName = 'RB'
      DisplayFormat = '0.000'
    end
    object taCMovePOSTIN: TFloatField
      FieldName = 'POSTIN'
      DisplayFormat = '0.000'
    end
    object taCMovePOSTOUT: TFloatField
      FieldName = 'POSTOUT'
      DisplayFormat = '0.000'
    end
    object taCMoveVNIN: TFloatField
      FieldName = 'VNIN'
      DisplayFormat = '0.000'
    end
    object taCMoveVNOUT: TFloatField
      FieldName = 'VNOUT'
      DisplayFormat = '0.000'
    end
    object taCMoveINV: TFloatField
      FieldName = 'INV'
      DisplayFormat = '0.000'
    end
    object taCMoveQREAL: TFloatField
      FieldName = 'QREAL'
      DisplayFormat = '0.000'
    end
    object taCMoveRE: TFloatField
      FieldName = 'RE'
      DisplayFormat = '0.000'
    end
  end
  object dstaCMove: TDataSource
    DataSet = taCMove
    Left = 80
    Top = 184
  end
  object quDocsInvSel: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHEADINV'
      'SET '
      '    DATEDOC = :DATEDOC,'
      '    NUMDOC = :NUMDOC,'
      '    IDSKL = :IDSKL,'
      '    IACTIVE = :IACTIVE,'
      '    ITYPE = :ITYPE,'
      '    SUM1 = :SUM1,'
      '    SUM11 = :SUM11,'
      '    SUM2 = :SUM2,'
      '    SUM21 = :SUM21,'
      '    SUM3 = :SUM3,'
      '    SUM31 = :SUM31,'
      '    COMMENT = :COMMENT,'
      '    SUMTARAR = :SUMTARAR,'
      '    SUMTARAF = :SUMTARAF,'
      '    SUMTARAD = :SUMTARAD,'
      '    IDETAL = :IDETAL'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHEADINV'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHEADINV('
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    IDSKL,'
      '    IACTIVE,'
      '    ITYPE,'
      '    SUM1,'
      '    SUM11,'
      '    SUM2,'
      '    SUM21,'
      '    SUM3,'
      '    SUM31,'
      '    COMMENT,'
      '    SUMTARAR,'
      '    SUMTARAF,'
      '    SUMTARAD,'
      '    IDETAL'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATEDOC,'
      '    :NUMDOC,'
      '    :IDSKL,'
      '    :IACTIVE,'
      '    :ITYPE,'
      '    :SUM1,'
      '    :SUM11,'
      '    :SUM2,'
      '    :SUM21,'
      '    :SUM3,'
      '    :SUM31,'
      '    :COMMENT,'
      '    :SUMTARAR,'
      '    :SUMTARAF,'
      '    :SUMTARAD,'
      '    :IDETAL'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT IH.ID,IH.DATEDOC,IH.NUMDOC,IH.IDSKL,IH.IACTIVE,IH.ITYPE,I' +
        'H.SUM1,'
      
        '       IH.SUM11,IH.SUM2,IH.SUM21,IH.SUM3,IH.SUM31,(IH.SUM2-IH.SU' +
        'M1) as SumDifIn,(IH.SUM21-IH.SUM11) as SumDifUch,(IH.SUM2-IH.SUM' +
        '3) as SumDifInF,'
      
        '       mh.NAMEMH,IH.COMMENT,IH.SUMTARAR,IH.SUMTARAF,IH.SUMTARAD,' +
        'IH.IDETAL'
      'FROM OF_DOCHEADINV IH'
      'left join OF_MH mh on mh.ID=IH.IDSKL'
      ''
      'Where(  IH.DATEDOC>=:DATEB and IH.DATEDOC<:DATEE'
      '     ) and (     IH.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT IH.ID,IH.DATEDOC,IH.NUMDOC,IH.IDSKL,IH.IACTIVE,IH.ITYPE,I' +
        'H.SUM1,'
      
        '       IH.SUM11,IH.SUM2,IH.SUM21,IH.SUM3,IH.SUM31,(IH.SUM2-IH.SU' +
        'M1) as SumDifIn,(IH.SUM21-IH.SUM11) as SumDifUch,(IH.SUM2-IH.SUM' +
        '3) as SumDifInF,'
      
        '       mh.NAMEMH,IH.COMMENT,IH.SUMTARAR,IH.SUMTARAF,IH.SUMTARAD,' +
        'IH.IDETAL'
      'FROM OF_DOCHEADINV IH'
      'left join OF_MH mh on mh.ID=IH.IDSKL'
      ''
      'Where IH.DATEDOC>=:DATEB and IH.DATEDOC<:DATEE'
      
        'and IH.IDSKL in (SELECT IDMH FROM RPERSONALMH where IDPERSON=:ID' +
        'PERSON)')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 440
    Top = 16
    poAskRecordCount = True
    object quDocsInvSelID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quDocsInvSelDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object quDocsInvSelNUMDOC: TFIBStringField
      FieldName = 'NUMDOC'
      Size = 15
      EmptyStrToNull = True
    end
    object quDocsInvSelIDSKL: TFIBIntegerField
      FieldName = 'IDSKL'
    end
    object quDocsInvSelNAMEMH: TFIBStringField
      FieldName = 'NAMEMH'
      Size = 100
      EmptyStrToNull = True
    end
    object quDocsInvSelIACTIVE: TFIBIntegerField
      FieldName = 'IACTIVE'
    end
    object quDocsInvSelITYPE: TFIBIntegerField
      FieldName = 'ITYPE'
    end
    object quDocsInvSelSUM1: TFIBFloatField
      FieldName = 'SUM1'
      DisplayFormat = '0.00'
    end
    object quDocsInvSelSUM11: TFIBFloatField
      FieldName = 'SUM11'
      DisplayFormat = '0.00'
    end
    object quDocsInvSelSUM2: TFIBFloatField
      FieldName = 'SUM2'
      DisplayFormat = '0.00'
    end
    object quDocsInvSelSUM21: TFIBFloatField
      FieldName = 'SUM21'
      DisplayFormat = '0.00'
    end
    object quDocsInvSelSUMDIFIN: TFIBFloatField
      FieldName = 'SUMDIFIN'
      DisplayFormat = '0.00'
    end
    object quDocsInvSelSUMDIFUCH: TFIBFloatField
      FieldName = 'SUMDIFUCH'
      DisplayFormat = '0.00'
    end
    object quDocsInvSelSUM3: TFIBFloatField
      FieldName = 'SUM3'
      DisplayFormat = '0.00'
    end
    object quDocsInvSelSUM31: TFIBFloatField
      FieldName = 'SUM31'
      DisplayFormat = '0.00'
    end
    object quDocsInvSelSUMDIFINF: TFIBFloatField
      FieldName = 'SUMDIFINF'
      DisplayFormat = '0.00'
    end
    object quDocsInvSelCOMMENT: TFIBStringField
      FieldName = 'COMMENT'
      Size = 150
      EmptyStrToNull = True
    end
    object quDocsInvSelSUMTARAR: TFIBFloatField
      FieldName = 'SUMTARAR'
      DisplayFormat = '0.00'
    end
    object quDocsInvSelSUMTARAF: TFIBFloatField
      FieldName = 'SUMTARAF'
      DisplayFormat = '0.00'
    end
    object quDocsInvSelSUMTARAD: TFIBFloatField
      FieldName = 'SUMTARAD'
      DisplayFormat = '0.00'
    end
    object quDocsInvSelIDETAL: TFIBSmallIntField
      FieldName = 'IDETAL'
    end
  end
  object dsquDocsInvSel: TDataSource
    DataSet = quDocsInvSel
    Left = 440
    Top = 72
  end
  object quDocsInvId: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHEADINV'
      'SET '
      '    DATEDOC = :DATEDOC,'
      '    NUMDOC = :NUMDOC,'
      '    IDSKL = :IDSKL,'
      '    IACTIVE = :IACTIVE,'
      '    ITYPE = :ITYPE,'
      '    SUM1 = :SUM1,'
      '    SUM11 = :SUM11,'
      '    SUM2 = :SUM2,'
      '    SUM21 = :SUM21,'
      '    SUM3 = :SUM3,'
      '    SUM31 = :SUM31,'
      '    COMMENT = :COMMENT,'
      '    SUMTARAR = :SUMTARAR,'
      '    SUMTARAF = :SUMTARAF,'
      '    SUMTARAD = :SUMTARAD,'
      '    IDETAL = :IDETAL'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHEADINV'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHEADINV('
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    IDSKL,'
      '    IACTIVE,'
      '    ITYPE,'
      '    SUM1,'
      '    SUM11,'
      '    SUM2,'
      '    SUM21,'
      '    SUM3,'
      '    SUM31,'
      '    COMMENT,'
      '    SUMTARAR,'
      '    SUMTARAF,'
      '    SUMTARAD,'
      '    IDETAL'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATEDOC,'
      '    :NUMDOC,'
      '    :IDSKL,'
      '    :IACTIVE,'
      '    :ITYPE,'
      '    :SUM1,'
      '    :SUM11,'
      '    :SUM2,'
      '    :SUM21,'
      '    :SUM3,'
      '    :SUM31,'
      '    :COMMENT,'
      '    :SUMTARAR,'
      '    :SUMTARAF,'
      '    :SUMTARAD,'
      '    :IDETAL'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT IH.ID,IH.DATEDOC,IH.NUMDOC,IH.IDSKL,IH.IACTIVE,IH.ITYPE,I' +
        'H.SUM1,IH.SUM11,'
      
        'IH.SUM2,IH.SUM21,IH.SUM3,IH.SUM31,IH.COMMENT,IH.SUMTARAR,IH.SUMT' +
        'ARAF,IH.SUMTARAD,IH.IDETAL'
      'FROM OF_DOCHEADINV IH'
      'Where(  IH.ID=:IDH'
      '     ) and (     IH.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT IH.ID,IH.DATEDOC,IH.NUMDOC,IH.IDSKL,IH.IACTIVE,IH.ITYPE,I' +
        'H.SUM1,IH.SUM11,'
      
        'IH.SUM2,IH.SUM21,IH.SUM3,IH.SUM31,IH.COMMENT,IH.SUMTARAR,IH.SUMT' +
        'ARAF,IH.SUMTARAD,IH.IDETAL'
      'FROM OF_DOCHEADINV IH'
      'Where IH.ID=:IDH')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 520
    Top = 72
    poAskRecordCount = True
    object quDocsInvIdID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quDocsInvIdDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object quDocsInvIdNUMDOC: TFIBStringField
      FieldName = 'NUMDOC'
      Size = 15
      EmptyStrToNull = True
    end
    object quDocsInvIdIDSKL: TFIBIntegerField
      FieldName = 'IDSKL'
    end
    object quDocsInvIdIACTIVE: TFIBIntegerField
      FieldName = 'IACTIVE'
    end
    object quDocsInvIdITYPE: TFIBIntegerField
      FieldName = 'ITYPE'
    end
    object quDocsInvIdSUM1: TFIBFloatField
      FieldName = 'SUM1'
    end
    object quDocsInvIdSUM11: TFIBFloatField
      FieldName = 'SUM11'
    end
    object quDocsInvIdSUM2: TFIBFloatField
      FieldName = 'SUM2'
    end
    object quDocsInvIdSUM21: TFIBFloatField
      FieldName = 'SUM21'
    end
    object quDocsInvIdSUM3: TFIBFloatField
      FieldName = 'SUM3'
    end
    object quDocsInvIdSUM31: TFIBFloatField
      FieldName = 'SUM31'
    end
    object quDocsInvIdCOMMENT: TFIBStringField
      FieldName = 'COMMENT'
      Size = 150
      EmptyStrToNull = True
    end
    object quDocsInvIdSUMTARAR: TFIBFloatField
      FieldName = 'SUMTARAR'
    end
    object quDocsInvIdSUMTARAF: TFIBFloatField
      FieldName = 'SUMTARAF'
    end
    object quDocsInvIdSUMTARAD: TFIBFloatField
      FieldName = 'SUMTARAD'
    end
    object quDocsInvIdIDETAL: TFIBSmallIntField
      FieldName = 'IDETAL'
    end
  end
  object quSpecInv: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCSPECINV'
      'SET '
      '    NUM = :NUM,'
      '    IDCARD = :IDCARD,'
      '    QUANT = :QUANT,'
      '    SUMIN = :SUMIN,'
      '    SUMUCH = :SUMUCH,'
      '    IDMESSURE = :IDMESSURE,'
      '    QUANTFACT = :QUANTFACT,'
      '    SUMINFACT = :SUMINFACT,'
      '    SUMUCHFACT = :SUMUCHFACT,'
      '    KM = :KM,'
      '    TCARD = :TCARD,'
      '    IDGROUP = :IDGROUP,'
      '    NOCALC = :NOCALC,'
      '    SUMINR = :SUMINR,'
      '    PRICER = :PRICER,'
      '    SUMIN0 = :SUMIN0,'
      '    SUMINFACT0 = :SUMINFACT0,'
      '    NDSPROC = :NDSPROC,'
      '    NDSSUM = :NDSSUM'
      'WHERE'
      '    IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCSPECINV'
      'WHERE'
      '        IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCSPECINV('
      '    IDHEAD,'
      '    ID,'
      '    NUM,'
      '    IDCARD,'
      '    QUANT,'
      '    SUMIN,'
      '    SUMUCH,'
      '    IDMESSURE,'
      '    QUANTFACT,'
      '    SUMINFACT,'
      '    SUMUCHFACT,'
      '    KM,'
      '    TCARD,'
      '    IDGROUP,'
      '    NOCALC,'
      '    SUMINR,'
      '    PRICER,'
      '    SUMIN0,'
      '    SUMINFACT0,'
      '    NDSPROC,'
      '    NDSSUM'
      ')'
      'VALUES('
      '    :IDHEAD,'
      '    :ID,'
      '    :NUM,'
      '    :IDCARD,'
      '    :QUANT,'
      '    :SUMIN,'
      '    :SUMUCH,'
      '    :IDMESSURE,'
      '    :QUANTFACT,'
      '    :SUMINFACT,'
      '    :SUMUCHFACT,'
      '    :KM,'
      '    :TCARD,'
      '    :IDGROUP,'
      '    :NOCALC,'
      '    :SUMINR,'
      '    :PRICER,'
      '    :SUMIN0,'
      '    :SUMINFACT0,'
      '    :NDSPROC,'
      '    :NDSSUM'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT s.IDHEAD,s.ID,s.NUM,s.IDCARD,s.QUANT,s.SUMIN,s.SUMUCH,s.I' +
        'DMESSURE,'
      
        's.QUANTFACT,s.SUMINFACT,s.SUMUCHFACT,s.KM,s.TCARD,s.IDGROUP,s.NO' +
        'CALC,s.SUMINR,s.PRICER,'
      's.SUMIN0,s.SUMINFACT0,s.NDSPROC,s.NDSSUM,'
      'me.NAMESHORT,cl.NAMECL,ca.NAME, ca.CATEGORY'
      'FROM OF_DOCSPECINV s'
      'left join OF_MESSUR me on me.ID=s.IDMESSURE'
      'left join OF_CLASSIF cl on cl.ID=s.IDGROUP'
      'left join of_cards ca on ca.ID=s.IDCARD'
      'where(  IDHEAD=:IDH'
      '     ) and (     S.IDHEAD = :OLD_IDHEAD'
      '    and S.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT s.IDHEAD,s.ID,s.NUM,s.IDCARD,s.QUANT,s.SUMIN,s.SUMUCH,s.I' +
        'DMESSURE,'
      
        's.QUANTFACT,s.SUMINFACT,s.SUMUCHFACT,s.KM,s.TCARD,s.NOCALC,s.SUM' +
        'INR,s.PRICER,'
      's.SUMIN0,s.SUMINFACT0,s.NDSPROC,s.NDSSUM,'
      
        'me.NAMESHORT,cl.NAMECL,ca.NAME, ca.CATEGORY, ca.PARENT as IDGROU' +
        'P '
      'FROM OF_DOCSPECINV s'
      'left join OF_MESSUR me on me.ID=s.IDMESSURE'
      'left join OF_CLASSIF cl on cl.ID=s.IDGROUP'
      'left join of_cards ca on ca.ID=s.IDCARD'
      'where IDHEAD=:IDH')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 520
    Top = 16
    poAskRecordCount = True
    object quSpecInvIDHEAD: TFIBIntegerField
      FieldName = 'IDHEAD'
    end
    object quSpecInvID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quSpecInvNUM: TFIBIntegerField
      FieldName = 'NUM'
    end
    object quSpecInvIDCARD: TFIBIntegerField
      FieldName = 'IDCARD'
    end
    object quSpecInvQUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quSpecInvSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quSpecInvSUMUCH: TFIBFloatField
      FieldName = 'SUMUCH'
    end
    object quSpecInvIDMESSURE: TFIBIntegerField
      FieldName = 'IDMESSURE'
    end
    object quSpecInvQUANTFACT: TFIBFloatField
      FieldName = 'QUANTFACT'
    end
    object quSpecInvSUMINFACT: TFIBFloatField
      FieldName = 'SUMINFACT'
    end
    object quSpecInvSUMUCHFACT: TFIBFloatField
      FieldName = 'SUMUCHFACT'
    end
    object quSpecInvKM: TFIBFloatField
      FieldName = 'KM'
    end
    object quSpecInvTCARD: TFIBSmallIntField
      FieldName = 'TCARD'
    end
    object quSpecInvIDGROUP: TFIBIntegerField
      FieldName = 'IDGROUP'
    end
    object quSpecInvNAMESHORT: TFIBStringField
      FieldName = 'NAMESHORT'
      Size = 50
      EmptyStrToNull = True
    end
    object quSpecInvNAMECL: TFIBStringField
      FieldName = 'NAMECL'
      Size = 150
      EmptyStrToNull = True
    end
    object quSpecInvNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 200
      EmptyStrToNull = True
    end
    object quSpecInvNOCALC: TFIBSmallIntField
      FieldName = 'NOCALC'
    end
    object quSpecInvSUMINR: TFIBFloatField
      FieldName = 'SUMINR'
    end
    object quSpecInvPRICER: TFIBFloatField
      FieldName = 'PRICER'
    end
    object quSpecInvCATEGORY: TFIBIntegerField
      FieldName = 'CATEGORY'
    end
    object quSpecInvSUMIN0: TFIBFloatField
      FieldName = 'SUMIN0'
    end
    object quSpecInvSUMINFACT0: TFIBFloatField
      FieldName = 'SUMINFACT0'
    end
    object quSpecInvNDSPROC: TFIBFloatField
      FieldName = 'NDSPROC'
    end
    object quSpecInvNDSSUM: TFIBFloatField
      FieldName = 'NDSSUM'
    end
  end
  object quSpecInvC: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCSPECINVC'
      'SET '
      '    NUM = :NUM,'
      '    IDCARD = :IDCARD,'
      '    QUANT = :QUANT,'
      '    SUMIN = :SUMIN,'
      '    SUMINR = :SUMINR,'
      '    SUMUCH = :SUMUCH,'
      '    IDMESSURE = :IDMESSURE,'
      '    QUANTFACT = :QUANTFACT,'
      '    SUMINFACT = :SUMINFACT,'
      '    SUMUCHFACT = :SUMUCHFACT,'
      '    KM = :KM,'
      '    TCARD = :TCARD,'
      '    SUMIN0 = :SUMIN0,'
      '    SUMINFACT0 = :SUMINFACT0,'
      '    NDSPROC = :NDSPROC,'
      '    NDSSUM = :NDSSUM'
      'WHERE'
      '    IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCSPECINVC'
      'WHERE'
      '        IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCSPECINVC('
      '    IDHEAD,'
      '    ID,'
      '    NUM,'
      '    IDCARD,'
      '    QUANT,'
      '    SUMIN,'
      '    SUMINR,'
      '    SUMUCH,'
      '    IDMESSURE,'
      '    QUANTFACT,'
      '    SUMINFACT,'
      '    SUMUCHFACT,'
      '    KM,'
      '    TCARD,'
      '    SUMIN0,'
      '    SUMINFACT0,'
      '    NDSPROC,'
      '    NDSSUM'
      ')'
      'VALUES('
      '    :IDHEAD,'
      '    :ID,'
      '    :NUM,'
      '    :IDCARD,'
      '    :QUANT,'
      '    :SUMIN,'
      '    :SUMINR,'
      '    :SUMUCH,'
      '    :IDMESSURE,'
      '    :QUANTFACT,'
      '    :SUMINFACT,'
      '    :SUMUCHFACT,'
      '    :KM,'
      '    :TCARD,'
      '    :SUMIN0,'
      '    :SUMINFACT0,'
      '    :NDSPROC,'
      '    :NDSSUM'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT s.IDHEAD,s.ID,s.NUM,s.IDCARD,s.QUANT,s.SUMIN,s.SUMINR,s.S' +
        'UMUCH,s.IDMESSURE,'
      's.QUANTFACT,s.SUMINFACT,s.SUMUCHFACT,s.KM,s.TCARD,'
      's.SUMIN0,s.SUMINFACT0,S.NDSPROC,s.NDSSUM,'
      'me.NAMESHORT,cl.NAMECL,ca.NAME,ca.CATEGORY,ca.PARENT as IdGroup'
      'FROM OF_DOCSPECINVC s'
      'left join OF_MESSUR me on me.ID=s.IDMESSURE'
      'left join OF_CLASSIF cl on cl.ID=s.IDGROUP'
      'left join of_cards ca on ca.ID=s.IDCARD'
      'where(  IDHEAD=:IDH'
      '     ) and (     S.IDHEAD = :OLD_IDHEAD'
      '    and S.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT s.IDHEAD,s.ID,s.NUM,s.IDCARD,s.QUANT,s.SUMIN,s.SUMINR,s.S' +
        'UMUCH,s.IDMESSURE,'
      's.QUANTFACT,s.SUMINFACT,s.SUMUCHFACT,s.KM,s.TCARD,'
      's.SUMIN0,s.SUMINFACT0,S.NDSPROC,s.NDSSUM,'
      'me.NAMESHORT,cl.NAMECL,ca.NAME,ca.CATEGORY,ca.PARENT as IdGroup'
      'FROM OF_DOCSPECINVC s'
      'left join of_cards ca on ca.ID=s.IDCARD'
      'left join OF_MESSUR me on me.ID=s.IDMESSURE'
      'left join OF_CLASSIF cl on cl.ID=ca.PARENT'
      ''
      'where IDHEAD=:IDH')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 584
    Top = 16
    poAskRecordCount = True
    object quSpecInvCIDHEAD: TFIBIntegerField
      FieldName = 'IDHEAD'
    end
    object quSpecInvCID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quSpecInvCNUM: TFIBIntegerField
      FieldName = 'NUM'
    end
    object quSpecInvCIDCARD: TFIBIntegerField
      FieldName = 'IDCARD'
    end
    object quSpecInvCQUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quSpecInvCSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quSpecInvCSUMUCH: TFIBFloatField
      FieldName = 'SUMUCH'
    end
    object quSpecInvCIDMESSURE: TFIBIntegerField
      FieldName = 'IDMESSURE'
    end
    object quSpecInvCQUANTFACT: TFIBFloatField
      FieldName = 'QUANTFACT'
    end
    object quSpecInvCSUMINFACT: TFIBFloatField
      FieldName = 'SUMINFACT'
    end
    object quSpecInvCSUMUCHFACT: TFIBFloatField
      FieldName = 'SUMUCHFACT'
    end
    object quSpecInvCKM: TFIBFloatField
      FieldName = 'KM'
    end
    object quSpecInvCTCARD: TFIBSmallIntField
      FieldName = 'TCARD'
    end
    object quSpecInvCIDGROUP: TFIBIntegerField
      FieldName = 'IDGROUP'
    end
    object quSpecInvCNAMESHORT: TFIBStringField
      FieldName = 'NAMESHORT'
      Size = 50
      EmptyStrToNull = True
    end
    object quSpecInvCNAMECL: TFIBStringField
      FieldName = 'NAMECL'
      Size = 150
      EmptyStrToNull = True
    end
    object quSpecInvCNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 200
      EmptyStrToNull = True
    end
    object quSpecInvCSUMINR: TFIBFloatField
      FieldName = 'SUMINR'
    end
    object quSpecInvCCATEGORY: TFIBIntegerField
      FieldName = 'CATEGORY'
    end
    object quSpecInvCSUMIN0: TFIBFloatField
      FieldName = 'SUMIN0'
    end
    object quSpecInvCSUMINFACT0: TFIBFloatField
      FieldName = 'SUMINFACT0'
    end
    object quSpecInvCNDSPROC: TFIBFloatField
      FieldName = 'NDSPROC'
    end
    object quSpecInvCNDSSUM: TFIBFloatField
      FieldName = 'NDSSUM'
    end
  end
  object trSelRep: TpFIBTransaction
    DefaultDatabase = dmO.OfficeRnDb
    TimeoutAction = TARollback
    Left = 264
    Top = 136
  end
  object quDocOutBPrint: TpFIBDataSet
    SelectSQL.Strings = (
      
        'SELECT vr.IDB,vr.CODEB,vr.QUANT,vr.SUMOUT,vr.IDSKL,vr.SUMIN,ds.N' +
        'AMEB,ds.PRICER,ca.NAME,ME.NAMESHORT'
      'FROM OF_VREALB_DOC vr'
      
        'left join of_docspecoutb ds on ((ds.IDHEAD=:IDH)and(ds.IDCARD=vr' +
        '.CODEB)and(ds.ID=vr.IDB))'
      'left join of_Cards ca on ca.ID=ds.IDCARD'
      'left join OF_MESSUR ME on ME.ID=ds.IDM')
    Transaction = trSelRep
    Database = dmO.OfficeRnDb
    Left = 384
    Top = 192
    poAskRecordCount = True
    object quDocOutBPrintIDB: TFIBIntegerField
      FieldName = 'IDB'
    end
    object quDocOutBPrintCODEB: TFIBIntegerField
      FieldName = 'CODEB'
    end
    object quDocOutBPrintQUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quDocOutBPrintSUMOUT: TFIBFloatField
      FieldName = 'SUMOUT'
    end
    object quDocOutBPrintIDSKL: TFIBIntegerField
      FieldName = 'IDSKL'
    end
    object quDocOutBPrintSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quDocOutBPrintNAMEB: TFIBStringField
      FieldName = 'NAMEB'
      Size = 100
      EmptyStrToNull = True
    end
    object quDocOutBPrintPRICER: TFIBFloatField
      FieldName = 'PRICER'
    end
    object quDocOutBPrintNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 200
      EmptyStrToNull = True
    end
    object quDocOutBPrintNAMESHORT: TFIBStringField
      FieldName = 'NAMESHORT'
      Size = 50
      EmptyStrToNull = True
    end
  end
  object quSelInSum: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT IDATE,QPART,PRICEIN'
      'FROM OF_PARTIN'
      
        'where IDATE>=:DATEB and IDATE<:DATEE and IDSTORE=:IDSKL and ARTI' +
        'CUL=:IDGOOD and DTYPE=:ITYPE'
      'Order by IDATE DESC')
    Transaction = trSelRep
    Database = dmO.OfficeRnDb
    Left = 432
    Top = 136
    object quSelInSumIDATE: TFIBIntegerField
      FieldName = 'IDATE'
    end
    object quSelInSumQPART: TFIBFloatField
      FieldName = 'QPART'
    end
    object quSelInSumPRICEIN: TFIBFloatField
      FieldName = 'PRICEIN'
    end
  end
  object quSelNamePos: TpFIBDataSet
    SelectSQL.Strings = (
      
        'SELECT cl.NAMECL,ca.PARENT,ca.NAME,ME.NAMESHORT as SM,ME.KOEF,ca' +
        '.IMESSURE'
      'FROM OF_CARDS ca'
      'left join of_classif cl on cl.ID=ca.PARENT'
      'left join OF_MESSUR ME on ME.ID=ca.IMESSURE'
      'where ca.ID=:IDCARD')
    Transaction = trSelRep
    Database = dmO.OfficeRnDb
    Left = 504
    Top = 136
    poAskRecordCount = True
    object quSelNamePosNAMECL: TFIBStringField
      FieldName = 'NAMECL'
      Size = 150
      EmptyStrToNull = True
    end
    object quSelNamePosPARENT: TFIBIntegerField
      FieldName = 'PARENT'
    end
    object quSelNamePosNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 200
      EmptyStrToNull = True
    end
    object quSelNamePosSM: TFIBStringField
      FieldName = 'SM'
      Size = 50
      EmptyStrToNull = True
    end
    object quSelNamePosKOEF: TFIBFloatField
      FieldName = 'KOEF'
    end
    object quSelNamePosIMESSURE: TFIBIntegerField
      FieldName = 'IMESSURE'
    end
  end
  object quSelOutSum: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT IDDATE,QUANT,PRICEIN,SUMOUT'
      'FROM OF_PARTOUT'
      
        'where IDDATE>=:DATEB and IDDATE<=:DATEE and IDSTORE=:IDSKL and A' +
        'RTICUL=:IDGOOD and DTYPE=1'
      'order by IDDATE desc')
    Transaction = trSelRep
    Database = dmO.OfficeRnDb
    Left = 464
    Top = 200
    poAskRecordCount = True
    object quSelOutSumIDDATE: TFIBIntegerField
      FieldName = 'IDDATE'
    end
    object quSelOutSumQUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quSelOutSumPRICEIN: TFIBFloatField
      FieldName = 'PRICEIN'
    end
    object quSelOutSumSUMOUT: TFIBFloatField
      FieldName = 'SUMOUT'
    end
  end
  object quSelNameCl: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT ca.PARENT, cl.NAMECL FROM OF_CARDS ca'
      'left join OF_Classif cl on cl.ID=ca.PARENT'
      'where ca.ID=:IDC')
    Transaction = dmO.trSelect
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    Left = 576
    Top = 136
    poAskRecordCount = True
    object quSelNameClPARENT: TFIBIntegerField
      FieldName = 'PARENT'
    end
    object quSelNameClNAMECL: TFIBStringField
      FieldName = 'NAMECL'
      Size = 150
      EmptyStrToNull = True
    end
  end
  object quSpecInvBC: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCSPECINVBC'
      'SET '
      '    CODEB = :CODEB,'
      '    NAMEB = :NAMEB,'
      '    QUANT = :QUANT,'
      '    PRICEOUT = :PRICEOUT,'
      '    SUMOUT = :SUMOUT,'
      '    IDCARD = :IDCARD,'
      '    NAMEC = :NAMEC,'
      '    QUANTC = :QUANTC,'
      '    PRICEIN = :PRICEIN,'
      '    SUMIN = :SUMIN,'
      '    IM = :IM,'
      '    SM = :SM,'
      '    SB = :SB,'
      '    PRICEIN0 = :PRICEIN0,'
      '    SUMIN0 = :SUMIN0'
      'WHERE'
      '    IDHEAD = :OLD_IDHEAD'
      '    and IDB = :OLD_IDB'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCSPECINVBC'
      'WHERE'
      '        IDHEAD = :OLD_IDHEAD'
      '    and IDB = :OLD_IDB'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCSPECINVBC('
      '    IDHEAD,'
      '    IDB,'
      '    ID,'
      '    CODEB,'
      '    NAMEB,'
      '    QUANT,'
      '    PRICEOUT,'
      '    SUMOUT,'
      '    IDCARD,'
      '    NAMEC,'
      '    QUANTC,'
      '    PRICEIN,'
      '    SUMIN,'
      '    IM,'
      '    SM,'
      '    SB,'
      '    PRICEIN0,'
      '    SUMIN0'
      ')'
      'VALUES('
      '    :IDHEAD,'
      '    :IDB,'
      '    :ID,'
      '    :CODEB,'
      '    :NAMEB,'
      '    :QUANT,'
      '    :PRICEOUT,'
      '    :SUMOUT,'
      '    :IDCARD,'
      '    :NAMEC,'
      '    :QUANTC,'
      '    :PRICEIN,'
      '    :SUMIN,'
      '    :IM,'
      '    :SM,'
      '    :SB,'
      '    :PRICEIN0,'
      '    :SUMIN0'
      ')')
    RefreshSQL.Strings = (
      'SELECT IDHEAD,IDB,ID,CODEB,NAMEB,QUANT,PRICEOUT,SUMOUT,'
      
        '       IDCARD,NAMEC,QUANTC,PRICEIN,SUMIN,IM,SM,SB,PRICEIN0,SUMIN' +
        '0'
      'FROM OF_DOCSPECINVBC'
      'where(  IDHEAD=:IDH'
      '     ) and (     OF_DOCSPECINVBC.IDHEAD = :OLD_IDHEAD'
      '    and OF_DOCSPECINVBC.IDB = :OLD_IDB'
      '    and OF_DOCSPECINVBC.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT IDHEAD,IDB,ID,CODEB,NAMEB,QUANT,PRICEOUT,SUMOUT,'
      
        '       IDCARD,NAMEC,QUANTC,PRICEIN,SUMIN,IM,SM,SB,PRICEIN0,SUMIN' +
        '0'
      'FROM OF_DOCSPECINVBC'
      'where IDHEAD=:IDH')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 652
    Top = 16
    poAskRecordCount = True
    object quSpecInvBCIDHEAD: TFIBIntegerField
      FieldName = 'IDHEAD'
    end
    object quSpecInvBCIDB: TFIBIntegerField
      FieldName = 'IDB'
    end
    object quSpecInvBCID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quSpecInvBCCODEB: TFIBIntegerField
      FieldName = 'CODEB'
    end
    object quSpecInvBCNAMEB: TFIBStringField
      FieldName = 'NAMEB'
      Size = 100
      EmptyStrToNull = True
    end
    object quSpecInvBCQUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quSpecInvBCPRICEOUT: TFIBFloatField
      FieldName = 'PRICEOUT'
    end
    object quSpecInvBCSUMOUT: TFIBFloatField
      FieldName = 'SUMOUT'
    end
    object quSpecInvBCIDCARD: TFIBIntegerField
      FieldName = 'IDCARD'
    end
    object quSpecInvBCNAMEC: TFIBStringField
      FieldName = 'NAMEC'
      Size = 100
      EmptyStrToNull = True
    end
    object quSpecInvBCQUANTC: TFIBFloatField
      FieldName = 'QUANTC'
    end
    object quSpecInvBCPRICEIN: TFIBFloatField
      FieldName = 'PRICEIN'
    end
    object quSpecInvBCSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quSpecInvBCIM: TFIBIntegerField
      FieldName = 'IM'
    end
    object quSpecInvBCSM: TFIBStringField
      FieldName = 'SM'
      EmptyStrToNull = True
    end
    object quSpecInvBCSB: TFIBStringField
      FieldName = 'SB'
      EmptyStrToNull = True
    end
    object quSpecInvBCPRICEIN0: TFIBFloatField
      FieldName = 'PRICEIN0'
    end
    object quSpecInvBCSUMIN0: TFIBFloatField
      FieldName = 'SUMIN0'
    end
  end
  object quDocsCompl: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHEADCOMPL'
      'SET '
      '    DATEDOC = :DATEDOC,'
      '    NUMDOC = :NUMDOC,'
      '    IDSKL = :IDSKL,'
      '    SUMIN = :SUMIN,'
      '    SUMUCH = :SUMUCH,'
      '    SUMTAR = :SUMTAR,'
      '    PROCNAC = :PROCNAC,'
      '    IACTIVE = :IACTIVE,'
      '    OPER = :OPER,'
      '    IDSKLTO = :IDSKLTO,'
      '    IDFROM = :IDFROM,'
      '    TOREAL = :TOREAL'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHEADCOMPL'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHEADCOMPL('
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    IDSKL,'
      '    SUMIN,'
      '    SUMUCH,'
      '    SUMTAR,'
      '    PROCNAC,'
      '    IACTIVE,'
      '    OPER,'
      '    IDSKLTO,'
      '    IDFROM,'
      '    TOREAL'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATEDOC,'
      '    :NUMDOC,'
      '    :IDSKL,'
      '    :SUMIN,'
      '    :SUMUCH,'
      '    :SUMTAR,'
      '    :PROCNAC,'
      '    :IACTIVE,'
      '    :OPER,'
      '    :IDSKLTO,'
      '    :IDFROM,'
      '    :TOREAL'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT dh.ID,dh.DATEDOC,dh.NUMDOC,dh.IDSKL,dh.SUMIN,dh.SUMUCH,dh' +
        '.SUMTAR,'
      'dh.PROCNAC,dh.IACTIVE,dh.OPER,dh.IDSKLTO,dh.IDFROM,dh.TOREAL,'
      'mh.NAMEMH as NAMEMH,'
      'mh1.NAMEMH as NAMEMH'
      'FROM OF_DOCHEADCOMPL dh'
      'left join OF_MH mh on mh.ID=dh.IDSKL'
      'left join OF_MH mh1 on mh1.ID=dh.IDSKLTO'
      'Where(  dh.DATEDOC>=:DATEB and dh.DATEDOC<:DATEE'
      '     ) and (     DH.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT dh.ID,dh.DATEDOC,dh.NUMDOC,dh.IDSKL,dh.SUMIN,dh.SUMUCH,dh' +
        '.SUMTAR,'
      'dh.PROCNAC,dh.IACTIVE,dh.OPER,dh.IDSKLTO,dh.IDFROM,dh.TOREAL,'
      'mh.NAMEMH as NAMEMH,'
      'mh1.NAMEMH as NAMEMH1'
      'FROM OF_DOCHEADCOMPL dh'
      'left join OF_MH mh on mh.ID=dh.IDSKL'
      'left join OF_MH mh1 on mh1.ID=dh.IDSKLTO'
      'Where dh.DATEDOC>=:DATEB and dh.DATEDOC<=:DATEE'
      'and'
      
        '( dh.IDSKL in (SELECT IDMH FROM RPERSONALMH where IDPERSON=:IDPE' +
        'RSON)'
      'or'
      
        '  dh.IDSKLTO in (SELECT IDMH FROM RPERSONALMH where IDPERSON=:ID' +
        'PERSON1)'
      ')')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 552
    Top = 200
    poAskRecordCount = True
    object quDocsComplID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quDocsComplDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object quDocsComplNUMDOC: TFIBStringField
      FieldName = 'NUMDOC'
      Size = 15
      EmptyStrToNull = True
    end
    object quDocsComplIDSKL: TFIBIntegerField
      FieldName = 'IDSKL'
    end
    object quDocsComplSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
      DisplayFormat = '0.00'
    end
    object quDocsComplSUMUCH: TFIBFloatField
      FieldName = 'SUMUCH'
      DisplayFormat = '0.00'
    end
    object quDocsComplSUMTAR: TFIBFloatField
      FieldName = 'SUMTAR'
      DisplayFormat = '0.00'
    end
    object quDocsComplPROCNAC: TFIBFloatField
      FieldName = 'PROCNAC'
      DisplayFormat = '0.0'
    end
    object quDocsComplIACTIVE: TFIBIntegerField
      FieldName = 'IACTIVE'
    end
    object quDocsComplOPER: TFIBStringField
      FieldName = 'OPER'
      EmptyStrToNull = True
    end
    object quDocsComplNAMEMH: TFIBStringField
      FieldName = 'NAMEMH'
      Size = 100
      EmptyStrToNull = True
    end
    object quDocsComplIDSKLTO: TFIBIntegerField
      FieldName = 'IDSKLTO'
    end
    object quDocsComplIDFROM: TFIBIntegerField
      FieldName = 'IDFROM'
    end
    object quDocsComplTOREAL: TFIBSmallIntField
      FieldName = 'TOREAL'
    end
    object quDocsComplNAMEMH1: TFIBStringField
      FieldName = 'NAMEMH1'
      Size = 100
      EmptyStrToNull = True
    end
  end
  object dsquDocsCompl: TDataSource
    DataSet = quDocsCompl
    Left = 556
    Top = 252
  end
  object quDocsComlRec: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHEADCOMPL'
      'SET '
      '    DATEDOC = :DATEDOC,'
      '    NUMDOC = :NUMDOC,'
      '    IDSKL = :IDSKL,'
      '    SUMIN = :SUMIN,'
      '    SUMUCH = :SUMUCH,'
      '    SUMTAR = :SUMTAR,'
      '    PROCNAC = :PROCNAC,'
      '    IACTIVE = :IACTIVE,'
      '    OPER = :OPER,'
      '    IDFROM = :IDFROM,'
      '    TOREAL = :TOREAL,'
      '    IDSKLTO = :IDSKLTO'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHEADCOMPL'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHEADCOMPL('
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    IDSKL,'
      '    SUMIN,'
      '    SUMUCH,'
      '    SUMTAR,'
      '    PROCNAC,'
      '    IACTIVE,'
      '    OPER,'
      '    IDFROM,'
      '    TOREAL,'
      '    IDSKLTO'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATEDOC,'
      '    :NUMDOC,'
      '    :IDSKL,'
      '    :SUMIN,'
      '    :SUMUCH,'
      '    :SUMTAR,'
      '    :PROCNAC,'
      '    :IACTIVE,'
      '    :OPER,'
      '    :IDFROM,'
      '    :TOREAL,'
      '    :IDSKLTO'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT ID,DATEDOC,NUMDOC,IDSKL,SUMIN,SUMUCH,SUMTAR,PROCNAC,IACTI' +
        'VE,OPER,IDFROM,TOREAL,IDSKLTO'
      'FROM OF_DOCHEADCOMPL'
      'Where(  ID=:IDH'
      '     ) and (     OF_DOCHEADCOMPL.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT ID,DATEDOC,NUMDOC,IDSKL,SUMIN,SUMUCH,SUMTAR,PROCNAC,IACTI' +
        'VE,OPER,IDFROM,TOREAL,IDSKLTO'
      'FROM OF_DOCHEADCOMPL'
      'Where ID=:IDH')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 644
    Top = 200
    poAskRecordCount = True
    object quDocsComlRecID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quDocsComlRecDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object quDocsComlRecNUMDOC: TFIBStringField
      FieldName = 'NUMDOC'
      Size = 15
      EmptyStrToNull = True
    end
    object quDocsComlRecIDSKL: TFIBIntegerField
      FieldName = 'IDSKL'
    end
    object quDocsComlRecSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quDocsComlRecSUMUCH: TFIBFloatField
      FieldName = 'SUMUCH'
    end
    object quDocsComlRecSUMTAR: TFIBFloatField
      FieldName = 'SUMTAR'
    end
    object quDocsComlRecPROCNAC: TFIBFloatField
      FieldName = 'PROCNAC'
    end
    object quDocsComlRecIACTIVE: TFIBIntegerField
      FieldName = 'IACTIVE'
    end
    object quDocsComlRecOPER: TFIBStringField
      FieldName = 'OPER'
      EmptyStrToNull = True
    end
    object quDocsComlRecIDFROM: TFIBIntegerField
      FieldName = 'IDFROM'
    end
    object quDocsComlRecTOREAL: TFIBSmallIntField
      FieldName = 'TOREAL'
    end
    object quDocsComlRecIDSKLTO: TFIBIntegerField
      FieldName = 'IDSKLTO'
    end
  end
  object quSpecCompl: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCSPECCOMPLB'
      'SET '
      '    IDCARD = :IDCARD,'
      '    QUANT = :QUANT,'
      '    IDM = :IDM,'
      '    KM = :KM,'
      '    PRICEIN0 = :PRICEIN0,'
      '    SUMIN0 = :SUMIN0,'
      '    PRICEIN = :PRICEIN,'
      '    SUMIN = :SUMIN,'
      '    PRICEINUCH = :PRICEINUCH,'
      '    SUMINUCH = :SUMINUCH,'
      '    TCARD = :TCARD'
      'WHERE'
      '    IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCSPECCOMPLB'
      'WHERE'
      '        IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCSPECCOMPLB('
      '    IDHEAD,'
      '    ID,'
      '    IDCARD,'
      '    QUANT,'
      '    IDM,'
      '    KM,'
      '    PRICEIN0,'
      '    SUMIN0,'
      '    PRICEIN,'
      '    SUMIN,'
      '    PRICEINUCH,'
      '    SUMINUCH,'
      '    TCARD'
      ')'
      'VALUES('
      '    :IDHEAD,'
      '    :ID,'
      '    :IDCARD,'
      '    :QUANT,'
      '    :IDM,'
      '    :KM,'
      '    :PRICEIN0,'
      '    :SUMIN0,'
      '    :PRICEIN,'
      '    :SUMIN,'
      '    :PRICEINUCH,'
      '    :SUMINUCH,'
      '    :TCARD'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT s.IDHEAD,s.ID,s.IDCARD,s.QUANT,s.IDM,s.KM,s.PRICEIN0,s.SU' +
        'MIN0,s.PRICEIN,s.SUMIN,s.PRICEINUCH,s.SUMINUCH,s.TCARD,'
      'me.NAMESHORT,ca.NAME'
      'FROM OF_DOCSPECCOMPLB s'
      'left join OF_MESSUR me on me.ID=s.IDM'
      'left join of_cards ca on ca.ID=s.IDCARD'
      'where(  s.IDHEAD=:IDH'
      '     ) and (     S.IDHEAD = :OLD_IDHEAD'
      '    and S.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT s.IDHEAD,s.ID,s.IDCARD,s.QUANT,s.IDM,s.KM,s.PRICEIN0,s.SU' +
        'MIN0,s.PRICEIN,s.SUMIN,s.PRICEINUCH,s.SUMINUCH,s.TCARD,'
      'me.NAMESHORT,ca.NAME'
      'FROM OF_DOCSPECCOMPLB s'
      'left join OF_MESSUR me on me.ID=s.IDM'
      'left join of_cards ca on ca.ID=s.IDCARD'
      'where s.IDHEAD=:IDH'
      'order by s.ID')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 780
    Top = 124
    poAskRecordCount = True
    object quSpecComplIDHEAD: TFIBIntegerField
      FieldName = 'IDHEAD'
    end
    object quSpecComplID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quSpecComplIDCARD: TFIBIntegerField
      FieldName = 'IDCARD'
    end
    object quSpecComplQUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quSpecComplIDM: TFIBIntegerField
      FieldName = 'IDM'
    end
    object quSpecComplKM: TFIBFloatField
      FieldName = 'KM'
    end
    object quSpecComplPRICEIN: TFIBFloatField
      FieldName = 'PRICEIN'
    end
    object quSpecComplSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quSpecComplPRICEINUCH: TFIBFloatField
      FieldName = 'PRICEINUCH'
    end
    object quSpecComplSUMINUCH: TFIBFloatField
      FieldName = 'SUMINUCH'
    end
    object quSpecComplTCARD: TFIBSmallIntField
      FieldName = 'TCARD'
    end
    object quSpecComplNAMESHORT: TFIBStringField
      FieldName = 'NAMESHORT'
      Size = 50
      EmptyStrToNull = True
    end
    object quSpecComplNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 200
      EmptyStrToNull = True
    end
    object quSpecComplPRICEIN0: TFIBFloatField
      FieldName = 'PRICEIN0'
    end
    object quSpecComplSUMIN0: TFIBFloatField
      FieldName = 'SUMIN0'
    end
  end
  object quSpecComplC: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCSPECCOMPLC'
      'SET '
      '    IDCARD = :IDCARD,'
      '    QUANT = :QUANT,'
      '    IDM = :IDM,'
      '    KM = :KM,'
      '    PRICEIN0 = :PRICEIN0,'
      '    SUMIN0 = :SUMIN0,'
      '    PRICEIN = :PRICEIN,'
      '    SUMIN = :SUMIN,'
      '    PRICEINUCH = :PRICEINUCH,'
      '    SUMINUCH = :SUMINUCH'
      'WHERE'
      '    IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCSPECCOMPLC'
      'WHERE'
      '        IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCSPECCOMPLC('
      '    IDHEAD,'
      '    ID,'
      '    IDCARD,'
      '    QUANT,'
      '    IDM,'
      '    KM,'
      '    PRICEIN0,'
      '    SUMIN0,'
      '    PRICEIN,'
      '    SUMIN,'
      '    PRICEINUCH,'
      '    SUMINUCH'
      ')'
      'VALUES('
      '    :IDHEAD,'
      '    :ID,'
      '    :IDCARD,'
      '    :QUANT,'
      '    :IDM,'
      '    :KM,'
      '    :PRICEIN0,'
      '    :SUMIN0,'
      '    :PRICEIN,'
      '    :SUMIN,'
      '    :PRICEINUCH,'
      '    :SUMINUCH'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT s.IDHEAD,s.ID,s.IDCARD,s.QUANT,s.IDM,s.KM,s.PRICEIN0,s.SU' +
        'MIN0,s.PRICEIN,s.SUMIN,s.PRICEINUCH,s.SUMINUCH,'
      'me.NAMESHORT,ca.NAME'
      'FROM OF_DOCSPECCOMPLC s'
      'left join OF_MESSUR me on me.ID=s.IDM'
      'left join of_cards ca on ca.ID=s.IDCARD'
      'where(  s.IDHEAD=:IDH'
      '     ) and (     S.IDHEAD = :OLD_IDHEAD'
      '    and S.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT s.IDHEAD,s.ID,s.IDCARD,s.QUANT,s.IDM,s.KM,s.PRICEIN0,s.SU' +
        'MIN0,s.PRICEIN,s.SUMIN,s.PRICEINUCH,s.SUMINUCH,'
      'me.NAMESHORT,ca.NAME'
      'FROM OF_DOCSPECCOMPLC s'
      'left join OF_MESSUR me on me.ID=s.IDM'
      'left join of_cards ca on ca.ID=s.IDCARD'
      'where s.IDHEAD=:IDH')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 780
    Top = 224
    poAskRecordCount = True
    object quSpecComplCIDHEAD: TFIBIntegerField
      FieldName = 'IDHEAD'
    end
    object quSpecComplCID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quSpecComplCIDCARD: TFIBIntegerField
      FieldName = 'IDCARD'
    end
    object quSpecComplCQUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quSpecComplCIDM: TFIBIntegerField
      FieldName = 'IDM'
    end
    object quSpecComplCKM: TFIBFloatField
      FieldName = 'KM'
    end
    object quSpecComplCPRICEIN: TFIBFloatField
      FieldName = 'PRICEIN'
    end
    object quSpecComplCSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quSpecComplCPRICEINUCH: TFIBFloatField
      FieldName = 'PRICEINUCH'
    end
    object quSpecComplCSUMINUCH: TFIBFloatField
      FieldName = 'SUMINUCH'
    end
    object quSpecComplCNAMESHORT: TFIBStringField
      FieldName = 'NAMESHORT'
      Size = 50
      EmptyStrToNull = True
    end
    object quSpecComplCNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 200
      EmptyStrToNull = True
    end
    object quSpecComplCPRICEIN0: TFIBFloatField
      FieldName = 'PRICEIN0'
    end
    object quSpecComplCSUMIN0: TFIBFloatField
      FieldName = 'SUMIN0'
    end
  end
  object quSpecComplCB: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCSPECCOMPLCB'
      'SET '
      '    CODEB = :CODEB,'
      '    NAMEB = :NAMEB,'
      '    QUANT = :QUANT,'
      '    IDCARD = :IDCARD,'
      '    NAMEC = :NAMEC,'
      '    QUANTC = :QUANTC,'
      '    PRICEIN0 = :PRICEIN0,'
      '    SUMIN0 = :SUMIN0,'
      '    PRICEIN = :PRICEIN,'
      '    SUMIN = :SUMIN,'
      '    IM = :IM,'
      '    SM = :SM,'
      '    SB = :SB'
      'WHERE'
      '    IDHEAD = :OLD_IDHEAD'
      '    and IDB = :OLD_IDB'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCSPECCOMPLCB'
      'WHERE'
      '        IDHEAD = :OLD_IDHEAD'
      '    and IDB = :OLD_IDB'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCSPECCOMPLCB('
      '    IDHEAD,'
      '    IDB,'
      '    ID,'
      '    CODEB,'
      '    NAMEB,'
      '    QUANT,'
      '    IDCARD,'
      '    NAMEC,'
      '    QUANTC,'
      '    PRICEIN0,'
      '    SUMIN0,'
      '    PRICEIN,'
      '    SUMIN,'
      '    IM,'
      '    SM,'
      '    SB'
      ')'
      'VALUES('
      '    :IDHEAD,'
      '    :IDB,'
      '    :ID,'
      '    :CODEB,'
      '    :NAMEB,'
      '    :QUANT,'
      '    :IDCARD,'
      '    :NAMEC,'
      '    :QUANTC,'
      '    :PRICEIN0,'
      '    :SUMIN0,'
      '    :PRICEIN,'
      '    :SUMIN,'
      '    :IM,'
      '    :SM,'
      '    :SB'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT IDHEAD,IDB,ID,CODEB,NAMEB,QUANT,IDCARD,NAMEC,QUANTC,PRICE' +
        'IN0,SUMIN0,PRICEIN,SUMIN,IM,SM,SB'
      'FROM OF_DOCSPECCOMPLCB '
      'where(  IDHEAD=:IDH'
      '     ) and (     OF_DOCSPECCOMPLCB.IDHEAD = :OLD_IDHEAD'
      '    and OF_DOCSPECCOMPLCB.IDB = :OLD_IDB'
      '    and OF_DOCSPECCOMPLCB.ID = :OLD_ID'
      '     )'
      '    '
      '')
    SelectSQL.Strings = (
      
        'SELECT IDHEAD,IDB,ID,CODEB,NAMEB,QUANT,IDCARD,NAMEC,QUANTC,PRICE' +
        'IN0,SUMIN0,PRICEIN,SUMIN,IM,SM,SB'
      'FROM OF_DOCSPECCOMPLCB '
      'where IDHEAD=:IDH'
      '')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 776
    Top = 172
    poAskRecordCount = True
    object quSpecComplCBIDHEAD: TFIBIntegerField
      FieldName = 'IDHEAD'
    end
    object quSpecComplCBIDB: TFIBIntegerField
      FieldName = 'IDB'
    end
    object quSpecComplCBID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quSpecComplCBCODEB: TFIBIntegerField
      FieldName = 'CODEB'
    end
    object quSpecComplCBNAMEB: TFIBStringField
      FieldName = 'NAMEB'
      Size = 100
      EmptyStrToNull = True
    end
    object quSpecComplCBQUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quSpecComplCBIDCARD: TFIBIntegerField
      FieldName = 'IDCARD'
    end
    object quSpecComplCBNAMEC: TFIBStringField
      FieldName = 'NAMEC'
      Size = 100
      EmptyStrToNull = True
    end
    object quSpecComplCBQUANTC: TFIBFloatField
      FieldName = 'QUANTC'
    end
    object quSpecComplCBPRICEIN: TFIBFloatField
      FieldName = 'PRICEIN'
    end
    object quSpecComplCBSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quSpecComplCBIM: TFIBIntegerField
      FieldName = 'IM'
    end
    object quSpecComplCBSM: TFIBStringField
      FieldName = 'SM'
      EmptyStrToNull = True
    end
    object quSpecComplCBSB: TFIBStringField
      FieldName = 'SB'
      EmptyStrToNull = True
    end
    object quSpecComplCBPRICEIN0: TFIBFloatField
      FieldName = 'PRICEIN0'
    end
    object quSpecComplCBSUMIN0: TFIBFloatField
      FieldName = 'SUMIN0'
    end
  end
  object taHeadDoc: TClientDataSet
    Aggregates = <>
    FileName = 'HeadDoc.cds'
    Params = <>
    Left = 32
    Top = 296
    object taHeadDocIType: TSmallintField
      FieldName = 'IType'
    end
    object taHeadDocId: TIntegerField
      FieldName = 'Id'
    end
    object taHeadDocDateDoc: TIntegerField
      FieldName = 'DateDoc'
    end
    object taHeadDocNumDoc: TStringField
      FieldName = 'NumDoc'
      Size = 15
    end
    object taHeadDocIdCli: TIntegerField
      FieldName = 'IdCli'
    end
    object taHeadDocNameCli: TStringField
      FieldName = 'NameCli'
      Size = 70
    end
    object taHeadDocIdSkl: TIntegerField
      FieldName = 'IdSkl'
    end
    object taHeadDocNameSkl: TStringField
      FieldName = 'NameSkl'
      Size = 70
    end
    object taHeadDocSumIN: TFloatField
      FieldName = 'SumIN'
      DisplayFormat = '0.00'
    end
    object taHeadDocSumUch: TFloatField
      FieldName = 'SumUch'
      DisplayFormat = '0.00'
    end
    object taHeadDocComment: TStringField
      FieldName = 'Comment'
      Size = 100
    end
  end
  object taSpecDoc: TClientDataSet
    Aggregates = <>
    FileName = 'SpecDoc.cds'
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'taSpecDocIndex1'
        Fields = 'IType;IdHead;Num'
      end>
    IndexFieldNames = 'IType;IdHead;Num'
    MasterSource = dsHeadDoc
    PacketRecords = 0
    Params = <>
    StoreDefs = True
    Left = 100
    Top = 288
    object taSpecDocIType: TSmallintField
      FieldName = 'IType'
    end
    object taSpecDocIdHead: TIntegerField
      FieldName = 'IdHead'
    end
    object taSpecDocNum: TIntegerField
      FieldName = 'Num'
    end
    object taSpecDocIdCard: TIntegerField
      FieldName = 'IdCard'
    end
    object taSpecDocQuant: TFloatField
      FieldName = 'Quant'
      DisplayFormat = '0.###'
    end
    object taSpecDocPriceIn: TFloatField
      FieldName = 'PriceIn'
      DisplayFormat = '0.00'
    end
    object taSpecDocSumIn: TFloatField
      FieldName = 'SumIn'
      DisplayFormat = '0.00'
    end
    object taSpecDocPriceUch: TFloatField
      FieldName = 'PriceUch'
      DisplayFormat = '0.00'
    end
    object taSpecDocSumUch: TFloatField
      FieldName = 'SumUch'
      DisplayFormat = '0.00'
    end
    object taSpecDocIdNds: TIntegerField
      FieldName = 'IdNds'
    end
    object taSpecDocSumNds: TFloatField
      FieldName = 'SumNds'
    end
    object taSpecDocNameC: TStringField
      FieldName = 'NameC'
      Size = 30
    end
    object taSpecDocSm: TStringField
      FieldName = 'Sm'
      Size = 15
    end
    object taSpecDocIdM: TIntegerField
      FieldName = 'IdM'
    end
    object taSpecDocKm: TFloatField
      FieldName = 'Km'
    end
    object taSpecDocPriceUch1: TFloatField
      FieldName = 'PriceUch1'
    end
    object taSpecDocSumUch1: TFloatField
      FieldName = 'SumUch1'
    end
    object taSpecDocProcPrice: TFloatField
      FieldName = 'ProcPrice'
    end
  end
  object dsHeadDoc: TDataSource
    DataSet = taHeadDoc
    Left = 32
    Top = 340
  end
  object dsSpecDoc: TDataSource
    DataSet = taSpecDoc
    Left = 104
    Top = 340
  end
  object quDocsActs: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHEADACTS'
      'SET '
      '    DATEDOC = :DATEDOC,'
      '    NUMDOC = :NUMDOC,'
      '    IDSKL = :IDSKL,'
      '    SUMIN = :SUMIN,'
      '    SUMUCH = :SUMUCH,'
      '    IACTIVE = :IACTIVE,'
      '    OPER = :OPER,'
      '    COMMENT = :COMMENT'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHEADACTS'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHEADACTS('
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    IDSKL,'
      '    SUMIN,'
      '    SUMUCH,'
      '    IACTIVE,'
      '    OPER,'
      '    COMMENT'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATEDOC,'
      '    :NUMDOC,'
      '    :IDSKL,'
      '    :SUMIN,'
      '    :SUMUCH,'
      '    :IACTIVE,'
      '    :OPER,'
      '    :COMMENT'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT AH.ID,AH.DATEDOC,AH.NUMDOC,AH.IDSKL,AH.SUMIN,AH.SUMUCH,AH' +
        '.IACTIVE,AH.OPER,mh.NAMEMH,AH.COMMENT'
      'FROM OF_DOCHEADACTS AH'
      'left join OF_MH mh on mh.ID=AH.IDSKL'
      'Where(  AH.DATEDOC>=:DATEB and AH.DATEDOC<:DATEE'
      '     ) and (     AH.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT AH.ID,AH.DATEDOC,AH.NUMDOC,AH.IDSKL,AH.SUMIN,AH.SUMUCH,AH' +
        '.IACTIVE,AH.OPER,mh.NAMEMH,AH.COMMENT'
      'FROM OF_DOCHEADACTS AH'
      'left join OF_MH mh on mh.ID=AH.IDSKL'
      'Where AH.DATEDOC>=:DATEB and AH.DATEDOC<:DATEE'
      
        'and AH.IDSKL in (SELECT IDMH FROM RPERSONALMH where IDPERSON=:ID' +
        'PERSON)')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 244
    Top = 256
    poAskRecordCount = True
    object quDocsActsID2: TFIBIntegerField
      FieldName = 'ID'
    end
    object quDocsActsDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object quDocsActsNUMDOC: TFIBStringField
      FieldName = 'NUMDOC'
      Size = 15
      EmptyStrToNull = True
    end
    object quDocsActsIDSKL: TFIBIntegerField
      FieldName = 'IDSKL'
    end
    object quDocsActsSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
      DisplayFormat = '0.00'
    end
    object quDocsActsSUMUCH: TFIBFloatField
      FieldName = 'SUMUCH'
      DisplayFormat = '0.00'
    end
    object quDocsActsIACTIVE: TFIBIntegerField
      FieldName = 'IACTIVE'
    end
    object quDocsActsOPER: TFIBStringField
      FieldName = 'OPER'
      EmptyStrToNull = True
    end
    object quDocsActsNAMEMH: TFIBStringField
      FieldName = 'NAMEMH'
      Size = 100
      EmptyStrToNull = True
    end
    object quDocsActsCOMMENT: TFIBStringField
      FieldName = 'COMMENT'
      Size = 200
      EmptyStrToNull = True
    end
  end
  object dsquDocsActs: TDataSource
    DataSet = quDocsActs
    Left = 240
    Top = 312
  end
  object quDocsActsId: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHEADACTS'
      'SET '
      '    DATEDOC = :DATEDOC,'
      '    NUMDOC = :NUMDOC,'
      '    IDSKL = :IDSKL,'
      '    SUMIN = :SUMIN,'
      '    SUMUCH = :SUMUCH,'
      '    IACTIVE = :IACTIVE,'
      '    OPER = :OPER,'
      '    COMMENT = :COMMENT'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHEADACTS'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHEADACTS('
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    IDSKL,'
      '    SUMIN,'
      '    SUMUCH,'
      '    IACTIVE,'
      '    OPER,'
      '    COMMENT'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATEDOC,'
      '    :NUMDOC,'
      '    :IDSKL,'
      '    :SUMIN,'
      '    :SUMUCH,'
      '    :IACTIVE,'
      '    :OPER,'
      '    :COMMENT'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT AH.ID,AH.DATEDOC,AH.NUMDOC,AH.IDSKL,AH.SUMIN,AH.SUMUCH,AH' +
        '.IACTIVE,AH.OPER,AH.COMMENT'
      'FROM OF_DOCHEADACTS AH'
      'left join OF_MH mh on mh.ID=AH.IDSKL'
      'Where(  AH.ID=:IDH'
      '     ) and (     AH.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT AH.ID,AH.DATEDOC,AH.NUMDOC,AH.IDSKL,AH.SUMIN,AH.SUMUCH,AH' +
        '.IACTIVE,AH.OPER,AH.COMMENT'
      'FROM OF_DOCHEADACTS AH'
      'left join OF_MH mh on mh.ID=AH.IDSKL'
      'Where AH.ID=:IDH')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 320
    Top = 280
    poAskRecordCount = True
    object quDocsActsIdID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quDocsActsIdDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object quDocsActsIdNUMDOC: TFIBStringField
      FieldName = 'NUMDOC'
      Size = 15
      EmptyStrToNull = True
    end
    object quDocsActsIdIDSKL: TFIBIntegerField
      FieldName = 'IDSKL'
    end
    object quDocsActsIdSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quDocsActsIdSUMUCH: TFIBFloatField
      FieldName = 'SUMUCH'
    end
    object quDocsActsIdIACTIVE: TFIBIntegerField
      FieldName = 'IACTIVE'
    end
    object quDocsActsIdOPER: TFIBStringField
      FieldName = 'OPER'
      EmptyStrToNull = True
    end
    object quDocsActsIdCOMMENT: TFIBStringField
      FieldName = 'COMMENT'
      Size = 200
      EmptyStrToNull = True
    end
  end
  object quDocsVnSel: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHEADVN'
      'SET '
      '    DATEDOC = :DATEDOC,'
      '    NUMDOC = :NUMDOC,'
      '    IDSKL_FROM = :IDSKL_FROM,'
      '    IDSKL_TO = :IDSKL_TO,'
      '    SUMIN = :SUMIN,'
      '    SUMUCH = :SUMUCH,'
      '    SUMUCH1 = :SUMUCH1,'
      '    SUMTAR = :SUMTAR,'
      '    PROCNAC = :PROCNAC,'
      '    IACTIVE = :IACTIVE'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHEADVN'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHEADVN('
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    IDSKL_FROM,'
      '    IDSKL_TO,'
      '    SUMIN,'
      '    SUMUCH,'
      '    SUMUCH1,'
      '    SUMTAR,'
      '    PROCNAC,'
      '    IACTIVE'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATEDOC,'
      '    :NUMDOC,'
      '    :IDSKL_FROM,'
      '    :IDSKL_TO,'
      '    :SUMIN,'
      '    :SUMUCH,'
      '    :SUMUCH1,'
      '    :SUMTAR,'
      '    :PROCNAC,'
      '    :IACTIVE'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT dh.ID,dh.DATEDOC,dh.NUMDOC,dh.IDSKL_FROM,dh.IDSKL_TO,dh.S' +
        'UMIN,'
      '       dh.SUMUCH,dh.SUMUCH1,dh.SUMTAR,dh.PROCNAC,dh.IACTIVE,'
      '       mh_f.NAMEMH, mh_t.NAMEMH'
      'FROM OF_DOCHEADVN dh'
      'left join OF_MH mh_f on mh_f.ID=dh.IDSKL_FROM'
      'left join OF_MH mh_t on mh_t.ID=dh.IDSKL_TO'
      ''
      'Where(  dh.DATEDOC>=:DATEB and dh.DATEDOC<:DATEE'
      '     ) and (     DH.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT dh.ID,dh.DATEDOC,dh.NUMDOC,dh.IDSKL_FROM,dh.IDSKL_TO,dh.S' +
        'UMIN,'
      '       dh.SUMUCH,dh.SUMUCH1,dh.SUMTAR,dh.PROCNAC,dh.IACTIVE,'
      '       mh_f.NAMEMH, mh_t.NAMEMH as NAMEMH1'
      'FROM OF_DOCHEADVN dh'
      'left join OF_MH mh_f on mh_f.ID=dh.IDSKL_FROM'
      'left join OF_MH mh_t on mh_t.ID=dh.IDSKL_TO'
      ''
      'Where dh.DATEDOC>=:DATEB and dh.DATEDOC<:DATEE'
      'and'
      
        '( dh.IDSKL_FROM in (SELECT IDMH FROM RPERSONALMH where IDPERSON=' +
        ':IDPERSON)'
      'or'
      
        '  dh.IDSKL_TO in (SELECT IDMH FROM RPERSONALMH where IDPERSON=:I' +
        'DPERSON1)'
      ')'
      '')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 304
    Top = 368
    poAskRecordCount = True
    object quDocsVnSelID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quDocsVnSelDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object quDocsVnSelNUMDOC: TFIBStringField
      FieldName = 'NUMDOC'
      Size = 15
      EmptyStrToNull = True
    end
    object quDocsVnSelIDSKL_FROM: TFIBIntegerField
      FieldName = 'IDSKL_FROM'
    end
    object quDocsVnSelIDSKL_TO: TFIBIntegerField
      FieldName = 'IDSKL_TO'
    end
    object quDocsVnSelSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
      DisplayFormat = '0.00'
    end
    object quDocsVnSelSUMUCH: TFIBFloatField
      FieldName = 'SUMUCH'
      DisplayFormat = '0.00'
    end
    object quDocsVnSelSUMUCH1: TFIBFloatField
      FieldName = 'SUMUCH1'
      DisplayFormat = '0.00'
    end
    object quDocsVnSelSUMTAR: TFIBFloatField
      FieldName = 'SUMTAR'
    end
    object quDocsVnSelPROCNAC: TFIBFloatField
      FieldName = 'PROCNAC'
    end
    object quDocsVnSelIACTIVE: TFIBIntegerField
      FieldName = 'IACTIVE'
    end
    object quDocsVnSelNAMEMH: TFIBStringField
      FieldName = 'NAMEMH'
      Size = 100
      EmptyStrToNull = True
    end
    object quDocsVnSelNAMEMH1: TFIBStringField
      FieldName = 'NAMEMH1'
      Size = 100
      EmptyStrToNull = True
    end
  end
  object dsDocsVnSel: TDataSource
    DataSet = quDocsVnSel
    Left = 304
    Top = 424
  end
  object quSpecVnSel: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCSPECVN'
      'SET '
      '    NUM = :NUM,'
      '    IDCARD = :IDCARD,'
      '    QUANT = :QUANT,'
      '    PRICEIN = :PRICEIN,'
      '    SUMIN = :SUMIN,'
      '    PRICEUCH = :PRICEUCH,'
      '    SUMUCH = :SUMUCH,'
      '    PRICEUCH1 = :PRICEUCH1,'
      '    SUMUCH1 = :SUMUCH1,'
      '    IDM = :IDM,'
      '    KM = :KM'
      'WHERE'
      '    IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCSPECVN'
      'WHERE'
      '        IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCSPECVN('
      '    IDHEAD,'
      '    ID,'
      '    NUM,'
      '    IDCARD,'
      '    QUANT,'
      '    PRICEIN,'
      '    SUMIN,'
      '    PRICEUCH,'
      '    SUMUCH,'
      '    PRICEUCH1,'
      '    SUMUCH1,'
      '    IDM,'
      '    KM'
      ')'
      'VALUES('
      '    :IDHEAD,'
      '    :ID,'
      '    :NUM,'
      '    :IDCARD,'
      '    :QUANT,'
      '    :PRICEIN,'
      '    :SUMIN,'
      '    :PRICEUCH,'
      '    :SUMUCH,'
      '    :PRICEUCH1,'
      '    :SUMUCH1,'
      '    :IDM,'
      '    :KM'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT SP.IDHEAD,SP.ID,SP.NUM,SP.IDCARD,SP.QUANT,SP.PRICEIN,SP.S' +
        'UMIN,'
      'SP.PRICEUCH,SP.SUMUCH,SP.PRICEUCH1,SP.SUMUCH1,SP.IDM,SP.KM,'
      'C.NAME as NAMEC,M.NAMESHORT as SM'
      'FROM OF_DOCSPECVN SP'
      'left join OF_CARDS C on C.ID=SP.IDCARD'
      'left join OF_MESSUR M on M.ID=SP.IDM'
      ''
      'where(  SP.IDHEAD=:IDHD'
      '     ) and (     SP.IDHEAD = :OLD_IDHEAD'
      '    and SP.ID = :OLD_ID'
      '     )'
      '    '
      '')
    SelectSQL.Strings = (
      
        'SELECT SP.IDHEAD,SP.ID,SP.NUM,SP.IDCARD,SP.QUANT,SP.PRICEIN,SP.S' +
        'UMIN,'
      'SP.PRICEUCH,SP.SUMUCH,SP.PRICEUCH1,SP.SUMUCH1,SP.IDM,SP.KM,'
      'C.NAME as NAMEC,M.NAMESHORT as SM, C.CATEGORY'
      'FROM OF_DOCSPECVN SP'
      'left join OF_CARDS C on C.ID=SP.IDCARD'
      'left join OF_MESSUR M on M.ID=SP.IDM'
      ''
      'where SP.IDHEAD=:IDHD'
      ''
      'ORDER BY SP.NUM')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 304
    Top = 476
    object quSpecVnSelIDHEAD: TFIBIntegerField
      FieldName = 'IDHEAD'
    end
    object quSpecVnSelID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quSpecVnSelNUM: TFIBIntegerField
      FieldName = 'NUM'
    end
    object quSpecVnSelIDCARD: TFIBIntegerField
      FieldName = 'IDCARD'
    end
    object quSpecVnSelQUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quSpecVnSelPRICEIN: TFIBFloatField
      FieldName = 'PRICEIN'
    end
    object quSpecVnSelSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quSpecVnSelPRICEUCH: TFIBFloatField
      FieldName = 'PRICEUCH'
    end
    object quSpecVnSelSUMUCH: TFIBFloatField
      FieldName = 'SUMUCH'
    end
    object quSpecVnSelPRICEUCH1: TFIBFloatField
      FieldName = 'PRICEUCH1'
    end
    object quSpecVnSelSUMUCH1: TFIBFloatField
      FieldName = 'SUMUCH1'
    end
    object quSpecVnSelIDM: TFIBIntegerField
      FieldName = 'IDM'
    end
    object quSpecVnSelNAMEC: TFIBStringField
      FieldName = 'NAMEC'
      Size = 200
      EmptyStrToNull = True
    end
    object quSpecVnSelSM: TFIBStringField
      FieldName = 'SM'
      Size = 50
      EmptyStrToNull = True
    end
    object quSpecVnSelKM: TFIBFloatField
      FieldName = 'KM'
    end
    object quSpecVnSelCATEGORY: TFIBIntegerField
      FieldName = 'CATEGORY'
    end
  end
  object quDocsVnId: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHEADVN'
      'SET '
      '    DATEDOC = :DATEDOC,'
      '    NUMDOC = :NUMDOC,'
      '    IDSKL_FROM = :IDSKL_FROM,'
      '    IDSKL_TO = :IDSKL_TO,'
      '    SUMIN = :SUMIN,'
      '    SUMUCH = :SUMUCH,'
      '    SUMUCH1 = :SUMUCH1,'
      '    SUMTAR = :SUMTAR,'
      '    PROCNAC = :PROCNAC,'
      '    IACTIVE = :IACTIVE'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHEADVN'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHEADVN('
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    IDSKL_FROM,'
      '    IDSKL_TO,'
      '    SUMIN,'
      '    SUMUCH,'
      '    SUMUCH1,'
      '    SUMTAR,'
      '    PROCNAC,'
      '    IACTIVE'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATEDOC,'
      '    :NUMDOC,'
      '    :IDSKL_FROM,'
      '    :IDSKL_TO,'
      '    :SUMIN,'
      '    :SUMUCH,'
      '    :SUMUCH1,'
      '    :SUMTAR,'
      '    :PROCNAC,'
      '    :IACTIVE'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    IDSKL_FROM,'
      '    IDSKL_TO,'
      '    SUMIN,'
      '    SUMUCH,'
      '    SUMUCH1,'
      '    SUMTAR,'
      '    PROCNAC,'
      '    IACTIVE'
      'FROM'
      '    OF_DOCHEADVN '
      'Where(  ID=:IDH'
      '     ) and (     OF_DOCHEADVN.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    IDSKL_FROM,'
      '    IDSKL_TO,'
      '    SUMIN,'
      '    SUMUCH,'
      '    SUMUCH1,'
      '    SUMTAR,'
      '    PROCNAC,'
      '    IACTIVE'
      'FROM'
      '    OF_DOCHEADVN '
      'Where ID=:IDH')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 232
    Top = 368
    poAskRecordCount = True
    object quDocsVnIdID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quDocsVnIdDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object quDocsVnIdNUMDOC: TFIBStringField
      FieldName = 'NUMDOC'
      Size = 15
      EmptyStrToNull = True
    end
    object quDocsVnIdIDSKL_FROM: TFIBIntegerField
      FieldName = 'IDSKL_FROM'
    end
    object quDocsVnIdIDSKL_TO: TFIBIntegerField
      FieldName = 'IDSKL_TO'
    end
    object quDocsVnIdSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quDocsVnIdSUMUCH: TFIBFloatField
      FieldName = 'SUMUCH'
    end
    object quDocsVnIdSUMUCH1: TFIBFloatField
      FieldName = 'SUMUCH1'
    end
    object quDocsVnIdSUMTAR: TFIBFloatField
      FieldName = 'SUMTAR'
    end
    object quDocsVnIdPROCNAC: TFIBFloatField
      FieldName = 'PROCNAC'
    end
    object quDocsVnIdIACTIVE: TFIBIntegerField
      FieldName = 'IACTIVE'
    end
  end
  object quSpecAO: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCSPECACTSOUT'
      'SET '
      '    IDCARD = :IDCARD,'
      '    QUANT = :QUANT,'
      '    IDM = :IDM,'
      '    KM = :KM,'
      '    PRICEIN0 = :PRICEIN0,'
      '    SUMIN0 = :SUMIN0,'
      '    PRICEIN = :PRICEIN,'
      '    SUMIN = :SUMIN,'
      '    PRICEINUCH = :PRICEINUCH,'
      '    SUMINUCH = :SUMINUCH,'
      '    TCARD = :TCARD,'
      '    INDS = :INDS,'
      '    RNDS = :RNDS'
      'WHERE'
      '    IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCSPECACTSOUT'
      'WHERE'
      '        IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCSPECACTSOUT('
      '    IDHEAD,'
      '    ID,'
      '    IDCARD,'
      '    QUANT,'
      '    IDM,'
      '    KM,'
      '    PRICEIN0,'
      '    SUMIN0,'
      '    PRICEIN,'
      '    SUMIN,'
      '    PRICEINUCH,'
      '    SUMINUCH,'
      '    TCARD,'
      '    INDS,'
      '    RNDS'
      ')'
      'VALUES('
      '    :IDHEAD,'
      '    :ID,'
      '    :IDCARD,'
      '    :QUANT,'
      '    :IDM,'
      '    :KM,'
      '    :PRICEIN0,'
      '    :SUMIN0,'
      '    :PRICEIN,'
      '    :SUMIN,'
      '    :PRICEINUCH,'
      '    :SUMINUCH,'
      '    :TCARD,'
      '    :INDS,'
      '    :RNDS'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT so.IDHEAD,so.ID,so.IDCARD,so.QUANT,so.IDM,so.KM,so.PRICEI' +
        'N0,so.SUMIN0,'
      
        'so.PRICEIN,so.SUMIN,so.PRICEINUCH,so.SUMINUCH,so.TCARD,so.INDS,s' +
        'o.RNDS,'
      'me.NAMESHORT,ca.NAME,n.NAMENDS'
      'FROM OF_DOCSPECACTSOUT so'
      'left join OF_MESSUR me on me.ID=so.IDM'
      'left join of_cards ca on ca.ID=so.IDCARD'
      'left join OF_NDS N on N.ID=so.INDS'
      ''
      'where(  IDHEAD=:IDH'
      '     ) and (     SO.IDHEAD = :OLD_IDHEAD'
      '    and SO.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT so.IDHEAD,so.ID,so.IDCARD,so.QUANT,so.IDM,so.KM,so.PRICEI' +
        'N0,so.SUMIN0,'
      
        'so.PRICEIN,so.SUMIN,so.PRICEINUCH,so.SUMINUCH,so.TCARD,so.INDS,s' +
        'o.RNDS,'
      'me.NAMESHORT,ca.NAME,n.NAMENDS'
      'FROM OF_DOCSPECACTSOUT so'
      'left join OF_MESSUR me on me.ID=so.IDM'
      'left join of_cards ca on ca.ID=so.IDCARD'
      'left join OF_NDS N on N.ID=so.INDS'
      ''
      'where IDHEAD=:IDH'
      'Order by so.ID')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 376
    Top = 256
    poAskRecordCount = True
    object quSpecAOIDHEAD: TFIBIntegerField
      FieldName = 'IDHEAD'
    end
    object quSpecAOID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quSpecAOIDCARD: TFIBIntegerField
      FieldName = 'IDCARD'
    end
    object quSpecAOQUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quSpecAOIDM: TFIBIntegerField
      FieldName = 'IDM'
    end
    object quSpecAOKM: TFIBFloatField
      FieldName = 'KM'
    end
    object quSpecAOPRICEIN: TFIBFloatField
      FieldName = 'PRICEIN'
    end
    object quSpecAOSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quSpecAOPRICEINUCH: TFIBFloatField
      FieldName = 'PRICEINUCH'
    end
    object quSpecAOSUMINUCH: TFIBFloatField
      FieldName = 'SUMINUCH'
    end
    object quSpecAOTCARD: TFIBSmallIntField
      FieldName = 'TCARD'
    end
    object quSpecAONAMESHORT: TFIBStringField
      FieldName = 'NAMESHORT'
      Size = 50
      EmptyStrToNull = True
    end
    object quSpecAONAME: TFIBStringField
      FieldName = 'NAME'
      Size = 200
      EmptyStrToNull = True
    end
    object quSpecAOPRICEIN0: TFIBFloatField
      FieldName = 'PRICEIN0'
    end
    object quSpecAOSUMIN0: TFIBFloatField
      FieldName = 'SUMIN0'
    end
    object quSpecAOINDS: TFIBIntegerField
      FieldName = 'INDS'
    end
    object quSpecAORNDS: TFIBFloatField
      FieldName = 'RNDS'
    end
    object quSpecAONAMENDS: TFIBStringField
      FieldName = 'NAMENDS'
      Size = 30
      EmptyStrToNull = True
    end
  end
  object quSpecAI: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCSPECACTSIN'
      'SET '
      '    IDCARD = :IDCARD,'
      '    QUANT = :QUANT,'
      '    IDM = :IDM,'
      '    KM = :KM,'
      '    PRICEIN0 = :PRICEIN0,'
      '    SUMIN0 = :SUMIN0,'
      '    PRICEIN = :PRICEIN,'
      '    SUMIN = :SUMIN,'
      '    PRICEINUCH = :PRICEINUCH,'
      '    SUMINUCH = :SUMINUCH,'
      '    TCARD = :TCARD,'
      '    INDS = :INDS,'
      '    RNDS = :RNDS,'
      '    PROCPRICE = :PROCPRICE'
      'WHERE'
      '    IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCSPECACTSIN'
      'WHERE'
      '        IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCSPECACTSIN('
      '    IDHEAD,'
      '    ID,'
      '    IDCARD,'
      '    QUANT,'
      '    IDM,'
      '    KM,'
      '    PRICEIN0,'
      '    SUMIN0,'
      '    PRICEIN,'
      '    SUMIN,'
      '    PRICEINUCH,'
      '    SUMINUCH,'
      '    TCARD,'
      '    INDS,'
      '    RNDS,'
      '    PROCPRICE'
      ')'
      'VALUES('
      '    :IDHEAD,'
      '    :ID,'
      '    :IDCARD,'
      '    :QUANT,'
      '    :IDM,'
      '    :KM,'
      '    :PRICEIN0,'
      '    :SUMIN0,'
      '    :PRICEIN,'
      '    :SUMIN,'
      '    :PRICEINUCH,'
      '    :SUMINUCH,'
      '    :TCARD,'
      '    :INDS,'
      '    :RNDS,'
      '    :PROCPRICE'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT so.IDHEAD,so.ID,so.IDCARD,so.QUANT,so.IDM,so.KM,so.PRICEI' +
        'N0,so.SUMIN0'
      
        ',so.PRICEIN,so.SUMIN,so.PRICEINUCH,so.SUMINUCH,so.TCARD,so.INDS,' +
        'so.RNDS,'
      'me.NAMESHORT,ca.NAME,so.PROCPRICE,n.NAMENDS'
      'FROM OF_DOCSPECACTSIN so'
      'left join OF_MESSUR me on me.ID=so.IDM'
      'left join of_cards ca on ca.ID=so.IDCARD'
      'left join OF_NDS N on N.ID=so.INDS'
      'where(  IDHEAD=:IDH'
      '     ) and (     SO.IDHEAD = :OLD_IDHEAD'
      '    and SO.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT so.IDHEAD,so.ID,so.IDCARD,so.QUANT,so.IDM,so.KM,so.PRICEI' +
        'N0,so.SUMIN0'
      
        ',so.PRICEIN,so.SUMIN,so.PRICEINUCH,so.SUMINUCH,so.TCARD,so.INDS,' +
        'so.RNDS,'
      'me.NAMESHORT,ca.NAME,so.PROCPRICE,n.NAMENDS'
      'FROM OF_DOCSPECACTSIN so'
      'left join OF_MESSUR me on me.ID=so.IDM'
      'left join of_cards ca on ca.ID=so.IDCARD'
      'left join OF_NDS N on N.ID=so.INDS'
      'where IDHEAD=:IDH'
      'Order by so.ID')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 376
    Top = 312
    poAskRecordCount = True
    object quSpecAIIDHEAD: TFIBIntegerField
      FieldName = 'IDHEAD'
    end
    object quSpecAIID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quSpecAIIDCARD: TFIBIntegerField
      FieldName = 'IDCARD'
    end
    object quSpecAIQUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quSpecAIIDM: TFIBIntegerField
      FieldName = 'IDM'
    end
    object quSpecAIKM: TFIBFloatField
      FieldName = 'KM'
    end
    object quSpecAIPRICEIN: TFIBFloatField
      FieldName = 'PRICEIN'
    end
    object quSpecAISUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quSpecAIPRICEINUCH: TFIBFloatField
      FieldName = 'PRICEINUCH'
    end
    object quSpecAISUMINUCH: TFIBFloatField
      FieldName = 'SUMINUCH'
    end
    object quSpecAITCARD: TFIBSmallIntField
      FieldName = 'TCARD'
    end
    object quSpecAINAMESHORT: TFIBStringField
      FieldName = 'NAMESHORT'
      Size = 50
      EmptyStrToNull = True
    end
    object quSpecAINAME: TFIBStringField
      FieldName = 'NAME'
      Size = 200
      EmptyStrToNull = True
    end
    object quSpecAIPROCPRICE: TFIBFloatField
      FieldName = 'PROCPRICE'
    end
    object quSpecAIPRICEIN0: TFIBFloatField
      FieldName = 'PRICEIN0'
    end
    object quSpecAISUMIN0: TFIBFloatField
      FieldName = 'SUMIN0'
    end
    object quSpecAIINDS: TFIBIntegerField
      FieldName = 'INDS'
    end
    object quSpecAIRNDS: TFIBFloatField
      FieldName = 'RNDS'
    end
    object quSpecAINAMENDS: TFIBStringField
      FieldName = 'NAMENDS'
      Size = 30
      EmptyStrToNull = True
    end
  end
  object quOperT: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_OPER'
      'SET '
      '    ID = :ID,'
      '    DOCTYPE = :DOCTYPE,'
      '    NAMEOPER = :NAMEOPER,'
      '    TPRIB = :TPRIB,'
      '    TSPIS = :TSPIS'
      'WHERE'
      '    ABR = :OLD_ABR'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_OPER'
      'WHERE'
      '        ABR = :OLD_ABR'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_OPER('
      '    ABR,'
      '    ID,'
      '    DOCTYPE,'
      '    NAMEOPER,'
      '    TPRIB,'
      '    TSPIS'
      ')'
      'VALUES('
      '    :ABR,'
      '    :ID,'
      '    :DOCTYPE,'
      '    :NAMEOPER,'
      '    :TPRIB,'
      '    :TSPIS'
      ')')
    RefreshSQL.Strings = (
      'SELECT op.ABR,op.ID,op.DOCTYPE,op.NAMEOPER,'
      'd.NAMEDOC,op.TPRIB,op.TSPIS'
      'FROM OF_OPER op'
      'left join of_doctype d on d.ID=op.DOCTYPE'
      'where(  op.ID>0'
      '     ) and (     OP.ABR = :OLD_ABR'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT op.ABR,op.ID,op.DOCTYPE,op.NAMEOPER,'
      'd.NAMEDOC,op.TPRIB,op.TSPIS'
      'FROM OF_OPER op'
      'left join of_doctype d on d.ID=op.DOCTYPE'
      'where op.ID>0')
    Transaction = dmO.trSel1
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    AutoCommit = True
    Left = 456
    Top = 264
    object quOperTABR: TFIBStringField
      FieldName = 'ABR'
      EmptyStrToNull = True
    end
    object quOperTID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quOperTDOCTYPE: TFIBSmallIntField
      FieldName = 'DOCTYPE'
    end
    object quOperTNAMEOPER: TFIBStringField
      FieldName = 'NAMEOPER'
      Size = 30
      EmptyStrToNull = True
    end
    object quOperTNAMEDOC: TFIBStringField
      FieldName = 'NAMEDOC'
      Size = 30
      EmptyStrToNull = True
    end
    object quOperTTPRIB: TFIBSmallIntField
      FieldName = 'TPRIB'
    end
    object quOperTTSPIS: TFIBSmallIntField
      FieldName = 'TSPIS'
    end
  end
  object dsquOperT: TDataSource
    DataSet = quOperT
    Left = 456
    Top = 320
  end
  object taDocType: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCTYPE'
      'SET '
      '    NAMEDOC = :NAMEDOC'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCTYPE'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCTYPE('
      '    ID,'
      '    NAMEDOC'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAMEDOC'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    NAMEDOC'
      'FROM'
      '    OF_DOCTYPE '
      ''
      ' WHERE '
      '        OF_DOCTYPE.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    NAMEDOC'
      'FROM'
      '    OF_DOCTYPE ')
    Transaction = dmO.trSel2
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    Left = 800
    Top = 292
    object taDocTypeID: TFIBSmallIntField
      FieldName = 'ID'
    end
    object taDocTypeNAMEDOC: TFIBStringField
      FieldName = 'NAMEDOC'
      Size = 30
      EmptyStrToNull = True
    end
  end
  object dstaDocType: TDataSource
    DataSet = taDocType
    Left = 800
    Top = 344
  end
  object taOperT: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_OPER'
      'SET '
      '    ID = :ID,'
      '    DOCTYPE = :DOCTYPE,'
      '    NAMEOPER = :NAMEOPER'
      'WHERE'
      '    ABR = :OLD_ABR'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_OPER'
      'WHERE'
      '        ABR = :OLD_ABR'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_OPER('
      '    ABR,'
      '    ID,'
      '    DOCTYPE,'
      '    NAMEOPER'
      ')'
      'VALUES('
      '    :ABR,'
      '    :ID,'
      '    :DOCTYPE,'
      '    :NAMEOPER'
      ')')
    RefreshSQL.Strings = (
      'SELECT op.ABR,op.ID,op.DOCTYPE,op.NAMEOPER,d.NAMEDOC'
      'FROM OF_OPER op'
      'left join of_doctype d on d.ID=op.DOCTYPE'
      'where(  op.ID>0'
      '     ) and (     OP.ABR = :OLD_ABR'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT op.ABR,op.ID'
      'FROM OF_OPER op'
      'where op.ABR=:ABR'
      '')
    Transaction = dmO.trSel1
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    AutoCommit = True
    Left = 528
    Top = 312
    poAskRecordCount = True
    object taOperTABR: TFIBStringField
      FieldName = 'ABR'
      EmptyStrToNull = True
    end
    object taOperTID: TFIBIntegerField
      FieldName = 'ID'
    end
  end
  object taOperD: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_OPER'
      'SET '
      '    ID = :ID,'
      '    DOCTYPE = :DOCTYPE,'
      '    NAMEOPER = :NAMEOPER'
      'WHERE'
      '    ABR = :OLD_ABR'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_OPER'
      'WHERE'
      '        ABR = :OLD_ABR'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_OPER('
      '    ABR,'
      '    ID,'
      '    DOCTYPE,'
      '    NAMEOPER'
      ')'
      'VALUES('
      '    :ABR,'
      '    :ID,'
      '    :DOCTYPE,'
      '    :NAMEOPER'
      ')')
    RefreshSQL.Strings = (
      'SELECT op.ABR,op.ID,op.DOCTYPE,op.NAMEOPER,d.NAMEDOC'
      'FROM OF_OPER op'
      'left join of_doctype d on d.ID=op.DOCTYPE'
      'where(  op.ID>0'
      '     ) and (     OP.ABR = :OLD_ABR'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT op.ABR,op.ID,op.DOCTYPE,op.NAMEOPER'
      'FROM OF_OPER op'
      'where op.DOCTYPE=:IDT'
      'order by op.NAMEOPER')
    Transaction = dmO.trSelect
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    AutoCommit = True
    Left = 584
    Top = 312
    object taOperDABR: TFIBStringField
      FieldName = 'ABR'
      EmptyStrToNull = True
    end
    object taOperDID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taOperDDOCTYPE: TFIBSmallIntField
      FieldName = 'DOCTYPE'
    end
    object taOperDNAMEOPER: TFIBStringField
      FieldName = 'NAMEOPER'
      Size = 30
      EmptyStrToNull = True
    end
  end
  object dstaOperD: TDataSource
    DataSet = taOperD
    Left = 584
    Top = 368
  end
  object taSpecDoc1: TClientDataSet
    Aggregates = <>
    FileName = 'SpecDoc1.cds'
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'taSpecDocIndex1'
        Fields = 'IType;IdHead;Num'
      end>
    IndexFieldNames = 'IType;IdHead;Num'
    MasterSource = dsHeadDoc
    PacketRecords = 0
    Params = <>
    StoreDefs = True
    Left = 164
    Top = 316
    object taSpecDoc1IType: TSmallintField
      FieldName = 'IType'
    end
    object taSpecDoc1IdHead: TIntegerField
      FieldName = 'IdHead'
    end
    object taSpecDoc1Num: TIntegerField
      FieldName = 'Num'
    end
    object taSpecDoc1IdCard: TIntegerField
      FieldName = 'IdCard'
    end
    object taSpecDoc1Quant: TFloatField
      FieldName = 'Quant'
      DisplayFormat = '0.###'
    end
    object taSpecDoc1PriceIn: TFloatField
      FieldName = 'PriceIn'
      DisplayFormat = '0.00'
    end
    object taSpecDoc1SumIn: TFloatField
      FieldName = 'SumIn'
      DisplayFormat = '0.00'
    end
    object taSpecDoc1PriceUch: TFloatField
      FieldName = 'PriceUch'
      DisplayFormat = '0.00'
    end
    object taSpecDoc1SumUch: TFloatField
      FieldName = 'SumUch'
      DisplayFormat = '0.00'
    end
    object taSpecDoc1IdNDS: TIntegerField
      FieldName = 'IdNds'
    end
    object taSpecDoc1SumNds: TFloatField
      FieldName = 'SumNds'
    end
    object taSpecDoc1NameC: TStringField
      FieldName = 'NameC'
      Size = 30
    end
    object taSpecDoc1SM: TStringField
      FieldName = 'Sm'
      Size = 15
    end
    object taSpecDoc1IDM: TIntegerField
      FieldName = 'IdM'
    end
    object taSpecDoc1Km: TFloatField
      FieldName = 'Km'
    end
    object taSpecDoc1PriceUch1: TFloatField
      FieldName = 'PriceUch1'
    end
    object taSpecDoc1SumUch1: TFloatField
      FieldName = 'SumUch1'
    end
  end
  object prAddPartOutT: TpFIBStoredProc
    Transaction = dmO.trUpdate
    Database = dmO.OfficeRnDb
    SQL.Strings = (
      
        'EXECUTE PROCEDURE PR_ADDPARTOUT_T (?ARTICUL, ?IDDATE, ?IDSTORE, ' +
        '?IDPARTIN, ?IDDOC, ?IDCLI, ?DTYPE, ?QUANT, ?PRICEIN, ?SUMOUT)')
    StoredProcName = 'PR_ADDPARTOUT_T'
    Left = 720
    Top = 80
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object prAddPartIn1T: TpFIBStoredProc
    Transaction = dmO.trUpdate
    Database = dmO.OfficeRnDb
    SQL.Strings = (
      
        'EXECUTE PROCEDURE PR_ADDPARTIN1_T (?IDSKL, ?IDDOC, ?DTYPE, ?IDAT' +
        'E, ?IDCARD, ?IDCLI, ?QUANT, ?PRICEIN, ?PRICEUCH)')
    StoredProcName = 'PR_ADDPARTIN1_T'
    Left = 712
    Top = 144
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object quSpecVnGrPos: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCSPECVN'
      'SET '
      '    NUM = :NUM,'
      '    IDCARD = :IDCARD,'
      '    QUANT = :QUANT,'
      '    PRICEIN = :PRICEIN,'
      '    SUMIN = :SUMIN,'
      '    PRICEUCH = :PRICEUCH,'
      '    SUMUCH = :SUMUCH,'
      '    PRICEUCH1 = :PRICEUCH1,'
      '    SUMUCH1 = :SUMUCH1,'
      '    IDM = :IDM,'
      '    KM = :KM'
      'WHERE'
      '    IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCSPECVN'
      'WHERE'
      '        IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCSPECVN('
      '    IDHEAD,'
      '    ID,'
      '    NUM,'
      '    IDCARD,'
      '    QUANT,'
      '    PRICEIN,'
      '    SUMIN,'
      '    PRICEUCH,'
      '    SUMUCH,'
      '    PRICEUCH1,'
      '    SUMUCH1,'
      '    IDM,'
      '    KM'
      ')'
      'VALUES('
      '    :IDHEAD,'
      '    :ID,'
      '    :NUM,'
      '    :IDCARD,'
      '    :QUANT,'
      '    :PRICEIN,'
      '    :SUMIN,'
      '    :PRICEUCH,'
      '    :SUMUCH,'
      '    :PRICEUCH1,'
      '    :SUMUCH1,'
      '    :IDM,'
      '    :KM'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT SP.IDHEAD,SP.ID,SP.NUM,SP.IDCARD,SP.QUANT,SP.PRICEIN,SP.S' +
        'UMIN,'
      'SP.PRICEUCH,SP.SUMUCH,SP.PRICEUCH1,SP.SUMUCH1,SP.IDM,SP.KM,'
      'C.NAME as NAMEC,M.NAMESHORT as SM'
      'FROM OF_DOCSPECVN SP'
      'left join OF_CARDS C on C.ID=SP.IDCARD'
      'left join OF_MESSUR M on M.ID=SP.IDM'
      ''
      'where(  SP.IDHEAD=:IDHD'
      '     ) and (     SP.IDHEAD = :OLD_IDHEAD'
      '    and SP.ID = :OLD_ID'
      '     )'
      '    '
      '')
    SelectSQL.Strings = (
      
        'SELECT SP.IDHEAD,SP.ID,SP.NUM,SP.IDCARD,SP.QUANT,SP.PRICEIN,SP.S' +
        'UMIN,'
      'SP.PRICEUCH,SP.SUMUCH,SP.PRICEUCH1,SP.SUMUCH1,SP.IDM,SP.KM,'
      'C.NAME as NAMEC,M.NAMESHORT as SM'
      'FROM OF_DOCSPECVN SP'
      'left join OF_CARDS C on C.ID=SP.IDCARD'
      'left join OF_MESSUR M on M.ID=SP.IDM'
      ''
      'where SP.IDHEAD=:IDHD'
      ''
      'ORDER BY SP.NUM')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 304
    Top = 528
    object FIBIntegerField1: TFIBIntegerField
      FieldName = 'IDHEAD'
    end
    object FIBIntegerField2: TFIBIntegerField
      FieldName = 'ID'
    end
    object FIBIntegerField3: TFIBIntegerField
      FieldName = 'NUM'
    end
    object FIBIntegerField4: TFIBIntegerField
      FieldName = 'IDCARD'
    end
    object FIBFloatField1: TFIBFloatField
      FieldName = 'QUANT'
    end
    object FIBFloatField2: TFIBFloatField
      FieldName = 'PRICEIN'
    end
    object FIBFloatField3: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object FIBFloatField4: TFIBFloatField
      FieldName = 'PRICEUCH'
    end
    object FIBFloatField5: TFIBFloatField
      FieldName = 'SUMUCH'
    end
    object FIBFloatField6: TFIBFloatField
      FieldName = 'PRICEUCH1'
    end
    object FIBFloatField7: TFIBFloatField
      FieldName = 'SUMUCH1'
    end
    object FIBIntegerField5: TFIBIntegerField
      FieldName = 'IDM'
    end
    object FIBStringField1: TFIBStringField
      FieldName = 'NAMEC'
      Size = 200
      EmptyStrToNull = True
    end
    object FIBStringField2: TFIBStringField
      FieldName = 'SM'
      Size = 50
      EmptyStrToNull = True
    end
    object FIBFloatField8: TFIBFloatField
      FieldName = 'KM'
    end
  end
  object prTestSpecPart: TpFIBStoredProc
    Transaction = trSelRep
    Database = dmO.OfficeRnDb
    SQL.Strings = (
      'EXECUTE PROCEDURE PR_TESTSPECPART (?IDDOC, ?DTYPE, ?DSUM)')
    StoredProcName = 'PR_TESTSPECPART'
    Left = 32
    Top = 412
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object prResetSum: TpFIBStoredProc
    Transaction = trSelRep
    Database = dmO.OfficeRnDb
    SQL.Strings = (
      'EXECUTE PROCEDURE PR_RESETSUM (?IDDOC, ?DTYPE)')
    StoredProcName = 'PR_RESETSUM'
    Left = 32
    Top = 468
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object quCardPO: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT SUM(QUANT)as QUANT,SUM(PRICEIN*QUANT)as RSUM'
      'FROM OF_PARTOUT'
      'where ARTICUL=:IDCARD and IDDOC=:IDH and DTYPE=:DT'
      '')
    Transaction = dmO.trSel1
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    AutoCommit = True
    Left = 112
    Top = 412
    poAskRecordCount = True
    object quCardPOQUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quCardPORSUM: TFIBFloatField
      FieldName = 'RSUM'
    end
  end
  object prCalcSumRemn: TpFIBStoredProc
    Transaction = trSelRep
    Database = dmO.OfficeRnDb
    SQL.Strings = (
      'EXECUTE PROCEDURE PR_CALCREMNSUM (?IDCARD, ?IDSKL, ?DDATE, ?ISS)')
    StoredProcName = 'PR_CALCREMNSUM'
    Left = 112
    Top = 468
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object quDocsRSel: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHEADOUTR'
      'SET '
      '    DATEDOC = :DATEDOC,'
      '    NUMDOC = :NUMDOC,'
      '    DATESF = :DATESF,'
      '    NUMSF = :NUMSF,'
      '    IDCLI = :IDCLI,'
      '    IDSKL = :IDSKL,'
      '    SUMIN = :SUMIN,'
      '    SUMUCH = :SUMUCH,'
      '    SUMTAR = :SUMTAR,'
      '    SUMNDS0 = :SUMNDS0,'
      '    SUMNDS1 = :SUMNDS1,'
      '    SUMNDS2 = :SUMNDS2,'
      '    PROCNAC = :PROCNAC,'
      '    IACTIVE = :IACTIVE,'
      '    BZTYPE = :BZTYPE,'
      '    BZSTATUS = :BZSTATUS,'
      '    BZSUMR = :BZSUMR,'
      '    BZSUMF = :BZSUMF,'
      '    BZSUMS = :BZSUMS,'
      '    IEDIT = :IEDIT,'
      '    PERSEDIT = :PERSEDIT,'
      '    IPERSEDIT = :IPERSEDIT,'
      '    IDFROM = :IDFROM,'
      '    IDHCB = :IDHCB,'
      '    UPL = :UPL,'
      '    IDRV = :IDRV,'
      '    IDCLITRANS = :IDCLITRANS'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHEADOUTR'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHEADOUTR('
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    DATESF,'
      '    NUMSF,'
      '    IDCLI,'
      '    IDSKL,'
      '    SUMIN,'
      '    SUMUCH,'
      '    SUMTAR,'
      '    SUMNDS0,'
      '    SUMNDS1,'
      '    SUMNDS2,'
      '    PROCNAC,'
      '    IACTIVE,'
      '    BZTYPE,'
      '    BZSTATUS,'
      '    BZSUMR,'
      '    BZSUMF,'
      '    BZSUMS,'
      '    IEDIT,'
      '    PERSEDIT,'
      '    IPERSEDIT,'
      '    IDFROM,'
      '    IDHCB,'
      '    UPL,'
      '    IDRV,'
      '    IDCLITRANS'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATEDOC,'
      '    :NUMDOC,'
      '    :DATESF,'
      '    :NUMSF,'
      '    :IDCLI,'
      '    :IDSKL,'
      '    :SUMIN,'
      '    :SUMUCH,'
      '    :SUMTAR,'
      '    :SUMNDS0,'
      '    :SUMNDS1,'
      '    :SUMNDS2,'
      '    :PROCNAC,'
      '    :IACTIVE,'
      '    :BZTYPE,'
      '    :BZSTATUS,'
      '    :BZSUMR,'
      '    :BZSUMF,'
      '    :BZSUMS,'
      '    :IEDIT,'
      '    :PERSEDIT,'
      '    :IPERSEDIT,'
      '    :IDFROM,'
      '    :IDHCB,'
      '    :UPL,'
      '    :IDRV,'
      '    :IDCLITRANS'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT dh.ID,dh.DATEDOC,dh.NUMDOC,dh.DATESF,dh.NUMSF,dh.IDCLI,dh' +
        '.IDSKL,dh.SUMIN,'
      
        '       dh.SUMUCH,dh.SUMTAR,dh.SUMNDS0,dh.SUMNDS1,dh.SUMNDS2,dh.P' +
        'ROCNAC,dh.IACTIVE,'
      
        '       dh.BZTYPE,dh.BZSTATUS,dh.BZSUMR,dh.BZSUMF,dh.BZSUMS,dh.IE' +
        'DIT,dh.PERSEDIT,dh.IPERSEDIT,'
      
        '       cl.NAMECL,mh.NAMEMH,dh.IDFROM,cl1.NAMECL as NAMECLFROM,dh' +
        '.IDHCB,dh.UPL,dh.IDRV,dh.IDCLITRANS'
      'FROM OF_DOCHEADOUTR dh'
      'left join OF_CLIENTS cl on cl.ID=dh.IDCLI'
      'left join OF_CLIENTS cl1 on cl1.ID=dh.IDFROM'
      'left join OF_MH mh on mh.ID=dh.IDSKL'
      ''
      'Where(  dh.DATEDOC>='#39'01.01.2012'#39' and dh.DATEDOC<='#39'31.12.2012'#39
      
        'and dh.IDSKL in (SELECT IDMH FROM RPERSONALMH where IDPERSON=100' +
        '1)'
      '     ) and (     DH.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT dh.ID,dh.DATEDOC,dh.NUMDOC,dh.DATESF,dh.NUMSF,dh.IDCLI,dh' +
        '.IDSKL,dh.SUMIN,'
      
        '       dh.SUMUCH,dh.SUMTAR,dh.SUMNDS0,dh.SUMNDS1,dh.SUMNDS2,dh.P' +
        'ROCNAC,dh.IACTIVE,'
      
        '       dh.BZTYPE,dh.BZSTATUS,dh.BZSUMR,dh.BZSUMF,dh.BZSUMS,dh.IE' +
        'DIT,dh.PERSEDIT,dh.IPERSEDIT,'
      
        '       cl.NAMECL,mh.NAMEMH,dh.IDFROM,cl1.NAMECL as NAMECLFROM,dh' +
        '.IDHCB,dh.UPL,dh.IDRV,dh.IDCLITRANS'
      'FROM OF_DOCHEADOUTR dh'
      'left join OF_CLIENTS cl on cl.ID=dh.IDCLI'
      'left join OF_CLIENTS cl1 on cl1.ID=dh.IDFROM'
      'left join OF_MH mh on mh.ID=dh.IDSKL'
      ''
      'Where dh.DATEDOC>='#39'01.01.2012'#39' and dh.DATEDOC<='#39'31.12.2012'#39
      
        'and dh.IDSKL in (SELECT IDMH FROM RPERSONALMH where IDPERSON=100' +
        '1)')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 468
    Top = 376
    poAskRecordCount = True
    object quDocsRSelID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quDocsRSelDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object quDocsRSelNUMDOC: TFIBStringField
      FieldName = 'NUMDOC'
      Size = 15
      EmptyStrToNull = True
    end
    object quDocsRSelDATESF: TFIBDateField
      FieldName = 'DATESF'
    end
    object quDocsRSelNUMSF: TFIBStringField
      FieldName = 'NUMSF'
      Size = 15
      EmptyStrToNull = True
    end
    object quDocsRSelIDCLI: TFIBIntegerField
      FieldName = 'IDCLI'
    end
    object quDocsRSelIDSKL: TFIBIntegerField
      FieldName = 'IDSKL'
    end
    object quDocsRSelSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
      DisplayFormat = '0.00'
    end
    object quDocsRSelSUMUCH: TFIBFloatField
      FieldName = 'SUMUCH'
      DisplayFormat = '0.00'
    end
    object quDocsRSelSUMTAR: TFIBFloatField
      FieldName = 'SUMTAR'
      DisplayFormat = '0.00'
    end
    object quDocsRSelSUMNDS0: TFIBFloatField
      FieldName = 'SUMNDS0'
      DisplayFormat = '0.00'
    end
    object quDocsRSelSUMNDS1: TFIBFloatField
      FieldName = 'SUMNDS1'
      DisplayFormat = '0.00'
    end
    object quDocsRSelSUMNDS2: TFIBFloatField
      FieldName = 'SUMNDS2'
      DisplayFormat = '0.00'
    end
    object quDocsRSelPROCNAC: TFIBFloatField
      FieldName = 'PROCNAC'
      DisplayFormat = '0.0'#39'%'#39
    end
    object quDocsRSelIACTIVE: TFIBIntegerField
      FieldName = 'IACTIVE'
    end
    object quDocsRSelNAMECL: TFIBStringField
      FieldName = 'NAMECL'
      Size = 100
      EmptyStrToNull = True
    end
    object quDocsRSelNAMEMH: TFIBStringField
      FieldName = 'NAMEMH'
      Size = 100
      EmptyStrToNull = True
    end
    object quDocsRSelIDFROM: TFIBIntegerField
      FieldName = 'IDFROM'
    end
    object quDocsRSelNAMECLFROM: TFIBStringField
      FieldName = 'NAMECLFROM'
      Size = 100
      EmptyStrToNull = True
    end
    object quDocsRSelBZTYPE: TFIBSmallIntField
      FieldName = 'BZTYPE'
    end
    object quDocsRSelBZSTATUS: TFIBSmallIntField
      FieldName = 'BZSTATUS'
    end
    object quDocsRSelBZSUMR: TFIBFloatField
      FieldName = 'BZSUMR'
      DisplayFormat = '0.00'
    end
    object quDocsRSelBZSUMF: TFIBFloatField
      FieldName = 'BZSUMF'
      DisplayFormat = '0.00'
    end
    object quDocsRSelBZSUMS: TFIBFloatField
      FieldName = 'BZSUMS'
      DisplayFormat = '0.00'
    end
    object quDocsRSelIEDIT: TFIBSmallIntField
      FieldName = 'IEDIT'
    end
    object quDocsRSelPERSEDIT: TFIBStringField
      FieldName = 'PERSEDIT'
      EmptyStrToNull = True
    end
    object quDocsRSelIPERSEDIT: TFIBIntegerField
      FieldName = 'IPERSEDIT'
    end
    object quDocsRSelIDHCB: TFIBIntegerField
      FieldName = 'IDHCB'
    end
    object quDocsRSelUPL: TFIBIntegerField
      FieldName = 'UPL'
    end
    object quDocsRSelIDRV: TFIBIntegerField
      FieldName = 'IDRV'
    end
    object quDocsRSelIDCLITRANS: TFIBIntegerField
      FieldName = 'IDCLITRANS'
    end
  end
  object dsquDocsRSel: TDataSource
    DataSet = quDocsRSel
    Left = 464
    Top = 432
  end
  object quDocsRId: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHEADOUTR'
      'SET '
      '    DATEDOC = :DATEDOC,'
      '    NUMDOC = :NUMDOC,'
      '    DATESF = :DATESF,'
      '    NUMSF = :NUMSF,'
      '    IDCLI = :IDCLI,'
      '    IDSKL = :IDSKL,'
      '    SUMIN = :SUMIN,'
      '    SUMUCH = :SUMUCH,'
      '    SUMTAR = :SUMTAR,'
      '    SUMNDS0 = :SUMNDS0,'
      '    SUMNDS1 = :SUMNDS1,'
      '    SUMNDS2 = :SUMNDS2,'
      '    PROCNAC = :PROCNAC,'
      '    IACTIVE = :IACTIVE,'
      '    IDFROM = :IDFROM,'
      '    BZTYPE = :BZTYPE,'
      '    BZSTATUS = :BZSTATUS,'
      '    BZSUMR = :BZSUMR,'
      '    BZSUMF = :BZSUMF,'
      '    BZSUMS = :BZSUMS,'
      '    IEDIT = :IEDIT,'
      '    PERSEDIT = :PERSEDIT,'
      '    IPERSEDIT = :IPERSEDIT,'
      '    UPL = :UPL,'
      '    IDRV = :IDRV,'
      '    IDCLITRANS = :IDCLITRANS'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHEADOUTR'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHEADOUTR('
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    DATESF,'
      '    NUMSF,'
      '    IDCLI,'
      '    IDSKL,'
      '    SUMIN,'
      '    SUMUCH,'
      '    SUMTAR,'
      '    SUMNDS0,'
      '    SUMNDS1,'
      '    SUMNDS2,'
      '    PROCNAC,'
      '    IACTIVE,'
      '    IDFROM,'
      '    BZTYPE,'
      '    BZSTATUS,'
      '    BZSUMR,'
      '    BZSUMF,'
      '    BZSUMS,'
      '    IEDIT,'
      '    PERSEDIT,'
      '    IPERSEDIT,'
      '    UPL,'
      '    IDRV,'
      '    IDCLITRANS'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATEDOC,'
      '    :NUMDOC,'
      '    :DATESF,'
      '    :NUMSF,'
      '    :IDCLI,'
      '    :IDSKL,'
      '    :SUMIN,'
      '    :SUMUCH,'
      '    :SUMTAR,'
      '    :SUMNDS0,'
      '    :SUMNDS1,'
      '    :SUMNDS2,'
      '    :PROCNAC,'
      '    :IACTIVE,'
      '    :IDFROM,'
      '    :BZTYPE,'
      '    :BZSTATUS,'
      '    :BZSUMR,'
      '    :BZSUMF,'
      '    :BZSUMS,'
      '    :IEDIT,'
      '    :PERSEDIT,'
      '    :IPERSEDIT,'
      '    :UPL,'
      '    :IDRV,'
      '    :IDCLITRANS'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    DATESF,'
      '    NUMSF,'
      '    IDCLI,'
      '    IDSKL,'
      '    SUMIN,'
      '    SUMUCH,'
      '    SUMTAR,'
      '    SUMNDS0,'
      '    SUMNDS1,'
      '    SUMNDS2,'
      '    PROCNAC,'
      '    IACTIVE,'
      '    IDFROM,'
      '    BZTYPE,'
      '    BZSTATUS,'
      '    BZSUMR,'
      '    BZSUMF,'
      '    BZSUMS,'
      '    IEDIT,'
      '    PERSEDIT,'
      '    IPERSEDIT,'
      '    UPL,'
      '    IDRV,'
      '    IDCLITRANS'
      'FROM'
      '    OF_DOCHEADOUTR '
      'Where(  ID=:IDH'
      '     ) and (     OF_DOCHEADOUTR.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    DATESF,'
      '    NUMSF,'
      '    IDCLI,'
      '    IDSKL,'
      '    SUMIN,'
      '    SUMUCH,'
      '    SUMTAR,'
      '    SUMNDS0,'
      '    SUMNDS1,'
      '    SUMNDS2,'
      '    PROCNAC,'
      '    IACTIVE,'
      '    IDFROM,'
      '    BZTYPE,'
      '    BZSTATUS,'
      '    BZSUMR,'
      '    BZSUMF,'
      '    BZSUMS,'
      '    IEDIT,'
      '    PERSEDIT,'
      '    IPERSEDIT,'
      '    UPL,'
      '    IDRV,'
      '    IDCLITRANS'
      'FROM'
      '    OF_DOCHEADOUTR '
      'Where ID=:IDH')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 388
    Top = 380
    poAskRecordCount = True
    object quDocsRIdID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quDocsRIdDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object quDocsRIdNUMDOC: TFIBStringField
      FieldName = 'NUMDOC'
      Size = 15
      EmptyStrToNull = True
    end
    object quDocsRIdDATESF: TFIBDateField
      FieldName = 'DATESF'
    end
    object quDocsRIdNUMSF: TFIBStringField
      FieldName = 'NUMSF'
      Size = 15
      EmptyStrToNull = True
    end
    object quDocsRIdIDCLI: TFIBIntegerField
      FieldName = 'IDCLI'
    end
    object quDocsRIdIDSKL: TFIBIntegerField
      FieldName = 'IDSKL'
    end
    object quDocsRIdSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quDocsRIdSUMUCH: TFIBFloatField
      FieldName = 'SUMUCH'
    end
    object quDocsRIdSUMTAR: TFIBFloatField
      FieldName = 'SUMTAR'
    end
    object quDocsRIdSUMNDS0: TFIBFloatField
      FieldName = 'SUMNDS0'
    end
    object quDocsRIdSUMNDS1: TFIBFloatField
      FieldName = 'SUMNDS1'
    end
    object quDocsRIdSUMNDS2: TFIBFloatField
      FieldName = 'SUMNDS2'
    end
    object quDocsRIdPROCNAC: TFIBFloatField
      FieldName = 'PROCNAC'
    end
    object quDocsRIdIACTIVE: TFIBIntegerField
      FieldName = 'IACTIVE'
    end
    object quDocsRIdIDFROM: TFIBIntegerField
      FieldName = 'IDFROM'
    end
    object quDocsRIdIEDIT: TFIBSmallIntField
      FieldName = 'IEDIT'
    end
    object quDocsRIdPERSEDIT: TFIBStringField
      FieldName = 'PERSEDIT'
      EmptyStrToNull = True
    end
    object quDocsRIdIPERSEDIT: TFIBIntegerField
      FieldName = 'IPERSEDIT'
    end
    object quDocsRIdBZTYPE: TFIBSmallIntField
      FieldName = 'BZTYPE'
    end
    object quDocsRIdBZSTATUS: TFIBSmallIntField
      FieldName = 'BZSTATUS'
    end
    object quDocsRIdBZSUMR: TFIBFloatField
      FieldName = 'BZSUMR'
    end
    object quDocsRIdBZSUMF: TFIBFloatField
      FieldName = 'BZSUMF'
    end
    object quDocsRIdBZSUMS: TFIBFloatField
      FieldName = 'BZSUMS'
    end
    object quDocsRIdUPL: TFIBIntegerField
      FieldName = 'UPL'
    end
    object quDocsRIdIDRV: TFIBIntegerField
      FieldName = 'IDRV'
    end
    object quDocsRIdIDCLITRANS: TFIBIntegerField
      FieldName = 'IDCLITRANS'
    end
  end
  object quSpecRSel: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCSPECOUTR'
      'SET '
      '    IDCARD = :IDCARD,'
      '    QUANT = :QUANT,'
      '    IDM = :IDM,'
      '    KM = :KM,'
      '    PRICEIN0 = :PRICEIN0,'
      '    SUMIN0 = :SUMIN0,'
      '    PRICEIN = :PRICEIN,'
      '    SUMIN = :SUMIN,'
      '    PRICER = :PRICER,'
      '    SUMR = :SUMR,'
      '    TCARD = :TCARD,'
      '    IDNDS = :IDNDS,'
      '    SUMNDS = :SUMNDS,'
      '    SUMOUT0 = :SUMOUT0,'
      '    SUMNDSOUT = :SUMNDSOUT,'
      '    OPER = :OPER,'
      '    BZQUANTR = :BZQUANTR,'
      '    BZQUANTF = :BZQUANTF,'
      '    BZQUANTS = :BZQUANTS,'
      '    BZQUANTSHOP = :BZQUANTSHOP,'
      '    QUANTDOC = :QUANTDOC'
      'WHERE'
      '    IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCSPECOUTR'
      'WHERE'
      '        IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCSPECOUTR('
      '    IDHEAD,'
      '    ID,'
      '    IDCARD,'
      '    QUANT,'
      '    IDM,'
      '    KM,'
      '    PRICEIN0,'
      '    SUMIN0,'
      '    PRICEIN,'
      '    SUMIN,'
      '    PRICER,'
      '    SUMR,'
      '    TCARD,'
      '    IDNDS,'
      '    SUMNDS,'
      '    SUMOUT0,'
      '    SUMNDSOUT,'
      '    OPER,'
      '    BZQUANTR,'
      '    BZQUANTF,'
      '    BZQUANTS,'
      '    BZQUANTSHOP,'
      '    QUANTDOC'
      ')'
      'VALUES('
      '    :IDHEAD,'
      '    :ID,'
      '    :IDCARD,'
      '    :QUANT,'
      '    :IDM,'
      '    :KM,'
      '    :PRICEIN0,'
      '    :SUMIN0,'
      '    :PRICEIN,'
      '    :SUMIN,'
      '    :PRICER,'
      '    :SUMR,'
      '    :TCARD,'
      '    :IDNDS,'
      '    :SUMNDS,'
      '    :SUMOUT0,'
      '    :SUMNDSOUT,'
      '    :OPER,'
      '    :BZQUANTR,'
      '    :BZQUANTF,'
      '    :BZQUANTS,'
      '    :BZQUANTSHOP,'
      '    :QUANTDOC'
      ')')
    RefreshSQL.Strings = (
      ''
      'SELECT'
      
        '    SP.IDHEAD,SP.ID,SP.IDCARD,SP.QUANT,SP.IDM,SP.KM,SP.PRICEIN0,' +
        'SP.SUMIN0,SP.PRICEIN,SP.SUMIN'
      
        '    ,SP.PRICER,SP.SUMR,SP.TCARD,SP.IDNDS,SP.SUMNDS,SP.SUMOUT0,SP' +
        '.SUMNDSOUT,'
      
        '    C.NAME as NAMEC,C.CATEGORY,M.NAMESHORT as SM,N.NAMENDS,SP.OP' +
        'ER,N.PROC,(SP.SUMR*100/(100+N.PROC)) as OUTNDSSUM,'
      '    cto.ID,cto.NAME as NAMECTO,'
      
        '    C.INDS,SP.BZQUANTR,SP.BZQUANTF,SP.BZQUANTS,SP.BZQUANTSHOP,SP' +
        '.QUANTDOC'
      ' '
      ''
      'FROM OF_DOCSPECOUTR SP'
      'left join OF_CARDS C on C.ID=SP.IDCARD'
      'left join OF_MESSUR M on M.ID=SP.IDM'
      'left join OF_NDS N on N.ID=SP.IDNDS'
      'left join OF_CTO cto on cto.ID=C.CTO'
      ''
      'where(  SP.IDHEAD=:IDHD'
      '     ) and (     SP.IDHEAD = :OLD_IDHEAD'
      '    and SP.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      ''
      'SELECT'
      
        '    SP.IDHEAD,SP.ID,SP.IDCARD,SP.QUANT,SP.IDM,SP.KM,SP.PRICEIN0,' +
        'SP.SUMIN0,SP.PRICEIN,SP.SUMIN'
      
        '    ,SP.PRICER,SP.SUMR,SP.TCARD,SP.IDNDS,SP.SUMNDS,SP.SUMOUT0,SP' +
        '.SUMNDSOUT,'
      
        '    C.NAME as NAMEC,C.CATEGORY,M.NAMESHORT as SM,N.NAMENDS,SP.OP' +
        'ER,N.PROC,(SP.SUMR*100/(100+N.PROC)) as OUTNDSSUM,'
      '    cto.ID,cto.NAME as NAMECTO,'
      
        '    C.INDS,SP.BZQUANTR,SP.BZQUANTF,SP.BZQUANTS,SP.BZQUANTSHOP,SP' +
        '.QUANTDOC'
      ' '
      ''
      'FROM OF_DOCSPECOUTR SP'
      'left join OF_CARDS C on C.ID=SP.IDCARD'
      'left join OF_MESSUR M on M.ID=SP.IDM'
      'left join OF_NDS N on N.ID=SP.IDNDS'
      'left join OF_CTO cto on cto.ID=C.CTO'
      ''
      'where SP.IDHEAD=:IDHD'
      'ORDER BY SP.ID')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 464
    Top = 488
    object quSpecRSelIDHEAD: TFIBIntegerField
      FieldName = 'IDHEAD'
    end
    object quSpecRSelID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quSpecRSelIDCARD: TFIBIntegerField
      FieldName = 'IDCARD'
    end
    object quSpecRSelQUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quSpecRSelIDM: TFIBIntegerField
      FieldName = 'IDM'
    end
    object quSpecRSelKM: TFIBFloatField
      FieldName = 'KM'
    end
    object quSpecRSelPRICEIN0: TFIBFloatField
      FieldName = 'PRICEIN0'
    end
    object quSpecRSelSUMIN0: TFIBFloatField
      FieldName = 'SUMIN0'
    end
    object quSpecRSelPRICEIN: TFIBFloatField
      FieldName = 'PRICEIN'
    end
    object quSpecRSelSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quSpecRSelPRICER: TFIBFloatField
      FieldName = 'PRICER'
    end
    object quSpecRSelSUMR: TFIBFloatField
      FieldName = 'SUMR'
    end
    object quSpecRSelTCARD: TFIBSmallIntField
      FieldName = 'TCARD'
    end
    object quSpecRSelIDNDS: TFIBIntegerField
      FieldName = 'IDNDS'
    end
    object quSpecRSelSUMNDS: TFIBFloatField
      FieldName = 'SUMNDS'
    end
    object quSpecRSelSUMOUT0: TFIBFloatField
      FieldName = 'SUMOUT0'
    end
    object quSpecRSelSUMNDSOUT: TFIBFloatField
      FieldName = 'SUMNDSOUT'
    end
    object quSpecRSelNAMEC: TFIBStringField
      FieldName = 'NAMEC'
      Size = 200
      EmptyStrToNull = True
    end
    object quSpecRSelCATEGORY: TFIBIntegerField
      FieldName = 'CATEGORY'
    end
    object quSpecRSelSM: TFIBStringField
      FieldName = 'SM'
      Size = 50
      EmptyStrToNull = True
    end
    object quSpecRSelNAMENDS: TFIBStringField
      FieldName = 'NAMENDS'
      Size = 30
      EmptyStrToNull = True
    end
    object quSpecRSelOPER: TFIBSmallIntField
      FieldName = 'OPER'
    end
    object quSpecRSelPROC: TFIBFloatField
      FieldName = 'PROC'
    end
    object quSpecRSelOUTNDSSUM: TFIBFloatField
      FieldName = 'OUTNDSSUM'
    end
    object quSpecRSelID1: TFIBIntegerField
      FieldName = 'ID1'
    end
    object quSpecRSelNAMECTO: TFIBStringField
      FieldName = 'NAMECTO'
      Size = 50
      EmptyStrToNull = True
    end
    object quSpecRSelINDS: TFIBIntegerField
      FieldName = 'INDS'
    end
    object quSpecRSelBZQUANTR: TFIBFloatField
      FieldName = 'BZQUANTR'
    end
    object quSpecRSelBZQUANTF: TFIBFloatField
      FieldName = 'BZQUANTF'
    end
    object quSpecRSelBZQUANTS: TFIBFloatField
      FieldName = 'BZQUANTS'
    end
    object quSpecRSelBZQUANTSHOP: TFIBFloatField
      FieldName = 'BZQUANTSHOP'
    end
    object quSpecRSelQUANTDOC: TFIBFloatField
      FieldName = 'QUANTDOC'
    end
  end
  object quSpecRDay: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCSPECOUTR'
      'SET '
      '    IDCARD = :IDCARD,'
      '    QUANT = :QUANT,'
      '    IDM = :IDM,'
      '    KM = :KM,'
      '    PRICEIN = :PRICEIN,'
      '    SUMIN = :SUMIN,'
      '    PRICER = :PRICER,'
      '    SUMR = :SUMR,'
      '    TCARD = :TCARD,'
      '    IDNDS = :IDNDS,'
      '    SUMNDS = :SUMNDS'
      'WHERE'
      '    IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCSPECOUTR'
      'WHERE'
      '        IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCSPECOUTR('
      '    IDHEAD,'
      '    ID,'
      '    IDCARD,'
      '    QUANT,'
      '    IDM,'
      '    KM,'
      '    PRICEIN,'
      '    SUMIN,'
      '    PRICER,'
      '    SUMR,'
      '    TCARD,'
      '    IDNDS,'
      '    SUMNDS'
      ')'
      'VALUES('
      '    :IDHEAD,'
      '    :ID,'
      '    :IDCARD,'
      '    :QUANT,'
      '    :IDM,'
      '    :KM,'
      '    :PRICEIN,'
      '    :SUMIN,'
      '    :PRICER,'
      '    :SUMR,'
      '    :TCARD,'
      '    :IDNDS,'
      '    :SUMNDS'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    SP.IDHEAD,SP.ID,SP.IDCARD,SP.QUANT,SP.IDM,SP.KM,SP.PRICEIN,'
      '    SP.SUMIN,SP.PRICER,SP.SUMR,SP.TCARD,SP.IDNDS,SP.SUMNDS,'
      '    C.NAME as NAMEC,M.NAMESHORT as SM'
      'FROM OF_DOCSPECOUTR SP'
      'left join OF_CARDS C on C.ID=SP.IDCARD'
      'left join OF_MESSUR M on M.ID=SP.IDM'
      ''
      'where(  SP.IDHEAD=:IDHD'
      '     ) and (     SP.IDHEAD = :OLD_IDHEAD'
      '    and SP.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT sr.IDCARD,ca.NAME,sr.IDM,me.NAMESHORT,sr.KM,SUM(sr.QUANT)' +
        ' as QUANT, SUM(sr.QUANTDOC) as QUANTDOC, SUM(sr.SUMIN) as SUMIN,' +
        ' SUM(sr.SUMR) as SUMR'
      'FROM OF_DOCSPECOUTR sr'
      'left join of_docheadoutr  dh on dh.ID=sr.IDHEAD'
      'left join of_cards ca on ca.ID=sr.IDCARD'
      'left join of_messur me on me.ID=sr.IDM'
      'where sr.QUANT>0'
      'and dh.DATEDOC=:DDATE'
      'and dh.IDSKL=:IDSKL'
      'and dh.IACTIVE=1'
      'and ca.CATEGORY=1'
      'group by sr.IDCARD,ca.NAME,sr.IDM,me.NAMESHORT,sr.KM'
      'order by sr.IDCARD')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 552
    Top = 432
    poAskRecordCount = True
    object quSpecRDayIDCARD: TFIBIntegerField
      FieldName = 'IDCARD'
    end
    object quSpecRDayIDM: TFIBIntegerField
      FieldName = 'IDM'
    end
    object quSpecRDayKM: TFIBFloatField
      FieldName = 'KM'
    end
    object quSpecRDayQUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quSpecRDaySUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quSpecRDaySUMR: TFIBFloatField
      FieldName = 'SUMR'
    end
    object quSpecRDayNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 200
      EmptyStrToNull = True
    end
    object quSpecRDayNAMESHORT: TFIBStringField
      FieldName = 'NAMESHORT'
      Size = 50
      EmptyStrToNull = True
    end
    object quSpecRDayQUANTDOC: TFIBFloatField
      FieldName = 'QUANTDOC'
    end
  end
  object taDelCard: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT ID,SIFR,NAME,TTYPE,IMESSURE'
      'FROM'
      '    OF_CARDS_DEL ')
    Transaction = dmO.trSel1
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    AutoCommit = True
    Left = 32
    Top = 532
    poAskRecordCount = True
    object taDelCardID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taDelCardSIFR: TFIBIntegerField
      FieldName = 'SIFR'
    end
    object taDelCardNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 200
      EmptyStrToNull = True
    end
    object taDelCardTTYPE: TFIBIntegerField
      FieldName = 'TTYPE'
    end
    object taDelCardIMESSURE: TFIBIntegerField
      FieldName = 'IMESSURE'
    end
  end
  object prRECALCGDS: TpFIBStoredProc
    Transaction = trD
    Database = dmO.OfficeRnDb
    SQL.Strings = (
      'EXECUTE PROCEDURE PR_RECALCGDSMOVE (?DDATE, ?IDATE)')
    StoredProcName = 'PR_RECALCGDSMOVE'
    Left = 200
    Top = 520
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object trD: TpFIBTransaction
    DefaultDatabase = dmO.OfficeRnDb
    TimeoutAction = TARollback
    Left = 112
    Top = 532
  end
  object quClass: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCSPECIN_T'
      'SET '
      '    IDCARD = :IDCARD,'
      '    QUANT = :QUANT,'
      '    PRICEIN = :PRICEIN,'
      '    SUMIN = :SUMIN,'
      '    PRICEUCH = :PRICEUCH,'
      '    SUMUCH = :SUMUCH,'
      '    IDM = :IDM,'
      '    KM = :KM'
      'WHERE'
      '    IDHEAD = :OLD_IDHEAD'
      '    and NUM = :OLD_NUM'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCSPECIN_T'
      'WHERE'
      '        IDHEAD = :OLD_IDHEAD'
      '    and NUM = :OLD_NUM'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCSPECIN_T('
      '    IDHEAD,'
      '    NUM,'
      '    IDCARD,'
      '    QUANT,'
      '    PRICEIN,'
      '    SUMIN,'
      '    PRICEUCH,'
      '    SUMUCH,'
      '    IDM,'
      '    KM'
      ')'
      'VALUES('
      '    :IDHEAD,'
      '    :NUM,'
      '    :IDCARD,'
      '    :QUANT,'
      '    :PRICEIN,'
      '    :SUMIN,'
      '    :PRICEUCH,'
      '    :SUMUCH,'
      '    :IDM,'
      '    :KM'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    IDHEAD,'
      '    NUM,'
      '    IDCARD,'
      '    QUANT,'
      '    PRICEIN,'
      '    SUMIN,'
      '    PRICEUCH,'
      '    SUMUCH,'
      '    IDM,'
      '    KM'
      'FROM'
      '    OF_DOCSPECIN_T '
      'where(  IDHEAD=:IDH'
      '     ) and (     OF_DOCSPECIN_T.IDHEAD = :OLD_IDHEAD'
      '    and OF_DOCSPECIN_T.NUM = :OLD_NUM'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT ca.ID,ca.NAME,ca.PARENT,ca.IMESSURE,cl.NAMECL'
      'FROM OF_CARDS ca'
      'left join OF_CLASSIF cl on (cl.ID=ca.PARENT)and(cl.ITYPE=1)'
      ''
      'where PARENT=:IPARENT and CATEGORY<2')
    Transaction = dmO.trSelect
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    AutoCommit = True
    Left = 632
    Top = 432
    poAskRecordCount = True
    object quClassID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quClassNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 200
      EmptyStrToNull = True
    end
    object quClassPARENT: TFIBIntegerField
      FieldName = 'PARENT'
    end
    object quClassIMESSURE: TFIBIntegerField
      FieldName = 'IMESSURE'
    end
    object quClassNAMECL: TFIBStringField
      FieldName = 'NAMECL'
      Size = 150
      EmptyStrToNull = True
    end
  end
  object quFindPart: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCSPECIN_T'
      'SET '
      '    IDCARD = :IDCARD,'
      '    QUANT = :QUANT,'
      '    PRICEIN = :PRICEIN,'
      '    SUMIN = :SUMIN,'
      '    PRICEUCH = :PRICEUCH,'
      '    SUMUCH = :SUMUCH,'
      '    IDM = :IDM,'
      '    KM = :KM'
      'WHERE'
      '    IDHEAD = :OLD_IDHEAD'
      '    and NUM = :OLD_NUM'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCSPECIN_T'
      'WHERE'
      '        IDHEAD = :OLD_IDHEAD'
      '    and NUM = :OLD_NUM'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCSPECIN_T('
      '    IDHEAD,'
      '    NUM,'
      '    IDCARD,'
      '    QUANT,'
      '    PRICEIN,'
      '    SUMIN,'
      '    PRICEUCH,'
      '    SUMUCH,'
      '    IDM,'
      '    KM'
      ')'
      'VALUES('
      '    :IDHEAD,'
      '    :NUM,'
      '    :IDCARD,'
      '    :QUANT,'
      '    :PRICEIN,'
      '    :SUMIN,'
      '    :PRICEUCH,'
      '    :SUMUCH,'
      '    :IDM,'
      '    :KM'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    IDHEAD,'
      '    NUM,'
      '    IDCARD,'
      '    QUANT,'
      '    PRICEIN,'
      '    SUMIN,'
      '    PRICEUCH,'
      '    SUMUCH,'
      '    IDM,'
      '    KM'
      'FROM'
      '    OF_DOCSPECIN_T '
      'where(  IDHEAD=:IDH'
      '     ) and (     OF_DOCSPECIN_T.IDHEAD = :OLD_IDHEAD'
      '    and OF_DOCSPECIN_T.NUM = :OLD_NUM'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT pi.ID,pi.IDSTORE,pi.IDDOC,pi.ARTICUL,pi.IDCLI,pi.DTYPE,'
      'pi.QPART,pi.QREMN,pi.PRICEIN,pi.IDATE,Cli.NAMECL,mh.NAMEMH'
      'FROM OF_PARTIN pi'
      'left join OF_CLIENTS cli on pi.IDCLI=Cli.ID'
      'left join OF_MH mh on pi.IDSTORE=mh.ID'
      ''
      'where ARTICUL=:IDCARD and pi.QREMN>0.001 and pi.IDSTORE=:IDSTORE'
      'Order by pi.IDATE DESC')
    Transaction = dmO.trSelect
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    AutoCommit = True
    Left = 704
    Top = 432
    poAskRecordCount = True
    object quFindPartID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quFindPartIDSTORE: TFIBIntegerField
      FieldName = 'IDSTORE'
    end
    object quFindPartIDDOC: TFIBIntegerField
      FieldName = 'IDDOC'
    end
    object quFindPartARTICUL: TFIBIntegerField
      FieldName = 'ARTICUL'
    end
    object quFindPartIDCLI: TFIBIntegerField
      FieldName = 'IDCLI'
    end
    object quFindPartDTYPE: TFIBIntegerField
      FieldName = 'DTYPE'
    end
    object quFindPartQPART: TFIBFloatField
      FieldName = 'QPART'
      DisplayFormat = '0.000'
    end
    object quFindPartQREMN: TFIBFloatField
      FieldName = 'QREMN'
      DisplayFormat = '0.000'
    end
    object quFindPartPRICEIN: TFIBFloatField
      FieldName = 'PRICEIN'
      DisplayFormat = '0.00'
    end
    object quFindPartIDATE: TFIBIntegerField
      FieldName = 'IDATE'
    end
    object quFindPartNAMECL: TFIBStringField
      FieldName = 'NAMECL'
      Size = 100
      EmptyStrToNull = True
    end
    object quFindPartNAMEMH: TFIBStringField
      FieldName = 'NAMEMH'
      Size = 100
      EmptyStrToNull = True
    end
  end
  object quMainClass: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCSPECIN_T'
      'SET '
      '    IDCARD = :IDCARD,'
      '    QUANT = :QUANT,'
      '    PRICEIN = :PRICEIN,'
      '    SUMIN = :SUMIN,'
      '    PRICEUCH = :PRICEUCH,'
      '    SUMUCH = :SUMUCH,'
      '    IDM = :IDM,'
      '    KM = :KM'
      'WHERE'
      '    IDHEAD = :OLD_IDHEAD'
      '    and NUM = :OLD_NUM'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCSPECIN_T'
      'WHERE'
      '        IDHEAD = :OLD_IDHEAD'
      '    and NUM = :OLD_NUM'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCSPECIN_T('
      '    IDHEAD,'
      '    NUM,'
      '    IDCARD,'
      '    QUANT,'
      '    PRICEIN,'
      '    SUMIN,'
      '    PRICEUCH,'
      '    SUMUCH,'
      '    IDM,'
      '    KM'
      ')'
      'VALUES('
      '    :IDHEAD,'
      '    :NUM,'
      '    :IDCARD,'
      '    :QUANT,'
      '    :PRICEIN,'
      '    :SUMIN,'
      '    :PRICEUCH,'
      '    :SUMUCH,'
      '    :IDM,'
      '    :KM'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    IDHEAD,'
      '    NUM,'
      '    IDCARD,'
      '    QUANT,'
      '    PRICEIN,'
      '    SUMIN,'
      '    PRICEUCH,'
      '    SUMUCH,'
      '    IDM,'
      '    KM'
      'FROM'
      '    OF_DOCSPECIN_T '
      'where(  IDHEAD=:IDH'
      '     ) and (     OF_DOCSPECIN_T.IDHEAD = :OLD_IDHEAD'
      '    and OF_DOCSPECIN_T.NUM = :OLD_NUM'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT ID FROM OF_CLASSIF'
      'WHERE ID_PARENT=0')
    Transaction = dmO.trSelect
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    AutoCommit = True
    Left = 632
    Top = 496
    poAskRecordCount = True
    object quMainClassID: TFIBIntegerField
      FieldName = 'ID'
    end
  end
  object pr_CalcLastPrice: TpFIBStoredProc
    Transaction = trSelRep
    Database = dmO.OfficeRnDb
    SQL.Strings = (
      'EXECUTE PROCEDURE PR_CALCLASTPRICE2 (?IDGOOD, ?IDSKL, ?IDATE)')
    StoredProcName = 'PR_CALCLASTPRICE2'
    Left = 200
    Top = 440
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object quMovePer: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT'
      'SUM(POSTIN)as POSTIN,'
      'SUM(POSTOUT)as POSTOUT,'
      'SUM(VNIN)as VNIN,'
      'SUM(VNOUT)as VNOUT,'
      'SUM(INV)as INV,'
      'SUM(QREAL)as QREAL'
      'FROM OF_GDSMOVE'
      
        'where ARTICUL=:IDGOOD and IDATE>=:DATEB and IDATE<:DATEE and IDS' +
        'TORE=:IDSTORE')
    Transaction = dmO.trSelect
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    Left = 704
    Top = 496
    object quMovePerPOSTIN: TFIBFloatField
      FieldName = 'POSTIN'
    end
    object quMovePerPOSTOUT: TFIBFloatField
      FieldName = 'POSTOUT'
    end
    object quMovePerVNIN: TFIBFloatField
      FieldName = 'VNIN'
    end
    object quMovePerVNOUT: TFIBFloatField
      FieldName = 'VNOUT'
    end
    object quMovePerINV: TFIBFloatField
      FieldName = 'INV'
    end
    object quMovePerQREAL: TFIBFloatField
      FieldName = 'QREAL'
    end
  end
  object quFindSebReal: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT first 5 IDM,KM,PRICEIN,PRICEIN0'
      'FROM OF_DOCSPECOUTR sp'
      'left join OF_DOCHEADOUTR dh on dh.ID=sp.IDHEAD'
      'where sp.IDCARD=:IDCARD and sp.QUANT>0.01'
      'and dh.IACTIVE=1 and IDSKL=:IDSKL'
      'order by sp.IDHEAD desc')
    Transaction = dmO.trSelect
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    AutoCommit = True
    Left = 788
    Top = 432
    poAskRecordCount = True
    object quFindSebRealIDM: TFIBIntegerField
      FieldName = 'IDM'
    end
    object quFindSebRealKM: TFIBFloatField
      FieldName = 'KM'
    end
    object quFindSebRealPRICEIN: TFIBFloatField
      FieldName = 'PRICEIN'
    end
    object quFindSebRealPRICEIN0: TFIBFloatField
      FieldName = 'PRICEIN0'
    end
  end
  object dsquFindSebReal: TDataSource
    DataSet = quFindSebReal
    Left = 788
    Top = 492
  end
  object quFindCl: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT ID_PARENT,NAMECL'
      'FROM OF_CLASSIF'
      'where ITYPE=1'
      'and ID=:IDCL')
    Transaction = dmO.trSel1
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    AutoCommit = True
    Left = 208
    Top = 136
    poAskRecordCount = True
    object quFindClID_PARENT: TFIBIntegerField
      FieldName = 'ID_PARENT'
    end
    object quFindClNAMECL: TFIBStringField
      FieldName = 'NAMECL'
      Size = 150
      EmptyStrToNull = True
    end
  end
  object quFindMassa: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT first 1 POUTPUT'
      'FROM OF_CARDST'
      'where IDCARD=:IDCARD'
      'order by DATEB desc')
    Transaction = dmO.trSel1
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    AutoCommit = True
    Left = 204
    Top = 188
    poAskRecordCount = True
    object quFindMassaPOUTPUT: TFIBStringField
      FieldName = 'POUTPUT'
      Size = 30
      EmptyStrToNull = True
    end
  end
  object quFindTSpis: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT TSPIS FROM OF_OPER'
      'where ABR=:ABR'
      '')
    Transaction = dmO.trSel1
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    AutoCommit = True
    Left = 268
    Top = 188
    poAskRecordCount = True
    object quFindTSpisTSPIS: TFIBSmallIntField
      FieldName = 'TSPIS'
    end
  end
  object quDB: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    NAMEDB,'
      '    PATHDB,'
      '    AUTOCLOSE'
      'FROM'
      '    OF_DB '
      'order by ID')
    Transaction = trSelRep
    Database = dmO.OfficeRnDb
    Left = 400
    Top = 552
    poAskRecordCount = True
    object quDBID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quDBNAMEDB: TFIBStringField
      FieldName = 'NAMEDB'
      Size = 30
      EmptyStrToNull = True
    end
    object quDBPATHDB: TFIBStringField
      FieldName = 'PATHDB'
      Size = 150
      EmptyStrToNull = True
    end
    object quDBAUTOCLOSE: TFIBSmallIntField
      FieldName = 'AUTOCLOSE'
    end
  end
  object dsquDB: TDataSource
    DataSet = quDB
    Left = 472
    Top = 552
  end
  object quClosePartIn: TpFIBQuery
    Transaction = trU
    Database = dmO.OfficeRnDb
    SQL.Strings = (
      'update OF_PARTIN'
      'Set QREMN=0'
      'where ARTICUL=:IDCARD'
      'and IDSTORE=:IDSKL'
      'and IDATE<=:IDATE')
    Left = 348
    Top = 132
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object trU: TpFIBTransaction
    DefaultDatabase = dmO.OfficeRnDb
    TimeoutAction = TARollback
    Left = 328
    Top = 192
  end
  object quPODoc: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT SUM(QUANT*PRICEIN)as SUMIN '
      'FROM OF_PARTOUT'
      'where ARTICUL=:IDCARD'
      'and DTYPE=:IDTYPE'
      'and IDDOC=:IDH')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 704
    Top = 552
    poAskRecordCount = True
    object quPODocSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
  end
  object quFindCli: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT first 2 ID,NAMECL'
      'FROM OF_CLIENTS'
      'where UPPER(NAMECL) like '#39'%'#1040#1064'%'#39)
    Transaction = dmO.trSel1
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    AutoCommit = True
    Left = 184
    Top = 240
    poAskRecordCount = True
    object quFindCliID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quFindCliNAMECL: TFIBStringField
      FieldName = 'NAMECL'
      Size = 100
      EmptyStrToNull = True
    end
  end
  object quSpecRSel1: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCSPECOUTR'
      'SET '
      '    IDCARD = :IDCARD,'
      '    QUANT = :QUANT,'
      '    IDM = :IDM,'
      '    KM = :KM,'
      '    PRICEIN0 = :PRICEIN0,'
      '    SUMIN0 = :SUMIN0,'
      '    PRICEIN = :PRICEIN,'
      '    SUMIN = :SUMIN,'
      '    PRICER = :PRICER,'
      '    SUMR = :SUMR,'
      '    TCARD = :TCARD,'
      '    IDNDS = :IDNDS,'
      '    SUMNDS = :SUMNDS,'
      '    SUMOUT0 = :SUMOUT0,'
      '    SUMNDSOUT = :SUMNDSOUT,'
      '    OPER = :OPER'
      'WHERE'
      '    IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCSPECOUTR'
      'WHERE'
      '        IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCSPECOUTR('
      '    IDHEAD,'
      '    ID,'
      '    IDCARD,'
      '    QUANT,'
      '    IDM,'
      '    KM,'
      '    PRICEIN0,'
      '    SUMIN0,'
      '    PRICEIN,'
      '    SUMIN,'
      '    PRICER,'
      '    SUMR,'
      '    TCARD,'
      '    IDNDS,'
      '    SUMNDS,'
      '    SUMOUT0,'
      '    SUMNDSOUT,'
      '    OPER'
      ')'
      'VALUES('
      '    :IDHEAD,'
      '    :ID,'
      '    :IDCARD,'
      '    :QUANT,'
      '    :IDM,'
      '    :KM,'
      '    :PRICEIN0,'
      '    :SUMIN0,'
      '    :PRICEIN,'
      '    :SUMIN,'
      '    :PRICER,'
      '    :SUMR,'
      '    :TCARD,'
      '    :IDNDS,'
      '    :SUMNDS,'
      '    :SUMOUT0,'
      '    :SUMNDSOUT,'
      '    :OPER'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      
        '    SP.IDHEAD,SP.ID,SP.IDCARD,SP.QUANT,SP.IDM,SP.KM,SP.PRICEIN0,' +
        'SP.SUMIN0,SP.PRICEIN,'
      
        '    SP.SUMIN,SP.PRICER,SP.SUMR,SP.TCARD,SP.IDNDS,SP.SUMNDS,SP.SU' +
        'MOUT0,SP.SUMNDSOUT,'
      '    C.NAME as NAMEC,M.NAMESHORT as SM,N.NAMENDS,SP.OPER'
      'FROM OF_DOCSPECOUTR SP'
      'left join OF_CARDS C on C.ID=SP.IDCARD'
      'left join OF_MESSUR M on M.ID=SP.IDM'
      'left join OF_NDS N on N.ID=SP.IDNDS'
      ''
      'where(  SP.IDHEAD=:IDHD and OPER=:OPER'
      '     ) and (     SP.IDHEAD = :OLD_IDHEAD'
      '    and SP.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      
        '    SP.IDHEAD,SP.ID,SP.IDCARD,SP.QUANT,SP.IDM,SP.KM,SP.PRICEIN0,' +
        'SP.SUMIN0,SP.PRICEIN,'
      
        '    SP.SUMIN,SP.PRICER,SP.SUMR,SP.TCARD,SP.IDNDS,SP.SUMNDS,SP.SU' +
        'MOUT0,SP.SUMNDSOUT,'
      
        '    C.NAME as NAMEC,M.NAMESHORT as SM,N.NAMENDS,SP.OPER, C.CATEG' +
        'ORY'
      'FROM OF_DOCSPECOUTR SP'
      'left join OF_CARDS C on C.ID=SP.IDCARD'
      'left join OF_MESSUR M on M.ID=SP.IDM'
      'left join OF_NDS N on N.ID=SP.IDNDS'
      ''
      'where SP.IDHEAD=:IDHD and OPER=:OPER'
      'ORDER BY SP.ID')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 548
    Top = 488
    poAskRecordCount = True
    object quSpecRSel1IDHEAD: TFIBIntegerField
      FieldName = 'IDHEAD'
    end
    object quSpecRSel1ID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quSpecRSel1IDCARD: TFIBIntegerField
      FieldName = 'IDCARD'
    end
    object quSpecRSel1QUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quSpecRSel1IDM: TFIBIntegerField
      FieldName = 'IDM'
    end
    object quSpecRSel1KM: TFIBFloatField
      FieldName = 'KM'
    end
    object quSpecRSel1PRICEIN: TFIBFloatField
      FieldName = 'PRICEIN'
    end
    object quSpecRSel1SUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quSpecRSel1PRICER: TFIBFloatField
      FieldName = 'PRICER'
    end
    object quSpecRSel1SUMR: TFIBFloatField
      FieldName = 'SUMR'
    end
    object quSpecRSel1TCARD: TFIBSmallIntField
      FieldName = 'TCARD'
    end
    object quSpecRSel1IDNDS: TFIBIntegerField
      FieldName = 'IDNDS'
    end
    object quSpecRSel1SUMNDS: TFIBFloatField
      FieldName = 'SUMNDS'
    end
    object quSpecRSel1NAMEC: TFIBStringField
      FieldName = 'NAMEC'
      Size = 200
      EmptyStrToNull = True
    end
    object quSpecRSel1SM: TFIBStringField
      FieldName = 'SM'
      Size = 50
      EmptyStrToNull = True
    end
    object quSpecRSel1NAMENDS: TFIBStringField
      FieldName = 'NAMENDS'
      Size = 30
      EmptyStrToNull = True
    end
    object quSpecRSel1OPER: TFIBSmallIntField
      FieldName = 'OPER'
    end
    object quSpecRSel1PRICEIN0: TFIBFloatField
      FieldName = 'PRICEIN0'
    end
    object quSpecRSel1SUMIN0: TFIBFloatField
      FieldName = 'SUMIN0'
    end
    object quSpecRSel1SUMOUT0: TFIBFloatField
      FieldName = 'SUMOUT0'
    end
    object quSpecRSel1SUMNDSOUT: TFIBFloatField
      FieldName = 'SUMNDSOUT'
    end
    object quSpecRSel1CATEGORY: TFIBIntegerField
      FieldName = 'CATEGORY'
    end
  end
  object taCardM: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 24
    Top = 239
    object taCardMIdDoc: TIntegerField
      FieldName = 'IdDoc'
    end
    object taCardMTypeD: TIntegerField
      FieldName = 'TypeD'
    end
    object taCardMDateD: TDateField
      FieldName = 'DateD'
    end
    object taCardMsFrom: TStringField
      FieldName = 'sFrom'
      Size = 50
    end
    object taCardMsTo: TStringField
      FieldName = 'sTo'
      Size = 50
    end
    object taCardMiM: TIntegerField
      FieldName = 'iM'
    end
    object taCardMsM: TStringField
      FieldName = 'sM'
      Size = 10
    end
    object taCardMQuant: TFloatField
      FieldName = 'Quant'
      DisplayFormat = '0.000'
    end
    object taCardMQRemn: TFloatField
      FieldName = 'QRemn'
      DisplayFormat = '0.000'
    end
    object taCardMComment: TStringField
      FieldName = 'Comment'
      Size = 100
    end
  end
  object dstaCardM: TDataSource
    DataSet = taCardM
    Left = 80
    Top = 239
  end
  object quTestInput: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHEADOUTB'
      'SET '
      '    DATEDOC = :DATEDOC,'
      '    NUMDOC = :NUMDOC,'
      '    IDCLI = :IDCLI,'
      '    IDSKL = :IDSKL,'
      '    SUMIN = :SUMIN,'
      '    SUMUCH = :SUMUCH,'
      '    IACTIVE = :IACTIVE,'
      '    OPER = :OPER,'
      '    COMMENT = :COMMENT'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHEADOUTB'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHEADOUTB('
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    IDCLI,'
      '    IDSKL,'
      '    SUMIN,'
      '    SUMUCH,'
      '    IACTIVE,'
      '    OPER,'
      '    COMMENT'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATEDOC,'
      '    :NUMDOC,'
      '    :IDCLI,'
      '    :IDSKL,'
      '    :SUMIN,'
      '    :SUMUCH,'
      '    :IACTIVE,'
      '    :OPER,'
      '    :COMMENT'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT dhb.ID,dhb.DATEDOC,dhb.NUMDOC,dhb.IDCLI,dhb.IDSKL,mh.NAME' +
        'MH,dhb.SUMIN,dhb.SUMUCH,dhb.IACTIVE,dhb.OPER,dhb.COMMENT'
      'FROM OF_DOCHEADOUTB dhb'
      'left join of_mh mh on mh.ID=dhb.IDSKL'
      'where(  dhb.DATEDOC=:DDATE and dhb.IDCLI=:IDB'
      '     ) and (     DHB.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT dhb.ID,dhb.DATEDOC,dhb.NUMDOC,dhb.IDCLI,dhb.IDSKL,mh.NAME' +
        'MH,dhb.SUMIN,dhb.SUMUCH,dhb.IACTIVE,dhb.OPER,dhb.COMMENT'
      'FROM OF_DOCHEADOUTB dhb'
      'left join of_mh mh on mh.ID=dhb.IDSKL'
      'where dhb.DATEDOC=:DDATE and dhb.IDCLI=:IDB')
    Transaction = trSelRep
    Database = dmO.OfficeRnDb
    UpdateTransaction = trD
    AutoCommit = True
    Left = 400
    Top = 608
    poAskRecordCount = True
    object quTestInputID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quTestInputDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object quTestInputNUMDOC: TFIBStringField
      FieldName = 'NUMDOC'
      Size = 15
      EmptyStrToNull = True
    end
    object quTestInputIDCLI: TFIBIntegerField
      FieldName = 'IDCLI'
    end
    object quTestInputIDSKL: TFIBIntegerField
      FieldName = 'IDSKL'
    end
    object quTestInputNAMEMH: TFIBStringField
      FieldName = 'NAMEMH'
      Size = 100
      EmptyStrToNull = True
    end
    object quTestInputSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quTestInputIACTIVE: TFIBIntegerField
      FieldName = 'IACTIVE'
    end
    object quTestInputOPER: TFIBStringField
      FieldName = 'OPER'
      EmptyStrToNull = True
    end
    object quTestInputCOMMENT: TFIBStringField
      FieldName = 'COMMENT'
      Size = 100
      EmptyStrToNull = True
    end
    object quTestInputSUMUCH: TFIBFloatField
      FieldName = 'SUMUCH'
    end
  end
  object dsquTestInput: TDataSource
    DataSet = quTestInput
    Left = 472
    Top = 608
  end
  object teCalcB1: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 40
    Top = 604
    object teCalcB1ID: TIntegerField
      FieldName = 'ID'
    end
    object teCalcB1CODEB: TIntegerField
      FieldName = 'CODEB'
    end
    object teCalcB1NAMEB: TStringField
      FieldName = 'NAMEB'
      Size = 30
    end
    object teCalcB1QUANT: TFloatField
      FieldName = 'QUANT'
      DisplayFormat = '0.000'
    end
    object teCalcB1PRICEOUT: TFloatField
      FieldName = 'PRICEOUT'
      DisplayFormat = '0.00'
    end
    object teCalcB1SUMOUT: TFloatField
      FieldName = 'SUMOUT'
      DisplayFormat = '0.00'
    end
    object teCalcB1IDCARD: TIntegerField
      FieldName = 'IDCARD'
    end
    object teCalcB1NAMEC: TStringField
      FieldName = 'NAMEC'
      Size = 30
    end
    object teCalcB1QUANTC: TFloatField
      FieldName = 'QUANTC'
      DisplayFormat = '0.000'
    end
    object teCalcB1PRICEIN: TFloatField
      FieldName = 'PRICEIN'
      DisplayFormat = '0.00'
    end
    object teCalcB1SUMIN: TFloatField
      FieldName = 'SUMIN'
      DisplayFormat = '0.00'
    end
    object teCalcB1IM: TIntegerField
      FieldName = 'IM'
    end
    object teCalcB1SM: TStringField
      FieldName = 'SM'
    end
    object teCalcB1SB: TStringField
      FieldName = 'SB'
      Size = 10
    end
    object teCalcB1PRICEIN0: TFloatField
      FieldName = 'PRICEIN0'
      DisplayFormat = '0.00'
    end
    object teCalcB1SUMIN0: TFloatField
      FieldName = 'SUMIN0'
      DisplayFormat = '0.00'
    end
  end
  object dsteCalcB1: TDataSource
    DataSet = teCalcB1
    Left = 96
    Top = 604
  end
  object quDocsRCard: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHEADOUTR'
      'SET '
      '    DATEDOC = :DATEDOC,'
      '    NUMDOC = :NUMDOC,'
      '    DATESF = :DATESF,'
      '    NUMSF = :NUMSF,'
      '    IDCLI = :IDCLI,'
      '    IDSKL = :IDSKL,'
      '    SUMIN = :SUMIN,'
      '    SUMUCH = :SUMUCH,'
      '    SUMTAR = :SUMTAR,'
      '    SUMNDS0 = :SUMNDS0,'
      '    SUMNDS1 = :SUMNDS1,'
      '    SUMNDS2 = :SUMNDS2,'
      '    PROCNAC = :PROCNAC,'
      '    IACTIVE = :IACTIVE,'
      '    IDFROM = :IDFROM'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHEADOUTR'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHEADOUTR('
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    DATESF,'
      '    NUMSF,'
      '    IDCLI,'
      '    IDSKL,'
      '    SUMIN,'
      '    SUMUCH,'
      '    SUMTAR,'
      '    SUMNDS0,'
      '    SUMNDS1,'
      '    SUMNDS2,'
      '    PROCNAC,'
      '    IACTIVE,'
      '    IDFROM'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATEDOC,'
      '    :NUMDOC,'
      '    :DATESF,'
      '    :NUMSF,'
      '    :IDCLI,'
      '    :IDSKL,'
      '    :SUMIN,'
      '    :SUMUCH,'
      '    :SUMTAR,'
      '    :SUMNDS0,'
      '    :SUMNDS1,'
      '    :SUMNDS2,'
      '    :PROCNAC,'
      '    :IACTIVE,'
      '    :IDFROM'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT dh.ID,dh.DATEDOC,dh.NUMDOC,dh.DATESF,dh.NUMSF,dh.IDCLI,dh' +
        '.IDSKL,dh.SUMIN,'
      
        '       dh.SUMUCH,dh.SUMTAR,dh.SUMNDS0,dh.SUMNDS1,dh.SUMNDS2,dh.P' +
        'ROCNAC,dh.IACTIVE,'
      '       cl.NAMECL,mh.NAMEMH,dh.IDFROM'
      'FROM OF_DOCHEADOUTR dh'
      'left join OF_CLIENTS cl on cl.ID=dh.IDCLI'
      'left join OF_MH mh on mh.ID=dh.IDSKL'
      ''
      'Where(  dh.DATEDOC>=:DATEB and dh.DATEDOC<:DATEE'
      '     ) and (     DH.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT sr.IDCARD,ca.NAME,sr.IDM,me.NAMESHORT,sr.KM,ca.CATEGORY,c' +
        'a.RCATEGORY,rc.NAMECAT,'
      'dh.DATEDOC, dh.NUMDOC, dh.IDCLI, dh.IDSKL, dh.IDFROM,'
      'mh1.NAMEMH as MHTO, cli.NAMECL,'
      'ds.IDATE,ds.KVART,ds.WEEKOFM,ds.DAYWEEK,'
      'sr.QUANT,sr.SUMIN,sr.SUMR,sr.QUANTDOC,'
      'ca.VOL,ca.ALGCLASS,ca.RKREP'
      'FROM OF_DOCSPECOUTR sr'
      'left join of_docheadoutr dh on dh.ID=sr.IDHEAD'
      'left join of_cards ca on ca.ID=sr.IDCARD'
      'left join of_messur me on me.ID=sr.IDM'
      'left join of_mh mh1 on mh1.ID=dh.IDSKL'
      'left join of_clients cli on cli.ID=dh.IDCLI'
      'left join of_Dates ds on ds.DDATE=dh.DATEDOC'
      'left join OF_CARDSRCATEGORY rc on rc.ID=ca.RCATEGORY'
      ''
      'where'
      'dh.DATEDOC>=:DATEB and dh.DATEDOC<:DATEE'
      'and'
      
        '( dh.IDSKL in (SELECT IDMH FROM RPERSONALMH where IDPERSON=:IDPE' +
        'RSON)'
      'or'
      
        '  dh.IDFROM in (SELECT IDMH FROM RPERSONALMH where IDPERSON=:IDP' +
        'ERSON1)'
      ')'
      ''
      'and dh.IACTIVE=1')
    OnCalcFields = quDocsRCardCalcFields
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    Left = 612
    Top = 572
    poAskRecordCount = True
    object quDocsRCardIDCARD: TFIBIntegerField
      FieldName = 'IDCARD'
    end
    object quDocsRCardNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 200
      EmptyStrToNull = True
    end
    object quDocsRCardIDM: TFIBIntegerField
      FieldName = 'IDM'
    end
    object quDocsRCardNAMESHORT: TFIBStringField
      FieldName = 'NAMESHORT'
      Size = 50
      EmptyStrToNull = True
    end
    object quDocsRCardKM: TFIBFloatField
      FieldName = 'KM'
    end
    object quDocsRCardCATEGORY: TFIBIntegerField
      FieldName = 'CATEGORY'
    end
    object quDocsRCardRCATEGORY: TFIBIntegerField
      FieldName = 'RCATEGORY'
    end
    object quDocsRCardNAMECAT: TFIBStringField
      FieldName = 'NAMECAT'
      Size = 100
      EmptyStrToNull = True
    end
    object quDocsRCardDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object quDocsRCardIDCLI: TFIBIntegerField
      FieldName = 'IDCLI'
    end
    object quDocsRCardIDSKL: TFIBIntegerField
      FieldName = 'IDSKL'
    end
    object quDocsRCardIDFROM: TFIBIntegerField
      FieldName = 'IDFROM'
    end
    object quDocsRCardMHTO: TFIBStringField
      FieldName = 'MHTO'
      Size = 100
      EmptyStrToNull = True
    end
    object quDocsRCardNAMECL: TFIBStringField
      FieldName = 'NAMECL'
      Size = 100
      EmptyStrToNull = True
    end
    object quDocsRCardIDATE: TFIBIntegerField
      FieldName = 'IDATE'
    end
    object quDocsRCardKVART: TFIBSmallIntField
      FieldName = 'KVART'
    end
    object quDocsRCardWEEKOFM: TFIBIntegerField
      FieldName = 'WEEKOFM'
    end
    object quDocsRCardDAYWEEK: TFIBSmallIntField
      FieldName = 'DAYWEEK'
    end
    object quDocsRCardQUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quDocsRCardSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quDocsRCardSUMR: TFIBFloatField
      FieldName = 'SUMR'
    end
    object quDocsRCardQUANTDOC: TFIBFloatField
      FieldName = 'QUANTDOC'
    end
    object quDocsRCardVOL: TFIBFloatField
      FieldName = 'VOL'
    end
    object quDocsRCardALGCLASS: TFIBIntegerField
      FieldName = 'ALGCLASS'
    end
    object quDocsRCardRKREP: TFIBFloatField
      FieldName = 'RKREP'
    end
    object quDocsRCardOPER: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'OPER'
      Calculated = True
    end
    object quDocsRCardRNac: TFloatField
      FieldKind = fkCalculated
      FieldName = 'RNac'
      Calculated = True
    end
    object quDocsRCardNUMDOC: TFIBStringField
      FieldName = 'NUMDOC'
      Size = 15
      EmptyStrToNull = True
    end
  end
  object dsquDocsRCard: TDataSource
    DataSet = quDocsRCard
    Left = 612
    Top = 612
  end
  object quSpecRSel2: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCSPECOUTR'
      'SET '
      '    IDCARD = :IDCARD,'
      '    QUANT = :QUANT,'
      '    IDM = :IDM,'
      '    KM = :KM,'
      '    PRICEIN0 = :PRICEIN0,'
      '    SUMIN0 = :SUMIN0,'
      '    PRICEIN = :PRICEIN,'
      '    SUMIN = :SUMIN,'
      '    PRICER = :PRICER,'
      '    SUMR = :SUMR,'
      '    TCARD = :TCARD,'
      '    IDNDS = :IDNDS,'
      '    SUMNDS = :SUMNDS,'
      '    SUMOUT0 = :SUMOUT0,'
      '    SUMNDSOUT = :SUMNDSOUT,'
      '    OPER = :OPER,'
      '    BZQUANTR = :BZQUANTR,'
      '    BZQUANTF = :BZQUANTF,'
      '    BZQUANTS = :BZQUANTS,'
      '    BZQUANTSHOP = :BZQUANTSHOP,'
      '    QUANTDOC = :QUANTDOC'
      'WHERE'
      '    IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCSPECOUTR'
      'WHERE'
      '        IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCSPECOUTR('
      '    IDHEAD,'
      '    ID,'
      '    IDCARD,'
      '    QUANT,'
      '    IDM,'
      '    KM,'
      '    PRICEIN0,'
      '    SUMIN0,'
      '    PRICEIN,'
      '    SUMIN,'
      '    PRICER,'
      '    SUMR,'
      '    TCARD,'
      '    IDNDS,'
      '    SUMNDS,'
      '    SUMOUT0,'
      '    SUMNDSOUT,'
      '    OPER,'
      '    BZQUANTR,'
      '    BZQUANTF,'
      '    BZQUANTS,'
      '    BZQUANTSHOP,'
      '    QUANTDOC'
      ')'
      'VALUES('
      '    :IDHEAD,'
      '    :ID,'
      '    :IDCARD,'
      '    :QUANT,'
      '    :IDM,'
      '    :KM,'
      '    :PRICEIN0,'
      '    :SUMIN0,'
      '    :PRICEIN,'
      '    :SUMIN,'
      '    :PRICER,'
      '    :SUMR,'
      '    :TCARD,'
      '    :IDNDS,'
      '    :SUMNDS,'
      '    :SUMOUT0,'
      '    :SUMNDSOUT,'
      '    :OPER,'
      '    :BZQUANTR,'
      '    :BZQUANTF,'
      '    :BZQUANTS,'
      '    :BZQUANTSHOP,'
      '    :QUANTDOC'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      
        '    SP.IDHEAD,SP.ID,SP.IDCARD,SP.QUANT,SP.IDM,SP.KM,SP.PRICEIN0,' +
        'SP.SUMIN0,SP.PRICEIN,SP.SUMIN'
      
        '    ,SP.PRICER,SP.SUMR,SP.TCARD,SP.IDNDS,SP.SUMNDS,SP.SUMOUT0,SP' +
        '.SUMNDSOUT,'
      
        '    C.NAME as NAMEC,C.CATEGORY,M.NAMESHORT as SM,N.NAMENDS,SP.OP' +
        'ER,N.PROC,(SP.SUMR*100/(100+N.PROC)) as OUTNDSSUM,'
      '    cto.ID,cto.NAME as NAMECTO,M.CODE,C.RKREP,'
      
        '    C.INDS,SP.BZQUANTR,SP.BZQUANTF,SP.BZQUANTS,SP.BZQUANTSHOP,SP' +
        '.QUANTDOC,    '
      '    algr.RKREP1,algr.RKREP2,algr.NAMEKREP,'
      '    akc.RVAL*SP.QUANT*C.VOL as RVAL,'
      '    SP.QUANT*C.VOL/10 as DVOL,'
      '    C.SERT'
      'FROM OF_DOCSPECOUTR SP'
      'left join OF_CARDS C on C.ID=SP.IDCARD'
      'left join OF_MESSUR M on M.ID=SP.IDM'
      'left join OF_NDS N on N.ID=SP.IDNDS'
      'left join OF_CTO cto on cto.ID=C.CTO'
      
        'left join of_alcogr algr on (C.RKREP>=algr.RKREP1 and C.RKREP<=a' +
        'lgr.RKREP2)'
      'left join of_akciz akc on (akc.IGR=algr.ID)'
      ''
      'where(  SP.IDHEAD=:IDHD'
      'and akc.IY=:IYY'
      'and ((SP.QUANT>0.0001)or(SP.QUANT<-0.0001))'
      '     ) and (     SP.IDHEAD = :OLD_IDHEAD'
      '    and SP.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      
        '    SP.IDHEAD,SP.ID,SP.IDCARD,SP.QUANT,SP.IDM,SP.KM,SP.PRICEIN0,' +
        'SP.SUMIN0,SP.PRICEIN,SP.SUMIN'
      
        '    ,SP.PRICER,SP.SUMR,SP.TCARD,SP.IDNDS,SP.SUMNDS,SP.SUMOUT0,SP' +
        '.SUMNDSOUT,'
      
        '    C.NAME as NAMEC,C.CATEGORY,M.NAMESHORT as SM,N.NAMENDS,SP.OP' +
        'ER,N.PROC,(SP.SUMR*100/(100+N.PROC)) as OUTNDSSUM,'
      '    cto.ID,cto.NAME as NAMECTO,M.CODE,C.RKREP,'
      
        '    C.INDS,SP.BZQUANTR,SP.BZQUANTF,SP.BZQUANTS,SP.BZQUANTSHOP,SP' +
        '.QUANTDOC,    '
      '    algr.RKREP1,algr.RKREP2,algr.NAMEKREP,'
      '    akc.RVAL*SP.QUANT*C.VOL as RVAL,'
      '    SP.QUANT*C.VOL/10 as DVOL,'
      '    C.SERT,C.VES'
      'FROM OF_DOCSPECOUTR SP'
      'left join OF_CARDS C on C.ID=SP.IDCARD'
      'left join OF_MESSUR M on M.ID=SP.IDM'
      'left join OF_NDS N on N.ID=SP.IDNDS'
      'left join OF_CTO cto on cto.ID=C.CTO'
      
        'left join of_alcogr algr on (C.RKREP>=algr.RKREP1 and C.RKREP<=a' +
        'lgr.RKREP2)'
      'left join of_akciz akc on (akc.IGR=algr.ID)'
      ''
      'where SP.IDHEAD=:IDHD'
      'and akc.IY=:IYY'
      'and ((SP.QUANT>0.0001)or(SP.QUANT<-0.0001))'
      'ORDER BY SP.ID')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 552
    Top = 536
    object quSpecRSel2IDHEAD: TFIBIntegerField
      FieldName = 'IDHEAD'
    end
    object quSpecRSel2ID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quSpecRSel2IDCARD: TFIBIntegerField
      FieldName = 'IDCARD'
    end
    object quSpecRSel2QUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quSpecRSel2IDM: TFIBIntegerField
      FieldName = 'IDM'
    end
    object quSpecRSel2KM: TFIBFloatField
      FieldName = 'KM'
    end
    object quSpecRSel2PRICEIN0: TFIBFloatField
      FieldName = 'PRICEIN0'
    end
    object quSpecRSel2SUMIN0: TFIBFloatField
      FieldName = 'SUMIN0'
    end
    object quSpecRSel2PRICEIN: TFIBFloatField
      FieldName = 'PRICEIN'
    end
    object quSpecRSel2SUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quSpecRSel2PRICER: TFIBFloatField
      FieldName = 'PRICER'
    end
    object quSpecRSel2SUMR: TFIBFloatField
      FieldName = 'SUMR'
    end
    object quSpecRSel2TCARD: TFIBSmallIntField
      FieldName = 'TCARD'
    end
    object quSpecRSel2IDNDS: TFIBIntegerField
      FieldName = 'IDNDS'
    end
    object quSpecRSel2SUMNDS: TFIBFloatField
      FieldName = 'SUMNDS'
    end
    object quSpecRSel2SUMOUT0: TFIBFloatField
      FieldName = 'SUMOUT0'
    end
    object quSpecRSel2SUMNDSOUT: TFIBFloatField
      FieldName = 'SUMNDSOUT'
    end
    object quSpecRSel2NAMEC: TFIBStringField
      FieldName = 'NAMEC'
      Size = 200
      EmptyStrToNull = True
    end
    object quSpecRSel2CATEGORY: TFIBIntegerField
      FieldName = 'CATEGORY'
    end
    object quSpecRSel2SM: TFIBStringField
      FieldName = 'SM'
      Size = 50
      EmptyStrToNull = True
    end
    object quSpecRSel2NAMENDS: TFIBStringField
      FieldName = 'NAMENDS'
      Size = 30
      EmptyStrToNull = True
    end
    object quSpecRSel2OPER: TFIBSmallIntField
      FieldName = 'OPER'
    end
    object quSpecRSel2PROC: TFIBFloatField
      FieldName = 'PROC'
    end
    object quSpecRSel2OUTNDSSUM: TFIBFloatField
      FieldName = 'OUTNDSSUM'
    end
    object quSpecRSel2ID1: TFIBIntegerField
      FieldName = 'ID1'
    end
    object quSpecRSel2NAMECTO: TFIBStringField
      FieldName = 'NAMECTO'
      Size = 50
      EmptyStrToNull = True
    end
    object quSpecRSel2CODE: TFIBIntegerField
      FieldName = 'CODE'
    end
    object quSpecRSel2RKREP: TFIBFloatField
      FieldName = 'RKREP'
    end
    object quSpecRSel2INDS: TFIBIntegerField
      FieldName = 'INDS'
    end
    object quSpecRSel2BZQUANTR: TFIBFloatField
      FieldName = 'BZQUANTR'
    end
    object quSpecRSel2BZQUANTF: TFIBFloatField
      FieldName = 'BZQUANTF'
    end
    object quSpecRSel2BZQUANTS: TFIBFloatField
      FieldName = 'BZQUANTS'
    end
    object quSpecRSel2BZQUANTSHOP: TFIBFloatField
      FieldName = 'BZQUANTSHOP'
    end
    object quSpecRSel2QUANTDOC: TFIBFloatField
      FieldName = 'QUANTDOC'
    end
    object quSpecRSel2RKREP1: TFIBFloatField
      FieldName = 'RKREP1'
    end
    object quSpecRSel2RKREP2: TFIBFloatField
      FieldName = 'RKREP2'
    end
    object quSpecRSel2NAMEKREP: TFIBStringField
      FieldName = 'NAMEKREP'
      Size = 50
      EmptyStrToNull = True
    end
    object quSpecRSel2RVAL: TFIBFloatField
      FieldName = 'RVAL'
    end
    object quSpecRSel2DVOL: TFIBFloatField
      FieldName = 'DVOL'
    end
    object quSpecRSel2SERT: TFIBBlobField
      FieldName = 'SERT'
      Size = 8
    end
    object quSpecRSel2VES: TFIBFloatField
      FieldName = 'VES'
    end
  end
  object taCalcB1: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 152
    Top = 136
    object taCalcB1ID: TIntegerField
      FieldName = 'ID'
    end
    object taCalcB1CODEB: TIntegerField
      FieldName = 'CODEB'
    end
    object taCalcB1NAMEB: TStringField
      FieldName = 'NAMEB'
      Size = 30
    end
    object taCalcB1QUANT: TFloatField
      FieldName = 'QUANT'
      DisplayFormat = '0.000'
    end
    object taCalcB1PRICEOUT: TFloatField
      FieldName = 'PRICEOUT'
      DisplayFormat = '0.00'
    end
    object taCalcB1SUMOUT: TFloatField
      FieldName = 'SUMOUT'
      DisplayFormat = '0.00'
    end
    object taCalcB1IDCARD: TIntegerField
      FieldName = 'IDCARD'
    end
    object taCalcB1NAMEC: TStringField
      FieldName = 'NAMEC'
      Size = 30
    end
    object taCalcB1QUANTC: TFloatField
      FieldName = 'QUANTC'
      DisplayFormat = '0.000'
    end
    object taCalcB1PRICEIN: TFloatField
      FieldName = 'PRICEIN'
      DisplayFormat = '0.00'
    end
    object taCalcB1SUMIN: TFloatField
      FieldName = 'SUMIN'
      DisplayFormat = '0.00'
    end
    object taCalcB1IM: TIntegerField
      FieldName = 'IM'
    end
    object taCalcB1SM: TStringField
      FieldName = 'SM'
    end
    object taCalcB1SB: TStringField
      FieldName = 'SB'
      Size = 10
    end
    object taCalcB1PRICEIN0: TFloatField
      FieldName = 'PRICEIN0'
    end
    object taCalcB1SUMIN0: TFloatField
      FieldName = 'SUMIN0'
    end
  end
  object quSetSync: TpFIBQuery
    Transaction = trSetSync
    Database = dmO.OfficeRnDb
    Left = 880
    Top = 128
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object quGetSync: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_SYNC'
      'SET '
      '    DOCTYPE = :DOCTYPE,'
      '    IDSKL = :IDSKL,'
      '    IPERS = :IPERS,'
      '    TIMEEDIT = :TIMEEDIT,'
      '    STYPEEDIT = :STYPEEDIT,'
      '    IDDOC = :IDDOC'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_SYNC'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_SYNC('
      '    ID,'
      '    DOCTYPE,'
      '    IDSKL,'
      '    IPERS,'
      '    TIMEEDIT,'
      '    STYPEEDIT,'
      '    IDDOC'
      ')'
      'VALUES('
      '    :ID,'
      '    :DOCTYPE,'
      '    :IDSKL,'
      '    :IPERS,'
      '    :TIMEEDIT,'
      '    :STYPEEDIT,'
      '    :IDDOC'
      ')')
    RefreshSQL.Strings = (
      'SELECT ID,DOCTYPE,IDSKL,IPERS,TIMEEDIT,STYPEEDIT,IDDOC'
      'FROM OF_SYNC'
      'where(  DOCTYPE=8'
      'and IPERS<>:PERS1'
      'and TIMEEDIT>=:DACT'
      
        'and IDSKL in (SELECT IDMH FROM RPERSONALMH where IDPERSON=:PERS2' +
        ')'
      '     ) and (     OF_SYNC.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT ID,DOCTYPE,IDSKL,IPERS,TIMEEDIT,STYPEEDIT,IDDOC'
      'FROM OF_SYNC'
      'where DOCTYPE=8'
      'and IPERS<>:PERS1'
      'and TIMEEDIT>=:DACT'
      
        'and IDSKL in (SELECT IDMH FROM RPERSONALMH where IDPERSON=:PERS2' +
        ')')
    Transaction = trGetSync
    Database = dmO.OfficeRnDb
    UpdateTransaction = trSetSync
    AutoCommit = True
    Left = 880
    Top = 184
    poAskRecordCount = True
    object quGetSyncID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quGetSyncDOCTYPE: TFIBSmallIntField
      FieldName = 'DOCTYPE'
    end
    object quGetSyncIDSKL: TFIBIntegerField
      FieldName = 'IDSKL'
    end
    object quGetSyncIPERS: TFIBIntegerField
      FieldName = 'IPERS'
    end
    object quGetSyncTIMEEDIT: TFIBDateTimeField
      FieldName = 'TIMEEDIT'
    end
    object quGetSyncSTYPEEDIT: TFIBStringField
      FieldName = 'STYPEEDIT'
      Size = 3
      EmptyStrToNull = True
    end
    object quGetSyncIDDOC: TFIBIntegerField
      FieldName = 'IDDOC'
    end
  end
  object trSetSync: TpFIBTransaction
    DefaultDatabase = dmO.OfficeRnDb
    TimeoutAction = TARollback
    Left = 876
    Top = 16
  end
  object trGetSync: TpFIBTransaction
    DefaultDatabase = dmO.OfficeRnDb
    TimeoutAction = TARollback
    Left = 876
    Top = 72
  end
  object quSpecRToCB: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCSPECOUTR'
      'SET '
      '    IDCARD = :IDCARD,'
      '    QUANT = :QUANT,'
      '    IDM = :IDM,'
      '    KM = :KM,'
      '    PRICEIN0 = :PRICEIN0,'
      '    SUMIN0 = :SUMIN0,'
      '    PRICEIN = :PRICEIN,'
      '    SUMIN = :SUMIN,'
      '    PRICER = :PRICER,'
      '    SUMR = :SUMR,'
      '    TCARD = :TCARD,'
      '    IDNDS = :IDNDS,'
      '    SUMNDS = :SUMNDS,'
      '    SUMOUT0 = :SUMOUT0,'
      '    SUMNDSOUT = :SUMNDSOUT,'
      '    OPER = :OPER,'
      '    BZQUANTR = :BZQUANTR,'
      '    BZQUANTF = :BZQUANTF,'
      '    BZQUANTS = :BZQUANTS'
      'WHERE'
      '    IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCSPECOUTR'
      'WHERE'
      '        IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCSPECOUTR('
      '    IDHEAD,'
      '    ID,'
      '    IDCARD,'
      '    QUANT,'
      '    IDM,'
      '    KM,'
      '    PRICEIN0,'
      '    SUMIN0,'
      '    PRICEIN,'
      '    SUMIN,'
      '    PRICER,'
      '    SUMR,'
      '    TCARD,'
      '    IDNDS,'
      '    SUMNDS,'
      '    SUMOUT0,'
      '    SUMNDSOUT,'
      '    OPER,'
      '    BZQUANTR,'
      '    BZQUANTF,'
      '    BZQUANTS'
      ')'
      'VALUES('
      '    :IDHEAD,'
      '    :ID,'
      '    :IDCARD,'
      '    :QUANT,'
      '    :IDM,'
      '    :KM,'
      '    :PRICEIN0,'
      '    :SUMIN0,'
      '    :PRICEIN,'
      '    :SUMIN,'
      '    :PRICER,'
      '    :SUMR,'
      '    :TCARD,'
      '    :IDNDS,'
      '    :SUMNDS,'
      '    :SUMOUT0,'
      '    :SUMNDSOUT,'
      '    :OPER,'
      '    :BZQUANTR,'
      '    :BZQUANTF,'
      '    :BZQUANTS'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      
        '    SP.IDHEAD,SP.ID,SP.IDCARD,SP.QUANT,SP.IDM,SP.KM,SP.PRICEIN0,' +
        'SP.SUMIN0,SP.PRICEIN,SP.SUMIN'
      
        '    ,SP.PRICER,SP.SUMR,SP.TCARD,SP.IDNDS,SP.SUMNDS,SP.SUMOUT0,SP' +
        '.SUMNDSOUT,'
      
        '    C.NAME as NAMEC,C.CATEGORY,M.NAMESHORT as SM,N.NAMENDS,SP.OP' +
        'ER,N.PROC,(SP.SUMR*100/(100+N.PROC)) as OUTNDSSUM,'
      '    cto.ID,cto.NAME as NAMECTO,'
      '    C.INDS,SP.BZQUANTR,SP.BZQUANTF,SP.BZQUANTS '
      ''
      'FROM OF_DOCSPECOUTR SP'
      'left join OF_CARDS C on C.ID=SP.IDCARD'
      'left join OF_MESSUR M on M.ID=SP.IDM'
      'left join OF_NDS N on N.ID=SP.IDNDS'
      'left join OF_CTO cto on cto.ID=C.CTO'
      ''
      'where(  SP.IDHEAD=:IDHD'
      '     ) and (     SP.IDHEAD = :OLD_IDHEAD'
      '    and SP.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT sp.IDHEAD,'
      '       sp.ID,'
      '       sp.IDCARD,'
      '       sp.QUANT,'
      '       sp.IDM,'
      '       sp.KM,'
      '       sp.PRICEIN,'
      '       sp.SUMIN,'
      '       sp.PRICER,'
      '       sp.SUMR,'
      '       sp.TCARD,'
      '       sp.IDNDS,'
      '       sp.SUMNDS,'
      '       sp.OPER,'
      '       sp.PRICEIN0,'
      '       sp.SUMIN0,'
      '       sp.SUMOUT0,'
      '       sp.SUMNDSOUT,'
      '       sp.BZQUANTR,'
      '       sp.BZQUANTF,'
      '       sp.BZQUANTS,'
      '       ca.CODEZAK,'
      '       ca.CATEGORY,'
      '       nds.PROC,'
      '       ca.NAME'
      'FROM OF_DOCSPECOUTR sp'
      'left join OF_CARDS ca on ca.ID=sp.IDCARD'
      'left join OF_NDS nds on nds.ID=sp.IDNDS'
      'where sp.IDHEAD=:IDH'
      'order by sp.ID')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 400
    Top = 456
    object quSpecRToCBIDHEAD: TFIBIntegerField
      FieldName = 'IDHEAD'
    end
    object quSpecRToCBID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quSpecRToCBIDCARD: TFIBIntegerField
      FieldName = 'IDCARD'
    end
    object quSpecRToCBQUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quSpecRToCBIDM: TFIBIntegerField
      FieldName = 'IDM'
    end
    object quSpecRToCBKM: TFIBFloatField
      FieldName = 'KM'
    end
    object quSpecRToCBPRICEIN: TFIBFloatField
      FieldName = 'PRICEIN'
    end
    object quSpecRToCBSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quSpecRToCBPRICER: TFIBFloatField
      FieldName = 'PRICER'
    end
    object quSpecRToCBSUMR: TFIBFloatField
      FieldName = 'SUMR'
    end
    object quSpecRToCBTCARD: TFIBSmallIntField
      FieldName = 'TCARD'
    end
    object quSpecRToCBIDNDS: TFIBIntegerField
      FieldName = 'IDNDS'
    end
    object quSpecRToCBSUMNDS: TFIBFloatField
      FieldName = 'SUMNDS'
    end
    object quSpecRToCBOPER: TFIBSmallIntField
      FieldName = 'OPER'
    end
    object quSpecRToCBPRICEIN0: TFIBFloatField
      FieldName = 'PRICEIN0'
    end
    object quSpecRToCBSUMIN0: TFIBFloatField
      FieldName = 'SUMIN0'
    end
    object quSpecRToCBSUMOUT0: TFIBFloatField
      FieldName = 'SUMOUT0'
    end
    object quSpecRToCBSUMNDSOUT: TFIBFloatField
      FieldName = 'SUMNDSOUT'
    end
    object quSpecRToCBBZQUANTR: TFIBFloatField
      FieldName = 'BZQUANTR'
    end
    object quSpecRToCBBZQUANTF: TFIBFloatField
      FieldName = 'BZQUANTF'
    end
    object quSpecRToCBBZQUANTS: TFIBFloatField
      FieldName = 'BZQUANTS'
    end
    object quSpecRToCBCODEZAK: TFIBIntegerField
      FieldName = 'CODEZAK'
    end
    object quSpecRToCBCATEGORY: TFIBIntegerField
      FieldName = 'CATEGORY'
    end
    object quSpecRToCBPROC: TFIBFloatField
      FieldName = 'PROC'
    end
    object quSpecRToCBNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 200
      EmptyStrToNull = True
    end
  end
  object quAlgClass: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_CLASSIFALG'
      'SET '
      '    NAMECLA = :NAMECLA'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_CLASSIFALG'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_CLASSIFALG('
      '    ID,'
      '    NAMECLA'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAMECLA'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    NAMECLA'
      'FROM'
      '    OF_CLASSIFALG '
      ''
      ' WHERE '
      '        OF_CLASSIFALG.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    NAMECLA'
      'FROM'
      '    OF_CLASSIFALG ')
    Transaction = trSelRep
    Database = dmO.OfficeRnDb
    UpdateTransaction = trD
    AutoCommit = True
    Left = 32
    Top = 664
    poAskRecordCount = True
    object quAlgClassID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quAlgClassNAMECLA: TFIBStringField
      FieldName = 'NAMECLA'
      Size = 200
      EmptyStrToNull = True
    end
  end
  object dsquAlgClass: TDataSource
    DataSet = quAlgClass
    Left = 96
    Top = 664
  end
  object quMakers: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_MAKERS'
      'SET '
      '    NAMEM = :NAMEM,'
      '    INNM = :INNM,'
      '    KPPM = :KPPM'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_MAKERS'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_MAKERS('
      '    ID,'
      '    NAMEM,'
      '    INNM,'
      '    KPPM'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAMEM,'
      '    :INNM,'
      '    :KPPM'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    NAMEM,'
      '    INNM,'
      '    KPPM'
      'FROM'
      '    OF_MAKERS '
      ''
      ' WHERE '
      '        OF_MAKERS.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    NAMEM,'
      '    INNM,'
      '    KPPM'
      'FROM'
      '    OF_MAKERS ')
    Transaction = trSelRep
    Database = dmO.OfficeRnDb
    UpdateTransaction = trD
    AutoCommit = True
    Left = 32
    Top = 724
    poAskRecordCount = True
    object quMakersID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quMakersNAMEM: TFIBStringField
      FieldName = 'NAMEM'
      Size = 200
      EmptyStrToNull = True
    end
    object quMakersINNM: TFIBStringField
      FieldName = 'INNM'
      Size = 15
      EmptyStrToNull = True
    end
    object quMakersKPPM: TFIBStringField
      FieldName = 'KPPM'
      Size = 15
      EmptyStrToNull = True
    end
  end
  object dsquMakers: TDataSource
    DataSet = quMakers
    Left = 96
    Top = 724
  end
  object dsquAlgClass1: TDataSource
    DataSet = quAlgClass
    Left = 172
    Top = 664
  end
  object dsquMakers1: TDataSource
    DataSet = quMakers
    Left = 172
    Top = 724
  end
  object dsquCliLic: TDataSource
    DataSet = quCliLic
    Left = 96
    Top = 784
  end
  object quCardsAlg: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_CARDS'
      'SET '
      '    PARENT = :PARENT,'
      '    NAME = :NAME,'
      '    TTYPE = :TTYPE,'
      '    IMESSURE = :IMESSURE,'
      '    INDS = :INDS,'
      '    MINREST = :MINREST,'
      '    LASTPRICEIN = :LASTPRICEIN,'
      '    LASTPRICEOUT = :LASTPRICEOUT,'
      '    LASTPOST = :LASTPOST,'
      '    IACTIVE = :IACTIVE,'
      '    TCARD = :TCARD,'
      '    CATEGORY = :CATEGORY,'
      '    SPISSTORE = :SPISSTORE,'
      '    BB = :BB,'
      '    GG = :GG,'
      '    U1 = :U1,'
      '    U2 = :U2,'
      '    EE = :EE,'
      '    RCATEGORY = :RCATEGORY,'
      '    COMMENT = :COMMENT,'
      '    CTO = :CTO,'
      '    CODEZAK = :CODEZAK,'
      '    ALGCLASS = :ALGCLASS,'
      '    ALGMAKER = :ALGMAKER'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_CARDS'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_CARDS('
      '    ID,'
      '    PARENT,'
      '    NAME,'
      '    TTYPE,'
      '    IMESSURE,'
      '    INDS,'
      '    MINREST,'
      '    LASTPRICEIN,'
      '    LASTPRICEOUT,'
      '    LASTPOST,'
      '    IACTIVE,'
      '    TCARD,'
      '    CATEGORY,'
      '    SPISSTORE,'
      '    BB,'
      '    GG,'
      '    U1,'
      '    U2,'
      '    EE,'
      '    RCATEGORY,'
      '    COMMENT,'
      '    CTO,'
      '    CODEZAK,'
      '    ALGCLASS,'
      '    ALGMAKER'
      ')'
      'VALUES('
      '    :ID,'
      '    :PARENT,'
      '    :NAME,'
      '    :TTYPE,'
      '    :IMESSURE,'
      '    :INDS,'
      '    :MINREST,'
      '    :LASTPRICEIN,'
      '    :LASTPRICEOUT,'
      '    :LASTPOST,'
      '    :IACTIVE,'
      '    :TCARD,'
      '    :CATEGORY,'
      '    :SPISSTORE,'
      '    :BB,'
      '    :GG,'
      '    :U1,'
      '    :U2,'
      '    :EE,'
      '    :RCATEGORY,'
      '    :COMMENT,'
      '    :CTO,'
      '    :CODEZAK,'
      '    :ALGCLASS,'
      '    :ALGMAKER'
      ')')
    RefreshSQL.Strings = (
      'SELECT ca.ID,'
      '       ca.PARENT,'
      '       ca.NAME,'
      '       ca.TTYPE,'
      '       ca.IMESSURE,'
      '       ca.INDS,'
      '       ca.MINREST,'
      '       ca.LASTPRICEIN,'
      '       ca.LASTPRICEOUT,'
      '       ca.LASTPOST,'
      '       ca.IACTIVE,'
      '       ca.TCARD,'
      '       ca.CATEGORY,'
      '       ca.SPISSTORE,'
      '       ca.BB,'
      '       ca.GG,'
      '       ca.U1,'
      '       ca.U2,'
      '       ca.EE,'
      '       ca.RCATEGORY,'
      '       ca.COMMENT,'
      '       ca.CTO,'
      '       ca.CODEZAK,'
      '       ca.ALGCLASS,'
      '       ca.ALGMAKER,'
      '       cla.NAMECLA,'
      '       mk.NAMEM,'
      '       mk.INNM,'
      '       mk.KPPM'
      'FROM OF_CARDS ca'
      'left join of_classifalg cla on cla.ID=ca.ALGCLASS'
      'left join of_makers mk on mk.ID=ca.ALGMAKER'
      ''
      'where(  ca.ALGCLASS>0'
      'and ca.ALGMAKER>0'
      '     ) and (     CA.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT ca.ID,'
      '       ca.PARENT,'
      '       ca.NAME,'
      '       ca.TTYPE,'
      '       ca.IMESSURE,'
      '       ca.INDS,'
      '       ca.MINREST,'
      '       ca.LASTPRICEIN,'
      '       ca.LASTPRICEOUT,'
      '       ca.LASTPOST,'
      '       ca.IACTIVE,'
      '       ca.TCARD,'
      '       ca.CATEGORY,'
      '       ca.SPISSTORE,'
      '       ca.BB,'
      '       ca.GG,'
      '       ca.U1,'
      '       ca.U2,'
      '       ca.EE,'
      '       ca.RCATEGORY,'
      '       ca.COMMENT,'
      '       ca.CTO,'
      '       ca.CODEZAK,'
      '       ca.ALGCLASS,'
      '       ca.ALGMAKER,'
      '       ca.VOL,'
      '       cla.NAMECLA,'
      '       mk.NAMEM,'
      '       mk.INNM,'
      '       mk.KPPM'
      'FROM OF_CARDS ca'
      'left join of_classifalg cla on cla.ID=ca.ALGCLASS'
      'left join of_makers mk on mk.ID=ca.ALGMAKER'
      ''
      'where ca.ALGCLASS>0'
      'order by ca.ALGCLASS')
    Transaction = trSelRep
    Database = dmO.OfficeRnDb
    UpdateTransaction = trD
    AutoCommit = True
    Left = 256
    Top = 668
    poAskRecordCount = True
    object quCardsAlgID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quCardsAlgPARENT: TFIBIntegerField
      FieldName = 'PARENT'
    end
    object quCardsAlgNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 200
      EmptyStrToNull = True
    end
    object quCardsAlgTTYPE: TFIBIntegerField
      FieldName = 'TTYPE'
    end
    object quCardsAlgIMESSURE: TFIBIntegerField
      FieldName = 'IMESSURE'
    end
    object quCardsAlgINDS: TFIBIntegerField
      FieldName = 'INDS'
    end
    object quCardsAlgMINREST: TFIBFloatField
      FieldName = 'MINREST'
    end
    object quCardsAlgLASTPRICEIN: TFIBFloatField
      FieldName = 'LASTPRICEIN'
    end
    object quCardsAlgLASTPRICEOUT: TFIBFloatField
      FieldName = 'LASTPRICEOUT'
    end
    object quCardsAlgLASTPOST: TFIBIntegerField
      FieldName = 'LASTPOST'
    end
    object quCardsAlgIACTIVE: TFIBIntegerField
      FieldName = 'IACTIVE'
    end
    object quCardsAlgTCARD: TFIBIntegerField
      FieldName = 'TCARD'
    end
    object quCardsAlgCATEGORY: TFIBIntegerField
      FieldName = 'CATEGORY'
    end
    object quCardsAlgSPISSTORE: TFIBStringField
      FieldName = 'SPISSTORE'
      EmptyStrToNull = True
    end
    object quCardsAlgBB: TFIBFloatField
      FieldName = 'BB'
    end
    object quCardsAlgGG: TFIBFloatField
      FieldName = 'GG'
    end
    object quCardsAlgU1: TFIBFloatField
      FieldName = 'U1'
    end
    object quCardsAlgU2: TFIBFloatField
      FieldName = 'U2'
    end
    object quCardsAlgEE: TFIBFloatField
      FieldName = 'EE'
    end
    object quCardsAlgRCATEGORY: TFIBIntegerField
      FieldName = 'RCATEGORY'
    end
    object quCardsAlgCOMMENT: TFIBStringField
      FieldName = 'COMMENT'
      Size = 150
      EmptyStrToNull = True
    end
    object quCardsAlgCTO: TFIBIntegerField
      FieldName = 'CTO'
    end
    object quCardsAlgCODEZAK: TFIBIntegerField
      FieldName = 'CODEZAK'
    end
    object quCardsAlgALGCLASS: TFIBIntegerField
      FieldName = 'ALGCLASS'
    end
    object quCardsAlgALGMAKER: TFIBIntegerField
      FieldName = 'ALGMAKER'
    end
    object quCardsAlgNAMECLA: TFIBStringField
      FieldName = 'NAMECLA'
      Size = 200
      EmptyStrToNull = True
    end
    object quCardsAlgNAMEM: TFIBStringField
      FieldName = 'NAMEM'
      Size = 200
      EmptyStrToNull = True
    end
    object quCardsAlgINNM: TFIBStringField
      FieldName = 'INNM'
      Size = 15
      EmptyStrToNull = True
    end
    object quCardsAlgKPPM: TFIBStringField
      FieldName = 'KPPM'
      Size = 15
      EmptyStrToNull = True
    end
    object quCardsAlgVOL: TFIBFloatField
      FieldName = 'VOL'
    end
  end
  object quInLnAlg: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCSPECIN'
      'SET '
      '    NUM = :NUM,'
      '    IDCARD = :IDCARD,'
      '    QUANT = :QUANT,'
      '    PRICEIN = :PRICEIN,'
      '    SUMIN = :SUMIN,'
      '    PRICEUCH = :PRICEUCH,'
      '    SUMUCH = :SUMUCH,'
      '    IDNDS = :IDNDS,'
      '    SUMNDS = :SUMNDS,'
      '    IDM = :IDM,'
      '    KM = :KM,'
      '    PRICE0 = :PRICE0,'
      '    SUM0 = :SUM0,'
      '    NDSPROC = :NDSPROC,'
      '    GTD = :GTD'
      'WHERE'
      '    IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCSPECIN'
      'WHERE'
      '        IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCSPECIN('
      '    IDHEAD,'
      '    ID,'
      '    NUM,'
      '    IDCARD,'
      '    QUANT,'
      '    PRICEIN,'
      '    SUMIN,'
      '    PRICEUCH,'
      '    SUMUCH,'
      '    IDNDS,'
      '    SUMNDS,'
      '    IDM,'
      '    KM,'
      '    PRICE0,'
      '    SUM0,'
      '    NDSPROC,'
      '    GTD'
      ')'
      'VALUES('
      '    :IDHEAD,'
      '    :ID,'
      '    :NUM,'
      '    :IDCARD,'
      '    :QUANT,'
      '    :PRICEIN,'
      '    :SUMIN,'
      '    :PRICEUCH,'
      '    :SUMUCH,'
      '    :IDNDS,'
      '    :SUMNDS,'
      '    :IDM,'
      '    :KM,'
      '    :PRICE0,'
      '    :SUM0,'
      '    :NDSPROC,'
      '    :GTD'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT sp.IDHEAD,sp.ID,sp.NUM,sp.IDCARD,sp.QUANT,sp.PRICEIN,sp.S' +
        'UMIN,'
      '       sp.PRICEUCH,sp.SUMUCH,sp.IDNDS,sp.SUMNDS,sp.IDM,sp.KM,'
      '       sp.PRICE0,sp.SUM0,sp.NDSPROC,sp.GTD,'
      '       hd.DATEDOC,hd.NUMDOC,hd.IDCLI,hd.IDSKL,hd.IACTIVE,'
      '       cli.FULLNAMECL,cli.INN,cli.KPP'
      '       '
      'FROM OF_DOCSPECIN sp'
      'left join OF_DOCHEADIN hd on hd.ID=sp.IDHEAD'
      'left join OF_CLIENTS cli on cli.ID=hd.IDCLI'
      ''
      'where(  sp.IDCARD=:ICARD'
      'and hd.IDSKL=:ISKL'
      'and hd.IACTIVE=1'
      'and hd.DATEDOC>=:DATEB'
      'and hd.DATEDOC<=:DATEE'
      '     ) and (     SP.IDHEAD = :OLD_IDHEAD'
      '    and SP.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT sp.IDHEAD,sp.ID,sp.NUM,sp.IDCARD,sp.QUANT,sp.PRICEIN,sp.S' +
        'UMIN,'
      '       sp.PRICEUCH,sp.SUMUCH,sp.IDNDS,sp.SUMNDS,sp.IDM,sp.KM,'
      '       sp.PRICE0,sp.SUM0,sp.NDSPROC,sp.GTD,'
      '       hd.DATEDOC,hd.NUMDOC,hd.IDCLI,hd.IDSKL,hd.IACTIVE,'
      '       cli.FULLNAMECL,cli.INN,cli.KPP'
      '       '
      'FROM OF_DOCSPECIN sp'
      'left join OF_DOCHEADIN hd on hd.ID=sp.IDHEAD'
      'left join OF_CLIENTS cli on cli.ID=hd.IDCLI'
      ''
      'where sp.IDCARD=:ICARD'
      'and hd.IDSKL=:ISKL'
      'and hd.IACTIVE=1'
      'and hd.DATEDOC>=:DATEB'
      'and hd.DATEDOC<=:DATEE')
    Transaction = trSelRep
    Database = dmO.OfficeRnDb
    UpdateTransaction = trD
    AutoCommit = True
    Left = 256
    Top = 720
    poAskRecordCount = True
    object quInLnAlgIDHEAD: TFIBIntegerField
      FieldName = 'IDHEAD'
    end
    object quInLnAlgID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quInLnAlgNUM: TFIBIntegerField
      FieldName = 'NUM'
    end
    object quInLnAlgIDCARD: TFIBIntegerField
      FieldName = 'IDCARD'
    end
    object quInLnAlgQUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quInLnAlgPRICEIN: TFIBFloatField
      FieldName = 'PRICEIN'
    end
    object quInLnAlgSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quInLnAlgPRICEUCH: TFIBFloatField
      FieldName = 'PRICEUCH'
    end
    object quInLnAlgSUMUCH: TFIBFloatField
      FieldName = 'SUMUCH'
    end
    object quInLnAlgIDNDS: TFIBIntegerField
      FieldName = 'IDNDS'
    end
    object quInLnAlgSUMNDS: TFIBFloatField
      FieldName = 'SUMNDS'
    end
    object quInLnAlgIDM: TFIBIntegerField
      FieldName = 'IDM'
    end
    object quInLnAlgKM: TFIBFloatField
      FieldName = 'KM'
    end
    object quInLnAlgPRICE0: TFIBFloatField
      FieldName = 'PRICE0'
    end
    object quInLnAlgSUM0: TFIBFloatField
      FieldName = 'SUM0'
    end
    object quInLnAlgNDSPROC: TFIBFloatField
      FieldName = 'NDSPROC'
    end
    object quInLnAlgDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object quInLnAlgNUMDOC: TFIBStringField
      FieldName = 'NUMDOC'
      Size = 15
      EmptyStrToNull = True
    end
    object quInLnAlgIDCLI: TFIBIntegerField
      FieldName = 'IDCLI'
    end
    object quInLnAlgIDSKL: TFIBIntegerField
      FieldName = 'IDSKL'
    end
    object quInLnAlgIACTIVE: TFIBIntegerField
      FieldName = 'IACTIVE'
    end
    object quInLnAlgFULLNAMECL: TFIBStringField
      FieldName = 'FULLNAMECL'
      Size = 300
      EmptyStrToNull = True
    end
    object quInLnAlgINN: TFIBStringField
      FieldName = 'INN'
      Size = 15
      EmptyStrToNull = True
    end
    object quInLnAlgKPP: TFIBStringField
      FieldName = 'KPP'
      Size = 15
      EmptyStrToNull = True
    end
    object quInLnAlgGTD: TFIBStringField
      FieldName = 'GTD'
      Size = 50
      EmptyStrToNull = True
    end
  end
  object quCliLicMax: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_CLIENTSLIC'
      'SET '
      '    ICLI = :ICLI,'
      '    IDATEB = :IDATEB,'
      '    DDATEB = :DDATEB,'
      '    IDATEE = :IDATEE,'
      '    DDATEE = :DDATEE,'
      '    SER = :SER,'
      '    SNUM = :SNUM,'
      '    ORGAN = :ORGAN'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_CLIENTSLIC'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_CLIENTSLIC('
      '    ID,'
      '    ICLI,'
      '    IDATEB,'
      '    DDATEB,'
      '    IDATEE,'
      '    DDATEE,'
      '    SER,'
      '    SNUM,'
      '    ORGAN'
      ')'
      'VALUES('
      '    :ID,'
      '    :ICLI,'
      '    :IDATEB,'
      '    :DDATEB,'
      '    :IDATEE,'
      '    :DDATEE,'
      '    :SER,'
      '    :SNUM,'
      '    :ORGAN'
      ')')
    RefreshSQL.Strings = (
      'SELECT first 1'
      '    ID,'
      '    ICLI,'
      '    IDATEB,'
      '    DDATEB,'
      '    IDATEE,'
      '    DDATEE,'
      '    SER,'
      '    SNUM,'
      '    ORGAN'
      'FROM'
      '    OF_CLIENTSLIC '
      'where(  ICLI=:ICLI'
      '     ) and (     OF_CLIENTSLIC.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT first 1'
      '    ID,'
      '    ICLI,'
      '    IDATEB,'
      '    DDATEB,'
      '    IDATEE,'
      '    DDATEE,'
      '    SER,'
      '    SNUM,'
      '    ORGAN'
      'FROM'
      '    OF_CLIENTSLIC '
      'where ICLI=:ICLI'
      'order by IDATEB desc')
    Transaction = trSelRep
    Database = dmO.OfficeRnDb
    UpdateTransaction = trD
    AutoCommit = True
    Left = 256
    Top = 780
    poAskRecordCount = True
    object quCliLicMaxICLI: TFIBIntegerField
      FieldName = 'ICLI'
    end
    object quCliLicMaxIDATEB: TFIBIntegerField
      FieldName = 'IDATEB'
    end
    object quCliLicMaxDDATEB: TFIBDateField
      FieldName = 'DDATEB'
    end
    object quCliLicMaxIDATEE: TFIBIntegerField
      FieldName = 'IDATEE'
    end
    object quCliLicMaxDDATEE: TFIBDateField
      FieldName = 'DDATEE'
    end
    object quCliLicMaxSER: TFIBStringField
      FieldName = 'SER'
      Size = 50
      EmptyStrToNull = True
    end
    object quCliLicMaxSNUM: TFIBStringField
      FieldName = 'SNUM'
      Size = 50
      EmptyStrToNull = True
    end
    object quCliLicMaxORGAN: TFIBStringField
      FieldName = 'ORGAN'
      Size = 200
      EmptyStrToNull = True
    end
    object quCliLicMaxID: TFIBIntegerField
      FieldName = 'ID'
    end
  end
  object quDocsRCard1: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHEADOUTR'
      'SET '
      '    DATEDOC = :DATEDOC,'
      '    NUMDOC = :NUMDOC,'
      '    DATESF = :DATESF,'
      '    NUMSF = :NUMSF,'
      '    IDCLI = :IDCLI,'
      '    IDSKL = :IDSKL,'
      '    SUMIN = :SUMIN,'
      '    SUMUCH = :SUMUCH,'
      '    SUMTAR = :SUMTAR,'
      '    SUMNDS0 = :SUMNDS0,'
      '    SUMNDS1 = :SUMNDS1,'
      '    SUMNDS2 = :SUMNDS2,'
      '    PROCNAC = :PROCNAC,'
      '    IACTIVE = :IACTIVE,'
      '    IDFROM = :IDFROM'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHEADOUTR'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHEADOUTR('
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    DATESF,'
      '    NUMSF,'
      '    IDCLI,'
      '    IDSKL,'
      '    SUMIN,'
      '    SUMUCH,'
      '    SUMTAR,'
      '    SUMNDS0,'
      '    SUMNDS1,'
      '    SUMNDS2,'
      '    PROCNAC,'
      '    IACTIVE,'
      '    IDFROM'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATEDOC,'
      '    :NUMDOC,'
      '    :DATESF,'
      '    :NUMSF,'
      '    :IDCLI,'
      '    :IDSKL,'
      '    :SUMIN,'
      '    :SUMUCH,'
      '    :SUMTAR,'
      '    :SUMNDS0,'
      '    :SUMNDS1,'
      '    :SUMNDS2,'
      '    :PROCNAC,'
      '    :IACTIVE,'
      '    :IDFROM'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT dh.ID,dh.DATEDOC,dh.NUMDOC,dh.DATESF,dh.NUMSF,dh.IDCLI,dh' +
        '.IDSKL,dh.SUMIN,'
      
        '       dh.SUMUCH,dh.SUMTAR,dh.SUMNDS0,dh.SUMNDS1,dh.SUMNDS2,dh.P' +
        'ROCNAC,dh.IACTIVE,'
      '       cl.NAMECL,mh.NAMEMH,dh.IDFROM'
      'FROM OF_DOCHEADOUTR dh'
      'left join OF_CLIENTS cl on cl.ID=dh.IDCLI'
      'left join OF_MH mh on mh.ID=dh.IDSKL'
      ''
      'Where(  dh.DATEDOC>=:DATEB and dh.DATEDOC<:DATEE'
      '     ) and (     DH.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT sr.IDCARD,ca.NAME,cla.NAMECL as GRNAME, dh.IDCLI, dh.IDSK' +
        'L,mh1.NAMEMH as MHTO,cli.NAMECL'
      ',SUM(sr.QUANT) as QUANT,SUM(sr.BZQUANTF) as BZQUANTF'
      ''
      'FROM OF_DOCSPECOUTR sr'
      'left join of_docheadoutr dh on dh.ID=sr.IDHEAD'
      'left join of_cards ca on ca.ID=sr.IDCARD'
      'left join of_messur me on me.ID=sr.IDM'
      'left join of_mh mh1 on mh1.ID=dh.IDSKL'
      'left join of_clients cli on cli.ID=dh.IDCLI'
      'left join of_Classif cla on cla.ID=ca.PARENT'
      ''
      'where'
      'dh.DATEDOC>=:DATEB and dh.DATEDOC<:DATEE'
      'and dh.IDSKL=:IDSKL'
      'and ca.CATEGORY=1'
      '/* and dh.BZTYPE=1'
      ' and dh.BZSTATUS=1 */'
      'and sr.BZQUANTF>0'
      ''
      'group by'
      
        'sr.IDCARD,ca.NAME,cla.NAMECL,dh.IDCLI, dh.IDSKL,mh1.NAMEMH ,cli.' +
        'NAMECL'
      ''
      'order by cla.NAMECL,ca.NAME,cli.NAMECL')
    OnCalcFields = quDocsRCardCalcFields
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    Left = 736
    Top = 644
    poAskRecordCount = True
    object quDocsRCard1IDCARD: TFIBIntegerField
      FieldName = 'IDCARD'
    end
    object quDocsRCard1NAME: TFIBStringField
      FieldName = 'NAME'
      Size = 200
      EmptyStrToNull = True
    end
    object quDocsRCard1IDCLI: TFIBIntegerField
      FieldName = 'IDCLI'
    end
    object quDocsRCard1IDSKL: TFIBIntegerField
      FieldName = 'IDSKL'
    end
    object quDocsRCard1MHTO: TFIBStringField
      FieldName = 'MHTO'
      Size = 100
      EmptyStrToNull = True
    end
    object quDocsRCard1NAMECL: TFIBStringField
      FieldName = 'NAMECL'
      Size = 100
      EmptyStrToNull = True
    end
    object quDocsRCard1QUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quDocsRCard1BZQUANTF: TFIBFloatField
      FieldName = 'BZQUANTF'
    end
    object quDocsRCard1GRNAME: TFIBStringField
      FieldName = 'GRNAME'
      Size = 150
      EmptyStrToNull = True
    end
  end
  object quDocsRCard2: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHEADOUTR'
      'SET '
      '    DATEDOC = :DATEDOC,'
      '    NUMDOC = :NUMDOC,'
      '    DATESF = :DATESF,'
      '    NUMSF = :NUMSF,'
      '    IDCLI = :IDCLI,'
      '    IDSKL = :IDSKL,'
      '    SUMIN = :SUMIN,'
      '    SUMUCH = :SUMUCH,'
      '    SUMTAR = :SUMTAR,'
      '    SUMNDS0 = :SUMNDS0,'
      '    SUMNDS1 = :SUMNDS1,'
      '    SUMNDS2 = :SUMNDS2,'
      '    PROCNAC = :PROCNAC,'
      '    IACTIVE = :IACTIVE,'
      '    IDFROM = :IDFROM'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHEADOUTR'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHEADOUTR('
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    DATESF,'
      '    NUMSF,'
      '    IDCLI,'
      '    IDSKL,'
      '    SUMIN,'
      '    SUMUCH,'
      '    SUMTAR,'
      '    SUMNDS0,'
      '    SUMNDS1,'
      '    SUMNDS2,'
      '    PROCNAC,'
      '    IACTIVE,'
      '    IDFROM'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATEDOC,'
      '    :NUMDOC,'
      '    :DATESF,'
      '    :NUMSF,'
      '    :IDCLI,'
      '    :IDSKL,'
      '    :SUMIN,'
      '    :SUMUCH,'
      '    :SUMTAR,'
      '    :SUMNDS0,'
      '    :SUMNDS1,'
      '    :SUMNDS2,'
      '    :PROCNAC,'
      '    :IACTIVE,'
      '    :IDFROM'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT dh.ID,dh.DATEDOC,dh.NUMDOC,dh.DATESF,dh.NUMSF,dh.IDCLI,dh' +
        '.IDSKL,dh.SUMIN,'
      
        '       dh.SUMUCH,dh.SUMTAR,dh.SUMNDS0,dh.SUMNDS1,dh.SUMNDS2,dh.P' +
        'ROCNAC,dh.IACTIVE,'
      '       cl.NAMECL,mh.NAMEMH,dh.IDFROM'
      'FROM OF_DOCHEADOUTR dh'
      'left join OF_CLIENTS cl on cl.ID=dh.IDCLI'
      'left join OF_MH mh on mh.ID=dh.IDSKL'
      ''
      'Where(  dh.DATEDOC>=:DATEB and dh.DATEDOC<:DATEE'
      '     ) and (     DH.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT sr.IDCARD,ca.NAME,ca.PARENT,cla.NAMECL as GRNAME,dh.IDSKL' +
        ',mh1.NAMEMH as MHTO, me.NAMESHORT'
      ',SUM(sr.QUANT) as QUANT,SUM(sr.BZQUANTF) as BZQUANTF'
      ''
      'FROM OF_DOCSPECOUTR sr'
      'left join of_docheadoutr dh on dh.ID=sr.IDHEAD'
      'left join of_cards ca on ca.ID=sr.IDCARD'
      'left join of_messur me on me.ID=sr.IDM'
      'left join of_mh mh1 on mh1.ID=dh.IDSKL'
      'left join of_Classif cla on cla.ID=ca.PARENT'
      ''
      'where'
      'dh.DATEDOC>=:DATEB and dh.DATEDOC<:DATEE'
      'and dh.IDSKL=:IDSKL'
      'and ca.CATEGORY=1'
      '/* and dh.BZTYPE=1'
      ' and dh.BZSTATUS=1 */'
      'and sr.BZQUANTF>0'
      ''
      'group by'
      
        'sr.IDCARD,ca.NAME,ca.PARENT,cla.NAMECL,dh.IDSKL,mh1.NAMEMH,me.NA' +
        'MESHORT'
      ''
      'order by cla.NAMECL,ca.NAME')
    OnCalcFields = quDocsRCardCalcFields
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    Left = 820
    Top = 640
    poAskRecordCount = True
    object quDocsRCard2IDCARD: TFIBIntegerField
      FieldName = 'IDCARD'
    end
    object quDocsRCard2NAME: TFIBStringField
      FieldName = 'NAME'
      Size = 200
      EmptyStrToNull = True
    end
    object quDocsRCard2GRNAME: TFIBStringField
      FieldName = 'GRNAME'
      Size = 150
      EmptyStrToNull = True
    end
    object quDocsRCard2IDSKL: TFIBIntegerField
      FieldName = 'IDSKL'
    end
    object quDocsRCard2MHTO: TFIBStringField
      FieldName = 'MHTO'
      Size = 100
      EmptyStrToNull = True
    end
    object quDocsRCard2QUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quDocsRCard2BZQUANTF: TFIBFloatField
      FieldName = 'BZQUANTF'
    end
    object quDocsRCard2NAMESHORT: TFIBStringField
      FieldName = 'NAMESHORT'
      Size = 50
      EmptyStrToNull = True
    end
    object quDocsRCard2PARENT: TFIBIntegerField
      FieldName = 'PARENT'
    end
  end
  object quDocsRCli: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHEADOUTR'
      'SET '
      '    DATEDOC = :DATEDOC,'
      '    NUMDOC = :NUMDOC,'
      '    DATESF = :DATESF,'
      '    NUMSF = :NUMSF,'
      '    IDCLI = :IDCLI,'
      '    IDSKL = :IDSKL,'
      '    SUMIN = :SUMIN,'
      '    SUMUCH = :SUMUCH,'
      '    SUMTAR = :SUMTAR,'
      '    SUMNDS0 = :SUMNDS0,'
      '    SUMNDS1 = :SUMNDS1,'
      '    SUMNDS2 = :SUMNDS2,'
      '    PROCNAC = :PROCNAC,'
      '    IACTIVE = :IACTIVE,'
      '    IDFROM = :IDFROM'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHEADOUTR'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHEADOUTR('
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    DATESF,'
      '    NUMSF,'
      '    IDCLI,'
      '    IDSKL,'
      '    SUMIN,'
      '    SUMUCH,'
      '    SUMTAR,'
      '    SUMNDS0,'
      '    SUMNDS1,'
      '    SUMNDS2,'
      '    PROCNAC,'
      '    IACTIVE,'
      '    IDFROM'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATEDOC,'
      '    :NUMDOC,'
      '    :DATESF,'
      '    :NUMSF,'
      '    :IDCLI,'
      '    :IDSKL,'
      '    :SUMIN,'
      '    :SUMUCH,'
      '    :SUMTAR,'
      '    :SUMNDS0,'
      '    :SUMNDS1,'
      '    :SUMNDS2,'
      '    :PROCNAC,'
      '    :IACTIVE,'
      '    :IDFROM'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT dh.ID,dh.DATEDOC,dh.NUMDOC,dh.DATESF,dh.NUMSF,dh.IDCLI,dh' +
        '.IDSKL,dh.SUMIN,'
      
        '       dh.SUMUCH,dh.SUMTAR,dh.SUMNDS0,dh.SUMNDS1,dh.SUMNDS2,dh.P' +
        'ROCNAC,dh.IACTIVE,'
      '       cl.NAMECL,mh.NAMEMH,dh.IDFROM'
      'FROM OF_DOCHEADOUTR dh'
      'left join OF_CLIENTS cl on cl.ID=dh.IDCLI'
      'left join OF_MH mh on mh.ID=dh.IDSKL'
      ''
      'Where(  dh.DATEDOC>=:DATEB and dh.DATEDOC<:DATEE'
      '     ) and (     DH.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT dh.IDCLI,cli.NAMECL'
      ''
      'FROM of_docheadoutr dh'
      'left join of_clients cli on cli.ID=dh.IDCLI'
      ''
      'where'
      'dh.DATEDOC>=:DATEB and dh.DATEDOC<:DATEE'
      'and dh.IDSKL=:IDSKL'
      ''
      'group by'
      'dh.IDCLI,cli.NAMECL'
      ''
      'order by cli.NAMECL')
    OnCalcFields = quDocsRCardCalcFields
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    Left = 740
    Top = 692
    poAskRecordCount = True
    object quDocsRCliIDCLI: TFIBIntegerField
      FieldName = 'IDCLI'
    end
    object quDocsRCliNAMECL: TFIBStringField
      FieldName = 'NAMECL'
      Size = 100
      EmptyStrToNull = True
    end
  end
  object quDocsRCliGr: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHEADOUTR'
      'SET '
      '    DATEDOC = :DATEDOC,'
      '    NUMDOC = :NUMDOC,'
      '    DATESF = :DATESF,'
      '    NUMSF = :NUMSF,'
      '    IDCLI = :IDCLI,'
      '    IDSKL = :IDSKL,'
      '    SUMIN = :SUMIN,'
      '    SUMUCH = :SUMUCH,'
      '    SUMTAR = :SUMTAR,'
      '    SUMNDS0 = :SUMNDS0,'
      '    SUMNDS1 = :SUMNDS1,'
      '    SUMNDS2 = :SUMNDS2,'
      '    PROCNAC = :PROCNAC,'
      '    IACTIVE = :IACTIVE,'
      '    IDFROM = :IDFROM'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHEADOUTR'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHEADOUTR('
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    DATESF,'
      '    NUMSF,'
      '    IDCLI,'
      '    IDSKL,'
      '    SUMIN,'
      '    SUMUCH,'
      '    SUMTAR,'
      '    SUMNDS0,'
      '    SUMNDS1,'
      '    SUMNDS2,'
      '    PROCNAC,'
      '    IACTIVE,'
      '    IDFROM'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATEDOC,'
      '    :NUMDOC,'
      '    :DATESF,'
      '    :NUMSF,'
      '    :IDCLI,'
      '    :IDSKL,'
      '    :SUMIN,'
      '    :SUMUCH,'
      '    :SUMTAR,'
      '    :SUMNDS0,'
      '    :SUMNDS1,'
      '    :SUMNDS2,'
      '    :PROCNAC,'
      '    :IACTIVE,'
      '    :IDFROM'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT dh.ID,dh.DATEDOC,dh.NUMDOC,dh.DATESF,dh.NUMSF,dh.IDCLI,dh' +
        '.IDSKL,dh.SUMIN,'
      
        '       dh.SUMUCH,dh.SUMTAR,dh.SUMNDS0,dh.SUMNDS1,dh.SUMNDS2,dh.P' +
        'ROCNAC,dh.IACTIVE,'
      '       cl.NAMECL,mh.NAMEMH,dh.IDFROM'
      'FROM OF_DOCHEADOUTR dh'
      'left join OF_CLIENTS cl on cl.ID=dh.IDCLI'
      'left join OF_MH mh on mh.ID=dh.IDSKL'
      ''
      'Where(  dh.DATEDOC>=:DATEB and dh.DATEDOC<:DATEE'
      '     ) and (     DH.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT ca.PARENT, dh.IDCLI , SUM(sr.BZQUANTF) as BZQUANTF'
      'FROM OF_DOCSPECOUTR sr'
      'left join of_docheadoutr dh on dh.ID=sr.IDHEAD'
      'left join of_cards ca on ca.ID=sr.IDCARD'
      ''
      'where'
      'dh.DATEDOC>=:DATEB and dh.DATEDOC<:DATEE'
      'and dh.IDSKL=:IDSKL'
      'and ca.CATEGORY=1'
      '/* and dh.BZTYPE=1'
      ' and dh.BZSTATUS=1 */'
      'and sr.BZQUANTF>0'
      ''
      'group by ca.PARENT,dh.IDCLI'
      ''
      'order by ca.PARENT,dh.IDCLI')
    OnCalcFields = quDocsRCardCalcFields
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    Left = 828
    Top = 692
    poAskRecordCount = True
    object quDocsRCliGrPARENT: TFIBIntegerField
      FieldName = 'PARENT'
    end
    object quDocsRCliGrIDCLI: TFIBIntegerField
      FieldName = 'IDCLI'
    end
    object quDocsRCliGrBZQUANTF: TFIBFloatField
      FieldName = 'BZQUANTF'
    end
  end
  object quTara: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_CARDS'
      'SET '
      '    PARENT = :PARENT,'
      '    NAME = :NAME,'
      '    TTYPE = :TTYPE,'
      '    IMESSURE = :IMESSURE,'
      '    INDS = :INDS,'
      '    MINREST = :MINREST,'
      '    LASTPRICEIN = :LASTPRICEIN,'
      '    LASTPRICEOUT = :LASTPRICEOUT,'
      '    LASTPOST = :LASTPOST,'
      '    IACTIVE = :IACTIVE,'
      '    TCARD = :TCARD,'
      '    CATEGORY = :CATEGORY,'
      '    SPISSTORE = :SPISSTORE,'
      '    BB = :BB,'
      '    GG = :GG,'
      '    U1 = :U1,'
      '    U2 = :U2,'
      '    EE = :EE,'
      '    RCATEGORY = :RCATEGORY,'
      '    COMMENT = :COMMENT,'
      '    CTO = :CTO,'
      '    CODEZAK = :CODEZAK,'
      '    ALGCLASS = :ALGCLASS,'
      '    ALGMAKER = :ALGMAKER'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_CARDS'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_CARDS('
      '    ID,'
      '    PARENT,'
      '    NAME,'
      '    TTYPE,'
      '    IMESSURE,'
      '    INDS,'
      '    MINREST,'
      '    LASTPRICEIN,'
      '    LASTPRICEOUT,'
      '    LASTPOST,'
      '    IACTIVE,'
      '    TCARD,'
      '    CATEGORY,'
      '    SPISSTORE,'
      '    BB,'
      '    GG,'
      '    U1,'
      '    U2,'
      '    EE,'
      '    RCATEGORY,'
      '    COMMENT,'
      '    CTO,'
      '    CODEZAK,'
      '    ALGCLASS,'
      '    ALGMAKER'
      ')'
      'VALUES('
      '    :ID,'
      '    :PARENT,'
      '    :NAME,'
      '    :TTYPE,'
      '    :IMESSURE,'
      '    :INDS,'
      '    :MINREST,'
      '    :LASTPRICEIN,'
      '    :LASTPRICEOUT,'
      '    :LASTPOST,'
      '    :IACTIVE,'
      '    :TCARD,'
      '    :CATEGORY,'
      '    :SPISSTORE,'
      '    :BB,'
      '    :GG,'
      '    :U1,'
      '    :U2,'
      '    :EE,'
      '    :RCATEGORY,'
      '    :COMMENT,'
      '    :CTO,'
      '    :CODEZAK,'
      '    :ALGCLASS,'
      '    :ALGMAKER'
      ')')
    RefreshSQL.Strings = (
      'SELECT ca.ID,'
      '       ca.PARENT,'
      '       ca.NAME,'
      '       ca.TTYPE,'
      '       ca.IMESSURE,'
      '       ca.INDS,'
      '       ca.MINREST,'
      '       ca.LASTPRICEIN,'
      '       ca.LASTPRICEOUT,'
      '       ca.LASTPOST,'
      '       ca.IACTIVE,'
      '       ca.TCARD,'
      '       ca.CATEGORY,'
      '       ca.SPISSTORE,'
      '       ca.BB,'
      '       ca.GG,'
      '       ca.U1,'
      '       ca.U2,'
      '       ca.EE,'
      '       ca.RCATEGORY,'
      '       ca.COMMENT,'
      '       ca.CTO,'
      '       ca.CODEZAK,'
      '       ca.ALGCLASS,'
      '       ca.ALGMAKER,'
      '       cla.NAMECLA,'
      '       mk.NAMEM,'
      '       mk.INNM,'
      '       mk.KPPM'
      'FROM OF_CARDS ca'
      'left join of_classifalg cla on cla.ID=ca.ALGCLASS'
      'left join of_makers mk on mk.ID=ca.ALGMAKER'
      ''
      'where(  ca.ALGCLASS>0'
      'and ca.ALGMAKER>0'
      '     ) and (     CA.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    PARENT,'
      '    NAME,'
      '    TTYPE,'
      '    IMESSURE,'
      '    INDS,'
      '    MINREST,'
      '    LASTPRICEIN,'
      '    LASTPRICEOUT,'
      '    LASTPOST,'
      '    IACTIVE,'
      '    TCARD,'
      '    CATEGORY,'
      '    SPISSTORE,'
      '    BB,'
      '    GG,'
      '    U1,'
      '    U2,'
      '    EE,'
      '    RCATEGORY,'
      '    COMMENT,'
      '    CTO,'
      '    CODEZAK,'
      '    ALGCLASS,'
      '    ALGMAKER,'
      '    VOL'
      'FROM'
      '    OF_CARDS '
      'where CATEGORY=4'
      'order by NAME')
    Transaction = trSelTara
    Database = dmO.OfficeRnDb
    UpdateTransaction = trD
    AutoCommit = True
    Left = 936
    Top = 648
    poAskRecordCount = True
    object quTaraID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quTaraPARENT: TFIBIntegerField
      FieldName = 'PARENT'
    end
    object quTaraNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 200
      EmptyStrToNull = True
    end
    object quTaraTTYPE: TFIBIntegerField
      FieldName = 'TTYPE'
    end
    object quTaraIMESSURE: TFIBIntegerField
      FieldName = 'IMESSURE'
    end
    object quTaraINDS: TFIBIntegerField
      FieldName = 'INDS'
    end
    object quTaraMINREST: TFIBFloatField
      FieldName = 'MINREST'
    end
    object quTaraLASTPRICEIN: TFIBFloatField
      FieldName = 'LASTPRICEIN'
    end
    object quTaraLASTPRICEOUT: TFIBFloatField
      FieldName = 'LASTPRICEOUT'
    end
    object quTaraLASTPOST: TFIBIntegerField
      FieldName = 'LASTPOST'
    end
    object quTaraIACTIVE: TFIBIntegerField
      FieldName = 'IACTIVE'
    end
    object quTaraTCARD: TFIBIntegerField
      FieldName = 'TCARD'
    end
    object quTaraCATEGORY: TFIBIntegerField
      FieldName = 'CATEGORY'
    end
    object quTaraSPISSTORE: TFIBStringField
      FieldName = 'SPISSTORE'
      EmptyStrToNull = True
    end
    object quTaraBB: TFIBFloatField
      FieldName = 'BB'
    end
    object quTaraGG: TFIBFloatField
      FieldName = 'GG'
    end
    object quTaraU1: TFIBFloatField
      FieldName = 'U1'
    end
    object quTaraU2: TFIBFloatField
      FieldName = 'U2'
    end
    object quTaraEE: TFIBFloatField
      FieldName = 'EE'
    end
    object quTaraRCATEGORY: TFIBIntegerField
      FieldName = 'RCATEGORY'
    end
    object quTaraCOMMENT: TFIBStringField
      FieldName = 'COMMENT'
      Size = 150
      EmptyStrToNull = True
    end
    object quTaraCTO: TFIBIntegerField
      FieldName = 'CTO'
    end
    object quTaraCODEZAK: TFIBIntegerField
      FieldName = 'CODEZAK'
    end
    object quTaraALGCLASS: TFIBIntegerField
      FieldName = 'ALGCLASS'
    end
    object quTaraALGMAKER: TFIBIntegerField
      FieldName = 'ALGMAKER'
    end
    object quTaraVOL: TFIBFloatField
      FieldName = 'VOL'
    end
  end
  object dsquTara: TDataSource
    DataSet = quTara
    Left = 936
    Top = 696
  end
  object trSelTara: TpFIBTransaction
    DefaultDatabase = dmO.OfficeRnDb
    TimeoutAction = TARollback
    Left = 880
    Top = 608
  end
  object quCliLic: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_CLIENTSLIC'
      'SET '
      '    ICLI = :ICLI,'
      '    IDATEB = :IDATEB,'
      '    DDATEB = :DDATEB,'
      '    IDATEE = :IDATEE,'
      '    DDATEE = :DDATEE,'
      '    SER = :SER,'
      '    SNUM = :SNUM,'
      '    ORGAN = :ORGAN'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_CLIENTSLIC'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_CLIENTSLIC('
      '    ID,'
      '    ICLI,'
      '    IDATEB,'
      '    DDATEB,'
      '    IDATEE,'
      '    DDATEE,'
      '    SER,'
      '    SNUM,'
      '    ORGAN'
      ')'
      'VALUES('
      '    :ID,'
      '    :ICLI,'
      '    :IDATEB,'
      '    :DDATEB,'
      '    :IDATEE,'
      '    :DDATEE,'
      '    :SER,'
      '    :SNUM,'
      '    :ORGAN'
      ')')
    RefreshSQL.Strings = (
      'SELECT ID,'
      '       ICLI,'
      '       IDATEB,'
      '       DDATEB,'
      '       IDATEE,'
      '       DDATEE,'
      '       SER,'
      '       SNUM,'
      '       ORGAN'
      'FROM OF_CLIENTSLIC'
      'where(  ICLI=:ICLI'
      '     ) and (     OF_CLIENTSLIC.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT ID,'
      '       ICLI,'
      '       IDATEB,'
      '       DDATEB,'
      '       IDATEE,'
      '       DDATEE,'
      '       SER,'
      '       SNUM,'
      '       ORGAN'
      'FROM OF_CLIENTSLIC'
      'where ICLI=:ICLI'
      'order by IDATEB,IDATEE')
    Transaction = trSelRep
    Database = dmO.OfficeRnDb
    UpdateTransaction = trU
    AutoCommit = True
    Left = 28
    Top = 788
    object quCliLicID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quCliLicICLI: TFIBIntegerField
      FieldName = 'ICLI'
    end
    object quCliLicIDATEB: TFIBIntegerField
      FieldName = 'IDATEB'
    end
    object quCliLicDDATEB: TFIBDateField
      FieldName = 'DDATEB'
    end
    object quCliLicIDATEE: TFIBIntegerField
      FieldName = 'IDATEE'
    end
    object quCliLicDDATEE: TFIBDateField
      FieldName = 'DDATEE'
    end
    object quCliLicSER: TFIBStringField
      FieldName = 'SER'
      Size = 50
      EmptyStrToNull = True
    end
    object quCliLicSNUM: TFIBStringField
      FieldName = 'SNUM'
      Size = 50
      EmptyStrToNull = True
    end
    object quCliLicORGAN: TFIBStringField
      FieldName = 'ORGAN'
      Size = 200
      EmptyStrToNull = True
    end
  end
  object quAlcoDH: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_ALCG_DH'
      'SET '
      '    IDATEB = :IDATEB,'
      '    IDATEE = :IDATEE,'
      '    DDATEB = :DDATEB,'
      '    DDATEE = :DDATEE,'
      '    SPERS = :SPERS,'
      '    IACTIVE = :IACTIVE,'
      '    IT1 = :IT1,'
      '    IT2 = :IT2,'
      '    INUMCORR = :INUMCORR'
      'WHERE'
      '    IDORG = :OLD_IDORG'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_ALCG_DH'
      'WHERE'
      '        IDORG = :OLD_IDORG'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_ALCG_DH('
      '    IDORG,'
      '    ID,'
      '    IDATEB,'
      '    IDATEE,'
      '    DDATEB,'
      '    DDATEE,'
      '    SPERS,'
      '    IACTIVE,'
      '    IT1,'
      '    IT2,'
      '    INUMCORR'
      ')'
      'VALUES('
      '    :IDORG,'
      '    :ID,'
      '    :IDATEB,'
      '    :IDATEE,'
      '    :DDATEB,'
      '    :DDATEE,'
      '    :SPERS,'
      '    :IACTIVE,'
      '    :IT1,'
      '    :IT2,'
      '    :INUMCORR'
      ')')
    RefreshSQL.Strings = (
      'SELECT dh.IDORG,'
      '       dh.ID,'
      '       dh.IDATEB,'
      '       dh.IDATEE,'
      '       dh.DDATEB,'
      '       dh.DDATEE,'
      '       dh.SPERS,'
      '       dh.IACTIVE,'
      '       dh.IT1,'
      '       dh.IT2,'
      '       dh.INUMCORR,'
      '       mh.FULLNAME as Name,'
      '       mh.INN'
      'FROM OF_ALCG_DH dh'
      'left join of_mh mh  on mh.ID=dh.IDORG'
      'where( '
      ' dh.IDATEB>=:IDATEB'
      'and dh.IDATEE<=:IDATEE'
      '     ) and (     DH.IDORG = :OLD_IDORG'
      '    and DH.ID = :OLD_ID'
      '     )'
      '    '
      '')
    SelectSQL.Strings = (
      'SELECT dh.IDORG,'
      '       dh.ID,'
      '       dh.IDATEB,'
      '       dh.IDATEE,'
      '       dh.DDATEB,'
      '       dh.DDATEE,'
      '       dh.SPERS,'
      '       dh.IACTIVE,'
      '       dh.IT1,'
      '       dh.IT2,'
      '       dh.INUMCORR,'
      '       mh.FULLNAME as Name,'
      '       mh.INN'
      'FROM OF_ALCG_DH dh'
      'left join of_mh mh  on mh.ID=dh.IDORG'
      'where'
      ' dh.IDATEB>=:IDATEB'
      'and dh.IDATEE<=:IDATEE'
      '')
    Transaction = dmO.trSelect
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    AutoCommit = True
    Left = 340
    Top = 668
    poAskRecordCount = True
    object quAlcoDHIDORG: TFIBIntegerField
      FieldName = 'IDORG'
    end
    object quAlcoDHID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quAlcoDHIDATEB: TFIBIntegerField
      FieldName = 'IDATEB'
    end
    object quAlcoDHIDATEE: TFIBIntegerField
      FieldName = 'IDATEE'
    end
    object quAlcoDHDDATEB: TFIBDateField
      FieldName = 'DDATEB'
    end
    object quAlcoDHDDATEE: TFIBDateField
      FieldName = 'DDATEE'
    end
    object quAlcoDHSPERS: TFIBStringField
      FieldName = 'SPERS'
      Size = 200
      EmptyStrToNull = True
    end
    object quAlcoDHIACTIVE: TFIBSmallIntField
      FieldName = 'IACTIVE'
    end
    object quAlcoDHIT1: TFIBSmallIntField
      FieldName = 'IT1'
    end
    object quAlcoDHIT2: TFIBSmallIntField
      FieldName = 'IT2'
    end
    object quAlcoDHINUMCORR: TFIBSmallIntField
      FieldName = 'INUMCORR'
    end
    object quAlcoDHNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 200
      EmptyStrToNull = True
    end
    object quAlcoDHINN: TFIBStringField
      FieldName = 'INN'
      Size = 15
      EmptyStrToNull = True
    end
  end
  object quAlcoDS1: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_ALCG_DS1'
      'SET '
      '    IDEP = :IDEP,'
      '    INUM = :INUM,'
      '    IVID = :IVID,'
      '    IPROD = :IPROD,'
      '    RQB = :RQB,'
      '    RQIN1 = :RQIN1,'
      '    RQIN2 = :RQIN2,'
      '    RQIN3 = :RQIN3,'
      '    RQINIT1 = :RQINIT1,'
      '    RQIN4 = :RQIN4,'
      '    RQIN5 = :RQIN5,'
      '    RQIN6 = :RQIN6,'
      '    RQINIT = :RQINIT,'
      '    RQOUT1 = :RQOUT1,'
      '    RQOUT2 = :RQOUT2,'
      '    RQOUT3 = :RQOUT3,'
      '    RQOUT4 = :RQOUT4,'
      '    RQOUTIT = :RQOUTIT,'
      '    RQE = :RQE'
      'WHERE'
      '    IDH = :OLD_IDH'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_ALCG_DS1'
      'WHERE'
      '        IDH = :OLD_IDH'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_ALCG_DS1('
      '    IDH,'
      '    ID,'
      '    IDEP,'
      '    INUM,'
      '    IVID,'
      '    IPROD,'
      '    RQB,'
      '    RQIN1,'
      '    RQIN2,'
      '    RQIN3,'
      '    RQINIT1,'
      '    RQIN4,'
      '    RQIN5,'
      '    RQIN6,'
      '    RQINIT,'
      '    RQOUT1,'
      '    RQOUT2,'
      '    RQOUT3,'
      '    RQOUT4,'
      '    RQOUTIT,'
      '    RQE'
      ')'
      'VALUES('
      '    :IDH,'
      '    :ID,'
      '    :IDEP,'
      '    :INUM,'
      '    :IVID,'
      '    :IPROD,'
      '    :RQB,'
      '    :RQIN1,'
      '    :RQIN2,'
      '    :RQIN3,'
      '    :RQINIT1,'
      '    :RQIN4,'
      '    :RQIN5,'
      '    :RQIN6,'
      '    :RQINIT,'
      '    :RQOUT1,'
      '    :RQOUT2,'
      '    :RQOUT3,'
      '    :RQOUT4,'
      '    :RQOUTIT,'
      '    :RQE'
      ')')
    RefreshSQL.Strings = (
      'SELECT ds1.IDH,'
      '       ds1.ID,'
      '       ds1.IDEP,'
      '       ds1.INUM,'
      '       ds1.IVID,'
      '       ds1.IPROD,'
      '       ds1.RQB,'
      '       ds1.RQIN1,'
      '       ds1.RQIN2,'
      '       ds1.RQIN3,'
      '       ds1.RQINIT1,'
      '       ds1.RQIN4,'
      '       ds1.RQIN5,'
      '       ds1.RQIN6,'
      '       ds1.RQINIT,'
      '       ds1.RQOUT1,'
      '       ds1.RQOUT2,'
      '       ds1.RQOUT3,'
      '       ds1.RQOUT4,'
      '       ds1.RQOUTIT,'
      '       ds1.RQE,'
      '       mh.FULLNAME as NameShop,'
      '       vid.NAMECLA as NameVid,'
      '       mk.NAMEM as NameProducer,'
      '       mk.INNM as INN,'
      '       mk.KPPM as KPP'
      '       '
      'FROM OF_ALCG_DS1 ds1'
      ''
      'left join of_mh mh on mh.ID=ds1.IDEP'
      'left join of_classifalg vid on vid.ID=ds1.IVID'
      'left join of_makers mk on mk.ID=ds1.IPROD'
      ''
      'where(  ds1.IDH=:IDH'
      '     ) and (     DS1.IDH = :OLD_IDH'
      '    and DS1.ID = :OLD_ID'
      '     )'
      '    '
      '')
    SelectSQL.Strings = (
      'SELECT ds1.IDH,'
      '       ds1.ID,'
      '       ds1.IDEP,'
      '       ds1.INUM,'
      '       ds1.IVID,'
      '       ds1.IPROD,'
      '       ds1.RQB,'
      '       ds1.RQIN1,'
      '       ds1.RQIN2,'
      '       ds1.RQIN3,'
      '       ds1.RQINIT1,'
      '       ds1.RQIN4,'
      '       ds1.RQIN5,'
      '       ds1.RQIN6,'
      '       ds1.RQINIT,'
      '       ds1.RQOUT1,'
      '       ds1.RQOUT2,'
      '       ds1.RQOUT3,'
      '       ds1.RQOUT4,'
      '       ds1.RQOUTIT,'
      '       ds1.RQE,'
      '       mh.FULLNAME as NameShop,'
      '       vid.NAMECLA as NameVid,'
      '       mk.NAMEM as NameProducer,'
      '       mk.INNM as INN,'
      '       mk.KPPM as KPP'
      '       '
      'FROM OF_ALCG_DS1 ds1'
      ''
      'left join of_mh mh on mh.ID=ds1.IDEP'
      'left join of_classifalg vid on vid.ID=ds1.IVID'
      'left join of_makers mk on mk.ID=ds1.IPROD'
      ''
      'where ds1.IDH=:IDH'
      ''
      'order by ds1.IDEP,ds1.IVID,mk.NAMEM')
    Transaction = dmO.trSelect
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    AutoCommit = True
    Left = 416
    Top = 668
    poAskRecordCount = True
    object quAlcoDS1IDH: TFIBIntegerField
      FieldName = 'IDH'
    end
    object quAlcoDS1ID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quAlcoDS1IDEP: TFIBIntegerField
      FieldName = 'IDEP'
    end
    object quAlcoDS1INUM: TFIBIntegerField
      FieldName = 'INUM'
    end
    object quAlcoDS1IVID: TFIBIntegerField
      FieldName = 'IVID'
    end
    object quAlcoDS1IPROD: TFIBIntegerField
      FieldName = 'IPROD'
    end
    object quAlcoDS1RQB: TFIBFloatField
      FieldName = 'RQB'
    end
    object quAlcoDS1RQIN1: TFIBFloatField
      FieldName = 'RQIN1'
    end
    object quAlcoDS1RQIN2: TFIBFloatField
      FieldName = 'RQIN2'
    end
    object quAlcoDS1RQIN3: TFIBFloatField
      FieldName = 'RQIN3'
    end
    object quAlcoDS1RQINIT1: TFIBFloatField
      FieldName = 'RQINIT1'
    end
    object quAlcoDS1RQIN4: TFIBFloatField
      FieldName = 'RQIN4'
    end
    object quAlcoDS1RQIN5: TFIBFloatField
      FieldName = 'RQIN5'
    end
    object quAlcoDS1RQIN6: TFIBFloatField
      FieldName = 'RQIN6'
    end
    object quAlcoDS1RQINIT: TFIBFloatField
      FieldName = 'RQINIT'
    end
    object quAlcoDS1RQOUT1: TFIBFloatField
      FieldName = 'RQOUT1'
    end
    object quAlcoDS1RQOUT2: TFIBFloatField
      FieldName = 'RQOUT2'
    end
    object quAlcoDS1RQOUT3: TFIBFloatField
      FieldName = 'RQOUT3'
    end
    object quAlcoDS1RQOUT4: TFIBFloatField
      FieldName = 'RQOUT4'
    end
    object quAlcoDS1RQOUTIT: TFIBFloatField
      FieldName = 'RQOUTIT'
    end
    object quAlcoDS1RQE: TFIBFloatField
      FieldName = 'RQE'
    end
    object quAlcoDS1NAMESHOP: TFIBStringField
      FieldName = 'NAMESHOP'
      Size = 200
      EmptyStrToNull = True
    end
    object quAlcoDS1NAMEVID: TFIBStringField
      FieldName = 'NAMEVID'
      Size = 200
      EmptyStrToNull = True
    end
    object quAlcoDS1NAMEPRODUCER: TFIBStringField
      FieldName = 'NAMEPRODUCER'
      Size = 200
      EmptyStrToNull = True
    end
    object quAlcoDS1INN: TFIBStringField
      FieldName = 'INN'
      Size = 15
      EmptyStrToNull = True
    end
    object quAlcoDS1KPP: TFIBStringField
      FieldName = 'KPP'
      Size = 15
      EmptyStrToNull = True
    end
  end
  object quAlcoDS2: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_ALCG_DS2'
      'SET '
      '    IDEP = :IDEP,'
      '    IVID = :IVID,'
      '    IPROD = :IPROD,'
      '    IPOST = :IPOST,'
      '    ILIC = :ILIC,'
      '    DOCDATE = :DOCDATE,'
      '    DOCNUM = :DOCNUM,'
      '    GTD = :GTD,'
      '    RQIN = :RQIN,'
      '    IDDH = :IDDH'
      'WHERE'
      '    IDH = :OLD_IDH'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_ALCG_DS2'
      'WHERE'
      '        IDH = :OLD_IDH'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_ALCG_DS2('
      '    IDH,'
      '    ID,'
      '    IDEP,'
      '    IVID,'
      '    IPROD,'
      '    IPOST,'
      '    ILIC,'
      '    DOCDATE,'
      '    DOCNUM,'
      '    GTD,'
      '    RQIN,'
      '    IDDH'
      ')'
      'VALUES('
      '    :IDH,'
      '    :ID,'
      '    :IDEP,'
      '    :IVID,'
      '    :IPROD,'
      '    :IPOST,'
      '    :ILIC,'
      '    :DOCDATE,'
      '    :DOCNUM,'
      '    :GTD,'
      '    :RQIN,'
      '    :IDDH'
      ')')
    RefreshSQL.Strings = (
      'SELECT ds2.IDH,'
      '       ds2.ID,'
      '       ds2.IDEP,'
      '       ds2.IVID,'
      '       ds2.IPROD,'
      '       ds2.IPOST,'
      '       ds2.ILIC,'
      '       ds2.DOCDATE,'
      '       ds2.DOCNUM,'
      '       ds2.GTD,'
      '       ds2.RQIN,'
      '       ds2.IDDH,'
      '       mh.FULLNAME as NameShop,'
      '       vid.NAMECLA as NameVid,'
      '       mk.NAMEM as NameProducer,'
      '       mk.INNM as INN,'
      '       mk.KPPM as KPP,'
      '       cli.FULLNAMECL as NameCli,'
      '       cli.INN as InnCli,'
      '       cli.KPP as KppCli,'
      '       lic.SER,'
      '       lic.SNUM,'
      '       lic.DDATEB,'
      '       lic.DDATEE,'
      '       lic.ORGAN'
      '       '
      'FROM OF_ALCG_DS2 ds2'
      'left join of_mh mh on mh.ID=ds2.IDEP'
      'left join of_classifalg vid on vid.ID=ds2.IVID'
      'left join of_makers mk on mk.ID=ds2.IPROD'
      'left join of_clients cli on cli.ID=ds2.IPOST'
      'left join of_clientslic lic on lic.ID=ds2.ILIC'
      ''
      'where(  ds2.IDH=:IDH'
      '     ) and (     DS2.IDH = :OLD_IDH'
      '    and DS2.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT ds2.IDH,'
      '       ds2.ID,'
      '       ds2.IDEP,'
      '       ds2.IVID,'
      '       ds2.IPROD,'
      '       ds2.IPOST,'
      '       ds2.ILIC,'
      '       ds2.DOCDATE,'
      '       ds2.DOCNUM,'
      '       ds2.GTD,'
      '       ds2.RQIN,'
      '       ds2.IDDH,'
      '       mh.FULLNAME as NameShop,'
      '       vid.NAMECLA as NameVid,'
      '       mk.NAMEM as NameProducer,'
      '       mk.INNM as INN,'
      '       mk.KPPM as KPP,'
      '       cli.FULLNAMECL as NameCli,'
      '       cli.INN as InnCli,'
      '       cli.KPP as KppCli,'
      '       lic.SER,'
      '       lic.SNUM,'
      '       lic.DDATEB,'
      '       lic.DDATEE,'
      '       lic.ORGAN'
      '       '
      'FROM OF_ALCG_DS2 ds2'
      'left join of_mh mh on mh.ID=ds2.IDEP'
      'left join of_classifalg vid on vid.ID=ds2.IVID'
      'left join of_makers mk on mk.ID=ds2.IPROD'
      'left join of_clients cli on cli.ID=ds2.IPOST'
      'left join of_clientslic lic on lic.ID=ds2.ILIC'
      ''
      'where ds2.IDH=:IDH'
      
        'order by ds2.IDEP, ds2.IVID, mk.NAMEM, cli.FULLNAMECL, ds2.DOCDA' +
        'TE')
    Transaction = dmO.trSelect
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    AutoCommit = True
    Left = 492
    Top = 668
    poAskRecordCount = True
    object quAlcoDS2IDH: TFIBIntegerField
      FieldName = 'IDH'
    end
    object quAlcoDS2ID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quAlcoDS2IDEP: TFIBIntegerField
      FieldName = 'IDEP'
    end
    object quAlcoDS2IVID: TFIBIntegerField
      FieldName = 'IVID'
    end
    object quAlcoDS2IPROD: TFIBIntegerField
      FieldName = 'IPROD'
    end
    object quAlcoDS2IPOST: TFIBIntegerField
      FieldName = 'IPOST'
    end
    object quAlcoDS2ILIC: TFIBIntegerField
      FieldName = 'ILIC'
    end
    object quAlcoDS2DOCDATE: TFIBDateField
      FieldName = 'DOCDATE'
    end
    object quAlcoDS2DOCNUM: TFIBStringField
      FieldName = 'DOCNUM'
      EmptyStrToNull = True
    end
    object quAlcoDS2GTD: TFIBStringField
      FieldName = 'GTD'
      Size = 50
      EmptyStrToNull = True
    end
    object quAlcoDS2RQIN: TFIBFloatField
      FieldName = 'RQIN'
    end
    object quAlcoDS2NAMESHOP: TFIBStringField
      FieldName = 'NAMESHOP'
      Size = 200
      EmptyStrToNull = True
    end
    object quAlcoDS2NAMEVID: TFIBStringField
      FieldName = 'NAMEVID'
      Size = 200
      EmptyStrToNull = True
    end
    object quAlcoDS2NAMEPRODUCER: TFIBStringField
      FieldName = 'NAMEPRODUCER'
      Size = 200
      EmptyStrToNull = True
    end
    object quAlcoDS2INN: TFIBStringField
      FieldName = 'INN'
      Size = 15
      EmptyStrToNull = True
    end
    object quAlcoDS2KPP: TFIBStringField
      FieldName = 'KPP'
      Size = 15
      EmptyStrToNull = True
    end
    object quAlcoDS2NAMECLI: TFIBStringField
      FieldName = 'NAMECLI'
      Size = 300
      EmptyStrToNull = True
    end
    object quAlcoDS2INNCLI: TFIBStringField
      FieldName = 'INNCLI'
      Size = 15
      EmptyStrToNull = True
    end
    object quAlcoDS2KPPCLI: TFIBStringField
      FieldName = 'KPPCLI'
      Size = 15
      EmptyStrToNull = True
    end
    object quAlcoDS2SER: TFIBStringField
      FieldName = 'SER'
      Size = 50
      EmptyStrToNull = True
    end
    object quAlcoDS2SNUM: TFIBStringField
      FieldName = 'SNUM'
      Size = 50
      EmptyStrToNull = True
    end
    object quAlcoDS2DDATEB: TFIBDateField
      FieldName = 'DDATEB'
    end
    object quAlcoDS2DDATEE: TFIBDateField
      FieldName = 'DDATEE'
    end
    object quAlcoDS2ORGAN: TFIBStringField
      FieldName = 'ORGAN'
      Size = 200
      EmptyStrToNull = True
    end
    object quAlcoDS2IDDH: TFIBIntegerField
      FieldName = 'IDDH'
    end
  end
  object dsquAlcoDH: TDataSource
    DataSet = quAlcoDH
    Left = 340
    Top = 720
  end
  object dsquAlcoDS1: TDataSource
    DataSet = quAlcoDS1
    Left = 416
    Top = 720
  end
  object dsquAlcoDS2: TDataSource
    DataSet = quAlcoDS2
    Left = 492
    Top = 720
  end
  object quDelDS1: TpFIBQuery
    Transaction = trD
    Database = dmO.OfficeRnDb
    SQL.Strings = (
      'delete FROM OF_ALCG_DS1 where IDH=:IDH')
    Left = 348
    Top = 780
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object quDelDS2: TpFIBQuery
    Transaction = trD
    Database = dmO.OfficeRnDb
    SQL.Strings = (
      'delete FROM OF_ALCG_DS2 where IDH=:IDH')
    Left = 412
    Top = 780
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object quFindDH: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_ALCG_DH'
      'SET '
      '    IDATEB = :IDATEB,'
      '    IDATEE = :IDATEE,'
      '    DDATEB = :DDATEB,'
      '    DDATEE = :DDATEE,'
      '    SPERS = :SPERS,'
      '    IACTIVE = :IACTIVE,'
      '    IT1 = :IT1,'
      '    IT2 = :IT2,'
      '    INUMCORR = :INUMCORR'
      'WHERE'
      '    IDORG = :OLD_IDORG'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_ALCG_DH'
      'WHERE'
      '        IDORG = :OLD_IDORG'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_ALCG_DH('
      '    IDORG,'
      '    ID,'
      '    IDATEB,'
      '    IDATEE,'
      '    DDATEB,'
      '    DDATEE,'
      '    SPERS,'
      '    IACTIVE,'
      '    IT1,'
      '    IT2,'
      '    INUMCORR'
      ')'
      'VALUES('
      '    :IDORG,'
      '    :ID,'
      '    :IDATEB,'
      '    :IDATEE,'
      '    :DDATEB,'
      '    :DDATEE,'
      '    :SPERS,'
      '    :IACTIVE,'
      '    :IT1,'
      '    :IT2,'
      '    :INUMCORR'
      ')')
    RefreshSQL.Strings = (
      'SELECT dh.IDORG,'
      '       dh.ID,'
      '       dh.IDATEB,'
      '       dh.IDATEE,'
      '       dh.DDATEB,'
      '       dh.DDATEE,'
      '       dh.SPERS,'
      '       dh.IACTIVE,'
      '       dh.IT1,'
      '       dh.IT2,'
      '       dh.INUMCORR,'
      '       mh.FULLNAME as Name,'
      '       mh.INN'
      'FROM OF_ALCG_DH dh'
      'left join of_mh mh  on mh.ID=dh.IDORG'
      'where( '
      ' dh.IDATEB>=:IDATEB'
      'and dh.IDATEE<=:IDATEE'
      '     ) and (     DH.IDORG = :OLD_IDORG'
      '    and DH.ID = :OLD_ID'
      '     )'
      '    '
      '')
    SelectSQL.Strings = (
      'SELECT dh.IDORG,'
      '       dh.ID,'
      '       dh.IDATEB,'
      '       dh.IDATEE,'
      '       dh.DDATEB,'
      '       dh.DDATEE,'
      '       dh.SPERS,'
      '       dh.IACTIVE,'
      '       dh.IT1,'
      '       dh.IT2,'
      '       dh.INUMCORR'
      'FROM OF_ALCG_DH dh'
      'where'
      'dh.IDORG=:IORG'
      'and dh.IT1=:ITYPE'
      'and dh.IDATEE=:IDATEE'
      '')
    Transaction = dmO.trSelect
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    AutoCommit = True
    Left = 496
    Top = 780
    poAskRecordCount = True
    object quFindDHIDORG: TFIBIntegerField
      FieldName = 'IDORG'
    end
    object quFindDHID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quFindDHIDATEB: TFIBIntegerField
      FieldName = 'IDATEB'
    end
    object quFindDHIDATEE: TFIBIntegerField
      FieldName = 'IDATEE'
    end
    object quFindDHDDATEB: TFIBDateField
      FieldName = 'DDATEB'
    end
    object quFindDHDDATEE: TFIBDateField
      FieldName = 'DDATEE'
    end
    object quFindDHSPERS: TFIBStringField
      FieldName = 'SPERS'
      Size = 200
      EmptyStrToNull = True
    end
    object quFindDHIACTIVE: TFIBSmallIntField
      FieldName = 'IACTIVE'
    end
    object quFindDHIT1: TFIBSmallIntField
      FieldName = 'IT1'
    end
    object quFindDHIT2: TFIBSmallIntField
      FieldName = 'IT2'
    end
    object quFindDHINUMCORR: TFIBSmallIntField
      FieldName = 'INUMCORR'
    end
  end
  object quDetPosD: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCSPECINV'
      'SET '
      '    NUM = :NUM,'
      '    IDCARD = :IDCARD,'
      '    QUANT = :QUANT,'
      '    SUMIN = :SUMIN,'
      '    SUMUCH = :SUMUCH,'
      '    IDMESSURE = :IDMESSURE,'
      '    QUANTFACT = :QUANTFACT,'
      '    SUMINFACT = :SUMINFACT,'
      '    SUMUCHFACT = :SUMUCHFACT,'
      '    KM = :KM,'
      '    TCARD = :TCARD,'
      '    IDGROUP = :IDGROUP,'
      '    NOCALC = :NOCALC,'
      '    SUMINR = :SUMINR,'
      '    PRICER = :PRICER,'
      '    SUMIN0 = :SUMIN0,'
      '    SUMINFACT0 = :SUMINFACT0,'
      '    NDSPROC = :NDSPROC,'
      '    NDSSUM = :NDSSUM'
      'WHERE'
      '    IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCSPECINV'
      'WHERE'
      '        IDHEAD = :OLD_IDHEAD'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCSPECINV('
      '    IDHEAD,'
      '    ID,'
      '    NUM,'
      '    IDCARD,'
      '    QUANT,'
      '    SUMIN,'
      '    SUMUCH,'
      '    IDMESSURE,'
      '    QUANTFACT,'
      '    SUMINFACT,'
      '    SUMUCHFACT,'
      '    KM,'
      '    TCARD,'
      '    IDGROUP,'
      '    NOCALC,'
      '    SUMINR,'
      '    PRICER,'
      '    SUMIN0,'
      '    SUMINFACT0,'
      '    NDSPROC,'
      '    NDSSUM'
      ')'
      'VALUES('
      '    :IDHEAD,'
      '    :ID,'
      '    :NUM,'
      '    :IDCARD,'
      '    :QUANT,'
      '    :SUMIN,'
      '    :SUMUCH,'
      '    :IDMESSURE,'
      '    :QUANTFACT,'
      '    :SUMINFACT,'
      '    :SUMUCHFACT,'
      '    :KM,'
      '    :TCARD,'
      '    :IDGROUP,'
      '    :NOCALC,'
      '    :SUMINR,'
      '    :PRICER,'
      '    :SUMIN0,'
      '    :SUMINFACT0,'
      '    :NDSPROC,'
      '    :NDSSUM'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT s.IDHEAD,s.ID,s.NUM,s.IDCARD,s.QUANT,s.SUMIN,s.SUMUCH,s.I' +
        'DMESSURE,'
      
        's.QUANTFACT,s.SUMINFACT,s.SUMUCHFACT,s.KM,s.TCARD,s.IDGROUP,s.NO' +
        'CALC,s.SUMINR,s.PRICER,'
      's.SUMIN0,s.SUMINFACT0,s.NDSPROC,s.NDSSUM,'
      'me.NAMESHORT,cl.NAMECL,ca.NAME, ca.CATEGORY'
      'FROM OF_DOCSPECINV s'
      'left join OF_MESSUR me on me.ID=s.IDMESSURE'
      'left join OF_CLASSIF cl on cl.ID=s.IDGROUP'
      'left join of_cards ca on ca.ID=s.IDCARD'
      'where(  IDHEAD=:IDH'
      '     ) and (     S.IDHEAD = :OLD_IDHEAD'
      '    and S.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT s.IDHEAD,s.ID,s.NUM,s.IDCARD,s.QUANT,s.SUMIN,s.SUMUCH,s.I' +
        'DMESSURE,'
      
        's.QUANTFACT,s.SUMINFACT,s.SUMUCHFACT,s.KM,s.TCARD,s.NOCALC,s.SUM' +
        'INR,s.PRICER,'
      's.SUMIN0,s.SUMINFACT0,s.NDSPROC,s.NDSSUM,'
      
        'me.NAMESHORT,cl.NAMECL,ca.NAME, ca.CATEGORY, ca.PARENT as IDGROU' +
        'P '
      'FROM OF_DOCSPECINV s'
      'left join OF_MESSUR me on me.ID=s.IDMESSURE'
      'left join OF_CLASSIF cl on cl.ID=s.IDGROUP'
      'left join of_cards ca on ca.ID=s.IDCARD'
      'where IDHEAD=:IDH')
    Transaction = trSelRep
    Database = dmO.OfficeRnDb
    AutoCommit = True
    Left = 612
    Top = 780
    poAskRecordCount = True
  end
  object quLog: TpFIBQuery
    Transaction = trLog
    Database = dmO.OfficeRnDb
    Left = 812
    Top = 16
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object trLog: TpFIBTransaction
    DefaultDatabase = dmO.OfficeRnDb
    TimeoutAction = TARollback
    Left = 812
    Top = 68
  end
  object quDocComplAuto: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHEADCOMPL'
      'SET '
      '    DATEDOC = :DATEDOC,'
      '    NUMDOC = :NUMDOC,'
      '    IDSKL = :IDSKL,'
      '    SUMIN = :SUMIN,'
      '    SUMUCH = :SUMUCH,'
      '    SUMTAR = :SUMTAR,'
      '    PROCNAC = :PROCNAC,'
      '    IACTIVE = :IACTIVE,'
      '    OPER = :OPER,'
      '    IDSKLTO = :IDSKLTO,'
      '    IDFROM = :IDFROM,'
      '    TOREAL = :TOREAL'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHEADCOMPL'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHEADCOMPL('
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    IDSKL,'
      '    SUMIN,'
      '    SUMUCH,'
      '    SUMTAR,'
      '    PROCNAC,'
      '    IACTIVE,'
      '    OPER,'
      '    IDSKLTO,'
      '    IDFROM,'
      '    TOREAL'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATEDOC,'
      '    :NUMDOC,'
      '    :IDSKL,'
      '    :SUMIN,'
      '    :SUMUCH,'
      '    :SUMTAR,'
      '    :PROCNAC,'
      '    :IACTIVE,'
      '    :OPER,'
      '    :IDSKLTO,'
      '    :IDFROM,'
      '    :TOREAL'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT dh.ID,dh.DATEDOC,dh.NUMDOC,dh.IDSKL,dh.SUMIN,dh.SUMUCH,dh' +
        '.SUMTAR,'
      'dh.PROCNAC,dh.IACTIVE,dh.OPER,dh.IDSKLTO,dh.IDFROM,dh.TOREAL,'
      'mh.NAMEMH as NAMEMH,'
      'mh1.NAMEMH as NAMEMH'
      'FROM OF_DOCHEADCOMPL dh'
      'left join OF_MH mh on mh.ID=dh.IDSKL'
      'left join OF_MH mh1 on mh1.ID=dh.IDSKLTO'
      'Where(  dh.DATEDOC>=:DATEB and dh.DATEDOC<:DATEE'
      '     ) and (     DH.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT dh.ID,dh.DATEDOC,dh.NUMDOC,dh.IDSKL,dh.SUMIN,dh.SUMUCH,dh' +
        '.SUMTAR,'
      'dh.PROCNAC,dh.IACTIVE,dh.OPER,dh.IDSKLTO,dh.IDFROM,dh.TOREAL,'
      'mh.NAMEMH as NAMEMH'
      'FROM OF_DOCHEADCOMPL dh'
      'left join OF_MH mh on mh.ID=dh.IDSKL'
      'Where dh.DATEDOC>=:DATEB and dh.DATEDOC<=:DATEE'
      'and dh.IDSKL = :ISKL'
      'and dh.TOREAL=1'
      'and dh.IACTIVE=1')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 640
    Top = 256
    poAskRecordCount = True
    object quDocComplAutoID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quDocComplAutoDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object quDocComplAutoNUMDOC: TFIBStringField
      FieldName = 'NUMDOC'
      Size = 15
      EmptyStrToNull = True
    end
    object quDocComplAutoIDSKL: TFIBIntegerField
      FieldName = 'IDSKL'
    end
    object quDocComplAutoSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quDocComplAutoSUMUCH: TFIBFloatField
      FieldName = 'SUMUCH'
    end
    object quDocComplAutoSUMTAR: TFIBFloatField
      FieldName = 'SUMTAR'
    end
    object quDocComplAutoPROCNAC: TFIBFloatField
      FieldName = 'PROCNAC'
    end
    object quDocComplAutoIACTIVE: TFIBIntegerField
      FieldName = 'IACTIVE'
    end
    object quDocComplAutoOPER: TFIBStringField
      FieldName = 'OPER'
      EmptyStrToNull = True
    end
    object quDocComplAutoIDSKLTO: TFIBIntegerField
      FieldName = 'IDSKLTO'
    end
    object quDocComplAutoIDFROM: TFIBIntegerField
      FieldName = 'IDFROM'
    end
    object quDocComplAutoTOREAL: TFIBSmallIntField
      FieldName = 'TOREAL'
    end
    object quDocComplAutoNAMEMH: TFIBStringField
      FieldName = 'NAMEMH'
      Size = 100
      EmptyStrToNull = True
    end
  end
  object quUPL: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_UPLICA'
      'SET '
      '    IMH = :IMH,'
      '    NAME = :NAME,'
      '    IACTIVE = :IACTIVE'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_UPLICA'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_UPLICA('
      '    ID,'
      '    IMH,'
      '    NAME,'
      '    IACTIVE'
      ')'
      'VALUES('
      '    :ID,'
      '    :IMH,'
      '    :NAME,'
      '    :IACTIVE'
      ')')
    RefreshSQL.Strings = (
      'SELECT li.ID,li.IMH,li.NAME,li.IACTIVE,MH.NAMEMH'
      'FROM OF_UPLICA li'
      'left JOIN OF_MH MH ON MH.ID=li.IMH'
      'WHERE(  MH.ITYPE=1'
      
        'and MH.ID in (SELECT IDMH FROM RPERSONALMH where IDPERSON=:IDPER' +
        'SON)'
      '     ) and (     LI.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT li.ID,li.IMH,li.NAME,li.IACTIVE,MH.NAMEMH'
      'FROM OF_UPLICA li'
      'left JOIN OF_MH MH ON MH.ID=li.IMH'
      'WHERE MH.ITYPE=1'
      
        'and MH.ID in (SELECT IDMH FROM RPERSONALMH where IDPERSON=:IDPER' +
        'SON)'
      'Order by MH.PRIOR')
    Transaction = dmO.trSelect
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    AutoCommit = True
    Left = 736
    Top = 752
    poAskRecordCount = True
    object quUPLID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quUPLIMH: TFIBIntegerField
      FieldName = 'IMH'
    end
    object quUPLNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 200
      EmptyStrToNull = True
    end
    object quUPLIACTIVE: TFIBSmallIntField
      FieldName = 'IACTIVE'
    end
    object quUPLNAMEMH: TFIBStringField
      FieldName = 'NAMEMH'
      Size = 100
      EmptyStrToNull = True
    end
  end
  object dsquUPL: TDataSource
    DataSet = quUPL
    Left = 736
    Top = 804
  end
  object quUPLMH: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_UPLICA'
      'SET '
      '    IMH = :IMH,'
      '    NAME = :NAME,'
      '    IACTIVE = :IACTIVE'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_UPLICA'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_UPLICA('
      '    ID,'
      '    IMH,'
      '    NAME,'
      '    IACTIVE'
      ')'
      'VALUES('
      '    :ID,'
      '    :IMH,'
      '    :NAME,'
      '    :IACTIVE'
      ')')
    RefreshSQL.Strings = (
      'SELECT li.ID,li.IMH,li.NAME,li.IACTIVE,MH.NAMEMH'
      'FROM OF_UPLICA li'
      'left JOIN OF_MH MH ON MH.ID=li.IMH'
      'WHERE(  MH.ITYPE=1'
      
        'and MH.ID in (SELECT IDMH FROM RPERSONALMH where IDPERSON=:IDPER' +
        'SON)'
      '     ) and (     LI.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT li.ID,li.IMH,li.NAME,li.IACTIVE'
      'FROM OF_UPLICA li'
      'WHERE li.IMH=:IMH'
      'and li.IACTIVE=1'
      '')
    Transaction = dmO.trSelect
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    AutoCommit = True
    Left = 816
    Top = 752
    poAskRecordCount = True
    object quUPLMHID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quUPLMHIMH: TFIBIntegerField
      FieldName = 'IMH'
    end
    object quUPLMHNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 200
      EmptyStrToNull = True
    end
    object quUPLMHIACTIVE: TFIBSmallIntField
      FieldName = 'IACTIVE'
    end
  end
  object dsquUPLMH: TDataSource
    DataSet = quUPLMH
    Left = 816
    Top = 804
  end
  object quUPLRec: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_UPLICA'
      'SET '
      '    IMH = :IMH,'
      '    NAME = :NAME,'
      '    IACTIVE = :IACTIVE'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_UPLICA'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_UPLICA('
      '    ID,'
      '    IMH,'
      '    NAME,'
      '    IACTIVE'
      ')'
      'VALUES('
      '    :ID,'
      '    :IMH,'
      '    :NAME,'
      '    :IACTIVE'
      ')')
    RefreshSQL.Strings = (
      'SELECT li.ID,li.IMH,li.NAME,li.IACTIVE,MH.NAMEMH'
      'FROM OF_UPLICA li'
      'left JOIN OF_MH MH ON MH.ID=li.IMH'
      'WHERE(  MH.ITYPE=1'
      
        'and MH.ID in (SELECT IDMH FROM RPERSONALMH where IDPERSON=:IDPER' +
        'SON)'
      '     ) and (     LI.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT li.ID,li.IMH,li.NAME,li.IACTIVE'
      'FROM OF_UPLICA li'
      'WHERE li.ID=:IUPL'
      ''
      '')
    Transaction = dmO.trSelect
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    AutoCommit = True
    Left = 880
    Top = 752
    poAskRecordCount = True
    object quUPLRecID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quUPLRecIMH: TFIBIntegerField
      FieldName = 'IMH'
    end
    object quUPLRecNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 200
      EmptyStrToNull = True
    end
    object quUPLRecIACTIVE: TFIBSmallIntField
      FieldName = 'IACTIVE'
    end
  end
  object quDocsPrice: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHPR'
      'SET '
      '    IDATE = :IDATE,'
      '    DDATE = :DDATE,'
      '    COMMENT = :COMMENT'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHPR'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHPR('
      '    ID,'
      '    IDATE,'
      '    DDATE,'
      '    COMMENT'
      ')'
      'VALUES('
      '    :ID,'
      '    :IDATE,'
      '    :DDATE,'
      '    :COMMENT'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    IDATE,'
      '    DDATE,'
      '    COMMENT'
      'FROM'
      '    OF_DOCHPR '
      'where(  IDATE>=:DATEB'
      'and IDATE<=:DATEE'
      '     ) and (     OF_DOCHPR.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    IDATE,'
      '    DDATE,'
      '    COMMENT'
      'FROM'
      '    OF_DOCHPR '
      'where IDATE>=:DATEB'
      'and IDATE<=:DATEE')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 1020
    Top = 632
    poAskRecordCount = True
    object quDocsPriceID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quDocsPriceIDATE: TFIBIntegerField
      FieldName = 'IDATE'
    end
    object quDocsPriceDDATE: TFIBDateField
      FieldName = 'DDATE'
    end
    object quDocsPriceCOMMENT: TFIBStringField
      FieldName = 'COMMENT'
      Size = 200
      EmptyStrToNull = True
    end
  end
  object dsquDocsPrice: TDataSource
    DataSet = quDocsPrice
    Left = 1020
    Top = 688
  end
  object quDocPrId: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHPR'
      'SET '
      '    IDATE = :IDATE,'
      '    DDATE = :DDATE,'
      '    COMMENT = :COMMENT'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHPR'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHPR('
      '    ID,'
      '    IDATE,'
      '    DDATE,'
      '    COMMENT'
      ')'
      'VALUES('
      '    :ID,'
      '    :IDATE,'
      '    :DDATE,'
      '    :COMMENT'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    IDATE,'
      '    DDATE,'
      '    COMMENT'
      'FROM'
      '    OF_DOCHPR '
      'where(  ID=:IDH'
      '     ) and (     OF_DOCHPR.ID = :OLD_ID'
      '     )'
      '    '
      '')
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    IDATE,'
      '    DDATE,'
      '    COMMENT'
      'FROM'
      '    OF_DOCHPR '
      'where ID=:IDH'
      '')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 1024
    Top = 756
    poAskRecordCount = True
    object quDocPrIdID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quDocPrIdIDATE: TFIBIntegerField
      FieldName = 'IDATE'
    end
    object quDocPrIdDDATE: TFIBDateField
      FieldName = 'DDATE'
    end
    object quDocPrIdCOMMENT: TFIBStringField
      FieldName = 'COMMENT'
      Size = 200
      EmptyStrToNull = True
    end
  end
  object quSpecPrSel: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCSPR'
      'SET '
      '    ICODE = :ICODE,'
      '    OLDPRICE = :OLDPRICE,'
      '    NEWPRICE = :NEWPRICE,'
      '    IM = :IM,'
      '    KM = :KM'
      'WHERE'
      '    IDH = :OLD_IDH'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCSPR'
      'WHERE'
      '        IDH = :OLD_IDH'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCSPR('
      '    IDH,'
      '    ID,'
      '    ICODE,'
      '    OLDPRICE,'
      '    NEWPRICE,'
      '    IM,'
      '    KM'
      ')'
      'VALUES('
      '    :IDH,'
      '    :ID,'
      '    :ICODE,'
      '    :OLDPRICE,'
      '    :NEWPRICE,'
      '    :IM,'
      '    :KM'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT sp.IDH,sp.ID,sp.ICODE,sp.OLDPRICE,sp.NEWPRICE,sp.IM,sp.KM' +
        ',ca.NAME,me.NAMESHORT'
      'FROM OF_DOCSPR sp'
      'left join OF_MESSUR me on me.ID=sp.IM'
      'left join of_cards ca on ca.ID=sp.ICODE'
      'where(  sp.IDH=:IDH'
      '     ) and (     SP.IDH = :OLD_IDH'
      '    and SP.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT sp.IDH,sp.ID,sp.ICODE,sp.OLDPRICE,sp.NEWPRICE,sp.IM,sp.KM' +
        ',ca.NAME,me.NAMESHORT'
      ',ca.TCARD,ca.INDS,ca.CATEGORY,n.NAMENDS,n.PROC'
      'FROM OF_DOCSPR sp'
      'left join OF_MESSUR me on me.ID=sp.IM'
      'left join of_cards ca on ca.ID=sp.ICODE'
      'left join OF_NDS n on n.ID=ca.INDS'
      'where sp.IDH=:IDH')
    Transaction = dmO.trDocsSel
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trDocsUpd
    AutoCommit = True
    Left = 1028
    Top = 808
    poAskRecordCount = True
    object quSpecPrSelIDH: TFIBIntegerField
      FieldName = 'IDH'
    end
    object quSpecPrSelID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quSpecPrSelICODE: TFIBIntegerField
      FieldName = 'ICODE'
    end
    object quSpecPrSelOLDPRICE: TFIBFloatField
      FieldName = 'OLDPRICE'
    end
    object quSpecPrSelNEWPRICE: TFIBFloatField
      FieldName = 'NEWPRICE'
    end
    object quSpecPrSelIM: TFIBIntegerField
      FieldName = 'IM'
    end
    object quSpecPrSelKM: TFIBFloatField
      FieldName = 'KM'
    end
    object quSpecPrSelNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 200
      EmptyStrToNull = True
    end
    object quSpecPrSelNAMESHORT: TFIBStringField
      FieldName = 'NAMESHORT'
      Size = 50
      EmptyStrToNull = True
    end
    object quSpecPrSelTCARD: TFIBIntegerField
      FieldName = 'TCARD'
    end
    object quSpecPrSelINDS: TFIBIntegerField
      FieldName = 'INDS'
    end
    object quSpecPrSelCATEGORY: TFIBIntegerField
      FieldName = 'CATEGORY'
    end
    object quSpecPrSelNAMENDS: TFIBStringField
      FieldName = 'NAMENDS'
      Size = 30
      EmptyStrToNull = True
    end
    object quSpecPrSelPROC: TFIBFloatField
      FieldName = 'PROC'
    end
  end
  object quDelSpecPrice: TpFIBQuery
    Transaction = trD
    Database = dmO.OfficeRnDb
    SQL.Strings = (
      'delete FROM OF_DOCSPR where IDH=:IDH    ')
    Left = 1044
    Top = 576
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object quFOldPrice: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHPR'
      'SET '
      '    IDATE = :IDATE,'
      '    DDATE = :DDATE,'
      '    COMMENT = :COMMENT'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHPR'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHPR('
      '    ID,'
      '    IDATE,'
      '    DDATE,'
      '    COMMENT'
      ')'
      'VALUES('
      '    :ID,'
      '    :IDATE,'
      '    :DDATE,'
      '    :COMMENT'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    IDATE,'
      '    DDATE,'
      '    COMMENT'
      'FROM'
      '    OF_DOCHPR '
      'where(  ID=:IDH'
      '     ) and (     OF_DOCHPR.ID = :OLD_ID'
      '     )'
      '    '
      '')
    SelectSQL.Strings = (
      'Select first 1 sp.NEWPRICE from of_docspr sp'
      'left join OF_DOCHPR dh on dh.ID=sp.IDH'
      'where sp.ICODE=:ICODE'
      'and dh.IDATE<:IDATE'
      'order by dh.IDATE desc')
    Transaction = dmO.trSel
    Database = dmO.OfficeRnDb
    AutoCommit = True
    Left = 1100
    Top = 628
    poAskRecordCount = True
    object quFOldPriceNEWPRICE: TFIBFloatField
      FieldName = 'NEWPRICE'
    end
  end
  object quFPricePP: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHPR'
      'SET '
      '    IDATE = :IDATE,'
      '    DDATE = :DDATE,'
      '    COMMENT = :COMMENT'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHPR'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHPR('
      '    ID,'
      '    IDATE,'
      '    DDATE,'
      '    COMMENT'
      ')'
      'VALUES('
      '    :ID,'
      '    :IDATE,'
      '    :DDATE,'
      '    :COMMENT'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    IDATE,'
      '    DDATE,'
      '    COMMENT'
      'FROM'
      '    OF_DOCHPR '
      'where(  ID=:IDH'
      '     ) and (     OF_DOCHPR.ID = :OLD_ID'
      '     )'
      '    '
      '')
    SelectSQL.Strings = (
      'SELECT first 1'
      '    ID,'
      '    IDSTORE,'
      '    IDDOC,'
      '    ARTICUL,'
      '    IDCLI,'
      '    DTYPE,'
      '    QPART,'
      '    QREMN,'
      '    PRICEIN,'
      '    PRICEOUT,'
      '    IDATE,'
      '    PRICEIN0'
      'FROM'
      '    OF_PARTIN '
      'where ARTICUL=:ICODE'
      'order by IDATE desc, ID desc')
    Transaction = dmO.trSel
    Database = dmO.OfficeRnDb
    AutoCommit = True
    Left = 1104
    Top = 680
    poAskRecordCount = True
    object quFPricePPID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quFPricePPIDSTORE: TFIBIntegerField
      FieldName = 'IDSTORE'
    end
    object quFPricePPIDDOC: TFIBIntegerField
      FieldName = 'IDDOC'
    end
    object quFPricePPARTICUL: TFIBIntegerField
      FieldName = 'ARTICUL'
    end
    object quFPricePPIDCLI: TFIBIntegerField
      FieldName = 'IDCLI'
    end
    object quFPricePPDTYPE: TFIBIntegerField
      FieldName = 'DTYPE'
    end
    object quFPricePPQPART: TFIBFloatField
      FieldName = 'QPART'
    end
    object quFPricePPQREMN: TFIBFloatField
      FieldName = 'QREMN'
    end
    object quFPricePPPRICEIN: TFIBFloatField
      FieldName = 'PRICEIN'
    end
    object quFPricePPPRICEOUT: TFIBFloatField
      FieldName = 'PRICEOUT'
    end
    object quFPricePPIDATE: TFIBIntegerField
      FieldName = 'IDATE'
    end
    object quFPricePPPRICEIN0: TFIBFloatField
      FieldName = 'PRICEIN0'
    end
  end
  object PRNORMPARTIN: TpFIBStoredProc
    Transaction = trU
    Database = dmO.OfficeRnDb
    SQL.Strings = (
      
        'EXECUTE PROCEDURE PR_NORMPARTIN (?IDSKL, ?IDATE, ?IDCARD, ?QUANT' +
        ')')
    StoredProcName = 'PR_NORMPARTIN'
    Left = 200
    Top = 584
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object quRepAkciz: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_CARDS'
      'SET '
      '    PARENT = :PARENT,'
      '    NAME = :NAME,'
      '    TTYPE = :TTYPE,'
      '    IMESSURE = :IMESSURE,'
      '    INDS = :INDS,'
      '    MINREST = :MINREST,'
      '    LASTPRICEIN = :LASTPRICEIN,'
      '    LASTPRICEOUT = :LASTPRICEOUT,'
      '    LASTPOST = :LASTPOST,'
      '    IACTIVE = :IACTIVE,'
      '    TCARD = :TCARD,'
      '    CATEGORY = :CATEGORY,'
      '    SPISSTORE = :SPISSTORE,'
      '    BB = :BB,'
      '    GG = :GG,'
      '    U1 = :U1,'
      '    U2 = :U2,'
      '    EE = :EE,'
      '    RCATEGORY = :RCATEGORY,'
      '    COMMENT = :COMMENT,'
      '    CTO = :CTO,'
      '    CODEZAK = :CODEZAK,'
      '    ALGCLASS = :ALGCLASS,'
      '    ALGMAKER = :ALGMAKER'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_CARDS'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_CARDS('
      '    ID,'
      '    PARENT,'
      '    NAME,'
      '    TTYPE,'
      '    IMESSURE,'
      '    INDS,'
      '    MINREST,'
      '    LASTPRICEIN,'
      '    LASTPRICEOUT,'
      '    LASTPOST,'
      '    IACTIVE,'
      '    TCARD,'
      '    CATEGORY,'
      '    SPISSTORE,'
      '    BB,'
      '    GG,'
      '    U1,'
      '    U2,'
      '    EE,'
      '    RCATEGORY,'
      '    COMMENT,'
      '    CTO,'
      '    CODEZAK,'
      '    ALGCLASS,'
      '    ALGMAKER'
      ')'
      'VALUES('
      '    :ID,'
      '    :PARENT,'
      '    :NAME,'
      '    :TTYPE,'
      '    :IMESSURE,'
      '    :INDS,'
      '    :MINREST,'
      '    :LASTPRICEIN,'
      '    :LASTPRICEOUT,'
      '    :LASTPOST,'
      '    :IACTIVE,'
      '    :TCARD,'
      '    :CATEGORY,'
      '    :SPISSTORE,'
      '    :BB,'
      '    :GG,'
      '    :U1,'
      '    :U2,'
      '    :EE,'
      '    :RCATEGORY,'
      '    :COMMENT,'
      '    :CTO,'
      '    :CODEZAK,'
      '    :ALGCLASS,'
      '    :ALGMAKER'
      ')')
    RefreshSQL.Strings = (
      'SELECT ca.ID,'
      '       ca.PARENT,'
      '       ca.NAME,'
      '       ca.TTYPE,'
      '       ca.IMESSURE,'
      '       ca.INDS,'
      '       ca.MINREST,'
      '       ca.LASTPRICEIN,'
      '       ca.LASTPRICEOUT,'
      '       ca.LASTPOST,'
      '       ca.IACTIVE,'
      '       ca.TCARD,'
      '       ca.CATEGORY,'
      '       ca.SPISSTORE,'
      '       ca.BB,'
      '       ca.GG,'
      '       ca.U1,'
      '       ca.U2,'
      '       ca.EE,'
      '       ca.RCATEGORY,'
      '       ca.COMMENT,'
      '       ca.CTO,'
      '       ca.CODEZAK,'
      '       ca.ALGCLASS,'
      '       ca.ALGMAKER,'
      '       cla.NAMECLA,'
      '       mk.NAMEM,'
      '       mk.INNM,'
      '       mk.KPPM'
      'FROM OF_CARDS ca'
      'left join of_classifalg cla on cla.ID=ca.ALGCLASS'
      'left join of_makers mk on mk.ID=ca.ALGMAKER'
      ''
      'where(  ca.ALGCLASS>0'
      'and ca.ALGMAKER>0'
      '     ) and (     CA.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT sp.IDCARD,'
      '       sp.IDM,'
      '       sp.KM,'
      '       SUM(sp.QUANT) as Quant,'
      '       SUM(sp.SUMIN) as SUMIN,'
      '       SUM(sp.SUMR) as SUMR,'
      '       SUM(sp.SUMNDS) as SUMNDS,'
      '       SUM(sp.SUMIN0) as SUMIN0,'
      '       SUM(sp.SUMOUT0) as SUMOUT0,'
      '       SUM(sp.SUMNDSOUT) as SUMNDSOUT,'
      '       SUM(sp.QUANTDOC) as QUANTDOC'
      'FROM OF_DOCSPECOUTR sp'
      'inner join of_cards_alco_buh spa on spa.IDCARD=sp.IDCARD'
      'left join of_docheadoutr hd on hd.ID=sp.IDHEAD'
      'where hd.IDSKL=39'
      'and hd.DATEDOC>=:DATEB'
      'and hd.DATEDOC<=:DATEE'
      'group by sp.IDCARD,'
      '       sp.IDM,'
      '       sp.KM'
      ' '
      'order by sp.IDCARD'
      '       '
      ''
      '')
    Transaction = trSelTara
    Database = dmO.OfficeRnDb
    UpdateTransaction = trD
    AutoCommit = True
    Left = 956
    Top = 20
    poAskRecordCount = True
    object quRepAkcizIDCARD: TFIBIntegerField
      FieldName = 'IDCARD'
    end
    object quRepAkcizIDM: TFIBIntegerField
      FieldName = 'IDM'
    end
    object quRepAkcizKM: TFIBFloatField
      FieldName = 'KM'
    end
    object quRepAkcizQUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quRepAkcizSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quRepAkcizSUMR: TFIBFloatField
      FieldName = 'SUMR'
    end
    object quRepAkcizSUMNDS: TFIBFloatField
      FieldName = 'SUMNDS'
    end
    object quRepAkcizSUMIN0: TFIBFloatField
      FieldName = 'SUMIN0'
    end
    object quRepAkcizSUMOUT0: TFIBFloatField
      FieldName = 'SUMOUT0'
    end
    object quRepAkcizSUMNDSOUT: TFIBFloatField
      FieldName = 'SUMNDSOUT'
    end
    object quRepAkcizQUANTDOC: TFIBFloatField
      FieldName = 'QUANTDOC'
    end
  end
  object teRepAkciz: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 956
    Top = 76
    object teRepAkciziCode: TIntegerField
      FieldName = 'iCode'
    end
    object teRepAkcizNameC: TStringField
      FieldName = 'NameC'
      Size = 100
    end
    object teRepAkcizSM: TStringField
      FieldName = 'SM'
      Size = 10
    end
    object teRepAkcizQuantDoc: TFloatField
      FieldName = 'QuantDoc'
    end
    object teRepAkcizrSumIn: TFloatField
      FieldName = 'rSumIn'
    end
    object teRepAkcizrSumIn0: TFloatField
      FieldName = 'rSumIn0'
    end
    object teRepAkcizrSumOut: TFloatField
      FieldName = 'rSumOut'
    end
    object teRepAkcizrSumOut0: TFloatField
      FieldName = 'rSumOut0'
    end
    object teRepAkcizrSumNDSOut: TFloatField
      FieldName = 'rSumNDSOut'
    end
    object teRepAkciziCodeDoc: TIntegerField
      FieldName = 'iCodeDoc'
    end
    object teRepAkcizQuant: TFloatField
      FieldName = 'Quant'
    end
  end
  object quQD: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHEADOUTB'
      'SET '
      '    DATEDOC = :DATEDOC,'
      '    NUMDOC = :NUMDOC,'
      '    DATESF = :DATESF,'
      '    NUMSF = :NUMSF,'
      '    IDCLI = :IDCLI,'
      '    IDSKL = :IDSKL,'
      '    SUMIN = :SUMIN,'
      '    SUMUCH = :SUMUCH,'
      '    SUMTAR = :SUMTAR,'
      '    SUMNDS0 = :SUMNDS0,'
      '    SUMNDS1 = :SUMNDS1,'
      '    SUMNDS2 = :SUMNDS2,'
      '    PROCNAC = :PROCNAC,'
      '    IACTIVE = :IACTIVE,'
      '    OPER = :OPER,'
      '    COMMENT = :COMMENT'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHEADOUTB'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHEADOUTB('
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    DATESF,'
      '    NUMSF,'
      '    IDCLI,'
      '    IDSKL,'
      '    SUMIN,'
      '    SUMUCH,'
      '    SUMTAR,'
      '    SUMNDS0,'
      '    SUMNDS1,'
      '    SUMNDS2,'
      '    PROCNAC,'
      '    IACTIVE,'
      '    OPER,'
      '    COMMENT'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATEDOC,'
      '    :NUMDOC,'
      '    :DATESF,'
      '    :NUMSF,'
      '    :IDCLI,'
      '    :IDSKL,'
      '    :SUMIN,'
      '    :SUMUCH,'
      '    :SUMTAR,'
      '    :SUMNDS0,'
      '    :SUMNDS1,'
      '    :SUMNDS2,'
      '    :PROCNAC,'
      '    :IACTIVE,'
      '    :OPER,'
      '    :COMMENT'
      ')')
    RefreshSQL.Strings = (
      'SELECT ID,DATEDOC,NUMDOC,DATESF,NUMSF,IDCLI,IDSKL,SUMIN,'
      
        '       SUMUCH,SUMTAR,SUMNDS0,SUMNDS1,SUMNDS2,PROCNAC,IACTIVE,OPE' +
        'R,COMMENT'
      'FROM OF_DOCHEADOUTB dh'
      'Where(  ID=:IDH'
      '     ) and (     DH.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT sp.IDCARD,'
      '       sp.SETIDCARDS,'
      '       sp.MAXQUANT,'
      '       Ca.NAME as NAMEC,'
      '       Ca.IMESSURE,'
      '       Me.NAMESHORT as SM'
      'FROM OF_CARDS_ALCO_BUH sp'
      'left join OF_CARDS Ca on Ca.ID=SP.IDCARD'
      'left join OF_MESSUR Me on Me.ID=Ca.IMESSURE'
      '')
    Transaction = dmO.trSelect
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    AutoCommit = True
    Left = 956
    Top = 132
    poAskRecordCount = True
    object quQDIDCARD: TFIBIntegerField
      FieldName = 'IDCARD'
    end
    object quQDSETIDCARDS: TFIBIntegerField
      FieldName = 'SETIDCARDS'
    end
    object quQDMAXQUANT: TFIBFloatField
      FieldName = 'MAXQUANT'
    end
    object quQDNAMEC: TFIBStringField
      FieldName = 'NAMEC'
      Size = 200
      EmptyStrToNull = True
    end
    object quQDIMESSURE: TFIBIntegerField
      FieldName = 'IMESSURE'
    end
    object quQDSM: TFIBStringField
      FieldName = 'SM'
      Size = 50
      EmptyStrToNull = True
    end
  end
  object frteRepAkciz: TfrDBDataSet
    DataSet = teRepAkciz
    Left = 960
    Top = 196
  end
  object quDrv: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DRIVERS'
      'SET '
      '    NAME1 = :NAME1,'
      '    NAME2 = :NAME2,'
      '    NAME3 = :NAME3,'
      '    SDOC = :SDOC,'
      '    CARNAME = :CARNAME,'
      '    CARNUM = :CARNUM'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DRIVERS'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DRIVERS('
      '    ID,'
      '    NAME1,'
      '    NAME2,'
      '    NAME3,'
      '    SDOC,'
      '    CARNAME,'
      '    CARNUM'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAME1,'
      '    :NAME2,'
      '    :NAME3,'
      '    :SDOC,'
      '    :CARNAME,'
      '    :CARNUM'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    NAME1,'
      '    NAME2,'
      '    NAME3,'
      '    SDOC,'
      '    CARNAME,'
      '    CARNUM'
      'FROM'
      '    OF_DRIVERS '
      ''
      ' WHERE '
      '        OF_DRIVERS.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    NAME1,'
      '    NAME2,'
      '    NAME3,'
      '    SDOC,'
      '    CARNAME,'
      '    CARNUM'
      'FROM'
      '    OF_DRIVERS ')
    OnCalcFields = quDrvCalcFields
    Transaction = trSelTara
    Database = dmO.OfficeRnDb
    UpdateTransaction = trD
    AutoCommit = True
    Left = 960
    Top = 268
    poAskRecordCount = True
    object quDrvID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quDrvNAME1: TFIBStringField
      FieldName = 'NAME1'
      Size = 50
      EmptyStrToNull = True
    end
    object quDrvNAME2: TFIBStringField
      FieldName = 'NAME2'
      Size = 50
      EmptyStrToNull = True
    end
    object quDrvNAME3: TFIBStringField
      FieldName = 'NAME3'
      Size = 50
      EmptyStrToNull = True
    end
    object quDrvSDOC: TFIBStringField
      FieldName = 'SDOC'
      EmptyStrToNull = True
    end
    object quDrvCARNAME: TFIBStringField
      FieldName = 'CARNAME'
      Size = 30
      EmptyStrToNull = True
    end
    object quDrvCARNUM: TFIBStringField
      FieldName = 'CARNUM'
      EmptyStrToNull = True
    end
    object quDrvFIO: TStringField
      FieldKind = fkCalculated
      FieldName = 'FIO'
      Size = 50
      Calculated = True
    end
    object quDrvFIO_CAR: TStringField
      FieldKind = fkCalculated
      FieldName = 'FIO_CAR'
      Size = 70
      Calculated = True
    end
  end
  object dsquDrv: TDataSource
    DataSet = quDrv
    Left = 960
    Top = 316
  end
  object dsquDrv1: TDataSource
    DataSet = quDrv
    Left = 960
    Top = 364
  end
  object quAcloOrg: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_ALCG_DS2'
      'SET '
      '    IDEP = :IDEP,'
      '    IVID = :IVID,'
      '    IPROD = :IPROD,'
      '    IPOST = :IPOST,'
      '    ILIC = :ILIC,'
      '    DOCDATE = :DOCDATE,'
      '    DOCNUM = :DOCNUM,'
      '    GTD = :GTD,'
      '    RQIN = :RQIN,'
      '    IDDH = :IDDH'
      'WHERE'
      '    IDH = :OLD_IDH'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_ALCG_DS2'
      'WHERE'
      '        IDH = :OLD_IDH'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_ALCG_DS2('
      '    IDH,'
      '    ID,'
      '    IDEP,'
      '    IVID,'
      '    IPROD,'
      '    IPOST,'
      '    ILIC,'
      '    DOCDATE,'
      '    DOCNUM,'
      '    GTD,'
      '    RQIN,'
      '    IDDH'
      ')'
      'VALUES('
      '    :IDH,'
      '    :ID,'
      '    :IDEP,'
      '    :IVID,'
      '    :IPROD,'
      '    :IPOST,'
      '    :ILIC,'
      '    :DOCDATE,'
      '    :DOCNUM,'
      '    :GTD,'
      '    :RQIN,'
      '    :IDDH'
      ')')
    RefreshSQL.Strings = (
      'SELECT ds2.IDH,'
      '       ds2.ID,'
      '       ds2.IDEP,'
      '       ds2.IVID,'
      '       ds2.IPROD,'
      '       ds2.IPOST,'
      '       ds2.ILIC,'
      '       ds2.DOCDATE,'
      '       ds2.DOCNUM,'
      '       ds2.GTD,'
      '       ds2.RQIN,'
      '       ds2.IDDH,'
      '       mh.FULLNAME as NameShop,'
      '       vid.NAMECLA as NameVid,'
      '       mk.NAMEM as NameProducer,'
      '       mk.INNM as INN,'
      '       mk.KPPM as KPP,'
      '       cli.FULLNAMECL as NameCli,'
      '       cli.INN as InnCli,'
      '       cli.KPP as KppCli,'
      '       lic.SER,'
      '       lic.SNUM,'
      '       lic.DDATEB,'
      '       lic.DDATEE,'
      '       lic.ORGAN'
      '       '
      'FROM OF_ALCG_DS2 ds2'
      'left join of_mh mh on mh.ID=ds2.IDEP'
      'left join of_classifalg vid on vid.ID=ds2.IVID'
      'left join of_makers mk on mk.ID=ds2.IPROD'
      'left join of_clients cli on cli.ID=ds2.IPOST'
      'left join of_clientslic lic on lic.ID=ds2.ILIC'
      ''
      'where(  ds2.IDH=:IDH'
      '     ) and (     DS2.IDH = :OLD_IDH'
      '    and DS2.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT ID,'
      '       PARENT,'
      '       ITYPE,'
      '       NAMEMH,'
      '       DEFPRICE,'
      '       EXPNAME,'
      '       NUMSPIS,'
      '       ISS,'
      '       GLN,'
      '       PRIOR,'
      '       FULLNAME,'
      '       INN,'
      '       KPP,'
      '       DIR1,'
      '       DIR2,'
      '       DIR3,'
      '       GB1,'
      '       GB2,'
      '       GB3,'
      '       CITY,'
      '       STREET,'
      '       HOUSE,'
      '       KORP,'
      '       POSTINDEX,'
      '       PHONE,'
      '       SERLIC,'
      '       NUMLIC,'
      '       ORGAN,'
      '       DATEB,'
      '       DATEE,'
      '       IP,'
      '       IPREG'
      'FROM OF_MH'
      'where ID=:IMH')
    Transaction = dmO.trSelect
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    AutoCommit = True
    Left = 580
    Top = 692
    poAskRecordCount = True
    object quAcloOrgID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quAcloOrgPARENT: TFIBIntegerField
      FieldName = 'PARENT'
    end
    object quAcloOrgITYPE: TFIBIntegerField
      FieldName = 'ITYPE'
    end
    object quAcloOrgNAMEMH: TFIBStringField
      FieldName = 'NAMEMH'
      Size = 100
      EmptyStrToNull = True
    end
    object quAcloOrgDEFPRICE: TFIBIntegerField
      FieldName = 'DEFPRICE'
    end
    object quAcloOrgEXPNAME: TFIBStringField
      FieldName = 'EXPNAME'
      Size = 10
      EmptyStrToNull = True
    end
    object quAcloOrgNUMSPIS: TFIBSmallIntField
      FieldName = 'NUMSPIS'
    end
    object quAcloOrgISS: TFIBSmallIntField
      FieldName = 'ISS'
    end
    object quAcloOrgGLN: TFIBStringField
      FieldName = 'GLN'
      EmptyStrToNull = True
    end
    object quAcloOrgPRIOR: TFIBSmallIntField
      FieldName = 'PRIOR'
    end
    object quAcloOrgFULLNAME: TFIBStringField
      FieldName = 'FULLNAME'
      Size = 200
      EmptyStrToNull = True
    end
    object quAcloOrgINN: TFIBStringField
      FieldName = 'INN'
      Size = 15
      EmptyStrToNull = True
    end
    object quAcloOrgKPP: TFIBStringField
      FieldName = 'KPP'
      Size = 15
      EmptyStrToNull = True
    end
    object quAcloOrgDIR1: TFIBStringField
      FieldName = 'DIR1'
      Size = 50
      EmptyStrToNull = True
    end
    object quAcloOrgDIR2: TFIBStringField
      FieldName = 'DIR2'
      Size = 50
      EmptyStrToNull = True
    end
    object quAcloOrgDIR3: TFIBStringField
      FieldName = 'DIR3'
      Size = 50
      EmptyStrToNull = True
    end
    object quAcloOrgGB1: TFIBStringField
      FieldName = 'GB1'
      Size = 50
      EmptyStrToNull = True
    end
    object quAcloOrgGB2: TFIBStringField
      FieldName = 'GB2'
      Size = 50
      EmptyStrToNull = True
    end
    object quAcloOrgGB3: TFIBStringField
      FieldName = 'GB3'
      Size = 50
      EmptyStrToNull = True
    end
    object quAcloOrgCITY: TFIBStringField
      FieldName = 'CITY'
      Size = 50
      EmptyStrToNull = True
    end
    object quAcloOrgSTREET: TFIBStringField
      FieldName = 'STREET'
      Size = 50
      EmptyStrToNull = True
    end
    object quAcloOrgHOUSE: TFIBStringField
      FieldName = 'HOUSE'
      Size = 10
      EmptyStrToNull = True
    end
    object quAcloOrgKORP: TFIBStringField
      FieldName = 'KORP'
      Size = 5
      EmptyStrToNull = True
    end
    object quAcloOrgPOSTINDEX: TFIBStringField
      FieldName = 'POSTINDEX'
      Size = 10
      EmptyStrToNull = True
    end
    object quAcloOrgPHONE: TFIBStringField
      FieldName = 'PHONE'
      EmptyStrToNull = True
    end
    object quAcloOrgSERLIC: TFIBStringField
      FieldName = 'SERLIC'
      Size = 50
      EmptyStrToNull = True
    end
    object quAcloOrgNUMLIC: TFIBStringField
      FieldName = 'NUMLIC'
      Size = 50
      EmptyStrToNull = True
    end
    object quAcloOrgORGAN: TFIBStringField
      FieldName = 'ORGAN'
      Size = 200
      EmptyStrToNull = True
    end
    object quAcloOrgDATEB: TFIBDateField
      FieldName = 'DATEB'
    end
    object quAcloOrgDATEE: TFIBDateField
      FieldName = 'DATEE'
    end
    object quAcloOrgIP: TFIBIntegerField
      FieldName = 'IP'
    end
    object quAcloOrgIPREG: TFIBStringField
      FieldName = 'IPREG'
      Size = 200
      EmptyStrToNull = True
    end
  end
  object quChangeGr: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_CHANGEGR'
      'SET '
      '    NAME = :NAME,'
      '    FIFO = :FIFO'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_CHANGEGR'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_CHANGEGR('
      '    ID,'
      '    NAME,'
      '    FIFO'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAME,'
      '    :FIFO'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    NAME,'
      '    FIFO'
      'FROM'
      '    OF_CHANGEGR '
      ''
      ' WHERE '
      '        OF_CHANGEGR.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    NAME,'
      '    FIFO'
      'FROM'
      '    OF_CHANGEGR ')
    Transaction = trSelTara
    Database = dmO.OfficeRnDb
    UpdateTransaction = trD
    AutoCommit = True
    Left = 960
    Top = 440
    poAskRecordCount = True
    object quChangeGrID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quChangeGrNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 150
      EmptyStrToNull = True
    end
    object quChangeGrFIFO: TFIBSmallIntField
      FieldName = 'FIFO'
    end
  end
  object quChange: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_CHANGE'
      'SET '
      '    KOEF = :KOEF,'
      '    PRIOR = :PRIOR,'
      '    IDATEB = :IDATEB,'
      '    IDATEE = :IDATEE,'
      '    IDM = :IDM,'
      '    KM = :KM'
      'WHERE'
      '    IDGR = :OLD_IDGR'
      '    and IDCARD = :OLD_IDCARD'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_CHANGE'
      'WHERE'
      '        IDGR = :OLD_IDGR'
      '    and IDCARD = :OLD_IDCARD'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_CHANGE('
      '    IDGR,'
      '    IDCARD,'
      '    KOEF,'
      '    PRIOR,'
      '    IDATEB,'
      '    IDATEE,'
      '    IDM,'
      '    KM'
      ')'
      'VALUES('
      '    :IDGR,'
      '    :IDCARD,'
      '    :KOEF,'
      '    :PRIOR,'
      '    :IDATEB,'
      '    :IDATEE,'
      '    :IDM,'
      '    :KM'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    sp.IDGR,'
      '    sp.IDCARD,'
      '    sp.KOEF,'
      '    sp.PRIOR,'
      '    sp.IDATEB,'
      '    sp.IDATEE,'
      '    sp.IDM,'
      '    sp.KM,'
      '    ca.NAME,'
      '    me.NAMESHORT,'
      '    ca.IMESSURE'
      'FROM OF_CHANGE sp'
      'left join OF_CARDS ca on ca.ID=sp.IDCARD'
      'left join OF_MESSUR me on me.ID=sp.IDM'
      'where(  IDGR=:IGR'
      '     ) and (     SP.IDGR = :OLD_IDGR'
      '    and SP.IDCARD = :OLD_IDCARD'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    sp.IDGR,'
      '    sp.IDCARD,'
      '    sp.KOEF,'
      '    sp.PRIOR,'
      '    sp.IDATEB,'
      '    sp.IDATEE,'
      '    sp.IDM,'
      '    sp.KM,'
      '    ca.NAME,'
      '    me.NAMESHORT,'
      '    ca.IMESSURE'
      'FROM OF_CHANGE sp'
      'left join OF_CARDS ca on ca.ID=sp.IDCARD'
      'left join OF_MESSUR me on me.ID=sp.IDM'
      'where IDGR=:IGR'
      'order by sp.PRIOR')
    OnCalcFields = quChangeCalcFields
    Transaction = trSelTara
    Database = dmO.OfficeRnDb
    UpdateTransaction = trD
    AutoCommit = True
    Left = 1024
    Top = 440
    poAskRecordCount = True
    object quChangeIDGR: TFIBIntegerField
      FieldName = 'IDGR'
    end
    object quChangeIDCARD: TFIBIntegerField
      FieldName = 'IDCARD'
    end
    object quChangeKOEF: TFIBFloatField
      FieldName = 'KOEF'
      DisplayFormat = '0.000'
    end
    object quChangePRIOR: TFIBIntegerField
      FieldName = 'PRIOR'
    end
    object quChangeIDATEB: TFIBIntegerField
      FieldName = 'IDATEB'
    end
    object quChangeIDATEE: TFIBIntegerField
      FieldName = 'IDATEE'
    end
    object quChangeIDM: TFIBIntegerField
      FieldName = 'IDM'
    end
    object quChangeKM: TFIBFloatField
      FieldName = 'KM'
    end
    object quChangeNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 200
      EmptyStrToNull = True
    end
    object quChangeNAMESHORT: TFIBStringField
      FieldName = 'NAMESHORT'
      Size = 50
      EmptyStrToNull = True
    end
    object quChangeEDIZM: TStringField
      FieldKind = fkCalculated
      FieldName = 'EDIZM'
      Size = 10
      Calculated = True
    end
    object quChangeIMESSURE: TFIBIntegerField
      FieldName = 'IMESSURE'
    end
  end
  object dsquChange: TDataSource
    DataSet = quChange
    Left = 1024
    Top = 492
  end
  object quRepRealAP: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHPR'
      'SET '
      '    IDATE = :IDATE,'
      '    DDATE = :DDATE,'
      '    COMMENT = :COMMENT'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHPR'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHPR('
      '    ID,'
      '    IDATE,'
      '    DDATE,'
      '    COMMENT'
      ')'
      'VALUES('
      '    :ID,'
      '    :IDATE,'
      '    :DDATE,'
      '    :COMMENT'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    IDATE,'
      '    DDATE,'
      '    COMMENT'
      'FROM'
      '    OF_DOCHPR '
      'where(  IDATE>=:DATEB'
      'and IDATE<=:DATEE'
      '     ) and (     OF_DOCHPR.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'select  hd.idskl, hd.datedoc, sp.articul, '#39#39' as BarCode, ca.name' +
        ', sp.quant, sp.idm, ca.algclass, ca.vol'
      'from of_docspecoutc sp'
      'left join  of_docheadoutb hd on hd.id=sp.idhead'
      'left join of_cards ca on ca.id=sp.articul'
      'where hd.idskl=:ISKL'
      'and hd.iactive=1'
      'and ((hd.oper='#39'Sale'#39')or(hd.oper='#39'SaleBank'#39'))'
      'and hd.datedoc>=:DDATEB'
      'and hd.datedoc<:DDATEE'
      'and ca.algclass>0'
      'and ca.algclass<999'
      'order by hd.datedoc')
    Transaction = dmO.trSel1
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    AutoCommit = True
    Left = 1068
    Top = 24
    poAskRecordCount = True
    object quRepRealAPIDSKL: TFIBIntegerField
      FieldName = 'IDSKL'
    end
    object quRepRealAPDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object quRepRealAPARTICUL: TFIBIntegerField
      FieldName = 'ARTICUL'
    end
    object quRepRealAPNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 200
      EmptyStrToNull = True
    end
    object quRepRealAPQUANT: TFIBFloatField
      FieldName = 'QUANT'
      DisplayFormat = '0.00'
    end
    object quRepRealAPIDM: TFIBIntegerField
      FieldName = 'IDM'
    end
    object quRepRealAPALGCLASS: TFIBIntegerField
      FieldName = 'ALGCLASS'
    end
    object quRepRealAPVOL: TFIBFloatField
      FieldName = 'VOL'
    end
    object quRepRealAPBARCODE: TFIBStringField
      FieldName = 'BARCODE'
      Size = 0
      EmptyStrToNull = True
    end
  end
  object dsquRepRealAP: TDataSource
    DataSet = quRepRealAP
    Left = 1068
    Top = 80
  end
  object quRepRealItog: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHPR'
      'SET '
      '    IDATE = :IDATE,'
      '    DDATE = :DDATE,'
      '    COMMENT = :COMMENT'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHPR'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHPR('
      '    ID,'
      '    IDATE,'
      '    DDATE,'
      '    COMMENT'
      ')'
      'VALUES('
      '    :ID,'
      '    :IDATE,'
      '    :DDATE,'
      '    :COMMENT'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    IDATE,'
      '    DDATE,'
      '    COMMENT'
      'FROM'
      '    OF_DOCHPR '
      'where(  IDATE>=:DATEB'
      'and IDATE<=:DATEE'
      '     ) and (     OF_DOCHPR.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'select  sp.articul, '#39#39' as BarCode, ca.name, ca.algclass, ca.vol,' +
        ' SUM(sp.quant) as QUANTITOG'
      'from of_docspecoutc sp'
      'left join  of_docheadoutb hd on hd.id=sp.idhead'
      'left join of_cards ca on ca.id=sp.articul'
      'where hd.idskl=:ISKL'
      'and hd.iactive=1'
      'and ((hd.oper='#39'Sale'#39')or(hd.oper='#39'SaleBank'#39'))'
      'and hd.datedoc>=:DDATEB'
      'and hd.datedoc<:DDATEE'
      'and ca.algclass>0'
      'and ca.algclass<999'
      'group by sp.articul, ca.name, ca.algclass, ca.vol'
      'order by ca.algclass, ca.name')
    Transaction = dmO.trSel1
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    AutoCommit = True
    Left = 1068
    Top = 160
    poAskRecordCount = True
    object quRepRealItogARTICUL: TFIBIntegerField
      FieldName = 'ARTICUL'
    end
    object quRepRealItogBARCODE: TFIBStringField
      FieldName = 'BARCODE'
      Size = 0
      EmptyStrToNull = True
    end
    object quRepRealItogNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 200
      EmptyStrToNull = True
    end
    object quRepRealItogALGCLASS: TFIBIntegerField
      FieldName = 'ALGCLASS'
    end
    object quRepRealItogVOL: TFIBFloatField
      FieldName = 'VOL'
    end
    object quRepRealItogQUANTITOG: TFIBFloatField
      FieldName = 'QUANTITOG'
    end
  end
  object quFMakers: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_MAKERS'
      'SET '
      '    NAMEM = :NAMEM,'
      '    INNM = :INNM,'
      '    KPPM = :KPPM'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_MAKERS'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_MAKERS('
      '    ID,'
      '    NAMEM,'
      '    INNM,'
      '    KPPM'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAMEM,'
      '    :INNM,'
      '    :KPPM'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    NAMEM,'
      '    INNM,'
      '    KPPM'
      'FROM'
      '    OF_MAKERS '
      ''
      ' WHERE '
      '        OF_MAKERS.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'SELECT ID,NAMEM,INNM,KPPM'
      'FROM OF_MAKERS ')
    Transaction = trSelRep
    Database = dmO.OfficeRnDb
    UpdateTransaction = trD
    AutoCommit = True
    Left = 176
    Top = 820
    poAskRecordCount = True
    object quFMakersID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quFMakersNAMEM: TFIBStringField
      FieldName = 'NAMEM'
      Size = 200
      EmptyStrToNull = True
    end
    object quFMakersINNM: TFIBStringField
      FieldName = 'INNM'
      Size = 15
      EmptyStrToNull = True
    end
    object quFMakersKPPM: TFIBStringField
      FieldName = 'KPPM'
      Size = 15
      EmptyStrToNull = True
    end
  end
  object quCliLicFind: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_CLIENTSLIC'
      'SET '
      '    ICLI = :ICLI,'
      '    IDATEB = :IDATEB,'
      '    DDATEB = :DDATEB,'
      '    IDATEE = :IDATEE,'
      '    DDATEE = :DDATEE,'
      '    SER = :SER,'
      '    SNUM = :SNUM,'
      '    ORGAN = :ORGAN'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_CLIENTSLIC'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_CLIENTSLIC('
      '    ID,'
      '    ICLI,'
      '    IDATEB,'
      '    DDATEB,'
      '    IDATEE,'
      '    DDATEE,'
      '    SER,'
      '    SNUM,'
      '    ORGAN'
      ')'
      'VALUES('
      '    :ID,'
      '    :ICLI,'
      '    :IDATEB,'
      '    :DDATEB,'
      '    :IDATEE,'
      '    :DDATEE,'
      '    :SER,'
      '    :SNUM,'
      '    :ORGAN'
      ')')
    RefreshSQL.Strings = (
      'SELECT ID,'
      '       ICLI,'
      '       IDATEB,'
      '       DDATEB,'
      '       IDATEE,'
      '       DDATEE,'
      '       SER,'
      '       SNUM,'
      '       ORGAN'
      'FROM OF_CLIENTSLIC'
      'where(  ICLI=:ICLI'
      '     ) and (     OF_CLIENTSLIC.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'select first 1 id, icli, idateb, ddateb, idatee, ddatee, ser, sn' +
        'um, organ'
      'from of_clientslic'
      'where icli=:ICLI'
      'and idateb<:IDB'
      'and idatee>:IDE'
      'order by id desc')
    Transaction = trSelRep
    Database = dmO.OfficeRnDb
    UpdateTransaction = trU
    AutoCommit = True
    Left = 29
    Top = 842
    poAskRecordCount = True
    object quCliLicFindID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quCliLicFindICLI: TFIBIntegerField
      FieldName = 'ICLI'
    end
    object quCliLicFindIDATEB: TFIBIntegerField
      FieldName = 'IDATEB'
    end
    object quCliLicFindDDATEB: TFIBDateField
      FieldName = 'DDATEB'
    end
    object quCliLicFindIDATEE: TFIBIntegerField
      FieldName = 'IDATEE'
    end
    object quCliLicFindDDATEE: TFIBDateField
      FieldName = 'DDATEE'
    end
    object quCliLicFindSER: TFIBStringField
      FieldName = 'SER'
      Size = 50
      EmptyStrToNull = True
    end
    object quCliLicFindSNUM: TFIBStringField
      FieldName = 'SNUM'
      Size = 50
      EmptyStrToNull = True
    end
    object quCliLicFindORGAN: TFIBStringField
      FieldName = 'ORGAN'
      Size = 200
      EmptyStrToNull = True
    end
  end
end
