// JCL_DEBUG_EXPERT_GENERATEJDBG OFF
// JCL_DEBUG_EXPERT_INSERTJDBG OFF
// JCL_DEBUG_EXPERT_DELETEMAPFILE OFF
program RnOffice;

uses
  Forms,
  MainRnOffice in 'MainRnOffice.pas' {fmMainRnOffice},
  dmOffice in 'dmOffice.pas' {dmO: TDataModule},
  Un1 in 'Un1.pas',
  OMessure in 'OMessure.pas' {fmMessure},
  AddMess in 'AddMess.pas' {fmAddMess},
  Goods in 'Goods.pas' {fmGoods},
  AddClass in 'AddClass.pas' {fmAddClass},
  PriceType in 'PriceType.pas' {fmPriceType},
  AddPriceT in 'AddPriceT.pas' {fmAddPriceT},
  MH in 'MH.pas' {fmMH},
  AddMH in 'AddMH.pas' {fmAddMH},
  AddGoods in 'AddGoods.pas' {fmAddGood},
  AddBar in 'AddBar.pas' {fmAddBar},
  FindResult in 'FindResult.pas' {fmFind},
  AddEUGoods in 'AddEUGoods.pas' {fmAddEUGoods},
  Clients in 'Clients.pas' {fmClients},
  AddClient in 'AddClient.pas' {fmAddClients},
  DocsIn in 'DocsIn.pas' {fmDocsIn},
  PeriodUni in 'PeriodUni.pas' {fmPeriodUni},
  AddDoc1 in 'AddDoc1.pas' {fmAddDoc1},
  TCard in 'TCard.pas' {fmTCard},
  CurMessure in 'CurMessure.pas' {fmCurMessure},
  GoodsSel in 'GoodsSel.pas' {fmGoodsSel},
  TransMenuBack in 'TransMenuBack.pas' {fmMenuCr},
  uTransM in 'uTransM.pas' {fmTransM},
  SelPerSkl in 'SelPerSkl.pas' {fmSelPerSkl},
  MoveSel in 'MoveSel.pas' {fmMoveSel},
  DocsOut in 'DocsOut.pas' {fmDocsOut},
  AddDoc2 in 'AddDoc2.pas' {fmAddDoc2},
  SelPartIn in 'SelPartIn.pas' {fmPartIn},
  SelPartIn1 in 'SelPartIn1.pas' {fmPartIn1},
  CalcSeb in 'CalcSeb.pas' {fmCalcSeb},
  DocOutB in 'DocOutB.pas' {fmDocsOutB},
  Period3 in 'Period3.pas' {fmSelPerSkl1},
  DOBSpec in 'DOBSpec.pas' {fmDOBSpec},
  Message in 'Message.pas' {fmMessage},
  DMOReps in 'DMOReps.pas' {dmORep: TDataModule},
  ExportFromOf in 'ExportFromOf.pas' {fmExport},
  u2fdk in 'U2FDK.PAS',
  TOSel in 'TOSel.pas' {fmTO},
  CardsMove in 'CardsMove.pas' {fmCardsMove},
  TBuff in 'TBuff.pas' {fmTBuff},
  AddInv in 'AddInv.pas' {fmAddInv},
  DocInv in 'DocInv.pas' {fmDocsInv},
  FCards in 'FCards.pas' {fmFCards},
  RepPrib in 'RepPrib.pas' {fmRepPrib},
  RepObSa in 'RepObSa.pas' {fmRepOb},
  DocCompl in 'DocCompl.pas' {fmDocsCompl},
  AddCompl in 'AddCompl.pas' {fmAddCompl},
  ActPer in 'ActPer.pas' {fmDocsActs},
  AddAct in 'AddAct.pas' {fmAddAct},
  Refer in 'Refer.pas' {fmComm},
  AddDoc3 in 'AddDoc3.pas' {fmAddDoc3},
  DocsVn in 'DocsVn.pas' {fmDocsVn},
  Input in 'Input.pas' {fmInput},
  OperType in 'OperType.pas' {fmOperType},
  AddOperT in 'AddOperT.pas' {fmAddOper},
  DocOutR in 'DocOutR.pas' {fmDocsReal},
  AddDoc4 in 'AddDoc4.pas' {fmAddDoc4},
  RecalcPer in 'RecalcPer.pas' {fmRecalcPer},
  RecalcReal in 'RecalcReal.pas' {fmRecalcPer1},
  PerA_Office in 'PerA_Office\PerA_Office.pas' {fmPerA_Office},
  ClassSel in 'ClassSel.pas' {fmClassSel},
  ParamSel in 'ParamSel.pas' {fmParamSel},
  RepPost in 'RepPost.pas' {fmRepPost},
  FindSebPr in 'FindSebPr.pas' {fmFindSebPr},
  SummaryReal in 'SummaryReal.pas' {fmSummary},
  RecalcPart in 'RecalcPart.pas' {fmRecalcPer2},
  of_TTKView in 'of_TTKView.pas' {fmTTKView},
  SortPar in 'SortPar.pas' {fmSortF},
  of_EditPosMenu in 'of_EditPosMenu.pas' {fmEditPosM},
  of_AvansRep in 'of_AvansRep.pas' {fmRepAvans},
  SprBGU in 'SprBGU.pas' {fmSprBGU},
  SetStatus in 'SetStatus.pas' {fmSetSt},
  TestPrice in 'TestPrice.pas' {fmTestPrice},
  TermoObr in 'TermoObr.pas' {fmTermoObr},
  AddTermoObr in 'AddTermoObr.pas' {fmAddTermoObr},
  InputMess in 'InputMess.pas' {fmInputMess},
  CalcCal in 'CalcCal.pas' {fmCalcCal},
  sumprops in 'SummaProp\sumprops.pas',
  rCategory in 'rCategory.pas' {fmRCategory},
  ExcelList in 'ExcelList.pas' {fmExcelList},
  SelPerSkl2 in 'SelPerSkl2.pas' {fmSelPerSkl2},
  RemnsDay in 'RemnsDay.pas' {fmRemnsSpeed},
  AddBGU in 'AddBGU.pas' {fmAddBGU},
  SummaryRDoc in 'SummaryRDoc.pas' {fmSummary1},
  TCardAddPars in 'TCardAddPars.pas' {fmTCardsAddPars},
  ImportDoc in 'ImportDoc.pas' {fmImportDocR},
  SetStatusBZ in 'SetStatusBZ.pas' {fmSetStatusBZ},
  CB in 'CB.pas' {dmCB: TDataModule},
  ClassAlg in 'ClassAlg.pas' {fmAlgClass},
  ImpExcelStr in 'ImpExcelStr.pas' {fmImpExcel: Unit1},
  AddAlgClass in 'AddAlgClass.pas' {fmAddAlgClass},
  Makers in 'Makers.pas' {fmMakers},
  AddMaker in 'AddMaker.pas' {fmAddMaker},
  SetCardsPars in 'SetCardsPars.pas' {fmSetCardsParams},
  CliLic in 'CliLic.pas' {fmCliLic},
  AddCliLic in 'AddCliLic.pas' {fmAddCliLic},
  PreAlcogol in 'PreAlcogol.pas' {fmPreAlg},
  RepAlcogol in 'RepAlcogol.pas' {fmAlg},
  SElPerSkl3 in 'SElPerSkl3.pas' {fmSelPerSkl3},
  Tara in 'Tara.pas' {fmTara},
  AlcoDecl in 'AlcoDecl.pas' {fmAlcoE},
  AddAlcoDecl in 'AddAlcoDecl.pas' {fmAddAlco},
  RepSS in 'RepSS.pas' {fmPreRepSS},
  RepSSRep in 'RepSSRep.pas' {fmRepSS},
  Pre in 'Pre.pas' {fmPre},
  DetPosSum in 'DetPosSum.pas' {fmDetPosSum},
  UpLica in 'UpLica.pas' {fmUpLica},
  AddUPL in 'AddUPL.pas' {fmAddUPL},
  SetOper in 'SetOper.pas' {fmSetOper},
  AddMDL in 'AddMDL.pas' {fmAddMDL},
  AddMDG in 'AddMDG.pas' {fmMDG},
  DocPrice in 'DocPrice.pas' {fmDocsPrice},
  AddDocPrice in 'AddDocPrice.pas' {fmAddDocsPrice},
  RecalcPrice in 'RecalcPrice.pas' {fmRecalcPrice},
  RMol in 'RMol.pas' {fmRMol},
  dbCash in 'dbCash.pas' {dmCash: TDataModule},
  Status in 'Status.pas' {fmSt},
  RepPars in 'RepPars.pas' {fmRepPars},
  RepDocMove in 'RepDocMove.pas' {fmRepDocMove},
  PartInRemn in 'PartInRemn.pas' {fmPartRemn},
  CreateBFromP in 'CreateBFromP.pas' {fmCreateBFromP},
  Drivers in 'Drivers.pas' {fmDrivers},
  AddDRV in 'AddDRV.pas' {fmAddDrv},
  SelTermoPr in 'SelTermoPr.pas' {fmTermoPr},
  Change in 'Change.pas' {fmChange},
  AddChangeGr in 'AddChangeGr.pas' {fmAddChangeGr},
  CasrdsChange in 'CasrdsChange.pas' {fmCardsChange},
  SelChangeGr in 'SelChangeGr.pas' {fmSelChangeGr},
  RepRealAP in 'RepRealAP.pas' {fmRepSaleAP},
  SelPost in 'SelPost.pas' {fmSelPost};

{$R *.res}

begin
  Application.Initialize;
  with TfmPre.Create(nil) do
  try
    Show;  // show a splash screen contain ProgressBar control
    Update; // force display of FormPre
    prWrSt('fmMainRnOffice');
    Application.CreateForm(TfmMainRnOffice, fmMainRnOffice);
  Application.CreateForm(TfmSelPost, fmSelPost);
  prWrSt('dmO');
    Application.CreateForm(TdmO, dmO);
    prWrSt('dmCash');
    Application.CreateForm(TdmCash, dmCash);
    prWrSt('fmMessure');
    Application.CreateForm(TfmMessure, fmMessure);
    prWrSt('fmAddMess');
    Application.CreateForm(TfmAddMess, fmAddMess);
    prWrSt('fmAddClass');
    Application.CreateForm(TfmAddClass, fmAddClass);
    prWrSt('fmPriceType');
    Application.CreateForm(TfmPriceType, fmPriceType);
    prWrSt('fmMH');
    Application.CreateForm(TfmMH, fmMH);
    prWrSt('fmAddGood');
    Application.CreateForm(TfmAddGood, fmAddGood);
    prWrSt('fmFind');
    Application.CreateForm(TfmFind, fmFind);
    prWrSt('fmAddEUGoods');
    Application.CreateForm(TfmAddEUGoods, fmAddEUGoods);
    prWrSt('fmClients');
    Application.CreateForm(TfmClients, fmClients);
    prWrSt('fmAddClients');
    Application.CreateForm(TfmAddClients, fmAddClients);
    prWrSt('fmDocsIn');
    Application.CreateForm(TfmDocsIn, fmDocsIn);
    prWrSt('fmPeriodUni');
    Application.CreateForm(TfmPeriodUni, fmPeriodUni);
    prWrSt('fmAddDoc1');
    Application.CreateForm(TfmAddDoc1, fmAddDoc1);
    prWrSt('fmTCard');
    Application.CreateForm(TfmTCard, fmTCard);
    prWrSt('fmCurMessure');
    Application.CreateForm(TfmCurMessure, fmCurMessure);
    prWrSt('fmMoveSel');
    Application.CreateForm(TfmMoveSel, fmMoveSel);
    prWrSt('fmDocsOut');
    Application.CreateForm(TfmDocsOut, fmDocsOut);
    prWrSt('fmAddDoc2');
    Application.CreateForm(TfmAddDoc2, fmAddDoc2);
    prWrSt('fmDocsOutB');
    Application.CreateForm(TfmDocsOutB, fmDocsOutB);
    prWrSt('fmDOBSpec');
    Application.CreateForm(TfmDOBSpec, fmDOBSpec);
    prWrSt('dmORep');
    Application.CreateForm(TdmORep, dmORep);
    prWrSt('fmTO');
    Application.CreateForm(TfmTO, fmTO);
    prWrSt('fmCardsMove');
    Application.CreateForm(TfmCardsMove, fmCardsMove);
    prWrSt('fmAddInv');
    Application.CreateForm(TfmAddInv, fmAddInv);
    prWrSt('fmDocsInv');
    Application.CreateForm(TfmDocsInv, fmDocsInv);
    prWrSt('fmFCards');
    Application.CreateForm(TfmFCards, fmFCards);
    prWrSt('fmRepPrib');
    Application.CreateForm(TfmRepPrib, fmRepPrib);
    prWrSt('fmRepOb');
    Application.CreateForm(TfmRepOb, fmRepOb);
    prWrSt('fmDocsCompl');
    Application.CreateForm(TfmDocsCompl, fmDocsCompl);
    prWrSt('fmAddCompl');
    Application.CreateForm(TfmAddCompl, fmAddCompl);
    prWrSt('fmDocsActs');
    Application.CreateForm(TfmDocsActs, fmDocsActs);
    prWrSt('fmAddAct');
    Application.CreateForm(TfmAddAct, fmAddAct);
    prWrSt('fmAddDoc3');
    Application.CreateForm(TfmAddDoc3, fmAddDoc3);
    prWrSt('fmDocsVn');
    Application.CreateForm(TfmDocsVn, fmDocsVn);
    prWrSt('fmAddOper');
    Application.CreateForm(TfmAddOper, fmAddOper);
    prWrSt('fmAddDoc4');
    Application.CreateForm(TfmAddDoc4, fmAddDoc4);
    prWrSt('fmRepPost');
    Application.CreateForm(TfmRepPost, fmRepPost);
    prWrSt('fmTTKView');
    Application.CreateForm(TfmTTKView, fmTTKView);
    prWrSt('fmCalcSeb');
    Application.CreateForm(TfmCalcSeb, fmCalcSeb);
    prWrSt('fmTestPrice');
    Application.CreateForm(TfmTestPrice, fmTestPrice);
    prWrSt('fmRCategory');
    Application.CreateForm(TfmRCategory, fmRCategory);
    prWrSt('fmRemnsSpeed');
    Application.CreateForm(TfmRemnsSpeed, fmRemnsSpeed);
    prWrSt('fmAddBGU');
    Application.CreateForm(TfmAddBGU, fmAddBGU);
    prWrSt('fmSummary1');
    Application.CreateForm(TfmSummary1, fmSummary1);
    prWrSt('fmTCardsAddPars');
    Application.CreateForm(TfmTCardsAddPars, fmTCardsAddPars);
    prWrSt('fmSetStatusBZ');
    Application.CreateForm(TfmSetStatusBZ, fmSetStatusBZ);
    prWrSt('dmCB');
    Application.CreateForm(TdmCB, dmCB);
    prWrSt('fmMakers');
    Application.CreateForm(TfmMakers, fmMakers);
    prWrSt('fmSetCardsParams');
    Application.CreateForm(TfmSetCardsParams, fmSetCardsParams);
    prWrSt('fmCliLic');
    Application.CreateForm(TfmCliLic, fmCliLic);
    prWrSt('fmAddCliLic');
    Application.CreateForm(TfmAddCliLic, fmAddCliLic);
    prWrSt('fmPreAlg');
    Application.CreateForm(TfmPreAlg, fmPreAlg);
    prWrSt('fmAlg');
    Application.CreateForm(TfmAlg, fmAlg);
    prWrSt('fmTara');
    Application.CreateForm(TfmTara, fmTara);
    prWrSt('fmAlcoE');
    Application.CreateForm(TfmAlcoE, fmAlcoE);
    prWrSt('fmAddAlco');
    Application.CreateForm(TfmAddAlco, fmAddAlco);
    prWrSt('fmRepSS');
    Application.CreateForm(TfmRepSS, fmRepSS);
    prWrSt('fmDetPosSum');
    Application.CreateForm(TfmDetPosSum, fmDetPosSum);
    prWrSt('fmUpLica');
    Application.CreateForm(TfmUpLica, fmUpLica);
    prWrSt('fmSetOper');
    Application.CreateForm(TfmSetOper, fmSetOper);
    prWrSt('fmDocsPrice');
    Application.CreateForm(TfmDocsPrice, fmDocsPrice);
    prWrSt('fmSt');
    Application.CreateForm(TfmSt, fmSt);
    prWrSt('fmRepDocMove');
    Application.CreateForm(TfmRepDocMove, fmRepDocMove);
    prWrSt('fmPartRemn');
    Application.CreateForm(TfmPartRemn, fmPartRemn);
    prWrSt('fmAddChangeGr');
    Application.CreateForm(TfmAddChangeGr, fmAddChangeGr);
    prWrSt('fmCardsChange');
    Application.CreateForm(TfmCardsChange, fmCardsChange);
    prWrSt('fmSelChangeGr');
    Application.CreateForm(TfmSelChangeGr, fmSelChangeGr);
    prWrSt('fmRepSaleAP');
    Application.CreateForm(TfmRepSaleAP, fmRepSaleAP);
    prWrSt('�������� ��.');
    prWrSt('...');
  finally
    Free;
  end;
  Application.Run;
end.
