unit AddDocPrice;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxImageComboBox, cxButtonEdit, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxControls,
  cxGridCustomView, cxGrid, ComCtrls, cxLabel, cxTextEdit, cxContainer,
  cxMaskEdit, cxDropDownEdit, cxCalendar, StdCtrls, cxButtons, ExtCtrls,
  dxmdaset, Placemnt, ActnList, XPStyleActnCtrls, ActnMan, FR_Class,
  FR_DSet, FR_DBSet;

type
  TfmAddDocsPrice = class(TForm)
    StatusBar1: TStatusBar;
    Panel2: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Panel1: TPanel;
    Label1: TLabel;
    Label16: TLabel;
    cxDateEdit1: TcxDateEdit;
    cxTextEdit3: TcxTextEdit;
    Panel3: TPanel;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    cxLabel5: TcxLabel;
    cxLabel6: TcxLabel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    GridAddDocPr: TcxGrid;
    ViewAddDocPr: TcxGridDBTableView;
    LevelAddDocPr: TcxGridLevel;
    taSpecPr: TdxMemData;
    taSpecPrIdGoods: TIntegerField;
    taSpecPrNameG: TStringField;
    taSpecPrIM: TIntegerField;
    taSpecPrSM: TStringField;
    dstaSpecPr: TDataSource;
    taSpecPrPriceOld: TFloatField;
    taSpecPrPriceNew: TFloatField;
    ViewAddDocPrIdGoods: TcxGridDBColumn;
    ViewAddDocPrNameG: TcxGridDBColumn;
    ViewAddDocPrIM: TcxGridDBColumn;
    ViewAddDocPrSM: TcxGridDBColumn;
    ViewAddDocPrPriceOld: TcxGridDBColumn;
    ViewAddDocPrPriceNew: TcxGridDBColumn;
    FormPlacement1: TFormPlacement;
    amADPr: TActionManager;
    acExit: TAction;
    acAddPos: TAction;
    acDelPos: TAction;
    acAddList: TAction;
    acDelAll: TAction;
    acSaveDoc: TAction;
    taSpecPrKM: TFloatField;
    taSpecPrPricePP: TFloatField;
    taSpecPrPriceRemn: TFloatField;
    ViewAddDocPrPricePP: TcxGridDBColumn;
    acRecalcPrice: TAction;
    PopupMenu1: TPopupMenu;
    acFillOld: TAction;
    cxLabel3: TcxLabel;
    acFillPricePP: TAction;
    acSetPricePP: TAction;
    cxLabel4: TcxLabel;
    N1: TMenuItem;
    cxButton3: TcxButton;
    frRepUstCost: TfrReport;
    frtaSpecPr: TfrDBDataSet;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acExitExecute(Sender: TObject);
    procedure acAddPosExecute(Sender: TObject);
    procedure acDelPosExecute(Sender: TObject);
    procedure cxLabel2Click(Sender: TObject);
    procedure cxLabel5Click(Sender: TObject);
    procedure acAddListExecute(Sender: TObject);
    procedure cxLabel1Click(Sender: TObject);
    procedure acDelAllExecute(Sender: TObject);
    procedure ViewAddDocPrEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure ViewAddDocPrEditKeyPress(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
    procedure cxLabel6Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure acSaveDocExecute(Sender: TObject);
    procedure acRecalcPriceExecute(Sender: TObject);
    procedure acFillOldExecute(Sender: TObject);
    procedure cxLabel3Click(Sender: TObject);
    procedure acFillPricePPExecute(Sender: TObject);
    procedure acSetPricePPExecute(Sender: TObject);
    procedure cxLabel4Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAddDocsPrice: TfmAddDocsPrice;
  bAdd:Boolean = False;

implementation

uses Un1, Goods, dmOffice, FCards, OMessure, DMOReps, DocPrice;

{$R *.dfm}

procedure TfmAddDocsPrice.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  PageControl1.Align:=alClient;
  PageControl1.ActivePageIndex:=0;
  ViewAddDocPr.RestoreFromIniFile(CurDir+GridIni);
end;

procedure TfmAddDocsPrice.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  ViewAddDocPr.StoreToIniFile(CurDir+GridIni,False);
//  fmAddDocsPrice.Release;
end;

procedure TfmAddDocsPrice.acExitExecute(Sender: TObject);
begin
  if cxButton2.Enabled then Close;
end;

procedure TfmAddDocsPrice.acAddPosExecute(Sender: TObject);
begin
  //�������� �������
  if PageControl1.ActivePageIndex=0 then
  begin
    ViewAddDocPr.BeginUpdate;

    taSpecPr.Append;
    taSpecPrIdGoods.AsInteger:=0;
    taSpecPrNameG.AsString:='';
    taSpecPrIM.AsInteger:=0;
    taSpecPrSM.AsString:='';
    taSpecPrPriceOld.AsFloat:=0;
    taSpecPrPriceNew.AsFloat:=0;
    taSpecPrKM.AsFloat:=1;
    taSpecPr.Post;

    ViewAddDocPr.EndUpdate;
    GridAddDocPr.SetFocus;

    ViewAddDocPrNameG.Options.Editing:=True;
    ViewAddDocPrNameG.Focused:=True;

    prRowFocus(ViewAddDocPr);
  end;
end;


procedure TfmAddDocsPrice.acDelPosExecute(Sender: TObject);
begin
  //������� �������
  if PageControl1.ActivePageIndex=0 then
  begin
    if taSpecPr.RecordCount>0 then
    begin
      taSpecPr.Delete;
    end;
  end;
end;

procedure TfmAddDocsPrice.cxLabel2Click(Sender: TObject);
begin
  acDelPos.Execute;
end;

procedure TfmAddDocsPrice.cxLabel5Click(Sender: TObject);
begin
  acAddPos.Execute;
end;

procedure TfmAddDocsPrice.acAddListExecute(Sender: TObject);
begin
  bAddSpecPr:=True;
  fmGoods.Show;
end;

procedure TfmAddDocsPrice.cxLabel1Click(Sender: TObject);
begin
  acAddList.Execute;
end;

procedure TfmAddDocsPrice.acDelAllExecute(Sender: TObject);
begin
  if PageControl1.ActivePageIndex=0 then
  begin
    if taSpecPr.RecordCount>0 then
    begin
      if MessageDlg('�� ������������� ������ �������� ������������ �������?',
       mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        ViewAddDocPr.BeginUpdate;
        CloseTe(taSpecPr);
        ViewAddDocPr.EndUpdate;
      end;
    end;
  end;
end;

procedure TfmAddDocsPrice.ViewAddDocPrEditKeyDown(
  Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
  AEdit: TcxCustomEdit; var Key: Word; Shift: TShiftState);
Var iCode:Integer;
    Km,rK:Real;
    sName:String;
    //���� ������ ��� ������ ����� �� ������
    iM:Integer;
    Sm:String;

begin
  with dmO do
  begin
    if (Key=$0D) then
    begin

      if ViewAddDocPr.Controller.FocusedColumn.Name='ViewAddDocPrIdGoods' then
      begin
        iCode:=VarAsType(AEdit.EditingValue, varInteger);

        quFCard.Active:=False;
        quFCard.SelectSQL.Clear;
        quFCard.SelectSQL.Add('SELECT ID,NAME,IMESSURE,PARENT,TCARD,INDS,LASTPRICEOUT,CATEGORY');
        quFCard.SelectSQL.Add('FROM OF_CARDS');
        quFCard.SelectSQL.Add('where ID='+IntToStr(iCode));
        quFCard.SelectSQL.Add('and IACTIVE>0');
        quFCard.SelectSQL.Add('and PARENT in (SELECT ID_CLASSIF FROM RCLASSIF WHERE RIGHTS=0 AND ID_PERSONAL='+its(Person.Id)+')');
        quFCard.Active:=True;

        if quFCard.RecordCount=1 then
        begin

          ViewAddDocPr.BeginUpdate;
          taSpecPr.Edit;
          taSpecPrIdGoods.AsInteger:=iCode;
          taSpecPrNameG.AsString:=quFCardNAME.AsString;
          taSpecPrIM.AsInteger:=quFCardIMESSURE.AsInteger;
          taSpecPrSM.AsString:=prFindKNM(quFCardIMESSURE.AsInteger,Km);
          taSpecPrPriceOld.AsFloat:=0;
          taSpecPrPriceNew.AsFloat:=0;
          taSpecPrKM.AsFloat:=Km;
          taSpecPr.Post;

          ViewAddDocPr.EndUpdate;
        end;
      end;
      if ViewAddDocPr.Controller.FocusedColumn.Name='ViewAddDocPrNameG' then
      begin
        sName:=VarAsType(AEdit.EditingValue, varString);

        fmFCards.ViewFCards.BeginUpdate;
        quFCard.Active:=False;
        quFCard.SelectSQL.Clear;
        quFCard.SelectSQL.Add('SELECT first 100 ID,NAME,IMESSURE,PARENT,TCARD,INDS,LASTPRICEOUT,CATEGORY');
        quFCard.SelectSQL.Add('FROM OF_CARDS');
        quFCard.SelectSQL.Add('where UPPER(NAME) like ''%'+AnsiUpperCase(sName)+'%''');
        quFCard.SelectSQL.Add('and IACTIVE>0');
        quFCard.SelectSQL.Add('and PARENT in (SELECT ID_CLASSIF FROM RCLASSIF WHERE RIGHTS=0 AND ID_PERSONAL='+its(Person.Id)+')');
        quFCard.SelectSQL.Add('Order by NAME');

        quFCard.Active:=True;

        bAdd:=True;

        quFCard.Locate('NAME',sName,[loCaseInsensitive, loPartialKey]);
        prRowFocus(fmFCards.ViewFCards);
        fmFCards.ViewFCards.EndUpdate;

        //�������� ����� ������ � ����� ������
        fmFCards.ShowModal;
        if fmFCards.ModalResult=mrOk then
        begin
          if quFCard.RecordCount>0 then
          begin
            ViewAddDocPr.BeginUpdate;
            taSpecPr.Edit;
            taSpecPrIdGoods.AsInteger:=quFCardID.AsInteger;
            taSpecPrNameG.AsString:=quFCardNAME.AsString;
            taSpecPrIM.AsInteger:=quFCardIMESSURE.AsInteger;
            taSpecPrSM.AsString:=prFindKNM(quFCardIMESSURE.AsInteger,Km);
            taSpecPrPriceOld.AsFloat:=0;
            taSpecPrPriceNew.AsFloat:=0;
            taSpecPrKM.AsFloat:=Km;
            taSpecPr.Post;
            ViewAddDocPr.EndUpdate;

            AEdit.SelectAll;
          end;
        end;

        bAdd:=False;

      end;
      if ViewAddDocPr.Controller.FocusedColumn.Name='ViewAddDocPrSM' then
      begin
        if taSpecPrIM.AsInteger>0 then
        begin
          bAddSpec:=True;
          fmMessure.ShowModal;
          if fmMessure.ModalResult=mrOk then
          begin
            iM:=iMSel; iMSel:=0;
            Sm:=prFindKNM(iM,rK); //�������� ����� �������� � ����� �����

            taSpecPr.Edit;
            taSpecPrIM.AsInteger:=iM;
            taSpecPrSM.AsString:=Sm;
            taSpecPrKM.AsFloat:=rK;
            taSpecPr.Post;
          end;
        end;
      end;

      ViewAddDocPrPriceOld.Focused:=True;

    end else
      if ViewAddDocPr.Controller.FocusedColumn.Name='ViewAddDocPrIdGoods' then
        if fTestKey(Key)=False then
          if taSpecPr.State in [dsEdit,dsInsert] then taSpecPr.Cancel;
  end;
end;

procedure TfmAddDocsPrice.ViewAddDocPrEditKeyPress(
  Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
  AEdit: TcxCustomEdit; var Key: Char);
Var Km:Real;
    sName:String;
begin
  if bAdd then exit;
  with dmO do
  begin
    if (ViewAddDocPr.Controller.FocusedColumn.Name='ViewAddDocPrNameG') then
    begin
      sName:=VarAsType(AEdit.EditingValue ,varString)+Key;
      if length(sName)>2 then
      begin
        quFCard.Active:=False;
        quFCard.SelectSQL.Clear;
        quFCard.SelectSQL.Add('SELECT  first 2 ID,NAME,IMESSURE,PARENT,TCARD,INDS,LASTPRICEOUT,CATEGORY');
        quFCard.SelectSQL.Add('FROM OF_CARDS');
        quFCard.SelectSQL.Add('where UPPER(NAME) like ''%'+AnsiUpperCase(sName)+'%''');
        quFCard.SelectSQL.Add('and IACTIVE>0');
        quFCard.SelectSQL.Add('and PARENT in (SELECT ID_CLASSIF FROM RCLASSIF WHERE RIGHTS=0 AND ID_PERSONAL='+its(Person.Id)+')');
        quFCard.SelectSQL.Add('Order by NAME');
        quFCard.Active:=True;

        if quFCard.RecordCount=1 then
        begin
          ViewAddDocPr.BeginUpdate;

          taSpecPr.Edit;
          taSpecPrIdGoods.AsInteger:=quFCardID.AsInteger;
          taSpecPrNameG.AsString:=quFCardNAME.AsString;
          taSpecPrIM.AsInteger:=quFCardIMESSURE.AsInteger;
          taSpecPrSM.AsString:=prFindKNM(quFCardIMESSURE.AsInteger,Km);
          taSpecPrPriceOld.AsFloat:=0;
          taSpecPrPriceNew.AsFloat:=0;
          taSpecPrKM.AsFloat:=Km;
          taSpecPr.Post;

          ViewAddDocPr.EndUpdate;
          AEdit.SelectAll;
          ViewAddDocPrNameG.Options.Editing:=False;
          ViewAddDocPrNameG.Focused:=True;
          Key:=#0;
        end;
      end;
    end;
  end;//}
end;


procedure TfmAddDocsPrice.cxLabel6Click(Sender: TObject);
begin
  acDelAll.Execute;
end;

procedure TfmAddDocsPrice.cxButton1Click(Sender: TObject);
begin
  acSaveDoc.Execute;
end;

procedure TfmAddDocsPrice.acSaveDocExecute(Sender: TObject);
Var IdH,Id:Integer;
begin
  IdH:=cxTextEdit3.Tag;
  if taSpecPr.State in [dsEdit,dsInsert] then taSpecPr.Post;
  if cxDateEdit1.Date<3000 then  cxDateEdit1.Date:=Date;

  if cxTextEdit3.Tag=0 then
  begin
    IDH:=GetId('DocPr');
    cxTextEdit3.Tag:=IDH;
  end;

  with dmORep do
  begin
    quDocPrId.Active:=False;
    quDocPrId.ParamByName('IDH').AsInteger:=IDH;
    quDocPrId.Active:=True;

    if quDocPrId.RecordCount=0 then quDocPrId.Append else quDocPrId.Edit;

    quDocPrIdID.AsInteger:=IDH;
    quDocPrIdIDATE.AsInteger:=Trunc(cxDateEdit1.Date);
    quDocPrIdDDATE.AsDateTime:=cxDateEdit1.Date;
    quDocPrIdCOMMENT.AsString:=cxTextEdit3.Text;
    quDocPrId.Post;

    quSpecPrSel.Active:=False;
    quSpecPrSel.ParamByName('IDH').AsInteger:=IDH;
    quSpecPrSel.Active:=True;

    quSpecPrSel.First;
    while not quSpecPrSel.Eof do quSpecPrSel.Delete;

    Id:=1;

    ViewAddDocPr.BeginUpdate;

    taSpecPr.First;
    while not taSpecPr.Eof do
    begin
      quSpecPrSel.Append;

      quSpecPrSelIDH.AsInteger:=IDH;
      quSpecPrSelID.AsInteger:=Id;
      quSpecPrSelICODE.AsInteger:=taSpecPrIdGoods.AsInteger;
      quSpecPrSelOLDPRICE.AsFloat:=taSpecPrPriceOld.AsFloat;
      quSpecPrSelNEWPRICE.AsFloat:=taSpecPrPriceNew.AsFloat;
      quSpecPrSelIM.AsInteger:=taSpecPrIM.AsInteger;
      quSpecPrSelKM.AsFloat:=taSpecPrKM.AsFloat;
      quSpecPrSel.Post;

      inc(Id);

      taSpecPr.Next;
    end;

    ViewAddDocPr.EndUpdate;

    quSpecPrSel.Active:=False;
    quDocPrId.Active:=False;

    fmDocsPrice.ViewDocsPrice.BeginUpdate;
    quDocsPrice.FullRefresh;
    quDocsPrice.Locate('ID',IDH,[]);
    fmDocsPrice.ViewDocsPrice.EndUpdate;

    ShowMessage('���������� ��');
  end;
end;

procedure TfmAddDocsPrice.acRecalcPriceExecute(Sender: TObject);
begin
  //����������� ����
  with dmO do
  with dmORep do
  begin
    {
    if MessageDlg('������� �������� ��������� ��������� ��� "'+quDocsPriceCOMMENT.AsString+'" �� '+quDocsPriceDDATE.AsString+'?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      taSpecPr.First;

    end;
    }
  end;
end;

procedure TfmAddDocsPrice.acFillOldExecute(Sender: TObject);
begin
  with dmO do
  with dmORep do
  begin
    ViewAddDocPr.BeginUpdate;
    try
      taSpecPr.First;
      while not taSpecPr.Eof do
      begin
        taSpecPr.Edit;
        taSpecPrPriceOld.AsFloat:=prFOldUPrice(taSpecPrIdGoods.AsInteger,Trunc(cxDateEdit1.Date));
//        taSpecPrPricePP.AsFloat:=prFPricePP(taSpecPrIdGoods.AsInteger);
        taSpecPr.Post;

        taSpecPr.Next;
      end;
    finally
      ViewAddDocPr.EndUpdate;
    end;
    showmessage('������ ���� ���������.');
  end;
end;

procedure TfmAddDocsPrice.cxLabel3Click(Sender: TObject);
begin
  acFillOld.Execute;
end;

procedure TfmAddDocsPrice.acFillPricePPExecute(Sender: TObject);
begin
  with dmO do
  with dmORep do
  begin
//    ViewAddDocPr.BeginUpdate;
    try
      taSpecPr.First;
      while not taSpecPr.Eof do
      begin
        taSpecPr.Edit;
        taSpecPrPricePP.AsFloat:=prFPricePP(taSpecPrIdGoods.AsInteger);
        taSpecPr.Post;

        taSpecPr.Next;
        delay(10);
      end;
    finally
//      ViewAddDocPr.EndUpdate;
    end;
    showmessage('���� �� ���������.');
  end;
end;

procedure TfmAddDocsPrice.acSetPricePPExecute(Sender: TObject);
begin
  with dmO do
  with dmORep do
  begin
    ViewAddDocPr.BeginUpdate;
    try
      taSpecPr.First;
      while not taSpecPr.Eof do
      begin
        taSpecPr.Edit;
        taSpecPrPriceNew.AsFloat:=taSpecPrPricePP.AsFloat;
        taSpecPr.Post;

        taSpecPr.Next;
      end;
    finally
      ViewAddDocPr.EndUpdate;
    end;
    showmessage('���� �� ���������.');
  end;
end;

procedure TfmAddDocsPrice.cxLabel4Click(Sender: TObject);
begin
  acFillPricePP.Execute;
end;

procedure TfmAddDocsPrice.cxButton3Click(Sender: TObject);
begin
  //������
  with dmO do
  with dmORep do
  begin

    frRepUstCost.LoadFromFile(CurDir + 'DocUstCost.frf');

    frVariables.Variable['Depart']:=CommonSet.DepartName;
    frVariables.Variable['DOCDATE']:=cxDateEdit1.Date;
    frVariables.Variable['Comment']:=cxTextEdit3.Text;

    frRepUstCost.ReportName:='��������� ���.';
    frRepUstCost.PrepareReport;
    frRepUstCost.ShowPreparedReport;
  end;
end;

end.
