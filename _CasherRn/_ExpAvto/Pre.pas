unit Pre;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxTextEdit, cxMemo, cxControls, cxContainer, cxEdit,
  cxProgressBar, StdCtrls;

type
  TfmPre = class(TForm)
    Label1: TLabel;
    PBarPre: TcxProgressBar;
    Memo1: TcxMemo;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure prWrSt(S:String);
  end;

var
  fmPre: TfmPre;

implementation

uses Un1;

{$R *.dfm}

procedure TfmPre.prWrSt(S:String);
begin
  Memo1.Lines.Add(formatDateTime('dd.mm.yyyy hh:nn:ss  ',now)+S);
  if PBarPre.Position<99 then PBarPre.Position:=PBarPre.Position+1;
  if S='...' then PBarPre.Position:=100;

  Memo1.Update;
  PBarPre.Update;

  prWriteStart(formatDateTime('dd.mm.yyyy hh:nn:ss  ',now)+S);
end;


procedure TfmPre.FormCreate(Sender: TObject);
begin
  CurDir := ExtractFilePath(ParamStr(0));
  Memo1.Clear;
  PBarPre.Position:=0;
  prWriteStart('');
  prWriteStart(formatDateTime('dd.mm.yyyy hh:nn:ss  ',now)+'Start');
end;

end.
