object fmMainAutoExp: TfmMainAutoExp
  Left = 358
  Top = 181
  Width = 539
  Height = 647
  Caption = #1052#1077#1085#1077#1076#1078#1077#1088' '#1101#1082#1089#1087#1086#1088#1090#1072' '#1074' 1'#1057
  Color = 16770250
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object cxDateEdit1: TcxDateEdit
    Left = 24
    Top = 16
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 0
    Width = 145
  end
  object cxButton1: TcxButton
    Left = 400
    Top = 12
    Width = 97
    Height = 25
    Caption = #1055#1091#1089#1082
    TabOrder = 1
    OnClick = cxButton1Click
    LookAndFeel.Kind = lfOffice11
  end
  object cxCheckBox1: TcxCheckBox
    Left = 224
    Top = 16
    Caption = #1040#1074#1090#1086#1089#1090#1072#1088#1090
    ParentColor = False
    State = cbsChecked
    Style.BorderStyle = ebsOffice11
    Style.Color = clWhite
    Style.Shadow = True
    TabOrder = 2
    Width = 121
  end
  object Memo1: TcxMemo
    Left = 0
    Top = 52
    Align = alBottom
    Lines.Strings = (
      'Memo1')
    TabOrder = 3
    Height = 561
    Width = 531
  end
  object Timer1: TTimer
    Enabled = False
    OnTimer = Timer1Timer
    Left = 392
    Top = 204
  end
  object amAZ: TActionManager
    Left = 388
    Top = 132
    StyleName = 'XP Style'
    object acStart: TAction
      Caption = 'acStart'
      OnExecute = acStartExecute
    end
  end
  object taCloseHist: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_CLOSEHIST'
      'SET '
      '    DATEDOC = :DATEDOC,'
      '    IDP = :IDP,'
      '    VALEDIT = :VALEDIT,'
      '    ISKL = :ISKL'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_CLOSEHIST'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_CLOSEHIST('
      '    ID,'
      '    DATEDOC,'
      '    IDP,'
      '    VALEDIT,'
      '    ISKL'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATEDOC,'
      '    :IDP,'
      '    :VALEDIT,'
      '    :ISKL'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT h.ID,h.DATEDOC,h.IDP,h.VALEDIT,h.ISKL,pe.NAME as PNAME,mh' +
        '.NAMEMH'
      'FROM OF_CLOSEHIST h'
      'left join rpersonal pe on h.IDP=pe.ID'
      'left join of_mh mh on h.ISKL=mh.ID '
      'where(  h.ISKL=:ISKL'
      '     ) and (     H.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT h.ID,h.DATEDOC,h.IDP,h.VALEDIT,h.ISKL,pe.NAME as PNAME,mh' +
        '.NAMEMH'
      'FROM OF_CLOSEHIST h'
      'left join rpersonal pe on h.IDP=pe.ID'
      'left join of_mh mh on h.ISKL=mh.ID '
      'where h.ISKL=:ISKL'
      'order by h.ID DESC'
      '')
    Transaction = dmO.trSelect
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    AutoCommit = True
    Left = 72
    Top = 300
    object taCloseHistID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taCloseHistDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
      DisplayFormat = 'DD.MM.YYYY'
    end
    object taCloseHistIDP: TFIBIntegerField
      FieldName = 'IDP'
    end
    object taCloseHistVALEDIT: TFIBDateTimeField
      FieldName = 'VALEDIT'
      DisplayFormat = 'DD.MM.YYYY HH:MM:SSS'
    end
    object taCloseHistPNAME: TFIBStringField
      FieldName = 'PNAME'
      Size = 200
      EmptyStrToNull = True
    end
    object taCloseHistISKL: TFIBIntegerField
      FieldName = 'ISKL'
    end
    object taCloseHistNAMEMH: TFIBStringField
      FieldName = 'NAMEMH'
      Size = 100
      EmptyStrToNull = True
    end
  end
  object quMaxIdCH: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT MAX(ID) as MAXID'
      'FROM OF_CLOSEHIST ')
    Transaction = dmO.trSelect
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    Left = 144
    Top = 300
    poAskRecordCount = True
    object quMaxIdCHMAXID: TFIBIntegerField
      FieldName = 'MAXID'
    end
  end
end
