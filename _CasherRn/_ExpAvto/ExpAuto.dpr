program ExpAuto;

uses
  Forms,
  MainAutoExp in 'MainAutoExp.pas' {fmMainAutoExp},
  Pre in '..\Pre.pas' {fmPre},
  Un1 in '..\Un1.pas',
  RecalcPer in '..\RecalcPer.pas' {fmRecalcPer},
  dmOffice in '..\dmOffice.pas' {dmO: TDataModule},
  DMOReps in '..\DMOReps.pas' {dmORep: TDataModule},
  DocCompl in '..\DocCompl.pas' {fmDocsCompl},
  AddCompl in '..\AddCompl.pas' {fmAddCompl},
  ActPer in '..\ActPer.pas' {fmDocsActs},
  DocsVn in '..\DocsVn.pas' {fmDocsVn},
  DocsOut in '..\DocsOut.pas' {fmDocsOut},
  DocOutB in '..\DocOutB.pas' {fmDocsOutB},
  DOBSpec in '..\DOBSpec.pas' {fmDOBSpec},
  DocInv in '..\DocInv.pas' {fmDocsInv},
  DocOutR in '..\DocOutR.pas' {fmDocsReal},
  PeriodUni in '..\PeriodUni.pas' {fmPeriodUni},
  AddDoc1 in '..\AddDoc1.pas' {fmAddDoc1},
  Period3 in '..\Period3.pas' {fmSelPerSkl1},
  Clients in '..\Clients.pas' {fmClients},
  Goods in '..\Goods.pas' {fmGoods},
  AddClient in '..\AddClient.pas' {fmAddClients},
  FindResult in '..\FindResult.pas' {fmFind},
  TransMenuBack in '..\TransMenuBack.pas' {fmMenuCr},
  dmRnEdit in '..\dmRnEdit.pas' {dmC: TDataModule},
  AddCateg in '..\AddCateg.pas' {fmAddCateg},
  AddM in '..\AddM.pas' {fmAddM},
  AddBar in '..\AddBar.pas' {fmAddBar},
  AddClass in '..\AddClass.pas' {fmAddClass},
  AddGoods in '..\AddGoods.pas' {fmAddGood},
  TCard in '..\TCard.pas' {fmTCard},
  CurMessure in '..\CurMessure.pas' {fmCurMessure},
  GoodsSel in '..\GoodsSel.pas' {fmGoodsSel},
  CalcSeb in '..\CalcSeb.pas' {fmCalcSeb},
  Message in '..\Message.pas' {fmMessage},
  TBuff in '..\TBuff.pas' {fmTBuff},
  Refer in '..\Refer.pas' {fmComm},
  FCards in '..\FCards.pas' {fmFCards},
  OMessure in '..\OMessure.pas' {fmMessure},
  AddMess in '..\AddMess.pas' {fmAddMess},
  of_TTKView in '..\of_TTKView.pas' {fmTTKView},
  CalcCal in '..\CalcCal.pas' {fmCalcCal},
  TCardAddPars in '..\TCardAddPars.pas' {fmTCardsAddPars},
  AddEUGoods in '..\AddEUGoods.pas' {fmAddEUGoods},
  SprBGU in '..\SprBGU.pas' {fmSprBGU},
  AddBGU in '..\AddBGU.pas' {fmAddBGU},
  AddDoc2 in '..\AddDoc2.pas' {fmAddDoc2},
  SelPartIn in '..\SelPartIn.pas' {fmPartIn},
  SelPartIn1 in '..\SelPartIn1.pas' {fmPartIn1},
  MainRnOffice in '..\MainRnOffice.pas' {fmMainRnOffice},
  PriceType in '..\PriceType.pas' {fmPriceType},
  AddPriceT in '..\AddPriceT.pas' {fmAddPriceT},
  MH in '..\MH.pas' {fmMH},
  AddMH in '..\AddMH.pas' {fmAddMH},
  DocsIn in '..\DocsIn.pas' {fmDocsIn},
  SelPerSkl in '..\SelPerSkl.pas' {fmSelPerSkl},
  MoveSel in '..\MoveSel.pas' {fmMoveSel},
  CardsMove in '..\CardsMove.pas' {fmCardsMove},
  AddInv in '..\AddInv.pas' {fmAddInv},
  sumprops in '..\sumprops.pas',
  DetPosSum in '..\DetPosSum.pas' {fmDetPosSum},
  AddDoc3 in '..\AddDoc3.pas' {fmAddDoc3},
  AddAct in '..\AddAct.pas' {fmAddAct},
  AddDoc4 in '..\AddDoc4.pas' {fmAddDoc4},
  FindSebPr in '..\FindSebPr.pas' {fmFindSebPr},
  Tara in '..\Tara.pas' {fmTara},
  SummaryRDoc in '..\SummaryRDoc.pas' {fmSummary1},
  RepPrib in '..\RepPrib.pas' {fmRepPrib},
  SummaryReal in '..\SummaryReal.pas' {fmSummary},
  SortPar in '..\SortPar.pas' {fmSortF},
  ExcelList in '..\ExcelList.pas' {fmExcelList},
  SelPerSkl2 in '..\SelPerSkl2.pas' {fmSelPerSkl2},
  SetStatusBZ in '..\SetStatusBZ.pas' {fmSetStatusBZ},
  CB in '..\CB.pas' {dmCB: TDataModule},
  SElPerSkl3 in '..\SElPerSkl3.pas' {fmSelPerSkl3},
  InputMess in '..\InputMess.pas' {fmInputMess},
  ExportFromOf in '..\ExportFromOf.pas' {fmExport},
  u2fdk in '..\U2FDK.PAS',
  TOSel in '..\TOSel.pas' {fmTO},
  RepObSa in '..\RepObSa.pas' {fmRepOb},
  OperType in '..\OperType.pas' {fmOperType},
  AddOperT in '..\AddOperT.pas' {fmAddOper},
  RecalcReal in '..\RecalcReal.pas' {fmRecalcPer1},
  PerA_Office in '..\PerA_Office\PerA_Office.pas' {fmPerA_Office},
  ClassSel in '..\ClassSel.pas' {fmClassSel},
  ParamSel in '..\ParamSel.pas' {fmParamSel},
  RepPost in '..\RepPost.pas' {fmRepPost},
  RecalcPart in '..\RecalcPart.pas' {fmRecalcPer2},
  of_AvansRep in '..\of_AvansRep.pas' {fmRepAvans},
  TermoObr in '..\TermoObr.pas' {fmTermoObr},
  AddTermoObr in '..\AddTermoObr.pas' {fmAddTermoObr},
  rCategory in '..\rCategory.pas' {fmRCategory},
  RemnsDay in '..\RemnsDay.pas' {fmRemnsSpeed},
  ImportDoc in '..\ImportDoc.pas' {fmImportDocR},
  ClassAlg in '..\ClassAlg.pas' {fmAlgClass},
  ImpExcelStr in '..\ImpExcelStr.pas' {fmImpExcel},
  AddAlgClass in '..\AddAlgClass.pas' {fmAddAlgClass},
  Makers in '..\Makers.pas' {fmMakers},
  AddMaker in '..\AddMaker.pas' {fmAddMaker},
  SetCardsPars in '..\SetCardsPars.pas' {fmSetCardsParams},
  PreAlcogol in '..\PreAlcogol.pas' {fmPreAlg},
  RepAlcogol in '..\RepAlcogol.pas' {fmAlg},
  AlcoDecl in '..\AlcoDecl.pas' {fmAlcoE},
  AddAlcoDecl in '..\AddAlcoDecl.pas' {fmAddAlco},
  RepSS in '..\RepSS.pas' {fmPreRepSS},
  RepSSRep in '..\RepSSRep.pas' {fmRepSS},
  uTransM in '..\uTransM.pas' {fmTransM},
  Input in '..\Input.pas' {fmInput},
  SetStatus in '..\SetStatus.pas' {fmSetSt},
  of_EditPosMenu in '..\of_EditPosMenu.pas' {fmEditPosM},
  CliLic in '..\CliLic.pas' {fmCliLic},
  AddCliLic in '..\AddCliLic.pas' {fmAddCliLic},
  TestPrice in '..\TestPrice.pas' {fmTestPrice},
  UpLica in '..\UpLica.pas' {fmUpLica},
  AddUPL in '..\AddUPL.pas' {fmAddUPL},
  AddMDL in '..\AddMDL.pas' {fmAddMDL},
  AddMDG in '..\AddMDG.pas' {fmMDG},
  RecalcPrice in '..\RecalcPrice.pas' {fmRecalcPrice},
  RMol in '..\RMol.pas' {fmRMol},
  dbCash in '..\dbCash.pas' {dmCash: TDataModule},
  SetOper in '..\SetOper.pas' {fmSetOper},
  DocPrice in '..\DocPrice.pas' {fmDocsPrice},
  AddDocPrice in '..\AddDocPrice.pas' {fmAddDocsPrice},
  RepPars in '..\RepPars.pas' {fmRepPars},
  RepDocMove in '..\RepDocMove.pas' {fmRepDocMove},
  Status in '..\Status.pas' {fmSt},
  PartInRemn in '..\PartInRemn.pas' {fmPartRemn},
  CreateBFromP in '..\CreateBFromP.pas' {fmCreateBFromP},
  Drivers in '..\Drivers.pas' {fmDrivers},
  SelTermoPr in '..\SelTermoPr.pas' {fmTermoPr},
  AddDRV in '..\AddDRV.pas' {fmAddDrv};

{$R *.res}

begin
  Application.Initialize;
  Application.Title := '����������';
  with TfmPre.Create(nil) do
  try
    Show;  // show a splash screen contain ProgressBar control
    Update; // force display of FormPre
    prWrSt('fmMainAutoExp');
    Application.CreateForm(TfmMainAutoExp, fmMainAutoExp);
  prWrSt('dmO');
    Application.CreateForm(TdmO, dmO);
    prWrSt('dmORep');
    Application.CreateForm(TdmORep, dmORep);
    prWrSt('fmExport');
    Application.CreateForm(TfmExport, fmExport);

    {
    prWrSt('fmMainRnOffice');
    Application.CreateForm(TfmMainRnOffice, fmMainRnOffice);
    prWrSt('fmRecalcPer');
    Application.CreateForm(TfmRecalcPer, fmRecalcPer);
    prWrSt('fmDocsCompl');
    Application.CreateForm(TfmDocsCompl, fmDocsCompl);
    prWrSt('fmAddCompl');
    Application.CreateForm(TfmAddCompl, fmAddCompl);
    prWrSt('fmDocsVn');
    Application.CreateForm(TfmDocsVn, fmDocsVn);
    prWrSt('fmDocsReal');
    Application.CreateForm(TfmDocsReal, fmDocsReal);
    prWrSt('fmDocsActs');
    Application.CreateForm(TfmDocsActs, fmDocsActs);
    prWrSt('fmDocsOut');
    Application.CreateForm(TfmDocsOut, fmDocsOut);
    prWrSt('fmDocsOutB');
    Application.CreateForm(TfmDocsOutB, fmDocsOutB);
    prWrSt('fmDOBSpec');
    Application.CreateForm(TfmDOBSpec, fmDOBSpec);
    prWrSt('fmDocsInv');
    Application.CreateForm(TfmDocsInv, fmDocsInv);
    prWrSt('fmAddAct');
    Application.CreateForm(TfmAddAct, fmAddAct);
    prWrSt('fmAddDoc1');
    Application.CreateForm(TfmAddDoc1, fmAddDoc1);
    prWrSt('fmAddDoc2');
    Application.CreateForm(TfmAddDoc2, fmAddDoc2);
    prWrSt('fmAddDoc3');
    Application.CreateForm(TfmAddDoc3, fmAddDoc3);
    prWrSt('fmAddDoc4');
    Application.CreateForm(TfmAddDoc4, fmAddDoc4);
    prWrSt('fmAddInv');
    Application.CreateForm(TfmAddInv, fmAddInv);}

{
  Application.CreateForm(TfmAddCompl, fmAddCompl);
  Application.CreateForm(TfmDocsActs, fmDocsActs);
  Application.CreateForm(TfmDocsVn, fmDocsVn);
  Application.CreateForm(TfmDocsOut, fmDocsOut);
  Application.CreateForm(TfmDocsOutB, fmDocsOutB);
  Application.CreateForm(TfmDOBSpec, fmDOBSpec);
  Application.CreateForm(TfmDocsInv, fmDocsInv);
  Application.CreateForm(TfmDocsReal, fmDocsReal);
  Application.CreateForm(TfmPeriodUni, fmPeriodUni);
  Application.CreateForm(TfmAddDoc1, fmAddDoc1);
  Application.CreateForm(TfmSelPerSkl1, fmSelPerSkl1);
  Application.CreateForm(TfmClients, fmClients);
  Application.CreateForm(TfmGoods, fmGoods);
  Application.CreateForm(TfmAddClients, fmAddClients);
  Application.CreateForm(TfmFind, fmFind);
  Application.CreateForm(TfmMenuCr, fmMenuCr);
  Application.CreateForm(TdmC, dmC);
  Application.CreateForm(TfmAddCateg, fmAddCateg);
  Application.CreateForm(TfmAddM, fmAddM);
  Application.CreateForm(TfmAddBar, fmAddBar);
  Application.CreateForm(TfmAddClass, fmAddClass);
  Application.CreateForm(TfmAddGood, fmAddGood);
  Application.CreateForm(TfmTCard, fmTCard);
  Application.CreateForm(TfmCurMessure, fmCurMessure);
  Application.CreateForm(TfmGoodsSel, fmGoodsSel);
  Application.CreateForm(TfmCalcSeb, fmCalcSeb);
  Application.CreateForm(TfmMessage, fmMessage);
  Application.CreateForm(TfmTBuff, fmTBuff);
  Application.CreateForm(TfmComm, fmComm);
  Application.CreateForm(TfmFCards, fmFCards);
  Application.CreateForm(TfmMessure, fmMessure);
  Application.CreateForm(TfmAddMess, fmAddMess);
  Application.CreateForm(TfmTTKView, fmTTKView);
  Application.CreateForm(TfmCalcCal, fmCalcCal);
  Application.CreateForm(TfmTCardsAddPars, fmTCardsAddPars);
  Application.CreateForm(TfmAddEUGoods, fmAddEUGoods);
  Application.CreateForm(TfmSprBGU, fmSprBGU);
  Application.CreateForm(TfmAddBGU, fmAddBGU);
  Application.CreateForm(TfmAddDoc2, fmAddDoc2);
  Application.CreateForm(TfmPartIn, fmPartIn);
  Application.CreateForm(TfmPartIn1, fmPartIn1);
  Application.CreateForm(TfmMainRnOffice, fmMainRnOffice);
  Application.CreateForm(TfmPriceType, fmPriceType);
  Application.CreateForm(TfmAddPriceT, fmAddPriceT);
  Application.CreateForm(TfmMH, fmMH);
  Application.CreateForm(TfmAddMH, fmAddMH);
  Application.CreateForm(TfmDocsIn, fmDocsIn);
  Application.CreateForm(TfmSelPerSkl, fmSelPerSkl);
  Application.CreateForm(TfmMoveSel, fmMoveSel);
  Application.CreateForm(TfmCardsMove, fmCardsMove);
  Application.CreateForm(TfmAddInv, fmAddInv);
  Application.CreateForm(TfmDetPosSum, fmDetPosSum);
  Application.CreateForm(TfmAddDoc3, fmAddDoc3);
  Application.CreateForm(TfmAddAct, fmAddAct);
  Application.CreateForm(TfmAddDoc4, fmAddDoc4);
  Application.CreateForm(TfmFindSebPr, fmFindSebPr);
  Application.CreateForm(TfmTara, fmTara);
  Application.CreateForm(TfmSummary1, fmSummary1);
  Application.CreateForm(TfmRepPrib, fmRepPrib);
  Application.CreateForm(TfmSummary, fmSummary);
  Application.CreateForm(TfmSortF, fmSortF);
  Application.CreateForm(TfmExcelList, fmExcelList);
  Application.CreateForm(TfmSelPerSkl2, fmSelPerSkl2);
  Application.CreateForm(TfmSetStatusBZ, fmSetStatusBZ);
  Application.CreateForm(TdmCB, dmCB);
  Application.CreateForm(TfmSelPerSkl3, fmSelPerSkl3);
  Application.CreateForm(TfmInputMess, fmInputMess);
  Application.CreateForm(TfmExport, fmExport);
  Application.CreateForm(TfmTO, fmTO);
  Application.CreateForm(TfmRepOb, fmRepOb);
  Application.CreateForm(TfmOperType, fmOperType);
  Application.CreateForm(TfmAddOper, fmAddOper);
  Application.CreateForm(TfmRecalcPer1, fmRecalcPer1);
  Application.CreateForm(TfmPerA_Office, fmPerA_Office);
  Application.CreateForm(TfmClassSel, fmClassSel);
  Application.CreateForm(TfmParamSel, fmParamSel);
  Application.CreateForm(TfmRepPost, fmRepPost);
  Application.CreateForm(TfmRecalcPer2, fmRecalcPer2);
  Application.CreateForm(TfmRepAvans, fmRepAvans);
  Application.CreateForm(TfmTermoObr, fmTermoObr);
  Application.CreateForm(TfmAddTermoObr, fmAddTermoObr);
  Application.CreateForm(TfmRCategory, fmRCategory);
  Application.CreateForm(TfmRemnsSpeed, fmRemnsSpeed);
  Application.CreateForm(TfmImportDocR, fmImportDocR);
  Application.CreateForm(TfmAlgClass, fmAlgClass);
  Application.CreateForm(TfmImpExcel, fmImpExcel);
  Application.CreateForm(TfmAddAlgClass, fmAddAlgClass);
  Application.CreateForm(TfmMakers, fmMakers);
  Application.CreateForm(TfmAddMaker, fmAddMaker);
  Application.CreateForm(TfmSetCardsParams, fmSetCardsParams);
  Application.CreateForm(TfmPreAlg, fmPreAlg);
  Application.CreateForm(TfmAlg, fmAlg);
  Application.CreateForm(TfmAlcoE, fmAlcoE);
  Application.CreateForm(TfmAddAlco, fmAddAlco);
  Application.CreateForm(TfmPreRepSS, fmPreRepSS);
  Application.CreateForm(TfmRepSS, fmRepSS);
  Application.CreateForm(TfmTransM, fmTransM);
  Application.CreateForm(TfmInput, fmInput);
  Application.CreateForm(TfmSetSt, fmSetSt);
  Application.CreateForm(TfmEditPosM, fmEditPosM);
  Application.CreateForm(TfmCliLic, fmCliLic);
  Application.CreateForm(TfmAddCliLic, fmAddCliLic);
  Application.CreateForm(TfmTestPrice, fmTestPrice);}


    prWrSt('...');
  finally
    free;
  end;
  Application.Run;
end.
