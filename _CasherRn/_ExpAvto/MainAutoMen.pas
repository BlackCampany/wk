unit MainAutoMen;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, cxMemo, cxCheckBox, StdCtrls,
  cxButtons, ExtCtrls, cxControls, cxContainer, cxEdit, cxTextEdit,
  cxMaskEdit, cxDropDownEdit, cxCalendar, ActnList, XPStyleActnCtrls,
  ActnMan, DB, dxmdaset, cxSpinEdit, cxGraphics, cxLookupEdit,
  cxDBLookupEdit, cxDBLookupComboBox, ADODB, FIBDataSet, pFIBDataSet;

type
  TfmMainAutoMen = class(TForm)
    cxDateEdit1: TcxDateEdit;
    Timer1: TTimer;
    cxButton1: TcxButton;
    cxCheckBox1: TcxCheckBox;
    Memo1: TcxMemo;
    amAZ: TActionManager;
    acStart: TAction;
    taCloseHist: TpFIBDataSet;
    taCloseHistID: TFIBIntegerField;
    taCloseHistDATEDOC: TFIBDateField;
    taCloseHistIDP: TFIBIntegerField;
    taCloseHistVALEDIT: TFIBDateTimeField;
    taCloseHistPNAME: TFIBStringField;
    taCloseHistISKL: TFIBIntegerField;
    taCloseHistNAMEMH: TFIBStringField;
    quMaxIdCH: TpFIBDataSet;
    quMaxIdCHMAXID: TFIBIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure acStartExecute(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmMainAutoMen: TfmMainAutoMen;
  iCliSel:INteger;

implementation

uses Un1, DMOReps, dmOffice, AddCompl, DocCompl, RecalcPer, DocsVn,
  DocOutR, ActPer, DocsOut, DocOutB, DOBSpec, DocInv;

{$R *.dfm}

procedure TfmMainAutoMen.FormCreate(Sender: TObject);
begin
  CurDir := ExtractFilePath(ParamStr(0));

  Memo1.Clear;
  cxDateEdit1.Date:=Date;
  ReadIni;
end;

procedure TfmMainAutoMen.Timer1Timer(Sender: TObject);
Var n:Integer;
begin
  Timer1.Enabled:=False;

  for n:=0 to 10 do
  begin
    cxCheckBox1.Caption:='�� ������� '+INtToStr(10-n)+' ���.';
    delay(1000); //�������� 10 ���
    if cxCheckBox1.Checked=False then Break;
  end;

  if cxCheckBox1.Checked then
  begin
    iCliSel:=0;
    acStart.Execute;
    Close;
  end;
end;

procedure TfmMainAutoMen.acStartExecute(Sender: TObject);
Var iCurD:INteger;
    iCd:Integer;
    iWeek:INteger;
    iDb,iDe:INteger;
    IdSkl,iSkl,iMax,iM,iTC:INteger;
    rPrice,Km:Real;
    NameM:String;
    IDH:INteger;
    NumDoc:String;
    Sum0,Sum1,Sum11:Real;
    iE:INteger;
    rSum0,rSum1,rSum2,rSum3:Real;

    rSum11,rSum22:Real;
    iC:INteger;
    rQ:Real;
    rQRemn:Real;
    iNDS:Integer;
    rProcNDS,rSumIn0,rPriceIn0,rSumNDS:Real;
    iSS:INteger;
    rSumIn,rSumT:Real;
    iSklFrom,iSklTo:INteger;

    iR:INteger;

    rSumB,rPostIn,rPostOut,rVnIn,rVnOut,rInv,rQReal:Real;
    rSumBT,rPostInT,rPostOutT,rVnInT,rVnOutT,rInvT,rRealTara:Real;
    StrWk:String;
    iType:SmallINt;

begin
  //�������
  prWHAUTO('������.',Memo1);
  bPrintMemo:=False;

  Person.Id:=1031; //���
  Person.Name:='Auto';
  Person.sCli:='';

  iCurD:=Trunc(cxDateEdit1.Date);
  with dmO do
  with dmORep do
  begin
    //1 - �������� �������������� �� ���� ������� �� ��������� 7 ����
    for iCd:=iCurD-8 to iCurD-1 do
    begin
      prWHAUTO('      �������� �������������� - '+ds1(iCd),Memo1);
      prRECALCGDS.ParamByName('DDATE').AsDate:=iCD;
      prRECALCGDS.ParamByName('IDATE').AsInteger:=iCD;
      prRECALCGDS.ExecProc;
      prWHAUTO('      --- ������� ��. ',Memo1); delay(10);
    end;

    iWeek:=DayOfWeek(iCurD);
    if iWeek>1 then iWeek:=iWeek-1 else iWeek:=7;

    //2 �������� ������������

    iDb:=0;
    iDe:=1;

    if iWeek=1 then begin iDb:=0; iDe:=1; end;  //�����������  -  ������ �� ������
    if iWeek=2 then begin iDb:=5; iDe:=3; end;  //�������  - 4,5,6
    if iWeek=3 then begin iDb:=3; iDe:=3; end;  //����� - 7
    if iWeek=4 then begin iDb:=3; iDe:=3; end;  //������� - 1
    if iWeek=5 then begin iDb:=3; iDe:=3; end;  //������� - 2
    if iWeek=6 then begin iDb:=3; iDe:=3; end;  //������� - 3
    if iWeek=7 then begin iDb:=0; iDe:=1; end;  //����������� -  ������ �� ������

    prWHAUTO('   ',Memo1);
    prWHAUTO('   ������� ������������.',Memo1);

    quMHAll.Active:=False;
    quMHAll.ParamByName('IDPERSON').AsInteger:=Person.Id;
    quMHAll.Active:=True;

    quMHAll.First;
    while not quMHAll.Eof do
    begin
      prWHAUTO('    '+its(quMHAllID.AsInteger)+' '+quMHAllNAMEMH.AsString,Memo1);

      IdSkl:=quMHAllID.AsInteger;

      for iCd:=iCurD-iDb to iCurD-iDe do
      begin
        prWHAUTO('        '+ds1(iCd),Memo1);

        if CanEdit(iCd,IdSkl) then
        begin
          quDocComplAuto.Active:=False;
          quDocComplAuto.ParamByName('DATEB').AsDate:=iCd;
          quDocComplAuto.ParamByName('DATEE').AsDate:=iCd;
          quDocComplAuto.ParamByName('ISKL').AsInteger:=quMHAllID.AsInteger;
          quDocComplAuto.Active:=True;
          if (quDocComplAuto.RecordCount>0)or(IdSkl>25) then //��������
          begin //��� ����
            prWHAUTO('          - ������������ ��� ����.',Memo1);
          end else
          begin //�������
            prWHAUTO('          - ������������ ���, �������.',Memo1);

            with fmAddCompl do //������������ �������
            begin
              ViewCom.BeginUpdate;

              CloseTe(taSpec);
              CloseTe(taSpecC);
              CloseTe(teCalcB1);

              iMax:=1;

              quSpecRDay.Active:=False;
              quSpecRDay.ParamByName('DDATE').AsDateTime:=iCd;
              quSpecRDay.ParamByName('IDSKL').AsInteger:=IdSkl;
              quSpecRDay.Active:=True;
              quSpecRDay.First;
              while not quSpecRDay.Eof do
              begin
                if quSpecRDayQUANT.AsFloat>0.001 then //�������� ��� ������������� ���-�� (��������)
                begin
                  rPrice:=quSpecRDaySUMR.AsFloat/quSpecRDayQUANT.AsFloat;

                  iM:=quSpecRDayIDM.AsInteger;
                  kM:=quSpecRDayKM.AsFloat;
                  NameM:=quSpecRDayNAMESHORT.AsString;

                  quFindTCard.Active:=False;
                  quFindTCard.ParamByName('IDCARD').AsInteger:=quSpecRDayIDCARD.AsInteger;
                  quFindTCard.ParamByName('IDATE').AsDateTime:=iCd;
                  quFindTCard.Active:=True;
                  if quFindTCard.RecordCount>0 then iTC:=1 else iTC:=0;
                  quFindTCard.Active:=False;

                  if iM=0 then iM:=prFindM(quSpecRDayIDCARD.AsInteger,kM,NameM);

                  if (iM>0)and(iTC>0) then
                  begin
                    taSpec.Append;
                    taSpecNum.AsInteger:=iMax;
                    taSpecIdGoods.AsInteger:=quSpecRDayIDCARD.AsInteger;
                    taSpecNameG.AsString:=quSpecRDayNAME.AsString;
                    taSpecIM.AsInteger:=iM;
                    taSpecSM.AsString:=NameM;
                    taSpecQuantFact.AsFloat:=quSpecRDayQUANT.AsFloat;
                    taSpecPriceIn.AsFloat:=0;
                    taSpecSumIn.AsFloat:=0;
                    taSpecPriceUch.AsFloat:=rPrice;
                    taSpecSumUch.AsFloat:=quSpecRDaySUMR.AsFloat;
                    taSpecKm.AsFloat:=kM;
                    taSpecTCard.AsInteger:=1;
                    taSpec.Post;

                    inc(iMax);
                  end else
                  begin
                    prWHAUTO('������ ���������� - '+quSpecRDayIDCARD.AsString+' '+quSpecRDayNAME.AsString,fmMainAutoMen.Memo1);
                  end;
                end;
                quSpecRDay.Next;
              end;
              quSpecRDay.Active:=False;

              ViewCom.EndUpdate;

              //����� ������������ - ����� �����������
              prWHAUTO('   ������ ����������.',fmMainAutoMen.Memo1); delay(10);
              prCalcC(iCd,IdSkl);
              //���� ������� ���-��

              prWHAUTO('   �������� ���.',fmMainAutoMen.Memo1); delay(10);
              prCalcPr(IdSkl);
            end;

            prWHAUTO('   ��������.',fmMainAutoMen.Memo1); delay(10);

            IDH:=GetId('HeadCompl');
            NumDoc:='AUTO'+prGetNum(6,1);

            quDocsComlRec.Active:=False;
            quDocsComlRec.ParamByName('IDH').AsInteger:=IDH;
            quDocsComlRec.Active:=True;

            quDocsComlRec.First;

            quDocsComlRec.Append;
            quDocsComlRecID.AsInteger:=IDH;
            quDocsComlRecDATEDOC.AsDateTime:=iCd;
            quDocsComlRecNUMDOC.AsString:=NumDoc;
            quDocsComlRecIDSKL.AsInteger:=IdSkl;
            quDocsComlRecIACTIVE.AsInteger:=0; //1
            quDocsComlRecOPER.AsString:='';
            quDocsComlRecSUMIN.AsFloat:=0;
            quDocsComlRecSUMUCH.AsFloat:=0;
            quDocsComlRecSUMTAR.AsFloat:=0;
            quDocsComlRecPROCNAC.AsFloat:=0;
            quDocsComlRecIDSKLTO.AsInteger:=IdSkl;
            quDocsComlRecTOREAL.AsInteger:=1;
            quDocsComlRec.Post;

            //�������� ������������ ������ � �����
            fmAddCompl.prSave(IDH,Sum0,Sum1,Sum11);

            prWHAUTO('   ��������.',fmMainAutoMen.Memo1); delay(10);

            fmDocsCompl.prOn(IDH,quDocsComlRecIDSKL.AsInteger,iCd,quDocsComlRecIDSKLTO.AsInteger,Sum1,Sum11);

            quDocsComlRec.Edit;
            quDocsComlRecSUMIN.AsFloat:=RoundVal(Sum1);
            quDocsComlRecSUMUCH.AsFloat:=RoundVal(Sum11);
            quDocsComlRecIACTIVE.AsInteger:=1;
            quDocsComlRec.Post;
            quDocsComlRec.Active:=False;

            prWHAUTO('            ������� '+NumDoc+'  '+its(IDH) ,Memo1);

          end;
          quDocComplAuto.Active:=False;
        end else
          prWHAUTO(' ------������ ������',Memo1);
      end;

      prWHAUTO('     ',Memo1); delay(10);
      quMHAll.Next;
    end;
    prWHAUTO('   �������� ������������ ���������.',Memo1);

    prWHAUTO('   ',Memo1);
    prWHAUTO('   �������������� �������.',Memo1);

    prWHAUTO('      ���� ����������� �����.',fmMainAutoMen.Memo1);
    fmRecalcPer.prTestPart.ExecProc;

    quMHAll.First;
    while not quMHAll.Eof do
    begin
      prWHAUTO('    '+its(quMHAllID.AsInteger)+' '+quMHAllNAMEMH.AsString,Memo1);

      IdSkl:=quMHAllID.AsInteger;
      iSkl:=quMHAllID.AsInteger;

      for iCd:=iCurD-iDb to iCurD-iDe do
      begin
        prWHAUTO('        '+ds1(iCd),Memo1);

        if CanEdit(iCd,IdSkl) then
        begin
          prLog(11,iCd,2,IdSkl);

          with fmRecalcPer do
          begin
            prWHAUTO('           ������ ������.',fmMainAutoMen.Memo1);
            prDelPer.ParamByName('IDATE').AsInteger:=iCd;
            prDelPer.ParamByName('ISKL').AsInteger:=IdSkl;
            prDelPer.ExecProc;

            prSetSpecActive.ParamByName('DDATE').AsDateTime:=iCd;
            prSetSpecActive.ParamByName('ATYPE').AsInteger:=2;
            prSetSpecActive.ParamByName('ISKL').AsInteger:=IdSkl;
            prSetSpecActive.ExecProc;
            Delay(100);

            prWHAUTO('          ��������� ���������.',fmMainAutoMen.Memo1);

            iE:=0;

            quDIn.Active:=False;
            quDIn.ParamByName('DDATE').AsDate:=iCd;
            quDIn.ParamByName('ISKL').AsInteger:=iSkl;
            quDIn.Active:=True;
            quDIn.First;
            while not quDIn.Eof do
            begin
              quSpecInSel.Active:=False;
              quSpecInSel.ParamByName('IDHD').AsInteger:=quDInID.AsInteger;
              quSpecInSel.Active:=True;

              rSum1:=0;  rSum0:=0;  rSumIn:=0; rSum2:=0;  rSumT:=0;

              sNDS[1]:=0;sNDS[2]:=0;sNDS[3]:=0;

              quSpecInSel.First;
              while not quSpecInSel.Eof do
              begin
                if quSpecInSelCATEGORY.AsInteger=1 then rSum1:=rSum1+quSpecInSelSUMIN.AsFloat;
                if quSpecInSelCATEGORY.AsInteger=4 then rSum2:=rSum2+quSpecInSelSUMIN.AsFloat;

                //�������� �������� �������� �� ����. ���� ������� =0 �� ������� ��� ���������� ������
                rQRemn:=prCalcRemn(quSpecInSelIDCARD.AsInteger,Trunc(quDocsInSelDATEDOC.AsDateTime)-1,quDocsInSelIDSKL.AsInteger);
                if rQRemn<=0.001 then //������� ��� ������ �� ������� ������
                begin
                  quClosePartIn.ParamByName('IDCARD').AsInteger:=quSpecInSelIDCARD.AsInteger;
                  quClosePartIn.ParamByName('IDSKL').AsInteger:=quDocsInSelIDSKL.AsInteger;
                  quClosePartIn.ParamByName('IDATE').AsInteger:=Trunc(quDocsInSelDATEDOC.AsDateTime)-1;
                  quClosePartIn.ExecQuery;
                end;

                if CommonSet.RecalcNDS=1 then //������������ ���
                begin
                  quFindCard.Active:=False;
                  quFindCard.ParamByName('IDCARD').AsInteger:=quSpecInSelIDCARD.AsInteger;
                  quFindCard.Active:=True;
                  if quFindCard.RecordCount>0 then
                  begin
                    iNDS:=quFindCardINDS.AsInteger;
                    prGetNDS(INds,rProcNDS);
                    rSumIn0:=rv(quSpecInSelSUMIN.AsFloat*100/(100+rProcNDS));
                    rSumNDS:=quSpecInSelSUMIN.AsFloat-rSumIn0;
                    rPriceIn0:=rv(quSpecInSelPRICEIN.AsFloat*100/(100+rProcNDS));
                    if quSpecInSelQUANT.AsFloat<>0 then rPriceIn0:=rSumIn0/quSpecInSelQUANT.AsFloat;

                    quSpecInSel.Edit;
                    quSpecInSelPRICE0.AsFloat:=rPriceIn0;
                    quSpecInSelSUM0.AsFloat:=rSumIn0;
                    quSpecInSelNDSPROC.AsFloat:=rProcNDS;
                    quSpecInSelIDNDS.AsInteger:=iNDS;
                    quSpecInSelSUMNDS.AsFloat:=rSumNDS;
                    quSpecInSel.Post;
                  end;
                end;

                sNDS[quSpecInSelIDNDS.AsInteger]:=sNDS[quSpecInSelIDNDS.AsInteger]+quSpecInSelSUMNDS.AsFloat;

                if (quSpecInSelCATEGORY.AsInteger=1)or(quSpecInSelCATEGORY.AsInteger=2)or(quSpecInSelCATEGORY.AsInteger=3) then
                begin
                  rSum0:=rSum0+rv(quSpecInSelSUM0.AsFloat);
                  rSumIn:=rSumIn+rv(quSpecInSelSUMIN.AsFloat);
                  rSum2:=rSum2+rv(quSpecInSelSUMUCH.AsFloat);
                end;
                if (quSpecInSelCATEGORY.AsInteger=4) then
                begin
                  rSumT:=rSumT+rv(quSpecInSelSUMIN.AsFloat);
                end;

                quSpecInSel.Next;
              end;
              quSpecInSel.Active:=False;

              iSS:=prISS(quDInIDSKL.AsInteger);

              prAddPartIn.ParamByName('IDDOC').AsInteger:=quDInID.AsInteger;
              prAddPartIn.ParamByName('DTYPE').AsInteger:=1;
              prAddPartIn.ParamByName('IDSKL').AsInteger:=quDInIDSKL.AsInteger;
              prAddPartIn.ParamByName('IDCLI').AsInteger:=quDInIDCLI.AsInteger;
              prAddPartIn.ParamByName('IDATE').AsInteger:=iCd;
              prAddPartIn.ParamByName('ISS').AsInteger:=iSS;
              prAddPartIn.ExecProc;

              quDIn.Edit;
              if iSS<>2 then
              begin
//              quDInSUMIN.AsFloat:=rSum1;
//              quDInSUMTAR.AsFloat:=rSum2;
                quDInSUMIN.AsFloat:=RoundVal(rSumIn);
                quDInSUMUCH.AsFloat:=RoundVal(rSum2);
                quDInSUMTAR.AsFloat:=RoundVal(rSumT);
                quDInSUMNDS0.AsFloat:=RoundVal(sNDS[1]);
                quDInSUMNDS1.AsFloat:=RoundVal(sNDS[2]);
                quDInSUMNDS2.AsFloat:=RoundVal(sNDS[3]);
              end else
              begin
//              quDInSUMIN.AsFloat:=rSum0;
//              quDInSUMTAR.AsFloat:=rSum3;
                quDInSUMIN.AsFloat:=RoundVal(rSum0);
                quDInSUMUCH.AsFloat:=RoundVal(rSum2);
                quDInSUMTAR.AsFloat:=RoundVal(rSumT);
                quDInSUMNDS0.AsFloat:=RoundVal(sNDS[1]);
                quDInSUMNDS1.AsFloat:=RoundVal(sNDS[2]);
                quDInSUMNDS2.AsFloat:=RoundVal(sNDS[3]);
              end;
              quDInIACTIVE.AsInteger:=1;
              quDIn.Post;

              quDIn.Next; delay(10); inc(iE);
            end;
            quDIn.Active:=False;

            prWHAUTO('          ������� ��. ('+INtToStr(iE)+')',fmMainAutoMen.Memo1); delay(10);
            prWHAUTO('          ������������.',fmMainAutoMen.Memo1);

            iE:=0;
            quDCompl.Active:=False;
            quDCompl.ParamByName('DDATE').AsDate:=iCd;
            quDCompl.ParamByName('ISKL').AsINteger:=iSkl;
            quDCompl.ParamByName('ISKL1').AsINteger:=iSkl;
            quDCompl.Active:=True;
            quDCompl.First;
            while not quDCompl.Eof do
            begin
              fmDocsCompl.prOpenSpec(quDComplID.AsInteger); delay(100);
              fmAddCompl.prCalcC(iCd,quDComplIDSKL.AsInteger); delay(100);
              fmAddCompl.prCalcPr(quDComplIDSKL.AsInteger); delay(100);
              fmAddCompl.prSave(quDComplID.AsInteger,rSum0,rSum1,rSum2); delay(100);
              fmDocsCompl.prOn(quDComplID.AsInteger,quDComplIDSKL.AsInteger,iCd,quDComplIDSKLTO.AsInteger,rSum1,rSum2);

              quDCompl.Edit;
              quDComplIACTIVE.AsInteger:=1;
              quDComplSUMIN.AsFloat:=rSum1;
              quDComplSUMUCH.AsFloat:=rSum2;
              quDCompl.Post;

              quDCompl.Next; delay(10);inc(iE);
            end;
            quDCompl.Active:=False;

            prWHAUTO('          ������� ��. ('+INtToStr(iE)+')',fmMainAutoMen.Memo1); delay(10);
            prWHAUTO('          ���������� �����������.',fmMainAutoMen.Memo1);

            iE:=0;
            quDVn.Active:=False;
            quDVn.ParamByName('DDATE').AsDate:=iCd;
            quDVn.ParamByName('ISKL').AsINteger:=iSkl;
            quDVn.ParamByName('ISKL1').AsINteger:=iSkl;
            quDVn.Active:=True;
            quDVn.First;
            while not quDVn.Eof do
            begin
              fmDocsVn.prPart(quDVnID.AsInteger,quDVnIDSKL_FROM.AsInteger,quDVnIDSKL_TO.AsInteger,iCd,iSkl,rSum1,rSum2);

              quDVn.Edit;
              quDVnIACTIVE.AsInteger:=1;
              quDVnSUMIN.AsFloat:=RoundVal(rSum1);
              quDVnSUMTAR.AsFloat:=RoundVal(rSum2);
              quDVn.Post;

              quDVn.Next; delay(10);inc(iE);
            end;
            quDVn.Active:=False;

            prWHAUTO('          ������� ��. ('+INtToStr(iE)+')',fmMainAutoMen.Memo1); delay(10);
            prWHAUTO('          ���������� �� �������.',fmMainAutoMen.Memo1);

            iE:=0;
            quDR.Active:=False;
            quDR.ParamByName('DDATE').AsDate:=iCd;
            quDR.ParamByName('ISKL').AsINteger:=iSkl;
            quDR.Active:=True;
            quDR.First;
            while not quDR.Eof do
            begin
              fmDocsReal.prOn(quDRID.AsInteger,quDRIDSKL.AsInteger,iCd,quDRIDCLI.AsInteger,rSum1,rSum2);

              quDR.Edit;
              quDRIACTIVE.AsInteger:=1;
              quDRSUMIN.AsFloat:=RoundVal(rSum1);
              quDRSUMTAR.AsFloat:=RoundVal(rSum2);
              quDR.Post;

              quDR.Next; delay(10);inc(iE);
            end;
            quDR.Active:=False;

            prWHAUTO('          ������� ��. ('+INtToStr(iE)+')',fmMainAutoMen.Memo1); delay(10);
            prWHAUTO('          ���� �����������.',fmMainAutoMen.Memo1);

            iE:=0;
            quDAct.Active:=False;
            quDAct.ParamByName('DDATE').AsDate:=iCd;
            quDAct.ParamByName('ISKL').AsINteger:=iSkl;
            quDAct.Active:=True;
            quDAct.First;
            while not quDAct.Eof do
            begin
              fmDocsActs.prPriceOut(quDActID.AsInteger,quDActIDSKL.AsInteger,iCd); delay(100);//�������� ������ � ������������ ���� ������
              fmDocsActs.prPriceIn(quDActID.AsInteger); delay(100);//�������� ���� ������� � �������
              fmDocsActs.prPartIn(quDActID.AsInteger,quDActIDSKL.AsInteger,iCd,rSum1);delay(100); // ������������ ��������� ������

              rSum2:=rSum1; //����

              quDAct.Edit;
              quDActIACTIVE.AsInteger:=1;
              quDActSUMIN.AsFloat:=rSum1;
              quDActSUMUCH.AsFloat:=rSum2;
              quDAct.Post;

              quDAct.Next; delay(10);inc(iE);
            end;
            quDAct.Active:=False;

            prWHAUTO('          ������� ��. ('+INtToStr(iE)+')',fmMainAutoMen.Memo1); delay(10);
            prWHAUTO('          ������� ����������.',fmMainAutoMen.Memo1);

            iE:=0;
            quDRet.Active:=False;
            quDRet.ParamByName('DDATE').AsDate:=iCd;
            quDRet.ParamByName('ISKL').AsINteger:=iSkl;
            quDRet.Active:=True;
            quDRet.First;
            while not quDRet.Eof do
            begin
              fmDocsOut.prOn(quDRetID.AsInteger,quDRetIDSKL.AsInteger,iCd,quDRetIDCLI.AsInteger,rSum1,rSum2);

              quDRet.Edit;
              quDRetIACTIVE.AsInteger:=1;
              quDRetSUMIN.AsFloat:=RoundVal(rSum1);
              quDRetSUMTAR.AsFloat:=RoundVal(rSum2);
              quDRet.Post;

              quDRet.Next; delay(10);inc(iE);
            end;
            quDRet.Active:=False;

            prWHAUTO('          ������� ��. ('+INtToStr(iE)+')',fmMainAutoMen.Memo1); delay(10);

            prWHAUTO('          ����������.',fmMainAutoMen.Memo1);

            iE:=0;
            quDOutB.Active:=False;
            quDOutB.ParamByName('DDATE').AsDate:=iCd;
            quDOutB.ParamByName('ISKL').AsINteger:=iSkl;
            quDOutB.Active:=True;
            quDOutB.First;
            while not quDOutB.Eof do
            begin
              prWHAUTO('             ���. �'+quDOutBNUMDOC.AsString+' '+FloatToStr(quDOutBSUMUCH.AsFloat)+' ����. '+quDOutBOPER.AsString,fmMainAutoMen.Memo1);
              fmDocsOutB.prOpen(quDOutBID.AsInteger,1,Memo1);  //1 - ������ ��������, �� ��������� ������������ ������� � �������
              fmDOBSpec.prCalcQuant(quDOutBIDSKL.AsInteger,iCd,False,FindTSpis(quDOutBOPER.AsString));                                           // ��� �������� �������
              //������� �������� ����
              fmDOBSpec.prCalcPrice(quDOutBIDSKL.AsInteger,iCd);
              fmDOBSpec.prSave(quDOutBID.AsInteger,quDOutBIDSKL.AsInteger,rSum1,rSum2);
              fmDocsOutB.prOn(quDOutBID.AsInteger,quDOutBIDSKL.AsInteger,iCd,rSum2,rSum2);

              quDOutB.Edit;
              quDOutBIACTIVE.AsInteger:=1;
              quDOutBSUMUCH.AsFloat:=RoundVal(rSum1);
              quDOutBSUMIN.AsFloat:=RoundVal(rSum2);
              quDOutB.Post;

              quDOutB.Next; delay(10);inc(iE);
            end;
            quDOutB.Active:=False;

            prWHAUTO('          ������� ��. ('+INtToStr(iE)+')',fmMainAutoMen.Memo1); delay(10);
            prWHAUTO('          ��������������.',fmMainAutoMen.Memo1);

            iE:=0;
            quDInv.Active:=False;
            quDInv.ParamByName('DDATE').AsDate:=iCd;
            quDInv.ParamByName('ISKL').AsINteger:=iSkl;
            quDInv.Active:=True;
            quDInv.First;
            while not quDInv.Eof do
            begin
              prWHAUTO('             ���. �'+quDInvNUMDOC.AsString+'����� ����.'+FloatToStr(RoundVal(quDInvSUM1.AsFloat))+' ����.'+FloatToStr(RoundVal(quDInvSUM2.AsFloat)),fmMainAutoMen.Memo1);
              fmDocsInv.prCalcRemnInv(quDInvID.AsInteger,iCd,quDInvIDSKL.AsInteger); //�������� ���������� ���-��
              //����������
              fmDocsInv.prOn(quDInvId.AsInteger,iCd,quDInvIDSKL.AsInteger,rSum1,rSum2,rSum11,rSum22);

              quDInv.Edit;
              quDInvSUM1.AsFloat:=RoundVal(rSum1);
              quDInvSUM11.AsFloat:=RoundVal(rSum11);
              quDInvSUM2.AsFloat:=RoundVal(rSum2);
              quDInvSUM21.AsFloat:=RoundVal(rSum22);
              quDInvIACTIVE.AsInteger:=1;
              quDInv.Post;

              quDInv.Next; delay(10);inc(iE);
            end;
            quDInv.Active:=False;

            prWHAUTO('          ������� ��. ('+INtToStr(iE)+')',fmMainAutoMen.Memo1); delay(10);

            prSetSpecActive.ParamByName('DDATE').AsDateTime:=iCd;
            prSetSpecActive.ParamByName('ATYPE').AsInteger:=1;
            prSetSpecActive.ParamByName('ISKL').AsInteger:=IdSkl;
            prSetSpecActive.ExecProc;
            Delay(100);

          end;
        end else
          prWHAUTO(' ------������ ������',Memo1);
      end;

      prWHAUTO('     ',Memo1); delay(10);
      quMHAll.Next;
    end;
    prWHAUTO('   �������������� ������� ���������.',Memo1);
    prWHAUTO('   ',Memo1);
    prWHAUTO('   ������������ ��',Memo1);

    quMHAll.First;
    while not quMHAll.Eof do
    begin
      prWHAUTO('    '+its(quMHAllID.AsInteger)+' '+quMHAllNAMEMH.AsString,Memo1);

      IdSkl:=quMHAllID.AsInteger;

      taCloseHist.Active:=False;
      taCloseHist.ParamByName('ISKL').AsInteger:=IdSkl;
      taCloseHist.Active:=True;
      taCloseHist.First;

      for iCd:=iCurD-iDb to iCurD-iDe do
      begin
        prWHAUTO('        '+ds1(iCd),Memo1);

        if CanEdit(iCd,IdSkl) then
        begin
          prTOFindRemB(iCd,IdSkl,rSumB,rSumBT);

          Str(rSumB:12:2,StrWk);
          prWHAUTO('          ������� �� ������: '+StrWk,Memo1); Delay(10);

          //�������� �� ��������
          iR:=prTOFind(iCd,IdSkl);
          if iR<>0 then //���� ������� ���������
          begin
            //��������
            prWHAUTO('          ���� �������� �� � '+FormatDateTime('dd.mm.yyyy',iCd),Memo1); Delay(10);
            prTODel(iCd,IdSkl);
            prWHAUTO('          �������� ��.',Memo1); Delay(10);
          end;

          prLog(10,iCd,2,IdSkl); //������������

          //������
          prWHAUTO('            ������.',Memo1); Delay(10);
          rPostIn:=RoundEx(prTOPostIn(iCd,IdSkl,Memo1,rPostInT)*100)/100;

          //������
          prWHAUTO('            �������.',Memo1); Delay(10);
          rPostOut:=RoundEx(prTOPostOut(iCd,IdSkl,Memo1,rPostOutT)*100)/100;
          //�� ������
          prWHAUTO('            ��. ������.',Memo1); Delay(10);
          rVnIn:=RoundEx(prTOVnIn(iCd,IdSkl,Memo1,rVnInT)*100)/100;
          //�� ������
          prWHAUTO('            ��. ������.',Memo1); Delay(10);
          rVnOut:=RoundEx(prTOVnOut(iCd,IdSkl,Memo1,rVnOutT)*100)/100;
          //����������
          prWHAUTO('            ����������.',Memo1); Delay(10);
          rQReal:=RoundEx(prTOReal(iCd,IdSkl,Memo1,rRealTara)*100)/100;

          //��������������
          prWHAUTO('            ��������������.',Memo1); Delay(10);
          rInv:=RoundEx(prTOInv(iCd,IdSkl,rInvT,iType)*100)/100;

          quTOSel.Active:=False;
          quTOSel.ParamByName('DATEB').AsInteger:=iCd;
          quTOSel.ParamByName('DATEE').AsInteger:=iCd+1;
          quTOSel.ParamByName('IDSKL').AsInteger:=IdSkl;
          quTOSel.Active:=True;

          if (iType=2) then   //��� ���������
          begin
            quTOSel.Append;
            quTOSelIDATE.AsInteger:=iCd;
            quTOSelIDSTORE.AsInteger:=IdSkl;
            quTOSelREMNIN.AsFloat:=rSumB;
            quTOSelPOSTIN.AsFloat:=rPostIn;
            quTOSelPOSTOUT.AsFloat:=rPostOut;
            quTOSelVNIN.AsFloat:=rVnIn;
            quTOSelVNOUT.AsFloat:=rVnOut;
            quTOSelQREAL.AsFloat:=rQReal;
            if abs(rInv)>0.001 then
            begin
              quTOSelREMNOUT.AsFloat:=rInv;
              quTOSelINV.AsFloat:=rInv-(rSumB+rPostIn-rPostOut+rVnIn-rVnOut-rQReal);
            end else
            begin
              quTOSelINV.AsFloat:=0;
              quTOSelREMNOUT.AsFloat:=rSumB+rPostIn-rPostOut+rVnIn-rVnOut-rQReal;
            end;

            quTOSelREMNINT.AsFloat:=rSumBT;
            quTOSelPOSTINT.AsFloat:=rPostInT;
            quTOSelPOSTOUTT.AsFloat:=rPostOutT+rRealTara;
            quTOSelVNINT.AsFloat:=rVnInT;
            quTOSelVNOUTT.AsFloat:=rVnOutT;

            if abs(rInvT)>0.001 then
            begin
              quTOSelINVT.AsFloat:=rInvT-(rSumBT+rPostInT-rPostOutT+rVnInT-rVnOutT);
              quTOSelREMNOUTT.AsFloat:=rInvT;
            end else
            begin
              quTOSelINVT.AsFloat:=0;
              quTOSelREMNOUTT.AsFloat:=rSumBT+rPostInT-rPostOutT+rVnInT-rVnOutT;
            end;

            quTOSel.Post;
          end else
          begin
            quTOSel.Append;
            quTOSelIDATE.AsInteger:=iCd;
            quTOSelIDSTORE.AsInteger:=IdSkl;
            quTOSelREMNIN.AsFloat:=rSumB;
            quTOSelPOSTIN.AsFloat:=rPostIn;
            quTOSelPOSTOUT.AsFloat:=rPostOut;
            quTOSelVNIN.AsFloat:=rVnIn;
            quTOSelVNOUT.AsFloat:=rVnOut;
            quTOSelINV.AsFloat:=rInv;
            quTOSelQREAL.AsFloat:=rQReal;
            quTOSelREMNOUT.AsFloat:=rSumB+rPostIn-rPostOut+rVnIn-rVnOut+rInv-rQReal;

            quTOSelREMNINT.AsFloat:=rSumBT;
            quTOSelPOSTINT.AsFloat:=rPostInT;
            quTOSelPOSTOUTT.AsFloat:=rPostOutT+rRealTara;
            quTOSelVNINT.AsFloat:=rVnInT;
            quTOSelVNOUTT.AsFloat:=rVnOutT;
            quTOSelINVT.AsFloat:=rInvT;
            quTOSelREMNOUTT.AsFloat:=rSumBT+rPostInT-rPostOutT-rRealTara+rVnInT-rVnOutT+rInvT;

            quTOSel.Post;
          end;

          quTOSel.Active:=False;

          //��������� �����

          iMax:=1;

          quMaxIdCH.Active:=False;
          quMaxIdCH.Active:=True;
          if quMaxIdCH.RecordCount>0 then iMax:=quMaxIdCHMAXID.AsInteger+1;
          quMaxIdCH.Active:=False;


          taCloseHist.Append;
          taCloseHistID.AsInteger:=iMax;
          taCloseHistDATEDOC.AsDateTime:=iCd;
          taCloseHistIDP.AsInteger:=Person.Id;
          taCloseHistVALEDIT.AsDateTime:=Now;
          taCloseHistISKL.AsInteger:=IdSkl;
          taCloseHist.Post;

          taCloseHist.Refresh;

          prWHAUTO('            �������� '+its(IdSkl)+' �� '+ds1(iCd)+' ������������.',Memo1);



          prWHAUTO(' ',Memo1);

        end else
          prWHAUTO(' ------������ ������',Memo1);
      end;

      taCloseHist.Active:=False;

      prWHAUTO('     ',Memo1); delay(10);
      quMHAll.Next;
    end;

    prWHAUTO('   ������������ �� ���������',Memo1);
    prWHAUTO('   ',Memo1);

    quMHAll.Active:=False;

     //��� ��� ��������� �������������� �� ��������
    //1 - �������� �������������� �� ���� ������� �� ��������� 7 ����
    for iCd:=iCurD-8 to iCurD-1 do
    begin
      prWHAUTO('      �������� �������������� - '+ds1(iCd),Memo1);
      prRECALCGDS.ParamByName('DDATE').AsDate:=iCD;
      prRECALCGDS.ParamByName('IDATE').AsInteger:=iCD;
      prRECALCGDS.ExecProc;
      prWHAUTO('      --- ������� ��.',Memo1); delay(10);
    end;
  end;

  bPrintMemo:=True;
  prWHAUTO('������� ��������.',Memo1);
end;

procedure TfmMainAutoMen.cxButton1Click(Sender: TObject);
begin
  Timer1.Enabled:=False;
  cxCheckBox1.Checked:=False;
  cxCheckBox1.Caption:='������ ����������.';
  delay(10);
  acStart.Execute;
end;

procedure TfmMainAutoMen.FormShow(Sender: TObject);
begin
//  showmessage('Ok');
  Timer1.Enabled:=True;
end;


end.
