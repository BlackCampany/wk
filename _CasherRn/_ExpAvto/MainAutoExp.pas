unit MainAutoExp;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, cxMemo, cxCheckBox, StdCtrls,
  cxButtons, ExtCtrls, cxControls, cxContainer, cxEdit, cxTextEdit,
  cxMaskEdit, cxDropDownEdit, cxCalendar, ActnList, XPStyleActnCtrls,
  ActnMan, DB, dxmdaset, cxSpinEdit, cxGraphics, cxLookupEdit,
  cxDBLookupEdit, cxDBLookupComboBox, ADODB, FIBDataSet, pFIBDataSet;

type
  TfmMainAutoExp = class(TForm)
    cxDateEdit1: TcxDateEdit;
    Timer1: TTimer;
    cxButton1: TcxButton;
    cxCheckBox1: TcxCheckBox;
    Memo1: TcxMemo;
    amAZ: TActionManager;
    acStart: TAction;
    taCloseHist: TpFIBDataSet;
    taCloseHistID: TFIBIntegerField;
    taCloseHistDATEDOC: TFIBDateField;
    taCloseHistIDP: TFIBIntegerField;
    taCloseHistVALEDIT: TFIBDateTimeField;
    taCloseHistPNAME: TFIBStringField;
    taCloseHistISKL: TFIBIntegerField;
    taCloseHistNAMEMH: TFIBStringField;
    quMaxIdCH: TpFIBDataSet;
    quMaxIdCHMAXID: TFIBIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure acStartExecute(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmMainAutoExp: TfmMainAutoExp;
  iCliSel:INteger;

implementation

uses Un1, DMOReps, dmOffice, AddCompl, DocCompl, RecalcPer, DocsVn,
  DocOutR, ActPer, DocsOut, DocOutB, DOBSpec, DocInv, ExportFromOf;

{$R *.dfm}

procedure TfmMainAutoExp.FormCreate(Sender: TObject);
begin
  CurDir := ExtractFilePath(ParamStr(0));

  Memo1.Clear;
  cxDateEdit1.Date:=Date;
  ReadIni;
end;

procedure TfmMainAutoExp.Timer1Timer(Sender: TObject);
Var n:Integer;
begin
  Timer1.Enabled:=False;

  for n:=0 to 10 do
  begin
    cxCheckBox1.Caption:='�� ������� '+INtToStr(10-n)+' ���.';
    delay(1000); //�������� 10 ���
    if cxCheckBox1.Checked=False then Break;
  end;

  if cxCheckBox1.Checked then
  begin
    iCliSel:=0;
    acStart.Execute;
    Close;
  end;
end;

procedure TfmMainAutoExp.acStartExecute(Sender: TObject);
Var iCurD:INteger;

begin
  //�������
  prWHAUTO('������.',Memo1);
  bPrintMemo:=False;

  Person.Id:=1031; //���
  Person.Name:='Auto';
  Person.sCli:='';

  iCurD:=Trunc(cxDateEdit1.Date);
  with dmO do
  with dmORep do
  begin
    fmExport.Show;
    fmExport.cxDateEdit1.Date:=iCurD-7;
    fmExport.cxDateEdit2.Date:=iCurD-1;

    quPer.Active:=False;
    quPer.SelectSQL.Clear;
    quPer.SelectSQL.Add('select * from rpersonal');
    quPer.SelectSQL.Add('where uvolnen=1 and modul2=0');
    quPer.SelectSQL.Add('order by Name');
    quPer.Active:=True;

    quPer.First;
    while not quPer.Eof do
    begin
      Person.Id:=quPerId.AsInteger;
      Person.Name:=quPerName.AsString;
      Person.sCli:='';

      CommonSet.FtpExport:=quPerPATHEXP.AsString;
      if cxCheckBox1.Checked then CommonSet.FtpExport:=CommonSet.FtpExport+'Auto\';

      CommonSet.HostIp:=quPerHOSTFTP.AsString;
      CommonSet.HostUser:=quPerLOGINFTP.AsString;
      CommonSet.HostPassw:=quPerPASSWFTP.AsString;
      if (pos('\',CommonSet.FtpExport)>0)and(CommonSet.HostIp>'')and(CommonSet.HostUser>'') then
      begin
        prWHAUTO('     '+Person.Name+'  ������.',Memo1);

        fmExport.cxButton2.Click;
        prWHAUTO('     '+Person.Name+'  �������� �� � '+CommonSet.FtpExport,Memo1);
        delay(100);

        fmExport.cxButton3.Click;
        prWHAUTO('     '+Person.Name+'  ��',Memo1);
        prWHAUTO('     ',Memo1);
        delay(100);
      end;
      quPer.Next;
      delay(100);
    end;

    quPer.Active:=False;

    fmExport.Close;
  end;

  bPrintMemo:=True;
  prWHAUTO('������� ��������.',Memo1);
end;

procedure TfmMainAutoExp.cxButton1Click(Sender: TObject);
begin
  Timer1.Enabled:=False;
  cxCheckBox1.Checked:=False;
  cxCheckBox1.Caption:='������ ����������.';
  delay(10);
  acStart.Execute;
end;

procedure TfmMainAutoExp.FormShow(Sender: TObject);
begin
//  showmessage('Ok');
  Timer1.Enabled:=True;
end;


end.
