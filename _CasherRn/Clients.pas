unit Clients;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGrid, Placemnt, ExtCtrls, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ActnList,
  ActnMan, SpeedBar, cxContainer, cxTextEdit, cxCheckBox,
  cxImageComboBox, XPStyleActnCtrls;

type
  TfmClients = class(TForm)
    StatusBar1: TStatusBar;
    ViewCl: TcxGridDBTableView;
    LevelCl: TcxGridLevel;
    GridCl: TcxGrid;
    ViewClID: TcxGridDBColumn;
    ViewClNAMECL: TcxGridDBColumn;
    ViewClFULLNAMECL: TcxGridDBColumn;
    ViewClINN: TcxGridDBColumn;
    ViewClPHONE: TcxGridDBColumn;
    ViewClMOL: TcxGridDBColumn;
    ViewClCOMMENT: TcxGridDBColumn;
    ViewClINDS: TcxGridDBColumn;
    ViewClIACTIVE: TcxGridDBColumn;
    FormPlacement1: TFormPlacement;
    Timer1: TTimer;
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    amCli: TActionManager;
    Label1: TLabel;
    cxTextEdit1: TcxTextEdit;
    cxButton3: TcxButton;
    acExit: TAction;
    acAddCli: TAction;
    acEditCli: TAction;
    acDelCli: TAction;
    SpeedItem2: TSpeedItem;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    acDelSel: TAction;
    acActSel: TAction;
    N5: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    acReDel: TAction;
    N8: TMenuItem;
    ViewClKPP: TcxGridDBColumn;
    ViewClADDRES: TcxGridDBColumn;
    cxButton4: TcxButton;
    SpeedItem5: TSpeedItem;
    ViewClSROKPLAT: TcxGridDBColumn;
    ViewClNAMEOTP: TcxGridDBColumn;
    ViewClADROTPR: TcxGridDBColumn;
    ViewClRSCH: TcxGridDBColumn;
    ViewClKSCH: TcxGridDBColumn;
    ViewClBIK: TcxGridDBColumn;
    ViewClGLN: TcxGridDBColumn;
    acCliLic: TAction;
    SpeedItem6: TSpeedItem;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Timer1Timer(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure acAddCliExecute(Sender: TObject);
    procedure acEditCliExecute(Sender: TObject);
    procedure acDelCliExecute(Sender: TObject);
    procedure acDelSelExecute(Sender: TObject);
    procedure acActSelExecute(Sender: TObject);
    procedure acReDelExecute(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure ViewClCellDblClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxButton4Click(Sender: TObject);
    procedure ViewClKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpeedItem5Click(Sender: TObject);
    procedure acCliLicExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    Procedure prClearFmAddCli;
  end;

var
  fmClients: TfmClients;
  bClearCli:Boolean = False;

implementation

uses dmOffice, Un1, AddClient, FindResult, DMOReps, CliLic;

{$R *.dfm}


Procedure TfmClients.prClearFmAddCli;
begin
  with fmAddClients do
  begin
    cxTextEdit1.Text:='';
    cxTextEdit2.Text:='';
    cxTextEdit3.Text:='';
    cxTextEdit4.Text:='';
    cxTextEdit5.Text:='';
    cxTextEdit6.Text:='';
    cxTextEdit7.Text:='';
    cxTextEdit8.Text:='';
    cxTextEdit9.Text:='';
    cxTextEdit10.Text:='';
    cxTextEdit11.Text:='';
    cxTextEdit12.Text:='';
    cxTextEdit13.Text:='';
    cxTextEdit14.Text:='';
    cxTextEdit15.Text:='';
    cxTextEdit16.Text:='';
    cxMemo1.Clear;
    cxCheckBox1.EditValue:=1;
    cxSpinEdit1.Value:=0;
  end;
end;

procedure TfmClients.FormCreate(Sender: TObject);
begin
  GridCl.Align:=AlClient;
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  Timer1.Enabled:=True;
  ViewCl.RestoreFromIniFile(CurDir+GridIni);
  cxTextEdit1.Text:='';

end;

procedure TfmClients.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewCl.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmClients.Timer1Timer(Sender: TObject);
begin
  if bClearCli=True then begin StatusBar1.Panels[0].Text:=''; bClearCli:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearCli:=True;
end;

procedure TfmClients.acExitExecute(Sender: TObject);
begin
  close;
end;

procedure TfmClients.acAddCliExecute(Sender: TObject);
Var iId:Integer;
begin
// �������� �����������
  if not CanDo('prAddCli') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  with dmO do
  begin
    fmAddClients.Caption:='���������� �����������.';
    prClearFmAddCli;
    fmAddClients.ShowModal;
    if fmAddClients.ModalResult=mrOk then
    begin
      with fmAddClients do
      begin
        iId:=GetId('Cli');
        ViewCl.BeginUpdate;
        taClients.Append;
        taClientsID.AsInteger:=iId;
        taClientsNAMECL.AsString:=cxTextEdit1.Text;
        taClientsFULLNAMECL.AsString:=cxTextEdit2.Text;
        taClientsINN.AsString:=cxTextEdit3.Text;
        taClientsPHONE.AsString:=cxTextEdit4.Text;
        taClientsMOL.AsString:=cxTextEdit5.Text;
        taClientsCOMMENT.AsString:=cxMemo1.Text;
        taClientsINDS.AsInteger:=cxCheckBox1.EditValue;
        taClientsIACTIVE.AsInteger:=1;
        taClientsKPP.AsString:=cxTextEdit6.Text;
        taClientsKPP1.AsString:=cxTextEdit16.Text;
        taClientsADDRES.AsString:=cxTextEdit7.Text;
        taClientsSROKPLAT.AsInteger:=cxSpinEdit1.EditValue;
        taClientsNAMEOTP.AsString:=cxTextEdit8.Text;
        taClientsADROTPR.AsString:=cxTextEdit9.Text;
        taClientsRSCH.AsString:=cxTextEdit11.Text;
        taClientsKSCH.AsString:=cxTextEdit12.Text;
        taClientsBANK.AsString:=cxTextEdit10.Text;
        taClientsBIK.AsString:=cxTextEdit13.Text;
        taClientsGLN.AsString:=cxTextEdit14.Text;
        taClientsOGRN.AsString:=cxTextEdit15.Text;
        taClients.Post;
        delay(10);
        taClients.Refresh;
        ViewCl.EndUpdate;
      end;
    end;
  end;
end;

procedure TfmClients.acEditCliExecute(Sender: TObject);
begin
  //�������������
  if not CanDo('prEditCli') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  with dmO do
  begin
    if taClients.Eof then begin StatusBar1.Panels[0].Text:='�������� �����������.'; exit; end;

    fmAddClients.Caption:='�������������� ����������� - '+taClientsNAMECL.AsString;
    prClearFmAddCli;
    with fmAddClients do
    begin
      cxTextEdit1.Text:=taClientsNAMECL.AsString;
      cxTextEdit2.Text:=taClientsFULLNAMECL.AsString;
      cxTextEdit3.Text:=taClientsINN.AsString;
      cxTextEdit4.Text:=taClientsPHONE.AsString;
      cxTextEdit5.Text:=taClientsMOL.AsString;
      cxTextEdit6.Text:=taClientsKPP.AsString;
      cxTextEdit7.Text:=taClientsADDRES.AsString;
      cxSpinEdit1.EditValue:=taClientsSROKPLAT.AsInteger;

      cxTextEdit8.Text:=taClientsNAMEOTP.AsString;
      cxTextEdit9.Text:=taClientsADROTPR.AsString;
      cxTextEdit11.Text:=taClientsRSCH.AsString;
      cxTextEdit12.Text:=taClientsKSCH.AsString;
      cxTextEdit10.Text:=taClientsBANK.AsString;
      cxTextEdit13.Text:=taClientsBIK.AsString;
      cxTextEdit14.Text:=taClientsGLN.AsString;
      cxTextEdit15.Text:=taClientsOGRN.AsString;
      cxTextEdit16.Text:=taClientsKPP1.AsString;

      cxMemo1.Text:=taClientsCOMMENT.AsString;
      cxCheckBox1.EditValue:=taClientsINDS.AsInteger;
    end;
    fmAddClients.ShowModal;
    if fmAddClients.ModalResult=mrOk then
    begin
      with fmAddClients do
      begin
        ViewCl.BeginUpdate;
        taClients.Edit;
        taClientsNAMECL.AsString:=cxTextEdit1.Text;
        taClientsFULLNAMECL.AsString:=cxTextEdit2.Text;
        taClientsINN.AsString:=cxTextEdit3.Text;
        taClientsPHONE.AsString:=cxTextEdit4.Text;
        taClientsMOL.AsString:=cxTextEdit5.Text;
        taClientsCOMMENT.AsString:=cxMemo1.Text;
        taClientsINDS.AsInteger:=cxCheckBox1.EditValue;
        taClientsKPP.AsString:=cxTextEdit6.Text;
        taClientsKPP1.AsString:=cxTextEdit16.Text;
        taClientsADDRES.AsString:=cxTextEdit7.Text;
        taClientsSROKPLAT.AsInteger:=cxSpinEdit1.EditValue;
        taClientsNAMEOTP.AsString:=cxTextEdit8.Text;
        taClientsADROTPR.AsString:=cxTextEdit9.Text;
        taClientsRSCH.AsString:=cxTextEdit11.Text;
        taClientsKSCH.AsString:=cxTextEdit12.Text;
        taClientsBANK.AsString:=cxTextEdit10.Text;
        taClientsBIK.AsString:=cxTextEdit13.Text;
        taClientsGLN.AsString:=cxTextEdit14.Text;
        taClientsOGRN.AsString:=cxTextEdit15.Text;
        taClients.Post;
        delay(10);
        taClients.Refresh;
        ViewCl.EndUpdate;
      end;
    end;
  end;
end;

procedure TfmClients.acDelCliExecute(Sender: TObject);
begin
  //�������
  if not CanDo('prDelCli') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  with dmO do
  begin
    if taClients.Eof then begin StatusBar1.Panels[0].Text:='�������� �����������.'; exit; end;
    if MessageDlg('�� ������������� ������ ������� ����������� - '+taClientsNAMECL.AsString,
    mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      ViewCl.BeginUpdate;
      taClients.Edit;
      taClientsIACTIVE.AsInteger:=0;
      taClients.Post;
      delay(10);
      taClients.FullRefresh;
      ViewCl.EndUpdate;
    end;
  end;
end;

procedure TfmClients.acDelSelExecute(Sender: TObject);
begin
  if not CanDo('prViewDelCli') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  with dmO do
  begin
    ViewCl.BeginUpdate;
    taClients.Active:=False;
    taClients.SelectSQL.Clear;
    taClients.SelectSQL.Add('SELECT ID,NAMECL,FULLNAMECL,INN,PHONE,MOL,COMMENT,INDS,IACTIVE,KPP,ADDRES,SROKPLAT,NAMEOTP,ADROTPR,RSCH,KSCH,BANK,BIK,GLN,OGRN');
    taClients.SelectSQL.Add('FROM OF_CLIENTS');
    taClients.SelectSQL.Add('Order by NAMECL');
    taClients.Active:=True;
    ViewCl.EndUpdate;
  end;
end;

procedure TfmClients.acActSelExecute(Sender: TObject);
begin
  with dmO do
  begin
    ViewCl.BeginUpdate;
    taClients.Active:=False;
    taClients.SelectSQL.Clear;
    taClients.SelectSQL.Add('SELECT ID,NAMECL,FULLNAMECL,INN,PHONE,MOL,COMMENT,INDS,IACTIVE,KPP,ADDRES,SROKPLAT,NAMEOTP,ADROTPR,RSCH,KSCH,BANK,BIK,GLN,OGRN');
    taClients.SelectSQL.Add('FROM OF_CLIENTS');
    taClients.SelectSQL.Add('WHERE IACTIVE=1');
    taClients.SelectSQL.Add('Order by NAMECL');
    taClients.Active:=True;
    ViewCl.EndUpdate;
  end;
end;

procedure TfmClients.acReDelExecute(Sender: TObject);
begin
//������������
  if not CanDo('prReDelCli') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  with dmO do
  begin
    if taClients.Eof then begin StatusBar1.Panels[0].Text:='�������� �����������.'; exit; end;
    ViewCl.BeginUpdate;
    taClients.Edit;
    taClientsIACTIVE.AsInteger:=1;
    taClients.Post;
    delay(10);
    taClients.Refresh;
    ViewCl.EndUpdate;
  end;
end;

procedure TfmClients.cxButton3Click(Sender: TObject);
begin
  if cxTextEdit1.Text>'' then
  begin
    with dmO do
    begin
// ����� ��� ���������� �� �������� ���������� � ����������
      quFindCli.Active:=False;
      quFindCli.SelectSQL.Clear;
      quFindCli.SelectSQL.Add('SELECT ID,NAMECL ');
      quFindCli.SelectSQL.Add('FROM OF_CLIENTS');
      quFindCli.SelectSQL.Add('where UPPER(NAMECL) like ''%'+AnsiUpperCase(cxTextEdit1.Text)+'%''');
      quFindCli.Active:=True;

      fmFind.LevelFind.Visible:=False;
      fmFind.LevelFMenu.Visible:=False;
      fmFind.LevelFCli.Visible:=True;

      fmFind.ShowModal;
      if fmFind.ModalResult=mrOk then
      begin
        if quFindCli.RecordCount>0 then
        begin
          taClients.locate('ID',quFindCliID.AsInteger,[]);
        end;
      end;
      cxTextEdit1.Text:='';
      delay(10);
      GridCl.SetFocus;
    end;
  end;
end;

procedure TfmClients.ViewClCellDblClick(Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  ModalResult:=mrOk;
end;

procedure TfmClients.cxButton4Click(Sender: TObject);
begin
  dmO.taClients.locate('INN',cxTextEdit1.Text,[loCaseInsensitive,loPartialKey]);
end;

procedure TfmClients.ViewClKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=$0D) then
    if bSelCli then
    begin
      bSelCli:=False;
      ModalResult:=mrOk;
    end;
end;

procedure TfmClients.SpeedItem5Click(Sender: TObject);
begin
  prNExportExel5(ViewCl);
end;

procedure TfmClients.acCliLicExecute(Sender: TObject);
begin
  with dmORep do
  with dmO do
  begin
    if taClients.RecordCount>0 then
    begin
      fmCliLic.ViCliLic.BeginUpdate;
      quCliLic.Active:=False;
      quCliLic.ParamByName('ICLI').AsInteger:=taClientsID.AsInteger;
      quCliLic.Active:=True;
      fmCliLic.ViCliLic.EndUpdate;
      fmCliLic.Tag:=taClientsID.AsInteger;
      fmCliLic.ShowModal;
    end;
  end;
end;

end.
