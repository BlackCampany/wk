unit MenuDayList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SpeedBar, ExtCtrls, ComCtrls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Placemnt, cxImageComboBox, XPStyleActnCtrls, ActnList, ActnMan, Menus,
  FR_DSet, FR_DBSet, FR_Class, cxContainer, cxTextEdit, cxMemo, FIBQuery,
  pFIBQuery, pFIBStoredProc;

type
  TfmMenuDayList = class(TForm)
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    Timer1: TTimer;
    FormPlacement1: TFormPlacement;
    GridMDL: TcxGrid;
    ViewMDL: TcxGridDBTableView;
    LevelMDL: TcxGridLevel;
    amMDL: TActionManager;
    acPeriod: TAction;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem6: TSpeedItem;
    acAddDoc1: TAction;
    acEditDoc1: TAction;
    acViewDoc1: TAction;
    acDelDoc1: TAction;
    acOnDoc1: TAction;
    acOffDoc1: TAction;
    SpeedItem7: TSpeedItem;
    SpeedItem8: TSpeedItem;
    acVid: TAction;
    SpeedItem9: TSpeedItem;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    acPrint1: TAction;
    frRepDocsVn: TfrReport;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    acCopy: TAction;
    acInsertD: TAction;
    frquSpecVnSel: TfrDBDataSet;
    Memo1: TcxMemo;
    ViewMDLNAME: TcxGridDBColumn;
    ViewMDLMDATE: TcxGridDBColumn;
    ViewMDLIACTIVE: TcxGridDBColumn;
    acOpen: TAction;
    procedure SpeedItem1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acPeriodExecute(Sender: TObject);
    procedure acAddDoc1Execute(Sender: TObject);
    procedure acEditDoc1Execute(Sender: TObject);
    procedure acDelDoc1Execute(Sender: TObject);
    procedure acOnDoc1Execute(Sender: TObject);
    procedure acOffDoc1Execute(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure SpeedItem1Click0(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure acOpenExecute(Sender: TObject);
    procedure ViewMDLDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmMenuDayList: TfmMenuDayList;
  bClearDocIn:Boolean = false;

implementation

uses Un1,dmRnEdit, AddMDL, PeriodRn, MenuDay;

{$R *.dfm}

procedure TfmMenuDayList.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmMenuDayList.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  Timer1.Enabled:=True;
  GridMDL.Align:=AlClient;
  ViewMDL.RestoreFromIniFile(CurDir+GridIni);
  StatusBar1.Color:=$00FFCACA;
end;

procedure TfmMenuDayList.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewMDL.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmMenuDayList.acPeriodExecute(Sender: TObject);
begin
//  ������
  fmPeriod:=tfmPeriod.Create(Application);
  fmPeriod.DateEdit1.Date:=CommonSet.DateFrom;
  fmPeriod.DateEdit2.Date:=CommonSet.DateTo-1;
  fmPeriod.ShowModal;
  if fmPeriod.ModalResult=mrOk then
  begin
    CommonSet.DateFrom:=Trunc(fmPeriod.DateEdit1.Date);
    CommonSet.DateTo:=Trunc(fmPeriod.DateEdit2.Date)+1;

    with dmC do
    begin
      ViewMDL.BeginUpdate;
      quMenuDayList.Active:=False;
      quMenuDayList.ParamByName('DATEB').AsDateTime:=CommonSet.DateFrom;
      quMenuDayList.ParamByName('DATEE').AsDateTime:=CommonSet.DateTo;
      quMenuDayList.Active:=True;
      ViewMDL.EndUpdate;
      quMenuDayList.First;
    end;
  end;
  fmPeriod.Release;
end;

procedure TfmMenuDayList.acAddDoc1Execute(Sender: TObject);
Var IDH,IdhGr:INteger;
begin
  //�������� ��������

  if not CanDo('prAddMDL') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmC do
  begin
    IdHGr:=0;
    if quMenuDayList.RecordCount>0 then  IdHGr:=quMenuDayListID.AsInteger;

    fmAddMDL:=tfmAddMDL.Create(Application);
    fmAddMDL.cxTextEdit1.Text:='';
    fmAddMDL.cxDateEdit1.Date:=Date+1;
    fmAddMDL.ShowModal;
    if fmAddMDL.ModalResult=mrOk then
    begin
      prGetId.ParamByName('ITYPE').Value:=12;  //��� ����� � ��
      prGetId.ExecProc;
      IDH:=prGetId.ParamByName('RESULT').Value;

      quMDL_Id.Active:=False;
      quMDL_Id.ParamByName('IDH').AsInteger:=IDH;
      quMDL_Id.Active:=True;

      quMDL_Id.Append;
      quMDL_IdID.AsInteger:=IDH;
      quMDL_IdNAME.AsString:=fmAddMDL.cxTextEdit1.Text;
      quMDL_IdMDATE.AsDateTime:=Trunc(fmAddMDL.cxDateEdit1.Date);
      quMDL_IdIACTIVE.AsInteger:=0;
      quMDL_Id.Post;

      quMDL_Id.Active:=False;

      //�������� ����� ������
      if IdHGr>0 then
      begin
        quMDGroups.Active:=False;
        quMDGroups.ParamByName('IDH').AsInteger:=IdHGr;
        quMDGroups.Active:=True;

        quMDGroups.First;
        while not quMDGroups.Eof do
        begin
          quMDGID.Active:=False;
          quMDGID.ParamByName('IDH').AsInteger:=IDH;
          quMDGID.ParamByName('ID').AsInteger:=quMDGroupsID.AsInteger;
          quMDGID.Active:=True;

          while not quMDGID.Eof do quMDGID.Delete;

          quMDGID.Append;
          quMDGIDIDH.AsInteger:=IDH;
          quMDGIDID.AsInteger:=quMDGroupsID.AsInteger;
          quMDGIDNAMEGR.AsString:=quMDGroupsNAMEGR.AsString;
          quMDGIDPRIOR.AsInteger:=quMDGroupsPRIOR.AsInteger;
          quMDGIDPARENT.AsInteger:=quMDGroupsPARENT.AsInteger;
          quMDGID.Post;

          quMDGID.Active:=False;

          quMDGroups.Next;
        end;

        quMDGroups.Active:=False;
      end;


      quMenuDayList.FullRefresh;
      quMenuDayList.Locate('ID',IDH,[]);

    end;
    fmAddMDL.Release;
  end;
end;

procedure TfmMenuDayList.acEditDoc1Execute(Sender: TObject);
begin
  //�������������
  if not CanDo('prEditMDL') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmC do
  begin
    if quMenuDayList.RecordCount>0 then
    begin
      fmAddMDL:=tfmAddMDL.Create(Application);
      fmAddMDL.cxTextEdit1.Text:=quMenuDayListNAME.AsString;
      fmAddMDL.cxDateEdit1.Date:=quMenuDayListMDATE.AsDateTime;
      fmAddMDL.ShowModal;
      if fmAddMDL.ModalResult=mrOk then
      begin
        quMenuDayList.Edit;
        quMenuDayListNAME.AsString:=fmAddMDL.cxTextEdit1.Text;
        quMenuDayListMDATE.AsDateTime:=Trunc(fmAddMDL.cxDateEdit1.Date);
        quMenuDayList.Post;

        quMenuDayList.Refresh;
      end;
      fmAddMDL.Release;
    end;
  end;

end;

procedure TfmMenuDayList.acDelDoc1Execute(Sender: TObject);
begin
  //������� ��������
  if not CanDo('prDelMDL') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  with dmC do
  begin
    if MessageDlg('�� ������������� ������ ������� ������� ���� - '+quMenuDayListNAME.AsString+' �� '+quMenuDayListMDATE.AsString+'?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      quD.SQL.Clear;
      quD.SQL.Add('Delete FROM MENUDAY where IDH='+quMenuDayListID.AsString);
      quD.ExecQuery;

      quD.SQL.Clear;
      quD.SQL.Add('Delete FROM MENUDAYGR where IDH='+quMenuDayListID.AsString);
      quD.ExecQuery;

      quMenuDayList.Delete;
    end;
  end
end;

procedure TfmMenuDayList.acOnDoc1Execute(Sender: TObject);
begin
  //������� ��� �������� ����� ���� �����������
  with dmC do
  begin
    if quMenuDayList.RecordCount=0 then exit;

    if MessageDlg('������� ��������� ��� ������� ������� �������� ����?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      Memo1.Clear;
      Memo1.Lines.Add('�����... ���� ��������� �������..'); Delay(10);

      trSet.StartTransaction;
      prSetMDL.ParamByName('VAL').AsInteger:=1;
      prSetMDL.ParamByName('IDH').AsInteger:=quMenuDayListID.AsInteger;
      prSetMDL.ExecProc;
      trSet.Commit;

      quMenuDayList.Edit;
      quMenuDayListIACTIVE.AsInteger:=1;
      quMenuDayList.Post;
      quMenuDayList.Refresh;

      Memo1.Lines.Add('��������� ��.'); Delay(10);

    end;
  end;
end;

procedure TfmMenuDayList.acOffDoc1Execute(Sender: TObject);
begin
  //������� ��� �������� ����� ���� �����������
  with dmC do
  begin
    if quMenuDayList.RecordCount=0 then exit;

    if MessageDlg('������� ��� ������� ������� �������� ���� �����������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      Memo1.Lines.Add('�����... ���� ��������� �������..'); Delay(10);

      trSet.StartTransaction;
      prSetMDL.ParamByName('VAL').AsInteger:=0;
      prSetMDL.ParamByName('IDH').AsInteger:=quMenuDayListID.AsInteger;
      prSetMDL.ExecProc;
      trSet.Commit;

      quMenuDayList.Edit;
      quMenuDayListIACTIVE.AsInteger:=0;
      quMenuDayList.Post;
      quMenuDayList.Refresh;

      Memo1.Lines.Add('��������� ��.'); Delay(10);
      
    end;
  end;
end;

procedure TfmMenuDayList.Timer1Timer(Sender: TObject);
begin
  if bClearDocIn=True then begin StatusBar1.Panels[0].Text:=''; bClearDocIn:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearDocIn:=True;
end;

procedure TfmMenuDayList.SpeedItem1Click0(Sender: TObject);
begin
  Close;
end;

procedure TfmMenuDayList.FormShow(Sender: TObject);
begin
  Memo1.Clear;
end;

procedure TfmMenuDayList.acOpenExecute(Sender: TObject);
begin
  //�������
  with dmC do
  begin
    if quMenuDayList.RecordCount>0 then
    begin
      bRefresh:=False;
      fmMenuDay.ViewMenuDay.BeginUpdate;
      fmMenuDay.MenuTree.Tag:=quMenuDayListID.AsInteger;
      fmMenuDay.GridMenuDay.Tag:=Trunc(quMenuDayListMDATE.AsDateTime);
      MenuExpandLevel(nil,fmMenuDay.MenuTree,dmC.quMenuDay,quMenuDayListID.AsInteger);
      fmMenuDay.ViewMenuDay.EndUpdate;
      bRefresh:=True;

      fmMenuDay.Show;
    end;  
  end;
end;

procedure TfmMenuDayList.ViewMDLDblClick(Sender: TObject);
begin
  acOpen.Execute;
end;

end.
