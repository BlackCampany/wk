object fmPartRemn: TfmPartRemn
  Left = 347
  Top = 387
  Width = 886
  Height = 535
  Caption = #1055#1072#1088#1090#1080#1080' '#1090#1086#1074#1072#1088#1072
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 388
    Width = 878
    Height = 113
    Align = alBottom
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 0
    object Panel3: TPanel
      Left = 2
      Top = 2
      Width = 259
      Height = 109
      Align = alLeft
      Color = clWhite
      TabOrder = 0
      object cxButton1: TcxButton
        Left = 46
        Top = 32
        Width = 129
        Height = 37
        Caption = 'Ok'
        ModalResult = 1
        TabOrder = 0
        OnClick = cxButton1Click
        Colors.Default = 16776176
        Colors.Normal = 16776176
        LookAndFeel.Kind = lfOffice11
      end
    end
    object Memo1: TcxMemo
      Left = 261
      Top = 2
      Align = alClient
      Lines.Strings = (
        'Memo1')
      TabOrder = 1
      OnDblClick = Memo1DblClick
      Height = 109
      Width = 615
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 878
    Height = 57
    Align = alTop
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 1
    object Label1: TLabel
      Left = 24
      Top = 32
      Width = 82
      Height = 13
      Caption = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 24
      Top = 8
      Width = 41
      Height = 13
      Caption = #1040#1088#1090#1080#1082#1091#1083
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 120
      Top = 8
      Width = 39
      Height = 13
      Caption = 'Label4'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 120
      Top = 32
      Width = 39
      Height = 13
      Caption = 'Label5'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object GridPartInRemn: TcxGrid
    Left = 0
    Top = 57
    Width = 878
    Height = 308
    TabOrder = 2
    LookAndFeel.Kind = lfOffice11
    object ViewPartIn: TcxGridDBTableView
      PopupMenu = PopupMenu1
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dmO.dsquCPartIn1
      DataController.Summary.DefaultGroupSummaryItems = <
        item
          Format = '0.000'
          Kind = skSum
          Position = spFooter
          FieldName = 'QPART'
          Column = ViewPartInQPART
        end
        item
          Format = '0.000'
          Kind = skSum
          Position = spFooter
          FieldName = 'QREMN'
          Column = ViewPartInQREMN
        end
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'SUMIN'
          Column = ViewPartInSUMIN
        end
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'SUMREMN'
          Column = ViewPartInSUMREMN
        end>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'QPART'
          Column = ViewPartInQPART
        end
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'QREMN'
          Column = ViewPartInQREMN
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SUMIN'
          Column = ViewPartInSUMIN
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SUMREMN'
          Column = ViewPartInSUMREMN
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.MultiSelect = True
      OptionsView.Footer = True
      OptionsView.GroupFooters = gfAlwaysVisible
      OptionsView.Indicator = True
      object ViewPartInNUMDOC: TcxGridDBColumn
        Caption = #1053#1086#1084#1077#1088' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
        DataBinding.FieldName = 'NUMDOC'
      end
      object ViewPartInIDATE: TcxGridDBColumn
        Caption = #1044#1072#1090#1072
        DataBinding.FieldName = 'IDATE'
        PropertiesClassName = 'TcxDateEditProperties'
      end
      object ViewPartInDTYPE: TcxGridDBColumn
        Caption = #1058#1080#1087' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
        DataBinding.FieldName = 'DTYPE'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.Items = <
          item
            Description = #1055#1088#1080#1093#1086#1076
            ImageIndex = 0
            Value = 1
          end
          item
            Description = #1048#1085#1074#1077#1085#1090#1072#1088#1080#1079#1072#1094#1080#1103
            Value = 3
          end
          item
            Description = #1042#1085'.'#1087#1077#1088#1077#1084#1077#1097#1077#1085#1080#1077
            Value = 4
          end
          item
            Description = #1040#1082#1090' '#1087#1077#1088#1077#1088#1072#1073#1086#1090#1082#1080
            Value = 5
          end
          item
            Description = #1050#1086#1084#1087#1083#1077#1082#1090#1072#1094#1080#1103
            Value = 6
          end
          item
            Description = #1056#1077#1072#1083#1080#1079#1072#1094#1080#1103' '#1085#1072' '#1089#1090#1086#1088#1086#1085#1091
            Value = 8
          end>
      end
      object ViewPartInQPART: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086' '#1087#1072#1088#1090#1080#1080
        DataBinding.FieldName = 'QPART'
        Styles.Content = dmO.cxStyle25
      end
      object ViewPartInQREMN: TcxGridDBColumn
        Caption = #1054#1089#1090#1072#1090#1086#1082' '#1087#1072#1088#1090#1080#1080
        DataBinding.FieldName = 'QREMN'
        Styles.Content = dmO.cxStyle25
      end
      object ViewPartInPRICEIN: TcxGridDBColumn
        Caption = #1062#1077#1085#1072' '#1079#1072#1082#1091#1087'. c '#1053#1044#1057
        DataBinding.FieldName = 'PRICEIN'
        Styles.Content = dmO.cxStyle25
      end
      object ViewPartInPRICEOUT: TcxGridDBColumn
        Caption = #1062#1077#1085#1072' '#1091#1095#1077#1090#1085'.'
        DataBinding.FieldName = 'PRICEOUT'
        Visible = False
      end
      object ViewPartInSUMIN: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1079#1072#1082#1091#1087'. c '#1053#1044#1057
        DataBinding.FieldName = 'SUMIN'
        Styles.Content = dmO.cxStyle15
      end
      object ViewPartInSUMREMN: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1086#1089#1090#1072#1090#1082#1072' '#1074' '#1079#1072#1082#1091#1087'. '#1094#1077#1085#1072#1093
        DataBinding.FieldName = 'SUMREMN'
        Styles.Content = dmO.cxStyle15
      end
      object ViewPartInPRICEIN0: TcxGridDBColumn
        Caption = #1062#1077#1085#1072' '#1079#1072#1082#1091#1087'. '#1073#1077#1079' '#1053#1044#1057
        DataBinding.FieldName = 'PRICEIN0'
      end
      object ViewPartInSUMIN0: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1079#1072#1082#1091#1087'. '#1073#1077#1079' '#1053#1044#1057
        DataBinding.FieldName = 'SUMIN0'
      end
      object ViewPartInARTICUL: TcxGridDBColumn
        Caption = #1040#1088#1090#1080#1082#1091#1083
        DataBinding.FieldName = 'ARTICUL'
        Width = 47
      end
      object ViewPartInID: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1087#1072#1088#1090#1080#1080
        DataBinding.FieldName = 'ID'
        Width = 48
      end
      object ViewPartInNAMEMH: TcxGridDBColumn
        Caption = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103
        DataBinding.FieldName = 'NAMEMH'
        Width = 117
      end
      object ViewPartInIDDOC: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
        DataBinding.FieldName = 'IDDOC'
        Visible = False
      end
      object ViewPartInIDCLI: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
        DataBinding.FieldName = 'IDCLI'
        Visible = False
      end
      object ViewPartInNAMECL: TcxGridDBColumn
        Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
        DataBinding.FieldName = 'NAMECL'
        Width = 149
      end
    end
    object LevelPartIn: TcxGridLevel
      GridView = ViewPartIn
    end
  end
  object acPartAction: TActionManager
    Left = 364
    Top = 196
    StyleName = 'XP Style'
    object acCreateB: TAction
      Caption = #1057#1086#1079#1076#1072#1090#1100
      OnExecute = acCreateBExecute
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 292
    Top = 196
    object N1: TMenuItem
      Action = acCreateB
    end
  end
  object taTSpec: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 196
    Top = 248
    object taTSpecNum: TSmallintField
      FieldName = 'Num'
    end
    object taTSpecIdCard: TIntegerField
      FieldName = 'IdCard'
    end
    object taTSpecIdM: TIntegerField
      FieldName = 'IdM'
    end
    object taTSpecSM: TStringField
      FieldName = 'SM'
      Size = 10
    end
    object taTSpecKm: TFloatField
      FieldName = 'Km'
    end
    object taTSpecName: TStringField
      FieldName = 'Name'
      Size = 200
    end
    object taTSpecNetto: TFloatField
      FieldName = 'Netto'
    end
    object taTSpecBrutto: TFloatField
      FieldName = 'Brutto'
    end
    object taTSpecKnb: TFloatField
      FieldName = 'Knb'
    end
    object taTSpecTCard: TSmallintField
      FieldName = 'TCard'
    end
    object taTSpecNetto1: TFloatField
      FieldName = 'Netto1'
    end
    object taTSpecPr1: TFloatField
      FieldName = 'Pr1'
    end
    object taTSpecNetto2: TFloatField
      FieldName = 'Netto2'
    end
    object taTSpecPr2: TFloatField
      FieldName = 'Pr2'
    end
    object taTSpecIChange: TSmallintField
      FieldName = 'IChange'
    end
  end
end
