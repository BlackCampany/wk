object fmClosePerBuh: TfmClosePerBuh
  Left = 591
  Top = 111
  Width = 752
  Height = 508
  Caption = #1047#1072#1082#1088#1099#1090#1080#1077' '#1087#1077#1088#1080#1086#1076#1072' ('#1073#1091#1093#1075#1072#1083#1090#1077#1088#1089#1082#1080#1081')'
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 744
    Height = 85
    Align = alTop
    BevelInner = bvLowered
    Color = 35840
    TabOrder = 1
    object Label1: TLabel
      Left = 16
      Top = 56
      Width = 120
      Height = 13
      Caption = #1047#1072#1082#1088#1099#1090#1100' '#1087#1077#1088#1080#1086#1076' '#1087#1086' '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 16
      Top = 24
      Width = 97
      Height = 13
      Caption = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object cxButton1: TcxButton
      Left = 296
      Top = 18
      Width = 75
      Height = 25
      Caption = #1047#1072#1082#1088#1099#1090#1100
      TabOrder = 0
      OnClick = cxButton1Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 384
      Top = 18
      Width = 75
      Height = 25
      Caption = #1042#1099#1093#1086#1076
      TabOrder = 1
      OnClick = cxButton2Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxDateEdit1: TcxDateEdit
      Left = 144
      Top = 52
      Style.BorderStyle = ebsOffice11
      TabOrder = 2
      Width = 129
    end
    object cxLookupComboBox1: TcxLookupComboBox
      Left = 128
      Top = 20
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Caption = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103
          FieldName = 'NAMEMH'
        end>
      Properties.ListSource = dsquMHList
      Properties.OnChange = cxLookupComboBox1PropertiesChange
      Style.BorderStyle = ebsOffice11
      TabOrder = 3
      Width = 145
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 455
    Width = 744
    Height = 19
    Panels = <
      item
        Width = 300
      end
      item
        Width = 50
      end>
  end
  object GridClose: TcxGrid
    Left = 0
    Top = 85
    Width = 744
    Height = 370
    Align = alClient
    TabOrder = 2
    LookAndFeel.Kind = lfOffice11
    object ViewClose: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dstaCloseHistBuh
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      object ViewCloseID: TcxGridDBColumn
        Caption = #8470' '#1087'.'#1087'.'
        DataBinding.FieldName = 'ID'
        Width = 37
      end
      object ViewCloseDATEDOC: TcxGridDBColumn
        Caption = #1047#1072#1082#1088#1099#1090#1086' '#1080#1079#1084#1077#1085#1077#1085#1080#1077' '#1087#1086
        DataBinding.FieldName = 'DATEDOC'
        Width = 82
      end
      object ViewCloseIDP: TcxGridDBColumn
        Caption = #1047#1072#1082#1088#1099#1083' ('#1082#1086#1076')'
        DataBinding.FieldName = 'IDP'
      end
      object ViewClosePNAME: TcxGridDBColumn
        Caption = #1047#1072#1082#1088#1099#1083
        DataBinding.FieldName = 'PNAME'
        Width = 124
      end
      object ViewCloseVALEDIT: TcxGridDBColumn
        Caption = #1050#1086#1075#1076#1072
        DataBinding.FieldName = 'VALEDIT'
        SortIndex = 0
        SortOrder = soDescending
        Width = 125
      end
      object ViewCloseISKL: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1052#1061
        DataBinding.FieldName = 'ISKL'
      end
      object ViewCloseNAMEMH: TcxGridDBColumn
        Caption = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103
        DataBinding.FieldName = 'NAMEMH'
        Width = 216
      end
    end
    object LevelClose: TcxGridLevel
      GridView = ViewClose
    end
  end
  object RnDb: TpFIBDatabase
    DBName = '192.168.0.76:E:\_OfficeRn\DB\OFFICERN.GDB'
    DBParams.Strings = (
      'user_name=SYSDBA'
      'lc_ctype=WIN1251'
      'password=masterkey'
      'sql_role_name=SYSDBA')
    DefaultTransaction = trSelect
    DefaultUpdateTransaction = trUpdate
    SQLDialect = 3
    Timeout = 0
    SynchronizeTime = False
    DesignDBOptions = [ddoIsDefaultDatabase]
    AliasName = 'CasherRnDb'
    WaitForRestoreConnect = 0
    Left = 24
    Top = 271
  end
  object trSelect: TpFIBTransaction
    DefaultDatabase = RnDb
    TimeoutAction = TARollback
    Left = 24
    Top = 328
  end
  object trUpdate: TpFIBTransaction
    DefaultDatabase = RnDb
    TimeoutAction = TARollback
    Left = 24
    Top = 384
  end
  object quPer: TpFIBDataSet
    SelectSQL.Strings = (
      'select * from rpersonal'
      'where uvolnen=1 and modul1=0'
      'order by Name')
    Transaction = trSelect
    Database = RnDb
    UpdateTransaction = trUpdate
    Left = 96
    Top = 272
    object quPerID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quPerID_PARENT: TFIBIntegerField
      FieldName = 'ID_PARENT'
    end
    object quPerNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 200
      EmptyStrToNull = True
    end
    object quPerUVOLNEN: TFIBBooleanField
      FieldName = 'UVOLNEN'
    end
    object quPerCheck: TFIBStringField
      FieldName = 'PASSW'
      EmptyStrToNull = True
    end
    object quPerMODUL1: TFIBBooleanField
      FieldName = 'MODUL1'
    end
    object quPerMODUL2: TFIBBooleanField
      FieldName = 'MODUL2'
    end
    object quPerMODUL3: TFIBBooleanField
      FieldName = 'MODUL3'
    end
    object quPerMODUL4: TFIBBooleanField
      FieldName = 'MODUL4'
    end
    object quPerMODUL5: TFIBBooleanField
      FieldName = 'MODUL5'
    end
    object quPerMODUL6: TFIBBooleanField
      FieldName = 'MODUL6'
    end
  end
  object taPersonal: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE RPERSONAL'
      'SET '
      '    ID = :ID,'
      '    ID_PARENT = :ID_PARENT,'
      '    NAME = :NAME,'
      '    UVOLNEN = :UVOLNEN,'
      '    PASSW = :PASSW,'
      '    MODUL1 = :MODUL1,'
      '    MODUL2 = :MODUL2,'
      '    MODUL3 = :MODUL3,'
      '    MODUL4 = :MODUL4,'
      '    MODUL5 = :MODUL5,'
      '    MODUL6 = :MODUL6,'
      '    BARCODE = :BARCODE'
      'WHERE'
      '    ID = :OLD_ID'
      ''
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    RPERSONAL'
      'WHERE'
      '        ID = :OLD_ID'
      ' '
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO RPERSONAL('
      '    ID,'
      '    ID_PARENT,'
      '    NAME,'
      '    UVOLNEN,'
      '    PASSW,'
      '    MODUL1,'
      '    MODUL2,'
      '    MODUL3,'
      '    MODUL4,'
      '    MODUL5,'
      '    MODUL6,'
      '    BARCODE'
      ')'
      'VALUES('
      '    :ID,'
      '    :ID_PARENT,'
      '    :NAME,'
      '    :UVOLNEN,'
      '    :PASSW,'
      '    :MODUL1,'
      '    :MODUL2,'
      '    :MODUL3,'
      '    :MODUL4,'
      '    :MODUL5,'
      '    :MODUL6,'
      '    :BARCODE'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    ID_PARENT,'
      '    NAME,'
      '    UVOLNEN,'
      '    PASSW,'
      '    MODUL1,'
      '    MODUL2,'
      '    MODUL3,'
      '    MODUL4,'
      '    MODUL5,'
      '    MODUL6,'
      '    BARCODE'
      'FROM'
      '    RPERSONAL '
      ''
      ' WHERE '
      '        RPERSONAL.ID = :OLD_ID'
      '')
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    ID_PARENT,'
      '    NAME,'
      '    UVOLNEN,'
      '    PASSW,'
      '    MODUL1,'
      '    MODUL2,'
      '    MODUL3,'
      '    MODUL4,'
      '    MODUL5,'
      '    MODUL6,'
      '    BARCODE'
      'FROM'
      '    RPERSONAL ')
    Transaction = trSelect
    Database = RnDb
    UpdateTransaction = trUpdate
    AutoCommit = True
    Left = 144
    Top = 272
    object taPersonalID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taPersonalID_PARENT: TFIBIntegerField
      FieldName = 'ID_PARENT'
    end
    object taPersonalNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 200
      EmptyStrToNull = True
    end
    object taPersonalUVOLNEN: TFIBBooleanField
      FieldName = 'UVOLNEN'
    end
    object taPersonalPASSW: TFIBStringField
      FieldName = 'PASSW'
      EmptyStrToNull = True
    end
    object taPersonalMODUL1: TFIBBooleanField
      FieldName = 'MODUL1'
    end
    object taPersonalMODUL2: TFIBBooleanField
      FieldName = 'MODUL2'
    end
    object taPersonalMODUL3: TFIBBooleanField
      FieldName = 'MODUL3'
    end
    object taPersonalMODUL4: TFIBBooleanField
      FieldName = 'MODUL4'
    end
    object taPersonalMODUL5: TFIBBooleanField
      FieldName = 'MODUL5'
    end
    object taPersonalMODUL6: TFIBBooleanField
      FieldName = 'MODUL6'
    end
    object taPersonalBARCODE: TFIBStringField
      FieldName = 'BARCODE'
      EmptyStrToNull = True
    end
  end
  object dsPer: TDataSource
    DataSet = quPer
    Left = 96
    Top = 328
  end
  object dsPersonal: TDataSource
    DataSet = taPersonal
    Left = 144
    Top = 328
  end
  object taCloseHistBuh: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_CLOSEHISTBUH'
      'SET '
      '    DATEDOC = :DATEDOC,'
      '    IDP = :IDP,'
      '    VALEDIT = :VALEDIT,'
      '    ISKL = :ISKL'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_CLOSEHISTBUH'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_CLOSEHISTBUH('
      '    ID,'
      '    DATEDOC,'
      '    IDP,'
      '    VALEDIT,'
      '    ISKL'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATEDOC,'
      '    :IDP,'
      '    :VALEDIT,'
      '    :ISKL'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT h.ID,h.DATEDOC,h.IDP,h.VALEDIT,h.ISKL,pe.NAME as PNAME,mh' +
        '.NAMEMH'
      'FROM OF_CLOSEHISTBUH h'
      'left join rpersonal pe on h.IDP=pe.ID'
      'left join of_mh mh on h.ISKL=mh.ID '
      'where(  h.ISKL=:ISKL'
      '     ) and (     H.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT h.ID,h.DATEDOC,h.IDP,h.VALEDIT,h.ISKL,pe.NAME as PNAME,mh' +
        '.NAMEMH'
      'FROM OF_CLOSEHISTBUH h'
      'left join rpersonal pe on h.IDP=pe.ID'
      'left join of_mh mh on h.ISKL=mh.ID '
      'where h.ISKL=:ISKL'
      'order by h.ID DESC'
      '')
    Transaction = trSelect
    Database = RnDb
    UpdateTransaction = trUpdate
    AutoCommit = True
    Left = 232
    Top = 272
    object taCloseHistBuhID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taCloseHistBuhDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object taCloseHistBuhIDP: TFIBIntegerField
      FieldName = 'IDP'
    end
    object taCloseHistBuhVALEDIT: TFIBDateTimeField
      FieldName = 'VALEDIT'
    end
    object taCloseHistBuhISKL: TFIBIntegerField
      FieldName = 'ISKL'
    end
    object taCloseHistBuhPNAME: TFIBStringField
      FieldName = 'PNAME'
      Size = 200
      EmptyStrToNull = True
    end
    object taCloseHistBuhNAMEMH: TFIBStringField
      FieldName = 'NAMEMH'
      Size = 100
      EmptyStrToNull = True
    end
  end
  object dstaCloseHistBuh: TDataSource
    DataSet = taCloseHistBuh
    Left = 232
    Top = 328
  end
  object quMHList: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT MH.ID,MH.PARENT,MH.ITYPE,MH.NAMEMH,'
      '       MH.DEFPRICE,MH.ISS'
      'FROM OF_MH MH'
      'WHERE MH.ITYPE=1'
      
        'and MH.ID in (SELECT IDMH FROM RPERSONALMH where IDPERSON=:IDPER' +
        'SON)'
      'Order by MH.ID')
    Transaction = trSelect
    Database = RnDb
    UpdateTransaction = trUpdate
    Left = 328
    Top = 200
    poAskRecordCount = True
    object quMHListID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quMHListPARENT: TFIBIntegerField
      FieldName = 'PARENT'
    end
    object quMHListITYPE: TFIBIntegerField
      FieldName = 'ITYPE'
    end
    object quMHListNAMEMH: TFIBStringField
      FieldName = 'NAMEMH'
      Size = 100
      EmptyStrToNull = True
    end
    object quMHListDEFPRICE: TFIBIntegerField
      FieldName = 'DEFPRICE'
    end
    object quMHListISS: TFIBSmallIntField
      FieldName = 'ISS'
    end
  end
  object dsquMHList: TDataSource
    DataSet = quMHList
    Left = 328
    Top = 252
  end
  object quMaxIdCHBuh: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT MAX(ID) as MAXID'
      'FROM OF_CLOSEHISTBUH ')
    Transaction = trSelect
    Database = RnDb
    UpdateTransaction = trUpdate
    Left = 328
    Top = 332
    poAskRecordCount = True
    object quMaxIdCHBuhMAXID: TFIBIntegerField
      FieldName = 'MAXID'
    end
  end
  object taCloseHist: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_CLOSEHIST'
      'SET '
      '    DATEDOC = :DATEDOC,'
      '    IDP = :IDP,'
      '    VALEDIT = :VALEDIT,'
      '    ISKL = :ISKL'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_CLOSEHIST'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_CLOSEHIST('
      '    ID,'
      '    DATEDOC,'
      '    IDP,'
      '    VALEDIT,'
      '    ISKL'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATEDOC,'
      '    :IDP,'
      '    :VALEDIT,'
      '    :ISKL'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT h.ID,h.DATEDOC,h.IDP,h.VALEDIT,h.ISKL,pe.NAME as PNAME,mh' +
        '.NAMEMH'
      'FROM OF_CLOSEHIST h'
      'left join rpersonal pe on h.IDP=pe.ID'
      'left join of_mh mh on h.ISKL=mh.ID '
      'where(  h.ISKL=:ISKL'
      '     ) and (     H.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT h.ID,h.DATEDOC,h.IDP,h.VALEDIT,h.ISKL,pe.NAME as PNAME,mh' +
        '.NAMEMH'
      'FROM OF_CLOSEHIST h'
      'left join rpersonal pe on h.IDP=pe.ID'
      'left join of_mh mh on h.ISKL=mh.ID '
      'where h.ISKL=:ISKL'
      'order by h.ID DESC'
      '')
    Transaction = trSelect
    Database = RnDb
    UpdateTransaction = trUpdate
    AutoCommit = True
    Left = 464
    Top = 272
    object taCloseHistID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taCloseHistDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object taCloseHistIDP: TFIBIntegerField
      FieldName = 'IDP'
    end
    object taCloseHistVALEDIT: TFIBDateTimeField
      FieldName = 'VALEDIT'
    end
    object taCloseHistISKL: TFIBIntegerField
      FieldName = 'ISKL'
    end
    object taCloseHistPNAME: TFIBStringField
      FieldName = 'PNAME'
      Size = 200
      EmptyStrToNull = True
    end
    object taCloseHistNAMEMH: TFIBStringField
      FieldName = 'NAMEMH'
      Size = 100
      EmptyStrToNull = True
    end
  end
  object quMaxIdCH: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT MAX(ID) as MAXID'
      'FROM OF_CLOSEHIST ')
    Transaction = trSelect
    Database = RnDb
    UpdateTransaction = trUpdate
    Left = 464
    Top = 332
    poAskRecordCount = True
    object quMaxIdCHMAXID: TFIBIntegerField
      FieldName = 'MAXID'
    end
  end
end
