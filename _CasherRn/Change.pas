unit Change;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxImageComboBox, ComCtrls,
  cxSplitter, cxContainer, cxTreeView, ExtCtrls, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxControls, cxGridCustomView, cxGrid, SpeedBar, Placemnt, ActnList,
  XPStyleActnCtrls, ActnMan, Menus, cxCalendar, cxCalc;

type
  TfmChange = class(TForm)
    fpChange: TFormPlacement;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    GrChange: TcxGrid;
    ViewChange: TcxGridDBTableView;
    LevelChange: TcxGridLevel;
    Panel1: TPanel;
    ChangeTree: TcxTreeView;
    cxSplitter1: TcxSplitter;
    StatusBar1: TStatusBar;
    amChange: TActionManager;
    acAddGr: TAction;
    acEditGr: TAction;
    acDelGr: TAction;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    acExit: TAction;
    ViewChangeIDCARD: TcxGridDBColumn;
    ViewChangeKOEF: TcxGridDBColumn;
    ViewChangePRIOR: TcxGridDBColumn;
    ViewChangeIDATEB: TcxGridDBColumn;
    ViewChangeIDATEE: TcxGridDBColumn;
    ViewChangeNAME: TcxGridDBColumn;
    ViewChangeEDIZM: TcxGridDBColumn;
    acAddPOs: TAction;
    acDelPos: TAction;
    PopupMenu2: TPopupMenu;
    N4: TMenuItem;
    N5: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acAddGrExecute(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure acEditGrExecute(Sender: TObject);
    procedure ChangeTreeChange(Sender: TObject; Node: TTreeNode);
    procedure acDelGrExecute(Sender: TObject);
    procedure acAddPOsExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure acDelPosExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmChange: TfmChange;

implementation

uses Un1, dmOffice, DMOReps, AddClass, AddChangeGr, Goods;

{$R *.dfm}

procedure TfmChange.FormCreate(Sender: TObject);
Var TreeNode : TTreeNode;
begin
  GrChange.Align:=AlClient;
  fpChange.IniFileName:=CurDir+Person.Name+'\'+GridIni;
  fpChange.Active:=True;
  ViewChange.RestoreFromIniFile(CurDir+Person.Name+'\'+GridIni);

  with dmORep do
  begin
    ChangeTree.Items.BeginUpdate;
    quChangeGr.Active:=False;
    quChangeGr.Active:=True;
    quChangeGr.First;
    while not quChangeGr.Eof do
    begin
      TreeNode:=ChangeTree.Items.AddChildObject(nil, quChangeGrNAME.AsString, Pointer(quChangeGrID.AsInteger));
      TreeNode.ImageIndex:=8;
      TreeNode.SelectedIndex:=7;

      quChangeGr.Next;
    end;
    quChangeGr.Active:=False;
    ChangeTree.Items.EndUpdate;
  end;
end;

procedure TfmChange.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewChange.StoreToIniFile(CurDir+Person.Name+'\'+GridIni,False);
end;

procedure TfmChange.acAddGrExecute(Sender: TObject);
Var TreeNode : TTreeNode;
    iId:Integer;
    iFifo:INteger;
begin
  //�������� ������
  if not CanDo('prAddChangeGr') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  fmAddChangeGr.Caption:='���������� ������.';
  fmAddChangeGr.label4.Caption:='���������� ������ ������������������.';
  fmAddChangeGr.cxTextEdit1.Text:='';
  fmAddChangeGr.cxCheckBox1.Checked:=False;

  fmAddChangeGr.ShowModal;
  if fmAddChangeGr.ModalResult=mrOk then
  begin
    with dmO do
    begin
      iId:=GetId('ChangeGr');
      if fmAddChangeGr.cxCheckBox1.Checked then iFifo:=1 else iFifo:=0;

      quE.SQL.Clear;
      quE.SQL.Add('INSERT INTO OF_CHANGEGR (ID,NAME,FIFO) VALUES ('+its(iId)+','''+fmAddChangeGr.cxTextEdit1.Text+''','+its(iFifo)+')');
      quE.ExecQuery;
      delay(10);

      ChangeTree.Items.BeginUpdate;
      TreeNode:=ChangeTree.Items.AddChildObject(nil,fmAddChangeGr.cxTextEdit1.Text,Pointer(iId));
      TreeNode.ImageIndex:=8;
      TreeNode.SelectedIndex:=7;
      ChangeTree.Items.EndUpdate;

    end;
  end;
end;

procedure TfmChange.SpeedItem3Click(Sender: TObject);
begin
  prNExportExel5(ViewChange);
end;

procedure TfmChange.acExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmChange.acEditGrExecute(Sender: TObject);
Var iFifo:INteger;
begin
  //������������� ������
  if not CanDo('prEditChangeGr') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  with dmORep do
  with dmO do
  begin
    fmAddChangeGr.Caption:='�������������� ������.';
    fmAddChangeGr.label4.Caption:='�������������� ������ ������������������.';
    fmAddChangeGr.cxTextEdit1.Text:=ChangeTree.Selected.Text;
    fmAddChangeGr.cxCheckBox1.Checked:=False;

    quChangeGr.Active:=False;
    quChangeGr.Active:=True;
    if quChangeGr.Locate('ID',Integer(ChangeTree.Selected.Data),[]) then
      if quChangeGrFIFO.AsInteger=1 then fmAddChangeGr.cxCheckBox1.Checked:=True;
    quChangeGr.Active:=False;

    fmAddChangeGr.ShowModal;
    if fmAddChangeGr.ModalResult=mrOk then
    begin

      if fmAddChangeGr.cxCheckBox1.Checked then iFifo:=1 else iFifo:=0;

      quE.SQL.Clear;
      quE.SQL.Add('UPDATE OF_CHANGEGR SET NAME = '''+fmAddChangeGr.cxTextEdit1.Text+''', FIFO = '+its(iFifo)+' WHERE ID = '+its(Integer(ChangeTree.Selected.Data)));
      quE.ExecQuery;
      delay(10);

      ChangeTree.Selected.Text:=fmAddChangeGr.cxTextEdit1.Text;
    end;
  end;
end;

procedure TfmChange.ChangeTreeChange(Sender: TObject; Node: TTreeNode);
begin
  if dmO=nil then exit;
  with dmORep do
  begin
    ViewChange.BeginUpdate;
    quChange.Active:=False;
    quChange.ParamByName('IGR').AsInteger:=Integer(Node.Data);
    quChange.Active:=True;
    ViewChange.EndUpdate;
  end;
end;

procedure TfmChange.acDelGrExecute(Sender: TObject);
begin
  //�������� ������ ������������������
  if not CanDo('prDelChangeGr') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  with dmORep do
  with dmO do
  begin
    if MessageDlg('������� ������ ������������������ "'+ChangeTree.Selected.Text+'" ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      //��������� �������� � ������ ����� ����������� ����� �������


    end;
  end;
end;

procedure TfmChange.acAddPOsExecute(Sender: TObject);
begin
  //��������
  bAddToChange:=True;
  fmGoods.Show;
end;

procedure TfmChange.FormShow(Sender: TObject);
begin
  if CanDo('prEditChange') then
  begin
    acAddPOs.Enabled:=True;
    acDelPOs.Enabled:=True;
    ViewChange.OptionsData.Editing:=True;
  end else
  begin
    acAddPOs.Enabled:=False;
    acDelPOs.Enabled:=False;
    ViewChange.OptionsData.Editing:=False;
  end;
end;

procedure TfmChange.acDelPosExecute(Sender: TObject);
begin
  with dmORep do
    if quChange.RecordCount>0 then quChange.Delete;
end;

end.
