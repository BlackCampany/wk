object fmPCardsRn: TfmPCardsRn
  Left = 370
  Top = 400
  BorderStyle = bsDialog
  Caption = #1044#1077#1073#1077#1090#1086#1074#1099#1077' '#1082#1072#1088#1090#1099
  ClientHeight = 290
  ClientWidth = 427
  Color = 6832674
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 28
    Width = 53
    Height = 13
    Caption = #1050#1086#1076' '#1082#1072#1088#1090#1099
    Color = 12615808
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    Transparent = True
  end
  object Label2: TLabel
    Left = 16
    Top = 84
    Width = 37
    Height = 13
    Caption = #1041#1072#1083#1072#1085#1089
    Color = 12615808
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    Transparent = True
  end
  object Label3: TLabel
    Left = 12
    Top = 108
    Width = 88
    Height = 13
    Caption = #1044#1085#1077#1074#1085#1086#1081' '#1086#1089#1090#1072#1090#1086#1082
    Color = 12615808
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    Transparent = True
  end
  object Label4: TLabel
    Left = 12
    Top = 132
    Width = 79
    Height = 13
    Caption = #1044#1085#1077#1074#1085#1086#1081' '#1083#1080#1084#1080#1090
    Color = 12615808
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    Transparent = True
  end
  object Label5: TLabel
    Left = 16
    Top = 48
    Width = 237
    Height = 29
    AutoSize = False
    Caption = #1048#1074#1072#1085#1086#1074' '#1048#1074#1072#1085' '#1048#1074#1072#1085#1086#1074#1080#1095
    Color = 12615808
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    Transparent = True
    WordWrap = True
  end
  object Label6: TLabel
    Left = 8
    Top = 164
    Width = 51
    Height = 13
    Caption = #1053#1072#1083#1080#1095#1085#1099#1077
    Color = 12615808
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    Transparent = True
  end
  object Label7: TLabel
    Left = 8
    Top = 196
    Width = 63
    Height = 13
    Caption = #1041#1072#1085#1082'.'#1082#1072#1088#1090#1086#1081
    Color = 12615808
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    Transparent = True
  end
  object Panel2: TPanel
    Left = 261
    Top = 8
    Width = 158
    Height = 201
    BevelInner = bvRaised
    BevelOuter = bvNone
    Color = 14006946
    TabOrder = 0
    object cxButton10: TcxButton
      Left = 8
      Top = 148
      Width = 45
      Height = 45
      Caption = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 0
      TabStop = False
      OnClick = cxButton10Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfFlat
    end
    object cxButton3: TcxButton
      Left = 8
      Top = 100
      Width = 45
      Height = 45
      Caption = '1'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 1
      TabStop = False
      OnClick = cxButton3Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfFlat
    end
    object cxButton4: TcxButton
      Left = 56
      Top = 100
      Width = 45
      Height = 45
      Caption = '2'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 2
      TabStop = False
      OnClick = cxButton4Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfFlat
    end
    object cxButton5: TcxButton
      Left = 104
      Top = 100
      Width = 45
      Height = 45
      Caption = '3'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 3
      TabStop = False
      OnClick = cxButton5Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfFlat
    end
    object cxButton6: TcxButton
      Left = 8
      Top = 52
      Width = 45
      Height = 45
      Caption = '4'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 4
      TabStop = False
      OnClick = cxButton6Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfFlat
    end
    object cxButton7: TcxButton
      Left = 56
      Top = 52
      Width = 45
      Height = 45
      Caption = '5'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 5
      TabStop = False
      OnClick = cxButton7Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfFlat
    end
    object cxButton8: TcxButton
      Left = 104
      Top = 52
      Width = 45
      Height = 45
      Caption = '6'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 6
      TabStop = False
      OnClick = cxButton8Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfFlat
    end
    object cxButton9: TcxButton
      Left = 8
      Top = 4
      Width = 45
      Height = 45
      Caption = '7'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 7
      TabStop = False
      OnClick = cxButton9Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfFlat
    end
    object cxButton11: TcxButton
      Left = 56
      Top = 4
      Width = 45
      Height = 45
      Caption = '8'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 8
      TabStop = False
      OnClick = cxButton11Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfFlat
    end
    object cxButton12: TcxButton
      Left = 104
      Top = 4
      Width = 45
      Height = 45
      Caption = '9'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      TabOrder = 9
      TabStop = False
      OnClick = cxButton12Click
      Colors.Default = 12621940
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      LookAndFeel.Kind = lfFlat
    end
    object cxButton14: TcxButton
      Left = 56
      Top = 148
      Width = 93
      Height = 44
      Caption = 'Ok'
      Default = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 10
      OnClick = cxButton14Click
      Colors.Default = 10658385
      Colors.Normal = 10658385
      Colors.Pressed = 13619102
      LookAndFeel.Kind = lfFlat
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 228
    Width = 427
    Height = 62
    Align = alBottom
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 1
    object cxButton1: TcxButton
      Left = 104
      Top = 12
      Width = 209
      Height = 41
      Caption = #1047#1072#1082#1088#1099#1090#1100
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ModalResult = 1
      ParentFont = False
      TabOrder = 0
      Colors.Default = 14864576
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      Layout = blGlyphTop
      LookAndFeel.Kind = lfFlat
    end
  end
  object cxTextEdit1: TcxTextEdit
    Left = 92
    Top = 20
    Properties.EchoMode = eemPassword
    Properties.PasswordChar = '*'
    Style.LookAndFeel.Kind = lfUltraFlat
    Style.Shadow = True
    StyleDisabled.LookAndFeel.Kind = lfUltraFlat
    StyleFocused.LookAndFeel.Kind = lfUltraFlat
    StyleHot.BorderStyle = ebsUltraFlat
    StyleHot.LookAndFeel.Kind = lfUltraFlat
    TabOrder = 2
    Text = 'cxTextEdit1'
    OnEnter = cxTextEdit1Enter
    Width = 137
  end
  object cxCurrencyEdit1: TcxCurrencyEdit
    Left = 108
    Top = 80
    TabStop = False
    EditValue = 0.000000000000000000
    Properties.Alignment.Horz = taRightJustify
    Properties.ReadOnly = True
    TabOrder = 3
    Width = 121
  end
  object cxCurrencyEdit2: TcxCurrencyEdit
    Left = 108
    Top = 104
    TabStop = False
    EditValue = 0.000000000000000000
    Properties.Alignment.Horz = taRightJustify
    Properties.ReadOnly = True
    TabOrder = 4
    Width = 121
  end
  object cxCurrencyEdit3: TcxCurrencyEdit
    Left = 108
    Top = 128
    TabStop = False
    EditValue = 0.000000000000000000
    Properties.Alignment.Horz = taRightJustify
    Properties.ReadOnly = True
    TabOrder = 5
    Width = 121
  end
  object cxCurrencyEdit4: TcxCurrencyEdit
    Left = 80
    Top = 160
    EditValue = 0.000000000000000000
    ParentFont = False
    Properties.Alignment.Horz = taRightJustify
    Properties.DisplayFormat = ',0'#1088#39'.'#39';-,0'#1088#39'.'#39
    Properties.ReadOnly = False
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clGreen
    Style.Font.Height = -16
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = [fsBold]
    Style.IsFontAssigned = True
    TabOrder = 6
    OnEnter = cxCurrencyEdit4Enter
    Width = 81
  end
  object cxButton2: TcxButton
    Left = 176
    Top = 160
    Width = 73
    Height = 57
    Caption = #1042#1085#1077#1089#1090#1080
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 7
    OnClick = cxButton2Click
    Colors.Default = 14864576
    Colors.Normal = 12621940
    Colors.Pressed = 10843723
    Layout = blGlyphTop
    LookAndFeel.Kind = lfFlat
  end
  object cxCurrencyEdit5: TcxCurrencyEdit
    Left = 80
    Top = 192
    EditValue = 0.000000000000000000
    ParentFont = False
    Properties.Alignment.Horz = taRightJustify
    Properties.DisplayFormat = ',0'#1088#39'.'#39';-,0'#1088#39'.'#39
    Properties.ReadOnly = False
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clGreen
    Style.Font.Height = -16
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = [fsBold]
    Style.IsFontAssigned = True
    TabOrder = 8
    OnEnter = cxCurrencyEdit5Enter
    Width = 81
  end
end
