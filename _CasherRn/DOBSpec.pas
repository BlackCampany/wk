unit DOBSpec;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ComCtrls, Menus, cxLookAndFeelPainters, StdCtrls,
  cxButtons, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, Placemnt, cxImageComboBox, cxSplitter,
  cxContainer, cxTextEdit, cxMemo, DBClient, pFIBDataSet, ActnList,
  XPStyleActnCtrls, ActnMan, cxMaskEdit, cxDropDownEdit, cxCalendar,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxButtonEdit, cxCalc, FR_Class,
  FIBDataSet, FIBDatabase, pFIBDatabase, dxmdaset, FR_DSet, FR_DBSet;

type
  TfmDOBSpec = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Panel2: TPanel;
    cxButton1: TcxButton;
    Timer1: TTimer;
    FormPlacement1: TFormPlacement;
    Panel3: TPanel;
    Panel4: TPanel;
    cxSplitter1: TcxSplitter;
    Label1: TLabel;
    Memo1: TcxMemo;
    Label2: TLabel;
    cxButton2: TcxButton;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    amDOB: TActionManager;
    acCalcB: TAction;
    Panel5: TPanel;
    Label7: TLabel;
    cxTextEdit1: TcxTextEdit;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    cxDateEdit1: TcxDateEdit;
    cxLookupComboBox1: TcxLookupComboBox;
    Label11: TLabel;
    Label12: TLabel;
    acCalcBB: TAction;
    Label15: TLabel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    GridB: TcxGrid;
    ViewB: TcxGridDBTableView;
    ViewBSIFR: TcxGridDBColumn;
    ViewBNAMEB: TcxGridDBColumn;
    ViewBCODEB: TcxGridDBColumn;
    ViewBKB: TcxGridDBColumn;
    ViewBQUANT: TcxGridDBColumn;
    ViewBPRICER: TcxGridDBColumn;
    ViewBDSUM: TcxGridDBColumn;
    ViewBRSUM: TcxGridDBColumn;
    ViewBIDCARD: TcxGridDBColumn;
    ViewBNAME: TcxGridDBColumn;
    ViewBNAMESHORT: TcxGridDBColumn;
    ViewBTCARD: TcxGridDBColumn;
    ViewBKM: TcxGridDBColumn;
    LevelB: TcxGridLevel;
    GridC: TcxGrid;
    ViewC: TcxGridDBTableView;
    ViewCArticul: TcxGridDBColumn;
    ViewCName: TcxGridDBColumn;
    ViewCsM: TcxGridDBColumn;
    ViewCQuant: TcxGridDBColumn;
    ViewCQuantFact: TcxGridDBColumn;
    ViewCQuantDiff: TcxGridDBColumn;
    ViewCSumIn: TcxGridDBColumn;
    LevelC: TcxGridLevel;
    GridBC: TcxGrid;
    ViewBC: TcxGridDBTableView;
    ViewBCID: TcxGridDBColumn;
    ViewBCCODEB: TcxGridDBColumn;
    ViewBCNAMEB: TcxGridDBColumn;
    ViewBCQUANT: TcxGridDBColumn;
    ViewBCPRICEOUT: TcxGridDBColumn;
    ViewBCSUMOUT: TcxGridDBColumn;
    ViewBCIDCARD: TcxGridDBColumn;
    ViewBCNAMEC: TcxGridDBColumn;
    ViewBCSB: TcxGridDBColumn;
    ViewBCQUANTC: TcxGridDBColumn;
    ViewBCPRICEIN: TcxGridDBColumn;
    ViewBCSUMIN: TcxGridDBColumn;
    ViewBCIM: TcxGridDBColumn;
    ViewBCSM: TcxGridDBColumn;
    LevelBC: TcxGridLevel;
    PopupMenu1: TPopupMenu;
    Excel1: TMenuItem;
    acExpExcel1: TAction;
    acTest1: TAction;
    Label3: TLabel;
    Label13: TLabel;
    acInsPos: TAction;
    acAddList: TAction;
    acDelPos: TAction;
    acClear: TAction;
    ViewBID: TcxGridDBColumn;
    cxLookupComboBox2: TcxLookupComboBox;
    Label14: TLabel;
    cxTextEdit2: TcxTextEdit;
    CasherRnDb2: TpFIBDatabase;
    trSelCode: TpFIBTransaction;
    quFindCode: TpFIBDataSet;
    quFindCodePRICE: TFIBFloatField;
    quFindCodeCODE: TFIBStringField;
    quFindCodeCONSUMMA: TFIBFloatField;
    quFindCodeIACTIVE: TFIBSmallIntField;
    PopupMenu2: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    N2: TMenuItem;
    teCalcB: TdxMemData;
    teCalcBID: TIntegerField;
    teCalcBCODEB: TIntegerField;
    teCalcBNAMEB: TStringField;
    teCalcBQUANT: TFloatField;
    teCalcBPRICEOUT: TFloatField;
    teCalcBSUMOUT: TFloatField;
    teCalcBIDCARD: TIntegerField;
    teCalcBNAMEC: TStringField;
    teCalcBQUANTC: TFloatField;
    teCalcBPRICEIN: TFloatField;
    teCalcBSUMIN: TFloatField;
    teCalcBIM: TIntegerField;
    teCalcBSM: TStringField;
    teCalcBSB: TStringField;
    dsteCalcB: TDataSource;
    teSpecB: TdxMemData;
    teSpecBID: TIntegerField;
    teSpecBIDCARD: TIntegerField;
    teSpecBQUANT: TFloatField;
    teSpecBIDM: TIntegerField;
    teSpecBSIFR: TIntegerField;
    teSpecBNAMEB: TStringField;
    teSpecBCODEB: TStringField;
    teSpecBKB: TFloatField;
    teSpecBPRICER: TFloatField;
    teSpecBDSUM: TCurrencyField;
    teSpecBNAME: TStringField;
    teSpecBNAMESHORT: TStringField;
    teSpecBTCARD: TSmallintField;
    teSpecBKM: TFloatField;
    teCalc: TdxMemData;
    teCalcArticul: TIntegerField;
    teCalcName: TStringField;
    teCalcIdM: TIntegerField;
    teCalcsM: TStringField;
    teCalcKm: TFloatField;
    teCalcQuant: TFloatField;
    teCalcQuantFact: TFloatField;
    teCalcQuantDiff: TFloatField;
    teCalcSumUch: TFloatField;
    teCalcSumIn: TFloatField;
    teSpecBRSUM: TCurrencyField;
    dsteSpecB: TDataSource;
    dsteCalc: TDataSource;
    frRepDOB: TfrReport;
    frdsteCalcB: TfrDBDataSet;
    acMovePos: TAction;
    teSpecBSALET: TIntegerField;
    teSpecBSSALET: TStringField;
    ViewBSSALET: TcxGridDBColumn;
    PopupMenu3: TPopupMenu;
    MenuItem3: TMenuItem;
    teCalcSumIn0: TFloatField;
    ViewCSumIn0: TcxGridDBColumn;
    teCalcBPRICEIN0: TFloatField;
    teCalcBSUMIN0: TFloatField;
    ViewBCPRICEIN0: TcxGridDBColumn;
    ViewBCSUMIN0: TcxGridDBColumn;
    acDelPos1: TAction;
    acSaveB1: TAction;
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Label1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Label2Click(Sender: TObject);
    procedure Memo1DblClick(Sender: TObject);
    procedure Label4Click(Sender: TObject);
    procedure Label5Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure Label6Click(Sender: TObject);
    procedure acCalcBExecute(Sender: TObject);
    procedure ViewBCODEBPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure Label11Click(Sender: TObject);
    procedure acCalcBBExecute(Sender: TObject);
    procedure Label15Click(Sender: TObject);
    procedure ViewBCCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure acExpExcel1Execute(Sender: TObject);
    procedure acTest1Execute(Sender: TObject);
    procedure acInsPosExecute(Sender: TObject);
    procedure acAddListExecute(Sender: TObject);
    procedure acDelPosExecute(Sender: TObject);
    procedure Label12Click(Sender: TObject);
    procedure Label3Click(Sender: TObject);
    procedure acClearExecute(Sender: TObject);
    procedure Label13Click(Sender: TObject);
    procedure ViewBEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure ViewBEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure ViewBEditKeyPress(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
    procedure N3Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure teSpecBPRICERChange(Sender: TField);
    procedure teSpecBDSUMChange(Sender: TField);
    procedure teSpecBRSUMChange(Sender: TField);
    procedure acMovePosExecute(Sender: TObject);
    procedure ViewBNAMESHORTPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MenuItem3Click(Sender: TObject);
    procedure acDelPos1Execute(Sender: TObject);
    procedure acSaveB1Execute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure prCalcQuant(IdSkl,iDate:INteger;bNeedMemo:Boolean;iSpis,iOper:Integer);
    procedure prCalcPrice(IdSkl,iDate:INteger);
    procedure prSave(IDH,IdSkl:Integer;Var rSum1,rSum2:Real);
    procedure prSave1(IDH,IdSkl:Integer;Var rSum1,rSum2:Real);
    function prTestPos(MemoOut:TcxMemo):Integer;
  end;

var
  fmDOBSpec: TfmDOBSpec;
  bClearDoB:Boolean = False;
  iCol:Integer;
  bAdd:Boolean = False;

implementation

uses Un1, dmOffice, Message, SelPartIn1, DMOReps, Goods, FCards,
  MainRnOffice, SelPerSkl, CardsMove, OMessure;

{$R *.dfm}

function TfmDOBSpec.prTestPos(MemoOut:TcxMemo):Integer;
Var iErr:INteger;
    bCash:Boolean;
    iCode,iTCard:Integer;
    Km:Real;
    iM:INteger;
    sName,sM:String;

  procedure prWM(sStr:String);
  begin
    if MemoOut<>nil then
    begin
      Memo1.Lines.Add(sStr);
      delay(10);
    end;
  end;

begin
  with dmORep do
  with dmO do
  begin
    iErr:=0;
    Memo1.Clear;

    bCash:=False;

    if cxTextEdit1.Tag>0 then //��� �������������� ��������
    begin
      if quDocsOutB.Locate('ID',cxTextEdit1.Tag,[]) then //����� ��������
      begin
        if quDocsOutBIDCLI.AsInteger>0 then //��� ���������� ����������� � ���� ������ �����
        begin
          if quDB.Active=False then quDB.Active:=True else quDB.FullRefresh;
          if quDB.Locate('ID',quDocsOutBIDCLI.AsInteger,[]) then
          begin
            DBName:=quDBPATHDB.AsString;

            CasherRnDb2.Connected:=False;
            CasherRnDb2.DatabaseName:=DBName;
            CasherRnDb2.Open;
            delay(100);
            if CasherRnDb2.Connected then bCash:=True;
          end;
        end;
      end;
    end;

    prAllViewOff;

    teSpecB.First;
    while not teSpecB.Eof do
    begin
      if teSpecBIDCARD.AsInteger=0 then
      begin
        if bCash then
        begin
          quFindCode.Active:=False;
          quFindCode.ParamByName('ISIFR').AsInteger:=teSpecBSIFR.AsInteger;
          quFindCode.Active:=True;
          if quFindCode.RecordCount>0 then
          begin
            iM:=0; Km:=0; sM:=''; iTCard:=0;
            quFindCode.First;
            iCode:=StrToIntDef(quFindCodeCODE.AsString,0);
            if iCode >0 then //��� �������� � �� ��������
            begin            //���� ��������� ���� �� ����� ��������
              quFindCard.Active:=False;
              quFindCard.ParamByName('IDCARD').AsInteger:=iCode;
              quFindCard.Active:=True;
              if quFindCard.RecordCount>0 then
              begin
                iM:=quFindCardIMESSURE.AsInteger;
                sName:=quFindCardNAME.AsString;
                iTCard:=quFindCardTCARD.AsInteger;
                if iM>0 then
                begin
                  quM.Active:=False;
                  quM.ParamByName('IDM').AsInteger:=iM;
                  quM.Active:=True;
                  if quM.RecordCount>0 then
                  begin
                    KM:=quMKOEF.AsFloat;
                    sM:=quMNAMESHORT.AsString;
                  end;
                  quM.Active:=False;
                end;
              end else iCode:=0;
              quFindCard.Active:=False;
            end;

            if iCode>0 then
            begin
              teSpecB.Edit;
              teSpecBIDCARD.AsInteger:=iCode;
              teSpecBIDM.AsInteger:=iM;
              teSpecBKM.AsFloat:=kM;
              teSpecBNAME.AsString:=sName;
              teSpecBCODEB.AsString:=IntToStr(iCode);
              teSpecBNAMESHORT.AsString:=sM;
              teSpecBTCARD.AsInteger:=iTCard;
              teSpecB.Post;
            end else
            begin
              prWM('������: ��� ������������ �� �����:  '+teSpecBNAMEB.AsString+' ('+teSpecBSIFR.AsString+')');
              inc(iErr);
            end;
          end else
          begin
            prWM('������: ��� ������������ �� �����:  '+teSpecBNAMEB.AsString+' ('+teSpecBSIFR.AsString+')');
            inc(iErr);
          end;
          quFindCode.Active:=False;
        end
        else
        begin
          prWM('������: ��� ������������ �� �����:  '+teSpecBNAMEB.AsString+' ('+teSpecBSIFR.AsString+')');
          inc(iErr);
        end;
      end;
      teSpecB.Next;
    end;
    teSpecB.First;

    prAllViewOn;
  end;
  if bCash then CasherRnDb2.Connected:=False;
  if iErr>0 then prWM('�����:'+IntToStr(iErr)+' ������.')
            else prWM('��� ��. ������ ������������ ������������.');
  Result:=iErr;
end;

procedure TfmDOBSpec.prSave(IDH,IdSkl:Integer;Var rSum1,rSum2:Real);
Var Ids:Integer;
    iSS:INteger;
begin
  with dmO do
  with dmORep do
  begin
    prAllViewOff;

    iCol:=0;
    iSS:=prISS(IdSkl);

    rSum1:=0;
    teSpecB.First;
    while not teSpecB.Eof do
    begin
      rSum1:=rSum1+RoundEx(teSpecBRSUM.AsFloat*100)/100;
      teSpecB.Next;
    end;

    rSum2:=0;
    teCalc.First;
    while not teCalc.Eof do
    begin
      if iSS<>2 then rSum2:=rSum2+rv(teCalcSumIn.AsFloat)
      else rSum2:=rSum2+rv(teCalcSumIn0.AsFloat);
      teCalc.Next;
    end;

    quDobSpec.Active:=False;
    quDobSpec.ParamByName('IDH').AsInteger:=IdH;
    quDobSpec.Active:=True;

    while not quDobSpec.Eof do quDobSpec.Delete;

    teSpecB.First;
    while not teSpecB.Eof do
    begin
      quDobSpec.Append;
      quDOBSPECIDHEAD.AsInteger:=IDH;
      quDOBSPECID.AsInteger:=teSpecBID.AsInteger;
      quDOBSPECIDCARD.AsInteger:=teSpecBIDCARD.AsInteger;
      quDOBSPECQUANT.AsFloat:=teSpecBQUANT.AsFloat;
      quDOBSPECIDM.AsInteger:=teSpecBIDM.AsInteger;
      quDOBSPECSIFR.AsInteger:=teSpecBSIFR.AsInteger;
      quDOBSPECNAMEB.AsString:=teSpecBNAMEB.AsString;
      quDOBSPECCODEB.AsString:=teSpecBCODEB.AsString;
      quDOBSPECKB.AsFloat:=teSpecBKB.AsFloat;
      quDOBSPECPRICER.AsFloat:=teSpecBPRICER.AsFloat;
      quDOBSPECDSUM.AsFloat:=rv(teSpecBDSUM.AsFloat);
      quDOBSPECRSUM.AsFloat:=rv(teSpecBRSUM.AsFloat);
      quDOBSPECKM.AsFloat:=teSpecBKM.AsFloat;
      quDOBSPECSALET.AsInteger:=teSpecBSALET.AsInteger;
      quDOBSPECSSALET.AsString:=teSpecBSSALET.AsString;

      quDobSpec.Post;

      teSpecB.Next;
    end;

    taDobSpec1.Active:=False;
    taDobSpec1.ParamByName('IDH').AsInteger:=IdH;
    taDobSpec1.Active:=True;

    while not taDobSpec1.Eof do taDobSpec1.Delete;

    teCalc.First;
    while not teCalc.Eof do
    begin
      Ids:=GetId('SpecOutC');

      taDobSpec1.Append;
      taDobSpec1IDHEAD.AsInteger:=Idh;
      taDobSpec1ID.AsInteger:=Ids;
      taDobSpec1ARTICUL.AsInteger:=teCalcArticul.AsInteger;
      taDobSpec1IDM.AsInteger:=teCalcIdm.AsInteger;
      taDobSpec1SM.AsString:=teCalcsM.AsString;
      taDobSpec1KM.AsFloat:=teCalcKm.AsFloat;
      taDobSpec1QUANT.AsFloat:=teCalcQuant.AsFloat;
      taDobSpec1SUMIN.AsFloat:=rv(teCalcSumIn.AsFloat);
      taDobSpec1SUMIN0.AsFloat:=rv(teCalcSumIn0.AsFloat);
      taDobSpec1.Post;

      teCalc.Next;
    end;

    taDobSpec2.Active:=False;
    taDobSpec2.ParamByName('IDH').AsInteger:=IdH;
    taDobSpec2.Active:=True;

    while not taDobSpec2.Eof do taDobSpec2.Delete;

    IdS:=1;
    teCalcB.First;
    while not teCalcB.Eof do
    begin
      taDobSpec2.Append;
      taDobSpec2IDHEAD.AsInteger:=Idh;
      taDobSpec2IDB.AsInteger:=teCalcBID.AsInteger;
      taDobSpec2ID.AsInteger:=Ids;
      taDobSpec2CODEB.AsInteger:=teCalcBCODEB.AsInteger;
      taDobSpec2NAMEB.AsString:=teCalcBNAMEB.AsString;
      taDobSpec2QUANT.AsFloat:=teCalcBQUANT.AsFloat;
      taDobSpec2PRICEOUT.AsFloat:=rv(teCalcBPRICEOUT.AsFloat);
      taDobSpec2SUMOUT.AsFloat:=teCalcBSUMOUT.AsFloat;
      taDobSpec2IDCARD.AsInteger:=teCalcBIDCARD.AsInteger;
      taDobSpec2NAMEC.AsString:=teCalcBNAMEC.AsString;
      taDobSpec2QUANTC.AsFloat:=teCalcBQUANTC.AsFloat;
      taDobSpec2PRICEIN.AsFloat:=teCalcBPRICEIN.AsFloat;
      taDobSpec2SUMIN.AsFloat:=rv(teCalcBSUMIN.AsFloat);
      taDobSpec2IM.AsInteger:=teCalcBIM.AsInteger;
      taDobSpec2SM.AsString:=teCalcBSM.AsString;
      taDobSpec2SB.AsString:=teCalcBSB.AsString;
      taDobSpec2PRICEIN0.AsFloat:=teCalcBPRICEIN0.AsFloat;
      taDobSpec2SUMIN0.AsFloat:=rv(teCalcBSUMIN0.AsFloat);
      taDobSpec2.Post;

      inc(Ids);
      teCalcB.Next;
    end;

    quDobSpec.Active:=False;
    taDobSpec1.Active:=False;
    taDobSpec2.Active:=False;

    prAllViewOn;
  end;
end;


procedure TfmDOBSpec.prSave1(IDH,IdSkl:Integer;Var rSum1,rSum2:Real);
Var iSS:INteger;
begin
  with dmO do
  with dmORep do
  begin
    prAllViewOff;

    iCol:=0;
    iSS:=prISS(IdSkl);

    rSum1:=0;
    teSpecB.First;
    while not teSpecB.Eof do
    begin
      rSum1:=rSum1+RoundEx(teSpecBRSUM.AsFloat*100)/100;
      teSpecB.Next;
    end;

    rSum2:=0;
    teCalc.First;
    while not teCalc.Eof do
    begin
      if iSS<>2 then rSum2:=rSum2+rv(teCalcSumIn.AsFloat)
      else rSum2:=rSum2+rv(teCalcSumIn0.AsFloat);
      teCalc.Next;
    end;

    quDobSpec.Active:=False;
    quDobSpec.ParamByName('IDH').AsInteger:=IdH;
    quDobSpec.Active:=True;

    while not quDobSpec.Eof do quDobSpec.Delete;

    teSpecB.First;
    while not teSpecB.Eof do
    begin
      quDobSpec.Append;
      quDOBSPECIDHEAD.AsInteger:=IDH;
      quDOBSPECID.AsInteger:=teSpecBID.AsInteger;
      quDOBSPECIDCARD.AsInteger:=teSpecBIDCARD.AsInteger;
      quDOBSPECQUANT.AsFloat:=teSpecBQUANT.AsFloat;
      quDOBSPECIDM.AsInteger:=teSpecBIDM.AsInteger;
      quDOBSPECSIFR.AsInteger:=teSpecBSIFR.AsInteger;
      quDOBSPECNAMEB.AsString:=teSpecBNAMEB.AsString;
      quDOBSPECCODEB.AsString:=teSpecBCODEB.AsString;
      quDOBSPECKB.AsFloat:=teSpecBKB.AsFloat;
      quDOBSPECPRICER.AsFloat:=teSpecBPRICER.AsFloat;
      quDOBSPECDSUM.AsFloat:=rv(teSpecBDSUM.AsFloat);
      quDOBSPECRSUM.AsFloat:=rv(teSpecBRSUM.AsFloat);
      quDOBSPECKM.AsFloat:=teSpecBKM.AsFloat;
      quDOBSPECSALET.AsInteger:=teSpecBSALET.AsInteger;
      quDOBSPECSSALET.AsString:=teSpecBSSALET.AsString;

      quDobSpec.Post;

      teSpecB.Next;
    end;
    quDobSpec.Active:=False;

    prAllViewOn;
  end;
end;


procedure TfmDOBSpec.prCalcPrice(IdSkl,iDate:INteger);
Var PriceSp,rSumIn,rSumUch,rQ,rQs,rQp,PriceUch,rMessure,PriceSp0,rSumIn0:Real;
//    iSS:INteger;
begin
  with dmO do
  with dmORep do
  begin
    prAllViewOff;

//    iSS:=prISS(IdSkl);

    teCalc.First;
    while not teCalc.Eof do
    begin
      PriceSp:=0;
      PriceSp0:=0;
      rSumIn:=0;
      rSumIn0:=0;
      rSumUch:=0;

//      rQs:=teCalcQUANT.AsFloat;  //teCalc - � �������� �������� ���������

      //������ - teCalc � �������, � �� � �������� �� - ���� � �������� ����������
      //���������

      rQs:=teCalcQUANT.AsFloat*teCalcKM.AsFloat;

      prSelPartIn(teCalcARTICUL.AsInteger,IdSkl,0,0);

      quSelPartIn.First;
      if rQs>0 then
      begin
        while (not quSelPartIn.Eof) and (rQs>0) do
        begin
          //���� �� ���� ������� ���� �����, ��������� �������� ���
          rQp:=quSelPartInQREMN.AsFloat;
          if rQs<=rQp then  rQ:=rQs//��������� ������ ������ ���������
                            else  rQ:=rQp;
          rQs:=rQs-rQ;

          PriceSp:=quSelPartInPRICEIN.AsFloat;
          PriceSp0:=quSelPartInPRICEIN0.AsFloat;
          PriceUch:=quSelPartInPRICEOUT.AsFloat;

          rSumIn:=rSumIn+PriceSp*rQ;
          rSumIn0:=rSumIn0+PriceSp0*rQ;
          rSumUch:=rSumUch+PriceUch*rQ;

          quSelPartIn.Next;
        end;

        if rQs>0 then //�������� ������������� ������, �������� � ������, �� � ������� ��������� ������ ���, ��� � �������������
        begin
          if (PriceSp=0) or (PriceSp0=0) then
          begin //��� ���� ���������� ������� � ���������� ����������
            prCalcLastPrice1.ParamByName('IDGOOD').AsInteger:=teCalcARTICUL.AsInteger;
            prCalcLastPrice1.ParamByName('ISKL').AsInteger:=IdSkl;
            prCalcLastPrice1.ExecProc;

            PriceSp:=prCalcLastPrice1.ParamByName('PRICEIN').AsFloat;
            PriceSp0:=prCalcLastPrice1.ParamByName('PRICEIN0').AsFloat;

            rMessure:=prCalcLastPrice1.ParamByName('KOEF').AsFloat;
            if (rMessure<>0)and(rMessure<>1) then
            begin
              PriceSp:=PriceSp/rMessure;
              PriceSp0:=PriceSp0/rMessure;
            end;
          end;
          rSumIn:=rSumIn+PriceSp*rQs;
          rSumIn0:=rSumIn0+PriceSp0*rQs;
        end;
      end;
      if rQs<0 then //���� �������� , ���� ������
      begin
        if quSelPartIn.RecordCount>0 then
        begin
          PriceSp:=quSelPartInPRICEIN.AsFloat;
          PriceSp0:=quSelPartInPRICEIN0.AsFloat;
          PriceUch:=quSelPartInPRICEOUT.AsFloat;

          rSumIn:=PriceSp*rQs;
          rSumIn0:=PriceSp0*rQs;
          rSumUch:=PriceUch*rQs;
        end else
        begin //��������� ��������� ��� - ���� ���������� �������
          prCalcLastPrice1.ParamByName('IDGOOD').AsInteger:=teCalcARTICUL.AsInteger;
          prCalcLastPrice1.ParamByName('ISKL').AsInteger:=IdSkl;
          prCalcLastPrice1.ExecProc;

          PriceSp:=prCalcLastPrice1.ParamByName('PRICEIN').AsFloat;
          PriceSp0:=prCalcLastPrice1.ParamByName('PRICEIN0').AsFloat;

          rMessure:=prCalcLastPrice1.ParamByName('KOEF').AsFloat;
          if (rMessure<>0)and(rMessure<>1) then
          begin
            PriceSp:=PriceSp/rMessure;
            PriceSp0:=PriceSp0/rMessure;
          end;

          rSumIn:=PriceSp*rQs;
          rSumIn0:=PriceSp0*rQs;
        end;
      end;

      quSelPartIn.Active:=False;

      //�������� ���������
      teCalc.Edit;
      teCalcSumUch.AsFloat:=rv(rSumUch);
      teCalcSumIn.AsFloat:=rv(rSumIn);
      teCalcSumIn0.AsFloat:=rv(rSumIn0);
      teCalc.Post;

      teCalc.Next;
    end;

    teCalcB.First;
    while not teCalcB.Eof do
    begin
      if teCalc.Locate('Articul',teCalcBIDCARD.AsInteger,[]) then
      if teCalcQuant.AsFloat<>0 then
      begin
        teCalcB.Edit;
        teCalcBPRICEIN.AsFloat:=rv(teCalcSumIn.AsFloat/teCalcQuant.AsFloat);
        teCalcBSUMIN.AsFloat:=rv(teCalcSumIn.AsFloat/teCalcQuant.AsFloat*teCalcBQUANTC.AsFloat);
        teCalcBPRICEIN0.AsFloat:=rv(teCalcSumIn0.AsFloat/teCalcQuant.AsFloat);
        teCalcBSUMIN0.AsFloat:=rv(teCalcSumIn0.AsFloat/teCalcQuant.AsFloat*teCalcBQUANTC.AsFloat);
        teCalcB.Post;
      end;
      teCalcB.Next;
    end;

    prAllViewOn;
  end;
end;


procedure TfmDOBSpec.prCalcQuant(IdSkl,iDate:INteger;bNeedMemo:Boolean;iSpis,iOper:Integer);
Var rQ:Real;
//    rQRemn,rQbs,rQb:Real;
    IdPos:INteger;
begin
  with dmO do
  with dmORep do
  begin
    prAllViewOff;

    CloseTe(teCalc);
    CloseTe(teCalcB);

//    iOper:=prFindIdOper(cxLookupComboBox2.EditValue);

    teSpecB.First;
    while not teSpecB.Eof do
    begin
      IdPos:=teSpecBID.AsInteger;

      rQ:=teSpecBQUANT.AsFloat*teSpecBKB.AsFloat;

      if (teSpecBIDCARD.AsInteger>0)and(rQ<>0) then prCalcBl('    ',teSpecBNAME.AsString,IdSkl,IdPos,teSpecBIDCARD.AsInteger,iDate,teSpecBTCARD.AsInteger,rQ,teSpecBIDM.AsInteger,teSpecB,teCalc,teCalcB,Memo1,bNeedMemo,iSpis,0,iOper);

      teSpecB.Next;
//      inc(IdPos);
    end;
    teSpecB.First;

    prAllViewOn;
  end;
end;

procedure TfmDOBSpec.FormCreate(Sender: TObject);
begin
  GridB.Align:=AlClient;
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  ViewB.RestoreFromIniFile(CurDir+GridIni);
  ViewC.RestoreFromIniFile(CurDir+GridIni);
  ViewBC.RestoreFromIniFile(CurDir+GridIni);
end;

procedure TfmDOBSpec.Timer1Timer(Sender: TObject);
begin
  if bClearDoB=True then begin StatusBar1.Panels[0].Text:=''; bClearDoB:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearDoB:=True;
end;

procedure TfmDOBSpec.cxButton1Click(Sender: TObject);
begin
  close;
end;

procedure TfmDOBSpec.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if cxButton1.Enabled=True then
  begin
    if MessageDlg('�� ������������� ������ ����� �� ���������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      ViewB.StoreToIniFile(CurDir+GridIni,False);
      ViewC.StoreToIniFile(CurDir+GridIni,False);
      ViewBC.StoreToIniFile(CurDir+GridIni,False);
      with dmORep do
      begin
        CloseTe(teSpecB);
        CloseTe(teCalc);
        CloseTe(teCalcB);
      end;
      Action := caHide;
    end
    else  Action := caNone;
  end;

end;

procedure TfmDOBSpec.Label1Click(Sender: TObject);
begin
  prTestPos(Memo1);
end;

procedure TfmDOBSpec.FormShow(Sender: TObject);
begin
  Memo1.Clear;
  PageControl1.ActivePageIndex:=0;
  iCol:=0;
  ViewBNAME.Options.Editing:=False;

end;

procedure TfmDOBSpec.Label2Click(Sender: TObject);
Var iSpis,iOper:Integer;
begin
  //������ ���-�� ��� ��������
  Memo1.Clear;
  Memo1.Lines.Add('�������� ������.');
  iErr:=0;
  sErr:='';

  iSpis:=FindTSpis(cxLookupComboBox2.EditValue);
  iOper:=prFindIdOper(cxLookupComboBox2.EditValue);

  prCalcQuant(cxLookupComboBox1.EditValue,Trunc(cxDateEdit1.Date),True,iSpis,iOper);

  Memo1.Lines.Add('������ ��������.');
  Memo1.Lines.Add('');
  Memo1.Lines.Add('������ �����: '+INtToStr(iErr)+' ('+sErr+')');
end;

procedure TfmDOBSpec.Memo1DblClick(Sender: TObject);
begin
  fmMessage:=tfmMessage.Create(Application);
  fmMessage.Memo1.Lines:=Memo1.Lines;
  fmMessage.ShowModal;
  fmMessage.Release;
end;

procedure TfmDOBSpec.Label4Click(Sender: TObject);
Var rQ,rQ1:Real;
begin
  iErr:=0;
  Memo1.Clear;
  Memo1.Lines.Add('������ ��������.');
  with dmORep do
  begin
    if teCalc.Active then
    begin
      prAllViewOff;

      teCalc.First;
      while not teCalc.Eof do
      begin
        with dmO do  rQ:=prCalcRemn(teCalcArticul.AsInteger,Trunc(quDocsOutBDATEDOC.AsDateTime),quDocsOutBIDSKL.AsInteger);
        rQ1:=rQ-teCalcQuant.AsFloat;
        if (teCalcQuant.AsFloat-rQ)>0.001 then
        begin
          Memo1.Lines.Add('    ������: �� ������� ���-�� '+teCalcName.AsString+' ('+teCalcArticul.AsString+')  '+ FloatToStr(RoundEx((teCalcQuant.AsFloat-rQ)*1000)/1000)+' '+teCalcsM.AsString);
          inc(iErr);
        end;
        teCalc.Edit;
        teCalcQuantFact.AsFloat:=rQ;
        teCalcQuantDiff.AsFloat:=rQ1;
        teCalc.Post;

        teCalc.Next;
      end;
      prAllViewOn;
    end else CloseTe(teCalc);
  end;
  Memo1.Lines.Add('������ �������� ��������.');
  Memo1.Lines.Add('');
  Memo1.Lines.Add('������ �����: '+INtToStr(iErr));
end;

procedure TfmDOBSpec.Label5Click(Sender: TObject);
Var iYes,iNum:Integer;
    rQP,rQs:Real;
begin
//�������� ������
  fmPartIn1:=tfmPartIn1.Create(Application);
  with dmO do
  with dmORep do
  begin
    if quDocsOutBIDSKL.AsInteger>0 then
    begin
      iNum:=1;
      CloseTa(taPartTest);

      if teCalc.Active=False then CloseTe(teCalc);

      prAllViewOff;

      teCalc.First;
      while not teCalc.Eof do
      begin
        //��� ��� ������
    //    k:=prFindKM(teCalcIM.AsInteger);
    //    PriceSp:=teCalcPrice1.AsFloat/k;

        rQs:=teCalcQuant.AsFloat;
        rQp:=0;
        prSelPartIn(teCalcArticul.AsInteger,cxLookupComboBox1.EditValue,0,0);
        quSelPartIn.First;
        while not quSelPartIn.Eof do
        begin
          //���� �� ���� ������� ���� �����, ��������� �������� ���
          rQp:=rQp+quSelPartInQREMN.AsFloat;
          quSelPartIn.Next;
        end;
        quSelPartIn.Active:=False;

        if rQp>=rQs then iYes:=1 else iYes:=0;

        taPartTest.Append;
        taPartTestNum.AsInteger:=iNum;
        taPartTestIdGoods.AsInteger:=teCalcArticul.AsInteger;
        taPartTestNameG.AsString:=teCalcName.AsString;
        taPartTestIM.AsInteger:=teCalcIdm.AsInteger;
        taPartTestSM.AsString:=teCalcsM.AsString;
        taPartTestQuant.AsFloat:=teCalcQuant.AsFloat;
        taPartTestPrice1.AsFloat:=0;
        taPartTestiRes.AsInteger:=iYes;
        taPartTest.Post;

        inc(iNum);
        teCalc.Next;
      end;

      prAllViewOn;

      fmPartIn1.ViewPartInPrice1.Visible:=False;
      fmPartIn1.Label5.Caption:=quDocsOutBNAMEMH.AsString;

      fmPartIn1.Label6.Caption:='�� ����.';
      fmPartIn1.ShowModal;

      taPartTest.Active:=False;
      fmPartIn1.Release;
    end else showmessage('����� �������� �� ����������.');
  end;
end;

procedure TfmDOBSpec.cxButton2Click(Sender: TObject);
Var Idh:Integer;
    rSumR,rSumIn:Real;
    bErr:Boolean;
begin
  //��������� ������
  with dmO do
  with dmORep do
  begin
    Memo1.Lines.Add('����� ���� ����������.');

    ViewB.BeginUpdate;

    bErr:=False;
    teSpecB.First;
    while not teSpecB.Eof do
    begin
      if teSpecBIdCARD.AsInteger=0 then bErr:=True;
      if teSpecBQuant.AsFloat=0 then bErr:=True;
      if teSpecBName.AsString='' then bErr:=True;
      teSpecB.Next;
    end;
    if bErr then
    begin
      showmessage('������ ��������� ������������ (������� ��� ��� ������� ���-��). ���������� ����������!');
      Memo1.Lines.Add('������ ��������� ������������ (������� ��� ��� ������� ���-��). ���������� ����������!');
      ViewB.EndUpdate;
      exit;
    end;

    ViewB.EndUpdate;

    if cxTextEdit1.Tag=0 then
    begin //����������
      IDH:=GetID('DocOutB');
      cxTextEdit1.Tag:=IDH;
      if cxTextEdit1.Text=prGetNum(2,0) then prGetNum(2,1); //��������

      quDobHead.Active:=False;
      quDobHead.ParamByName('IDH').AsInteger:=IdH;
      quDobHead.Active:=True;

      while not quDobHead.Eof do quDobHead.Delete; //�� ������ ������

      quDobHead.Append;
      quDobHeadID.AsInteger:=IdH;
      quDobHeadDATEDOC.AsDateTime:=Trunc(cxDateEdit1.Date);
      quDobHeadOPER.AsString:=cxLookupComboBox2.EditValue;
      quDobHeadNUMDOC.AsString:=cxTextEdit1.Text;
      quDobHeadIDSKL.AsInteger:=cxLookupComboBox1.EditValue;
      quDobHeadSUMIN.AsFloat:=0;
      quDobHeadSUMUCH.AsFloat:=0;
      quDobHeadIACTIVE.AsInteger:=0;
      quDOBHEADCOMMENT.AsString:=cxTextEdit2.Text;
      quDobHead.Post;

      prSave(IDH,cxLookupComboBox1.EditValue,rSumR,rSumIn);

      quDobHead.Edit;
      quDobHeadSUMIN.AsFloat:=rSumIn;
      quDobHeadSUMUCH.AsFloat:=rSumR;
      quDobHead.Post;

    end else  //��������������
    begin
      IdH:=cxTextEdit1.Tag;

      prSave(IDH,cxLookupComboBox1.EditValue,rSumR,rSumIn);

      quDobHead.Active:=False;
      quDobHead.ParamByName('IDH').AsInteger:=IdH;
      quDobHead.Active:=True;

      quDobHead.First;
      if quDobHead.RecordCount>0 then
      begin
        quDobHead.Edit;
        quDobHeadDATEDOC.AsDateTime:=Trunc(cxDateEdit1.Date);
        quDobHeadOPER.AsString:=cxLookupComboBox2.EditValue;
        quDobHeadNUMDOC.AsString:=cxTextEdit1.Text;
        quDobHeadIDSKL.AsInteger:=cxLookupComboBox1.EditValue;
        quDobHeadSUMIN.AsFloat:=rSumIn;
        quDobHeadSUMUCH.AsFloat:=rSumR;
        quDOBHEADCOMMENT.AsString:=cxTextEdit2.Text;
        quDobHead.Post;
      end;
    end;


    quDocsOutB.FullRefresh;
    quDocsOutB.Locate('ID',IDH,[]);

    Memo1.Lines.Add('���������� ���������.');
  end;
//  close;
end;

procedure TfmDOBSpec.Label6Click(Sender: TObject);
begin
  Memo1.Lines.Add('�����, ���� ������.'); delay(10);
  with dmO do
  with dmORep do
  begin
    if teCalc.Active=False then
    begin
      showmessage('���������� ������� ��������� �������.');
      exit;
    end;

    prCalcPrice(cxLookupComboBox1.EditValue,Trunc(dmO.quDocsOutBDATEDOC.AsDateTime));

  end;

//  acCalcB.Execute;
  Memo1.Lines.Add('������ ��������.'); delay(10);
end;

procedure TfmDOBSpec.acCalcBExecute(Sender: TObject);
  //������ ������������� ����
Var // rQP,rQs,PriceSp,PriceUch,rQ,rSumIn,rSumUch:Real;
    IdCard,iDC:Integer;
    kM:Real;
    sM:String;
    iM,iTc,IPCount,iMain,iDate:Integer;
    QT1:TpFIBDataSet;
    rQ,rSumIn,rQ1,kBrutto,MassaB:Real;

procedure prSaveB(iCode:INteger;rQ,rQ1:Real);
begin
  with dmO do
  with dmORep do
  begin
    if teCalc.Locate('Articul',iCode,[]) then
    if teCalcQuant.AsFloat<>0 then
    begin
      teCalcB.Append;
//      teCalcBID.AsInteger:=taDobSpecID.AsInteger;
      teCalcBID.AsInteger:=iId;
      teCalcBCODEB.AsInteger:=taDobSpecIDCARD.AsInteger;
      teCalcBNAMEB.AsString:=taDobSpecNAME.AsString;
      teCalcBQUANT.AsFloat:=rQ;
      if rQ<>0 then
      begin
        teCalcBPRICEOUT.AsFloat:=taDobSpecRSUM.AsFloat/rQ;
      end else
      begin
        teCalcBPRICEOUT.AsFloat:=0;
      end;
      teCalcBSUMOUT.AsFloat:=taDobSpecRSUM.AsFloat;
      teCalcBIDCARD.AsInteger:=teCalcArticul.AsInteger;
      teCalcBNAMEC.AsString:=teCalcName.AsString;
      teCalcBQUANTC.AsFloat:=rQ1;
      if teCalcQuant.AsFloat<>0 then
      begin
        teCalcBPRICEIN.AsFloat:=teCalcSumIn.AsFloat/teCalcQuant.AsFloat;
        teCalcBSUMIN.AsFloat:=teCalcSumIn.AsFloat/teCalcQuant.AsFloat*rQ1;
      end else
      begin
        teCalcBPRICEIN.AsFloat:=0;
        teCalcBSUMIN.AsFloat:=0;
      end;
      teCalcBIM.AsInteger:=teCalcidM.AsInteger;
      teCalcBSM.AsString:=teCalcSm.AsString;
      teCalcBSB.AsString:='';
      teCalcB.Post;
    end;
  end;
end;


begin
  //������ ������������� ����
  // 1. ���������� ������� ��������� - ������� ��� ��� �������
  // 2. ���������� ������������� ��������
  // 3. ���������� ������������� ���� �� 2-��� ������

  iId:=1;
  // 2. ���������� ������������� ��������
  with dmO do
  with dmORep do
  begin
    if teCalc.Active=False then
    begin
      showmessage('���������� ������� ��������� �������.');
      exit;
    end;

    iDate:=Trunc(quDocsOutBDATEDOC.AsDateTime);

    CloseTe(teCalcB);


    ViewB.BeginUpdate;
    taDobSpec.First;
    while not taDobSpec.Eof do
    begin
      if (taDobSpecIDCARD.AsInteger>0)and(taDobSpecNAME.AsString>'') then
      begin //����� � �������� ����������
        IdCard:=taDobSpecIDCARD.AsInteger;
        rQ:=taDobSpecQUANT.AsFloat*taDobSpecKB.AsFloat;
//       rSumIn:=0;

        if taDobSpecTCARD.AsInteger=1 then
        begin //���� �� - ������ ������� ���� ������������
          iTC:=0; iPCount:=0; MassaB:=1;
          //��� ��� ��
          quFindTCard.Active:=False;
          quFindTCard.ParamByName('IDCARD').AsInteger:=IdCard;
          quFindTCard.ParamByName('IDATE').AsDateTime:=iDate;
          quFindTCard.Active:=True;
          if quFindTCard.RecordCount>0 then
          begin
            iTC:=quFindTCardID.AsInteger;
            iPCount:=quFindTCardPCOUNT.AsInteger;
            MassaB:=quFindTCardPVES.AsFloat/1000;
            if prFindMT(taDobSpecIDM.AsInteger)=1 then MassaB:=1;
          end;
          quFindTCard.Active:=False;
          if iTC>0 then
          begin  //��� �����������
            QT1:=TpFIBDataSet.Create(Owner);
            QT1.Active:=False;
            QT1.Database:=OfficeRnDb;
            QT1.Transaction:=trSel;
            QT1.SelectSQL.Clear;
            QT1.SelectSQL.Add('SELECT CS.ID,CS.IDCARD,CS.CURMESSURE,CS.NETTO,CS.BRUTTO,CS.KNB,');
            QT1.SelectSQL.Add('CD.NAME,CD.TCARD');
            QT1.SelectSQL.Add('FROM OF_CARDSTSPEC CS');
            QT1.SelectSQL.Add('left join of_cards cd on cd.ID=CS.IDCARD');
            QT1.SelectSQL.Add('where IDC='+IntToStr(IdCard)+' and IDT='+IntToStr(iTC));
            QT1.SelectSQL.Add('ORDER BY CS.ID');
            QT1.Active:=True;

            QT1.First;
            while not QT1.Eof do
            begin
        //����� �����
              rQ1:=QT1.FieldByName('NETTO').AsFloat;

             //��������� � �������� �������
              kM:=prFindKM(QT1.FieldByName('CURMESSURE').AsInteger);
              prFindSM(QT1.FieldByName('CURMESSURE').AsInteger,sM,iMain); //��� �������� �������� ������� ���������
              rQ1:=rQ1*kM;

              //���� ������ �� ������ ����
              IdC:=QT1.FieldByName('IDCARD').AsInteger;
              kBrutto:=prFindBrutto(IdC,iDate);
              rQ1:=rQ1*(100+kBrutto)/100; //��� ����� � ������
              rQ1:=rQ1/iPCount; //��� ����� �� 1-� ������ ���� �������� �� �������
              rQ1:=rQ1*rQ/MassaB; //��� ����� �� ��� ���-�� ������
              rQ1:=rQ1*rQ; //��� ����� �� ��� ���-�� ������

              if QT1.FieldByName('TCARD').AsInteger=1 then
              begin  //���� ��
                prCalcBlPrice(QT1.FieldByName('IDCARD').AsInteger,iDate,rQ1,iMain,teCalc,rSumIn);

                teCalcB.Append;
                teCalcBID.AsInteger:=iId;
                teCalcBCODEB.AsInteger:=taDobSpecIDCARD.AsInteger;
                teCalcBNAMEB.AsString:=taDobSpecNAME.AsString;
                teCalcBQUANT.AsFloat:=rQ;
                if rQ<>0 then
                begin
                  teCalcBPRICEOUT.AsFloat:=taDobSpecRSUM.AsFloat/rQ;
                  teCalcBPRICEIN.AsFloat:=rSumIn/rQ;
                  teCalcBSUMIN.AsFloat:=rSumIn;
                end else
                begin
                  teCalcBPRICEOUT.AsFloat:=0;
                  teCalcBPRICEIN.AsFloat:=0;
                  teCalcBSUMIN.AsFloat:=0;
                end;
                teCalcBSUMOUT.AsFloat:=taDobSpecRSUM.AsFloat;
                teCalcBIDCARD.AsInteger:=QT1.FieldByName('IDCARD').AsInteger;
                teCalcBNAMEC.AsString:=QT1.FieldByName('NAME').AsString;
                teCalcBQUANTC.AsFloat:=rQ1;
                teCalcBIM.AsInteger:=iMain;
                teCalcBSM.AsString:=sM;
                teCalcBSB.AsString:='�����';
                teCalcB.Post;

              end else prSaveB(QT1.FieldByName('IDCARD').AsInteger,rQ,rQ1); //��� ��
              QT1.Next;
            end;
            QT1.Active:=False;
            QT1.Free;
          end;
        end else
        begin //��� ��
          //�������� � �������� �����
          kM:=prFindKM(taDobSpecIDM.AsInteger);
          prFindSM(taDobSpecIDM.AsInteger,sM,iM); //��� �������� �������� ������� ���������
          rQ:=rQ*kM;

          prSaveB(IdCard,rQ,rQ);
        end;
      end;
      inc(iId);
      taDobSpec.Next;
    end;
    taDobSpec.First;
    ViewB.EndUpdate;

    teCalcB.First;
  end;
end;

procedure TfmDOBSpec.ViewBCODEBPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  bAddSpecB1:=True;
  if fmGoods.Visible then fmGoods.Close; delay(10);
  fmGoods.Show;
end;

procedure TfmDOBSpec.Label11Click(Sender: TObject);
begin
  acInsPos.Execute;
end;

procedure TfmDOBSpec.acCalcBBExecute(Sender: TObject);
Var StrWk:String;
    rSum:Real;
    rQ,rQ1:Real;
begin
  //������ ����� � �����
  with dmO do
  with dmORep do
  begin
    if teCalcB.Active then
    if teCalcBSB.AsString>'' then
    begin //��� �����
      Memo1.Clear;
      rQ:=teCalcBQUANT.AsFloat;
      rQ1:=0; if rQ<>0 then rQ1:=teCalcBQUANTC.AsFloat/rQ;
      StrWk:=teCalcBNAMEC.AsString;
      While Length(StrWk)<20 do StrWk:=StrWk+' ';
      Memo1.Lines.Add('    '+StrWk+' ('+teCalcBIDCARD.AsString+'). ���-��: '+FloatToStr(RoundEx(teCalcBQUANTC.AsFloat*1000)/1000)+'  ������: '+FloatToStr(RoundEx(rQ)*1000/1000)+' �� 1-� ����.: '+FloatToStr(RoundEx(rQ1*1000)/1000));
      prCalcBlPrice1('    ',teCalcBIDCARD.AsInteger,Trunc(quDocsOutBDATEDOC.AsDateTime),teCalcBQUANTC.AsFloat,teCalcBIM.AsInteger,teCalc,Memo1,rSum);
      Memo1.Lines.Add('');
      if teCalcBQUANTC.AsFloat<>0 then
        Memo1.Lines.Add('    ����� �� '+IntToStr(RoundEx(teCalcBQUANT.AsFloat))+' ������ :     '+FloatToStr(RoundEx(rSum*100)/100)+' ���.  ���� ������: '+FloatToStr(RoundEx(rSum/teCalcBQUANT.AsFloat*100)/100));
    end;
  end;
end;

procedure TfmDOBSpec.Label15Click(Sender: TObject);
begin
  with dmO do
  with dmORep do
  begin
    frRepDOB.LoadFromFile(CurDir + 'BludaSeb1.frf');

    frVariables.Variable['DocNum']:=quDocsOutBNUMDOC.AsString;
    frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',quDocsOutBDATEDOC.AsDateTime);
    frVariables.Variable['SumOut']:=quDocsOutBSUMUCH.AsFloat;
    frVariables.Variable['SumIn']:=quDocsOutBSUMIN.AsFloat;
    frVariables.Variable['DocOper']:=quDocsOutBOPER.AsString;

    frRepDOB.ReportName:='������������� ����.';
    frRepDOB.PrepareReport;
    frRepDOB.ShowPreparedReport;
  end;
end;

procedure TfmDOBSpec.ViewBCCustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);

Var i:Integer;
    sType:String;

begin
//����������
  sType:='';
  for i:=0 to ViewBC.ColumnCount-1 do
  begin
    if ViewBC.Columns[i].Name='ViewBCSB' then
    begin
      try
        sType:=AViewInfo.GridRecord.DisplayTexts[i];
        break;
      except
      end;
    end;
  end;

  if sType>''  then
  begin
    ACanvas.Canvas.Brush.Color := $00B3FF66;
    ACanvas.Canvas.Font.Color := clBlack;
  end;
end;

procedure TfmDOBSpec.acExpExcel1Execute(Sender: TObject);
begin
  prNExportExel5(ViewC);
end;

procedure TfmDOBSpec.acTest1Execute(Sender: TObject);
Var rQ:Real;
begin
  if MessageDlg('����� ��������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
    Memo1.Clear;
    Memo1.Lines.Add('������'); Delay(10);
    with dmORep do
    begin
      teCalcB.First;
      while not teCalcB.Eof do
      begin
        if teCalcBSB.AsString='' then
        begin
          if teCalc.Locate('Articul',teCalcBIDCARD.AsInteger,[]) then
          begin
            rQ:=teCalcQuant.AsFloat-teCalcBQUANTC.AsFloat;
            teCalc.Edit;
            teCalcQuant.AsFloat:=rQ;
            teCalc.Post;
            teCalcB.Delete;
          end else teCalcB.Next;
        end else teCalcB.Next;
      end;
    end;
    Memo1.Lines.Add('���������.');
  end;
end;

procedure TfmDOBSpec.acInsPosExecute(Sender: TObject);
Var iMax:Integer;
begin
  iMax:=1;
  ViewB.BeginUpdate;

  with dmORep do
  begin
    teSpecB.First;
    if not teSpecB.Eof then
    begin
      teSpecB.Last;
      iMax:=teSpecBID.AsInteger+1;
    end;

    teSpecB.Append;
    teSpecBID.AsInteger:=iMax;
    teSpecBIDCARD.AsInteger:=0;
    teSpecBQUANT.AsFloat:=0;
    teSpecBIDM.AsInteger:=0;
    teSpecBSIFR.AsInteger:=0;
    teSpecBNAMEB.AsString:='';
    teSpecBCODEB.AsString:='';
    teSpecBKB.AsFloat:=0;
    teSpecBPRICER.AsFloat:=0;
    teSpecBDSUM.AsFloat:=0;
    teSpecBRSUM.AsFloat:=0;
    teSpecBNAME.AsString:='';
    teSpecBNAMESHORT.AsString:='';
    teSpecBTCARD.AsInteger:=0;
    teSpecBKM.AsFloat:=0;
    teSpecBSALET.AsInteger:=0;
    teSpecBSSALET.AsString:='';
    teSpecB.Post;
  end;

  ViewB.EndUpdate;
  GridB.SetFocus;

  ViewBNAME.Options.Editing:=True;
  ViewBNAME.Focused:=True;
end;

procedure TfmDOBSpec.acAddListExecute(Sender: TObject);
begin
  bAddSpecB:=True;
//  if fmGoods.Visible then fmGoods.Close; delay(10);
  fmGoods.Show;
end;

procedure TfmDOBSpec.acDelPosExecute(Sender: TObject);
begin
  with dmORep do
  begin
    if teSpecB.RecordCount>0 then
    begin
      teSpecB.Delete;

      teCalc.First;
      while not teCalc.Eof do teCalc.Delete;

      teCalcB.First;
      while not teCalcB.Eof do teCalcB.Delete;
    end;
  end;
end;

procedure TfmDOBSpec.Label12Click(Sender: TObject);
begin
  acDelPos.Execute;
end;

procedure TfmDOBSpec.Label3Click(Sender: TObject);
begin
  acAddList.Execute;
end;

procedure TfmDOBSpec.acClearExecute(Sender: TObject);
begin
  with dmORep do
  begin
    teSpecB.First;
    while not teSpecB.Eof do teSpecB.Delete;

    teCalc.First;
    while not teCalc.Eof do teCalc.Delete;

    teCalcB.First;
    while not teCalcB.Eof do teCalcB.Delete;
  end;
end;

procedure TfmDOBSpec.Label13Click(Sender: TObject);
begin
  acClear.Execute;
end;

procedure TfmDOBSpec.ViewBEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  iCol:=0;
  if ViewB.Controller.FocusedColumn.Name='ViewBQUANT' then iCol:=1;
  if ViewB.Controller.FocusedColumn.Name='ViewBPRICER' then iCol:=2;
  if ViewB.Controller.FocusedColumn.Name='ViewBRSUM' then iCol:=3;
end;

procedure TfmDOBSpec.ViewBEditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
Var Km:Real;
    sName:String;
    iCode:INteger;
begin
  with dmORep do
  with dmO do
  begin
    if (Key=$0D) then
    begin
      if ViewB.Controller.FocusedColumn.Name='ViewBIDCARD' then
      begin
        iCode:=VarAsType(AEdit.EditingValue, varInteger);

        quFCard.Active:=False;
        quFCard.SelectSQL.Clear;
        quFCard.SelectSQL.Add('SELECT ID,NAME,IMESSURE,PARENT,TCARD,INDS,LASTPRICEOUT,CATEGORY');
        quFCard.SelectSQL.Add('FROM OF_CARDS');
        quFCard.SelectSQL.Add('where ID='+IntToStr(iCode));
        quFCard.SelectSQL.Add('and IACTIVE>0');
        quFCard.SelectSQL.Add('and PARENT in (SELECT ID_CLASSIF FROM RCLASSIF WHERE RIGHTS=0 AND ID_PERSONAL='+its(Person.Id)+')');
        quFCard.Active:=True;

        if quFCard.RecordCount=1 then
        begin
          iCol:=0;
          ViewB.BeginUpdate;
          teSpecB.Edit;
          teSpecBSIFR.AsInteger:=0;
          teSpecBNAMEB.AsString:='';
          teSpecBCODEB.AsString:=quFCardID.AsString;
          teSpecBKB.AsFloat:=1;
          teSpecBDSUM.AsFloat:=0;
          teSpecBRSUM.AsFloat:=0;
          teSpecBQUANT.AsFloat:=1;
          teSpecBIDCARD.AsInteger:=quFCardID.AsInteger;
          teSpecBIDM.AsInteger:=quFCardIMESSURE.AsInteger;;
          teSpecBNAME.AsString:=quFCardNAME.AsString;
          teSpecBNAMESHORT.AsString:=prFindKNM(quFCardIMESSURE.AsInteger,Km);
          teSpecBKM.AsFloat:=Km;
          teSpecBPRICER.AsFloat:=0;
          teSpecBTCard.AsInteger:=quFCardTCARD.AsInteger;
          teSpecBSALET.AsInteger:=0;
          teSpecBSSALET.AsString:='';
          teSpecB.Post;
          ViewB.EndUpdate;
          AEdit.SelectAll;
        end else
        begin
          ShowMessage('��� - '+IntToStr(iCode)+' �� ������.');
          ViewB.BeginUpdate;
          teSpecB.Edit;
          teSpecBIDCARD.AsInteger:=0;
          teSpecBIDM.AsInteger:=0;
          teSpecBNAME.AsString:='';
          teSpecBNAMESHORT.AsString:='';
          teSpecBKM.AsFloat:=0;
          teSpecBTCard.AsInteger:=0;
          teSpecBSALET.AsInteger:=0;
          teSpecBSSALET.AsString:='';
          teSpecB.Post;
          ViewB.EndUpdate;

          AEdit.SelectAll;
        end;
      end;

      if ViewB.Controller.FocusedColumn.Name='ViewBNAME' then
      begin
        sName:=VarAsType(AEdit.EditingValue, varString);

        fmFCards.ViewFCards.BeginUpdate;
        quFCard.Active:=False;
        quFCard.SelectSQL.Clear;
        quFCard.SelectSQL.Add('SELECT first 100 ID,NAME,IMESSURE,PARENT,TCARD,INDS,LASTPRICEOUT,CATEGORY');
        quFCard.SelectSQL.Add('FROM OF_CARDS');
        quFCard.SelectSQL.Add('where UPPER(NAME) like ''%'+AnsiUpperCase(sName)+'%''');
        quFCard.SelectSQL.Add('and IACTIVE>0');
        quFCard.SelectSQL.Add('and PARENT in (SELECT ID_CLASSIF FROM RCLASSIF WHERE RIGHTS=0 AND ID_PERSONAL='+its(Person.Id)+')');
        quFCard.SelectSQL.Add('Order by NAME');

        quFCard.Active:=True;

        bAdd:=True;

        quFCard.Locate('NAME',sName,[loCaseInsensitive, loPartialKey]);
        prRowFocus(fmFCards.ViewFCards);
        fmFCards.ViewFCards.EndUpdate;

        //�������� ����� ������ � ����� ������
        fmFCards.ShowModal;
        if fmFCards.ModalResult=mrOk then
        begin
          if quFCard.RecordCount>0 then
          begin
            iCol:=0;
            ViewB.BeginUpdate;
            teSpecB.Edit;
            teSpecBSIFR.AsInteger:=0;
            teSpecBNAMEB.AsString:='';
            teSpecBCODEB.AsString:=quFCardID.AsString;
            teSpecBKB.AsFloat:=1;
            teSpecBDSUM.AsFloat:=0;
            teSpecBRSUM.AsFloat:=0;
            teSpecBQUANT.AsFloat:=1;
            teSpecBIDCARD.AsInteger:=quFCardID.AsInteger;
            teSpecBIDM.AsInteger:=quFCardIMESSURE.AsInteger;;
            teSpecBNAME.AsString:=quFCardNAME.AsString;
            teSpecBNAMESHORT.AsString:=prFindKNM(quFCardIMESSURE.AsInteger,Km);
            teSpecBKM.AsFloat:=Km;
            teSpecBPRICER.AsFloat:=0;
            teSpecBTCard.AsInteger:=quFCardTCARD.AsInteger;
            teSpecBSALET.AsInteger:=0;
            teSpecBSSALET.AsString:='';
            teSpecB.Post;
            ViewB.EndUpdate;
            AEdit.SelectAll;
          end;
        end;
        bAdd:=False;
      end;
    end;
  end;
end;

procedure TfmDOBSpec.ViewBEditKeyPress(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
Var Km:Real;
    sName:String;
begin
  if bAdd then exit;
  with dmO do
  with dmORep do
  begin
    if (ViewB.Controller.FocusedColumn.Name='ViewBNAME') then
    begin
      sName:=VarAsType(AEdit.EditingValue ,varString)+Key;
//      Label2.Caption:=sName;
      if length(sName)>2 then
      begin
        quFCard.Active:=False;
        quFCard.SelectSQL.Clear;
        quFCard.SelectSQL.Add('SELECT  first 2  ID,NAME,IMESSURE,PARENT,TCARD,INDS,LASTPRICEOUT,CATEGORY');
        quFCard.SelectSQL.Add('FROM OF_CARDS');
        quFCard.SelectSQL.Add('where UPPER(NAME) like ''%'+AnsiUpperCase(sName)+'%''');
        quFCard.SelectSQL.Add('and IACTIVE>0');
        quFCard.SelectSQL.Add('and PARENT in (SELECT ID_CLASSIF FROM RCLASSIF WHERE RIGHTS=0 AND ID_PERSONAL='+its(Person.Id)+')');
        quFCard.SelectSQL.Add('Order by NAME');
        quFCard.Active:=True;

        if quFCard.RecordCount=1 then
        begin
          ViewB.BeginUpdate;

          teSpecB.Edit;
          teSpecBSIFR.AsInteger:=0;
          teSpecBNAMEB.AsString:='';
          teSpecBCODEB.AsString:=quFCardID.AsString;
          teSpecBKB.AsFloat:=1;
          teSpecBDSUM.AsFloat:=0;
          teSpecBRSUM.AsFloat:=0;
          teSpecBQUANT.AsFloat:=1;
          teSpecBIDCARD.AsInteger:=quFCardID.AsInteger;
          teSpecBIDM.AsInteger:=quFCardIMESSURE.AsInteger;;
          teSpecBNAME.AsString:=quFCardNAME.AsString;
          teSpecBNAMESHORT.AsString:=prFindKNM(quFCardIMESSURE.AsInteger,Km);
          teSpecBKM.AsFloat:=Km;
          teSpecBPRICER.AsFloat:=0;
          teSpecBTCard.AsInteger:=quFCardTCARD.AsInteger;
          teSpecBSALET.AsInteger:=0;
          teSpecBSSALET.AsString:='';
          teSpecB.Post;
          ViewB.EndUpdate;
          AEdit.SelectAll;
          
          ViewBName.Options.Editing:=False;
          ViewBName.Focused:=True;
          Key:=#0;
        end;
      end;
    end;
  end;//}
end;

procedure TfmDOBSpec.N3Click(Sender: TObject);
begin
  Memo1.Clear;
  Memo1.Lines.Add('��������� ��� �������.');
  ViewB.BeginUpdate;
  with dmORep do
  begin
    if teSpecB.Active then
    begin
      teSpecB.First;
      while not teCalc.Eof do
      begin
        teSpecB.Edit;
        teSpecBPRICER.AsFloat:=0;
        teSpecB.Post;

        teSpecB.Next;
      end;
    end else teSpecB.Active:=True;
  end;
  ViewB.EndUpdate;
  Memo1.Lines.Add('');
  Memo1.Lines.Add('������ ��');
end;

procedure TfmDOBSpec.N2Click(Sender: TObject);
begin
  Memo1.Clear;
  Memo1.Lines.Add('��������� ��� �������.');
  ViewB.BeginUpdate;
  with dmORep do
  begin
    if teSpecB.Active then
    begin
      teSpecB.First;
      while not teSpecB.Eof do
      begin
        teSpecB.Edit;
        teSpecBPRICER.AsFloat:=0;
        teSpecBRSUM.AsFloat:=0;
        teSpecB.Post;

        teSpecB.Next;
      end;
    end else teSpecB.Active:=True;
  end;
  ViewB.EndUpdate;
  Memo1.Lines.Add('');
  Memo1.Lines.Add('������ ��');
end;

procedure TfmDOBSpec.MenuItem1Click(Sender: TObject);
begin
  prNExportExel5(ViewB);
end;

procedure TfmDOBSpec.teSpecBPRICERChange(Sender: TField);
begin
  //���������� ����
  if iCol=2 then
  begin
    teSpecBRSUM.AsFloat:=teSpecBPRICER.AsFloat*teSpecBQUANT.AsFloat;
  end;
end;

procedure TfmDOBSpec.teSpecBDSUMChange(Sender: TField);
begin
  if iCol=1 then
  begin
    teSpecBRSUM.AsFloat:=teSpecBPRICER.AsFloat*teSpecBQUANT.AsFloat;
  end;
end;

procedure TfmDOBSpec.teSpecBRSUMChange(Sender: TField);
begin
  if iCol=3 then
  begin
    if teSpecBQUANT.AsFloat<>0 then
      teSpecBPRICER.AsFloat:=teSpecBRSUM.AsFloat/teSpecBQUANT.AsFloat;
  end;
end;

procedure TfmDOBSpec.acMovePosExecute(Sender: TObject);
Var iDateB,iDateE,iM:INteger;
    IdCard:INteger;
    NameC:String;
    iMe,RecC:INteger;
begin
  //�������� �� ������
  with dmO do
  begin
    RecC:=0;
    IdCard:=0;
    iMe:=0;

    if PageControl1.ActivePageIndex=0 then
    begin
      RecC:=teSpecB.RecordCount;
      IdCard:=teSpecBIDCARD.AsInteger;
      NameC:=teSpecBNAME.AsString;
      iMe:=teSpecBIDM.AsInteger;
    end;

    if PageControl1.ActivePageIndex=1 then
    begin
      RecC:=teCalc.RecordCount;
      IdCard:=teCalcArticul.AsInteger;
      NameC:=teCalcName.AsString;
      iMe:=teCalcIdM.AsInteger;
    end;

    if RecC>0 then
    begin
      //�� ������
      fmSelPerSkl:=tfmSelPerSkl.Create(Application);
      with fmSelPerSkl do
      begin
        cxDateEdit1.Date:=CommonSet.DateFrom;
        quMHAll1.Active:=False;
        quMHAll1.ParamByName('IDPERSON').AsInteger:=Person.Id;
        quMHAll1.Active:=True;
        quMHAll1.First;
        if CommonSet.IdStore>0 then cxLookupComboBox1.EditValue:=CommonSet.IdStore
        else  cxLookupComboBox1.EditValue:=quMHAll1ID.AsInteger;
      end;
      fmSelPerSkl.ShowModal;
      if fmSelPerSkl.ModalResult=mrOk then
      begin
        iDateB:=Trunc(fmSelPerSkl.cxDateEdit1.Date);
        iDateE:=Trunc(fmSelPerSkl.cxDateEdit2.Date)+1;

        CommonSet.IdStore:=fmSelPerSkl.cxLookupComboBox1.EditValue;
        CommonSet.NameStore:=fmSelPerSkl.cxLookupComboBox1.Text;
        CommonSet.DateFrom:=iDateB;
        CommonSet.DateTo:=iDateE+1;

        CardSel.Id:=IdCard;
        CardSel.Name:=NameC;
        prFindSM(iMe,CardSel.mesuriment,iM);
//        CardSel.NameGr:=ClassTree.Selected.Text;

        prPreShowMove(IdCard,iDateB,iDateE,fmSelPerSkl.cxLookupComboBox1.EditValue,NameC);
        fmSelPerSkl.Release;

//        fmCardsMove.PageControl1.ActivePageIndex:=2;
        fmCardsMove.ShowModal;

      end else fmSelPerSkl.Release;
    end;
  end;
end;

procedure TfmDOBSpec.ViewBNAMESHORTPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
Var Km:Real;
    iM:Integer;
begin
  with dmO do
  begin
    if teSpecBIDM.AsInteger>0 then
    begin
      bAddSpec:=True;
      fmMessure.ShowModal;
      if fmMessure.ModalResult=mrOk then
      begin
        iM:=iMSel; iMSel:=0;
        
        teSpecB.Edit;

        teSpecBIDM.AsInteger:=iM;
        teSpecBNAMESHORT.AsString:=prFindKNM(iM,Km);
        teSpecBKM.AsFloat:=Km;

        teSpecB.Post;
      end;
    end;
  end;
end;

procedure TfmDOBSpec.MenuItem3Click(Sender: TObject);
begin
  prNExportExel5(ViewBC);
end;

procedure TfmDOBSpec.acDelPos1Execute(Sender: TObject);
begin
  with dmORep do
  begin
    if teSpecB.RecordCount>0 then
    begin
      teSpecB.Delete;
    end;
  end;
end;

procedure TfmDOBSpec.acSaveB1Execute(Sender: TObject);
Var Idh:Integer;
    rSumR,rSumIn:Real;
begin
  //��������� ������ //��������� ��� ���������
  with dmO do
  with dmORep do
  begin
    Memo1.Lines.Add('����� ���� ����������.');

    if cxTextEdit1.Tag>0 then
    begin //��������������
      IdH:=cxTextEdit1.Tag;

      prSave1(IDH,cxLookupComboBox1.EditValue,rSumR,rSumIn);

      quDobHead.Active:=False;
      quDobHead.ParamByName('IDH').AsInteger:=IdH;
      quDobHead.Active:=True;

      quDobHead.First;
      if quDobHead.RecordCount>0 then
      begin
        quDobHead.Edit;
        quDobHeadDATEDOC.AsDateTime:=Trunc(cxDateEdit1.Date);
        quDobHeadOPER.AsString:=cxLookupComboBox2.EditValue;
        quDobHeadNUMDOC.AsString:=cxTextEdit1.Text;
        quDobHeadIDSKL.AsInteger:=cxLookupComboBox1.EditValue;
        quDobHeadSUMIN.AsFloat:=rSumIn;
        quDobHeadSUMUCH.AsFloat:=rSumR;
        quDOBHEADCOMMENT.AsString:=cxTextEdit2.Text;
        quDobHead.Post;
      end;

      quDocsOutB.FullRefresh;
      quDocsOutB.Locate('ID',IDH,[]);
    end;

    Memo1.Lines.Add('���������� ���������.');
  end;
//  close;
end;

end.
