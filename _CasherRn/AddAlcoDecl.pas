unit AddAlcoDecl;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Placemnt, cxControls, cxContainer, cxEdit, cxTextEdit,
  cxMemo, cxMaskEdit, cxButtonEdit, StdCtrls, cxDropDownEdit, cxCalendar,
  cxGraphics, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, DB,
  cxDBData, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, dxmdaset, Menus,
  cxLookAndFeelPainters, cxButtons, ActnList, XPStyleActnCtrls, ActnMan,
  ComObj, DBClient, cxSpinEdit, cxProgressBar;

type
  TfmAddAlco = class(TForm)
    FormPlacement1: TFormPlacement;
    Panel1: TPanel;
    Memo1: TcxMemo;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    cxDateEdit1: TcxDateEdit;
    cxDateEdit2: TcxDateEdit;
    Label4: TLabel;
    cxComboBox1: TcxComboBox;
    Panel2: TPanel;
    ViAlco1: TcxGridDBTableView;
    LeAlco1: TcxGridLevel;
    GrAddAlco: TcxGrid;
    cxLookupComboBox1: TcxLookupComboBox;
    LeAlco2: TcxGridLevel;
    ViAlco2: TcxGridDBTableView;
    teAlcoOb: TdxMemData;
    teAlcoIn: TdxMemData;
    dsteAlcoOb: TDataSource;
    dsteAlcoIn: TDataSource;
    teAlcoObiDep: TIntegerField;
    teAlcoObsDep: TStringField;
    teAlcoObiVid: TIntegerField;
    teAlcoObsVid: TStringField;
    teAlcoObsVidName: TStringField;
    teAlcoObiProd: TIntegerField;
    teAlcoObsProd: TStringField;
    teAlcoObsProdInn: TStringField;
    teAlcoObsProdKPP: TStringField;
    teAlcoObrQb: TFloatField;
    teAlcoObrQIn1: TFloatField;
    teAlcoObrQIn2: TFloatField;
    teAlcoObrQIn3: TFloatField;
    teAlcoObrQInIt1: TFloatField;
    teAlcoObrQIn4: TFloatField;
    teAlcoObrQIn5: TFloatField;
    teAlcoObrQIn6: TFloatField;
    teAlcoObrQInIt: TFloatField;
    teAlcoObrQOut1: TFloatField;
    teAlcoObrQOut2: TFloatField;
    teAlcoObrQOut3: TFloatField;
    teAlcoObrQOut4: TFloatField;
    teAlcoObrQOutIt: TFloatField;
    teAlcoObrQe: TFloatField;
    ViAlco1iDep: TcxGridDBColumn;
    ViAlco1sDep: TcxGridDBColumn;
    ViAlco1iVid: TcxGridDBColumn;
    ViAlco1sVid: TcxGridDBColumn;
    ViAlco1sVidName: TcxGridDBColumn;
    ViAlco1iProd: TcxGridDBColumn;
    ViAlco1sProd: TcxGridDBColumn;
    ViAlco1sProdInn: TcxGridDBColumn;
    ViAlco1sProdKPP: TcxGridDBColumn;
    ViAlco1rQb: TcxGridDBColumn;
    ViAlco1rQIn1: TcxGridDBColumn;
    ViAlco1rQIn2: TcxGridDBColumn;
    ViAlco1rQIn3: TcxGridDBColumn;
    ViAlco1rQInIt1: TcxGridDBColumn;
    ViAlco1rQIn4: TcxGridDBColumn;
    ViAlco1rQIn5: TcxGridDBColumn;
    ViAlco1rQIn6: TcxGridDBColumn;
    ViAlco1rQInIt: TcxGridDBColumn;
    ViAlco1rQOut1: TcxGridDBColumn;
    ViAlco1rQOut2: TcxGridDBColumn;
    ViAlco1rQOut3: TcxGridDBColumn;
    ViAlco1rQOut4: TcxGridDBColumn;
    ViAlco1rQOutIt: TcxGridDBColumn;
    ViAlco1rQe: TcxGridDBColumn;
    teAlcoIniDep: TIntegerField;
    teAlcoInsDep: TStringField;
    teAlcoIniVid: TIntegerField;
    teAlcoInsVid: TStringField;
    teAlcoInsVidName: TStringField;
    teAlcoIniProd: TIntegerField;
    teAlcoInsProd: TStringField;
    teAlcoInsProdInn: TStringField;
    teAlcoInsProdKPP: TStringField;
    teAlcoIniPost: TIntegerField;
    teAlcoInsPost: TStringField;
    teAlcoInsPostInn: TStringField;
    teAlcoInsPostKpp: TStringField;
    teAlcoIniLic: TIntegerField;
    teAlcoInsLicNumber: TStringField;
    teAlcoInLicDateB: TDateField;
    teAlcoInLicDateE: TDateField;
    teAlcoInLicOrg: TStringField;
    teAlcoInDocDate: TDateField;
    teAlcoInDocNum: TStringField;
    teAlcoInGTD: TStringField;
    teAlcoInrQIn: TFloatField;
    ViAlco2iDep: TcxGridDBColumn;
    ViAlco2sDep: TcxGridDBColumn;
    ViAlco2iVid: TcxGridDBColumn;
    ViAlco2sVid: TcxGridDBColumn;
    ViAlco2sVidName: TcxGridDBColumn;
    ViAlco2iProd: TcxGridDBColumn;
    ViAlco2sProd: TcxGridDBColumn;
    ViAlco2sProdInn: TcxGridDBColumn;
    ViAlco2sProdKPP: TcxGridDBColumn;
    ViAlco2iPost: TcxGridDBColumn;
    ViAlco2sPost: TcxGridDBColumn;
    ViAlco2sPostInn: TcxGridDBColumn;
    ViAlco2sPostKpp: TcxGridDBColumn;
    ViAlco2iLic: TcxGridDBColumn;
    ViAlco2sLicNumber: TcxGridDBColumn;
    ViAlco2LicDateB: TcxGridDBColumn;
    ViAlco2LicDateE: TcxGridDBColumn;
    ViAlco2LicOrg: TcxGridDBColumn;
    ViAlco2DocDate: TcxGridDBColumn;
    ViAlco2DocNum: TcxGridDBColumn;
    ViAlco2GTD: TcxGridDBColumn;
    ViAlco2rQIn: TcxGridDBColumn;
    Label5: TLabel;
    cxComboBox2: TcxComboBox;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    amAddAlco: TActionManager;
    acGetIn: TAction;
    cxButton5: TcxButton;
    cxButton6: TcxButton;
    acGetOutR: TAction;
    cxButton7: TcxButton;
    acGetRet: TAction;
    cxButton8: TcxButton;
    cxButton9: TcxButton;
    acExpExls: TAction;
    acExpXML: TAction;
    cxButton10: TcxButton;
    OpenDialog1: TOpenDialog;
    teProds: TdxMemData;
    teProdsiProd: TIntegerField;
    teProdssProd: TStringField;
    teProdssProdInn: TStringField;
    teProdssProdKPP: TStringField;
    tePost: TdxMemData;
    tePostiPost: TIntegerField;
    tePostsPost: TStringField;
    tePostsPostInn: TStringField;
    tePostsPostKpp: TStringField;
    tePostiLic: TIntegerField;
    tePostLicNumber: TStringField;
    tePostLicDateB: TDateField;
    tePostLicDateE: TDateField;
    tePostLicOrg: TStringField;
    teA1: TClientDataSet;
    teA1iDep: TIntegerField;
    teA1sDep: TStringField;
    teA1iVid: TIntegerField;
    teA1sVidName: TStringField;
    teA1iProd: TIntegerField;
    teA1rQb: TFloatField;
    teA1rQIn1: TFloatField;
    teA1rQIn2: TFloatField;
    teA1rQIn3: TFloatField;
    teA1rQInIt1: TFloatField;
    teA1rQIn4: TFloatField;
    teA1rQIn5: TFloatField;
    teA1rQIn6: TFloatField;
    teA1rQInIt: TFloatField;
    teA1rQOut1: TFloatField;
    teA1rQOut2: TFloatField;
    teA1rQOut3: TFloatField;
    teA1rQOut4: TFloatField;
    teA1rQOutIt: TFloatField;
    teA1rQe: TFloatField;
    teA2: TClientDataSet;
    teA2iDep: TIntegerField;
    teA2iVid: TIntegerField;
    teA2iProd: TIntegerField;
    teA2iPost: TIntegerField;
    teA2DocDate: TDateTimeField;
    teA2DocNum: TStringField;
    teA2rQIn: TFloatField;
    dsteA1: TDataSource;
    teA2GTD: TStringField;
    teA2iLic: TIntegerField;
    acGenGUID: TAction;
    teLic: TdxMemData;
    teLiciPost: TIntegerField;
    teLiciLic: TIntegerField;
    teLicLicNumber: TStringField;
    teLicLicDateB: TDateField;
    teLicLicDateE: TDateField;
    teLicLicOrg: TStringField;
    acImportXLS: TAction;
    PopupMenu1: TPopupMenu;
    acImportXLS1: TMenuItem;
    acGetRemn2: TAction;
    Label6: TLabel;
    cxSpinEdit1: TcxSpinEdit;
    PBar1: TcxProgressBar;
    teAlcoInIdDH: TIntegerField;
    acInputPost: TAction;
    N1: TMenuItem;
    acRecalcLic: TAction;
    N2: TMenuItem;
    acExpXMLold: TAction;
    procedure FormCreate(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cxButton4Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure acGetInExecute(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure acGetOutRExecute(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
    procedure acGetRetExecute(Sender: TObject);
    procedure cxButton7Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton8Click(Sender: TObject);
    procedure acExpExlsExecute(Sender: TObject);
    procedure acExpXMLExecute(Sender: TObject);
    procedure acGenGUIDExecute(Sender: TObject);
    procedure acImportXLSExecute(Sender: TObject);
    procedure acGetRemn2Execute(Sender: TObject);
    procedure acInputPostExecute(Sender: TObject);
    procedure acRecalcLicExecute(Sender: TObject);
    procedure acExpXMLoldExecute(Sender: TObject);
  private
    { Private declarations }
    procedure prButtons(bStr:Boolean);
  public
    { Public declarations }
  end;

var
  fmAddAlco: TfmAddAlco;

implementation

uses Un1, DMOReps, dmOffice, AlcoDecl, SelPost;

{$R *.dfm}
procedure TfmAddAlco.prButtons(bStr:Boolean);
begin
  cxButton1.Enabled:=bStr;
  cxButton2.Enabled:=bStr;
  cxButton3.Enabled:=bStr;
  cxButton4.Enabled:=bStr;
  cxButton5.Enabled:=bStr;
  cxButton6.Enabled:=bStr;
  cxButton7.Enabled:=bStr;
  cxButton8.Enabled:=bStr;
  cxButton9.Enabled:=bStr;
  cxButton10.Enabled:=bStr;
  delay(10);
end;

procedure TfmAddAlco.FormCreate(Sender: TObject);
begin
  GrAddAlco.Align:=AlClient;
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  Memo1.Clear;
end;

procedure TfmAddAlco.cxButton4Click(Sender: TObject);
begin
//  if cxButton1.Enabled then close;
  close;
end;

procedure TfmAddAlco.cxButton1Click(Sender: TObject);
Var IDH:INteger;
//    par:Variant;
begin
  with dmORep do
  begin
    if MessageDlg('��������� ��������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      Memo1.Clear;
      Memo1.Lines.Add('����� ���� ���������� ���������.'); delay(10);
      Memo1.Lines.Add('  - ���������'); delay(10);

      prButtons(False);

      if cxLookupComboBox1.Tag=0 then
      begin //����������
        IDH:=GetId('124');

        cxLookupComboBox1.Tag:=IDH;

        quAlcoDH.Append;
        quAlcoDHIdOrg.AsInteger:=cxLookupComboBox1.EditValue;
        quAlcoDHId.AsInteger:=IDH;
        quAlcoDHIDateB.AsInteger:=Trunc(cxDateEdit1.Date);
        quAlcoDHIDateE.AsInteger:=Trunc(cxDateEdit2.Date);
        quAlcoDHDDateB.AsDateTime:=cxDateEdit1.Date;
        quAlcoDHDDateE.AsDateTime:=cxDateEdit2.Date;
        quAlcoDHSPers.AsString:=FormatDateTime('dd.mm.yy hh:nn',now)+' '+Person.Name;
        quAlcoDHiT1.AsInteger:=cxComboBox1.ItemIndex;
        quAlcoDHiT2.AsInteger:=cxComboBox2.ItemIndex;
        quAlcoDHiNumCorr.AsInteger:=cxSpinEdit1.Value;
        quAlcoDH.Post;

        quAlcoDH.FullRefresh;

        quAlcoDH.Locate('ID',IDH,[]);

      end else //��������������
      begin
        IDH:=cxLookupComboBox1.Tag;

        quAlcoDH.Edit;
        quAlcoDHIdOrg.AsInteger:=cxLookupComboBox1.EditValue;
        quAlcoDHIDateB.AsInteger:=Trunc(cxDateEdit1.Date);
        quAlcoDHIDateE.AsInteger:=Trunc(cxDateEdit2.Date);
        quAlcoDHDDateB.AsDateTime:=cxDateEdit1.Date;
        quAlcoDHDDateE.AsDateTime:=cxDateEdit2.Date;
        quAlcoDHSPers.AsString:=FormatDateTime('dd.mm.yy hh:nn',now)+' '+Person.Name;
        quAlcoDHiT1.AsInteger:=cxComboBox1.ItemIndex;
        quAlcoDHiT2.AsInteger:=cxComboBox2.ItemIndex;
        quAlcoDHiNumCorr.AsInteger:=cxSpinEdit1.Value;
        quAlcoDH.Post;

        quAlcoDH.Refresh;

      end;

      Memo1.Lines.Add('  - ������������ .'); delay(10);

      quDelDS1.ParamByName('IDH').AsInteger:=IDH;
      quDelDS1.ExecQuery ;

      delay(100);

      quAlcoDS1.Active:=False;
      quAlcoDS1.ParamByName('IDH').AsInteger:=IDH;
      quAlcoDS1.Active:=True;

      ViAlco1.BeginUpdate;
      teAlcoOb.First;        //����
      while not teAlcoOb.Eof do
      begin
        quAlcoDS1.Append;
        quAlcoDS1ID.AsInteger:=GetId('125');
        quAlcoDS1IdH.AsInteger:=IDH;
        quAlcoDS1iDep.AsInteger:=teAlcoObiDep.AsInteger;
        quAlcoDS1iNum.AsInteger:=0;
        quAlcoDS1iVid.AsInteger:=teAlcoObiVid.AsInteger;
        quAlcoDS1iProd.AsInteger:=teAlcoObiProd.AsInteger;
        quAlcoDS1rQb.AsFloat:=teAlcoObrQb.AsFloat;
        quAlcoDS1rQIn1.AsFloat:=teAlcoObrQIn1.AsFloat;
        quAlcoDS1rQIn2.AsFloat:=teAlcoObrQIn2.AsFloat;
        quAlcoDS1rQIn3.AsFloat:=teAlcoObrQIn3.AsFloat;
        quAlcoDS1rQInIt1.AsFloat:=teAlcoObrQInIt1.AsFloat;
        quAlcoDS1rQIn4.AsFloat:=teAlcoObrQIn4.AsFloat;
        quAlcoDS1rQIn5.AsFloat:=teAlcoObrQIn5.AsFloat;
        quAlcoDS1rQIn6.AsFloat:=teAlcoObrQIn6.AsFloat;
        quAlcoDS1rQInIt.AsFloat:=teAlcoObrQInIt.AsFloat;
        quAlcoDS1rQOut1.AsFloat:=teAlcoObrQOut1.AsFloat;
        quAlcoDS1rQOut2.AsFloat:=teAlcoObrQOut2.AsFloat;
        quAlcoDS1rQOut3.AsFloat:=teAlcoObrQOut3.AsFloat;
        quAlcoDS1rQOut4.AsFloat:=teAlcoObrQOut4.AsFloat;
        quAlcoDS1rQOutIt.AsFloat:=teAlcoObrQOutIt.AsFloat;
        quAlcoDS1rQe.AsFloat:=teAlcoObrQe.AsFloat;
        quAlcoDS1.Post;

        teAlcoOb.Next;
      end;

      teAlcoOb.First;

      ViAlco1.EndUpdate;

      quAlcoDS1.Active:=False;

      Memo1.Lines.Add('  - ������������ ..'); delay(10);

      quDelDS2.ParamByName('IDH').AsInteger:=IDH;
      quDelDS2.ExecQuery;

      delay(100);

      quAlcoDS2.Active:=False;
      quAlcoDS2.ParamByName('IDH').AsInteger:=IDH;
      quAlcoDS2.Active:=True;

      ViAlco2.BeginUpdate;
      teAlcoIn.First;
      while not teAlcoIn.Eof do
      begin
        quAlcoDS2.Append;
        quAlcoDS2IdH.AsInteger:=IDH;
        quAlcoDS2ID.AsInteger:=GetId('126');
        quAlcoDS2iDep.AsInteger:=teAlcoIniDep.AsInteger;
        quAlcoDS2iVid.AsInteger:=teAlcoIniVid.AsInteger;
        quAlcoDS2iProd.AsInteger:=teAlcoIniProd.AsInteger;
        quAlcoDS2iPost.AsInteger:=teAlcoIniPost.AsInteger;
        quAlcoDS2iLic.AsInteger:=teAlcoIniLic.AsInteger;
        quAlcoDS2DocDate.AsDateTime:=teAlcoInDocDate.AsDateTime;
        quAlcoDS2DocNum.AsString:=teAlcoInDocNum.AsString;
        quAlcoDS2GTD.AsString:=teAlcoInGTD.AsString;
        quAlcoDS2rQIn.AsFloat:=teAlcoInrQIn.AsFloat;
        quAlcoDS2.Post;

        teAlcoIn.Next;
      end;
      teAlcoIn.First;
      ViAlco2.EndUpdate;
      quAlcoDS2.Active:=False;

      Memo1.Lines.Add('C��������� Ok.'); delay(10);

      prButtons(True);
    end;
  end;
end;

procedure TfmAddAlco.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if cxButton4.Enabled=False then Action := caNone;
end;

procedure TfmAddAlco.cxButton2Click(Sender: TObject);
begin
  if cxButton1.Enabled then
  begin
    Memo1.Clear;
    Memo1.Lines.Add('  ������.'); delay(10);

    with fmAddAlco do
    begin
      ViAlco1.BeginUpdate;
      ViAlco2.BeginUpdate;
      CloseTe(teAlcoOb);
      CloseTe(teAlcoIn);
      ViAlco1.EndUpdate;
      ViAlco2.EndUpdate;
    end;

    Memo1.Lines.Add('  ������ ��.'); delay(10);

    prButtons(False);
//    acGetRemn1.Execute;
    acGetRemn2.Execute;
    prButtons(True);
  end;
end;

procedure TfmAddAlco.cxButton5Click(Sender: TObject);
begin
  if cxButton1.Enabled then
  begin
    prButtons(False);
    acGetIn.Execute;
    prButtons(True);
  end;
end;

procedure TfmAddAlco.acGetInExecute(Sender: TObject);
Var par,par1:Variant;
    iC:INteger;
    iVb,iVe, iMax, iStep,iDep,iVb1,iVe1:INteger;
    kVol,rQIn:Real;
    LicId:INteger;
    LicSerN,LicOrg:String;
    LicDateB,LicDateE: TDateTime;

begin
  with dmORep do
  with dmO do
  begin
    Memo1.Lines.Add('  �������.'); delay(10);
    iDep:=cxLookupComboBox1.EditValue;

    iC:=0;

    if cxComboBox1.ItemIndex=0 then begin iVb1:=0; iVe1:=259;  iVb:=264; iVe:=499; end
    else begin iVb1:=260; iVe1:=263; iVb:=500; iVe:=999; end;

    PBar1.Position:=0;
    PBar1.Visible:=True;

    quCardsAlg.Active:=False;
    quCardsAlg.Active:=True;

    iMax:=quCardsAlg.RecordCount;
    iStep:=iMax div 100;
    if iStep=0 then iStep:=1;

    Memo1.Lines.Add('   ����� '+its(iMax)); Delay(10);

    ViAlco1.BeginUpdate;
    ViAlco2.BeginUpdate;

    par := VarArrayCreate([0,3], varInteger);
    par1 := VarArrayCreate([0,2], varInteger);

    teAlcoIn.First; //������ ��������� �������������
    while not teAlcoIn.Eof do teAlcoIn.Delete;

    teAlcoOb.First;
    while not teAlcoOb.Eof do
    begin
      teAlcoOb.Edit;
      teAlcoObrQIn1.AsFloat:=0;
      teAlcoObrQIn2.AsFloat:=0;
      teAlcoObrQIn3.AsFloat:=0;
      teAlcoObrQIn4.AsFloat:=0;
      teAlcoObrQIn5.AsFloat:=0;
      teAlcoObrQIn6.AsFloat:=0;
      teAlcoObrQInIt.AsFloat:=0;
      teAlcoObrQInIt1.AsFloat:=0;
      teAlcoObrQe.AsFloat:=teAlcoObrQb.AsFloat-teAlcoObrQOutIt.AsFloat;
      teAlcoOb.Post;

      teAlcoOb.Next;
    end;

    quCardsAlg.First;
    while not quCardsAlg.Eof do
    begin
      if ((quCardsAlgALGCLASS.AsInteger>=iVb1)and(quCardsAlgALGCLASS.AsInteger<=iVe1))or((quCardsAlgALGCLASS.AsInteger>=iVb)and(quCardsAlgALGCLASS.AsInteger<=iVe)) then
      begin
        kVol:=quCardsAlgVOL.AsFloat;

        quInLnAlg.Active:=False;
        quInLnAlg.ParamByName('ICARD').AsInteger:=quCardsAlgID.AsInteger;
        quInLnAlg.ParamByName('ISKL').AsInteger:=iDep;
        quInLnAlg.ParamByName('DATEB').AsDate:=cxDateEdit1.Date;
        quInLnAlg.ParamByName('DATEE').AsDate:=cxDateEdit2.Date;
        quInLnAlg.Active:=True;

        quInLnAlg.First;
        while not quInLnAlg.Eof do
        begin
          par[0]:=iDep;
          par[1]:=quCardsAlgALGCLASS.AsInteger;
          par[2]:=quCardsAlgALGMAKER.AsInteger;
          par[3]:=quInLnAlgIDHEAD.AsInteger;

          rQIn:=quInLnAlgQUANT.AsFloat*quInLnAlgKM.AsFloat;
          rQIn:=r1000((rQIn*kVol)/10);

          if teAlcoIn.Locate('iDep;iVid;iProd;IdDH',par,[]) then
          begin //�����������
            teAlcoIn.Edit;
            teAlcoInrQIn.AsFloat:=teAlcoInrQIn.AsFloat+rQIn;
            teAlcoIn.Post;
          end else
          begin  //��������� ������  -  ����� ���� ������
            LicId:=0;
            LicSerN:='';
            LicOrg:='';
            LicDateB:=date;
            LicDateE:=date;

            quCliLicMax.Active:=False;
            quCliLicMax.ParamByName('ICLI').AsInteger:=quInLnAlgIDCLI.AsInteger;
            quCliLicMax.Active:=True;
            quCliLicMax.First;

            if quCliLicMax.RecordCount>0 then
            begin
              LicSerN:=quCliLicMaxSER.AsString+' '+quCliLicMaxSNUM.AsString;
              LicOrg:=quCliLicMaxORGAN.AsString;
              LicDateB:=quCliLicMaxDDATEB.AsDateTime;
              LicDateE:=quCliLicMaxDDATEE.AsDateTime;
              LicId:=quCliLicMaxID.AsInteger;
            end;
            quCliLicMax.Active:=False;

            teAlcoIn.Append;
            teAlcoIniDep.AsInteger:=iDep;
            teAlcoInsDep.AsString:=cxLookupComboBox1.Text;
            teAlcoIniVid.AsInteger:=quCardsAlgALGCLASS.AsInteger;
            teAlcoInsVid.AsString:=quCardsAlgALGCLASS.AsString;
            teAlcoInsVidName.AsString:=quCardsAlgNAMECLA.AsString;
            teAlcoIniProd.AsInteger:=quCardsAlgALGMAKER.AsInteger;
            teAlcoInsProd.AsString:=quCardsAlgNAMEM.AsString;
            teAlcoInsProdInn.AsString:=quCardsAlgINNM.AsString;
            teAlcoInsProdKPP.AsString:=quCardsAlgKPPM.AsString;
            teAlcoIniPost.AsInteger:=quInLnAlgIDCLI.AsInteger;
            teAlcoInsPost.AsString:=quInLnAlgFULLNAMECL.AsString;
            teAlcoInsPostInn.AsString:=quInLnAlgINN.AsString;
            teAlcoInsPostKpp.AsString:=quInLnAlgKPP.AsString;
            teAlcoIniLic.AsInteger:=LicId;
            teAlcoInsLicNumber.AsString:=LicSerN;
            teAlcoInLicDateB.AsDateTime:=LicDateB;
            teAlcoInLicDateE.AsDateTime:=LicDateE;
            teAlcoInLicOrg.AsString:=LicOrg;
            teAlcoInDocDate.AsDateTime:=quInLnAlgDATEDOC.AsDateTime;
            teAlcoInDocNum.AsString:=quInLnAlgNUMDOC.AsString;
            teAlcoInGTD.AsString:=quInLnAlgGTD.AsString;
            teAlcoInrQIn.AsFloat:=rQIn;
            teAlcoInIdDH.AsInteger:=quInLnAlgIDHEAD.AsInteger;
            teAlcoIn.Post;
          end;


          par1[0]:=iDep;
          par1[1]:=quCardsAlgALGCLASS.AsInteger;
          par1[2]:=quCardsAlgALGMAKER.AsInteger;

          if teAlcoOb.Locate('iDep;iVid;iProd',par,[]) then
          begin //���� ����� ������
            teAlcoOb.Edit;
            teAlcoObrQIn2.AsFloat:=teAlcoObrQIn2.AsFloat+rQIn;
            teAlcoObrQIn3.AsFloat:=0;
            teAlcoObrQIn4.AsFloat:=0;
            teAlcoObrQIn5.AsFloat:=0;
            teAlcoObrQIn6.AsFloat:=0;
            teAlcoObrQInIt.AsFloat:=teAlcoObrQInIt.AsFloat+rQIn;
            teAlcoObrQInIt1.AsFloat:=teAlcoObrQInIt1.AsFloat+rQIn;
            teAlcoObrQe.AsFloat:=teAlcoObrQb.AsFloat+teAlcoObrQInIt.AsFloat-teAlcoObrQOutIt.AsFloat;
            teAlcoOb.Post;
          end else
          begin
            teAlcoOb.Append;
            teAlcoObiDep.AsInteger:=iDep;
            teAlcoObsDep.AsString:=cxLookupComboBox1.Text;
            teAlcoObiVid.AsInteger:=quCardsAlgALGCLASS.AsInteger;
            teAlcoObsVid.AsString:=quCardsAlgALGCLASS.AsString;
            teAlcoObsVidName.AsString:=quCardsAlgNAMECLA.AsString;
            teAlcoObiProd.AsInteger:=quCardsAlgALGMAKER.AsInteger;
            teAlcoObsProd.AsString:=quCardsAlgNAMEM.AsString;
            teAlcoObsProdInn.AsString:=quCardsAlgINNM.AsString;
            teAlcoObsProdKPP.AsString:=quCardsAlgKPPM.AsString;
            teAlcoObrQb.AsFloat:=0;
            teAlcoObrQIn1.AsFloat:=0;
            teAlcoObrQIn2.AsFloat:=rQIn;
            teAlcoObrQIn3.AsFloat:=0;
            teAlcoObrQInIt1.AsFloat:=rQIn;
            teAlcoObrQIn4.AsFloat:=0;
            teAlcoObrQIn5.AsFloat:=0;
            teAlcoObrQIn6.AsFloat:=0;
            teAlcoObrQInIt.AsFloat:=rQIn;
            teAlcoObrQOut1.AsFloat:=0;
            teAlcoObrQOut2.AsFloat:=0;
            teAlcoObrQOut3.AsFloat:=0;
            teAlcoObrQOut4.AsFloat:=0;
            teAlcoObrQOutIt.AsFloat:=0;
            teAlcoObrQe.AsFloat:=rQIn;
            teAlcoOb.Post;
          end;

          quInLnAlg.Next;
        end;

        quInLnAlg.Active:=False;
      end;

      quCardsAlg.Next; inc(iC);
      if iC mod iStep = 0 then
      begin
        PBar1.Position:=iC div iStep;
        delay(10);
      end;
    end;

    ViAlco2.EndUpdate;
    ViAlco1.EndUpdate;

    Memo1.Lines.Add('  ������� ��'); delay(10);

    delay(100); PBar1.Visible:=False;
  end;
end;

procedure TfmAddAlco.cxButton6Click(Sender: TObject);
begin
  if cxButton1.Enabled then
  begin
    prButtons(False);
    acGetOutR.Execute;
    prButtons(True);
  end;
end;

procedure TfmAddAlco.acGetOutRExecute(Sender: TObject);
Var par:Variant;
    iC,iVb,iVe, iMax, iStep,iDep:INteger;
    kVol,rQe:Real;
begin
  with dmORep do
  begin
    Memo1.Lines.Add('  ������� �� ����� � ������.'); delay(10);

    iDep:=cxLookupComboBox1.EditValue;

    iC:=0;

    if cxComboBox1.ItemIndex=0 then begin iVb:=0; iVe:=499; end
    else begin iVb:=500; iVe:=999; end;

    PBar1.Position:=0;
    PBar1.Visible:=True;

    quCardsAlg.Active:=False;
    quCardsAlg.Active:=True;

    iMax:=quCardsAlg.RecordCount;
    iStep:=iMax div 100;
    if iStep=0 then iStep:=1;

    Memo1.Lines.Add('   ����� '+its(iMax)); Delay(10);

    ViAlco1.BeginUpdate;

    par := VarArrayCreate([0,3], varInteger);

    teAlcoOb.First;
    while not teAlcoOb.Eof do
    begin
      teAlcoOb.Edit;
      teAlcoObrQOut1.AsFloat:=0;
      teAlcoObrQOutIt.AsFloat:=teAlcoObrQOut3.AsFloat; //��������
      teAlcoObrQe.AsFloat:=teAlcoObrQb.AsFloat+teAlcoObrQInIt.AsFloat-teAlcoObrQOutIt.AsFloat;
      teAlcoOb.Post;

      teAlcoOb.Next;
    end;

    quCardsAlg.First;
    while not quCardsAlg.Eof do
    begin
      if (quCardsAlgALGCLASS.AsInteger>=iVb)and(quCardsAlgALGCLASS.AsInteger<=iVe) then
      begin
        kVol:=quCardsAlgVOL.AsFloat;
        rQE:=prCalcRemn(quCardsAlgID.AsInteger,Trunc(cxDateEdit2.Date),iDep); //������� �� �����

        rQE:=r1000((rQe*kVol)/10);

        par[0]:=iDep;
        par[1]:=quCardsAlgALGCLASS.AsInteger;
        par[2]:=quCardsAlgALGMAKER.AsInteger;

        if teAlcoOb.Locate('iDep;iVid;iProd',par,[]) then
        begin //���� ����� ������
          teAlcoOb.Edit;
          teAlcoObrQOut1.AsFloat :=teAlcoObrQb.AsFloat+teAlcoObrQInIt.AsFloat-teAlcoObrQOut3.AsFloat-rQe;
          teAlcoObrQOut2.AsFloat :=0;
          teAlcoObrQOut4.AsFloat :=0;
          teAlcoObrQOutIt.AsFloat:=teAlcoObrQb.AsFloat+teAlcoObrQInIt.AsFloat-rQe;
          teAlcoObrQe.AsFloat:=rQe;
          teAlcoOb.Post;
        end else
        begin //���� ������ �.�. ������ ������ ����, � 0 ������ �� �����
        end;
      end;

      quCardsAlg.Next; inc(iC);
      if iC mod iStep = 0 then
      begin
        PBar1.Position:=iC div iStep;
        delay(10);
      end;
    end;

    ViAlco1.EndUpdate;
    Memo1.Lines.Add('  ������� � ������� �� ����� ��'); delay(10);

    delay(100); PBar1.Visible:=False;
  end;
end;


procedure TfmAddAlco.acGetRetExecute(Sender: TObject);
Var par:Variant;
    iC,iVb,iVe, iMax, iStep,iDep,iVb1,iVe1:INteger;
    kVol,rQRet:Real;
begin
  with dmORep do
  with dmO do
  begin
    Memo1.Lines.Add('  ��������.'); delay(10);

    iDep:=cxLookupComboBox1.EditValue;

    iC:=0;

//    if cxComboBox1.ItemIndex=0 then begin iVb:=0; iVe:=499; end
//    else begin iVb:=500; iVe:=999; end;

    if cxComboBox1.ItemIndex=0 then begin iVb1:=0; iVe1:=259;  iVb:=264; iVe:=499; end
    else begin iVb1:=260; iVe1:=263; iVb:=500; iVe:=999; end;

    PBar1.Position:=0;
    PBar1.Visible:=True;

    quCardsAlg.Active:=False;
    quCardsAlg.Active:=True;

    iMax:=quCardsAlg.RecordCount;
    iStep:=iMax div 100;
    if iStep=0 then iStep:=1;

    Memo1.Lines.Add('   ����� '+its(iMax)); Delay(10);

    ViAlco1.BeginUpdate;

    par:=VarArrayCreate([0,3], varInteger);

    teAlcoOb.First;
    while not teAlcoOb.Eof do
    begin
      teAlcoOb.Edit;
      teAlcoObrQOut3.AsFloat:=0;    //�������� ��������
      teAlcoObrQOutIt.AsFloat:=teAlcoObrQOut1.AsFloat;
      teAlcoObrQe.AsFloat:=teAlcoObrQb.AsFloat+teAlcoObrQInIt.AsFloat-teAlcoObrQOutIt.AsFloat;
      teAlcoOb.Post;

      teAlcoOb.Next;
    end;

    quCardsAlg.First;
    while not quCardsAlg.Eof do
    begin
      if ((quCardsAlgALGCLASS.AsInteger>=iVb)and(quCardsAlgALGCLASS.AsInteger<=iVe))or((quCardsAlgALGCLASS.AsInteger>=iVb1)and(quCardsAlgALGCLASS.AsInteger<=iVe1)) then
      begin
        kVol:=quCardsAlgVOL.AsFloat;

        quQuantInOut.Active:=False;
        quQuantInOut.ParamByName('IDCARD').AsInteger:=quCardsAlgID.AsInteger;
        quQuantInOut.ParamByName('ISKL').AsInteger:=iDep;
        quQuantInOut.ParamByName('IDATEB').AsInteger:=Trunc(cxDateEdit1.Date);
        quQuantInOut.ParamByName('IDATEE').AsInteger:=Trunc(cxDateEdit2.Date);
        quQuantInOut.Active:=True;

        rQRet:=quQuantInOutQOUT.AsFloat;  //��� � ��������

        rQRet:=r1000((rQRet*kVol)/10);

        quQuantInOut.Active:=False;

        par[0]:=iDep;
        par[1]:=quCardsAlgALGCLASS.AsInteger;
        par[2]:=quCardsAlgALGMAKER.AsInteger;

        if teAlcoOb.Locate('iDep;iVid;iProd',par,[]) then
        begin //���� ����� ������
          teAlcoOb.Edit;
          teAlcoObrQOut3.AsFloat:=rQRet;
          teAlcoObrQOutIt.AsFloat:=teAlcoObrQOut1.AsFloat+rQRet;
          teAlcoObrQe.AsFloat:=teAlcoObrQb.AsFloat+teAlcoObrQInIt.AsFloat-teAlcoObrQOutIt.AsFloat;
          teAlcoOb.Post;
        end else
        begin
//       ����������� ���-�� - ���� ������ ��� �� ����� �� ��������� - ������ � ��������� �������� ��� �������� - ����� �������� ������� ��������������, � �� � ��������� ����������
        end;
      end;

      quCardsAlg.Next; inc(iC);
      if iC mod iStep = 0 then
      begin
        PBar1.Position:=iC div iStep;
        delay(10);
      end;
    end;

    ViAlco1.EndUpdate;
    Memo1.Lines.Add('  ������� � ������� �� ����� ��'); delay(10);

    delay(100); PBar1.Visible:=False;

    Memo1.Lines.Add('  �������� ��'); delay(10);
  end;
end;

procedure TfmAddAlco.cxButton7Click(Sender: TObject);
begin
  if cxButton1.Enabled then
  begin
    prButtons(False);
    acGetRet.Execute;
    prButtons(True);
  end;
end;

procedure TfmAddAlco.cxButton3Click(Sender: TObject);
begin
  if cxButton1.Enabled then
  begin
    Memo1.Clear;
    Memo1.Lines.Add(formatdatetime('hh:nn:ss',now)+'  ������.'); delay(10);

    with fmAddAlco do
    begin
      ViAlco1.BeginUpdate;
      ViAlco2.BeginUpdate;
      CloseTe(teAlcoOb);
      CloseTe(teAlcoIn);
      ViAlco1.EndUpdate;
      ViAlco2.EndUpdate;
    end;
    Memo1.Lines.Add(formatdatetime('hh:nn:ss',now)+'  ������ ��.'); delay(10);

    Memo1.Lines.Add(formatdatetime('hh:nn:ss',now)+'������.'); delay(10);

    prButtons(False);
    acGetRemn2.Execute;
    Memo1.Lines.Add(formatdatetime('hh:nn:ss',now)); delay(10);
    acGetIn.Execute;
    Memo1.Lines.Add(formatdatetime('hh:nn:ss',now)); delay(10);
    acGetRet.Execute;
    Memo1.Lines.Add(formatdatetime('hh:nn:ss',now)); delay(10);
    acGetOutR.Execute;
    prButtons(True);

    Memo1.Lines.Add(formatdatetime('hh:nn:ss',now)+'������ ��.'); delay(10);
  end;
end;

procedure TfmAddAlco.cxButton8Click(Sender: TObject);
Var par:Variant;
    iC:INteger;
begin
  // �����������
  with dmORep do
  begin
    if MessageDlg('����������� ��������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      Memo1.Clear;
      Memo1.Lines.Add('����� ���� �������� ���������.'); delay(10);
      Memo1.Lines.Add('  - ������� ('+Its(teAlcoIn.RecordCount)+')'); delay(10);

      ViAlco1.BeginUpdate;
      ViAlco2.BeginUpdate;

      par := VarArrayCreate([0,3], varInteger);

      iC:=0;
      // ������� ��� �������� �� ������ ������

      teAlcoOb.First;
      while not teAlcoOb.Eof do
      begin
        teAlcoOb.Edit;
        teAlcoObrQb.AsFloat:=R1000(teAlcoObrQb.AsFloat);
        teAlcoObrQIn1.AsFloat:=0;
        teAlcoObrQIn2.AsFloat:=0;
        teAlcoObrQIn3.AsFloat:=0;
        teAlcoObrQIn4.AsFloat:=0;
        teAlcoObrQIn5.AsFloat:=0;
        teAlcoObrQIn6.AsFloat:=0;
        teAlcoObrQInIt.AsFloat:=0;
        teAlcoObrQInIt1.AsFloat:=0;

        teAlcoObrQOut1.AsFloat:=R1000(teAlcoObrQOut1.AsFloat);
        teAlcoObrQOut2.AsFloat:=R1000(teAlcoObrQOut2.AsFloat);
        teAlcoObrQOut3.AsFloat:=R1000(teAlcoObrQOut3.AsFloat);
        teAlcoObrQOut4.AsFloat:=R1000(teAlcoObrQOut4.AsFloat);
        teAlcoObrQOutIt.AsFloat:=R1000(teAlcoObrQOutIt.AsFloat);

        teAlcoObrQe.AsFloat:=teAlcoObrQb.AsFloat-teAlcoObrQOutIt.AsFloat;
        teAlcoOb.Post;

        teAlcoOb.Next;
      end;

      teAlcoIn.First;
      while not teAlcoIn.Eof do
      begin
        teAlcoIn.Edit;
        teAlcoInrQIn.AsFloat:=R1000(teAlcoInrQIn.AsFloat);
        teAlcoIn.Post;

        par[0]:=teAlcoIniDep.AsInteger;
        par[1]:=teAlcoIniVid.AsInteger;
        par[2]:=teAlcoIniProd.AsInteger;

        if teAlcoOb.Locate('iDep;iVid;iProd',par,[]) then
        begin //���� ����� ������
          teAlcoOb.Edit;
          teAlcoObrQIn2.AsFloat:=teAlcoObrQIn2.AsFloat+teAlcoInrQIn.AsFloat;
          teAlcoObrQIn3.AsFloat:=0;
          teAlcoObrQIn4.AsFloat:=0;
          teAlcoObrQIn5.AsFloat:=0;
          teAlcoObrQIn6.AsFloat:=0;
          teAlcoObrQInIt.AsFloat:=teAlcoObrQInIt.AsFloat+teAlcoInrQIn.AsFloat;
          teAlcoObrQInIt1.AsFloat:=teAlcoObrQInIt1.AsFloat+teAlcoInrQIn.AsFloat;
          teAlcoObrQe.AsFloat:=teAlcoObrQb.AsFloat+teAlcoObrQInIt.AsFloat-teAlcoObrQOutIt.AsFloat;
          teAlcoOb.Post;
        end else
        begin
          teAlcoOb.Append;
          teAlcoObiDep.AsInteger:=teAlcoIniDep.AsInteger;
          teAlcoObsDep.AsString:=teAlcoInsDep.AsString;
          teAlcoObiVid.AsInteger:=teAlcoIniVid.AsInteger;
          teAlcoObsVid.AsString:=teAlcoIniVid.AsString;
          teAlcoObsVidName.AsString:=teAlcoInsVidName.AsString;
          teAlcoObiProd.AsInteger:=teAlcoIniProd.AsInteger;
          teAlcoObsProd.AsString:=teAlcoInsProd.AsString;
          teAlcoObsProdInn.AsString:=teAlcoInsProdInn.AsString;
          teAlcoObsProdKPP.AsString:=teAlcoInsProdKPP.AsString;
          teAlcoObrQb.AsFloat:=0;
          teAlcoObrQIn1.AsFloat:=0;
          teAlcoObrQIn2.AsFloat:=teAlcoInrQIn.AsFloat;
          teAlcoObrQIn3.AsFloat:=0;
          teAlcoObrQInIt1.AsFloat:=teAlcoInrQIn.AsFloat;
          teAlcoObrQIn4.AsFloat:=0;
          teAlcoObrQIn5.AsFloat:=0;
          teAlcoObrQIn6.AsFloat:=0;
          teAlcoObrQInIt.AsFloat:=teAlcoInrQIn.AsFloat;
          teAlcoObrQOut1.AsFloat:=0;
          teAlcoObrQOut2.AsFloat:=0;
          teAlcoObrQOut3.AsFloat:=0;
          teAlcoObrQOut4.AsFloat:=0;
          teAlcoObrQOutIt.AsFloat:=0;
          teAlcoObrQe.AsFloat:=teAlcoInrQIn.AsFloat;
          teAlcoOb.Post;
        end;

        teAlcoIn.Next;  inc(iC);
        if iC mod 100 = 0 then
        begin
          Memo1.Lines.Add('     '+its(iC)); delay(10);
        end;

      end;

      Memo1.Lines.Add('  - ������� ('+Its(teAlcoOb.RecordCount)+')'); delay(10);

      iC:=0;
      
      teAlcoOb.First;
      while not teAlcoOb.Eof do
      begin
        teAlcoOb.Edit;
        teAlcoObrQOutIt.AsFloat:=teAlcoObrQOut1.AsFloat+teAlcoObrQOut3.AsFloat;
        teAlcoObrQe.AsFloat:=teAlcoObrQb.AsFloat+teAlcoObrQInIt.AsFloat-(teAlcoObrQOut1.AsFloat+teAlcoObrQOut3.AsFloat);
        teAlcoOb.Post;

        if teAlcoObrQb.AsFloat<0 then //���� ������� �� ������ 0
        begin
          teAlcoOb.Edit;
          teAlcoObrQb.AsFloat:=0;
          teAlcoObrQe.AsFloat:=teAlcoObrQInIt.AsFloat-(teAlcoObrQOut1.AsFloat+teAlcoObrQOut3.AsFloat);
          teAlcoOb.Post;
        end;

        if teAlcoObrQe.AsFloat<0 then //���� ������� �� ����� 0
        begin
          teAlcoOb.Edit;
          teAlcoObrQe.AsFloat:=0;
          teAlcoObrQOut1.AsFloat:=teAlcoObrQb.AsFloat+teAlcoObrQInIt.AsFloat-teAlcoObrQOut3.AsFloat;
          teAlcoObrQOutIt.AsFloat:=teAlcoObrQOut1.AsFloat+teAlcoObrQOut3.AsFloat;
          teAlcoOb.Post;
        end;

        if teAlcoObrQOut1.AsFloat<0 then //���� ���������� �������������
        begin
          teAlcoOb.Edit;
          teAlcoObrQe.AsFloat:=teAlcoObrQe.AsFloat+teAlcoObrQOut1.AsFloat;
          teAlcoObrQOut1.AsFloat:=0;
          teAlcoObrQOutIt.AsFloat:=teAlcoObrQOut3.AsFloat;
          teAlcoOb.Post;
        end;

        teAlcoOb.Next;  inc(iC);
        if iC mod 100 = 0 then
        begin
          Memo1.Lines.Add('     '+its(iC)); delay(10);
        end;
      end;

      ViAlco1.EndUpdate;
      ViAlco2.EndUpdate;

      Memo1.Lines.Add('�������� ��.'); delay(10);

    end;
  end;
end;

procedure TfmAddAlco.acExpExlsExecute(Sender: TObject);
begin
//������� � ������
  prNExportExel5(ViAlco1);
  prNExportExel5(ViAlco2);
end;

procedure TfmAddAlco.acExpXMLExecute(Sender: TObject);
Var fName:String;
    f:Textfile;
    S:String;
    iShop,iVid,iProd,iPost:INteger;
    sOrg:TOrg;
    iNumVid,iNumProd,iNumPost:Integer;
    rSumVid:Real;

    StrN,StrGuid:String;
    a:TGuid;

  function sfmt(s1:String):String;
  Var Se:String;
  begin
    Se:=s1;
    Se:=Trim(Se);
    while pos('"',Se)>0 do delete(Se,pos('"',Se),1);
    Result:=Se;
  end;

  function rfmt(rSum:Real):String;
  Var Se:String;
  begin
    str(rSum:10:5,Se);
//    Se:=Se+'000';
    while pos(',',Se)>0 do Se[pos(',',Se)]:='.';
    while pos(' ',Se)>0 do delete(Se,pos(' ',Se),1);
    Result:=Se;
  end;

begin
  //������� � XML

  CreateGUID(a);
  StrGuid:=GUIDTostring(a);
  teAlcoOb.First;
  if cxComboBox1.ItemIndex=0 then StrN:='R1_' else StrN:='R2_';

  sOrg:=prGetOrg(cxLookupComboBox1.EditValue,teAlcoObiDep.AsInteger);
  StrN:=StrN+sOrg.Inn+'003_'+formatdatetime('ddmmyyyy',date)+'_'+StrGuid+'.xml';
  while pos('{',StrN)>0 do delete(StrN,pos('{',StrN),1);
  while pos('}',StrN)>0 do delete(StrN,pos('}',StrN),1);

//  Memo1.Lines.Add(StrN);

  fName:=StrN;
  OpenDialog1.InitialDir:=CurDir;
  OpenDialog1.fileName:=StrN;

  if OpenDialog1.Execute then
  begin
    Memo1.Clear;
    fName:=OpenDialog1.fileName;
    Memo1.Lines.Add('������� � XML � ���� - '+fName); delay(10);
    Memo1.Lines.Add('   - ���������� ������'); delay(10);

    ViAlco1.BeginUpdate;
    ViAlco2.BeginUpdate;

    Memo1.Lines.Add('       �������������.'); delay(10);
    CloseTe(teProds);
    teAlcoOb.First;
    while not teAlcoOb.Eof do
    begin
      if teProds.Locate('iProd',teAlcoObiProd.AsInteger,[])=False then
      begin
        teProds.Append;
        teProdsiProd.AsInteger:=teAlcoObiProd.AsInteger;
        teProdssProd.AsString:=sfmt(teAlcoObsProd.AsString);
        teProdssProdInn.AsString:=sfmt(teAlcoObsProdInn.AsString);
        teProdssProdKPP.AsString:=sfmt(teAlcoObsProdKPP.AsString);
        teProds.Post;
      end;
      teAlcoOb.Next;
    end;

    Memo1.Lines.Add('       ����������.'); delay(10);
    CloseTe(tePost);
    teAlcoIn.First;
    while not teAlcoIn.Eof do
    begin
      if tePost.Locate('iPost',teAlcoIniPost.AsInteger,[])=False then
      begin
        tePost.Append;
        tePostiPost.AsInteger:=teAlcoIniPost.AsInteger;
        tePostsPost.AsString:=sfmt(teAlcoInsPost.AsString);
        tePostsPostInn.AsString:=sfmt(teAlcoInsPostInn.AsString);
        tePostsPostKPP.AsString:=sfmt(teAlcoInsPostKPP.AsString);
        tePostiLic.AsInteger:=teAlcoIniLic.AsInteger;
        tePostLicNumber.AsString:=sfmt(teAlcoInsLicNumber.AsString);
        tePostLicDateB.AsDateTime:=teAlcoInLicDateB.AsDateTime;
        tePostLicDateE.AsDateTime:=teAlcoInLicDateE.AsDateTime;
        tePostLicOrg.AsString:=sfmt(teAlcoInLicOrg.AsString);
        tePost.Post;
      end;
      teAlcoIn.Next;
    end;

    Memo1.Lines.Add('       ��������.'); delay(10);
    CloseTe(teLic);
    teAlcoIn.First;
    while not teAlcoIn.Eof do
    begin
      if tePost.Locate('iLic',teAlcoIniLic.AsInteger,[])=False then
      begin
//        Memo1.Lines.Add('�������� '+teAlcoIniLic.AsString+' '+teAlcoIniPost.AsString); delay(10);
        if teLic.Locate('iLic',teAlcoIniLic.AsInteger,[])=False then
        begin
          teLic.Append;
          teLiciPost.AsInteger:=teAlcoIniPost.AsInteger;
          teLiciLic.AsInteger:=teAlcoIniLic.AsInteger;
          teLicLicNumber.AsString:=teAlcoInsLicNumber.AsString;
          teLicLicNumber.AsString:=sfmt(teAlcoInsLicNumber.AsString);
          teLicLicDateB.AsDateTime:=teAlcoInLicDateB.AsDateTime;
          teLicLicDateE.AsDateTime:=teAlcoInLicDateE.AsDateTime;
          teLicLicOrg.AsString:=sfmt(teAlcoInLicOrg.AsString);
          teLic.Post;
        end;
      end;

      teAlcoIn.Next;
    end;

    Memo1.Lines.Add('       ������..'); delay(10);

    CloseTa(teA1);
    CloseTa(teA2);

    teAlcoIn.First;
    while not teAlcoIn.Eof do
    begin
      teA2.Append;
      teA2iDep.AsInteger:=teAlcoIniDep.AsInteger;
      teA2iVid.AsInteger:=teAlcoIniVid.AsInteger;
      teA2iProd.AsInteger:=teAlcoIniProd.AsInteger;
      teA2iPost.AsInteger:=teAlcoIniPost.AsInteger;
      teA2iLic.AsInteger:=teAlcoIniLic.AsInteger;
      teA2DocDate.AsDateTime:=teAlcoInDocDate.AsDateTime;
      teA2DocNum.AsString:=teAlcoInDocNum.AsString;
      teA2GTD.AsString:=teAlcoInGTD.AsString;
      teA2rQIn.AsFloat:=teAlcoInrQIn.AsFloat;
      teA2.Post;

      teAlcoIn.Next;
    end;

    Memo1.Lines.Add('       ������.'); delay(10);

    teAlcoOb.First;
    while not teAlcoOb.Eof do
    begin
      teA1.Append;
      teA1iDep.AsInteger:=teAlcoObiDep.AsInteger;
      teA1sDep.AsString:=teAlcoObsDep.AsString;
      teA1iVid.AsInteger:=teAlcoObiVid.AsInteger;
      teA1sVidName.AsString:=teAlcoObsVidName.AsString;
      teA1iProd.AsInteger:=teAlcoObiProd.AsInteger;
      teA1rQb.AsFloat:=teAlcoObrQb.AsFloat;
      teA1rQIn1.AsFloat:=teAlcoObrQIn1.AsFloat;
      teA1rQIn2.AsFloat:=teAlcoObrQIn2.AsFloat;
      teA1rQIn3.AsFloat:=teAlcoObrQIn3.AsFloat;
      teA1rQInIt1.AsFloat:=teAlcoObrQInIt1.AsFloat;
      teA1rQIn4.AsFloat:=teAlcoObrQIn4.AsFloat;
      teA1rQIn5.AsFloat:=teAlcoObrQIn5.AsFloat;
      teA1rQIn6.AsFloat:=teAlcoObrQIn6.AsFloat;
      teA1rQInIt.AsFloat:=teAlcoObrQInIt.AsFloat;
      teA1rQOut1.AsFloat:=teAlcoObrQOut1.AsFloat;
      teA1rQOut2.AsFloat:=teAlcoObrQOut2.AsFloat;
      teA1rQOut3.AsFloat:=teAlcoObrQOut3.AsFloat;
      teA1rQOut4.AsFloat:=teAlcoObrQOut4.AsFloat;
      teA1rQOutIt.AsFloat:=teAlcoObrQOutIt.AsFloat;
      teA1rQe.AsFloat:=teAlcoObrQe.AsFloat;
      teA1.Post;

      teAlcoOb.Next;
    end;


    ViAlco1.EndUpdate;
    ViAlco2.EndUpdate;

    Memo1.Lines.Add('   - ���������� ������'); delay(10);

    assignfile(f,fName);
    rewrite(f);
    try
      Memo1.Lines.Add('     �����������'); delay(10);

      S:='<?xml version="1.0" encoding="windows-1251"?>';
      writeln(f,s);

      S:='<���� �������="'+ds1(date)+'" ��������="4.20" ��������="��������� ���� ������ 4.20.24">';
      writeln(f,s);

      if cxComboBox1.ItemIndex=0 then
      begin
        S:='	<�������� �������="11-�" �������������="'+its(cxSpinEdit1.Value)+'" ��������="4" ������������="2012">';
        writeln(f,s);
      end else
      begin
        S:='	<�������� �������="12-�" �������������="'+its(cxSpinEdit1.Value)+'" ��������="4" ������������="2012">';
        writeln(f,s);
      end;

      if cxComboBox2.ItemIndex=0 then S:='		<��������� />' else
      begin
        S:='		<�������������� ���������="'+its(cxSpinEdit1.Value)+'" />'; //����� ��� ����� ���������
      end;
      writeln(f,s);

      S:='	</��������>';
      writeln(f,s);

      S:='	<�����������>'; //������ ������������
      writeln(f,s);

      teProds.First;
      while not teProds.Eof do
      begin
        if cxComboBox1.ItemIndex=0 then
        begin
          S:='		<���������������������� �����������="'+its(teProdsiProd.AsInteger)+'" �000000000004="'+teProdssProd.AsString+'" �000000000005="'+teProdssProdInn.AsString+'" �000000000006="'+teProdssProdKPP.AsString+'" />';
          writeln(f,s);
        end else
        begin
          S:='		<���������������������� �����������="'+its(teProdsiProd.AsInteger)+'" �000000000004="'+teProdssProd.AsString+'">';
          writeln(f,s);
          if pos('�� ',teProdssProd.AsString)=1 then
          begin
            S:='			<�� �000000000005="'+teProdssProdInn.AsString+'" />';
          end else
          begin
            S:='			<�� �000000000005="'+teProdssProdInn.AsString+'" �000000000006="'+teProdssProdKPP.AsString+'" />';
          end;
          writeln(f,s);
          S:='		</����������������������>';
          writeln(f,s);
        end;

        teProds.Next;
      end;

      tePost.First;
      while not tePost.Eof do
      begin
        if cxComboBox1.ItemIndex=0 then
        begin
          S:='		<���������� ��������="'+its(tePostiPost.AsInteger)+'" �000000000007="'+tePostsPost.AsString+'">';
          writeln(f,s);

          S:='			<��������>';   writeln(f,s);

          S:='				<�������� ����������="'+its(tePostiLic.AsInteger)+'" �000000000011="'+tePostLicNumber.AsString+'" �000000000012="'+FormatDateTime('dd.mm.yyyy',tePostLicDateB.AsDateTime)+'" �000000000013="'+FormatDateTime('dd.mm.yyyy',tePostLicDateE.AsDateTime)+'" �000000000014="'+tePostLicOrg.AsString+'" />';
          writeln(f,s);

          S:='			</��������>'; writeln(f,s);

          teLic.First;
          while not teLic.Eof do
          begin
            if teLiciPost.AsInteger=tePostiPost.AsInteger then
            begin
              S:='			<��������>';   writeln(f,s);
              S:='				<�������� ����������="'+its(teLiciLic.AsInteger)+'" �000000000011="'+teLicLicNumber.AsString+'" �000000000012="'+FormatDateTime('dd.mm.yyyy',teLicLicDateB.AsDateTime)+'" �000000000013="'+FormatDateTime('dd.mm.yyyy',teLicLicDateE.AsDateTime)+'" �000000000014="'+teLicLicOrg.AsString+'" />';
              writeln(f,s);
              S:='			</��������>'; writeln(f,s);

              teLic.Delete;
            end else teLic.Next;
          end;


          S:='			<�� �000000000009="'+tePostsPostInn.AsString+'" �000000000010="'+tePostsPostKpp.AsString+'" />';
          writeln(f,s);

          S:='		</����������>'; writeln(f,s);
        end else
        begin
          S:='		<���������� ��������="'+its(tePostiPost.AsInteger)+'" �000000000007="'+tePostsPost.AsString+'">';  writeln(f,s);
          S:='			<�� �000000000009="'+tePostsPostInn.AsString+'" �000000000010="'+tePostsPostKpp.AsString+'" />'; writeln(f,s);
          S:='		</����������>'; writeln(f,s);
        end;
        tePost.Next;
      end;

      S:='	</�����������>'; //����� ������������
      writeln(f,s);

      Memo1.Lines.Add('     ������'); delay(10);

      S:='	<��������>';
      writeln(f,s);

      iShop:=0; iVid:=0; iProd:=0;
      teA1.First;

      sOrg:=prGetOrg(cxLookupComboBox1.EditValue,teA1iDep.AsInteger);

      if cxComboBox1.ItemIndex=0 then
      begin
        S:='		<�����������>'; writeln(f,s);
        S:='			<��������� ������="'+sfmt(sOrg.FullName)+'" �����="'+sOrg.Inn+'" �����="'+sOrg.KPP+'" ������="'+sOrg.sTel+'" Email����="'+sOrg.sMail+'">'; writeln(f,s);
        S:='				<������>'; writeln(f,s);
        S:='					<���������>'+sOrg.CodeCountry+'</���������>'; writeln(f,s);
        S:='					<������>620000</������>'; writeln(f,s);
        S:='					<���������>'+sOrg.CodeReg+'</���������>'; writeln(f,s);
        S:='					<����� />'; writeln(f,s);
        S:='					<�����>������������ �</�����>'; writeln(f,s);
        S:='					<����������> </����������>'; writeln(f,s);
        S:='					<�����>'+sOrg.Street+' ��</�����>'; writeln(f,s);
        S:='					<���>'+sOrg.House+'</���>'; writeln(f,s);
        S:='					<������ />'; writeln(f,s);
        if Length(sOrg.Korp)>0 then
        begin
          S:='					<������>'+sOrg.Korp+'</������>'; writeln(f,s);
        end else
        begin
          S:='					<������ />'; writeln(f,s);
        end;
        S:='					<����� />'; writeln(f,s);
        S:='				</������>'; writeln(f,s);
        S:='			</���������>'; writeln(f,s);
        S:='			<���������>'; writeln(f,s);
        S:='				<������������>'; writeln(f,s);
        S:='					<�������>'+sOrg.Dir1+'</�������> '; writeln(f,s);
        S:='					<���>'+sOrg.Dir2+'</���>'; writeln(f,s);
        S:='					<��������>'+sOrg.Dir3+'</��������>'; writeln(f,s);
        S:='				</������������>'; writeln(f,s);
        S:='				<�������>'; writeln(f,s);
        S:='					<�������>'+sOrg.gb1+'</�������>'; writeln(f,s);
        S:='					<���>'+sOrg.gb2+'</���>'; writeln(f,s);
        S:='					<��������>'+sOrg.gb3+'</��������>'; writeln(f,s);
        S:='				</�������>'; writeln(f,s);
        S:='			</���������>'; writeln(f,s);
        S:='			<��������>'; writeln(f,s);
        S:='				<�������� �������="14" ������="'+sOrg.LicSer+'" ��������="'+sOrg.LicNum+'" ����������="'+ds1(sOrg.dDateB)+'" �����������="'+ds1(sOrg.dDateE)+'" />'; writeln(f,s);
        S:='			</��������>'; writeln(f,s);
        S:='		</�����������>'; writeln(f,s);
      end else
      begin
        S:='		<�����������>'; writeln(f,s);
        S:='			<��������� �������="'+sfmt(sOrg.FullName)+'" ������="'+sOrg.sTel+'" Email����="'+sOrg.sMail+'">'; writeln(f,s);
        S:='				<������>'; writeln(f,s);
        S:='					<���������>'+sOrg.CodeCountry+'</���������>'; writeln(f,s);
        S:='					<������>620000</������>'; writeln(f,s);
        S:='					<���������>'+sOrg.CodeReg+'</���������>'; writeln(f,s);
        S:='					<����� />'; writeln(f,s);
        S:='					<�����>������������ �</�����>'; writeln(f,s);
        S:='					<����������> </����������>'; writeln(f,s);
        S:='					<�����>'+sOrg.Street+'</�����>'; writeln(f,s);
        S:='					<���>'+sOrg.House+'</���>'; writeln(f,s);
        S:='					<������ />'; writeln(f,s);
        if Length(sOrg.Korp)>0 then
        begin
          S:='					<������>'+sOrg.Korp+'</������>'; writeln(f,s);
        end else
        begin
          S:='					<������ />'; writeln(f,s);
        end;
        S:='					<����� />'; writeln(f,s);
        S:='				</������>'; writeln(f,s);

        if sOrg.ip=0 then
        begin
          S:='				<�� �����="'+sOrg.Inn+'" �����="'+sOrg.KPP+'" />'; writeln(f,s);
        end else
        begin
          S:='				<�� �����="'+sOrg.Inn+'"/>'; writeln(f,s);
        end;

//        S:='				<�� �����="'+sOrg.Inn+'" �����="'+sOrg.KPP+'" />'; writeln(f,s);
        S:='			</���������>'; writeln(f,s);
        S:='			<���������>'; writeln(f,s);
        S:='				<������������>'; writeln(f,s);
        S:='					<�������>'+sOrg.Dir1+'</�������> '; writeln(f,s);
        S:='					<���>'+sOrg.Dir2+'</���>'; writeln(f,s);
        S:='					<��������>'+sOrg.Dir3+'</��������>'; writeln(f,s);
        S:='				</������������>'; writeln(f,s);
        S:='				<�������>'; writeln(f,s);
        S:='					<�������>'+sOrg.gb1+'</�������>'; writeln(f,s);
        S:='					<���>'+sOrg.gb2+'</���>'; writeln(f,s);
        S:='					<��������>'+sOrg.gb3+'</��������>'; writeln(f,s);
        S:='				</�������>'; writeln(f,s);
        S:='			</���������>'; writeln(f,s);
        S:='		</�����������>'; writeln(f,s);
      end;

      iNumVid:=1;
      iNumProd:=1;
      while not teA1.Eof do
      begin
        if iShop<>teA1iDep.AsInteger then
        begin
          if iProd>0 then
          begin
            S:='				</����������������>'; writeln(f,s);
//            S:='               ������������� ����� - '+its(iProd); writeln(f,s);
          end;

          if iVid>0 then
          begin
//            S:='         ��� ����� - '+its(iVid)+';';  writeln(f,s);

            S:='			</������>'; writeln(f,s);
          end;

          if iShop>0 then
          begin
//            S:='   ������� ����� - '+its(iShop)+';';    writeln(f,s);
            S:='		</������������>'; writeln(f,s);
          end;

//          S:='   ������� ������ - '+teA1iDep.AsString+';'+teA1sDep.AsString;  writeln(f,s);
          iShop:=teA1iDep.AsInteger;
          sOrg:=prGetOrg(cxLookupComboBox1.EditValue,teA1iDep.AsInteger);

          if sOrg.ip=0 then
          begin
             S:='		<������������ ������="'+sfmt(sOrg.sAdr)+'" �����="'+sOrg.KPP+'" ��������������="true">'; writeln(f,s);
          end else
          begin
             S:='		<������������ ������="'+sfmt(sOrg.sAdr)+'" ��������������="true">'; writeln(f,s);
          end;

//          S:='		<������������ ������="'+sfmt(sOrg.sAdr)+'" �����="'+sOrg.KPP+'" ��������������="true">'; writeln(f,s);

          S:='			<������>'; writeln(f,s);
          S:='				<���������>'+sOrg.CodeCountry+'</���������>'; writeln(f,s);
          S:='				<������>620000</������>'; writeln(f,s);
          S:='				<���������>'+sOrg.CodeReg+'</���������>'; writeln(f,s);
          S:='				<����� />'; writeln(f,s);
          S:='				<�����>������������ �</�����>'; writeln(f,s);
          S:='				<����������> </����������>'; writeln(f,s);
          S:='				<�����>'+sOrg.Street+' ��</�����>'; writeln(f,s);
          S:='				<���>'+sOrg.House+'</���>'; writeln(f,s);
          if sOrg.Korp>'' then
          begin
            S:='				<������>'+sOrg.Korp+'</������>'; writeln(f,s);
          end else
          begin
            S:='				<������ />'; writeln(f,s);
          end;
          S:='				<������ />'; writeln(f,s);
          S:='				<����� />'; writeln(f,s);
          S:='			</������>'; writeln(f,s);

          //���������� ���������
          iVid:=0;
          iProd:=0;

          iNumVid:=1;
          iNumProd:=1;
        end;
        if iVid<>teA1iVid.AsInteger then
        begin
          if iProd>0 then
          begin
//            S:='               ������������� ����� - '+its(iProd); writeln(f,s);
            S:='				</����������������>'; writeln(f,s);
          end;

          if iVid>0 then
          begin
//            S:='         ��� ����� - '+its(iVid)+';'; writeln(f,s);
            S:='			</������>'; writeln(f,s);
          end;

//          S:='         ��� ������ - '+teA1iVid.AsString+';'+teA1sVidName.AsString; writeln(f,s);
          S:='			<������ �N="'+its(iNumVid)+'" �000000000003="'+its(teA1iVid.AsInteger)+'">'; writeln(f,s);


          iVid:=teA1iVid.AsInteger;
          //���������� ���������
          iProd:=0;

          iNumProd:=1;

          inc(iNumVid);
        end;
        if iProd<>teA1iProd.AsInteger then
        begin
          if iProd>0 then
          begin
//            S:='               ������������� ����� - '+its(iProd); writeln(f,s);
            S:='				</����������������>'; writeln(f,s);
          end;

//          S:='               ������������� ������ - '+teA1iProd.AsString; writeln(f,s);
          S:='				<���������������� �N="'+its(iNumProd)+'" �����������="'+its(teA1iProd.AsInteger)+'">'; writeln(f,s);

          iProd:=teA1iProd.AsInteger;

        end;

        iNumPost:=1;
        iPost:=0;
        rSumVid:=0;

        teA2.First;
        while not teA2.Eof do
        begin
//          S:='                      ��������� - '+teA2iDep.AsString+';'+teA2iVid.AsString+';'+teA2iProd.AsString+';'+teA2iPost.AsString+';'+teA2iLic.AsString+';'+teA2DocDate.AsString+';'+teA2DocNum.AsString+';'+teA2GTD.AsString+';'+fs(teA2rQIn.AsFloat);         writeln(f,s);
          if iPost<>teA2iPost.AsInteger then
          begin
            if iPost>0 then //����� �������
            begin
              S:='					</���������>'; writeln(f,s);
            end;
            //������ ������
            if cxComboBox1.ItemIndex=0 then
            begin
              S:='					<��������� �N="'+its(iNumPost)+'" ������������="'+its(teA2iPost.AsInteger)+'" ����������="'+its(teA2iLic.AsInteger)+'">';  writeln(f,s);
            end else
            begin
              S:='					<��������� �N="'+its(iNumPost)+'" ������������="'+its(teA2iPost.AsInteger)+'" >';  writeln(f,s);
            end;

            iPost:=teA2iPost.AsInteger;

            inc(iNumPost);
          end;

          // ����� ��������
//          S:='						<��������� �200000000013="'+ds1(teA2DocDate.AsDateTime)+'" �200000000014="'+teA2DocNum.AsString+'" �200000000015="'+teA2GTD.AsString+'" �200000000016="'+rfmt(teA2rQIn.AsFloat)+'" />'; writeln(f,s);
          S:='						<��������� �200000000013="'+ds1(teA2DocDate.AsDateTime)+'" �200000000014="'+teA2DocNum.AsString+'" �200000000015="" �200000000016="'+rfmt(teA2rQIn.AsFloat)+'" />'; writeln(f,s);
          rSumVid:=rSumVid+teA2rQIn.AsFloat;

          teA2.Next;
        end;;
        if iPost>0 then
        begin
          S:='					</���������>'; writeln(f,s);
        end;

        if abs(rSumVid-teA1rQIn2.AsFloat)>0.000001 then
        begin
          Memo1.Lines.Add('   ������ �����������: ��-�� '+rfmt(teA1rQIn2.AsFloat)+' �� ���������� '+rfmt(rSumVid)); delay(10);
          Memo1.Lines.Add('   '+its(iShop)+';'+its(iVid)+';'+its(iProd)); delay(10);
        end;

//        S:='   '+teA1iDep.AsString+';'+teA1sDep.AsString+';'+teA1iVid.AsString+';'+teA1sVidName.AsString+';'+teA1iProd.AsString+' - ������.';
//        writeln(f,s);

        if cxComboBox1.ItemIndex=0 then
        begin
          S:='					<�������� �N="'+its(iNumProd);
          S:=S+'" �100000000006="'+rfmt(teA1rQb.AsFloat)+'"'; //�������
          S:=S+' �100000000007="0.00000"';
          S:=S+' �100000000008="'+rfmt(teA1rQIn2.AsFloat)+'"'; //������
          S:=S+' �100000000009="0.00000"';
          S:=S+' �100000000010="'+rfmt(teA1rQInIt1.AsFloat)+'"';  //������ ��
          S:=S+' �100000000011="0.00000"';
          S:=S+' �100000000012="0.00000"';
          S:=S+' �100000000013="0.00000"';
          S:=S+' �100000000014="'+rfmt(teA1rQInIt.AsFloat)+'"'; //������ �����
          S:=S+' �100000000015="'+rfmt(teA1rQOut1.AsFloat)+'"'; //����������
          S:=S+' �100000000016="'+rfmt(teA1rQOut2.AsFloat)+'"';
          S:=S+' �100000000017="'+rfmt(teA1rQOut3.AsFloat)+'"'; //�������
          S:=S+' �100000000018="0.00000"';
          S:=S+' �100000000019="'+rfmt(teA1rQOutIt.AsFloat)+'"'; //������ �����
          S:=S+' �100000000020="'+rfmt(teA1rQe.AsFloat)+'" />';  //������� �� �����
          writeln(f,s);
        end else
        begin
          S:='					<�������� �N="'+its(iNumProd);
          S:=S+'" �100000000006="'+rfmt(teA1rQb.AsFloat)+'"'; //�������
          S:=S+' �100000000007="0.00000"';
          S:=S+' �100000000008="'+rfmt(teA1rQIn2.AsFloat)+'"'; //������
          S:=S+' �100000000009="0.00000"';
          S:=S+' �100000000010="'+rfmt(teA1rQInIt1.AsFloat)+'"';  //������ ��
          S:=S+' �100000000011="0.00000"';
          S:=S+' �100000000012="0.00000"';
          S:=S+' �100000000013="'+rfmt(teA1rQInIt.AsFloat)+'"'; //������ �����
          S:=S+' �100000000014="'+rfmt(teA1rQOut1.AsFloat)+'"'; //����������
          S:=S+' �100000000015="'+rfmt(teA1rQOut2.AsFloat)+'"';
          S:=S+' �100000000016="'+rfmt(teA1rQOut3.AsFloat)+'"'; //�������
          S:=S+' �100000000017="'+rfmt(teA1rQOutIt.AsFloat)+'"'; //������ �����
          S:=S+' �100000000018="'+rfmt(teA1rQe.AsFloat)+'" />';  //������� �� �����
          writeln(f,s);
        end;

        teA1.Next;   inc(iNumProd);
      end;
      if iProd>0 then
      begin
//        S:='               ������������� ����� - '+its(iProd); writeln(f,s);

        S:='				</����������������>'; writeln(f,s);
      end;
      if iVid>0 then
      begin
//        S:='         ��� ����� - '+its(iVid)+';';  writeln(f,s);
        S:='			</������>'; writeln(f,s);
      end;
      if iShop>0 then
      begin
//        S:='   ������� ����� - '+its(iShop)+';';
        S:='		</������������>'; writeln(f,s);
      end;

      S:='	</��������>';
      writeln(f,s);
      S:='</����>';
      writeln(f,s);

    finally
      Memo1.Lines.Add('������������ ���������.'); delay(10);
      teProds.Active:=False;
      tePost.Active:=False;
      teLic.Active:=False;
      closefile(f);
    end;
  end;
end;


procedure TfmAddAlco.acExpXMLoldExecute(Sender: TObject);
Var fName:String;
    f:Textfile;
    S:String;
    iShop,iVid,iProd,iPost:INteger;
    sOrg:TOrg;
    iNumVid,iNumProd,iNumPost:Integer;
    rSumVid:Real;

    StrN,StrGuid:String;
    a:TGuid;
    iKv,iM:INteger;
    sKv,sY:String;

  function sfmt(s1:String):String;
  Var Se:String;
  begin
    Se:=s1;
    Se:=Trim(Se);
    while pos('"',Se)>0 do delete(Se,pos('"',Se),1);
    Result:=Se;
  end;

  function rfmt(rSum:Real):String;
  Var Se:String;
  begin
    str(rSum:10:5,Se);
//    Se:=Se+'000';
    while pos(',',Se)>0 do Se[pos(',',Se)]:='.';
    while pos(' ',Se)>0 do delete(Se,pos(' ',Se),1);
    Result:=Se;
  end;

begin
  //������� � XML
  iM:=fDateToKvartal(Trunc(cxDateEdit2.Date));
  iKv:=0;
  if (iM>=1) and (iM<=3) then iKv:=3;
  if (iM>=4) and (iM<=6) then iKv:=6;
  if (iM>=7) and (iM<=9) then iKv:=9;
  if (iM>=10) and (iM<=12) then iKv:=0;

  sKv:=Its(iKv); sKv:='0'+sKv;
  sY:=FormatDateTime('yyyy',cxDateEdit2.Date);
  while length(sY)>1 do delete(sY,1,1);

  CreateGUID(a);
  StrGuid:=GUIDTostring(a);
  teAlcoOb.First;
  if cxComboBox1.ItemIndex=0 then StrN:='R1_' else StrN:='R2_';

  sOrg:=prGetOrg(cxLookupComboBox1.EditValue,teAlcoObiDep.AsInteger);
  StrN:=StrN+sOrg.Inn+'_'+sKv+sY+'_'+formatdatetime('ddmmyyyy',date)+'_'+StrGuid+'.xml';
  while pos('{',StrN)>0 do delete(StrN,pos('{',StrN),1);
  while pos('}',StrN)>0 do delete(StrN,pos('}',StrN),1);

//  Memo1.Lines.Add(StrN);

  fName:=StrN;
  OpenDialog1.InitialDir:=CurDir;
  OpenDialog1.fileName:=StrN;

  if OpenDialog1.Execute then
  begin
    Memo1.Clear;
    fName:=OpenDialog1.fileName;
    Memo1.Lines.Add('������� � XML � ���� - '+fName); delay(10);
    Memo1.Lines.Add('   - ���������� ������'); delay(10);

    ViAlco1.BeginUpdate;
    ViAlco2.BeginUpdate;

    Memo1.Lines.Add('       �������������.'); delay(10);
    CloseTe(teProds);
    teAlcoOb.First;
    while not teAlcoOb.Eof do
    begin
      if teProds.Locate('iProd',teAlcoObiProd.AsInteger,[])=False then
      begin
        teProds.Append;
        teProdsiProd.AsInteger:=teAlcoObiProd.AsInteger;
        teProdssProd.AsString:=sfmt(teAlcoObsProd.AsString);
        teProdssProdInn.AsString:=sfmt(teAlcoObsProdInn.AsString);
        teProdssProdKPP.AsString:=sfmt(teAlcoObsProdKPP.AsString);
        teProds.Post;
      end;
      teAlcoOb.Next;
    end;

    Memo1.Lines.Add('       ����������.'); delay(10);
    CloseTe(tePost);
    teAlcoIn.First;
    while not teAlcoIn.Eof do
    begin
      if tePost.Locate('iPost',teAlcoIniPost.AsInteger,[])=False then
      begin
        tePost.Append;
        tePostiPost.AsInteger:=teAlcoIniPost.AsInteger;
        tePostsPost.AsString:=sfmt(teAlcoInsPost.AsString);
        tePostsPostInn.AsString:=sfmt(teAlcoInsPostInn.AsString);
        tePostsPostKPP.AsString:=sfmt(teAlcoInsPostKPP.AsString);
        tePostiLic.AsInteger:=teAlcoIniLic.AsInteger;
        tePostLicNumber.AsString:=sfmt(teAlcoInsLicNumber.AsString);
        tePostLicDateB.AsDateTime:=teAlcoInLicDateB.AsDateTime;
        tePostLicDateE.AsDateTime:=teAlcoInLicDateE.AsDateTime;
        tePostLicOrg.AsString:=sfmt(teAlcoInLicOrg.AsString);
        tePost.Post;
      end;
      teAlcoIn.Next;
    end;

    Memo1.Lines.Add('       ��������.'); delay(10);
    CloseTe(teLic);
    teAlcoIn.First;
    while not teAlcoIn.Eof do
    begin
      if tePost.Locate('iLic',teAlcoIniLic.AsInteger,[])=False then
      begin
//        Memo1.Lines.Add('�������� '+teAlcoIniLic.AsString+' '+teAlcoIniPost.AsString); delay(10);
        if teLic.Locate('iLic',teAlcoIniLic.AsInteger,[])=False then
        begin
          teLic.Append;
          teLiciPost.AsInteger:=teAlcoIniPost.AsInteger;
          teLiciLic.AsInteger:=teAlcoIniLic.AsInteger;
          teLicLicNumber.AsString:=teAlcoInsLicNumber.AsString;
          teLicLicNumber.AsString:=sfmt(teAlcoInsLicNumber.AsString);
          teLicLicDateB.AsDateTime:=teAlcoInLicDateB.AsDateTime;
          teLicLicDateE.AsDateTime:=teAlcoInLicDateE.AsDateTime;
          teLicLicOrg.AsString:=sfmt(teAlcoInLicOrg.AsString);
          teLic.Post;
        end;  
      end;

      teAlcoIn.Next;
    end;

    Memo1.Lines.Add('       ������..'); delay(10);

    CloseTa(teA1);
    CloseTa(teA2);

    teAlcoIn.First;
    while not teAlcoIn.Eof do
    begin
      teA2.Append;
      teA2iDep.AsInteger:=teAlcoIniDep.AsInteger;
      teA2iVid.AsInteger:=teAlcoIniVid.AsInteger;
      teA2iProd.AsInteger:=teAlcoIniProd.AsInteger;
      teA2iPost.AsInteger:=teAlcoIniPost.AsInteger;
      teA2iLic.AsInteger:=teAlcoIniLic.AsInteger;
      teA2DocDate.AsDateTime:=teAlcoInDocDate.AsDateTime;
      teA2DocNum.AsString:=teAlcoInDocNum.AsString;
      teA2GTD.AsString:=teAlcoInGTD.AsString;
      teA2rQIn.AsFloat:=teAlcoInrQIn.AsFloat;
      teA2.Post;

      teAlcoIn.Next;
    end;

    Memo1.Lines.Add('       ������.'); delay(10);

    teAlcoOb.First;
    while not teAlcoOb.Eof do
    begin
      teA1.Append;
      teA1iDep.AsInteger:=teAlcoObiDep.AsInteger;
      teA1sDep.AsString:=teAlcoObsDep.AsString;
      teA1iVid.AsInteger:=teAlcoObiVid.AsInteger;
      teA1sVidName.AsString:=teAlcoObsVidName.AsString;
      teA1iProd.AsInteger:=teAlcoObiProd.AsInteger;
      teA1rQb.AsFloat:=teAlcoObrQb.AsFloat;
      teA1rQIn1.AsFloat:=teAlcoObrQIn1.AsFloat;
      teA1rQIn2.AsFloat:=teAlcoObrQIn2.AsFloat;
      teA1rQIn3.AsFloat:=teAlcoObrQIn3.AsFloat;
      teA1rQInIt1.AsFloat:=teAlcoObrQInIt1.AsFloat;
      teA1rQIn4.AsFloat:=teAlcoObrQIn4.AsFloat;
      teA1rQIn5.AsFloat:=teAlcoObrQIn5.AsFloat;
      teA1rQIn6.AsFloat:=teAlcoObrQIn6.AsFloat;
      teA1rQInIt.AsFloat:=teAlcoObrQInIt.AsFloat;
      teA1rQOut1.AsFloat:=teAlcoObrQOut1.AsFloat;
      teA1rQOut2.AsFloat:=teAlcoObrQOut2.AsFloat;
      teA1rQOut3.AsFloat:=teAlcoObrQOut3.AsFloat;
      teA1rQOut4.AsFloat:=teAlcoObrQOut4.AsFloat;
      teA1rQOutIt.AsFloat:=teAlcoObrQOutIt.AsFloat;
      teA1rQe.AsFloat:=teAlcoObrQe.AsFloat;
      teA1.Post;

      teAlcoOb.Next;
    end;


    ViAlco1.EndUpdate;
    ViAlco2.EndUpdate;

    Memo1.Lines.Add('   - ���������� ������'); delay(10);

    assignfile(f,fName);
    rewrite(f);
    try
      Memo1.Lines.Add('     �����������'); delay(10);

      S:='<?xml version="1.0" encoding="windows-1251"?>';
      writeln(f,s);

      S:='<���� �������="'+ds1(date)+'" ��������="4.30" ��������="��������� ���� ������ 4.30.03">';
      writeln(f,s);

      if cxComboBox1.ItemIndex=0 then
      begin
        S:=#9+'<�������� �������="11" �������������="'+its(iKv)+'" ������������="'+FormatDateTime('yyyy',cxDateEdit2.Date)+'">';
        writeln(f,s);
      end else
      begin
        S:=#9+'<�������� �������="12" �������������="'+its(iKv)+'" ������������="'+FormatDateTime('yyyy',cxDateEdit2.Date)+'">';
        writeln(f,s);
      end;

      if cxComboBox2.ItemIndex=0 then S:=#9+#9+'<��������� />' else
      begin
//        S:='		<�������������� />'; //����� ��� ����� ���������
        S:=#9+#9+'<�������������� ���������="'+its(cxSpinEdit1.Value)+'" />'; //����� ��� ����� ���������
      end;
      writeln(f,s);

      S:=#9+'</��������>';
      writeln(f,s);

      S:=#9+'<�����������>'; //������ ������������
      writeln(f,s);

      teProds.First;
      while not teProds.Eof do
      begin
        if cxComboBox1.ItemIndex=0 then
        begin
          S:='		<���������������������� �����������="'+its(teProdsiProd.AsInteger)+'" �000000000004="'+teProdssProd.AsString+'" �000000000005="'+teProdssProdInn.AsString+'" �000000000006="'+teProdssProdKPP.AsString+'" />';
          writeln(f,s);
        end else
        begin
          S:='		<���������������������� �����������="'+its(teProdsiProd.AsInteger)+'" �000000000004="'+teProdssProd.AsString+'">';
          writeln(f,s);
          if pos('�� ',teProdssProd.AsString)=1 then
          begin
            S:='			<�� �000000000005="'+teProdssProdInn.AsString+'" />';
          end else
          begin
            S:='			<�� �000000000005="'+teProdssProdInn.AsString+'" �000000000006="'+teProdssProdKPP.AsString+'" />';
          end;
          writeln(f,s);
          S:='		</����������������������>';
          writeln(f,s);
        end;

        teProds.Next;
      end;

      tePost.First;
      while not tePost.Eof do
      begin
        if cxComboBox1.ItemIndex=0 then
        begin
          S:=#9+#9+'<���������� ��������="'+its(tePostiPost.AsInteger)+'" �000000000007="'+tePostsPost.AsString+'">';
          writeln(f,s);

          S:=#9+#9+#9+'<��������>';   writeln(f,s);

          S:=#9+#9+#9+#9+'<�������� ����������="'+its(tePostiLic.AsInteger)+'" �000000000011="'+tePostLicNumber.AsString+'" �000000000012="'+FormatDateTime('dd.mm.yyyy',tePostLicDateB.AsDateTime)+'" �000000000013="'+FormatDateTime('dd.mm.yyyy',tePostLicDateE.AsDateTime)+'" �000000000014="'+tePostLicOrg.AsString+'" />';
          writeln(f,s);

          S:=#9+#9+#9+'</��������>'; writeln(f,s);

          teLic.First;
          while not teLic.Eof do
          begin
            if teLiciPost.AsInteger=tePostiPost.AsInteger then
            begin
              S:=#9+#9+#9+'<��������>';   writeln(f,s);
              S:=#9+#9+#9+#9+'<�������� ����������="'+its(teLiciLic.AsInteger)+'" �000000000011="'+teLicLicNumber.AsString+'" �000000000012="'+FormatDateTime('dd.mm.yyyy',teLicLicDateB.AsDateTime)+'" �000000000013="'+FormatDateTime('dd.mm.yyyy',teLicLicDateE.AsDateTime)+'" �000000000014="'+teLicLicOrg.AsString+'" />';
              writeln(f,s);
              S:=#9+#9+#9+'</��������>'; writeln(f,s);

              teLic.Delete;
            end else teLic.Next;
          end;


          S:=#9+#9+#9+'<�� �000000000009="'+tePostsPostInn.AsString+'" �000000000010="'+tePostsPostKpp.AsString+'" />';
          writeln(f,s);

          S:=#9+#9+'</����������>'; writeln(f,s);
        end else
        begin
          S:=#9+#9+'<���������� ��������="'+its(tePostiPost.AsInteger)+'" �000000000007="'+tePostsPost.AsString+'">';  writeln(f,s);
          S:=#9+#9+#9+'<�� �000000000009="'+tePostsPostInn.AsString+'" �000000000010="'+tePostsPostKpp.AsString+'" />'; writeln(f,s);
          S:=#9+#9+'</����������>'; writeln(f,s);
        end;
        tePost.Next;
      end;

      S:=#9+'</�����������>'; //����� ������������
      writeln(f,s);

      Memo1.Lines.Add('     ������'); delay(10);

      S:=#9+'<��������>';
      writeln(f,s);

      iShop:=0; iVid:=0; iProd:=0;
      teA1.First;

      sOrg:=prGetOrg(cxLookupComboBox1.EditValue,teA1iDep.AsInteger);

      if cxComboBox1.ItemIndex=0 then
      begin
        S:=#9+#9+'<�����������>'; writeln(f,s);
        S:=#9+#9+#9+'<��������� ������="'+sfmt(sOrg.FullName)+'" �����="'+sOrg.Inn+'" �����="'+sOrg.KPP+'" ������="'+sOrg.sTel+'" Email����="'+sOrg.sMail+'">'; writeln(f,s);
        S:=#9+#9+#9+#9+'<������>'; writeln(f,s);
        S:=#9+#9+#9+#9+#9+'<���������>'+sOrg.CodeCountry+'</���������>'; writeln(f,s);
        S:=#9+#9+#9+#9+#9+'<������>620000</������>'; writeln(f,s);
        S:=#9+#9+#9+#9+#9+'<���������>'+sOrg.CodeReg+'</���������>'; writeln(f,s);
        S:=#9+#9+#9+#9+#9+'<����� />'; writeln(f,s);
        S:=#9+#9+#9+#9+#9+'<�����>������������ �</�����>'; writeln(f,s);
        S:=#9+#9+#9+#9+#9+'<����������> </����������>'; writeln(f,s);
        S:=#9+#9+#9+#9+#9+'<�����>'+sOrg.Street+' ��</�����>'; writeln(f,s);
        S:=#9+#9+#9+#9+#9+'<���>'+sOrg.House+'</���>'; writeln(f,s);
        S:=#9+#9+#9+#9+#9+'<������ />'; writeln(f,s);
        if Length(sOrg.Korp)>0 then
        begin
          S:=#9+#9+#9+#9+#9+'<������>'+sOrg.Korp+'</������>'; writeln(f,s);
        end else
        begin
          S:=#9+#9+#9+#9+#9+'<������ />'; writeln(f,s);
        end;
        S:=#9+#9+#9+#9+#9+'<����� />'; writeln(f,s);
        S:=#9+#9+#9+#9+'</������>'; writeln(f,s);
        S:=#9+#9+#9+'</���������>'; writeln(f,s);
        S:=#9+#9+#9+'<���������>'; writeln(f,s);
        S:=#9+#9+#9+#9+'<������������>'; writeln(f,s);
        S:=#9+#9+#9+#9+#9+'<�������>'+sOrg.Dir1+'</�������> '; writeln(f,s);
        S:=#9+#9+#9+#9+#9+'<���>'+sOrg.Dir2+'</���>'; writeln(f,s);
        S:=#9+#9+#9+#9+#9+'<��������>'+sOrg.Dir3+'</��������>'; writeln(f,s);
        S:=#9+#9+#9+#9+'</������������>'; writeln(f,s);
        S:=#9+#9+#9+#9+'<�������>'; writeln(f,s);
        S:=#9+#9+#9+#9+#9+'<�������>'+sOrg.gb1+'</�������>'; writeln(f,s);
        S:=#9+#9+#9+#9+#9+'<���>'+sOrg.gb2+'</���>'; writeln(f,s);
        S:=#9+#9+#9+#9+#9+'<��������>'+sOrg.gb3+'</��������>'; writeln(f,s);
        S:=#9+#9+#9+#9+'</�������>'; writeln(f,s);
        S:=#9+#9+#9+'</���������>'; writeln(f,s);
        S:=#9+#9+#9+'<������������>'; writeln(f,s);
        S:=#9+#9+#9+#9+'<�������������>'; writeln(f,s);
        S:=#9+#9+#9+#9+#9+'<�������� �������="14" ���������="'+sOrg.LicSer+' '+sOrg.LicNum+'" ����������="'+ds1(sOrg.dDateB)+'" �����������="'+ds1(sOrg.dDateE)+'" />'; writeln(f,s);
        S:=#9+#9+#9+#9+'</�������������>'; writeln(f,s);
        S:=#9+#9+#9+'</������������>'; writeln(f,s);

        S:=#9+#9+'</�����������>'; writeln(f,s);
      end else
      begin
        S:=#9+#9+'<�����������>'; writeln(f,s);
        S:=#9+#9+#9+'<��������� �������="'+sfmt(sOrg.FullName)+'" ������="'+sOrg.sTel+'" Email����="'+sOrg.sMail+'">'; writeln(f,s);
        S:=#9+#9+#9+#9+'<������>'; writeln(f,s);
        S:=#9+#9+#9+#9+#9+'<���������>'+sOrg.CodeCountry+'</���������>'; writeln(f,s);
        S:=#9+#9+#9+#9+#9+'<������>620000</������>'; writeln(f,s);
        S:=#9+#9+#9+#9+#9+'<���������>'+sOrg.CodeReg+'</���������>'; writeln(f,s);
        S:=#9+#9+#9+#9+#9+'<����� />'; writeln(f,s);
        S:=#9+#9+#9+#9+#9+'<�����>������������ �</�����>'; writeln(f,s);
        S:=#9+#9+#9+#9+#9+'<����������> </����������>'; writeln(f,s);
        S:=#9+#9+#9+#9+#9+'<�����>'+sOrg.Street+'</�����>'; writeln(f,s);
        S:=#9+#9+#9+#9+#9+'<���>'+sOrg.House+'</���>'; writeln(f,s);
        S:=#9+#9+#9+#9+#9+'<������ />'; writeln(f,s);
        if Length(sOrg.Korp)>0 then
        begin
          S:=#9+#9+#9+#9+#9+'<������>'+sOrg.Korp+'</������>'; writeln(f,s);
        end else
        begin
          S:=#9+#9+#9+#9+#9+'<������ />'; writeln(f,s);
        end;
        S:=#9+#9+#9+#9+#9+'<����� />'; writeln(f,s);
        S:=#9+#9+#9+#9+'</������>'; writeln(f,s);

        if sOrg.ip=0 then
        begin
          S:=#9+#9+#9+#9+'<�� �����="'+sOrg.Inn+'" �����="'+sOrg.KPP+'" />'; writeln(f,s);
        end else
        begin
          S:=#9+#9+#9+#9+'<�� �����="'+sOrg.Inn+'"/>'; writeln(f,s);
        end;

        S:=#9+#9+#9+'</���������>'; writeln(f,s);
        S:=#9+#9+#9+'<���������>'; writeln(f,s);
        S:=#9+#9+#9+#9+'<������������>'; writeln(f,s);
        S:=#9+#9+#9+#9+#9+'<�������>'+sOrg.Dir1+'</�������> '; writeln(f,s);
        S:=#9+#9+#9+#9+#9+'<���>'+sOrg.Dir2+'</���>'; writeln(f,s);
        S:=#9+#9+#9+#9+#9+'<��������>'+sOrg.Dir3+'</��������>'; writeln(f,s);
        S:=#9+#9+#9+#9+'</������������>'; writeln(f,s);
        S:=#9+#9+#9+#9+'<�������>'; writeln(f,s);
        S:=#9+#9+#9+#9+#9+'<�������>'+sOrg.gb1+'</�������>'; writeln(f,s);
        S:=#9+#9+#9+#9+#9+'<���>'+sOrg.gb2+'</���>'; writeln(f,s);
        S:=#9+#9+#9+#9+#9+'<��������>'+sOrg.gb3+'</��������>'; writeln(f,s);
        S:=#9+#9+#9+#9+'</�������>'; writeln(f,s);
        S:=#9+#9+#9+'</���������>'; writeln(f,s);
        S:=#9+#9+'</�����������>'; writeln(f,s);
      end;

      iNumVid:=1;
      iNumProd:=1;
      while not teA1.Eof do
      begin
        if iShop<>teA1iDep.AsInteger then
        begin
          if iProd>0 then
          begin
            S:=#9+#9+#9+#9+'</����������������>'; writeln(f,s);
//            S:='               ������������� ����� - '+its(iProd); writeln(f,s);
          end;

          if iVid>0 then
          begin
//            S:='         ��� ����� - '+its(iVid)+';';  writeln(f,s);

            S:=#9+#9+#9+'</������>'; writeln(f,s);
          end;

          if iShop>0 then
          begin
//            S:='   ������� ����� - '+its(iShop)+';';    writeln(f,s);
            S:=#9+#9+'</������������>'; writeln(f,s);
          end;

//          S:='   ������� ������ - '+teA1iDep.AsString+';'+teA1sDep.AsString;  writeln(f,s);
          iShop:=teA1iDep.AsInteger;
          sOrg:=prGetOrg(cxLookupComboBox1.EditValue,teA1iDep.AsInteger);

          if sOrg.ip=0 then
          begin
             S:=#9+#9+'<������������ ������="'+sfmt(sOrg.sAdr)+'" �����="'+sOrg.KPP+'" ��������������="true">'; writeln(f,s);
          end else
          begin
             S:=#9+#9+'<������������ ������="'+sfmt(sOrg.sAdr)+'" ��������������="true">'; writeln(f,s);
          end;

          S:=#9+#9+#9+'<������>'; writeln(f,s);
          S:=#9+#9+#9+#9+'<���������>'+sOrg.CodeCountry+'</���������>'; writeln(f,s);
          S:=#9+#9+#9+#9+'<������>620000</������>'; writeln(f,s);
          S:=#9+#9+#9+#9+'<���������>'+sOrg.CodeReg+'</���������>'; writeln(f,s);
          S:=#9+#9+#9+#9+'<����� />'; writeln(f,s);
          S:=#9+#9+#9+#9+'<�����>������������ �</�����>'; writeln(f,s);
          S:=#9+#9+#9+#9+'<����������> </����������>'; writeln(f,s);
          S:=#9+#9+#9+#9+'<�����>'+sOrg.Street+' ��</�����>'; writeln(f,s);
          S:=#9+#9+#9+#9+'<���>'+sOrg.House+'</���>'; writeln(f,s);
          if sOrg.Korp>'' then
          begin
            S:=#9+#9+#9+#9+'<������>'+sOrg.Korp+'</������>'; writeln(f,s);
          end else
          begin
            S:=#9+#9+#9+#9+'<������ />'; writeln(f,s);
          end;
          S:=#9+#9+#9+#9+'<������ />'; writeln(f,s);
          S:=#9+#9+#9+#9+'<����� />'; writeln(f,s);
          S:=#9+#9+#9+'</������>'; writeln(f,s);

          //���������� ���������
          iVid:=0;
          iProd:=0;

          iNumVid:=1;
          iNumProd:=1;
        end;
        if iVid<>teA1iVid.AsInteger then
        begin
          if iProd>0 then
          begin
//            S:='               ������������� ����� - '+its(iProd); writeln(f,s);
            S:=#9+#9+#9+#9+'</����������������>'; writeln(f,s);
          end;

          if iVid>0 then
          begin
//            S:='         ��� ����� - '+its(iVid)+';'; writeln(f,s);
            S:=#9+#9+#9+'</������>'; writeln(f,s);
          end;

//          S:='         ��� ������ - '+teA1iVid.AsString+';'+teA1sVidName.AsString; writeln(f,s);
          S:=#9+#9+#9+'<������ �N="'+its(iNumVid)+'" �000000000003="'+its(teA1iVid.AsInteger)+'">'; writeln(f,s);


          iVid:=teA1iVid.AsInteger;
          //���������� ���������
          iProd:=0;

          iNumProd:=1;

          inc(iNumVid);
        end;
        if iProd<>teA1iProd.AsInteger then
        begin
          if iProd>0 then
          begin
//            S:='               ������������� ����� - '+its(iProd); writeln(f,s);
            S:=#9+#9+#9+#9+'</����������������>'; writeln(f,s);
          end;

//          S:='               ������������� ������ - '+teA1iProd.AsString; writeln(f,s);
          S:=#9+#9+#9+#9+'<���������������� �N="'+its(iNumProd)+'" �����������="'+its(teA1iProd.AsInteger)+'">'; writeln(f,s);

          iProd:=teA1iProd.AsInteger;

        end;

        iNumPost:=1;
        iPost:=0;
        rSumVid:=0;

        teA2.First;
        while not teA2.Eof do
        begin
//          S:='                      ��������� - '+teA2iDep.AsString+';'+teA2iVid.AsString+';'+teA2iProd.AsString+';'+teA2iPost.AsString+';'+teA2iLic.AsString+';'+teA2DocDate.AsString+';'+teA2DocNum.AsString+';'+teA2GTD.AsString+';'+fs(teA2rQIn.AsFloat);         writeln(f,s);
          if iPost<>teA2iPost.AsInteger then
          begin
            if iPost>0 then //����� �������
            begin
              S:=#9+#9+#9+#9+#9+'</���������>'; writeln(f,s);
            end;
            //������ ������
            if cxComboBox1.ItemIndex=0 then
            begin
              S:=#9+#9+#9+#9+#9+'<��������� �N="'+its(iNumPost)+'" ������������="'+its(teA2iPost.AsInteger)+'" ����������="'+its(teA2iLic.AsInteger)+'">';  writeln(f,s);
            end else
            begin
              S:=#9+#9+#9+#9+#9+'<��������� �N="'+its(iNumPost)+'" ������������="'+its(teA2iPost.AsInteger)+'" >';  writeln(f,s);
            end;

            iPost:=teA2iPost.AsInteger;

            inc(iNumPost);
          end;

          // ����� ��������
//          S:='						<��������� �200000000013="'+ds1(teA2DocDate.AsDateTime)+'" �200000000014="'+teA2DocNum.AsString+'" �200000000015="'+teA2GTD.AsString+'" �200000000016="'+rfmt(teA2rQIn.AsFloat)+'" />'; writeln(f,s);
          S:=#9+#9+#9+#9+#9+#9+'<��������� �200000000013="'+ds1(teA2DocDate.AsDateTime)+'" �200000000014="'+teA2DocNum.AsString+'" �200000000015="" �200000000016="'+rfmt(teA2rQIn.AsFloat)+'" />'; writeln(f,s);
          rSumVid:=rSumVid+teA2rQIn.AsFloat;

          teA2.Next;
        end;;
        if iPost>0 then
        begin
          S:=#9+#9+#9+#9+#9+'</���������>'; writeln(f,s);
        end;

        if abs(rSumVid-teA1rQIn2.AsFloat)>0.000001 then
        begin
          Memo1.Lines.Add('   ������ �����������: ��-�� '+rfmt(teA1rQIn2.AsFloat)+' �� ���������� '+rfmt(rSumVid)); delay(10);
          Memo1.Lines.Add('   '+its(iShop)+';'+its(iVid)+';'+its(iProd)); delay(10);
        end;

//        S:='   '+teA1iDep.AsString+';'+teA1sDep.AsString+';'+teA1iVid.AsString+';'+teA1sVidName.AsString+';'+teA1iProd.AsString+' - ������.';
//        writeln(f,s);

        if cxComboBox1.ItemIndex=0 then
        begin
          S:=#9+#9+#9+#9+#9+'<�������� �N="'+its(iNumProd);
          S:=S+'" �100000000006="'+rfmt(teA1rQb.AsFloat)+'"'; //�������
          S:=S+' �100000000007="0.00000"';
          S:=S+' �100000000008="'+rfmt(teA1rQIn2.AsFloat)+'"'; //������
          S:=S+' �100000000009="0.00000"';
          S:=S+' �100000000010="'+rfmt(teA1rQInIt1.AsFloat)+'"';  //������ ��
          S:=S+' �100000000011="0.00000"';
          S:=S+' �100000000012="0.00000"';
          S:=S+' �100000000013="0.00000"';
          S:=S+' �100000000014="'+rfmt(teA1rQInIt.AsFloat)+'"'; //������ �����
          S:=S+' �100000000015="'+rfmt(teA1rQOut1.AsFloat)+'"'; //����������
          S:=S+' �100000000016="0.00000"';
          S:=S+' �100000000017="'+rfmt(teA1rQOut3.AsFloat)+'"'; //�������
          S:=S+' �100000000018="0.00000"';
          S:=S+' �100000000019="'+rfmt(teA1rQOutIt.AsFloat)+'"'; //������ �����
          S:=S+' �100000000020="'+rfmt(teA1rQe.AsFloat)+'" />';  //������� �� �����
          writeln(f,s);
        end else
        begin
          S:=#9+#9+#9+#9+#9+'<�������� �N="'+its(iNumProd);
          S:=S+'" �100000000006="'+rfmt(teA1rQb.AsFloat)+'"'; //�������
          S:=S+' �100000000007="0.00000"';
          S:=S+' �100000000008="'+rfmt(teA1rQIn2.AsFloat)+'"'; //������
          S:=S+' �100000000009="0.00000"';
          S:=S+' �100000000010="'+rfmt(teA1rQInIt1.AsFloat)+'"';  //������ ��
          S:=S+' �100000000011="0.00000"';
          S:=S+' �100000000012="0.00000"';
          S:=S+' �100000000013="'+rfmt(teA1rQInIt.AsFloat)+'"'; //������ �����
          S:=S+' �100000000014="'+rfmt(teA1rQOut1.AsFloat)+'"'; //����������
          S:=S+' �100000000015="0.00000"';
          S:=S+' �100000000016="'+rfmt(teA1rQOut3.AsFloat)+'"'; //�������
          S:=S+' �100000000017="'+rfmt(teA1rQOutIt.AsFloat)+'"'; //������ �����
          S:=S+' �100000000018="'+rfmt(teA1rQe.AsFloat)+'" />';  //������� �� �����
          writeln(f,s);
        end;

        teA1.Next;   inc(iNumProd);
      end;
      if iProd>0 then
      begin
//        S:='               ������������� ����� - '+its(iProd); writeln(f,s);

        S:=#9+#9+#9+#9+'</����������������>'; writeln(f,s);
      end;
      if iVid>0 then
      begin
//        S:='         ��� ����� - '+its(iVid)+';';  writeln(f,s);
        S:=#9+#9+#9+'</������>'; writeln(f,s);
      end;
      if iShop>0 then
      begin
//        S:='   ������� ����� - '+its(iShop)+';';
        S:=#9+#9+'</������������>'; writeln(f,s);
      end;

      S:=#9+'</��������>';
      writeln(f,s);
      S:='</����>';
      writeln(f,s);

    finally
      Memo1.Lines.Add('������������ ���������.'); delay(10);
      teProds.Active:=False;
      tePost.Active:=False;
      teLic.Active:=False;
      closefile(f);
    end;
  end;
end;

procedure TfmAddAlco.acGenGUIDExecute(Sender: TObject);
Var StrN,StrGuid:String;
    sOrg:TOrg;
    a:TGuid;

begin
  CreateGUID(a);
  StrGuid:=GUIDTostring(a);
  teAlcoOb.First;
  StrN:='R1_';
  sOrg:=prGetOrg(cxLookupComboBox1.EditValue,teAlcoObiDep.AsInteger);
  StrN:=StrN+sOrg.Inn+'_003_'+formatdatetime('ddmmyyyy',date)+'_'+StrGuid+'.xml';
  while pos('{',StrN)>0 do delete(StrN,pos('{',StrN),1);
  while pos('}',StrN)>0 do delete(StrN,pos('}',StrN),1);

  Memo1.Lines.Add(StrN);
end;

procedure TfmAddAlco.acImportXLSExecute(Sender: TObject);
{var
    ExcelApp, Workbook, ISheet: Variant;
    iVid:INteger;
    sProdINN,sProdKPP:String;
    sPostINN,sPostKPP:String;
    rQb,rQIn,rQOut:Real;
    i:Integer;
    iProd,iPost:INteger;
    sProd,sPost:String;
    sDateDoc,sNUmDoc,sGTD:String;
    dDateDoc:TDateTime;
    iLic:INteger;
    sNum,sOrg:String;
    LicDB,LicDE:TDateTime;
    par:Variant;
    sProdEx,sPostEx:String;}
begin
// ������� �� ��
{  fmAlcoImport.cxButtonEdit1.Text:='';
  fmAlcoImport.ShowModal;
  if fmAlcoImport.ModalResult=mrOk then
  begin
    delay(10);
    memo1.Clear;
    ExcelApp := CreateOleObject('Excel.Application');
    ExcelApp.Application.EnableEvents := false;
    Workbook := ExcelApp.WorkBooks.Add(fmAlcoImport.cxButtonEdit1.Text);
    ISheet := Workbook.Worksheets.Item[1];

    i:=fmAlcoImport.cxSpinEdit1.Value;

    if fmAlcoImport.cxRadioGroup1.ItemIndex=0 then
    begin
      Memo1.Lines.Add('��������� ����������� ���������� (11-1)'); delay(10);

      ViAlco1.BeginUpdate;

      While String(ISheet.Cells.Item[i, 1].Value)<>'' do
      begin
        iVid:=StrToINtDef(String(ISheet.Cells.Item[i,3].Value),0);
        sProdEx:=String(ISheet.Cells.Item[i,4].Value);
        sProdINN:=String(ISheet.Cells.Item[i,5].Value);
        sProdKPP:=String(ISheet.Cells.Item[i,6].Value);
        rQb:=StrToFloatDef(String(ISheet.Cells.Item[i,7].Value),0);
        rQOut:=StrToFloatDef(String(ISheet.Cells.Item[i,16].Value),0);

        if prFindProd(sProdINN,sProdKPP,iProd,sProd)=False then
        begin
          Memo1.Lines.Add(' ������ ����������� ������������� ��� '+sProdINN+'  ���'+sProdKPP+' '+sProdEx); delay(10);
        end;

        teAlcoOb.Append;
        teAlcoObiDep.AsInteger:=999;
        teAlcoObsDep.AsString:='';
        teAlcoObiVid.AsInteger:=iVid;
        teAlcoObsVid.AsString:=its(iVid);
        teAlcoObsVidName.AsString:='';
        teAlcoObiProd.AsInteger:=iProd;
        teAlcoObsProd.AsString:=sProd;
        teAlcoObsProdInn.AsString:=sProdINN;
        teAlcoObsProdKPP.AsString:=sProdKpp;
        teAlcoObrQb.AsFloat:=rQb;

        teAlcoObrQIn1.AsFloat:=0;
        teAlcoObrQIn2.AsFloat:=0;
        teAlcoObrQIn3.AsFloat:=0;
        teAlcoObrQInIt1.AsFloat:=0;
        teAlcoObrQIn4.AsFloat:=0;
        teAlcoObrQIn5.AsFloat:=0;
        teAlcoObrQIn6.AsFloat:=0;
        teAlcoObrQInIt.AsFloat:=0;

        teAlcoObrQOut1.AsFloat:=rQOut;
        teAlcoObrQOut2.AsFloat:=0;
        teAlcoObrQOut3.AsFloat:=0;
        teAlcoObrQOut4.AsFloat:=0;
        teAlcoObrQOutIt.AsFloat:=rQOut;

        teAlcoObrQe.AsFloat:=rQb-rQout;
        teAlcoOb.Post;

        inc(i);
      end;
      ViAlco1.EndUpdate;
    end;
    if fmAlcoImport.cxRadioGroup1.ItemIndex=1 then
    begin
      ViAlco1.BeginUpdate;
      ViAlco2.BeginUpdate;

      par := VarArrayCreate([0,2], varInteger);

      Memo1.Lines.Add('��������� ����������� ���������� (11-2)'); delay(10);

      While String(ISheet.Cells.Item[i, 1].Value)<>'' do
      begin
        iVid:=StrToINtDef(String(ISheet.Cells.Item[i,3].Value),0);
        sProdEx:=String(ISheet.Cells.Item[i,4].Value);
        sProdINN:=String(ISheet.Cells.Item[i,5].Value);
        sProdKPP:=String(ISheet.Cells.Item[i,6].Value);

        sPostEx:=String(ISheet.Cells.Item[i,7].Value);
        sPostINN:=String(ISheet.Cells.Item[i,8].Value);
        sPostKPP:=String(ISheet.Cells.Item[i,9].Value);


        sDateDoc:=String(ISheet.Cells.Item[i,14].Value);
        sNUmDoc:=String(ISheet.Cells.Item[i,15].Value);
        sGTD:=String(ISheet.Cells.Item[i,16].Value);

        rQIn:=StrToFloatDef(String(ISheet.Cells.Item[i,17].Value),0);

        if prFindProd(sProdINN,sProdKPP,iProd,sProd)=False then
        begin
          Memo1.Lines.Add(' ������ ����������� ������������� ��� '+sProdINN+'  ���'+sProdKPP+' '+sProdEx); delay(10);
        end;

        if prFindPost(sPostINN,sPostKPP,iPost,sPost)=False then
        begin
          Memo1.Lines.Add(' ������ ����������� ���������� ��� '+sPostINN+'  ���'+sPostKPP+' '+sPostEx); delay(10);
        end;

        dDateDoc:=StrToDateDef(sDateDoc,date);

        if prFindLic(iPost,dDateDoc,iLic,sNum,sOrg,LicDB,LicDE)=False then
        begin
          Memo1.Lines.Add(' �� ����� �������� ���������� ��� '+sPostINN+'  ���'+sPostKPP+' '+sPostEx); delay(10);
        end;

        teAlcoIn.Append;
        teAlcoIniDep.AsInteger:=999;
        teAlcoInsDep.AsString:='';
        teAlcoIniVid.AsInteger:=iVid;
        teAlcoInsVid.AsString:=its(iVid);
        teAlcoInsVidName.AsString:='';
        teAlcoIniProd.AsInteger:=iProd;
        teAlcoInsProd.AsString:=sProd;
        teAlcoInsProdInn.AsString:=sProdINN;
        teAlcoInsProdKPP.AsString:=sProdKPP;
        teAlcoIniPost.AsInteger:=iPost;
        teAlcoInsPost.AsString:=sPost;
        teAlcoInsPostInn.AsString:=sPostInn;
        teAlcoInsPostKpp.AsString:=sPostKPP;
        teAlcoIniLic.AsInteger:=iLIc;
        teAlcoInsLicNumber.AsString:=sNum;
        teAlcoInLicDateB.AsDateTime:=LicDB;
        teAlcoInLicDateE.AsDateTime:=LicDE;
        teAlcoInLicOrg.AsString:=sOrg;
        teAlcoInDocDate.AsDateTime:=dDateDoc;
        teAlcoInDocNum.AsString:=sNUmDoc;
        teAlcoInGTD.AsString:=sGTD;
        teAlcoInrQIn.AsFloat:=rQIn;
        teAlcoIn.Post;

        par[0]:=999;
        par[1]:=iVid;
        par[2]:=iProd;

        if teAlcoOb.Locate('iDep;iVid;iProd',par,[]) then
        begin //���� ����� ������
          teAlcoOb.Edit;
          teAlcoObrQIn2.AsFloat:=teAlcoObrQIn2.AsFloat+rQIn;
          teAlcoObrQIn3.AsFloat:=0;
          teAlcoObrQIn4.AsFloat:=0;
          teAlcoObrQIn5.AsFloat:=0;
          teAlcoObrQIn6.AsFloat:=0;
          teAlcoObrQInIt.AsFloat:=teAlcoObrQInIt.AsFloat+rQIn;
          teAlcoObrQInIt1.AsFloat:=teAlcoObrQInIt1.AsFloat+rQIn;
          teAlcoObrQe.AsFloat:=teAlcoObrQb.AsFloat+teAlcoObrQInIt.AsFloat-teAlcoObrQOutIt.AsFloat;
          teAlcoOb.Post;
        end else
        begin
          teAlcoOb.Append;
          teAlcoObiDep.AsInteger:=999;
          teAlcoObsDep.AsString:='';
          teAlcoObiVid.AsInteger:=iVid;
          teAlcoObsVid.AsString:=its(iVid);
          teAlcoObsVidName.AsString:='';
          teAlcoObiProd.AsInteger:=iProd;
          teAlcoObsProd.AsString:=sProd;
          teAlcoObsProdInn.AsString:=sProdInn;
          teAlcoObsProdKPP.AsString:=sProdKPP;
          teAlcoObrQb.AsFloat:=0;
          teAlcoObrQIn1.AsFloat:=0;
          teAlcoObrQIn2.AsFloat:=rQIn;
          teAlcoObrQIn3.AsFloat:=0;
          teAlcoObrQInIt1.AsFloat:=rQIn;
          teAlcoObrQIn4.AsFloat:=0;
          teAlcoObrQIn5.AsFloat:=0;
          teAlcoObrQIn6.AsFloat:=0;
          teAlcoObrQInIt.AsFloat:=rQIn;
          teAlcoObrQOut1.AsFloat:=0;
          teAlcoObrQOut2.AsFloat:=0;
          teAlcoObrQOut3.AsFloat:=0;
          teAlcoObrQOut4.AsFloat:=0;
          teAlcoObrQOutIt.AsFloat:=0;
          teAlcoObrQe.AsFloat:=rQIn;
          teAlcoOb.Post;
        end;

        inc(i);
      end;

      ViAlco1.EndUpdate;
      ViAlco2.EndUpdate;
    end;

    Memo1.Lines.Add('�������� ���������');
    ExcelApp.Quit;
    ExcelApp:=Unassigned;
  end;}
end;

procedure TfmAddAlco.acGetRemn2Execute(Sender: TObject);
Var iOrg,iType,iDateE:INteger;
    iC,iVb,iVe,iMax,iStep:INteger;
    kVol,rQb:Real;
    par:Variant;
begin
  // �� ��������� ����
  with dmORep do
  begin
    with fmAddAlco do
    begin
      Memo1.Lines.Add('  ������� �� ������..'); delay(10);

      iType:=cxComboBox1.ItemIndex;
      iOrg:=cxLookupComboBox1.EditValue;
      iDateE:=Trunc(cxDateEdit1.Date)-1;

      quFindDH.Active:=False;
      quFindDH.ParamByName('IORG').AsInteger:=iOrg;
      quFindDH.ParamByName('ITYPE').AsInteger:=iType;
      quFindDH.ParamByName('IDATEE').AsInteger:=iDateE;
      quFindDH.Active:=True;
      if quFindDH.RecordCount>0 then
      begin
        Memo1.Lines.Add('  ������� ������� ('+its(quFindDHId.AsInteger)+')'); delay(10);
        Memo1.Lines.Add('    - ������������ '); delay(10);

        ViAlco1.BeginUpdate;

        quAlcoDS1.Active:=False;
        quAlcoDS1.ParamByName('IDH').AsInteger:=quFindDHId.AsInteger;
        quAlcoDS1.Active:=True;

        quAlcoDS1.First;
        while not quAlcoDS1.Eof do
        begin
          with fmAddAlco do
          begin
            teAlcoOb.Append;
            teAlcoObiDep.AsInteger:=quAlcoDS1iDep.AsInteger;
            teAlcoObsDep.AsString:=quAlcoDS1NameShop.AsString;
            teAlcoObiVid.AsInteger:=quAlcoDS1iVid.AsInteger;
            teAlcoObsVid.AsString:=quAlcoDS1iVid.AsString;
            teAlcoObsVidName.AsString:=quAlcoDS1NameVid.AsString;
            teAlcoObiProd.AsInteger:=quAlcoDS1iProd.AsInteger;
            teAlcoObsProd.AsString:=quAlcoDS1NameProducer.AsString;
            teAlcoObsProdInn.AsString:=quAlcoDS1INN.AsString;
            teAlcoObsProdKPP.AsString:=quAlcoDS1KPP.AsString;
            teAlcoObrQb.AsFloat:=quAlcoDS1rQe.AsFloat;
            teAlcoObrQIn1.AsFloat:=0;
            teAlcoObrQIn2.AsFloat:=0;
            teAlcoObrQIn3.AsFloat:=0;
            teAlcoObrQInIt1.AsFloat:=0;
            teAlcoObrQIn4.AsFloat:=0;
            teAlcoObrQIn5.AsFloat:=0;
            teAlcoObrQIn6.AsFloat:=0;
            teAlcoObrQInIt.AsFloat:=0;
            teAlcoObrQOut1.AsFloat:=0;
            teAlcoObrQOut2.AsFloat:=0;
            teAlcoObrQOut3.AsFloat:=0;
            teAlcoObrQOut4.AsFloat:=0;
            teAlcoObrQOutIt.AsFloat:=0;
            teAlcoObrQe.AsFloat:=quAlcoDS1rQe.AsFloat;
            teAlcoOb.Post;
          end;
          quAlcoDS1.Next;
        end;
        quAlcoDS1.Active:=False;

        ViAlco1.EndUpdate;

      end else
      begin
        Memo1.Lines.Add('  ����������� �������.'); delay(10);

        iOrg:=cxLookupComboBox1.EditValue;

        iC:=0;
        par := VarArrayCreate([0,2], varInteger);

        if cxComboBox1.ItemIndex=0 then begin iVb:=0; iVe:=499; end
        else begin iVb:=500; iVe:=999; end;

        PBar1.Position:=0;
        PBar1.Visible:=True;

        quCardsAlg.Active:=False;
        quCardsAlg.Active:=True;

        iMax:=quCardsAlg.RecordCount;
        iStep:=iMax div 100;
        if iStep=0 then iStep:=1;

        Memo1.Lines.Add('   ����� '+its(iMax)); Delay(10);

        ViAlco1.BeginUpdate;

        quCardsAlg.First;
        while not quCardsAlg.Eof do
        begin
          if (quCardsAlgALGCLASS.AsInteger>=iVb)and(quCardsAlgALGCLASS.AsInteger<=iVe) then
          begin
            kVol:=quCardsAlgVOL.AsFloat;
            rQb:=r1000(prCalcRemn(quCardsAlgID.AsInteger,Trunc(cxDateEdit1.Date)-1,iOrg)*kVol/10); //������� �� �����

            if rQb>0 then //������� ���� �����-�� �������������
            begin
              par[0]:=iOrg;
              par[1]:=quCardsAlgALGCLASS.AsInteger;
              par[2]:=quCardsAlgALGMAKER.AsInteger;
              if teAlcoOb.Locate('iDep;iVid;iProd',par,[]) then
              begin //�����������
                teAlcoOb.Edit;
                teAlcoObrQb.AsFloat:=teAlcoObrQb.AsFloat+rQb;
                teAlcoObrQe.AsFloat:=teAlcoObrQb.AsFloat+teAlcoObrQInIt.AsFloat-teAlcoObrQOutIt.AsFloat;
                teAlcoOb.Post;
              end else
              begin  //��������� ������
                teAlcoOb.Append;
                teAlcoObiDep.AsInteger:=iOrg;
                teAlcoObsDep.AsString:=cxLookupComboBox1.Text;
                teAlcoObiVid.AsInteger:=quCardsAlgALGCLASS.AsInteger;
                teAlcoObsVid.AsString:=quCardsAlgALGCLASS.AsString;
                teAlcoObsVidName.AsString:=quCardsAlgNAMECLA.AsString;
                teAlcoObiProd.AsInteger:=quCardsAlgALGMAKER.AsInteger;
                teAlcoObsProd.AsString:=quCardsAlgNAMEM.AsString;
                teAlcoObsProdInn.AsString:=quCardsAlgINNM.AsString;
                teAlcoObsProdKPP.AsString:=quCardsAlgKPPM.AsString;
                teAlcoObrQb.AsFloat:=rQb;
                teAlcoObrQIn1.AsFloat:=0;
                teAlcoObrQIn2.AsFloat:=0;
                teAlcoObrQIn3.AsFloat:=0;
                teAlcoObrQInIt1.AsFloat:=0;
                teAlcoObrQIn4.AsFloat:=0;
                teAlcoObrQIn5.AsFloat:=0;
                teAlcoObrQIn6.AsFloat:=0;
                teAlcoObrQInIt.AsFloat:=0;
                teAlcoObrQOut1.AsFloat:=0;
                teAlcoObrQOut2.AsFloat:=0;
                teAlcoObrQOut3.AsFloat:=0;
                teAlcoObrQOut4.AsFloat:=0;
                teAlcoObrQOutIt.AsFloat:=0;
                teAlcoObrQe.AsFloat:=rQb;
                teAlcoOb.Post;
              end;  
            end;
          end;

          quCardsAlg.Next; inc(iC);
          if iC mod iStep = 0 then
          begin
            PBar1.Position:=iC div iStep;
            delay(10);
          end;
        end;
        ViAlco1.EndUpdate;
      end;
      quFindDH.Active:=False;

      delay(100);
      PBar1.Visible:=False;

      Memo1.Lines.Add('  ������ �������� ��������.'); delay(10);
    end;
  end;
end;

procedure TfmAddAlco.acInputPostExecute(Sender: TObject);
Var fName:String;
    ExcelApp, Workbook, ISheet: Variant;
    iCli:INteger;
    i,iAdd,iDep:INteger;
    arrs:array[1..11] of string;
begin
  //������� ����������
  if fmSelPost=nil then fmSelPost:=tfmSelPost.Create(Application);
  fmSelPost.ShowModal;
  if fmSelPost.ModalResult=mrOk then
  begin
    if MessageDlg('������� ������ �� ���������� '+fmSelPost.cxButtonEdit1.Text+'?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      fName:=fmSelPost.cxButtonEdit2.Text;
      iCli:=fmSelPost.cxButtonEdit1.Tag;
      iDep:=cxLookupComboBox1.EditValue;
      if FileExists(fName) then
      begin
        //����� ���������
        Memo1.Clear;
        Memo1.Lines.Add('������ ��������� ������ �� ����������..'); delay(10);

        ViAlco2.BeginUpdate;
        teAlcoIn.First;
        while not teAlcoIn.Eof do
          if teAlcoIniPost.AsInteger=iCli then teAlcoIn.Delete else teAlcoIn.Next;
        ViAlco2.EndUpdate;
        delay(100);

        Memo1.Lines.Add('����� .. ���� ������� ������..'); delay(10);

        ExcelApp := CreateOleObject('Excel.Application');
        ExcelApp.Application.EnableEvents := false;
        Workbook := ExcelApp.WorkBooks.Add(fName);
        ISheet := Workbook.Worksheets.Item[1];

        i:=2;  iAdd:=0;
        While String(ISheet.Cells.Item[i, 1].Value)<>'' do
        begin
          teAlcoIn.Append;
          teAlcoIniDep.AsInteger:=iDep;
          teAlcoInsDep.AsString:=cxLookupComboBox1.Text;
          teAlcoIniVid.AsInteger:=StrToIntDef(String(ISheet.Cells.Item[i,1].Value),0);
          teAlcoInsVid.AsString:=String(ISheet.Cells.Item[i,1].Value);
          teAlcoInsVidName.AsString:=String(ISheet.Cells.Item[i,2].Value);
          teAlcoIniProd.AsInteger:=prFindProd(String(ISheet.Cells.Item[i,4].Value),String(ISheet.Cells.Item[i,5].Value),String(ISheet.Cells.Item[i,3].Value));
          teAlcoInsProd.AsString:=String(ISheet.Cells.Item[i,3].Value);
          teAlcoInsProdInn.AsString:=String(ISheet.Cells.Item[i,4].Value);
          teAlcoInsProdKPP.AsString:=String(ISheet.Cells.Item[i,5].Value);
          teAlcoIniPost.AsInteger:=iCli;
          prFindCl1(iCli,arrs);
          teAlcoInsPost.AsString:=arrs[1];
          teAlcoInsPostInn.AsString:=arrs[3];
          teAlcoInsPostKpp.AsString:=arrs[4];
//          teAlcoIniLic.AsInteger:=prFindLic(iCli,StrToDateDef(String(ISheet.Cells.Item[i,10].Value),date),StrToDateDef(String(ISheet.Cells.Item[i,11].Value),date),String(ISheet.Cells.Item[i,12].Value),String(ISheet.Cells.Item[i,9].Value));
          teAlcoIniLic.AsInteger:=0;
          teAlcoInsLicNumber.AsString:=String(ISheet.Cells.Item[i,9].Value);
          teAlcoInLicDateB.AsDateTime:=StrToDateDef(String(ISheet.Cells.Item[i,10].Value),date);
          teAlcoInLicDateE.AsDateTime:=StrToDateDef(String(ISheet.Cells.Item[i,11].Value),date);
          teAlcoInLicOrg.AsString:=String(ISheet.Cells.Item[i,12].Value);
          teAlcoInDocDate.AsDateTime:=StrToDateDef(String(ISheet.Cells.Item[i,13].Value),date);
          teAlcoInDocNum.AsString:=String(ISheet.Cells.Item[i,14].Value);
          teAlcoInGTD.AsString:=String(ISheet.Cells.Item[i,15].Value);
          teAlcoInrQIn.AsFloat:=StrToFloatDef(String(ISheet.Cells.Item[i,16].Value),0);
          teAlcoInIdDH.AsInteger:=0;
          teAlcoIn.Post;

          inc(iAdd);

          inc(i);
          if i mod 100 =0 then
          begin
            Memo1.Lines.Add('  ��������� '+its(iAdd)); delay(10);
          end;
        end;
        Memo1.Lines.Add('  ��������� '+its(iAdd)); delay(10);
        Memo1.Lines.Add('������� ��������.'); delay(10);

        ExcelApp.Quit;
        ExcelApp:=Unassigned;
      end else showmessage('���� �� ������');
    end;
  end;

end;

procedure TfmAddAlco.acRecalcLicExecute(Sender: TObject);
Var LicSerN,LicOrg:String;
    LicDateB,LicDateE:tDateTime;
begin
  Memo1.Clear;
  Memo1.Lines.Add('���� �������������� ������ �������� �����������. ����� ...');
  ViAlco2.BeginUpdate;
  try
    with dmORep do
    begin
      teAlcoIn.First;
      while not teAlcoIn.Eof do
      begin
        LicSerN:='';
        LicOrg:='';
        LicDateB:=0;
        LicDateE:=0;

        quCliLicFind.Active:=False;
        quCliLicFind.ParamByName('ICLI').AsInteger:=teAlcoIniPost.AsInteger;
        quCliLicFind.ParamByName('IDB').AsInteger:=Trunc(teAlcoInDocDate.AsDateTime);
        quCliLicFind.ParamByName('IDE').AsInteger:=Trunc(teAlcoInDocDate.AsDateTime);
        quCliLicFind.Active:=True;
        quCliLicFind.First;
        while not quCliLicFind.Eof do
        begin
          LicSerN:=quCliLicFindSER.AsString+' '+quCliLicFindSNUM.AsString;
          LicOrg:=quCliLicFindORGAN.AsString;
          LicDateB:=quCliLicFindDDATEB.AsDateTime;
          LicDateE:=quCliLicFindDDATEE.AsDateTime;

          quCliLicFind.Next;
        end;

        quCliLicFind.Active:=False;

        teAlcoIn.Edit;
//        teAlcoIniLic.AsInteger:=0;
        teAlcoInsLicNumber.AsString:=LicSerN;
        teAlcoInLicDateB.AsDateTime:=LicDateB;
        teAlcoInLicDateE.AsDateTime:=LicDateE;
        teAlcoInLicOrg.AsString:=LicOrg;
        teAlcoIn.Post;

        teAlcoIn.Next;
      end;
    end;
  finally
    ViAlco2.EndUpdate;
    Memo1.Lines.Add('�������� ���������.');
    ShowMessage('�������� ���������');
  end;
end;

end.
