unit RepSS;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons,
  cxGraphics, cxCheckBox, cxControls, cxContainer, cxEdit, cxTextEdit,
  cxMaskEdit, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox, cxCalendar, DB, FIBDataSet, pFIBDataSet,
  cxImageComboBox;

type
  TfmPreRepSS = class(TForm)
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    cxLookupComboBox1: TcxLookupComboBox;
    CheckBox1: TcxCheckBox;
    CheckBox2: TcxCheckBox;
    cxLookupComboBox3: TcxLookupComboBox;
    CheckBox3: TcxCheckBox;
    cxDateEdit1: TcxDateEdit;
    cxDateEdit2: TcxDateEdit;
    cxDateEdit3: TcxDateEdit;
    cxDateEdit4: TcxDateEdit;
    cxDateEdit5: TcxDateEdit;
    cxDateEdit6: TcxDateEdit;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    quMHAll1: TpFIBDataSet;
    quMHAll1ID: TFIBIntegerField;
    quMHAll1PARENT: TFIBIntegerField;
    quMHAll1ITYPE: TFIBIntegerField;
    quMHAll1NAMEMH: TFIBStringField;
    quMHAll1DEFPRICE: TFIBIntegerField;
    quMHAll1NAMEPRICE: TFIBStringField;
    dsMHAll1: TDataSource;
    quCateg: TpFIBDataSet;
    quCategID: TFIBIntegerField;
    quCategNAMECAT: TFIBStringField;
    dsquCateg: TDataSource;
    cxLookupComboBox2: TcxImageComboBox;
    procedure cxButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmPreRepSS: TfmPreRepSS;

implementation

uses Un1;

{$R *.dfm}

procedure TfmPreRepSS.cxButton1Click(Sender: TObject);
begin
  Cursor:=crHourGlass; Delay(10);
  Prib.MHId:=cxLookupComboBox1.EditValue;
  Prib.MHAll:=CheckBox1.Checked;
  Prib.Cat:=cxLookupComboBox3.EditValue;
  Prib.CatAll:=CheckBox3.Checked;
  Prib.Oper:=cxLookupComboBox2.EditValue;
  Prib.OperAll:=CheckBox2.Checked;
  Prib.d11:=cxDateEdit1.Date;
  Prib.d12:=cxDateEdit2.Date;
  Prib.d21:=cxDateEdit3.Date;
  Prib.d22:=cxDateEdit4.Date;
  Prib.d31:=cxDateEdit5.Date;
  Prib.d32:=cxDateEdit6.Date;
end;

end.
