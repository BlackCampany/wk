unit AddDoc2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, cxGraphics, cxCurrencyEdit, cxDropDownEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxButtonEdit,
  cxMaskEdit, cxCalendar, cxControls, cxContainer, cxEdit, cxTextEdit,
  StdCtrls, ExtCtrls, Menus, cxLookAndFeelPainters, cxButtons, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, DB, cxDBData, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxLabel, dxfLabel, Placemnt, FIBDataSet,
  pFIBDataSet, DBClient, FIBQuery, pFIBQuery, pFIBStoredProc, ActnList,
  XPStyleActnCtrls, ActnMan, cxImageComboBox, dxmdaset, cxSpinEdit;

type
  TfmAddDoc2 = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Label1: TLabel;
    cxTextEdit1: TcxTextEdit;
    cxDateEdit1: TcxDateEdit;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label12: TLabel;
    cxTextEdit2: TcxTextEdit;
    cxDateEdit2: TcxDateEdit;
    cxButtonEdit1: TcxButtonEdit;
    cxLookupComboBox1: TcxLookupComboBox;
    Panel2: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Panel3: TPanel;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    cxLabel3: TcxLabel;
    Label15: TLabel;
    FormPlacement1: TFormPlacement;
    dsSpec: TDataSource;
    prCalcPrice: TpFIBStoredProc;
    cxLabel4: TcxLabel;
    cxLabel5: TcxLabel;
    cxLabel6: TcxLabel;
    Image7: TImage;
    amDocOut: TActionManager;
    acSave: TAction;
    acExit: TAction;
    cxLabel7: TcxLabel;
    cxLabel8: TcxLabel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    GridDoc2: TcxGrid;
    ViewDoc2: TcxGridDBTableView;
    ViewDoc2Num: TcxGridDBColumn;
    ViewDoc2IdGoods: TcxGridDBColumn;
    ViewDoc2NameG: TcxGridDBColumn;
    ViewDoc2IM: TcxGridDBColumn;
    ViewDoc2SM: TcxGridDBColumn;
    ViewDoc2Quant: TcxGridDBColumn;
    ViewDoc2Price1: TcxGridDBColumn;
    ViewDoc2Sum1: TcxGridDBColumn;
    ViewDoc2Price2: TcxGridDBColumn;
    ViewDoc2Sum2: TcxGridDBColumn;
    ViewDoc2SumNac: TcxGridDBColumn;
    ViewDoc2ProcNac: TcxGridDBColumn;
    ViewDoc2INds: TcxGridDBColumn;
    ViewDoc2SNds: TcxGridDBColumn;
    ViewDoc2RNds: TcxGridDBColumn;
    LevelDoc2: TcxGridLevel;
    acAddPos: TAction;
    acAddList: TAction;
    acDelPos: TAction;
    acDelAll: TAction;
    Label6: TLabel;
    cxButtonEdit2: TcxButtonEdit;
    PopupMenu1: TPopupMenu;
    MenuItem1: TMenuItem;
    acMovePos: TAction;
    N1: TMenuItem;
    N2: TMenuItem;
    ViewDoc2CType: TcxGridDBColumn;
    taSpecOut: TdxMemData;
    taSpecOutNum: TIntegerField;
    taSpecOutIdGoods: TIntegerField;
    taSpecOutNameG: TStringField;
    taSpecOutIM: TIntegerField;
    taSpecOutSM: TStringField;
    taSpecOutQuant: TFloatField;
    taSpecOutPrice0: TFloatField;
    taSpecOutSum0: TFloatField;
    taSpecOutPrice1: TFloatField;
    taSpecOutSum1: TFloatField;
    taSpecOutPrice2: TFloatField;
    taSpecOutSum2: TFloatField;
    taSpecOutINds: TIntegerField;
    taSpecOutSNds: TStringField;
    taSpecOutSumNac: TFloatField;
    taSpecOutProcNac: TFloatField;
    taSpecOutKM: TFloatField;
    taSpecOutCType: TSmallintField;
    ViewDoc2Price0: TcxGridDBColumn;
    ViewDoc2Sum0: TcxGridDBColumn;
    taSpecOutRNds: TFloatField;
    Label17: TLabel;
    cxSpinEdit1: TcxSpinEdit;
    procedure FormCreate(Sender: TObject);
    procedure cxLookupComboBox1PropertiesChange(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxLabel1Click(Sender: TObject);
    procedure ViewDoc2DragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure ViewDoc2DragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure taSpecOut2QuantChange(Sender: TField);
    procedure taSpecOut2Price1Change(Sender: TField);
    procedure taSpecOut2Sum1Change(Sender: TField);
    procedure taSpecOut2Price2Change(Sender: TField);
    procedure taSpecOut2Sum2Change(Sender: TField);
    procedure cxLabel3Click(Sender: TObject);
    procedure cxLabel2Click(Sender: TObject);
    procedure cxLabel4Click(Sender: TObject);
    procedure ViewDoc2Editing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure FormShow(Sender: TObject);
    procedure cxLabel5Click(Sender: TObject);
    procedure cxLabel6Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cxButton1Click(Sender: TObject);
    procedure acSaveExecute(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure acAddPosExecute(Sender: TObject);
    procedure acAddListExecute(Sender: TObject);
    procedure cxLabel7Click(Sender: TObject);
    procedure acDelPosExecute(Sender: TObject);
    procedure ViewDoc2EditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure ViewDoc2EditKeyPress(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
    procedure cxLabel8Click(Sender: TObject);
    procedure acDelAllExecute(Sender: TObject);
    procedure ViewDoc2SMPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxButtonEdit2PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxButtonEdit1KeyPress(Sender: TObject; var Key: Char);
    procedure cxButtonEdit2KeyPress(Sender: TObject; var Key: Char);
    procedure MenuItem1Click(Sender: TObject);
    procedure acMovePosExecute(Sender: TObject);
    procedure taSpecOutQuantChange(Sender: TField);
    procedure taSpecOutPrice1Change(Sender: TField);
    procedure taSpecOutSum1Change(Sender: TField);
    procedure taSpecOutPrice2Change(Sender: TField);
    procedure taSpecOutSum2Change(Sender: TField);
    procedure taSpecOutPrice0Change(Sender: TField);
    procedure taSpecOutSum0Change(Sender: TField);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAddDoc2: TfmAddDoc2;
  iCol:Integer = 0;
  iColT:Integer = 0;
  bAdd:Boolean = False;

implementation

uses Un1, dmOffice, Clients, Goods, SelPartIn, SelPartIn1, DMOReps,
  DocsOut, FCards, OMessure, SelPerSkl, CardsMove, MainRnOffice;

{$R *.dfm}

procedure TfmAddDoc2.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  PageControl1.Align:=alClient;
  PageControl1.ActivePageIndex:=0;
  ViewDoc2.RestoreFromIniFile(CurDir+GridIni);
end;

procedure TfmAddDoc2.cxLookupComboBox1PropertiesChange(Sender: TObject);
begin
  with dmO do
  begin
    CurVal.IdMH:=cxLookupComboBox1.EditValue;
    CurVal.NAMEMH:=cxLookupComboBox1.Text;
    if quMHAll.Locate('ID',CurVal.IdMH,[]) then
    begin
      Label15.Caption:='��. ����: '+quMHAllNAMEPRICE.AsString;
      Label15.Tag:=quMHAllDEFPRICE.AsInteger;
    end else
    begin
      Label15.Caption:='';
      Label15.Tag:=0;
    end;  
  end;
end;

procedure TfmAddDoc2.cxButtonEdit1PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  if dmO.taClients.Active=False then dmO.taClients.Active:=True;
  if fmClients.Visible then fmClients.Close;
  if cxButtonEdit1.Tag>0 then dmO.taClients.Locate('ID',cxButtonEdit1.Tag,[]);
  fmClients.ShowModal;
  if fmClients.ModalResult=mrOk then
  begin
    cxButtonEdit1.Tag:=dmO.taClientsID.AsInteger;
    cxButtonEdit1.Text:=dmO.taClientsNAMECL.AsString;
  end else
  begin
    cxButtonEdit1.Tag:=0;
    cxButtonEdit1.Text:='';
  end;
end;

procedure TfmAddDoc2.cxLabel1Click(Sender: TObject);
begin
  //�������� �������
  acAddPos.Execute;
end;

procedure TfmAddDoc2.ViewDoc2DragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept:=False;
  if bDrRet then  Accept:=True;
end;

procedure TfmAddDoc2.ViewDoc2DragDrop(Sender, Source: TObject; X,
  Y: Integer);
Var iCo:Integer;
    i,j: Integer;
    iNum: Integer;
    Rec:TcxCustomGridRecord;
    iMax:Integer;
begin
//
  if bDrRet then
  begin
    ResetAddVars;

    iCo:=fmGoods.ViewGoods.Controller.SelectedRecordCount;
    if iCo>0 then
    begin
      if MessageDlg('�� ������������� ������ ����������� ��������� ������� ('+IntToStr(iCo)+' ��.) � ������������ ���������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        iMax:=1;
        fmAddDoc2.taSpecOut.First;
        if not fmAddDoc2.taSpecOut.Eof then
        begin
          fmAddDoc2.taSpecOut.Last;
          iMax:=fmAddDoc2.taSpecOutNum.AsInteger+1;
        end;

        with dmO do
        begin
          ViewDoc2.BeginUpdate;
          for i:=0 to fmGoods.ViewGoods.Controller.SelectedRecordCount-1 do
          begin
            Rec:=fmGoods.ViewGoods.Controller.SelectedRecords[i];

            for j:=0 to Rec.ValueCount-1 do
            begin
              if fmGoods.ViewGoods.Columns[j].Name='ViewGoodsID' then break;
            end;

            iNum:=Rec.Values[j];
          //��� ��� - ����������

            if quCardsSel.Locate('ID',iNum,[]) then
            begin
              with fmAddDoc2 do
              begin
                taSpecOut.Append;
                taSpecOutNum.AsInteger:=iMax;
                taSpecOutIdGoods.AsInteger:=quCardsSelID.AsInteger;
                taSpecOutNameG.AsString:=quCardsSelNAME.AsString;
                taSpecOutIM.AsInteger:=quCardsSelIMESSURE.AsInteger;
                taSpecOutSM.AsString:=quCardsSelNAMESHORT.AsString;
                taSpecOutQuant.AsFloat:=1;
                taSpecOutPrice1.AsFloat:=0;
                taSpecOutSum1.AsFloat:=0;
                taSpecOutPrice2.AsFloat:=0;
                taSpecOutSum2.AsFloat:=0;
                taSpecOutINds.AsInteger:=quCardsSelINDS.AsInteger;
                taSpecOutSNds.AsString:=quCardsSelNAMENDS.AsString;
                taSpecOutRNds.AsFloat:=0;
                taSpecOutSumNac.AsFloat:=0;
                taSpecOutProcNac.AsFloat:=0;
                taSpecOutCType.AsInteger:=quCardsSelCATEGORY.AsInteger;
                taSpecOut.Post;
                inc(iMax);
              end;
            end;
          end;
          ViewDoc2.EndUpdate;
        end;
      end;
    end;
  end;
end;

procedure TfmAddDoc2.taSpecOut2QuantChange(Sender: TField);
begin
  //���������� �����
  if iCol=1 then
  begin
    taSpecOutSum1.AsFloat:=taSpecOutPrice1.AsFloat*taSpecOutQuant.AsFloat;
    taSpecOutSum2.AsFloat:=taSpecOutPrice2.AsFloat*taSpecOutQuant.AsFloat;
    taSpecOutSumNac.AsFloat:=taSpecOutSum2.AsFloat-taSpecOutSum1.AsFloat;
    if taSpecOutSum1.AsFloat>0.01 then
    begin
      taSpecOutProcNac.AsFloat:=(taSpecOutSum2.AsFloat-taSpecOutSum1.AsFloat)/taSpecOutSum1.AsFloat*100;
    end;
    taSpecOutRNds.AsFloat:=RoundEx(taSpecOutSum1.AsFloat*vNds[taSpecOutINds.AsInteger]/(100+vNds[taSpecOutINds.AsInteger])*100)/100;
  end;
end;

procedure TfmAddDoc2.taSpecOut2Price1Change(Sender: TField);
begin
  //���������� ����
  if iCol=2 then
  begin
    taSpecOutSum1.AsFloat:=taSpecOutPrice1.AsFloat*taSpecOutQuant.AsFloat;
//  taSpecOutSum2.AsFloat:=taSpecOutPrice2.AsFloat*taSpecOutQuant.AsFloat;
    taSpecOutSumNac.AsFloat:=taSpecOutSum2.AsFloat-taSpecOutSum1.AsFloat;
    if taSpecOutSum1.AsFloat>0.01 then
    begin
      taSpecOutProcNac.AsFloat:=(taSpecOutSum2.AsFloat-taSpecOutSum1.AsFloat)/taSpecOutSum1.AsFloat*100;
    end else taSpecOutProcNac.AsFloat:=0;
    taSpecOutRNds.AsFloat:=RoundEx(taSpecOutSum1.AsFloat*vNds[taSpecOutINds.AsInteger]/(100+vNds[taSpecOutINds.AsInteger])*100)/100;
  end;
end;

procedure TfmAddDoc2.taSpecOut2Sum1Change(Sender: TField);
begin
  if iCol=3 then
  begin
    if abs(taSpecOutQuant.AsFloat)>0 then taSpecOutPrice1.AsFloat:=RoundEx(taSpecOutSum1.AsFloat/taSpecOutQuant.AsFloat*100)/100;
//  taSpecOutPrice1.AsFloat:=RoundEx(taSpecOutSum1.AsFloat/taSpecOutQuant.AsFloat*100)/100;
    taSpecOutSumNac.AsFloat:=taSpecOutSum2.AsFloat-taSpecOutSum1.AsFloat;
    if abs(taSpecOutSum1.AsFloat)>0.01 then
    begin
      taSpecOutProcNac.AsFloat:=(taSpecOutSum2.AsFloat-taSpecOutSum1.AsFloat)/taSpecOutSum1.AsFloat*100;
    end else taSpecOutProcNac.AsFloat:=0;
    taSpecOutRNds.AsFloat:=RoundEx(taSpecOutSum1.AsFloat*vNds[taSpecOutINds.AsInteger]/(100+vNds[taSpecOutINds.AsInteger])*100)/100;
  end;
end;

procedure TfmAddDoc2.taSpecOut2Price2Change(Sender: TField);
begin
  //���������� ����
  if iCol=4 then
  begin

//  taSpecSum1.AsFloat:=taSpecOutPrice1.AsFloat*taSpecOutQuant.AsFloat;
    taSpecOutSum2.AsFloat:=taSpecOutPrice2.AsFloat*taSpecOutQuant.AsFloat;
    taSpecOutSumNac.AsFloat:=taSpecOutSum2.AsFloat-taSpecOutSum1.AsFloat;
    if taSpecOutSum1.AsFloat>0.01 then
    begin
      taSpecOutProcNac.AsFloat:=(taSpecOutSum2.AsFloat-taSpecOutSum1.AsFloat)/taSpecOutSum1.AsFloat*100;
    end else taSpecOutProcNac.AsFloat:=0;
  end;
end;

procedure TfmAddDoc2.taSpecOut2Sum2Change(Sender: TField);
begin
  if iCol=5 then
  begin
    if abs(taSpecOutQuant.AsFloat)>0 then taSpecOutPrice2.AsFloat:=RoundEx(taSpecOutSum2.AsFloat/taSpecOutQuant.AsFloat*100)/100;
    taSpecOutSumNac.AsFloat:=taSpecOutSum2.AsFloat-taSpecOutSum1.AsFloat;
    if taSpecOutSum1.AsFloat>0.01 then
    begin
      taSpecOutProcNac.AsFloat:=(taSpecOutSum2.AsFloat-taSpecOutSum1.AsFloat)/taSpecOutSum1.AsFloat*100;
    end else taSpecOutProcNac.AsFloat:=0;
  end;
end;

procedure TfmAddDoc2.cxLabel3Click(Sender: TObject);
Var rPrice,rPrice1,rPrice0:Currency;
    rMessure,kSp,k:Real;
begin
  //����������� ����  � ����� ���������� �������
  if PageControl1.ActivePageIndex=0 then
  begin
    if cxButtonEdit1.Tag=0 then
    begin
      showmessage('�������� ������� �����������.');
      exit;
    end else
    begin
      iCol:=0;
      taSpecOut.First;
      while not taSpecOut.Eof do
      begin
        with dmO do
        begin
          prCalcLastPrice.ParamByName('IDGOOD').AsInteger:=taSpecOutIdGoods.AsInteger;
          prCalcLastPrice.ParamByName('IDCLI').AsFloat:=cxButtonEdit1.Tag;
          prCalcLastPrice.ParamByName('ISKL').AsInteger:=cxLookupComboBox1.EditValue;
          prCalcLastPrice.ExecProc;

          rPrice:=prCalcLastPrice.ParamByName('PRICEIN').AsFloat;
          rPrice0:=prCalcLastPrice.ParamByName('PRICEIN0').AsFloat;
          rPrice1:=prCalcLastPrice.ParamByName('PRICEUCH').AsFloat;
          rMessure:=prCalcLastPrice.ParamByName('KOEF').AsFloat;

          quM.Active:=False;
          quM.ParamByName('IDM').AsInteger:=taSpecOutIM.AsInteger;
          quM.Active:=True;
          kSp:=quMKOEF.AsFloat;
          quM.Active:=False;

          if kSp<>rMessure then
          begin
            k:=rMessure/kSp;
          end else k:=1;

          taSpecOut.Edit;
          taSpecOutPrice0.AsFloat:=rPrice0*k;
          taSpecOutPrice1.AsFloat:=rPrice*k;
          taSpecOutPrice2.AsFloat:=rPrice1*k;
          taSpecOutSum0.AsFloat:=rPrice0*k*taSpecOutQuant.AsFloat;
          taSpecOutSum1.AsFloat:=rPrice*k*taSpecOutQuant.AsFloat;
          taSpecOutSum2.AsFloat:=rPrice1*k*taSpecOutQuant.AsFloat;
          taSpecOutSumNac.AsFloat:=taSpecOutSum2.AsFloat-taSpecOutSum1.AsFloat;
          if taSpecOutSum1.AsFloat>0.01 then
          begin
            taSpecOutProcNac.AsFloat:=(taSpecOutSum2.AsFloat-taSpecOutSum1.AsFloat)/taSpecOutSum1.AsFloat*100;
          end else taSpecOutProcNac.AsFloat:=0;
          taSpecOutRNds.AsFloat:=RoundEx(taSpecOutSum1.AsFloat-taSpecOutSum0.AsFloat);
          taSpecOut.Post;

          taSpecOut.Next;
        end;
      end;
      taSpecOut.First;
    end;
  end;
end;

procedure TfmAddDoc2.cxLabel2Click(Sender: TObject);
begin
  acDelPos.Execute;
end;

procedure TfmAddDoc2.cxLabel4Click(Sender: TObject);
begin
  //�������� ����
  if PageControl1.ActivePageIndex=0 then
  begin
    ViewDoc2.BeginUpdate;
    taSpecOut.First;
    while not taSpecOut.Eof do
    begin
      taSpecOut.Edit;
      taSpecOutPrice2.AsFloat:=taSpecOutPrice1.AsFloat;
      taSpecOutSum2.AsFloat:=taSpecOutSum1.AsFloat;
      taSpecOutSumNac.AsFloat:=0;
      taSpecOutProcNac.AsFloat:=0;
      taSpecOut.Post;

      taSpecOut.Next;
      delay(10);
    end;
    taSpecOut.First;
    ViewDoc2.EndUpdate;
  end;
end;

procedure TfmAddDoc2.ViewDoc2Editing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  iCol:=0;
  if ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2Quant' then iCol:=1;
  if ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2Price1' then iCol:=2;
  if ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2Sum1' then iCol:=3;
  if ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2Price2' then iCol:=4;
  if ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2Sum2' then iCol:=5;
  if ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2Price0' then iCol:=6;
  if ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2Sum0' then iCol:=7;
end;

procedure TfmAddDoc2.FormShow(Sender: TObject);
begin
  iCol:=0;
  iColT:=0;
  PageControl1.ActivePageIndex:=0;

  ViewDoc2NameG.Options.Editing:=False;
end;

procedure TfmAddDoc2.cxLabel5Click(Sender: TObject);
var sMessure:String;
    iM:Integer;
    rPrice,rPrice1,k:Real;
begin
  //������ ������
  if PageControl1.ActivePageIndex=0 then
  begin
    fmPartIn:=tfmPartIn.Create(Application);
    if cxLookupComboBox1.EditValue>0 then
    begin
      if not taSpecOut.Eof then prSelPartInRet(taSpecOutIdGoods.AsInteger,cxLookupComboBox1.EditValue,cxButtonEdit1.Tag);
      with dmO do
      begin
        quM.Active:=false;
        quM.ParamByName('IDM').AsInteger:=taSpecOutIM.AsInteger;
        quM.Active:=True;
        if quMID_PARENT.AsInteger=0 then sMessure:=quMNAMESHORT.AsString
        else
        begin
          iM:=quMID_PARENT.AsInteger;
          quM.Active:=false;
          quM.ParamByName('IDM').AsInteger:=iM;
          quM.Active:=True;
          sMessure:=quMNAMESHORT.AsString;
        end;
        quM.Active:=false;
      end;
      fmPartIn.Label4.Caption:=taSpecOutNameG.AsString+' ('+sMessure+')';
      fmPartIn.Label5.Caption:=cxLookupComboBox1.Text;
      if cxButtonEdit1.Tag=0 then fmPartIn.Label6.Caption:='�� ���������'
                           else fmPartIn.Label6.Caption:=cxButtonEdit1.Text;
      fmPartIn.ShowModal;
      if fmPartIn.ModalResult=mrOk then
      begin //������������ ����
        rPrice:=dmO.quSelPartInPRICEIN.AsFloat; // � ��������
        rPrice1:=dmO.quSelPartInPRICEOUT.AsFloat;
        k:=prFindKM(taSpecOutIM.AsInteger);

        taSpecOut.Edit;
        taSpecOutPrice1.AsFloat:=rPrice*k;
        taSpecOutPrice2.AsFloat:=rPrice1*k;
        taSpecOutSum1.AsFloat:=rPrice*k*taSpecOutQuant.AsFloat;
        taSpecOutSum2.AsFloat:=rPrice1*k*taSpecOutQuant.AsFloat;
        taSpecOutSumNac.AsFloat:=taSpecOutSum2.AsFloat-taSpecOutSum1.AsFloat;
        if taSpecOutSum1.AsFloat>0.01 then
        begin
          taSpecOutProcNac.AsFloat:=(taSpecOutSum2.AsFloat-taSpecOutSum1.AsFloat)/taSpecOutSum1.AsFloat*100;
        end else taSpecOutProcNac.AsFloat:=0;
        taSpecOutRNds.AsFloat:=RoundEx(taSpecOutSum1.AsFloat*vNds[taSpecOutINds.AsInteger]/(100+vNds[taSpecOutINds.AsInteger])*100)/100;
        taSpecOut.Post;
      end;
    end else showmessage('�������� ����� ��������.');
    fmPartIn.Release;
  end;
end;

procedure TfmAddDoc2.cxLabel6Click(Sender: TObject);
Var iYes:Integer;
    rQP,rQs,PriceSp,k:Real;
begin
//�������� ������
  if PageControl1.ActivePageIndex=0 then
  begin
    fmPartIn1:=tfmPartIn1.Create(Application);
    if cxLookupComboBox1.EditValue>0 then
    begin
      with dmO do
      with dmORep do
      begin
        iCol:=0;
        CloseTa(taPartTest);
        taSpecOut.First;
        while not taSpecOut.Eof do
        begin
          //��� ��� ������
          rQP:=0;
          k:=prFindKM(taSpecOutIM.AsInteger);
          rQs:=taSpecOutQuant.AsFloat*k; //20*0,5=10
          PriceSp:=taSpecOutPrice1.AsFloat/k;

          prSelPartIn(taSpecOutIdGoods.AsInteger,cxLookupComboBox1.EditValue,cxButtonEdit1.Tag,0);
          with dmO do
          begin
            quSelPartIn.First;
            while not quSelPartIn.Eof do
            begin
            //���� �� ���� ������� ���� �����, ��������� �������� ���
              if RoundVal(quSelPartInPRICEIN.AsFloat)=RoundVal(PriceSp) then rQp:=rQp+quSelPartInQREMN.AsFloat;
              quSelPartIn.Next;
            end;
            quSelPartIn.Active:=False;
          end;

          if rQp>=rQs then iYes:=1 else iYes:=0;

          taPartTest.Append;
          taPartTestNum.AsInteger:=taSpecOutNum.AsInteger;
          taPartTestIdGoods.AsInteger:=taSpecOutIdGoods.AsInteger;
          taPartTestNameG.AsString:=taSpecOutNameG.AsString;
          taPartTestIM.AsInteger:=taSpecOutIM.AsInteger;
          taPartTestSM.AsString:=taSpecOutSM.AsString;
          taPartTestQuant.AsFloat:=taSpecOutQuant.AsFloat;
          taPartTestPrice1.AsFloat:=taSpecOutPrice1.AsFloat;
          taPartTestiRes.AsInteger:=iYes;
          taPartTest.Post;

          taSpecOut.Next;
        end;

        fmPartIn1.Label5.Caption:=cxLookupComboBox1.Text;
        if cxButtonEdit1.Tag=0 then fmPartIn1.Label6.Caption:='�� ���������'
                               else fmPartIn1.Label6.Caption:=cxButtonEdit1.Text;
        fmPartIn1.ShowModal;

        taPartTest.Active:=False;
      end;
    end else showmessage('�������� ����� ��������.');
    fmPartIn1.Release;
  end;
end;

procedure TfmAddDoc2.FormClose(Sender: TObject; var Action: TCloseAction);
begin
//  taSpecOut.Active:=False;
  if cxButton1.Enabled=True then
  begin
    if MessageDlg('�� ������������� ������ ����� �� ���������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      ViewDoc2.StoreToIniFile(CurDir+GridIni,False);
      taSpecOut.Active:=False;
      Action := caHide;
    end
    else  Action := caNone;
  end;
end;

procedure TfmAddDoc2.cxButton1Click(Sender: TObject);
Var IdH:Integer;
    rSum1,rSum2,rSumT:Real;
    bErr:Boolean;
    iSS:INteger;
begin
  //��������
  with dmO do
  with dmORep do
  begin
    IdH:=cxTextEdit1.Tag;
    if taSpecOut.State in [dsEdit,dsInsert] then taSpecOut.Post;
    if cxDateEdit1.Date<3000 then  cxDateEdit1.Date:=Date;
    if cxDateEdit2.Date<3000 then  cxDateEdit2.Date:=Date;

    //������� ������ ������
    ViewDoc2.BeginUpdate;

    bErr:=False;
    taSpecOut.First;
    while not taSpecOut.Eof do
    begin
      if taSpecOutIdGoods.AsInteger=0 then bErr:=True;
      if taSpecOutQuant.AsFloat=0 then bErr:=True;
      taSpecOut.Next;
    end;
    if bErr then
    begin
      showmessage('������ ��������� ������������ (������� ��� ��� ������� ���-��). ���������� ����������!');
      ViewDoc2.EndUpdate;
      exit;
    end;

    if cxTextEdit1.Tag=0 then
    begin
      IDH:=GetId('DocOut');
      cxTextEdit1.Tag:=IDH;
      if cxTextEdit1.Text=prGetNum(7,0) then prGetNum(7,1); //��������
    end;

    quDocsOutId.Active:=False;
    quDocsOutId.ParamByName('IDH').AsInteger:=IDH;
    quDocsOutId.Active:=True;

    quDocsOutId.First;
    if quDocsOutId.RecordCount=0 then quDocsOutId.Append else quDocsOutId.Edit;

    quDocsOutIdID.AsInteger:=IDH;
    quDocsOutIdDATEDOC.AsDateTime:=Trunc(cxDateEdit1.Date);
    quDocsOutIdNUMDOC.AsString:=cxTextEdit1.Text;
    quDocsOutIdDATESF.AsDateTime:=Trunc(cxDateEdit2.Date);
    quDocsOutIdNUMSF.AsString:=cxTextEdit2.Text;
    quDocsOutIdIDCLI.AsInteger:=cxButtonEdit1.Tag;
    quDocsOutIdIDSKL.AsInteger:=cxLookupComboBox1.EditValue;
    quDocsOutIdSUMIN.AsFloat:=0;
    quDocsOutIdSUMUCH.AsFloat:=0;
    quDocsOutIdSUMTAR.AsFloat:=0;
    quDocsOutIdSUMNDS0.AsFloat:=sNDS[1];
    quDocsOutIdSUMNDS1.AsFloat:=sNDS[2];
    quDocsOutIdSUMNDS2.AsFloat:=sNDS[3];
    quDocsOutIdPROCNAC.AsFloat:=0;
    quDocsOutIdIACTIVE.AsInteger:=0;
    quDocsOutIdIDFROM.AsInteger:=cxButtonEdit2.Tag;
    quDocsOutIdOPRIZN.AsInteger:=cxSpinEdit1.Value;
    quDocsOutId.Post;

    //�������� ������������
    quSpecOutSel.Active:=False;
    quSpecOutSel.ParamByName('IDHD').AsInteger:=IDH;
    quSpecOutSel.Active:=True;

    quSpecOutSel.First; //������� ������
    while not quSpecOutSel.Eof do quSpecOutSel.Delete;

    sNDS[1]:=0;sNDS[2]:=0;sNDS[3]:=0;
    rSum1:=0; rSum2:=0; rSumT:=0;
    iSS:=prISS(cxLookupComboBox1.EditValue);
    with fmAddDoc2 do
    begin
      taSpecOut.First;
      while not taSpecOut.Eof do
      begin
        quSpecOutSel.Append;
        quSpecOutSelIDHEAD.AsInteger:=IDH;
        quSpecOutSelID.AsInteger:=GetId('SpecIn');
        quSpecOutSelNUM.AsInteger:=taSpecOutNum.AsInteger;
        quSpecOutSelIDCARD.AsInteger:=taSpecOutIdGoods.AsInteger;
        quSpecOutSelQUANT.AsFloat:=taSpecOutQuant.AsFloat;
        quSpecOutSelPRICEIN.AsFloat:=taSpecOutPrice1.AsFloat;
        quSpecOutSelSUMIN.AsFloat:=RV(taSpecOutSum1.AsFloat);
        quSpecOutSelPRICEUCH.AsFloat:=taSpecOutPrice2.AsFloat;
        quSpecOutSelSUMUCH.AsFloat:=RV(taSpecOutSum2.AsFloat);
        quSpecOutSelIDNDS.AsInteger:=taSpecOutINds.AsInteger;
        quSpecOutSelSUMNDS.AsFloat:=RV(taSpecOutRNds.AsFloat);
        quSpecOutSelIDM.AsInteger:=taSpecOutIM.AsInteger;
        quSpecOutSelPRICEIN0.AsFloat:=taSpecOutPrice0.AsFloat;
        quSpecOutSelSUMIN0.AsFloat:=RV(taSpecOutSum0.AsFloat);
        quSpecOutSel.Post;
        sNDS[taSpecOutINds.AsInteger]:=sNDS[taSpecOutINds.AsInteger]+taSpecOutRNds.AsFloat;

        if (taSpecOutCType.AsInteger=1)or(taSpecOutCType.AsInteger=2)or(taSpecOutCType.AsInteger=3) then
        begin
          if iSS<>2 then
          begin
            rSum1:=rSum1+RoundVal(taSpecOutSum1.AsFloat);
            rSum2:=rSum2+RoundVal(taSpecOutSum2.AsFloat);
          end else
          begin
            rSum1:=rSum1+RoundVal(taSpecOutSum0.AsFloat);
            rSum2:=rSum2+RoundVal(taSpecOutSum2.AsFloat);
          end;
        end;
        if (taSpecOutCType.AsInteger=4) then
        begin
          if iSS<>2 then
          begin
            rSumT:=rSumT+taSpecOutSum1.AsFloat;
          end else
          begin
            rSumT:=rSumT+taSpecOutSum0.AsFloat;
          end;
        end;

        taSpecOut.Next;
      end;
    end;
    quSpecOutSel.Active:=False;

    quDocsOutId.Edit;
    quDocsOutIdSUMIN.AsFloat:=RoundVal(rSum1);
    quDocsOutIdSUMUCH.AsFloat:=RoundVal(rSum2);
    quDocsOutIdSUMTAR.AsFloat:=RoundVal(rSumT);
    quDocsOutIdSUMNDS0.AsFloat:=RoundVal(sNDS[1]);
    quDocsOutIdSUMNDS1.AsFloat:=RoundVal(sNDS[2]);
    quDocsOutIdSUMNDS2.AsFloat:=RoundVal(sNDS[3]);
    quDocsOutIdPROCNAC.AsFloat:=0;
    quDocsOutIdIACTIVE.AsInteger:=0;
    quDocsOutId.Post;

    quDocsOutId.Active:=False;

    fmDocsOut.ViewDocsOut.BeginUpdate;
    quDocsOutSel.FullRefresh;
    quDocsOutSel.Locate('ID',IDH,[]);
    fmDocsOut.ViewDocsOut.EndUpdate;

    ViewDoc2.EndUpdate;
  end;
end;

procedure TfmAddDoc2.acSaveExecute(Sender: TObject);
begin
  if cxButton1.Enabled then cxButton1.Click;
end;

procedure TfmAddDoc2.acExitExecute(Sender: TObject);
begin
  cxButton2.Click;
end;

procedure TfmAddDoc2.cxButton2Click(Sender: TObject);
begin
  close;
end;

procedure TfmAddDoc2.acAddPosExecute(Sender: TObject);
Var iMax:Integer;
begin
  //�������� �������
  if PageControl1.ActivePageIndex=0 then
  begin
    iMax:=1;
    ViewDoc2.BeginUpdate;

    taSpecOut.First;
    if not taSpecOut.Eof then
    begin
      taSpecOut.Last;
      iMax:=taSpecOutNum.AsInteger+1;
    end;

    taSpecOut.Append;
    taSpecOutNum.AsInteger:=iMax;
    taSpecOutIdGoods.AsInteger:=0;
    taSpecOutNameG.AsString:='';
    taSpecOutIM.AsInteger:=0;
    taSpecOutSM.AsString:='';
    taSpecOutKm.AsFloat:=0;
    taSpecOutQuant.AsFloat:=1;
    taSpecOutPrice1.AsFloat:=0;
    taSpecOutSum1.AsFloat:=0;
    taSpecOutPrice2.AsFloat:=0;
    taSpecOutSum2.AsFloat:=0;
    taSpecOutINds.AsInteger:=0;
    taSpecOutSNds.AsString:='';
    taSpecOutRNds.AsFloat:=0;
    taSpecOutSumNac.AsFloat:=0;
    taSpecOutProcNac.AsFloat:=0;
    taSpecOutCType.AsInteger:=1;
    taSpecOut.Post;
    ViewDoc2.EndUpdate;
    GridDoc2.SetFocus;

    ViewDoc2NameG.Options.Editing:=True;
    ViewDoc2NameG.Focused:=True;

    prRowFocus(ViewDoc2);

  end;
end;

procedure TfmAddDoc2.acAddListExecute(Sender: TObject);
begin
  bAddSpecRet:=True;
  fmGoods.Show;
end;

procedure TfmAddDoc2.cxLabel7Click(Sender: TObject);
begin
  acAddList.Execute;
end;

procedure TfmAddDoc2.acDelPosExecute(Sender: TObject);
begin
  //������� �������
  if PageControl1.ActivePageIndex=0 then
  begin
    if taSpecOut.RecordCount>0 then
    begin
      taSpecOut.Delete;
    end;
  end;
end;

procedure TfmAddDoc2.ViewDoc2EditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
Var iCode:Integer;
    Km:Real;
    sName:String;
    //���� ������ ��� ������ ����� �� ������
    rK:Real;
    iM:Integer;
    Sm:String;

begin
  with dmO do
  begin
    if (Key=$0D) then
    begin
      if ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2IdGoods' then
      begin
        iCode:=VarAsType(AEdit.EditingValue, varInteger);

        quFCard.Active:=False;
        quFCard.SelectSQL.Clear;
        quFCard.SelectSQL.Add('SELECT ID,NAME,IMESSURE,PARENT,TCARD,INDS,LASTPRICEOUT,CATEGORY');
        quFCard.SelectSQL.Add('FROM OF_CARDS');
        quFCard.SelectSQL.Add('where ID='+IntToStr(iCode));
        quFCard.SelectSQL.Add('and IACTIVE>0');
        quFCard.SelectSQL.Add('and PARENT in (SELECT ID_CLASSIF FROM RCLASSIF WHERE RIGHTS=0 AND ID_PERSONAL='+its(Person.Id)+')');
        quFCard.Active:=True;

        if quFCard.RecordCount=1 then
        begin
          ViewDoc2.BeginUpdate;
          taSpecOut.Edit;
          taSpecOutIdGoods.AsInteger:=iCode;
          taSpecOutNameG.AsString:=quFCardNAME.AsString;
          taSpecOutIM.AsInteger:=quFCardIMESSURE.AsInteger;
          taSpecOutSM.AsString:=prFindKNM(quFCardIMESSURE.AsInteger,Km);
          taSpecOutKm.AsFloat:=Km;
          taSpecOutQuant.AsFloat:=1;
          taSpecOutPrice1.AsFloat:=0;
          taSpecOutSum1.AsFloat:=0;
          taSpecOutPrice2.AsFloat:=0;
          taSpecOutSum2.AsFloat:=0;
          taSpecOutINds.AsInteger:=0;
          taSpecOutSNds.AsString:='';
          if taNDS.Active=False then taNDS.Active:=True;
          if taNds.Locate('ID',quFCardINDS.AsInteger,[]) then
          begin
            taSpecOutINds.AsInteger:=quFCardINDS.AsInteger;
            taSpecOutSNds.AsString:=taNDSNAMENDS.AsString;
          end;

          taSpecOutRNds.AsFloat:=0;
          taSpecOutSumNac.AsFloat:=0;
          taSpecOutProcNac.AsFloat:=0;
          taSpecOutCType.AsInteger:=quFCardCATEGORY.AsInteger;
          taSpecOut.Post;

          ViewDoc2.EndUpdate;
        end;
      end;
      if ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2NameG' then
      begin
        sName:=VarAsType(AEdit.EditingValue, varString);

        fmFCards.ViewFCards.BeginUpdate;
        quFCard.Active:=False;
        quFCard.SelectSQL.Clear;
        quFCard.SelectSQL.Add('SELECT first 100 ID,NAME,IMESSURE,PARENT,TCARD,INDS,LASTPRICEOUT,CATEGORY');
        quFCard.SelectSQL.Add('FROM OF_CARDS');
        quFCard.SelectSQL.Add('where UPPER(NAME) like ''%'+AnsiUpperCase(sName)+'%''');
        quFCard.SelectSQL.Add('and IACTIVE>0');
        quFCard.SelectSQL.Add('and PARENT in (SELECT ID_CLASSIF FROM RCLASSIF WHERE RIGHTS=0 AND ID_PERSONAL='+its(Person.Id)+')');
        quFCard.SelectSQL.Add('Order by NAME');

        quFCard.Active:=True;

        bAdd:=True;

        quFCard.Locate('NAME',sName,[loCaseInsensitive, loPartialKey]);
        prRowFocus(fmFCards.ViewFCards);
        fmFCards.ViewFCards.EndUpdate;

        //�������� ����� ������ � ����� ������
        fmFCards.ShowModal;
        if fmFCards.ModalResult=mrOk then
        begin
          if quFCard.RecordCount>0 then
          begin
            ViewDoc2.BeginUpdate;
            taSpecOut.Edit;
            taSpecOutIdGoods.AsInteger:=quFCardID.AsInteger;
            taSpecOutNameG.AsString:=quFCardNAME.AsString;
            taSpecOutIM.AsInteger:=quFCardIMESSURE.AsInteger;
            taSpecOutSM.AsString:=prFindKNM(quFCardIMESSURE.AsInteger,Km);
            taSpecOutKm.AsFloat:=Km;
            taSpecOutQuant.AsFloat:=1;
            taSpecOutPrice1.AsFloat:=0;
            taSpecOutSum1.AsFloat:=0;
            taSpecOutPrice2.AsFloat:=0;
            taSpecOutSum2.AsFloat:=0;
            taSpecOutINds.AsInteger:=0;
            taSpecOutSNds.AsString:='';
            taSpecOutRNds.AsFloat:=0;
            if taNDS.Active=False then taNDS.Active:=True;
            if taNds.Locate('ID',quFCardINDS.AsInteger,[]) then
            begin
              taSpecOutINds.AsInteger:=quFCardINDS.AsInteger;
              taSpecOutSNds.AsString:=taNDSNAMENDS.AsString;
            end;
            taSpecOutSumNac.AsFloat:=0;
            taSpecOutProcNac.AsFloat:=0;
            taSpecOutCType.AsInteger:=quFCardCATEGORY.AsInteger;
            taSpecOut.Post;
            ViewDoc2.EndUpdate;
            AEdit.SelectAll;
          end;
        end;
        bAdd:=False;
      end;
      if ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2SM' then
      begin
        if taSpecOutIm.AsInteger>0 then
        begin
          bAddSpec:=True;
          fmMessure.ShowModal;
          if fmMessure.ModalResult=mrOk then
          begin
            iM:=iMSel; iMSel:=0;
            Sm:=prFindKNM(iM,rK); //�������� ����� �������� � ����� �����

            taSpecOut.Edit;
            taSpecOutIM.AsInteger:=iM;
            taSpecOutKm.AsFloat:=rK;
            taSpecOutSM.AsString:=Sm;
            taSpecOut.Post;
          end;
        end;
      end;
      ViewDoc2Quant.Focused:=True;
    end else
      if ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2IdGoods' then
        if fTestKey(Key)=False then
          if taSpecOut.State in [dsEdit,dsInsert] then taSpecOut.Cancel;
  end;
end;


procedure TfmAddDoc2.ViewDoc2EditKeyPress(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
Var Km:Real;
    sName:String;
begin
  if bAdd then exit;
  with dmO do
  begin
    if (ViewDoc2.Controller.FocusedColumn.Name='ViewDoc2NameG') then
    begin
      sName:=VarAsType(AEdit.EditingValue ,varString)+Key;
//      Label2.Caption:=sName;
      if length(sName)>2 then
      begin
        quFCard.Active:=False;
        quFCard.SelectSQL.Clear;
        quFCard.SelectSQL.Add('SELECT first 2 ID,NAME,IMESSURE,PARENT,TCARD,INDS,LASTPRICEOUT,CATEGORY');
        quFCard.SelectSQL.Add('FROM OF_CARDS');
        quFCard.SelectSQL.Add('where UPPER(NAME) like ''%'+AnsiUpperCase(sName)+'%''');
        quFCard.SelectSQL.Add('and IACTIVE>0');
        quFCard.SelectSQL.Add('and PARENT in (SELECT ID_CLASSIF FROM RCLASSIF WHERE RIGHTS=0 AND ID_PERSONAL='+its(Person.Id)+')');
        quFCard.SelectSQL.Add('Order by NAME');
        quFCard.Active:=True;

        if quFCard.RecordCount=1 then
        begin
          ViewDoc2.BeginUpdate;
          taSpecOut.Edit;
          taSpecOutIdGoods.AsInteger:=quFCardID.AsInteger;
          taSpecOutNameG.AsString:=quFCardNAME.AsString;
          taSpecOutIM.AsInteger:=quFCardIMESSURE.AsInteger;
          taSpecOutSM.AsString:=prFindKNM(quFCardIMESSURE.AsInteger,Km);
          taSpecOutKm.AsFloat:=Km;
          taSpecOutQuant.AsFloat:=1;
          taSpecOutPrice1.AsFloat:=0;
          taSpecOutSum1.AsFloat:=0;
          taSpecOutPrice2.AsFloat:=0;
          taSpecOutSum2.AsFloat:=0;
          taSpecOutINds.AsInteger:=0;
          taSpecOutSNds.AsString:='';
          if taNDS.Active=False then taNDS.Active:=True;
          if taNds.Locate('ID',quFCardINDS.AsInteger,[]) then
          begin
            taSpecOutINds.AsInteger:=quFCardINDS.AsInteger;
            taSpecOutSNds.AsString:=taNDSNAMENDS.AsString;
          end;
          taSpecOutRNds.AsFloat:=0;
          taSpecOutSumNac.AsFloat:=0;
          taSpecOutProcNac.AsFloat:=0;
          taSpecOutCType.AsInteger:=quFCardCATEGORY.AsInteger;
          taSpecOut.Post;
          ViewDoc2.EndUpdate;
          AEdit.SelectAll;
          ViewDoc2NameG.Options.Editing:=False;
          ViewDoc2NameG.Focused:=True;
          Key:=#0;
        end;
      end;
    end;

  end;//}
end;

procedure TfmAddDoc2.cxLabel8Click(Sender: TObject);
begin
  acDelAll.Execute;
end;

procedure TfmAddDoc2.acDelAllExecute(Sender: TObject);
begin
  if PageControl1.ActivePageIndex=0 then
  begin
    if taSpecOut.RecordCount>0 then
    begin
      if MessageDlg('�� ������������� ������ �������� ������������ �������?',
       mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        taSpecOut.First; while not taSpecOut.Eof do taSpecOut.Delete;
      end;
    end;
  end;  
end;

procedure TfmAddDoc2.ViewDoc2SMPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
Var rK:Real;
    iM:Integer;
    Sm:String;
begin
  with dmO do
  begin
    if taSpecOutIm.AsInteger>0 then
    begin
      bAddSpec:=True;
      fmMessure.ShowModal;
      if fmMessure.ModalResult=mrOk then
      begin
        iM:=iMSel; iMSel:=0;
        Sm:=prFindKNM(iM,rK); //�������� ����� �������� � ����� �����

        taSpecOut.Edit;
        taSpecOutIM.AsInteger:=iM;
        taSpecOutKm.AsFloat:=rK;
        taSpecOutSM.AsString:=Sm;
        taSpecOut.Post;
      end;
    end;
  end;
end;

procedure TfmAddDoc2.cxButtonEdit2PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  if dmO.taClients.Active=False then dmO.taClients.Active:=True;
  if fmClients.Visible then fmClients.Close;
  if cxButtonEdit2.Tag>0 then dmO.taClients.Locate('ID',cxButtonEdit2.Tag,[]);
  fmClients.ShowModal;
  if fmClients.ModalResult=mrOk then
  begin
    cxButtonEdit2.Tag:=dmO.taClientsID.AsInteger;
    cxButtonEdit2.Text:=dmO.taClientsNAMECL.AsString;
  end else
  begin
    cxButtonEdit2.Tag:=0;
    cxButtonEdit2.Text:='';
  end;
end;

procedure TfmAddDoc2.cxButtonEdit1KeyPress(Sender: TObject; var Key: Char);
Var sName:String;
begin
  sName:=cxButtonEdit1.Text+Key;
  if Length(sName)>1 then
  begin
    with dmORep do
    begin
      quFindCli.Close;
      quFindCli.SelectSQL.Clear;
      quFindCli.SelectSQL.Add('SELECT first 2 ID,NAMECL');
      quFindCli.SelectSQL.Add('FROM OF_CLIENTS');
      quFindCli.SelectSQL.Add('where UPPER(NAMECL) like ''%'+AnsiUpperCase(sName)+'%''');
      quFindCli.Open;
      if quFindCli.RecordCount=1 then
      begin
        cxButtonEdit1.Text:=quFindCliNAMECL.AsString;
        cxButtonEdit1.Tag:=quFindCliID.AsInteger;
        Key:=#0;
      end;
      quFindCli.Close;
    end;
  end;
end;

procedure TfmAddDoc2.cxButtonEdit2KeyPress(Sender: TObject; var Key: Char);
Var sName:String;
begin
  sName:=cxButtonEdit2.Text+Key;
  if Length(sName)>1 then
  begin
    with dmORep do
    begin
      quFindCli.Close;
      quFindCli.SelectSQL.Clear;
      quFindCli.SelectSQL.Add('SELECT first 2 ID,NAMECL');
      quFindCli.SelectSQL.Add('FROM OF_CLIENTS');
      quFindCli.SelectSQL.Add('where UPPER(NAMECL) like ''%'+AnsiUpperCase(sName)+'%''');
      quFindCli.Open;
      if quFindCli.RecordCount=1 then
      begin
        cxButtonEdit2.Text:=quFindCliNAMECL.AsString;
        cxButtonEdit2.Tag:=quFindCliID.AsInteger;
        Key:=#0;
      end;
      quFindCli.Close;
    end;
  end;
end;

procedure TfmAddDoc2.MenuItem1Click(Sender: TObject);
begin
  prNExportExel5(ViewDoc2);
end;

procedure TfmAddDoc2.acMovePosExecute(Sender: TObject);
Var iDateB,iDateE,iM:INteger;
    IdCard:INteger;
    NameC:String;
    iMe:INteger;
begin
  //�������� �� ������
  IdCard:=taSpecOutIdGoods.AsInteger;
  NameC:=taSpecOutNameG.AsString;
  iMe:=taSpecOutIM.AsInteger;
  with dmO do
  begin
    if taSpecOut.RecordCount>0 then
    begin
      //�� ������
      fmSelPerSkl:=tfmSelPerSkl.Create(Application);
      with fmSelPerSkl do
      begin
        cxDateEdit1.Date:=CommonSet.DateFrom;
        quMHAll1.Active:=False;
        quMHAll1.ParamByName('IDPERSON').AsInteger:=Person.Id;
        quMHAll1.Active:=True;
        quMHAll1.First;
        if CommonSet.IdStore>0 then cxLookupComboBox1.EditValue:=CommonSet.IdStore
        else  cxLookupComboBox1.EditValue:=quMHAll1ID.AsInteger;
      end;
      fmSelPerSkl.ShowModal;
      if fmSelPerSkl.ModalResult=mrOk then
      begin
        iDateB:=Trunc(fmSelPerSkl.cxDateEdit1.Date);
        iDateE:=Trunc(fmSelPerSkl.cxDateEdit2.Date)+1;

        CommonSet.IdStore:=fmSelPerSkl.cxLookupComboBox1.EditValue;
        CommonSet.NameStore:=fmSelPerSkl.cxLookupComboBox1.Text;
        CommonSet.DateFrom:=iDateB;
        CommonSet.DateTo:=iDateE+1;

        CardSel.Id:=IdCard;
        CardSel.Name:=NameC;
        prFindSM(iMe,CardSel.mesuriment,iM);
//        CardSel.NameGr:=ClassTree.Selected.Text;

        prPreShowMove(IdCard,iDateB,iDateE,fmSelPerSkl.cxLookupComboBox1.EditValue,NameC);
        fmSelPerSkl.Release;

//        fmCardsMove.PageControl1.ActivePageIndex:=2;
        fmCardsMove.ShowModal;

      end else fmSelPerSkl.Release;
    end;
  end;
end;

procedure TfmAddDoc2.taSpecOutQuantChange(Sender: TField);
begin
  //���������� �����
  if iCol=1 then
  begin
    taSpecOutSum0.AsFloat:=taSpecOutPrice0.AsFloat*taSpecOutQuant.AsFloat;
    taSpecOutSum1.AsFloat:=taSpecOutPrice1.AsFloat*taSpecOutQuant.AsFloat;
    taSpecOutSum2.AsFloat:=taSpecOutPrice2.AsFloat*taSpecOutQuant.AsFloat;
    taSpecOutSumNac.AsFloat:=taSpecOutSum2.AsFloat-taSpecOutSum1.AsFloat;
    if taSpecOutSum1.AsFloat>0.01 then
    begin
      taSpecOutProcNac.AsFloat:=(taSpecOutSum2.AsFloat-taSpecOutSum1.AsFloat)/taSpecOutSum1.AsFloat*100;
    end;
    taSpecOutRNds.AsFloat:=RoundEx(taSpecOutSum1.AsFloat*vNds[taSpecOutINds.AsInteger]/(100+vNds[taSpecOutINds.AsInteger])*100)/100;
  end;
end;

procedure TfmAddDoc2.taSpecOutPrice1Change(Sender: TField);
begin
  //���������� ����
  if iCol=2 then
  begin
    taSpecOutSum1.AsFloat:=taSpecOutPrice1.AsFloat*taSpecOutQuant.AsFloat;
    taSpecOutSumNac.AsFloat:=taSpecOutSum2.AsFloat-taSpecOutSum1.AsFloat;
    if taSpecOutSum1.AsFloat>0.01 then
    begin
      taSpecOutProcNac.AsFloat:=(taSpecOutSum2.AsFloat-taSpecOutSum1.AsFloat)/taSpecOutSum1.AsFloat*100;
    end else taSpecOutProcNac.AsFloat:=0;
    taSpecOutRNds.AsFloat:=rv(taSpecOutSum1.AsFloat*vNds[taSpecOutINds.AsInteger]/(100+vNds[taSpecOutINds.AsInteger]));

    taSpecOutSum0.AsFloat:=taSpecOutSum1.AsFloat-taSpecOutRNds.AsFloat;
    if taSpecOutQuant.AsFloat<>0 then
      taSpecOutPrice0.AsFloat:=taSpecOutSum0.AsFloat/taSpecOutQuant.AsFloat
    else
    begin
      taSpecOutPrice0.AsFloat:=rv(taSpecOutPrice1.AsFloat*vNds[taSpecOutINds.AsInteger]/(100+vNds[taSpecOutINds.AsInteger]));
      taSpecOutSum0.AsFloat:=0;
    end;

  end;
end;

procedure TfmAddDoc2.taSpecOutSum1Change(Sender: TField);
begin
  if iCol=3 then
  begin
    if abs(taSpecOutQuant.AsFloat)>0 then taSpecOutPrice1.AsFloat:=RoundEx(taSpecOutSum1.AsFloat/taSpecOutQuant.AsFloat*100)/100;
//  taSpecOutPrice1.AsFloat:=RoundEx(taSpecOutSum1.AsFloat/taSpecOutQuant.AsFloat*100)/100;
    taSpecOutSumNac.AsFloat:=taSpecOutSum2.AsFloat-taSpecOutSum1.AsFloat;
    if abs(taSpecOutSum1.AsFloat)>0.01 then
    begin
      taSpecOutProcNac.AsFloat:=(taSpecOutSum2.AsFloat-taSpecOutSum1.AsFloat)/taSpecOutSum1.AsFloat*100;
    end else taSpecOutProcNac.AsFloat:=0;
    taSpecOutRNds.AsFloat:=rv(taSpecOutSum1.AsFloat*vNds[taSpecOutINds.AsInteger]/(100+vNds[taSpecOutINds.AsInteger]));

    taSpecOutSum0.AsFloat:=taSpecOutSum1.AsFloat-taSpecOutRNds.AsFloat;
    if taSpecOutQuant.AsFloat<>0 then
      taSpecOutPrice0.AsFloat:=taSpecOutSum0.AsFloat/taSpecOutQuant.AsFloat
    else
    begin
      taSpecOutPrice0.AsFloat:=rv(taSpecOutPrice1.AsFloat*vNds[taSpecOutINds.AsInteger]/(100+vNds[taSpecOutINds.AsInteger]));
      taSpecOutSum0.AsFloat:=0;
    end;

  end;
end;

procedure TfmAddDoc2.taSpecOutPrice2Change(Sender: TField);
begin
  //���������� ����
  if iCol=4 then
  begin

//  taSpecSum1.AsFloat:=taSpecOutPrice1.AsFloat*taSpecOutQuant.AsFloat;
    taSpecOutSum2.AsFloat:=taSpecOutPrice2.AsFloat*taSpecOutQuant.AsFloat;
    taSpecOutSumNac.AsFloat:=taSpecOutSum2.AsFloat-taSpecOutSum1.AsFloat;
    if taSpecOutSum1.AsFloat>0.01 then
    begin
      taSpecOutProcNac.AsFloat:=(taSpecOutSum2.AsFloat-taSpecOutSum1.AsFloat)/taSpecOutSum1.AsFloat*100;
    end else taSpecOutProcNac.AsFloat:=0;
  end;
end;

procedure TfmAddDoc2.taSpecOutSum2Change(Sender: TField);
begin
  if iCol=5 then
  begin
    if abs(taSpecOutQuant.AsFloat)>0 then taSpecOutPrice2.AsFloat:=RoundEx(taSpecOutSum2.AsFloat/taSpecOutQuant.AsFloat*100)/100;
    taSpecOutSumNac.AsFloat:=taSpecOutSum2.AsFloat-taSpecOutSum1.AsFloat;
    if taSpecOutSum1.AsFloat>0.01 then
    begin
      taSpecOutProcNac.AsFloat:=(taSpecOutSum2.AsFloat-taSpecOutSum1.AsFloat)/taSpecOutSum1.AsFloat*100;
    end else taSpecOutProcNac.AsFloat:=0;
  end;
end;

procedure TfmAddDoc2.taSpecOutPrice0Change(Sender: TField);
begin
// ���� ��� ���
  if iCol=6 then
  begin
    taSpecOutSum0.AsFloat:=rv(taSpecOutPrice0.AsFloat*taSpecOutQuant.AsFloat);
    taSpecOutPrice1.AsFloat:=rv(taSpecOutPrice0.AsFloat/100*vNds[taSpecOutINds.AsInteger]+taSpecOutPrice0.AsFloat);
    taSpecOutSum1.AsFloat:=rv(taSpecOutPrice1.AsFloat*taSpecOutQuant.AsFloat);

    taSpecOutRNds.AsFloat:=rv(taSpecOutSum1.AsFloat-taSpecOutSum0.AsFloat);

    taSpecOutSumNac.AsFloat:=taSpecOutSum2.AsFloat-taSpecOutSum1.AsFloat;
    if abs(taSpecOutSum1.AsFloat)>0.01 then
    begin
      taSpecOutProcNac.AsFloat:=(taSpecOutSum2.AsFloat-taSpecOutSum1.AsFloat)/taSpecOutSum1.AsFloat*100;
    end else taSpecOutProcNac.AsFloat:=0;
  end;
end;

procedure TfmAddDoc2.taSpecOutSum0Change(Sender: TField);
begin
// ����� ��� ���
  if iCol=7 then
  begin
    taSpecOutPrice0.AsFloat:=0;
    if taSpecOutQuant.AsFloat<>0 then
      taSpecOutPrice0.AsFloat:=taSpecOutSum0.AsFloat/taSpecOutQuant.AsFloat;

    taSpecOutPrice1.AsFloat:=rv(taSpecOutPrice0.AsFloat/100*vNds[taSpecOutINds.AsInteger]+taSpecOutPrice0.AsFloat);
    taSpecOutSum1.AsFloat:=rv(taSpecOutPrice1.AsFloat*taSpecOutQuant.AsFloat);

    taSpecOutRNds.AsFloat:=rv(taSpecOutSum1.AsFloat-taSpecOutSum0.AsFloat);

    taSpecOutSumNac.AsFloat:=taSpecOutSum2.AsFloat-taSpecOutSum1.AsFloat;
    if abs(taSpecOutSum1.AsFloat)>0.01 then
    begin
      taSpecOutProcNac.AsFloat:=(taSpecOutSum2.AsFloat-taSpecOutSum1.AsFloat)/taSpecOutSum1.AsFloat*100;
    end else taSpecOutProcNac.AsFloat:=0;
  end;

end;

end.
