unit SelChangeGr;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons, ExtCtrls,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, cxImageComboBox, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, FIBDataSet, pFIBDataSet, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGrid;

type
  TfmSelChangeGr = class(TForm)
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    GrSelChangeGr: TcxGrid;
    ViewSelChangeGr: TcxGridDBTableView;
    LevelSelChangeGr: TcxGridLevel;
    quSelChangeGr: TpFIBDataSet;
    dsquCaChange: TDataSource;
    quSelChangeGrID: TFIBIntegerField;
    quSelChangeGrNAME: TFIBStringField;
    quSelChangeGrFIFO: TFIBSmallIntField;
    ViewSelChangeGrID: TcxGridDBColumn;
    ViewSelChangeGrNAME: TcxGridDBColumn;
    ViewSelChangeGrFIFO: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure ViewSelChangeGrDblClick(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmSelChangeGr: TfmSelChangeGr;

implementation

uses dmOffice, CasrdsChange;

{$R *.dfm}

procedure TfmSelChangeGr.FormCreate(Sender: TObject);
begin
  GrSelChangeGr.Align:=AlClient;
end;

procedure TfmSelChangeGr.ViewSelChangeGrDblClick(Sender: TObject);
begin
  if quSelChangeGr.RecordCount>0 then
  begin
    with fmCardsChange do
    begin
      if quCaChange.Locate('IDCHANGEGR',quSelChangeGrID.AsInteger,[])=False then
      begin
        quCaChange.Append;
        quCaChangeIDCARD.AsInteger:=fmCardsChange.Tag;
        quCaChangeIDCHANGEGR.AsInteger:=quSelChangeGrID.AsInteger;
        quCaChangeCOMMENT.AsString:='';
        quCaChange.Post;
        quCaChange.Refresh;
      end;
    end;
  end;
end;

procedure TfmSelChangeGr.cxButton1Click(Sender: TObject);
begin
  if quSelChangeGr.RecordCount>0 then
  begin
    with fmCardsChange do
    begin
      if quCaChange.Locate('IDCHANGEGR',quSelChangeGrID.AsInteger,[])=False then
      begin
        quCaChange.Append;
        quCaChangeIDCARD.AsInteger:=fmCardsChange.Tag;
        quCaChangeIDCHANGEGR.AsInteger:=quSelChangeGrID.AsInteger;
        quCaChangeCOMMENT.AsString:='';
        quCaChange.Post;
        quCaChange.Refresh;
      end;
    end;
  end;
  ModalResult:=mrOk;
end;

end.
