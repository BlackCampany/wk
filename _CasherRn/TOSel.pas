unit TOSel;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxTextEdit, cxImageComboBox,
  ExtCtrls, Placemnt, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxControls, cxGridCustomView, cxGrid,
  ComCtrls, SpeedBar, ActnList, XPStyleActnCtrls, ActnMan, cxCalendar,
  cxCurrencyEdit, cxContainer, cxMemo, FR_Class;

type
  TfmTO = class(TForm)
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    StatusBar1: TStatusBar;
    GridTO: TcxGrid;
    ViewTO: TcxGridDBTableView;
    LevelTO: TcxGridLevel;
    FormPlacement1: TFormPlacement;
    Timer1: TTimer;
    amTO: TActionManager;
    acExit: TAction;
    acPerSkl: TAction;
    acExportEx: TAction;
    Memo1: TcxMemo;
    acCreate: TAction;
    SpeedItem4: TSpeedItem;
    acPrintTO: TAction;
    SpeedItem5: TSpeedItem;
    acTODelPer: TAction;
    SpeedItem6: TSpeedItem;
    ViewTOIDATE: TcxGridDBColumn;
    ViewTOREMNIN: TcxGridDBColumn;
    ViewTOREMNOUT: TcxGridDBColumn;
    ViewTOPOSTIN: TcxGridDBColumn;
    ViewTOPOSTOUT: TcxGridDBColumn;
    ViewTOVNIN: TcxGridDBColumn;
    ViewTOVNOUT: TcxGridDBColumn;
    ViewTOINV: TcxGridDBColumn;
    ViewTOQREAL: TcxGridDBColumn;
    ViewTONAMEMH: TcxGridDBColumn;
    ViewTOREMNINT: TcxGridDBColumn;
    ViewTOPOSTINT: TcxGridDBColumn;
    ViewTOPOSTOUTT: TcxGridDBColumn;
    ViewTOVNINT: TcxGridDBColumn;
    ViewTOVNOUTT: TcxGridDBColumn;
    ViewTOINVT: TcxGridDBColumn;
    ViewTOREMNOUTT: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure acExportExExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acPerSklExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure acCreateExecute(Sender: TObject);
    procedure acPrintTOExecute(Sender: TObject);
    procedure acTODelPerExecute(Sender: TObject);
    procedure Memo1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmTO: TfmTO;

implementation

uses Un1, dmOffice, SelPerSkl, DMOReps, Message;

{$R *.dfm}

procedure TfmTO.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  Timer1.Enabled:=True;
  GridTO.Align:=AlClient;
  ViewTO.RestoreFromIniFile(CurDir+GridIni);
end;

procedure TfmTO.acExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmTO.acExportExExecute(Sender: TObject);
begin
  // ������� � ������
  prNExportExel5(ViewTO);
end;

procedure TfmTO.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewTO.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmTO.acPerSklExecute(Sender: TObject);
begin
//������
  fmSelPerSkl:=tfmSelPerSkl.Create(Application);
  with fmSelPerSkl do
  begin
    cxDateEdit1.Date:=CommonSet.DateFrom;
    quMHAll1.Active:=False;
    quMHAll1.ParamByName('IDPERSON').AsInteger:=Person.Id;
    quMHAll1.Active:=True;
    quMHAll1.First;
    cxLookupComboBox1.EditValue:=quMHAll1ID.AsInteger;
    if CommonSet.IdStore>0 then  cxLookupComboBox1.EditValue:=CommonSet.IdStore;
  end;
  fmSelPerSkl.ShowModal;
  if fmSelPerSkl.ModalResult=mrOk then
  begin
    CommonSet.DateFrom:=Trunc(fmSelPerSkl.cxDateEdit1.Date);
    CommonSet.DateTo:=Trunc(fmSelPerSkl.cxDateEdit2.Date)+1;
    CommonSet.IdStore:=fmSelPerSkl.cxLookupComboBox1.EditValue;
    CommonSet.NameStore:=fmSelPerSkl.cxLookupComboBox1.Text;

    if CommonSet.DateTo>=iMaxDate then fmTO.Caption:='�������� ������ �� �� '+CommonSet.NameStore+' � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
    else fmTO.Caption:='�������� ������ �� �� '+CommonSet.NameStore+' �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);
    fmSelPerSkl.quMHAll1.Active:=False;
    fmSelPerSkl.Release;
    with dmO do
    begin
      quTO.Active:=False;
      quTO.ParamByName('DATEB').AsInteger:=Trunc(CommonSet.DateFrom);
      quTO.ParamByName('DATEE').AsInteger:=Trunc(CommonSet.DateTo);
      quTO.ParamByName('IDSKL').AsInteger:=CommonSet.IdStore;
      quTO.Active:=True;
    end;
  end else
  begin
    fmSelPerSkl.quMHAll1.Active:=False;
    fmSelPerSkl.Release;
  end;
end;

procedure TfmTO.FormShow(Sender: TObject);
begin
  Memo1.Clear;
end;

procedure TfmTO.acCreateExecute(Sender: TObject);
Var iDateB,iDateE,IdSkl,iR,iD:INteger;
    rSumB,rPostIn,rPostOut,rVnIn,rVnOut,rInv,rQReal:Real;
    rSumBT,rPostInT,rPostOutT,rVnInT,rVnOutT,rInvT,rRealTara:Real;
    StrWk:String;
    iType:SmallINt;
begin
  //������������ �� �� ������
  if not CanDo('prCreateTO') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  with dmO do
  with dmORep do
  begin
    // ������
    fmSelPerSkl:=tfmSelPerSkl.Create(Application);
    with fmSelPerSkl do
    begin
      cxDateEdit1.Date:=CommonSet.DateFrom;
      quMHAll1.Active:=False;
      quMHAll1.ParamByName('IDPERSON').AsInteger:=Person.Id;
      quMHAll1.Active:=True;
      quMHAll1.First;
      cxLookupComboBox1.EditValue:=quMHAll1ID.AsInteger;
      if CommonSet.IdStore>0 then  cxLookupComboBox1.EditValue:=CommonSet.IdStore;
    end;
    fmSelPerSkl.ShowModal;
    
    if fmSelPerSkl.ModalResult=mrOk then
    begin
      iDateB:=Trunc(fmSelPerSkl.cxDateEdit1.Date);
      iDateE:=Trunc(fmSelPerSkl.cxDateEdit2.Date);
      IdSkl:=fmSelPerSkl.cxLookupComboBox1.EditValue;

      CommonSet.IdStore:=fmSelPerSkl.cxLookupComboBox1.EditValue;
      CommonSet.NameStore:=fmSelPerSkl.cxLookupComboBox1.Text;
      CommonSet.DateFrom:=iDateB;
      CommonSet.DateTo:=iDateE+1;

      if not CanEdit(iDateB,IdSkl) then begin StatusBar1.Panels[0].Text:='������ ������.'; exit; end;

      WriteHistory('������������ �� � '+FormatDateTime('dd.mm.yyyy',iDateB)+' �� '+FormatDateTime('dd.mm.yyyy',iDateE)+'. ��������: '+Person.Name);

      if iDateE>=iMaxDate then  iDateE:=Trunc(Date+1);
      fmSelPerSkl.quMHAll1.Active:=False;
      fmSelPerSkl.Release;

      //�������
      Memo1.Clear;
      prWH('������ �������.  ������ � '+FormatDateTime('dd.mm.yyyy',iDateB)+' �� '+FormatDateTime('dd.mm.yyyy',iDateE),Memo1); Delay(10);

      for iD:=iDateB to iDateE do
      begin
        //������� �� ������
        prWH('  ������� �� '+FormatDateTime('dd.mm.yyyy',iD),Memo1); Delay(10);
        iR:=prTOFindRemB(iD,IdSkl,rSumB,rSumBT);

        if iR<>0 then
          if MessageDlg('������� �� ������ �� ���������. ���������� ������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then exit;

        Str(rSumB:12:2,StrWk);
        prWH('    ������� �� ������: '+StrWk,Memo1); Delay(10);

        //�������� �� ��������
        iR:=prTOFind(iD,IdSkl);
        if iR<>0 then //���� ������� ���������
        begin
          StrWk:='    �� �� '+FormatDateTime('dd.mm.yyyy',iD)+' ��� ����. ������� ��� �� � '+FormatDateTime('dd.mm.yyyy',iD)+'?';
          if MessageDlg(StrWk,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
          begin
            if not CanDo('prDelTO') then
            begin
              prWH('��� ���� �� �������� ��. ������ �� ��������.',Memo1); Delay(10);
              exit;
            end;
            //��������
            prWH('    ���� �������� �� � '+FormatDateTime('dd.mm.yyyy',iD),Memo1); Delay(10);
            prTODel(iD,IdSkl);
            prWH('    �������� ��.',Memo1); Delay(10);
          end else
          begin
            prWH('������ �� ��������.',Memo1); Delay(10);
            exit;
          end;
        end;

        prLog(10,iD,2,IdSkl); //������������

        //������
        prWH('    ������.',Memo1); Delay(10);
        rPostIn:=RoundEx(prTOPostIn(iD,IdSkl,fmTO.Memo1,rPostInT)*100)/100;
        //������
        prWH('    �������.',Memo1); Delay(10);
        rPostOut:=RoundEx(prTOPostOut(iD,IdSkl,fmTO.Memo1,rPostOutT)*100)/100;
        //�� ������
        prWH('    ��. ������.',Memo1); Delay(10);
        rVnIn:=RoundEx(prTOVnIn(iD,IdSkl,fmTO.Memo1,rVnInT)*100)/100;
        //�� ������
        prWH('    ��. ������.',Memo1); Delay(10);
        rVnOut:=RoundEx(prTOVnOut(iD,IdSkl,fmTO.Memo1,rVnOutT)*100)/100;
        //����������
        prWH('    ����������.',Memo1); Delay(10);
        rQReal:=RoundEx(prTOReal(iD,IdSkl,fmTO.Memo1,rRealTara)*100)/100;

        //��������������
        prWH('    ��������������.',Memo1); Delay(10);
        rInv:=RoundEx(prTOInv(iD,IdSkl,rInvT,iType)*100)/100;

        quTOSel.Active:=False;
        quTOSel.ParamByName('DATEB').AsInteger:=iD;
        quTOSel.ParamByName('DATEE').AsInteger:=iD+1;
        quTOSel.ParamByName('IDSKL').AsInteger:=IdSkl;
        quTOSel.Active:=True;

        if (iType=2) then   //��� ���������
        begin
          quTOSel.Append;
          quTOSelIDATE.AsInteger:=iD;
          quTOSelIDSTORE.AsInteger:=IdSkl;
          quTOSelREMNIN.AsFloat:=rSumB;
          quTOSelPOSTIN.AsFloat:=rPostIn;
          quTOSelPOSTOUT.AsFloat:=rPostOut;
          quTOSelVNIN.AsFloat:=rVnIn;
          quTOSelVNOUT.AsFloat:=rVnOut;
          quTOSelQREAL.AsFloat:=rQReal;
          if abs(rInv)>0.001 then
          begin
//          quTOSelREMNOUT.AsFloat:=rSumB+rPostIn-rPostOut+rVnIn-rVnOut+rInv-rQReal;
            quTOSelREMNOUT.AsFloat:=rInv;
            quTOSelINV.AsFloat:=rInv-(rSumB+rPostIn-rPostOut+rVnIn-rVnOut-rQReal);
          end else
          begin
            quTOSelINV.AsFloat:=0;
            quTOSelREMNOUT.AsFloat:=rSumB+rPostIn-rPostOut+rVnIn-rVnOut-rQReal;
          end;

          quTOSelREMNINT.AsFloat:=rSumBT;
          quTOSelPOSTINT.AsFloat:=rPostInT;
          quTOSelPOSTOUTT.AsFloat:=rPostOutT+rRealTara;
          quTOSelVNINT.AsFloat:=rVnInT;
          quTOSelVNOUTT.AsFloat:=rVnOutT;

          if abs(rInvT)>0.001 then
          begin
            quTOSelINVT.AsFloat:=rInvT-(rSumBT+rPostInT-rPostOutT-rRealTara+rVnInT-rVnOutT);
            quTOSelREMNOUTT.AsFloat:=rInvT;
          end else
          begin
            quTOSelINVT.AsFloat:=0;
            quTOSelREMNOUTT.AsFloat:=rSumBT+rPostInT-rPostOutT-rRealTara+rVnInT-rVnOutT;
          end;

          quTOSel.Post;
        end else
        begin
          quTOSel.Append;
          quTOSelIDATE.AsInteger:=iD;
          quTOSelIDSTORE.AsInteger:=IdSkl;
          quTOSelREMNIN.AsFloat:=rSumB;
          quTOSelPOSTIN.AsFloat:=rPostIn;
          quTOSelPOSTOUT.AsFloat:=rPostOut;
          quTOSelVNIN.AsFloat:=rVnIn;
          quTOSelVNOUT.AsFloat:=rVnOut;
          quTOSelINV.AsFloat:=rInv;
          quTOSelQREAL.AsFloat:=rQReal;
          quTOSelREMNOUT.AsFloat:=rSumB+rPostIn-rPostOut+rVnIn-rVnOut+rInv-rQReal;

          quTOSelREMNINT.AsFloat:=rSumBT;
          quTOSelPOSTINT.AsFloat:=rPostInT;
          quTOSelPOSTOUTT.AsFloat:=rPostOutT+rRealTara;
          quTOSelVNINT.AsFloat:=rVnInT;
          quTOSelVNOUTT.AsFloat:=rVnOutT;
          quTOSelINVT.AsFloat:=rInvT;
          quTOSelREMNOUTT.AsFloat:=rSumBT+rPostInT-rPostOutT-rRealTara+rVnInT-rVnOutT+rInvT;

          quTOSel.Post;
        end;

        quTOSel.Active:=False;

      end;
      prWH('������ ��������.',Memo1); Delay(10);

      quTO.Active:=False;
      quTO.Active:=True;
    end else
    begin
      fmSelPerSkl.quMHAll1.Active:=False;
      fmSelPerSkl.Release;
    end;
  end;
end;

procedure TfmTO.acPrintTOExecute(Sender: TObject);
Var rSum:Real;
    StrWk,StrWk1:String;
    i,j,iDate,iDateB,iDateE:Integer;
    rSumB,rSumE:Real;
    rSumT,rSumBT,rSumET:Real;
    Rec:TcxCustomGridRecord;
    iSS:INteger;
    rSum1,rSum0:Real;
    sPriceT:String;
begin
  //������
  with dmO do
  with dmORep do
  begin
    Memo1.Clear;
    if quTO.RecordCount=0 then
    begin
      Memo1.Lines.Add('�������� ������.'); delay(10);
      exit;
    end;
    Memo1.Lines.Add('����� ���� ������������ ������ ��������� ������ (��).'); delay(10);
    CloseTa(taTORep);

    rSum:=0;
    rSumB:=0;
    rSumE:=0;

    rSumT:=0;
    rSumBT:=0;
    rSumET:=0;

    iDateB:=quTOIDATE.AsInteger;
    iDateE:=quTOIDATE.AsInteger;

    //��� ����������� - ����� �� ��������� �� ������
    ViewTO.BeginUpdate;
    for i:=0 to ViewTO.Controller.SelectedRecordCount-1 do
    begin
      Rec:=ViewTO.Controller.SelectedRecords[i];

      for j:=0 to Rec.ValueCount-1 do
      begin
        if ViewTO.Columns[j].Name='ViewTOIDATE' then break;
      end;
      iDate:=Rec.Values[j];

      if quTO.Locate('IDATE',iDate,[]) then
      begin
        if i=0 then
        begin
          rSumB:=quTOREMNIN.AsFloat;
          rSum:=quTOREMNIN.AsFloat;
          rSumBT:=quTOREMNINT.AsFloat;
          rSumT:=quTOREMNINT.AsFloat;
          iDateB:=iDate;
        end;
        if i=(ViewTO.Controller.SelectedRecordCount-1) then
        begin
          rSumE:=quTOREMNOUT.AsFloat;
          rSumET:=quTOREMNOUTT.AsFloat;
          iDateE:=iDate;
        end;

        iSS:=prISS(quTOIDSTORE.AsInteger);

        //������
        taTOPostIn.Active:=False;
        taTOpostIn.ParamByName('DATEB').AsDate:=quTOIDATE.AsInteger;
        taTOpostIn.ParamByName('IDSKL').AsInteger:=quTOIDSTORE.AsInteger;
        taTOPostIn.Active:=True;

        taTOPostIn.First;
        while not taTOPostIn.Eof do
        begin
          taTORep.Append;
          taTORepsType.AsString:='������';
          taTORepsDocType.AsString:='������ �� �����������';
//          taTORepPriceType.AsString:='����� �����. ��� ���';
          taTORepPriceType.AsString:='';
          taTORepsCliName.AsString:=taTOPostInNAMECL.AsString;
          taTORepsDate.AsString:=FormatDateTime('dd.mm.yyyy',taTOPostInDATEDOC.AsDateTime);
          taTORepsDocNum.AsString:=taTOPostInNUMDOC.AsString;
          if iSS<>2 then
          begin
            taTOReprSum.AsFloat:=taTOPostInSUMIN.AsFloat;
            taTOReprSum1.AsFloat:=taTOPostInSUMIN.AsFloat-taTOPostInSUMNDS.AsFloat;
          end else
          begin
            taTOReprSum.AsFloat:=taTOPostInSUMIN.AsFloat+taTOPostInSUMNDS.AsFloat;
            taTOReprSum1.AsFloat:=taTOPostInSUMIN.AsFloat;
          end;
//          taTOReprSumO.AsFloat:=taTOPostInSUMUCH.AsFloat;
//          taTOReprSumO1.AsFloat:=taTOPostInSUMUCH.AsFloat;
          taTOReprSumO.AsFloat:=taTOPostInSUMTAR.AsFloat;
          taTOReprSumO1.AsFloat:=rv(taTOPostInSUMTAR.AsFloat);
          taTOReprSumPr.AsFloat:=0;
          taTORepsSumPr.AsString:='';
          taTORepsSumNDS.AsString:='����� ���';
          taTORep.Post;

          if iSS<>2 then  rSum:=rSum+taTOPostInSUMIN.AsFloat
          else rSum:=rSum+taTOPostInSUMIN.AsFloat;

          rSumT:=rSumT+rv(taTOPostInSUMTAR.AsFloat);

          taTOPostIn.Next;
        end;
        taTOPostIn.Active:=False;

        //���������� ������
        taTOVnIn.Active:=False;
        taTOVnIn.ParamByName('DATEB').AsDate:=quTOIDATE.AsInteger;
        taTOVnIn.ParamByName('IDSKL').AsInteger:=quTOIDSTORE.AsInteger;
        taTOVnIn.Active:=True;

        taTOVnIn.First;
        while not taTOVnIn.Eof do
        begin
          taTORep.Append;
          taTORepsType.AsString:='������';
          taTORepsDocType.AsString:='���������� ������';
          taTORepPriceType.AsString:='';
          taTORepsCliName.AsString:=taTOVnInNAMEMH.AsString;
          taTORepsDate.AsString:=FormatDateTime('dd.mm.yyyy',taTOVnInDATEDOC.AsDateTime);
          taTORepsDocNum.AsString:=taTOVnInNUMDOC.AsString;
          taTOReprSum.AsFloat:=taTOVnInSUMIN.AsFloat;
          taTOReprSum1.AsFloat:=taTOVnInSUMIN.AsFloat;
//          taTOReprSumO.AsFloat:=taTOVnInSUMUCH1.AsFloat;
//          taTOReprSumO1.AsFloat:=taTOVnInSUMUCH1.AsFloat;
          taTOReprSumO.AsFloat:=taTOVnInSUMTAR.AsFloat;
          taTOReprSumO1.AsFloat:=taTOVnInSUMTAR.AsFloat;
          taTOReprSumPr.AsFloat:=0;
          taTORepsSumPr.AsString:='';
          taTORep.Post;

          rSum:=rSum+taTOVnInSUMIN.AsFloat;
          rSumT:=rSumT+taTOVnInSUMTAR.AsFloat;

          taTOVnIn.Next;
        end;
        taTOVnIn.Active:=False;

        //���� �����������
        taTOAct.Active:=False;
        taTOAct.ParamByName('DATEB').AsDate:=quTOIDATE.AsInteger;
        taTOAct.ParamByName('IDSKL').AsInteger:=quTOIDSTORE.AsInteger;
        taTOAct.Active:=True;

        taTOAct.First;      //������� � ������
        while not taTOAct.Eof do
        begin
          taTORep.Append;
          taTORepsType.AsString:='������';
          taTORepsDocType.AsString:='������������';
          taTORepPriceType.AsString:='';
          taTORepsCliName.AsString:='��� �';
          taTORepsDate.AsString:=FormatDateTime('dd.mm.yyyy',quTOIDATE.AsInteger);
          taTORepsDocNum.AsString:=taTOActNUMDOC.AsString;
          taTOReprSum.AsFloat:=taTOActSUMIN.AsFloat;
          taTOReprSum1.AsFloat:=taTOActSUMIN.AsFloat;
//          taTOReprSumO.AsFloat:=taTOActSUMUCH.AsFloat;
//          taTOReprSumO1.AsFloat:=taTOActSUMUCH.AsFloat;
          taTOReprSumO.AsFloat:=0; //�� ����
          taTOReprSumO1.AsFloat:=0;
          taTOReprSumPr.AsFloat:=0;
          taTORepsSumPr.AsString:='';

          taTORep.Post;

          rSum:=rSum+taTOActSUMIN.AsFloat;

          taTOAct.Next;
        end;

        taTOAct.First;     //����� � ������
        while not taTOAct.Eof do
        begin
          taTORep.Append;
          taTORepsType.AsString:='������';
          taTORepsDocType.AsString:='������������';
          taTORepPriceType.AsString:='';
          taTORepsCliName.AsString:='��� �';
          taTORepsDate.AsString:=FormatDateTime('dd.mm.yyyy',quTOIDATE.AsInteger);
          taTORepsDocNum.AsString:=taTOActNUMDOC.AsString;
          taTOReprSum.AsFloat:=taTOActSUMIN.AsFloat;
          taTOReprSum1.AsFloat:=taTOActSUMIN.AsFloat;
//          taTOReprSumO.AsFloat:=taTOActSUMUCH.AsFloat;
//          taTOReprSumO1.AsFloat:=taTOActSUMUCH.AsFloat;
           //�� ���� ������
          taTOReprSumO.AsFloat:=0;
          taTOReprSumO1.AsFloat:=0;
          taTOReprSumPr.AsFloat:=0;
          taTORepsSumPr.AsString:='';
          taTORep.Post;

          rSum:=rSum-taTOActSUMIN.AsFloat;

          taTOAct.Next;
        end;
        taTOAct.Active:=False;

        //������������
        taTOComplIn.Active:=False;
        taTOComplIn.ParamByName('DATEB').AsDate:=quTOIDATE.AsInteger;
        taTOComplIn.ParamByName('IDSKL').AsInteger:=quTOIDSTORE.AsInteger;
        taTOComplIn.Active:=True;

        taTOComplIn.First;      //������� � ������
        while not taTOComplIn.Eof do
        begin
          rSum1:=0; rSum0:=0;

          taTOComplSpec.Active:=False;
          taTOComplSpec.ParamByName('IDH').AsInteger:=taTOComplInID.AsInteger;
          taTOComplSpec.Active:=True;
          taTOComplSpec.First;
          if taTOComplSpec.RecordCount>0 then
          begin
            rSum1:=taTOComplSpecSUMIN.AsFloat;
            rSum0:=taTOComplSpecSUMIN0.AsFloat;
          end;
          taTOComplSpec.Active:=False;


          taTORep.Append;
          taTORepsType.AsString:='������';
          taTORepsDocType.AsString:='������������';
          taTORepPriceType.AsString:='';
          taTORepsCliName.AsString:='������������ �';
          taTORepsDate.AsString:=FormatDateTime('dd.mm.yyyy',quTOIDATE.AsInteger);
          taTORepsDocNum.AsString:=taTOComplInNUMDOC.AsString;
          taTOReprSum.AsFloat:=rSum1;
          taTOReprSum1.AsFloat:=rSum0;
//          taTOReprSumO.AsFloat:=taTOComplSUMUCH.AsFloat;
//          taTOReprSumO1.AsFloat:=taTOComplSUMUCH.AsFloat;
          taTOReprSumO.AsFloat:=0;
          taTOReprSumO1.AsFloat:=0;
          taTOReprSumPr.AsFloat:=0;
          taTORepsSumPr.AsString:='';
          taTORepsSumNDS.AsString:='����� ���';
          taTORep.Post;

          if iSS<>2 then  rSum:=rSum+rSum1
          else rSum:=rSum+rSum0;

          taTOComplIn.Next;
        end;
        taTOComplIn.Active:=False;

        taTOComplOut.Active:=False;
        taTOComplOut.ParamByName('DATEB').AsDate:=quTOIDATE.AsInteger;
        taTOComplOut.ParamByName('IDSKL').AsInteger:=quTOIDSTORE.AsInteger;
        taTOComplOut.Active:=True;

        taTOComplOut.First;     //����� � ������
        while not taTOComplOut.Eof do
        begin
          rSum1:=0; rSum0:=0;

          taTOComplSpec.Active:=False;
          taTOComplSpec.ParamByName('IDH').AsInteger:=taTOComplOutID.AsInteger;
          taTOComplSpec.Active:=True;
          taTOComplSpec.First;
          if taTOComplSpec.RecordCount>0 then
          begin
            rSum1:=taTOComplSpecSUMIN.AsFloat;
            rSum0:=taTOComplSpecSUMIN0.AsFloat;
          end;
          taTOComplSpec.Active:=False;

          taTORep.Append;
          taTORepsType.AsString:='������';
          taTORepsDocType.AsString:='������������';
          taTORepPriceType.AsString:='';
          taTORepsCliName.AsString:='������������ �';
          taTORepsDate.AsString:=FormatDateTime('dd.mm.yyyy',quTOIDATE.AsInteger);
          taTORepsDocNum.AsString:=taTOComplOutNUMDOC.AsString;
          taTOReprSum.AsFloat:=rSum1;
          taTOReprSum1.AsFloat:=rSum0;
//          taTOReprSumO.AsFloat:=taTOComplSUMUCH.AsFloat;
//          taTOReprSumO1.AsFloat:=taTOComplSUMUCH.AsFloat;
          taTOReprSumO.AsFloat:=0;
          taTOReprSumO1.AsFloat:=0;
          taTOReprSumPr.AsFloat:=0;
          taTORepsSumPr.AsString:='';
          taTORepsSumNDS.AsString:='����� ���';
          taTORep.Post;

          if iSS<>2 then  rSum:=rSum+rSum1
          else rSum:=rSum+rSum0;

          taTOComplOut.Next;
        end;
        taTOComplOut.Active:=False;


        //��������������

        taTOInv.Active:=False;
        taTOInv.ParamByName('DATEB').AsDate:=quTOIDATE.AsInteger;
        taTOInv.ParamByName('IDSKL').AsInteger:=quTOIDSTORE.AsInteger;
        taTOInv.Active:=True;

        taTOInv.First;
        while not taTOInv.Eof do
        begin
          if ((taTOInvSUM2.AsFloat-taTOInvSUM3.AsFloat)>=0) or (taTOInvSUMTARAD.AsFloat>=0) then
          begin
            taTORep.Append;
            taTORepsType.AsString:='������';
            taTORepsDocType.AsString:='��������������';
            taTORepPriceType.AsString:='';
            taTORepsCliName.AsString:='��������������  �';
            taTORepsDate.AsString:=FormatDateTime('dd.mm.yyyy',quTOIDATE.AsInteger);
            taTORepsDocNum.AsString:=taTOInvNUMDOC.AsString;
            if taTOInvIDETAL.AsInteger=2 then //������
            begin
              taTOReprSum.AsFloat:=rv(taTOInvSUM2.AsFloat-taTOInvSUM3.AsFloat);
              taTOReprSum1.AsFloat:=rv(taTOInvSUM2.AsFloat-taTOInvSUM3.AsFloat);
            end;
            if taTOInvIDETAL.AsInteger=1 then  // �����������
            begin
              taTOReprSum1.AsFloat:=rv(taTOInvSUM2.AsFloat-taTOInvSUM1.AsFloat);
              taTOReprSum.AsFloat:=rv(taTOInvSUM2.AsFloat-taTOInvSUM11.AsFloat);
            end;

//            taTOReprSumO.AsFloat:=(taTOInvSUM21.AsFloat-taTOInvSUM3.AsFloat);
//            taTOReprSumO1.AsFloat:=(taTOInvSUM21.AsFloat-taTOInvSUM3.AsFloat);

            taTOReprSumO.AsFloat:=rv(taTOInvSUMTARAD.AsFloat);
            taTOReprSumO1.AsFloat:=rv(taTOInvSUMTARAD.AsFloat);
            taTOReprSumPr.AsFloat:=0;
            taTORepsSumPr.AsString:='';
            taTORep.Post;

//rSum:=rSum+taTOInvSUM2.AsFloat-taTOInvSUM3.AsFloat;

          end else
          begin
            taTORep.Append;
            taTORepsType.AsString:='������';
            taTORepsDocType.AsString:='��������������';
            taTORepPriceType.AsString:='';
            taTORepsCliName.AsString:='��������������  �';
            taTORepsDate.AsString:=FormatDateTime('dd.mm.yyyy',quTOIDATE.AsInteger);
            taTORepsDocNum.AsString:=taTOInvNUMDOC.AsString;

            if taTOInvIDETAL.AsInteger=2 then //������
            begin
              taTOReprSum.AsFloat:=(taTOInvSUM2.AsFloat-taTOInvSUM3.AsFloat)*(-1);
              taTOReprSum1.AsFloat:=(taTOInvSUM2.AsFloat-taTOInvSUM3.AsFloat)*(-1);
            end;
            if taTOInvIDETAL.AsInteger=1 then  // �����������
            begin
              taTOReprSum.AsFloat:=rv(taTOInvSUM2.AsFloat-taTOInvSUM1.AsFloat)*(-1);
              taTOReprSum1.AsFloat:=rv(taTOInvSUM2.AsFloat-taTOInvSUM11.AsFloat)*(-1);
            end;

//            taTOReprSumO.AsFloat:=(taTOInvSUM21.AsFloat-taTOInvSUM3.AsFloat)*(-1);
//            taTOReprSumO1.AsFloat:=(taTOInvSUM21.AsFloat-taTOInvSUM3.AsFloat)*(-1);
            taTOReprSumO.AsFloat:=rv(taTOInvSUMTARAD.AsFloat)*(-1);
            taTOReprSumO1.AsFloat:=rv(taTOInvSUMTARAD.AsFloat)*(-1);
            taTOReprSumPr.AsFloat:=0;
            taTORepsSumPr.AsString:='';
            taTORep.Post;
          end;

          if taTOInvIDETAL.AsInteger=2 then //������
          begin
            rSum:=rSum+(taTOInvSUM2.AsFloat-taTOInvSUM3.AsFloat);
            rSumT:=rSumT+rv(taTOInvSUMTARAD.AsFloat);
          end;

          if taTOInvIDETAL.AsInteger=2 then //�����������
          begin
            rSum:=rSum+(taTOInvSUM2.AsFloat-taTOInvSUM1.AsFloat);
            rSumT:=rSumT+rv(taTOInvSUMTARAD.AsFloat);
          end;

          taTOInv.Next;
        end;
        taTOInv.Active:=False;
    //


    //������ - �������
        taTOPostOut.Active:=False;
        taTOPostOut.ParamByName('DATEB').AsDate:=quTOIDATE.AsInteger;
        taTOPostOut.ParamByName('IDSKL').AsInteger:=quTOIDSTORE.AsInteger;
        taTOPostOut.Active:=True;

        taTOPostOut.First;
        while not taTOPostOut.Eof do
        begin
          taTORep.Append;
          taTORepsType.AsString:='������';
          taTORepsDocType.AsString:='�������� �����������';
          taTORepPriceType.AsString:='';
          taTORepsCliName.AsString:=taTOPostOutNAMECL.AsString;
          taTORepsDate.AsString:=FormatDateTime('dd.mm.yyyy',taTOPostOutDATEDOC.AsDateTime);
          taTORepsDocNum.AsString:=taTOPostOutNUMDOC.AsString;

          if iSS<>2 then  taTOReprSum1.AsFloat:=taTOPostOutSUMIN.AsFloat
          else taTOReprSum1.AsFloat:=taTOPostOutSUMIN.AsFloat-taTOPostOutSUMNDS.AsFloat;

          taTOReprSum.AsFloat:=taTOPostOutSUMIN.AsFloat;
//          taTOReprSumO.AsFloat:=taTOPostOutSUMUCH.AsFloat;
//          taTOReprSumO1.AsFloat:=taTOPostOutSUMUCH.AsFloat;
          taTOReprSumO.AsFloat:=taTOPostOutSUMTAR.AsFloat;
          taTOReprSumO1.AsFloat:=taTOPostOutSUMTAR.AsFloat;
          taTOReprSumPr.AsFloat:=0;
          taTORepsSumPr.AsString:='';
          taTORep.Post;

          if iSS<>2 then  rSum:=rSum-taTOPostOutSUMIN.AsFloat
          else rSum:=rSum-taTOPostOutSUMIN.AsFloat+taTOPostOutSUMNDS.AsFloat;

          rSumT:=rSumT-taTOPostOutSUMTAR.AsFloat;

          taTOPostOut.Next;
        end;
        taTOPostOut.Active:=False;

    //���������� ������
        taTOVnOut.Active:=False;
        taTOVnOut.ParamByName('DATEB').AsDate:=quTOIDATE.AsInteger;
        taTOVnOut.ParamByName('IDSKL').AsInteger:=quTOIDSTORE.AsInteger;
        taTOVnOut.Active:=True;

        taTOVnOut.First;
        while not taTOVnOut.Eof do
        begin
          taTORep.Append;
          taTORepsType.AsString:='������';
          taTORepsDocType.AsString:='���������� ������';
          taTORepPriceType.AsString:='';
          taTORepsCliName.AsString:=taTOVnOutNAMEMH.AsString;
          taTORepsDate.AsString:=FormatDateTime('dd.mm.yyyy',taTOVnOutDATEDOC.AsDateTime);
          taTORepsDocNum.AsString:=taTOVnOutNUMDOC.AsString;
          taTOReprSum.AsFloat:=taTOVnOutSUMIN.AsFloat;
          taTOReprSum1.AsFloat:=taTOVnOutSUMIN.AsFloat;
//          taTOReprSumO.AsFloat:=taTOVnOutSUMUCH.AsFloat;
//          taTOReprSumO1.AsFloat:=taTOVnOutSUMUCH.AsFloat;
          taTOReprSumO.AsFloat:=taTOVnOutSUMTAR.AsFloat;
          taTOReprSumO1.AsFloat:=taTOVnOutSUMTAR.AsFloat;
          taTOReprSumPr.AsFloat:=0;
          taTORepsSumPr.AsString:='';
          taTORep.Post;

          rSum:=rSum-taTOVnOutSUMIN.AsFloat;
          rSumT:=rSumT-taTOVnOutSUMTAR.AsFloat;

          taTOVnOut.Next;
        end;
        taTOVnOut.Active:=False;


    //������ �� �����
        taTOOutB.Active:=False;
        taTOOutB.ParamByName('DATEB').AsDate:=quTOIDATE.AsInteger;
        taTOOutB.ParamByName('IDSKL').AsInteger:=quTOIDSTORE.AsInteger;
        taTOOutB.Active:=True;

        taTOOutB.First;
        while not taTOOutB.Eof do
        begin
          StrWk:=taTOOutBOPER.AsString;
          StrWk1:='�����';
          sPriceT:='����� ����.';

          if taTOOutBOPER.AsString='Sale' then StrWk:='������� ��������';
          if taTOOutBOPER.AsString='SaleBank' then StrWk:='������� �� ���������� �����';
          if taTOOutBOPER.AsString='Ret' then StrWk:='������� ��������';
          if taTOOutBOPER.AsString='RetBank' then StrWk:='������� �� ���������� �����';

          if taTOOutBOPER.AsString='SalePC' then
          begin
            StrWk:='������� ��. ������';
            StrWk1:='��������';
          end;
          if taTOOutBOPER.AsString='Del' then
          begin
            StrWk:='�������� �� ���������';
            StrWk1:='��������';
          end;

          if taTOOutBOPER.AsString='��' then
          begin
            StrWk:='�������� ('+taTOOutBOPER.AsString+')';
            StrWk1:=taTOOutBCOMMENT.AsString;
            sPriceT:='';
          end;

          taTORep.Append;
          taTORepsType.AsString:='������';
          taTORepsDocType.AsString:=StrWk1;
          taTORepsCliName.AsString:=StrWk;

          taTORepPriceType.AsString:=sPriceT;

          taTORepsDate.AsString:=FormatDateTime('dd.mm.yyyy',taTOOutBDATEDOC.AsDateTime);
          taTORepsDocNum.AsString:=taTOOutBNUMDOC.AsString;


          taTOReprSum.AsFloat:=0;
          taTOReprSum1.AsFloat:=0;

          taTOOutBSpec.Active:=False;
          taTOOutBSpec.ParamByName('IDHEAD').AsInteger:=taTOOutBID.AsInteger;
          taTOOutBSpec.Active:=True;
          taTOOutBSpec.First;

          if taTOOutBSpec.RecordCount>0 then
          begin
            taTOReprSum.AsFloat:=taTOOutBSpecSUMIN.AsFloat;
            taTOReprSum1.AsFloat:=taTOOutBSpecSUMIN0.AsFloat;
          end;
          taTOOutBSpec.Active:=False;

          taTOReprSumO.AsFloat:=0;
          taTOReprSumO1.AsFloat:=0;

          if sPriceT>'' then
          begin
            taTOReprSumPr.AsFloat:=taTOOutBSUMUCH.AsFloat;
            Str(taTOOutBSUMUCH.AsFloat:8:2,StrWk);
            taTORepsSumPr.AsString:=StrWk;
          end else
          begin
            taTOReprSumPr.AsFloat:=0;
            taTORepsSumPr.AsString:='';
          end;

          taTORep.Post;

          if iSS<>2 then  rSum:=rSum+taTOReprSum.AsFloat
          else rSum:=rSum+taTOReprSum1.AsFloat;

          taTOOutB.Next;
        end;
        taTOOutB.Active:=False;

        //������ �� ����� - ������
        taTOOutBCat.Active:=False;
        taTOOutBCat.ParamByName('DATEB').AsDate:=quTOIDATE.AsInteger;
        taTOOutBCat.ParamByName('ICAT').AsInteger:=2;
        taTOOutBCat.ParamByName('IDSKL').AsInteger:=quTOIDSTORE.AsInteger;
        taTOOutBCat.Active:=True;

        taTOOutCCat.Active:=False;
        taTOOutCCat.ParamByName('DATEB').AsDate:=quTOIDATE.AsInteger;
        taTOOutCCat.ParamByName('ICAT').AsInteger:=2;
        taTOOutCCat.ParamByName('IDSKL').AsInteger:=quTOIDSTORE.AsInteger;
        taTOOutCCat.Active:=True;

        if (taTOOutBCatRSUM.AsFloat<>0)or(taTOOutCCatRSUM.AsFloat<>0) then
        begin
          taTORep.Append;
          taTORepsType.AsString:='������';
          taTORepsDocType.AsString:='�����';
          taTORepPriceType.AsString:='���� ����.';
          taTORepsCliName.AsString:='���������� ����� � �.�.';
          taTORepsDate.AsString:='';
          taTORepsDocNum.AsString:='';
          taTOReprSum.AsFloat:=taTOOutCCatRSUM.AsFloat;
          taTOReprSum1.AsFloat:=0;
          taTOReprSumO.AsFloat:=0;
          taTOReprSumO1.AsFloat:=0;
          taTOReprSumPr.AsFloat:=taTOOutBCatRSUM.AsFloat;

          Str(taTOOutBCatRSUM.AsFloat:8:2,StrWk);
          taTORepsSumPr.AsString:=StrWk;

          taTORep.Post;
        end;
        taTOOutBCat.Active:=False;
        taTOOutCCat.Active:=False;

    //������ �� ����� - ������
        taTOOutBCat.Active:=False;
        taTOOutBCat.ParamByName('DATEB').AsDate:=quTOIDATE.AsInteger;
        taTOOutBCat.ParamByName('ICAT').AsInteger:=3;
        taTOOutBCat.ParamByName('IDSKL').AsInteger:=quTOIDSTORE.AsInteger;
        taTOOutBCat.Active:=True;

        taTOOutCCat.Active:=False;
        taTOOutCCat.ParamByName('DATEB').AsDate:=quTOIDATE.AsInteger;
        taTOOutCCat.ParamByName('ICAT').AsInteger:=3;
        taTOOutCCat.ParamByName('IDSKL').AsInteger:=quTOIDSTORE.AsInteger;
        taTOOutCCat.Active:=True;

        if (taTOOutBCatRSUM.AsFloat<>0)or(taTOOutCCatRSUM.AsFloat<>0) then
        begin
          taTORep.Append;
          taTORepsType.AsString:='������';
          taTORepsDocType.AsString:='�����';
          taTORepPriceType.AsString:='���� ����.';
          if taTOOutBCatRSUM.AsFloat>0 then taTORepsCliName.AsString:='������ (������) � �.�.'
                                       else taTORepsCliName.AsString:='������ (�������) � �.�.';
          taTORepsDate.AsString:='';
          taTORepsDocNum.AsString:='';
          taTOReprSum.AsFloat:=taTOOutCCatRSUM.AsFloat;
          taTOReprSum1.AsFloat:=0;
          taTOReprSumO.AsFloat:=0;
          taTOReprSumO1.AsFloat:=0;
          taTOReprSumPr.AsFloat:=taTOOutBCatRSUM.AsFloat;

          Str(taTOOutBCatRSUM.AsFloat:8:2,StrWk);
          taTORepsSumPr.AsString:=StrWk;

          taTORep.Post;
        end;
        taTOOutBCat.Active:=False;
        taTOOutCCat.Active:=False;


        //������ ���������� �� �������
        taTOOutR.Active:=False;
        taTOOutR.ParamByName('DATEB').AsDate:=quTOIDATE.AsInteger;
        taTOOutR.ParamByName('IDSKL').AsInteger:=quTOIDSTORE.AsInteger;
        taTOOutR.Active:=True;

        taTOOutR.First;
        while not taTOOutR.Eof do
        begin
          if taTOOutRSUMIN.AsFloat>=0 then
          begin
            taTORep.Append;
            taTORepsType.AsString:='������';
            taTORepsDocType.AsString:='���������� �� �������';
            taTORepPriceType.AsString:='���� ����.';
            taTORepsCliName.AsString:=taTOOutRNAMECL.AsString;
            taTORepsDate.AsString:=FormatDateTime('dd.mm.yyyy',taTOOutRDATEDOC.AsDateTime);
            taTORepsDocNum.AsString:=taTOOutRNUMDOC.AsString;

            taTOReprSum.AsFloat:=0;
            taTOReprSum1.AsFloat:=0;

            taTOOutRSpec.Active:=False;
            taTOOutRSpec.ParamByName('IDHEAD').AsInteger:=taTOOutRID.AsInteger;
            taTOOutRSpec.Active:=True;
            taTOOutRSpec.First;

            if taTOOutRSpec.RecordCount>0 then
            begin
              taTOOutRSpec.First;
              while not taTOOutRSpec.Eof do
              begin
                if taTOOutRSpecCATEGORY.AsInteger=1 then
                begin
                  taTOReprSum.AsFloat:=taTOOutRSpecSUMIN.AsFloat;
                  taTOReprSum1.AsFloat:=taTOOutRSpecSUMIN0.AsFloat;
                end;
                if taTOOutRSpecCATEGORY.AsInteger=4 then
                begin
                  taTOReprSumO.AsFloat:=taTOOutRSpecSUMIN.AsFloat;
                  taTOReprSumO1.AsFloat:=taTOOutRSpecSUMIN.AsFloat;

                  rSumT:=rSumT-rv(taTOOutRSpecSUMIN.AsFloat);
                end;
                taTOOutRSpec.Next;
              end;
            end;
            taTOOutRSpec.Active:=False;

            taTOReprSumPr.AsFloat:=taTOOutRSUMUCH.AsFloat;
            Str(taTOOutRSUMUCH.AsFloat:8:2,StrWk);
            taTORepsSumPr.AsString:=StrWk;

            taTORep.Post;

          end else
          begin
            taTORep.Append;
            taTORepsType.AsString:='������';
            taTORepsDocType.AsString:='���������� �� ������� (�������)';
            taTORepPriceType.AsString:='���� ����.';
            taTORepsCliName.AsString:=taTOOutRNAMECL.AsString;
            taTORepsDate.AsString:=FormatDateTime('dd.mm.yyyy',taTOOutRDATEDOC.AsDateTime);
            taTORepsDocNum.AsString:=taTOOutRNUMDOC.AsString;

            taTOReprSum.AsFloat:=0;
            taTOReprSum1.AsFloat:=0;

            taTOOutRSpec.Active:=False;
            taTOOutRSpec.ParamByName('IDHEAD').AsInteger:=taTOOutRID.AsInteger;
            taTOOutRSpec.Active:=True;
            taTOOutRSpec.First;

            if taTOOutRSpec.RecordCount>0 then
            begin
              taTOOutRSpec.First;
              while not taTOOutRSpec.Eof do
              begin
                if taTOOutRSpecCATEGORY.AsInteger=1 then
                begin
                  taTOReprSum.AsFloat:=taTOOutRSpecSUMIN.AsFloat;
                  taTOReprSum1.AsFloat:=taTOOutRSpecSUMIN0.AsFloat;
                end;
                if taTOOutRSpecCATEGORY.AsInteger=4 then
                begin
                  taTOReprSumO.AsFloat:=taTOOutRSpecSUMIN.AsFloat;
                  taTOReprSumO1.AsFloat:=taTOOutRSpecSUMIN.AsFloat;

                  rSumT:=rSumT+rv(taTOOutRSpecSUMIN.AsFloat);
                end;
                taTOOutRSpec.Next;
              end;
            end;
            taTOOutRSpec.Active:=False;

            taTOReprSumPr.AsFloat:=taTOOutRSUMUCH.AsFloat;

            Str(taTOOutRSUMUCH.AsFloat:8:2,StrWk);
            taTORepsSumPr.AsString:=StrWk;

            taTORep.Post;
          end;

          if iSS<>2 then  rSum:=rSum+taTOReprSum.AsFloat
          else rSum:=rSum+taTOReprSum1.AsFloat;

          taTOOutR.Next;
       end;
       taTOOutR.Active:=False;
      end;
    end;
    ViewTO.EndUpdate;

    Memo1.Lines.Add('������������ ��.'); delay(10);

    frRep1.LoadFromFile(CurDir + 'to.frf');

    if iDateB=iDateE then
      frVariables.Variable['sPeriod']:=FormatDateTime('dd.mm.yyyy',iDateB)
    else
      frVariables.Variable['sPeriod']:=' ������ � '+FormatDateTime('dd.mm.yyyy',iDateB)+' �� '+FormatDateTime('dd.mm.yyyy',iDateE);

    frVariables.Variable['Depart']:=quTONAMEMH.AsString;
    frVariables.Variable['RemnB']:=rSumB;
    frVariables.Variable['RemnE']:=rSumE;
    frVariables.Variable['RemnBT']:=rSumBT;
    frVariables.Variable['RemnET']:=rSumET;

    frRep1.ReportName:='�������� �����.';
    frRep1.PrepareReport;
    frRep1.ShowPreparedReport;

    taTORep.Active:=False;
  end;
end;

procedure TfmTO.acTODelPerExecute(Sender: TObject);
Var iDateB,IdSkl:INteger;
begin
  //������� �� ������
  if not CanDo('prDelTO') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  with dmO do
  with dmORep do
  begin
    // ������
    fmSelPerSkl:=tfmSelPerSkl.Create(Application);
    with fmSelPerSkl do
    begin
      cxDateEdit1.Date:=quTOIDATE.AsInteger;
      quMHAll1.Active:=False;
      quMHAll1.ParamByName('IDPERSON').AsInteger:=Person.Id;
      quMHAll1.Active:=True;
      quMHAll1.First;
      cxLookupComboBox1.EditValue:=quTOIDSTORE.AsInteger;
    end;
    fmSelPerSkl.ShowModal;
    if fmSelPerSkl.ModalResult=mrOk then
    begin
      iDateB:=Trunc(fmSelPerSkl.cxDateEdit1.Date);
      IdSkl:=fmSelPerSkl.cxLookupComboBox1.EditValue;

      if not CanEdit(iDateB,IdSkl) then begin StatusBar1.Panels[0].Text:='������ ������.'; exit; end;

      fmSelPerSkl.quMHAll1.Active:=False;
      fmSelPerSkl.Release;

      //�������
      Memo1.Clear;
      Memo1.Lines.Add('������ ��������.  ������ � '+FormatDateTime('dd.mm.yyyy',iDateB)+'.'); Delay(10);
      prTODel(iDateB,IdSkl);
      Memo1.Lines.Add('    �������� ��.'); Delay(10);

      quTO.Active:=False;
      quTO.Active:=True;
    end else
    begin
      fmSelPerSkl.quMHAll1.Active:=False;
      fmSelPerSkl.Release;
    end;
  end;
end;

procedure TfmTO.Memo1DblClick(Sender: TObject);
begin
  fmMessage:=tfmMessage.Create(Application);
  fmMessage.Memo1.Lines:=Memo1.Lines;
  fmMessage.ShowModal;
  fmMessage.Release;
end;

end.
