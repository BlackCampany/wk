object fmRMol: TfmRMol
  Left = 285
  Top = 321
  BorderStyle = bsDialog
  Caption = #1054#1090#1074#1077#1090#1089#1090#1074#1077#1085#1085#1099#1077' '#1083#1080#1094#1072
  ClientHeight = 191
  ClientWidth = 458
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 20
    Top = 20
    Width = 50
    Height = 13
    Caption = #1044#1080#1088#1077#1082#1090#1086#1088
  end
  object Label2: TLabel
    Left = 20
    Top = 48
    Width = 104
    Height = 13
    Caption = #1047#1072#1074'. '#1087#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1086#1084
  end
  object Label3: TLabel
    Left = 20
    Top = 76
    Width = 47
    Height = 13
    Caption = #1058#1077#1093#1085#1086#1083#1086#1075
  end
  object Label4: TLabel
    Left = 20
    Top = 104
    Width = 51
    Height = 13
    Caption = #1041#1091#1093#1075#1072#1083#1090#1077#1088
  end
  object Panel2: TPanel
    Left = 0
    Top = 143
    Width = 458
    Height = 48
    Align = alBottom
    BevelInner = bvLowered
    Color = 16756912
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 100
      Top = 8
      Width = 97
      Height = 33
      Caption = #1054#1082
      Default = True
      ModalResult = 1
      TabOrder = 0
      Colors.Normal = clCaptionText
      Colors.Hot = 14869218
      Colors.Pressed = clGray
      LookAndFeel.Kind = lfOffice11
      LookAndFeel.NativeStyle = False
    end
    object cxButton2: TcxButton
      Left = 256
      Top = 8
      Width = 97
      Height = 33
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      Colors.Normal = clCaptionText
      Colors.Hot = 14869218
      Colors.Pressed = clGray
      LookAndFeel.Kind = lfOffice11
      LookAndFeel.NativeStyle = False
    end
  end
  object cxTextEdit1: TcxTextEdit
    Left = 144
    Top = 16
    TabOrder = 1
    Text = 'cxTextEdit1'
    Width = 289
  end
  object cxTextEdit2: TcxTextEdit
    Left = 144
    Top = 44
    TabOrder = 2
    Text = 'cxTextEdit2'
    Width = 289
  end
  object cxTextEdit3: TcxTextEdit
    Left = 144
    Top = 72
    TabOrder = 3
    Text = 'cxTextEdit3'
    Width = 289
  end
  object cxTextEdit4: TcxTextEdit
    Left = 144
    Top = 100
    TabOrder = 4
    Text = 'cxTextEdit4'
    Width = 289
  end
end
