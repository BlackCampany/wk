unit StopListCash;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ComCtrls, XPStyleActnCtrls, ActnList, ActnMan;

type
  TfmStopListCash = class(TForm)
    Panel1: TPanel;
    cxButton8: TcxButton;
    cxButton9: TcxButton;
    cxButton3: TcxButton;
    ViewSl: TcxGridDBTableView;
    LevelSL: TcxGridLevel;
    GridSL: TcxGrid;
    ViewSlSIFR: TcxGridDBColumn;
    ViewSlNAME: TcxGridDBColumn;
    ViewSlBACKBGR: TcxGridDBColumn;
    TimerSl: TTimer;
    StatusBar1: TStatusBar;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton4: TcxButton;
    amStopList: TActionManager;
    acEditSL: TAction;
    procedure FormCreate(Sender: TObject);
    procedure cxButton8Click(Sender: TObject);
    procedure cxButton9Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure TimerSlTimer(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure ViewSlCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure cxButton1Click(Sender: TObject);
    procedure acEditSLExecute(Sender: TObject);
    procedure ViewSlDblClick(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmStopListCash: TfmStopListCash;

implementation

uses Dm, Menu, Un1, Calc;

{$R *.dfm}

procedure TfmStopListCash.FormCreate(Sender: TObject);
begin
  GridSL.Align:=AlClient;
end;

procedure TfmStopListCash.cxButton8Click(Sender: TObject);
begin
  with dmC do
  begin
    try
      quMenuSL.Prior;
    except
    end;
  end;
end;

procedure TfmStopListCash.cxButton9Click(Sender: TObject);
begin
  with dmC do
  begin
    try
      quMenuSL.Next;
    except
    end;
  end;
end;

procedure TfmStopListCash.cxButton3Click(Sender: TObject);
begin
  Close;
end;

procedure TfmStopListCash.TimerSlTimer(Sender: TObject);
Var iSifr:INteger;
begin
  if bStopList then exit;  //���� ���������� ����� ������� - � ��� ����� ������ ������ �� ����.
  with dmC do
  begin
    try
      fmStopListCash.ViewSl.BeginUpdate;
      iSifr:=0;
      if quMenuSL.RecordCount>0 then iSifr:=quMenuSLSIFR.AsInteger;


      quMenuSL.Active:=False;
      quMenuSL.Active:=True;

      if iSifr>0 then quMenuSL.Locate('SIFR',iSifr,[]);

      StatusBar1.Panels[0].Text:=FormatDateTime('hh:nn:ss',now);
    finally
      fmStopListCash.ViewSl.EndUpdate;
    end;
  end;
end;

procedure TfmStopListCash.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  TimerSl.Enabled:=False;
end;

procedure TfmStopListCash.FormShow(Sender: TObject);
begin
  if CanDo('prNotEditStopList')=False then   //��������� �������������
  begin
    cxButton1.Visible:=True;
    cxButton2.Visible:=True;
    cxButton4.Visible:=True;
    acEditSL.Enabled:=True;
  end else
  begin //�� ��������� ��� ���������� ������� �����
    cxButton1.Visible:=False;
    cxButton2.Visible:=False;
    cxButton4.Visible:=False;
    acEditSL.Enabled:=False;
 end;
  TimerSl.Enabled:=True;
end;

procedure TfmStopListCash.ViewSlCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  i:Integer;
  sVal:String;
  rVal:Real;
begin
//  if AViewInfo.GridRecord.Selected then exit;

  for i:=0 to ViewSl.ColumnCount-1 do
  begin
    if ViewSl.Columns[i].Name='ViewSlBACKBGR' then
    begin
      sVal := VarAsType(AViewInfo.GridRecord.DisplayTexts[i], varString);
      rVal:=StrToFloatDef(sVal,0);

      if rVal<=0 then
      begin
        ACanvas.Canvas.Brush.Color:=$00DDBBFF;
      end
      else
      begin
      //  ACanvas.Canvas.Brush.Bitmap := ABitmap;
      end;
    end;
  end;
end;

procedure TfmStopListCash.cxButton1Click(Sender: TObject);
begin
  with dmC do
  begin
    bBludoSel:=False;
    bStopList:=True;
    if quMenu.RecordCount>0 then
    begin
      try
        fmMenuClass.ViewM.BeginUpdate;
        quMenu.FullRefresh;
      finally
        fmMenuClass.ViewM.EndUpdate;
      end;
    end;

    fmMenuClass.ShowModal;

    bStopList:=False;
  end;
end;

procedure TfmStopListCash.acEditSLExecute(Sender: TObject);
Var n:INteger;
begin
  //��������
  if dmC.quMenuSL.RecordCount=0 then exit;
  n:=0;
  while (TimerSl.Enabled=False)and(n<10) do
  begin
    delay(100);
    inc(n);
  end;
  if TimerSl.Enabled then
  begin
    with dmC do
    begin

      fmCalc.CalcEdit1.Value:=quMenuSLBACKBGR.AsFloat;
      fmCalc.ShowModal;
      if fmCalc.ModalResult=mrOk then
      begin
        prAddMove(1,quMenuSLSIFR.AsInteger,fmCalc.CalcEdit1.Value);
        quMenuSL.Refresh;
      end;
    end;
  end;  
end;

procedure TfmStopListCash.ViewSlDblClick(Sender: TObject);
begin
  acEditSL.Execute;
end;

procedure TfmStopListCash.cxButton4Click(Sender: TObject);
Var n:INteger;
begin
  //�������
  if dmC.quMenuSL.RecordCount=0 then exit;
  n:=0;
  while (TimerSl.Enabled=False)and(n<10) do
  begin
    delay(100);
    inc(n);
  end;
  if TimerSl.Enabled then
  begin
    with dmC do
    begin
      prAddMove(3,quMenuSLSIFR.AsInteger,0);

      ViewSL.BeginUpdate;
      quMenuSL.FullRefresh;
      ViewSL.EndUpdate;
    end;
  end;
end;

end.
