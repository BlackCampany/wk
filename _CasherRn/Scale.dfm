object fmScale: TfmScale
  Left = 435
  Top = 230
  Width = 990
  Height = 666
  Caption = #1042#1077#1089#1099
  Color = 16776176
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 982
    Height = 45
    Align = alTop
    BevelInner = bvLowered
    Color = 16776176
    TabOrder = 0
    object Label1: TLabel
      Left = 24
      Top = 16
      Width = 27
      Height = 13
      Caption = #1042#1077#1089#1099
    end
    object cxLookupComboBox1: TcxLookupComboBox
      Left = 68
      Top = 12
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Caption = #1050#1086#1076
          FieldName = 'ID'
        end
        item
          Caption = #1053#1072#1079#1074#1072#1085#1080#1077
          FieldName = 'NAME'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListOptions.SyncMode = True
      Properties.ListSource = dmC.dsquScales
      Properties.MaxLength = 0
      Properties.OnChange = cxLookupComboBox1PropertiesChange
      Style.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 0
      Width = 177
    end
    object cxButton1: TcxButton
      Left = 344
      Top = 11
      Width = 65
      Height = 23
      Caption = #1047#1072#1075#1088'. '#1080#1079#1084'.'
      TabOrder = 1
      OnClick = cxButton1Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 412
      Top = 11
      Width = 65
      Height = 23
      Caption = #1047#1072#1075#1088'. '#1074#1099#1076'.'
      TabOrder = 2
      OnClick = cxButton2Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton3: TcxButton
      Left = 844
      Top = 4
      Width = 77
      Height = 33
      Caption = #1047#1072#1082#1088#1099#1090#1100
      Default = True
      TabOrder = 3
      OnClick = cxButton3Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton4: TcxButton
      Left = 252
      Top = 12
      Width = 21
      Height = 21
      TabOrder = 4
      OnClick = cxButton4Click
      Glyph.Data = {
        26040000424D2604000000000000360000002800000012000000120000000100
        180000000000F003000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCEC7C6A5B6A584AE8484AA84
        94A694B5B2ADFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFBDC7B55AAE5A29AE2931BE3929C34231BE4A52B26394A6
        94FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFBDC7
        B5429E3110A2084ABA4AA5CFA552CB6B29CB4A29CB4A42BA5294A694FFFFFFFF
        FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFD6CFCE63A2521092006BBA63CE
        D7CED6D3D6BDD3BD8CD39C39CB5A21C73952AE5AFFFFFFFFFFFFFFFFFFFFFFFF
        0000FFFFFFFFFFFFFFFFFFBDC3B5318E18188A005AAE4AC6D7C6DEDBDEC6D3C6
        D6D3D69CCFA529C73929BA3194A68CFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
        FFFFFFBDC3B5529E4221860021920842AA3194CF945AC35AA5CFADCED3CE4AC7
        5218BA187BAA7BFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFC6C7BD94BE
        8484BA73C6DFBD73BE6B189A1018A6108CCB8CDEDFDE52C35210B21084AE7BFF
        FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFCEC7C6ADC7A5ADCFA5F7FBF7E7
        EFDE6BB65A63BA5AD6E3CEDEE3D64ABA4231AE29A5B69CFFFFFFFFFFFFFFFFFF
        0000FFFFFFFFFFFFFFFFFFD6CBC6BDCBBDADCFA5CEE3C6FFFFFFFFFFFFFFFBFF
        F7F7F7A5D39C4AB63973B66BFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
        FFFFFFFFFFFFC6C7C6BDCFB5B5D3A5C6DBB5D6E7CECEE7CEA5D3946BBA5A73B6
        6BBDC7B5FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFC6C7C6C6CBBDB5CFADADCB9C9CC78C94BE849CBE94C6C7BDFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6CBC6CE
        C7C6C6CBC6C6CBBDC6CBC6D6CFCEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFF0000}
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton6: TcxButton
      Left = 580
      Top = 11
      Width = 65
      Height = 23
      Hint = #1054#1073#1085#1086#1074#1080#1090#1100
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100' '
      TabOrder = 5
      OnClick = cxButton6Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton8: TcxButton
      Left = 720
      Top = 11
      Width = 113
      Height = 23
      Hint = #1054#1073#1085#1086#1074#1080#1090#1100' '#1080#1079' '#1082#1072#1088#1090#1086#1095#1077#1082' '#1090#1086#1074#1072#1088#1072
      Caption = #1047#1072#1075#1088#1091#1079#1082#1072' '#1101#1090#1080#1082#1077#1090#1086#1082
      TabOrder = 6
      OnClick = cxButton8Click
      LookAndFeel.Kind = lfOffice11
    end
  end
  object GridScale: TcxGrid
    Left = 12
    Top = 56
    Width = 781
    Height = 445
    TabOrder = 1
    LookAndFeel.Kind = lfOffice11
    object ViewScale: TcxGridDBTableView
      PopupMenu = PopupMenu1
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dmC.dsquScaleItems
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.MultiSelect = True
      OptionsView.GroupByBox = False
      object ViewScaleSTATUS: TcxGridDBColumn
        Caption = #1057#1090#1072#1090#1091#1089
        DataBinding.FieldName = 'STATUS'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Images = dmC.imState
        Properties.Items = <
          item
            Description = #1048#1079#1084'.'
            ImageIndex = 2
            Value = 0
          end
          item
            Description = #1047#1072#1075#1088'.'
            ImageIndex = 0
            Value = 1
          end>
        Width = 58
      end
      object ViewScalePLU: TcxGridDBColumn
        DataBinding.FieldName = 'PLU'
        Width = 47
      end
      object ViewScaleSIFR: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1090#1086#1074#1072#1088#1072
        DataBinding.FieldName = 'SIFR'
        Width = 53
      end
      object ViewScaleNAME1: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077' 1'
        DataBinding.FieldName = 'NAME1'
        Width = 164
      end
      object ViewScaleNAME2: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077' 2'
        DataBinding.FieldName = 'NAME2'
        Width = 172
      end
      object ViewScalePRICE: TcxGridDBColumn
        Caption = #1062#1077#1085#1072
        DataBinding.FieldName = 'PRICE'
      end
      object ViewScaleSHELFLIFE: TcxGridDBColumn
        Caption = #1057#1088#1086#1082' '#1088#1077#1072#1083'.'
        DataBinding.FieldName = 'SHELFLIFE'
      end
      object ViewScaleTAREW: TcxGridDBColumn
        Caption = #1042#1077#1089' '#1090#1072#1088#1099
        DataBinding.FieldName = 'TAREW'
        Width = 51
      end
      object ViewScaleMESSNO: TcxGridDBColumn
        Caption = #1060#1086#1088#1084#1072#1090' '#1101#1090#1080#1082#1077#1090#1082#1080
        DataBinding.FieldName = 'MESSNO'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Items = <
          item
            Description = '1'
            ImageIndex = 0
            Value = 0
          end
          item
            Description = '2'
            Value = 1
          end
          item
            Description = '3'
            Value = 2
          end>
        Width = 52
      end
    end
    object LevelScale: TcxGridLevel
      GridView = ViewScale
    end
  end
  object Memo1: TcxMemo
    Left = 0
    Top = 513
    Align = alBottom
    Lines.Strings = (
      'Memo1')
    TabOrder = 2
    Height = 100
    Width = 982
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 613
    Width = 982
    Height = 19
    Color = clWhite
    Panels = <
      item
        Width = 300
      end
      item
        Width = 50
      end>
  end
  object cxButton5: TcxButton
    Left = 280
    Top = 7
    Width = 41
    Height = 34
    TabOrder = 4
    OnClick = cxButton5Click
    Glyph.Data = {
      56080000424D560800000000000036000000280000001A0000001A0000000100
      18000000000020080000C40E0000C40E00000000000000000000C0C0C0C0C0C0
      C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
      C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
      C0C0C0C0C0C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
      C0C0C0C0C0C0C0C0004000004000004000004000C0C0C0C0C0C0C0C0C0C0C0C0
      C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C00000C0C0C0004000
      80808080808080808080808080808080808080808000400040E02040E020FFFF
      FFC8D0D4A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0808080C0
      C0C0C0C0C0C0C0C00000C0C0C0C0C0C000400040E02000C02000C02000C02000
      C02000400040E02040E020FFFFFF004000C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
      C8D0D4C8D0D4C8D0D4A4A0A0A4A0A0404040C0C0C0C0C0C00000C0C0C0C0C0C0
      C0C0C000400000C02000C02000C02000400000C02040E020FFFFFF00400000C0
      20C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4A4A0A0A4A0A040
      2020808080C0C0C00000C0C0C0C0C0C0C0C0C0A4A0A000400000C02000400000
      C02040E020FFFFFF004000004000004000C8D0D4A4A0A0A4A0A0A4A0A0A4A0A0
      A4A0A0A4A0A0808080C8D0D4808080404040808080C0C0C00000C0C0C0C0C0C0
      C0C0C0C0C0C0C0C0C000400040E02040E020FFFFFF004000F0FBFFF0FBFFF0FB
      FFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFA4A0A0C8D0D4C8D0D440
      2020808080C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C000400040E02040E020FF
      FFFF004000808080004000C8D0D4C0DCC0C0DCC0C0DCC0C0DCC0C0DCC0C0DCC0
      C0DCC0C8D0D4F0FBFFA4A0A0C8D0D4402020808080C0C0C00000C0C0C0C0C0C0
      C0C0C000400000C02040E020FFFFFF00400080E0A000C020808080004000FFFF
      FFFFFFFFFFFFFFF0FBFFC0DCC0C0DCC0F0FBFFC0DCC0F0FBFF808080C8D0D440
      2020808080C0C0C00000C0C0C0C0C0C000400000C02040E020FFFFFF004000FF
      FFFF00400000C02000C020808080004000FFFFFFFFFFFFC8D0D4F0FBFFC0DCC0
      C0DCC0C0DCC0F0FBFFC0DCC0A4A0A0404020808080C0C0C00000C0C0C0004000
      004000004000004000FFFFFFFFFFFFFFFFFFFFFFFF0040000040000040000040
      00C8D0D4F0FBFFC0DCC0C8D0D4C8D0D4F0FBFFC0DCC0F0FBFFFFFFFF80808040
      4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFC0DCC0FFFFFFF0FBFFFFFFFFFFFFFFF0FBFFF0FBFFF0FBFF
      FFFFFFC0DCC0F0FBFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
      C0C0C0808080FFFFFFFFFFFFC0DCC0F0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FB
      FFF0FBFFFFFFFFFFFFFFF0FBFFF0FBFFFFFFFFC0DCC0F0FBFFF0FBFF80808040
      4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0F0
      FBFFF0FBFFF0FBFFC0DCC0C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
      C8D0D4C8D0D4F0FBFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
      C0C0C0808080FFFFFFFFFFFFC0DCC0C0DCC0C0DCC0F0FBFFF0FBFFC0DCC0FFFF
      FFC0DCC0F0FBFFC0DCC0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
      4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0C0
      DCC0F0FBFFF0FBFFF0FBFFF0FBFFFFFFFFF0FBFFF0FBFFC0DCC0F0FBFFFFFFFF
      FFFFFFFFFFFFFFFFFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
      C0C0C0808080FFFFFFFFFFFFC0DCC0C0DCC0C8D0D4F0FBFFF0FBFFC8D0D4FFFF
      FFC8D0D4F0FBFFC0DCC0F0FBFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
      4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0F0
      FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFC8D0D4F0FBFFFFFFFF
      FFFFFFFFFFFFFFFFFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
      C0C0C0808080FFFFFFFFFFFFF0FBFFC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
      D4C8D0D4C8D0D4C8D0D4F0FBFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
      4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFF0FBFFF0
      FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFF808080404040808080C0C0C00000C0C0C0C0C0C0
      C0C0C0808080C0DCC0C8D0D4C8D0D4F0FBFFC8D0D4C0DCC0C0DCC0C8D0D4F0FB
      FFC8D0D4C0DCC0F0FBFFC8D0D4F0FBFFC8D0D4C8D0D4F0FBFFA4A0A080808040
      4040808080C0C0C00000C0C0C0C0C0C0C0C0C080808080E0E040E0E040E0E080
      E0E040E0E080E0E080E0E040E0E080E0E040E0E040E0E080E0E040E0E080E0E0
      40E0E040E0E080E0E000E0E0408080404040808080C0C0C00000C0C0C0C0C0C0
      C0C0C080808080E0E080E0E0C0DCC080E0E0C0DCC080E0E080E0E0C0DCC080E0
      E0C0DCC080E0E080E0E0C0DCC080E0E0C0DCC080E0E080E0E0C0DCC000808000
      4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080F0FBFFF0FBFFF0FBFF80
      E0E0F0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFF
      F0FBFFF0FBFFF0FBFFF0FBFF00C0C0002040808080C0C0C00000C0C0C0C0C0C0
      C0C0C08080808080808080808080808080808080808080808080808080808080
      8080808080808080808080808080808080808080808080808080808080808000
      2040C0C0C0C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
      C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
      C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C00000}
    LookAndFeel.Kind = lfOffice11
  end
  object cxButton7: TcxButton
    Left = 648
    Top = 11
    Width = 65
    Height = 23
    Hint = #1059#1076#1072#1083#1080#1090#1100' '#1080#1079' '#1074#1077#1089#1086#1074
    Caption = #1059#1076#1072#1083#1080#1090#1100
    TabOrder = 5
    OnClick = cxButton7Click
    Glyph.Data = {
      E6010000424DE60100000000000036000000280000000C0000000C0000000100
      180000000000B0010000C40E0000C40E00000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFF0000FFC6C6C6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFF0000FF848484FFFFFFFFFFFFFFFFFFFFFFFF848484
      C6C6C6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FF848484C6C6C6FFFFFF8484
      840000FF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FF848484C6
      C6C60000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC6C6C6
      0000FF0000FF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8484
      840000FF0000FF0000FF848484FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF84
      84840000FF0000FFFFFFFF0000FF848484848484FFFFFFFFFFFFFFFFFFFFFFFF
      8484840000FFFFFFFFFFFFFFFFFFFFFFFFFF0000FF848484C6C6C6FFFFFFFFFF
      FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FF848484FF
      FFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFF}
    LookAndFeel.Kind = lfOffice11
  end
  object FormPlacement1: TFormPlacement
    Active = False
    Left = 76
    Top = 84
  end
  object amScale: TActionManager
    Left = 488
    Top = 140
    StyleName = 'XP Style'
    object acEditPlu: TAction
      Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100' PLU'
      ShortCut = 115
      OnExecute = acEditPluExecute
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 240
    Top = 156
    object N1: TMenuItem
      Caption = #1047#1072#1075#1088#1091#1079#1080#1090#1100' '#1074#1099#1076#1077#1083#1077#1085#1085#1099#1077' '#1090#1086#1074#1072#1088#1099' '#1076#1083#1103' '#1082#1086#1085#1090#1088#1086#1083#1103' '#1074#1077#1089#1072' '#1085#1072' '#1082#1072#1089#1089#1077
    end
  end
  object teTestVes: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 396
    Top = 216
    object teTestVesArticul: TIntegerField
      FieldName = 'Articul'
    end
  end
  object teL: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 580
    Top = 136
    object teLiPlu: TIntegerField
      FieldName = 'iPlu'
    end
    object teLiSrok: TIntegerField
      FieldName = 'iSrok'
    end
    object teLsItem: TStringField
      FieldName = 'sItem'
    end
    object teLName1: TStringField
      FieldName = 'Name1'
      Size = 50
    end
    object teLName2: TStringField
      FieldName = 'Name2'
      Size = 50
    end
    object teLrPrice: TFloatField
      FieldName = 'rPrice'
    end
    object teLiFormat: TSmallintField
      FieldName = 'iFormat'
    end
    object teLStrI1: TStringField
      FieldName = 'StrI1'
      Size = 200
    end
    object teLStrI2: TStringField
      FieldName = 'StrI2'
      Size = 180
    end
    object teLIngr: TIntegerField
      FieldName = 'Ingr'
    end
    object teLIStr1: TStringField
      FieldName = 'IStr1'
      Size = 200
    end
    object teLIStr2: TStringField
      FieldName = 'IStr2'
      Size = 200
    end
  end
end
