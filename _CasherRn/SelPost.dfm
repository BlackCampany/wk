object fmSelPost: TfmSelPost
  Left = 628
  Top = 476
  BorderStyle = bsDialog
  Caption = #1055#1088#1080#1085#1103#1090#1100' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
  ClientHeight = 153
  ClientWidth = 535
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label4: TLabel
    Left = 24
    Top = 24
    Width = 58
    Height = 13
    Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
  end
  object Label2: TLabel
    Left = 24
    Top = 60
    Width = 79
    Height = 13
    Caption = #1060#1072#1081#1083' '#1087#1088#1080#1093#1086#1076#1086#1074
  end
  object Panel1: TPanel
    Left = 0
    Top = 92
    Width = 535
    Height = 61
    Align = alBottom
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 112
      Top = 16
      Width = 109
      Height = 33
      Caption = #1054#1082
      ModalResult = 1
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 312
      Top = 16
      Width = 105
      Height = 33
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      LookAndFeel.Kind = lfOffice11
    end
  end
  object cxButtonEdit1: TcxButtonEdit
    Left = 124
    Top = 20
    Properties.Buttons = <
      item
        Default = True
        Kind = bkEllipsis
      end>
    Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
    TabOrder = 1
    Text = 'cxButtonEdit1'
    Width = 321
  end
  object cxButtonEdit2: TcxButtonEdit
    Left = 124
    Top = 56
    Properties.Buttons = <
      item
        Default = True
        Kind = bkEllipsis
      end>
    Properties.OnButtonClick = cxButtonEdit2PropertiesButtonClick
    TabOrder = 2
    Text = 'cxButtonEdit2'
    Width = 321
  end
  object OpenDialog1: TOpenDialog
    Left = 472
    Top = 16
  end
end
