object fmSetQuant: TfmSetQuant
  Left = 520
  Top = 477
  BorderStyle = bsDialog
  ClientHeight = 148
  ClientWidth = 311
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 20
    Top = 40
    Width = 78
    Height = 13
    Caption = #1042#1074#1077#1076#1080#1090#1077' '#1082#1086#1083'-'#1074#1086
  end
  object Panel1: TPanel
    Left = 0
    Top = 89
    Width = 311
    Height = 59
    Align = alBottom
    BevelInner = bvLowered
    Color = 16765864
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 48
      Top = 12
      Width = 75
      Height = 33
      Caption = #1054#1082
      Default = True
      ModalResult = 1
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 180
      Top = 12
      Width = 75
      Height = 33
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 1
      TabOrder = 1
      LookAndFeel.Kind = lfOffice11
    end
  end
  object cxCalcEdit1: TcxCalcEdit
    Left = 132
    Top = 36
    EditValue = 0.000000000000000000
    Properties.Alignment.Horz = taRightJustify
    Properties.DisplayFormat = '0.000'
    Style.BorderStyle = ebsOffice11
    Style.LookAndFeel.Kind = lfOffice11
    Style.Shadow = True
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 1
    Width = 125
  end
end
