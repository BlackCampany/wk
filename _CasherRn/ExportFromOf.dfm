object fmExport: TfmExport
  Left = 1439
  Top = 141
  BorderStyle = bsDialog
  Caption = #1069#1082#1089#1087#1086#1088#1090' '#1074' '#1073#1091#1093#1075#1072#1083#1090#1077#1088#1080#1102
  ClientHeight = 414
  ClientWidth = 400
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 16
    Top = 72
    Width = 61
    Height = 13
    Caption = #1047#1072' '#1087#1077#1088#1080#1086#1076' '#1089
  end
  object Label3: TLabel
    Left = 224
    Top = 72
    Width = 15
    Height = 13
    Caption = #1087#1086' '
  end
  object Label4: TLabel
    Left = 24
    Top = 200
    Width = 12
    Height = 13
    Caption = 'L4'
  end
  object Panel1: TPanel
    Left = 0
    Top = 362
    Width = 400
    Height = 52
    Align = alBottom
    BevelInner = bvLowered
    Color = 12582911
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 264
      Top = 16
      Width = 97
      Height = 25
      Caption = #1042#1099#1093#1086#1076
      TabOrder = 0
      OnClick = cxButton1Click
      Glyph.Data = {
        5E040000424D5E04000000000000360000002800000012000000130000000100
        18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
        8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
        0000CED3D6848684848684848684848684848684848684848684848684848684
        848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
        7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
        FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
        FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
        007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
        FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
        00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
        494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
        00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000}
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 32
      Top = 16
      Width = 97
      Height = 25
      Caption = #1069#1082#1089#1087#1086#1088#1090
      TabOrder = 1
      OnClick = cxButton2Click
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C104200000000000000000000000000000000
        00001F7C1F7C1F7C1F7C1F7C1F7C1042FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        00001F7C1F7C1F7C1F7C1F7C1F7C1042FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        00001F7C1F7C1F7C1F7C1F7C1F7C1042FF7FFF7F0002FF7FFF7FFF7FFF7FFF7F
        00001F7C1F7C1F7C1F7C1F7C1F7C1042FF7FFF7F00020002FF7FFF7FFF7FFF7F
        00001F7C1F7C1F7C000200020002000200020002000200020002FF7FFF7FFF7F
        00001F7C1F7C1F7C0002000200020002000200020002000200020002FF7FFF7F
        00001F7C1F7C1F7C000200020002000200020002000200020002FF7FFF7FFF7F
        00001F7C1F7C1F7C1F7C1F7C1F7C1042FF7FFF7F00020002FF7FFF7FFF7FFF7F
        00001F7C1F7C1F7C1F7C1F7C1F7C1042FF7FFF7F0002FF7FFF7FFF7FFF7FFF7F
        00001F7C1F7C1F7C1F7C1F7C1F7C1042FF7FFF7FFF7FFF7FFF7F000000000000
        00001F7C1F7C1F7C1F7C1F7C1F7C1042FF7FFF7FFF7FFF7FFF7F000018630000
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1042FF7FFF7FFF7FFF7FFF7F000000001F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C10421042104210421042104200001F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton3: TcxButton
      Left = 148
      Top = 16
      Width = 97
      Height = 25
      Caption = #1053#1072' FTP'
      TabOrder = 2
      OnClick = cxButton3Click
      LookAndFeel.Kind = lfOffice11
    end
  end
  object GroupBox1: TGroupBox
    Left = 16
    Top = 8
    Width = 369
    Height = 49
    TabOrder = 1
    object Label1: TLabel
      Left = 24
      Top = 16
      Width = 298
      Height = 13
      Caption = #1069#1082#1089#1087#1086#1088#1090' '#1087#1088#1086#1080#1079#1074#1086#1076#1080#1090#1089#1103' '#1074' '#1092#1072#1081#1083'   "'#1055#1091#1090#1100' '#1101#1082#1089#1087#1086#1088#1090#1072'"\expout.txt'
    end
  end
  object cxDateEdit1: TcxDateEdit
    Left = 88
    Top = 68
    TabOrder = 2
    Width = 121
  end
  object cxDateEdit2: TcxDateEdit
    Left = 248
    Top = 68
    TabOrder = 3
    Width = 121
  end
  object ProgressBar1: TdxfProgressBar
    Left = 16
    Top = 224
    Width = 369
    Height = 17
    BarBevelOuter = bvRaised
    BeginColor = 16612864
    BevelOuter = bvLowered
    EndColor = 8453888
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Max = 100
    Min = 0
    Orientation = orHorizontal
    ParentFont = False
    Position = 99
    ShowText = True
    ShowTextStyle = stsPercent
    Step = 10
    Style = sExSolid
    TabOrder = 4
    TransparentGlyph = True
  end
  object ProgressBar2: TdxfProgressBar
    Left = 16
    Top = 248
    Width = 369
    Height = 17
    BarBevelOuter = bvRaised
    BeginColor = 16612864
    BevelOuter = bvLowered
    EndColor = 8453888
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Max = 100
    Min = 0
    Orientation = orHorizontal
    ParentFont = False
    Position = 99
    ShowText = True
    ShowTextStyle = stsPercent
    Step = 10
    Style = sExSolid
    TabOrder = 5
    TransparentGlyph = True
    Visible = False
  end
  object cxCheckBox1: TcxCheckBox
    Left = 24
    Top = 104
    Caption = #1055#1088#1080#1093#1086#1076#1099
    State = cbsChecked
    TabOrder = 6
    Width = 121
  end
  object cxCheckBox2: TcxCheckBox
    Left = 24
    Top = 120
    Caption = #1042#1086#1079#1074#1088#1072#1090#1099
    State = cbsChecked
    TabOrder = 7
    Width = 121
  end
  object cxCheckBox3: TcxCheckBox
    Left = 24
    Top = 136
    Caption = #1056#1077#1072#1083'. '#1085#1072' '#1089#1090#1086#1088#1086#1085#1091
    State = cbsChecked
    TabOrder = 8
    Width = 121
  end
  object cxCheckBox4: TcxCheckBox
    Left = 24
    Top = 152
    Caption = #1056#1077#1072#1083#1080#1079#1072#1094#1080#1103
    State = cbsChecked
    TabOrder = 9
    Width = 121
  end
  object cxCheckBox5: TcxCheckBox
    Left = 24
    Top = 168
    Caption = #1042#1085'. '#1076#1086#1082#1091#1084#1077#1085#1090#1099
    State = cbsChecked
    TabOrder = 10
    Width = 121
  end
  object Memo1: TcxMemo
    Left = 0
    Top = 276
    Align = alBottom
    Lines.Strings = (
      'Memo1')
    TabOrder = 11
    Height = 86
    Width = 400
  end
  object quExport: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT SP.IDHEAD,ca.CATEGORY,SP.NDSPROC,'
      
        'dh.DATEDOC,dh.NUMDOC,dh.DATESF,dh.NUMSF,dh.IDCLI,dh.IDSKL,mh.EXP' +
        'NAME,cl.NAMECL,cl.INN,DH.OPRIZN,'
      'DH.IDCLITO,cl1.NAMECL as NAMECLTO,cl1.INN as INNTO,'
      
        'SUM(SP.SUMIN) as SUMIN,SUM(SP.SUMUCH) as SUMUCH,SUM(SP.SUMNDS) a' +
        's SUMNDS'
      'FROM OF_DOCSPECIN SP'
      'left join OF_DOCHEADIN DH on DH.ID=SP.IDHEAD'
      'left join OF_CLIENTS cl on cl.ID=dh.IDCLI'
      'left join OF_CLIENTS cl1 on cl1.ID=dh.IDCLITO'
      'left join OF_MH mh on mh.ID=dh.IDSKL'
      'left join OF_CARDS ca on ca.ID=SP.IDCARD'
      'Where dh.DATEDOC>=:DATEB and dh.DATEDOC<:DATEE and dh.IACTIVE=1'
      
        'and dh.IDSKL in (SELECT IDMH FROM RPERSONALMH where IDPERSON=:ID' +
        'PERSON)'
      
        'group by SP.IDHEAD,ca.CATEGORY,SP.NDSPROC,dh.DATEDOC,dh.NUMDOC,d' +
        'h.DATESF,dh.NUMSF,dh.IDCLI,dh.IDSKL,mh.EXPNAME,cl.NAMECL,cl.INN,' +
        'DH.OPRIZN,DH.IDCLITO,cl1.NAMECL,cl1.INN'
      'ORDER BY SP.IDHEAD,ca.CATEGORY,SP.NDSPROC'
      '')
    Transaction = dmO.trSelect
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    AutoCommit = True
    Left = 232
    Top = 20
    object quExportCATEGORY: TFIBIntegerField
      FieldName = 'CATEGORY'
    end
    object quExportIDHEAD: TFIBIntegerField
      FieldName = 'IDHEAD'
    end
    object quExportDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object quExportNUMDOC: TFIBStringField
      FieldName = 'NUMDOC'
      Size = 15
      EmptyStrToNull = True
    end
    object quExportDATESF: TFIBDateField
      FieldName = 'DATESF'
    end
    object quExportNUMSF: TFIBStringField
      FieldName = 'NUMSF'
      Size = 15
      EmptyStrToNull = True
    end
    object quExportIDCLI: TFIBIntegerField
      FieldName = 'IDCLI'
    end
    object quExportIDSKL: TFIBIntegerField
      FieldName = 'IDSKL'
    end
    object quExportEXPNAME: TFIBStringField
      FieldName = 'EXPNAME'
      Size = 10
      EmptyStrToNull = True
    end
    object quExportNAMECL: TFIBStringField
      FieldName = 'NAMECL'
      Size = 100
      EmptyStrToNull = True
    end
    object quExportINN: TFIBStringField
      FieldName = 'INN'
      Size = 15
      EmptyStrToNull = True
    end
    object quExportSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quExportSUMUCH: TFIBFloatField
      FieldName = 'SUMUCH'
    end
    object quExportSUMNDS: TFIBFloatField
      FieldName = 'SUMNDS'
    end
    object quExportOPRIZN: TFIBSmallIntField
      FieldName = 'OPRIZN'
    end
    object quExportNDSPROC: TFIBFloatField
      FieldName = 'NDSPROC'
    end
    object quExportIDCLITO: TFIBIntegerField
      FieldName = 'IDCLITO'
    end
    object quExportNAMECLTO: TFIBStringField
      FieldName = 'NAMECLTO'
      Size = 100
      EmptyStrToNull = True
    end
    object quExportINNTO: TFIBStringField
      FieldName = 'INNTO'
      Size = 15
      EmptyStrToNull = True
    end
  end
  object quCountD: TpFIBDataSet
    SelectSQL.Strings = (
      'select Count(*) as CountDoc from OF_DOCHEADIN'
      'Where DATEDOC>=:DATEB and DATEDOC<:DATEE and IACTIVE=1'
      
        'and IDSKL in (SELECT IDMH FROM RPERSONALMH where IDPERSON=:IDPER' +
        'SON)')
    Transaction = dmO.trSelect
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    Left = 332
    Top = 24
    object quCountDCOUNTDOC: TFIBIntegerField
      FieldName = 'COUNTDOC'
    end
  end
  object quExpOut: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT SP.IDHEAD,ca.CATEGORY,SP.IDNDS,'
      
        'dh.DATEDOC,dh.NUMDOC,dh.DATESF,dh.NUMSF,dh.IDCLI,dh.IDSKL,mh.EXP' +
        'NAME,cl.NAMECL,cl.INN,DH.OPRIZN,'
      
        'SUM(SP.SUMIN) as SUMIN,SUM(SP.SUMUCH) as SUMUCH,SUM(SP.SUMNDS) a' +
        's SUMNDS'
      'FROM OF_DOCSPECOUT SP'
      'left join OF_DOCHEADOUT DH on DH.ID=SP.IDHEAD'
      'left join OF_CLIENTS cl on cl.ID=dh.IDCLI'
      'left join OF_MH mh on mh.ID=dh.IDSKL'
      'left join OF_CARDS ca on ca.ID=SP.IDCARD'
      'Where dh.DATEDOC>=:DATEB and dh.DATEDOC<:DATEE and dh.IACTIVE=1'
      
        'and dh.IDSKL in (SELECT IDMH FROM RPERSONALMH where IDPERSON=:ID' +
        'PERSON)'
      ''
      
        'group by SP.IDHEAD,ca.CATEGORY,SP.IDNDS,dh.DATEDOC,dh.NUMDOC,dh.' +
        'DATESF,dh.NUMSF,dh.IDCLI,dh.IDSKL,mh.EXPNAME,cl.NAMECL,cl.INN,DH' +
        '.OPRIZN'
      'ORDER BY SP.IDHEAD,ca.CATEGORY,SP.IDNDS'
      '')
    Transaction = dmO.trSelect
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    AutoCommit = True
    Left = 192
    Top = 96
    object quExpOutIDHEAD: TFIBIntegerField
      FieldName = 'IDHEAD'
    end
    object quExpOutIDNDS: TFIBIntegerField
      FieldName = 'IDNDS'
    end
    object quExpOutDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object quExpOutNUMDOC: TFIBStringField
      FieldName = 'NUMDOC'
      Size = 15
      EmptyStrToNull = True
    end
    object quExpOutDATESF: TFIBDateField
      FieldName = 'DATESF'
    end
    object quExpOutNUMSF: TFIBStringField
      FieldName = 'NUMSF'
      Size = 15
      EmptyStrToNull = True
    end
    object quExpOutIDCLI: TFIBIntegerField
      FieldName = 'IDCLI'
    end
    object quExpOutIDSKL: TFIBIntegerField
      FieldName = 'IDSKL'
    end
    object quExpOutEXPNAME: TFIBStringField
      FieldName = 'EXPNAME'
      Size = 10
      EmptyStrToNull = True
    end
    object quExpOutNAMECL: TFIBStringField
      FieldName = 'NAMECL'
      Size = 100
      EmptyStrToNull = True
    end
    object quExpOutINN: TFIBStringField
      FieldName = 'INN'
      Size = 15
      EmptyStrToNull = True
    end
    object quExpOutSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quExpOutSUMUCH: TFIBFloatField
      FieldName = 'SUMUCH'
    end
    object quExpOutSUMNDS: TFIBFloatField
      FieldName = 'SUMNDS'
    end
    object quExpOutCATEGORY: TFIBIntegerField
      FieldName = 'CATEGORY'
    end
    object quExpOutOPRIZN: TFIBSmallIntField
      FieldName = 'OPRIZN'
    end
  end
  object quCountDOut: TpFIBDataSet
    SelectSQL.Strings = (
      'select Count(*) as CountDoc from OF_DOCHEADOUT'
      'Where DATEDOC>=:DATEB and DATEDOC<:DATEE and IACTIVE=1'
      
        'and IDSKL in (SELECT IDMH FROM RPERSONALMH where IDPERSON=:IDPER' +
        'SON)')
    Transaction = dmO.trSelect
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    Left = 256
    Top = 96
    object quCountDOutCOUNTDOC: TFIBIntegerField
      FieldName = 'COUNTDOC'
    end
  end
  object quListR: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT dh.DATEDOC,dh.IDSKL,mh.EXPNAME FROM OF_DOCHEADOUTB dh'
      'left join OF_MH mh on mh.ID=dh.IDSKL'
      'where IACTIVE=1 and DATEDOC>=:DATEB and DATEDOC<:DATEE'
      
        'and dh.IDSKL in (SELECT IDMH FROM RPERSONALMH where IDPERSON=:ID' +
        'PERSON)'
      'group by dh.DATEDOC,dh.IDSKL,mh.EXPNAME'
      'Order by dh.DATEDOC,dh.IDSKL,mh.EXPNAME')
    Transaction = dmO.trSelect
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    Left = 308
    Top = 176
    poAskRecordCount = True
    object quListRDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object quListRIDSKL: TFIBIntegerField
      FieldName = 'IDSKL'
    end
    object quListREXPNAME: TFIBStringField
      FieldName = 'EXPNAME'
      Size = 10
      EmptyStrToNull = True
    end
  end
  object quAvIn: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT SUM(ds.RSUM) as RSUMA'
      'FROM OF_DOCSPECOUTB ds'
      'left join of_docheadoutb dh on dh.ID=ds.IDHEAD'
      'left join of_cards ca on ca.ID=ds.IDCARD'
      ''
      'where ca.CATEGORY=3 and'
      'dh.IACTIVE=1 and'
      'dh.IDSKL=:IDSKL and'
      'dh.DATEDOC=:DATEB and'
      'ds.RSUM>0'
      
        'and dh.IDSKL in (SELECT IDMH FROM RPERSONALMH where IDPERSON=:ID' +
        'PERSON)')
    Transaction = dmO.trSelect
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    Left = 188
    Top = 148
    poAskRecordCount = True
    object quAvInRSUMA: TFIBFloatField
      FieldName = 'RSUMA'
    end
  end
  object quAvOut: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT SUM(ds.RSUM) as RSUMA'
      'FROM OF_DOCSPECOUTB ds'
      'left join of_docheadoutb dh on dh.ID=ds.IDHEAD'
      'left join of_cards ca on ca.ID=ds.IDCARD'
      ''
      'where ca.CATEGORY=3 and'
      'dh.IACTIVE=1 and'
      'dh.IDSKL=:IDSKL and'
      'dh.DATEDOC=:DATEB and'
      'ds.RSUM<0'
      
        'and dh.IDSKL in (SELECT IDMH FROM RPERSONALMH where IDPERSON=:ID' +
        'PERSON)')
    Transaction = dmO.trSelect
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    Left = 244
    Top = 176
    poAskRecordCount = True
    object quAvOutRSUMA: TFIBFloatField
      FieldName = 'RSUMA'
    end
  end
  object quCountVn: TpFIBDataSet
    SelectSQL.Strings = (
      'select Count(*) as CountDoc from OF_DOCHEADVN'
      'Where DATEDOC>=:DATEB and DATEDOC<:DATEE and IACTIVE=1'
      'and'
      
        '( IDSKL_FROM in (SELECT IDMH FROM RPERSONALMH where IDPERSON=:ID' +
        'PERSON)'
      'or'
      
        '  IDSKL_TO in (SELECT IDMH FROM RPERSONALMH where IDPERSON=:IDPE' +
        'RSON1)'
      ')')
    Transaction = dmO.trSelect
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    Left = 124
    Top = 16
    object quCountVnCOUNTDOC: TFIBIntegerField
      FieldName = 'COUNTDOC'
    end
  end
  object quExportVn: TpFIBDataSet
    SelectSQL.Strings = (
      
        'SELECT dh.ID,dh.DATEDOC,dh.NUMDOC,dh.IDSKL_FROM,mh1.EXPNAME,dh.I' +
        'DSKL_TO,mh2.EXPNAME,'
      'dh.SUMIN,dh.SUMUCH,dh.SUMUCH1,dh.SUMTAR'
      'FROM OF_DOCHEADVN dh'
      'left join OF_MH mh1 on mh1.ID=dh.IDSKL_FROM'
      'left join OF_MH mh2 on mh2.ID=dh.IDSKL_TO'
      ''
      'Where dh.DATEDOC>=:DATEB and dh.DATEDOC<:DATEE and dh.IACTIVE=1'
      'and'
      
        '( dh.IDSKL_FROM in (SELECT IDMH FROM RPERSONALMH where IDPERSON=' +
        ':IDPERSON)'
      'or'
      
        '  dh.IDSKL_TO in (SELECT IDMH FROM RPERSONALMH where IDPERSON=:I' +
        'DPERSON1)'
      ')'
      '')
    Transaction = dmO.trSelect
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    AutoCommit = True
    Left = 48
    Top = 20
    object quExportVnID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quExportVnDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object quExportVnNUMDOC: TFIBStringField
      FieldName = 'NUMDOC'
      Size = 15
      EmptyStrToNull = True
    end
    object quExportVnIDSKL_FROM: TFIBIntegerField
      FieldName = 'IDSKL_FROM'
    end
    object quExportVnEXPNAME: TFIBStringField
      FieldName = 'EXPNAME'
      Size = 10
      EmptyStrToNull = True
    end
    object quExportVnIDSKL_TO: TFIBIntegerField
      FieldName = 'IDSKL_TO'
    end
    object quExportVnEXPNAME1: TFIBStringField
      FieldName = 'EXPNAME1'
      Size = 10
      EmptyStrToNull = True
    end
    object quExportVnSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quExportVnSUMUCH: TFIBFloatField
      FieldName = 'SUMUCH'
    end
    object quExportVnSUMUCH1: TFIBFloatField
      FieldName = 'SUMUCH1'
    end
    object quExportVnSUMTAR: TFIBFloatField
      FieldName = 'SUMTAR'
    end
  end
  object quListR1: TpFIBDataSet
    SelectSQL.Strings = (
      
        'SELECT dh.DATEDOC,dh.IDSKL,mh.EXPNAME,dh.OPER,dh.COMMENT, SUM(dh' +
        '.SUMIN) as SUMIN,SUM(dh.SUMUCH) as SUMUCH  FROM OF_DOCHEADOUTB d' +
        'h'
      'left join OF_MH mh on mh.ID=dh.IDSKL'
      'left join of_oper op on op.ABR=dh.OPER'
      'where IACTIVE=1 and DATEDOC>=:DATEB and DATEDOC<:DATEE'
      'and (op.ID>0 or op.ID=-4)'
      
        'and dh.IDSKL in (SELECT IDMH FROM RPERSONALMH where IDPERSON=:ID' +
        'PERSON)'
      'group by dh.DATEDOC,dh.IDSKL,mh.EXPNAME,dh.OPER,dh.COMMENT'
      'Order by dh.DATEDOC,dh.IDSKL,mh.EXPNAME,dh.OPER,dh.COMMENT')
    Transaction = dmO.trSelect
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    Left = 356
    Top = 176
    poAskRecordCount = True
    object quListR1DATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object quListR1IDSKL: TFIBIntegerField
      FieldName = 'IDSKL'
    end
    object quListR1EXPNAME: TFIBStringField
      FieldName = 'EXPNAME'
      Size = 10
      EmptyStrToNull = True
    end
    object quListR1OPER: TFIBStringField
      FieldName = 'OPER'
      EmptyStrToNull = True
    end
    object quListR1SUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quListR1SUMUCH: TFIBFloatField
      FieldName = 'SUMUCH'
    end
    object quListR1COMMENT: TFIBStringField
      FieldName = 'COMMENT'
      Size = 100
      EmptyStrToNull = True
    end
  end
  object quCountDOutR: TpFIBDataSet
    SelectSQL.Strings = (
      'select Count(*) as CountDoc from OF_DOCHEADOUTR'
      'Where DATEDOC>=:DATEB and DATEDOC<:DATEE and IACTIVE=1'
      
        'and IDSKL in (SELECT IDMH FROM RPERSONALMH where IDPERSON=:IDPER' +
        'SON)')
    Transaction = dmO.trSelect
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    Left = 348
    Top = 120
    object quCountDOutRCOUNTDOC: TFIBIntegerField
      FieldName = 'COUNTDOC'
    end
  end
  object quExpOutR: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT SP.IDHEAD,SP.IDNDS,'
      
        'dh.DATEDOC,dh.NUMDOC,dh.DATESF,dh.NUMSF,dh.IDCLI,dh.IDSKL,mh.EXP' +
        'NAME,cl.NAMECL,cl.INN,'
      'SP.SUMIN,SP.SUMR,SP.SUMNDS,SP.SUMIN0,SP.SUMOUT0,SP.SUMNDSOUT,'
      'ca.CATEGORY,dh.IDFROM,cl1.INN as INNFROM'
      'FROM OF_DOCSPECOUTR SP'
      'left join OF_DOCHEADOUTR DH on DH.ID=SP.IDHEAD'
      'left join OF_CLIENTS cl on cl.ID=dh.IDCLI'
      'left join OF_CLIENTS cl1 on cl1.ID=dh.IDFROM'
      'left join OF_MH mh on mh.ID=dh.IDSKL'
      'left join OF_CARDS ca on ca.ID=SP.IDCARD'
      'Where dh.DATEDOC>=:DATEB and dh.DATEDOC<:DATEE and dh.IACTIVE=1'
      
        'and dh.IDSKL in (SELECT IDMH FROM RPERSONALMH where IDPERSON=:ID' +
        'PERSON)'
      'ORDER BY SP.IDHEAD,SP.IDNDS')
    Transaction = dmO.trSelect
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    AutoCommit = True
    Left = 308
    Top = 84
    object quExpOutRIDHEAD: TFIBIntegerField
      FieldName = 'IDHEAD'
    end
    object quExpOutRIDNDS: TFIBIntegerField
      FieldName = 'IDNDS'
    end
    object quExpOutRDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object quExpOutRNUMDOC: TFIBStringField
      FieldName = 'NUMDOC'
      Size = 15
      EmptyStrToNull = True
    end
    object quExpOutRDATESF: TFIBDateField
      FieldName = 'DATESF'
    end
    object quExpOutRNUMSF: TFIBStringField
      FieldName = 'NUMSF'
      Size = 15
      EmptyStrToNull = True
    end
    object quExpOutRIDCLI: TFIBIntegerField
      FieldName = 'IDCLI'
    end
    object quExpOutRIDSKL: TFIBIntegerField
      FieldName = 'IDSKL'
    end
    object quExpOutREXPNAME: TFIBStringField
      FieldName = 'EXPNAME'
      Size = 10
      EmptyStrToNull = True
    end
    object quExpOutRNAMECL: TFIBStringField
      FieldName = 'NAMECL'
      Size = 100
      EmptyStrToNull = True
    end
    object quExpOutRINN: TFIBStringField
      FieldName = 'INN'
      Size = 15
      EmptyStrToNull = True
    end
    object quExpOutRSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quExpOutRSUMR: TFIBFloatField
      FieldName = 'SUMR'
    end
    object quExpOutRSUMNDS: TFIBFloatField
      FieldName = 'SUMNDS'
    end
    object quExpOutRSUMIN0: TFIBFloatField
      FieldName = 'SUMIN0'
    end
    object quExpOutRSUMOUT0: TFIBFloatField
      FieldName = 'SUMOUT0'
    end
    object quExpOutRSUMNDSOUT: TFIBFloatField
      FieldName = 'SUMNDSOUT'
    end
    object quExpOutRCATEGORY: TFIBIntegerField
      FieldName = 'CATEGORY'
    end
    object quExpOutRIDFROM: TFIBIntegerField
      FieldName = 'IDFROM'
    end
    object quExpOutRINNFROM: TFIBStringField
      FieldName = 'INNFROM'
      Size = 15
      EmptyStrToNull = True
    end
  end
  object IdFTP1: TIdFTP
    MaxLineAction = maException
    ReadTimeout = 0
    RecvBufferSize = 1024
    Host = '80.75.90.50'
    Password = 'tf875bvcxfg35HGFdfg'
    Username = 'dfsd54dsf'
    ProxySettings.ProxyType = fpcmNone
    ProxySettings.Port = 0
    Left = 146
    Top = 148
  end
  object quComplInS: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT qin.IDHEAD,'
      '       qin.ID,'
      '       qin.IDCARD,'
      '       qin.QUANT,'
      '       qin.IDM,'
      '       qin.KM,'
      '       qin.PRICEIN,'
      '       qin.SUMIN,'
      '       qin.PRICEIN0,'
      '       qin.SUMIN0'
      'FROM OF_DOCSPECCOMPLB qin'
      'where qin.IDHEAD=:IDH'
      'and qin.IDCARD in (5127,5128,5129,5130)')
    Transaction = dmO.trSelect
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    AutoCommit = True
    Left = 92
    Top = 300
    poAskRecordCount = True
    object quComplInSIDHEAD: TFIBIntegerField
      FieldName = 'IDHEAD'
    end
    object quComplInSID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quComplInSIDCARD: TFIBIntegerField
      FieldName = 'IDCARD'
    end
    object quComplInSQUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quComplInSIDM: TFIBIntegerField
      FieldName = 'IDM'
    end
    object quComplInSKM: TFIBFloatField
      FieldName = 'KM'
    end
    object quComplInSPRICEIN: TFIBFloatField
      FieldName = 'PRICEIN'
    end
    object quComplInSSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quComplInSPRICEIN0: TFIBFloatField
      FieldName = 'PRICEIN0'
    end
    object quComplInSSUMIN0: TFIBFloatField
      FieldName = 'SUMIN0'
    end
  end
  object quComplInH: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT dh.ID,'
      '       dh.DATEDOC,'
      '       dh.NUMDOC,'
      '       dh.IDSKL,'
      '       dh.IDSKLTO,'
      '       mh1.EXPNAME'
      'FROM of_docheadcompl dh'
      'left join OF_MH mh1 on mh1.ID=dh.IDSKLTO'
      ''
      'where dh.DATEDOC>=:DATEB and dh.DATEDOC<:DATEE and dh.IACTIVE=1'
      
        'and (dh.IDSKLTO in (SELECT IDMH FROM RPERSONALMH where IDPERSON=' +
        ':IDPERSON))')
    Transaction = dmO.trSelect
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    AutoCommit = True
    Left = 24
    Top = 304
    object quComplInHDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object quComplInHNUMDOC: TFIBStringField
      FieldName = 'NUMDOC'
      Size = 15
      EmptyStrToNull = True
    end
    object quComplInHIDSKL: TFIBIntegerField
      FieldName = 'IDSKL'
    end
    object quComplInHIDSKLTO: TFIBIntegerField
      FieldName = 'IDSKLTO'
    end
    object quComplInHEXPNAME: TFIBStringField
      FieldName = 'EXPNAME'
      Size = 10
      EmptyStrToNull = True
    end
    object quComplInHID: TFIBIntegerField
      FieldName = 'ID'
    end
  end
  object quComplOutH: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT dh.ID,'
      '       dh.DATEDOC,'
      '       dh.NUMDOC,'
      '       dh.IDSKL,'
      '       dh.IDSKLTO,'
      '       mh1.EXPNAME'
      'FROM of_docheadcompl dh'
      'left join OF_MH mh1 on mh1.ID=dh.IDSKL'
      ''
      'where dh.DATEDOC>=:DATEB and dh.DATEDOC<:DATEE and dh.IACTIVE=1'
      
        'and (dh.IDSKL in (SELECT IDMH FROM RPERSONALMH where IDPERSON=:I' +
        'DPERSON))')
    Transaction = dmO.trSelect
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    AutoCommit = True
    Left = 168
    Top = 304
    object quComplOutHID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quComplOutHDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object quComplOutHNUMDOC: TFIBStringField
      FieldName = 'NUMDOC'
      Size = 15
      EmptyStrToNull = True
    end
    object quComplOutHIDSKL: TFIBIntegerField
      FieldName = 'IDSKL'
    end
    object quComplOutHIDSKLTO: TFIBIntegerField
      FieldName = 'IDSKLTO'
    end
    object quComplOutHEXPNAME: TFIBStringField
      FieldName = 'EXPNAME'
      Size = 10
      EmptyStrToNull = True
    end
  end
  object quComplOutS: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT qin.IDHEAD,'
      '       qin.ID,'
      '       qin.IDCARD,'
      '       qin.QUANT,'
      '       qin.IDM,'
      '       qin.KM,'
      '       qin.PRICEIN,'
      '       qin.SUMIN,'
      '       qin.PRICEIN0,'
      '       qin.SUMIN0'
      'FROM OF_DOCSPECCOMPLC qin'
      'where qin.IDHEAD=:IDH'
      'and qin.IDCARD in (5127,5128,5129,5130)')
    Transaction = dmO.trSelect
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    AutoCommit = True
    Left = 248
    Top = 304
    poAskRecordCount = True
    object quComplOutSIDHEAD: TFIBIntegerField
      FieldName = 'IDHEAD'
    end
    object quComplOutSID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quComplOutSIDCARD: TFIBIntegerField
      FieldName = 'IDCARD'
    end
    object quComplOutSQUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quComplOutSIDM: TFIBIntegerField
      FieldName = 'IDM'
    end
    object quComplOutSKM: TFIBFloatField
      FieldName = 'KM'
    end
    object quComplOutSPRICEIN: TFIBFloatField
      FieldName = 'PRICEIN'
    end
    object quComplOutSSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quComplOutSPRICEIN0: TFIBFloatField
      FieldName = 'PRICEIN0'
    end
    object quComplOutSSUMIN0: TFIBFloatField
      FieldName = 'SUMIN0'
    end
  end
  object quComplOutS1: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT qin.IDHEAD,'
      '       qin.ID,'
      '       qin.IDCARD,'
      '       qin.QUANT,'
      '       qin.IDM,'
      '       qin.KM,'
      '       qin.PRICEIN,'
      '       qin.SUMIN,'
      '       qin.PRICEIN0,'
      '       qin.SUMIN0'
      'FROM OF_DOCSPECCOMPLC qin'
      'where qin.IDHEAD=:IDH'
      'and qin.IDCARD not in (5127,5128,5129,5130)')
    Transaction = dmO.trSelect
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    AutoCommit = True
    Left = 324
    Top = 304
    poAskRecordCount = True
    object quComplOutS1IDHEAD: TFIBIntegerField
      FieldName = 'IDHEAD'
    end
    object quComplOutS1ID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quComplOutS1IDCARD: TFIBIntegerField
      FieldName = 'IDCARD'
    end
    object quComplOutS1QUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quComplOutS1IDM: TFIBIntegerField
      FieldName = 'IDM'
    end
    object quComplOutS1KM: TFIBFloatField
      FieldName = 'KM'
    end
    object quComplOutS1PRICEIN: TFIBFloatField
      FieldName = 'PRICEIN'
    end
    object quComplOutS1SUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quComplOutS1PRICEIN0: TFIBFloatField
      FieldName = 'PRICEIN0'
    end
    object quComplOutS1SUMIN0: TFIBFloatField
      FieldName = 'SUMIN0'
    end
  end
  object quSelSumNDSOut: TpFIBDataSet
    SelectSQL.Strings = (
      
        'SELECT ca.INDS,nds.PROC,SUM(ds.RSUM) as RSUM,sum(ds.RSUM*nds.PRO' +
        'C/(100+nds.PROC)) as RSUMNDS'
      'FROM OF_DOCSPECOUTB ds'
      'left join OF_CARDS ca on ca.ID=ds.IDCARD'
      'left join OF_NDS nds on nds.ID=ca.INDS'
      'where IDHEAD=:IDH'
      'group by ca.INDS,nds.PROC')
    Transaction = dmO.trSelect
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    AutoCommit = True
    Left = 108
    Top = 196
    poAskRecordCount = True
    object quSelSumNDSOutINDS: TFIBIntegerField
      FieldName = 'INDS'
    end
    object quSelSumNDSOutPROC: TFIBFloatField
      FieldName = 'PROC'
    end
    object quSelSumNDSOutRSUM: TFIBFloatField
      FieldName = 'RSUM'
    end
    object quSelSumNDSOutRSUMNDS: TFIBFloatField
      FieldName = 'RSUMNDS'
    end
  end
end
