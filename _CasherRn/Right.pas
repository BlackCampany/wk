unit Right;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ExtCtrls, ImgList, StdCtrls,Buttons, cxLookAndFeelPainters, cxButtons, cxControls, cxContainer,
  cxEdit, cxTextEdit, cxCheckBox, Grids, DBGrids, Menus, cxStyles,
  cxCustomData, cxGraphics, cxFilter, cxData, DB, cxDBData, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, Placemnt, cxDataStorage, cxImageComboBox,
  FIBDataSet, pFIBDataSet;

type
  TfmRight = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Splitter1: TSplitter;
    Panel2: TPanel;
    ImageList1: TImageList;
    TreePersonal: TTreeView;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    Panel3: TPanel;
    Bevel1: TBevel;
    Label1: TLabel;
    Edit1: TcxTextEdit;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    Edit2: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    Edit3: TEdit;
    GroupBox1: TGroupBox;
    CheckBox1: TcxCheckBox;
    CheckBox2: TcxCheckBox;
    CheckBox3: TcxCheckBox;
    CheckBox4: TcxCheckBox;
    TreeClassif: TTreeView;
    View1: TcxGridDBTableView;
    Level1: TcxGridLevel;
    Grid1: TcxGrid;
    Button1: TButton;
    CheckBox5: TcxCheckBox;
    CheckBox6: TcxCheckBox;
    CheckBox7: TcxCheckBox;
    View1NAME: TcxGridDBColumn;
    View1PREXEC: TcxGridDBColumn;
    View1COMMENT: TcxGridDBColumn;
    FormPlacement1: TFormPlacement;
    Edit4: TEdit;
    Label4: TLabel;
    TabSheet4: TTabSheet;
    ViMHAll: TcxGridDBTableView;
    LevelMHAll: TcxGridLevel;
    GridMHAll: TcxGrid;
    GridMH: TcxGrid;
    ViMH: TcxGridDBTableView;
    LevelMH: TcxGridLevel;
    Panel4: TPanel;
    cxButton4: TcxButton;
    cxButton5: TcxButton;
    cxButton6: TcxButton;
    cxButton7: TcxButton;
    quMHAll: TpFIBDataSet;
    dsMHAll: TDataSource;
    quMHAllID: TFIBIntegerField;
    quMHAllNAMEMH: TFIBStringField;
    ViMHAllID: TcxGridDBColumn;
    ViMHAllNAMEMH: TcxGridDBColumn;
    quMHPers: TpFIBDataSet;
    dsquMHPers: TDataSource;
    quMHPersIDPERSON: TFIBIntegerField;
    quMHPersIDMH: TFIBIntegerField;
    quMHPersREDIT: TFIBSmallIntField;
    quMHPersNAMEMH: TFIBStringField;
    ViMHIDMH: TcxGridDBColumn;
    ViMHNAMEMH: TcxGridDBColumn;
    Label5: TLabel;
    Label6: TLabel;
    procedure TreePersonalExpanding(Sender: TObject; Node: TTreeNode;
      var AllowExpansion: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure CheckBox4Enter(Sender: TObject);
    procedure TreePersonalChanging(Sender: TObject; Node: TTreeNode;
      var AllowChange: Boolean);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure TreePersonalChange(Sender: TObject; Node: TTreeNode);
    procedure TreeClassifExpanding(Sender: TObject; Node: TTreeNode;
      var AllowExpansion: Boolean);
    procedure TreeClassifChange(Sender: TObject; Node: TTreeNode);
    procedure PageControl1Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure PageControl1Change(Sender: TObject);
    procedure TreeClassifMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Grid1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure View1MouseDown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure cxButton4Click(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
    procedure cxButton7Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmRight: TfmRight;

implementation

uses Dm, MainAdm, Un1, Passw;

{$R *.dfm}

procedure TfmRight.TreePersonalExpanding(Sender: TObject; Node: TTreeNode;
  var AllowExpansion: Boolean);
begin
  with dmC do
   if quFuncList.State in [dsInsert,dsEdit] then quFuncList.Cancel;

  if Node.getFirstChild.Data = nil then
  begin
    Node.DeleteChildren;
    ExpandLevel(Node,TreePersonal,dmC.quPersonal);
  end;
end;

procedure TfmRight.FormCreate(Sender: TObject);
begin
  //������ ��� ����
  Edit1.Text:=''; Edit2.Text:=''; Edit3.Text:='';
  CheckBox1.Checked:=False; CheckBox2.Checked:=False; CheckBox3.Checked:=False; CheckBox4.Checked:=False;

  CommonSet.ItsOffice:=0;
  with dmC do
  begin
    quListTabs.Active:=False;
    quListTabs.Active:=True;
    if quListTabs.Locate('NAMETAB','OF_CLASSIF',[]) then CommonSet.ItsOffice:=1;
    quListTabs.Active:=False;

    if CommonSet.ItsOffice=1 then
    begin
      quClassif.SelectSQL.Clear;
      quClassif.SelectSQL.Add('SELECT a.ID_CLASSIF , a.RIGHTS, b.ID_PARENT, b.NAMECL as NAME');
      quClassif.SelectSQL.Add('from RCLASSIF a, OF_CLASSIF b');
      quClassif.SelectSQL.Add('WHERE');
      quClassif.SelectSQL.Add('b.ID_PARENT=:PARENTID');
      quClassif.SelectSQL.Add('and a.ID_PERSONAL=:PERSONALID');
      quClassif.SelectSQL.Add('and a.ID_CLASSIF=b.ID');

      TabSheet4.Visible:=True;

      quMHAll.Active:=False;
      quMHAll.Active:=True;
      quMHAll.First;

    end else
    begin
      TabSheet4.Visible:=False;

    end;
  end;

  //�������� ������� �������
  ExpandLevel(nil,TreePersonal,dmC.quPersonal);
  try
    RExpandLevel(nil,TreeClassif,dmC.quClassif,dmC.taPersonalID.AsInteger);
  except
  end;
  PageControl1.ActivePageIndex:=0;
  FormPlacement1.IniFileName := CurDir+CurIni;
  FormPlacement1.Active:=True;
end;

procedure TfmRight.CheckBox4Enter(Sender: TObject);
begin
  cxButton2.Enabled:=True;
end;

procedure TfmRight.TreePersonalChanging(Sender: TObject; Node: TTreeNode;
  var AllowChange: Boolean);
begin
  with dmC do
   if quFuncList.State in [dsInsert,dsEdit] then quFuncList.Cancel;

  if cxButton2.Enabled then
  begin
    showmessage('��������� �� ���������. ������� ������ "���������" ��� ����������, ��� ������ "������" ��� ������ ���������.');
    AllowChange:=False;
  end;
end;

procedure TfmRight.cxButton2Click(Sender: TObject);
begin
  //�������� ���������
  if Edit2.Text<>Edit3.Text then
  begin
    showmessage('������ ������ �����������, ���������� ��� ���.');
    exit;
  end;
  with dmC do
  begin
    try
      taPersonal.Edit;
      taPersonalName.AsString:=Edit1.Text;
      if CheckBox1.Checked then taPersonalModul1.AsInteger:=0 else taPersonalModul1.AsInteger:=1;
      if CheckBox2.Checked then taPersonalModul2.AsInteger:=0 else taPersonalModul2.AsInteger:=1;
      if CheckBox3.Checked then taPersonalModul3.AsInteger:=0 else taPersonalModul3.AsInteger:=1;
      if CheckBox5.Checked then taPersonalModul4.AsInteger:=0 else taPersonalModul4.AsInteger:=1;
      if CheckBox6.Checked then taPersonalModul5.AsInteger:=0 else taPersonalModul5.AsInteger:=1;
      if CheckBox7.Checked then taPersonalModul6.AsInteger:=0 else taPersonalModul6.AsInteger:=1;

      if CheckBox4.Checked then taPersonalUvolnen.AsInteger:=0 else taPersonalUvolnen.AsInteger:=1;

      strwk:=Edit2.Text;
      while pos(' ',strwk)>0 do delete(Strwk,pos(' ',strwk),1);
      taPersonalPassw.AsString:=StrWk;

      strwk:=Edit4.Text;
      while pos(' ',strwk)>0 do delete(Strwk,pos(' ',strwk),1);
      taPersonalBARCODE.AsString:=StrWk;

      taPersonal.Post;
//��������� � ���� ������� ���� ������ ��������
      TreePersonal.Items.BeginUpdate;
      TreePersonal.Selected.Text:=Edit1.Text;
      TreePersonal.Items.EndUpdate;
      TreePersonal.Refresh;

    except
    end;
  end;
  cxButton2.Enabled:=False;
end;

procedure TfmRight.cxButton1Click(Sender: TObject);
begin
  if Edit2.Text<>Edit3.Text then
  begin
    showmessage('������ ������ �����������, ���������� ��� ���.');
    exit;
  end;
  //�������� ���������
  with dmC do
  begin
    try
      taPersonal.Edit;
      taPersonalName.AsString:=Edit1.Text;
      if CheckBox1.Checked then taPersonalModul1.AsInteger:=0 else taPersonalModul1.AsInteger:=1;
      if CheckBox2.Checked then taPersonalModul2.AsInteger:=0 else taPersonalModul2.AsInteger:=1;
      if CheckBox3.Checked then taPersonalModul3.AsInteger:=0 else taPersonalModul3.AsInteger:=1;
      if CheckBox5.Checked then taPersonalModul4.AsInteger:=0 else taPersonalModul4.AsInteger:=1;
      if CheckBox6.Checked then taPersonalModul5.AsInteger:=0 else taPersonalModul5.AsInteger:=1;
      if CheckBox7.Checked then taPersonalModul6.AsInteger:=0 else taPersonalModul6.AsInteger:=1;

      if CheckBox4.Checked then taPersonalUvolnen.AsInteger:=0 else taPersonalUvolnen.AsInteger:=1;

      strwk:=Edit2.Text;
      while pos(' ',strwk)>0 do delete(Strwk,pos(' ',strwk),1);
      taPersonalPassw.AsString:=StrWk;

      strwk:=Edit4.Text;
      while pos(' ',strwk)>0 do delete(Strwk,pos(' ',strwk),1);
      taPersonalBARCODE.AsString:=StrWk;
      taPersonal.Post;

    except
    end;
  end;
  cxButton2.Enabled:=False;
  PageControl1.ActivePageIndex:=1;
end;

procedure TfmRight.cxButton3Click(Sender: TObject);
begin
  cxButton2.Enabled:=False;
  Close;
end;

procedure TfmRight.TreePersonalChange(Sender: TObject; Node: TTreeNode);
Var Id:Integer;
begin
  id:=Integer(TreePersonal.Selected.Data);
  with dmC do
  begin
    if quFuncList.State in [dsInsert,dsEdit] then quFuncList.Cancel;

    taPersonal.Locate('ID',ID,[]);
    if taPersonalId_Parent.AsInteger=0 then
    begin  //������ �������������
      Edit1.Text:=taPersonalName.AsString;

      Edit2.Text:='';
      Edit3.Text:='';
      Edit4.Text:='';
      CheckBox1.Checked:=False; CheckBox2.Checked:=False; CheckBox3.Checked:=False; CheckBox4.Checked:=False;CheckBox5.Checked:=False;CheckBox6.Checked:=False; CheckBox7.Checked:=False;
      Edit2.Enabled:=False; Edit3.Enabled:=False; Edit4.Enabled:=False;
      CheckBox1.Enabled:=False; CheckBox2.Enabled:=False; CheckBox3.Enabled:=False; CheckBox4.Enabled:=False;  CheckBox5.Enabled:=False;
      CheckBox5.Enabled:=False; CheckBox6.Enabled:=False; CheckBox7.Enabled:=False;
    end
    else //���������� ������������
    begin
      Edit2.Enabled:=True; Edit3.Enabled:=True; Edit4.Enabled:=True;
      CheckBox1.Enabled:=True; CheckBox2.Enabled:=True; CheckBox3.Enabled:=True; CheckBox4.Enabled:=True;  CheckBox5.Enabled:=True;
      CheckBox5.Enabled:=True; CheckBox6.Enabled:=True; CheckBox7.Enabled:=True;
      Edit1.Text:=taPersonalName.AsString;
      Edit2.Text:=taPersonalPassw.AsString;
      Edit4.Text:=taPersonalBARCODE.AsString;
      
      while Length(Edit2.Text)<15 do Edit2.Text:=Edit2.Text+' ';
      Edit3.Text:=Edit2.Text;
      if taPersonalModul1.AsInteger=0 then CheckBox1.Checked:=True else CheckBox1.Checked:=False;
      if taPersonalModul2.AsInteger=0 then CheckBox2.Checked:=True else CheckBox2.Checked:=False;
      if taPersonalModul3.AsInteger=0 then CheckBox3.Checked:=True else CheckBox3.Checked:=False;
      if taPersonalModul4.AsInteger=0 then CheckBox5.Checked:=True else CheckBox5.Checked:=False;
      if taPersonalModul5.AsInteger=0 then CheckBox6.Checked:=True else CheckBox6.Checked:=False;
      if taPersonalModul6.AsInteger=0 then CheckBox7.Checked:=True else CheckBox7.Checked:=False;

      if taPersonalUvolnen.AsInteger=0 then CheckBox4.Checked:=True else CheckBox4.Checked:=False;


    end;
    //�������� ������ �������������� � �������

    quFuncList.Active:=False;
{    quFuncList.SelectSQL.Clear;
    quFuncList.SelectSQL.Add('Select rf.ID_PERSONAL, rf.NAME, rf.PREXEC, fl.COMMENT');
    quFuncList.SelectSQL.Add('from RFunction rf, RFuncList fl');
    quFuncList.SelectSQL.Add('where');
    quFuncList.SelectSQL.Add('rf.Id_Personal='+IntToStr(Id));
    quFuncList.SelectSQL.Add('and fl.Name=rf.Name');
}
    quFuncList.ParamByName('PERSONAL').Value:=ID;
    quFuncList.Active:=True;
    delay(10);

    quMHPers.Active:=False;
    quMHPers.ParamByName('IPERS').Value:=ID;
    quMHPers.Active:=True;

    RefreshTree(taPersonalId.AsInteger,TreeClassif, quClassif);
  end;
end;

procedure TfmRight.TreeClassifExpanding(Sender: TObject; Node: TTreeNode;
  var AllowExpansion: Boolean);
begin
  if Node.getFirstChild.Data = nil then
  begin
    Node.DeleteChildren;
    RExpandLevel(Node,TreeClassif,dmC.quClassif,dmC.taPersonalID.AsInteger);
  end;
end;

procedure TfmRight.TreeClassifChange(Sender: TObject; Node: TTreeNode);
begin
//  dmC.taClassif.Locate('ID',Integer(TreeClassif.Selected.Data),[]);
end;

procedure TfmRight.PageControl1Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
  if cxButton2.Enabled then
  begin
    showmessage('��������� �� ���������. ������� ������ "���������" ��� ����������, ��� ������ "������" ��� ������ ���������.');
    AllowChange:=False;
  end;
end;

procedure TfmRight.PageControl1Change(Sender: TObject);
begin
  if PageControl1.ActivePageIndex=0 then
  begin
    cxButton1.Enabled:=True;
    cxButton3.Enabled:=True;
  end
  else
  begin
    cxButton1.Enabled:=False;
    cxButton3.Enabled:=False;
  end;
end;

procedure TfmRight.TreeClassifMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
Var CurNode:TTreeNode;
    par:Variant;
begin
  with dmC do
  if button = mbRight then
  begin
    CurNode:=TreeClassif.GetNodeAt(X,Y);

    par := VarArrayCreate([0,1], varInteger);
    par[0]:=taPersonalId.AsInteger;
    par[1]:=Integer(CurNode.Data);

    If CurNode.ImageIndex=0  then
    begin //��� �� ���������� - �������
      CurNode.ImageIndex:=1;
      CurNode.SelectedIndex:=3;
      taRClassif.First;
      if taRClassif.Locate('ID_PERSONAL;ID_CLASSIF',par,[]) then
      begin
        try
          taRClassif.Edit;
          taRClassifRights.AsInteger:=0;
          taRClassif.Post;
        except
        end;
      end;
    end
    else
    begin //��� ���������� - ������ ���������
      CurNode.ImageIndex:=0;
      CurNode.SelectedIndex:=2;
      taRClassif.First;
      if taRClassif.Locate('ID_PERSONAL;ID_CLASSIF',par,[]) then
      begin
        try
          taRClassif.Edit;
          taRClassifRights.AsInteger:=1;
          taRClassif.Post;
        except
        end;
      end;
    end;
  end;
end;

procedure TfmRight.Grid1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Button=mbRight then
  begin
    showmessage('22');
  end;
end;

procedure TfmRight.View1MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbRight then
  begin
    with dmC do
    begin
      delay(10);
      quFuncList.Edit;
      if quFuncListprExec.AsInteger = 1 then quFuncListprExec.AsINteger := 0
      else quFuncListprExec.AsInteger := 1;
      quFuncList.Post;
      delay(10);
      Grid1.Refresh;
    end;
  end;
end;

procedure TfmRight.cxButton4Click(Sender: TObject);
begin
  //�������� ����
  with dmC do
  begin
    if quMHAll.RecordCount>0 then
    begin
      ViMH.BeginUpdate;

      quMHPers.FullRefresh;

      if quMHPers.Locate('IDMH',quMHAllID.AsInteger,[])=False then
      begin
        quMHPers.Append;
        quMHPersIDPERSON.AsInteger:=taPersonalID.AsInteger;
        quMHPersIDMH.AsInteger:=quMHAllID.AsInteger;
        quMHPersREDIT.AsInteger:=1;
        quMHPers.Post;

        quMHPers.Refresh;
      end;

      if quMHPers.Locate('IDMH',0,[])=False then
      begin
        quMHPers.Append;
        quMHPersIDPERSON.AsInteger:=taPersonalID.AsInteger;
        quMHPersIDMH.AsInteger:=0;
        quMHPersREDIT.AsInteger:=1;
        quMHPers.Post;

        quMHPers.Refresh;
      end;

      ViMH.EndUpdate;

    end;
  end;
end;

procedure TfmRight.cxButton5Click(Sender: TObject);
begin
  //�������� ���
  with dmC do
  begin
    ViMH.BeginUpdate;
    
    quMHPers.FullRefresh;

    quMHAll.First;
    while not quMHAll.Eof do
    begin
      if quMHPers.Locate('IDMH',quMHAllID.AsInteger,[])=False then
      begin
        quMHPers.Append;
        quMHPersIDPERSON.AsInteger:=taPersonalID.AsInteger;
        quMHPersIDMH.AsInteger:=quMHAllID.AsInteger;
        quMHPersREDIT.AsInteger:=1;
        quMHPers.Post;

        quMHPers.Refresh;
      end;

      quMHAll.Next;
    end;

    if quMHPers.Locate('IDMH',0,[])=False then
    begin
      quMHPers.Append;
      quMHPersIDPERSON.AsInteger:=taPersonalID.AsInteger;
      quMHPersIDMH.AsInteger:=0;
      quMHPersREDIT.AsInteger:=1;
      quMHPers.Post;

      quMHPers.Refresh;
    end;

    ViMH.EndUpdate;
  end;
end;

procedure TfmRight.cxButton6Click(Sender: TObject);
begin
  if quMHPers.RecordCount>0 then
  begin
    if quMHPersIDMH.AsInteger<>0 then
    begin
      quMHPers.Delete;
    end;
  end;
end;

procedure TfmRight.cxButton7Click(Sender: TObject);
begin
  quMHPers.First;
  while not quMHPers.Eof do quMHPers.Delete;
end;

end.
