program TranspCB;

uses
  Forms,
  uMaintr in 'uMaintr.pas' {fmMainTr},
  Un1 in 'Un1.pas',
  TranspMess in 'TranspMess.pas' {fmTrMess},
  u2fdk in 'U2FDK.PAS',
  uDMTr in 'uDMTR.pas' {dmTr: TDataModule},
  uDMC in 'uDMC.pas' {dmC: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfmMainTr, fmMainTr);
  Application.CreateForm(TfmTrMess, fmTrMess);
  Application.CreateForm(TdmTr, dmTr);
  Application.CreateForm(TdmC, dmC);
  Application.ShowMainForm:=FALSE;
  Application.Run;
end.
