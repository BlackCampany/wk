unit TranspMess;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxLookAndFeelPainters, StdCtrls, cxButtons, ExtCtrls, ComCtrls,
  Menus, cxControls, cxContainer, cxEdit, cxTextEdit, cxMemo;

type
  TfmTrMess = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Button1: TcxButton;
    Button2: TcxButton;
    cxButton1: TcxButton;
    Memo1: TcxMemo;
    procedure Button2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmTrMess: TfmTrMess;
  function GetFileVersion(const FileName: string): string;

implementation

uses Un1, uMaintr;

{$R *.dfm}

procedure TfmTrMess.Button2Click(Sender: TObject);
begin
  close;
end;

procedure TfmTrMess.FormCreate(Sender: TObject);
begin
  Memo1.Clear;
  StatusBar1.Panels[1].Text:='������ '+GetFileVersion(Application.ExeName);
end;

procedure TfmTrMess.Button1Click(Sender: TObject);
//Var F:TextFile;
begin
{  if not FileExists(CommonSet.PathExport+FileInd) then
  begin
    AssignFile(F,CommonSet.PathExport+FileInd);
    rewrite(F);
    CloseFile(F);
  end;}
end;

procedure TfmTrMess.cxButton1Click(Sender: TObject);
begin
  with fmMainTr do
  begin
    iCountSec:=0;
    Timer1.Enabled:=False;
    try
      if prOpenTr then
      begin
        prGetMenu;
        prWriteReal;
        prCloseTr;
      end;
    except
    end;
    Timer1.Enabled:=True;
  end;
end;

function GetFileVersion(const FileName: string): string;
type
  PDWORD = ^DWORD;
  PLangAndCodePage = ^TLangAndCodePage;
  TLangAndCodePage = packed record
    wLanguage: WORD;
    wCodePage: WORD;
  end;
  PLangAndCodePageArray = ^TLangAndCodePageArray;
  TLangAndCodePageArray = array[0..0] of TLangAndCodePage;
var
  loc_InfoBufSize: DWORD;
  loc_InfoBuf: PChar;
  loc_VerBufSize: DWORD;
  loc_VerBuf: PChar;
  cbTranslate: DWORD;
  lpTranslate: PDWORD;
  i: DWORD;
begin
  Result := '';
  if (Length(FileName) = 0) or (not Fileexists(FileName)) then
    Exit;
  loc_InfoBufSize := GetFileVersionInfoSize(PChar(FileName), loc_InfoBufSize);
  if loc_InfoBufSize > 0 then
  begin
    loc_VerBuf := nil;
    loc_InfoBuf := AllocMem(loc_InfoBufSize);
    try
      if not GetFileVersionInfo(PChar(FileName), 0, loc_InfoBufSize, loc_InfoBuf)
        then
        exit;
      if not VerQueryValue(loc_InfoBuf, '\\VarFileInfo\\Translation',
        Pointer(lpTranslate), DWORD(cbTranslate)) then
        exit;
      for i := 0 to (cbTranslate div SizeOf(TLangAndCodePage)) - 1 do
      begin
        if VerQueryValue(
          loc_InfoBuf,
          PChar(Format(
          'StringFileInfo\0%x0%x\FileVersion', [
          PLangAndCodePageArray(lpTranslate)[i].wLanguage,
            PLangAndCodePageArray(lpTranslate)[i].wCodePage])),
            Pointer(loc_VerBuf),
          DWORD(loc_VerBufSize)
          ) then
        begin
          Result := loc_VerBuf;
          Break;
        end;
      end;
    finally
      FreeMem(loc_InfoBuf, loc_InfoBufSize);
    end;
  end;
end;

end.
