unit uDMC;

interface

uses
  SysUtils, Classes, DB, FIBDataSet, pFIBDataSet, FIBDatabase, pFIBDatabase,
  FIBQuery, pFIBQuery;

type
  TdmC = class(TDataModule)
    CasherRnDb: TpFIBDatabase;
    trSelect: TpFIBTransaction;
    trUpdate: TpFIBTransaction;
    quCS: TpFIBDataSet;
    quCSID: TFIBIntegerField;
    quCSCASHNUM: TFIBIntegerField;
    quCSZNUM: TFIBIntegerField;
    quCSCHECKNUM: TFIBIntegerField;
    quCSTAB_ID: TFIBIntegerField;
    quCSTABSUM: TFIBFloatField;
    quCSCLIENTSUM: TFIBFloatField;
    quCSCASHERID: TFIBIntegerField;
    quCSWAITERID: TFIBIntegerField;
    quCSCHDATE: TFIBDateTimeField;
    quCSPAYTYPE: TFIBSmallIntField;
    quCSPAYID: TFIBIntegerField;
    quCSPAYBAR: TFIBStringField;
    quCSIACTIVE: TFIBSmallIntField;
    quTA: TpFIBDataSet;
    quTAID: TFIBIntegerField;
    quTAID_PERSONAL: TFIBIntegerField;
    quTANUMTABLE: TFIBStringField;
    quTAQUESTS: TFIBIntegerField;
    quTATABSUM: TFIBFloatField;
    quTABEGTIME: TFIBDateTimeField;
    quTAENDTIME: TFIBDateTimeField;
    quTADISCONT: TFIBStringField;
    quTAOPERTYPE: TFIBStringField;
    quTACHECKNUM: TFIBIntegerField;
    quTASKLAD: TFIBSmallIntField;
    quTADISCONT1: TFIBStringField;
    quTASTATION: TFIBIntegerField;
    quTANUMZ: TFIBIntegerField;
    quTADELT: TFIBSmallIntField;
    quTASALET: TFIBSmallIntField;
    quTAID_PERSONALCLOSE: TFIBIntegerField;
    quTAIACTIVE: TFIBSmallIntField;
    quTAS: TpFIBDataSet;
    quTASID_TAB: TFIBIntegerField;
    quTASID: TFIBIntegerField;
    quTASID_PERSONAL: TFIBIntegerField;
    quTASNUMTABLE: TFIBStringField;
    quTASSIFR: TFIBIntegerField;
    quTASPRICE: TFIBFloatField;
    quTASQUANTITY: TFIBFloatField;
    quTASDISCOUNTPROC: TFIBFloatField;
    quTASDISCOUNTSUM: TFIBFloatField;
    quTASSUMMA: TFIBFloatField;
    quTASISTATUS: TFIBIntegerField;
    quTASITYPE: TFIBSmallIntField;
    quTASQUANTITY1: TFIBFloatField;
    quTASINEED: TFIBIntegerField;
    quTASIPRINT: TFIBIntegerField;
    quTASSTREAM: TFIBIntegerField;
    quUpd1T: TpFIBQuery;
    quMenuId: TpFIBDataSet;
    quMenuIdSIFR: TFIBIntegerField;
    quMenuIdNAME: TFIBStringField;
    quMenuIdPRICE: TFIBFloatField;
    quMenuIdCODE: TFIBStringField;
    quMenuIdTREETYPE: TFIBStringField;
    quMenuIdLIMITPRICE: TFIBFloatField;
    quMenuIdCATEG: TFIBSmallIntField;
    quMenuIdPARENT: TFIBSmallIntField;
    quMenuIdLINK: TFIBSmallIntField;
    quMenuIdSTREAM: TFIBSmallIntField;
    quMenuIdLACK: TFIBSmallIntField;
    quMenuIdDESIGNSIFR: TFIBSmallIntField;
    quMenuIdALTNAME: TFIBStringField;
    quMenuIdNALOG: TFIBFloatField;
    quMenuIdBARCODE: TFIBStringField;
    quMenuIdIMAGE: TFIBSmallIntField;
    quMenuIdCONSUMMA: TFIBFloatField;
    quMenuIdMINREST: TFIBSmallIntField;
    quMenuIdPRNREST: TFIBSmallIntField;
    quMenuIdCOOKTIME: TFIBSmallIntField;
    quMenuIdDISPENSER: TFIBSmallIntField;
    quMenuIdDISPKOEF: TFIBSmallIntField;
    quMenuIdACCESS: TFIBSmallIntField;
    quMenuIdFLAGS: TFIBSmallIntField;
    quMenuIdTARA: TFIBSmallIntField;
    quMenuIdCNTPRICE: TFIBSmallIntField;
    quMenuIdBACKBGR: TFIBFloatField;
    quMenuIdFONTBGR: TFIBFloatField;
    quMenuIdIACTIVE: TFIBSmallIntField;
    quMenuIdIEDIT: TFIBSmallIntField;
    quMenuIdDATEB: TFIBIntegerField;
    quMenuIdDATEE: TFIBIntegerField;
    quMenuIdDAYWEEK: TFIBStringField;
    quMenuIdTIMEB: TFIBTimeField;
    quMenuIdTIMEE: TFIBTimeField;
    quMenuIdALLTIME: TFIBSmallIntField;
    quBarC: TpFIBDataSet;
    quBarCBARCODE: TFIBStringField;
    quBarCSIFR: TFIBIntegerField;
    quBarCQUANT: TFIBFloatField;
    quBarCPRICE: TFIBFloatField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmC: TdmC;

implementation

{$R *.dfm}

end.
