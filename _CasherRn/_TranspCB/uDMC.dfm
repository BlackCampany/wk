object dmC: TdmC
  OldCreateOrder = False
  Left = 1549
  Top = 387
  Height = 254
  Width = 292
  object CasherRnDb: TpFIBDatabase
    DBName = '192.168.0.76:E:\_OfficeRn\DB\OFFICERN.GDB'
    DBParams.Strings = (
      'user_name=SYSDBA'
      'lc_ctype=WIN1251'
      'password=masterkey'
      'sql_role_name=SYSDBA')
    DefaultTransaction = trSelect
    DefaultUpdateTransaction = trUpdate
    SQLDialect = 3
    Timeout = 0
    SynchronizeTime = False
    DesignDBOptions = [ddoIsDefaultDatabase]
    AliasName = 'CasherRnDb'
    WaitForRestoreConnect = 0
    Left = 24
    Top = 15
  end
  object trSelect: TpFIBTransaction
    DefaultDatabase = CasherRnDb
    TimeoutAction = TARollback
    Left = 24
    Top = 72
  end
  object trUpdate: TpFIBTransaction
    DefaultDatabase = CasherRnDb
    TimeoutAction = TARollback
    Left = 24
    Top = 128
  end
  object quCS: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE TABLES_ALL'
      'SET '
      '    ID_PERSONAL = :ID_PERSONAL,'
      '    NUMTABLE = :NUMTABLE,'
      '    QUESTS = :QUESTS,'
      '    TABSUM = :TABSUM,'
      '    BEGTIME = :BEGTIME,'
      '    ENDTIME = :ENDTIME,'
      '    DISCONT = :DISCONT,'
      '    OPERTYPE = :OPERTYPE,'
      '    CHECKNUM = :CHECKNUM,'
      '    SKLAD = :SKLAD,'
      '    DISCONT1 = :DISCONT1,'
      '    STATION = :STATION,'
      '    NUMZ = :NUMZ,'
      '    DELT = :DELT,'
      '    SALET = :SALET,'
      '    ID_PERSONALCLOSE = :ID_PERSONALCLOSE'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    TABLES_ALL'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO TABLES_ALL('
      '    ID,'
      '    ID_PERSONAL,'
      '    NUMTABLE,'
      '    QUESTS,'
      '    TABSUM,'
      '    BEGTIME,'
      '    ENDTIME,'
      '    DISCONT,'
      '    OPERTYPE,'
      '    CHECKNUM,'
      '    SKLAD,'
      '    DISCONT1,'
      '    STATION,'
      '    NUMZ,'
      '    DELT,'
      '    SALET,'
      '    ID_PERSONALCLOSE'
      ')'
      'VALUES('
      '    :ID,'
      '    :ID_PERSONAL,'
      '    :NUMTABLE,'
      '    :QUESTS,'
      '    :TABSUM,'
      '    :BEGTIME,'
      '    :ENDTIME,'
      '    :DISCONT,'
      '    :OPERTYPE,'
      '    :CHECKNUM,'
      '    :SKLAD,'
      '    :DISCONT1,'
      '    :STATION,'
      '    :NUMZ,'
      '    :DELT,'
      '    :SALET,'
      '    :ID_PERSONALCLOSE'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    ID_PERSONAL,'
      '    NUMTABLE,'
      '    QUESTS,'
      '    TABSUM,'
      '    BEGTIME,'
      '    ENDTIME,'
      '    DISCONT,'
      '    OPERTYPE,'
      '    CHECKNUM,'
      '    SKLAD,'
      '    DISCONT1,'
      '    STATION,'
      '    NUMZ,'
      '    DELT,'
      '    SALET,'
      '    ID_PERSONALCLOSE'
      'FROM'
      '    TABLES_ALL '
      'WHERE( '
      '    ID=:TID'
      '     ) and (     TABLES_ALL.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    CASHNUM,'
      '    ZNUM,'
      '    CHECKNUM,'
      '    TAB_ID,'
      '    TABSUM,'
      '    CLIENTSUM,'
      '    CASHERID,'
      '    WAITERID,'
      '    CHDATE,'
      '    PAYTYPE,'
      '    PAYID,'
      '    PAYBAR,'
      '    IACTIVE'
      'FROM'
      '    CASHSAIL '
      'where TAB_ID=:IDH')
    Transaction = trSelect
    Database = CasherRnDb
    UpdateTransaction = trUpdate
    AutoCommit = True
    Left = 87
    Top = 16
    object quCSID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quCSCASHNUM: TFIBIntegerField
      FieldName = 'CASHNUM'
    end
    object quCSZNUM: TFIBIntegerField
      FieldName = 'ZNUM'
    end
    object quCSCHECKNUM: TFIBIntegerField
      FieldName = 'CHECKNUM'
    end
    object quCSTAB_ID: TFIBIntegerField
      FieldName = 'TAB_ID'
    end
    object quCSTABSUM: TFIBFloatField
      FieldName = 'TABSUM'
    end
    object quCSCLIENTSUM: TFIBFloatField
      FieldName = 'CLIENTSUM'
    end
    object quCSCASHERID: TFIBIntegerField
      FieldName = 'CASHERID'
    end
    object quCSWAITERID: TFIBIntegerField
      FieldName = 'WAITERID'
    end
    object quCSCHDATE: TFIBDateTimeField
      FieldName = 'CHDATE'
    end
    object quCSPAYTYPE: TFIBSmallIntField
      FieldName = 'PAYTYPE'
    end
    object quCSPAYID: TFIBIntegerField
      FieldName = 'PAYID'
    end
    object quCSPAYBAR: TFIBStringField
      FieldName = 'PAYBAR'
      Size = 30
      EmptyStrToNull = True
    end
    object quCSIACTIVE: TFIBSmallIntField
      FieldName = 'IACTIVE'
    end
  end
  object quTA: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE TABLES_ALL'
      'SET '
      '    ID_PERSONAL = :ID_PERSONAL,'
      '    NUMTABLE = :NUMTABLE,'
      '    QUESTS = :QUESTS,'
      '    TABSUM = :TABSUM,'
      '    BEGTIME = :BEGTIME,'
      '    ENDTIME = :ENDTIME,'
      '    DISCONT = :DISCONT,'
      '    OPERTYPE = :OPERTYPE,'
      '    CHECKNUM = :CHECKNUM,'
      '    SKLAD = :SKLAD,'
      '    DISCONT1 = :DISCONT1,'
      '    STATION = :STATION,'
      '    NUMZ = :NUMZ,'
      '    DELT = :DELT,'
      '    SALET = :SALET,'
      '    ID_PERSONALCLOSE = :ID_PERSONALCLOSE,'
      '    IACTIVE = :IACTIVE'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    TABLES_ALL'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO TABLES_ALL('
      '    ID,'
      '    ID_PERSONAL,'
      '    NUMTABLE,'
      '    QUESTS,'
      '    TABSUM,'
      '    BEGTIME,'
      '    ENDTIME,'
      '    DISCONT,'
      '    OPERTYPE,'
      '    CHECKNUM,'
      '    SKLAD,'
      '    DISCONT1,'
      '    STATION,'
      '    NUMZ,'
      '    DELT,'
      '    SALET,'
      '    ID_PERSONALCLOSE,'
      '    IACTIVE'
      ')'
      'VALUES('
      '    :ID,'
      '    :ID_PERSONAL,'
      '    :NUMTABLE,'
      '    :QUESTS,'
      '    :TABSUM,'
      '    :BEGTIME,'
      '    :ENDTIME,'
      '    :DISCONT,'
      '    :OPERTYPE,'
      '    :CHECKNUM,'
      '    :SKLAD,'
      '    :DISCONT1,'
      '    :STATION,'
      '    :NUMZ,'
      '    :DELT,'
      '    :SALET,'
      '    :ID_PERSONALCLOSE,'
      '    :IACTIVE'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    ID_PERSONAL,'
      '    NUMTABLE,'
      '    QUESTS,'
      '    TABSUM,'
      '    BEGTIME,'
      '    ENDTIME,'
      '    DISCONT,'
      '    OPERTYPE,'
      '    CHECKNUM,'
      '    SKLAD,'
      '    DISCONT1,'
      '    STATION,'
      '    NUMZ,'
      '    DELT,'
      '    SALET,'
      '    ID_PERSONALCLOSE,'
      '    IACTIVE'
      'FROM'
      '    TABLES_ALL '
      'where(  IACTIVE=2'
      '     ) and (     TABLES_ALL.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    ID_PERSONAL,'
      '    NUMTABLE,'
      '    QUESTS,'
      '    TABSUM,'
      '    BEGTIME,'
      '    ENDTIME,'
      '    DISCONT,'
      '    OPERTYPE,'
      '    CHECKNUM,'
      '    SKLAD,'
      '    DISCONT1,'
      '    STATION,'
      '    NUMZ,'
      '    DELT,'
      '    SALET,'
      '    ID_PERSONALCLOSE,'
      '    IACTIVE'
      'FROM'
      '    TABLES_ALL '
      'where IACTIVE=2')
    Transaction = trSelect
    Database = CasherRnDb
    UpdateTransaction = trUpdate
    AutoCommit = True
    Left = 143
    Top = 16
    object quTAID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quTAID_PERSONAL: TFIBIntegerField
      FieldName = 'ID_PERSONAL'
    end
    object quTANUMTABLE: TFIBStringField
      FieldName = 'NUMTABLE'
      EmptyStrToNull = True
    end
    object quTAQUESTS: TFIBIntegerField
      FieldName = 'QUESTS'
    end
    object quTATABSUM: TFIBFloatField
      DefaultExpression = '0,0'
      FieldName = 'TABSUM'
    end
    object quTABEGTIME: TFIBDateTimeField
      FieldName = 'BEGTIME'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object quTAENDTIME: TFIBDateTimeField
      FieldName = 'ENDTIME'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object quTADISCONT: TFIBStringField
      FieldName = 'DISCONT'
      EmptyStrToNull = True
    end
    object quTAOPERTYPE: TFIBStringField
      DefaultExpression = #39'SALE'#39
      FieldName = 'OPERTYPE'
      Size = 10
      EmptyStrToNull = True
    end
    object quTACHECKNUM: TFIBIntegerField
      DefaultExpression = '0'
      FieldName = 'CHECKNUM'
    end
    object quTASKLAD: TFIBSmallIntField
      DefaultExpression = '0'
      FieldName = 'SKLAD'
    end
    object quTADISCONT1: TFIBStringField
      FieldName = 'DISCONT1'
      Size = 30
      EmptyStrToNull = True
    end
    object quTASTATION: TFIBIntegerField
      FieldName = 'STATION'
    end
    object quTANUMZ: TFIBIntegerField
      FieldName = 'NUMZ'
    end
    object quTADELT: TFIBSmallIntField
      FieldName = 'DELT'
    end
    object quTASALET: TFIBSmallIntField
      FieldName = 'SALET'
    end
    object quTAID_PERSONALCLOSE: TFIBIntegerField
      FieldName = 'ID_PERSONALCLOSE'
    end
    object quTAIACTIVE: TFIBSmallIntField
      DefaultExpression = '1'
      FieldName = 'IACTIVE'
    end
  end
  object quTAS: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE TABLES_ALL'
      'SET '
      '    ID_PERSONAL = :ID_PERSONAL,'
      '    NUMTABLE = :NUMTABLE,'
      '    QUESTS = :QUESTS,'
      '    TABSUM = :TABSUM,'
      '    BEGTIME = :BEGTIME,'
      '    ENDTIME = :ENDTIME,'
      '    DISCONT = :DISCONT,'
      '    OPERTYPE = :OPERTYPE,'
      '    CHECKNUM = :CHECKNUM,'
      '    SKLAD = :SKLAD,'
      '    DISCONT1 = :DISCONT1,'
      '    STATION = :STATION,'
      '    NUMZ = :NUMZ,'
      '    DELT = :DELT,'
      '    SALET = :SALET,'
      '    ID_PERSONALCLOSE = :ID_PERSONALCLOSE'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    TABLES_ALL'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO TABLES_ALL('
      '    ID,'
      '    ID_PERSONAL,'
      '    NUMTABLE,'
      '    QUESTS,'
      '    TABSUM,'
      '    BEGTIME,'
      '    ENDTIME,'
      '    DISCONT,'
      '    OPERTYPE,'
      '    CHECKNUM,'
      '    SKLAD,'
      '    DISCONT1,'
      '    STATION,'
      '    NUMZ,'
      '    DELT,'
      '    SALET,'
      '    ID_PERSONALCLOSE'
      ')'
      'VALUES('
      '    :ID,'
      '    :ID_PERSONAL,'
      '    :NUMTABLE,'
      '    :QUESTS,'
      '    :TABSUM,'
      '    :BEGTIME,'
      '    :ENDTIME,'
      '    :DISCONT,'
      '    :OPERTYPE,'
      '    :CHECKNUM,'
      '    :SKLAD,'
      '    :DISCONT1,'
      '    :STATION,'
      '    :NUMZ,'
      '    :DELT,'
      '    :SALET,'
      '    :ID_PERSONALCLOSE'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    ID_PERSONAL,'
      '    NUMTABLE,'
      '    QUESTS,'
      '    TABSUM,'
      '    BEGTIME,'
      '    ENDTIME,'
      '    DISCONT,'
      '    OPERTYPE,'
      '    CHECKNUM,'
      '    SKLAD,'
      '    DISCONT1,'
      '    STATION,'
      '    NUMZ,'
      '    DELT,'
      '    SALET,'
      '    ID_PERSONALCLOSE'
      'FROM'
      '    TABLES_ALL '
      'WHERE( '
      '    ID=:TID'
      '     ) and (     TABLES_ALL.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    ID_TAB,'
      '    ID,'
      '    ID_PERSONAL,'
      '    NUMTABLE,'
      '    SIFR,'
      '    PRICE,'
      '    QUANTITY,'
      '    DISCOUNTPROC,'
      '    DISCOUNTSUM,'
      '    SUMMA,'
      '    ISTATUS,'
      '    ITYPE,'
      '    QUANTITY1,'
      '    INEED,'
      '    IPRINT,'
      '    STREAM'
      'FROM'
      '    SPEC_ALL '
      'where ID_TAB=:IDH'
      'order by ID')
    Transaction = trSelect
    Database = CasherRnDb
    UpdateTransaction = trUpdate
    AutoCommit = True
    Left = 143
    Top = 72
    object quTASID_TAB: TFIBIntegerField
      FieldName = 'ID_TAB'
    end
    object quTASID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quTASID_PERSONAL: TFIBIntegerField
      FieldName = 'ID_PERSONAL'
    end
    object quTASNUMTABLE: TFIBStringField
      FieldName = 'NUMTABLE'
      EmptyStrToNull = True
    end
    object quTASSIFR: TFIBIntegerField
      FieldName = 'SIFR'
    end
    object quTASPRICE: TFIBFloatField
      FieldName = 'PRICE'
    end
    object quTASQUANTITY: TFIBFloatField
      FieldName = 'QUANTITY'
    end
    object quTASDISCOUNTPROC: TFIBFloatField
      FieldName = 'DISCOUNTPROC'
    end
    object quTASDISCOUNTSUM: TFIBFloatField
      FieldName = 'DISCOUNTSUM'
    end
    object quTASSUMMA: TFIBFloatField
      FieldName = 'SUMMA'
    end
    object quTASISTATUS: TFIBIntegerField
      FieldName = 'ISTATUS'
    end
    object quTASITYPE: TFIBSmallIntField
      FieldName = 'ITYPE'
    end
    object quTASQUANTITY1: TFIBFloatField
      FieldName = 'QUANTITY1'
    end
    object quTASINEED: TFIBIntegerField
      FieldName = 'INEED'
    end
    object quTASIPRINT: TFIBIntegerField
      FieldName = 'IPRINT'
    end
    object quTASSTREAM: TFIBIntegerField
      FieldName = 'STREAM'
    end
  end
  object quUpd1T: TpFIBQuery
    Transaction = trUpdate
    Database = CasherRnDb
    SQL.Strings = (
      'Update TABLES_ALL Set IACTIVE=2 '
      'where IACTIVE=1'
      '    ')
    Left = 92
    Top = 128
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object quMenuId: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE MENU'
      'SET '
      '    NAME = :NAME,'
      '    PRICE = :PRICE,'
      '    CODE = :CODE,'
      '    TREETYPE = :TREETYPE,'
      '    LIMITPRICE = :LIMITPRICE,'
      '    CATEG = :CATEG,'
      '    PARENT = :PARENT,'
      '    LINK = :LINK,'
      '    STREAM = :STREAM,'
      '    LACK = :LACK,'
      '    DESIGNSIFR = :DESIGNSIFR,'
      '    ALTNAME = :ALTNAME,'
      '    NALOG = :NALOG,'
      '    BARCODE = :BARCODE,'
      '    IMAGE = :IMAGE,'
      '    CONSUMMA = :CONSUMMA,'
      '    MINREST = :MINREST,'
      '    PRNREST = :PRNREST,'
      '    COOKTIME = :COOKTIME,'
      '    DISPENSER = :DISPENSER,'
      '    DISPKOEF = :DISPKOEF,'
      '    ACCESS = :ACCESS,'
      '    FLAGS = :FLAGS,'
      '    TARA = :TARA,'
      '    CNTPRICE = :CNTPRICE,'
      '    BACKBGR = :BACKBGR,'
      '    FONTBGR = :FONTBGR,'
      '    IACTIVE = :IACTIVE,'
      '    IEDIT = :IEDIT,'
      '    DATEB = :DATEB,'
      '    DATEE = :DATEE,'
      '    DAYWEEK = :DAYWEEK,'
      '    TIMEB = :TIMEB,'
      '    TIMEE = :TIMEE,'
      '    ALLTIME = :ALLTIME'
      'WHERE'
      '    SIFR = :OLD_SIFR'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    MENU'
      'WHERE'
      '        SIFR = :OLD_SIFR'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO MENU('
      '    SIFR,'
      '    NAME,'
      '    PRICE,'
      '    CODE,'
      '    TREETYPE,'
      '    LIMITPRICE,'
      '    CATEG,'
      '    PARENT,'
      '    LINK,'
      '    STREAM,'
      '    LACK,'
      '    DESIGNSIFR,'
      '    ALTNAME,'
      '    NALOG,'
      '    BARCODE,'
      '    IMAGE,'
      '    CONSUMMA,'
      '    MINREST,'
      '    PRNREST,'
      '    COOKTIME,'
      '    DISPENSER,'
      '    DISPKOEF,'
      '    ACCESS,'
      '    FLAGS,'
      '    TARA,'
      '    CNTPRICE,'
      '    BACKBGR,'
      '    FONTBGR,'
      '    IACTIVE,'
      '    IEDIT,'
      '    DATEB,'
      '    DATEE,'
      '    DAYWEEK,'
      '    TIMEB,'
      '    TIMEE,'
      '    ALLTIME'
      ')'
      'VALUES('
      '    :SIFR,'
      '    :NAME,'
      '    :PRICE,'
      '    :CODE,'
      '    :TREETYPE,'
      '    :LIMITPRICE,'
      '    :CATEG,'
      '    :PARENT,'
      '    :LINK,'
      '    :STREAM,'
      '    :LACK,'
      '    :DESIGNSIFR,'
      '    :ALTNAME,'
      '    :NALOG,'
      '    :BARCODE,'
      '    :IMAGE,'
      '    :CONSUMMA,'
      '    :MINREST,'
      '    :PRNREST,'
      '    :COOKTIME,'
      '    :DISPENSER,'
      '    :DISPKOEF,'
      '    :ACCESS,'
      '    :FLAGS,'
      '    :TARA,'
      '    :CNTPRICE,'
      '    :BACKBGR,'
      '    :FONTBGR,'
      '    :IACTIVE,'
      '    :IEDIT,'
      '    :DATEB,'
      '    :DATEE,'
      '    :DAYWEEK,'
      '    :TIMEB,'
      '    :TIMEE,'
      '    :ALLTIME'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    SIFR,'
      '    NAME,'
      '    PRICE,'
      '    CODE,'
      '    TREETYPE,'
      '    LIMITPRICE,'
      '    CATEG,'
      '    PARENT,'
      '    LINK,'
      '    STREAM,'
      '    LACK,'
      '    DESIGNSIFR,'
      '    ALTNAME,'
      '    NALOG,'
      '    BARCODE,'
      '    IMAGE,'
      '    CONSUMMA,'
      '    MINREST,'
      '    PRNREST,'
      '    COOKTIME,'
      '    DISPENSER,'
      '    DISPKOEF,'
      '    ACCESS,'
      '    FLAGS,'
      '    TARA,'
      '    CNTPRICE,'
      '    BACKBGR,'
      '    FONTBGR,'
      '    IACTIVE,'
      '    IEDIT,'
      '    DATEB,'
      '    DATEE,'
      '    DAYWEEK,'
      '    TIMEB,'
      '    TIMEE,'
      '    ALLTIME'
      'FROM'
      '    MENU '
      'where(  SIFR=:SIFR'
      '     ) and (     MENU.SIFR = :OLD_SIFR'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    SIFR,'
      '    NAME,'
      '    PRICE,'
      '    CODE,'
      '    TREETYPE,'
      '    LIMITPRICE,'
      '    CATEG,'
      '    PARENT,'
      '    LINK,'
      '    STREAM,'
      '    LACK,'
      '    DESIGNSIFR,'
      '    ALTNAME,'
      '    NALOG,'
      '    BARCODE,'
      '    IMAGE,'
      '    CONSUMMA,'
      '    MINREST,'
      '    PRNREST,'
      '    COOKTIME,'
      '    DISPENSER,'
      '    DISPKOEF,'
      '    ACCESS,'
      '    FLAGS,'
      '    TARA,'
      '    CNTPRICE,'
      '    BACKBGR,'
      '    FONTBGR,'
      '    IACTIVE,'
      '    IEDIT,'
      '    DATEB,'
      '    DATEE,'
      '    DAYWEEK,'
      '    TIMEB,'
      '    TIMEE,'
      '    ALLTIME'
      'FROM'
      '    MENU '
      'where SIFR=:SIFR')
    Transaction = trSelect
    Database = CasherRnDb
    UpdateTransaction = trUpdate
    AutoCommit = True
    Left = 215
    Top = 16
    poAskRecordCount = True
    object quMenuIdSIFR: TFIBIntegerField
      FieldName = 'SIFR'
    end
    object quMenuIdNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 100
      EmptyStrToNull = True
    end
    object quMenuIdPRICE: TFIBFloatField
      FieldName = 'PRICE'
    end
    object quMenuIdCODE: TFIBStringField
      FieldName = 'CODE'
      Size = 10
      EmptyStrToNull = True
    end
    object quMenuIdTREETYPE: TFIBStringField
      FieldName = 'TREETYPE'
      Size = 1
      EmptyStrToNull = True
    end
    object quMenuIdLIMITPRICE: TFIBFloatField
      FieldName = 'LIMITPRICE'
    end
    object quMenuIdCATEG: TFIBSmallIntField
      FieldName = 'CATEG'
    end
    object quMenuIdPARENT: TFIBSmallIntField
      FieldName = 'PARENT'
    end
    object quMenuIdLINK: TFIBSmallIntField
      FieldName = 'LINK'
    end
    object quMenuIdSTREAM: TFIBSmallIntField
      FieldName = 'STREAM'
    end
    object quMenuIdLACK: TFIBSmallIntField
      FieldName = 'LACK'
    end
    object quMenuIdDESIGNSIFR: TFIBSmallIntField
      FieldName = 'DESIGNSIFR'
    end
    object quMenuIdALTNAME: TFIBStringField
      FieldName = 'ALTNAME'
      Size = 100
      EmptyStrToNull = True
    end
    object quMenuIdNALOG: TFIBFloatField
      FieldName = 'NALOG'
    end
    object quMenuIdBARCODE: TFIBStringField
      FieldName = 'BARCODE'
      EmptyStrToNull = True
    end
    object quMenuIdIMAGE: TFIBSmallIntField
      FieldName = 'IMAGE'
    end
    object quMenuIdCONSUMMA: TFIBFloatField
      FieldName = 'CONSUMMA'
    end
    object quMenuIdMINREST: TFIBSmallIntField
      FieldName = 'MINREST'
    end
    object quMenuIdPRNREST: TFIBSmallIntField
      FieldName = 'PRNREST'
    end
    object quMenuIdCOOKTIME: TFIBSmallIntField
      FieldName = 'COOKTIME'
    end
    object quMenuIdDISPENSER: TFIBSmallIntField
      FieldName = 'DISPENSER'
    end
    object quMenuIdDISPKOEF: TFIBSmallIntField
      FieldName = 'DISPKOEF'
    end
    object quMenuIdACCESS: TFIBSmallIntField
      FieldName = 'ACCESS'
    end
    object quMenuIdFLAGS: TFIBSmallIntField
      FieldName = 'FLAGS'
    end
    object quMenuIdTARA: TFIBSmallIntField
      FieldName = 'TARA'
    end
    object quMenuIdCNTPRICE: TFIBSmallIntField
      FieldName = 'CNTPRICE'
    end
    object quMenuIdBACKBGR: TFIBFloatField
      FieldName = 'BACKBGR'
    end
    object quMenuIdFONTBGR: TFIBFloatField
      FieldName = 'FONTBGR'
    end
    object quMenuIdIACTIVE: TFIBSmallIntField
      FieldName = 'IACTIVE'
    end
    object quMenuIdIEDIT: TFIBSmallIntField
      FieldName = 'IEDIT'
    end
    object quMenuIdDATEB: TFIBIntegerField
      FieldName = 'DATEB'
    end
    object quMenuIdDATEE: TFIBIntegerField
      FieldName = 'DATEE'
    end
    object quMenuIdDAYWEEK: TFIBStringField
      FieldName = 'DAYWEEK'
      Size = 10
      EmptyStrToNull = True
    end
    object quMenuIdTIMEB: TFIBTimeField
      FieldName = 'TIMEB'
    end
    object quMenuIdTIMEE: TFIBTimeField
      FieldName = 'TIMEE'
    end
    object quMenuIdALLTIME: TFIBSmallIntField
      FieldName = 'ALLTIME'
    end
  end
  object quBarC: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE BARCODE'
      'SET '
      '    SIFR = :SIFR,'
      '    QUANT = :QUANT,'
      '    PRICE = :PRICE'
      'WHERE'
      '    BARCODE = :OLD_BARCODE'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    BARCODE'
      'WHERE'
      '        BARCODE = :OLD_BARCODE'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO BARCODE('
      '    BARCODE,'
      '    SIFR,'
      '    QUANT,'
      '    PRICE'
      ')'
      'VALUES('
      '    :BARCODE,'
      '    :SIFR,'
      '    :QUANT,'
      '    :PRICE'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    BARCODE,'
      '    SIFR,'
      '    QUANT,'
      '    PRICE'
      'FROM'
      '    BARCODE '
      'where(  BARCODE=:SBAR'
      '     ) and (     BARCODE.BARCODE = :OLD_BARCODE'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    BARCODE,'
      '    SIFR,'
      '    QUANT,'
      '    PRICE'
      'FROM'
      '    BARCODE '
      'where BARCODE=:SBAR')
    Transaction = trSelect
    Database = CasherRnDb
    UpdateTransaction = trUpdate
    AutoCommit = True
    Left = 211
    Top = 124
    poAskRecordCount = True
    object quBarCBARCODE: TFIBStringField
      FieldName = 'BARCODE'
      EmptyStrToNull = True
    end
    object quBarCSIFR: TFIBIntegerField
      FieldName = 'SIFR'
    end
    object quBarCQUANT: TFIBFloatField
      FieldName = 'QUANT'
    end
    object quBarCPRICE: TFIBFloatField
      FieldName = 'PRICE'
    end
  end
end
