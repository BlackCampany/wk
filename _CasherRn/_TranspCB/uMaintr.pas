unit uMaintr;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, RXShell, ExtCtrls, EasyCompression, IdBaseComponent,
  IdComponent, IdTCPConnection, IdTCPClient, IdFTP, FileUtil, DB, StdCtrls, sqldataset, StrUtils;


type
  TfmMainTr = class(TForm)
    RxTrayIcon1: TRxTrayIcon;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    Timer1: TTimer;
    procedure N3Click(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure RxTrayIcon1Click(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
    function trGroup(sStr:String):Boolean;
    function trSubGroup(sStr:String):Boolean;
    function trSubGroup1(sStr:String):Boolean;
    function trCountry(sStr:String):Boolean;
    function trBar(sStr:String):Boolean;
    function trGoods(sStr:String):Boolean;
    function trClients(sStr:String):Boolean;
    function trDisc(sStr:String):Boolean;
    function trBrand(sStr:String):Boolean;
    function trGrpNac(sStr:String):Boolean;
    function trGdsNac(sStr:String):Boolean;
    function trGdsCat(sStr:String):Boolean;
    function trGdsDisc(sStr:String):Boolean;
    function trPostAss(sStr:String):Boolean;
    function trGdsPrice(sStr:String):Boolean;
    procedure prGetMenu;
    procedure prWriteReal;
    function prOpenTr:Boolean;
    procedure prCloseTr;
  end;
{
Procedure prTransp;
Procedure prTranspImp;
Procedure prTranspExp;
Procedure prImpToCrystal;
procedure FindSortFiles;
procedure FindSortFilesOut;
Function ImportFile(fName:String;Var iOk,iNon:Integer):Boolean;
}
function stf(StrWk:string;D:Extended):real;

Procedure WrMess(S:String);
procedure WrF(S:String);

var
  fmMainTr: TfmMainTr;
  fs: TECLFileStream;
  bClearPostAss:Boolean;


implementation

uses Un1, TranspMess, u2fdk, uDMTr, uDMC;

{$R *.dfm}

procedure WrF(S:String);
var F: TextFile;
    Strwk1:String;
    FileN:String;
begin

  try
    Strwk1:='transport.txt';
    Application.ProcessMessages;
    FileN:=CurDir+strwk1;
    AssignFile(F, FileN);
    Rewrite(F);
    WriteLn(F,S);
    Flush(F);
  finally
    CloseFile(F);
  end;
end;


function TFmMainTr.trDisc(sStr:String):Boolean;
begin
  Result:=True;
end;

function TFmMainTr.trGdsDisc(sStr:String):Boolean;
begin
  Result:=True;
end;


function TFmMainTr.trGdsPrice(sStr:String):Boolean;
begin
  Result:=True;
end;


function TFmMainTr.trPostAss(sStr:String):Boolean;
begin
  Result:=True;
end;



function TFmMainTr.trClients(sStr:String):Boolean;
begin
  Result:=True;
end;

function TFmMainTr.trGoods(sStr:String):Boolean;
begin
  Result:=True;
end;

Function TFmMainTr.trBar(sStr:String):Boolean;
begin
  Result:=True;
end;



function TFmMainTr.trCountry(sStr:String):Boolean;
begin
  Result:=True;
end;


function TFmMainTr.trSubGroup(sStr:String):Boolean;
begin
  Result:=True;
end;

function TFmMainTr.trSubGroup1(sStr:String):Boolean;
begin
  Result:=True;
end;

function TFmMainTr.trGroup(sStr:String):Boolean;
begin
  Result:=True;
end;

function TFmMainTr.trGrpNac(sStr:String):Boolean;
begin
  Result:=True;
end;

function TFmMainTr.trGdsNac(sStr:String):Boolean;
begin
  Result:=True;
end;

function TFmMainTr.trGdsCat(sStr:String):Boolean;
begin
  Result:=True;
end;



Function ImportFile(fName:String;Var iOk,iNon:Integer):Boolean;
Var f:TextFile;
    StrWk:String;
    iTypeF:INteger;
begin
  with fmMainTr do
  begin
    result:=True;
    iOk:=0;
    iNon:=0;
    WrMess(' ������ ���������.');

    iTypeF:=1; //����� �� �����

    if iTypeF=1 then
    begin
      try
        assignfile(F,CommonSet.PathImport+fName);
        reset(F);
        while not EOF(F) do
        begin
          ReadLn(F,StrWk);
        end;
        CloseFile(F);
        delay(10);
        MoveFile(CommonSet.PathImport+fName, CommonSet.PathArh+fName);
      except
        Result:=False;
        WrMess('  ------------- ������ ��������� ������. ');
        CloseFile(F);
      end;
    end;
    WrMess(' ��������� ���������.');
  end;
end;

Procedure WrMess(S:String);
Var StrWk:String;
begin
  try
    if fmTrMess.Memo1.Lines.Count > 500 then fmTrMess.Memo1.Clear;
    delay(10);
    if S>'' then StrWk:=FormatDateTime('dd.mm hh:nn:ss:zzz',now)+'  '+S
    else StrWk:=S;
    WriteHistory(StrWk);
    fmTrMess.Memo1.Lines.Add(StrWk);
  except
    try
      WriteHistory(StrWk);
      StrWk:=FormatDateTime('dd.mm hh:nn:ss:zzz',now)+'  ���� ������ �������.';
      WriteHistory(StrWk);
    except
    end;
  end;
end;

procedure TfmMainTr.N3Click(Sender: TObject);
begin
  close;
end;

procedure TfmMainTr.N1Click(Sender: TObject);
begin
  fmTrMess.Show;
end;

procedure TfmMainTr.FormCreate(Sender: TObject);
begin
  CurDir := ExtractFilePath(ParamStr(0));
  ReadIni;
//  TestDir;
  iCountSec:=0;

  Timer1.Enabled:=True;
end;

procedure TfmMainTr.Timer1Timer(Sender: TObject);
begin
  //�� ��������� �� 2-� �������
  Timer1.Enabled:=False;

{  if TestExport then
  begin
    prTransp;  //���� �����-�� �����
  end;}
  inc(iCountSec);
  if iCountSec>=CommonSet.TransDelSec then //������ �� �������� - ������ ������ ��� (��� ���������� �������� ������ 20 ���)
  begin
    try
//      prTranspImp;  //���� �����-�� �����
      if prOpenTr then
      begin
        WrF('����� �� '+FormatDateTime('hh:nn:sss',now));
        prGetMenu;
        prWriteReal;
        prCloseTr;
      end else WrF('����� ��� '+FormatDateTime('hh:nn:sss',now));

      delay(100);
    except
    end;
    iCountSec:=0;

    if CommonSet.CountStart>0 then
    begin
      dec(CommonSet.CountStart);
      if CommonSet.CountStart=0 then Close;
    end;  
  end;
  Timer1.Enabled:=True;
end;

procedure TfmMainTr.RxTrayIcon1Click(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  fmTrMess.Show;
end;

function TFmMainTr.trBrand(sStr:String):Boolean;
begin
  Result:=True;
end;

function stf(StrWk:string;D:Extended):real;
begin
  result := StrToFloatDef(AnsiReplaceStr(StrWk,'.',','),D);
end;

procedure TFmMainTr.prGetMenu;
Var sSifr:String;
begin
  with dmTr do
  with dmC do
  begin
    WrMess('  ��������� ���������.');
    try
    quUpd1.ParamByName('ST').AsInteger:=CommonSet.Station;
    quUpd1.ExecQuery;
    delay(33);
    sSifr:='';
    quTr.Active:=False;
    quTr.ParamByName('ST').AsInteger:=CommonSet.Station;
    quTr.Active:=True;
    quTr.First;
    while not quTr.Eof do
    begin
      sSifr:=sSifr+','+its(quTrSIFR.AsInteger);

      quMenuId.Active:=False;
      quMenuId.ParamByName('SIFR').AsInteger:=quTrSIFR.AsInteger;
      quMenuId.Active:=True;

      quMenuId.First;

      if quMenuId.RecordCount=1 then
      begin
        quMenuId.Edit;
      end else
      begin
        quMenuId.Append;
        quMenuIdSIFR.AsInteger:=quTrSIFR.AsInteger;
      end;

      quMenuIdNAME.AsString:=quTrNAME.AsString;
      quMenuIdPRICE.AsFloat:=quTrPRICE.AsFloat;
      quMenuIdCODE.AsString:=quTrCODE.AsString;
      quMenuIdTREETYPE.AsString:=quTrTREETYPE.AsString;
      quMenuIdLIMITPRICE.AsFloat:=quTrLIMITPRICE.AsFloat;
      quMenuIdCATEG.AsInteger:=quTrCATEG.AsInteger;
      quMenuIdPARENT.AsInteger:=quTrPARENT.AsInteger;
      quMenuIdLINK.AsInteger:=quTrLINK.AsInteger;
      quMenuIdSTREAM.AsInteger:=quTrSTREAM.AsInteger;
      quMenuIdLACK.AsInteger:=quTrLACK.AsInteger;
      quMenuIdDESIGNSIFR.AsInteger:=quTrDESIGNSIFR.AsInteger;
      quMenuIdALTNAME.AsString:=quTrALTNAME.AsString;
      quMenuIdNALOG.AsFloat:=quTrNALOG.AsFloat;
      quMenuIdBARCODE.AsString:=quTrBARCODE.AsString;
      quMenuIdIMAGE.AsInteger:=quTrIMAGE.AsInteger;
      quMenuIdCONSUMMA.AsFloat:=quTrCONSUMMA.AsFloat;
      quMenuIdMINREST.AsInteger:=quTrMINREST.AsInteger;
      quMenuIdPRNREST.AsInteger:=quTrPRNREST.AsInteger;
      quMenuIdCOOKTIME.AsInteger:=quTrCOOKTIME.AsInteger;
      quMenuIdDISPENSER.AsInteger:=quTrDISPENSER.AsInteger;
      quMenuIdDISPKOEF.AsInteger:=quTrDISPKOEF.AsInteger;
      quMenuIdACCESS.AsInteger:=quTrACCESS.AsInteger;
      quMenuIdFLAGS.AsInteger:=quTrFLAGS.AsInteger;
      quMenuIdTARA.AsInteger:=quTrTARA.AsInteger;
      quMenuIdCNTPRICE.AsInteger:=quTrCNTPRICE.AsInteger;
      quMenuIdBACKBGR.AsFloat:=quTrBACKBGR.AsFloat;
      quMenuIdFONTBGR.AsFloat:=quTrFONTBGR.AsFloat;
      quMenuIdIACTIVE.AsInteger:=quTrIACTIVE.AsInteger;
      quMenuIdIEDIT.AsInteger:=quTrIEDIT.AsInteger;
      quMenuIdDATEB.AsInteger:=quTrDATEB.AsInteger;
      quMenuIdDATEE.AsInteger:=quTrDATEE.AsInteger;
      quMenuIdDAYWEEK.AsString:=quTrDAYWEEK.AsString;
      quMenuIdTIMEB.AsDateTime:=quTrTIMEB.AsDateTime;
      quMenuIdTIMEE.AsDateTime:=quTrTIMEE.AsDateTime;
      quMenuIdALLTIME.AsInteger:=quTrALLTIME.AsInteger;
      quMenuId.Post;

      quMenuId.Active:=False;

      //���������



      quTr.Next;
    end;

    quUpd2.ParamByName('ST').AsInteger:=CommonSet.Station;
    quUpd2.ExecQuery;

    delay(33);

    quTr.Active:=False;

    if sSifr>'' then
    begin
      delete(sSifr,1,1); //������� ������ �������
      quTrBarC.Active:=False;
      quTrBarC.SelectSQL.Clear;
      quTrBarC.SelectSQL.Add('SELECT BARCODE,SIFR,QUANT,PRICE');
      quTrBarC.SelectSQL.Add('FROM BARCODE');
      quTrBarC.SelectSQL.Add('where SIFR in ('+sSifr+')');
      quTrBarC.Active:=True;

      quTrBarC.First;
      while not quTrBarC.Eof do
      begin
        quTrBarC.Active:=False;
        quTrBarC.ParamByName('SBAR').AsString:=quTrBarCBARCODE.AsString;
        quTrBarC.Active:=True;

        quTrBarC.First;

        if quTrBarC.RecordCount=1 then
        begin
          quTrBarC.Edit;
        end else
        begin
          quTrBarC.Append;
          quBarCBARCODE.AsString:=quTrBarCBARCODE.AsString;
        end;
        quBarCSIFR.AsInteger:=quTrBarCSIFR.AsInteger;
        quBarCQUANT.AsFloat:=quTrBarCQUANT.AsFloat;
        quBarCPRICE.AsFloat:=quTrBarCPRICE.AsFloat;
        quTrBarC.Post;

        quTrBarC.Active:=False;

        quTrBarC.Next;
      end;
      quTrBarC.Active:=False;
    end;


    WrMess('  ��.');

    except
      WrMess('  ������ ��� ������� ������.');
    end;
  end;

end;

procedure TFmMainTr.prWriteReal;
Var IDH:INteger;
begin
  with dmTr do
  with dmC do
  begin
    try
    WrMess('  �������� ������.');

    quUpd1T.ExecQuery;    //cashsail
    delay(33);

    quTA.Active:=False;
    quTA.Active:=True;
    quTA.First;
    while not quTA.Eof do
    begin
      prTRADDTABLES.ParamByName('ID_PERSONAL').AsInteger:=quTAID_PERSONAL.AsInteger;
      prTRADDTABLES.ParamByName('NUMTABLE').AsString:=quTANUMTABLE.AsString;
      prTRADDTABLES.ParamByName('QUESTS').AsInteger:=quTAQUESTS.AsInteger;
      prTRADDTABLES.ParamByName('TABSUM').AsFloat:=quTATABSUM.AsFloat;
      prTRADDTABLES.ParamByName('BEGTIME').AsDateTime:=quTABEGTIME.AsDateTime;
      prTRADDTABLES.ParamByName('ENDTIME').AsDateTime:=quTAENDTIME.AsDateTime;
      prTRADDTABLES.ParamByName('DISCONT').AsString:=quTADISCONT.AsString;
      prTRADDTABLES.ParamByName('OPERTYPE').AsString:=quTAOPERTYPE.AsString;
      prTRADDTABLES.ParamByName('CHECKNUM').AsInteger:=quTACHECKNUM.AsInteger;
      prTRADDTABLES.ParamByName('SKLAD').AsInteger:=quTASKLAD.AsInteger;
      prTRADDTABLES.ParamByName('SPBAR').AsString:=quTADISCONT1.AsString;
      prTRADDTABLES.ParamByName('STATION').AsInteger:=CommonSet.Station;
      prTRADDTABLES.ParamByName('NUMZ').AsInteger:=quTANUMZ.AsInteger;
      prTRADDTABLES.ParamByName('SALET').AsInteger:=quTASALET.AsInteger;
      prTRADDTABLES.ExecProc;
      delay(33);
      IDH:=prTRADDTABLES.ParamByName('ID_H').AsInteger;
      if IDH>0 then
      begin
        quTAS.Active:=False;
        quTAS.ParamByName('IDH').AsInteger:=quTAID.AsInteger;
        quTAS.Active:=True;
        quTAS.First;
        while not quTAS.Eof do
        begin
          prTRADDSPECT.ParamByName('ID').AsInteger:=quTASID.AsInteger;
          prTRADDSPECT.ParamByName('ID_TAB').AsInteger:=IDH;
          prTRADDSPECT.ParamByName('ID_PERSONAL').AsInteger:=quTASID_PERSONAL.AsInteger;
          prTRADDSPECT.ParamByName('NUMTABLE').AsString:=quTASNUMTABLE.AsString;
          prTRADDSPECT.ParamByName('SIFR').AsInteger:=quTASSIFR.AsInteger;
          prTRADDSPECT.ParamByName('PRICE').AsFloat:=quTASPRICE.AsFloat;
          prTRADDSPECT.ParamByName('QUANTITY').AsFloat:=quTASQUANTITY.AsFloat;
          prTRADDSPECT.ParamByName('DISCOUNTPROC').AsFloat:=quTASDISCOUNTPROC.AsFloat;
          prTRADDSPECT.ParamByName('DISCOUNTSUM').AsFloat:=quTASDISCOUNTSUM.AsFloat;
          prTRADDSPECT.ParamByName('SUMMA').AsFloat:=quTASSUMMA.AsFloat;
          prTRADDSPECT.ParamByName('ISTATUS').AsInteger:=quTASISTATUS.AsInteger;
          prTRADDSPECT.ParamByName('ITYPE').AsInteger:=quTASITYPE.AsInteger;
          prTRADDSPECT.ParamByName('QUANTITY1').AsFloat:=quTASQUANTITY1.AsFloat;
          prTRADDSPECT.ParamByName('INEED').AsInteger:=quTASINEED.AsInteger;
          prTRADDSPECT.ParamByName('IPRINT').AsInteger:=quTASIPRINT.AsInteger;
          prTRADDSPECT.ParamByName('STREAM').AsInteger:=quTASSTREAM.AsInteger;
          prTRADDSPECT.ExecProc;

          quTAS.Next;  delay(33);
        end;

        quTAS.Active:=False;

        delay(33);
        quCS.Active:=False;
        quCS.ParamByName('IDH').AsInteger:=quTAID.AsInteger;
        quCS.Active:=True;
        quCS.First;
        while not quCS.Eof do
        begin
          prTRADDCASHSAIL.ParamByName('CASHNUM').AsInteger:=CommonSet.Station;
          prTRADDCASHSAIL.ParamByName('ZNUM').AsInteger:=quCSZNUM.AsInteger;
          prTRADDCASHSAIL.ParamByName('CHECKNUM').AsInteger:=quCSCHECKNUM.AsInteger;
          prTRADDCASHSAIL.ParamByName('TAB_ID').AsInteger:=IDH;
          prTRADDCASHSAIL.ParamByName('TABSUM').AsFloat:=quCSTABSUM.AsFloat;
          prTRADDCASHSAIL.ParamByName('CLIENTSUM').AsFloat:=quCSCLIENTSUM.AsFloat;
          prTRADDCASHSAIL.ParamByName('CASHERID').AsInteger:=quCSCASHERID.AsInteger;
          prTRADDCASHSAIL.ParamByName('WAITERID').AsInteger:=quCSWAITERID.AsInteger;
          prTRADDCASHSAIL.ParamByName('CHDATE').AsDateTime:=quCSCHDATE.AsDateTime;
          prTRADDCASHSAIL.ParamByName('PAYTYPE').AsInteger:=quCSPAYTYPE.AsInteger;
          prTRADDCASHSAIL.ParamByName('PAYID').AsInteger:=quCSPAYID.AsInteger;
          prTRADDCASHSAIL.ParamByName('PAYBAR').AsString:=quCSPAYBAR.AsString;
          prTRADDCASHSAIL.ParamByName('IACTIVE').AsInteger:=1;
          prTRADDCASHSAIL.ExecProc;

          quCS.Next; delay(33);
        end;

        quCS.Active:=False;
      end;
      quTA.Edit;
      quTAIACTIVE.AsInteger:=3;
      quTA.Post;

      quTA.Next;
    end;
    WrMess('  ��.');
    except
      WrMess('  ������ �������� ������.');
    end;
  end;
end;

function TFmMainTr.prOpenTr:Boolean;
begin
  WrMess('');
  WrMess('��������� ���� ����� '+DBName);
  with dmC do
  begin
    try
      CasherRnDb.Connected:=False;
      CasherRnDb.DBName:=DBName;
      CasherRnDb.Connected:=True;
      Result:=True;
    except
      WrMess('������ �������� ');
      Result:=False;
    end;
  end;
  if Result=True then
  begin
    WrMess('��������� ���� CB '+DBNameCB);
    with dmTr do
    begin
      try
        TRDb.Connected:=False;
        TrDB.DBName:=DBNameCB;
        TrDB.Connected:=True;
      except
        WrMess('������ �������� ');
        Result:=False;
      end;
    end;
  end;
end;

procedure TFmMainTr.prCloseTr;
begin
  WrMess('��������� ����.');
  with dmTr do
  begin
    try
      TRDb.Connected:=False;
    except
    end;
  end;
  with dmC do
  begin
    try
      CasherRnDb.Connected:=False;
    except
    end;
  end;
  WrMess('');
end;


end.
