unit AddBGU;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, FIBDataSet, pFIBDataSet,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  ExtCtrls, StdCtrls, Menus, cxLookAndFeelPainters, cxButtons, cxTextEdit,
  cxContainer, cxMaskEdit, cxDropDownEdit, cxCalc;

type
  TfmAddBGU = class(TForm)
    Label1: TLabel;
    Panel1: TPanel;
    ViewABGU: TcxGridDBTableView;
    LevABGU: TcxGridLevel;
    GrABGU: TcxGrid;
    taBGUGr: TpFIBDataSet;
    dstaBGUGr: TDataSource;
    taBGUGrID: TFIBIntegerField;
    taBGUGrNAMEGR: TFIBStringField;
    ViewABGUID: TcxGridDBColumn;
    ViewABGUNAMEGR: TcxGridDBColumn;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    cxCalcEdit5: TcxCalcEdit;
    cxCalcEdit4: TcxCalcEdit;
    cxCalcEdit3: TcxCalcEdit;
    cxCalcEdit2: TcxCalcEdit;
    cxTextEdit1: TcxTextEdit;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    quMaxBGU: TpFIBDataSet;
    quMaxBGUMAXID: TFIBIntegerField;
    procedure cxButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAddBGU: TfmAddBGU;

implementation

uses dmOffice;

{$R *.dfm}

procedure TfmAddBGU.cxButton1Click(Sender: TObject);
var iMax:INteger;
begin
  //��������
  quMaxBGU.Active:=False;
  quMaxBGU.Active:=True;
  iMax:=quMaxBGUMAXID.AsInteger+1;
  quMaxBGU.Active:=False;
  with dmO do
  begin
    if taBGU.Active=False then taBGU.Active:=True;
    taBGU.Append;
    taBGUID.AsInteger:=iMax;
    taBGUPARENT.AsInteger:=taBGUGrID.AsInteger;
    taBGUNAMEBGU.AsString:=cxTextEdit1.Text;
    taBGUBB.AsFloat:=cxCalcEdit2.Value;
    taBGUGG.AsFloat:=cxCalcEdit3.Value;
    taBGUU1.AsFloat:=cxCalcEdit4.Value;
    taBGUEE.AsFloat:=cxCalcEdit5.Value;
    taBGU.Post;

    taBGU.Refresh;
  end;

  showmessage(cxTextEdit1.Text+'  � ���������� ��������.');

end;

end.
