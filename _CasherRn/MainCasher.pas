unit MainCasher;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxMemo, StdCtrls, cxControls, cxContainer, cxEdit, cxTextEdit,
  cxMaskEdit, cxDropDownEdit, cxCalc, cxCurrencyEdit, cxStyles,
  cxCustomData, cxGraphics, cxFilter, cxData, DB, cxDBData, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ExtCtrls, RXClock, ClipView,
  cxLookAndFeelPainters, cxButtons, cxDataStorage, Menus;

type
  TfmMainCasher = class(TForm)
    cxTextEdit1: TcxTextEdit;
    Label1: TLabel;
    Label2: TLabel;
    cxTextEdit2: TcxTextEdit;
    cxMemo1: TcxMemo;
    Label3: TLabel;
    Label4: TLabel;
    cxCalcEdit1: TcxCalcEdit;
    cxCurrencyEdit1: TcxCurrencyEdit;
    Label5: TLabel;
    Label6: TLabel;
    cxCurrencyEdit2: TcxCurrencyEdit;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    Label7: TLabel;
    cxCurrencyEdit3: TcxCurrencyEdit;
    RxClock1: TRxClock;
    Panel1: TPanel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    cxTextEdit3: TcxTextEdit;
    Label12: TLabel;
    Panel2: TPanel;
    Label13: TLabel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    cxButton5: TcxButton;
    cxButton6: TcxButton;
    cxButton7: TcxButton;
    cxButton8: TcxButton;
    cxButton9: TcxButton;
    cxButton10: TcxButton;
    cxButton11: TcxButton;
    cxButton12: TcxButton;
    cxButton13: TcxButton;
    cxButton14: TcxButton;
    cxButton15: TcxButton;
    cxButton16: TcxButton;
    cxButton17: TcxButton;
    cxButton18: TcxButton;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmMainCasher: TfmMainCasher;

implementation

uses Un1, Dm;

{$R *.dfm}

procedure TfmMainCasher.FormCreate(Sender: TObject);
begin
  CurDir := ExtractFilePath(ParamStr(0));
  ReadIni;
end;

end.
