unit SelPost;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons, ExtCtrls,
  cxControls, cxContainer, cxEdit, cxTextEdit, cxMaskEdit, cxButtonEdit;

type
  TfmSelPost = class(TForm)
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Label4: TLabel;
    Label2: TLabel;
    cxButtonEdit1: TcxButtonEdit;
    cxButtonEdit2: TcxButtonEdit;
    OpenDialog1: TOpenDialog;
    procedure FormCreate(Sender: TObject);
    procedure cxButtonEdit2PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmSelPost: TfmSelPost;

implementation

uses Clients, Un1, dmOffice;

{$R *.dfm}

procedure TfmSelPost.FormCreate(Sender: TObject);
begin
  cxButtonEdit1.Text:='';
  cxButtonEdit1.Tag:=0;
  cxButtonEdit2.Text:='';
  cxButtonEdit2.Tag:=0;
end;

procedure TfmSelPost.cxButtonEdit2PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  if OpenDialog1.Execute then cxButtonEdit2.Text:=OpenDialog1.fileName;
end;

procedure TfmSelPost.cxButtonEdit1PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  if dmO.taClients.Active=False then dmO.taClients.Active:=True;
  if fmClients.Visible then fmClients.Close;
  bSelCli:=True;
  if cxButtonEdit1.Tag>0 then dmO.taClients.Locate('ID',cxButtonEdit1.Tag,[]);
  fmClients.ShowModal;
  if fmClients.ModalResult=mrOk then
  begin
    cxButtonEdit1.Tag:=dmO.taClientsID.AsInteger;
    cxButtonEdit1.Text:=dmO.taClientsNAMECL.AsString;
  end else
  begin
    cxButtonEdit1.Tag:=0;
    cxButtonEdit1.Text:='';
  end;
  bSelCli:=False;
end;

end.
