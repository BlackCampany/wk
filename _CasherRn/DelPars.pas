unit DelPars;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, Placemnt, cxStyles, cxCustomData, cxGraphics,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, ActnList,
  XPStyleActnCtrls, ActnMan, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxControls,
  cxGridCustomView, cxGrid, SpeedBar, ExtCtrls;

type
  TfmDelPar = class(TForm)
    StatusBar1: TStatusBar;
    FormPlacement1: TFormPlacement;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    GridDelPar: TcxGrid;
    ViewDelPar: TcxGridDBTableView;
    LevelDelPar: TcxGridLevel;
    am3: TActionManager;
    acAdd: TAction;
    acEdit: TAction;
    acDel: TAction;
    acExit: TAction;
    Timer1: TTimer;
    ViewDelParID: TcxGridDBColumn;
    ViewDelParNAMEDEL: TcxGridDBColumn;
    ViewDelParBTYPE: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure acAddExecute(Sender: TObject);
    procedure acEditExecute(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmDelPar: TfmDelPar;
  bClear3: Boolean;

implementation

uses Un1, dmRnEdit, AddCateg;

{$R *.dfm}

procedure TfmDelPar.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  GridDelPar.Align:=AlClient;
  ViewDelPar.RestoreFromIniFile(CurDir+GridIni);
  delay(10);
end;

procedure TfmDelPar.acExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmDelPar.acAddExecute(Sender: TObject);
Var Id:Integer;
begin
  //���������� ���������
  if not CanDo('prAddDelPar') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  with dmC do
  begin
    fmAddCateg:=TFmAddCateg.create(Application);
    fmAddCateg.Caption:='���������� ������� ��������.';
    fmAddCateg.cxTextEdit1.Text:='';
    fmAddCateg.ShowModal;
    if fmAddCateg.ModalResult=mrOk then
    begin
      try
        Id:=1;
        if taDelPar.RecordCount>0 then
        begin
          taDelPar.Last;
          Id:=taDelParID.AsInteger+1;
        end;

        taDelPar.Append;
        taDelParID.AsInteger:=Id;
        taDelParNAMEDEL.AsString:=Copy(fmAddCateg.cxTextEdit1.Text,1,100);
        taDelParBTYPE.AsInteger:=0;
        taDelPar.Post;

      except
      end;
    end;
    fmAddCateg.Release;
  end;
end;

procedure TfmDelPar.acEditExecute(Sender: TObject);
begin
  //�������������� ���������
  if not CanDo('prEditDelPar') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  with dmC do
  begin
    if taDelPar.RecordCount>0 then
    begin
      fmAddCateg:=TFmAddCateg.create(Application);
      fmAddCateg.Caption:='�������������� ������� ��������.';
      fmAddCateg.cxTextEdit1.Text:=taDelParNAMEDEL.AsString;
      fmAddCateg.ShowModal;
      if fmAddCateg.ModalResult=mrOk then
      begin
        try
          taDelPar.Edit;
          taDelParNAMEDEL.AsString:=Copy(fmAddCateg.cxTextEdit1.Text,1,100);
          taDelPar.Post;
        except
        end;
      end;
      fmAddCateg.Release;
    end;
  end;
end;

procedure TfmDelPar.Timer1Timer(Sender: TObject);
begin
  if bClear3=True then begin StatusBar1.Panels[0].Text:=''; bClear3:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClear3:=True;
end;

procedure TfmDelPar.acDelExecute(Sender: TObject);
begin
  //������� ���������
  if not CanDo('prDelDelPar') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  with dmC do
  begin
    if taDelPar.RecordCount>0 then
    begin
      if MessageDlg('�� ������������� ������ ������� ������� �������� - '+taDelParNAMEDEL.AsString+' ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        try
          taDelPar.Delete;
        except
        end;
      end;
    end;
  end;
end;

procedure TfmDelPar.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewDelPar.StoreToIniFile(CurDir+GridIni,False);
end;

end.
