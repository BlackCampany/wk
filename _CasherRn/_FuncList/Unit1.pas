unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, dxmdaset, Menus, cxLookAndFeelPainters,
  cxButtons, cxTextEdit, cxContainer, cxLabel, FIBDatabase, pFIBDatabase,
  FIBDataSet, pFIBDataSet;

type
  TfmAddFuncList = class(TForm)
    Button1: TButton;
    Label1: TLabel;
    taF: TdxMemData;
    ViewF: TcxGridDBTableView;
    LevelF: TcxGridLevel;
    GridF: TcxGrid;
    dstaF: TDataSource;
    taFaAbr: TStringField;
    taFComment: TStringField;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    ViewFRecId: TcxGridDBColumn;
    ViewFaAbr: TcxGridDBColumn;
    ViewFComment: TcxGridDBColumn;
    cxLabel1: TcxLabel;
    cxTextEdit1: TcxTextEdit;
    OfficeRnDb: TpFIBDatabase;
    trSelect: TpFIBTransaction;
    trUpdate: TpFIBTransaction;
    taFuncList: TpFIBDataSet;
    taFuncListNAME: TFIBStringField;
    taFuncListCOMMENT: TFIBStringField;
    taFuncListLOG: TFIBSmallIntField;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

procedure Delay(MSecs: Longint);

var
  fmAddFuncList: TfmAddFuncList;
  CurDir:String;

implementation

{$R *.dfm}

procedure Delay(MSecs: Longint);
var
  FirstTickCount, Now: Longint;
begin
  FirstTickCount := GetTickCount;
  repeat
    Application.ProcessMessages;
    { allowing access to other controls, etc. }
    Now := GetTickCount;
  until (Now - FirstTickCount >= MSecs) or (Now < FirstTickCount);
end;


procedure TfmAddFuncList.Button1Click(Sender: TObject);
var F0001,F0002: TextFile;
    FileN:String;
    StrWk,Strwk1:String;
begin
  try
    Application.ProcessMessages;
    FileN:=CurDir+'FuncList.csv';
    AssignFile(F0001, FileN);
    Reset(F0001);

    FileN:=CurDir+'FuncList1.csv';
    AssignFile(F0002, FileN);
    Rewrite(F0002);
//    delay(1000);
    while not Eof(F0001) do
    begin
      readln(F0001,StrWk);
      if StrWk>'' then
      begin
        delete(StrWk,1,Pos('''',StrWk));
        StrWk1:=Copy(StrWk,1,Pos('''',StrWk)-1);
        WriteLn(F0002,Strwk1);
      end;
    end;
  finally
    CloseFile(F0001);
    CloseFile(F0002);
  end;
  Label1.Caption:='Ok';
end;

procedure TfmAddFuncList.FormCreate(Sender: TObject);
begin
  CurDir := ExtractFilePath(ParamStr(0));
end;

procedure TfmAddFuncList.cxButton1Click(Sender: TObject);
var F0001: TextFile;
    FileN:String;
    StrWk,Strwk1:String;
begin
  Label1.Caption:='����� ...';
  try
    Application.ProcessMessages;
    FileN:=CurDir+'FuncList.csv';
    AssignFile(F0001, FileN);
    Reset(F0001);

    ViewF.BeginUpdate;
    taF.Active:=False;
    taF.Active:=True;

    while not Eof(F0001) do
    begin
      readln(F0001,StrWk);
      if StrWk>'' then
      begin
        StrWk1:=Copy(StrWk,1,Pos(';',StrWk)-1);
        delete(StrWk,1,Pos(';',StrWk));
        taF.Append;
        taFaAbr.AsString:=Strwk1;
        taFComment.AsString:=StrWk;
        taF.Post;
      end;
    end;
  finally
    CloseFile(F0001);
    ViewF.EndUpdate;
  end;
  Label1.Caption:='Ok';
end;

procedure TfmAddFuncList.cxButton2Click(Sender: TObject);
begin
  OfficeRnDb.Connected:=False;
  OfficeRnDb.DBName:=cxTextEdit1.Text;
  try
    OfficeRnDb.Open;

    if OfficeRnDb.Connected then
    begin
      if taF.Active then
      begin
        taFuncList.Active:=False;
        taFuncList.Active:=True;

        taF.First;
        while not taF.Eof do
        begin
          if taFuncList.Locate('NAME',taFaAbr.AsString,[]) then
          begin
            taFuncList.Edit;
            taFuncListCOMMENT.AsString:=taFComment.AsString;
            taFuncList.Post;
          end else
          begin
            taFuncList.Append;
            taFuncListNAME.AsString:=taFaAbr.AsString;
            taFuncListCOMMENT.AsString:=taFComment.AsString;
            taFuncListLOG.AsInteger:=0;
            taFuncList.Post;
            Delay(100);
          end;
          taF.Next;
        end;
        taFuncList.Active:=False;
      end;
    end;
    OfficeRnDb.Connected:=False;
  except
  end;
end;

end.
