object fmAddFuncList: TfmAddFuncList
  Left = 427
  Top = 152
  Width = 728
  Height = 451
  Caption = 'fmAddFuncList'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 20
    Top = 8
    Width = 9
    Height = 13
    Caption = '...'
  end
  object Button1: TButton
    Left = 580
    Top = 316
    Width = 129
    Height = 25
    Caption = #1057#1095#1080#1090#1072#1090#1100' '#1089' '#1092#1072#1081#1083#1072
    TabOrder = 0
    Visible = False
    OnClick = Button1Click
  end
  object GridF: TcxGrid
    Left = 8
    Top = 28
    Width = 525
    Height = 321
    TabOrder = 1
    LookAndFeel.Kind = lfOffice11
    object ViewF: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dstaF
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      object ViewFRecId: TcxGridDBColumn
        DataBinding.FieldName = 'RecId'
        Visible = False
      end
      object ViewFaAbr: TcxGridDBColumn
        Caption = #1040#1073#1088#1077#1074#1080#1072#1090#1091#1088#1072
        DataBinding.FieldName = 'aAbr'
        Width = 155
      end
      object ViewFComment: TcxGridDBColumn
        Caption = #1057#1086#1076#1077#1088#1078#1072#1085#1080#1077
        DataBinding.FieldName = 'Comment'
        Width = 308
      end
    end
    object LevelF: TcxGridLevel
      GridView = ViewF
    end
  end
  object cxButton1: TcxButton
    Left = 552
    Top = 36
    Width = 141
    Height = 25
    Caption = #1055#1088#1080#1085#1103#1090#1100' '#1080#1079' '#1092#1072#1081#1083#1072
    TabOrder = 2
    OnClick = cxButton1Click
    LookAndFeel.Kind = lfOffice11
  end
  object cxButton2: TcxButton
    Left = 552
    Top = 76
    Width = 141
    Height = 25
    Caption = #1047#1072#1087#1080#1089#1072#1090#1100' '#1074' '#1073#1072#1079#1091
    TabOrder = 3
    OnClick = cxButton2Click
    LookAndFeel.Kind = lfOffice11
  end
  object cxLabel1: TcxLabel
    Left = 20
    Top = 372
    Caption = #1055#1091#1090#1100' '#1076#1086' '#1073#1072#1079#1099
  end
  object cxTextEdit1: TcxTextEdit
    Left = 104
    Top = 370
    TabOrder = 5
    Text = 'localhost:C:\_CasherRn\DB2\OFFICERN.GDB'
    Width = 425
  end
  object taF: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 44
    Top = 76
    object taFaAbr: TStringField
      FieldName = 'aAbr'
      Size = 30
    end
    object taFComment: TStringField
      FieldName = 'Comment'
      Size = 100
    end
  end
  object dstaF: TDataSource
    DataSet = taF
    Left = 44
    Top = 128
  end
  object OfficeRnDb: TpFIBDatabase
    DBName = 'localhost:C:\_CasherRn\DB2\CASHERRN.GDB'
    DBParams.Strings = (
      'user_name=SYSDBA'
      'lc_ctype=WIN1251'
      'password=masterkey'
      'sql_role_name=SYSDBA')
    DefaultTransaction = trSelect
    DefaultUpdateTransaction = trUpdate
    SQLDialect = 3
    Timeout = 0
    SynchronizeTime = False
    DesignDBOptions = []
    AliasName = 'CasherRnDb'
    WaitForRestoreConnect = 20
    Left = 100
    Top = 160
  end
  object trSelect: TpFIBTransaction
    DefaultDatabase = OfficeRnDb
    TimeoutAction = TARollback
    Left = 100
    Top = 212
  end
  object trUpdate: TpFIBTransaction
    DefaultDatabase = OfficeRnDb
    TimeoutAction = TARollback
    Left = 100
    Top = 268
  end
  object taFuncList: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE RFUNCLIST'
      'SET '
      '    COMMENT = :COMMENT,'
      '    LOG = :LOG'
      'WHERE'
      '    NAME = :OLD_NAME'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    RFUNCLIST'
      'WHERE'
      '        NAME = :OLD_NAME'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO RFUNCLIST('
      '    NAME,'
      '    COMMENT,'
      '    LOG'
      ')'
      'VALUES('
      '    :NAME,'
      '    :COMMENT,'
      '    :LOG'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    NAME,'
      '    COMMENT,'
      '    LOG'
      'FROM'
      '    RFUNCLIST '
      ''
      ' WHERE '
      '        RFUNCLIST.NAME = :OLD_NAME'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    NAME,'
      '    COMMENT,'
      '    LOG'
      'FROM'
      '    RFUNCLIST ')
    Transaction = trSelect
    Database = OfficeRnDb
    UpdateTransaction = trUpdate
    AutoCommit = True
    Left = 176
    Top = 160
    object taFuncListNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 50
      EmptyStrToNull = True
    end
    object taFuncListCOMMENT: TFIBStringField
      FieldName = 'COMMENT'
      Size = 100
      EmptyStrToNull = True
    end
    object taFuncListLOG: TFIBSmallIntField
      FieldName = 'LOG'
    end
  end
end
