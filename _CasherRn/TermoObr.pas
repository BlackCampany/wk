unit TermoObr;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxCheckBox, cxImageComboBox, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxControls,
  cxGridCustomView, cxGrid, Placemnt, StdCtrls, cxButtons, ExtCtrls,
  SpeedBar;

type
  TfmTermoObr = class(TForm)
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem5: TSpeedItem;
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    FormPlacement1: TFormPlacement;
    GridTermoObr: TcxGrid;
    ViewTermoObr: TcxGridDBTableView;
    LevelTermoObr: TcxGridLevel;
    ViewTermoObrID: TcxGridDBColumn;
    ViewTermoObrNAMEOBR: TcxGridDBColumn;
    ViewTermoObrBB: TcxGridDBColumn;
    ViewTermoObrGG: TcxGridDBColumn;
    ViewTermoObrU1: TcxGridDBColumn;
    ViewTermoObrU2: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure SpeedItem4Click(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
    procedure SpeedItem5Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmTermoObr: TfmTermoObr;

implementation

uses Un1, dmOffice, AddTermoObr;

{$R *.dfm}

procedure TfmTermoObr.FormCreate(Sender: TObject);
begin
  GridTermoObr.Align:=AlClient;
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  ViewTermoObr.RestoreFromIniFile(CurDir+GridIni);
end;

procedure TfmTermoObr.SpeedItem4Click(Sender: TObject);
begin
  Close;
end;

procedure TfmTermoObr.SpeedItem1Click(Sender: TObject);
Var iMax:Integer;
begin
  //��������
//  if not CanDo('prAddTermoObr') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  with dmO do
  begin
    fmAddTermoObr:=tfmAddTermoObr.Create(Application);
    fmAddTermoObr.Caption:='����������.';
    fmAddTermoObr.cxTextEdit1.Text:='';
    fmAddTermoObr.cxCalcEdit1.Value:=0;
    fmAddTermoObr.cxCalcEdit2.Value:=0;
    fmAddTermoObr.cxCalcEdit3.Value:=0;
    fmAddTermoObr.cxCalcEdit4.Value:=0;
    fmAddTermoObr.ShowModal;
    if fmAddTermoObr.ModalResult=mrOk then
    begin
      with dmO do
      begin
        iMax:=1;
        quMaxTermo.Active:=False;
        quMaxTermo.Active:=True;
        if quMaxTermo.RecordCount>0 then iMax:=quMaxTermoMAXID.AsInteger;
        quMaxTermo.Active:=False;

        taTermoObr.Append;
        taTermoObrID.AsInteger:=iMax+1;
        taTermoObrNAMEOBR.AsString:=fmAddTermoObr.cxTextEdit1.Text;
        taTermoObrBB.AsFloat:=fmAddTermoObr.cxCalcEdit1.Value;
        taTermoObrGG.AsFloat:=fmAddTermoObr.cxCalcEdit2.Value;
        taTermoObrU1.AsFloat:=fmAddTermoObr.cxCalcEdit3.Value;
        taTermoObrU2.AsFloat:=fmAddTermoObr.cxCalcEdit4.Value;
        taTermoObr.Post;

        GridTermoObr.SetFocus;
      end;
    end;
    fmAddTermoObr.Release;
  end;
end;

procedure TfmTermoObr.SpeedItem2Click(Sender: TObject);
begin
  with dmO do
  begin
    if taTermoObr.RecordCount>0 then
    begin
      fmAddTermoObr:=tfmAddTermoObr.Create(Application);
      fmAddTermoObr.Caption:='��������������.';
      fmAddTermoObr.cxTextEdit1.Text:=taTermoObrNAMEOBR.AsString;
      fmAddTermoObr.cxCalcEdit1.Value:=taTermoObrBB.AsFloat;
      fmAddTermoObr.cxCalcEdit2.Value:=taTermoObrGG.AsFloat;
      fmAddTermoObr.cxCalcEdit3.Value:=taTermoObrU1.AsFloat;
      fmAddTermoObr.cxCalcEdit4.Value:=taTermoObrU2.AsFloat;
      fmAddTermoObr.ShowModal;
      if fmAddTermoObr.ModalResult=mrOk then
      begin
        with dmO do
        begin
          taTermoObr.Edit;
          taTermoObrNAMEOBR.AsString:=fmAddTermoObr.cxTextEdit1.Text;
          taTermoObrBB.AsFloat:=fmAddTermoObr.cxCalcEdit1.Value;
          taTermoObrGG.AsFloat:=fmAddTermoObr.cxCalcEdit2.Value;
          taTermoObrU1.AsFloat:=fmAddTermoObr.cxCalcEdit3.Value;
          taTermoObrU2.AsFloat:=fmAddTermoObr.cxCalcEdit4.Value;
          taTermoObr.Post;

          GridTermoObr.SetFocus;
        end;
      end;
      fmAddTermoObr.Release;
    end;
  end;
end;

procedure TfmTermoObr.SpeedItem3Click(Sender: TObject);
Var bUse:Boolean;
begin
  //�������
  with dmO do
  begin
    if taTermoObr.RecordCount>0 then
    begin
      if MessageDlg('������� "'+taTermoObrNAMEOBR.AsString+'"', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        //��������� �� �������������
        bUse:=False;

        quUseTermoO.Active:=False;
        quUseTermoO.SQLs.SelectSQL.Clear;
        quUseTermoO.SQLs.SelectSQL.Add('SELECT first 3 IDCARD');
        quUseTermoO.SQLs.SelectSQL.Add('FROM OF_CARDSTSPEC where IOBR='+IntToStr(taTermoObrID.AsInteger));
        quUseTermoO.Active:=True;
        if quUseTermoO.RecordCount>0 then bUse:=True;
        quUseTermoO.Active:=False;

        if not bUse then
        begin
          quUseTermoO.SQLs.SelectSQL.Clear;
          quUseTermoO.SQLs.SelectSQL.Add('SELECT first 3 IDCARD');
          quUseTermoO.SQLs.SelectSQL.Add('FROM OF_CARDST where IOBR='+IntToStr(taTermoObrID.AsInteger));
          quUseTermoO.Active:=True;
          if quUseTermoO.RecordCount>0 then bUse:=True;
          quUseTermoO.Active:=False;
        end;

        if not bUse then  taTermoObr.Delete;
      end;
    end;
  end;
end;

procedure TfmTermoObr.SpeedItem5Click(Sender: TObject);
begin
  prNExportExel5(ViewTermoObr);
end;

end.
