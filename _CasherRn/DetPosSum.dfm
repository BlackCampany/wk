object fmDetPosSum: TfmDetPosSum
  Left = 205
  Top = 263
  Width = 1026
  Height = 532
  Caption = #1044#1077#1090#1072#1083#1080#1079#1072#1094#1080#1103' '#1076#1086#1082#1091#1084#1077#1085#1090#1086#1074' '#1087#1086' '#1087#1086#1079#1080#1094#1080#1080
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object GridPosDetD: TcxGrid
    Left = 12
    Top = 12
    Width = 1001
    Height = 405
    TabOrder = 0
    LookAndFeel.Kind = lfOffice11
    object ViewPosDetDoc: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dsteDocLn
      DataController.Summary.DefaultGroupSummaryItems = <
        item
          Format = '0.000'
          Kind = skSum
          Position = spFooter
          FieldName = 'QuantIn'
          Column = ViewPosDetDocQuantIn
        end
        item
          Format = '0.000'
          Kind = skSum
          Position = spFooter
          FieldName = 'QuantOut'
          Column = ViewPosDetDocQuantOut
        end
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'SumIn'
          Column = ViewPosDetDocSumIn
        end
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'SumIn0'
          Column = ViewPosDetDocSumIn0
        end
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'SumOut'
          Column = ViewPosDetDocSumOut
        end
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'SumOut0'
          Column = ViewPosDetDocSumOut0
        end>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'QuantIn'
          Column = ViewPosDetDocQuantIn
        end
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'QuantOut'
          Column = ViewPosDetDocQuantOut
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SumIn'
          Column = ViewPosDetDocSumIn
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SumIn0'
          Column = ViewPosDetDocSumIn0
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SumOut'
          Column = ViewPosDetDocSumOut
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SumOut0'
          Column = ViewPosDetDocSumOut0
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.Footer = True
      OptionsView.GroupFooters = gfAlwaysVisible
      object ViewPosDetDocRecId: TcxGridDBColumn
        DataBinding.FieldName = 'RecId'
        Visible = False
      end
      object ViewPosDetDocITypeD: TcxGridDBColumn
        Caption = #1058#1080#1087' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
        DataBinding.FieldName = 'ITypeD'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Items = <
          item
            Description = #1055#1088#1080#1093#1086#1076#1085#1099#1077' '#1076#1086#1082#1091#1084#1077#1085#1090#1099
            ImageIndex = 0
            Value = 1
          end
          item
            Description = #1056#1077#1072#1083#1080#1079#1072#1094#1080#1103' '#1087#1086' '#1082#1072#1089#1089#1077
            Value = 2
          end
          item
            Description = #1048#1085#1074#1077#1085#1090#1072#1088#1080#1079#1072#1094#1080#1103
            Value = 3
          end
          item
            Description = #1042#1085'.'#1087#1077#1088#1077#1084#1077#1097#1077#1085#1080#1077
            Value = 4
          end
          item
            Description = #1040#1082#1090' '#1087#1077#1088#1077#1088#1072#1073#1086#1090#1082#1080
            Value = 5
          end
          item
            Description = #1050#1086#1084#1087#1083#1077#1082#1090#1072#1094#1080#1103
            Value = 6
          end
          item
            Description = #1042#1086#1079#1074#1088#1072#1090
            Value = 7
          end
          item
            Description = #1056#1077#1072#1083#1080#1079#1072#1094#1080#1103' '#1085#1072' '#1089#1090#1086#1088#1086#1085#1091
            Value = 8
          end>
      end
      object ViewPosDetDocIdDoc: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
        DataBinding.FieldName = 'IdDoc'
      end
      object ViewPosDetDocNumDoc: TcxGridDBColumn
        Caption = #1053#1086#1084#1077#1088' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
        DataBinding.FieldName = 'NumDoc'
      end
      object ViewPosDetDocIDateD: TcxGridDBColumn
        Caption = #1044#1072#1090#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1072' ('#1094#1077#1083#1086#1077')'
        DataBinding.FieldName = 'IDateD'
      end
      object ViewPosDetDocDateDoc: TcxGridDBColumn
        Caption = #1044#1072#1090#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
        DataBinding.FieldName = 'DateDoc'
      end
      object ViewPosDetDocQuantIn: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086' '#1087#1088#1080#1093#1086#1076#1072
        DataBinding.FieldName = 'QuantIn'
        Styles.Content = dmO.cxStyle25
      end
      object ViewPosDetDocQuantOut: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086' '#1088#1072#1089#1093#1086#1076#1072
        DataBinding.FieldName = 'QuantOut'
        Styles.Content = dmO.cxStyle25
      end
      object ViewPosDetDocPriceIn0: TcxGridDBColumn
        Caption = #1062#1077#1085#1072' '#1087#1088#1080#1093#1086#1076#1072' '#1073#1077#1079' '#1053#1044#1057
        DataBinding.FieldName = 'PriceIn0'
      end
      object ViewPosDetDocSumIn0: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1087#1088#1080#1093#1086#1076#1072' '#1073#1077#1079' '#1053#1044#1057
        DataBinding.FieldName = 'SumIn0'
      end
      object ViewPosDetDocPriceIn: TcxGridDBColumn
        Caption = #1062#1077#1085#1072' '#1087#1088#1080#1093#1086#1076#1072' '#1089' '#1053#1044#1057
        DataBinding.FieldName = 'PriceIn'
      end
      object ViewPosDetDocSumIn: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1087#1088#1080#1093#1086#1076#1072' '#1089' '#1053#1044#1057
        DataBinding.FieldName = 'SumIn'
      end
      object ViewPosDetDocPriceOut0: TcxGridDBColumn
        Caption = #1062#1077#1085#1072' '#1088#1072#1089#1093#1086#1076#1072' '#1073#1077#1079' '#1053#1044#1057
        DataBinding.FieldName = 'PriceOut0'
      end
      object ViewPosDetDocSumOut0: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1088#1072#1089#1093#1086#1076#1072' '#1073#1077#1079' '#1053#1044#1057
        DataBinding.FieldName = 'SumOut0'
      end
      object ViewPosDetDocPriceOut: TcxGridDBColumn
        Caption = #1062#1077#1085#1072' '#1088#1072#1089#1093#1086#1076#1072' c '#1053#1044#1057
        DataBinding.FieldName = 'PriceOut'
      end
      object ViewPosDetDocSumOut: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1088#1072#1089#1093#1086#1076#1072' '#1089' '#1053#1044#1057
        DataBinding.FieldName = 'SumOut'
      end
    end
    object LevelPosDetD: TcxGridLevel
      GridView = ViewPosDetDoc
    end
  end
  object Memo1: TcxMemo
    Left = 0
    Top = 428
    Align = alBottom
    Lines.Strings = (
      'Memo1')
    TabOrder = 1
    Height = 70
    Width = 1018
  end
  object teDocLn: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 136
    Top = 88
    object teDocLnITypeD: TIntegerField
      FieldName = 'ITypeD'
    end
    object teDocLnIdDoc: TIntegerField
      FieldName = 'IdDoc'
    end
    object teDocLnNumDoc: TStringField
      FieldName = 'NumDoc'
    end
    object teDocLnIDateD: TIntegerField
      FieldName = 'IDateD'
    end
    object teDocLnDateDoc: TDateField
      FieldName = 'DateDoc'
    end
    object teDocLnQuantIn: TFloatField
      FieldName = 'QuantIn'
      DisplayFormat = '0.000'
    end
    object teDocLnQuantOut: TFloatField
      FieldName = 'QuantOut'
      DisplayFormat = '0.000'
    end
    object teDocLnPriceIn0: TFloatField
      FieldName = 'PriceIn0'
      DisplayFormat = '0.00'
    end
    object teDocLnSumIn0: TFloatField
      FieldName = 'SumIn0'
      DisplayFormat = '0.00'
    end
    object teDocLnPriceIn: TFloatField
      FieldName = 'PriceIn'
      DisplayFormat = '0.00'
    end
    object teDocLnSumIn: TFloatField
      FieldName = 'SumIn'
      DisplayFormat = '0.00'
    end
    object teDocLnPriceOut: TFloatField
      FieldName = 'PriceOut'
      DisplayFormat = '0.00'
    end
    object teDocLnSumOut: TFloatField
      FieldName = 'SumOut'
      DisplayFormat = '0.00'
    end
    object teDocLnPriceOut0: TFloatField
      FieldName = 'PriceOut0'
      DisplayFormat = '0.00'
    end
    object teDocLnSumOut0: TFloatField
      FieldName = 'SumOut0'
      DisplayFormat = '0.00'
    end
  end
  object dsteDocLn: TDataSource
    DataSet = teDocLn
    Left = 136
    Top = 152
  end
end
