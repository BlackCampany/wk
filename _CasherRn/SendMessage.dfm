object fmSendMessage: TfmSendMessage
  Left = 355
  Top = 198
  BorderStyle = bsDialog
  Caption = #1054#1090#1087#1088#1072#1074#1080#1090#1100' '#1089#1086#1086#1073#1097#1077#1085#1080#1077
  ClientHeight = 417
  ClientWidth = 332
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 340
    Width = 332
    Height = 77
    Align = alBottom
    BevelInner = bvLowered
    Color = 12621940
    TabOrder = 0
    object Button3: TcxButton
      Left = 219
      Top = 8
      Width = 90
      Height = 60
      Caption = #1054#1090#1084#1077#1085#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ModalResult = 2
      ParentFont = False
      TabOrder = 0
      TabStop = False
      Colors.Default = 14864576
      Colors.Normal = 9989189
      Colors.Pressed = 13875616
      LookAndFeel.Kind = lfFlat
    end
    object cxButton1: TcxButton
      Left = 27
      Top = 8
      Width = 90
      Height = 60
      Caption = #1055#1086#1089#1083#1072#1090#1100
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ModalResult = 1
      ParentFont = False
      TabOrder = 1
      TabStop = False
      Colors.Default = 14864576
      Colors.Normal = 9989189
      Colors.Pressed = 13875616
      LookAndFeel.Kind = lfFlat
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 332
    Height = 49
    Align = alTop
    BevelInner = bvLowered
    Color = 9989189
    TabOrder = 1
    object Label1: TLabel
      Left = 16
      Top = 8
      Width = 32
      Height = 13
      Caption = 'Label1'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Label2: TLabel
      Left = 16
      Top = 28
      Width = 32
      Height = 13
      Caption = 'Label2'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
  end
  object GridMessage: TcxGrid
    Left = 12
    Top = 56
    Width = 305
    Height = 232
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    LookAndFeel.Kind = lfOffice11
    object ViewMessage: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dmC.dstaMes
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      object ViewMessageMESSAG: TcxGridDBColumn
        Caption = #1057#1086#1086#1073#1097#1077#1085#1080#1077
        DataBinding.FieldName = 'MESSAG'
        Styles.Content = dmC.cxStyle5
        Width = 290
      end
    end
    object LevelMessage: TcxGridLevel
      GridView = ViewMessage
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 288
    Width = 332
    Height = 52
    Align = alBottom
    BevelInner = bvLowered
    Color = 9989189
    TabOrder = 3
    object Label3: TLabel
      Left = 12
      Top = 16
      Width = 30
      Height = 13
      Caption = #1050#1091#1076#1072'  '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object cxIComboBox1: TcxImageComboBox
      Left = 60
      Top = 12
      ParentFont = False
      Properties.Items = <>
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -13
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsBold]
      Style.LookAndFeel.Kind = lfOffice11
      Style.IsFontAssigned = True
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 0
      Width = 221
    end
  end
end
