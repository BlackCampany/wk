object fmSpec: TfmSpec
  Left = 468
  Top = 219
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = '\'
  ClientHeight = 638
  ClientWidth = 727
  Color = 16765348
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 8
    Width = 75
    Height = 13
    Caption = #1057#1086#1089#1090#1072#1074' '#1079#1072#1082#1072#1079#1072
    Color = clBlack
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    Transparent = True
  end
  object Label2: TLabel
    Left = 531
    Top = 16
    Width = 46
    Height = 13
    Caption = #1054#1090#1074'.'#1083#1080#1094#1086
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 16514043
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object Label3: TLabel
    Left = 531
    Top = 104
    Width = 38
    Height = 13
    Caption = #1057#1090#1086#1083' '#8470
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 16514043
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object Label5: TLabel
    Left = 531
    Top = 64
    Width = 38
    Height = 13
    Caption = #1054#1090#1082#1088#1099#1090
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 16514043
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object Label6: TLabel
    Left = 531
    Top = 80
    Width = 34
    Height = 13
    Caption = #1057#1090#1072#1090#1091#1089
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 16514043
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object Label7: TLabel
    Left = 603
    Top = 16
    Width = 12
    Height = 13
    Caption = '....'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 16514043
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object Label10: TLabel
    Left = 603
    Top = 64
    Width = 12
    Height = 13
    Caption = '....'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 16514043
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object Label11: TLabel
    Left = 603
    Top = 80
    Width = 12
    Height = 13
    Caption = '....'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 16514043
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object Label12: TLabel
    Left = 587
    Top = 16
    Width = 3
    Height = 13
    Caption = ':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 16514043
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object Label15: TLabel
    Left = 587
    Top = 64
    Width = 3
    Height = 13
    Caption = ':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 16514043
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object Label16: TLabel
    Left = 587
    Top = 80
    Width = 3
    Height = 13
    Caption = ':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 16514043
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object Label17: TLabel
    Left = 152
    Top = 8
    Width = 370
    Height = 13
    Alignment = taRightJustify
    AutoSize = False
    Caption = '.....'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold, fsUnderline]
    ParentFont = False
    Transparent = True
  end
  object Label4: TLabel
    Left = 531
    Top = 136
    Width = 35
    Height = 13
    Caption = #1043#1086#1089#1090#1077#1081
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 16514043
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object Label8: TLabel
    Left = 531
    Top = 40
    Width = 73
    Height = 16
    Caption = #1047#1072#1082#1072#1079' '#8470'1'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 16514043
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Edit1: TcxTextEdit
    Left = 568
    Top = 560
    BeepOnEnter = False
    TabOrder = 13
    Width = 89
  end
  object GridSpec: TcxGrid
    Tag = 1
    Left = 16
    Top = 26
    Width = 505
    Height = 383
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    LookAndFeel.Kind = lfFlat
    object ViewSpec: TcxGridDBBandedTableView
      DragMode = dmAutomatic
      OnDragDrop = ViewSpecDragDrop
      OnDragOver = ViewSpecDragOver
      OnStartDrag = ViewSpecStartDrag
      NavigatorButtons.ConfirmDelete = False
      FilterBox.CustomizeDialog = False
      OnFocusedRecordChanged = ViewSpecFocusedRecordChanged
      DataController.DataSource = dmC.dsCurSpec
      DataController.KeyFieldNames = 'ID'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SUMMA'
          Column = ViewSpecSUMMA
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnGrouping = False
      OptionsCustomize.ColumnSorting = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsSelection.UnselectFocusedRecordOnExit = False
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      Styles.Selection = dmC.cxStyle16
      Styles.Footer = dmC.cxStyle29
      Styles.Header = dmC.cxStyle28
      Styles.BandBackground = dmC.cxStyle28
      Styles.BandHeader = dmC.cxStyle28
      Bands = <
        item
          Width = 271
        end
        item
          Width = 200
        end>
      object ViewSpecCODE: TcxGridDBBandedColumn
        Caption = #1050#1086#1076
        DataBinding.FieldName = 'SIFR'
        Options.Editing = False
        Options.Sorting = False
        Width = 79
        Position.BandIndex = 0
        Position.ColIndex = 1
        Position.RowIndex = 1
      end
      object ViewSpecNAME: TcxGridDBBandedColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'NAME'
        Options.Editing = False
        Options.Sorting = False
        Styles.Content = dmC.cxStyle18
        Width = 226
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object ViewSpecPRICE: TcxGridDBBandedColumn
        Caption = #1062#1077#1085#1072
        DataBinding.FieldName = 'PRICE'
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Options.Editing = False
        Options.Sorting = False
        Styles.Content = dmC.cxStyle18
        Width = 70
        Position.BandIndex = 1
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object ViewSpecQUANTITY: TcxGridDBBandedColumn
        Caption = #1050#1086#1083'-'#1074#1086
        DataBinding.FieldName = 'QUANTITY'
        PropertiesClassName = 'TcxCalcEditProperties'
        Options.Editing = False
        Styles.Content = dmC.cxStyle18
        Width = 61
        Position.BandIndex = 1
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object ViewSpecSUMMA: TcxGridDBBandedColumn
        Caption = #1057#1091#1084#1084#1072' '#1080#1090#1086#1075#1086
        DataBinding.FieldName = 'SUMMA'
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Options.Editing = False
        Styles.Content = dmC.cxStyle18
        Width = 83
        Position.BandIndex = 1
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object ViewSpecDPROC: TcxGridDBBandedColumn
        Caption = #1055#1088#1086#1094#1077#1085#1090' '#1089#1082#1080#1076#1082#1080
        DataBinding.FieldName = 'DPROC'
        Options.Editing = False
        Styles.Content = dmC.cxStyle12
        Position.BandIndex = 1
        Position.ColIndex = 0
        Position.RowIndex = 1
      end
      object ViewSpecDSUM: TcxGridDBBandedColumn
        Caption = #1057#1091#1084#1084#1072' '#1089#1082#1080#1076#1082#1080
        DataBinding.FieldName = 'DSUM'
        Options.Editing = False
        Styles.Content = dmC.cxStyle12
        Position.BandIndex = 1
        Position.ColIndex = 1
        Position.RowIndex = 1
      end
      object ViewSpecISTATUS: TcxGridDBBandedColumn
        Caption = #1057#1090#1072#1090#1091#1089
        DataBinding.FieldName = 'ISTATUS'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Images = dmC.imState
        Properties.Items = <
          item
            Description = #1074' '#1088#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1085#1080#1080
            ImageIndex = 2
            Value = 0
          end
          item
            Description = #1079#1072#1082#1072#1079#1072#1085#1086
            ImageIndex = 0
            Value = 1
          end>
        Options.Editing = False
        Width = 156
        Position.BandIndex = 0
        Position.ColIndex = 2
        Position.RowIndex = 1
      end
      object ViewSpecQUEST: TcxGridDBBandedColumn
        Caption = #1043#1086#1089#1090#1100
        DataBinding.FieldName = 'QUEST'
        Styles.Content = dmC.cxStyle5
        Position.BandIndex = 1
        Position.ColIndex = 2
        Position.RowIndex = 1
      end
      object ViewSpecID: TcxGridDBBandedColumn
        Caption = #8470
        DataBinding.FieldName = 'ID'
        Width = 36
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 1
      end
    end
    object LevelSpec: TcxGridLevel
      GridView = ViewSpec
    end
  end
  object Button1: TcxButton
    Left = 16
    Top = 544
    Width = 89
    Height = 60
    Caption = #1044#1086#1073#1072#1074#1080#1090#1100
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    TabStop = False
    OnClick = Button1Click
    Colors.Default = 11457198
    Colors.Normal = 15921891
    Colors.Hot = 7384944
    Colors.Pressed = 5411922
    Layout = blGlyphTop
    LookAndFeel.Kind = lfUltraFlat
  end
  object cxButton1: TcxButton
    Left = 208
    Top = 544
    Width = 89
    Height = 60
    Action = acChangeQuantity
    Caption = #1048#1079#1084'. '#1082#1086#1083'-'#1074#1086
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    TabStop = False
    Colors.Default = 11457198
    Colors.Normal = 15921891
    Colors.Hot = 7384944
    Colors.Pressed = 5411922
    Layout = blGlyphTop
    LookAndFeel.Kind = lfUltraFlat
  end
  object cxButton2: TcxButton
    Left = 424
    Top = 544
    Width = 97
    Height = 60
    Caption = #1059#1076#1083'.'#1041#1083#1102#1076#1086
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 3
    TabStop = False
    OnClick = cxButton2Click
    Colors.Default = 10790143
    Colors.Normal = 10790143
    Colors.Hot = 7961087
    Colors.Pressed = 7961087
    Layout = blGlyphTop
    LookAndFeel.Kind = lfUltraFlat
  end
  object Button5: TcxButton
    Left = 600
    Top = 367
    Width = 113
    Height = 42
    Action = acPrePrint
    Caption = #1057#1095#1077#1090
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 4
    TabStop = False
    Colors.Default = clWhite
    Colors.Normal = clWhite
    Colors.Hot = clMoneyGreen
    Colors.Pressed = clMoneyGreen
    Layout = blGlyphTop
    LookAndFeel.Kind = lfUltraFlat
  end
  object cxButton3: TcxButton
    Left = 532
    Top = 544
    Width = 181
    Height = 60
    Action = Action10
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 5
    TabStop = False
    Colors.Default = 11457198
    Colors.Normal = 15921891
    Colors.Hot = 7384944
    Colors.Pressed = 5411922
    Glyph.Data = {
      5E040000424D5E04000000000000360000002800000012000000130000000100
      18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
      CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
      8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
      0000CED3D6848684848684848684848684848684848684848684848684848684
      848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
      7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
      FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
      00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
      75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
      0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
      FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
      CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
      007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
      00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
      75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
      0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
      FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
      CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
      00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
      494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
      0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
      00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
      CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
      0000}
    Layout = blGlyphTop
    LookAndFeel.Kind = lfUltraFlat
  end
  object cxButton4: TcxButton
    Left = 304
    Top = 544
    Width = 89
    Height = 60
    Caption = #1055#1077#1088#1077#1085#1077#1089#1090#1080
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 6
    TabStop = False
    OnClick = cxButton4Click
    Colors.Default = 11457198
    Colors.Normal = 15921891
    Colors.Hot = 7384944
    Colors.Pressed = 5411922
    Layout = blGlyphTop
    LookAndFeel.Kind = lfUltraFlat
  end
  object GridMod: TcxGrid
    Left = 16
    Top = 424
    Width = 305
    Height = 105
    TabOrder = 7
    LookAndFeel.Kind = lfFlat
    object ViewMod: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      OnCustomDrawCell = ViewModCustomDrawCell
      DataController.DataSource = dmC.dsCurMod
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnGrouping = False
      OptionsCustomize.ColumnMoving = False
      OptionsCustomize.ColumnSorting = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsSelection.UnselectFocusedRecordOnExit = False
      OptionsView.GroupByBox = False
      Styles.Header = dmC.cxStyle28
      object ViewModName: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'NAME'
        Styles.Content = dmC.cxStyle18
        Width = 269
      end
      object ViewModQuantity: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086
        DataBinding.FieldName = 'QUANTITY'
        Visible = False
        Styles.Content = dmC.cxStyle18
        Width = 74
      end
      object ViewModSIFR: TcxGridDBColumn
        Caption = #1050#1086#1076
        DataBinding.FieldName = 'SIFR'
        Visible = False
      end
    end
    object LevelMod: TcxGridLevel
      GridView = ViewMod
    end
  end
  object cxButton5: TcxButton
    Left = 344
    Top = 424
    Width = 81
    Height = 49
    BiDiMode = bdRightToLeftNoAlign
    Caption = #1044#1086#1073'. '#1052#1086#1076#1080#1092'.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentBiDiMode = False
    ParentFont = False
    TabOrder = 8
    TabStop = False
    OnClick = cxButton5Click
    Colors.Default = 8454143
    Colors.Normal = 8454143
    Colors.Hot = 57825
    Colors.Pressed = 52942
    Layout = blGlyphTop
    LookAndFeel.Kind = lfUltraFlat
  end
  object cxButton6: TcxButton
    Left = 344
    Top = 480
    Width = 81
    Height = 49
    BiDiMode = bdRightToLeftNoAlign
    Caption = #1059#1076#1083'. '#1052#1086#1076#1080#1092'.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentBiDiMode = False
    ParentFont = False
    TabOrder = 9
    TabStop = False
    OnClick = cxButton6Click
    Colors.Default = 8454143
    Colors.Normal = 8454143
    Colors.Hot = 57825
    Colors.Pressed = 52942
    Layout = blGlyphTop
    LookAndFeel.Kind = lfUltraFlat
  end
  object Panel1: TPanel
    Left = 0
    Top = 611
    Width = 727
    Height = 27
    Align = alBottom
    BevelInner = bvLowered
    Color = 15565648
    TabOrder = 10
    object Label18: TLabel
      Left = 16
      Top = 8
      Width = 13
      Height = 13
      Caption = '...'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object Button2: TButton
    Left = 312
    Top = 520
    Width = 121
    Height = 25
    Caption = #1055#1088#1086#1073#1072' '#1087#1077#1095#1072#1090#1080
    TabOrder = 11
    Visible = False
    OnClick = Button2Click
  end
  object cxButton7: TcxButton
    Left = 112
    Top = 544
    Width = 89
    Height = 60
    Caption = #1055#1086' '#1082#1086#1076#1091
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 12
    TabStop = False
    OnClick = cxButton7Click
    Colors.Default = 11457198
    Colors.Normal = 15921891
    Colors.Hot = 7384944
    Colors.Pressed = 5411922
    Layout = blGlyphTop
    LookAndFeel.Kind = lfUltraFlat
  end
  object cxButton8: TcxButton
    Left = 532
    Top = 316
    Width = 61
    Height = 41
    BiDiMode = bdRightToLeftNoAlign
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentBiDiMode = False
    ParentFont = False
    TabOrder = 14
    TabStop = False
    OnClick = cxButton8Click
    Colors.Default = 14408629
    Colors.Normal = 15921891
    Colors.Hot = 7384944
    Colors.Pressed = 5411922
    Glyph.Data = {
      22090000424D2209000000000000360400002800000023000000230000000100
      080000000000EC04000000000000000000000001000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000C0DCC000F0CA
      A6000020400000206000002080000020A0000020C0000020E000004000000040
      20000040400000406000004080000040A0000040C0000040E000006000000060
      20000060400000606000006080000060A0000060C0000060E000008000000080
      20000080400000806000008080000080A0000080C0000080E00000A0000000A0
      200000A0400000A0600000A0800000A0A00000A0C00000A0E00000C0000000C0
      200000C0400000C0600000C0800000C0A00000C0C00000C0E00000E0000000E0
      200000E0400000E0600000E0800000E0A00000E0C00000E0E000400000004000
      20004000400040006000400080004000A0004000C0004000E000402000004020
      20004020400040206000402080004020A0004020C0004020E000404000004040
      20004040400040406000404080004040A0004040C0004040E000406000004060
      20004060400040606000406080004060A0004060C0004060E000408000004080
      20004080400040806000408080004080A0004080C0004080E00040A0000040A0
      200040A0400040A0600040A0800040A0A00040A0C00040A0E00040C0000040C0
      200040C0400040C0600040C0800040C0A00040C0C00040C0E00040E0000040E0
      200040E0400040E0600040E0800040E0A00040E0C00040E0E000800000008000
      20008000400080006000800080008000A0008000C0008000E000802000008020
      20008020400080206000802080008020A0008020C0008020E000804000008040
      20008040400080406000804080008040A0008040C0008040E000806000008060
      20008060400080606000806080008060A0008060C0008060E000808000008080
      20008080400080806000808080008080A0008080C0008080E00080A0000080A0
      200080A0400080A0600080A0800080A0A00080A0C00080A0E00080C0000080C0
      200080C0400080C0600080C0800080C0A00080C0C00080C0E00080E0000080E0
      200080E0400080E0600080E0800080E0A00080E0C00080E0E000C0000000C000
      2000C0004000C0006000C0008000C000A000C000C000C000E000C0200000C020
      2000C0204000C0206000C0208000C020A000C020C000C020E000C0400000C040
      2000C0404000C0406000C0408000C040A000C040C000C040E000C0600000C060
      2000C0604000C0606000C0608000C060A000C060C000C060E000C0800000C080
      2000C0804000C0806000C0808000C080A000C080C000C080E000C0A00000C0A0
      2000C0A04000C0A06000C0A08000C0A0A000C0A0C000C0A0E000C0C00000C0C0
      2000C0C04000C0C06000C0C08000C0C0A000F0FBFF00A4A0A000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFF
      FFFFFF000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFF
      FFFFFFFFFFFFFF00A4A4A4A4A4A40700FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFF
      FFFFFFFFFFFFFFFFFFFFFF00A4A4A4A4A4A40700FFFFFFFFFFFFFFFFFFFFFFFF
      FF00FFFFFFFFFFFFFFFFFFFFFFFFFF00A4A4A4A4A4A40700FFFFFFFFFFFFFFFF
      FFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFF00A4A4A4A4A4A40700FFFFFFFF
      FFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFF00A4A4A4A4A4A40700
      FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFF00A4A4A4A4
      A4A40700FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFF00
      A4A4A4A4A4A40700FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFF
      FFFFFF00A4A4A4A4A4A40700FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFF
      FFFFFFFFFFFFFF00A4A4A4A4A4A40700FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFF
      FFFFFFFFFFFFFFFFFFFFFF00A4A4A4A4A4A40700FFFFFFFFFFFFFFFFFFFFFFFF
      FF00FFFFFFFFFFFFFFFFFFFFFFFFFF00A4A4A4A4A4A40700FFFFFFFFFFFFFFFF
      FFFFFFFFFF00FFFFFFFFFFFFFFFF000000000000A4A4A4A4A4A4070000000000
      00FFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFF00A4A4A4A4A4A4A4A4A4A4A4A4A4
      A4A407A400FFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFF00A4A4A4A4A4A4A4A4
      A4A4A4A4A407A400FFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFF00A4A4A4
      A4A4A4A4A4A4A4A407A400FFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFF
      FF00A4A4A4A4A4A4A4A4A407A400FFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFF
      FFFFFFFFFFFF00A4A4A4A4A4A4A407A400FFFFFFFFFFFFFFFFFFFFFFFF00FFFF
      FFFFFFFFFFFFFFFFFFFFFF00A4A4A4A4A407A400FFFFFFFFFFFFFFFFFFFFFFFF
      FF00FFFFFFFFFFFFFFFFFFFFFFFFFFFF00A4A4A407A400FFFFFFFFFFFFFFFFFF
      FFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00A407A400FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00A400FFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFF00}
    Layout = blGlyphTop
    LookAndFeel.Kind = lfUltraFlat
  end
  object cxButton9: TcxButton
    Left = 532
    Top = 368
    Width = 61
    Height = 41
    BiDiMode = bdRightToLeftNoAlign
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentBiDiMode = False
    ParentFont = False
    TabOrder = 15
    TabStop = False
    OnClick = cxButton9Click
    Colors.Default = 11457198
    Colors.Normal = 15921891
    Colors.Hot = 7384944
    Colors.Pressed = 5411922
    Glyph.Data = {
      22090000424D2209000000000000360400002800000023000000230000000100
      080000000000EC04000000000000000000000001000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000C0DCC000F0CA
      A6000020400000206000002080000020A0000020C0000020E000004000000040
      20000040400000406000004080000040A0000040C0000040E000006000000060
      20000060400000606000006080000060A0000060C0000060E000008000000080
      20000080400000806000008080000080A0000080C0000080E00000A0000000A0
      200000A0400000A0600000A0800000A0A00000A0C00000A0E00000C0000000C0
      200000C0400000C0600000C0800000C0A00000C0C00000C0E00000E0000000E0
      200000E0400000E0600000E0800000E0A00000E0C00000E0E000400000004000
      20004000400040006000400080004000A0004000C0004000E000402000004020
      20004020400040206000402080004020A0004020C0004020E000404000004040
      20004040400040406000404080004040A0004040C0004040E000406000004060
      20004060400040606000406080004060A0004060C0004060E000408000004080
      20004080400040806000408080004080A0004080C0004080E00040A0000040A0
      200040A0400040A0600040A0800040A0A00040A0C00040A0E00040C0000040C0
      200040C0400040C0600040C0800040C0A00040C0C00040C0E00040E0000040E0
      200040E0400040E0600040E0800040E0A00040E0C00040E0E000800000008000
      20008000400080006000800080008000A0008000C0008000E000802000008020
      20008020400080206000802080008020A0008020C0008020E000804000008040
      20008040400080406000804080008040A0008040C0008040E000806000008060
      20008060400080606000806080008060A0008060C0008060E000808000008080
      20008080400080806000808080008080A0008080C0008080E00080A0000080A0
      200080A0400080A0600080A0800080A0A00080A0C00080A0E00080C0000080C0
      200080C0400080C0600080C0800080C0A00080C0C00080C0E00080E0000080E0
      200080E0400080E0600080E0800080E0A00080E0C00080E0E000C0000000C000
      2000C0004000C0006000C0008000C000A000C000C000C000E000C0200000C020
      2000C0204000C0206000C0208000C020A000C020C000C020E000C0400000C040
      2000C0404000C0406000C0408000C040A000C040C000C040E000C0600000C060
      2000C0604000C0606000C0608000C060A000C060C000C060E000C0800000C080
      2000C0804000C0806000C0808000C080A000C080C000C080E000C0A00000C0A0
      2000C0A04000C0A06000C0A08000C0A0A000C0A0C000C0A0E000C0C00000C0C0
      2000C0C04000C0C06000C0C08000C0C0A000F0FBFF00A4A0A000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFF00A400FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFF00A407A400FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFF
      FFFFFFFFFFFFFFFF00A4A4A407A400FFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFF
      FFFFFFFFFFFFFFFFFFFFFF00A4A4A4A4A407A400FFFFFFFFFFFFFFFFFFFFFFFF
      FF00FFFFFFFFFFFFFFFFFFFFFFFF00A4A4A4A4A4A4A407A400FFFFFFFFFFFFFF
      FFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFF00A4A4A4A4A4A4A4A4A407A400FFFF
      FFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFF00A4A4A4A4A4A4A4A4A4A4A4
      07A400FFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFF00A4A4A4A4A4A4A4A4
      A4A4A4A4A407A400FFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFF00A4A4A4A4A4
      A4A4A4A4A4A4A4A4A4A407A400FFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFF0000
      00000000A4A4A4A4A4A407000000000000FFFFFFFFFFFFFFFF00FFFFFFFFFFFF
      FFFFFFFFFFFFFF00A4A4A4A4A4A40700FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFF
      FFFFFFFFFFFFFFFFFFFFFF00A4A4A4A4A4A40700FFFFFFFFFFFFFFFFFFFFFFFF
      FF00FFFFFFFFFFFFFFFFFFFFFFFFFF00A4A4A4A4A4A40700FFFFFFFFFFFFFFFF
      FFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFF00A4A4A4A4A4A40700FFFFFFFF
      FFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFF00A4A4A4A4A4A40700
      FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFF00A4A4A4A4
      A4A40700FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFF00
      A4A4A4A4A4A40700FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFF
      FFFFFF00A4A4A4A4A4A40700FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFF
      FFFFFFFFFFFFFF00A4A4A4A4A4A40700FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFF
      FFFFFFFFFFFFFFFFFFFFFF00A4A4A4A4A4A40700FFFFFFFFFFFFFFFFFFFFFFFF
      FF00FFFFFFFFFFFFFFFFFFFFFFFFFF00A4A4A4A4A4A40700FFFFFFFFFFFFFFFF
      FFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000FFFFFFFF
      FFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFF00}
    Layout = blGlyphTop
    LookAndFeel.Kind = lfUltraFlat
  end
  object cxButton10: TcxButton
    Left = 600
    Top = 316
    Width = 113
    Height = 41
    Caption = #1057#1082#1080#1076#1082#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 16
    TabStop = False
    OnClick = cxButton10Click
    Colors.Default = 11192063
    Colors.Normal = 11192063
    Colors.Hot = 7446527
    Colors.Pressed = 7446527
    Layout = blGlyphTop
    LookAndFeel.Kind = lfUltraFlat
  end
  object BEdit1: TcxButtonEdit
    Left = 584
    Top = 100
    Properties.Buttons = <
      item
        Default = True
        Kind = bkEllipsis
      end>
    Properties.OnButtonClick = BEdit1PropertiesButtonClick
    Style.LookAndFeel.Kind = lfOffice11
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 17
    Text = 'BEdit1'
    OnClick = BEdit1Click
    Width = 121
  end
  object SEdit1: TcxSpinEdit
    Left = 584
    Top = 132
    Properties.AssignedValues.MinValue = True
    Properties.MaxValue = 999.000000000000000000
    Style.BorderStyle = ebsOffice11
    Style.HotTrack = True
    TabOrder = 18
    Value = 1
    Width = 41
  end
  object cxButton11: TcxButton
    Left = 632
    Top = 126
    Width = 33
    Height = 27
    BiDiMode = bdRightToLeftNoAlign
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentBiDiMode = False
    ParentFont = False
    TabOrder = 19
    TabStop = False
    OnClick = cxButton11Click
    Colors.Default = clWhite
    Colors.Normal = clWhite
    Colors.Hot = clMoneyGreen
    Colors.Pressed = clMoneyGreen
    Glyph.Data = {
      76010000424D760100000000000036000000280000000A0000000A0000000100
      18000000000040010000C40E0000C40E00000000000000000000FFFFFFFFFFFF
      FFFFFFFF6600FF6600FF6600FF6600FFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
      FFFFFFFF6600FF6600FF6600FF6600FFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
      FFFFFFFF6600FF6600FF6600FF6600FFFFFFFFFFFFFFFFFF0000FF6600FF6600
      FF6600FF6600FF6600FF6600FF6600FF6600FF6600FF66000000FF6600FF6600
      FF6600FF6600FF6600FF6600FF6600FF6600FF6600FF66000000FF9966FF9966
      FF9966FF9966FF9966FF9966FF9966FF9966FF9966FF99660000CCCC99CCCC99
      CCCC99CCCC99CCCC99CCCC99CCCC99CCCC99CCCC99CCCC990000FFFFFFFFFFFF
      FFFFFFFF9966CCCC99CCCC99FF9966FFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
      FFFFFFFF9966CCCC99CCCC99FF9966FFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
      FFFFFFFF9966FF9966FF9966FF9966FFFFFFFFFFFFFFFFFF0000}
    Layout = blGlyphTop
    LookAndFeel.Kind = lfUltraFlat
  end
  object cxButton12: TcxButton
    Left = 532
    Top = 423
    Width = 181
    Height = 54
    Caption = #1056#1072#1089#1095#1077#1090
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 20
    TabStop = False
    OnClick = cxButton12Click
    Colors.Default = 12621940
    Colors.Normal = 12621940
    Colors.Hot = 10843723
    Colors.Pressed = 10843723
    Layout = blGlyphTop
    LookAndFeel.Kind = lfUltraFlat
  end
  object cxButton13: TcxButton
    Left = 668
    Top = 126
    Width = 33
    Height = 27
    BiDiMode = bdRightToLeftNoAlign
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentBiDiMode = False
    ParentFont = False
    TabOrder = 21
    TabStop = False
    OnClick = cxButton13Click
    Colors.Default = clWhite
    Colors.Normal = clWhite
    Colors.Hot = clMoneyGreen
    Colors.Pressed = clMoneyGreen
    Glyph.Data = {
      76010000424D760100000000000036000000280000000D000000080000000100
      1800000000004001000000000000000000000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFF00FFFFFF633AF3633AF3633AF3633AF3633AF3633AF363
      3AF3633AF3633AF3633AF3633AF3FFFFFF00FFFFFF633AF3A777E8A777E8A777
      E8A777E8A777E8A777E8A777E8A777E8A777E8633AF3FFFFFF00FFFFFF633AF3
      C3A7FFC3A7FFC3A7FFC3A7FFC3A7FFC3A7FFC3A7FFC3A7FFC3A7FFA777E8FFFF
      FF00FFFFFFA777E8A777E8A777E8A777E8A777E8A777E8A777E8A777E8A777E8
      A777E8A777E8FFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00}
    Layout = blGlyphTop
    LookAndFeel.Kind = lfUltraFlat
  end
  object cxButton14: TcxButton
    Left = 440
    Top = 424
    Width = 81
    Height = 49
    BiDiMode = bdRightToLeftNoAlign
    Caption = #1044#1086#1073'. '#1057#1086#1086#1073#1097'.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentBiDiMode = False
    ParentFont = False
    TabOrder = 22
    TabStop = False
    OnClick = cxButton14Click
    Colors.Default = 8454143
    Colors.Normal = 8454143
    Colors.Hot = 57825
    Colors.Pressed = 52942
    Layout = blGlyphTop
    LookAndFeel.Kind = lfUltraFlat
  end
  object cxButton15: TcxButton
    Left = 440
    Top = 480
    Width = 81
    Height = 49
    BiDiMode = bdRightToLeftNoAlign
    Caption = #1059#1076#1083'.'#1057#1086#1086#1073#1097'.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentBiDiMode = False
    ParentFont = False
    TabOrder = 23
    TabStop = False
    OnClick = cxButton15Click
    Colors.Default = 8454143
    Colors.Normal = 8454143
    Colors.Hot = 57825
    Colors.Pressed = 52942
    Layout = blGlyphTop
    LookAndFeel.Kind = lfUltraFlat
  end
  object cxButton16: TcxButton
    Left = 600
    Top = 487
    Width = 113
    Height = 42
    Caption = #1050#1091#1087#1086#1085#1099
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 24
    TabStop = False
    OnClick = cxButton16Click
    Colors.Default = clLime
    Colors.Normal = clLime
    Colors.Hot = clLime
    Colors.Pressed = 46848
    Layout = blGlyphTop
    LookAndFeel.Kind = lfUltraFlat
  end
  object cxButton17: TcxButton
    Left = 600
    Top = 264
    Width = 113
    Height = 41
    Caption = #1058#1080#1087' '#1087#1088#1086#1076#1072#1078'12345'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 25
    TabStop = False
    OnClick = cxButton17Click
    Colors.Default = 15921891
    Colors.DefaultText = clBlack
    Colors.Normal = 15921891
    Colors.Hot = 15921891
    Colors.Pressed = 13816484
    Colors.PressedText = clBlack
    Layout = blGlyphTop
    LookAndFeel.Kind = lfUltraFlat
  end
  object Panel2: TPanel
    Left = 532
    Top = 164
    Width = 185
    Height = 89
    Color = 15565648
    DragMode = dmAutomatic
    TabOrder = 26
    OnDragDrop = Panel2DragDrop
    OnDragOver = Panel2DragOver
    OnStartDrag = Panel2StartDrag
    object Image1: TImage
      Left = 8
      Top = 52
      Width = 25
      Height = 33
      DragMode = dmAutomatic
      Picture.Data = {
        07544269746D61708E060000424D8E0600000000000036000000280000001200
        00001D0000000100180000000000580600000000000000000000000000000000
        0000C56A31C56A31C56A31C56A31C56A31C56A31C56A31C56A31C56A31C56A31
        C56A31C56A31C56A31C56A31C56A31C56A31C56A31C56A310000C56A31C56A31
        C56A31C56A31C56A31C56A31C56A31C56A31C56A31C56A31C56A31C56A31C56A
        31C56A31C56A31C56A31C56A31C56A310000C56A31C56A31C56A31C56A31C56A
        3100000000000000000000000000000000000000000000000000000000000000
        0000C56A31C56A310000C56A31C56A31C56A31C56A31C56A31000000FFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000C56A31C56A31
        0000C56A31C56A31C56A31C56A31C56A31000000FFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000C56A31C56A310000C56A31C56A31
        C56A31C56A31C56A31000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000C56A31C56A310000C56A31C56A31C56A31C56A31C56A
        31000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
        0000C56A31C56A310000C56A31C56A31C56A31C56A31C56A31000000FFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000C56A31C56A31
        0000C56A31C56A31C56A31C56A31C56A31000000FFFFFFFFFFFFFFFFFF000000
        000000FFFFFFFFFFFFFFFFFFFFFFFF000000C56A31C56A310000C56A31C56A31
        C56A31C56A31C56A31000000FFFFFFFFFFFF000000FFFFFFFFFFFF000000FFFF
        FFFFFFFFFFFFFF000000C56A31C56A310000C56A31C56A31C56A31C56A31C56A
        31000000FFFFFFFFFFFF000000FFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFF00
        0000C56A31C56A310000C56A31C56A31C56A31C56A31C56A31000000FFFFFF00
        0000FFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFF000000C56A31C56A31
        0000C56A31000000C56A31C56A31C56A31000000FFFFFF000000FFFFFFFFFFFF
        000000FFFFFFFFFFFFFFFFFFFFFFFF000000C56A31C56A310000C56A31000000
        000000C56A31C56A31000000000000FFFFFFFFFFFF000000FFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000C56A31C56A310000C56A31000000FFFFFF000000C56A
        31000000000000FFFFFFFFFFFF00000000000000000000000000000000000000
        0000C56A31C56A310000C56A31000000FFFFFFFFFFFF000000000000FFFFFFFF
        FFFF000000C56A31C56A31C56A31C56A31C56A31C56A31C56A31C56A31C56A31
        0000C56A31000000FFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF000000C56A31
        C56A31C56A31C56A31C56A31C56A31C56A31C56A31C56A310000C56A31000000
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000
        00C56A31C56A31C56A31C56A31C56A310000C56A31000000FFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000C56A31C56A31C56A31C5
        6A31C56A31C56A310000C56A31000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFF000000C56A31C56A31C56A31C56A31C56A31C56A31C56A31
        0000C56A31000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000
        C56A31C56A31C56A31C56A31C56A31C56A31C56A31C56A310000C56A31000000
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000C56A31C56A31C56A31C56A
        31C56A31C56A31C56A31C56A31C56A310000C56A31000000FFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000C56A31C56A31C56A31C56A31C56A31C56A31C56A31C5
        6A31C56A31C56A310000C56A31000000FFFFFFFFFFFFFFFFFFFFFFFF000000C5
        6A31C56A31C56A31C56A31C56A31C56A31C56A31C56A31C56A31C56A31C56A31
        0000C56A31000000FFFFFFFFFFFFFFFFFF000000C56A31C56A31C56A31C56A31
        C56A31C56A31C56A31C56A31C56A31C56A31C56A31C56A310000C56A31000000
        FFFFFFFFFFFF000000C56A31C56A31C56A31C56A31C56A31C56A31C56A31C56A
        31C56A31C56A31C56A31C56A31C56A310000C56A31000000FFFFFF000000C56A
        31C56A31C56A31C56A31C56A31C56A31C56A31C56A31C56A31C56A31C56A31C5
        6A31C56A31C56A310000C56A31000000000000C56A31C56A31C56A31C56A31C5
        6A31C56A31C56A31C56A31C56A31C56A31C56A31C56A31C56A31C56A31C56A31
        0000C56A31C56A31C56A31C56A31C56A31C56A31C56A31C56A31C56A31C56A31
        C56A31C56A31C56A31C56A31C56A31C56A31C56A31C56A310000}
      Transparent = True
      OnDragDrop = Panel2DragDrop
      OnDragOver = Panel2DragOver
      OnStartDrag = Panel2StartDrag
    end
    object cxImageComboBox1: TcxImageComboBox
      Left = 72
      Top = 7
      EditValue = 1
      ParentFont = False
      Properties.ButtonGlyph.Data = {
        16080000424D16080000000000003600000028000000170000001C0000000100
        180000000000E007000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB5B2AD6B7D8494AAB5ADB6BDBDBE
        BDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
        0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC6C3BD528AA57BAEC6
        4292BD4296BD529EC673AEC694BACEA5BEC6BDC3C6C6C7C6D6C7C6FFFFFFFFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA5
        AEB5318AAD7BB2C63986AD317DA53986AD4A96BD52A2C652A6CE5AAED66BBAD6
        84C3DEA5CBDEADBEC6C6BAB5FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFA5B2BD318EB57BB6CE2996C6218ABD3992BD529ABD4292B542
        8EB54292B53996BD399AC639A6CE4A9EBDBDBAB5FFFFFF000000FFFFFFFFFFFF
        FFFFFFFFFFFFEFDBCEE7CFC6DECBBD8CA2AD2996BD7BBACE299ED6219AD652B6
        DE5AB6DE39AADE42AED639A6D631A2CE299EC6299AC6318AADC6C3C6FFFFFF00
        0000FFFFFFFFFFFFFFFFFFD6C3BDA5968C8C827B8C868463828C299AC684BECE
        29A2D642AEDE6BC3E742B6DE52BAE752BEE742BAE729B6E718B2E710B6EF21A6
        CEC6C7C6FFFFFF000000FFFFFFFFFFFFADA69C847D7B525152393C394A49424A
        718429A2CE84BED642B2DE6BC7E752BEE763C7EF63CBEF4AC3EF29BAEF10B6EF
        10BAEF10BEF721AED6C6C7C6FFFFFF000000FFFFFFA596947B7D7B6B696B6365
        637371734A494A31596329A6CE94C3D66BAAC66BA6BD73B2CE73BED652BADE29
        B6E718BAEF18BEEF10C3F710C7F721B2DEC6C7CEFFFFFF000000C6BAB57B7D7B
        7B7D7B6361637B797BB5B2B57B797B4A718421AAD69CCBD68CCBDE63BAD639AA
        CE31A6CE31A2C639A6C642AACE42AECE42B2D631BADE31AACEC6CBCEFFFFFF00
        0000C6B6AD7B7D7B7B7D7B5A5D5A737573CECFCE9496944A6D7B42A2BD5A757B
        7BA6B55AA6BD39A6C631AAD621B2DE18B6E718BAE721BAE721BADE29B6DE39A2
        BDCECBCEFFFFFF000000D6C3BD7B7D7B7B797B5A5D5A5A595AB5B2B59C9A9C42
        494A7B929C5282945A96AD6B9EB584AABD73AEC67BB6C67BBACE7BBED66BBED6
        5ABEDE4AC3DE42B2CEC6C7C6FFFFFF000000E7CFC68C86847B797B5A595A4A49
        4A737173B5B6B531303139383952595A4A595A5A656B5A696B4A798C528EA54A
        92AD639AA573BAD66BC7DE84CBDEA5BEC6CEC3BDFFFFFF000000FFFFFFADA29C
        7375735A595A4245424A4D4AADAAAD393C392124214241424A494A4245426365
        63525552525552636563A59E9CD6D3CEBDCBCEB5C3C6FFFFFFFFFFFFFFFFFF00
        0000FFFFFFFFFFFFADA29C736D6B4A4D4A4A5152738AAD5A6D8C313431181C18
        393839424542393C394A494A948A84FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFEFD3C6B5AAAD6B7D945A86CE63
        8EDE6382B52934422120215A595A6B6963BDAEA5FFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFF9CBA
        F784B2F76B9AEF5A86D6638EDE6B8ECE424D5A847973DECFC6FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
        FFFFFFB5C3E78CBAFF8CBEFF8CBAFF73A6F7638EE773A2EF94A6CEE7CBBDFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
        0000FFFFFFFFFFFFE7D7CEA5BEF794BEFF8CBAFF8CBAFF8CB6FF6B9EF7739EEF
        A5BEE7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFFDED3DEA5C3FF94BEFF94BEFF94BEFF94
        BEFF7BAAF76B9AEF94AEE7EFD7CEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFCECFDEADCFFF9CC7
        FF9CC7FF9CC7FF9CC3FF8CB6FF6B9AEF5A7DB5B5AAA5FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
        D6D3E7BDD7FFA5CBFFA5CFFFADCFFFA5CFFF94BAFF6B96E75A82C652515AFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
        0000FFFFFFFFFFFFD6D7E7C6DFFFB5D7FFBDD7FFBDDBFFBDDBFF94B6EF394D6B
        31416B212429B5AAA5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFFDEDBEFDEEBFFCEE3FFD6E7FFDEEBFFCE
        E7FF8CB2E7313439000000101410AD9E9CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFBDC3D6C6D3
        EFDEEFFFEFF7FFCEE3FF7B96BD313031080808181818ADA2A5FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFEFE3DE
        7B757B393C4239454A5A697B7B92AD6B82A5525963393C39212021313029D6C3
        BDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
        0000FFFFFFEFDFD6A59E9C7371735A595A424142313431101410212421525152
        4245427B7573F7E3DEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFD6CBC6A59E9C7371734A4D4A29
        2C29211C1863615AB5A6A5E7DBCEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFF7E3DEDECFC6CEBEBDCEC3BDF7DFDEF7EBDEFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000}
      Properties.DropDownRows = 9
      Properties.Items = <
        item
          Description = #1043#1086#1089#1090#1100' 1'
          ImageIndex = 0
          Value = 1
        end
        item
          Description = #1043#1086#1089#1090#1100' 2'
          ImageIndex = 0
          Value = 2
        end
        item
          Description = #1043#1086#1089#1090#1100' 3'
          ImageIndex = 0
          Value = 3
        end
        item
          Description = #1043#1086#1089#1090#1100' 4'
          ImageIndex = 0
          Value = 4
        end
        item
          Description = #1043#1086#1089#1090#1100' 5'
          ImageIndex = 0
          Value = 5
        end
        item
          Description = #1043#1086#1089#1090#1100' 6'
          ImageIndex = 0
          Value = 6
        end
        item
          Description = #1043#1086#1089#1090#1100' 7'
          ImageIndex = 0
          Value = 7
        end
        item
          Description = #1043#1086#1089#1090#1100' 8'
          ImageIndex = 0
          Value = 8
        end
        item
          Description = #1043#1086#1089#1090#1100' 9 '
          ImageIndex = 0
          Value = 9
        end>
      Properties.OnChange = cxImageComboBox1PropertiesChange
      Style.BorderStyle = ebsOffice11
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -19
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = []
      Style.LookAndFeel.Kind = lfOffice11
      Style.ButtonStyle = btsDefault
      Style.ButtonTransparency = ebtAlways
      Style.IsFontAssigned = True
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 0
      Width = 105
    end
    object cxCheckBox1: TdxfCheckBox
      Left = 72
      Top = 44
      Width = 90
      Height = 35
      Checked = False
      GroupIndex = 0
      GlyphChecked.Data = {
        560D0000424D560D00000000000036000000280000001F000000230000000100
        180000000000200D000000000000000000000000000000000000C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0000000C0C0C0FFFFFF
        FFFFFFFFFFFFFFFFFFFEFEFEFCFCFCFAFAFAF5F5F5F4F4F4F1F1F1F2F2F2EFEF
        EFEEEEEEF4F4F4F3F3F3F2F2F2F3F3F3F7F7F7FAFAFAFCFCFCFDFDFDFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0000000C0C0C0FFFFFF
        FFFFFFFFFFFFFFFFFFF9F9F9F5F5F5F1F1F1EBEBEBE6E6E6DDDDDDD1D1D1CCCC
        CCCCCCCCCCCCCCD1D1D1DBDBDBE6E6E6EAEAEAF0F0F0F4F4F4F9F9F9FDFDFDFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0000000C0C0C0FFFFFF
        FFFFFFFFFEFFFDFEFFFAFCFDF9FAF8F5F7F1EEF1EFEFEFF5E3E2EBDBE3D9ADC3
        A0658457E3E2EBE4E0E5E7EBDFEBEFEAEEF0F1F6F7F5F9F9F9FEFEFEFEFEFEFE
        FEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0000000C0C0C0FFFFFF
        FFFFFFFFFEFFFFFEFFFEFDFFFFFDFFFFFFFFFDFAFCFFFFFFFAFFF2B0CD9C3B69
        1735670F30500FFAFFEBFBFFFCFFFFFFFFFEFFFFFDFFFFFFF9FEFFFDFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0000000C0C0C0FFFFFF
        FFFFFFFFFFFEFFFFFEFFFFFFFFFDFFFFFBFFFFFAFDF8FFF293B37D2E5E0C306A
        003D750A356D02295207EEFFE8F8F6FCFFFBFFFFFFFFFFFFFCFFFFFEFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0000000C0C0C0FFFFFF
        FFFFFFFFFFFEFFFFFCFFFEFDFEFDFFFDFFFFFBFFF2A1BC913B741F2D710C2D72
        09306F0F2D700D2F710B397019E7FDE1F9FFFAF8F9F5FFFDFFFFFEFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0000000C0C0C0FFFFFF
        FFFFFFFFFFFFFFFFFEF8FCF7FAFFFCF4FFF798BC902C5D1F30741B2E771B2E75
        1F3173212E751F347C2233781D396E25E7FFE4F8FDFFF5F7F7FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0000000C0C0C0FFFFFF
        FFFFFFFFFDFFFFFDFFFAFFFCE0F1E39ABD9B326E313B7D35377F2C30782B2E77
        2F2E782C3880332C752C2A772636822A457B3FF3FFFBF4FBF4FFFFFEFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0000000C0C0C0FFFFFF
        FFFFFFFFFCFFFFFDFFF3FFF899BE9E3F7B45308239328437348137347E3C307F
        3C2F843437833B38873E2D823831843F3C8841497945E7FFECFBFFFEFDFFFFFF
        FFFFFFFFFFFDFFFFFDFFFFFFFFFFFFFFFFFFFFFFC0C0C0000000C0C0C0F1FFFF
        FFFDFFFFF9FBF1F6ED9FCDA93C824D3089433686433487432F88432D88432F87
        452F87453286453287433287433087432F83423C884E3C854DEDFFF4F3F8F6FF
        FEFFFFFEFFF8FFFFFDFFFEFFFFFEFFFFFFFFFFFFC0C0C0000000C0C0C0FFFBFF
        FFFFFCEDFFF59AD2AF368B573390532F8D4C2A93502E9250359150388F4F398F
        4F388F4F35915033915031925030934F298C48308D50338D513C9262E2FFE8FB
        FFF8F6FFFFFBFEFFFFFEFFFFFFFEFFFFFFFFFFFFC0C0C0000000C0C0C0FFFDFF
        E7FFF297D4B2348753319A5B2B9657369B5D2999592F98593796593B95593797
        573498573297593497593297593099562C92512D92542F93573096554D9B6CD8
        FFE9FDFFFFFFFEFFFFFEFFFFFEFFFFFFFFFFFFFFC0C0C0000000C0C0C0F0FAF4
        8DD0B33796693E9D64339F5D2B9F5E2D995E329E632F995E379D63339A612B9B
        6128A065309D65319A61339C63309B5D329D5E349C5F349B6139A05C2A985E46
        9770F8FFFBFFFEFFFDFDFDFFFFFFFFFFFFFFFFFFC0C0C0000000C0C0C0CCF1E1
        40BB931AA572309F6B37A66E2AA56B32A46E38A56D32A26C31A97325A2702AAF
        7D36BD8D2DA97B2CA4742EA47135A77135A66D35A66D33A56F34AB6E2B9C6239
        A874479976D5FAE6F5FCF7F3FEFCFFFFFFFFFFFFC0C0C0000000C0C0C0C5E9D9
        32BC9321C3932EB2832FA8762BAF7A35AB7635AC7532B17A21A87628B4853FC6
        9E25A88338BD952AB18527AB7C2DAA782FAB752FAB752FAB752BAD783AB07B32
        AA7330AF7B44A47FEAFFFBFCFFFDFFFFFFFFFFFFC0C0C0000000C0C0C0F7FFF5
        50B19731C0A02DC2A025B99122B0813BB18035AF7F26B58832C7A63BCEB292DC
        CAE6FFF744B79C2FBE9E27C5A028B28830B18429B68331AF7E45AF862BB5862B
        B58534B18328B28348A987DCFFF2FDFFF7FFFFFFC0C0C0000000C0C0C0F8FDFB
        DFFFFA5EB6A62CBFA314C9A323C29A34B99124B58F23C39F25CAAF52B0A5EFFF
        FFE4FFFAB6EBDE2FB69C21C8A71CBC9824B7912AB78C2CBA8D37B78E20BB8E25
        BA8E36B58E29B68D2DB78D48B08FCCFFEFF4FEF8C0C0C0000000C0C0C0FBFEFF
        FFFDFFDEFEF951BFAD1CD4B619CDB114BDA310C4A820D1B63EB8A8E4FEFEFBFA
        FEF4FFFFEAFFF9B4F2E432C0A921D7B920C3A730BB9A20C09623BD941EBE9423
        BD942CBA952ABE9825BA922DB8963AAF98FFFFFEC0C0C0000000C0C0C0FFF7FD
        FBFFFFF9FFFBF0FEF866BFB52EC9BB10D1C428D5C73CC2B6F4FFFFF6FFFFFEFD
        FFFFFBFFF6FFFAFFFFFEEBFEFF27BEB428D5C72DC4B126BCA42BC1A42BC1A327
        C2A31BBA9A2DC6A72ABEA02DBEA231BCA73DC8B3DEFFFB000000C0C0C0FFFEFF
        F8FFFCFBFFFBFFFFFBE0FFFB70CBC23ACBC256CBC2CEF8F7FFFEFAFAFFFBF6FF
        FCFFFEFFFFFCFCFFFDFCFFFCFFACECF046CAC45DE5D935BFAE28C8B022C2AA2C
        CAB22BC9B11FBFA71CC2A924CAB130CDB952E0CD75C9BD000000C0C0C0FDFFFE
        FFFEFEFFFFFEF4FFFFFAFFFFF2FFFDE2FFFEEEFFFFFBFFFFFFFDFCFAFFFCFFFF
        FFFFFFFFFFFFFFFFFFFFF7FFFEF9FDFEEFFFFF63D2CA7BE7E16DE7DD1EC2B021
        D0BC23CDBB18CAB71FC1AF5FE1D687EEE92DD1BF70CEC3000000C0C0C0FFFFFE
        FBFFFFFBFEFFFFFBFFFFFCFEF8FFFEF2FFF5FAFFFDF6FEFDFFFAFDFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFF6FCF7FDFEF5F6FFFCBBF2EF7ADBD7ABEAEE52D9CF16
        CAB810C9B917CFBF76E8E1ADEDF183E6E46FD6CEE3FFFF000000C0C0C0FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFFFFF1FFFFE5FCF87CD0CBC6FBFE9E
        F0F133C6BE85E7E7B2F7FA68CFCA75E3D7E8FDFEC0C0C0000000C0C0C0FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFCFCF4FFFDF1FFFDDDFFFF97E6E3AD
        FBFABDF7FCBCF6FB82DED97AE1DAD8FEFEFCFDFFC0C0C0000000C0C0C0FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFFFFF5FDFCFFFEFFD6F8F8AB
        EAE8C0F9FAA9F6F3ADEDE8F3FFFFFFFFFFFFFFFFC0C0C0000000C0C0C0FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFCFAFFFFF1
        FFFECCEEEDD7F7F6FAFFFFFFFFFFFFFFFFFFFFFFC0C0C0000000C0C0C0FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0000000C0C0C0FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0000000C0C0C0FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0000000C0C0C0FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0000000C0C0C0FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0000000C0C0C0FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0000000C0C0C0FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0000000C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0000000}
      GlyphUnChecked.Data = {
        560D0000424D560D00000000000036000000280000001F000000230000000100
        180000000000200D000000000000000000000000000000000000C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0000000C0C0C0FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0000000C0C0C0FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFCFCFCFAFAFAF5F5F5F4F4F4F1F1F1F2F2
        F2EFEFEFEEEEEEF4F4F4F3F3F3F2F2F2F3F3F3F7F7F7FAFAFAFCFCFCFDFDFDFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0000000C0C0C0FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFF9F9F9F5F5F5F1F1F1EBEBEBE6E6E6DDDDDDD1D1
        D1CCCCCCCCCCCCCCCCCCD1D1D1DBDBDBE6E6E6EAEAEAF0F0F0F4F4F4F9F9F9FD
        FDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0000000C0C0C0FFFFFF
        FFFFFEFFFFFFFFFEFFFDFEFFFAFCFDF9FAF8F5F7F1EEF1EFEFEFF5E3E2EBDBE3
        D9E3E2EBE3E2EBE3E2EBE4E0E5E7EBDFEBEFEAEEF0F1F6F7F5F9F9F9FEFEFEFE
        FEFEFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0000000C0C0C0FFFFFF
        FFFFFFFFFFFFFFFEFFFFFEFFFEFDFFFFFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFDFFFFFFF9FEFFFDFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0000000C0C0C0FFFFFF
        FFFFFFFFFFFFFFFFFEFFFFFEFFFFFFFFFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFFFFFEFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0000000C0C0C0FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F9F5FFFDFFFFFEFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0000000C0C0C0FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0000000C0C0C0FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0000000C0C0C0FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0000000C0C0C0FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0000000C0C0C0FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0000000C0C0C0FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0000000C0C0C0FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0000000C0C0C0FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0000000C0C0C0FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0000000C0C0C0FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0000000C0C0C0FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0000000C0C0C0FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0000000C0C0C0FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0000000C0C0C0FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0000000C0C0C0FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0000000C0C0C0FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0000000C0C0C0FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0000000C0C0C0FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0000000C0C0C0FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0000000C0C0C0FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0000000C0C0C0FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0000000C0C0C0FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0000000C0C0C0FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0000000C0C0C0FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0000000C0C0C0FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0000000C0C0C0FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0000000C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0000000}
      Caption = #1060#1080#1083#1100#1090#1088
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = cxCheckBox1Click
    end
  end
  object FormPlacement1: TFormPlacement
    Active = False
    Left = 408
    Top = 240
  end
  object amSpec: TActionManager
    Left = 232
    Top = 248
    StyleName = 'XP Style'
    object acMenu: TAction
      Caption = 'acMenu'
      ShortCut = 107
      OnExecute = acMenuExecute
    end
    object acChangeQuantity: TAction
      Caption = 'acChangeQuantity'
      ShortCut = 115
      OnExecute = acChangeQuantityExecute
    end
    object acDelPos: TAction
      Caption = 'acDelPos'
      ShortCut = 109
      OnExecute = acDelPosExecute
    end
    object acMovePos: TAction
      Caption = 'acMovePos'
      ShortCut = 117
      OnExecute = acMovePosExecute
    end
    object acPrePrint: TAction
      Caption = 'acPrePrint'
      ShortCut = 114
      OnExecute = acPrePrintExecute
    end
    object acModify: TAction
      Caption = 'acModify'
      OnExecute = acModifyExecute
    end
    object Action1: TAction
      Caption = 'Action1'
      ShortCut = 112
      OnExecute = Action1Execute
    end
    object Action2: TAction
      Caption = 'Action2'
      ShortCut = 113
      OnExecute = Action2Execute
    end
    object Action5: TAction
      Caption = 'Action5'
      ShortCut = 116
      OnExecute = Action5Execute
    end
    object Action7: TAction
      Caption = 'Action7'
      ShortCut = 118
      OnExecute = Action7Execute
    end
    object Action8: TAction
      Caption = 'Action8'
      ShortCut = 119
      OnExecute = Action8Execute
    end
    object Action9: TAction
      Caption = 'Action9'
      ShortCut = 120
      OnExecute = Action9Execute
    end
    object Action10: TAction
      Caption = #1042#1099#1093#1086#1076
      ShortCut = 121
      OnExecute = Action10Execute
    end
    object acAddMod: TAction
      Caption = #1044#1086#1073'.'#1052#1086#1076#1080#1092'.'
      ShortCut = 122
      OnExecute = acAddModExecute
    end
    object acDelMod: TAction
      Caption = #1059#1076#1083'. '#1052#1086#1076#1080#1092'.'
      ShortCut = 123
      OnExecute = acDelModExecute
    end
    object acKod: TAction
      Caption = #1055#1086' '#1082#1086#1076#1091
      OnExecute = acKodExecute
    end
    object acDiscount: TAction
      Caption = 'acDiscount'
      ShortCut = 186
      OnExecute = acDiscountExecute
    end
    object acExitN: TAction
      Caption = 'acExitN'
      OnExecute = acExitNExecute
    end
    object acNumTab: TAction
      Caption = #1053#1086#1084#1077#1088' '#1079#1072#1082#1072#1079#1072
      OnExecute = acNumTabExecute
    end
    object acAddMe: TAction
      Caption = #1044#1086#1073'. '#1089#1086#1086#1073#1097#1077#1085#1080#1077
      OnExecute = acAddMeExecute
    end
  end
  object Timer1: TTimer
    Interval = 3000
    OnTimer = Timer1Timer
    Left = 408
    Top = 184
  end
  object frRepSp: TfrReport
    InitialZoom = pzDefault
    PreviewButtons = [pbZoom, pbLoad, pbSave, pbPrint, pbFind, pbHelp, pbExit]
    ShowPrintDialog = False
    ShowProgress = False
    RebuildPrinter = False
    Left = 168
    Top = 136
    ReportForm = {19000000}
  end
  object frquCheck: TfrDBDataSet
    DataSet = dmC.quCheck
    OpenDataSource = False
    Left = 253
    Top = 141
  end
  object Timer2: TTimer
    Enabled = False
    Interval = 100
    OnTimer = Timer2Timer
    Left = 64
    Top = 188
  end
end
