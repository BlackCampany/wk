object fmMainCashRn: TfmMainCashRn
  Tag = 1
  Left = 176
  Top = 424
  Width = 803
  Height = 593
  Align = alTop
  BorderIcons = []
  Caption = #1050#1072#1089#1089#1086#1074#1099#1081' '#1084#1086#1076#1091#1083#1100
  Color = clGray
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCanResize = FormCanResize
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object cxButton12: TcxButton
    Left = 636
    Top = 379
    Width = 145
    Height = 35
    Caption = #1057#1090#1086#1087' '#1083#1080#1089#1090
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 17
    Visible = False
    WordWrap = True
    OnClick = cxButton12Click
    Colors.Default = 14531583
    Colors.Normal = 14531583
    Colors.Pressed = 8388863
    Layout = blGlyphTop
    LookAndFeel.Kind = lfFlat
  end
  object cxButton11: TcxButton
    Left = 636
    Top = 334
    Width = 145
    Height = 36
    Caption = #1057#1086#1086#1073#1097#1077#1085#1080#1077
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 15
    Visible = False
    OnClick = cxButton11Click
    Colors.Default = 14864576
    Colors.Normal = 12621940
    Colors.Pressed = 10843723
    Layout = blGlyphTop
    LookAndFeel.Kind = lfFlat
  end
  object cxButton6: TcxButton
    Left = 637
    Top = 291
    Width = 144
    Height = 35
    Caption = #1042#1089#1077' '#1079#1072#1082#1072#1079#1099' ('#1086#1073#1085#1086#1074#1080#1090#1100')'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 13
    Visible = False
    WordWrap = True
    OnClick = cxButton6Click
    Colors.Default = 8453888
    Colors.Normal = 8453888
    Colors.Pressed = clGreen
    Layout = blGlyphTop
    LookAndFeel.Kind = lfFlat
  end
  object Panel1: TPanel
    Left = 628
    Top = 4
    Width = 165
    Height = 189
    BevelInner = bvLowered
    Color = 2434341
    TabOrder = 0
    object Label8: TLabel
      Left = 16
      Top = 16
      Width = 7
      Height = 11
      Caption = 'l8'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 15456197
      Font.Height = -11
      Font.Name = 'MS Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Label9: TLabel
      Left = 16
      Top = 40
      Width = 7
      Height = 11
      Caption = 'l9'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 15456197
      Font.Height = -11
      Font.Name = 'MS Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Label2: TLabel
      Left = 16
      Top = 64
      Width = 7
      Height = 11
      Caption = 'l2'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 15456197
      Font.Height = -11
      Font.Name = 'MS Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Label3: TLabel
      Left = 16
      Top = 112
      Width = 7
      Height = 11
      Caption = 'l3'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 15456197
      Font.Height = -11
      Font.Name = 'MS Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Label4: TLabel
      Left = 16
      Top = 88
      Width = 7
      Height = 11
      Caption = 'l4'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 15456197
      Font.Height = -11
      Font.Name = 'MS Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Label5: TLabel
      Left = 16
      Top = 160
      Width = 7
      Height = 11
      Caption = 'l5'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 15456197
      Font.Height = -11
      Font.Name = 'MS Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Label6: TLabel
      Left = 16
      Top = 136
      Width = 7
      Height = 11
      Caption = 'l6'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 15456197
      Font.Height = -11
      Font.Name = 'MS Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Label12: TLabel
      Left = 151
      Top = 172
      Width = 10
      Height = 11
      Alignment = taRightJustify
      Caption = 'l13'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = 15456197
      Font.Height = -9
      Font.Name = 'MS Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Label14: TLabel
      Left = 123
      Top = 172
      Width = 20
      Height = 11
      Alignment = taRightJustify
      Caption = 'v3.12'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = 15456197
      Font.Height = -9
      Font.Name = 'MS Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
  end
  object Panel2: TPanel
    Left = 636
    Top = 196
    Width = 146
    Height = 41
    BevelInner = bvLowered
    Color = 12303291
    TabOrder = 1
    OnClick = Panel2Click
    object Label13: TLabel
      Left = 4
      Top = 13
      Width = 137
      Height = 16
      Alignment = taCenter
      AutoSize = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      OnClick = Panel2Click
    end
  end
  object RxClock1: TRxClock
    Left = 652
    Top = 512
    Width = 129
    Height = 41
    Color = clBlack
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 14927276
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    OnClick = RxClock1Click
  end
  object Panel3: TPanel
    Left = 4
    Top = 4
    Width = 621
    Height = 493
    BevelInner = bvLowered
    Color = 6832674
    TabOrder = 2
    Visible = False
    object Label1: TLabel
      Left = 16
      Top = 8
      Width = 78
      Height = 12
      Caption = #1054#1090#1082#1088#1099#1090#1099#1077' '#1079#1072#1082#1072#1079#1099
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 15456197
      Font.Height = -9
      Font.Name = 'Microsoft Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object GridTab: TcxGrid
      Tag = 1
      Left = 8
      Top = 20
      Width = 605
      Height = 465
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
      object PersView: TcxGridDBTableView
        NavigatorButtons.ConfirmDelete = False
        OnCellClick = PersViewCellClick
        DataController.DataSource = dmC.dsPers
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsSelection.HideFocusRectOnExit = False
        OptionsSelection.InvertSelect = False
        OptionsSelection.UnselectFocusedRecordOnExit = False
        OptionsView.GroupByBox = False
        Styles.Header = dmC.cxStyle122
        object PersViewNAME: TcxGridDBColumn
          Caption = #1048#1084#1103
          DataBinding.FieldName = 'NAME'
          Styles.Content = dmC.cxStyle3
          Width = 136
        end
        object PersViewsCountTab: TcxGridDBColumn
          Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077
          DataBinding.FieldName = 'sCountTab'
          Styles.Content = dmC.cxStyle3
          Width = 250
        end
      end
      object TabView: TcxGridDBCardView
        Synchronization = False
        OnDblClick = TabViewDblClick
        NavigatorButtons.ConfirmDelete = False
        FilterBox.CustomizeDialog = False
        OnCellClick = TabViewCellClick
        OnCustomDrawCell = TabViewCustomDrawCell
        DataController.DataSource = dmC.dsTabs
        DataController.DetailKeyFieldNames = 'ID_PERSONAL'
        DataController.KeyFieldNames = 'ID'
        DataController.MasterKeyFieldNames = 'ID_PERSONAL'
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        LayoutDirection = ldVertical
        OptionsCustomize.RowFiltering = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsSelection.HideFocusRectOnExit = False
        OptionsSelection.InvertSelect = False
        OptionsView.CardWidth = 100
        OptionsView.SeparatorColor = 16713471
        OptionsView.SeparatorWidth = 5
        Styles.Background = dmC.cxStyle111
        Styles.CaptionRow = dmC.cxStyle112
        Styles.CardBorder = dmC.cxStyle103
        object TabViewSSTAT: TcxGridDBCardViewRow
          Caption = #1057#1090#1086#1083
          DataBinding.FieldName = 'SSTAT'
          Styles.Content = dmC.cxStyle113
        end
        object TabViewQUESTS: TcxGridDBCardViewRow
          Caption = #1043#1086#1089#1090#1077#1081
          DataBinding.FieldName = 'QUESTS'
          Styles.Content = dmC.cxStyle100
        end
        object TabViewNAME: TcxGridDBCardViewRow
          Caption = #1043#1086#1089#1090#1100
          DataBinding.FieldName = 'NAME'
          Styles.Content = dmC.cxStyle126
        end
        object TabViewTABSUM: TcxGridDBCardViewRow
          Caption = #1057#1091#1084#1084#1072
          DataBinding.FieldName = 'TABSUM'
          Styles.Content = dmC.cxStyle112
        end
        object TabViewSTIME: TcxGridDBCardViewRow
          Caption = #1054#1090#1082#1088#1099#1090
          DataBinding.FieldName = 'STIME'
          Styles.Content = dmC.cxStyle100
        end
      end
      object PersLevel: TcxGridLevel
        GridView = PersView
        object TabLevel: TcxGridLevel
          GridView = TabView
        end
      end
    end
  end
  object Button1: TcxButton
    Left = 16
    Top = 508
    Width = 81
    Height = 45
    Caption = #1057#1086#1079#1076#1072#1090#1100
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -8
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    Visible = False
    OnClick = Button1Click
    Colors.Default = 14864576
    Colors.Normal = 12621940
    Colors.Pressed = 10843723
    Layout = blGlyphTop
    LookAndFeel.Kind = lfFlat
  end
  object Button2: TcxButton
    Left = 104
    Top = 508
    Width = 81
    Height = 45
    Caption = #1054#1090#1082#1088#1099#1090#1100
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -8
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
    Visible = False
    OnClick = Button2Click
    Colors.Default = 14864576
    Colors.Normal = 12621940
    Colors.Pressed = 10843723
    Layout = blGlyphTop
    LookAndFeel.Kind = lfFlat
  end
  object Button3: TcxButton
    Left = 192
    Top = 508
    Width = 81
    Height = 45
    Caption = #1059#1076#1072#1083#1080#1090#1100
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -8
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
    Visible = False
    OnClick = Button3Click
    Colors.Default = 14864576
    Colors.Normal = 12621940
    Colors.Pressed = 10843723
    Layout = blGlyphTop
    LookAndFeel.Kind = lfFlat
  end
  object Button4: TcxButton
    Left = 280
    Top = 508
    Width = 81
    Height = 45
    Caption = #1055#1077#1088#1077#1084#1077#1089#1090#1080#1090#1100
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -8
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 6
    Visible = False
    OnClick = Button4Click
    Colors.Default = 14864576
    Colors.Normal = 12621940
    Colors.Pressed = 10843723
    Layout = blGlyphTop
    LookAndFeel.Kind = lfFlat
  end
  object Button5: TcxButton
    Left = 632
    Top = 448
    Width = 145
    Height = 42
    Caption = #1056#1072#1089#1095#1077#1090
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 8
    Visible = False
    OnClick = Button5Click
    Colors.Default = 14864576
    Colors.Normal = 12621940
    Colors.Pressed = 10843723
    Layout = blGlyphTop
    LookAndFeel.Kind = lfFlat
  end
  object Button6: TcxButton
    Left = 368
    Top = 508
    Width = 81
    Height = 45
    Action = acRetPre
    Caption = #1054#1090#1084#1077#1085#1072' '#1055#1088#1063
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -8
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 7
    Visible = False
    Colors.Default = 14864576
    Colors.Normal = 12621940
    Colors.Pressed = 10843723
    Layout = blGlyphTop
    LookAndFeel.Kind = lfFlat
  end
  object cxButton1: TcxButton
    Left = 636
    Top = 242
    Width = 145
    Height = 36
    Caption = #1050#1072#1089#1089#1086#1074#1099#1077' '#1086#1090#1095#1077#1090#1099
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 9
    Visible = False
    OnClick = cxButton1Click
    Colors.Default = 14864576
    Colors.Normal = 12621940
    Colors.Pressed = 10843723
    Layout = blGlyphTop
    LookAndFeel.Kind = lfFlat
  end
  object Panel4: TPanel
    Left = 636
    Top = 280
    Width = 146
    Height = 273
    BevelInner = bvLowered
    Color = 6832674
    TabOrder = 10
    Visible = False
    object cxButton2: TcxButton
      Left = 8
      Top = 8
      Width = 130
      Height = 25
      Caption = 'X-'#1086#1090#1095#1077#1090' ('#1086#1090#1095#1077#1090' '#1086' '#1074#1099#1088#1091#1095#1082#1077')'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = cxButton2Click
      Colors.Default = 14864576
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      Layout = blGlyphTop
      LookAndFeel.Kind = lfFlat
    end
    object cxButton3: TcxButton
      Left = 8
      Top = 40
      Width = 130
      Height = 25
      Caption = 'Z-'#1086#1090#1095#1077#1090' ('#1079#1072#1082#1088#1099#1090#1100' '#1089#1084#1077#1085#1091')'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = cxButton3Click
      Colors.Default = 14864576
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      Layout = blGlyphTop
      LookAndFeel.Kind = lfFlat
    end
    object cxButton4: TcxButton
      Left = 20
      Top = 200
      Width = 109
      Height = 25
      Caption = #1040#1085#1085#1091#1083#1080#1088#1086#1074#1072#1090#1100' '#1095#1077#1082
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      Visible = False
      OnClick = cxButton4Click
      Colors.Default = 14864576
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      Layout = blGlyphTop
      LookAndFeel.Kind = lfFlat
    end
    object cxButton5: TcxButton
      Left = 20
      Top = 232
      Width = 109
      Height = 25
      Caption = #1054#1092#1086#1088#1084#1080#1090#1100' '#1074#1086#1079#1074#1088#1072#1090
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      OnClick = cxButton5Click
      Colors.Default = 14864576
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      Layout = blGlyphTop
      LookAndFeel.Kind = lfFlat
    end
    object cxButton7: TcxButton
      Left = 8
      Top = 136
      Width = 130
      Height = 25
      Caption = #1057#1073#1077#1088#1073#1072#1085#1082
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      OnClick = cxButton7Click
      Colors.Default = 14864576
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      Layout = blGlyphTop
      LookAndFeel.Kind = lfFlat
    end
    object cxButton8: TcxButton
      Left = 8
      Top = 72
      Width = 130
      Height = 25
      Caption = #1042#1099#1088#1091#1095#1082#1072' '#1087#1086' '#1086#1092#1080#1094#1080#1072#1085#1090#1072#1084
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 5
      OnClick = cxButton8Click
      Colors.Default = 14864576
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      Layout = blGlyphTop
      LookAndFeel.Kind = lfFlat
    end
    object cxButton9: TcxButton
      Left = 8
      Top = 104
      Width = 130
      Height = 25
      Caption = #1054#1090#1082#1088#1099#1090#1100' '#1044#1071
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 6
      OnClick = cxButton9Click
      Colors.Default = 14864576
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      Layout = blGlyphTop
      LookAndFeel.Kind = lfFlat
    end
    object cxButton10: TcxButton
      Left = 8
      Top = 168
      Width = 130
      Height = 25
      Caption = #1048#1085#1082#1072#1089#1089#1072#1094#1080#1103
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 7
      OnClick = cxButton10Click
      Colors.Default = 14864576
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      Layout = blGlyphTop
      LookAndFeel.Kind = lfFlat
    end
    object cxButton13: TcxButton
      Left = 8
      Top = 200
      Width = 129
      Height = 25
      Caption = #1050#1086#1087#1080#1103' '#1095#1077#1082#1072' ('#1090#1086#1074'.'#1095#1077#1082')'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 8
      OnClick = cxButton13Click
      Colors.Default = 14864576
      Colors.Normal = 12621940
      Colors.Pressed = 10843723
      Layout = blGlyphTop
      LookAndFeel.Kind = lfFlat
    end
  end
  object Button7: TcxButton
    Left = 456
    Top = 508
    Width = 73
    Height = 45
    Action = acPrintPre
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -8
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 11
    Visible = False
    WordWrap = True
    Colors.Default = 14864576
    Colors.Normal = 12621940
    Colors.Pressed = 10843723
    Layout = blGlyphTop
    LookAndFeel.Kind = lfFlat
  end
  object Button8: TcxButton
    Left = 536
    Top = 508
    Width = 73
    Height = 45
    Action = acCashPCard
    Caption = #1054#1087#1083#1072#1090#1072' '#1041#1053
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 12
    Visible = False
    WordWrap = True
    Colors.Default = 9943551
    Colors.Normal = 9943551
    Colors.Pressed = 4227327
    Layout = blGlyphTop
    LookAndFeel.Kind = lfFlat
  end
  object Panel5: TPanel
    Left = 12
    Top = 12
    Width = 305
    Height = 129
    BevelInner = bvLowered
    Color = 6832674
    TabOrder = 14
    Visible = False
    object dxfLabel1: TdxfLabel
      Left = 56
      Top = 8
      Width = 185
      Height = 25
      AutoSize = False
      Caption = #1057#1077#1075#1086#1076#1085#1103' '#1083#1091#1095#1096#1080#1077
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 13276516
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Style = dxfNormal
      Angle = 0
      Effect3D.Style = dxfFun
      Effect3D.Orientation = dxfRightBottom
      Effect3D.Depth = 3
      Effect3D.ShadowedColor = 15456197
      PenWidth = 1
    end
    object Label7: TLabel
      Left = 8
      Top = 44
      Width = 105
      Height = 13
      AutoSize = False
      Caption = #1048#1074#1072#1085#1086#1074
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 15456197
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Label10: TLabel
      Left = 8
      Top = 68
      Width = 105
      Height = 13
      AutoSize = False
      Caption = #1048#1074#1072#1085#1086#1074
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 15456197
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Label11: TLabel
      Left = 8
      Top = 92
      Width = 105
      Height = 13
      AutoSize = False
      Caption = #1048#1074#1072#1085#1086#1074
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 15456197
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object PBar1: TcxProgressBar
      Left = 112
      Top = 40
      Position = 100.000000000000000000
      Properties.AnimationPath = cxapPingPong
      Properties.AnimationSpeed = 3
      Properties.BarStyle = cxbsAnimation
      Properties.BeginColor = 54056
      Properties.PeakValue = 100.000000000000000000
      Properties.ShowText = False
      TabOrder = 0
      Width = 180
    end
    object PBar2: TcxProgressBar
      Left = 112
      Top = 64
      Position = 100.000000000000000000
      Properties.AnimationPath = cxapPingPong
      Properties.AnimationSpeed = 3
      Properties.BarStyle = cxbsAnimation
      Properties.BeginColor = 54056
      Properties.PeakValue = 100.000000000000000000
      Properties.ShowText = False
      TabOrder = 1
      Width = 180
    end
    object PBar3: TcxProgressBar
      Left = 112
      Top = 88
      Position = 100.000000000000000000
      Properties.AnimationPath = cxapPingPong
      Properties.AnimationSpeed = 3
      Properties.BarStyle = cxbsAnimation
      Properties.BeginColor = 54056
      Properties.PeakValue = 100.000000000000000000
      Properties.ShowText = False
      TabOrder = 2
      Width = 180
    end
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 500
    OnTimer = Timer1Timer
    Left = 354
    Top = 152
  end
  object amRn: TActionManager
    Left = 282
    Top = 253
    StyleName = 'XP Style'
    object acCreateTab: TAction
      Caption = 'acCreateTab'
      ShortCut = 107
      OnExecute = acCreateTabExecute
    end
    object acDel: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1079#1072#1082#1072#1079
      ShortCut = 109
      OnExecute = acDelExecute
    end
    object acMove: TAction
      Caption = #1055#1077#1088#1077#1084#1077#1089#1090#1080#1090#1100
      ShortCut = 117
      OnExecute = acMoveExecute
    end
    object acRetPre: TAction
      Caption = 'acRetPre'
      ShortCut = 120
      OnExecute = acRetPreExecute
    end
    object acCashEnd: TAction
      Caption = 'acCashEnd'
      ShortCut = 116
      OnExecute = acCashEndExecute
    end
    object acCashRep: TAction
      Caption = #1050#1072#1089#1089#1086#1074#1099#1077' '#1086#1090#1095#1077#1090#1099
      ShortCut = 118
      OnExecute = acCashRepExecute
    end
    object acExit: TAction
      Caption = 'Exit'
      ShortCut = 121
      OnExecute = acExitExecute
    end
    object acOpen: TAction
      Caption = 'acOpen'
      ShortCut = 115
      OnExecute = acOpenExecute
    end
    object acPrintPre: TAction
      Caption = #1054#1092#1086#1088#1084#1080#1090#1100' '#1057#1095#1077#1090
      ShortCut = 113
      OnExecute = acPrintPreExecute
    end
    object acCashPCard: TAction
      Caption = 'acCashPCard'
      OnExecute = acCashPCardExecute
    end
    object acRecalcCount: TAction
      Caption = #1055#1077#1088#1077#1095#1080#1090#1072#1090#1100' '#1080#1090#1086#1075#1086#1074#1099#1077' '#1089#1095#1077#1090#1095#1080#1082#1080
      ShortCut = 49234
      OnExecute = acRecalcCountExecute
    end
    object Action1: TAction
      Caption = #1058#1077#1089#1090' '#1096#1088#1080#1092#1090#1086#1074
      ShortCut = 49222
      OnExecute = Action1Execute
    end
    object acViewTab: TAction
      OnExecute = acViewTabExecute
    end
  end
  object dxfBackGround1: TdxfBackGround
    BkColor.BeginColor = 10921638
    BkColor.EndColor = clBlack
    BkColor.FillStyle = fsVert
    BkAnimate.Speed = 700
    Left = 304
    Top = 360
  end
  object frRepMain: TfrReport
    InitialZoom = pzDefault
    PreviewButtons = [pbZoom, pbLoad, pbSave, pbPrint, pbFind, pbHelp, pbExit]
    ShowPrintDialog = False
    ShowProgress = False
    RebuildPrinter = False
    Left = 479
    Top = 93
    ReportForm = {19000000}
  end
  object frquCheck: TfrDBDataSet
    DataSet = dmC.quCheck
    Left = 554
    Top = 93
  end
  object TimerDelayPrintQ: TTimer
    Enabled = False
    OnTimer = TimerDelayPrintQTimer
    Left = 418
    Top = 197
  end
  object TiRefresh: TTimer
    Enabled = False
    OnTimer = TiRefreshTimer
    Left = 418
    Top = 253
  end
  object TimerPrintQ: TTimer
    Enabled = False
    Interval = 3000
    OnTimer = TimerPrintQTimer
    Left = 418
    Top = 141
  end
  object Timer2: TTimer
    Left = 492
    Top = 312
  end
  object TimerClose: TTimer
    Enabled = False
    Interval = 5000
    OnTimer = TimerCloseTimer
    Left = 368
    Top = 320
  end
end
