object fmCalcSeb: TfmCalcSeb
  Left = 325
  Top = 325
  Width = 594
  Height = 476
  Caption = #1057#1077#1073#1077#1089#1090#1086#1080#1084#1086#1089#1090#1100
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 586
    Height = 113
    Align = alTop
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 0
    object Label2: TLabel
      Left = 24
      Top = 8
      Width = 33
      Height = 13
      Caption = #1041#1083#1102#1076#1086
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
      Transparent = True
    end
    object Label3: TLabel
      Left = 24
      Top = 32
      Width = 74
      Height = 13
      Caption = #1041#1088#1091#1090#1090#1086' '#1085#1072' '#1076#1072#1090#1091
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
      Transparent = True
    end
    object Label4: TLabel
      Left = 112
      Top = 8
      Width = 233
      Height = 13
      AutoSize = False
      Caption = 'Label4'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label1: TLabel
      Left = 112
      Top = 32
      Width = 39
      Height = 13
      Caption = 'Label1'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label5: TLabel
      Left = 248
      Top = 80
      Width = 73
      Height = 13
      Caption = #1062#1077#1085#1072' '#1087#1088#1086#1076#1072#1078#1080
    end
    object Label6: TLabel
      Left = 64
      Top = 8
      Width = 41
      Height = 13
      Alignment = taCenter
      AutoSize = False
      Caption = 'Label6'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object cxButton2: TcxButton
      Left = 480
      Top = 48
      Width = 83
      Height = 25
      Caption = #1042#1099#1093#1086#1076
      TabOrder = 0
      OnClick = cxButton2Click
      Colors.Default = 16776176
      Colors.Normal = 16776176
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton1: TcxButton
      Left = 480
      Top = 8
      Width = 83
      Height = 25
      Caption = #1056#1072#1089#1095#1080#1090#1072#1090#1100
      TabOrder = 1
      OnClick = cxButton1Click
      Colors.Default = 16776176
      Colors.Normal = 16776176
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton3: TcxButton
      Left = 24
      Top = 64
      Width = 65
      Height = 41
      TabOrder = 2
      OnClick = cxButton3Click
      Glyph.Data = {
        56080000424D560800000000000036000000280000001A0000001A0000000100
        18000000000020080000C40E0000C40E00000000000000000000C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0004000004000004000004000C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C00000C0C0C0004000
        80808080808080808080808080808080808080808000400040E02040E020FFFF
        FFC8D0D4A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0808080C0
        C0C0C0C0C0C0C0C00000C0C0C0C0C0C000400040E02000C02000C02000C02000
        C02000400040E02040E020FFFFFF004000C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4A4A0A0A4A0A0404040C0C0C0C0C0C00000C0C0C0C0C0C0
        C0C0C000400000C02000C02000C02000400000C02040E020FFFFFF00400000C0
        20C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4A4A0A0A4A0A040
        2020808080C0C0C00000C0C0C0C0C0C0C0C0C0A4A0A000400000C02000400000
        C02040E020FFFFFF004000004000004000C8D0D4A4A0A0A4A0A0A4A0A0A4A0A0
        A4A0A0A4A0A0808080C8D0D4808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C000400040E02040E020FFFFFF004000F0FBFFF0FBFFF0FB
        FFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFA4A0A0C8D0D4C8D0D440
        2020808080C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C000400040E02040E020FF
        FFFF004000808080004000C8D0D4C0DCC0C0DCC0C0DCC0C0DCC0C0DCC0C0DCC0
        C0DCC0C8D0D4F0FBFFA4A0A0C8D0D4402020808080C0C0C00000C0C0C0C0C0C0
        C0C0C000400000C02040E020FFFFFF00400080E0A000C020808080004000FFFF
        FFFFFFFFFFFFFFF0FBFFC0DCC0C0DCC0F0FBFFC0DCC0F0FBFF808080C8D0D440
        2020808080C0C0C00000C0C0C0C0C0C000400000C02040E020FFFFFF004000FF
        FFFF00400000C02000C020808080004000FFFFFFFFFFFFC8D0D4F0FBFFC0DCC0
        C0DCC0C0DCC0F0FBFFC0DCC0A4A0A0404020808080C0C0C00000C0C0C0004000
        004000004000004000FFFFFFFFFFFFFFFFFFFFFFFF0040000040000040000040
        00C8D0D4F0FBFFC0DCC0C8D0D4C8D0D4F0FBFFC0DCC0F0FBFFFFFFFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFC0DCC0FFFFFFF0FBFFFFFFFFFFFFFFF0FBFFF0FBFFF0FBFF
        FFFFFFC0DCC0F0FBFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0F0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FB
        FFF0FBFFFFFFFFFFFFFFF0FBFFF0FBFFFFFFFFC0DCC0F0FBFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0F0
        FBFFF0FBFFF0FBFFC0DCC0C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4F0FBFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0C0DCC0C0DCC0F0FBFFF0FBFFC0DCC0FFFF
        FFC0DCC0F0FBFFC0DCC0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0C0
        DCC0F0FBFFF0FBFFF0FBFFF0FBFFFFFFFFF0FBFFF0FBFFC0DCC0F0FBFFFFFFFF
        FFFFFFFFFFFFFFFFFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0C0DCC0C8D0D4F0FBFFF0FBFFC8D0D4FFFF
        FFC8D0D4F0FBFFC0DCC0F0FBFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0F0
        FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFC8D0D4F0FBFFFFFFFF
        FFFFFFFFFFFFFFFFFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFF0FBFFC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4C8D0D4C8D0D4C8D0D4F0FBFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFF0FBFFF0
        FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080C0DCC0C8D0D4C8D0D4F0FBFFC8D0D4C0DCC0C0DCC0C8D0D4F0FB
        FFC8D0D4C0DCC0F0FBFFC8D0D4F0FBFFC8D0D4C8D0D4F0FBFFA4A0A080808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C080808080E0E040E0E040E0E080
        E0E040E0E080E0E080E0E040E0E080E0E040E0E040E0E080E0E040E0E080E0E0
        40E0E040E0E080E0E000E0E0408080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C080808080E0E080E0E0C0DCC080E0E0C0DCC080E0E080E0E0C0DCC080E0
        E0C0DCC080E0E080E0E0C0DCC080E0E0C0DCC080E0E080E0E0C0DCC000808000
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080F0FBFFF0FBFFF0FBFF80
        E0E0F0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFF
        F0FBFFF0FBFFF0FBFFF0FBFF00C0C0002040808080C0C0C00000C0C0C0C0C0C0
        C0C0C08080808080808080808080808080808080808080808080808080808080
        8080808080808080808080808080808080808080808080808080808080808000
        2040C0C0C0C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C00000}
      LookAndFeel.Kind = lfOffice11
    end
    object cxRadioButton1: TcxRadioButton
      Left = 352
      Top = 12
      Width = 113
      Height = 17
      Caption = #1042' '#1094#1077#1085#1072#1093' '#1055'.'#1055'.'
      Checked = True
      TabOrder = 3
      TabStop = True
    end
    object cxRadioButton2: TcxRadioButton
      Left = 352
      Top = 28
      Width = 113
      Height = 17
      Caption = #1055#1086' '#1089#1077#1073'-'#1090#1080' (FiFo)'
      TabOrder = 4
    end
    object cxButton4: TcxButton
      Left = 104
      Top = 64
      Width = 65
      Height = 41
      TabOrder = 5
      OnClick = cxButton4Click
      Glyph.Data = {
        86070000424D86070000000000003600000028000000180000001A0000000100
        1800000000005007000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0BDB2
        B3B0A7FFFFFFFFFFFFD7D7D7D7D7D7D7D7D7FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFB0AEA8989896838280898883A9A7A0B4B3AA93928D85848096948EAFADA5
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFA1A09BAEAEADC9C9C8ABABAA84848371706D767573A2A2A1A4
        A4A376767472716D93918CACAAA3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFB8B7B0A3A3A0D8D8D8E7E7E7D5D5D5C7C7C7A9A9A98585
        85A0A09FA8A8A8BABABAD7D7D7C3C3C38686856E6D6B8B8A85FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFA5A4A0C6C6C5F6F6F6F8F8F8E2E2E2D2D2D2
        C6C6C6AEAEAE787878777777929292A4A4A4B3B3B3C9C9C9E2E2E2D9D9D9A1A1
        A07D7C7AB0AEA8FFFFFFFFFFFFFFFFFFFFFFFFADADABE7E7E7FDFDFDFEFEFEF4
        F4F4E3E3E3D4D4D4C3C3C3B7B7B79E9E9E888888828282929292AAAAAABDBDBD
        C6C6C6BDBDBDA1A1A181817FBEBDB6FFFFFFFFFFFFFFFFFFBCBBB9FDFDFDFFFF
        FFFFFFFFFFFFFFF3F3F3D7D7D7C9C9C9BEBEBEB6B6B6C4C4C4D0D0D0C6C6C6AB
        ABAB9B9B9B9090905353537E7E7E7C7C7B9B9A96FFFFFFFFFFFFFFFFFFFFFFFF
        BCBCBAFFFFFFFFFFFFFBFBFBE9E9E9D7D7D7D4D4D4D7D7D7D1D1D1C8C8C8C0C0
        C0C3C3C3CECECEDBDBDBD9D9D9BBBBBB8D8D8D9F9D9E878686979692FFFFFFFF
        FFFFFFFFFFFFFFFFBDBDBBFDFDFDE9E9E9D8D8D8DEDEDEE4E4E4E0E0E0DBDBDB
        D6D6D6CFCFCFC9C9C9C2C2C2BBBBBBBABABAC1C1C1C9C9C9C5C1C448FF737D94
        87979692FFFFFFFFFFFFFFFFFFFFFFFFBEBEBBEAEAEAE5E5E5EEEEEEEBEBEBE3
        E3E3E7E7E7F1F1F1EAEAEADCDCDCCFCFCFC6C6C6BFBFBFB6B6B6B0B0B0B2B2B2
        B4B3B3A7B7AE8C9A93979592FFFFFFFFFFFFFFFFFFFFFFFFBFBEBDFEFEFEF7F7
        F7EEEEEEEBEBEBF2F2F2F9F9F9E6E6E6D8D8D8DCDCDCDCDCDCD5D5D5CCCCCCC0
        C0C0B7B7B7B3B3B3B2B2B2B6B2B4B2ADAFA09F9DFFFFFFFFFFFFFFFFFFFFFFFF
        C0C0C0FBFBFBF8F8F8F7F7F7FBFBFBF7F7F7DFDFDFD6D6D6E6E6E6E5E5E5E1E1
        E1DDDDDDD7D7D7D0D0D0C6C6C6BEBEBEB8B8B8B3B3B3B0B0B0B8B7B4FFFFFFFF
        FFFFFFFFFFFFFFFFDCDCDBDCDCDBF7F7F7F4F4F4E5E5E5D2D2D2D1D1D1E0E0E0
        E9E8E8F1F1F1F9F8F8FCFCFCFEFEFEF4F4F4EBEBEBDADADABABABAB3B3B3B1B1
        B0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE0DFDDCFCFCED5D6D7D7
        D8DAD7D9DBCED0D2CED0D2D5D5D7DCDDDDE9E9E9F3F4F4FFFFFFFBFBFBDADADA
        B3B3B3BABAB9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFDDDCDBEEECEBE6E1DADFD9CFD4D0CAC8C7C4BCBCBCB3B4B7B4B6BABEC1C2D3
        D3D3C3C2C2C1C1C0DBDBD9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFE7D5C6F4E5C7F4E0BEF2DDBBECD8B7E4D2B4D9C8
        B0CCBDABAFA9A6B2B2B2E0DFDDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2D7C3FFF5D3FFEBC7FFE9C1
        FFE6B8FFE3B1FFE3AFFED9AAAD9B95D7D7D5FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE5E1E0F7E1CEFF
        F4D9FFECCDFFEAC7FFE7C0FFE4B8FFE6B4FCD9ACA79B98FFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFDED3D0FEF0DFFFF4DCFFEFD3FFECCDFFE9C6FFE6C0FFEABBF3D2ADB1A8A6FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFE7D5D0FFFBECFFF4E1FFF1DBFFEED4FFEBCDFFE9C6FFEE
        C1DBBEA6DCDADAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFE6E3E3F7EBE5FFFEF4FFF5E7FFF3E1FFF1DA
        FFEED3FFEDCDFFEFC8C0A89EDCDADAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE4D9D9FFFEFDFFFEF9FF
        F9EFFFF6E8FFF3E1FFF0D9FFF4D6FAE5C8C2B5B3F3F3F3FFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEAD7
        D7FFFEFEFFFFFEFFFEF8FFFDF2FFFBECFFFCE9FFFBE0D9C2B9E9E5E5FFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFEFEEEAF5EEEEF1E8E8EDE1E1EAD9D8E5D1CEE5CCC7E3CBC6FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFF}
      LookAndFeel.Kind = lfOffice11
    end
    object cxCalcEdit1: TcxCalcEdit
      Left = 352
      Top = 76
      EditValue = 0.000000000000000000
      Properties.Alignment.Horz = taRightJustify
      Style.BorderStyle = ebsOffice11
      Style.Shadow = True
      TabOrder = 6
      Width = 97
    end
    object cxRadioButton3: TcxRadioButton
      Left = 352
      Top = 44
      Width = 113
      Height = 17
      Caption = #1055#1086' '#1088#1072#1089#1095'. '#1094#1077#1085#1072#1084
      TabOrder = 7
    end
  end
  object GrSeb: TcxGrid
    Left = 11
    Top = 120
    Width = 566
    Height = 241
    TabOrder = 1
    LookAndFeel.Kind = lfOffice11
    object ViewSeb: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      OnCustomDrawCell = ViewSebCustomDrawCell
      DataController.DataSource = dsSeb
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SUM1'
          Column = ViewSebSUM1
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnGrouping = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.MultiSelect = True
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      object ViewSebIDCARD: TcxGridDBColumn
        Caption = #1050#1086#1076
        DataBinding.FieldName = 'IDCARD'
        Width = 38
      end
      object ViewSebNAME: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'NAME'
        Width = 136
      end
      object ViewSebNAMESHORT: TcxGridDBColumn
        Caption = #1045#1076'.'#1080#1079#1084'.'
        DataBinding.FieldName = 'NAMESHORT'
        Width = 49
      end
      object ViewSebCURMESSURE: TcxGridDBColumn
        Caption = #1045#1076'.'#1080#1079#1084'.'
        DataBinding.FieldName = 'CURMESSURE'
        Visible = False
      end
      object ViewSebNETTO: TcxGridDBColumn
        Caption = #1053#1077#1090#1090#1086
        DataBinding.FieldName = 'NETTO'
        Width = 59
      end
      object ViewSebBRUTTO: TcxGridDBColumn
        Caption = #1041#1088#1091#1090#1090#1086
        DataBinding.FieldName = 'BRUTTO'
      end
      object ViewSebKOEF: TcxGridDBColumn
        DataBinding.FieldName = 'KOEF'
        Visible = False
      end
      object ViewSebKNB: TcxGridDBColumn
        DataBinding.FieldName = 'KNB'
        Visible = False
      end
      object ViewSebPRICE1: TcxGridDBColumn
        Caption = #1062#1077#1085#1072' '
        DataBinding.FieldName = 'PRICE1'
      end
      object ViewSebPrice1000: TcxGridDBColumn
        Caption = #1062#1077#1085#1072' '#1079#1072' '#1086#1089#1085'. '#1077#1076'.'#1080#1079#1084'.'
        DataBinding.FieldName = 'PRICE1000'
        Width = 72
      end
      object ViewSebSUM1: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '
        DataBinding.FieldName = 'SUM1'
        Styles.Content = dmO.cxStyle25
        Width = 63
      end
      object ViewSebPRICE2: TcxGridDBColumn
        Caption = #1062#1077#1085#1072' '#1087#1086' '#1087#1072#1088#1090#1080#1103#1084
        DataBinding.FieldName = 'PRICE2'
        Visible = False
      end
      object ViewSebSUM2: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1087#1086' '#1087#1072#1088#1090#1080#1103#1084
        DataBinding.FieldName = 'SUM2'
        Visible = False
      end
      object ViewSebTCARD: TcxGridDBColumn
        Caption = #1058#1058#1050
        DataBinding.FieldName = 'TCARD'
        Visible = False
      end
    end
    object LevelSeb: TcxGridLevel
      GridView = ViewSeb
    end
  end
  object Memo1: TcxMemo
    Left = 0
    Top = 368
    Align = alBottom
    Lines.Strings = (
      'Memo1')
    TabOrder = 2
    OnDblClick = Memo1DblClick
    Height = 74
    Width = 586
  end
  object taSeb: TClientDataSet
    Aggregates = <>
    FileName = 'CalcSeb.cds'
    Params = <>
    Left = 216
    Top = 144
    object taSebIDCARD: TIntegerField
      FieldName = 'IDCARD'
    end
    object taSebCURMESSURE: TIntegerField
      FieldName = 'CURMESSURE'
    end
    object taSebNETTO: TFloatField
      FieldName = 'NETTO'
      DisplayFormat = '0.000'
    end
    object taSebBRUTTO: TFloatField
      FieldName = 'BRUTTO'
      DisplayFormat = '0.000'
    end
    object taSebNAME: TStringField
      FieldName = 'NAME'
      Size = 200
    end
    object taSebNAMESHORT: TStringField
      FieldName = 'NAMESHORT'
      Size = 50
    end
    object taSebKOEF: TFloatField
      FieldName = 'KOEF'
    end
    object taSebKNB: TFloatField
      FieldName = 'KNB'
    end
    object taSebPRICE1: TCurrencyField
      FieldName = 'PRICE1'
      DisplayFormat = '0.00'
    end
    object taSebSUM1: TCurrencyField
      FieldName = 'SUM1'
      DisplayFormat = '0.00'
    end
    object taSebPRICE2: TCurrencyField
      FieldName = 'PRICE2'
      DisplayFormat = '0.00'
    end
    object taSebSUM2: TCurrencyField
      FieldName = 'SUM2'
      DisplayFormat = '0.00'
    end
    object taSebPRICE1000: TFloatField
      FieldName = 'PRICE1000'
      DisplayFormat = '0.00'
    end
    object taSebTCARD: TSmallintField
      FieldName = 'TCARD'
    end
  end
  object dsSeb: TDataSource
    DataSet = taSeb
    Left = 216
    Top = 200
  end
  object quMHAll: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT MH.ID,MH.PARENT,MH.ITYPE,MH.NAMEMH,'
      '       MH.DEFPRICE,PT.NAMEPRICE'
      'FROM OF_MH MH'
      'left JOIN OF_PRICETYPE PT ON PT.ID=MH.DEFPRICE'
      'WHERE MH.ITYPE=1'
      'Order by MH.ID')
    Transaction = dmO.trSel1
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    Left = 296
    Top = 144
    object quMHAllID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quMHAllPARENT: TFIBIntegerField
      FieldName = 'PARENT'
    end
    object quMHAllITYPE: TFIBIntegerField
      FieldName = 'ITYPE'
    end
    object quMHAllNAMEMH: TFIBStringField
      FieldName = 'NAMEMH'
      Size = 100
      EmptyStrToNull = True
    end
    object quMHAllDEFPRICE: TFIBIntegerField
      FieldName = 'DEFPRICE'
    end
    object quMHAllNAMEPRICE: TFIBStringField
      FieldName = 'NAMEPRICE'
      Size = 100
      EmptyStrToNull = True
    end
  end
  object dsMHAll: TDataSource
    DataSet = quMHAll
    Left = 296
    Top = 200
  end
  object taSpecC: TClientDataSet
    Aggregates = <>
    FileName = 'SpecSeb.cds'
    FieldDefs = <
      item
        Name = 'Num'
        DataType = ftInteger
      end
      item
        Name = 'IdGoods'
        DataType = ftInteger
      end
      item
        Name = 'NameG'
        DataType = ftString
        Size = 200
      end
      item
        Name = 'IM'
        DataType = ftInteger
      end
      item
        Name = 'SM'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Quant'
        DataType = ftFloat
      end
      item
        Name = 'PriceIn'
        DataType = ftFloat
      end
      item
        Name = 'SumIn'
        DataType = ftFloat
      end
      item
        Name = 'PriceUch'
        DataType = ftFloat
      end
      item
        Name = 'SumUch'
        DataType = ftFloat
      end
      item
        Name = 'Km'
        DataType = ftFloat
      end>
    IndexDefs = <
      item
        Name = 'taSpecCIndex1'
        Fields = 'IDGoods'
        Options = [ixCaseInsensitive]
      end>
    IndexName = 'taSpecCIndex1'
    Params = <>
    StoreDefs = True
    Left = 112
    Top = 168
    object taSpecCNum: TIntegerField
      FieldName = 'Num'
    end
    object taSpecCIdGoods: TIntegerField
      FieldName = 'IdGoods'
    end
    object taSpecCNameG: TStringField
      FieldName = 'NameG'
      Size = 200
    end
    object taSpecCIM: TIntegerField
      FieldName = 'IM'
    end
    object taSpecCSM: TStringField
      FieldName = 'SM'
    end
    object taSpecCQuant: TFloatField
      FieldName = 'Quant'
      DisplayFormat = '0.000'
    end
    object taSpecCPriceIn: TFloatField
      FieldName = 'PriceIn'
      DisplayFormat = '0.00'
    end
    object taSpecCSumIn: TFloatField
      FieldName = 'SumIn'
      DisplayFormat = '0.00'
    end
    object taSpecCPriceUch: TFloatField
      FieldName = 'PriceUch'
      DisplayFormat = '0.00'
    end
    object taSpecCSumUch: TFloatField
      FieldName = 'SumUch'
      DisplayFormat = '0.00'
    end
    object taSpecCKm: TFloatField
      FieldName = 'Km'
    end
  end
  object frRepCSeb: TfrReport
    InitialZoom = pzDefault
    PreviewButtons = [pbZoom, pbLoad, pbSave, pbPrint, pbFind, pbHelp, pbExit]
    RebuildPrinter = False
    Left = 64
    Top = 256
    ReportForm = {19000000}
  end
  object frtaSeb: TfrDBDataSet
    DataSet = taSeb
    Left = 136
    Top = 256
  end
end
