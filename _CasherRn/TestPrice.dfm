object fmTestPrice: TfmTestPrice
  Left = 412
  Top = 324
  Width = 557
  Height = 374
  Caption = #1050#1086#1085#1090#1088#1086#1083#1100' '#1094#1077#1085
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 549
    Height = 33
    Align = alTop
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 0
    object Label1: TLabel
      Left = 28
      Top = 12
      Width = 136
      Height = 13
      Caption = #1055#1088#1086#1074#1077#1088#1082#1072' '#1094#1077#1085' '#1087#1088#1080#1093#1086#1076#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 286
    Width = 549
    Height = 54
    Align = alBottom
    BevelInner = bvLowered
    Color = clBtnHighlight
    TabOrder = 1
    object cxButton1: TcxButton
      Left = 76
      Top = 16
      Width = 97
      Height = 25
      Caption = #1055#1088#1086#1076#1086#1083#1078#1080#1090#1100
      Default = True
      ModalResult = 1
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 396
      Top = 16
      Width = 99
      Height = 25
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton3: TcxButton
      Left = 196
      Top = 16
      Width = 99
      Height = 25
      Caption = ' '#1074' Excel'
      TabOrder = 2
      OnClick = cxButton3Click
      Glyph.Data = {
        E6040000424DE604000000000000360000002800000014000000140000000100
        180000000000B004000000000000000000000000000000000000CCC7CCD8D0D8
        C0C8BFC3CCC4C2CBC3C1CBC2C6CAC6CBC8CA918798718073788F76C8C8CAC4C5
        C5C4C5C6C4C5C6C4C6C6C4C5C5C5C5C5BABABABFBFBF9EA99E859684B8A8BBB0
        9EAEB4A2B1B3A0B0B8ACB86D856F046F004B87465F7E63B1AEADB0ADADB0AEAE
        B1AEAEB0AEADB1B0AFADAEAEC7C6C6BCBCBCB3BCB24360453C882E3BB0472E98
        3F2FAA442E5E340B91007FFF61A2C19FC3C7CEB8BBBEB5B8BBB6B9BBB5B8BAB7
        BBBDA7A4A5838081929393C5C5C5C7C5C6C9C4CA41773900A30000D71D00820D
        00890076FF5E909C8C006200A0C7AFCBCBD5C1CACCC2CACDC1C9CDC6CED3B3B5
        B6857A7A4C3E3EB7B9B9B9BAB9C1C2C2BBAEBA4D774F097C1303870067F95CAA
        C2A94B6F4E3A713EC5CFD1BEBAC0B9B7BBB8B7BBC1BEC0969393B5BBBF888889
        4C4545B0B1B1BEBEBEB8BBB8D0CBD07B867B1395007DFF5E9CAD98354E35B6CE
        C0F9FFFFD2E9DAD6EDDFD9EEE2DBF0E3DCECE2D5DBE0C2C6C7ACAAAD4B3434B1
        B5B5BCBDBCD4CDD4708671038D026DF95495A89330894340B6542F5030A9C2A9
        E9F3E6E4EDE7C7DFCBBCD9BACBDED0DCEFE8ADB1B49E9A9C4D3938B1B4B4C8C9
        C95B725900600033CB28B6C7B38FA69078A17B00940028873612370FD4E2D8F1
        F3F7D1DDDFCBDED1C2DDC3D7EADFD8E9DE88887E4C4B3CB1B1B49FA99F487148
        3C5C3C627E63FFFFFFFFFFFFFFFFFF698767527C4F648865D0D8D8E9F6F2CCDD
        D6DEE3EBE2F0E8D0E5D7FEFFFF7474744D4D4DB1B1B1C0BEC0DBD4DB9F999FF9
        F4F9F3F9F2DCEBE2F7FDFFFFFFFFFCFFFFFFFFFFFBFFFFFFFDFFF5FEFFF6FFFF
        E8F0E6CFE5D7F5FDFD7475754D4C4CB1B1B1BABBBAC2C3C28D8E8DE1E1E1EFF8
        EECAE2D1ECF7F3E2F2EEC1D6C7C6CFD2C8D3D4CED7D9CED6D7D2D9DCCDD4D6DD
        E7E8F6FEFE7476764D4C4CB1B1B1BBBBBBC6C6C6909090E5E3E5F4FBF4B6D4B5
        CDE4CFF1FAFDDDEFE8EBF4EAE2F1E6D5E9E0DAE9DBFEFEFEFBFCFBFFFDFDF5FD
        FE7476764D4C4CB1B1B1BBBBBBC6C6C6909090E5E3E5F4FBF4B4D4B2D3DFDEF2
        FCFFE1EDF2EFF1F3E9EDF3D9EBE2D3E8DBFDFDFDFBFBFAFCFCFBF5FCFD747676
        4D4C4CB1B1B1BBBBBBC6C6C6909090E4E3E4F0F8EFCAE1D1E2EBEBE8F3F6E2EC
        F0E5EEF1E5EFF1D7E2E3D6E2E4FDFEFEFBFBFBFDFDFCF3FCFD7476764E4C4CB1
        B1B1BBBBBBC6C6C6909191E5E3E5FFFFFFDCE9EED0DCE0D4DDE5D4DEE4D0DBDF
        D4DFE4CDD7DEE3EDF1FEFFFFFFFFFFFDFEFDFBFFFF7578784D4C4CB1B1B1BBBB
        BBC5C5C5919292CED2C6EDE5E6EEF0F5EEEEF2DEEBDCDFE6E0F6F6FDE8E9EAEB
        F3ECF3F0F4F9F5F7EBE2E5F8F3F6EADADB7670704D4E4EB1B1B1BBBBBBC4C5C5
        9993947AC0B951D6D67FDADB6FD7D77FD5CA6CD2CC7EDBDD5ED4D16BDAD374D8
        D980DCDC54D0D17BDBDC43CECD3D6D6D54504FB1B1B1BBBBBBC4C5C59992928C
        D7DBAAF6EE9CE8DDA9EBDE9FF1F3AEEFE7AAEDE4B6EFE190EFF4B2EDE1A9EEE6
        B4EFE18FEEF2B6F3E40084832C484CB2B0AFBABABAC5C5C5909090ACAEAFCCC4
        C596B6B8A7BBBDC6C2C4BFC0C3C1C1C4C0C1C4C2C1C3C0C1C4C0C1C4C1C1C4C2
        BFC2C6C8C81C7D87293A51C1C0BCBDBDBDC0BFBFB3B3B3A1A1A19FA0A0A7A2A2
        A4A1A19FA0A0A1A0A0A0A0A0A1A0A0A0A0A0A1A0A0A0A0A0A0A0A0A0A0A0A4A3
        A2A69C9E9CA4A9C5C4C3}
      LookAndFeel.Kind = lfOffice11
    end
  end
  object GridTP: TcxGrid
    Left = 8
    Top = 44
    Width = 533
    Height = 233
    TabOrder = 2
    LookAndFeel.Kind = lfOffice11
    object ViewTP: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = fmAddDoc1.dsmePrice
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      object ViewTPNum: TcxGridDBColumn
        Caption = #8470' '#1087#1087
        DataBinding.FieldName = 'Num'
        Width = 56
      end
      object ViewTPIdGoods: TcxGridDBColumn
        Caption = #1050#1086#1076' '
        DataBinding.FieldName = 'IdGoods'
        Width = 55
      end
      object ViewTPNameG: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'NameG'
        Width = 138
      end
      object ViewTPSM: TcxGridDBColumn
        Caption = #1045#1076'.'#1080#1079#1084'.'
        DataBinding.FieldName = 'SM'
        Width = 39
      end
      object ViewTPQuant: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086
        DataBinding.FieldName = 'Quant'
        Width = 54
      end
      object ViewTPPrice1: TcxGridDBColumn
        Caption = #1062#1077#1085#1072
        DataBinding.FieldName = 'Price1'
        Styles.Content = dmO.cxStyle25
        Width = 59
      end
      object ViewTPPricePP: TcxGridDBColumn
        Caption = #1062#1077#1085#1072' '#1055#1055
        DataBinding.FieldName = 'PricePP'
        Styles.Content = dmO.cxStyle25
        Width = 54
      end
      object ViewTPDif: TcxGridDBColumn
        Caption = #1056#1072#1079#1085#1080#1094#1072' %'
        DataBinding.FieldName = 'Dif'
        Styles.Content = dmO.cxStyle12
        Width = 54
      end
    end
    object LevelTP: TcxGridLevel
      GridView = ViewTP
    end
  end
end
