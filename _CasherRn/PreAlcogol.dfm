object fmPreAlg: TfmPreAlg
  Left = 382
  Top = 590
  BorderStyle = bsDialog
  Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099' '#1086#1090#1095#1077#1090#1072' '#1087#1086' '#1072#1083#1082#1086#1075#1086#1083#1102'.'
  ClientHeight = 178
  ClientWidth = 577
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 12
    Top = 16
    Width = 50
    Height = 13
    Caption = #1055#1077#1088#1080#1086#1076' '#1089' '
  end
  object Label1: TLabel
    Left = 196
    Top = 16
    Width = 12
    Height = 13
    Caption = #1087#1086
  end
  object Label3: TLabel
    Left = 344
    Top = 16
    Width = 72
    Height = 13
    Caption = #1074#1082#1083#1102#1095#1080#1090#1077#1083#1100#1085#1086
  end
  object Label4: TLabel
    Left = 12
    Top = 48
    Width = 31
    Height = 13
    Caption = #1054#1090#1076#1077#1083
  end
  object Panel1: TPanel
    Left = 459
    Top = 0
    Width = 118
    Height = 178
    Align = alRight
    BevelInner = bvLowered
    Color = 16769476
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 8
      Top = 8
      Width = 101
      Height = 25
      Caption = #1054#1082
      Default = True
      ModalResult = 1
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 8
      Top = 48
      Width = 101
      Height = 25
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      LookAndFeel.Kind = lfOffice11
    end
  end
  object cxDateEdit1: TcxDateEdit
    Left = 68
    Top = 12
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 1
    Width = 113
  end
  object cxDateEdit2: TcxDateEdit
    Left = 220
    Top = 12
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 2
    Width = 109
  end
  object cxRadioButton1: TcxRadioButton
    Left = 40
    Top = 88
    Width = 113
    Height = 17
    Caption = #1060#1086#1088#1084#1072' 1-1'
    Checked = True
    TabOrder = 3
    TabStop = True
    LookAndFeel.Kind = lfOffice11
  end
  object cxRadioButton2: TcxRadioButton
    Left = 40
    Top = 116
    Width = 113
    Height = 17
    Caption = #1060#1086#1088#1084#1072' 1-2'
    TabOrder = 4
    LookAndFeel.Kind = lfOffice11
  end
  object cxRadioButton3: TcxRadioButton
    Left = 184
    Top = 88
    Width = 113
    Height = 17
    Caption = #1060#1086#1088#1084#1072' 2-1 ('#1055#1080#1074#1086')'
    TabOrder = 5
    LookAndFeel.Kind = lfOffice11
  end
  object cxRadioButton4: TcxRadioButton
    Left = 184
    Top = 116
    Width = 113
    Height = 17
    Caption = #1060#1086#1088#1084#1072' 2-2 ('#1055#1080#1074#1086')'
    TabOrder = 6
    LookAndFeel.Kind = lfOffice11
  end
  object cxLookupComboBox2: TcxLookupComboBox
    Left = 68
    Top = 42
    Properties.KeyFieldNames = 'Id'
    Properties.ListColumns = <
      item
        Caption = #1050#1086#1076
        FieldName = 'ID'
      end
      item
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        FieldName = 'NAMEMH'
      end>
    Properties.ListFieldIndex = 1
    Properties.ListSource = dsMHAll1
    Style.BorderStyle = ebsOffice11
    Style.LookAndFeel.Kind = lfOffice11
    Style.Shadow = True
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 7
    Width = 261
  end
  object quMHAll1: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT MH.ID,MH.PARENT,MH.ITYPE,MH.NAMEMH,'
      '       MH.DEFPRICE,PT.NAMEPRICE'
      'FROM OF_MH MH'
      'left JOIN OF_PRICETYPE PT ON PT.ID=MH.DEFPRICE'
      'WHERE MH.ITYPE=1'
      'Order by MH.ID')
    Transaction = dmO.trSelect
    Database = dmO.OfficeRnDb
    UpdateTransaction = dmO.trUpdate
    Left = 340
    Top = 96
    object quMHAll1ID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quMHAll1PARENT: TFIBIntegerField
      FieldName = 'PARENT'
    end
    object quMHAll1ITYPE: TFIBIntegerField
      FieldName = 'ITYPE'
    end
    object quMHAll1NAMEMH: TFIBStringField
      FieldName = 'NAMEMH'
      Size = 100
      EmptyStrToNull = True
    end
    object quMHAll1DEFPRICE: TFIBIntegerField
      FieldName = 'DEFPRICE'
    end
    object quMHAll1NAMEPRICE: TFIBStringField
      FieldName = 'NAMEPRICE'
      Size = 100
      EmptyStrToNull = True
    end
  end
  object dsMHAll1: TDataSource
    DataSet = quMHAll1
    Left = 396
    Top = 96
  end
end
