unit PartInRemn;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons, ExtCtrls,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxImageComboBox, cxCalendar, ActnList,
  XPStyleActnCtrls, ActnMan, cxContainer, cxTextEdit, cxMemo, dxmdaset;

type
  TfmPartRemn = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    GridPartInRemn: TcxGrid;
    ViewPartIn: TcxGridDBTableView;
    ViewPartInARTICUL: TcxGridDBColumn;
    ViewPartInID: TcxGridDBColumn;
    ViewPartInNAMEMH: TcxGridDBColumn;
    ViewPartInIDDOC: TcxGridDBColumn;
    ViewPartInNUMDOC: TcxGridDBColumn;
    ViewPartInIDATE: TcxGridDBColumn;
    ViewPartInIDCLI: TcxGridDBColumn;
    ViewPartInNAMECL: TcxGridDBColumn;
    ViewPartInDTYPE: TcxGridDBColumn;
    ViewPartInQPART: TcxGridDBColumn;
    ViewPartInQREMN: TcxGridDBColumn;
    ViewPartInPRICEIN: TcxGridDBColumn;
    ViewPartInPRICEOUT: TcxGridDBColumn;
    ViewPartInSUMIN: TcxGridDBColumn;
    ViewPartInSUMREMN: TcxGridDBColumn;
    ViewPartInPRICEIN0: TcxGridDBColumn;
    ViewPartInSUMIN0: TcxGridDBColumn;
    LevelPartIn: TcxGridLevel;
    acPartAction: TActionManager;
    acCreateB: TAction;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Panel3: TPanel;
    cxButton1: TcxButton;
    Memo1: TcxMemo;
    taTSpec: TdxMemData;
    taTSpecNum: TSmallintField;
    taTSpecIdCard: TIntegerField;
    taTSpecIdM: TIntegerField;
    taTSpecSM: TStringField;
    taTSpecKm: TFloatField;
    taTSpecName: TStringField;
    taTSpecNetto: TFloatField;
    taTSpecBrutto: TFloatField;
    taTSpecKnb: TFloatField;
    taTSpecTCard: TSmallintField;
    taTSpecNetto1: TFloatField;
    taTSpecPr1: TFloatField;
    taTSpecNetto2: TFloatField;
    taTSpecPr2: TFloatField;
    taTSpecIChange: TSmallintField;
    procedure cxButton2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ViewPartInRemnDblClick(Sender: TObject);
    procedure acCreateBExecute(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Memo1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmPartRemn: TfmPartRemn;

implementation

uses Un1, dmOffice, CreateBFromP, Message, DMOReps, DocCompl;

{$R *.dfm}

procedure TfmPartRemn.cxButton2Click(Sender: TObject);
begin
  close;
end;

procedure TfmPartRemn.FormCreate(Sender: TObject);
begin
//  GrPartInRemn.Align:=AlClient;
end;

procedure TfmPartRemn.ViewPartInRemnDblClick(Sender: TObject);
begin
  ModalResult:=mrOk;
end;

procedure TfmPartRemn.acCreateBExecute(Sender: TObject);
Var iCodeCT,iMax:INteger;
    rM1,rM, rMItog, rMIngr1, rMIngr, rKp:Real;
    iDate,IDH:Integer;
    bOn:Boolean;
    rSum:Real;
begin
  if dmO.quCPartIn1.RecordCount>0 then
  begin
    fmCreateBFromP:=TfmCreateBFromP.Create(Application);
    fmCreateBFromP.ViewF.BeginUpdate;
    try
      with fmCreateBFromP do
      begin
        quInputF.Active:=False;
        quInputF.ParamByName('IDC').AsInteger:=Label4.tag;
        quInputF.ParamByName('CURDATE').AsDateTime:=Trunc(Date);
        quInputF.Active:=True;
      end;
    finally
      fmCreateBFromP.ViewF.EndUpdate;
    end;
    fmCreateBFromP.cxCalcEdit1.Value:=r1000(dmO.quCPartIn1QREMN.AsFloat);
    fmCreateBFromP.cxDateEdit1.Date:=date;
    fmCreateBFromP.ShowModal;
    if fmCreateBFromP.ModalResult=mrOk then
    begin
      iCodeCT:=0;
      with fmCreateBFromP do
      begin
        if quInputF.RecordCount>0 then
        begin
          if MessageDlg('������������ ������������ �� ������������  '+quInputFSHORTNAME.AsString+' ��  '+ds1(fmCreateBFromP.cxDateEdit1.Date),mtConfirmation, [mbYes, mbNo], 0) = mrYes then
          begin
            iCodeCT:=quInputFIDCT.AsInteger;
          end;
        end;
      end;

      if iCodeCT>0 then
      begin
        Memo1.Clear;
        Memo1.Lines.Add('����� ���� ������������ ��������� ...'); delay(10);
        try
          //�������� ������ ���-��
          // 1. ���� ���  - �����
          with dmO do
          with dmORep do
          with fmCreateBFromP do
          begin
            iDate:=Trunc(fmCreateBFromP.cxDateEdit1.Date);

            quTSpec.Active:=False;
            quTSpec.ParamByName('IDC').AsInteger:=quInputFIDC.AsInteger;
            quTSpec.ParamByName('IDTC').AsInteger:=quInputFIDCT.AsInteger;
            quTSpec.Active:=True;

            CloseTe(taTSpec);

            iMax:=1;
            quTSpec.First;
            while not quTSpec.Eof do
            begin
              taTSpec.Append;
              taTSpecNum.AsInteger:=iMax;
              taTSpecIdCard.AsInteger:=quTSpecIDCARD.AsInteger;
              taTSpecIdM.AsInteger:=quTSpecCURMESSURE.AsInteger;
              taTSpecSM.AsString:=quTSpecNAMESHORT.AsString;
              taTSpecKm.AsFloat:=quTSpecKOEF.AsFloat;
              taTSpecName.AsString:=quTSpecNAME.AsString;
              taTSpecNetto.AsFloat:=quTSpecNETTO.AsFloat;
              taTSpecBrutto.AsFloat:=quTSpecBRUTTO.asfloat;
              taTSpecKnb.AsFloat:=0;
              taTSpecTCard.AsInteger:=quTSpecTCARD.AsInteger;
              taTSpecNetto1.AsFloat:=quTSpecNETTO1.AsFloat;
              taTSpecPr1.AsFloat:=0;
              if taTSpecNetto.AsFloat>0 then taTSpecPr1.AsFloat:=RoundEx((taTSpecNetto.AsFloat-taTSpecNetto1.AsFloat)/taTSpecNetto.AsFloat*10000)/100;
              taTSpecNetto2.AsFloat:=quTSpecNETTO2.AsFloat;
              taTSpecPr2.AsFloat:=0;
              if taTSpecNetto1.AsFloat>0 then taTSpecPr2.AsFloat:=RoundEx((taTSpecNetto1.AsFloat-taTSpecNetto2.AsFloat)/taTSpecNetto1.AsFloat*10000)/100;
              taTSpecIChange.AsInteger:=quTSpecICHANGE.AsInteger;
              taTSpec.Post;

              quTSpec.Next;   inc(iMax);
            end;
            quTSpec.Active:=False;

          // 2. ��������� ����� 1-�� ������ �� ���
            rM:=0;
            taTSpec.First;
            while not taTSpec.Eof do
            begin
              rM:=rM+taTSpecNetto2.AsFloat*taTSpecKm.AsFloat; //� ��������
              taTSpec.Next;
            end;

            rM1:=rM/quInputFPCOUNT.AsInteger; // ����� ����� ������ � ��������
//            rKb:=quInputFNETTO2.AsFloat/quInputFNETTO.AsFloat; //����. ����������� � ���

            rMIngr:=r1000(fmCreateBFromP.cxCalcEdit1.Value);
            rMIngr1:=quInputFNETTO.AsFloat*quInputFKOEF.AsFloat;

            rKp:=rMIngr/rMIngr1; //������� ������ ����� �������
            rMItog:=rM1*rKp;
            //������� ���� �� �������

            Memo1.Lines.Add('  ������ ��������� ���-��');
            Memo1.Lines.Add('    - rMIngr  '+fts(rMIngr));
            Memo1.Lines.Add('    - rMIngr1 '+fts(rMIngr1));
            Memo1.Lines.Add('    - rKp     '+fts(r1000(rKp)));
            Memo1.Lines.Add('    - rM1     '+fts(r1000(rM1)));
            Memo1.Lines.Add('    - rMItog  '+fts(r1000(rMItog)));
            Memo1.Lines.Add(''); delay(10);

            // 5. ����������� ������.
            Memo1.Lines.Add('  ������ ������������');

            quFindCard.Active:=False;
            quFindCard.ParamByName('IDCARD').AsInteger:=quInputFIDC.AsInteger;
            quFindCard.Active:=True;
            if quFindCard.RecordCount>0 then
            begin
              CloseTe(teC0);
              CloseTe(teC1);
              CloseTe(teC2);

              teC0.Append;
              teC0ID.AsInteger:=1;
              teC0IDCARD.AsInteger:=quInputFIDC.AsInteger;
              teC0NAME.AsString:=quFindCardNAME.AsString;
              teC0TCARD.AsInteger:=quFindCardTCARD.AsInteger;
              teC0QUANT.AsFloat:=rMItog;
              teC0IDM.AsInteger:=quFindCardIMESSURE.AsInteger;
              teC0PRICER.AsFloat:=0;
              teC0RSUM.AsFloat:=0;
              teC0KM.AsInteger:=1;
              teC0.Post;

              prCalcBl('    ',quFindCardNAME.AsString,Label5.Tag,teC0ID.AsInteger,teC0IDCARD.AsInteger,iDate,teC0TCARD.AsInteger,rMItog,teC0IDM.AsInteger,teC0,teC1,teC2,Memo1,True,1,0,0);
            end;

            //������������� �� ������, �.�. ��� ������������� ��� ����� ��� �����������
            IDH:=GetId('HeadCompl');

            Memo1.Lines.Add('  ��������� ��������. (��.��� '+its(IDH)+')');

            quDocsComlRec.Active:=False;
            quDocsComlRec.ParamByName('IDH').AsInteger:=IDH;
            quDocsComlRec.Active:=True;

            quDocsComlRec.First;
            quDocsComlRec.Append;
            quDocsComlRecID.AsInteger:=IDH;
            quDocsComlRecDATEDOC.AsDateTime:=iDate;
            quDocsComlRecNUMDOC.AsString:=prGetNum(6,1);
            quDocsComlRecIDSKL.AsInteger:=Label5.Tag;
            quDocsComlRecIACTIVE.AsInteger:=0;
            quDocsComlRecOPER.AsString:='';
            quDocsComlRecSUMIN.AsFloat:=0;
            quDocsComlRecSUMUCH.AsFloat:=0;
            quDocsComlRecSUMTAR.AsFloat:=0;
            quDocsComlRecPROCNAC.AsFloat:=0;
            quDocsComlRecIDSKLTO.AsInteger:=Label5.Tag;
            quDocsComlRecTOREAL.AsInteger:=0;
            quDocsComlRec.Post;

            quSpecCompl.Active:=False;
            quSpecCompl.ParamByName('IDH').AsInteger:=IDH;
            quSpecCompl.Active:=True;

            quSpecCompl.First;
            while not quSpecCompl.Eof do quSpecCompl.Delete;

            teC0.First;
            while not teC0.Eof do
            begin
              quSpecCompl.Append;
              quSpecComplIDHEAD.AsInteger:=IDH;
              quSpecComplID.AsInteger:=teC0ID.AsInteger;
              quSpecComplIDCARD.AsInteger:=teC0IDCARD.AsInteger;
              quSpecComplQUANT.AsFloat:=teC0QUANT.AsFloat;
              quSpecComplIDM.AsInteger:=teC0IDM.AsInteger;
              quSpecComplKM.AsFloat:=teC0KM.AsFloat;
              quSpecComplPRICEIN.AsFloat:=0;
              quSpecComplSUMIN.AsFloat:=0;
              quSpecComplPRICEINUCH.AsFloat:=0;
              quSpecComplSUMINUCH.AsFloat:=0;
              quSpecComplTCARD.AsInteger:=teC0TCard.AsInteger;
              quSpecComplPRICEIN0.AsFloat:=0;
              quSpecComplSUMIN0.AsFloat:=0;
              quSpecCompl.Post;

              teC0.Next; delay(10);
            end;
            quSpecCompl.Active:=False;

            quSpecComplC.Active:=False;
            quSpecComplC.ParamByName('IDH').AsInteger:=IDH;
            quSpecComplC.Active:=True;

            quSpecComplC.First;
            while not quSpecComplC.Eof do quSpecComplC.Delete;

            iMax:=1;

            teC1.First;
            while not teC1.Eof do
            begin
              quSpecComplC.Append;
              quSpecComplCIDHEAD.AsInteger:=IDH;
              quSpecComplCID.AsInteger:=iMax;
              quSpecComplCIDCARD.AsInteger:=teC1Articul.AsInteger;
              quSpecComplCQUANT.AsFloat:=teC1Quant.AsFloat;
              quSpecComplCIDM.AsInteger:=teC1IdM.AsInteger;
              quSpecComplCKM.AsFloat:=teC1Km.AsFloat;
              quSpecComplCPRICEIN.AsFloat:=0;
              quSpecComplCSUMIN.AsFloat:=0;
              quSpecComplCPRICEIN0.AsFloat:=0;
              quSpecComplCSUMIN0.AsFloat:=0;
              quSpecComplCPRICEINUCH.AsFloat:=0;
              quSpecComplCSUMINUCH.AsFloat:=0;
              quSpecComplCNAMESHORT.AsString:=teC1SM.AsString;
              quSpecComplCNAME.AsString:=teC1Name.AsString;
              quSpecComplC.Post;

              teC1.Next; inc(iMax); delay(10);
            end;
            quSpecComplC.Active:=False;

            quSpecComplCB.Active:=False;
            quSpecComplCB.ParamByName('IDH').AsInteger:=IdH;
            quSpecComplCB.Active:=True;

            while not quSpecComplCB.Eof do quSpecComplCB.Delete;

            iMax:=1;
            teC2.First;
            while not teC2.Eof do
            begin
              quSpecComplCB.Append;
              quSpecComplCBIDHEAD.AsInteger:=Idh;
              quSpecComplCBIDB.AsInteger:=teC2ID.AsInteger;
              quSpecComplCBID.AsInteger:=iMax; //��� ��� ������
              quSpecComplCBCODEB.AsInteger:=teC2CODEB.AsInteger;
              quSpecComplCBNAMEB.AsString:=teC2NAMEB.AsString;
              quSpecComplCBQUANT.AsFloat:=teC2QUANT.AsFloat;
              quSpecComplCBIDCARD.AsInteger:=teC2IDCARD.AsInteger;
              quSpecComplCBNAMEC.AsString:=teC2NAMEC.AsString;
              quSpecComplCBQUANTC.AsFloat:=teC2QUANTC.AsFloat;
              quSpecComplCBPRICEIN.AsFloat:=teC2PRICEIN.AsFloat;
              quSpecComplCBSUMIN.AsFloat:=teC2SUMIN.AsFloat;
              quSpecComplCBIM.AsInteger:=teC2IM.AsInteger;
              quSpecComplCBSM.AsString:=teC2SM.AsString;
              quSpecComplCBSB.AsString:=teC2SB.AsString;
              quSpecComplCBPRICEIN0.AsFloat:=teC2PRICEIN0.AsFloat;
              quSpecComplCBSUMIN0.AsFloat:=teC2SUMIN0.AsFloat;
              quSpecComplCB.Post;

              inc(iMax);
              teC2.Next;
            end;
            quSpecComplCB.Active:=False;


            Memo1.Lines.Add('  �����.. ���� ������������� ���������.');

            bOn:=True;

            if not CanDo('prOnCompl') then begin Memo1.Lines.Add('  ��� ����.'); bOn:=False; end;
            if not CanEdit(iDate,Label5.Tag) then begin Memo1.Lines.Add('  ������ ������.'); bOn:=False; end;
            if prTOFind(iDate,Label5.Tag)=1 then  prTODel(Trunc(quDocsComplDATEDOC.AsDateTime),quDocsComplIDSKL.AsInteger);

            if bOn then
            begin
              fmDocsCompl.prOn(IDH,Label5.Tag,iDate,Label5.Tag,rSum,rSum);

              quDocsComlRec.Edit;
              quDocsComlRecSUMIN.AsFloat:=rSum;
              quDocsComlRecSUMUCH.AsFloat:=rSum;//����
              quDocsComlRecIACTIVE.AsInteger:=1;
              quDocsComlRec.Post;
              quDocsComlRec.Refresh;

              Memo1.Lines.Add('   ������������� ��'); delay(10);
             

//              Memo1.Lines.Add('  ���������� ����������'); delay(10);
              
//              quCPartIn1.Refresh;
//              quCardsSel.Refresh;
            end;
            quDocsComlRec.Active:=False;

            taTSpec.Close;
          end;
        except
          Memo1.Lines.Add('  ������ ������������.'); delay(10);
        end;
        Memo1.Lines.Add('������� ��������.'); delay(10);
      end;
    end;
    fmCreateBFromP.Release;
  end;
end;

procedure TfmPartRemn.cxButton1Click(Sender: TObject);
begin
  dmO.quCPartIn1.Active:=False;
  Close;
end;

procedure TfmPartRemn.FormShow(Sender: TObject);
begin
  Memo1.Clear;
end;

procedure TfmPartRemn.Memo1DblClick(Sender: TObject);
begin
  fmMessage:=tfmMessage.Create(Application);
  fmMessage.Memo1.Lines:=Memo1.Lines;
  fmMessage.ShowModal;
  fmMessage.Release;
end;

end.
