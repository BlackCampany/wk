unit CB;

interface

uses
  SysUtils, Classes, DB, ADODB;

type
  TdmCB = class(TDataModule)
    quDocZHRec: TADOQuery;
    quDocZSSel: TADOQuery;
    msConnection: TADOConnection;
    quDocZSSelIDH: TLargeintField;
    quDocZSSelIDS: TLargeintField;
    quDocZSSelCODE: TIntegerField;
    quDocZSSelBARCODE: TStringField;
    quDocZSSelQUANTPACK: TFloatField;
    quDocZSSelPRICEIN: TFloatField;
    quDocZSSelPRICEIN0: TFloatField;
    quDocZSSelPRICEM: TFloatField;
    quDocZSSelSUMIN: TFloatField;
    quDocZSSelSUMIN0: TFloatField;
    quDocZSSelSUMNDS: TFloatField;
    quDocZSSelNDSPROC: TFloatField;
    quDocZSSelREMN1: TFloatField;
    quDocZSSelVREAL: TFloatField;
    quDocZSSelREMNDAY: TFloatField;
    quDocZSSelREMNMIN: TFloatField;
    quDocZSSelREMNMAX: TFloatField;
    quDocZSSelREMNSTRAH: TFloatField;
    quDocZSSelREMN2: TFloatField;
    quDocZSSelPKDOC: TFloatField;
    quDocZSSelPKCARD: TFloatField;
    quDocZSSelDAYSTOZ: TSmallintField;
    quDocZSSelQUANTZ: TFloatField;
    quDocZSSelQUANTZALR: TFloatField;
    quDocZSSelQUANTZITOG: TFloatField;
    quDocZSSelQUANTZFACT: TFloatField;
    quDocZSSelCLIQUANT: TFloatField;
    quDocZSSelCLIPRICE: TFloatField;
    quDocZSSelCLIPRICE0: TFloatField;
    quDocZSSelCLIQUANTN: TFloatField;
    quDocZSSelCLIPRICEN: TFloatField;
    quDocZSSelCLIPRICE0N: TFloatField;
    quDocZSSelCLINNUM: TIntegerField;
    quDocZSSelROUNDPACK: TIntegerField;
    quDocZHRecID: TLargeintField;
    quDocZHRecDOCDATE: TWideStringField;
    quDocZHRecDOCDATEZ: TWideStringField;
    quDocZHRecIDATE: TIntegerField;
    quDocZHRecIDATEZ: TIntegerField;
    quDocZHRecDAYSTOZ: TSmallintField;
    quDocZHRecDOCNUM: TStringField;
    quDocZHRecISHOP: TIntegerField;
    quDocZHRecDEPART: TIntegerField;
    quDocZHRecITYPE: TSmallintField;
    quDocZHRecINSUMIN: TFloatField;
    quDocZHRecINSUMIN0: TFloatField;
    quDocZHRecDEPARTORG: TIntegerField;
    quDocZHRecCLITYPE: TSmallintField;
    quDocZHRecCLICODE: TIntegerField;
    quDocZHRecIACTIVE: TSmallintField;
    quDocZHRecDATEEDIT: TDateTimeField;
    quDocZHRecPERSON: TStringField;
    quDocZHRecCLISUMIN: TFloatField;
    quDocZHRecCLISUMIN0: TFloatField;
    quDocZHRecCLISUMINN: TFloatField;
    quDocZHRecCLISUMIN0N: TFloatField;
    quDocZHRecQUANT: TFloatField;
    quDocZHRecCLIQUANT: TFloatField;
    quDocZHRecCLIQUANTN: TFloatField;
    quDocZHRecQUANTZAUTO: TFloatField;
    quDocZHRecIDATEPOST: TIntegerField;
    quDocZHRecIDATENACL: TIntegerField;
    quDocZHRecSNUMNACL: TStringField;
    quDocZHRecIDATESCHF: TIntegerField;
    quDocZHRecSNUMSCHF: TStringField;
    quDocZHRecSENDTO: TSmallintField;
    quDocZHRecSENDDATE: TDateTimeField;
    quDocZHRecSNUMPODTV: TWideStringField;
    quDocZHRecSENDNACL: TSmallintField;
    quDocZHRecIDHPRO: TIntegerField;
    quDocZSNacl: TADOQuery;
    quDocZSNaclIDH: TIntegerField;
    quDocZSNaclIDS: TIntegerField;
    quDocZSNaclICODE: TIntegerField;
    quDocZSNaclICARDTYPE: TIntegerField;
    quDocZSNaclQUANT: TFloatField;
    quDocZSNaclPRICE0: TFloatField;
    quDocZSNaclPRICE: TFloatField;
    quDocZSNaclRNDS: TFloatField;
    quDocZSNaclRSUM0: TFloatField;
    quDocZSNaclRSUM: TFloatField;
    quDocZSNaclRSUMNDS: TFloatField;
    quDocZSNaclNAME: TStringField;
    taNums: TADOQuery;
    taNumsIT: TSmallintField;
    taNumsSPRE: TStringField;
    taNumsCURNUM: TLargeintField;
    quDocZH: TADOQuery;
    quDocZHID: TLargeintField;
    quDocZHDOCDATE: TWideStringField;
    quDocZHDOCDATEZ: TWideStringField;
    quDocZHIDATE: TIntegerField;
    quDocZHIDATEZ: TIntegerField;
    quDocZHDAYSTOZ: TSmallintField;
    quDocZHDOCNUM: TStringField;
    quDocZHISHOP: TIntegerField;
    quDocZHDEPART: TIntegerField;
    quDocZHITYPE: TSmallintField;
    quDocZHINSUMIN: TFloatField;
    quDocZHINSUMIN0: TFloatField;
    quDocZHDEPARTORG: TIntegerField;
    quDocZHCLITYPE: TSmallintField;
    quDocZHCLICODE: TIntegerField;
    quDocZHIACTIVE: TSmallintField;
    quDocZHDATEEDIT: TDateTimeField;
    quDocZHPERSON: TStringField;
    quDocZHCLISUMIN: TFloatField;
    quDocZHCLISUMIN0: TFloatField;
    quDocZHCLISUMINN: TFloatField;
    quDocZHCLISUMIN0N: TFloatField;
    quDocZHQUANT: TFloatField;
    quDocZHCLIQUANT: TFloatField;
    quDocZHCLIQUANTN: TFloatField;
    quDocZHQUANTZAUTO: TFloatField;
    quDocZHIDATEPOST: TIntegerField;
    quDocZHIDATENACL: TIntegerField;
    quDocZHSNUMNACL: TStringField;
    quDocZHIDATESCHF: TIntegerField;
    quDocZHSNUMSCHF: TStringField;
    quDocZHSENDTO: TSmallintField;
    quDocZHSENDDATE: TDateTimeField;
    quDocZHSNUMPODTV: TWideStringField;
    quDocZHSENDNACL: TSmallintField;
    quDocZHIDHPRO: TIntegerField;
    quCliSel: TADOQuery;
    quCliSelId: TAutoIncField;
    quCliSelINN: TStringField;
    quCliSelName: TStringField;
    quCliSelFullName: TStringField;
    quCliSelGln: TWideStringField;
    quCliSelEDIProvider: TIntegerField;
    quCliSelPayNDS: TBooleanField;
    quCliSelEmail: TWideStringField;
    quCliSelIActive: TSmallintField;
    quFindDep: TADOQuery;
    quFindDepId_Shop: TIntegerField;
    quFindDepId: TIntegerField;
    quFindDepName: TStringField;
    quFindDepFullName: TStringField;
    quFindDepIStatus: TSmallintField;
    quFindDepIdOrg: TIntegerField;
    quFindDepGLN: TStringField;
    quFindDepAddres: TStringField;
    quFCardPro: TADOQuery;
    quFCardProiCode: TIntegerField;
    quFCardProKb: TFloatField;
    quFCardProiCodePro: TIntegerField;
    quFCardProiM: TIntegerField;
    quFCardProKm: TFloatField;
    quFCardProNamePro: TStringField;
    quFCardProSM: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

Function prGetNumZ(iT,iAdd:INteger):String;
procedure prFindFromGLNDep(sGln:String; Var IShop,iDep,iOrg:INteger);
procedure prFindFromGLNCli(sGln:String; Var iCli:INteger);

var
  dmCB: TdmCB;

implementation

uses Un1;

{$R *.dfm}

Function prGetNumZ(iT,iAdd:INteger):String;
var iNum:Integer;
    sPre:String;
    sMess:String;
begin
  Result:='';
  with dmCB do
  begin
    taNums.Active:=False;
    taNums.Parameters.ParamByName('ITYPE').Value:=iT;
    taNums.Active:=True;

    taNums.First;

    if taNums.RecordCount>0  then
    begin
      iNum:=taNumsCURNUM.AsInteger+1;
      sPre:=taNumsSPRE.AsString;
      sMess:=its(iNum);
      while length(sMess)<6 do sMess:='0'+sMess;
      Result:=TrimStr(sPre)+sMess;
      if iAdd=1 then
      begin
        taNums.Edit;
        taNumsCURNUM.AsInteger:=iNum;
        taNums.Post;
      end;
    end;

  end;
end;

procedure prFindFromGLNDep(sGln:String; Var IShop,iDep,iOrg:INteger);
begin
  iShop:=0; iDep:=0; iOrg:=0;
  with dmCB do
  begin
    quFindDep.Active:=False;
    quFindDep.Parameters.ParamByName('SGLN').Value:=sGln;
    quFindDep.Active:=True;
    if quFindDep.RecordCount>0 then
    begin
      quFindDep.First;
      iShop:=quFindDepId_Shop.asinteger;
      iDep:=quFindDepId.AsInteger;
      iOrg:=quFindDepIdOrg.AsInteger;
    end;
    quFindDep.Active:=False;
  end;
end;

procedure prFindFromGLNCli(sGln:String; Var iCli:INteger);
begin
  with dmCB do
  begin
    quCliSel.Active:=False;
    quCliSel.Parameters.ParamByName('SGLN').Value:=trim(sGln);
    quCliSel.Active:=True;
    if quCliSel.RecordCount>0 then iCli:=quCliSelId.AsInteger else iCli:=0;
    quCliSel.Active:=False;
  end;
end;



end.
