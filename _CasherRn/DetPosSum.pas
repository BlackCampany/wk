unit DetPosSum;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, dxmdaset, cxStyles, cxCustomData, cxGraphics, cxFilter,
  cxData, cxDataStorage, cxEdit, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGrid, cxContainer, cxTextEdit, cxMemo,
  cxImageComboBox;

type
  TfmDetPosSum = class(TForm)
    teDocLn: TdxMemData;
    teDocLnITypeD: TIntegerField;
    teDocLnIdDoc: TIntegerField;
    teDocLnNumDoc: TStringField;
    teDocLnIDateD: TIntegerField;
    teDocLnDateDoc: TDateField;
    teDocLnPriceIn0: TFloatField;
    teDocLnSumIn0: TFloatField;
    teDocLnPriceIn: TFloatField;
    teDocLnSumIn: TFloatField;
    teDocLnPriceOut: TFloatField;
    teDocLnSumOut: TFloatField;
    teDocLnQuantIn: TFloatField;
    teDocLnQuantOut: TFloatField;
    dsteDocLn: TDataSource;
    ViewPosDetDoc: TcxGridDBTableView;
    LevelPosDetD: TcxGridLevel;
    GridPosDetD: TcxGrid;
    ViewPosDetDocRecId: TcxGridDBColumn;
    ViewPosDetDocITypeD: TcxGridDBColumn;
    ViewPosDetDocIdDoc: TcxGridDBColumn;
    ViewPosDetDocNumDoc: TcxGridDBColumn;
    ViewPosDetDocIDateD: TcxGridDBColumn;
    ViewPosDetDocDateDoc: TcxGridDBColumn;
    ViewPosDetDocQuantIn: TcxGridDBColumn;
    ViewPosDetDocQuantOut: TcxGridDBColumn;
    ViewPosDetDocPriceIn0: TcxGridDBColumn;
    ViewPosDetDocSumIn0: TcxGridDBColumn;
    ViewPosDetDocPriceIn: TcxGridDBColumn;
    ViewPosDetDocSumIn: TcxGridDBColumn;
    ViewPosDetDocPriceOut: TcxGridDBColumn;
    ViewPosDetDocSumOut: TcxGridDBColumn;
    Memo1: TcxMemo;
    teDocLnPriceOut0: TFloatField;
    teDocLnSumOut0: TFloatField;
    ViewPosDetDocPriceOut0: TcxGridDBColumn;
    ViewPosDetDocSumOut0: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmDetPosSum: TfmDetPosSum;

implementation

uses dmOffice;

{$R *.dfm}

procedure TfmDetPosSum.FormCreate(Sender: TObject);
begin
  GridPosDetD.Align:=AlClient;
  Memo1.Clear;
end;

end.
