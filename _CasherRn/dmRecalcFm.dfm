object dmRecalc: TdmRecalc
  OldCreateOrder = False
  Left = 264
  Top = 110
  Height = 408
  Width = 336
  object prDelPer: TpFIBStoredProc
    Transaction = trD
    Database = dmO.OfficeRnDb
    SQL.Strings = (
      'EXECUTE PROCEDURE PR_DELPER (?IDATE, ?ISKL)')
    StoredProcName = 'PR_DELPER'
    Left = 28
    Top = 16
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object prTestPart: TpFIBStoredProc
    Transaction = trD
    Database = dmO.OfficeRnDb
    SQL.Strings = (
      'EXECUTE PROCEDURE PR_RECALCPARTREMN ')
    StoredProcName = 'PR_RECALCPARTREMN'
    Left = 100
    Top = 20
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object quDIn: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHEADIN'
      'SET '
      '    IDCLI = :IDCLI,'
      '    IDSKL = :IDSKL,'
      '    SUMIN = :SUMIN,'
      '    SUMUCH = :SUMUCH,'
      '    SUMTAR = :SUMTAR,'
      '    SUMNDS0 = :SUMNDS0,'
      '    SUMNDS1 = :SUMNDS1,'
      '    SUMNDS2 = :SUMNDS2,'
      '    PROCNAC = :PROCNAC,'
      '    IACTIVE = :IACTIVE'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHEADIN'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHEADIN('
      '    ID,'
      '    IDCLI,'
      '    IDSKL,'
      '    SUMIN,'
      '    SUMUCH,'
      '    SUMTAR,'
      '    SUMNDS0,'
      '    SUMNDS1,'
      '    SUMNDS2,'
      '    PROCNAC,'
      '    IACTIVE'
      ')'
      'VALUES('
      '    :ID,'
      '    :IDCLI,'
      '    :IDSKL,'
      '    :SUMIN,'
      '    :SUMUCH,'
      '    :SUMTAR,'
      '    :SUMNDS0,'
      '    :SUMNDS1,'
      '    :SUMNDS2,'
      '    :PROCNAC,'
      '    :IACTIVE'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT ID,IDCLI,IDSKL,SUMIN,SUMUCH,SUMTAR,SUMNDS0,SUMNDS1,SUMNDS' +
        '2,PROCNAC,IACTIVE'
      'FROM OF_DOCHEADIN'
      'where(  DATEDOC=:DDATE and IACTIVE>0'
      '     ) and (     OF_DOCHEADIN.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT ID,IDCLI,IDSKL,SUMIN,SUMUCH,SUMTAR,SUMNDS0,SUMNDS1,SUMNDS' +
        '2,PROCNAC,IACTIVE'
      'FROM OF_DOCHEADIN'
      'where DATEDOC=:DDATE and IACTIVE>0'
      'and IDSKL=:ISKL'
      'order by ID')
    Transaction = trS
    Database = dmO.OfficeRnDb
    UpdateTransaction = trU
    AutoCommit = True
    Left = 180
    Top = 20
    object quDInID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quDInIDCLI: TFIBIntegerField
      FieldName = 'IDCLI'
    end
    object quDInIDSKL: TFIBIntegerField
      FieldName = 'IDSKL'
    end
    object quDInSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quDInSUMUCH: TFIBFloatField
      FieldName = 'SUMUCH'
    end
    object quDInSUMTAR: TFIBFloatField
      FieldName = 'SUMTAR'
    end
    object quDInSUMNDS0: TFIBFloatField
      FieldName = 'SUMNDS0'
    end
    object quDInSUMNDS1: TFIBFloatField
      FieldName = 'SUMNDS1'
    end
    object quDInSUMNDS2: TFIBFloatField
      FieldName = 'SUMNDS2'
    end
    object quDInPROCNAC: TFIBFloatField
      FieldName = 'PROCNAC'
    end
    object quDInIACTIVE: TFIBIntegerField
      FieldName = 'IACTIVE'
    end
  end
  object quDInv: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHEADINV'
      'SET '
      '    DATEDOC = :DATEDOC,'
      '    NUMDOC = :NUMDOC,'
      '    IDSKL = :IDSKL,'
      '    IACTIVE = :IACTIVE,'
      '    ITYPE = :ITYPE,'
      '    SUM1 = :SUM1,'
      '    SUM11 = :SUM11,'
      '    SUM2 = :SUM2,'
      '    SUM21 = :SUM21'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHEADINV'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHEADINV('
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    IDSKL,'
      '    IACTIVE,'
      '    ITYPE,'
      '    SUM1,'
      '    SUM11,'
      '    SUM2,'
      '    SUM21'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATEDOC,'
      '    :NUMDOC,'
      '    :IDSKL,'
      '    :IACTIVE,'
      '    :ITYPE,'
      '    :SUM1,'
      '    :SUM11,'
      '    :SUM2,'
      '    :SUM21'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    IDSKL,'
      '    IACTIVE,'
      '    ITYPE,'
      '    SUM1,'
      '    SUM11,'
      '    SUM2,'
      '    SUM21'
      'FROM'
      '    OF_DOCHEADINV '
      'where(  DATEDOC=:DDATE and IACTIVE>0'
      '     ) and (     OF_DOCHEADINV.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    IDSKL,'
      '    IACTIVE,'
      '    ITYPE,'
      '    SUM1,'
      '    SUM11,'
      '    SUM2,'
      '    SUM21'
      'FROM'
      '    OF_DOCHEADINV '
      'where DATEDOC=:DDATE and IACTIVE>0'
      'and IDSKL=:ISKL'
      'order by ID'
      '')
    Transaction = trS
    Database = dmO.OfficeRnDb
    UpdateTransaction = trU
    AutoCommit = True
    Left = 260
    Top = 20
    object quDInvID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quDInvDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object quDInvNUMDOC: TFIBStringField
      FieldName = 'NUMDOC'
      Size = 15
      EmptyStrToNull = True
    end
    object quDInvIDSKL: TFIBIntegerField
      FieldName = 'IDSKL'
    end
    object quDInvIACTIVE: TFIBIntegerField
      FieldName = 'IACTIVE'
    end
    object quDInvITYPE: TFIBIntegerField
      FieldName = 'ITYPE'
    end
    object quDInvSUM1: TFIBFloatField
      FieldName = 'SUM1'
    end
    object quDInvSUM11: TFIBFloatField
      FieldName = 'SUM11'
    end
    object quDInvSUM2: TFIBFloatField
      FieldName = 'SUM2'
    end
    object quDInvSUM21: TFIBFloatField
      FieldName = 'SUM21'
    end
  end
  object trS: TpFIBTransaction
    DefaultDatabase = dmO.OfficeRnDb
    TimeoutAction = TARollback
    Left = 28
    Top = 84
  end
  object prSetSpecActive: TpFIBStoredProc
    Transaction = trD
    Database = dmO.OfficeRnDb
    SQL.Strings = (
      'EXECUTE PROCEDURE PR_SETSPECACTIVE (?DDATE, ?ATYPE, ?ISKL)')
    StoredProcName = 'PR_SETSPECACTIVE'
    Left = 100
    Top = 84
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object quDCompl: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHEADCOMPL'
      'SET '
      '    DATEDOC = :DATEDOC,'
      '    NUMDOC = :NUMDOC,'
      '    IDSKL = :IDSKL,'
      '    SUMIN = :SUMIN,'
      '    SUMUCH = :SUMUCH,'
      '    SUMTAR = :SUMTAR,'
      '    PROCNAC = :PROCNAC,'
      '    IACTIVE = :IACTIVE,'
      '    OPER = :OPER,'
      '    IDSKLTO = :IDSKLTO'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHEADCOMPL'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHEADCOMPL('
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    IDSKL,'
      '    SUMIN,'
      '    SUMUCH,'
      '    SUMTAR,'
      '    PROCNAC,'
      '    IACTIVE,'
      '    OPER,'
      '    IDSKLTO'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATEDOC,'
      '    :NUMDOC,'
      '    :IDSKL,'
      '    :SUMIN,'
      '    :SUMUCH,'
      '    :SUMTAR,'
      '    :PROCNAC,'
      '    :IACTIVE,'
      '    :OPER,'
      '    :IDSKLTO'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT ID,DATEDOC,NUMDOC,IDSKL,SUMIN,SUMUCH,SUMTAR,PROCNAC,IACTI' +
        'VE,OPER,IDSKLTO'
      'FROM OF_DOCHEADCOMPL'
      'where(  DATEDOC=:DDATE and IACTIVE>0'
      '     ) and (     OF_DOCHEADCOMPL.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT ID,DATEDOC,NUMDOC,IDSKL,SUMIN,SUMUCH,SUMTAR,PROCNAC,IACTI' +
        'VE,OPER,IDSKLTO'
      'FROM OF_DOCHEADCOMPL'
      'where DATEDOC=:DDATE and IACTIVE>0'
      'and ((IDSKL=:ISKL) or (IDSKLTO=:ISKL1))'
      'order by ID'
      ''
      ''
      '')
    Transaction = trS
    Database = dmO.OfficeRnDb
    UpdateTransaction = trU
    AutoCommit = True
    Left = 180
    Top = 76
    object quDComplID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quDComplDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object quDComplNUMDOC: TFIBStringField
      FieldName = 'NUMDOC'
      Size = 15
      EmptyStrToNull = True
    end
    object quDComplIDSKL: TFIBIntegerField
      FieldName = 'IDSKL'
    end
    object quDComplSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quDComplSUMUCH: TFIBFloatField
      FieldName = 'SUMUCH'
    end
    object quDComplSUMTAR: TFIBFloatField
      FieldName = 'SUMTAR'
    end
    object quDComplPROCNAC: TFIBFloatField
      FieldName = 'PROCNAC'
    end
    object quDComplIACTIVE: TFIBIntegerField
      FieldName = 'IACTIVE'
    end
    object quDComplOPER: TFIBStringField
      FieldName = 'OPER'
      EmptyStrToNull = True
    end
    object quDComplIDSKLTO: TFIBIntegerField
      FieldName = 'IDSKLTO'
    end
  end
  object quDR: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHEADOUTR'
      'SET '
      '    DATEDOC = :DATEDOC,'
      '    NUMDOC = :NUMDOC,'
      '    DATESF = :DATESF,'
      '    NUMSF = :NUMSF,'
      '    IDCLI = :IDCLI,'
      '    IDSKL = :IDSKL,'
      '    SUMIN = :SUMIN,'
      '    SUMUCH = :SUMUCH,'
      '    SUMTAR = :SUMTAR,'
      '    SUMNDS0 = :SUMNDS0,'
      '    SUMNDS1 = :SUMNDS1,'
      '    SUMNDS2 = :SUMNDS2,'
      '    PROCNAC = :PROCNAC,'
      '    IACTIVE = :IACTIVE'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHEADOUTR'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHEADOUTR('
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    DATESF,'
      '    NUMSF,'
      '    IDCLI,'
      '    IDSKL,'
      '    SUMIN,'
      '    SUMUCH,'
      '    SUMTAR,'
      '    SUMNDS0,'
      '    SUMNDS1,'
      '    SUMNDS2,'
      '    PROCNAC,'
      '    IACTIVE'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATEDOC,'
      '    :NUMDOC,'
      '    :DATESF,'
      '    :NUMSF,'
      '    :IDCLI,'
      '    :IDSKL,'
      '    :SUMIN,'
      '    :SUMUCH,'
      '    :SUMTAR,'
      '    :SUMNDS0,'
      '    :SUMNDS1,'
      '    :SUMNDS2,'
      '    :PROCNAC,'
      '    :IACTIVE'
      ')')
    RefreshSQL.Strings = (
      'SELECT ID,DATEDOC,NUMDOC,DATESF,NUMSF,IDCLI,IDSKL,SUMIN,SUMUCH,'
      '    SUMTAR,SUMNDS0,SUMNDS1,SUMNDS2,PROCNAC,IACTIVE'
      'FROM'
      '    OF_DOCHEADOUTR '
      'where(  DATEDOC=:DDATE and IACTIVE>0'
      '     ) and (     OF_DOCHEADOUTR.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT ID,DATEDOC,NUMDOC,DATESF,NUMSF,IDCLI,IDSKL,SUMIN,SUMUCH,'
      '    SUMTAR,SUMNDS0,SUMNDS1,SUMNDS2,PROCNAC,IACTIVE'
      'FROM'
      '    OF_DOCHEADOUTR '
      'where DATEDOC=:DDATE and IACTIVE>0'
      'and IDSKL=:ISKL'
      'order by SUMUCH desc'
      '')
    Transaction = trS
    Database = dmO.OfficeRnDb
    UpdateTransaction = trU
    AutoCommit = True
    Left = 260
    Top = 76
    object quDRID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quDRDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object quDRNUMDOC: TFIBStringField
      FieldName = 'NUMDOC'
      Size = 15
      EmptyStrToNull = True
    end
    object quDRDATESF: TFIBDateField
      FieldName = 'DATESF'
    end
    object quDRNUMSF: TFIBStringField
      FieldName = 'NUMSF'
      Size = 15
      EmptyStrToNull = True
    end
    object quDRIDCLI: TFIBIntegerField
      FieldName = 'IDCLI'
    end
    object quDRIDSKL: TFIBIntegerField
      FieldName = 'IDSKL'
    end
    object quDRSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quDRSUMUCH: TFIBFloatField
      FieldName = 'SUMUCH'
    end
    object quDRSUMTAR: TFIBFloatField
      FieldName = 'SUMTAR'
    end
    object quDRSUMNDS0: TFIBFloatField
      FieldName = 'SUMNDS0'
    end
    object quDRSUMNDS1: TFIBFloatField
      FieldName = 'SUMNDS1'
    end
    object quDRSUMNDS2: TFIBFloatField
      FieldName = 'SUMNDS2'
    end
    object quDRPROCNAC: TFIBFloatField
      FieldName = 'PROCNAC'
    end
    object quDRIACTIVE: TFIBIntegerField
      FieldName = 'IACTIVE'
    end
  end
  object trD: TpFIBTransaction
    DefaultDatabase = dmO.OfficeRnDb
    TimeoutAction = TARollback
    Left = 28
    Top = 140
  end
  object quDAct: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHEADACTS'
      'SET '
      '    DATEDOC = :DATEDOC,'
      '    NUMDOC = :NUMDOC,'
      '    IDSKL = :IDSKL,'
      '    SUMIN = :SUMIN,'
      '    SUMUCH = :SUMUCH,'
      '    IACTIVE = :IACTIVE,'
      '    OPER = :OPER,'
      '    COMMENT = :COMMENT'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHEADACTS'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHEADACTS('
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    IDSKL,'
      '    SUMIN,'
      '    SUMUCH,'
      '    IACTIVE,'
      '    OPER,'
      '    COMMENT'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATEDOC,'
      '    :NUMDOC,'
      '    :IDSKL,'
      '    :SUMIN,'
      '    :SUMUCH,'
      '    :IACTIVE,'
      '    :OPER,'
      '    :COMMENT'
      ')')
    RefreshSQL.Strings = (
      'SELECT ID,DATEDOC,NUMDOC,IDSKL,SUMIN,SUMUCH,IACTIVE,OPER,COMMENT'
      'FROM OF_DOCHEADACTS '
      'where(  DATEDOC=:DDATE and IACTIVE>0'
      '     ) and (     OF_DOCHEADACTS.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT ID,DATEDOC,NUMDOC,IDSKL,SUMIN,SUMUCH,IACTIVE,OPER,COMMENT'
      'FROM OF_DOCHEADACTS '
      'where DATEDOC=:DDATE and IACTIVE>0'
      'and IDSKL=:ISKL'
      'order by ID'
      '')
    Transaction = trS
    Database = dmO.OfficeRnDb
    UpdateTransaction = trU
    AutoCommit = True
    Left = 180
    Top = 132
    object quDActID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quDActDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object quDActNUMDOC: TFIBStringField
      FieldName = 'NUMDOC'
      Size = 15
      EmptyStrToNull = True
    end
    object quDActIDSKL: TFIBIntegerField
      FieldName = 'IDSKL'
    end
    object quDActSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quDActSUMUCH: TFIBFloatField
      FieldName = 'SUMUCH'
    end
    object quDActIACTIVE: TFIBIntegerField
      FieldName = 'IACTIVE'
    end
    object quDActOPER: TFIBStringField
      FieldName = 'OPER'
      EmptyStrToNull = True
    end
    object quDActCOMMENT: TFIBStringField
      FieldName = 'COMMENT'
      Size = 200
      EmptyStrToNull = True
    end
  end
  object trU: TpFIBTransaction
    DefaultDatabase = dmO.OfficeRnDb
    TimeoutAction = TARollback
    Left = 28
    Top = 196
  end
  object quMHAll1: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT MH.ID,MH.PARENT,MH.ITYPE,MH.NAMEMH,'
      '       MH.DEFPRICE,PT.NAMEPRICE'
      'FROM OF_MH MH'
      'left JOIN OF_PRICETYPE PT ON PT.ID=MH.DEFPRICE'
      'WHERE MH.ITYPE=1'
      'Order by MH.ID')
    Transaction = trS1
    Database = dmO.OfficeRnDb
    Left = 104
    Top = 212
    object quMHAll1ID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quMHAll1PARENT: TFIBIntegerField
      FieldName = 'PARENT'
    end
    object quMHAll1ITYPE: TFIBIntegerField
      FieldName = 'ITYPE'
    end
    object quMHAll1NAMEMH: TFIBStringField
      FieldName = 'NAMEMH'
      Size = 100
      EmptyStrToNull = True
    end
    object quMHAll1DEFPRICE: TFIBIntegerField
      FieldName = 'DEFPRICE'
    end
    object quMHAll1NAMEPRICE: TFIBStringField
      FieldName = 'NAMEPRICE'
      Size = 100
      EmptyStrToNull = True
    end
  end
  object quDVn: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHEADVN'
      'SET '
      '    DATEDOC = :DATEDOC,'
      '    NUMDOC = :NUMDOC,'
      '    IDSKL_FROM = :IDSKL_FROM,'
      '    IDSKL_TO = :IDSKL_TO,'
      '    SUMIN = :SUMIN,'
      '    SUMUCH = :SUMUCH,'
      '    SUMUCH1 = :SUMUCH1,'
      '    SUMTAR = :SUMTAR,'
      '    PROCNAC = :PROCNAC,'
      '    IACTIVE = :IACTIVE'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHEADVN'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHEADVN('
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    IDSKL_FROM,'
      '    IDSKL_TO,'
      '    SUMIN,'
      '    SUMUCH,'
      '    SUMUCH1,'
      '    SUMTAR,'
      '    PROCNAC,'
      '    IACTIVE'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATEDOC,'
      '    :NUMDOC,'
      '    :IDSKL_FROM,'
      '    :IDSKL_TO,'
      '    :SUMIN,'
      '    :SUMUCH,'
      '    :SUMUCH1,'
      '    :SUMTAR,'
      '    :PROCNAC,'
      '    :IACTIVE'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT ID,DATEDOC,NUMDOC,IDSKL_FROM,IDSKL_TO,SUMIN,SUMUCH,SUMUCH' +
        '1,SUMTAR,PROCNAC,IACTIVE'
      'FROM OF_DOCHEADVN '
      'where(  DATEDOC=:DDATE and IACTIVE>0'
      '     ) and (     OF_DOCHEADVN.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT ID,DATEDOC,NUMDOC,IDSKL_FROM,IDSKL_TO,SUMIN,SUMUCH,SUMUCH' +
        '1,SUMTAR,PROCNAC,IACTIVE'
      'FROM OF_DOCHEADVN '
      'where DATEDOC=:DDATE and IACTIVE>0'
      'and ((IDSKL_FROM=:ISKL) or (IDSKL_TO=:ISKL1))'
      'order by ID'
      '')
    Transaction = trS
    Database = dmO.OfficeRnDb
    UpdateTransaction = trU
    AutoCommit = True
    Left = 180
    Top = 188
    object quDVnID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quDVnDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object quDVnNUMDOC: TFIBStringField
      FieldName = 'NUMDOC'
      Size = 15
      EmptyStrToNull = True
    end
    object quDVnIDSKL_FROM: TFIBIntegerField
      FieldName = 'IDSKL_FROM'
    end
    object quDVnIDSKL_TO: TFIBIntegerField
      FieldName = 'IDSKL_TO'
    end
    object quDVnSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quDVnSUMUCH: TFIBFloatField
      FieldName = 'SUMUCH'
    end
    object quDVnSUMUCH1: TFIBFloatField
      FieldName = 'SUMUCH1'
    end
    object quDVnSUMTAR: TFIBFloatField
      FieldName = 'SUMTAR'
    end
    object quDVnPROCNAC: TFIBFloatField
      FieldName = 'PROCNAC'
    end
    object quDVnIACTIVE: TFIBIntegerField
      FieldName = 'IACTIVE'
    end
  end
  object trS1: TpFIBTransaction
    DefaultDatabase = dmO.OfficeRnDb
    TimeoutAction = TARollback
    Left = 16
    Top = 252
  end
  object PRNORMPARTIN: TpFIBStoredProc
    Transaction = trU
    Database = dmO.OfficeRnDb
    SQL.Strings = (
      
        'EXECUTE PROCEDURE PR_NORMPARTIN (?IDSKL, ?IDATE, ?IDCARD, ?QUANT' +
        ')')
    StoredProcName = 'PR_NORMPARTIN'
    Left = 104
    Top = 268
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object quDRet: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHEADOUT'
      'SET '
      '    DATEDOC = :DATEDOC,'
      '    NUMDOC = :NUMDOC,'
      '    DATESF = :DATESF,'
      '    NUMSF = :NUMSF,'
      '    IDCLI = :IDCLI,'
      '    IDSKL = :IDSKL,'
      '    SUMIN = :SUMIN,'
      '    SUMUCH = :SUMUCH,'
      '    SUMTAR = :SUMTAR,'
      '    SUMNDS0 = :SUMNDS0,'
      '    SUMNDS1 = :SUMNDS1,'
      '    SUMNDS2 = :SUMNDS2,'
      '    PROCNAC = :PROCNAC,'
      '    IACTIVE = :IACTIVE'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHEADOUT'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHEADOUT('
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    DATESF,'
      '    NUMSF,'
      '    IDCLI,'
      '    IDSKL,'
      '    SUMIN,'
      '    SUMUCH,'
      '    SUMTAR,'
      '    SUMNDS0,'
      '    SUMNDS1,'
      '    SUMNDS2,'
      '    PROCNAC,'
      '    IACTIVE'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATEDOC,'
      '    :NUMDOC,'
      '    :DATESF,'
      '    :NUMSF,'
      '    :IDCLI,'
      '    :IDSKL,'
      '    :SUMIN,'
      '    :SUMUCH,'
      '    :SUMTAR,'
      '    :SUMNDS0,'
      '    :SUMNDS1,'
      '    :SUMNDS2,'
      '    :PROCNAC,'
      '    :IACTIVE'
      ')')
    RefreshSQL.Strings = (
      
        'SELECT ID,DATEDOC,NUMDOC,DATESF,NUMSF,IDCLI,IDSKL,SUMIN,SUMUCH,S' +
        'UMTAR,SUMNDS0,'
      '    SUMNDS1,SUMNDS2,PROCNAC,IACTIVE'
      'FROM'
      '    OF_DOCHEADOUT '
      'where(  DATEDOC=:DDATE and IACTIVE>0'
      '     ) and (     OF_DOCHEADOUT.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'SELECT ID,DATEDOC,NUMDOC,DATESF,NUMSF,IDCLI,IDSKL,SUMIN,SUMUCH,S' +
        'UMTAR,SUMNDS0,'
      '    SUMNDS1,SUMNDS2,PROCNAC,IACTIVE'
      'FROM'
      '    OF_DOCHEADOUT '
      'where DATEDOC=:DDATE and IACTIVE>0'
      'and IDSKL=:ISKL'
      'order by ID'
      '')
    Transaction = trS
    Database = dmO.OfficeRnDb
    UpdateTransaction = trU
    AutoCommit = True
    Left = 180
    Top = 244
    object quDRetID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quDRetDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object quDRetNUMDOC: TFIBStringField
      FieldName = 'NUMDOC'
      Size = 15
      EmptyStrToNull = True
    end
    object quDRetDATESF: TFIBDateField
      FieldName = 'DATESF'
    end
    object quDRetNUMSF: TFIBStringField
      FieldName = 'NUMSF'
      Size = 15
      EmptyStrToNull = True
    end
    object quDRetIDCLI: TFIBIntegerField
      FieldName = 'IDCLI'
    end
    object quDRetIDSKL: TFIBIntegerField
      FieldName = 'IDSKL'
    end
    object quDRetSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quDRetSUMUCH: TFIBFloatField
      FieldName = 'SUMUCH'
    end
    object quDRetSUMTAR: TFIBFloatField
      FieldName = 'SUMTAR'
    end
    object quDRetSUMNDS0: TFIBFloatField
      FieldName = 'SUMNDS0'
    end
    object quDRetSUMNDS1: TFIBFloatField
      FieldName = 'SUMNDS1'
    end
    object quDRetSUMNDS2: TFIBFloatField
      FieldName = 'SUMNDS2'
    end
    object quDRetPROCNAC: TFIBFloatField
      FieldName = 'PROCNAC'
    end
    object quDRetIACTIVE: TFIBIntegerField
      FieldName = 'IACTIVE'
    end
  end
  object quC: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHEADOUTR'
      'SET '
      '    DATEDOC = :DATEDOC,'
      '    NUMDOC = :NUMDOC,'
      '    DATESF = :DATESF,'
      '    NUMSF = :NUMSF,'
      '    IDCLI = :IDCLI,'
      '    IDSKL = :IDSKL,'
      '    SUMIN = :SUMIN,'
      '    SUMUCH = :SUMUCH,'
      '    SUMTAR = :SUMTAR,'
      '    SUMNDS0 = :SUMNDS0,'
      '    SUMNDS1 = :SUMNDS1,'
      '    SUMNDS2 = :SUMNDS2,'
      '    PROCNAC = :PROCNAC,'
      '    IACTIVE = :IACTIVE'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHEADOUTR'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHEADOUTR('
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    DATESF,'
      '    NUMSF,'
      '    IDCLI,'
      '    IDSKL,'
      '    SUMIN,'
      '    SUMUCH,'
      '    SUMTAR,'
      '    SUMNDS0,'
      '    SUMNDS1,'
      '    SUMNDS2,'
      '    PROCNAC,'
      '    IACTIVE'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATEDOC,'
      '    :NUMDOC,'
      '    :DATESF,'
      '    :NUMSF,'
      '    :IDCLI,'
      '    :IDSKL,'
      '    :SUMIN,'
      '    :SUMUCH,'
      '    :SUMTAR,'
      '    :SUMNDS0,'
      '    :SUMNDS1,'
      '    :SUMNDS2,'
      '    :PROCNAC,'
      '    :IACTIVE'
      ')')
    RefreshSQL.Strings = (
      'SELECT ID,DATEDOC,NUMDOC,DATESF,NUMSF,IDCLI,IDSKL,SUMIN,SUMUCH,'
      '    SUMTAR,SUMNDS0,SUMNDS1,SUMNDS2,PROCNAC,IACTIVE'
      'FROM'
      '    OF_DOCHEADOUTR '
      'where(  DATEDOC=:DDATE and IACTIVE>0'
      '     ) and (     OF_DOCHEADOUTR.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT ID'
      'FROM OF_CARDS'
      'order by ID')
    Transaction = trS
    Database = dmO.OfficeRnDb
    UpdateTransaction = trU
    AutoCommit = True
    Left = 16
    Top = 304
    poAskRecordCount = True
    object quCID: TFIBIntegerField
      FieldName = 'ID'
    end
  end
  object quDOutB: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHEADOUTB'
      'SET '
      '    DATEDOC = :DATEDOC,'
      '    NUMDOC = :NUMDOC,'
      '    DATESF = :DATESF,'
      '    NUMSF = :NUMSF,'
      '    IDCLI = :IDCLI,'
      '    IDSKL = :IDSKL,'
      '    SUMIN = :SUMIN,'
      '    SUMUCH = :SUMUCH,'
      '    SUMTAR = :SUMTAR,'
      '    SUMNDS0 = :SUMNDS0,'
      '    SUMNDS1 = :SUMNDS1,'
      '    SUMNDS2 = :SUMNDS2,'
      '    PROCNAC = :PROCNAC,'
      '    IACTIVE = :IACTIVE,'
      '    OPER = :OPER'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHEADOUTB'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHEADOUTB('
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    DATESF,'
      '    NUMSF,'
      '    IDCLI,'
      '    IDSKL,'
      '    SUMIN,'
      '    SUMUCH,'
      '    SUMTAR,'
      '    SUMNDS0,'
      '    SUMNDS1,'
      '    SUMNDS2,'
      '    PROCNAC,'
      '    IACTIVE,'
      '    OPER'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATEDOC,'
      '    :NUMDOC,'
      '    :DATESF,'
      '    :NUMSF,'
      '    :IDCLI,'
      '    :IDSKL,'
      '    :SUMIN,'
      '    :SUMUCH,'
      '    :SUMTAR,'
      '    :SUMNDS0,'
      '    :SUMNDS1,'
      '    :SUMNDS2,'
      '    :PROCNAC,'
      '    :IACTIVE,'
      '    :OPER'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    DATESF,'
      '    NUMSF,'
      '    IDCLI,'
      '    IDSKL,'
      '    SUMIN,'
      '    SUMUCH,'
      '    SUMTAR,'
      '    SUMNDS0,'
      '    SUMNDS1,'
      '    SUMNDS2,'
      '    PROCNAC,'
      '    IACTIVE,'
      '    OPER'
      'FROM'
      '    OF_DOCHEADOUTB'
      'where(  DATEDOC=:DDATE and IACTIVE>0'
      '     ) and (     OF_DOCHEADOUTB.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    DATESF,'
      '    NUMSF,'
      '    IDCLI,'
      '    IDSKL,'
      '    SUMIN,'
      '    SUMUCH,'
      '    SUMTAR,'
      '    SUMNDS0,'
      '    SUMNDS1,'
      '    SUMNDS2,'
      '    PROCNAC,'
      '    IACTIVE,'
      '    OPER'
      'FROM'
      '    OF_DOCHEADOUTB'
      'where DATEDOC=:DDATE and IACTIVE>0'
      'and IDSKL=:ISKL'
      'order by OPER desc'
      ''
      ' ')
    Transaction = trS
    Database = dmO.OfficeRnDb
    UpdateTransaction = trU
    AutoCommit = True
    Left = 180
    Top = 300
    object quDOutBID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quDOutBDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object quDOutBNUMDOC: TFIBStringField
      FieldName = 'NUMDOC'
      Size = 15
      EmptyStrToNull = True
    end
    object quDOutBDATESF: TFIBDateField
      FieldName = 'DATESF'
    end
    object quDOutBNUMSF: TFIBStringField
      FieldName = 'NUMSF'
      Size = 15
      EmptyStrToNull = True
    end
    object quDOutBIDCLI: TFIBIntegerField
      FieldName = 'IDCLI'
    end
    object quDOutBIDSKL: TFIBIntegerField
      FieldName = 'IDSKL'
    end
    object quDOutBSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quDOutBSUMUCH: TFIBFloatField
      FieldName = 'SUMUCH'
    end
    object quDOutBSUMTAR: TFIBFloatField
      FieldName = 'SUMTAR'
    end
    object quDOutBSUMNDS0: TFIBFloatField
      FieldName = 'SUMNDS0'
    end
    object quDOutBSUMNDS1: TFIBFloatField
      FieldName = 'SUMNDS1'
    end
    object quDOutBSUMNDS2: TFIBFloatField
      FieldName = 'SUMNDS2'
    end
    object quDOutBPROCNAC: TFIBFloatField
      FieldName = 'PROCNAC'
    end
    object quDOutBIACTIVE: TFIBIntegerField
      FieldName = 'IACTIVE'
    end
    object quDOutBOPER: TFIBStringField
      FieldName = 'OPER'
      EmptyStrToNull = True
    end
  end
end
