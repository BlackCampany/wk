program Casher;

uses
  Forms,
  MainCasher in 'MainCasher.pas' {fmMainCasher},
  Dm in 'Dm.pas' {dmC: TDataModule},
  Un1 in 'Un1.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfmMainCasher, fmMainCasher);
  Application.CreateForm(TdmC, dmC);
  Application.Run;
end.
