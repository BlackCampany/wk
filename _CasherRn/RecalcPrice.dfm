object fmRecalcPrice: TfmRecalcPrice
  Left = 494
  Top = 446
  Width = 368
  Height = 278
  Caption = #1055#1077#1088#1077#1089#1095#1077#1090' '#1087#1088#1086#1076#1072#1078#1085#1099#1093' '#1094#1077#1085
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 24
    Width = 88
    Height = 13
    Caption = #1055#1088#1086#1094#1077#1085#1090' '#1085#1072#1094#1077#1085#1082#1080
  end
  object Label2: TLabel
    Left = 208
    Top = 24
    Width = 75
    Height = 13
    Caption = #1054#1082#1088#1091#1075#1083#1077#1085#1080#1077' '#1076#1086
  end
  object Panel1: TPanel
    Left = 0
    Top = 190
    Width = 360
    Height = 54
    Align = alBottom
    Color = 16756912
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 68
      Top = 12
      Width = 101
      Height = 33
      Caption = 'Ok'
      ModalResult = 1
      TabOrder = 0
      Colors.Normal = clCaptionText
      Colors.Hot = 14869218
      Colors.Pressed = clGray
      LookAndFeel.Kind = lfOffice11
      LookAndFeel.NativeStyle = False
    end
    object cxButton2: TcxButton
      Left = 192
      Top = 12
      Width = 101
      Height = 33
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      Colors.Normal = clCaptionText
      Colors.Hot = 14869218
      Colors.Pressed = clGray
      LookAndFeel.Kind = lfOffice11
      LookAndFeel.NativeStyle = False
    end
  end
  object cxCalcEdit1: TcxCalcEdit
    Left = 112
    Top = 20
    EditValue = 80.000000000000000000
    Properties.Alignment.Horz = taRightJustify
    Properties.DisplayFormat = '0.0'
    Style.BorderStyle = ebsOffice11
    Style.LookAndFeel.Kind = lfOffice11
    Style.Shadow = True
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 1
    Width = 69
  end
  object cxRadioGroup1: TcxRadioGroup
    Left = 16
    Top = 60
    Caption = #1052#1077#1090#1086#1076' '#1088#1072#1089#1095#1077#1090#1072
    Properties.Items = <
      item
        Caption = #1058#1086#1083#1100#1082#1086' '#1087#1086' '#1088#1072#1089#1095#1077#1090#1085#1099#1084' '#1094#1077#1085#1072#1084
        Value = 0
      end
      item
        Caption = #1055#1086' '#1088#1072#1089#1095#1077#1090#1085#1072#1084' '#1094#1077#1085#1072#1084', '#1087#1086' '#1094#1077#1085#1072#1084' '#1087#1086#1089#1083#1077#1076#1085#1077#1075#1086' '#1087#1088#1080#1093#1086#1076#1072
        Value = 1
      end
      item
        Caption = #1058#1086#1083#1100#1082#1086' '#1087#1086' '#1094#1077#1085#1072#1084' '#1087#1086#1089#1083#1077#1076#1085#1077#1075#1086' '#1087#1088#1080#1093#1086#1076#1072
        Value = 2
      end>
    ItemIndex = 0
    TabOrder = 2
    Height = 113
    Width = 329
  end
  object cxComboBox1: TcxComboBox
    Left = 292
    Top = 20
    Properties.Items.Strings = (
      '10.00'
      '1.00'
      '0.1'
      '0.01')
    Style.Shadow = True
    TabOrder = 3
    Text = '1.00'
    Width = 53
  end
end
