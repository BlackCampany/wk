unit GoodsSel;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, XPStyleActnCtrls, ActnList, ActnMan, ComCtrls, Placemnt,
  ToolWin, ActnCtrls, ActnMenus, cxControls, cxContainer, cxTreeView,
  ExtCtrls, SpeedBar, cxSplitter, cxStyles, cxCustomData, cxGraphics,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxDropDownEdit, cxImageComboBox,
  cxGridCustomPopupMenu, cxGridPopupMenu, cxTextEdit, StdCtrls, cxCalc,
  Menus, cxLookAndFeelPainters, cxButtons, pFIBDataSet,cxCheckBox;

type
  TfmGoodsSel = class(TForm)
    StatusBar1: TStatusBar;
    amG: TActionManager;
    FormPlacement1: TFormPlacement;
    ActionMainMenuBar1: TActionMainMenuBar;
    Action1: TAction;
    Action2: TAction;
    Action3: TAction;
    Action4: TAction;
    Panel1: TPanel;
    ClassTree: TcxTreeView;
    SpeedBar1: TSpeedBar;
    cxSplitter1: TcxSplitter;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    Action5: TAction;
    Action6: TAction;
    Action7: TAction;
    Action8: TAction;
    Timer1: TTimer;
    ViewGoodsSel: TcxGridDBTableView;
    LevelGoodsSel: TcxGridLevel;
    GrGoodsSel: TcxGrid;
    ViewGoodsSelID: TcxGridDBColumn;
    ViewGoodsSelPARENT: TcxGridDBColumn;
    ViewGoodsSelNAME: TcxGridDBColumn;
    ViewGoodsSelTTYPE: TcxGridDBColumn;
    ViewGoodsSelIMESSURE: TcxGridDBColumn;
    ViewGoodsSelINDS: TcxGridDBColumn;
    ViewGoodsSelMINREST: TcxGridDBColumn;
    ViewGoodsSelLASTPRICEIN: TcxGridDBColumn;
    ViewGoodsSelLASTPRICEOUT: TcxGridDBColumn;
    ViewGoodsSelLASTPOST: TcxGridDBColumn;
    ViewGoodsSelIACTIVE: TcxGridDBColumn;
    ViewGoodsSelNAMESHORT: TcxGridDBColumn;
    ViewGoodsSelNAMENDS: TcxGridDBColumn;
    ViewGoodsSelPROC: TcxGridDBColumn;
    cxGridPopupMenu1: TcxGridPopupMenu;
    Panel2: TPanel;
    Label1: TLabel;
    cxTextEdit1: TcxTextEdit;
    acAddGoods: TAction;
    acEditGoods: TAction;
    acDelGoods: TAction;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    PopupMenu2: TPopupMenu;
    N6: TMenuItem;
    N7: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    Action9: TAction;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    ViewGoodsSelTCARD: TcxGridDBColumn;
    acTCard: TAction;
    ViewGoodsSelCOMMENT: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure Action2Execute(Sender: TObject);
    procedure Action1Execute(Sender: TObject);
    procedure Action3Execute(Sender: TObject);
    procedure Action5Execute(Sender: TObject);
    procedure Action6Execute(Sender: TObject);
    procedure Action7Execute(Sender: TObject);
    procedure Action8Execute(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure ClassTreeChange(Sender: TObject; Node: TTreeNode);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acAddGoodsExecute(Sender: TObject);
    procedure acEditGoodsExecute(Sender: TObject);
    procedure acDelGoodsExecute(Sender: TObject);
    procedure Action9Execute(Sender: TObject);
    procedure ViewGoodsSelStartDrag(Sender: TObject;
      var DragObject: TDragObject);
    procedure ClassTreeDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure ClassTreeDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure ClassTreeExpanding(Sender: TObject; Node: TTreeNode;
      var AllowExpansion: Boolean);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure ViewGoodsSelDblClick(Sender: TObject);
    procedure acTCardExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

Function prCanSel(iParent,iChild:INteger):Boolean;

var
  fmGoodsSel: TfmGoodsSel;
  bClearGoodsSel:Boolean = False;
  bDr:Boolean = False;
implementation

uses dmOffice, Un1, AddClass, AddGoods, FindResult, AddDoc1, TCard, Goods;

{$R *.dfm}

Function prCanSel(iParent,iChild:Integer):Boolean;
Var iT:INteger;
    QT:TpFIBDataSet;
begin
  with dmO do
  begin
    Result:=True;
    if iParent=iChild then Result:=False
    else
    begin
      quS.Active:=False;
      quS.ParamByName('IID').AsInteger:=iChild;
      quS.Active:=True;
      iT:=quSTCARD.AsInteger;
      quS.Active:=False;
      if iT=1 then
      begin
        QT:=TpFIBDataSet.Create(Application);
        QT.Active:=False;
        QT.Database:=OfficeRnDb;
        QT.Transaction:=trSel;
        QT.SelectSQL.Clear;
        QT.SelectSQL.Add('SELECT IDC,IDCARD');
        QT.SelectSQL.Add('FROM OF_CARDSTSPEC');
        QT.SelectSQL.Add('where IDC='+IntToStr(iChild));
        QT.Active:=True;

        QT.First;
        while not QT.Eof do
        begin
          if prCanSel(iParent,QT.FieldByName('IDCARD').AsInteger)=False then
          begin
            Result:=False;
            Break;
          end;
          QT.Next;
        end;
        QT.Active:=False;
        QT.Free;
      end;
    end;
  end;
end;

procedure TfmGoodsSel.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;

  ClassTree.Items.BeginUpdate;
  ClassifExpand(nil,ClassTree,dmO.quClassTree,Person.Id,1);
  ClassTree.FullExpand;
  ClassTree.FullCollapse;
  ClassTree.Items.EndUpdate;

  Timer1.Enabled:=True;
  GrGoodsSel.Align:=AlClient;
  ViewGoodsSel.RestoreFromIniFile(CurDir+GridIni);
  cxTextEdit1.Text:='';
end;

procedure TfmGoodsSel.Action2Execute(Sender: TObject);
begin
//
end;

procedure TfmGoodsSel.Action1Execute(Sender: TObject);
begin
  bAddTSpec:=False;
  close;
end;

procedure TfmGoodsSel.Action3Execute(Sender: TObject);
begin
 //�������������
end;

procedure TfmGoodsSel.Action5Execute(Sender: TObject);
Var TreeNode : TTreeNode;
    iId,i:Integer;
    StrWk:String;
begin
// �������� ��������  ������
  if not CanDo('prAddClass') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  fmAddClass.Caption:='���������� ������.';
  fmAddClass.label4.Caption:='���������� ������.';
  fmAddClass.cxTextEdit1.Text:='';

  fmAddClass.ShowModal;
  if fmAddClass.ModalResult=mrOk then
  begin
    with dmO do
    begin
      iId:=GetId('Class');


      taClass.Active:=False;
      taClass.Active:=True;
      taClass.Append;
      taClassID.AsInteger:=iId;
      taClassID_PARENT.AsInteger:=0;
      taClassNAMECL.AsString:=Copy(fmAddClass.cxTextEdit1.Text,1,150);
      taClassITYPE.AsInteger:=1; //������������� �������
      taClass.Post;

      StrWk:=taClass.FieldByName('NAMECL').AsString;

      ClassTree.Items.BeginUpdate;
      TreeNode:=ClassTree.Items.AddChildObject(nil,StrWk,Pointer(iId));
      TreeNode.ImageIndex:=8;
      TreeNode.SelectedIndex:=7;
      ClassTree.Items.AddChildObject(TreeNode,'', nil);
      ClassTree.Items.EndUpdate;

      //�������� ����� � �������� ����
      with fmGoods do
      begin
        ClassTree.Items.BeginUpdate;
        TreeNode:=ClassTree.Items.AddChildObject(nil,StrWk,Pointer(iId));
        TreeNode.ImageIndex:=8;
        TreeNode.SelectedIndex:=7;
        ClassTree.Items.AddChildObject(TreeNode,'', nil);
        ClassTree.Items.EndUpdate;
        ClassTree.Repaint;
        delay(10);
      end;

      for i:=0 To ClassTree.Items.Count-1 do
        if Integer(ClassTree.Items[i].Data) = iId then
        begin
          ClassTree.Items[i].Expand(False);
          ClassTree.Items[i].Selected:=True;
          ClassTree.Repaint;
          Break;
        end;

      taClass.Active:=False;
    end;
  end;
end;

procedure TfmGoodsSel.Action6Execute(Sender: TObject);
Var TreeNode : TTreeNode;
    CurNode : TTreeNode;
    iId,i,iParent:Integer;
    StrWk:String;
begin
//�������� ���������
  if not CanDo('prAddSubClass') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  if ClassTree.Selected=nil then
  begin
    showmessage('�������� ������ ��� ���������� ���������.');
    exit;
  end;

  iParent:=Integer(ClassTree.Selected.Data);
  CurNode:=ClassTree.Selected;
  with dmO do
  begin
    taClass.Active:=False;
    taClass.Active:=True;
    if taClass.Locate('ID',iParent,[]) then
    begin
      fmAddClass.Caption:='���������� ��������� � ������ "'+CurNode.Text+'"';
      fmAddClass.label4.Caption:='���������� ��������� � ������ "'+CurNode.Text+'"';
      fmAddClass.cxTextEdit1.Text:='';

      fmAddClass.ShowModal;
      if fmAddClass.ModalResult=mrOk then
      begin
        iId:=GetId('Class');

        taClass.Active:=False;
        taClass.Active:=True;
        taClass.Append;
        taClassID.AsInteger:=iId;
        taClassID_PARENT.AsInteger:=iParent;
        taClassNAMECL.AsString:=Copy(fmAddClass.cxTextEdit1.Text,1,150);
        taClassITYPE.AsInteger:=1; //������������� �������
        taClass.Post;

        StrWk:=taClass.FieldByName('NAMECL').AsString;

        ClassTree.Items.BeginUpdate;
        TreeNode:=ClassTree.Items.AddChildObject(CurNode,StrWk,Pointer(iId));
        TreeNode.ImageIndex:=8;
        TreeNode.SelectedIndex:=7;
        ClassTree.Items.AddChildObject(TreeNode,'', nil);
        ClassTree.Items.EndUpdate;

        with fmGoods do
        begin
          for i:=0 To ClassTree.Items.Count-1 do
            if Integer(ClassTree.Items[i].Data) =iParent  then
            begin
              ClassTree.Items.BeginUpdate;
              TreeNode:=ClassTree.Items.AddChildObject(ClassTree.Items[i],StrWk,Pointer(iId));
              TreeNode.ImageIndex:=8;
              TreeNode.SelectedIndex:=7;
              ClassTree.Items.AddChildObject(TreeNode,'', nil);
              ClassTree.Items.EndUpdate;

              ClassTree.Repaint;
              Break;
            end;
        end;

        for i:=0 To ClassTree.Items.Count-1 do
          if Integer(ClassTree.Items[i].Data) = iId then
          begin
            ClassTree.Items[i].Expand(False);
            ClassTree.Items[i].Selected:=True;
            ClassTree.Repaint;
            Break;
          end;

      end;
    end;
    taClass.Active:=False;
  end;
end;

procedure TfmGoodsSel.Action7Execute(Sender: TObject);
Var CurNode : TTreeNode;
    iId,i:Integer;
begin
// �������������
  if not CanDo('prEditClass') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  if ClassTree.Selected=nil then
  begin
    showmessage('�������� ������ ��� ��������������.');
    exit;
  end;

  iId:=Integer(ClassTree.Selected.Data);
  CurNode:=ClassTree.Selected;
  with dmO do
  begin
    taClass.Active:=False;
    taClass.Active:=True;
    if taClass.Locate('ID',iId,[]) then
    begin
      fmAddClass.Caption:='�������������� ������ "'+taClassNAMECL.AsString+'"';
      fmAddClass.label4.Caption:='�������������� ������"'+taClassNAMECL.AsString+'"';
      fmAddClass.cxTextEdit1.Text:=taClassNAMECL.AsString;

      fmAddClass.ShowModal;
      if fmAddClass.ModalResult=mrOk then
      begin
        taClass.Edit;
        taClassNAMECL.AsString:=Copy(fmAddClass.cxTextEdit1.Text,1,150);
        taClassITYPE.AsInteger:=1; //������������� �������
        taClass.Post;

        CurNode.Text:=fmAddClass.cxTextEdit1.Text;

        with fmGoods do
        begin
          for i:=0 To ClassTree.Items.Count-1 do
            if Integer(ClassTree.Items[i].Data) = iId then
            begin
              ClassTree.Items[i].Text:=fmAddClass.cxTextEdit1.Text;
              ClassTree.Repaint;
              Break;
            end;
        end;
      end;
    end;
    taClass.Active:=False;
  end;
end;

procedure TfmGoodsSel.Action8Execute(Sender: TObject);
Var CurNode : TTreeNode;
    iRes:Integer;
    iId,i:Integer;
begin
//�������
  if not CanDo('prDelClass') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  if ClassTree.Selected=nil then
  begin
    showmessage('�������� ������ ��� ��������.');
    exit;
  end;

  CurNode:=ClassTree.Selected;
  with dmO do
  begin
    if MessageDlg('�� ������������� ������ ������� ������ "'+CurNode.Text+'"', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      prCanDelClass.ParamByName('IDCL').AsInteger:=Integer(CurNode.Data);
      prCanDelClass.ExecProc;
      iRes:=prCanDelClass.ParamByName('RESULT').AsInteger;

      if iRes=0 then
      begin //�������� ���������
        taClass.Active:=False;
        taClass.Active:=True;
        if taClass.Locate('ID',Integer(CurNode.Data),[]) then
        begin
          iId:=Integer(CurNode.Data);
          taClass.Delete;
          CurNode.Delete;
          ClassTree.Repaint;

          with fmGoods do
          begin
            for i:=0 To ClassTree.Items.Count-1 do
              if Integer(ClassTree.Items[i].Data)=iId then
              begin
                ClassTree.Items[i].Delete;
                ClassTree.Repaint;
                Break;
              end;
          end;
        end;
        taClass.Active:=False;
      end;

      if iRes=1 then
      begin //�������� ��������� - ���� ������ � ������ �����
        showmessage('�������� ���������! ���� ���������.');
      end;

      if iRes=2 then
      begin //�������� ��������� - ���� ������ � ������ �����
        showmessage('�������� ���������! ������ �� �����.');
      end;

    end;
  end;
end;

procedure TfmGoodsSel.Timer1Timer(Sender: TObject);
begin
  if bClearGoodsSel=True then begin StatusBar1.Panels[0].Text:=''; bClearGoodsSel:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearGoodsSel:=True;
end;

procedure TfmGoodsSel.ClassTreeChange(Sender: TObject; Node: TTreeNode);
begin
  if dmO=nil then exit;
  with dmO do
  begin
    quCardsSel1.Active:=False;
    quCardsSel1.ParamByName('PARENTID').AsInteger:=Integer(Node.Data);
    quCardsSel1.Active:=True;
  end;
end;

procedure TfmGoodsSel.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewGoodsSel.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmGoodsSel.acAddGoodsExecute(Sender: TObject);
Var iId:Integer;
    StrSp:String;
    i:Integer;
begin
// �������� �����
  if not CanDo('prAddGoods') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;

  if ClassTree.Selected=Nil then
  begin
    showmessage('�������� ������.');
    exit;
  end;

  with dmO do
  begin
    iId:=GetId('GD');
    while iId<100000 do
    begin
      quGdsFind.Active:=False;
      quGdsFind.ParamByName('IID').AsInteger:=iId;
      quGdsFind.Active:=True;
      if quGdsFind.RecordCount=0 then break
      else iId:=GetId('GD');
    end;
    quGdsFind.Active:=False;
    if iId=100000 then
    begin
      showmessage('������������ ��� ���������. ���������� ��������� ����� � ��������� ���������.');
      exit;
    end;

    taMess.Active:=False;
    taMess.Active:=True;
    taNDS.Active:=False;
    taNds.Active:=True;
    taCateg.Active:=False;
    taCateg.Active:=True;

    with fmAddGood do
    begin
      CheckListBox1.Items.BeginUpdate;
      CheckListBox1.Items.Clear;
      quMHSpis.Active:=False;
      quMHSpis.Active:=True;
      quMHSpis.First;
      while not quMHSpis.Eof do
      begin
        with  CheckListBox1.Items.add do
        begin
          Enabled:=True;
          State:=cbsUnChecked;
          Text:=quMHSpisNAMEMH.AsString;
          Tag:=quMHSpisNUMSPIS.AsInteger;
        end;
        quMHSpis.Next;
      end;
      CheckListBox1.Items.EndUpdate;
      CheckListBox1.ReadOnly:=False;
      quMHSpis.Active:=False;

      Caption:='���������� ������.';
      Label1.Caption:='���������� ������ � ������ - '+ClassTree.Selected.Text;

      cxTextEdit1.Text:=''; cxTextEdit1.Properties.ReadOnly:=False;
      cxTextEdit2.Text:=''; cxTextEdit2.Properties.ReadOnly:=False;

      cxCEdit1.EditValue:=0; cxCEdit1.Properties.ReadOnly:=False;
      cxSpinEdit1.Value:=iId; cxSpinEdit1.Properties.ReadOnly:=False;
      cxLookUpComboBox1.EditValue:=2; cxLookUpComboBox1.Properties.ReadOnly:=False;
      cxLookUpComboBox2.EditValue:=2; cxLookUpComboBox2.Properties.ReadOnly:=False;
      cxLookUpComboBox3.EditValue:=1; cxLookUpComboBox3.Properties.ReadOnly:=False;

      cxCalcEdit1.Value:=0; cxCalcEdit1.Properties.ReadOnly:=False;
      cxCheckBox1.EditValue:=0; cxCheckBox1.Properties.ReadOnly:=False;
      cxCheckBox2.EditValue:=1; cxCheckBox2.Properties.ReadOnly:=False;
      cxButton3.Enabled:=False;

      cxCalcEdit2.Value:=0; cxCalcEdit2.Properties.ReadOnly:=False;
      cxCalcEdit3.Value:=0; cxCalcEdit3.Properties.ReadOnly:=False;
      cxCalcEdit4.Value:=0; cxCalcEdit4.Properties.ReadOnly:=False;
      cxCalcEdit5.Value:=0; cxCalcEdit5.Properties.ReadOnly:=False;

      CloseTa(tBar);
      CloseTa(tEU);

      if quCardsSel1.Eof=False then
      begin
        cxTextEdit1.Text:=quCardsSel1NAME.AsString;
        cxTextEdit2.Text:=quCardsSel1COMMENT.AsString;
        cxCEdit1.EditValue:=quCardsSel1LASTPRICEOUT.AsFloat;

        cxLookUpComboBox1.EditValue:=quCardsSel1IMESSURE.AsInteger;
        cxLookUpComboBox2.EditValue:=quCardsSel1INDS.AsInteger;
        cxLookUpComboBox3.EditValue:=quCardsSel1CATEGORY.AsInteger;
        cxCalcEdit1.Value:=quCardsSel1MINREST.AsFloat;
        cxCheckBox1.EditValue:=quCardsSel1TTYPE.AsInteger;

        cxCalcEdit2.Value:=quCardsSel1BB.AsFloat;
        cxCalcEdit3.Value:=quCardsSel1GG.AsFloat;
        cxCalcEdit4.Value:=quCardsSel1U1.AsFloat;
        cxCalcEdit5.Value:=quCardsSel1EE.AsFloat;

        quGoodsEU.Active:=False;
        quGoodsEU.ParamByName('GOODSID').AsInteger:=quCardsSel1ID.AsInteger;
        quGoodsEU.Active:=True;
        quGoodsEu.First;
        while not quGoodsEU.Eof do
        begin
          tEu.Append;
          tEUiDateB.AsInteger:=quGoodsEUIDATEB.AsInteger;
          tEUiDateE.AsInteger:=quGoodsEUIDATEE.AsInteger;
          tEUto100g.AsFloat:=quGoodsEUTO100GRAMM.AsFloat;
          tEU.Post;

          quGoodsEU.Next;
        end;
      end;
    end;

    fmAddGood.ShowModal;
    if fmAddGood.ModalResult=mrOk then
    begin
      //��������

      iId:=fmAddGood.cxSpinEdit1.Value;
      quGdsFind.Active:=False;
      quGdsFind.ParamByName('IID').AsInteger:=iId;
      quGdsFind.Active:=True;
      if quGdsFind.RecordCount=0 then
      begin
        StrSp:='00000000000000000000';
        for i:=0 to fmAddGood.CheckListBox1.Items.Count-1 do
          if fmAddGood.CheckListBox1.Items[i].State=cbsChecked then StrSp[fmAddGood.CheckListBox1.Items[i].Tag]:='1';

        quCardsSel1.Append;
        quCardsSel1ID.AsInteger:=iId;
        quCardsSel1PARENT.AsInteger:=Integer(ClassTree.Selected.Data);
        quCardsSel1NAME.AsString:=fmAddGood.cxTextEdit1.Text;
        quCardsSel1COMMENT.AsString:=fmAddGood.cxTextEdit2.Text;
        quCardsSel1LASTPRICEOUT.AsFloat:=fmAddGood.cxCEdit1.EditValue;
        quCardsSel1TTYPE.AsInteger:=fmAddGood.cxCheckBox1.EditValue;
        quCardsSel1IMESSURE.AsInteger:=fmAddGood.cxLookUpComboBox1.EditValue;
        quCardsSel1INDS.AsInteger:=fmAddGood.cxLookUpComboBox2.EditValue;
        quCardsSel1MINREST.AsFloat:=fmAddGood.cxCalcEdit1.Value;
        quCardsSel1IACTIVE.AsInteger:=1;
        quCardsSel1CATEGORY.AsInteger:=fmAddGood.cxLookUpComboBox3.EditValue;
        quCardsSel1SPISSTORE.AsString:=StrSp;
        quCardsSel1BB.AsFloat:=fmAddGood.cxCalcEdit2.Value;
        quCardsSel1GG.AsFloat:=fmAddGood.cxCalcEdit3.Value;
        quCardsSel1U1.AsFloat:=fmAddGood.cxCalcEdit4.Value;
        quCardsSel1EE.AsFloat:=fmAddGood.cxCalcEdit5.Value;
        quCardsSel1.Post;

        with fmAddGood do
        begin
          quBars.Active:=False;
          quBars.ParamByName('IID').AsInteger:=iId;
          quBars.Active:=True;

          tBar.First;
          while not tBar.Eof do
          begin
            if tBariStatus.AsInteger=1 then
            begin
              quBars.Append;
              quBarsBAR.AsString:=tBarBarNew.AsString;
              quBarsGOODSID.AsInteger:=iId;
              quBarsQUANT.AsFloat:=tBarQuant.AsFloat;
              quBarsBARFORMAT.AsInteger:=fmAddGood.cxCheckBox1.EditValue;
              quBarsPRICE.AsFloat:=0;
              quBars.Post;
            end;
            tBar.Next;
          end;
          quBars.Active:=False;

          quGoodsEU.Active:=False;
          quGoodsEU.ParamByName('GOODSID').AsInteger:=iID;
          quGoodsEU.Active:=True;
          quGoodsEu.First; while not quGoodsEU.Eof do quGoodsEU.Delete;

          tEU.first;
          while not tEu.Eof do
          begin
            quGoodsEU.Append;
            quGoodsEUGOODSID.AsInteger:=iId;
            quGoodsEUIDATEB.AsInteger:=tEUiDateB.AsInteger;
            quGoodsEUIDATEE.AsInteger:=tEUiDateE.AsInteger;
            quGoodsEUTO100GRAMM.AsFloat:=tEUto100g.AsFloat;
            quGoodsEU.Post;

            tEU.next;
          end;
          tEu.Active:=False;
        end;
        quCardsSel1.Refresh;
      end else
      begin
        Showmessage('�������� � ����� ����� '+IntToStr(iId)+' ��� ����������, ���������� ����������.');
      end;
      quGdsFind.Active:=False;
    end;

    taCateg.Active:=False;
    taMess.Active:=False;
    taNDS.Active:=False;
    fmAddGood.tBar.Active:=False;
  end;
end;

procedure TfmGoodsSel.acEditGoodsExecute(Sender: TObject);
Var IId:Integer;
    StrF:String;
    StrSp:String;
    i:Integer;
begin
//������������� �����
  if not CanDo('prEditGoods') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;

  with dmO do
  begin
    if quCardsSel1.Eof then
    begin
      showmessage('�������� ����� ��� ��������������.');
      exit;
    end;

    if quCardsSel1ID.AsInteger=quCardsSelID.AsInteger then
    begin
      showmessage('���� ����� ������������� ������.');
      exit;
    end;

    iId:=quCardsSel1ID.AsInteger;
    StrF:=quCardsSel1SPISSTORE.AsString;

    taCateg.Active:=False;
    taCateg.Active:=True;
    taMess.Active:=False;
    taMess.Active:=True;
    taNDS.Active:=False;
    taNds.Active:=True;

    with fmAddGood do
    begin
      CheckListBox1.Items.BeginUpdate;
      CheckListBox1.Items.Clear;
      quMHSpis.Active:=False;
      quMHSpis.Active:=True;
      quMHSpis.First;
      while not quMHSpis.Eof do
      begin
        with  CheckListBox1.Items.add do
        begin
          Enabled:=True;
          if StrF[quMHSpisNUMSPIS.AsInteger]='0' then State:=cbsUnChecked
          else State:=cbsChecked;
          Text:=quMHSpisNAMEMH.AsString;
          Tag:=quMHSpisNUMSPIS.AsInteger;
        end;
        quMHSpis.Next;
      end;
      CheckListBox1.Items.EndUpdate;
      CheckListBox1.ReadOnly:=False;
      quMHSpis.Active:=False;
    
      Caption:='�������������� ������.';
      Label1.Caption:='������ - '+ClassTree.Selected.Text;

      cxTextEdit1.Text:=quCardsSel1NAME.AsString; cxTextEdit1.Properties.ReadOnly:=False;
      cxTextEdit2.Text:=quCardsSel1COMMENT.AsString; cxTextEdit2.Properties.ReadOnly:=False;

      cxCEdit1.EditValue:=quCardsSel1LASTPRICEOUT.AsFloat; cxCEdit1.Properties.ReadOnly:=False;
      cxSpinEdit1.Value:=iId; cxSpinEdit1.Properties.ReadOnly:=True;
      cxLookUpComboBox1.EditValue:=quCardsSel1IMESSURE.AsInteger;cxLookUpComboBox1.Properties.ReadOnly:=False;
      cxLookUpComboBox2.EditValue:=quCardsSel1INDS.AsInteger; cxLookUpComboBox2.Properties.ReadOnly:=False;
      cxLookUpComboBox3.EditValue:=quCardsSel1CATEGORY.AsInteger;
      cxCalcEdit1.Value:=quCardsSel1MINREST.AsFloat; cxCalcEdit1.Properties.ReadOnly:=False;
      cxCheckBox1.EditValue:=quCardsSel1TTYPE.AsInteger; cxCheckBox1.Properties.ReadOnly:=False;
      cxCheckBox2.EditValue:=quCardsSel1IACTIVE.AsInteger; cxCheckBox2.Properties.ReadOnly:=False;
      cxButton3.Enabled:=True;

      cxCalcEdit2.Value:=quCardsSel1BB.AsFloat; cxCalcEdit2.Properties.ReadOnly:=False;
      cxCalcEdit3.Value:=quCardsSel1GG.AsFloat; cxCalcEdit3.Properties.ReadOnly:=False;
      cxCalcEdit4.Value:=quCardsSel1U1.AsFloat; cxCalcEdit4.Properties.ReadOnly:=False;
      cxCalcEdit5.Value:=quCardsSel1EE.AsFloat; cxCalcEdit5.Properties.ReadOnly:=False;

      CloseTa(tBar);
      CloseTa(tEu);

      quBars.Active:=False;
      quBars.ParamByName('IID').AsInteger:=iID;
      quBars.Active:=True;
      quBars.First;
      while not quBars.Eof do
      begin
        tBar.Append;
        tBarBarNew.AsString:=quBarsBAR.AsString;
        tBarBarOld.AsString:=quBarsBAR.AsString;
        tBarQuant.AsFloat:=quBarsQUANT.AsFloat;
        tBariStatus.AsInteger:=1;
        tBar.Post;

        quBars.Next;
      end;

      quBars.Active:=False;

      quGoodsEU.Active:=False;
      quGoodsEU.ParamByName('GOODSID').AsInteger:=quCardsSel1ID.AsInteger;
      quGoodsEU.Active:=True;
      quGoodsEu.First;
      while not quGoodsEU.Eof do
      begin
        tEu.Append;
        tEUiDateB.AsInteger:=quGoodsEUIDATEB.AsInteger;
        tEUiDateE.AsInteger:=quGoodsEUIDATEE.AsInteger;
        tEUto100g.AsFloat:=quGoodsEUTO100GRAMM.AsFloat;
        tEU.Post;

        quGoodsEU.Next;
      end;
    end;

    fmAddGood.ShowModal;
    if fmAddGood.ModalResult=mrOk then
    begin
      StrSp:='00000000000000000000';
      for i:=0 to fmAddGood.CheckListBox1.Items.Count-1 do
        if fmAddGood.CheckListBox1.Items[i].State=cbsChecked then StrSp[fmAddGood.CheckListBox1.Items[i].Tag]:='1';

        //����������
      quCardsSel1.Edit;
      quCardsSel1NAME.AsString:=fmAddGood.cxTextEdit1.Text;
      quCardsSel1COMMENT.AsString:=fmAddGood.cxTextEdit2.Text;
      quCardsSel1LASTPRICEOUT.AsFloat:=fmAddGood.cxCEdit1.EditValue;
      quCardsSel1TTYPE.AsInteger:=fmAddGood.cxCheckBox1.EditValue;
      quCardsSel1IMESSURE.AsInteger:=fmAddGood.cxLookUpComboBox1.EditValue;
      quCardsSel1INDS.AsInteger:=fmAddGood.cxLookUpComboBox2.EditValue;
      quCardsSel1MINREST.AsFloat:=fmAddGood.cxCalcEdit1.Value;
      quCardsSel1IACTIVE.AsInteger:=fmAddGood.cxCheckBox2.EditValue;
      quCardsSel1CATEGORY.AsInteger:=fmAddGood.cxLookUpComboBox3.EditValue;
      quCardsSel1SPISSTORE.AsString:=StrSp;
      quCardsSel1BB.AsFloat:=fmAddGood.cxCalcEdit2.Value;
      quCardsSel1GG.AsFloat:=fmAddGood.cxCalcEdit3.Value;
      quCardsSel1U1.AsFloat:=fmAddGood.cxCalcEdit4.Value;
      quCardsSel1EE.AsFloat:=fmAddGood.cxCalcEdit5.Value;
      quCardsSel1.Post;

      with fmAddGood do
      begin
        quBars.Active:=False;
        quBars.ParamByName('IID').AsInteger:=iId;
        quBars.Active:=True;

        quBars.First;
        while not quBars.Eof do quBars.Delete;

        tBar.First;
        while not tBar.Eof do
        begin
          if tBariStatus.AsInteger=1 then
          begin
            quBars.Append;
            quBarsBAR.AsString:=tBarBarNew.AsString;
            quBarsGOODSID.AsInteger:=iId;
            quBarsQUANT.AsFloat:=tBarQuant.AsFloat;
            quBarsBARFORMAT.AsInteger:=fmAddGood.cxCheckBox1.EditValue;
            quBarsPRICE.AsFloat:=0;
            quBars.Post;
          end;
          tBar.Next;
        end;
        quBars.Active:=False;

        quGoodsEU.Active:=False;
        quGoodsEU.ParamByName('GOODSID').AsInteger:=iID;
        quGoodsEU.Active:=True;
        quGoodsEu.First; while not quGoodsEU.Eof do quGoodsEU.Delete;

        tEU.first;
        while not tEu.Eof do
        begin
          quGoodsEU.Append;
          quGoodsEUGOODSID.AsInteger:=iId;
          quGoodsEUIDATEB.AsInteger:=tEUiDateB.AsInteger;
          quGoodsEUIDATEE.AsInteger:=tEUiDateE.AsInteger;
          quGoodsEUTO100GRAMM.AsFloat:=tEUto100g.AsFloat;
          quGoodsEU.Post;

          tEU.next;
        end;
        tEu.Active:=False;
      end;

      quCardsSel1.Refresh;
    end;
    taCateg.Active:=False;
    taMess.Active:=False;
    taNDS.Active:=False;
    fmAddGood.tBar.Active:=False;
  end;
end;

procedure TfmGoodsSel.acDelGoodsExecute(Sender: TObject);
Var iRes:Integer;
begin
// ������� �����
  if not CanDo('prDelGoods') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;

  with dmO do
  begin
    if quCardsSel1.Eof then
    begin
      showmessage('�������� ����� ��� ��������������.');
      exit;
    end;

    if quCardsSel1ID.AsInteger=quCardsSelID.AsInteger then
    begin
      showmessage('���� ����� ������� ������.');
      exit;
    end;

    if MessageDlg('�� ������������� ������ ������� �����: '+quCardsSel1NAME.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      if MessageDlg('����������� �������� ������: '+quCardsSel1NAME.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        prDelCard.ParamByName('SIFR').AsInteger:=quCardsSel1ID.AsInteger;
        prDelCard.ExecProc;
        iRes:=prDelCard.ParamByName('RESULT').AsInteger;

        if iRes=0 then
        begin //�������� ���������

          quGoodsEU.Active:=False;
          quGoodsEU.ParamByName('GOODSID').AsInteger:=quCardsSel1ID.AsInteger;
          quGoodsEU.Active:=True;
          quGoodsEu.First; while not quGoodsEU.Eof do quGoodsEU.Delete;

          quCardsSel1.Delete;
          quCardsSel1.Refresh;
        end else
          ShowMessage('����� �������������. �������� ����������.');

      end;
    end;
  end;
end;

procedure TfmGoodsSel.Action9Execute(Sender: TObject);
begin
// �����������
  showmessage('�������� ����������� ������� ��� �������� � ���������� �� ������ � ������ ������.');
end;

procedure TfmGoodsSel.ViewGoodsSelStartDrag(Sender: TObject;
  var DragObject: TDragObject);
begin
  if not CanDo('prMoveGoods') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  bDr:=True;
end;

procedure TfmGoodsSel.ClassTreeDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept:=False;
  if bDr then  Accept:=True;
end;

procedure TfmGoodsSel.ClassTreeDragDrop(Sender, Source: TObject; X,
  Y: Integer);
Var sGr:String;
    iGr:Integer;
    iCo:Integer;
    i,j: Integer;
    iNum: Integer;
    Rec:TcxCustomGridRecord;
begin
  if not CanDo('prMoveGoods') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  if bDr then
  begin
    bDr:=False;
    sGr:=ClassTree.DropTarget.Text;
    iGr:=Integer(ClassTree.DropTarget.data);
    iCo:=ViewGoodsSel.Controller.SelectedRecordCount;
    if iCo>0 then
    begin
      if MessageDlg('�� ������������� ������ ����������� ��������� ������� ('+IntToStr(iCo)+' ��.) � ������ "'+sGr+'"?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        with dmO do
        begin
          for i:=0 to ViewGoodsSel.Controller.SelectedRecordCount-1 do
          begin
            Rec:=ViewGoodsSel.Controller.SelectedRecords[i];

            for j:=0 to Rec.ValueCount-1 do
            begin
              if ViewGoodsSel.Columns[j].Name='ViewGoodsSelID' then break;
            end;

            iNum:=Rec.Values[j];
          //��� ��� - ����������

            if quCardsSel1.Locate('ID',iNum,[]) then
            begin
              quCardsSel1.Edit;
              quCardsSel1PARENT.AsInteger:=iGr;
              quCardsSel1.Post;
            end;
          end;
          quCardsSel1.FullRefresh;
        end;
      end;
    end;
  end;
end;

procedure TfmGoodsSel.ClassTreeExpanding(Sender: TObject; Node: TTreeNode;
  var AllowExpansion: Boolean);
begin
  if Node.getFirstChild.Data = nil then
  begin
    Node.DeleteChildren;
    ClassifExpand(Node,ClassTree,dmO.quClassTree,Person.Id,1);
  end;
end;

procedure TfmGoodsSel.cxButton1Click(Sender: TObject);
Var i:INteger;
begin
  if cxTextEdit1.Text>'' then
  begin
    with dmO do
    begin
      quFind.Active:=False;
      quFind.SelectSQL.Clear;
      quFind.SelectSQL.Add('SELECT ID,PARENT,NAME,TCARD');
      quFind.SelectSQL.Add('FROM OF_CARDS');
      quFind.SelectSQL.Add('where UPPER(NAME) like ''%'+AnsiUpperCase(cxTextEdit1.Text)+'%''');
      quFind.Active:=True;
                                    
      fmFind.LevelFind.Visible:=True;
      fmFind.LevelFMenu.Visible:=False;
      fmFind.LevelFCli.Visible:=False;

      fmFind.ShowModal;
      if fmFind.ModalResult=mrOk then
      begin
        if quFind.RecordCount>0 then
        begin
          for i:=0 to ClassTree.Items.Count-1 Do
          if Integer(ClassTree.Items[i].Data) = quFindPARENT.AsInteger Then
          begin
            ClassTree.Items[i].Expand(False);
            ClassTree.Repaint;
            ClassTree.Items[i].Selected:=True;
            Break;
          End;
          delay(10);
          quCardsSel1.First;
          quCardsSel1.locate('ID',quFindID.AsInteger,[]);
        end;
      end;
      cxTextEdit1.Text:='';
      delay(10);
      GrGoodsSel.SetFocus;
    end;
  end;
end;

procedure TfmGoodsSel.cxButton2Click(Sender: TObject);
//�� ����
Var i:INteger;
    iCode:INteger;
begin
  iCode:=StrToIntDef(cxTextEdit1.Text,0);
  if iCode>0 then
  begin
    with dmO do
    begin
      quFind.Active:=False;
      quFind.SelectSQL.Clear;
      quFind.SelectSQL.Add('SELECT ID,PARENT,NAME,TCARD');
      quFind.SelectSQL.Add('FROM OF_CARDS');
      quFind.SelectSQL.Add('where ID ='+IntToStr(iCode));
      quFind.Active:=True;

      fmFind.LevelFind.Visible:=True;
      fmFind.LevelFMenu.Visible:=False;
      fmFind.LevelFCli.Visible:=False;

      fmFind.ShowModal;
      if fmFind.ModalResult=mrOk then
      begin
        if quFind.RecordCount>0 then
        begin
          for i:=0 to ClassTree.Items.Count-1 Do
          if Integer(ClassTree.Items[i].Data) = quFindPARENT.AsInteger Then
          begin
            ClassTree.Items[i].Expand(False);
            ClassTree.Repaint;
            ClassTree.Items[i].Selected:=True;
            Break;
          End;
          delay(10);
          quCardsSel1.First;
          quCardsSel1.locate('ID',quFindID.AsInteger,[]);
        end;
      end;
      cxTextEdit1.Text:='';
      delay(10);
      GrGoodsSel.SetFocus;
    end;
  end;
end;

procedure TfmGoodsSel.cxButton3Click(Sender: TObject);
Var i:INteger;
begin
{
SELECT c.ID,c.PARENT,c.NAME
FROM OF_CARDS c
left Join OF_BARCODE b on b.GOODSID=c.ID
where b.BAR like '%2222%'
}
  if cxTextEdit1.Text>'' then
  begin
    with dmO do
    begin
      quFind.Active:=False;
      quFind.SelectSQL.Clear;
      quFind.SelectSQL.Add('SELECT c.ID,c.PARENT,c.NAME,TCARD');
      quFind.SelectSQL.Add('FROM OF_CARDS c');
      quFind.SelectSQL.Add('left Join OF_BARCODE b on b.GOODSID=c.ID');
      quFind.SelectSQL.Add('where b.BAR like ''%'+cxTextEdit1.Text+'%''');
      quFind.Active:=True;

      fmFind.LevelFind.Visible:=True;
      fmFind.LevelFMenu.Visible:=False;
      fmFind.LevelFCli.Visible:=False;

      fmFind.ShowModal;
      if fmFind.ModalResult=mrOk then
      begin
        if quFind.RecordCount>0 then
        begin
          for i:=0 to ClassTree.Items.Count-1 Do
          if Integer(ClassTree.Items[i].Data) = quFindPARENT.AsInteger Then
          begin
            ClassTree.Items[i].Expand(False);
            ClassTree.Repaint;
            ClassTree.Items[i].Selected:=True;
            Break;
          End;
          delay(10);
          quCardsSel1.First;
          quCardsSel1.locate('ID',quFindID.AsInteger,[]);
        end;
      end;
      cxTextEdit1.Text:='';
      delay(10);
      GrGoodsSel.SetFocus;
    end;
  end;
end;

procedure TfmGoodsSel.ViewGoodsSelDblClick(Sender: TObject);
Var iMax:INteger;
    kBrutto,Km:Real;
    iCurDate:Integer;
begin
  if bAddTSpec then
  begin
    //������� ������� ��������� ����� � ������������ ��
    //��� ������� �� ���� �� ������� ��� ����� ������
    with dmO do
    begin
      //���� ������� ��������� ���� � ��������� ������ � ��� �� ������ ���������
      if prCanSel(quCardsSelId.asInteger,quCardsSel1Id.asInteger) then
      begin
        kBrutto:=0;
        iCurDate:=prDateToI(fmTCard.cxDateEdit1.Date);
        quFindEU.Active:=False;
        quFindEU.ParamByName('GOODSID').AsInteger:=quCardsSel1ID.AsInteger;
        quFindEU.ParamByName('DATEB').AsInteger:=iCurDate;
        quFindEU.ParamByName('DATEE').AsInteger:=iCurDate;
        quFindEU.Active:=True;

        if quFindEU.RecordCount>0 then
        begin
          quFindEU.First;
          kBrutto:=quFindEUTO100GRAMM.AsFloat;
        end;

        quFindEU.Active:=False;
        with fmTCard do
        begin
          ViewTSpec.BeginUpdate;

          iMax:=1;

          taTSpec.First;
          if not taTSpec.Eof then
          begin
            taTSpec.Last;
            iMax:=taTSpecNum.AsInteger+1;
          end;

          taTSpec.Append;
          taTSpecNum.AsInteger:=iMax;
          taTSpecIDCARD.AsInteger:=quCardsSel1ID.AsInteger;
          taTSpecName.AsString:=quCardsSel1NAME.AsString;
          taTSpecNETTO.AsFloat:=0;
          taTSpecBRUTTO.AsFloat:=0;
          taTSpecKNB.AsFloat:=kBrutto;

          if fmTCard.cxCheckBox1.Checked then
          begin
            taTSpecIdM.AsInteger:=4;
            taTSpecSM.AsString:='�.';
            taTSpecKm.AsFloat:=0.001;
          end else
          begin
            taTSpecIdM.AsInteger:=quCardsSel1IMESSURE.AsInteger;
            taTSpecSM.AsString:=prFindKNM(quCardsSel1IMESSURE.AsInteger,Km);;
            taTSpecKm.AsFloat:=Km;
          end;
          taTSpecTCard.AsInteger:=quCardsSel1TCARD.AsInteger;

          taTSpecNetto1.AsFloat:=0;
          taTSpecComment.AsString:='';
          taTSpecPr1.AsFloat:=0;

          taTSpecNetto2.AsFloat:=0;
          taTSpecPr2.AsFloat:=0;

          taTSpec.Post;
          ViewTSpec.EndUpdate;
        end;
      end else
      begin
        showmessage('���������� ����������: ����������� ������.');
      end;
    end;
  end;
end;

procedure TfmGoodsSel.acTCardExecute(Sender: TObject);
Var iCurDate:INteger;
    kBrutto,rN:Real;
begin
  exit;
  if not CanDo('prEditTCards') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;

  with dmO do
  begin
    if not quCardsSel1.Eof then
    begin
      quTCards.Active:=False;
      quTCards.ParamByName('IDGOOD').AsInteger:=quCardsSel1ID.AsInteger;
      quTCards.Active:=True;

      if quTCards.RecordCount=0 then
      begin
        fmTCard.Panel3.Visible:=False;
        quTSpec.Active:=False;
      end  else
      begin
        fmTCard.Panel3.Visible:=True;

        quTCards.Last;
        fmTCard.cxTextEdit2.Text:=quTCardsSHORTNAME.AsString;
        fmTCard.cxTextEdit3.Text:=quTCardsRECEIPTNUM.AsString;
        fmTCard.cxTextEdit4.Text:=quTCardsPOUTPUT.AsString;
        fmTCard.cxSpinEdit1.EditValue:=quTCardsPCOUNT.AsInteger;
        fmTCard.cxCalcEdit1.EditValue:=quTCardsPVES.AsFloat;

        quTSpec.Active:=False;
        quTSpec.ParamByName('IDC').AsInteger:=quTCardsIDCARD.AsInteger;
        quTSpec.ParamByName('IDTC').AsInteger:=quTCardsID.AsInteger;
        quTSpec.Active:=True;

        //������������� ������
        iCurDate:=prDateToI(Date);
        fmTCard.ViewTSpec.BeginUpdate;

        quTSpec.First;
        while not quTSpec.Eof do
        begin
          kBrutto:=0;
          quFindEU.Active:=False;
          quFindEU.ParamByName('GOODSID').AsInteger:=quTSpecIDCARD.AsInteger;
          quFindEU.ParamByName('DATEB').AsInteger:=iCurDate;
          quFindEU.ParamByName('DATEE').AsInteger:=iCurDate;
          quFindEU.Active:=True;

          if quFindEU.RecordCount>0 then
          begin
            quFindEU.First;
            kBrutto:=quFindEUTO100GRAMM.AsFloat;
          end;

          rN:=quTSpecNETTO.AsFloat;
          quFindEU.Active:=False;
          quTSpec.Edit;
          quTSpecBRUTTO.AsFloat:=rN*(100+kBrutto)/100;
          quTSpecKNB.AsFloat:=kBrutto;
          quTSpec.Post;
          quTSpec.Next;
        end;
        quTSpec.FullRefresh;
        quTSpec.First;

        fmTCard.ViewTSpec.EndUpdate;
      end;

      fmTCard.Label6.Caption:= IntToStr(quCardsSel1ID.AsInteger);
      fmTCard.cxTextEdit1.Text:=quCardsSel1NAME.AsString;
      fmTCard.cxTextEdit1.Tag:=quCardsSel1ID.AsInteger;

      TK.Add:=False;
      TK.Edit:=False;

      fmTCard.Caption:='��������������� �����. '+IntToStr(quCardsSel1ID.AsInteger)+' '+quCardsSel1NAME.AsString;
      fmTCard.Show;
    end else
    begin
      showmessage('�������� ������ ��� �������������� !!');
    end;
  end;
end;

end.
