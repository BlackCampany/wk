unit of_EditPosMenu;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons, ExtCtrls,
  ComCtrls, cxGraphics, cxDropDownEdit, cxCalc, cxMaskEdit, cxLookupEdit,
  cxDBLookupEdit, cxDBLookupComboBox, cxCurrencyEdit, cxControls,
  cxContainer, cxEdit, cxTextEdit;

type
  TfmEditPosM = class(TForm)
    StatusBar1: TStatusBar;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    GroupBox1: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxTextEdit1: TcxTextEdit;
    cxCurrencyEdit1: TcxCurrencyEdit;
    cxLookupComboBox1: TcxLookupComboBox;
    cxCalcEdit1: TcxCalcEdit;
    cxCalcEdit2: TcxCalcEdit;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmEditPosM: TfmEditPosM;

implementation

uses TransMenuBack;

{$R *.dfm}

end.
