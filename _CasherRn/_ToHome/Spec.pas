unit Spec;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Placemnt, cxGridBandedTableView, cxGridDBBandedTableView, StdCtrls,
  cxLookAndFeelPainters, cxButtons, ActnList, XPStyleActnCtrls, ActnMan,
  DBClient, ExtCtrls, VaClasses, VaComm, VaControls, VaDisplay, VaSystem,
  dxfBackGround, cxContainer, cxTextEdit, Grids, DBGrids, cxDataStorage,
  cxCurrencyEdit, cxCalc, cxImageComboBox, Menus, FR_Class, FR_DSet,
  FR_DBSet, cxMaskEdit, cxButtonEdit, cxSpinEdit, cxDropDownEdit,
  dxfCheckBox;

type
  TfmSpec = class(TForm)
    LevelSpec: TcxGridLevel;
    GridSpec: TcxGrid;
    FormPlacement1: TFormPlacement;
    ViewSpec: TcxGridDBBandedTableView;
    ViewSpecPRICE: TcxGridDBBandedColumn;
    ViewSpecQUANTITY: TcxGridDBBandedColumn;
    ViewSpecDPROC: TcxGridDBBandedColumn;
    ViewSpecDSUM: TcxGridDBBandedColumn;
    ViewSpecSUMMA: TcxGridDBBandedColumn;
    ViewSpecISTATUS: TcxGridDBBandedColumn;
    ViewSpecNAME: TcxGridDBBandedColumn;
    ViewSpecCODE: TcxGridDBBandedColumn;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Button1: TcxButton;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Button5: TcxButton;
    cxButton3: TcxButton;
    amSpec: TActionManager;
    acMenu: TAction;
    acChangeQuantity: TAction;
    acDelPos: TAction;
    cxButton4: TcxButton;
    acMovePos: TAction;
    acPrePrint: TAction;
    Label17: TLabel;
    ViewMod: TcxGridDBTableView;
    LevelMod: TcxGridLevel;
    GridMod: TcxGrid;
    cxButton5: TcxButton;
    acModify: TAction;
    ViewModName: TcxGridDBColumn;
    ViewModQuantity: TcxGridDBColumn;
    cxButton6: TcxButton;
    Panel1: TPanel;
    Label18: TLabel;
    Timer1: TTimer;
    Action1: TAction;
    Action2: TAction;
    Action5: TAction;
    Action7: TAction;
    Action8: TAction;
    Action9: TAction;
    Action10: TAction;
    acAddMod: TAction;
    acDelMod: TAction;
    Button2: TButton;
    cxButton7: TcxButton;
    acKod: TAction;
    Edit1: TcxTextEdit;
    acDiscount: TAction;
    acExitN: TAction;
    cxButton8: TcxButton;
    cxButton9: TcxButton;
    frRepSp: TfrReport;
    frquCheck: TfrDBDataSet;
    cxButton10: TcxButton;
    Label4: TLabel;
    BEdit1: TcxButtonEdit;
    SEdit1: TcxSpinEdit;
    cxButton11: TcxButton;
    cxButton12: TcxButton;
    cxButton13: TcxButton;
    acNumTab: TAction;
    Label8: TLabel;
    cxButton14: TcxButton;
    cxButton15: TcxButton;
    acAddMe: TAction;
    ViewModSIFR: TcxGridDBColumn;
    cxButton16: TcxButton;
    cxButton17: TcxButton;
    Timer2: TTimer;
    Panel2: TPanel;
    Image1: TImage;
    cxImageComboBox1: TcxImageComboBox;
    cxCheckBox1: TdxfCheckBox;
    ViewSpecQUEST: TcxGridDBBandedColumn;
    ViewSpecID: TcxGridDBBandedColumn;
    procedure FormCreate(Sender: TObject);
    procedure acMenuExecute(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure acChangeQuantityExecute(Sender: TObject);
    procedure acDelPosExecute(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure acMovePosExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure acPrePrintExecute(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure acModifyExecute(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure Action1Execute(Sender: TObject);
    procedure Action2Execute(Sender: TObject);
    procedure acExitNExecute(Sender: TObject);
    procedure Action4Execute(Sender: TObject);
    procedure Action5Execute(Sender: TObject);
    procedure Action6Execute(Sender: TObject);
    procedure Action7Execute(Sender: TObject);
    procedure Action8Execute(Sender: TObject);
    procedure Action9Execute(Sender: TObject);
    procedure Action10Execute(Sender: TObject);
    procedure acAddModExecute(Sender: TObject);
    procedure acDelModExecute(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure acKodExecute(Sender: TObject);
    procedure cxButton7Click(Sender: TObject);
    procedure acDiscountExecute(Sender: TObject);
    procedure ViewSpecFocusedRecordChanged(Sender: TcxCustomGridTableView;
      APrevFocusedRecord, AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure cxButton8Click(Sender: TObject);
    procedure cxButton9Click(Sender: TObject);
    procedure cxButton10Click(Sender: TObject);
    procedure cxButton11Click(Sender: TObject);
    procedure cxButton13Click(Sender: TObject);
    procedure acNumTabExecute(Sender: TObject);
    procedure BEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure BEdit1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure BEdit1Click(Sender: TObject);
    procedure cxButton12Click(Sender: TObject);
    procedure acAddMeExecute(Sender: TObject);
    procedure cxButton14Click(Sender: TObject);
    procedure ViewModCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure cxButton15Click(Sender: TObject);
    procedure cxButton16Click(Sender: TObject);
    procedure cxButton17Click(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
    procedure ViewSpecDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure Panel2StartDrag(Sender: TObject;
      var DragObject: TDragObject);
    procedure ViewSpecDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure cxCheckBox1Click(Sender: TObject);
    procedure cxImageComboBox1PropertiesChange(Sender: TObject);
    procedure ViewSpecStartDrag(Sender: TObject;
      var DragObject: TDragObject);
    procedure Panel2DragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure Panel2DragDrop(Sender, Source: TObject; X, Y: Integer);
  private
    { Private declarations }
  public
    { Public declarations }

    Procedure SaveSpecN;
    Function TestBL(Var SMess:String):Boolean;
    Procedure FormServChN;
    Procedure CreateViewPers1;
    Function TestSave:Boolean;
    Procedure SaveStatusTab(TabId,TabiStatus,iQuests,SaleT:Integer; sNumTab:String;rSum:Real;sDisc:String);
    Procedure SetStatus(Var S:String);
    procedure prSetButton(bSt:Boolean);
  end;

Function prFindKey(iC:Integer;Var sName,sCode:String;Var rPrice:Real;Var Sifr,iType,iLinkM,iLimit,iStream:Integer):Boolean;

var
  fmSpec: TfmSpec;
  bSaveSpec:Boolean = False;
  bAbort:Boolean = False;
  bQuest:Boolean = False;
  bQuestTo:Boolean = False;
  bMoveSpec:Boolean = False;


implementation

uses Un1, Dm, Menu, Calc, MessQ, MainCashRn, MessDel, PosMove, Modif,
  UnCash, u2fdk, Discont, fmDiscountShape, uDB1, Attention, CashEnd,
  cash_WarnPaper, prdb;

{$R *.dfm}

procedure TfmSpec.prSetButton(bSt:Boolean);
begin
  cxButton3.Enabled:=bSt;
  delay(10);
end;


Function TfmSpec.TestBL(Var SMess:String):Boolean;
Var IdMain:Integer;
    rQMain:Real;
begin
  Result:=True;
  with dmC do
  begin
    quTestGr.Active:=False;
    quTestGr.ParamByName('STATION').AsInteger:=CommonSet.Station;
    quTestGr.ParamByName('IDT').AsInteger:=Tab.Id;
    quTestGr.Active:=True;
    if quTestGr.RecordCount>0 then
    begin
      //������� �������
      IdMain:=0;
      rQMain:=0;
      quTestGr.First;
      while not quTestGr.Eof do //����� ������ ������ - ������ ���� ����������
      begin
        quTestGr.Next;
        if not quTestGr.Eof then
        begin
          IdMain:=quTestGrMAINGR.AsInteger;
          Break;
        end;
      end;

      quTestGr.First;
      while not quTestGr.Eof do
      begin
        if quTestGrPARENT.AsInteger=IdMain then
        begin
          rQMain:=quTestGrQUANT.AsFloat;  //����� ����� �������� ������ - ���� ��� ��������
          break;
        end;
        quTestGr.Next;
      end;

      if rQMain=0 then
      begin
        sMess:='�������� !! ��� ���������� ��������� �����.';
        Result:=False;
      end else
      begin

        quTestGr.First;
        while not quTestGr.Eof do
        begin
          if quTestGrPARENT.AsInteger<>IdMain then
          begin
            if quTestGrQUANT.AsFloat>rQMain then
            begin
              sMess:='�������� !! ��������� ���������� ���-�� �� ������ "'+quTestGrNAMEGR.AsString+'".';
              Result:=False;
              break;
            end;
          end;
          quTestGr.Next;
        end;

      end;
    end;
    quTestGr.Active:=False;
  end;
end;

Procedure TfmSpec.SetStatus(Var S:String);
begin
  S:='';
  if Tab.iStatus=0 then
  begin
    S:='� ��������������';
    Button1.Enabled:=True;
    cxButton1.Enabled:=True;
    cxButton4.Enabled:=True;
    cxButton2.Enabled:=True;
    cxButton5.Enabled:=True;
    cxButton6.Enabled:=True;
    cxButton7.Enabled:=True;
    cxButton10.Enabled:=True;
    cxButton11.Enabled:=True;
    cxButton13.Enabled:=True;
    cxButton17.Enabled:=True;
    bEdit1.Properties.ReadOnly:=False;
    bEdit1.Enabled:=True;
    sEdit1.Properties.ReadOnly:=False;
  end;
  if Tab.iStatus=1 then
  begin
    S:='�������';
    Button1.Enabled:=False;
    cxButton1.Enabled:=False;
    cxButton4.Enabled:=False;
    cxButton2.Enabled:=False;
    cxButton5.Enabled:=False;
    cxButton6.Enabled:=False;
    cxButton7.Enabled:=False;
    cxButton10.Enabled:=False;
    cxButton11.Enabled:=False;
    cxButton13.Enabled:=False;
    cxButton17.Enabled:=False;
    bEdit1.Properties.ReadOnly:=True;
    bEdit1.Enabled:=False;
    sEdit1.Properties.ReadOnly:=True;
  end;
end;



Procedure TfmSpec.SaveStatusTab(TabId,TabiStatus,iQuests,SaleT:Integer; sNumTab:String;rSum:Real;sDisc:String);
begin
  with dmC do
  begin
      fmMainCashRn.TabView.BeginUpdate;
      if quTabs.Locate('ID',TabId,[]) then
      begin
        quTabs.Edit;
        quTabsISTATUS.AsInteger:=TabiStatus;
        quTabsNUMTABLE.AsString:=sNumTab;
        quTabsQUESTS.AsInteger:=iQuests;
        quTabsTABSUM.AsFloat:=rSum;
        quTabsDISCONT.AsString:=sDisc;
        quTabsSALET.AsInteger:=SaleT;
        quTabs.Post;

        quTabs.Locate('ID',TabId,[]);
        quTabs.Refresh;
      end;  
      fmMainCashRn.TabView.EndUpdate;

      Tab.NumTable:=sNumTab;
      Tab.Quests:=iQuests;
      Tab.Summa:=rSum;
      Tab.DBar:=sDisc;
      Tab.SaleT:=SaleT;

{

    if quTabs.Locate('ID',TabId,[]) then
    begin
//      TabView.BeginUpdate;
      trUpdate.StartTransaction;
      quTabs.Edit;
      quTabsISTATUS.AsInteger:=TabiStatus;
      quTabsNUMTABLE.AsString:=sNumTab;
      quTabsQUESTS.AsInteger:=iQuests;
      quTabs.Post;
      trUpdate.Commit;
//      TabView.EndUpdate;
      quTabs.Locate('ID',TabId,[]);
      quTabs.Refresh;
      Tab.NumTable:=sNumTab;
      Tab.Quests:=iQuests;
    end;}
  end;
end;


Function TfmSpec.TestSave:Boolean;
Var rSum:Real;
begin
  Result:=True;
  with dmC do
  begin
    quTestSave.Active:=False;
    quTestSave.ParamByName('IDTAB').AsInteger:=Tab.Id;
    quTestSave.Active:=True;

    rSum:= quTestSaveTABSUM.AsFloat;
    quTestSave.Active:=False;
    if rSum<>Tab.Summa then
    begin
      Result:=False;
      prWriteLog('---TestSaveBad;'+IntToStr(Tab.Id)+';'+Tab.NumTable+'; Spec-'+FloatToStr(rSum)+';CurSpec-'+FloatToStr(Tab.Summa)+';');
    end else
    begin
      prWriteLog('---TestSaveGood;'+IntToStr(Tab.Id)+';'+Tab.NumTable+'; Spec-'+FloatToStr(rSum)+';CurSpec-'+FloatToStr(Tab.Summa)+';');
    end;
  end;
end;

Procedure TfmSpec.CreateViewPers1;
begin
  bChangeView:=True;
end;


Procedure TfmSpec.FormServChN;
Var ArrServ:Array[1..500] of Integer;
    i,iServ:Integer;
    bServ:Boolean;
begin
    //�������� ���� ��������
  with dmC do
  begin
    CloseTe(taServP);

    for i:=1 to 500 do ArrServ[i]:=0;

    quCurModAll.Active:=False;
    quCurModAll.ParamByName('IDT').AsInteger:=Tab.Id;
    quCurModAll.Active:=True;

    //���� ������� ��� ����������

    prWriteLog('--SaveSpec;'+IntToStr(Tab.Id)+';'+Tab.NumTable+';'+Person.Name+';'+IntToStr(Tab.Id_Personal)+';'+IntToStr(Tab.iNumZ));
{
    quCurSpec.First;
    while not quCurSpec.Eof do
    begin
      if quCurSpecIStatus.AsInteger=0 then
      begin
        prWriteLog('-----Addpos;'+IntToStr(quCurSpecID_TAB.AsInteger)+';'+IntToStr(quCurSpecID.AsInteger)+';'+IntTostr(quCurSpecID_PERSONAL.AsInteger)+';'+quCurSpecNAME.AsString+';'+IntToStr(quCurSpecSIFR.AsInteger)+';'+FloatToStr(quCurSpecSUMMA.AsFloat)+';');
      end;
      quCurSpec.Next;
    end;
    prWriteLog('--EndSpec;'+IntToStr(Tab.Id)+';'+Tab.NumTable+';'+Person.Name+';'+IntToStr(Tab.Id_Personal)+';');
}
    //��������
    bServ:=False;
    quCurSpec.First;
    while not quCurSpec.Eof do
    begin
      if quCurSpecIStatus.AsInteger=0 then
      begin
        prWriteLog('-----Addpos;'+IntToStr(quCurSpecID_TAB.AsInteger)+';'+IntToStr(quCurSpecID.AsInteger)+';'+IntTostr(quCurSpecID_PERSONAL.AsInteger)+';'+quCurSpecNAME.AsString+';'+IntToStr(quCurSpecSIFR.AsInteger)+';'+FloatToStr(quCurSpecSUMMA.AsFloat)+';');

        //�������� ��� ������ �����
        taServP.Append;
        taServPName.AsString:=quCurSpecName.AsString;
        taServPCode.AsString:=quCurSpecCode.AsString;
        taServPQuant.AsFloat:=quCurSpecQuantity.AsFloat;
        taServPStream.AsInteger:=quCurSpecStream.AsInteger;
        taServPiType.AsInteger:=0; //�����
        taServPiServ.AsInteger:=0;
        taServPIPos.AsInteger:=quCurSpecID.AsInteger;
        taServP.Post;

        //��������� ������ �� ������� �� ������
        while (quCurModAllID_POS.AsInteger<quCurSpecID.AsInteger) and (quCurModAll.Eof=False) do quCurModAll.Next;
        while (quCurModAllID_POS.AsInteger=quCurSpecID.AsInteger) and (quCurModAll.Eof=False) do
        begin
          iServ:=0;
          if quCurModAllISERV.AsInteger=1 then
          begin
            iServ:=1;
            bServ:=True;
            if quCurSpecID.AsInteger<=500 then ArrServ[quCurSpecID.AsInteger]:=1;
          end;

          taServP.Append;
          taServPName.AsString:=quCurModAllNAME.AsString;
          taServPCode.AsString:='';
          taServPQuant.AsFloat:=0;
          taServPStream.AsInteger:=quCurSpecStream.AsInteger;
          taServPiType.AsInteger:=1; //�����������
          taServPiServ.AsInteger:=iServ;
          taServPIPos.AsInteger:=quCurSpecID.AsInteger;
          taServP.Post;

          quCurModAll.Next;
        end;
      end;
      quCurSpec.Next;
    end;
    if bServ then //����� ���� - ���� ��������� � ����������� ��������
    begin
      taServP.First;
      while not taServP.Eof do
      begin
        if taServPIPos.AsInteger<=500 then
        begin
          if ArrServ[taServPIPos.AsInteger]=1 then
          begin
            taServP.Edit; //���������� ��� � ����������� ��������
            taServPiServ.AsInteger:=1;
            taServP.Post;
          end;
        end;
        taServP.Next;
      end;
    end;

    prWriteLog('--EndSpec;'+IntToStr(Tab.Id)+';'+Tab.NumTable+';'+Person.Name+';'+IntToStr(Tab.Id_Personal)+';');

    //�������� �������
    if Operation=0 then PrintServCh('����� ');// ���� ������� (1) - �� ������� ������ �� ����
    taServP.Active:=False;
    quCurModAll.Active:=False;
  end;
end;


Procedure TfmSpec.SaveSpecN; //
begin
  //�������� ���� ��������
  with dmC do
  begin
    FormServChN;

    //������� ������ �� ���� - ������������ ����������� ����������

//    quDelSpec.ParamByName('IDTAB').AsInteger:=Tab.Id;
//    quDelSpec.ExecQuery;

   //��������
    prSaveTab.ParamByName('ID_TAB').AsInteger:=Tab.Id;
    prSaveTab.ParamByName('ID_PERSONAL').AsInteger:=Tab.Id_Personal;
    prSaveTab.ParamByName('NUMTABLE').AsString:=Tab.NumTable;
    prSaveTab.ParamByName('QUESTS').AsInteger:=Tab.Quests;
    prSaveTab.ParamByName('TABSUM').AsDouble:=Tab.Summa;
    prSaveTab.ParamByName('BEGTIME').AsDateTime:=Tab.OpenTime;
    prSaveTab.ParamByName('ISTATUS').AsInteger:=Tab.iStatus;
    prSaveTab.ParamByName('DBAR').AsString:=Tab.DBar;
    prSaveTab.ParamByName('ID_STATION').AsInteger:=CommonSet.Station;
    prSaveTab.ParamByName('NUMZ').AsInteger:=Tab.iNumZ;
    prSaveTab.ParamByName('SALET').AsInteger:=Tab.SaleT;
    prSaveTab.ExecProc;

    //�������� ������ ����
    quSetStatus.ParamByName('ID_TAB').AsInteger:=Tab.Id;
    quSetStatus.ParamByName('ID_STATION').AsInteger:=CommonSet.Station;
    quSetStatus.ExecQuery;

    quCurSpec.FullRefresh;

  end;
end;

Function prFindKey(iC:Integer;Var sName,sCode:String;Var rPrice:Real;Var Sifr,iType,iLinkM,iLimit,iStream:Integer):Boolean;
begin
  result:=False;
  if CommonSet.bTouch then exit; //��� ����� ������� ������ �� ��������� ���� ��� - ���������.
  with dmC do
  begin
    taCF_All.Active:=False;
    taCF_All.Active:=True;
    if taCF_All.locate('KEY_CODE',iC,[]) then
    begin
      case taCF_AllKEY_STATUS.AsInteger of
        0:begin //��������
          end;
        1:begin //������ �������
            Sifr:=taCF_AllID_CLASSIF.AsInteger-10000;
            iType:=2;
            Result:=True;
          end;
        2:begin //������
            quMenuSelKey.Active:=False;
            quMenuSelKey.Active:=True;
            if quMenuSelKey.Locate('SIFR',taCF_AllSIFR.AsInteger,[]) then
            begin
              Sifr:= quMenuSelKeySIFR.AsInteger;
              sName:=quMenuSelKeyNAME.AsString;
              rPrice:=quMenuSelKeyPRICE.AsFloat;
              sCode:=quMenuSelKeyCODE.AsString;
              iLinkM:=quMenuSelKeyLINK.AsInteger;
              iLimit:=RoundEx(quMenuSelKeyLIMITPRICE.AsFloat);
              iStream:=quMenuSelKeySTREAM.AsInteger;
              iType:=1;
              Result:=True;
            end;
            quMenuSelKey.Active:=False;
          end;
        3:begin //������
          end;
        else
        end;
    end;
    taCF_All.Active:=False;
  end;
end;

procedure TfmSpec.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  cxButton16.Visible:=False;
  if CommonSet.KuponButton=1 then
  begin
    if CommonSet.KuponCode>0 then
    begin
      cxButton16.Caption:=CommonSet.KuponName;
      cxButton16.Visible:=True
    end;
  end;
end;

procedure TfmSpec.acMenuExecute(Sender: TObject);
begin
  with dmC do
  begin
    bBludoSel:=False;
    if quMenu.RecordCount>0 then
    begin
      try
        fmMenuClass.ViewM.BeginUpdate;
        quMenu.FullRefresh;
      finally
        fmMenuClass.ViewM.EndUpdate;
      end;
    end;

    fmMenuClass.ShowModal;
  end;
end;

procedure TfmSpec.Button1Click(Sender: TObject);
begin
  if Tab.iStatus>0 then exit;
  prWriteLog('---Menu;'+IntToStr(Tab.Id)+';'+Tab.NumTable+';'+Person.Name+';'+IntToStr(Tab.Id_Personal)+';');
  bSaveSpec:=False; //�� �������� ������� ��������
  acMenu.Execute;
  GridSpec.SetFocus;
end;

procedure TfmSpec.acChangeQuantityExecute(Sender: TObject);
Var rDif:Real;
    rQ:Real;
    cSum,cSumD:Real;
begin
  if Tab.iStatus>0 then
  begin
    Label18.Caption:='����� � ������� ��������. �������������� ���������.';
    exit;
  end;
  with dmC do
  begin
    bSaveSpec:=False; //�� �������� ������� ��������
    if quCurSpec.RecordCount=0 then exit;
    prWriteLog('---Quantity;'+quCurSpecSifr.AsString+';'+quCurSpecName.AsString+';'+quCurSpecQuantity.AsString+';'+Person.Name+';'+IntToStr(Tab.Id_Personal)+';');
    bAddPosFromSpec:=True;
    fmCalc.CalcEdit1.EditValue:=quCurSpecQuantity.AsFloat;
    fmCalc.ShowModal;
    if fmCalc.ModalResult=mrOk then
    begin
      rQ:=RoundEx(fmCalc.CalcEdit1.EditValue*1000)/1000;

      if quCurSpecIStatus.AsInteger=0 then
      begin
        CalcDiscontN(quCurSpecSifr.AsInteger,quCurSpecId.AsInteger,-1,quCurSpecSTREAM.AsInteger,quCurSpecPrice.AsFloat,rQ,0,Tab.DBar,Now,Check.DProc,Check.DSum);

        cSum:=RoundEx((quCurSpecPrice.AsFloat*rQ-Check.DSum)*100)/100;
        cSumD:=RoundEx((quCurSpecPrice.AsFloat*rQ)*100)/100-cSum;

        quCurSpec.Edit;
        quCurSpecQuantity.AsFloat:=rQ;
        quCurSpecDProc.AsFloat:=Check.DProc;
        quCurSpecDSum.AsFloat:=cSumD;
        quCurSpecSumma.AsFloat:=cSum;
        quCurSpec.Post;
      end
      else
      begin
        rDif:=rQ-quCurSpecQuantity.AsFloat;
        if rDif>0 then
        begin
          Check.Id_personal:=quCurSpecId_Personal.AsInteger;
          Check.NumTable:=quCurSpecNumTable.AsString;
          Check.Sifr:=quCurSpecSifr.AsInteger;
          Check.Price:=quCurSpecPrice.AsFloat;
          Check.Quantity:=quCurSpecQuantity.AsFloat;
          Check.Summa:=quCurSpecSumma.AsFloat;
          Check.DProc:=quCurSpecDProc.AsFloat;
          Check.DSum:=quCurSpecDSum.AsFloat;
          Check.Name:=quCurSpecName.AsString;
          Check.Code:=quCurSpecCode.AsString;
          Check.LimitM:=quCurSpecLimitM.AsInteger;
          Check.LinkM:=quCurSpecLinkM.AsInteger;
          Check.Stream:=quCurSpecStream.AsInteger;

          CalcDiscontN(Check.Sifr,Check.Max+1,-1,Check.Stream,Check.Price,rDif,0,Tab.DBar,Now,Check.DProc,Check.DSum);

          cSum:=RoundEx((quCurSpecPrice.AsFloat*rDif-Check.DSum)*100)/100;
          cSumD:=RoundEx((quCurSpecPrice.AsFloat*rDif)*100)/100-cSum;

          quCurSpec.Append;
          quCurSpecSTATION.AsInteger:=CommonSet.Station;
          quCurSpecID_TAB.AsInteger:=Tab.Id;
          quCurSpecId_Personal.AsInteger:=Check.Id_personal;
          quCurSpecNumTable.AsString:=Check.NumTable;
          quCurSpecId.AsInteger:=Check.Max+1;
          quCurSpecSifr.AsInteger:=Check.Sifr;
          quCurSpecPrice.AsFloat:=Check.Price;
          quCurSpecQuantity.AsFloat:=rDif;
          quCurSpecSumma.AsFloat:=cSum;
          quCurSpecDProc.AsFloat:=Check.DProc;
          quCurSpecDSum.AsFloat:=cSumD;
          quCurSpecIStatus.AsInteger:=0;
          quCurSpecName.AsString:=Check.Name;
          quCurSpecCode.AsString:=Check.Code;
          quCurSpecLinkM.AsInteger:=Check.LinkM;
          quCurSpecLimitM.AsInteger:=Check.LimitM;
          quCurSpecStream.AsInteger:=Check.Stream;
          quCurSpecQUEST.AsInteger:=cxImageComboBox1.EditValue;
          quCurSpec.Post;
          inc(Check.Max);

          if quCurSpecLinkM.AsInteger>0 then
          begin //������������
            acModify.Execute;
          end;
        end;
      end;
    end;
    bAddPosFromSpec:=False;
  end;
  GridSpec.SetFocus;
end;


procedure TfmSpec.acDelPosExecute(Sender: TObject);
  //������� �������
Var StrWk:String;
    IdHead, iSklad, iNum:Integer;
    rSum,rSumD:Real;
    bDel:Boolean;
    rQ:Real;
begin
  with dmC do
  with dmC1 do
  begin
    if quCurSpec.RecordCount=0 then exit;
    if quCurSpecIStatus.AsInteger=0 then //��� �� �������� - ��� �����������
    begin
      quCurMod.First;
      while not quCurMod.Eof do quCurMod.Delete;

      quCurSpec.Delete;
      exit;
    end;

    if not CanDo('prPosDel') then begin Label18.Caption:='��� ����.'; exit; end;
    fmMessDel:=TfmMessDel.Create(Application);
    if not CanDo('prPosDelNoSkl') then fmMessDel.cxButton1.Enabled:=False;
    if not CanDo('prPosDelWithSkl') then fmMessDel.cxButton2.Enabled:=False;

    fmMessDel.Label1.Caption:=quCurSpecName.AsString+' '+quCurSpecCode.AsString;
    Str(quCurSpecPrice.AsFloat:10:2,StrWk);
//    fmMessDel.Label2.Caption:='����� '+StrWk+'  ���-�� '; //+quCurSpecQuantity.asstring;
    fmMessDel.Label2.Caption:=  '���� '+StrWk+'  ���-�� '; //+quCurSpecQuantity.asstring;
    fmMessDel.CalcEdit1.Visible:=True;
    fmMessDel.CalcEdit1.EditValue:=quCurSpecQuantity.AsFloat;

    taDelPar.Active:=False;
    taDelPar.Active:=True;
    taDelPar.First;
    if taDelPar.RecordCount>0 then fmMessDel.cxLookupComboBox1.EditValue:=taDelParID.AsInteger
    else fmMessDel.cxLookupComboBox1.EditValue:=0;

    fmMessDel.ShowModal;

    iSklad:=0;
    bDel:=False;
    rQ:=RoundEx(fmMessDel.CalcEdit1.EditValue*1000)/1000;

    if (fmMessDel.ModalResult=mrYes)and(rQ>0) then
    begin
      iSklad:=0;
      bDel:=True;
      //�� ��������� ������ ��� ����������� �� �����
      FormLog('DelPos0',IntToStr(Person.Id)+' '+quCurSpecId.AsString+' '+quCurSpecSifr.AsString+' '+Trim(StrWk)+' '+IntToStr(Tab.iNumZ));
    end;
    if (fmMessDel.ModalResult=mrNo)and(rQ>0) then
    begin
      iSklad:=1;
      bDel:=True;
      //��� �������� ����� ����������� ������ ��� �� ������
      FormLog('DelPos1',IntToStr(Person.Id)+' '+quCurSpecId.AsString+' '+quCurSpecSifr.AsString+' '+Trim(StrWk)+' '+IntToStr(Tab.iNumZ));

      //����� �������� �������������� � + �.�. ��� ��������
      prAddMove(2,quCurSpecSifr.AsInteger,rQ);
    end;
      //����� ������ ������������.
    if bDel then
    begin

      idHead:=GetId('TH');

      //���� ���������
      taTabAll.Active:=False;
      taTabAll.ParamByName('TID').AsInteger:=idHead;
      taTabAll.Active:=True;

      trUpdTab.StartTransaction;
      taTabAll.Append;
      taTabAllID.AsInteger:=IdHead;
      taTabAllID_PERSONAL.AsInteger:=Tab.Id_Personal;
      taTabAllNUMTABLE.AsString:=Tab.NumTable;
      taTabAllQUESTS.AsInteger:=Tab.Quests;
      taTabAllTABSUM.AsFloat:=RoundEx(quCurSpecSUMMA.AsFloat*rQ/quCurSpecQuantity.AsFloat*100)/100;
      taTabAllBEGTIME.AsDateTime:=Tab.OpenTime;
      taTabAllENDTIME.AsDateTime:=now;
      taTabAllDISCONT.AsString:=Tab.DBar;
      taTabAllOPERTYPE.AsString:='Del';
      taTabAllCHECKNUM.AsInteger:=fmMessDel.cxLookupComboBox1.EditValue;
      taTabAllSKLAD.AsInteger:=iSklad; //�� ��������� ��� ���
      taTabAllSTATION.AsInteger:=CommonSet.Station*100+CommonSet.Station;
      taTabAllNUMZ.AsInteger:=Tab.iNumZ;
      taTabAllDELT.AsInteger:=fmMessDel.cxLookupComboBox1.EditValue;
      taTabAllSALET.AsInteger:=1;
      taTabAll.Post;
      trUpdTab.Commit;

      taTabAll.Active:=False;

      //���� ������������ 1-�������
      taSpecAll.Active:=False;
      taSpecAll.ParamByName('TID').AsInteger:=idHead;
      taSpecAll.Active:=True;

      trUpdTab.StartTransaction;
      iNum:=1;
      taSpecAll.Append;
      taSpecAllID_TAB.AsInteger:=IdHead;
      taSpecAllID.AsInteger:=iNum;
      taSpecAllID_PERSONAL.AsInteger:=Tab.Id_Personal;
      taSpecAllNUMTABLE.AsString:=Tab.NumTable;
      taSpecAllSIFR.AsInteger:=quCurSpecSifr.AsInteger;
      taSpecAllPRICE.AsFloat:=quCurSpecPrice.AsFloat;
      taSpecAllQUANTITY.AsFloat:=rQ;
      taSpecAllDISCOUNTPROC.AsFloat:=quCurSpecDProc.AsFloat;
      taSpecAllDISCOUNTSUM.AsFloat:=RoundEx(quCurSpecDSum.AsFloat*rQ/quCurSpecQuantity.AsFloat*100)/100;
      taSpecAllSUMMA.AsFloat:=RoundEx(quCurSpecSUMMA.AsFloat*rQ/quCurSpecQuantity.AsFloat*100)/100;
      taSpecAllISTATUS.AsInteger:=1;
      taSpecAllITYPE.AsInteger:=0;//�����
      taSpecAll.Post;
      inc(iNum);

      ViewSpec.BeginUpdate;
      ViewMod.BeginUpdate;
      try

        quCurMod.First;
        while not quCurMod.Eof do
        begin
          if quCurModSifr.AsInteger>0 then
          begin
            taSpecAll.Append;
            taSpecAllID_TAB.AsInteger:=IdHead;
            taSpecAllID.AsInteger:=iNum;
            taSpecAllID_PERSONAL.AsInteger:=Tab.Id_Personal;
            taSpecAllNUMTABLE.AsString:=Tab.NumTable;
            taSpecAllSIFR.AsInteger:=quCurModSifr.AsInteger;
            taSpecAllPRICE.AsFloat:=0;
            taSpecAllQUANTITY.AsFloat:=rQ;
            taSpecAllDISCOUNTPROC.AsFloat:=0;
            taSpecAllDISCOUNTSUM.AsFloat:=0;
            taSpecAllSUMMA.AsFloat:=0;
            taSpecAllISTATUS.AsInteger:=1;
            taSpecAllITYPE.AsInteger:=1;//�����������
            taSpecAll.Post;

            inc(iNum);
          end;

          quCurMod.Next;
        end;
        trUpdTab.Commit;
        taSpecAll.Active:=False;

      //�������� ������ ��� �� ������
        CloseTe(taServP);

        taServP.Append;
        taServPName.AsString:=quCurSpecName.AsString;
        taServPCode.AsString:=quCurSpecCode.AsString;
        taServPQuant.AsFloat:=rQ;
        taServPStream.AsInteger:=quCurSpecStream.AsInteger;
        taServPiType.AsInteger:=0; //�����
        taServP.Post;

        PrintServCh('������ ');

        if rQ=quCurSpecQuantity.AsFloat then
        begin
          while not quCurMod.Eof do quCurMod.delete;
          quCurSpec.Delete;
        end else
        begin
          quCurMod.First;
          while not quCurMod.Eof do
          begin
            if quCurModSifr.AsInteger>0 then
            begin
              quCurMod.Edit;
              quCurModQUANTITY.AsFloat:=quCurSpecQuantity.AsFloat-rQ;
              quCurMod.Post;
            end;

            quCurMod.Next;
          end;

          rSumD:=quCurSpecDSum.AsFloat-RoundEx(quCurSpecDSum.AsFloat*rQ/quCurSpecQuantity.AsFloat*100)/100;
          rSum:=quCurSpecSUMMA.AsFloat-RoundEx(quCurSpecSUMMA.AsFloat*rQ/quCurSpecQuantity.AsFloat*100)/100;

          quCurSpec.Edit;
          quCurSpecQUANTITY.AsFloat:=quCurSpecQuantity.AsFloat-rQ;
          quCurSpecDSum.AsFloat:=rSumD;
          quCurSpecSUMMA.AsFloat:=rSum;
          quCurSpec.Post;
        end;

        rSum:=0;
        quCurSpec.First;
        while not quCurSpec.Eof do
        begin
          rSum:=rSum+quCurSpecSumma.AsFloat;
          quCurSpec.Next;
        end;

        Tab.Summa:=rSum;
        Tab.SaleT:=quSaleTID.AsInteger;

        fmMainCashRn.TabView.BeginUpdate;
        if quTabs.Locate('ID',Tab.Id,[]) then
        begin
          quTabs.Edit;
          quTabsTABSUM.AsFloat:=Tab.Summa;
          quTabs.Post;
          quTabs.Refresh;
        end;
        fmMainCashRn.TabView.EndUpdate;

        SaveSpecN; // � ������� ������ �����

        quCurSpec.First;

      finally
        ViewMod.EndUpdate;
        ViewSpec.EndUpdate;
      end;

      CreateViewPers1;
    end;

    taDelPar.Active:=False;
  end;
  fmMessDel.Release;
end;

procedure TfmSpec.cxButton2Click(Sender: TObject);
begin
  if Tab.iStatus>0 then exit;
  if dmC.quCurSpec.RecordCount=0 then exit;
  prWriteLog('---Del;'+dmC.quCurSpecSifr.AsString+';'+dmC.quCurSpecName.AsString+';');
  acDelPos.Execute;
  GridSpec.SetFocus;
end;

procedure TfmSpec.acMovePosExecute(Sender: TObject);
Var StrWk:String;
    IdHead, MaxId, Id, Id_Pos,iNewNumZ:INteger;
    rQ,rSum:Real;
    cSum,cSumD:Real;
    iGost,iCountPos,iDm:Integer;
    bDel:Boolean;
begin
  //����������� �������
  if not CanDo('prPosMove') then  begin Label18.Caption:='��� ����.'; exit; end;

  with dmC do
  begin
    if quCurSpec.RecordCount=0 then exit;

    if quCurSpecIStatus.AsInteger=0 then begin Label18.Caption:='���������� ����� ������ ����������� �������.'; exit; end;
    if Tab.iStatus>0 then begin Label18.Caption:='����� �� �������� ����� ������� �� �������������.'; exit; end;

    fmPosMove:=TfmPosMove.Create(Application);
    with dmC do
    begin
      fmPosMove.Label1.Caption:=quCurSpecName.AsString+' '+quCurSpecCode.AsString;
      Str(quCurSpecSumma.AsFloat:10:2,StrWk);
      fmPosMove.Label2.Caption:='����� '+StrWk+'  ���-�� '+quCurSpecQuantity.asstring;
      fmPosMove.CalcEdit1.EditValue:=quCurSpecQuantity.AsFloat;
      fmPosMove.cxImageComboBox1.ItemIndex:=fmSpec.cxImageComboBox1.ItemIndex;

      fmPosMove.ShowModal;
      if fmPosMove.ModalResult=mrOk then
      begin
        bMoveSpec:=True;
        prSetButton(False);
        try
        Str(quCurSpecSumma.AsFloat:10:2,StrWk);
        rQ:=quCurSpecQuantity.AsFloat;
        bDel:=False;
        if fmPosMove.cxRadioButton1.Checked then //����� ��� �� �����
        begin
          if (fmPosMove.CalcEdit1.EditValue<=rQ)and(fmPosMove.CalcEdit1.EditValue>0) then
          begin
            if fmPosMove.cxRadioGroup1.ItemIndex=0 then
            begin
              iNewNumZ:=dmC1.GetNumZ;

              FormLog('MovePos0',IntToStr(Person.Id)+' '+quCurSpecId.AsString+' '+quCurSpecSifr.AsString+' '+Trim(StrWk)+' '+INtToStr(Tab.iNumZ)+' '+INtToStr(iNewNumZ));
              prWriteLog('MovePos0 '+IntToStr(Person.Id)+' '+quCurSpecId.AsString+' '+quCurSpecSifr.AsString+' '+Trim(StrWk)+' '+INtToStr(Tab.iNumZ)+' '+INtToStr(iNewNumZ));
              //������� ����� ����
              FormLog('CreateTab',IntToStr(Tab.Id_Personal)+' '+fmPosMove.TextEdit1.Text+' '+IntToStr(iNewNumZ));
              prWriteLog('CreateTab '+IntToStr(Tab.Id_Personal)+' '+fmPosMove.TextEdit1.Text+' '+IntToStr(iNewNumZ));

              taTabs.Active:=False;
              taTabs.Active:=True;

              IdHead:=GetId('TabH');

              trUpdTab.StartTransaction;

              taTabs.Append;
              taTabsId.AsInteger:=IdHead;
              taTabsID_PERSONAL.AsInteger:=Tab.Id_Personal;
              taTabsNUMTABLE.AsString:=fmPosMove.TextEdit1.Text;
              taTabsQUESTS.AsInteger:=1;
              taTabsTABSUM.AsFloat:=RoundEx(fmPosMove.CalcEdit1.EditValue*quCurSpecPrice.AsFloat*100)/100;
              taTabsBEGTIME.AsDateTime:=Now;
              taTabsISTATUS.AsInteger:=0; // ����������� c������ � ���.
              taTabsDISCONT.AsString:=''; //��� ������ ������
              taTabsNUMZ.AsInteger:=iNewNumZ;
              taTabsSTATION.AsInteger:=CommonSet.Station;
              taTabs.Post;

              trUpdTab.Commit;

              taTabs.Active:=False;

              Id:=1;
              taSpec.Active:=False;
              taSpec.ParamByName('ITAB').AsInteger:=IdHead;
              taSpec.Active:=True;
              trUpdTab.StartTransaction;

              taSpec.Append;
              taSpecID_TAB.AsInteger:=IdHead;
              taSpecID_PERSONAL.AsInteger:=Tab.Id_Personal;
              taSpecNUMTABLE.AsString:=fmPosMove.TextEdit1.Text;
              taSpecID.AsInteger:=Id;//1
              taSpecID_POS.AsInteger:=0;//�����
              taSpecSIFR.AsInteger:=quCurSpecSifr.AsInteger;
              taSpecPRICE.AsFloat:=quCurSpecPrice.AsFloat;
              taSpecQUANTITY.AsFloat:=fmPosMove.CalcEdit1.EditValue;
              taSpecSUMMA.AsFloat:=RoundEx(fmPosMove.CalcEdit1.EditValue*quCurSpecPrice.AsFloat*100)/100;
              taSpecDISCOUNTPROC.AsFloat:=0;
              taSpecDISCOUNTSUM.AsFloat:=0;
              taSpecISTATUS.AsInteger:=1; // ����� �� ���� ������, ������ ��� ������ �� ����
              taSpecLIMITM.AsInteger:=quCurSpecLIMITM.AsInteger;
              taSpecLINKM.AsInteger:=quCurSpecLINKM.AsInteger;
              taSpecITYPE.AsInteger:=0; //�����
              taSpecQUEST.AsInteger:=quCurSpecQUEST.AsInteger;
              taSpec.Post;

              inc(Id);

              try
                ViewMod.BeginUpdate;

                quCurMod.First;
                while not quCurMod.Eof do
                begin
                  taSpec.Append;

                  taSpecID_TAB.AsInteger:=IdHead;
                  taSpecID_PERSONAL.AsInteger:=Tab.Id_Personal;
                  taSpecNUMTABLE.AsString:=fmPosMove.TextEdit1.Text;
                  taSpecID.AsInteger:=Id;
                  taSpecID_POS.AsInteger:=1;//1 �� � ����� ���� � ����� �������������� ��������
                  taSpecSIFR.AsInteger:=quCurModSifr.AsInteger;
                  taSpecPRICE.AsFloat:=0;
                  taSpecQUANTITY.AsFloat:=fmPosMove.CalcEdit1.EditValue;
                  taSpecSUMMA.AsFloat:=0;
                  taSpecDISCOUNTPROC.AsFloat:=0;
                  taSpecDISCOUNTSUM.AsFloat:=0;
                  taSpecISTATUS.AsInteger:=1;
                  taSpecLIMITM.AsInteger:=0;
                  taSpecLINKM.AsInteger:=0;
                  taSpecITYPE.AsInteger:=1; //������������
                  taSpecQUEST.AsInteger:=quCurSpecQUEST.AsInteger;
                  taSpec.Post;

                  inc(id);
                  quCurMod.Next;
                end;
                quCurMod.First;
              finally
                ViewMod.EndUpdate;
              end;
              trUpdTab.Commit;
              taSpec.Active:=False;
              bDel:=True;
            end; //����� ������� ��� �����
            //� ���������
            if fmPosMove.cxRadioGroup1.ItemIndex=1 then
            begin
              if quTabsPers.RecordCount>0 then
              begin
                quTabsP1.Active:=False;
                quTabsP1.SelectSQL.Clear;
                quTabsP1.SelectSQL.Add('SELECT ID, ID_PERSONAL, NUMTABLE, QUESTS, TABSUM, DISCONT, BEGTIME, ISTATUS, NUMZ');
                quTabsP1.SelectSQL.Add('FROM TABLES');
                quTabsP1.SelectSQL.Add('Where Id_personal='+IntToStr(Tab.Id_Personal));
                quTabsP1.SelectSQL.Add('and ISTATUS=0'); //������ ������c��� � ������ �� ��� ��������� ���� - �������
                quTabsP1.SelectSQL.Add('and ID<>'+IntToStr(Tab.Id)); //������ ������c��� � ����
                quTabsP1.SelectSQL.Add('and ID='+IntToStr(quTabsPersID.AsInteger)); //����� ������� ���������
                quTabsP1.Active:=True;
                if quTabsP1.RecordCount>0 then
                begin
                  IdHead:=quTabsPersID.AsInteger;

                  FormLog('MovePos1',IntToStr(Person.Id)+' '+quCurSpecId.AsString+' '+quCurSpecSifr.AsString+' '+Trim(StrWk)+' '+INtToStr(Tab.iNumZ)+' '+INtToStr(quTabsPersNUMZ.AsInteger)+' to '+its(IdHead));
                  prWriteLog('MovePos1 '+IntToStr(Person.Id)+' '+quCurSpecId.AsString+' '+quCurSpecSifr.AsString+' '+Trim(StrWk)+' '+INtToStr(Tab.iNumZ)+' '+INtToStr(quTabsPersNUMZ.AsInteger)+' to '+its(IdHead));

                  taSpec1.Active:=False;
                  taSpec1.ParamByName('IDTAB').AsInteger:=IdHead;
                  taSpec1.Active:=True;

                  rSum:=0; MaxId:=0;
                  taSpec1.First;
                  while not taSpec1.Eof do
                  begin
                    if taSpec1Id.AsInteger>MaxId then MaxId:=taSpec1Id.AsInteger;
                    rSum:=rSum+taSpec1SUMMA.AsFloat;
                    taSpec1.Next;
                  end;

                  inc(MaxId);

                  taTabs.Active:=False;
                  taTabs.Active:=True;

                  if taTabs.Locate('ID',IDHead,[]) then
                  begin
                    trUpdTab.StartTransaction;
                    taTabs.Edit;
                    taTabsTABSUM.AsFloat:=rSum+RoundEx(fmPosMove.CalcEdit1.EditValue*quCurSpecPrice.AsFloat*100)/100;
                    taTabs.Post;
                    trUpdTab.Commit;

                    trUpdTab.StartTransaction;
                    taSpec1.Append;
                    taSpec1ID_TAB.AsInteger:=IdHead;
                    taSpec1ID_PERSONAL.AsInteger:=Tab.Id_Personal;
                    taSpec1NUMTABLE.AsString:=taTabsNUMTABLE.AsString;
                    taSpec1ID.AsInteger:=MaxId;
                    taSpec1ID_POS.AsInteger:=0; //�����
                    taSpec1SIFR.AsInteger:=quCurSpecSifr.AsInteger;
                    taSpec1PRICE.AsFloat:=quCurSpecPrice.AsFloat;
                    taSpec1QUANTITY.AsFloat:=fmPosMove.CalcEdit1.EditValue;
                    taSpec1SUMMA.AsFloat:=RoundEx(fmPosMove.CalcEdit1.EditValue*quCurSpecPrice.AsFloat*100)/100;
                    taSpec1DISCOUNTPROC.AsFloat:=0;
                    taSpec1DISCOUNTSUM.AsFloat:=0;
                    taSpec1ISTATUS.AsInteger:=1; // ����� �� ���� ������, ������ ��� ������ �� ����
                    taSpec1LIMITM.AsInteger:=quCurSpecLIMITM.AsInteger;
                    taSpec1LINKM.AsInteger:=quCurSpecLINKM.AsInteger;
                    taSpec1ITYPE.AsInteger:=0; //�����
                    taSpec1QUEST.AsInteger:=quCurSpecQUEST.AsInteger;
                    taSpec1.Post;

                    inc(MaxId);

                    Id_Pos:=MaxId-1;
                    try
                      ViewMod.BeginUpdate;
                      quCurMod.First;
                      while not quCurMod.Eof do
                      begin
                        taSpec1.Append;
                        taSpec1ID_TAB.AsInteger:=IdHead;
                        taSpec1ID_PERSONAL.AsInteger:=Tab.Id_Personal;
                        taSpec1NUMTABLE.AsString:=taTabsNUMTABLE.AsString;
                        taSpec1ID.AsInteger:=MaxId;
                        taSpec1ID_POS.AsInteger:=ID_POS;
                        taSpec1SIFR.AsInteger:=quCurModSifr.AsInteger;
                        taSpec1PRICE.AsFloat:=0;
                        taSpec1QUANTITY.AsFloat:=fmPosMove.CalcEdit1.EditValue;
                        taSpec1SUMMA.AsFloat:=0;
                        taSpec1DISCOUNTPROC.AsFloat:=0;
                        taSpec1DISCOUNTSUM.AsFloat:=0;
                        taSpec1ISTATUS.AsInteger:=1;
                        taSpec1LIMITM.AsInteger:=0;
                        taSpec1LINKM.AsInteger:=0;
                        taSpec1ITYPE.AsInteger:=1; //������������
                        taSpec1QUEST.AsInteger:=quCurSpecQUEST.AsInteger;
                        taSpec1.Post;

                        inc(Maxid);
                        quCurMod.Next;
                      end;
                      quCurMod.First;
                    finally
                      ViewMod.EndUpdate;
                    end;

                    trUpdTab.Commit;
                    bDel:=True;
                  end;
                end else showmessage('� ������ ������ ������� � ��������� ����� ���������� ...');
                quTabsP1.Active:=False;
              end else showmessage('�������� ���� ���� ��������� ...');
              taTabs.Active:=False;
              taSpec1.Active:=False;
            end;
          end;

//����������� ������� �����
          if bDel then //��� ��������� ������ ������ �� �������� ������
          begin
            rQ:=RoundEx((rQ-fmPosMove.CalcEdit1.EditValue)*1000)/1000; //� ��� �� ���-��
            if rQ=0 then
            begin
              quCurMod.First;
              while not quCurMod.Eof do quCurMod.Delete;

              quCurSpec.Delete;
            end
            else
            begin
              //��� ����� ��������� � ��������� �.�. �������� ���
              CalcDiscontN(quCurSpecSifr.AsInteger,quCurSpecId.AsInteger,-1,quCurSpecSTREAM.AsInteger,quCurSpecPrice.AsFloat,rQ,0,Tab.DBar,Now,Check.DProc,Check.DSum);

              cSum:=RoundEx((quCurSpecPrice.AsFloat*rQ-Check.DSum)*100)/100;
              cSumD:=RoundEx((quCurSpecPrice.AsFloat*rQ)*100)/100-cSum;

              quCurSpec.Edit;
              quCurSpecQuantity.AsFloat:=rQ;
              quCurSpecDProc.AsFloat:=Check.DProc;
              quCurSpecDSum.AsFloat:=cSumD;
              quCurSpecSumma.AsFloat:=cSum;
              quCurSpec.Post;

              quCurMod.First;
              while not quCurMod.Eof do
              begin
                quCurMod.Edit;
                quCurModQUANTITY.AsFloat:=rQ;
                quCurMod.Post;

                quCurMod.Next;
              end;
            end;

           //����� ��������� ����� �� �������� ������������
            try
              ViewSpec.BeginUpdate;

              Tab.Summa:=0;
              quCurSpec.First;
              while not quCurSpec.Eof do
              begin
                Tab.Summa:=Tab.Summa+quCurSpecSumma.AsFloat;
                quCurSpec.Next;
              end;

              Tab.SaleT:=dmC1.quSaleTID.AsInteger;

              fmMainCashRn.TabView.BeginUpdate;
              if quTabs.Locate('ID',Tab.Id,[]) then
              begin
                quTabs.Edit;
                quTabsTABSUM.AsFloat:=Tab.Summa;
                quTabs.Post;
                quTabs.Refresh;
              end;
              fmMainCashRn.TabView.EndUpdate;


              SaveSpecN; delay(33);

              quCurSpec.First;
            finally
              ViewSpec.EndUpdate;
            end;
            //��������� �������� ���
            CreateViewPers1;
          end;
        end else //����� ���������
        begin
          try
            ViewSpec.BeginUpdate;
            ViewMod.BeginUpdate;

            iCountPos:=0;
            rSum:=0;
            iGost:=fmPosMove.cxImageComboBox1.EditValue;
            quCurSpec.Filtered:=False;

            quCurSpec.First; //���� ����� �� �����
            while not quCurSpec.Eof do
            begin
              if quCurSpecQUEST.AsInteger=iGost then
              begin
                if quCurSpecIStatus.AsInteger=0 then iCountPos:=-1000;
                rSum:=rSum+quCurSpecSUMMA.AsFloat;
                inc(iCountPos);
              end;
              quCurSpec.Next;
            end;
          finally
            ViewSpec.EndUpdate;
            ViewMod.EndUpdate;
          end;

          if iCountPos>0 then   //���� � ������� �� ����� ���-��
          begin

          bDel:=False;

          if fmPosMove.cxRadioGroup1.ItemIndex=0 then
          begin
            try
              ViewSpec.BeginUpdate;
              ViewMod.BeginUpdate;

              iNewNumZ:=dmC1.GetNumZ;

              FormLog('MovePos0G',IntToStr(Person.Id)+' '+quCurSpecId.AsString+' '+quCurSpecSifr.AsString+' '+Trim(StrWk)+' '+INtToStr(Tab.iNumZ)+' '+INtToStr(iNewNumZ));
              prWriteLog('MovePos0G '+IntToStr(Person.Id)+' '+quCurSpecId.AsString+' '+quCurSpecSifr.AsString+' '+Trim(StrWk)+' '+INtToStr(Tab.iNumZ)+' '+INtToStr(iNewNumZ));
              //������� ����� ����
              FormLog('CreateTabG',IntToStr(Tab.Id_Personal)+' '+fmPosMove.TextEdit1.Text+' '+IntToStr(iNewNumZ));
              prWriteLog('CreateTabG '+IntToStr(Tab.Id_Personal)+' '+fmPosMove.TextEdit1.Text+' '+IntToStr(iNewNumZ));

              taTabs.Active:=False;
              taTabs.Active:=True;

              IdHead:=GetId('TabH');

              trUpdTab.StartTransaction;

              taTabs.Append;
              taTabsId.AsInteger:=IdHead;
              taTabsID_PERSONAL.AsInteger:=Tab.Id_Personal;
              taTabsNUMTABLE.AsString:=fmPosMove.TextEdit1.Text;
              taTabsQUESTS.AsInteger:=1;
              taTabsTABSUM.AsFloat:=rv(rSum);
              taTabsBEGTIME.AsDateTime:=Now;
              taTabsISTATUS.AsInteger:=0; // ����������� c������ � ���.
              taTabsDISCONT.AsString:=Tab.DBar; //��� ������ ������
              taTabsNUMZ.AsInteger:=iNewNumZ;
              taTabsSTATION.AsInteger:=CommonSet.Station;
              taTabs.Post;

              trUpdTab.Commit;

              taTabs.Active:=False;

              Id:=1;
              taSpec.Active:=False;
              taSpec.ParamByName('ITAB').AsInteger:=IdHead;
              taSpec.Active:=True;

              trUpdTab.StartTransaction;

              quCurSpec.First; //���� ����� �� �����
              while not quCurSpec.Eof do
              begin
                if quCurSpecQUEST.AsInteger=iGost then
                begin
                  taSpec.Append;
                  taSpecID_TAB.AsInteger:=IdHead;
                  taSpecID_PERSONAL.AsInteger:=Tab.Id_Personal;
                  taSpecNUMTABLE.AsString:=fmPosMove.TextEdit1.Text;
                  taSpecID.AsInteger:=Id;//1
                  taSpecID_POS.AsInteger:=0;//�����
                  taSpecSIFR.AsInteger:=quCurSpecSifr.AsInteger;
                  taSpecPRICE.AsFloat:=quCurSpecPrice.AsFloat;
                  taSpecQUANTITY.AsFloat:=quCurSpecQUANTITY.AsFloat;
                  taSpecSUMMA.AsFloat:=rv(quCurSpecSUMMA.AsFloat);
                  taSpecDISCOUNTPROC.AsFloat:=quCurSpecDPROC.AsFloat;
                  taSpecDISCOUNTSUM.AsFloat:=quCurSpecDSUM.AsFloat;
                  taSpecISTATUS.AsInteger:=quCurSpecISTATUS.AsInteger; // ����� �� ���� ������, ������ ��� ������ �� ����
                  taSpecLIMITM.AsInteger:=quCurSpecLIMITM.AsInteger;
                  taSpecLINKM.AsInteger:=quCurSpecLINKM.AsInteger;
                  taSpecITYPE.AsInteger:=0; //�����
                  taSpecQUEST.AsInteger:=quCurSpecQUEST.AsInteger;
                  taSpec.Post;

                  iDm:=0;

                  quCurMod.Active:=False;
                  quCurMod.ParamByName('IDT').AsInteger:=quCurSpecID_TAB.AsInteger;
                  quCurMod.ParamByName('IDP').AsInteger:=quCurSpecID.AsInteger;
                  quCurMod.ParamByName('STATION').AsInteger:=CommonSet.Station;
                  quCurMod.Active:=True;

                  quCurMod.First;
                  while not quCurMod.Eof do
                  begin
                    if quCurModSifr.AsInteger>0 then
                    begin //������ ��� ������������� ��� ���������

                      taSpec.Append;
                      taSpecID_TAB.AsInteger:=IdHead;
                      taSpecID_PERSONAL.AsInteger:=Tab.Id_Personal;
                      taSpecNUMTABLE.AsString:=fmPosMove.TextEdit1.Text;
                      taSpecID.AsInteger:=Id+Idm+1;
                      taSpecID_POS.AsInteger:=Id;//������� ��� �������������
                      taSpecSIFR.AsInteger:=quCurModSifr.AsInteger;
                      taSpecPRICE.AsFloat:=0;
                      taSpecQUANTITY.AsFloat:=quCurModQUANTITY.AsFloat;
                      taSpecSUMMA.AsFloat:=0;
                      taSpecDISCOUNTPROC.AsFloat:=0;
                      taSpecDISCOUNTSUM.AsFloat:=0;
                      taSpecISTATUS.AsInteger:=1;
                      taSpecLIMITM.AsInteger:=0;
                      taSpecLINKM.AsInteger:=0;
                      taSpecITYPE.AsInteger:=1; //������������
                      taSpecQUEST.AsInteger:=quCurSpecQUEST.AsInteger;
                      taSpec.Post;
                      inc(idm);

                    end;

                    quCurMod.Next;
                  end;
                  id:=id+idm;
                  inc(id);
                end;

                quCurSpec.Next;
              end;

              quCurSpec.First;
              quCurMod.First;

              trUpdTab.Commit;

              bDel:=True;

//             //����� ������� ��� �����
            finally
              taSpec.Active:=False;
              ViewSpec.EndUpdate;
              ViewMod.EndUpdate;
            end;
          end;

            //� ���������
          if fmPosMove.cxRadioGroup1.ItemIndex=1 then
          begin
            if quTabsPers.RecordCount>0 then
            begin
              quTabsP1.Active:=False;
              quTabsP1.SelectSQL.Clear;
              quTabsP1.SelectSQL.Add('SELECT ID, ID_PERSONAL, NUMTABLE, QUESTS, TABSUM, DISCONT, BEGTIME, ISTATUS, NUMZ');
              quTabsP1.SelectSQL.Add('FROM TABLES');
              quTabsP1.SelectSQL.Add('Where Id_personal='+IntToStr(Tab.Id_Personal));
              quTabsP1.SelectSQL.Add('and ISTATUS=0'); //������ ������c��� � ������ �� ��� ��������� ���� - �������
              quTabsP1.SelectSQL.Add('and ID<>'+IntToStr(Tab.Id)); //������ ������c��� � ����
              quTabsP1.SelectSQL.Add('and ID='+IntToStr(quTabsPersID.AsInteger)); //����� ������� ���������
              quTabsP1.Active:=True;
              if quTabsP1.RecordCount>0 then
              begin
                delay(33);
                IdHead:=quTabsPersID.AsInteger;

                FormLog('MovePos1G',IntToStr(Person.Id)+' '+quCurSpecId.AsString+' '+quCurSpecSifr.AsString+' '+Trim(StrWk)+' '+INtToStr(Tab.iNumZ)+' '+INtToStr(quTabsPersNUMZ.AsInteger)+' to '+its(IdHead));
                prWriteLog('MovePos1G '+IntToStr(Person.Id)+' '+quCurSpecId.AsString+' '+quCurSpecSifr.AsString+' '+Trim(StrWk)+' '+INtToStr(Tab.iNumZ)+' '+INtToStr(quTabsPersNUMZ.AsInteger)+' to '+its(IdHead));

                try
                  ViewSpec.BeginUpdate;
                  ViewMod.BeginUpdate;

                  delay(33);

                  taSpec.Active:=False;
                  taSpec.ParamByName('ITAB').AsInteger:=IdHead;
                  taSpec.Active:=True;

                  //rSum:=0; ��� ��������� �� �����
                  MaxId:=0;
                  taSpec.First;
                  while not taSpec.Eof do
                  begin
                    if taSpecId.AsInteger>MaxId then MaxId:=taSpecId.AsInteger;
                    rSum:=rSum+taSpecSUMMA.AsFloat;
                    taSpec.Next;
                  end;
                  taSpec.First;

                  inc(MaxId);

                  taTabs.Active:=False;
                  taTabs.Active:=True;

                  delay(33);

                  if taTabs.Locate('ID',IDHead,[]) then
                  begin
                    trUpdTab.StartTransaction;
                    taTabs.Edit;
                    taTabsTABSUM.AsFloat:=rSum;
                    taTabs.Post;
                    trUpdTab.Commit;

                    taTabs.Active:=False;

                    Id:=MaxId;

                    trUpdTab.StartTransaction;

                    quCurSpec.First; //���� ����� �� �����
                    while not quCurSpec.Eof do
                    begin
                      if quCurSpecQUEST.AsInteger=iGost then
                      begin
                        taSpec.Append;
                        taSpecID_TAB.AsInteger:=IdHead;
                        taSpecID_PERSONAL.AsInteger:=Tab.Id_Personal;
                        taSpecNUMTABLE.AsString:=quTabsPersNUMTABLE.AsString;
                        taSpecID.AsInteger:=Id;//1
                        taSpecID_POS.AsInteger:=0;//�����
                        taSpecSIFR.AsInteger:=quCurSpecSifr.AsInteger;
                        taSpecPRICE.AsFloat:=quCurSpecPrice.AsFloat;
                        taSpecQUANTITY.AsFloat:=quCurSpecQUANTITY.AsFloat;
                        taSpecSUMMA.AsFloat:=rv(quCurSpecSUMMA.AsFloat);
                        taSpecDISCOUNTPROC.AsFloat:=quCurSpecDPROC.AsFloat;
                        taSpecDISCOUNTSUM.AsFloat:=quCurSpecDSUM.AsFloat;
                        taSpecISTATUS.AsInteger:=quCurSpecISTATUS.AsInteger; // ����� �� ���� ������, ������ ��� ������ �� ����
                        taSpecLIMITM.AsInteger:=quCurSpecLIMITM.AsInteger;
                        taSpecLINKM.AsInteger:=quCurSpecLINKM.AsInteger;
                        taSpecITYPE.AsInteger:=0; //�����
                        taSpecQUEST.AsInteger:=quCurSpecQUEST.AsInteger;
                        taSpec.Post;

                        iDm:=0;

                        quCurMod.Active:=False;
                        quCurMod.ParamByName('IDT').AsInteger:=quCurSpecID_TAB.AsInteger;
                        quCurMod.ParamByName('IDP').AsInteger:=quCurSpecID.AsInteger;
                        quCurMod.ParamByName('STATION').AsInteger:=CommonSet.Station;
                        quCurMod.Active:=True;

                        quCurMod.First;
                        while not quCurMod.Eof do
                        begin
                          if quCurModSifr.AsInteger>0 then
                          begin //������ ��� ������������� ��� ���������
                            taSpec.Append;

                            taSpecID_TAB.AsInteger:=IdHead;
                            taSpecID_PERSONAL.AsInteger:=quTabsPersID_PERSONAL.AsInteger;
                            taSpecNUMTABLE.AsString:=quTabsPersNUMTABLE.AsString;
                            taSpecID.AsInteger:=Id+Idm+1;
                            taSpecID_POS.AsInteger:=Id;//������� ��� �������������
                            taSpecSIFR.AsInteger:=quCurModSifr.AsInteger;
                            taSpecPRICE.AsFloat:=0;
                            taSpecQUANTITY.AsFloat:=quCurModQUANTITY.AsFloat;
                            taSpecSUMMA.AsFloat:=0;
                            taSpecDISCOUNTPROC.AsFloat:=0;
                            taSpecDISCOUNTSUM.AsFloat:=0;
                            taSpecISTATUS.AsInteger:=1;
                            taSpecLIMITM.AsInteger:=0;
                            taSpecLINKM.AsInteger:=0;
                            taSpecITYPE.AsInteger:=1; //������������
                            taSpecQUEST.AsInteger:=quCurSpecQUEST.AsInteger;
                            taSpec.Post;

                            inc(idm);
                          end;
                          quCurMod.Next;
                        end;
                        id:=id+idm;
                        inc(id);
                      end;

                      quCurSpec.Next;
                    end;
                    quCurSpec.First;
                    quCurMod.First;

                    trUpdTab.Commit;
                    bDel:=True;
                  end;
                finally
                  taSpec.Active:=False;
                  ViewSpec.EndUpdate;
                  ViewMod.EndUpdate;
                end;
              end else showmessage('� ������ ������ ������� � ��������� ����� ���������� ...');
              quTabsP1.Active:=False;
            end else showmessage('�������� ���� ���� ��������� ...');
          end;

//����� ��������� ����� �� �������� ������������
          if bDel then //��� ��������� ������ ������ �� �������� ������
          begin
            delay(10);
            try
              ViewSpec.BeginUpdate;
              ViewMod.BeginUpdate;
              quCurSpec.First; //���� ����� �� �����
              while not quCurSpec.Eof do
              begin
                if quCurSpecQUEST.AsInteger=iGost then
                begin
                  quCurMod.Active:=False;
                  quCurMod.ParamByName('IDT').AsInteger:=quCurSpecID_TAB.AsInteger;
                  quCurMod.ParamByName('IDP').AsInteger:=quCurSpecID.AsInteger;
                  quCurMod.ParamByName('STATION').AsInteger:=CommonSet.Station;
                  quCurMod.Active:=True;

                  quCurMod.First;
                  while not quCurMod.Eof do quCurMod.Delete;

                  quCurSpec.Delete;
                end else quCurSpec.Next;
              end;

              Tab.Summa:=0;
              quCurSpec.First;
              while not quCurSpec.Eof do
              begin
                Tab.Summa:=Tab.Summa+quCurSpecSumma.AsFloat;
                quCurSpec.Next;
              end;

              Tab.SaleT:=dmC1.quSaleTID.AsInteger;

              fmMainCashRn.TabView.BeginUpdate;
              if quTabs.Locate('ID',Tab.Id,[]) then
              begin
                quTabs.Edit;
                quTabsTABSUM.AsFloat:=Tab.Summa;
                quTabs.Post;
                quTabs.Refresh;
              end;
              fmMainCashRn.TabView.EndUpdate;
              fmSpec.cxCheckBox1.Checked:=False;

              delay(10);
              SaveSpecN;
              delay(33);

              quCurSpec.First;

            finally
              ViewSpec.EndUpdate;
              ViewMod.EndUpdate;
            end;

            //��������� �������� ���
            CreateViewPers1;

            end //if iCounPos>0 �� �����
            else
            begin
              if iCountPos=0 then showmessage('�� ���������� ����� ��� �������.');
//              else showmessage('���������� ����� ������ ����������� �������.'); // iCountPos<0
            end;
          end;
        end;
        finally
          bMoveSpec:=False; //�������� ����������
          delay(10);
          prSetButton(True);
        end;
      end;
    end;
  end;
  fmPosMove.Release;
end;

procedure TfmSpec.FormShow(Sender: TObject);
begin
  if not CanDo('prPosMove') then cxButton4.Visible:=False
  else cxButton4.Visible:=True;

  bPressB12Spec:=False;
  cxButton12.Visible:=True;
  cxButton12.Enabled:=True;
  if not CanDo('prPrintCheck') then cxButton12.Visible:=False
  else begin
 {   if CommonSet.CashNum<=0 then
    begin
      cxButton12.Visible:=False;
      cxButton12.Enabled:=False;
    end else cxButton12.Visible:=True;}
  end;
  if (CommonSet.SpecChar=1)or(CommonSet.CashNum=0) then  //�������� ��� ������������ ������ �� ������������, ���� �� ������������ ������ ���
  begin
    cxButton12.Visible:=False;
    cxButton12.Enabled:=False;
  end;
  Edit1.Text:='';
  DiscountBar:='';
  GridSpec.SetFocus;
  if GridSpec.Tag=0 then  //1024�768
  begin
    Top:=40;
    Left:=20;
  end;
  if GridSpec.Tag=1 then  //800�600
  begin
    Top:=10;
    Left:=10;
  end;

  bPrintCheck:=False;


  if CanDo('prPrintPre') then   //�������� ����
  begin
    if CommonSet.iPrePrintSt=0 then
    begin
      Button5.Visible:=True;
      acPrePrint.Enabled:=True;
    end else
    begin
      if CanDo('prPrintPreSt') then
      begin
        Button5.Visible:=True;
        acPrePrint.Enabled:=True;
      end else
      begin
        acPrePrint.Enabled:=False;
        Button5.Visible:=False;
      end;
    end;
  end
  else
  begin
    acPrePrint.Enabled:=False;
    Button5.Visible:=False;
  end;

  if fmSpec.Tag=1 then
  begin
    Timer2.Enabled:=True;
    fmSpec.Tag:=0;
  end;

  fmSpec.ViewSpec.Tag:=0; //�������������� ������� ��� ���������� �� ��������� �� ����


//  dmC.quCurSpec.Filtered:=False;
  cxCheckBox1.Checked:=False; //������ �� ������ ��������
  cxImageComboBox1.Tag:=0;
  cxImageComboBox1.ItemIndex:=0;
  

{  if fmSpec.Tag=1 then
  begin
    delay(200);
    if Tab.iStatus=0 then
    begin
      prWriteLog('---Menu;'+IntToStr(Tab.Id)+';'+Tab.NumTable+';'+Person.Name+';'+IntToStr(Tab.Id_Personal)+';');
      bSaveSpec:=False; //�� �������� ������� ��������
      acMenu.Execute;
      GridSpec.SetFocus;
    end;
  end;}
end;

procedure TfmSpec.cxButton4Click(Sender: TObject);
begin
  if Tab.iStatus>0 then exit;
  if dmC.quCurSpec.RecordCount=0 then exit;
  prWriteLog('---Remove;'+dmC.quCurSpecSifr.AsString+';'+dmC.quCurSpecName.AsString+';');
  acMovePos.Execute;
  GridSpec.SetFocus;
end;

procedure TfmSpec.acPrePrintExecute(Sender: TObject);
Var rSum,rSumD,rSumQ:Real;
    StrWk,StrWk1:String;
    bEdit,b35,bQuest:Boolean;
    iQ,iQuest:Integer;
    StrP:String;
    BonusSum,BonusLim:Real;
    rBalansDay,DLimit:Real;
    CliName,CliType:String;
    sMess:String;
    nd:Smallint;

  function LenStr(S1,S2:String):String;
  begin
    if b35 then Result:=S2 else Result:=S1;
  end;

begin
  if (Tab.iStatus=1)and(CommonSet.PreCheckCount=1) then
  begin
    showmessage('��������� ������ ����� ���������.');
    exit;
  end;

  //���� �� ������� ��� ������� ����� ������ �����
  if ViewSpec.Tag=1 then
  begin
    if prTestRemn(sMess)=False then
    begin
      showmessage(sMess);
      prWriteLog('---��������� �������; '+sMess);
      exit;
    end;
  end;


  Button5.Enabled:=False;
  bPressB12Spec:=True;
  cxButton12.Enabled:=False;
  delay(10);

// �������� ����� � ���������� �������
  bSaveSpec:=False; //�� �������� ������� ��������
  bPrintPre:=True;
  with dmC do
  with dmC1 do
  begin
    if cxCheckBox1.Checked then cxCheckBox1.Checked:=False;
    quCurSpec.Filtered:=False;
    delay(10);

    ViewSpec.BeginUpdate;
    ViewMod.BeginUpdate;
    try
      rSum:=0;
      rSumD:=0;
      bEdit:=False;

      quCurSpec.FullRefresh;

      bQuest:=False; iQuest:=1; //����� ������ �������� �� ����� ������
      quCurSpec.First;
      if quCurSpec.RecordCount>0 then iQuest:=quCurSpecQUEST.AsInteger;
      while not quCurSpec.Eof do
      begin
        rSum:=rSum+quCurSpecSumma.AsFloat;
        rSumD:=rSumD+quCurSpecDSum.AsFloat;
        if quCurSpecIStatus.AsInteger=0 then bEdit:=True;
        if iQuest<>quCurSpecQUEST.AsInteger then bQuest:=True;

        quCurSpec.Next;
      end;

      if rSum<>Tab.Summa then bEdit:=True;
      if quSaleTID.AsInteger<>fmSpec.cxButton17.Tag then bEdit:=True;

      bAbort:=False;

      if bEdit then //���� ��������� ������������
      begin
        if TestBL(sMess) then
        begin
          Tab.iStatus:=1; //���� � ��������� ��������
          Tab.Summa:=rSum;
          cxImageComboBox1.Tag:=0;
          //�������� ������ ��������� � ������ ����� ���� ����
          SaveStatusTab(Tab.Id,Tab.iStatus,sEdit1.Value,quSaleTID.AsInteger,BEdit1.Text,Tab.Summa,Tab.DBar);

          SaveSpecN;
        end
        else
        begin
          bAbort:=True;
          fmAttention.Label1.Caption:=sMess;
          prWriteLog('~~AttentionBLShow;'+sMess);
          fmAttention.ShowModal;
        end;
      end else
      begin //���� ���� ��������� �� ����� - ����� ��������� ������ - ��.
        Tab.iStatus:=1; //���� � ��������� ��������
        SaveStatusTab(Tab.Id,Tab.iStatus,Tab.Quests,Tab.SaleT,Tab.NumTable,Tab.Summa,Tab.DBar); //��� ������ ��������� ��� ��� ��������
      end;
// Procedure TfmSpec.SaveStatusTab(TabId,TabiStatus,iQuests,SaleT:Integer; sNumTab:String;rSum:Real;sDisc:String);


      if not bAbort then
      begin

      SetStatus(StrWk); //���������� �� ������
      Label11.Caption:=StrWk;

      inc(CommonSet.PreCheckNum);
      WriteCheckNum;

      prWriteLog('--PrePrintS;'+IntToStr(Tab.Id)+';'+Tab.NumTable+';'+Person.Name+';'+IntToStr(Tab.Id_Personal)+';'+FloatToStr(Tab.Summa)+';'+INtToStr(Tab.iNumZ)+';'+Tab.DBar);
      FindDiscount(Tab.DBar); //��� ������������ Tab.DBar, Tab.DPercent, Tab.DName

    //������ �������� �� ��
      if pos('fis',CommonSet.PrePrintPort)>0 then
      begin
        if CommonSet.PrePrintPort='fisprim' then
        begin
          TestPrint:=1;
          While PrintQu and (TestPrint<=MaxDelayPrint) do
          begin
//        showmessage('������� �����, ��������� �������.');
            prWriteLog('������������� ������ ����� � �������.');
            delay(1000);
            inc(TestPrint);
          end;
          try
            PrintFCheck:=True; //��� ���� � ������� ������� ��������� ������
            OpenNFDoc;
            SelectF(13); PrintNFStr(' '+CommonSet.DepartName);

            if CommonSet.PreLine>'' then
            begin
              if fDifStr(1,CommonSet.PreLine)>'' then PrintNFStr(fDifStr(1,CommonSet.PreLine));
              if fDifStr(2,CommonSet.PreLine)>'' then PrintNFStr(fDifStr(2,CommonSet.PreLine));
            end;

            PrintNFStr('');

            SelectF(14); PrintNFStr('    ���� �'+IntToStr(Tab.iNumZ));
            SelectF(13); PrintNFStr(''); PrintNFStr('');
            PrintNFStr('��������: '+Tab.Name); PrintNFStr('����: '+Tab.NumTable);
            SelectF(3); PrintNFStr('������: '+IntToStr(Tab.Quests));

            if Tab.DBar>'' then
            begin
              Str(Tab.DPercent:5:2,StrWk);
              PrintNFStr('�����: '+Tab.DName+' ('+StrWk+'%)');
            end;
            PrintNFStr('������: '+IntToStr(Tab.Quests));
            PrintNFStr('������: '+FormatDateTime('dd.mm.yyyy hh:nn:ss',Tab.OpenTime));

            SelectF(3); StrWk:='                                                       ';
            PrintNFStr(StrWk);
            SelectF(15); StrWk:=' ��������          ���-��   ����   ����� ';
            PrintNFStr(StrWk);
            SelectF(3); StrWk:='                                                       ';
            PrintNFStr(StrWk);

            quCurModAll.Active:=False;
            quCurModAll.ParamByName('IDT').AsInteger:=Tab.Id;
            quCurModAll.Active:=True;


            rSumQ:=0;
            quCurSpec.First;
            if quCurSpec.RecordCount>0 then iQuest:=quCurSpecQUEST.AsInteger;
            while not quCurSpec.Eof do
            begin
              if iQuest<>quCurSpecQUEST.AsInteger then   //�������� �����
              begin
                SelectF(15); StrWk:=' '; PrintNFStr(StrWk);
                SelectF(3); StrWk:='  ����� �� ����� '+its(iQuest)+'   '+vts(rSumQ); PrintNFStr(StrWk);
                SelectF(15); StrWk:=' '; PrintNFStr(StrWk);

                iQuest:=quCurSpecQUEST.AsInteger;
                rSumQ:=0;
              end;
              rSumQ:=rSumQ+quCurSpecSUMMA.AsFloat;

              StrWk:= Copy(quCurSpecName.AsString,1,29);
              while Length(StrWk)<29 do StrWk:=StrWk+' ';

              if abs(frac(quCurSpecQuantity.AsFloat*100))>0 then nd:=3  //���� ������� ����� �� �������
              else
                if abs(frac(quCurSpecQuantity.AsFloat*10))>0 then nd:=2  //���� ������� ����� �������� �� �������
                else nd:=1;

              Str(quCurSpecQuantity.AsFloat:5:nd,StrWk1);
              StrWk:=StrWk+' '+StrWk1;
              Str(quCurSpecPrice.AsFloat:7:2,StrWk1);
              StrWk:=StrWk+' '+StrWk1;
              Str(quCurSpecSumma.AsFloat:8:2,StrWk1);
              StrWk:=StrWk+' '+StrWk1+'���';
              SelectF(3);
              PrintNFStr(StrWk);

              while (quCurModAllID_POS.AsInteger<quCurSpecID.AsInteger) and (quCurModAll.Eof=False) do quCurModAll.Next;
              while (quCurModAllID_POS.AsInteger=quCurSpecID.AsInteger) and (quCurModAll.Eof=False) do
              begin
                if quCurModSIFR.AsInteger>0 then   //� ��������� ������ 0
                begin
                  StrWk:=Copy(quCurModAllNAME.AsString,1,29);
                  StrWk:='   '+StrWk;
                  SelectF(0);
                  PrintNFStr(StrWk);
                end;
                quCurModAll.Next;
              end;

              quCurSpec.Next;
            end;
            if bQuest then
            begin
              SelectF(15); StrWk:=' '; PrintNFStr(StrWk);
              SelectF(3); StrWk:='  ����� �� ����� '+its(iQuest)+'   '+vts(rSumQ); PrintNFStr(StrWk);
              SelectF(15); StrWk:=' '; PrintNFStr(StrWk);
            end;

            SelectF(3); StrWk:='                                                       ';
            PrintNFStr(StrWk);
            SelectF(10); PrintNFStr(' ');
            Str(rSum:8:2,StrWk1);
            StrWk:=' �����                      '+StrWk1+' ���';
            SelectF(15); PrintNFStr(StrWk);

            if rSumD>0.02 then
            begin
              SelectF(3);
              StrWk:='                                                       ';
              PrintNFStr(StrWk);

              Str(rSumD:8:2,StrWk);
//              rSumD:=RoundEx((rSumD/(rSum+rSumD)*100)*100)/100;
//              Str(rSumD:5:2,StrWk1);
              Str(Tab.DPercent:4:1,StrWk1);
              StrWk:=' � �.�. ������ - '+StrWk1+'%  '+StrWk+'�.';

              SelectF(10); PrintNFStr(StrWk);
            end;

        if Tab.DType=3 then
        begin
          BonusSum:=rv(CalcBonus(Tab.Id,Tab.DPercent));
          prFindPCardBalans(Tab.DBar,BonusLim,rBalansDay,DLimit,CliName,CliType);

          StrWk:=CliName; PrintNFStr(StrWk);
          StrWk:='����������� ������ - '+FlToS(rv(BonusLim))+' ���'; PrintNFStr(StrWk);

          Str(Tab.DPercent:4:1,StrWk);
          StrWk:='�������� ������ - ('+StrWk+'%)';  PrintNFStr(StrWk);
          StrWk:='����� ��������� - '+FlToS(BonusSum)+' ���' ;  PrintNFStr(StrWk);
        end;

            if CommonSet.LastLine>'' then
            begin
              PrintNFStr(' ');
              if fDifStr(1,CommonSet.LastLine)>'' then PrintNFStr(fDifStr(1,CommonSet.LastLine));
              if fDifStr(2,CommonSet.LastLine)>'' then PrintNFStr(fDifStr(2,CommonSet.LastLine));
            end;

            PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');
            CloseNFDoc;
            if CommonSet.TypeFis<>'sp101' then CutDoc;
//            CutDoc;
          finally
            PrintFCheck:=False; //��� ���� � ������� ������� ��������� ������
          end;
        end;
        if CommonSet.PrePrintPort='fisshtrih' then
        begin
          TestPrint:=1;
          While PrintQu and (TestPrint<=MaxDelayPrint) do
          begin
//        showmessage('������� �����, ��������� �������.');
            prWriteLog('������������� ������ ����� � �������.');
            delay(1000);
            inc(TestPrint);
          end;
          try
            PrintFCheck:=True; //��� ���� � ������� ������� ��������� ������

            PrintNFStr(' '+CommonSet.DepartName);

            if CommonSet.PreLine>'' then
            begin
              if fDifStr(1,CommonSet.PreLine)>'' then PrintNFStr(fDifStr(1,CommonSet.PreLine));
              if fDifStr(2,CommonSet.PreLine)>'' then PrintNFStr(fDifStr(2,CommonSet.PreLine));
            end;

            PrintNFStr('');

            PrintNFStr('        ���� �'+IntToStr(Tab.iNumZ));
            PrintNFStr(' ');
            PrintNFStr('��������: '+Tab.Name);
            PrintNFStr('����: '+Tab.NumTable);
            if Tab.DBar>'' then
            begin
              Str(Tab.DPercent:5:2,StrWk);
              PrintNFStr('�����: '+Tab.DName+' ('+StrWk+'%)');
            end;
            PrintNFStr('������: '+IntToStr(Tab.Quests));
            PrintNFStr('������: '+FormatDateTime('dd.mm.yyyy hh:nn:ss',Tab.OpenTime));

            StrWk:='-----------------------------------';
            PrintNFStr(StrWk);
            StrWk:=' ��������     ���-��  ����   ����� ';
            PrintNFStr(StrWk);
            StrWk:='-----------------------------------';
            PrintNFStr(StrWk);

            quCurModAll.Active:=False;
            quCurModAll.ParamByName('IDT').AsInteger:=Tab.Id;
            quCurModAll.Active:=True;

            rSumQ:=0;
            quCurSpec.First;
            if quCurSpec.RecordCount>0 then iQuest:=quCurSpecQUEST.AsInteger;
            while not quCurSpec.Eof do
            begin
              if iQuest<>quCurSpecQUEST.AsInteger then   //�������� �����
              begin
                StrWk:=' '; PrintNFStr(StrWk);
                StrWk:='  ����� �� ����� '+its(iQuest)+'   '+vts(rSumQ); PrintNFStr(StrWk);
                StrWk:=' '; PrintNFStr(StrWk);

                iQuest:=quCurSpecQUEST.AsInteger;
                rSumQ:=0;
              end;
              rSumQ:=rSumQ+quCurSpecSUMMA.AsFloat;

              StrWk:= Copy(quCurSpecName.AsString,1,36);
              while Length(StrWk)<36 do StrWk:=StrWk+' ';
              PrintNFStr(StrWk); //������� ����� �������� - �������� �������



              if abs(frac(quCurSpecQuantity.AsFloat*100))>0 then nd:=3  //���� ������� ����� �� �������
              else
                if abs(frac(quCurSpecQuantity.AsFloat*10))>0 then nd:=2  //���� ������� ����� �������� �� �������
                else nd:=1;

              Str(quCurSpecQuantity.AsFloat:5:nd,StrWk1);
              StrWk:='          '+StrWk1;
              Str(quCurSpecPrice.AsFloat:7:2,StrWk1);
              StrWk:=StrWk+' '+StrWk1;
              Str(quCurSpecSumma.AsFloat:8:2,StrWk1);
              StrWk:=StrWk+' '+StrWk1+'���';
              PrintNFStr(StrWk);

              while (quCurModAllID_POS.AsInteger<quCurSpecID.AsInteger) and (quCurModAll.Eof=False) do quCurModAll.Next;
              while (quCurModAllID_POS.AsInteger=quCurSpecID.AsInteger) and (quCurModAll.Eof=False) do
              begin
                if quCurModSIFR.AsInteger>0 then
                begin
                  StrWk:=Copy(quCurModAllNAME.AsString,1,30);
                  StrWk:='     '+StrWk;
                  PrintNFStr(StrWk);
                end;
                quCurModAll.Next;
              end;

              quCurSpec.Next;
            end;
            if bQuest then
            begin
              StrWk:=' '; PrintNFStr(StrWk);
              StrWk:='  ����� �� ����� '+its(iQuest)+'   '+vts(rSumQ); PrintNFStr(StrWk);
              StrWk:=' '; PrintNFStr(StrWk);
            end;

            StrWk:='-----------------------------------';
            PrintNFStr(StrWk);

            Str(rSum:8:2,StrWk1);
            StrWk:=' �����                 '+StrWk1+' ���';
            PrintNFStr(StrWk);

            if rSumD>0.02 then
            begin
              StrWk:='-----------------------------------';
              PrintNFStr(StrWk);

              Str(rSumD:8:2,StrWk);
//              rSumD:=RoundEx((rSumD/(rSum+rSumD)*100)*100)/100;
//              Str(rSumD:5:2,StrWk1);
              Str(Tab.DPercent:4:1,StrWk1);

              StrWk:=' � �.�. ������ - '+StrWk1+'%  '+StrWk+'�.';
              PrintNFStr(StrWk);
            end;

        if Tab.DType=3 then
        begin
          BonusSum:=rv(CalcBonus(Tab.Id,Tab.DPercent));
          prFindPCardBalans(Tab.DBar,BonusLim,rBalansDay,DLimit,CliName,CliType);

          StrWk:=CliName; PrintNFStr(StrWk);
          StrWk:='����������� ������ - '+FlToS(rv(BonusLim))+' ���'; PrintNFStr(StrWk);

          Str(Tab.DPercent:4:1,StrWk);
          StrWk:='�������� ������ - ('+StrWk+'%)';  PrintNFStr(StrWk);
          StrWk:='����� ��������� - '+FlToS(BonusSum)+' ���' ;  PrintNFStr(StrWk);
        end;

            if CommonSet.LastLine>'' then
            begin
              PrintNFStr(' ');
              if fDifStr(1,CommonSet.LastLine)>'' then PrintNFStr(fDifStr(1,CommonSet.LastLine));
              if fDifStr(2,CommonSet.LastLine)>'' then PrintNFStr(fDifStr(2,CommonSet.LastLine));
            end;

            PrintNFStr('');PrintNFStr('');PrintNFStr('');PrintNFStr('');PrintNFStr('');PrintNFStr('');PrintNFStr('');
            CutDoc;
          finally
            PrintFCheck:=False; //��� ���� � ������� ������� ��������� ������
          end;
        end;
      end
      else
      if (CommonSet.PrePrintPort<>'0')and(CommonSet.PrePrintPort<>'G')and(pos('DB',CommonSet.PrePrintPort)=0) then
      begin
        try
          prDevOpen(CommonSet.PrePrintPort,0);

          StrP:=CommonSet.PrePrintPort;
          BufPr.iC:=0;

          if CommonSet.PrePrintPort[1]='Z' then
          begin
            b35:=True;
            if not fTestPaper then fmWarnPaper.ShowModal;
          end
          else b35:=False;

          prSetFont(StrP,13,0); PrintStr(StrP,' '+CommonSet.DepartName);
          if CommonSet.PreLine>'' then
          begin
            if fDifStr(1,CommonSet.PreLine)>'' then PrintStr(StrP,fDifStr(1,CommonSet.PreLine));
            if fDifStr(2,CommonSet.PreLine)>'' then PrintStr(StrP,fDifStr(2,CommonSet.PreLine));
          end;
          PrintStr(StrP,LenStr(' ','                                   '));
          prSetFont(StrP,14,0); PrintStr(StrP,'      ���� �'+IntToStr(Tab.iNumZ));
          prSetFont(StrP,13,0); PrintStr(StrP,LenStr(' ','                                   '));
          PrintStr(StrP,'��������: '+Tab.Name);
          PrintStr(StrP,'����: '+Tab.NumTable);
          if Tab.DBar>'' then
          begin
            Str(Tab.DPercent:5:2,StrWk);
            PrintStr(StrP,'�����: '+Tab.DName+' ('+StrWk+'%)');
          end;
          prSetFont(StrP,13,0); PrintStr(StrP,'������: '+IntToStr(Tab.Quests));
          PrintStr(StrP,'������: '+FormatDateTime('dd.mm.yyyy hh:nn:ss',Tab.OpenTime));

          prSetFont(StrP,13,0); PrintStr(StrP,LenStr(' ','                                   '));
          prSetFont(StrP,15,0); StrWk:=LenStr(' ��������      ���-��   ����   �����',' ��������    ���-��   ����   �����'); PrintStr(StrP,StrWk);
          prSetFont(StrP,13,0); StrWk:=LenStr('                                          ','                                   '); PrintStr(StrP,StrWk);

          quCurModAll.Active:=False;
          quCurModAll.ParamByName('IDT').AsInteger:=Tab.Id;
          quCurModAll.Active:=True;


          rSumQ:=0;
          quCurSpec.First;
          if quCurSpec.RecordCount>0 then iQuest:=quCurSpecQUEST.AsInteger;
          while not quCurSpec.Eof do
          begin
            if iQuest<>quCurSpecQUEST.AsInteger then   //�������� �����
            begin
              prSetFont(StrP,15,0); StrWk:=' '; PrintStr(StrP,StrWk);
              prSetFont(StrP,13,0); StrWk:='  ����� �� ����� '+its(iQuest)+'   '+vts(rSumQ); PrintStr(StrP,StrWk);
              prSetFont(StrP,15,0); StrWk:=' '; PrintStr(StrP,StrWk);

              iQuest:=quCurSpecQUEST.AsInteger;
              rSumQ:=0;
            end;
            rSumQ:=rSumQ+quCurSpecSUMMA.AsFloat;

            if not b35 then
            begin
              StrWk:= Copy(quCurSpecName.AsString,1,19);
              while Length(StrWk)<19 do StrWk:=StrWk+' ';
            end else
            begin
              StrWk:= Copy(quCurSpecName.AsString,1,15);
              while Length(StrWk)<15 do StrWk:=StrWk+' ';
            end;

            if abs(frac(quCurSpecQuantity.AsFloat*100))>0 then nd:=3  //���� ������� ����� �� �������
            else
              if abs(frac(quCurSpecQuantity.AsFloat*10))>0 then nd:=2  //���� ������� ����� �������� �� �������
              else nd:=1;

            Str(quCurSpecQuantity.AsFloat:5:nd,StrWk1);
            StrWk:=StrWk+' '+StrWk1;

            if not b35 then Str(quCurSpecPrice.AsFloat:7:2,StrWk1)
            else Str(quCurSpecPrice.AsFloat:4:0,StrWk1);
            StrWk:=StrWk+' '+StrWk1;

            Str((quCurSpecQuantity.AsFloat*quCurSpecPrice.AsFloat):8:2,StrWk1);
            StrWk:=StrWk+StrWk1;
            prSetFont(StrP,15,0);
            PrintStr(StrP,StrWk);

            while (quCurModAllID_POS.AsInteger<quCurSpecID.AsInteger) and (quCurModAll.Eof=False) do quCurModAll.Next;
            while (quCurModAllID_POS.AsInteger=quCurSpecID.AsInteger) and (quCurModAll.Eof=False) do
            begin
              if quCurModSIFR.AsInteger>0 then
              begin
                StrWk:=Copy(quCurModAllNAME.AsString,1,29);
                StrWk:='   '+StrWk;
                prSetFont(StrP,10,0);
                PrintStr(StrP,StrWk);
              end;
              quCurModAll.Next;
            end;

            quCurSpec.Next;
          end;
          if bQuest then
          begin
            prSetFont(StrP,15,0); StrWk:=' '; PrintStr(StrP,StrWk);
            prSetFont(StrP,13,0); StrWk:='  ����� �� ����� '+its(iQuest)+'   '+vts(rSumQ); PrintStr(StrP,StrWk);
            prSetFont(StrP,15,0); StrWk:=' '; PrintStr(StrP,StrWk);
          end;

          prSetFont(StrP,13,0); StrWk:=LenStr('                                          ','                                   '); PrintStr(StrP,StrWk);

          prSetFont(StrP,15,0);
          Str((rSum+rSumD):8:2,StrWk1);
          StrWk:=LenStr(' �����                      '+StrWk1+' ���',' �����                 '+StrWk1+' ���');  PrintStr(StrP,StrWk);

          if rSumD>0.02 then
          begin
            Str(((-1)*rSumD):8:2,StrWk);
//            rSumD:=RoundEx((rSumD/(rSum+rSumD)*100)*100)/100;
//            Str(rSumD:5:2,StrWk1);
            Str(Tab.DPercent:4:1,StrWk1);

//            StrWk:=LenStr(' ������                     '+StrWk+' ���',' ������                '+StrWk+' ���'); PrintStr(StrP,StrWk);
            StrWk:=LenStr(' ������    '+StrWk1+'%            '+StrWk+' ���',' ������    '+StrWk1+'%       '+StrWk+' ���'); PrintStr(StrP,StrWk);
          end;

        if Tab.DType=3 then
        begin
          BonusSum:=rv(CalcBonus(Tab.Id,Tab.DPercent));
          prFindPCardBalans(Tab.DBar,BonusLim,rBalansDay,DLimit,CliName,CliType);

          StrWk:=CliName; PrintStr(StrP,StrWk);
          StrWk:='����������� ������ - '+FlToS(rv(BonusLim))+' ���'; PrintStr(StrP,StrWk);

          Str(Tab.DPercent:4:1,StrWk);
          StrWk:='�������� ������ - ('+StrWk+'%)';  PrintStr(StrP,StrWk);
          StrWk:='����� ��������� - '+FlToS(BonusSum)+' ���';  PrintStr(StrP,StrWk);
        end;

          Str(rSum:8:2,StrWk1);
          StrWk:=LenStr(' ����� � ������             '+StrWk1+' ���',' ����� � ������        '+StrWk1+' ���'); PrintStr(StrP,StrWk);

          if CommonSet.LastLine>'' then
          begin
            PrintStr(StrP,'');
            if fDifStr(1,CommonSet.LastLine)>'' then PrintStr(StrP,fDifStr(1,CommonSet.LastLine));
            if fDifStr(2,CommonSet.LastLine)>'' then PrintStr(StrP,fDifStr(2,CommonSet.LastLine));
          end;

          prWrBuf(StrP);
          prCutDoc(StrP,0);
        finally
//        Delay(100);
          if CommonSet.PrePrintPort[1]='Z' then prRelease; //��������� ���.�������
          prDevClose(StrP,0);
        end;
      end;

      if CommonSet.PrePrintPort='G' then
      begin
        quCheck.Active:=False;
        quCheck.ParamByName('IDTAB').Value:=Tab.Id;
        quCheck.Active:=True;

        quCheck.Filtered:=False;
        quCheck.Filter:='ITYPE=0';
        quCheck.Filtered:=True;

        frRepSp.LoadFromFile(CurDir + 'PreCheck1.frf');

        frVariables.Variable['Waiter']:=Tab.Name;
        frVariables.Variable['TabNum']:=Tab.NumTable;
        frVariables.Variable['OpenTime']:=FormatDateTime('dd.mm.yyyy hh:nn:ss',Tab.OpenTime);
        frVariables.Variable['Quests']:=IntToStr(Tab.Quests);
        frVariables.Variable['ZNum']:=IntToStr(Tab.Id);
        frVariables.Variable['PreNum']:=IntToStr(CommonSet.PreCheckNum-1);

        if rSumD>0.2 then
        begin
          str(rSumD:8:2,StrWk);
          frVariables.Variable['Discount']:='� �.�. ������ '+StrWk;
        end else frVariables.Variable['Discount']:='';

        frRepSp.ReportName:='����.';
        frRepSp.PrepareReport;

//      frRepSp.ShowPreparedReport;
        frRepSp.PrintPreparedReportDlg;

        quCheck.Active:=False;
      end;
      if pos('DB',CommonSet.PrePrintPort)>0 then //���� � ����  DB1 ������; DB2 POSIFLEX AURA PP7000; DB3 STAR TSP600
      begin                    //� ������ �������� ����� ������ DB, � � ������ ������� ��� ��� - 1COM1,2COM1,3COM1 - c ���������� �������
        //��������� �������
        with dmC1 do
        begin
          quPrint.Active:=False;
          quPrint.Active:=True;
          iQ:=GetId('PQH'); //����������� ������� �� 1-�

          PrintDBStr(iQ,0,CommonSet.PrePrintPort,' '+CommonSet.DepartName,13,0);
          if CommonSet.PreLine>'' then
          begin
            if fDifStr(1,CommonSet.PreLine)>'' then PrintDBStr(iQ,0,CommonSet.PrePrintPort,fDifStr(1,CommonSet.PreLine),13,0);
            if fDifStr(2,CommonSet.PreLine)>'' then PrintDBStr(iQ,0,CommonSet.PrePrintPort,fDifStr(2,CommonSet.PreLine),13,0);
          end;
          PrintDBStr(iQ,0,CommonSet.PrePrintPort,'',13,0);
          PrintDBStr(iQ,0,CommonSet.PrePrintPort,'    ���� �'+IntToStr(Tab.iNumZ),14,0);
          PrintDBStr(iQ,0,CommonSet.PrePrintPort,'',13,0);
          PrintDBStr(iQ,0,CommonSet.PrePrintPort,'��������: '+Tab.Name,13,0);
          PrintDBStr(iQ,0,CommonSet.PrePrintPort,'����: '+Tab.NumTable,13,0);
          if Tab.DBar>'' then
          begin
            Str(Tab.DPercent:5:2,StrWk);
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,'�����: '+Tab.DName+' ('+StrWk+'%)',13,0);
          end;
          PrintDBStr(iQ,0,CommonSet.PrePrintPort,'������: '+IntToStr(Tab.Quests),13,0);
          PrintDBStr(iQ,0,CommonSet.PrePrintPort,'������: '+FormatDateTime('dd.mm.yyyy hh:nn:ss',Tab.OpenTime),13,0);
          PrintDBStr(iQ,0,CommonSet.PrePrintPort,'-',13,0);
          PrintDBStr(iQ,0,CommonSet.PrePrintPort,' ��������          ���-��   ����   ����� ',15,0);
          PrintDBStr(iQ,0,CommonSet.PrePrintPort,'-',13,0);

          quCurModAll.Active:=False;
          quCurModAll.ParamByName('IDT').AsInteger:=Tab.Id;
          quCurModAll.Active:=True;

          rSumQ:=0;
          quCurSpec.First;
          if quCurSpec.RecordCount>0 then iQuest:=quCurSpecQUEST.AsInteger;
          while not quCurSpec.Eof do
          begin
            if iQuest<>quCurSpecQUEST.AsInteger then   //�������� �����
            begin
              PrintDBStr(iQ,0,CommonSet.PrePrintPort,' ',15,0);
              PrintDBStr(iQ,0,CommonSet.PrePrintPort,'  ����� �� ����� '+its(iQuest)+'   '+vts(rSumQ),13,0);
              PrintDBStr(iQ,0,CommonSet.PrePrintPort,' ',15,0);

              iQuest:=quCurSpecQUEST.AsInteger;
              rSumQ:=0;
            end;
            rSumQ:=rSumQ+quCurSpecSUMMA.AsFloat;


            if pos('DBfis2',CommonSet.PrePrintPort)>0 then
            begin  //�� ���������� �����   - ��� �� 2-� ������ �� 36 �������� - ���� �������� �������
              StrWk:= Copy(quCurSpecName.AsString,1,36);
              while Length(StrWk)<36 do StrWk:=StrWk+' ';
              PrintDBStr(iQ,0,CommonSet.PrePrintPort,StrWk,15,0);

              if abs(frac(quCurSpecQuantity.AsFloat*100))>0 then nd:=3  //���� ������� ����� �� �������
              else
                if abs(frac(quCurSpecQuantity.AsFloat*10))>0 then nd:=2  //���� ������� ����� �������� �� �������
                else nd:=1;

              Str(quCurSpecQuantity.AsFloat:5:nd,StrWk1);
              StrWk:=' '+StrWk1;
              Str(quCurSpecPrice.AsFloat:7:2,StrWk1);
              StrWk:=StrWk+' '+StrWk1;
              Str(quCurSpecSumma.AsFloat:8:2,StrWk1);
              StrWk:=StrWk+' '+StrWk1+'�';
              while Length(StrWk)<36 do StrWk:=' '+StrWk;

              PrintDBStr(iQ,0,CommonSet.PrePrintPort,StrWk,15,0);
            end else
            begin
              StrWk:= Copy(quCurSpecName.AsString,1,19);
              while Length(StrWk)<19 do StrWk:=StrWk+' ';

              if abs(frac(quCurSpecQuantity.AsFloat*100))>0 then nd:=3  //���� ������� ����� �� �������
              else
                if abs(frac(quCurSpecQuantity.AsFloat*10))>0 then nd:=2  //���� ������� ����� �������� �� �������
                else nd:=1;

              Str(quCurSpecQuantity.AsFloat:5:nd,StrWk1);
              StrWk:=StrWk+' '+StrWk1;
              Str(quCurSpecPrice.AsFloat:7:2,StrWk1);
              StrWk:=StrWk+' '+StrWk1;
              Str((quCurSpecQuantity.AsFloat*quCurSpecPrice.AsFloat):8:2,StrWk1);
              StrWk:=StrWk+StrWk1+'�';

              PrintDBStr(iQ,0,CommonSet.PrePrintPort,StrWk,15,0);
            end;

            while (quCurModAllID_POS.AsInteger<quCurSpecID.AsInteger) and (quCurModAll.Eof=False) do quCurModAll.Next;
            while (quCurModAllID_POS.AsInteger=quCurSpecID.AsInteger) and (quCurModAll.Eof=False) do
            begin
              if quCurModSIFR.AsInteger>0 then
              begin
                StrWk:=Copy(quCurModAllNAME.AsString,1,29);
                StrWk:='   '+StrWk;

                PrintDBStr(iQ,0,CommonSet.PrePrintPort,StrWk,10,0);
              end;
              quCurModAll.Next;
            end;

            quCurSpec.Next;
          end;
          if bQuest then
          begin
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,' ',15,0);
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,'  ����� �� ����� '+its(iQuest)+'   '+vts(rSumQ),13,0);
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,' ',15,0);
          end;

          PrintDBStr(iQ,0,CommonSet.PrePrintPort,'-',13,0);

          Str((rSum+rSumD):8:2,StrWk1);
          StrWk:=' �����                      '+StrWk1+' ���';
          PrintDBStr(iQ,0,CommonSet.PrePrintPort,StrWk,15,0);

          if rSumD>0.02 then
          begin
            Str((rSumD*(-1)):8:2,StrWk);
            Str(Tab.DPercent:4:1,StrWk1);

//            StrWk:=' ������                     '+StrWk+' ���';
            StrWk:=LenStr(' ������    '+StrWk1+'%            '+StrWk+' ���',' ������    '+StrWk1+'%       '+StrWk+' ���');

            PrintDBStr(iQ,0,CommonSet.PrePrintPort,StrWk,15,0);
          end;

        if Tab.DType=3 then
        begin
          BonusSum:=rv(CalcBonus(Tab.Id,Tab.DPercent));
          prFindPCardBalans(Tab.DBar,BonusLim,rBalansDay,DLimit,CliName,CliType);

          StrWk:=CliName; PrintDBStr(iQ,0,CommonSet.PrePrintPort,StrWk,15,0);
          StrWk:='����������� ������ - '+FlToS(rv(BonusLim))+' ���'; PrintDBStr(iQ,0,CommonSet.PrePrintPort,StrWk,15,0);;

          Str(Tab.DPercent:4:1,StrWk);
          StrWk:='�������� ������ - ('+StrWk+'%)';  PrintDBStr(iQ,0,CommonSet.PrePrintPort,StrWk,15,0);
          StrWk:='����� ��������� - '+FlToS(BonusSum)+' ���' ;  PrintDBStr(iQ,0,CommonSet.PrePrintPort,StrWk,15,0);
        end;

          Str((rSum):8:2,StrWk1);
          StrWk:=' ����� � ������             '+StrWk1+' ���';
          PrintDBStr(iQ,0,CommonSet.PrePrintPort,StrWk,15,0);

          if CommonSet.LastLine>'' then
          begin
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,'',15,0);
            if fDifStr(1,CommonSet.LastLine)>'' then PrintDBStr(iQ,0,CommonSet.PrePrintPort,fDifStr(1,CommonSet.LastLine),15,0);
            if fDifStr(2,CommonSet.LastLine)>'' then PrintDBStr(iQ,0,CommonSet.PrePrintPort,fDifStr(2,CommonSet.LastLine),15,0);
          end;

          prAddPrintQH(0,iQ,FormatDateTime('dd.mm hh:nn:sss',now)+', ������� - '+IntToStr(CommonSet.Station)+','+CommonSet.PrePrintPort);
          quPrint.Active:=False;
        end;
      end;

      end; //bAbort
      quCurSpec.First;
    finally
      ViewSpec.EndUpdate;
      ViewMod.EndUpdate;
    end;
//    delay(100);

    GridSpec.SetFocus;

    CreateViewPers1; //��� ������ ��������� ���� ����������� ����������
//  quTabs.Refresh;

    bPrintPre:=False;
//    Action10.Enabled:=True;

//    bCloseSpec:=True;

    if bCloseSpec then
    begin
      bCloseSpec:=False;
      prWriteLog('---ExitFromPrePrint;'+IntToStr(Tab.Id)+';'+Tab.NumTable+';'+Person.Name+';'+IntToStr(Tab.Id_Personal)+';');;
      prWriteLog('');
      prClearCur.ParamByName('ID_TAB').AsInteger:=Tab.Id;
      prClearCur.ParamByName('ID_STATION').AsInteger:=CommonSet.Station;
      prClearCur.ExecProc;
      close;
    end;
  end;
  cxButton12.Enabled:=True;
  bPressB12Spec:=False;
  Button5.Enabled:=True;
  delay(10);
end;

procedure TfmSpec.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var bCh:Byte;
    iShift,iType,Sifr,iLinkM,iLimit,iStream,iWeek:Integer;
    StrWk:String;
    iKey:Integer;
    sName,sCode:String;
    rPrice:Real;
    cSum,cSumD:Real;
    sW,sT,sD:String;
begin
  bCh:=ord(Key);

  if Tab.iStatus=1 then exit; // � �������� �������������� ���������

  if (Chr(bCh) in ['0','1','2','3','4','5','6','7','8','9',';','?']=False)
  and (bCh <> 13) then
  begin  //��� ��
    exit;
  end;

  //121,27,18
  iShift:=0;
  if Shift=[ssShift]  then iShift:=1;
  if Shift=[ssCtrl]   then iShift:=2;
  if Shift=[ssAlt]  then iShift:=3;

  iKey:=bCh+1000*iShift;

  with dmC do
  begin
    if not CommonSet.bTouch then //���� �� ��� �� ����� ����� ��������� �� ������� �������
    begin
      if prFindKey(iKey,sName,sCode,rPrice,Sifr,iType,iLinkM,iLimit,iStream) then
      begin
        if iType=1 then //�����
        begin
          CalcDiscontN(Sifr,Check.Max+1,-1,iStream,rPrice,1,0,Tab.DBar,Now,Check.DProc,Check.DSum);

          cSum:=RoundEx((rPrice-Check.DSum)*100)/100;
          cSumD:=RoundEx((rPrice)*100)/100-cSum;

          quCurSpec.Append;
          quCurSpecSTATION.AsInteger:=CommonSet.Station;
          quCurSpecID_TAB.AsInteger:=Tab.Id;
          quCurSpecId_Personal.AsInteger:=Person.Id;
          quCurSpecNumTable.AsString:=Tab.NumTable;
          quCurSpecId.AsInteger:=Check.Max+1;
          quCurSpecSifr.AsInteger:=Sifr;
          quCurSpecPrice.AsFloat:=rPrice;
          quCurSpecQuantity.AsFloat:=1;
          quCurSpecSumma.AsFloat:=cSum;
          quCurSpecDProc.AsFloat:=Check.DProc;
          quCurSpecDSum.AsFloat:=cSumD;
          quCurSpecIStatus.AsInteger:=0;
          quCurSpecName.AsString:=sName;
          quCurSpecCode.AsString:=sCode;
          quCurSpecLinkM.AsInteger:=iLinkM;
          if iLimit>0 then quCurSpecLimitM.AsInteger:=iLimit
          else quCurSpecLimitM.AsInteger:=99;
          quCurSpecStream.AsInteger:=iStream;
          quCurSpecQUEST.AsInteger:=cxImageComboBox1.EditValue;
          quCurSpec.Post;
          inc(Check.Max);

          if iLinkM>0 then
          begin //������������
            acModify.Execute;
          end;
          GridSpec.SetFocus;
        end;
        if iType=2 then //������ �������
        begin
          prFormMenuList(Sifr);

          fmMenuClass.ShowModal;
          if fmMenuClass.ModalResult=mrOk then
          begin
            fmCalc.CalcEdit1.EditValue:=1;
            fmCalc.ShowModal;
            if (fmCalc.ModalResult=mrOk)and(fmCalc.CalcEdit1.EditValue>0)  then
            begin
              CalcDiscontN(quMenuSIFR.AsInteger,Check.Max+1,Sifr,quMenuSTREAM.AsInteger,quMenuPRICE.AsFloat,fmCalc.CalcEdit1.EditValue,0,Tab.DBar,Now,Check.DProc,Check.DSum);

              cSum:=RoundEx((quMenuPRICE.AsFloat*(RoundEx(fmCalc.CalcEdit1.EditValue*1000)/1000)-Check.DSum)*100)/100;
              cSumD:=RoundEx((quMenuPRICE.AsFloat*(RoundEx(fmCalc.CalcEdit1.EditValue*1000)/1000))*100)/100-cSum;

              quCurSpec.Append;
              quCurSpecSTATION.AsInteger:=CommonSet.Station;
              quCurSpecID_TAB.AsInteger:=Tab.Id;
              quCurSpecId_Personal.AsInteger:=Person.Id;
              quCurSpecNumTable.AsString:=Tab.NumTable;
              quCurSpecId.AsInteger:=Check.Max+1;
              quCurSpecSifr.AsInteger:=quMenuSIFR.AsInteger;
              quCurSpecPrice.AsFloat:=quMenuPRICE.AsFloat;
              quCurSpecQuantity.AsFloat:=RoundEx(fmCalc.CalcEdit1.EditValue*1000)/1000;
              quCurSpecSumma.AsFloat:=cSum;
              quCurSpecDProc.AsFloat:=Check.DProc;
              quCurSpecDSum.AsFloat:=cSumD;
              quCurSpecIStatus.AsInteger:=0;
              quCurSpecName.AsString:=quMenuNAME.AsString;
              quCurSpecCode.AsString:=quMenuCODE.AsString;
              quCurSpecLinkM.AsInteger:=quMenuLINK.AsInteger;
              if quMenuLIMITPRICE.AsFloat>0 then
              quCurSpecLimitM.AsInteger:=RoundEx(quMenuLIMITPRICE.AsFloat)
              else quCurSpecLimitM.AsInteger:=99;
              quCurSpecStream.AsInteger:=quMenuSTREAM.AsInteger;
              quCurSpecQUEST.AsInteger:=cxImageComboBox1.EditValue;
              quCurSpec.Post;
              inc(Check.Max);

              if dmC.quMenuLINK.AsInteger>0 then
              begin //������������
                acModify.Execute;
              end;
              GridSpec.SetFocus;
            end;
          end;
        end;
        DiscountBar:=''
      end; //FindKey - ��� ���� �����������
    end
    else //���, ��, ��
    begin
      if (bCh = 13)and(Length(DiscountBar)>0) then
      begin //���� ����������
//      if (Length(DiscountBar)<=5)or(Length(DiscountBar)=13) then
        if DiscountBar[1]<>';' then //��� �� ������� - �� ������� ��� �������
        begin  //��� ��� ��
           //����� �� ����
          sW:=FormatDateTime('ddd',Date);

          iWeek:=1;
          if Pos('��',sW)>0 then iWeek:=1;
          if Pos('��',sW)>0 then iWeek:=2;
          if Pos('��',sW)>0 then iWeek:=3;
          if Pos('��',sW)>0 then iWeek:=4;
          if Pos('��',sW)>0 then iWeek:=5;
          if Pos('��',sW)>0 then iWeek:=6;
          if Pos('��',sW)>0 then iWeek:=7;

          if Pos('Mon',sW)>0 then iWeek:=1;
          if Pos('Tue',sW)>0 then iWeek:=2;
          if Pos('Wed',sW)>0 then iWeek:=3;
          if Pos('Thu',sW)>0 then iWeek:=4;
          if Pos('Fri',sW)>0 then iWeek:=5;
          if Pos('Sat',sW)>0 then iWeek:=6;
          if Pos('Sun',sW)>0 then iWeek:=7;

          sW:=its(iWeek);

          iWeek:=Trunc(date);
          sD:=its(iWeek);

          sT:=FormatDateTime('hh:nn',now);

          quMenuSel.Active:=False;
          quMenuSel.SelectSQL.Clear;
          quMenuSel.SelectSQL.Add('SELECT SIFR,NAME,PRICE,CODE,LIMITPRICE,LINK,STREAM,BARCODE,PARENT,PRNREST');
          quMenuSel.SelectSQL.Add('FROM MENU');
//          if Length(DiscountBar)=4 then
          quMenuSel.SelectSQL.Add('WHERE SIFR='''+DiscountBar+'''');
//          if Length(DiscountBar)=13 then
          quMenuSel.SelectSQL.Add('or BARCODE='''+DiscountBar+'''');
          quMenuSel.SelectSQL.Add('and TREETYPE=''F''');
          quMenuSel.SelectSQL.Add('and IACTIVE=1');
          quMenuSel.SelectSQL.Add('and ((ALLTIME=1)');
          quMenuSel.SelectSQL.Add('or ((ALLTIME=0)and(DAYWEEK like ''%'+sW+'%'')and(DATEB<='+sD+')and(DATEE>='+sD+')and(TIMEB<='''+sT+''')and(TIMEE>='''+sT+''')))');
          quMenuSel.SelectSQL.Add('and ((PRNREST=0)or(PRNREST=1 and BACKBGR>=0.01))');

          quMenuSel.Active:=True;

          if (quMenuSel.RecordCount>0) then
          begin
            fmCalc.CalcEdit1.EditValue:=1;
            fmCalc.ShowModal;
            if (fmCalc.ModalResult=mrOk)and(fmCalc.CalcEdit1.EditValue>0)and(quMenuSelPRICE.AsFloat>0)
            then begin
              CalcDiscontN(quMenuSelSIFR.AsInteger,Check.Max+1,-1,quMenuSelSTREAM.AsInteger,quMenuSelPRICE.AsFloat,fmCalc.CalcEdit1.EditValue,0,Tab.DBar,Now,Check.DProc,Check.DSum);

              cSum:=RoundEx((quMenuSelPRICE.AsFloat*fmCalc.CalcEdit1.EditValue-Check.DSum)*100)/100;
              cSumD:=RoundEx((quMenuSelPRICE.AsFloat*fmCalc.CalcEdit1.EditValue)*100)/100-cSum;

              quCurSpec.Append;
              quCurSpecSTATION.AsInteger:=CommonSet.Station;
              quCurSpecID_TAB.AsInteger:=Tab.Id;
              quCurSpecId_Personal.AsInteger:=Person.Id;
              quCurSpecNumTable.AsString:=Tab.NumTable;
              quCurSpecId.AsInteger:=Check.Max+1;
              quCurSpecSifr.AsInteger:=quMenuSelSIFR.AsInteger;
              quCurSpecPrice.AsFloat:=quMenuSelPRICE.AsFloat;
              quCurSpecQuantity.AsFloat:=fmCalc.CalcEdit1.EditValue;
              quCurSpecSumma.AsFloat:=cSum;
              quCurSpecDProc.AsFloat:=Check.DProc;
              quCurSpecDSum.AsFloat:=cSumD;

              quCurSpecIStatus.AsInteger:=0;
              quCurSpecName.AsString:=quMenuSelNAME.AsString;
              quCurSpecCode.AsString:=quMenuSelCODE.AsString;
              quCurSpecLinkM.AsInteger:=quMenuSelLINK.AsInteger;
              if quMenuSelLIMITPRICE.AsFloat>0 then
              quCurSpecLimitM.AsInteger:=RoundEx(quMenuSelLIMITPRICE.AsFloat)
              else quCurSpecLimitM.AsInteger:=99;
              quCurSpecStream.AsInteger:=quMenuSelSTREAM.AsInteger;
              quCurSpecQUEST.AsInteger:=cxImageComboBox1.EditValue;
              quCurSpec.Post;

              inc(Check.Max);

              if quMenuSelLINK.AsInteger>0 then
              begin //������������
                acModify.Execute;
              end;

              if quMenuSelPRNREST.AsInteger=1 then ViewSpec.Tag:=1;

              GridSpec.SetFocus;
            end;
          end
          else Label18.Caption:='����� �� K��� (��) �� �������.';
        end
        else  //�������
        begin
          If FindDiscount(DiscountBar) then
          begin
            if Tab.DType<>3 then
            begin
              // ��������� ������
              Str(Tab.DPercent:5:2,StrWk);
              Label17.Caption:='������������ ������ - '+ Tab.DName+' ('+StrWk+'%)';
              //������ ������ �� ��� �������
              ViewSpec.BeginUpdate;
              try
                quCurSpec.First;
                While not quCurSpec.Eof do
                begin
                  CalcDiscontN(quCurSpecSifr.AsInteger,quCurSpecId.AsInteger,-1,quCurSpecSTREAM.AsInteger,quCurSpecPrice.AsFloat,quCurSpecQuantity.AsFloat,0,Tab.DBar,Now,Check.DProc,Check.DSum);
                  if Check.DSum<>0 then
                  begin

                    cSum:=RoundEx((quCurSpecPrice.AsFloat*quCurSpecQuantity.AsFloat-Check.DSum)*100)/100;
                    cSumD:=RoundEx((quCurSpecPrice.AsFloat*quCurSpecQuantity.AsFloat)*100)/100-cSum;

                    quCurSpec.Edit;
                    quCurSpecSumma.AsFloat:=cSum;
                    quCurSpecDProc.AsFloat:=Check.DProc;
                    quCurSpecDSum.AsFloat:=cSumD;
                    quCurSpec.Post;

                    ViewSpecDPROC.Visible:=True;
                    ViewSpecDSum.Visible:=True;
                  end;
                  quCurSpec.Next;
                end;
                quCurSpec.First;
              finally
                ViewSpec.EndUpdate;
              end;
            end else
            begin
              Str(Tab.DPercent:5:2,StrWk);
              Label17.Caption:='�������� ������ - '+ Tab.DName+' ('+StrWk+'%)';
              FormLog('DiscCard',IntToStr(Person.Id)+' '+quTabsNUMZ.AsString+' '+StrWk+' '+DiscountBar);

              Check.DSum:=0;

//������ ������ �� ��� �������
              ViewSpec.BeginUpdate;
              try
                quCurSpec.First;
                While not quCurSpec.Eof do
                begin
                  cSum:=rv(quCurSpecPrice.AsFloat*quCurSpecQuantity.AsFloat);

                  quCurSpec.Edit;
                  quCurSpecSumma.AsFloat:=cSum;
                  quCurSpecDProc.AsFloat:=0;
                  quCurSpecDSum.AsFloat:=0;
                  quCurSpec.Post;

                  ViewSpecDPROC.Visible:=False;
                  ViewSpecDSum.Visible:=False;
                  quCurSpec.Next;
                end;
                quCurSpec.First;
                prDiscHist(Tab.OpenTime,Tab.iNumZ,Tab.DPercent,0);
              finally
                ViewSpec.EndUpdate;
              end;
            end;
          end;
        end;
        DiscountBar:='';
        Edit1.Text:='';
        GridSpec.SetFocus;
      end
      else //�����
        DiscountBar:=DiscountBar+Chr(bCh);
    end;
  end;
end;

procedure TfmSpec.acModifyExecute(Sender: TObject);
begin
  //������������
  with dmC do
  begin
    quModif.Active:=False;
    quModif.ParamByName('PARENT').AsInteger:=quCurSpecLINKM.AsInteger;
    quModif.Active:=True;
    fmModif:=TFmModif.Create(Application);
    //������� �����
    MaxMod:=quCurSpecLimitM.AsInteger;
    CountMod:=quCurMod.RecordCount;

    fmModif.Label1.Caption:='�������� � ���������� '+INtToStr(MaxMod-CountMod)+' ������������.';
    fmModif.Caption:='������������ �����.';
    fmModif.LevelMo.Visible:=True;
    fmModif.LevelMe.Visible:=False;

    fmModif.ShowModal;
    fmModif.Release;
    quModif.Active:=False;
  end;
end;

procedure TfmSpec.cxButton5Click(Sender: TObject);
begin
  prWriteLog('---AddMod;');
  acAddMod.Execute;
end;

procedure TfmSpec.cxButton6Click(Sender: TObject);
begin
  prWriteLog('---DelMod;');
  acDelMod.Execute;
end;

procedure TfmSpec.Timer1Timer(Sender: TObject);
begin
  if MessClearSpec then
  begin
    Label18.Caption:='';
    MessClearSpec:=False;
  end;
  if Label18.Caption>'' then
  MessClearSpec:=True;
end;

procedure TfmSpec.Action1Execute(Sender: TObject);
begin
//
end;

procedure TfmSpec.Action2Execute(Sender: TObject);
begin
//
end;

procedure TfmSpec.acExitNExecute(Sender: TObject);
Var rSum:Real;
    bEdit,bEditH:Boolean;
    sMess:String;
//    Id:Integer;
begin
  ViewSpec.BeginUpdate;
  ViewMod.BeginUpdate;
//  dsCheck.DataSet:=nil;
  try
    rSum:=0;
    bEdit:=False;
    bEditH:=False;
    with dmC do
    with dmC1 do
    begin
      quCurSpec.First;
      while not quCurSpec.Eof do
      begin
        rSum:=rSum+quCurSpecSumma.AsFloat;
        if quCurSpecIStatus.AsInteger=0 then bEdit:=True;
        quCurSpec.Next;
      end;

      if abs(rSum-Tab.Summa)>0.001 then bEdit:=True;
      if Tab.DBar<>Tab.DBar1 then bEdit:=True;
      if (Tab.NumTable<>BEdit1.Text)or(Tab.Quests<>SEdit1.EditValue) then bEditH:=True;
      if quSaleTID.AsInteger<>fmSpec.cxButton17.Tag then bEditH:=True;

//      if Tab.iStatus=3 then bEdit:=True; //���� ��������� �� ������ - ����� ���������
      if cxImageComboBox1.Tag=1 then bEdit:=True; //���� ��������� �� ������ - ����� ���������

      if bEdit or bEditH then
      begin
        fmMess:=tfmMess.Create(Application);
        fmMess.ShowModal;
        if fmMess.ModalResult=mrOk then
        begin
          Tab.Summa:=rSum;

          if bEditH then SaveStatusTab(Tab.Id,Tab.iStatus,sEdit1.Value,quSaleTID.AsInteger,BEdit1.Text,rSum,Tab.DBar);

          if bEdit then
          begin
            if TestBL(sMess) then SaveSpecN
            else
            begin
              bAbort:=True;
              fmAttention.Label1.Caption:=sMess;
              prWriteLog('~~AttentionBLShow;'+sMess);
              fmAttention.ShowModal;
            end;

//        fmMainCashRn.CreateViewPers;
//      quTabs.Refresh;
            CreateViewPers1;
          end;
//        Delay(100);
        end else
        begin
          if fmMess.ModalResult=mrAbort then bAbort:=True
          else FormLog('ExitNoSave',IntToStr(Tab.Id_Personal)+' '+IntToStr(Tab.iNumZ));

          fmMainCashRn.TabView.BeginUpdate;
          if quTabs.Locate('ID',Tab.Id,[]) then
          begin
            quTabs.Edit;
            quTabsISTATUS.AsInteger:=Tab.iStatus;
            quTabs.Post;
            quTabs.Refresh;
          end;
          fmMainCashRn.TabView.EndUpdate;

        end;

        fmMess.Release;
      end
      else //� ��������������� ������ ���, �� ������������ ������� ��� ����� ����.
      begin            //� ������ ��������
        with dmC do
        begin

          fmMainCashRn.TabView.BeginUpdate;
          if quTabs.Locate('ID',Tab.Id,[]) then
          begin
            quTabs.Edit;
            quTabsISTATUS.AsInteger:=Tab.iStatus;
            quTabs.Post;
            quTabs.Refresh;
          end;
          fmMainCashRn.TabView.EndUpdate;

          prClearCur.ParamByName('ID_TAB').AsInteger:=Tab.Id;
          prClearCur.ParamByName('ID_STATION').AsInteger:=CommonSet.Station;
          prClearCur.ExecProc;
        end;
      end;
    end;
//  dsCheck.DataSet:=quCurSpec;
  finally
    ViewMod.EndUpdate;
    ViewSpec.EndUpdate;
  end;  
end;

procedure TfmSpec.Action4Execute(Sender: TObject);
begin
//
end;

procedure TfmSpec.Action5Execute(Sender: TObject);
begin
//
end;

procedure TfmSpec.Action6Execute(Sender: TObject);
begin
//
end;

procedure TfmSpec.Action7Execute(Sender: TObject);
begin
//
end;

procedure TfmSpec.Action8Execute(Sender: TObject);
begin
//
end;

procedure TfmSpec.Action9Execute(Sender: TObject);
begin
//
end;

procedure TfmSpec.Action10Execute(Sender: TObject);
Var sMess:String;
begin
  if bPrintPre then
  begin
    prWriteLog('---ExitFlagAfterPrePrint;'+IntToStr(Tab.Id)+';'+Tab.NumTable+';'+Person.Name+';'+IntToStr(Tab.Id_Personal)+';');
    prWriteLog('');
    bCloseSpec:=True;  //����� ������ � ����� ��������� ������ �����
    exit;
  end;

  if bPrintCheck then
  begin //���� ������, � ����� ���� ����� ����� - ������ ���������
//    prWriteLog('---ExitFlagAfterRasch;'+IntToStr(Tab.Id)+';'+Tab.NumTable+';'+Person.Name+';'+IntToStr(Tab.Id_Personal)+';');
//    prWriteLog('');
//    bCloseSpec:=True;  //����� ������ � ����� ��������� ������ �����
    exit;
  end;

  if bMoveSpec then
  begin
    prWriteLog('---ExitBeforeEndSaveMove;'+IntToStr(Tab.Id)+';'+Tab.NumTable+';'+Person.Name+';'+IntToStr(Tab.Id_Personal)+';');
    prWriteLog('');
    exit;
  end;

  //���� �� ������� ��� ������� ����� ������ �����
  if ViewSpec.Tag=1 then
  begin
    if prTestRemn(sMess)=False then
    begin
      showmessage(sMess);
      prWriteLog('---��������� �������; '+sMess);
      exit;
    end;
  end;

  if bSaveSpec then exit; //�� �������� �������
  bSaveSpec:=True;

  prWriteLog('---Exit;'+IntToStr(Tab.Id)+';'+Tab.NumTable+';'+Person.Name+';'+IntToStr(Tab.Id_Personal)+';');;
  prWriteLog('');


  if cxCheckBox1.Checked then cxCheckBox1.Checked:=False;
  if (Tab.iStatus=0) then
  begin
    bAbort:=False; //���������� ��� ������ ������
    acExitN.Execute; //��������   ....  ��� ��� ����  c ������� ������ ����� � �.�.
  end
  else //������� ������� ������������ ��� ����� ����
  begin
    bAbort:=False;
    if CommonSet.KuponButton=1 then
      if CommonSet.KuponCode>0 then  acExitN.Execute;
    with dmC do
    begin
      prClearCur.ParamByName('ID_TAB').AsInteger:=Tab.Id;
      prClearCur.ParamByName('ID_STATION').AsInteger:=CommonSet.Station;
      prClearCur.ExecProc;

      fmMainCashRn.TabView.BeginUpdate;
      if quTabs.Locate('ID',Tab.Id,[]) then
      begin
        quTabs.Edit;
        quTabsISTATUS.AsInteger:=Tab.iStatus;
        quTabs.Post;
        quTabs.Refresh;
      end;
      fmMainCashRn.TabView.EndUpdate;
    end;
  end;
  bSaveSpec:=False;
  if bAbort=False then close;
end;

procedure TfmSpec.acAddModExecute(Sender: TObject);
begin
  with dmC do
  begin
    if quCurSpec.RecordCount=0 then exit;

    if Tab.iStatus>0 then
    begin
      Label18.Caption:='������ ������ �� ��������� ��������������.';
      exit;
    end;
    if quCurSpecIStatus.AsInteger=1 then
    begin
      Label18.Caption:='����� ��������, ��������� ������������ �����������.';
      exit; //������������� ����� ������ ������������� �������
    end;
    if quCurSpecLinkM.AsInteger=0 then
    begin
      Label18.Caption:='����� ��� �������������.';
      exit;
    end;
  end;
  acModify.Execute;
  GridSpec.SetFocus;
end;

procedure TfmSpec.acDelModExecute(Sender: TObject);
begin
  with dmC do
  begin
    if quCurSpec.RecordCount=0 then exit;
    with dmC do
    begin
      if quCurMod.RecordCount>0 then
      begin
        if quCurSpecIStatus.AsInteger=0 then quCurMod.Delete
        else Label18.Caption:='����� ��������, ��������� ������������ �����������.';
      end;
    end;  
  end;
end;

procedure TfmSpec.Button2Click(Sender: TObject);
//Var StrWk:String;
begin
{  DevPrint.DeviceName:='COM1';
  DevPrint.Open;

  prSetFont(StrP,1);
  StrWk:='����� ������ 1';
  PrintStr(StrP,StrWk);

  prSetFont(StrP,2);
  StrWk:='����� ������ 2';
  PrintStr(StrP,StrWk);

  prSetFont(StrP,3);
  StrWk:='����� ������ 3';
  PrintStr(StrP,StrWk);

  prSetFont(StrP,0);
  StrWk:='����� ������   0';
  PrintStr(StrP,StrWk);
  prSetFont(StrP,0);
  StrWk:='����� ������   0';
  PrintStr(StrP,StrWk);
  prSetFont(StrP,0);
  StrWk:='����� ������   0';
  PrintStr(StrP,StrWk);
  prSetFont(StrP,0);
  StrWk:='����� ������   0';
  PrintStr(StrP,StrWk);
  prSetFont(StrP,0);
  StrWk:='����� ������   0';
  PrintStr(StrP,StrWk);
  prSetFont(StrP,0);
  StrWk:='����� ������   0';
  PrintStr(StrP,StrWk);
  prSetFont(StrP,0);
  StrWk:='����� ������   0';
  PrintStr(StrP,StrWk);
  prSetFont(StrP,0);
  StrWk:='����� ������   0';
  PrintStr(StrP,StrWk);
  prSetFont(StrP,0);
  StrWk:='����� ������   0';
  PrintStr(StrP,StrWk);
  prSetFont(StrP,0);
  StrWk:='����� ������   0';
  PrintStr(StrP,StrWk);
  prSetFont(StrP,0);
  StrWk:='����� ������   0';
  PrintStr(StrP,StrWk);
  prSetFont(StrP,0);
  StrWk:='����� ������   0';
  PrintStr(StrP,StrWk);
  prSetFont(StrP,0);
  StrWk:='����� ������   0';
  PrintStr(StrP,StrWk);
  prSetFont(StrP,0);
  StrWk:='����� ������   0';
  PrintStr(StrP,StrWk);
  prSetFont(StrP,0);
  StrWk:='����� ������   0';
  PrintStr(StrP,StrWk);
  prSetFont(StrP,0);
  StrWk:='����� ������   0';
  PrintStr(StrP,StrWk);
  prSetFont(StrP,0);
  StrWk:='����� ������   0';
  PrintStr(StrP,StrWk);
  prSetFont(StrP,0);
  StrWk:='����� ������   0';
  PrintStr(StrP,StrWk);
  prSetFont(StrP,0);
  StrWk:='����� ������   0';
  PrintStr(StrP,StrWk);
  prSetFont(StrP,0);
  StrWk:='����� ������   0';
  PrintStr(StrP,StrWk);
  prSetFont(StrP,0);
  StrWk:='����� ������   0';
  PrintStr(StrP,StrWk);
  StrWk:='����� ������   0';
  PrintStr(StrP,StrWk);
  StrWk:='����� ������   0';
  PrintStr(StrP,StrWk);
  StrWk:='����� ������   0';
  PrintStr(StrP,StrWk);
  StrWk:='����� ������   0';
  PrintStr(StrP,StrWk);
  StrWk:='����� ������   0';
  PrintStr(StrP,StrWk);
  StrWk:='����� ������   0';
  PrintStr(StrP,StrWk);
  StrWk:='����� ������   0';
  PrintStr(StrP,StrWk);
  StrWk:='����� ������   0';
  PrintStr(StrP,StrWk);
  StrWk:='����� ������   0';
  PrintStr(StrP,StrWk);
  StrWk:='����� ������   0';
  PrintStr(StrP,StrWk);
  StrWk:='����� ������   0';
  PrintStr(StrP,StrWk);
  StrWk:='����� ������   0';
  PrintStr(StrP,StrWk);
  StrWk:='����� ������   0';
  PrintStr(StrP,StrWk);
  StrWk:='����� ������   0';
  PrintStr(StrP,StrWk);
  StrWk:='����� ������   0';
  PrintStr(StrP,StrWk);
  StrWk:='����� ������   0';
  PrintStr(StrP,StrWk);
  StrWk:='����� ������   0';
  PrintStr(StrP,StrWk);
  StrWk:='����� ������   0';
  PrintStr(StrP,StrWk);
  StrWk:='����� ������   0';
  PrintStr(StrP,StrWk);
  StrWk:='����� ������   0';
  PrintStr(StrP,StrWk);

  StrWk:='';
  PrintStr(StrP,StrWk);
  StrWk:='';
  PrintStr(StrP,StrWk);
  StrWk:='';
  PrintStr(StrP,StrWk);
  StrWk:='';
  PrintStr(StrP,StrWk);
  StrWk:='';
  PrintStr(StrP,StrWk);
  CutDocPr;

  delay(300);
  DevPrint.Close;}
end;

procedure TfmSpec.acKodExecute(Sender: TObject);
Var sKod:String;
    cSum,cSumD:Real;
    sW,sT,sD:String;
    iWeek:Integer;
begin
//
  fmCalc.CalcEdit1.EditValue:=0;
  fmCalc.ShowModal;
  if (fmCalc.ModalResult=mrOk)and(Trunc(fmCalc.CalcEdit1.EditValue)>0)  then
  begin
    sKod:=IntToStr(Trunc(fmCalc.CalcEdit1.EditValue));
    with dmC do
    begin
      //����� �� ����

      sW:=FormatDateTime('ddd',Date);

      iWeek:=1;
      if Pos('��',sW)>0 then iWeek:=1;
      if Pos('��',sW)>0 then iWeek:=2;
      if Pos('��',sW)>0 then iWeek:=3;
      if Pos('��',sW)>0 then iWeek:=4;
      if Pos('��',sW)>0 then iWeek:=5;
      if Pos('��',sW)>0 then iWeek:=6;
      if Pos('��',sW)>0 then iWeek:=7;

      if Pos('Mon',sW)>0 then iWeek:=1;
      if Pos('Tue',sW)>0 then iWeek:=2;
      if Pos('Wed',sW)>0 then iWeek:=3;
      if Pos('Thu',sW)>0 then iWeek:=4;
      if Pos('Fri',sW)>0 then iWeek:=5;
      if Pos('Sat',sW)>0 then iWeek:=6;
      if Pos('Sun',sW)>0 then iWeek:=7;

      sW:=its(iWeek);

      iWeek:=Trunc(date);
      sD:=its(iWeek);

      sT:=FormatDateTime('hh:nn',now);

      prWriteLog('---Kod;'+IntToStr(Tab.Id)+';'+Tab.NumTable+';'+Person.Name+';'+IntToStr(Tab.Id_Personal)+';'+sKod);

      quMenuSel.Active:=False;
      quMenuSel.SelectSQL.Clear;
      quMenuSel.SelectSQL.Add('SELECT SIFR,NAME,PARENT,PRICE,CODE,LIMITPRICE,LINK,STREAM,BARCODE,PARENT,PRNREST');
      quMenuSel.SelectSQL.Add('FROM MENU');

      if (Length(sKod)<=6) then
      quMenuSel.SelectSQL.Add('WHERE SIFR='+sKod);
      if Length(sKod)=13 then
      quMenuSel.SelectSQL.Add('WHERE BARCODE='''+sKod+'''');
      quMenuSel.SelectSQL.Add('and TREETYPE<>''T''');

      quMenuSel.SelectSQL.Add('and IACTIVE=1');
      quMenuSel.SelectSQL.Add('and ((ALLTIME=1)');
      quMenuSel.SelectSQL.Add('or ((ALLTIME=0)and(DAYWEEK like ''%'+sW+'%'')and(DATEB<='+sD+')and(DATEE>='+sD+')and(TIMEB<='''+sT+''')and(TIMEE>='''+sT+''')))');
      quMenuSel.SelectSQL.Add('and ((ALLTIME=1)');
      quMenuSel.SelectSQL.Add('or ((ALLTIME=0)and(DAYWEEK like ''%'+sW+'%'')and(DATEB<='+sD+')and(DATEE>='+sD+')and(TIMEB<='''+sT+''')and(TIMEE>='''+sT+''')))');
      quMenuSel.SelectSQL.Add('and ((PRNREST=0)or(PRNREST=1 and BACKBGR>=0.01))');

      quMenuSel.Active:=True;

      if quMenuSel.RecordCount>0 then
      begin
        prWriteLog('---KodOk;'+sKod);

        CalcDiscontN(quMenuSelSIFR.AsInteger,Check.Max+1,quMenuSelPARENT.AsInteger,quMenuSelSTREAM.AsInteger,quMenuSelPRICE.AsFloat,1,0,Tab.DBar,Now,Check.DProc,Check.DSum);

        cSum:=RoundEx((quMenuSelPRICE.AsFloat-Check.DSum)*100)/100;
        cSumD:=RoundEx((quMenuSelPRICE.AsFloat)*100)/100-cSum;

        quCurSpec.Append;
        quCurSpecSTATION.AsInteger:=CommonSet.Station;
        quCurSpecID_TAB.AsInteger:=Tab.Id;
        quCurSpecId_Personal.AsInteger:=Person.Id;
        quCurSpecNumTable.AsString:=Tab.NumTable;
        quCurSpecId.AsInteger:=Check.Max+1;
        quCurSpecSifr.AsInteger:=quMenuSelSIFR.AsInteger;
        quCurSpecPrice.AsFloat:=quMenuSelPRICE.AsFloat;
        quCurSpecQuantity.AsFloat:=1;
        quCurSpecSumma.AsFloat:=cSum;
        quCurSpecDProc.AsFloat:=Check.DProc;
        quCurSpecDSum.AsFloat:=cSumD;
        quCurSpecIStatus.AsInteger:=0;
        quCurSpecName.AsString:=quMenuSelNAME.AsString;
        quCurSpecCode.AsString:=quMenuSelCODE.AsString;
        quCurSpecLinkM.AsInteger:=quMenuSelLINK.AsInteger;
        if quMenuSelLIMITPRICE.AsFloat>0 then
        quCurSpecLimitM.AsInteger:=RoundEx(quMenuSelLIMITPRICE.AsFloat)
        else quCurSpecLimitM.AsInteger:=99;
        quCurSpecStream.AsInteger:=quMenuSelSTREAM.AsInteger;
        quCurSpecQUEST.AsInteger:=cxImageComboBox1.EditValue;
        quCurSpec.Post;
        inc(Check.Max);

        if quMenuSelLINK.AsInteger>0 then
        begin //������������
          acModify.Execute;
        end;

        if quMenuSelPRNREST.AsInteger=1 then ViewSpec.Tag:=1;

        GridSpec.SetFocus;
      end
        else
      begin
         Label18.Caption:='����� �� K��� (��) �� �������.';
         prWriteLog('---KodBad;'+sKod);
      end;
    end;
  end;
end;

procedure TfmSpec.cxButton7Click(Sender: TObject);
begin
  with dmC do
  begin
    if Tab.iStatus>0 then exit;

//    prWriteLog('---Kod;'+IntToStr(Tab.Id)+';'+Tab.NumTable+';'+Person.Name+';'+IntToStr(Tab.Id_Personal)+';');
    acKod.Execute;
    GridSpec.SetFocus;
  end;
end;

procedure TfmSpec.acDiscountExecute(Sender: TObject);
begin
  Edit1.SetFocus;
  DiscountBar:=';';
end;

procedure TfmSpec.ViewSpecFocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord;
  ANewItemRecordFocusingChanged: Boolean);
begin
  with dmC do
  begin
    quCurMod.Active:=False;
    quCurMod.ParamByName('IDT').AsInteger:=quCurSpecID_TAB.AsInteger;
    quCurMod.ParamByName('IDP').AsInteger:=quCurSpecID.AsInteger;
    quCurMod.ParamByName('STATION').AsInteger:=CommonSet.Station;
    quCurMod.Active:=True;
  end;
end;

procedure TfmSpec.cxButton8Click(Sender: TObject);
begin
  bSaveSpec:=False; //�� �������� ������� ��������
  with dmC do
  begin
    if not quCurSpec.Bof then quCurSpec.Prior;
  end;
end;

procedure TfmSpec.cxButton9Click(Sender: TObject);
begin
  bSaveSpec:=False; //�� �������� ������� ��������
  with dmC do
  begin
    if not quCurSpec.Eof then
    begin
      quCurSpec.Next;
      if quCurSpec.Eof then
      begin
        quCurSpec.Prior;
        quCurSpec.Next;
      end;
    end;
  end;
end;

procedure TfmSpec.cxButton10Click(Sender: TObject);
Var StrWk:String;
    cSum,cSumD:Real;
    SumDItog,ProcMax:Real;
begin
  if Tab.iStatus>0 then
  begin
    Label18.Caption:='����� � ������� ��������. �������������� ���������.';
    exit;
  end;
  with dmC do
  begin
    if cxCheckBox1.Checked then cxCheckBox1.Checked:=False;
    quCurSpec.Filtered:=False;
    delay(10);

    if quCurSpec.RecordCount>0 then
    begin
      fmDiscount_Shape.cxTextEdit1.Text:='';
      fmDiscount_Shape.Caption:='��������� ���������� �����';
      fmDiscount_Shape.Label1.Caption:='��������� ���������� �����';
      fmDiscount_Shape.ShowModal;
      if fmDiscount_Shape.ModalResult=mrOk then
      begin
        //������� ������� ������
//      showmessage('');

        if fmDiscount_Shape.cxTextEdit1.Text>'' then
        begin
          DiscountBar:=SOnlyDigit(fmDiscount_Shape.cxTextEdit1.Text);

          prWriteLog('��:"'+DiscountBar+'"');

          If FindDiscount(DiscountBar) then
          begin
            if Tab.DType=3 then
            begin            // ��������� ������
              Str(Tab.DPercent:5:2,StrWk);
              Label17.Caption:='�������� ������ - '+ Tab.DName+' ('+StrWk+'%)';
              FormLog('DiscCard',IntToStr(Person.Id)+' '+quTabsNUMZ.AsString+' '+StrWk+' '+DiscountBar);

              Check.DSum:=0;

//������ ������ �� ��� �������
              ViewSpec.BeginUpdate;
              try
                quCurSpec.First;
                While not quCurSpec.Eof do
                begin
                  cSum:=rv(quCurSpecPrice.AsFloat*quCurSpecQuantity.AsFloat);

                  quCurSpec.Edit;
                  quCurSpecSumma.AsFloat:=cSum;
                  quCurSpecDProc.AsFloat:=0;
                  quCurSpecDSum.AsFloat:=0;
                  quCurSpec.Post;

                  ViewSpecDPROC.Visible:=False;
                  ViewSpecDSum.Visible:=False;
                  quCurSpec.Next;
                end;
                quCurSpec.First;
                prDiscHist(Tab.OpenTime,Tab.iNumZ,Tab.DPercent,0);
              finally
                ViewSpec.EndUpdate;
              end;
            end else
            begin
             // ��������� ������
              Str(Tab.DPercent:5:2,StrWk);
              Label17.Caption:='������������ ������ - '+ Tab.DName+' ('+StrWk+'%)';
              FormLog('DiscCard',IntToStr(Person.Id)+' '+quTabsNUMZ.AsString+' '+StrWk+' '+DiscountBar+' '+quTabsID.AsString);

              //������ ������ �� ��� �������
              ViewSpec.BeginUpdate;
              try
                SumDItog:=0;
                ProcMax:=0;
                quCurSpec.First;
                While not quCurSpec.Eof do
                begin
                  CalcDiscontN(quCurSpecSifr.AsInteger,quCurSpecId.AsInteger,-1,quCurSpecSTREAM.AsInteger,quCurSpecPrice.AsFloat,quCurSpecQuantity.AsFloat,0,Tab.DBar,Now,Check.DProc,Check.DSum);
//              if Check.DSum<>0 then //�������� ������� ������
//              begin
                  cSum:=RoundEx((quCurSpecPrice.AsFloat*quCurSpecQuantity.AsFloat-Check.DSum)*100)/100;
                  cSumD:=RoundEx((quCurSpecPrice.AsFloat*quCurSpecQuantity.AsFloat)*100)/100-cSum;
                  SumDItog:=SumDItog+cSumD;
                  if Check.DProc>ProcMax then ProcMax:=Check.DProc;

                  quCurSpec.Edit;
                  quCurSpecSumma.AsFloat:=cSum;
                  quCurSpecDProc.AsFloat:=Check.DProc;
                  quCurSpecDSum.AsFloat:=cSumD;
                  quCurSpec.Post;

                  ViewSpecDPROC.Visible:=True;
                  ViewSpecDSum.Visible:=True;
//              end;
                  quCurSpec.Next;
                end;
                quCurSpec.First;
                prDiscHist(Tab.OpenTime,Tab.iNumZ,ProcMax,SumDItog);

              finally
                ViewSpec.EndUpdate;
              end;
            end;
          end;
        end else //������ �������� - ��� ������ ������
        begin
          //������ ������ �� ��� �������
          DiscountBar:='';
          Check.DProc:=0;
          Check.DSum:=0;
          Tab.DBar:='';
          Tab.DPercent:=0;
          Tab.DName:='';
          Label17.Caption:='';

          prWriteLog('--CalcDiscTo0;'+IntToStr(Tab.Id)+';'+IntToStr(Tab.Id_Personal)+';'+FloatToStr(Tab.DPercent)+';'+Tab.DBar);

          ViewSpec.BeginUpdate;
          try
            quCurSpec.First;
            While not quCurSpec.Eof do
            begin
              quCurSpec.Edit;
              quCurSpecSumma.AsFloat:=RoundEx(quCurSpecPrice.AsFloat*quCurSpecQuantity.AsFloat*100)/100;
              quCurSpecDProc.AsFloat:=0;
              quCurSpecDSum.AsFloat:=0;
              quCurSpec.Post;

              quCurSpec.Next;
            end;
            //�������

            Label7.Caption:='';
            ViewSpecDPROC.Visible:=False;
            ViewSpecDSum.Visible:=False;

            quCurSpec.First;
          finally
            ViewSpec.EndUpdate;
          end;
        end;
      end;
    end;
  end;
end;

procedure TfmSpec.cxButton11Click(Sender: TObject);
begin
  SEdit1.EditValue:=SEdit1.EditValue+1;
end;

procedure TfmSpec.cxButton13Click(Sender: TObject);
begin
  if SEdit1.EditValue>=1 then SEdit1.EditValue:=SEdit1.EditValue-1;
end;

procedure TfmSpec.acNumTabExecute(Sender: TObject);
Var bTab:Boolean;
    iCount:Integer;
    sNUmT:String;
begin
  fmCalc.CalcEdit1.Text:=BEdit1.Text;
  bTab:=False;
  while not bTab do
  begin
    fmCalc.ShowModal;
    if fmCalc.ModalResult=mrOk then
    begin
      sNumT:=fmCalc.CalcEdit1.Text;
      if Length(sNumT)>8 then sNumT:=Copy(sNumT,1,8);
      with dmC do
      begin
        quTabExists.Active:=False;
        quTabExists.ParamByName('Id_Personal').AsInteger:=Tab.Id_Personal;
        quTabExists.ParamByName('NumTab').AsString:=sNUmT;
        quTabExists.Active:=True;
        iCount:=quTabExistsCOUNT.AsInteger;
        if iCount>0 then showmessage('������ ����� ��� ���������. �������� ������.')
        else bTab:=true;
      end;
    end else
    begin
      bTab:=True;
      fmCalc.CalcEdit1.Text:=BEdit1.Text;
    end;
  end;
  BEdit1.Text:=Copy(fmCalc.CalcEdit1.Text,1,8);
end;

procedure TfmSpec.BEdit1PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  acNumTab.Execute;
end;

procedure TfmSpec.BEdit1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  acNumTab.Execute;
end;

procedure TfmSpec.BEdit1Click(Sender: TObject);
begin
  acNumTab.Execute;
end;

procedure TfmSpec.cxButton12Click(Sender: TObject);
Var bEdit,bEditH:Boolean;
    rSum,rSumD:Real;

    strWk,StrWk1:String;
    iRet,IdH,IdC:Integer;
    bCheckOk:Boolean;
    rDiscont,rDProc:Real;
    iSumPos,iSumTotal,iSumDisc,iSumIt:INteger;
    TabCh:TTab;
    iAvans,iSumA,iQ:INteger;
    iCurCheck:INteger;
    StrP,sMess:String;
    iPC:INteger;
    nd:Smallint;

 procedure prClearCheck;
 begin
   fmAttention.Label1.Caption:='���� ��������� ���� � ��. ��������� ������������ ����!';
   fmAttention.Label2.Caption:='';
   fmAttention.Label3.Caption:='';

   fmAttention.ShowModal;
   Nums.iRet:=InspectSt(Nums.sRet);
   prWriteLog('~~AttentionShow;'+'���� ��������� ���� � ��. ��������� ������������ ����!');

   fmAttention.Label2.Caption:='����� ���������� ������ ������� "�����"';
   fmAttention.Label3.Caption:='��������� ������������ ��������� ��������.';

   prWriteLog('~~CheckCancel;');
   CheckCancel;
    //��������� �����
   bCheckOk:=False;
 end;

 procedure prQuite;
 begin
   bPressB12Spec:=False;
   cxButton12.Enabled:=True;
 end;

begin
//������
  if bMoveSpec then
  begin
    prWriteLog('---ExitBeforeEndSaveMove1;'+IntToStr(Tab.Id)+';'+Tab.NumTable+';'+Person.Name+';'+IntToStr(Tab.Id_Personal)+';');
    prWriteLog('');
    exit;
  end;

  //���� �� ������� ��� ������� ����� ������ �����
  if ViewSpec.Tag=1 then
  begin
    if prTestRemn(sMess)=False then
    begin
      showmessage(sMess);
      prWriteLog('---��������� �������; '+sMess);
      exit;
    end;
  end;

  bPressB12Spec:=True;
  cxButton12.Enabled:=False;
  with dmC do
  with dmC1 do
  begin

    if (CommonSet.MustPrePrint=1)and(Tab.iStatus=0)then
    begin
      ShowMessage('�������� ������� ����.');
      prQuite;
      exit;
    end;

    //�������� � ������� ������ �����
    if cxCheckBox1.Checked then cxCheckBox1.Checked:=False;
    quCurSpec.Filtered:=False;
    delay(10);

    ViewSpec.BeginUpdate;
    ViewMod.BeginUpdate;
    try
      rSum:=0;
      rSumD:=0;
      bEdit:=False;
      bEditH:=False;

      quCurSpec.First;
      while not quCurSpec.Eof do
      begin
        rSum:=rSum+quCurSpecSumma.AsFloat;
        rSumD:=rSumD+quCurSpecDSum.AsFloat;
        if quCurSpecIStatus.AsInteger=0 then bEdit:=True;
        quCurSpec.Next;
      end;

      if rSum<>Tab.Summa then bEdit:=True;
      if Tab.DBar<>Tab.DBar1 then bEdit:=True;
      if (Tab.NumTable<>BEdit1.Text)or(Tab.Quests<>SEdit1.EditValue) then bEditH:=True;
      if quSaleTID.AsInteger<>fmSpec.cxButton17.Tag then bEditH:=True;

      bAbort:=False;

      if bEdit or bEditH then
      begin
        if TestBL(sMess) then
        begin
          Tab.Summa:=rSum;
          //������, ���� ���� ��������� ��� ����������� ������ ������
          SaveStatusTab(Tab.Id,Tab.iStatus,sEdit1.Value,quSaleTID.AsInteger,BEdit1.Text,rSum,Tab.DBar);

          if bEdit then
          begin
            SaveSpecN;
          end;

          CreateViewPers1;
        end else
        begin
          bAbort:=True;
          fmAttention.Label1.Caption:=sMess;
          prWriteLog('~~AttentionBLShow;'+sMess);
          fmAttention.ShowModal;
        end;
      end;

    finally
      ViewSpec.EndUpdate;
      ViewMod.EndUpdate;
    end;

   //��������, ������ ��� � ��������


    if (CanDo('prPrintCheck')=False) or (bAbort=True) then begin prQuite; exit; end;

    if More24H(iRet) then
    begin
      fmAttention.Label1.Caption:='������ ����� 24 �����. ���������� ������� �����.';
      prWriteLog('~~AttentionShow;'+'������ ����� 24 �����. ���������� ������� �����.');
      fmAttention.ShowModal;
      prQuite;   //������� , �� ���� �� ������ �� ���������� ������ �� ����
      exit;
    end;

    TabCh.Id_Personal:=Tab.Id_Personal;
    TabCh.Name:=Tab.Name;

    TabCh.OpenTime:=Tab.OpenTime;
    TabCh.NumTable:=Tab.NumTable;
    TabCh.Quests:=sEdit1.Value;
    TabCh.iStatus:=Tab.iStatus;
    TabCh.DBar:=Tab.DBar;
    TabCh.Id:=Tab.Id;

    FindDiscount(TabCh.DBar); //��� ������������ Tab.DBar, Tab.DPercent, Tab.DName, Tab.DType

    prWriteLog('--RaschSpec;'+IntToStr(Tab.Id)+';'+Tab.NumTable+';'+IntToStr(Tab.Id_Personal)+';'+Tab.Name+';'+fts(rSum)+';'+Tab.DBar+';'+fts(rSumD));

    //���� ��������� �������� �� ������ �������� iAvans
    // 1 ����� ������� ������ ����� ����  ---- ������������ ������
    // 2 ����� ������ = ����� ����  ----- TabCh.Summa=0
    // 3 ����� ������ < ����� ����  ----- �� ���������� ����� ���� �������� ���

    quCheck.Active:=False;
    quCheck.ParamByName('IDTAB').Value:=TabCh.Id;
    quCheck.Active:=True;

    rSum:=0; iSumA:=0;
    iAvans:=0;
    quCheck.First;
    while not quCheck.Eof do
    begin
      rSum:=rSum+quCheckSUMMA.AsFloat;
      if quCheckPRICE.AsFloat<0 then iSumA:=RoundEx(quCheckSUMMA.AsFloat*100);
      quCheck.Next;
    end;
    if iSumA<0 then //������ �� ���� � ������
    begin
      rSum:=rv(rSum);
      if abs(rSum)>0.001 then iAvans:=3; // 3 ����� ������ < ����� ����  ----- �� ���������� ����� ���� �������� ���
      if abs(rSum)<0.001 then iAvans:=2; // 2 ����� ������ = ����� ����
      if rSum<0 then iAvans:=1; // 1 ����� ������� ������ ����� ����  ---- ������������ ������
      if not CanDo('prAvansCheck') then
      begin
        fmAttention.Label1.Caption:='��� ���� ��� ������ � �������� !!!.';
        prWriteLog('~~AttentionShow;'+'��� ���� ��� ������ � �������� !!!.');
        fmAttention.ShowModal;
        prQuite;
        exit;
      end;
    end;

    TabCh.Summa:=rSum;

    if TabCh.Summa<0.01 then
    begin
      if iAvans<2 then  //��������, ��� �������� ������
      begin
        fmAttention.Label1.Caption:='��� � ������� � ������������� ������ �������� ������ !!!.';
        prWriteLog('~~AttentionShow;'+'��� � ������� � ������������� ������ �������� ������ !!!.');
        fmAttention.ShowModal;
        prQuite;   //������� , �� ���� �� ������ �� ���������� ������ �� ����
        exit;
      end;
    end;

    if (Operation=1) and (iAvans>1) then
    begin
      fmAttention.Label1.Caption:='������ �������� ��� �������� (������)!!!.';
      prWriteLog('~~AttentionShow;'+'������ �������� ��� �������� (������)!!!.');
      fmAttention.ShowModal;
      prQuite;   //������� , �� ���� �� ������ �� ���������� ������ �� ����
      exit;
    end;

    taModif.Active:=True;

    if iAvans=2 then // 2 ����� ������ = ����� ����  ������� ������ ������������ ��������
    begin
      PosCh.Name:='';
      PosCh.Code:='';
      PosCh.Price:=0; //� ��������
      PosCh.Count:=0; //� �������
      PosCh.Sum:=0;

      try
        if CommonSet.PrePrintPort='fisprim' then
        begin
          OpenNFDoc;

          SelectF(13);PrintNFStr(' '+CommonSet.DepartName); PrintNFStr('');
          SelectF(14); PrintNFStr('       ������');
          SelectF(13); PrintNFStr('');
          PrintNFStr('��������: '+TabCh.Name);
          PrintNFStr('����: '+TabCh.NumTable);
          SelectF(13);PrintNFStr('������: '+IntToStr(TabCh.Quests));
          PrintNFStr('������: '+FormatDateTime('dd.mm.yyyy hh:nn:ss',TabCh.OpenTime));
          SelectF(13);StrWk:='                                          ';
          PrintNFStr(StrWk);
          SelectF(15);StrWk:=' ��������          ���-��   ����   ����� ';
          PrintNFStr(StrWk);
          SelectF(13);StrWk:='                                          ';
          PrintNFStr(StrWk);
          SelectF(15); PrintNFStr(' ');PrintNFStr(' ');

          iSumA:=iSumA*(-1);
          Str((rSum+iSumA/100):10:2,StrWk);
          StrWk:='����� ����� �����:    '+StrWk+' ���';
          PrintNFStr(StrWk); PrintNFStr(' ');PrintNFStr(' ');

          Str((iSumA/100):10:2,StrWk);
          StrWk:='������ ����� �� �����:'+StrWk+' ���';
          PrintNFStr(StrWk); PrintNFStr(' ');PrintNFStr(' ');

          StrWk:='����� �� �����       :      0,00 ���';
          PrintNFStr(StrWk); PrintNFStr(' ');

          SelectF(13);StrWk:='                                          ';
          PrintNFStr(StrWk);
          SelectF(11);
          PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');
          CloseNFDoc;
          if CommonSet.TypeFis<>'sp101' then CutDoc;
//          CutDoc;
        end;
        if pos('DB',CommonSet.PrePrintPort)>0 then
        begin
        //��������� �������
          with dmC1 do
          begin
            quPrint.Active:=False;
            quPrint.Active:=True;
            iQ:=GetId('PQH'); //����������� ������� �� 1-�

            PrintDBStr(iQ,0,CommonSet.PrePrintPort,' '+CommonSet.DepartName,13,0);
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,'',13,0);
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,'       ������',14,0);
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,'',13,0);
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,'��������: '+TabCh.Name,13,0);
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,'����: '+TabCh.NumTable,13,0);
            if Tab.DBar>'' then
            begin
              Str(Tab.DPercent:5:2,StrWk);
              PrintDBStr(iQ,0,CommonSet.PrePrintPort,'�����: '+Tab.DName+' ('+StrWk+'%)',13,0);
            end;
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,'������: '+IntToStr(TabCh.Quests),13,0);
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,'������: '+FormatDateTime('dd.mm.yyyy hh:nn:ss',TabCh.OpenTime),13,0);
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,'-',13,0);
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,' ��������      ���-��   ����   �����',15,0);
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,'-',13,0);
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,'',13,0);
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,'',13,0);


            iSumA:=iSumA*(-1);
            Str((rSum+iSumA/100):10:2,StrWk);
            StrWk:='����� ����� �����:    '+StrWk+' ���';
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,StrWk,15,0);
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,'',13,0);
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,'',13,0);

            Str((iSumA/100):10:2,StrWk);
            StrWk:='������ ����� �� �����:'+StrWk+' ���';
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,StrWk,15,0);
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,'',13,0);
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,'',13,0);

            StrWk:='����� �� �����       :      0,00 ���';
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,StrWk,15,0);
            PrintDBStr(iQ,0,CommonSet.PrePrintPort,'',13,0);

            PrintDBStr(iQ,0,CommonSet.PrePrintPort,'-',13,0);

            prAddPrintQH(0,iQ,FormatDateTime('dd.mm hh:nn:sss',now)+', ������� - '+IntToStr(CommonSet.Station)+','+CommonSet.PrePrintPort);
            quPrint.Active:=False;
          end;
        end;

      finally

       //�������� � ������ ����� ��� ���������� ��������� - ��� �� �������

        prSaveToAll.ParamByName('ID_TAB').AsInteger:=TabCh.Id;
        prSaveToAll.ParamByName('OPERTYPE').AsString:='Sale';
        prSaveToAll.ParamByName('CHECKNUM').AsInteger:=0;
        prSaveToAll.ParamByName('SKLAD').AsInteger:=0; //�������� �����������
        prSaveToAll.ParamByName('STATION').AsInteger:=CommonSet.Station; //����� �������
        prSaveToAll.ParamByName('ID_PERSONCLOSE').AsInteger:=Person.Id; //��� ������
        prSaveToAll.ParamByName('SPBAR').AsString:=''; // ��� ���� �� ���� ������� - ��������� ����� �� ������������
        prSaveToAll.ExecProc;

        if Tab.DType=3 then prWritePCSum(Tab.DBar,rv(CalcBonus(Tab.Id,Tab.DPercent)),prSaveToAll.ParamByName('ID_H').AsInteger); //ID - ������������ ������

        // ������ ����
        quDelTab.Active:=False;
        quDelTab.ParamByName('Id').AsInteger:=TabCh.Id;

        trDel.StartTransaction;
        quDelTab.Active:=True;
        trDel.Commit;
        //��� � cashsaile ������ �� ���� �.�. ����� =0
      end;

      fmMainCashRn.CreateViewPers(True);
//      prQuite;
      bPressB12Spec:=False;
      fmSpec.cxButton12.Enabled:=True;
//      fmSpec.close; //�� ����������� ������ ��������
    end;


    //������ �������� ������  iAvans:=3; ����� ������ < ����� ����
    if (iAvans=3)or(iAvans=0) then
    begin
    with fmCash do
    begin
      fmCash.Caption:='�������� ������';
      fmCash.Label5.Caption:='���. '+TabCh.Name+'  ���� - '+TabCh.NumTable;
      fmCash.Label6.Caption:='����� - '+Floattostr(TabCh.Summa)+' �.';
      fmCash.cEdit1.EditValue:=rv(TabCh.Summa);
      fmCash.cEdit2.EditValue:=rv(TabCh.Summa);
      fmCash.cEdit3.EditValue:=0;
      fmCash.cEdit2.SelectAll;

      if CommonSet.CashNum>0 then
      begin
        iRet:=InspectSt(StrWk);
        if iRet=0 then
        begin
          fmCash.Label7.Caption:='������ ���: ��� ��.';
        end
        else //��� ������  - ����� ���� ������ ��� ���������� ���
        begin
          fmCash.Label7.Caption:='������ ���: ������ ('+IntToStr(iRet)+') '+StrWk;
          fmMainCashRn.Label3.Caption:='������ ���: ������ ('+IntToStr(iRet)+')';

          fmAttention.Label1.Caption:=StrWk;
          prWriteLog('~~AttentionShow;'+StrWk);
          fmAttention.ShowModal;

          while TestStatus('InspectSt',sMessage)=False do
          begin
            fmAttention.Label1.Caption:=sMessage;
            prWriteLog('~~AttentionShow;'+sMessage);
            fmAttention.ShowModal;
            prQuite;   //�������  �  ���� �� ������ �� ���������� ������ �� ����
            exit;
          end;
        end;
        //���� ����� �� ���� ��� � �����

        fmCash.Label7.Caption:='������ ���: ��� ��.';
        fmMainCashRn.Label3.Caption:='������ ���: ��� ��.';
      end;
    end;

    fmCash.ShowModal;
    if fmCash.ModalResult=mrOk then
    begin
      TestPrint:=1;
      While PrintQu and (TestPrint<=MaxDelayPrint) do
      begin
//        showmessage('������� �����, ��������� �������.');
        prWriteLog('������������� ������ ���� � �������.');
        delay(1000);
        inc(TestPrint);
      end;

      try
        bCheckOk:=False;
//        bPrintCheck:=False; //��� �� 2-��� �������
        bPrintCheck:=True;
        PrintFCheck:=True; //��� ���� � ������� ������� ��������� ������


        iPC:=0; //�� ��������� ��������� ���� ���

        //��������� ��������� �����
        if fmCash.CEdit6.value>0 then
        begin
          prFindPCardBalans(SalePC.BarCode,SalePC.rBalans,SalePC.rBalansDay,SalePC.DLimit,SalePC.CliName,SalePC.CliType);
          if SalePC.rBalansDay>=fmCash.CEdit6.value then
          begin
            if fmCash.CEdit6.value=fmCash.CEdit1.value then iPc:=2 else iPc:=1;  //���� ������ ������������, ���� ���������� , �� ��������
           // iPc:=2  ������������ �������
          end;
        end;

      //������� ���
        if (CommonSet.CashNum>0)and(CommonSet.SpecChar=0)and(iPC<2) then
        begin

       //�������� ������� �������� �����
          CashDriver;
          GetNums;

          Case Operation of
          0: begin
//             prWriteLog('!!CheckStart;'+IntToStr((Nums.iCheckNum+1))+';'+CurPos.Articul+';'+CurPos.Bar+';'+FloatToStr(CurPos.Quant)+';'+FloatToStr(CurPos.Price)+';'+FloatToStr(CurPos.DSum)+';');
               prWriteLog('!!CheckStart;'+IntToStr((Nums.iCheckNum+1))+';'+IntToStr(Person.Id)+';'+Person.Name+';');
               CutDoc;
               CheckStart;
               while TestStatus('CheckStart',sMessage)=False do
               begin
                 fmAttention.Label1.Caption:=sMessage;
                 prWriteLog('~~AttentionShow;'+sMessage);
                 fmAttention.ShowModal;
                 Nums.iRet:=InspectSt(Nums.sRet);
               end;
             end;
          1: begin
               prWriteLog('!!CheckRetStart;'+IntToStr((Nums.iCheckNum+1))+';'+IntToStr(Person.Id)+';'+Person.Name+';');
               CheckRetStart;
               while TestStatus('CheckRetStart',sMessage)=False do
               begin
                 fmAttention.Label1.Caption:=sMessage;
                 prWriteLog('~~AttentionShow;'+sMessage);
                 fmAttention.ShowModal;
                 Nums.iRet:=InspectSt(Nums.sRet);
               end;
             end;
          end;


          //�������

          rDiscont:=0;
          if (iAvans=0)and(iPC=0) then
          begin
            rSum:=0;
            quCheck.First;
            while not quCheck.Eof do
            begin      //��������� �������
              PosCh.Name:=quCheckNAME.AsString;
              PosCh.Code:=quCheckSIFR.AsString;
              PosCh.AddName:='';
              PosCh.Price:=RoundEx(quCheckPRICE.AsFloat*100);
              PosCh.Count:=RoundEx(quCheckQUANTITY.AsFloat*1000);
              PosCh.Sum:=RoundEx(quCheckPRICE.AsFloat*quCheckQUANTITY.AsFloat*100)/100;

              rDiscont:=rDiscont+quCheckDISCOUNTSUM.AsFloat;
              rSum:=rSum+quCheckSUMMA.AsFloat;

              quCheck.Next;
              if not quCheck.Eof then
              begin
                while quCheckITYPE.AsInteger=1 do
                begin  //������������
                  if quCheck.Eof then break;
                  if quCheckSifr.AsInteger>0 then
                    if taModif.Locate('SIFR',quCheckSifr.AsInteger,[]) then
                      PosCh.AddName:=PosCh.AddName+'|   '+taModifNAME.AsString;
                  quCheck.Next;
                end;
                delete(PosCh.AddName,1,1);
              end;

              prWriteLog('~!CheckAddPos; � ���� '+IntToStr((Nums.iCheckNum+1))+'; ��� '+PosCh.Code+'; ���-�� '+FloatToStr(PosCh.Count)+'; ���� '+FloatToStr(PosCh.Price)+'; ����� '+FloatToStr(PosCh.Sum)+';');
              if PosCh.Sum>=0.01 then   //������� ������� �������� ������
              begin
                CheckAddPos(iSumPos);
                while TestStatus('CheckAddPos',sMessage)=False do
                begin
                  fmAttention.Label1.Caption:=sMessage;
                  prWriteLog('~~AttentionShow;'+sMessage);
                  fmAttention.ShowModal;
                  Nums.iRet:=InspectSt(Nums.sRet);
                end;
              end else prWriteLog('~!CheckAddPosBad; � ���� '+IntToStr((Nums.iCheckNum+1))+'; ��� '+PosCh.Code+'; ���-�� '+FloatToStr(PosCh.Count)+'; ���� '+FloatToStr(PosCh.Price)+'; ����� '+FloatToStr(PosCh.Sum)+';');
            end;
          end else //���� ���� ������ �� ��� ����� �������
          begin
            if iAvans<>0 then
            begin
              PosCh.Name:='� ������ �� �����';
              PosCh.Code:='';
              iSumA:=iSumA*(-1);
              Str((rSum+iSumA/100):10:2,StrWk);
              PosCh.AddName:='|����� ����� �����:    '+StrWk+'|������ ����� �� �����:';
              Str((iSumA/100):10:2,StrWk);
              PosCh.AddName:=PosCh.AddName+StrWk+'| ';
              PosCh.Price:=RoundEx(rSum*100); //� ��������
              PosCh.Count:=1000; //� �������
              PosCh.Sum:=RoundEx(rSum*100)/100;

              CheckAddPos(iSumPos);
              while TestStatus('CheckAddPos',sMessage)=False do
              begin
                fmAttention.Label1.Caption:=sMessage;
                prWriteLog('~~AttentionShow;'+sMessage);
                fmAttention.ShowModal;
                Nums.iRet:=InspectSt(Nums.sRet);
              end;
            end;
            if iPC<>0 then  //�������� ��������� ������
            begin
              PosCh.Name:='� ������ �� �����';
              PosCh.Code:='';

              Str(rv(fmCash.cEdit1.value):10:2,StrWk);
              PosCh.AddName:='|����� ����� �����:    '+StrWk+'|';
              Str(rv(fmCash.cEdit6.value):10:2,StrWk);
              PosCh.AddName:=PosCh.AddName+'�������� '+Copy(SalePC.CliName,1,20)+':'+StrWk+'| ';
              Str(rv(SalePC.rBalans-fmCash.cEdit6.value):10:2,StrWk);
              PosCh.AddName:=PosCh.AddName+'������ �� �����:'+ StrWk+'| ';
              Str(rv(SalePC.rBalansDay-fmCash.cEdit6.value):10:2,StrWk);
              PosCh.AddName:=PosCh.AddName+'������� �������� ������:'+ StrWk+'| ';

              PosCh.Price:=RoundEx(rv(fmCash.cEdit1.value-fmCash.cEdit6.value)*100); //� ��������
              PosCh.Count:=1000; //� �������
              PosCh.Sum:=RoundEx(rv(fmCash.cEdit1.value-fmCash.cEdit6.value)*100)/100; //�� ������������ � CheckAddPos(iSumPos);

              CheckAddPos(iSumPos);
              while TestStatus('CheckAddPos',sMessage)=False do
              begin
                fmAttention.Label1.Caption:=sMessage;
                prWriteLog('~~AttentionShow;'+sMessage);
                fmAttention.ShowModal;
                Nums.iRet:=InspectSt(Nums.sRet);
              end;

              rSum:=PosCh.Sum;
            end;

          end;

          //������������ ���������� - ������ ����, ������, ������;
          CheckTotal(iSumTotal);
          prWriteLog('!!AfterCheckTotal;'+IntToStr((Nums.iCheckNum+1))+';'+inttostr(iSumTotal)+';');
          while TestStatus('CheckTotal',sMessage)=False do
          begin
            fmAttention.Label1.Caption:=sMessage;
            prWriteLog('~~AttentionShow;'+sMessage);
            fmAttention.ShowModal;
            Nums.iRet:=InspectSt(Nums.sRet);
          end;

          iSumIt:=0; iSumDisc:=0;
          if abs(rDiscont)>=0.01 then //������� ����� ������ ��� ������� ����
          begin
            CheckDiscount(rDiscont,iSumDisc,iSumIt); //� ������ iSumIt=0 ������
            prWriteLog('!!AfterCheckDisc;'+IntToStr((Nums.iCheckNum+1))+';'+FloatToStr(rDiscont)+';'+IntToStr(iSumDisc)+';'+IntToStr(iSumIt)+';');
            while TestStatus('CheckDiscount',sMessage)=False do
            begin
              fmAttention.Label1.Caption:=sMessage;
              prWriteLog('~~AttentionShow;'+sMessage);
              fmAttention.ShowModal;
              Nums.iRet:=InspectSt(Nums.sRet);
            end;
          end;

//        ��������, ���� ���� ����������� - ��������� ���
        //�������� iSumTotal

{
12:57:34 ;!!AfterCheckDisc;3777;33;3300;0;
12:57:34 ;!!AfterCheckRasch;3777; ����. ����� 77; ����. ����� 7700;
12:57:41 ;~~AttentionShow;���� ��������� ���� � ��. ��������� ������������ ����!
12:57:41 ;~~CheckCancel;

}

          if iSumTotal=0 then iSumTotal:=RoundEx(rSum*100); //rSum - ��� ����� ��� ������
          if (iSumDisc=0)and (abs(rDiscont)>=0.01) then iSumDisc:=RoundEx(rDiscont*100);
          if iSumIt=0 then iSumIt:=iSumTotal-iSumDisc;
          if abs(rSum -(iSumIt/100))>0.05 then
          begin
            prWriteLog('!!AfterCheckRasch;'+IntToStr((Nums.iCheckNum+1))+'; ����. ����� '+FloatToStr(rSum*100)+'; ����. ����� '+INtToStr(iSumIt)+';');
            prClearCheck;
          end
          else
          begin
//            if iCashType=0 then CheckRasch(0,RoundEx(fmCash.cEdit2.EditValue*100),'',iSumIt) //���
//            else CheckRasch(1,RoundEx(fmCash.cEdit1.EditValue*100),'',iSumIt); //������

            if fmCash.cEdit5.EditValue>0 then
            begin
              CheckRasch(1,RoundEx(fmCash.cEdit5.EditValue*100),'',iSumIt); //������
            end;
            if fmCash.cEdit2.EditValue>0 then
            begin
              CheckRasch(0,RoundEx(fmCash.cEdit2.EditValue*100),'',iSumIt) //���
            end;

            prWriteLog('!!AfterCheckRasch;'+IntToStr((Nums.iCheckNum+1))+';'+FloatToStr(fmCash.cEdit5.EditValue)+';'+FloatToStr(fmCash.cEdit2.EditValue)+';'+INtToStr(iSumIt)+';');
            while TestStatus('CheckRasch',sMessage)=False do
            begin
              fmAttention.Label1.Caption:=sMessage;
              prWriteLog('~~AttentionShow;'+sMessage);
              fmAttention.ShowModal;
              Nums.iRet:=InspectSt(Nums.sRet);
            end;

            prWriteLog('!!CheckClose;'+IntToStr((Nums.iCheckNum+1))+';');
            CheckClose;
            while TestStatus('CheckClose',sMessage)=False do
            begin
              fmAttention.Label1.Caption:=sMessage;
              prWriteLog('~~AttentionShow;'+sMessage);
              fmAttention.ShowModal;
              Nums.iRet:=InspectSt(Nums.sRet);
            end;

          //�������� ������ ����� ������
            iRet:=InspectSt(StrWk);
            iCurCheck:=Nums.iCheckNum+1; //��� ���-�� ������� ���

            if iRet=0 then
            begin
              fmMainCashRn.Label3.Caption:='������ ���: ��� ��.';
              if CashDate(StrWk) then fmMainCashRn.Label2.Caption:='�������� ����: '+StrWk;
              if GetNums then fmMainCashRn.Label4.Caption:='��� � '+Nums.CheckNum;
          //��� ����������� �������� CommonSet.CashChNum - ����� �������� ��� � ���
              WriteCheckNum;
            end
            else
            begin
              fmMainCashRn.Label3.Caption:='������ ���: ������ ('+IntToStr(iRet)+')';

              while TestStatus('InspectSt',sMessage)=False do
              begin
                fmAttention.Label1.Caption:=sMessage;
                prWriteLog('~~AttentionShow;'+sMessage);
                fmAttention.ShowModal;
                Nums.iRet:=InspectSt(Nums.sRet);
              end;
            end;

            bCheckOk:=True;

            if CommonSet.ControlNums=1 then
            begin
              if (Nums.iCheckNum <> iCurCheck) then
              begin //���������� ������������������ �����
                  //������� ���������� ������������
                prWriteLog('!!ErrorNumCheck;new-'+IntToStr(Nums.iCheckNum+1)+';old-'+IntToStr(iCurCheck)+';');
                prClearCheck;
              end;
            end;  

            PrintBn(BnStr);
            PrintBn(BnStr);
            BnStr:='';
          end;
        end else //����� ������������, �� ��� �� �������� ����� .. ������� c ��������� ������
        begin
          if CommonSet.PrePrintPort<>'0' then
        //�� ����������� ��� STAR
          begin
            if (CommonSet.CashNum<0)and(CommonSet.SpecChar=0)and(pos('DB',CommonSet.PrePrintPort)=0) then //������ ������������ ����� �� ��������� �������
            begin
            try
              OpenMoneyBox;
              delay(20);

              prDevOpen(CommonSet.PrePrintPort,0);
              BufPr.iC:=0;

              StrP:=CommonSet.PrePrintPort;

              prSetFont(StrP,13,0);PrintStr(StrP,' '+CommonSet.DepartName); PrintStr(StrP,'');

              prSetFont(StrP,14,0); PrintStr(StrP,'       ������');
              prSetFont(StrP,13,0); PrintStr(StrP,'');
              PrintStr(StrP,'��������: '+TabCh.Name);
              PrintStr(StrP,'����: '+TabCh.NumTable);
              prSetFont(StrP,13,0);PrintStr(StrP,'������: '+IntToStr(TabCh.Quests));
              PrintStr(StrP,'������: '+FormatDateTime('dd.mm.yyyy hh:nn:ss',TabCh.OpenTime));

              prSetFont(StrP,13,0);StrWk:='                                          ';
              PrintStr(StrP,StrWk);
              prSetFont(StrP,15,0);StrWk:=' ��������      ���-��   ����   �����';
              PrintStr(StrP,StrWk);
              prSetFont(StrP,13,0);StrWk:='                                          ';
              PrintStr(StrP,StrWk);

             //�������
              rDiscont:=0;

              taStreams.Active:=False;
              taStreams.Active:=True;
              taStreams.First;
              while not taStreams.Eof do
              begin
                rSum:=0;

                quCheck.First;
                while not quCheck.Eof do
                begin      //��������� �������

                  PosCh.Name:=quCheckNAME.AsString;
                  PosCh.Code:=quCheckCODE.AsString;
                  PosCh.AddName:='';
                  PosCh.Stream:=quCheckSTREAM.AsInteger;

                  if PosCh.Stream=taStreamsID.AsInteger then
                  begin
                    rDiscont:=rDiscont+quCheckDISCOUNTSUM.AsFloat;
                    rSum:=rSum+quCheckSUMMA.AsFloat;

                    StrWk:= Copy(PosCh.Name,1,18);
                    while Length(StrWk)<18 do StrWk:=StrWk+' ';

                    if abs(frac(quCheckQUANTITY.AsFloat*100))>0 then nd:=3  //���� ������� ����� �� �������
                    else
                      if abs(frac(quCheckQUANTITY.AsFloat*10))>0 then nd:=2  //���� ������� ����� �������� �� �������
                      else nd:=1;

                    Str(quCheckQUANTITY.AsFloat:5:nd,StrWk1);
                    StrWk:=StrWk+' '+StrWk1;
                    Str(quCheckPRICE.AsFloat:7:2,StrWk1);
                    StrWk:=StrWk+' '+StrWk1;
                    Str(quCheckSUMMA.AsFloat:8:2,StrWk1);
                    StrWk:=StrWk+' '+StrWk1+'�';
                    prSetFont(StrP,15,0);
                    PrintStr(StrP,StrWk);


                    quCheck.Next;
                    if not quCheck.Eof then
                    begin
                      while quCheckITYPE.AsInteger=1 do
                      begin  //������������
                        if quCheck.Eof then break;
                        if quCheckSifr.AsInteger>0 then
                          if taModif.Locate('SIFR',quCheckSifr.AsInteger,[]) then
                          begin
                            PrintStr(StrP,'   '+Copy(taModifNAME.AsString,1,29));
                          end;
                        quCheck.Next;
                      end;
                    end;
                  end else
                  quCheck.Next;
                end;
                if rSum<>0 then
                begin
                  prSetFont(StrP,13,0);
                  StrWk:='                    ';
                  PrintStr(StrP,StrWk);
                  //�������� ������� �� ������
                  Str(rSum:8:2,StrWk1);
                  StrWk:='����� �� '+taStreamsNAMESTREAM.AsString+'  '+StrWk1+'�';

                  prSetFont(StrP,15,0);
                  PrintStr(StrP,StrWk);
                  PrintStr(StrP,'');
                end;


                taStreams.Next;
              end;

              prSetFont(StrP,13,0);
              StrWk:='                                          ';
              PrintStr(StrP,StrWk);

              prSetFont(StrP,15,0);
              Str(TabCh.Summa:8:2,StrWk1);
              StrWk:=' �����                      '+StrWk1+' ���';
              PrintStr(StrP,StrWk);

              if rDiscont>0.02 then
              begin
                prSetFont(StrP,15,0);
                PrintStr(StrP,'');
                prSetFont(StrP,10,0);
                rDProc:=RoundEx((rDiscont/(TabCh.Summa+rDiscont)*100)*100)/100;
                Str(rDProc:5:2,StrWk1);
                Str(rDiscont:8:2,StrWk);
//                StrWk:=' � �.�. ������ - '+StrWk1+'%  '+StrWk+'�.';
                StrWk:=' � �.�. ������ - '+StrWk+'�.';
                PrintStr(StrP,StrWk);
              end;

              prWrBuf(StrP);
              prCutDoc(StrP,0);
            finally
              taStreams.Active:=False;
//              Delay(100);
              prDevClose(StrP,0);
            end;
            end;

            if ((CommonSet.CashNum<0)or(iPC=2))and(CommonSet.SpecChar=0)and(pos('DB',CommonSet.PrePrintPort)>0) then //������ ������������ ����� �� � �������
            begin
              if pos('fis',CommonSet.SpecBox)=0 then OpenMoneyBox
              else
              begin
                try
                  StrWk:='COM'+IntToStr(CommonSet.CashPort);
                  CommonSet.CashNum:=CommonSet.CashNum*(-1);
                  CashOpen(PChar(StrWk));
                  CashDriver;
                  CashClose;
                  CommonSet.CashNum:=CommonSet.CashNum*(-1);
                except
                end;
              end;

              with dmC1 do
              begin
               try
                quPrint.Active:=False;
                quPrint.Active:=True;
                iQ:=GetId('PQH'); //����������� ������� �� 1-�

                rSum:=0;

                if CommonSet.PreLine1>'' then
                begin
                  if fDifStr(1,CommonSet.PreLine1)>'' then PrintDBStr(iQ,0,CommonSet.PrePrintPort,fDifStr(1,CommonSet.PreLine1),10,0);
                  if fDifStr(2,CommonSet.PreLine1)>'' then PrintDBStr(iQ,0,CommonSet.PrePrintPort,fDifStr(2,CommonSet.PreLine1),10,0);
                end;

                PrintDBStr(iQ,0,CommonSet.PrePrintPort,' ',10,0);
                PrintDBStr(iQ,0,CommonSet.PrePrintPort,'  �������� ��� �'+IntToStr(CommonSet.CashChNum),14,0);
                PrintDBStr(iQ,0,CommonSet.PrePrintPort,' ',10,0);
                PrintDBStr(iQ,0,CommonSet.PrePrintPort,'��������: '+TabCh.Name,13,0);
                PrintDBStr(iQ,0,CommonSet.PrePrintPort,'����: '+TabCh.NumTable,13,0);
                if Tab.DBar>'' then
                begin
                  Str(Tab.DPercent:5:2,StrWk);
                  PrintDBStr(iQ,0,CommonSet.PrePrintPort,'�����: '+Tab.DName+' ('+StrWk+'%)',13,0);
                end;
                PrintDBStr(iQ,0,CommonSet.PrePrintPort,'������: '+IntToStr(TabCh.Quests),13,0);
                PrintDBStr(iQ,0,CommonSet.PrePrintPort,'������: '+FormatDateTime('dd.mm.yyyy hh:nn:ss',TabCh.OpenTime),13,0);
                PrintDBStr(iQ,0,CommonSet.PrePrintPort,'-',13,0);
                PrintDBStr(iQ,0,CommonSet.PrePrintPort,' ��������      ���-��   ����   �����',15,0);
                PrintDBStr(iQ,0,CommonSet.PrePrintPort,'-',13,0);

                rDiscont:=0;

              {  quCheck.First;
                while not quCheck.Eof do
                begin      //��������� �������

                  PosCh.Name:=quCheckNAME.AsString;
                  PosCh.Code:=quCheckCODE.AsString;
                  PosCh.AddName:='';

                  rDiscont:=rDiscont+quCheckDISCOUNTSUM.AsFloat;

                  StrWk:= Copy(PosCh.Name,1,19);
                  while Length(StrWk)<19 do StrWk:=StrWk+' ';

                  if abs(frac(quCheckQUANTITY.AsFloat*100))>0 then nd:=3  //���� ������� ����� �� �������
                  else
                    if abs(frac(quCheckQUANTITY.AsFloat*10))>0 then nd:=2  //���� ������� ����� �������� �� �������
                    else nd:=1;

                  Str(quCheckQUANTITY.AsFloat:5:nd,StrWk1);
                  StrWk:=StrWk+' '+StrWk1;
                  Str(quCheckPRICE.AsFloat:7:2,StrWk1);
                  StrWk:=StrWk+' '+StrWk1;
                  Str((quCheckPRICE.AsFloat*quCheckQUANTITY.AsFloat):8:2,StrWk1);
                  StrWk:=StrWk+StrWk1+'�';

                  PrintDBStr(iQ,0,CommonSet.PrePrintPort,StrWk,15,0);

                  rSum:=rSum+quCheckSUMMA.AsFloat;

                  quCheck.Next;

                  if not quCheck.Eof then
                  begin
                    while quCheckITYPE.AsInteger=1 do
                    begin  //������������
                      if quCheck.Eof then break;
                      if quCheckSifr.AsInteger>0 then
                        if taModif.Locate('SIFR',quCheckSifr.AsInteger,[]) then
                        begin
                          PrintDBStr(iQ,0,CommonSet.PrePrintPort,'   '+Copy(taModifNAME.AsString,1,29),15,0);
                        end;
                      quCheck.Next;
                    end;
                  end;
                end;
                }


                taStreams.Active:=False;
                taStreams.Active:=True;
                taStreams.First;
                while not taStreams.Eof do
                begin
                  rSum:=0;

                  quCheck.First;
                  while not quCheck.Eof do
                  begin      //��������� �������
                    PosCh.Stream:=quCheckSTREAM.AsInteger;

                    if (PosCh.Stream=taStreamsID.AsInteger) then
                    begin
                      if (quCheckITYPE.AsInteger=0) then
                      begin
                        PosCh.Name:=quCheckNAME.AsString;
                        PosCh.Code:=quCheckCODE.AsString;
                        PosCh.AddName:='';

                        rDiscont:=rDiscont+quCheckDISCOUNTSUM.AsFloat;
                        rSum:=rSum+quCheckSUMMA.AsFloat;

                        StrWk:= Copy(PosCh.Name,1,18);
                        while Length(StrWk)<18 do StrWk:=StrWk+' ';

                        if abs(frac(quCheckQUANTITY.AsFloat*100))>0 then nd:=3  //���� ������� ����� �� �������
                        else
                          if abs(frac(quCheckQUANTITY.AsFloat*10))>0 then nd:=2  //���� ������� ����� �������� �� �������
                          else nd:=1;

                        Str(quCheckQUANTITY.AsFloat:5:nd,StrWk1);
                        StrWk:=StrWk+' '+StrWk1;
                        Str(quCheckPRICE.AsFloat:7:2,StrWk1);
                        StrWk:=StrWk+' '+StrWk1;
                        Str(quCheckSUMMA.AsFloat:8:2,StrWk1);
                        StrWk:=StrWk+' '+StrWk1+'�';

//                      prSetFont(StrP,15,0); PrintStr(StrP,StrWk);
                        PrintDBStr(iQ,0,CommonSet.PrePrintPort,StrWk,15,0);
                        quCheck.Next;
                        if not quCheck.Eof then
                        begin
                          while quCheckITYPE.AsInteger=1 do
                          begin  //������������
                            if quCheck.Eof then break;
                            if quCheckSifr.AsInteger>0 then
                              if taModif.Locate('SIFR',quCheckSifr.AsInteger,[]) then
                              begin
//                              PrintStr(StrP,'   '+Copy(taModifNAME.AsString,1,29));
                                PrintDBStr(iQ,0,CommonSet.PrePrintPort,'   '+Copy(taModifNAME.AsString,1,29),15,0);
                              end;
                            quCheck.Next;
                          end;
                        end;
                      end else quCheck.Next;
                    end else
                    quCheck.Next;
                  end;
                  if rSum<>0 then
                  begin
  //                  prSetFont(StrP,13,0);
                    StrWk:='                    ';
//                    PrintStr(StrP,StrWk);

                    PrintDBStr(iQ,0,CommonSet.PrePrintPort,StrWk,13,0);
                    //�������� ������� �� ������
                    Str(rSum:8:2,StrWk1);
                    StrWk:='����� �� '+taStreamsNAMESTREAM.AsString+'  '+StrWk1+'�';
//                    prSetFont(StrP,15,0);
//                    PrintStr(StrP,StrWk);
//                    PrintStr(StrP,'');

                    PrintDBStr(iQ,0,CommonSet.PrePrintPort,StrWk,15,0);
                    PrintDBStr(iQ,0,CommonSet.PrePrintPort,'  ',15,0);
                  end;

                  taStreams.Next;
                end;

                PrintDBStr(iQ,0,CommonSet.PrePrintPort,'-',13,0);

                Str(TabCh.Summa:8:2,StrWk1);
                StrWk:=' �����                    '+StrWk1+' ���';
                PrintDBStr(iQ,0,CommonSet.PrePrintPort,StrWk,15,0);

                Case Operation of
                0:begin
                    PrintDBStr(iQ,0,CommonSet.PrePrintPort,'�������                            ',15,0);
                    if fmCash.cEdit5.EditValue>0 then
                    begin
                      PrintDBStr(iQ,0,CommonSet.PrePrintPort,prDefFormatStr(40,'��������� �����:',fmCash.cEdit5.EditValue),15,0);
                    end;
                    if fmCash.cEdit6.EditValue>0 then
                    begin
                      PrintDBStr(iQ,0,CommonSet.PrePrintPort,prDefFormatStr(40,'��������� �����:',fmCash.cEdit6.EditValue),15,0);
                    end;
                    if fmCash.cEdit2.EditValue>0 then
                    begin
                      PrintDBStr(iQ,0,CommonSet.PrePrintPort,prDefFormatStr(40,'��������:',fmCash.cEdit2.EditValue),15,0);
                    end;
                    if (fmCash.cEdit5.EditValue+fmCash.cEdit2.EditValue+fmCash.cEdit6.EditValue)>TabCh.Summa then
                    begin
                      PrintDBStr(iQ,0,CommonSet.PrePrintPort,prDefFormatStr(40,'�����:',(fmCash.cEdit5.EditValue+fmCash.cEdit6.EditValue+fmCash.cEdit2.EditValue)-TabCh.Summa),15,0);
                    end;
                  end;
                1:begin
                    PrintDBStr(iQ,0,CommonSet.PrePrintPort,'�������                            ',15,0);
                    if fmCash.cEdit5.EditValue>0 then
                    begin
                      PrintDBStr(iQ,0,CommonSet.PrePrintPort,prDefFormatStr(40,'��������� �����:',fmCash.cEdit5.EditValue),15,0);
                    end;
                    if fmCash.cEdit6.EditValue>0 then
                    begin
                      PrintDBStr(iQ,0,CommonSet.PrePrintPort,prDefFormatStr(40,'��������� �����:',fmCash.cEdit6.EditValue),15,0);
                    end;
                    if (rSum-fmCash.cEdit5.EditValue-fmCash.cEdit6.EditValue)>0 then
                    begin
                      PrintDBStr(iQ,0,CommonSet.PrePrintPort,prDefFormatStr(40,'��������:',(rSum-fmCash.cEdit5.EditValue-fmCash.cEdit6.EditValue)),15,0);
                    end;
                  end;
                end;

                if rDiscont>0.02 then
                begin
                  PrintDBStr(iQ,0,CommonSet.PrePrintPort,'',15,0);
                  rDProc:=RoundEx((rDiscont/(TabCh.Summa+rDiscont)*100)*100)/100;
                  Str(rDProc:5:2,StrWk1);
                  Str(rDiscont:8:2,StrWk);
                  StrWk:=' � �.�. ������ - '+StrWk+'�.';
                  PrintDBStr(iQ,0,CommonSet.PrePrintPort,StrWk,10,0);
                end;

                if iPC=2 then
                begin
                  PrintDBStr(iQ,0,CommonSet.PrePrintPort,'',10,0);
                  Str(rv(fmCash.cEdit6.value):10:2,StrWk);
                  PrintDBStr(iQ,0,CommonSet.PrePrintPort,'�������� '+Copy(SalePC.CliName,1,20)+':'+StrWk,10,0);
                  Str(rv(SalePC.rBalans-fmCash.cEdit6.value):10:2,StrWk);
                  PrintDBStr(iQ,0,CommonSet.PrePrintPort,'������ �� �����:'+ StrWk,10,0);
                  Str(rv(SalePC.rBalansDay-fmCash.cEdit6.value):10:2,StrWk);
                  PrintDBStr(iQ,0,CommonSet.PrePrintPort,'������� �������� ������:'+ StrWk,10,0);
                  PrintDBStr(iQ,0,CommonSet.PrePrintPort,'',10,0);
                end;

                PrintDBStr(iQ,0,CommonSet.PrePrintPort,'������: '+Person.Name,10,0);
                PrintDBStr(iQ,0,CommonSet.PrePrintPort,'�������: _____________________',10,0);

                if CommonSet.LastLine1>'' then
                begin
                  PrintDBStr(iQ,0,CommonSet.PrePrintPort,'',10,0);
                  if fDifStr(1,CommonSet.LastLine1)>'' then PrintDBStr(iQ,0,CommonSet.PrePrintPort,fDifStr(1,CommonSet.LastLine1),10,0);
                  if fDifStr(2,CommonSet.LastLine1)>'' then PrintDBStr(iQ,0,CommonSet.PrePrintPort,fDifStr(2,CommonSet.LastLine1),10,0);
                end;

                prAddPrintQH(0,iQ,FormatDateTime('dd.mm hh:nn:sss',now)+', ������� - '+IntToStr(CommonSet.Station)+','+CommonSet.PrePrintPort);
                quPrint.Active:=False;

               finally
                bCheckOk:=True;
               end;
              end;
            end;
            //
            if (CommonSet.CashNum>0)and((CommonSet.SpecChar=1)or(iPC=2)) then //������ ������������ ����� ��� ������������ ���
            begin
              if pos('fisshtrih',CommonSet.PrePrintPort)>0 then
              begin
                TestPrint:=1;
                While PrintQu and (TestPrint<=MaxDelayPrint) do
                begin
//                showmessage('������� �����, ��������� �������.');
                  prWriteLog('������������� ������ ���� � �������.');
                  delay(1000);
                  inc(TestPrint);
                end;
                try
                  PrintFCheck:=True; //��� ���� � ������� ������� ��������� ������

                  CashDriver;

                  PrintNFStr(' '+CommonSet.DepartName);

                  if CommonSet.PreLine1>'' then
                  begin
                    if fDifStr(1,CommonSet.PreLine1)>'' then PrintNFStr(fDifStr(1,CommonSet.PreLine1));
                    if fDifStr(2,CommonSet.PreLine1)>'' then PrintNFStr(fDifStr(2,CommonSet.PreLine1));
                  end;

                  PrintNFStr(' ');
                  PrintNFStr('       ��� �'+IntToStr(CommonSet.CashChNum));
                  PrintNFStr(' ');
                  PrintNFStr('��������: '+TabCh.Name);
                  PrintNFStr('����: '+TabCh.NumTable);
                  PrintNFStr('������: '+FormatDateTime('dd.mm.yyyy hh:nn:ss',TabCh.OpenTime));
                  StrWk:='-----------------------------------';
                  PrintNFStr(StrWk);
                  StrWk:=' ��������     ���-��  ����   ����� ';
                  PrintNFStr(StrWk);
                  StrWk:='-----------------------------------';
                  PrintNFStr(StrWk);

                  rDiscont:=0;

                  quCheck.First;
                  while not quCheck.Eof do
                  begin      //��������� �������

                    PosCh.Name:=quCheckNAME.AsString;
                    PosCh.Code:=quCheckCODE.AsString;
                    PosCh.AddName:='';

                    StrWk:= Copy(PosCh.Name,1,36);
                    while Length(StrWk)<36 do StrWk:=StrWk+' ';
                    PrintNFStr(StrWk); //������� ����� �������� - �������� �������

                    rDiscont:=rDiscont+quCheckDISCOUNTSUM.AsFloat;

                    if abs(frac(quCheckQUANTITY.AsFloat*100))>0 then nd:=3  //���� ������� ����� �� �������
                    else
                      if abs(frac(quCheckQUANTITY.AsFloat*10))>0 then nd:=2  //���� ������� ����� �������� �� �������
                      else nd:=1;

                    Str(quCheckQUANTITY.AsFloat:5:nd,StrWk1);
                    StrWk:='          '+StrWk1;
                    Str(quCheckPRICE.AsFloat:7:2,StrWk1);
                    StrWk:=StrWk+' '+StrWk1;
                    Str(quCheckSUMMA.AsFloat:8:2,StrWk1);
                    StrWk:=StrWk+' '+StrWk1+'���';
                    PrintNFStr(StrWk);

                    quCheck.Next;
                    if not quCheck.Eof then
                    begin
                      while quCheckITYPE.AsInteger=1 do
                      begin  //������������
                        if quCheck.Eof then break;
                        if quCheckSifr.AsInteger>0 then
                        if taModif.Locate('SIFR',quCheckSifr.AsInteger,[]) then
                        begin
                          StrWk:='   '+Copy(taModifNAME.AsString,1,29);
                          PrintNFStr(StrWk);
                        end;
                        quCheck.Next;
                      end;
                    end;
                  end;

                  StrWk:='-----------------------------------';
                  PrintNFStr(StrWk);
                  Str(TabCh.Summa:8:2,StrWk1);
                  StrWk:=' �����                '+StrWk1+' ���';
                  PrintNFStr(StrWk);

                  Case Operation of
                  0:begin
                      PrintNFStr('�������                                 ');
                      if fmCash.cEdit5.EditValue>0 then
                      begin
                        PrintNFStr(prDefFormatStr(32,'��������� �����:',fmCash.cEdit5.EditValue));
                      end;
                      if fmCash.cEdit6.EditValue>0 then
                      begin
                        PrintNFStr(prDefFormatStr(32,'��������� �����:',fmCash.cEdit6.EditValue));
                      end;
                      if fmCash.cEdit2.EditValue>0 then
                      begin
                        PrintNFStr(prDefFormatStr(32,'��������:',fmCash.cEdit2.EditValue));
                      end;
                      if (fmCash.cEdit5.EditValue+fmCash.cEdit6.EditValue+fmCash.cEdit2.EditValue)>rSum then
                      begin
                        PrintNFStr(prDefFormatStr(32,'�����:',(fmCash.cEdit5.EditValue+fmCash.cEdit6.EditValue+fmCash.cEdit2.EditValue)-rSum));
                      end;
                    end;
                  1:begin
                      PrintNFStr('�������                                 ');
                      if fmCash.cEdit5.EditValue>0 then
                      begin
                        PrintNFStr(prDefFormatStr(32,'��������� �����:',fmCash.cEdit5.EditValue));
                      end;
                      if fmCash.cEdit6.EditValue>0 then
                      begin
                        PrintNFStr(prDefFormatStr(32,'��������� �����:',fmCash.cEdit6.EditValue));
                      end;
                      if (rSum-fmCash.cEdit5.EditValue-fmCash.cEdit6.EditValue)>0 then
                      begin
                        PrintNFStr(prDefFormatStr(32,'��������:',(rSum-fmCash.cEdit5.EditValue-fmCash.cEdit6.EditValue)));
                      end;
                    end;
                  end;

                  if rDiscont>0.02 then
                  begin
                    PrintNFStr('');
                    rDProc:=RoundEx((rDiscont/(TabCh.Summa+rDiscont)*100)*100)/100;
                    Str(rDProc:5:2,StrWk1);
                    Str(rDiscont:8:2,StrWk);
                    StrWk:=' � �.�. ������ - '+StrWk1+'%  '+StrWk+'�.';
                    PrintNFStr(StrWk);
                  end;

                  if iPC=2 then
                  begin
                    PrintNFStr(' ');
                    Str(rv(fmCash.cEdit6.value):10:2,StrWk);
                    PrintNFStr('�������� '+Copy(SalePC.CliName,1,20)+':'+StrWk);
                    Str(rv(SalePC.rBalans-fmCash.cEdit6.value):10:2,StrWk);
                    PrintNFStr('������ �� �����:'+ StrWk);
                    Str(rv(SalePC.rBalansDay-fmCash.cEdit6.value):10:2,StrWk);
                    PrintNFStr('������� �������� ������:'+ StrWk);
                    PrintNFStr(' ');
                  end;

                  PrintNFStr('������: '+Person.Name);
                  PrintNFStr('�������: _____________________');


                  if CommonSet.LastLine1>'' then
                  begin
                    PrintNFStr('');
                    if fDifStr(1,CommonSet.LastLine1)>'' then PrintNFStr(fDifStr(1,CommonSet.LastLine1));
                    if fDifStr(2,CommonSet.LastLine1)>'' then PrintNFStr(fDifStr(2,CommonSet.LastLine1));
                  end;

                  PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');PrintNFStr(' ');
                  CutDoc;
                finally
                  bCheckOk:=True;
                  PrintFCheck:=False; //��� ���� � ������� ������� ��������� ������
                end;
              end;
              if pos('fisprim',CommonSet.PrePrintPort)>0 then
              begin
                TestPrint:=1;
                While PrintQu and (TestPrint<=MaxDelayPrint) do
                begin
//                showmessage('������� �����, ��������� �������.');
                  prWriteLog('������������� ������ ���� � �������.');
                  delay(1000);
                  inc(TestPrint);
                end;
                try
                  PrintFCheck:=True; //��� ���� � ������� ������� ��������� ������

                  CashDriver;

                  OpenNFDoc;

                  PrintNFStr(' '+CommonSet.DepartName);

                  if CommonSet.PreLine1>'' then
                  begin
                    if fDifStr(1,CommonSet.PreLine1)>'' then PrintNFStr(fDifStr(1,CommonSet.PreLine1));
                    if fDifStr(2,CommonSet.PreLine1)>'' then PrintNFStr(fDifStr(2,CommonSet.PreLine1));
                  end;

                  PrintNFStr(' ');
                  SelectF(14);
                  PrintNFStr('     ��� �'+IntToStr(CommonSet.CashChNum));
                  SelectF(10);
                  PrintNFStr(' ');
                  PrintNFStr('��������: '+TabCh.Name);
                  PrintNFStr('����: '+TabCh.NumTable);
                  PrintNFStr('������: '+FormatDateTime('dd.mm.yyyy hh:nn:ss',TabCh.OpenTime));
                  StrWk:='----------------------------------------';
                  PrintNFStr(StrWk);
                  StrWk:=' ��������     ���-��   ����    �����  ';
                  PrintNFStr(StrWk);
                  StrWk:='----------------------------------------';
                  PrintNFStr(StrWk);

                  rDiscont:=0;

                  quCheck.First;
                  while not quCheck.Eof do
                  begin      //��������� �������

                    PosCh.Name:=quCheckNAME.AsString;
                    PosCh.Code:=quCheckCODE.AsString;
                    PosCh.AddName:='';

                    StrWk:= Copy(PosCh.Name,1,36);
                    while Length(StrWk)<36 do StrWk:=StrWk+' ';
                    PrintNFStr(StrWk); //������� ����� �������� - �������� �������

                    rDiscont:=rDiscont+quCheckDISCOUNTSUM.AsFloat;

                    Str(quCheckQUANTITY.AsFloat:7:3,StrWk1);
                    StrWk:='          '+StrWk1;
                    Str(quCheckPRICE.AsFloat:7:2,StrWk1);
                    StrWk:=StrWk+' '+StrWk1;
                    Str(quCheckSUMMA.AsFloat:8:2,StrWk1);
                    StrWk:=StrWk+' '+StrWk1+'���';
                    PrintNFStr(StrWk);

                    quCheck.Next;
                    if not quCheck.Eof then
                    begin
                      while quCheckITYPE.AsInteger=1 do
                      begin  //������������
                        if quCheck.Eof then break;
                        if quCheckSifr.AsInteger>0 then
                        if taModif.Locate('SIFR',quCheckSifr.AsInteger,[]) then
                        begin
                          StrWk:='   '+Copy(taModifNAME.AsString,1,32);
                          PrintNFStr(StrWk);
                        end;
                        quCheck.Next;
                      end;
                    end;
                  end;

                  StrWk:='----------------------------------------';
                  PrintNFStr(StrWk);
                  Str(TabCh.Summa:8:2,StrWk1);
                  StrWk:=' �����                '+StrWk1+' ���';
                  PrintNFStr(StrWk);

                  Case Operation of
                  0:begin
                      PrintNFStr('�������                                 ');
                      if fmCash.cEdit5.EditValue>0 then
                      begin
                        PrintNFStr(prDefFormatStr(32,'��������� �����:',fmCash.cEdit5.EditValue));
                      end;
                      if fmCash.cEdit6.EditValue>0 then
                      begin
                        PrintNFStr(prDefFormatStr(32,'��������� �����:',fmCash.cEdit6.EditValue));
                      end;
                      if fmCash.cEdit2.EditValue>0 then
                      begin
                        PrintNFStr(prDefFormatStr(32,'��������:',fmCash.cEdit2.EditValue));
                      end;
                      if (fmCash.cEdit5.EditValue+fmCash.cEdit6.EditValue+fmCash.cEdit2.EditValue)>rSum then
                      begin
                        PrintNFStr(prDefFormatStr(32,'�����:',(fmCash.cEdit5.EditValue+fmCash.cEdit6.EditValue+fmCash.cEdit2.EditValue)-rSum));
                      end;
                    end;
                  1:begin
                      PrintNFStr('�������                                 ');
                      if fmCash.cEdit5.EditValue>0 then
                      begin
                        PrintNFStr(prDefFormatStr(32,'��������� �����:',fmCash.cEdit5.EditValue));
                      end;
                      if fmCash.cEdit6.EditValue>0 then
                      begin
                        PrintNFStr(prDefFormatStr(32,'��������� �����:',fmCash.cEdit6.EditValue));
                      end;
                      if (rSum-fmCash.cEdit5.EditValue-fmCash.cEdit6.EditValue)>0 then
                      begin
                        PrintNFStr(prDefFormatStr(32,'��������:',(rSum-fmCash.cEdit5.EditValue-fmCash.cEdit6.EditValue)));
                      end;
                    end;
                  end;

                  if rDiscont>0.02 then
                  begin
                    PrintNFStr('');
                    rDProc:=RoundEx((rDiscont/(TabCh.Summa+rDiscont)*100)*100)/100;
                    Str(rDProc:5:1,StrWk1);
                    Str(rDiscont:8:2,StrWk);
                    StrWk:=' � �.�. ������ - '+StrWk1+'%  '+StrWk+'�.';
                    PrintNFStr(StrWk);
                  end;

                  if iPC=2 then
                  begin
                    PrintNFStr(' ');
                    Str(rv(fmCash.cEdit6.value):10:2,StrWk);
                    PrintNFStr('�������� '+Copy(SalePC.CliName,1,20)+':'+StrWk);
                    Str(rv(SalePC.rBalans-fmCash.cEdit6.value):10:2,StrWk);
                    PrintNFStr('������ �� �����:'+ StrWk);
                    Str(rv(SalePC.rBalansDay-fmCash.cEdit6.value):10:2,StrWk);
                    PrintNFStr('������� �������� ������:'+ StrWk);
                    PrintNFStr(' ');
                  end;

                  PrintNFStr('������: '+Person.Name);
                  PrintNFStr('�������: _____________________');


                  if CommonSet.LastLine1>'' then
                  begin
                    PrintNFStr('');
                    if fDifStr(1,CommonSet.LastLine1)>'' then PrintNFStr(fDifStr(1,CommonSet.LastLine1));
                    if fDifStr(2,CommonSet.LastLine1)>'' then PrintNFStr(fDifStr(2,CommonSet.LastLine1));
                  end;

                  PrintNFStr(' ');// PrintNFStr(' '); // PrintNFStr(' '); PrintNFStr(' ');PrintNFStr(' ');

                  CloseNFDoc;
                  if CommonSet.TypeFis<>'sp101' then CutDoc;
//                  CutDoc;
                finally
                  bCheckOk:=True;
                  PrintFCheck:=False; //��� ���� � ������� ������� ��������� ������
                end;
              end;
            end;

          end;
        end;
        taModif.Active:=False;

        if CommonSet.CashNum<=0 then bCheckOk:=True;
        if bCheckOk then
        begin
          inc(CommonSet.CashChNum); //����� �������� 1-��
          WriteCheckNum;

          prSaveToAll.ParamByName('ID_TAB').AsInteger:=TabCh.Id;

          Case Operation of
        0:begin
            if iPC>0 then
            begin
              FormLog('SalePC',IntToStr(Person.Id)+' '+quTabsID_Personal.AsString+' '+quTabsNUMTABLE.AsString+' '+quTabsNUMZ.AsString);
              prSaveToAll.ParamByName('OPERTYPE').AsString:='SalePC';
            end else
            begin
              if iPayType=0 then
              begin
                FormLog('Sale',IntToStr(Person.Id)+' '+quTabsID_Personal.AsString+' '+quTabsNUMTABLE.AsString+' '+quTabsNUMZ.AsString);
                prSaveToAll.ParamByName('OPERTYPE').AsString:='Sale';
              end else
              begin
                FormLog('SaleBank',IntToStr(Person.Id)+' '+quTabsID_Personal.AsString+' '+quTabsNUMTABLE.AsString+' '+quTabsNUMZ.AsString);
                prSaveToAll.ParamByName('OPERTYPE').AsString:='SaleBank';
              end;
            end;
          end;
        1:begin
            if iPayType=0 then
            begin
              FormLog('Ret',IntToStr(Person.Id)+' '+quTabsID_Personal.AsString+' '+quTabsNUMTABLE.AsString+' '+quTabsNUMZ.AsString);
              prSaveToAll.ParamByName('OPERTYPE').AsString:='Ret';
            end else
            begin
              FormLog('RetBank',IntToStr(Person.Id)+' '+quTabsID_Personal.AsString+' '+quTabsNUMTABLE.AsString+' '+quTabsNUMZ.AsString);
              prSaveToAll.ParamByName('OPERTYPE').AsString:='RetBank';
             end;
            end;
          end;

          if CommonSet.CashNum<=0 then
          begin
            fmMainCashRn.Label4.Caption:='����� �'+IntToStr(CommonSet.CashZ)+'  ��� � '+IntToStr(CommonSet.CashChNum);
            prSaveToAll.ParamByName('CHECKNUM').AsInteger:=CommonSet.CashChNum;
          end
          else prSaveToAll.ParamByName('CHECKNUM').AsInteger:=Nums.iCheckNum;

          prSaveToAll.ParamByName('SKLAD').AsInteger:=0; //�������� �����������
          prSaveToAll.ParamByName('STATION').AsInteger:=CommonSet.Station; //����� �������
          prSaveToAll.ParamByName('ID_PERSONCLOSE').AsInteger:=Person.Id; //��� ������
          prSaveToAll.ParamByName('SPBAR').AsString:=SalePC.BarCode;
          prSaveToAll.ExecProc;

          IdH:=prSaveToAll.ParamByName('ID_H').AsInteger;

          //�������� �����
          if (Tab.DType=3) then
          begin
            Case Operation of    //��� ���� �� ������� ������ � ����� � ��������� ������� ��
            0: prWritePCSum(Tab.DBar,rv(CalcBonus(Tab.Id,Tab.DPercent)),IdH); //��� �������
            1: prWritePCSum(Tab.DBar,rv(CalcBonus(Tab.Id,Tab.DPercent)*(-1)),IdH);      //��� ��������
            end;
          end;

         // ��������� �������� �� ��������� �����
          Case Operation of    //��� ���� �� ������� ������ � ����� � ��������� ������� ��
          0: if iPC>0 then prWritePCSum(SalePC.BarCode,rv(fmCash.CEdit6.Value*(-1)),IdH); //- ��� �������
          1: if iPC>0 then prWritePCSum(SalePC.BarCode,rv(fmCash.CEdit6.Value),IdH);      //+ ��� ��������
          end;

         // ������ ����
          quDelTab.Active:=False;
          quDelTab.ParamByName('Id').AsInteger:=TabCh.Id;

          trDel.StartTransaction;
          quDelTab.Active:=True;
          trDel.Commit;

          //���� ���
          if fmCash.cEdit5.EditValue>0 then //��� ������
          begin
            idC:=GetId('CS');
            taCashSailSel.Active:=False;
            taCashSailSel.ParamByName('IDH').AsInteger:=IDC;
            taCashSailSel.Active:=True;

            taCashSailSel.Append;
            taCashSailSelID.AsInteger:=IdC;
            taCashSailSelCASHNUM.AsInteger:=CommonSet.CashNum;
            if CommonSet.CashNum<0 then
            begin
              taCashSailSelZNUM.AsInteger:=CommonSet.CashZ;
              taCashSailSelCHECKNUM.AsInteger:=CommonSet.CashChNum;
            end
            else
            begin
              taCashSailSelZNUM.AsInteger:=Nums.ZNum;
              taCashSailSelCHECKNUM.AsInteger:=Nums.iCheckNum;
            end;

            taCashSailSelTAB_ID.AsInteger:=IdH;

            Case Operation of
            0: taCashSailSelTABSUM.AsFloat:=fmCash.cEdit5.EditValue;
            1: taCashSailSelTABSUM.AsFloat:=(-1)*fmCash.cEdit5.EditValue;
            end;

            taCashSailSelCLIENTSUM.AsFloat:=fmCash.cEdit5.EditValue;
            taCashSailSelCHDATE.AsDateTime:=now;
            taCashSailSelCASHERID.AsInteger:=Person.Id;
            taCashSailSelWAITERID.AsInteger:=TabCh.Id_Personal;
            taCashSailSelPAYTYPE.AsInteger:=1;
            taCashSailSelPAYID.AsInteger:=iPayType;
            taCashSailSel.Post;

            taCashSailSel.Active:=False;
          end;
          if fmCash.cEdit2.EditValue>0 then //��� ���
          begin
            idC:=GetId('CS');
            taCashSailSel.Active:=False;
            taCashSailSel.ParamByName('IDH').AsInteger:=IDC;
            taCashSailSel.Active:=True;

            taCashSailSel.Append;
            taCashSailSelID.AsInteger:=IdC;
            taCashSailSelCASHNUM.AsInteger:=CommonSet.CashNum;
            if CommonSet.CashNum<0 then
            begin
              taCashSailSelZNUM.AsInteger:=CommonSet.CashZ;
              taCashSailSelCHECKNUM.AsInteger:=CommonSet.CashChNum;
            end
            else
            begin
              taCashSailSelZNUM.AsInteger:=Nums.ZNum;
              taCashSailSelCHECKNUM.AsInteger:=Nums.iCheckNum;
            end;

            taCashSailSelTAB_ID.AsInteger:=IdH;

            Case Operation of
            0: taCashSailSelTABSUM.AsFloat:=TabCh.Summa-fmCash.cEdit5.EditValue-fmCash.cEdit6.EditValue;
            1: taCashSailSelTABSUM.AsFloat:=(-1)*(TabCh.Summa-fmCash.cEdit5.EditValue-fmCash.cEdit6.EditValue);
            end;

            taCashSailSelCLIENTSUM.AsFloat:=fmCash.cEdit2.EditValue;
            taCashSailSelCHDATE.AsDateTime:=now;
            taCashSailSelCASHERID.AsInteger:=Person.Id;
            taCashSailSelWAITERID.AsInteger:=TabCh.Id_Personal;
            taCashSailSelPAYTYPE.AsInteger:=0;
            taCashSailSelPAYID.AsInteger:=0;
            taCashSailSel.Post;

            taCashSailSel.Active:=False;
          end;
        end;
      finally
        bPrintCheck:=False; //��� �� �������� �������
        PrintFCheck:=False; //��� ���� � ������� ������� ��������� ������

        prWriteLog('---CloseSpec ;'+IntToStr(Tab.Id)+';'+Tab.NumTable+';'+Person.Name+';'+IntToStr(Tab.Id_Personal)+';');;
        prWriteLog('');

        prClearCur.ParamByName('ID_TAB').AsInteger:=Tab.Id;
        prClearCur.ParamByName('ID_STATION').AsInteger:=CommonSet.Station;
        prClearCur.ExecProc;

        CreateViewPers1;
        Operation:=0;
        fmMainCashRn.Label5.Caption:='�������� - ������� ��������.';

        close;
      end;
    end;

    end; //if avans=3
    if iAvans=2 then close;
  end;
  bPressB12Spec:=False;
  cxButton12.Enabled:=True;
end;

procedure TfmSpec.acAddMeExecute(Sender: TObject);
begin
  with dmC do
  begin
    if quCurSpec.RecordCount=0 then exit;

    if Tab.iStatus>0 then
    begin
      Label18.Caption:='������ ������ �� ��������� ��������������.';
      exit;
    end;
    if quCurSpecIStatus.AsInteger=1 then
    begin
      Label18.Caption:='����� ��������, ��������� �����������.';
      exit; //������������� ����� ������ ������������� �������
    end;

    if taMes.Active=False then taMes.Active:=True;
    taMes.FullRefresh;

    fmModif:=TFmModif.Create(Application);
    
    fmModif.Label1.Caption:='';
    fmModif.Caption:='���������.';
    fmModif.LevelMo.Visible:=False;
    fmModif.LevelMe.Visible:=True;

    fmModif.ShowModal;
    fmModif.Release;
  end;
end;

procedure TfmSpec.cxButton14Click(Sender: TObject);
begin
  acAddMe.Execute;
end;

procedure TfmSpec.ViewModCustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);
Var iType,i:Integer;
begin
  if AViewInfo.GridRecord.Selected then exit;

  iType:=0;
  for i:=0 to ViewMod.ColumnCount-1 do
  begin
    if ViewMod.Columns[i].Name='ViewModSIFR' then
    begin
      iType:=VarAsType(AViewInfo.GridRecord.DisplayTexts[i], varInteger);
      break;
    end;
  end;

  if iType<0  then
  begin
    ACanvas.Canvas.Brush.Color := clWhite;
    ACanvas.Canvas.Font.Color := clGray;
  end;

end;

procedure TfmSpec.cxButton15Click(Sender: TObject);
begin
  prWriteLog('---DelMess;');
  acDelMod.Execute;
end;

procedure TfmSpec.cxButton16Click(Sender: TObject);
Var rSum:Real;
begin
  with dmC do
  begin
    ViewSpec.BeginUpdate;
    ViewMod.BeginUpdate;
    try
      rSum:=0;
      quCurSpec.First;
      while not quCurSpec.Eof do
      begin
        rSum:=rSum+quCurSpecSumma.AsFloat;
        quCurSpec.Next;
      end;
    finally
      ViewSpec.EndUpdate;
      ViewMod.EndUpdate;
    end;

    quMenuSel.Active:=False;
    quMenuSel.SelectSQL.Clear;
    quMenuSel.SelectSQL.Add('SELECT SIFR,NAME,PARENT,PRICE,CODE,LIMITPRICE,LINK,STREAM,BARCODE,PARENT,PRNREST');
    quMenuSel.SelectSQL.Add('FROM MENU');
    quMenuSel.SelectSQL.Add('WHERE SIFR='+IntToStr(CommonSet.KuponCode));
    quMenuSel.SelectSQL.Add('and TREETYPE<>''T''');

    quMenuSel.Active:=True;

    if quMenuSel.RecordCount>0 then
    begin
      if (rSum+quMenuSelPRICE.AsFloat)>=0 then
      begin
        quCurSpec.Append;
        quCurSpecSTATION.AsInteger:=CommonSet.Station;
        quCurSpecID_TAB.AsInteger:=Tab.Id;
        quCurSpecId_Personal.AsInteger:=Person.Id;
        quCurSpecNumTable.AsString:=Tab.NumTable;
        quCurSpecId.AsInteger:=Check.Max+1;
        quCurSpecSifr.AsInteger:=quMenuSelSIFR.AsInteger;
        quCurSpecPrice.AsFloat:=quMenuSelPRICE.AsFloat;
        quCurSpecQuantity.AsFloat:=1;
        quCurSpecSumma.AsFloat:=quMenuSelPRICE.AsFloat;
        quCurSpecDProc.AsFloat:=0;
        quCurSpecDSum.AsFloat:=0;
        quCurSpecIStatus.AsInteger:=0;
        quCurSpecName.AsString:=quMenuSelNAME.AsString;
        quCurSpecCode.AsString:=quMenuSelCODE.AsString;
        quCurSpecLinkM.AsInteger:=quMenuSelLINK.AsInteger;
        quCurSpecLimitM.AsInteger:=0;
        quCurSpecStream.AsInteger:=quMenuSelSTREAM.AsInteger;
        quCurSpecQUEST.AsInteger:=cxImageComboBox1.EditValue;
        quCurSpec.Post;

        inc(Check.Max);
      end;
    end;
  end;
  GridSpec.SetFocus;
end;

procedure TfmSpec.cxButton17Click(Sender: TObject);
begin
  with dmC1 do
  begin
//    if not quSaleT.Eof then quSaleT.Next else quSaleT.First;
    quSaleT.Next;
    if quSaleT.Eof then quSaleT.First;
    cxButton17.Caption:=Copy(quSaleTNAMECS.AsString,1,15);
  end;  
end;

procedure TfmSpec.Timer2Timer(Sender: TObject);
begin
  Timer2.Enabled:=False;
  if Button1.Visible and Button1.Enabled then Button1.Click;
end;

procedure TfmSpec.ViewSpecDragOver(Sender, Source: TObject; X, Y: Integer;
  State: TDragState; var Accept: Boolean);
begin
  Accept:=False;
  if bQuest then  Accept:=True;
end;

procedure TfmSpec.Panel2StartDrag(Sender: TObject;
  var DragObject: TDragObject);
begin
  bQuest:=True;
end;

procedure TfmSpec.ViewSpecDragDrop(Sender, Source: TObject; X, Y: Integer);
var j: Integer;
    iNum: Integer;
    Rec:TcxCustomGridRecord;
    AHitTest: TcxCustomGridHitTest;
begin
  if bQuest then
  begin
    bQuest:=False;
    with dmC do
    begin
      if quCurSpec.RecordCount>0 then
      begin
        if Sender is TcxGridSite then
        begin
          AHitTest := TcxGridSite(Sender).ViewInfo.GetHitTest(X, Y);
          if AHitTest.HitTestCode=107 then //������
          begin
            Rec:=TcxGridRecordHitTest(AHitTest).GridRecord;
            for j:=0 to Rec.ValueCount-1 do
            begin
              if ViewSpec.Columns[j].Name='ViewSpecID' then break;
            end;
            iNum:=Rec.Values[j];
            if quCurSpec.Locate('ID',iNum,[]) then
            begin
              if quCurSpecQUEST.AsInteger<>cxImageComboBox1.EditValue then
              begin
                quCurSpec.Edit;
                quCurSpecQUEST.AsInteger:=cxImageComboBox1.EditValue;
                quCurSpec.Post;

                cxImageComboBox1.Tag:=1;
              end;
            end;
          end;
        end;

      end;
    end;
  end;
end;

procedure TfmSpec.cxCheckBox1Click(Sender: TObject);
begin
  try
    ViewSpec.BeginUpdate;
    if cxCheckBox1.Checked then
    begin
//    showmessage('������ �� '+its(cxImageComboBox1.EditValue));
      dmC.quCurSpec.Filter:='QUEST='+its(cxImageComboBox1.EditValue);
      dmC.quCurSpec.Filtered:=True;
    end else
    begin
//    showmessage('������ �������.');
      dmC.quCurSpec.Filtered:=False;
    end;
  finally
    ViewSpec.EndUpdate;
  end;
end;

procedure TfmSpec.cxImageComboBox1PropertiesChange(Sender: TObject);
begin
  with dmC do
  begin
{    if cxCheckBox1.Checked then
    begin
      try
        ViewSpec.BeginUpdate;
        dmC.quCurSpec.Filtered:=False;
        dmC.quCurSpec.Filter:='QUEST='+its(cxImageComboBox1.EditValue);
        dmC.quCurSpec.Filtered:=True;
      finally
        ViewSpec.EndUpdate;
      end;
    end;}
  end;
end;

procedure TfmSpec.ViewSpecStartDrag(Sender: TObject;
  var DragObject: TDragObject);
begin
  bQuestTo:=True;
end;

procedure TfmSpec.Panel2DragOver(Sender, Source: TObject; X, Y: Integer;
  State: TDragState; var Accept: Boolean);
begin
  Accept:=False;
  if bQuestTo then  Accept:=True;
end;

procedure TfmSpec.Panel2DragDrop(Sender, Source: TObject; X, Y: Integer);
Var bDrobn:Boolean;
    rMod,rQ1,rQ2,rSum1,rSum2,rSumD1,rSumD2,rPr,rDProc:Real;
    iStatus,iSifr:INteger;
    sName,sCode:String;
    iLink,iLimitM,iStream:Integer;
begin
  if bQuestTo then
  begin
    bQuestTo:=False;
    with dmC do
    begin
      if quCurSpec.RecordCount>0 then
      begin
        if quCurSpecQUEST.AsInteger<>cxImageComboBox1.EditValue then
        begin
          bDrobn:=False;
          rMod:=Frac(quCurSpecQUANTITY.AsFloat);
          if abs(rMod)>0.00001 then bDrobn:=True; //���� ������� ������� �����
          if FindCardT(quCurSpecSIFR.AsInteger)>0 then bDrobn:=True; //� ����� ����� ����������� �����
          if quCurSpecQUANTITY.AsFloat<1 then rQ1:=quCurSpecQUANTITY.AsFloat else rQ1:=1;
   //       rQ1:=rQ1; //������� ����������
          rQ2:=quCurSpecQUANTITY.AsFloat-rQ1; //������� ��������
          if bDrobn then
          begin
            fmCalc.CalcEdit1.EditValue:=rQ1;
            fmCalc.ShowModal;
            if (fmCalc.ModalResult=mrOk)and(fmCalc.CalcEdit1.EditValue>0.00001)  then
            begin
              rQ1:=RoundEx(fmCalc.CalcEdit1.EditValue*1000)/1000;
              if rQ1>quCurSpecQUANTITY.AsFloat then rQ1:=quCurSpecQUANTITY.AsFloat;
              rQ2:=quCurSpecQUANTITY.AsFloat-rQ1; //���-�� ������� �������� �� ������ �����
            end;
          end;

          if abs(rQ2)<0.00001 then
          begin //���������� ���
            quCurSpec.Edit;
            quCurSpecQUEST.AsInteger:=cxImageComboBox1.EditValue;
            quCurSpec.Post;
          end else //����� - ����� ��� ���������
          begin
            if quCurSpecQUANTITY.AsFloat<>0 then
            begin
              rSum1:=RoundVal(rQ2*quCurSpecSUMMA.AsFloat/quCurSpecQUANTITY.AsFloat);
              rPr:=quCurSpecPRICE.AsFloat;
              rSumD1:=RoundVal(rQ2*quCurSpecDSUM.AsFloat/quCurSpecQUANTITY.AsFloat);
              rSum2:=quCurSpecSUMMA.AsFloat-rSum1;
              rSumD2:=quCurSpecDSUM.AsFloat-rSumD1;

              iStatus:=quCurSpecIStatus.AsInteger;
              iSifr:=quCurSpecSifr.AsInteger;
              rDProc:=quCurSpecDProc.AsFloat;
              sName:=quCurSpecName.AsString;
              sCode:=quCurSpecCode.AsString;
              iLink:=quCurSpecLinkM.AsInteger;
              iLimitM:=quCurSpecLimitM.AsInteger;
              iStream:=quCurSpecStream.AsInteger;

              quCurSpec.Edit;
              quCurSpecQUANTITY.AsFloat:=rQ2;
              quCurSpecSUMMA.AsFloat:=rSum1;
              quCurSpecDSUM.AsFloat:=rSumD1;
              quCurSpec.Post;

              quCurSpec.Append;
              quCurSpecSTATION.AsInteger:=CommonSet.Station;
              quCurSpecID_TAB.AsInteger:=Tab.Id;
              quCurSpecId_Personal.AsInteger:=Person.Id;
              quCurSpecNumTable.AsString:=Tab.NumTable;
              quCurSpecId.AsInteger:=Check.Max+1;
              quCurSpecSifr.AsInteger:=iSifr;
              quCurSpecPrice.AsFloat:=rPr;
              quCurSpecQuantity.AsFloat:=rQ1;
              quCurSpecSumma.AsFloat:=rSum2;
              quCurSpecDProc.AsFloat:=rDProc;
              quCurSpecDSum.AsFloat:=rSumD2;
              quCurSpecIStatus.AsInteger:=iStatus;
              quCurSpecName.AsString:=sName;
              quCurSpecCode.AsString:=sCode;
              quCurSpecLinkM.AsInteger:=iLink;
              quCurSpecLimitM.AsInteger:=iLimitM;
              quCurSpecStream.AsInteger:=iStream;
              quCurSpecQUEST.AsInteger:=cxImageComboBox1.EditValue;
              quCurSpec.Post;
              inc(Check.Max);

            end;
          end;

          cxImageComboBox1.Tag:=1;
        end;
      end;
    end;
  end;
end;

end.
