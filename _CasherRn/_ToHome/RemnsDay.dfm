object fmRemnsSpeed: TfmRemnsSpeed
  Left = 283
  Top = 332
  Width = 803
  Height = 488
  Caption = #1054#1089#1090#1072#1090#1082#1080' '#1090#1086#1074#1072#1088#1085#1099#1093' '#1079#1072#1087#1072#1089#1086#1074
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 435
    Width = 795
    Height = 19
    Panels = <
      item
        Width = 300
      end
      item
        Width = 50
      end>
  end
  object GridRemnsSpeed: TcxGrid
    Left = 4
    Top = 48
    Width = 681
    Height = 261
    TabOrder = 1
    LookAndFeel.Kind = lfOffice11
    object ViewRemnsSpeed: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      OnSelectionChanged = ViewRemnsSpeedSelectionChanged
      DataController.DataSource = dsteRemnsSp
      DataController.Summary.DefaultGroupSummaryItems = <
        item
          Format = '0.000'
          Kind = skSum
          Position = spFooter
          FieldName = 'Quant'
          Column = ViewRemnsSpeedQuant
        end
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'RSum'
        end>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'Quant'
          Column = ViewRemnsSpeedQuant
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'RSum'
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.MultiSelect = True
      OptionsView.Footer = True
      OptionsView.GroupFooters = gfAlwaysVisible
      OptionsView.Indicator = True
      object ViewRemnsSpeedRecId: TcxGridDBColumn
        DataBinding.FieldName = 'RecId'
        Visible = False
        IsCaptionAssigned = True
      end
      object ViewRemnsSpeedArticul: TcxGridDBColumn
        Caption = #1050#1086#1076
        DataBinding.FieldName = 'Articul'
      end
      object ViewRemnsSpeedName: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'Name'
        Styles.Content = dmO.cxStyle25
        Width = 146
      end
      object ViewRemnsSpeedCType: TcxGridDBColumn
        Caption = #1058#1080#1087' '#1087#1086#1079#1080#1094#1080#1080
        DataBinding.FieldName = 'CType'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Items = <
          item
            Description = #1058#1086#1074#1072#1088
            ImageIndex = 0
            Value = 1
          end
          item
            Description = #1059#1089#1083#1091#1075#1080
            ImageIndex = 0
            Value = 2
          end
          item
            Description = #1040#1074#1072#1085#1089#1099
            ImageIndex = 0
            Value = 3
          end
          item
            Description = #1058#1072#1088#1072
            ImageIndex = 0
            Value = 4
          end>
        Styles.Content = dmO.cxStyle5
      end
      object ViewRemnsSpeedGr: TcxGridDBColumn
        Caption = #1043#1088#1091#1087#1087#1072
        DataBinding.FieldName = 'Gr'
        Width = 132
      end
      object ViewRemnsSpeedSGr: TcxGridDBColumn
        Caption = #1055#1086#1076#1075#1088#1091#1087#1087#1072
        DataBinding.FieldName = 'SGr'
        Width = 166
      end
      object ViewRemnsSpeedIdM: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1077#1076'.'#1080#1079#1084'.'
        DataBinding.FieldName = 'IdM'
      end
      object ViewRemnsSpeedSM: TcxGridDBColumn
        Caption = #1045#1076'. '#1080#1079#1084'.'
        DataBinding.FieldName = 'SM'
      end
      object ViewRemnsSpeedTCard: TcxGridDBColumn
        Caption = #1058#1058#1050
        DataBinding.FieldName = 'TCard'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Images = dmO.imState
        Properties.Items = <
          item
            ImageIndex = 11
            Value = 1
          end>
      end
      object ViewRemnsSpeedQuant: TcxGridDBColumn
        Caption = #1054#1089#1090#1072#1090#1086#1082
        DataBinding.FieldName = 'Quant'
        Styles.Content = dmO.cxStyle25
      end
      object ViewRemnsSpeedRealSpeed: TcxGridDBColumn
        Caption = #1057#1082#1086#1088#1086#1089#1090#1100' '#1087#1088#1086#1076#1072#1078
        DataBinding.FieldName = 'RealSpeed'
        Styles.Content = dmO.cxStyle25
      end
      object ViewRemnsSpeedQuantDay: TcxGridDBColumn
        Caption = #1054#1089#1090#1072#1090#1086#1082' '#1074' '#1076#1085#1103#1093
        DataBinding.FieldName = 'QuantDay'
        Styles.Content = dmO.cxStyle25
      end
    end
    object LevelRemnsSpeed: TcxGridLevel
      GridView = ViewRemnsSpeed
    end
  end
  object SpeedBar1: TSpeedBar
    Left = 0
    Top = 0
    Width = 795
    Height = 48
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Options = [sbAllowDrag, sbFlatBtns, sbTransparentBtns]
    BtnOffsetHorz = 4
    BtnOffsetVert = 4
    BtnWidth = 60
    BtnHeight = 40
    BevelInner = bvLowered
    Color = 13224393
    TabOrder = 2
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object SpeedItem1: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = 'SpeedItem1'
      Glyph.Data = {
        5E040000424D5E04000000000000360000002800000012000000130000000100
        18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
        8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
        0000CED3D6848684848684848684848684848684848684848684848684848684
        848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
        7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
        FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
        FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
        007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
        FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
        00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
        494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
        00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000}
      Hint = 'SpeedItem1|'
      Spacing = 1
      Left = 424
      Top = 4
      Visible = True
      OnClick = SpeedItem1Click
      SectionName = 'Untitled (0)'
    end
    object SpeedItem3: TSpeedItem
      Action = acExportEx
      Glyph.Data = {
        56080000424D560800000000000036000000280000001A0000001A0000000100
        18000000000020080000C40E0000C40E00000000000000000000C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0004000004000004000004000C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C00000C0C0C0004000
        80808080808080808080808080808080808080808000400040E02040E020FFFF
        FFC8D0D4A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0808080C0
        C0C0C0C0C0C0C0C00000C0C0C0C0C0C000400040E02000C02000C02000C02000
        C02000400040E02040E020FFFFFF004000C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4A4A0A0A4A0A0404040C0C0C0C0C0C00000C0C0C0C0C0C0
        C0C0C000400000C02000C02000C02000400000C02040E020FFFFFF00400000C0
        20C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4A4A0A0A4A0A040
        2020808080C0C0C00000C0C0C0C0C0C0C0C0C0A4A0A000400000C02000400000
        C02040E020FFFFFF004000004000004000C8D0D4A4A0A0A4A0A0A4A0A0A4A0A0
        A4A0A0A4A0A0808080C8D0D4808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C000400040E02040E020FFFFFF004000F0FBFFF0FBFFF0FB
        FFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFA4A0A0C8D0D4C8D0D440
        2020808080C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C000400040E02040E020FF
        FFFF004000808080004000C8D0D4C0DCC0C0DCC0C0DCC0C0DCC0C0DCC0C0DCC0
        C0DCC0C8D0D4F0FBFFA4A0A0C8D0D4402020808080C0C0C00000C0C0C0C0C0C0
        C0C0C000400000C02040E020FFFFFF00400080E0A000C020808080004000FFFF
        FFFFFFFFFFFFFFF0FBFFC0DCC0C0DCC0F0FBFFC0DCC0F0FBFF808080C8D0D440
        2020808080C0C0C00000C0C0C0C0C0C000400000C02040E020FFFFFF004000FF
        FFFF00400000C02000C020808080004000FFFFFFFFFFFFC8D0D4F0FBFFC0DCC0
        C0DCC0C0DCC0F0FBFFC0DCC0A4A0A0404020808080C0C0C00000C0C0C0004000
        004000004000004000FFFFFFFFFFFFFFFFFFFFFFFF0040000040000040000040
        00C8D0D4F0FBFFC0DCC0C8D0D4C8D0D4F0FBFFC0DCC0F0FBFFFFFFFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFC0DCC0FFFFFFF0FBFFFFFFFFFFFFFFF0FBFFF0FBFFF0FBFF
        FFFFFFC0DCC0F0FBFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0F0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FB
        FFF0FBFFFFFFFFFFFFFFF0FBFFF0FBFFFFFFFFC0DCC0F0FBFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0F0
        FBFFF0FBFFF0FBFFC0DCC0C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4F0FBFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0C0DCC0C0DCC0F0FBFFF0FBFFC0DCC0FFFF
        FFC0DCC0F0FBFFC0DCC0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0C0
        DCC0F0FBFFF0FBFFF0FBFFF0FBFFFFFFFFF0FBFFF0FBFFC0DCC0F0FBFFFFFFFF
        FFFFFFFFFFFFFFFFFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0C0DCC0C8D0D4F0FBFFF0FBFFC8D0D4FFFF
        FFC8D0D4F0FBFFC0DCC0F0FBFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0F0
        FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFC8D0D4F0FBFFFFFFFF
        FFFFFFFFFFFFFFFFFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFF0FBFFC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4C8D0D4C8D0D4C8D0D4F0FBFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFF0FBFFF0
        FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080C0DCC0C8D0D4C8D0D4F0FBFFC8D0D4C0DCC0C0DCC0C8D0D4F0FB
        FFC8D0D4C0DCC0F0FBFFC8D0D4F0FBFFC8D0D4C8D0D4F0FBFFA4A0A080808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C080808080E0E040E0E040E0E080
        E0E040E0E080E0E080E0E040E0E080E0E040E0E040E0E080E0E040E0E080E0E0
        40E0E040E0E080E0E000E0E0408080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C080808080E0E080E0E0C0DCC080E0E0C0DCC080E0E080E0E0C0DCC080E0
        E0C0DCC080E0E080E0E0C0DCC080E0E0C0DCC080E0E080E0E0C0DCC000808000
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080F0FBFFF0FBFFF0FBFF80
        E0E0F0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFF
        F0FBFFF0FBFFF0FBFFF0FBFF00C0C0002040808080C0C0C00000C0C0C0C0C0C0
        C0C0C08080808080808080808080808080808080808080808080808080808080
        8080808080808080808080808080808080808080808080808080808080808000
        2040C0C0C0C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C00000}
      Hint = #1069#1082#1089#1087#1086#1088#1090' '#1074' Excel'
      Spacing = 1
      Left = 24
      Top = 4
      Visible = True
      OnClick = acExportExExecute
      SectionName = 'Untitled (0)'
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 312
    Width = 795
    Height = 123
    Align = alBottom
    BevelInner = bvLowered
    Color = 13224393
    TabOrder = 3
    object Memo1: TcxMemo
      Left = 2
      Top = 2
      Align = alLeft
      Lines.Strings = (
        'Memo1')
      Style.BorderStyle = ebsOffice11
      TabOrder = 0
      Height = 119
      Width = 231
    end
    object PBar1: TcxProgressBar
      Left = 100
      Top = 96
      ParentColor = False
      Position = 100.000000000000000000
      Properties.BarBevelOuter = cxbvRaised
      Properties.BarStyle = cxbsGradient
      Properties.BeginColor = clBlue
      Properties.EndColor = 16745090
      Properties.PeakValue = 100.000000000000000000
      Style.Color = clWhite
      TabOrder = 1
      Visible = False
      Width = 129
    end
    object GrLastCli: TcxGrid
      Left = 352
      Top = 2
      Width = 441
      Height = 119
      Align = alRight
      TabOrder = 2
      LookAndFeel.Kind = lfOffice11
      object ViewLastCli: TcxGridDBTableView
        NavigatorButtons.ConfirmDelete = False
        DataController.DataSource = dmO.dsquLastCli
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsCustomize.ColumnFiltering = False
        OptionsCustomize.ColumnGrouping = False
        OptionsCustomize.ColumnHidingOnGrouping = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.GroupByBox = False
        object ViewLastCliDATEDOC: TcxGridDBColumn
          Caption = #1044#1072#1090#1072' '#1076#1086#1082'.'
          DataBinding.FieldName = 'DATEDOC'
          Width = 59
        end
        object ViewLastCliNUMDOC: TcxGridDBColumn
          Caption = #8470' '#1076#1086#1082'.'
          DataBinding.FieldName = 'NUMDOC'
          Width = 56
        end
        object ViewLastCliNAMECL: TcxGridDBColumn
          Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
          DataBinding.FieldName = 'NAMECL'
          Styles.Content = dmO.cxStyle25
          Width = 116
        end
        object ViewLastCliQUANT: TcxGridDBColumn
          Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086
          DataBinding.FieldName = 'QUANT'
          Width = 47
        end
        object ViewLastCliNAMESHORT: TcxGridDBColumn
          Caption = #1045#1076'.'#1080#1079#1084'.'
          DataBinding.FieldName = 'NAMESHORT'
          Width = 32
        end
        object ViewLastCliPRICEIN: TcxGridDBColumn
          Caption = #1062#1077#1085#1072' '#1079#1072#1082#1091#1087#1072
          DataBinding.FieldName = 'PRICEIN'
          Styles.Content = dmO.cxStyle25
          Width = 53
        end
        object ViewLastCliSUMIN: TcxGridDBColumn
          Caption = #1057#1091#1084#1084#1072' '#1079#1072#1082#1091#1087#1072
          DataBinding.FieldName = 'SUMIN'
          Styles.Content = dmO.cxStyle25
          Width = 54
        end
      end
      object LeLastCli: TcxGridLevel
        GridView = ViewLastCli
      end
    end
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 3000
    OnTimer = Timer1Timer
    Left = 352
    Top = 184
  end
  object FormPlacement1: TFormPlacement
    Active = False
    Left = 432
    Top = 184
  end
  object amRemnsSpeed: TActionManager
    Left = 92
    Top = 192
    StyleName = 'XP Style'
    object acExit: TAction
      Caption = 'acExit'
      OnExecute = acExitExecute
    end
    object acPerSkl: TAction
      Caption = #1055#1077#1088#1080#1086#1076
    end
    object acExportEx: TAction
      OnExecute = acExportExExecute
    end
    object acMovePos: TAction
      Caption = #1044#1074#1080#1078#1077#1085#1080#1077' '#1087#1086' '#1090#1086#1074#1072#1088#1091
      ShortCut = 32885
      OnExecute = acMovePosExecute
    end
  end
  object teRemnsSp: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 188
    Top = 152
    object teRemnsSpArticul: TIntegerField
      FieldName = 'Articul'
    end
    object teRemnsSpName: TStringField
      FieldName = 'Name'
      Size = 100
    end
    object teRemnsSpGr: TStringField
      FieldName = 'Gr'
      Size = 50
    end
    object teRemnsSpSGr: TStringField
      FieldName = 'SGr'
      Size = 50
    end
    object teRemnsSpIdM: TIntegerField
      FieldName = 'IdM'
    end
    object teRemnsSpKM: TFloatField
      FieldName = 'KM'
    end
    object teRemnsSpSM: TStringField
      FieldName = 'SM'
      Size = 10
    end
    object teRemnsSpTCard: TIntegerField
      FieldName = 'TCard'
    end
    object teRemnsSpQuant: TFloatField
      FieldName = 'Quant'
      DisplayFormat = '0.000'
    end
    object teRemnsSpCType: TSmallintField
      FieldName = 'CType'
    end
    object teRemnsSpRealSpeed: TFloatField
      FieldName = 'RealSpeed'
      DisplayFormat = '0.000'
    end
    object teRemnsSpQuantDay: TFloatField
      FieldName = 'QuantDay'
      DisplayFormat = '0.0'
    end
  end
  object dsteRemnsSp: TDataSource
    DataSet = teRemnsSp
    Left = 260
    Top = 152
  end
end
