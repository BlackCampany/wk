unit DocsOut;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SpeedBar, ExtCtrls, ComCtrls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Placemnt, cxImageComboBox, XPStyleActnCtrls, ActnList, ActnMan, Menus,
  cxContainer, cxTextEdit, cxMemo, FR_DSet, FR_DBSet, FR_Class;

type
  TfmDocsOut = class(TForm)
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    Timer1: TTimer;
    FormPlacement1: TFormPlacement;
    GridDocsOut: TcxGrid;
    ViewDocsOut: TcxGridDBTableView;
    LevelDocsOut: TcxGridLevel;
    ViewDocsOutID: TcxGridDBColumn;
    ViewDocsOutDATEDOC: TcxGridDBColumn;
    ViewDocsOutNUMDOC: TcxGridDBColumn;
    ViewDocsOutDATESF: TcxGridDBColumn;
    ViewDocsOutNUMSF: TcxGridDBColumn;
    ViewDocsOutIDCLI: TcxGridDBColumn;
    ViewDocsOutIDSKL: TcxGridDBColumn;
    ViewDocsOutSUMIN: TcxGridDBColumn;
    ViewDocsOutSUMUCH: TcxGridDBColumn;
    ViewDocsOutSUMTAR: TcxGridDBColumn;
    ViewDocsOutSUMNDS0: TcxGridDBColumn;
    ViewDocsOutSUMNDS1: TcxGridDBColumn;
    ViewDocsOutSUMNDS2: TcxGridDBColumn;
    ViewDocsOutPROCNAC: TcxGridDBColumn;
    ViewDocsOutNAMECL: TcxGridDBColumn;
    ViewDocsOutNAMEMH: TcxGridDBColumn;
    ViewDocsOutIACTIVE: TcxGridDBColumn;
    amDocsOut: TActionManager;
    acPeriod: TAction;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    SpeedItem6: TSpeedItem;
    acAddDoc1: TAction;
    acEditDoc1: TAction;
    acViewDoc1: TAction;
    acDelDoc1: TAction;
    acOnDoc1: TAction;
    acOffDoc1: TAction;
    SpeedItem7: TSpeedItem;
    SpeedItem8: TSpeedItem;
    PopupMenu1: TPopupMenu;
    acCopy: TAction;
    acPost: TAction;
    N1: TMenuItem;
    N2: TMenuItem;
    Memo1: TcxMemo;
    N3: TMenuItem;
    Excel1: TMenuItem;
    acPrintDO: TAction;
    frRepDocsOD: TfrReport;
    frquSpecOutSel: TfrDBDataSet;
    N4: TMenuItem;
    acPrintSCHF: TAction;
    N5: TMenuItem;
    PopupMenu2: TPopupMenu;
    N6: TMenuItem;
    acExit: TAction;
    procedure SpeedItem1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acPeriodExecute(Sender: TObject);
    procedure acAddDoc1Execute(Sender: TObject);
    procedure acEditDoc1Execute(Sender: TObject);
    procedure acViewDoc1Execute(Sender: TObject);
    procedure ViewDocsOutDblClick(Sender: TObject);
    procedure acDelDoc1Execute(Sender: TObject);
    procedure acOnDoc1Execute(Sender: TObject);
    procedure acOffDoc1Execute(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure acCopyExecute(Sender: TObject);
    procedure acPostExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Excel1Click(Sender: TObject);
    procedure acPrintDOExecute(Sender: TObject);
    procedure acPrintSCHFExecute(Sender: TObject);
    procedure ViewDocsOutCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure N6Click(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure prButtonSet(bSet:Boolean);
    Procedure prOn(IDH,IdSkl,IDate,iCli:Integer; Var rSumO,rSumT:Real);
  end;

var
  fmDocsOut: TfmDocsOut;
  bClearDocOut:Boolean = false;

implementation

uses Un1, dmOffice, PeriodUni, AddDoc2, SelPartIn1, DMOReps, TBuff,
  MainRnOffice;

{$R *.dfm}

Procedure TfmDocsOut.prOn(IDH,IdSkl,IDate,iCli:Integer; Var rSumO,rSumT:Real);
Var rQP,rQs,PriceSp,PriceSp0,k,rQ,PriceSpOut,rSum:Real;
    iSS:INteger;
begin
  with dmO do
  with dmORep do
  begin
    CloseTa(taPartTest);

    iSS:=prISS(IdSkl);

    quSpecOutSel.Active:=False;
    quSpecOutSel.ParamByName('IDHD').AsInteger:=IDH;
    quSpecOutSel.Active:=True;

    rSumO:=0;   rSumT:=0;

    quSpecOutSel.First;
    while not quSpecOutSel.Eof do
    begin
      k:=prFindKM(quSpecOutSelIDM.AsInteger);
      rQs:=quSpecOutSelQUANT.AsFloat*k; //20*0,5=10

      if abs(quSpecOutSelQUANT.AsFloat)>0 then
      begin
        PriceSp0:=(quSpecOutSelSUMIN0.AsFloat/quSpecOutSelQUANT.AsFloat)/k; //20/0,5=40
        PriceSp:=(quSpecOutSelSUMIN.AsFloat/quSpecOutSelQUANT.AsFloat)/k; //20/0,5=40
      end else
      begin
        PriceSp0:=quSpecOutSelPRICEIN0.AsFloat/k; //20/0,5=40
        PriceSp:=quSpecOutSelPRICEIN.AsFloat/k; //20/0,5=40
      end;

      PriceSpOut:=quSpecOutSelPRICEUCH.AsFloat/k; //20/0,5=40
      rSum:=0;

      prSelPartIn(quSpecOutSelIDCARD.AsInteger,IdSkl,iCli,0);

      quSelPartIn.First;
      while (not quSelPartIn.Eof) and (rQs>0) do
      begin
        {��� ���� �� �����������, ����� �� ��������, �.�. ��������� ��� �������
        ��� ������ ����, ��������� ������� � ��������� ���������� �� �����
        ��������������� ������ ������ ������ ����.
        ���� ��� �� � ��������� ������ �������� ���.
        }
        if RoundVal(PriceSp)=RoundVal(quSelPartInPRICEIN.AsFloat) then
        begin
          rQp:=quSelPartInQREMN.AsFloat;
          if rQs<=rQp then  rQ:=rQs//��������� ������ ������ ���������
                      else  rQ:=rQp;
          rQs:=rQs-rQ;
          //��������� ��������� ������
          //����������� ��������� ������

          prAddPartOut.ParamByName('ARTICUL').AsInteger:=quSpecOutSelIDCARD.AsInteger;
          prAddPartOut.ParamByName('IDDATE').AsInteger:=iDate;
          prAddPartOut.ParamByName('IDSTORE').AsInteger:=IdSkl;
          prAddPartOut.ParamByName('IDPARTIN').AsInteger:=quSelPartInID.AsInteger;
          prAddPartOut.ParamByName('IDDOC').AsInteger:=IDH;
          if iCli>0 then
            prAddPartOut.ParamByName('IDCLI').AsInteger:=iCli
          else
            prAddPartOut.ParamByName('IDCLI').AsInteger:=quSelPartInIDCLI.AsInteger;

          prAddPartOut.ParamByName('DTYPE').AsInteger:=7; //�������� ���� ��� 1 �.�. ������ � ������� ������������ ���� �� �����
          prAddPartOut.ParamByName('QUANT').AsFloat:=rQ;

          if iSS<>2 then
          begin
            prAddPartOut.ParamByName('PRICEIN').AsFloat:=PriceSp;
            rSum:=rSum+(PriceSp*rQ);
          end else
          begin
            prAddPartOut.ParamByName('PRICEIN').AsFloat:=PriceSp0;
            rSum:=rSum+(PriceSp0*rQ);
          end;

          prAddPartOut.ParamByName('SUMOUT').AsFloat:=PriceSpOut*rQ;
          prAddPartout.ExecProc;
        end;
        quSelPartIn.Next;
      end;
      quSelPartIn.Active:=False;

      //��������� ��������
      if rQs>0 then //�������� ������������� ������
      begin
        //��������� ��� ����� ���������
        prAddPartOut.ParamByName('ARTICUL').AsInteger:=quSpecOutSelIDCARD.AsInteger;
        prAddPartOut.ParamByName('IDDATE').AsInteger:=iDate;
        prAddPartOut.ParamByName('IDSTORE').AsInteger:=IdSkl;
        prAddPartOut.ParamByName('IDPARTIN').AsInteger:=-1;
        prAddPartOut.ParamByName('IDDOC').AsInteger:=IdH;
        if iCli>0 then
          prAddPartOut.ParamByName('IDCLI').AsInteger:=iCli
        else
          prAddPartOut.ParamByName('IDCLI').AsInteger:=0;
        prAddPartOut.ParamByName('DTYPE').AsInteger:=7;  //�������� ���� ��� 1 �.�. ������ � ������� ������������ ���� �� �����
        prAddPartOut.ParamByName('QUANT').AsFloat:=rQs;

        if iSS<>2 then
        begin
          prAddPartOut.ParamByName('PRICEIN').AsFloat:=PriceSp;
          rSum:=rSum+(PriceSp*rQs);
        end else
        begin
          prAddPartOut.ParamByName('PRICEIN').AsFloat:=PriceSp0;
          rSum:=rSum+(PriceSp0*rQs);
        end;

//        prAddPartOut.ParamByName('PRICEIN').AsFloat:=PriceSp;

        prAddPartOut.ParamByName('SUMOUT').AsFloat:=PriceSpOut*rQs;
        prAddPartout.ExecProc;

//        rSum:=rSum+RoundVal(PriceSp*rQs);

        taPartTest.Append;
        taPartTestNum.AsInteger:=quSpecOutSelNUM.AsInteger;
        taPartTestIdGoods.AsInteger:=quSpecOutSelIDCARD.AsInteger;
        taPartTestNameG.AsString:=quSpecOutSelNAMEC.AsString;
        taPartTestIM.AsInteger:=quSpecOutSelIM.AsInteger;
        taPartTestSM.AsString:=quSpecOutSelSM.AsString;
        taPartTestQuant.AsFloat:=quSpecOutSelQUANT.AsFloat;
        taPartTestPrice1.AsFloat:=quSpecOutSelPRICEIN.AsFloat;
        taPartTestiRes.AsInteger:=0;
        taPartTest.Post;

      end else
      begin
        taPartTest.Append;
        taPartTestNum.AsInteger:=quSpecOutSelNUM.AsInteger;
        taPartTestIdGoods.AsInteger:=quSpecOutSelIDCARD.AsInteger;
        taPartTestNameG.AsString:=quSpecOutSelNAMEC.AsString;
        taPartTestIM.AsInteger:=quSpecOutSelIM.AsInteger;
        taPartTestSM.AsString:=quSpecOutSelSM.AsString;
        taPartTestQuant.AsFloat:=quSpecOutSelQUANT.AsFloat;
        taPartTestPrice1.AsFloat:=quSpecOutSelPRICEIN.AsFloat;
        taPartTestiRes.AsInteger:=1;
        taPartTest.Post;
      end;

      if quSpecOutSelCATEGORY.AsInteger=4 then rSumT:=rSumT+rv(rSum) else rSumO:=rSumO+rv(rSum);

      quSpecOutSel.Next;
    end;
    quSpecOutSel.Active:=False;
  end;
end;

procedure TfmDocsOut.prButtonSet(bSet:Boolean);
begin
  SpeedItem1.Enabled:=bSet;
  SpeedItem2.Enabled:=bSet;
  SpeedItem3.Enabled:=bSet;
  SpeedItem4.Enabled:=bSet;
  SpeedItem5.Enabled:=bSet;
  SpeedItem6.Enabled:=bSet;
  SpeedItem7.Enabled:=bSet;
  SpeedItem8.Enabled:=bSet;
  delay(100);
end;


procedure TfmDocsOut.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmDocsOut.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  Timer1.Enabled:=True;
  GridDocsOut.Align:=AlClient;
  ViewDocsOut.RestoreFromIniFile(CurDir+GridIni);
  //���� ����� ������� �������� ������ ���
  with dmO do
  begin
    if taNDS.Active=False then taNDS.Active:=True;
    taNDS.First;
    while not taNDS.Eof do
    begin
      if taNDSID.AsInteger=1 then ViewDocsOutSUMNDS0.Caption:=taNDSNAMENDS.AsString;
      if taNDSID.AsInteger=2 then ViewDocsOutSUMNDS1.Caption:=taNDSNAMENDS.AsString;
      if taNDSID.AsInteger=3 then ViewDocsOutSUMNDS2.Caption:=taNDSNAMENDS.AsString;
      taNDS.Next;
    end;
  end;
  StatusBar1.Color:= UserColor.TTnOut;
  SpeedBar1.Color := UserColor.TTnOut;
end;

procedure TfmDocsOut.FormClose(Sender: TObject; var Action: TCloseAction);
begin
   ViewDocsOut.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmDocsOut.acPeriodExecute(Sender: TObject);
begin
//  ������
  fmPeriodUni.DateTimePicker1.Date:=CommonSet.DateFrom;
//  fmPeriodUni.DateTimePicker2.Date:=CommonSet.DateTo-1;
  fmPeriodUni.ShowModal;
  if fmPeriodUni.ModalResult=mrOk then
  begin
    CommonSet.DateFrom:=Trunc(fmPeriodUni.DateTimePicker1.Date);
    CommonSet.DateTo:=Trunc(fmPeriodUni.DateTimePicker2.Date)+1;

    if CommonSet.DateTo>=iMaxDate then fmDocsOut.Caption:='��������� ��������� � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)
    else fmDocsOut.Caption:='��������� ��������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateFrom)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateTo-1);
    with dmO do
    begin
      ViewDocsOut.BeginUpdate;
      quDocsOutSel.Active:=False;
      quDocsOutSel.ParamByName('DATEB').AsDate:=CommonSet.DateFrom;
      quDocsOutSel.ParamByName('DATEE').AsDate:=CommonSet.DateTo;
      quDocsOutSel.ParamByName('IDPERSON').AsInteger:=Person.Id;
      quDocsOutSel.Active:=True;
      ViewDocsOut.EndUpdate;
    end;
  end;
end;

procedure TfmDocsOut.acAddDoc1Execute(Sender: TObject);
begin
  //�������� ��������

  if not CanDo('prAddDocOut') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  if fmAddDoc2.Visible then if MessageDlg('������� �������� ��������?',mtConfirmation, [mbYes, mbNo], 0) <> mrYes then exit;
  //��� ���������
  with dmO do
  begin
    fmAddDoc2.Caption:='���������: ����� ��������.';
    fmAddDoc2.cxTextEdit1.Tag:=0;
    fmAddDoc2.cxTextEdit1.Text:=prGetNum(7,0);
    fmAddDoc2.cxTextEdit1.Properties.ReadOnly:=False;
    fmAddDoc2.cxTextEdit2.Text:='';
    fmAddDoc2.cxTextEdit2.Properties.ReadOnly:=False;
    fmAddDoc2.cxDateEdit1.Date:=Date;
    fmAddDoc2.cxDateEdit1.Properties.ReadOnly:=False;
    fmAddDoc2.cxDateEdit2.Date:=Date;
    fmAddDoc2.cxDateEdit2.Properties.ReadOnly:=False;

    fmAddDoc2.cxButtonEdit1.Tag:=0;
    fmAddDoc2.cxButtonEdit1.EditValue:=0;
    fmAddDoc2.cxButtonEdit1.Text:='';
    fmAddDoc2.cxButtonEdit1.Properties.ReadOnly:=False;

    fmAddDoc2.cxButtonEdit2.Tag:=0;
    fmAddDoc2.cxButtonEdit2.EditValue:=0;
    fmAddDoc2.cxButtonEdit2.Text:='';
    fmAddDoc2.cxButtonEdit2.Properties.ReadOnly:=False;

    if quMHAll.Active=False then
    begin
       quMHAll.ParamByName('IDPERSON').AsInteger:=Person.Id;
       quMHAll.Active:=True;
    end;
    quMHAll.FullRefresh;

    fmAddDoc2.cxLookupComboBox1.EditValue:=0;
    fmAddDoc2.cxLookupComboBox1.Text:='';
    fmAddDoc2.cxLookupComboBox1.Properties.ReadOnly:=False;

    if CurVal.IdMH<>0 then
    begin
      fmAddDoc2.cxLookupComboBox1.EditValue:=CurVal.IdMH;
      fmAddDoc2.cxLookupComboBox1.Text:=CurVal.NAMEMH;
    end else
    begin
       quMHAll.First;
       if not quMHAll.Eof then
       begin
         CurVal.IdMH:=quMHAllID.AsInteger;
         CurVal.NAMEMH:=quMHAllNAMEMH.AsString;
         fmAddDoc2.cxLookupComboBox1.EditValue:=CurVal.IdMH;
         fmAddDoc2.cxLookupComboBox1.Text:=CurVal.NAMEMH;
       end;
    end;
    if quMHAll.Locate('ID',CurVal.IdMH,[]) then
    begin
      fmAddDoc2.Label15.Caption:='��. ����: '+quMHAllNAMEPRICE.AsString;
      fmAddDoc2.Label15.Tag:=quMHAllDEFPRICE.AsInteger;
    end else
    begin
      fmAddDoc2.Label15.Caption:='��. ����: ';
      fmAddDoc2.Label15.Tag:=0;
    end;

    fmAddDoc2.cxLabel1.Enabled:=True;
    fmAddDoc2.cxLabel2.Enabled:=True;
    fmAddDoc2.cxLabel3.Enabled:=True;
    fmAddDoc2.cxLabel4.Enabled:=True;
    fmAddDoc2.cxLabel5.Enabled:=True;
    fmAddDoc2.cxLabel6.Enabled:=True;
    fmAddDoc2.cxLabel7.Enabled:=True;
    fmAddDoc2.cxLabel8.Enabled:=True;

    fmAddDoc2.ViewDoc2.OptionsData.Editing:=True;
    fmAddDoc2.ViewDoc2.OptionsData.Deleting:=True;

    fmAddDoc2.cxButton1.Enabled:=True;

    CloseTe(fmAddDoc2.taSpecOut);

    fmAddDoc2.acSave.Enabled:=True;

    fmAddDoc2.Show;
  end;
end;

procedure TfmDocsOut.acEditDoc1Execute(Sender: TObject);
Var IDH:INteger;
begin
  //�������������
  if not CanDo('prEditDocOut') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  if fmAddDoc2.Visible then if MessageDlg('������� �������� ��������?',mtConfirmation, [mbYes, mbNo], 0) <> mrYes then exit;
  //��� ���������
  with dmO do
  with dmORep do
  begin
    if quDocsOutSel.RecordCount>0 then //���� ��� �������������
    begin
      if quDocsOutSelIACTIVE.AsInteger=0 then
      begin

        prAllViewOff;

        fmAddDoc2.Caption:='���������: ��������������.';
        fmAddDoc2.cxTextEdit1.Tag:=quDocsOutSelId.AsInteger;

        fmAddDoc2.cxTextEdit1.Text:=quDocsOutSelNUMDOC.AsString;
        fmAddDoc2.cxTextEdit1.Properties.ReadOnly:=False;
        fmAddDoc2.cxTextEdit2.Text:=quDocsOutSelNUMSF.AsString;
        fmAddDoc2.cxTextEdit2.Properties.ReadOnly:=False;
        fmAddDoc2.cxDateEdit1.Date:=quDocsOutSelDATEDOC.AsDateTime;
        fmAddDoc2.cxDateEdit1.Properties.ReadOnly:=False;
        fmAddDoc2.cxDateEdit2.Date:=quDocsOutSelDATESF.AsDateTime;
        fmAddDoc2.cxDateEdit2.Properties.ReadOnly:=False;

        fmAddDoc2.cxButtonEdit1.Tag:=quDocsOutSelIDCLI.AsInteger;
        fmAddDoc2.cxButtonEdit1.EditValue:=quDocsOutSelIDCLI.AsInteger;
        fmAddDoc2.cxButtonEdit1.Text:=quDocsOutSelNAMECL.AsString;
        fmAddDoc2.cxButtonEdit1.Properties.ReadOnly:=False;

        if quDocsOutSelIDFROM.AsInteger>0 then
        begin
          fmAddDoc2.cxButtonEdit2.Tag:=quDocsOutSelIDFROM.AsInteger;
          fmAddDoc2.cxButtonEdit2.EditValue:=quDocsOutSelIDFROM.AsInteger;
          fmAddDoc2.cxButtonEdit2.Text:=quDocsOutSelNAMECL1.AsString;
          fmAddDoc2.cxButtonEdit2.Properties.ReadOnly:=False;
        end else
        begin
          fmAddDoc2.cxButtonEdit2.Tag:=0;
          fmAddDoc2.cxButtonEdit2.EditValue:=0;
          fmAddDoc2.cxButtonEdit2.Text:='';
          fmAddDoc2.cxButtonEdit2.Properties.ReadOnly:=False;
        end;

        if quMHAll.Active=False then
        begin
          quMHAll.ParamByName('IDPERSON').AsInteger:=Person.Id;
          quMHAll.Active:=True;
        end;
        quMHAll.FullRefresh;

        fmAddDoc2.cxLookupComboBox1.EditValue:=quDocsOutSelIDSKL.AsInteger;
        fmAddDoc2.cxLookupComboBox1.Text:=quDocsOutSelNAMEMH.AsString;
        fmAddDoc2.cxLookupComboBox1.Properties.ReadOnly:=False;

        CurVal.IdMH:=quDocsOutSelIDSKL.AsInteger;
        CurVal.NAMEMH:=quDocsOutSelNAMEMH.AsString;

        if quMHAll.Locate('ID',CurVal.IdMH,[]) then
        begin
          fmAddDoc2.Label15.Caption:='��. ����: '+quMHAllNAMEPRICE.AsString;
          fmAddDoc2.Label15.Tag:=quMHAllDEFPRICE.AsInteger;
        end else
        begin
          fmAddDoc2.Label15.Caption:='��. ����: ';
          fmAddDoc2.Label15.Tag:=0;
        end;

        fmAddDoc2.cxLabel1.Enabled:=True;
        fmAddDoc2.cxLabel2.Enabled:=True;
        fmAddDoc2.cxLabel3.Enabled:=True;
        fmAddDoc2.cxLabel4.Enabled:=True;
        fmAddDoc2.cxLabel5.Enabled:=True;
        fmAddDoc2.cxLabel6.Enabled:=True;
        fmAddDoc2.cxLabel7.Enabled:=True;
        fmAddDoc2.cxLabel8.Enabled:=True;

        fmAddDoc2.cxButton1.Enabled:=True;

        fmAddDoc2.ViewDoc2.OptionsData.Editing:=True;
        fmAddDoc2.ViewDoc2.OptionsData.Deleting:=True;

        CloseTe(fmAddDoc2.taSpecOut);

        fmAddDoc2.acSave.Enabled:=True;

        IDH:=quDocsOutSelID.AsInteger;

        quSpecOutSel.Active:=False;
        quSpecOutSel.ParamByName('IDHD').AsInteger:=IDH;
        quSpecOutSel.Active:=True;

        quSpecOutSel.First;
        while not quSpecOutSel.Eof do
        begin
          with fmAddDoc2 do
          begin
            taSpecOut.Append;
            taSpecOutNum.AsInteger:=quSpecOutSelNUM.AsInteger;
            taSpecOutIdGoods.AsInteger:=quSpecOutSelIDCARD.AsInteger;
            taSpecOutNameG.AsString:=quSpecOutSelNAMEC.AsString;
            taSpecOutIM.AsInteger:=quSpecOutSelIDM.AsInteger;
            taSpecOutSM.AsString:=quSpecOutSelSM.AsString;
            taSpecOutQuant.AsFloat:=quSpecOutSelQUANT.AsFloat;
            taSpecOutPrice0.AsFloat:=quSpecOutSelPRICEIN0.AsFloat;
            taSpecOutSum0.AsFloat:=quSpecOutSelSUMIN0.AsFloat;
            taSpecOutPrice1.AsFloat:=quSpecOutSelPRICEIN.AsFloat;
            taSpecOutSum1.AsFloat:=quSpecOutSelSUMIN.AsFloat;
            taSpecOutPrice2.AsFloat:=quSpecOutSelPRICEUCH.AsFloat;
            taSpecOutSum2.AsFloat:=quSpecOutSelSUMUCH.AsFloat;
            taSpecOutINds.AsInteger:=quSpecOutSelIDNDS.AsInteger;
            taSpecOutSNds.AsString:=quSpecOutSelNAMENDS.AsString;
            taSpecOutRNds.AsFloat:=quSpecOutSelSUMNDS.AsFloat;
            taSpecOutSumNac.AsFloat:=quSpecOutSelSUMUCH.AsFloat-quSpecOutSelSUMIN.AsFloat;
            taSpecOutProcNac.AsFloat:=0;
            if quSpecOutSelSUMIN.AsFloat<>0 then
              taSpecOutProcNac.AsFloat:=RoundEx((quSpecOutSelSUMUCH.AsFloat-quSpecOutSelSUMIN.AsFloat)/quSpecOutSelSUMIN.AsFloat*10000)/100;
            taSpecOutCType.AsInteger:=quSpecOutSelCATEGORY.AsInteger;
            taSpecOut.Post;
          end;
          quSpecOutSel.Next;
        end;

        quSpecOutSel.Active:=False;

        prAllViewOn;

        fmAddDoc2.Show;
      end else
      begin
        showmessage('������������� �������������� �������� ������.');
      end;
    end else
    begin
      showmessage('�������� �������� ��� ��������������.');
    end;
  end;
end;

procedure TfmDocsOut.acViewDoc1Execute(Sender: TObject);
Var IDH:INteger;
begin
  //��������
  if not CanDo('prViewDocOut') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  if fmAddDoc2.Visible then if MessageDlg('������� �������� ��������?',mtConfirmation, [mbYes, mbNo], 0) <> mrYes then exit;
  //��� ���������
  with dmO do
  with dmORep do
  begin
    if quDocsOutSel.RecordCount>0 then //���� ��� �������������
    begin
      prAllViewOff;

      fmAddDoc2.Caption:='���������: ��������.';
      fmAddDoc2.cxTextEdit1.Tag:=quDocsOutSelId.AsInteger;
      fmAddDoc2.cxTextEdit1.Text:=quDocsOutSelNUMDOC.AsString;
      fmAddDoc2.cxTextEdit1.Properties.ReadOnly:=True;
      fmAddDoc2.cxTextEdit2.Text:=quDocsOutSelNUMSF.AsString;
      fmAddDoc2.cxTextEdit2.Properties.ReadOnly:=True;
      fmAddDoc2.cxDateEdit1.Date:=quDocsOutSelDATEDOC.AsDateTime;
      fmAddDoc2.cxDateEdit1.Properties.ReadOnly:=True;
      fmAddDoc2.cxDateEdit2.Date:=quDocsOutSelDATESF.AsDateTime;
      fmAddDoc2.cxDateEdit2.Properties.ReadOnly:=True;

      fmAddDoc2.cxButtonEdit1.Tag:=quDocsOutSelIDCLI.AsInteger;
      fmAddDoc2.cxButtonEdit1.EditValue:=quDocsOutSelIDCLI.AsInteger;
      fmAddDoc2.cxButtonEdit1.Text:=quDocsOutSelNAMECL.AsString;
      fmAddDoc2.cxButtonEdit1.Properties.ReadOnly:=True;

      if quDocsOutSelIDFROM.AsInteger>0 then
      begin
        fmAddDoc2.cxButtonEdit2.Tag:=quDocsOutSelIDFROM.AsInteger;
        fmAddDoc2.cxButtonEdit2.EditValue:=quDocsOutSelIDFROM.AsInteger;
        fmAddDoc2.cxButtonEdit2.Text:=quDocsOutSelNAMECL1.AsString;
        fmAddDoc2.cxButtonEdit2.Properties.ReadOnly:=True;
      end else
      begin
        fmAddDoc2.cxButtonEdit2.Tag:=0;
        fmAddDoc2.cxButtonEdit2.EditValue:=0;
        fmAddDoc2.cxButtonEdit2.Text:='';
        fmAddDoc2.cxButtonEdit2.Properties.ReadOnly:=True;
      end;

      if quMHAll.Active=False then
      begin
        quMHAll.ParamByName('IDPERSON').AsInteger:=Person.Id;
        quMHAll.Active:=True;
      end;
      quMHAll.FullRefresh;

      fmAddDoc2.cxLookupComboBox1.EditValue:=quDocsOutSelIDSKL.AsInteger;
      fmAddDoc2.cxLookupComboBox1.Text:=quDocsOutSelNAMEMH.AsString;
      fmAddDoc2.cxLookupComboBox1.Properties.ReadOnly:=True;

      CurVal.IdMH:=quDocsOutSelIDSKL.AsInteger;
      CurVal.NAMEMH:=quDocsOutSelNAMEMH.AsString;

      if quMHAll.Locate('ID',CurVal.IdMH,[]) then
      begin
        fmAddDoc2.Label15.Caption:='��. ����: '+quMHAllNAMEPRICE.AsString;
        fmAddDoc2.Label15.Tag:=quMHAllDEFPRICE.AsInteger;
      end else
      begin
        fmAddDoc2.Label15.Caption:='��. ����: ';
        fmAddDoc2.Label15.Tag:=0;
      end;

      fmAddDoc2.cxLabel1.Enabled:=False;
      fmAddDoc2.cxLabel2.Enabled:=False;
      fmAddDoc2.cxLabel3.Enabled:=False;
      fmAddDoc2.cxLabel4.Enabled:=False;
      fmAddDoc2.cxLabel5.Enabled:=False;
      fmAddDoc2.cxLabel6.Enabled:=False;
      fmAddDoc2.cxLabel7.Enabled:=False;
      fmAddDoc2.cxLabel8.Enabled:=False;

      fmAddDoc2.cxButton1.Enabled:=False;

      CloseTe(fmAddDoc2.taSpecOut);

      fmAddDoc2.ViewDoc2.OptionsData.Editing:=False;
      fmAddDoc2.ViewDoc2.OptionsData.Deleting:=False;

      fmAddDoc2.acSave.Enabled:=False;

      IDH:=quDocsOutSelID.AsInteger;

      quSpecOutSel.Active:=False;
      quSpecOutSel.ParamByName('IDHD').AsInteger:=IDH;
      quSpecOutSel.Active:=True;

      quSpecOutSel.First;
      while not quSpecOutSel.Eof do
      begin
        with fmAddDoc2 do
        begin
          taSpecOut.Append;
          taSpecOutNum.AsInteger:=quSpecOutSelNUM.AsInteger;
          taSpecOutIdGoods.AsInteger:=quSpecOutSelIDCARD.AsInteger;
          taSpecOutNameG.AsString:=quSpecOutSelNAMEC.AsString;
          taSpecOutIM.AsInteger:=quSpecOutSelIDM.AsInteger;
          taSpecOutSM.AsString:=quSpecOutSelSM.AsString;
          taSpecOutQuant.AsFloat:=quSpecOutSelQUANT.AsFloat;
          taSpecOutPrice0.AsFloat:=quSpecOutSelPRICEIN0.AsFloat;
          taSpecOutSum0.AsFloat:=quSpecOutSelSUMIN0.AsFloat;
          taSpecOutPrice1.AsFloat:=quSpecOutSelPRICEIN.AsFloat;
          taSpecOutSum1.AsFloat:=quSpecOutSelSUMIN.AsFloat;
          taSpecOutPrice2.AsFloat:=quSpecOutSelPRICEUCH.AsFloat;
          taSpecOutSum2.AsFloat:=quSpecOutSelSUMUCH.AsFloat;
          taSpecOutINds.AsInteger:=quSpecOutSelIDNDS.AsInteger;
          taSpecOutSNds.AsString:=quSpecOutSelNAMENDS.AsString;
          taSpecOutRNds.AsFloat:=quSpecOutSelSUMNDS.AsFloat;
          taSpecOutSumNac.AsFloat:=quSpecOutSelSUMUCH.AsFloat-quSpecOutSelSUMIN.AsFloat;
          taSpecOutProcNac.AsFloat:=0;
          if quSpecOutSelSUMIN.AsFloat<>0 then
            taSpecOutProcNac.AsFloat:=RoundEx((quSpecOutSelSUMUCH.AsFloat-quSpecOutSelSUMIN.AsFloat)/quSpecOutSelSUMIN.AsFloat*10000)/100;
          taSpecOutCType.AsInteger:=quSpecOutSelCATEGORY.AsInteger;
          taSpecOut.Post;
        end;
        quSpecOutSel.Next;
      end;

      quSpecOutSel.Active:=False;

      prAllViewOn;

      fmAddDoc2.Show;
    end else
    begin
      showmessage('�������� �������� ��� ���������.');
    end;
  end;
end;

procedure TfmDocsOut.ViewDocsOutDblClick(Sender: TObject);
begin
  //������� �������
  with dmO do
  begin
    if quDocsOutSelIACTIVE.AsInteger=0 then acEditDoc1.Execute //��������������
    else acViewDoc1.Execute; //��������
  end;
end;

procedure TfmDocsOut.acDelDoc1Execute(Sender: TObject);
//Var IDH:INteger;
begin
  //������� ��������
  if not CanDo('prDelDocOut') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmO do
  begin
    if quDocsOutSel.RecordCount>0 then //���� ��� �������������
    begin
      if quDocsOutSelIACTIVE.AsInteger=0 then
      begin
        if MessageDlg('�� ������������� ������ ������� ��������� �'+quDocsOutSelNUMDOC.AsString+' �� '+quDocsOutSelNAMECL.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
//          IDH:=quDocsOutSelID.AsInteger;
          quDocsOutSel.Delete;

{          quSpecOutSel.Active:=False;
          quSpecOutSel.ParamByName('IDHD').AsInteger:=IDH;
          quSpecOutSel.Active:=True;

          quSpecOutSel.First; //������
          while not quSpecOutSel.Eof do quSpecOutSel.Delete;
          quSpecOutSel.Active:=False;
}          
        end;
      end else
      begin
        showmessage('������� �������������� �������� ������.');
      end;
    end else
    begin
      showmessage('�������� �������� ��� ��������.');
    end;
  end;
end;

procedure TfmDocsOut.acOnDoc1Execute(Sender: TObject);
Var IdH:INteger;
    rSumO,rSumT:Real;
begin
//������������
  with dmO do
  with dmORep do
  begin
    if quDocsOutSel.RecordCount=0 then  exit; //���� ��� ������������

    if not CanDo('prOnDocOut') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
    if not CanEdit(Trunc(quDocsOutSelDATEDOC.AsDateTime),quDocsOutSelIDSKL.AsInteger) then begin StatusBar1.Panels[0].Text:='������ ������.'; exit; end;

    prButtonSet(False);

    if quDocsOutSel.RecordCount>0 then //���� ��� ������������
    begin
      if quDocsOutSelIACTIVE.AsInteger=0 then
      begin
        if MessageDlg('������������ �������� �'+quDocsOutSelNUMDOC.AsString+' �� '+quDocsOutSelNAMECL.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          if prTOFind(Trunc(quDocsOutSelDATEDOC.AsDateTime),quDocsOutSelIDSKL.AsInteger)=1 then
          begin //�� ����
            if MessageDlg('������� ��� �������� ������ �� �� '+quDocsOutSelNAMEMH.AsString+' � '+quDocsOutSelNUMDOC.AsString+'?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
              prTODel(Trunc(quDocsOutSelDATEDOC.AsDateTime),quDocsOutSelIDSKL.AsInteger)
            else
            begin
              showmessage('��������� ������� ��������� ����������...');
              prButtonSet(True);
              exit;
            end;
          end;

         // 1 - ������� ��������� ������
         // 2 - �������� ��������������
         // 3 - �������� ������
          Memo1.Clear;
          Memo1.Lines.Add('����� ... ���� ������.'); Delay(10);


          //������� ��������� �� ������ ������, ����� ������ �������
          
          prDelPartOut.ParamByName('IDDOC').AsInteger:=quDocsOutSelID.AsInteger;
          prDelPartOut.ParamByName('DTYPE').AsInteger:=1;
          prDelPartOut.ExecProc;

          prDelPartOut.ParamByName('IDDOC').AsInteger:=quDocsOutSelID.AsInteger;
          prDelPartOut.ParamByName('DTYPE').AsInteger:=7;
          prDelPartOut.ExecProc;

          IDH:=quDocsOutSelID.AsInteger;
          if quDocsOutSelIDSKL.AsInteger>0 then
          begin
            prOn(IDH,quDocsOutSelIDSKL.AsInteger,Trunc(quDocsOutSelDATEDOC.AsDateTime),quDocsOutSelIDCLI.AsInteger,rSumO,rSumT);

            // 3 - �������� ������
            quDocsOutSel.Edit;
            quDocsOutSelSumIn.AsFloat:=RoundVal(rSumO);
            quDocsOutSelSUMTAR.AsFloat:=RoundVal(rSumT);
            quDocsOutSelIACTIVE.AsInteger:=1;
            quDocsOutSel.Post;
            quDocsOutSel.Refresh;

            fmPartIn1:=tfmPartIn1.Create(Application);
            fmPartIn1.Label5.Caption:=quDocsOutSelNAMEMH.AsString;
            fmPartIn1.Label6.Caption:=quDocsOutSelNAMECL.AsString;
            fmPartIn1.ShowModal;
            fmPartIn1.Release;
            taPartTest.Active:=False;
          end else showmessage('���������� ����� �������� ���������.');
          Memo1.Lines.Add('������ ��.'); Delay(10);
        end;
      end;
    end;
  end;
  prButtonSet(True);
end;

procedure TfmDocsOut.acOffDoc1Execute(Sender: TObject);
begin
//��������
  with dmO do
  begin
    if quDocsOutSel.RecordCount=0 then  exit; 
    if not CanDo('prOffDocOut') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
    if not CanEdit(Trunc(quDocsOutSelDATEDOC.AsDateTime),quDocsOutSelIDSKL.AsInteger) then begin StatusBar1.Panels[0].Text:='������ ������.'; exit; end;

    prButtonSet(False);

    if quDocsOutSel.RecordCount>0 then //���� ��� ����������
    begin
      if quDocsOutSelIACTIVE.AsInteger=1 then
      begin
        if MessageDlg('�������� �������� �'+quDocsOutSelNUMDOC.AsString+' �� '+quDocsOutSelNAMECL.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          if prTOFind(Trunc(quDocsOutSelDATEDOC.AsDateTime),quDocsOutSelIDSKL.AsInteger)=1 then
          begin //�� ����
            if MessageDlg('������� ��� �������� ������ �� �� '+quDocsOutSelNAMEMH.AsString+' � '+quDocsOutSelNUMDOC.AsString+'?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
              prTODel(Trunc(quDocsOutSelDATEDOC.AsDateTime),quDocsOutSelIDSKL.AsInteger)
            else
            begin
              showmessage('��������� ������� ��������� ����������...');
              prButtonSet(True);
              exit;
            end;
          end;
          Memo1.Clear;
          Memo1.Lines.Add('����� ... ���� ������.'); Delay(10);

          //������� ��� �������� ������
//          ?IDDOC, ?DTYPE
          prDelPartOut.ParamByName('IDDOC').AsInteger:=quDocsOutSelID.AsInteger;
          prDelPartOut.ParamByName('DTYPE').AsInteger:=1;
          prDelPartOut.ExecProc;

          prDelPartOut.ParamByName('IDDOC').AsInteger:=quDocsOutSelID.AsInteger;
          prDelPartOut.ParamByName('DTYPE').AsInteger:=7;
          prDelPartOut.ExecProc;

          quDocsOutSel.Edit;
          quDocsOutSelIACTIVE.AsInteger:=0;
          quDocsOutSel.Post;
          quDocsOutSel.Refresh;

          Memo1.Lines.Add('������ ��.'); Delay(10);

        end;
      end;
    end;
  end;
  prButtonSet(True);
end;

procedure TfmDocsOut.Timer1Timer(Sender: TObject);
begin
  if bClearDocOut=True then begin StatusBar1.Panels[0].Text:=''; bClearDocOut:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearDocOut:=True;
end;

procedure TfmDocsOut.acCopyExecute(Sender: TObject);
var Par:Variant;
    iNum:Integer;
begin
  //����������
  with dmO do
  with dmORep do
  begin
    if quDocsOutSel.RecordCount=0 then exit;

    taHeadDoc.Active:=False;
    taHeadDoc.FileName:=CurDir+'HeadDoc.cds';
    if FileExists(CurDir+'HeadDoc.cds') then taHeadDoc.Active:=True
    else taHeadDoc.CreateDataSet;

    taSpecDoc.Active:=False;
    taSpecDoc.FileName:=CurDir+'SpecDoc.cds';
    if FileExists(CurDir+'SpecDoc.cds') then taSpecDoc.Active:=True
    else taSpecDoc.CreateDataSet;

    par := VarArrayCreate([0,1], varInteger);
    par[0]:=7;
    par[1]:=quDocsOutSelID.AsInteger;
    if taHeadDoc.Locate('IType;Id',par,[])=False then
    begin
      taHeadDoc.Append;
      taHeadDocIType.AsInteger:=7;
      taHeadDocId.AsInteger:=quDocsOutSelID.AsInteger;
      taHeadDocDateDoc.AsInteger:=Trunc(quDocsOutSelDATEDOC.AsDateTime);
      taHeadDocNumDoc.AsString:=quDocsOutSelNUMDOC.AsString;
      taHeadDocIdCli.AsInteger:=quDocsOutSelIDCLI.AsInteger;
      taHeadDocNameCli.AsString:=Copy(quDocsOutSelNAMECL.AsString,1,70);
      taHeadDocIdSkl.AsInteger:=quDocsOutSelIDSKL.AsInteger;
      taHeadDocNameSkl.AsString:=Copy(quDocsOutSelNAMEMH.AsString,1,70);
      taHeadDocSumIN.AsFloat:=quDocsOutSelSUMIN.AsFloat;
      taHeadDocSumUch.AsFloat:=quDocsOutSelSUMUCH.AsFloat;
      taHeadDoc.Post;

      quSpecOutSel.Active:=False;
      quSpecOutSel.ParamByName('IDHD').AsInteger:=quDocsOutSelID.AsInteger;
      quSpecOutSel.Active:=True;

      quSpecOutSel.First;  iNum:=1;
      while not quSpecOutSel.Eof do
      begin
        taSpecDoc.Append;
        taSpecDocIType.AsInteger:=7;
        taSpecDocIdHead.AsInteger:=quDocsOutSelID.AsInteger;
        taSpecDocNum.AsInteger:=iNum;
        taSpecDocIdCard.AsInteger:=quSpecOutSelIDCARD.AsInteger;
        taSpecDocQuant.AsFloat:=quSpecOutSelQUANT.AsFloat;
        taSpecDocPriceIn.AsFloat:=quSpecOutSelPRICEIN.AsFloat;
        taSpecDocSumIn.AsFloat:=quSpecOutSelSUMIN.AsFloat;
        taSpecDocPriceUch.AsFloat:=quSpecOutSelPRICEUCH.AsFloat;
        taSpecDocSumUch.AsFloat:=quSpecOutSelSUMUCH.AsFloat;
        taSpecDocIdNds.AsInteger:=quSpecOutSelIDNDS.AsInteger;
        taSpecDocSumNds.AsFloat:=quSpecOutSelSUMNDS.AsFloat;
        taSpecDocNameC.AsString:=Copy(quSpecOutSelNAMEC.AsString,1,30);
        taSpecDocSm.AsString:=quSpecOutSelSM.AsString;
        taSpecDocIdM.AsInteger:=quSpecOutSelIM.AsInteger;
        taSpecDocKm.AsFloat:=prFindKM(quSpecOutSelIM.AsInteger);
        taSpecDocPriceUch1.AsFloat:=quSpecOutSelPRICEUCH.AsFloat;
        taSpecDocSumUch1.AsFloat:=quSpecOutSelSUMUCH.AsFloat;
        taSpecDoc.Post;

        inc(iNum);

        quSpecOutSel.Next;
      end;
      quSpecOutSel.Active:=False;

    end else
    begin
      showmessage('�������� ��� ���� � ������.');
    end;
    taHeadDoc.Active:=False;
    taSpecDoc.Active:=False;
  end;
end;

procedure TfmDocsOut.acPostExecute(Sender: TObject);
Var iCType:Integer;
begin
  // ��������
  with dmO do
  with dmORep do
  begin
    taHeadDoc.Active:=False;
    taHeadDoc.FileName:=CurDir+'HeadDoc.cds';
    if FileExists(CurDir+'HeadDoc.cds') then taHeadDoc.Active:=True
    else taHeadDoc.CreateDataSet;

    taSpecDoc.Active:=False;
    taSpecDoc.FileName:=CurDir+'SpecDoc.cds';
    if FileExists(CurDir+'SpecDoc.cds') then taSpecDoc.Active:=True
    else taSpecDoc.CreateDataSet;

    fmTBuff:=TfmTBuff.Create(Application);

    fmTBuff.LevelTH.Visible:=False;
    fmTBuff.LevelTS.Visible:=False;
    fmTBuff.LevelD.Visible:=True;
    fmTBuff.LevelDSpec.Visible:=True;

    fmTBuff.ShowModal;
    if fmTBuff.ModalResult=mrOk then
    begin //���������
      fmTBuff.Release;
      if taHeadDoc.RecordCount>0 then
      begin
        if CanDo('prAddDocOut') then
        begin
          prAllViewOff;

          fmAddDoc2.Caption:='���������: ����� ��������.';
          fmAddDoc2.cxTextEdit1.Text:=prGetNum(7,0);
          fmAddDoc2.cxTextEdit1.Properties.ReadOnly:=False;
          fmAddDoc2.cxTextEdit1.Tag:=0;

          fmAddDoc2.cxTextEdit2.Text:='';
          fmAddDoc2.cxTextEdit2.Properties.ReadOnly:=False;
          fmAddDoc2.cxDateEdit1.Date:=Date;
          fmAddDoc2.cxDateEdit1.Properties.ReadOnly:=False;
          fmAddDoc2.cxDateEdit2.Date:=Date;
          fmAddDoc2.cxDateEdit2.Properties.ReadOnly:=False;

          if taHeadDocIType.AsInteger=7 then
          begin
            fmAddDoc2.cxButtonEdit1.Tag:=taHeadDocIdCli.AsInteger;
            fmAddDoc2.cxButtonEdit1.EditValue:=taHeadDocIdCli.AsInteger;
            fmAddDoc2.cxButtonEdit1.Text:=taHeadDocNameCli.AsString;
          end else
          begin
            fmAddDoc2.cxButtonEdit1.Tag:=0;
            fmAddDoc2.cxButtonEdit1.EditValue:=0;
            fmAddDoc2.cxButtonEdit1.Text:='';
          end;

          fmAddDoc2.cxButtonEdit1.Properties.ReadOnly:=False;

          if quMHAll.Active=False then
          begin
            quMHAll.ParamByName('IDPERSON').AsInteger:=Person.Id;
            quMHAll.Active:=True;
          end;
          quMHAll.FullRefresh;

          fmAddDoc2.cxLookupComboBox1.EditValue:=taHeadDocIdSkl.AsInteger;
          fmAddDoc2.cxLookupComboBox1.Text:=taHeadDocNameSkl.AsString;
          fmAddDoc2.cxLookupComboBox1.Properties.ReadOnly:=False;

          if quMHAll.Locate('ID',taHeadDocIdSkl.AsInteger,[]) then
          begin
            fmAddDoc2.Label15.Caption:='��. ����: '+quMHAllNAMEPRICE.AsString;
            fmAddDoc2.Label15.Tag:=quMHAllDEFPRICE.AsInteger;
          end else
          begin
            fmAddDoc2.Label15.Caption:='��. ����: ';
            fmAddDoc2.Label15.Tag:=0;
          end;

          fmAddDoc2.cxLabel1.Enabled:=True;
          fmAddDoc2.cxLabel2.Enabled:=True;
          fmAddDoc2.cxLabel3.Enabled:=True;
          fmAddDoc2.cxLabel4.Enabled:=True;
          fmAddDoc2.cxLabel5.Enabled:=True;
          fmAddDoc2.cxLabel6.Enabled:=True;


          fmAddDoc2.cxButton1.Enabled:=True;

          fmAddDoc2.ViewDoc2.OptionsData.Editing:=True;
          fmAddDoc2.ViewDoc2.OptionsData.Deleting:=True;
          CloseTe(fmAddDoc2.taSpecOut);

          fmAddDoc2.acSave.Enabled:=True;

          taSpecDoc.First;
          while not taSpecDoc.Eof do
          begin
            if (taSpecDocIType.AsInteger=taHeadDocIType.AsInteger)and(taSpecDocIdHead.AsInteger=taHeadDocId.AsInteger) then
            begin
              with fmAddDoc2 do
              begin
                iCType:=1;

                quFCard.Active:=False;
                quFCard.SelectSQL.Clear;
                quFCard.SelectSQL.Add('SELECT ID,NAME,IMESSURE,PARENT,TCARD,INDS,LASTPRICEOUT,CATEGORY');
                quFCard.SelectSQL.Add('FROM OF_CARDS');
                quFCard.SelectSQL.Add('where ID='+IntToStr(taSpecDocIdCard.AsInteger));
                quFCard.SelectSQL.Add('and IACTIVE>0');
                quFCard.SelectSQL.Add('and PARENT in (SELECT ID_CLASSIF FROM RCLASSIF WHERE RIGHTS=0 AND ID_PERSONAL='+its(Person.Id)+')');
                quFCard.Active:=True;

                if quFCard.RecordCount=1 then iCType:=quFCardCATEGORY.AsInteger;

                taSpecOut.Append;
                taSpecOutNum.AsInteger:=taSpecDocNum.AsInteger;
                taSpecOutIdGoods.AsInteger:=taSpecDocIdCard.AsInteger;
                taSpecOutNameG.AsString:=taSpecDocNameC.AsString;
                taSpecOutIM.AsInteger:=taSpecDocIdM.AsInteger;
                taSpecOutSM.AsString:=taSpecDocSm.AsString;
                taSpecOutQuant.AsFloat:=RoundEx(taSpecDocQuant.AsFloat*1000)/1000;
                taSpecOutPrice1.AsFloat:=taSpecDocPriceIn.AsFloat;
                taSpecOutSum1.AsFloat:=taSpecDocSumIn.AsFloat;
                taSpecOutPrice2.AsFloat:=taSpecDocPriceUch.AsFloat;
                taSpecOutSum2.AsFloat:=taSpecDocSumUch.AsFloat;
                taSpecOutINds.AsInteger:=taSpecDocIdNds.AsInteger;
                taSpecOutSNds.AsString:='���';
                taSpecOutRNds.AsFloat:=taSpecDocSumNds.AsFloat;
                taSpecOutSumNac.AsFloat:=taSpecDocSumUch.AsFloat-taSpecDocSumIn.AsFloat;
                taSpecOutProcNac.AsFloat:=0;
                if taSpecDocSumIn.AsFloat<>0 then
                taSpecOutProcNac.AsFloat:=RoundEx((taSpecDocSumUch.AsFloat-taSpecDocSumIn.AsFloat)/taSpecDocSumIn.AsFloat*10000)/100;
                taSpecOutCType.AsInteger:=iCType;
                taSpecOut.Post;
              end;
            end;
            taSpecDoc.Next;
          end;

          taHeadDoc.Active:=False;
          taSpecDoc.Active:=False;

          prAllViewOn;

          fmAddDoc2.Show;
        end else showmessage('��� ����.');
      end;
    end else
    begin
      fmTBuff.Release;
      taHeadDoc.Active:=False;
      taSpecDoc.Active:=False;
    end;  
  end;
end;

procedure TfmDocsOut.FormShow(Sender: TObject);
begin
  Memo1.Clear;
end;

procedure TfmDocsOut.Excel1Click(Sender: TObject);
begin
  prNExportExel5(ViewDocsOut);
end;

procedure TfmDocsOut.acPrintDOExecute(Sender: TObject);
Var S1,S2,S3,S4,S5,S6,S7,S8,S9,S10:String;
begin
  if LevelDocsOut.Visible=False then exit;
  with dmO do
  begin
    if quDocsOutSel.RecordCount>0 then //���� ��� ��������
    begin
      quSpecOutSel.Active:=False;
      quSpecOutSel.ParamByName('IDHD').AsInteger:=quDocsOutSelID.AsInteger;
      quSpecOutSel.Active:=True;

      frRepDocsOD.LoadFromFile(CurDir + 'ttn12_1.frf');

      frVariables.Variable['Num']:=quDocsOutSelNUMDOC.AsString;
      frVariables.Variable['sDate']:=FormatDateTime('dd.mm.yyyy',quDocsOutSelDATEDOC.AsDateTime);
      frVariables.Variable['FromMH']:=quDocsOutSelNAMEMH.AsString;
      frVariables.Variable['ToMH']:=quDocsOutSelNAMECL.AsString;
      frVariables.Variable['Depart']:=CommonSet.DepartName;
{
      prFindCl(quDocsOutSelIDCLI.AsInteger,S1,S2,S3,S4,S5,S6,S7,S8,S9,S10); //�������� ������� �� ��� ID
      frVariables.Variable['Cli2Name']:=S1;
      frVariables.Variable['Cli2Adr']:=S2;
      frVariables.Variable['Cli2Inn']:=S3+'/'+S4;
      frVariables.Variable['Poluch']:=S5;
      frVariables.Variable['PoluchAdr']:=S6;

      prFindCl(quDocsOutSelIDFROM.AsInteger,S1,S2,S3,S4,S5,S6,S7,S8,S9,S10); //�������� ������� �� ��� ID
      frVariables.Variable['Cli1Name']:=S1;
      frVariables.Variable['Cli1Adr']:=S2;
      frVariables.Variable['Cli1Inn']:=S3+'/'+S4;
      frVariables.Variable['Otpr']:=S5;
      frVariables.Variable['OtprAdr']:=S6;
}
      prFindCl(quDocsOutSelIDCLI.AsInteger,S1,S2,S3,S4,S5,S6,S7,S8,S9,S10); //�������� ������� �� ��� ID
      frVariables.Variable['Cli2']:=S1;
      frVariables.Variable['Cli2Adr']:=S2;
      frVariables.Variable['Cli2Inn']:=S3+'/'+S4;
      frVariables.Variable['Cli2Otpr']:=S5;
      frVariables.Variable['Cli2OtprAdr']:=S6;
      frVariables.Variable['Cli2RSch']:=S7;
      frVariables.Variable['Cli2KSch']:=S8;
      frVariables.Variable['Cli2Bank']:=S9;
      frVariables.Variable['Cli2Bik']:=S10;


      prFindCl(quDocsOutSelIDFROM.AsInteger,S1,S2,S3,S4,S5,S6,S7,S8,S9,S10); //�������� ������� �� ��� ID
      frVariables.Variable['Cli1']:=S1;
      frVariables.Variable['Cli1Adr']:=S2;
      frVariables.Variable['Cli1Inn']:=S3+'/'+S4;
      frVariables.Variable['Cli1Otpr']:=S5;
      frVariables.Variable['Cli1OtprAdr']:=S6;
      frVariables.Variable['Cli1RSch']:=S7;
      frVariables.Variable['Cli1KSch']:=S8;
      frVariables.Variable['Cli1Bank']:=S9;
      frVariables.Variable['Cli1Bik']:=S10;


      frRepDocsOD.ReportName:='��������� - �������.';
      frRepDocsOD.PrepareReport;
      frRepDocsOD.ShowPreparedReport;

      quSpecOutSel.Active:=False;
    end else
    begin
      showmessage('�������� �������� ��� ������.');
    end;
  end;
end;

procedure TfmDocsOut.acPrintSCHFExecute(Sender: TObject);
Var S1,S2,S3,S4,S5,S6,S7,S8,S9,S10:String;
begin
  if LevelDocsOut.Visible=False then exit;
  with dmO do
  begin
    if quDocsOutSel.RecordCount>0 then //���� ��� ��������
    begin
      quSpecOutSel.Active:=False;
      quSpecOutSel.ParamByName('IDHD').AsInteger:=quDocsOutSelID.AsInteger;
      quSpecOutSel.Active:=True;

      frRepDocsOD.LoadFromFile(CurDir + 'schf.frf');

      frVariables.Variable['Num']:=quDocsOutSelNUMSF.AsString;
      frVariables.Variable['sDate']:=FormatDateTime('dd.mm.yyyy',quDocsOutSelDATESF.AsDateTime);

      prFindCl(quDocsOutSelIDCLI.AsInteger,S1,S2,S3,S4,S5,S6,S7,S8,S9,S10); //�������� ������� �� ��� ID
      frVariables.Variable['Cli2Name']:=S1;
      frVariables.Variable['Cli2Adr']:=S2;
      frVariables.Variable['Cli2Inn']:=S3+'/'+S4;
      frVariables.Variable['Poluch']:=S5;
      frVariables.Variable['PoluchAdr']:=S6;

      prFindCl(quDocsOutSelIDFROM.AsInteger,S1,S2,S3,S4,S5,S6,S7,S8,S9,S10); //�������� ������� �� ��� ID
      frVariables.Variable['Cli1Name']:=S1;
      frVariables.Variable['Cli1Adr']:=S2;
      frVariables.Variable['Cli1Inn']:=S3+'/'+S4;
      frVariables.Variable['Otpr']:=S5;
      frVariables.Variable['OtprAdr']:=S6;

      frRepDocsOD.ReportName:='����-�������.';
      frRepDocsOD.PrepareReport;
      frRepDocsOD.ShowPreparedReport;

      quSpecOutSel.Active:=False;
    end else
    begin
      showmessage('�������� �������� ��� ������.');
    end;
  end;
end;

procedure TfmDocsOut.ViewDocsOutCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
Var i:Integer;
    sA:String;
begin
  sA:=' ';
  for i:=0 to ViewDocsOut.ColumnCount-1 do
  begin
    if ViewDocsOut.Columns[i].Name='ViewDocsOutIACTIVE' then
    begin
      sA:=AViewInfo.GridRecord.DisplayTexts[i];
      break;
    end;
  end;

//  if pos('�����',sA)=0  then  ACanvas.Canvas.Brush.Color := $00ECD9FF;
  if pos('�����',sA)=0  then  ACanvas.Canvas.Brush.Color := $00CACAFF;
end;

procedure TfmDocsOut.N6Click(Sender: TObject);
begin
  dmO.ColorDialog1.Color:=SpeedBar1.Color;
  if dmO.ColorDialog1.Execute then
  begin
    SpeedBar1.Color := dmO.ColorDialog1.Color;
    StatusBar1.Color:= dmO.ColorDialog1.Color;
    UserColor.TTnOut := dmO.ColorDialog1.Color;
    WriteColor;
  end;
end;

procedure TfmDocsOut.acExitExecute(Sender: TObject);
begin
  Close;
end;

end.
