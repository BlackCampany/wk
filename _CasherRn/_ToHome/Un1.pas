unit Un1;

interface
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  IniFiles, ADODB, Variants, IdGlobal, ComCtrls, pFIBDataSet, cxTreeView,
  ComObj, ActiveX, Excel2000, OleServer, ExcelXP, cxGridDBTableView,
  cxGridDBBandedTableView,DB,DBClient,cxMemo,dxmdaset,cxCustomData,cxDBData,
  EasyCompression, FileUtil, cxGridCustomTableView;

type TPerson = record
     Id:Integer;
     Name,sCli:String;
     end;

     TCurVal = record
     IdMH:INteger;
     IdMH1:INteger;
     IdMH2:INteger;
     NAMEMH:String;
     NAMEMH1:String;
     NAMEMH2:String;
     ICLASS:Integer;
     SCLASS:STRING;
     ICLALL:Integer;
     end;

     TCheck  = record
     Max:Integer;
     Id_personal:Integer;
     NumTable:String;
     Sifr:Integer;
     Price:Real;
     Quantity:real;
     Summa:Real;
     DProc:real;
     DSum:Real;
     Name:String;
     Code:String;
     LimitM:INteger;
     LinkM:Integer;
     Stream:Integer;
     end;

     TStartId = record
     IdBeg:Integer;
     IdBegT:Integer;
     end;

     TCommonSet = record
     AutoTake:Integer;
     CashNum:Integer;
     CashChNum:Integer;
     CashSer:String;
     CashPort:Integer;
     PortCash:String;
     CashZ:Integer;
     ComDelay:Integer;
     PathExport:String;
     PathImport:String;
     FtpExport:String;
     FtpImport:String;
     PathHistory:String;
     PathArh:String;
     DepartId:String;
     DepartName:String;
     DateFrom:TDateTime;
     DateTo:TDateTime;
     PeriodPing:INteger;
     Key_Char:String;
     Key_Code:Integer;
     Key_Shift:Integer;
     Key_Add: Boolean;
     Pgroup1,PGroup2,PGroup3,PGroup4,PGroup5,PGroupW:String;
     Pgroup1N,PGroup2N,PGroup3N,PGroup4N,PGroup5N,PGroupWN:String;
     PRing1,PRing2,PRing3,PRing4,PRing5,PRingW:Integer;
     Qgroup1,QGroup2,QGroup3,QGroup4,QGroup5:String;
     PrePrintPort:String;
     PathRkData:String;
     sUnit:String;
     bTouch:Boolean;
     ExportFile:String;
     WriteLog:INteger;
     OfficeDb:String;
     Station:Integer;
     BNManual:ShortInt;
     BNPath:String;
     BNPointNum:String;
     BNDelay:INteger;
     BNButton:INteger;
     PreCheckCount:Integer;
     MReader:Integer;
     Prefix,PrefixVes:String;
     PreCheckNum:INteger;
     iDelay,iStartDelaySec,CheckDelaySec:Integer;
     ZTimeShift:String;
     IncNumTab:Integer;
     IdStore:INteger;
     NameStore:String;
     iQuestVisible:INteger;
     HourShift:Integer;
     SpecChar:Integer;
     MustPrePrint:Integer;
     SpecBox,SpecBoxX:String;
     UseDP2300:INteger;
     DiscAuto:Integer;
     LastLine,LastLine1:String;
     PreLine,PreLine1:String;
     iPrePrintSt:Integer;
     KuponButton:Integer;
     KuponCode:Integer;
     KuponName:String;
     RaschotWarn:ShortInt;
     SelPCard:ShortInt;
     PriceFromCash:ShortInt;
     iRefreshTime:INteger;
     ControlNums:Integer;
     TestOpenTBeforeClose:Integer;
     InventryToTO:INteger;
     CalcRemns:INteger;
     FisTypeBn:INteger;

     HostIp:String;
//     NumPost:Integer;
     HostUser:String;
     HostPassw:string;
//     PathTmp:String;
     Compress:INteger;

     ViewTab:INteger;
     RecalcNDS:INteger;
     ItsOffice:Integer;
//     NoFis:INteger;
     TypeFis:String;
     AutoZak:INteger;
     Of_DocRefresh:INteger;
     end;

     TPrib = record
     MHId:Integer; MHAll,MHUnion:Boolean;
     MProd:Integer; MProdAll,MProdUnion:Boolean;
     Cat:Integer; CatAll,CatUnion:Boolean;
     Salet:String; SaletAll,SaletUnion:Boolean;
     Oper:Integer; OperAll, OperUnion, OperUnionCli :Boolean;
     end;


     TSpecVal = record
       SPH1,SPH2,SPH3,SPH4,SPH5:String;
       SPE1,SPE2,SPE3,SPE4,SPE5:String;
       DepartName:String;
       CountStarts:INteger;
       DateTo:TDateTime;
     end;

     TTrebSel= record
     CLTO:Integer;
     CLTO_Name:String;
     DType:Integer;
     DType_Name:String;
     DateFrom:TDateTime;
     DateTo:TDateTime;
     TimeFrom:TDateTime;
     TimeTo:TDateTime;
     end;


     tClassifRec = record
     TYPE_CLASSIF:Integer;
     ID:Integer;
     Id_Parent:Integer;
     Name:String;
     end;

     tMesuriment = record
     Id:Integer;
     Name:String;
     end;

     tCardRec = record
     Id:Integer;
     Id_Classif:Integer;
     Name:String;
     Mesuriment:Integer;
     end;

     tCategRec = record
     Id:Integer;
     Name:String;
     Tax_Group:Integer;
     end;

     tMenu = record
     Sifr:INteger;
     Name:String;
     Price:Real;
     Code:String;
     TreeType:String[1];
     LimitPrice:Real;
     Categ:Integer;
     Parent:Integer;
     Link:Integer;
     Stream:Integer;
     Lack:Integer;
     DesignSifr:Integer;
     AltName:String;
     Nalog:Real;
     Barcode:String;
     Image:Integer;
     Consumma:Real;
     MinRest:Integer;
     PrnRest:Integer;
     CookTime:Integer;
     Dispenser:Integer;
     DispKoef:Integer;
     Access:Integer;
     Flags:Integer;
     Tara:Integer;
     CntPrice:Integer;
     BackBGR:Real;
     FontBGR:Real;
     end;

     tModRec = record
     Id:Integer;
     Name:String;
     Parent:Integer;
     Price:Integer;
     RealPrice:Integer;
     end;

     TCardSel = Record
     iType:ShortInt;
     Id:Integer;
     Name:String;
     Id_Group:Integer;
     NameGr:String;
     Quant:Real;
     Depart:String;
     DepartId:String;
     mesuriment:String;
     end;

     tTrebHRec = record
     SID:String;
     DEPARTID:String;
     DEPARTNAME:String;
     DTYPE:Integer;
     TREBDATE:TDateTime;
     SENDDATE:TDateTime;
     CLTO:Integer;
     Comment:String;
     end;

     tSpecRec = record
     SID_HEAD:String;
     ID:Integer;
     CARD_ID:Integer;
     CARD1_ID:Integer;
     QUANT:Real;
     NUM:Integer;
     GROUPCARD:Integer;
     GROUPCARD1:Integer;
     end;

     TTab = record
     Id_Personal:Integer;
     Name:String;
     OpenTime:TDateTime;
     NumTable:String[20];
     Quests:Integer;
     iStatus:Integer; //0 - ��������������    1- �������
     Id:Integer;
     DBar,DBar1:String;
     DPercent:Real;
     DName:String;
     Summa:Real;
     PBar:String;
     PName:String;
     iNumZ:INteger;
     SaleT:SmallINt;
     DType:SmallINt;
     end;

     TUserColor = Record
     Color1,Color2,Color3:TColor;
     Main,TTnIn,TTnOut,TTnOutB,TTnVn,TTnOutR,TTnInv,TTnCompl,TTnAP:TColor;
     end;

     TUserPars = Record
     CheckGram:ShortInt;
     end;

     TBufPr = Record
     Arr:Array[1..8000] of Char;
     iC:Integer;
     end;



procedure Delay(MSecs: Longint);
Function TrimStr(StrIn:String):String;
Procedure CheckNodeOn(Node:TTreeNode;bOn:Boolean);
Procedure ExpandLevel( Node : TTreeNode; Tree:TTreeView; quTree:TpFIBDataSet);
Procedure RExpandLevel( Node : TTreeNode; Tree:TTreeView; quTree:TpFIBDataSet; PersonalId:Integer);
Procedure RefreshTree(PersonalId:Integer; Tree:TTreeView; quTree:TpFIBDataSet);

Procedure ReadIni;
Procedure WriteIni;

Procedure WriteCheckNum;
Procedure WriteTabNum;

Procedure ReadStartIni;
Procedure WriteStartIni;
Procedure WriteColor;
Procedure WriteParams;

Function TestExch:Boolean;
procedure WriteHistory(Strwk_: string);
procedure WriteTestLog(Strwk_: string);

Function StrToClassif(StrIn:String; Var ClRec:TClassifRec):Boolean;
Function StrToMes(StrIn:String; Var vMes:TMesuriment):Boolean;
Function StrToCard(StrIn:String; Var vCard:TCardRec):Boolean;
Function StrToCateg(StrIn:String; Var vCateg:TCategRec):Boolean;
Function StrToMenu(StrIn:String; Var vMenu:TMenu):Boolean;
Function StrToMod(StrIn:String; Var vMod:TModRec):Boolean;
Procedure CardsExpandLevel( Node : TTreeNode; Tree:TTreeView; quTree:TpFIBDataSet);
Procedure ClassifExpandLevel( Node : TTreeNode; Tree:TTreeView; quTree:TpFIBDataSet; PersonalId:Integer);

Function StrToTrHead(StrIn:String; Var HRec:tTrebHRec):Boolean;
Function StrToSpec(StrIn:String; Var SRec:tSpecRec):Boolean;
procedure TestDir;
Function TestExport:Boolean;
Function DelSp(sStr:String):String;
function RoundEx( X: Double ): Integer;
function RoundVal( X: Double ): Double;
procedure prWriteLog(Strwk_: string);
procedure prWriteLogPS(Strwk_: string);
Function ToStandart(S:String):String;

Procedure ClassifExpand( Node : TTreeNode; Tree:TcxTreeView; quTree:TpFIBDataSet; PersonalId,CLTYPE:Integer );
Procedure MessExpandLevel( Node : TTreeNode; Tree:TTreeView; quTree:TpFIBDataSet);
Procedure MenuExpandLevel( Node : TTreeNode; Tree:TcxTreeView; quTree:TpFIBDataSet;IDH:INteger);

Procedure ClassifEx( Node : TTreeNode; Tree:TcxTreeView; quTree:TpFIBDataSet; CLTYPE:Integer; NAMEF:String);
Function prDateToI(tD:TDateTime):INteger;
Function SOnlyDigit(S:String):String;
function IsOLEObjectInstalled(Name: String): boolean;

procedure prExportExel(Vi:tcxGridDBTableView;dsT:tDataSource;taT:tpFIBDataSet);
procedure prExportExel1(Vi:tcxGridDBTableView;dsT:tDataSource;taT:tClientDataSet);
procedure prExportExel2(Vi:tcxGridDBBandedTableView;dsT:tDataSource;taT:tClientDataSet);
procedure prExportExel3(Vi:tcxGridDBTableView;dsT:tDataSource;taT:tdxMemData);
procedure prExportExel4(Vi:tcxGridDBBandedTableView;dsT:tDataSource;taT:tdxMemData);
procedure prExportExelb(Vi:tcxGridDBBandedTableView;dsT:tDataSource;taT:tdxMemData);

procedure prNExportExel3(Vi:tcxGridDBTableView);
procedure prNExportExel4(Vi:tcxGridDBTableView);
procedure prNExportExel5(Vi:tcxGridDBTableView);

procedure prCalcDefGroup(i1,i2,i3,iParent:Integer; V:TcxGridDBTableView);
procedure prCalcSumNac(i1,i2,i3:Integer; V:TcxGridDBTableView);

function FlToS(rS:Real):String;
Function fTestKey(Key:Word):Boolean;
Procedure ResetAddVars;
Function prDefFormatStr(l:ShortInt;sN:String;rS:Real):String;
Function prDefFormatStr1(l:ShortInt;sN:String;rS:Real):String;
Function prDefFormatStr2(l:ShortInt;sN:String;rS:Integer):String;

procedure prWH(Strwk_: string;Me:TcxMemo);
procedure CloseTa(taT:tClientDataSet);
procedure CloseTe(taT:tdxMemData);

Function fDifStr(iT:Integer;S:String):String;
procedure prRowFocus(Vi:tcxGridDBTableView);

Procedure prInBuf(Ch:Char);
Function its(i:Integer):String;
Function fts(rSum:Real):String;
Function vts(rSum:Real):String;
function RV( X: Double ): Double;
function ds1(d:TDateTime):String;
function DelP(S:String):String;


//              ViewGoods.Controller.FocusRecord(ViewGoods.DataController.FocusedRowIndex,True);


Const CurIni:String = 'Profiles.ini';
      GridIni:String = 'ProfilesGr.ini';
      LicIni:String = 'ProfilesSp';
      ParmsIni:String = 'Parms.ini';
      FileInd:String = 'Start';
      CashNon:String = 'Cash.non';
      R:String = ';';
      kClassif:Integer = 10000;
      TrExt:String = '.cr~';
      MaxDelayPrint:INteger = 20;

{      levelValue: array [0..9] of TECLCompressionLevel =
              (eclNone, zlibFastest, zlibNormal, zlibMax, bzipFastest,
               bzipNormal, bzipMax, ppmFastest, ppmNormal, ppmMax);}
      odInac:String = 'odNoFocusRect_';
      odInac1:String = '314159SoftUr314159';
      StartIni:String = 'Start.ini';
      iMaxDate:Integer = 70000;
      iColor:Integer = 1;  //1-������� ����, 2-����� ����

Var StrWk:String;
    Person:TPerson;
    CurDir:String;
    DBName:String;
    DBNamePC:String;
    CommonSet:TCommonSet;
    bClear1,bClear2,bClear3,bClear4,bClear5,bClear6:Boolean;
    ClassifRec:TClassifRec;
    vMesuriment:TMesuriment;
    CardRec:TCardRec;
    CategRec:TCategRec;
    MenuRec:TMenu;
    ModRec:TModRec;
    CardSel:TCardSel;
    BegDrag:Boolean = False;
    TrebSel:TTrebSel;
    ViewOnly:Boolean = False;
    TrebHRec:tTrebHRec;
    SpecRec:TSpecRec;
    iCountSec:Integer;
    bSave:Boolean;
    StrPre,DiscountBar,PDiscountBar :String;
    Check:TCheck;
    Tab:TTab;
    MaxMod, CountMod:Integer;
    MessClear,bClearStatusBar,MessClearSpec:Boolean;
    Operation:ShortInt;
    StartId:TStartId; //��������� ���� ��� ���������� ����������
    bBludoSel:Boolean = False;
    sFormatDate:String = 'dd.mm.yyyy';
    bPrintCheck:Boolean = False;
    bExitPers:Boolean = False;
    bMenuList:Boolean = False;
    bMenuListMo:Boolean = False;
    bPrintPre:Boolean = False;
    bCloseSpec:Boolean = False;
    bChangeView:Boolean = False;
    bFF:Boolean = False;
    iCashType:ShortInt = 0; //�������� ������
    CurVal:TCurVal;
    sErr:String;
    iErr:INteger;
    UserColor:TUserColor;
    UserPars:TUserPars;

    bAddSpecIn:Boolean = False;
    bAddSpecB:Boolean = False;
    bAddSpecB1:Boolean = False;
    bAddSpecInv:Boolean = False;
    bAddSpecCompl:Boolean = False;
    bAddTSpec:Boolean = False;
    bAddSpecAO:Boolean = False;
    bAddSpecVn:Boolean = False;
    bAddSpecRet:Boolean = False;
    bAddSpecR:Boolean = False;
    bEE:Boolean = False;

    bAddSL:Boolean = False;

    vNDS:array[1..3] of Real;
    sNDS:array[1..3] of Real;
    PrintFCheck:Boolean = False;
    PrintQu:Boolean = False;
    TestPrint:Integer;

    bDM:Boolean = False;
    bDInv:Boolean = False;
    bDCompl:Boolean = False;
    bDrIn:Boolean = False;
    bDrRet:Boolean = False;
    bDrVn:Boolean = False;
    bDAct:Boolean = False;
    bDrR:Boolean = False;
    bStartGoods:Boolean = False;

    bAddPosFromMenu:Boolean = False;
    bAddPosFromSpec:Boolean = False;
    bRefresh:Boolean = True;
    iId:Integer; //��� ������� �������������
    BnStr:String;
    iPayType:INteger;
    bSelCli:Boolean = False;
    bSet:Boolean = True;
    bPrintMemo:Boolean = True;
    SpecVal:TSpecVal;
    fs: TECLFileStream;
    levelValue: array [0..9] of TECLCompressionLevel =
              (eclNone, zlibFastest, zlibNormal, zlibMax, bzipFastest,
               bzipNormal, bzipMax, ppmFastest, ppmNormal, ppmMax);
    BufPr:TBufPr;
    bPressB5Main:Boolean = False;
    bPressB5Cash:Boolean = False;
    bPressB12Spec:Boolean = False;
    Prib:TPrib;
    bStopAlg:Boolean = False;

implementation


function ds1(d:TDateTime):String;
begin
  result:=FormatDateTime('dd.mm.yyyy',d);
end;

function DelP(S:String):String;
Var Str1:String;
begin
  Str1:=S;
  while pos('.',Str1)>0 do delete(Str1,pos('.',Str1),1);
  while pos(' ',Str1)>0 do delete(Str1,pos(' ',Str1),1);
  Result:=Str1;
end;


procedure prNExportExel5(Vi:tcxGridDBTableView);
Var ExcelApp, Workbook, Range, Cell1, Cell2, ArrayData  : Variant;
    iCol,iRow,i:Integer;
    RowInf:TcxRowInfo;
    iRowV,J,k,iGCount,iLev,iItem:Integer;
    StrWk:String;
    arCol:array[1..20] of integer;
    iCountDG,ll,cc,ff:INteger;
    sField,sColumn,sSum:String;
    SumGr:tcxGridDBTableSummaryItem;
    Rec:TcxCustomGridRecord;
    rVal:Real;


function IsGroupingRow(ARowInfo: TcxRowInfo; ADataController: TcxCustomDataController): Boolean;
begin
  with ADataController do
    Result := ARowInfo.Level <> Groups.GroupingItemCount;
end;

begin
//������� � ������
//  Vi.BeginUpdate;

  if not IsOLEObjectInstalled('Excel.Application') then exit;
  ExcelApp := CreateOleObject('Excel.Application');
  ExcelApp.Application.EnableEvents := false;
  //� ������� �����
  Workbook := ExcelApp.WorkBooks.Add;

  for i:=1 to 20 do arCol[i]:=-1; //������� ��� �������

  iCol:=Vi.DataController.Groups.GroupingItemCount; //����� ����� �����
  for i:=0 to iCol-1 do
  begin
    arCol[i+1]:=Vi.DataController.Groups.GroupingItemIndex[i];
  end;

  if Vi.Controller.SelectedRowCount>1 then //������������ ������ ���������
  begin
    iGCount:=0;
    iCol:=Vi.VisibleColumnCount;

    iRow:=Vi.Controller.SelectedRowCount+1;

    iRowV:=Vi.Controller.SelectedRowCount;

    ArrayData := VarArrayCreate([1,iRow, 1, iCol+iGCount], varVariant);
    for i:=iGCount+1 to iCol+iGCount do
    begin
      ArrayData[1,i] := Vi.VisibleColumns[i-1-iGCount].Caption;
    end;

    iRow:=2;

    for i:=0 to Vi.Controller.SelectedRecordCount-1 do
    begin
      Rec:=Vi.Controller.SelectedRecords[i];
      k:=1;
      for j:=0 to Vi.ColumnCount-1 do
      begin
        if Vi.Columns[j].Visible then
        begin
          try
            StrWk:=Rec.Values[j]
          except
            StrWk:='';
          end;
          if pos('�',StrWk)=length(StrWk) then delete(StrWk,length(StrWk),1);
          if pos('%',StrWk)=length(StrWk) then delete(StrWk,length(StrWk),1);

          rVal:=StrToFloatDef(StrWk,1000000);
          if rVal<>1000000 then  ArrayData[iRow+i,k]:=rVal
          else ArrayData[iRow+i,k]:=StrWk;
          inc(k);
        end;
      end;
    end;
  end else
  begin       //��� ������

    iGCount:=Vi.DataController.Groups.GroupingItemCount;
    iCol:=Vi.VisibleColumnCount;

    iRow:=Vi.DataController.RowCount+1;

    iRowV:=Vi.DataController.RowCount;
//  iJ:=Vi.DataController.ItemCount;
//  iJ:=Vi.VisibleColumnCount;

    ArrayData := VarArrayCreate([1,iRow, 1, iCol+iGCount], varVariant);
    for i:=iGCount+1 to iCol+iGCount do
    begin
      ArrayData[1,i] := Vi.VisibleColumns[i-1-iGCount].Caption;
    end;

    iRow:=2;

    for i:=0 to iRowV-1 do
    begin
      with Vi.DataController do
      begin
        RowInf:=GetRowInfo(i);
        if IsGroupingRow(RowInf,Vi.DataController)=False then
        begin
          k:=1+iGCount;
          for j:=0 to Vi.ColumnCount-1 do
          begin
            if Vi.Columns[j].Visible then
            begin
              StrWk:=GetRowDisplayText(RowInf,j);
              if pos('�',StrWk)=length(StrWk) then delete(StrWk,length(StrWk),1);
              if pos('%',StrWk)=length(StrWk) then delete(StrWk,length(StrWk),1);

              rVal:=StrToFloatDef(StrWk,1000000);
              if rVal<>1000000 then  ArrayData[iRow,k]:=rVal
              else ArrayData[iRow,k]:=StrWk;

              inc(k);
            end;
          end
        end else
        begin
          iLev:=RowInf.Level;
          iItem:=Groups.GroupingItemIndex[iLev];

          ArrayData[iRow,iLev+1]:=Vi.Columns[iItem].Caption+': '+GetRowDisplayText(RowInf,iItem);

//        StrWk:=Summary.GroupSummaryText[i];

//        prStrToArr(StrWk); //��� �������������� ��� �� �����

{        for mm:=1 to 20 do
        begin
          if arSum[mm]<1000000000 then
          begin
            ArrayData[iRow,iLev+2+mm]:=arSum[mm];
          end;
        end;

GroupFooterSummaryTexts[RowIndex, Level, Index: Integer]
}
          iCountDG:=Summary.DefaultGroupSummaryItems.Count;
          for ll:=0 to iCountDG-1 do
          begin
            SumGr:=(Summary.DefaultGroupSummaryItems.Items[ll] as tcxGridDBTableSummaryItem);
            if SumGr.Position=spFooter then
            begin
              sField:=SumGr.FieldName;
              sSum:=Summary.GroupFooterSummaryTexts[i,iLev,ll];

              for cc:=iGCount+1 to iCol+iGCount do
              begin
                sColumn:=Vi.VisibleColumns[cc-1-iGCount].Name;
                delete(sColumn,1,Length(Vi.Name)); //������ �� �������� ������� �������� View
                if sColumn=sField then //����� �������
                begin
                  ff:=cc;
                  ArrayData[iRow,ff]:=sSum;
                end;
              end;
            end;
          end;


        end;
        inc(iRow);
      end;
    end;
  end;

  Cell1 := WorkBook.WorkSheets[1].Cells[1,1];
  Cell2 := WorkBook.WorkSheets[1].Cells[1+iRowV,iCol+iGCount];
  Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
  Range.Value := ArrayData;

//  Vi.EndUpdate;

  ExcelApp.Visible := true;
end;


function RV( X: Double ): Double;
var  ScaledFractPart:Integer;
     Temp : Double;

begin

 ScaledFractPart := Trunc(X*100);
 if X>=0 then Temp := Trunc(Frac(X*100)*1000000)+1 else  Temp := Trunc(Frac(X*100)*1000000)-1;
 if Temp >=  500000 then ScaledFractPart := ScaledFractPart + 1;
 if Temp <= -500000 then ScaledFractPart := ScaledFractPart - 1;
{ if Temp >=  0.5 then ScaledFractPart := ScaledFractPart + 1;
 if Temp <= -0.5 then ScaledFractPart := ScaledFractPart - 1;}
 RV:= ScaledFractPart/100;
end;


Function vts(rSum:Real):String;
Var s:String;
begin
  Str(rSum:8:2,s);
  while pos(',',s)>0 do s[pos(',',s)]:='.';
  while pos(' ',s)>0 do delete(s,pos(' ',s),1);
  Result:=s;
end;

Function fts(rSum:Real):String;
Var s:String;
begin
  s:=FloatToStr(rSum);
  while pos(',',s)>0 do s[pos(',',s)]:='.';
  Result:=s;
end;

Function its(i:Integer):String;
begin
  result:=IntToStr(i);
end;

Procedure prInBuf(Ch:Char);
begin
  if BufPr.iC<8000 then
  begin
    BufPr.Arr[BufPr.iC+1]:=Ch;
    inc(BufPr.iC);
  end;
end;

procedure prRowFocus(Vi:tcxGridDBTableView);
begin
  Vi.Controller.FocusRecord(Vi.DataController.FocusedRowIndex,True);
end;

procedure prNExportExel4(Vi:tcxGridDBTableView);
Var ExcelApp, Workbook, Range, Cell1, Cell2, ArrayData  : Variant;
    iCol,iRow,i:Integer;
    RowInf:TcxRowInfo;
    iRowV,J,k,iGCount,iLev,iItem:Integer;
    StrWk:String;
    arCol:array[1..20] of integer;
    iCountDG,ll,cc,ff:INteger;
    sField,sColumn,sSum:String;
    SumGr:tcxGridDBTableSummaryItem;
    rVal:Real;

function IsGroupingRow(ARowInfo: TcxRowInfo; ADataController: TcxCustomDataController): Boolean;
begin
  with ADataController do
    Result := ARowInfo.Level <> Groups.GroupingItemCount;
end;

begin
//������� � ������
//  Vi.BeginUpdate;

  if not IsOLEObjectInstalled('Excel.Application') then exit;
  ExcelApp := CreateOleObject('Excel.Application');
  ExcelApp.Application.EnableEvents := false;
  //� ������� �����
  Workbook := ExcelApp.WorkBooks.Add;

  for i:=1 to 20 do arCol[i]:=-1; //������� ��� �������

  iCol:=Vi.DataController.Groups.GroupingItemCount; //����� ����� �����
  for i:=0 to iCol-1 do
  begin
    arCol[i+1]:=Vi.DataController.Groups.GroupingItemIndex[i];
  end;

  iGCount:=Vi.DataController.Groups.GroupingItemCount;
  iCol:=Vi.VisibleColumnCount;

  iRow:=Vi.DataController.RowCount+1;

  iRowV:=Vi.DataController.RowCount;
//  iJ:=Vi.DataController.ItemCount;
//  iJ:=Vi.VisibleColumnCount;

  ArrayData := VarArrayCreate([1,iRow, 1, iCol+iGCount], varVariant);
  for i:=iGCount+1 to iCol+iGCount do
  begin
    ArrayData[1,i] := Vi.VisibleColumns[i-1-iGCount].Caption;
  end;

  iRow:=2;

  for i:=0 to iRowV-1 do
  begin
    with Vi.DataController do
    begin
      RowInf:=GetRowInfo(i);
      if IsGroupingRow(RowInf,Vi.DataController)=False then
      begin
        k:=1+iGCount;
        for j:=0 to Vi.ColumnCount-1 do
        begin
          if Vi.Columns[j].Visible then
          begin
            StrWk:=GetRowDisplayText(RowInf,j);
            if pos('�',StrWk)=length(StrWk) then delete(StrWk,length(StrWk),1);
            if pos('%',StrWk)=length(StrWk) then delete(StrWk,length(StrWk),1);

            rVal:=StrToFloatDef(StrWk,1000000);
            if rVal<>1000000 then  ArrayData[iRow,k]:=rVal
            else ArrayData[iRow,k]:=StrWk;
            inc(k);
          end;
        end
      end else
      begin
        iLev:=RowInf.Level;
        iItem:=Groups.GroupingItemIndex[iLev];

        ArrayData[iRow,iLev+1]:=Vi.Columns[iItem].Caption+': '+GetRowDisplayText(RowInf,iItem);

//        StrWk:=Summary.GroupSummaryText[i];

//        prStrToArr(StrWk); //��� �������������� ��� �� �����

{        for mm:=1 to 20 do
        begin
          if arSum[mm]<1000000000 then
          begin
            ArrayData[iRow,iLev+2+mm]:=arSum[mm];
          end;
        end;

GroupFooterSummaryTexts[RowIndex, Level, Index: Integer]
}
        iCountDG:=Summary.DefaultGroupSummaryItems.Count;
        for ll:=0 to iCountDG-1 do
        begin
          SumGr:=(Summary.DefaultGroupSummaryItems.Items[ll] as tcxGridDBTableSummaryItem);
          if SumGr.Position=spFooter then
          begin
            sField:=SumGr.FieldName;
            sSum:=Summary.GroupFooterSummaryTexts[i,iLev,ll];

            for cc:=iGCount+1 to iCol+iGCount do
            begin
              sColumn:=Vi.VisibleColumns[cc-1-iGCount].Name;
              delete(sColumn,1,Length(Vi.Name)); //������ �� �������� ������� �������� View
              if sColumn=sField then //����� �������
              begin
                ff:=cc;

                rVal:=StrToFloatDef(sSum,1000000);
                if rVal<>1000000 then  ArrayData[iRow,ff]:=rVal
                else ArrayData[iRow,ff]:=sSum;

//                ArrayData[iRow,ff]:=sSum;
              end;
            end;
          end;
        end;


      end;
      inc(iRow);
    end;
  end;

  Cell1 := WorkBook.WorkSheets[1].Cells[1,1];
  Cell2 := WorkBook.WorkSheets[1].Cells[1+iRowV,iCol+iGCount];
  Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
  Range.Value := ArrayData;

//  Vi.EndUpdate;

  ExcelApp.Visible := true;
end;



procedure prNExportExel3(Vi:tcxGridDBTableView);
Var ExcelApp, Workbook, Range, Cell1, Cell2, ArrayData  : Variant;
    iCol,iRow,i:Integer;
    RowInf:TcxRowInfo;
    iRowV,J,k,iGCount,iLev,iItem:Integer;
    StrWk:String;
    arCol:array[1..20] of integer;
    arSum:Array[1..20] of Real;
    iCountDG,ll,cc,ff:INteger;
    sField,sColumn:String;
    SumGr:tcxGridDBTableSummaryItem;

procedure prStrToArr(S:String);
Var n:Integer;
    s2:String;
begin
  for n:=1 to 20 do arSum[n]:=1000000000; //������� ��� �������
  while pos('(',S)>0 do delete(S,pos('(',S),1);
  while pos(')',S)>0 do delete(S,pos(')',S),1);
  while pos('�',S)>0 do delete(S,pos('�',S),1);
  while pos('.',S)>0 do delete(S,pos('.',S),1);
  while pos(',',S)>0 do
  begin
    if pos(',',S)>0 then S[pos(',',S)]:='&';
    if pos(',',S)>0 then S[pos(',',S)]:='^';
  end;
  while pos('&',S)>0 do S[pos('&',S)]:=',';
  n:=0;
  while S>'' do
  begin
    if Pos('^',S)>0 then
    begin
      S2:=Copy(S,1,Pos('^',S)-1);
      delete(S,1,pos('^',S));
    end else
    begin
      S2:=S;
      S:='';
    end;
    inc(n);
    arSum[n]:=StrToFloatDef(S2,0);
  end;

end;

function IsGroupingRow(ARowInfo: TcxRowInfo; ADataController: TcxCustomDataController): Boolean;
begin
  with ADataController do
    Result := ARowInfo.Level <> Groups.GroupingItemCount;
end;

begin
//������� � ������
//  Vi.BeginUpdate;

  if not IsOLEObjectInstalled('Excel.Application') then exit;
  ExcelApp := CreateOleObject('Excel.Application');
  ExcelApp.Application.EnableEvents := false;
  //� ������� �����
  Workbook := ExcelApp.WorkBooks.Add;

  for i:=1 to 20 do arCol[i]:=-1; //������� ��� �������

  iCol:=Vi.DataController.Groups.GroupingItemCount; //����� ����� �����
  for i:=0 to iCol-1 do
  begin
    arCol[i+1]:=Vi.DataController.Groups.GroupingItemIndex[i];
  end;

  iGCount:=Vi.DataController.Groups.GroupingItemCount;
  iCol:=Vi.VisibleColumnCount;

  iRow:=Vi.DataController.RowCount+1;

  iRowV:=Vi.DataController.RowCount;
//  iJ:=Vi.DataController.ItemCount;
//  iJ:=Vi.VisibleColumnCount;

  ArrayData := VarArrayCreate([1,iRow, 1, iCol+iGCount], varVariant);
  for i:=iGCount+1 to iCol+iGCount do
  begin
    ArrayData[1,i] := Vi.VisibleColumns[i-1-iGCount].Caption;
  end;

  iRow:=2;

  for i:=0 to iRowV-1 do
  begin
    with Vi.DataController do
    begin
      RowInf:=GetRowInfo(i);
      if IsGroupingRow(RowInf,Vi.DataController)=False then
      begin
        k:=1+iGCount;
        for j:=0 to Vi.ColumnCount-1 do
        begin
          if Vi.Columns[j].Visible then
          begin
            StrWk:=GetRowDisplayText(RowInf,j);
            if pos('�',StrWk)=length(StrWk) then delete(StrWk,length(StrWk),1);
            ArrayData[iRow,k] :=StrWk;
            inc(k);
          end;
        end
      end else
      begin
        iLev:=RowInf.Level;
        iItem:=Groups.GroupingItemIndex[iLev];

        ArrayData[iRow,iLev+1]:=Vi.Columns[iItem].Caption+': '+GetRowDisplayText(RowInf,iItem);

        StrWk:=Summary.GroupSummaryText[i];

        prStrToArr(StrWk); //��� �������������� ��� �� �����

{        for mm:=1 to 20 do
        begin
          if arSum[mm]<1000000000 then
          begin
            ArrayData[iRow,iLev+2+mm]:=arSum[mm];
          end;
        end;

GroupFooterSummaryTexts[RowIndex, Level, Index: Integer]
}
        iCountDG:=Summary.DefaultGroupSummaryItems.Count;
        for ll:=0 to iCountDG-1 do
        begin
           SumGr:=(Summary.DefaultGroupSummaryItems.Items[ll] as tcxGridDBTableSummaryItem);
          if SumGr.Position=spGroup then
          begin
            sField:=SumGr.FieldName;
//            ArrayData[iRow,iLev+5+ll]:=sField;

            for cc:=iGCount+1 to iCol+iGCount do
            begin
              sColumn:=Vi.VisibleColumns[cc-1-iGCount].Name;
              delete(sColumn,1,Length(Vi.Name)); //������ �� �������� ������� �������� View
              if sColumn=sField then //����� �������
              begin
                ff:=cc;
                if arSum[ll+1]<1000000000 then
                begin
                  ArrayData[iRow,ff]:=arSum[ll+1];
                end;

              end;
            end;
          end;
        end;


      end;
      inc(iRow);
    end;
  end;

  Cell1 := WorkBook.WorkSheets[1].Cells[1,1];
  Cell2 := WorkBook.WorkSheets[1].Cells[1+iRowV,iCol+iGCount];
  Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
  Range.Value := ArrayData;

//  Vi.EndUpdate;

  ExcelApp.Visible := true;
end;


procedure prExportExel3(Vi:tcxGridDBTableView;dsT:tDataSource;taT:tdxMemData);
Var ExcelApp, Workbook, Range, Cell1, Cell2, ArrayData  : Variant;
    iCol,iRow,i:Integer;
    NameF:String;
begin
//������� � ������
  Vi.BeginUpdate;

  dsT.DataSet:=nil;
  try
    taT.Filter:=Vi.DataController.Filter.FilterText;
    taT.Filtered:=True;

      //����� ������ ������
    if not IsOLEObjectInstalled('Excel.Application') then exit;
    ExcelApp := CreateOleObject('Excel.Application');
    ExcelApp.Application.EnableEvents := false;
    //� ������� �����
    Workbook := ExcelApp.WorkBooks.Add;

    iCol:=Vi.VisibleColumnCount;
    iRow:=taT.RecordCount+1;

    ArrayData := VarArrayCreate([1,iRow, 1, iCol], varVariant);
    for i:=1 to iCol do
    begin
      ArrayData[1,i] := Vi.VisibleColumns[i-1].Caption;
    end;

    taT.First;
    iRow:=2;
    While not taT.eof do
    begin
      for i:=1 to iCol do
      begin
        NameF:=Vi.VisibleColumns[i-1].Name;
        delete(NameF,1,Length(Vi.Name)); //������ �� �������� ������� �������� View
        ArrayData[iRow,i] := taT.Fields.FieldByName(NameF).Value;
      end;
      taT.Next; //Delay(10);
      inc(iRow);
    end;

    Cell1 := WorkBook.WorkSheets[1].Cells[1,1];
    Cell2 := WorkBook.WorkSheets[1].Cells[1+taT.RecordCount,iCol];
    Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
    Range.Value := ArrayData;

    taT.Filter:='';
    taT.Filtered:=False;

    taT.First;

    dsT.DataSet:=taT;

    Vi.EndUpdate;

    ExcelApp.Visible := true;
  except
    showmessage('������ ��� ������������... ���������� � ������� ���������.')
  end;
end;


procedure CloseTe(taT:tdxMemData);
begin
  taT.Close;
  if taT.Active then
  begin
    taT.First;
    while not taT.Eof do taT.Delete;
    taT.Active:=False;
  end;
//  taT.Free;
//  taT.Create(Application);
  taT.Open;
  if taT.Active then
  begin
    taT.First;
    while not taT.Eof do taT.Delete;
  end;  
end;

Function fDifStr(iT:Integer;S:String):String;
Var SS:String;
begin
  result:='';
  if iT=1 then
  begin
    if pos('|',S)>0 then
    begin
      result:=Copy(S,1,pos('|',S)-1);
    end else result:=S;
  end;
  if iT=2 then
  begin
    if pos('|',S)>0 then
    begin
      SS:=S;
      delete(SS,1,pos('|',S));
      result:=SS;
    end else Result:='';
  end;
end;

procedure CloseTa(taT:tClientDataSet);
begin
 if taT.Active then
 begin
   taT.Close;
//   taT.Free;
   Delay(50);
   taT.CreateDataSet;
 end else taT.CreateDataSet;
end;

Procedure MenuExpandLevel( Node : TTreeNode; Tree:TcxTreeView; quTree:TpFIBDataSet;IDH:INteger);
Var ID , i   : Integer;
    TreeNode : TTreeNode;
Begin
// ��� ������ �������� ������ ������� ������ ���,
// ��� �� ����� ���������.
  Tree.Items.BeginUpdate;

  if Node = nil then
  begin
    ID:=0;
    Tree.Items.Clear;
  end
  else ID:=Integer(Node.Data);
  quTree.Close;
  quTree.ParamByName('PARENTID').Value:=ID;
  quTree.ParamByName('IDH').Value:=IDH;
  quTree.Open;
  // ��� ������ ������ �� ����������� ������ ������
  // ��������� ����� � TreeView, ��� �������� ����� � ���,
  // ������� �� ������ ��� "��������"
  for i:=1 to quTree.RecordCount do
  begin    // ������� � ���� Data ����� �� ����������������� �����(ID) � �������
    TreeNode:=Tree.Items.AddChildObject(Node, quTree.FieldByName('Name').AsString, Pointer(quTree.FieldByName('ID').AsInteger));
    TreeNode.ImageIndex:=8;
    TreeNode.SelectedIndex:=7;
    // ������� ��������� (������) �������� ����� ������ ��� ����,
    // ����� ��� ��������� [+] �� ����� � �� ����� ���� �� ��������
    Tree.Items.AddChildObject(TreeNode,'', nil);
    quTree.Next;
  end;
  Tree.Items.EndUpdate;
end;



procedure prWH(Strwk_: string;Me:TcxMemo);
var F: TextFile;
    Strwk1:String;
    FileN:String;
begin
  try
    if not DirectoryExists(CommonSet.PathHistory) then
    begin
      exit;
    end;

    strwk1:=FormatDateTime('yyyy_mm_dd',Date);
    Strwk1:=StrWk1+'.tolog';
    Application.ProcessMessages;

    FileN:=CommonSet.PathHistory+strwk1;

    AssignFile(F, FileN);
    if Not FileExists(FileN) then Rewrite(F)
    else                           Append(F);
    Write(F, FormatDateTime('HH:NN:SS ;', Now));
    WriteLn(F, Strwk_);
    Flush(F);

    if bPrintMemo then
    begin
      if Me.Lines.Count>500 then Me.Clear;
      Me.Lines.Add(Strwk_);
    end;
  finally
    CloseFile(F);
  end;
end;



Function prDefFormatStr(l:ShortInt;sN:String;rS:Real):String;
Var StrWk,StrWk1:String;
begin
  StrWk:=sN;
  Str(rS:8:2,StrWk1);
  StrWk1:=DelSp(StrWk1);
  StrWk1:='='+StrWk1+' ���';
  while (Length(StrWk)+Length(StrWk1))<l do StrWk:=StrWk+' ';
  Result:=StrWk+StrWk1;
end;

Function prDefFormatStr1(l:ShortInt;sN:String;rS:Real):String;
Var StrWk,StrWk1:String;
begin
  StrWk:=sN;
  Str(rS:8:2,StrWk1);
  StrWk1:=DelSp(StrWk1);
  StrWk1:=StrWk1+' ���';
  while (Length(StrWk)+Length(StrWk1))<l do StrWk:=StrWk+' ';
  Result:=StrWk+StrWk1;
end;

Function prDefFormatStr2(l:ShortInt;sN:String;rS:Integer):String;
Var StrWk,StrWk1:String;
begin
  StrWk:=sN;
  StrWk1:=IntToStr(rS);
  while (Length(StrWk)+Length(StrWk1))<l do StrWk:=StrWk+' ';
  Result:=StrWk+StrWk1;
end;


Procedure ResetAddVars;
begin
  bDrIn:=False;
  bDrRet:=False;
  bDrVn:=False;
  bDM:=False;
  bDInv:=False;
  bDCompl:=False;
  bDAct:=False;
  bDrRet:=False;
  bDrR:=False;
  bStartGoods:=False;
end;

procedure prExportExel2(Vi:tcxGridDBBandedTableView;dsT:tDataSource;taT:tClientDataSet);
Var ExcelApp, Workbook, Range, Cell1, Cell2, ArrayData  : Variant;
    iCol,iRow,i:Integer;
    NameF:String;
begin
//������� � ������
  Vi.BeginUpdate;

  dsT.DataSet:=nil;
  try
    taT.Filter:=Vi.DataController.Filter.FilterText;
    taT.Filtered:=True;

      //����� ������ ������
    if not IsOLEObjectInstalled('Excel.Application') then exit;
    ExcelApp := CreateOleObject('Excel.Application');
    ExcelApp.Application.EnableEvents := false;
    //� ������� �����
    Workbook := ExcelApp.WorkBooks.Add;

    iCol:=Vi.VisibleColumnCount;
    iRow:=taT.RecordCount+1;

    ArrayData := VarArrayCreate([1,iRow, 1, iCol], varVariant);
    for i:=1 to iCol do
    begin
      ArrayData[1,i] := Vi.VisibleColumns[i-1].Caption;
    end;

    taT.First;
    iRow:=2;
    While not taT.eof do
    begin
      for i:=1 to iCol do
      begin
        NameF:=Vi.VisibleColumns[i-1].Name;
        delete(NameF,1,Length(Vi.Name)); //������ �� �������� ������� �������� View
        ArrayData[iRow,i] := taT.Fields.FieldByName(NameF).Value;
      end;
      taT.Next; //Delay(10);
      inc(iRow);
    end;

    Cell1 := WorkBook.WorkSheets[1].Cells[1,1];
    Cell2 := WorkBook.WorkSheets[1].Cells[1+taT.RecordCount,iCol];
    Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
    Range.Value := ArrayData;

    taT.Filter:='';
    taT.Filtered:=False;

    taT.First;

    dsT.DataSet:=taT;

    Vi.EndUpdate;

    ExcelApp.Visible := true;
  except
    showmessage('������ ��� ������������... ���������� � ������� ���������.')
  end;
end;

procedure prExportExel4(Vi:tcxGridDBBandedTableView;dsT:tDataSource;taT:tdxMemData);
Var ExcelApp, Workbook, Range, Cell1, Cell2, ArrayData  : Variant;
    iCol,iRow,i:Integer;
    NameF:String;
begin
//������� � ������
  Vi.BeginUpdate;

  dsT.DataSet:=nil;
  try
    taT.Filter:=Vi.DataController.Filter.FilterText;
    taT.Filtered:=True;

      //����� ������ ������
    if not IsOLEObjectInstalled('Excel.Application') then exit;
    ExcelApp := CreateOleObject('Excel.Application');
    ExcelApp.Application.EnableEvents := false;
    //� ������� �����
    Workbook := ExcelApp.WorkBooks.Add;

    iCol:=Vi.VisibleColumnCount;
    iRow:=taT.RecordCount+1;

    ArrayData := VarArrayCreate([1,iRow, 1, iCol], varVariant);
    for i:=1 to iCol do
    begin
      ArrayData[1,i] := Vi.VisibleColumns[i-1].Caption;
    end;

    taT.First;
    iRow:=2;
    While not taT.eof do
    begin
      for i:=1 to iCol do
      begin
        NameF:=Vi.VisibleColumns[i-1].Name;
        delete(NameF,1,Length(Vi.Name)); //������ �� �������� ������� �������� View
        ArrayData[iRow,i] := taT.Fields.FieldByName(NameF).Value;
      end;
      taT.Next; //Delay(10);
      inc(iRow);
    end;

    Cell1 := WorkBook.WorkSheets[1].Cells[1,1];
    Cell2 := WorkBook.WorkSheets[1].Cells[1+taT.RecordCount,iCol];
    Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
    Range.Value := ArrayData;

    taT.Filter:='';
    taT.Filtered:=False;

    taT.First;

    dsT.DataSet:=taT;

    Vi.EndUpdate;

    ExcelApp.Visible := true;
  except
    showmessage('������ ��� ������������... ���������� � ������� ���������.')
  end;
end;

procedure prExportExelb(Vi:tcxGridDBBandedTableView;dsT:tDataSource;taT:tdxMemData);
Var ExcelApp, Workbook, Range, Cell1, Cell2, ArrayData  : Variant;
    iCol,iRow,i:Integer;
    NameF:String;
begin
//������� � ������
  Vi.BeginUpdate;

  dsT.DataSet:=nil;
  try
    taT.Filter:=Vi.DataController.Filter.FilterText;
    taT.Filtered:=True;

      //����� ������ ������
    if not IsOLEObjectInstalled('Excel.Application') then exit;
    ExcelApp := CreateOleObject('Excel.Application');
    ExcelApp.Application.EnableEvents := false;
    //� ������� �����
    Workbook := ExcelApp.WorkBooks.Add;

    iCol:=Vi.VisibleColumnCount;
    iRow:=taT.RecordCount+1;

    ArrayData := VarArrayCreate([1,iRow, 1, iCol], varVariant);
    for i:=1 to iCol do
    begin
      ArrayData[1,i] := Vi.VisibleColumns[i-1].Caption;
    end;

    taT.First;
    iRow:=2;
    While not taT.eof do
    begin
      for i:=1 to iCol do
      begin
        NameF:=Vi.VisibleColumns[i-1].Name;
        delete(NameF,1,Length(Vi.Name)); //������ �� �������� ������� �������� View
        ArrayData[iRow,i] := taT.Fields.FieldByName(NameF).Value;
      end;
      taT.Next; //Delay(10);
      inc(iRow);
    end;

    Cell1 := WorkBook.WorkSheets[1].Cells[1,1];
    Cell2 := WorkBook.WorkSheets[1].Cells[1+taT.RecordCount,iCol];
    Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
    Range.Value := ArrayData;

    taT.Filter:='';
    taT.Filtered:=False;

    taT.First;

    dsT.DataSet:=taT;

    Vi.EndUpdate;

    ExcelApp.Visible := true;
  except
    showmessage('������ ��� ������������... ���������� � ������� ���������.')
  end;
end;

Function fTestKey(Key:Word):Boolean;
begin
  Result:=False;
  if (Key=$30)
  or (Key=$31)
  or (Key=$32)
  or (Key=$33)
  or (Key=$34)
  or (Key=$35)
  or (Key=$36)
  or (Key=$37)
  or (Key=$38)
  or (Key=$39)
  or (Key=$60)
  or (Key=$61)
  or (Key=$62)
  or (Key=$63)
  or (Key=$64)
  or (Key=$65)
  or (Key=$66)
  or (Key=$67)
  or (Key=$68)
  or (Key=$69)
  then Result:=True;
end;

function FlToS(rS:Real):String;
Var S:String;
begin
  Result:='0.00';
  Str(rS:10:2,S);
  while pos(' ',S)>0 do delete(S,Pos(' ',S),1);
  while pos(',',S)>0 do S[Pos(',',S)]:='.';
  Result:=S;
end;

function IsOLEObjectInstalled(Name: String): boolean;
var
  ClassID: TCLSID;
  Rez : HRESULT;
begin

// L��� CLSID OLE-������
  Rez := CLSIDFromProgID(PWideChar(WideString(Name)), ClassID);
  if Rez = S_OK then
 // +���� ������
    Result := true
  else
    Result := false;
end;

procedure prExportExel1(Vi:tcxGridDBTableView;dsT:tDataSource;taT:tClientDataSet);
Var ExcelApp, Workbook, Range, Cell1, Cell2, ArrayData  : Variant;
    iCol,iRow,i:Integer;
    NameF:String;
begin
//������� � ������
  Vi.BeginUpdate;

  dsT.DataSet:=nil;
  try
    taT.Filter:=Vi.DataController.Filter.FilterText;
    taT.Filtered:=True;

      //����� ������ ������
    if not IsOLEObjectInstalled('Excel.Application') then exit;
    ExcelApp := CreateOleObject('Excel.Application');
    ExcelApp.Application.EnableEvents := false;
    //� ������� �����
    Workbook := ExcelApp.WorkBooks.Add;

    iCol:=Vi.VisibleColumnCount;
    iRow:=taT.RecordCount+1;

    ArrayData := VarArrayCreate([1,iRow, 1, iCol], varVariant);
    for i:=1 to iCol do
    begin
      ArrayData[1,i] := Vi.VisibleColumns[i-1].Caption;
    end;

    taT.First;
    iRow:=2;
    While not taT.eof do
    begin
      for i:=1 to iCol do
      begin
        NameF:=Vi.VisibleColumns[i-1].Name;
        delete(NameF,1,Length(Vi.Name)); //������ �� �������� ������� �������� View
        ArrayData[iRow,i] := taT.Fields.FieldByName(NameF).Value;
      end;
      taT.Next; //Delay(10);
      inc(iRow);
    end;

    Cell1 := WorkBook.WorkSheets[1].Cells[1,1];
    Cell2 := WorkBook.WorkSheets[1].Cells[1+taT.RecordCount,iCol];
    Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
    Range.Value := ArrayData;

    taT.Filter:='';
    taT.Filtered:=False;

    taT.First;

    dsT.DataSet:=taT;

    Vi.EndUpdate;

    ExcelApp.Visible := true;
  except
    showmessage('������ ��� ������������... ���������� � ������� ���������.')
  end;
end;

procedure prExportExel(Vi:tcxGridDBTableView;dsT:tDataSource;taT:tpFIBDataSet);
Var ExcelApp, Workbook, Range, Cell1, Cell2, ArrayData  : Variant;
    iCol,iRow,i:Integer;
    NameF:String;
begin
//������� � ������
  Vi.BeginUpdate;

  dsT.DataSet:=nil;
  try
    taT.Filter:=Vi.DataController.Filter.FilterText;
    taT.Filtered:=True;

      //����� ������ ������
    if not IsOLEObjectInstalled('Excel.Application') then exit;
    ExcelApp := CreateOleObject('Excel.Application');
    ExcelApp.Application.EnableEvents := false;
    //� ������� �����
    Workbook := ExcelApp.WorkBooks.Add;

    iCol:=Vi.VisibleColumnCount;
    iRow:=taT.RecordCount+1;

    ArrayData := VarArrayCreate([1,iRow, 1, iCol], varVariant);
    for i:=1 to iCol do
    begin
      ArrayData[1,i] := Vi.VisibleColumns[i-1].Caption;
    end;

    taT.First;
    iRow:=2;
    While not taT.eof do
    begin
      for i:=1 to iCol do
      begin
        NameF:=Vi.VisibleColumns[i-1].Name;
        delete(NameF,1,Length(Vi.Name)); //������ �� �������� ������� �������� View
        ArrayData[iRow,i] := taT.Fields.FieldByName(NameF).Value;
      end;
      taT.Next; //Delay(10);
      inc(iRow);
    end;

    Cell1 := WorkBook.WorkSheets[1].Cells[1,1];
    Cell2 := WorkBook.WorkSheets[1].Cells[1+taT.RecordCount,iCol];
    Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
    Range.Value := ArrayData;

    taT.Filter:='';
    taT.Filtered:=False;

    taT.First;

    dsT.DataSet:=taT;

    Vi.EndUpdate;

    ExcelApp.Visible := true;
  except
    showmessage('������ ��� ������������... ���������� � ������� ���������.')
  end;
end;





Function SOnlyDigit(S:String):String;
Var i:Integer;
begin
  result:='';
  for i:=1 to length(S) do
  begin
    while pos(#186,S)>0 do delete(S,pos(#186,S),1);
    while pos(#187,S)>0 do delete(S,pos(#187,S),1);
    if s[i] in ['0','1','2','3','4','5','6','7','8','9','='] then
    begin
      Result:=Result+s[i];
    end;
  end;
  if result='' then result:='0';
end;


Function prDateToI(tD:TDateTime):INteger;
begin
  Result:=StrToINtDef(FormatDateTime('mm',tD)+FormatDateTime('dd',tD),0);
end;

Function ToStandart(S:String):String;
var StrWk,Str1:String;
    iBar: array[1..13] of integer;
    n,c,n1:Integer;
begin
  StrWk:=S;
  while Length(StrWk)<12 do StrWk:=StrWk+'0';
  for n:=1 to 12 do
  begin
    str1:=Copy(StrWk,n,1);
    iBar[n]:=StrToIntDef(Str1,0);
  end;
  //220123401000C
  c:=0;
  n:=(iBar[2]+iBar[4]+iBar[6]+iBar[8]+iBar[10]+iBar[12])*3+(iBar[1]+iBar[3]+iBar[5]+iBar[7]+iBar[9]+iBar[11]);
  for n1:=0 to 9 do
  begin
    if ((n+n1) mod 10)=0 then
    begin
      c:=n1;
      break;
    end;
  end;
  iBar[13]:=c;
  Str1:='';
  for n:=1 to 13 do str1:=Str1+IntToStr(iBar[n]);

  strwk:=Str1;
  Result:=StrWk;
end;


Procedure ClassifEx( Node : TTreeNode; Tree:TcxTreeView; quTree:TpFIBDataSet; CLTYPE:Integer; NAMEF:String);
Var ID , i   : Integer;
    TreeNode : TTreeNode;
Begin
// ��� ������ �������� ������ ������� ������ ���,
// ��� �� ����� ���������.
  if Node = nil then ID:=0
  else ID:=Integer(Node.Data);
  quTree.Close;
  quTree.ParamByName('PARENTID').Value:=ID;
  quTree.ParamByName('CLTYPE').Value:=CLTYPE;
  quTree.Open;
  Tree.Items.BeginUpdate;
  for i:=1 to quTree.RecordCount do
  begin    // ������� � ���� Data ����� �� ����������������� �����(ID) � �������
    TreeNode:=Tree.Items.AddChildObject(Node, quTree.FieldByName(NameF).AsString, Pointer(quTree.FieldByName('ID').AsInteger));
    TreeNode.ImageIndex:=8;
    TreeNode.SelectedIndex:=7;

    // ������� ��������� (������) �������� ����� ������ ��� ����,
    // ����� ��� ��������� [+] �� ����� � �� ����� ���� �� ��������
    Tree.Items.AddChildObject(TreeNode,'', nil);
    quTree.Next;
  end;
  Tree.Items.EndUpdate;
end;



Procedure MessExpandLevel( Node : TTreeNode; Tree:TTreeView; quTree:TpFIBDataSet);
Var ID , i   : Integer;
    TreeNode : TTreeNode;
    StrWk:String;
    k:Real;
Begin
// ��� ������ �������� ������ ������� ������ ���,
// ��� �� ����� ���������.
  if Node = nil then ID:=0
  else ID:=Integer(Node.Data);
  quTree.Close;
  quTree.ParamByName('PARENTID').Value:=ID;
  quTree.Open;
  Tree.Items.BeginUpdate;
  // ��� ������ ������ �� ����������� ������ ������
  // ��������� ����� � TreeView, ��� �������� ����� � ���,
  // ������� �� ������ ��� "��������"
  for i:=1 to quTree.RecordCount do
  begin    // ������� � ���� Data ����� �� ����������������� �����(ID) � �������
    k:=RoundEx(quTree.FieldByName('KOEF').AsFloat*1000)/1000;

    StrWk:=quTree.FieldByName('NAMEFULL').AsString+' ('+quTree.FieldByName('NAMESHORT').AsString+' x'+FloatToStr(k)+')';
    TreeNode:=Tree.Items.AddChildObject(Node, StrWk, Pointer(quTree.FieldByName('ID').AsInteger));
    TreeNode.ImageIndex:=26;
    TreeNode.SelectedIndex:=25;
    // ������� ��������� (������) �������� ����� ������ ��� ����,
    // ����� ��� ��������� [+] �� ����� � �� ����� ���� �� ��������
    Tree.Items.AddChildObject(TreeNode,'', nil);
    quTree.Next;
  end;
  Tree.Items.EndUpdate;
end;

procedure prWriteLogPS(Strwk_: string);
var F: TextFile;
    Strwk1:String;
    FileN:String;
begin
  if CommonSet.WriteLog<>1 then exit;
  try
    if not DirectoryExists(CommonSet.PathHistory) then
    begin
      exit;
    end;

    strwk1:=FormatDateTime('yyyy_mm_dd',Date);
    Strwk1:=StrWk1+'.ps';
    Application.ProcessMessages;

    FileN:=CommonSet.PathHistory+strwk1;

    AssignFile(F, FileN);
    if Not FileExists(FileN) then Rewrite(F)
    else                           Append(F);
    Write(F, FormatDateTime('HH:NN:SS ;', Now));
    WriteLn(F, Strwk_);
    Flush(F);
  finally
    CloseFile(F);
  end;
end;



procedure prWriteLog(Strwk_: string);
var F: TextFile;
    Strwk1:String;
    FileN:String;
begin
  if CommonSet.WriteLog<>1 then exit;
  try
    if not DirectoryExists(CommonSet.PathArh) then
    begin
      exit;
    end;

    strwk1:=FormatDateTime('yyyy_mm_dd',Date);
    Strwk1:=StrWk1+'.log';
    Application.ProcessMessages;

    FileN:=CommonSet.PathArh+strwk1;

    AssignFile(F, FileN);
    if Not FileExists(FileN) then Rewrite(F)
    else                           Append(F);
    Write(F, FormatDateTime('HH:NN:SS ;', Now));
    WriteLn(F, Strwk_);
    Flush(F);
  finally
    CloseFile(F);
  end;
end;

function RoundVal( X: Double ): Double;
var  ScaledFractPart:Integer;
     Temp : Double;

begin

 ScaledFractPart := Trunc(X*100);
 Temp := Trunc(Frac(X*100)*1000000)+1;
 if Temp >=  500000 then ScaledFractPart := ScaledFractPart + 1;
 if Temp <= -500000 then ScaledFractPart := ScaledFractPart - 1;
{ if Temp >=  0.5 then ScaledFractPart := ScaledFractPart + 1;
 if Temp <= -0.5 then ScaledFractPart := ScaledFractPart - 1;}
 RoundVal := ScaledFractPart/100;
end;


function RoundEx( X: Double ): Integer;
var  ScaledFractPart:Integer;
     Temp : Double;
begin
 ScaledFractPart := Trunc(X);
 Temp := Trunc(Frac(X)*1000000)+1;
 if Temp >=  500000 then ScaledFractPart := ScaledFractPart + 1;
 if Temp <= -500000 then ScaledFractPart := ScaledFractPart - 1;
{ if Temp >=  0.5 then ScaledFractPart := ScaledFractPart + 1;
 if Temp <= -0.5 then ScaledFractPart := ScaledFractPart - 1;}
 RoundEx := ScaledFractPart;
end;


Function DelSp(sStr:String):String;
begin
  while pos(' ',sStr)>0 do delete(sStr,pos(' ',sStr),1);
  Result:=sStr;
end;

Procedure ClassifExpandLevel( Node : TTreeNode; Tree:TTreeView; quTree:TpFIBDataSet; PersonalId:Integer );
Var ID , i   : Integer;
    TreeNode : TTreeNode;
Begin
// ��� ������ �������� ������ ������� ������ ���,
// ��� �� ����� ���������.
  if Node = nil then ID:=0
  else ID:=Integer(Node.Data);
  quTree.Close;
  quTree.ParamByName('ParentID').Value:=ID;
  quTree.ParamByName('PersonalID').Value:=PersonalId;
  quTree.Open;
  Tree.Items.BeginUpdate;
  for i:=1 to quTree.RecordCount do
  begin    // ������� � ���� Data ����� �� ����������������� �����(ID) � �������
    TreeNode:=Tree.Items.AddChildObject(Node, quTree.FieldByName('Name').AsString, Pointer(quTree.FieldByName('ID').AsInteger));
    TreeNode.ImageIndex:=8;
    TreeNode.SelectedIndex:=7;
    // ������� ��������� (������) �������� ����� ������ ��� ����,
    // ����� ��� ��������� [+] �� ����� � �� ����� ���� �� ��������
    Tree.Items.AddChildObject(TreeNode,'', nil);
    quTree.Next;
  end;
  Tree.Items.EndUpdate;
end;


Procedure ClassifExpand( Node : TTreeNode; Tree:TcxTreeView; quTree:TpFIBDataSet; PersonalId,CLTYPE:Integer );
Var ID , i   : Integer;
    TreeNode : TTreeNode;
Begin
// ��� ������ �������� ������ ������� ������ ���,
// ��� �� ����� ���������.
  if Node = nil then ID:=0
  else ID:=Integer(Node.Data);
  quTree.Close;
  quTree.ParamByName('PARENTID').Value:=ID;
  quTree.ParamByName('PERSONID').Value:=PersonalId;
  quTree.ParamByName('CLTYPE').Value:=CLTYPE;
  quTree.Open;
  Tree.Items.BeginUpdate;
  for i:=1 to quTree.RecordCount do
  begin    // ������� � ���� Data ����� �� ����������������� �����(ID) � �������
    TreeNode:=Tree.Items.AddChildObject(Node, quTree.FieldByName('NAMECL').AsString, Pointer(quTree.FieldByName('ID').AsInteger));
    TreeNode.ImageIndex:=8;
    TreeNode.SelectedIndex:=7;

    // ������� ��������� (������) �������� ����� ������ ��� ����,
    // ����� ��� ��������� [+] �� ����� � �� ����� ���� �� ��������
    Tree.Items.AddChildObject(TreeNode,'', nil);
    quTree.Next;
  end;
  Tree.Items.EndUpdate;
end;



Procedure CardsExpandLevel( Node : TTreeNode; Tree:TTreeView; quTree:TpFIBDataSet);
Var ID , i   : Integer;
    TreeNode : TTreeNode;
Begin
// ��� ������ �������� ������ ������� ������ ���,
// ��� �� ����� ���������.
  if Node = nil then ID:=0
  else ID:=Integer(Node.Data);
  quTree.Close;
  quTree.ParamByName('PARENTID').Value:=ID;
//  quTree.ParamByName('PERSONID').Value:=Person.Id;
  quTree.Open;
  Tree.Items.BeginUpdate;
  // ��� ������ ������ �� ����������� ������ ������
  // ��������� ����� � TreeView, ��� �������� ����� � ���,
  // ������� �� ������ ��� "��������"
  for i:=1 to quTree.RecordCount do
  begin    // ������� � ���� Data ����� �� ����������������� �����(ID) � �������
    TreeNode:=Tree.Items.AddChildObject(Node, quTree.FieldByName('Name').AsString, Pointer(quTree.FieldByName('ID').AsInteger));
    if quTree.FieldByName('IACTIVE').AsInteger=1 then
    begin
      if quTree.FieldByName('LINK').AsInteger>0 then
      begin
        TreeNode.ImageIndex:=26;
        TreeNode.SelectedIndex:=27;
      end else
      begin
        TreeNode.ImageIndex:=8;
        TreeNode.SelectedIndex:=7;
      end;
    end else
    begin
      TreeNode.ImageIndex:=17;
      TreeNode.SelectedIndex:=18;
    end;

    // ������� ��������� (������) �������� ����� ������ ��� ����,
    // ����� ��� ��������� [+] �� ����� � �� ����� ���� �� ��������
    Tree.Items.AddChildObject(TreeNode,'', nil);
    quTree.Next;
  end;
  Tree.Items.EndUpdate;
end;



Function StrToMod(StrIn:String; Var vMod:TModRec):Boolean;
Var strwk:String;
    n:Integer;
begin
  result:=True;
  for n:=1 to 5 do
  begin
    Delete(StrIn,1,pos(r,strin));
    if pos(r,strin)>0 then strwk:=Copy(StrIn,1,pos(r,strin)-1);
    Case n of
    1: begin
         vMod.Id:=StrToIntDef(StrWk,-1);
         if vMod.Id<0 then result:=False;
       end;
    2: begin
         vMod.Name:=StrWk;
       end;
    3: begin
         vMod.Parent:=StrToIntDef(StrWk,-1);
         if vMod.Parent<0 then result:=False;
       end;
    4: begin
         vMod.Price:=StrToIntDef(StrWk,-1);
         if vMod.Price<0 then result:=False;
       end;
    5: begin
         if StrIn='' then StrIn:='0';
         vMod.RealPrice:=StrToIntDef(StrIn,-1);
         if vMod.RealPrice<0 then result:=False;
       end;
    end;
  end;
end;



Function StrToMenu(StrIn:String; Var vMenu:TMenu):Boolean;
Var strwk:String;
    n:Integer;
begin
  result:=True;
  for n:=1 to 28 do
  begin
    Delete(StrIn,1,pos(r,strin));
    if pos(r,strin)>0 then strwk:=Copy(StrIn,1,pos(r,strin)-1);
    if strwk='' then strwk:='0';
    while pos('.',strWk)>0 do StrWk[pos('.',strWk)]:=','; //��� ����� � ������� ��� ����������� ��������������
    Case n of
    1: begin
         vMenu.Sifr:=StrToIntDef(StrWk,-1);
         if vMenu.Sifr<0 then result:=False;
       end;
    2: begin
         vMenu.Name:=StrWk;
       end;
    3: begin
         vMenu.Price:=StrToFloatDef(StrWk,-1);
         if vMenu.Price<0 then result:=False;
       end;
    4: begin
         vMenu.Code:=StrWk;
       end;
    5: begin
         vMenu.TreeType:=StrWk;
       end;
    6: begin
         vMenu.LimitPrice:=StrToFloatDef(StrWk,-1);
         if vMenu.LimitPrice<0 then result:=False;
       end;
    7: begin
         vMenu.Categ:=StrToIntDef(StrWk,-1);
         if vMenu.Categ<0 then result:=False;
       end;
    8: begin
         vMenu.Parent:=StrToIntDef(StrWk,-1);
         if vMenu.Parent<0 then result:=False;
       end;
    9: begin
         vMenu.Link:=StrToIntDef(StrWk,-1);
         if vMenu.Link<0 then result:=False;
       end;
    10: begin
         vMenu.Stream:=StrToIntDef(StrWk,-1);
         if vMenu.Stream<0 then result:=False;
       end;
    11: begin
         vMenu.Lack:=StrToIntDef(StrWk,-1);
         if vMenu.Lack<0 then result:=False;
       end;
    12: begin
         vMenu.DesignSifr:=StrToIntDef(StrWk,-1);
         if vMenu.DesignSifr<0 then result:=False;
       end;
    13: begin
         if strwk='0' then strwk:='';
         vMenu.AltName:=StrWk;
       end;
    14: begin
         vMenu.Nalog:=StrToFloatDef(StrWk,-1);
         if vMenu.Nalog<0 then result:=False;
       end;
    15: begin
         if strwk='0' then strwk:='';
         vMenu.Barcode:=StrWk;
       end;
    16: begin
         vMenu.Image:=StrToIntDef(StrWk,-1);
         if vMenu.Image<0 then result:=False;
       end;
    17: begin
         vMenu.Consumma:=StrToFloatDef(StrWk,-1);
         if vMenu.Consumma<0 then result:=False;
       end;
    18: begin
         vMenu.MinRest:=StrToIntDef(StrWk,-1);
         if vMenu.MinRest<0 then result:=False;
       end;
    19: begin
         vMenu.PrnRest:=StrToIntDef(StrWk,-1);
         if vMenu.PrnRest<0 then result:=False;
       end;
    20: begin
         vMenu.CookTime:=StrToIntDef(StrWk,-1);
         if vMenu.CookTime<0 then result:=False;
       end;
    21: begin
         vMenu.Dispenser:=StrToIntDef(StrWk,-1);
         if vMenu.Dispenser<0 then result:=False;
       end;
    22: begin
         vMenu.DispKoef:=StrToIntDef(StrWk,-1);
         if vMenu.DispKoef<0 then result:=False;
       end;
    23: begin
         vMenu.Access:=StrToIntDef(StrWk,-1);
         if vMenu.Access<0 then result:=False;
       end;
    24: begin
         vMenu.Flags:=StrToIntDef(StrWk,-1);
         if vMenu.Flags<0 then result:=False;
       end;
    25: begin
         vMenu.Tara:=StrToIntDef(StrWk,-1);
         if vMenu.Tara<0 then result:=False;
       end;
    26: begin
         vMenu.CntPrice:=StrToIntDef(StrWk,-1);
         if vMenu.CntPrice<0 then result:=False;
       end;
    27: begin
         vMenu.BackBGR:=StrToFloatDef(StrWk,-1);
         if vMenu.BackBGR<0 then result:=False;
       end;
    28: begin
         if StrIn='' then strin:='0';
         vMenu.FontBGR:=StrToFloatDef(StrIn,-1);
         if vMenu.FontBGR<0 then result:=False;
       end;
    end;
  end;
end;



Function StrToCateg(StrIn:String; Var vCateg:TCategRec):Boolean;
Var strwk:String;
    n:Integer;
begin
  result:=True;
  for n:=1 to 3 do
  begin
    Delete(StrIn,1,pos(r,strin));
    if pos(r,strin)>0 then strwk:=Copy(StrIn,1,pos(r,strin)-1);
    Case n of
    1: begin
         vCateg.Id:=StrToIntDef(StrWk,-1);
         if vCateg.Id<0 then result:=False;
       end;
    2: begin
         vCateg.Name:=StrWk;
       end;
    3: begin
         vCateg.Tax_Group:=StrToIntDef(StrIn,-1);
         if vCateg.Tax_Group<0 then result:=False;
       end;
    end;
  end;
end;



Function StrToCard(StrIn:String; Var vCard:TCardRec):Boolean;
Var strwk:String;
    n:Integer;
begin
  result:=True;
  for n:=1 to 4 do
  begin
    Delete(StrIn,1,pos(r,strin));
    if pos(r,strin)>0 then strwk:=Copy(StrIn,1,pos(r,strin)-1);
    Case n of
    1: begin
         CardRec.Id:=StrToIntDef(StrWk,-1);
         if CardRec.Id<0 then result:=False;
       end;
    2: begin
         CardRec.Name:=StrWk;
       end;
    3: begin
         CardRec.Mesuriment:=StrToIntDef(StrWk,-1);
         if CardRec.Mesuriment<0 then result:=False;
       end;
    4: begin
         CardRec.Id_Classif:=StrToIntDef(StrIn,-1);
         if CardRec.Id_Classif<0 then result:=False;
       end;
    end;
  end;
end;


Function StrToMes(StrIn:String; Var vMes:TMesuriment):Boolean;
Var strwk:String;
    n:Integer;
begin
  result:=True;
  for n:=1 to 2 do
  begin
    Delete(StrIn,1,pos(r,strin));
    if pos(r,strin)>0 then strwk:=Copy(StrIn,1,pos(r,strin)-1);
    Case n of
    1: begin
         vMes.Id:=StrToIntDef(StrWk,-1);
         if vMes.Id<0 then result:=False;
       end;
    2: begin
         vMes.Name:=StrIn;
       end;
    end;
  end;
end;


Function StrToClassif(StrIn:String; Var ClRec:TClassifRec):Boolean;
Var strwk:String;
    n:Integer;
begin
  result:=True;
  for n:=1 to 4 do
  begin
    Delete(StrIn,1,pos(r,strin));
    if pos(r,strin)>0 then strwk:=Copy(StrIn,1,pos(r,strin)-1);
    Case n of
    1: begin
         ClRec.TYPE_CLASSIF:=StrToIntDef(StrWk,-1);
         if ClRec.TYPE_CLASSIF<0 then result:=False;
       end;
    2: begin
         ClRec.ID:=StrToIntDef(StrWk,-1);
         if ClRec.Id<0 then result:=False;
       end;
    3: begin
         ClRec.Id_Parent:=StrToIntDef(StrWk,-1);
         if ClRec.Id_Parent<0 then result:=False;
       end;
    4: begin
         ClRec.Name:=StrIn;
       end;
    end;
  end;
end;

Function StrToTrHead(StrIn:String; Var HRec:tTrebHRec):Boolean;
Var strwk:String;
    n:Integer;
begin
  result:=True;
  for n:=1 to 8 do
  begin
    Delete(StrIn,1,pos(r,strin));
    if pos(r,strin)>0 then strwk:=Copy(StrIn,1,pos(r,strin)-1);
    Case n of
    1: begin
         HRec.SID:=StrWk;
       end;
    2: begin
         HRec.DEPARTID:=StrWk;
       end;
    3: begin
         HRec.DEPARTNAME:=StrWk;
       end;
    4: begin
         HRec.DTYPE:=StrToIntDef(StrWk,-1);
         if HRec.DTYPE<0 then result:=False;
       end;
    5: begin
         try
           HRec.TREBDATE:=StrToDate(StrWk);
         except
           result:=False;
         end;
       end;
    6: begin
         try
           HRec.SENDDATE:=StrToDate(StrWk);
         except
           result:=False;
         end;
       end;
    7: begin
         HRec.CLTO:=StrToIntDef(StrWk,-1);
         if HRec.CLTO<0 then result:=False;
       end;
    8: begin
         HRec.Comment:=StrIn;
       end;
    end;
  end;
end;

Function StrToSpec(StrIn:String; Var SRec:tSpecRec):Boolean;
Var strwk:String;
    n:Integer;
begin
  result:=True;
  for n:=1 to 8 do
  begin
    Delete(StrIn,1,pos(r,strin));
    if pos(r,strin)>0 then strwk:=Copy(StrIn,1,pos(r,strin)-1);
    Case n of
    1: begin
         SRec.SID_HEAD:=StrWk;
       end;
    2: begin
         SRec.ID:=333; //�� ������������ ��� �������
       end;
    3: begin
         SRec.CARD_ID:=StrToIntDef(StrWk,-1);
         if SRec.CARD_ID<0 then result:=False;
       end;
    4: begin
         SRec.CARD1_ID:=StrToIntDef(StrWk,-1);
         if SRec.CARD1_ID<0 then result:=False;
       end;
    5: begin
         SRec.NUM:=StrToIntDef(StrWk,-1);
         if SRec.NUM<0 then result:=False;
       end;
    6: begin
         SRec.QUANT:=StrToFloatDef(StrWk,-1);
         if SRec.QUANT<0 then result:=False;
       end;
    7: begin
         SRec.GROUPCARD:=StrToIntDef(StrWk,-1);
         if SRec.GROUPCARD<0 then result:=False;
       end;
    8: begin
         SRec.GROUPCARD1:=StrToIntDef(StrIn,-1);
         if SRec.GROUPCARD1<0 then result:=False;
       end;
    end;
  end;
end;

procedure WriteTestLog(Strwk_: string);
var F: TextFile;
    FileN:String;
begin
  try
    Application.ProcessMessages;
    FileN:=CurDir+'Test.txt';
    AssignFile(F, FileN);
    if Not FileExists(FileN) then Rewrite(F)
    else                           Append(F);
    WriteLn(F, Strwk_);
    Flush(F);
  finally
    CloseFile(F);
  end;
end;



procedure WriteHistory(Strwk_: string);
var F: TextFile;
    Strwk1:String;
    FileN:String;
begin
  try
    strwk1:=FormatDateTime('yyyy_mm_dd',Date);
    Strwk1:=StrWk1+'.txt';
    Application.ProcessMessages;
    FileN:=CommonSet.PathHistory+strwk1;
    AssignFile(F, FileN);
    if Not FileExists(FileN) then Rewrite(F)
    else                           Append(F);
    Write(F, FormatDateTime('dd.mm.yyyy HH:NN:SS  ', Now));
    WriteLn(F, Strwk_);
    Flush(F);
  finally
    CloseFile(F);
  end;
end;

procedure TestDir;
begin
  if not DirectoryExists(CommonSet.PathExport) then
    if not CreateDir(CommonSet.PathExport) then
    raise Exception.Create('�� ���� ������� ����������� "'+CommonSet.PathExport+'"');
  if not DirectoryExists(CommonSet.PathImport) then
    if not CreateDir(CommonSet.PathImport) then
    raise Exception.Create('�� ���� ������� ����������� "'+CommonSet.PathImport+'"');
  if not DirectoryExists(CommonSet.PathHistory) then
    if not CreateDir(CommonSet.PathHistory) then
    raise Exception.Create('�� ���� ������� ����������� "'+CommonSet.PathHistory+'"');
  if not DirectoryExists(CommonSet.PathArh) then
    if not CreateDir(CommonSet.PathArh) then
    raise Exception.Create('�� ���� ������� ����������� "'+CommonSet.PathArh+'"');
end;

Function TestExch:Boolean;
Var F:TextFile;
begin
  Result:=False;
  if FileExists(CommonSet.PathImport+FileInd) then
  begin
    Result:=True;
    AssignFile(F,CommonSet.PathImport+FileInd);
    try
      Erase(F);
    except
      result:=False;
    end;
  end;
end;

Function TestExport:Boolean;
Var F:TextFile;
begin
  Result:=False;
  if FileExists(CommonSet.PathExport+FileInd) then
  begin
    Result:=True;
    AssignFile(F,CommonSet.PathExport+FileInd);
    try
      Erase(F);
    except
      result:=False;
    end;
  end;
end;


Procedure ReadIni;
Var f:TIniFile;
    s:String;
    ft:TextFile;
begin
  f:=TIniFile.create(CurDir+CurIni);

  DBName:=f.ReadString('Config_','DBName','C:\Database\Ust\Ust.GDB');
  CommonSet.OfficeDb:=f.ReadString('Config_','OfficeDBName','C:\Database\Ust\Ust.GDB');
  Person.Id:=f.ReadInteger('Config_','PersinId',0);
  CommonSet.AutoTake:=f.ReadInteger('Config_','AutoTake',1);
  CommonSet.CashNum:=f.ReadInteger('Config_','CashNum',0);
  CommonSet.CashChNum:=f.ReadInteger('Config_','CashChNum',1);
  CommonSet.CashZ:=f.ReadInteger('Config_','CashZ',1);
  CommonSet.CashPort:=f.ReadInteger('Config_','CashPort',1);
  CommonSet.ComDelay:=f.ReadInteger('Config_','ComDelay',100);
  CommonSet.CashSer:=f.ReadString('Config_','CashSer','');
  while pos(' ',CommonSet.CashSer)>0 do delete(CommonSet.CashSer,pos(' ',CommonSet.CashSer),1);

  CommonSet.PathImport:=f.ReadString('Config_','PathImport',CurDir+'Import\');
  CommonSet.PathExport:=f.ReadString('Config_','PathExport',CurDir+'Export\');
  CommonSet.FtpImport:=f.ReadString('Config_','FtpImport',CurDir+'temp\04\');
  CommonSet.PathHistory:=f.ReadString('Config_','PathHistory',CurDir+'History\');
  CommonSet.PathArh:=f.ReadString('Config_','PathArh',CurDir+'Arh\');
  CommonSet.DepartId:=f.ReadString('Config_','DepartId','10');
  CommonSet.DepartName:=f.ReadString('Config_','DepartName','����� �������� ��������� ���������� !!!');
  CommonSet.PeriodPing:=f.ReadInteger('Config_','PeriodPing',30);
  CommonSet.PathRkData:=f.ReadString('Config_','PathRkData',CurDir+'PathRkData');

  CommonSet.PGroup1:=f.ReadString('Config_','PGroup1','0');
  CommonSet.PGroup2:=f.ReadString('Config_','PGroup2','0');
  CommonSet.PGroup3:=f.ReadString('Config_','PGroup3','0');
  CommonSet.PGroup4:=f.ReadString('Config_','PGroup4','0');
  CommonSet.PGroup5:=f.ReadString('Config_','PGroup5','0');
  CommonSet.PGroupW:=f.ReadString('Config_','PGroupW','0');

  CommonSet.PGroup1N:=f.ReadString('Config_','PGroup1N','0');
  CommonSet.PGroup2N:=f.ReadString('Config_','PGroup2N','0');
  CommonSet.PGroup3N:=f.ReadString('Config_','PGroup3N','0');
  CommonSet.PGroup4N:=f.ReadString('Config_','PGroup4N','0');
  CommonSet.PGroup5N:=f.ReadString('Config_','PGroup5N','0');
  CommonSet.PGroupWN:=f.ReadString('Config_','PGroupWN','0');

  CommonSet.PRing1:=f.ReadInteger('Config_','PRing1',0);
  CommonSet.PRing2:=f.ReadInteger('Config_','PRing2',0);
  CommonSet.PRing3:=f.ReadInteger('Config_','PRing3',0);
  CommonSet.PRing4:=f.ReadInteger('Config_','PRing4',0);
  CommonSet.PRing5:=f.ReadInteger('Config_','PRing5',0);
  CommonSet.PRingW:=f.ReadInteger('Config_','PRingW',0);

  CommonSet.QGroup1:=f.ReadString('Config_','QGroup1','0');
  CommonSet.QGroup2:=f.ReadString('Config_','QGroup2','0');
  CommonSet.QGroup3:=f.ReadString('Config_','QGroup3','0');
  CommonSet.QGroup4:=f.ReadString('Config_','QGroup4','0');
  CommonSet.QGroup5:=f.ReadString('Config_','QGroup5','0');

  CommonSet.PrePrintPort:=f.ReadString('Config_','PrePrintPort','fis');
  CommonSet.sUnit :=f.ReadString('Config_','UNIT','01');
  CommonSet.ExportFile:=f.ReadString('Config_','ExportFile','');

  CommonSet.WriteLog:=f.ReadInteger('Config_','WriteLog',1);
  CommonSet.Station:=f.ReadInteger('Config_','Station',1);

  CommonSet.BNManual:=f.ReadINteger('Config_','BNManual',1);  //��
  CommonSet.BNPath:=f.ReadString('Config_','BNPath','C:\_CasherRn\BIN\BN');
  CommonSet.BNPointNum:=f.ReadString('Config_','BNPointNum','00001');
  CommonSet.BNDelay:=f.ReadINteger('Config_','BNDelay',30);  //��
  CommonSet.MReader:=f.ReadINteger('Config_','MReader',0);  //���
  CommonSet.Prefix:=f.ReadString('Config_','Prefix','26');
  CommonSet.PrefixVes:=f.ReadString('Config_','PrefixVes','22');
  CommonSet.BNButton:=f.ReadINteger('Config_','BNButton',0);  //�������

  CommonSet.iDelay:=f.ReadInteger('Config_','iDelaySec',3)*1000;
  CommonSet.iStartDelaySec:=f.ReadInteger('Config_','iStartDelaySec',10)*1000;
  CommonSet.CheckDelaySec:=f.ReadInteger('Config_','CheckDelaySec',1)*1000;

  CommonSet.PreCheckCount :=f.ReadINteger('Config_','PreCheckCount',1);  //���������� ������ ���
  CommonSet.PreCheckNum :=f.ReadINteger('Config_','PreCheckNum',1);  //
  CommonSet.ZTimeShift:=f.ReadString('Config_','ZTimeShift','06:00'); //����� �� ������� ��� �������� ����������
  CommonSet.IncNumTab:=f.ReadINteger('Config_','IncNumTab',1);  //
  CommonSet.iQuestVisible:=f.ReadINteger('Config_','iQuestVisible',1);  // 1-������ ������ 2-������ ����� 3-� �� � ��
  CommonSet.HourShift:=f.ReadINteger('Config_','HourShift',48);  // ���� ����� ������ 48 ����� �� �� �� �����
  CommonSet.SpecChar:=f.ReadINteger('Config_','SpecChar',0);  // 0 - ������� �����, 1 - �������� �����������
  CommonSet.MustPrePrint:=f.ReadINteger('Config_','MustPrePrint',1);  // 0 - ������� �����, 1 - ������������ ������ �����
  CommonSet.SpecBox:=f.ReadString('Config_','SpecBox','fisprim');  // ���� �� ���� �� ����������
  CommonSet.SpecBoxX:=f.ReadString('Config_','SpecBoxX','COM3');  // ���� �-������ ���� �� ����������
  CommonSet.UseDP2300:=f.ReadINteger('Config_','UseDP2300',0);  // 0 - ������� �����, 1 - �������� �����������
  CommonSet.DiscAuto:=f.ReadINteger('Config_','DiscAuto',0);  // 0 - �������������� ������
  CommonSet.LastLine:=f.ReadString('Config_','LastLine','');  // ��������� ������ � �����
  CommonSet.PreLine:=f.ReadString('Config_','PreLine','');  // ������ ������ � �����
  CommonSet.LastLine1:=f.ReadString('Config_','LastLine1','');  // ��������� ������ � ����
  CommonSet.PreLine1:=f.ReadString('Config_','PreLine1','');  // ������ ������ � ����
  CommonSet.iPrePrintSt :=f.ReadINteger('Config_','PrePrintSt',0);  // 0 - ������� ���� �� ������ ��� ������ �����
  CommonSet.InventryToTO :=f.ReadINteger('Config_','InventryToTO',1);  // 1 - ������ ������ �������������� - ����� ��-����� ��������������
  CommonSet.CalcRemns:=f.ReadINteger('Config_','CalcRemns',1);  // 1 - ������� ������� � �������� 0 -���

  CommonSet.KuponButton :=f.ReadINteger('Config_','KuponButton',0);  // 0 - ������ ������ �� �����  1- �����
  CommonSet.KuponCode :=f.ReadINteger('Config_','KuponCode',0);  // ��� ������ ������
  CommonSet.KuponName:=f.ReadString('Config_','KuponName','');  // �������� ������ ������

  CommonSet.RaschotWarn :=f.ReadINteger('Config_','RaschotWarn',0);  // ���. ��������������� ��� �������
  CommonSet.SelPCard :=f.ReadINteger('Config_','SelPCard',1);  // ������������ ������ �� �� 1- ������������ 0-���
  CommonSet.PriceFromCash :=f.ReadINteger('Config_','PriceFromCash',1);  // ��������� ���� ��������� � ���� 1- �� 0-���
  CommonSet.iRefreshTime:=f.ReadINteger('Config_','RefreshTime',60);  // ����������

  CommonSet.AutoZak :=f.ReadINteger('Config_','AutoZak',0);  // 0 - ����������� ����. 1- ������� ����������
  CommonSet.Of_DocRefresh :=f.ReadINteger('Config_','Of_DocRefresh',5); // 5 ��� - ����������� ������� �������

  UserColor.Color1:=f.ReadINteger('Colors_','Color1',$00FFD3A8);
  UserColor.Color2:=f.ReadINteger('Colors_','Color2',$00FFD3A8);
  UserColor.Color3:=f.ReadINteger('Colors_','Color3',$00FFD3A8);
  UserColor.Main:=f.ReadINteger('Colors_','Main',$00FFD3A8);
  UserColor.TTnIn:=f.ReadINteger('Colors_','TTnIn',$00FFD3A8);
  UserColor.TTnOut:=f.ReadINteger('Colors_','TTnOut',$00FFD3A8);
  UserColor.TTnOutB:=f.ReadINteger('Colors_','TTnOutB',$00FFD3A8);
  UserColor.TTnVn:=f.ReadINteger('Colors_','TTnVn',$00FFD3A8);
  UserColor.TTnOutR:=f.ReadINteger('Colors_','TTnOutR',$00FFD3A8);
  UserColor.TTnInv:=f.ReadINteger('Colors_','TTnInv',$00FFD3A8);
  UserColor.TTnCompl:=f.ReadINteger('Colors_','TTnCompl',$00FFD3A8);
  UserColor.TTnAP:=f.ReadINteger('Colors_','TTnAP',$00FFD3A8);

  UserPars.CheckGram:=f.ReadINteger('Params_','CheckGram',1);

  CommonSet.ControlNums:=f.ReadINteger('Config_','ControlNums',0);
  CommonSet.TestOpenTBeforeClose:=f.ReadINteger('Config_','TestOpenTBeforeClose',0);

 //����������� �� ������������� ����� ����
//  CommonSet.HostIp:=f.ReadString('Config','HostIp','80.75.80.206');
//  CommonSet.HostUser:=f.ReadString('Config','HostUser','admin');
//  CommonSet.HostPassw:=f.ReadString('Config','HostPassw','314159314159');
//  CommonSet.FtpExport:=f.ReadString('Config_','FtpExport',CurDir+'temp\Office\;temp\office1\');

  CommonSet.ViewTab:=f.ReadINteger('Config_','ViewTab',0);  // �������� �������
  CommonSet.FisTypeBn:=f.ReadINteger('Config_','FisTypeBn',1);  // ��� �� ������� �� ���������

  CommonSet.RecalcNDS:=f.ReadINteger('Config_','RecalcNDS',0);  // ������������� ��� � ����������
//  CommonSet.NoFis:=f.ReadINteger('Config_','NoFis',0);  // 1-������������


  s:=f.ReadString('Config_','bTouch','No');
  if (pos('y',s)>0)or(pos('Y',s)>0) then CommonSet.bTouch:=True else CommonSet.bTouch:=False;

  if CommonSet.PathExport[Length(CommonSet.PathExport)]<>'\' then CommonSet.PathExport:=CommonSet.PathExport+'\';
  if CommonSet.PathImport[Length(CommonSet.PathImport)]<>'\' then CommonSet.PathImport:=CommonSet.PathImport+'\';
  if CommonSet.PathArh[Length(CommonSet.PathArh)]<>'\' then CommonSet.PathArh:=CommonSet.PathArh+'\';
  if CommonSet.PathHistory[Length(CommonSet.PathHistory)]<>'\' then CommonSet.PathHistory:=CommonSet.PathHistory+'\';
  if CommonSet.PathRkData[Length(CommonSet.PathRkData)]<>'\' then CommonSet.PathRkData:=CommonSet.PathRkData+'\';

  f.WriteString('Config_','DBName',DBName);
  f.WriteString('Config_','OfficeDBName',CommonSet.OfficeDb);
  f.WriteInteger('Config_','PersinId',Person.Id);
  f.WriteInteger('Config_','AutoTake',CommonSet.AutoTake);
  f.WriteInteger('Config_','CashNum',CommonSet.CashNum);
  f.WriteInteger('Config_','CashChNum',CommonSet.CashChNum);
  f.WriteInteger('Config_','CashZ',CommonSet.CashZ);
  f.WriteInteger('Config_','CashPort',CommonSet.CashPort);
  f.WriteString('Config_','CashSer',CommonSet.CashSer);
  f.WriteString('Config_','PathExport',CommonSet.PathExport);
  f.WriteString('Config_','PathImport',CommonSet.PathImport);
//  f.WriteString('Config_','FtpExport',CommonSet.FtpExport);
  f.WriteString('Config_','FtpImport',CommonSet.FtpImport);
  f.WriteString('Config_','PathHistory',CommonSet.PathHistory);
  f.WriteString('Config_','PathArh',CommonSet.PathArh);
  f.WriteString('Config_','DepartId',CommonSet.DepartId);
  f.WriteString('Config_','DepartName',CommonSet.DepartName);
  f.WriteInteger('Config_','PeriodPing',CommonSet.PeriodPing);
  f.WriteInteger('Config_','ComDelay',CommonSet.ComDelay);
  f.WriteString('Config_','PathRkData',CommonSet.PathRkData);

  f.WriteString('Config_','PGroup1',CommonSet.PGroup1);
  f.WriteString('Config_','PGroup2',CommonSet.PGroup2);
  f.WriteString('Config_','PGroup3',CommonSet.PGroup3);
  f.WriteString('Config_','PGroup4',CommonSet.PGroup4);
  f.WriteString('Config_','PGroup5',CommonSet.PGroup5);
  f.WriteString('Config_','PGroupW',CommonSet.PGroupW);

  f.WriteString('Config_','QGroup1',CommonSet.QGroup1);
  f.WriteString('Config_','QGroup2',CommonSet.QGroup2);
  f.WriteString('Config_','QGroup3',CommonSet.QGroup3);
  f.WriteString('Config_','QGroup4',CommonSet.QGroup4);
  f.WriteString('Config_','QGroup5',CommonSet.QGroup5);

  f.WriteString('Config_','PGroup1N',CommonSet.PGroup1N);
  f.WriteString('Config_','PGroup2N',CommonSet.PGroup2N);
  f.WriteString('Config_','PGroup3N',CommonSet.PGroup3N);
  f.WriteString('Config_','PGroup4N',CommonSet.PGroup4N);
  f.WriteString('Config_','PGroup5N',CommonSet.PGroup5N);
  f.WriteString('Config_','PGroupWN',CommonSet.PGroupWN);

  f.WriteInteger('Config_','PRing1',CommonSet.PRing1);
  f.WriteInteger('Config_','PRing2',CommonSet.PRing2);
  f.WriteInteger('Config_','PRing3',CommonSet.PRing3);
  f.WriteInteger('Config_','PRing4',CommonSet.PRing4);
  f.WriteInteger('Config_','PRing5',CommonSet.PRing5);
  f.WriteInteger('Config_','PRingW',CommonSet.PRingW);


  f.WriteString('Config_','PrePrintPort',CommonSet.PrePrintPort);
  f.WriteString('Config_','UNIT',CommonSet.sUnit);
  f.WriteString('Config_','ExportFile',CommonSet.ExportFile);
  f.WriteInteger('Config_','WriteLog',CommonSet.WriteLog);
  f.WriteInteger('Config_','Station',CommonSet.Station);
  f.WriteINteger('Config_','BNManual',CommonSet.BNManual);
  f.WriteString('Config_','BNPath',CommonSet.BNPath);
  f.WriteString('Config_','BNPointNum',CommonSet.BNPointNum);
  f.WriteINteger('Config_','BNDelay',CommonSet.BNDelay);
  f.WriteINteger('Config_','PreCheckCount',CommonSet.PreCheckCount);
  f.WriteINteger('Config_','MReader',CommonSet.MReader);
  f.WriteString('Config_','Prefix',CommonSet.Prefix);
  f.WriteString('Config_','PrefixVes',CommonSet.PrefixVes);
  f.WriteINteger('Config_','PreCheckNum',CommonSet.PreCheckNum);  //
  f.WriteINteger('Config_','BNButton',CommonSet.BNButton);  //
  f.WriteINteger('Config_','iDelaySec',Trunc(CommonSet.iDelay/1000));  // ����� ��������� ������
  f.WriteINteger('Config_','iStartDelaySec',Trunc(CommonSet.iStartDelaySec/1000));  // �������� ������ ������
  f.WriteInteger('Config_','CheckDelaySec',Trunc(CommonSet.CheckDelaySec/1000)); //����� ��������� ������

  f.WriteString('Config_','ZTimeShift',CommonSet.ZTimeShift); //����� �� ������� ��� �������� ����������
  f.WriteINteger('Config_','IncNumTab',CommonSet.IncNumTab);  //
  f.WriteINteger('Config_','iQuestVisible',CommonSet.iQuestVisible);  // 1-������ ������ 2-������ ����� 3-� �� � ��
  f.WriteINteger('Config_','HourShift',CommonSet.HourShift);  // ���� ����� �������� ������ 48 ����� �� �� �� �����
  f.WriteINteger('Config_','SpecChar',CommonSet.SpecChar);  // 0 - ������� �����, 1 - �������� �����������
  f.WriteINteger('Config_','MustPrePrint',CommonSet.MustPrePrint);  // 0 - ������� �����, 1 - ������������ ������ �����
  f.WriteString('Config_','SpecBox',CommonSet.SpecBox);  // ���� �� ���� �� ����������
  f.WriteString('Config_','SpecBoxX',CommonSet.SpecBoxX);  // ���� �-������ ���� �� ����������
  f.WriteINteger('Config_','UseDP2300',CommonSet.UseDP2300);  // ������������ ��
  f.WriteINteger('Config_','DiscAuto',CommonSet.DiscAuto);  // 0 - �������������� ������
  f.WriteString('Config_','LastLine',CommonSet.LastLine);  // ��������� ������ � �����
  f.WriteString('Config_','PreLine',CommonSet.PreLine);  // ��������� ������ � �����
  f.WriteString('Config_','LastLine1',CommonSet.LastLine1);  // ��������� ������ � �����
  f.WriteString('Config_','PreLine1',CommonSet.PreLine1);  // ��������� ������ � �����
  f.WriteINteger('Config_','PrePrintSt',CommonSet.iPrePrintSt);  // 0 - ������� ���� �� ������ ��� ������ �����
  f.WriteINteger('Config_','InventryToTO',CommonSet.InventryToTO);  // 1 - ������ ������ �������������� - ����� ��-����� ��������������
  f.WriteINteger('Config_','CalcRemns',CommonSet.CalcRemns);  // 1 - ������� ������� � �������� 0 -���
  f.WriteINteger('Config_','ViewTab',CommonSet.ViewTab);  // �������� �������
  f.WriteINteger('Config_','FisTypeBn',CommonSet.FisTypeBn);  // ��� �� ������� �� ���������

  f.WriteINteger('Config_','KuponButton',CommonSet.KuponButton);  // 0 - ������ ������ �� �����  1- �����
  f.WriteINteger('Config_','KuponCode',CommonSet.KuponCode);  // ��� ������ ������
  f.WriteString('Config_','KuponName',CommonSet.KuponName);  // �������� ������ ������
  f.WriteINteger('Config_','RaschotWarn',CommonSet.RaschotWarn);  // ���. ��������������� ��� �������
  f.WriteINteger('Config_','SelPCard',CommonSet.SelPCard);  // ������������ ������ �� �� 1- ������������ 0-���
  f.WriteINteger('Config_','PriceFromCash',CommonSet.PriceFromCash);  // ��������� ���� ��������� � ���� 1- �� 0-���
  f.WriteINteger('Config_','RefreshTime',CommonSet.iRefreshTime);  // ���������� ����
  f.WriteINteger('Config_','ControlNums',CommonSet.ControlNums);
  f.WriteINteger('Config_','TestOpenTBeforeClose',CommonSet.TestOpenTBeforeClose); //����������� �������� ������ ����� ��������� ���
  f.WriteINteger('Config_','RecalcNDS',CommonSet.RecalcNDS);  // ������������� ��� � ����������

  f.WriteINteger('Config_','AutoZak',CommonSet.AutoZak);  // 0 - ����������� ����. 1- ������� ����������
  f.WriteINteger('Config_','Of_DocRefresh',CommonSet.Of_DocRefresh);  // 5 ��� - ����������� ������� �������

//  f.WriteINteger('Config_','NoFis',CommonSet.NoFis);  // 1-������������

//  f.WriteString('Config','HostIp',CommonSet.HostIp);
//  f.WriteString('Config','HostUser',CommonSet.HostUser);
//  f.WriteString('Config','HostPassw',CommonSet.HostPassw);

  if CommonSet.bTouch then
  begin
    f.WriteString('Config_','bTouch','Yes');
  end
  else
    f.WriteString('Config_','bTouch','No');

  f.Free;

  //� ��� ������ ������� � ����������� ������
  if FileExists(CurDir+LicIni+'.lic') then //�����������
  begin
    if FileExists(CurDir+LicIni+'.ini') then //�������������
    begin
      Assignfile(ft,CurDir+LicIni+'.ini');
      erase(ft);
    end;
      //�������������
    {$I-}
    Assignfile(ft,CurDir+LicIni+'.lic');
    FileMode := 2;  {Set file access to read/Write }
    Reset(Ft);
    CloseFile(Ft);
    {$I+}
    if IOResult = 0 then
    begin
      try
        fs := TECLFileStream.Create(CurDir+LicIni+'.lic',fmOpenReadWrite,odInac1,levelValue[9]);
        fs.SaveToFile(CurDir+LicIni+'.ini');

        if FileExists(CurDir+LicIni+'.ini') then //�������������
        begin
          f:=TIniFile.create(CurDir+LicIni+'.ini');

          SpecVal.SPH1:=f.ReadString('Config_','SPH1',' ');
          SpecVal.SPH2:=f.ReadString('Config_','SPH2',' ');
          SpecVal.SPH4:=f.ReadString('Config_','SPH4',' ');
          SpecVal.SPH3:=f.ReadString('Config_','SPH3',' ');
          SpecVal.SPH5:=f.ReadString('Config_','SPH5',' ');

          SpecVal.SPE1:=f.ReadString('Config_','SPE1',' ');
          SpecVal.SPE2:=f.ReadString('Config_','SPE2',' ');
          SpecVal.SPE3:=f.ReadString('Config_','SPE3',' ');
          SpecVal.SPE4:=f.ReadString('Config_','SPE4',' ');
          SpecVal.SPE5:=f.ReadString('Config_','SPE5',' ');

          SpecVal.DepartName:=f.ReadString('Config_','DepartName','');
          SpecVal.CountStarts:=f.ReadInteger('Config_','CountStarts',-2);
          SpecVal.DateTo:=StrToDateTimeDef(f.ReadString('Config_','DateTo','01.01.2100'),StrToDateTime('01.01.2100'));

          if SpecVal.DepartName>'' then CommonSet.DepartName:=SpecVal.DepartName;
          if SpecVal.CountStarts>=0 then dec(SpecVal.CountStarts);

          DBNamePC:=f.ReadString('Config_','DBNamePC','new');
          if DBNamePC='new' then DBNamePC:=DBName;


          f.WriteString('Config_','SPH1',SpecVal.SPH1);
          f.WriteString('Config_','SPH2',SpecVal.SPH2);
          f.WriteString('Config_','SPH3',SpecVal.SPH3);
          f.WriteString('Config_','SPH4',SpecVal.SPH4);
          f.WriteString('Config_','SPH5',SpecVal.SPH5);

          f.WriteString('Config_','SPE1',SpecVal.SPE1);
          f.WriteString('Config_','SPE2',SpecVal.SPE2);
          f.WriteString('Config_','SPE3',SpecVal.SPE3);
          f.WriteString('Config_','SPE4',SpecVal.SPE4);
          f.WriteString('Config_','SPE5',SpecVal.SPE5);

          f.WriteString('Config_','DepartName',SpecVal.DepartName);
          f.WriteInteger('Config_','CountStarts',SpecVal.CountStarts);
          f.WriteString('Config_','DateTo',FormatDateTime('dd.mm.yyyy',SpecVal.DateTo));

          f.WriteString('Config_','DBNamePC',DBNamePC);

          f.Free;
        end;

        fs.LoadFromFile(CurDir+LicIni+'.ini');
        fs.Free;
      except
      end;
    end;

    if FileExists(CurDir+LicIni+'.ini') then //�������������
    begin
      Assignfile(ft,LicIni+'.ini');
      erase(ft);
    end;
  end;
end;

Procedure WriteIni;
Var f:TIniFile;
begin
  f:=TIniFile.create(CurDir+CurIni);

  f.WriteString('Config_','DBName',DBName);
  f.WriteString('Config_','OfficeDBName',CommonSet.OfficeDb);
  f.WriteInteger('Config_','PersinId',Person.Id);
  f.WriteInteger('Config_','AutoTake',CommonSet.AutoTake);
//  f.WriteInteger('Config_','CashNum',CommonSet.CashNum); //�������� �� ������ ������������ ������� �������
  f.WriteInteger('Config_','CashChNum',CommonSet.CashChNum);
  f.WriteInteger('Config_','CashZ',CommonSet.CashZ);
  f.WriteInteger('Config_','CashPort',CommonSet.CashPort);
  f.WriteString('Config_','CashSer',CommonSet.CashSer);
  f.WriteString('Config_','DepartId',CommonSet.DepartId);
  f.WriteString('Config_','DepartName',CommonSet.DepartName);
  f.WriteInteger('Config_','PeriodPing',CommonSet.PeriodPing);
  f.WriteString('Config_','ExportFile',CommonSet.ExportFile);
  f.WriteInteger('Config_','WriteLog',CommonSet.WriteLog);
  f.WriteInteger('Config_','Station',CommonSet.Station);
  f.WriteINteger('Config_','PreCheckNum',CommonSet.PreCheckNum);  //
  f.WriteINteger('Config_','RaschotWarn',CommonSet.RaschotWarn);  // ���. ��������������� ��� �������

  if CommonSet.bTouch then
  begin
    f.WriteString('Config_','bTouch','Yes');
  end
  else
    f.WriteString('Config_','bTouch','No');

  f.Free;
end;



Procedure WriteCheckNum;
Var f:TIniFile;
begin
  f:=TIniFile.create(CurDir+CurIni);
  f.WriteINteger('Config_','PreCheckNum',CommonSet.PreCheckNum);  //
  f.WriteInteger('Config_','CashChNum',CommonSet.CashChNum);
  f.WriteInteger('Config_','CashZ',CommonSet.CashZ);
  f.Free;
end;

Procedure WriteTabNum;
Var f:TIniFile;
begin
  f:=TIniFile.create(CurDir+CurIni);
  f.WriteINteger('Config_','IncNumTab',CommonSet.IncNumTab);  //
  f.Free;
end;



Procedure ReadStartIni;
Var f:TIniFile;
begin
  f:=TIniFile.create(CurDir+StartIni);

  StartId.IdBeg:=f.ReadInteger('Config_','IdBeg',1);
  StartId.IdBegT:=f.ReadInteger('Config_','IdBegT',1);
  f.WriteInteger('Config_','IdBeg',StartId.IdBeg);
  f.WriteInteger('Config_','IdBegT',StartId.IdBegT);
  f.Free;
end;

Procedure WriteColor;                
Var f:TIniFile;
begin
  f:=TIniFile.create(CurDir+CurIni);
  f.WriteInteger('Colors_','Color1',UserColor.Color1);
  f.WriteInteger('Colors_','Color2',UserColor.Color2);
  f.WriteInteger('Colors_','Color3',UserColor.Color3);
  f.WriteInteger('Colors_','Main',UserColor.Main);
  f.WriteInteger('Colors_','TTnIn',UserColor.TTnIn);
  f.WriteInteger('Colors_','TTnOut',UserColor.TTnOut);
  f.WriteInteger('Colors_','TTnOutB',UserColor.TTnOutB);
  f.WriteInteger('Colors_','TTnVn',UserColor.TTnVn);
  f.WriteInteger('Colors_','TTnOutR',UserColor.TTnOutR);
  f.WriteInteger('Colors_','TTnInv',UserColor.TTnCompl);
  f.WriteInteger('Colors_','TTnCompl',UserColor.Main);
  f.WriteInteger('Colors_','TTnAP',UserColor.TTnAP);

  f.Free;
end;

Procedure WriteParams;
Var f:TIniFile;
begin
  f:=TIniFile.create(CurDir+CurIni);
  f.WriteInteger('Params_','CheckGram',UserPars.CheckGram);
  f.Free;
end;


Procedure WriteStartIni;
Var f:TIniFile;
begin
  f:=TIniFile.create(CurDir+StartIni);
  f.WriteInteger('Config_','IdBeg',StartId.IdBeg);
  f.WriteInteger('Config_','IdBegT',StartId.IdBegT);
  f.Free;
end;

Function TrimStr(StrIn:String):String;
begin
  delete(StrIn,Pos('//',StrIn),(Length(StrIn)-Pos('//',StrIn)+1)); //������ ����������
  while Pos(' ',StrIn)>0 do delete(StrIn,Pos(' ',StrIn),1);        //������ �������
  Result:=StrIn;
end;

Procedure RefreshTree(PersonalId:Integer; Tree:TTreeView; quTree:TpFIBDataSet);
//Var I:Integer;
begin
//  for I:=1 to tree.Items.Count do tree.Items[i].Delete;
  while tree.Items.Count>0 do tree.Items[0].Delete;
  RExpandLevel( Nil,Tree,quTree,PersonalId);
//  tree.Items[0].Expand(True);
  delay(10);
end;

Procedure RExpandLevel( Node : TTreeNode; Tree:TTreeView; quTree:TpFIBDataSet; PersonalId:Integer );
Var ID , i   : Integer;
    TreeNode : TTreeNode;
Begin
// ��� ������ �������� ������ ������� ������ ���,
// ��� �� ����� ���������.
  if Node = nil then ID:=0
  else ID:=Integer(Node.Data);
  quTree.Close;
  quTree.ParamByName('ParentID').Value:=ID;
  quTree.ParamByName('PersonalID').Value:=PersonalId;
  quTree.Open;
  Tree.Items.BeginUpdate;
  for i:=1 to quTree.RecordCount do
  begin    // ������� � ���� Data ����� �� ����������������� �����(ID) � �������
    TreeNode:=Tree.Items.AddChildObject(Node, quTree.FieldByName('Name').AsString, Pointer(quTree.FieldByName('ID_Classif').AsInteger));
    if quTree.FieldByName('Rights').AsInteger=1 then //��� �������
    begin
      TreeNode.ImageIndex:=0;
      TreeNode.SelectedIndex:=2;
    end;
    if quTree.FieldByName('Rights').AsInteger=0 then  //���� ������
    begin
      TreeNode.ImageIndex:=1;
      TreeNode.SelectedIndex:=3;
    end;
    // ������� ��������� (������) �������� ����� ������ ��� ����,
    // ����� ��� ��������� [+] �� ����� � �� ����� ���� �� ��������
    Tree.Items.AddChildObject(TreeNode,'', nil);
    quTree.Next;
  end;
  Tree.Items.EndUpdate;
end;



Procedure ExpandLevel( Node : TTreeNode; Tree:TTreeView; quTree:TpFIBDataSet);
Var ID , i   : Integer;
    TreeNode : TTreeNode;
Begin
// ��� ������ �������� ������ ������� ������ ���,
// ��� �� ����� ���������.
  if Node = nil then ID:=0
  else ID:=Integer(Node.Data);
  quTree.Close;
  quTree.ParamByName('ParentID').Value:=ID;
  quTree.Open;
  Tree.Items.BeginUpdate;
  // ��� ������ ������ �� ����������� ������ ������
  // ��������� ����� � TreeView, ��� �������� ����� � ���,
  // ������� �� ������ ��� "��������"
  for i:=1 to quTree.RecordCount do
  begin    // ������� � ���� Data ����� �� ����������������� �����(ID) � �������
    TreeNode:=Tree.Items.AddChildObject(Node, quTree.FieldByName('Name').AsString, Pointer(quTree.FieldByName('ID').AsInteger));
    if quTree.FieldByName('ID_PARENT').AsInteger=0 then
    begin
      TreeNode.ImageIndex:=0;
      TreeNode.SelectedIndex:=2;
    end;
    if quTree.FieldByName('ID_PARENT').AsInteger>0 then
    begin
      TreeNode.ImageIndex:=1;
      TreeNode.SelectedIndex:=3;
    end;
    // ������� ��������� (������) �������� ����� ������ ��� ����,
    // ����� ��� ��������� [+] �� ����� � �� ����� ���� �� ��������
    Tree.Items.AddChildObject(TreeNode,'', nil);
    quTree.Next;
  end;
  Tree.Items.EndUpdate;
end;



Procedure CheckNodeOn(Node:TTreeNode;bOn:Boolean);
Var ChildNode,CurNode:TTreeNode;
begin
  ChildNode:=Node.getFirstChild;
  while ChildNode<>Nil do
  begin
    if bOn then ChildNode.ImageIndex:=2
    else ChildNode.ImageIndex:=0;
    CurNode:=ChildNode;
    CheckNodeOn(CurNode,bOn);
    ChildNode:=CurNode.getNextChild(CurNode);
  end;
end;


procedure Delay(MSecs: Longint);
var
  FirstTickCount, Now: Longint;
begin
  FirstTickCount := GetTickCount;
  repeat
    Application.ProcessMessages;
    { allowing access to other controls, etc. }
    Now := GetTickCount;
  until (Now - FirstTickCount >= MSecs) or (Now < FirstTickCount);
end;


procedure prCalcSumNac(i1,i2,i3:Integer; V:TcxGridDBTableView);
Var rSumIn,rSumOut,rNac:Real;
    StrWk:String;
    vSum:Variant;
begin
  rSumIn:=0; rSumOut:=0;
  vSum:=V.DataController.Summary.FooterSummaryValues[i1];
  if vSum<>Null then rSumIn:=vSum;

  vSum:=V.DataController.Summary.FooterSummaryValues[i2];
  if vSum<>Null then rSumOut:=vSum;

  rNac:=0;
  if rSumIn<>0 then
    rNac:=(rSumOut-rSumIn)/rSumIn*100;
  str(rNac:2:1,StrWk);
  StrWk:=StrWk+'%';
  V.DataController.Summary.FooterSummaryValues[i3]:=StrWk;
end;


procedure prCalcDefGroup(i1,i2,i3,iParent:Integer; V:TcxGridDBTableView);
var
  AChildDataGroupsCount: Integer;
  AChildDataGroupIndex: TcxDataGroupIndex;
  AChildPosition: Integer;
  AVarSum, AVarSum1, AVarNac: Variant;
  RowInf:TcxRowInfo;

begin
  with V.DataController.Groups do
  begin
    AChildDataGroupsCount := ChildCount[iParent];

    for AChildPosition := 0 to AChildDataGroupsCount - 1 do
    begin
      //data group index of a child
      AChildDataGroupIndex := ChildDataGroupIndex[iParent, AChildPosition];

      if AChildDataGroupIndex>=0 then
      begin
        prCalcDefGroup(i1,i2,i3,AChildDataGroupIndex,V);

        with V.DataController.Summary do
        begin
          AVarSum := GroupSummaryValues[AChildDataGroupIndex, i1];
          AVarSum1 := GroupSummaryValues[AChildDataGroupIndex, i2];
          if ((not (VarIsNull(AVarSum) or VarIsNull(AVarSum1))) and (AVarSum<>0)) then
          begin
            AVarNac := (AVarSum1 - AVarSum) / AVarSum*100;
            GroupSummaryValues[AChildDataGroupIndex, i3] := Format('%4.1f', [Double(AVarNac)])+'%';
//          GroupSummaryValues[AChildDataGroupIndex, 2] := Double(AVarNac);
//          Memo1.Lines.Add('In '+FloatToStr(Double(AVarSum))+' Out '+FloatToStr(Double(AVarSum1))+' Nac '+FloatToStr(Double(AVarNac))+' AChildDataGroupIndex '+INtToStr(AChildDataGroupIndex));
            delay(10);
          end;
        end;
      end;
    end;
  end;
end;

{
function IsGroupingRow(ARowInfo: TcxRowInfo; ADataController: TcxCustomDataController): Boolean;
begin
  with ADataController do
    Result := ARowInfo.Level <> Groups.GroupingItemCount;
end;

}

end.
