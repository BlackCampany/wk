unit ViewTab;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, Menus, cxLookAndFeelPainters, StdCtrls,
  cxButtons;

type
  TfmViewTab = class(TForm)
    ViewVT: TcxGridDBTableView;
    LevelVT: TcxGridLevel;
    GridVT: TcxGrid;
    ViewVTID: TcxGridDBColumn;
    ViewVTSIFR: TcxGridDBColumn;
    ViewVTNAME: TcxGridDBColumn;
    ViewVTPRICE: TcxGridDBColumn;
    ViewVTQUANTITY: TcxGridDBColumn;
    ViewVTDISCOUNTPROC: TcxGridDBColumn;
    ViewVTSUMMA: TcxGridDBColumn;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmViewTab: TfmViewTab;

implementation

uses Dm, Attention, MainCashRn, Un1, Spec;

{$R *.dfm}

procedure TfmViewTab.cxButton3Click(Sender: TObject);
begin
  dmC.quSpecSel.Active:=False;
  Close;
end;

procedure TfmViewTab.cxButton1Click(Sender: TObject);
//��������
Var sMessage:String;
begin
  fmViewTab.Visible:=False;
  delay(20);

  if bPrintCheck then
  begin
    sMessage:='���������� ��������� ��������� ����.';
    fmAttention.Label1.Caption:=sMessage;
    prWriteLog('~~AttentionShow;'+sMessage);
    fmAttention.ShowModal;
    exit;
  end;
  fmSpec.Tag:=1;
  fmMainCashRn.acOpen.Execute;
end;

procedure TfmViewTab.FormCreate(Sender: TObject);
begin
  Left:=336;
  Top:=510;
end;

procedure TfmViewTab.cxButton2Click(Sender: TObject);
begin
  fmViewTab.Visible:=False;
  delay(20);
  fmMainCashRn.cxButton11.Click;
end;

end.
