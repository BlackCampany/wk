unit RecalcPer;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, cxMemo, StdCtrls, cxButtons,
  cxControls, cxContainer, cxEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit,
  cxCalendar, ComCtrls, FIBQuery, pFIBQuery, pFIBStoredProc, FIBDatabase,
  pFIBDatabase, DB, FIBDataSet, pFIBDataSet, cxCheckBox, cxGraphics,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox;

type
  TfmRecalcPer = class(TForm)
    StatusBar1: TStatusBar;
    Label1: TLabel;
    cxDateEdit1: TcxDateEdit;
    cxButton1: TcxButton;
    Memo1: TcxMemo;
    cxButton2: TcxButton;
    prDelPer: TpFIBStoredProc;
    trS: TpFIBTransaction;
    trD: TpFIBTransaction;
    trU: TpFIBTransaction;
    prTestPart: TpFIBStoredProc;
    quDIn: TpFIBDataSet;
    quDInID: TFIBIntegerField;
    quDInIDCLI: TFIBIntegerField;
    quDInIDSKL: TFIBIntegerField;
    quDInSUMIN: TFIBFloatField;
    quDInSUMUCH: TFIBFloatField;
    quDInSUMTAR: TFIBFloatField;
    quDInSUMNDS0: TFIBFloatField;
    quDInSUMNDS1: TFIBFloatField;
    quDInSUMNDS2: TFIBFloatField;
    quDInPROCNAC: TFIBFloatField;
    quDCompl: TpFIBDataSet;
    quDAct: TpFIBDataSet;
    quDComplID: TFIBIntegerField;
    quDComplDATEDOC: TFIBDateField;
    quDComplNUMDOC: TFIBStringField;
    quDComplIDSKL: TFIBIntegerField;
    quDComplSUMIN: TFIBFloatField;
    quDComplSUMUCH: TFIBFloatField;
    quDComplSUMTAR: TFIBFloatField;
    quDComplPROCNAC: TFIBFloatField;
    quDComplIACTIVE: TFIBIntegerField;
    quDComplOPER: TFIBStringField;
    quDActID: TFIBIntegerField;
    quDActDATEDOC: TFIBDateField;
    quDActNUMDOC: TFIBStringField;
    quDActIDSKL: TFIBIntegerField;
    quDActSUMIN: TFIBFloatField;
    quDActSUMUCH: TFIBFloatField;
    quDActIACTIVE: TFIBIntegerField;
    quDActOPER: TFIBStringField;
    quDActCOMMENT: TFIBStringField;
    quDVn: TpFIBDataSet;
    quDVnID: TFIBIntegerField;
    quDVnDATEDOC: TFIBDateField;
    quDVnNUMDOC: TFIBStringField;
    quDVnIDSKL_FROM: TFIBIntegerField;
    quDVnIDSKL_TO: TFIBIntegerField;
    quDVnSUMIN: TFIBFloatField;
    quDVnSUMUCH: TFIBFloatField;
    quDVnSUMUCH1: TFIBFloatField;
    quDVnSUMTAR: TFIBFloatField;
    quDVnPROCNAC: TFIBFloatField;
    quDVnIACTIVE: TFIBIntegerField;
    quDRet: TpFIBDataSet;
    quDRetID: TFIBIntegerField;
    quDRetDATEDOC: TFIBDateField;
    quDRetNUMDOC: TFIBStringField;
    quDRetDATESF: TFIBDateField;
    quDRetNUMSF: TFIBStringField;
    quDRetIDCLI: TFIBIntegerField;
    quDRetIDSKL: TFIBIntegerField;
    quDRetSUMIN: TFIBFloatField;
    quDRetSUMUCH: TFIBFloatField;
    quDRetSUMTAR: TFIBFloatField;
    quDRetSUMNDS0: TFIBFloatField;
    quDRetSUMNDS1: TFIBFloatField;
    quDRetSUMNDS2: TFIBFloatField;
    quDRetPROCNAC: TFIBFloatField;
    quDRetIACTIVE: TFIBIntegerField;
    quDOutB: TpFIBDataSet;
    quDOutBID: TFIBIntegerField;
    quDOutBDATEDOC: TFIBDateField;
    quDOutBNUMDOC: TFIBStringField;
    quDOutBDATESF: TFIBDateField;
    quDOutBNUMSF: TFIBStringField;
    quDOutBIDCLI: TFIBIntegerField;
    quDOutBIDSKL: TFIBIntegerField;
    quDOutBSUMIN: TFIBFloatField;
    quDOutBSUMUCH: TFIBFloatField;
    quDOutBSUMTAR: TFIBFloatField;
    quDOutBSUMNDS0: TFIBFloatField;
    quDOutBSUMNDS1: TFIBFloatField;
    quDOutBSUMNDS2: TFIBFloatField;
    quDOutBPROCNAC: TFIBFloatField;
    quDOutBIACTIVE: TFIBIntegerField;
    quDOutBOPER: TFIBStringField;
    quDInv: TpFIBDataSet;
    quDInvID: TFIBIntegerField;
    quDInvDATEDOC: TFIBDateField;
    quDInvNUMDOC: TFIBStringField;
    quDInvIDSKL: TFIBIntegerField;
    quDInvIACTIVE: TFIBIntegerField;
    quDInvITYPE: TFIBIntegerField;
    quDInvSUM1: TFIBFloatField;
    quDInvSUM11: TFIBFloatField;
    quDInvSUM2: TFIBFloatField;
    quDInvSUM21: TFIBFloatField;
    prSetSpecActive: TpFIBStoredProc;
    quDInIACTIVE: TFIBIntegerField;
    quDR: TpFIBDataSet;
    quDRID: TFIBIntegerField;
    quDRDATEDOC: TFIBDateField;
    quDRNUMDOC: TFIBStringField;
    quDRDATESF: TFIBDateField;
    quDRNUMSF: TFIBStringField;
    quDRIDCLI: TFIBIntegerField;
    quDRIDSKL: TFIBIntegerField;
    quDRSUMIN: TFIBFloatField;
    quDRSUMUCH: TFIBFloatField;
    quDRSUMTAR: TFIBFloatField;
    quDRSUMNDS0: TFIBFloatField;
    quDRSUMNDS1: TFIBFloatField;
    quDRSUMNDS2: TFIBFloatField;
    quDRPROCNAC: TFIBFloatField;
    quDRIACTIVE: TFIBIntegerField;
    cxCheckBox1: TcxCheckBox;
    quC: TpFIBDataSet;
    quCID: TFIBIntegerField;
    PRNORMPARTIN: TpFIBStoredProc;
    quMHAll1: TpFIBDataSet;
    quMHAll1ID: TFIBIntegerField;
    quMHAll1PARENT: TFIBIntegerField;
    quMHAll1ITYPE: TFIBIntegerField;
    quMHAll1NAMEMH: TFIBStringField;
    quMHAll1DEFPRICE: TFIBIntegerField;
    quMHAll1NAMEPRICE: TFIBStringField;
    trS1: TpFIBTransaction;
    quDComplIDSKLTO: TFIBIntegerField;
    Label2: TLabel;
    cxLookupComboBox2: TcxLookupComboBox;
    cxCheckBox2: TcxCheckBox;
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure prWr(StrWk:String);
  end;

var
  fmRecalcPer: TfmRecalcPer;

implementation

uses Un1, dmOffice, DMOReps, DocCompl, AddCompl, ActPer, DocsVn, DocsOut,
  DocOutB, DOBSpec, DocInv, DocOutR;

{$R *.dfm}

procedure TfmRecalcPer.prWr(StrWk:String);
Var StrWk1:String;
begin
  StrWk1:=FormatDateTime('mm.dd hh:nn:sss',now)+' '+StrWk;
  Memo1.Lines.Add(StrWk1);
end;


procedure TfmRecalcPer.cxButton1Click(Sender: TObject);
Var iDateB,iDateE,iDateCur:Integer;
    iE:INteger;
    rSum0,rSum1,rSum2,rSum3:Real;
    rSum11,rSum22:Real;
    iC:INteger;
    rQ:Real;

    rQRemn:Real;
    iNDS:Integer;
    rProcNDS,rSumIn0,rPriceIn0,rSumNDS:Real;
    iSS:INteger;
    rSumIn,rSumT:Real;

    iSkl,iSklFrom,iSklTo:INteger;
begin
  //�������� �������
  Memo1.Clear;
  iDateB:=Trunc(cxDateEdit1.Date);
  iDateE:=Trunc(Date);

  iSkl:=cxLookupComboBox2.EditValue;
  if iSkl<=0 then  begin prWr('������������ ��.'); exit; end;
  if not CanEdit(iDateB,iSkl) then begin prWr('������ ������.'); exit; end;

  if cxCheckBox2.Checked=False then   //�� ������ ��
  begin
    prWr('�������� ��������.');

    prWr('  ������ ������.');
    prDelPer.ParamByName('IDATE').AsInteger:=iDateB;
    prDelPer.ParamByName('ISKL').AsInteger:=iSkl;
    prDelPer.ExecProc;
    prWr('  --- ������� ��.'); delay(10);

    prWr('  ���� ����������� �����.');
    prTestPart.ExecProc;
    iE:=prTestPart.ParamByName('RESULT').AsInteger;
    prWr('  --- ������� ��. ('+INtToStr(iE)+')'); delay(10);


    if iSkl<=0 then  begin prWr('�� ������ ��.'); exit; end;

    if cxCheckBox1.Checked then
    begin
      prWr('  �������� ����������� �����.');
      prWr('    ����� ... ');

      try
        quC.Active:=False;
        quC.Active:=True;

        iC:=0;
        quC.First;
        while not quC.Eof do
        begin

        //������� ��� �� ���� - �� �� ����� ����������� ���

          rQ:=prCalcRemn(quCID.AsInteger,iDateB-1,iSkl);

//      EXECUTE PROCEDURE PR_NORMPARTIN (?IDSKL, ?IDATE, ?IDCARD, ?QUANT)

          PRNORMPARTIN.ParamByName('IDSKL').AsInteger:=iSkl;
          PRNORMPARTIN.ParamByName('IDATE').AsInteger:=iDateB-1;
          PRNORMPARTIN.ParamByName('IDCARD').AsInteger:=quCID.AsInteger;
          PRNORMPARTIN.ParamByName('QUANT').AsFloat:=rQ;
          PRNORMPARTIN.ExecProc;
          delay(10);

          quC.Next; inc(iC);
          if iC mod 100 = 0 then
          begin
            prWr('    ����� ... ');
          end;
        end;
      finally
        quC.Active:=False;
      end;

      prWr('  --- ������� ��.'); delay(10);
    end;

    prWr('  ����������� ������ ����������.(2)');
    prSetSpecActive.ParamByName('DDATE').AsDateTime:=iDateB;
    prSetSpecActive.ParamByName('ATYPE').AsInteger:=2;
    prSetSpecActive.ParamByName('ISKL').AsInteger:=iSkl;
    prSetSpecActive.ExecProc;
    prWr('  --- ������� ��. '); delay(10);

    with dmO do
    with dmORep do
    begin
      for iDateCur:=iDateB to iDateE do
      begin
        prWr('  ��������� - '+FormatDateTime('dd.mm.yyyy',iDateCur)+' ('+IntToStr(iDateCur)+')');

        delay(10); //�������� 10 ��� �� ������ ������ - ����� �������� ������ ��������

        bPrintMemo:=False;

        prWr('    ��������� ���������.');
        iE:=0;
        quDIn.Active:=False;
        quDIn.ParamByName('DDATE').AsDate:=iDateCur;
        quDIn.ParamByName('ISKL').AsInteger:=iSkl;
        quDIn.Active:=True;
        quDIn.First;
        while not quDIn.Eof do
        begin
          quSpecInSel.Active:=False;
          quSpecInSel.ParamByName('IDHD').AsInteger:=quDInID.AsInteger;
          quSpecInSel.Active:=True;

          rSum1:=0;

          rSum0:=0;  rSumIn:=0;
          sNDS[1]:=0;sNDS[2]:=0;sNDS[3]:=0;
          rSum2:=0;  rSumT:=0;


          quSpecInSel.First;
          while not quSpecInSel.Eof do
          begin
            if quSpecInSelCATEGORY.AsInteger=1 then rSum1:=rSum1+quSpecInSelSUMIN.AsFloat;
            if quSpecInSelCATEGORY.AsInteger=4 then rSum2:=rSum2+quSpecInSelSUMIN.AsFloat;

            //�������� �������� �������� �� ����. ���� ������� =0 �� ������� ��� ���������� ������
            rQRemn:=prCalcRemn(quSpecInSelIDCARD.AsInteger,Trunc(quDocsInSelDATEDOC.AsDateTime)-1,quDocsInSelIDSKL.AsInteger);
            if rQRemn<=0.001 then //������� ��� ������ �� ������� ������
            begin
              quClosePartIn.ParamByName('IDCARD').AsInteger:=quSpecInSelIDCARD.AsInteger;
              quClosePartIn.ParamByName('IDSKL').AsInteger:=quDocsInSelIDSKL.AsInteger;
              quClosePartIn.ParamByName('IDATE').AsInteger:=Trunc(quDocsInSelDATEDOC.AsDateTime)-1;
              quClosePartIn.ExecQuery;
            end;

            if CommonSet.RecalcNDS=1 then //������������ ���
            begin
              quFindCard.Active:=False;
              quFindCard.ParamByName('IDCARD').AsInteger:=quSpecInSelIDCARD.AsInteger;
              quFindCard.Active:=True;
              if quFindCard.RecordCount>0 then
              begin
                iNDS:=quFindCardINDS.AsInteger;
                prGetNDS(INds,rProcNDS);
                rSumIn0:=rv(quSpecInSelSUMIN.AsFloat*100/(100+rProcNDS));
                rSumNDS:=quSpecInSelSUMIN.AsFloat-rSumIn0;
                rPriceIn0:=rv(quSpecInSelPRICEIN.AsFloat*100/(100+rProcNDS));
                if quSpecInSelQUANT.AsFloat<>0 then rPriceIn0:=rSumIn0/quSpecInSelQUANT.AsFloat;

                quSpecInSel.Edit;
                quSpecInSelPRICE0.AsFloat:=rPriceIn0;
                quSpecInSelSUM0.AsFloat:=rSumIn0;
                quSpecInSelNDSPROC.AsFloat:=rProcNDS;
                quSpecInSelIDNDS.AsInteger:=iNDS;
                quSpecInSelSUMNDS.AsFloat:=rSumNDS;
                quSpecInSel.Post;
              end;
            end;

            sNDS[quSpecInSelIDNDS.AsInteger]:=sNDS[quSpecInSelIDNDS.AsInteger]+quSpecInSelSUMNDS.AsFloat;

            if (quSpecInSelCATEGORY.AsInteger=1)or(quSpecInSelCATEGORY.AsInteger=2)or(quSpecInSelCATEGORY.AsInteger=3) then
            begin
              rSum0:=rSum0+rv(quSpecInSelSUM0.AsFloat);
              rSumIn:=rSumIn+rv(quSpecInSelSUMIN.AsFloat);
              rSum2:=rSum2+rv(quSpecInSelSUMUCH.AsFloat);
            end;
            if (quSpecInSelCATEGORY.AsInteger=4) then
            begin
              rSumT:=rSumT+rv(quSpecInSelSUMIN.AsFloat);
            end;

            quSpecInSel.Next;
          end;
          quSpecInSel.Active:=False;

          iSS:=prISS(quDInIDSKL.AsInteger);

          prAddPartIn.ParamByName('IDDOC').AsInteger:=quDInID.AsInteger;
          prAddPartIn.ParamByName('DTYPE').AsInteger:=1;
          prAddPartIn.ParamByName('IDSKL').AsInteger:=quDInIDSKL.AsInteger;
          prAddPartIn.ParamByName('IDCLI').AsInteger:=quDInIDCLI.AsInteger;
          prAddPartIn.ParamByName('IDATE').AsInteger:=iDateCur;
          prAddPartIn.ParamByName('ISS').AsInteger:=iSS;
          prAddPartIn.ExecProc;

          quDIn.Edit;
          if iSS<>2 then
          begin
//          quDInSUMIN.AsFloat:=rSum1;
//          quDInSUMTAR.AsFloat:=rSum2;
            quDInSUMIN.AsFloat:=RoundVal(rSumIn);
            quDInSUMUCH.AsFloat:=RoundVal(rSum2);
            quDInSUMTAR.AsFloat:=RoundVal(rSumT);
            quDInSUMNDS0.AsFloat:=RoundVal(sNDS[1]);
            quDInSUMNDS1.AsFloat:=RoundVal(sNDS[2]);
            quDInSUMNDS2.AsFloat:=RoundVal(sNDS[3]);
          end else
          begin
//          quDInSUMIN.AsFloat:=rSum0;
//          quDInSUMTAR.AsFloat:=rSum3;
            quDInSUMIN.AsFloat:=RoundVal(rSum0);
            quDInSUMUCH.AsFloat:=RoundVal(rSum2);
            quDInSUMTAR.AsFloat:=RoundVal(rSumT);
            quDInSUMNDS0.AsFloat:=RoundVal(sNDS[1]);
            quDInSUMNDS1.AsFloat:=RoundVal(sNDS[2]);
            quDInSUMNDS2.AsFloat:=RoundVal(sNDS[3]);
          end;
          quDInIACTIVE.AsInteger:=1;
          quDIn.Post;

          quDIn.Next; delay(10); inc(iE);
        end;
        quDIn.Active:=False;
        prWr('    --- ������� ��. ('+INtToStr(iE)+')'); delay(10);


        prWr('    ������������.');

        iE:=0;
        quDCompl.Active:=False;
        quDCompl.ParamByName('DDATE').AsDate:=iDateCur;
        quDCompl.ParamByName('ISKL').AsINteger:=iSkl;
        quDCompl.ParamByName('ISKL1').AsINteger:=iSkl;
        quDCompl.Active:=True;
        quDCompl.First;
        while not quDCompl.Eof do
        begin
          fmDocsCompl.prOpenSpec(quDComplID.AsInteger); delay(100);
          fmAddCompl.prCalcC(iDateCur,quDComplIDSKL.AsInteger); delay(100);
          fmAddCompl.prCalcPr(quDComplIDSKL.AsInteger); delay(100);
          fmAddCompl.prSave(quDComplID.AsInteger,rSum0,rSum1,rSum2); delay(100);
          fmDocsCompl.prOn(quDComplID.AsInteger,quDComplIDSKL.AsInteger,iDateCur,quDComplIDSKLTO.AsInteger,rSum1,rSum2);

          quDCompl.Edit;
          quDComplIACTIVE.AsInteger:=1;
          quDComplSUMIN.AsFloat:=rSum1;
          quDComplSUMUCH.AsFloat:=rSum2;
          quDCompl.Post;

          quDCompl.Next; delay(10);inc(iE);
        end;
        quDCompl.Active:=False;

        prWr('    --- ������� ��. ('+INtToStr(iE)+')'); delay(10);

        prWr('    ���������� �����������.');

        iE:=0;
        quDVn.Active:=False;
        quDVn.ParamByName('DDATE').AsDate:=iDateCur;
        quDVn.ParamByName('ISKL').AsINteger:=iSkl;
        quDVn.ParamByName('ISKL1').AsINteger:=iSkl;
        quDVn.Active:=True;
        quDVn.First;
        while not quDVn.Eof do
        begin
          fmDocsVn.prPart(quDVnID.AsInteger,quDVnIDSKL_FROM.AsInteger,quDVnIDSKL_TO.AsInteger,iDateCur,iSkl,rSum1,rSum2);

          quDVn.Edit;
          quDVnIACTIVE.AsInteger:=1;
          quDVnSUMIN.AsFloat:=RoundVal(rSum1);
          quDVnSUMTAR.AsFloat:=RoundVal(rSum2);
          quDVn.Post;

          quDVn.Next; delay(10);inc(iE);
        end;
        quDVn.Active:=False;

        prWr('    --- ������� ��. ('+INtToStr(iE)+')'); delay(10);


        prWr('    ���������� �� �������.');

        iE:=0;
        quDR.Active:=False;
        quDR.ParamByName('DDATE').AsDate:=iDateCur;
        quDR.ParamByName('ISKL').AsINteger:=iSkl;
        quDR.Active:=True;
        quDR.First;
        while not quDR.Eof do
        begin
          fmDocsReal.prOn(quDRID.AsInteger,quDRIDSKL.AsInteger,iDateCur,quDRIDCLI.AsInteger,rSum1,rSum2);

          quDR.Edit;
          quDRIACTIVE.AsInteger:=1;
          quDRSUMIN.AsFloat:=RoundVal(rSum1);
          quDRSUMTAR.AsFloat:=RoundVal(rSum2);
          quDR.Post;

          quDR.Next; delay(10);inc(iE);
        end;
        quDR.Active:=False;

        prWr('    --- ������� ��. ('+INtToStr(iE)+')'); delay(10);


        prWr('    ���� �����������.');

        iE:=0;
        quDAct.Active:=False;
        quDAct.ParamByName('DDATE').AsDate:=iDateCur;
        quDAct.ParamByName('ISKL').AsINteger:=iSkl;
        quDAct.Active:=True;
        quDAct.First;
        while not quDAct.Eof do
        begin
          fmDocsActs.prPriceOut(quDActID.AsInteger,quDActIDSKL.AsInteger,iDateCur); delay(100);//�������� ������ � ������������ ���� ������
          fmDocsActs.prPriceIn(quDActID.AsInteger); delay(100);//�������� ���� ������� � �������
          fmDocsActs.prPartIn(quDActID.AsInteger,quDActIDSKL.AsInteger,iDateCur,rSum1);delay(100); // ������������ ��������� ������

          rSum2:=rSum1; //����

          quDAct.Edit;
          quDActIACTIVE.AsInteger:=1;
          quDActSUMIN.AsFloat:=rSum1;
          quDActSUMUCH.AsFloat:=rSum2;
          quDAct.Post;

          quDAct.Next; delay(10);inc(iE);
        end;
        quDAct.Active:=False;

        prWr('    --- ������� ��. ('+INtToStr(iE)+')'); delay(10);


        prWr('    ������� ����������.');

        iE:=0;
        quDRet.Active:=False;
        quDRet.ParamByName('DDATE').AsDate:=iDateCur;
        quDRet.ParamByName('ISKL').AsINteger:=iSkl;
        quDRet.Active:=True;
        quDRet.First;
        while not quDRet.Eof do
        begin
//        fmDocsVn.prPart(quDRetID.AsInteger,quDRetIDSKL_FROM.AsInteger,quDRetIDSKL_TO.AsInteger,iDateCur,rSum1,rSum2);
          fmDocsOut.prOn(quDRetID.AsInteger,quDRetIDSKL.AsInteger,iDateCur,quDRetIDCLI.AsInteger,rSum1,rSum2);

          quDRet.Edit;
          quDRetIACTIVE.AsInteger:=1;
          quDRetSUMIN.AsFloat:=RoundVal(rSum1);
          quDRetSUMTAR.AsFloat:=RoundVal(rSum2);
          quDRet.Post;

          quDRet.Next; delay(10);inc(iE);
        end;
        quDRet.Active:=False;

        prWr('    --- ������� ��. ('+INtToStr(iE)+')'); delay(10);

        prWr('    ����������.');

        iE:=0;
        quDOutB.Active:=False;
        quDOutB.ParamByName('DDATE').AsDate:=iDateCur;
        quDOutB.ParamByName('ISKL').AsINteger:=iSkl;
        quDOutB.Active:=True;
        quDOutB.First;
        while not quDOutB.Eof do
        begin
          prWr('        ���. �'+quDOutBNUMDOC.AsString+' '+FloatToStr(quDOutBSUMUCH.AsFloat)+' ����. '+quDOutBOPER.AsString);
//        prWr('          prOpen');
          fmDocsOutB.prOpen(quDOutBID.AsInteger,1,Memo1);  //1 - ������ ��������, �� ��������� ������������ ������� � �������
//        prWr('          prCalcQuant');
          fmDOBSpec.prCalcQuant(quDOutBIDSKL.AsInteger,iDateCur,False,FindTSpis(quDOutBOPER.AsString));                                           // ��� �������� �������
//        prWr('          prSave');

//������� �������� ����
          fmDOBSpec.prCalcPrice(quDOutBIDSKL.AsInteger,iDateCur);

          fmDOBSpec.prSave(quDOutBID.AsInteger,quDOutBIDSKL.AsInteger,rSum1,rSum2);
//        prWr('          prOn');
          fmDocsOutB.prOn(quDOutBID.AsInteger,quDOutBIDSKL.AsInteger,iDateCur,rSum2,rSum2);

//        prWr('          end');
          quDOutB.Edit;
          quDOutBIACTIVE.AsInteger:=1;
          quDOutBSUMUCH.AsFloat:=RoundVal(rSum1);
          quDOutBSUMIN.AsFloat:=RoundVal(rSum2);
          quDOutB.Post;

          quDOutB.Next; delay(10);inc(iE);
        end;
        quDOutB.Active:=False;

        prWr('    --- ������� ��. ('+INtToStr(iE)+')'); delay(10);


        prWr('    ��������������.');

        iE:=0;
        quDInv.Active:=False;
        quDInv.ParamByName('DDATE').AsDate:=iDateCur;
        quDInv.ParamByName('ISKL').AsINteger:=iSkl;
        quDInv.Active:=True;
        quDInv.First;
        while not quDInv.Eof do
        begin
          prWr('        ���. �'+quDInvNUMDOC.AsString+'����� ����.'+FloatToStr(RoundVal(quDInvSUM1.AsFloat))+' ����.'+FloatToStr(RoundVal(quDInvSUM2.AsFloat)));
          fmDocsInv.prCalcRemnInv(quDInvID.AsInteger,iDateCur,quDInvIDSKL.AsInteger); //�������� ���������� ���-��
          //����������
          fmDocsInv.prOn(quDInvId.AsInteger,iDateCur,quDInvIDSKL.AsInteger,rSum1,rSum2,rSum11,rSum22);


          quDInv.Edit;
          quDInvSUM1.AsFloat:=RoundVal(rSum1);
          quDInvSUM11.AsFloat:=RoundVal(rSum11);
          quDInvSUM2.AsFloat:=RoundVal(rSum2);
          quDInvSUM21.AsFloat:=RoundVal(rSum22);
          quDInvIACTIVE.AsInteger:=1;
          quDInv.Post;

          quDInv.Next; delay(10);inc(iE);
        end;
        quDInv.Active:=False;

        bPrintMemo:=True;

        prWr('    --- ������� ��. ('+INtToStr(iE)+')'); delay(10);
      end;
    end;

    prWr('  ����������� ������ ����������.(1)');
    prSetSpecActive.ParamByName('DDATE').AsDateTime:=iDateB;
    prSetSpecActive.ParamByName('ATYPE').AsInteger:=1;
    prSetSpecActive.ParamByName('ISKL').AsInteger:=iSkl;
    prSetSpecActive.ExecProc;
    prWr('  --- ������� ��. '); delay(10);

  end else
  begin
    with dmO do
    with dmORep do
    begin
      if quMHAll.Active then
      begin
        quMHAll.First;
        while not quMHAll.Eof do
        begin
          iSkl:=quMHAllID.AsInteger;
          prWr('�������� �������� �� �� '+quMHAllNAMEMH.AsString);

          prWr('  ������ ������ �� �� '+quMHAllNAMEMH.AsString);
          prDelPer.ParamByName('IDATE').AsInteger:=iDateB;
          prDelPer.ParamByName('ISKL').AsInteger:=iSkl;
          prDelPer.ExecProc;
          prWr('  --- ������� ��.'); delay(10);

          prWr('  ���� ����������� �����.');
          prTestPart.ExecProc;
          iE:=prTestPart.ParamByName('RESULT').AsInteger;
          prWr('  --- ������� ��. ('+INtToStr(iE)+')'); delay(10);

          if cxCheckBox1.Checked then
          begin
            prWr('  �������� ����������� �����.');
            prWr('    ����� ... ');

            try
              quC.Active:=False;
              quC.Active:=True;

              iC:=0;
              quC.First;
              while not quC.Eof do
              begin

        //������� ��� �� ���� - �� �� ����� ����������� ���

                rQ:=prCalcRemn(quCID.AsInteger,iDateB-1,iSkl);

//      EXECUTE PROCEDURE PR_NORMPARTIN (?IDSKL, ?IDATE, ?IDCARD, ?QUANT)

                PRNORMPARTIN.ParamByName('IDSKL').AsInteger:=iSkl;
                PRNORMPARTIN.ParamByName('IDATE').AsInteger:=iDateB-1;
                PRNORMPARTIN.ParamByName('IDCARD').AsInteger:=quCID.AsInteger;
                PRNORMPARTIN.ParamByName('QUANT').AsFloat:=rQ;
                PRNORMPARTIN.ExecProc;
                delay(10);

                quC.Next; inc(iC);
                if iC mod 100 = 0 then
                begin
                  prWr('    ����� ... ');
                end;
              end;
            finally
              quC.Active:=False;
            end;

            prWr('  --- ������� ��.'); delay(10);
          end;

          prWr('  ����������� ������ ����������.(2) �� �� '+quMHAllNAMEMH.AsString);
          prSetSpecActive.ParamByName('DDATE').AsDateTime:=iDateB;
          prSetSpecActive.ParamByName('ATYPE').AsInteger:=2;
          prSetSpecActive.ParamByName('ISKL').AsInteger:=iSkl;
          prSetSpecActive.ExecProc;
          prWr('  --- ������� ��. '); delay(10);

          for iDateCur:=iDateB to iDateE do
          begin
            prWr('  ��������� - '+FormatDateTime('dd.mm.yyyy',iDateCur)+' ('+IntToStr(iDateCur)+')');

            delay(10); //�������� 10 ��� �� ������ ������ - ����� �������� ������ ��������

            bPrintMemo:=False;

            prWr('    ��������� ��������� �� �� '+quMHAllNAMEMH.AsString);
            iE:=0;
            quDIn.Active:=False;
            quDIn.ParamByName('DDATE').AsDate:=iDateCur;
            quDIn.ParamByName('ISKL').AsInteger:=iSkl;
            quDIn.Active:=True;
            quDIn.First;
            while not quDIn.Eof do
            begin
              quSpecInSel.Active:=False;
              quSpecInSel.ParamByName('IDHD').AsInteger:=quDInID.AsInteger;
              quSpecInSel.Active:=True;

              rSum1:=0;

              rSum0:=0;  rSumIn:=0;
              sNDS[1]:=0;sNDS[2]:=0;sNDS[3]:=0;
              rSum2:=0;  rSumT:=0;


              quSpecInSel.First;
              while not quSpecInSel.Eof do
              begin
                if quSpecInSelCATEGORY.AsInteger=1 then rSum1:=rSum1+quSpecInSelSUMIN.AsFloat;
                if quSpecInSelCATEGORY.AsInteger=4 then rSum2:=rSum2+quSpecInSelSUMIN.AsFloat;

                //�������� �������� �������� �� ����. ���� ������� =0 �� ������� ��� ���������� ������
                rQRemn:=prCalcRemn(quSpecInSelIDCARD.AsInteger,Trunc(quDocsInSelDATEDOC.AsDateTime)-1,quDocsInSelIDSKL.AsInteger);
                if rQRemn<=0.001 then //������� ��� ������ �� ������� ������
                begin
                  quClosePartIn.ParamByName('IDCARD').AsInteger:=quSpecInSelIDCARD.AsInteger;
                  quClosePartIn.ParamByName('IDSKL').AsInteger:=quDocsInSelIDSKL.AsInteger;
                  quClosePartIn.ParamByName('IDATE').AsInteger:=Trunc(quDocsInSelDATEDOC.AsDateTime)-1;
                  quClosePartIn.ExecQuery;
                end;

                if CommonSet.RecalcNDS=1 then //������������ ���
                begin
                  quFindCard.Active:=False;
                  quFindCard.ParamByName('IDCARD').AsInteger:=quSpecInSelIDCARD.AsInteger;
                  quFindCard.Active:=True;
                  if quFindCard.RecordCount>0 then
                  begin
                    iNDS:=quFindCardINDS.AsInteger;
                    prGetNDS(INds,rProcNDS);
                    rSumIn0:=rv(quSpecInSelSUMIN.AsFloat*100/(100+rProcNDS));
                    rSumNDS:=quSpecInSelSUMIN.AsFloat-rSumIn0;
                    rPriceIn0:=rv(quSpecInSelPRICEIN.AsFloat*100/(100+rProcNDS));
                    if quSpecInSelQUANT.AsFloat<>0 then rPriceIn0:=rSumIn0/quSpecInSelQUANT.AsFloat;

                    quSpecInSel.Edit;
                    quSpecInSelPRICE0.AsFloat:=rPriceIn0;
                    quSpecInSelSUM0.AsFloat:=rSumIn0;
                    quSpecInSelNDSPROC.AsFloat:=rProcNDS;
                    quSpecInSelIDNDS.AsInteger:=iNDS;
                    quSpecInSelSUMNDS.AsFloat:=rSumNDS;
                    quSpecInSel.Post;
                  end;
                end;

                sNDS[quSpecInSelIDNDS.AsInteger]:=sNDS[quSpecInSelIDNDS.AsInteger]+quSpecInSelSUMNDS.AsFloat;

                if (quSpecInSelCATEGORY.AsInteger=1)or(quSpecInSelCATEGORY.AsInteger=2)or(quSpecInSelCATEGORY.AsInteger=3) then
                begin
                  rSum0:=rSum0+rv(quSpecInSelSUM0.AsFloat);
                  rSumIn:=rSumIn+rv(quSpecInSelSUMIN.AsFloat);
                  rSum2:=rSum2+rv(quSpecInSelSUMUCH.AsFloat);
                end;
                if (quSpecInSelCATEGORY.AsInteger=4) then
                begin
                  rSumT:=rSumT+rv(quSpecInSelSUMIN.AsFloat);
                end;

                quSpecInSel.Next;
              end;
              quSpecInSel.Active:=False;

              iSS:=prISS(quDInIDSKL.AsInteger);

              prAddPartIn.ParamByName('IDDOC').AsInteger:=quDInID.AsInteger;
              prAddPartIn.ParamByName('DTYPE').AsInteger:=1;
              prAddPartIn.ParamByName('IDSKL').AsInteger:=quDInIDSKL.AsInteger;
              prAddPartIn.ParamByName('IDCLI').AsInteger:=quDInIDCLI.AsInteger;
              prAddPartIn.ParamByName('IDATE').AsInteger:=iDateCur;
              prAddPartIn.ParamByName('ISS').AsInteger:=iSS;
              prAddPartIn.ExecProc;

              quDIn.Edit;
              if iSS<>2 then
              begin
//              quDInSUMIN.AsFloat:=rSum1;
//              quDInSUMTAR.AsFloat:=rSum2;
                quDInSUMIN.AsFloat:=RoundVal(rSumIn);
                quDInSUMUCH.AsFloat:=RoundVal(rSum2);
                quDInSUMTAR.AsFloat:=RoundVal(rSumT);
                quDInSUMNDS0.AsFloat:=RoundVal(sNDS[1]);
                quDInSUMNDS1.AsFloat:=RoundVal(sNDS[2]);
                quDInSUMNDS2.AsFloat:=RoundVal(sNDS[3]);
              end else
              begin
//              quDInSUMIN.AsFloat:=rSum0;
//              quDInSUMTAR.AsFloat:=rSum3;
                quDInSUMIN.AsFloat:=RoundVal(rSum0);
                quDInSUMUCH.AsFloat:=RoundVal(rSum2);
                quDInSUMTAR.AsFloat:=RoundVal(rSumT);
                quDInSUMNDS0.AsFloat:=RoundVal(sNDS[1]);
                quDInSUMNDS1.AsFloat:=RoundVal(sNDS[2]);
                quDInSUMNDS2.AsFloat:=RoundVal(sNDS[3]);
              end;
              quDInIACTIVE.AsInteger:=1;
              quDIn.Post;

              quDIn.Next; delay(10); inc(iE);
            end;
            quDIn.Active:=False;
            prWr('    --- ������� ��. ('+INtToStr(iE)+')'); delay(10);


            prWr('    ������������ �� �� '+quMHAllNAMEMH.AsString);

            iE:=0;
            quDCompl.Active:=False;
            quDCompl.ParamByName('DDATE').AsDate:=iDateCur;
            quDCompl.ParamByName('ISKL').AsINteger:=iSkl;
            quDCompl.ParamByName('ISKL1').AsINteger:=iSkl;
            quDCompl.Active:=True;
            quDCompl.First;
            while not quDCompl.Eof do
            begin
              fmDocsCompl.prOpenSpec(quDComplID.AsInteger); delay(100);
              fmAddCompl.prCalcC(iDateCur,quDComplIDSKL.AsInteger); delay(100);
              fmAddCompl.prCalcPr(quDComplIDSKL.AsInteger); delay(100);
              fmAddCompl.prSave(quDComplID.AsInteger,rSum0,rSum1,rSum2); delay(100);

              iSklFrom:=0;
              iSklTo:=0;

              if iSkl=quDComplIDSKL.AsInteger then iSklFrom:=quDComplIDSKL.AsInteger;
              if iSkl=quDComplIDSKLTO.AsInteger then iSklTo:=quDComplIDSKLTO.AsInteger;

              fmDocsCompl.prOn(quDComplID.AsInteger,iSklFrom,iDateCur,iSklTo,rSum1,rSum2);

              quDCompl.Edit;
              quDComplIACTIVE.AsInteger:=1;
              quDComplSUMIN.AsFloat:=rSum1;
              quDComplSUMUCH.AsFloat:=rSum2;
              quDCompl.Post;

              quDCompl.Next; delay(10);inc(iE);
            end;
            quDCompl.Active:=False;

            prWr('    --- ������� ��. ('+INtToStr(iE)+')'); delay(10);

            prWr('    ���������� ����������� �� �� '+quMHAllNAMEMH.AsString);

            iE:=0;
            quDVn.Active:=False;
            quDVn.ParamByName('DDATE').AsDate:=iDateCur;
            quDVn.ParamByName('ISKL').AsINteger:=iSkl;
            quDVn.ParamByName('ISKL1').AsINteger:=iSkl;
            quDVn.Active:=True;
            quDVn.First;
            while not quDVn.Eof do
            begin
              fmDocsVn.prPart(quDVnID.AsInteger,quDVnIDSKL_FROM.AsInteger,quDVnIDSKL_TO.AsInteger,iDateCur,iSkl,rSum1,rSum2);

              quDVn.Edit;
              quDVnIACTIVE.AsInteger:=1;
              quDVnSUMIN.AsFloat:=RoundVal(rSum1);
              quDVnSUMTAR.AsFloat:=RoundVal(rSum2);
              quDVn.Post;

              quDVn.Next; delay(10);inc(iE);
            end;
            quDVn.Active:=False;

            prWr('    --- ������� ��. ('+INtToStr(iE)+')'); delay(10);


            prWr('    ���������� �� ������� �� �� '+quMHAllNAMEMH.AsString);

            iE:=0;
            quDR.Active:=False;
            quDR.ParamByName('DDATE').AsDate:=iDateCur;
            quDR.ParamByName('ISKL').AsINteger:=iSkl;
            quDR.Active:=True;
            quDR.First;
            while not quDR.Eof do
            begin
              fmDocsReal.prOn(quDRID.AsInteger,quDRIDSKL.AsInteger,iDateCur,quDRIDCLI.AsInteger,rSum1,rSum2);

              quDR.Edit;
              quDRIACTIVE.AsInteger:=1;
              quDRSUMIN.AsFloat:=RoundVal(rSum1);
              quDRSUMTAR.AsFloat:=RoundVal(rSum2);
              quDR.Post;

              quDR.Next; delay(10);inc(iE);
            end;
            quDR.Active:=False;

            prWr('    --- ������� ��. ('+INtToStr(iE)+')'); delay(10);
            prWr('    ���� ����������� �� �� '+quMHAllNAMEMH.AsString);

            iE:=0;
            quDAct.Active:=False;
            quDAct.ParamByName('DDATE').AsDate:=iDateCur;
            quDAct.ParamByName('ISKL').AsINteger:=iSkl;
            quDAct.Active:=True;
            quDAct.First;
            while not quDAct.Eof do
            begin
              fmDocsActs.prPriceOut(quDActID.AsInteger,quDActIDSKL.AsInteger,iDateCur); delay(100);//�������� ������ � ������������ ���� ������
              fmDocsActs.prPriceIn(quDActID.AsInteger); delay(100);//�������� ���� ������� � �������
              fmDocsActs.prPartIn(quDActID.AsInteger,quDActIDSKL.AsInteger,iDateCur,rSum1);delay(100); // ������������ ��������� ������

              rSum2:=rSum1; //����

              quDAct.Edit;
              quDActIACTIVE.AsInteger:=1;
              quDActSUMIN.AsFloat:=rSum1;
              quDActSUMUCH.AsFloat:=rSum2;
              quDAct.Post;

              quDAct.Next; delay(10);inc(iE);
            end;
            quDAct.Active:=False;

            prWr('    --- ������� ��. ('+INtToStr(iE)+')'); delay(10);
            prWr('    ������� ���������� �� �� '+quMHAllNAMEMH.AsString);

            iE:=0;
            quDRet.Active:=False;
            quDRet.ParamByName('DDATE').AsDate:=iDateCur;
            quDRet.ParamByName('ISKL').AsINteger:=iSkl;
            quDRet.Active:=True;
            quDRet.First;
            while not quDRet.Eof do
            begin
//            fmDocsVn.prPart(quDRetID.AsInteger,quDRetIDSKL_FROM.AsInteger,quDRetIDSKL_TO.AsInteger,iDateCur,rSum1,rSum2);
              fmDocsOut.prOn(quDRetID.AsInteger,quDRetIDSKL.AsInteger,iDateCur,quDRetIDCLI.AsInteger,rSum1,rSum2);

              quDRet.Edit;
              quDRetIACTIVE.AsInteger:=1;
              quDRetSUMIN.AsFloat:=RoundVal(rSum1);
              quDRetSUMTAR.AsFloat:=RoundVal(rSum2);
              quDRet.Post;

              quDRet.Next; delay(10);inc(iE);
            end;
            quDRet.Active:=False;
            prWr('    --- ������� ��. ('+INtToStr(iE)+')'); delay(10);
            prWr('    ���������� �� �� '+quMHAllNAMEMH.AsString);
            iE:=0;
            quDOutB.Active:=False;
            quDOutB.ParamByName('DDATE').AsDate:=iDateCur;
            quDOutB.ParamByName('ISKL').AsINteger:=iSkl;
            quDOutB.Active:=True;
            quDOutB.First;
            while not quDOutB.Eof do
            begin
              prWr('        ���. �'+quDOutBNUMDOC.AsString+' '+FloatToStr(quDOutBSUMUCH.AsFloat)+' ����. '+quDOutBOPER.AsString);
              fmDocsOutB.prOpen(quDOutBID.AsInteger,1,Memo1);  //1 - ������ ��������, �� ��������� ������������ ������� � �������
              fmDOBSpec.prCalcQuant(quDOutBIDSKL.AsInteger,iDateCur,False,FindTSpis(quDOutBOPER.AsString));                                           // ��� �������� �������
              fmDOBSpec.prCalcPrice(quDOutBIDSKL.AsInteger,iDateCur);
              fmDOBSpec.prSave(quDOutBID.AsInteger,quDOutBIDSKL.AsInteger,rSum1,rSum2);
              fmDocsOutB.prOn(quDOutBID.AsInteger,quDOutBIDSKL.AsInteger,iDateCur,rSum2,rSum2);
              quDOutB.Edit;
              quDOutBIACTIVE.AsInteger:=1;
              quDOutBSUMUCH.AsFloat:=RoundVal(rSum1);
              quDOutBSUMIN.AsFloat:=RoundVal(rSum2);
              quDOutB.Post;

              quDOutB.Next; delay(10);inc(iE);
            end;
            quDOutB.Active:=False;

            prWr('    --- ������� ��. ('+INtToStr(iE)+')'); delay(10);

            prWr('    �������������� �� �� '+quMHAllNAMEMH.AsString);

            iE:=0;
            quDInv.Active:=False;
            quDInv.ParamByName('DDATE').AsDate:=iDateCur;
            quDInv.ParamByName('ISKL').AsINteger:=iSkl;
            quDInv.Active:=True;
            quDInv.First;
            while not quDInv.Eof do
            begin
              prWr('        ���. �'+quDInvNUMDOC.AsString+'����� ����.'+FloatToStr(RoundVal(quDInvSUM1.AsFloat))+' ����.'+FloatToStr(RoundVal(quDInvSUM2.AsFloat)));
              fmDocsInv.prCalcRemnInv(quDInvID.AsInteger,iDateCur,quDInvIDSKL.AsInteger); //�������� ���������� ���-��
              //����������
              fmDocsInv.prOn(quDInvId.AsInteger,iDateCur,quDInvIDSKL.AsInteger,rSum1,rSum2,rSum11,rSum22);


              quDInv.Edit;
              quDInvSUM1.AsFloat:=RoundVal(rSum1);
              quDInvSUM11.AsFloat:=RoundVal(rSum11);
              quDInvSUM2.AsFloat:=RoundVal(rSum2);
              quDInvSUM21.AsFloat:=RoundVal(rSum22);
              quDInvIACTIVE.AsInteger:=1;
              quDInv.Post;

              quDInv.Next; delay(10);inc(iE);
            end;
            quDInv.Active:=False;

            bPrintMemo:=True;

            prWr('    --- ������� ��. ('+INtToStr(iE)+')'); delay(10);

          end;

          prWr('  ����������� ������ ����������.(1)');
          prSetSpecActive.ParamByName('DDATE').AsDateTime:=iDateB;
          prSetSpecActive.ParamByName('ATYPE').AsInteger:=1;
          prSetSpecActive.ParamByName('ISKL').AsInteger:=iSkl;
          prSetSpecActive.ExecProc;
          prWr('  --- ������� ��. '); delay(10);

          quMHAll.Next;
        end;
      end; //if quMHAll.Active then
    end;  //with
  end;

  prWr('�������� ��������.');
end;

procedure TfmRecalcPer.cxButton2Click(Sender: TObject);
begin
  close;
end;

procedure TfmRecalcPer.FormShow(Sender: TObject);
begin
//  fmRecalcPer.cxCheckBox2.Checked:=False;
end;

end.
