unit AddOperT;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, dxfBackGround, StdCtrls,
  cxButtons, ExtCtrls, cxControls, cxContainer, cxEdit, cxTextEdit,
  cxGraphics, cxMaskEdit, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox, cxImageComboBox;

type
  TfmAddOper = class(TForm)
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    dxfBackGround1: TdxfBackGround;
    Label1: TLabel;
    cxTextEdit1: TcxTextEdit;
    Label2: TLabel;
    Label3: TLabel;
    cxTextEdit2: TcxTextEdit;
    cxLookupComboBox1: TcxLookupComboBox;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    cxImageComboBox1: TcxImageComboBox;
    cxImageComboBox2: TcxImageComboBox;
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAddOper: TfmAddOper;

implementation

uses DMOReps;

{$R *.dfm}

procedure TfmAddOper.cxButton2Click(Sender: TObject);
begin
  Close;
end;

procedure TfmAddOper.cxButton1Click(Sender: TObject);
Var iC:Integer;
begin
//
  if cxTextEdit1.Text='' then
  begin
    ShowMessage('������������ ������ ���� ����������.');
    exit;
  end;

  with dmORep do
  begin
    taOperT.Active:=False;
    taOperT.ParamByName('ABR').AsString:=cxTextEdit1.Text;
    taOperT.Active:=True;
    iC:=taOperT.RecordCount;
    taOperT.Active:=False;
    if cxTextEdit1.Tag=0 then
    begin
      if iC=0 then
      begin
        modalresult:=mrOk;
      end else
      begin
        ShowMessage('������ ������������ "'+cxTextEdit1.Text+'" ������������ ������.')
      end;
    end else
    begin
      if cxTextEdit1.Text=quOperTABR.AsString then
      begin
        if iC=1 then
        begin
          modalresult:=mrOk;
        end else
        begin
          ShowMessage('������ ������������ "'+cxTextEdit1.Text+'" ������������ ������.')
        end;
      end else
      begin
        ShowMessage('�������� ������������ ������.')
      end;
    end;
  end;
end;

end.
