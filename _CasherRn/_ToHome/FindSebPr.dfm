object fmFindSebPr: TfmFindSebPr
  Left = 503
  Top = 263
  BorderStyle = bsDialog
  Caption = #1042#1099#1073#1077#1088#1080#1090#1077' '#1094#1077#1085#1091
  ClientHeight = 123
  ClientWidth = 123
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object GrSPr: TcxGrid
    Left = 0
    Top = 0
    Width = 123
    Height = 123
    Align = alClient
    TabOrder = 0
    LookAndFeel.Kind = lfOffice11
    object VSPr: TcxGridDBTableView
      OnDblClick = VSPrDblClick
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dmORep.dsquFindSebReal
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      object VSPrPRICEIN: TcxGridDBColumn
        Caption = #1062#1077#1085#1072
        DataBinding.FieldName = 'PRICEIN'
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Width = 120
      end
    end
    object LSPr: TcxGridLevel
      GridView = VSPr
    end
  end
end
