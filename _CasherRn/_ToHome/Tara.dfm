object fmTara: TfmTara
  Left = 374
  Top = 268
  Width = 399
  Height = 640
  Caption = #1058#1072#1088#1072
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 587
    Width = 391
    Height = 19
    Panels = <
      item
        Width = 100
      end
      item
        Width = 50
      end>
  end
  object GrTara: TcxGrid
    Left = 8
    Top = 16
    Width = 373
    Height = 557
    TabOrder = 1
    LookAndFeel.Kind = lfOffice11
    object ViTara: TcxGridDBTableView
      OnDblClick = ViTaraDblClick
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dmORep.dsquTara
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      object ViTaraID: TcxGridDBColumn
        Caption = #1050#1086#1076
        DataBinding.FieldName = 'ID'
      end
      object ViTaraNAME: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'NAME'
        Width = 271
      end
    end
    object LeTara: TcxGridLevel
      GridView = ViTara
    end
  end
end
