unit ImportDoc;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons, cxControls,
  cxContainer, cxEdit, cxTextEdit, cxMemo, ExtCtrls, FileUtil, DB, dxmdaset;

type
  TfmImportDocR = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Panel1: TPanel;
    Memo1: TcxMemo;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    ListBox1: TListBox;
    taDH: TdxMemData;
    taDS: TdxMemData;
    taDHsType: TStringField;
    taDHsId: TStringField;
    taDHDocDate: TDateField;
    taDHDocNum: TStringField;
    taDHsIdMH: TStringField;
    taDHsINN: TStringField;
    taDHsNameCli: TStringField;
    taDSsId: TStringField;
    taDSSifr: TIntegerField;
    taDSName: TStringField;
    taDSQuant: TFloatField;
    taDSPriceIn: TFloatField;
    taDSSumIn: TFloatField;
    taDSPriceOut: TFloatField;
    taDSSumOut: TFloatField;
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }

    function trDocRH(sStr:String):Boolean; //���������
    function trDocRS(sStr:String):Boolean; //������
  end;

procedure FindSortFiles;
function ImportFile(fName:String;Var iOk,iNon:Integer):Boolean;

var
  fmImportDocR: TfmImportDocR;

implementation

uses Un1, dmOffice, DMOReps, u2fdk;

{$R *.dfm}

procedure TfmImportDocR.cxButton1Click(Sender: TObject);
Var StrWk:String;
    i,iOk,iNon:Integer;
    IDH,iNum:INteger;
    rSumIn,rSumOut:Real;
begin
  Memo1.Lines.Add('');
  Memo1.Lines.Add('����� ���� ������ ����������.');
  try
    cxButton2.Enabled:=False;
    CloseTe(taDH);
    CloseTe(taDS);

    FindSortFiles; //�������� ��������������� ������ ������ (����� ������������)
    for i:=0 to ListBox1.Items.Count-1 do
    begin
      strwk:=ListBox1.Items[i];
      Memo1.Lines.Add('  ��������� ������: "'+StrWk+'"');
      if ImportFile(StrWk,iOk,iNon) then
      begin
        Memo1.Lines.Add('    ������� ������� - '+IntToStr(iOk)+' �����.');
        Memo1.Lines.Add('    �� ������� - '+IntToStr(iNon)+' �����.');
        Memo1.Lines.Add('  ������ ��.');
        delay(100); //��������� ����������
      end
      else Memo1.Lines.Add('  ������ ��� ������ ������.');
    end;

    with dmO do
    with dmORep do
    begin
      Memo1.Lines.Add(' ');
      Memo1.Lines.Add('  ��������� ��������� � ����.');
      taDH.First;
      while not taDH.Eof do
      begin
        Memo1.Lines.Add('    �������� - '+taDHDocNum.AsString);

        IDH:=GetId('DocIn');

        quDocsInId.Active:=False;
        quDocsInId.ParamByName('IDH').AsInteger:=IDH;
        quDocsInId.Active:=True;


        quDocsInId.First;

        quDocsInId.Append;
        quDocsInIdID.AsInteger:=IDH;
        quDocsInIdDATEDOC.AsDateTime:=taDHDocDate.AsDateTime;
        quDocsInIdNUMDOC.AsString:=taDHDocNum.AsString;
        quDocsInIdDATESF.AsDateTime:=taDHDocDate.AsDateTime;
        quDocsInIdNUMSF.AsString:=taDHDocNum.AsString;
        quDocsInIdIDCLI.AsInteger:=prFindIdCl(taDHsINN.AsString);
        quDocsInIdIDSKL.AsInteger:=StrToINtDef(taDHsIdMH.AsString,0);
        quDocsInIdSUMIN.AsFloat:=0;
        quDocsInIdSUMUCH.AsFloat:=0;
        quDocsInIdSUMTAR.AsFloat:=0;
        quDocsInIdSUMNDS0.AsFloat:=0;
        quDocsInIdSUMNDS1.AsFloat:=0;
        quDocsInIdSUMNDS2.AsFloat:=0;
        quDocsInIdPROCNAC.AsFloat:=0;
        quDocsInIdIACTIVE.AsInteger:=0;
        quDocsInId.Post;

        rSumIn:=0; rSumOut:=0;
        //�������� ������������
        quSpecInSel.Active:=False;
        quSpecInSel.ParamByName('IDHD').AsInteger:=IDH;
        quSpecInSel.Active:=True;


        iNum:=1;

        taDS.First;
        while not taDS.Eof do
        begin
          if taDSsId.AsString=taDHsId.AsString then //������ � ����� ���������
          begin
              quFindCard.Active:=False;
              quFindCard.ParamByName('IDCARD').AsInteger:=taDSSifr.AsInteger;
              quFindCard.Active:=True;
              if quFindCard.RecordCount>0 then
              begin

                quSpecInSel.Append;
                quSpecInSelIDHEAD.AsInteger:=IDH;
                quSpecInSelID.AsInteger:=GetId('SpecIn');
                quSpecInSelNUM.AsInteger:=iNum;
                quSpecInSelIDCARD.AsInteger:=taDSSifr.AsInteger;
                quSpecInSelQUANT.AsFloat:=taDSQuant.AsFloat;
                quSpecInSelPRICEIN.AsFloat:=taDSPriceIn.AsFloat;
                quSpecInSelSUMIN.AsFloat:=rv(taDSSumIn.AsFloat);
                quSpecInSelPRICEUCH.AsFloat:=taDSPriceOut.AsFloat;
                quSpecInSelSUMUCH.AsFloat:=rv(taDSSumOut.AsFloat);
                quSpecInSelIDNDS.AsInteger:=1;
                quSpecInSelSUMNDS.AsFloat:=0;
                quSpecInSelIDM.AsInteger:=quFindCardIMESSURE.AsInteger;
                quSpecInSelKM.AsFloat:=1;
                quSpecInSelSUM0.AsFloat:=rv(taDSSumIn.AsFloat);
                quSpecInSelPRICE0.AsFloat:=taDSPriceIn.AsFloat;
                quSpecInSelNDSPROC.AsFloat:=0;
                quSpecInSel.Post;

                rSumIn:=rSumIn+rv(taDSSumIn.AsFloat);
                rSumOut:=rSumOut+rv(taDSSumOut.AsFloat);

                inc(iNum);
              end else
              begin
                showmessage('�������� � ����� '+taDSSifr.AsString+' �� ������� !!!');
              end;

            taDS.Delete;
          end else taDS.Next;
        end;

        quSpecInSel.Active:=False;

        quDocsInId.Edit;
        quDocsInIdSUMIN.AsFloat:=RoundVal(rSumIn);
        quDocsInIdSUMUCH.AsFloat:=RoundVal(rSumOut);
        quDocsInIdSUMTAR.AsFloat:=0;
        quDocsInIdSUMNDS0.AsFloat:=RoundVal(rSumIn);
        quDocsInIdSUMNDS1.AsFloat:=RoundVal(0);
        quDocsInIdSUMNDS2.AsFloat:=RoundVal(0);
        quDocsInIdPROCNAC.AsFloat:=0;
        if rSumIn<>0 then  quDocsInIdPROCNAC.AsFloat:=(rSumOut-rSumIn)/rSumIn*100;
        quDocsInIdIACTIVE.AsInteger:=0;
        quDocsInId.Post;

        quDocsInId.Active:=False;

        taDH.Next;
      end;
    end;
  finally
    taDH.Active:=False;
    taDS.Active:=False;
    cxButton2.Enabled:=True;
    Memo1.Lines.Add('������� ��������.');
  end;
end;

procedure FindSortFiles;
Var SR:TSearchRec; // ��������� ����������
    FindRes:Integer; // ���������� ��� ������ ���������� ������
begin
  with fmImportDocR do
  begin
    ListBox1.Clear; // ������� ���������� ListBox1 ����� ���������� � ���� ������ ������
    FindRes:=FindFirst(CommonSet.PathImport+'doc*.*',32 ,SR);  //32 faArhive
    While FindRes=0 do // ���� �� ������� ����� (��������), �� ��������� ����
    begin
      ListBox1.Items.Add(SR.Name); // ���������� � ������ �������� ���������� ��������
      FindRes:=FindNext(SR); // ����������� ������ �� �������� ��������
    end;
    FindClose(SR); // ��������� �����
    //���������
  end;
end;

function ImportFile(fName:String;Var iOk,iNon:Integer):Boolean;
Var f:TextFile;
    StrWk,StrWk1:String;
    bDos:Boolean;
    iL:INteger;
begin
  Result:=True;
  iOk:=0; iNon:=0;
  with fmImportDocR do
  begin
    try
      assignfile(F,CommonSet.PathImport+fName);
      reset(F);
      bDos:=False;
      iL:=1;
      while not EOF(F) do
      begin
        ReadLn(F,StrWk);
        if StrWk>'' then
        begin
          if iL<3 then
            if pos('DOS',StrWk)>0 then bDos:=True;
          if bDos then StrWk:=OemToAnsiConvert(StrWk);

          StrWk1:=Copy(StrWk,1,Pos(r,StrWk)-1);
          if pos('����������',StrWk1)>0 then
          begin
            if StrWk1='����������' then
            begin
              if trDocRH(StrWk) then inc(iOk)
              else inc(iNon);
            end else
            begin
              if trDocRS(StrWk) then inc(iOk)
              else inc(iNon);
            end;
          end;
          delay(30);
        end;
        inc(iL);
      end;
      CloseFile(F);
      delay(10);
      MoveFile(CommonSet.PathImport+fName, CommonSet.PathArh+fName);
    except
      Result:=False;
      Memo1.Lines.Add('  ------------- ������ ��������� ������. ');
      CloseFile(F);
    end;
  end;
end;


Function TfmImportDocR.trDocRH(sStr:String):Boolean;
Type TGr = record
     sType:String;
     sId:String;
     DocDate:TDateTime;
     DocNum:String;
     sIdMH,sInn:String;
     end;

Var Gr:TGr;
    n:INteger;
    strwk:String;
begin
  Result:=True;
  for n:=1 to 6 do
  begin
    if pos(r,sStr)>0 then strwk:=Copy(sStr,1,pos(r,sStr)-1) else StrWk:=sStr;
    Case n of
    1: begin
         Gr.sType:=StrWk;
       end;
    2: begin
         Gr.sId:=StrWk;
       end;
    3: begin
         Gr.DocDate:=StrToDateDef(StrWk,Date+101);
         if Gr.DocDate>(Date+90) then result:=False;
       end;
    4: begin
         Gr.DocNum:=StrWk;
       end;
    5: begin
         Gr.sIdMH:=StrWk;
       end;
    6: begin
         Gr.sInn:=StrWk;
       end;
    end;
    Delete(sStr,1,pos(r,sStr));
  end;
  if Result then
  begin
    try
      taDH.Append;
      taDHsType.AsString:=Gr.sType;
      taDHsId.AsString:=Gr.sId;
      taDHDocDate.AsDateTime:=Gr.DocDate;
      taDHDocNum.AsString:=Gr.DocNum;
      taDHsIdMH.AsString:=Gr.sIdMH;
      taDHsINN.AsString:=Gr.sInn;
      taDHsNameCli.AsString:='';
      taDH.Post;
    except
      Result:=False;
    end;
  end;
end;

Function TfmImportDocR.trDocRS(sStr:String):Boolean;
Type TGr = record
     sIdH:String;
     iCode:INteger;
     sName:String;
     Quant,PriceIn,SumIn,PriceOut,SumOut:Real;
     end;

Var Gr:TGr;
    n:INteger;
    strwk:String;
begin
  Result:=True;
  for n:=1 to 9 do
  begin
    if pos(r,sStr)>0 then strwk:=Copy(sStr,1,pos(r,sStr)-1) else StrWk:=sStr;
    Case n of
    1: begin
         Gr.sIdH:=StrWk;
       end;
    2: begin
         Gr.iCode:=StrToINtDef(StrWk,-1);
         if Gr.iCode=-1 then result:=False;
       end;
    3: begin
         Gr.sName:=StrWk;
       end;
    4: begin
       end;
    5: begin
         while pos('.',StrWk)>0 do StrWk[pos('.',StrWk)]:=',';
         Gr.Quant:=StrToFloatDef(StrWk,-10000);
         if Gr.Quant=-10000 then result:=False;
       end;
    6: begin
         while pos('.',StrWk)>0 do StrWk[pos('.',StrWk)]:=',';
         Gr.PriceIn:=StrToFloatDef(StrWk,-10000);
         if Gr.PriceIn=-10000 then result:=False;
       end;
    7: begin
         while pos('.',StrWk)>0 do StrWk[pos('.',StrWk)]:=',';
         Gr.SumIn:=StrToFloatDef(StrWk,-10000);
         if Gr.SumIn=-10000 then result:=False;
       end;
    8: begin
         while pos('.',StrWk)>0 do StrWk[pos('.',StrWk)]:=',';
         Gr.PriceOut:=StrToFloatDef(StrWk,-10000);
         if Gr.PriceOut=-10000 then result:=False;
       end;
    9: begin
         while pos('.',StrWk)>0 do StrWk[pos('.',StrWk)]:=',';
         Gr.SumOut:=StrToFloatDef(StrWk,-10000);
         if Gr.SumOut=-10000 then result:=False;
       end;
    end;
    Delete(sStr,1,pos(r,sStr));
   end;
  if Result then
  begin
    try
      taDS.Append;
      taDSsId.AsString:=Gr.sIdH;
      taDSSifr.AsInteger:=Gr.iCode;
      taDSName.AsString:=Gr.sName;
      taDSQuant.AsFloat:=Gr.Quant;
      taDSPriceIn.AsFloat:=Gr.PriceIn;
      taDSSumIn.AsFloat:=Gr.SumIn;
      taDSPriceOut.AsFloat:=Gr.PriceOut;
      taDSSumOut.AsFloat:=Gr.SumOut;
      taDS.Post;
    except
      Result:=False;
    end;
  end;
end;


procedure TfmImportDocR.cxButton2Click(Sender: TObject);
begin
  Close;
end;

end.
