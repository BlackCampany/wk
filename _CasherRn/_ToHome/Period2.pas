unit Period2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, ExtCtrls, Menus, cxLookAndFeelPainters,
  cxButtons, dxfBackGround;

type
  TfmPeriod2 = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Label1: TLabel;
    DateTimePicker1: TDateTimePicker;
    DateTimePicker2: TDateTimePicker;
    DateTimePicker3: TDateTimePicker;
    DateTimePicker4: TDateTimePicker;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    dxfBackGround1: TdxfBackGround;
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmPeriod2: TfmPeriod2;

implementation

uses Un1;

{$R *.dfm}

procedure TfmPeriod2.cxButton1Click(Sender: TObject);
begin
  TrebSel.DateFrom:=Trunc(DateTimePicker1.Date);
  TrebSel.DateTo:=Trunc(DateTimePicker2.Date)+1;
  TrebSel.TimeFrom:=DateTimePicker3.Time;
  TrebSel.TimeTo:=DateTimePicker4.Time;
end;

procedure TfmPeriod2.cxButton2Click(Sender: TObject);
begin
  close;
end;

end.
