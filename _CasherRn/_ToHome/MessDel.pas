unit MessDel;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxLookAndFeelPainters, ExtCtrls, StdCtrls, cxButtons, Menus,
  cxControls, cxContainer, cxEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit,
  cxCalc, cxGraphics, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox;

type
  TfmMessDel = class(TForm)
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    Bevel1: TBevel;
    Bevel2: TBevel;
    Label1: TLabel;
    Label2: TLabel;
    CalcEdit1: TcxCalcEdit;
    Label3: TLabel;
    cxLookupComboBox1: TcxLookupComboBox;
    procedure CalcEdit1Enter(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmMessDel: TfmMessDel;

implementation

uses Calc, Dm, uDB1;

{$R *.dfm}

procedure TfmMessDel.CalcEdit1Enter(Sender: TObject);
Var rQ:real;
begin
  fmCalc.Caption:='Количество';
  rQ:=dmC.quCurSpecQUANTITY.AsFloat;
  fmCalc.CalcEdit1.EditValue:=rQ;
  fmCalc.ShowModal;
  if fmCalc.ModalResult=mrOk then
  begin
    if (fmCalc.CalcEdit1.EditValue<=rQ) and (fmCalc.CalcEdit1.EditValue>0)
    then CalcEdit1.EditValue:=fmCalc.CalcEdit1.EditValue;
  end;
  fmCalc.Caption:='';
  cxButton3.SetFocus;
end;

end.
