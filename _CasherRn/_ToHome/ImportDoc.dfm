object fmImportDocR: TfmImportDocR
  Left = 335
  Top = 342
  BorderStyle = bsDialog
  Caption = #1048#1084#1087#1086#1088#1090
  ClientHeight = 319
  ClientWidth = 447
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 12
    Width = 93
    Height = 13
    Caption = #1050#1072#1090#1072#1083#1086#1075' '#1080#1084#1087#1086#1088#1090#1072' :'
    Transparent = True
  end
  object Label2: TLabel
    Left = 128
    Top = 12
    Width = 32
    Height = 13
    Caption = 'Label2'
  end
  object ListBox1: TListBox
    Left = 288
    Top = 52
    Width = 141
    Height = 185
    ItemHeight = 13
    TabOrder = 2
  end
  object Panel1: TPanel
    Left = 0
    Top = 264
    Width = 447
    Height = 55
    Align = alBottom
    BevelInner = bvLowered
    Color = 16759739
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 68
      Top = 8
      Width = 89
      Height = 37
      Caption = #1055#1088#1080#1085#1103#1090#1100
      Default = True
      TabOrder = 0
      OnClick = cxButton1Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 284
      Top = 8
      Width = 89
      Height = 37
      Caption = #1042#1099#1093#1086#1076
      TabOrder = 1
      OnClick = cxButton2Click
      LookAndFeel.Kind = lfOffice11
    end
  end
  object Memo1: TcxMemo
    Left = 8
    Top = 44
    Lines.Strings = (
      'Memo1')
    TabOrder = 1
    Height = 205
    Width = 429
  end
  object taDH: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 80
    Top = 92
    object taDHsType: TStringField
      FieldName = 'sType'
      Size = 50
    end
    object taDHsId: TStringField
      FieldName = 'sId'
      Size = 50
    end
    object taDHDocDate: TDateField
      FieldName = 'DocDate'
    end
    object taDHDocNum: TStringField
      FieldName = 'DocNum'
      Size = 50
    end
    object taDHsIdMH: TStringField
      FieldName = 'sIdMH'
    end
    object taDHsINN: TStringField
      FieldName = 'sINN'
    end
    object taDHsNameCli: TStringField
      FieldName = 'sNameCli'
      Size = 100
    end
  end
  object taDS: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 148
    Top = 92
    object taDSsId: TStringField
      FieldName = 'sId'
      Size = 50
    end
    object taDSSifr: TIntegerField
      FieldName = 'Sifr'
    end
    object taDSName: TStringField
      FieldName = 'Name'
      Size = 100
    end
    object taDSQuant: TFloatField
      FieldName = 'Quant'
    end
    object taDSPriceIn: TFloatField
      FieldName = 'PriceIn'
    end
    object taDSSumIn: TFloatField
      FieldName = 'SumIn'
    end
    object taDSPriceOut: TFloatField
      FieldName = 'PriceOut'
    end
    object taDSSumOut: TFloatField
      FieldName = 'SumOut'
    end
  end
end
