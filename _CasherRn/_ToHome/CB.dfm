object dmCB: TdmCB
  OldCreateOrder = False
  Left = 304
  Top = 387
  Height = 267
  Width = 799
  object quDocZHRec: TADOQuery
    Connection = msConnection
    CursorType = ctStatic
    CommandTimeout = 1800
    Parameters = <
      item
        Name = 'IDH'
        Attributes = [paSigned]
        DataType = ftLargeint
        Precision = 19
        Size = 8
        Value = Null
      end>
    SQL.Strings = (
      'SELECT '
      '* FROM [DOCZHEAD] DZH'
      'where DZH.[ID]=:IDH')
    Left = 192
    Top = 9
    object quDocZHRecID: TLargeintField
      FieldName = 'ID'
      ReadOnly = True
    end
    object quDocZHRecDOCDATE: TWideStringField
      FieldName = 'DOCDATE'
      Size = 10
    end
    object quDocZHRecDOCDATEZ: TWideStringField
      FieldName = 'DOCDATEZ'
      FixedChar = True
      Size = 10
    end
    object quDocZHRecIDATE: TIntegerField
      FieldName = 'IDATE'
    end
    object quDocZHRecIDATEZ: TIntegerField
      FieldName = 'IDATEZ'
    end
    object quDocZHRecDAYSTOZ: TSmallintField
      FieldName = 'DAYSTOZ'
    end
    object quDocZHRecDOCNUM: TStringField
      FieldName = 'DOCNUM'
    end
    object quDocZHRecISHOP: TIntegerField
      FieldName = 'ISHOP'
    end
    object quDocZHRecDEPART: TIntegerField
      FieldName = 'DEPART'
    end
    object quDocZHRecITYPE: TSmallintField
      FieldName = 'ITYPE'
    end
    object quDocZHRecINSUMIN: TFloatField
      FieldName = 'INSUMIN'
    end
    object quDocZHRecINSUMIN0: TFloatField
      FieldName = 'INSUMIN0'
    end
    object quDocZHRecDEPARTORG: TIntegerField
      FieldName = 'DEPARTORG'
    end
    object quDocZHRecCLITYPE: TSmallintField
      FieldName = 'CLITYPE'
    end
    object quDocZHRecCLICODE: TIntegerField
      FieldName = 'CLICODE'
    end
    object quDocZHRecIACTIVE: TSmallintField
      FieldName = 'IACTIVE'
    end
    object quDocZHRecDATEEDIT: TDateTimeField
      FieldName = 'DATEEDIT'
    end
    object quDocZHRecPERSON: TStringField
      FieldName = 'PERSON'
      Size = 50
    end
    object quDocZHRecCLISUMIN: TFloatField
      FieldName = 'CLISUMIN'
    end
    object quDocZHRecCLISUMIN0: TFloatField
      FieldName = 'CLISUMIN0'
    end
    object quDocZHRecCLISUMINN: TFloatField
      FieldName = 'CLISUMINN'
    end
    object quDocZHRecCLISUMIN0N: TFloatField
      FieldName = 'CLISUMIN0N'
    end
    object quDocZHRecQUANT: TFloatField
      FieldName = 'QUANT'
    end
    object quDocZHRecCLIQUANT: TFloatField
      FieldName = 'CLIQUANT'
    end
    object quDocZHRecCLIQUANTN: TFloatField
      FieldName = 'CLIQUANTN'
    end
    object quDocZHRecQUANTZAUTO: TFloatField
      FieldName = 'QUANTZAUTO'
    end
    object quDocZHRecIDATEPOST: TIntegerField
      FieldName = 'IDATEPOST'
    end
    object quDocZHRecIDATENACL: TIntegerField
      FieldName = 'IDATENACL'
    end
    object quDocZHRecSNUMNACL: TStringField
      FieldName = 'SNUMNACL'
      Size = 50
    end
    object quDocZHRecIDATESCHF: TIntegerField
      FieldName = 'IDATESCHF'
    end
    object quDocZHRecSNUMSCHF: TStringField
      FieldName = 'SNUMSCHF'
      Size = 50
    end
    object quDocZHRecSENDTO: TSmallintField
      FieldName = 'SENDTO'
    end
    object quDocZHRecSENDDATE: TDateTimeField
      FieldName = 'SENDDATE'
    end
    object quDocZHRecSNUMPODTV: TWideStringField
      FieldName = 'SNUMPODTV'
      Size = 50
    end
    object quDocZHRecSENDNACL: TSmallintField
      FieldName = 'SENDNACL'
    end
    object quDocZHRecIDHPRO: TIntegerField
      FieldName = 'IDHPRO'
    end
  end
  object quDocZSSel: TADOQuery
    Connection = msConnection
    CursorType = ctStatic
    CommandTimeout = 1800
    Parameters = <
      item
        Name = 'IIDH'
        Attributes = [paSigned]
        DataType = ftLargeint
        Precision = 19
        Size = 8
        Value = '1'
      end>
    SQL.Strings = (
      'SELECT ds.[IDH]'
      '      ,ds.[IDS]'
      '      ,ds.[CODE]'
      '      ,ds.[BARCODE]'
      '      ,ds.[QUANTPACK]'
      '      ,ds.[PRICEIN]'
      '      ,ds.[PRICEIN0]'
      '      ,ds.[PRICEM]'
      '      ,ds.[SUMIN]'
      '      ,ds.[SUMIN0]'
      '      ,ds.[SUMNDS]'
      '      ,ds.[NDSPROC]'
      '      ,ds.[REMN1]'
      '      ,ds.[VREAL]'
      '      ,ds.[REMNDAY]'
      '      ,ds.[REMNMIN]'
      '      ,ds.[REMNMAX]'
      '      ,ds.[REMNSTRAH]'
      '      ,ds.[REMN2]'
      '      ,ds.[PKDOC]'
      '      ,ds.[PKCARD]'
      '      ,ds.[DAYSTOZ]'
      '      ,ds.[QUANTZ]'
      '      ,ds.[QUANTZALR]'
      '      ,ds.[QUANTZITOG]'
      '      ,ds.[QUANTZFACT]'
      '      ,ds.[CLIQUANT]'
      '      ,ds.[CLIPRICE]'
      '      ,ds.[CLIPRICE0]'
      '      ,ds.[CLIQUANTN]'
      '      ,ds.[CLIPRICEN]'
      '      ,ds.[CLIPRICE0N]'
      '      ,ds.[CLINNUM]'
      '     ,ds.[ROUNDPACK]'
      '  '
      'FROM [DOCZSPEC] ds'
      'where ds.[IDH]=:IIDH')
    Left = 112
    Top = 9
    object quDocZSSelIDH: TLargeintField
      FieldName = 'IDH'
    end
    object quDocZSSelIDS: TLargeintField
      FieldName = 'IDS'
      ReadOnly = True
    end
    object quDocZSSelCODE: TIntegerField
      FieldName = 'CODE'
    end
    object quDocZSSelBARCODE: TStringField
      FieldName = 'BARCODE'
      FixedChar = True
      Size = 15
    end
    object quDocZSSelQUANTPACK: TFloatField
      FieldName = 'QUANTPACK'
    end
    object quDocZSSelPRICEIN: TFloatField
      FieldName = 'PRICEIN'
    end
    object quDocZSSelPRICEIN0: TFloatField
      FieldName = 'PRICEIN0'
    end
    object quDocZSSelPRICEM: TFloatField
      FieldName = 'PRICEM'
    end
    object quDocZSSelSUMIN: TFloatField
      FieldName = 'SUMIN'
    end
    object quDocZSSelSUMIN0: TFloatField
      FieldName = 'SUMIN0'
    end
    object quDocZSSelSUMNDS: TFloatField
      FieldName = 'SUMNDS'
    end
    object quDocZSSelNDSPROC: TFloatField
      FieldName = 'NDSPROC'
    end
    object quDocZSSelREMN1: TFloatField
      FieldName = 'REMN1'
    end
    object quDocZSSelVREAL: TFloatField
      FieldName = 'VREAL'
    end
    object quDocZSSelREMNDAY: TFloatField
      FieldName = 'REMNDAY'
    end
    object quDocZSSelREMNMIN: TFloatField
      FieldName = 'REMNMIN'
    end
    object quDocZSSelREMNMAX: TFloatField
      FieldName = 'REMNMAX'
    end
    object quDocZSSelREMNSTRAH: TFloatField
      FieldName = 'REMNSTRAH'
    end
    object quDocZSSelREMN2: TFloatField
      FieldName = 'REMN2'
    end
    object quDocZSSelPKDOC: TFloatField
      FieldName = 'PKDOC'
    end
    object quDocZSSelPKCARD: TFloatField
      FieldName = 'PKCARD'
    end
    object quDocZSSelDAYSTOZ: TSmallintField
      FieldName = 'DAYSTOZ'
    end
    object quDocZSSelQUANTZ: TFloatField
      FieldName = 'QUANTZ'
    end
    object quDocZSSelQUANTZALR: TFloatField
      FieldName = 'QUANTZALR'
    end
    object quDocZSSelQUANTZITOG: TFloatField
      FieldName = 'QUANTZITOG'
    end
    object quDocZSSelQUANTZFACT: TFloatField
      FieldName = 'QUANTZFACT'
    end
    object quDocZSSelCLIQUANT: TFloatField
      FieldName = 'CLIQUANT'
    end
    object quDocZSSelCLIPRICE: TFloatField
      FieldName = 'CLIPRICE'
    end
    object quDocZSSelCLIPRICE0: TFloatField
      FieldName = 'CLIPRICE0'
    end
    object quDocZSSelCLIQUANTN: TFloatField
      FieldName = 'CLIQUANTN'
    end
    object quDocZSSelCLIPRICEN: TFloatField
      FieldName = 'CLIPRICEN'
    end
    object quDocZSSelCLIPRICE0N: TFloatField
      FieldName = 'CLIPRICE0N'
    end
    object quDocZSSelCLINNUM: TIntegerField
      FieldName = 'CLINNUM'
    end
    object quDocZSSelROUNDPACK: TIntegerField
      FieldName = 'ROUNDPACK'
    end
  end
  object msConnection: TADOConnection
    CommandTimeout = 1800
    ConnectionString = 'FILE NAME=C:\_CasherRn\BIN\ECr.udl'
    ConnectionTimeout = 1800
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 28
    Top = 9
  end
  object quDocZSNacl: TADOQuery
    Connection = msConnection
    CursorType = ctStatic
    CommandTimeout = 1800
    Parameters = <
      item
        Name = 'IDH'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM [DOCZSPECNACL] ds'
      'where ds.[IDH]=:IDH')
    Left = 112
    Top = 73
    object quDocZSNaclIDH: TIntegerField
      FieldName = 'IDH'
    end
    object quDocZSNaclIDS: TIntegerField
      FieldName = 'IDS'
    end
    object quDocZSNaclICODE: TIntegerField
      FieldName = 'ICODE'
    end
    object quDocZSNaclICARDTYPE: TIntegerField
      FieldName = 'ICARDTYPE'
    end
    object quDocZSNaclQUANT: TFloatField
      FieldName = 'QUANT'
    end
    object quDocZSNaclPRICE0: TFloatField
      FieldName = 'PRICE0'
    end
    object quDocZSNaclPRICE: TFloatField
      FieldName = 'PRICE'
    end
    object quDocZSNaclRNDS: TFloatField
      FieldName = 'RNDS'
    end
    object quDocZSNaclRSUM0: TFloatField
      FieldName = 'RSUM0'
    end
    object quDocZSNaclRSUM: TFloatField
      FieldName = 'RSUM'
    end
    object quDocZSNaclRSUMNDS: TFloatField
      FieldName = 'RSUMNDS'
    end
    object quDocZSNaclNAME: TStringField
      FieldName = 'NAME'
      Size = 200
    end
  end
end
