unit PCard;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, SpeedBar, ExtCtrls, Placemnt, cxImageComboBox;

type
  TfmPCard = class(TForm)
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    SpeedItem6: TSpeedItem;
    ViewPCards: TcxGridDBTableView;
    LevelPCards: TcxGridLevel;
    GridPCards: TcxGrid;
    FormPlacement1: TFormPlacement;
    ViewPCardsBARCODE: TcxGridDBColumn;
    ViewPCardsPLATTYPE: TcxGridDBColumn;
    ViewPCardsIACTIVE: TcxGridDBColumn;
    ViewPCardsCLINAME: TcxGridDBColumn;
    ViewPCardsNAME: TcxGridDBColumn;
    ViewPCardsDATEFROM: TcxGridDBColumn;
    ViewPCardsDATETO: TcxGridDBColumn;
    ViewPCardsCOMMENT: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SpeedItem2Click(Sender: TObject);
    procedure SpeedItem5Click(Sender: TObject);
    procedure SpeedItem6Click(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
    procedure SpeedItem7Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmPCard: TfmPCard;

implementation

uses Un1, DmRnDisc, AddPlCard;

{$R *.dfm}

procedure TfmPCard.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  GridPCards.Align:=AlClient;
  ViewPCards.RestoreFromIniFile(CurDir+GridIni);
end;

procedure TfmPCard.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewPCards.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmPCard.SpeedItem2Click(Sender: TObject);
begin
  close;
end;

procedure TfmPCard.SpeedItem5Click(Sender: TObject);
begin
  with dmCDisc do
  begin
    if not taPCard.Eof then
    begin
      trUpdate.StartTransaction;
      taPCard.Edit;
      taPCardIACTIVE.AsInteger:=1;
      taPCard.Post;
      trUpdate.Commit;
      taPCard.Refresh;
    end;
  end;
end;

procedure TfmPCard.SpeedItem6Click(Sender: TObject);
begin
  with dmCDisc do
  begin
    if not taPCard.Eof then
    begin
      trUpdate.StartTransaction;
      taPCard.Edit;
      taPCardIACTIVE.AsInteger:=0;
      taPCard.Post;
      trUpdate.Commit;
      taPCard.Refresh;
    end;
  end;
end;

procedure TfmPCard.SpeedItem1Click(Sender: TObject);
Var StrWk:String;
begin
  //���������� ��
  with dmCDisc do
  begin
    with fmAddPlCard do
    begin
      if taTypePl.Active=False then taTypePl.Active:=True;

      cxTextEdit1.Text:=taPCardCLINAME.AsString;
      cxTextEdit2.Text:=taPCardBARCODE.AsString;
      if taPCardIACTIVE.AsInteger=1 then CheckBox1.Checked:=True
      else CheckBox1.Checked:=False;

      cxLookupComboBox1.EditValue:=taPCardPLATTYPE.AsInteger;
      Caption:='���������� ��������� �����.';
    end;
    fmAddPlCard.ShowModal;
    if fmAddPlCard.ModalResult=mrOk then
    begin
      StrWk:=fmAddPlCard.cxTextEdit2.Text;
      if taPCard.Locate('BARCODE',StrWk,[]) then
      begin
        ShowMessage('��������� ����� � ����� ����� ��� ���� !!! ���������� ���������.');
      end else
      begin
        trUpdate.StartTransaction;
        taPCard.Append;
        taPCardBARCODE.AsString:=StrWk;
        taPCardPLATTYPE.AsInteger:=fmAddPlCard.cxLookupComboBox1.EditValue;
        if fmAddPlCard.CheckBox1.Checked=True then taPCardIACTIVE.AsInteger:=1
        else taPCardIACTIVE.AsInteger:=0;
        taPCardCLINAME.asstring:=fmAddPlCard.cxTextEdit1.Text;
        taPCard.Post;
        trUpdate.Commit;

        taPCard.Refresh;

      end;

    end;
  end;
end;

procedure TfmPCard.SpeedItem3Click(Sender: TObject);
Var StrWk:String;
    sBar:String;
begin
  with dmCDisc do
  begin
    with fmAddPlCard do
    begin
      if taTypePl.Active=False then taTypePl.Active:=True;

      sBar:=taPCardBARCODE.AsString;
      cxTextEdit1.Text:=taPCardCLINAME.AsString;
      cxTextEdit2.Text:=taPCardBARCODE.AsString;
      if taPCardIACTIVE.AsInteger=1 then CheckBox1.Checked:=True
      else CheckBox1.Checked:=False;

      cxLookupComboBox1.EditValue:=taPCardPLATTYPE.AsInteger;
      Caption:='�������������� ��������� �����.';
    end;
    fmAddPlCard.ShowModal;
    if fmAddPlCard.ModalResult=mrOk then
    begin
      StrWk:=fmAddPlCard.cxTextEdit2.Text;
      if StrWk<>sBar then
      begin
        if taPCard.Locate('BARCODE',StrWk,[]) then
        begin
          ShowMessage('��������� ����� � ����� ����� ��� ���� !!! ���������� ���������.');
          exit;
        end;
      end;

      if taPCard.Locate('BARCODE',sBar,[]) then
      begin
        trUpdate.StartTransaction;
        taPCard.Edit;
        taPCardBARCODE.AsString:=StrWk;
        taPCardPLATTYPE.AsInteger:=fmAddPlCard.cxLookupComboBox1.EditValue;
        if fmAddPlCard.CheckBox1.Checked=True then taPCardIACTIVE.AsInteger:=1
        else taPCardIACTIVE.AsInteger:=0;
        taPCardCLINAME.asstring:=fmAddPlCard.cxTextEdit1.Text;
        taPCard.Post;
        trUpdate.Commit;

        taPCard.Refresh;
      end;
    end;
  end;

end;

procedure TfmPCard.SpeedItem7Click(Sender: TObject);
begin
  //����� �� ����, ������



end;

end.
