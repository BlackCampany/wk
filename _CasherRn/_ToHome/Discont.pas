unit Discont;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxControls, cxContainer, cxEdit, cxTextEdit, ExtCtrls,
  dxfBackGround, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons;

type
  TfmDiscount = class(TForm)
    cxTextEdit1: TcxTextEdit;
    dxfBackGround1: TdxfBackGround;
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    procedure cxButton2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmDiscount: TfmDiscount;

implementation

{$R *.dfm}

procedure TfmDiscount.cxButton2Click(Sender: TObject);
begin
  close;
end;

procedure TfmDiscount.FormShow(Sender: TObject);
begin
  cxTextEdit1.SetFocus;
end;

end.
