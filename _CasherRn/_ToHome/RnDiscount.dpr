program RnDiscount;

uses
  Forms,
  MainDiscount in 'MainDiscount.pas' {fmMainDiscount},
  Un1 in 'Un1.pas',
  DmRnDisc in 'DmRnDisc.pas' {dmCDisc: TDataModule},
  Discount in 'Discount.pas' {fmRnDisc},
  AddCateg in 'AddCateg.pas' {fmAddCateg},
  DC in 'DC.pas' {fmAddDC},
  TypePl in 'TypePl.pas' {fmTypePl},
  AddTypePl in 'AddTypePl.pas' {fmAddTypePl},
  AddPlCard in 'AddPlCard.pas' {fmAddPlCard},
  PCard in 'PCard.pas' {fmPCard},
  Period in 'Period.pas' {fmPeriod},
  DiscDetail in 'DiscDetail.pas' {fmDiscDet},
  PCDetail in 'PCDetail.pas' {fmPCDet},
  RepPCCli in 'RepPCCli.pas' {fmPCCli},
  repDiscCli in 'repDiscCli.pas' {fmDiscCli},
  PerA_Disc in 'PerA_Discount\PerA_Disc.pas' {fmPerA_Disc};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfmMainDiscount, fmMainDiscount);
  Application.CreateForm(TdmCDisc, dmCDisc);
  Application.CreateForm(TfmTypePl, fmTypePl);
  Application.CreateForm(TfmAddTypePl, fmAddTypePl);
  Application.CreateForm(TfmAddPlCard, fmAddPlCard);
  Application.CreateForm(TfmPCard, fmPCard);
  Application.CreateForm(TfmDiscDet, fmDiscDet);
  Application.CreateForm(TfmPCDet, fmPCDet);
  Application.CreateForm(TfmPCCli, fmPCCli);
  Application.CreateForm(TfmDiscCli, fmDiscCli);
  Application.CreateForm(TfmPerA_disc, fmPerA_disc);
  Application.Run;
end.
