program CasherRn;

uses
  Windows,
  Forms,
  MainCashRn in 'MainCashRn.pas' {fmMainCashRn},
  uPre in 'uPre.pas' {fmPre},
  Un1 in 'Un1.pas',
  Dm in 'Dm.pas' {dmC: TDataModule},
  Spec in 'Spec.pas' {fmSpec},
  Menu in 'Menu.pas' {fmMenuClass},
  Calc in 'Calc.pas' {fmCalc},
  MessQ in 'MessQ.pas' {fmMess},
  CreateTab in 'CreateTab.pas' {fmCreateTab},
  MessDel in 'MessDel.pas' {fmMessDel},
  PosMove in 'PosMove.pas' {fmPosMove},
  Modif in 'Modif.pas' {fmModif},
  CashEnd in 'CashEnd.pas' {fmCash},
  UnCash in 'UnCash.pas',
  u2fdk in 'U2FDK.PAS',
  Attention in 'Attention.pas' {fmAttention},
  AbstractHandler in 'AbstractHandler.pas',
  fmDiscountShape in 'fmDiscountShape.pas' {fmDiscount_Shape},
  uDB1 in 'uDB1.pas' {dmC1: TDataModule},
  CredCards in 'CredCards.pas' {fmCredCards},
  BnSber in 'BnSber.pas' {fmSber},
  MessTxt in 'MessTxt.pas' {fmMessTxt},
  RaschotWarn in 'RaschotWarn.pas' {fmRaschotWarn},
  cash_WarnPaper in 'cash_WarnPaper.pas' {fmWarnPaper},
  CashInOut in 'CashInOut.pas' {fmCashInOut},
  ViewTab in 'ViewTab.pas' {fmViewTab},
  PCardList in 'PCardList.pas' {fmPCardList},
  SendMessage in 'SendMessage.pas' {fmSendMessage},
  PCardsCash in 'PCardsCash.pas' {fmPCardsRn},
  prdb in 'Discount\prdb.pas' {dmPC: TDataModule},
  StopListCash in 'StopListCash.pas' {fmStopListCash};

{$R *.res}

Const AppID='SoftUr';

Var Handle:THandle;

begin
  // ������� � ���������� ������ 1-�������� "����" � ����������
  // ������ AppID, ���������� ��� � ���� �������� ������������
  // � ���������, ��� �� �� ������ ��� ������ ������.

  Handle:=CreateFileMapping($FFFFFFFF,Nil,PAGE_READONLY,0,1,AppID);
  If GetLastError=ERROR_ALREADY_EXISTS then MessageBox(0,'������ ������ ����� ��������� ����������.',AppID,MB_OK or MB_ICONSTOP or MB_TOPMOST)
  else
  begin
    Application.Initialize;
    Application.CreateForm(TfmMainCashRn, fmMainCashRn);
  Application.CreateForm(TdmC, dmC);
  Application.CreateForm(TfmPre, fmPre);
  Application.CreateForm(TfmSpec, fmSpec);
  Application.CreateForm(TfmMenuClass, fmMenuClass);
  Application.CreateForm(TfmCalc, fmCalc);
  Application.CreateForm(TfmCash, fmCash);
  Application.CreateForm(TfmAttention, fmAttention);
  Application.CreateForm(TfmDiscount_Shape, fmDiscount_Shape);
  Application.CreateForm(TdmC1, dmC1);
  Application.CreateForm(TfmRaschotWarn, fmRaschotWarn);
  Application.CreateForm(TfmWarnPaper, fmWarnPaper);
  Application.CreateForm(TfmViewTab, fmViewTab);
  Application.CreateForm(TfmPCardList, fmPCardList);
  Application.CreateForm(TfmSber, fmSber);
  Application.CreateForm(TfmSendMessage, fmSendMessage);
  Application.CreateForm(TfmPCardsRn, fmPCardsRn);
  Application.CreateForm(TdmPC, dmPC);
  Application.CreateForm(TfmStopListCash, fmStopListCash);
  Application.Run;
  end;
  // ����������� ������ � ��� ����� ��������� ��������� ������.
  CloseHandle(Handle)
end.
