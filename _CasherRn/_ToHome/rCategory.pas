unit rCategory;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxCheckBox, cxImageComboBox, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxControls,
  cxGridCustomView, cxGrid, Placemnt, StdCtrls, cxButtons, ExtCtrls,
  SpeedBar;

type
  TfmRCategory = class(TForm)
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem5: TSpeedItem;
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    FormPlacement1: TFormPlacement;
    GridCateg: TcxGrid;
    ViewCateg: TcxGridDBTableView;
    LevelCateg: TcxGridLevel;
    ViewCategID: TcxGridDBColumn;
    ViewCategNAMECAT: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure SpeedItem4Click(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
    procedure SpeedItem5Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmRCategory: TfmRCategory;

implementation

uses Un1, dmOffice, AddTermoObr, AddClass;

{$R *.dfm}

procedure TfmRCategory.FormCreate(Sender: TObject);
begin
  GridCateg.Align:=AlClient;
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  ViewCateg.RestoreFromIniFile(CurDir+GridIni);
end;

procedure TfmRCategory.SpeedItem4Click(Sender: TObject);
begin
  Close;
end;

procedure TfmRCategory.SpeedItem1Click(Sender: TObject);
Var iMax:Integer;
begin
  //��������
  if not CanDo('prAddCateg') then begin ShowMessage('��� ����.'); exit; end;
  with dmO do
  begin
    fmAddClass.Caption:='���������� ���������.';
    fmAddClass.label4.Caption:='���������� ���������.';
    fmAddClass.cxTextEdit1.Text:='';

    fmAddClass.ShowModal;
    if fmAddClass.ModalResult=mrOk then
    begin
      with dmO do
      begin
        iMax:=1;
        if quCateg.RecordCount>0 then
        begin
          quCateg.Last;
          iMax:=quCategID.AsInteger+1;
        end;

        quCateg.Append;
        quCategID.AsInteger:=iMax;
        quCategNAMECAT.AsString:=fmAddClass.cxTextEdit1.Text;
        quCateg.Post;

        GridCateg.SetFocus;
      end;
    end;
  end;
end;

procedure TfmRCategory.SpeedItem2Click(Sender: TObject);
begin
  if not CanDo('prEditCateg') then begin ShowMessage('��� ����.'); exit; end;
  with dmO do
  begin
   if quCateg.RecordCount>0 then
    begin
      fmAddClass.Caption:='�������������� ���������.';
      fmAddClass.label4.Caption:='�������������� ���������.';
      fmAddClass.cxTextEdit1.Text:=quCategNAMECAT.AsString;

      fmAddClass.ShowModal;
      if fmAddClass.ModalResult=mrOk then
      begin
        with dmO do
        begin
          quCateg.Edit;
          quCategNAMECAT.AsString:=fmAddClass.cxTextEdit1.Text;
          quCateg.Post;

          GridCateg.SetFocus;
        end;
      end;
    end;
  end;
end;

procedure TfmRCategory.SpeedItem3Click(Sender: TObject);
begin
  //�������
  with dmO do
  begin
    if quCateg.RecordCount>0 then
    begin
      if MessageDlg('������� "'+quCategNAMECAT.AsString+'"', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        //��������� �� �������������

        quUseCateg.Active:=False;
        quUseCateg.SQLs.SelectSQL.Clear;
        quUseCateg.SQLs.SelectSQL.Add('SELECT first 3 ID');
        quUseCateg.SQLs.SelectSQL.Add('FROM OF_CARDS where RCATEGORY='+IntToStr(quCategID.AsInteger));
        quUseCateg.Active:=True;
        if quUseCateg.RecordCount=0 then quCateg.Delete;
        quUseCateg.Active:=False;
      end;
    end;
  end;
end;

procedure TfmRCategory.SpeedItem5Click(Sender: TObject);
begin
  prNExportExel5(ViewCateg);
end;

end.
