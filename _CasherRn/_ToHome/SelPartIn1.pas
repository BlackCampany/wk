unit SelPartIn1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons, ExtCtrls,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxImageComboBox;

type
  TfmPartIn1 = class(TForm)
    Panel1: TPanel;
    cxButton2: TcxButton;
    Panel2: TPanel;
    ViewPartIn: TcxGridDBTableView;
    LevelPartIn: TcxGridLevel;
    GrPartIn: TcxGrid;
    Label1: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    ViewPartInNum: TcxGridDBColumn;
    ViewPartInIdGoods: TcxGridDBColumn;
    ViewPartInNameG: TcxGridDBColumn;
    ViewPartInIM: TcxGridDBColumn;
    ViewPartInSM: TcxGridDBColumn;
    ViewPartInQuant: TcxGridDBColumn;
    ViewPartInPrice1: TcxGridDBColumn;
    ViewPartIniRes: TcxGridDBColumn;
    procedure cxButton2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ViewPartInDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmPartIn1: TfmPartIn1;

implementation

uses Un1, dmOffice, AddDoc2, DMOReps;

{$R *.dfm}

procedure TfmPartIn1.cxButton2Click(Sender: TObject);
begin
  close;
end;

procedure TfmPartIn1.FormCreate(Sender: TObject);
begin
  GrPartIn.Align:=AlClient;
end;

procedure TfmPartIn1.ViewPartInDblClick(Sender: TObject);
begin
  ModalResult:=mrOk;
end;

end.
