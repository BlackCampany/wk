unit CalcCal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridBandedTableView, cxGridDBBandedTableView,
  cxControls, cxGridCustomView, cxClasses, cxGridLevel, cxGrid, ComCtrls,
  dxmdaset, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons, cxContainer,
  cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalc;

type
  TfmCalcCal = class(TForm)
    taCal: TdxMemData;
    taCalNum: TSmallintField;
    taCalName: TStringField;
    taCalObr: TStringField;
    taCalMassa: TFloatField;
    taCalb1: TFloatField;
    taCalb2: TFloatField;
    taCalg1: TFloatField;
    taCalg2: TFloatField;
    taCalu1: TFloatField;
    taCalu2: TFloatField;
    dstaCal: TDataSource;
    StatusBar1: TStatusBar;
    LevelCal: TcxGridLevel;
    GrCal: TcxGrid;
    ViewCal: TcxGridDBBandedTableView;
    ViewCalNum: TcxGridDBBandedColumn;
    ViewCalName: TcxGridDBBandedColumn;
    ViewCalObr: TcxGridDBBandedColumn;
    ViewCalMassa: TcxGridDBBandedColumn;
    ViewCalb1: TcxGridDBBandedColumn;
    ViewCalb2: TcxGridDBBandedColumn;
    ViewCalg1: TcxGridDBBandedColumn;
    ViewCalg2: TcxGridDBBandedColumn;
    ViewCalu1: TcxGridDBBandedColumn;
    ViewCalu2: TcxGridDBBandedColumn;
    taCalkb: TFloatField;
    taCalim: TIntegerField;
    taCalsm: TStringField;
    ViewCalsm: TcxGridDBBandedColumn;
    Label1: TLabel;
    Label2: TLabel;
    cxCalcEdit1: TcxCalcEdit;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    taCalIdCard: TIntegerField;
    PopupMenu1: TPopupMenu;
    Excel1: TMenuItem;
    cxCalcEdit2: TcxCalcEdit;
    cxCalcEdit3: TcxCalcEdit;
    cxCalcEdit4: TcxCalcEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    taCalMassa1: TFloatField;
    procedure cxButton2Click(Sender: TObject);
    procedure Excel1Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmCalcCal: TfmCalcCal;

implementation

uses Un1, dmOffice;

{$R *.dfm}

procedure TfmCalcCal.cxButton2Click(Sender: TObject);
begin
  Close;
end;

procedure TfmCalcCal.Excel1Click(Sender: TObject);
begin
  prExportExelb(ViewCal,dstaCal,taCal);
end;

procedure TfmCalcCal.cxButton1Click(Sender: TObject);
begin
  with dmO do
  begin
    quUpd.SQL.Clear;
    quUpd.SQL.Add('Update OF_CARDS Set BB='+fts(RoundVal(cxCalcEdit2.EditValue))+',  GG='+fts(RoundVal(cxCalcEdit3.EditValue))+',  U1='+fts(RoundVal(cxCalcEdit4.EditValue))+',  EE='+fts(RoundVal(cxCalcEdit1.EditValue))+' where ID='+its(cxCalcEdit1.Tag));
    quUpd.ExecQuery;

    quCardsSel.FullRefresh;
  end;
end;

end.
