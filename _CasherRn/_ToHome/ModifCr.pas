unit ModifCr;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Placemnt, ComCtrls, SpeedBar, ExtCtrls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  RXSplit, XPStyleActnCtrls, ActnList, ActnMan, Menus;

type
  TfmModCr = class(TForm)
    FormPlacement1: TFormPlacement;
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    ModTree: TTreeView;
    RxSplitter1: TRxSplitter;
    ViewModCr: TcxGridDBTableView;
    LevelModCr: TcxGridLevel;
    GridModCr: TcxGrid;
    am3: TActionManager;
    acAddGrMo: TAction;
    acEditGrMo: TAction;
    acDelGrMo: TAction;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    SpeedItem6: TSpeedItem;
    SpeedItem7: TSpeedItem;
    Timer1: TTimer;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    acExit: TAction;
    ViewModCrNAME: TcxGridDBColumn;
    ViewModCrPRICE: TcxGridDBColumn;
    acAddMod: TAction;
    acEditMod: TAction;
    acDelMod: TAction;
    SpeedItem3: TSpeedItem;
    ViewModCrCODEB: TcxGridDBColumn;
    ViewModCrKB: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acAddGrMoExecute(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure acEditGrMoExecute(Sender: TObject);
    procedure acDelGrMoExecute(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure ModTreeChange(Sender: TObject; Node: TTreeNode);
    procedure acAddModExecute(Sender: TObject);
    procedure acEditModExecute(Sender: TObject);
    procedure acDelModExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    Procedure RefreshModTree;
    Procedure RefreshMods;
  end;


var
  fmModCr: TfmModCr;

implementation

uses Un1, dmRnEdit, AddCateg;

{$R *.dfm}

Procedure TfmModCr.RefreshMods;
begin
  with dmC do
  begin
    quMods.Active:=False;
    quMods.ParamByName('PARENTID').AsInteger:=Integer(ModTree.Selected.Data);
    quMods.Active:=True;
    quMods.First;
  end;
end;

Procedure TfmModCr.RefreshModTree;
Var TreeNode : TTreeNode;
begin
  ModTree.Items.Clear;
  with dmC do
  begin
    quModGrMo.Active:=False;
    quModGrMo.Active:=True;
    quModGrMo.First;
    while not quModGrMo.Eof do
    begin
      TreeNode:=ModTree.Items.AddChildObject(nil, quModGrMoNAME.AsString, Pointer(quModGrMoSIFR.AsInteger));
      TreeNode.ImageIndex:=8;
      TreeNode.SelectedIndex:=7;

      quModGrMo.Next;
    end;
    if ModTree.Items.Count>0 then
    begin
      ModTree.Items[0].Selected:=True;
      ModTree.Repaint;
      delay(10);
    end;
  end;
end;

procedure TfmModCr.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  GridModCr.Align:=AlClient;
  ViewModCr.RestoreFromIniFile(CurDir+GridIni);
//  dmC.taSpecAllSel.Active:=False;
//  ViewLog.StoreToIniFile(CurDir+GridIni,False);
  RefreshModTree;

  if ModTree.Items.Count>0 then RefreshMods;

{  with dmC do
  begin
    if not quModGrMo.Eof then
    begin
      quMods.Active:=False;
      quMods.ParamByName('PARENTID').AsInteger:=quModGrMoSIFR.AsInteger;
      quMods.Active:=True;

      quMods.First;
    end;
  end;}
end;

procedure TfmModCr.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewModCr.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmModCr.acAddGrMoExecute(Sender: TObject);
Var Id,i:Integer;
begin
  //���������� ������ ���
  if not CanDo('prAddMod') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  with dmC do
  begin
    Id:=0;
    fmAddCateg:=TFmAddCateg.create(Application);
    fmAddCateg.Caption:='���������� ������ �������������.';
    fmAddCateg.cxTextEdit1.Text:='';
    fmAddCateg.ShowModal;
    if fmAddCateg.ModalResult=mrOk then
    begin
      try
//        showmessage('�������� ������ �������������');

        quMaxIdGrMo.Active:=False;
        quMaxIdGrMo.Active:=True;
        Id:=quMaxIdGrMoMAXID.AsInteger+1;
        quMaxIdGrMo.Active:=False;

        trUpdate.StartTransaction;


        quModGrMo.Active:=False;
        quModGrMo.Active:=True;

        quModGrMo.Append;
        quModGrMoSIFR.AsInteger:=Id;
        quModGrMoNAME.AsString:=fmAddCateg.cxTextEdit1.Text;
        quModGrMoPARENT.AsInteger:=0;
        quModGrMoPRICE.AsFloat:=0;
        quModGrMoREALPRICE.AsFloat:=0;
        quModGrMoIACTIVE.AsInteger:=1;
        quModGrMoIEDIT.AsInteger:=0;
        quModGrMo.post;

        trUpdate.Commit;

        quMods.Active:=False;
        quMods.ParamByName('PARENTID').AsInteger:=Id;
        quMods.Active:=True;
        quMods.First;

      except
      end;
    end;
    fmAddCateg.Release;
    if Id>0 then //������ ��� �������� - ����� �� ���������
    begin
      bMenuListMo:=False;

      RefreshModTree;

      for i:=0 to ModTree.Items.Count-1 do
      if Integer(ModTree.Items.Item[i].Data) = Id then
      begin
        ModTree.Items[i].Selected:=True;
        ModTree.Repaint;
      end;

      bMenuListMo:=True;
    end;
  end;
end;

procedure TfmModCr.Timer1Timer(Sender: TObject);
begin
  if bClear2=True then begin StatusBar1.Panels[0].Text:=''; bClear2:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClear2:=True;
end;

procedure TfmModCr.acEditGrMoExecute(Sender: TObject);
Var Id,i:Integer;
begin
//������������� ������ ���
  if not CanDo('prEditMod') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  with dmC do
  begin
    Id:=0;
    fmAddCateg:=TFmAddCateg.create(Application);
    fmAddCateg.Caption:='�������������� ������ �������������.';
    fmAddCateg.cxTextEdit1.Text:=ModTree.Selected.Text;
    fmAddCateg.ShowModal;
    if fmAddCateg.ModalResult=mrOk then
    begin
      try
        Id:=Integer(ModTree.Selected.Data);

        quModGrMo.Active:=False;
        quModGrMo.Active:=True;

        if quModGrMo.Locate('SIFR',Id,[]) then
        begin
          trUpdate.StartTransaction;

          quModGrMo.Edit;
          quModGrMoNAME.AsString:=fmAddCateg.cxTextEdit1.Text;
          quModGrMo.post;

          trUpdate.Commit;

          quMods.Active:=False;
          quMods.ParamByName('PARENTID').AsInteger:=Id;
          quMods.Active:=True;
          quMods.First;


        end else Id:=0;

      except
      end;
    end;
    fmAddCateg.Release;
    if Id>0 then //������ ��� �������� - ����� �� ���������
    begin
      bMenuListMo:=False;

      RefreshModTree;

      for i:=0 to ModTree.Items.Count-1 do
      if Integer(ModTree.Items.Item[i].Data) = Id then
      begin
        ModTree.Items[i].Selected:=True;
        ModTree.Repaint;
      end;

      bMenuListMo:=True;
    end;
  end;
end;

procedure TfmModCr.acDelGrMoExecute(Sender: TObject);
begin
  //�������� ������ �������������
  if not CanDo('prDelMod') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  with dmC do
  begin
    if MessageDlg('�� ������������� ������ ������� ������ �������������: '+ModTree.Selected.Text,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      try

        quModGrMo.Active:=False;
        quModGrMo.Active:=True;

        if quModGrMo.Locate('SIFR',Integer(ModTree.Selected.Data),[]) then
        begin
          //��������� ��� ������������ �� ���� ������ � ��������� (PARENT=-1)
          trUpd1.StartTransaction;

          quDelMod.Active:=False;
          quDelMod.ParamByName('PARENTID').AsInteger:=Integer(ModTree.Selected.Data);
          quDelMod.Active:=True;

          trUpd1.Commit;


          trUpdate.StartTransaction;
          quModGrMo.Delete;
          trUpdate.Commit;
        end;
      except
      end;

      bMenuListMo:=False;
      RefreshModTree;

      if ModTree.Items.Count>0 then RefreshMods
      else
      begin
        with dmC do
        begin
          quMods.Active:=False;
          quMods.ParamByName('PARENTID').AsInteger:=-100;
          quMods.Active:=True;
          quMods.First;
        end;
      end;
      bMenuListMo:=True;

    end;
  end;
end;

procedure TfmModCr.acExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmModCr.ModTreeChange(Sender: TObject; Node: TTreeNode);
begin
  if bMenuListMo then
  begin
    RefreshMods;
  end;  
end;

procedure TfmModCr.acAddModExecute(Sender: TObject);
Var Id:Integer;
begin
  //���������� ������������
  if not CanDo('prAddMod') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  if ModTree.Items.Count=0 then
  begin
    showmessage('�������� ������� ������');
    exit;
  end;
  with dmC do
  begin
    fmAddCateg:=TFmAddCateg.create(Application);
    fmAddCateg.Caption:='���������� ������������.';
    fmAddCateg.cxTextEdit1.Text:='';
    fmAddCateg.Label1.Visible:=True;
    fmAddCateg.cxSpinEdit1.Visible:=True;
    fmAddCateg.GroupBox1.Visible:=True;
    fmAddCateg.cxCalcEdit1.EditValue:=0;
    fmAddCateg.cxCalcEdit3.EditValue:=0;

    fmAddCateg.ShowModal;
    if fmAddCateg.ModalResult=mrOk then
    begin
      try
//        showmessage('�������� ���������');
        quMaxIdGrMo.Active:=False;
        quMaxIdGrMo.Active:=True;
        Id:=quMaxIdGrMoMAXID.AsInteger+1;
        quMaxIdGrMo.Active:=False;

        trUpdate.StartTransaction;
        quMods.Append;
        quModsSIFR.AsInteger:=Id;
        quModsNAME.AsString:=fmAddCateg.cxTextEdit1.Text;
        quModsPARENT.AsInteger:=Integer(ModTree.Selected.Data);
        quModsPRICE.AsFloat:=fmAddCateg.cxSpinEdit1.Value;
        quModsREALPRICE.AsFloat:=0;
        quModsIACTIVE.AsInteger:=1;
        quModsIEDIT.AsInteger:=0;
        quModsCODEB.AsInteger:=Trunc(fmAddCateg.cxCalcEdit1.EditValue);
        quModsKB.AsFloat:=fmAddCateg.cxCalcEdit3.EditValue;
        quMods.post;
        trUpdate.Commit;
      except
      end;
    end;
    fmAddCateg.Release;
  end;
end;

procedure TfmModCr.acEditModExecute(Sender: TObject);
begin
  //�������������� ������������
  if not CanDo('prEditMod') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  if ModTree.Items.Count=0 then
  begin
    showmessage('�������� ������� ������');
    exit;
  end;
  with dmC do
  begin
    if not quMods.Eof then
    begin
      fmAddCateg:=TFmAddCateg.create(Application);
      fmAddCateg.Caption:='�������������� ������������.';
      fmAddCateg.cxTextEdit1.Text:=quModsNAME.AsString;
      fmAddCateg.Label1.Visible:=True;
      fmAddCateg.cxSpinEdit1.Visible:=True;
      fmAddCateg.cxSpinEdit1.Value:=Trunc(quModsPRICE.AsFloat);
      fmAddCateg.GroupBox1.Visible:=True;
      fmAddCateg.cxCalcEdit1.EditValue:=quModsCODEB.AsInteger;
      fmAddCateg.cxCalcEdit3.EditValue:=quModsKB.AsFloat;

      fmAddCateg.ShowModal;
      if fmAddCateg.ModalResult=mrOk then
      begin
        try
          trUpdate.StartTransaction;
          quMods.Edit;
          quModsNAME.AsString:=fmAddCateg.cxTextEdit1.Text;
          quModsPRICE.AsFloat:=fmAddCateg.cxSpinEdit1.Value;
          quModsCODEB.AsInteger:=fmAddCateg.cxCalcEdit1.EditValue;
          quModsKB.AsFloat:=fmAddCateg.cxCalcEdit3.EditValue;
          quMods.post;
          trUpdate.Commit;
          quMods.Refresh;
        except
        end;
      end;
    end;
    fmAddCateg.Release;
  end;
end;

procedure TfmModCr.acDelModExecute(Sender: TObject);
begin
  //��������
  if not CanDo('prDelMod') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  with dmC do
  begin
    if not quMods.Eof then
    begin
      if MessageDlg('�� ������������� ������ ������� �����������: '+quModsNAME.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        trUpdate.StartTransaction;
        quMods.Delete;
        trUpdate.Commit;
        quMods.Refresh;
      end;
    end;
  end;
end;

end.
