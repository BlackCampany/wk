unit UnDP2300;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls,StdCtrls;

Function OpenDllPD:Integer; stdcall; far; external 'usbpd.dll' name 'OpenUSBpd';
Function CloseDllPD:Integer; stdcall; far; external 'usbpd.dll' name 'CloseUSBpd';
Function WriteDllPD(StrP:PChar;iCount:INteger):Integer; stdcall; far; external 'usbpd.dll' name 'WritePD';

procedure WriteDP(Str1,Str2:String);

implementation

uses Un1, u2fdk;

procedure WriteDP(Str1,Str2:String);
Var iR:Integer;
    StrP:PChar;
begin
  if CommonSet.UseDP2300=0 then exit;
  try
    StrP:=StrAlloc(100);
    iR:=OpenDllPD;
    if iR=0 then
    begin
      StrP[0]:=#$1B;
      StrP[1]:=#$40;
      iR:=WriteDllPD(StrP,2); //�������������
    end;
    if iR=0 then //��������� ������� ��������
    begin
      StrP[0]:=#$1B;
      StrP[1]:=#$74;
      StrP[2]:=#$06;
      iR:=WriteDllPD(StrP,3);
    end;
    if iR=0 then //����� �����
    begin
      if Length(Str1)<20 then Str1:=Str1+' ';
      if Length(Str2)<20 then Str2:=Str2+' ';
      Str1:=Copy(Str1,1,20); //������� ��� � ����������� �����
      Str2:=Copy(Str2,1,20);

      StrP:=PChar(AnsiToOemConvert(Str1+Str2));

      iR:=WriteDllPD(StrP,40);
    end;
    if iR=0 then
    begin
      CloseDllPD;
    end;
  except
  end;
end;

end.
