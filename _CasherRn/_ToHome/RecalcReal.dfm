object fmRecalcPer1: TfmRecalcPer1
  Left = 353
  Top = 286
  BorderStyle = bsDialog
  Caption = #1055#1077#1088#1077#1087#1088#1086#1074#1077#1076#1077#1085#1080#1077' '#1056#1077#1072#1083#1080#1079#1072#1094#1080#1080' '#1085#1072' '#1089#1090#1086#1088#1086#1085#1091' '#1079#1072' '#1087#1077#1088#1080#1086#1076
  ClientHeight = 531
  ClientWidth = 368
  Color = 16728736
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 48
    Width = 61
    Height = 13
    Caption = #1047#1072' '#1087#1077#1088#1080#1086#1076' '#1089
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object Label2: TLabel
    Left = 48
    Top = 80
    Width = 12
    Height = 13
    Caption = #1087#1086
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object Label3: TLabel
    Left = 8
    Top = 16
    Width = 296
    Height = 13
    Caption = #1055#1077#1088#1077#1087#1088#1086#1074#1077#1089#1090#1080' '#1056#1077#1072#1083#1080#1079#1072#1094#1080#1102' '#1085#1072' '#1089#1090#1086#1088#1086#1085#1091' '#1079#1072' '#1087#1077#1088#1080#1086#1076
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label4: TLabel
    Left = 8
    Top = 112
    Width = 33
    Height = 13
    Caption = #1055#1086' '#1052#1061
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 512
    Width = 368
    Height = 19
    Panels = <
      item
        Width = 300
      end
      item
        Width = 50
      end>
  end
  object cxDateEdit1: TcxDateEdit
    Left = 80
    Top = 44
    Style.LookAndFeel.Kind = lfOffice11
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 1
    Width = 113
  end
  object cxButton1: TcxButton
    Left = 200
    Top = 42
    Width = 73
    Height = 25
    Caption = #1055#1091#1089#1082
    TabOrder = 2
    OnClick = cxButton1Click
    LookAndFeel.Kind = lfOffice11
  end
  object Memo1: TcxMemo
    Left = 8
    Top = 140
    Lines.Strings = (
      'Memo1')
    TabOrder = 3
    Height = 365
    Width = 353
  end
  object cxButton2: TcxButton
    Left = 280
    Top = 42
    Width = 73
    Height = 25
    Caption = #1042#1099#1093#1086#1076
    TabOrder = 4
    OnClick = cxButton2Click
    Glyph.Data = {
      5E040000424D5E04000000000000360000002800000012000000130000000100
      18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
      CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
      8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
      0000CED3D6848684848684848684848684848684848684848684848684848684
      848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
      7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
      FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
      00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
      75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
      0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
      FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
      CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
      007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
      00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
      75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
      0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
      FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
      CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
      00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
      494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
      0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
      00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
      CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
      D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
      D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
      0000}
    LookAndFeel.Kind = lfOffice11
  end
  object cxDateEdit2: TcxDateEdit
    Left = 80
    Top = 76
    Style.LookAndFeel.Kind = lfOffice11
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 5
    Width = 113
  end
  object cxLookupComboBox2: TcxLookupComboBox
    Left = 80
    Top = 108
    Properties.KeyFieldNames = 'ID'
    Properties.ListColumns = <
      item
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        FieldName = 'NAMEMH'
      end>
    Properties.ListOptions.AnsiSort = True
    Properties.ListSource = dmO.dsMHAll
    Style.BorderStyle = ebsOffice11
    Style.LookAndFeel.Kind = lfOffice11
    Style.PopupBorderStyle = epbsDefault
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 6
    Width = 129
  end
  object prDelPer1: TpFIBStoredProc
    Transaction = trD
    Database = dmO.OfficeRnDb
    SQL.Strings = (
      'EXECUTE PROCEDURE PR_DELPER1 (?IDATEB, ?IDATEE, ?ISKL)')
    StoredProcName = 'PR_DELPER1'
    Left = 48
    Top = 216
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object trS: TpFIBTransaction
    DefaultDatabase = dmO.OfficeRnDb
    TimeoutAction = TARollback
    Left = 48
    Top = 280
  end
  object trD: TpFIBTransaction
    DefaultDatabase = dmO.OfficeRnDb
    TimeoutAction = TARollback
    Left = 48
    Top = 336
  end
  object trU: TpFIBTransaction
    DefaultDatabase = dmO.OfficeRnDb
    TimeoutAction = TARollback
    Left = 48
    Top = 392
  end
  object prTestPart: TpFIBStoredProc
    Transaction = trD
    Database = dmO.OfficeRnDb
    SQL.Strings = (
      'EXECUTE PROCEDURE PR_RECALCPARTREMN ')
    StoredProcName = 'PR_RECALCPARTREMN'
    Left = 112
    Top = 216
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object prSetSpecActive: TpFIBStoredProc
    Transaction = trD
    Database = dmO.OfficeRnDb
    SQL.Strings = (
      
        'EXECUTE PROCEDURE PR_SETSPECACTIVE1 (?IDATEB, ?IDATEE, ?ATYPE, ?' +
        'ISKL)')
    StoredProcName = 'PR_SETSPECACTIVE1'
    Left = 112
    Top = 280
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object quDR: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE OF_DOCHEADOUTR'
      'SET '
      '    DATEDOC = :DATEDOC,'
      '    NUMDOC = :NUMDOC,'
      '    DATESF = :DATESF,'
      '    NUMSF = :NUMSF,'
      '    IDCLI = :IDCLI,'
      '    IDSKL = :IDSKL,'
      '    SUMIN = :SUMIN,'
      '    SUMUCH = :SUMUCH,'
      '    SUMTAR = :SUMTAR,'
      '    SUMNDS0 = :SUMNDS0,'
      '    SUMNDS1 = :SUMNDS1,'
      '    SUMNDS2 = :SUMNDS2,'
      '    PROCNAC = :PROCNAC,'
      '    IACTIVE = :IACTIVE'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    OF_DOCHEADOUTR'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO OF_DOCHEADOUTR('
      '    ID,'
      '    DATEDOC,'
      '    NUMDOC,'
      '    DATESF,'
      '    NUMSF,'
      '    IDCLI,'
      '    IDSKL,'
      '    SUMIN,'
      '    SUMUCH,'
      '    SUMTAR,'
      '    SUMNDS0,'
      '    SUMNDS1,'
      '    SUMNDS2,'
      '    PROCNAC,'
      '    IACTIVE'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATEDOC,'
      '    :NUMDOC,'
      '    :DATESF,'
      '    :NUMSF,'
      '    :IDCLI,'
      '    :IDSKL,'
      '    :SUMIN,'
      '    :SUMUCH,'
      '    :SUMTAR,'
      '    :SUMNDS0,'
      '    :SUMNDS1,'
      '    :SUMNDS2,'
      '    :PROCNAC,'
      '    :IACTIVE'
      ')')
    RefreshSQL.Strings = (
      'SELECT ID,DATEDOC,NUMDOC,DATESF,NUMSF,IDCLI,IDSKL,SUMIN,SUMUCH,'
      '    SUMTAR,SUMNDS0,SUMNDS1,SUMNDS2,PROCNAC,IACTIVE'
      'FROM'
      '    OF_DOCHEADOUTR '
      'where(  DATEDOC=:DDATE and IACTIVE>0'
      '     ) and (     OF_DOCHEADOUTR.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT ID,DATEDOC,NUMDOC,DATESF,NUMSF,IDCLI,IDSKL,SUMIN,SUMUCH,'
      '    SUMTAR,SUMNDS0,SUMNDS1,SUMNDS2,PROCNAC,IACTIVE'
      'FROM'
      '    OF_DOCHEADOUTR '
      'where DATEDOC=:DDATE and IACTIVE>0'
      'and IDSKL=:ISKL'
      'order by SUMUCH desc'
      '')
    Transaction = trS
    Database = dmO.OfficeRnDb
    UpdateTransaction = trU
    AutoCommit = True
    Left = 200
    Top = 232
    object quDRID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quDRDATEDOC: TFIBDateField
      FieldName = 'DATEDOC'
    end
    object quDRNUMDOC: TFIBStringField
      FieldName = 'NUMDOC'
      Size = 15
      EmptyStrToNull = True
    end
    object quDRDATESF: TFIBDateField
      FieldName = 'DATESF'
    end
    object quDRNUMSF: TFIBStringField
      FieldName = 'NUMSF'
      Size = 15
      EmptyStrToNull = True
    end
    object quDRIDCLI: TFIBIntegerField
      FieldName = 'IDCLI'
    end
    object quDRIDSKL: TFIBIntegerField
      FieldName = 'IDSKL'
    end
    object quDRSUMIN: TFIBFloatField
      FieldName = 'SUMIN'
    end
    object quDRSUMUCH: TFIBFloatField
      FieldName = 'SUMUCH'
    end
    object quDRSUMTAR: TFIBFloatField
      FieldName = 'SUMTAR'
    end
    object quDRSUMNDS0: TFIBFloatField
      FieldName = 'SUMNDS0'
    end
    object quDRSUMNDS1: TFIBFloatField
      FieldName = 'SUMNDS1'
    end
    object quDRSUMNDS2: TFIBFloatField
      FieldName = 'SUMNDS2'
    end
    object quDRPROCNAC: TFIBFloatField
      FieldName = 'PROCNAC'
    end
    object quDRIACTIVE: TFIBIntegerField
      FieldName = 'IACTIVE'
    end
  end
  object prRECALCGDS: TpFIBStoredProc
    Transaction = trD
    Database = dmO.OfficeRnDb
    SQL.Strings = (
      'EXECUTE PROCEDURE PR_RECALCGDSMOVE (?DDATE, ?IDATE)')
    StoredProcName = 'PR_RECALCGDSMOVE'
    Left = 112
    Top = 344
    qoAutoCommit = True
    qoStartTransaction = True
  end
end
