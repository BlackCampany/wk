unit InputMess;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, Menus, cxLookAndFeelPainters, StdCtrls,
  cxButtons, cxImageComboBox;

type
  TfmInputMess = class(TForm)
    Panel1: TPanel;
    ViewInpMess: TcxGridDBTableView;
    LevelInpMess: TcxGridLevel;
    GrInpMess: TcxGrid;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    ViewInpMessDATEDOC: TcxGridDBColumn;
    ViewInpMessNUMDOC: TcxGridDBColumn;
    ViewInpMessSUMIN: TcxGridDBColumn;
    ViewInpMessIACTIVE: TcxGridDBColumn;
    ViewInpMessOPER: TcxGridDBColumn;
    ViewInpMessCOMMENT: TcxGridDBColumn;
    ViewInpMessNAMEMH: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmInputMess: TfmInputMess;

implementation

uses DMOReps, dmOffice;

{$R *.dfm}

procedure TfmInputMess.FormCreate(Sender: TObject);
begin
  GrInpMess.Align:=AlClient;
end;

procedure TfmInputMess.cxButton1Click(Sender: TObject);
begin
  GrInpMess.Tag:=1;
end;

procedure TfmInputMess.cxButton2Click(Sender: TObject);
begin
  GrInpMess.Tag:=2;
end;

procedure TfmInputMess.cxButton3Click(Sender: TObject);
begin
  GrInpMess.Tag:=3;
end;

end.
