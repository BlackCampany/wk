object fmInputMess: TfmInputMess
  Left = 262
  Top = 470
  Width = 721
  Height = 262
  Caption = #1042#1085#1080#1084#1072#1085#1080#1077'! '#1044#1086#1082#1091#1084#1077#1085#1090#1099' '#1074' '#1089#1080#1089#1090#1077#1084#1077' '#1091#1078#1077' '#1077#1089#1090#1100'.'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 556
    Top = 0
    Width = 157
    Height = 228
    Align = alRight
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 20
      Top = 12
      Width = 121
      Height = 25
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ModalResult = 1
      TabOrder = 0
      OnClick = cxButton1Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 20
      Top = 44
      Width = 121
      Height = 25
      Caption = #1055#1088#1086#1087#1091#1089#1090#1080#1090#1100
      ModalResult = 1
      TabOrder = 1
      OnClick = cxButton2Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton3: TcxButton
      Left = 20
      Top = 108
      Width = 121
      Height = 25
      Caption = #1054#1089#1090#1072#1085#1086#1074#1080#1090#1100
      ModalResult = 1
      TabOrder = 2
      OnClick = cxButton3Click
      LookAndFeel.Kind = lfOffice11
    end
  end
  object GrInpMess: TcxGrid
    Left = 12
    Top = 4
    Width = 537
    Height = 217
    TabOrder = 1
    LookAndFeel.Kind = lfOffice11
    object ViewInpMess: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dmORep.dsquTestInput
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      object ViewInpMessDATEDOC: TcxGridDBColumn
        Caption = #1044#1072#1090#1072
        DataBinding.FieldName = 'DATEDOC'
        Width = 61
      end
      object ViewInpMessNUMDOC: TcxGridDBColumn
        Caption = #1053#1086#1084#1077#1088
        DataBinding.FieldName = 'NUMDOC'
        Width = 63
      end
      object ViewInpMessSUMIN: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072
        DataBinding.FieldName = 'SUMIN'
        Width = 62
      end
      object ViewInpMessIACTIVE: TcxGridDBColumn
        Caption = #1057#1090#1072#1090#1091#1089
        DataBinding.FieldName = 'IACTIVE'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Images = dmO.imState
        Properties.Items = <
          item
            Description = #1054#1087#1088#1080#1093#1086#1076#1086#1074#1072#1085
            ImageIndex = 0
            Value = 1
          end
          item
            Description = #1074' '#1088#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1085#1080#1080
            Value = 0
          end>
        Width = 50
      end
      object ViewInpMessNAMEMH: TcxGridDBColumn
        Caption = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103
        DataBinding.FieldName = 'NAMEMH'
        Width = 116
      end
      object ViewInpMessOPER: TcxGridDBColumn
        Caption = #1054#1087#1077#1088#1072#1094#1080#1103
        DataBinding.FieldName = 'OPER'
        Width = 56
      end
      object ViewInpMessCOMMENT: TcxGridDBColumn
        Caption = #1050#1086#1084#1084#1077#1085#1090#1072#1088#1080#1081
        DataBinding.FieldName = 'COMMENT'
        Width = 105
      end
    end
    object LevelInpMess: TcxGridLevel
      GridView = ViewInpMess
    end
  end
end
