object fmAddOper: TfmAddOper
  Left = 303
  Top = 200
  Width = 341
  Height = 342
  Caption = #1054#1087#1077#1088#1072#1094#1080#1080
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 24
    Top = 16
    Width = 71
    Height = 13
    Caption = #1040#1073#1073#1088#1077#1074#1080#1072#1090#1091#1088#1072
    Transparent = True
  end
  object Label2: TLabel
    Left = 24
    Top = 64
    Width = 51
    Height = 13
    Caption = #1044#1086#1082#1091#1084#1077#1085#1090
    Transparent = True
  end
  object Label3: TLabel
    Left = 24
    Top = 112
    Width = 50
    Height = 13
    Caption = #1053#1072#1079#1074#1072#1085#1080#1077
    Transparent = True
  end
  object Label4: TLabel
    Left = 24
    Top = 168
    Width = 149
    Height = 13
    Caption = #1044#1083#1103' '#1088#1072#1089#1093#1086#1076#1072' ('#1089#1087#1080#1089#1072#1085#1080#1103'):'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label5: TLabel
    Left = 40
    Top = 200
    Width = 53
    Height = 13
    Caption = #1050#1072#1090#1077#1075#1086#1088#1080#1103
    Transparent = True
  end
  object Label6: TLabel
    Left = 40
    Top = 224
    Width = 70
    Height = 13
    Caption = #1058#1080#1087' '#1089#1087#1080#1089#1072#1085#1080#1103
    Transparent = True
  end
  object Panel1: TPanel
    Left = 0
    Top = 251
    Width = 333
    Height = 57
    Align = alBottom
    BevelInner = bvLowered
    Color = 12910532
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 96
      Top = 16
      Width = 89
      Height = 25
      Caption = 'Ok'
      Default = True
      TabOrder = 0
      OnClick = cxButton1Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 216
      Top = 16
      Width = 89
      Height = 25
      Caption = #1042#1099#1093#1086#1076
      ModalResult = 2
      TabOrder = 1
      OnClick = cxButton2Click
      Glyph.Data = {
        5E040000424D5E04000000000000360000002800000012000000130000000100
        18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
        8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
        0000CED3D6848684848684848684848684848684848684848684848684848684
        848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
        7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
        FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
        FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
        007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
        FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
        00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
        494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
        00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000}
      LookAndFeel.Kind = lfOffice11
    end
  end
  object cxTextEdit1: TcxTextEdit
    Left = 24
    Top = 32
    Properties.MaxLength = 100
    Style.Shadow = True
    TabOrder = 1
    Text = 'cxTextEdit1'
    Width = 273
  end
  object cxTextEdit2: TcxTextEdit
    Left = 24
    Top = 128
    Properties.MaxLength = 100
    Style.Shadow = True
    TabOrder = 2
    Text = 'cxTextEdit2'
    Width = 273
  end
  object cxLookupComboBox1: TcxLookupComboBox
    Left = 24
    Top = 80
    Properties.KeyFieldNames = 'ID'
    Properties.ListColumns = <
      item
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        FieldName = 'NAMEDOC'
      end>
    Properties.ListSource = dmORep.dstaDocType
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 3
    Width = 273
  end
  object cxImageComboBox1: TcxImageComboBox
    Left = 120
    Top = 196
    Properties.Items = <
      item
        Description = #1055#1088#1086#1076#1072#1078#1072' '#1082#1072#1089#1089#1072
        ImageIndex = 0
        Value = 1
      end
      item
        Description = #1057#1087#1080#1089#1072#1085#1080#1077
        ImageIndex = 0
        Value = 2
      end
      item
        Description = #1062#1077#1093'.'#1087#1080#1090#1072#1085#1080#1077
        ImageIndex = 0
        Value = 3
      end
      item
        Description = #1055#1088#1086#1095#1077#1077
        ImageIndex = 0
        Value = 4
      end
      item
        Description = #1056#1077#1072#1083'. '#1085#1072' '#1089#1090#1086#1088#1086#1085#1091
        ImageIndex = 0
        Value = 5
      end>
    Style.Shadow = True
    TabOrder = 4
    Width = 177
  end
  object cxImageComboBox2: TcxImageComboBox
    Left = 120
    Top = 220
    Properties.Items = <
      item
        Description = #1056#1072#1089#1082#1083#1072#1076#1099#1074#1072#1090#1100' '#1085#1072' '#1089#1086#1089#1090#1072#1074#1083'.'
        ImageIndex = 0
        Value = 1
      end
      item
        Description = #1053#1077' '#1088#1072#1089#1082#1083#1072#1076#1099#1074#1072#1090#1100' '#1085#1072' '#1089#1086#1089#1090#1072#1074#1083'.'
        ImageIndex = 0
        Value = 2
      end>
    Style.Shadow = True
    TabOrder = 5
    Width = 177
  end
  object dxfBackGround1: TdxfBackGround
    BkColor.BeginColor = 14024661
    BkColor.EndColor = 9699219
    BkColor.FillStyle = fsVert
    BkAnimate.Speed = 700
    Left = 256
  end
end
