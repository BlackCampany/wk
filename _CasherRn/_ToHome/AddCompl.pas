unit AddCompl;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, cxGraphics, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxCurrencyEdit, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxControls, cxGridCustomView, cxGrid,
  cxLabel, ExtCtrls, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox, cxMaskEdit, cxCalendar, cxContainer, cxTextEdit,
  StdCtrls, ComCtrls, cxButtons, Placemnt, DBClient, cxButtonEdit,
  ActnList, XPStyleActnCtrls, ActnMan, cxSpinEdit, cxImageComboBox, cxMemo,
  cxGroupBox, cxRadioGroup, FR_DSet, FR_DBSet, FR_Class,
  pFIBDataSet, FIBDatabase, pFIBDatabase, cxCheckBox, dxmdaset;

type
  TfmAddCompl = class(TForm)
    Panel2: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Label1: TLabel;
    Label5: TLabel;
    Label12: TLabel;
    cxTextEdit1: TcxTextEdit;
    cxDateEdit1: TcxDateEdit;
    cxLookupComboBox1: TcxLookupComboBox;
    Panel3: TPanel;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    FormPlacement1: TFormPlacement;
    taSpec2: TClientDataSet;
    dsSpec: TDataSource;
    taSpec2Num: TIntegerField;
    taSpec2IdGoods: TIntegerField;
    taSpec2NameG: TStringField;
    taSpec2IM: TIntegerField;
    taSpec2SM: TStringField;
    taSpec2PriceIn: TFloatField;
    taSpec2SumIn: TFloatField;
    taSpec2PriceUch: TFloatField;
    taSpec2SumUch: TFloatField;
    cxLabel7: TcxLabel;
    cxLabel8: TcxLabel;
    cxLabel9: TcxLabel;
    amCom: TActionManager;
    acAddPos: TAction;
    taSpec2Km: TFloatField;
    Label2: TLabel;
    taSpec2TCard: TIntegerField;
    Panel5: TPanel;
    Panel4: TPanel;
    Memo1: TcxMemo;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    GridCom: TcxGrid;
    ViewCom: TcxGridDBTableView;
    ViewComNum: TcxGridDBColumn;
    ViewComIdGoods: TcxGridDBColumn;
    ViewComNameG: TcxGridDBColumn;
    ViewComTCard: TcxGridDBColumn;
    ViewComIM: TcxGridDBColumn;
    ViewComSM: TcxGridDBColumn;
    ViewComQuantFact: TcxGridDBColumn;
    ViewComPriceIn: TcxGridDBColumn;
    ViewComSumIn: TcxGridDBColumn;
    ViewComPriceUch: TcxGridDBColumn;
    ViewComSumUch: TcxGridDBColumn;
    LevelCom: TcxGridLevel;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    GridComBC: TcxGrid;
    ViewComBC: TcxGridDBTableView;
    ViewComBCID: TcxGridDBColumn;
    ViewComBCCODEB: TcxGridDBColumn;
    ViewComBCNAMEB: TcxGridDBColumn;
    ViewComBCQUANT: TcxGridDBColumn;
    ViewComBCIDCARD: TcxGridDBColumn;
    ViewComBCNAMEC: TcxGridDBColumn;
    ViewComBCSB: TcxGridDBColumn;
    ViewComBCQUANTC: TcxGridDBColumn;
    ViewComBCPRICEIN: TcxGridDBColumn;
    ViewComBCSUMIN: TcxGridDBColumn;
    ViewComBCIM: TcxGridDBColumn;
    ViewComBCSM: TcxGridDBColumn;
    LevelComBC: TcxGridLevel;
    acSaveInv: TAction;
    cxButton3: TcxButton;
    taSpecC2: TClientDataSet;
    taSpecC2Num: TIntegerField;
    taSpecC2IdGoods: TIntegerField;
    taSpecC2NameG: TStringField;
    taSpecC2IM: TIntegerField;
    taSpecC2SM: TStringField;
    taSpecC2Quant: TFloatField;
    taSpecC2PriceIn: TFloatField;
    taSpecC2SumIn: TFloatField;
    taSpecC2PriceUch: TFloatField;
    taSpecC2SumUch: TFloatField;
    taSpecC2QuantFact: TFloatField;
    taSpecC2PriceInF: TFloatField;
    taSpecC2SumInF: TFloatField;
    taSpecC2PriceUchF: TFloatField;
    taSpecC2SumUchF: TFloatField;
    taSpecC2QuantDif: TFloatField;
    taSpecC2SumInDif: TFloatField;
    taSpecC2SumUchDif: TFloatField;
    taSpecC2Km: TFloatField;
    taSpecC2TCard: TIntegerField;
    taSpecC2Id_Group: TIntegerField;
    taSpecC2NameGr: TStringField;
    dsSpecC: TDataSource;
    GridComC: TcxGrid;
    ViewComC: TcxGridDBTableView;
    LevelComC: TcxGridLevel;
    RepComplSpec: TfrReport;
    frdsSpec: TfrDBDataSet;
    acAddList: TAction;
    acDelPos: TAction;
    acDelAll: TAction;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    ViewComCNum: TcxGridDBColumn;
    ViewComCIdGoods: TcxGridDBColumn;
    ViewComCIM: TcxGridDBColumn;
    ViewComCSM: TcxGridDBColumn;
    ViewComCPriceIn: TcxGridDBColumn;
    ViewComCSumIn: TcxGridDBColumn;
    ViewComCQuantFact: TcxGridDBColumn;
    ViewComCPriceUch: TcxGridDBColumn;
    ViewComCSumUch: TcxGridDBColumn;
    frdsSpecC: TfrDBDataSet;
    taSpec2QuantFact: TFloatField;
    ViewComCNameG: TcxGridDBColumn;
    cxRadioButton1: TcxRadioButton;
    frRepCompl: TfrReport;
    frdsCalcB: TfrDBDataSet;
    cxRadioButton2: TcxRadioButton;
    acAddRealis: TAction;
    acEquialReal: TAction;
    acPartOutTest: TAction;
    N3: TMenuItem;
    Excel01: TMenuItem;
    PopupMenu2: TPopupMenu;
    MenuItem1: TMenuItem;
    PopupMenu3: TPopupMenu;
    MenuItem2: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    acExit: TAction;
    Label3: TLabel;
    cxLookupComboBox2: TcxLookupComboBox;
    cxCheckBox1: TcxCheckBox;
    taSpec: TdxMemData;
    taSpecNum: TIntegerField;
    taSpecIdGoods: TIntegerField;
    taSpecNameG: TStringField;
    taSpecIM: TIntegerField;
    taSpecSM: TStringField;
    taSpecPriceIn: TFloatField;
    taSpecSumIn: TFloatField;
    taSpecPriceUch: TFloatField;
    taSpecSumUch: TFloatField;
    taSpecKm: TFloatField;
    taSpecTCard: TIntegerField;
    taSpecQuantFact: TFloatField;
    taSpecC: TdxMemData;
    taSpecCNum: TIntegerField;
    taSpecCIdGoods: TIntegerField;
    taSpecCNameG: TStringField;
    taSpecCIM: TIntegerField;
    taSpecCSM: TStringField;
    taSpecCQuant: TFloatField;
    taSpecCPriceIn: TFloatField;
    taSpecCSumIn: TFloatField;
    taSpecCPriceUch: TFloatField;
    taSpecCSumUch: TFloatField;
    taSpecCQuantFact: TFloatField;
    taSpecCPriceInF: TFloatField;
    taSpecCSumInF: TFloatField;
    taSpecCPriceUchF: TFloatField;
    taSpecCSumUchF: TFloatField;
    taSpecCQuantDif: TFloatField;
    taSpecCSumInDif: TFloatField;
    taSpecCSumUchDif: TFloatField;
    taSpecCKm: TFloatField;
    taSpecCTCard: TIntegerField;
    taSpecCId_Group: TIntegerField;
    taSpecPriceIn0: TFloatField;
    taSpecSumIn0: TFloatField;
    ViewComPriceIn0: TcxGridDBColumn;
    ViewComSumIn0: TcxGridDBColumn;
    taSpecCPriceIn0: TFloatField;
    taSpecCSumIn0: TFloatField;
    ViewComCPriceIn0: TcxGridDBColumn;
    ViewComCSumIn0: TcxGridDBColumn;
    ViewComBCPRICEIN0: TcxGridDBColumn;
    ViewComBCSUMIN0: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acAddPosExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxLabel1Click(Sender: TObject);
    procedure ViewComEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure ViewComEditKeyPress(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
    procedure cxLabel7Click(Sender: TObject);
    procedure ViewComDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure ViewComDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure cxLabel2Click(Sender: TObject);
    procedure cxLabel9Click(Sender: TObject);
    procedure acSaveInvExecute(Sender: TObject);
    procedure ViewComEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure acAddListExecute(Sender: TObject);
    procedure acDelPosExecute(Sender: TObject);
    procedure acDelAllExecute(Sender: TObject);
    procedure Memo1DblClick(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxLookupComboBox1PropertiesChange(Sender: TObject);
    procedure taSpecQuantChange(Sender: TField);
    procedure taSpec2PriceInChange(Sender: TField);
    procedure taSpec2SumInChange(Sender: TField);
    procedure taSpec2PriceUchChange(Sender: TField);
    procedure taSpec2SumUchChange(Sender: TField);
    procedure cxButton3Click(Sender: TObject);
    procedure acAddRealisExecute(Sender: TObject);
    procedure cxLabel8Click(Sender: TObject);
    procedure acEquialRealExecute(Sender: TObject);
    procedure acPartOutTestExecute(Sender: TObject);
    procedure Excel01Click(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure MenuItem2Click(Sender: TObject);
    procedure N5Click(Sender: TObject);
    procedure N6Click(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure prCalcC(iDate,IdSkl:INteger);
    procedure prCalcPr(IdSkl:INteger);
    procedure prSave(IDH:INteger;Var Sum0,Sum1,Sum11:Real);
  end;

procedure prCalcSebSpec(rQs:Real;IdSkl,IdCli:Integer;taSpecC:TdxMemData);

//����� �� te ����Table ������ prCalcBlInv
Procedure prCalcBlCompl(sBeg,sName:String;IdPos,iCode,iDate,iTCard,IdSkl:Integer;rQ:Real;IdM:INteger;taCalcB,taSpecC,taSpec:TdxMemData;Memo1:TcxMemo;iT:Byte);

var
  fmAddCompl: TfmAddCompl;
  bAdd:Boolean = False;
  iCol:INteger;
//  Qr,Qf,Pr1,Pr11,Pr2,Pr22,Sum1,Sum11,Sum2,Sum21:Real;

implementation

uses Un1, dmOffice, FCards, Goods, DMOReps, DocInv, Message, AddInv,
  DocCompl, MainRnOffice, Clients;

{$R *.dfm}

Procedure prCalcBlCompl(sBeg,sName:String;IdPos,iCode,iDate,iTCard,IdSkl:Integer;rQ:Real;IdM:INteger;taCalcB,taSpecC,taSpec:TdxMemData;Memo1:TcxMemo;iT:Byte);
Var iTC,iPCount:INteger;
    QT1:TpFIBDataSet;
    rQ1,kBrutto,kM,MassaB:Real;
    iMain,iMax:INteger;
    sM:String;
    Par:Variant;
    rQb:Real;
//    NameGr:String;
//    Id_Group:INteger;
    Qr,Qf,Pr1,Pr11,Pr2,Pr22,Sum1,Sum11,Sum2,Sum21:Real;
    bCalc:Boolean;

  Procedure VarsToZero;
  begin
    Qr:=0;Qf:=0;Pr1:=0;Pr11:=0;Pr2:=0;Pr22:=0;Sum1:=0;Sum11:=0;Sum2:=0;Sum21:=0;
  end;

  Procedure VarsToSpec;
  begin
    Qr:=taSpecC.FieldbyName('Quant').AsFloat;
    Qf:=taSpecC.FieldbyName('QuantFact').AsFloat;
    Pr1:=taSpecC.FieldbyName('PriceIn').AsFloat;
    Pr11:=taSpecC.FieldbyName('PriceUch').AsFloat;
    Pr2:=taSpecC.FieldbyName('PriceInF').AsFloat;
    Pr22:=taSpecC.FieldbyName('PriceUchF').AsFloat;
    Sum1:=taSpecC.FieldbyName('SumIn').AsFloat;
    Sum11:=taSpecC.FieldbyName('SumUch').AsFloat;
    Sum2:=taSpecC.FieldbyName('SumInF').AsFloat;
    Sum21:=taSpecC.FieldbyName('SumUchF').AsFloat;
  end;

begin
  sBeg:=sBeg+'    ';

  with dmO do
  begin
    //���
    if iTCard=1 then bCalc:=True else bCalc:=False;
    if fCalcBlasCard(IdSkl,iCode) then bCalc:=False; //���� �� ������������ �� ��� �����

    if bCalc then
    begin
      iTC:=0; iPCount:=0; MassaB:=1;
      quFindTCard.Active:=False;
      quFindTCard.ParamByName('IDCARD').AsInteger:=iCode;
      quFindTCard.ParamByName('IDATE').AsDateTime:=iDate;
      quFindTCard.Active:=True;
      if quFindTCard.RecordCount>0 then
      begin
        iTC:=quFindTCardID.AsInteger;
        iPCount:=quFindTCardPCOUNT.AsInteger;
        MassaB:=quFindTCardPVES.AsFloat/1000;
        if prFindMT(IdM)=1 then MassaB:=1; // ���� ���� ����� �������
        prFindSM(IdM,sM,iMain); //��� �������� �������� ������� ���������
        prWH(sBeg+sName+' ('+INtToStr(iCode)+'). �� ����. ���-��: '+FloatToStr(RoundEx(rQ*1000)/1000)+' '+sM,Memo1);
      end else
      begin
        inc(iErr);
        sErr:=sErr+IntToStr(iCode)+' '+sName+';';
        prWH(SBeg+'������: �� �� �������.( ���-'+INtToStr(iCode)+')',Memo1);
      end;
      quFindTCard.Active:=False;
      if iTC>0 then
      begin
        QT1:=TpFIBDataSet.Create(Owner);
        QT1.Active:=False;
        QT1.Database:=OfficeRnDb;
        QT1.Transaction:=trSel;
        QT1.SelectSQL.Clear;
        QT1.SelectSQL.Add('SELECT CS.ID,CS.IDCARD,CS.CURMESSURE,CS.NETTO,CS.BRUTTO,CS.KNB,');
        QT1.SelectSQL.Add('CD.NAME,CD.TCARD');
        QT1.SelectSQL.Add('FROM OF_CARDSTSPEC CS');
        QT1.SelectSQL.Add('left join of_cards cd on cd.ID=CS.IDCARD');
        QT1.SelectSQL.Add('where IDC='+IntToStr(iCode)+' and IDT='+IntToStr(iTC));
        QT1.SelectSQL.Add('ORDER BY CS.ID');
        QT1.Active:=True;

        QT1.First;
        while not QT1.Eof do
        begin
          rQ1:=QT1.FieldByName('NETTO').AsFloat;
          kM:=prFindKM(QT1.FieldByName('CURMESSURE').AsInteger);
          rQ1:=rQ1*kM;//��������� � �������� �������
          prFindSM(QT1.FieldByName('CURMESSURE').AsInteger,sM,iMain); //��� �������� �������� ������� ���������
          //���� ������ �� ������ ����
          kBrutto:=prFindBrutto(QT1.FieldByName('IDCARD').AsInteger,iDate);
          rQ1:=rQ1*(100+kBrutto)/100; //��� ����� � ������
          rQ1:=rQ1/iPCount; //��� ����� �� 1-� ������ ���� �������� �� �������

          rQ1:=rQ1*rQ/MassaB; //��� ����� �� ��� ���-�� ������

//          prWH(sBeg+QT1.FieldByName('NAME').AsString+' ('+QT1.FieldByName('IDCARD').AsString+'). �� ����. ���-��: '+FloatToStr(RoundEx(rQ1*1000)/1000)+' '+sM);
          prCalcBlCompl(sBeg,QT1.FieldByName('NAME').AsString,IdPos,QT1.FieldByName('IDCARD').AsInteger,iDate,QT1.FieldByName('TCARD').AsInteger,IdSkl,rQ1,iMain,taCalcB,taSpecC,taSpec,Memo1,iT);

          QT1.Next;
        end;
        QT1.Active:=False;
        QT1.Free;
      end;
    end else
    begin
      //��������� � �������� ������� �� ������ ������ ����� ������ ��� - �� ����
      kM:=prFindKM(IdM);
      prFindSM(IdM,sM,iMain); //��� �������� �������� ������� ���������
      rQ:=rQ*kM;              //������ ������� - ��� ������ �� �����������

      prWH(sBeg+sName+' ('+IntToStr(iCode)+'). �����.  ���-��: '+FloatToStr(RoundEx(rQ*1000)/1000)+' '+sM,Memo1);

      //������ ���� ��� � �������� � � ������
//      prCalcSave;
      par := VarArrayCreate([0,1], varInteger);
      par[0]:=IdPos;
      par[1]:=iCode;
      if taCalcB.Locate('ID;IDCARD',par,[]) then
      begin
        rQb:=taCalcB.fieldByName('QUANTC').AsFloat+rQ;
        taCalcB.edit;
        taCalcB.fieldByName('QUANTC').AsFloat:=rQb;
        taCalcB.fieldByName('PRICEIN').AsFloat:=0;
        taCalcB.fieldByName('SUMIN').AsFloat:=0;
        taCalcB.fieldByName('IM').AsInteger:=iMain;
        taCalcB.fieldByName('SM').AsString:=sM;
        taCalcB.fieldByName('SB').AsString:='';
        taCalcB.Post;
      end else
      begin
        taCalcB.Append;
        taCalcB.fieldByName('ID').AsInteger:=IdPos;
        taCalcB.fieldByName('CODEB').AsInteger:=taSpec.FieldbyName('IdGoods').AsInteger;
        taCalcB.fieldByName('NAMEB').AsString:=taSpec.FieldbyName('NameG').AsString;
        taCalcB.fieldByName('QUANT').AsFloat:=taSpec.FieldbyName('QuantFact').AsFloat;
        taCalcB.fieldByName('PRICEOUT').AsFloat:=0;
        taCalcB.fieldByName('SUMOUT').AsFloat:=0;
        taCalcB.fieldByName('IDCARD').AsInteger:=iCode;
        taCalcB.fieldByName('NAMEC').AsString:=sName;
        taCalcB.fieldByName('QUANTC').AsFloat:=rQ;
        taCalcB.fieldByName('PRICEIN').AsFloat:=0;
        taCalcB.fieldByName('SUMIN').AsFloat:=0;
        taCalcB.fieldByName('IM').AsInteger:=iMain;
        taCalcB.fieldByName('SM').AsString:=sM;
        taCalcB.fieldByName('SB').AsString:='';
        taCalcB.Post;
      end;

      if taSpecC.Locate('IdGoods',iCode,[]) then
      begin //�������������
        VarsToSpec; //������� �������� ��������
        taSpecC.Edit;
        if iT=0 then //����������� ������� (�� ����� � �������������� ���������)
        begin
          taSpecC.FieldbyName('QuantFact').AsFloat:=rQ+Qf;
          taSpecC.FieldbyName('SumInF').AsFloat:=(rQ+Qf)*Pr2;
        end;
        if iT=1 then //��� ����� � �������������� ���������
        begin
          taSpecC.FieldbyName('Quant').AsFloat:=Qr-rQ; //�������� ���������� �����
          taSpecC.FieldbyName('SumIn').AsFloat:=(Qr-rQ)*Pr1;
        end;
        taSpecC.Post;
      end else
      begin
        VarsToZero;

        iMax:=1;
        if not taSpecC.Eof then
        begin
          taSpecC.Last;
          iMax:=taSpecC.FieldbyName('Num').AsInteger+1;
        end;

        taSpecC.Append;
        taSpecC.FieldbyName('Num').AsInteger:=iMax;

        taSpecC.FieldbyName('IdGoods').AsInteger:=iCode;
        taSpecC.FieldbyName('NameG').AsString:=sName;
        taSpecC.FieldbyName('IM').AsInteger:=iMain;
        taSpecC.FieldbyName('SM').AsString:=sM;

//        prFindGroup(iCode,NameGr,Id_Group); //�������� ������

        taSpecC.FieldbyName('Id_Group').AsInteger:=0;
//        taSpecC.FieldbyName('Id_Group').AsInteger:=Id_Group;
//        taSpecC.FieldbyName('NameGr').AsString:=NameGr;

        if iT=0 then
        begin
          taSpecC.FieldbyName('Quant').AsFloat:=0;
          taSpecC.FieldbyName('QuantFact').AsFloat:=rQ+Qf;
        end;

        if iT=1 then
        begin
          taSpecC.FieldbyName('Quant').AsFloat:=-rQ;
          taSpecC.FieldbyName('QuantFact').AsFloat:=0;
        end;

        taSpecC.FieldbyName('PriceIn').AsFloat:=0;
        taSpecC.FieldbyName('SumIn').AsFloat:=0;
        taSpecC.FieldbyName('PriceUch').AsFloat:=0;
        taSpecC.FieldbyName('SumUch').AsFloat:=0;

        taSpecC.FieldbyName('PriceInF').AsFloat:=0;
        taSpecC.FieldbyName('SumInF').AsFloat:=0;
        taSpecC.FieldbyName('PriceUchF').AsFloat:=0;
        taSpecC.FieldbyName('SumUchF').AsFloat:=0;

        taSpecC.FieldbyName('Km').AsFloat:=1;
        taSpecC.FieldbyName('TCard').AsInteger:=0;
        taSpecC.Post;

      end;
    end;
  end;
end;



procedure TfmAddCompl.prSave(IDH:INteger;Var Sum0,Sum1,Sum11:Real);
Var iMax:Integer;
begin
  with dmO do
  with dmORep do
  begin
    prAllViewOff;

    quSpecCompl.Active:=False;
    quSpecCompl.ParamByName('IDH').AsInteger:=IDH;
    quSpecCompl.Active:=True;

    quSpecCompl.First;
    while not quSpecCompl.Eof do quSpecCompl.Delete;

    taSpec.First;
    while not taSpec.Eof do
    begin
      quSpecCompl.Append;
      quSpecComplIDHEAD.AsInteger:=IDH;
      quSpecComplID.AsInteger:=taSpecNum.AsInteger;
      quSpecComplIDCARD.AsInteger:=taSpecIdGoods.AsInteger;
      quSpecComplQUANT.AsFloat:=taSpecQuantFact.AsFloat;
      quSpecComplIDM.AsInteger:=taSpecIM.AsInteger;
      quSpecComplKM.AsFloat:=taSpecKm.AsFloat;
      quSpecComplPRICEIN.AsFloat:=taSpecPriceIn.AsFloat;
      quSpecComplSUMIN.AsFloat:=taSpecSumIn.AsFloat;
      quSpecComplPRICEINUCH.AsFloat:=taSpecPriceUch.AsFloat;
      quSpecComplSUMINUCH.AsFloat:=taSpecSumUch.AsFloat;
      quSpecComplTCARD.AsInteger:=taSpecTCard.AsInteger;
      quSpecComplPRICEIN0.AsFloat:=taSpecPriceIn0.AsFloat;
      quSpecComplSUMIN0.AsFloat:=taSpecSumIn0.AsFloat;
      quSpecCompl.Post;

      taSpec.Next; delay(10);
    end;

    quSpecCompl.Active:=False;

    quSpecComplC.Active:=False;
    quSpecComplC.ParamByName('IDH').AsInteger:=IDH;
    quSpecComplC.Active:=True;

    quSpecComplC.First;
    while not quSpecComplC.Eof do quSpecComplC.Delete;

    Sum0:=0;Sum1:=0;Sum11:=0;

    taSpecC.First;
    while not taSpecC.Eof do
    begin
      quSpecComplC.Append;
      quSpecComplCIDHEAD.AsInteger:=IDH;
      quSpecComplCID.AsInteger:=taSpecCNum.AsInteger;
      quSpecComplCIDCARD.AsInteger:=taSpecCIdGoods.AsInteger;
      quSpecComplCQUANT.AsFloat:=taSpecCQuantFact.AsFloat;
      quSpecComplCIDM.AsInteger:=taSpecCIm.AsInteger;
      quSpecComplCKM.AsFloat:=taSpecCKm.AsFloat;
      quSpecComplCPRICEIN.AsFloat:=taSpecCPriceIn.AsFloat;
      quSpecComplCSUMIN.AsFloat:=taSpecCSumIn.AsFloat;
      quSpecComplCPRICEIN0.AsFloat:=taSpecCPriceIn0.AsFloat;
      quSpecComplCSUMIN0.AsFloat:=taSpecCSumIn0.AsFloat;
      quSpecComplCPRICEINUCH.AsFloat:=taSpecCPriceUch.AsFloat;
      quSpecComplCSUMINUCH.AsFloat:=taSpecCSumUch.AsFloat;
      quSpecComplCNAMESHORT.AsString:=taSpecCSM.AsString;
      quSpecComplCNAME.AsString:=taSpecCNameG.AsString;
      quSpecComplC.Post;

      Sum0:=Sum0+RoundVal(taSpecCSumIn0.AsFloat);
      Sum1:=Sum1+RoundVal(taSpecCSumIn.AsFloat);
      Sum11:=Sum11+RoundVal(taSpecCSumUch.AsFloat);

      taSpecC.Next; delay(10);
    end;
    quSpecComplC.Active:=False;

    if bPrintMemo then Memo1.Lines.Add('   ���������� ����.'); delay(10);

    quSpecComplCB.Active:=False;
    quSpecComplCB.ParamByName('IDH').AsInteger:=IdH;
    quSpecComplCB.Active:=True;

    while not quSpecComplCB.Eof do quSpecComplCB.Delete;

    iMax:=1;
    teCalcB1.First;
    while not teCalcB1.Eof do
    begin
      quSpecComplCB.Append;
      quSpecComplCBIDHEAD.AsInteger:=Idh;
      quSpecComplCBIDB.AsInteger:=teCalcB1ID.AsInteger;
      quSpecComplCBID.AsInteger:=iMax; //��� ��� ������
      quSpecComplCBCODEB.AsInteger:=teCalcB1CODEB.AsInteger;
      quSpecComplCBNAMEB.AsString:=teCalcB1NAMEB.AsString;
      quSpecComplCBQUANT.AsFloat:=teCalcB1QUANT.AsFloat;
      quSpecComplCBIDCARD.AsInteger:=teCalcB1IDCARD.AsInteger;
      quSpecComplCBNAMEC.AsString:=teCalcB1NAMEC.AsString;
      quSpecComplCBQUANTC.AsFloat:=teCalcB1QUANTC.AsFloat;
      quSpecComplCBPRICEIN.AsFloat:=teCalcB1PRICEIN.AsFloat;
      quSpecComplCBSUMIN.AsFloat:=teCalcB1SUMIN.AsFloat;
      quSpecComplCBIM.AsInteger:=teCalcB1IM.AsInteger;
      quSpecComplCBSM.AsString:=teCalcB1SM.AsString;
      quSpecComplCBSB.AsString:=teCalcB1SB.AsString;
      quSpecComplCBPRICEIN0.AsFloat:=teCalcB1PRICEIN0.AsFloat;
      quSpecComplCBSUMIN0.AsFloat:=teCalcB1SUMIN0.AsFloat;
      quSpecComplCB.Post;

      inc(iMax);
      teCalcB1.Next;
    end;

    quSpecComplCB.Active:=False;

    prAllViewOn;
  end;
end;

procedure TfmAddCompl.prCalcC(iDate,IdSkl:INteger);
Var IdM:INteger;
    rQ1:Real;
//    IdSkl:Integer;
begin
  with dmO do
  with dmORep do
  begin
    prAllViewOff;

    CloseTe(taSpecC); //����
    CloseTe(teCalcB1); //���� �����

//    IdSkl:=cxLookupComboBox1.EditValue;

    taSpec.First;
    while not taSpec.Eof do
    begin
     //��� ������ - ����� ��������� ���-�� � ��������, ��� ����� ���� � �������
      prFindSM(taSpecIM.AsInteger,StrWk,IdM);
      rQ1:=taSpecQuantFact.AsFloat*taSpecKm.AsFloat;
      prCalcBlCompl('    ',taSpecNameG.AsString,taSpecNum.AsInteger,taSpecIdGoods.AsInteger,iDate,taSpecTCard.AsInteger,IdSkl,rQ1,IdM,dmORep.teCalcB1,taSpecC,taSpec,Memo1,0);

      taSpec.Next;
    end;

    prAllViewOn;
  end;
end;

procedure TfmAddCompl.prCalcPr(IdSkl:INteger);
Var rQ,rSum,rSum0:Real;
begin
  with dmO do
  with dmORep do
  begin
    prAllViewOff;
    //����������� ���� ������
    taSpecC.First;
    while not taSpecC.Eof do
    begin
      prCalcSebSpec(taSpecCQuantFact.AsFloat,IdSkl,0,taSpecC);
      taSpecC.Next;
      delay(10);
    end;
    taSpecC.First;
    if bPrintMemo then Memo1.Lines.Add('   ������ ������������� ��.'); delay(10);

    //������������� ����
    teCalcB1.First;
    while not teCalcB1.Eof do
    begin
      if taSpecC.Locate('IdGoods',teCalcB1IDCARD.AsInteger,[]) then
      begin
        teCalcB1.Edit;
        teCalcB1PRICEIN.AsFloat:=taSpecCPriceIn.AsFloat;
        teCalcB1SUMIN.AsFloat:=teCalcB1QUANTC.AsFloat*taSpecCPriceIn.AsFloat;
        teCalcB1PRICEIN0.AsFloat:=taSpecCPriceIn0.AsFloat;
        teCalcB1SUMIN0.AsFloat:=teCalcB1QUANTC.AsFloat*taSpecCPriceIn0.AsFloat;
        teCalcB1.Post;
      end;
      teCalcB1.Next;
    end;

    iCol:=0;

    taSpec.First;
    while not taSpec.Eof do
    begin
      rSum:=0;
      rSum0:=0;
      teCalcB1.First;
      while not teCalcB1.Eof do
      begin
        if teCalcB1ID.AsInteger=taSpecNum.AsInteger then
        begin
          rSum:=rSum+teCalcB1SUMIN.AsFloat;
          rSum0:=rSum0+teCalcB1SUMIN0.AsFloat;
        end;
        teCalcB1.Next;
      end;

      rQ:=taSpecQuantFact.AsFloat;
      taSpec.Edit;
      taSpecSumIn.AsFloat:=rSum;
      taSpecSumIn0.AsFloat:=rSum0;
      if rQ<>0 then
      begin
        taSpecPriceIn.AsFloat:=rSum/rQ;
        taSpecPriceIn0.AsFloat:=rSum0/rQ;
      end
      else
      begin
        taSpecPriceIn.AsFloat:=0;
        taSpecPriceIn0.AsFloat:=0;
      end;
      taSpec.Post;

      taSpec.Next;
    end;

    prAllViewOn;
  end;
end;


procedure prCalcSebSpec(rQs:Real;IdSkl,IdCli:Integer;taSpecC:TdxMemData);
var PriceSp,PriceUch,rSumIn,rSumUch,rQ,rQp,rMessure,PriceSp0,rSumIn0:Real;
begin
  with dmO do
  with dmORep do
  begin
    PriceSp:=0;
    rSumIn:=0;
    rSumUch:=0;
    PriceSp0:=0;
    rSumIn0:=0;

    prSelPartIn(taSpecC.FieldByName('IdGoods').AsInteger,IdSkl,IdCli,0);

    quSelPartIn.First;
    if rQs>0 then
    begin
      while (not quSelPartIn.Eof) and (rQs>0) do
      begin
        //���� �� ���� ������� ���� �����, ��������� �������� ���
        rQp:=quSelPartInQREMN.AsFloat;
        if rQs<=rQp then  rQ:=rQs//��������� ������ ������ ���������
                          else  rQ:=rQp;
        rQs:=rQs-rQ;

        PriceSp:=quSelPartInPRICEIN.AsFloat;
        PriceSp0:=quSelPartInPRICEIN0.AsFloat;

        PriceUch:=quSelPartInPRICEOUT.AsFloat;
        rSumIn:=rSumIn+PriceSp*rQ;
        rSumIn0:=rSumIn0+PriceSp0*rQ;
        rSumUch:=rSumUch+PriceUch*rQ;
        quSelPartIn.Next;
      end;

      if rQs>0 then //�������� ������������� ������, �������� � ������, �� � ������� ��������� ������ ���, ��� � �������������
      begin
        if PriceSp=0 then
        begin //��� ���� ���������� ������� � ���������� ����������
          prCalcLastPrice1.ParamByName('IDGOOD').AsInteger:=taSpecC.FieldByName('IdGoods').AsInteger;
          prCalcLastPrice1.ParamByName('ISKL').AsInteger:=IdSkl;
          prCalcLastPrice1.ExecProc;

          PriceSp:=prCalcLastPrice1.ParamByName('PRICEIN').AsFloat;
          PriceSp0:=prCalcLastPrice1.ParamByName('PRICEIN0').AsFloat;

          rMessure:=prCalcLastPrice1.ParamByName('KOEF').AsFloat;
          if (rMessure<>0)and(rMessure<>1) then PriceSp:=PriceSp/rMessure;
          if (rMessure<>0)and(rMessure<>1) then PriceSp0:=PriceSp0/rMessure;
        end;
        rSumIn:=rSumIn+PriceSp*rQs;
        rSumIn0:=rSumIn0+PriceSp0*rQs;
      end;
    end;
    quSelPartIn.Active:=False;

    //�������� ���������
    taSpecC.Edit;
    if taSpecC.FieldByName('QuantFact').AsFloat<>0 then
    begin
      taSpecC.FieldByName('SumIn').AsFloat:=RoundVal(rSumIn);
      taSpecC.FieldByName('SumUch').AsFloat:=RoundVal(rSumUch);
      taSpecC.FieldByName('PriceIn').AsFloat:=RoundVal(rSumIn)/taSpecC.FieldByName('QuantFact').AsFloat;
      taSpecC.FieldByName('PriceUch').AsFloat:=RoundVal(rSumUch)/taSpecC.FieldByName('QuantFact').AsFloat;
      taSpecC.FieldByName('SumIn0').AsFloat:=RoundVal(rSumIn0);
      taSpecC.FieldByName('PriceIn0').AsFloat:=RoundVal(rSumIn0)/taSpecC.FieldByName('QuantFact').AsFloat;
    end else
    begin
      taSpecC.FieldByName('SumIn').AsFloat:=0;
      taSpecC.FieldByName('SumUch').AsFloat:=0;
      taSpecC.FieldByName('PriceIn').AsFloat:=0;
      taSpecC.FieldByName('PriceUch').AsFloat:=0;
      taSpecC.FieldByName('SumIn0').AsFloat:=0;
      taSpecC.FieldByName('PriceIn0').AsFloat:=0;
    end;
    taSpecC.Post;
  end;
end;

procedure TfmAddCompl.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  PageControl1.Align:=alClient;
  ViewCom.RestoreFromIniFile(CurDir+GridIni);
  ViewComC.RestoreFromIniFile(CurDir+GridIni);
  ViewComBC.RestoreFromIniFile(CurDir+GridIni);
end;

procedure TfmAddCompl.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if cxButton1.Enabled=True then
  begin
    if MessageDlg('�� ������������� ������ ����� �� ���������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      bAddSpecCompl:=False;
      ViewCom.StoreToIniFile(CurDir+GridIni,False);
      ViewComC.StoreToIniFile(CurDir+GridIni,False);
      ViewComBC.StoreToIniFile(CurDir+GridIni,False);
      Action:=caHide;
    end
    else Action:= caNone;
  end;
end;

procedure TfmAddCompl.acAddPosExecute(Sender: TObject);
Var iMax:INteger;
begin
//�������� �������
  iMax:=1;
  ViewCom.BeginUpdate;

  taSpec.First;
  if not taSpec.Eof then
  begin
    taSpec.Last;
    iMax:=taSpecNum.AsInteger+1;
  end;

  taSpec.Append;
  taSpecNum.AsInteger:=iMax;
  taSpecIdGoods.AsInteger:=0;
  taSpecNameG.AsString:='';
  taSpecIM.AsInteger:=0;
  taSpecSM.AsString:='';
  taSpecQuantFact.AsFloat:=0;
  taSpecPriceIn.AsFloat:=0;
  taSpecSumIn.AsFloat:=0;
  taSpecPriceUch.AsFloat:=0;
  taSpecSumUch.AsFloat:=0;
  taSpecKm.AsFloat:=0;
  taSpecTCard.AsInteger:=0;
  taSpec.Post;
  ViewCom.EndUpdate;

  ViewComNameG.Options.Editing:=True;
  ViewComNameG.Focused:=True;

  prRowFocus(ViewCom);
end;

procedure TfmAddCompl.FormShow(Sender: TObject);
begin
  Memo1.Clear;
  PageControl1.ActivePageIndex:=0;
  iCol:=0;
end;

procedure TfmAddCompl.cxLabel1Click(Sender: TObject);
begin
  acAddPos.Execute;
end;

procedure TfmAddCompl.ViewComEditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
Var iCode:Integer;
    Km:Real;
    sName:String;
begin
  with dmO do
  begin

    if (Key=$0D) then
    begin
      if ViewCom.Controller.FocusedColumn.Name='ViewComIdGoods' then
      begin
        iCode:=VarAsType(AEdit.EditingValue, varInteger);

        quFCard.Active:=False;
        quFCard.SelectSQL.Clear;
        quFCard.SelectSQL.Add('SELECT ID,NAME,IMESSURE,PARENT,TCARD,INDS,LASTPRICEOUT,CATEGORY');
        quFCard.SelectSQL.Add('FROM OF_CARDS');
        quFCard.SelectSQL.Add('where ID='+IntToStr(iCode));
        quFCard.SelectSQL.Add('and PARENT in (SELECT ID_CLASSIF FROM RCLASSIF WHERE RIGHTS=0 AND ID_PERSONAL='+its(Person.Id)+')');
        quFCard.SelectSQL.Add('and IACTIVE>0');
        quFCard.Active:=True;

        if quFCard.RecordCount=1 then
        begin
          ViewCom.BeginUpdate;
          taSpec.Edit;
          taSpecIdGoods.AsInteger:=iCode;
          taSpecNameG.AsString:=quFCardNAME.AsString;
          taSpecIM.AsInteger:=quFCardIMESSURE.AsInteger;
          taSpecSM.AsString:=prFindKNM(quFCardIMESSURE.AsInteger,Km);
          taSpecKm.AsFloat:=Km;
          taSpecTCard.AsInteger:=quFCardTCARD.AsInteger;

          taSpecQuantFact.AsFloat:=0;
          taSpecPriceIn.AsFloat:=0;
          taSpecSumIn.AsFloat:=0;
          taSpecPriceUch.AsFloat:=0;
          taSpecSumUch.AsFloat:=0;

          taSpec.Post;

          ViewCom.EndUpdate;
        end;
      end;
      if ViewCom.Controller.FocusedColumn.Name='ViewComNameG' then
      begin
        sName:=VarAsType(AEdit.EditingValue, varString);

        fmFCards.ViewFCards.BeginUpdate;
        quFCard.Active:=False;
        quFCard.SelectSQL.Clear;
        quFCard.SelectSQL.Add('SELECT first 100 ID,NAME,IMESSURE,PARENT,TCARD,INDS,LASTPRICEOUT,CATEGORY');
        quFCard.SelectSQL.Add('FROM OF_CARDS');
        quFCard.SelectSQL.Add('where UPPER(NAME) like ''%'+AnsiUpperCase(sName)+'%''');
        quFCard.SelectSQL.Add('and IACTIVE>0');
        quFCard.SelectSQL.Add('and PARENT in (SELECT ID_CLASSIF FROM RCLASSIF WHERE RIGHTS=0 AND ID_PERSONAL='+its(Person.Id)+')');
        quFCard.SelectSQL.Add('Order by NAME');

        quFCard.Active:=True;

        bAdd:=True;

        quFCard.Locate('NAME',sName,[loCaseInsensitive, loPartialKey]);
        prRowFocus(fmFCards.ViewFCards);
        fmFCards.ViewFCards.EndUpdate;

        //�������� ����� ������ � ����� ������
        fmFCards.ShowModal;
        if fmFCards.ModalResult=mrOk then
        begin
          if quFCard.RecordCount>0 then
          begin
            ViewCom.BeginUpdate;
            taSpec.Edit;
            taSpecIdGoods.AsInteger:=quFCardID.AsInteger;
            taSpecNameG.AsString:=quFCardNAME.AsString;
            taSpecIM.AsInteger:=quFCardIMESSURE.AsInteger;
            taSpecSM.AsString:=prFindKNM(quFCardIMESSURE.AsInteger,Km);
            taSpecKm.AsFloat:=Km;
            taSpecTCard.AsInteger:=quFCardTCARD.AsInteger;

            taSpecQuantFact.AsFloat:=0;
            taSpecPriceIn.AsFloat:=0;
            taSpecSumIn.AsFloat:=0;
            taSpecPriceUch.AsFloat:=0;
            taSpecSumUch.AsFloat:=0;

            taSpec.Post;
            ViewCom.EndUpdate;
            AEdit.SelectAll;
          end;
        end;
        bAdd:=False;
      end;
      ViewComQuantFact.Focused:=True;
    end else
      if ViewCom.Controller.FocusedColumn.Name='ViewComIdGoods' then
        if fTestKey(Key)=False then
          if taSpec.State in [dsEdit,dsInsert] then taSpec.Cancel;
  end;
end;

procedure TfmAddCompl.ViewComEditKeyPress(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
Var Km:Real;
    sName:String;
begin
  if bAdd then exit;
  with dmO do
  begin
    if ViewCom.Controller.FocusedColumn.Name='ViewComNameG' then
    begin
      sName:=VarAsType(AEdit.EditingValue ,varString)+Key;
//      Label2.Caption:=sName;
      if length(sName)>2 then
      begin
        quFCard.Active:=False;
        quFCard.SelectSQL.Clear;
        quFCard.SelectSQL.Add('SELECT  first 2 ID,NAME,IMESSURE,PARENT,TCARD,INDS,LASTPRICEOUT,CATEGORY');
        quFCard.SelectSQL.Add('FROM OF_CARDS');
        quFCard.SelectSQL.Add('where UPPER(NAME) like ''%'+AnsiUpperCase(sName)+'%''');
        quFCard.SelectSQL.Add('and IACTIVE>0');
        quFCard.SelectSQL.Add('and PARENT in (SELECT ID_CLASSIF FROM RCLASSIF WHERE RIGHTS=0 AND ID_PERSONAL='+its(Person.Id)+')');
        quFCard.SelectSQL.Add('Order by NAME');
        quFCard.Active:=True;

        if quFCard.RecordCount=1 then
        begin
          ViewCom.BeginUpdate;
          taSpec.Edit;
          taSpecIdGoods.AsInteger:=quFCardID.AsInteger;
          taSpecNameG.AsString:=quFCardNAME.AsString;
          taSpecIM.AsInteger:=quFCardIMESSURE.AsInteger;
          taSpecSM.AsString:=prFindKNM(quFCardIMESSURE.AsInteger,Km);
          taSpecKm.AsFloat:=Km;
          taSpecTCard.AsInteger:=quFCardTCARD.AsInteger;

          taSpecQuantFact.AsFloat:=0;
          taSpecPriceIn.AsFloat:=0;
          taSpecSumIn.AsFloat:=0;
          taSpecPriceUch.AsFloat:=0;
          taSpecSumUch.AsFloat:=0;

          taSpec.Post;
          ViewCom.EndUpdate;
          AEdit.SelectAll;
          ViewComNameG.Options.Editing:=False;
          ViewComNameG.Focused:=True;
          Key:=#0;
        end;
      end;
    end;
  end;//}
end;

procedure TfmAddCompl.cxLabel7Click(Sender: TObject);
begin
  acAddList.Execute;
end;

procedure TfmAddCompl.ViewComDragOver(Sender, Source: TObject; X, Y: Integer;
  State: TDragState; var Accept: Boolean);
begin
  Accept:=False;
  if bDCompl then  Accept:=True;
end;

procedure TfmAddCompl.ViewComDragDrop(Sender, Source: TObject; X,
  Y: Integer);
Var iCo:Integer;
    i,j: Integer;
    iNum: Integer;
    Rec:TcxCustomGridRecord;
    Km:Real;
    iMax:Integer;
begin
  if bDCompl then
  begin
    ResetAddVars;
    iCo:=fmGoods.ViewGoods.Controller.SelectedRecordCount;
    if iCo>0 then
    begin
      if MessageDlg('�� ������������� ������ �������� ��������� ������� ('+IntToStr(iCo)+' ��.) � ��������������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        with dmO do
        begin
          Memo1.Clear;
          Memo1.Lines.Add('����� .. ���� ���������� �������.');
          ViewCom.BeginUpdate;

          iMax:=1;
          taSpec.First;
          if not taSpec.Eof then
          begin
            taSpec.Last;
            iMax:=taSpecNum.AsInteger+1;
          end;

          for i:=0 to fmGoods.ViewGoods.Controller.SelectedRecordCount-1 do
          begin
            Rec:=fmGoods.ViewGoods.Controller.SelectedRecords[i];

            for j:=0 to Rec.ValueCount-1 do
            begin
              if fmGoods.ViewGoods.Columns[j].Name='ViewGoodsID' then break;
            end;

            iNum:=Rec.Values[j];
          //��� ��� - ����������
            with dmO do
            begin
              if quCardsSel.Locate('ID',iNum,[]) then
              begin
                try
                  if taSpec.Locate('IdGoods',iNum,[])=False then
                  begin
                    taSpec.Append;
                    taSpecNum.AsInteger:=iMax;
                    taSpecIdGoods.AsInteger:=quCardsSelID.AsInteger;
                    taSpecNameG.AsString:=quCardsSelNAME.AsString;
                    taSpecIM.AsInteger:=quCardsSelIMESSURE.AsInteger;
                    taSpecSM.AsString:=prFindKNM(quCardsSelIMESSURE.AsInteger,Km);
                    taSpecQuantFact.AsFloat:=0;
                    taSpecPriceIn.AsFloat:=0;
                    taSpecSumIn.AsFloat:=0;
                    taSpecPriceUch.AsFloat:=0;
                    taSpecSumUch.AsFloat:=0;
                    taSpecKm.AsFloat:=Km;
                    taSpecTCard.AsInteger:=quCardsSelTCard.AsInteger;
                    taSpec.Post;
                    delay(10);
                    inc(iMax);
                  end;
                except
                end;
              end;
            end;
          end;
          ViewCom.EndUpdate;
          Memo1.Lines.Add('���������� ��.');
        end;
      end;
    end;
  end;
end;

procedure TfmAddCompl.cxLabel2Click(Sender: TObject);
begin
  acDelPos.Execute;
end;

procedure TfmAddCompl.cxLabel9Click(Sender: TObject);
begin
  acDelAll.Execute;
end;

procedure TfmAddCompl.acSaveInvExecute(Sender: TObject);
Var Idh:Integer;
    iDate:Integer;
    Sum0,Sum1,Sum11:Real;
    iSS:INteger;
begin
  //��������� ������������
  Memo1.Clear;
  Memo1.Lines.Add('����� ... ���� ���������� ������.'); delay(10);
  iSS:=prIss(cxLookupComboBox1.EditValue);

  iDate:=Trunc(date);
  if cxDateEdit1.Date>3000 then iDate:=Trunc(cxDateEdit1.Date);

  with dmO do
  with dmORep do
  begin
    if taSpec.State in [dsEdit,dsInsert] then taSpec.Post;

    //������� ������ ������
    Memo1.Lines.Add('   �������� ������.'); delay(10);

    prCalcC(iDate,cxLookupComboBox1.EditValue);
    Memo1.Lines.Add('   ������ ���������� ��.'); delay(10);
    //���� ������� ���-��
    prCalcPr(cxLookupComboBox1.EditValue);
    Memo1.Lines.Add('   �������� ��� ��.'); delay(10);

    IDH:=cxTextEdit1.Tag;
    if cxTextEdit1.Tag=0 then
    begin
      IDH:=GetId('HeadCompl');
      cxTextEdit1.Tag:=IDH;
      if cxTextEdit1.Text=prGetNum(6,0) then prGetNum(6,1); //��������
    end;

    quDocsComlRec.Active:=False;

    quDocsComlRec.ParamByName('IDH').AsInteger:=IDH;
    quDocsComlRec.Active:=True;

    quDocsComlRec.First;
    if quDocsComlRec.RecordCount=0 then quDocsComlRec.Append else quDocsComlRec.Edit;

    quDocsComlRecID.AsInteger:=IDH;
    quDocsComlRecDATEDOC.AsDateTime:=iDate;
    quDocsComlRecNUMDOC.AsString:=cxTextEdit1.Text;
    quDocsComlRecIDSKL.AsInteger:=cxLookupComboBox1.EditValue;
    quDocsComlRecIACTIVE.AsInteger:=0;
    quDocsComlRecOPER.AsString:='';
    quDocsComlRecSUMIN.AsFloat:=0;
    quDocsComlRecSUMUCH.AsFloat:=0;
    quDocsComlRecSUMTAR.AsFloat:=0;
    quDocsComlRecPROCNAC.AsFloat:=0;
    quDocsComlRecIDSKLTO.AsInteger:=cxLookupComboBox2.EditValue;
    if cxCheckBox1.Checked then quDocsComlRecTOREAL.AsInteger:=1 else quDocsComlRecTOREAL.AsInteger:=0;
    quDocsComlRec.Post;

    cxTextEdit1.Tag:=IDH;

    //�������� ������������ ������ � �����

    Memo1.Lines.Add('   ���������� ������,�����.'); delay(10);
    prSave(IDH,Sum0,Sum1,Sum11);

    //�������� ������������ ������
    quDocsComlRec.Edit;

    if iSS<>2 then quDocsComlRecSUMIN.AsFloat:=RoundVal(Sum1)
    else quDocsComlRecSUMIN.AsFloat:=RoundVal(Sum0);

    quDocsComlRecSUMUCH.AsFloat:=RoundVal(Sum11);
    quDocsComlRec.Post;

    quDocsComlRec.Active:=False;

    fmDocsCompl.ViewComplB.BeginUpdate;
    quDocsCompl.FullRefresh;
    quDocsCompl.Locate('ID',IDH,[]);
    fmDocsCompl.ViewComplB.EndUpdate;

  end;
  Memo1.Lines.Add('���������� ��.'); delay(10);
end;

procedure TfmAddCompl.ViewComEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  iCol:=0;
  if ViewCom.Controller.FocusedColumn.Name='ViewComQuant' then iCol:=1;
  if ViewCom.Controller.FocusedColumn.Name='ViewComPriceIn' then iCol:=2;
  if ViewCom.Controller.FocusedColumn.Name='ViewComSumIn' then iCol:=3;
  if ViewCom.Controller.FocusedColumn.Name='ViewComPriceUch' then iCol:=4;
  if ViewCom.Controller.FocusedColumn.Name='ViewComSumUch' then iCol:=5;
end;

procedure TfmAddCompl.acAddListExecute(Sender: TObject);
begin
  bAddSpecCompl:=True;
  fmGoods.Show;
end;

procedure TfmAddCompl.acDelPosExecute(Sender: TObject);
begin
  if taSpec.RecordCount>0 then taSpec.Delete;
end;

procedure TfmAddCompl.acDelAllExecute(Sender: TObject);
begin
  if MessageDlg('�������� ��������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
    taSpec.First;
    while not taSpec.Eof do taSpec.Delete;
  end;
end;

procedure TfmAddCompl.Memo1DblClick(Sender: TObject);
begin
  fmMessage:=tfmMessage.Create(Application);
  fmMessage.Memo1.Lines:=Memo1.Lines;
  fmMessage.ShowModal;
  fmMessage.Release;
end;

procedure TfmAddCompl.cxButton2Click(Sender: TObject);
begin
  Close;
end;

procedure TfmAddCompl.cxLookupComboBox1PropertiesChange(Sender: TObject);
begin
  with dmO do
  begin
    CurVal.IdMH:=cxLookupComboBox1.EditValue;
    CurVal.NAMEMH:=cxLookupComboBox1.Text;
  end;
end;

procedure TfmAddCompl.taSpecQuantChange(Sender: TField);
begin
  //���������� �����
  if iCol=1 then
  begin
    taSpecSumIn.AsFloat:=taSpecPriceIn.AsFloat*taSpecQuantFact.AsFloat;
    taSpecSumUch.AsFloat:=taSpecPriceUch.AsFloat*taSpecQuantFact.AsFloat;
  end;
end;

procedure TfmAddCompl.taSpec2PriceInChange(Sender: TField);
begin
  //���������� ����
 if iCol=2 then
  begin
    taSpecSumIn.AsFloat:=taSpecPriceIn.AsFloat*taSpecQuantFact.AsFloat;
  end;
end;

procedure TfmAddCompl.taSpec2SumInChange(Sender: TField);
begin
  if iCol=3 then  //���������� �����
  begin
    if abs(taSpecQuantFact.AsFloat)>0 then taSpecPriceIn.AsFloat:=RoundEx(taSpecSumIn.AsFloat/taSpecQuantFact.AsFloat*100)/100;
  end;
end;

procedure TfmAddCompl.taSpec2PriceUchChange(Sender: TField);
begin
  //���������� ���� �������
  if iCol=4 then
  begin
    taSpecSumUch.AsFloat:=taSpecPriceUch.AsFloat*taSpecQuantFact.AsFloat;
  end;
end;

procedure TfmAddCompl.taSpec2SumUchChange(Sender: TField);
begin
  if iCol=5 then  //���������� �����
  begin
    if abs(taSpecQuantFact.AsFloat)>0 then taSpecPriceUch.AsFloat:=RoundEx(taSpecSumUch.AsFloat/taSpecQuantFact.AsFloat*100)/100;
  end;
end;

procedure TfmAddCompl.cxButton3Click(Sender: TObject);
Var rQSum:Real;
begin
//������
  rQSum:=0;
  ViewCom.BeginUpdate;
  taSpec.First;
  while not taSpec.Eof do
  begin
    rQSum:=rQSum+taSpecQuantFact.AsFloat;
    taSpec.Next;
  end;
  taSpec.First;
  ViewCom.EndUpdate;

  if cxRadioButton1.Checked then
  begin
    frRepCompl.LoadFromFile(CurDir + 'Compl.frf');

    frVariables.Variable['DocNum']:=cxTextEdit1.Text;
    frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date);
    frVariables.Variable['Depart']:=CommonSet.DepartName;
    frVariables.Variable['DocStore']:=cxLookupComboBox1.Text;
    frVariables.Variable['SumOut']:=rQsum;

    frRepCompl.ReportName:='����� ����� (������������ ����).';
    frRepCompl.PrepareReport;
    frRepCompl.ShowPreparedReport;
  end;
  if cxRadioButton2.Checked then
  begin
    frRepCompl.LoadFromFile(CurDir + 'Compl1.frf');

    frVariables.Variable['DocNum']:=cxTextEdit1.Text;
    frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date);
    frVariables.Variable['Depart']:=CommonSet.DepartName;
    frVariables.Variable['DocStore']:=cxLookupComboBox1.Text;
    frVariables.Variable['SumOut']:=rQsum;

    frRepCompl.ReportName:='����� ����� (������������ ����).';
    frRepCompl.PrepareReport;
    frRepCompl.ShowPreparedReport;
  end;
end;

procedure TfmAddCompl.acAddRealisExecute(Sender: TObject);
Var iMax,IdSkl:Integer;
    rPrice:Real;
    iM,iTC:integer;
    KM:Real;
    NameM:String;
begin
//�������� ���������� �� ����
  with dmO do
  with dmORep do
  begin
    IdSkl:=cxLookupComboBox1.EditValue;
    if IdSkl<=0 then
    begin
      showmessage('�������� ����� ��������');
      exit;
    end;

    if taSpec.RecordCount>0 then
    begin
      if MessageDlg('������� ������������ ����� �����������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        try
          ViewCom.BeginUpdate;
          ViewComC.BeginUpdate;
          ViewComBC.BeginUpdate;

          CloseTe(taSpec);
          CloseTe(taSpecC);
          CloseTe(teCalcB1);
        finally
          ViewCom.EndUpdate;
          ViewComC.EndUpdate;
          ViewComBC.EndUpdate;
        end;
      end;
    end;

    iMax:=1;
    ViewCom.BeginUpdate;

    cxCheckBox1.Checked:=True;

    taSpec.First;
    if not taSpec.Eof then
    begin
      taSpec.Last;
      iMax:=taSpecNum.AsInteger+1;
    end;

    quSpecRDay.Active:=False;
    quSpecRDay.ParamByName('DDATE').AsDateTime:=cxDateEdit1.Date;
    quSpecRDay.ParamByName('IDSKL').AsInteger:=IdSkl;
    quSpecRDay.Active:=True;
    quSpecRDay.First;
    while not quSpecRDay.Eof do
    begin
      if quSpecRDayQUANT.AsFloat>0.001 then //�������� ��� ������������� ���-�� (��������)
      begin
        rPrice:=quSpecRDaySUMR.AsFloat/quSpecRDayQUANT.AsFloat;

        iM:=quSpecRDayIDM.AsInteger;
        kM:=quSpecRDayKM.AsFloat;
        NameM:=quSpecRDayNAMESHORT.AsString;

        quFindTCard.Active:=False;
        quFindTCard.ParamByName('IDCARD').AsInteger:=quSpecRDayIDCARD.AsInteger;
        quFindTCard.ParamByName('IDATE').AsDateTime:=Trunc(cxDateEdit1.Date);
        quFindTCard.Active:=True;
        if quFindTCard.RecordCount>0 then iTC:=1 else iTC:=0;
        quFindTCard.Active:=False;

        if iM=0 then
        begin
          iM:=prFindM(quSpecRDayIDCARD.AsInteger,kM,NameM);
        end;

        if (iM>0)and(iTC>0) then
        begin
          taSpec.Append;
          taSpecNum.AsInteger:=iMax;
          taSpecIdGoods.AsInteger:=quSpecRDayIDCARD.AsInteger;
          taSpecNameG.AsString:=quSpecRDayNAME.AsString;
          taSpecIM.AsInteger:=iM;
          taSpecSM.AsString:=NameM;
          taSpecQuantFact.AsFloat:=quSpecRDayQUANT.AsFloat;
          taSpecPriceIn.AsFloat:=0;
          taSpecSumIn.AsFloat:=0;
          taSpecPriceUch.AsFloat:=rPrice;
          taSpecSumUch.AsFloat:=quSpecRDaySUMR.AsFloat;
          taSpecKm.AsFloat:=kM;
          taSpecTCard.AsInteger:=1;
          taSpec.Post;

          inc(iMax);
        end else
        begin
          Memo1.Lines.Add('������ ���������� - '+quSpecRDayIDCARD.AsString+' '+quSpecRDayNAME.AsString);
        end;
      end;

      quSpecRDay.Next;
    end;
    ViewCom.EndUpdate;
    quSpecRDay.Active:=False;
  end;
end;

procedure TfmAddCompl.cxLabel8Click(Sender: TObject);
begin
  acAddRealis.Execute;
end;

procedure TfmAddCompl.acEquialRealExecute(Sender: TObject);
Var IdSkl:Integer;
    rSumC,rSumRe:Real;
begin
  //��������� ������������� ���������� � ���� ������������
  with dmO do
  with dmORep do
  begin
    IdSkl:=cxLookupComboBox1.EditValue;

    Memo1.Lines.Add('������ ��������.'); delay(10);

    rSumRe:=0;

    quSpecRDay.Active:=False;
    quSpecRDay.ParamByName('DDATE').AsDateTime:=cxDateEdit1.Date;
    quSpecRDay.ParamByName('IDSKL').AsInteger:=IdSkl;
    quSpecRDay.Active:=True;
    quSpecRDay.First;
    while not quSpecRDay.Eof do
    begin
      if quSpecRDayQUANT.AsFloat>0 then
      begin
        if taSpec.Locate('IdGoods',quSpecRDayIDCARD.AsInteger,[]) then
        begin
          if abs(taSpecQuantFact.AsFloat-quSpecRDayQUANT.AsFloat)>0.001 then
            Memo1.Lines.Add('����������� � ���-�� - '+quSpecRDayIDCARD.AsString+' '+quSpecRDayNAME.AsString);
          if abs(taSpecSumIn.AsFloat-quSpecRDaySUMIN.AsFloat)>0.05 then
            Memo1.Lines.Add('����������� � ����� (�����) - '+quSpecRDayIDCARD.AsString+' '+quSpecRDayNAME.AsString+' '+fts(rv(taSpecSumIn.AsFloat-quSpecRDaySUMIN.AsFloat)));
        end else
          Memo1.Lines.Add('��� � ���� - '+quSpecRDayIDCARD.AsString+' '+quSpecRDayNAME.AsString);
      end;
      delay(10);

      rSumRe:=rSumRe+quSpecRDaySUMIN.AsFloat;
      quSpecRDay.Next;
    end;


    Memo1.Lines.Add('�������.'); delay(10);
    rSumC:=0;
    taSpec.First;
    while not taSpec.Eof do
    begin
      if quSpecRDay.Locate('IDCARD',taSpecIdGoods.AsInteger,[])=False then
      begin
        Memo1.Lines.Add('��� � ���������� - '+taSpecIdGoods.AsString+' '+taSpecNameG.AsString); delay(10);
      end;

      rSumC:=rSumC+taSpecSumIn.AsFloat;
      taSpec.Next;
    end;

    quSpecRDay.Active:=False;

    if abs(rSumC-rSumRe)>0.05 then
      Memo1.Lines.Add('����������� � ����� ����� - ���: '+FloatToStr(RoundVal(rSumC))+' ����:'+FloatToStr(RoundVal(rSumRe)))
    else
      Memo1.Lines.Add('����������� � ����� ��� - ���: '+FloatToStr(RoundVal(rSumC))+' ����:'+FloatToStr(RoundVal(rSumRe)));


    Memo1.Lines.Add('�������� ��.'); delay(10);
  end;
end;

procedure TfmAddCompl.acPartOutTestExecute(Sender: TObject);
Var rSumIn:Real;
begin
  //��������� ������������� ���������� � ���� ������������
  with dmO do
  with dmORep do
  begin
    Memo1.Lines.Add('������ ��������.'); delay(10);
    if taSpecC.Active then
    begin
      taSpecC.First;
      while not taSpecC.Eof do
      begin
        rSumIn:=0;
        quPODoc.Active:=False;
        quPODoc.ParamByName('IDCARD').AsInteger:=taSpecCIdGoods.AsInteger;
        quPODoc.ParamByName('IDTYPE').AsInteger:=6;
        quPODoc.ParamByName('IDH').AsInteger:=cxTextEdit1.Tag;
        quPODoc.Active:=True;
        if quPODoc.RecordCount>0 then rSumIn:=quPODocSUMIN.AsFloat;
        quPODoc.Active:=False;

        if abs(rSumIn-taSpecCSumIn.AsFloat)>0.03 then
        begin
          Memo1.Lines.Add('����������� � ����� ��: '+FloatToStr(RoundVal(taSpecCSumIn.AsFloat))+' ����:'+FloatToStr(RoundVal(rSumIn)))
        end;

        taSpecC.Next; delay(10);
      end;
    end;
    Memo1.Lines.Add('�������� ��.'); delay(10);
  end;
end;

procedure TfmAddCompl.Excel01Click(Sender: TObject);
begin
  prNExportExel5(ViewCom);
end;

procedure TfmAddCompl.MenuItem1Click(Sender: TObject);
begin
  prNExportExel5(ViewComC);
end;

procedure TfmAddCompl.MenuItem2Click(Sender: TObject);
begin
  prNExportExel5(ViewComBC);
end;

procedure TfmAddCompl.N5Click(Sender: TObject);
begin
//��������
  ViewComBC.DataController.Groups.FullCollapse;
end;

procedure TfmAddCompl.N6Click(Sender: TObject);
begin
//��������
  ViewComBC.DataController.Groups.FullExpand;
end;

procedure TfmAddCompl.acExitExecute(Sender: TObject);
begin
  Close;
end;

end.
