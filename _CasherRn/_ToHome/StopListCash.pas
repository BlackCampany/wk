unit StopListCash;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ComCtrls;

type
  TfmStopListCash = class(TForm)
    Panel1: TPanel;
    cxButton8: TcxButton;
    cxButton9: TcxButton;
    cxButton3: TcxButton;
    ViewSl: TcxGridDBTableView;
    LevelSL: TcxGridLevel;
    GridSL: TcxGrid;
    ViewSlSIFR: TcxGridDBColumn;
    ViewSlNAME: TcxGridDBColumn;
    ViewSlBACKBGR: TcxGridDBColumn;
    TimerSl: TTimer;
    StatusBar1: TStatusBar;
    procedure FormCreate(Sender: TObject);
    procedure cxButton8Click(Sender: TObject);
    procedure cxButton9Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure TimerSlTimer(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure ViewSlCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmStopListCash: TfmStopListCash;

implementation

uses Dm;

{$R *.dfm}

procedure TfmStopListCash.FormCreate(Sender: TObject);
begin
  GridSL.Align:=AlClient;
end;

procedure TfmStopListCash.cxButton8Click(Sender: TObject);
begin
  with dmC do
  begin
    try
      quMenuSL.Prior;
    except
    end;
  end;
end;

procedure TfmStopListCash.cxButton9Click(Sender: TObject);
begin
  with dmC do
  begin
    try
      quMenuSL.Next;
    except
    end;
  end;
end;

procedure TfmStopListCash.cxButton3Click(Sender: TObject);
begin
  Close;
end;

procedure TfmStopListCash.TimerSlTimer(Sender: TObject);
Var iSifr:INteger;
begin
  with dmC do
  begin
    try
      fmStopListCash.ViewSl.BeginUpdate;
      iSifr:=0;
      if quMenuSL.RecordCount>0 then iSifr:=quMenuSLSIFR.AsInteger;


      quMenuSL.Active:=False;
      quMenuSL.Active:=True;

      if iSifr>0 then quMenuSL.Locate('SIFR',iSifr,[]);

      StatusBar1.Panels[0].Text:=FormatDateTime('hh:nn:ss',now);
    finally
      fmStopListCash.ViewSl.EndUpdate;
    end;
  end;
end;

procedure TfmStopListCash.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  TimerSl.Enabled:=False;
end;

procedure TfmStopListCash.FormShow(Sender: TObject);
begin
  TimerSl.Enabled:=True;
end;

procedure TfmStopListCash.ViewSlCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  i:Integer;
  sVal:String;
  rVal:Real;
begin
//  if AViewInfo.GridRecord.Selected then exit;

  for i:=0 to ViewSl.ColumnCount-1 do
  begin
    if ViewSl.Columns[i].Name='ViewSlBACKBGR' then
    begin
      sVal := VarAsType(AViewInfo.GridRecord.DisplayTexts[i], varString);
      rVal:=StrToFloatDef(sVal,0);

      if rVal<=0 then
      begin
        ACanvas.Canvas.Brush.Color:=$00DDBBFF;
      end
      else
      begin
      //  ACanvas.Canvas.Brush.Bitmap := ABitmap;
      end;
    end;
  end;
end;

end.
