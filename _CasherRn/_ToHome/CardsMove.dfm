object fmCardsMove: TfmCardsMove
  Left = 285
  Top = 242
  Width = 805
  Height = 408
  Caption = #1044#1074#1080#1078#1077#1085#1080#1077' '#1090#1086#1074#1072#1088#1072
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Edit1: TEdit
    Left = 152
    Top = 232
    Width = 137
    Height = 21
    TabOrder = 2
    Text = 'Edit1'
  end
  object PageControl1: TPageControl
    Left = 80
    Top = 8
    Width = 657
    Height = 349
    ActivePage = TabSheet4
    Style = tsFlatButtons
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = #1055#1088#1080#1093#1086#1076' '#1087#1086' '#1087#1072#1088#1090#1080#1103#1084
      object GridPartIn: TcxGrid
        Left = 0
        Top = 0
        Width = 649
        Height = 318
        Align = alClient
        TabOrder = 0
        LookAndFeel.Kind = lfOffice11
        object ViewPartIn: TcxGridDBTableView
          OnDblClick = ViewPartInDblClick
          NavigatorButtons.ConfirmDelete = False
          DataController.DataSource = dmO.dsquCPartIn
          DataController.Summary.DefaultGroupSummaryItems = <
            item
              Format = '0.000'
              Kind = skSum
              Position = spFooter
              FieldName = 'QPART'
              Column = ViewPartInQPART
            end
            item
              Format = '0.000'
              Kind = skSum
              Position = spFooter
              FieldName = 'QREMN'
              Column = ViewPartInQREMN
            end
            item
              Format = '0.00'
              Kind = skSum
              Position = spFooter
              FieldName = 'SUMIN'
              Column = ViewPartInSUMIN
            end
            item
              Format = '0.00'
              Kind = skSum
              Position = spFooter
              FieldName = 'SUMREMN'
              Column = ViewPartInSUMREMN
            end>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = '0.000'
              Kind = skSum
              FieldName = 'QPART'
              Column = ViewPartInQPART
            end
            item
              Format = '0.000'
              Kind = skSum
              FieldName = 'QREMN'
              Column = ViewPartInQREMN
            end
            item
              Format = '0.00'
              Kind = skSum
              FieldName = 'SUMIN'
              Column = ViewPartInSUMIN
            end
            item
              Format = '0.00'
              Kind = skSum
              FieldName = 'SUMREMN'
              Column = ViewPartInSUMREMN
            end>
          DataController.Summary.SummaryGroups = <>
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsSelection.MultiSelect = True
          OptionsView.Footer = True
          OptionsView.GroupFooters = gfAlwaysVisible
          OptionsView.Indicator = True
          object ViewPartInARTICUL: TcxGridDBColumn
            Caption = #1040#1088#1090#1080#1082#1091#1083
            DataBinding.FieldName = 'ARTICUL'
            Width = 48
          end
          object ViewPartInID: TcxGridDBColumn
            Caption = #1050#1086#1076' '#1087#1072#1088#1090#1080#1080
            DataBinding.FieldName = 'ID'
          end
          object ViewPartInNAMEMH: TcxGridDBColumn
            Caption = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103
            DataBinding.FieldName = 'NAMEMH'
            Width = 150
          end
          object ViewPartInIDDOC: TcxGridDBColumn
            Caption = #1050#1086#1076' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
            DataBinding.FieldName = 'IDDOC'
            Visible = False
          end
          object ViewPartInNUMDOC: TcxGridDBColumn
            Caption = #1053#1086#1084#1077#1088' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
            DataBinding.FieldName = 'NUMDOC'
          end
          object ViewPartInIDATE: TcxGridDBColumn
            Caption = #1044#1072#1090#1072
            DataBinding.FieldName = 'IDATE'
            PropertiesClassName = 'TcxDateEditProperties'
          end
          object ViewPartInIDCLI: TcxGridDBColumn
            Caption = #1050#1086#1076' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
            DataBinding.FieldName = 'IDCLI'
            Visible = False
          end
          object ViewPartInNAMECL: TcxGridDBColumn
            Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
            DataBinding.FieldName = 'NAMECL'
            Width = 149
          end
          object ViewPartInDTYPE: TcxGridDBColumn
            Caption = #1058#1080#1087' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
            DataBinding.FieldName = 'DTYPE'
            PropertiesClassName = 'TcxImageComboBoxProperties'
            Properties.Alignment.Horz = taLeftJustify
            Properties.Items = <
              item
                Description = #1055#1088#1080#1093#1086#1076
                ImageIndex = 0
                Value = 1
              end
              item
                Description = #1048#1085#1074#1077#1085#1090#1072#1088#1080#1079#1072#1094#1080#1103
                Value = 3
              end
              item
                Description = #1042#1085'.'#1087#1077#1088#1077#1084#1077#1097#1077#1085#1080#1077
                Value = 4
              end
              item
                Description = #1040#1082#1090' '#1087#1077#1088#1077#1088#1072#1073#1086#1090#1082#1080
                Value = 5
              end
              item
                Description = #1050#1086#1084#1087#1083#1077#1082#1090#1072#1094#1080#1103
                Value = 6
              end
              item
                Description = #1056#1077#1072#1083#1080#1079#1072#1094#1080#1103' '#1085#1072' '#1089#1090#1086#1088#1086#1085#1091
                Value = 8
              end>
          end
          object ViewPartInQPART: TcxGridDBColumn
            Caption = #1050#1086#1083'-'#1074#1086' '#1087#1072#1088#1090#1080#1080
            DataBinding.FieldName = 'QPART'
            Styles.Content = dmO.cxStyle25
          end
          object ViewPartInQREMN: TcxGridDBColumn
            Caption = #1054#1089#1090#1072#1090#1086#1082' '#1087#1072#1088#1090#1080#1080
            DataBinding.FieldName = 'QREMN'
            Styles.Content = dmO.cxStyle25
          end
          object ViewPartInPRICEIN: TcxGridDBColumn
            Caption = #1062#1077#1085#1072' '#1079#1072#1082#1091#1087'. c '#1053#1044#1057
            DataBinding.FieldName = 'PRICEIN'
            Styles.Content = dmO.cxStyle25
          end
          object ViewPartInPRICEOUT: TcxGridDBColumn
            Caption = #1062#1077#1085#1072' '#1091#1095#1077#1090#1085'.'
            DataBinding.FieldName = 'PRICEOUT'
          end
          object ViewPartInSUMIN: TcxGridDBColumn
            Caption = #1057#1091#1084#1084#1072' '#1079#1072#1082#1091#1087'. c '#1053#1044#1057
            DataBinding.FieldName = 'SUMIN'
            Styles.Content = dmO.cxStyle15
          end
          object ViewPartInSUMREMN: TcxGridDBColumn
            Caption = #1057#1091#1084#1084#1072' '#1086#1089#1090#1072#1090#1082#1072' '#1074' '#1079#1072#1082#1091#1087'. '#1094#1077#1085#1072#1093
            DataBinding.FieldName = 'SUMREMN'
            Styles.Content = dmO.cxStyle15
          end
          object ViewPartInPRICEIN0: TcxGridDBColumn
            Caption = #1062#1077#1085#1072' '#1079#1072#1082#1091#1087'. '#1073#1077#1079' '#1053#1044#1057
            DataBinding.FieldName = 'PRICEIN0'
          end
          object ViewPartInSUMIN0: TcxGridDBColumn
            Caption = #1057#1091#1084#1084#1072' '#1079#1072#1082#1091#1087'. '#1073#1077#1079' '#1053#1044#1057
            DataBinding.FieldName = 'SUMIN0'
          end
        end
        object LevelPartIn: TcxGridLevel
          GridView = ViewPartIn
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = #1056#1072#1089#1093#1086#1076' '#1087#1086' '#1087#1072#1088#1090#1080#1103#1084
      ImageIndex = 1
      object GridPartOut: TcxGrid
        Left = 0
        Top = 0
        Width = 649
        Height = 318
        Align = alClient
        TabOrder = 0
        LookAndFeel.Kind = lfOffice11
        object ViewPartOut: TcxGridDBTableView
          OnDblClick = ViewPartOutDblClick
          NavigatorButtons.ConfirmDelete = False
          DataController.DataSource = dmO.dsquCPartOut
          DataController.Summary.DefaultGroupSummaryItems = <
            item
              Format = '0.000'
              Kind = skSum
              Position = spFooter
              FieldName = 'QUANT'
              Column = ViewPartOutQUANT
            end
            item
              Format = '0.00'
              Kind = skSum
              Position = spFooter
              FieldName = 'SUMOUT'
              Column = ViewPartOutSUMIN
            end>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = '0.000'
              Kind = skSum
              FieldName = 'QUANT'
              Column = ViewPartOutQUANT
            end
            item
              Format = '0.00'
              Kind = skSum
              FieldName = 'SUMOUT'
              Column = ViewPartOutSUMIN
            end>
          DataController.Summary.SummaryGroups = <>
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsSelection.MultiSelect = True
          OptionsView.Footer = True
          OptionsView.GroupFooters = gfAlwaysVisible
          OptionsView.Indicator = True
          object ViewPartOutARTICUL: TcxGridDBColumn
            Caption = #1040#1088#1090#1080#1082#1091#1083
            DataBinding.FieldName = 'ARTICUL'
          end
          object ViewPartOutIDDATE: TcxGridDBColumn
            Caption = #1044#1072#1090#1072
            DataBinding.FieldName = 'IDDATE'
            PropertiesClassName = 'TcxDateEditProperties'
          end
          object ViewPartOutIDSTORE: TcxGridDBColumn
            Caption = #1050#1086#1076' '#1052#1061
            DataBinding.FieldName = 'IDSTORE'
            Visible = False
          end
          object ViewPartOutNAMEMH: TcxGridDBColumn
            Caption = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103
            DataBinding.FieldName = 'NAMEMH'
            Width = 103
          end
          object ViewPartOutIDPARTIN: TcxGridDBColumn
            Caption = #1050#1086#1076' '#1087#1088#1080#1093#1086#1076#1085#1086#1081' '#1087#1072#1088#1090#1080#1080
            DataBinding.FieldName = 'IDPARTIN'
          end
          object ViewPartOutIDDOC: TcxGridDBColumn
            Caption = #1050#1086#1076' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
            DataBinding.FieldName = 'IDDOC'
            Visible = False
          end
          object ViewPartOutNUMDOC: TcxGridDBColumn
            Caption = #1053#1086#1084#1077#1088' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
            DataBinding.FieldName = 'NUMDOC'
          end
          object ViewPartOutIDCLI: TcxGridDBColumn
            Caption = #1050#1086#1076' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
            DataBinding.FieldName = 'IDCLI'
            Visible = False
          end
          object ViewPartOutNAMECL: TcxGridDBColumn
            Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
            DataBinding.FieldName = 'NAMECL'
            Width = 148
          end
          object ViewPartOutDTYPE: TcxGridDBColumn
            Caption = #1058#1080#1087' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
            DataBinding.FieldName = 'DTYPE'
            PropertiesClassName = 'TcxImageComboBoxProperties'
            Properties.Items = <
              item
                Description = #1055#1088#1080#1093#1086#1076
                ImageIndex = 0
                Value = 1
              end
              item
                Description = #1056#1072#1089#1093#1086#1076
                Value = 2
              end
              item
                Description = #1048#1085#1074#1077#1085#1090#1072#1088#1080#1079#1072#1094#1080#1103
                Value = 3
              end
              item
                Description = #1042#1085'. '#1087#1077#1088#1077#1084#1077#1097#1077#1085#1080#1077
                Value = 4
              end
              item
                Description = #1040#1082#1090' '#1087#1077#1088#1077#1088#1072#1073#1086#1090#1082#1080
                Value = 5
              end
              item
                Description = #1050#1086#1084#1087#1083#1077#1082#1090#1072#1094#1080#1103
                Value = 6
              end
              item
                Description = #1042#1086#1079#1074#1088#1072#1090
                Value = 7
              end
              item
                Description = #1056#1077#1072#1083#1080#1079#1072#1094#1080#1103' '#1085#1072' '#1089#1090#1086#1088#1086#1085#1091
                Value = 8
              end
              item
                Description = #1057#1087#1080#1089#1072#1085#1080#1077
                Value = 9
              end>
          end
          object ViewPartOutQUANT: TcxGridDBColumn
            Caption = #1050#1086#1083'-'#1074#1086
            DataBinding.FieldName = 'QUANT'
            Styles.Content = dmO.cxStyle25
          end
          object ViewPartOutPRICEIN: TcxGridDBColumn
            Caption = #1062#1077#1085#1072' '#1079#1072#1082#1091#1087'.'
            DataBinding.FieldName = 'PRICEIN'
          end
          object ViewPartOutSUMIN: TcxGridDBColumn
            Caption = #1057#1091#1084#1084#1072' '#1079#1072#1082#1091#1087'.'
            DataBinding.FieldName = 'SUMIN'
            Styles.Content = dmO.cxStyle25
          end
        end
        object LevelPartOut: TcxGridLevel
          GridView = ViewPartOut
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = #1044#1074#1080#1078#1077#1085#1080#1077
      ImageIndex = 2
      object GridMove: TcxGrid
        Left = 0
        Top = 0
        Width = 649
        Height = 318
        Align = alClient
        TabOrder = 0
        LookAndFeel.Kind = lfOffice11
        object ViewMove: TcxGridDBTableView
          NavigatorButtons.ConfirmDelete = False
          DataController.DataSource = dmORep.dstaCMove
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = '0.000'
              Kind = skSum
              FieldName = 'POSTIN'
              Column = ViewMovePOSTIN
            end
            item
              Format = '0.000'
              Kind = skSum
              FieldName = 'POSTOUT'
              Column = ViewMovePOSTOUT
            end
            item
              Format = '0.000'
              Kind = skSum
              FieldName = 'VNIN'
              Column = ViewMoveVNIN
            end
            item
              Format = '0.000'
              Kind = skSum
              FieldName = 'VNOUT'
              Column = ViewMoveVNOUT
            end
            item
              Format = '0.000'
              Kind = skSum
              FieldName = 'INV'
              Column = ViewMoveINV
            end
            item
              Format = '0.000'
              Kind = skSum
              FieldName = 'QREAL'
              Column = ViewMoveQREAL
            end>
          DataController.Summary.SummaryGroups = <>
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsSelection.MultiSelect = True
          OptionsView.Footer = True
          OptionsView.GroupFooters = gfAlwaysVisible
          OptionsView.Indicator = True
          object ViewMoveARTICUL: TcxGridDBColumn
            Caption = #1040#1088#1090#1080#1082#1091#1083
            DataBinding.FieldName = 'ARTICUL'
          end
          object ViewMoveIDATE: TcxGridDBColumn
            Caption = #1044#1072#1090#1072
            DataBinding.FieldName = 'IDATE'
            PropertiesClassName = 'TcxDateEditProperties'
          end
          object ViewMoveIDSTORE: TcxGridDBColumn
            Caption = #1050#1086#1076' '#1052#1061
            DataBinding.FieldName = 'IDSTORE'
            Visible = False
          end
          object ViewMoveNAMEMH: TcxGridDBColumn
            Caption = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103
            DataBinding.FieldName = 'NAMEMH'
            Width = 146
          end
          object ViewMoveRB: TcxGridDBColumn
            Caption = #1054#1089#1090#1072#1090#1086#1082' '#1085#1072' '#1085#1072#1095#1072#1083#1086
            DataBinding.FieldName = 'RB'
            Width = 95
          end
          object ViewMovePOSTIN: TcxGridDBColumn
            Caption = #1055#1088#1080#1093#1086#1076
            DataBinding.FieldName = 'POSTIN'
          end
          object ViewMovePOSTOUT: TcxGridDBColumn
            Caption = #1042#1086#1079#1074#1088#1072#1090
            DataBinding.FieldName = 'POSTOUT'
          end
          object ViewMoveVNIN: TcxGridDBColumn
            Caption = #1042#1085'. '#1087#1088#1080#1093#1086#1076
            DataBinding.FieldName = 'VNIN'
          end
          object ViewMoveVNOUT: TcxGridDBColumn
            Caption = #1042#1085'. '#1088#1072#1089#1093#1086#1076
            DataBinding.FieldName = 'VNOUT'
          end
          object ViewMoveINV: TcxGridDBColumn
            Caption = #1048#1085#1074#1077#1085#1090#1072#1088#1080#1079#1072#1094#1080#1103
            DataBinding.FieldName = 'INV'
          end
          object ViewMoveQREAL: TcxGridDBColumn
            Caption = #1056#1072#1089#1093#1086#1076
            DataBinding.FieldName = 'QREAL'
          end
          object ViewMoveRE: TcxGridDBColumn
            Caption = #1054#1089#1090#1072#1090#1086#1082' '#1085#1072' '#1082#1086#1085#1077#1094
            DataBinding.FieldName = 'RE'
            Styles.Content = dmO.cxStyle25
          end
        end
        object LevelMove: TcxGridLevel
          GridView = ViewMove
        end
      end
    end
    object TabSheet4: TTabSheet
      Caption = #1056#1077#1072#1083#1080#1079#1072#1094#1080#1103
      ImageIndex = 3
      object GridRealC: TcxGrid
        Left = 0
        Top = 0
        Width = 649
        Height = 318
        Align = alClient
        TabOrder = 0
        LookAndFeel.Kind = lfOffice11
        object ViewRealC: TcxGridDBTableView
          NavigatorButtons.ConfirmDelete = False
          DataController.DataSource = dmO.dsquCReal
          DataController.Summary.DefaultGroupSummaryItems = <
            item
              Format = '0.000'
              Kind = skSum
              Position = spFooter
              FieldName = 'QUANTC'
              Column = ViewRealCQUANTC
            end
            item
              Format = '0.00'
              Kind = skSum
              Position = spFooter
              FieldName = 'SUMIN'
              Column = ViewRealCSUMIN
            end>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = '0.000'
              Kind = skSum
              FieldName = 'QUANTC'
              Column = ViewRealCQUANTC
            end
            item
              Format = '0.00'
              Kind = skSum
              FieldName = 'SUMIN'
              Column = ViewRealCSUMIN
            end>
          DataController.Summary.SummaryGroups = <>
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsSelection.MultiSelect = True
          OptionsView.Footer = True
          OptionsView.GroupFooters = gfAlwaysVisible
          OptionsView.Indicator = True
          object ViewRealCDATEDOC: TcxGridDBColumn
            Caption = #1044#1072#1090#1072
            DataBinding.FieldName = 'DATEDOC'
          end
          object ViewRealCCODEB: TcxGridDBColumn
            Caption = #1050#1086#1076' '#1073#1083#1102#1076#1072
            DataBinding.FieldName = 'CODEB'
          end
          object ViewRealCNAMEB: TcxGridDBColumn
            Caption = #1041#1083#1102#1076#1086
            DataBinding.FieldName = 'NAMEB'
            Styles.Content = dmO.cxStyle25
            Width = 154
          end
          object ViewRealCQUANT: TcxGridDBColumn
            Caption = #1050#1086#1083'-'#1074#1086' '#1073#1083#1102#1076#1072
            DataBinding.FieldName = 'QUANT'
          end
          object ViewRealCPRICEOUT: TcxGridDBColumn
            Caption = #1062#1077#1085#1072' '#1073#1083#1102#1076#1072
            DataBinding.FieldName = 'PRICEOUT'
          end
          object ViewRealCSUMOUT: TcxGridDBColumn
            Caption = #1057#1091#1084#1084#1072' '#1073#1083#1102#1076#1072
            DataBinding.FieldName = 'SUMOUT'
          end
          object ViewRealCQUANTC: TcxGridDBColumn
            Caption = #1050#1086#1083'-'#1074#1086
            DataBinding.FieldName = 'QUANTC'
            Styles.Content = dmO.cxStyle1
          end
          object ViewRealCSM: TcxGridDBColumn
            Caption = #1045#1076'.'#1080#1079#1084'.'
            DataBinding.FieldName = 'SM'
            Width = 49
          end
          object ViewRealCPRICEIN: TcxGridDBColumn
            Caption = #1062#1077#1085#1072' '#1079#1072#1082#1091#1087'.'
            DataBinding.FieldName = 'PRICEIN'
          end
          object ViewRealCSUMIN: TcxGridDBColumn
            Caption = #1057#1091#1084#1084#1072' '#1079#1072#1082#1091#1087'.'
            DataBinding.FieldName = 'SUMIN'
            Styles.Content = dmO.cxStyle15
          end
          object ViewRealCQB: TcxGridDBColumn
            Caption = #1050#1086#1083'-'#1074#1086' '#1085#1072' 1-'#1091' '#1073#1083#1102#1076#1072
            DataBinding.FieldName = 'QB'
          end
        end
        object ViewRealB: TcxGridDBTableView
          NavigatorButtons.ConfirmDelete = False
          DataController.DataSource = dmO.dsquBReal
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = '0.000'
              Kind = skSum
              FieldName = 'QUANT'
              Column = ViewRealBQUANT
            end
            item
              Format = '0.00'
              Kind = skSum
              FieldName = 'SUMIN'
              Column = ViewRealBSUMIN
            end
            item
              Format = '0.00'
              Kind = skSum
              FieldName = 'SUMOUT'
              Column = ViewRealBSUMOUT
            end>
          DataController.Summary.SummaryGroups = <>
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.Footer = True
          OptionsView.GroupFooters = gfAlwaysVisible
          object ViewRealBDATEDOC: TcxGridDBColumn
            Caption = #1044#1072#1090#1072
            DataBinding.FieldName = 'DATEDOC'
          end
          object ViewRealBNUMDOC: TcxGridDBColumn
            Caption = #1053#1086#1084#1077#1088' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
            DataBinding.FieldName = 'NUMDOC'
          end
          object ViewRealBCODEB: TcxGridDBColumn
            Caption = #1050#1086#1076' '#1073#1083#1102#1076#1072
            DataBinding.FieldName = 'CODEB'
          end
          object ViewRealBNAMEB: TcxGridDBColumn
            Caption = #1053#1072#1079#1074#1072#1085#1080#1077' '#1073#1083#1102#1076#1072
            DataBinding.FieldName = 'NAMEB'
            Styles.Content = dmO.cxStyle25
            Width = 300
          end
          object ViewRealBQUANT: TcxGridDBColumn
            Caption = #1050#1086#1083'-'#1074#1086
            DataBinding.FieldName = 'QUANT'
            Styles.Content = dmO.cxStyle1
          end
          object ViewRealBPRICEOUT: TcxGridDBColumn
            Caption = #1062#1077#1085#1072' '#1087#1088#1086#1076#1072#1078#1080
            DataBinding.FieldName = 'PRICEOUT'
          end
          object ViewRealBSUMOUT: TcxGridDBColumn
            Caption = #1057#1091#1084#1084#1072' '#1087#1088#1086#1076#1072#1078#1085#1072#1103
            DataBinding.FieldName = 'SUMOUT'
            Styles.Content = dmO.cxStyle25
          end
          object ViewRealBSUMIN: TcxGridDBColumn
            Caption = #1057#1091#1084#1084#1072' '#1079#1072#1082#1091#1087#1086#1095#1085#1072#1103
            DataBinding.FieldName = 'SUMIN'
            Styles.Content = dmO.cxStyle4
          end
          object ViewRealBRNAC: TcxGridDBColumn
            Caption = '% '#1085#1072#1094'.'
            DataBinding.FieldName = 'RNAC'
            Styles.Content = dmO.cxStyle12
          end
        end
        object LevelRealC: TcxGridLevel
          GridView = ViewRealC
        end
        object LevelRealB: TcxGridLevel
          GridView = ViewRealB
        end
      end
    end
    object TabSheet5: TTabSheet
      Caption = #1050#1072#1088#1090#1086#1095#1082#1072' '#1090#1086#1074#1072#1088#1072
      ImageIndex = 4
      object GrCardM: TcxGrid
        Left = 0
        Top = 0
        Width = 649
        Height = 318
        Align = alClient
        TabOrder = 0
        LookAndFeel.Kind = lfOffice11
        object ViCardM: TcxGridDBTableView
          OnDblClick = ViCardMDblClick
          NavigatorButtons.ConfirmDelete = False
          OnCustomDrawCell = ViCardMCustomDrawCell
          DataController.DataSource = dmORep.dstaCardM
          DataController.Summary.DefaultGroupSummaryItems = <
            item
              Format = '0.000'
              Kind = skSum
              Position = spFooter
              FieldName = 'Quant'
              Column = ViCardMQuant
            end>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = '0.000'
              Kind = skSum
              FieldName = 'Quant'
              Column = ViCardMQuant
            end>
          DataController.Summary.SummaryGroups = <>
          OptionsCustomize.ColumnSorting = False
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsSelection.InvertSelect = False
          OptionsSelection.MultiSelect = True
          OptionsView.Footer = True
          OptionsView.GroupFooters = gfAlwaysVisible
          OptionsView.Indicator = True
          object ViCardMRecId: TcxGridDBColumn
            Caption = #8470#1087#1087
            DataBinding.FieldName = 'RecId'
            Visible = False
          end
          object ViCardMTypeD: TcxGridDBColumn
            Caption = #1058#1080#1087' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
            DataBinding.FieldName = 'TypeD'
            PropertiesClassName = 'TcxImageComboBoxProperties'
            Properties.Items = <
              item
                Description = #1055#1088#1080#1093'.'
                ImageIndex = 0
                Value = 1
              end
              item
                Description = #1056#1072#1089#1093#1086#1076'.'
                Value = 2
              end
              item
                Description = #1048#1085#1074#1077#1085#1090'.'
                Value = 3
              end
              item
                Description = #1042#1085'.'#1087#1077#1088#1077#1084'.'
                Value = 4
              end
              item
                Description = #1040#1082#1090' '#1087#1077#1088#1077#1088#1072#1073'.'
                Value = 5
              end
              item
                Description = #1050#1086#1084#1087#1083#1077#1082#1090#1072#1094#1080#1103
                Value = 6
              end
              item
                Description = #1042#1086#1079#1074#1088#1072#1090
                Value = 7
              end
              item
                Description = #1056#1077#1072#1083#1080#1079'. '#1085#1072' '#1089#1090#1086#1088#1086#1085#1091
                Value = 8
              end>
            Width = 55
          end
          object ViCardMDateD: TcxGridDBColumn
            Caption = #1044#1072#1090#1072
            DataBinding.FieldName = 'DateD'
          end
          object ViCardMsFrom: TcxGridDBColumn
            Caption = #1054#1090#1087#1088#1072#1074#1080#1090#1077#1083#1100
            DataBinding.FieldName = 'sFrom'
            Width = 127
          end
          object ViCardMsTo: TcxGridDBColumn
            Caption = #1055#1086#1083#1091#1095#1072#1090#1077#1083#1100
            DataBinding.FieldName = 'sTo'
            Width = 149
          end
          object ViCardMsM: TcxGridDBColumn
            Caption = #1077#1076'.'#1080#1079#1084'.'
            DataBinding.FieldName = 'sM'
            Width = 55
          end
          object ViCardMQuant: TcxGridDBColumn
            Caption = #1050#1086#1083'-'#1074#1086
            DataBinding.FieldName = 'Quant'
            Styles.Content = dmO.cxStyle25
          end
          object ViCardMQRemn: TcxGridDBColumn
            Caption = #1054#1089#1090#1072#1090#1086#1082
            DataBinding.FieldName = 'QRemn'
            Styles.Content = dmO.cxStyle25
          end
          object ViCardMComment: TcxGridDBColumn
            Caption = #1050#1086#1084#1077#1085#1090#1072#1088#1080#1081
            DataBinding.FieldName = 'Comment'
            Width = 118
          end
        end
        object LevelCardM: TcxGridLevel
          GridView = ViCardM
        end
      end
    end
  end
  object SpeedBar1: TSpeedBar
    Left = 0
    Top = 0
    Width = 66
    Height = 374
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Align = alLeft
    Options = [sbAllowDrag, sbFlatBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 60
    BtnHeight = 40
    Color = 11206570
    TabOrder = 1
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object SpeedItem1: TSpeedItem
      Action = acPeriod
      BtnCaption = #1055#1077#1088#1080#1086#1076
      Caption = #1055#1077#1088#1080#1086#1076
      Glyph.Data = {
        BA030000424DBA030000000000003600000028000000140000000F0000000100
        18000000000084030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00800000800000
        8000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFF008000008000008000FFFFFFFFFFFF008000000000000000FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFF000000000000008000FFFFFFFFFFFF008000000000FFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FF000000008000FFFFFFFFFFFF008000000000FFFFFFFFFFFFFFFFFF000000FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFF000000
        008000FFFFFFFFFFFF008000000000FFFFFFFFFFFF000000008000FFFFFFFFFF
        FF000000000000FFFFFFFFFFFF008000000000FFFFFFFFFFFF000000008000FF
        FFFFFFFFFF008000000000FFFFFF00000000800000E100000000000000000000
        00000000000000000000E100008000000000FFFFFF000000008000FFFFFFFFFF
        FF00800000000000000000800000E10000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000E100008000000000000000008000FFFFFFFFFFFF008000
        000000FFFFFF00000000800000E1000000000000000000000000000000000000
        0000E100008000000000FFFFFF000000008000FFFFFFFFFFFF008000000000FF
        FFFFFFFFFF000000008000FFFFFFFFFFFF000000000000FFFFFFFFFFFF008000
        000000FFFFFFFFFFFF000000008000FFFFFFFFFFFF008000000000FFFFFFFFFF
        FFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFF
        FFFFFFFFFF000000008000FFFFFFFFFFFF008000000000FFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FF000000008000FFFFFFFFFFFF008000000000000000FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000
        008000FFFFFFFFFFFF008000008000008000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF008000008000008000FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      Hint = #1055#1077#1088#1080#1086#1076
      Spacing = 1
      Left = 3
      Top = 11
      Visible = True
      OnClick = acPeriodExecute
      SectionName = 'Untitled (0)'
    end
    object SpeedItem2: TSpeedItem
      Action = acExit
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Glyph.Data = {
        5E040000424D5E04000000000000360000002800000012000000130000000100
        18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
        8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
        0000CED3D6848684848684848684848684848684848684848684848684848684
        848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
        7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
        FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
        FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
        007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
        FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
        00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
        494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
        00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000}
      Hint = #1042#1099#1093#1086#1076
      Spacing = 1
      Left = 3
      Top = 235
      Visible = True
      OnClick = acExitExecute
      SectionName = 'Untitled (0)'
    end
    object SpeedItem3: TSpeedItem
      Action = acSelDoc
      BtnCaption = #1055#1077#1088#1077#1081#1090#1080' '
      Caption = #1055#1077#1088#1077#1081#1090#1080' '
      Glyph.Data = {
        66030000424D6603000000000000360000002800000010000000110000000100
        18000000000030030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFF1A6600FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF1A660037DD001A6600FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFF1A660037DD002BA80037DD001A6600FFFFFFFFFFFFFFFFFFFFFFFFADAAAD
        737173523C29523C29ADAAADE7E7E71A66002BA8002BA8002BA8002BA80037DD
        001A6600FFFFFFFFFFFFFFFFFF949294ADFFFFADFFFFADFFFF9CFBFF1A66002B
        A800E2FFD9E2FFD92BA8002BA8002BA80037DD001A6600FFFFFFFFFFFF949294
        ADFFFFADFFFFADFFFF37DD001A66001A66001A6600E2FFD9E2FFD92BA8001A66
        001A66001A660037DD00FFFFFF9492949CF7FFADFFFFADFFFFADFFFFADFFFF9C
        FBFF1A6600E2FFD9E2FFD9E2FFD91A6600FFFFFFFFFFFFFFFFFFFFFFFFC6C3C6
        9CF7FF9CF7FFADFFFFADFFFFADFFFFADFFFF2BA800E2FFD9E2FFD937DD007BFF
        4FFFFFFFFFFFFFFFFFFFFFFFFFC0C0C09CF7FF9CF7FFADFFFFADFFFFADFFFF2B
        A80037DD0099FF77E2FFD91A6600FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0
        9CF7FF9CF7FF9CFBFFADFFFF9CFBFF2BA8007BFF4F99FF7737DD007BFF4FFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFF7375739CF7FF9CF7FFADFFFF9CFBFF2BA80037
        DD007BFF4F37DD00E7E7E7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF737573
        9CF7FF9CF7FF9CF7FF37DD0037DD0037DD0037DD007BFF4FA5A6A5FFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFF523C299CF7FF9CF7FF9CF7FF9CF7FF9CF7FF9C
        F7FF9CF7FFCED3D6523C29FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF523C29
        9CF7FFCED3D69CF7FFE7E7E79CF7FF9CF7FF9CF7FF9CF7FF523C29FFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFF523C29523C29523C29523C298C8E8C73757373
        7573523C29523C29523C29FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFF}
      Hint = #1055#1077#1088#1077#1081#1090#1080' '#1085#1072' '#1076#1086#1082#1091#1084#1077#1085#1090
      Spacing = 1
      Left = 3
      Top = 107
      Visible = True
      OnClick = acSelDocExecute
      SectionName = 'Untitled (0)'
    end
    object SpeedItem4: TSpeedItem
      Action = acPrint
      Glyph.Data = {
        86070000424D86070000000000003600000028000000180000001A0000000100
        1800000000005007000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0BDB2
        B3B0A7FFFFFFFFFFFFD7D7D7D7D7D7D7D7D7FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFB0AEA8989896838280898883A9A7A0B4B3AA93928D85848096948EAFADA5
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFA1A09BAEAEADC9C9C8ABABAA84848371706D767573A2A2A1A4
        A4A376767472716D93918CACAAA3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFB8B7B0A3A3A0D8D8D8E7E7E7D5D5D5C7C7C7A9A9A98585
        85A0A09FA8A8A8BABABAD7D7D7C3C3C38686856E6D6B8B8A85FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFA5A4A0C6C6C5F6F6F6F8F8F8E2E2E2D2D2D2
        C6C6C6AEAEAE787878777777929292A4A4A4B3B3B3C9C9C9E2E2E2D9D9D9A1A1
        A07D7C7AB0AEA8FFFFFFFFFFFFFFFFFFFFFFFFADADABE7E7E7FDFDFDFEFEFEF4
        F4F4E3E3E3D4D4D4C3C3C3B7B7B79E9E9E888888828282929292AAAAAABDBDBD
        C6C6C6BDBDBDA1A1A181817FBEBDB6FFFFFFFFFFFFFFFFFFBCBBB9FDFDFDFFFF
        FFFFFFFFFFFFFFF3F3F3D7D7D7C9C9C9BEBEBEB6B6B6C4C4C4D0D0D0C6C6C6AB
        ABAB9B9B9B9090905353537E7E7E7C7C7B9B9A96FFFFFFFFFFFFFFFFFFFFFFFF
        BCBCBAFFFFFFFFFFFFFBFBFBE9E9E9D7D7D7D4D4D4D7D7D7D1D1D1C8C8C8C0C0
        C0C3C3C3CECECEDBDBDBD9D9D9BBBBBB8D8D8D9F9D9E878686979692FFFFFFFF
        FFFFFFFFFFFFFFFFBDBDBBFDFDFDE9E9E9D8D8D8DEDEDEE4E4E4E0E0E0DBDBDB
        D6D6D6CFCFCFC9C9C9C2C2C2BBBBBBBABABAC1C1C1C9C9C9C5C1C448FF737D94
        87979692FFFFFFFFFFFFFFFFFFFFFFFFBEBEBBEAEAEAE5E5E5EEEEEEEBEBEBE3
        E3E3E7E7E7F1F1F1EAEAEADCDCDCCFCFCFC6C6C6BFBFBFB6B6B6B0B0B0B2B2B2
        B4B3B3A7B7AE8C9A93979592FFFFFFFFFFFFFFFFFFFFFFFFBFBEBDFEFEFEF7F7
        F7EEEEEEEBEBEBF2F2F2F9F9F9E6E6E6D8D8D8DCDCDCDCDCDCD5D5D5CCCCCCC0
        C0C0B7B7B7B3B3B3B2B2B2B6B2B4B2ADAFA09F9DFFFFFFFFFFFFFFFFFFFFFFFF
        C0C0C0FBFBFBF8F8F8F7F7F7FBFBFBF7F7F7DFDFDFD6D6D6E6E6E6E5E5E5E1E1
        E1DDDDDDD7D7D7D0D0D0C6C6C6BEBEBEB8B8B8B3B3B3B0B0B0B8B7B4FFFFFFFF
        FFFFFFFFFFFFFFFFDCDCDBDCDCDBF7F7F7F4F4F4E5E5E5D2D2D2D1D1D1E0E0E0
        E9E8E8F1F1F1F9F8F8FCFCFCFEFEFEF4F4F4EBEBEBDADADABABABAB3B3B3B1B1
        B0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE0DFDDCFCFCED5D6D7D7
        D8DAD7D9DBCED0D2CED0D2D5D5D7DCDDDDE9E9E9F3F4F4FFFFFFFBFBFBDADADA
        B3B3B3BABAB9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFDDDCDBEEECEBE6E1DADFD9CFD4D0CAC8C7C4BCBCBCB3B4B7B4B6BABEC1C2D3
        D3D3C3C2C2C1C1C0DBDBD9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFE7D5C6F4E5C7F4E0BEF2DDBBECD8B7E4D2B4D9C8
        B0CCBDABAFA9A6B2B2B2E0DFDDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2D7C3FFF5D3FFEBC7FFE9C1
        FFE6B8FFE3B1FFE3AFFED9AAAD9B95D7D7D5FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE5E1E0F7E1CEFF
        F4D9FFECCDFFEAC7FFE7C0FFE4B8FFE6B4FCD9ACA79B98FFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFDED3D0FEF0DFFFF4DCFFEFD3FFECCDFFE9C6FFE6C0FFEABBF3D2ADB1A8A6FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFE7D5D0FFFBECFFF4E1FFF1DBFFEED4FFEBCDFFE9C6FFEE
        C1DBBEA6DCDADAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFE6E3E3F7EBE5FFFEF4FFF5E7FFF3E1FFF1DA
        FFEED3FFEDCDFFEFC8C0A89EDCDADAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE4D9D9FFFEFDFFFEF9FF
        F9EFFFF6E8FFF3E1FFF0D9FFF4D6FAE5C8C2B5B3F3F3F3FFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEAD7
        D7FFFEFEFFFFFEFFFEF8FFFDF2FFFBECFFFCE9FFFBE0D9C2B9E9E5E5FFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFEFEEEAF5EEEEF1E8E8EDE1E1EAD9D8E5D1CEE5CCC7E3CBC6FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFF}
      Hint = '|'
      Spacing = 1
      Left = 3
      Top = 67
      Visible = True
      OnClick = acPrintExecute
      SectionName = 'Untitled (0)'
    end
    object SpeedItem5: TSpeedItem
      Action = acExportXl
      Glyph.Data = {
        56080000424D560800000000000036000000280000001A0000001A0000000100
        18000000000020080000C40E0000C40E00000000000000000000C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0004000004000004000004000C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C00000C0C0C0004000
        80808080808080808080808080808080808080808000400040E02040E020FFFF
        FFC8D0D4A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0A4A0A0808080C0
        C0C0C0C0C0C0C0C00000C0C0C0C0C0C000400040E02000C02000C02000C02000
        C02000400040E02040E020FFFFFF004000C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4C8D0D4A4A0A0A4A0A0404040C0C0C0C0C0C00000C0C0C0C0C0C0
        C0C0C000400000C02000C02000C02000400000C02040E020FFFFFF00400000C0
        20C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4A4A0A0A4A0A040
        2020808080C0C0C00000C0C0C0C0C0C0C0C0C0A4A0A000400000C02000400000
        C02040E020FFFFFF004000004000004000C8D0D4A4A0A0A4A0A0A4A0A0A4A0A0
        A4A0A0A4A0A0808080C8D0D4808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C000400040E02040E020FFFFFF004000F0FBFFF0FBFFF0FB
        FFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFA4A0A0C8D0D4C8D0D440
        2020808080C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C000400040E02040E020FF
        FFFF004000808080004000C8D0D4C0DCC0C0DCC0C0DCC0C0DCC0C0DCC0C0DCC0
        C0DCC0C8D0D4F0FBFFA4A0A0C8D0D4402020808080C0C0C00000C0C0C0C0C0C0
        C0C0C000400000C02040E020FFFFFF00400080E0A000C020808080004000FFFF
        FFFFFFFFFFFFFFF0FBFFC0DCC0C0DCC0F0FBFFC0DCC0F0FBFF808080C8D0D440
        2020808080C0C0C00000C0C0C0C0C0C000400000C02040E020FFFFFF004000FF
        FFFF00400000C02000C020808080004000FFFFFFFFFFFFC8D0D4F0FBFFC0DCC0
        C0DCC0C0DCC0F0FBFFC0DCC0A4A0A0404020808080C0C0C00000C0C0C0004000
        004000004000004000FFFFFFFFFFFFFFFFFFFFFFFF0040000040000040000040
        00C8D0D4F0FBFFC0DCC0C8D0D4C8D0D4F0FBFFC0DCC0F0FBFFFFFFFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFC0DCC0FFFFFFF0FBFFFFFFFFFFFFFFF0FBFFF0FBFFF0FBFF
        FFFFFFC0DCC0F0FBFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0F0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FB
        FFF0FBFFFFFFFFFFFFFFF0FBFFF0FBFFFFFFFFC0DCC0F0FBFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0F0
        FBFFF0FBFFF0FBFFC0DCC0C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0D4
        C8D0D4C8D0D4F0FBFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0C0DCC0C0DCC0F0FBFFF0FBFFC0DCC0FFFF
        FFC0DCC0F0FBFFC0DCC0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0C0
        DCC0F0FBFFF0FBFFF0FBFFF0FBFFFFFFFFF0FBFFF0FBFFC0DCC0F0FBFFFFFFFF
        FFFFFFFFFFFFFFFFFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFC0DCC0C0DCC0C8D0D4F0FBFFF0FBFFC8D0D4FFFF
        FFC8D0D4F0FBFFC0DCC0F0FBFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFC0DCC0F0
        FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFC8D0D4F0FBFFFFFFFF
        FFFFFFFFFFFFFFFFFFF0FBFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080FFFFFFFFFFFFF0FBFFC8D0D4C8D0D4C8D0D4C8D0D4C8D0D4C8D0
        D4C8D0D4C8D0D4C8D0D4F0FBFFFFFFFFFFFFFFFFFFFFFFFFFFF0FBFF80808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080FFFFFFFFFFFFF0FBFFF0
        FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF808080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C0808080C0DCC0C8D0D4C8D0D4F0FBFFC8D0D4C0DCC0C0DCC0C8D0D4F0FB
        FFC8D0D4C0DCC0F0FBFFC8D0D4F0FBFFC8D0D4C8D0D4F0FBFFA4A0A080808040
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C080808080E0E040E0E040E0E080
        E0E040E0E080E0E080E0E040E0E080E0E040E0E040E0E080E0E040E0E080E0E0
        40E0E040E0E080E0E000E0E0408080404040808080C0C0C00000C0C0C0C0C0C0
        C0C0C080808080E0E080E0E0C0DCC080E0E0C0DCC080E0E080E0E0C0DCC080E0
        E0C0DCC080E0E080E0E0C0DCC080E0E0C0DCC080E0E080E0E0C0DCC000808000
        4040808080C0C0C00000C0C0C0C0C0C0C0C0C0808080F0FBFFF0FBFFF0FBFF80
        E0E0F0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFFF0FBFF
        F0FBFFF0FBFFF0FBFFF0FBFF00C0C0002040808080C0C0C00000C0C0C0C0C0C0
        C0C0C08080808080808080808080808080808080808080808080808080808080
        8080808080808080808080808080808080808080808080808080808080808000
        2040C0C0C0C0C0C00000C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C00000}
      Hint = #1069#1082#1089#1087#1086#1088#1090' '#1074' Excel'
      Spacing = 1
      Left = 3
      Top = 171
      Visible = True
      OnClick = acExportXlExecute
      SectionName = 'Untitled (0)'
    end
  end
  object amCM: TActionManager
    Left = 340
    Top = 104
    StyleName = 'XP Style'
    object acExit: TAction
      Caption = 'acExit'
      OnExecute = acExitExecute
    end
    object acPeriod: TAction
      Caption = #1055#1077#1088#1080#1086#1076
      OnExecute = acPeriodExecute
    end
    object acSelDoc: TAction
      Caption = #1055#1077#1088#1077#1081#1090#1080' '
      OnExecute = acSelDocExecute
    end
    object acPrint: TAction
      OnExecute = acPrintExecute
    end
    object acExportXl: TAction
      OnExecute = acExportXlExecute
    end
  end
  object FormPlacement1: TFormPlacement
    Active = False
    Left = 480
    Top = 304
  end
  object taCardMoveRep: TClientDataSet
    Aggregates = <>
    FileName = 'CardMoveRep.cds'
    Params = <>
    Left = 284
    Top = 163
    object taCardMoveRepNum: TIntegerField
      FieldName = 'Num'
    end
    object taCardMoveRepsDate: TStringField
      FieldName = 'sDate'
      Size = 10
    end
    object taCardMoveRepDocNum: TStringField
      FieldName = 'DocNum'
    end
    object taCardMoveRepClient: TStringField
      FieldName = 'Client'
      Size = 50
    end
    object taCardMoveRepQuantIn: TFloatField
      FieldName = 'QuantIn'
    end
    object taCardMoveRepSumIn: TFloatField
      FieldName = 'SumIn'
    end
    object taCardMoveRepQuantOut: TFloatField
      FieldName = 'QuantOut'
    end
    object taCardMoveRepSumOut: TFloatField
      FieldName = 'SumOut'
    end
    object taCardMoveRepDocType: TStringField
      FieldName = 'DocType'
    end
  end
  object RepCMove: TfrReport
    InitialZoom = pzDefault
    PreviewButtons = [pbZoom, pbLoad, pbSave, pbPrint, pbFind, pbHelp, pbExit]
    RebuildPrinter = False
    Left = 132
    Top = 115
    ReportForm = {19000000}
  end
  object frtaCardMoveRep: TfrDBDataSet
    DataSet = taCardMoveRep
    Left = 132
    Top = 171
  end
end
