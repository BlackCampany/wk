object fmAddDoc1: TfmAddDoc1
  Left = 222
  Top = 225
  Width = 1058
  Height = 695
  Caption = #1044#1086#1082#1091#1084#1077#1085#1090#1099
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1050
    Height = 129
    Align = alTop
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 1
    object Label1: TLabel
      Left = 24
      Top = 16
      Width = 65
      Height = 13
      Caption = #1044#1086#1082#1091#1084#1077#1085#1090' '#8470
      Transparent = True
    end
    object Label2: TLabel
      Left = 24
      Top = 40
      Width = 33
      Height = 13
      Caption = #1057#1063#1060#1050
      Transparent = True
    end
    object Label3: TLabel
      Left = 248
      Top = 40
      Width = 11
      Height = 13
      Caption = #1086#1090
      Transparent = True
    end
    object Label4: TLabel
      Tag = 1
      Left = 24
      Top = 72
      Width = 58
      Height = 13
      Caption = #1050#1086#1085#1090#1088#1072#1075#1077#1085#1090
      Transparent = True
    end
    object Label5: TLabel
      Left = 24
      Top = 96
      Width = 82
      Height = 13
      Caption = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103
      Transparent = True
    end
    object Label6: TLabel
      Left = 424
      Top = 16
      Width = 110
      Height = 13
      Caption = #1057#1091#1084#1084#1072' '#1074' '#1079#1072#1082#1091#1087'. '#1094#1077#1085#1072#1093
      Transparent = True
      Visible = False
    end
    object Label7: TLabel
      Left = 480
      Top = 64
      Width = 41
      Height = 13
      Caption = #1053#1044#1057' 0%'
      Transparent = True
      Visible = False
    end
    object Label8: TLabel
      Left = 424
      Top = 40
      Width = 118
      Height = 13
      Caption = #1057#1091#1084#1084#1072' '#1074' '#1091#1095#1077#1090#1085#1099#1093' '#1094#1077#1085#1072#1093
      Transparent = True
      Visible = False
    end
    object Label9: TLabel
      Left = 480
      Top = 80
      Width = 47
      Height = 13
      Caption = #1053#1044#1057' 10%'
      Transparent = True
      Visible = False
    end
    object Label10: TLabel
      Left = 480
      Top = 96
      Width = 47
      Height = 13
      Caption = #1053#1044#1057' 18%'
      Transparent = True
      Visible = False
    end
    object Label11: TLabel
      Left = 552
      Top = 64
      Width = 65
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = '1000.00'#1088'.'
      Color = 16384
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 16384
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Transparent = True
      Visible = False
    end
    object Label12: TLabel
      Left = 248
      Top = 16
      Width = 11
      Height = 13
      Caption = #1086#1090
      Transparent = True
    end
    object Label13: TLabel
      Left = 552
      Top = 80
      Width = 65
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = '1000.00'#1088'.'
      Color = 16384
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 16384
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Transparent = True
      Visible = False
    end
    object Label14: TLabel
      Left = 552
      Top = 96
      Width = 65
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = '1000.00'#1088'.'
      Color = 16384
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 16384
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Transparent = True
      Visible = False
    end
    object Label15: TLabel
      Left = 264
      Top = 96
      Width = 201
      Height = 13
      AutoSize = False
      Caption = #1056#1086#1079#1085#1080#1095#1085#1072#1103' '#1094#1077#1085#1072
      Transparent = True
    end
    object cxTextEdit1: TcxTextEdit
      Left = 112
      Top = 12
      Properties.MaxLength = 15
      TabOrder = 0
      Text = 'cxTextEdit1'
      Width = 121
    end
    object cxDateEdit1: TcxDateEdit
      Left = 272
      Top = 12
      Properties.OnChange = cxDateEdit1PropertiesChange
      Style.BorderStyle = ebsOffice11
      TabOrder = 1
      Width = 121
    end
    object cxTextEdit2: TcxTextEdit
      Left = 112
      Top = 36
      Properties.MaxLength = 15
      TabOrder = 2
      Text = 'cxTextEdit2'
      Width = 121
    end
    object cxDateEdit2: TcxDateEdit
      Left = 272
      Top = 36
      Style.BorderStyle = ebsOffice11
      TabOrder = 3
      Width = 121
    end
    object cxButtonEdit1: TcxButtonEdit
      Left = 112
      Top = 68
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
      Style.BorderStyle = ebsOffice11
      TabOrder = 4
      Text = 'cxButtonEdit1'
      OnKeyPress = cxButtonEdit1KeyPress
      Width = 281
    end
    object cxLookupComboBox1: TcxLookupComboBox
      Left = 112
      Top = 92
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Caption = #1053#1072#1079#1074#1072#1085#1080#1077
          FieldName = 'NAMEMH'
        end>
      Properties.ListOptions.AnsiSort = True
      Properties.ListSource = dmO.dsMHAll
      Properties.OnChange = cxLookupComboBox1PropertiesChange
      Style.BorderStyle = ebsOffice11
      Style.LookAndFeel.Kind = lfOffice11
      Style.PopupBorderStyle = epbsDefault
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 5
      Width = 145
    end
    object cxCurrencyEdit1: TcxCurrencyEdit
      Left = 552
      Top = 12
      AutoSize = False
      EditValue = 10000000c
      Properties.Alignment.Horz = taRightJustify
      Properties.NullString = '0'
      Properties.ReadOnly = True
      Properties.UseLeftAlignmentOnEditing = False
      Style.TextColor = clNavy
      Style.TextStyle = [fsBold]
      TabOrder = 6
      Visible = False
      Height = 21
      Width = 105
    end
    object cxCurrencyEdit2: TcxCurrencyEdit
      Left = 552
      Top = 36
      AutoSize = False
      EditValue = '1000'
      Properties.Alignment.Horz = taRightJustify
      Properties.NullString = '0'
      Properties.ReadOnly = True
      Properties.UseLeftAlignmentOnEditing = False
      Style.TextColor = clNavy
      Style.TextStyle = [fsBold]
      TabOrder = 7
      Visible = False
      Height = 21
      Width = 105
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 642
    Width = 1050
    Height = 19
    Color = clWhite
    Panels = <
      item
        Width = 300
      end
      item
        Width = 50
      end>
  end
  object Panel2: TPanel
    Left = 0
    Top = 601
    Width = 1050
    Height = 41
    Align = alBottom
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 2
    object cxButton1: TcxButton
      Left = 32
      Top = 8
      Width = 121
      Height = 25
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100'   Ctrl+S'
      TabOrder = 0
      OnClick = cxButton1Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 208
      Top = 8
      Width = 137
      Height = 25
      Caption = #1042#1099#1093#1086#1076'    F10'
      ModalResult = 2
      TabOrder = 1
      OnClick = cxButton2Click
      Glyph.Data = {
        5E040000424D5E04000000000000360000002800000012000000130000000100
        18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
        8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
        0000CED3D6848684848684848684848684848684848684848684848684848684
        848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
        7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
        FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
        FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
        007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
        FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
        00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
        494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
        00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000}
      LookAndFeel.Kind = lfOffice11
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 129
    Width = 153
    Height = 472
    Align = alLeft
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 3
    object cxLabel1: TcxLabel
      Left = 8
      Top = 32
      Cursor = crHandPoint
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1089#1087#1080#1089#1086#1082' Ctrl+Ins'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = 4227072
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel1Click
    end
    object cxLabel2: TcxLabel
      Left = 8
      Top = 72
      Cursor = crHandPoint
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1087#1086#1079#1080#1094#1080#1102'  F8'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clRed
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel2Click
    end
    object cxLabel3: TcxLabel
      Left = 8
      Top = 136
      Cursor = crHandPoint
      Caption = #1055#1077#1088#1077#1089#1095#1080#1090#1072#1090#1100' '#1094#1077#1085#1099
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clBlack
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel3Click
    end
    object cxLabel4: TcxLabel
      Left = 7
      Top = 160
      Cursor = crHandPoint
      Caption = #1058#1077#1082#1091#1097#1080#1081' '#1086#1089#1090#1072#1090#1086#1082
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clBlack
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel4Click
    end
    object cxLabel5: TcxLabel
      Left = 8
      Top = 16
      Cursor = crHandPoint
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1087#1086#1079#1080#1094#1080#1102' Ins'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = 4227072
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel5Click
    end
    object cxLabel6: TcxLabel
      Left = 8
      Top = 88
      Cursor = crHandPoint
      Caption = #1054#1095#1080#1089#1090#1080#1090#1100' Ctrl+F8'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clRed
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel6Click
    end
  end
  object PageControl1: TPageControl
    Left = 160
    Top = 136
    Width = 837
    Height = 281
    ActivePage = TabSheet1
    Style = tsFlatButtons
    TabOrder = 4
    object TabSheet1: TTabSheet
      Caption = #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1103
      object GridDoc1: TcxGrid
        Left = 0
        Top = 0
        Width = 829
        Height = 250
        Align = alClient
        PopupMenu = PopupMenu1
        TabOrder = 0
        LookAndFeel.Kind = lfOffice11
        object ViewDoc1: TcxGridDBTableView
          OnDragDrop = ViewDoc1DragDrop
          OnDragOver = ViewDoc1DragOver
          NavigatorButtons.ConfirmDelete = False
          OnCustomDrawCell = ViewDoc1CustomDrawCell
          OnEditing = ViewDoc1Editing
          OnEditKeyDown = ViewDoc1EditKeyDown
          OnEditKeyPress = ViewDoc1EditKeyPress
          DataController.DataSource = dsSpec
          DataController.Summary.DefaultGroupSummaryItems = <
            item
              Format = '0.00'
              Kind = skSum
              Position = spFooter
              FieldName = 'RNds'
              Column = ViewDoc1RNds
            end
            item
              Format = '0.00'
              Kind = skSum
              Position = spFooter
              FieldName = 'Sum1'
              Column = ViewDoc1Sum1
            end
            item
              Format = '0.00'
              Kind = skSum
              Position = spFooter
              FieldName = 'Sum2'
              Column = ViewDoc1Sum2
            end
            item
              Format = '0.00'
              Kind = skSum
              Position = spFooter
              FieldName = 'SumNac'
              Column = ViewDoc1SumNac
            end
            item
              Format = '0.00'
              Kind = skSum
              Position = spFooter
              FieldName = 'Sum0'
              Column = ViewDoc1Sum0
            end>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = '0.00'
              Kind = skSum
              FieldName = 'RNds'
              Column = ViewDoc1RNds
            end
            item
              Format = '0.00'
              Kind = skSum
              FieldName = 'Sum1'
              Column = ViewDoc1Sum1
            end
            item
              Format = '0.00'
              Kind = skSum
              FieldName = 'Sum2'
              Column = ViewDoc1Sum2
            end
            item
              Format = '0.00'
              Kind = skSum
              FieldName = 'SumNac'
              Column = ViewDoc1SumNac
            end
            item
              Format = ',0.0%;-,0.0%'
              Kind = skAverage
              FieldName = 'ProcNac'
              Column = ViewDoc1ProcNac
            end
            item
              Format = '0.00'
              Kind = skSum
              FieldName = 'Sum0'
              Column = ViewDoc1Sum0
            end>
          DataController.Summary.SummaryGroups = <>
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsData.Inserting = False
          OptionsSelection.MultiSelect = True
          OptionsView.Footer = True
          OptionsView.GroupFooters = gfAlwaysVisible
          OptionsView.Indicator = True
          object ViewDoc1Num: TcxGridDBColumn
            Caption = #8470' '#1087#1087
            DataBinding.FieldName = 'Num'
            Options.Editing = False
            Width = 51
          end
          object ViewDoc1IdGoods: TcxGridDBColumn
            Caption = #1050#1086#1076
            DataBinding.FieldName = 'IdGoods'
            Width = 50
          end
          object ViewDoc1NameG: TcxGridDBColumn
            Caption = #1053#1072#1079#1074#1072#1085#1080#1077
            DataBinding.FieldName = 'NameG'
            Width = 179
          end
          object ViewDoc1CType: TcxGridDBColumn
            Caption = #1058#1080#1087' '#1087#1086#1079#1080#1094#1080#1080
            DataBinding.FieldName = 'CType'
            PropertiesClassName = 'TcxImageComboBoxProperties'
            Properties.Items = <
              item
                Description = #1058#1086#1074#1072#1088
                ImageIndex = 0
                Value = 1
              end
              item
                Description = #1059#1089#1083#1091#1075#1072
                ImageIndex = 0
                Value = 2
              end
              item
                Description = #1040#1074#1072#1085#1089
                ImageIndex = 0
                Value = 3
              end
              item
                Description = #1058#1072#1088#1072
                ImageIndex = 0
                Value = 4
              end>
            Styles.Content = dmO.cxStyle5
          end
          object ViewDoc1SM: TcxGridDBColumn
            Caption = #1045#1076'.'#1080#1079#1084'.'
            DataBinding.FieldName = 'SM'
            PropertiesClassName = 'TcxButtonEditProperties'
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.ReadOnly = True
            Properties.OnButtonClick = ViewDoc1SMPropertiesButtonClick
            Width = 57
          end
          object ViewDoc1Quant: TcxGridDBColumn
            Caption = #1050#1086#1083'-'#1074#1086
            DataBinding.FieldName = 'Quant'
          end
          object ViewDoc1QuantRemn: TcxGridDBColumn
            Caption = #1054#1089#1090#1072#1090#1086#1082
            DataBinding.FieldName = 'QuantRemn'
            Options.Editing = False
            Styles.Content = dmO.cxStyle1
          end
          object ViewDoc1Price0: TcxGridDBColumn
            Caption = #1062#1077#1085#1072' '#1073#1077#1079' '#1053#1044#1057
            DataBinding.FieldName = 'Price0'
          end
          object ViewDoc1Sum0: TcxGridDBColumn
            Caption = #1057#1091#1084#1084#1072' '#1079#1072#1082#1091#1087'. '#1073#1077#1079' '#1053#1044#1057
            DataBinding.FieldName = 'Sum0'
            Width = 116
          end
          object ViewDoc1Price1: TcxGridDBColumn
            Caption = #1062#1077#1085#1072' '#1079#1072#1082#1091#1087'. c '#1053#1044#1057
            DataBinding.FieldName = 'Price1'
          end
          object ViewDoc1Sum1: TcxGridDBColumn
            Caption = #1057#1091#1084#1084#1072' '#1079#1072#1082#1091#1087'. '#1089' '#1053#1044#1057
            DataBinding.FieldName = 'Sum1'
            Width = 90
          end
          object ViewDoc1NDSProc: TcxGridDBColumn
            Caption = #1053#1044#1057' %'
            DataBinding.FieldName = 'NDSProc'
          end
          object ViewDoc1RNds: TcxGridDBColumn
            Caption = #1053#1044#1057' '#1089#1091#1084#1084#1072
            DataBinding.FieldName = 'RNds'
          end
          object ViewDoc1PricePP: TcxGridDBColumn
            Caption = #1062#1077#1085#1072' '#1087#1086#1089#1083'. '#1087#1088#1080#1093#1086#1076#1072
            DataBinding.FieldName = 'PricePP'
            Styles.Content = dmO.cxStyle25
          end
          object ViewDoc1TCard: TcxGridDBColumn
            Caption = 'TTK'
            DataBinding.FieldName = 'TCard'
            Visible = False
          end
          object ViewDoc1Sum2: TcxGridDBColumn
            Caption = #1057#1091#1084#1084#1072' '#1091#1095#1077#1090#1085'.'
            DataBinding.FieldName = 'Sum2'
            Visible = False
            Options.Editing = False
          end
          object ViewDoc1Price2: TcxGridDBColumn
            Caption = #1062#1077#1085#1072' '#1091#1095#1077#1090#1085'.'
            DataBinding.FieldName = 'Price2'
            Visible = False
            Options.Editing = False
          end
          object ViewDoc1IM: TcxGridDBColumn
            Caption = #1050#1086#1076' '#1045#1076'.'#1080#1079#1084'.'
            DataBinding.FieldName = 'IM'
            Options.Editing = False
          end
          object ViewDoc1ProcNac: TcxGridDBColumn
            Caption = '% '#1085#1072#1094#1077#1085#1082#1080
            DataBinding.FieldName = 'ProcNac'
            Visible = False
            Options.Editing = False
          end
          object ViewDoc1SumNac: TcxGridDBColumn
            Caption = #1057#1091#1084#1084#1072' '#1085#1072#1094#1077#1085#1082#1080
            DataBinding.FieldName = 'SumNac'
            Visible = False
            Options.Editing = False
          end
        end
        object LevelDoc1: TcxGridLevel
          GridView = ViewDoc1
        end
      end
    end
  end
  object FormPlacement1: TFormPlacement
    Active = False
    Left = 480
    Top = 200
  end
  object taSpec2: TClientDataSet
    Aggregates = <>
    FileName = 'SpecIn.cds'
    Params = <>
    Left = 280
    Top = 200
    object taSpec2Num: TIntegerField
      FieldName = 'Num'
    end
    object taSpec2IdGoods: TIntegerField
      FieldName = 'IdGoods'
    end
    object taSpec2NameG: TStringField
      FieldName = 'NameG'
      Size = 200
    end
    object taSpec2IM: TIntegerField
      FieldName = 'IM'
    end
    object taSpec2SM: TStringField
      FieldName = 'SM'
      Size = 50
    end
    object taSpec2Quant: TFloatField
      FieldName = 'Quant'
      OnChange = taSpec2QuantChange
      DisplayFormat = '0.000'
      EditFormat = '0.000'
    end
    object taSpec2Price1: TCurrencyField
      FieldName = 'Price1'
      OnChange = taSpec2Price1Change
      EditFormat = ',0.00;-,0.00'
    end
    object taSpec2Sum1: TCurrencyField
      FieldName = 'Sum1'
      OnChange = taSpec2Sum1Change
      EditFormat = ',0.00;-,0.00'
    end
    object taSpec2Price2: TCurrencyField
      FieldName = 'Price2'
      OnChange = taSpec2Price2Change
      EditFormat = ',0.00;-,0.00'
    end
    object taSpec2Sum2: TCurrencyField
      FieldName = 'Sum2'
      OnChange = taSpec2Sum2Change
      EditFormat = ',0.00;-,0.00'
    end
    object taSpec2INds: TIntegerField
      FieldName = 'INds'
    end
    object taSpec2SNds: TStringField
      FieldName = 'SNds'
      Size = 30
    end
    object taSpec2RNds: TCurrencyField
      FieldName = 'RNds'
      EditFormat = ',0.00'#1088#39'.'#39';-,0.00'#1088#39'.'#39
    end
    object taSpec2SumNac: TCurrencyField
      FieldName = 'SumNac'
      EditFormat = ',0.00;-,0.00'
    end
    object taSpec2ProcNac: TFloatField
      FieldName = 'ProcNac'
      DisplayFormat = ',0.0%;-,0.0%'
    end
    object taSpec2Km: TFloatField
      FieldName = 'Km'
    end
    object taSpec2PricePP: TFloatField
      FieldName = 'PricePP'
      DisplayFormat = '0.00'
    end
    object taSpec2TCard: TIntegerField
      FieldName = 'TCard'
    end
    object taSpec2CType: TSmallintField
      FieldName = 'CType'
    end
    object taSpec2QuantRemn: TFloatField
      FieldName = 'QuantRemn'
      DisplayFormat = '0.000'
    end
  end
  object dsSpec: TDataSource
    DataSet = taSpec
    Left = 296
    Top = 256
  end
  object prCalcPrice: TpFIBStoredProc
    Transaction = dmO.trSelect
    Database = dmO.OfficeRnDb
    SQL.Strings = (
      
        'EXECUTE PROCEDURE PR_CALCPRICE (?INPRICE, ?IDGOOD, ?PRICE0, ?PRI' +
        'CE1)')
    StoredProcName = 'PR_CALCPRICE'
    Left = 224
    Top = 272
  end
  object amDocIn: TActionManager
    Left = 192
    Top = 208
    StyleName = 'XP Style'
    object acAddPos: TAction
      Caption = 'acAddPos'
      ShortCut = 45
      OnExecute = acAddPosExecute
    end
    object acDelPos: TAction
      Caption = 'acDelPos'
      ShortCut = 119
      OnExecute = acDelPosExecute
    end
    object acAddList: TAction
      Caption = 'acAddList'
      ShortCut = 16429
      OnExecute = acAddListExecute
    end
    object acDelAll: TAction
      Caption = 'acDelAll'
      ShortCut = 16503
      OnExecute = acDelAllExecute
    end
    object acSaveDoc: TAction
      Caption = 'acSaveDoc'
      ShortCut = 16467
      OnExecute = acSaveDocExecute
    end
    object acExitDoc: TAction
      Caption = 'acExitDoc'
      ShortCut = 121
      OnExecute = acExitDocExecute
    end
    object acMovePos: TAction
      Caption = #1044#1074#1080#1078#1077#1085#1080#1077' '#1090#1086#1074#1072#1088#1072
      ShortCut = 32885
      OnExecute = acMovePosExecute
    end
  end
  object PopupMenu1: TPopupMenu
    Images = dmO.imState
    Left = 424
    Top = 240
    object N1: TMenuItem
      Caption = #1050#1086#1087#1080#1088#1086#1074#1072#1090#1100' '#1087#1086#1079#1080#1094#1080#1102
      OnClick = N1Click
    end
    object N3: TMenuItem
      Action = acMovePos
      Hint = #1044#1074#1080#1078#1077#1085#1080#1077' '#1090#1086#1074#1072#1088#1072
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Excel1: TMenuItem
      Caption = #1069#1082#1089#1087#1086#1088#1090' '#1074' Excel'
      ImageIndex = 47
      OnClick = Excel1Click
    end
  end
  object mePrice: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 344
    Top = 312
    object mePriceNum: TIntegerField
      FieldName = 'Num'
    end
    object mePriceIdGoods: TIntegerField
      FieldName = 'IdGoods'
    end
    object mePriceNameG: TStringField
      FieldName = 'NameG'
      Size = 100
    end
    object mePriceSM: TStringField
      FieldName = 'SM'
      Size = 10
    end
    object mePriceQuant: TFloatField
      FieldName = 'Quant'
      DisplayFormat = '0.000'
    end
    object mePricePrice1: TFloatField
      FieldName = 'Price1'
      DisplayFormat = '0.00'
    end
    object mePricePricePP: TFloatField
      FieldName = 'PricePP'
      DisplayFormat = '0.00'
    end
    object mePriceDif: TFloatField
      FieldName = 'Dif'
      DisplayFormat = '0.0'#39'%'#39
    end
  end
  object dsmePrice: TDataSource
    DataSet = mePrice
    Left = 344
    Top = 363
  end
  object taSpec: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 340
    Top = 207
    object taSpecNum: TIntegerField
      FieldName = 'Num'
    end
    object taSpecIdGoods: TIntegerField
      FieldName = 'IdGoods'
    end
    object taSpecNameG: TStringField
      FieldName = 'NameG'
      Size = 200
    end
    object taSpecIM: TIntegerField
      FieldName = 'IM'
    end
    object taSpecSM: TStringField
      FieldName = 'SM'
      Size = 50
    end
    object taSpecQuant: TFloatField
      FieldName = 'Quant'
      OnChange = taSpecQuantChange
      DisplayFormat = '0.000'
    end
    object taSpecPrice1: TFloatField
      FieldName = 'Price1'
      OnChange = taSpecPrice1Change
      DisplayFormat = '0.00'
    end
    object taSpecSum1: TFloatField
      FieldName = 'Sum1'
      OnChange = taSpecSum1Change
      DisplayFormat = '0.00'
    end
    object taSpecPrice2: TFloatField
      FieldName = 'Price2'
      OnChange = taSpecPrice2Change
      DisplayFormat = '0.00'
    end
    object taSpecSum2: TFloatField
      FieldName = 'Sum2'
      OnChange = taSpecSum2Change
      DisplayFormat = '0.00'
    end
    object taSpecINds: TSmallintField
      FieldName = 'INds'
    end
    object taSpecSNds: TStringField
      FieldName = 'SNds'
      Size = 50
    end
    object taSpecRNds: TFloatField
      FieldName = 'RNds'
      OnChange = taSpecRNdsChange
      DisplayFormat = '0.00'
    end
    object taSpecSumNac: TFloatField
      FieldName = 'SumNac'
      DisplayFormat = '0.00'
    end
    object taSpecProcNac: TFloatField
      FieldName = 'ProcNac'
      DisplayFormat = '0.00'
    end
    object taSpecKm: TFloatField
      FieldName = 'Km'
      DisplayFormat = '0.000'
    end
    object taSpecPricePP: TFloatField
      FieldName = 'PricePP'
      DisplayFormat = '0.00'
    end
    object taSpecTCard: TIntegerField
      FieldName = 'TCard'
    end
    object taSpecCType: TSmallintField
      FieldName = 'CType'
    end
    object taSpecQuantRemn: TFloatField
      FieldName = 'QuantRemn'
      DisplayFormat = '0.000'
    end
    object taSpecPrice0: TFloatField
      FieldName = 'Price0'
      OnChange = taSpecPrice0Change
      DisplayFormat = '0.00'
    end
    object taSpecNDSProc: TFloatField
      FieldName = 'NDSProc'
      OnChange = taSpecNDSProcChange
      DisplayFormat = '0.0'
    end
    object taSpecSum0: TFloatField
      FieldName = 'Sum0'
      DisplayFormat = '0.00'
    end
  end
end
