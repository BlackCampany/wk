object fmPressKey: TfmPressKey
  Left = 671
  Top = 473
  BorderStyle = bsSingle
  Caption = #1054#1087#1088#1077#1076#1077#1083#1077#1085#1080#1077' '#1082#1086#1076#1072' '#1082#1083#1072#1074#1080#1096#1080
  ClientHeight = 195
  ClientWidth = 250
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 136
    Width = 225
    Height = 13
    AutoSize = False
    Caption = 'Label1'
  end
  object Label2: TLabel
    Left = 16
    Top = 160
    Width = 221
    Height = 13
    Caption = #1044#1083#1103' '#1074#1099#1093#1086#1076#1072' '#1089' '#1089#1086#1093#1088#1072#1085#1077#1085#1080#1077#1084'  '#1085#1072#1078#1084#1080#1090#1077'  ESC '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clGreen
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 16
    Top = 176
    Width = 216
    Height = 13
    Caption = #1044#1083#1103' '#1074#1099#1093#1086#1076#1072' '#1073#1077#1079' '#1089#1086#1093#1088#1072#1085#1077#1085#1080#1103' '#1085#1072#1078#1084#1080#1090#1077'  F10'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 233
    Height = 57
    Caption = #1057#1080#1084#1074#1086#1083
    TabOrder = 0
    object Edit1: TEdit
      Left = 64
      Top = 8
      Width = 161
      Height = 45
      BevelKind = bkFlat
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -32
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      Text = 'Edit1'
    end
  end
  object GroupBox2: TGroupBox
    Left = 8
    Top = 72
    Width = 233
    Height = 57
    Caption = #1050#1086#1076
    TabOrder = 1
    object Edit2: TEdit
      Left = 64
      Top = 8
      Width = 161
      Height = 45
      BevelKind = bkFlat
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -32
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      Text = 'Edit2'
    end
  end
end
