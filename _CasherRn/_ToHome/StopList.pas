unit StopList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SpeedBar, ExtCtrls, Placemnt, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  cxImageComboBox, ComCtrls, ActnList, XPStyleActnCtrls, ActnMan;

type
  TfmStopList = class(TForm)
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    FormPlacement1: TFormPlacement;
    GridSL: TcxGrid;
    ViewSL: TcxGridDBTableView;
    LevelSL: TcxGridLevel;
    ViewSLSIFR: TcxGridDBColumn;
    ViewSLNAME: TcxGridDBColumn;
    ViewSLPRICE: TcxGridDBColumn;
    ViewSLBACKBGR: TcxGridDBColumn;
    ViewSLIACTIVE: TcxGridDBColumn;
    StatusBar1: TStatusBar;
    TimerSL: TTimer;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    acSL: TActionManager;
    acAddIntoSL: TAction;
    acEditSL: TAction;
    acDelFromSL: TAction;
    procedure SpeedItem1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure TimerSLTimer(Sender: TObject);
    procedure acAddIntoSLExecute(Sender: TObject);
    procedure acEditSLExecute(Sender: TObject);
    procedure acDelFromSLExecute(Sender: TObject);
    procedure ViewSLDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure ViewSLDragDrop(Sender, Source: TObject; X, Y: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmStopList: TfmStopList;

implementation

uses Un1, dmRnEdit, SetQuant, MenuCr;

{$R *.dfm}

procedure TfmStopList.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmStopList.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  GridSL.Align:=AlClient;
end;

procedure TfmStopList.FormShow(Sender: TObject);
begin
  TimerSL.Enabled:=True;
end;

procedure TfmStopList.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  TimerSL.Enabled:=False;
end;

procedure TfmStopList.TimerSLTimer(Sender: TObject);
Var Code:INteger;
begin
  TimerSL.Enabled:=False;
  with dmC do
  begin
    Code:=0;
    if quMenuSL.RecordCount>0 then Code:=quMenuSLSIFR.AsInteger;

    ViewSL.BeginUpdate;
    quMenuSL.Active:=False;
    quMenuSL.Active:=True;
    if Code>0 then
    begin
      quMenuSL.Locate('SIFR',Code,[]);
      ViewSL.Controller.FocusRecord(ViewSL.DataController.FocusedRowIndex,True);
    end;
    ViewSL.EndUpdate;
    StatusBar1.Panels[0].Text:='������������ '+FormatDateTime('hh:nn:ss',now);
  end;
  TimerSL.Enabled:=True;
end;

procedure TfmStopList.acAddIntoSLExecute(Sender: TObject);
begin
  //��������
  bAddSL:=True;
  fmMenuCr.Show;
end;

procedure TfmStopList.acEditSLExecute(Sender: TObject);
Var n:INteger;
begin
 //��������
  if dmC.quMenuSL.RecordCount=0 then exit;
  n:=0;
  while (TimerSl.Enabled=False)and(n<10) do
  begin
    delay(100);
    inc(n);
  end;
  if TimerSl.Enabled then
  begin
    with dmC do
    begin
      TimerSl.Enabled:=False;
      fmSetQuant.cxCalcEdit1.Value:=quMenuSLBACKBGR.AsFloat;
      fmSetQuant.ShowModal;
      if fmSetQuant.ModalResult=mrOk then
      begin
        prAddMove(1,quMenuSLSIFR.AsInteger,fmSetQuant.cxCalcEdit1.Value);
        quMenuSL.Refresh;
      end;
      TimerSl.Enabled:=True;
    end;
  end;
end;

procedure TfmStopList.acDelFromSLExecute(Sender: TObject);
Var n:INteger;
begin
  //�������
  if dmC.quMenuSL.RecordCount=0 then exit;
  n:=0;
  while (TimerSl.Enabled=False)and(n<10) do
  begin
    delay(100);
    inc(n);
  end;
  if TimerSl.Enabled then
  begin
    with dmC do
    begin
      TimerSl.Enabled:=False;

      prAddMove(3,quMenuSLSIFR.AsInteger,0);

      ViewSL.BeginUpdate;
//      quMenuSL.Edit;
//      quMenuSLPRNREST.AsInteger:=0;
//      quMenuSL.Post;
      quMenuSL.FullRefresh;
      ViewSL.EndUpdate;

      TimerSl.Enabled:=True;
    end;
  end;
end;

procedure TfmStopList.ViewSLDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept:=False;
  if bAddSL then  Accept:=True;
end;

procedure TfmStopList.ViewSLDragDrop(Sender, Source: TObject; X,
  Y: Integer);
Var iCo:Integer;
    i,j: Integer;
    iNum: Integer;
    Rec:TcxCustomGridRecord;
begin
//
  if bAddSL then
  begin
    bAddSL:=False;
    iCo:=fmMenuCr.ViewMenuCr.Controller.SelectedRecordCount;
    if iCo>0 then
    begin
      if fmMenuCr.MenuTree.Selected=nil then showmessage('�������� ������.')
      else
      begin
        with dmC do
        begin
          if MessageDlg('�� ������������� ������ ��������� ��������� ������� ('+IntToStr(iCo)+' ��.) � ���� ���� ?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
          begin
            ViewSL.BeginUpdate;
            fmMenuCr.ViewMenuCr.BeginUpdate;

            for i:=0 to fmMenuCr.ViewMenuCr.Controller.SelectedRecordCount-1 do
            begin
              Rec:=fmMenuCr.ViewMenuCr.Controller.SelectedRecords[i];

              for j:=0 to Rec.ValueCount-1 do
              begin
                if fmMenuCr.ViewMenuCr.Columns[j].Name='ViewMenuCrSIFR' then break;
              end;

              iNum:=Rec.Values[j];
          //��� ��� - ����������

              prAddMove(1,iNum,0);
            end;

            quMenuSel.FullRefresh;
            quMenuSL.FullRefresh;

            fmMenuCr.ViewMenuCr.EndUpdate;
            ViewSL.EndUpdate;

            fmStopList.Show;
          end;
        end;
      end;
    end;
  end;
end;

end.
