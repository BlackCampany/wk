object fmImpExcel: TfmImpExcel
  Left = 381
  Top = 496
  BorderStyle = bsDialog
  Caption = #1055#1088#1080#1085#1103#1090#1100' '#1080#1079' Excel'
  ClientHeight = 93
  ClientWidth = 573
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 20
    Width = 78
    Height = 13
    Caption = #1057' '#1082#1072#1082#1086#1081' '#1089#1090#1088#1086#1082#1080
  end
  object Label2: TLabel
    Left = 16
    Top = 52
    Width = 79
    Height = 13
    Caption = #1042#1099#1073#1077#1088#1080#1090#1077' '#1092#1072#1081#1083
  end
  object Panel1: TPanel
    Left = 455
    Top = 0
    Width = 118
    Height = 93
    Align = alRight
    BevelInner = bvLowered
    Color = 16769476
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 8
      Top = 8
      Width = 101
      Height = 25
      Caption = #1054#1082
      Default = True
      ModalResult = 1
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 8
      Top = 48
      Width = 101
      Height = 25
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      LookAndFeel.Kind = lfOffice11
    end
  end
  object cxSpinEdit1: TcxSpinEdit
    Left = 108
    Top = 16
    Properties.MinValue = 1.000000000000000000
    Style.BorderStyle = ebsOffice11
    Style.LookAndFeel.Kind = lfOffice11
    Style.Shadow = True
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 1
    Value = 1
    Width = 65
  end
  object cxButtonEdit1: TcxButtonEdit
    Left = 108
    Top = 48
    Properties.Buttons = <
      item
        Default = True
        Kind = bkEllipsis
      end>
    Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
    Style.LookAndFeel.Kind = lfOffice11
    Style.Shadow = True
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 2
    Width = 325
  end
  object OpenDialog1: TOpenDialog
    Left = 260
    Top = 8
  end
end
