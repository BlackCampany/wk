object fmViewTab: TfmViewTab
  Left = 336
  Top = 510
  AlphaBlend = True
  AlphaBlendValue = 190
  BorderIcons = []
  BorderStyle = bsNone
  Caption = #1055#1088#1086#1089#1084#1086#1090#1088' '#1079#1072#1082#1072#1079#1072
  ClientHeight = 150
  ClientWidth = 395
  Color = 15787997
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object GridVT: TcxGrid
    Left = 0
    Top = 0
    Width = 301
    Height = 150
    Align = alLeft
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    LookAndFeel.Kind = lfOffice11
    object ViewVT: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dmC.dsquSpecSel
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SUMMA'
          Column = ViewVTSUMMA
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnGrouping = False
      OptionsCustomize.ColumnHidingOnGrouping = False
      OptionsCustomize.ColumnMoving = False
      OptionsCustomize.ColumnSorting = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      object ViewVTID: TcxGridDBColumn
        Caption = #8470
        DataBinding.FieldName = 'ID'
        Width = 20
      end
      object ViewVTSIFR: TcxGridDBColumn
        Caption = #1050#1086#1076
        DataBinding.FieldName = 'SIFR'
        Visible = False
        Width = 29
      end
      object ViewVTNAME: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'NAME'
        Width = 97
      end
      object ViewVTPRICE: TcxGridDBColumn
        Caption = #1062#1077#1085#1072
        DataBinding.FieldName = 'PRICE'
        Width = 41
      end
      object ViewVTQUANTITY: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086
        DataBinding.FieldName = 'QUANTITY'
        Width = 37
      end
      object ViewVTDISCOUNTPROC: TcxGridDBColumn
        Caption = '% '#1089#1082#1080#1076#1082#1080
        DataBinding.FieldName = 'DISCOUNTPROC'
        Width = 26
      end
      object ViewVTSUMMA: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072
        DataBinding.FieldName = 'SUMMA'
        Width = 46
      end
    end
    object LevelVT: TcxGridLevel
      GridView = ViewVT
    end
  end
  object cxButton1: TcxButton
    Left = 308
    Top = 4
    Width = 75
    Height = 37
    Caption = #1044#1086#1073#1072#1074#1080#1090#1100
    TabOrder = 1
    OnClick = cxButton1Click
    LookAndFeel.Kind = lfOffice11
  end
  object cxButton2: TcxButton
    Left = 308
    Top = 48
    Width = 75
    Height = 37
    Caption = #1057#1086#1086#1073#1097#1077#1085#1080#1077
    TabOrder = 2
    OnClick = cxButton2Click
    LookAndFeel.Kind = lfOffice11
  end
  object cxButton3: TcxButton
    Left = 308
    Top = 108
    Width = 75
    Height = 37
    Caption = #1047#1072#1082#1088#1099#1090#1100
    TabOrder = 3
    OnClick = cxButton3Click
    LookAndFeel.Kind = lfOffice11
  end
end
