program RnConvDos;

uses
  Forms,
  MainRnConv in 'MainRnConv.pas' {fmMainConvRn},
  Dm in 'Dm.pas' {dmC: TDataModule},
  HistoryTr in 'HistoryTr.pas' {fmHistoryTr},
  Un1 in 'Un1.pas',
  Passw in 'Passw.pas' {fmPerA},
  u2fdk in 'U2FDK.PAS';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfmMainConvRn, fmMainConvRn);
  Application.CreateForm(TdmC, dmC);
  Application.CreateForm(TfmHistoryTr, fmHistoryTr);
  Application.CreateForm(TfmPerA, fmPerA);
  Application.ShowMainForm:=FALSE;
  Application.Run;
end.
