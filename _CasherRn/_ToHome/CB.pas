unit CB;

interface

uses
  SysUtils, Classes, DB, ADODB;

type
  TdmCB = class(TDataModule)
    quDocZHRec: TADOQuery;
    quDocZSSel: TADOQuery;
    msConnection: TADOConnection;
    quDocZSSelIDH: TLargeintField;
    quDocZSSelIDS: TLargeintField;
    quDocZSSelCODE: TIntegerField;
    quDocZSSelBARCODE: TStringField;
    quDocZSSelQUANTPACK: TFloatField;
    quDocZSSelPRICEIN: TFloatField;
    quDocZSSelPRICEIN0: TFloatField;
    quDocZSSelPRICEM: TFloatField;
    quDocZSSelSUMIN: TFloatField;
    quDocZSSelSUMIN0: TFloatField;
    quDocZSSelSUMNDS: TFloatField;
    quDocZSSelNDSPROC: TFloatField;
    quDocZSSelREMN1: TFloatField;
    quDocZSSelVREAL: TFloatField;
    quDocZSSelREMNDAY: TFloatField;
    quDocZSSelREMNMIN: TFloatField;
    quDocZSSelREMNMAX: TFloatField;
    quDocZSSelREMNSTRAH: TFloatField;
    quDocZSSelREMN2: TFloatField;
    quDocZSSelPKDOC: TFloatField;
    quDocZSSelPKCARD: TFloatField;
    quDocZSSelDAYSTOZ: TSmallintField;
    quDocZSSelQUANTZ: TFloatField;
    quDocZSSelQUANTZALR: TFloatField;
    quDocZSSelQUANTZITOG: TFloatField;
    quDocZSSelQUANTZFACT: TFloatField;
    quDocZSSelCLIQUANT: TFloatField;
    quDocZSSelCLIPRICE: TFloatField;
    quDocZSSelCLIPRICE0: TFloatField;
    quDocZSSelCLIQUANTN: TFloatField;
    quDocZSSelCLIPRICEN: TFloatField;
    quDocZSSelCLIPRICE0N: TFloatField;
    quDocZSSelCLINNUM: TIntegerField;
    quDocZSSelROUNDPACK: TIntegerField;
    quDocZHRecID: TLargeintField;
    quDocZHRecDOCDATE: TWideStringField;
    quDocZHRecDOCDATEZ: TWideStringField;
    quDocZHRecIDATE: TIntegerField;
    quDocZHRecIDATEZ: TIntegerField;
    quDocZHRecDAYSTOZ: TSmallintField;
    quDocZHRecDOCNUM: TStringField;
    quDocZHRecISHOP: TIntegerField;
    quDocZHRecDEPART: TIntegerField;
    quDocZHRecITYPE: TSmallintField;
    quDocZHRecINSUMIN: TFloatField;
    quDocZHRecINSUMIN0: TFloatField;
    quDocZHRecDEPARTORG: TIntegerField;
    quDocZHRecCLITYPE: TSmallintField;
    quDocZHRecCLICODE: TIntegerField;
    quDocZHRecIACTIVE: TSmallintField;
    quDocZHRecDATEEDIT: TDateTimeField;
    quDocZHRecPERSON: TStringField;
    quDocZHRecCLISUMIN: TFloatField;
    quDocZHRecCLISUMIN0: TFloatField;
    quDocZHRecCLISUMINN: TFloatField;
    quDocZHRecCLISUMIN0N: TFloatField;
    quDocZHRecQUANT: TFloatField;
    quDocZHRecCLIQUANT: TFloatField;
    quDocZHRecCLIQUANTN: TFloatField;
    quDocZHRecQUANTZAUTO: TFloatField;
    quDocZHRecIDATEPOST: TIntegerField;
    quDocZHRecIDATENACL: TIntegerField;
    quDocZHRecSNUMNACL: TStringField;
    quDocZHRecIDATESCHF: TIntegerField;
    quDocZHRecSNUMSCHF: TStringField;
    quDocZHRecSENDTO: TSmallintField;
    quDocZHRecSENDDATE: TDateTimeField;
    quDocZHRecSNUMPODTV: TWideStringField;
    quDocZHRecSENDNACL: TSmallintField;
    quDocZHRecIDHPRO: TIntegerField;
    quDocZSNacl: TADOQuery;
    quDocZSNaclIDH: TIntegerField;
    quDocZSNaclIDS: TIntegerField;
    quDocZSNaclICODE: TIntegerField;
    quDocZSNaclICARDTYPE: TIntegerField;
    quDocZSNaclQUANT: TFloatField;
    quDocZSNaclPRICE0: TFloatField;
    quDocZSNaclPRICE: TFloatField;
    quDocZSNaclRNDS: TFloatField;
    quDocZSNaclRSUM0: TFloatField;
    quDocZSNaclRSUM: TFloatField;
    quDocZSNaclRSUMNDS: TFloatField;
    quDocZSNaclNAME: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmCB: TdmCB;

implementation

{$R *.dfm}

end.
