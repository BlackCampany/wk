unit uTransM;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, cxControls, cxContainer, cxEdit,
  cxGroupBox, StdCtrls, cxButtons, cxMaskEdit, cxDropDownEdit, cxCalc,
  cxTextEdit;

type
  TfmTransM = class(TForm)
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    cxGroupBox1: TcxGroupBox;
    cxGroupBox2: TcxGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    cxTextEdit1: TcxTextEdit;
    cxCalcEdit1: TcxCalcEdit;
    cxCalcEdit2: TcxCalcEdit;
    Label4: TLabel;
    cxTextEdit2: TcxTextEdit;
    Label5: TLabel;
    cxCalcEdit3: TcxCalcEdit;
    Label6: TLabel;
    cxCalcEdit4: TcxCalcEdit;
    procedure cxButton3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmTransM: TfmTransM;

implementation

{$R *.dfm}

procedure TfmTransM.cxButton3Click(Sender: TObject);
begin
  close;
end;

end.
