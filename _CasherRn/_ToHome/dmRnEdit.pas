unit dmRnEdit;

interface

uses
  SysUtils, Classes, DB, FIBDataSet, pFIBDataSet, FIBDatabase, pFIBDatabase,
  cxStyles, ImgList, Controls, FIBQuery, pFIBQuery, pFIBStoredProc,
  frexpimg, frRtfExp, frTXTExp, frXMLExl, frOLEExl, FR_E_HTML2, FR_E_HTM,
  FR_E_CSV, FR_E_RTF, FR_Class, FR_E_TXT;

type
  TdmC = class(TDataModule)
    CasherRnDb: TpFIBDatabase;
    trSelect: TpFIBTransaction;
    trUpdate: TpFIBTransaction;
    trDel: TpFIBTransaction;
    quPer: TpFIBDataSet;
    quPerID: TFIBIntegerField;
    quPerID_PARENT: TFIBIntegerField;
    quPerNAME: TFIBStringField;
    quPerUVOLNEN: TFIBBooleanField;
    quPerCheck: TFIBStringField;
    quPerMODUL1: TFIBBooleanField;
    quPerMODUL2: TFIBBooleanField;
    quPerMODUL3: TFIBBooleanField;
    quPerMODUL4: TFIBBooleanField;
    quPerMODUL5: TFIBBooleanField;
    quPerMODUL6: TFIBBooleanField;
    dsPer: TDataSource;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    cxStyle14: TcxStyle;
    cxStyle15: TcxStyle;
    cxStyle16: TcxStyle;
    cxStyle17: TcxStyle;
    cxStyle18: TcxStyle;
    cxStyle19: TcxStyle;
    cxStyle20: TcxStyle;
    cxStyle21: TcxStyle;
    cxStyle22: TcxStyle;
    cxStyle23: TcxStyle;
    imState: TImageList;
    quMenuTree: TpFIBDataSet;
    quMenuSel: TpFIBDataSet;
    quMenuSelSIFR: TFIBIntegerField;
    quMenuSelNAME: TFIBStringField;
    quMenuSelPRICE: TFIBFloatField;
    quMenuSelCODE: TFIBStringField;
    quMenuSelTREETYPE: TFIBStringField;
    quMenuSelLIMITPRICE: TFIBFloatField;
    quMenuSelCATEG: TFIBSmallIntField;
    quMenuSelPARENT: TFIBSmallIntField;
    quMenuSelLINK: TFIBSmallIntField;
    quMenuSelSTREAM: TFIBSmallIntField;
    quMenuSelLACK: TFIBSmallIntField;
    quMenuSelDESIGNSIFR: TFIBSmallIntField;
    quMenuSelALTNAME: TFIBStringField;
    quMenuSelNALOG: TFIBFloatField;
    quMenuSelBARCODE: TFIBStringField;
    quMenuSelIMAGE: TFIBSmallIntField;
    quMenuSelCONSUMMA: TFIBFloatField;
    quMenuSelMINREST: TFIBSmallIntField;
    quMenuSelPRNREST: TFIBSmallIntField;
    quMenuSelCOOKTIME: TFIBSmallIntField;
    quMenuSelDISPENSER: TFIBSmallIntField;
    quMenuSelDISPKOEF: TFIBSmallIntField;
    quMenuSelACCESS: TFIBSmallIntField;
    quMenuSelFLAGS: TFIBSmallIntField;
    quMenuSelTARA: TFIBSmallIntField;
    quMenuSelCNTPRICE: TFIBSmallIntField;
    quMenuSelBACKBGR: TFIBFloatField;
    quMenuSelFONTBGR: TFIBFloatField;
    quMenuSelIACTIVE: TFIBSmallIntField;
    quMenuSelIEDIT: TFIBSmallIntField;
    dsMenuSel: TDataSource;
    quCateg: TpFIBDataSet;
    dsCateg: TDataSource;
    quCategSIFR: TFIBIntegerField;
    quCategNAME: TFIBStringField;
    quCategTAX_GROUP: TFIBIntegerField;
    quCategIACTIVE: TFIBSmallIntField;
    quCategIEDIT: TFIBSmallIntField;
    quCanDo: TpFIBDataSet;
    quCanDoID_PERSONAL: TFIBIntegerField;
    quCanDoNAME: TFIBStringField;
    quCanDoPREXEC: TFIBBooleanField;
    trCanDo: TpFIBTransaction;
    quMaxIdCateg: TpFIBDataSet;
    quMaxIdCategMAXID: TFIBIntegerField;
    quModGrMo: TpFIBDataSet;
    quModGrMoSIFR: TFIBIntegerField;
    quModGrMoNAME: TFIBStringField;
    quModGrMoPARENT: TFIBIntegerField;
    quModGrMoPRICE: TFIBFloatField;
    quModGrMoREALPRICE: TFIBFloatField;
    quModGrMoIACTIVE: TFIBSmallIntField;
    quModGrMoIEDIT: TFIBSmallIntField;
    quMaxIdGrMo: TpFIBDataSet;
    quMaxIdGrMoMAXID: TFIBIntegerField;
    quMods: TpFIBDataSet;
    dsMods: TDataSource;
    quModsSIFR: TFIBIntegerField;
    quModsNAME: TFIBStringField;
    quModsPARENT: TFIBIntegerField;
    quModsPRICE: TFIBFloatField;
    quModsREALPRICE: TFIBFloatField;
    quModsIACTIVE: TFIBSmallIntField;
    quModsIEDIT: TFIBSmallIntField;
    quDelMod: TpFIBDataSet;
    trUpd1: TpFIBTransaction;
    quMaxIdM: TpFIBDataSet;
    quMaxIdMMAXID: TFIBIntegerField;
    taMenu: TpFIBDataSet;
    trUpd2: TpFIBTransaction;
    trSel2: TpFIBTransaction;
    taMenuSIFR: TFIBIntegerField;
    taMenuNAME: TFIBStringField;
    taMenuPRICE: TFIBFloatField;
    taMenuCODE: TFIBStringField;
    taMenuTREETYPE: TFIBStringField;
    taMenuLIMITPRICE: TFIBFloatField;
    taMenuCATEG: TFIBSmallIntField;
    taMenuPARENT: TFIBSmallIntField;
    taMenuLINK: TFIBSmallIntField;
    taMenuSTREAM: TFIBSmallIntField;
    taMenuLACK: TFIBSmallIntField;
    taMenuDESIGNSIFR: TFIBSmallIntField;
    taMenuALTNAME: TFIBStringField;
    taMenuNALOG: TFIBFloatField;
    taMenuBARCODE: TFIBStringField;
    taMenuIMAGE: TFIBSmallIntField;
    taMenuCONSUMMA: TFIBFloatField;
    taMenuMINREST: TFIBSmallIntField;
    taMenuPRNREST: TFIBSmallIntField;
    taMenuCOOKTIME: TFIBSmallIntField;
    taMenuDISPENSER: TFIBSmallIntField;
    taMenuDISPKOEF: TFIBSmallIntField;
    taMenuACCESS: TFIBSmallIntField;
    taMenuFLAGS: TFIBSmallIntField;
    taMenuTARA: TFIBSmallIntField;
    taMenuCNTPRICE: TFIBSmallIntField;
    taMenuBACKBGR: TFIBFloatField;
    taMenuFONTBGR: TFIBFloatField;
    taMenuIACTIVE: TFIBSmallIntField;
    taMenuIEDIT: TFIBSmallIntField;
    prAddRClassif: TpFIBStoredProc;
    trUpdProc: TpFIBTransaction;
    quMenuFindParent: TpFIBDataSet;
    quMenuFindParentCOUNTREC: TFIBIntegerField;
    prDelRClassif: TpFIBStoredProc;
    prEditRClassif: TpFIBStoredProc;
    quCategUp: TpFIBDataSet;
    FIBIntegerField1: TFIBIntegerField;
    FIBStringField1: TFIBStringField;
    FIBIntegerField2: TFIBIntegerField;
    FIBSmallIntField1: TFIBSmallIntField;
    FIBSmallIntField2: TFIBSmallIntField;
    dsCategUp: TDataSource;
    trSel3: TpFIBTransaction;
    quModsGr: TpFIBDataSet;
    FIBIntegerField3: TFIBIntegerField;
    FIBStringField2: TFIBStringField;
    FIBIntegerField4: TFIBIntegerField;
    FIBFloatField1: TFIBFloatField;
    FIBFloatField2: TFIBFloatField;
    FIBSmallIntField3: TFIBSmallIntField;
    FIBSmallIntField4: TFIBSmallIntField;
    dsModsGr: TDataSource;
    quNalog: TpFIBDataSet;
    dsNalog: TDataSource;
    quNalogID: TFIBIntegerField;
    quNalogPRIOR: TFIBSmallIntField;
    quNalogNAME: TFIBStringField;
    quMenuSelNAMECA: TFIBStringField;
    quMenuSelNAMEMO: TFIBStringField;
    cxStyle24: TcxStyle;
    cxStyle25: TcxStyle;
    prGetId: TpFIBStoredProc;
    quMaxIdMe: TpFIBDataSet;
    quMaxIdMeMAXID: TFIBIntegerField;
    taMenuPrint: TpFIBDataSet;
    taMenuPrintSIFR: TFIBIntegerField;
    taMenuPrintNAME: TFIBStringField;
    taMenuPrintPRICE: TFIBFloatField;
    taMenuPrintCODE: TFIBStringField;
    taMenuPrintTREETYPE: TFIBStringField;
    taMenuPrintLIMITPRICE: TFIBFloatField;
    taMenuPrintCATEG: TFIBSmallIntField;
    taMenuPrintPARENT: TFIBSmallIntField;
    taMenuPrintLINK: TFIBSmallIntField;
    taMenuPrintSTREAM: TFIBSmallIntField;
    taMenuPrintLACK: TFIBSmallIntField;
    taMenuPrintDESIGNSIFR: TFIBSmallIntField;
    taMenuPrintALTNAME: TFIBStringField;
    taMenuPrintNALOG: TFIBFloatField;
    taMenuPrintBARCODE: TFIBStringField;
    taMenuPrintIMAGE: TFIBSmallIntField;
    taMenuPrintCONSUMMA: TFIBFloatField;
    taMenuPrintMINREST: TFIBSmallIntField;
    taMenuPrintPRNREST: TFIBSmallIntField;
    taMenuPrintCOOKTIME: TFIBSmallIntField;
    taMenuPrintDISPENSER: TFIBSmallIntField;
    taMenuPrintDISPKOEF: TFIBSmallIntField;
    taMenuPrintACCESS: TFIBSmallIntField;
    taMenuPrintFLAGS: TFIBSmallIntField;
    taMenuPrintTARA: TFIBSmallIntField;
    taMenuPrintCNTPRICE: TFIBSmallIntField;
    taMenuPrintBACKBGR: TFIBFloatField;
    taMenuPrintFONTBGR: TFIBFloatField;
    taMenuPrintIACTIVE: TFIBSmallIntField;
    taMenuPrintIEDIT: TFIBSmallIntField;
    taMenuPrintNAMEGR: TFIBStringField;
    dsMenuPrint: TDataSource;
    frTextExport1: TfrTextExport;
    frRTFExport1: TfrRTFExport;
    frCSVExport1: TfrCSVExport;
    frHTMExport1: TfrHTMExport;
    frHTML2Export1: TfrHTML2Export;
    frOLEExcelExport1: TfrOLEExcelExport;
    frXMLExcelExport1: TfrXMLExcelExport;
    frTextAdvExport1: TfrTextAdvExport;
    frRtfAdvExport1: TfrRtfAdvExport;
    frJPEGExport1: TfrJPEGExport;
    quModList: TpFIBDataSet;
    quModListSIFR: TFIBIntegerField;
    quModListNAME: TFIBStringField;
    quModListPARENT: TFIBIntegerField;
    quModListNAMEGR: TFIBStringField;
    taMH: TpFIBDataSet;
    trMH: TpFIBTransaction;
    dsMH: TDataSource;
    taMHID: TFIBIntegerField;
    taMHNAMESTREAM: TFIBStringField;
    quMenuSelNAMESTREAM: TFIBStringField;
    taMESSAGE: TpFIBDataSet;
    taMESSAGEID: TFIBIntegerField;
    taMESSAGEMESSAG: TFIBStringField;
    dstaMESSAGE: TDataSource;
    quMenuDayList: TpFIBDataSet;
    quMenuDayListID: TFIBSmallIntField;
    quMenuDayListNAME: TFIBStringField;
    quMenuDayListMDATE: TFIBDateField;
    quMenuDayListIACTIVE: TFIBIntegerField;
    dsquMenuDayList: TDataSource;
    quMDL_Id: TpFIBDataSet;
    quMDL_IdID: TFIBSmallIntField;
    quMDL_IdNAME: TFIBStringField;
    quMDL_IdMDATE: TFIBDateField;
    quMDL_IdIACTIVE: TFIBIntegerField;
    quMenuDay: TpFIBDataSet;
    quMenuDayIDH: TFIBIntegerField;
    quMenuDayID: TFIBIntegerField;
    quMenuDayNAME: TFIBStringField;
    quMenuDayPRIOR: TFIBIntegerField;
    quMenuDayPARENT: TFIBIntegerField;
    quMaxMDG: TpFIBDataSet;
    quMaxMDGID: TFIBIntegerField;
    quMDGID: TpFIBDataSet;
    quMDGIDIDH: TFIBIntegerField;
    quMDGIDID: TFIBIntegerField;
    quMDGIDNAMEGR: TFIBStringField;
    quMDGIDPRIOR: TFIBIntegerField;
    quMDGIDPARENT: TFIBIntegerField;
    quMDS: TpFIBDataSet;
    dsquMDS: TDataSource;
    quMDSIDH: TFIBIntegerField;
    quMDSIDGR: TFIBIntegerField;
    quMDSSIFR: TFIBIntegerField;
    quMDSNAME: TFIBStringField;
    quMDSPRICE: TFIBFloatField;
    quMDFindParent: TpFIBDataSet;
    quMDFindParentCOUNTREC: TFIBIntegerField;
    quMDRep: TpFIBDataSet;
    quMDRepIDH: TFIBIntegerField;
    quMDRepIDGR: TFIBIntegerField;
    quMDRepSIFR: TFIBIntegerField;
    quMDRepNAMEGR: TFIBStringField;
    quMDRepPRIOR: TFIBIntegerField;
    quMDRepNAME: TFIBStringField;
    quMDRepPRICE: TFIBFloatField;
    prSetMDL: TpFIBStoredProc;
    trSet: TpFIBTransaction;
    quD: TpFIBQuery;
    quMDSALTNAME: TFIBStringField;
    quMDRepALTNAME: TFIBStringField;
    quMDGroups: TpFIBDataSet;
    quMDGroupsIDH: TFIBIntegerField;
    quMDGroupsID: TFIBIntegerField;
    quMDGroupsNAMEGR: TFIBStringField;
    quMDGroupsPRIOR: TFIBIntegerField;
    quMDGroupsPARENT: TFIBIntegerField;
    quMDSPRI: TFIBIntegerField;
    quMDRepPRI: TFIBIntegerField;
    quFindB: TpFIBDataSet;
    quFindBSIFR: TFIBIntegerField;
    quFindBNAME: TFIBStringField;
    quFindBPRICE: TFIBFloatField;
    quFindBCODE: TFIBStringField;
    quFindBPARENT: TFIBSmallIntField;
    quFindBCONSUMMA: TFIBFloatField;
    dsFindB: TDataSource;
    quModsCODEB: TFIBIntegerField;
    quModsKB: TFIBFloatField;
    taTabsAll: TpFIBDataSet;
    taTabsAllID: TFIBIntegerField;
    taTabsAllID_PERSONAL: TFIBIntegerField;
    taTabsAllNUMTABLE: TFIBStringField;
    taTabsAllQUESTS: TFIBIntegerField;
    taTabsAllTABSUM: TFIBFloatField;
    taTabsAllBEGTIME: TFIBDateTimeField;
    taTabsAllENDTIME: TFIBDateTimeField;
    taTabsAllDISCONT: TFIBStringField;
    taTabsAllOPERTYPE: TFIBStringField;
    taTabsAllCHECKNUM: TFIBIntegerField;
    taTabsAllSKLAD: TFIBSmallIntField;
    taTabsAllDISCONT1: TFIBStringField;
    taTabsAllSTATION: TFIBIntegerField;
    taTabsAllNUMZ: TFIBIntegerField;
    taDelPar: TpFIBDataSet;
    dstaDelPar: TDataSource;
    taDelParID: TFIBIntegerField;
    taDelParNAMEDEL: TFIBStringField;
    taDelParBTYPE: TFIBSmallIntField;
    taCategSale: TpFIBDataSet;
    dstaCategSale: TDataSource;
    taCategSaleID: TFIBIntegerField;
    taCategSaleNAMECS: TFIBStringField;
    quMenuSelDATEB: TFIBIntegerField;
    quMenuSelDATEE: TFIBIntegerField;
    quMenuSelDAYWEEK: TFIBStringField;
    quMenuSelTIMEB: TFIBTimeField;
    quMenuSelTIMEE: TFIBTimeField;
    quMenuSelALLTIME: TFIBSmallIntField;
    quMenuSelTIMEBB: TStringField;
    quMenuSelTIMEEE: TStringField;
    trUpdMenu: TpFIBTransaction;
    quMenuID: TpFIBDataSet;
    quMenuIDSIFR: TFIBIntegerField;
    quMenuIDNAME: TFIBStringField;
    quMenuIDPRICE: TFIBFloatField;
    quMenuIDCODE: TFIBStringField;
    quMenuIDTREETYPE: TFIBStringField;
    quMenuIDLIMITPRICE: TFIBFloatField;
    quMenuIDCATEG: TFIBSmallIntField;
    quMenuIDPARENT: TFIBSmallIntField;
    quMenuIDLINK: TFIBSmallIntField;
    quMenuIDSTREAM: TFIBSmallIntField;
    quMenuIDLACK: TFIBSmallIntField;
    quMenuIDDESIGNSIFR: TFIBSmallIntField;
    quMenuIDALTNAME: TFIBStringField;
    quMenuIDNALOG: TFIBFloatField;
    quMenuIDBARCODE: TFIBStringField;
    quMenuIDIMAGE: TFIBSmallIntField;
    quMenuIDCONSUMMA: TFIBFloatField;
    quMenuIDMINREST: TFIBSmallIntField;
    quMenuIDPRNREST: TFIBSmallIntField;
    quMenuIDCOOKTIME: TFIBSmallIntField;
    quMenuIDDISPENSER: TFIBSmallIntField;
    quMenuIDDISPKOEF: TFIBSmallIntField;
    quMenuIDACCESS: TFIBSmallIntField;
    quMenuIDFLAGS: TFIBSmallIntField;
    quMenuIDTARA: TFIBSmallIntField;
    quMenuIDCNTPRICE: TFIBSmallIntField;
    quMenuIDBACKBGR: TFIBFloatField;
    quMenuIDFONTBGR: TFIBFloatField;
    quMenuIDIACTIVE: TFIBSmallIntField;
    quMenuIDIEDIT: TFIBSmallIntField;
    quMenuIDDATEB: TFIBIntegerField;
    quMenuIDDATEE: TFIBIntegerField;
    quMenuIDDAYWEEK: TFIBStringField;
    quMenuIDTIMEB: TFIBTimeField;
    quMenuIDTIMEE: TFIBTimeField;
    quMenuIDALLTIME: TFIBSmallIntField;
    taMESSAGEISERV: TFIBSmallIntField;
    quFBar: TpFIBDataSet;
    quFBarBARCODE: TFIBStringField;
    quFBarSIFR: TFIBIntegerField;
    quFBarQUANT: TFIBFloatField;
    quFBarPRICE: TFIBFloatField;
    quFBarC: TpFIBDataSet;
    quFBarCBARCODE: TFIBStringField;
    quFBarCSIFR: TFIBIntegerField;
    quFBarCQUANT: TFIBFloatField;
    quFBarCPRICE: TFIBFloatField;
    quMenuSL: TpFIBDataSet;
    quMenuSLSIFR: TFIBIntegerField;
    quMenuSLNAME: TFIBStringField;
    quMenuSLPRICE: TFIBFloatField;
    quMenuSLCODE: TFIBStringField;
    quMenuSLTREETYPE: TFIBStringField;
    quMenuSLLIMITPRICE: TFIBFloatField;
    quMenuSLCATEG: TFIBSmallIntField;
    quMenuSLPARENT: TFIBSmallIntField;
    quMenuSLLINK: TFIBSmallIntField;
    quMenuSLSTREAM: TFIBSmallIntField;
    quMenuSLLACK: TFIBSmallIntField;
    quMenuSLDESIGNSIFR: TFIBSmallIntField;
    quMenuSLALTNAME: TFIBStringField;
    quMenuSLNALOG: TFIBFloatField;
    quMenuSLBARCODE: TFIBStringField;
    quMenuSLIMAGE: TFIBSmallIntField;
    quMenuSLCONSUMMA: TFIBFloatField;
    quMenuSLMINREST: TFIBSmallIntField;
    quMenuSLPRNREST: TFIBSmallIntField;
    quMenuSLCOOKTIME: TFIBSmallIntField;
    quMenuSLDISPENSER: TFIBSmallIntField;
    quMenuSLDISPKOEF: TFIBSmallIntField;
    quMenuSLACCESS: TFIBSmallIntField;
    quMenuSLFLAGS: TFIBSmallIntField;
    quMenuSLTARA: TFIBSmallIntField;
    quMenuSLCNTPRICE: TFIBSmallIntField;
    quMenuSLBACKBGR: TFIBFloatField;
    quMenuSLFONTBGR: TFIBFloatField;
    quMenuSLIACTIVE: TFIBSmallIntField;
    quMenuSLIEDIT: TFIBSmallIntField;
    quMenuSLDATEB: TFIBIntegerField;
    quMenuSLDATEE: TFIBIntegerField;
    quMenuSLDAYWEEK: TFIBStringField;
    quMenuSLTIMEB: TFIBTimeField;
    quMenuSLTIMEE: TFIBTimeField;
    quMenuSLALLTIME: TFIBSmallIntField;
    dsquMenuSL: TDataSource;
    prADDMOVESL: TpFIBStoredProc;
    quTabDBList: TpFIBDataSet;
    quTabDBListRDBRELATION_NAME: TFIBWideStringField;
    quTabDBListRDBFIELD_POSITION: TFIBSmallIntField;
    quTabDBListRDBFIELD_NAME: TFIBWideStringField;
    quUpd: TpFIBQuery;
    trUpd: TpFIBTransaction;
    quProcDBList: TpFIBDataSet;
    quProcDBListRDBPROCEDURE_NAME: TFIBWideStringField;
    quProcDBListRDBSYSTEM_FLAG: TFIBSmallIntField;
    quGensDBList: TpFIBDataSet;
    quGensDBListRDBGENERATOR_NAME: TFIBWideStringField;
    quGensDBListRDBSYSTEM_FLAG: TFIBSmallIntField;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure quMenuSelCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

Function CanDo(Name_F:String):Boolean;
function prFindBar(sBar:String):INteger;
procedure prAddMove(Oper,Code:Integer;rQ:Real);


procedure prUpd;

var
  dmC: TdmC;

implementation

uses Un1;

{$R *.dfm}

procedure prUpd;
Var bUpd:Boolean;
begin
  with dmC do
  begin
    bUpd:=True; //��� ���������� 12-��� ����

    quTabDBList.Active:=False;
    quTabDBList.ParamByName('STABNAME').AsString:='DISCCARD';
    quTabDBList.Active:=True;
    if quTabDBList.RecordCount>0 then
    begin //�������� ����
      quTabDBList.First;
      while not quTabDBList.Eof do
      begin
        if quTabDBListRDBFIELD_NAME.AsString='PREF' then begin bUpd:=False; break; end;
        quTabDBList.Next;
      end;

      if bUpd then //������� ���� ��� - ��������
      begin
        quUpd.SQL.Clear;
        quUpd.SQL.Add('ALTER TABLE DISCCARD');
        quUpd.SQL.Add('ADD PREF VARCHAR(30)');
        quUpd.ExecQuery;
        delay(100);
      end;
    end;

    quProcDBList.Active:=False;
    quProcDBList.ParamByName('SPROCNAME').AsString:='PR_SETDISCPREF';
    quProcDBList.Active:=True;
    if quProcDBList.RecordCount=0 then
    begin //��������� ��� ������ - ����� ������
      quUpd.SQL.Clear;
      quUpd.SQL.Add('CREATE PROCEDURE PR_SETDISCPREF');
      quUpd.SQL.Add('AS');
      quUpd.SQL.Add('declare variable SPRE VARCHAR (30);');
      quUpd.SQL.Add('begin');
      quUpd.SQL.Add('  for SELECT PREF');
      quUpd.SQL.Add('  FROM DISCCARD');
      quUpd.SQL.Add('  where BARCODE like ''!%'' and PREF is not NULL');
      quUpd.SQL.Add('  into :SPRE do');
      quUpd.SQL.Add('  begin');
      quUpd.SQL.Add('    update tables_all set DISCONT=:SPRE');
      quUpd.SQL.Add('    where DISCONT like :SPRE||''%'';');
      quUpd.SQL.Add('  end');
      quUpd.SQL.Add('  suspend;');
      quUpd.SQL.Add('end');
      quUpd.ExecQuery;
      delay(100);
    end;

    //15.03.2012

    quGensDBList.Active:=False;
    quGensDBList.ParamByName('SGENNAME').AsString:='GEN_MOVE';
    quGensDBList.Active:=True;
    if quGensDBList.RecordCount=0 then
    begin //���������� ��� ������ - ����� ������
      quUpd.SQL.Clear;
      quUpd.SQL.Add('CREATE GENERATOR GEN_MOVE');
      quUpd.ExecQuery;
      delay(100);
    end;

    quTabDBList.Active:=False;
    quTabDBList.ParamByName('STABNAME').AsString:='CARDSMOVE';
    quTabDBList.Active:=True;
    if quTabDBList.RecordCount=0 then
    begin //�������� ���
      quUpd.SQL.Clear;
      quUpd.SQL.Add('CREATE TABLE CARDSMOVE (');
      quUpd.SQL.Add('    CODE INTEGER NOT NULL,');
      quUpd.SQL.Add('    DDATE DATE NOT NULL,');
      quUpd.SQL.Add('    ID INTEGER NOT NULL,');
      quUpd.SQL.Add('    OPER SMALLINT,');
      quUpd.SQL.Add('    QUANT DOUBLE PRECISION,');
      quUpd.SQL.Add('    RQUANT DOUBLE PRECISION,');
      quUpd.SQL.Add('    PERSON VARCHAR (50) character set WIN1251 collate WIN1251);');
      quUpd.ExecQuery;
      delay(100);

      quUpd.SQL.Clear;
      quUpd.SQL.Add('ALTER TABLE CARDSMOVE ADD CONSTRAINT PK_CARDSMOVE PRIMARY KEY (CODE, DDATE, ID);');
      quUpd.ExecQuery;
      delay(100);

      quUpd.SQL.Clear;
      quUpd.SQL.Add('ALTER TABLE CARDSMOVE ADD  CONSTRAINT FK_CARDSMOVE FOREIGN KEY (CODE) REFERENCES MENU (SIFR) ON DELETE CASCADE;');
      quUpd.ExecQuery;
      delay(100);
    end;

    quProcDBList.Active:=False;
    quProcDBList.ParamByName('SPROCNAME').AsString:='PR_ADDMOVESL';
    quProcDBList.Active:=True;
    if quProcDBList.RecordCount=0 then
    begin //��������� ��� ������ - ����� ������
      quUpd.SQL.Clear;
      quUpd.SQL.Add('CREATE PROCEDURE PR_ADDMOVESL (');
      quUpd.SQL.Add('    SIFR INTEGER,');
      quUpd.SQL.Add('    PERSON VARCHAR (100),');
      quUpd.SQL.Add('    QUANT FLOAT,');
      quUpd.SQL.Add('    OPER INTEGER)');
      quUpd.SQL.Add('AS ');
      quUpd.SQL.Add('DECLARE VARIABLE CURDATE DATE;');
      quUpd.SQL.Add('DECLARE VARIABLE IMAX integer;');
      quUpd.SQL.Add('DECLARE VARIABLE RQUANT DOUBLE PRECISION;');
      quUpd.SQL.Add('begin ');
      quUpd.SQL.Add('  CURDATE = cast(''now'' as date);');
      quUpd.SQL.Add('  if (OPER=1) then /* ������ �������� �������  ���������� � ��������� */');
      quUpd.SQL.Add('  begin ');
      quUpd.SQL.Add('    IMAX=GEN_ID(GEN_MOVE, 1);');
      quUpd.SQL.Add('    update MENU set BACKBGR = :QUANT, PRNREST=1  where SIFR=:SIFR;');
      quUpd.SQL.Add('    insert into CARDSMOVE (CODE,DDATE,ID,OPER,QUANT,RQUANT,PERSON)');
      quUpd.SQL.Add('    values (:SIFR,:CURDATE,:IMAX,:OPER,:QUANT,:QUANT,:PERSON);');
      quUpd.SQL.Add('  end ');
      quUpd.SQL.Add('  if (OPER=2) then /* �������� ������� ��������� */');
      quUpd.SQL.Add('  begin ');
      quUpd.SQL.Add('    IMAX=GEN_ID(GEN_MOVE, 1);');
      quUpd.SQL.Add('    Select BACKBGR from menu where SIFR=:SIFR into :RQUANT;');
      quUpd.SQL.Add('    RQUANT=:RQUANT+:QUANT;');
      quUpd.SQL.Add('    update MENU set BACKBGR = :RQUANT where SIFR=:SIFR;');
      quUpd.SQL.Add('    insert into CARDSMOVE (CODE,DDATE,ID,OPER,QUANT,RQUANT,PERSON)');
      quUpd.SQL.Add('    values (:SIFR,:CURDATE,:IMAX,:OPER,:QUANT,:RQUANT,:PERSON);');
      quUpd.SQL.Add('  end ');
      quUpd.SQL.Add('  if (OPER=3) then /* ������ �������� ������� �������� �� ������*/ ');
      quUpd.SQL.Add('  begin ');
      quUpd.SQL.Add('    IMAX=GEN_ID(GEN_MOVE, 1);');
      quUpd.SQL.Add('    update MENU set BACKBGR = :QUANT, PRNREST=0  where SIFR=:SIFR;');
      quUpd.SQL.Add('    insert into CARDSMOVE (CODE,DDATE,ID,OPER,QUANT,RQUANT,PERSON)');
      quUpd.SQL.Add('    values (:SIFR,:CURDATE,:IMAX,:OPER,:QUANT,:QUANT,:PERSON);');
      quUpd.SQL.Add('  end');
      quUpd.SQL.Add('  SUSPEND; ');
      quUpd.SQL.Add('END ');
      quUpd.ExecQuery;
      delay(100);
    end;

    quProcDBList.Active:=False;
    quProcDBList.ParamByName('SPROCNAME').AsString:='PR_SAVEMOVE';
    quProcDBList.Active:=True;
    if quProcDBList.RecordCount=0 then
    begin //��������� ��� ������ - ����� ������
      quUpd.SQL.Clear;
      quUpd.SQL.Add('CREATE PROCEDURE PR_SAVEMOVE ( ');
      quUpd.SQL.Add('    ID_TAB INTEGER,');
      quUpd.SQL.Add('    ID_PERSONAL INTEGER,');
      quUpd.SQL.Add('    ID_STATION INTEGER)');
      quUpd.SQL.Add('AS ');
      quUpd.SQL.Add('DECLARE VARIABLE SIFR integer;');
      quUpd.SQL.Add('DECLARE VARIABLE QUANTITY double precision;');
      quUpd.SQL.Add('DECLARE VARIABLE SPERS varchar(50);');
      quUpd.SQL.Add('begin ');
      quUpd.SQL.Add('  SPERS=cast(:ID_PERSONAL as varchar(50));');
      quUpd.SQL.Add('  SPERS=''����� �������� - ''||:SPERS;');
      quUpd.SQL.Add('  for SELECT cs.SIFR,cs.QUANTITY ');
      quUpd.SQL.Add('      FROM CURSPEC cs ');
      quUpd.SQL.Add('      left join MENU me on cs.SIFR=me.SIFR ');
      quUpd.SQL.Add('      where cs.Station=:ID_STATION ');
      quUpd.SQL.Add('      and cs.ID_TAB=:ID_TAB ');
      quUpd.SQL.Add('      and cs.ISTATUS=0 ');
      quUpd.SQL.Add('      and me.PRNREST=1 ');
      quUpd.SQL.Add('      into :SIFR,:QUANTITY ');
      quUpd.SQL.Add('  do ');
      quUpd.SQL.Add('  begin ');
      quUpd.SQL.Add('    QUANTITY=:QUANTITY*(-1);');
      quUpd.SQL.Add('    execute procedure PR_ADDMOVESL :SIFR,:SPERS,:QUANTITY,2;');
      quUpd.SQL.Add('  end');
      quUpd.SQL.Add('  SUSPEND;');
      quUpd.SQL.Add('END ');
      quUpd.ExecQuery;
      delay(100);
    end;

    quProcDBList.Active:=False;
    quProcDBList.ParamByName('SPROCNAME').AsString:='PR_SAVETAB';
    quProcDBList.Active:=True;
    if quProcDBList.RecordCount>0 then
    begin //��������� ���� - �������
      quUpd.SQL.Clear;

      quUpd.SQL.Add('ALTER PROCEDURE PR_SAVETAB ( ');
      quUpd.SQL.Add('    ID_TAB INTEGER,          ');
      quUpd.SQL.Add('    ID_PERSONAL INTEGER,     ');
      quUpd.SQL.Add('    NUMTABLE VARCHAR (20),   ');
      quUpd.SQL.Add('    QUESTS INTEGER,          ');
      quUpd.SQL.Add('    TABSUM DOUBLE PRECISION, ');
      quUpd.SQL.Add('    BEGTIME TIMESTAMP,       ');
      quUpd.SQL.Add('    ISTATUS INTEGER,         ');
      quUpd.SQL.Add('    DBAR VARCHAR (20),       ');
      quUpd.SQL.Add('    ID_STATION INTEGER,      ');
      quUpd.SQL.Add('    NUMZ INTEGER,            ');
      quUpd.SQL.Add('    SALET INTEGER)           ');
      quUpd.SQL.Add('AS                           ');
      quUpd.SQL.Add('DECLARE VARIABLE ID integer; ');
      quUpd.SQL.Add('DECLARE VARIABLE ID_POS integer; ');
      quUpd.SQL.Add('DECLARE VARIABLE SIFR integer;   ');
      quUpd.SQL.Add('DECLARE VARIABLE PRICE double precision;   ');
      quUpd.SQL.Add('DECLARE VARIABLE QUANTITY double precision;');
      quUpd.SQL.Add('DECLARE VARIABLE DPROC FLOAT;              ');
      quUpd.SQL.Add('DECLARE VARIABLE DSUM double precision;    ');
      quUpd.SQL.Add('DECLARE VARIABLE ITYPE integer;            ');
      quUpd.SQL.Add('DECLARE VARIABLE SUMMA double precision;   ');
      quUpd.SQL.Add('DECLARE VARIABLE LIMITM integer;           ');
      quUpd.SQL.Add('DECLARE VARIABLE LINKM integer;            ');
      quUpd.SQL.Add('DECLARE VARIABLE SUMALL double precision;  ');
      quUpd.SQL.Add('DECLARE VARIABLE ENDTIME TIMESTAMP;        ');
      quUpd.SQL.Add('DECLARE VARIABLE ITYPESAVE integer;        ');
      quUpd.SQL.Add('declare variable SNAME VARCHAR (200);      ');
      quUpd.SQL.Add('DECLARE VARIABLE CUR_POS integer;          ');
      quUpd.SQL.Add('DECLARE VARIABLE SIFRM integer;            ');
      quUpd.SQL.Add('DECLARE VARIABLE QUEST smallint;           ');
      quUpd.SQL.Add('begin                                      ');
      quUpd.SQL.Add('  ENDTIME = cast(''now'' as timestamp);    ');
      quUpd.SQL.Add('  ITYPESAVE=2;                             ');
      quUpd.SQL.Add('  execute procedure PR_SAVEMOVE :ID_TAB,:ID_PERSONAL,:ID_STATION;');
      quUpd.SQL.Add('  if (exists(select * from tables where ID=:ID_TAB)) then        ');
      quUpd.SQL.Add('  begin                                                          ');
      quUpd.SQL.Add('    UPDATE TABLES                                                ');
      quUpd.SQL.Add('    SET                                                          ');
      quUpd.SQL.Add('      ID_PERSONAL = :ID_PERSONAL,                                ');
      quUpd.SQL.Add('      NUMTABLE = :NUMTABLE,                                      ');
      quUpd.SQL.Add('      QUESTS = :QUESTS,                                          ');
      quUpd.SQL.Add('      TABSUM = :TABSUM,                                          ');
      quUpd.SQL.Add('      BEGTIME = :BEGTIME,                                        ');
      quUpd.SQL.Add('      ENDTIME = :ENDTIME,                                        ');
      quUpd.SQL.Add('      ISTATUS = :ISTATUS,                                        ');
      quUpd.SQL.Add('      DISCONT = :DBAR,                                           ');
      quUpd.SQL.Add('      NUMZ = :NUMZ,                                              ');
      quUpd.SQL.Add('      SALET = :SALET                                             ');
      quUpd.SQL.Add('    WHERE                                                        ');
      quUpd.SQL.Add('      ID = :ID_TAB;                                              ');
      quUpd.SQL.Add('  end else                                                       ');
      quUpd.SQL.Add('  begin                                                          ');
      quUpd.SQL.Add('    INSERT INTO TABLES(ID,ID_PERSONAL,NUMTABLE,QUESTS,TABSUM,BEGTIME,ENDTIME,ISTATUS,DISCONT,NUMZ,STATION,SALET)      ');
      quUpd.SQL.Add('    VALUES(:ID_TAB,:ID_PERSONAL,:NUMTABLE,:QUESTS,:TABSUM,:BEGTIME,:ENDTIME,:ISTATUS,:DBAR,:NUMZ,:ID_STATION,:SALET); ');
      quUpd.SQL.Add('  end                                                                                                                 ');
      quUpd.SQL.Add('  if (ITYPESAVE=1) then                                                                                               ');
      quUpd.SQL.Add('  begin                                                                                                               ');
      quUpd.SQL.Add('     FOR SELECT cs.ID,cs.SIFR,cs.PRICE,cs.QUANTITY,cs.DPROC,cs.DSUM,cs.SUMMA,cs.LIMITM,cs.LINKM                       ');
      quUpd.SQL.Add('         FROM CURSPEC cs                                                                                              ');
      quUpd.SQL.Add('         where cs.ID_TAB=:ID_TAB and STATION=:ID_STATION                                                              ');
      quUpd.SQL.Add('         Order by cs.ID                                                                                               ');
      quUpd.SQL.Add('        into :ID_POS,:SIFR,:PRICE,:QUANTITY,:DPROC,:DSUM,:SUMMA,:LIMITM,:LINKM                                        ');
      quUpd.SQL.Add('     do begin                                                                                                         ');
      quUpd.SQL.Add('         INSERT INTO SPEC(ID_TAB,ID,ID_POS,ID_PERSONAL,NUMTABLE,SIFR,PRICE,QUANTITY,                                  ');
      quUpd.SQL.Add('           DISCOUNTPROC,DISCOUNTSUM,SUMMA,ISTATUS,LIMITM,LINKM,ITYPE)                                                 ');
      quUpd.SQL.Add('         VALUES(:ID_TAB,:ID_POS,0,:ID_PERSONAL,:NUMTABLE,:SIFR,:PRICE,:QUANTITY,                                      ');
      quUpd.SQL.Add('           :DPROC,:DSUM,:SUMMA,1,:LIMITM,:LINKM,0);                                                                   ');
      quUpd.SQL.Add('     end                                                                                                              ');
      quUpd.SQL.Add('     FOR SELECT cm.ID,cm.ID_POS,cm.SIFR,cm.QUANTITY                                                                   ');
      quUpd.SQL.Add('         FROM CURMOD cm                                                                                               ');
      quUpd.SQL.Add('         where cm.ID_TAB=:ID_TAB and STATION=:ID_STATION                                                              ');
      quUpd.SQL.Add('         Order by cm.ID,cm.ID_POS                                                                                     ');
      quUpd.SQL.Add('         into :ID,:ID_POS,:SIFR,:QUANTITY                                                                             ');
      quUpd.SQL.Add('     do begin                                                                                                         ');
      quUpd.SQL.Add('         INSERT INTO SPEC(ID_TAB,ID,ID_POS,ID_PERSONAL,NUMTABLE,SIFR,PRICE,QUANTITY,                                  ');
      quUpd.SQL.Add('           DISCOUNTPROC,DISCOUNTSUM,SUMMA,ISTATUS,LIMITM,LINKM,ITYPE)                                                 ');
      quUpd.SQL.Add('         VALUES(:ID_TAB,:ID,:ID_POS,:ID_PERSONAL,:NUMTABLE,:SIFR,0,:QUANTITY,                                         ');
      quUpd.SQL.Add('           0,0,0,1,0,0,1);                                                                                            ');
      quUpd.SQL.Add('     end                                                                                                              ');
      quUpd.SQL.Add('  end                                                                                                                 ');
      quUpd.SQL.Add('  if (ITYPESAVE=2) then /* ����������� �� �������*/                                                                   ');
      quUpd.SQL.Add('  begin                                                                                                               ');
      quUpd.SQL.Add('     ID_POS=0;                                                                                                        ');
      quUpd.SQL.Add('     Delete from Spec where ID_TAB=:ID_TAB;                                                                           ');
      quUpd.SQL.Add('     FOR  SELECT cs.NAME,cs.SIFR,cs.PRICE,cs.LIMITM,cs.LINKM, cs.QUEST,                                               ');
      quUpd.SQL.Add('             SUM(cs.QUANTITY) as QUANTITY ,MAX(cs.DPROC) as DPROC,SUM(cs.DSUM) as DSUM,SUM(cs.SUMMA) as SUMMA         ');
      quUpd.SQL.Add('          FROM CURSPEC cs                                                                                             ');
      quUpd.SQL.Add('          left join MENU me on cs.SIFR=me.SIFR                                                                        ');
      quUpd.SQL.Add('             where cs.ID_TAB=:ID_TAB and STATION=:ID_STATION                                                          ');
      quUpd.SQL.Add('          Group by cs.NAME,cs.SIFR,cs.PRICE,cs.LIMITM,cs.LINKM,cs.QUEST                                               ');
      quUpd.SQL.Add('          Order by cs.QUEST,cs.NAME                                                                                   ');
      quUpd.SQL.Add('          into :SNAME,:SIFR,:PRICE,:LIMITM,:LINKM,:QUEST,:QUANTITY,:DPROC,:DSUM,:SUMMA                                ');
      quUpd.SQL.Add('     do begin                                                                                                         ');
      quUpd.SQL.Add('         ID_POS=:ID_POS+1;                                                                                            ');
      quUpd.SQL.Add('         INSERT INTO SPEC(ID_TAB,ID,ID_POS,ID_PERSONAL,NUMTABLE,SIFR,PRICE,QUANTITY,                                  ');
      quUpd.SQL.Add('           DISCOUNTPROC,DISCOUNTSUM,SUMMA,ISTATUS,LIMITM,LINKM,ITYPE,QUEST)                                           ');
      quUpd.SQL.Add('         VALUES(:ID_TAB,:ID_POS,0,:ID_PERSONAL,:NUMTABLE,:SIFR,:PRICE,:QUANTITY,                                      ');
      quUpd.SQL.Add('           :DPROC,:DSUM,:SUMMA,1,:LIMITM,:LINKM,0,:QUEST);                                                            ');
      quUpd.SQL.Add('         CUR_POS=:ID_POS;                                                                                             ');
      quUpd.SQL.Add('         FOR SELECT mm.ID_TAB,mm.SIFR,mm.QUANTITY                                                                     ');
      quUpd.SQL.Add('             FROM CURMOD mm                                                                                           ');
      quUpd.SQL.Add('             left join CURSPEC ss on (ss.STATION=mm.STATION and ss.ID_TAB=mm.ID_TAB and ss.ID=mm.ID_POS)              ');
      quUpd.SQL.Add('             WHERE ss.SIFR=:SIFR and mm.ID_TAB=:ID_TAB and mm.STATION=:ID_STATION and ss.QUEST=:QUEST                 ');
      quUpd.SQL.Add('             ORDER BY mm.ID                                                                                           ');
      quUpd.SQL.Add('             into :ID_TAB,:SIFRM,:QUANTITY                                                                            ');
      quUpd.SQL.Add('         do begin                                                                                                     ');
      quUpd.SQL.Add('            ID_POS=:ID_POS+1;                                                                                         ');
      quUpd.SQL.Add('            if (ID_POS < 300) then                                                                                    ');
      quUpd.SQL.Add('            begin                                                                                                     ');
      quUpd.SQL.Add('               INSERT INTO SPEC(ID_TAB,ID,ID_POS,ID_PERSONAL,NUMTABLE,SIFR,PRICE,QUANTITY,                            ');
      quUpd.SQL.Add('               DISCOUNTPROC,DISCOUNTSUM,SUMMA,ISTATUS,LIMITM,LINKM,ITYPE,QUEST)                                       ');
      quUpd.SQL.Add('               VALUES(:ID_TAB,:ID_POS,:CUR_POS,:ID_PERSONAL,:NUMTABLE,:SIFRM,0,:QUANTITY,                             ');
      quUpd.SQL.Add('               0,0,0,1,0,0,1,:QUEST);                                                                                 ');
      quUpd.SQL.Add('            end                                                                                                       ');
      quUpd.SQL.Add('         end                                                                                                          ');
      quUpd.SQL.Add('     end                                                                                                              ');
      quUpd.SQL.Add('  end                                                                                                                 ');
      quUpd.SQL.Add('  suspend;                      ');
      quUpd.SQL.Add('end                             ');
      quUpd.ExecQuery;
      delay(100);
    end;

  end;
end;

procedure prAddMove(Oper,Code:Integer;rQ:Real);
begin
  with dmC do
  begin

  //EXECUTE PROCEDURE PR_ADDMOVESL (?SIFR, ?PERSON, ?QUANT, ?OPER)

    prADDMOVESL.ParamByName('SIFR').AsInteger:=Code;
    prADDMOVESL.ParamByName('PERSON').AsString:=Person.Name;
    prADDMOVESL.ParamByName('QUANT').AsFloat:=rQ;
    prADDMOVESL.ParamByName('OPER').AsInteger:=Oper;
    prADDMOVESL.ExecProc;

    delay(10);
  end;
end;

function prFindBar(sBar:String):INteger;
begin
  Result:=0;
  with dmC do
  begin
    quFBar.Active:=False;
    quFBar.ParamByName('SBAR').AsString:=sBar;
    quFBar.Active:=True;
    if quFBar.RecordCount=1 then Result:=quFBarSIFR.AsInteger;

    quFBar.Active:=False;
  end;
end;

procedure TdmC.DataModuleCreate(Sender: TObject);
begin
  CasherRnDb.Connected:=False;
  CasherRnDb.DBName:=DBName;
  try
    CasherRnDb.Open;
    prUpd;
  except
  end;
end;

procedure TdmC.DataModuleDestroy(Sender: TObject);
begin
  try
    CasherRnDb.Close;
  except
  end;
end;

Function CanDo(Name_F:String):Boolean;
begin
  result:=True;
  with dmC do
  begin
    quCanDo.Active:=False;
    quCanDo.ParamByName('Person_id').Value:=Person.Id;
    quCanDo.ParamByName('Name_F').Value:=Name_F;
    quCanDo.Active:=True;
    if quCanDoprExec.AsBoolean=True then Result:=False;
  end;
end;


procedure TdmC.quMenuSelCalcFields(DataSet: TDataSet);
begin
  if quMenuSelALLTIME.AsInteger<>1 then
  begin
    quMenuSelTIMEBB.AsString:=FormatDateTime('hh:nn',quMenuSelTIMEB.AsDateTime);
    quMenuSelTIMEEE.AsString:=FormatDateTime('hh:nn',quMenuSelTIMEE.AsDateTime);
  end;  
end;

end.
