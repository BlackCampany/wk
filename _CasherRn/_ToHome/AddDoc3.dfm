object fmAddDoc3: TfmAddDoc3
  Left = 317
  Top = 431
  Width = 860
  Height = 532
  Caption = #1042#1085#1091#1090#1088#1077#1085#1085#1080#1077' '#1076#1086#1082#1091#1084#1077#1085#1090#1099
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 852
    Height = 105
    Align = alTop
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 1
    object Label1: TLabel
      Left = 24
      Top = 16
      Width = 65
      Height = 13
      Caption = #1044#1086#1082#1091#1084#1077#1085#1090' '#8470
      Transparent = True
    end
    object Label4: TLabel
      Left = 24
      Top = 48
      Width = 39
      Height = 13
      Caption = #1054#1090' '#1082#1086#1075#1086
      Transparent = True
    end
    object Label5: TLabel
      Left = 24
      Top = 72
      Width = 26
      Height = 13
      Caption = #1050#1086#1084#1091
      Transparent = True
    end
    object Label12: TLabel
      Left = 248
      Top = 16
      Width = 11
      Height = 13
      Caption = #1086#1090
      Transparent = True
    end
    object Label15: TLabel
      Left = 264
      Top = 72
      Width = 201
      Height = 13
      AutoSize = False
      Caption = #1056#1086#1079#1085#1080#1095#1085#1072#1103' '#1094#1077#1085#1072
      Transparent = True
    end
    object Label2: TLabel
      Left = 264
      Top = 48
      Width = 201
      Height = 13
      AutoSize = False
      Caption = #1056#1086#1079#1085#1080#1095#1085#1072#1103' '#1094#1077#1085#1072
      Transparent = True
    end
    object cxTextEdit1: TcxTextEdit
      Left = 112
      Top = 12
      Properties.MaxLength = 15
      TabOrder = 0
      Text = 'cxTextEdit1'
      Width = 121
    end
    object cxDateEdit1: TcxDateEdit
      Left = 272
      Top = 12
      Style.BorderStyle = ebsOffice11
      TabOrder = 1
      Width = 121
    end
    object cxLookupComboBox1: TcxLookupComboBox
      Left = 112
      Top = 68
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Caption = #1053#1072#1079#1074#1072#1085#1080#1077
          FieldName = 'NAMEMH'
        end>
      Properties.ListOptions.AnsiSort = True
      Properties.ListSource = dmO.dsMHAll
      Properties.OnChange = cxLookupComboBox1PropertiesChange
      Style.BorderStyle = ebsOffice11
      Style.LookAndFeel.Kind = lfOffice11
      Style.PopupBorderStyle = epbsDefault
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 2
      Width = 145
    end
    object cxLookupComboBox2: TcxLookupComboBox
      Left = 112
      Top = 44
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Caption = #1053#1072#1079#1074#1072#1085#1080#1077
          FieldName = 'NAMEMH'
        end>
      Properties.ListOptions.AnsiSort = True
      Properties.ListSource = dmO.dsMHAll
      Properties.OnEditValueChanged = cxLookupComboBox2PropertiesEditValueChanged
      Style.BorderStyle = ebsOffice11
      Style.LookAndFeel.Kind = lfOffice11
      Style.PopupBorderStyle = epbsDefault
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 3
      Width = 145
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 479
    Width = 852
    Height = 19
    Color = clWhite
    Panels = <
      item
        Width = 300
      end
      item
        Width = 50
      end>
  end
  object Panel2: TPanel
    Left = 0
    Top = 438
    Width = 852
    Height = 41
    Align = alBottom
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 2
    object cxButton1: TcxButton
      Left = 32
      Top = 8
      Width = 113
      Height = 25
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100'   Ctrl+S'
      TabOrder = 0
      OnClick = cxButton1Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 208
      Top = 8
      Width = 105
      Height = 25
      Caption = #1042#1099#1093#1086#1076'  F10'
      ModalResult = 2
      TabOrder = 1
      OnClick = cxButton2Click
      Glyph.Data = {
        5E040000424D5E04000000000000360000002800000012000000130000000100
        18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
        8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
        0000CED3D6848684848684848684848684848684848684848684848684848684
        848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
        7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
        FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
        FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
        007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
        FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
        00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
        494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
        00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000}
      LookAndFeel.Kind = lfOffice11
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 105
    Width = 153
    Height = 333
    Align = alLeft
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 3
    object cxLabel1: TcxLabel
      Left = 8
      Top = 32
      Cursor = crHandPoint
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1089#1087#1080#1089#1086#1082' Ctrl+Ins'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = 4227072
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel1Click
    end
    object cxLabel2: TcxLabel
      Left = 8
      Top = 72
      Cursor = crHandPoint
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1087#1086#1079#1080#1094#1080#1102'  F8'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clRed
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel2Click
    end
    object cxLabel3: TcxLabel
      Left = 8
      Top = 136
      Cursor = crHandPoint
      Caption = #1055#1077#1088#1077#1089#1095#1080#1090#1072#1090#1100' '#1094#1077#1085#1099' '#1079#1072#1082#1091#1087#1072
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clBlack
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel3Click
    end
    object cxLabel4: TcxLabel
      Left = 7
      Top = 160
      Cursor = crHandPoint
      Caption = #1059#1088#1072#1074#1085#1103#1090#1100' '#1094#1077#1085#1099
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clBlack
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel4Click
    end
    object cxLabel5: TcxLabel
      Left = 8
      Top = 16
      Cursor = crHandPoint
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1087#1086#1079#1080#1094#1080#1102' Ins'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = 4227072
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel5Click
    end
    object cxLabel6: TcxLabel
      Left = 8
      Top = 88
      Cursor = crHandPoint
      Caption = #1054#1095#1080#1089#1090#1080#1090#1100' Ctrl+F8'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clRed
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel6Click
    end
  end
  object PageControl1: TPageControl
    Left = 160
    Top = 112
    Width = 565
    Height = 321
    ActivePage = TabSheet1
    Style = tsFlatButtons
    TabOrder = 4
    object TabSheet1: TTabSheet
      Caption = #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1103
      object GridDoc3: TcxGrid
        Left = 0
        Top = 0
        Width = 557
        Height = 290
        Align = alClient
        PopupMenu = PopupMenu1
        TabOrder = 0
        LookAndFeel.Kind = lfOffice11
        object ViewDoc3: TcxGridDBTableView
          OnDragDrop = ViewDoc3DragDrop
          OnDragOver = ViewDoc3DragOver
          NavigatorButtons.ConfirmDelete = False
          OnCustomDrawCell = ViewDoc3CustomDrawCell
          OnEditing = ViewDoc3Editing
          OnEditKeyDown = ViewDoc3EditKeyDown
          OnEditKeyPress = ViewDoc3EditKeyPress
          DataController.DataSource = dsSpec
          DataController.Summary.DefaultGroupSummaryItems = <
            item
              Format = '0.00'
              Kind = skSum
              Position = spFooter
              FieldName = 'Sum1'
              Column = ViewDoc3Sum1
            end
            item
              Format = '0.00'
              Kind = skSum
              Position = spFooter
              FieldName = 'Sum2'
              Column = ViewDoc3Sum2
            end
            item
              Format = '0.00'
              Kind = skSum
              Position = spFooter
              FieldName = 'Sum3'
              Column = ViewDoc3Sum3
            end
            item
              Format = '0.00'
              Kind = skSum
              Position = spFooter
              FieldName = 'SumNac'
              Column = ViewDoc3SumNac
            end
            item
              Format = '0.0'#39'%'#39
              Kind = skAverage
              Position = spFooter
              FieldName = 'ProcNac'
              Column = ViewDoc3ProcNac
            end>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = '0.00'
              Kind = skSum
              FieldName = 'Sum1'
              Column = ViewDoc3Sum1
            end
            item
              Format = '0.00'
              Kind = skSum
              FieldName = 'Sum2'
              Column = ViewDoc3Sum2
            end
            item
              Format = '0.00'
              Kind = skSum
              FieldName = 'Sum3'
              Column = ViewDoc3Sum3
            end
            item
              Format = '0.00'
              Kind = skSum
              FieldName = 'SumNac'
              Column = ViewDoc3SumNac
            end
            item
              Format = ',0.0%;-,0.0%'
              Kind = skAverage
              FieldName = 'ProcNac'
              Column = ViewDoc3ProcNac
            end>
          DataController.Summary.SummaryGroups = <>
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsData.Inserting = False
          OptionsSelection.MultiSelect = True
          OptionsView.Footer = True
          OptionsView.GroupFooters = gfAlwaysVisible
          OptionsView.Indicator = True
          object ViewDoc3Num: TcxGridDBColumn
            Caption = #8470' '#1087#1087
            DataBinding.FieldName = 'Num'
            Options.Editing = False
            Width = 51
          end
          object ViewDoc3IdGoods: TcxGridDBColumn
            Caption = #1050#1086#1076
            DataBinding.FieldName = 'IdGoods'
            Width = 50
          end
          object ViewDoc3NameG: TcxGridDBColumn
            Caption = #1053#1072#1079#1074#1072#1085#1080#1077
            DataBinding.FieldName = 'NameG'
            Width = 179
          end
          object ViewDoc3CType: TcxGridDBColumn
            Caption = #1058#1080#1087' '#1087#1086#1079#1080#1094#1080#1080
            DataBinding.FieldName = 'CType'
            PropertiesClassName = 'TcxImageComboBoxProperties'
            Properties.Items = <
              item
                Description = #1058#1086#1074#1072#1088
                ImageIndex = 0
                Value = 1
              end
              item
                Description = #1059#1089#1083#1091#1075#1080
                ImageIndex = 0
                Value = 2
              end
              item
                Description = #1040#1074#1072#1085#1089#1099
                ImageIndex = 0
                Value = 3
              end
              item
                Description = #1058#1072#1088#1072
                ImageIndex = 0
                Value = 4
              end>
            Styles.Content = dmO.cxStyle25
          end
          object ViewDoc3IM: TcxGridDBColumn
            Caption = #1050#1086#1076' '#1045#1076'.'#1080#1079#1084'.'
            DataBinding.FieldName = 'IM'
            Options.Editing = False
          end
          object ViewDoc3SM: TcxGridDBColumn
            Caption = #1045#1076'.'#1080#1079#1084'.'
            DataBinding.FieldName = 'SM'
            PropertiesClassName = 'TcxButtonEditProperties'
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.ReadOnly = True
            Properties.OnButtonClick = ViewDoc3SMPropertiesButtonClick
            Width = 57
          end
          object ViewDoc3Quant: TcxGridDBColumn
            Caption = #1050#1086#1083'-'#1074#1086
            DataBinding.FieldName = 'Quant'
          end
          object ViewDoc3Price1: TcxGridDBColumn
            Caption = #1062#1077#1085#1072' '#1079#1072#1082#1091#1087'.'
            DataBinding.FieldName = 'Price1'
            Options.Editing = False
          end
          object ViewDoc3Sum1: TcxGridDBColumn
            Caption = #1057#1091#1084#1084#1072' '#1079#1072#1082#1091#1087'.'
            DataBinding.FieldName = 'Sum1'
            Options.Editing = False
          end
          object ViewDoc3Price2: TcxGridDBColumn
            Caption = #1062#1077#1085#1072' '#1091#1095'. '#1088#1072#1089#1093
            DataBinding.FieldName = 'Price2'
            Options.Editing = False
          end
          object ViewDoc3Sum2: TcxGridDBColumn
            Caption = #1057#1091#1084#1084#1072' '#1091#1095'. '#1088#1072#1089#1093
            DataBinding.FieldName = 'Sum2'
            Options.Editing = False
          end
          object ViewDoc3SumNac: TcxGridDBColumn
            Caption = #1057#1091#1084#1084#1072' '#1085#1072#1094#1077#1085#1082#1080
            DataBinding.FieldName = 'SumNac'
            Options.Editing = False
          end
          object ViewDoc3ProcNac: TcxGridDBColumn
            Caption = '% '#1085#1072#1094#1077#1085#1082#1080
            DataBinding.FieldName = 'ProcNac'
            Options.Editing = False
          end
          object ViewDoc3Price3: TcxGridDBColumn
            Caption = #1062#1077#1085#1072' '#1091#1095'. '#1087#1088#1080#1093
            DataBinding.FieldName = 'Price3'
            Options.Editing = False
          end
          object ViewDoc3Sum3: TcxGridDBColumn
            Caption = #1057#1091#1084#1084#1072' '#1091#1095'. '#1087#1088#1080#1093
            DataBinding.FieldName = 'Sum3'
            Options.Editing = False
          end
          object ViewDoc3Remn: TcxGridDBColumn
            DataBinding.FieldName = 'Remn'
            Styles.Content = dmO.cxStyle1
          end
        end
        object LevelDoc3: TcxGridLevel
          GridView = ViewDoc3
        end
      end
    end
  end
  object FormPlacement1: TFormPlacement
    Active = False
    Left = 424
    Top = 176
  end
  object taSpec: TClientDataSet
    Aggregates = <>
    FileName = 'SpecIn1.cds'
    Params = <>
    OnCalcFields = taSpecCalcFields
    Left = 344
    Top = 176
    object taSpecNum: TIntegerField
      FieldName = 'Num'
    end
    object taSpecIdGoods: TIntegerField
      FieldName = 'IdGoods'
    end
    object taSpecNameG: TStringField
      FieldName = 'NameG'
      Size = 200
    end
    object taSpecIM: TIntegerField
      FieldName = 'IM'
    end
    object taSpecSM: TStringField
      FieldName = 'SM'
      Size = 50
    end
    object taSpecKm: TFloatField
      FieldName = 'Km'
    end
    object taSpecQuant: TFloatField
      FieldName = 'Quant'
      OnChange = taSpecQuantChange
      DisplayFormat = '0.000'
      EditFormat = '0.000'
    end
    object taSpecPrice1: TCurrencyField
      FieldName = 'Price1'
      OnChange = taSpecPrice1Change
      EditFormat = ',0.00;-,0.00'
    end
    object taSpecSum1: TCurrencyField
      FieldName = 'Sum1'
      OnChange = taSpecSum1Change
      EditFormat = ',0.00;-,0.00'
    end
    object taSpecPrice2: TCurrencyField
      FieldName = 'Price2'
      OnChange = taSpecPrice2Change
      EditFormat = ',0.00;-,0.00'
    end
    object taSpecSum2: TCurrencyField
      FieldName = 'Sum2'
      OnChange = taSpecSum2Change
      EditFormat = ',0.00;-,0.00'
    end
    object taSpecSumNac: TCurrencyField
      FieldName = 'SumNac'
      EditFormat = ',0.00;-,0.00'
    end
    object taSpecProcNac: TFloatField
      FieldName = 'ProcNac'
      DisplayFormat = ',0.0%;-,0.0%'
    end
    object taSpecPrice3: TCurrencyField
      FieldName = 'Price3'
      DisplayFormat = '0.00'
    end
    object taSpecSum3: TCurrencyField
      FieldName = 'Sum3'
      DisplayFormat = '0.00'
    end
    object taSpecRemn: TFloatField
      DisplayLabel = #1054#1089#1090#1072#1090#1086#1082
      FieldKind = fkCalculated
      FieldName = 'Remn'
      DisplayFormat = '0.000'
      Calculated = True
    end
    object taSpecCType: TSmallintField
      FieldName = 'CType'
    end
  end
  object dsSpec: TDataSource
    DataSet = taSpec
    Left = 344
    Top = 240
  end
  object prCalcPrice: TpFIBStoredProc
    Transaction = dmO.trSelect
    Database = dmO.OfficeRnDb
    SQL.Strings = (
      
        'EXECUTE PROCEDURE PR_CALCPRICE (?INPRICE, ?IDGOOD, ?PRICE0, ?PRI' +
        'CE1)')
    StoredProcName = 'PR_CALCPRICE'
    Left = 240
    Top = 264
  end
  object amDocVn: TActionManager
    Left = 208
    Top = 184
    StyleName = 'XP Style'
    object acAddPos: TAction
      Caption = 'acAddPos'
      ShortCut = 45
      OnExecute = acAddPosExecute
    end
    object acDelPos: TAction
      Caption = 'acDelPos'
      ShortCut = 119
      OnExecute = acDelPosExecute
    end
    object acAddList: TAction
      Caption = 'acAddList'
      ShortCut = 16429
      OnExecute = acAddListExecute
    end
    object acDelAll: TAction
      Caption = 'acDelAll'
      ShortCut = 16503
      OnExecute = acDelAllExecute
    end
    object acSave: TAction
      Caption = 'acSave'
      ShortCut = 16467
      OnExecute = acSaveExecute
    end
    object acMovePos: TAction
      Caption = #1044#1074#1080#1078#1077#1085#1080#1077' '#1090#1086#1074#1072#1088#1072
      ShortCut = 32885
      OnExecute = acMovePosExecute
    end
    object acExit: TAction
      Caption = 'acExit'
      ShortCut = 121
      OnExecute = acExitExecute
    end
  end
  object PopupMenu1: TPopupMenu
    Images = dmO.imState
    Left = 424
    Top = 240
    object N1: TMenuItem
      Caption = #1050#1086#1087#1080#1088#1086#1074#1072#1090#1100' '#1087#1086#1079#1080#1094#1080#1102
      OnClick = N1Click
    end
    object N3: TMenuItem
      Action = acMovePos
      Hint = #1044#1074#1080#1078#1077#1085#1080#1077' '#1090#1086#1074#1072#1088#1072
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Excel1: TMenuItem
      Caption = #1069#1082#1089#1087#1086#1088#1090' '#1074' Excel'
      ImageIndex = 47
      OnClick = Excel1Click
    end
  end
end
