program RnReports;

uses
  Forms,
  MainRep in 'MainRep.pas' {fmMainRep},
  Un1 in 'Un1.pas',
  Dm in 'Dm.pas' {dmC: TDataModule},
  Passw in 'Passw.pas' {fmPerA},
  Period in 'Period.pas' {fmPeriod},
  Tabs in 'Tabs.pas' {fmTabs},
  PeriodUni in 'PeriodUni.pas' {fmPeriodUni},
  SpecSel in 'SpecSel.pas' {fmSpecSel},
  LogSpec in 'LogSpec.pas' {fmLogSpec},
  Period2 in 'Period2.pas' {fmPeriod2},
  RepGridDayTime in 'RepGridDayTime.pas' {fmRDTime},
  RepRealDays in 'RepRealDays.pas' {fmRealDays},
  UnCash in 'UnCash.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfmMainRep, fmMainRep);
  Application.CreateForm(TdmC, dmC);
  Application.CreateForm(TfmTabs, fmTabs);
  Application.CreateForm(TfmPeriodUni, fmPeriodUni);
  Application.CreateForm(TfmSpecSel, fmSpecSel);
  Application.CreateForm(TfmLogSpec, fmLogSpec);
  Application.CreateForm(TfmRDTime, fmRDTime);
  Application.CreateForm(TfmRealDays, fmRealDays);
  Application.Run;
end.
