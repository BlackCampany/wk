unit ClassSel;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, cxControls, cxContainer, cxTreeView, ExtCtrls, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons;

type
  TfmClassSel = class(TForm)
    StatusBar1: TStatusBar;
    ClassTree: TcxTreeView;
    Panel2: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    procedure ClassTreeExpanding(Sender: TObject; Node: TTreeNode;
      var AllowExpansion: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure ClassTreeDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmClassSel: TfmClassSel;

implementation

uses Un1, dmOffice, DMOReps;

{$R *.dfm}

procedure TfmClassSel.ClassTreeExpanding(Sender: TObject; Node: TTreeNode;
  var AllowExpansion: Boolean);
begin
  if Node.getFirstChild.Data = nil then
  begin
    Node.DeleteChildren;
    ClassifExpand(Node,ClassTree,dmO.quClassTree,Person.Id,1);
  end;
end;

procedure TfmClassSel.FormCreate(Sender: TObject);
begin
  ClassTree.Align:=alClient;
  ClassifExpand(nil,ClassTree,dmO.quClassTree,Person.Id,1);
  ClassTree.FullExpand;
  ClassTree.FullCollapse;
end;

procedure TfmClassSel.ClassTreeDblClick(Sender: TObject);
begin
  ModalResult:=mrOk;
end;

end.
