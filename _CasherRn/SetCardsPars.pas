unit SetCardsPars;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxDropDownEdit, cxCalc, cxControls, cxContainer, cxEdit,
  cxTextEdit, cxMaskEdit, cxSpinEdit, StdCtrls, ExtCtrls, Menus,
  cxLookAndFeelPainters, cxButtons, cxCheckBox, cxGraphics, cxLookupEdit,
  cxDBLookupEdit, cxDBLookupComboBox, cxButtonEdit;

type
  TfmSetCardsParams = class(TForm)
    Panel1: TPanel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    cxSpinEdit1: TcxSpinEdit;
    cxSpinEdit2: TcxSpinEdit;
    cxCalcEdit3: TcxCalcEdit;
    Label1: TLabel;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    cxCheckBox1: TcxCheckBox;
    cxCheckBox2: TcxCheckBox;
    cxCheckBox3: TcxCheckBox;
    Label2: TLabel;
    cxCalcEdit1: TcxCalcEdit;
    cxCheckBox4: TcxCheckBox;
    Label3: TLabel;
    cxCheckBox5: TcxCheckBox;
    Label4: TLabel;
    cxCheckBox6: TcxCheckBox;
    cxButtonEdit1: TcxButtonEdit;
    cxLookupComboBox1: TcxLookupComboBox;
    Label5: TLabel;
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure FormCreate(Sender: TObject);
    procedure Label5Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmSetCardsParams: TfmSetCardsParams;

implementation

uses Makers, DMOReps;

{$R *.dfm}

procedure TfmSetCardsParams.cxButtonEdit1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  fmMakers.Show;
end;

procedure TfmSetCardsParams.FormCreate(Sender: TObject);
begin
  fmSetCardsParams.cxButtonEdit1.Tag:=0;
  fmSetCardsParams.cxButtonEdit1.Text:='';
  fmSetCardsParams.cxLookupComboBox1.EditValue:=0;
end;

procedure TfmSetCardsParams.Label5Click(Sender: TObject);
begin
  fmSetCardsParams.cxButtonEdit1.Tag:=0;
  fmSetCardsParams.cxButtonEdit1.Text:='';
end;

end.
