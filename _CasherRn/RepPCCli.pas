unit RepPCCli;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, Placemnt, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxControls, cxGridCustomView, cxGrid, SpeedBar, ExtCtrls, ComCtrls,
  cxCurrencyEdit,ComObj, ActiveX, Excel2000, OleServer, ExcelXP;

type
  TfmPCCli = class(TForm)
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem2: TSpeedItem;
    GridPCCli: TcxGrid;
    ViewPCCli: TcxGridDBTableView;
    LevelPCCli: TcxGridLevel;
    FormPlacement1: TFormPlacement;
    ViewPCCliDISCONT: TcxGridDBColumn;
    ViewPCCliDCNAME: TcxGridDBColumn;
    ViewPCCliSUM: TcxGridDBColumn;
    ViewPCCliSUM1: TcxGridDBColumn;
    ViewPCCliSITOG: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SpeedItem4Click(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;


var
  fmPCCli: TfmPCCli;

implementation

uses Un1, DmRnDisc, Period;

{$R *.dfm}

procedure TfmPCCli.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  GridPCCli.Align:=AlClient;
  ViewPCCli.RestoreFromIniFile(CurDir+GridIni);
end;

procedure TfmPCCli.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewPCCli.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmPCCli.SpeedItem4Click(Sender: TObject);
begin
  close;
end;

procedure TfmPCCli.SpeedItem3Click(Sender: TObject);
begin
  fmPeriod:=TfmPeriod.Create(Application);
  fmPeriod.DateTimePicker1.Date:=TrebSel.DateFrom;
  fmPeriod.DateTimePicker2.Date:=TrebSel.DateTo;
  fmPeriod.DateTimePicker3.Visible:=True;
  fmPeriod.DateTimePicker3.Time:=Frac(TrebSel.DateFrom);
  fmPeriod.DateTimePicker4.Visible:=True;
  fmPeriod.DateTimePicker4.Time:=Frac(TrebSel.DateTo);

  fmPeriod.cxCheckBox1.Visible:=False;

  fmPeriod.ShowModal;
  if fmPeriod.ModalResult=mrOk then
  begin
    with dmCDisc do
    begin
      fmPeriod.Release;

      ViewPCCli.BeginUpdate;
      quPCCli.Active:=False;
      quPCCli.ParamByName('DateB').AsDateTime:=TrebSel.DateFrom;
      quPCCli.ParamByName('DateE').AsDateTime:=TrebSel.DateTo;
      quPCCli.Active:=True;
      ViewPCCli.EndUpdate;

      fmPCCli.Caption:='���������� �� �� �� �������� �� ������ � '+FormatDateTime('dd.mm.yyyy hh:nn',TrebSel.DateFrom)+' �� '+FormatDateTime('dd.mm.yyyy hh:nn',TrebSel.DateTo);
      exit;
    end;
  end;
  fmPeriod.Release;
end;

procedure TfmPCCli.SpeedItem2Click(Sender: TObject);
Var ExcelApp, Workbook, Range, Cell1, Cell2, ArrayData  : Variant;
    iCol,iRow:Integer;
    i:Integer;
    NameF:String;
begin
// ����� � Excel
  with dmCDisc do
  begin
    StatusBar1.Panels[0].Text:='����� ���� ������������.'; delay(10);
    ViewPCCli.BeginUpdate;

    dsPCCli.DataSet:=nil;

    quPCCli.Filter:=ViewPCCli.DataController.Filter.FilterText;
    quPCCli.Filtered:=True;

    //����� ������ ������
    if not IsOLEObjectInstalled('Excel.Application') then exit;
    ExcelApp := CreateOleObject('Excel.Application');
    ExcelApp.Application.EnableEvents := false;
    //� ������� �����
    Workbook := ExcelApp.WorkBooks.Add;

    iCol:=ViewPCCli.VisibleColumnCount;
    iRow:=quPCCli.RecordCount+1;

    ArrayData := VarArrayCreate([1,iRow, 1, iCol], varVariant);
    for i:=1 to iCol do
    begin
      ArrayData[1,i] := ViewPCCli.VisibleColumns[i-1].Caption;
    end;

    quPCCli.First;
    iRow:=2;
    While not quPCCli.eof do
    begin
      for i:=1 to iCol do
      begin
        NameF:=ViewPCCli.VisibleColumns[i-1].Name;
        delete(NameF,1,9); //������ �� �������� ������� ViewPCCli
        ArrayData[iRow,i] := quPCCli.Fields.FieldByName(NameF).Value;
      end;
      quPCCli.Next; //Delay(10);
      inc(iRow);
    end;

    StatusBar1.Panels[0].Text:='���� ������� ������.'; delay(10);

    Cell1 := WorkBook.WorkSheets[1].Cells[1,1];
    Cell2 := WorkBook.WorkSheets[1].Cells[1+quPCCli.RecordCount,iCol];
    Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
    Range.Value := ArrayData;

    quPCCli.Filter:='';
    quPCCli.Filtered:=False;

    quPCCli.First;

    dsPCCli.DataSet:=quPCCli;

    ViewPCCli.EndUpdate;
    StatusBar1.Panels[0].Text:='������������ ��.';

    ExcelApp.Visible := true;
  end;
end;

end.
