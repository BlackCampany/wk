object fmTermoPr: TfmTermoPr
  Left = 567
  Top = 367
  BorderStyle = bsDialog
  Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080' '#1090#1077#1088#1084#1086#1087#1088#1080#1085#1090#1077#1088#1072
  ClientHeight = 210
  ClientWidth = 403
  Color = 16776176
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 16
    Width = 191
    Height = 13
    Caption = #1060#1072#1081#1083' '#1096#1072#1073#1083#1086#1085#1072' '#1074#1089#1077#1075#1076#1072': TRF\Termo.trf '
  end
  object Label2: TLabel
    Left = 16
    Top = 52
    Width = 157
    Height = 13
    Caption = #1053#1072#1079#1074#1072#1085#1080#1077' '#1087#1088#1080#1085#1090#1077#1088#1072' ('#1074#1080#1085#1076#1086#1074#1086#1077')'
  end
  object Panel1: TPanel
    Left = 280
    Top = 0
    Width = 123
    Height = 210
    Align = alRight
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 12
      Top = 16
      Width = 93
      Height = 25
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
      TabOrder = 0
      OnClick = cxButton1Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 12
      Top = 60
      Width = 93
      Height = 25
      Caption = #1047#1072#1082#1088#1099#1090#1100
      TabOrder = 1
      OnClick = cxButton2Click
      LookAndFeel.Kind = lfOffice11
    end
  end
  object cxTextEdit1: TcxTextEdit
    Left = 16
    Top = 68
    Style.LookAndFeel.Kind = lfOffice11
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 1
    Text = 'Zebra  LP2824'
    Width = 221
  end
  object cxRadioGroup1: TcxRadioGroup
    Left = 16
    Top = 108
    Caption = '   '#1050#1086#1076#1080#1088#1086#1074#1082#1072'   '
    Properties.Items = <
      item
        Caption = 'Win'
        Value = 1
      end
      item
        Caption = 'Dos'
        Value = 2
      end>
    ItemIndex = 0
    TabOrder = 2
    Height = 81
    Width = 241
  end
  object cxButton3: TcxButton
    Left = 240
    Top = 68
    Width = 33
    Height = 21
    TabOrder = 3
    OnClick = cxButton3Click
    LookAndFeel.Kind = lfOffice11
  end
  object PrintDialog1: TPrintDialog
    Copies = 1
    Left = 228
    Top = 8
  end
end
