unit MenuCr;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Placemnt, ComCtrls, SpeedBar, ExtCtrls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  RXSplit, cxCurrencyEdit, cxImageComboBox, cxTextEdit, XPStyleActnCtrls,
  ActnList, ActnMan, Menus, ToolWin, ActnCtrls, ActnMenus, StdCtrls,
  FR_Class, FR_DSet, FR_DBSet, DBClient, FR_BarC, cxLookAndFeelPainters,
  cxButtons, cxContainer, cxCheckBox, cxCalendar, cxDBLookupComboBox,
  cxTimeEdit, dxmdaset;

type
  TfmMenuCr = class(TForm)
    FormPlacement1: TFormPlacement;
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    MenuTree: TTreeView;
    RxSplitter1: TRxSplitter;
    ViewMenuCr: TcxGridDBTableView;
    LevelMenuCr: TcxGridLevel;
    GridMenuCr: TcxGrid;
    ViewMenuCrSIFR: TcxGridDBColumn;
    ViewMenuCrNAME: TcxGridDBColumn;
    ViewMenuCrPRICE: TcxGridDBColumn;
    ViewMenuCrCODE: TcxGridDBColumn;
    ViewMenuCrLIMITPRICE: TcxGridDBColumn;
    ViewMenuCrCATEG: TcxGridDBColumn;
    ViewMenuCrSTREAM: TcxGridDBColumn;
    ViewMenuCrALTNAME: TcxGridDBColumn;
    ViewMenuCrCONSUMMA: TcxGridDBColumn;
    ViewMenuCrIACTIVE: TcxGridDBColumn;
    am3: TActionManager;
    acEditMGr: TAction;
    acAddMGr: TAction;
    acExit: TAction;
    PopupMenu1: TPopupMenu;
    acAddMGr1: TMenuItem;
    N1: TMenuItem;
    acAddMSubGr: TAction;
    Timer1: TTimer;
    acAddMSubGr1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    acDelMGr: TAction;
    ActionMainMenuBar1: TActionMainMenuBar;
    acOn: TAction;
    acOff: TAction;
    acAddM: TAction;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    ViewMenuCrNAMECA: TcxGridDBColumn;
    ViewMenuCrNAMEMO: TcxGridDBColumn;
    ViewMenuCrIACTIVE1: TcxGridDBColumn;
    acEditM: TAction;
    acDelM: TAction;
    SpeedItem5: TSpeedItem;
    acMOn: TAction;
    acMOff: TAction;
    SpeedItem6: TSpeedItem;
    acPrint: TAction;
    frRep: TfrReport;
    frtaMenuPrint: TfrDBDataSet;
    SpeedItem7: TSpeedItem;
    acPrintCenn: TAction;
    SpeedItem8: TSpeedItem;
    taCenn2: TClientDataSet;
    taCenn2Name: TStringField;
    taCenn2Price: TCurrencyField;
    taCenn2Code: TStringField;
    taCenn2BarCode: TStringField;
    dsCenn: TDataSource;
    frtaCenn: TfrDBDataSet;
    frBarCodeObject1: TfrBarCodeObject;
    acExportMenu: TAction;
    ViewMenuCrNAMESTREAM: TcxGridDBColumn;
    Panel2: TPanel;
    Label1: TLabel;
    cxTextEdit1: TcxTextEdit;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    PopupMenu2: TPopupMenu;
    MenuItem1: TMenuItem;
    ViewMenuCrCNTPRICE: TcxGridDBColumn;
    ViewMenuCrALLTIME: TcxGridDBColumn;
    ViewMenuCrDATEB: TcxGridDBColumn;
    ViewMenuCrDATEE: TcxGridDBColumn;
    ViewMenuCrDAYWEEK: TcxGridDBColumn;
    ViewMenuCrTIMEBB: TcxGridDBColumn;
    ViewMenuCrTIMEEE: TcxGridDBColumn;
    PopupMenu3: TPopupMenu;
    acOn1: TMenuItem;
    acOff1: TMenuItem;
    N7: TMenuItem;
    Excel1: TMenuItem;
    acBizMain: TAction;
    acBizSlave: TAction;
    N8: TMenuItem;
    acBLOn: TAction;
    acBLOff: TAction;
    N9: TMenuItem;
    N10: TMenuItem;
    cxButton3: TcxButton;
    acFindByCode: TAction;
    cxTextEdit2: TcxTextEdit;
    acFBarScan: TAction;
    ViewMenuCrPRNREST: TcxGridDBColumn;
    ViewMenuCrBACKBGR: TcxGridDBColumn;
    acAddToStopList: TAction;
    N11: TMenuItem;
    N12: TMenuItem;
    acDelFromStopL: TAction;
    N13: TMenuItem;
    taCenn2AltName: TStringField;
    taCenn: TdxMemData;
    taCennName: TStringField;
    taCennPrice: TFloatField;
    taCennCode: TStringField;
    taCennBarCode: TStringField;
    taCennAltName: TStringField;
    ViewMenuCrDISPKOEF: TcxGridDBColumn;
    SpeedItem9: TSpeedItem;
    acAddScale: TAction;
    N14: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure MenuTreeExpanding(Sender: TObject; Node: TTreeNode;
      var AllowExpansion: Boolean);
    procedure MenuTreeChange(Sender: TObject; Node: TTreeNode);
    procedure acExitExecute(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure acAddMGrExecute(Sender: TObject);
    procedure acAddMSubGrExecute(Sender: TObject);
    procedure acDelMGrExecute(Sender: TObject);
    procedure acEditMGrExecute(Sender: TObject);
    procedure acOnExecute(Sender: TObject);
    procedure acOffExecute(Sender: TObject);
    procedure acAddMExecute(Sender: TObject);
    procedure ViewMenuCrCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure acEditMExecute(Sender: TObject);
    procedure acDelMExecute(Sender: TObject);
    procedure acMOnExecute(Sender: TObject);
    procedure acMOffExecute(Sender: TObject);
    procedure ViewMenuCrStartDrag(Sender: TObject;
      var DragObject: TDragObject);
    procedure MenuTreeDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure MenuTreeDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure acPrintExecute(Sender: TObject);
    procedure acPrintCennExecute(Sender: TObject);
    procedure acExportMenuExecute(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Excel1Click(Sender: TObject);
    procedure acBLOnExecute(Sender: TObject);
    procedure acBLOffExecute(Sender: TObject);
    procedure acFindByCodeExecute(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure acFBarScanExecute(Sender: TObject);
    procedure cxTextEdit2KeyPress(Sender: TObject; var Key: Char);
    procedure acAddToStopListExecute(Sender: TObject);
    procedure acDelFromStopLExecute(Sender: TObject);
    procedure SpeedItem9Click(Sender: TObject);
    procedure acAddScaleExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

Procedure FillFm1;

var
  fmMenuCr: TfmMenuCr;
  bDr:Boolean=False;
  bDrMenuDay:Boolean=False;

implementation

uses Un1, dmRnEdit, AddCateg, AddM, FindResult1, MainRnEdit, AddBar,
  StopList;

{$R *.dfm}

Procedure FillFm1;
begin
  with dmC do
  begin
    with fmAddM do
    begin
      CloseTe(teBar);

      cxTextEdit1.Text:='';
      cxCalcEdit1.Value:=0;
      cxCalcEdit3.Value:=1;
      cxCurrencyEdit1.Value:=0;

//      cxTextEdit2.Text:='';
      cxLookupComboBox3.EditValue:=Null;
      cxLookupComboBox1.EditValue:=Null;
      cxLookupComboBox2.EditValue:=Null;
      cxLookupComboBox4.EditValue:=Null;

      cxSpinEdit1.EditValue:=0;
      cxCheckBox1.Checked:=False;

      cxCalcEdit2.EditValue:=0;
      cxCheckBox3.Checked:=False;
      cxCheckBox4.Checked:=False;

      if quMenuSel.RecordCount>0 then
      begin
        cxTextEdit1.Text:=quMenuSelName.AsString;
        cxCalcEdit1.Value:=StrToINtDef(quMenuSelCODE.AsString,0);
        cxCalcEdit3.Value:=quMenuSelCONSUMMA.AsFloat;
        cxCurrencyEdit1.Value:=quMenuSelPRICE.AsCurrency;
//        cxTextEdit2.Text:=quMenuSelBARCODE.AsString;
        cxTextEdit3.Text:=quMenuSelALTNAME.AsString;

        cxLookupComboBox3.EditValue:=Round(quMenuSelNALOG.AsFloat);
        cxLookupComboBox1.EditValue:=quMenuSelCATEG.AsInteger;
        cxLookupComboBox2.EditValue:=quMenuSelLINK.AsInteger;
        cxLookupComboBox4.EditValue:=quMenuSelSTREAM.AsInteger;

        if quMenuSelCNTPRICE.AsInteger>0 then cxLookupComboBox5.EditValue:=quMenuSelCNTPRICE.AsInteger
        else cxLookupComboBox5.EditValue:=1;

        cxSpinEdit1.EditValue:=Round(quMenuSelLIMITPRICE.AsFloat);
        if quMenuSelDESIGNSIFR.AsInteger>0 then cxCheckBox1.Checked:=True;

        if quMenuSelALLTIME.AsInteger=1 then
        begin
          cxCheckBox2.Checked:=True;

          fmAddM.Panel2.Enabled:=False;

          cxDateEdit1.EditValue:=null;
          cxDateEdit2.EditValue:=null;

          cxCheckComboBox1.States[0]:=cbsUnChecked;
          cxCheckComboBox1.States[1]:=cbsUnChecked;
          cxCheckComboBox1.States[2]:=cbsUnChecked;
          cxCheckComboBox1.States[3]:=cbsUnChecked;
          cxCheckComboBox1.States[4]:=cbsUnChecked;
          cxCheckComboBox1.States[5]:=cbsUnChecked;
          cxCheckComboBox1.States[6]:=cbsUnChecked;

          cxTimeEdit1.EditValue:=null;
          cxTimeEdit2.EditValue:=null;
        end else
        begin
          cxCheckBox2.Checked:=False;

          fmAddM.Panel2.Enabled:=True;

          cxDateEdit1.EditValue:=quMenuSelDATEB.AsInteger;
          cxDateEdit2.EditValue:=quMenuSelDATEE.AsInteger;

          if pos('1',quMenuSelDAYWEEK.AsString)>0 then cxCheckComboBox1.States[0]:=cbsChecked else cxCheckComboBox1.States[0]:=cbsUnChecked;
          if pos('2',quMenuSelDAYWEEK.AsString)>0 then cxCheckComboBox1.States[1]:=cbsChecked else cxCheckComboBox1.States[1]:=cbsUnChecked;
          if pos('3',quMenuSelDAYWEEK.AsString)>0 then cxCheckComboBox1.States[2]:=cbsChecked else cxCheckComboBox1.States[2]:=cbsUnChecked;
          if pos('4',quMenuSelDAYWEEK.AsString)>0 then cxCheckComboBox1.States[3]:=cbsChecked else cxCheckComboBox1.States[3]:=cbsUnChecked;
          if pos('5',quMenuSelDAYWEEK.AsString)>0 then cxCheckComboBox1.States[4]:=cbsChecked else cxCheckComboBox1.States[4]:=cbsUnChecked;
          if pos('6',quMenuSelDAYWEEK.AsString)>0 then cxCheckComboBox1.States[5]:=cbsChecked else cxCheckComboBox1.States[5]:=cbsUnChecked;
          if pos('7',quMenuSelDAYWEEK.AsString)>0 then cxCheckComboBox1.States[6]:=cbsChecked else cxCheckComboBox1.States[6]:=cbsUnChecked;

          cxTimeEdit1.EditValue:=quMenuSelTIMEB.AsDateTime;
          cxTimeEdit2.EditValue:=quMenuSelTIMEE.AsDateTime;
        end;

        if quMenuSelPRNREST.AsInteger=1 then
        begin
          cxCalcEdit2.EditValue:=quMenuSelBACKBGR.AsFloat;
          cxCheckBox3.Checked:=True;
        end else
        begin
          cxCalcEdit2.EditValue:=0;
          cxCheckBox3.Checked:=False;
        end;

        if quMenuSelDISPKOEF.AsInteger=1 then cxCheckBox4.Checked:=True else cxCheckBox4.Checked:=False;

      end;
    end;
  end;
end;

procedure TfmMenuCr.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  GridMenuCr.Align:=AlClient;
  ViewMenuCr.RestoreFromIniFile(CurDir+GridIni);
  CardsExpandLevel(nil,MenuTree,dmC.quMenuTree);
  MenuTree.FullExpand;
  MenuTree.FullCollapse;


  with dmC do
  begin

    quMenuTree.First;
    quMenuSel.Active:=False;
    quMenuSel.ParamByName('PARENTID').AsInteger:=quMenuTree.fieldbyName('Id').AsInteger;
    quMenuSel.Active:=True;
  end;
  cxTextEdit1.Text:='';
  delay(10);

  if UserColor.Color2<>0 then
  begin
    SpeedBar1.Color:=UserColor.Color2;
    Panel2.Color:=UserColor.Color2;
  end;  

//  dmC.taSpecAllSel.Active:=False;
//  ViewLog.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmMenuCr.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewMenuCr.StoreToIniFile(CurDir+GridIni,False);
end;

procedure TfmMenuCr.MenuTreeExpanding(Sender: TObject; Node: TTreeNode;
  var AllowExpansion: Boolean);
begin
  if Node.getFirstChild.Data = nil then
  begin
    Node.DeleteChildren;
    CardsExpandLevel(Node,MenuTree,dmC.quMenuTree);
  end;
end;

procedure TfmMenuCr.MenuTreeChange(Sender: TObject; Node: TTreeNode);
begin
  if bMenuList then
  begin
    with dmC do
    begin
      ViewMenuCr.BeginUpdate;

      quMenuSel.Active:=False;
      quMenuSel.ParamByName('PARENTID').AsInteger:=Integer(MenuTree.Selected.Data);
      quMenuSel.Active:=True;

      ViewMenuCr.EndUpdate;
    end;
  end;
end;

procedure TfmMenuCr.acExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmMenuCr.acAddMGrExecute(Sender: TObject);
//procedure TfmMenuCr.acAddMGr1Click(Sender: TObject);
Var Id,i:Integer;
begin
  //�������� ������
  if not CanDo('prAddMenuGr') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  with dmC do
  begin
    Id:=0;
    fmAddCateg:=TFmAddCateg.create(Application);
    fmAddCateg.Caption:='���������� ������ ����.';
    fmAddCateg.cxTextEdit1.Text:='';
    fmAddCateg.ShowModal;
    if fmAddCateg.ModalResult=mrOk then
    begin
      try
//        showmessage('�������� ������ ����');

        quMaxIdM.Active:=False;
        quMaxIdM.Active:=True;
        Id:=quMaxIdMMAXID.AsInteger+1;
        quMaxIdM.Active:=False;

        if Id=1 then Id:=30001;

        taMenu.Active:=False;
        taMenu.Active:=True;

        trUpd2.StartTransaction;
        taMenu.Append;
        taMenuSIFR.AsInteger:=Id;
        taMenuNAME.AsString:=Copy(fmAddCateg.cxTextEdit1.Text,1,100);
        taMenuPRICE.AsFloat:=0;
        taMenuCODE.AsString:='0';
        taMenuTREETYPE.AsString:='T';
        taMenuLIMITPRICE.AsFloat:=0;
        taMenuCATEG.AsInteger:=0;
        taMenuPARENT.AsInteger:=0;
        taMenuLACK.AsInteger:=0;
        taMenuDESIGNSIFR.AsInteger:=0;
        taMenuALTNAME.AsString:='';
        taMenuNALOG.AsFloat:=0;
        taMenuBARCODE.AsString:='';
        taMenuIMAGE.AsInteger:=0;
        taMenuCONSUMMA.AsFloat:=0;
        taMenuMINREST.AsInteger:=0;
        taMenuPRNREST.AsInteger:=0;
        taMenuCOOKTIME.AsInteger:=0;
        taMenuDISPENSER.AsInteger:=0;
        taMenuDISPKOEF.AsInteger:=0;
        taMenuACCESS.AsInteger:=0;
        taMenuFLAGS.AsInteger:=0;
        taMenuTARA.AsInteger:=0;
        taMenuCNTPRICE.AsInteger:=0;
        taMenuBACKBGR.AsFloat:=0;
        taMenuIACTIVE.AsInteger:=1;
        taMenuIEDIT.AsInteger:=0;
        taMenu.Post;

        trUpd2.Commit;

        //���� �������� ������ � ����� ��� ����

        prAddRClassif.ParamByName('IDCL').AsInteger:=Id+10000;
        prAddRClassif.ParamByName('TYPECL').AsInteger:=2;
        prAddRClassif.ParamByName('IDPARENT').AsInteger:=0;
        prAddRClassif.ParamByName('NAMECL').AsString:=Copy(fmAddCateg.cxTextEdit1.Text,1,40);
        prAddRClassif.ExecProc;

  {     ��� ����� ���� ������ ������� ���� �� ����� ������ ���� (����� ������ �������)
 }
        quMenuSel.Active:=False;
        quMenuSel.ParamByName('PARENTID').AsInteger:=Id;
        quMenuSel.Active:=True;
      except
      end;
    end;
    fmAddCateg.Release;
    if Id>0 then //������ ��� �������� - ����� �� ���������
    begin
      bMenuList:=False;

      MenuTree.Items.Clear;
      CardsExpandLevel(nil,MenuTree,dmC.quMenuTree);

      for i:=0 to MenuTree.Items.Count-1 do
      if Integer(MenuTree.Items.Item[i].Data) = Id then
      begin
        MenuTree.Items[i].Selected:=True;
        MenuTree.Repaint;
      end;

      bMenuList:=True;
    end;
  end;
end;

procedure TfmMenuCr.Timer1Timer(Sender: TObject);
begin
  if bClear3=True then begin StatusBar1.Panels[0].Text:=''; bClear3:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClear3:=True;
end;


procedure TfmMenuCr.acAddMSubGrExecute(Sender: TObject);
Var Id,IdGr:Integer;
    TreeNode : TTreeNode;
begin
// �������� ���������
  if not CanDo('prAddMenuSubGr') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  if (MenuTree.Items.Count=0)  then
  begin
    showmessage('�������� ������.');
    exit;
  end;
  if (MenuTree.Selected=nil)  then
  begin
    showmessage('�������� ������,���������.');
    exit;
  end;

  with dmC do
  begin
    Id:=0;
    IdGr:=Integer(MenuTree.Selected.data);

    fmAddCateg:=TFmAddCateg.create(Application);
    fmAddCateg.Caption:='���������� � ������ "'+MenuTree.Selected.Text+'" ����� ���������.';
    fmAddCateg.cxTextEdit1.Text:='';
    fmAddCateg.ShowModal;
    if fmAddCateg.ModalResult=mrOk then
    begin
      try
//        showmessage('�������� ������ �������������');

        quMaxIdM.Active:=False;
        quMaxIdM.Active:=True;
        Id:=quMaxIdMMAXID.AsInteger+1;
        quMaxIdM.Active:=False;


        taMenu.Active:=False;
        taMenu.Active:=True;

        trUpd2.StartTransaction;
        taMenu.Append;
        taMenuSIFR.AsInteger:=Id;
        taMenuNAME.AsString:=Copy(fmAddCateg.cxTextEdit1.Text,1,100);
        taMenuPRICE.AsFloat:=0;
        taMenuCODE.AsString:='0';
        taMenuTREETYPE.AsString:='T';
        taMenuLIMITPRICE.AsFloat:=0;
        taMenuCATEG.AsInteger:=0;
        taMenuPARENT.AsInteger:=IdGr;
        taMenuLACK.AsInteger:=0;
        taMenuDESIGNSIFR.AsInteger:=0;
        taMenuALTNAME.AsString:='';
        taMenuNALOG.AsFloat:=0;
        taMenuBARCODE.AsString:='';
        taMenuIMAGE.AsInteger:=0;
        taMenuCONSUMMA.AsFloat:=0;
        taMenuMINREST.AsInteger:=0;
        taMenuPRNREST.AsInteger:=0;
        taMenuCOOKTIME.AsInteger:=0;
        taMenuDISPENSER.AsInteger:=0;
        taMenuDISPKOEF.AsInteger:=0;
        taMenuACCESS.AsInteger:=0;
        taMenuFLAGS.AsInteger:=0;
        taMenuTARA.AsInteger:=0;
        taMenuCNTPRICE.AsInteger:=0;
        taMenuBACKBGR.AsFloat:=0;
        taMenuIACTIVE.AsInteger:=1;
        taMenuIEDIT.AsInteger:=0;
        taMenu.Post;

        trUpd2.Commit;

        //���� �������� ������ � ����� ��� ����
        //����������� � classif � ��������� � rclassif
        
        prAddRClassif.ParamByName('IDCL').AsInteger:=Id+10000;
        prAddRClassif.ParamByName('TYPECL').AsInteger:=2;
        prAddRClassif.ParamByName('IDPARENT').AsInteger:=IdGr;
        prAddRClassif.ParamByName('NAMECL').AsString:=Copy(fmAddCateg.cxTextEdit1.Text,1,40);
        prAddRClassif.ExecProc;

  {     ��� ������� ����� �� ������ �.�. ��������� �������� ����}
      except
      end;
    end;
    if Id>0 then //������ ���� �������� - ����� �� �������� � ������
    begin
      bMenuList:=False;

      TreeNode:=MenuTree.Items.AddChildObject(MenuTree.Selected,Copy(fmAddCateg.cxTextEdit1.Text,1,40), Pointer(Id));
      TreeNode.ImageIndex:=8;
      TreeNode.SelectedIndex:=7;
//      ���������� ������ �� �����
//      MenuTree.Items[MenuTree.Items.Count-1].Selected:=True;

      bMenuList:=True;
    end;
    fmAddCateg.Release;
  end;
end;

procedure TfmMenuCr.acDelMGrExecute(Sender: TObject);
begin
  //������� ������
  if not CanDo('prDelMenuGr') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  if (MenuTree.Items.Count=0)  then
  begin
//    showmessage('�������� ������.');
    exit;
  end;
  if (MenuTree.Selected=nil)  then
  begin
    showmessage('�������� ������,���������.');
    exit;
  end;

  with dmC do
  begin
    if MessageDlg('�� ������������� ������ ������� ������ - "'+MenuTree.Selected.Text+'"?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      if quMenuSel.RecordCount>0 then
      begin
        showmessage('������ �� �����, �������� ����������!!!');
        exit;
      end;
      quMenuFindParent.Active:=False;
      quMenuFindParent.ParamByName('PARENTID').AsInteger:=Integer(MenuTree.Selected.Data);
      quMenuFindParent.Active:=True;

      if quMenuFindParentCOUNTREC.AsInteger>0 then
      begin
        showmessage('������ �� �����, �������� ����������!!!');
        quMenuFindParent.Active:=False;
          exit;
      end;
      quMenuFindParent.Active:=False;

      //���� ����� �� ���� �� �������� ��������

      //������
      prDelRClassif.ParamByName('IDCL').AsInteger:=Integer(MenuTree.Selected.Data)+10000;
      prDelRClassif.ExecProc;

      //������ � ������
      MenuTree.Selected.Delete;

      //��� ����� ���������� ������ �������� - ����� ������� ������� �� ����
      quMenuSel.Active:=False;
      quMenuSel.ParamByName('PARENTID').AsInteger:=Integer(MenuTree.Selected.Data);
      quMenuSel.Active:=True;
    end;
  end;
end;

procedure TfmMenuCr.acEditMGrExecute(Sender: TObject);
Var Id:Integer;
begin
  //�������������� ������
  if not CanDo('prEditMenuGr') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  if (MenuTree.Items.Count=0)  then
  begin
   // showmessage('�������� ������.');
    exit;
  end;
  if (MenuTree.Selected=nil)  then
  begin
    showmessage('�������� ������,���������.');
    exit;
  end;
  with dmC do
  begin
    Id:=Integer(MenuTree.Selected.Data);
    fmAddCateg:=TFmAddCateg.create(Application);
    fmAddCateg.Caption:='��������� �������� ������.';
    fmAddCateg.cxTextEdit1.Text:=MenuTree.Selected.Text;
    fmAddCateg.ShowModal;
    if fmAddCateg.ModalResult=mrOk then
    begin
      try
        prEditRClassif.ParamByName('IDCL').AsInteger:=Id+10000;
        prEditRClassif.ParamByName('NAMECL').AsString:=Copy(fmAddCateg.cxTextEdit1.Text,1,40);
        prEditRClassif.ParamByName('IACTIVE').AsInteger:=1;
        prEditRClassif.ExecProc;
      except
      end;
      MenuTree.Selected.Text:=Copy(fmAddCateg.cxTextEdit1.Text,1,40);
    end;
    fmAddCateg.Release;
  end;
end;

procedure TfmMenuCr.acOnExecute(Sender: TObject);
Var Id:Integer;
begin
  //������� ��������
  if not CanDo('prEditMenuGrOn') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  if (MenuTree.Items.Count=0)  then
  begin
   // showmessage('�������� ������.');
    exit;
  end;
  if (MenuTree.Selected=nil)  then
  begin
    showmessage('�������� ������,���������.');
    exit;
  end;
  with dmC do
  begin
    Id:=Integer(MenuTree.Selected.Data);
    try
      prEditRClassif.ParamByName('IDCL').AsInteger:=Id+10000;
      prEditRClassif.ParamByName('NAMECL').AsString:=MenuTree.Selected.Text;
      prEditRClassif.ParamByName('IACTIVE').AsInteger:=1;
      prEditRClassif.ExecProc;
    except
    end;

    quMenuID.Active:=False;
    quMenuID.ParamByName('IDM').AsInteger:=Id;
    quMenuID.Active:=True;
    if quMenuID.RecordCount>0 then
    begin
      if quMenuID.FieldByName('LINK').AsInteger=0 then
      begin
        MenuTree.Selected.ImageIndex:=8;
        MenuTree.Selected.SelectedIndex:=7;
      end else
      begin
        MenuTree.Selected.ImageIndex:=26;
        MenuTree.Selected.SelectedIndex:=27;
      end;
    end;
    quMenuID.Active:=False;
  end;
end;

procedure TfmMenuCr.acOffExecute(Sender: TObject);
Var Id:Integer;
begin
  //������� ����������
  if not CanDo('prEditMenuGrOn') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  if (MenuTree.Items.Count=0)  then
  begin
   // showmessage('�������� ������.');
    exit;
  end;
  if (MenuTree.Selected=nil)  then
  begin
    showmessage('�������� ������,���������.');
    exit;
  end;
  with dmC do
  begin
    Id:=Integer(MenuTree.Selected.Data);
    try
      prEditRClassif.ParamByName('IDCL').AsInteger:=Id+10000;
      prEditRClassif.ParamByName('NAMECL').AsString:=MenuTree.Selected.Text;
      prEditRClassif.ParamByName('IACTIVE').AsInteger:=0;
      prEditRClassif.ExecProc;
    except
    end;
    MenuTree.Selected.ImageIndex:=17;
    MenuTree.Selected.SelectedIndex:=18;
  end;
end;

procedure TfmMenuCr.acAddMExecute(Sender: TObject);
Var iCode:Integer;
    sWeek:String;
begin
  //���������� �����
  if not CanDo('prAddMenu') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  with dmC do
  begin
    quCategUp.Active:=False;
    quCategUp.Active:=True;
    quModsGr.Active:=False;
    quModsGr.Active:=True;
    quNalog.Active:=False;
    quNalog.Active:=True;
    taMH.Active:=False;
    taMH.Active:=True;

    if taCategSale.Active=False then taCategSale.Active:=True;
    taCategSale.FullRefresh;

    FillFm1;

    prGetId.ParamByName('ITYPE').Value:=8;  //��� ����� � ��
    prGetId.ExecProc;
    iCode:=prGetId.ParamByName('RESULT').Value;
    {
    sBar:=IntToStr(iCode);
    while length(sBar)<10 do sBar:='0'+sBar;
    sBar:='25'+sBar;
    sBar:=ToStandart(sBar);
    }
    fmAddM.Tag:=iCode;
    fmAddM.cxCalcEdit1.EditValue:=0;
    fmAddM.cxCalcEdit3.EditValue:=1;

//    fmAddM.cxTextEdit2.Text:=sBar;
    fmAddM.cxTextEdit3.Text:='';

    fmAddM.cxCheckBox2.Checked:=True;
    fmAddM.cxCheckBox2.Tag:=0;

    fmAddM.cxCheckBox3.Checked:=False;
    fmAddM.cxCalcEdit2.EditValue:=0;

    fmAddM.ShowModal;
    if fmAddM.ModalResult=mrOk then
    begin
      //��������
      try
   {     quMaxIdMe.Active:=False;
        quMaxIdMe.Active:=True;
        Id:=quMaxIdMeMAXID.AsInteger+1;
        quMaxIdMe.Active:=False;}

        quMenusel.Append;
        quMenuSelSIFR.AsInteger:=iCode;
        quMenuSelNAME.AsString:=fmAddM.cxTextEdit1.Text;
        quMenuSelALTNAME.AsString:=fmAddM.cxTextEdit3.Text;

        quMenuSelPRICE.AsFloat:=fmAddM.cxCurrencyEdit1.EditValue;
        //��� ����
        quMenuSelCODE.AsString:=IntToStr(fmAddM.cxCalcEdit1.EditValue);
        quMenuSelCONSUMMA.AsFloat:=fmAddM.cxCalcEdit3.EditValue;

        quMenuSelTREETYPE.AsString:='F';
        quMenuSelLIMITPRICE.AsFloat:=fmAddM.cxSpinEdit1.EditValue;

        quMenuSelCATEG.AsInteger:=0;
        if fmAddM.cxLookupComboBox1.EditValue<>Null then
          quMenuSelCATEG.AsInteger:=fmAddM.cxLookupComboBox1.EditValue;

        quMenuSelPARENT.AsInteger:=Integer(MenuTree.Selected.Data);

        quMenuSelLINK.AsInteger:=0;
        if fmAddM.cxLookupComboBox2.EditValue<>Null then
          quMenuSelLINK.AsInteger:=fmAddM.cxLookupComboBox2.EditValue;

        quMenuSelSTREAM.AsInteger:=0;
        if fmAddM.cxLookupComboBox4.EditValue<>Null then
        quMenuSelSTREAM.AsInteger:=fmAddM.cxLookupComboBox4.EditValue;

        quMenuSelLACK.AsInteger:=0;

        if fmAddM.cxCheckBox1.Checked then quMenuSelDESIGNSIFR.AsInteger:=1
        else quMenuSelDESIGNSIFR.AsInteger:=0;


        if fmAddM.cxLookupComboBox5.EditValue<>Null then
        quMenuSelCNTPRICE.AsInteger:=fmAddM.cxLookupComboBox5.EditValue
        else quMenuSelCNTPRICE.AsInteger:=1;

        quMenuSelNALOG.AsFloat:=fmAddM.cxLookupComboBox3.EditValue;
//        quMenuSelBARCODE.AsString:=fmAddM.cxTextEdit2.Text;
        quMenuSelIMAGE.AsInteger:=0;

        quMenuSelMINREST.AsInteger:=0;

        if fmAddM.cxCheckBox3.Checked then
        begin
          quMenuSelBACKBGR.AsFloat:=fmAddM.cxCalcEdit2.EditValue;
          quMenuSelPRNREST.AsInteger:=1;
        end else
        begin
          quMenuSelBACKBGR.AsFloat:=fmAddM.cxCalcEdit2.EditValue;
          quMenuSelPRNREST.AsInteger:=0;
        end;

        if fmAddM.cxCheckBox4.Checked then quMenuSelDISPKOEF.AsInteger:=1 else quMenuSelDISPKOEF.AsInteger:=0; //������� ����� ��� ���

        quMenuSelCOOKTIME.AsInteger:=0;
        quMenuSelDISPENSER.AsInteger:=0;

        quMenuSelACCESS.AsInteger:=0;
        quMenuSelFLAGS.AsInteger:=0;
        quMenuSelTARA.AsInteger:=0;
//        quMenuSelBACKBGR.AsFloat:=0;
        quMenuSelFONTBGR.AsFloat:=0;
        quMenuSelIACTIVE.AsInteger:=1;
        quMenuSelIEDIT.AsInteger:=0;

        if fmAddM.cxCheckBox2.Checked then
        begin
          quMenuSelALLTIME.AsInteger:=1;
          quMenuSelDATEB.AsString:='';
          quMenuSelDATEE.AsString:='';
          quMenuSelDAYWEEK.AsString:='';
          quMenuSelTIMEB.AsString:='';
          quMenuSelTIMEE.AsString:='';
        end
        else
        begin
          quMenuSelALLTIME.AsInteger:=0;
          quMenuSelDATEB.AsInteger:=Trunc(fmAddM.cxDateEdit1.Date);
          quMenuSelDATEE.AsInteger:=Trunc(fmAddM.cxDateEdit2.Date);
          quMenuSelDAYWEEK.AsString:='';
          quMenuSelTIMEB.AsDateTime:=fmAddM.cxTimeEdit1.Time;
          quMenuSelTIMEE.AsDateTime:=fmAddM.cxTimeEdit2.Time;

          sWeek:='';
          if fmAddM.cxCheckComboBox1.States[0]=cbsChecked then sWeek:=sWeek+'1';
          if fmAddM.cxCheckComboBox1.States[1]=cbsChecked then sWeek:=sWeek+'2';
          if fmAddM.cxCheckComboBox1.States[2]=cbsChecked then sWeek:=sWeek+'3';
          if fmAddM.cxCheckComboBox1.States[3]=cbsChecked then sWeek:=sWeek+'4';
          if fmAddM.cxCheckComboBox1.States[4]=cbsChecked then sWeek:=sWeek+'5';
          if fmAddM.cxCheckComboBox1.States[5]=cbsChecked then sWeek:=sWeek+'6';
          if fmAddM.cxCheckComboBox1.States[6]=cbsChecked then sWeek:=sWeek+'7';

        end;

        quMenuSel.Post;

//�������� ���������
        with fmAddM do
        begin
          try
            fmAddM.VBar.BeginUpdate;

            quFBarC.Active:=False;
            quFBarC.ParamByName('ICODE').AsInteger:=iCode;
            quFBarC.Active:=True;
            quFBarC.First;
            while not quFBarC.Eof do
            begin
              quFBarC.Delete;
            end;

            teBar.First;
            while not teBar.Eof do
            begin
              quFBarC.Append;
              quFBarCBARCODE.AsString:=teBarBarNew.AsString;
              quFBarCSIFR.AsInteger:=iCode;
              quFBarCQUANT.AsFloat:=rv(teBarQuant.AsFloat);
              quFBarCPRICE.AsFloat:=0;
              quFBarC.Post;

              teBar.Next;
            end;
          finally
            teBar.Active:=False;
            fmAddM.VBar.EndUpdate;
          end;
        end;

      except
      end;
      quMenuSel.Refresh;
    end;
  end;
end;

procedure TfmMenuCr.ViewMenuCrCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
Var iType,i:Integer;
begin
  if AViewInfo.GridRecord.Selected then exit;

  iType:=0;
  for i:=0 to ViewMenuCr.ColumnCount-1 do
  begin
    if ViewMenuCr.Columns[i].Name='ViewMenuCrIACTIVE1' then
    begin
      iType:=VarAsType(AViewInfo.GridRecord.DisplayTexts[i], varInteger);
      break;
    end;
  end;

  if iType=0  then
  begin
    ACanvas.Canvas.Brush.Color := clWhite;
    ACanvas.Canvas.Font.Color := clGray;
  end;

end;

procedure TfmMenuCr.acEditMExecute(Sender: TObject);
Var sWeek:String;
begin
//�������������
  if not CanDo('prEditMenu') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  if dmC.quMenuSel.RecordCount=0 then exit;

  with dmC do
  begin
    quCategUp.Active:=False;
    quCategUp.Active:=True;
    quModsGr.Active:=False;
    quModsGr.Active:=True;
    quNalog.Active:=False;
    quNalog.Active:=True;
    taMH.Active:=False;
    taMH.Active:=True;

    if taCategSale.Active=False then taCategSale.Active:=True;
    taCategSale.FullRefresh;

    FillFm1;

    quFBarC.Active:=False;
    quFBarC.ParamByName('ICODE').AsInteger:=quMenuSelSIFR.AsInteger;
    quFBarC.Active:=True;
    quFBarC.First;
    while not quFBarC.Eof do
    begin
      with fmAddM do
      begin
        teBar.Append;
        teBarBarNew.AsString:=quFBarCBARCODE.AsString;
        teBarBarOld.AsString:='';
        teBarQuant.AsFloat:=quFBarCQUANT.AsFloat;
        teBar.Post;
      end;

      quFBarC.Next;
    end;
    quFBarC.Active:=False;

    fmAddM.Tag:=quMenuSelSIFR.AsInteger;

    fmAddM.ShowModal;
    if fmAddM.ModalResult=mrOk then
    begin
      //��������
      try
        quMenusel.Edit;
//        quMenuSelSIFR.AsInteger:=Id;
        quMenuSelNAME.AsString:=fmAddM.cxTextEdit1.Text;
        quMenuSelALTNAME.AsString:=fmAddM.cxTextEdit3.Text;
        quMenuSelPRICE.AsFloat:=fmAddM.cxCurrencyEdit1.EditValue;
        quMenuSelCODE.AsString:=IntToStr(fmAddM.cxCalcEdit1.EditValue);
        quMenuSelCONSUMMA.AsFloat:=fmAddM.cxCalcEdit3.EditValue;
        quMenuSelTREETYPE.AsString:='F';
        quMenuSelLIMITPRICE.AsFloat:=fmAddM.cxSpinEdit1.EditValue;

        quMenuSelCATEG.AsInteger:=0;
        if fmAddM.cxLookupComboBox1.EditValue<>Null then
          quMenuSelCATEG.AsInteger:=fmAddM.cxLookupComboBox1.EditValue;

        quMenuSelPARENT.AsInteger:=Integer(MenuTree.Selected.Data);

        quMenuSelLINK.AsInteger:=0;
        if fmAddM.cxLookupComboBox2.EditValue<>Null then
          quMenuSelLINK.AsInteger:=fmAddM.cxLookupComboBox2.EditValue;

        quMenuSelSTREAM.AsInteger:=0;
        if fmAddM.cxLookupComboBox4.EditValue>0 then
          quMenuSelSTREAM.AsInteger:=fmAddM.cxLookupComboBox4.EditValue;

        if fmAddM.cxLookupComboBox5.EditValue<>Null then
        quMenuSelCNTPRICE.AsInteger:=fmAddM.cxLookupComboBox5.EditValue
        else quMenuSelCNTPRICE.AsInteger:=0;


        quMenuSelLACK.AsInteger:=0;

        if fmAddM.cxCheckBox1.Checked then quMenuSelDESIGNSIFR.AsInteger:=1
        else quMenuSelDESIGNSIFR.AsInteger:=0;

        quMenuSelNALOG.AsFloat:=fmAddM.cxLookupComboBox3.EditValue;
//        quMenuSelBARCODE.AsString:=fmAddM.cxTextEdit2.Text;
        quMenuSelIMAGE.AsInteger:=0;

        if fmAddM.cxCheckBox3.Checked then
        begin
          quMenuSelBACKBGR.AsFloat:=fmAddM.cxCalcEdit2.EditValue;
          quMenuSelPRNREST.AsInteger:=1;
        end else
        begin
          quMenuSelBACKBGR.AsFloat:=fmAddM.cxCalcEdit2.EditValue;
          quMenuSelPRNREST.AsInteger:=0;
        end;

        quMenuSelMINREST.AsInteger:=0;

        quMenuSelCOOKTIME.AsInteger:=0;
        quMenuSelDISPENSER.AsInteger:=0;
        quMenuSelACCESS.AsInteger:=0;
        quMenuSelFLAGS.AsInteger:=0;
        quMenuSelTARA.AsInteger:=0;
//        quMenuSelCNTPRICE.AsInteger:=0;
//        quMenuSelBACKBGR.AsFloat:=0;
        quMenuSelFONTBGR.AsFloat:=0;
        quMenuSelIACTIVE.AsInteger:=1;
        quMenuSelIEDIT.AsInteger:=0;

        if fmAddM.cxCheckBox4.Checked then quMenuSelDISPKOEF.AsInteger:=1 else quMenuSelDISPKOEF.AsInteger:=0; //������� ����� ��� ���

        if fmAddM.cxCheckBox2.Checked then
        begin
          quMenuSelALLTIME.AsInteger:=1;
          quMenuSelDATEB.AsString:='';
          quMenuSelDATEE.AsString:='';
          quMenuSelDAYWEEK.AsString:='';
          quMenuSelTIMEB.AsString:='';
          quMenuSelTIMEE.AsString:='';
        end
        else
        begin
          quMenuSelALLTIME.AsInteger:=0;
          quMenuSelDATEB.AsInteger:=Trunc(fmAddM.cxDateEdit1.Date);
          quMenuSelDATEE.AsInteger:=Trunc(fmAddM.cxDateEdit2.Date);
          quMenuSelTIMEB.AsDateTime:=fmAddM.cxTimeEdit1.Time;
          quMenuSelTIMEE.AsDateTime:=fmAddM.cxTimeEdit2.Time;

          sWeek:='';
          if fmAddM.cxCheckComboBox1.States[0]=cbsChecked then sWeek:=sWeek+'1';
          if fmAddM.cxCheckComboBox1.States[1]=cbsChecked then sWeek:=sWeek+'2';
          if fmAddM.cxCheckComboBox1.States[2]=cbsChecked then sWeek:=sWeek+'3';
          if fmAddM.cxCheckComboBox1.States[3]=cbsChecked then sWeek:=sWeek+'4';
          if fmAddM.cxCheckComboBox1.States[4]=cbsChecked then sWeek:=sWeek+'5';
          if fmAddM.cxCheckComboBox1.States[5]=cbsChecked then sWeek:=sWeek+'6';
          if fmAddM.cxCheckComboBox1.States[6]=cbsChecked then sWeek:=sWeek+'7';

          quMenuSelDAYWEEK.AsString:=sWeek;

        end;


        quMenuSel.Post;

        //�������� ���������
        with fmAddM do
        begin
          try
            fmAddM.VBar.BeginUpdate;

            quFBarC.Active:=False;
            quFBarC.ParamByName('ICODE').AsInteger:=quMenuSelSIFR.AsInteger;
            quFBarC.Active:=True;
            quFBarC.First;
            while not quFBarC.Eof do
            begin
              if teBar.Locate('BarNew',quFBarCBARCODE.AsString,[]) then
              begin
                quFBarC.Edit;
                quFBarCQUANT.AsFloat:=rv(teBarQuant.AsFloat);
                quFBarC.Post;
                teBar.Delete;

                 quFBarC.Next;
              end else  quFBarC.Delete;
            end;

            teBar.First;
            while not teBar.Eof do
            begin
              quFBarC.Append;
              quFBarCBARCODE.AsString:=teBarBarNew.AsString;
              quFBarCSIFR.AsInteger:=quMenuSelSIFR.AsInteger;
              quFBarCQUANT.AsFloat:=rv(teBarQuant.AsFloat);
              quFBarCPRICE.AsFloat:=0;
              quFBarC.Post;

              teBar.Next;
            end;
          finally
            teBar.Active:=False;
            fmAddM.VBar.EndUpdate;
          end;
        end;

      except
      end;
      quMenuSel.Refresh;
    end;
  end;
end;

procedure TfmMenuCr.acDelMExecute(Sender: TObject);
begin
  //������� �����
  if not CanDo('prDelMenu') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  if dmC.quMenuSel.Eof then exit;
  if MessageDlg('�� ������������� ������ ������� �����: "'+dmC.quMenuSelNAME.AsString+'"  ('+dmC.quMenuSelPRICE.AsString+'�.)',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
    with dmC do
    begin
      quMenuSel.Delete;
      quMenuSel.Refresh;
    end;
  end;
end;

procedure TfmMenuCr.acMOnExecute(Sender: TObject);
begin
// �������
  if not CanDo('prOnMenu') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  if dmC.quMenuSel.Eof then exit;
  with dmC do
  begin
    quMenusel.Edit;
    quMenuSelIACTIVE.AsInteger:=1;
    quMenuSel.Post;

    quMenuSel.Refresh;
    quMenuSel.Next;
  end;
end;

procedure TfmMenuCr.acMOffExecute(Sender: TObject);
begin
// ���������
  if not CanDo('prOffMenu') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  if dmC.quMenuSel.Eof then exit;
  with dmC do
  begin
    quMenusel.Edit;
    quMenuSelIACTIVE.AsInteger:=0;
    quMenuSel.Post;

    quMenuSel.Refresh;
    quMenuSel.Next;
  end;
end;

procedure TfmMenuCr.ViewMenuCrStartDrag(Sender: TObject;
  var DragObject: TDragObject);
begin
  if not CanDo('prMoveMenu') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  bDr:=True;
  bDrMenuDay:=True;
end;

procedure TfmMenuCr.MenuTreeDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept:=False;
  if bDr then Accept:=True;
end;

procedure TfmMenuCr.MenuTreeDragDrop(Sender, Source: TObject; X,
  Y: Integer);
Var sGr:String;
    iGr:Integer;
    iCo:Integer;
    i,j: Integer;
    iNum: Integer;
    Rec:TcxCustomGridRecord;

begin
  if bDr then
  begin
    bDr:=False;
    sGr:=MenuTree.DropTarget.Text;
    iGr:=Integer(MenuTree.DropTarget.data);
    iCo:=ViewMenuCr.Controller.SelectedRecordCount;
    if iCo>0 then
    begin
      if MessageDlg('�� ������������� ������ ����������� ��������� ������� ('+IntToStr(iCo)+' ��.) � ������ "'+sGr+'"?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        with dmC do
        begin
          taMenu.Active:=False;
          taMenu.Active:=True;

          for i:=0 to ViewMenuCr.Controller.SelectedRecordCount-1 do
          begin
            Rec:=ViewMenuCr.Controller.SelectedRecords[i];

            for j:=0 to Rec.ValueCount-1 do
            begin
              if ViewMenuCr.Columns[j].Name='ViewMenuCrSIFR' then break;
            end;

            iNum:=Rec.Values[j];
          //��� ��� - ����������

            if taMenu.Locate('SIFR',iNum,[]) then
            begin
              trUpd2.StartTransaction;
              taMenu.Edit;
              taMenuPARENT.AsInteger:=iGr;
              taMenu.Post;
              trUpd2.Commit;
            end;
          end;
          taMenu.Active:=False;
          quMenuSel.FullRefresh;
        end;
      end;
    end;
  end;

{
      if CardsView.Controller.SelectedRecordCount>0 then
      begin
        for i:=0 to CardsView.Controller.SelectedRecordCount-1 do
        begin
          Rec:=CardsView.Controller.SelectedRecords[i];

          for j:=0 to Rec.ValueCount-1 do
          begin
            if CardsView.Columns[j].Name='CardsViewId' then break;
          end;

          iNum:=Rec.Values[j];
          //��� ��� - ����������

          if taGoods.Locate('Id',iNum,[]) then
          begin
            taGoods.Edit;
            taGoodsId_Group.AsInteger:=PosP.IdGr;
            taGoodsId_SubGroup.AsInteger:=PosP.Id-10000;
            taGoods.Post;
          end;
        end;
        quCardSel.Active:=False;
        quCardSel.Active:=True;
      end;

}

      //        quMenuSel.Refresh;

end;

procedure TfmMenuCr.acPrintExecute(Sender: TObject);
begin
//������
  with dmC do
  begin
    taMenuPrint.Active:=False;
    taMenuPrint.Active:=True;

    frRep.LoadFromFile(CurDir + 'Menu.frf');
    frRep.ReportName:='����.';
    frRep.PrepareReport;
    frRep.ShowPreparedReport;

{    frRepLog.LoadFromFile(CurDir + 'PersLog.frf');

    frVariables.Variable['sPeriod']:='c '+FormatDateTime('dd.mm.yyyy hh:nn',CommonSet.DateFrom)+' �� '+FormatDateTime('dd.mm.yyyy hh:nn',CommonSet.DateTo);
    StrWk:=ViewLog.DataController.Filter.FilterCaption;
    while Pos('AND',StrWk)>0 do
    begin
      i:=Pos('AND',StrWk);
      StrWk[i]:=' ';
      StrWk[i+1]:='�';
      StrWk[i+2]:=' ';
    end;
    while Pos('and',StrWk)>0 do
    begin
      i:=Pos('and',StrWk);
      StrWk[i]:=' ';
      StrWk[i+1]:='�';
      StrWk[i+2]:=' ';
    end;

    frVariables.Variable['sDop']:=StrWk;

    frRepLog.ReportName:='�������� �������� ���������.';
    frRepLog.PrepareReport;
    frRepLog.ShowPreparedReport;
    }

  end;
end;

procedure TfmMenuCr.acPrintCennExecute(Sender: TObject);
Type TCen = record
     Name:String;
     Code:String;
     Price:Currency;
     sBar:String;
     sAltName:String;
     end;

Var iCo,i,j:Integer;
    Rec:TcxCustomGridRecord;
    Cen:TCen;

begin
  //������ �������
  iCo:=ViewMenuCr.Controller.SelectedRecordCount;
  if iCo>0 then
  begin
    taCenn.Active:=False;
    taCenn.Open;

//    taCenn.CreateDataSet;

    for i:=0 to ViewMenuCr.Controller.SelectedRecordCount-1 do
    begin
      Rec:=ViewMenuCr.Controller.SelectedRecords[i];

      Cen.Name:='';
      Cen.Code:='';
      Cen.Price:=0;
      Cen.sBar:='';
      Cen.sAltName:='';

      for j:=0 to Rec.ValueCount-1 do
      begin
        if ViewMenuCr.Columns[j].Name='ViewMenuCrNAME' then
          if Rec.Values[j]<>Null then Cen.Name:=Rec.Values[j];
        if ViewMenuCr.Columns[j].Name='ViewMenuCrPRICE' then
          if Rec.Values[j]<>Null then Cen.Price:=Rec.Values[j];
        if ViewMenuCr.Columns[j].Name='ViewMenuCrCODE' then
          if Rec.Values[j]<>Null then Cen.Code:=Rec.Values[j];
        if ViewMenuCr.Columns[j].Name='ViewMenuCrBARCODE' then
          if Rec.Values[j]<>Null then Cen.sBar:=Rec.Values[j];
        if ViewMenuCr.Columns[j].Name='ViewMenuCrALTNAME' then
          if Rec.Values[j]<>Null then Cen.sAltName:=Rec.Values[j];
      end;

      taCenn.Append;
      taCennName.AsString:=Cen.Name;
      taCennPrice.AsCurrency:=Cen.Price;
      taCennCode.AsString:=Cen.Code;
      taCennBarCode.AsString:=Cen.sBar;
      taCennAltName.AsString:=Cen.sAltName;
      taCenn.Post;
    end;

    frRep.LoadFromFile(CurDir + 'Cennik.frf');
    frRep.ReportName:='������ ��������.';
    frVariables.Variable['sDepart']:=CommonSet.DepartName;
    frRep.PrepareReport;
    frRep.ShowPreparedReport;

    taCenn.Close;

  end;
end;

procedure TfmMenuCr.acExportMenuExecute(Sender: TObject);
Var f:TextFile;
    StrWk:String;
begin
  //������� ����
  try
    AssignFile(f,CommonSet.pathexport+'Menu.txt');
    rewrite(f);
    with dmC do
    begin
      taMenuprint.Active:=False;
      taMenuPrint.Active:=True;
      taMenuPrint.First;

      writeLn(f,'');
      StrWk:='rem �����,���������� ���,��������,����,��� ������,�������� ������,��������� ������,��,������ �������������';
      writeLn(f,StrWk);
      writeLn(f,'');
      StrWk:='rem �����������,���������� ���,��������,��� ������,�������� ������';
      writeLn(f,StrWk);
      writeLn(f,'');

      while not taMenuPrint.Eof do
      begin
        StrWk:='�����;'+taMenuPrintSIFR.AsString+';'+taMenuPrintNAME.AsString+';'+taMenuPrintPRICE.AsString+';'+
        taMenuPrintPARENT.AsString+';'+taMenuPrintNAMEGR.AsString+';'+taMenuPrintNALOG.AsString+';'+
        taMenuPrintBARCODE.AsString+';'+taMenuPrintLINK.AsString;
        writeLn(f,StrWk);
        taMenuPrint.Next;
      end;

      quModList.Active:=False;
      quModList.Active:=True;
      quModList.First;
      while not quModList.Eof do
      begin
        StrWk:='�����������;'+quModListSIFR.AsString+';'+quModListNAME.AsString+';'+quModListPARENT.AsString+';'+quModListNAMEGR.AsString;
        writeLn(f,StrWk);
        quModList.Next;
      end;

    end;
  finally
    dmC.taMenuPrint.Active:=False;
    dmC.quModList.Active:=False;
    closefile(f);
  end;
  ShowMessage('������� ��������.');
end;

procedure TfmMenuCr.cxButton1Click(Sender: TObject);
Var i:INteger;
begin
  if cxTextEdit1.Text>'' then
  begin
    with dmC do
    begin
      quFindB.Active:=False;
      quFindB.SelectSQL.Clear;
      quFindB.SelectSQL.Add('SELECT SIFR,NAME,PRICE,CODE,PARENT,CONSUMMA');
      quFindB.SelectSQL.Add('FROM MENU ');
      quFindB.SelectSQL.Add('where UPPER(NAME)like ''%'+AnsiUpperCase(cxTextEdit1.Text)+'%''');
      quFindB.Active:=True;

      fmFind.LevelFind.Visible:=False;
      fmFind.LevelFMenu.Visible:=True;

      fmFind.ShowModal;
      if fmFind.ModalResult=mrOk then
      begin
        if quFindB.RecordCount>0 then
        begin
          bMenuList:=False;
          ViewMenuCr.BeginUpdate;

          for i:=0 to MenuTree.Items.Count-1 Do
          if Integer(MenuTree.Items[i].Data) = quFindBPARENT.AsInteger Then
          begin
            MenuTree.Items[i].Expand(False);
            MenuTree.Repaint;
            MenuTree.Items[i].Selected:=True;
            Break;
          End;
          delay(10);

          quMenuSel.Active:=False;
          quMenuSel.ParamByName('PARENTID').AsInteger:=quFindBPARENT.AsInteger;
          quMenuSel.Active:=True;

          quMenuSel.First;
          quMenuSel.locate('SIFR',quFindBSIFR.AsInteger,[]);

          ViewMenuCr.EndUpdate;
          bMenuList:=True;
        end;
      end;

      fmFind.LevelFind.Visible:=True;
      fmFind.LevelFMenu.Visible:=False;

      cxTextEdit1.Text:='';
      delay(10);
      GridMenuCr.SetFocus;
      ViewMenuCr.Controller.FocusRecord(ViewMenuCr.DataController.FocusedRowIndex,True);
    end;
  end;
end;

procedure TfmMenuCr.cxButton2Click(Sender: TObject);
Var i:INteger;
    iCode:INteger;
begin
  iCode:=StrToIntDef(cxTextEdit1.Text,0);
  if iCode>0 then
  begin
    with dmC do
    begin
      quFindB.Active:=False;
      quFindB.SelectSQL.Clear;
      quFindB.SelectSQL.Add('SELECT SIFR,NAME,PRICE,CODE,PARENT,CONSUMMA');
      quFindB.SelectSQL.Add('FROM MENU ');
      quFindB.SelectSQL.Add('where SIFR = '+IntToStr(iCode));
      quFindB.Active:=True;

      if quFindB.RecordCount=1 then
      begin
        bMenuList:=False;
        ViewMenuCr.BeginUpdate;

        for i:=0 to MenuTree.Items.Count-1 Do
        if Integer(MenuTree.Items[i].Data) = quFindBPARENT.AsInteger Then
        begin
          MenuTree.Items[i].Expand(False);
          MenuTree.Repaint;
          MenuTree.Items[i].Selected:=True;
          Break;
        End;
        delay(10);

        quMenuSel.Active:=False;
        quMenuSel.ParamByName('PARENTID').AsInteger:=quFindBPARENT.AsInteger;
        quMenuSel.Active:=True;

        quMenuSel.First;
        quMenuSel.locate('SIFR',quFindBSIFR.AsInteger,[]);

        ViewMenuCr.EndUpdate;
        bMenuList:=True;

      end else showmessage('����� �� ���� - '+its(iCode)+' �� ������.');

      cxTextEdit1.Text:='';
      delay(10);
      GridMenuCr.SetFocus;
      ViewMenuCr.Controller.FocusRecord(ViewMenuCr.DataController.FocusedRowIndex,True);
    end;
  end;
end;


procedure TfmMenuCr.MenuItem1Click(Sender: TObject);
begin
  fmMainRnEdit.ColorDialog1.Color:=SpeedBar1.Color;
  if fmMainRnEdit.ColorDialog1.Execute then
  begin
    SpeedBar1.Color := fmMainRnEdit.ColorDialog1.Color;
    UserColor.Color2:=fmMainRnEdit.ColorDialog1.Color;
    Panel2.Color:=UserColor.Color2;
    WriteColor;
  end;
end;

procedure TfmMenuCr.FormShow(Sender: TObject);
begin
  dmC.taCategSale.Active:=False;
  dmC.taCategSale.Active:=True;
end;

procedure TfmMenuCr.Excel1Click(Sender: TObject);
begin
  prNExportExel5(ViewMenuCr);
end;

procedure TfmMenuCr.acBLOnExecute(Sender: TObject);
Var Id:Integer;
begin
//������ ���� ����������
  if not CanDo('prEditMenuBL') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  if (MenuTree.Items.Count=0)  then
  begin
   // showmessage('�������� ������.');
    exit;
  end;
  if (MenuTree.Selected=nil)  then
  begin
    showmessage('�������� ������,���������.');
    exit;
  end;
  with dmC do
  begin
    Id:=Integer(MenuTree.Selected.Data);

    quMenuID.Active:=False;
    quMenuID.ParamByName('IDM').AsInteger:=Id;
    quMenuID.Active:=True;
    if quMenuID.RecordCount>0 then
    begin
      quMenuID.Edit;
      quMenuIdLINK.AsInteger:=1;
      quMenuID.Post;
    end;
    quMenuID.Active:=False;

    MenuTree.Selected.ImageIndex:=26;
    MenuTree.Selected.SelectedIndex:=27;
  end;
end;

procedure TfmMenuCr.acBLOffExecute(Sender: TObject);
Var Id:Integer;
begin
//������ ���� �����
  if not CanDo('prEditMenuBL') then begin  StatusBar1.Panels[0].Text:='��� ����'; exit; end;
  if (MenuTree.Items.Count=0)  then
  begin
   // showmessage('�������� ������.');
    exit;
  end;
  if (MenuTree.Selected=nil)  then
  begin
    showmessage('�������� ������,���������.');
    exit;
  end;
  with dmC do
  begin
    Id:=Integer(MenuTree.Selected.Data);

    quMenuID.Active:=False;
    quMenuID.ParamByName('IDM').AsInteger:=Id;
    quMenuID.Active:=True;
    if quMenuID.RecordCount>0 then
    begin
      quMenuID.Edit;
      quMenuIdLINK.AsInteger:=0;
      quMenuID.Post;
    end;
    quMenuID.Active:=False;

    if quMenuID.FieldByName('IACTIVE').AsInteger=1 then
    begin
      MenuTree.Selected.ImageIndex:=8;
      MenuTree.Selected.SelectedIndex:=7;
    end else
    begin
      MenuTree.Selected.ImageIndex:=17;
      MenuTree.Selected.SelectedIndex:=18;
    end;
  end;

end;

procedure TfmMenuCr.acFindByCodeExecute(Sender: TObject);
begin
  cxButton2.Click;
end;

procedure TfmMenuCr.cxButton3Click(Sender: TObject);
Var i:INteger;
    iCode:INteger;
    sBar:String;
begin
  // ����� �� ��


//  iCode:=StrToIntDef(cxTextEdit1.Text,0);
  with dmC do
  begin
    sBar:=cxTextEdit1.Text;
    trim(sBar);
    if sBar>'' then
    begin

    iCode:=prFindBar(sBar);

    if iCode>0 then
    begin
      quFindB.Active:=False;
      quFindB.SelectSQL.Clear;
      quFindB.SelectSQL.Add('SELECT SIFR,NAME,PRICE,CODE,PARENT,CONSUMMA');
      quFindB.SelectSQL.Add('FROM MENU ');
      quFindB.SelectSQL.Add('where SIFR = '+IntToStr(iCode));
      quFindB.Active:=True;

      if quFindB.RecordCount=1 then
      begin
        bMenuList:=False;
        ViewMenuCr.BeginUpdate;

        for i:=0 to MenuTree.Items.Count-1 Do
        if Integer(MenuTree.Items[i].Data) = quFindBPARENT.AsInteger Then
        begin
          MenuTree.Items[i].Expand(False);
          MenuTree.Repaint;
          MenuTree.Items[i].Selected:=True;
          Break;
        End;
        delay(10);

        quMenuSel.Active:=False;
        quMenuSel.ParamByName('PARENTID').AsInteger:=quFindBPARENT.AsInteger;
        quMenuSel.Active:=True;

        quMenuSel.First;
        quMenuSel.locate('SIFR',quFindBSIFR.AsInteger,[]);

        ViewMenuCr.EndUpdate;
        bMenuList:=True;

      end else showmessage('����� �� ���� - '+its(iCode)+' �� ������.');

      cxTextEdit1.Text:='';
      delay(10);
      GridMenuCr.SetFocus;
      ViewMenuCr.Controller.FocusRecord(ViewMenuCr.DataController.FocusedRowIndex,True);

    end else showmessage('����� � �� - '+sBar+' �� ������.');

    end;
  end;
end;


procedure TfmMenuCr.acFBarScanExecute(Sender: TObject);
begin
  cxTextEdit2.Clear;
  cxTextEdit2.SetFocus;
end;

procedure TfmMenuCr.cxTextEdit2KeyPress(Sender: TObject; var Key: Char);
Var sBar:String;
    bCh:Byte;
    iCode,i:INteger;
begin
  bCh:=ord(Key);
  if bCh = 13 then
  begin //���� ����������
    with dmC do
    begin
      sBar:=SOnlyDigit(cxTextEdit2.Text);
      trim(sBar);
      if sBar>'' then
      begin

        iCode:=prFindBar(sBar);

        if iCode>0 then
        begin
          quFindB.Active:=False;
          quFindB.SelectSQL.Clear;
          quFindB.SelectSQL.Add('SELECT SIFR,NAME,PRICE,CODE,PARENT,CONSUMMA');
          quFindB.SelectSQL.Add('FROM MENU ');
          quFindB.SelectSQL.Add('where SIFR = '+IntToStr(iCode));
          quFindB.Active:=True;

          if quFindB.RecordCount=1 then
          begin
            bMenuList:=False;
            ViewMenuCr.BeginUpdate;

            for i:=0 to MenuTree.Items.Count-1 Do
              if Integer(MenuTree.Items[i].Data) = quFindBPARENT.AsInteger Then
              begin
                MenuTree.Items[i].Expand(False);
                MenuTree.Repaint;
                MenuTree.Items[i].Selected:=True;
               Break;
              End;
            delay(10);

            quMenuSel.Active:=False;
            quMenuSel.ParamByName('PARENTID').AsInteger:=quFindBPARENT.AsInteger;
            quMenuSel.Active:=True;

            quMenuSel.First;
            quMenuSel.locate('SIFR',quFindBSIFR.AsInteger,[]);

            ViewMenuCr.EndUpdate;
            bMenuList:=True;

          end else showmessage('����� �� ���� - '+its(iCode)+' �� ������.');

          cxTextEdit1.Text:='';
          delay(10);
          GridMenuCr.SetFocus;
          ViewMenuCr.Controller.FocusRecord(ViewMenuCr.DataController.FocusedRowIndex,True);
        end;
      end else showmessage('����� � �� - '+sBar+' �� ������.');
    end;
  end;
end;

procedure TfmMenuCr.acAddToStopListExecute(Sender: TObject);
// �������� � ���� ����
Var iCo:Integer;
    i,j: Integer;
    iNum: Integer;
    Rec:TcxCustomGridRecord;
begin
//
    bAddSL:=False;
    iCo:=ViewMenuCr.Controller.SelectedRecordCount;
    if iCo>0 then
    begin
      if MenuTree.Selected=nil then showmessage('�������� ������.')
      else
      begin
        with dmC do
        begin
          if MessageDlg('�� ������������� ������ ��������� ��������� ������� ('+IntToStr(iCo)+' ��.) � ���� ���� ?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
          begin
            if fmStopList.Visible then fmStopList.ViewSL.BeginUpdate;
            ViewMenuCr.BeginUpdate;

            for i:=0 to fmMenuCr.ViewMenuCr.Controller.SelectedRecordCount-1 do
            begin
              Rec:=fmMenuCr.ViewMenuCr.Controller.SelectedRecords[i];

              for j:=0 to Rec.ValueCount-1 do
              begin
                if fmMenuCr.ViewMenuCr.Columns[j].Name='ViewMenuCrSIFR' then break;
              end;

              iNum:=Rec.Values[j];
          //��� ��� - ����������

              prAddMove(1,iNum,0);
            end;

            quMenuSel.FullRefresh;
            if fmStopList.Visible then quMenuSL.FullRefresh;

            fmMenuCr.ViewMenuCr.EndUpdate;
            if fmStopList.Visible then fmStopList.ViewSL.EndUpdate;
          end;
        end;
      end;
    end;
end;

procedure TfmMenuCr.acDelFromStopLExecute(Sender: TObject);
// ������� �� ���� ����
Var iCo:Integer;
    i,j: Integer;
    iNum: Integer;
    Rec:TcxCustomGridRecord;
begin
//
    bAddSL:=False;
    iCo:=ViewMenuCr.Controller.SelectedRecordCount;
    if iCo>0 then
    begin
      if MenuTree.Selected=nil then showmessage('�������� ������.')
      else
      begin
        with dmC do
        begin
          if MessageDlg('�� ������������� ������ ������� ��������� ������� ('+IntToStr(iCo)+' ��.) �� ���� ����� ?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
          begin
            if fmStopList.Visible then fmStopList.ViewSL.BeginUpdate;
            ViewMenuCr.BeginUpdate;

            for i:=0 to fmMenuCr.ViewMenuCr.Controller.SelectedRecordCount-1 do
            begin
              Rec:=fmMenuCr.ViewMenuCr.Controller.SelectedRecords[i];

              for j:=0 to Rec.ValueCount-1 do
              begin
                if fmMenuCr.ViewMenuCr.Columns[j].Name='ViewMenuCrSIFR' then break;
              end;

              iNum:=Rec.Values[j];
          //��� ��� - ����������

              prAddMove(3,iNum,0);
            end;

            quMenuSel.FullRefresh;
            if fmStopList.Visible then quMenuSL.FullRefresh;

            fmMenuCr.ViewMenuCr.EndUpdate;
            if fmStopList.Visible then fmStopList.ViewSL.EndUpdate;
          end;
        end;
      end;
    end;

end;

procedure TfmMenuCr.SpeedItem9Click(Sender: TObject);
Var SName,sTehno,sBar:String;
begin
  //������ �������� ����� FR
  with dmC do
  begin
    if quMenuSel.RecordCount>0 then
    begin
      sBar:='';

      sName:=quMenuSelNAME.AsString;
      sTehno:=quMenuSelALTNAME.AsString;

      quFBarC.Active:=False;
      quFBarC.ParamByName('ICODE').AsInteger:=quMenuSelSIFR.AsInteger;
      quFBarC.Active:=True;
      quFBarC.First;
      if quFBarC.RecordCount>0 then sBar:=quFBarCBARCODE.AsString;
      quFBarC.Active:=False;


      frRep.LoadFromFile(CurDir + 'LabelBar.frf');

      frVariables.Variable['SNAME']:=sName;
      frVariables.Variable['SBAR']:=sBar;
      frVariables.Variable['STEHNO']:=sTehno;

      frRep.ReportName:='������ ��������.';
      frRep.PrepareReport;
      frRep.ShowPreparedReport;
    end;
  end;
end;

procedure TfmMenuCr.acAddScaleExecute(Sender: TObject);
Var iCo:Integer;
    i,j,l: Integer;
    iNum,iPlu: Integer;
    Rec:TcxCustomGridRecord;
    sN1,sN2,s:String;
begin
  iCo:=ViewMenuCr.Controller.SelectedRecordCount;
  if iCo>0 then
  begin
    if MenuTree.Selected=nil then showmessage('�������� ������.')
    else
    begin
      with dmC do
      begin
        if MessageDlg('�� ������������� ������ ��������� ��������� ������� ('+IntToStr(iCo)+' ��.) � ������ ��� ����� ?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          ViewMenuCr.BeginUpdate;

          quScales.Active:=False;
          quScales.Active:=True;
          if quScales.RecordCount>0 then
          begin
            quScales.First;
            quScaleItems.Active:=False;
            quScaleItems.ParamByName('IDSC').AsInteger:=quScalesID.AsInteger;
            quScaleItems.Active:=True;

            for i:=0 to fmMenuCr.ViewMenuCr.Controller.SelectedRecordCount-1 do
            begin
              Rec:=fmMenuCr.ViewMenuCr.Controller.SelectedRecords[i];

              for j:=0 to Rec.ValueCount-1 do
              begin
                if fmMenuCr.ViewMenuCr.Columns[j].Name='ViewMenuCrSIFR' then break;
              end;

              iNum:=Rec.Values[j];
              //��� ���

              quMenuID.Active:=False;
              quMenuID.ParamByName('IDM').AsInteger:=iNum;
              quMenuID.Active:=True;
              if quMenuID.RecordCount>0 then
              begin
                if quMenuIDDISPKOEF.AsInteger=1 then //������� �����
                begin
                  sN1:=Copy(quMenuIDNAME.AsString,1,29);
                  l:=28;
                  if (Pos(' ',sN1)>0) and(length(quMenuIDNAME.AsString)>28) then
                  begin
                    s:=sN1[l];     //��� ������ ������ � ����� � ������ �������� - ������ �������������� � ���� ��������
                    while s<>' ' do
                    begin
                      dec(l);
                      s:=sN1[l];
                    end;
                  end;

                  sN1:=Copy(quMenuIDNAME.AsString,1,l);
                  if Length(quMenuIDNAME.AsString)>l then sN2:=Copy(quMenuIDNAME.AsString,(l+1),28) else sN2:='';

                  if quScaleItems.Locate('SIFR',iNum,[]) then
                  begin //����������
                    quScaleItems.Edit;
                    quScaleItemsNAME1.AsString:=sN1;
                    quScaleItemsNAME2.AsString:=sN2;
                    quScaleItemsPRICE.AsFloat:=quMenuIDPRICE.AsFloat;
                    quScaleItemsSTATUS.AsInteger:=0; //�������
                    quScaleItems.Post;
                  end else  //���������
                  begin
                    iPlu:=1;
                    quMaxPlu.Active:=False;
                    quMaxPlu.ParamByName('IDSC').AsInteger:=quScalesID.AsInteger;
                    quMaxPlu.Active:=True;
                    if iPlu<(quMaxPluMAXPLU.AsINteger+1) then iPlu:=(quMaxPluMAXPLU.AsINteger+1);
                    quMaxPlu.Active:=False;

                    quScaleItems.Append;
                    quScaleItemsIDSC.AsInteger:=quScalesID.AsInteger;
                    quScaleItemsPLU.AsInteger:=iPlu;
                    quScaleItemsSIFR.AsInteger:=iNum;
                    quScaleItemsNAME1.AsString:=sN1;
                    quScaleItemsNAME2.AsString:=sN2;
                    quScaleItemsPRICE.AsFloat:=quMenuIDPRICE.AsFloat;
                    quScaleItemsSTATUS.AsInteger:=0; //�������
                    quScaleItemsSHELFLIFE.AsInteger:=1;
                    quScaleItemsTAREW.AsInteger:=0;
                    quScaleItemsGRCODE.AsInteger:=StrToINtDef(CommonSet.PrefixVes,22);
                    quScaleItemsMESSNO.AsInteger:=0;
                    quScaleItems.Post;
                  end;
                end;
              end;
            end;
            quScaleItems.Active:=False;
          end;
          quScales.Active:=False;
          fmMenuCr.ViewMenuCr.EndUpdate;
        end;
      end;
    end;
  end;
end;

end.
