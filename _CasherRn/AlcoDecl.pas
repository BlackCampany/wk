unit AlcoDecl;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxControls, cxContainer, cxEdit, cxTextEdit, cxMemo, SpeedBar,
  ExtCtrls, Placemnt, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, DB, cxDBData, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  ActnList, XPStyleActnCtrls, ActnMan, cxImageComboBox, Menus;

type
  TfmAlcoE = class(TForm)
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    SpeedItem6: TSpeedItem;
    SpeedItem7: TSpeedItem;
    SpeedItem8: TSpeedItem;
    SpeedItem9: TSpeedItem;
    SpeedItem10: TSpeedItem;
    SpeedItem11: TSpeedItem;
    Memo1: TcxMemo;
    FormPlacement1: TFormPlacement;
    ViewAlco: TcxGridDBTableView;
    LevelAlco: TcxGridLevel;
    GridAlco: TcxGrid;
    ViewAlcoIdOrg: TcxGridDBColumn;
    ViewAlcoId: TcxGridDBColumn;
    ViewAlcoDDateB: TcxGridDBColumn;
    ViewAlcoDDateE: TcxGridDBColumn;
    ViewAlcoiT1: TcxGridDBColumn;
    ViewAlcoSPers: TcxGridDBColumn;
    ViewAlcoName: TcxGridDBColumn;
    ViewAlcoINN: TcxGridDBColumn;
    amAlco: TActionManager;
    acPeriod: TAction;
    acAddAlco: TAction;
    acEditAlco: TAction;
    acViewAlco: TAction;
    acDelAlco: TAction;
    ViewAlcoiT2: TcxGridDBColumn;
    ViewAlcoiActive: TcxGridDBColumn;
    acSetSend: TAction;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure acPeriodExecute(Sender: TObject);
    procedure acAddAlcoExecute(Sender: TObject);
    procedure acEditAlcoExecute(Sender: TObject);
    procedure acDelAlcoExecute(Sender: TObject);
    procedure ViewAlcoDblClick(Sender: TObject);
    procedure acViewAlcoExecute(Sender: TObject);
    procedure acSetSendExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAlcoE: TfmAlcoE;

implementation

uses Un1, DMOReps, SelPerSkl, dmOffice, AddAlcoDecl;

{$R *.dfm}

procedure TfmAlcoE.FormCreate(Sender: TObject);
begin
  GridAlco.Align:=AlClient;
  FormPlacement1.IniFileName := CurDir+GridIni;
  FormPlacement1.Active:=True;
  Memo1.Clear;
end;

procedure TfmAlcoE.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmAlcoE.acPeriodExecute(Sender: TObject);
begin
  //
  with dmORep do
  begin
    fmSelPerSkl:=tfmSelPerSkl.Create(Application);
    with fmSelPerSkl do
    begin
      fmSelPerSkl.cxDateEdit1.Date:=CommonSet.ADateBeg;
      fmSelPerSkl.cxDateEdit2.Date:=CommonSet.ADateEnd;

      quMHAll1.Active:=False;
      quMHAll1.ParamByName('IDPERSON').AsInteger:=Person.Id;
      quMHAll1.Active:=True;
      quMHAll1.First;
      cxLookupComboBox1.EditValue:=quMHAll1ID.AsInteger;
    end;
    fmSelPerSkl.ShowModal;
    if fmSelPerSkl.ModalResult=mrOk then
    begin
      CommonSet.ADateBeg:=fmSelPerSkl.cxDateEdit1.Date;
      CommonSet.ADateEnd:=fmSelPerSkl.cxDateEdit2.Date;

      fmAlcoE.Caption:='������ ����������� ��������� �� ������ � '+ds1(CommonSet.ADateBeg)+' �� '+ds1(CommonSet.ADateEnd);
      fmAlcoE.Memo1.Clear;

      fmAlcoE.Memo1.Lines.Add('����� ... ���� ������������ ������.'); delay(10);

      fmAlcoE.ViewAlco.BeginUpdate;
      quAlcoDH.Active:=False;
      quAlcoDH.ParamByName('IDATEB').Value:=Trunc(CommonSet.ADateBeg);
      quAlcoDH.ParamByName('IDATEE').Value:=Trunc(CommonSet.ADateEnd);
      quAlcoDH.Active:=True;
      fmAlcoE.ViewAlco.EndUpdate;

      fmAlcoE.Memo1.Lines.Add('������������ ������ ��.'); delay(10);
    end;

    fmSelPerSkl.Release;
  end;
end;

procedure TfmAlcoE.acAddAlcoExecute(Sender: TObject);
begin
  //�������� ����������
  if not CanDo('prAddAlcoE') then
  begin
    Memo1.Lines.Add('��� ����.'); delay(10);
  end else
  begin
    with dmORep do
    with dmO do
    begin
      if quMHAll.Active=False then
      begin
        quMHAll.ParamByName('IDPERSON').AsInteger:=Person.Id;
        quMHAll.Active:=True;
      end;
      quMHAll.FullRefresh;
      quMHAll.First;

      fmAddAlco.cxLookupComboBox1.EditValue:=quMHAllID.AsInteger;
      fmAddAlco.cxLookupComboBox1.Tag:=0;

      fmAddAlco.cxDateEdit1.Date:=Date-1;
      fmAddAlco.cxDateEdit2.Date:=Date;

      fmAddAlco.cxComboBox1.ItemIndex:=0;
      fmAddAlco.cxComboBox2.ItemIndex:=0;
      fmAddAlco.cxSpinEdit1.Value:=0;
      fmAddAlco.cxSpinEdit1.Properties.ReadOnly:=False;

      fmAddAlco.cxLookupComboBox1.Properties.ReadOnly:=False;
      fmAddAlco.cxDateEdit1.Properties.ReadOnly:=False;
      fmAddAlco.cxDateEdit2.Properties.ReadOnly:=False;
      fmAddAlco.cxComboBox1.Properties.ReadOnly:=False;
      fmAddAlco.cxComboBox2.Properties.ReadOnly:=False;

      fmAddAlco.ViAlco1.OptionsData.Editing:=True;
      fmAddAlco.ViAlco2.OptionsData.Editing:=True;

      fmAddAlco.cxButton1.Enabled:=True;
      fmAddAlco.cxButton2.Enabled:=True;
      fmAddAlco.cxButton3.Enabled:=True;
      fmAddAlco.cxButton4.Enabled:=True; //�����
      fmAddAlco.cxButton5.Enabled:=True;
      fmAddAlco.cxButton6.Enabled:=True;
      fmAddAlco.cxButton7.Enabled:=True;
      fmAddAlco.cxButton8.Enabled:=True;
      fmAddAlco.cxButton9.Enabled:=True;
      fmAddAlco.cxButton10.Enabled:=True;


      CloseTe(fmAddAlco.teAlcoOb);
      CloseTe(fmAddAlco.teAlcoIn);

      fmAddAlco.cxButton1.Enabled:=True;
    end;
    fmAddAlco.ShowModal;
  end;
end;

procedure TfmAlcoE.acEditAlcoExecute(Sender: TObject);
Var IDH:INteger;
//    sInnCli:String;
begin
  //�������������
  if not CanDo('prEditAlcoE') then
  begin
    Memo1.Lines.Add('��� ����.'); delay(10);
  end else
  begin
    with dmORep do
    with dmO do
    begin
      if (quAlcoDH.RecordCount>0)and(quAlcoDHiActive.AsInteger=0) then
      begin
        if quMHAll.Active=False then
        begin
          quMHAll.ParamByName('IDPERSON').AsInteger:=Person.Id;
          quMHAll.Active:=True;
        end;
        quMHAll.FullRefresh;
        quMHAll.First;

        quMHAll.Locate('ID',quAlcoDHIdOrg.AsInteger,[]);

        fmAddAlco.cxLookupComboBox1.EditValue:=quAlcoDHIdOrg.AsInteger;
        fmAddAlco.cxLookupComboBox1.Tag:=quAlcoDHId.AsInteger;
        fmAddAlco.cxLookupComboBox1.Properties.ReadOnly:=False;

        fmAddAlco.cxDateEdit1.Date:=quAlcoDHDDateB.AsDateTime;
        fmAddAlco.cxDateEdit1.Properties.ReadOnly:=False;

        fmAddAlco.cxDateEdit2.Date:=quAlcoDHDDateE.AsDateTime;
        fmAddAlco.cxDateEdit2.Properties.ReadOnly:=False;

        fmAddAlco.cxComboBox1.ItemIndex:=quAlcoDHiT1.AsInteger;
        fmAddAlco.cxComboBox1.Properties.ReadOnly:=False;

        fmAddAlco.cxComboBox2.ItemIndex:=quAlcoDHiT2.AsInteger;
        fmAddAlco.cxComboBox2.Properties.ReadOnly:=False;

        fmAddAlco.cxSpinEdit1.Value:=quAlcoDHiNumCorr.AsInteger;
        fmAddAlco.cxSpinEdit1.Properties.ReadOnly:=False;

        IDH:=quAlcoDHId.AsInteger;

        CloseTe(fmAddAlco.teAlcoOb);
        CloseTe(fmAddAlco.teAlcoIn);

        fmAddAlco.cxButton1.Enabled:=True;

        fmAddAlco.ViAlco1.OptionsData.Editing:=True;
        fmAddAlco.ViAlco2.OptionsData.Editing:=True;

        fmAddAlco.cxButton1.Enabled:=True;
        fmAddAlco.cxButton2.Enabled:=True;
        fmAddAlco.cxButton3.Enabled:=True;
//        fmAddAlco.cxButton4.Enabled:=bStr; //�����
        fmAddAlco.cxButton5.Enabled:=True;
        fmAddAlco.cxButton6.Enabled:=True;
        fmAddAlco.cxButton7.Enabled:=True;
        fmAddAlco.cxButton8.Enabled:=True;
        fmAddAlco.cxButton9.Enabled:=True;
        fmAddAlco.cxButton10.Enabled:=True;

        fmAddAlco.ViAlco1.BeginUpdate;
        fmAddAlco.ViAlco2.BeginUpdate;

        fmAddAlco.dsteAlcoOb.DataSet:=nil;
        fmAddAlco.dsteAlcoIn.DataSet:=nil;


        quAlcoDS1.Active:=False;
        quAlcoDS1.ParamByName('IDH').Value:=IDH;
        quAlcoDS1.Active:=True;

        quAlcoDS1.First;
        while not quAlcoDS1.Eof do
        begin
          with fmAddAlco do
          begin
            teAlcoOb.Append;
            teAlcoObiDep.AsInteger:=quAlcoDS1iDep.AsInteger;
            teAlcoObsDep.AsString:=quAlcoDS1NameShop.AsString;
            teAlcoObiVid.AsInteger:=quAlcoDS1iVid.AsInteger;
            teAlcoObsVid.AsString:=quAlcoDS1iVid.AsString;
            teAlcoObsVidName.AsString:=quAlcoDS1NameVid.AsString;
            teAlcoObiProd.AsInteger:=quAlcoDS1iProd.AsInteger;
            teAlcoObsProd.AsString:=quAlcoDS1NameProducer.AsString;
            teAlcoObsProdInn.AsString:=quAlcoDS1INN.AsString;
            teAlcoObsProdKPP.AsString:=quAlcoDS1KPP.AsString;
            teAlcoObrQb.AsFloat:=quAlcoDS1rQb.AsFloat;
            teAlcoObrQIn1.AsFloat:=quAlcoDS1rQIn1.AsFloat;
            teAlcoObrQIn2.AsFloat:=quAlcoDS1rQIn2.AsFloat;
            teAlcoObrQIn3.AsFloat:=quAlcoDS1rQIn3.AsFloat;
            teAlcoObrQInIt1.AsFloat:=quAlcoDS1rQInIt1.AsFloat;
            teAlcoObrQIn4.AsFloat:=quAlcoDS1rQIn4.AsFloat;
            teAlcoObrQIn5.AsFloat:=quAlcoDS1rQIn5.AsFloat;
            teAlcoObrQIn6.AsFloat:=quAlcoDS1rQIn6.AsFloat;
            teAlcoObrQInIt.AsFloat:=quAlcoDS1rQInIt.AsFloat;
            teAlcoObrQOut1.AsFloat:=quAlcoDS1rQOut1.AsFloat;
            teAlcoObrQOut2.AsFloat:=quAlcoDS1rQOut2.AsFloat;
            teAlcoObrQOut3.AsFloat:=quAlcoDS1rQOut3.AsFloat;
            teAlcoObrQOut4.AsFloat:=quAlcoDS1rQOut4.AsFloat;
            teAlcoObrQOutIt.AsFloat:=quAlcoDS1rQOutIt.AsFloat;
            teAlcoObrQe.AsFloat:=quAlcoDS1rQe.AsFloat;
            teAlcoOb.Post;
          end;
          quAlcoDS1.Next;
        end;
        quAlcoDS1.Active:=False;

        quAlcoDS2.Active:=False;
        quAlcoDS2.ParamByName('IDH').Value:=IDH;
        quAlcoDS2.Active:=True;

        quAlcoDS2.First;
        while not quAlcoDS2.Eof do
        begin
          with fmAddAlco do
          begin
            teAlcoIn.Append;
            teAlcoIniDep.AsInteger:=quAlcoDS2iDep.AsInteger;
            teAlcoInsDep.AsString:=quAlcoDS2NameShop.AsString;
            teAlcoIniVid.AsInteger:=quAlcoDS2iVid.AsInteger;
            teAlcoInsVid.AsString:=quAlcoDS2iVid.AsString;
            teAlcoInsVidName.AsString:=quAlcoDS2NAMEVID.AsString;
            teAlcoIniProd.AsInteger:=quAlcoDS2iProd.AsInteger;
            teAlcoInsProd.AsString:=quAlcoDS2NameProducer.AsString;
            teAlcoInsProdInn.AsString:=quAlcoDS2INN.AsString;
            teAlcoInsProdKPP.AsString:=quAlcoDS2KPP.AsString;
            teAlcoIniPost.AsInteger:=quAlcoDS2iPost.AsInteger;
            teAlcoInsPost.AsString:=quAlcoDS2NameCli.AsString;
            teAlcoInsPostInn.AsString:=quAlcoDS2INNCLI.AsString;
            teAlcoInsPostKpp.AsString:=quAlcoDS2KppCli.AsString;
            teAlcoIniLic.AsInteger:=quAlcoDS2iLic.AsInteger;
            teAlcoInsLicNumber.AsString:=quAlcoDS2SER.AsString+' '+quAlcoDS2SNUM.AsString;
            teAlcoInLicDateB.AsDateTime:=quAlcoDS2DDATEB.AsDateTime;
            teAlcoInLicDateE.AsDateTime:=quAlcoDS2DDATEE.AsDateTime;
            teAlcoInLicOrg.AsString:=quAlcoDS2Organ.AsString;
            teAlcoInDocDate.AsDateTime:=quAlcoDS2DocDate.AsDateTime;
            teAlcoInDocNum.AsString:=quAlcoDS2DocNum.AsString;
            teAlcoInGTD.AsString:=quAlcoDS2GTD.AsString;
            teAlcoInrQIn.AsFloat:=quAlcoDS2rQIn.AsFloat;
            teAlcoIn.Post;
          end;
          quAlcoDS2.Next;
        end;
        quAlcoDS2.Active:=False;

        fmAddAlco.dsteAlcoOb.DataSet:=fmAddAlco.teAlcoOb;
        fmAddAlco.dsteAlcoIn.DataSet:=fmAddAlco.teAlcoIn;

        fmAddAlco.ViAlco1.EndUpdate;
        fmAddAlco.ViAlco2.EndUpdate;

        fmAddAlco.ShowModal;
      end else showmessage('�������������� ��������� ����������.');
    end;
  end;
end;

procedure TfmAlcoE.acDelAlcoExecute(Sender: TObject);
Var IDH:INteger;
begin
//�������
  if not CanDo('prDelAlcoE') then
  begin
    Memo1.Lines.Add('��� ����.'); delay(10);
  end else
  begin
    with dmORep do
    begin
      if MessageDlg('����� ������ ������� ��������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        if (quAlcoDH.RecordCount>0)and(quAlcoDHiActive.AsInteger=0) then
        begin
          IDH:=quAlcoDHId.AsInteger;
          Memo1.Clear;
          Memo1.Lines.Add('����� ���� �������� ���������.'); delay(10);
          Memo1.Lines.Add('  - �������������.'); delay(10);

          quDelDS1.ParamByName('IDH').AsInteger:=IDH;
          quDelDS1.ExecQuery;

          Memo1.Lines.Add('  - �������������..'); delay(10);

          quDelDS2.ParamByName('IDH').AsInteger:=IDH;
          quDelDS2.ExecQuery;

          Memo1.Lines.Add('  - ���������'); delay(10);
          quAlcoDH.Delete;
          Memo1.Lines.Add('�������� ��'); delay(10);

        end else showmessage('�������� ����������.');
      end;
    end;
  end;
end;

procedure TfmAlcoE.ViewAlcoDblClick(Sender: TObject);
begin
  //������� �������
  with dmORep do
  begin
    if quAlcoDH.RecordCount>0 then
    begin
      if quAlcoDHiActive.AsInteger=0 then acEditAlco.Execute else acViewAlco.Execute;
    end;
  end;
end;

procedure TfmAlcoE.acViewAlcoExecute(Sender: TObject);
Var IDH:INteger;
//    sInnCli:String;
begin
  //��������
  if not CanDo('prViewAlcoE') then
  begin
    Memo1.Lines.Add('��� ����.'); delay(10);
  end else
  begin
    with dmORep do
    with dmO do
    begin
      if quAlcoDH.RecordCount>0 then
      begin
        if quMHAll.Active=False then
        begin
          quMHAll.ParamByName('IDPERSON').AsInteger:=Person.Id;
          quMHAll.Active:=True;
        end;
        quMHAll.FullRefresh;
        quMHAll.First;

        quMHAll.Locate('ID',quAlcoDHIdOrg.AsInteger,[]);

        fmAddAlco.cxLookupComboBox1.EditValue:=quAlcoDHIdOrg.AsInteger;
        fmAddAlco.cxLookupComboBox1.Tag:=quAlcoDHId.AsInteger;
        fmAddAlco.cxLookupComboBox1.Properties.ReadOnly:=True;

        fmAddAlco.cxDateEdit1.Date:=quAlcoDHDDateB.AsDateTime;
        fmAddAlco.cxDateEdit1.Properties.ReadOnly:=True;

        fmAddAlco.cxDateEdit2.Date:=quAlcoDHDDateE.AsDateTime;
        fmAddAlco.cxDateEdit2.Properties.ReadOnly:=True;

        fmAddAlco.cxComboBox1.ItemIndex:=quAlcoDHIT1.AsInteger;
        fmAddAlco.cxComboBox1.Properties.ReadOnly:=True;

        fmAddAlco.cxComboBox2.ItemIndex:=quAlcoDHIT2.AsInteger;
        fmAddAlco.cxComboBox2.Properties.ReadOnly:=True;

        fmAddAlco.cxSpinEdit1.Value:=quAlcoDHiNumCorr.AsInteger;
        fmAddAlco.cxSpinEdit1.Properties.ReadOnly:=True;

        IDH:=quAlcoDHId.AsInteger;

        CloseTe(fmAddAlco.teAlcoOb);
        CloseTe(fmAddAlco.teAlcoIn);

        fmAddAlco.ViAlco1.OptionsData.Editing:=False;
        fmAddAlco.ViAlco2.OptionsData.Editing:=False;

        fmAddAlco.cxButton1.Enabled:=False;
        fmAddAlco.cxButton2.Enabled:=False;
        fmAddAlco.cxButton3.Enabled:=False;
//        fmAddAlco.cxButton4.Enabled:=bStr; //�����
        fmAddAlco.cxButton5.Enabled:=False;
        fmAddAlco.cxButton6.Enabled:=False;
        fmAddAlco.cxButton7.Enabled:=False;
        fmAddAlco.cxButton8.Enabled:=False;
        fmAddAlco.cxButton9.Enabled:=False;
//        fmAddAlco.cxButton10.Enabled:=False;

        fmAddAlco.ViAlco1.BeginUpdate;
        fmAddAlco.ViAlco2.BeginUpdate;

        fmAddAlco.dsteAlcoOb.DataSet:=nil;
        fmAddAlco.dsteAlcoIn.DataSet:=nil;


        quAlcoDS1.Active:=False;
        quAlcoDS1.ParamByName('IDH').AsInteger:=IDH;
        quAlcoDS1.Active:=True;

        quAlcoDS1.First;
        while not quAlcoDS1.Eof do
        begin
          with fmAddAlco do
          begin
            teAlcoOb.Append;
            teAlcoObiDep.AsInteger:=quAlcoDS1iDep.AsInteger;
            teAlcoObsDep.AsString:=quAlcoDS1NameShop.AsString;
            teAlcoObiVid.AsInteger:=quAlcoDS1iVid.AsInteger;
            teAlcoObsVid.AsString:=quAlcoDS1iVid.AsString;
            teAlcoObsVidName.AsString:=quAlcoDS1NameVid.AsString;
            teAlcoObiProd.AsInteger:=quAlcoDS1iProd.AsInteger;
            teAlcoObsProd.AsString:=quAlcoDS1NameProducer.AsString;
            teAlcoObsProdInn.AsString:=quAlcoDS1INN.AsString;
            teAlcoObsProdKPP.AsString:=quAlcoDS1KPP.AsString;
            teAlcoObrQb.AsFloat:=quAlcoDS1rQb.AsFloat;
            teAlcoObrQIn1.AsFloat:=quAlcoDS1rQIn1.AsFloat;
            teAlcoObrQIn2.AsFloat:=quAlcoDS1rQIn2.AsFloat;
            teAlcoObrQIn3.AsFloat:=quAlcoDS1rQIn3.AsFloat;
            teAlcoObrQInIt1.AsFloat:=quAlcoDS1rQInIt1.AsFloat;
            teAlcoObrQIn4.AsFloat:=quAlcoDS1rQIn4.AsFloat;
            teAlcoObrQIn5.AsFloat:=quAlcoDS1rQIn5.AsFloat;
            teAlcoObrQIn6.AsFloat:=quAlcoDS1rQIn6.AsFloat;
            teAlcoObrQInIt.AsFloat:=quAlcoDS1rQInIt.AsFloat;
            teAlcoObrQOut1.AsFloat:=quAlcoDS1rQOut1.AsFloat;
            teAlcoObrQOut2.AsFloat:=quAlcoDS1rQOut2.AsFloat;
            teAlcoObrQOut3.AsFloat:=quAlcoDS1rQOut3.AsFloat;
            teAlcoObrQOut4.AsFloat:=quAlcoDS1rQOut4.AsFloat;
            teAlcoObrQOutIt.AsFloat:=quAlcoDS1rQOutIt.AsFloat;
            teAlcoObrQe.AsFloat:=quAlcoDS1rQe.AsFloat;
            teAlcoOb.Post;
          end;
          quAlcoDS1.Next;
        end;
        quAlcoDS1.Active:=False;

        quAlcoDS2.Active:=False;
        quAlcoDS2.ParamByName('IDH').AsInteger:=IDH;
        quAlcoDS2.Active:=True;

        quAlcoDS2.First;
        while not quAlcoDS2.Eof do
        begin
          with fmAddAlco do
          begin
            teAlcoIn.Append;
            teAlcoIniDep.AsInteger:=quAlcoDS2iDep.AsInteger;
            teAlcoInsDep.AsString:=quAlcoDS2NameShop.AsString;
            teAlcoIniVid.AsInteger:=quAlcoDS2iVid.AsInteger;
            teAlcoInsVid.AsString:=quAlcoDS2iVid.AsString;
            teAlcoInsVidName.AsString:=quAlcoDS2NAMEVID.AsString;
            teAlcoIniProd.AsInteger:=quAlcoDS2iProd.AsInteger;
            teAlcoInsProd.AsString:=quAlcoDS2NameProducer.AsString;
            teAlcoInsProdInn.AsString:=quAlcoDS2INN.AsString;
            teAlcoInsProdKPP.AsString:=quAlcoDS2KPP.AsString;
            teAlcoIniPost.AsInteger:=quAlcoDS2iPost.AsInteger;
            teAlcoInsPost.AsString:=quAlcoDS2NameCli.AsString;
            teAlcoInsPostInn.AsString:=quAlcoDS2INNCLI.AsString;
            teAlcoInsPostKpp.AsString:=quAlcoDS2KppCli.AsString;
            teAlcoIniLic.AsInteger:=quAlcoDS2iLic.AsInteger;
            teAlcoInsLicNumber.AsString:=quAlcoDS2SER.AsString+quAlcoDS2SNUM.AsString;
            teAlcoInLicDateB.AsDateTime:=quAlcoDS2DDATEB.AsDateTime;
            teAlcoInLicDateE.AsDateTime:=quAlcoDS2DDATEE.AsDateTime;
            teAlcoInLicOrg.AsString:=quAlcoDS2Organ.AsString;
            teAlcoInDocDate.AsDateTime:=quAlcoDS2DocDate.AsDateTime;
            teAlcoInDocNum.AsString:=quAlcoDS2DocNum.AsString;
            teAlcoInGTD.AsString:=quAlcoDS2GTD.AsString;
            teAlcoInrQIn.AsFloat:=quAlcoDS2rQIn.AsFloat;
            teAlcoIn.Post;
          end;
          quAlcoDS2.Next;
        end;
        quAlcoDS2.Active:=False;

        fmAddAlco.dsteAlcoOb.DataSet:=fmAddAlco.teAlcoOb;
        fmAddAlco.dsteAlcoIn.DataSet:=fmAddAlco.teAlcoIn;

        fmAddAlco.ViAlco1.EndUpdate;
        fmAddAlco.ViAlco2.EndUpdate;

        fmAddAlco.ShowModal;
      end;
    end;
  end;
end;

procedure TfmAlcoE.acSetSendExecute(Sender: TObject);
//Var IDH:INteger;
begin
  if not CanDo('prSendAlcoE') then
  begin
    Memo1.Lines.Add('��� ����.'); delay(10);
  end else
  begin
    with dmORep do
    begin
      if MessageDlg('��������� ������ "���������" ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        if (quAlcoDH.RecordCount>0)and(quAlcoDHiActive.AsInteger=0) then
        begin
//          IDH:=quAlcoDHId.AsInteger;
          Memo1.Clear;
          Memo1.Lines.Add('  ����� ���� ����������� �������.'); delay(10);

          quAlcoDH.Edit;
          quAlcoDHiActive.AsInteger:=1;
          quAlcoDH.Post;
          quAlcoDH.Refresh;

          Memo1.Lines.Add('  ��.'); delay(10);

        end else showmessage('�������� ����������.');
      end;
    end;
  end;
end;

end.
