object fmAddAlco: TfmAddAlco
  Left = 932
  Top = 214
  Width = 990
  Height = 641
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 974
    Height = 109
    Align = alTop
    BevelInner = bvLowered
    Color = 13553358
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 12
      Width = 67
      Height = 13
      Caption = #1054#1088#1075#1072#1085#1080#1079#1072#1094#1080#1103
      Transparent = True
    end
    object Label2: TLabel
      Left = 16
      Top = 36
      Width = 47
      Height = 13
      Caption = #1055#1077#1088#1080#1086#1076' '#1089
    end
    object Label3: TLabel
      Left = 240
      Top = 36
      Width = 12
      Height = 13
      Caption = #1087#1086
    end
    object Label4: TLabel
      Left = 16
      Top = 84
      Width = 34
      Height = 13
      Caption = #1057#1090#1072#1090#1091#1089
    end
    object Label5: TLabel
      Left = 16
      Top = 60
      Width = 19
      Height = 13
      Caption = #1058#1080#1087
      Transparent = True
    end
    object Label6: TLabel
      Left = 268
      Top = 84
      Width = 68
      Height = 13
      Caption = #8470' '#1082#1086#1088#1088#1077#1082#1094#1080#1080
    end
    object cxDateEdit1: TcxDateEdit
      Left = 92
      Top = 32
      Style.LookAndFeel.Kind = lfOffice11
      Style.Shadow = True
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 0
      Width = 121
    end
    object cxDateEdit2: TcxDateEdit
      Left = 264
      Top = 32
      Style.LookAndFeel.Kind = lfOffice11
      Style.Shadow = True
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 1
      Width = 121
    end
    object cxComboBox1: TcxComboBox
      Left = 92
      Top = 56
      Properties.Items.Strings = (
        #1055#1086' '#1072#1083#1082#1086#1075#1086#1083#1102
        #1055#1086' '#1087#1080#1074#1091)
      Style.BorderStyle = ebsOffice11
      Style.LookAndFeel.Kind = lfOffice11
      Style.Shadow = True
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 2
      Text = #1055#1086' '#1072#1083#1082#1086#1075#1086#1083#1102
      Width = 125
    end
    object Panel2: TPanel
      Left = 464
      Top = 2
      Width = 508
      Height = 105
      Align = alRight
      BevelInner = bvLowered
      Color = 15329769
      TabOrder = 3
      object cxButton1: TcxButton
        Left = 8
        Top = 52
        Width = 89
        Height = 37
        Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
        TabOrder = 0
        OnClick = cxButton1Click
        LookAndFeel.Kind = lfOffice11
      end
      object cxButton2: TcxButton
        Left = 352
        Top = 4
        Width = 145
        Height = 25
        Caption = #1054#1089#1090#1072#1090#1082#1080' '#1085#1072' '#1085#1072#1095'.'
        TabOrder = 1
        OnClick = cxButton2Click
        LookAndFeel.Kind = lfOffice11
      end
      object cxButton3: TcxButton
        Left = 8
        Top = 8
        Width = 89
        Height = 37
        Caption = #1057#1092#1086#1088#1084#1080#1088#1086#1074#1072#1090#1100
        TabOrder = 2
        OnClick = cxButton3Click
        LookAndFeel.Kind = lfOffice11
      end
      object cxButton4: TcxButton
        Left = 224
        Top = 8
        Width = 89
        Height = 37
        Caption = #1042#1099#1093#1086#1076
        TabOrder = 3
        OnClick = cxButton4Click
        LookAndFeel.Kind = lfOffice11
      end
      object cxButton5: TcxButton
        Left = 352
        Top = 28
        Width = 145
        Height = 25
        Caption = #1055#1088#1080#1093#1086#1076
        TabOrder = 4
        OnClick = cxButton5Click
        LookAndFeel.Kind = lfOffice11
      end
      object cxButton6: TcxButton
        Left = 352
        Top = 76
        Width = 145
        Height = 25
        Caption = #1054#1089#1090#1072#1090#1082#1080' '#1085#1072' '#1082#1086#1085#1077#1094' ('#1088#1072#1089#1093#1086#1076')'
        TabOrder = 5
        OnClick = cxButton6Click
        LookAndFeel.Kind = lfOffice11
      end
      object cxButton7: TcxButton
        Left = 352
        Top = 52
        Width = 145
        Height = 25
        Caption = #1042#1086#1079#1074#1088#1072#1090#1099
        TabOrder = 6
        OnClick = cxButton7Click
        LookAndFeel.Kind = lfOffice11
      end
      object cxButton8: TcxButton
        Left = 224
        Top = 52
        Width = 89
        Height = 37
        Caption = #1055#1077#1088#1077#1089#1095#1080#1090#1072#1090#1100
        TabOrder = 7
        OnClick = cxButton8Click
        LookAndFeel.Kind = lfOffice11
      end
      object cxButton9: TcxButton
        Left = 108
        Top = 8
        Width = 105
        Height = 37
        Action = acExpExls
        TabOrder = 8
        LookAndFeel.Kind = lfOffice11
      end
      object cxButton10: TcxButton
        Left = 108
        Top = 52
        Width = 105
        Height = 37
        Action = acExpXML
        TabOrder = 9
        LookAndFeel.Kind = lfOffice11
      end
    end
    object cxLookupComboBox1: TcxLookupComboBox
      Left = 92
      Top = 8
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          FieldName = 'FULLNAME'
        end>
      Properties.ListSource = dmO.dsMHAll
      Style.LookAndFeel.Kind = lfOffice11
      Style.Shadow = True
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 4
      Width = 313
    end
    object cxComboBox2: TcxComboBox
      Left = 92
      Top = 80
      Properties.Items.Strings = (
        #1055#1077#1088#1074#1080#1095#1085#1072#1103
        #1050#1086#1088#1088#1077#1082#1090#1080#1088#1091#1102#1097#1072#1103)
      Style.BorderStyle = ebsOffice11
      Style.LookAndFeel.Kind = lfOffice11
      Style.Shadow = True
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 5
      Text = #1055#1077#1088#1074#1080#1095#1085#1072#1103
      Width = 161
    end
    object cxSpinEdit1: TcxSpinEdit
      Left = 344
      Top = 80
      Style.BorderStyle = ebsOffice11
      Style.LookAndFeel.Kind = lfOffice11
      Style.Shadow = True
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 6
      Width = 81
    end
    object PBar1: TcxProgressBar
      Left = 264
      Top = 56
      TabOrder = 7
      Visible = False
      Width = 185
    end
  end
  object Memo1: TcxMemo
    Left = 0
    Top = 514
    Align = alBottom
    Lines.Strings = (
      'Memo1')
    TabOrder = 1
    Height = 89
    Width = 974
  end
  object GrAddAlco: TcxGrid
    Left = 12
    Top = 116
    Width = 957
    Height = 389
    PopupMenu = PopupMenu1
    TabOrder = 2
    LookAndFeel.Kind = lfOffice11
    RootLevelOptions.DetailTabsPosition = dtpTop
    object ViAlco1: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dsteAlcoOb
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'rQb'
          Column = ViAlco1rQb
        end
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'rQe'
          Column = ViAlco1rQe
        end
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'rQInIt'
          Column = ViAlco1rQInIt
        end
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'rQOutIt'
          Column = ViAlco1rQOutIt
        end
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'rQOut3'
          Column = ViAlco1rQOut3
        end
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'rQIn2'
          Column = ViAlco1rQIn2
        end
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'rQInIt1'
          Column = ViAlco1rQInIt1
        end
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'rQOut1'
          Column = ViAlco1rQOut1
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Inserting = False
      OptionsView.Footer = True
      object ViAlco1iDep: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1084#1072#1075#1072#1079#1080#1085#1072
        DataBinding.FieldName = 'iDep'
        Options.Editing = False
        Width = 51
      end
      object ViAlco1sDep: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077' '#1084#1072#1075#1072#1079#1080#1085#1072
        DataBinding.FieldName = 'sDep'
        Options.Editing = False
        Width = 128
      end
      object ViAlco1iVid: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1074#1080#1076#1072' '#1087#1088#1086#1076#1091#1082#1094#1080#1080
        DataBinding.FieldName = 'iVid'
        Visible = False
        Options.Editing = False
        Width = 46
      end
      object ViAlco1sVid: TcxGridDBColumn
        Caption = #1042#1080#1076' '#1087#1088#1086#1076#1091#1082#1094#1080#1080
        DataBinding.FieldName = 'sVid'
        Options.Editing = False
        Width = 43
      end
      object ViAlco1sVidName: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077' '#1074#1080#1076#1072#1087#1088#1086#1076#1091#1082#1094#1080#1080
        DataBinding.FieldName = 'sVidName'
        Options.Editing = False
        Width = 217
      end
      object ViAlco1iProd: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1087#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1103
        DataBinding.FieldName = 'iProd'
        Visible = False
        Options.Editing = False
      end
      object ViAlco1sProd: TcxGridDBColumn
        Caption = #1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1100
        DataBinding.FieldName = 'sProd'
        Options.Editing = False
        Width = 147
      end
      object ViAlco1sProdInn: TcxGridDBColumn
        Caption = #1048#1053#1053' '#1087#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1103
        DataBinding.FieldName = 'sProdInn'
        Options.Editing = False
        Width = 98
      end
      object ViAlco1sProdKPP: TcxGridDBColumn
        Caption = #1050#1055#1055' '#1087#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1103
        DataBinding.FieldName = 'sProdKPP'
        Options.Editing = False
        Width = 90
      end
      object ViAlco1rQb: TcxGridDBColumn
        Caption = #1054#1089#1090#1072#1090#1086#1082' '#1085#1072' '#1085#1072#1095#1072#1083#1086
        DataBinding.FieldName = 'rQb'
      end
      object ViAlco1rQIn1: TcxGridDBColumn
        Caption = #1055#1088#1080#1093#1086#1076' '#1086#1090' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1081' '#1087#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1077#1081
        DataBinding.FieldName = 'rQIn1'
        Options.Editing = False
      end
      object ViAlco1rQIn2: TcxGridDBColumn
        Caption = #1055#1088#1080#1093#1086#1076' '#1086#1090' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1081' '#1086#1087#1090#1086#1074#1086#1081' '#1090#1086#1088#1075#1086#1074#1083#1080
        DataBinding.FieldName = 'rQIn2'
        Options.Editing = False
      end
      object ViAlco1rQIn3: TcxGridDBColumn
        Caption = #1055#1088#1080#1093#1086#1076' '#1087#1086' '#1080#1084#1087#1086#1088#1090#1091
        DataBinding.FieldName = 'rQIn3'
        Options.Editing = False
      end
      object ViAlco1rQInIt1: TcxGridDBColumn
        Caption = #1047#1072#1082#1091#1087#1082#1080' '#1080#1090#1086#1075#1086
        DataBinding.FieldName = 'rQInIt1'
        Options.Editing = False
      end
      object ViAlco1rQIn4: TcxGridDBColumn
        Caption = #1042#1086#1079#1074#1088#1072#1090' '#1086#1090' '#1087#1086#1082#1091#1087#1072#1090#1077#1083#1103
        DataBinding.FieldName = 'rQIn4'
        Options.Editing = False
      end
      object ViAlco1rQIn5: TcxGridDBColumn
        Caption = #1055#1088#1086#1095#1077#1077' '#1087#1086#1089#1090#1091#1087#1083#1077#1085#1080#1077
        DataBinding.FieldName = 'rQIn5'
        Options.Editing = False
      end
      object ViAlco1rQIn6: TcxGridDBColumn
        Caption = #1055#1077#1088#1077#1084#1077#1097#1077#1085#1080#1077' '#1074#1085#1091#1090#1088#1080' '#1086#1076#1085#1086#1081' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1080
        DataBinding.FieldName = 'rQIn6'
        Options.Editing = False
      end
      object ViAlco1rQInIt: TcxGridDBColumn
        Caption = #1055#1088#1080#1093#1086#1076' '#1048#1058#1054#1043#1054
        DataBinding.FieldName = 'rQInIt'
        Options.Editing = False
      end
      object ViAlco1rQOut1: TcxGridDBColumn
        Caption = #1056#1072#1089#1093#1086#1076' '#1086#1073#1098#1077#1084' '#1088#1086#1079#1085#1080#1095#1085#1086#1081' '#1087#1088#1086#1076#1072#1078#1080
        DataBinding.FieldName = 'rQOut1'
      end
      object ViAlco1rQOut2: TcxGridDBColumn
        Caption = #1055#1088#1086#1095#1080#1081' '#1088#1072#1089#1093#1086#1076
        DataBinding.FieldName = 'rQOut2'
        Options.Editing = False
      end
      object ViAlco1rQOut3: TcxGridDBColumn
        Caption = #1042#1086#1079#1074#1088#1072#1090' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1091
        DataBinding.FieldName = 'rQOut3'
      end
      object ViAlco1rQOut4: TcxGridDBColumn
        Caption = #1056#1072#1089#1093#1086#1076' '#1087#1077#1088#1077#1084#1077#1097#1077#1085#1080#1077' '#1074#1085#1091#1090#1088#1080' '#1086#1076#1085#1086#1081' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1080
        DataBinding.FieldName = 'rQOut4'
        Options.Editing = False
      end
      object ViAlco1rQOutIt: TcxGridDBColumn
        Caption = #1056#1072#1089#1093#1086#1076' '#1048#1058#1054#1043#1054
        DataBinding.FieldName = 'rQOutIt'
        Options.Editing = False
      end
      object ViAlco1rQe: TcxGridDBColumn
        Caption = #1054#1089#1090#1072#1090#1086#1082' '#1085#1072' '#1082#1086#1085#1077#1094
        DataBinding.FieldName = 'rQe'
        Options.Editing = False
      end
    end
    object ViAlco2: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dsteAlcoIn
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'rQIn'
          Column = ViAlco2rQIn
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Inserting = False
      OptionsView.Footer = True
      object ViAlco2iDep: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1084#1072#1075#1072#1079#1080#1085#1072
        DataBinding.FieldName = 'iDep'
        Visible = False
        Options.Editing = False
      end
      object ViAlco2sDep: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077' '#1084#1072#1075#1072#1079#1080#1085#1072
        DataBinding.FieldName = 'sDep'
        Options.Editing = False
        Width = 151
      end
      object ViAlco2iVid: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1074#1080#1076#1072' '#1087#1088#1086#1076#1091#1082#1094#1080#1080
        DataBinding.FieldName = 'iVid'
        Visible = False
        Options.Editing = False
      end
      object ViAlco2sVid: TcxGridDBColumn
        Caption = #1042#1080#1076' '#1087#1088#1086#1076#1091#1082#1094#1080#1080
        DataBinding.FieldName = 'sVid'
        Options.Editing = False
      end
      object ViAlco2sVidName: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077' '#1074#1080#1076#1072' '#1087#1088#1086#1076#1091#1082#1094#1080#1080
        DataBinding.FieldName = 'sVidName'
        Options.Editing = False
        Width = 178
      end
      object ViAlco2iProd: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1087#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1103
        DataBinding.FieldName = 'iProd'
        Visible = False
        Options.Editing = False
      end
      object ViAlco2sProd: TcxGridDBColumn
        Caption = #1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1100
        DataBinding.FieldName = 'sProd'
        Options.Editing = False
        Width = 168
      end
      object ViAlco2sProdInn: TcxGridDBColumn
        Caption = #1048#1053#1053' '#1087#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1103
        DataBinding.FieldName = 'sProdInn'
        Options.Editing = False
        Width = 100
      end
      object ViAlco2sProdKPP: TcxGridDBColumn
        Caption = #1050#1055#1055' '#1087#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1103
        DataBinding.FieldName = 'sProdKPP'
        Options.Editing = False
        Width = 93
      end
      object ViAlco2iPost: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
        DataBinding.FieldName = 'iPost'
        Visible = False
        Options.Editing = False
      end
      object ViAlco2sPost: TcxGridDBColumn
        Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
        DataBinding.FieldName = 'sPost'
        Options.Editing = False
        Width = 164
      end
      object ViAlco2sPostInn: TcxGridDBColumn
        Caption = #1048#1053#1053' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
        DataBinding.FieldName = 'sPostInn'
        Options.Editing = False
      end
      object ViAlco2sPostKpp: TcxGridDBColumn
        Caption = #1050#1055#1055' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
        DataBinding.FieldName = 'sPostKpp'
        Options.Editing = False
      end
      object ViAlco2iLic: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1083#1080#1094#1077#1085#1079#1080#1080
        DataBinding.FieldName = 'iLic'
        Options.Editing = False
      end
      object ViAlco2sLicNumber: TcxGridDBColumn
        Caption = #1051#1080#1094#1077#1085#1079#1080#1103' '#1089#1077#1088'. '#1085#1086#1084#1077#1088
        DataBinding.FieldName = 'sLicNumber'
        Options.Editing = False
        Width = 113
      end
      object ViAlco2LicDateB: TcxGridDBColumn
        Caption = #1044#1072#1090#1072' '#1074#1099#1076#1072#1095#1080
        DataBinding.FieldName = 'LicDateB'
        Options.Editing = False
      end
      object ViAlco2LicDateE: TcxGridDBColumn
        Caption = #1044#1072#1090#1072' '#1086#1082#1086#1085#1095#1072#1085#1080#1103
        DataBinding.FieldName = 'LicDateE'
        Options.Editing = False
      end
      object ViAlco2LicOrg: TcxGridDBColumn
        Caption = #1050#1077#1084' '#1074#1099#1076#1072#1085#1072' '#1083#1080#1094#1077#1085#1079#1080#1103
        DataBinding.FieldName = 'LicOrg'
        Options.Editing = False
        Width = 205
      end
      object ViAlco2DocDate: TcxGridDBColumn
        Caption = #1044#1072#1090#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
        DataBinding.FieldName = 'DocDate'
        Options.Editing = False
      end
      object ViAlco2DocNum: TcxGridDBColumn
        Caption = #1053#1086#1084#1077#1088' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
        DataBinding.FieldName = 'DocNum'
        Options.Editing = False
      end
      object ViAlco2GTD: TcxGridDBColumn
        Caption = #1043#1058#1044
        DataBinding.FieldName = 'GTD'
        Options.Editing = False
        Width = 125
      end
      object ViAlco2rQIn: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086
        DataBinding.FieldName = 'rQIn'
      end
    end
    object LeAlco1: TcxGridLevel
      Caption = #1054#1073#1086#1088#1086#1090
      GridView = ViAlco1
    end
    object LeAlco2: TcxGridLevel
      Caption = #1055#1088#1080#1093#1086#1076
      GridView = ViAlco2
    end
  end
  object FormPlacement1: TFormPlacement
    Active = False
    Left = 716
    Top = 220
  end
  object teAlcoOb: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 340
    Top = 228
    object teAlcoObiDep: TIntegerField
      FieldName = 'iDep'
    end
    object teAlcoObsDep: TStringField
      FieldName = 'sDep'
      Size = 50
    end
    object teAlcoObiVid: TIntegerField
      FieldName = 'iVid'
    end
    object teAlcoObsVid: TStringField
      FieldName = 'sVid'
      Size = 5
    end
    object teAlcoObsVidName: TStringField
      FieldName = 'sVidName'
      Size = 200
    end
    object teAlcoObiProd: TIntegerField
      FieldName = 'iProd'
    end
    object teAlcoObsProd: TStringField
      FieldName = 'sProd'
      Size = 200
    end
    object teAlcoObsProdInn: TStringField
      FieldName = 'sProdInn'
    end
    object teAlcoObsProdKPP: TStringField
      FieldName = 'sProdKPP'
    end
    object teAlcoObrQb: TFloatField
      FieldName = 'rQb'
      DisplayFormat = '0.000'
    end
    object teAlcoObrQIn1: TFloatField
      FieldName = 'rQIn1'
      DisplayFormat = '0.000'
    end
    object teAlcoObrQIn2: TFloatField
      FieldName = 'rQIn2'
      DisplayFormat = '0.000'
    end
    object teAlcoObrQIn3: TFloatField
      FieldName = 'rQIn3'
      DisplayFormat = '0.000'
    end
    object teAlcoObrQInIt1: TFloatField
      FieldName = 'rQInIt1'
      DisplayFormat = '0.000'
    end
    object teAlcoObrQIn4: TFloatField
      FieldName = 'rQIn4'
      DisplayFormat = '0.000'
    end
    object teAlcoObrQIn5: TFloatField
      FieldName = 'rQIn5'
      DisplayFormat = '0.000'
    end
    object teAlcoObrQIn6: TFloatField
      FieldName = 'rQIn6'
      DisplayFormat = '0.000'
    end
    object teAlcoObrQInIt: TFloatField
      FieldName = 'rQInIt'
      DisplayFormat = '0.000'
    end
    object teAlcoObrQOut1: TFloatField
      FieldName = 'rQOut1'
      DisplayFormat = '0.000'
    end
    object teAlcoObrQOut2: TFloatField
      FieldName = 'rQOut2'
      DisplayFormat = '0.000'
    end
    object teAlcoObrQOut3: TFloatField
      FieldName = 'rQOut3'
      DisplayFormat = '0.000'
    end
    object teAlcoObrQOut4: TFloatField
      FieldName = 'rQOut4'
      DisplayFormat = '0.000'
    end
    object teAlcoObrQOutIt: TFloatField
      FieldName = 'rQOutIt'
      DisplayFormat = '0.000'
    end
    object teAlcoObrQe: TFloatField
      FieldName = 'rQe'
      DisplayFormat = '0.000'
    end
  end
  object teAlcoIn: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 416
    Top = 228
    object teAlcoIniDep: TIntegerField
      FieldName = 'iDep'
    end
    object teAlcoInsDep: TStringField
      FieldName = 'sDep'
      Size = 100
    end
    object teAlcoIniVid: TIntegerField
      FieldName = 'iVid'
    end
    object teAlcoInsVid: TStringField
      FieldName = 'sVid'
      Size = 10
    end
    object teAlcoInsVidName: TStringField
      FieldName = 'sVidName'
      Size = 200
    end
    object teAlcoIniProd: TIntegerField
      FieldName = 'iProd'
    end
    object teAlcoInsProd: TStringField
      FieldName = 'sProd'
      Size = 200
    end
    object teAlcoInsProdInn: TStringField
      FieldName = 'sProdInn'
    end
    object teAlcoInsProdKPP: TStringField
      FieldName = 'sProdKPP'
    end
    object teAlcoIniPost: TIntegerField
      FieldName = 'iPost'
    end
    object teAlcoInsPost: TStringField
      FieldName = 'sPost'
      Size = 200
    end
    object teAlcoInsPostInn: TStringField
      FieldName = 'sPostInn'
    end
    object teAlcoInsPostKpp: TStringField
      FieldName = 'sPostKpp'
    end
    object teAlcoIniLic: TIntegerField
      FieldName = 'iLic'
    end
    object teAlcoInsLicNumber: TStringField
      FieldName = 'sLicNumber'
      Size = 100
    end
    object teAlcoInLicDateB: TDateField
      FieldName = 'LicDateB'
    end
    object teAlcoInLicDateE: TDateField
      FieldName = 'LicDateE'
    end
    object teAlcoInLicOrg: TStringField
      FieldName = 'LicOrg'
      Size = 200
    end
    object teAlcoInDocDate: TDateField
      FieldName = 'DocDate'
    end
    object teAlcoInDocNum: TStringField
      FieldName = 'DocNum'
    end
    object teAlcoInGTD: TStringField
      FieldName = 'GTD'
      Size = 30
    end
    object teAlcoInrQIn: TFloatField
      FieldName = 'rQIn'
      DisplayFormat = '0.000'
    end
    object teAlcoInIdDH: TIntegerField
      FieldName = 'IdDH'
    end
  end
  object dsteAlcoOb: TDataSource
    DataSet = teAlcoOb
    Left = 344
    Top = 292
  end
  object dsteAlcoIn: TDataSource
    DataSet = teAlcoIn
    Left = 416
    Top = 292
  end
  object amAddAlco: TActionManager
    Left = 556
    Top = 224
    StyleName = 'XP Style'
    object acGetIn: TAction
      Caption = 'acGetIn'
      OnExecute = acGetInExecute
    end
    object acGetOutR: TAction
      Caption = 'acGetOutR'
      OnExecute = acGetOutRExecute
    end
    object acGetRet: TAction
      Caption = 'acGetRet'
      OnExecute = acGetRetExecute
    end
    object acExpExls: TAction
      Caption = #1069#1082#1089#1087#1086#1088#1090' '#1074' '#1101#1082#1089#1077#1083#1100
      OnExecute = acExpExlsExecute
    end
    object acExpXML: TAction
      Caption = #1069#1082#1089#1087#1086#1088#1090' '#1074' XML'
      OnExecute = acExpXMLExecute
    end
    object acGenGUID: TAction
      Caption = 'acGenGUID'
      ShortCut = 49223
      OnExecute = acGenGUIDExecute
    end
    object acImportXLS: TAction
      Caption = #1048#1084#1087#1086#1088#1090' '#1080#1079' Excel'
      ShortCut = 49225
      Visible = False
      OnExecute = acImportXLSExecute
    end
    object acGetRemn2: TAction
      Caption = 'acGetRemn2'
      OnExecute = acGetRemn2Execute
    end
    object acInputPost: TAction
      Caption = #1055#1088#1080#1085#1103#1090#1100' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
      OnExecute = acInputPostExecute
    end
    object acRecalcLic: TAction
      Caption = #1055#1077#1088#1077#1089#1095#1080#1090#1072#1090#1100' '#1083#1080#1094#1077#1085#1079#1080#1080
      OnExecute = acRecalcLicExecute
    end
    object acExpXMLold: TAction
      Caption = 'acExpXMLold'
    end
  end
  object OpenDialog1: TOpenDialog
    Left = 232
    Top = 252
  end
  object teProds: TdxMemData
    Indexes = <>
    SortOptions = []
    SortedField = 'iProd'
    Left = 148
    Top = 228
    object teProdsiProd: TIntegerField
      FieldName = 'iProd'
    end
    object teProdssProd: TStringField
      FieldName = 'sProd'
      Size = 200
    end
    object teProdssProdInn: TStringField
      FieldName = 'sProdInn'
    end
    object teProdssProdKPP: TStringField
      FieldName = 'sProdKPP'
    end
  end
  object tePost: TdxMemData
    Indexes = <>
    SortOptions = []
    SortedField = 'iPost'
    Left = 148
    Top = 292
    object tePostiPost: TIntegerField
      FieldName = 'iPost'
    end
    object tePostsPost: TStringField
      FieldName = 'sPost'
      Size = 200
    end
    object tePostsPostInn: TStringField
      FieldName = 'sPostInn'
    end
    object tePostsPostKpp: TStringField
      FieldName = 'sPostKpp'
    end
    object tePostiLic: TIntegerField
      FieldName = 'iLic'
    end
    object tePostLicNumber: TStringField
      FieldName = 'LicNumber'
      Size = 100
    end
    object tePostLicDateB: TDateField
      FieldName = 'LicDateB'
    end
    object tePostLicDateE: TDateField
      FieldName = 'LicDateE'
    end
    object tePostLicOrg: TStringField
      FieldName = 'LicOrg'
      Size = 200
    end
  end
  object teA1: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'iDep'
        DataType = ftInteger
      end
      item
        Name = 'sDep'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'iVid'
        DataType = ftInteger
      end
      item
        Name = 'sVidName'
        DataType = ftString
        Size = 200
      end
      item
        Name = 'iProd'
        DataType = ftInteger
      end
      item
        Name = 'rQb'
        DataType = ftFloat
      end
      item
        Name = 'rQIn1'
        DataType = ftFloat
      end
      item
        Name = 'rQIn2'
        DataType = ftFloat
      end
      item
        Name = 'rQIn3'
        DataType = ftFloat
      end
      item
        Name = 'rQInIt1'
        DataType = ftFloat
      end
      item
        Name = 'rQIn4'
        DataType = ftFloat
      end
      item
        Name = 'rQIn5'
        DataType = ftFloat
      end
      item
        Name = 'rQIn6'
        DataType = ftFloat
      end
      item
        Name = 'rQInIt'
        DataType = ftFloat
      end
      item
        Name = 'rQOut1'
        DataType = ftFloat
      end
      item
        Name = 'rQOut2'
        DataType = ftFloat
      end
      item
        Name = 'rQOut3'
        DataType = ftFloat
      end
      item
        Name = 'rQOut4'
        DataType = ftFloat
      end
      item
        Name = 'rQOutIt'
        DataType = ftFloat
      end
      item
        Name = 'rQe'
        DataType = ftFloat
      end>
    IndexDefs = <
      item
        Name = 'teA1Index1'
        Fields = 'iDep;iVid;iProd'
      end>
    IndexFieldNames = 'iDep;iVid;iProd'
    Params = <>
    StoreDefs = True
    Left = 340
    Top = 356
    object teA1iDep: TIntegerField
      FieldName = 'iDep'
    end
    object teA1sDep: TStringField
      FieldName = 'sDep'
      Size = 100
    end
    object teA1iVid: TIntegerField
      FieldName = 'iVid'
    end
    object teA1sVidName: TStringField
      FieldName = 'sVidName'
      Size = 200
    end
    object teA1iProd: TIntegerField
      FieldName = 'iProd'
    end
    object teA1rQb: TFloatField
      FieldName = 'rQb'
    end
    object teA1rQIn1: TFloatField
      FieldName = 'rQIn1'
    end
    object teA1rQIn2: TFloatField
      FieldName = 'rQIn2'
    end
    object teA1rQIn3: TFloatField
      FieldName = 'rQIn3'
    end
    object teA1rQInIt1: TFloatField
      FieldName = 'rQInIt1'
    end
    object teA1rQIn4: TFloatField
      FieldName = 'rQIn4'
    end
    object teA1rQIn5: TFloatField
      FieldName = 'rQIn5'
    end
    object teA1rQIn6: TFloatField
      FieldName = 'rQIn6'
    end
    object teA1rQInIt: TFloatField
      FieldName = 'rQInIt'
    end
    object teA1rQOut1: TFloatField
      FieldName = 'rQOut1'
    end
    object teA1rQOut2: TFloatField
      FieldName = 'rQOut2'
    end
    object teA1rQOut3: TFloatField
      FieldName = 'rQOut3'
    end
    object teA1rQOut4: TFloatField
      FieldName = 'rQOut4'
    end
    object teA1rQOutIt: TFloatField
      FieldName = 'rQOutIt'
    end
    object teA1rQe: TFloatField
      FieldName = 'rQe'
    end
  end
  object teA2: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'iDep'
        DataType = ftInteger
      end
      item
        Name = 'iVid'
        DataType = ftInteger
      end
      item
        Name = 'iProd'
        DataType = ftInteger
      end
      item
        Name = 'iPost'
        DataType = ftInteger
      end
      item
        Name = 'DocDate'
        DataType = ftDateTime
      end
      item
        Name = 'DocNum'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'rQIn'
        DataType = ftFloat
      end
      item
        Name = 'GTD'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'iLic'
        DataType = ftInteger
      end>
    IndexDefs = <
      item
        Name = 'teA2Index1'
        Fields = 'iDep;iVid;iProd;iPost;DocDate'
      end>
    IndexFieldNames = 'iDep;iVid;iProd;iPost;DocDate'
    MasterFields = 'iDep;iVid;iProd'
    MasterSource = dsteA1
    PacketRecords = 0
    Params = <>
    StoreDefs = True
    Left = 416
    Top = 356
    object teA2iDep: TIntegerField
      FieldName = 'iDep'
    end
    object teA2iVid: TIntegerField
      FieldName = 'iVid'
    end
    object teA2iProd: TIntegerField
      FieldName = 'iProd'
    end
    object teA2iPost: TIntegerField
      FieldName = 'iPost'
    end
    object teA2DocDate: TDateTimeField
      FieldName = 'DocDate'
    end
    object teA2DocNum: TStringField
      FieldName = 'DocNum'
      Size = 50
    end
    object teA2rQIn: TFloatField
      FieldName = 'rQIn'
    end
    object teA2GTD: TStringField
      FieldName = 'GTD'
      Size = 100
    end
    object teA2iLic: TIntegerField
      FieldName = 'iLic'
    end
  end
  object dsteA1: TDataSource
    DataSet = teA1
    Left = 344
    Top = 412
  end
  object teLic: TdxMemData
    Indexes = <>
    SortOptions = []
    SortedField = 'iPost'
    Left = 148
    Top = 356
    object teLiciPost: TIntegerField
      FieldName = 'iPost'
    end
    object teLiciLic: TIntegerField
      FieldName = 'iLic'
    end
    object teLicLicNumber: TStringField
      FieldName = 'LicNumber'
      Size = 100
    end
    object teLicLicDateB: TDateField
      FieldName = 'LicDateB'
    end
    object teLicLicDateE: TDateField
      FieldName = 'LicDateE'
    end
    object teLicLicOrg: TStringField
      FieldName = 'LicOrg'
      Size = 200
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 532
    Top = 292
    object acImportXLS1: TMenuItem
      Action = acImportXLS
    end
    object N1: TMenuItem
      Action = acInputPost
    end
    object N2: TMenuItem
      Action = acRecalcLic
    end
  end
end
