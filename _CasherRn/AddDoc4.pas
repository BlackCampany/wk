unit AddDoc4;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, cxGraphics, cxCurrencyEdit, cxDropDownEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxButtonEdit,
  cxMaskEdit, cxCalendar, cxControls, cxContainer, cxEdit, cxTextEdit,
  StdCtrls, ExtCtrls, Menus, cxLookAndFeelPainters, cxButtons, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, DB, cxDBData, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxLabel, dxfLabel, Placemnt, FIBDataSet,
  pFIBDataSet, DBClient, FIBQuery, pFIBQuery, pFIBStoredProc,
  XPStyleActnCtrls, ActnList, ActnMan, FIBDatabase, pFIBDatabase,
  cxImageComboBox, cxCalc, dxmdaset, cxGroupBox, cxRadioGroup, cxMemo;

type
  TfmAddDoc4 = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Label1: TLabel;
    cxTextEdit1: TcxTextEdit;
    cxDateEdit1: TcxDateEdit;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label12: TLabel;
    cxTextEdit2: TcxTextEdit;
    cxDateEdit2: TcxDateEdit;
    cxButtonEdit1: TcxButtonEdit;
    cxLookupComboBox1: TcxLookupComboBox;
    Panel2: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Panel3: TPanel;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    cxLabel3: TcxLabel;
    FormPlacement1: TFormPlacement;
    taSpec1: TClientDataSet;
    dsSpec: TDataSource;
    taSpec1Num: TIntegerField;
    taSpec1IdGoods: TIntegerField;
    taSpec1NameG: TStringField;
    taSpec1IM: TIntegerField;
    taSpec1SM: TStringField;
    taSpec1Quant: TFloatField;
    taSpec1PriceIn: TCurrencyField;
    taSpec1SumIn: TCurrencyField;
    taSpec1PriceR: TCurrencyField;
    taSpec1SumR: TCurrencyField;
    taSpec1INds: TIntegerField;
    taSpec1SNds: TStringField;
    taSpec1RNds: TCurrencyField;
    taSpec1SumNac: TCurrencyField;
    taSpec1ProcNac: TFloatField;
    cxLabel4: TcxLabel;
    cxLabel5: TcxLabel;
    cxLabel6: TcxLabel;
    taSpec1Km: TFloatField;
    amDocR: TActionManager;
    acAddPos: TAction;
    acDelPos: TAction;
    acAddList: TAction;
    acDelAll: TAction;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    acSaveDoc: TAction;
    acExitDoc: TAction;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    GridDoc4: TcxGrid;
    ViewDoc4: TcxGridDBTableView;
    ViewDoc4Num: TcxGridDBColumn;
    ViewDoc4IdGoods: TcxGridDBColumn;
    ViewDoc4NameG: TcxGridDBColumn;
    ViewDoc4IM: TcxGridDBColumn;
    ViewDoc4SM: TcxGridDBColumn;
    ViewDoc4Quant: TcxGridDBColumn;
    ViewDoc4PriceIn: TcxGridDBColumn;
    ViewDoc4SumIn: TcxGridDBColumn;
    ViewDoc4PriceR: TcxGridDBColumn;
    ViewDoc4SumR: TcxGridDBColumn;
    ViewDoc4INds: TcxGridDBColumn;
    ViewDoc4RNds: TcxGridDBColumn;
    ViewDoc4SumNac: TcxGridDBColumn;
    ViewDoc4ProcNac: TcxGridDBColumn;
    LevelDoc4: TcxGridLevel;
    ViewDoc4Km: TcxGridDBColumn;
    CasherRnDb1: TpFIBDatabase;
    trSelCash: TpFIBTransaction;
    quFindPrice: TpFIBDataSet;
    quFindPricePRICE: TFIBFloatField;
    quFindPriceCODE: TFIBStringField;
    quFindPriceCONSUMMA: TFIBFloatField;
    quFindPriceIACTIVE: TFIBSmallIntField;
    taSpec1TCard: TIntegerField;
    ViewDoc4TCard: TcxGridDBColumn;
    cxCalcEdit1: TcxCalcEdit;
    Label6: TLabel;
    taSpec1Oper: TSmallintField;
    ViewDoc4Oper: TcxGridDBColumn;
    N2: TMenuItem;
    Excel1: TMenuItem;
    acMovePos: TAction;
    N3: TMenuItem;
    Label7: TLabel;
    cxButtonEdit2: TcxButtonEdit;
    taSpec1CTO: TStringField;
    ViewDoc4CTO: TcxGridDBColumn;
    taSpec: TdxMemData;
    taSpecNum: TIntegerField;
    taSpecIdGoods: TIntegerField;
    taSpecNameG: TStringField;
    taSpecIM: TIntegerField;
    taSpecSM: TStringField;
    taSpecQuant: TFloatField;
    taSpecPriceIn: TFloatField;
    taSpecSumIn: TFloatField;
    taSpecPriceR: TFloatField;
    taSpecSumR: TFloatField;
    taSpecINds: TIntegerField;
    taSpecSNds: TStringField;
    taSpecRNds: TFloatField;
    taSpecSumNac: TFloatField;
    taSpecProcNac: TFloatField;
    taSpecKm: TFloatField;
    taSpecTCard: TSmallintField;
    taSpecOper: TSmallintField;
    taSpecCTO: TStringField;
    taSpecCType: TIntegerField;
    ViewDoc4CType: TcxGridDBColumn;
    taSpecPriceIn0: TFloatField;
    taSpecSumIn0: TFloatField;
    ViewDoc4PriceIn0: TcxGridDBColumn;
    ViewDoc4SumIn0: TcxGridDBColumn;
    taSpecSumOut0: TFloatField;
    taSpecRNdsOut: TFloatField;
    ViewDoc4SumOut0: TcxGridDBColumn;
    ViewDoc4RNdsOut: TcxGridDBColumn;
    taSpecBZQUANTR: TFloatField;
    taSpecBZQUANTF: TFloatField;
    taSpecBZQUANTS: TFloatField;
    ViewDoc4BZQUANTR: TcxGridDBColumn;
    ViewDoc4BZQUANTF: TcxGridDBColumn;
    ViewDoc4BZQUANTS: TcxGridDBColumn;
    cxRadioGroup1: TcxRadioGroup;
    acAddTara: TAction;
    N4: TMenuItem;
    acDelTara: TAction;
    cxLabel7: TcxLabel;
    cxButton3: TcxButton;
    taSpecBZQUANTSHOP: TFloatField;
    ViewDoc4BZQUANTSHOP: TcxGridDBColumn;
    Label8: TLabel;
    cxLookupComboBox2: TcxLookupComboBox;
    acSetOper: TAction;
    N5: TMenuItem;
    N6: TMenuItem;
    Memo1: TcxMemo;
    taSpecQUANTDOC: TFloatField;
    ViewDoc4QUANTDOC: TcxGridDBColumn;
    acSaveQuantDoc: TAction;
    Label9: TLabel;
    cxLookupComboBox3: TcxLookupComboBox;
    Label10: TLabel;
    cxButtonEdit3: TcxButtonEdit;
    acSetPriceFromPrice: TAction;
    Label11: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure cxLookupComboBox1PropertiesChange(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxLabel1Click(Sender: TObject);
    procedure ViewDoc4DragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure ViewDoc4DragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure taSpec1QuantChange(Sender: TField);
    procedure taSpec1PriceInChange(Sender: TField);
    procedure taSpec1SumInChange(Sender: TField);
    procedure taSpec1PriceRChange(Sender: TField);
    procedure taSpec1SumRChange(Sender: TField);
    procedure cxLabel3Click(Sender: TObject);
    procedure cxLabel2Click(Sender: TObject);
    procedure ViewDoc4Editing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cxLabel5Click(Sender: TObject);
    procedure ViewDoc4EditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure ViewDoc4EditKeyPress(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
    procedure acAddPosExecute(Sender: TObject);
    procedure acDelPosExecute(Sender: TObject);
    procedure acAddListExecute(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure acDelAllExecute(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure cxLabel6Click(Sender: TObject);
    procedure cxDateEdit1PropertiesChange(Sender: TObject);
    procedure ViewDoc4SMPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxButton2Click(Sender: TObject);
    procedure acSaveDocExecute(Sender: TObject);
    procedure acExitDocExecute(Sender: TObject);
    procedure ViewTaraDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure cxButtonEdit1KeyPress(Sender: TObject; var Key: Char);
    procedure Excel1Click(Sender: TObject);
    procedure acMovePosExecute(Sender: TObject);
    procedure cxButtonEdit2PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure taSpecQuantChange(Sender: TField);
    procedure taSpecPriceInChange(Sender: TField);
    procedure taSpecSumInChange(Sender: TField);
    procedure taSpecSumRChange(Sender: TField);
    procedure taSpecPriceRChange(Sender: TField);
    procedure taSpecPriceIn0Change(Sender: TField);
    procedure taSpecSumIn0Change(Sender: TField);
    procedure taSpecSumOut0Change(Sender: TField);
    procedure taSpecRNdsOutChange(Sender: TField);
    procedure taSpecINdsChange(Sender: TField);
    procedure taSpecBeforePost(DataSet: TDataSet);
    procedure taSpecBZQUANTFChange(Sender: TField);
    procedure taSpecBZQUANTSChange(Sender: TField);
    procedure acAddTaraExecute(Sender: TObject);
    procedure cxLabel4Click(Sender: TObject);
    procedure acDelTaraExecute(Sender: TObject);
    procedure cxLabel7Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure acSetOperExecute(Sender: TObject);
    procedure acSaveQuantDocExecute(Sender: TObject);
    procedure cxButtonEdit3PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure acSetPriceFromPriceExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function vFNds(i:Integer):Real;
  end;

var
  fmAddDoc4: TfmAddDoc4;
  iCol:Integer = 0;
  iColT:Integer = 0;
  bAdd:Boolean = False;

implementation

uses Un1, dmOffice, Clients, Goods, FCards, DocsIn, CurMessure, OMessure,
  DMOReps, DocOutR, FindSebPr, SelPerSkl, CardsMove, Tara, MainRnOffice,
  SetOper, RecalcPrice, DocPrice;

{$R *.dfm}

function TfmAddDoc4.vFNds(i:Integer):Real;
Var kPostPayNDS:INteger;
begin
  Result:=0;
  kPostPayNDS:=1;
  if (i>=1)and(i<=3)then Result:=vNds[i]*kPostPayNDS;
end;


procedure TfmAddDoc4.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=CurDir+GridIni;
  FormPlacement1.Active:=True;
  PageControl1.Align:=alClient;
  PageControl1.ActivePageIndex:=0;
end;

procedure TfmAddDoc4.cxLookupComboBox1PropertiesChange(Sender: TObject);
begin
  with dmO do
  with dmORep do
  begin
    CurVal.IdMH:=cxLookupComboBox1.EditValue;
    CurVal.NAMEMH:=cxLookupComboBox1.Text;
    if quMHAll.Locate('ID',CurVal.IdMH,[]) then
    begin
      cxLookupComboBox1.Tag:=quMHAllISS.AsInteger; //ISS
    end else
    begin
      cxLookupComboBox1.Tag:=0;
    end;

    quUPLMH.Active:=False;
    quUPLMH.ParamByName('IMH').AsInteger:=cxLookupComboBox1.EditValue;
    quUPLMH.Active:=True;

    if quUPLMH.RecordCount>0 then
    begin
      fmAddDoc4.cxLookupComboBox2.EditValue:=quUPLMHID.AsInteger;
      fmAddDoc4.cxLookupComboBox2.Text:=quUPLMHNAME.AsString;
    end;

  end;
end;

procedure TfmAddDoc4.cxButtonEdit1PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  if dmO.taClients.Active=False then dmO.taClients.Active:=True;
  if fmClients.Visible then fmClients.Close;
  bSelCli:=True;
  if cxButtonEdit1.Tag>0 then dmO.taClients.Locate('ID',cxButtonEdit1.Tag,[]);
  fmClients.ShowModal;
  if fmClients.ModalResult=mrOk then
  begin
    cxButtonEdit1.Tag:=dmO.taClientsID.AsInteger;
    cxButtonEdit1.Text:=dmO.taClientsNAMECL.AsString;
  end else
  begin
    cxButtonEdit1.Tag:=0;
    cxButtonEdit1.Text:='';
  end;
  bSelCli:=False;
end;

procedure TfmAddDoc4.cxLabel1Click(Sender: TObject);
begin
  acAddList.Execute;
end;

procedure TfmAddDoc4.ViewDoc4DragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept:=False;
  if bDrR then  Accept:=True;
end;

procedure TfmAddDoc4.ViewDoc4DragDrop(Sender, Source: TObject; X,
  Y: Integer);
Var iCo:Integer;
    i,j: Integer;
    iNum: Integer;
    Rec:TcxCustomGridRecord;
    iMax:Integer;
begin
//
  if bDrR then
  begin
    ResetAddVars;
    iCo:=fmGoods.ViewGoods.Controller.SelectedRecordCount;
    if iCo>0 then
    begin
      if MessageDlg('�� ������������� ������ ����������� ��������� ������� ('+IntToStr(iCo)+' ��.) � ������������ ���������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        iMax:=1;
        fmAddDoc4.taSpec.First;
        if not fmAddDoc4.taSpec.Eof then
        begin
          fmAddDoc4.taSpec.Last;
          iMax:=fmAddDoc4.taSpecNum.AsInteger+1;
        end;

        with dmO do
        begin
          ViewDoc4.BeginUpdate;
          for i:=0 to fmGoods.ViewGoods.Controller.SelectedRecordCount-1 do
          begin
            Rec:=fmGoods.ViewGoods.Controller.SelectedRecords[i];

            for j:=0 to Rec.ValueCount-1 do
            begin
              if fmGoods.ViewGoods.Columns[j].Name='ViewGoodsID' then break;
            end;

            iNum:=Rec.Values[j];
          //��� ��� - ����������

            if quCardsSel.Locate('ID',iNum,[]) then
            begin
              with fmAddDoc4 do
              begin
                taSpec.Append;
                taSpecNum.AsInteger:=iMax;
                taSpecIdGoods.AsInteger:=quCardsSelID.AsInteger;
                taSpecNameG.AsString:=quCardsSelNAME.AsString;
                taSpecIM.AsInteger:=quCardsSelIMESSURE.AsInteger;
                taSpecSM.AsString:=quCardsSelNAMESHORT.AsString;
                taSpecKm.AsFloat:=quCardsselKOEF.AsFloat;
                taSpecQuant.AsFloat:=1;
                taSpecPriceIn.AsFloat:=0;
                taSpecSumIn.AsFloat:=0;
                taSpecPriceR.AsFloat:=quCardsSelLASTPRICEOUT.AsFloat;
                taSpecSumR.AsFloat:=quCardsSelLASTPRICEOUT.AsFloat;
                taSpecINds.AsInteger:=quCardsSelINDS.AsInteger;
                taSpecSNds.AsString:=quCardsSelNAMENDS.AsString;
                taSpecRNds.AsFloat:=0;
                taSpecSumNac.AsFloat:=0;
                taSpecProcNac.AsFloat:=0;
                taSpecTCard.AsInteger:=quCardsSelTCard.AsInteger;
                taSpecOper.AsInteger:=0;
                taSpecCType.AsInteger:=quCardsSelCATEGORY.AsInteger;
                taSpecPriceIn0.AsFloat:=0;
                taSpecSumIn0.AsFloat:=0;
                taSpecSumOut0.AsFloat:=0;
                taSpecRNdsOut.AsFloat:=0;
                taSpec.Post;
                inc(iMax);
              end;
            end;
          end;
          ViewDoc4.EndUpdate;
        end;
      end;
    end;
  end;
end;

procedure TfmAddDoc4.taSpec1QuantChange(Sender: TField);
begin
  //���������� �����
  if iCol=1 then
  begin
    taSpecSumIn.AsFloat:=taSpecPriceIn.AsFloat*taSpecQuant.AsFloat;
    taSpecSumR.AsFloat:=taSpecPriceR.AsFloat*taSpecQuant.AsFloat;
    taSpecSumNac.AsFloat:=taSpecSumR.AsFloat-taSpecSumIn.AsFloat;
    if taSpecSumIn.AsFloat>0.01 then
    begin
      taSpecProcNac.AsFloat:=(taSpecSumR.AsFloat-taSpecSumIn.AsFloat)/taSpecSumIn.AsFloat*100;
    end;
    taSpecRNds.AsFloat:=taSpecSumIn.AsFloat-taSpecSumIn0.AsFloat;
  end;
end;

procedure TfmAddDoc4.taSpec1PriceInChange(Sender: TField);
begin
  //���������� ����
  if iCol=2 then
  begin
    taSpecSumIn.AsFloat:=taSpecPriceIn.AsFloat*taSpecQuant.AsFloat;
//  taSpecSumR.AsFloat:=taSpecPriceR.AsFloat*taSpecQuant.AsFloat;
    taSpecSumNac.AsFloat:=taSpecSumR.AsFloat-taSpecSumIn.AsFloat;
    if taSpecSumIn.AsFloat>0.01 then
    begin
      taSpecProcNac.AsFloat:=(taSpecSumR.AsFloat-taSpecSumIn.AsFloat)/taSpecSumIn.AsFloat*100;
    end;
    taSpecRNds.AsFloat:=taSpecSumIn.AsFloat-taSpecSumIn0.AsFloat;
  end;
end;

procedure TfmAddDoc4.taSpec1SumInChange(Sender: TField);
begin
  if iCol=3 then
  begin
    if abs(taSpecQuant.AsFloat)>0 then taSpecPriceIn.AsFloat:=RoundEx(taSpecSumIn.AsFloat/taSpecQuant.AsFloat*100)/100;
    taSpecSumNac.AsFloat:=taSpecSumR.AsFloat-taSpecSumIn.AsFloat;
    if taSpecSumIn.AsFloat>0.01 then
    begin
      taSpecProcNac.AsFloat:=(taSpecSumR.AsFloat-taSpecSumIn.AsFloat)/taSpecSumIn.AsFloat*100;
    end;
    taSpecRNds.AsFloat:=taSpecSumIn.AsFloat-taSpecSumIn0.AsFloat;
  end;
end;

procedure TfmAddDoc4.taSpec1PriceRChange(Sender: TField);
begin
  //���������� ����
  if iCol=4 then
  begin
//  taSpecSumIn.AsFloat:=taSpecPriceIn.AsFloat*taSpecQuant.AsFloat;
    taSpecSumR.AsFloat:=taSpecPriceR.AsFloat*taSpecQuant.AsFloat;
    taSpecSumNac.AsFloat:=taSpecSumR.AsFloat-taSpecSumIn.AsFloat;
    if taSpecSumIn.AsFloat>0.01 then
    begin
      taSpecProcNac.AsFloat:=(taSpecSumR.AsFloat-taSpecSumIn.AsFloat)/taSpecSumIn.AsFloat*100;
    end;
  end;
end;

procedure TfmAddDoc4.taSpec1SumRChange(Sender: TField);
begin
  if iCol=5 then
  begin
    if abs(taSpecQuant.AsFloat)>0 then taSpecPriceR.AsFloat:=RoundEx(taSpecSumR.AsFloat/taSpecQuant.AsFloat*100)/100;
    taSpecSumNac.AsFloat:=taSpecSumR.AsFloat-taSpecSumIn.AsFloat;
    if taSpecSumIn.AsFloat>0.01 then
    begin
      taSpecProcNac.AsFloat:=(taSpecSumR.AsFloat-taSpecSumIn.AsFloat)/taSpecSumIn.AsFloat*100;
    end;
  end;
end;

procedure TfmAddDoc4.cxLabel3Click(Sender: TObject);
Var PriceSp:Real;
    rQs,rQ,rQp:Real;
    rMessure,rSumIn,rPrice,rDisc:Real;
    rSumIn0,PriceSp0:Real;
    rNac:Real;
    iT,iUstCost:INteger;
    rPrSS,rPrOut:Real;
    iRound:Real;
begin
  //����������� ����
  if PageControl1.ActivePageIndex=0 then
  begin
//    ViewDoc4.BeginUpdate;

    GridDoc4.SetFocus;

    if CommonSet.PriceFromCash=2 then //���� ������������� �� �������
    begin
      fmRecalcPrice:=TfmRecalcPrice.Create(Application);
      fmRecalcPrice.ShowModal;
      if fmRecalcPrice.ModalResult=mrOk then
      begin
        delay(100);
        Memo1.Clear;
        Memo1.Lines.Add('����� ���� ������ ��� ...'); delay(10);
        with dmO do
        with dmORep do
        begin
          taSpec.First;
          while not taSpec.Eof do
          begin
            Memo1.Lines.Add(' ');  delay(10);
            Memo1.Lines.Add('  ���. '+taSpecNameG.AsString+', ��� - '+its(taSpecIdGoods.AsInteger));  delay(10);

            quFindCard.Active:=False;
            quFindCard.ParamByName('IDCARD').AsInteger:=taSpecIdGoods.AsInteger;
            quFindCard.Active:=True;
            if (quFindCard.RecordCount>0) then
            begin
              rNac:=fmRecalcPrice.cxCalcEdit1.Value;
              iT:=fmRecalcPrice.cxRadioGroup1.ItemIndex;

              CloseTe(teC0);

              teC0.Append;
              teC0ID.AsInteger:=1;
              teC0IDCARD.AsInteger:=taSpecIdGoods.AsInteger;
              teC0NAME.AsString:=quFindCardNAME.AsString;
              teC0TCARD.AsInteger:=quFindCardTCARD.AsInteger;
              teC0QUANT.AsFloat:=1;
              teC0IDM.AsInteger:=quFindCardIMESSURE.AsInteger;
              teC0PRICER.AsFloat:=0;
              teC0RSUM.AsFloat:=0;
              teC0KM.AsInteger:=1;
              teC0.Post;

              //������ ������������� �����
              rPrSS:=prCalcBlPriceIn(taSpecIdGoods.AsInteger,iT,0,Trunc(date),Memo1,iUstCost);

              if (iUstCost=1)or(iT=2) then
              begin
                Memo1.Lines.Add('          ���� ������� - '+fts(rv(rPrSS))); delay(10);
                Memo1.Lines.Add('          ������� - '+fts(rv(rNac))+'%'); delay(10);

                rPrOut:=rv(rPrSS*(rNac+100)/100); //���������� �� ������

                Memo1.Lines.Add('          ���� ������� - '+fts(rPrOut));  delay(10);

                iRound:=fmRecalcPrice.cxComboBox1.ItemIndex;
                if iRound=0 then rPrOut:=RoundEx(rPrOut/10)*10; //���������� �� 10-��� ������
                if iRound=1 then rPrOut:=RoundEx(rPrOut); //���������� �� ������
                if iRound=2 then rPrOut:=RoundEx(rPrOut*10)/10; //���������� �� 10-��� ������

                taSpec.Edit;
                taSpecPriceR.AsFloat:=rPrOut;
                taSpecSumR.AsFloat:=rv(rPrOut*taSpecQuant.AsFloat);
                taSpec.Post;

                Memo1.Lines.Add(' ');  delay(10);
              end else
              begin
                Memo1.Lines.Add('      ��� ������������ ���� - ������ ����������.'); delay(10);
                ShowMessage('��� ������������ ���� - ������ ����������.');
                Break;
              end;
            end else Memo1.Lines.Add('      ������������ ���������� - ������ ����������.'); delay(10);

            taSpec.Next;
          end;
        end;
        Memo1.Lines.Add('������ ��� ��������.'); delay(10);
      end;
      fmRecalcPrice.Release;
    end;

    if CommonSet.PriceFromCash=1 then //���� ������� � �����
    begin
      try
        CasherRnDb1.Connected:=False;
        CasherRnDb1.DatabaseName:=DBName;
        CasherRnDb1.Open;
        delay(500);
        if CasherRnDb1.Connected then
        begin
          rDisc:=RoundVal(cxCalcEdit1.EditValue);
          taSpec.First;
          while not taSpec.Eof do
          begin
            quFindPrice.Active:=False;
            quFindPrice.ParamByName('IDCARD').AsString:=INtToStr(taSpecIdGoods.AsInteger);
            quFindPrice.Active:=True;
            if quFindPrice.RecordCount>0 then
            begin
              quFindPrice.First;
              rPrice:=RoundVal(quFindPricePRICE.AsFloat*(100-rDisc)/100);

              taSpec.Edit;
              taSpecPriceR.AsFloat:=rPrice;
              taSpecSumR.AsFloat:=RV(taSpecQUANT.AsFloat*rPrice);

              taSpecSumOut0.AsFloat:=rv(taSpecSumR.AsFloat*100/(100+vFNds(taSpecINds.AsInteger)));
              taSpecRNdsOut.AsFloat:=taSpecSumR.AsFloat-taSpecSumOut0.AsFloat;

              taSpec.Post;
            end else
            begin
              if taSpecPriceR.AsFloat<>0 then
              begin
                rPrice:=RoundVal(taSpecPriceR.AsFloat*(100-rDisc)/100);

                taSpec.Edit;
                taSpecPriceR.AsFloat:=rPrice;
                taSpecSumR.AsFloat:=RoundVal(taSpecQUANT.AsFloat*rPrice);

                taSpecSumOut0.AsFloat:=rv(taSpecSumR.AsFloat*100/(100+vFNds(taSpecINds.AsInteger)));
                taSpecRNdsOut.AsFloat:=taSpecSumR.AsFloat-taSpecSumOut0.AsFloat;

                taSpec.Post;
              end;
            end;
            quFindPrice.Active:=False;

            taSpec.Next;
          end;
          CasherRnDb1.Connected:=False;
        end;
      except
        CasherRnDb1.Connected:=False;
      end;
    end;

    if CommonSet.PriceFromCash=0 then //���� ������� �� ��������
    begin
      rDisc:=RoundVal(cxCalcEdit1.EditValue);
      taSpec.First;
      while not taSpec.Eof do
      begin
        with dmO do
        begin
          quFCard.Active:=False;
          quFCard.SelectSQL.Clear;
          quFCard.SelectSQL.Add('SELECT ID,NAME,IMESSURE,PARENT,TCARD,INDS,LASTPRICEOUT,CATEGORY');
          quFCard.SelectSQL.Add('FROM OF_CARDS');
          quFCard.SelectSQL.Add('where ID='+IntToStr(taSpecIdGoods.AsInteger));
          quFCard.SelectSQL.Add('and IACTIVE>0');
          quFCard.SelectSQL.Add('and PARENT in (SELECT ID_CLASSIF FROM RCLASSIF WHERE RIGHTS=0 AND ID_PERSONAL='+its(Person.Id)+')');
          quFCard.Active:=True;

          if quFCard.RecordCount=1 then
          begin
            rPrice:=RoundVal(quFCardLASTPRICEOUT.AsFloat*(100-rDisc)/100);

            if rPrice>0.001 then
            begin
              taSpec.Edit;
              taSpecPriceR.AsFloat:=rPrice;
              taSpecSumR.AsFloat:=RoundVal(rPrice*taSpecQuant.AsFloat);

              taSpecSumOut0.AsFloat:=rv(taSpecSumR.AsFloat*100/(100+vFNds(taSpecINds.AsInteger)));
              taSpecRNdsOut.AsFloat:=taSpecSumR.AsFloat-taSpecSumOut0.AsFloat;
              taSpec.Post;
            end;
          end;

          quFCard.Active:=False;
        end;

        taSpec.Next;
      end;
    end;

    taSpec.First;
    while not taSpec.Eof do
    begin
      PriceSp:=0;
      rSumIn:=0;
      PriceSp0:=0;
      rSumIn0:=0;

      rQs:=taSpecQuant.AsFloat*taSpecKm.AsFloat; //�������� � ��������
      prSelPartIn(taSpecIdGoods.AsInteger,cxLookupComboBox1.EditValue,0,0);

      with dmO do
      begin
        quSelPartIn.First;
        if rQs>0 then
        begin
          while (not quSelPartIn.Eof) and (rQs>0) do
          begin
            //���� �� ���� ������� ���� �����, ��������� �������� ���
            rQp:=quSelPartInQREMN.AsFloat;
            if rQs<=rQp then  rQ:=rQs//��������� ������ ������ ���������
                              else  rQ:=rQp;
            rQs:=rQs-rQ;

            PriceSp:=quSelPartInPRICEIN.AsFloat;
            PriceSp0:=quSelPartInPRICEIN0.AsFloat;

            rSumIn:=rSumIn+PriceSp*rQ;
            rSumIn0:=rSumIn0+PriceSp0*rQ;

            quSelPartIn.Next;
          end;

          if rQs>0 then //�������� ������������� ������, �������� � ������, �� � ������� ��������� ������ ���, ��� � �������������
          begin
            if PriceSp=0 then
            begin //��� ���� ���������� ������� � ���������� ����������
              prCalcLastPrice1.ParamByName('IDGOOD').AsInteger:=taSpecIdGoods.AsInteger;
              prCalcLastPrice1.ParamByName('ISKL').AsInteger:=cxLookupComboBox1.EditValue;
              prCalcLastPrice1.ExecProc;

              PriceSp:=prCalcLastPrice1.ParamByName('PRICEIN').AsFloat;
              PriceSp0:=prCalcLastPrice1.ParamByName('PRICEIN0').AsFloat;

              rMessure:=prCalcLastPrice1.ParamByName('KOEF').AsFloat;
              if (rMessure<>0)and(rMessure<>1) then
              begin
                PriceSp:=PriceSp/rMessure;
                PriceSp0:=PriceSp0/rMessure;
              end;
            end;
            rSumIn:=rSumIn+PriceSp*rQs;
            rSumIn0:=rSumIn0+PriceSp0*rQs;
          end;
        end;
        quSelPartIn.Active:=False;
      end;

      rSumIn:=rv(rSumIn);
      rSumIn0:=rv(rSumIn0);

      //�������� ���������
      taSpec.Edit;
      if taSpecQuant.AsFloat<>0 then
      begin
        taSpecSumIn.AsFloat:=rSumIn;
        taSpecPriceIn.AsFloat:=rSumIn/taSpecQuant.AsFloat;
        taSpecSumIn0.AsFloat:=rSumIn0;
        taSpecPriceIn0.AsFloat:=rSumIn0/taSpecQuant.AsFloat;

        taSpecRNds.AsFloat:=rSumIn-rSumIn0;

      end else
      begin
        taSpecSumIn.AsFloat:=0;
        taSpecPriceIn.AsFloat:=0;
        taSpecSumIn0.AsFloat:=0;
        taSpecPriceIn0.AsFloat:=0;
        taSpecRNds.AsFloat:=0;
      end;

      taSpecSumNac.AsFloat:=taSpecSumR.AsFloat-taSpecSumIn.AsFloat;
      taSpecProcNac.AsFloat:=0;
      if taSpecSumIn.AsFloat>0.01 then
      begin
        taSpecProcNac.AsFloat:=(taSpecSumR.AsFloat-taSpecSumIn.AsFloat)/taSpecSumIn.AsFloat*100;
      end;
      taSpecRNds.AsFloat:=taSpecSumIn.AsFloat-taSpecSumIn0.AsFloat;


      taSpec.Post;

      taSpec.Next;
    end;
    delay(10);

    taSpec.First;
//  ViewDoc4.EndUpdate;
  end;
end;

procedure TfmAddDoc4.cxLabel2Click(Sender: TObject);
begin
  acDelPos.Execute;
end;

procedure TfmAddDoc4.ViewDoc4Editing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  iCol:=0;
  if ViewDoc4.Controller.FocusedColumn.Name='ViewDoc4Quant' then iCol:=1;
  if ViewDoc4.Controller.FocusedColumn.Name='ViewDoc4PriceIn' then
  begin
    iCol:=2;
    if taSpec.RecordCount>0 then
    if (taSpecQuant.AsFloat<0)and(cxButton1.Enabled=True)and(taSpecIdGoods.AsInteger>0) then
    begin
      with dmORep do
      with dmO do
      begin
        quFindSebReal.Active:=False;
        quFindSebReal.ParamByName('IDCARD').AsInteger:=taSpecIdGoods.AsInteger;
        quFindSebReal.ParamByName('IDSKL').AsInteger:=cxLookupComboBox1.EditValue;
        quFindSebReal.Active:=True;

        fmFindSebPr:=TfmFindSebPr.Create(Application);
        fmFindSebPr.ShowModal;
        if fmFindSebPr.ModalResult=mrOk then
        begin
          taSpec.Edit;
          taSpecPriceIn.AsFloat:=quFindSebRealPRICEIN.AsFloat;
          taSpecSumIn.AsFloat:=RoundVal(taSpecQuant.AsFloat*quFindSebRealPRICEIN.AsFloat);
          taSpecPriceIn0.AsFloat:=quFindSebRealPRICEIN0.AsFloat;
          taSpecSumIn0.AsFloat:=RoundVal(taSpecQuant.AsFloat*quFindSebRealPRICEIN0.AsFloat);
          taSpec.Post;
        end;
        fmFindSebPr.Release;
        quFindSebReal.Active:=False;
      end;
    end;
  end;
  if ViewDoc4.Controller.FocusedColumn.Name='ViewDoc4SumIn' then iCol:=3;
  if ViewDoc4.Controller.FocusedColumn.Name='ViewDoc4PriceR' then iCol:=4;
  if ViewDoc4.Controller.FocusedColumn.Name='ViewDoc4SumR' then iCol:=5;
  if ViewDoc4.Controller.FocusedColumn.Name='ViewDoc4PriceIn0' then iCol:=6;
  if ViewDoc4.Controller.FocusedColumn.Name='ViewDoc4SumIn0' then iCol:=7;
  if ViewDoc4.Controller.FocusedColumn.Name='ViewDoc4SumOut0' then iCol:=8;
  if ViewDoc4.Controller.FocusedColumn.Name='ViewDoc4RNdsOut' then iCol:=9;
  if ViewDoc4.Controller.FocusedColumn.Name='ViewDoc4INds' then iCol:=10;

  if ViewDoc4.Controller.FocusedColumn.Name='ViewDoc4BZQUANTR' then iCol:=11;
  if ViewDoc4.Controller.FocusedColumn.Name='ViewDoc4BZQUANTF' then iCol:=12;
  if ViewDoc4.Controller.FocusedColumn.Name='ViewDoc4BZQUANTS' then iCol:=13;

end;

procedure TfmAddDoc4.FormShow(Sender: TObject);
begin
  iCol:=0;
  iColT:=0;

  PageControl1.ActivePageIndex:=0;

  if cxTextEdit1.Text='' then
  begin
    cxTextEdit1.SetFocus;
    cxTextEdit1.SelectAll;
  end else GridDoc4.SetFocus;

  ViewDoc4NameG.Options.Editing:=False;

  Memo1.Clear;
end;

procedure TfmAddDoc4.FormClose(Sender: TObject; var Action: TCloseAction);
Var IdH:INteger;
begin
  if cxButton1.Enabled=True then
  begin
    if MessageDlg('�� ������������� ������ ����� �� ���������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      IdH:=cxTextEdit1.Tag;
      if iDH>0 then
      begin
        with dmORep do
        begin
          if quDocsRSel.Locate('ID',IDH,[]) then
          begin
            quDocsRSel.Edit;
            quDocsRSelIEDIT.AsInteger:=0;
            quDocsRSelPERSEDIT.AsString:='';
            quDocsRSelIPERSEDIT.AsInteger:=0;
            quDocsRSel.Post;

            prSetSync('DocR','CLS',IDH,quDocsRSelIDSKL.AsInteger);

            quDocsRSel.Refresh;
          end;
        end;
      end;

      ViewDoc4.StoreToIniFile(CurDir+Person.Name+'\'+GridIni,False);
      Action := caHide;
    end
    else  Action := caNone;
  end else ViewDoc4.StoreToIniFile(CurDir+Person.Name+'\'+GridIni,False);
end;

procedure TfmAddDoc4.cxLabel5Click(Sender: TObject);
begin
  acAddPos.Execute;
end;

procedure TfmAddDoc4.ViewDoc4EditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
Var iCode:Integer;
    Km:Real;
    sName:String;
    //���� ������ ��� ������ ����� �� ������
    rK:Real;
    iM:Integer;
    Sm:String;
    //--
begin
  with dmO do
  begin
    if (Key=$0D) then
    begin
      if ViewDoc4.Controller.FocusedColumn.Name='ViewDoc4IdGoods' then
      begin
        iCode:=VarAsType(AEdit.EditingValue, varInteger);

        quFCard.Active:=False;
        quFCard.SelectSQL.Clear;
        quFCard.SelectSQL.Add('SELECT ID,NAME,IMESSURE,PARENT,TCARD,INDS,LASTPRICEOUT,CATEGORY');
        quFCard.SelectSQL.Add('FROM OF_CARDS');
        quFCard.SelectSQL.Add('where ID='+IntToStr(iCode));
        quFCard.SelectSQL.Add('and IACTIVE>0');
        quFCard.SelectSQL.Add('and PARENT in (SELECT ID_CLASSIF FROM RCLASSIF WHERE RIGHTS=0 AND ID_PERSONAL='+its(Person.Id)+')');
        quFCard.Active:=True;

        if quFCard.RecordCount=1 then
        begin
          ViewDoc4.BeginUpdate;
          taSpec.Edit;
          taSpecIdGoods.AsInteger:=iCode;
          taSpecNameG.AsString:=quFCardNAME.AsString;
          taSpecIM.AsInteger:=quFCardIMESSURE.AsInteger;
          taSpecSM.AsString:=prFindKNM(quFCardIMESSURE.AsInteger,Km);
          taSpecKm.AsFloat:=Km;
          taSpecQuant.AsFloat:=1;
          taSpecPriceIn.AsFloat:=0;
          taSpecSumIn.AsFloat:=0;
          taSpecPriceR.AsFloat:=quFCardLASTPRICEOUT.AsFloat;
          taSpecSumR.AsFloat:=quFCardLASTPRICEOUT.AsFloat;
          taSpecINds.AsInteger:=0;
          taSpecSNds.AsString:='';
          if taNDS.Active=False then taNDS.Active:=True;
          if taNds.Locate('ID',quFCardINDS.AsInteger,[]) then
          begin
            taSpecINds.AsInteger:=quFCardINDS.AsInteger;
            taSpecSNds.AsString:=taNDSNAMENDS.AsString;
          end;

          taSpecRNds.AsFloat:=0;
          taSpecSumNac.AsFloat:=0;
          taSpecProcNac.AsFloat:=0;
          taSpecTCard.AsInteger:=quFCardTCard.AsInteger;
          taSpecCType.AsInteger:=quFCardCATEGORY.AsInteger;
          taSpecPriceIn0.AsFloat:=0;
          taSpecSumIn0.AsFloat:=0;
          taSpec.Post;

          ViewDoc4.EndUpdate;
        end;
      end;
      if ViewDoc4.Controller.FocusedColumn.Name='ViewDoc4NameG' then
      begin
        sName:=VarAsType(AEdit.EditingValue, varString);

        fmFCards.ViewFCards.BeginUpdate;
        quFCard.Active:=False;
        quFCard.SelectSQL.Clear;
        quFCard.SelectSQL.Add('SELECT first 100 ID,NAME,IMESSURE,PARENT,TCARD,INDS,LASTPRICEOUT,CATEGORY');
        quFCard.SelectSQL.Add('FROM OF_CARDS');
        quFCard.SelectSQL.Add('where UPPER(NAME) like ''%'+AnsiUpperCase(sName)+'%''');
        quFCard.SelectSQL.Add('and IACTIVE>0');
        quFCard.SelectSQL.Add('and PARENT in (SELECT ID_CLASSIF FROM RCLASSIF WHERE RIGHTS=0 AND ID_PERSONAL='+its(Person.Id)+')');
        quFCard.SelectSQL.Add('Order by NAME');

        quFCard.Active:=True;

        bAdd:=True;

        quFCard.Locate('NAME',sName,[loCaseInsensitive, loPartialKey]);
        prRowFocus(fmFCards.ViewFCards);
        fmFCards.ViewFCards.EndUpdate;

        //�������� ����� ������ � ����� ������
        fmFCards.ShowModal;
        if fmFCards.ModalResult=mrOk then
        begin
          if quFCard.RecordCount>0 then
          begin
            ViewDoc4.BeginUpdate;
            taSpec.Edit;
            taSpecIdGoods.AsInteger:=quFCardID.AsInteger;
            taSpecNameG.AsString:=quFCardNAME.AsString;
            taSpecIM.AsInteger:=quFCardIMESSURE.AsInteger;
            taSpecSM.AsString:=prFindKNM(quFCardIMESSURE.AsInteger,Km);
            taSpecKm.AsFloat:=Km;
            taSpecQuant.AsFloat:=1;
            taSpecPriceIn.AsFloat:=0;
            taSpecSumIn.AsFloat:=0;
            taSpecPriceR.AsFloat:=quFCardLASTPRICEOUT.AsFloat;
            taSpecSumR.AsFloat:=quFCardLASTPRICEOUT.AsFloat;
            taSpecINds.AsInteger:=0;
            taSpecSNds.AsString:='';
            taSpecRNds.AsFloat:=0;
            if taNDS.Active=False then taNDS.Active:=True;
            if taNds.Locate('ID',quFCardINDS.AsInteger,[]) then
            begin
              taSpecINds.AsInteger:=quFCardINDS.AsInteger;
              taSpecSNds.AsString:=taNDSNAMENDS.AsString;
            end;
            taSpecSumNac.AsFloat:=0;
            taSpecProcNac.AsFloat:=0;
            taSpecTCard.AsInteger:=quFCardTCard.AsInteger;
            taSpecCType.AsInteger:=quFCardCATEGORY.AsInteger;
            taSpecPriceIn0.AsFloat:=0;
            taSpecSumIn0.AsFloat:=0;
            taSpec.Post;
            ViewDoc4.EndUpdate;
            AEdit.SelectAll;
          end;
        end;
        bAdd:=False;
      end;
      if ViewDoc4.Controller.FocusedColumn.Name='ViewDoc4SM' then
      begin
        if taSpecIm.AsInteger>0 then
        begin
          bAddSpec:=True;
          fmMessure.ShowModal;
          if fmMessure.ModalResult=mrOk then
          begin
            iM:=iMSel; iMSel:=0;
            Sm:=prFindKNM(iM,rK); //�������� ����� �������� � ����� �����

            taSpec.Edit;
            taSpecIM.AsInteger:=iM;
            taSpecKm.AsFloat:=rK;
            taSpecSM.AsString:=Sm;
            taSpec.Post;
          end;
        end;
      end;
//      ViewDoc4Quant.Focused:=True;
    end else
      if ViewDoc4.Controller.FocusedColumn.Name='ViewDoc4IdGoods' then
        if fTestKey(Key)=False then
          if taSpec.State in [dsEdit,dsInsert] then taSpec.Cancel;
  end;
end;

procedure TfmAddDoc4.ViewDoc4EditKeyPress(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
Var Km:Real;
    sName:String;
begin
  if bAdd then exit;
  with dmO do
  begin
    if (ViewDoc4.Controller.FocusedColumn.Name='ViewDoc4NameG') then
    begin
      sName:=VarAsType(AEdit.EditingValue ,varString)+Key;
//      Label2.Caption:=sName;
      if length(sName)>2 then
      begin
        quFCard.Active:=False;
        quFCard.SelectSQL.Clear;
        quFCard.SelectSQL.Add('SELECT first 2 ID,NAME,IMESSURE,PARENT,TCARD,INDS,LASTPRICEOUT,CATEGORY');
        quFCard.SelectSQL.Add('FROM OF_CARDS');
        quFCard.SelectSQL.Add('where UPPER(NAME) like ''%'+AnsiUpperCase(sName)+'%''');
        quFCard.SelectSQL.Add('and IACTIVE>0');
        quFCard.SelectSQL.Add('and PARENT in (SELECT ID_CLASSIF FROM RCLASSIF WHERE RIGHTS=0 AND ID_PERSONAL='+its(Person.Id)+')');
        quFCard.SelectSQL.Add('Order by NAME');
        quFCard.Active:=True;

        if quFCard.RecordCount=1 then
        begin
          ViewDoc4.BeginUpdate;
          taSpec.Edit;
          taSpecIdGoods.AsInteger:=quFCardID.AsInteger;
          taSpecNameG.AsString:=quFCardNAME.AsString;
          taSpecIM.AsInteger:=quFCardIMESSURE.AsInteger;
          taSpecSM.AsString:=prFindKNM(quFCardIMESSURE.AsInteger,Km);
          taSpecKm.AsFloat:=Km;
          taSpecQuant.AsFloat:=0;
          taSpecPriceIn.AsFloat:=0;
          taSpecSumIn.AsFloat:=0;
          taSpecPriceR.AsFloat:=0;
          taSpecSumR.AsFloat:=0;
          taSpecINds.AsInteger:=0;
          taSpecSNds.AsString:='';
          if taNDS.Active=False then taNDS.Active:=True;
          if taNds.Locate('ID',quFCardINDS.AsInteger,[]) then
          begin
            taSpecINds.AsInteger:=quFCardINDS.AsInteger;
            taSpecSNds.AsString:=taNDSNAMENDS.AsString;
          end;
          taSpecRNds.AsFloat:=0;
          taSpecSumNac.AsFloat:=0;
          taSpecProcNac.AsFloat:=0;
          taSpecTCard.AsInteger:=quFCardTCard.AsInteger;
          taSpecCType.AsInteger:=quFCardCATEGORY.AsInteger;
          taSpecPriceIn0.AsFloat:=0;
          taSpecSumIn0.AsFloat:=0;
          taSpec.Post;
          ViewDoc4.EndUpdate;
          AEdit.SelectAll;
          ViewDoc4NameG.Options.Editing:=False;
          ViewDoc4NameG.Focused:=True;
          Key:=#0;
        end;
      end;
    end;
  end;//}
end;

procedure TfmAddDoc4.acAddPosExecute(Sender: TObject);
Var iMax:Integer;
begin
  if cxButton1.Enabled=False then exit;

  if fmAddDoc4.cxLabel1.Enabled=False then
    if not CanDo('prAddPosDocR') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;

  //�������� �������
  if PageControl1.ActivePageIndex=0 then
  begin
    iMax:=1;
    ViewDoc4.BeginUpdate;

    taSpec.First;
    if not taSpec.Eof then
    begin
      taSpec.Last;
      iMax:=taSpecNum.AsInteger+1;
    end;

    taSpec.Append;
    taSpecNum.AsInteger:=iMax;
    taSpecIdGoods.AsInteger:=0;
    taSpecNameG.AsString:='';
    taSpecIM.AsInteger:=0;
    taSpecSM.AsString:='';
    taSpecKm.AsFloat:=0;
    taSpecQuant.AsFloat:=0;
    taSpecPriceIn.AsFloat:=0;
    taSpecSumIn.AsFloat:=0;
    taSpecPriceR.AsFloat:=0;
    taSpecSumR.AsFloat:=0;
    taSpecINds.AsInteger:=0;
    taSpecSNds.AsString:='';
    taSpecRNds.AsFloat:=0;
    taSpecSumNac.AsFloat:=0;
    taSpecProcNac.AsFloat:=0;
    taSpecOper.AsInteger:=0;
    taSpecCType.AsInteger:=1;
    taSpecPriceIn0.AsFloat:=0;
    taSpecSumIn0.AsFloat:=0;
    taSpecSumOut0.AsFloat:=0;
    taSpecRNdsOut.AsFloat:=0;
    taSpec.Post;
    ViewDoc4.EndUpdate;
    GridDoc4.SetFocus;

    ViewDoc4NameG.Options.Editing:=True;
    ViewDoc4NameG.Focused:=True;

    prRowFocus(ViewDoc4);

  end;
end;

procedure TfmAddDoc4.acDelPosExecute(Sender: TObject);
begin
  //������� �������
  if PageControl1.ActivePageIndex=0 then
  begin
    if taSpec.RecordCount>0 then
    begin
      taSpec.Delete;
    end;
  end;
end;

procedure TfmAddDoc4.acAddListExecute(Sender: TObject);
begin
  if not CanDo('prAddPosDocR') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  bAddSpecR:=True;
  fmGoods.Show;
end;

procedure TfmAddDoc4.cxButton1Click(Sender: TObject);
Var IdH:Integer;
    rSum0,rSum1,rSum2,rSumT:Real;
    iSS:INteger;
    rSumR,rSumF,rSumS:Real;
begin
  //��������
  with dmO do
  with dmORep do
  begin
    IdH:=cxTextEdit1.Tag;
    if taSpec.State in [dsEdit,dsInsert] then taSpec.Post;
    if cxDateEdit1.Date<3000 then  cxDateEdit1.Date:=Date;
    if cxDateEdit2.Date<3000 then  cxDateEdit2.Date:=Date;

    //������� ������ ������
    ViewDoc4.BeginUpdate;

    iSS:=prISS(cxLookupComboBox1.EditValue);

    //�������� �� ������� ����
    taSpec.First;
    while not taSpec.Eof do
    begin
      if taSpecQuant.AsFloat<0 then
      begin
        if abs(taSpecSumIn.AsFloat)<0.01 then
        begin
          showmessage('������� - "'+taSpecNameG.AsString+'" ��������� ���� �������.');
          ViewDoc4.EndUpdate;
          exit;
        end;
      end;

      taSpec.Edit;
      taSpecSumR.AsFloat:=rv(taSpecPriceR.AsFloat*taSpecQuant.AsFloat);
      taSpec.Post;

      taSpec.Next;
    end;

    if cxTextEdit1.Tag=0 then
    begin
      IDH:=GetId('DocR');
      cxTextEdit1.Tag:=IDH;
      if cxTextEdit1.Text=prGetNum(8,0) then prGetNum(8,1); //��������
    end;

    if cxTextEdit2.Text='' then
    begin
      cxTextEdit2.Text:=prGetNumSCHF(cxLookupComboBox1.EditValue,DToYY(cxDateEdit2.Date))
    end;

    quDocsRId.Active:=False;
    quDocsRId.ParamByName('IDH').AsInteger:=IDH;
    quDocsRId.Active:=True;

    quDocsRId.First;
    if quDocsRId.RecordCount=0 then quDocsRId.Append else quDocsRId.Edit;

    quDocsRIdID.AsInteger:=IDH;
    quDocsRIdDATEDOC.AsDateTime:=Trunc(cxDateEdit1.Date);
    quDocsRIdNUMDOC.AsString:=cxTextEdit1.Text;
    quDocsRIdDATESF.AsDateTime:=Trunc(cxDateEdit2.Date);
    quDocsRIdNUMSF.AsString:=cxTextEdit2.Text;
    quDocsRIdIDCLI.AsInteger:=cxButtonEdit1.Tag;
    quDocsRIdIDSKL.AsInteger:=cxLookupComboBox1.EditValue;
    quDocsRIdSUMIN.AsFloat:=0;
    quDocsRIdSUMUCH.AsFloat:=0;
    quDocsRIdSUMTAR.AsFloat:=0;
    quDocsRIdSUMNDS0.AsFloat:=sNDS[1];
    quDocsRIdSUMNDS1.AsFloat:=sNDS[2];
    quDocsRIdSUMNDS2.AsFloat:=sNDS[3];
    quDocsRIdPROCNAC.AsFloat:=RoundVal(cxCalcEdit1.EditValue);
    quDocsRIdIACTIVE.AsInteger:=0;
    quDocsRIdIDFROM.AsInteger:=cxButtonEdit2.Tag;
    try quDocsRIdUPL.AsInteger:=cxLookupComboBox2.EditValue; except  quDocsRIdUPL.AsInteger:=0; end;
    try quDocsRIdIDRV.AsInteger:=cxLookupComboBox3.EditValue; except  quDocsRIdIDRV.AsInteger:=0; end;
    quDocsRIdIDCLITRANS.AsInteger:=cxButtonEdit3.Tag;
    quDocsRId.Post;

    //�������� ������������
    quSpecRSel.Active:=False;
    quSpecRSel.ParamByName('IDHD').AsInteger:=IDH;
    quSpecRSel.Active:=True;

    quSpecRSel.First; //������� ������
    while not quSpecRSel.Eof do quSpecRSel.Delete;
    quSpecRSel.FullRefresh;

    sNDS[1]:=0;sNDS[2]:=0;sNDS[3]:=0;
    rSum1:=0; rSum2:=0;   rSumT:=0;
    rSum0:=0;
    rSumR:=0;
    rSumF:=0;
    rSumS:=0;
    taSpec.First;
    while not taSpec.Eof do
    begin
      if quSpecRSel.Locate('IDCARD',taSpecIdGoods.AsInteger,[])=False then
      begin
        quSpecRSel.Append;
        quSpecRSelIDHEAD.AsInteger:=IDH;
        quSpecRSelID.AsInteger:=taSpecNum.AsInteger;
        quSpecRSelIDCARD.AsInteger:=taSpecIdGoods.AsInteger;
        quSpecRSelQUANT.AsFloat:=taSpecQuant.AsFloat;
        quSpecRSelPRICEIN.AsFloat:=taSpecPriceIn.AsFloat;
        quSpecRSelSUMIN.AsFloat:=RoundVal(taSpecSumIn.AsFloat);
        quSpecRSelPRICER.AsFloat:=taSpecPriceR.AsFloat;
        quSpecRSelSUMR.AsFloat:=taSpecSumR.AsFloat;
        quSpecRSelIDNDS.AsInteger:=taSpecINds.AsInteger;
        quSpecRSelSUMNDS.AsFloat:=taSpecRNds.AsFloat;
        quSpecRSelIDM.AsInteger:=taSpecIM.AsInteger;
        quSpecRSelKM.AsFloat:=taSpecKM.AsFloat;
        quSpecRSelTCARD.AsInteger:=taSpecTCARD.AsInteger;
        quSpecRSelOPER.AsInteger:=taSpecOper.AsInteger;
        if ISS=2 then
        begin
          quSpecRSelPRICEIN0.AsFloat:=taSpecPriceIn0.AsFloat;
          quSpecRSelSUMIN0.AsFloat:=RoundVal(taSpecSumIn0.AsFloat);
          quSpecRSelSUMOUT0.AsFloat:=rv(taSpecSumOut0.AsFloat);
          quSpecRSelSUMNDSOUT.AsFloat:=rv(taSpecSumR.AsFloat)-rv(taSpecSumOut0.AsFloat);

          sNDS[taSpecINds.AsInteger]:=sNDS[taSpecINds.AsInteger]+RoundVal(taSpecRNds.AsFloat);
        end else
        begin
          quSpecRSelPRICEIN0.AsFloat:=taSpecPriceIn.AsFloat;
          quSpecRSelSUMIN0.AsFloat:=RoundVal(taSpecSumIn.AsFloat);
          quSpecRSelSUMOUT0.AsFloat:=rv(taSpecSumR.AsFloat);
          quSpecRSelSUMNDSOUT.AsFloat:=0;
        end;

        quSpecRSelBZQUANTR.AsFloat:=taSpecBZQUANTR.AsFloat;
        quSpecRSelBZQUANTF.AsFloat:=taSpecBZQUANTF.AsFloat;
        quSpecRSelBZQUANTS.AsFloat:=taSpecBZQUANTS.AsFloat;

        quSpecRSelQUANTDOC.AsFloat:=taSpecQUANTDOC.AsFloat;

        quSpecRSel.Post;

        if taSpecCType.AsInteger=1 then //�����
        begin
          rSum1:=rSum1+RoundVal(taSpecSumIn.AsFloat);
          rSum2:=rSum2+RoundVal(taSpecSumR.AsFloat);
          rSum0:=rSum0+RoundVal(taSpecSumIn0.AsFloat);
          rSumR:=rSumR+rv(taSpecPriceR.AsFloat*taSpecBZQUANTR.AsFloat);
          rSumF:=rSumF+rv(taSpecPriceR.AsFloat*taSpecBZQUANTF.AsFloat);
          rSumS:=rSumS+rv(taSpecPriceR.AsFloat*taSpecBZQUANTS.AsFloat);
        end;
        if taSpecCType.AsInteger=4 then  //����
        begin
          rSumT:=rSumT+RoundVal(taSpecSumIn.AsFloat); //���� � ���������� �����
        end;

      end else
      begin
        showmessage('���������� ������� ���������. ( ��� '+taSpecNum.AsString+', ��� '+taSpecIdGoods.AsString+')');
      end;

      taSpec.Next;
    end;

    quSpecRSel.Active:=False;

    quDocsRId.Edit;

    if iSS<>2 then  quDocsRIdSUMIN.AsFloat:=rSum1 //��� ������ ���� ��� ����������� �� ��������
    else quDocsRIdSUMIN.AsFloat:=rSum0;

    quDocsRIdSUMUCH.AsFloat:=rSum2;
    quDocsRIdBZSUMR.AsFloat:=rSumR;
    quDocsRIdBZSUMF.AsFloat:=rSumF;
    quDocsRIdBZSUMS.AsFloat:=rSumS;

    quDocsRIdSUMTAR.AsFloat:=rSumT;
    quDocsRIdSUMNDS0.AsFloat:=sNDS[1];
    quDocsRIdSUMNDS1.AsFloat:=sNDS[2];
    quDocsRIdSUMNDS2.AsFloat:=sNDS[3];
    quDocsRIdPROCNAC.AsFloat:=cxCalcEdit1.EditValue;
    quDocsRIdIACTIVE.AsInteger:=0;
    quDocsRIdIEDIT.AsInteger:=1;
    quDocsRIdPERSEDIT.AsString:=Person.Name;
    quDocsRIdIPERSEDIT.AsInteger:=Person.Id;
    quDocsRId.Post;

    prSetSync('DocR','SAV',IDH,cxLookupComboBox1.EditValue);

    quDocsRId.Active:=False;

    fmDocsReal.ViewDocsR.BeginUpdate;
    quDocsRSel.FullRefresh;
    quDocsRSel.Locate('ID',IDH,[]);
    fmDocsReal.ViewDocsR.EndUpdate;

    ViewDoc4.EndUpdate;

  end;
end;

procedure TfmAddDoc4.acDelAllExecute(Sender: TObject);
begin
  if PageControl1.ActivePageIndex=0 then
  begin
    if taSpec.RecordCount>0 then
    begin
      if MessageDlg('�� ������������� ������ �������� ������������ �������?',
       mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        taSpec.First; while not taSpec.Eof do taSpec.Delete;
      end;
    end;
  end;  
end;

procedure TfmAddDoc4.N1Click(Sender: TObject);
Type tRecSpec = record
     Num:INteger;
     IdGoods:Integer;
     NameG:String;
     IM:Integer;
     SM:String;
     Quant,Price1,Price2,Sum1,Sum2:Real;
     iNds,CType:INteger;
     sNds:String;
     rNds,SumNac,ProcNac,Km:Real;
     end;
Var RecSpec:TRecSpec;
    iMax:Integer;
begin
  //���������� �������
  if not taSpec.Eof then
  begin
    RecSpec.Num:=taSpecNum.AsInteger;
    RecSpec.IdGoods:=taSpecIdGoods.AsInteger;
    RecSpec.Quant:=taSpecQuant.AsFloat;
    RecSpec.Price1:=taSpecPriceIn.AsFloat;
    RecSpec.Sum1:=taSpecSumIn.AsFloat;
    RecSpec.Price2:=taSpecPriceR.AsFloat;
    RecSpec.Sum2:=taSpecSumR.AsFloat;
    RecSpec.iNds:=taSpecINds.AsInteger;
    RecSpec.rNds:=taSpecRNds.AsFloat;
    RecSpec.IM:=taSpecIM.AsInteger;
    RecSpec.NameG:=taSpecNameG.AsString;
    RecSpec.SM:=taSpecSM.AsString;
    RecSpec.sNds:=taSpecsNds.AsString;
    RecSpec.SumNac:=taSpecSumNac.AsFloat;
    RecSpec.ProcNac:=taSpecProcNac.AsFloat;
    RecSpec.Km:=taSpecKm.AsFloat;
    RecSpec.CType:=taSpecCType.AsInteger;

    iMax:=1;
    ViewDoc4.BeginUpdate;

    taSpec.First;
    if not taSpec.Eof then
    begin
      taSpec.Last;
      iMax:=taSpecNum.AsInteger+1;
    end;

    taSpec.Append;
    taSpecNum.AsInteger:=iMax;
    taSpecIdGoods.AsInteger:=RecSpec.IdGoods;
    taSpecNameG.AsString:=RecSpec.NameG;
    taSpecIM.AsInteger:=RecSpec.IM;
    taSpecSM.AsString:=RecSpec.SM;
    taSpecKm.AsFloat:=RecSpec.Km;
    taSpecQuant.AsFloat:=RecSpec.Quant;
    taSpecPriceIn.AsFloat:=RecSpec.Price1;
    taSpecSumIn.AsFloat:=RecSpec.Sum1;
    taSpecPriceR.AsFloat:=RecSpec.Price2;
    taSpecSumR.AsFloat:=RecSpec.Sum2;
    taSpecINds.AsInteger:=RecSpec.iNds;
    taSpecSNds.AsString:=RecSpec.sNds;
    taSpecRNds.AsFloat:=RecSpec.rNds;
    taSpecSumNac.AsFloat:=RecSpec.SumNac;
    taSpecProcNac.AsFloat:=RecSpec.ProcNac;
    taSpecOper.AsInteger:=0;
    taSpecCType.AsInteger:=RecSpec.CType;
    taSpec.Post;
    ViewDoc4.EndUpdate;
    GridDoc4.SetFocus;
  end;
end;

procedure TfmAddDoc4.cxLabel6Click(Sender: TObject);
begin
  acDelall.Execute;
end;

procedure TfmAddDoc4.cxDateEdit1PropertiesChange(Sender: TObject);
begin
  cxDateEdit2.EditValue:=cxDateEdit1.EditValue;
end;

procedure TfmAddDoc4.ViewDoc4SMPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
Var rK:Real;
    iM:Integer;
    Sm:String;
begin
  with dmO do
  begin
    if taSpecIm.AsInteger>0 then
    begin
      bAddSpec:=True;
      fmMessure.ShowModal;
      if fmMessure.ModalResult=mrOk then
      begin
        iM:=iMSel; iMSel:=0;
        Sm:=prFindKNM(iM,rK); //�������� ����� �������� � ����� �����

        taSpec.Edit;
        taSpecIM.AsInteger:=iM;
        taSpecKm.AsFloat:=rK;
        taSpecSM.AsString:=Sm;
        taSpec.Post;
      end;
    end;
  end;
end;

procedure TfmAddDoc4.cxButton2Click(Sender: TObject);
begin
  Close;
end;

procedure TfmAddDoc4.acSaveDocExecute(Sender: TObject);
begin
  if cxButton1.Enabled then cxButton1.Click;
end;

procedure TfmAddDoc4.acExitDocExecute(Sender: TObject);
begin
  cxButton2.Click;
end;

procedure TfmAddDoc4.ViewTaraDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept:=False;
  if bDrR then  Accept:=True;
end;

procedure TfmAddDoc4.cxButtonEdit1KeyPress(Sender: TObject; var Key: Char);
Var sName:String;
begin
  sName:=cxButtonEdit1.Text+Key;
  if Length(sName)>1 then
  begin
    with dmORep do
    begin
      quFindCli.Close;
      quFindCli.SelectSQL.Clear;
      quFindCli.SelectSQL.Add('SELECT first 2 ID,NAMECL');
      quFindCli.SelectSQL.Add('FROM OF_CLIENTS');
      quFindCli.SelectSQL.Add('where UPPER(NAMECL) like ''%'+AnsiUpperCase(sName)+'%''');
      quFindCli.Open;
      if quFindCli.RecordCount=1 then
      begin
        cxButtonEdit1.Text:=quFindCliNAMECL.AsString;
        cxButtonEdit1.Tag:=quFindCliID.AsInteger;
        Key:=#0;
      end;
      quFindCli.Close;
    end;
  end;
end;

procedure TfmAddDoc4.Excel1Click(Sender: TObject);
begin
  prNExportExel5(ViewDoc4);
end;

procedure TfmAddDoc4.acMovePosExecute(Sender: TObject);
Var iDateB,iDateE,iM:INteger;
    IdCard:INteger;
    NameC:String;
    iMe:INteger;
begin
  //�������� �� ������
  IdCard:=taSpecIdGoods.AsInteger;
  NameC:=taSpecNameG.AsString;
  iMe:=taSpecIM.AsInteger;
  with dmO do
  begin
    if taSpec.RecordCount>0 then
    begin
      //�� ������
      fmSelPerSkl:=tfmSelPerSkl.Create(Application);
      with fmSelPerSkl do
      begin
        cxDateEdit1.Date:=CommonSet.DateFrom;
        quMHAll1.Active:=False;
        quMHAll1.ParamByName('IDPERSON').AsInteger:=Person.Id;
        quMHAll1.Active:=True;
        quMHAll1.First;
        if CommonSet.IdStore>0 then cxLookupComboBox1.EditValue:=CommonSet.IdStore
        else  cxLookupComboBox1.EditValue:=quMHAll1ID.AsInteger;
      end;
      fmSelPerSkl.ShowModal;
      if fmSelPerSkl.ModalResult=mrOk then
      begin
        iDateB:=Trunc(fmSelPerSkl.cxDateEdit1.Date);
        iDateE:=Trunc(fmSelPerSkl.cxDateEdit2.Date)+1;
//        if fmSelPerSkl.cxRadioButton1.Checked then iType:=0 else iType:=1;

        CommonSet.IdStore:=fmSelPerSkl.cxLookupComboBox1.EditValue;
        CommonSet.NameStore:=fmSelPerSkl.cxLookupComboBox1.Text;
        CommonSet.DateFrom:=iDateB;
        CommonSet.DateTo:=iDateE+1;

        CardSel.Id:=IdCard;
        CardSel.Name:=NameC;
        prFindSM(iMe,CardSel.mesuriment,iM);
//        CardSel.NameGr:=ClassTree.Selected.Text;

        prPreShowMove(IdCard,iDateB,iDateE,fmSelPerSkl.cxLookupComboBox1.EditValue,NameC);
        fmSelPerSkl.Release;

//        fmCardsMove.PageControl1.ActivePageIndex:=2;
        fmCardsMove.ShowModal;

      end else fmSelPerSkl.Release;
    end;
  end;
end;

procedure TfmAddDoc4.cxButtonEdit2PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  if dmO.taClients.Active=False then dmO.taClients.Active:=True;
  if fmClients.Visible then fmClients.Close;
  if cxButtonEdit2.Tag>0 then dmO.taClients.Locate('ID',cxButtonEdit2.Tag,[]);
  fmClients.ShowModal;
  if fmClients.ModalResult=mrOk then
  begin
    cxButtonEdit2.Tag:=dmO.taClientsID.AsInteger;
    cxButtonEdit2.Text:=dmO.taClientsNAMECL.AsString;
  end else
  begin
    cxButtonEdit2.Tag:=0;
    cxButtonEdit2.Text:='';
  end;
end;

procedure TfmAddDoc4.taSpecQuantChange(Sender: TField);
begin
  //���������� �����
  if iCol=1 then
  begin
    taSpecSumIn0.AsFloat:=rv(taSpecPriceIn0.AsFloat*taSpecQuant.AsFloat);
    taSpecSumIn.AsFloat:=rv(taSpecPriceIn.AsFloat*taSpecQuant.AsFloat);
    taSpecSumR.AsFloat:=taSpecPriceR.AsFloat*taSpecQuant.AsFloat;
    taSpecSumNac.AsFloat:=taSpecSumR.AsFloat-taSpecSumIn.AsFloat;
    if taSpecSumIn.AsFloat>0.01 then
    begin
      taSpecProcNac.AsFloat:=(taSpecSumR.AsFloat-taSpecSumIn.AsFloat)/taSpecSumIn.AsFloat*100;
    end;
    taSpecRNds.AsFloat:=taSpecSumIn.AsFloat-taSpecSumIn0.AsFloat;

    taSpecSumOut0.AsFloat:=rv(taSpecSumR.AsFloat*100/(100+vFNds(taSpecINds.AsInteger)));
    taSpecRNdsOut.AsFloat:=taSpecSumR.AsFloat-taSpecSumOut0.AsFloat;
  end;
end;

procedure TfmAddDoc4.taSpecPriceInChange(Sender: TField);
begin
  //���������� ����  � ���
  if iCol=2 then
  begin
    taSpecSumIn.AsFloat:=taSpecPriceIn.AsFloat*taSpecQuant.AsFloat;
    taSpecSumIn0.AsFloat:=rv(taSpecSumIn.AsFloat*100/(100+vFNds(taSpecINds.AsInteger)));

    if taSpecQuant.AsFloat<>0 then taSpecPriceIn0.AsFloat:=taSpecSumIn0.AsFloat/taSpecQuant.AsFloat
    else taSpecPriceIn0.AsFloat:=taSpecPriceIn.AsFloat*100/(100+vFNds(taSpecINds.AsInteger));

    //  taSpecSumR.AsFloat:=taSpecPriceR.AsFloat*taSpecQuant.AsFloat;
    taSpecSumNac.AsFloat:=taSpecSumR.AsFloat-taSpecSumIn.AsFloat;
    if taSpecSumIn.AsFloat>0.01 then
    begin
      taSpecProcNac.AsFloat:=(taSpecSumR.AsFloat-taSpecSumIn.AsFloat)/taSpecSumIn.AsFloat*100;
    end;

    taSpecRNds.AsFloat:=taSpecSumIn.AsFloat-taSpecSumIn0.AsFloat;
  end;
end;

procedure TfmAddDoc4.taSpecSumInChange(Sender: TField);
begin
  if iCol=3 then
  begin
    if abs(taSpecQuant.AsFloat)>0 then taSpecPriceIn.AsFloat:=taSpecSumIn.AsFloat/taSpecQuant.AsFloat;

    taSpecSumIn0.AsFloat:=rv(taSpecSumIn.AsFloat*100/(100+vFNds(taSpecINds.AsInteger)));

    if taSpecQuant.AsFloat<>0 then taSpecPriceIn0.AsFloat:=taSpecSumIn0.AsFloat/taSpecQuant.AsFloat
    else taSpecPriceIn0.AsFloat:=taSpecPriceIn.AsFloat*100/(100+vFNds(taSpecINds.AsInteger));

    taSpecSumNac.AsFloat:=taSpecSumR.AsFloat-taSpecSumIn.AsFloat;
    if taSpecSumIn.AsFloat>0.01 then
    begin
      taSpecProcNac.AsFloat:=(taSpecSumR.AsFloat-taSpecSumIn.AsFloat)/taSpecSumIn.AsFloat*100;
    end;

    taSpecRNds.AsFloat:=taSpecSumIn.AsFloat-taSpecSumIn0.AsFloat;

//    taSpecRNds.AsFloat:=RoundEx(taSpecSumIn.AsFloat*vFNds(taSpecINds.AsInteger)/(100+vFNds(taSpecINds.AsInteger))*100)/100;
  end;
end;

procedure TfmAddDoc4.taSpecSumRChange(Sender: TField);
begin
  if iCol=5 then
  begin
    if abs(taSpecQuant.AsFloat)>0 then taSpecPriceR.AsFloat:=RoundEx(taSpecSumR.AsFloat/taSpecQuant.AsFloat*100)/100;
    taSpecSumNac.AsFloat:=taSpecSumR.AsFloat-taSpecSumIn.AsFloat;
    if taSpecSumIn.AsFloat>0.01 then
    begin
      taSpecProcNac.AsFloat:=(taSpecSumR.AsFloat-taSpecSumIn.AsFloat)/taSpecSumIn.AsFloat*100;
    end;

    taSpecSumOut0.AsFloat:=rv(taSpecSumR.AsFloat*100/(100+vFNds(taSpecINds.AsInteger)));
    taSpecRNdsOut.AsFloat:=taSpecSumR.AsFloat-taSpecSumOut0.AsFloat;
  end;
end;


procedure TfmAddDoc4.taSpecPriceRChange(Sender: TField);
begin
  //���������� ����
  if iCol=4 then
  begin
//  taSpecSumIn.AsFloat:=taSpecPriceIn.AsFloat*taSpecQuant.AsFloat;
    taSpecSumR.AsFloat:=taSpecPriceR.AsFloat*taSpecQuant.AsFloat;
    taSpecSumNac.AsFloat:=taSpecSumR.AsFloat-taSpecSumIn.AsFloat;
    if taSpecSumIn.AsFloat>0.01 then
    begin
      taSpecProcNac.AsFloat:=(taSpecSumR.AsFloat-taSpecSumIn.AsFloat)/taSpecSumIn.AsFloat*100;
    end;
    taSpecSumOut0.AsFloat:=rv(taSpecSumR.AsFloat*100/(100+vFNds(taSpecINds.AsInteger)));
    taSpecRNdsOut.AsFloat:=taSpecSumR.AsFloat-taSpecSumOut0.AsFloat;

    taSpecSumOut0.AsFloat:=rv(taSpecSumR.AsFloat*100/(100+vFNds(taSpecINds.AsInteger)));
    taSpecRNdsOut.AsFloat:=taSpecSumR.AsFloat-taSpecSumOut0.AsFloat;
  end;
end;

procedure TfmAddDoc4.taSpecPriceIn0Change(Sender: TField);
begin
  //���������� ����  ������ ��� ���
  if iCol=6 then
  begin
    taSpecSumIn0.AsFloat:=rv(taSpecPriceIn0.AsFloat*taSpecQuant.AsFloat);
    taSpecSumIn.AsFloat:=rv(taSpecSumIn0.AsFloat*(100+vFNds(taSpecINds.AsInteger))/100);

    if taSpecQuant.AsFloat<>0 then taSpecPriceIn.AsFloat:=taSpecSumIn.AsFloat/taSpecQuant.AsFloat
    else taSpecPriceIn.AsFloat:=taSpecPriceIn0.AsFloat*(100+vFNds(taSpecINds.AsInteger))/100;

    //  taSpecSumR.AsFloat:=taSpecPriceR.AsFloat*taSpecQuant.AsFloat;
    taSpecSumNac.AsFloat:=taSpecSumR.AsFloat-taSpecSumIn.AsFloat;
    if taSpecSumIn.AsFloat>0.01 then
    begin
      taSpecProcNac.AsFloat:=(taSpecSumR.AsFloat-taSpecSumIn.AsFloat)/taSpecSumIn.AsFloat*100;
    end;

    taSpecRNds.AsFloat:=taSpecSumIn.AsFloat-taSpecSumIn0.AsFloat;
  end;
end;

procedure TfmAddDoc4.taSpecSumIn0Change(Sender: TField);
begin
  //���������� ����� ������ ��� ���
  if iCol=7 then
  begin
    if taSpecQuant.AsFloat<>0 then taSpecPriceIn0.AsFloat:=taSpecSumIn0.AsFloat/taSpecQuant.AsFloat
    else taSpecPriceIn0.AsFloat:=taSpecPriceIn.AsFloat*100/(100+vFNds(taSpecINds.AsInteger));

    taSpecSumIn.AsFloat:=rv(taSpecSumIn0.AsFloat*(100+vFNds(taSpecINds.AsInteger))/100);

    if taSpecQuant.AsFloat<>0 then taSpecPriceIn.AsFloat:=taSpecSumIn.AsFloat/taSpecQuant.AsFloat;
//    else taSpecPriceIn.AsFloat:=taSpecPriceIn0.AsFloat*(100+vFNds(taSpecINds.AsInteger))/100);

    //  taSpecSumR.AsFloat:=taSpecPriceR.AsFloat*taSpecQuant.AsFloat;
    taSpecSumNac.AsFloat:=taSpecSumR.AsFloat-taSpecSumIn.AsFloat;
    if taSpecSumIn.AsFloat>0.01 then
    begin
      taSpecProcNac.AsFloat:=(taSpecSumR.AsFloat-taSpecSumIn.AsFloat)/taSpecSumIn.AsFloat*100;
    end;

    taSpecRNds.AsFloat:=taSpecSumIn.AsFloat-taSpecSumIn0.AsFloat;
  end;
end;

procedure TfmAddDoc4.taSpecSumOut0Change(Sender: TField);
begin
  if iCol=8 then
  begin
    taSpecRNdsOut.AsFloat:=taSpecSumR.AsFloat-taSpecSumOut0.AsFloat;
  end;
end;

procedure TfmAddDoc4.taSpecRNdsOutChange(Sender: TField);
begin
  if iCol=9 then
  begin
    taSpecSumOut0.AsFloat:=taSpecSumR.AsFloat-taSpecRNdsOut.AsFloat;
  end;
end;

procedure TfmAddDoc4.taSpecINdsChange(Sender: TField);
begin
  if iCol=10 then
  begin
    taSpecSumOut0.AsFloat:=rv(taSpecSumR.AsFloat*100/(100+vFNds(taSpecINds.AsInteger)));
    taSpecRNdsOut.AsFloat:=taSpecSumR.AsFloat-taSpecSumOut0.AsFloat;
  end;
end;

procedure TfmAddDoc4.taSpecBeforePost(DataSet: TDataSet);
Var iSS:INteger;
begin
  iSS:=cxLookupComboBox1.Tag;
  if iSS<>2 then
  begin
    taSpecSumOut0.AsFloat:=taSpecSumR.AsFloat;
    taSpecRNdsOut.AsFloat:=0;
  end;
end;

procedure TfmAddDoc4.taSpecBZQUANTFChange(Sender: TField);
begin
  //���������� ����� ������ �����������
  if iCol=12 then
  begin
    taSpecBZQUANTS.AsFloat:=taSpecBZQUANTF.AsFloat;
    taSpecQuant.AsFloat:=taSpecBZQUANTF.AsFloat;
    taSpecSumIn0.AsFloat:=rv(taSpecPriceIn0.AsFloat*taSpecQuant.AsFloat);
    taSpecSumIn.AsFloat:=rv(taSpecPriceIn.AsFloat*taSpecQuant.AsFloat);
    taSpecSumR.AsFloat:=taSpecPriceR.AsFloat*taSpecQuant.AsFloat;
    taSpecSumNac.AsFloat:=taSpecSumR.AsFloat-taSpecSumIn.AsFloat;
    if taSpecSumIn.AsFloat>0.01 then
    begin
      taSpecProcNac.AsFloat:=(taSpecSumR.AsFloat-taSpecSumIn.AsFloat)/taSpecSumIn.AsFloat*100;
    end;
    taSpecRNds.AsFloat:=taSpecSumIn.AsFloat-taSpecSumIn0.AsFloat;

    taSpecSumOut0.AsFloat:=rv(taSpecSumR.AsFloat*100/(100+vFNds(taSpecINds.AsInteger)));
    taSpecRNdsOut.AsFloat:=taSpecSumR.AsFloat-taSpecSumOut0.AsFloat;
  end;
end;

procedure TfmAddDoc4.taSpecBZQUANTSChange(Sender: TField);
begin
  //���������� ����� ������ ������������
  if iCol=13 then
  begin
    taSpecQuant.AsFloat:=taSpecBZQUANTS.AsFloat;
    taSpecSumIn0.AsFloat:=rv(taSpecPriceIn0.AsFloat*taSpecQuant.AsFloat);
    taSpecSumIn.AsFloat:=rv(taSpecPriceIn.AsFloat*taSpecQuant.AsFloat);
    taSpecSumR.AsFloat:=taSpecPriceR.AsFloat*taSpecQuant.AsFloat;
    taSpecSumNac.AsFloat:=taSpecSumR.AsFloat-taSpecSumIn.AsFloat;
    if taSpecSumIn.AsFloat>0.01 then
    begin
      taSpecProcNac.AsFloat:=(taSpecSumR.AsFloat-taSpecSumIn.AsFloat)/taSpecSumIn.AsFloat*100;
    end;
    taSpecRNds.AsFloat:=taSpecSumIn.AsFloat-taSpecSumIn0.AsFloat;

    taSpecSumOut0.AsFloat:=rv(taSpecSumR.AsFloat*100/(100+vFNds(taSpecINds.AsInteger)));
    taSpecRNdsOut.AsFloat:=taSpecSumR.AsFloat-taSpecSumOut0.AsFloat;
  end;

end;

procedure TfmAddDoc4.acAddTaraExecute(Sender: TObject);
begin
  //�������� ����
  with dmORep do
  begin
    fmTara.ViTara.BeginUpdate;
    quTara.Active:=False;
    quTara.Active:=True;
    fmTara.ViTara.EndUpdate;

    fmTara.Show;
  end;
end;

procedure TfmAddDoc4.cxLabel4Click(Sender: TObject);
begin
  acAddTara.Execute;
end;

procedure TfmAddDoc4.acDelTaraExecute(Sender: TObject);
begin
  //������� �������
  if PageControl1.ActivePageIndex=0 then
  begin
    if taSpec.RecordCount>0 then
    begin
      if taSpecCType.asInteger=4 then  taSpec.Delete;
    end;
  end;
end;

procedure TfmAddDoc4.cxLabel7Click(Sender: TObject);
begin
  acDelTara.Execute;
end;

procedure TfmAddDoc4.cxButton3Click(Sender: TObject);
begin
  if cxButton1.Enabled then
  begin
    cxTextEdit2.Text:=prGetNumSCHF(cxLookupComboBox1.EditValue,DToYY(cxDateEdit2.Date));
  end;
end;

procedure TfmAddDoc4.acSetOperExecute(Sender: TObject);
Var i,j,iNum:INteger;
    Rec:TcxCustomGridRecord;
begin
  if cxButton1.Enabled then
  begin
    if ViewDoc4.Controller.SelectedRecordCount=0 then exit;
    fmSetOper.cxRadioGroup1.ItemIndex:=0;
    fmSetOper.ShowModal;
    if fmSetOper.ModalResult=mrOk then
    begin
      for i:=0 to ViewDoc4.Controller.SelectedRecordCount-1 do
      begin
        Rec:=ViewDoc4.Controller.SelectedRecords[i];
        iNum:=0;
        for j:=0 to Rec.ValueCount-1 do
          if ViewDoc4.Columns[j].Name='ViewDoc4Num' then begin iNum:=Rec.Values[j]; break; end;;

        if (iNum>0) then
        begin
          if taSpec.Locate('Num',iNum,[]) then
          begin
            taSpec.Edit;
            taSpecOper.AsInteger:=fmSetOper.cxRadioGroup1.ItemIndex;
            taSpec.Post;
          end;
        end;
      end;
      ShowMessage('��������� �������� ��');
    end;
  end;
end;

procedure TfmAddDoc4.acSaveQuantDocExecute(Sender: TObject);
Var IDH:INteger;
begin
  //��������� ���-�� �� ���������
  with dmO do
  with dmORep do
  begin
    IdH:=cxTextEdit1.Tag;
    if taSpec.State in [dsEdit,dsInsert] then taSpec.Post;

    if IdH>0 then
    begin
      //�������� ������������
      quSpecRSel.Active:=False;
      quSpecRSel.ParamByName('IDHD').AsInteger:=IDH;
      quSpecRSel.Active:=True;

      taSpec.First;
      while not taSpec.Eof do
      begin
        if quSpecRSel.Locate('IDCARD',taSpecIdGoods.AsInteger,[]) then
        begin
          quSpecRSel.Edit;
          quSpecRSelQUANTDOC.AsFloat:=taSpecQUANTDOC.AsFloat;
          quSpecRSel.Post;
        end;

        taSpec.Next;
      end;

      quSpecRSel.Active:=False;

      showMessage('Ok');
    end;
  end;
end;

procedure TfmAddDoc4.cxButtonEdit3PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  if dmO.taClients.Active=False then dmO.taClients.Active:=True;
  if fmClients.Visible then fmClients.Close;
  if cxButtonEdit3.Tag>0 then dmO.taClients.Locate('ID',cxButtonEdit3.Tag,[]);
  fmClients.ShowModal;
  if fmClients.ModalResult=mrOk then
  begin
    cxButtonEdit3.Tag:=dmO.taClientsID.AsInteger;
    cxButtonEdit3.Text:=dmO.taClientsNAMECL.AsString;
  end else
  begin
    cxButtonEdit3.Tag:=0;
    cxButtonEdit3.Text:='';
  end;
end;

procedure TfmAddDoc4.acSetPriceFromPriceExecute(Sender: TObject);
var iC,iCSet,iCNo:Integer;
    iSS:Integer;
begin
  //��������� ���� �� ������
  with dmORep do
  begin
    if cxButton1.Enabled then
    begin
      if fmDocsPrice.Showing then fmDocsPrice.Close;

      quDocsPrice.Active:=False;
      quDocsPrice.ParamByName('DATEB').AsInteger:=Trunc(cxDateEdit1.Date);
      quDocsPrice.ParamByName('DATEE').AsInteger:=Trunc(cxDateEdit1.Date+1);
      quDocsPrice.Active:=True;

      fmDocsPrice.ShowModal;
      if fmDocsPrice.ModalResult=mrOk then
      begin
        if quDocsPrice.RecordCount>0 then
        begin
          if MessageDlg('���������� ���� �� ������ �� '+ds1(quDocsPriceDDATE.AsDateTime)+' '+quDocsPriceCOMMENT.AsString+'?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
          begin //���� �� ������������ � ������������ ���� ������� �� ������
            iC:=0;
            iCSet:=0;
            iCNo:=0;

            quSpecPrSel.Active:=False;
            quSpecPrSel.ParamByName('IDH').AsInteger:=quDocsPriceID.AsInteger;
            quSpecPrSel.Active:=True;

            Memo1.Clear;

            ViewDoc4.BeginUpdate;

            try
              if taSpec.RecordCount=0 then
              begin  //��������� �� ������
                iSS:=prIss(fmAddDoc4.cxLookupComboBox1.EditValue);

                quSpecPrSel.First;
                while not quSpecPrSel.Eof do
                begin
                  inc(iC);

                  taSpec.Append;
                  taSpecNum.AsInteger:=iC;
                  taSpecIdGoods.AsInteger:=quSpecPrSelICODE.AsInteger;
                  taSpecNameG.AsString:=quSpecPrSelNAME.AsString;
                  taSpecIM.AsInteger:=quSpecPrSelIM.AsInteger;
                  taSpecSM.AsString:=quSpecPrSelNAMESHORT.AsString;
                  taSpecKm.AsFloat:=quSpecPrSelKM.AsFloat;
                  taSpecQuant.AsFloat:=0;
                  taSpecPriceIn.AsFloat:=0;
                  taSpecSumIn.AsFloat:=0;
                  taSpecPriceR.AsFloat:=quSpecPrSelNEWPRICE.AsFloat;
                  taSpecSumR.AsFloat:=0;
                  if iSS<>2 then
                  begin
                    taSpecINds.AsInteger:=1;
                    taSpecSNds.AsString:='��� ���';
                  end else
                  begin
                    taSpecINds.AsInteger:=quSpecPrSelINDS.AsInteger;
                    taSpecSNds.AsString:=quSpecPrSelNAMENDS.AsString;
                  end;
                  taSpecRNds.AsFloat:=0;
                  taSpecSumNac.AsFloat:=0;
                  taSpecProcNac.AsFloat:=0;
                  taSpecTCard.AsInteger:=quSpecPrSelTCARD.AsInteger;
                  taSpecOper.AsInteger:=0;
                  taSpecCType.AsInteger:=quSpecPrSelCATEGORY.AsInteger;
                  taSpecPriceIn0.AsFloat:=0;
                  taSpecSumIn0.AsFloat:=0;
                  taSpecSumOut0.AsFloat:=0;
                  taSpecRNdsOut.AsFloat:=0;
                  taSpec.Post;

                  quSpecPrSel.Next;
                end;
                Memo1.Lines.Add('������� ����� '+its(iC));
                Memo1.Lines.Add('������� ��������.');

              end else //��������� ������ ���� �� ������
              begin
                taSpec.First;
                while not taSpec.Eof do
                begin
                  inc(iC);

                  if quSpecPrSel.Locate('ICODE',taSpecIdGoods.AsInteger,[]) then
                  begin
                    Inc(iCSet);

                    iCol:=4;

                    taSpec.Edit;
                    taSpecPriceR.AsFloat:=quSpecPrSelNEWPRICE.AsFloat;
                    taSpec.Post;

                    iCol:=0;

                  end else inc(iCNo);

                  taSpec.Next;
                end;

                Memo1.Lines.Add('������� ����� '+its(iC)+', ���������� �� '+its(iCSet)+', ����������� '+its(iCNo));
                Memo1.Lines.Add('������� ��������.');
              end;

            except
              Memo1.Lines.Add('������ ��������� ����.');
            end;
            ViewDoc4.EndUpdate;
            quSpecPrSel.Active:=False;
          end;
        end;
      end;
    end;
  end;
end;

end.
