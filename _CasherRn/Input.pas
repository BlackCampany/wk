unit Input;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, FIBDataSet, pFIBDataSet, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ActnList, XPStyleActnCtrls,
  ActnMan, ExtCtrls;

type
  TfmInput = class(TForm)
    StatusBar1: TStatusBar;
    ViewF: TcxGridDBTableView;
    LevelF: TcxGridLevel;
    GridF: TcxGrid;
    quInputF: TpFIBDataSet;
    dsquInputF: TDataSource;
    quInputFIDC: TFIBIntegerField;
    quInputFSHORTNAME: TFIBStringField;
    quInputFRECEIPTNUM: TFIBStringField;
    quInputFPCOUNT: TFIBIntegerField;
    quInputFPVES: TFIBFloatField;
    quInputFCURMESSURE: TFIBIntegerField;
    quInputFNAMESHORT: TFIBStringField;
    quInputFNETTO: TFIBFloatField;
    ViewFIDC: TcxGridDBColumn;
    ViewFSHORTNAME: TcxGridDBColumn;
    ViewFRECEIPTNUM: TcxGridDBColumn;
    ViewFPCOUNT: TcxGridDBColumn;
    ViewFPVES: TcxGridDBColumn;
    ViewFNAMESHORT: TcxGridDBColumn;
    ViewFNETTO: TcxGridDBColumn;
    quInputFPARENT: TFIBIntegerField;
    PopupMenu1: TPopupMenu;
    Excel1: TMenuItem;
    N1: TMenuItem;
    JN1: TMenuItem;
    amInput: TActionManager;
    acOpenTTKFromInput: TAction;
    quInputFSGR: TStringField;
    quInputFSSGR: TStringField;
    ViewFSGR: TcxGridDBColumn;
    ViewFSSGR: TcxGridDBColumn;
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    procedure cxButton2Click(Sender: TObject);
    procedure ViewFDblClick(Sender: TObject);
    procedure Excel1Click(Sender: TObject);
    procedure acOpenTTKFromInputExecute(Sender: TObject);
    procedure quInputFCalcFields(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmInput: TfmInput;

implementation

uses dmOffice, Un1, TCard, DMOReps;

{$R *.dfm}

procedure TfmInput.cxButton2Click(Sender: TObject);
begin
  close;
end;

procedure TfmInput.ViewFDblClick(Sender: TObject);
begin
  ModalResult:=mrOk;
end;

procedure TfmInput.Excel1Click(Sender: TObject);
begin
  prNExportExel5(ViewF);
end;

procedure TfmInput.acOpenTTKFromInputExecute(Sender: TObject);
//������� ���
Var iCurDate:INteger;
    kBrutto,rN:Real;
    iMax:Integer;
begin
  if not CanDo('prViewTCards') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  if fmTCard.Visible then if MessageDlg('������� �������� ��������?',mtConfirmation, [mbYes, mbNo], 0) <> mrYes then exit;
  bSet:=False;
  with dmO do
  begin
    if quInputF.RecordCount>0 then
    begin
      quTCards.Active:=False;
      quTCards.ParamByName('IDGOOD').AsInteger:=quInputFIDC.AsInteger;
      quTCards.Active:=True;

      fmTCard.Label6.Caption:= IntToStr(quInputFIDC.AsInteger);
      fmTCard.cxTextEdit1.Text:=quInputFSHORTNAME.AsString;
      fmTCard.cxTextEdit1.Tag:=quInputFIDC.AsInteger;

      CloseTa(fmTCard.taTSpec);
//      fmTCard.taTSpec.First; while not fmTCard.taTSpec.Eof do fmTCard.taTSpec.delete;

      if taTermoObr.Active=False then taTermoObr.Active:=True;

      if quTCards.RecordCount=0 then
      begin
        fmTCard.Panel3.Visible:=False;
        quTSpec.Active:=False;
      end  else
      begin
        with fmTCard do
        begin
          Panel3.Visible:=True;
          iColTC:=0;  //��� ���� ��������� ������������� �� ����

          quTCards.Last;
          cxTextEdit2.Text:=quTCardsSHORTNAME.AsString;
          cxTextEdit3.Text:=quTCardsRECEIPTNUM.AsString;
          cxTextEdit4.Text:=quTCardsPOUTPUT.AsString;
          cxSpinEdit1.EditValue:=quTCardsPCOUNT.AsInteger;
          cxCalcEdit1.EditValue:=quTCardsPVES.AsFloat;

          cxLookupComboBox1.EditValue:=quTCardsIOBR.AsInteger;

          quTSpec.Active:=False;
          quTSpec.ParamByName('IDC').AsInteger:=quTCardsIDCARD.AsInteger;
          quTSpec.ParamByName('IDTC').AsInteger:=quTCardsID.AsInteger;
          quTSpec.Active:=True;

          ViewTSpec.BeginUpdate;
          iMax:=1;
          quTSpec.First;
          while not quTSpec.Eof do
          begin
            taTSpec.Append;
            taTSpecNum.AsInteger:=iMax;
            taTSpecIdCard.AsInteger:=quTSpecIDCARD.AsInteger;
            taTSpecIdM.AsInteger:=quTSpecCURMESSURE.AsInteger;
            taTSpecSM.AsString:=quTSpecNAMESHORT.AsString;
            taTSpecKm.AsFloat:=quTSpecKOEF.AsFloat;
            taTSpecName.AsString:=quTSpecNAME.AsString;
            taTSpecNetto.AsFloat:=quTSpecNETTO.AsFloat;
            taTSpecBrutto.AsFloat:=quTSpecBRUTTO.asfloat;
            taTSpecKnb.AsFloat:=0;
            taTSpecTCard.AsInteger:=quTSpecTCARD.AsInteger;
            taTSpecNetto1.AsFloat:=quTSpecNETTO1.AsFloat;
            taTSpecComment.AsString:=quTSpecComment.AsString;

            taTSpecPr1.AsFloat:=0;
            if taTSpecNetto.AsFloat>0 then taTSpecPr1.AsFloat:=RoundEx((taTSpecNetto.AsFloat-taTSpecNetto1.AsFloat)/taTSpecNetto.AsFloat*10000)/100;

            taTSpecNetto2.AsFloat:=quTSpecNETTO2.AsFloat;
            taTSpecPr2.AsFloat:=0;
            if taTSpecNetto1.AsFloat>0 then taTSpecPr2.AsFloat:=RoundEx((taTSpecNetto1.AsFloat-taTSpecNetto2.AsFloat)/taTSpecNetto1.AsFloat*10000)/100;

            taTSpecIOBR.AsInteger:=quTSpecIOBR.AsInteger;
            taTSpecIChange.AsInteger:=quTSpecICHANGE.AsInteger;
            taTSpec.Post;

            quTSpec.Next;   inc(iMax);
          end;
          quTSpec.Active:=False;

          //������������� ������
          iCurDate:=prDateToI(Date);

          taTSpec.First;
          while not taTSpec.Eof do
          begin
            kBrutto:=0;
            quFindEU.Active:=False;
            quFindEU.ParamByName('GOODSID').AsInteger:=taTSpecIDCARD.AsInteger;
            quFindEU.ParamByName('DATEB').AsInteger:=iCurDate;
            quFindEU.ParamByName('DATEE').AsInteger:=iCurDate;
            quFindEU.Active:=True;

            if quFindEU.RecordCount>0 then
            begin
              quFindEU.First;
              kBrutto:=quFindEUTO100GRAMM.AsFloat;
            end;

            rN:=taTSpecNETTO.AsFloat;
            quFindEU.Active:=False;

            taTSpec.Edit;
            taTSpecBRUTTO.AsFloat:=rN*(100+kBrutto)/100;
            taTSpecKNB.AsFloat:=kBrutto;
            taTSpec.Post;
            taTSpec.Next;
          end;
          taTSpec.First;
          ViewTSpec.EndUpdate;
        end;
      end;

//      fmTCard.ViewTSpec.OptionsData.Editing:=False;
      fmTCard.prColsEnables(False);
      
      fmTCard.cxLabel1.Enabled:=False;
      fmTCard.cxLabel2.Enabled:=False;
      fmTCard.Image1.Enabled:=False;
      fmTCard.Image2.Enabled:=False;
      fmTCard.cxButton1.Enabled:=False;
      fmTCard.cxButton10.Enabled:=False;

      TK.Add:=False;
      TK.Edit:=False;

      fmTCard.Caption:='��������������� �����. '+IntToStr(quInputFIDC.AsInteger)+' '+quInputFSHORTNAME.AsString;

      fmTCard.Show;
    end else
    begin
      showmessage('�������� ������ ��� �������������� !!');
    end;
  end;
  bSet:=True;
end;


procedure TfmInput.quInputFCalcFields(DataSet: TDataSet);
Var S1,S2:String;
begin
  prFind2group(quInputFPARENT.AsInteger,S1,S2);
  quInputFSGR.AsString:=S1;
  quInputFSSGR.AsString:=S1;
end;

procedure TfmInput.FormCreate(Sender: TObject);
begin
  GridF.Align:=AlClient;
end;

end.
