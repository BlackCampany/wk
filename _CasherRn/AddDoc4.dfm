object fmAddDoc4: TfmAddDoc4
  Left = 385
  Top = 145
  Width = 939
  Height = 726
  Caption = #1044#1086#1082#1091#1084#1077#1085#1090#1099' '#1088#1077#1072#1083#1080#1079#1072#1094#1080#1103
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 923
    Height = 145
    Align = alTop
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 1
    object Label1: TLabel
      Left = 24
      Top = 16
      Width = 65
      Height = 13
      Caption = #1044#1086#1082#1091#1084#1077#1085#1090' '#8470
      Transparent = True
    end
    object Label2: TLabel
      Left = 24
      Top = 40
      Width = 33
      Height = 13
      Caption = #1057#1063#1060#1050
      Transparent = True
    end
    object Label3: TLabel
      Left = 252
      Top = 40
      Width = 11
      Height = 13
      Caption = #1086#1090
      Transparent = True
    end
    object Label4: TLabel
      Left = 24
      Top = 72
      Width = 58
      Height = 13
      Caption = #1050#1086#1085#1090#1088#1072#1075#1077#1085#1090
      Transparent = True
    end
    object Label5: TLabel
      Left = 24
      Top = 96
      Width = 82
      Height = 13
      Caption = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103
      Transparent = True
    end
    object Label12: TLabel
      Left = 252
      Top = 16
      Width = 11
      Height = 13
      Caption = #1086#1090
      Transparent = True
    end
    object Label6: TLabel
      Left = 432
      Top = 16
      Width = 54
      Height = 13
      Caption = #1057#1082#1080#1076#1082#1072' (%)'
      Transparent = True
    end
    object Label7: TLabel
      Left = 424
      Top = 72
      Width = 37
      Height = 13
      Caption = #1086#1090' '#1082#1086#1075#1086
      Transparent = True
    end
    object Label8: TLabel
      Left = 272
      Top = 96
      Width = 114
      Height = 13
      Caption = #1059#1087#1086#1083#1085#1086#1084#1086#1095#1077#1085#1085#1086#1077' '#1083#1080#1094#1086
      Transparent = True
    end
    object Label9: TLabel
      Left = 420
      Top = 124
      Width = 48
      Height = 13
      Caption = #1042#1086#1076#1080#1090#1077#1083#1100
      Transparent = True
    end
    object Label10: TLabel
      Left = 24
      Top = 120
      Width = 129
      Height = 13
      Caption = #1054#1088#1075#1072#1085#1080#1079#1072#1094#1080#1103' '#1087#1077#1088#1077#1074#1086#1079#1095#1080#1082
      Transparent = True
    end
    object cxTextEdit1: TcxTextEdit
      Left = 112
      Top = 12
      Properties.MaxLength = 15
      TabOrder = 0
      Text = 'cxTextEdit1'
      Width = 125
    end
    object cxDateEdit1: TcxDateEdit
      Left = 272
      Top = 12
      Properties.OnChange = cxDateEdit1PropertiesChange
      Style.BorderStyle = ebsOffice11
      TabOrder = 1
      Width = 121
    end
    object cxTextEdit2: TcxTextEdit
      Left = 112
      Top = 36
      Properties.MaxLength = 15
      TabOrder = 2
      Text = 'cxTextEdit2'
      Width = 105
    end
    object cxDateEdit2: TcxDateEdit
      Left = 272
      Top = 36
      Style.BorderStyle = ebsOffice11
      TabOrder = 3
      Width = 121
    end
    object cxButtonEdit1: TcxButtonEdit
      Left = 112
      Top = 68
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
      Style.BorderStyle = ebsOffice11
      TabOrder = 4
      Text = 'cxButtonEdit1'
      OnKeyPress = cxButtonEdit1KeyPress
      Width = 281
    end
    object cxLookupComboBox1: TcxLookupComboBox
      Left = 112
      Top = 92
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Caption = #1053#1072#1079#1074#1072#1085#1080#1077
          FieldName = 'NAMEMH'
        end>
      Properties.ListOptions.AnsiSort = True
      Properties.ListSource = dmO.dsMHAll
      Properties.OnChange = cxLookupComboBox1PropertiesChange
      Style.BorderStyle = ebsOffice11
      Style.LookAndFeel.Kind = lfOffice11
      Style.PopupBorderStyle = epbsDefault
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 5
      Width = 145
    end
    object cxCalcEdit1: TcxCalcEdit
      Left = 496
      Top = 12
      EditValue = 0.000000000000000000
      Properties.Alignment.Horz = taRightJustify
      Style.BorderStyle = ebsOffice11
      TabOrder = 6
      Width = 89
    end
    object cxButtonEdit2: TcxButtonEdit
      Left = 472
      Top = 68
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.OnButtonClick = cxButtonEdit2PropertiesButtonClick
      Style.BorderStyle = ebsOffice11
      TabOrder = 7
      Text = 'cxButtonEdit2'
      Width = 233
    end
    object cxRadioGroup1: TcxRadioGroup
      Left = 716
      Top = 8
      Caption = #1057#1090#1072#1090#1091#1089' '#1079#1072#1082#1072#1079#1072
      Properties.Items = <
        item
          Caption = #1057#1092#1086#1088#1084#1080#1088#1086#1074#1072#1085
        end
        item
          Caption = #1055#1086#1076#1090#1074#1077#1088#1078#1076#1077#1085
        end
        item
          Caption = #1054#1090#1075#1088#1091#1078#1077#1085
        end
        item
          Caption = #1055#1086#1083#1091#1095#1077#1085
        end>
      Properties.ReadOnly = True
      TabOrder = 8
      Height = 109
      Width = 185
    end
    object cxButton3: TcxButton
      Left = 216
      Top = 36
      Width = 21
      Height = 21
      TabOrder = 9
      OnClick = cxButton3Click
      Glyph.Data = {
        66020000424D660200000000000036000000280000000D0000000E0000000100
        18000000000030020000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFCB84D6B26BD6B26B
        D6B26BD6B26BFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE7
        BE7BFFD794DEA67BAD8A39FFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFD794E7BE73FFF3ADFFE79CBD964AFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFAD8A42FFFFC6FFF3ADDEA67BBD964AFFFFFFFFFFFFFFFF
        FF00FFFFFFFFFFFFFFFFFFFFFFFFFFD794DEA67BFFFFC6FFFFC6AD8A42FFFFFF
        FFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFF9C7D31FFF3ADFFFFC6FF
        FFC69C7D31FFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFE7BE7B8C6D188C6D188C6D
        18DEA67BFFFFC6FFFFC68C6D188C6D188C6D18E7BE7BFFFFFF00FFFFFFFFFFFF
        8C6D18FF9E29DEA67BDEA67BDEA67BFFFFC6FFFFC6DEA67B846100FFFFFFFFFF
        FF00FFFFFFFFFFFFFFFFFF8C6D18FF9E29DEA67BDEA67BDEA67BDEA67B846100
        FFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFF8C6D18FF9E29DEA67BFF
        9E29846100FFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FF8C6D18FF9E29846100FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF8C6D18FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFF00}
      LookAndFeel.Kind = lfOffice11
    end
    object cxLookupComboBox2: TcxLookupComboBox
      Left = 404
      Top = 92
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Caption = #1053#1072#1079#1074#1072#1085#1080#1077
          FieldName = 'NAME'
        end>
      Properties.ListOptions.AnsiSort = True
      Properties.ListSource = dmORep.dsquUPLMH
      Style.BorderStyle = ebsOffice11
      Style.LookAndFeel.Kind = lfOffice11
      Style.PopupBorderStyle = epbsDefault
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 10
      Width = 301
    end
    object cxLookupComboBox3: TcxLookupComboBox
      Left = 472
      Top = 116
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Caption = #1042#1086#1076#1080#1090#1077#1083#1100' ('#1072#1074#1090#1086')'
          FieldName = 'FIO_CAR'
        end>
      Properties.ListOptions.AnsiSort = True
      Properties.ListSource = dmORep.dsquDrv1
      Style.BorderStyle = ebsOffice11
      Style.LookAndFeel.Kind = lfOffice11
      Style.PopupBorderStyle = epbsDefault
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 11
      Width = 233
    end
    object cxButtonEdit3: TcxButtonEdit
      Left = 168
      Top = 116
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.OnButtonClick = cxButtonEdit3PropertiesButtonClick
      Style.BorderStyle = ebsOffice11
      TabOrder = 12
      Text = 'cxButtonEdit3'
      Width = 225
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 669
    Width = 923
    Height = 19
    Color = clWhite
    Panels = <
      item
        Width = 300
      end
      item
        Width = 50
      end>
  end
  object Panel2: TPanel
    Left = 0
    Top = 600
    Width = 923
    Height = 69
    Align = alBottom
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 2
    object cxButton1: TcxButton
      Left = 32
      Top = 24
      Width = 121
      Height = 25
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100'   Ctrl+S'
      TabOrder = 0
      OnClick = cxButton1Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 208
      Top = 24
      Width = 137
      Height = 25
      Caption = #1042#1099#1093#1086#1076'    F10'
      ModalResult = 2
      TabOrder = 1
      OnClick = cxButton2Click
      Glyph.Data = {
        5E040000424D5E04000000000000360000002800000012000000130000000100
        18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
        8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
        0000CED3D6848684848684848684848684848684848684848684848684848684
        848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
        7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
        FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
        FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
        007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
        FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
        00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
        494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
        00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000}
      LookAndFeel.Kind = lfOffice11
    end
    object Memo1: TcxMemo
      Left = 392
      Top = 2
      Align = alRight
      Lines.Strings = (
        'Memo1')
      TabOrder = 2
      Height = 65
      Width = 529
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 145
    Width = 153
    Height = 455
    Align = alLeft
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 3
    object Label11: TLabel
      Left = 8
      Top = 168
      Width = 104
      Height = 13
      Caption = #1059#1089#1090'. '#1094#1077#1085#1099' '#1087#1086' '#1087#1088#1072#1081#1089#1091
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
      OnClick = acSetPriceFromPriceExecute
    end
    object cxLabel1: TcxLabel
      Left = 8
      Top = 32
      Cursor = crHandPoint
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1089#1087#1080#1089#1086#1082' Ctrl+Ins'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = 4227072
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel1Click
    end
    object cxLabel2: TcxLabel
      Left = 8
      Top = 72
      Cursor = crHandPoint
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1087#1086#1079#1080#1094#1080#1102'  F8'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clRed
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel2Click
    end
    object cxLabel3: TcxLabel
      Left = 8
      Top = 136
      Cursor = crHandPoint
      Caption = #1055#1077#1088#1077#1089#1095#1080#1090#1072#1090#1100' '#1094#1077#1085#1099
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clBlack
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel3Click
    end
    object cxLabel4: TcxLabel
      Left = 11
      Top = 220
      Cursor = crHandPoint
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1090#1072#1088#1091
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = 4227072
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel4Click
    end
    object cxLabel5: TcxLabel
      Left = 8
      Top = 16
      Cursor = crHandPoint
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1087#1086#1079#1080#1094#1080#1102' Ins'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = 4227072
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel5Click
    end
    object cxLabel6: TcxLabel
      Left = 8
      Top = 88
      Cursor = crHandPoint
      Caption = #1054#1095#1080#1089#1090#1080#1090#1100' Ctrl+F8'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clRed
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel6Click
    end
    object cxLabel7: TcxLabel
      Left = 11
      Top = 240
      Cursor = crHandPoint
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1090#1072#1088#1091
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clRed
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsUnderline]
      Style.IsFontAssigned = True
      Properties.Orientation = cxoLeftTop
      Properties.PenWidth = 3
      Transparent = True
      OnClick = cxLabel7Click
    end
  end
  object PageControl1: TPageControl
    Left = 160
    Top = 180
    Width = 717
    Height = 345
    ActivePage = TabSheet1
    Style = tsFlatButtons
    TabOrder = 4
    object TabSheet1: TTabSheet
      Caption = #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1103
      object GridDoc4: TcxGrid
        Left = 0
        Top = 0
        Width = 709
        Height = 314
        Align = alClient
        PopupMenu = PopupMenu1
        TabOrder = 0
        LookAndFeel.Kind = lfOffice11
        object ViewDoc4: TcxGridDBTableView
          OnDragDrop = ViewDoc4DragDrop
          OnDragOver = ViewDoc4DragOver
          NavigatorButtons.ConfirmDelete = False
          OnEditing = ViewDoc4Editing
          OnEditKeyDown = ViewDoc4EditKeyDown
          OnEditKeyPress = ViewDoc4EditKeyPress
          DataController.DataSource = dsSpec
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = ',0.00'#1088#39'.'#39';-,0.00'#1088#39'.'#39
              Kind = skSum
              FieldName = 'RNds'
              Column = ViewDoc4RNds
            end
            item
              Format = ',0.00'#1088#39'.'#39';-,0.00'#1088#39'.'#39
              Kind = skSum
              FieldName = 'SumIn'
              Column = ViewDoc4SumIn
            end
            item
              Format = ',0.00'#1088#39'.'#39';-,0.00'#1088#39'.'#39
              Kind = skSum
              FieldName = 'SumR'
              Column = ViewDoc4SumR
            end
            item
              Format = ',0.00'#1088#39'.'#39';-,0.00'#1088#39'.'#39
              Kind = skSum
              FieldName = 'SumNac'
              Column = ViewDoc4SumNac
            end
            item
              Format = ',0.0%;-,0.0%'
              Kind = skAverage
              FieldName = 'ProcNac'
              Column = ViewDoc4ProcNac
            end
            item
              Format = '0.00'
              Kind = skSum
              FieldName = 'SumIn0'
              Column = ViewDoc4SumIn0
            end
            item
              Format = '0.00'
              Kind = skSum
              FieldName = 'SumOut0'
              Column = ViewDoc4SumOut0
            end
            item
              Format = '0.00'
              Kind = skSum
              FieldName = 'RNdsOut'
              Column = ViewDoc4RNdsOut
            end
            item
              Format = '0.000'
              Kind = skSum
              FieldName = 'BZQUANTF'
              Column = ViewDoc4BZQUANTF
            end
            item
              Format = '0.000'
              Kind = skSum
              FieldName = 'BZQUANTR'
              Column = ViewDoc4BZQUANTR
            end
            item
              Format = '0.000'
              Kind = skSum
              FieldName = 'BZQUANTS'
              Column = ViewDoc4BZQUANTS
            end
            item
              Format = '0.000'
              Kind = skSum
              FieldName = 'Quant'
              Column = ViewDoc4Quant
            end>
          DataController.Summary.SummaryGroups = <>
          OptionsCustomize.ColumnGrouping = False
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsData.Inserting = False
          OptionsSelection.MultiSelect = True
          OptionsView.Footer = True
          OptionsView.GroupByBox = False
          OptionsView.Indicator = True
          object ViewDoc4Num: TcxGridDBColumn
            Caption = #8470' '#1087#1087
            DataBinding.FieldName = 'Num'
            Options.Editing = False
            Width = 51
          end
          object ViewDoc4IdGoods: TcxGridDBColumn
            Caption = #1050#1086#1076
            DataBinding.FieldName = 'IdGoods'
            Width = 50
          end
          object ViewDoc4NameG: TcxGridDBColumn
            Caption = #1053#1072#1079#1074#1072#1085#1080#1077
            DataBinding.FieldName = 'NameG'
            Width = 179
          end
          object ViewDoc4IM: TcxGridDBColumn
            Caption = #1050#1086#1076' '#1045#1076'.'#1080#1079#1084'.'
            DataBinding.FieldName = 'IM'
            Options.Editing = False
          end
          object ViewDoc4SM: TcxGridDBColumn
            Caption = #1045#1076'.'#1080#1079#1084'.'
            DataBinding.FieldName = 'SM'
            PropertiesClassName = 'TcxButtonEditProperties'
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.ReadOnly = True
            Properties.OnButtonClick = ViewDoc4SMPropertiesButtonClick
            Width = 57
          end
          object ViewDoc4Quant: TcxGridDBColumn
            Caption = #1050#1086#1083'-'#1074#1086
            DataBinding.FieldName = 'Quant'
            Styles.Content = dmO.cxStyle31
          end
          object ViewDoc4PriceIn: TcxGridDBColumn
            Caption = #1062#1077#1085#1072' '#1079#1072#1082#1091#1087'. (c '#1053#1044#1057')'
            DataBinding.FieldName = 'PriceIn'
          end
          object ViewDoc4SumIn: TcxGridDBColumn
            Caption = #1057#1091#1084#1084#1072' '#1079#1072#1082#1091#1087'. ('#1089' '#1053#1044#1057')'
            DataBinding.FieldName = 'SumIn'
            Options.Editing = False
          end
          object ViewDoc4PriceR: TcxGridDBColumn
            Caption = #1062#1077#1085#1072' '#1087#1088#1086#1076#1072#1078#1085'.'
            DataBinding.FieldName = 'PriceR'
            Styles.Content = dmO.cxStyle25
          end
          object ViewDoc4SumR: TcxGridDBColumn
            Caption = #1057#1091#1084#1084#1072' '#1087#1088#1086#1076#1072#1078#1085'. '#1089' '#1053#1044#1057
            DataBinding.FieldName = 'SumR'
            Styles.Content = dmO.cxStyle25
          end
          object ViewDoc4INds: TcxGridDBColumn
            Caption = #1058#1080#1087' '#1053#1044#1057
            DataBinding.FieldName = 'INds'
            PropertiesClassName = 'TcxImageComboBoxProperties'
            Properties.Items = <
              item
                Description = '0%'
                ImageIndex = 0
                Value = 1
              end
              item
                Description = '10%'
                Value = 2
              end
              item
                Description = '18%'
                Value = 3
              end>
          end
          object ViewDoc4RNds: TcxGridDBColumn
            Caption = #1057#1091#1084#1084#1072' '#1053#1044#1057' ('#1079#1072#1082#1091#1087'.)'
            DataBinding.FieldName = 'RNds'
            Options.Editing = False
            Width = 124
          end
          object ViewDoc4SumNac: TcxGridDBColumn
            Caption = #1057#1091#1084#1084#1072' '#1085#1072#1094#1077#1085#1082#1080
            DataBinding.FieldName = 'SumNac'
            Options.Editing = False
            Width = 75
          end
          object ViewDoc4ProcNac: TcxGridDBColumn
            Caption = '% '#1085#1072#1094#1077#1085#1082#1080
            DataBinding.FieldName = 'ProcNac'
            Options.Editing = False
          end
          object ViewDoc4Km: TcxGridDBColumn
            Caption = #1050#1086#1077#1092'.'#1077#1076'.'#1080#1079#1084'.'
            DataBinding.FieldName = 'Km'
          end
          object ViewDoc4TCard: TcxGridDBColumn
            Caption = #1058#1050
            DataBinding.FieldName = 'TCard'
            PropertiesClassName = 'TcxImageComboBoxProperties'
            Properties.Images = dmO.imState
            Properties.Items = <
              item
                Description = #1085#1077#1090
                Value = 0
              end
              item
                Description = #1090#1082
                ImageIndex = 11
                Value = 1
              end>
            Width = 50
          end
          object ViewDoc4Oper: TcxGridDBColumn
            Caption = #1054#1087#1077#1088#1072#1094#1080#1103
            DataBinding.FieldName = 'Oper'
            PropertiesClassName = 'TcxImageComboBoxProperties'
            Properties.Items = <
              item
                Description = #1088#1077#1072#1083#1080#1079#1072#1094#1080#1103
                ImageIndex = 0
                Value = 0
              end
              item
                Description = #1085#1072' '#1089#1087#1080#1089#1072#1085#1080#1077
                Value = 1
              end
              item
                Description = #1085#1072' '#1087#1077#1088#1077#1088#1072#1073#1086#1090#1082#1091
                Value = 2
              end>
            Width = 100
          end
          object ViewDoc4CTO: TcxGridDBColumn
            Caption = #1058#1059
            DataBinding.FieldName = 'CTO'
            Options.Editing = False
            Width = 200
          end
          object ViewDoc4CType: TcxGridDBColumn
            Caption = #1058#1080#1087' '#1087#1086#1079#1080#1094#1080#1080
            DataBinding.FieldName = 'CType'
            PropertiesClassName = 'TcxImageComboBoxProperties'
            Properties.Items = <
              item
                Description = #1058#1086#1074#1072#1088
                ImageIndex = 0
                Value = 1
              end
              item
                Description = #1059#1089#1083#1091#1075#1072
                Value = 2
              end
              item
                Description = #1040#1074#1072#1085#1089
                Value = 3
              end
              item
                Description = #1058#1072#1088#1072
                Value = 4
              end>
            Options.Editing = False
          end
          object ViewDoc4PriceIn0: TcxGridDBColumn
            Caption = #1062#1077#1085#1072' '#1079#1072#1082#1091#1087'. ('#1073#1077#1079' '#1053#1044#1057')'
            DataBinding.FieldName = 'PriceIn0'
            Options.Editing = False
          end
          object ViewDoc4SumIn0: TcxGridDBColumn
            Caption = #1057#1091#1084#1084#1072' '#1079#1072#1082#1091#1087'. ('#1073#1077#1079' '#1053#1044#1057')'
            DataBinding.FieldName = 'SumIn0'
            Options.Editing = False
          end
          object ViewDoc4SumOut0: TcxGridDBColumn
            Caption = #1057#1091#1084#1084#1072' '#1087#1088#1086#1076'. '#1073#1077#1079' '#1053#1044#1057
            DataBinding.FieldName = 'SumOut0'
          end
          object ViewDoc4RNdsOut: TcxGridDBColumn
            Caption = #1057#1091#1084#1084#1072' '#1053#1044#1057' ('#1087#1088#1086#1076'.)'
            DataBinding.FieldName = 'RNdsOut'
            Width = 90
          end
          object ViewDoc4BZQUANTR: TcxGridDBColumn
            Caption = #1050#1086#1083'-'#1074#1086' '#1082' '#1079#1072#1082#1072#1079#1091' '#1088#1072#1089#1095#1077#1090#1085#1086#1077
            DataBinding.FieldName = 'BZQUANTR'
            Options.Editing = False
            Styles.Content = dmO.cxStyle3
            Width = 66
          end
          object ViewDoc4BZQUANTF: TcxGridDBColumn
            Caption = #1050#1086#1083'-'#1074#1086' '#1082' '#1079#1072#1082#1072#1079#1091' '#1092#1072#1082#1090#1080#1095#1077#1089#1082#1086#1077
            DataBinding.FieldName = 'BZQUANTF'
            Styles.Content = dmO.cxStyle28
            Width = 106
          end
          object ViewDoc4BZQUANTS: TcxGridDBColumn
            Caption = #1050#1086#1083'-'#1074#1086' '#1086#1090#1075#1088#1091#1078#1077#1085#1085#1086#1077
            DataBinding.FieldName = 'BZQUANTS'
            Styles.Content = dmO.cxStyle30
          end
          object ViewDoc4BZQUANTSHOP: TcxGridDBColumn
            Caption = #1054#1089#1090#1072#1090#1086#1082' '#1084#1072#1075#1072#1079#1080#1085#1072' '#1085#1072' '#1076#1077#1085#1100' '#1079#1072#1082#1072#1079#1072
            DataBinding.FieldName = 'BZQUANTSHOP'
            Options.Editing = False
          end
          object ViewDoc4QUANTDOC: TcxGridDBColumn
            Caption = #1050#1086#1083'-'#1074#1086' '#1087#1086' '#1076#1086#1082#1091#1084#1077#1085#1090#1091
            DataBinding.FieldName = 'QUANTDOC'
            Styles.Content = dmO.cxStyle12
          end
        end
        object LevelDoc4: TcxGridLevel
          GridView = ViewDoc4
        end
      end
    end
  end
  object FormPlacement1: TFormPlacement
    Active = False
    Left = 584
    Top = 200
  end
  object taSpec1: TClientDataSet
    Aggregates = <>
    FileName = 'SpecR.cds'
    Params = <>
    Left = 280
    Top = 204
    object taSpec1Num: TIntegerField
      FieldName = 'Num'
    end
    object taSpec1IdGoods: TIntegerField
      FieldName = 'IdGoods'
    end
    object taSpec1NameG: TStringField
      FieldName = 'NameG'
      Size = 200
    end
    object taSpec1IM: TIntegerField
      FieldName = 'IM'
    end
    object taSpec1SM: TStringField
      FieldName = 'SM'
      Size = 50
    end
    object taSpec1Quant: TFloatField
      FieldName = 'Quant'
      OnChange = taSpec1QuantChange
      DisplayFormat = '0.000'
      EditFormat = '0.000'
    end
    object taSpec1PriceIn: TCurrencyField
      FieldName = 'PriceIn'
      OnChange = taSpec1PriceInChange
      EditFormat = ',0.00;-,0.00'
    end
    object taSpec1SumIn: TCurrencyField
      FieldName = 'SumIn'
      OnChange = taSpec1SumInChange
      EditFormat = ',0.00;-,0.00'
    end
    object taSpec1PriceR: TCurrencyField
      FieldName = 'PriceR'
      OnChange = taSpec1PriceRChange
      EditFormat = ',0.00;-,0.00'
    end
    object taSpec1SumR: TCurrencyField
      FieldName = 'SumR'
      OnChange = taSpec1SumRChange
      EditFormat = ',0.00;-,0.00'
    end
    object taSpec1INds: TIntegerField
      FieldName = 'INds'
    end
    object taSpec1SNds: TStringField
      FieldName = 'SNds'
      Size = 30
    end
    object taSpec1RNds: TCurrencyField
      FieldName = 'RNds'
      EditFormat = ',0.00'#1088#39'.'#39';-,0.00'#1088#39'.'#39
    end
    object taSpec1SumNac: TCurrencyField
      FieldName = 'SumNac'
      EditFormat = ',0.00;-,0.00'
    end
    object taSpec1ProcNac: TFloatField
      FieldName = 'ProcNac'
      DisplayFormat = ',0.0%;-,0.0%'
    end
    object taSpec1Km: TFloatField
      FieldName = 'Km'
    end
    object taSpec1TCard: TIntegerField
      FieldName = 'TCard'
    end
    object taSpec1Oper: TSmallintField
      FieldName = 'Oper'
    end
    object taSpec1CTO: TStringField
      FieldName = 'CTO'
      Size = 150
    end
  end
  object dsSpec: TDataSource
    DataSet = taSpec
    Left = 364
    Top = 268
  end
  object amDocR: TActionManager
    Left = 192
    Top = 200
    StyleName = 'XP Style'
    object acAddPos: TAction
      Caption = 'acAddPos'
      ShortCut = 45
      OnExecute = acAddPosExecute
    end
    object acDelPos: TAction
      Caption = 'acDelPos'
      ShortCut = 119
      OnExecute = acDelPosExecute
    end
    object acAddList: TAction
      Caption = 'acAddList'
      ShortCut = 16429
      OnExecute = acAddListExecute
    end
    object acDelAll: TAction
      Caption = 'acDelAll'
      ShortCut = 16503
      OnExecute = acDelAllExecute
    end
    object acSaveDoc: TAction
      Caption = 'acSaveDoc'
      ShortCut = 16467
      OnExecute = acSaveDocExecute
    end
    object acExitDoc: TAction
      Caption = 'acExitDoc'
      ShortCut = 121
      OnExecute = acExitDocExecute
    end
    object acMovePos: TAction
      Caption = #1044#1074#1080#1078#1077#1085#1080#1077' '#1090#1086#1074#1072#1088#1072
      ShortCut = 32885
      OnExecute = acMovePosExecute
    end
    object acAddTara: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1090#1072#1088#1091
      OnExecute = acAddTaraExecute
    end
    object acDelTara: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1090#1072#1088#1091
      OnExecute = acDelTaraExecute
    end
    object acSetOper: TAction
      Caption = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1086#1087#1077#1088#1072#1094#1080#1102' '#1085#1072' '#1074#1099#1076#1077#1083#1077#1085#1085#1099#1077' '#1087#1086#1079#1080#1094#1080#1080
      OnExecute = acSetOperExecute
    end
    object acSaveQuantDoc: TAction
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1082#1086#1083'-'#1074#1086' '#1087#1086' '#1076#1086#1082#1091#1084#1077#1085#1090#1091
      ShortCut = 24659
      OnExecute = acSaveQuantDocExecute
    end
    object acSetPriceFromPrice: TAction
      Caption = #1047#1072#1087#1086#1083#1085#1080#1090#1100' '#1094#1077#1085#1099' '#1087#1086' '#1087#1088#1072#1081#1089#1091
      OnExecute = acSetPriceFromPriceExecute
    end
  end
  object PopupMenu1: TPopupMenu
    Images = dmO.imState
    Left = 584
    Top = 256
    object N1: TMenuItem
      Caption = #1050#1086#1087#1080#1088#1086#1074#1072#1090#1100' '#1087#1086#1079#1080#1094#1080#1102
      OnClick = N1Click
    end
    object N3: TMenuItem
      Action = acMovePos
      Hint = #1044#1074#1080#1078#1077#1085#1080#1077' '#1090#1086#1074#1072#1088#1072
    end
    object N4: TMenuItem
      Action = acAddTara
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object N6: TMenuItem
      Action = acSetOper
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Excel1: TMenuItem
      Caption = #1069#1082#1089#1087#1086#1088#1090' '#1074' Excel'
      ImageIndex = 47
      OnClick = Excel1Click
    end
  end
  object CasherRnDb1: TpFIBDatabase
    DBName = 'localhost:D:\_CasherRn\DB\CASHERRN.FDB'
    DBParams.Strings = (
      'user_name=SYSDBA'
      'lc_ctype=WIN1251'
      'password=masterkey'
      'sql_role_name=SYSDBA')
    DefaultTransaction = trSelCash
    DefaultUpdateTransaction = trSelCash
    SQLDialect = 3
    Timeout = 0
    SynchronizeTime = False
    DesignDBOptions = []
    AliasName = 'CasherRnDb'
    WaitForRestoreConnect = 20
    Left = 208
    Top = 312
  end
  object trSelCash: TpFIBTransaction
    DefaultDatabase = CasherRnDb1
    TimeoutAction = TARollback
    Left = 204
    Top = 371
  end
  object quFindPrice: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT PRICE,'
      '       CODE,'
      '       CONSUMMA,'
      '       IACTIVE'
      'FROM MENU'
      'where CODE=:IDCARD and CONSUMMA=1'
      'order by IACTIVE')
    Transaction = trSelCash
    Database = CasherRnDb1
    Left = 292
    Top = 323
    poAskRecordCount = True
    object quFindPricePRICE: TFIBFloatField
      FieldName = 'PRICE'
    end
    object quFindPriceCODE: TFIBStringField
      FieldName = 'CODE'
      Size = 10
      EmptyStrToNull = True
    end
    object quFindPriceCONSUMMA: TFIBFloatField
      FieldName = 'CONSUMMA'
    end
    object quFindPriceIACTIVE: TFIBSmallIntField
      FieldName = 'IACTIVE'
    end
  end
  object taSpec: TdxMemData
    Indexes = <>
    SortOptions = []
    BeforePost = taSpecBeforePost
    Left = 364
    Top = 215
    object taSpecNum: TIntegerField
      FieldName = 'Num'
    end
    object taSpecIdGoods: TIntegerField
      FieldName = 'IdGoods'
    end
    object taSpecNameG: TStringField
      FieldName = 'NameG'
      Size = 200
    end
    object taSpecIM: TIntegerField
      FieldName = 'IM'
    end
    object taSpecSM: TStringField
      FieldName = 'SM'
      Size = 50
    end
    object taSpecQuant: TFloatField
      FieldName = 'Quant'
      OnChange = taSpecQuantChange
      DisplayFormat = '0.000'
    end
    object taSpecPriceIn: TFloatField
      FieldName = 'PriceIn'
      OnChange = taSpecPriceInChange
      DisplayFormat = '0.00'
    end
    object taSpecSumIn: TFloatField
      FieldName = 'SumIn'
      OnChange = taSpecSumInChange
      DisplayFormat = '0.00'
    end
    object taSpecPriceR: TFloatField
      FieldName = 'PriceR'
      OnChange = taSpecPriceRChange
      DisplayFormat = '0.00'
    end
    object taSpecSumR: TFloatField
      FieldName = 'SumR'
      OnChange = taSpecSumRChange
      DisplayFormat = '0.00'
    end
    object taSpecINds: TIntegerField
      FieldName = 'INds'
      OnChange = taSpecINdsChange
    end
    object taSpecSNds: TStringField
      FieldName = 'SNds'
      Size = 30
    end
    object taSpecRNds: TFloatField
      FieldName = 'RNds'
      DisplayFormat = '0.00'
    end
    object taSpecSumNac: TFloatField
      FieldName = 'SumNac'
      DisplayFormat = '0.00'
    end
    object taSpecProcNac: TFloatField
      FieldName = 'ProcNac'
      DisplayFormat = '0.0'
    end
    object taSpecKm: TFloatField
      FieldName = 'Km'
    end
    object taSpecTCard: TSmallintField
      FieldName = 'TCard'
    end
    object taSpecOper: TSmallintField
      FieldName = 'Oper'
    end
    object taSpecCTO: TStringField
      FieldName = 'CTO'
      Size = 150
    end
    object taSpecCType: TIntegerField
      FieldName = 'CType'
    end
    object taSpecPriceIn0: TFloatField
      FieldName = 'PriceIn0'
      OnChange = taSpecPriceIn0Change
      DisplayFormat = '0.00'
    end
    object taSpecSumIn0: TFloatField
      FieldName = 'SumIn0'
      OnChange = taSpecSumIn0Change
      DisplayFormat = '0.00'
    end
    object taSpecSumOut0: TFloatField
      FieldName = 'SumOut0'
      OnChange = taSpecSumOut0Change
      DisplayFormat = '0.00'
    end
    object taSpecRNdsOut: TFloatField
      FieldName = 'RNdsOut'
      OnChange = taSpecRNdsOutChange
      DisplayFormat = '0.00'
    end
    object taSpecBZQUANTR: TFloatField
      FieldName = 'BZQUANTR'
      DisplayFormat = '0.000'
    end
    object taSpecBZQUANTF: TFloatField
      FieldName = 'BZQUANTF'
      OnChange = taSpecBZQUANTFChange
      DisplayFormat = '0.000'
    end
    object taSpecBZQUANTS: TFloatField
      FieldName = 'BZQUANTS'
      OnChange = taSpecBZQUANTSChange
      DisplayFormat = '0.000'
    end
    object taSpecBZQUANTSHOP: TFloatField
      FieldName = 'BZQUANTSHOP'
      DisplayFormat = '0.000'
    end
    object taSpecQUANTDOC: TFloatField
      FieldName = 'QUANTDOC'
      DisplayFormat = '0.000'
    end
  end
end
