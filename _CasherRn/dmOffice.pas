unit dmOffice;

interface

uses
  SysUtils, Classes, frexpimg, frRtfExp, frTXTExp, frXMLExl, frOLEExl,
  FR_E_HTML2, FR_E_HTM, FR_E_CSV, FR_E_RTF, FR_Class, FR_E_TXT, FIBQuery,
  pFIBQuery, pFIBStoredProc, DB, FIBDataSet, ImgList, Controls, cxStyles,
  pFIBDataSet, FIBDatabase, pFIBDatabase,DBClient,cxMemo,Variants,dxmdaset,
  Dialogs;

type
  TdmO = class(TDataModule)
    OfficeRnDb: TpFIBDatabase;
    trSelect: TpFIBTransaction;
    trUpdate: TpFIBTransaction;
    trDel: TpFIBTransaction;
    quPer: TpFIBDataSet;
    quPerID: TFIBIntegerField;
    quPerID_PARENT: TFIBIntegerField;
    quPerNAME: TFIBStringField;
    quPerUVOLNEN: TFIBBooleanField;
    quPerCheck: TFIBStringField;
    quPerMODUL1: TFIBBooleanField;
    quPerMODUL2: TFIBBooleanField;
    quPerMODUL3: TFIBBooleanField;
    quPerMODUL4: TFIBBooleanField;
    quPerMODUL5: TFIBBooleanField;
    quPerMODUL6: TFIBBooleanField;
    dsPer: TDataSource;
    cxRepos1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    cxStyle14: TcxStyle;
    cxStyle15: TcxStyle;
    cxStyle16: TcxStyle;
    cxStyle17: TcxStyle;
    cxStyle18: TcxStyle;
    cxStyle19: TcxStyle;
    cxStyle20: TcxStyle;
    cxStyle21: TcxStyle;
    cxStyle22: TcxStyle;
    cxStyle23: TcxStyle;
    cxStyle24: TcxStyle;
    cxStyle25: TcxStyle;
    imState: TImageList;
    quCanDo: TpFIBDataSet;
    quCanDoID_PERSONAL: TFIBIntegerField;
    quCanDoNAME: TFIBStringField;
    quCanDoPREXEC: TFIBBooleanField;
    trCanDo: TpFIBTransaction;
    prGetId: TpFIBStoredProc;
    frTextExport1: TfrTextExport;
    frRTFExport1: TfrRTFExport;
    frCSVExport1: TfrCSVExport;
    frHTMExport1: TfrHTMExport;
    frHTML2Export1: TfrHTML2Export;
    frOLEExcelExport1: TfrOLEExcelExport;
    frXMLExcelExport1: TfrXMLExcelExport;
    frTextAdvExport1: TfrTextAdvExport;
    frRtfAdvExport1: TfrRtfAdvExport;
    frJPEGExport1: TfrJPEGExport;
    quMessTree: TpFIBDataSet;
    taMess: TpFIBDataSet;
    taMessID: TFIBIntegerField;
    taMessID_PARENT: TFIBIntegerField;
    taMessNAMESHORT: TFIBStringField;
    taMessNAMEFULL: TFIBStringField;
    taMessKOEF: TFIBFloatField;
    prCanDelMess: TpFIBStoredProc;
    quClassTree: TpFIBDataSet;
    taClass: TpFIBDataSet;
    taClassID: TFIBIntegerField;
    taClassID_PARENT: TFIBIntegerField;
    taClassITYPE: TFIBSmallIntField;
    taClassNAMECL: TFIBStringField;
    prCanDelClass: TpFIBStoredProc;
    taPriceT: TpFIBDataSet;
    taPriceTID: TFIBIntegerField;
    taPriceTNAMEPRICE: TFIBStringField;
    dsPriceT: TDataSource;
    quMHTree: TpFIBDataSet;
    taMH: TpFIBDataSet;
    taMHID: TFIBIntegerField;
    taMHPARENT: TFIBIntegerField;
    taMHITYPE: TFIBIntegerField;
    taMHNAMEMH: TFIBStringField;
    taMHDEFPRICE: TFIBIntegerField;
    quMHSel: TpFIBDataSet;
    quMHSelID: TFIBIntegerField;
    quMHSelPARENT: TFIBIntegerField;
    quMHSelITYPE: TFIBIntegerField;
    quMHSelNAMEMH: TFIBStringField;
    quMHSelDEFPRICE: TFIBIntegerField;
    dsMHSel: TDataSource;
    quMHSelNAMEPRICE: TFIBStringField;
    quMHParent: TpFIBDataSet;
    quPriceTSel: TpFIBDataSet;
    quPriceTSelID: TFIBIntegerField;
    quPriceTSelNAMEPRICE: TFIBStringField;
    dsPriceTSel: TDataSource;
    quCardsSel: TpFIBDataSet;
    dsCardsSel: TDataSource;
    quCardsSelID: TFIBIntegerField;
    quCardsSelPARENT: TFIBIntegerField;
    quCardsSelNAME: TFIBStringField;
    quCardsSelTTYPE: TFIBIntegerField;
    quCardsSelIMESSURE: TFIBIntegerField;
    quCardsSelINDS: TFIBIntegerField;
    quCardsSelMINREST: TFIBFloatField;
    quCardsSelLASTPRICEIN: TFIBFloatField;
    quCardsSelLASTPRICEOUT: TFIBFloatField;
    quCardsSelLASTPOST: TFIBIntegerField;
    quCardsSelIACTIVE: TFIBIntegerField;
    quCardsSelNAMESHORT: TFIBStringField;
    quCardsSelNAMENDS: TFIBStringField;
    quCardsSelPROC: TFIBFloatField;
    taNDS: TpFIBDataSet;
    taNDSID: TFIBIntegerField;
    taNDSNAMENDS: TFIBStringField;
    taNDSPROC: TFIBFloatField;
    dsMess: TDataSource;
    dsNDS: TDataSource;
    quGdsFind: TpFIBDataSet;
    quBarFind: TpFIBDataSet;
    quBarFindBAR: TFIBStringField;
    quBarFindGOODSID: TFIBIntegerField;
    quBars: TpFIBDataSet;
    quBarsBAR: TFIBStringField;
    quBarsGOODSID: TFIBIntegerField;
    quBarsQUANT: TFIBFloatField;
    quBarsBARFORMAT: TFIBSmallIntField;
    quBarsPRICE: TFIBFloatField;
    trUpdCards: TpFIBTransaction;
    trUpdBars: TpFIBTransaction;
    quGoodsEU: TpFIBDataSet;
    quGoodsEUGOODSID: TFIBIntegerField;
    quGoodsEUIDATEB: TFIBIntegerField;
    quGoodsEUIDATEE: TFIBIntegerField;
    quGoodsEUTO100GRAMM: TFIBFloatField;
    dsGoodsEU: TDataSource;
    trUpdEU: TpFIBTransaction;
    quFind: TpFIBDataSet;
    dsFind: TDataSource;
    quFindID: TFIBIntegerField;
    quFindPARENT: TFIBIntegerField;
    quFindNAME: TFIBStringField;
    taClients: TpFIBDataSet;
    taClientsID: TFIBIntegerField;
    taClientsNAMECL: TFIBStringField;
    taClientsFULLNAMECL: TFIBStringField;
    taClientsINN: TFIBStringField;
    taClientsPHONE: TFIBStringField;
    taClientsMOL: TFIBStringField;
    taClientsCOMMENT: TFIBStringField;
    taClientsINDS: TFIBIntegerField;
    taClientsIACTIVE: TFIBSmallIntField;
    dsClients: TDataSource;
    cxStyle26: TcxStyle;
    quDocsInSel: TpFIBDataSet;
    trDocsSel: TpFIBTransaction;
    trDocsUpd: TpFIBTransaction;
    dsDocsInSel: TDataSource;
    quDocsInSelID: TFIBIntegerField;
    quDocsInSelDATEDOC: TFIBDateField;
    quDocsInSelNUMDOC: TFIBStringField;
    quDocsInSelDATESF: TFIBDateField;
    quDocsInSelNUMSF: TFIBStringField;
    quDocsInSelIDCLI: TFIBIntegerField;
    quDocsInSelIDSKL: TFIBIntegerField;
    quDocsInSelSUMIN: TFIBFloatField;
    quDocsInSelSUMUCH: TFIBFloatField;
    quDocsInSelSUMTAR: TFIBFloatField;
    quDocsInSelSUMNDS0: TFIBFloatField;
    quDocsInSelSUMNDS1: TFIBFloatField;
    quDocsInSelSUMNDS2: TFIBFloatField;
    quDocsInSelPROCNAC: TFIBFloatField;
    quDocsInSelNAMECL: TFIBStringField;
    quDocsInSelNAMEMH: TFIBStringField;
    quDocsInSelIACTIVE: TFIBIntegerField;
    quSpecInSel: TpFIBDataSet;
    quSpecInSelIDHEAD: TFIBIntegerField;
    quSpecInSelID: TFIBIntegerField;
    quSpecInSelNUM: TFIBIntegerField;
    quSpecInSelIDCARD: TFIBIntegerField;
    quSpecInSelQUANT: TFIBFloatField;
    quSpecInSelPRICEIN: TFIBFloatField;
    quSpecInSelSUMIN: TFIBFloatField;
    quSpecInSelPRICEUCH: TFIBFloatField;
    quSpecInSelSUMUCH: TFIBFloatField;
    quSpecInSelSUMNDS: TFIBFloatField;
    quCardsSelTCARD: TFIBIntegerField;
    quTCards: TpFIBDataSet;
    quTCardsIDCARD: TFIBIntegerField;
    quTCardsID: TFIBIntegerField;
    quTCardsDATEB: TFIBDateField;
    quTCardsDATEE: TFIBDateField;
    quTCardsSHORTNAME: TFIBStringField;
    quTCardsRECEIPTNUM: TFIBStringField;
    quTCardsPOUTPUT: TFIBStringField;
    quTCardsPCOUNT: TFIBIntegerField;
    quTCardsPVES: TFIBFloatField;
    dsTCards: TDataSource;
    quTSpec: TpFIBDataSet;
    dsTSpec: TDataSource;
    quFindEU: TpFIBDataSet;
    quFindEUGOODSID: TFIBIntegerField;
    quFindEUIDATEB: TFIBIntegerField;
    quFindEUIDATEE: TFIBIntegerField;
    quFindEUTO100GRAMM: TFIBFloatField;
    trSElMessure: TpFIBTransaction;
    quMessureSel: TpFIBDataSet;
    dsMessureSel: TDataSource;
    quMessureSelID: TFIBIntegerField;
    quMessureSelID_PARENT: TFIBIntegerField;
    quMessureSelNAMESHORT: TFIBStringField;
    quMessureSelKOEF: TFIBFloatField;
    taClientsKPP: TFIBStringField;
    taClientsADDRES: TFIBStringField;
    quCardsSel1: TpFIBDataSet;
    dsCardsSel1: TDataSource;
    quCardsSel1ID: TFIBIntegerField;
    quCardsSel1PARENT: TFIBIntegerField;
    quCardsSel1NAME: TFIBStringField;
    quCardsSel1TTYPE: TFIBIntegerField;
    quCardsSel1IMESSURE: TFIBIntegerField;
    quCardsSel1INDS: TFIBIntegerField;
    quCardsSel1MINREST: TFIBFloatField;
    quCardsSel1LASTPRICEIN: TFIBFloatField;
    quCardsSel1LASTPRICEOUT: TFIBFloatField;
    quCardsSel1LASTPOST: TFIBIntegerField;
    quCardsSel1IACTIVE: TFIBIntegerField;
    quCardsSel1TCARD: TFIBIntegerField;
    quCardsSel1NAMESHORT: TFIBStringField;
    quCardsSel1NAMENDS: TFIBStringField;
    quCardsSel1PROC: TFIBFloatField;
    quS: TpFIBDataSet;
    quSTCARD: TFIBIntegerField;
    quST: TpFIBDataSet;
    FIBIntegerField1: TFIBIntegerField;
    trSel: TpFIBTransaction;
    quTCardsSDATEE: TStringField;
    quSpecInSelNAMEC: TFIBStringField;
    quSpecInSelSM: TFIBStringField;
    quSpecInSelIDM: TFIBIntegerField;
    quSpecInSelIM: TFIBIntegerField;
    prDelPart: TpFIBStoredProc;
    prAddPartIn: TpFIBStoredProc;
    prFindPartOut: TpFIBStoredProc;
    trSel1: TpFIBTransaction;
    prPartInDel: TpFIBStoredProc;
    taParams: TpFIBDataSet;
    taParamsID: TFIBIntegerField;
    taParamsIDATEB: TFIBIntegerField;
    taParamsIDATEE: TFIBIntegerField;
    taParamsIDSTORE: TFIBIntegerField;
    quMoveSel: TpFIBDataSet;
    quMoveSelID: TFIBIntegerField;
    quMoveSelPARENT: TFIBIntegerField;
    quMoveSelNAME: TFIBStringField;
    quMoveSelTTYPE: TFIBIntegerField;
    quMoveSelIMESSURE: TFIBIntegerField;
    quMoveSelINDS: TFIBIntegerField;
    quMoveSelMINREST: TFIBFloatField;
    quMoveSelIACTIVE: TFIBIntegerField;
    quMoveSelTCARD: TFIBIntegerField;
    quMoveSelNAMECL: TFIBStringField;
    quMoveSelNAMEMESSURE: TFIBStringField;
    quMoveSelKOEF: TFIBFloatField;
    quMoveSelPOSTIN: TFIBFloatField;
    quMoveSelPOSTOUT: TFIBFloatField;
    quMoveSelVNIN: TFIBFloatField;
    quMoveSelVNOUT: TFIBFloatField;
    quMoveSelINV: TFIBFloatField;
    quMoveSelQREAL: TFIBFloatField;
    quMoveSelItog: TFloatField;
    dsMoveSel: TDataSource;
    quDocsOutSel: TpFIBDataSet;
    quDocsOutSelID: TFIBIntegerField;
    quDocsOutSelDATEDOC: TFIBDateField;
    quDocsOutSelNUMDOC: TFIBStringField;
    quDocsOutSelDATESF: TFIBDateField;
    quDocsOutSelNUMSF: TFIBStringField;
    quDocsOutSelIDCLI: TFIBIntegerField;
    quDocsOutSelIDSKL: TFIBIntegerField;
    quDocsOutSelSUMIN: TFIBFloatField;
    quDocsOutSelSUMUCH: TFIBFloatField;
    quDocsOutSelSUMTAR: TFIBFloatField;
    quDocsOutSelSUMNDS0: TFIBFloatField;
    quDocsOutSelSUMNDS1: TFIBFloatField;
    quDocsOutSelSUMNDS2: TFIBFloatField;
    quDocsOutSelPROCNAC: TFIBFloatField;
    quDocsOutSelIACTIVE: TFIBIntegerField;
    quDocsOutSelNAMECL: TFIBStringField;
    quDocsOutSelNAMEMH: TFIBStringField;
    dsDocsOutSel: TDataSource;
    quSpecOutSel: TpFIBDataSet;
    quSpecOutSelIDHEAD: TFIBIntegerField;
    quSpecOutSelID: TFIBIntegerField;
    quSpecOutSelNUM: TFIBIntegerField;
    quSpecOutSelIDCARD: TFIBIntegerField;
    quSpecOutSelQUANT: TFIBFloatField;
    quSpecOutSelPRICEIN: TFIBFloatField;
    quSpecOutSelSUMIN: TFIBFloatField;
    quSpecOutSelPRICEUCH: TFIBFloatField;
    quSpecOutSelSUMUCH: TFIBFloatField;
    quSpecOutSelIDNDS: TFIBIntegerField;
    quSpecOutSelSUMNDS: TFIBFloatField;
    quSpecOutSelIDM: TFIBIntegerField;
    quSpecOutSelNAMEC: TFIBStringField;
    quSpecOutSelIM: TFIBIntegerField;
    quSpecOutSelSM: TFIBStringField;
    quSpecOutSelNAMENDS: TFIBStringField;
    prCalcLastPrice: TpFIBStoredProc;
    quM: TpFIBDataSet;
    quMID: TFIBIntegerField;
    quMID_PARENT: TFIBIntegerField;
    quMNAMESHORT: TFIBStringField;
    quMKOEF: TFIBFloatField;
    quSelPartIn: TpFIBDataSet;
    quSelPartInID: TFIBIntegerField;
    quSelPartInIDSTORE: TFIBIntegerField;
    quSelPartInIDDOC: TFIBIntegerField;
    quSelPartInARTICUL: TFIBIntegerField;
    quSelPartInIDCLI: TFIBIntegerField;
    quSelPartInDTYPE: TFIBIntegerField;
    quSelPartInQPART: TFIBFloatField;
    quSelPartInQREMN: TFIBFloatField;
    quSelPartInPRICEIN: TFIBFloatField;
    quSelPartInPRICEOUT: TFIBFloatField;
    quSelPartInIDATE: TFIBIntegerField;
    dsSelPartIn: TDataSource;
    quSelPartInSDATE: TStringField;
    prAddPartOut: TpFIBStoredProc;
    prDelPartOut: TpFIBStoredProc;
    prCalcLastPrice1: TpFIBStoredProc;
    quFindCard: TpFIBDataSet;
    quFindCardID: TFIBIntegerField;
    quFindCardPARENT: TFIBIntegerField;
    quFindCardNAME: TFIBStringField;
    quFindCardTTYPE: TFIBIntegerField;
    quFindCardIMESSURE: TFIBIntegerField;
    quFindCardINDS: TFIBIntegerField;
    quFindCardMINREST: TFIBFloatField;
    quFindCardLASTPRICEIN: TFIBFloatField;
    quFindCardLASTPRICEOUT: TFIBFloatField;
    quFindCardLASTPOST: TFIBIntegerField;
    quFindCardIACTIVE: TFIBIntegerField;
    quFindCardTCARD: TFIBIntegerField;
    quDocsOutB: TpFIBDataSet;
    quDocsOutBID: TFIBIntegerField;
    quDocsOutBDATEDOC: TFIBDateField;
    quDocsOutBNUMDOC: TFIBStringField;
    quDocsOutBDATESF: TFIBDateField;
    quDocsOutBNUMSF: TFIBStringField;
    quDocsOutBIDCLI: TFIBIntegerField;
    quDocsOutBIDSKL: TFIBIntegerField;
    quDocsOutBSUMIN: TFIBFloatField;
    quDocsOutBSUMUCH: TFIBFloatField;
    quDocsOutBSUMTAR: TFIBFloatField;
    quDocsOutBSUMNDS0: TFIBFloatField;
    quDocsOutBSUMNDS1: TFIBFloatField;
    quDocsOutBSUMNDS2: TFIBFloatField;
    quDocsOutBPROCNAC: TFIBFloatField;
    quDocsOutBIACTIVE: TFIBIntegerField;
    quDocsOutBNAMEMH: TFIBStringField;
    dsDocsOutB: TDataSource;
    quDOBHEAD: TpFIBDataSet;
    quDOBHEADID: TFIBIntegerField;
    quDOBHEADDATEDOC: TFIBDateField;
    quDOBHEADNUMDOC: TFIBStringField;
    quDOBHEADDATESF: TFIBDateField;
    quDOBHEADNUMSF: TFIBStringField;
    quDOBHEADIDCLI: TFIBIntegerField;
    quDOBHEADIDSKL: TFIBIntegerField;
    quDOBHEADSUMIN: TFIBFloatField;
    quDOBHEADSUMUCH: TFIBFloatField;
    quDOBHEADSUMTAR: TFIBFloatField;
    quDOBHEADSUMNDS0: TFIBFloatField;
    quDOBHEADSUMNDS1: TFIBFloatField;
    quDOBHEADSUMNDS2: TFIBFloatField;
    quDOBHEADPROCNAC: TFIBFloatField;
    quDOBHEADIACTIVE: TFIBIntegerField;
    quDOBHEADOPER: TFIBStringField;
    quDOBSPEC: TpFIBDataSet;
    quDOBSPECIDHEAD: TFIBIntegerField;
    quDOBSPECID: TFIBIntegerField;
    quDOBSPECIDCARD: TFIBIntegerField;
    quDOBSPECQUANT: TFIBFloatField;
    quDOBSPECIDM: TFIBIntegerField;
    quDOBSPECSIFR: TFIBIntegerField;
    quDOBSPECNAMEB: TFIBStringField;
    quDOBSPECCODEB: TFIBStringField;
    quDOBSPECKB: TFIBFloatField;
    quDOBSPECPRICER: TFIBFloatField;
    quDOBSPECDSUM: TFIBFloatField;
    quDOBSPECRSUM: TFIBFloatField;
    quDOBSPECKM: TFIBFloatField;
    quDocsOutBOPER: TFIBStringField;
    taDobSpec: TpFIBDataSet;
    taDobSpecIDHEAD: TFIBIntegerField;
    taDobSpecID: TFIBIntegerField;
    taDobSpecIDCARD: TFIBIntegerField;
    taDobSpecQUANT: TFIBFloatField;
    taDobSpecIDM: TFIBIntegerField;
    taDobSpecSIFR: TFIBIntegerField;
    taDobSpecNAMEB: TFIBStringField;
    taDobSpecCODEB: TFIBStringField;
    taDobSpecKB: TFIBFloatField;
    taDobSpecPRICER: TFIBFloatField;
    taDobSpecDSUM: TFIBFloatField;
    taDobSpecRSUM: TFIBFloatField;
    taDobSpecKM: TFIBFloatField;
    taDobSpecNAME: TFIBStringField;
    taDobSpecTCARD: TFIBIntegerField;
    taDobSpecNAMESHORT: TFIBStringField;
    dsDobSpec: TDataSource;
    quFindTCard: TpFIBDataSet;
    quFindTCardID: TFIBIntegerField;
    quFindTCardSHORTNAME: TFIBStringField;
    quFindTCardRECEIPTNUM: TFIBStringField;
    quFindTCardPOUTPUT: TFIBStringField;
    quFindTCardPCOUNT: TFIBIntegerField;
    quFindTCardPVES: TFIBFloatField;
    quRemn: TpFIBDataSet;
    quRemnREMN: TFIBFloatField;
    trSel2: TpFIBTransaction;
    taDobSpec1: TpFIBDataSet;
    taDobSpec1IDHEAD: TFIBIntegerField;
    taDobSpec1ID: TFIBIntegerField;
    taDobSpec1ARTICUL: TFIBIntegerField;
    taDobSpec1IDM: TFIBIntegerField;
    taDobSpec1SM: TFIBStringField;
    taDobSpec1KM: TFIBFloatField;
    taDobSpec1QUANT: TFIBFloatField;
    taDobSpec1NAME: TFIBStringField;
    taDobSpec1SUMIN: TFIBFloatField;
    quMHAll: TpFIBDataSet;
    quMHAllID: TFIBIntegerField;
    quMHAllPARENT: TFIBIntegerField;
    quMHAllITYPE: TFIBIntegerField;
    quMHAllNAMEMH: TFIBStringField;
    quMHAllDEFPRICE: TFIBIntegerField;
    quMHAllNAMEPRICE: TFIBStringField;
    dsMHAll: TDataSource;
    taMessITYPE: TFIBSmallIntField;
    quMITYPE: TFIBSmallIntField;
    taCateg: TpFIBDataSet;
    taCategID: TFIBIntegerField;
    taCategNAMECAT: TFIBStringField;
    dsCateg: TDataSource;
    quCardsSelCATEGORY: TFIBIntegerField;
    quCardsSel1CATEGORY: TFIBIntegerField;
    taDobSpec2: TpFIBDataSet;
    taDobSpec2IDHEAD: TFIBIntegerField;
    taDobSpec2IDB: TFIBIntegerField;
    taDobSpec2ID: TFIBIntegerField;
    taDobSpec2CODEB: TFIBIntegerField;
    taDobSpec2NAMEB: TFIBStringField;
    taDobSpec2QUANT: TFIBFloatField;
    taDobSpec2PRICEOUT: TFIBFloatField;
    taDobSpec2SUMOUT: TFIBFloatField;
    taDobSpec2IDCARD: TFIBIntegerField;
    taDobSpec2NAMEC: TFIBStringField;
    taDobSpec2QUANTC: TFIBFloatField;
    taDobSpec2PRICEIN: TFIBFloatField;
    taDobSpec2SUMIN: TFIBFloatField;
    taDobSpec2IM: TFIBIntegerField;
    taDobSpec2SM: TFIBStringField;
    taDobSpec2SB: TFIBStringField;
    quTO: TpFIBDataSet;
    dsTO: TDataSource;
    quTOIDATE: TFIBIntegerField;
    quTOIDSTORE: TFIBIntegerField;
    quTOREMNIN: TFIBFloatField;
    quTOREMNOUT: TFIBFloatField;
    quTOPOSTIN: TFIBFloatField;
    quTOPOSTOUT: TFIBFloatField;
    quTOVNIN: TFIBFloatField;
    quTOVNOUT: TFIBFloatField;
    quTOINV: TFIBFloatField;
    quTOQREAL: TFIBFloatField;
    quTOSel: TpFIBDataSet;
    quTOSelIDATE: TFIBIntegerField;
    quTOSelIDSTORE: TFIBIntegerField;
    quTOSelREMNIN: TFIBFloatField;
    quTOSelREMNOUT: TFIBFloatField;
    quTOSelPOSTIN: TFIBFloatField;
    quTOSelPOSTOUT: TFIBFloatField;
    quTOSelVNIN: TFIBFloatField;
    quTOSelVNOUT: TFIBFloatField;
    quTOSelINV: TFIBFloatField;
    quTOSelQREAL: TFIBFloatField;
    quTODel: TpFIBQuery;
    taTOPostIn: TpFIBDataSet;
    taTOPostInID: TFIBIntegerField;
    taTOPostInDATEDOC: TFIBDateField;
    taTOPostInNUMDOC: TFIBStringField;
    taTOPostInNAMECL: TFIBStringField;
    taTOPostInDATESF: TFIBDateField;
    taTOPostInNUMSF: TFIBStringField;
    taTOPostInIDCLI: TFIBIntegerField;
    taTOPostInSUMIN: TFIBFloatField;
    taTOPostInSUMUCH: TFIBFloatField;
    taTOPostInSUMTAR: TFIBFloatField;
    taTOPostOut: TpFIBDataSet;
    taTOPostOutID: TFIBIntegerField;
    taTOPostOutDATEDOC: TFIBDateField;
    taTOPostOutNUMDOC: TFIBStringField;
    taTOPostOutNAMECL: TFIBStringField;
    taTOPostOutDATESF: TFIBDateField;
    taTOPostOutNUMSF: TFIBStringField;
    taTOPostOutIDCLI: TFIBIntegerField;
    taTOPostOutSUMIN: TFIBFloatField;
    taTOPostOutSUMUCH: TFIBFloatField;
    taTOPostOutSUMTAR: TFIBFloatField;
    taTOOutB: TpFIBDataSet;
    taTOOutBID: TFIBIntegerField;
    taTOOutBDATEDOC: TFIBDateField;
    taTOOutBNUMDOC: TFIBStringField;
    taTOOutBDATESF: TFIBDateField;
    taTOOutBNUMSF: TFIBStringField;
    taTOOutBIDCLI: TFIBIntegerField;
    taTOOutBIDSKL: TFIBIntegerField;
    taTOOutBSUMIN: TFIBFloatField;
    taTOOutBSUMUCH: TFIBFloatField;
    taTOOutBSUMTAR: TFIBFloatField;
    taTOOutBOPER: TFIBStringField;
    quSelCardsCat: TpFIBDataSet;
    quSelCardsCatCATEGORY: TFIBIntegerField;
    quTONAMEMH: TFIBStringField;
    quSelPartIn1: TpFIBDataSet;
    quSelPartIn1ID: TFIBIntegerField;
    quSelPartIn1IDSTORE: TFIBIntegerField;
    quSelPartIn1IDDOC: TFIBIntegerField;
    quSelPartIn1ARTICUL: TFIBIntegerField;
    quSelPartIn1IDCLI: TFIBIntegerField;
    quSelPartIn1DTYPE: TFIBIntegerField;
    quSelPartIn1QPART: TFIBFloatField;
    quSelPartIn1QREMN: TFIBFloatField;
    quSelPartIn1PRICEIN: TFIBFloatField;
    quSelPartIn1PRICEOUT: TFIBFloatField;
    quSelPartIn1IDATE: TFIBIntegerField;
    prGdsMove: TpFIBStoredProc;
    taTOOutBCat: TpFIBDataSet;
    taTOOutBCatRSUM: TFIBFloatField;
    taTOOutCCat: TpFIBDataSet;
    taTOOutCCatRSUM: TFIBFloatField;
    quRemnDate: TpFIBDataSet;
    quCPartIn: TpFIBDataSet;
    quCPartOut: TpFIBDataSet;
    quCMove: TpFIBDataSet;
    dsquCPartIn: TDataSource;
    dsquCPartOut: TDataSource;
    quCPartInID: TFIBIntegerField;
    quCPartInIDSTORE: TFIBIntegerField;
    quCPartInNAMEMH: TFIBStringField;
    quCPartInIDDOC: TFIBIntegerField;
    quCPartInARTICUL: TFIBIntegerField;
    quCPartInIDCLI: TFIBIntegerField;
    quCPartInNAMECL: TFIBStringField;
    quCPartInDTYPE: TFIBIntegerField;
    quCPartInQPART: TFIBFloatField;
    quCPartInQREMN: TFIBFloatField;
    quCPartInPRICEIN: TFIBFloatField;
    quCPartInPRICEOUT: TFIBFloatField;
    quCPartInIDATE: TFIBIntegerField;
    quCPartOutARTICUL: TFIBIntegerField;
    quCPartOutIDDATE: TFIBIntegerField;
    quCPartOutIDSTORE: TFIBIntegerField;
    quCPartOutNAMEMH: TFIBStringField;
    quCPartOutIDPARTIN: TFIBIntegerField;
    quCPartOutIDDOC: TFIBIntegerField;
    quCPartOutIDCLI: TFIBIntegerField;
    quCPartOutNAMECL: TFIBStringField;
    quCPartOutDTYPE: TFIBIntegerField;
    quCPartOutQUANT: TFIBFloatField;
    quCPartOutPRICEIN: TFIBFloatField;
    quCPartOutSUMOUT: TFIBFloatField;
    quCPartInSUMIN: TFIBFloatField;
    quCPartInSUMREMN: TFIBFloatField;
    quCPartOutSUMIN: TFIBFloatField;
    quCMoveARTICUL: TFIBIntegerField;
    quCMoveIDATE: TFIBIntegerField;
    quCMoveIDSTORE: TFIBIntegerField;
    quCMoveNAMEMH: TFIBStringField;
    quCMovePOSTIN: TFIBFloatField;
    quCMovePOSTOUT: TFIBFloatField;
    quCMoveVNIN: TFIBFloatField;
    quCMoveVNOUT: TFIBFloatField;
    quCMoveINV: TFIBFloatField;
    quCMoveQREAL: TFIBFloatField;
    quCMoveRB: TFloatField;
    quCMoveRE: TFloatField;
    quCRemn: TpFIBDataSet;
    quCRemnREMN: TFIBFloatField;
    quFindUse: TpFIBDataSet;
    quFindUseMAXDATE: TFIBDateField;
    quFCard: TpFIBDataSet;
    quFCardID: TFIBIntegerField;
    quFCardIMESSURE: TFIBIntegerField;
    quFCardNAME: TFIBStringField;
    dsFCard: TDataSource;
    quFCardPARENT: TFIBIntegerField;
    quFCardTCARD: TFIBIntegerField;
    quFindGrName: TpFIBDataSet;
    quFindGrNameID: TFIBIntegerField;
    quFindGrNameID_PARENT: TFIBIntegerField;
    quFindGrNameITYPE: TFIBSmallIntField;
    quFindGrNameNAMECL: TFIBStringField;
    quAllCards: TpFIBDataSet;
    quAllCardsID: TFIBIntegerField;
    quAllCardsNAME: TFIBStringField;
    quAllCardsIMESSURE: TFIBIntegerField;
    quAllCardsTCARD: TFIBIntegerField;
    quAllCardsPARENT: TFIBIntegerField;
    quAllCardsNAMESHORT: TFIBStringField;
    quAllCardsKOEF: TFIBFloatField;
    quCalcRemnSum: TpFIBDataSet;
    quCalcRemnSumID: TFIBIntegerField;
    quCalcRemnSumIDSTORE: TFIBIntegerField;
    quCalcRemnSumIDDOC: TFIBIntegerField;
    quCalcRemnSumARTICUL: TFIBIntegerField;
    quCalcRemnSumIDCLI: TFIBIntegerField;
    quCalcRemnSumDTYPE: TFIBIntegerField;
    quCalcRemnSumQPART: TFIBFloatField;
    quCalcRemnSumQREMN: TFIBFloatField;
    quCalcRemnSumPRICEIN: TFIBFloatField;
    quCalcRemnSumPRICEOUT: TFIBFloatField;
    quCalcRemnSumIDATE: TFIBIntegerField;
    quDocsInId: TpFIBDataSet;
    quDocsInIdID: TFIBIntegerField;
    quDocsInIdDATEDOC: TFIBDateField;
    quDocsInIdNUMDOC: TFIBStringField;
    quDocsInIdDATESF: TFIBDateField;
    quDocsInIdNUMSF: TFIBStringField;
    quDocsInIdIDCLI: TFIBIntegerField;
    quDocsInIdIDSKL: TFIBIntegerField;
    quDocsInIdSUMIN: TFIBFloatField;
    quDocsInIdSUMUCH: TFIBFloatField;
    quDocsInIdSUMTAR: TFIBFloatField;
    quDocsInIdSUMNDS0: TFIBFloatField;
    quDocsInIdSUMNDS1: TFIBFloatField;
    quDocsInIdSUMNDS2: TFIBFloatField;
    quDocsInIdPROCNAC: TFIBFloatField;
    quDocsInIdIACTIVE: TFIBIntegerField;
    quFCardINDS: TFIBIntegerField;
    quDocsInCard: TpFIBDataSet;
    quDocsInCardNAME: TFIBStringField;
    quDocsInCardNAMESHORT: TFIBStringField;
    quDocsInCardIDCARD: TFIBIntegerField;
    quDocsInCardQUANT: TFIBFloatField;
    quDocsInCardPRICEIN: TFIBFloatField;
    quDocsInCardSUMIN: TFIBFloatField;
    quDocsInCardPRICEUCH: TFIBFloatField;
    quDocsInCardSUMUCH: TFIBFloatField;
    quDocsInCardIDNDS: TFIBIntegerField;
    quDocsInCardSUMNDS: TFIBFloatField;
    quDocsInCardIDM: TFIBIntegerField;
    quDocsInCardDATEDOC: TFIBDateField;
    quDocsInCardNUMDOC: TFIBStringField;
    quDocsInCardNAMECL: TFIBStringField;
    quDocsInCardNAMEMH: TFIBStringField;
    dsDocsInCard: TDataSource;
    prDelPartInv: TpFIBStoredProc;
    prAddPartIn1: TpFIBStoredProc;
    taTOInv: TpFIBDataSet;
    taTOInvNUMDOC: TFIBStringField;
    taTOInvSUM1: TFIBFloatField;
    taTOInvSUM11: TFIBFloatField;
    taTOInvSUM2: TFIBFloatField;
    taTOInvSUM21: TFIBFloatField;
    quTCardsTEHNO: TFIBStringField;
    quTCardsOFORM: TFIBStringField;
    quFindTCARD2: TFIBIntegerField;
    taTOVnIn: TpFIBDataSet;
    taTOVnInID: TFIBIntegerField;
    taTOVnInDATEDOC: TFIBDateField;
    taTOVnInNUMDOC: TFIBStringField;
    taTOVnInIDSKL_FROM: TFIBIntegerField;
    taTOVnInNAMEMH: TFIBStringField;
    taTOVnInIDSKL_TO: TFIBIntegerField;
    taTOVnInSUMIN: TFIBFloatField;
    taTOVnInSUMUCH: TFIBFloatField;
    taTOVnInSUMUCH1: TFIBFloatField;
    taTOVnOut: TpFIBDataSet;
    taTOVnOutID: TFIBIntegerField;
    taTOVnOutDATEDOC: TFIBDateField;
    taTOVnOutNUMDOC: TFIBStringField;
    taTOVnOutIDSKL_FROM: TFIBIntegerField;
    taTOVnOutNAMEMH: TFIBStringField;
    taTOVnOutIDSKL_TO: TFIBIntegerField;
    taTOVnOutSUMIN: TFIBFloatField;
    taTOVnOutSUMUCH: TFIBFloatField;
    taTOVnOutSUMUCH1: TFIBFloatField;
    taTOAct: TpFIBDataSet;
    taTOActNUMDOC: TFIBStringField;
    taTOActSUMIN: TFIBFloatField;
    taTOActSUMUCH: TFIBFloatField;
    taTOActOPER: TFIBStringField;
    taTOComplOut: TpFIBDataSet;
    taTOComplOutNUMDOC: TFIBStringField;
    taTOComplOutSUMIN: TFIBFloatField;
    taTOComplOutSUMUCH: TFIBFloatField;
    taTOComplOutOPER: TFIBStringField;
    quDocsOutId: TpFIBDataSet;
    quDocsOutIdID: TFIBIntegerField;
    quDocsOutIdDATEDOC: TFIBDateField;
    quDocsOutIdNUMDOC: TFIBStringField;
    quDocsOutIdDATESF: TFIBDateField;
    quDocsOutIdNUMSF: TFIBStringField;
    quDocsOutIdIDCLI: TFIBIntegerField;
    quDocsOutIdIDSKL: TFIBIntegerField;
    quDocsOutIdSUMIN: TFIBFloatField;
    quDocsOutIdSUMUCH: TFIBFloatField;
    quDocsOutIdSUMTAR: TFIBFloatField;
    quDocsOutIdSUMNDS0: TFIBFloatField;
    quDocsOutIdSUMNDS1: TFIBFloatField;
    quDocsOutIdSUMNDS2: TFIBFloatField;
    quDocsOutIdPROCNAC: TFIBFloatField;
    quDocsOutIdIACTIVE: TFIBIntegerField;
    taNums: TpFIBDataSet;
    taNumsIT: TFIBIntegerField;
    taNumsSPRE: TFIBStringField;
    taNumsCURNUM: TFIBIntegerField;
    prAddPartIn2: TpFIBStoredProc;
    quGDS: TpFIBDataSet;
    quGDSARTICUL: TFIBIntegerField;
    quGDSIDATE: TFIBIntegerField;
    quGDSIDSTORE: TFIBIntegerField;
    quGDSPOSTIN: TFIBFloatField;
    quGDSPOSTOUT: TFIBFloatField;
    quGDSVNIN: TFIBFloatField;
    quGDSVNOUT: TFIBFloatField;
    quGDSINV: TFIBFloatField;
    quGDSQREAL: TFIBFloatField;
    quCardsSelKOEF: TFIBFloatField;
    quSpecInSelKM: TFIBFloatField;
    trUpdDel: TpFIBTransaction;
    quCReal: TpFIBDataSet;
    dsquCReal: TDataSource;
    quCRealDATEDOC: TFIBDateField;
    quCRealCODEB: TFIBIntegerField;
    quCRealNAMEB: TFIBStringField;
    quCRealQUANT: TFIBFloatField;
    quCRealPRICEOUT: TFIBFloatField;
    quCRealSUMOUT: TFIBFloatField;
    quCRealQUANTC: TFIBFloatField;
    quCRealPRICEIN: TFIBFloatField;
    quCRealSUMIN: TFIBFloatField;
    quCRealIM: TFIBIntegerField;
    quCRealSM: TFIBStringField;
    quCRealQB: TFloatField;
    quCPartInNUMDOCIN: TFIBStringField;
    quCPartInNUMDOCINV: TFIBStringField;
    quCPartInNUMDOCVN: TFIBStringField;
    quCPartInNUMDOCACTS: TFIBStringField;
    quCPartInNUMDOCCOMPL: TFIBStringField;
    quCPartInNUMDOC: TStringField;
    quCPartOutNDOB: TFIBStringField;
    quCPartOutNDI: TFIBStringField;
    quCPartOutNDV: TFIBStringField;
    quCPartOutNDA: TFIBStringField;
    quCPartOutNDC: TFIBStringField;
    quCPartOutNDO: TFIBStringField;
    quCPartOutNDR: TFIBStringField;
    quCPartOutNUMDOC: TStringField;
    quDOBHEADCOMMENT: TFIBStringField;
    quDocsOutBCOMMENT: TFIBStringField;
    quCanEdit: TpFIBDataSet;
    quCanEditID: TFIBIntegerField;
    quCanEditDATEDOC: TFIBDateField;
    quCanEditIDP: TFIBIntegerField;
    quCanEditVALEDIT: TFIBDateTimeField;
    quMHSpis: TpFIBDataSet;
    quMHSpisID: TFIBIntegerField;
    quMHSpisNAMEMH: TFIBStringField;
    quMHSpisNUMSPIS: TFIBSmallIntField;
    quCardsSelSPISSTORE: TFIBStringField;
    quCardsSel1SPISSTORE: TFIBStringField;
    quFCardSpis: TpFIBDataSet;
    quFCardSpisSPISSTORE: TFIBStringField;
    taTOInvSUM3: TFIBFloatField;
    quFCardLASTPRICEOUT: TFIBFloatField;
    quCardsSelRemn: TFloatField;
    quMHr: TpFIBDataSet;
    dsquMHr: TDataSource;
    quMHrID: TFIBIntegerField;
    quMHrPARENT: TFIBIntegerField;
    quMHrITYPE: TFIBIntegerField;
    quMHrNAMEMH: TFIBStringField;
    quMHrDEFPRICE: TFIBIntegerField;
    quMHrNAMEPRICE: TFIBStringField;
    quFCardRemn: TFloatField;
    dsMHr1: TDataSource;
    prDelCard: TpFIBStoredProc;
    quMH_: TpFIBDataSet;
    quMH_ID: TFIBIntegerField;
    quCPartInNDR: TFIBStringField;
    taTOOutR: TpFIBDataSet;
    taTOOutRID: TFIBIntegerField;
    taTOOutRDATEDOC: TFIBDateField;
    taTOOutRNUMDOC: TFIBStringField;
    taTOOutRDATESF: TFIBDateField;
    taTOOutRNUMSF: TFIBStringField;
    taTOOutRIDCLI: TFIBIntegerField;
    taTOOutRIDSKL: TFIBIntegerField;
    taTOOutRSUMIN: TFIBFloatField;
    taTOOutRSUMUCH: TFIBFloatField;
    taTOOutRSUMTAR: TFIBFloatField;
    taTOOutRNAMECL: TFIBStringField;
    taTOComplOutDATEDOC: TFIBDateField;
    taTOComplOutID: TFIBIntegerField;
    quCalcRemnSumInv: TpFIBDataSet;
    quCalcRemnSumInvID: TFIBIntegerField;
    quCalcRemnSumInvIDSTORE: TFIBIntegerField;
    quCalcRemnSumInvIDDOC: TFIBIntegerField;
    quCalcRemnSumInvARTICUL: TFIBIntegerField;
    quCalcRemnSumInvIDCLI: TFIBIntegerField;
    quCalcRemnSumInvDTYPE: TFIBIntegerField;
    quCalcRemnSumInvQPART: TFIBFloatField;
    quCalcRemnSumInvQREMN: TFIBFloatField;
    quCalcRemnSumInvPRICEIN: TFIBFloatField;
    quCalcRemnSumInvPRICEOUT: TFIBFloatField;
    quCalcRemnSumInvIDATE: TFIBIntegerField;
    quFindUse1: TpFIBDataSet;
    quFindUse1MAXDATE: TFIBDateField;
    quMH_NAMEMH: TFIBStringField;
    quDocsOutSelIDFROM: TFIBIntegerField;
    quDocsOutSelNAMECL1: TFIBStringField;
    quDocsOutIdIDFROM: TFIBIntegerField;
    cxStyle27: TcxStyle;
    quTC: TpFIBDataSet;
    quTCIDCARD: TFIBIntegerField;
    quTCID: TFIBIntegerField;
    quTCDATEB: TFIBDateField;
    quTCDATEE: TFIBDateField;
    quTCSHORTNAME: TFIBStringField;
    quTCRECEIPTNUM: TFIBStringField;
    quTCPOUTPUT: TFIBStringField;
    quTCPCOUNT: TFIBIntegerField;
    quTCPVES: TFIBFloatField;
    quTCTEHNO: TFIBStringField;
    quTCOFORM: TFIBStringField;
    quFindGROUP: TStringField;
    quFindSGROUP: TStringField;
    quFindCli: TpFIBDataSet;
    dsquFindCli: TDataSource;
    quFindCliID: TFIBIntegerField;
    quFindCliNAMECL: TFIBStringField;
    taClientsSROKPLAT: TFIBIntegerField;
    quCardsSelCOMMENT: TFIBStringField;
    quCardsSel1COMMENT: TFIBStringField;
    prAddKb: TpFIBStoredProc;
    quCardsSel1BB: TFIBFloatField;
    quCardsSel1GG: TFIBFloatField;
    quCardsSel1U1: TFIBFloatField;
    quCardsSel1U2: TFIBFloatField;
    quCardsSel1EE: TFIBFloatField;
    quCardsSelBB: TFIBFloatField;
    quCardsSelGG: TFIBFloatField;
    quCardsSelU1: TFIBFloatField;
    quCardsSelU2: TFIBFloatField;
    quCardsSelEE: TFIBFloatField;
    taTOActID: TFIBIntegerField;
    quFCardGR: TStringField;
    quFCardSGR: TStringField;
    quTSpecL: TpFIBDataSet;
    FIBIntegerField2: TFIBIntegerField;
    FIBIntegerField3: TFIBIntegerField;
    FIBIntegerField4: TFIBIntegerField;
    FIBIntegerField5: TFIBIntegerField;
    FIBIntegerField6: TFIBIntegerField;
    FIBFloatField1: TFIBFloatField;
    FIBFloatField2: TFIBFloatField;
    FIBStringField1: TFIBStringField;
    FIBStringField2: TFIBStringField;
    FIBFloatField3: TFIBFloatField;
    FIBFloatField4: TFIBFloatField;
    FIBIntegerField7: TFIBIntegerField;
    FIBFloatField5: TFIBFloatField;
    FIBStringField3: TFIBStringField;
    FIBIntegerField8: TFIBIntegerField;
    quTSpecIDC: TFIBIntegerField;
    quTSpecIDT: TFIBIntegerField;
    quTSpecID: TFIBIntegerField;
    quTSpecIDCARD: TFIBIntegerField;
    quTSpecCURMESSURE: TFIBIntegerField;
    quTSpecNETTO: TFIBFloatField;
    quTSpecBRUTTO: TFIBFloatField;
    quTSpecKNB: TFIBFloatField;
    quTSpecNAME: TFIBStringField;
    quTSpecTCARD: TFIBIntegerField;
    quTSpecNAMESHORT: TFIBStringField;
    quTSpecKOEF: TFIBFloatField;
    quTSpecNETTO1: TFIBFloatField;
    quTSpecCOMMENT: TFIBStringField;
    quTSpecIOBR: TFIBIntegerField;
    quRemnDateID: TFIBIntegerField;
    quRemnDatePARENT: TFIBIntegerField;
    quRemnDateNAME: TFIBStringField;
    quRemnDateTTYPE: TFIBIntegerField;
    quRemnDateIMESSURE: TFIBIntegerField;
    quRemnDateIACTIVE: TFIBIntegerField;
    quRemnDateTCARD: TFIBIntegerField;
    quRemnDateNAMESHORT: TFIBStringField;
    quRemnDateKOEF: TFIBFloatField;
    quE: TpFIBQuery;
    taTermoObr: TpFIBDataSet;
    taTermoObrID: TFIBIntegerField;
    taTermoObrNAMEOBR: TFIBStringField;
    taTermoObrBB: TFIBFloatField;
    taTermoObrGG: TFIBFloatField;
    taTermoObrU1: TFIBFloatField;
    taTermoObrU2: TFIBFloatField;
    dstaTermoObr: TDataSource;
    quMaxTermo: TpFIBDataSet;
    quMaxTermoMAXID: TFIBIntegerField;
    quUseTermoO: TpFIBDataSet;
    quUseTermoOIDCARD: TFIBIntegerField;
    quTSpecNETTO2: TFIBFloatField;
    quTSpecLNETTO2: TFIBFloatField;
    dstaTermoObr1: TDataSource;
    dstaTermoObr2: TDataSource;
    quTCardsIOBR: TFIBIntegerField;
    quSpecInSelTCARD: TFIBIntegerField;
    quSetTCard: TpFIBQuery;
    quUpd: TpFIBQuery;
    quF: TpFIBDataSet;
    taClientsNAMEOTP: TFIBStringField;
    taClientsADROTPR: TFIBStringField;
    taClientsRSCH: TFIBStringField;
    taClientsKSCH: TFIBStringField;
    taClientsBANK: TFIBStringField;
    taClientsBIK: TFIBStringField;
    quSpecInSelCATEGORY: TFIBIntegerField;
    quFCardCATEGORY: TFIBIntegerField;
    quSpecOutSelCATEGORY: TFIBIntegerField;
    quAllCardsCATEGORY: TFIBIntegerField;
    ColorDialog1: TColorDialog;
    quRemnDateCATEGORY: TFIBIntegerField;
    quMoveSelCATEGORY: TFIBIntegerField;
    quCateg: TpFIBDataSet;
    dsquCateg: TDataSource;
    quCategID: TFIBIntegerField;
    quCategNAMECAT: TFIBStringField;
    quUseCateg: TpFIBDataSet;
    quUseCategID: TFIBIntegerField;
    quCardsSelRCATEGORY: TFIBIntegerField;
    quTOREMNINT: TFIBFloatField;
    quTOPOSTINT: TFIBFloatField;
    quTOPOSTOUTT: TFIBFloatField;
    quTOVNINT: TFIBFloatField;
    quTOVNOUTT: TFIBFloatField;
    quTOINVT: TFIBFloatField;
    quTOREMNOUTT: TFIBFloatField;
    quTOSelREMNINT: TFIBFloatField;
    quTOSelPOSTINT: TFIBFloatField;
    quTOSelPOSTOUTT: TFIBFloatField;
    quTOSelVNINT: TFIBFloatField;
    quTOSelVNOUTT: TFIBFloatField;
    quTOSelINVT: TFIBFloatField;
    quTOSelREMNOUTT: TFIBFloatField;
    taTOVnInSUMTAR: TFIBFloatField;
    taTOVnOutSUMTAR: TFIBFloatField;
    taTOInvSUMTARAR: TFIBFloatField;
    taTOInvSUMTARAF: TFIBFloatField;
    taTOInvSUMTARAD: TFIBFloatField;
    quDOBSPECSALET: TFIBIntegerField;
    quDOBSPECSSALET: TFIBStringField;
    taDobSpecSALET: TFIBIntegerField;
    taDobSpecSSALET: TFIBStringField;
    quDocsInCardCATEGORY: TFIBIntegerField;
    prCALCSPEEDREAL: TpFIBStoredProc;
    quLastCli: TpFIBDataSet;
    trSel3: TpFIBTransaction;
    quLastCliIDHEAD: TFIBIntegerField;
    quLastCliNAMECL: TFIBStringField;
    quLastCliIDCARD: TFIBIntegerField;
    quLastCliQUANT: TFIBFloatField;
    quLastCliNAMESHORT: TFIBStringField;
    quLastCliPRICEIN: TFIBFloatField;
    quLastCliSUMIN: TFIBFloatField;
    quLastCliIDM: TFIBIntegerField;
    quLastCliKM: TFIBFloatField;
    quLastCliDATEDOC: TFIBDateField;
    quLastCliNUMDOC: TFIBStringField;
    quLastCliIDCLI: TFIBIntegerField;
    dsquLastCli: TDataSource;
    taTOInvIDETAL: TFIBSmallIntField;
    taBGU: TpFIBDataSet;
    taBGUID: TFIBIntegerField;
    taBGUPARENT: TFIBIntegerField;
    taBGUNAMEBGU: TFIBStringField;
    taBGUNAMEGR: TFIBStringField;
    taBGUBB: TFIBFloatField;
    taBGUGG: TFIBFloatField;
    taBGUU1: TFIBFloatField;
    taBGUU2: TFIBFloatField;
    taBGUEE: TFIBFloatField;
    dstaBGU: TDataSource;
    quFindM: TpFIBDataSet;
    quFindMID: TFIBIntegerField;
    quFindMIMESSURE: TFIBIntegerField;
    quFindMTCARD: TFIBIntegerField;
    quFindMNAMESHORT: TFIBStringField;
    quFindMKOEF: TFIBFloatField;
    quSpecInSelPRICE0: TFIBFloatField;
    quSpecInSelSUM0: TFIBFloatField;
    quSpecInSelNDSPROC: TFIBFloatField;
    quSpecInSelIDNDS: TFIBIntegerField;
    quDocsInSelINDS: TFIBIntegerField;
    quSpecInSelINDS: TFIBIntegerField;
    quCardsSelCTO: TFIBIntegerField;
    quCTO: TpFIBDataSet;
    dsquCTO: TDataSource;
    quCTOID: TFIBIntegerField;
    quCTONAME: TFIBStringField;
    quCTO1: TpFIBDataSet;
    FIBIntegerField9: TFIBIntegerField;
    FIBStringField4: TFIBStringField;
    quCTO1CTONAME: TFIBStringField;
    quCTO1NAME1: TFIBStringField;
    quCTO1COMM1: TFIBStringField;
    quCTO1COMM2: TFIBStringField;
    quCTO1COMM3: TFIBStringField;
    quCTO1COMM4: TFIBStringField;
    quCTO1COMM5: TFIBStringField;
    quTCardsPAR1: TFIBStringField;
    quTCardsPAR2: TFIBStringField;
    quTCardsPAR3: TFIBStringField;
    quTCardsPAR4: TFIBStringField;
    quTCardsPAR5: TFIBStringField;
    quTCardsPAR6: TFIBStringField;
    quTCardsPAR7: TFIBStringField;
    quTCardsPAR8: TFIBStringField;
    quTCardsPAR9: TFIBStringField;
    quTCardsPAR10: TFIBStringField;
    quMHAllISS: TFIBSmallIntField;
    quMHrISS: TFIBSmallIntField;
    taMHISS: TFIBSmallIntField;
    taTOPostInSUMNDS: TFIBFloatField;
    taTOPostOutSUMNDS: TFIBFloatField;
    quCPartInPRICEIN0: TFIBFloatField;
    quCPartInSUMIN0: TFIBFloatField;
    quSelPartInPRICEIN0: TFIBFloatField;
    quSpecOutSelPRICEIN0: TFIBFloatField;
    quSpecOutSelSUMIN0: TFIBFloatField;
    quSelPartIn1PRICEIN0: TFIBFloatField;
    quAllCardsPROC: TFIBFloatField;
    quCalcRemnSumPRICEIN0: TFIBFloatField;
    quCalcRemnSumInvPRICEIN0: TFIBFloatField;
    taTOComplSpec: TpFIBDataSet;
    taTOComplSpecSUMIN: TFIBFloatField;
    taTOComplSpecSUMIN0: TFIBFloatField;
    taTOOutBCOMMENT: TFIBStringField;
    taDobSpec1SUMIN0: TFIBFloatField;
    taDobSpec2PRICEIN0: TFIBFloatField;
    taDobSpec2SUMIN0: TFIBFloatField;
    taTOOutBSpec: TpFIBDataSet;
    taTOOutBSpecSUMIN: TFIBFloatField;
    taTOOutBSpecSUMIN0: TFIBFloatField;
    taTOOutRSpec: TpFIBDataSet;
    taTOOutRSpecSUMIN: TFIBFloatField;
    taTOOutRSpecSUMIN0: TFIBFloatField;
    quInputTC: TpFIBDataSet;
    quInputTCIDC: TFIBIntegerField;
    quPerBARCODE: TFIBStringField;
    quPerPATHEXP: TFIBStringField;
    quPerHOSTFTP: TFIBStringField;
    quPerLOGINFTP: TFIBStringField;
    quPerPASSWFTP: TFIBStringField;
    quCanEditISKL: TFIBIntegerField;
    quCardsSelCODEZAK: TFIBIntegerField;
    quPerSCLI: TFIBStringField;
    taClientsGLN: TFIBStringField;
    taMHEXPNAME: TFIBStringField;
    taMHNUMSPIS: TFIBSmallIntField;
    taMHGLN: TFIBStringField;
    quMHSelGLN: TFIBStringField;
    cxStyle28: TcxStyle;
    cxStyle29: TcxStyle;
    cxStyle30: TcxStyle;
    cxStyle31: TcxStyle;
    taTOOutRSpecCATEGORY: TFIBIntegerField;
    taTOOutRSpecSUMR: TFIBFloatField;
    quBReal: TpFIBDataSet;
    quBRealDATEDOC: TFIBDateField;
    quBRealIDHEAD: TFIBIntegerField;
    quBRealIDB: TFIBIntegerField;
    quBRealCODEB: TFIBIntegerField;
    quBRealNAMEB: TFIBStringField;
    quBRealQUANT: TFIBFloatField;
    quBRealPRICEOUT: TFIBFloatField;
    quBRealSUMOUT: TFIBFloatField;
    quBRealSUMIN: TFIBFloatField;
    dsquBReal: TDataSource;
    quBRealNUMDOC: TFIBStringField;
    quBRealRNAC: TFloatField;
    quCardsSelALGCLASS: TFIBIntegerField;
    quCardsSelALGMAKER: TFIBIntegerField;
    quCardsSelNAMEM: TFIBStringField;
    quQuantInOut: TpFIBDataSet;
    quQuantInOutQIN: TFIBFloatField;
    quQuantInOutQOUT: TFIBFloatField;
    quQuantInOutQVNIN: TFIBFloatField;
    quQuantInOutQVNOUT: TFIBFloatField;
    quQuantInOutQINV: TFIBFloatField;
    quQuantInOutQREAL: TFIBFloatField;
    quCardsSelVOL: TFIBFloatField;
    quTCardsPAR11: TFIBStringField;
    quTCardsPAR12: TFIBStringField;
    quTCardsPAR13: TFIBStringField;
    quTCardsPAR14: TFIBStringField;
    quTCardsPAR15: TFIBStringField;
    taMHPRIOR: TFIBSmallIntField;
    taMHFULLNAME: TFIBStringField;
    taMHINN: TFIBStringField;
    taMHKPP: TFIBStringField;
    taMHDIR1: TFIBStringField;
    taMHDIR2: TFIBStringField;
    taMHDIR3: TFIBStringField;
    taMHGB1: TFIBStringField;
    taMHGB2: TFIBStringField;
    taMHGB3: TFIBStringField;
    taMHCITY: TFIBStringField;
    taMHSTREET: TFIBStringField;
    taMHHOUSE: TFIBStringField;
    taMHKORP: TFIBStringField;
    taMHPOSTINDEX: TFIBStringField;
    taMHPHONE: TFIBStringField;
    taMHSERLIC: TFIBStringField;
    taMHNUMLIC: TFIBStringField;
    taMHORGAN: TFIBStringField;
    taMHDATEB: TFIBDateField;
    taMHDATEE: TFIBDateField;
    taMHIP: TFIBIntegerField;
    quMHSelEXPNAME: TFIBStringField;
    quMHSelNUMSPIS: TFIBSmallIntField;
    quMHSelISS: TFIBSmallIntField;
    quMHSelPRIOR: TFIBSmallIntField;
    quMHSelFULLNAME: TFIBStringField;
    quMHSelINN: TFIBStringField;
    quMHSelKPP: TFIBStringField;
    quMHSelDIR1: TFIBStringField;
    quMHSelDIR2: TFIBStringField;
    quMHSelDIR3: TFIBStringField;
    quMHSelGB1: TFIBStringField;
    quMHSelGB2: TFIBStringField;
    quMHSelGB3: TFIBStringField;
    quMHSelCITY: TFIBStringField;
    quMHSelSTREET: TFIBStringField;
    quMHSelHOUSE: TFIBStringField;
    quMHSelKORP: TFIBStringField;
    quMHSelPOSTINDEX: TFIBStringField;
    quMHSelPHONE: TFIBStringField;
    quMHSelSERLIC: TFIBStringField;
    quMHSelNUMLIC: TFIBStringField;
    quMHSelORGAN: TFIBStringField;
    quMHSelDATEB: TFIBDateField;
    quMHSelDATEE: TFIBDateField;
    quMHSelIP: TFIBIntegerField;
    quMHAllFULLNAME: TFIBStringField;
    quDocsInSelCOMMENT: TFIBStringField;
    quDocsInIdCOMMENT: TFIBStringField;
    taNumSCHF: TpFIBDataSet;
    taNumSCHFIMH: TFIBIntegerField;
    taNumSCHFIYY: TFIBIntegerField;
    taNumSCHFSPRE: TFIBStringField;
    taNumSCHFCURNUM: TFIBIntegerField;
    taMessCODE: TFIBIntegerField;
    taClientsOGRN: TFIBStringField;
    taMHIPREG: TFIBStringField;
    quMHSelIPREG: TFIBStringField;
    quSpecInSelOPRIZN: TFIBIntegerField;
    quDocsInSelOPRIZN: TFIBSmallIntField;
    quDocsInIdOPRIZN: TFIBSmallIntField;
    quFCardM: TpFIBDataSet;
    quFCardMIDDB: TFIBSmallIntField;
    quFCardMITYPE: TFIBSmallIntField;
    quFCardMSIFR: TFIBIntegerField;
    quFCardMCODEB: TFIBIntegerField;
    quFCardMKB: TFIBFloatField;
    quFTCardVes: TpFIBDataSet;
    quFTCardVesID: TFIBIntegerField;
    quFTCardVesSHORTNAME: TFIBStringField;
    quFTCardVesRECEIPTNUM: TFIBStringField;
    quFTCardVesPOUTPUT: TFIBStringField;
    quFTCardVesPCOUNT: TFIBIntegerField;
    quFTCardVesPVES: TFIBFloatField;
    teC1: TdxMemData;
    teC1Articul: TIntegerField;
    teC1Name: TStringField;
    teC1IdM: TIntegerField;
    teC1sM: TStringField;
    teC1Km: TFloatField;
    teC1Quant: TFloatField;
    teC1QuantFact: TFloatField;
    teC1QuantDiff: TFloatField;
    teC1SumUch: TFloatField;
    teC1SumIn: TFloatField;
    teC1SumIn0: TFloatField;
    teC2: TdxMemData;
    teC2ID: TIntegerField;
    teC2CODEB: TIntegerField;
    teC2NAMEB: TStringField;
    teC2QUANT: TFloatField;
    teC2PRICEOUT: TFloatField;
    teC2SUMOUT: TFloatField;
    teC2IDCARD: TIntegerField;
    teC2NAMEC: TStringField;
    teC2QUANTC: TFloatField;
    teC2PRICEIN: TFloatField;
    teC2SUMIN: TFloatField;
    teC2IM: TIntegerField;
    teC2SM: TStringField;
    teC2SB: TStringField;
    teC2PRICEIN0: TFloatField;
    teC2SUMIN0: TFloatField;
    teC0: TdxMemData;
    teC0ID: TIntegerField;
    teC0IDCARD: TIntegerField;
    teC0QUANT: TFloatField;
    teC0IDM: TIntegerField;
    teC0PRICER: TFloatField;
    teC0RSUM: TCurrencyField;
    teC0NAME: TStringField;
    teC0TCARD: TSmallintField;
    teC0KM: TFloatField;
    prCalcLastPrice3: TpFIBStoredProc;
    quDelCardM: TpFIBQuery;
    quDocsInSelCREATETYPE: TFIBSmallIntField;
    quDocsInIdCREATETYPE: TFIBSmallIntField;
    quSpecInSelICODECB: TFIBIntegerField;
    quSpecInSelNAMECB: TFIBStringField;
    quSpecInSelIMCB: TFIBSmallIntField;
    quSpecInSelSMCB: TFIBStringField;
    quSpecInSelKBCB: TFIBFloatField;
    quSpecInSelSBARCB: TFIBStringField;
    quSpecInSelQUANTCB: TFIBFloatField;
    quSpecInSelGTD: TFIBStringField;
    quDocsOutSelOPRIZN: TFIBSmallIntField;
    quDocsOutIdOPRIZN: TFIBSmallIntField;
    quCardsSelRKREP: TFIBFloatField;
    quCardsSelSERT: TFIBBlobField;
    quCardsSelNOTESTQR: TFIBSmallIntField;
    quFindCardNOTESTQR: TFIBSmallIntField;
    quCPartIn1: TpFIBDataSet;
    quCPartIn1ID: TFIBIntegerField;
    quCPartIn1IDSTORE: TFIBIntegerField;
    quCPartIn1NAMEMH: TFIBStringField;
    quCPartIn1IDDOC: TFIBIntegerField;
    quCPartIn1NUMDOCIN: TFIBStringField;
    quCPartIn1NUMDOCINV: TFIBStringField;
    quCPartIn1NUMDOCVN: TFIBStringField;
    quCPartIn1NUMDOCACTS: TFIBStringField;
    quCPartIn1NUMDOCCOMPL: TFIBStringField;
    quCPartIn1NDR: TFIBStringField;
    quCPartIn1ARTICUL: TFIBIntegerField;
    quCPartIn1IDCLI: TFIBIntegerField;
    quCPartIn1NAMECL: TFIBStringField;
    quCPartIn1DTYPE: TFIBIntegerField;
    quCPartIn1QPART: TFIBFloatField;
    quCPartIn1QREMN: TFIBFloatField;
    quCPartIn1PRICEIN: TFIBFloatField;
    quCPartIn1PRICEIN0: TFIBFloatField;
    quCPartIn1PRICEOUT: TFIBFloatField;
    quCPartIn1IDATE: TFIBIntegerField;
    quCPartIn1SUMIN: TFIBFloatField;
    quCPartIn1SUMIN0: TFIBFloatField;
    quCPartIn1SUMREMN: TFIBFloatField;
    dsquCPartIn1: TDataSource;
    quCPartIn1NUMDOC: TStringField;
    quMCODE: TFIBIntegerField;
    quCardsSelVES: TFIBFloatField;
    quFindCardVES: TFIBFloatField;
    taClientsKPP1: TFIBStringField;
    quTCardsIACTIVE: TFIBSmallIntField;
    quFTCard: TpFIBDataSet;
    quFTCardID: TFIBIntegerField;
    quFTCardSHORTNAME: TFIBStringField;
    quFTCardRECEIPTNUM: TFIBStringField;
    quFTCardPOUTPUT: TFIBStringField;
    quFTCardPCOUNT: TFIBIntegerField;
    quFTCardPVES: TFIBFloatField;
    quFTCardTEHNO: TFIBStringField;
    quTSpecICHANGE: TFIBSmallIntField;
    taTOComplIn: TpFIBDataSet;
    taTOComplInID: TFIBIntegerField;
    taTOComplInNUMDOC: TFIBStringField;
    taTOComplInSUMIN: TFIBFloatField;
    taTOComplInSUMUCH: TFIBFloatField;
    taTOComplInOPER: TFIBStringField;
    taTOComplInDATEDOC: TFIBDateField;
    quDocsInSelIDCLITO: TFIBIntegerField;
    quDocsInSelNAMECLTO: TFIBStringField;
    quDocsInIdIDCLITO: TFIBIntegerField;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure quTCardsCalcFields(DataSet: TDataSet);
    procedure quMoveSelCalcFields(DataSet: TDataSet);
    procedure quSelPartInCalcFields(DataSet: TDataSet);
    procedure taDobSpecQUANTChange(Sender: TField);
    procedure taDobSpecPRICERChange(Sender: TField);
    procedure quCRealCalcFields(DataSet: TDataSet);
    procedure quCPartInCalcFields(DataSet: TDataSet);
    procedure quCPartOutCalcFields(DataSet: TDataSet);
    procedure quCardsSelCalcFields(DataSet: TDataSet);
    procedure quFCardCalcFields(DataSet: TDataSet);
    procedure quFindCalcFields(DataSet: TDataSet);
    procedure quBRealCalcFields(DataSet: TDataSet);
    procedure quCPartIn1CalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

Function CanDo(Name_F:String):Boolean;
Function CanEdit(IDate,ISKL:Integer):Boolean;

Function GetId(ta:String):Integer;
procedure prSelPartIn(Articul,IdStore,iCli:Integer;rPrice:Real);
procedure prSelPartInT(Articul,IdStore,iCli:Integer);
procedure prSelPartInRet(Articul,IdStore,iCli:Integer);

function prFindKM(Idm:Integer):Real; //���� �����������
function prFindKNM(Idm:Integer;Var km:Real):String; //���� ����������� � �������� ������� ��
//function prFindCardKNM(IdC:Integer;Var km:Real):String; //���� ����������� � �������� ������� ��
function prFindM(IdCard:Integer; Var kM:Real;NameM:String):INteger;

function prFindBrutto(IdC:Integer;rDate:TDateTime):Real;
Procedure prFindSM(Idm:Integer;Var SM:String; Var IM:Integer); //�������� ��������

Procedure prFindIMCCode(Idm:Integer;Var SM,MCODE:String); //�������� ������� � ��� �������


function prFindMT(Idm:Integer):Integer;
Procedure prFindGroup(Idc:Integer;Var NameGr:String; Var Id_Group:Integer); //�������� ������
Procedure prFindCl(Idc:Integer;Var S1,S2,S3,S4,S5,S6,s7,s8,s9,s10:String); //�������� ������� �� ��� ID
Procedure prFindCl1(Idc:Integer;Var arr:array of String); //�������� ������� �� ��� ID � ��
Procedure prFindGLNCl(Idc:Integer;Var sGln:String); //�� Id ���������� GLN

function prFindIdCl(sInn:String):INteger; //����� ID ������� �� ���

function prFindCM(iDb,iCode:INteger):INteger; //���� ��� ����� �� ������ �� ������� ������������

function prFTCardVes(iCode:INteger):String; //���� ����� 1-�� ������ � ���


Function prCalcRemn(iCode,iDate,iSkl:Integer):Real;
Procedure prCalcBl(sBeg,sName:String;IdSkl,IdPos,iCode,iDate,iTCard:Integer;rQ:Real;IdM:INteger;taSpecB,taCalc,taCalcB:tdxMemData;Memo1:TcxMemo;bNeedMemo:Boolean;iSpis,iLevel,iOper:Integer);

//��� ���� ��� �� �����
Procedure prCalcBlPrice(iCode,iDate:Integer;rQ:Real;IdM:INteger;taCalc:tdxMemData;Var rSum:Real);
Procedure prCalcBlPrice1(sBeg:String;iCode,iDate:Integer;rQ:Real;IdM:INteger;taCalc:tdxMemData;Memo1:TcxMemo;Var rSumOut:Real);

Function prTOFindRemB(iDateB,IdSkl:Integer; Var rSum,rSumT:Real):Integer;
Function prTOFind(iDateB,IdSkl:Integer):Integer;
Procedure prTODel(iDateB,IdSkl:Integer);
Function prTOPostIn(iD,IdSkl:Integer;Memo1:TcxMemo; Var rSumT:Real):Real;
Function prTOPostOut(iD,IdSkl:Integer;Memo1:TcxMemo; Var rSumT:Real):Real;
Function prTOVnIn(iD,IdSkl:Integer;Memo1:TcxMemo; Var rSumT:Real):Real;
Function prTOVnOut(iD,IdSkl:Integer;Memo1:TcxMemo; Var rSumT:Real):Real;
//Function prTOInv(iD,IdSkl:Integer; Var rSumT:Real):Real;
Function prTOInv(iD,IdSkl:Integer; Var rSumT:Real; Var iType:SmallInt):Real;

Function prTOReal(iD,IdSkl:Integer;Memo1:TcxMemo;var rSumTar:Real):Real;
Function prTypeC(IdC:Integer):Integer; //��������� ��������
procedure prTestUseTC(IdC:Integer; Var iDate,iC:INteger);
procedure prTestUseTC1(Var iDate:INteger);
Function prFindGrN(IdC:INteger):String;
Function prCalcRemnSum(iCode,iDate,iSkl:Integer):Real; //��� �������� ����
Function prCalcRemnSumF(iCode,iDate,iSkl:Integer;rQ:Real;Var rSumIn0:Real):Real; //��� �������������� ����
Function prCalcRemnSpeed(iCode,iDate,iSkl:Integer;Var rSpeedDay,rRemnDay:Real):Real;

Function prTypeTC(IdC:Integer):Integer;
Function prGetNum(iT,iAdd:INteger):String;
Function fCalcBlasCard(IdSkl,iCode:Integer):Boolean;
Procedure prSetTC(IdC:Integer;bTC:Boolean);
Procedure prCalcBlDoc(sBeg,sName:String;IdSkl,IdPos,iCode,iDate,iTCard:Integer;rQ:Real;IdM:INteger;taSpecB,taCalc,taCalcB:tdxMemData;Memo1:TcxMemo;bNeedMemo:Boolean;iSpis,iLevel,iOper:Integer);

procedure prFinfBGU(idc:Integer;var bb,gg,uu,ves:Real);
procedure prFindObr(iobr:INteger;var bo,go,uo:Real; var so:String);
procedure prFinfBGUE(idc:Integer;var bb,gg,uu,ee:Real);

Function prISS(iSkl:Integer):Integer;
Function prGetNDS(INds:INteger;Var rProc:Real):String;
procedure prFindInputCards(iCard,iDate:Integer); //���� ��� ��������� ������� ����� � ������ TTK �� ����
Function prFindTOEnd(iDateB,IdSkl:Integer):Real;

Function prGetNumSCHF(iMH,iYY:INteger):String;

Function prFindGlnMH(iSkl:Integer):String;
Function prFindIPMH(iSkl:Integer;Var sName,sReg:String):Boolean;

function prCalcBlPriceIn(iCode,iTPrice,iSkl,iDate:INteger;Memo1:TcxMemo;Var UstCost:INteger):Real;
function prFindIdOper(sOp:String):INteger;

var
  dmO: TdmO;
  rQb,rQe:Real;
  sCards:String;

implementation

uses Un1, DMOReps, DocCompl;


{$R *.dfm}

// ��������� ������� �� ���������
Procedure prCalcBlDoc(sBeg,sName:String;IdSkl,IdPos,iCode,iDate,iTCard:Integer;rQ:Real;IdM:INteger;taSpecB,taCalc,taCalcB:tdxMemData;Memo1:TcxMemo;bNeedMemo:Boolean;iSpis,iLevel,iOper:Integer);
Var iTC,iPCount:INteger;
    QT1:TpFIBDataSet;
    rQ1,kBrutto,kM,MassaB:Real;
    iMain:INteger;
    sM:String;
    Par:Variant;
    bCalc:Boolean;
    rQRemn,rQbs:Real;
// ------------------------------------------------------
Procedure prCalcSave;
Var rQb:Real;
begin
  if taCalcB.Active = False then taCalcB.Active:=True;
  if taCalc.Active = False then taCalc.Active:=True;

  par := VarArrayCreate([0,1], varInteger);
  par[0]:=IdPos;
  par[1]:=iCode;
  if taCalcB.Locate('ID;IDCARD',par,[]) then
  begin
    rQb:=taCalcB.fieldByName('QUANTC').AsFloat+rQbs;
    taCalcB.edit;
    taCalcB.fieldByName('QUANTC').AsFloat:=rQb;
    taCalcB.fieldByName('PRICEIN').AsFloat:=0;
    taCalcB.fieldByName('SUMIN').AsFloat:=0;
    taCalcB.fieldByName('IM').AsInteger:=iMain;
    taCalcB.fieldByName('SM').AsString:=sM;
    taCalcB.fieldByName('SB').AsString:='';
    taCalcB.Post;
  end else
  begin
    taCalcB.Append;
    taCalcB.fieldByName('ID').AsInteger:=IdPos;
    taCalcB.fieldByName('CODEB').AsInteger:=taSpecB.fieldByName('IDCARD').AsInteger;
    taCalcB.fieldByName('NAMEB').AsString:=taSpecB.fieldByName('NAME').AsString;
    taCalcB.fieldByName('QUANT').AsFloat:=taSpecB.fieldByName('QUANT').AsFloat;
    taCalcB.fieldByName('PRICEOUT').AsFloat:=taSpecB.fieldByName('PRICER').AsFloat;
    taCalcB.fieldByName('SUMOUT').AsFloat:=taSpecB.fieldByName('RSUM').AsFloat;
    taCalcB.fieldByName('IDCARD').AsInteger:=iCode;
    taCalcB.fieldByName('NAMEC').AsString:=sName;
    taCalcB.fieldByName('QUANTC').AsFloat:=rQbs;
    taCalcB.fieldByName('PRICEIN').AsFloat:=0;
    taCalcB.fieldByName('SUMIN').AsFloat:=0;
    taCalcB.fieldByName('IM').AsInteger:=iMain;
    taCalcB.fieldByName('SM').AsString:=sM;
    taCalcB.fieldByName('SB').AsString:='';
    taCalcB.Post;
  end;

  if taCalc.Locate('Articul',iCode,[]) then
  begin
    rQb:=taCalc.fieldByName('Quant').AsFloat+rQbs;
    taCalc.Edit;
    taCalc.fieldByName('Quant').AsFloat:=rQb;
    taCalc.fieldByName('QuantFact').AsFloat:=0;
    taCalc.fieldByName('QuantDiff').AsFloat:=0;
    taCalc.Post;
  end else
  begin
    taCalc.Append;
    taCalc.fieldByName('Articul').AsInteger:=iCode;
    taCalc.fieldByName('Name').AsString:=sName;
    taCalc.fieldByName('Idm').AsInteger:=iMain;
    taCalc.fieldByName('sM').AsString:=sM;
    taCalc.fieldByName('Km').AsFloat:=1;
    taCalc.fieldByName('Quant').AsFloat:=rQbs;
    taCalc.fieldByName('QuantFact').AsFloat:=0;
    taCalc.fieldByName('QuantDiff').AsFloat:=0;
    taCalc.fieldByName('SumIn').AsFloat:=0;
    taCalc.fieldByName('SumUch').AsFloat:=0;
    taCalc.Post;
  end;
end;
  // ----------------������--------------------------------
begin
  sBeg:=sBeg+'    ';
  iLevel:=iLevel+1;

  if abs(rQ)<0.001 then exit;

  with dmO do
  begin
    //���
    if bNeedMemo then fmDocsCompl.Memo1.Lines.Add(SBeg+'��� - '+IntToStr(iCode));

    rQbs:=rQ;

    if iSpis=1 then //�� �������� ������������
    begin
      if iTCard=1 then // ����� � ��������
      begin
        if fCalcBlasCard(IdSkl,iCode) then bCalc:=False //���� �� ������������ �� ��� �����
        else
        begin
          //����� ��������� � ���� �� ��� ������ �� ���� � �� ����� ���� ������ �����
          quFindTCard.Active:=False;
          quFindTCard.ParamByName('IDCARD').AsInteger:=iCode;
          quFindTCard.ParamByName('IDATE').AsDateTime:=iDate;
          quFindTCard.Active:=True;
          if quFindTCard.RecordCount=0 then bCalc:=False
          else
          begin
            bCalc:=True;
            // ����� ��������� �������, ����� ����, ���� ����� ������� �������
            //�������� �������� �������� ��� ������������, ���� ������ ������� , �� ����� ����� ������������

            //��� �������� ����� � ������ ������� ��������������� , �.�. ��������� ������� ����� - ������������ �� ��������


            if (iLevel>1)or(iOper>0) then
            begin
              rQremn:=prCalcRemn(iCode,iDate,IdSkl);
              if (rQRemn>0)and(rQ>0) then //����� �������� ���������� �������� ��� ��������� � �������������� ����������
              begin                       // � ��� ������� �� ������� ������� ����������
                bCalc:=False; //����� ���� ��� ������� ����� ���� ����� �� �������
              //���� �������� �� ������� �� �������� � ��� �������� ������ � ��� ����� � ����������
                if rQRemn<rQ then  rQbs:=rQRemn;
              end;
            end;
          end;
          quFindTCard.Active:=False;
        end;
      end else bCalc:=False;
    end else  // �� �������� �� ������������
    begin
      bCalc:=False; //����� ���� ��� ������� ����� �� �������� � rQbs:=rQ;
    end;

    if bCalc=False then // ������������ �� ���� � ����� rQbs
    begin
        //��������� � �������� ������� �� ������ ������ ����� ������ ��� - �� ����
      kM:=prFindKM(IdM);
      prFindSM(IdM,sM,iMain); //��� �������� �������� ������� ���������
      rQbs:=rQbs*kM;              //������ ������� - ��� ������ �� �����������

      if bNeedMemo then fmDocsCompl.Memo1.Lines.Add(sBeg+sName+' ('+IntToStr(iCode)+'). ��� �����.  ���-��: '+FloatToStr(RoundEx(rQbs*100000)/100000)+' '+sM);

      //������ ���� ��� � �������� � � ������
      prCalcSave;

      if ((rQ-rQbs)>0.00001) and (iSpis=1) then  //��� ���-�� ��������  -  ���� ������������
      begin
        rQbs:=rQ-rQbs;
        bCalc:=True;
      end;
    end;

    if bCalc then //���� ��� ����� � ��� ���� ������������
    begin
      //����������� ���� ������������ � �����  rQbs ������ rQ

      iTC:=0; iPCount:=0; MassaB:=1;
      quFindTCard.Active:=False;
      quFindTCard.ParamByName('IDCARD').AsInteger:=iCode;
      quFindTCard.ParamByName('IDATE').AsDateTime:=iDate;
      quFindTCard.Active:=True;
      if quFindTCard.RecordCount>0 then
      begin
        iTC:=quFindTCardID.AsInteger;
        iPCount:=quFindTCardPCOUNT.AsInteger;
        MassaB:=quFindTCardPVES.AsFloat/1000;
        if prFindMT(IdM)=1 then MassaB:=1;
        prFindSM(IdM,sM,iMain); //��� �������� �������� ������� ���������
        if (iPCount=0) or (MassaB=0) then
        begin
          iTC:=0;
          inc(iErr);
          sErr:=sErr+IntToStr(iCode)+' '+sName+';';
          if bNeedMemo then fmDocsCompl.Memo1.Lines.Add(SBeg+'������: ������������ ��������� � ��.');
        end else
        begin
          if bNeedMemo then fmDocsCompl.Memo1.Lines.Add(sBeg+sName+' ('+INtToStr(iCode)+'). �� ����. ���-��: '+FloatToStr(RoundEx(rQbs*1000)/1000)+' '+sM);
        end;
      end else
      begin
        inc(iErr);
        sErr:=sErr+IntToStr(iCode)+' '+sName+';';
        if bNeedMemo then fmDocsCompl.Memo1.Lines.Add(SBeg+'������: �� �� �������.')
      end;
      quFindTCard.Active:=False;
      if iTC>0 then
      begin
        QT1:=TpFIBDataSet.Create(Owner);
        QT1.Active:=False;
        QT1.Database:=OfficeRnDb;
        QT1.Transaction:=trSel;
        QT1.SelectSQL.Clear;
        QT1.SelectSQL.Add('SELECT CS.ID,CS.IDCARD,CS.CURMESSURE,CS.NETTO,CS.BRUTTO,CS.KNB,');
        QT1.SelectSQL.Add('CD.NAME,CD.TCARD');
        QT1.SelectSQL.Add('FROM OF_CARDSTSPEC CS');
        QT1.SelectSQL.Add('left join of_cards cd on cd.ID=CS.IDCARD');
        QT1.SelectSQL.Add('where IDC='+IntToStr(iCode)+' and IDT='+IntToStr(iTC));
        QT1.SelectSQL.Add('ORDER BY CS.ID');
        QT1.Active:=True;

        QT1.First;
        while not QT1.Eof do
        begin
          rQ1:=QT1.FieldByName('NETTO').AsFloat;
          kM:=prFindKM(QT1.FieldByName('CURMESSURE').AsInteger);
          rQ1:=rQ1*kM;//��������� � �������� �������
          prFindSM(QT1.FieldByName('CURMESSURE').AsInteger,sM,iMain); //��� �������� �������� ������� ���������
          //���� ������ �� ������ ����
          kBrutto:=prFindBrutto(QT1.FieldByName('IDCARD').AsInteger,iDate);
          rQ1:=rQ1*(100+kBrutto)/100; //��� ����� � ������

          rQ1:=rQ1/iPCount; //��� ����� �� 1-� ������ ���� �������� �� �������
          rQ1:=rQ1*rQbs/MassaB; //��� ����� �� ��� ���-�� ������
          prCalcBlDoc(sBeg,QT1.FieldByName('NAME').AsString,IdSkl,IdPos,QT1.FieldByName('IDCARD').AsInteger,iDate,QT1.FieldByName('TCARD').AsInteger,rQ1,iMain,taSpecB,taCalc,taCalcB,fmDocsCompl.Memo1,bNeedMemo,iSpis,iLevel,iOper);

          QT1.Next;
        end;
        QT1.Active:=False;
        QT1.Free;
      end;
    end;


  end;
end;


function prFindIdOper(sOp:String):INteger;
begin
  with dmO do
  begin
    Result:=0;
    quF.Active:=False;
    quF.SelectSQL.Clear;
    quF.SelectSQL.Add('SELECT ID');
    quF.SelectSQL.Add('FROM OF_OPER');
    quF.SelectSQL.Add('where ABR='''+sOp+'''');
    quF.Active:=True;
    if quF.RecordCount>0 then Result:=quF.fieldbyname('ID').AsInteger;
    quF.Active:=False;
  end;
end;

function prCalcBlPriceIn(iCode,iTPrice,iSkl,iDate:INteger;Memo1:TcxMemo;Var UstCost:INteger):Real;
Var rPrice,rSumIn:Real;
begin
  with dmO do
  begin
    CloseTe(teC1);
    CloseTe(teC2);

    if Memo1<>nil then  Memo1.Lines.Add('    ������ ���������� ...');

    prCalcBl('    ',teC0NAME.AsString,iSkl,0,iCode,iDate,teC0TCARD.AsInteger,1,teC0IDM.AsInteger,teC0,teC1,teC2,Memo1,True,1,0,0);

//    EXECUTE PROCEDURE PR_CALCLASTPRICE3 (?IDGOOD, ?ITYPE, ?IDATE)

    rSumIn:=0;

    if Memo1<>nil then  Memo1.Lines.Add('    ������ ���� ...');

    teC1.First;
    while not teC1.Eof do
    begin
      UstCost:=1;
      prCalcLastPrice3.ParamByName('IDGOOD').AsInteger:=teC1Articul.AsInteger;
      prCalcLastPrice3.ParamByName('ITYPE').AsInteger:=iTPrice;
      prCalcLastPrice3.ParamByName('IDATE').AsInteger:=iDate;
      prCalcLastPrice3.ExecProc;
      rPrice:=rv(prCalcLastPrice3.ParamByName('PRICEIN').AsFloat);

      if abs(rPrice)<0.01 then UstCost:=0;

      if Memo1<>nil then  Memo1.Lines.Add('        ��� - '+its(teC1Articul.AsInteger)+' '+teC1Name.AsString+', ���� ������ - '+fts(rv(rPrice))+', ����� ������ - '+fts(rv(teC1Quant.AsFloat*rPrice)));

      teC1.Edit;
      teC1SumIn.AsFloat:=rv(teC1Quant.AsFloat*rPrice);
      teC1.Post;

      rSumIn:=rSumIn+teC1Quant.AsFloat*rPrice;

      teC1.Next;
    end;

    if Memo1<>nil then  Memo1.Lines.Add('          ���� ������ �� ������� - '+fts(rv(rSumIn))+'');

    Result:=rSumIn;
  end;
end;

function prFTCardVes(iCode:INteger):String; //���� ����� 1-�� ������ � ���
begin
  Result:='';
  with dmO do
  begin
    quFTCardVes.Active:=False;
    quFTCardVes.ParamByName('IDC').AsInteger:=iCode;
    quFTCardVes.Active:=True;
    quFTCardVes.First;
    if quFTCardVes.RecordCount>0 then Result:=quFTCardVesPOUTPUT.AsString;
    quFTCardVes.Active:=False;
  end;
end;

function prFindCM(iDb,iCode:INteger):INteger; //���� ��� ����� �� ������ �� ������� ������������
begin
  Result:=0;
  with dmO do
  begin
    quFCardM.Active:=False;
    quFCardM.ParamByName('IDC').AsInteger:=iCode;
    quFCardM.ParamByName('IDB').AsInteger:=iDb;
    quFCardM.Active:=True;
    if quFCardM.RecordCount>0 then
    begin
      quFCardM.First;
      Result:=quFCardMSIFR.AsInteger;
    end;

    quFCardM.Active:=False;
  end;
end;

Procedure prFindInputCards(iCard,iDate:Integer);
Var quFindInp:TpFIBDataSet;
begin
  with dmO do
  begin
    //��� ���������
    quFindInp:=TpFIBDataSet.Create(Owner);
    quFindInp.Active:=False;
    quFindInp.Database:=OfficeRnDb;
    quFindInp.Transaction:=trSel;
    quFindInp.SelectSQL.Clear;
    quFindInp.SelectSQL.Add('SELECT cs1.IDC');
    quFindInp.SelectSQL.Add('FROM OF_CARDSTSPEC cs1');
    quFindInp.SelectSQL.Add('left join OF_CARDST cardst on (cardst.IDCARD=cs1.IDC and cardst.ID=cs1.IDT)');
    quFindInp.SelectSQL.Add('where cs1.IDCARD='+its(iCard));
    quFindInp.SelectSQL.Add('and cardst.DATEE>='''+ds1(idate)+'''');
    quFindInp.SelectSQL.Add('group by cs1.IDC');
    quFindInp.Active:=True;
    delay(10);

    quFindInp.First;

    while not quFindInp.Eof do
    begin
      sCards:=sCards+','+its(quFindInp.FieldByName('IDC').AsInteger);

      prFindInputCards(quFindInp.FieldByName('IDC').AsInteger,iDate);

      quFindInp.Next;
    end;
    quFindInp.Active:=False;
    quFindInp.Free;
  end;
end;


Function prGetNDS(INds:INteger;Var rProc:Real):String;
begin
  with dmO do
  begin
    Result:='';
    taNDS.Active:=False;
    taNDS.Active:=True;
    taNds.First;
    while not taNDS.Eof do
    begin
      if taNDSID.AsInteger=INds then
      begin
        rProc:=taNDSPROC.AsFloat;
        Result:=taNDSNameNDS.AsString;
        break;
      end;

      taNDS.Next;
    end;

    taNDS.Active:=False;
  end;
end;

Function prFindIPMH(iSkl:Integer;Var sName,sReg:String):Boolean;
begin
  with dmO do
  begin
    Result:=False;
    taMH.Active:=False;
    taMH.Active:=True;

    if taMH.Locate('ID',iSkl,[]) then
    begin
      if taMHIP.AsInteger=1 then
      begin
        Result:=True;
        sName:=taMHDIR1.AsString+Copy(taMHDIR2.AsString,1,1)+'.'+Copy(taMHDIR3.AsString,1,1)+'.';
        sReg:=taMHIPREG.AsString;
      end;
    end;  

    taMH.Active:=False;
  end;
end;


Function prFindGlnMH(iSkl:Integer):String;
begin
  with dmO do
  begin
    Result:='';
    taMH.Active:=False;
    taMH.Active:=True;

    if taMH.Locate('ID',iSkl,[]) then Result:=taMHGLN.AsString;

    taMH.Active:=False;
  end;
end;


Function prISS(iSkl:Integer):Integer;
begin
  with dmO do
  begin
    Result:=0;
    taMH.Active:=False;
    taMH.Active:=True;

    if taMH.Locate('ID',iSkl,[]) then Result:=taMHISS.AsInteger;

    taMH.Active:=False;
  end;
end;

procedure prFindObr(iobr:INteger;var bo,go,uo:Real; var so:String);
begin
  bo:=0; go:=0; uo:=0; so:='';
  if iobr>0 then
  begin
    with dmO do
    begin
      quF.Active:=False;
      quF.SelectSQL.Clear;
      quF.SelectSQL.Add('SELECT NAMEOBR,BB,GG,U1,U2');
      quF.SelectSQL.Add('FROM OF_BGUOBR');
      quF.SelectSQL.Add('where ID ='+IntToStr(iobr));
      quF.Active:=True;
      if quF.RecordCount>0 then
      begin
        so:=quF.fieldbyname('NAMEOBR').AsString;
        bo:=quF.fieldbyname('BB').AsFloat;
        go:=quF.fieldbyname('GG').AsFloat;
        uo:=quF.fieldbyname('U1').AsFloat;
      end;
      quFind.Active:=False;
    end;
  end;
end;

procedure prFinfBGUE(idc:Integer;var bb,gg,uu,ee:Real);
begin
  bb:=0; gg:=0; uu:=0; ee:=0;
  with dmO do
  begin
    quF.Active:=False;
    quF.SelectSQL.Clear;
    quF.SelectSQL.Add('SELECT BB,GG,U1,U2,EE');
    quF.SelectSQL.Add('FROM OF_CARDS');
    quF.SelectSQL.Add('where ID ='+IntToStr(IdC));
    quF.Active:=True;
    if quF.RecordCount>0 then
    begin
      bb:=quF.fieldbyname('BB').AsFloat;
      gg:=quF.fieldbyname('GG').AsFloat;
      uu:=quF.fieldbyname('U1').AsFloat;
      ee:=quF.fieldbyname('EE').AsFloat;
    end;
    quFind.Active:=False;
  end;
end;


procedure prFinfBGU(idc:Integer;var bb,gg,uu,ves:Real);
begin
  bb:=0; gg:=0; uu:=0;
  with dmO do
  begin
    quF.Active:=False;
    quF.SelectSQL.Clear;
    quF.SelectSQL.Add('SELECT BB,GG,U1,U2,VES');
    quF.SelectSQL.Add('FROM OF_CARDS');
    quF.SelectSQL.Add('where ID ='+IntToStr(IdC));
    quF.Active:=True;
    if quF.RecordCount>0 then
    begin
      bb:=quF.fieldbyname('BB').AsFloat;
      gg:=quF.fieldbyname('GG').AsFloat;
      uu:=quF.fieldbyname('U1').AsFloat;
      ves:=quF.fieldbyname('VES').AsFloat;
    end;
    quFind.Active:=False;
  end;
end;

Procedure prSetTC(IdC:Integer;bTC:Boolean);
Var iTC:INteger;
begin
  with dmO do
  begin
    if bTc then iTc:=1 else iTc:=0;
    if quCardsSel.locate('ID',IdC,[]) then
    begin
      quCardsSel.Edit;
      quCardsSelTCARD.AsInteger:=iTC;
      quCardsSel.Post;
      quCardsSel.Refresh;
    end else
    begin
      quSetTCard.SQL.Clear;
      quSetTCard.SQL.Add('Update OF_CARDS Set TCARD='+IntToStr(iTC)+' where ID='+IntToStr(IdC));
      quSetTCard.ExecQuery;
    end;
  end;
end;

function prFindIdCl(sInn:String):INteger; //����� ID ������� �� ���
begin
  Result:=0;
  with dmO do
  begin
    while pos(' ',sInn)>0 do delete(sInn,pos(' ',sInn),1);
    if taClients.Active=False then taClients.Active:=True;
    if taClients.Locate('INN',sInn,[]) then
    begin
      Result:=taClientsID.AsInteger;
    end;
  end;
end;

Procedure prFindGLNCl(Idc:Integer;Var sGln:String); //�� Id ���������� GLN
begin
  with dmO do
  begin
    if taClients.Active=False then taClients.Active:=True;
    if taClients.Locate('ID',Idc,[]) then
    begin
      sGln:=taClientsGLN.AsString;
    end else
    begin
      sGln:='';
    end;
  end;
end;

Procedure prFindCl1(Idc:Integer;Var arr:array of String); //�������� ������� �� ��� ID
begin
  with dmO do
  begin
    if taClients.Active=False then taClients.Active:=True;
    if taClients.Locate('ID',Idc,[]) then
    begin
      arr[0]:=taClientsFULLNAMECL.AsString;
      arr[1]:=taClientsADDRES.AsString;
      arr[2]:=taClientsINN.AsString;
      arr[3]:=taClientsKPP.AsString;
      if taClientsNAMEOTP.AsString>'' then  arr[4]:=taClientsNAMEOTP.AsString else arr[4]:='';
      if taClientsADROTPR.AsString>'' then  arr[5]:=taClientsADROTPR.AsString else arr[5]:='';
      arr[6]:=taClientsRSCH.AsString;
      arr[7]:=taClientsKSCH.AsString;
      arr[8]:=taClientsBANK.AsString;
      arr[9]:=taClientsBIK.AsString;
      arr[10]:=taClientsKPP1.AsString;
      sFl:=taClientsMOL.AsString;
      sOGRN:=taClientsOGRN.AsString;
    end else
    begin
      arr[0]:='';
      arr[1]:='';
      arr[2]:='';
      arr[3]:='';
      arr[4]:='';
      arr[5]:='';
      arr[6]:='';
      arr[7]:='';
      arr[8]:='';
      arr[9]:='';
      arr[10]:='';
      sFl:='';
      sOGRN:='';
    end;
  end;
end;

Procedure prFindCl(Idc:Integer;Var S1,S2,S3,S4,s5,s6,s7,s8,s9,s10:String); //�������� ������� �� ��� ID
begin
  with dmO do
  begin
    if taClients.Active=False then taClients.Active:=True;
    if taClients.Locate('ID',Idc,[]) then
    begin
      S1:=taClientsFULLNAMECL.AsString;
      S2:=taClientsADDRES.AsString;
      S3:=taClientsINN.AsString;
      S4:=taClientsKPP.AsString;
      if taClientsNAMEOTP.AsString>'' then  S5:=taClientsNAMEOTP.AsString else S5:='';
      if taClientsADROTPR.AsString>'' then  S6:=taClientsADROTPR.AsString else S6:='';
      s7:=taClientsRSCH.AsString;
      s8:=taClientsKSCH.AsString;
      s9:=taClientsBANK.AsString;
      s10:=taClientsBIK.AsString;
      sFl:=taClientsMOL.AsString;
      sOGRN:=taClientsOGRN.AsString;

    end else
    begin
      S1:='';
      S2:='';
      S3:='';
      S4:='';
      S5:='';
      S6:='';
      S7:='';
      S8:='';
      S9:='';
      S10:='';
      sFl:='';
      sOGRN:='';
    end;
  end;
end;

Function fCalcBlasCard(IdSkl,iCode:Integer):Boolean;
Var StrWk:String;
    iNum:Integer;
begin
  Result:=False;
  if IdSkl=0 then Exit;
  with dmO do
  begin
    quFCardSpis.Active:=False;
    quFCardSpis.ParamByName('ICODE').AsInteger:=iCode;
    quFCardSpis.Active:=True;
    StrWk:=quFCardSpisSPISSTORE.AsString;
    if length(StrWk)<20 then StrWk:='00000000000000000000';
    quFCardSpis.Active:=False;

    iNum:=0;
    quMHSpis.Active:=False;
    quMHSpis.Active:=True;
    quMHSpis.First;
    while not quMHSpis.Eof do
    begin
      if quMHSpisID.AsInteger=IdSkl then
      begin
        iNum:=quMHSpisNUMSPIS.AsInteger;
        Break;
      end;
      quMHSpis.Next;
    end;
    quMHSpis.Active:=False;

    if (iNum>0) and (iNum<20) and (StrWk>'') then
    begin
      if StrWk[iNum]='1' then Result:=True;
    end;
  end;
end;

Function prGetNum(iT,iAdd:INteger):String;
var iNum:Integer;
    sPre:String;
begin
  Result:='';
  with dmO do
  begin
    taNums.Active:=False;
    taNums.ParamByName('ITYPE').AsInteger:=iT;
    taNums.Active:=True;

    taNums.First;

    if taNums.RecordCount>0  then
    begin
      iNum:=taNumsCURNUM.AsInteger+1;
      sPre:=taNumsSPRE.AsString;
      TrimStr(sPre);
      Result:=sPre+INtToStr(iNum);
      if iAdd=1 then
      begin
        taNums.Edit;
        taNumsCURNUM.AsInteger:=iNum;
        taNums.Post;
      end;
    end;

  end;
end;

Function prGetNumSCHF(iMH,iYY:INteger):String;
var iNum:Integer;
    sPre:String;
begin
  Result:='';
  with dmO do
  begin
    taNumSCHF.Active:=False;
    taNumSCHF.ParamByName('IMH').AsInteger:=iMH;
    taNumSCHF.ParamByName('IYY').AsInteger:=iYY;
    taNumSCHF.Active:=True;
    if taNumSCHF.RecordCount>0 then
    begin
      iNum:=taNumSCHFCURNUM.AsInteger+1;
      sPre:=taNumSCHFSPRE.AsString;
      TrimStr(sPre);
      Result:=sPre+INtToStr(iNum);

      taNumSCHF.Edit;
      taNumSCHFCURNUM.AsInteger:=iNum;
      taNumSCHF.Post;
    end else
    begin
      iNum:=1;
      Result:=INtToStr(iNum);

      taNumSCHF.Append;
      taNumSCHFIMH.AsInteger:=iMH;
      taNumSCHFIYY.AsInteger:=iYY;
      taNumSCHFSPRE.AsString:='';
      taNumSCHFCURNUM.AsInteger:=iNum;
      taNumSCHF.Post;
    end;
    taNumSCHF.Active:=False;
  end;
end;

Function prTypeTC(IdC:Integer):Integer;
begin
  Result:=0;
  with dmO do
  begin
    quFind.Active:=False;
    quFind.SelectSQL.Clear;
    quFind.SelectSQL.Add('SELECT ID,PARENT,NAME,TCARD');
    quFind.SelectSQL.Add('FROM OF_CARDS');
    quFind.SelectSQL.Add('where ID ='+IntToStr(IdC));
    quFind.Active:=True;
    if quFind.RecordCount>0 then
    begin
      Result:=quFind.fieldbyname('TCARD').AsInteger;
    end;
    quFind.Active:=False;
  end;
end;


Procedure prFindGroup(Idc:Integer;Var NameGr:String; Var Id_Group:Integer); //�������� ������
begin
  with dmORep do
  begin
    Id_Group:=0;
    NameGr:='';

    quSelNameCl.Active:=False;
    quSelNameCl.ParamByName('IDC').AsInteger:=Idc;
    quSelNameCl.Active:=True;

    quSelNameCl.First;
    if quSelNameCl.RecordCount>0 then
    begin
      Id_Group:=quSelNameClPARENT.AsInteger;
      NameGr:=quSelNameClNAMECL.AsString;
    end;

    quSelNameCl.Active:=False;
  end;
end;

Function prCalcRemnSum(iCode,iDate,iSkl:Integer):Real;
Var iSS:Integer;
begin // � ���� �������� �������� ���� ������ ��������
  with dmORep do
  begin
    iSS:=prIss(iSkl);
    prCalcSumRemn.ParamByName('IDCARD').AsInteger:=iCode;
    prCalcSumRemn.ParamByName('IDSKL').AsInteger:=iSkl;
    prCalcSumRemn.ParamByName('DDATE').AsDate:=iDate;
    prCalcSumRemn.ParamByName('ISS').AsInteger:=iSS;
    prCalcSumRemn.ExecProc;
    Result:=prCalcSumRemn.ParamByName('REMNSUM').AsFloat;
  end;
end;


Function prCalcRemnSumF(iCode,iDate,iSkl:Integer;rQ:Real;Var rSumIn0:Real):Real; //rQ ������ ���� � ��������, �.�. ���� � �������� ��
Var rSumCur,rQCur,rPriceLast,rSumCur0,rPriceLast0:Real;
    k:Integer;
//    iSS:INteger;
begin
  Result:=0;
  k:=1;
  if rQ<0 then k:=-1;
  rQCur:=rQ*k;
  rSumCur:=0;
  rSumCur0:=0; //��� ���
//  iSS:=prIss(iSkl);

  if abs(rQ)<0.0001 then exit; //� ������ ���������� �������� �� �����

  with dmO do
  begin

  { ������� - ����� ��������� 10-� (50) ������ � ������� ���� (���� ���������) � �� ��� �������
SELECT FIRST 50 ID,IDSTORE,IDDOC,ARTICUL,IDCLI,DTYPE,QPART,QREMN,PRICEIN,PRICEOUT,IDATE
FROM OF_PARTIN     //��� � ��������
where IDSTORE=:IDSKL
and ARTICUL=:IDCARD
and IDATE<=:IDATE
and DTYPE<>3
ORDER by IDATE desc}

    quCalcRemnSum.Active:=False;
    quCalcRemnSum.ParamByName('IDSKL').AsInteger:=iSkl;
    quCalcRemnSum.ParamByName('IDCARD').AsInteger:=iCode;
    quCalcRemnSum.ParamByName('IDATE').AsInteger:=iDate;
    quCalcRemnSum.Active:=True;

    if quCalcRemnSum.RecordCount>0 then
    begin
//      rPriceCur:=0;
      rPriceLast:=0;
      rPriceLast0:=0;
      quCalcRemnSum.First;
      while (quCalcRemnSum.Eof=False)and(rQCur>0) do
      begin
        if rPriceLast=0 then rPriceLast:=quCalcRemnSumPRICEIN.AsFloat; // �������� ������ ������ �.����� - ���� ���������� �������
        if rPriceLast0=0 then rPriceLast0:=quCalcRemnSumPRICEIN0.AsFloat; // �������� ������ ������ �.����� - ���� ���������� �������
        if k=-1 then
        begin //��� ������������� ��������� ����� ������ ���� ���������� ������� �� ���� ���������� �� ���-�� ������
          rSumCur:=rQCur*quCalcRemnSumPRICEIN.AsFloat;
          rSumCur0:=rQCur*quCalcRemnSumPRICEIN0.AsFloat;
          rQCur:=0;
        end else
        begin
          if quCalcRemnSumQPART.AsFloat>=rQCur then
          begin
            rSumCur:=rSumCur+rQCur*quCalcRemnSumPRICEIN.AsFloat;
            rSumCur0:=rSumCur0+rQCur*quCalcRemnSumPRICEIN0.AsFloat;
            rQCur:=0;
          end else
          begin
            rSumCur:=rSumCur+quCalcRemnSumQPART.AsFloat*quCalcRemnSumPRICEIN.AsFloat;
            rSumCur0:=rSumCur0+quCalcRemnSumQPART.AsFloat*quCalcRemnSumPRICEIN0.AsFloat;
            rQCur:=rQCur-quCalcRemnSumQPART.AsFloat;
//            rPriceCur:=quCalcRemnSumPRICEIN.AsFloat;
          end;
        end;
        quCalcRemnSum.Next;
      end;
      if rQCur>0 then
      begin
        rSumCur:=rSumCur+rQCur*rPriceLast; //���� ��� ���-�� �������� �� �� ���� ������ ������ - ���� ���������� �������
        rSumCur0:=rSumCur0+rQCur*rPriceLast0; //���� ��� ���-�� �������� �� �� ���� ������ ������ - ���� ���������� �������
      end;

      result:=rSumCur*k;
      rSumIn0:=rSumCur0*k;

    end else
    begin
      quCalcRemnSumInv.Active:=False;
      quCalcRemnSumInv.ParamByName('IDSKL').AsInteger:=iSkl;
      quCalcRemnSumInv.ParamByName('IDCARD').AsInteger:=iCode;
      quCalcRemnSumInv.ParamByName('IDATE').AsInteger:=iDate;
      quCalcRemnSumInv.Active:=True;

      if quCalcRemnSumInv.RecordCount>0 then
      begin
//      rPriceCur:=0;
        rPriceLast:=0;
        rPriceLast0:=0;
        quCalcRemnSumInv.First;
        while (quCalcRemnSumInv.Eof=False)and(rQCur>0) do
        begin
          if rPriceLast=0 then rPriceLast:=quCalcRemnSumInvPRICEIN.AsFloat; // �������� ������ ������ �.����� - ���� ���������� �������
          if rPriceLast0=0 then rPriceLast0:=quCalcRemnSumInvPRICEIN0.AsFloat; // �������� ������ ������ �.����� - ���� ���������� �������
          if k=-1 then
          begin //��� ������������� ��������� ����� ������ ���� ���������� ������� �� ���� ���������� �� ���-�� ������
            rSumCur:=rQCur*quCalcRemnSumInvPRICEIN.AsFloat;
            rSumCur0:=rQCur*quCalcRemnSumInvPRICEIN0.AsFloat;
            rQCur:=0;
          end else
          begin
            if quCalcRemnSumInvQPART.AsFloat>=rQCur then
            begin
              rSumCur:=rSumCur+rQCur*quCalcRemnSumInvPRICEIN.AsFloat;
              rSumCur0:=rSumCur0+rQCur*quCalcRemnSumInvPRICEIN0.AsFloat;
              rQCur:=0;
            end else
            begin
              rSumCur:=rSumCur+quCalcRemnSumInvQPART.AsFloat*quCalcRemnSumInvPRICEIN.AsFloat;
              rSumCur0:=rSumCur0+quCalcRemnSumInvQPART.AsFloat*quCalcRemnSumInvPRICEIN0.AsFloat;
              rQCur:=rQCur-quCalcRemnSumInvQPART.AsFloat;
//            rPriceCur:=quCalcRemnSumInvPRICEIN.AsFloat;
            end;
          end;
          quCalcRemnSumInv.Next;
        end;
        if rQCur>0 then
        begin
          rSumCur:=rSumCur+rQCur*rPriceLast; //���� ��� ���-�� �������� �� �� ���� ������ ������ - ���� ���������� �������
          rSumCur0:=rSumCur0+rQCur*rPriceLast0; //���� ��� ���-�� �������� �� �� ���� ������ ������ - ���� ���������� �������
        end;

        result:=rSumCur*k;
        rSumIn0:=rSumCur0*k;
      end;

      quCalcRemnSumInv.Active:=False;
    end;
    quCalcRemnSum.Active:=False;
  end;
end;



Function prFindGrN(IdC:INteger):String;
begin
  with dmO do
  begin
    quFindGrName.Active:=False;
    quFindGrName.ParamByName('IDCL').AsInteger:=IdC;
    quFindGrName.Active:=True;
    if quFindGrName.RecordCount>0 then Result:=quFindGrNameNAMECL.AsString
    else Result:='';
    quFindGrName.Active:=False;
  end;
end;

procedure prTestUseTC1(Var iDate:INteger);
var iDD,iDD2:Integer;
begin
  //���������� ���� ���������� ������������� ������ ����. ����� � � ����� �����������
//  iCC:=IdC;
  //��� �������� � ��������
  with dmO do
  begin

    quFindUse.Active:=False;
    quFindUse.SelectSQL.Clear;
    quFindUse.SelectSQL.Add('SELECT max(dh.DATEDOC) as maxdate FROM OF_DOCSPECOUTB ds');
    quFindUse.SelectSQL.Add('left join of_docheadoutb dh on dh.ID=ds.IDHEAD');
    quFindUse.SelectSQL.Add('where IDCARD in ('+sCards+') and dh.IACTIVE=1');
    quFindUse.Active:=True;
    delay(10);

    iDD:=Trunc(quFindUseMaxDate.AsDateTime);
    quFindUse.Active:=False;

    quFindUse1.Active:=False;
    quFindUse1.SelectSQL.Clear;
    quFindUse1.SelectSQL.Add('SELECT max(dh.DATEDOC) as maxdate FROM OF_DOCSPECCOMPLB ds');
    quFindUse1.SelectSQL.Add('left join of_docheadCOMPL dh on dh.ID=ds.IDHEAD');
    quFindUse1.SelectSQL.Add('where IDCARD in ('+sCards+') and dh.IACTIVE=1');
    quFindUse1.Active:=True;
    delay(10);

    iDD2:=Trunc(quFindUse1MaxDate.AsDateTime);
    quFindUse1.Active:=False;
    if iDD2>iDD then iDD:=iDD2; //�������� ������������
  end;
  iDate:=iDD;
end;

procedure prTestUseTC(IdC:Integer; Var iDate,iC:INteger);
var iDD,iCC,iDD2:Integer;
begin
  //���������� ���� ���������� ������������� ������ ����. ����� � � ����� �����������
//  iDD:=0;
  exit;
  iCC:=IdC;
  //��� �������� � ��������
  with dmO do
  begin

    quFindUse.Active:=False;
    quFindUse.ParamByName('IDC').AsInteger:=IdC;
    quFindUse.Active:=True;
    delay(10);
{
SELECT max(dh.DATEDOC) as maxdate FROM OF_DOCSPECOUTB ds
left join of_docheadoutb dh on dh.ID=ds.IDHEAD
where IDCARD = :IDC and dh.IACTIVE=1
}
    iDD:=Trunc(quFindUseMaxDate.AsDateTime);
    quFindUse.Active:=False;

    quFindUse1.Active:=False;
    quFindUse1.ParamByName('IDC').AsInteger:=IdC;
    quFindUse1.Active:=True;
    delay(10);
{
SELECT max(dh.DATEDOC) as maxdate FROM OF_DOCSPECCOMPLB ds
left join of_docheadCOMPL dh on dh.ID=ds.IDHEAD
where IDCARD = :IDC and dh.IACTIVE=1
}
    iDD2:=Trunc(quFindUse1MaxDate.AsDateTime);
    quFindUse1.Active:=False;

    if iDD2>iDD then iDD:=iDD2; //�������� ������������

    //��� � �������� �����������
{   QT1:=TpFIBDataSet.Create(Owner);
    QT1.Active:=False;
    QT1.Database:=OfficeRnDb;
    QT1.Transaction:=trSel;
    QT1.SelectSQL.Clear;
    QT1.SelectSQL.Add('SELECT IDC FROM OF_CARDSTSPEC');
    QT1.SelectSQL.Add('where IDCARD='+IntToStr(IdC));
    QT1.Active:=True;
    delay(10);

    QT1.First;

    while not QT1.Eof do
    begin
//      prTestUseTC(QT1.FieldByName('IDC').AsInteger,iDD1,iCC1);
      if iDD1>iDD then
      begin
        iDD:=iDD1; iCC:=iCC1;
      end;
      QT1.Next;
    end;
    QT1.Active:=False;
    QT1.Free;}
  end;
  iDate:=iDD;
  iC:=iCC;
end;


Function prTypeC(IdC:Integer):Integer;
begin
  Result:=1;
  with dmO do
  begin
    quSelCardsCat.Active:=False;
    quSelCardsCat.ParamByName('IDCARD').AsInteger:=IdC;
    quSelCardsCat.Active:=True;
    quSelCardsCat.First;
    if quSelCardsCat.RecordCount>0 then
    begin
      Result:=quSelCardsCatCATEGORY.AsInteger;
    end;
    quSelCardsCat.Active:=False;
  end;
end;

Function prTOReal(iD,IdSkl:Integer;Memo1:TcxMemo;var rSumTar:Real):Real;
Var rSum:Real;
    rSumDoc:Real;
    iR:Integer;
begin
  with dmO do
  begin
    taTOOutB.Active:=False;
    taTOOutB.ParamByName('DATEB').AsDate:=iD;
    taTOOutB.ParamByName('IDSKL').AsInteger:=IdSkl;
    taTOOutB.Active:=True;

    rSum:=0;
    rSumTar:=0;

    taTOOutB.First;
    while not taTOOutB.Eof do
    begin
      prWH('      ��� � '+taTOOutBNUMDOC.AsString+' �� '+FormatDateTime('dd.mm.yyyy',taTOOutBDATEDOC.AsDateTime),Memo1);
//      iR:=fTestPartDoc(taTOOutBSUMIN.AsFloat,taTOOutBID.AsInteger,2);
      iR:=0; //������ �� ������������
      if iR>0 then
      begin
        if iR=1 then  prWH('      ���������� ����������� ���� ('+IntToStr(iR)+'). ����� ����������� ��������.',Memo1);
        if iR=2 then  prWH('      ���������� ����������� ���� ('+IntToStr(iR)+'). ����� ��������� ����� �����������.',Memo1);
        //�������� ����
        rSumDoc:=fRecalcSumDoc(taTOOutBID.AsInteger,2,iR,Memo1);
        rSum:=rSum+rSumDoc;
      end
      else
        rSum:=rSum+taTOOutBSUMIN.AsFloat;

//      rSum:=rSum+taTOOutBSUMIN.AsFloat;
      taTOOutB.Next;
    end;
    taTOOutB.Active:=False;


    taTOOutR.Active:=False;
    taTOOutR.ParamByName('DATEB').AsDate:=iD;
    taTOOutR.ParamByName('IDSKL').AsInteger:=IdSkl;
    taTOOutR.Active:=True;

    taTOOutR.First;
    while not taTOOutR.Eof do
    begin
      prWH('      ��� � '+taTOOutRNUMDOC.AsString+' �� '+FormatDateTime('dd.mm.yyyy',taTOOutRDATEDOC.AsDateTime),Memo1);
      iR:=fTestPartDoc(taTOOutRSUMIN.AsFloat,taTOOutRID.AsInteger,8);
      if iR>0 then
      begin
        if iR=1 then  prWH('      ���������� ����������� ���� ('+IntToStr(iR)+'). ����� ����������� ��������.',Memo1);
        if iR=2 then  prWH('      ���������� ����������� ���� ('+IntToStr(iR)+'). ����� ��������� ����� �����������.',Memo1);
        //�������� ����
        rSumDoc:=fRecalcSumDoc(taTOOutRID.AsInteger,2,iR,Memo1);
        rSum:=rSum+rSumDoc;
      end
      else
        rSum:=rSum+taTOOutRSUMIN.AsFloat;

      rSumTar:=rSumTar+taTOOutRSUMTAR.AsFloat;
      taTOOutR.Next;
    end;
    taTOOutR.Active:=False;

  end;
  Result:=rSum;
end;

Function prTOVnIn(iD,IdSkl:Integer;Memo1:TcxMemo;Var rSumT:Real):Real;
Var rSum:Real;
    rSumDoc:Real;
    iR:Integer;
begin
  with dmO do
  begin
    //���������� ���������

    taTOVnIn.Active:=False;
    taTOVnIn.ParamByName('DATEB').AsDate:=iD;
    taTOVnIn.ParamByName('IDSKL').AsInteger:=IdSkl;
    taTOVnIn.Active:=True;

    rSum:=0; rSumT:=0;
    taTOVnIn.First;
    while not taTOVnIn.Eof do
    begin
      prWH('      ��� � '+taTOVnInNUMDOC.AsString+' �� '+FormatDateTime('dd.mm.yyyy',taTOVnInDATEDOC.AsDateTime),Memo1);
      iR:=fTestPartDoc(taTOVnInSUMIN.AsFloat+taTOVnInSUMTAR.AsFloat,taTOVnInID.AsInteger,21);
      if iR>0 then
      begin
        if iR=1 then  prWH('      ���������� ����������� ���� ('+IntToStr(iR)+'). ����� ����������� ��������.',Memo1);
        if iR=2 then  prWH('      ���������� ����������� ���� ('+IntToStr(iR)+'). ����� ��������� ����� �����������.',Memo1);
        //�������� ����
        rSumDoc:=fRecalcSumDoc(taTOVnInID.AsInteger,4,iR,Memo1);
        rSum:=rSum+rv(rSumDoc);
      end
      else
        rSum:=rSum+rv(taTOVnInSUMIN.AsFloat);

      rSumT:=rSumT+rv(taTOVnInSUMTAR.AsFloat);

//      rSum:=rSum+taTOVnInSUMIN.AsFloat;
      taTOVnIn.Next;
    end;
    taTOVnIn.Active:=False;


    //������������
    taTOComplIn.Active:=False;
    taTOComplIn.ParamByName('DATEB').AsDate:=iD;
    taTOComplIn.ParamByName('IDSKL').AsInteger:=IdSkl;
    taTOComplIn.Active:=True;

    taTOComplIn.First;      //������� � ������
    while not taTOComplIn.Eof do
    begin
      prWH('      ��� � '+taTOComplInNUMDOC.AsString+' �� '+FormatDateTime('dd.mm.yyyy',taTOComplInDATEDOC.AsDateTime),Memo1);
      iR:=fTestPartDoc(taTOComplInSUMIN.AsFloat,taTOComplInID.AsInteger,22);
      if iR>0 then
      begin
        if iR=1 then  prWH('      ���������� ����������� ���� ('+IntToStr(iR)+'). ����� ����������� ��������.',Memo1);
        if iR=2 then  prWH('      ���������� ����������� ���� ('+IntToStr(iR)+'). ����� ��������� ����� �����������.',Memo1);
        //�������� ����
        rSumDoc:=fRecalcSumDoc(taTOComplInID.AsInteger,6,iR,Memo1);
        rSum:=rSum+rSumDoc;
      end
      else
        rSum:=rSum+taTOComplInSUMIN.AsFloat;

      taTOComplIn.Next;
    end;
    taTOComplIn.Active:=False;


    //���� �����������
    taTOAct.Active:=False;
    taTOAct.ParamByName('DATEB').AsDate:=iD;
    taTOAct.ParamByName('IDSKL').AsInteger:=IdSkl;
    taTOAct.Active:=True;

    taTOAct.First;      //������� � ������
    while not taTOAct.Eof do
    begin
      fTestPartDoc(taTOActSUMIN.AsFloat,taTOActID.AsInteger,23);
      taTOAct.Next;
    end;
    taTOAct.Active:=False;
  end;

  Result:=rSum;
end;

Function prTOVnOut(iD,IdSkl:Integer;Memo1:TcxMemo;Var rSumT:Real):Real;
Var rSum:Real;
    rSumDoc:Real;
    iR:Integer;
begin
  with dmO do
  begin
    taTOVnOut.Active:=False;
    taTOVnOut.ParamByName('DATEB').AsDate:=iD;
    taTOVnOut.ParamByName('IDSKL').AsInteger:=IdSkl;
    taTOVnOut.Active:=True;

    rSum:=0;  rSumT:=0;
    taTOVnOut.First;
    while not taTOVnOut.Eof do
    begin
//      rSum:=rSum+taTOVnOutSUMIN.AsFloat;
      prWH('      ��� � '+taTOVnOutNUMDOC.AsString+' �� '+FormatDateTime('dd.mm.yyyy',taTOVnOutDATEDOC.AsDateTime),Memo1);
      iR:=fTestPartDoc(taTOVnOutSUMIN.AsFloat+taTOVnOutSUMTAR.AsFloat,taTOVnOutID.AsInteger,31);
      if iR>0 then
      begin
        if iR=1 then  prWH('      ���������� ����������� ���� ('+IntToStr(iR)+'). ����� ����������� ��������.',Memo1);
        if iR=2 then  prWH('      ���������� ����������� ���� ('+IntToStr(iR)+'). ����� ��������� ����� �����������.',Memo1);
        //�������� ����

        rSumDoc:=fRecalcSumDoc(taTOVnOutID.AsInteger,4,iR,Memo1);
        rSum:=rSum+rv(rSumDoc);
      end
      else
        rSum:=rSum+rv(taTOVnOutSUMIN.AsFloat);

      rSumT:=rSumT+taTOVnOutSUMTAR.AsFloat;

      taTOVnOut.Next;
    end;
    taTOVnOut.Active:=False;

    //������������
    taTOComplOut.Active:=False;
    taTOComplOut.ParamByName('DATEB').AsDate:=iD;
    taTOComplOut.ParamByName('IDSKL').AsInteger:=IdSkl;
    taTOComplOut.Active:=True;

    taTOComplOut.First;
    while not taTOComplOut.Eof do
    begin
      prWH('      ��� � '+taTOComplOutNUMDOC.AsString+' �� '+FormatDateTime('dd.mm.yyyy',taTOComplOutDATEDOC.AsDateTime),Memo1);
      iR:=fTestPartDoc(taTOComplOutSUMIN.AsFloat,taTOComplOutID.AsInteger,32);
      if iR>0 then
      begin
        if iR=1 then  prWH('      ���������� ����������� ���� ('+IntToStr(iR)+'). ����� ����������� ��������.',Memo1);
        if iR=2 then  prWH('      ���������� ����������� ���� ('+IntToStr(iR)+'). ����� ��������� ����� �����������.',Memo1);
        //�������� ����
        rSumDoc:=fRecalcSumDoc(taTOComplOutID.AsInteger,6,iR,Memo1);
        rSum:=rSum+rSumDoc;
      end
      else
        rSum:=rSum+taTOComplOutSUMIN.AsFloat;

      taTOComplOut.Next;
    end;
    taTOComplOut.Active:=False;

    //���� �����������
    taTOAct.Active:=False;
    taTOAct.ParamByName('DATEB').AsDate:=iD;
    taTOAct.ParamByName('IDSKL').AsInteger:=IdSkl;
    taTOAct.Active:=True;

    taTOAct.First;      //������� � ������
    while not taTOAct.Eof do
    begin
      fTestPartDoc(taTOActSUMIN.AsFloat,taTOActID.AsInteger,33);
      taTOAct.Next;
    end;
    taTOAct.Active:=False;

  end;
  Result:=rSum;
end;

Function prTOInv(iD,IdSkl:Integer; Var rSumT:Real;Var iType:SmallInt):Real;
Var rSum:Real;
begin       //��������������
  with dmO do
  begin
    taTOInv.Active:=False;
    taTOInv.ParamByName('DATEB').AsDate:=iD;
    taTOInv.ParamByName('IDSKL').AsInteger:=IdSkl;
    taTOInv.Active:=True;

    rSum:=0;  rSumT:=0;
    taTOInv.First;
    while not taTOInv.Eof do
    begin
      if taTOInvIDETAL.AsInteger = 2 then      //����������� ��� ���������
      begin
        rSum:=rSum+rv(taTOInvSUM2.AsFloat); //������ ����� �����
        rSumT:=rSumT+rv(taTOInvSUMTARAF.AsFloat);

        iType:=2;
        break;
      end else  //�����������
      begin
//      rSum:=rSum+taTOInvSUM2.AsFloat;
        rSum:=rSum+rv(taTOInvSUM2.AsFloat-taTOInvSUM1.AsFloat); //������� ����� - ��������
        rSumT:=rSumT+rv(taTOInvSUMTARAD.AsFloat);
        iType:=1;
      end;

      taTOInv.Next;
    end;
    taTOInv.Active:=False;
  end;
  Result:=rSum;
end;

Function prTOPostOut(iD,IdSkl:Integer;Memo1:TcxMemo; Var rSumT:Real):Real;
Var rSum:Real;
    rSumDoc:Real;
    iR:Integer;
    iSS:INteger;
    rSumNDS:Real;
begin
  with dmO do
  begin
    iSS:=prISS(IdSkl);

    taTOPostOut.Active:=False;
    taTOpostOut.ParamByName('DATEB').AsDate:=iD;
    taTOpostOut.ParamByName('IDSKL').AsInteger:=IdSkl;
    taTOPostOut.Active:=True;

    rSum:=0;  rSumT:=0;  rSumNDS:=0;
    taTOPostOut.First;
    while not taTOPostOut.Eof do
    begin
      prWH('      ��� � '+taTOPostOutNumDoc.AsString+' �� '+FormatDateTime('dd.mm.yyyy',taTOPostOutDATEDOC.AsDateTime),Memo1);
      iR:=fTestPartDoc(taTOPostOutSUMIN.AsFloat+taTOPostOutSUMTAR.AsFloat,taTOPostOutID.AsInteger,7);
      if iR>0 then
      begin
        if iR=1 then  prWH('      ���������� ����������� ���� ('+IntToStr(iR)+'). ����� ����������� ��������.',Memo1);
        if iR=2 then  prWH('      ���������� ����������� ���� ('+IntToStr(iR)+'). ����� ��������� ����� �����������.',Memo1);
        //�������� ����
        rSumDoc:=fRecalcSumDoc(taTOPostOutID.AsInteger,7,iR,Memo1);
        rSum:=rSum+rv(rSumDoc);
      end
      else
        rSum:=rSum+rv(taTOPostOutSUMIN.AsFloat);

      rSumT:=rSumT+rv(taTOPostOutSUMTAR.AsFloat);
      rSumNDS:=rSumNDS+rv(taTOPostOutSUMNDS.AsFloat);

      taTOPostOut.Next;
    end;
    taTOPostOut.Active:=False;
  end;
  if iSS=2 then Result:=rSum-rSumNDS else Result:=rSum;
end;




Function prTOPostIn(iD,IdSkl:Integer;Memo1:TcxMemo; Var rSumT:Real):Real;
Var rSum:Real;
    rSumDoc:Real;
    iR:Integer;
//    iSS:INteger;
    rSumNDS:Real;
begin
  with dmO do
  begin
//    iSS:=prISS(IdSkl);

    taTOPostIn.Active:=False;
    taTOpostIn.ParamByName('DATEB').AsDate:=iD;
    taTOpostIn.ParamByName('IDSKL').AsInteger:=IdSkl;
    taTOPostIn.Active:=True;

    rSum:=0; rSumT:=0; rSumNDS:=0;
    taTOPostIn.First;
    while not taTOPostIn.Eof do
    begin
      prWH('      ��� � '+taTOPostInNUMDOC.AsString+' �� '+FormatDateTime('dd.mm.yyyy',taTOPostInDATEDOC.AsDateTime),Memo1);
//      iR:=fTestPartDoc(taTOPostInSUMIN.AsFloat+taTOPostInSUMTAR.AsFloat,taTOPostInID.AsInteger,1);
      iR:=0; //������ �� ������������
      if iR>0 then
      begin
        if iR=1 then  prWH('      ���������� ����������� ���� ('+IntToStr(iR)+'). ����� ����������� ��������.',Memo1);
        if iR=2 then  prWH('      ���������� ����������� ���� ('+IntToStr(iR)+'). ����� ��������� ����� �����������.',Memo1);
        //�������� ����
        if iR=1 then prWH('        ��������� ����������� ��������� (��������� ������������).',Memo1);
        rSumDoc:=fRecalcSumDoc(taTOPostInID.AsInteger,1,iR,Memo1);
        rSum:=rSum+rv(rSumDoc);
      end
      else
        rSum:=rSum+rv(taTOPostInSUMIN.AsFloat);

      rSumT:=rSumT+rv(taTOPostInSUMTAR.AsFloat);  
      rSumNDS:=rSumNDS+rv(taTOPostInSUMNDS.AsFloat);

      taTOPostIn.Next;
    end;
    taTOPostIn.Active:=False;
  end;
//  if iSS=2 then Result:=rSum-rSumNDS else
  Result:=rSum;
end;

Procedure prTODel(iDateB,IdSkl:Integer);
begin
  with dmO do
  begin
    prLog(10,iDateB,3,IdSkl); //������� ��

    WriteHistory('�������� �� � '+FormatDateTime('dd.mm.yyyy',iDateB)+'. ��������: '+Person.Name);
    quTODel.ParamByName('DATEB').AsInteger:=iDateB;
    quTODel.ParamByName('IDSKL').AsInteger:=IdSkl;
    quTODel.ExecQuery;
  end;
end;


Function prFindTOEnd(iDateB,IdSkl:Integer):Real;
begin
  with dmO do
  begin
    Result:=0;

    quTOSel.Active:=False;
    quTOSel.ParamByName('DATEB').AsInteger:=iDateB;
    quTOSel.ParamByName('DATEE').AsInteger:=iDateB+1;
    quTOSel.ParamByName('IDSKL').AsInteger:=IdSkl;
    quTOSel.Active:=True;
    if quTOSel.RecordCount>0 then Result:=quTOSelREMNOUT.AsFloat;
    quTOSel.Active:=False;
  end;
end;

Function prTOFind(iDateB,IdSkl:Integer):Integer;
begin
  with dmO do
  begin
    Result:=0;

    quTOSel.Active:=False;
    quTOSel.ParamByName('DATEB').AsInteger:=iDateB;
    quTOSel.ParamByName('DATEE').AsInteger:=iDateB+1;
    quTOSel.ParamByName('IDSKL').AsInteger:=IdSkl;
    quTOSel.Active:=True;
    if quTOSel.RecordCount>0 then Result:=1;
    quTOSel.Active:=False;
  end;
end;


Function prTOFindRemB(iDateB,IdSkl:Integer; Var rSum,rSumT:Real):Integer;
begin
  with dmO do
  begin
    Result:=1;
    rSum:=0;

    quTOSel.Active:=False;
    quTOSel.ParamByName('DATEB').AsInteger:=iDateB-1;
    quTOSel.ParamByName('DATEE').AsInteger:=iDateB;
    quTOSel.ParamByName('IDSKL').AsInteger:=IdSkl;
    quTOSel.Active:=True;
    if quTOSel.RecordCount>0 then
    begin
      Result:=0;
      rSum:=quTOSelREMNOUT.AsFloat;
      rSumT:=quTOSelREMNOUTT.AsFloat;
    end;
    quTOSel.Active:=False;
  end;
end;

Procedure prCalcBlPrice1(sBeg:String;iCode,iDate:Integer;rQ:Real;IdM:INteger;taCalc:tdxMemData;Memo1:TcxMemo;Var rSumOut:Real);
Var iTC,iPCount:INteger;
    QT1:TpFIBDataSet;
    rQ1,kBrutto,kM,MassaB:Real;
    IdC,iMain:INteger;
    sM,StrWk,StrWk1,StrWk2,StrWk3,StrWk4:String;
    rSum:Real;
begin
  sBeg:=sBeg+'    ';
  with dmO do
  begin
    //���
    rSumOut:=0; MassaB:=1;
    iTC:=0; iPCount:=0;
    quFindTCard.Active:=False;
    quFindTCard.ParamByName('IDCARD').AsInteger:=iCode;
    quFindTCard.ParamByName('IDATE').AsDateTime:=iDate;
    quFindTCard.Active:=True;
    if quFindTCard.RecordCount>0 then
    begin
      iTC:=quFindTCardID.AsInteger;
      iPCount:=quFindTCardPCOUNT.AsInteger;
      MassaB:=quFindTCardPVES.AsFloat/1000;
      if prFindMT(IdM)=1 then MassaB:=1;
    end else
    begin
      inc(iErr);
      sErr:=sErr+IntToStr(iCode)+';';
      Memo1.Lines.Add(SBeg+'������: �� �� �������.')
    end;
    quFindTCard.Active:=False;
    if iTC>0 then
    begin
      QT1:=TpFIBDataSet.Create(Owner);
      QT1.Active:=False;
      QT1.Database:=OfficeRnDb;
      QT1.Transaction:=trSel;
      QT1.SelectSQL.Clear;
      QT1.SelectSQL.Add('SELECT CS.ID,CS.IDCARD,CS.CURMESSURE,CS.NETTO,CS.BRUTTO,CS.KNB,');
      QT1.SelectSQL.Add('CD.NAME,CD.TCARD');
      QT1.SelectSQL.Add('FROM OF_CARDSTSPEC CS');
      QT1.SelectSQL.Add('left join of_cards cd on cd.ID=CS.IDCARD');
      QT1.SelectSQL.Add('where IDC='+IntToStr(iCode)+' and IDT='+IntToStr(iTC));
      QT1.SelectSQL.Add('ORDER BY CS.ID');
      QT1.Active:=True;

      QT1.First;
      while not QT1.Eof do
      begin
        //����� �����
        rQ1:=QT1.FieldByName('NETTO').AsFloat;

        //��������� � �������� �������
        kM:=prFindKM(QT1.FieldByName('CURMESSURE').AsInteger);
        prFindSM(QT1.FieldByName('CURMESSURE').AsInteger,sM,iMain); //��� �������� �������� ������� ���������
        rQ1:=rQ1*kM; // 0,050

        //���� ������ �� ������ ����
        IdC:=QT1.FieldByName('IDCARD').AsInteger;
        kBrutto:=prFindBrutto(IdC,iDate);
        rQ1:=rQ1*(100+kBrutto)/100; //��� ����� � ������   //0,050
        rQ1:=rQ1/iPCount; //��� ����� �� 1-� ������ ���� �������� �� ������� //0,050
        rQ1:=rQ1*rQ/MassaB; //��� ����� �� ��� ���-�� ������ rQ/����� ������ 0,050*0,6/0,05

        StrWk:=QT1.FieldByName('NAME').AsString;
        While Length(StrWk)<20 do StrWk:=StrWk+' ';
        StrWk1:=QT1.FieldByName('IDCARD').AsString;
        While Length(StrWk1)<5 do StrWk1:=' '+StrWk1;
        Str((RoundEx(rQ1*1000)/1000):8:3,StrWk2);

        if QT1.FieldByName('TCARD').AsInteger=1 then
        begin  //���� ��

          Memo1.Lines.Add(sBeg+StrWk+' ('+StrWk1+'). �� ����. ���-��: '+StrWk2+' '+sM);
          prCalcBlPrice1(sBeg,QT1.FieldByName('IDCARD').AsInteger,iDate,rQ1,iMain,taCalc,Memo1,rSum);
        end else
        begin //��� ��
          rSum:=0;
          if taCalc.Locate('Articul',QT1.FieldByName('IDCARD').AsInteger,[]) then
            if taCalc.FieldByName('Quant').AsFloat<>0 then
              rSum:=taCalc.FieldByName('SumIn').AsFloat/taCalc.FieldByName('Quant').AsFloat*rQ1;
          Str((RoundEx(rSum*100)/100):8:2,StrWk3);
          Str((RoundEx(rSum/rQ1*100)/100):8:2,StrWk4);
          Memo1.Lines.Add(sBeg+StrWk+' ('+StrWk1+'). �� ���.  ���-��: '+StrWk2+' '+sM+'    �����: '+StrWk3+ '�'+'    ����: '+StrWk4+ '�');
        end;
        rSumOut:=rSumOut+rSum;
        QT1.Next;
      end;
      QT1.Active:=False;
      QT1.Free;
    end;
  end;
end;



Procedure prCalcBlPrice(iCode,iDate:Integer;rQ:Real;IdM:INteger;taCalc:tdxMemData;Var rSum:Real);
Var iTC,iPCount:INteger;
    QT1:TpFIBDataSet;
    rQ1,kBrutto,kM:Real;
    IdC,iMain:INteger;
    sM:String;
    rSum1,MassaB:Real;
begin
  rSum:=0; MassaB:=1;
  with dmO do
  begin
    //���
    iTC:=0; iPCount:=0;
    quFindTCard.Active:=False;
    quFindTCard.ParamByName('IDCARD').AsInteger:=iCode;
    quFindTCard.ParamByName('IDATE').AsDateTime:=iDate;
    quFindTCard.Active:=True;
    if quFindTCard.RecordCount>0 then
    begin
      iTC:=quFindTCardID.AsInteger;
      iPCount:=quFindTCardPCOUNT.AsInteger;
      MassaB:=quFindTCardPVES.AsFloat/1000;
      if prFindMT(IdM)=1 then MassaB:=1;
    end;
    quFindTCard.Active:=False;
    if iTC>0 then
    begin
      QT1:=TpFIBDataSet.Create(Owner);
      QT1.Active:=False;
      QT1.Database:=OfficeRnDb;
      QT1.Transaction:=trSel;
      QT1.SelectSQL.Clear;
      QT1.SelectSQL.Add('SELECT CS.ID,CS.IDCARD,CS.CURMESSURE,CS.NETTO,CS.BRUTTO,CS.KNB,');
      QT1.SelectSQL.Add('CD.NAME,CD.TCARD');
      QT1.SelectSQL.Add('FROM OF_CARDSTSPEC CS');
      QT1.SelectSQL.Add('left join of_cards cd on cd.ID=CS.IDCARD');
      QT1.SelectSQL.Add('where IDC='+IntToStr(iCode)+' and IDT='+IntToStr(iTC));
      QT1.SelectSQL.Add('ORDER BY CS.ID');
      QT1.Active:=True;

      QT1.First;
      while not QT1.Eof do
      begin
        //����� �����
        rQ1:=QT1.FieldByName('NETTO').AsFloat;

        //��������� � �������� �������
        kM:=prFindKM(QT1.FieldByName('CURMESSURE').AsInteger);
        prFindSM(QT1.FieldByName('CURMESSURE').AsInteger,sM,iMain); //��� �������� �������� ������� ���������
        rQ1:=rQ1*kM;

        //���� ������ �� ������ ����
        IdC:=QT1.FieldByName('IDCARD').AsInteger;
        kBrutto:=prFindBrutto(IdC,iDate);
        rQ1:=rQ1*(100+kBrutto)/100; //��� ����� � ������
        rQ1:=rQ1/iPCount; //��� ����� �� 1-� ������ ���� �������� �� �������
        rQ1:=rQ1*rQ/MassaB; //��� ����� �� ��� ���-�� ������

        if QT1.FieldByName('TCARD').AsInteger=1 then
        begin  //���� ��
//          prCalcBl(sBeg,QT1.FieldByName('IDCARD').AsInteger,iDate,rQ,taCalc,Memo1);
          prCalcBlPrice(QT1.FieldByName('IDCARD').AsInteger,iDate,rQ1,iMain,taCalc,rSum1);
          rSum:=rSum+rSum1;
        end else
        begin //��� ��
          if taCalc.Locate('Articul',QT1.FieldByName('IDCARD').AsInteger,[]) then
            if taCalc.FieldByName('Quant').AsFloat<>0 then rSum:=rSum+taCalc.FieldByName('SumIn').AsFloat/taCalc.FieldByName('Quant').AsFloat*rQ1;
        end;
        QT1.Next;
      end;
      QT1.Active:=False;
      QT1.Free;
    end;
  end;
end;

Procedure prCalcBl(sBeg,sName:String;IdSkl,IdPos,iCode,iDate,iTCard:Integer;rQ:Real;IdM:INteger;taSpecB,taCalc,taCalcB:tdxMemData;Memo1:TcxMemo;bNeedMemo:Boolean;iSpis,iLevel,iOper:Integer);
Var iTC,iPCount:INteger;
    QT1:TpFIBDataSet;
    rQ1,kBrutto,kM,MassaB:Real;
    iMain:INteger;
    sM:String;
    Par:Variant;
    bCalc:Boolean;
    rQRemn,rQbs:Real;

Procedure prCalcSave;
Var rQb:Real;
begin
  if taCalcB.Active = False then taCalcB.Active:=True;
  if taCalc.Active = False then taCalc.Active:=True;

  par := VarArrayCreate([0,1], varInteger);
  par[0]:=IdPos;
  par[1]:=iCode;
  if taCalcB.Locate('ID;IDCARD',par,[]) then
  begin
    rQb:=taCalcB.fieldByName('QUANTC').AsFloat+rQbs;
    taCalcB.edit;
    taCalcB.fieldByName('QUANTC').AsFloat:=rQb;
    taCalcB.fieldByName('PRICEIN').AsFloat:=0;
    taCalcB.fieldByName('SUMIN').AsFloat:=0;
    taCalcB.fieldByName('IM').AsInteger:=iMain;
    taCalcB.fieldByName('SM').AsString:=sM;
    taCalcB.fieldByName('SB').AsString:='';
    taCalcB.Post;
  end else
  begin
    taCalcB.Append;
    taCalcB.fieldByName('ID').AsInteger:=IdPos;
    taCalcB.fieldByName('CODEB').AsInteger:=taSpecB.fieldByName('IDCARD').AsInteger;
    taCalcB.fieldByName('NAMEB').AsString:=taSpecB.fieldByName('NAME').AsString;
    taCalcB.fieldByName('QUANT').AsFloat:=taSpecB.fieldByName('QUANT').AsFloat;
    taCalcB.fieldByName('PRICEOUT').AsFloat:=taSpecB.fieldByName('PRICER').AsFloat;
    taCalcB.fieldByName('SUMOUT').AsFloat:=taSpecB.fieldByName('RSUM').AsFloat;
    taCalcB.fieldByName('IDCARD').AsInteger:=iCode;
    taCalcB.fieldByName('NAMEC').AsString:=sName;
    taCalcB.fieldByName('QUANTC').AsFloat:=rQbs;
    taCalcB.fieldByName('PRICEIN').AsFloat:=0;
    taCalcB.fieldByName('SUMIN').AsFloat:=0;
    taCalcB.fieldByName('IM').AsInteger:=iMain;
    taCalcB.fieldByName('SM').AsString:=sM;
    taCalcB.fieldByName('SB').AsString:='';
    taCalcB.Post;
  end;

  if taCalc.Locate('Articul',iCode,[]) then
  begin
    rQb:=taCalc.fieldByName('Quant').AsFloat+rQbs;
    taCalc.Edit;
    taCalc.fieldByName('Quant').AsFloat:=rQb;
    taCalc.fieldByName('QuantFact').AsFloat:=0;
    taCalc.fieldByName('QuantDiff').AsFloat:=0;
    taCalc.Post;
  end else
  begin
    taCalc.Append;
    taCalc.fieldByName('Articul').AsInteger:=iCode;
    taCalc.fieldByName('Name').AsString:=sName;
    taCalc.fieldByName('Idm').AsInteger:=iMain;
    taCalc.fieldByName('sM').AsString:=sM;
    taCalc.fieldByName('Km').AsFloat:=1;
    taCalc.fieldByName('Quant').AsFloat:=rQbs;
    taCalc.fieldByName('QuantFact').AsFloat:=0;
    taCalc.fieldByName('QuantDiff').AsFloat:=0;
    taCalc.fieldByName('SumIn').AsFloat:=0;
    taCalc.fieldByName('SumUch').AsFloat:=0;
    taCalc.Post;
  end;
end;

begin
  sBeg:=sBeg+'    ';
  iLevel:=iLevel+1;

  if abs(rQ)<0.001 then exit;

  with dmO do
  begin
    //���
    if bNeedMemo and bPrintMemo then Memo1.Lines.Add(SBeg+'��� - '+IntToStr(iCode));

    rQbs:=rQ;

    if iSpis=1 then //�� �������� ������������
    begin
      if iTCard=1 then // ����� � ��������
      begin
        if fCalcBlasCard(IdSkl,iCode) then bCalc:=False //���� �� ������������ �� ��� �����
        else
        begin
          //����� ��������� � ���� �� ��� ������ �� ���� � �� ����� ���� ������ �����
          quFindTCard.Active:=False;
          quFindTCard.ParamByName('IDCARD').AsInteger:=iCode;
          quFindTCard.ParamByName('IDATE').AsDateTime:=iDate;
          quFindTCard.Active:=True;
          if quFindTCard.RecordCount=0 then bCalc:=False
          else
          begin
            bCalc:=True;
            // ����� ��������� �������, ����� ����, ���� ����� ������� �������
            //�������� �������� �������� ��� ������������, ���� ������ ������� , �� ����� ����� ������������

            //��� �������� ����� � ������ ������� ��������������� , �.�. ��������� ������� ����� - ������������ �� ��������


            if (iLevel>1)or(iOper>0) then
            begin
              rQremn:=prCalcRemn(iCode,iDate,IdSkl);
              if (rQRemn>0)and(rQ>0) then //����� �������� ���������� �������� ��� ��������� � �������������� ����������
              begin                       // � ��� ������� �� ������� ������� ����������
                bCalc:=False; //����� ���� ��� ������� ����� ���� ����� �� �������
              //���� �������� �� ������� �� �������� � ��� �������� ������ � ��� ����� � ����������
                if rQRemn<rQ then  rQbs:=rQRemn;
              end;
            end;
          end;
          quFindTCard.Active:=False;
        end;
      end else bCalc:=False;
    end else  // �� �������� �� ������������
    begin
      bCalc:=False; //����� ���� ��� ������� ����� �� �������� � rQbs:=rQ;
    end;

    if bCalc=False then // ������������ �� ���� � ����� rQbs
    begin
        //��������� � �������� ������� �� ������ ������ ����� ������ ��� - �� ����
      kM:=prFindKM(IdM);
      prFindSM(IdM,sM,iMain); //��� �������� �������� ������� ���������
      rQbs:=rQbs*kM;              //������ ������� - ��� ������ �� �����������

      if bNeedMemo and bPrintMemo then Memo1.Lines.Add(sBeg+sName+' ('+IntToStr(iCode)+'). ��� �����.  ���-��: '+FloatToStr(RoundEx(rQbs*100000)/100000)+' '+sM);

      //������ ���� ��� � �������� � � ������
      prCalcSave;

      if ((rQ-rQbs)>0.00001) and (iSpis=1) then  //��� ���-�� ��������  -  ���� ������������
      begin
        rQbs:=rQ-rQbs;
        bCalc:=True;
      end;
    end;

    if bCalc then //���� ��� ����� � ��� ���� ������������
    begin
      //����������� ���� ������������ � �����  rQbs ������ rQ

      iTC:=0; iPCount:=0; MassaB:=1;
      quFindTCard.Active:=False;
      quFindTCard.ParamByName('IDCARD').AsInteger:=iCode;
      quFindTCard.ParamByName('IDATE').AsDateTime:=iDate;
      quFindTCard.Active:=True;
      if quFindTCard.RecordCount>0 then
      begin
        iTC:=quFindTCardID.AsInteger;
        iPCount:=quFindTCardPCOUNT.AsInteger;
        MassaB:=quFindTCardPVES.AsFloat/1000;
        if prFindMT(IdM)=1 then MassaB:=1;
        prFindSM(IdM,sM,iMain); //��� �������� �������� ������� ���������
        if (iPCount=0) or (MassaB=0) then
        begin
          iTC:=0;
          inc(iErr);
          sErr:=sErr+IntToStr(iCode)+' '+sName+';';
          if bNeedMemo and bPrintMemo then Memo1.Lines.Add(SBeg+'������: ������������ ��������� � ��.');
        end else
        begin
          if bNeedMemo and bPrintMemo then Memo1.Lines.Add(sBeg+sName+' ('+INtToStr(iCode)+'). �� ����. ���-��: '+FloatToStr(RoundEx(rQbs*1000)/1000)+' '+sM);
        end;
      end else
      begin
        inc(iErr);
        sErr:=sErr+IntToStr(iCode)+' '+sName+';';
        if bNeedMemo and bPrintMemo then Memo1.Lines.Add(SBeg+'������: �� �� �������.')
      end;
      quFindTCard.Active:=False;
      if iTC>0 then
      begin
        QT1:=TpFIBDataSet.Create(Owner);
        QT1.Active:=False;
        QT1.Database:=OfficeRnDb;
        QT1.Transaction:=trSel;
        QT1.SelectSQL.Clear;
        QT1.SelectSQL.Add('SELECT CS.ID,CS.IDCARD,CS.CURMESSURE,CS.NETTO,CS.BRUTTO,CS.KNB,');
        QT1.SelectSQL.Add('CD.NAME,CD.TCARD');
        QT1.SelectSQL.Add('FROM OF_CARDSTSPEC CS');
        QT1.SelectSQL.Add('left join of_cards cd on cd.ID=CS.IDCARD');
        QT1.SelectSQL.Add('where IDC='+IntToStr(iCode)+' and IDT='+IntToStr(iTC));
        QT1.SelectSQL.Add('ORDER BY CS.ID');
        QT1.Active:=True;

        QT1.First;
        while not QT1.Eof do
        begin
          rQ1:=QT1.FieldByName('NETTO').AsFloat;
          kM:=prFindKM(QT1.FieldByName('CURMESSURE').AsInteger);
          rQ1:=rQ1*kM;//��������� � �������� �������
          prFindSM(QT1.FieldByName('CURMESSURE').AsInteger,sM,iMain); //��� �������� �������� ������� ���������
          //���� ������ �� ������ ����
          kBrutto:=prFindBrutto(QT1.FieldByName('IDCARD').AsInteger,iDate);
          rQ1:=rQ1*(100+kBrutto)/100; //��� ����� � ������

          rQ1:=rQ1/iPCount; //��� ����� �� 1-� ������ ���� �������� �� �������
          rQ1:=rQ1*rQbs/MassaB; //��� ����� �� ��� ���-�� ������

          prCalcBl(sBeg,QT1.FieldByName('NAME').AsString,IdSkl,IdPos,QT1.FieldByName('IDCARD').AsInteger,iDate,QT1.FieldByName('TCARD').AsInteger,rQ1,iMain,taSpecB,taCalc,taCalcB,Memo1,bNeedMemo,iSpis,iLevel,iOper);

          QT1.Next;
        end;
        QT1.Active:=False;
        QT1.Free;
      end;
    end;


  end;
end;



{  //������ ����������� ������� ��� ������� ���-�� ��������������

Procedure prCalcBl(sBeg,sName:String;IdSkl,IdPos,iCode,iDate,iTCard:Integer;rQ:Real;IdM:INteger;taSpecB,taCalc,taCalcB:tdxMemData;Memo1:TcxMemo);
Var iTC,iPCount:INteger;
    QT1:TpFIBDataSet;
    rQ1,kBrutto,kM,MassaB:Real;
    iMain:INteger;
    sM:String;
    Par:Variant;
    bCalc:Boolean;

Procedure prCalcSave;
Var rQb:Real;
begin
  if taCalcB.Active = False then taCalcB.Active:=True;
  if taCalc.Active = False then taCalc.Active:=True;

  par := VarArrayCreate([0,1], varInteger);
  par[0]:=IdPos;
  par[1]:=iCode;
  if taCalcB.Locate('ID;IDCARD',par,[]) then
  begin
    rQb:=taCalcB.fieldByName('QUANTC').AsFloat+rQ;
    taCalcB.edit;
    taCalcB.fieldByName('QUANTC').AsFloat:=rQb;
    taCalcB.fieldByName('PRICEIN').AsFloat:=0;
    taCalcB.fieldByName('SUMIN').AsFloat:=0;
    taCalcB.fieldByName('IM').AsInteger:=iMain;
    taCalcB.fieldByName('SM').AsString:=sM;
    taCalcB.fieldByName('SB').AsString:='';
    taCalcB.Post;
  end else
  begin
    taCalcB.Append;
    taCalcB.fieldByName('ID').AsInteger:=IdPos;
    taCalcB.fieldByName('CODEB').AsInteger:=taSpecB.fieldByName('IDCARD').AsInteger;
    taCalcB.fieldByName('NAMEB').AsString:=taSpecB.fieldByName('NAME').AsString;
    taCalcB.fieldByName('QUANT').AsFloat:=taSpecB.fieldByName('QUANT').AsFloat;
    taCalcB.fieldByName('PRICEOUT').AsFloat:=taSpecB.fieldByName('PRICER').AsFloat;
    taCalcB.fieldByName('SUMOUT').AsFloat:=taSpecB.fieldByName('RSUM').AsFloat;
    taCalcB.fieldByName('IDCARD').AsInteger:=iCode;
    taCalcB.fieldByName('NAMEC').AsString:=sName;
    taCalcB.fieldByName('QUANTC').AsFloat:=rQ;
    taCalcB.fieldByName('PRICEIN').AsFloat:=0;
    taCalcB.fieldByName('SUMIN').AsFloat:=0;
    taCalcB.fieldByName('IM').AsInteger:=iMain;
    taCalcB.fieldByName('SM').AsString:=sM;
    taCalcB.fieldByName('SB').AsString:='';
    taCalcB.Post;
  end;

  if taCalc.Locate('Articul',iCode,[]) then
  begin
    rQ:=taCalc.fieldByName('Quant').AsFloat+rQ;
    taCalc.Edit;
    taCalc.fieldByName('Quant').AsFloat:=rQ;
    taCalc.fieldByName('QuantFact').AsFloat:=0;
    taCalc.fieldByName('QuantDiff').AsFloat:=0;
    taCalc.Post;
  end else
  begin
    taCalc.Append;
    taCalc.fieldByName('Articul').AsInteger:=iCode;
    taCalc.fieldByName('Name').AsString:=sName;
    taCalc.fieldByName('Idm').AsInteger:=iMain;
    taCalc.fieldByName('sM').AsString:=sM;
    taCalc.fieldByName('Km').AsFloat:=1;
    taCalc.fieldByName('Quant').AsFloat:=rQ;
    taCalc.fieldByName('QuantFact').AsFloat:=0;
    taCalc.fieldByName('QuantDiff').AsFloat:=0;
    taCalc.fieldByName('SumIn').AsFloat:=0;
    taCalc.fieldByName('SumUch').AsFloat:=0;
    taCalc.Post;
  end;
end;

begin
  sBeg:=sBeg+'    ';

  with dmO do
  begin
    //���
    Memo1.Lines.Add(SBeg+'��� - '+IntToStr(iCode));
    if iTCard=1 then bCalc:=True else bCalc:=False;
    if fCalcBlasCard(IdSkl,iCode) then bCalc:=False; //���� �� ������������ �� ��� �����

    if bCalc then //���� ��� ����� � ��� ���� ������������
    begin
      // ����� ��������� �������, ����� ����, ���� ����� ������� �������



      //����������� ����

      iTC:=0; iPCount:=0; MassaB:=1;
      quFindTCard.Active:=False;
      quFindTCard.ParamByName('IDCARD').AsInteger:=iCode;
      quFindTCard.ParamByName('IDATE').AsDateTime:=iDate;
      quFindTCard.Active:=True;
      if quFindTCard.RecordCount>0 then
      begin
        iTC:=quFindTCardID.AsInteger;
        iPCount:=quFindTCardPCOUNT.AsInteger;
        MassaB:=quFindTCardPVES.AsFloat/1000;
        if prFindMT(IdM)=1 then MassaB:=1;
        prFindSM(IdM,sM,iMain); //��� �������� �������� ������� ���������
        if (iPCount=0) or (MassaB=0) then
        begin
          iTC:=0;
          inc(iErr);
          sErr:=sErr+IntToStr(iCode)+' '+sName+';';
          Memo1.Lines.Add(SBeg+'������: ������������ ��������� � ��.');
        end else
        begin
          Memo1.Lines.Add(sBeg+sName+' ('+INtToStr(iCode)+'). �� ����. ���-��: '+FloatToStr(RoundEx(rQ*1000)/1000)+' '+sM);
        end;
      end else
      begin
        inc(iErr);
        sErr:=sErr+IntToStr(iCode)+' '+sName+';';
        Memo1.Lines.Add(SBeg+'������: �� �� �������.')
      end;
      quFindTCard.Active:=False;
      if iTC>0 then
      begin

        QT1:=TpFIBDataSet.Create(Owner);
        QT1.Active:=False;
        QT1.Database:=OfficeRnDb;
        QT1.Transaction:=trSel;
        QT1.SelectSQL.Clear;
        QT1.SelectSQL.Add('SELECT CS.ID,CS.IDCARD,CS.CURMESSURE,CS.NETTO,CS.BRUTTO,CS.KNB,');
        QT1.SelectSQL.Add('CD.NAME,CD.TCARD');
        QT1.SelectSQL.Add('FROM OF_CARDSTSPEC CS');
        QT1.SelectSQL.Add('left join of_cards cd on cd.ID=CS.IDCARD');
        QT1.SelectSQL.Add('where IDC='+IntToStr(iCode)+' and IDT='+IntToStr(iTC));
        QT1.SelectSQL.Add('ORDER BY CS.ID');
        QT1.Active:=True;

        QT1.First;
        while not QT1.Eof do
        begin
          rQ1:=QT1.FieldByName('NETTO').AsFloat;
          kM:=prFindKM(QT1.FieldByName('CURMESSURE').AsInteger);
          rQ1:=rQ1*kM;//��������� � �������� �������
          prFindSM(QT1.FieldByName('CURMESSURE').AsInteger,sM,iMain); //��� �������� �������� ������� ���������
          //���� ������ �� ������ ����
          kBrutto:=prFindBrutto(QT1.FieldByName('IDCARD').AsInteger,iDate);
          rQ1:=rQ1*(100+kBrutto)/100; //��� ����� � ������

          rQ1:=rQ1/iPCount; //��� ����� �� 1-� ������ ���� �������� �� �������
          rQ1:=rQ1*rQ/MassaB; //��� ����� �� ��� ���-�� ������

//          Memo1.Lines.Add(sBeg+QT1.FieldByName('NAME').AsString+' ('+QT1.FieldByName('IDCARD').AsString+'). �� ����. ���-��: '+FloatToStr(RoundEx(rQ1*1000)/1000)+' '+sM);
          prCalcBl(sBeg,QT1.FieldByName('NAME').AsString,IdSkl,IdPos,QT1.FieldByName('IDCARD').AsInteger,iDate,QT1.FieldByName('TCARD').AsInteger,rQ1,iMain,taSpecB,taCalc,taCalcB,Memo1);

          QT1.Next;
        end;
        QT1.Active:=False;
        QT1.Free;
      end;
    end else
    begin
      //��������� � �������� ������� �� ������ ������ ����� ������ ��� - �� ����
      kM:=prFindKM(IdM);                         
      prFindSM(IdM,sM,iMain); //��� �������� �������� ������� ���������
      rQ:=rQ*kM;              //������ ������� - ��� ������ �� �����������

      Memo1.Lines.Add(sBeg+sName+' ('+IntToStr(iCode)+'). �����.  ���-��: '+FloatToStr(RoundEx(rQ*1000)/1000)+' '+sM);

      //������ ���� ��� � �������� � � ������
      prCalcSave;
    end;
  end;
end;

}

Function GetId(ta:String):Integer;
Var iType:Integer;
begin
  with dmO do
  begin
    iType:=0;
    if ta='Pe' then iType:=1; //��������
    if ta='Cl' then iType:=2; //�������������
    if ta='Trh' then iType:=3; //��������� ������
    if ta='Trs' then iType:=4; //������������ ������
    if ta='TabH' then iType:=5; //��������� ������� (������)
    if ta='CS' then iType:=6; //��������� ������� (������)
    if ta='TH' then iType:=7; //��������� ������� (������) all
    if ta='MESS' then iType:=100; //������� ���������
    if ta='Class' then iType:=101; //������������� ������� � �����
    if ta='MH' then iType:=102; //����� �������� � ������������
    if ta='GD' then iType:=103; //�������
    if ta='Cli' then iType:=104; //�����������
    if ta='DocIn' then iType:=105; //��������� ��������� ����������
    if ta='SpecIn' then iType:=106; //������������ ��������� ����������
    if ta='TCards' then iType:=107; //��������������� �����
    if ta='PartIn' then iType:=108; //��������� ������
    if ta='InvH' then iType:=109; //��������� ��������������
    if ta='InvS' then iType:=110; //������������ ��������������
    if ta='Pars' then iType:=111; //��������� ������
    if ta='DocOut' then iType:=112; //��������� ��������� ����������
    if ta='SpecOut' then iType:=113; //������������ ��������� ����������
    if ta='DocOutB' then iType:=114; //��������� ��������� ���������� ����
    if ta='SpecOutB' then iType:=115; //������������ ��������� ���������� ����
    if ta='SpecOutC' then iType:=116; //������������ ��������� ���������� ��������
    if ta='HeadCompl' then iType:=117; //��������� ������������
    if ta='DocAct' then iType:=118; //��������� ����� �����������
    if ta='DocVn' then iType:=119; //��������� ����������
    if ta='SpecVn' then iType:=120; //��������� ����������
    if ta='DocR' then iType:=121; //��������� ���������� �� �������
    if ta='Makers' then iType:=122; //��������� ���������� �� �������
    if ta='123' then iType:=123; // ��� ��������
    if ta='124' then iType:=124; // ��� ��������� ����������
    if ta='125' then iType:=125; // ��� ���� ���������� 1
    if ta='126' then iType:=126; // ��� ���� ���������� 2
    if ta='UPL' then iType:=127; // ��� ���� ���������� 2
    if ta='DocPr' then iType:=128; // ��� ����
    if ta='ChangeGr' then iType:=133; // ������ ������������������

    prGetId.ParamByName('ITYPE').Value:=iType;
    prGetId.ExecProc;
    result:=prGetId.ParamByName('RESULT').Value;
  end;
end;


procedure TdmO.DataModuleCreate(Sender: TObject);
begin
  OfficeRnDb.Connected:=False;
  OfficeRnDb.DBName:=CommonSet.OfficeDb;
  try
    OfficeRnDb.Open;
  except
  end;
end;

procedure TdmO.DataModuleDestroy(Sender: TObject);
begin
  try
    OfficeRnDb.Close;
  except
  end;
end;

Function CanDo(Name_F:String):Boolean;
begin
  result:=True;
  with dmO do
  begin
    quCanDo.Active:=False;
    quCanDo.ParamByName('Person_id').Value:=Person.Id;
    quCanDo.ParamByName('Name_F').Value:=Name_F;
    quCanDo.Active:=True;
    if quCanDoprExec.AsBoolean=True then
     Result:=False;
  end;
end;

Function CanEdit(IDate,ISKL:Integer):Boolean;
begin
  with dmO do
  begin
    quCanEdit.Active:=False;
    quCanEdit.ParamByName('ISKL').AsInteger:=ISKL;
    quCanEdit.Active:=True;
    Result:=True;
    quCanEdit.First;
    if quCanEdit.RecordCount>0 then
    begin
      if Trunc(quCanEditDATEDOC.AsDateTime)>=IDate then Result:=False;
    end;
  end;
end;



procedure TdmO.quTCardsCalcFields(DataSet: TDataSet);
begin
  if Trunc(quTCardsDATEE.AsDateTime)>=iMaxDate then quTCardsSDATEE.AsString:=' '
  else quTCardsSDATEE.AsString:=FormatDateTime('dd.mm.yyyy',Trunc(quTCardsDATEE.AsDateTime));
end;

procedure TdmO.quMoveSelCalcFields(DataSet: TDataSet);
begin
  quMoveSelItog.AsFloat:=quMoveSelPOSTIN.AsFloat-quMoveSelPOSTOUT.AsFloat+quMoveSelVNIN.AsFloat-quMoveSelVNOUT.AsFloat+quMoveSelINV.AsFloat-quMoveSelQREAL.AsFloat;
end;

procedure TdmO.quSelPartInCalcFields(DataSet: TDataSet);
begin
  quSelPartInSDATE.AsString:=FormatdateTime('dd.mm.yyyy',quSelPartInIDATE.AsInteger);
end;

procedure prSelPartInT(Articul,IdStore,iCli:Integer);
begin
end;

procedure prSelPartIn(Articul,IdStore,iCli:Integer;rPrice:Real);
begin
  with dmO do
  begin
    quSelPartIn.Active:=False;
    quSelpartIn.SelectSQL.Clear;
    quSelPartIn.SelectSQL.Add('SELECT first 15 ID,IDSTORE,IDDOC,ARTICUL,IDCLI,DTYPE,QPART,QREMN,PRICEIN0,PRICEIN,PRICEOUT,IDATE');
    quSelPartIn.SelectSQL.Add('FROM OF_PARTIN');
    if iCli>0 then quSelPartIn.SelectSQL.Add('where QREMN>0.0001 and IDSTORE='+IntToStr(IdStore)+' and ARTICUL='+IntToStr(Articul)+' and IDCLI='+IntToStr(iCli))
              else quSelPartIn.SelectSQL.Add('where QREMN>0.0001 and IDSTORE='+IntToStr(IdStore)+' and ARTICUL='+IntToStr(Articul));
    if rPrice>0 then quSelPartIn.SelectSQL.Add(' and PRICEIN>='+FloatToStr(rPrice-0.001)+' and PRICEIN<='+FloatToStr(rPrice+0.001));
    quSelPartIn.SelectSQL.Add('Order by IDATE');

    quSelPartIn.Active:=true;
  end;
end;

procedure prSelPartInRet(Articul,IdStore,iCli:Integer);
begin
  with dmO do
  begin
    quSelPartIn.Active:=False;
    quSelpartIn.SelectSQL.Clear;
    quSelPartIn.SelectSQL.Add('SELECT first 5 ID,IDSTORE,IDDOC,ARTICUL,IDCLI,DTYPE,QPART,QREMN,PRICEIN0,PRICEIN,PRICEOUT,IDATE');
    quSelPartIn.SelectSQL.Add('FROM OF_PARTIN');
//    if iCli>0 then quSelPartIn.SelectSQL.Add('where QREMN>0 and IDSTORE='+IntToStr(IdStore)+' and ARTICUL='+IntToStr(Articul)+' and IDCLI='+IntToStr(iCli))
//              else quSelPartIn.SelectSQL.Add('where QREMN>0 and IDSTORE='+IntToStr(IdStore)+' and ARTICUL='+IntToStr(Articul));
    if iCli>0 then quSelPartIn.SelectSQL.Add('where IDSTORE='+IntToStr(IdStore)+' and ARTICUL='+IntToStr(Articul)+' and IDCLI='+IntToStr(iCli))
              else quSelPartIn.SelectSQL.Add('where IDSTORE='+IntToStr(IdStore)+' and ARTICUL='+IntToStr(Articul));
    quSelPartIn.SelectSQL.Add('Order by IDATE desc');

    quSelPartIn.Active:=true;
  end;
end;

function prFindM(IdCard:Integer; Var kM:Real;NameM:String):INteger;
begin
  with dmO do
  begin
    kM:=0;
    Result:=0;
    quFindM.Active:=False;
    quFindM.ParamByName('IDC').AsInteger:=IdCard;
    quFindM.Active:=True;
    if quFindM.RecordCount>0 then
    begin
      Result:=quFindMIMESSURE.AsInteger;
      kM:=quFindMKOEF.AsFloat;
      NameM:=quFindMNAMESHORT.AsString;
    end;
    quFindM.Active:=False;
  end;
end;


function prFindMT(Idm:Integer):Integer;
begin
  with dmO do
  begin
    quM.Active:=False;
    quM.ParamByName('IDM').AsInteger:=Idm;
    quM.Active:=True;
    result:=quMITYPE.AsInteger;  //
  end;
end;


function prFindKM(Idm:Integer):Real;
begin
  with dmO do
  begin
    quM.Active:=False;
    quM.ParamByName('IDM').AsInteger:=Idm;
    quM.Active:=True;
    result:=quMKOEF.AsFloat;
  end;
end;

function prFindKNM(Idm:Integer;Var km:Real):String;
begin
  with dmO do
  begin
    quM.Active:=False;
    quM.ParamByName('IDM').AsInteger:=Idm;
    quM.Active:=True;
    Km:=quMKOEF.AsFloat;
    Result:=quMNAMESHORT.AsString;
  end;
end;


function prFindBrutto(IdC:Integer;rDate:TDateTime):Real;
Var iDate:Integer;
begin
  with dmO do
  begin
    Result:=0;
    try
      iDate:=prDateToI(rDate);
      quFindEU.Active:=False;
      quFindEU.ParamByName('GOODSID').AsInteger:=iDc;
      quFindEU.ParamByName('DATEB').AsInteger:=iDate;
      quFindEU.ParamByName('DATEE').AsInteger:=iDate;
      quFindEU.Active:=True;

      if quFindEU.RecordCount>0 then
      begin
        quFindEU.First;
        Result:=quFindEUTO100GRAMM.AsFloat;
      end;
    finally
      quFindEU.Active:=False;
    end;
  end;
end;

Procedure prFindSM(Idm:Integer;Var SM:String; Var IM:Integer); //�������� ��������
begin
  IM:=Idm;
  with dmO do
  begin
    try
      quM.Active:=False;
      quM.ParamByName('IDM').AsInteger:=IM;
      quM.Active:=True;
      if quMID_PARENT.AsInteger=0 then SM:=quMNAMESHORT.asstring
      else begin
        IM:=quMID_PARENT.AsInteger;
        quM.Active:=False;
        quM.ParamByName('IDM').AsInteger:=IM;
        quM.Active:=True;
        SM:=quMNAMESHORT.asstring;
      end;
    finally
      quM.Active:=False;
    end;
  end;
end;

Procedure prFindIMCCode(Idm:Integer;Var SM,MCODE:String); //�������� ������� � ��� �������
begin
  with dmO do
  begin
    try
      SM:='';
      MCODE:='';

      quM.Active:=False;
      quM.ParamByName('IDM').AsInteger:=Idm;
      quM.Active:=True;
      if quM.RecordCount>0 then
      begin
        SM:=quMNAMESHORT.asstring;
        MCODE:=quMCODE.AsString;
      end;
    finally
      quM.Active:=False;
    end;
  end;
end;


Function prCalcRemn(iCode,iDate,iSkl:Integer):Real;
begin
  Result:=0;
  if iSkl=0 then exit;
  with dmO do
  begin
    quRemn.Active:=False;
    quRemn.ParamByName('IDCARD').AsInteger:=iCode;
    quRemn.ParamByName('IDATE').AsInteger:=iDate+1;
    quRemn.ParamByName('ISKL').AsInteger:=iSkl;
    quRemn.Active:=True;
    if quRemn.RecordCount>0 then
    begin
      quRemn.First;
      Result:=quRemnREMN.AsFloat;
    end;
    quRemn.Active:=False;
  end;
end;

Function prCalcRemnSpeed(iCode,iDate,iSkl:Integer;Var rSpeedDay,rRemnDay:Real):Real;
begin
  rSpeedDay:=0;
  rRemnDay:=365;
  with dmO do
  begin
//IDCARD, ?IDSKL, ?DDATE
    prCALCSPEEDREAL.ParamByName('IDCARD').AsInteger:=iCode;
    prCALCSPEEDREAL.ParamByName('IDSKL').AsInteger:=iSkl;
    prCALCSPEEDREAL.ParamByName('DDATE').AsInteger:=iDate;
    prCALCSPEEDREAL.ExecProc;
    Result:=prCALCSPEEDREAL.ParamByName('REMN').AsFloat;
    rSpeedDay:=prCALCSPEEDREAL.ParamByName('RSPEEDDAY').AsFloat;
    rRemnDay:=prCALCSPEEDREAL.ParamByName('RQDAY').AsFloat;
  end;
end;



procedure TdmO.taDobSpecQUANTChange(Sender: TField);
begin
//  taDobSpecRSUM.AsFloat:=taDobSpecQUANT.AsFloat*taDobSpecPRICER.AsFloat;
end;

procedure TdmO.taDobSpecPRICERChange(Sender: TField);
begin
//  taDobSpecRSUM.AsFloat:=taDobSpecQUANT.AsFloat*taDobSpecPRICER.AsFloat;
end;

procedure TdmO.quCRealCalcFields(DataSet: TDataSet);
begin
  //
  quCRealQB.AsFloat:=0;
  if quCRealQUANT.AsFloat<>0 then quCRealQB.AsFloat:=quCRealQUANTC.AsFloat/quCRealQUANT.AsFloat;
end;

procedure TdmO.quCPartInCalcFields(DataSet: TDataSet);
begin
  if quCPartInDTYPE.AsInteger=1 then quCPartInNUMDOC.AsString:=quCPartInNUMDOCIN.AsString;
  if quCPartInDTYPE.AsInteger=3 then quCPartInNUMDOC.AsString:=quCPartInNUMDOCINV.AsString;
  if quCPartInDTYPE.AsInteger=4 then quCPartInNUMDOC.AsString:=quCPartInNUMDOCVN.AsString;
  if quCPartInDTYPE.AsInteger=5 then quCPartInNUMDOC.AsString:=quCPartInNUMDOCACTS.AsString;
  if quCPartInDTYPE.AsInteger=6 then quCPartInNUMDOC.AsString:=quCPartInNUMDOCCOMPL.AsString;
  if quCPartInDTYPE.AsInteger=8 then quCPartInNUMDOC.AsString:=quCPartInNDR.AsString;
end;

procedure TdmO.quCPartOutCalcFields(DataSet: TDataSet);
begin
  if quCPartOutDTYPE.AsInteger=2 then quCPartOutNUMDOC.AsString:=quCPartOutNDOB.AsString;
  if quCPartOutDTYPE.AsInteger=3 then quCPartOutNUMDOC.AsString:=quCPartOutNDI.AsString;
  if quCPartOutDTYPE.AsInteger=4 then quCPartOutNUMDOC.AsString:=quCPartOutNDV.AsString;
  if quCPartOutDTYPE.AsInteger=5 then quCPartOutNUMDOC.AsString:=quCPartOutNDA.AsString;
  if quCPartOutDTYPE.AsInteger=6 then quCPartOutNUMDOC.AsString:=quCPartOutNDC.AsString;
  if quCPartOutDTYPE.AsInteger=7 then quCPartOutNUMDOC.AsString:=quCPartOutNDO.AsString;
  if quCPartOutDTYPE.AsInteger=8 then quCPartOutNUMDOC.AsString:=quCPartOutNDR.AsString;

end;

procedure TdmO.quCardsSelCalcFields(DataSet: TDataSet);
begin
  if CommonSet.CalcRemns=1 then quCardsSelRemn.AsFloat:=prCalcRemn(quCardsSelID.AsInteger,Trunc(Date),CurVal.IdMH1)
  else quCardsSelRemn.AsFloat:=0;
end;

procedure TdmO.quFCardCalcFields(DataSet: TDataSet);
Var S1,S2:String;
begin
  quFCardRemn.AsFloat:=prCalcRemn(quFCardID.AsInteger,Trunc(Date),CurVal.IdMH2);
  prFind2group(quFCardPARENT.AsInteger,S1,S2);
  quFCardGR.AsString:=S1;
  quFCardSGR.AsString:=S2;
end;

procedure TdmO.quFindCalcFields(DataSet: TDataSet);
Var S1,S2:String;
begin
  prFind2group(quFindPARENT.AsInteger,S1,S2);
  quFindGROUP.AsString:=S1;
  quFindSGROUP.AsString:=S2;
end;

procedure TdmO.quBRealCalcFields(DataSet: TDataSet);
begin
  if quBRealSUMIN.AsFloat>0 then quBRealRNAC.AsFloat:=rv(((quBRealSUMOUT.AsFloat-quBRealSUMIN.AsFloat)/quBRealSUMIN.AsFloat)*100)
  else quBRealRNAC.AsFloat:=0;
end;

procedure TdmO.quCPartIn1CalcFields(DataSet: TDataSet);
begin
  if quCPartIn1DTYPE.AsInteger=1 then quCPartIn1NUMDOC.AsString:=quCPartIn1NUMDOCIN.AsString;
  if quCPartIn1DTYPE.AsInteger=3 then quCPartIn1NUMDOC.AsString:=quCPartIn1NUMDOCINV.AsString;
  if quCPartIn1DTYPE.AsInteger=4 then quCPartIn1NUMDOC.AsString:=quCPartIn1NUMDOCVN.AsString;
  if quCPartIn1DTYPE.AsInteger=5 then quCPartIn1NUMDOC.AsString:=quCPartIn1NUMDOCACTS.AsString;
  if quCPartIn1DTYPE.AsInteger=6 then quCPartIn1NUMDOC.AsString:=quCPartIn1NUMDOCCOMPL.AsString;
  if quCPartIn1DTYPE.AsInteger=8 then quCPartIn1NUMDOC.AsString:=quCPartIn1NDR.AsString;
end;

end.
