unit dmRecalcFm;

interface

uses
  SysUtils, Classes, FIBDatabase, pFIBDatabase, DB, FIBDataSet,
  pFIBDataSet, FIBQuery, pFIBQuery, pFIBStoredProc;

type
  TdmRecalc = class(TDataModule)
    prDelPer: TpFIBStoredProc;
    prTestPart: TpFIBStoredProc;
    quDIn: TpFIBDataSet;
    quDInID: TFIBIntegerField;
    quDInIDCLI: TFIBIntegerField;
    quDInIDSKL: TFIBIntegerField;
    quDInSUMIN: TFIBFloatField;
    quDInSUMUCH: TFIBFloatField;
    quDInSUMTAR: TFIBFloatField;
    quDInSUMNDS0: TFIBFloatField;
    quDInSUMNDS1: TFIBFloatField;
    quDInSUMNDS2: TFIBFloatField;
    quDInPROCNAC: TFIBFloatField;
    quDInIACTIVE: TFIBIntegerField;
    quDInv: TpFIBDataSet;
    quDInvID: TFIBIntegerField;
    quDInvDATEDOC: TFIBDateField;
    quDInvNUMDOC: TFIBStringField;
    quDInvIDSKL: TFIBIntegerField;
    quDInvIACTIVE: TFIBIntegerField;
    quDInvITYPE: TFIBIntegerField;
    quDInvSUM1: TFIBFloatField;
    quDInvSUM11: TFIBFloatField;
    quDInvSUM2: TFIBFloatField;
    quDInvSUM21: TFIBFloatField;
    trS: TpFIBTransaction;
    prSetSpecActive: TpFIBStoredProc;
    quDCompl: TpFIBDataSet;
    quDComplID: TFIBIntegerField;
    quDComplDATEDOC: TFIBDateField;
    quDComplNUMDOC: TFIBStringField;
    quDComplIDSKL: TFIBIntegerField;
    quDComplSUMIN: TFIBFloatField;
    quDComplSUMUCH: TFIBFloatField;
    quDComplSUMTAR: TFIBFloatField;
    quDComplPROCNAC: TFIBFloatField;
    quDComplIACTIVE: TFIBIntegerField;
    quDComplOPER: TFIBStringField;
    quDComplIDSKLTO: TFIBIntegerField;
    quDR: TpFIBDataSet;
    quDRID: TFIBIntegerField;
    quDRDATEDOC: TFIBDateField;
    quDRNUMDOC: TFIBStringField;
    quDRDATESF: TFIBDateField;
    quDRNUMSF: TFIBStringField;
    quDRIDCLI: TFIBIntegerField;
    quDRIDSKL: TFIBIntegerField;
    quDRSUMIN: TFIBFloatField;
    quDRSUMUCH: TFIBFloatField;
    quDRSUMTAR: TFIBFloatField;
    quDRSUMNDS0: TFIBFloatField;
    quDRSUMNDS1: TFIBFloatField;
    quDRSUMNDS2: TFIBFloatField;
    quDRPROCNAC: TFIBFloatField;
    quDRIACTIVE: TFIBIntegerField;
    trD: TpFIBTransaction;
    quDAct: TpFIBDataSet;
    quDActID: TFIBIntegerField;
    quDActDATEDOC: TFIBDateField;
    quDActNUMDOC: TFIBStringField;
    quDActIDSKL: TFIBIntegerField;
    quDActSUMIN: TFIBFloatField;
    quDActSUMUCH: TFIBFloatField;
    quDActIACTIVE: TFIBIntegerField;
    quDActOPER: TFIBStringField;
    quDActCOMMENT: TFIBStringField;
    trU: TpFIBTransaction;
    quMHAll1: TpFIBDataSet;
    quMHAll1ID: TFIBIntegerField;
    quMHAll1PARENT: TFIBIntegerField;
    quMHAll1ITYPE: TFIBIntegerField;
    quMHAll1NAMEMH: TFIBStringField;
    quMHAll1DEFPRICE: TFIBIntegerField;
    quMHAll1NAMEPRICE: TFIBStringField;
    quDVn: TpFIBDataSet;
    quDVnID: TFIBIntegerField;
    quDVnDATEDOC: TFIBDateField;
    quDVnNUMDOC: TFIBStringField;
    quDVnIDSKL_FROM: TFIBIntegerField;
    quDVnIDSKL_TO: TFIBIntegerField;
    quDVnSUMIN: TFIBFloatField;
    quDVnSUMUCH: TFIBFloatField;
    quDVnSUMUCH1: TFIBFloatField;
    quDVnSUMTAR: TFIBFloatField;
    quDVnPROCNAC: TFIBFloatField;
    quDVnIACTIVE: TFIBIntegerField;
    trS1: TpFIBTransaction;
    PRNORMPARTIN: TpFIBStoredProc;
    quDRet: TpFIBDataSet;
    quDRetID: TFIBIntegerField;
    quDRetDATEDOC: TFIBDateField;
    quDRetNUMDOC: TFIBStringField;
    quDRetDATESF: TFIBDateField;
    quDRetNUMSF: TFIBStringField;
    quDRetIDCLI: TFIBIntegerField;
    quDRetIDSKL: TFIBIntegerField;
    quDRetSUMIN: TFIBFloatField;
    quDRetSUMUCH: TFIBFloatField;
    quDRetSUMTAR: TFIBFloatField;
    quDRetSUMNDS0: TFIBFloatField;
    quDRetSUMNDS1: TFIBFloatField;
    quDRetSUMNDS2: TFIBFloatField;
    quDRetPROCNAC: TFIBFloatField;
    quDRetIACTIVE: TFIBIntegerField;
    quC: TpFIBDataSet;
    quCID: TFIBIntegerField;
    quDOutB: TpFIBDataSet;
    quDOutBID: TFIBIntegerField;
    quDOutBDATEDOC: TFIBDateField;
    quDOutBNUMDOC: TFIBStringField;
    quDOutBDATESF: TFIBDateField;
    quDOutBNUMSF: TFIBStringField;
    quDOutBIDCLI: TFIBIntegerField;
    quDOutBIDSKL: TFIBIntegerField;
    quDOutBSUMIN: TFIBFloatField;
    quDOutBSUMUCH: TFIBFloatField;
    quDOutBSUMTAR: TFIBFloatField;
    quDOutBSUMNDS0: TFIBFloatField;
    quDOutBSUMNDS1: TFIBFloatField;
    quDOutBSUMNDS2: TFIBFloatField;
    quDOutBPROCNAC: TFIBFloatField;
    quDOutBIACTIVE: TFIBIntegerField;
    quDOutBOPER: TFIBStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmRecalc: TdmRecalc;

implementation

{$R *.dfm}

end.
