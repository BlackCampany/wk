unit TBuff;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, Menus, cxLookAndFeelPainters, StdCtrls,
  cxButtons, cxCalendar, cxImageComboBox;

type
  TfmTBuff = class(TForm)
    ViewTH: TcxGridDBTableView;
    LevelTH: TcxGridLevel;
    GridTH: TcxGrid;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    LevelTS: TcxGridLevel;
    ViewTS: TcxGridDBTableView;
    ViewTHDATEB: TcxGridDBColumn;
    ViewTHDATEE: TcxGridDBColumn;
    ViewTHSHORTNAME: TcxGridDBColumn;
    ViewTHRECEIPTNUM: TcxGridDBColumn;
    ViewTHPOUTPUT: TcxGridDBColumn;
    ViewTHPCOUNT: TcxGridDBColumn;
    ViewTHPVES: TcxGridDBColumn;
    ViewTSIDC: TcxGridDBColumn;
    ViewTSIDT: TcxGridDBColumn;
    ViewTSID: TcxGridDBColumn;
    ViewTSIDCARD: TcxGridDBColumn;
    ViewTSCURMESSURE: TcxGridDBColumn;
    ViewTSNETTO: TcxGridDBColumn;
    ViewTSBRUTTO: TcxGridDBColumn;
    ViewTSKNB: TcxGridDBColumn;
    LevelD: TcxGridLevel;
    ViewD: TcxGridDBTableView;
    LevelDSpec: TcxGridLevel;
    ViewDSpec: TcxGridDBTableView;
    ViewDIType: TcxGridDBColumn;
    ViewDId: TcxGridDBColumn;
    ViewDDateDoc: TcxGridDBColumn;
    ViewDNumDoc: TcxGridDBColumn;
    ViewDIdCli: TcxGridDBColumn;
    ViewDNameCli: TcxGridDBColumn;
    ViewDIdSkl: TcxGridDBColumn;
    ViewDNameSkl: TcxGridDBColumn;
    ViewDSumIN: TcxGridDBColumn;
    ViewDSumUch: TcxGridDBColumn;
    ViewDSpecIType: TcxGridDBColumn;
    ViewDSpecIdHead: TcxGridDBColumn;
    ViewDSpecNum: TcxGridDBColumn;
    ViewDSpecIdCard: TcxGridDBColumn;
    ViewDSpecQuant: TcxGridDBColumn;
    ViewDSpecPriceIn: TcxGridDBColumn;
    ViewDSpecSumIn: TcxGridDBColumn;
    ViewDSpecPriceUch: TcxGridDBColumn;
    ViewDSpecSumUch: TcxGridDBColumn;
    ViewDSpecIdNds: TcxGridDBColumn;
    ViewDSpecSumNds: TcxGridDBColumn;
    ViewDSpecNameC: TcxGridDBColumn;
    ViewDSpecSm: TcxGridDBColumn;
    ViewDSpecIdM: TcxGridDBColumn;
    ViewTSNAME: TcxGridDBColumn;
    procedure cxButton4Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmTBuff: TfmTBuff;

implementation

uses TCard, DMOReps, Un1;

{$R *.dfm}

procedure TfmTBuff.cxButton4Click(Sender: TObject);
begin
  //�������
  if LevelTH.Visible then
  begin
    with fmTCard do
    begin
      TCS.First;
      while not TCS.Eof do
      begin
        if TCSIDT.AsInteger=TCHID.AsInteger then TCS.Delete
        else TCS.Next;
      end;
      if not TCH.Eof then TCH.Delete;
    end;
  end;
  if LevelD.Visible then
  begin
    with dmORep do
    begin
      taSpecDoc.First;
      while not taSpecDoc.Eof do
      begin
        if (taSpecDocIType.AsInteger=taHeadDocIType.AsInteger)and(taSpecDocIdHead.AsInteger=taHeadDocId.AsInteger) then taSpecDoc.Delete
        else taSpecDoc.Next;
      end;

      if taSpecDoc1.Active=False then
      begin
      // taSpecDoc1.Active:=False;  //��� �������
        taSpecDoc1.FileName:=CurDir+'SpecDoc1.cds';
        if FileExists(CurDir+'SpecDoc1.cds') then taSpecDoc1.Active:=True
        else taSpecDoc1.CreateDataSet;
      end;

      taSpecDoc1.First;
      while not taSpecDoc1.Eof do
      begin
        if (taSpecDoc1IType.AsInteger=taHeadDocIType.AsInteger)and(taSpecDoc1IdHead.AsInteger=taHeadDocId.AsInteger) then taSpecDoc1.Delete
        else taSpecDoc1.Next;
      end;

      if not taHeadDoc.Eof then taHeadDoc.Delete;

    end;
  end;
end;

procedure TfmTBuff.cxButton3Click(Sender: TObject);
begin
//��������
  if LevelTH.Visible then
  begin
    with fmTCard do
    begin
      // ��������
      TCH.First;
      TCS.First;
      while not TCS.Eof do TCS.Delete;
      while not TCH.Eof do TCH.Delete;
    end;
  end;
  if LevelD.Visible then
  begin
    with dmORep do
    begin
      // ��������
      taHeadDoc.First;
      taSpecDoc.First;
      while not taSpecDoc.Eof do taSpecDoc.Delete;
      while not taHeadDoc.Eof do taHeadDoc.Delete;

      if taSpecDoc1.Active=False then
      begin
      // taSpecDoc1.Active:=False;  //��� �������
        taSpecDoc1.FileName:=CurDir+'SpecDoc1.cds';
        if FileExists(CurDir+'SpecDoc1.cds') then taSpecDoc1.Active:=True
        else taSpecDoc1.CreateDataSet;
      end;
      taSpecDoc1.First;
      while not taSpecDoc1.Eof do taSpecDoc1.Delete;

    end;
  end;
end;

end.
