object fmDrivers: TfmDrivers
  Left = 483
  Top = 427
  BorderStyle = bsDialog
  Caption = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082' "'#1042#1086#1076#1080#1090#1077#1083#1080'"'
  ClientHeight = 324
  ClientWidth = 766
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 305
    Width = 766
    Height = 19
    Panels = <
      item
        Width = 300
      end
      item
        Width = 50
      end>
  end
  object GrDrv: TcxGrid
    Left = 8
    Top = 64
    Width = 741
    Height = 233
    TabOrder = 1
    LookAndFeel.Kind = lfOffice11
    object ViewDrv: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dmORep.dsquDrv
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnGrouping = False
      OptionsCustomize.ColumnMoving = False
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      object ViewDrvID: TcxGridDBColumn
        Caption = #1042#1085'. '#1082#1086#1076
        DataBinding.FieldName = 'ID'
        Visible = False
        Width = 36
      end
      object ViewDrvFIO: TcxGridDBColumn
        Caption = #1060#1048#1054
        DataBinding.FieldName = 'FIO'
        Width = 117
      end
      object ViewDrvNAME1: TcxGridDBColumn
        Caption = #1060#1072#1084#1080#1083#1080#1103
        DataBinding.FieldName = 'NAME1'
        Width = 101
      end
      object ViewDrvNAME2: TcxGridDBColumn
        Caption = #1048#1084#1103
        DataBinding.FieldName = 'NAME2'
        Width = 100
      end
      object ViewDrvNAME3: TcxGridDBColumn
        Caption = #1054#1090#1095#1077#1089#1090#1074#1086
        DataBinding.FieldName = 'NAME3'
        Width = 107
      end
      object ViewDrvSDOC: TcxGridDBColumn
        Caption = #1044#1086#1082#1091#1084#1077#1085#1090' '#1089#1077#1088#1080#1103' '#8470
        DataBinding.FieldName = 'SDOC'
        Width = 81
      end
      object ViewDrvCARNAME: TcxGridDBColumn
        Caption = #1052#1072#1088#1082#1072' '#1072#1074#1090#1086#1084#1086#1073#1080#1083#1103
        DataBinding.FieldName = 'CARNAME'
        Width = 82
      end
      object ViewDrvCARNUM: TcxGridDBColumn
        Caption = #1043#1086#1089'.'#1085#1086#1084#1077#1088' '#1072#1074#1090#1086#1084#1086#1073#1080#1083#1103
        DataBinding.FieldName = 'CARNUM'
        Width = 82
      end
    end
    object LevelDrv: TcxGridLevel
      GridView = ViewDrv
    end
  end
  object SpeedBar1: TSpeedBar
    Left = 0
    Top = 0
    Width = 766
    Height = 46
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Options = [sbAllowDrag, sbFlatBtns, sbTransparentBtns]
    BtnOffsetHorz = 4
    BtnOffsetVert = 4
    BtnWidth = 60
    BtnHeight = 40
    Images = dmO.imState
    BevelInner = bvLowered
    Color = 14286809
    TabOrder = 2
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object SpeedItem1: TSpeedItem
      Action = acAddDrv
      BtnCaption = #1044#1086#1073#1072#1074#1080#1090#1100
      Caption = 'SpeedItem1'
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1500100010001000150015001000
        10001F7C1F7C1F7C1F7C1F7C1F7C1F7C15007F2D1F00BF147F2DBF147F2D1F00
        7F2D10001F7C1F7C1F7C1F7C1F7C1F7C10007F2D1F00BF141F001F001F21BF14
        1F0015001F7C1F7C1F7C1F7C1F7C1F7C10001F001F001F00BF147F2DBF147F2D
        BF1415001F7C1F7C1F7C1F7C1F7C1F7C10007F2DBF147F2D1F001F001F00BF14
        BF1410001F7C1F7C1F7C1F7C1F7C1F7C15001F001F001F00BF14BF141F007F2D
        7F2D15001F7C1F7C1F7C1F7C1F7C1F7C7F2D1F001F001F00BF141F001F001F00
        BF1410001F7C1F7C1F7C6003C001E00060037F2DBF141F001F001F21BF14BF14
        7F2D10001F7C1F7C1F7CE000F75FF75FE0001F001F00BF141F00BF14BF14BF14
        BF1415001F7C6003C001C001F75FF75FC001E00060037F2DBF147F2DBF147F2D
        7F2D10001F7CC001E313F75FF75FF75FF75F6003E0007F2D1500100010001000
        15001F7C1F7CC001FF67FF67E313F75FE313E313C0011F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7CE31360036003ED3BE313C001C001E3131F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C6003ED3BED3BC0011F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7CED3BE313E313ED3B1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      Spacing = 1
      Left = 24
      Top = 4
      Visible = True
      OnClick = acAddDrvExecute
      SectionName = 'Untitled (0)'
    end
    object SpeedItem2: TSpeedItem
      Action = acEditDrv
      BtnCaption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100
      Caption = 'SpeedItem2'
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C00001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C000000001F7C1F7C1F7C1000100010001000150015001000
        10001F7C1F7C1F7C0000000000001F7C15007F2D1F007F2D7F2DBF147F2D1F00
        7F2D10001F7C1F7C42082129C241000015001F001F001F21BF141F001F21BF14
        1F0015001F7C1F7C10426277036FC56A4A291F007F2D1F00BF147F2DBF147F2D
        BF1415001F7C1F7CD65AA17B4277E46E00001F001F007F2D1F001F001F00BF14
        BF1410001F7C1F7C1F7C1042817B2373C46A4A291F007F2DBF14BF141F007F2D
        7F2D15001F7C1F7C1F7CD65AC07F6277036F00001F001F00BF141F001F001F00
        BF1410001F7C1F7C1F7C1F7C1042A17B4277E46E4A297F2D1F001F21BF14BF14
        7F2D10001F7C1F7C1F7C1F7CD65AE07F817B237300001F001F00BF14BF14BF14
        BF1415001F7C1F7C1F7C1F7C1F7C1042C07F000000000000BF14BF14BF141F00
        1F0010001F7C1F7C1F7C1F7C1F7CD65ABD77D65A104200001000100010001000
        15001F7C1F7C1F7C1F7C1F7C1F7C1F7C10423967524A10424A291F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7CD65ABD77D65A104200001F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1042104210421F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      Spacing = 1
      Left = 84
      Top = 4
      Visible = True
      OnClick = acEditDrvExecute
      SectionName = 'Untitled (0)'
    end
    object SpeedItem3: TSpeedItem
      Action = acDelDrv
      BtnCaption = #1059#1076#1072#1083#1080#1090#1100
      Caption = 'SpeedItem3'
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1500100010001000150015001000
        10001F7C1F7C1F7C1F7C1F7C1F7C1F7C15007F2D1F00BF147F2DBF147F2D1F00
        7F2D10001F7C1F7C1F7C1F7C1F7C1F7C10007F2D1F00BF141F001F001F21BF14
        1F0015001F7C1F7C1F7C1F7C1F7C1F7C10001F001F001F00BF147F2DBF147F2D
        BF1415001F7C1F7C1F7C1F7C1F7C1F7C10007F2DBF147F2D1F001F001F00BF14
        BF1410001F7C1F7C1F7C1F7C1F7C1F7C10001F001F001F00BF14BF141F007F2D
        7F2D15001F7C1F7C1F7C1F7C1F7C1F7C10007F2D1F001F00BF141F001F001F00
        BF1410001F7C1F7C1F7C1F7C1F7C1F7C15001F00BF141F001F001F21BF14BF14
        7F2D10001F7C1F7C1F7C007C007CC67CC67CC67CCE7DBF141F00BF14BF14BF14
        BF1415001F7C1F7C0048C67CCE7DB57EB57EB57ECE7D007CBF147F2DBF147F2D
        7F2D10001F7C1F7C007CCE7DB57EB57EB57ECE7DC67C00481000100010001000
        15001F7C1F7C1F7C1F7CCE7DC67CC67CC67C007C007C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      Spacing = 1
      Left = 184
      Top = 4
      Visible = True
      OnClick = acDelDrvExecute
      SectionName = 'Untitled (0)'
    end
    object SpeedItem4: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Glyph.Data = {
        5E040000424D5E04000000000000360000002800000012000000130000000100
        18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
        8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
        0000CED3D6848684848684848684848684848684848684848684848684848684
        848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
        7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
        FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
        FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
        007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
        FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
        00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
        494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
        00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000}
      Hint = #1042#1099#1093#1086#1076'|'
      Spacing = 1
      Left = 284
      Top = 4
      Visible = True
      OnClick = SpeedItem4Click
      SectionName = 'Untitled (0)'
    end
  end
  object amDrv: TActionManager
    Images = dmO.imState
    Left = 292
    Top = 112
    StyleName = 'XP Style'
    object acAddDrv: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ImageIndex = 32
      ShortCut = 45
      OnExecute = acAddDrvExecute
    end
    object acEditDrv: TAction
      Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100
      ImageIndex = 30
      ShortCut = 115
      OnExecute = acEditDrvExecute
    end
    object acDelDrv: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ImageIndex = 31
      ShortCut = 16430
      OnExecute = acDelDrvExecute
    end
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 3000
    OnTimer = Timer1Timer
    Left = 200
    Top = 120
  end
end
