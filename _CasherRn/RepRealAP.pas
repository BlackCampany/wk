unit RepRealAP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxControls,
  cxGridCustomView, cxGrid, cxContainer, cxTextEdit, cxMemo, ExtCtrls,
  ComCtrls, JvSpeedbar, JvExExtCtrls, JvExtComponent, Menus
  ,Un1
  ,ADODB, ActnList, XPStyleActnCtrls, ActnMan, FR_DSet, FR_DBSet, FR_Class;

type
     TDelta = record
     ColB,ColE:SmallInt;
     end;

  TfmRepSaleAP = class(TForm)
    SpeedBar1: TJvSpeedBar;
    SpeedbarSection1: TJvSpeedBarSection;
    SpeedItem1: TJvSpeedItem;
    SpeedItem2: TJvSpeedItem;
    SpeedItem3: TJvSpeedItem;
    SpeedItem4: TJvSpeedItem;
    SpeedItem5: TJvSpeedItem;
    SpeedItem6: TJvSpeedItem;
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Memo1: TcxMemo;
    GridRepSaleAP: TcxGrid;
    ViewRepSaleAP: TcxGridDBTableView;
    LevelRepSaleAP: TcxGridLevel;
    ViewRepSaleAPIDSKL: TcxGridDBColumn;
    ViewRepSaleAPDATEDOC: TcxGridDBColumn;
    ViewRepSaleAPARTICUL: TcxGridDBColumn;
    ViewRepSaleAPBARCODE: TcxGridDBColumn;
    ViewRepSaleAPNAME: TcxGridDBColumn;
    ViewRepSaleAPVOL: TcxGridDBColumn;
    ViewRepSaleAPALGCLASS: TcxGridDBColumn;
    ViewRepSaleAPQUANT: TcxGridDBColumn;
    frRepAP: TfrReport;
    frdsSpecAP: TfrDBDataSet;
    frdsSpecAPItog: TfrDBDataSet;
    procedure FormCreate(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
    procedure SpeedItem4Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }

    iDateB,iDateE:INteger;
    iDep,iDateRep,IDREP:Integer;
    dDateB,dDateE:TDateTime;
  end;

var
  fmRepSaleAP: TfmRepSaleAP;

implementation

uses DMOReps;


{$R *.dfm}

procedure TfmRepSaleAP.FormCreate(Sender: TObject);
begin
  GridRepSaleAP.Align:=alClient;
end;

procedure TfmRepSaleAP.SpeedItem1Click(Sender: TObject);
begin
  close;
end;

procedure TfmRepSaleAP.SpeedItem3Click(Sender: TObject);
begin
  prNExportExel5(ViewRepSaleAP);
end;

procedure TfmRepSaleAP.SpeedItem4Click(Sender: TObject);
//var iDep:Integer;
Var  rOrg:TOrg;
begin
  //������

  with dmORep do
  begin
    if quRepRealAP.RecordCount>0 then
    begin
      iDep:=fmRepSaleAP.iDep;

      rOrg:=prGetOrg(iDep,iDep);

      fmRepSaleAP.Memo1.Clear;
      fmRepSaleAP.Memo1.Lines.Add('����� ... ���� ������������ ������.'); delay(10);

      quRepRealItog.Active:=False;
      quRepRealItog.ParamByName('ISKL').AsInteger:=fmRepSaleAP.iDep;
      quRepRealItog.ParamByName('DDATEB').AsDateTime:=fmRepSaleAP.dDateB;
      quRepRealItog.ParamByName('DDATEE').AsDateTime:=fmRepSaleAP.dDateE;
      quRepRealItog.Active:=True;

      fmRepSaleAP.Memo1.Lines.Add('������������ ��'); delay(10);

      frRepAP.LoadFromFile('RepRealAP.frf');

      frVariables.Variable['NameOrg']:=rOrg.FullName;
      frVariables.Variable['InnKpp']:=rOrg.Inn+'/'+rOrg.KPP;
      frVariables.Variable['Adress']:=rOrg.Sity+', '+rOrg.Street+', '+rOrg.House;
      frVariables.Variable['Info']:=rOrg.Name+' (���/��� '+rOrg.Inn+'/'+rOrg.KPP+'). ����� '+rOrg.Sity+', '+rOrg.Street+', '+rOrg.House;

      ViewRepSaleAP.BeginUpdate;
      frRepAP.PrepareReport;
      ViewRepSaleAP.EndUpdate;

      quRepRealItog.Active:=False;

      frRepAP.ShowPreparedReport;
    end;
  end;
end;

procedure TfmRepSaleAP.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin

  with dmORep do
  begin
    quRepRealAP.Close;
  end;

end;

end.
