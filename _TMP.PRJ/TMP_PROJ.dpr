// JCL_DEBUG_EXPERT_DELETEMAPFILE ON
program TMP_PROJ;

uses
  Vcl.Forms,
  Main in 'Main.pas' {fmMain},
  Shared_MDITaskBar in '..\SharedUnits\Shared_MDITaskBar.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
//  Application.CreateForm(TdmR, dmR);
  Application.CreateForm(TfmMain, fmMain);
  Application.Run;
end.
