unit Main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  dxRibbonForm, IniFiles, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, dxRibbon, dxBar, cxClasses, Vcl.PlatformDefaultStyleActnCtrls, System.Actions, Vcl.ActnList, Vcl.ActnMan,
  Shared_MDITaskBar,
  dxRibbonBackstageView, Vcl.ExtCtrls, dxStatusBar, dxRibbonStatusBar, Vcl.Menus, cxContainer, cxEdit, cxSplitter, cxTextEdit, cxMemo,
  Vcl.StdCtrls, cxButtons, JvFormPlacement, JvComponentBase, JvAppStorage, JvAppIniStorage, cxDBLookupComboBox, cxBarEditItem, cxCheckBox;

type
  TfmMain = class(TdxRibbonForm)
    ActionManager: TActionManager;
    dxBarManager: TdxBarManager;
    dxBarGroup1: TdxBarGroup;
    dxRibbon: TdxRibbon;
    rtMainTab: TdxRibbonTab;
    acClose: TAction;
    brbtnClose: TdxBarLargeButton;
    dxRibbonBackstageView: TdxRibbonBackstageView;
    brQuickMenu: TdxBar;
    StatusBar1: TdxRibbonStatusBar;
    pnlMDITaskBar: TPanel;
    PanelLog: TPanel;
    PanelCloseLog: TPanel;
    btnCloseLog: TcxButton;
    MemoLogGlobal: TcxMemo;
    SplitterBottom: TcxSplitter;
    acViewRar: TAction;
    dxBarLargeButton1: TdxBarLargeButton;
    MainAppIniFileStorage: TJvAppIniFileStorage;
    JvFormStorage: TJvFormStorage;
    dxBarManagerBar1: TdxBar;
    dxBarManagerBar2: TdxBar;
    acUTMCheck: TAction;
    dxBarLargeButton2: TdxBarLargeButton;
    acQBarcode: TAction;
    dxBarLargeButton3: TdxBarLargeButton;
    acCards: TAction;
    dxBarLargeButton4: TdxBarLargeButton;
    acDocIn: TAction;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarManagerBar3: TdxBar;
    dxBarEdit1: TdxBarEdit;
    beiPointMF: TcxBarEditItem;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    acDocRet: TAction;
    dxBarLargeButton9: TdxBarLargeButton;
    dxBarLargeButton10: TdxBarLargeButton;
    dxBarLargeButton11: TdxBarLargeButton;
    dxBarLargeButton12: TdxBarLargeButton;
    dxBarLargeButton13: TdxBarLargeButton;
    procedure acCloseExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure dxBarManagerMerge(Sender, ChildBarManager: TdxBarManager; AddItems: Boolean);
    procedure dxRibbonTabChanged(Sender: TdxCustomRibbon);
  private
    { Private declarations }
    MDITaskBar: TMDITaskBar;
    FTabSelect: Integer;
  public
    { Public declarations }
  end;

var
  fmMain: TfmMain;
  sVer: String;    //������ ����� �� Project/Options.../Version Info/ProgramVersion
  constMainRibbonTabsCount: Integer;  //��������� ���������� Ribbon ������� �� Merge

Const CurIni :String = 'Profiles.ini';
      GridIni:String = 'ProfilesGr.ini';
      FormIni:String = 'ProfilesFr.ini';

procedure ReadIni;
procedure ShowMessageLog(text:string);
procedure ClearMessageLog;


implementation


{$R *.dfm}

procedure ClearMessageLog;
begin
  fmMain.MemoLogGlobal.Clear;
end;

procedure ShowMessageLog(text:string);
begin
  if Assigned(fmMain)=false then Exit;

  if fmMain.PanelLog.Visible=false then begin
    fmMain.PanelLog.Visible:=true;
    fmMain.SplitterBottom.Visible:=true;
    fmMain.SplitterBottom.Top:=0;
    if fmMain.PanelLog.Height=0 then fmMain.PanelLog.Height:=100;
    fmMain.PanelLog.Refresh;
    Application.ProcessMessages;
  end;
  fmMain.MemoLogGlobal.Lines.Append(TimeToStr(now)+'> '+text);
end;


procedure ReadIni;
Var f:TIniFile;
begin
{  f:=TIniFile.create(CurDir+CurIni);

  CommonSet.TmpDir:=f.ReadString('Config','TmpDir',CurDir+'TMP\');
  f.WriteString('Config','TmpDir',CommonSet.TmpDir);

  CommonSet.ReportsDir:=f.ReadString('Config','ReportsDir',CurDir+'Reports\');
  f.WriteString('Config','ReportsDir',CommonSet.ReportsDir);

  CommonSet.IPUTM:=f.ReadString('Config','IPUTM','localhost');
  f.WriteString('Config','IPUTM',CommonSet.IPUTM);

  CommonSet.FSRAR_ID:=Trim(f.ReadString('Config','FSRAR_ID','0'));
  f.WriteString('Config','FSRAR_ID',CommonSet.FSRAR_ID);

  CommonSetTR.PathHistory:=f.ReadString('Config','PathHistory',CurDir+'History\');
  f.WriteString('Config','PathHistory',CommonSetTr.PathHistory);

  CommonSetTr.PeriodSec:=f.ReadInteger('Config','PeriodSec',180);
  f.WriteInteger('Config','PeriodSec',CommonSetTr.PeriodSec);

  CommonSetTr.writelog:=f.ReadInteger('Config','writelog',1);
  f.WriteInteger('Config','writelog',CommonSetTR.writelog);

  f.Free;}
end;

procedure TfmMain.acCloseExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmMain.dxBarManagerMerge(Sender, ChildBarManager: TdxBarManager; AddItems: Boolean);
var i: Integer;
  procedure DisableRefreshChildForms;
  //var i: integer;
  begin
    SendMessage(fmMain.ClientHandle, WM_SETREDRAW, WPARAM(False), 0);  //��������� ��������� �������� ����
    //for i := 0 to MDIChildCount-1 do begin
    //  SendMessage(MDIChildren[i].Handle, WM_SETREDRAW, WPARAM(False), 0);  //��������� ��������� ����
    //end;
  end;
  procedure EnableRefreshChildForms;
  //var i: integer;
  begin
    SendMessage(fmMain.ClientHandle, WM_SETREDRAW, WPARAM(True), 0);  //�������� ��������� �������� ����
    RedrawWindow(fmMain.ClientHandle, nil, 0, RDW_FRAME or RDW_INVALIDATE or RDW_ALLCHILDREN or RDW_NOINTERNALPAINT);
    //fmMain.Refresh;
    //for i := 0 to MDIChildCount-1 do begin
    //  SendMessage(MDIChildren[i].Handle, WM_SETREDRAW, WPARAM(True), 0);  //�������� ��������� ����
    //  RedrawWindow(MDIChildren[i].Handle, nil, 0, RDW_FRAME or RDW_INVALIDATE or RDW_ALLCHILDREN or RDW_NOINTERNALPAINT);
    //end;
  end;
begin
  i:=TdxRibbon(self.RibbonControl).Tabs.Count;

  TdxRibbon(self.RibbonControl).BeginUpdate;
  DisableRefreshChildForms;
  try
    //Merge - Unmerge
    if AddItems then begin
      //Merge
      //ShowMessageLog('Merge');
      if Assigned(ChildBarManager) then begin
        try
          if TdxRibbon(self.RibbonControl).Tabs.Count=constMainRibbonTabsCount then  //���� ������ �� Merged, �� Merge
            Sender.Merge(ChildBarManager,True);
        except
          //���� ��� �������� ������ ��� ��� Merged
        end;
        //AfterMerge
        if(TdxRibbon(self.RibbonControl).Tabs.Count>i) then
        begin
          try
            if TdxRibbon(self.RibbonControl).Tabs[i].Active=False then     //���� �� �������, �� ��������
              TdxRibbon(self.RibbonControl).Tabs[i].Active:=true;
          except
            //ShowMessageLog('Cannot activate Tab');
          end;
        end;
      end;
    end else begin
      //Unmerge
      //ShowMessageLog('Unmerge');
      if Assigned(ChildBarManager) then begin
        //i := FTabSelect;  //��������������, �� ������������ �� ��������� �������
        i := 0;             //������������ ������ �� ������� �������
        TdxRibbon(self.RibbonControl).Tabs[i].Active:=true;
        Sender.Unmerge(ChildBarManager);
      end;
    end;

    //<--������ ������� �������, ���� ���� ������������
//    if TdxRibbon(self.RibbonControl).Tabs.Count<=constMainRibbonTabsCount then
//      rtMainTab.Visible:=True
//    else
//      rtMainTab.Visible:=False;
    //-->
  finally
    TdxRibbon(self.RibbonControl).EndUpdate;
    EnableRefreshChildForms;
  end;
end;

procedure TfmMain.dxRibbonTabChanged(Sender: TdxCustomRibbon);
begin
  if dxRibbon.ActiveTab.Index < constMainRibbonTabsCount then
    FTabSelect := dxRibbon.ActiveTab.Index;
end;

procedure TfmMain.FormCreate(Sender: TObject);
begin
  ClearMessageLog;
{  CurDir := ExtractFilePath(ParamStr(0));
  ReadIni;

  Person.Id:=1;
  Person.Name:='Admin';
  Person.Id_Parent:=0;

  //<--������ ����� �� Project/Options.../Version Info/ProgramVersion
  sVer:=GetProgramVersion(Application.ExeName);
  //-->

  CommonSet.AppName:=ExtractFileName(Application.ExeName);
  CommonSet.AppName:=copy(CommonSet.AppName,1,length(CommonSet.AppName)-4);  //������� ����������
  CommonSet.AppName:=CommonSet.AppName+' ('+sVer+')';  //��������� ������
  StatusBar1.Panels[1].Text:=sVer;

  //<--��������� ���������� Ribbon ������� �� Merge
  constMainRibbonTabsCount:=dxRibbon.TabCount;
  //-->
  //<--
  MainAppIniFileStorage.FileName:=GetFormIniFile;  //������ ���� �� ini �����
  //-->
  //<--������������ ��������� �� �������
  LoadKeyboardLayout('00000419', KLF_ACTIVATE);
  //-->
  //<--�������� ������ ������
  MDITaskBar := TMDITaskBar.Create(Self);
  with MDITaskBar do begin
    Parent := pnlMDITaskBar;
    BevelOuter:=bvNone;
    Align := alClient;
    //Height := 27;
    AutoCreateButtons := True;
    AlwaysMaximized := False;
    ButtonWidth := 140;
    Flat := False;
    HideMinimizedMDI := True;
    GroupSimilar := False;
    SyncCaptions := True;
    DragDropReorder := False;
    ShowHint := True;
    Font.Style := [fsBold];
    ScrollType := TScrollType.stNone;
  end;

  with dmR do
  begin
    if DBConnectUDL then
    begin
      quPoint.Active:=False;
      quPoint.Active:=True;
      quPoint.First;
//      quPoint.Locate('ISHOP',29,[]);

      ShowMessageLog('����������� - '+its(quPoint.RecordCount));

      beiPointMF.EditValue:=quPointISHOP.AsInteger;
    end else
    begin
      ShowMessageLog('������ �������� ��.');
    end;
  end;
  //-->   }
end;

end.
