object fmCustomChildForm: TfmCustomChildForm
  Left = 0
  Top = 0
  Caption = 'fmCustomChildForm'
  ClientHeight = 361
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object PanelLog: TPanel
    Left = 0
    Top = 271
    Width = 635
    Height = 90
    Align = alBottom
    BevelOuter = bvNone
    ParentBackground = False
    TabOrder = 0
    Visible = False
    object PanelCloseLog: TPanel
      Left = 0
      Top = 0
      Width = 18
      Height = 90
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
      object btnCloseLog: TcxButton
        Left = 0
        Top = 0
        Width = 18
        Height = 18
        Align = alTop
        OptionsImage.Glyph.Data = {
          F6000000424DF600000000000000360000002800000008000000080000000100
          180000000000C000000000000000000000000000000000000000FF00FFFF00FF
          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FF000000000000FF00FFFF00FF000000000000FF
          00FFFF00FFFF00FF000000000000000000000000FF00FFFF00FFFF00FFFF00FF
          FF00FF000000000000FF00FFFF00FFFF00FFFF00FFFF00FF0000000000000000
          00000000FF00FFFF00FFFF00FF000000000000FF00FFFF00FF000000000000FF
          00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
        SpeedButtonOptions.CanBeFocused = False
        TabOrder = 0
        TabStop = False
        OnClick = btnCloseLogClick
      end
    end
    object MemoLogLocal: TcxMemo
      Left = 18
      Top = 0
      Align = alClient
      Properties.ReadOnly = True
      Properties.ScrollBars = ssVertical
      Properties.WordWrap = False
      TabOrder = 1
      ExplicitLeft = 24
      Height = 90
      Width = 617
    end
  end
  object SplitterBottom: TcxSplitter
    Left = 0
    Top = 267
    Width = 635
    Height = 4
    AlignSplitter = salBottom
    ResizeUpdate = True
    Control = PanelLog
    Visible = False
  end
  object JvFormStorageCustom: TJvFormStorage
    Active = False
    AppStorage = fmMain.MainAppIniFileStorage
    AppStoragePath = '%FORM_NAME%\'
    StoredValues = <>
    Left = 52
    Top = 56
  end
end
