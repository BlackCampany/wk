unit CliLic;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  CustomChildForm, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer,
  cxTextEdit, cxCalendar, cxCheckBox, cxDBLookupComboBox, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error,
  FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Vcl.Menus, cxClasses, FireDAC.Comp.Client,
  FireDAC.Comp.DataSet, System.Actions, Vcl.ActnList, dxBar, cxBarEditItem, cxMemo, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridCustomView, cxGrid, dxRibbon;

type
  TfmCliLic = class(TfmCustomChildForm)
    dxBarManager1: TdxBarManager;
    dxBarGroup1: TdxBarGroup;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    ActionList1: TActionList;
    acClose: TAction;
    quCliLic: TFDQuery;
    dsquCliLic: TDataSource;
    ViewCliLic: TcxGridDBTableView;
    LevelCliLic: TcxGridLevel;
    GridCliLIc: TcxGrid;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    PopupMenu1: TPopupMenu;
    cxStyle2: TcxStyle;
    dxBarSubItem1: TdxBarSubItem;
    quE: TFDQuery;
    acDelPos: TAction;
    N1: TMenuItem;
    dxBarManager1Bar2: TdxBar;
    acAddPos: TAction;
    N4: TMenuItem;
    quCliLicIDCLI: TStringField;
    quCliLicIDL: TFDAutoIncField;
    quCliLicLICNUM: TStringField;
    quCliLicIDATEB: TIntegerField;
    quCliLicIDATEE: TIntegerField;
    quCliLicORGAN: TStringField;
    ViewCliLicIDL: TcxGridDBColumn;
    ViewCliLicLICNUM: TcxGridDBColumn;
    ViewCliLicIDATEB: TcxGridDBColumn;
    ViewCliLicIDATEE: TcxGridDBColumn;
    ViewCliLicORGAN: TcxGridDBColumn;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarManager1Bar1: TdxBar;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarButton1: TdxBarButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    procedure acCloseExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acDelPosExecute(Sender: TObject);
    procedure acAddPosExecute(Sender: TObject);
  private
    { Private declarations }
    FCLIID,FCLINAME:string;
    procedure Init;

  public
    { Public declarations }
  end;

// var
//  fmDocSpec: TfmDocSpec;

procedure ShowCliLic(CLIID,FCLINAME:string);

implementation

{$R *.dfm}

uses ViewRar, dmRar, EgaisDecode, ViewDocCorr, AddPosCorrSpec, ViewCards, AddLic;

procedure ShowCliLic(CLIID,FCLINAME:string);
var F:TfmCliLic;
begin
  F:=TfmCliLic.Create(Application);
// ShowMessage(Xmlstr);

  F.FCLIID:=CLIID;
  F.FCLINAME:=FCLINAME;

  F.Init;
  F.Show;
end;

procedure TfmCliLic.acAddPosExecute(Sender: TObject);
begin
  //�������� �������
  fmAddLic:=TfmAddLic.Create(Application);
  fmAddLic.cxTextEdit1.Text:='';
  fmAddLic.cxTextEdit2.Text:='';
  fmAddLic.cxDateEdit1.Date:=Date;
  fmAddLic.cxDateEdit2.Date:=Date;
  fmAddLic.ShowModal;
  if fmAddLic.ModalResult=mrOk then
  begin
    quE.Active:=False;
    quE.SQL.Clear;
    quE.SQL.Add('');
    quE.SQL.Add('INSERT INTO [dbo].[ALIC] ([IDCLI],[LICNUM],[IDATEB],[IDATEE],[ORGAN])');
    quE.SQL.Add('     VALUES ('''+FCLIID+''','''+fmAddLic.cxTextEdit1.Text+''','+its(Trunc(fmAddLic.cxDateEdit1.Date))+','+its(Trunc(fmAddLic.cxDateEdit2.Date))+','''+fmAddLic.cxTextEdit2.Text+''')');
    quE.ExecSQL;

    Init;
  end;
  fmAddLic.Free;
end;

procedure TfmCliLic.acCloseExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmCliLic.acDelPosExecute(Sender: TObject);
begin
  if quCliLic.RecordCount>0 then
  begin
    quE.Active:=False;
    quE.SQL.Clear;
    quE.SQL.Add('');
    quE.SQL.Add('DELETE FROM [dbo].[ALIC] WHERE [IDCLI]='''+FCLIID+''' and [IDL]='+its(quCliLicIDL.AsInteger));
    quE.ExecSQL;

    Init;
  end;
end;

procedure TfmCliLic.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
end;

procedure TfmCliLic.Init;
begin
  Caption:='�������� ���������� '+FCLINAME;

  ViewCliLic.BeginUpdate;

  quCliLic.Active:=False;
  quCliLic.ParamByName('IDCLI').AsString:=FCLIID;
  quCliLic.Active:=True;

  ViewCliLic.EndUpdate;
end;

end.
