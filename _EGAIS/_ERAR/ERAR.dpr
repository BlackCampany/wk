// JCL_DEBUG_EXPERT_DELETEMAPFILE OFF
// JCL_DEBUG_EXPERT_GENERATEJDBG OFF
// JCL_DEBUG_EXPERT_INSERTJDBG OFF
program ERAR;

uses
  Vcl.Forms,
  ViewXML in 'ViewXML.pas' {fmShowXML},
  EgaisDecode in 'EgaisDecode.pas',
  Clients in 'Clients.pas' {fmClients},
  ViewSpecOut in 'ViewSpecOut.pas' {fmDocSpecOut},
  dmRar in 'dmRar.pas' {dmR: TDataModule},
  ProDocs in 'ProDocs.pas' {fmProDoc},
  UTMExch in 'UTMExch.pas' {fmUTMExch},
  SpecPro in 'SpecPro.pas' {fmSpecPro},
  DocHeaderCB in 'DocHeaderCB.pas' {fmDocCB},
  SpecMC in 'SpecMC.pas' {fmSpecMC},
  History in 'History.pas' {fmHistoryDoc},
  UTMArh in 'UTMArh.pas' {fmReplyListArh},
  DevExRus in '..\..\SharedUnits\DevExRusRes\DevExRus.pas',
  UTMCheck in 'UTMCheck.pas' {fmUTMCheck},
  Shared_Exception in '..\..\SharedUnits\Shared_Exception.pas',
  Shared_Functions in '..\..\SharedUnits\Shared_Functions.pas',
  Shared_Logs in '..\..\SharedUnits\Shared_Logs.pas',
  MCrystDocs in 'MCrystDocs.pas' {fmMCrystDoc},
  SpecCB in 'SpecCB.pas' {fmSpecCB},
  ViewSpec in 'ViewSpec.pas' {fmDocSpec},
  Main in 'Main.pas' {fmMain},
  Shared_MDITaskBar in '..\..\SharedUnits\Shared_MDITaskBar.pas',
  CustomChildForm in 'CustomChildForm.pas' {fmCustomChildForm},
  QBarCodeS in 'QBarCodeS.pas' {fmQBarCodeS},
  QBarCodeH in 'QBarCodeH.pas' {fmQBarCodeH},
  Shared_Types in '..\..\SharedUnits\Shared_Types.pas',
  UnitFunction in 'UnitFunction.pas',
  ViewRar in 'ViewRar.pas' {fmViewRar},
  ViewDocIn in 'ViewDocIn.pas' {fmViewDocIn},
  ViewDocRet in 'ViewDocRet.pas' {fmViewDocRet},
  ViewNATTN in 'ViewNATTN.pas' {fmNATTN},
  ViewCash in 'ViewCash.pas' {fmDocCash},
  ViewSpecVn in 'ViewSpecVn.pas' {fmDocSpecVn},
  ViewDocInv in 'ViewDocInv.pas' {fmDocInv},
  ViewSpecInv in 'ViewSpecInv.pas' {fmDocSpecInv},
  DocDeclHD in 'DocDeclHD.pas' {fmDocDecl},
  DocDeclSP in 'DocDeclSP.pas' {fmDocSpecDecl},
  SetCodeCB in 'SetCodeCB.pas' {fmSetCodeCB},
  TestRemnInv2 in 'TestRemnInv2.pas' {fmTestRemnInv2},
  Shared_Ping in '..\..\SharedUnits\Shared_Ping.pas',
  AddRetDoc in 'AddRetDoc.pas' {fmAddRetDoc},
  AddPosCorrSpec in 'AddPosCorrSpec.pas' {fmAddPosCorr},
  ViewDocVn in 'ViewDocVn.pas' {fmDocVn},
  AddPosOutSpec in 'AddPosOutSpec.pas' {fmAddPosOut},
  ViewDocCorr in 'ViewDocCorr.pas' {fmDocCorr},
  CliLic in 'CliLic.pas' {fmCliLic},
  InputFromFile in 'InputFromFile.pas' {fmInputFile},
  ViewCards in 'ViewCards.pas' {fmACard},
  ViewSpecCorr in 'ViewSpecCorr.pas' {fmDocSpecCorr},
  AddLic in 'AddLic.pas' {fmAddLic},
  ImpRemnDecl in 'ImpRemnDecl.pas' {fmImportRemnDecl};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TdmR, dmR);
  Application.CreateForm(TfmMain, fmMain);
  //<--����������� _Log � _ExceptionHandler
  _Log.Connection:=dmR.FDConnection;
  _Log.ScreenShotOnError:=True;  //������ ��������� ��� �������
  _Log.CacheLogEnabled:=True;          //����� ������ ���� � ���
  _Log.CacheLogMaxCount:=1000;         //����.����� ����� � ����, ������ �����, ����� ���� ���� ����
  _Log.TimesInterval:=5000;            //�������� ������ ���� ���� � �� �� �������
  _Log.TimesEnabled:=True;             //������ ���� �� ������� ���������
  _ExceptionHandler.ProgramVersion:=sVer;    //��������� ���������� � �������� ������ ��������� � �������
  _ExceptionHandler.ShowMessageError:=True;  //���������� ��������� �� �������, � ������ ���������� �����
  //-->
  Application.Run;
end.
