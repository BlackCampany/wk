unit UTMExch;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  CustomChildForm, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  dxRibbonForm,ShellApi,
  httpsend, synautil, nativexml, IniFiles,
  cxLookAndFeelPainters, dxRibbonSkins, dxRibbonCustomizationForm, cxContainer, cxEdit, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxNavigator, Data.DB, cxDBData, cxImageComboBox, cxCheckBox, cxTextEdit, cxDBLookupComboBox, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async,
  FireDAC.DApt, Vcl.Menus, FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.ImgList, System.Actions, Vcl.ActnList, dxBar, cxBarEditItem,
  dxBarExtItems, cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid, cxMemo,
  Vcl.ExtCtrls, dxRibbon;

type
  TfmUTMExch = class(TfmCustomChildForm)
    dxRibbon1: TdxRibbon;
    dxRibbon1Tab1: TdxRibbonTab;
    dxBarManager1: TdxBarManager;
    bmApplyDate: TdxBar;
    bmClose: TdxBar;
    btnDone: TdxBarLargeButton;
    btnClose: TdxBarLargeButton;
    deDateBeg: TdxBarDateCombo;
    deDateEnd: TdxBarDateCombo;
    dxBarGroup1: TdxBarGroup;
    ActionList1: TActionList;
    acRefresh: TAction;
    acClose: TAction;
    acSelectDate: TAction;
    quSendList: TFDQuery;
    quSendListRECEIVE_ID: TStringField;
    quSendListRETVAL: TStringField;
    quSendListIACTIVE: TSmallintField;
    quSendListIDEL: TSmallintField;
    quSendListDateSend: TSQLTimeStampField;
    dsquSendList: TDataSource;
    quReplyList: TFDQuery;
    quReplyListID: TLargeintField;
    quReplyListReplyId: TStringField;
    quReplyListReplyPath: TStringField;
    quReplyListReplyFile: TMemoField;
    quReplyListIACTIVE: TSmallintField;
    quReplyListIDEL: TSmallintField;
    quReplyListDateReply: TSQLTimeStampField;
    dsquReplyList: TDataSource;
    cbItem1: TcxBarEditItem;
    acDecodeTovar: TAction;
    pmReply: TPopupMenu;
    quS: TFDQuery;
    acSaveToFile: TAction;
    N2: TMenuItem;
    acDelList: TAction;
    acDecodeTTN: TAction;
    N4: TMenuItem;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarSubItem1: TdxBarSubItem;
    acCards: TAction;
    acSendActR: TAction;
    PopupMenu2: TPopupMenu;
    N5: TMenuItem;
    acDecodeWB: TAction;
    GridRar: TcxGrid;
    ViewSend: TcxGridDBTableView;
    ViewSendDateSend: TcxGridDBColumn;
    ViewSendRECEIVE_ID: TcxGridDBColumn;
    ViewSendRETVAL: TcxGridDBColumn;
    ViewSendIACTIVE: TcxGridDBColumn;
    ViewSendIDEL: TcxGridDBColumn;
    ViewReply: TcxGridDBTableView;
    ViewReplyDateReply: TcxGridDBColumn;
    ViewReplyID: TcxGridDBColumn;
    ViewReplyReplyId: TcxGridDBColumn;
    ViewReplyReplyPath: TcxGridDBColumn;
    ViewReplyReplyFile: TcxGridDBColumn;
    ViewReplyIACTIVE: TcxGridDBColumn;
    ViewReplyIDEL: TcxGridDBColumn;
    LevelReply: TcxGridLevel;
    LevelSend: TcxGridLevel;
    acUTMExch: TAction;
    dxBarLargeButton2: TdxBarLargeButton;
    LevelQuest: TcxGridLevel;
    ViewQuest: TcxGridDBTableView;
    ToRarList: TFDQuery;
    dsToRarList: TDataSource;
    ToRarListID: TLargeintField;
    ToRarListDATEQU: TSQLTimeStampField;
    ToRarListITYPEQU: TSmallintField;
    ToRarListISTATUS: TSmallintField;
    ToRarListSENDFILE: TMemoField;
    ToRarListRECEIVE_ID: TStringField;
    ToRarListRECEIVE_FILE: TMemoField;
    ViewQuestID: TcxGridDBColumn;
    ViewQuestDATEQU: TcxGridDBColumn;
    ViewQuestITYPEQU: TcxGridDBColumn;
    ViewQuestISTATUS: TcxGridDBColumn;
    ViewQuestSENDFILE: TcxGridDBColumn;
    ViewQuestRECEIVE_ID: TcxGridDBColumn;
    ViewQuestRECEIVE_FILE: TcxGridDBColumn;
    acDecode: TAction;
    N1: TMenuItem;
    acRemoveToArh: TAction;
    N3: TMenuItem;
    acDelFromUTM1: TAction;
    N6: TMenuItem;
    acDelFromUTM2: TAction;
    pmSend: TPopupMenu;
    N7: TMenuItem;
    quReplyListFSRARID: TStringField;
    ViewReplyFSRARID: TcxGridDBColumn;
    quSendListFSRARID: TStringField;
    ViewSendFSRARID: TcxGridDBColumn;
    ToRarListFSRARID: TStringField;
    ViewQuestFSRARID: TcxGridDBColumn;
    acCreateInv: TAction;
    N8: TMenuItem;
    N9: TMenuItem;
    acTestCAP: TAction;
    N10: TMenuItem;
    procedure acRefreshExecute(Sender: TObject);
    procedure acCloseExecute(Sender: TObject);
    procedure acSelectDateExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ViewReplyDblClick(Sender: TObject);
    procedure deDateBegChange(Sender: TObject);
    procedure acDecodeTovarExecute(Sender: TObject);
    procedure acSaveToFileExecute(Sender: TObject);
    procedure acDelListExecute(Sender: TObject);
    procedure acDecodeTTNExecute(Sender: TObject);
    procedure acCardsExecute(Sender: TObject);
    procedure ViewDocsDblClick(Sender: TObject);
    procedure acSendActRExecute(Sender: TObject);
    procedure acDecodeWBExecute(Sender: TObject);
    procedure acUTMExchExecute(Sender: TObject);
    procedure ViewQuestDblClick(Sender: TObject);
    procedure acDecodeExecute(Sender: TObject);
    procedure acRemoveToArhExecute(Sender: TObject);
    procedure acDelFromUTM1Execute(Sender: TObject);
    procedure acDelFromUTM2Execute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acCreateInvExecute(Sender: TObject);
    procedure acTestCAPExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }

    procedure Init;
  end;

var
  fmUTMExch: TfmUTMExch;

procedure ShowFormUTMExch;

implementation

{$R *.dfm}

uses ViewXML, EgaisDecode, ViewCards, dmRar, ViewRar;

procedure ShowFormUTMExch;
begin
  if Assigned(fmUTMExch)=False then //����� �� ����������, � ����� �������
    fmUTMExch:=TfmUTMExch.Create(Application);
  if fmUTMExch.WindowState=wsMinimized then //���� ���� ��������, �� ������������� ���
    fmUTMExch.WindowState:=wsNormal;

  with dmR do
  begin
    if FDConnection.Connected then
    begin
      fmUTMExch.Init;
    end else
    begin
      ShowMessage('������ �������� ��');
    end;
  end;
  fmUTMExch.Show;
end;

procedure TfmUTMExch.Init;
var
  FocusedRow, TopRow: Integer;
  flag: boolean;
begin
  if dmR.FDConnection.Connected then
  begin
    try
      //<--Refresh � ��������������� ������� ������� � �������
      flag:=quSendList.Active;  //���������� ���� �� ������� �������, ��� �������������� ������� �������
      TopRow := ViewSend.Controller.TopRowIndex;
      FocusedRow := ViewSend.DataController.FocusedRowIndex;
      //-->
      ViewSend.BeginUpdate;
      quSendList.Active:=False;
      quSendList.ParamByName('DATEB').AsDateTime:=Trunc(deDateBeg.Date);
      quSendList.ParamByName('DATEE').AsDateTime:=Trunc(deDateEnd.Date)+1;
      quSendList.ParamByName('RARID').AsString:=CommonSet.FSRAR_ID;
      quSendList.Active:=True;
      quSendList.First;
    finally
      ViewSend.EndUpdate;
      //<--��������������� �������
      if flag then begin
        ViewSend.DataController.FocusedRowIndex := FocusedRow;
        ViewSend.Controller.TopRowIndex := TopRow;
      end;
      //-->
    end;

    try
      //<--Refresh � ��������������� ������� ������� � �������
      flag:=quReplyList.Active;  //���������� ���� �� ������� �������, ��� �������������� ������� �������
      TopRow := ViewReply.Controller.TopRowIndex;
      FocusedRow := ViewReply.DataController.FocusedRowIndex;
      //-->
      ViewReply.BeginUpdate;
      quReplyList.Active:=False;
      quReplyList.ParamByName('DATEB').AsDateTime:=Trunc(deDateBeg.Date);
      quReplyList.ParamByName('DATEE').AsDateTime:=Trunc(deDateEnd.Date)+1;
      quReplyList.ParamByName('RARID').AsString:=CommonSet.FSRAR_ID;
      quReplyList.Active:=True;
      quReplyList.First;
    finally
      ViewReply.EndUpdate;
      //<--��������������� �������
      if flag then begin
        ViewReply.DataController.FocusedRowIndex := FocusedRow;
        ViewReply.Controller.TopRowIndex := TopRow;
      end;
      //-->
    end;

    try
      //<--Refresh � ��������������� ������� ������� � �������
      flag:=ToRarList.Active;  //���������� ���� �� ������� �������, ��� �������������� ������� �������
      TopRow := ViewQuest.Controller.TopRowIndex;
      FocusedRow := ViewQuest.DataController.FocusedRowIndex;
      //-->
      ViewQuest.BeginUpdate;
      ToRarList.Active:=False;
      ToRarList.ParamByName('DATEB').AsDateTime:=Trunc(deDateBeg.Date);
      ToRarList.ParamByName('DATEE').AsDateTime:=Trunc(deDateEnd.Date)+1;
      ToRarList.ParamByName('RARID').AsString:=CommonSet.FSRAR_ID;
      ToRarList.Active:=True;
      ToRarList.First;
    finally
      ViewQuest.EndUpdate;
      //<--��������������� �������
      if flag then begin
        ViewQuest.DataController.FocusedRowIndex := FocusedRow;
        ViewQuest.Controller.TopRowIndex := TopRow;
      end;
      //-->
    end;
  end;
//  FSRAR_ID:=CommonSet.FSRAR_ID;
end;


procedure TfmUTMExch.ViewDocsDblClick(Sender: TObject);
begin
{  // ������������
  with dmR do
  begin
    if dmR.FDConnection.Connected then
    begin
      if quDocInHd.RecordCount>0 then
      begin
        if quDocInHdIACTIVE.AsInteger=0 then
           ShowSpecView(quDocInHdFSRARID.AsString,quDocInHdSID.AsString,quDocInHdIDATE.AsInteger,1)
        else
           ShowSpecView(quDocInHdFSRARID.AsString,quDocInHdSID.AsString,quDocInHdIDATE.AsInteger,0);
      end;
    end;
  end;}
end;

procedure TfmUTMExch.ViewQuestDblClick(Sender: TObject);
Var
    ExecuteFile, ParamString: string;
    SEInfo: TShellExecuteInfo;
//    F:TextFile;
begin
  if ToRarList.RecordCount>0 then
  begin
    if (ViewQuest.Controller.FocusedColumn.Name='ViewQuestRECEIVE_FILE') then
    begin
      if cbItem1.EditValue=True then
      begin
        try
          ToRarListRECEIVE_FILE.SaveToFile(CommonSet.TmpDir+ToRarListID.AsString+'.xml');

          FillChar(SEInfo, SizeOf(SEInfo), 0);
          SEInfo.cbSize := SizeOf(TShellExecuteInfo);
          ExecuteFile := 'C:\XML Notepad 2007\XmlNotepad.exe';
          ParamString :=CommonSet.TmpDir+ToRarListID.AsString+'.xml';
          with SEInfo do
          begin
            fMask := SEE_MASK_NOCLOSEPROCESS;
            Wnd := Application.Handle;
            lpFile := PChar(ExecuteFile);
            lpParameters := PChar(ParamString);
            lpDirectory := PChar(CommonSet.TmpDir);
            nShow := SW_SHOWNORMAL;
          end;

          ShellExecuteEx(@SEInfo);
        except
          ShowMessage('������ ������� �������� C:\XML Notepad 2007\XmlNotepad.exe');
        end;
      end else ShowXMLView(ToRarListID.AsInteger,ToRarListRECEIVE_FILE.AsString);
    end else
    begin
      if cbItem1.EditValue=True then
      begin
        try
          ToRarListSENDFILE.SaveToFile(CommonSet.TmpDir+ToRarListID.AsString+'.xml');

          FillChar(SEInfo, SizeOf(SEInfo), 0);
          SEInfo.cbSize := SizeOf(TShellExecuteInfo);
          ExecuteFile := 'C:\XML Notepad 2007\XmlNotepad.exe';
          ParamString :=CommonSet.TmpDir+ToRarListID.AsString+'.xml';
          with SEInfo do
          begin
            fMask := SEE_MASK_NOCLOSEPROCESS;
            Wnd := Application.Handle;
            lpFile := PChar(ExecuteFile);
            lpParameters := PChar(ParamString);
            lpDirectory := PChar(CommonSet.TmpDir);
            nShow := SW_SHOWNORMAL;
          end;

          ShellExecuteEx(@SEInfo);
        except
          ShowMessage('������ ������� �������� C:\XML Notepad 2007\XmlNotepad.exe');
        end;
      end else ShowXMLView(ToRarListID.AsInteger,ToRarListSENDFILE.AsString);
    end;
  end;
end;

procedure TfmUTMExch.ViewReplyDblClick(Sender: TObject);
Var
    ExecuteFile, ParamString: string;
    SEInfo: TShellExecuteInfo;
//    F:TextFile;
begin
  if quReplyList.RecordCount>0 then
  begin
    if cbItem1.EditValue=True then
    begin
      try
        quReplyListReplyFile.SaveToFile(CommonSet.TmpDir+quReplyListID.AsString+'.xml');
        {
        AssignFile(F,CommonSet.TmpDir+quReplyListID.AsString+'.xml');
        Rewrite(F);
        Writeln(f,quReplyListReplyFile.AsString);
        CloseFile(F);
        }

        FillChar(SEInfo, SizeOf(SEInfo), 0);
        SEInfo.cbSize := SizeOf(TShellExecuteInfo);
        ExecuteFile := 'C:\XML Notepad 2007\XmlNotepad.exe';
        ParamString :=CommonSet.TmpDir+quReplyListID.AsString+'.xml';
        with SEInfo do
        begin
          fMask := SEE_MASK_NOCLOSEPROCESS;
          Wnd := Application.Handle;
          lpFile := PChar(ExecuteFile);
          lpParameters := PChar(ParamString);
          lpDirectory := PChar(CommonSet.TmpDir);
          nShow := SW_SHOWNORMAL;
        end;

        ShellExecuteEx(@SEInfo);
      except
        ShowMessage('������ ������� �������� C:\XML Notepad 2007\XmlNotepad.exe');
      end;
    end else ShowXMLView(quReplyListID.AsInteger,quReplyListReplyFile.AsString);
  end;
end;

procedure TfmUTMExch.acCardsExecute(Sender: TObject);
var F:TfmACard;
begin
  //�������� �����
  F:=TfmACard.Create(Self);
  F.ViewCards.BeginUpdate;
  F.quCards.Active:=False;
  F.quCards.Active:=True;
  F.ViewCards.EndUpdate;
  F.Show;
end;

procedure TfmUTMExch.acCloseExecute(Sender: TObject);
begin
  //�������
  Close;
end;

procedure TfmUTMExch.acCreateInvExecute(Sender: TObject);
type

     TCli = record
              ClientRegId,FullName,ShortName,INN,KPP,Country,RegionCode,description:string;
            end;

     TCard = record
               FullName,AlcCode:string;
               Capacity,AlcVolume:Real;
               ProductVCode:Integer;
               Prod:TCli;
               Impr:TCli;
             end;

     TPos = record
              Num:Integer;
              Quant,Price:Real;
              Card:TCard;
              info_a,info_b,party:string;
              sNum:string;
            end;

Var  xmlstr:string;
     iC:Integer;
     Xml: TNativeXml;
     nodeRoot,nodePos,nodeSpec: TXmlNode;
     IDH,iDateInv,i:integer;
     SFIXDATE:string;
     vPos:TPos;

  procedure ClearPos;
  begin
    vPos.Num:=0;
    vPos.Quant:=0; vPos.Price:=0;
    vPos.Card.FullName:=''; vPos.Card.AlcCode:=''; vPos.Card.Capacity:=0; vPos.Card.AlcVolume:=0; vPos.Card.ProductVCode:=0; vPos.info_a:=''; vPos.info_b:=''; vPos.party:='';
    vPos.Card.Prod.ClientRegId:=''; vPos.Card.Prod.FullName:=''; vPos.Card.Prod.ShortName:=' '; vPos.Card.Prod.INN:=' '; vPos.Card.Prod.INN:=' '; vPos.Card.Prod.KPP:=' '; vPos.Card.Prod.Country:=' '; vPos.Card.Prod.RegionCode:=' '; vPos.Card.Prod.description:=' ';
    vPos.Card.Impr.ClientRegId:=''; vPos.Card.Impr.FullName:=''; vPos.Card.Impr.ShortName:=' '; vPos.Card.Impr.INN:=' '; vPos.Card.Impr.INN:=' '; vPos.Card.Impr.KPP:=' '; vPos.Card.Impr.Country:=' '; vPos.Card.Impr.RegionCode:=' '; vPos.Card.Impr.description:=' ';
    vPos.sNum:='';
  end;

begin
  //������������ ������������������ ���������
  //�������� �� ������ ������� , ������ ��������
  with dmR do
  begin
    if fmUTMExch.quReplyList.RecordCount>0 then
    begin
      ClearMessageLogLocal;
      ShowMessageLogLocal('����� ... ���� ������������ ���������.');
      iC:=0;

      xmlstr:=UTF8ToString(fmUTMExch.quReplyListReplyFile.AsString);

      if (Pos('ns:ReplyRests',xmlstr)>0)or(Pos('ns:ReplyRestsShop_v2',xmlstr)>0) then  // ��� �������
      begin
        //������� ������������
        try
          Xml:= TNativeXml.Create(nil);
          Xml.XmlFormat := xfReadable;
//          while Pos('''',xmlstr)>0 do xmlstr[Pos('''',xmlstr)]:='"';

          Xml.ReadFromString(xmlstr);

          nodeRoot:=Xml.Root;
          IDH:=fGetId(6);
          iDateInv:=Trunc(Date);

          if Assigned(nodeRoot.NodeByName('ns:Document')) then
          begin
            if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:ReplyRests')) then
            begin  // �����  (1 �������)
              SFIXDATE:='';
              if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:ReplyRests').NodeByName('rst:RestsDate')) then
                SFIXDATE:=nodeRoot.NodeByName('ns:Document').NodeByName('ns:ReplyRests').NodeByName('rst:RestsDate').Value;

              //��������� ���������

              ShowMessageLogLocal('  - ��������� ��������� ��������� ('+its(IDH)+')');

              quS.Active:=False;
              quS.SQL.Clear;
              quS.SQL.Add('');
              quS.SQL.Add('INSERT INTO [dbo].[ADOCSINV_HD] ([FSRARID],[IDATE],[ID],[STYPE],[FIXDATE],[IDREPLY])');
              quS.SQL.Add('VALUES ('''+fmUTMExch.quReplyListFSRARID.AsString+'''');
              quS.SQL.Add('           ,'+its(iDateInv));
              quS.SQL.Add('           ,'+its(IDH));
              quS.SQL.Add('           ,''STOCK ( 1-reg )''');
              quS.SQL.Add('           ,'''+SFIXDATE+'''');
              quS.SQL.Add('           ,'+its(fmUTMExch.quReplyListID.AsInteger)+')');
              quS.ExecSQL;

              if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:ReplyRests').NodeByName('rst:Products')) then //������������
              begin
                nodeSpec:=nodeRoot.NodeByName('ns:Document').NodeByName('ns:ReplyRests').NodeByName('rst:Products');

                ShowMessageLogLocal('  - ��������� ������������ ( '+its(nodeSpec.NodeCount)+' ������� )');

                for i:=0 to nodeSpec.NodeCount-1 do
                begin
                  nodePos:=nodeSpec[i];

                  if i mod 100 = 0 then  ShowMessageLogLocal('     - ���������� '+its(iC)+' �������');

                  ClearPos;

                  if Pos('rst:StockPosition',nodePos.Name)>0 then  //��� �������
                  begin
                    if Assigned(nodePos.NodeByName('rst:Quantity')) then vPos.Quant:=stf(nodePos.NodeByName('rst:Quantity').Value);
                    if Assigned(nodePos.NodeByName('rst:InformARegId')) then vPos.info_a:=nodePos.NodeByName('rst:InformARegId').Value;
                    if Assigned(nodePos.NodeByName('rst:InformBRegId')) then vPos.info_b:=nodePos.NodeByName('rst:InformBRegId').Value;
                    if Assigned(nodePos.NodeByName('rst:Product')) then
                      if Assigned(nodePos.NodeByName('rst:Product').NodeByName('pref:AlcCode')) then vPos.Card.AlcCode:=nodePos.NodeByName('rst:Product').NodeByName('pref:AlcCode').Value;

                    if vPos.Card.AlcCode>'' then  //������� � �������� ���������� - ��������
                    begin
                      try
                        quS.Active:=False;
                        quS.SQL.Clear;
                        quS.SQL.Add('');
                        quS.SQL.Add('INSERT INTO [dbo].[ADOCSINV_SP] ([IDH],[ID],[CAP],[QUANTR],[QUANTF],[INFO_A],[INFO_B])');
                        quS.SQL.Add('     VALUES');
                        quS.SQL.Add('           ('+its(IDH));
                        quS.SQL.Add('           ,'+its(iC+1));
                        quS.SQL.Add('           ,'''+vPos.Card.AlcCode+'''');
                        quS.SQL.Add('           ,'+fts(vPos.Quant));
                        quS.SQL.Add('           ,0');
                        quS.SQL.Add('           ,'''+vPos.info_a+'''');
                        quS.SQL.Add('           ,'''+vPos.info_b+''')');
                        quS.ExecSQL;
                      except
                        ShowMessageLogLocal('     ������ ���������� ('+its(iC+1)+','+vPos.Card.AlcCode+','+fts(vPos.Quant)+')');
                      end;
                      inc(iC);
                    end;
                  end;
                end;
                ShowMessageLogLocal('     - ���������� ����� '+its(iC)+' �������');
              end;
            end;
            if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:ReplyRestsShop_v2')) then
            begin  // ������� (2 �������)
              SFIXDATE:='';
              if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:ReplyRestsShop_v2').NodeByName('rst:RestsDate')) then
                SFIXDATE:=nodeRoot.NodeByName('ns:Document').NodeByName('ns:ReplyRestsShop_v2').NodeByName('rst:RestsDate').Value;

              //��������� ���������
              quS.Active:=False;
              quS.SQL.Clear;
              quS.SQL.Add('');
              quS.SQL.Add('INSERT INTO [dbo].[ADOCSINV_HD] ([FSRARID],[IDATE],[ID],[STYPE],[FIXDATE],[IDREPLY])');
              quS.SQL.Add('VALUES ('''+fmUTMExch.quReplyListFSRARID.AsString+'''');
              quS.SQL.Add('           ,'+its(iDateInv));
              quS.SQL.Add('           ,'+its(IDH));
              quS.SQL.Add('           ,''SHOP ( 2-reg )''');
              quS.SQL.Add('           ,'''+SFIXDATE+'''');
              quS.SQL.Add('           ,'+its(fmUTMExch.quReplyListID.AsInteger)+')');
              quS.ExecSQL;

              if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:ReplyRestsShop_v2').NodeByName('rst:Products')) then //������������
              begin
                nodeSpec:=nodeRoot.NodeByName('ns:Document').NodeByName('ns:ReplyRestsShop_v2').NodeByName('rst:Products');

                ShowMessageLogLocal('  - ��������� ������������ ( '+its(nodeSpec.NodeCount)+' ������� )');

                for i:=0 to nodeSpec.NodeCount-1 do
                begin
                  nodePos:=nodeSpec[i];

                  if i mod 100 = 0 then begin ShowMessageLogLocal('     - ���������� '+its(iC)+' �������'); delay(10); end;

                  ClearPos;

                  if Pos('rst:ShopPosition',nodePos.Name)>0 then  //��� �������
                  begin
                    if Assigned(nodePos.NodeByName('rst:Quantity')) then vPos.Quant:=stf(nodePos.NodeByName('rst:Quantity').Value);
//                    if Assigned(nodePos.NodeByName('rst:InformARegId')) then vPos.info_a:=nodePos.NodeByName('rst:InformARegId').Value;
//                    if Assigned(nodePos.NodeByName('rst:InformBRegId')) then vPos.info_b:=nodePos.NodeByName('rst:InformBRegId').Value;
                    if Assigned(nodePos.NodeByName('rst:Product')) then
                      if Assigned(nodePos.NodeByName('rst:Product').NodeByName('pref:AlcCode')) then vPos.Card.AlcCode:=nodePos.NodeByName('rst:Product').NodeByName('pref:AlcCode').Value;

                    if vPos.Card.AlcCode>'' then  //������� � �������� ���������� - ��������
                    begin
                      try
                        quS.Active:=False;
                        quS.SQL.Clear;
                        quS.SQL.Add('');
                        quS.SQL.Add('INSERT INTO [dbo].[ADOCSINV_SP] ([IDH],[ID],[CAP],[QUANTR],[QUANTF])');
                        quS.SQL.Add('     VALUES');
                        quS.SQL.Add('           ('+its(IDH));
                        quS.SQL.Add('           ,'+its(iC+1));
                        quS.SQL.Add('           ,'''+vPos.Card.AlcCode+'''');
                        quS.SQL.Add('           ,'+fts(vPos.Quant));
                        quS.SQL.Add('           ,0)');
                        quS.ExecSQL;
                      except
                        ShowMessageLogLocal('     ������ ���������� ('+its(iC+1)+','+vPos.Card.AlcCode+','+fts(vPos.Quant)+')');
                      end;
                      inc(iC);
                    end;
                  end;
                end;
                ShowMessageLogLocal('     - ���������� ����� '+its(iC)+' �������');
              end;
            end;
          end;
        finally
          Xml.Free;
        end;
      end;

      ShowMessageLogLocal('������� ��������.');
    end;
  end;
end;

procedure TfmUTMExch.acDecodeExecute(Sender: TObject);
var Rec:TcxCustomGridRecord;
    xmlstr:string;
    i,j,iNum,iC:Integer;
begin
  //������������� TTN
  if ViewReply.Controller.SelectedRecordCount>0 then
  begin
    ClearMessageLogLocal;
    ShowMessageLogLocal('����� ... ���� ��������� ����������.');
    iC:=0;

    for i:=0 to ViewReply.Controller.SelectedRecordCount-1 do
    begin
      Rec:=ViewReply.Controller.SelectedRecords[i];

      iNum:=0;

      for j:=0 to Rec.ValueCount-1 do
      begin
        if ViewReply.Columns[j].Name='ViewReplyID' then begin iNum:=Rec.Values[j]; Break; end;
      end;

      if (iNum>0) then
      begin
        if quReplyList.Locate('ID',iNum,[]) then
        begin
          if quReplyListIACTIVE.AsInteger<=3 then   //2   � ��� ��������
          begin
            xmlstr:=UTF8ToString(quReplyListReplyFile.AsString);

            if Pos('ReplyAP',xmlstr)>0 then //��� ������
            begin
              prWMemo(MemoLogLocal,'  '+quReplyListID.AsString+' - ���� ��������� �� �������.');
              if prDecodeCards(UTF8ToString(quReplyListReplyFile.AsString),dmR.FDConnection) then
              begin
                inc(iC);
                quReplyList.Edit;
                quReplyListIACTIVE.AsInteger:=3;
                quReplyList.Post;
              end;
            end;
            if (Pos('ns:WayBillAct',xmlstr)>0)or(Pos('ns:WAYBILLACT',xmlstr)>0) then //��� ��������
            begin
              prWMemo(MemoLogLocal,'  '+quReplyListID.AsString+' - ���� ��������� ���������.');
              if prDecodeAct(UTF8ToString(quReplyListReplyFile.AsString),iNum,dmR.FDConnection) then
              begin
                inc(iC);
                quReplyList.Edit;
                quReplyListIACTIVE.AsInteger:=3;
                quReplyList.Post;
              end;
            end;
            if ((Pos('ns:WayBill',xmlstr)>0)or(Pos('ns:WAYBILL',xmlstr)>0))and(Pos('ns:WayBillAct',xmlstr)=0) then //��� ��������
            begin
              prWMemo(MemoLogLocal,'  '+quReplyListID.AsString+' - ���� ��������� ���������.');
              if prDecodeTTN(UTF8ToString(quReplyListReplyFile.AsString),iNum,dmR.FDConnection,CommonSet.FSRAR_ID) then
              begin
                inc(iC);
                quReplyList.Edit;
                quReplyListIACTIVE.AsInteger:=3;
                quReplyList.Post;
              end;
            end;
            if Pos('ns:TTNInformBReg',xmlstr)>0 then //��� �������������� ���� � ���������
            begin
              prWMemo(MemoLogLocal,'  '+quReplyListID.AsString+' - ���� ��������� ��������������� �����.');
              if prDecodeWB(UTF8ToString(quReplyListReplyFile.AsString),iNum,dmR.FDConnection,CommonSet.FSRAR_ID) then
              begin
                inc(iC);

                quReplyList.Edit;
                quReplyListIACTIVE.AsInteger:=3;
                quReplyList.Post;
              end;
            end;
            if Pos('ns:Ticket',xmlstr)>0 then //��� �������������� ���� � ���������
            begin
              prWMemo(MemoLogLocal,'  '+quReplyListID.AsString+' - ���� ��������� �������.');
              if prDecodeTicket(UTF8ToString(quReplyListReplyFile.AsString),iNum,dmR.FDConnection,CommonSet.FSRAR_ID) then
              begin
                inc(iC);

                quReplyList.Edit;
                quReplyListIACTIVE.AsInteger:=3;
                quReplyList.Post;
              end;
            end;
            if Pos('ns:ReplyNoAnswerTTN',xmlstr)>0 then //��� ������ �������������� ���
            begin
              prWMemo(MemoLogLocal,'  '+quReplyListID.AsString+' - ���� ��������� �������������� ���.');
              if prDecodeNoAnswerTTN(UTF8ToString(quReplyListReplyFile.AsString),quReplyListID.AsInteger,dmR.FDConnection) then
              begin
                inc(iC);

                quReplyList.Edit;
                quReplyListIACTIVE.AsInteger:=3;
                quReplyList.Post;
              end;
            end;
            if Pos('ns:ReplyBarcode',xmlstr)>0 then //��� ������ ���������� ����������
            begin
              prWMemo(MemoLogLocal,'  '+quReplyListID.AsString+' - ���� ��������� ���������� ����������.');
              if prDecodeQueryBarCode(UTF8ToString(quReplyListReplyFile.AsString),quReplyListID.AsInteger,dmR.FDConnection) then
              begin
                inc(iC);

                quReplyList.Edit;
                quReplyListIACTIVE.AsInteger:=3;
                quReplyList.Post;
              end;
            end;

          end else  ShowMessageLogLocal('  '+quReplyListID.AsString+' - �������� ������');
        end;
      end;
    end;
    ShowMessage('���������� '+its(iC)+' ����������.');
    ShowMessageLogLocal('������� ��������.');
  end;
end;

procedure TfmUTMExch.acDecodeTovarExecute(Sender: TObject);
var Rec:TcxCustomGridRecord;
    xmlstr:string;
    i,j,iNum,iC:Integer;
begin
  //������������� ������
  if ViewReply.Controller.SelectedRecordCount>0 then
  begin
    ClearMessageLogLocal;
    ShowMessageLogLocal('����� ... ���� ��������� ����������.');
    iC:=0;

    for i:=0 to ViewReply.Controller.SelectedRecordCount-1 do
    begin
      Rec:=ViewReply.Controller.SelectedRecords[i];

      iNum:=0;

      for j:=0 to Rec.ValueCount-1 do
      begin
        if ViewReply.Columns[j].Name='ViewReplyID' then begin iNum:=Rec.Values[j]; Break; end;
      end;

      if (iNum>0) then
      begin
        if quReplyList.Locate('ID',iNum,[]) then
        begin
          if quReplyListIACTIVE.AsInteger<=2 then
          begin
            xmlstr:=UTF8ToString(quReplyListReplyFile.AsString);
            if Pos('ReplyAP',xmlstr)>0 then //��� ������
            begin
              ShowMessageLogLocal('  '+quReplyListID.AsString+' - ���� ���������.');
              if prDecodeCards(UTF8ToString(quReplyListReplyFile.AsString),dmR.FDConnection) then
              begin
                inc(iC);
                quReplyList.Edit;
                quReplyListIACTIVE.AsInteger:=3;
                quReplyList.Post;
              end;
            end else  ShowMessageLogLocal('  '+quReplyListID.AsString+' - �������� ���');
          end else  ShowMessageLogLocal('  '+quReplyListID.AsString+' - �������� ������');
        end;
      end;
    end;
    ShowMessage('���������� '+its(iC)+' ����������.');
    ShowMessageLogLocal('������� ��������.');
  end;
  {
  if quReplyList.RecordCount>0 then
  begin
    if prDecodeCards(UTF8ToString(quReplyListReplyFile.AsString),quS) then ShowMessage('������� �������� ��');
  end;
  }
end;

procedure TfmUTMExch.acDecodeTTNExecute(Sender: TObject);
var Rec:TcxCustomGridRecord;
    xmlstr:string;
    i,j,iNum,iC:Integer;
begin
  //������������� TTN
  if ViewReply.Controller.SelectedRecordCount>0 then
  begin
    ClearMessageLogLocal;
    ShowMessageLogLocal('����� ... ���� ��������� ����������.');
    iC:=0;

    for i:=0 to ViewReply.Controller.SelectedRecordCount-1 do
    begin
      Rec:=ViewReply.Controller.SelectedRecords[i];

      iNum:=0;

      for j:=0 to Rec.ValueCount-1 do
      begin
        if ViewReply.Columns[j].Name='ViewReplyID' then begin iNum:=Rec.Values[j]; Break; end;
      end;

      if (iNum>0) then
      begin
        if quReplyList.Locate('ID',iNum,[]) then
        begin
          if quReplyListIACTIVE.AsInteger<=3 then   //2   � ��� ��������
          begin
            xmlstr:=UTF8ToString(quReplyListReplyFile.AsString);
            if Pos('WayBill',xmlstr)>0 then //��� ��������
            begin
              ShowMessageLogLocal('  '+quReplyListID.AsString+' - ���� ���������.');
              if prDecodeTTN(UTF8ToString(quReplyListReplyFile.AsString),quReplyListID.AsInteger,dmR.FDConnection,CommonSet.FSRAR_ID) then
              begin
                inc(iC);
                quReplyList.Edit;
                quReplyListIACTIVE.AsInteger:=3;
                quReplyList.Post;
              end;
            end else  ShowMessageLogLocal('  '+quReplyListID.AsString+' - �������� ���');
          end else  ShowMessageLogLocal('  '+quReplyListID.AsString+' - �������� ������');
        end;
      end;
    end;
    ShowMessage('���������� '+its(iC)+' ����������.');
    ShowMessageLogLocal('������� ��������.');
  end;

end;

procedure TfmUTMExch.acDecodeWBExecute(Sender: TObject);
var Rec:TcxCustomGridRecord;
    xmlstr:string;
    i,j,iNum,iC:Integer;
begin
//������������� ns:TTNInformBReg
  if ViewReply.Controller.SelectedRecordCount>0 then
  begin
    ClearMessageLogLocal;
    ShowMessageLogLocal('����� ... ���� ��������� ����������.');
    iC:=0;

    for i:=0 to ViewReply.Controller.SelectedRecordCount-1 do
    begin
      Rec:=ViewReply.Controller.SelectedRecords[i];

      iNum:=0;

      for j:=0 to Rec.ValueCount-1 do
      begin
        if ViewReply.Columns[j].Name='ViewReplyID' then begin iNum:=Rec.Values[j]; Break; end;
      end;

      if (iNum>0) then
      begin
        if quReplyList.Locate('ID',iNum,[]) then
        begin
          if quReplyListIACTIVE.AsInteger<=2 then
          begin
            xmlstr:=UTF8ToString(quReplyListReplyFile.AsString);
            if Pos('TTNInformBReg',xmlstr)>0 then //��� �������������� ���� � ���������
            begin
              ShowMessageLogLocal('  '+quReplyListID.AsString+' - ���� ���������.');
              if prDecodeWB(UTF8ToString(quReplyListReplyFile.AsString),quReplyListID.AsInteger,dmR.FDConnection,CommonSet.FSRAR_ID) then
              begin
                inc(iC);
                quReplyList.Edit;
                quReplyListIACTIVE.AsInteger:=3;
                quReplyList.Post;
              end;
            end else  ShowMessageLogLocal('  '+quReplyListID.AsString+' - �������� ���');
          end else  ShowMessageLogLocal('  '+quReplyListID.AsString+' - �������� ������');
        end;
      end;
    end;
    ShowMessage('���������� '+its(iC)+' ����������.');
    ShowMessageLogLocal('������� ��������.');
  end;
end;

procedure TfmUTMExch.acDelFromUTM1Execute(Sender: TObject);
var Rec:TcxCustomGridRecord;
    xmlstr:string;
    i,j,iNum,iC:Integer;
    sPath:string;
    httpsend: THTTPSend;
    RARID:string;
begin
  //������� �� ��� �� ��������
  if ViewReply.Controller.SelectedRecordCount>0 then
  begin
    if MessageDlg('������� �� ��� ������ �� '+its(ViewReply.Controller.SelectedRecordCount)+' ������� ?',mtConfirmation, [mbYes, mbNo], 0, mbYes) = mrYes then
    begin
      ClearMessageLogLocal;
      ShowMessageLogLocal('����� ... ���� ��������� ����������.');
      iC:=0;

      for i:=0 to ViewReply.Controller.SelectedRecordCount-1 do
      begin
        Rec:=ViewReply.Controller.SelectedRecords[i];

        iNum:=0; RARID:='';

        for j:=0 to Rec.ValueCount-1 do
        begin
          if ViewReply.Columns[j].Name='ViewReplyID' then begin iNum:=Rec.Values[j]; end;
          if ViewReply.Columns[j].Name='ViewReplyFSRARID' then begin RARID:=Rec.Values[j]; end;
        end;

        if (iNum>0)and(RARID>'') then
        begin
          if quReplyList.Locate('ID',iNum,[]) then
          begin
            sPath:=quReplyListReplyPath.AsString;
            ShowMessageLogLocal('  - ��������: '+sPath);
            try
              httpsend:=THTTPSend.Create;
              if httpsend.HTTPMethod('DELETE',sPath) then
              begin
                ShowMessageLogLocal('    TRUE '+'ResultCode='+IntToStr(httpsend.ResultCode));

                quS.Active:=False;
                quS.SQL.Clear;
                quS.SQL.Add('');
                quS.SQL.Add('UPDATE dbo.REPLYLIST ');
                quS.SQL.Add('SET IDEL = 1');
                quS.SQL.Add('WHERE ReplyPath = '''+sPath+''' and FSRARID='''+RARID+'''');
                quS.ExecSQL;

              end else
              begin
                ShowMessageLogLocal('    FALSE '+'ResultCode='+IntToStr(httpsend.ResultCode));
              end;

              inc(iC);
            finally
              httpsend.Free;
            end;
          end;
        end;
      end;
      Init;
      ShowMessage('���������� '+its(iC)+' ����������.');
      ShowMessageLogLocal('������� ��������.');
    end;
  end;
end;

procedure TfmUTMExch.acDelFromUTM2Execute(Sender: TObject);
var Rec:TcxCustomGridRecord;
    xmlstr:string;
    i,j,iNum,iC:Integer;
    sPath:string;
    httpsend: THTTPSend;
    RARID:string;
begin
  //������� �� ��� �� ��������
  if ViewSend.Controller.SelectedRecordCount>0 then
  begin
    if MessageDlg('������� �� ��� ������ �� '+its(ViewSend.Controller.SelectedRecordCount)+' ������� ?',mtConfirmation, [mbYes, mbNo], 0, mbYes) = mrYes then
    begin
      ClearMessageLogLocal;
      ShowMessageLogLocal('����� ... ���� ��������� ����������.');
      iC:=0;

      for i:=0 to ViewSend.Controller.SelectedRecordCount-1 do
      begin
        Rec:=ViewSend.Controller.SelectedRecords[i];

        sPath:='';
        RARID:='';
        for j:=0 to Rec.ValueCount-1 do
        begin
          if ViewSend.Columns[j].Name='ViewSendRETVAL' then begin sPath:=Rec.Values[j]; end;
          if ViewSend.Columns[j].Name='ViewSendFSRARID' then begin RARID:=Rec.Values[j]; end;
        end;

        if (sPath>'')and(RARID>'') then
        begin
          ShowMessageLogLocal('  - ��������: '+sPath);
          try
            httpsend:=THTTPSend.Create;
            if httpsend.HTTPMethod('DELETE',sPath) then
            begin
              ShowMessageLogLocal('    TRUE '+'ResultCode='+IntToStr(httpsend.ResultCode));

              quS.Active:=False;
              quS.SQL.Clear;
              quS.SQL.Add('');
              quS.SQL.Add('UPDATE dbo.QULIST');
              quS.SQL.Add('SET IDEL = 1');
              quS.SQL.Add('WHERE RETVAL = '''+sPath+''' and FSRARID='''+RARID+'''');
              quS.ExecSQL;
            end else
            begin
              ShowMessageLogLocal('    FALSE '+'ResultCode='+IntToStr(httpsend.ResultCode));
            end;

            inc(iC);
          finally
            httpsend.Free;
          end;
        end;
      end;

      Init;

      ShowMessage('���������� '+its(iC)+' ����������.');
      ShowMessageLogLocal('������� ��������.');
    end;
  end;
end;

procedure TfmUTMExch.acDelListExecute(Sender: TObject);
var Rec:TcxCustomGridRecord;
    xmlstr:string;
    i,j,iNum,iC:Integer;
begin
  // �������� ���������� �����
  if ViewReply.Controller.SelectedRecordCount>0 then
  begin
    ClearMessageLogLocal;
    ShowMessageLogLocal('����� ... ���� �������� ���������.');

    Rec:=ViewReply.Controller.SelectedRecords[i];

    iNum:=0;

    for j:=0 to Rec.ValueCount-1 do
    begin
      if ViewReply.Columns[j].Name='ViewReplyID' then begin iNum:=Rec.Values[j]; Break; end;
    end;

    if (iNum>0) then
      if quReplyList.Locate('ID',iNum,[]) then quReplyList.Delete;

    ShowMessageLogLocal('������� ��������.');
  end;
end;

procedure TfmUTMExch.acRefreshExecute(Sender: TObject);
begin
  //������
  Init;
end;

procedure TfmUTMExch.acRemoveToArhExecute(Sender: TObject);
var Rec:TcxCustomGridRecord;
    i,j,iNum,iC:Integer;
    sList:string;
    RARID:string;
begin
  //������������� TTN
  if ViewReply.Controller.SelectedRecordCount>0 then
  begin
    ClearMessageLogLocal;
    ShowMessageLogLocal('����� ... ���� ��������� ����������.');
    iC:=0;
    sList:='';

    for i:=0 to ViewReply.Controller.SelectedRecordCount-1 do
    begin
      Rec:=ViewReply.Controller.SelectedRecords[i];

      iNum:=0;
      RARID:='';

      for j:=0 to Rec.ValueCount-1 do
      begin
        if ViewReply.Columns[j].Name='ViewReplyID' then begin iNum:=Rec.Values[j]; end;
        if ViewReply.Columns[j].Name='ViewReplyFSRARID' then begin RARID:=Rec.Values[j]; end;
      end;

      if (iNum>0) then
      begin
        sList:=sList+','+its(iNum);
        inc(iC);
      end;
    end;
    if (sList>'')and(RARID>'') then
    begin
      Delete(sList,1,1);
      ShowMessageLogLocal('  ������ - '+sList);

      try
        quS.Active:=False;
        quS.SQL.Clear;
        quS.SQL.Add('');
        quS.SQL.Add('INSERT INTO dbo.REPLYLISTARH ([FSRARID],IDREC,ReplyId,ReplyPath,ReplyFile,IACTIVE,IDEL,DateReply)');
        quS.SQL.Add('(SELECT FSRARID,ID,ReplyId,ReplyPath,ReplyFile,IACTIVE,IDEL,DateReply FROM dbo.REPLYLIST');
        quS.SQL.Add('  WHERE ID IN ('+sList+') and [FSRARID]='''+RARID+''')');
        quS.ExecSQL;

        quS.Active:=False;
        quS.SQL.Clear;
        quS.SQL.Add('');
        quS.SQL.Add('delete from dbo.REPLYLIST');
        quS.SQL.Add('WHERE ID IN ('+sList+') and [FSRARID]='''+RARID+'''');
        quS.ExecSQL;

        Init;
      except
      end;


    end;

    ShowMessage('���������� '+its(iC)+' ����������.');
    ShowMessageLogLocal('������� ��������.');
  end;
end;

procedure TfmUTMExch.acSaveToFileExecute(Sender: TObject);
begin
  //��������� � ����
  if quReplyList.RecordCount>0 then
  begin
    try
      ClearMessageLogLocal;
      quReplyListReplyFile.SaveToFile(CommonSet.TmpDir+quReplyListID.AsString+'.xml');
      ShowMessageLogLocal(CommonSet.TmpDir+quReplyListID.AsString+'.xml');
    except
      ShowMessage('������ ����������.');
    end;
  end;
end;

procedure TfmUTMExch.acSelectDateExecute(Sender: TObject);
begin
  //��������
  Init;
end;

procedure TfmUTMExch.acSendActRExecute(Sender: TObject);
begin
  //������������ ��� �����������
  with dmR do
  begin
    if quDocInHd.RecordCount>0 then
    begin
      if quDocInHdIACTIVE.AsInteger>=0 then
      begin
        // ����� ����������� ��� �����������
        if Trim(quDocInHdWBREGID.AsString)>'' then
        begin
           ClearMessageLogLocal;
           ShowMessageLogLocal('�����.. ���� ������������ ����.');

           if prSendAct(quDocInHdFSRARID.AsString,quDocInHdSID.AsString,quDocInHdIDATE.AsInteger,0,MemoLogLocal) then
           begin
             quDocInHd.Edit;
             quDocInHdIACTIVE.AsInteger:=1;
             quDocInHd.Post;
           end;

           ShowMessageLogLocal('������� ��������.');
        end else ShowMessage('������������ ���� ����������, �.�. ����������� ������������� �������� �� ��� � �����.');
      end else ShowMessage('�������� ������.');
    end;
  end;
end;

procedure TfmUTMExch.acTestCAPExecute(Sender: TObject);
type

     TCli = record
              CliType,ClientRegId,FullName,ShortName,INN,KPP,Country,RegionCode,description:string;
            end;

     TCard = record
               FullName,AlcCode:string;
               Capacity,AlcVolume:Real;
               ProductVCode:Integer;
               Prod:TCli;
               Impr:TCli;
             end;

     TPos = record
              Num:Integer;
              Quant,Price:Real;
              Card:TCard;
              info_a,info_b,party:string;
              sNum:string;
            end;

Var  xmlstr:string;
     iC:Integer;
     Xml: TNativeXml;
     nodeRoot,nodePos,nodeSpec,nodeProd: TXmlNode;
     IDH,iDateInv,i:integer;
     SFIXDATE:string;
     vPos:TPos;

  procedure ClearPos;
  begin
    vPos.Num:=0;
    vPos.Quant:=0; vPos.Price:=0;
    vPos.Card.FullName:=''; vPos.Card.AlcCode:=''; vPos.Card.Capacity:=0; vPos.Card.AlcVolume:=0; vPos.Card.ProductVCode:=0; vPos.info_a:=''; vPos.info_b:=''; vPos.party:='';
    vPos.Card.Prod.CliType:=''; //��� 2 ���� ��������� ��������������
    vPos.Card.Prod.ClientRegId:=''; vPos.Card.Prod.FullName:=''; vPos.Card.Prod.ShortName:=' '; vPos.Card.Prod.INN:=' '; vPos.Card.Prod.INN:=' '; vPos.Card.Prod.KPP:=' '; vPos.Card.Prod.Country:=' '; vPos.Card.Prod.RegionCode:=' '; vPos.Card.Prod.description:=' ';
    vPos.Card.Impr.ClientRegId:=''; vPos.Card.Impr.FullName:=''; vPos.Card.Impr.ShortName:=' '; vPos.Card.Impr.INN:=' '; vPos.Card.Impr.INN:=' '; vPos.Card.Impr.KPP:=' '; vPos.Card.Impr.Country:=' '; vPos.Card.Impr.RegionCode:=' '; vPos.Card.Impr.description:=' ';
    vPos.sNum:='';
  end;

begin
  // �������� ������� ��� � ���� �� ��������� �������� 2 �������
  with dmR do
  begin
    if fmUTMExch.quReplyList.RecordCount>0 then
    begin
      ClearMessageLogLocal;
      ShowMessageLogLocal('����� ... ���� ���������� ���������.');
      iC:=0;

      xmlstr:=UTF8ToString(fmUTMExch.quReplyListReplyFile.AsString);

//      if (Pos('ns:ReplyRests',xmlstr)>0)or(Pos('ns:ReplyRestsShop_v2',xmlstr)>0) then  // ��� �������
      if (Pos('ns:ReplyRestsShop_v2',xmlstr)>0) then  // ��� �������
      begin
        //������� ������������
        try
          Xml:= TNativeXml.Create(nil);
          Xml.XmlFormat := xfReadable;
//          while Pos('''',xmlstr)>0 do xmlstr[Pos('''',xmlstr)]:='"';

          Xml.ReadFromString(xmlstr);

          nodeRoot:=Xml.Root;
          IDH:=fGetId(6);
          iDateInv:=Trunc(Date)-1;

          if Assigned(nodeRoot.NodeByName('ns:Document')) then
          begin
            if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:ReplyRestsShop_v2')) then
            begin  // ������� (2 �������)
              if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:ReplyRestsShop_v2').NodeByName('rst:Products')) then //������������
              begin
                nodeSpec:=nodeRoot.NodeByName('ns:Document').NodeByName('ns:ReplyRestsShop_v2').NodeByName('rst:Products');

                ShowMessageLogLocal('  - �������� ������������ ( '+its(nodeSpec.NodeCount)+' ������� )');

                for i:=0 to nodeSpec.NodeCount-1 do
                begin
                  nodePos:=nodeSpec[i];

                  if i mod 100 = 0 then begin ShowMessageLogLocal('     - ���������� '+its(iC)+' �������'); delay(10); end;

                  ClearPos;

                  if Pos('rst:ShopPosition',nodePos.Name)>0 then  //��� �������
                  begin
                    if Assigned(nodePos.NodeByName('rst:Quantity')) then vPos.Quant:=stf(nodePos.NodeByName('rst:Quantity').Value);
                    if Assigned(nodePos.NodeByName('rst:Product')) then
                    begin
                      if Assigned(nodePos.NodeByName('rst:Product').NodeByName('pref:AlcCode')) then vPos.Card.AlcCode:=nodePos.NodeByName('rst:Product').NodeByName('pref:AlcCode').Value;
                      if Assigned(nodePos.NodeByName('rst:Product').NodeByName('pref:FullName')) then vPos.Card.FullName:=nodePos.NodeByName('rst:Product').NodeByName('pref:FullName').Value;
                      if Assigned(nodePos.NodeByName('rst:Product').NodeByName('pref:Capacity')) then vPos.Card.Capacity:=stf(nodePos.NodeByName('rst:Product').NodeByName('pref:Capacity').Value);
                      if Assigned(nodePos.NodeByName('rst:Product').NodeByName('pref:AlcVolume')) then vPos.Card.AlcVolume:=stf(nodePos.NodeByName('rst:Product').NodeByName('pref:AlcVolume').Value);
                      if Assigned(nodePos.NodeByName('rst:Product').NodeByName('pref:ProductVCode')) then vPos.Card.ProductVCode:=StrToIntDef(nodePos.NodeByName('rst:Product').NodeByName('pref:ProductVCode').Value,0);

                      if Assigned(nodePos.NodeByName('rst:Product').NodeByName('pref:Producer').NodeByName('oref:UL')) then vPos.Card.Prod.CliType:='UL';
                      if Assigned(nodePos.NodeByName('rst:Product').NodeByName('pref:Producer').NodeByName('oref:FL')) then vPos.Card.Prod.CliType:='FL';
                      if Assigned(nodePos.NodeByName('rst:Product').NodeByName('pref:Producer').NodeByName('oref:FO')) then vPos.Card.Prod.CliType:='FO';
                      if Assigned(nodePos.NodeByName('rst:Product').NodeByName('pref:Producer').NodeByName('oref:TS')) then vPos.Card.Prod.CliType:='TS';

                      if vPos.Card.Prod.CliType>'' then
                      begin
                        nodeProd:=nodePos.NodeByName('rst:Product').NodeByName('pref:Producer').NodeByName('oref:'+vPos.Card.Prod.CliType);

                        if Assigned(nodeProd.NodeByName('oref:INN')) then  vPos.Card.Prod.INN:=nodeProd.NodeByName('oref:INN').Value;
                        if Assigned(nodeProd.NodeByName('oref:KPP')) then  vPos.Card.Prod.KPP:=nodeProd.NodeByName('oref:KPP').Value;
                        if Assigned(nodeProd.NodeByName('oref:ClientRegId')) then  vPos.Card.Prod.ClientRegId:=nodeProd.NodeByName('oref:ClientRegId').Value;
                        if Assigned(nodeProd.NodeByName('oref:FullName')) then  vPos.Card.Prod.FullName:=nodeProd.NodeByName('oref:FullName').Value;
                        if Assigned(nodeProd.NodeByName('oref:ShortName')) then  vPos.Card.Prod.ShortName:=nodeProd.NodeByName('oref:ShortName').Value;

                        if Assigned(nodeProd.NodeByName('oref:address').NodeByName('oref:Country')) then  vPos.Card.Prod.Country:=nodeProd.NodeByName('oref:address').NodeByName('oref:Country').Value;
                        if Assigned(nodeProd.NodeByName('oref:address').NodeByName('oref:RegionCode')) then  vPos.Card.Prod.RegionCode:=nodeProd.NodeByName('oref:address').NodeByName('oref:RegionCode').Value;
                        if Assigned(nodeProd.NodeByName('oref:address').NodeByName('oref:description')) then  vPos.Card.Prod.description:=nodeProd.NodeByName('oref:address').NodeByName('oref:description').Value;

                        if Trim(vPos.Card.Prod.ClientRegId)>'' then
                        begin
                          try
                            quS.Active:=False;
                            quS.SQL.Clear;
                            quS.SQL.Add('');
                            quS.SQL.Add('DECLARE @PRODID varchar(50) = '''+Trim(vPos.Card.Prod.ClientRegId)+'''');
                            quS.SQL.Add('DECLARE @PRODINN varchar(20) = '''+vPos.Card.Prod.INN+'''');
                            quS.SQL.Add('DECLARE @PRODKPP varchar(20) = '''+vPos.Card.Prod.KPP+'''');
                            quS.SQL.Add('DECLARE @FULLNAME varchar(max) = '''+strtest(vPos.Card.Prod.FullName)+'''');
                            quS.SQL.Add('DECLARE @NAME varchar(max) = '''+strtest(vPos.Card.Prod.ShortName)+'''');
                            quS.SQL.Add('DECLARE @COUNTRY varchar(10) = '''+vPos.Card.Prod.Country+'''');
                            quS.SQL.Add('DECLARE @REGCODE varchar(10) = '''+vPos.Card.Prod.RegionCode+'''');
                            quS.SQL.Add('DECLARE @ADDR varchar(max) = '''+strtest(vPos.Card.Prod.description)+'''');
                            quS.SQL.Add('DECLARE @CLITYPE varchar(5) = '''+vPos.Card.Prod.CliType+'''');
                            quS.SQL.Add('EXECUTE [dbo].[prAddProd_v2] @PRODID,@PRODINN,@PRODKPP,@FULLNAME,@NAME,@COUNTRY,@REGCODE,@ADDR,@CLITYPE');
                            quS.ExecSQL;
                          except
                            ShowMessageLogLocal('     ������ ��������  '+vPos.Card.Prod.ClientRegId+' ������������� ');
                          end;
                        end;
                      end;

                      if Trim(vPos.Card.AlcCode)>'' then
                      begin
                        try
                          quS.Active:=False;
                          quS.SQL.Clear;
                          quS.SQL.Add('');
                          quS.SQL.Add('DECLARE @KAP varchar(50) = '''+Trim(vPos.Card.AlcCode)+'''');
                          quS.SQL.Add('DECLARE @NAME varchar(max) = '''+strtest(vPos.Card.FullName)+'''');
                          quS.SQL.Add('DECLARE @VOL real = '+fts(vPos.Card.Capacity));
                          quS.SQL.Add('DECLARE @KREP real = '+fts(vPos.Card.AlcVolume));
                          quS.SQL.Add('DECLARE @AVID int = '+its(vPos.Card.ProductVCode));
                          quS.SQL.Add('DECLARE @PROID varchar(50) = '''+Trim(vPos.Card.Prod.ClientRegId)+'''');
                          quS.SQL.Add('DECLARE @IMPID varchar(50) = '''+Trim(vPos.Card.Impr.ClientRegId)+'''');
                          quS.SQL.Add('EXECUTE [dbo].[prAddCard] @KAP,@NAME,@VOL,@KREP,@AVID,@PROID,@IMPID');
                          quS.ExecSQL;
                        except
                          ShowMessageLogLocal('     ������ ��������  '+vPos.Card.AlcCode+' ���� ');
                        end;
                      end;

                    end;

                    inc(iC);
                  end;
                end;
                ShowMessageLogLocal('     - ���������� ����� '+its(iC)+' �������');
              end;
            end;
          end;
        finally
          Xml.Free;
        end;
      end;

      ShowMessageLogLocal('������� ��������.');
    end;
  end;
end;

procedure TfmUTMExch.acUTMExchExecute(Sender: TObject);
begin
  //��� �����
end;

procedure TfmUTMExch.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
  fmUTMExch:=Nil;
end;

procedure TfmUTMExch.FormCreate(Sender: TObject);
begin
  deDateBeg.Date:=Date-1;
  deDateEnd.Date:=Date;
  ClearMessageLogLocal;
end;

procedure TfmUTMExch.deDateBegChange(Sender: TObject);
begin
  Init;
end;

end.
