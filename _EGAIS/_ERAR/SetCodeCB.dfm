object fmSetCodeCB: TfmSetCodeCB
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = #1057#1086#1087#1086#1089#1090#1072#1074#1083#1077#1085#1080#1077' '#1082#1086#1076#1072
  ClientHeight = 193
  ClientWidth = 440
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 24
    Top = 24
    Width = 57
    Height = 13
    AutoSize = False
    Caption = #1050#1040#1055
  end
  object Label2: TLabel
    Left = 24
    Top = 48
    Width = 48
    Height = 13
    Caption = #1053#1072#1079#1074#1072#1085#1080#1077
  end
  object Label3: TLabel
    Left = 24
    Top = 104
    Width = 37
    Height = 13
    Caption = #1050#1086#1076' '#1062#1041
  end
  object Label4: TLabel
    Left = 96
    Top = 24
    Width = 241
    Height = 13
    AutoSize = False
    Caption = 'Label4'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label5: TLabel
    Left = 96
    Top = 48
    Width = 321
    Height = 41
    AutoSize = False
    Caption = 'Label5'
    WordWrap = True
  end
  object Panel1: TPanel
    Left = 0
    Top = 135
    Width = 440
    Height = 58
    Align = alBottom
    BevelInner = bvLowered
    TabOrder = 0
    ExplicitTop = 143
    object cxButton1: TcxButton
      Left = 80
      Top = 8
      Width = 105
      Height = 41
      Caption = #1057#1086#1087#1086#1089#1090#1072#1074#1080#1090#1100
      Default = True
      ModalResult = 1
      TabOrder = 0
    end
    object cxButton2: TcxButton
      Left = 248
      Top = 8
      Width = 105
      Height = 41
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
    end
  end
  object cxCalcEdit1: TcxCalcEdit
    Left = 96
    Top = 101
    EditValue = 0
    TabOrder = 1
    Width = 105
  end
end
