object fmDocCB: TfmDocCB
  Left = 0
  Top = 0
  Caption = #1044#1086#1082#1091#1084#1077#1085#1090#1099' '#1062#1041
  ClientHeight = 549
  ClientWidth = 1112
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1112
    Height = 127
    ApplicationButton.Visible = False
    BarManager = dxBarManager1
    Style = rs2010
    ColorSchemeAccent = rcsaOrange
    ColorSchemeName = 'Blue'
    SupportNonClientDrawing = True
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1044#1086#1082#1091#1084#1077#1085#1090#1099' '#1062#1041
      Groups = <
        item
          Caption = #1042#1099#1073#1088#1072#1090#1100
          ToolbarName = 'brSelect'
        end
        item
          ToolbarName = 'bmApplyDate'
        end
        item
          ToolbarName = 'bmClose'
        end>
      Index = 0
    end
  end
  object GridCBDoc: TcxGrid
    Left = 0
    Top = 127
    Width = 1112
    Height = 422
    Align = alClient
    TabOrder = 5
    LevelTabs.Style = 8
    LookAndFeel.Kind = lfOffice11
    LookAndFeel.NativeStyle = False
    RootLevelOptions.DetailTabsPosition = dtpTop
    object ViewCBDoc: TcxGridDBTableView
      PopupMenu = PopupMenu1
      OnDblClick = ViewCBDocDblClick
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.Last.Visible = True
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.InfoPanel.Visible = True
      Navigator.Visible = True
      DataController.DataSource = dsquCBDocIn_HD
      DataController.KeyFieldNames = 'ID'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      Images = dmR.SmallImage
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsCustomize.DataRowSizing = True
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.MultiSelect = True
      OptionsView.ColumnAutoWidth = True
      OptionsView.ExpandButtonsForEmptyDetails = False
      OptionsView.GroupByBox = False
      object ViewCBDocID: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1076#1086#1082#1091#1084#1077#1085#1090#1072' '#1062#1041
        DataBinding.FieldName = 'Id'
      end
      object ViewCBDocSID: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1076#1086#1082#1091#1084#1077#1085#1090#1072' '#1045#1043#1040#1048#1057' (SID)'
        DataBinding.FieldName = 'SID'
        Width = 171
      end
      object ViewCBDocId_Depart: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1086#1090#1076#1077#1083#1072
        DataBinding.FieldName = 'Id_Depart'
      end
      object ViewCBDocName: TcxGridDBColumn
        Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082' '
        DataBinding.FieldName = 'Name'
        Width = 201
      end
      object ViewCBDocDepName: TcxGridDBColumn
        Caption = #1054#1090#1076#1077#1083
        DataBinding.FieldName = 'DepName'
        Width = 123
      end
      object ViewCBDocNum: TcxGridDBColumn
        Caption = #1053#1086#1084#1077#1088' '#1076#1086#1082'.'
        DataBinding.FieldName = 'DocNum'
        Width = 76
      end
      object ViewCBDocNUMBERRAR: TcxGridDBColumn
        Caption = #1053#1086#1084#1077#1088' '#1074' '#1045#1043#1040#1048#1057
        DataBinding.FieldName = 'NUMBERRAR'
        Width = 143
      end
      object ViewCBDocDocDate: TcxGridDBColumn
        Caption = #1044#1072#1090#1072' '#1076#1086#1082'.'
        DataBinding.FieldName = 'DocDate'
        Width = 92
      end
      object ViewCBDocSHIPDATE: TcxGridDBColumn
        Caption = #1044#1072#1090#1072' '#1087#1086#1089#1090#1072#1074#1082#1080' '#1045#1043#1040#1048#1057
        DataBinding.FieldName = 'SHIPDATE'
      end
      object ViewCBDocIACTIVE: TcxGridDBColumn
        Caption = #1057#1090#1072#1090#1091#1089' '#1086#1090#1087#1088#1072#1074#1082#1080' '#1076#1086#1082'. '#1074' '#1056#1040#1056
        DataBinding.FieldName = 'IACTIVE'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Images = dmR.SmallImage
        Properties.Items = <
          item
            Description = #1074' '#1088#1077#1076#1072#1082#1090'.'
            ImageIndex = 74
            Value = 0
          end
          item
            Description = #1054#1090#1087#1088#1072#1074#1083#1077#1085
            ImageIndex = 75
            Value = 1
          end
          item
            Description = #1053#1045' '#1055#1054#1051#1059#1063#1045#1053
            ImageIndex = 4
            Value = 99
          end>
      end
      object ViewCBDocSFNum: TcxGridDBColumn
        Caption = #1053#1086#1084#1077#1088' '#1057#1063#1060#1050
        DataBinding.FieldName = 'SFNum'
      end
      object ViewCBDocSumIn: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
        DataBinding.FieldName = 'SumIn'
      end
      object ViewCBDocSumOut: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1084#1072#1075#1072#1079#1080#1085#1072
        DataBinding.FieldName = 'SumOut'
      end
      object ViewCBDocId_Client: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
        DataBinding.FieldName = 'Id_Client'
      end
      object ViewCBDocFullName: TcxGridDBColumn
        Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082' '#1087#1086#1083#1085#1086#1077' '#1085#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'FullName'
        Width = 229
      end
    end
    object ViewCBSpec: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      DataController.DataSource = dsquCBDocsIn_SP
      DataController.DetailKeyFieldNames = 'Id_TTN'
      DataController.MasterKeyFieldNames = 'Id'
      DataController.Options = [dcoAssignGroupingValues]
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'Quant'
          Column = ViewCBQuant
        end
        item
          Format = #1050#1086#1083'-'#1074#1086': ###'
          Kind = skCount
          FieldName = 'ID'
          Column = ViewCBSpecID
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SumIn'
          Column = ViewCBSpecSumIn
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.ExpandButtonsForEmptyDetails = False
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      Styles.Footer = dmR.stlBold
      object ViewCBSpecID: TcxGridDBColumn
        Caption = #8470' '#1087#1087
        DataBinding.FieldName = 'Id'
      end
      object ViewCBSpecArticul: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1090#1086#1074#1072#1088#1072
        DataBinding.FieldName = 'Articul'
      end
      object ViewCBSpecName: TcxGridDBColumn
        Caption = #1058#1086#1074#1072#1088' '#1085#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'Name'
      end
      object ViewCBSpecEdIzm: TcxGridDBColumn
        Caption = #1077#1076'.'#1080#1079#1084'.'
        DataBinding.FieldName = 'EdIzm'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Items = <
          item
            Description = #1096#1090'.'
            ImageIndex = 0
            Value = 1
          end
          item
            Description = #1082#1075'.'
            Value = 2
          end>
      end
      object ViewCBQuant: TcxGridDBColumn
        Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086
        DataBinding.FieldName = 'Quant'
        Styles.Content = dmR.stlBold
      end
      object ViewCBSpecPriceIn: TcxGridDBColumn
        Caption = #1062#1077#1085#1072' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
        DataBinding.FieldName = 'PriceIn'
      end
      object ViewCBSpecSumIn: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
        DataBinding.FieldName = 'SumIn'
      end
      object ViewCBSpecPriceOut: TcxGridDBColumn
        Caption = #1062#1077#1085#1072' '#1084#1072#1075#1072#1079#1080#1085#1072
        DataBinding.FieldName = 'PriceOut'
      end
      object ViewCBSpecSumOut: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1084#1072#1075#1072#1079#1080#1085#1072
        DataBinding.FieldName = 'SumOut'
      end
      object ViewCBSpecFullName: TcxGridDBColumn
        Caption = #1058#1086#1074#1072#1088' '#1087#1086#1083#1085#1086#1077' '#1085#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'FullName'
      end
      object ViewCBSpecId_TTN: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
        DataBinding.FieldName = 'Id_TTN'
        Visible = False
      end
    end
    object LevelCBDoc: TcxGridLevel
      Caption = #1042#1093#1086#1076#1103#1097#1080#1077
      GridView = ViewCBDoc
      Options.TabsForEmptyDetails = False
      object LevelCBSpec: TcxGridLevel
        GridView = ViewCBSpec
        Options.DetailTabsPosition = dtpLeft
        Options.TabsForEmptyDetails = False
      end
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      #1044#1077#1081#1089#1090#1074#1080#1103)
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmR.SmallImage
    ImageOptions.LargeImages = dmR.LargeImage
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 788
    Top = 52
    DockControlHeights = (
      0
      0
      0
      0)
    object bmApplyDate: TdxBar
      Caption = #1042#1099#1073#1086#1088' '#1087#1077#1088#1080#1086#1076#1072
      CaptionButtons = <>
      DockedLeft = 79
      DockedTop = 0
      FloatLeft = 778
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          UserDefine = [udWidth]
          UserWidth = 75
          Visible = True
          ItemName = 'deDateBeg'
        end
        item
          UserDefine = [udWidth]
          UserWidth = 74
          Visible = True
          ItemName = 'deDateEnd'
        end
        item
          Visible = True
          ItemName = 'btnDone'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object bmClose: TdxBar
      Caption = #1047#1072#1082#1088#1099#1090#1100
      CaptionButtons = <>
      DockedLeft = 277
      DockedTop = 0
      FloatLeft = 778
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'btnClose'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object brSelect: TdxBar
      Caption = #1042#1099#1073#1088#1072#1090#1100
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 1146
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object btnDone: TdxBarLargeButton
      Action = acRefresh
      Category = 0
    end
    object btnClose: TdxBarLargeButton
      Action = acClose
      Category = 0
    end
    object deDateBeg: TdxBarDateCombo
      Caption = 'C  '
      Category = 0
      Hint = 'C  '
      Visible = ivAlways
      OnChange = deDateBegChange
      ImageIndex = 25
      ShowDayText = False
    end
    object deDateEnd: TdxBarDateCombo
      Caption = #1087#1086
      Category = 0
      Hint = #1087#1086
      Visible = ivAlways
      OnChange = deDateBegChange
      ImageIndex = 25
      ShowDayText = False
    end
    object cbItem1: TcxBarEditItem
      Caption = 'XMLNotePad'
      Category = 0
      Hint = 'XMLNotePad'
      Visible = ivAlways
      ShowCaption = True
      Width = 0
      PropertiesClassName = 'TcxCheckBoxProperties'
      Properties.ImmediatePost = True
      InternalEditValue = False
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = acCards
      Category = 0
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = 'New SubItem'
      Category = 0
      Visible = ivAlways
      ItemLinks = <>
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = acUTMExch
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = acSelect
      Category = 0
    end
    object dxBarGroup1: TdxBarGroup
      Items = ()
    end
  end
  object ActionList1: TActionList
    Images = dmR.SmallImage
    Left = 560
    Top = 228
    object acRefresh: TAction
      Category = #1060#1086#1088#1084#1072
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100' (F5)'
      Hint = #1054#1073#1085#1086#1074#1080#1090#1100' (F5)'
      ImageIndex = 5
      ShortCut = 116
      OnExecute = acRefreshExecute
    end
    object acClose: TAction
      Category = #1060#1086#1088#1084#1072
      Caption = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 100
      OnExecute = acCloseExecute
    end
    object acSelectDate: TAction
      Category = #1060#1086#1088#1084#1072
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      Hint = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      ImageIndex = 1
      OnExecute = acSelectDateExecute
    end
    object acDecodeTovar: TAction
      Category = #1044#1077#1082#1086#1076#1080#1088#1086#1074#1072#1085#1080#1077
      Caption = #1044#1077#1082#1086#1076#1080#1088#1086#1074#1072#1090#1100' '#1082#1072#1082' '#1090#1086#1074#1072#1088
      ImageIndex = 38
    end
    object acSaveToFile: TAction
      Category = #1044#1077#1082#1086#1076#1080#1088#1086#1074#1072#1085#1080#1077
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1074' '#1092#1072#1081#1083
      ImageIndex = 7
    end
    object acDelList: TAction
      Category = #1044#1077#1082#1086#1076#1080#1088#1086#1074#1072#1085#1080#1077
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1074#1099#1076#1077#1083#1077#1085#1085#1099#1077' '#1089#1090#1088#1086#1082#1080
      ImageIndex = 4
      ShortCut = 16452
    end
    object acDecodeTTN: TAction
      Category = #1044#1077#1082#1086#1076#1080#1088#1086#1074#1072#1085#1080#1077
      Caption = #1044#1077#1082#1086#1076#1080#1088#1086#1074#1072#1090#1100' '#1082#1072#1082' '#1076#1086#1082#1091#1084#1077#1085#1090
      ImageIndex = 38
    end
    object acCards: TAction
      Category = #1060#1086#1088#1084#1072
      Caption = #1050#1072#1088#1090#1086#1095#1082#1080
      ImageIndex = 11
    end
    object acSendActR: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1057#1092#1086#1088#1084#1080#1088#1086#1074#1072#1090#1100' '#1072#1082#1090' '#1088#1072#1089#1093#1086#1078#1076#1077#1085#1080#1103
      ImageIndex = 49
    end
    object acDecodeWB: TAction
      Category = #1044#1077#1082#1086#1076#1080#1088#1086#1074#1072#1085#1080#1077
      Caption = 'acDecodeWB'
      ImageIndex = 38
    end
    object acUTMExch: TAction
      Category = #1060#1086#1088#1084#1072
      Caption = #1059#1058#1052' '#1086#1073#1084#1077#1085
      ImageIndex = 130
    end
    object acSelect: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1042#1099#1073#1088#1072#1090#1100' '#1076#1086#1082#1091#1084#1077#1085#1090
      ImageIndex = 1
      OnExecute = acSelectExecute
    end
  end
  object dsquCBDocIn_HD: TDataSource
    DataSet = quCBDocIn_HD
    Left = 328
    Top = 408
  end
  object PopupMenu1: TPopupMenu
    Images = dmR.SmallImage
    Left = 656
    Top = 248
  end
  object dsquCBDocsIn_SP: TDataSource
    DataSet = quCBDocIn_SP
    Left = 200
    Top = 408
  end
  object quCBDocIn_HD: TFDQuery
    Connection = dmR.FDConnection
    SQL.Strings = (
      'declare @DATEB datetime = :DATEB'
      'declare @DATEE datetime = :DATEE'
      'declare @SDEP varchar(200) = :SDEP'
      ''
      'declare @TDEP table ([IDEP] INT)'
      
        'insert into @TDEP ([IDEP]) SELECT Value FROM [dbo].[SplitString]' +
        '(@SDEP,'#39','#39') '
      ''
      'SELECT '
      '      hd.[Id]'
      '      ,[IDSKL] as Id_Depart'
      
        #9'  ,(dep.Name+'#39' ( '#1084#1072#1075'. '#39'+cast(dep.IDS as varchar)+'#39' )'#39') as DepNa' +
        'me'
      '      ,hd.[NUMDOC] as DocNum'
      '      ,hd.[DATEDOC] as DocDate'
      '      ,dep.IDS as Id_Shop'
      '      ,hd.IDCLI as Id_Client'
      #9'  ,cli.Name'
      #9'  ,isnull(rarhd.IACTIVE,99) as IACTIVE'
      '      ,rarhd.NUMBER as NUMBERRAR'
      '      ,rarhd.SID'
      '      ,rarhd.SHIPDATE'
      '      ,hd.NUMSF as SFNum'
      '      ,hd.SUMIN as SumIn'
      '      ,hd.SUMUCH as SumOut'
      #9'  ,cli.FullName'
      '  FROM [MCRYSTAL].[dbo].[DOCIN_HEAD] hd'
      
        '  left join [MCRYSTAL].[dbo].[Departs] dep on dep.Id=hd.[IDSKL] ' +
        'and dep.IStatus=1'
      '  left join [MCRYSTAL].[dbo].[Clients] cli on cli.Id=hd.[IDCLI] '
      
        '  left join [RAR].[dbo].[ADOCSIN_HD] rarhd on rarhd.IDHEAD=hd.[I' +
        'd]'
      '  where  '
      '    hd.[IDSKL] in (Select [IDEP] from @TDEP)'
      #9'and hd.[IDATEDOC]>=[dbo].d2i(@DATEB)'
      #9'and hd.[IDATEDOC]<=[dbo].d2i(@DATEE)'
      '  order by cli.Name,hd.[IDSKL],hd.[IDATEDOC],hd.[Id]')
    Left = 328
    Top = 344
    ParamData = <
      item
        Name = 'DATEB'
        DataType = ftDateTime
        ParamType = ptInput
        Value = 42401d
      end
      item
        Name = 'DATEE'
        DataType = ftDateTime
        ParamType = ptInput
        Value = 42415d
      end
      item
        Name = 'SDEP'
        DataType = ftString
        ParamType = ptInput
        Value = '1061,1071,1081,1091,1121'
      end>
    object quCBDocIn_HDId: TLargeintField
      FieldName = 'Id'
      Origin = 'Id'
      Required = True
    end
    object quCBDocIn_HDId_Depart: TIntegerField
      FieldName = 'Id_Depart'
      Origin = 'Id_Depart'
      Required = True
    end
    object quCBDocIn_HDDepName: TStringField
      FieldName = 'DepName'
      Origin = 'DepName'
      ReadOnly = True
      Size = 140
    end
    object quCBDocIn_HDDocNum: TStringField
      FieldName = 'DocNum'
      Origin = 'DocNum'
      Size = 30
    end
    object quCBDocIn_HDDocDate: TSQLTimeStampField
      FieldName = 'DocDate'
      Origin = 'DocDate'
    end
    object quCBDocIn_HDId_Shop: TIntegerField
      FieldName = 'Id_Shop'
      Origin = 'Id_Shop'
    end
    object quCBDocIn_HDId_Client: TIntegerField
      FieldName = 'Id_Client'
      Origin = 'Id_Client'
    end
    object quCBDocIn_HDName: TStringField
      FieldName = 'Name'
      Origin = 'Name'
      Size = 100
    end
    object quCBDocIn_HDIACTIVE: TSmallintField
      FieldName = 'IACTIVE'
      Origin = 'IACTIVE'
      ReadOnly = True
      Required = True
    end
    object quCBDocIn_HDNUMBERRAR: TStringField
      FieldName = 'NUMBERRAR'
      Origin = 'NUMBERRAR'
      Size = 100
    end
    object quCBDocIn_HDSID: TStringField
      FieldName = 'SID'
      Origin = 'SID'
      Size = 50
    end
    object quCBDocIn_HDSHIPDATE: TStringField
      FieldName = 'SHIPDATE'
      Origin = 'SHIPDATE'
    end
    object quCBDocIn_HDSFNum: TStringField
      FieldName = 'SFNum'
      Origin = 'SFNum'
      Size = 30
    end
    object quCBDocIn_HDSumIn: TSingleField
      FieldName = 'SumIn'
      Origin = 'SumIn'
      DisplayFormat = '0.00'
    end
    object quCBDocIn_HDSumOut: TSingleField
      FieldName = 'SumOut'
      Origin = 'SumOut'
      DisplayFormat = '0.00'
    end
    object quCBDocIn_HDFullName: TStringField
      FieldName = 'FullName'
      Origin = 'FullName'
      Size = 200
    end
  end
  object quCBDocIn_SP: TFDQuery
    Connection = dmR.FDConnection
    SQL.Strings = (
      'declare @DATEB datetime = :DATEB'
      'declare @DATEE datetime = :DATEE'
      'declare @SDEP varchar(200) = :SDEP'
      ''
      'declare @TDEP table ([IDEP] INT)'
      
        'insert into @TDEP ([IDEP]) SELECT Value FROM [dbo].[SplitString]' +
        '(@SDEP,'#39','#39') '
      ''
      'SELECT Id_Shop=dep.IDS'
      '      ,Id_Depart=hd.[IDSKL]'
      '      ,Id_TTN=hd.ID'
      '      ,Id=sp.[NUM]'
      '      ,Articul=sp.[IDCARD]'
      #9'  ,gd.[Name]'
      #9'  ,gd.[FullName]'
      '      ,gd.EdIzm'
      '      ,sp.[Quant]'
      '      ,sp.[PriceIn]'
      '      ,PriceOut=sp.[PRICEUCH]'
      '      ,sp.[SumIn]'
      '      ,SumOut=sp.[SUMUCH]'
      '  FROM [MCRYSTAL].[dbo].[DOCIN_SPEC] sp'
      
        '    left join [MCRYSTAL].[dbo].[DOCIN_HEAD] hd on hd.ID=sp.[IDHE' +
        'AD]'
      
        '    left join [MCRYSTAL].[dbo].[Clients] cli on cli.Id=hd.[IDCLI' +
        ']'
      #9'left join [MCRYSTAL].[dbo].[CARDS] gd on gd.Id=sp.[IDCARD]'
      #9'left join [MCRYSTAL].[dbo].[DEPARTS] dep on dep.ID=hd.[IDSKL]'
      '  where hd.[IDSKL] in (Select [IDEP] from @TDEP)'
      '  and hd.[IDATEDOC]>=[dbo].d2i(@DATEB)'
      '  and hd.[IDATEDOC]<=[dbo].d2i(@DATEE)'
      '  order by sp.[IDHEAD],sp.[Id]')
    Left = 200
    Top = 344
    ParamData = <
      item
        Name = 'DATEB'
        DataType = ftDateTime
        ParamType = ptInput
        Value = 42401d
      end
      item
        Name = 'DATEE'
        DataType = ftDateTime
        ParamType = ptInput
        Value = 42415d
      end
      item
        Name = 'SDEP'
        DataType = ftString
        ParamType = ptInput
        Value = '1061,1071,1081,1091,1121'
      end>
    object quCBDocIn_SPId_Shop: TIntegerField
      FieldName = 'Id_Shop'
      Origin = 'Id_Shop'
    end
    object quCBDocIn_SPId_Depart: TIntegerField
      FieldName = 'Id_Depart'
      Origin = 'Id_Depart'
    end
    object quCBDocIn_SPId_TTN: TLargeintField
      FieldName = 'Id_TTN'
      Origin = 'Id_TTN'
    end
    object quCBDocIn_SPId: TIntegerField
      FieldName = 'Id'
      Origin = 'Id'
    end
    object quCBDocIn_SPArticul: TIntegerField
      FieldName = 'Articul'
      Origin = 'Articul'
    end
    object quCBDocIn_SPName: TStringField
      FieldName = 'Name'
      Origin = 'Name'
      Size = 200
    end
    object quCBDocIn_SPFullName: TStringField
      FieldName = 'FullName'
      Origin = 'FullName'
      Size = 200
    end
    object quCBDocIn_SPEdIzm: TSmallintField
      FieldName = 'EdIzm'
      Origin = 'EdIzm'
    end
    object quCBDocIn_SPQuant: TSingleField
      FieldName = 'Quant'
      Origin = 'Quant'
    end
    object quCBDocIn_SPPriceIn: TFloatField
      FieldName = 'PriceIn'
      Origin = 'PriceIn'
    end
    object quCBDocIn_SPPriceOut: TFloatField
      FieldName = 'PriceOut'
      Origin = 'PriceOut'
    end
    object quCBDocIn_SPSumIn: TSingleField
      FieldName = 'SumIn'
      Origin = 'SumIn'
    end
    object quCBDocIn_SPSumOut: TSingleField
      FieldName = 'SumOut'
      Origin = 'SumOut'
    end
  end
end
