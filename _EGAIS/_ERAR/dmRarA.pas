unit dmRarA;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool,
  FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.MSSQL, FireDAC.Phys.MSSQLDef, FireDAC.VCLUI.Wait, FireDAC.Comp.UI, FireDAC.Phys.ODBCBase, FireDAC.Comp.Client, Data.DB,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt, FireDAC.Comp.DataSet;

type
  TdmRA = class(TDataModule)
    FDConnection: TFDConnection;
    FDTrans: TFDTransaction;
    FDPhysMSSQLDriverLink: TFDPhysMSSQLDriverLink;
    FDTransUpdate: TFDTransaction;
    FDGUIxWaitCursor: TFDGUIxWaitCursor;
    quPoint: TFDQuery;
    quPointRARID: TStringField;
    quPointISHOP: TIntegerField;
    quPointIPUTM: TStringField;
    quPointIPDB: TStringField;
    quPointDBNAME: TStringField;
    quPointPASSW: TStringField;
    quPointINN: TStringField;
    quPointKPP: TStringField;
    quPointSHORTNAME: TStringField;
    quPointFULLNAME: TStringField;
    quPointCOUNTRY: TStringField;
    quPointDESCR: TStringField;
    quPointNAMESHOP: TStringField;
    quPointISS: TSmallintField;
    quPointISPRODUCTION: TSmallintField;
    quPointSDEP: TStringField;
    quPointIACTIVE: TSmallintField;
    quPointWRITELOG: TSmallintField;
  private
    { Private declarations }
  public
    { Public declarations }
    FAppName: string;
    FServer: string;
    function  DBConnectUDL: boolean;
    function  DBConnectPool: boolean;
  end;

var
  dmRA: TdmRA;

const
  ConstConnectionDef='MSSQL_ConnectionDef';

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

function  TdmRA.DBConnectPool: boolean;
var UDLData: TStrings;
    Params: TStrings;
    CurDir : string;
    Database, Server, User_Name, Password: string;
    DefParams: TStrings;
begin
  Result:=False;

  //����� �� ��������� ����� ���� � ����������� ���������� MSSQL � FireDAC
  //c������ ������ ������������ ����� udl �� ADO
  CurDir := ExtractFilePath(ParamStr(0));
  if FileExists(CurDir+'Rar.udl')=False then Exit;

  UDLData := TStringList.Create;
  Params := TStringList.Create;
  DefParams := TStringList.Create;
  try
    UDLData.LoadFromFile(CurDir+'Rar.udl');

    //<--������ ��������� ����������� ;
    Params.Delimiter:=';';
    Params.StrictDelimiter := True;
    Params.DelimitedText:=UDLData.Text;
    Server:=trim(Params.Values['Data Source']);
    Database:=trim(Params.Values['Initial Catalog']);
    User_Name:=trim(Params.Values['User ID']);
    Password:=trim(Params.Values['Password']);
    //-->

    FServer:=Server;
    FAppName:=ExtractFileName(ParamStr(0));

    //<--������� FireDAC Private ���������� � ������ �� ��������� ConstConnectionDef
    DefParams.Values['SERVER']:=Server;
    DefParams.Values['DATABASE']:=Database;
    DefParams.Values['User_Name']:=User_Name;
    DefParams.Values['Password']:=Password;
    DefParams.Values['ApplicationName']:=FAppName;
    DefParams.Values['Workstation']:='';
    DefParams.Values['DriverID']:='MSSQL';
    DefParams.Values['Pooled']:='True';        //��������� Pool
    DefParams.Values['MARS']:='yes';
    FDManager.AddConnectionDef(ConstConnectionDef,'MSSQL',DefParams);   //FDManager ����������

    FDConnection.Connected:=False;
    FDConnection.Params.Clear;   //����������� ������� �� ����������, ����� Pool �� ���������
    FDConnection.ConnectionDefName:=ConstConnectionDef;   //��������� ��������� ����������
    FDConnection.Connected:=True;
    //-->
  finally
    UDLData.Free;
    Params.Free;
    DefParams.Free;
  end;

  Sleep(100);
  Result:=FDConnection.Connected;
end;


function  TdmRA.DBConnectUDL: boolean;
var UDLData: TStrings;
    Params: TStrings;
    CurDir : string;
    Database, Server, User_Name, Password: string;
begin
  Result:=False;
  //����� �� ��������� ����� ���� � ����������� ���������� MSSQL � FireDAC
  //c������ ������ ������������ ����� udl �� ADO
  CurDir := ExtractFilePath(ParamStr(0));
  if FileExists(CurDir+'Rar.udl')=False then Exit;

  UDLData := TStringList.Create;
  Params := TStringList.Create;
  try
    UDLData.LoadFromFile(CurDir+'Rar.udl');

    //<--������ ��������� ����������� ;
    Params.Delimiter:=';';
    Params.StrictDelimiter := True;
    Params.DelimitedText:=UDLData.Text;
    Server:=trim(Params.Values['Data Source']);
    Database:=trim(Params.Values['Initial Catalog']);
    User_Name:=trim(Params.Values['User ID']);
    Password:=trim(Params.Values['Password']);
    //-->

    //<--��������� ��������� ����������
    FDConnection.Close;
    FDConnection.Params.Values['SERVER']:=Server;
    FDConnection.Params.Values['DATABASE']:=Database;
    FDConnection.Params.Values['User_Name']:=User_Name;
    FDConnection.Params.Values['Password']:=Password;
    FDConnection.Params.Values['ApplicationName']:=ExtractFileName(ParamStr(0));
    FDConnection.Params.Values['Workstation']:='';
    //FDConnection.Params.Values['MARS']:='yes';
    FDConnection.Params.Values['DriverID']:='MSSQL';
    FDConnection.Connected:=True;

    //-->

  finally
    UDLData.Free;
    Params.Free;
  end;

  Sleep(100);
  Result:=FDConnection.Connected;
end;


end.
