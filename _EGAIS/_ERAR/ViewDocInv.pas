unit ViewDocInv;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  CustomChildForm, System.UITypes, ShellApi, httpsend, synautil, nativexml, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxRibbonSkins, dxRibbonCustomizationForm, cxContainer, cxEdit, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator,
  Data.DB, cxDBData, cxImageComboBox, cxTextEdit, cxDBLookupComboBox, cxCheckBox, cxCalendar, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.Menus, System.Actions, Vcl.ActnList, dxBar, cxBarEditItem, dxBarExtItems, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid, cxMemo, Vcl.ExtCtrls, dxRibbon;

type
  TfmDocInv = class(TfmCustomChildForm)
    dxRibbon1: TdxRibbon;
    dxRibbon1Tab1: TdxRibbonTab;
    dxBarManager1: TdxBarManager;
    bmApplyDate: TdxBar;
    bmClose: TdxBar;
    btnDone: TdxBarLargeButton;
    btnClose: TdxBarLargeButton;
    deDateBeg: TdxBarDateCombo;
    deDateEnd: TdxBarDateCombo;
    dxBarGroup1: TdxBarGroup;
    ActionList1: TActionList;
    acRefresh: TAction;
    acClose: TAction;
    acSelectDate: TAction;
    GridDocInv: TcxGrid;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    beiPoint: TcxBarEditItem;
    acExpExcel: TAction;
    dxBarLargeButton9: TdxBarLargeButton;
    dxBarLargeButton10: TdxBarLargeButton;
    beiGetTTN: TcxBarEditItem;
    dxBarLargeButton14: TdxBarLargeButton;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    LevelDocInv: TcxGridLevel;
    ViewDocInv: TcxGridDBTableView;
    pmDocInv: TPopupMenu;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton11: TdxBarLargeButton;
    acGetRemReg1: TAction;
    acGetRemReg2: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    ViewDocInvFSRARID: TcxGridDBColumn;
    ViewDocInvDOCDATE: TcxGridDBColumn;
    ViewDocInvID: TcxGridDBColumn;
    ViewDocInvSTYPE: TcxGridDBColumn;
    ViewDocInvFIXDATE: TcxGridDBColumn;
    ViewDocInvXML: TcxGridDBColumn;
    acCreateVnDoc: TAction;
    N1: TMenuItem;
    acSetFactQuant: TAction;
    N2: TMenuItem;
    acCreateCorr: TAction;
    N3: TMenuItem;
    acCreateCopyWithOutBeer: TAction;
    N4: TMenuItem;
    ViewDocInvIDREPLY: TcxGridDBColumn;
    procedure acRefreshExecute(Sender: TObject);
    procedure acCloseExecute(Sender: TObject);
    procedure acSelectDateExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure deDateBegChange(Sender: TObject);
    procedure acExpExcelExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acGetRemReg1Execute(Sender: TObject);
    procedure acGetRemReg2Execute(Sender: TObject);
    procedure ViewDocInvDblClick(Sender: TObject);
    procedure acCreateVnDocExecute(Sender: TObject);
    procedure acSetFactQuantExecute(Sender: TObject);
    procedure acCreateCorrExecute(Sender: TObject);
    procedure acCreateCopyWithOutBeerExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }

    procedure Init;
  end;

var
  fmDocInv: TfmDocInv;

procedure ShowFormDocInv;

implementation

{$R *.dfm}

uses ViewXML, EgaisDecode, ViewCards, dmRar, UTMExch, ViewSpecOut, MCrystDocs, ProDocs, History, UTMArh, UTMCheck, DocHeaderCB,
  ViewSpec, Main, ViewSpecInv, ViewDocVn, ViewDocCorr;

procedure ShowFormDocInv;
begin
  if Assigned(fmDocInv)=False then //����� �� ����������, � ����� �������
    fmDocInv:=TfmDocInv.Create(Application);
  if fmDocInv.WindowState=wsMinimized then //���� ���� ��������, �� ������������� ���
    fmDocInv.WindowState:=wsNormal;

  fmDocInv.deDateBeg.Date:=Date-7;
  fmDocInv.deDateEnd.Date:=Date;
  fmDocInv.Init;
  fmDocInv.Show;
end;

procedure TfmDocInv.Init;
var
  FocusedRow, TopRow: Integer;
  flag: boolean;
begin
  with dmR do
  begin
    ViewDocInv.BeginUpdate;
    try
      //<--Refresh � ��������������� ������� ������� � �������
      flag:=quDocsInv.Active;  //���������� ���� �� ������� �������, ��� �������������� ������� �������
      TopRow := ViewDocInv.Controller.TopRowIndex;
      FocusedRow := ViewDocInv.DataController.FocusedRowIndex;

      quDocsInv.Active:=False;
      quDocsInv.ParamByName('IDATEB').AsInteger:=Trunc(deDateBeg.Date);
      quDocsInv.ParamByName('IDATEE').AsInteger:=Trunc(deDateEnd.Date);
      quDocsInv.ParamByName('RARID').AsString:=CommonSet.FSRAR_ID;
      quDocsInv.Active:=True;
      quDocsInv.First;

    finally
      ViewDocInv.EndUpdate;
      //<--��������������� �������
      if flag then begin
        ViewDocInv.DataController.FocusedRowIndex := FocusedRow;
        ViewDocInv.Controller.TopRowIndex := TopRow;
      end;
    end;
  end;

//  FSRAR_ID:=CommonSet.FSRAR_ID;
end;


procedure TfmDocInv.ViewDocInvDblClick(Sender: TObject);
begin
  // ������������
  with dmR do
  begin
    if quDocsInv.RecordCount>0 then
    begin
      if (ViewDocInv.Controller.FocusedColumn.Name='ViewDocInvXML') then
      begin
        if (Abs(quDocsInvIDREPLY.AsInteger)>0) then
        begin
          quReplyRec.Active:=False;
          quReplyRec.SQL.Clear;
          quReplyRec.SQL.Add('SELECT * FROM dbo.REPLYLIST');
          quReplyRec.SQL.Add('  WHERE ID='+its(Abs(quDocsInvIDREPLY.AsInteger)));
          quReplyRec.SQL.Add('  and FSRARID='''+quDocsInvFSRARID.AsString+'''');
          quReplyRec.Active:=True;

          if quReplyRec.RecordCount>0 then
            ShowXMLView(Abs(quDocsInvIDREPLY.AsInteger),quReplyRecReplyFile.AsString);
        end;
        Exit;
      end;

      ShowSpecViewInv(quDocsInvFSRARID.AsString,quDocsInvIDATE.AsInteger,quDocsInvID.AsInteger,0,quDocsInvIDATE.AsInteger);
    end;
  end;
end;

procedure TfmDocInv.acCloseExecute(Sender: TObject);
begin
  //�������
  Close;
//  fmMain.Close;
end;

procedure TfmDocInv.acCreateCopyWithOutBeerExecute(Sender: TObject);
var IDH:Integer;
begin
  //
 with dmR do
  begin
    if quDocsInv.RecordCount>0 then
    begin
      ClearMessageLogLocal;
      ShowMessageLogLocal('����� ... ���� ������������ ���������.');

      IDH:=quDocsInvID.AsInteger;

      quProc.Active:=False;
      quProc.SQL.Clear;
      quProc.SQL.Add('DECLARE @IDH bigint = '+its(IDH));
      quProc.SQL.Add('EXECUTE [dbo].[prCreateCopyInvWOBeer] @IDH');
      quProc.ExecSQL;
      Delay(100);

      Init;

      ShowMessageLogLocal('������� ��������.');
    end;
  end;
end;

procedure TfmDocInv.acCreateCorrExecute(Sender: TObject);
var IDH:Integer;
begin
  //������������ �������������� ���������
  with dmR do
  begin
    if quDocsInv.RecordCount>0 then
    begin
      ClearMessageLogLocal;
      ShowMessageLogLocal('����� ... ���� ������������ ���������.');

      IDH:=quDocsInvID.AsInteger;

      quProc.Active:=False;
      quProc.SQL.Clear;
      quProc.SQL.Add('DECLARE @IDH bigint = '+its(IDH));
      quProc.SQL.Add('EXECUTE [dbo].[prCreateCorrDocs] @IDH');
      quProc.ExecSQL;
      Delay(100);

      if Assigned(fmDocCorr) then
        if fmDocCorr.Showing then fmDocCorr.Init;

      ShowMessageLogLocal('������� ��������.');
    end;
  end;
end;

procedure TfmDocInv.acCreateVnDocExecute(Sender: TObject);
var IDH:Integer;
begin
  // ������� �������� �� ���������� �����������
  with dmR do
  begin
    if quDocsInv.RecordCount>0 then
    begin
      ClearMessageLogLocal;
      ShowMessageLogLocal('����� ... ���� ������������ ���������.');

      IDH:=quDocsInvID.AsInteger;

      quProc.Active:=False;
      quProc.SQL.Clear;
      quProc.SQL.Add('DECLARE @IDH bigint = '+its(IDH));
      quProc.SQL.Add('EXECUTE [dbo].[prCreateVnDoc12fromInv] @IDH');
      quProc.ExecSQL;
      Delay(100);

      if Assigned(fmDocVn) then
        if fmDocVn.Showing then fmDocVn.Init;

      ShowMessageLogLocal('������� ��������.');
    end;
  end;
end;

procedure TfmDocInv.acExpExcelExecute(Sender: TObject);
begin
  ExportGridToFile(formatdatetime('ddmmyyyy',deDateBeg.Date)+'_'+formatdatetime('ddmmyyyy',deDateEnd.Date)+'_'+formatdatetime('ddmmyyyy_hh_nn_ss',now)+'.xls', GridDocInv);
end;

procedure TfmDocInv.acGetRemReg1Execute(Sender: TObject);
begin
  //
  if MessageDlg('��������� ������� 1 � ����� ? ( ���������� - '+CommonSet.FSRAR_ID+')',mtConfirmation, [mbYes, mbNo], 0, mbYes) <> mrYes then Exit;

  ClearMessageLogLocal;
  ShowMessageLogLocal('������.');
  if prGetRemn1Private(CommonSet.FSRAR_ID,MemoLogLocal,dmR.FDConnection,CommonSet.IPUTM) then ShowMessageLogLocal(' �������� ��') else ShowMessageLogLocal('  ������ ��������');
  ShowMessageLogLocal('������� ��������');
end;

{
procedure TfmDocInv.acGetRemReg1Execute(Sender: TObject);
Const
  CR = #$0d;
  LF = #$0a;
  CRLF = CR + LF;
  Boundary = 'END_OF_PART';

var  AskXml: TNativeXml;
     IdF:Integer;
     RetXml: TNativeXml;
     RetId:string;

     httpsend: THTTPSend;
     s: AnsiString;
     FS: TMemoryStream;
     RetVal:TStringList;
begin
  // ������� �� ������� ��������
  if MessageDlg('��������� ������� 1 � ����� ? ( ���������� - '+CommonSet.FSRAR_ID+')',mtConfirmation, [mbYes, mbNo], 0, mbYes) <> mrYes then Exit;
  if dmR.FDConnection.Connected=False then Exit;

  with dmR do
  begin
    try
      ClearMessageLogLocal;

      IdF:=fGetId(1);
      ShowMessageLogLocal('  ������ '+its(IdF));

      FS := TMemoryStream.Create;
      httpsend := THTTPSend.Create;

      //��������� ���� �������
      AskXml := TNativeXml.Create(nil);
      RetXml := TNativeXml.Create(nil);

      AskXml.XmlFormat := xfReadable;
      AskXml.CreateName('ns:Documents');
      AskXml.Root.WriteAttributeString('Version', '1.0');
      AskXml.Root.WriteAttributeString('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
      AskXml.Root.WriteAttributeString('xmlns:ns', 'http://fsrar.ru/WEGAIS/WB_DOC_SINGLE_01');
      AskXml.Root.WriteAttributeString('xmlns:qp', 'http://fsrar.ru/WEGAIS/QueryParameters');
      AskXml.Root.NodeNew('ns:Owner');
      AskXml.Root.NodeByName('ns:Owner').NodeNew('ns:FSRAR_ID').Value:=CommonSet.FSRAR_ID;

      AskXml.Root.NodeNew('ns:Document');
      AskXml.Root.NodeByName('ns:Document').NodeNew('ns:QueryRests');
      AskXml.Root.NodeByName('ns:Document').NodeByName('ns:QueryRests').Value:='';

      //AskXml.SaveToFile('c:\QueryRests.xml');
      AskXml.SaveToStream(FS);

      //����� � ����
      quToRar.Active:=False;
      quToRar.ParamByName('ID').AsInteger:=IdF;
      quToRar.ParamByName('RARID').AsString:=CommonSet.FSRAR_ID;
      quToRar.Active:=True;

      quToRar.Append;
      quToRarFSRARID.AsString:=CommonSet.FSRAR_ID;
      quToRarID.AsInteger:=IdF;
      quToRarDATEQU.AsDateTime:=Now;
      quToRarITYPEQU.AsInteger:=1;
      quToRarISTATUS.AsInteger:=1;
      //quToRarSENDFILE.LoadFromFile(NameF);
      quToRarSENDFILE.LoadFromStream(FS);
      quToRar.Post;

      httpsend.MimeType := 'multipart/form-data; boundary='+Boundary;  //���������� Contetn-Type �������
      // ���������� Mime-��� � ������ �� �����
      s:='--'+Boundary+CRLF+'Content-Disposition: form-data; name="xml_file"; filename="'+IdToName(IdF)+'"'+CRLF+'Content-Type: application/xml'+CRLF+CRLF;
      httpsend.Document.Write(PAnsiChar(s)^, Length(s));
      FS.Position := 0;
      httpsend.Document.CopyFrom(FS, FS.Size);  //���������� ����� � ���� ���������
      //S:=CRLF+CRLF+'--'+Boundary+CRLF;  //��������� ���� �������
      S:=CRLF+CRLF+'--'+Boundary+'--'+CRLF;  //��������� ���� �������
      httpsend.Document.Write(PAnsiChar(s)^, Length(s)); // ��������� ���� ���������

      httpsend.KeepAlive:=False;
      httpsend.Status100:=True;
      httpsend.Headers.Add('Accept: */*');

      // ���������� ������
      if httpsend.HTTPMethod('POST','http://'+CommonSet.IPUTM+':8080/opt/in/QueryRests') then
      begin
        ShowMessageLogLocal('������ �������� 1 �� ( ResultCode='+IntToStr(httpsend.ResultCode)+' )');
        ShowMessageLogLocal(httpsend.ResultString);
        //MemoLogLocal.Lines.LoadFromStream(httpsend.Document, TEncoding.UTF8);

        if httpsend.ResultCode<>200 then
        begin
          try
            RetVal:=TStringList.Create;
            RetVal.LoadFromStream(httpsend.Document, TEncoding.UTF8);
            prWMemo(MemoLogLocal,RetVal.Text);
          finally
            RetVal.Free;
          end;
        end;

        httpsend.Document.Position:=0;
        RetXml.LoadFromStream(httpsend.Document);
        RetXml.XmlFormat := xfReadable;

        RetId:=RetXml.Root.NodeByName('url').Value;

        if RetId>'' then
        begin
          httpsend.Document.Position:=0;

          quToRar.Edit;
          quToRarISTATUS.AsInteger:=2;
          quToRarRECEIVE_ID.AsString:=RetId;
          quToRarRECEIVE_FILE.LoadFromStream(httpsend.Document);
          quToRar.Post;
        end;

      end else begin
        ShowMessageLogLocal('FALSE');
        ShowMessageLogLocal('ResultCode='+IntToStr(httpsend.ResultCode));
        //ShowMessageLogLocal(httpsend.ResultString);

        quToRar.Edit;
        quToRarISTATUS.AsInteger:=100;
        quToRar.Post;
      end;

    finally
      FS.Free;
      AskXml.Free;
      RetXml.Free;
      quToRar.Active:=False;
    end;

    ShowMessageLogLocal('������� ��������.');
  end;
end;
}

procedure TfmDocInv.acGetRemReg2Execute(Sender: TObject);
Const
  CR = #$0d;
  LF = #$0a;
  CRLF = CR + LF;
  Boundary = 'END_OF_PART';

var  AskXml: TNativeXml;
     IdF:Integer;
     RetXml: TNativeXml;
     RetId:string;

     httpsend: THTTPSend;
     s: AnsiString;
     FS: TMemoryStream;
     RetVal:TStringList;
begin
  // ������� �� ������� ��������
  if MessageDlg('��������� ������� 2 � ����� ? ( ���������� - '+CommonSet.FSRAR_ID+')',mtConfirmation, [mbYes, mbNo], 0, mbYes) <> mrYes then Exit;
  if dmR.FDConnection.Connected=False then Exit;

  with dmR do
  begin
    try
      ClearMessageLogLocal;

      IdF:=fGetId(1);
      ShowMessageLogLocal('  ������ '+its(IdF));

      FS := TMemoryStream.Create;
      httpsend := THTTPSend.Create;

      //��������� ���� �������
      AskXml := TNativeXml.Create(nil);
      RetXml := TNativeXml.Create(nil);

      AskXml.XmlFormat := xfReadable;
      AskXml.CreateName('ns:Documents');
      AskXml.Root.WriteAttributeString('Version', '1.0');
      AskXml.Root.WriteAttributeString('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
      AskXml.Root.WriteAttributeString('xmlns:ns', 'http://fsrar.ru/WEGAIS/WB_DOC_SINGLE_01');
      AskXml.Root.WriteAttributeString('xmlns:qp', 'http://fsrar.ru/WEGAIS/QueryParameters');
      AskXml.Root.NodeNew('ns:Owner');
      AskXml.Root.NodeByName('ns:Owner').NodeNew('ns:FSRAR_ID').Value:=CommonSet.FSRAR_ID;

      AskXml.Root.NodeNew('ns:Document');
      AskXml.Root.NodeByName('ns:Document').NodeNew('ns:QueryRestsShop_v2');
      AskXml.Root.NodeByName('ns:Document').NodeByName('ns:QueryRestsShop_v2').Value:='';

      AskXml.SaveToStream(FS);

      //����� � ����
      quToRar.Active:=False;
      quToRar.ParamByName('ID').AsInteger:=IdF;
      quToRar.ParamByName('RARID').AsString:=CommonSet.FSRAR_ID;
      quToRar.Active:=True;

      quToRar.Append;
      quToRarFSRARID.AsString:=CommonSet.FSRAR_ID;
      quToRarID.AsInteger:=IdF;
      quToRarDATEQU.AsDateTime:=Now;
      quToRarITYPEQU.AsInteger:=1;
      quToRarISTATUS.AsInteger:=1;
      //quToRarSENDFILE.LoadFromFile(NameF);
      quToRarSENDFILE.LoadFromStream(FS);
      quToRar.Post;

      httpsend.MimeType := 'multipart/form-data; boundary='+Boundary;  //���������� Contetn-Type �������
      // ���������� Mime-��� � ������ �� �����
      s:='--'+Boundary+CRLF+'Content-Disposition: form-data; name="xml_file"; filename="'+IdToName(IdF)+'"'+CRLF+'Content-Type: application/xml'+CRLF+CRLF;
      httpsend.Document.Write(PAnsiChar(s)^, Length(s));
      FS.Position := 0;
      httpsend.Document.CopyFrom(FS, FS.Size);  //���������� ����� � ���� ���������
      //S:=CRLF+CRLF+'--'+Boundary+CRLF;  //��������� ���� �������
      S:=CRLF+CRLF+'--'+Boundary+'--'+CRLF;  //��������� ���� �������
      httpsend.Document.Write(PAnsiChar(s)^, Length(s)); // ��������� ���� ���������

      httpsend.KeepAlive:=False;
      httpsend.Status100:=True;
      httpsend.Headers.Add('Accept: */*');

      // ���������� ������
      if httpsend.HTTPMethod('POST','http://'+CommonSet.IPUTM+':8080/opt/in/QueryRestsShop_v2') then
      begin
        ShowMessageLogLocal('������ �������� 2 �� ( ResultCode='+IntToStr(httpsend.ResultCode)+' )');
        ShowMessageLogLocal(httpsend.ResultString);
        //MemoLogLocal.Lines.LoadFromStream(httpsend.Document, TEncoding.UTF8);

        if httpsend.ResultCode<>200 then
        begin
          try
            RetVal:=TStringList.Create;
            RetVal.LoadFromStream(httpsend.Document, TEncoding.UTF8);
            prWMemo(MemoLogLocal,RetVal.Text);
          finally
            RetVal.Free;
          end;
        end;

        httpsend.Document.Position:=0;
        RetXml.LoadFromStream(httpsend.Document);
        RetXml.XmlFormat := xfReadable;
        //RetXml.SaveToFile('C:\1110.xml');

        //RetId:='';
        //nodeRoot := RetXml.Root;
        //if assigned(nodeRoot) then  RetId:=nodeRoot.NodeByName('url').Value;

        RetId:=RetXml.Root.NodeByName('url').Value;

        if RetId>'' then
        begin
          httpsend.Document.Position:=0;

          quToRar.Edit;
          quToRarISTATUS.AsInteger:=2;
          quToRarRECEIVE_ID.AsString:=RetId;
          quToRarRECEIVE_FILE.LoadFromStream(httpsend.Document);
          quToRar.Post;
        end;

      end else begin
        ShowMessageLogLocal('FALSE');
        ShowMessageLogLocal('ResultCode='+IntToStr(httpsend.ResultCode));
        //ShowMessageLogLocal(httpsend.ResultString);

        quToRar.Edit;
        quToRarISTATUS.AsInteger:=100;
        quToRar.Post;
      end;

    finally
      FS.Free;
      AskXml.Free;
      RetXml.Free;
      quToRar.Active:=False;
    end;

    ShowMessageLogLocal('������� ��������.');
  end;
end;

procedure TfmDocInv.acRefreshExecute(Sender: TObject);
begin
  //������
  Init;
end;

procedure TfmDocInv.acSelectDateExecute(Sender: TObject);
begin
  //��������
  Init;
end;

procedure TfmDocInv.acSetFactQuantExecute(Sender: TObject);
var IDH:Integer;
begin
  //��������� ����������� ���-��
  with dmR do
  begin
    if quDocsInv.RecordCount>0 then
    begin
      ClearMessageLogLocal;
      ShowMessageLogLocal('����� ... ���� ������������ ������.');

      IDH:=quDocsInvID.AsInteger;

      quProc.Active:=False;
      quProc.SQL.Clear;
      quProc.SQL.Add('DECLARE @IDH bigint = '+its(IDH));
      quProc.SQL.Add('EXECUTE [dbo].[prCalcInvFact] @IDH');
      quProc.ExecSQL;
      Delay(100);

      ShowMessageLogLocal('������� ��������.');
    end;
  end;
end;

procedure TfmDocInv.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
  fmDocInv:=Nil;
end;

procedure TfmDocInv.FormCreate(Sender: TObject);
begin
  ClearMessageLogLocal;
end;

procedure TfmDocInv.deDateBegChange(Sender: TObject);
begin
  if dmR.FDConnection.Connected then Init;
end;

end.


