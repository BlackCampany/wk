object fmClients: TfmClients
  Left = 0
  Top = 0
  Caption = #1050#1086#1085#1090#1088#1072#1075#1077#1085#1090#1099
  ClientHeight = 576
  ClientWidth = 981
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object GridClients: TcxGrid
    Left = 0
    Top = 127
    Width = 981
    Height = 449
    Align = alClient
    TabOrder = 0
    LookAndFeel.Kind = lfOffice11
    LookAndFeel.NativeStyle = False
    object ViewClients: TcxGridDBTableView
      PopupMenu = PopupMenu1
      OnDblClick = ViewClientsDblClick
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.InfoPanel.Visible = True
      Navigator.Visible = True
      DataController.DataSource = dsquClients
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.IncSearch = True
      OptionsCustomize.ColumnMoving = False
      OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.Indicator = True
      object ViewClientsCLIENTID: TcxGridDBColumn
        Caption = #1050#1086#1076
        DataBinding.FieldName = 'CLIENTID'
        Width = 109
      end
      object ViewClientsCLIENTINN: TcxGridDBColumn
        Caption = #1048#1053#1053
        DataBinding.FieldName = 'CLIENTINN'
      end
      object ViewClientsCLIENTKPP: TcxGridDBColumn
        Caption = #1050#1055#1055
        DataBinding.FieldName = 'CLIENTKPP'
      end
      object ViewClientsFULLNAME: TcxGridDBColumn
        Caption = #1055#1086#1083#1085#1086#1077' '#1085#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'FULLNAME'
        Width = 170
      end
      object ViewClientsNAME: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'NAME'
        Width = 234
      end
      object ViewClientsCOUNTRY: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1089#1090#1088#1072#1085#1099
        DataBinding.FieldName = 'COUNTRY'
      end
      object ViewClientsREGCODE: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1088#1077#1075#1080#1086#1085#1072
        DataBinding.FieldName = 'REGCODE'
      end
      object ViewClientsADDR: TcxGridDBColumn
        Caption = #1040#1076#1088#1077#1089
        DataBinding.FieldName = 'ADDR'
        Width = 286
      end
    end
    object LevelClients: TcxGridLevel
      GridView = ViewClients
    end
  end
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 981
    Height = 127
    BarManager = dxBarManager1
    Style = rs2010
    ColorSchemeAccent = rcsaOrange
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 5
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1050#1086#1085#1090#1088#1072#1075#1077#1085#1090#1099
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar1'
        end>
      Index = 0
    end
  end
  object quClients: TFDQuery
    Connection = dmR.FDConnection
    SQL.Strings = (
      'SELECT [CLIENTID]'
      '      ,[CLIENTINN]'
      '      ,[CLIENTKPP]'
      '      ,[FULLNAME]'
      '      ,[NAME]'
      '      ,[COUNTRY]'
      '      ,[REGCODE]'
      '      ,[ADDR]'
      '  FROM [dbo].[ACLIENTS]')
    Left = 104
    Top = 201
    object quClientsCLIENTID: TStringField
      FieldName = 'CLIENTID'
      Origin = 'CLIENTID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 50
    end
    object quClientsCLIENTINN: TStringField
      FieldName = 'CLIENTINN'
      Origin = 'CLIENTINN'
    end
    object quClientsCLIENTKPP: TStringField
      FieldName = 'CLIENTKPP'
      Origin = 'CLIENTKPP'
    end
    object quClientsFULLNAME: TMemoField
      FieldName = 'FULLNAME'
      Origin = 'FULLNAME'
      BlobType = ftMemo
      Size = 2147483647
    end
    object quClientsNAME: TMemoField
      FieldName = 'NAME'
      Origin = 'NAME'
      BlobType = ftMemo
      Size = 2147483647
    end
    object quClientsCOUNTRY: TStringField
      FieldName = 'COUNTRY'
      Origin = 'COUNTRY'
      Size = 10
    end
    object quClientsREGCODE: TStringField
      FieldName = 'REGCODE'
      Origin = 'REGCODE'
      Size = 10
    end
    object quClientsADDR: TMemoField
      FieldName = 'ADDR'
      Origin = 'ADDR'
      BlobType = ftMemo
      Size = 2147483647
    end
  end
  object dsquClients: TDataSource
    DataSet = quClients
    Left = 104
    Top = 265
  end
  object ActionManager1: TActionManager
    LargeImages = dmR.LargeImage
    Images = dmR.SmallImage
    Left = 200
    Top = 200
    StyleName = 'Platform Default'
    object acClose: TAction
      Caption = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 100
      OnExecute = acCloseExecute
    end
    object acRefresh: TAction
      Caption = 'acRefresh'
      ShortCut = 116
      OnExecute = acRefreshExecute
    end
    object acLic: TAction
      Caption = #1051#1080#1094#1077#1085#1079#1080#1103
      ImageIndex = 66
      OnExecute = acLicExecute
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      #1044#1077#1081#1089#1090#1074#1080#1103)
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmR.SmallImage
    ImageOptions.LargeImages = dmR.LargeImage
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 292
    Top = 196
    DockControlHeights = (
      0
      0
      0
      0)
    object dxBarManager1Bar1: TdxBar
      Caption = #1047#1072#1082#1088#1099#1090#1100
      CaptionButtons = <>
      DockedLeft = 80
      DockedTop = 0
      FloatLeft = 925
      FloatTop = 8
      FloatClientWidth = 92
      FloatClientHeight = 54
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1044#1077#1081#1089#1090#1074#1080#1103
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 1015
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = acClose
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = acLic
      Category = 0
    end
    object dxBarGroup1: TdxBarGroup
      Items = ()
    end
  end
  object PopupMenu1: TPopupMenu
    Images = dmR.SmallImage
    Left = 432
    Top = 272
  end
end
