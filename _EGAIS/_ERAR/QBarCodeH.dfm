object fmQBarCodeH: TfmQBarCodeH
  Left = 0
  Top = 0
  Caption = #1047#1072#1087#1088#1086#1089' '#1085#1077#1095#1080#1090#1072#1077#1084#1099#1093' '#1096#1090#1088#1080#1093#1082#1086#1076#1086#1074
  ClientHeight = 570
  ClientWidth = 1030
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object grQBarCodeH: TcxGrid
    Left = 0
    Top = 127
    Width = 1030
    Height = 443
    Align = alClient
    TabOrder = 0
    LookAndFeel.Kind = lfOffice11
    LookAndFeel.NativeStyle = False
    ExplicitLeft = 8
    object vwQBarCodeH: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.InfoPanel.Visible = True
      Navigator.Visible = True
      DataController.DataSource = dsQBarCodeH
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      object vwQBarCodeHID: TcxGridDBColumn
        Caption = #1050#1086#1076
        DataBinding.FieldName = 'ID'
        Width = 47
      end
      object vwQBarCodeHFSRARID: TcxGridDBColumn
        Caption = 'FSRAR ID'
        DataBinding.FieldName = 'FSRARID'
        Width = 110
      end
      object vwQBarCodeHIDATE: TcxGridDBColumn
        Caption = #1044#1072#1090#1072' '#1079#1072#1087#1088#1086#1089#1072
        DataBinding.FieldName = 'IDATE'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.DisplayFormat = 'dd.mm.yyyy'
        Width = 88
      end
      object vwQBarCodeHINUMBER: TcxGridDBColumn
        Caption = #1053#1086#1084#1077#1088' '#1079#1072#1087#1088#1086#1089#1072
        DataBinding.FieldName = 'INUMBER'
        Width = 92
      end
      object vwQBarCodeHISHOP: TcxGridDBColumn
        Caption = #1053#1086#1084#1077#1088' '#1084#1072#1075#1072#1079#1080#1085#1072
        DataBinding.FieldName = 'ISHOP'
        Width = 99
      end
      object vwQBarCodeHNAMESHOP: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077' '#1084#1072#1075#1072#1079#1080#1085#1072
        DataBinding.FieldName = 'NAMESHOP'
        Width = 170
      end
      object vwQBarCodeHISTATUS: TcxGridDBColumn
        Caption = #1057#1090#1072#1090#1091#1089
        DataBinding.FieldName = 'ISTATUS'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.Images = dmR.SmallImage
        Properties.Items = <
          item
            Description = #1057#1092#1086#1088#1084#1080#1088#1086#1074#1072#1085
            ImageIndex = 80
            Value = 0
          end
          item
            Description = #1054#1090#1087#1088#1072#1074#1083#1077#1085
            ImageIndex = 79
            Value = 1
          end
          item
            Description = #1054#1090#1082#1072#1079#1072#1085
            ImageIndex = 82
            Value = 2
          end
          item
            Description = #1055#1086#1083#1091#1095#1077#1085
            ImageIndex = 81
            Value = 3
          end>
        Width = 98
      end
    end
    object lvQBarCodeH: TcxGridLevel
      GridView = vwQBarCodeH
    end
  end
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1030
    Height = 127
    BarManager = dxBarManager1
    Style = rs2010
    ColorSchemeAccent = rcsaOrange
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 5
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1047#1072#1087#1088#1086#1089' '#1085#1077#1095#1080#1090#1072#1077#1084#1099#1093' '#1096#1090#1088#1080#1093#1082#1086#1076#1086#1074
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end
        item
          ToolbarName = 'dxBarManager1Bar4'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end>
      Index = 0
    end
  end
  object quQBarCodeH: TFDQuery
    Active = True
    Connection = dmR.FDConnection
    SQL.Strings = (
      'SELECT'
      '  h.ID'
      ' ,h.FSRARID'
      ' ,h.IDATE'
      ' ,h.INUMBER'
      ' ,h.ISTATUS'
      ' ,h.IDTORAR'
      ' ,h.REJCOMMENT'
      ' ,(SELECT ISHOP FROM dbo.EGAISID WHERE RARID=h.FSRARID) AS ISHOP'
      
        ' ,(SELECT NAMESHOP FROM dbo.EGAISID WHERE RARID=h.FSRARID) AS NA' +
        'MESHOP'
      'FROM dbo.QBARCODE_HD h'
      'WHERE h.[IDATE] BETWEEN :IDATEB AND :IDATEE'
      'ORDER BY h.IDATE, ISHOP')
    Left = 168
    Top = 304
    ParamData = <
      item
        Name = 'IDATEB'
        DataType = ftInteger
        ParamType = ptInput
        Value = 40000
      end
      item
        Name = 'IDATEE'
        DataType = ftInteger
        ParamType = ptInput
        Value = 50000
      end>
    object quQBarCodeHID: TFDAutoIncField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object quQBarCodeHFSRARID: TStringField
      FieldName = 'FSRARID'
      Origin = 'FSRARID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 50
    end
    object quQBarCodeHIDATE: TIntegerField
      FieldName = 'IDATE'
      Origin = 'IDATE'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quQBarCodeHINUMBER: TIntegerField
      FieldName = 'INUMBER'
      Origin = 'INUMBER'
      Required = True
    end
    object quQBarCodeHISTATUS: TIntegerField
      FieldName = 'ISTATUS'
      Origin = 'ISTATUS'
      Required = True
    end
    object quQBarCodeHISHOP: TIntegerField
      FieldName = 'ISHOP'
      Origin = 'ISHOP'
      ReadOnly = True
    end
    object quQBarCodeHNAMESHOP: TStringField
      FieldName = 'NAMESHOP'
      Origin = 'NAMESHOP'
      ReadOnly = True
      Size = 100
    end
    object quQBarCodeHIDTORAR: TLargeintField
      FieldName = 'IDTORAR'
      Origin = 'IDTORAR'
    end
    object quQBarCodeHREJCOMMENT: TMemoField
      FieldName = 'REJCOMMENT'
      Origin = 'REJCOMMENT'
      BlobType = ftMemo
      Size = 2147483647
    end
  end
  object dsQBarCodeH: TDataSource
    DataSet = quQBarCodeH
    Left = 240
    Top = 304
  end
  object ActionManager1: TActionManager
    LargeImages = dmR.LargeImage
    Images = dmR.SmallImage
    Left = 168
    Top = 212
    StyleName = 'Platform Default'
    object acRefresh: TAction
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100' (F5)'
      ImageIndex = 5
      ShortCut = 116
      OnExecute = acRefreshExecute
    end
    object acAdd: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ImageIndex = 75
      OnExecute = acAddExecute
    end
    object acEdit: TAction
      Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100
      ImageIndex = 73
      OnExecute = acEditExecute
    end
    object acView: TAction
      Caption = #1055#1088#1086#1089#1084#1086#1090#1088
      ImageIndex = 39
      OnExecute = acViewExecute
    end
    object acDelete: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Enabled = False
      ImageIndex = 74
      OnExecute = acDeleteExecute
    end
    object acSendQueryBarcode: TAction
      Caption = #1054#1090#1087#1088#1072#1074#1080#1090#1100' '#1079#1072#1087#1088#1086#1089' '#1074' '#1045#1043#1040#1048#1057
      ImageIndex = 34
      OnExecute = acSendQueryBarcodeExecute
    end
    object acPrint: TAction
      Caption = #1055#1077#1095#1072#1090#1100
      ImageIndex = 10
      OnExecute = acPrintExecute
    end
    object acClose: TAction
      Caption = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 100
      OnExecute = acCloseExecute
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      #1044#1077#1081#1089#1090#1074#1080#1103)
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmR.SmallImage
    ImageOptions.LargeImages = dmR.LargeImage
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 260
    Top = 212
    DockControlHeights = (
      0
      0
      0
      0)
    object dxBarManager1Bar1: TdxBar
      Caption = #1042#1099#1073#1086#1088' '#1087#1077#1088#1080#1086#1076#1072
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 925
      FloatTop = 8
      FloatClientWidth = 92
      FloatClientHeight = 54
      ItemLinks = <
        item
          Visible = True
          ItemName = 'deDateBeg'
        end
        item
          Visible = True
          ItemName = 'deDateEnd'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1047#1072#1082#1088#1099#1090#1100
      CaptionButtons = <>
      DockedLeft = 700
      DockedTop = 0
      FloatLeft = 1064
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1059#1087#1088#1072#1074#1083#1077#1085#1080#1077
      CaptionButtons = <>
      DockedLeft = 223
      DockedTop = 0
      FloatLeft = 1064
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton4'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1044#1077#1081#1089#1090#1074#1080#1103
      CaptionButtons = <>
      DockedLeft = 531
      DockedTop = 0
      FloatLeft = 1064
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = acClose
      Category = 0
    end
    object deDateBeg: TdxBarDateCombo
      Caption = 'C  '
      Category = 0
      Hint = 'C  '
      Visible = ivAlways
      OnChange = deDateBegChange
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
        DDDDDDDD00000000000DDDDD0FFFFFFFFF0D00000F0000000F0D0FFF0FFFFFFF
        FF0D0F000FFF11FFFF0D0FFF0FFF11FFFF0D0FF10FFFF11FFF0D0FF10FFFFF11
        FF0D0FF10FF11111FF0D0FF10FFFFFFFFF0D0FF104444444440D0FFF04444444
        440D044400000000000D04444444440DDDDD00000000000DDDDD}
      ShowDayText = False
    end
    object deDateEnd: TdxBarDateCombo
      Caption = #1087#1086
      Category = 0
      Hint = #1087#1086
      Visible = ivAlways
      OnChange = deDateBegChange
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
        DDDDDDDD00000000000DDDDD0FFFFFFFFF0D00000F0000000F0D0FFF0FFFFFFF
        FF0D0F000FFF11FFFF0D0FFF0FFF11FFFF0D0FF10FFFF11FFF0D0FF10FFFFF11
        FF0D0FF10FF11111FF0D0FF10FFFFFFFFF0D0FF104444444440D0FFF04444444
        440D044400000000000D04444444440DDDDD00000000000DDDDD}
      ShowDayText = False
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = acRefresh
      Category = 0
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = acSendQueryBarcode
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = acAdd
      Category = 0
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = acEdit
      Category = 0
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = acView
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = acDelete
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = acPrint
      Category = 0
    end
    object dxBarGroup1: TdxBarGroup
      Items = ()
    end
  end
  object quDocSpecPrint: TFDQuery
    Active = True
    Connection = dmR.FDConnection
    Transaction = dmR.FDTrans
    UpdateTransaction = dmR.FDTransUpdate
    SQL.Strings = (
      'SELECT'
      '  IDH'
      ' ,ID'
      ' ,IDENT'
      ' ,TYPE'
      ' ,RANK'
      ' ,NUMBER'
      ' ,BARCODE'
      'FROM dbo.QBARCODE_SP'
      'WHERE '
      '  IDH=:IDH')
    Left = 504
    Top = 304
    ParamData = <
      item
        Name = 'IDH'
        DataType = ftInteger
        ParamType = ptInput
        Value = 18
      end>
    object quDocSpecPrintIDH: TIntegerField
      FieldName = 'IDH'
      Origin = 'IDH'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quDocSpecPrintID: TFDAutoIncField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object quDocSpecPrintIDENT: TIntegerField
      FieldName = 'IDENT'
      Origin = 'IDENT'
    end
    object quDocSpecPrintTYPE: TStringField
      FieldName = 'TYPE'
      Origin = 'TYPE'
      Size = 10
    end
    object quDocSpecPrintRANK: TStringField
      FieldName = 'RANK'
      Origin = 'RANK'
      Size = 10
    end
    object quDocSpecPrintNUMBER: TStringField
      FieldName = 'NUMBER'
      Origin = 'NUMBER'
    end
    object quDocSpecPrintBARCODE: TStringField
      FieldName = 'BARCODE'
      Origin = 'BARCODE'
      Size = 250
    end
  end
  object frxReport: TfrxReport
    Version = '5.3.16'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = #1055#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 42688.487721828700000000
    ReportOptions.LastChange = 42688.559046226850000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 504
    Top = 360
    Datasets = <
      item
        DataSet = frxdsDocHeadPrint
        DataSetName = 'frxdsDocHeadPrint'
      end
      item
        DataSet = frxdsDocSpecPrint
        DataSetName = 'frxdsDocSpecPrint'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 64.252010000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo88: TfrxMemoView
          Top = 7.559059999999999000
          Width = 718.472790000000000000
          Height = 22.000000000000000000
          StretchMode = smActualHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            
              #1047#1072#1087#1088#1086#1089' '#1085#1077#1095#1080#1090#1072#1077#1084#1099#1093' '#1096#1090#1088#1080#1093' '#1082#1086#1076#1086#1074' '#8470' [frxdsDocHeadPrint."ID"] '#1086#1090' [Dat' +
              'eToStr(<frxdsDocHeadPrint."IDATE">)]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo12: TfrxMemoView
          Top = 41.574830000000000000
          Width = 718.472790000000000000
          Height = 18.220470000000000000
          StretchMode = smActualHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            
              #1052#1072#1075#1072#1079#1080#1085' '#8470'[frxdsDocHeadPrint."ISHOP"] [frxdsDocHeadPrint."NAMESHO' +
              'P"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 219.653680000000000000
        Top = 143.622140000000000000
        Width = 718.110700000000000000
        DataSet = frxdsDocSpecPrint
        DataSetName = 'frxdsDocSpecPrint'
        RowCount = 0
        object Shape1: TfrxShapeView
          Top = 0.102350000000001300
          Width = 721.890230000000000000
          Height = 219.212740000000000000
        end
        object Memo1: TfrxMemoView
          Left = 75.590600000000000000
          Top = 37.897650000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          DataSet = frxdsDocSpecPrint
          DataSetName = 'frxdsDocSpecPrint'
          Memo.UTF8W = (
            '[frxdsDocSpecPrint."TYPE"]')
        end
        object Memo2: TfrxMemoView
          Left = 3.779530000000000000
          Top = 37.897650000000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          Memo.UTF8W = (
            #1058#1080#1087)
        end
        object Memo3: TfrxMemoView
          Left = 75.590600000000000000
          Top = 56.795300000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          DataSet = frxdsDocSpecPrint
          DataSetName = 'frxdsDocSpecPrint'
          Memo.UTF8W = (
            '[frxdsDocSpecPrint."RANK"]')
        end
        object Memo4: TfrxMemoView
          Left = 3.779530000000000000
          Top = 56.795300000000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          Memo.UTF8W = (
            #1057#1077#1088#1080#1103)
        end
        object Memo5: TfrxMemoView
          Left = 75.590600000000000000
          Top = 75.692950000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Memo.UTF8W = (
            '[frxdsDocSpecPrint."NUMBER"]')
        end
        object Memo6: TfrxMemoView
          Left = 3.779530000000000000
          Top = 75.692950000000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          Memo.UTF8W = (
            #1053#1086#1084#1077#1088)
        end
        object Barcode2D1: TfrxBarcode2DView
          Left = 562.031850000000000000
          Top = 62.015770000000000000
          Width = 63.000000000000000000
          Height = 63.000000000000000000
          BarType = bcCodeQR
          BarProperties.Encoding = qrISO88591
          BarProperties.QuietZone = 0
          BarProperties.ErrorLevels = ecL
          BarProperties.PixelSize = 3
          DataField = 'BARCODE'
          DataSet = frxdsDocSpecPrint
          DataSetName = 'frxdsDocSpecPrint'
          Rotation = 0
          ShowText = False
          Text = '12345678'
          Zoom = 1.000000000000000000
          FontScaled = True
          QuietZone = 0
        end
        object Memo7: TfrxMemoView
          Left = 75.590600000000000000
          Top = 94.590600000000000000
          Width = 415.748300000000000000
          Height = 37.795300000000000000
          DataSet = frxdsDocSpecPrint
          DataSetName = 'frxdsDocSpecPrint'
          Memo.UTF8W = (
            '[frxdsDocSpecPrint."BARCODE"]')
        end
        object Memo8: TfrxMemoView
          Left = 3.779530000000000000
          Top = 94.590600000000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          Memo.UTF8W = (
            #1064#1090#1088#1080#1093#1082#1086#1076)
        end
        object Memo10: TfrxMemoView
          Left = 75.590600000000000000
          Top = 18.559059999999990000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          DataField = 'IDENT'
          DataSet = frxdsDocSpecPrint
          DataSetName = 'frxdsDocSpecPrint'
          Memo.UTF8W = (
            '[frxdsDocSpecPrint."IDENT"]')
        end
        object Memo11: TfrxMemoView
          Left = 3.779530000000000000
          Top = 18.559059999999990000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          Memo.UTF8W = (
            #8470)
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 423.307360000000000000
        Width = 718.110700000000000000
        object Memo9: TfrxMemoView
          Left = 438.425480000000000000
          Width = 279.685220000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            #1057#1090#1088#1072#1085#1080#1094#1072' [Page#] '#1080#1079' [TotalPages#]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
      end
    end
  end
  object frxdsDocSpecPrint: TfrxDBDataset
    UserName = 'frxdsDocSpecPrint'
    CloseDataSource = False
    DataSet = quDocSpecPrint
    BCDToCurrency = False
    Left = 592
    Top = 304
  end
  object frxBarCodeObject1: TfrxBarCodeObject
    Left = 592
    Top = 360
  end
  object frxdsDocHeadPrint: TfrxDBDataset
    UserName = 'frxdsDocHeadPrint'
    CloseDataSource = False
    DataSet = quQBarCodeH
    BCDToCurrency = False
    Left = 688
    Top = 304
  end
end
