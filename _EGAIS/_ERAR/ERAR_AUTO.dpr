program ERAR_AUTO;

uses
  Vcl.SvcMgr,
  ServiceAuto in 'ServiceAuto.pas' {ERAR_Auto_Service: TService},
  dmRarA in 'dmRarA.pas' {dmRA: TDataModule},
  Shared_Exception in '..\..\SharedUnits\Shared_Exception.pas',
  Shared_Logs in '..\..\SharedUnits\Shared_Logs.pas',
  Shared_Functions in '..\..\SharedUnits\Shared_Functions.pas',
  Shared_Ping in '..\..\SharedUnits\Shared_Ping.pas',
  EgaisDecode in 'EgaisDecode.pas';

{$R *.RES}

begin
  // Windows 2003 Server requires StartServiceCtrlDispatcher to be
  // called before CoRegisterClassObject, which can be called indirectly
  // by Application.Initialize. TServiceApplication.DelayInitialize allows
  // Application.Initialize to be called from TService.Main (after
  // StartServiceCtrlDispatcher has been called).
  //
  // Delayed initialization of the Application object may affect
  // events which then occur prior to initialization, such as
  // TService.OnCreate. It is only recommended if the ServiceApplication
  // registers a class object with OLE and is intended for use with
  // Windows 2003 Server.
  //
  // Application.DelayInitialize := True;
  //
  if not Application.DelayInitialize or Application.Installing then
    Application.Initialize;
  Application.CreateForm(TERAR_Auto_Service, ERAR_Auto_Service);
  Application.CreateForm(TdmRA, dmRA);
  {
  //<--
  //��������� ����������, ����� ����� ���� ������ ���� � �� ELog
  _Log.Connection:=dmRA.FDConnection;
  _Log.Exemplar:=0;
  _Log.CacheLogEnabled:=False;   //����� ������ ���� � ���
  _Log.CacheLogMaxCount:=10000; //����.����� ����� � ����, ������ �����, ����� ���� ���� ����
  _Log.TimesInterval:=5000;     //�������� ������ ���� ���� � �� �� �������
  _Log.TimesEnabled:=False;     //������ ���� �� ������� ���������
  _ExceptionHandler.ShowMessageError:=False;  //���������� ��������� �� �������, � ������ ���������� ������
  //-->
  }
  Application.Run;
end.
