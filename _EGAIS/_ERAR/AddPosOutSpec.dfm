object fmAddPosOut: TfmAddPosOut
  Left = 0
  Top = 0
  Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1089#1090#1088#1086#1082#1091
  ClientHeight = 312
  ClientWidth = 516
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 19
    Width = 21
    Height = 13
    Caption = #1050#1040#1055
  end
  object Label2: TLabel
    Left = 8
    Top = 89
    Width = 74
    Height = 13
    Caption = #1050#1086#1076' '#1089#1090#1088#1072#1074#1082#1080' '#1040
  end
  object Label3: TLabel
    Left = 8
    Top = 131
    Width = 73
    Height = 13
    Caption = #1050#1086#1076' '#1089#1087#1088#1072#1074#1082#1080' '#1041
  end
  object Label4: TLabel
    Left = 8
    Top = 169
    Width = 60
    Height = 13
    Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086
  end
  object Label5: TLabel
    Left = 16
    Top = 42
    Width = 481
    Height = 31
    AutoSize = False
    Caption = #1053#1072#1079#1074#1072#1085#1080#1077' '#1090#1086#1074#1072#1088#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    WordWrap = True
  end
  object Label6: TLabel
    Left = 8
    Top = 204
    Width = 26
    Height = 13
    Caption = #1062#1077#1085#1072
  end
  object Panel1: TPanel
    Left = 0
    Top = 236
    Width = 516
    Height = 76
    Align = alBottom
    BevelInner = bvLowered
    BevelKind = bkFlat
    TabOrder = 0
    ExplicitTop = 211
    object Button1: TButton
      Left = 88
      Top = 16
      Width = 113
      Height = 41
      Caption = #1054#1082
      ModalResult = 1
      TabOrder = 0
    end
    object Button2: TButton
      Left = 296
      Top = 16
      Width = 113
      Height = 41
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
    end
  end
  object cxTextEdit1: TcxTextEdit
    Left = 104
    Top = 15
    TabOrder = 1
    Text = 'cxTextEdit1'
    OnExit = cxTextEdit1Exit
    Width = 145
  end
  object cxTextEdit2: TcxTextEdit
    Left = 104
    Top = 87
    TabOrder = 2
    Text = 'cxTextEdit2'
    Width = 145
  end
  object cxTextEdit3: TcxTextEdit
    Left = 104
    Top = 127
    TabOrder = 3
    Text = 'cxTextEdit3'
    Width = 145
  end
  object cxCalcEdit1: TcxCalcEdit
    Left = 104
    Top = 165
    EditValue = 0.000000000000000000
    TabOrder = 4
    Width = 145
  end
  object cxCurrencyEdit1: TcxCurrencyEdit
    Left = 104
    Top = 200
    EditValue = 0.000000000000000000
    TabOrder = 5
    Width = 145
  end
  object quSelCard: TFDQuery
    Connection = dmR.FDConnection
    SQL.Strings = (
      'SELECT [KAP]'
      '      ,cast([NAME] as varchar(200)) as NAME'
      '      ,[VOL]'
      '      ,[KREP]'
      '      ,[AVID]'
      '      ,[PRODID]'
      '      ,[IMPORTERID]'
      '  FROM [dbo].[ACARDS]'
      '  where [KAP]=:CODE')
    Left = 328
    Top = 115
    ParamData = <
      item
        Name = 'CODE'
        DataType = ftString
        ParamType = ptInput
        Size = 20
        Value = '0003477000001836431'
      end>
    object quSelCardKAP: TStringField
      FieldName = 'KAP'
      Origin = 'KAP'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 50
    end
    object quSelCardNAME: TStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      ReadOnly = True
      Size = 200
    end
    object quSelCardVOL: TSingleField
      FieldName = 'VOL'
      Origin = 'VOL'
    end
    object quSelCardKREP: TSingleField
      FieldName = 'KREP'
      Origin = 'KREP'
    end
    object quSelCardAVID: TIntegerField
      FieldName = 'AVID'
      Origin = 'AVID'
    end
    object quSelCardPRODID: TStringField
      FieldName = 'PRODID'
      Origin = 'PRODID'
      Size = 50
    end
    object quSelCardIMPORTERID: TStringField
      FieldName = 'IMPORTERID'
      Origin = 'IMPORTERID'
      Size = 50
    end
  end
end
