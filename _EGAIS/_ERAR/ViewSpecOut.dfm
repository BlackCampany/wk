object fmDocSpecOut: TfmDocSpecOut
  Left = 0
  Top = 0
  Caption = #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1103' '#1085#1072' '#1074#1086#1079#1074#1088#1072#1090
  ClientHeight = 555
  ClientWidth = 1127
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1127
    Height = 127
    BarManager = dxBarManager1
    Style = rs2010
    ColorSchemeAccent = rcsaOrange
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1103' '#1085#1072' '#1074#1086#1079#1074#1088#1072#1090
      Groups = <
        item
          Caption = 
            '         '#1059#1087#1088#1072#1074#1083#1077#1085#1080#1077'                                             ' +
            '         '
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end>
      Index = 0
    end
  end
  object GridSpec: TcxGrid
    Left = 0
    Top = 127
    Width = 1127
    Height = 428
    Align = alClient
    TabOrder = 1
    LookAndFeel.Kind = lfOffice11
    LookAndFeel.NativeStyle = False
    object ViewSpecOut: TcxGridDBTableView
      PopupMenu = PopupMenu1
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.InfoPanel.Visible = True
      Navigator.Visible = True
      DataController.DataSource = dsquSpecOut
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'QUANT'
          Column = ViewSpecOutQUANT
        end
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'QUANTF'
          Column = ViewSpecOutQUANTF
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SUMF'
          Column = ViewSpecOutSUMF
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Inserting = False
      OptionsSelection.MultiSelect = True
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      object ViewSpecOutNUM: TcxGridDBColumn
        Caption = #8470' '#1087#1087
        DataBinding.FieldName = 'NUM'
        Options.Editing = False
        Width = 47
      end
      object ViewSpecOutALCCODE: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1072#1083#1082#1086#1075#1086#1083#1100#1085#1086#1081' '#1087#1088#1086#1076#1091#1082#1094#1080#1080
        DataBinding.FieldName = 'ALCCODE'
        Options.Editing = False
        Width = 143
      end
      object ViewSpecOutNAME: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'NAME'
        Options.Editing = False
        Width = 266
      end
      object ViewSpecOutVOL: TcxGridDBColumn
        Caption = #1054#1073#1098#1077#1084' '#1083'.'
        DataBinding.FieldName = 'VOL'
        Options.Editing = False
      end
      object ViewSpecOutKREP: TcxGridDBColumn
        Caption = #1050#1088#1077#1087#1086#1089#1090#1100
        DataBinding.FieldName = 'KREP'
        Options.Editing = False
      end
      object ViewSpecOutPRICE: TcxGridDBColumn
        Caption = #1062#1077#1085#1072' '#1087#1086' '#1076#1086#1082#1091#1084#1077#1085#1090#1091
        DataBinding.FieldName = 'PRICE'
        Width = 77
      end
      object ViewSpecOutQUANT: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086' '#1087#1086' '#1076#1086#1082#1091#1084#1077#1085#1090#1091
        DataBinding.FieldName = 'QUANT'
        Visible = False
        Options.Editing = False
        Styles.Content = dmR.stlBold
      end
      object ViewSpecOutQUANTREMN: TcxGridDBColumn
        Caption = #1044#1086#1089#1090#1091#1087#1085#1086' '#1082' '#1074#1086#1079#1074#1088#1072#1090#1091
        DataBinding.FieldName = 'QUANTREMN'
        Visible = False
        Options.Editing = False
        Styles.Content = dmR.stlBold
      end
      object ViewSpecOutQUANTF: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086' '#1087#1086' '#1092#1072#1082#1090#1091
        DataBinding.FieldName = 'QUANTF'
        Styles.Content = dmR.stlBold
      end
      object ViewSpecOutSUMF: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1087#1086' '#1092#1072#1082#1090#1091
        DataBinding.FieldName = 'SUMF'
        Visible = False
        Options.Editing = False
        Width = 73
      end
      object ViewSpecOutCODECB: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1062#1041
        DataBinding.FieldName = 'CODECB'
        Visible = False
        Options.Editing = False
      end
      object ViewSpecOutWBREGID: TcxGridDBColumn
        DataBinding.FieldName = 'WBREGID'
        Width = 135
      end
      object ViewSpecOutINFO_A: TcxGridDBColumn
        Caption = #1057#1087#1088#1072#1074#1082#1072' '#1040
        DataBinding.FieldName = 'INFO_A'
        Width = 160
      end
      object ViewSpecOutINFO_B: TcxGridDBColumn
        Caption = #1057#1087#1088#1072#1074#1082#1072' '#1041
        DataBinding.FieldName = 'INFO_B'
        Width = 168
      end
    end
    object LevelSpec: TcxGridLevel
      GridView = ViewSpecOut
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      #1044#1077#1081#1089#1090#1074#1080#1103)
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmR.SmallImage
    ImageOptions.LargeImages = dmR.LargeImage
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 540
    Top = 164
    DockControlHeights = (
      0
      0
      0
      0)
    object dxBarManager1Bar1: TdxBar
      Caption = #1059#1087#1088#1072#1074#1083#1077#1085#1080#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 1001
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          BeginGroup = True
          UserDefine = [udWidth]
          UserWidth = 128
          Visible = True
          ItemName = 'cxBarEditItem1'
        end
        item
          UserDefine = [udWidth]
          UserWidth = 141
          Visible = True
          ItemName = 'cxBarEditItem2'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton4'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1047#1072#1082#1088#1099#1090#1100
      CaptionButtons = <>
      DockedLeft = 560
      DockedTop = 0
      FloatLeft = 1161
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = acSave
      Category = 0
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Caption = 
        '                                                                ' +
        '                '
      Category = 0
      Hint = 
        '                                                                ' +
        '                '
      Visible = ivAlways
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = acClose
      Category = 0
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = #1053#1086#1084#1077#1088' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
      Category = 0
      Hint = #1053#1086#1084#1077#1088' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
      Visible = ivAlways
      PropertiesClassName = 'TcxTextEditProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = 'New SubItem'
      Category = 0
      Visible = ivAlways
      ItemLinks = <>
    end
    object cxBarEditItem2: TcxBarEditItem
      Caption = #1044#1072#1090#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
      Category = 0
      Hint = #1044#1072#1090#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
      Visible = ivAlways
      PropertiesClassName = 'TcxDateEditProperties'
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = acTestSpec
      Category = 0
    end
    object dxBarGroup1: TdxBarGroup
      Items = ()
    end
  end
  object ActionList1: TActionList
    Images = dmR.SmallImage
    Left = 648
    Top = 168
    object acClose: TAction
      Caption = #1047#1072#1082#1088#1099#1090#1100
      ImageIndex = 100
      OnExecute = acCloseExecute
    end
    object acSave: TAction
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' (Ctrl + S)'
      ImageIndex = 7
      ShortCut = 16467
      OnExecute = acSaveExecute
    end
    object acTestSpec: TAction
      Caption = #1055#1088#1086#1074#1077#1088#1082#1072' '#1089#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1080
      ImageIndex = 38
      OnExecute = acTestSpecExecute
    end
    object acDelPos: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1087#1086#1079#1080#1094#1080#1102
      ImageIndex = 4
      OnExecute = acDelPosExecute
    end
    object acAddPosSpecOut: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1087#1086#1079#1080#1094#1080#1102
      OnExecute = acAddPosSpecOutExecute
    end
  end
  object quSpecOut: TFDQuery
    OnCalcFields = quSpecOutCalcFields
    CachedUpdates = True
    Connection = dmR.FDConnection
    Transaction = dmR.FDTrans
    UpdateTransaction = dmR.FDTransUpdate
    UpdateObject = UpdSQL1
    SQL.Strings = (
      'declare @FSRARID varchar(50) = :RARID'
      'declare @IDATE int = :IDATE'
      'declare @SID varchar(50) = :SID'
      'declare @IDHEADIN int = :IDHEAD '
      ''
      ''
      
        'SELECT sp.[FSRARID],sp.[IDATE],sp.[SIDHD],sp.[ID],sp.[NUM],sp.[A' +
        'LCCODE],sp.[INFO_A],sp.[INFO_B]'
      '       ,sp.[QUANT]'
      '  '#9'   ,(sp.[QUANT]-isnull(spremn.QUANTREMN,0)) as QUANTREMN'
      #9'   ,sp.[QUANTF]'
      #9'   ,sp.[PRICE],sp.[PRICEF],sp.[PRISEF0]'
      '       ,ca.NAME, ca.VOL, ca.KREP, prod.NAME as PRODNAME'
      #9'   ,sp.[WBREGID]'
      '       ,0 as [CODECB]'
      '  FROM [dbo].[ADOCSOUT_SP] sp'
      '  left join [dbo].[ACARDS] ca on ca.KAP=sp.[ALCCODE]'
      '  left join [dbo].[APRODS] prod on prod.PRODID=ca.PRODID'
      '  left join ( SELECT spo.[ALCCODE]'
      #9#9#9#9'     ,spo.[WBREGID]'
      #9#9#9#9#9' ,isnull(SUM(spo.[QUANTF]),0) as QUANTREMN'
      #9#9#9#9'FROM [dbo].[ADOCSOUT_SP] spo'
      
        #9#9#9#9'left join [dbo].[ADOCSOUT_HD] hdo on hdo.[FSRARID]=spo.[FSRA' +
        'RID] and hdo.[IDATE]=spo.[IDATE] and hdo.[SID]=spo.[SIDHD]'
      
        #9#9#9#9'where hdo.[IDHEAD]=@IDHEADIN and (hdo.IACTIVE=1 or hdo.READY' +
        'SEND=1)'
      
        #9#9#9#9'group by spo.[ALCCODE],spo.[WBREGID]) spremn on spremn.ALCCO' +
        'DE=sp.[ALCCODE] and spremn.WBREGID=sp.[WBREGID] '
      '  where sp.[FSRARID]=@FSRARID'
      '  and sp.[IDATE]=@IDATE'
      '  and sp.[SIDHD]=@SID')
    Left = 88
    Top = 360
    ParamData = <
      item
        Name = 'RARID'
        DataType = ftString
        ParamType = ptInput
        Value = '020000021354'
      end
      item
        Name = 'IDATE'
        DataType = ftInteger
        ParamType = ptInput
        Value = 42416
      end
      item
        Name = 'SID'
        DataType = ftString
        ParamType = ptInput
        Value = '04F3A6D5-970B-4C30-A927-AA50AA999EF2'
      end
      item
        Name = 'IDHEAD'
        DataType = ftInteger
        ParamType = ptInput
        Value = 168872
      end>
    object quSpecOutFSRARID: TStringField
      FieldName = 'FSRARID'
      Origin = 'FSRARID'
      Required = True
      Size = 50
    end
    object quSpecOutIDATE: TIntegerField
      FieldName = 'IDATE'
      Origin = 'IDATE'
      Required = True
    end
    object quSpecOutSIDHD: TStringField
      FieldName = 'SIDHD'
      Origin = 'SIDHD'
      Required = True
      Size = 50
    end
    object quSpecOutID: TLargeintField
      AutoGenerateValue = arAutoInc
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere]
      ReadOnly = True
    end
    object quSpecOutNUM: TIntegerField
      FieldName = 'NUM'
      Origin = 'NUM'
    end
    object quSpecOutALCCODE: TStringField
      FieldName = 'ALCCODE'
      Origin = 'ALCCODE'
      Size = 50
    end
    object quSpecOutQUANT: TFloatField
      FieldName = 'QUANT'
      Origin = 'QUANT'
      DisplayFormat = '0.000'
    end
    object quSpecOutQUANTREMN: TFloatField
      FieldName = 'QUANTREMN'
      Origin = 'QUANTREMN'
      ReadOnly = True
      DisplayFormat = '0.000'
    end
    object quSpecOutQUANTF: TFloatField
      FieldName = 'QUANTF'
      Origin = 'QUANTF'
      DisplayFormat = '0.000'
    end
    object quSpecOutPRICE: TFloatField
      FieldName = 'PRICE'
      Origin = 'PRICE'
      DisplayFormat = '0.00'
    end
    object quSpecOutPRICEF: TFloatField
      FieldName = 'PRICEF'
      Origin = 'PRICEF'
      DisplayFormat = '0.00'
    end
    object quSpecOutNAME: TMemoField
      FieldName = 'NAME'
      Origin = 'NAME'
      BlobType = ftMemo
      Size = 2147483647
    end
    object quSpecOutVOL: TSingleField
      FieldName = 'VOL'
      Origin = 'VOL'
      DisplayFormat = '0.000'
    end
    object quSpecOutKREP: TSingleField
      FieldName = 'KREP'
      Origin = 'KREP'
      DisplayFormat = '0.00'
    end
    object quSpecOutPRODNAME: TMemoField
      FieldName = 'PRODNAME'
      Origin = 'PRODNAME'
      BlobType = ftMemo
      Size = 2147483647
    end
    object quSpecOutWBREGID: TStringField
      FieldName = 'WBREGID'
      Origin = 'WBREGID'
      Size = 50
    end
    object quSpecOutCODECB: TIntegerField
      FieldName = 'CODECB'
      Origin = 'CODECB'
    end
    object quSpecOutINFO_A: TStringField
      FieldName = 'INFO_A'
      Origin = 'INFO_A'
      Size = 50
    end
    object quSpecOutINFO_B: TStringField
      FieldName = 'INFO_B'
      Origin = 'INFO_B'
      Size = 50
    end
    object quSpecOutPRISEF0: TFloatField
      FieldName = 'PRISEF0'
      Origin = 'PRISEF0'
    end
    object quSpecOutSUMF: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SUMF'
      DisplayFormat = '0.00'
      Calculated = True
    end
  end
  object UpdSQL1: TFDUpdateSQL
    Connection = dmR.FDConnection
    InsertSQL.Strings = (
      'INSERT INTO RAR.dbo.ADOCSOUT_SP'
      '(FSRARID, IDATE, SIDHD, NUM, ALCCODE, '
      '  QUANT, PRICE, QUANTF, PRICEF, PRISEF0, '
      '  WBREGID,INFO_A, INFO_B)'
      
        'VALUES (:NEW_FSRARID, :NEW_IDATE, :NEW_SIDHD, :NEW_NUM, :NEW_ALC' +
        'CODE, '
      
        '  :NEW_QUANT, :NEW_PRICE, :NEW_QUANTF, :NEW_PRICEF, :NEW_PRISEF0' +
        ', '
      '  :NEW_WBREGID,:NEW_INFO_A, :NEW_INFO_B);'
      'SELECT SCOPE_IDENTITY() AS ID, QUANTF, INFO_A, INFO_B'
      'FROM RAR.dbo.ADOCSOUT_SP'
      
        'WHERE FSRARID = :NEW_FSRARID AND IDATE = :NEW_IDATE AND SIDHD = ' +
        ':NEW_SIDHD AND '
      '  ID = SCOPE_IDENTITY()')
    ModifySQL.Strings = (
      'UPDATE RAR.dbo.ADOCSOUT_SP'
      
        'SET FSRARID = :NEW_FSRARID, IDATE = :NEW_IDATE, SIDHD = :NEW_SID' +
        'HD, '
      '  NUM = :NEW_NUM, ALCCODE = :NEW_ALCCODE, QUANT = :NEW_QUANT, '
      
        '  PRICE = :NEW_PRICE, QUANTF = :NEW_QUANTF, PRICEF = :NEW_PRICEF' +
        ', '
      
        '  PRISEF0 = :NEW_PRISEF0, WBREGID = :NEW_WBREGID,INFO_A = :NEW_I' +
        'NFO_A, '
      '  INFO_B = :NEW_INFO_B'
      
        'WHERE FSRARID = :OLD_FSRARID AND IDATE = :OLD_IDATE AND SIDHD = ' +
        ':OLD_SIDHD AND '
      '  ID = :OLD_ID;'
      'SELECT QUANTF, INFO_A, INFO_B'
      'FROM RAR.dbo.ADOCSOUT_SP'
      
        'WHERE FSRARID = :NEW_FSRARID AND IDATE = :NEW_IDATE AND SIDHD = ' +
        ':NEW_SIDHD AND '
      '  ID = :NEW_ID')
    DeleteSQL.Strings = (
      'DELETE FROM RAR.dbo.ADOCSOUT_SP'
      
        'WHERE FSRARID = :OLD_FSRARID AND IDATE = :OLD_IDATE AND SIDHD = ' +
        ':OLD_SIDHD AND '
      '  ID = :OLD_ID')
    FetchRowSQL.Strings = (
      
        'SELECT FSRARID, IDATE, SIDHD, SCOPE_IDENTITY() AS ID, NUM, ALCCO' +
        'DE, '
      '  QUANT, PRICE, QUANTF, PRICEF, PRISEF0, WBREGID, INFO_A, INFO_B'
      'FROM RAR.dbo.ADOCSOUT_SP'
      
        'WHERE FSRARID = :FSRARID AND IDATE = :IDATE AND SIDHD = :SIDHD A' +
        'ND '
      '  ID = :ID')
    Left = 176
    Top = 360
  end
  object dsquSpecOut: TDataSource
    DataSet = quSpecOut
    Left = 88
    Top = 456
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 232
    Top = 200
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svFont]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
    end
    object cxStyle2: TcxStyle
    end
  end
  object PopupMenu1: TPopupMenu
    Images = dmR.SmallImage
    Left = 432
    Top = 272
    object N2: TMenuItem
      Action = acAddPosSpecOut
    end
    object N1: TMenuItem
      Action = acDelPos
    end
  end
  object quE: TFDQuery
    Connection = dmR.FDConnection
    Left = 80
    Top = 280
  end
end
