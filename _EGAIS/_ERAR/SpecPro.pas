unit SpecPro;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  CustomChildForm, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid,
  dxRibbonSkins, dxRibbonCustomizationForm, dxRibbon, dxBar, System.Actions, Vcl.ActnList, Vcl.PlatformDefaultStyleActnCtrls, Vcl.ActnMan;

type
  TfmSpecPro = class(TfmCustomChildForm)
    GridSpecPro: TcxGrid;
    ViewSpecPro: TcxGridDBTableView;
    LevelSpecPro: TcxGridLevel;
    quSpecPro: TFDQuery;
    dsquSpecPro: TDataSource;
    quSpecProIDHEAD: TLargeintField;
    quSpecProID: TLargeintField;
    quSpecProNUM: TIntegerField;
    quSpecProIDCARD: TIntegerField;
    quSpecProNAME: TStringField;
    quSpecProQUANT: TFloatField;
    quSpecProIDM: TIntegerField;
    quSpecProNAMESHORT: TStringField;
    quSpecProKM: TSingleField;
    quSpecProPRICEIN: TFloatField;
    quSpecProSUMIN: TFloatField;
    quSpecProPRICEIN0: TFloatField;
    quSpecProSUMIN0: TFloatField;
    ViewSpecProIDHEAD: TcxGridDBColumn;
    ViewSpecProID: TcxGridDBColumn;
    ViewSpecProNUM: TcxGridDBColumn;
    ViewSpecProIDCARD: TcxGridDBColumn;
    ViewSpecProNAME: TcxGridDBColumn;
    ViewSpecProQUANT: TcxGridDBColumn;
    ViewSpecProIDM: TcxGridDBColumn;
    ViewSpecProNAMESHORT: TcxGridDBColumn;
    ViewSpecProKM: TcxGridDBColumn;
    ViewSpecProPRICEIN: TcxGridDBColumn;
    ViewSpecProSUMIN: TcxGridDBColumn;
    ViewSpecProPRICEIN0: TcxGridDBColumn;
    ViewSpecProSUMIN0: TcxGridDBColumn;
    ActionManager1: TActionManager;
    acClose: TAction;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarGroup1: TdxBarGroup;
    dxRibbon1: TdxRibbon;
    dxRibbon1Tab1: TdxRibbonTab;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acCloseExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

//var
//  fmSpecPro: TfmSpecPro;

implementation

{$R *.dfm}

uses ProDocs, dmRar;

procedure TfmSpecPro.acCloseExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmSpecPro.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
end;

end.
