unit UTMArh;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  CustomChildForm, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ImgList, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit,
  dxRibbonSkins, dxRibbonCustomizationForm, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator, Data.DB, cxDBData,
  cxCheckBox, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Vcl.Menus, FireDAC.Comp.DataSet, FireDAC.Comp.Client, System.Actions, Vcl.ActnList,
  dxBar, cxBarEditItem, dxBarExtItems, cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses, cxGridCustomView,
  cxGrid, dxRibbon, cxTextEdit, cxMemo;

type
  TfmReplyListArh = class(TfmCustomChildForm)
    dxRibbon1: TdxRibbon;
    dxRibbon1Tab1: TdxRibbonTab;
    dxBarManager1: TdxBarManager;
    bmApplyDate: TdxBar;
    bmClose: TdxBar;
    btnClose: TdxBarLargeButton;
    deDateBeg: TdxBarDateCombo;
    deDateEnd: TdxBarDateCombo;
    cbItem1: TcxBarEditItem;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarSubItem1: TdxBarSubItem;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarGroup1: TdxBarGroup;
    ActionList1: TActionList;
    acRefresh: TAction;
    acClose: TAction;
    GridRarARH: TcxGrid;
    ViewReplyARH: TcxGridDBTableView;
    ViewReplyARHDateReply: TcxGridDBColumn;
    ViewReplyARHID: TcxGridDBColumn;
    ViewReplyARHReplyId: TcxGridDBColumn;
    ViewReplyARHReplyPath: TcxGridDBColumn;
    ViewReplyARHReplyFile: TcxGridDBColumn;
    LevelReplyARH: TcxGridLevel;
    quReplyARHList: TFDQuery;
    quReplyARHListID: TLargeintField;
    quReplyARHListIDREC: TLargeintField;
    quReplyARHListReplyId: TStringField;
    quReplyARHListReplyPath: TStringField;
    quReplyARHListReplyFile: TMemoField;
    quReplyARHListIACTIVE: TSmallintField;
    quReplyARHListIDEL: TSmallintField;
    quReplyARHListDateReply: TSQLTimeStampField;
    dsquReplyARHList: TDataSource;
    ViewReplyARHIDREC: TcxGridDBColumn;
    acToReply: TAction;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    ViewReplyARHFSRARID: TcxGridDBColumn;
    quReplyARHListFSRARID: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure acCloseExecute(Sender: TObject);
    procedure acRefreshExecute(Sender: TObject);
    procedure deDateBegChange(Sender: TObject);
    procedure ViewReplyARHDblClick(Sender: TObject);
    procedure acToReplyExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Init;
  end;

var
  fmReplyListArh: TfmReplyListArh;

procedure ShowFormReplyListArh;

implementation

{$R *.dfm}

uses dmRar, ViewXML, EgaisDecode;

procedure ShowFormReplyListArh;
begin
  if Assigned(fmReplyListArh)=False then //����� �� ����������, � ����� �������
    fmReplyListArh:=TfmReplyListArh.Create(Application);
  if fmReplyListArh.WindowState=wsMinimized then //���� ���� ��������, �� ������������� ���
    fmReplyListArh.WindowState:=wsNormal;

  with dmR do
  begin
    if FDConnection.Connected then
    begin
      fmReplyListArh.Init;
    end else
    begin
      ShowMessage('������ �������� ��');
    end;
  end;
  fmReplyListArh.Show;
end;

procedure TfmReplyListArh.Init;
var
  FocusedRow, TopRow: Integer;
  flag: boolean;
begin
  if dmR.FDConnection.Connected then
  begin
    try
      //<--Refresh � ��������������� ������� ������� � �������
      flag:=quReplyARHList.Active;  //���������� ���� �� ������� �������, ��� �������������� ������� �������
      TopRow := ViewReplyARH.Controller.TopRowIndex;
      FocusedRow := ViewReplyARH.DataController.FocusedRowIndex;
      //-->
      ViewReplyARH.BeginUpdate;
      quReplyARHList.Active:=False;
      quReplyARHList.ParamByName('DATEB').AsDateTime:=Trunc(deDateBeg.Date);
      quReplyARHList.ParamByName('DATEE').AsDateTime:=Trunc(deDateEnd.Date)+1;
      quReplyARHList.ParamByName('RARID').AsString:=CommonSet.FSRAR_ID;
      quReplyARHList.Active:=True;
      quReplyARHList.First;
    finally
      ViewReplyARH.EndUpdate;
      //<--��������������� �������
      if flag then begin
        ViewReplyARH.DataController.FocusedRowIndex := FocusedRow;
        ViewReplyARH.Controller.TopRowIndex := TopRow;
      end;
      //-->
    end;
  end;
//  FSRAR_ID:=CommonSet.FSRAR_ID;
end;


procedure TfmReplyListArh.ViewReplyARHDblClick(Sender: TObject);
begin
  if quReplyARHList.RecordCount>0 then
  begin
    if (ViewReplyARH.Controller.FocusedColumn.Name='ViewReplyARHReplyFile') then ShowXMLView(quReplyARHListID.AsInteger,quReplyARHListReplyFile.AsString);
  end;
end;

procedure TfmReplyListArh.acCloseExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmReplyListArh.acRefreshExecute(Sender: TObject);
begin
  Init;
end;

procedure TfmReplyListArh.acToReplyExecute(Sender: TObject);
var iC,iNum,i,j,iNum1:Integer;
    Rec:TcxCustomGridRecord;
    RARID:string;
begin
  //������� �� ������ ����������
  if ViewReplyARH.Controller.SelectedRecordCount>0 then
  begin
    ClearMessageLogLocal;
    ShowMessageLogLocal('����� ... ���� ��������� ����������.');
    iC:=0;

    for i:=0 to ViewReplyARH.Controller.SelectedRecordCount-1 do
    begin
      Rec:=ViewReplyARH.Controller.SelectedRecords[i];

      iNum:=0;
      RARID:='';

      for j:=0 to Rec.ValueCount-1 do
      begin
        if ViewReplyARH.Columns[j].Name='ViewReplyARHID' then begin iNum:=Rec.Values[j]; end;
        if ViewReplyARH.Columns[j].Name='ViewReplyARHFSRARID' then begin RARID:=Rec.Values[j]; end;
      end;

      if (iNum>0)and(RARID>'') then
      begin
        if quReplyARHList.Locate('ID',iNum,[]) then
        begin
          //�������� � ReplyList

          with dmR do
          begin
            try
              ShowMessageLogLocal('   ��������� ��������� - '+its(iNum));

              iNum1:=fGetId(2);

              quS.Active:=False;
              quS.SQL.Clear;
              quS.SQL.Add('');
              quS.SQL.Add('INSERT INTO [dbo].[REPLYLIST]');
              quS.SQL.Add('           ([FSRARID],[ID],[ReplyId],[ReplyPath],[ReplyFile],[IACTIVE],[IDEL],[DateReply])');
              quS.SQL.Add('    (SELECT [FSRARID],[IDREC],[ReplyId],''ARH_''+[ReplyPath],[ReplyFile],[IACTIVE],[IDEL],[DateReply]');
              quS.SQL.Add('    FROM [dbo].[REPLYLISTARH]');
              quS.SQL.Add('    where [ID]='+its(iNum)+')');
              quS.ExecSQL;

              inc(iC);
            except

            end;
          end;
        end;
      end;
    end;
    ShowMessage('���������� '+its(iC)+' ����������.');
    ShowMessageLogLocal('������� ��������.');
  end;
end;

procedure TfmReplyListArh.deDateBegChange(Sender: TObject);
begin
  Init;
end;

procedure TfmReplyListArh.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
  fmReplyListArh:=Nil;
end;

procedure TfmReplyListArh.FormCreate(Sender: TObject);
begin
  deDateBeg.Date:=Date-1;
  deDateEnd.Date:=Date;

  ClearMessageLogLocal;
end;

end.
