object fmDocSpec: TfmDocSpec
  Left = 0
  Top = 0
  Caption = #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1103
  ClientHeight = 550
  ClientWidth = 1271
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1271
    Height = 127
    BarManager = dxBarManager1
    Style = rs2010
    ColorSchemeAccent = rcsaOrange
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1103
      Groups = <
        item
          Caption = 
            '         '#1059#1087#1088#1072#1074#1083#1077#1085#1080#1077'                                             ' +
            '         '
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end>
      Index = 0
    end
  end
  object GridSpec: TcxGrid
    Left = 0
    Top = 127
    Width = 1271
    Height = 423
    Align = alClient
    TabOrder = 5
    LookAndFeel.Kind = lfOffice11
    LookAndFeel.NativeStyle = False
    object ViewSpec: TcxGridDBTableView
      PopupMenu = PopupMenu1
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.InfoPanel.Visible = True
      Navigator.Visible = True
      OnCustomDrawCell = ViewSpecCustomDrawCell
      DataController.DataSource = dsquSpecIn
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'QUANT'
          Column = ViewSpecQUANT
        end
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'QUANTF'
          Column = ViewSpecQUANTF
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Inserting = False
      OptionsSelection.MultiSelect = True
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      object ViewSpecNUM: TcxGridDBColumn
        Caption = #8470' '#1087#1087
        DataBinding.FieldName = 'NUM'
        Options.Editing = False
        Width = 47
      end
      object ViewSpecSNUM: TcxGridDBColumn
        Caption = #8470' '#1087#1087' ('#1089#1090#1088#1086#1082#1072')'
        DataBinding.FieldName = 'SNUM'
        Width = 98
      end
      object ViewSpecALCCODE: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1072#1083#1082#1086#1075#1086#1083#1100#1085#1086#1081' '#1087#1088#1086#1076#1091#1082#1094#1080#1080
        DataBinding.FieldName = 'ALCCODE'
        Options.Editing = False
        Width = 143
      end
      object ViewSpecNAME: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'NAME'
        Options.Editing = False
        Width = 336
      end
      object ViewSpecVOL: TcxGridDBColumn
        Caption = #1054#1073#1098#1077#1084' '#1083'.'
        DataBinding.FieldName = 'VOL'
        Options.Editing = False
      end
      object ViewSpecKREP: TcxGridDBColumn
        Caption = #1050#1088#1077#1087#1086#1089#1090#1100
        DataBinding.FieldName = 'KREP'
        Options.Editing = False
      end
      object ViewSpecQUANT: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086' '#1087#1086' '#1076#1086#1082#1091#1084#1077#1085#1090#1091
        DataBinding.FieldName = 'QUANT'
        Options.Editing = False
        Styles.Content = cxStyle1
      end
      object ViewSpecPRICE: TcxGridDBColumn
        Caption = #1062#1077#1085#1072' '#1087#1086' '#1076#1086#1082#1091#1084#1077#1085#1090#1091
        DataBinding.FieldName = 'PRICE'
        Options.Editing = False
        Width = 77
      end
      object ViewSpecQUANTF: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086' '#1087#1086' '#1092#1072#1082#1090#1091
        DataBinding.FieldName = 'QUANTF'
        Styles.Content = cxStyle1
      end
      object ViewSpecPRICEF: TcxGridDBColumn
        Caption = #1062#1077#1085#1072' '#1087#1088#1080#1093#1086#1076#1072' '#1087#1086' '#1092#1072#1082#1090#1091
        DataBinding.FieldName = 'PRICEF'
        Options.Editing = False
      end
      object ViewSpecADDR: TcxGridDBColumn
        Caption = #1040#1076#1088#1077#1089' '#1087#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1103
        DataBinding.FieldName = 'ADDR'
        Options.Editing = False
        Width = 144
      end
      object ViewSpecPRODNAME: TcxGridDBColumn
        Caption = #1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1100
        DataBinding.FieldName = 'PRODNAME'
        Options.Editing = False
        Width = 116
      end
      object ViewSpecCODECB: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1062#1041
        DataBinding.FieldName = 'CODECB'
      end
      object ViewSpecCODECBNUM: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1062#1041' '#1087#1086' '#8470' '#1087#1087
        DataBinding.FieldName = 'CODECBNUM'
        Options.Editing = False
      end
      object ViewSpecQUANTDOC: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086' '#1087#1086' '#8470' '#1087#1087
        DataBinding.FieldName = 'QUANTDOC'
        Options.Editing = False
        Styles.Content = cxStyle1
      end
      object ViewSpecPRICEDOC: TcxGridDBColumn
        Caption = #1062#1077#1085#1072' '#1087#1086' '#8470' '#1087#1087
        DataBinding.FieldName = 'PRICEDOC'
        Options.Editing = False
      end
      object ViewSpecNAMECBNUM: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077' '#1062#1041' '#1087#1086' '#8470' '#1087#1087
        DataBinding.FieldName = 'NAMECBNUM'
        Options.Editing = False
        Width = 220
      end
      object ViewSpecWBREGID: TcxGridDBColumn
        DataBinding.FieldName = 'WBREGID'
        Options.Editing = False
        Width = 135
      end
      object ViewSpecINFO_A: TcxGridDBColumn
        Caption = #1057#1087#1088#1072#1074#1082#1072' '#1040
        DataBinding.FieldName = 'INFO_A'
        Options.Editing = False
        Width = 123
      end
      object ViewSpecINFO_B: TcxGridDBColumn
        Caption = #1057#1087#1088#1072#1074#1082#1072' '#1041
        DataBinding.FieldName = 'INFO_B'
        Options.Editing = False
        Width = 128
      end
      object ViewSpecPARTY: TcxGridDBColumn
        Caption = #1055#1072#1088#1090#1080#1103
        DataBinding.FieldName = 'PARTY'
        Options.Editing = False
        Width = 127
      end
    end
    object LevelSpec: TcxGridLevel
      GridView = ViewSpec
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      #1044#1077#1081#1089#1090#1074#1080#1103)
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmR.SmallImage
    ImageOptions.LargeImages = dmR.LargeImage
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 540
    Top = 164
    DockControlHeights = (
      0
      0
      0
      0)
    object dxBarManager1Bar1: TdxBar
      Caption = #1059#1087#1088#1072#1074#1083#1077#1085#1080#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 1001
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      Images = dmR.SmallImage
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1047#1072#1082#1088#1099#1090#1100
      CaptionButtons = <>
      DockedLeft = 266
      DockedTop = 0
      FloatLeft = 1305
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = acSave
      Category = 0
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Caption = 
        '                                                                ' +
        '                '
      Category = 0
      Hint = 
        '                                                                ' +
        '                '
      Visible = ivAlways
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = acClose
      Category = 0
    end
    object dxBarGroup1: TdxBarGroup
      Items = ()
    end
  end
  object ActionList1: TActionList
    Images = dmR.SmallImage
    Left = 648
    Top = 168
    object acClose: TAction
      Caption = #1047#1072#1082#1088#1099#1090#1100
      ImageIndex = 100
      OnExecute = acCloseExecute
    end
    object acSave: TAction
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' (Ctrl + S)'
      ImageIndex = 7
      ShortCut = 16467
      OnExecute = acSaveExecute
    end
    object acResetCode: TAction
      Caption = #1057#1086#1087#1086#1089#1090#1072#1074#1080#1090#1100' '#1082#1086#1076
      ImageIndex = 92
      ShortCut = 16465
      OnExecute = acResetCodeExecute
    end
  end
  object quSpecIn: TFDQuery
    CachedUpdates = True
    Connection = dmR.FDConnection
    Transaction = dmR.FDTrans
    UpdateTransaction = dmR.FDTransUpdate
    UpdateObject = UpdSQL1
    SQL.Strings = (
      
        'SELECT sp.[FSRARID],sp.[IDATE],sp.[SIDHD],sp.[ID],sp.[NUM],sp.[A' +
        'LCCODE],sp.[QUANT],sp.[PRICE],sp.[QUANTF],sp.[PRICEF],sp.[PRISEF' +
        '0]'
      
        '       ,ca.NAME, ca.VOL, ca.KREP, prod.NAME as PRODNAME,sp.[WBRE' +
        'GID],prod.[ADDR]'
      
        '       ,sp.[CODECB],sp.[CODECBNUM],sp.[NAMECBNUM],sp.[QUANTDOC],' +
        'sp.[PRICEDOC],sp.[INFO_A],sp.[INFO_B],sp.[PARTY],sp.[SNUM]'
      '  FROM [dbo].[ADOCSIN_SP] sp'
      '  left join [dbo].[ACARDS] ca on ca.KAP=sp.[ALCCODE]'
      '  left join [dbo].[APRODS] prod on prod.PRODID=ca.PRODID'
      '  where sp.[FSRARID]=:RARID'
      '  and sp.[IDATE]=:IDATE'
      '  and sp.[SIDHD]=:SID')
    Left = 88
    Top = 392
    ParamData = <
      item
        Name = 'RARID'
        DataType = ftString
        ParamType = ptInput
        Value = '020000021353'
      end
      item
        Name = 'IDATE'
        DataType = ftInteger
        ParamType = ptInput
        Value = 42333
      end
      item
        Name = 'SID'
        DataType = ftString
        ParamType = ptInput
        Value = '166178000003500451'
      end>
    object quSpecInFSRARID: TStringField
      FieldName = 'FSRARID'
      Origin = 'FSRARID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 50
    end
    object quSpecInIDATE: TIntegerField
      FieldName = 'IDATE'
      Origin = 'IDATE'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quSpecInSIDHD: TStringField
      FieldName = 'SIDHD'
      Origin = 'SIDHD'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 50
    end
    object quSpecInID: TLargeintField
      AutoGenerateValue = arAutoInc
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object quSpecInNUM: TIntegerField
      FieldName = 'NUM'
      Origin = 'NUM'
    end
    object quSpecInALCCODE: TStringField
      FieldName = 'ALCCODE'
      Origin = 'ALCCODE'
      Size = 50
    end
    object quSpecInQUANT: TFloatField
      FieldName = 'QUANT'
      Origin = 'QUANT'
      DisplayFormat = '0.000'
    end
    object quSpecInPRICE: TFloatField
      FieldName = 'PRICE'
      Origin = 'PRICE'
      DisplayFormat = '0.00'
    end
    object quSpecInQUANTF: TFloatField
      FieldName = 'QUANTF'
      Origin = 'QUANTF'
      DisplayFormat = '0.000'
    end
    object quSpecInPRICEF: TFloatField
      FieldName = 'PRICEF'
      Origin = 'PRICEF'
      DisplayFormat = '0.00'
    end
    object quSpecInNAME: TMemoField
      FieldName = 'NAME'
      Origin = 'NAME'
      BlobType = ftMemo
      Size = 2147483647
    end
    object quSpecInPRISEF0: TFloatField
      FieldName = 'PRISEF0'
      Origin = 'PRISEF0'
    end
    object quSpecInVOL: TSingleField
      FieldName = 'VOL'
      Origin = 'VOL'
      DisplayFormat = '0.000'
    end
    object quSpecInKREP: TSingleField
      FieldName = 'KREP'
      Origin = 'KREP'
      DisplayFormat = '0.0'
    end
    object quSpecInPRODNAME: TMemoField
      FieldName = 'PRODNAME'
      Origin = 'PRODNAME'
      BlobType = ftMemo
      Size = 2147483647
    end
    object quSpecInWBREGID: TStringField
      FieldName = 'WBREGID'
      Origin = 'WBREGID'
      Size = 50
    end
    object quSpecInCODECB: TIntegerField
      FieldName = 'CODECB'
      Origin = 'CODECB'
    end
    object quSpecInCODECBNUM: TIntegerField
      FieldName = 'CODECBNUM'
      Origin = 'CODECBNUM'
    end
    object quSpecInNAMECBNUM: TStringField
      FieldName = 'NAMECBNUM'
      Origin = 'NAMECBNUM'
      Size = 200
    end
    object quSpecInQUANTDOC: TFloatField
      FieldName = 'QUANTDOC'
      Origin = 'QUANTDOC'
      DisplayFormat = '0.000'
    end
    object quSpecInPRICEDOC: TFloatField
      FieldName = 'PRICEDOC'
      Origin = 'PRICEDOC'
      DisplayFormat = '0.00'
    end
    object quSpecInINFO_A: TStringField
      FieldName = 'INFO_A'
      Origin = 'INFO_A'
      Size = 50
    end
    object quSpecInINFO_B: TStringField
      FieldName = 'INFO_B'
      Origin = 'INFO_B'
      Size = 50
    end
    object quSpecInPARTY: TStringField
      FieldName = 'PARTY'
      Origin = 'PARTY'
      Size = 50
    end
    object quSpecInSNUM: TStringField
      FieldName = 'SNUM'
      Origin = 'SNUM'
      Size = 50
    end
    object quSpecInADDR: TMemoField
      FieldName = 'ADDR'
      Origin = 'ADDR'
      BlobType = ftMemo
      Size = 2147483647
    end
  end
  object UpdSQL1: TFDUpdateSQL
    Connection = dmR.FDConnection
    InsertSQL.Strings = (
      'INSERT INTO dbo.ADOCSIN_SP'
      '(NUM, ALCCODE, QUANT, PRICE, QUANTF, '
      '  PRICEF, PRISEF0, WBREGID, CODECB, CODECBNUM, '
      '  NAMECBNUM, QUANTDOC, PRICEDOC, INFO_A, '
      '  INFO_B, PARTY, SNUM)'
      
        'VALUES (:NEW_NUM, :NEW_ALCCODE, :NEW_QUANT, :NEW_PRICE, :NEW_QUA' +
        'NTF, '
      
        '  :NEW_PRICEF, :NEW_PRISEF0, :NEW_WBREGID, :NEW_CODECB, :NEW_COD' +
        'ECBNUM, '
      '  :NEW_NAMECBNUM, :NEW_QUANTDOC, :NEW_PRICEDOC, :NEW_INFO_A, '
      '  :NEW_INFO_B, :NEW_PARTY, :NEW_SNUM);'
      
        'SELECT SCOPE_IDENTITY() AS ID, NUM, ALCCODE, QUANT, PRICE, QUANT' +
        'F, '
      '  PRICEF, PRISEF0, WBREGID, CODECB, CODECBNUM, NAMECBNUM, '
      '  QUANTDOC, PRICEDOC, INFO_A, INFO_B, PARTY, SNUM'
      'FROM dbo.ADOCSIN_SP'
      
        'WHERE FSRARID = :NEW_FSRARID AND IDATE = :NEW_IDATE AND SIDHD = ' +
        ':NEW_SIDHD AND '
      '  ID = SCOPE_IDENTITY()')
    ModifySQL.Strings = (
      'UPDATE dbo.ADOCSIN_SP'
      'SET NUM = :NEW_NUM, ALCCODE = :NEW_ALCCODE, QUANT = :NEW_QUANT, '
      
        '  PRICE = :NEW_PRICE, QUANTF = :NEW_QUANTF, PRICEF = :NEW_PRICEF' +
        ', '
      
        '  PRISEF0 = :NEW_PRISEF0, WBREGID = :NEW_WBREGID, CODECB = :NEW_' +
        'CODECB, '
      '  CODECBNUM = :NEW_CODECBNUM, NAMECBNUM = :NEW_NAMECBNUM, '
      
        '  QUANTDOC = :NEW_QUANTDOC, PRICEDOC = :NEW_PRICEDOC, INFO_A = :' +
        'NEW_INFO_A, '
      '  INFO_B = :NEW_INFO_B, PARTY = :NEW_PARTY, SNUM = :NEW_SNUM'
      
        'WHERE FSRARID = :OLD_FSRARID AND IDATE = :OLD_IDATE AND SIDHD = ' +
        ':OLD_SIDHD AND '
      '  ID = :OLD_ID;'
      
        'SELECT ID, NUM, ALCCODE, QUANT, PRICE, QUANTF, PRICEF, PRISEF0, ' +
        'WBREGID, '
      '  CODECB, CODECBNUM, NAMECBNUM, QUANTDOC, PRICEDOC, INFO_A, '
      '  INFO_B, PARTY, SNUM'
      'FROM dbo.ADOCSIN_SP'
      
        'WHERE FSRARID = :NEW_FSRARID AND IDATE = :NEW_IDATE AND SIDHD = ' +
        ':NEW_SIDHD AND '
      '  ID = :NEW_ID')
    DeleteSQL.Strings = (
      'DELETE FROM dbo.ADOCSIN_SP'
      
        'WHERE FSRARID = :OLD_FSRARID AND IDATE = :OLD_IDATE AND SIDHD = ' +
        ':OLD_SIDHD AND '
      '  ID = :OLD_ID')
    FetchRowSQL.Strings = (
      
        'SELECT FSRARID, IDATE, SIDHD, SCOPE_IDENTITY() AS ID, NUM, ALCCO' +
        'DE, '
      '  QUANT, PRICE, QUANTF, PRICEF, PRISEF0, WBREGID, CODECB, '
      '  CODECBNUM, NAMECBNUM, QUANTDOC, PRICEDOC, INFO_A, INFO_B, '
      '  PARTY, SNUM'
      'FROM dbo.ADOCSIN_SP'
      
        'WHERE FSRARID = :FSRARID AND IDATE = :IDATE AND SIDHD = :SIDHD A' +
        'ND '
      '  ID = :ID')
    Left = 176
    Top = 392
  end
  object dsquSpecIn: TDataSource
    DataSet = quSpecIn
    Left = 88
    Top = 456
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 232
    Top = 200
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svFont]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
    end
    object cxStyle2: TcxStyle
    end
  end
  object PopupMenu1: TPopupMenu
    Images = dmR.SmallImage
    Left = 432
    Top = 272
    object N1: TMenuItem
      Action = acResetCode
    end
  end
end
