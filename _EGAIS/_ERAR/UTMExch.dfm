object fmUTMExch: TfmUTMExch
  Left = 0
  Top = 0
  Caption = #1059#1058#1052' '#1086#1073#1084#1077#1085
  ClientHeight = 565
  ClientWidth = 1042
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1042
    Height = 127
    ApplicationButton.Visible = False
    BarManager = dxBarManager1
    Style = rs2010
    ColorSchemeAccent = rcsaOrange
    ColorSchemeName = 'Blue'
    SupportNonClientDrawing = True
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1059#1058#1052' '#1086#1073#1084#1077#1085
      Groups = <
        item
          ToolbarName = 'bmApplyDate'
        end
        item
          ToolbarName = 'bmClose'
        end>
      Index = 0
    end
  end
  object GridRar: TcxGrid
    Left = 0
    Top = 127
    Width = 1042
    Height = 438
    Align = alClient
    TabOrder = 5
    LevelTabs.Style = 8
    LookAndFeel.Kind = lfOffice11
    LookAndFeel.NativeStyle = False
    RootLevelOptions.DetailTabsPosition = dtpTop
    object ViewSend: TcxGridDBTableView
      PopupMenu = pmSend
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.InfoPanel.Visible = True
      Navigator.Visible = True
      DataController.DataSource = dsquSendList
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      Images = dmR.SmallImage
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.MultiSelect = True
      object ViewSendFSRARID: TcxGridDBColumn
        DataBinding.FieldName = 'FSRARID'
        Width = 153
      end
      object ViewSendDateSend: TcxGridDBColumn
        Caption = #1044#1072#1090#1072
        DataBinding.FieldName = 'DateSend'
        Width = 128
      end
      object ViewSendRECEIVE_ID: TcxGridDBColumn
        Caption = 'ID '#1079#1072#1087#1088#1086#1089#1072
        DataBinding.FieldName = 'RECEIVE_ID'
        Width = 195
      end
      object ViewSendRETVAL: TcxGridDBColumn
        Caption = #1054#1090#1074#1077#1090
        DataBinding.FieldName = 'RETVAL'
        Width = 339
      end
      object ViewSendIACTIVE: TcxGridDBColumn
        Caption = #1057#1090#1072#1090#1091#1089
        DataBinding.FieldName = 'IACTIVE'
      end
      object ViewSendIDEL: TcxGridDBColumn
        Caption = #1059#1076#1072#1083#1077#1085' '#1080#1079' '#1059#1058#1052
        DataBinding.FieldName = 'IDEL'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Images = dmR.SmallImage
        Properties.Items = <
          item
            ImageIndex = 79
            Value = 0
          end
          item
            Description = #1091#1076#1083'.'
            ImageIndex = 82
            Value = 1
          end>
        Width = 80
      end
    end
    object ViewReply: TcxGridDBTableView
      PopupMenu = pmReply
      OnDblClick = ViewReplyDblClick
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.InfoPanel.Visible = True
      Navigator.Visible = True
      DataController.DataSource = dsquReplyList
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      Images = dmR.SmallImage
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.MultiSelect = True
      object ViewReplyFSRARID: TcxGridDBColumn
        DataBinding.FieldName = 'FSRARID'
        Width = 140
      end
      object ViewReplyDateReply: TcxGridDBColumn
        Caption = #1044#1072#1090#1072
        DataBinding.FieldName = 'DateReply'
        Width = 153
      end
      object ViewReplyID: TcxGridDBColumn
        Caption = #1050#1086#1076
        DataBinding.FieldName = 'ID'
        Width = 52
      end
      object ViewReplyReplyId: TcxGridDBColumn
        Caption = 'ID '#1079#1072#1087#1088#1086#1089#1072
        DataBinding.FieldName = 'ReplyId'
        Width = 158
      end
      object ViewReplyReplyPath: TcxGridDBColumn
        Caption = #1055#1091#1090#1100' '#1086#1090#1074#1077#1090#1072
        DataBinding.FieldName = 'ReplyPath'
        Width = 241
      end
      object ViewReplyReplyFile: TcxGridDBColumn
        Caption = #1054#1090#1074#1077#1090
        DataBinding.FieldName = 'ReplyFile'
        Width = 102
      end
      object ViewReplyIACTIVE: TcxGridDBColumn
        Caption = #1057#1090#1072#1090#1091#1089
        DataBinding.FieldName = 'IACTIVE'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Images = dmR.SmallImage
        Properties.Items = <
          item
            Description = #1050' '#1086#1073#1088#1072#1073#1086#1090#1082#1077
            ImageIndex = 80
            Value = 1
          end
          item
            Description = #1042' '#1086#1073#1088#1072#1073#1086#1090#1082#1077
            ImageIndex = 79
            Value = 2
          end
          item
            Description = #1054#1073#1088#1072#1073#1086#1090#1072#1085
            ImageIndex = 81
            Value = 3
          end>
        Width = 108
      end
      object ViewReplyIDEL: TcxGridDBColumn
        Caption = #1059#1076#1072#1083#1077#1085' '#1080#1079' '#1059#1058#1052
        DataBinding.FieldName = 'IDEL'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Images = dmR.SmallImage
        Properties.Items = <
          item
            ImageIndex = 79
            Value = 0
          end
          item
            Description = #1091#1076#1083'.'
            ImageIndex = 82
            Value = 1
          end>
      end
    end
    object ViewQuest: TcxGridDBTableView
      OnDblClick = ViewQuestDblClick
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.InfoPanel.Visible = True
      Navigator.Visible = True
      DataController.DataSource = dsToRarList
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      Images = dmR.SmallImage
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      object ViewQuestFSRARID: TcxGridDBColumn
        DataBinding.FieldName = 'FSRARID'
        Width = 143
      end
      object ViewQuestID: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1086#1090#1087#1088#1072#1074#1082#1080
        DataBinding.FieldName = 'ID'
        Width = 85
      end
      object ViewQuestDATEQU: TcxGridDBColumn
        Caption = #1044#1072#1090#1072' '#1086#1090#1087#1088#1072#1074#1082#1080
        DataBinding.FieldName = 'DATEQU'
        Width = 129
      end
      object ViewQuestITYPEQU: TcxGridDBColumn
        Caption = #1058#1080#1087
        DataBinding.FieldName = 'ITYPEQU'
      end
      object ViewQuestISTATUS: TcxGridDBColumn
        Caption = #1057#1090#1072#1090#1091#1089
        DataBinding.FieldName = 'ISTATUS'
      end
      object ViewQuestSENDFILE: TcxGridDBColumn
        Caption = #1060#1072#1081#1083' '#1086#1090#1087#1088#1072#1074#1082#1080
        DataBinding.FieldName = 'SENDFILE'
      end
      object ViewQuestRECEIVE_ID: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1086#1090#1074#1077#1090#1072
        DataBinding.FieldName = 'RECEIVE_ID'
        Width = 280
      end
      object ViewQuestRECEIVE_FILE: TcxGridDBColumn
        Caption = #1060#1072#1081#1083' '#1086#1090#1074#1077#1090#1072
        DataBinding.FieldName = 'RECEIVE_FILE'
        Width = 149
      end
    end
    object LevelReply: TcxGridLevel
      Caption = #1042#1093#1086#1076#1103#1097#1080#1077
      GridView = ViewReply
    end
    object LevelSend: TcxGridLevel
      Caption = #1048#1089#1093#1086#1076#1103#1097#1080#1077
      GridView = ViewSend
    end
    object LevelQuest: TcxGridLevel
      Caption = #1047#1072#1087#1088#1086#1089#1099
      GridView = ViewQuest
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      #1044#1077#1081#1089#1090#1074#1080#1103)
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmR.SmallImage
    ImageOptions.LargeImages = dmR.LargeImage
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 788
    Top = 52
    DockControlHeights = (
      0
      0
      0
      0)
    object bmApplyDate: TdxBar
      Caption = #1042#1099#1073#1086#1088' '#1087#1077#1088#1080#1086#1076#1072
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 778
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          UserDefine = [udWidth]
          UserWidth = 75
          Visible = True
          ItemName = 'deDateBeg'
        end
        item
          UserDefine = [udWidth]
          UserWidth = 74
          Visible = True
          ItemName = 'deDateEnd'
        end
        item
          Visible = True
          ItemName = 'btnDone'
        end
        item
          ViewLayout = ivlGlyphControlCaption
          Visible = True
          ItemName = 'cbItem1'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object bmClose: TdxBar
      Caption = #1047#1072#1082#1088#1099#1090#1100
      CaptionButtons = <>
      DockedLeft = 370
      DockedTop = 0
      FloatLeft = 778
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'btnClose'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object btnDone: TdxBarLargeButton
      Action = acRefresh
      Category = 0
    end
    object btnClose: TdxBarLargeButton
      Action = acClose
      Category = 0
    end
    object deDateBeg: TdxBarDateCombo
      Caption = 'C  '
      Category = 0
      Hint = 'C  '
      Visible = ivAlways
      OnChange = deDateBegChange
      ImageIndex = 25
      ShowDayText = False
    end
    object deDateEnd: TdxBarDateCombo
      Caption = #1087#1086
      Category = 0
      Hint = #1087#1086
      Visible = ivAlways
      OnChange = deDateBegChange
      ImageIndex = 25
      ShowDayText = False
    end
    object cbItem1: TcxBarEditItem
      Caption = 'XMLNotePad'
      Category = 0
      Hint = 'XMLNotePad'
      Visible = ivAlways
      ShowCaption = True
      Width = 0
      PropertiesClassName = 'TcxCheckBoxProperties'
      Properties.ImmediatePost = True
      InternalEditValue = False
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = acCards
      Category = 0
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = 'New SubItem'
      Category = 0
      Visible = ivAlways
      ItemLinks = <>
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = acUTMExch
      Category = 0
    end
    object dxBarGroup1: TdxBarGroup
      Items = ()
    end
  end
  object ActionList1: TActionList
    Images = dmR.SmallImage
    Left = 560
    Top = 228
    object acRefresh: TAction
      Category = #1060#1086#1088#1084#1072
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100' (F5)'
      Hint = #1054#1073#1085#1086#1074#1080#1090#1100' (F5)'
      ImageIndex = 5
      ShortCut = 116
      OnExecute = acRefreshExecute
    end
    object acClose: TAction
      Category = #1060#1086#1088#1084#1072
      Caption = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 100
      OnExecute = acCloseExecute
    end
    object acSelectDate: TAction
      Category = #1060#1086#1088#1084#1072
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      Hint = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      ImageIndex = 1
      OnExecute = acSelectDateExecute
    end
    object acDecodeTovar: TAction
      Category = #1044#1077#1082#1086#1076#1080#1088#1086#1074#1072#1085#1080#1077
      Caption = #1044#1077#1082#1086#1076#1080#1088#1086#1074#1072#1090#1100' '#1082#1072#1082' '#1090#1086#1074#1072#1088
      ImageIndex = 38
      OnExecute = acDecodeTovarExecute
    end
    object acSaveToFile: TAction
      Category = #1044#1077#1082#1086#1076#1080#1088#1086#1074#1072#1085#1080#1077
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1074' '#1092#1072#1081#1083
      ImageIndex = 7
      OnExecute = acSaveToFileExecute
    end
    object acDelList: TAction
      Category = #1044#1077#1082#1086#1076#1080#1088#1086#1074#1072#1085#1080#1077
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1074#1099#1076#1077#1083#1077#1085#1085#1099#1077' '#1089#1090#1088#1086#1082#1080
      ImageIndex = 4
      ShortCut = 16452
      OnExecute = acDelListExecute
    end
    object acDecodeTTN: TAction
      Category = #1044#1077#1082#1086#1076#1080#1088#1086#1074#1072#1085#1080#1077
      Caption = #1044#1077#1082#1086#1076#1080#1088#1086#1074#1072#1090#1100' '#1082#1072#1082' '#1076#1086#1082#1091#1084#1077#1085#1090
      ImageIndex = 38
      OnExecute = acDecodeTTNExecute
    end
    object acCards: TAction
      Category = #1060#1086#1088#1084#1072
      Caption = #1050#1072#1088#1090#1086#1095#1082#1080
      ImageIndex = 11
      OnExecute = acCardsExecute
    end
    object acSendActR: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1057#1092#1086#1088#1084#1080#1088#1086#1074#1072#1090#1100' '#1072#1082#1090' '#1088#1072#1089#1093#1086#1078#1076#1077#1085#1080#1103
      ImageIndex = 49
      OnExecute = acSendActRExecute
    end
    object acDecodeWB: TAction
      Category = #1044#1077#1082#1086#1076#1080#1088#1086#1074#1072#1085#1080#1077
      Caption = 'acDecodeWB'
      ImageIndex = 38
      OnExecute = acDecodeWBExecute
    end
    object acUTMExch: TAction
      Category = #1060#1086#1088#1084#1072
      Caption = #1059#1058#1052' '#1086#1073#1084#1077#1085
      ImageIndex = 130
      OnExecute = acUTMExchExecute
    end
    object acDecode: TAction
      Category = #1044#1077#1082#1086#1076#1080#1088#1086#1074#1072#1085#1080#1077
      Caption = #1044#1077#1082#1086#1076#1080#1088#1086#1074#1072#1090#1100' '#1074#1099#1076#1077#1083#1077#1085#1085#1099#1077
      ImageIndex = 38
      OnExecute = acDecodeExecute
    end
    object acRemoveToArh: TAction
      Category = #1044#1077#1082#1086#1076#1080#1088#1086#1074#1072#1085#1080#1077
      Caption = #1055#1077#1088#1077#1084#1077#1089#1090#1080#1090#1100' '#1074' '#1072#1088#1093#1080#1074' '#1074#1099#1076#1077#1083#1077#1085#1085#1086#1077
      ImageIndex = 45
      OnExecute = acRemoveToArhExecute
    end
    object acDelFromUTM1: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1080#1079' '#1059#1058#1052' '#1074#1099#1076#1077#1083#1077#1085#1085#1086#1077
      ImageIndex = 2
      OnExecute = acDelFromUTM1Execute
    end
    object acDelFromUTM2: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1080#1079' '#1059#1058#1052' '#1074#1099#1076#1077#1083#1077#1085#1085#1086#1077
      ImageIndex = 2
      OnExecute = acDelFromUTM2Execute
    end
    object acCreateInv: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1057#1086#1079#1076#1072#1090#1100' '#1080#1085#1074#1077#1085#1090#1072#1088#1080#1079#1072#1094#1080#1086#1085#1085#1091#1102' '#1074#1077#1076#1086#1084#1086#1089#1090#1100
      ImageIndex = 127
      OnExecute = acCreateInvExecute
    end
    object acTestCAP: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1055#1088#1086#1074#1077#1088#1082#1072' '#1085#1072#1083#1080#1095#1080#1103' '#1050#1040#1055' '#1074' '#1073#1072#1079#1077' ('#1086#1089#1090#1072#1090#1082#1080' 2 '#1088#1077#1075#1080#1089#1090#1088')'
      ImageIndex = 78
      OnExecute = acTestCAPExecute
    end
  end
  object quSendList: TFDQuery
    Connection = dmR.FDConnection
    SQL.Strings = (
      'SELECT [FSRARID]'
      '      ,[RECEIVE_ID]'
      '      ,[RETVAL]'
      '      ,[IACTIVE]'
      '      ,[IDEL]'
      '      ,[DateSend]'
      '  FROM [dbo].[QULIST]'
      
        'where [DateSend]>=:DATEB and [DateSend]<:DATEE and [FSRARID]=:RA' +
        'RID')
    Left = 64
    Top = 344
    ParamData = <
      item
        Name = 'DATEB'
        DataType = ftDateTime
        ParamType = ptInput
        Value = 42300d
      end
      item
        Name = 'DATEE'
        DataType = ftDateTime
        ParamType = ptInput
        Value = 42361d
      end
      item
        Name = 'RARID'
        DataType = ftString
        ParamType = ptInput
        Value = '111123'
      end>
    object quSendListRECEIVE_ID: TStringField
      FieldName = 'RECEIVE_ID'
      Origin = 'RECEIVE_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 100
    end
    object quSendListRETVAL: TStringField
      FieldName = 'RETVAL'
      Origin = 'RETVAL'
      Size = 500
    end
    object quSendListIACTIVE: TSmallintField
      FieldName = 'IACTIVE'
      Origin = 'IACTIVE'
    end
    object quSendListIDEL: TSmallintField
      FieldName = 'IDEL'
      Origin = 'IDEL'
    end
    object quSendListDateSend: TSQLTimeStampField
      FieldName = 'DateSend'
      Origin = 'DateSend'
    end
    object quSendListFSRARID: TStringField
      FieldName = 'FSRARID'
      Origin = 'FSRARID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 50
    end
  end
  object dsquSendList: TDataSource
    DataSet = quSendList
    Left = 64
    Top = 408
  end
  object quReplyList: TFDQuery
    Connection = dmR.FDConnection
    SQL.Strings = (
      'SELECT [FSRARID]'
      '      ,[ID]'
      '      ,[ReplyId]'
      '      ,[ReplyPath]'
      '      ,[ReplyFile]'
      '      ,[IACTIVE]'
      '      ,[IDEL]'
      '      ,[DateReply]'
      '  FROM [dbo].[REPLYLIST]'
      
        'where [DateReply]>=:DATEB and [DateReply]<:DATEE and [FSRARID]=:' +
        'RARID')
    Left = 144
    Top = 344
    ParamData = <
      item
        Name = 'DATEB'
        DataType = ftDateTime
        ParamType = ptInput
        Value = 42300d
      end
      item
        Name = 'DATEE'
        DataType = ftDateTime
        ParamType = ptInput
        Value = 42361d
      end
      item
        Name = 'RARID'
        DataType = ftString
        ParamType = ptInput
        Value = '1111111'
      end>
    object quReplyListID: TLargeintField
      AutoGenerateValue = arAutoInc
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object quReplyListReplyId: TStringField
      FieldName = 'ReplyId'
      Origin = 'ReplyId'
      Size = 100
    end
    object quReplyListReplyPath: TStringField
      FieldName = 'ReplyPath'
      Origin = 'ReplyPath'
      Size = 500
    end
    object quReplyListReplyFile: TMemoField
      FieldName = 'ReplyFile'
      Origin = 'ReplyFile'
      BlobType = ftMemo
      Size = 2147483647
      Transliterate = False
    end
    object quReplyListIACTIVE: TSmallintField
      FieldName = 'IACTIVE'
      Origin = 'IACTIVE'
    end
    object quReplyListIDEL: TSmallintField
      FieldName = 'IDEL'
      Origin = 'IDEL'
    end
    object quReplyListDateReply: TSQLTimeStampField
      FieldName = 'DateReply'
      Origin = 'DateReply'
    end
    object quReplyListFSRARID: TStringField
      FieldName = 'FSRARID'
      Origin = 'FSRARID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 50
    end
  end
  object dsquReplyList: TDataSource
    DataSet = quReplyList
    Left = 144
    Top = 408
  end
  object pmReply: TPopupMenu
    Images = dmR.SmallImage
    Left = 528
    Top = 304
    object N1: TMenuItem
      Action = acDecode
    end
    object N3: TMenuItem
      Action = acRemoveToArh
    end
    object N6: TMenuItem
      Action = acDelFromUTM1
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object N2: TMenuItem
      Action = acSaveToFile
    end
    object N8: TMenuItem
      Caption = '-'
    end
    object N9: TMenuItem
      Action = acCreateInv
    end
    object N10: TMenuItem
      Action = acTestCAP
    end
  end
  object quS: TFDQuery
    Connection = dmR.FDConnection
    Left = 224
    Top = 344
  end
  object PopupMenu2: TPopupMenu
    Images = dmR.SmallImage
    Left = 664
    Top = 304
    object N5: TMenuItem
      Action = acSendActR
    end
  end
  object ToRarList: TFDQuery
    Connection = dmR.FDConnection
    SQL.Strings = (
      'SELECT [FSRARID]'
      '      ,[ID]'
      '      ,[DATEQU]'
      '      ,[ITYPEQU]'
      '      ,[ISTATUS]'
      '      ,[SENDFILE]'
      '      ,[RECEIVE_ID]'
      '      ,[RECEIVE_FILE]'
      '  FROM [dbo].[TORAR]'
      'where [DATEQU]>=:DATEB and [DATEQU]<:DATEE and [FSRARID]=:RARID'
      'order by [ID] desc')
    Left = 304
    Top = 344
    ParamData = <
      item
        Name = 'DATEB'
        DataType = ftDateTime
        ParamType = ptInput
        Value = 42300d
      end
      item
        Name = 'DATEE'
        DataType = ftDateTime
        ParamType = ptInput
        Value = 42361d
      end
      item
        Name = 'RARID'
        DataType = ftString
        ParamType = ptInput
        Value = '111233333'
      end>
    object ToRarListID: TLargeintField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object ToRarListDATEQU: TSQLTimeStampField
      FieldName = 'DATEQU'
      Origin = 'DATEQU'
    end
    object ToRarListITYPEQU: TSmallintField
      FieldName = 'ITYPEQU'
      Origin = 'ITYPEQU'
    end
    object ToRarListISTATUS: TSmallintField
      FieldName = 'ISTATUS'
      Origin = 'ISTATUS'
    end
    object ToRarListSENDFILE: TMemoField
      FieldName = 'SENDFILE'
      Origin = 'SENDFILE'
      BlobType = ftMemo
      Size = 2147483647
    end
    object ToRarListRECEIVE_ID: TStringField
      FieldName = 'RECEIVE_ID'
      Origin = 'RECEIVE_ID'
      Size = 100
    end
    object ToRarListRECEIVE_FILE: TMemoField
      FieldName = 'RECEIVE_FILE'
      Origin = 'RECEIVE_FILE'
      BlobType = ftMemo
      Size = 2147483647
    end
    object ToRarListFSRARID: TStringField
      FieldName = 'FSRARID'
      Origin = 'FSRARID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 50
    end
  end
  object dsToRarList: TDataSource
    DataSet = ToRarList
    Left = 304
    Top = 408
  end
  object pmSend: TPopupMenu
    Images = dmR.SmallImage
    Left = 592
    Top = 304
    object N7: TMenuItem
      Action = acDelFromUTM2
    end
  end
end
