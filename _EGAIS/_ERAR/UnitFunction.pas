unit UnitFunction;

interface
uses Forms, Windows, Messages, SysUtils, cxGridExportLink, cxGrid, DB,
     IniFiles, ADODB, Variants, IdGlobal, ComCtrls, dxmdaset, ActiveX, Classes, dmRar;

Const CurIni:String = 'Profiles.ini';
      GridIni:String = 'ProfilesGr.ini';
      FormIni:String = 'ProfilesFr.ini';
      sSQLDate:String = 'yyyymmdd';
      sTimeDate:String = 'yyyy-mm-dd hh:nn:ss';
      sWindowsDateFormat:string = 'dd.mm.yyyy';
const m1: array [1..9] of string
      =('���� ','��� ','��� ','������ ','���� ','����� ','���� ','������','������ ');
const f1: array [1..9] of string
      =('���� ','��� ','��� ','������ ','���� ','����� ','���� ','������','������ ');
const n10: array [1..9] of string
      =('������ ','�������� ','�������� ','����� ','��������� ','����������',
       '��������� ','����������� ','��������� ');
const first10: array [11..19] of string
      =('����������� ','���������� ','���������� ','������������','���������� ',
        '����������� ','���������� ','������������ ','������������ ');
const n100: array [1..9] of string
      = ('��� ','������ ','������ ', '��������� ','������� ','��������','������� ',
         '��������� ','��������� ');
const kop: array [1..3] of string = ('�������','�������','������');
      rub: array [1..3] of string = ('����� ','����� ','������ ');
      tsd: array [1..3] of string = ('������ ','������ ','����� ');
      mln: array [1..3] of string = ('������� ','�������� ','��������� ');
      mrd: array [1..3] of string = ('�����ap� ','������p�a ','�����ap�o�');
      trl: array [1..3] of string = ('�������� ','��������a ','��������o�');
      cnt: array [1..3] of string = ('������ ','������ ','����� ');

const cent: array [1..3] of string = ('���� ���','����� ���','������ ���');
      doll: array [1..3] of string = ('������ ','������� ','�������� ');

const   ANSISymb : Ansistring = '�����������������������������������������������������������������';
        OEMSymb : Ansistring = '�����������������������������������������������������������������';

Const MAX_PACKET_SIZE = $10000; // 2^16
      SIO_RCVALL = $98000001;
      WSA_VER = $202;
      MAX_ADAPTER_NAME_LENGTH        = 256;
      MAX_ADAPTER_DESCRIPTION_LENGTH = 128;
      MAX_ADAPTER_ADDRESS_LENGTH    = 8;
      IPHelper = 'iphlpapi.dll';

type
  TSupportedExportType = (exHTML, exXML, exExcel97, exExcel, exPDF, exText);

  TPerson = record
     Id:Integer;
     Id_Parent:integer;
     Name:String;
     Modul:String;
     IModul:Integer;
     WinName:String;
   end;

   TCommonSetUF = record
     IDateB,iDateE,IntervalPanel,StyleOffice,CurShop,CurSkl:Integer;
     AppName,PathReports,PathInventryDB,Ip,CashDB,ColorScheme:string;
   end;

  TPeriodPer = record
     iDateBeg,iDateEnd:integer;
     ActiveGoods:Boolean;
     sClients,sGroups,sSGroups,sBrands,sCategs:string;
     iDeparts,iTopNew:Integer;
     end;
  TRecCens = record
     sTovar:WideString;
     sPrice:WideString;
     sCens:WideString;
     iCount:integer;
     end;

    USHORT = WORD;
    ULONG = DWORD;
    time_t = Longint;
    IP_ADDRESS_STRING = record
      S: array [0..15] of Char;
    end;

    IP_MASK_STRING = IP_ADDRESS_STRING;
    PIP_MASK_STRING = ^IP_MASK_STRING;

    PIP_ADDR_STRING = ^IP_ADDR_STRING;
    IP_ADDR_STRING = record
      Next: PIP_ADDR_STRING;
      IpAddress: IP_ADDRESS_STRING;
      IpMask: IP_MASK_STRING;
      Context: DWORD;
    end;

    PIP_ADAPTER_INFO = ^IP_ADAPTER_INFO;
    IP_ADAPTER_INFO = record
      Next: PIP_ADAPTER_INFO;
      ComboIndex: DWORD;
      AdapterName: array [0..MAX_ADAPTER_NAME_LENGTH + 3] of Char;
      Description: array [0..MAX_ADAPTER_DESCRIPTION_LENGTH + 3] of Char;
      AddressLength: UINT;
      Address: array [0..MAX_ADAPTER_ADDRESS_LENGTH - 1] of BYTE;
      Index: DWORD;
      Type_: UINT;
      DhcpEnabled: UINT;
      CurrentIpAddress: PIP_ADDR_STRING;
      IpAddressList: IP_ADDR_STRING;
      GatewayList: IP_ADDR_STRING;
      DhcpServer: IP_ADDR_STRING;
      HaveWins: BOOL;
      PrimaryWinsServer: IP_ADDR_STRING;
      SecondaryWinsServer: IP_ADDR_STRING;
      LeaseObtained: time_t;
      LeaseExpires: time_t;
    end;

function isNum(const st:string):boolean;
procedure ExportGridToFile(afilename: string; cxGrid: TcxGrid);
function DelP(S:String):String;
function GetGridIniFile: string;
function GetFormIniFile: string;

procedure CloseTe(taT:tdxMemData);
function RV( X: Double ): Double;
Function fs(rSum:Real):String;
function fts1(r:Real):String;
Function its(i:Integer):String;
function dss(d:TDateTime):String; //yyyymmdd
function dts(d:TDateTime):String; //dd.mm.yyyy
Function ToStandart(S:String):String;
function DelPoint(S1:String):String;  //�������� �������
function Upper (s: string): string;
function toVesBar(sBar:string):string;
function isVesBar(sBar:string):Boolean;
function GetCurrentUserName: string;
function MoneyToString(S:Currency; kpk:boolean; usd:boolean):string;
function IIF(Condition:boolean;vTRUE:Variant;vFALSE:Variant):Variant;
//   ������� �� �������� ������������� ����� ������ Currency
// ���������� ��������� ������������� �������� �����, ����:
// '90235.03' -> '��������� ����� ������ �������� ���� ������ 03 �������' .
// ������ ��������� ������������� �������� ����� ����� ��������������
// ���� � ������ � ��������, ���� � �������� ��� � ������ ���.
// ��������� :
//   S - ������������ �����, �� �������� �������� ���� �������������
//        ����� ��������;
//   kpk =True - ������ ������ ������,
//       =False - ������ ������ ��������;
//   usd =True - ������ ����� � �������� ��� � ������ ��� ,
//       =False - ������ ����� � ������ � ��������.

function Line2Win(Line: string; WinLen: byte): string;
function DigitToString00(S:Real): string;
procedure Delay(MSecs: Longint);
function ds1(d:TDateTime):String;
function isValidIPv4(ip:string):Boolean;
function POZ(inStr:string;Str:string;nPos:integer):Integer;
function AnsiToOemConvert(S: Ansistring): Ansistring;
function OemToAnsiConvert(S: string): string;
function RoundEx( X: Double ): Integer;
function IsOLEObjectInstalled(Name: String): boolean;
function normstr(Str:string):string; //������� ������ �������
function rfmt(rSum:Real):String;

function prGetIP:String;
function GetAdaptersInfo(pAdapterInfo: PIP_ADAPTER_INFO; var pOutBufLen: ULONG): DWORD; stdcall; external IPHelper;
function AddCenn(IDCARD:integer;PRICE:Real):string;
procedure ClearCenn;


function fDateToKvartal(iCurD:INteger):Integer;


var Person:TPerson;
    bRefreshCard:Boolean = False;
    bClearStatusBar:Boolean = False;
    bOpen:Boolean = False;
    CommonSetUF:TCommonSetUF;
    recCens: TRecCens;
    PeriodPer:TPeriodPer;//���������� ���������� ��� ���������� ������ ����� �������.
    FireDAC_ERR:String;
    CurDir:string;

implementation

function rfmt(rSum:Real):String;
Var Se:String;
begin
  str(rSum:10:5,Se);
  while pos(',',Se)>0 do Se[pos(',',Se)]:='.';
  while pos(' ',Se)>0 do delete(Se,pos(' ',Se),1);
  Result:=Se;
end;


function fDateToKvartal(iCurD:INteger):Integer;
Var StrM:String;
begin
  StrM:=FormatDateTime('m',iCurD);
  Result:=StrToINtDef(StrM,0);
end;

//-------------------------------------------------------
// ��������� � ������ �������� ��� ������ recCens (������)
//-------------------------------------------------------
function AddCenn(IDCARD:integer;PRICE:Real):string;
begin
 recCens.sCens:=normStr(recCens.sCens+its(IDCARD)+','+fs(RV(PRICE))+';');
 recCens.sTovar:=normStr(recCens.sTovar+','+its(IDCARD));
 recCens.sPrice:=normStr(recCens.sPrice+','+fs(RV(PRICE)));
 recCens.iCount:=recCens.iCount+1;
 Result:=recCens.sTovar;
end;

//-------------------------------------------------------
// ������� ���� ������ recCens (������)
//-------------------------------------------------------
procedure ClearCenn;
begin
 recCens.sTovar:='';
 recCens.sPrice:='';
 recCens.sCens:='';
 recCens.iCount:=0;
 // ����� ����� ������� ������� taCenn, ��������, ��. RefreshListCenn
end;

function prGetIP:String;
var
InterfaceInfo, TmpPointer: PIP_ADAPTER_INFO;
IP: PIP_ADDR_STRING;
Len: ULONG;
StrWk:String;
Ip0:Integer;
begin
  Result:='��';
  StrWk:='';

  if GetAdaptersInfo(nil, Len) = ERROR_BUFFER_OVERFLOW then
  begin
      GetMem(InterfaceInfo, Len);
      try
        if GetAdaptersInfo(InterfaceInfo, Len) = ERROR_SUCCESS then
        begin
            TmpPointer := InterfaceInfo;
            repeat
              IP := @TmpPointer.IpAddressList;
              repeat
                StrWk:=StrWk+Format('%s',[IP^.IpAddress.S])+' ';
//              memo1.Lines.Add(Format('%s - [%s]',[IP^.IpAddress.S, TmpPointer.Description]));
              IP := IP.Next;
              until IP = nil;
              TmpPointer := TmpPointer.Next;
            until TmpPointer = nil;
        end;
      finally
        FreeMem(InterfaceInfo);
      end;
  end
  else Result:='��';
//  ShowMessage('ERROR BUFER OVERFLOW');
  if StrWk>'' then
  begin
   // Ip0:=Pos('0.0.0.0',StrWk);
   // if Ip0>0 then Delete(StrWk,Ip0,8);
   // if Length(StrWk)>14 then StrWk:=Copy(StrWk,1,14);
    Result:=StrWk;
  end;
end;

function normstr(Str:string):string; //������� ������ � ��������� �������, �����, �������, ����� � �������.
begin
 while ((str[1]=',') or (str[1]='.') or (str[1]=' ')) do Delete(str,1,1);
 while ((str[Length(str)]=',') or (str[Length(str)]='.') or (str[Length(str)]=' ')) do Delete(str,Length(str),1);
 Result:=str;
end;

//------------------------------------------------------------------------------
//   ������� �������� ��������� �������� ������� OLE
//------------------------------------------------------------------------------
function IsOLEObjectInstalled(Name: String): boolean;
var
  ClassID: TCLSID;
  Rez : HRESULT;
begin
  Rez := CLSIDFromProgID(PWideChar(WideString(Name)), ClassID);
  if Rez = S_OK then
    Result := true
  else
    Result := false;
end;

//------------------------------------------------------------------------------
//   ������� ��������� ����� � ��������� ������ � �����
//------------------------------------------------------------------------------
function RoundEx( X: Double ): Integer;
var  ScaledFractPart:Integer;
     Temp : Double;
begin
 ScaledFractPart := Trunc(X);
 Temp := Trunc(Frac(X)*1000000)+1;
 if Temp >=  500000 then ScaledFractPart := ScaledFractPart + 1;
 if Temp <= -500000 then ScaledFractPart := ScaledFractPart - 1;
{ if Temp >=  0.5 then ScaledFractPart := ScaledFractPart + 1;
 if Temp <= -0.5 then ScaledFractPart := ScaledFractPart - 1;}
 RoundEx := ScaledFractPart;
end;

function OemToAnsiConvert(S: string): string;
var i, j: byte;
begin
  Result := '';
  for i:=1 to Length(S) do
  begin
   j := Pos(S[i],OEMSymb);
   if (j<>0) then
     Result := Result + ANSISymb[j]
   else
     Result := Result + S[i];
  end;
end;

function AnsiToOemConvert(S: Ansistring): Ansistring;
var i, j: byte;
begin
  Result := '';
  for i:=1 to Length(S) do
  begin
    j := Pos(S[i], ANSISymb);
    if j<>0 then Result := Result + OemSymb[j]
    else Result := Result + S[i];
  end;
end;

// ������� ���� ������� ��������� � ������ �� ������ ���������, ��� � Oracle.
// ����� �������, ��������, � ����� ��������������������� ����� ��������� ������� ���� ���� ��������� ����� �
// POZ('�','���������������������',2) = 5
function POZ(inStr:string;Str:string;nPos:integer):Integer;
var s:string; iPos,iPos1:integer; k:Integer;
begin
 Result:=0;
 if nPos=0 then Exit;
  s:=Str;
  iPos:=0;
  k:=0;
   while Pos(inStr,s)>0 do
   begin
    Inc(k);
    iPos1:=Pos(inStr,s);
    delete(s,1,iPos1);
    iPos:=iPos+iPos1;
    if k=nPos then Break;
   end;
   if k<>nPos then iPos:=0;
 Result:=iPos;
end;

function isValidIPv4(ip:string):Boolean;
var sIP,s:string;i,k:integer;Byte1,Byte2,Byte3,Byte4:string;
    iByte1,iByte2,iByte3,iByte4:integer;
begin
  Result:=True;
  if Length(Trim(ip))>15 then
  begin
    Result:=false;
    Exit;
  end;

  sIP:='1234567890.';
  s:=ip;
  k:=0;
  for i := 1 to Length(s) do
  begin
    if s[i] = '.' then Inc(k);
    if Pos(s[i],sIP)=0 then k:=k+10;
  end;

  if k=3 then
  begin
    Byte1:=copy(s,1,POZ('.',s,1)-1);
    Byte2:=copy(s,POZ('.',s,1)+1,POZ('.',s,2)-POZ('.',s,1)-1);
    Byte3:=copy(s,POZ('.',s,2)+1,POZ('.',s,3)-POZ('.',s,2)-1);
    Byte4:=copy(s,POZ('.',s,3)+1,Length(s)-POZ('.',s,3));
    if ((Byte1<>'') and (Byte2<>'') and (Byte3<>'') and (Byte4<>'')) then
    begin
      iByte1:=StrToInt(Byte1);
      iByte2:=StrToInt(Byte2);
      iByte3:=StrToInt(Byte3);
      iByte4:=StrToInt(Byte4);
      if ((iByte1<0) or (iByte1>255)) then Result:=False;
      if ((iByte2<0) or (iByte2>255)) then Result:=False;
      if ((iByte3<0) or (iByte3>255)) then Result:=False;
      if ((iByte4<0) or (iByte4>255)) then Result:=False;
    end else Result:=false;
  end else Result:=false;
end;

function ds1(d:TDateTime):String;
begin
  result:=FormatDateTime('dd.mm.yyyy',d);
end;

procedure Delay(MSecs: Longint);
var
  FirstTickCount, Now: Longint;
begin
  FirstTickCount := GetTickCount;
  repeat
    Application.ProcessMessages;
    { allowing access to other controls, etc. }
    Now := GetTickCount;
  until (Now - FirstTickCount >= MSecs) or (Now < FirstTickCount);
end;

function isNum(const st:string):boolean;
var i: Integer;
begin
  Result:=True;
  for i := 1 to Length(st) do
    if not (st[i] in ['0'..'9']) then Result:=False;
end;

function IIF(Condition:boolean;vTRUE:Variant;vFALSE:Variant):Variant;
begin
  if Condition then Result:=vTRUE else Result:=vFALSE;
end;


function DelP(S:String):String;
Var Str1:String;
begin
  Str1:=S;
  while pos('.',Str1)>0 do delete(Str1,pos('.',Str1),1);
  while pos(',',Str1)>0 do delete(Str1,pos(',',Str1),1);
  while pos(' ',Str1)>0 do delete(Str1,pos(' ',Str1),1);
  Result:=Str1;
end;


procedure ExportGridToFile(afilename: string; cxGrid: TcxGrid);
var AExportType: TSupportedExportType;
    Ext:String;
begin
  AExportType := exExcel97;
  dmR.SaveDialog.FileName := afilename;
  if dmR.SaveDialog.Execute then begin
    case dmR.SaveDialog.FilterIndex of
      1: begin AExportType := exExcel97; Ext := 'xls'; end;
      2: begin AExportType := exText; Ext := 'csv'; end;
    end;

    if UpperCase(Copy(dmR.SaveDialog.FileName,Length(dmR.SaveDialog.FileName)-2,3)) <> UpperCase(Ext) then
      dmR.SaveDialog.FileName := dmR.SaveDialog.FileName + '.' + Ext;

    case AExportType of
      exHTML:
        ExportGridToHTML(dmR.SaveDialog.FileName, cxGrid);
      exXML:
        ExportGridToXML(dmR.SaveDialog.FileName, cxGrid);
      exExcel97:
        ExportGridToExcel(dmR.SaveDialog.FileName, cxGrid);
      exExcel:
        ExportGridToXLSX(dmR.SaveDialog.FileName, cxGrid);
      exText:
        ExportGridToText(dmR.SaveDialog.FileName, cxGrid);
    end;
  end;
end;

function GetGridIniFile: string;
begin
  if not DirectoryExists(CurDir{+DelP(CommonSet.Ip)}+'\'+Person.Name) then CreateDir(CurDir{+DelP(CommonSet.Ip)}+'\'+Person.Name);

  Result:=CurDir{+DelP(CommonSet.Ip)}+'\'+Person.Name+'\'+GridIni;
end;

function GetFormIniFile: string;
begin
  if Person.Name <> '' then
    Result:=CurDir{+DelP(CommonSet.Ip)}+'\'+Person.Name+'\'+FormIni
  else
    Result:=CurDir{+DelP(CommonSet.Ip)}+'\'+FormIni;
end;

//--------------------------------------------------------------------------------
//   ������� ����������� ������� � ������� �������
//--------------------------------------------------------------------------------
function Upper (s: string): string;
var
  i: integer;
begin
  for i:=1 to length(s) do begin
    if (s[i] in ['a'..'z']) then
      s[i]:=chr(ord(s[i])-32)
    else if (s[i] in ['�'..'�']) then
      s[i]:=chr(ord(s[i])-32)
    else if (s[i] in ['�'..'�']) then
      s[i]:=chr(ord(s[i])-32)
    else if s[i]='�' then
      s[i]:='�'
  end;
  Upper:=s;
end;

//--------------------------------------------------------------------------------
//   ������� ����������� �������� � ������� 7-�� �������
//--------------------------------------------------------------------------------
function toVesBar(sBar:string):string;
begin
  Result:=sBar;
  if (sBar[1]='2')and((sBar[2]='0')or(sBar[2]='1')or(sBar[2]='2')or(sBar[2]='3')or(sBar[2]='4')or(sBar[2]='7')) then  Result:=copy(sBar,1,7); //������� �����
end;

//--------------------------------------------------------------------------------
//   ������� ���������� ������� ��� ��� ����� �� ���������
//--------------------------------------------------------------------------------
function isVesBar(sBar:string):Boolean;
begin
  Result:=false;
  if (sBar[1]='2')and((sBar[2]='0')or(sBar[2]='1')or(sBar[2]='2')or(sBar[2]='3')or(sBar[2]='4')or(sBar[2]='7')) then  Result:=true; //������� �����
end;

//------------------------------------------------------------------------------
//   ������� ���������� �������� ������������
//------------------------------------------------------------------------------
function GetCurrentUserName: string;
const
 cnMaxUserNameLen = 254;
var
  sUserName: string;
  dwUserNameLen: DWORD;
begin
  dwUserNameLen := cnMaxUserNameLen - 1;
  SetLength(sUserName, cnMaxUserNameLen);
  GetUserName(PChar(sUserName), dwUserNameLen);
  SetLength(sUserName, dwUserNameLen-1);

  Result := sUserName;
end;

//------------------------------------------------------------------------------
//   ��������� ������� ������� tdxMemData
//------------------------------------------------------------------------------
procedure CloseTe(taT:tdxMemData);
begin
  taT.Close;
  if taT.Active then
  begin
    taT.First;
    while not taT.Eof do taT.Delete;
    taT.Active:=False;
  end;
//  taT.Free;
//  taT.Create(Application);
  taT.Open;
  if taT.Active then
  begin
    taT.First;
    while not taT.Eof do taT.Delete;
  end;
end;

//------------------------------------------------------------------------------
//   ������� �������������� ��������� � ��������� EAN13
//------------------------------------------------------------------------------
Function ToStandart(S:String):String;
var StrWk,Str1:String;
    iBar: array[1..13] of integer;
    n,c,n1:Integer;
begin
  StrWk:=S;
  while Length(StrWk)<12 do StrWk:=StrWk+'0';
  for n:=1 to 12 do
  begin
    str1:=Copy(StrWk,n,1);
    iBar[n]:=StrToIntDef(Str1,0);
  end;
  //220123401000C
  c:=0;
  n:=(iBar[2]+iBar[4]+iBar[6]+iBar[8]+iBar[10]+iBar[12])*3+(iBar[1]+iBar[3]+iBar[5]+iBar[7]+iBar[9]+iBar[11]);
  for n1:=0 to 9 do
  begin
    if ((n+n1) mod 10)=0 then
    begin
      c:=n1;
      break;
    end;
  end;
  iBar[13]:=c;
  Str1:='';
  for n:=1 to 13 do str1:=Str1+IntToStr(iBar[n]);

  strwk:=Str1;
  Result:=StrWk;
end;

//------------------------------------------------------------------------------
//   ������� ����������� ����� � ������ � ������
//------------------------------------------------------------------------------
Function fs(rSum:Real):String;
Var s:String;
begin
  s:=FloatToStr(rSum);
  while pos(',',s)>0 do s[pos(',',s)]:='.';
  Result:=s;
end;

//------------------------------------------------------------------------------
//   ������� ����������� ����� � ��������� ������� � ������ ��� ����� � �������
//------------------------------------------------------------------------------
function fts1(r:Real):String;
Var s:String;
begin
  s:=Floattostr(r);
  while pos('.',s)>0 do delete(s,pos('.',s),1);
  while pos(',',s)>0 do delete(s,pos(',',s),1);
  Result:=s;
end;

//------------------------------------------------------------------------------
//   ������� ���������� �� �����
//------------------------------------------------------------------------------
function RV( X: Double ): Double;
var  ScaledFractPart:Integer;
     Temp : Double;
begin
 ScaledFractPart := Trunc(X*100);
 if X>=0 then Temp := Trunc(Frac(X*100)*1000000)+1 else  Temp := Trunc(Frac(X*100)*1000000)-1;
 if Temp >=  500000 then ScaledFractPart := ScaledFractPart + 1;
 if Temp <= -500000 then ScaledFractPart := ScaledFractPart - 1;
{ if Temp >=  0.5 then ScaledFractPart := ScaledFractPart + 1;
 if Temp <= -0.5 then ScaledFractPart := ScaledFractPart - 1;}
 RV:= ScaledFractPart/100;
end;

//------------------------------------------------------------------------------
//   ������� ���������� ����� � ������
//------------------------------------------------------------------------------
Function its(i:Integer):String;
begin
  result:=IntToStr(i);
end;

//------------------------------------------------------------------------------
//   ������� ���������� ���� � ����� � ������
//------------------------------------------------------------------------------
function dss(d:TDateTime):String;
begin
  result:=FormatDateTime(sSQLDate,d);
end;

function dts(d:TDateTime):String; //dd.mm.yyyy
begin
  result:=FormatDateTime(sWindowsDateFormat,d);
end;
//------------------------------------------------------------------------------
//   ������� �������� ������� �� �������
//------------------------------------------------------------------------------
function DelPoint(S1:String):String; //������ ������� �� ������ - ��� �������� ����� �� ������ �����
Var S:String;
begin
  S:=S1;
  while pos(',',s)>0 do s[pos(',',s)]:=' ';
  Result:=s;
end;

function Triada(I,n:Integer;k:boolean;usd:boolean):string;
var a,gender,sfx:integer;
begin
  Result:='';
  sfx:=3;
  if n=2 then gender:=0 else gender:=1;

  a:= I div 100;
  if (a>0)
  then begin
         Result:=Result+n100[a];
         I:=I-a*100;
       end;

  if (I>19)
  then begin
         a:= I div 10;
         if (a>0)
         then begin
                Result:=Result+n10[a];
                I:=I-a*10;
                if I>0 then
                  begin
                    if k then gender:=0;
                    if gender=1 then Result:=Result+m1[I]
                                else Result:=Result+f1[I];
                    case I of
                        1: sfx:=1;
                     2..4: sfx:=2;
                     5..9: sfx:=3;
                    end;
                  end;
              end;
       end
  else begin
         case I of
            1:begin
                if k then gender:=0;
                if gender=1 then Result:=Result+m1[I]
                            else Result:=Result+f1[I];
                sfx:=1;
              end;
         2..4:begin
                if k then gender:=0;
                if gender=1 then Result:=Result+m1[I]
                            else Result:=Result+f1[I];
                sfx:=2;
              end;
         5..9:begin
                if k then gender:=0;
                if gender=1 then Result:=Result+m1[I]
                            else Result:=Result+f1[I];
                sfx:=3;
              end;
           10:begin
                Result:=Result+n10[1];
                sfx:=3;
              end;
       11..19:begin
                Result:=Result+first10[I];
                sfx:=3;
              end;
         end;
       end;
  case n of
    1: if not k then
         if usd then
           result:=result+doll[sfx]
          else
           Result:=Result+rub[sfx]
        else
         if usd then
           result:=result+cent[sfx]
          else
           Result:=Result+kop[sfx];
    2: if not k then
         Result:=Result+tsd[sfx]
        else
         if usd then
           result:=result+cent[sfx]
          else
           Result:=Result+kop[sfx];
    3: if not k then
         Result:=Result+mln[sfx]
        else
         if usd then
           result:=result+cent[sfx]
          else
           Result:=Result+kop[sfx];
    4: if not k then
         Result:=Result+mrd[sfx]
        else
         if usd then
           result:=result+cent[sfx]
          else
           Result:=Result+kop[sfx];
    5: if not k then
         Result:=Result+trl[sfx]
        else
         if usd then
           result:=result+cent[sfx]
          else
           Result:=Result+kop[sfx];
  end;
end; // function Triada(I,n:Integer;k:boolean;usd:boolean):string;

function TriadaK(I:integer;kpk:boolean;usd:boolean):string;
var sfx,H:integer;
begin
  if kpk then
    begin
      Result:='';
      sfx:=3;
      H:=(I mod 10);
      case H of
           1: sfx:=1;
        2..4: sfx:=2;
      end;
      if (I in [11..19]) then sfx:=3;
      if usd then
        begin
          if I<10 then Result:='0'+IntToStr(I)+' '+cent[sfx]
                  else Result:=IntToStr(I)+' '+cent[sfx];
        end
      else
        begin
          if I<10 then Result:='0'+IntToStr(I)+' '+kop[sfx]
                  else Result:=IntToStr(I)+' '+kop[sfx];
        end;
    end
  else
    begin
      if i=0 then
        if usd then
          result:='00 ������ ���'
         else
          result:='00 ������'
       else
        result:=triada(i,1,true,usd);
    end;
end; // function TriadaK(I:integer;kpk:boolean;usd:boolean):string;

function MoneyToString(S:Currency; kpk:boolean; usd:boolean):string;
var I,H:LongInt;
    V:string;
    f,l:String;
    s1:Currency;
    dH: Currency;
begin
  V:='';
  s1:=S;

  dH:=1e12;
  I:=Trunc(S/dH);
  if (I>0)
  then begin
         V:=Triada(I,5,false,usd);
         S:=S-Trunc(S/dH)*dH;
       end;

  dH:=1000000000;
  I:=Trunc(S/dH);
  if (I>0)
  then begin
         V:=V+Triada(I,4,false,usd);
         S:=S-Trunc(S/dH)*dH;
       end;

  H:=1000000;
  I:=Trunc(S/H);
  if (I>0)
  then begin
         V:=V+Triada(I,3,false,usd);
         S:=S-Trunc(S/H)*H;
       end;

  H:=1000;
  I:=Trunc(S/H);
  if (I>0)
  then begin
         V:=V+Triada(I,2,false,usd);
         S:=S-Trunc(S/H)*H;
       end;

  H:=1;
  I:=Trunc(S/H);
  if (I>0)
  then begin
       V:=V+Triada(I,1,false,usd);
       S:=S-Trunc(S/H)*H;
       end
  else
   if usd then
    v:=v+doll[3]
   else
    V:=V+rub[3];

  I:=Trunc(S*100);
  V:=V+TriadaK(I,kpk,usd);
  if s1 < 1 then  V:='���� '+V;
  f:=AnsiUpperCase(Copy(V,1,1));
  l:=Copy(V,2,256);
  V:=f+l;
  Result:=V;
end; // function MoneyToString(S:Currency; kpk:boolean; usd:boolean):string;

function Line2Win(Line: string; WinLen: byte): string;
Var
 LineStart, LineEnd: string;

 function LastPos(SS, S: string): byte;
 Var TempPos: byte;
 Begin
 // TempPos:=0;
  Repeat
   TempPos:=Pos(SS, S);
   FillChar(S[TempPos], Length(SS), '_');
  Until Pos(SS, S)=0;
  LastPos:=TempPos;
 End; // function LastPos(SS, S: string): byte;

Begin
 If Length(Line)<=WinLen then Line2Win:=Line
 Else
  Begin
   LineStart:=Copy(Line, 1, 3);
   LineEnd:=Copy(Line, LastPos('\', Line), Length(Line)-LastPos('\',
Line)+1);
   If (Length(LineStart)+Length(LineEnd)+2) >= WinLen then
Line2Win:=LineStart+'..'+LineEnd
   Else
     Line2Win:=LineStart+
     Copy(Line, 4,
Trunc((WinLen-(Length(LineStart)+Length(LineEnd)))/2)-1)+'..'+
     Copy(Line, LastPos('\',
Line)-Trunc((WinLen-(Length(LineStart)+Length(LineEnd)))/2),
Trunc((WinLen-(Length(LineStart)+
     Length(LineEnd)))/2)-1)+LineEnd;
  end;
End; // function Line2Win(Line: string; WinLen: byte): string;

function DigitToString00(S:Real): string;
Var S1:String;
    iP:INteger;
begin
  S1:=MoneyToString(S,False,False);

//const kop: array [1..3] of string = ('�������','�������','������');
//      rub: array [1..3] of string = ('����� ','����� ','������ ');

  iP:=POS('�����',S1); if iP>0 then begin insert('�����',S1,iP); delete(S1,iP+5,5); end;
  iP:=POS('�����',S1); if iP>0 then begin insert('�����',S1,iP); delete(S1,iP+5,5); end;
  iP:=POS('������',S1); if iP>0 then begin insert('�����',S1,iP); delete(S1,iP+5,6); end;

  iP:=POS('�������',S1); if iP>0 then begin delete(S1,iP,7); S1:=S1+'�����'; end;
  iP:=POS('�������',S1); if iP>0 then begin delete(S1,iP,7); S1:=S1+'�����'; end;
  iP:=POS('������',S1); if iP>0 then begin delete(S1,iP,6); S1:=S1+'�����'; end;

  Result:=S1;
end;


end.
