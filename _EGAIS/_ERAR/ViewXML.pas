unit ViewXML;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  CustomChildForm, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  httpsend, synautil, NativeXml, cxLookAndFeelPainters, cxContainer, cxEdit, System.Actions, Vcl.ActnList,
  Vcl.PlatformDefaultStyleActnCtrls, Vcl.ActnMan, Vcl.Menus, cxTextEdit, cxMemo, dxRibbonSkins, dxRibbonCustomizationForm, dxBar, cxClasses,
  dxRibbon;

type
  TfmShowXML = class(TfmCustomChildForm)
    Memo1: TcxMemo;
    PopupMenu1: TPopupMenu;
    ActionManager1: TActionManager;
    acSaveToFile: TAction;
    xml1: TMenuItem;
    acClose: TAction;
    dxBarManager1: TdxBarManager;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarGroup1: TdxBarGroup;
    dxRibbon1: TdxRibbon;
    dxRibbon1Tab1: TdxRibbonTab;
    dxBarManager1Bar1: TdxBar;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acSaveToFileExecute(Sender: TObject);
    procedure acCloseExecute(Sender: TObject);
  private
    { Private declarations }
    FId:Integer;
    //FXml: TNativeXml;
    FStrXML:string;
    FCURDIR:string;
    procedure Init;
  public
    { Public declarations }
  end;

procedure ShowXMLView(ID:Integer;Xmlstr:String);
function GetXmlReadable(XmlText: String): String;

//var
//  fmShowXML: TfmShowXML;

implementation

uses
  dmRar;

{$R *.dfm}

function GetXmlReadable(XmlText: String): String;
var
  Xml: TNativeXml;
begin
  Xml:= TNativeXml.Create(nil);
  Xml.XmlFormat := xfReadable;
  Xml.ReadFromString(XmlText);
  Result:=Xml.WriteToString;
  Xml.Free;
end;

procedure ShowXMLView(ID:Integer;Xmlstr:String);
var F:TfmShowXML;
begin
  F:=TfmShowXML.Create(Application);
//  ShowMessage(Xmlstr);
  F.FStrXML:=Xmlstr;
  F.FiD:=ID;
  F.FCurDir:=ExtractFilePath(ParamStr(0));
  F.Init;
  F.Show;
end;

procedure TfmShowXML.acCloseExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmShowXML.acSaveToFileExecute(Sender: TObject);
begin
  Memo1.Lines.SaveToFile(FCurDir+IntToStr(FiD)+'.xml',TEncoding.UTF8);
end;

procedure TfmShowXML.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
end;

procedure TfmShowXML.Init;
begin
  Caption:='XML '+IntToStr(FiD);

  Memo1.Clear;
  Memo1.Text:=UTF8ToString(GetXmlReadable(FStrXML));
end;


end.
