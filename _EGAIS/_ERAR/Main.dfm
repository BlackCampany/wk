object fmMain: TfmMain
  Left = 0
  Top = 0
  Caption = 'ERAR'
  ClientHeight = 755
  ClientWidth = 1445
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIForm
  OldCreateOrder = False
  Position = poDesigned
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon: TdxRibbon
    Left = 0
    Top = 0
    Width = 1445
    Height = 160
    ApplicationButton.Menu = dxRibbonBackstageView
    BarManager = dxBarManager
    Style = rs2010
    ColorSchemeAccent = rcsaOrange
    ColorSchemeName = 'Blue'
    QuickAccessToolbar.Toolbar = brQuickMenu
    ShowMinimizeButton = False
    SupportNonClientDrawing = True
    Contexts = <>
    TabOrder = 2
    TabStop = False
    OnTabChanged = dxRibbonTabChanged
    object rtMainTab: TdxRibbonTab
      Active = True
      Caption = #1043#1083#1072#1074#1085#1072#1103
      Groups = <
        item
          ToolbarName = 'dxBarManagerBar1'
        end
        item
          ToolbarName = 'dxBarManagerBar3'
        end
        item
          ToolbarName = 'dxBarManagerBar2'
        end>
      Index = 0
    end
  end
  object dxRibbonBackstageView: TdxRibbonBackstageView
    Left = 8
    Top = 166
    Width = 553
    Height = 407
    Buttons = <
      item
        BeginGroup = True
        Item = brbtnClose
        Position = mbpAfterTabs
      end>
    Ribbon = dxRibbon
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 732
    Width = 1445
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Width = 300
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Width = 60
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Width = 500
      end>
    Ribbon = dxRibbon
    LookAndFeel.Kind = lfUltraFlat
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object pnlMDITaskBar: TPanel
    Left = 0
    Top = 702
    Width = 1445
    Height = 30
    Margins.Left = 0
    Margins.Right = 0
    Align = alBottom
    BevelOuter = bvLowered
    TabOrder = 3
  end
  object PanelLog: TPanel
    Left = 0
    Top = 612
    Width = 1445
    Height = 90
    Align = alBottom
    BevelOuter = bvNone
    ParentBackground = False
    TabOrder = 8
    Visible = False
    object PanelCloseLog: TPanel
      Left = 0
      Top = 0
      Width = 25
      Height = 90
      Align = alLeft
      BevelOuter = bvNone
      ParentBackground = False
      TabOrder = 0
      object btnCloseLog: TcxButton
        Left = 0
        Top = 0
        Width = 25
        Height = 18
        Align = alTop
        OptionsImage.Glyph.Data = {
          F6000000424DF600000000000000360000002800000008000000080000000100
          180000000000C000000000000000000000000000000000000000FF00FFFF00FF
          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FF000000000000FF00FFFF00FF000000000000FF
          00FFFF00FFFF00FF000000000000000000000000FF00FFFF00FFFF00FFFF00FF
          FF00FF000000000000FF00FFFF00FFFF00FFFF00FFFF00FF0000000000000000
          00000000FF00FFFF00FFFF00FF000000000000FF00FFFF00FF000000000000FF
          00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
        SpeedButtonOptions.CanBeFocused = False
        TabOrder = 0
        TabStop = False
      end
    end
    object MemoLogGlobal: TcxMemo
      Left = 25
      Top = 0
      Align = alClient
      Properties.ReadOnly = True
      Properties.ScrollBars = ssVertical
      Properties.WordWrap = False
      TabOrder = 1
      Height = 90
      Width = 1420
    end
  end
  object SplitterBottom: TcxSplitter
    Left = 0
    Top = 608
    Width = 1445
    Height = 4
    AlignSplitter = salBottom
    ResizeUpdate = True
    Control = PanelLog
    Visible = False
  end
  object ActionManager: TActionManager
    LargeImages = dmR.LargeImage
    Images = dmR.SmallImage
    Left = 329
    Top = 12
    StyleName = 'Platform Default'
    object acViewRar: TAction
      Caption = #1054#1073#1084#1077#1085' '#1089' '#1045#1043#1040#1048#1057
      ImageIndex = 92
      Visible = False
      OnExecute = acViewRarExecute
    end
    object acUTMCheck: TAction
      Caption = #1059#1058#1052' '#1087#1088#1086#1074#1077#1088#1082#1072
      ImageIndex = 166
      OnExecute = acUTMCheckExecute
    end
    object acQBarcode: TAction
      Caption = #1047#1072#1087#1088#1086#1089' '#1085#1077#1095#1080#1090#1072#1077#1084#1099#1093' '#1096#1090#1088#1080#1093#1082#1086#1076#1086#1074
      ImageIndex = 34
      OnExecute = acQBarcodeExecute
    end
    object acClose: TAction
      Caption = #1042#1099#1093#1086#1076' '#1080#1079' '#1087#1088#1086#1075#1088#1072#1084#1084#1099
      Hint = #1042#1099#1093#1086#1076' '#1080#1079' '#1087#1088#1086#1075#1088#1072#1084#1084#1099
      ImageIndex = 0
      OnExecute = acCloseExecute
    end
    object acCards: TAction
      Category = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082#1080
      Caption = #1050#1072#1088#1090#1086#1095#1082#1080' '#1090#1086#1074#1072#1088#1086#1074
      ImageIndex = 11
      OnExecute = acCardsExecute
    end
    object acDocIn: TAction
      Category = #1044#1086#1082#1091#1084#1077#1085#1090#1099
      Caption = #1055#1088#1080#1093#1086#1076#1099
      ImageIndex = 23
      OnExecute = acDocInExecute
    end
    object acUTMProc: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1103
      Caption = #1059#1058#1052' '#1086#1073#1084#1077#1085
      ImageIndex = 38
      OnExecute = acUTMProcExecute
    end
    object acUTMExch: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1103
      Caption = #1059#1058#1052' '#1080#1089#1090#1086#1088#1080#1103
      ImageIndex = 116
      OnExecute = acUTMExchExecute
    end
    object acUTMArh: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1103
      Caption = #1059#1058#1052' '#1072#1088#1093#1080#1074
      ImageIndex = 21
      OnExecute = acUTMArhExecute
    end
    object acDocRet: TAction
      Category = #1044#1086#1082#1091#1084#1077#1085#1090#1099
      Caption = #1042#1086#1079#1074#1088#1072#1090#1099
      ImageIndex = 22
      OnExecute = acDocRetExecute
    end
    object acNATTN: TAction
      Category = #1044#1086#1082#1091#1084#1077#1085#1090#1099
      Caption = #1053#1077#1086#1073#1088#1072#1073#1086#1090#1072#1085#1085#1099#1077' '#1076#1086#1082#1091#1084#1077#1085#1090#1099
      ImageIndex = 56
      OnExecute = acNATTNExecute
    end
    object acDocVn: TAction
      Category = #1044#1086#1082#1091#1084#1077#1085#1090#1099
      Caption = #1042#1085#1091#1090#1088#1077#1085#1085#1077#1077' '#1087#1077#1088#1077#1084#1077#1097#1077#1085#1080#1077
      ImageIndex = 130
      OnExecute = acDocVnExecute
    end
    object acDocInv: TAction
      Category = #1044#1086#1082#1091#1084#1077#1085#1090#1099
      Caption = #1048#1085#1074#1077#1085#1090#1072#1088#1080#1079#1072#1094#1080#1103
      ImageIndex = 127
      OnExecute = acDocInvExecute
    end
    object acDocsCorr: TAction
      Category = #1044#1086#1082#1091#1084#1077#1085#1090#1099
      Caption = #1050#1086#1088#1088#1077#1082#1094#1080#1103' '#1086#1089#1090#1072#1090#1082#1086#1074
      ImageIndex = 70
      OnExecute = acDocsCorrExecute
    end
    object acCashSail: TAction
      Category = #1044#1086#1082#1091#1084#1077#1085#1090#1099
      Caption = #1056#1077#1072#1083#1080#1079#1072#1094#1080#1103' '#1040#1055
      ImageIndex = 151
      OnExecute = acCashSailExecute
    end
    object acDeclar: TAction
      Category = #1044#1086#1082#1091#1084#1077#1085#1090#1099
      Caption = #1044#1077#1082#1083#1072#1088#1072#1094#1080#1103' '#1087#1086' '#1072#1083#1082#1086#1075#1086#1083#1102
      ImageIndex = 19
      OnExecute = acDeclarExecute
    end
    object acClients: TAction
      Category = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082#1080
      Caption = #1050#1086#1085#1090#1088#1072#1075#1077#1085#1090#1099
      ImageIndex = 24
      OnExecute = acClientsExecute
    end
  end
  object dxBarManager: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      #1044#1077#1081#1089#1090#1074#1080#1103)
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmR.SmallImage
    ImageOptions.LargeImages = dmR.LargeImage
    PopupMenuLinks = <>
    UseSystemFont = True
    OnMerge = dxBarManagerMerge
    Left = 412
    Top = 12
    DockControlHeights = (
      0
      0
      0
      0)
    object brQuickMenu: TdxBar
      AllowClose = False
      AllowCustomizing = False
      AllowQuickCustomizing = False
      AllowReset = False
      Caption = #1055#1072#1085#1077#1083#1100' '#1073#1099#1089#1090#1088#1086#1075#1086' '#1076#1086#1089#1090#1091#1087#1072
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 912
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManagerBar1: TdxBar
      Caption = #1062#1077#1085#1090#1088#1072#1083#1100#1085#1072#1103' '#1087#1072#1085#1077#1083#1100
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 912
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton4'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton16'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton11'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton14'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManagerBar2: TdxBar
      Caption = #1042#1099#1093#1086#1076
      CaptionButtons = <>
      DockedLeft = 1297
      DockedTop = 0
      FloatLeft = 912
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'brbtnClose'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManagerBar3: TdxBar
      Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099
      CaptionButtons = <>
      DockedLeft = 978
      DockedTop = 0
      FloatLeft = 1238
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          UserDefine = [udWidth]
          UserWidth = 227
          Visible = True
          ItemName = 'beiPointMF'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = acViewRar
      Category = 0
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = acUTMCheck
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = acQBarcode
      Category = 0
    end
    object brbtnClose: TdxBarLargeButton
      Action = acClose
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = acCards
      Category = 0
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = acDocIn
      Category = 0
    end
    object dxBarEdit1: TdxBarEdit
      Caption = #1058#1086#1095#1082#1072' '#1087#1088#1086#1076#1072#1078' '
      Category = 0
      Hint = #1058#1086#1095#1082#1072' '#1087#1088#1086#1076#1072#1078' '
      Visible = ivAlways
    end
    object beiPointMF: TcxBarEditItem
      Caption = #1054#1088#1075#1072#1085#1080#1079#1072#1094#1080#1103
      Category = 0
      Hint = #1054#1088#1075#1072#1085#1080#1079#1072#1094#1080#1103
      Visible = ivAlways
      OnChange = beiPointMFChange
      PropertiesClassName = 'TcxLookupComboBoxProperties'
      Properties.DropDownAutoSize = True
      Properties.DropDownListStyle = lsFixedList
      Properties.DropDownRows = 30
      Properties.DropDownWidth = 444
      Properties.KeyFieldNames = 'ISHOP'
      Properties.ListColumns = <
        item
          Caption = #1053#1072#1079#1074#1072#1085#1080#1077
          Width = 200
          FieldName = 'Name'
        end
        item
          Caption = #8470' '#1090#1086#1095#1082#1080' '#1087#1088#1086#1076#1072#1078
          Width = 70
          FieldName = 'ISHOP'
        end
        item
          Width = 130
          FieldName = 'RARID'
        end>
      Properties.ListSource = dmR.dsquPoint
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = acUTMProc
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = acUTMExch
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = acUTMArh
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = acDocRet
      Category = 0
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = acNATTN
      Category = 0
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Action = acDocVn
      Category = 0
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = acDocInv
      Category = 0
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = acDocsCorr
      Category = 0
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Action = acCashSail
      Category = 0
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = acDeclar
      Category = 0
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Action = acClients
      Category = 0
    end
    object dxBarGroup1: TdxBarGroup
      Items = ()
    end
  end
  object MainAppIniFileStorage: TJvAppIniFileStorage
    StorageOptions.BooleanStringTrueValues = 'TRUE, YES, Y'
    StorageOptions.BooleanStringFalseValues = 'FALSE, NO, N'
    AutoReload = True
    Location = flCustom
    SubStorages = <>
    Left = 508
    Top = 12
  end
  object JvFormStorage: TJvFormStorage
    AppStorage = MainAppIniFileStorage
    AppStoragePath = 'MainCard\'
    StoredValues = <>
    Left = 604
    Top = 12
  end
end
