unit DocHeaderCB;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  CustomChildForm, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  dxRibbonForm,ShellApi,
  httpsend, synautil, nativexml, IniFiles,
  cxLookAndFeelPainters, dxRibbonSkins, dxRibbonCustomizationForm, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, Data.DB, cxDBData, cxImageComboBox, cxCheckBox, cxTextEdit, cxDBLookupComboBox, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async,
  FireDAC.DApt, FireDAC.UI.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Phys, FireDAC.Phys.MSSQL, FireDAC.Phys.MSSQLDef,
  FireDAC.VCLUI.Wait, FireDAC.Comp.UI, FireDAC.Phys.ODBCBase, FireDAC.Comp.Client, Vcl.Menus, FireDAC.Comp.DataSet, Vcl.ImgList,
  System.Actions, Vcl.ActnList, dxBar, cxBarEditItem, dxBarExtItems, cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxClasses, cxGridCustomView, cxGrid, dxRibbon;

type
  TfmDocCB = class(TfmCustomChildForm)
    dxRibbon1: TdxRibbon;
    dxRibbon1Tab1: TdxRibbonTab;
    dxBarManager1: TdxBarManager;
    bmApplyDate: TdxBar;
    bmClose: TdxBar;
    btnDone: TdxBarLargeButton;
    btnClose: TdxBarLargeButton;
    deDateBeg: TdxBarDateCombo;
    deDateEnd: TdxBarDateCombo;
    dxBarGroup1: TdxBarGroup;
    ActionList1: TActionList;
    acRefresh: TAction;
    acClose: TAction;
    acSelectDate: TAction;
    dsquCBDocIn_HD: TDataSource;
    cbItem1: TcxBarEditItem;
    acDecodeTovar: TAction;
    PopupMenu1: TPopupMenu;
    acSaveToFile: TAction;
    acDelList: TAction;
    acDecodeTTN: TAction;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarSubItem1: TdxBarSubItem;
    acCards: TAction;
    acSendActR: TAction;
    acDecodeWB: TAction;
    GridCBDoc: TcxGrid;
    ViewCBDoc: TcxGridDBTableView;
    LevelCBDoc: TcxGridLevel;
    acUTMExch: TAction;
    dxBarLargeButton2: TdxBarLargeButton;
    ViewCBDocID: TcxGridDBColumn;
    ViewCBDocId_Depart: TcxGridDBColumn;
    ViewCBDocNum: TcxGridDBColumn;
    ViewCBDocDocDate: TcxGridDBColumn;
    ViewCBDocName: TcxGridDBColumn;
    ViewCBDocIACTIVE: TcxGridDBColumn;
    ViewCBDocSFNum: TcxGridDBColumn;
    ViewCBDocSumIn: TcxGridDBColumn;
    ViewCBDocSumOut: TcxGridDBColumn;
    ViewCBDocId_Client: TcxGridDBColumn;
    ViewCBDocFullName: TcxGridDBColumn;
    ViewCBDocDepName: TcxGridDBColumn;
    LevelCBSpec: TcxGridLevel;
    ViewCBSpec: TcxGridDBTableView;
    dsquCBDocsIn_SP: TDataSource;
    ViewCBSpecID: TcxGridDBColumn;
    ViewCBSpecArticul: TcxGridDBColumn;
    ViewCBSpecId_TTN: TcxGridDBColumn;
    ViewCBSpecName: TcxGridDBColumn;
    ViewCBSpecEdIzm: TcxGridDBColumn;
    ViewCBQuant: TcxGridDBColumn;
    ViewCBSpecPriceIn: TcxGridDBColumn;
    ViewCBSpecSumIn: TcxGridDBColumn;
    ViewCBSpecPriceOut: TcxGridDBColumn;
    ViewCBSpecSumOut: TcxGridDBColumn;
    ViewCBSpecFullName: TcxGridDBColumn;
    ViewCBDocNUMBERRAR: TcxGridDBColumn;
    ViewCBDocSID: TcxGridDBColumn;
    ViewCBDocSHIPDATE: TcxGridDBColumn;
    brSelect: TdxBar;
    dxBarLargeButton3: TdxBarLargeButton;
    acSelect: TAction;
    quCBDocIn_HD: TFDQuery;
    quCBDocIn_SP: TFDQuery;
    quCBDocIn_HDId: TLargeintField;
    quCBDocIn_HDId_Depart: TIntegerField;
    quCBDocIn_HDDepName: TStringField;
    quCBDocIn_HDDocNum: TStringField;
    quCBDocIn_HDDocDate: TSQLTimeStampField;
    quCBDocIn_HDId_Shop: TIntegerField;
    quCBDocIn_HDId_Client: TIntegerField;
    quCBDocIn_HDName: TStringField;
    quCBDocIn_HDIACTIVE: TSmallintField;
    quCBDocIn_HDNUMBERRAR: TStringField;
    quCBDocIn_HDSID: TStringField;
    quCBDocIn_HDSHIPDATE: TStringField;
    quCBDocIn_HDSFNum: TStringField;
    quCBDocIn_HDSumIn: TSingleField;
    quCBDocIn_HDSumOut: TSingleField;
    quCBDocIn_HDFullName: TStringField;
    quCBDocIn_SPId_Shop: TIntegerField;
    quCBDocIn_SPId_Depart: TIntegerField;
    quCBDocIn_SPId_TTN: TLargeintField;
    quCBDocIn_SPId: TIntegerField;
    quCBDocIn_SPArticul: TIntegerField;
    quCBDocIn_SPName: TStringField;
    quCBDocIn_SPFullName: TStringField;
    quCBDocIn_SPEdIzm: TSmallintField;
    quCBDocIn_SPQuant: TSingleField;
    quCBDocIn_SPPriceIn: TFloatField;
    quCBDocIn_SPPriceOut: TFloatField;
    quCBDocIn_SPSumIn: TSingleField;
    quCBDocIn_SPSumOut: TSingleField;
    procedure acRefreshExecute(Sender: TObject);
    procedure acCloseExecute(Sender: TObject);
    procedure acSelectDateExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure deDateBegChange(Sender: TObject);
    procedure ViewCBDocDblClick(Sender: TObject);
    procedure acSelectExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    FISHOP:Integer;
    FNAMESHOP:string;
    FDATEB,FDATEE: TDateTime;
    FSDEP:string;
  public
    { Public declarations }
    procedure Init;
  end;


procedure prShowCBDocs(ISHOP:Integer; NAMESHOP:string; DateB,DateE:TDateTime; sDep:string);
function prSelectCBDocs(ISHOP:Integer; NAMESHOP:string; DateB,DateE:TDateTime;sDep:String; var IDHEAD: Integer):Boolean;

//  var  fmMCrystDoc: TfmMCrystDoc;

implementation

{$R *.dfm}

uses ViewXML, EgaisDecode, ViewCards, dmRar, ViewRar, SpecMC, SpecCB;

procedure prShowCBDocs(ISHOP:Integer; NAMESHOP:string; DateB,DateE:TDateTime; sDep:string);
var F:tfmDocCB;
begin
  F:=tfmDocCB.Create(Application);
//  ShowMessage(Xmlstr);
  F.brSelect.Visible:=False;
  F.FISHOP:=ISHOP;
  F.FNAMESHOP:=NAMESHOP;
  F.FDATEB:=DateB;
  F.FDATEE:=DateE;
  F.FSDEP:=sDep;
  F.JvFormStorageCustom.Active:=False;
  F.deDateBeg.Date:=DateB;
  F.deDateEnd.Date:=DateE;

  F.Init;
  F.Show;
end;

function prSelectCBDocs(ISHOP:Integer; NAMESHOP:string; DateB,DateE:TDateTime;sDep:String; var IDHEAD: Integer):Boolean;
var F:TfmDocCB;
begin
  Result:=False;
  IDHEAD:=0;
  F:=TfmDocCB.Create(Application,TFormStyle.fsNormal);
  try
    F.brSelect.Visible:=True;
    F.FISHOP:=ISHOP;
    F.FNAMESHOP:=NAMESHOP;
    F.FDATEB:=DateB;
    F.FDATEE:=DateE;
    F.FSDEP:=sDep;

    F.JvFormStorageCustom.Active:=False;

    F.deDateBeg.Date:=DateB;
    F.deDateEnd.Date:=DateE;

    F.Init;

    //��������� ������
    F.ViewCBDoc.DataController.Filter.BeginUpdate;
    F.ViewCBDoc.DataController.Filter.Root.Clear;
    F.ViewCBDoc.DataController.Filter.Root.BoolOperatorKind := fboOr;
    F.ViewCBDoc.DataController.Filter.Root.AddItem(F.ViewCBDocNUMBERRAR, foEqual, null, 'Null' );
    F.ViewCBDoc.DataController.Filter.Root.AddItem(F.ViewCBDocNUMBERRAR, foEqual, '0', '0' );
    F.ViewCBDoc.DataController.Filter.Active:=True;
    F.ViewCBDoc.DataController.Filter.EndUpdate;

    F.ShowModal;

    if F.ModalResult=mrOk then
    begin
      IDHEAD:=F.quCBDocIn_HDId.AsInteger;
      Result:=True;
    end;
  finally
    F.Free;
  end;
end;

procedure TfmDocCB.Init;
var
  FocusedRow, TopRow: Integer;
  flag: boolean;
begin
  if dmR.FDConnection.Connected then
  begin
    Caption:=FNAMESHOP+' ( '+FSDEP+' ) c '+ds1(deDateBeg.Date)+' �� '+ds1(deDateEnd.Date);

    try
      //<--Refresh � ��������������� ������� ������� � �������
      flag:=quCBDocIn_HD.Active;  //���������� ���� �� ������� �������, ��� �������������� ������� �������
      TopRow := ViewCBDoc.Controller.TopRowIndex;
      FocusedRow := ViewCBDoc.DataController.FocusedRowIndex;
      //-->
      ViewCBDoc.BeginUpdate;
      ViewCBSpec.BeginUpdate;

      quCBDocIn_HD.Active:=False;
      quCBDocIn_HD.ParamByName('DATEB').AsDateTime:=Trunc(deDateBeg.Date);
      quCBDocIn_HD.ParamByName('DATEE').AsDateTime:=Trunc(deDateEnd.Date);
      quCBDocIn_HD.ParamByName('SDEP').AsString:=FSDEP;
      quCBDocIn_HD.Active:=True;
      quCBDocIn_HD.First;

    finally
      ViewCBSpec.EndUpdate;
      ViewCBDoc.EndUpdate;
      //<--��������������� �������
      if flag then begin
        ViewCBDoc.DataController.FocusedRowIndex := FocusedRow;
        ViewCBDoc.Controller.TopRowIndex := TopRow;
      end;
      //-->
    end;
  end;
end;


procedure TfmDocCB.ViewCBDocDblClick(Sender: TObject);
var F : TfmSpecCB;
begin
  if dmR.FDConnection.Connected then
  begin
    if FormStyle=fsMDIChild then
    begin
      if quCBDocIn_HD.RecordCount>0 then
      begin
        F:=TfmSpecCB.Create(Self);
        F.JvFormStorageCustom.Active:=False;
        F.quSpecCB.Active:=False;
        F.quSpecCB.ParamByName('IDH').Value:=quCBDocIn_HDId.AsInteger;
        F.quSpecCB.Active:=True;
        F.Show;
      end;
    end else ModalResult:=mrOk;
  end;
end;

procedure TfmDocCB.acCloseExecute(Sender: TObject);
begin
  //�������
  Close;
end;

procedure TfmDocCB.acRefreshExecute(Sender: TObject);
begin
  //������
  Init;
end;

procedure TfmDocCB.acSelectDateExecute(Sender: TObject);
begin
  //��������
  Init;
end;

procedure TfmDocCB.acSelectExecute(Sender: TObject);
begin
  ModalResult:=mrOk;
end;

procedure TfmDocCB.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
end;

procedure TfmDocCB.FormCreate(Sender: TObject);
begin
  deDateBeg.Date:=Date-1;
  deDateEnd.Date:=Date;
end;

procedure TfmDocCB.FormShow(Sender: TObject);
begin
  dxRibbon1.ShowTabHeaders:=False;
end;


procedure TfmDocCB.deDateBegChange(Sender: TObject);
begin
  Init;
end;

end.
