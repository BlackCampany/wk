object fmTestRemnInv2: TfmTestRemnInv2
  Left = 0
  Top = 0
  Caption = #1055#1088#1086#1074#1077#1088#1082#1072' '#1076#1086#1089#1090#1091#1087#1085#1086#1089#1090#1080' '#1086#1089#1090#1072#1090#1082#1086#1074
  ClientHeight = 412
  ClientWidth = 533
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object GridTest: TcxGrid
    Left = 0
    Top = 0
    Width = 533
    Height = 412
    Align = alClient
    TabOrder = 0
    ExplicitLeft = 64
    ExplicitTop = 24
    ExplicitWidth = 250
    ExplicitHeight = 200
    object ViewTest: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = dmR.dsquTestRemnInv2
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      object ViewTestNUM: TcxGridDBColumn
        Caption = #8470' '#1087#1087
        DataBinding.FieldName = 'NUM'
        Width = 67
      end
      object ViewTestALCCODE: TcxGridDBColumn
        Caption = #1050#1040#1055
        DataBinding.FieldName = 'ALCCODE'
        Width = 167
      end
      object ViewTestQUANT: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086' '#1087#1086' '#1076#1086#1082#1091#1084#1077#1085#1090#1091
        DataBinding.FieldName = 'QUANT'
        Width = 75
      end
      object ViewTestQUANTR: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086' '#1095#1080#1089#1083'.'
        DataBinding.FieldName = 'QUANTR'
        Width = 71
      end
      object ViewTestQUANTD: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086' '#1088#1072#1079#1085#1080#1094#1072
        DataBinding.FieldName = 'QUANTD'
        Width = 83
      end
    end
    object LevelTest: TcxGridLevel
      GridView = ViewTest
    end
  end
end
