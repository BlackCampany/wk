unit ProDocs;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  CustomChildForm, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  dxRibbonForm, ShellApi, StrUtils, httpsend, synautil, nativexml, IniFiles,
  cxLookAndFeelPainters, dxRibbonSkins, dxRibbonCustomizationForm, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, Data.DB, cxDBData, cxCheckBox, cxTextEdit, cxDBLookupComboBox, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.UI.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Phys, FireDAC.Phys.MSSQL, FireDAC.Phys.MSSQLDef, FireDAC.VCLUI.Wait,
  FireDAC.Comp.UI, FireDAC.Phys.ODBCBase, FireDAC.Comp.Client, Vcl.Menus, FireDAC.Comp.DataSet, Vcl.ImgList, System.Actions, Vcl.ActnList,
  dxBar, cxBarEditItem, dxBarExtItems, cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses, cxGridCustomView,
  cxGrid, dxRibbon, cxImageComboBox;

type
  TfmProDoc = class(TfmCustomChildForm)
    dxRibbon1: TdxRibbon;
    dxRibbon1Tab1: TdxRibbonTab;
    dxBarManager1: TdxBarManager;
    bmApplyDate: TdxBar;
    bmClose: TdxBar;
    bmD: TdxBar;
    btnDone: TdxBarLargeButton;
    btnClose: TdxBarLargeButton;
    deDateBeg: TdxBarDateCombo;
    deDateEnd: TdxBarDateCombo;
    dxBarGroup1: TdxBarGroup;
    ActionList1: TActionList;
    acRefresh: TAction;
    acClose: TAction;
    acSelectDate: TAction;
    quProDocsIn_HD: TFDQuery;
    dsquProDocsIn_HD: TDataSource;
    cbItem1: TcxBarEditItem;
    acDecodeTovar: TAction;
    pmProDocsIn: TPopupMenu;
    acSaveToFile: TAction;
    acDelList: TAction;
    acDecodeTTN: TAction;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarSubItem1: TdxBarSubItem;
    acCards: TAction;
    acSendActR: TAction;
    acDecodeWB: TAction;
    GridProDoc: TcxGrid;
    ViewProDoc: TcxGridDBTableView;
    LevelProDoc: TcxGridLevel;
    acUTMExch: TAction;
    dxBarLargeButton2: TdxBarLargeButton;
    LevelProSpec: TcxGridLevel;
    ViewProSpec: TcxGridDBTableView;
    quProDocsIn_SP: TFDQuery;
    dsquProDocsIn_SP: TDataSource;
    FDConnectPro: TFDConnection;
    FDTrans: TFDTransaction;
    FDTransUpdate: TFDTransaction;
    FDPhysMSSQLDriverLink: TFDPhysMSSQLDriverLink;
    FDGUIxWaitCursor: TFDGUIxWaitCursor;
    quProDocsIn_HDIDSKL: TIntegerField;
    quProDocsIn_HDNAMEMH: TStringField;
    quProDocsIn_HDIDATEDOC: TIntegerField;
    quProDocsIn_HDID: TLargeintField;
    quProDocsIn_HDDATEDOC: TSQLTimeStampField;
    quProDocsIn_HDNUMDOC: TStringField;
    quProDocsIn_HDDATESF: TSQLTimeStampField;
    quProDocsIn_HDNUMSF: TStringField;
    quProDocsIn_HDIDCLI: TIntegerField;
    quProDocsIn_HDNAMECL: TStringField;
    quProDocsIn_HDIACTIVE: TSmallintField;
    ViewProDocIDSKL: TcxGridDBColumn;
    ViewProDocNAMEMH: TcxGridDBColumn;
    ViewProDocID: TcxGridDBColumn;
    ViewProDocDATEDOC: TcxGridDBColumn;
    ViewProDocNUMDOC: TcxGridDBColumn;
    ViewProDocIDCLI: TcxGridDBColumn;
    ViewProDocNAMECL: TcxGridDBColumn;
    ViewProDocIACTIVE: TcxGridDBColumn;
    quProDocsIn_SPIDHEAD: TLargeintField;
    quProDocsIn_SPID: TLargeintField;
    quProDocsIn_SPNUM: TIntegerField;
    quProDocsIn_SPIDCARD: TIntegerField;
    quProDocsIn_SPNAME: TStringField;
    quProDocsIn_SPQUANT: TFloatField;
    quProDocsIn_SPIDM: TIntegerField;
    quProDocsIn_SPNAMESHORT: TStringField;
    quProDocsIn_SPKM: TSingleField;
    quProDocsIn_SPPRICEIN: TFloatField;
    quProDocsIn_SPSUMIN: TFloatField;
    quProDocsIn_SPPRICEIN0: TFloatField;
    quProDocsIn_SPSUMIN0: TFloatField;
    ViewProSpecIDHEAD: TcxGridDBColumn;
    ViewProSpecID: TcxGridDBColumn;
    ViewProSpecNUM: TcxGridDBColumn;
    ViewProSpecIDCARD: TcxGridDBColumn;
    ViewProSpecNAME: TcxGridDBColumn;
    ViewProSpecQUANT: TcxGridDBColumn;
    ViewProSpecIDM: TcxGridDBColumn;
    ViewProSpecNAMESHORT: TcxGridDBColumn;
    ViewProSpecKM: TcxGridDBColumn;
    ViewProSpecPRICEIN: TcxGridDBColumn;
    ViewProSpecSUMIN: TcxGridDBColumn;
    ViewProSpecPRICEIN0: TcxGridDBColumn;
    ViewProSpecSUMIN0: TcxGridDBColumn;
    LevelRealDoc: TcxGridLevel;
    ViewRealDoc: TcxGridDBTableView;
    dsquProDocsOut_HD: TDataSource;
    quProDocsOut_HD: TFDQuery;
    quProDocsOut_HDIDSKL: TIntegerField;
    quProDocsOut_HDNAMEMH: TStringField;
    quProDocsOut_HDIDATEDOC: TIntegerField;
    quProDocsOut_HDID: TLargeintField;
    quProDocsOut_HDDATEDOC: TSQLTimeStampField;
    quProDocsOut_HDNUMDOC: TStringField;
    quProDocsOut_HDDATESF: TSQLTimeStampField;
    quProDocsOut_HDNUMSF: TStringField;
    quProDocsOut_HDIDCLI: TIntegerField;
    quProDocsOut_HDNAMECL: TStringField;
    quProDocsOut_HDIACTIVE: TSmallintField;
    ViewRealDocIDSKL: TcxGridDBColumn;
    ViewRealDocNAMEMH: TcxGridDBColumn;
    ViewRealDocIDATEDOC: TcxGridDBColumn;
    ViewRealDocID: TcxGridDBColumn;
    ViewRealDocDATEDOC: TcxGridDBColumn;
    ViewRealDocNUMDOC: TcxGridDBColumn;
    ViewRealDocDATESF: TcxGridDBColumn;
    ViewRealDocNUMSF: TcxGridDBColumn;
    ViewRealDocIDCLI: TcxGridDBColumn;
    ViewRealDocNAMECL: TcxGridDBColumn;
    ViewRealDocIACTIVE: TcxGridDBColumn;
    ViewProDocIDATEDOC: TcxGridDBColumn;
    ViewProDocDATESF: TcxGridDBColumn;
    ViewProDocNUMSF: TcxGridDBColumn;
    pmProDocsOut: TPopupMenu;
    LevelRealSpec: TcxGridLevel;
    ViewRealSpec: TcxGridDBTableView;
    quProDocsOut_SP: TFDQuery;
    dsquProDocsOut_SP: TDataSource;
    quProDocsOut_SPIDHEAD: TLargeintField;
    quProDocsOut_SPID: TLargeintField;
    quProDocsOut_SPIDCARD: TIntegerField;
    quProDocsOut_SPNAME: TStringField;
    quProDocsOut_SPQUANT: TSingleField;
    quProDocsOut_SPIDM: TIntegerField;
    quProDocsOut_SPNAMESHORT: TStringField;
    quProDocsOut_SPKM: TFloatField;
    ViewRealSpecIDHEAD: TcxGridDBColumn;
    ViewRealSpecID: TcxGridDBColumn;
    ViewRealSpecIDCARD: TcxGridDBColumn;
    ViewRealSpecNAME: TcxGridDBColumn;
    ViewRealSpecQUANT: TcxGridDBColumn;
    ViewRealSpecIDM: TcxGridDBColumn;
    ViewRealSpecNAMESHORT: TcxGridDBColumn;
    ViewRealSpecKM: TcxGridDBColumn;
    quProDocsOut_SPNUM: TLargeintField;
    ViewRealSpecNUM: TcxGridDBColumn;
    quProDocsOut_HDWEEKNUM: TWideStringField;
    ViewRealDocWEEKNUM: TcxGridDBColumn;
    quProDocsOut_HDBZSTATUS: TSmallintField;
    ViewRealDocBZSTATUS: TcxGridDBColumn;
    quProDocsOut_SPPRICER: TSingleField;
    quProDocsOut_SPSumOut0: TFloatField;
    quProDocsOut_SPSUMR: TFloatField;
    ViewRealSpecPRICER: TcxGridDBColumn;
    ViewRealSpecSumOut0: TcxGridDBColumn;
    ViewRealSpecSUMR: TcxGridDBColumn;
    acSendDocsOut: TAction;
    N1: TMenuItem;
    quProDocsOut_SP_ByIDH: TFDQuery;
    quProDocsOut_SP_ByIDHIDHEAD: TLargeintField;
    quProDocsOut_SP_ByIDHID: TLargeintField;
    quProDocsOut_SP_ByIDHNUM: TLargeintField;
    quProDocsOut_SP_ByIDHIDCARD: TIntegerField;
    quProDocsOut_SP_ByIDHNAME: TStringField;
    quProDocsOut_SP_ByIDHQUANT: TSingleField;
    quProDocsOut_SP_ByIDHIDM: TIntegerField;
    quProDocsOut_SP_ByIDHNAMESHORT: TStringField;
    quProDocsOut_SP_ByIDHKM: TFloatField;
    quProDocsOut_SP_ByIDHPRICER: TSingleField;
    quProDocsOut_SP_ByIDHSumOut0: TFloatField;
    quProDocsOut_SP_ByIDHSUMR: TFloatField;
    quProDocsOut_SPCODEZAK: TIntegerField;
    quProDocsOut_SP_ByIDHCODEZAK: TIntegerField;
    procedure acRefreshExecute(Sender: TObject);
    procedure acCloseExecute(Sender: TObject);
    procedure acSelectDateExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure deDateBegChange(Sender: TObject);
    procedure ViewProDocDblClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acSendDocsOutExecute(Sender: TObject);
  private
    { Private declarations }
    FISHOP:Integer;
    FNAMESHOP:string;
    FDATEB,FDATEE: TDateTime;
    FFSRARID: string;
    FIsProduction: Boolean;
    function CreateTTNOut(FSRARID: string; IDH: Int64; IDATE: Integer):Boolean;
  public
    { Public declarations }
    function DBProConnect: boolean;
    procedure Init;
  end;

//  var  fmMCrystDoc: TfmMCrystDoc;

procedure prShowProDocs(DateB,DateE:TDateTime; FSRARID:string; IsProduction: Boolean);

implementation

{$R *.dfm}

uses ViewXML, EgaisDecode, ViewCards, dmRar, ViewRar, SpecPro, Shared_Functions;



procedure prShowProDocs(DateB,DateE:TDateTime; FSRARID:string; IsProduction: Boolean);
var F:TfmProDoc;
begin
  F:=TfmProDoc.Create(Application);
//  ShowMessage(Xmlstr);

  F.FDATEB:=DateB;
  F.FDATEE:=DateE;
  F.FFSRARID:=FSRARID;
  F.FIsProduction:=IsProduction;

  F.deDateBeg.Date:=DateB;
  F.deDateEnd.Date:=DateE;

  if F.DBProConnect then
  begin
    F.Init;
    F.Show;
  end;
end;


function  TfmProDoc.DBProConnect: boolean;
var UDLData: TStrings;
    Params: TStrings;
    CurDir : string;
    Database, Server, User_Name, Password: string;
begin
  Result:=False;
  //����� �� ��������� ����� ���� � ����������� ���������� MSSQL � FireDAC
  //c������ ������ ������������ ����� udl �� ADO
  CurDir := ExtractFilePath(ParamStr(0));
  if FileExists(CurDir+'Pro.udl')=False then Exit;

  UDLData := TStringList.Create;
  Params := TStringList.Create;
  try
    UDLData.LoadFromFile(CurDir+'Pro.udl');

    //<--������ ��������� ����������� ;
    Params.Delimiter:=';';
    Params.StrictDelimiter := True;
    Params.DelimitedText:=UDLData.Text;
    Server:=trim(Params.Values['Data Source']);
    Database:=trim(Params.Values['Initial Catalog']);
    User_Name:=trim(Params.Values['User ID']);
    Password:=trim(Params.Values['Password']);
    //-->

    //<--��������� ��������� ����������
    FDConnectPro.Close;
    FDConnectPro.Params.Values['SERVER']:=Server;
    FDConnectPro.Params.Values['DATABASE']:=Database;
    FDConnectPro.Params.Values['User_Name']:=User_Name;
    FDConnectPro.Params.Values['Password']:=Password;
    FDConnectPro.Params.Values['ApplicationName']:=CommonSet.AppName;
    FDConnectPro.Params.Values['Workstation']:='';
    //DBProConnect.Params.Values['MARS']:='yes';
    FDConnectPro.Params.Values['DriverID']:='MSSQL';
    FDConnectPro.Connected:=True;
    //-->

  finally
    UDLData.Free;
    Params.Free;
  end;

  Sleep(100);
  Result:=FDConnectPro.Connected;
end;


procedure TfmProDoc.Init;
var
  FocusedRow, TopRow: Integer;
  flag: boolean;
begin
  if FDConnectPro.Connected then
  begin
    Caption:='��������� �� ������ c '+ds1(deDateBeg.Date)+' �� '+ds1(deDateEnd.Date);

    try
      //<--Refresh � ��������������� ������� ������� � �������
      flag:=quProDocsIn_HD.Active;  //���������� ���� �� ������� �������, ��� �������������� ������� �������
      TopRow := ViewProDoc.Controller.TopRowIndex;
      FocusedRow := ViewProDoc.DataController.FocusedRowIndex;
      //-->
      ViewProDoc.BeginUpdate;
      ViewProSpec.BeginUpdate;
      ViewRealDoc.BeginUpdate;
      ViewRealSpec.BeginUpdate;

      quProDocsIn_HD.Active:=False;
      quProDocsIn_HD.ParamByName('IDATEB').AsInteger:=Trunc(deDateBeg.Date);
      quProDocsIn_HD.ParamByName('IDATEE').AsInteger:=Trunc(deDateEnd.Date);
      quProDocsIn_HD.Active:=True;
      quProDocsIn_HD.First;

      quProDocsIn_SP.Active:=False;
      quProDocsIn_SP.ParamByName('IDATEB').AsInteger:=Trunc(deDateBeg.Date);
      quProDocsIn_SP.ParamByName('IDATEE').AsInteger:=Trunc(deDateEnd.Date);
      quProDocsIn_SP.Active:=True;
      quProDocsIn_SP.First;

      if FIsProduction then begin
        quProDocsOut_HD.Active:=False;
        quProDocsOut_HD.ParamByName('IDATEB').AsInteger:=Trunc(deDateBeg.Date);
        quProDocsOut_HD.ParamByName('IDATEE').AsInteger:=Trunc(deDateEnd.Date);
        quProDocsOut_HD.Active:=True;
        quProDocsOut_HD.First;

        quProDocsOut_SP.Active:=False;
        quProDocsOut_SP.ParamByName('IDATEB').AsInteger:=Trunc(deDateBeg.Date);
        quProDocsOut_SP.ParamByName('IDATEE').AsInteger:=Trunc(deDateEnd.Date);
        quProDocsOut_SP.Active:=True;
        quProDocsOut_SP.First;
        LevelRealDoc.Visible:=True;  //���������� ������� ��������� (����������)
      end else begin
        LevelRealDoc.Visible:=False; //�������� ������� ��������� (����������)
      end;

    finally
      ViewProSpec.EndUpdate;
      ViewProDoc.EndUpdate;
      ViewRealSpec.EndUpdate;
      ViewRealDoc.EndUpdate;
      //<--��������������� �������
      if flag then begin
        ViewProDoc.DataController.FocusedRowIndex := FocusedRow;
        ViewProDoc.Controller.TopRowIndex := TopRow;
      end;
      //-->
    end;
  end;
end;


procedure TfmProDoc.ViewProDocDblClick(Sender: TObject);
var F:TfmSpecPro;
begin
  if FDConnectPro.Connected then
  begin
    if quProDocsIn_HD.RecordCount>0 then
    begin
      F:=TfmSpecPro.Create(Self);
      F.quSpecPro.Active:=False;
      F.quSpecPro.ParamByName('IDH').Value:=quProDocsIn_HDID.AsInteger;
      F.quSpecPro.Active:=True;
      F.Show;
    end;
  end;
end;

procedure TfmProDoc.acCloseExecute(Sender: TObject);
begin
  //�������
  Close;
end;

procedure TfmProDoc.acRefreshExecute(Sender: TObject);
begin
  //������
  Init;
end;

procedure TfmProDoc.acSelectDateExecute(Sender: TObject);
begin
  //��������
  Init;
end;

procedure TfmProDoc.acSendDocsOutExecute(Sender: TObject);
var Rec:TcxCustomGridRecord;
    i,iC: Integer;
    IDH: Int64;
    IDATE: Integer;
begin
  //����� �� ���������� �� �����, �������� �������
  Exit;

  //������������ ��������� ���������
  if (ViewRealDoc.Controller.SelectedRecordCount>0) then
  begin
    fmViewRar.ClearMessageLogLocal;
    fmViewRar.ShowMessageLogLocal('����� ... ���� ��������� ����������.');
    iC:=0;
    IDH:=0;
    IDATE:=0;

    for i:=0 to ViewRealDoc.Controller.SelectedRecordCount-1 do
    begin
      Rec:=ViewRealDoc.Controller.SelectedRecords[i];
      IDH:=Rec.Values[ViewRealDocID.Index];
      IDATE:=Rec.Values[ViewRealDocIDATEDOC.Index];

      if (IDH>0)and(IDATE>0) then
      begin
        with dmR do
        begin
          fmViewRar.ShowMessageLogLocal('    ���. '+IntToStr(IDH));

          CreateTTNOut(FFSRARID, IDH, IDATE);

          fmViewRar.ShowMessageLogLocal('        c����� '+IntToStr(IDH)+' ��.');
          quProc.Active:=False;
          inc(iC);
        end;
      end;
    end;
    //init;
    fmViewRar.ShowMessageLogLocal('���������� '+its(iC)+' ����������.');
    fmViewRar.ShowMessageLogLocal('������� ��������.');
  end;
end;

procedure TfmProDoc.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
end;

procedure TfmProDoc.FormCreate(Sender: TObject);
begin
  deDateBeg.Date:=Date-1;
  deDateEnd.Date:=Date;
end;

procedure TfmProDoc.FormShow(Sender: TObject);
begin
  if FDConnectPro.Connected then
  begin
//    ShowMessage('���� ��');
    Init;
  end else
  begin
    ShowMessage('������ �������� ��');
    Close;
  end;
end;


procedure TfmProDoc.deDateBegChange(Sender: TObject);
begin
  Init;
end;

function TfmProDoc.CreateTTNOut(FSRARID: string; IDH: Int64; IDATE: Integer):Boolean;
var sCliTo: string;
    SIDOUT: string;
    a: TGUID;
begin
  Result:=False;

  sCliTo:='030000155591';
  CreateGUID(a);
  SIDOUT:=GUIDTostring(a);
  SIDOUT:=AnsiReplaceStr(SIDOUT,'{','');
  SIDOUT:=AnsiReplaceStr(SIDOUT,'}','');

  //<--������� �����
  dmR.quS.Active:=False;
  dmR.quS.SQL.Clear;
  dmR.quS.SQL.Add('DECLARE @FSRARID varchar(50) = '''+Trim(FSRARID)+'''');
  dmR.quS.SQL.Add('DECLARE @IDATE int = '+IntToStr(IDATE));
  dmR.quS.SQL.Add('DECLARE @SIDOUT varchar(50) = '''+Trim(SIDOUT)+'''');
  dmR.quS.SQL.Add('DECLARE @sCliTo varchar(50) = '''+Trim(sCliTo)+'''');
  dmR.quS.SQL.Add('DECLARE @IDHEAD BIGINT = '''+IntToStr(IDH)+'''');
  dmR.quS.SQL.Add('DECLARE @DATEDOC DATETIME = dbo.i2d(@IDATE)');
  dmR.quS.SQL.Add('INSERT INTO [dbo].[ADOCSOUT_HD] ([FSRARID], [IDATE], [SID]  , [NUMBER], [CLIFTO], [IACTIVE], [READYSEND], [WBREGID], [FIXNUMBER], [FIXDATE], [IDHEAD], [DATEDOC] )');
  dmR.quS.SQL.Add('                         VALUES (@FSRARID , @IDATE , @SIDOUT, ''''    , @sCliTo , 0        , 0          , ''''     , ''''       , ''''     , @IDHEAD , @DATEDOC  )');
  //dmR.quS.SQL.SaveToFile('c:\test_h.sql');
  dmR.quS.ExecSQL;
  //-->

  //<--������� ������������
  quProDocsOut_SP_ByIDH.Active:=False;
  quProDocsOut_SP_ByIDH.ParamByName('IDH').AsLargeInt:=IDH;
  quProDocsOut_SP_ByIDH.Active:=True;
  quProDocsOut_SP_ByIDH.First;
  while not quProDocsOut_SP_ByIDH.Eof do
  begin
    dmR.quS.Active:=False;
    dmR.quS.SQL.Clear;
    dmR.quS.SQL.Add('DECLARE @FSRARID varchar(50) = '''+Trim(FSRARID)+'''');
    dmR.quS.SQL.Add('DECLARE @IDATE int = '+IntToStr(IDATE));
    dmR.quS.SQL.Add('DECLARE @SIDOUT varchar(50) = '''+Trim(SIDOUT)+'''');
    dmR.quS.SQL.Add('DECLARE @NUM INT = '+IntToStr(quProDocsOut_SP_ByIDHNUM.AsInteger));
    dmR.quS.SQL.Add('DECLARE @ALCCODE varchar(50)='''+GetKAP(quProDocsOut_SP_ByIDHCODEZAK.AsInteger)+'''');
    dmR.quS.SQL.Add('DECLARE @QUANT FLOAT ='+ fts(r1000(quProDocsOut_SP_ByIDHQUANT.AsFloat)));
    dmR.quS.SQL.Add('DECLARE @PRICE FLOAT ='+fts(r1000(quProDocsOut_SP_ByIDHPRICER.AsFloat)));
    dmR.quS.SQL.Add('DECLARE @PRICEF FLOAT = 0');
    dmR.quS.SQL.Add('DECLARE @PRISEF0 FLOAT = 0');
    dmR.quS.SQL.Add('DECLARE @WBREGID varchar(50)=''''');
    dmR.quS.SQL.Add('DECLARE @CODECB INT = '+IntToStr(quProDocsOut_SP_ByIDHCODEZAK.AsInteger));
    dmR.quS.SQL.Add('DECLARE @INFO_A varchar(50)=''''');
    dmR.quS.SQL.Add('DECLARE @INFO_B varchar(50)=''''');
    dmR.quS.SQL.Add('INSERT INTO [dbo].[ADOCSOUT_SP] ([FSRARID], [IDATE], [SIDHD], [NUM], [ALCCODE], [QUANT], [PRICE], [QUANTF], [PRICEF], [PRISEF0], [WBREGID], [CODECB], [CODECBNUM], [NAMECBNUM], [QUANTDOC], [PRICEDOC], [INFO_A], [INFO_B])');
    dmR.quS.SQL.Add('                         VALUES (@FSRARID , @IDATE , @SIDOUT, @NUM , @ALCCODE , @QUANT , @PRICE , 0       , @PRICEF , @PRISEF0 , @WBREGID , @CODECB , ''''       , ''''       , 0         , 0         , @INFO_A , @INFO_B )');
    //dmR.quS.SQL.SaveToFile('c:\test_s.sql');
    dmR.quS.ExecSQL;

    quProDocsOut_SP_ByIDH.Next;
  end;
  //-->

  Result:=True;
end;

end.
