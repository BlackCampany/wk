object fmReplyListArh: TfmReplyListArh
  Left = 0
  Top = 0
  Caption = #1059#1058#1052' '#1040#1088#1093#1080#1074
  ClientHeight = 562
  ClientWidth = 852
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 852
    Height = 127
    ApplicationButton.Visible = False
    BarManager = dxBarManager1
    Style = rs2010
    ColorSchemeAccent = rcsaOrange
    ColorSchemeName = 'Blue'
    SupportNonClientDrawing = True
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1059#1058#1052' '#1040#1088#1093#1080#1074
      Groups = <
        item
          ToolbarName = 'bmApplyDate'
        end
        item
          ToolbarName = 'bmClose'
        end>
      Index = 0
    end
  end
  object GridRarARH: TcxGrid
    Left = 0
    Top = 127
    Width = 852
    Height = 435
    Align = alClient
    TabOrder = 5
    LevelTabs.Style = 8
    LookAndFeel.Kind = lfOffice11
    LookAndFeel.NativeStyle = False
    RootLevelOptions.DetailTabsPosition = dtpTop
    ExplicitTop = 102
    ExplicitHeight = 387
    object ViewReplyARH: TcxGridDBTableView
      PopupMenu = PopupMenu1
      OnDblClick = ViewReplyARHDblClick
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.InfoPanel.Visible = True
      Navigator.Visible = True
      DataController.DataSource = dsquReplyARHList
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      Images = dmR.SmallImage
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.MultiSelect = True
      object ViewReplyARHFSRARID: TcxGridDBColumn
        DataBinding.FieldName = 'FSRARID'
        Width = 114
      end
      object ViewReplyARHDateReply: TcxGridDBColumn
        Caption = #1044#1072#1090#1072
        DataBinding.FieldName = 'DateReply'
        Width = 89
      end
      object ViewReplyARHID: TcxGridDBColumn
        Caption = #1050#1086#1076
        DataBinding.FieldName = 'ID'
        Width = 52
      end
      object ViewReplyARHIDREC: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1079#1072#1087#1080#1089#1080
        DataBinding.FieldName = 'IDREC'
      end
      object ViewReplyARHReplyId: TcxGridDBColumn
        Caption = 'ID '#1079#1072#1087#1088#1086#1089#1072
        DataBinding.FieldName = 'ReplyId'
        Width = 158
      end
      object ViewReplyARHReplyPath: TcxGridDBColumn
        Caption = #1055#1091#1090#1100' '#1086#1090#1074#1077#1090#1072
        DataBinding.FieldName = 'ReplyPath'
        Width = 241
      end
      object ViewReplyARHReplyFile: TcxGridDBColumn
        Caption = #1054#1090#1074#1077#1090
        DataBinding.FieldName = 'ReplyFile'
        Width = 160
      end
    end
    object LevelReplyARH: TcxGridLevel
      Caption = #1042#1093#1086#1076#1103#1097#1080#1077
      GridView = ViewReplyARH
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      #1044#1077#1081#1089#1090#1074#1080#1103)
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmR.SmallImage
    ImageOptions.LargeImages = dmR.LargeImage
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 788
    Top = 52
    DockControlHeights = (
      0
      0
      0
      0)
    object bmApplyDate: TdxBar
      Caption = #1042#1099#1073#1086#1088' '#1087#1077#1088#1080#1086#1076#1072
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 778
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          UserDefine = [udWidth]
          UserWidth = 75
          Visible = True
          ItemName = 'deDateBeg'
        end
        item
          UserDefine = [udWidth]
          UserWidth = 74
          Visible = True
          ItemName = 'deDateEnd'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object bmClose: TdxBar
      Caption = #1047#1072#1082#1088#1099#1090#1100
      CaptionButtons = <>
      DockedLeft = 198
      DockedTop = 0
      FloatLeft = 778
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'btnClose'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object btnClose: TdxBarLargeButton
      Action = acClose
      Category = 0
    end
    object deDateBeg: TdxBarDateCombo
      Caption = 'C  '
      Category = 0
      Hint = 'C  '
      Visible = ivAlways
      OnChange = deDateBegChange
      ImageIndex = 25
      ShowDayText = False
    end
    object deDateEnd: TdxBarDateCombo
      Caption = #1087#1086
      Category = 0
      Hint = #1087#1086
      Visible = ivAlways
      OnChange = deDateBegChange
      ImageIndex = 25
      ShowDayText = False
    end
    object cbItem1: TcxBarEditItem
      Caption = 'XMLNotePad'
      Category = 0
      Hint = 'XMLNotePad'
      Visible = ivAlways
      ShowCaption = True
      Width = 0
      PropertiesClassName = 'TcxCheckBoxProperties'
      Properties.ImmediatePost = True
      InternalEditValue = False
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = acRefresh
      Category = 0
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = 'New SubItem'
      Category = 0
      Visible = ivAlways
      ItemLinks = <>
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Category = 0
      Visible = ivAlways
    end
    object dxBarGroup1: TdxBarGroup
      Items = ()
    end
  end
  object ActionList1: TActionList
    Images = dmR.SmallImage
    Left = 456
    Top = 144
    object acRefresh: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100' (F5)'
      Hint = #1054#1073#1085#1086#1074#1080#1090#1100' (F5)'
      ImageIndex = 5
      ShortCut = 116
      OnExecute = acRefreshExecute
    end
    object acClose: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1047#1072#1082#1088#1099#1090#1100
      Hint = #1047#1072#1082#1088#1099#1090#1100
      ImageIndex = 100
      OnExecute = acCloseExecute
    end
    object acToReply: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1048#1079#1074#1083#1077#1095#1100' '#1080#1079' '#1072#1088#1093#1080#1074#1072' '#1074#1099#1076#1077#1083#1077#1085#1085#1099#1077
      ImageIndex = 72
      OnExecute = acToReplyExecute
    end
  end
  object quReplyARHList: TFDQuery
    Connection = dmR.FDConnection
    SQL.Strings = (
      
        'SELECT FSRARID,ID,IDREC,ReplyId,ReplyPath,ReplyFile,IACTIVE,IDEL' +
        ',DateReply'
      'FROM dbo.REPLYLISTARH'
      'WHERE DateReply>=:DATEB'
      'and DateReply<:DATEE'
      'and FSRARID=:RARID')
    Left = 248
    Top = 208
    ParamData = <
      item
        Name = 'DATEB'
        DataType = ftDate
        ParamType = ptInput
        Value = 42370d
      end
      item
        Name = 'DATEE'
        DataType = ftDate
        ParamType = ptInput
        Value = 42522d
      end
      item
        Name = 'RARID'
        DataType = ftString
        ParamType = ptInput
        Value = '111122222'
      end>
    object quReplyARHListID: TLargeintField
      AutoGenerateValue = arAutoInc
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object quReplyARHListIDREC: TLargeintField
      FieldName = 'IDREC'
      Origin = 'IDREC'
    end
    object quReplyARHListReplyId: TStringField
      FieldName = 'ReplyId'
      Origin = 'ReplyId'
      Size = 100
    end
    object quReplyARHListReplyPath: TStringField
      FieldName = 'ReplyPath'
      Origin = 'ReplyPath'
      Size = 500
    end
    object quReplyARHListReplyFile: TMemoField
      FieldName = 'ReplyFile'
      Origin = 'ReplyFile'
      BlobType = ftMemo
      Size = 2147483647
    end
    object quReplyARHListIACTIVE: TSmallintField
      FieldName = 'IACTIVE'
      Origin = 'IACTIVE'
    end
    object quReplyARHListIDEL: TSmallintField
      FieldName = 'IDEL'
      Origin = 'IDEL'
    end
    object quReplyARHListDateReply: TSQLTimeStampField
      FieldName = 'DateReply'
      Origin = 'DateReply'
    end
    object quReplyARHListFSRARID: TStringField
      FieldName = 'FSRARID'
      Origin = 'FSRARID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 50
    end
  end
  object dsquReplyARHList: TDataSource
    DataSet = quReplyARHList
    Left = 240
    Top = 272
  end
  object PopupMenu1: TPopupMenu
    Images = dmR.SmallImage
    Left = 584
    Top = 256
    object N1: TMenuItem
      Action = acToReply
    end
  end
end
