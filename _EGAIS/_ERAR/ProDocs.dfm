object fmProDoc: TfmProDoc
  Left = 0
  Top = 0
  Caption = #1044#1086#1082#1091#1084#1077#1085#1090#1099' '#1055#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1072
  ClientHeight = 563
  ClientWidth = 1222
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1222
    Height = 127
    ApplicationButton.Visible = False
    BarManager = dxBarManager1
    Style = rs2010
    ColorSchemeAccent = rcsaOrange
    ColorSchemeName = 'Blue'
    SupportNonClientDrawing = True
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1044#1086#1082#1091#1084#1077#1085#1090#1099' '#1055#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1072
      Groups = <
        item
          ToolbarName = 'bmApplyDate'
        end
        item
          ToolbarName = 'bmClose'
        end>
      Index = 0
    end
  end
  object GridProDoc: TcxGrid
    Left = 0
    Top = 127
    Width = 1222
    Height = 436
    Align = alClient
    TabOrder = 5
    LevelTabs.Style = 8
    LookAndFeel.Kind = lfOffice11
    LookAndFeel.NativeStyle = False
    RootLevelOptions.DetailTabsPosition = dtpTop
    ExplicitTop = 102
    ExplicitHeight = 461
    object ViewProDoc: TcxGridDBTableView
      PopupMenu = pmProDocsIn
      OnDblClick = ViewProDocDblClick
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.InfoPanel.Visible = True
      Navigator.Visible = True
      DataController.DataSource = dsquProDocsIn_HD
      DataController.KeyFieldNames = 'ID'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      Images = dmR.SmallImage
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.MultiSelect = True
      OptionsView.ExpandButtonsForEmptyDetails = False
      OptionsView.GroupByBox = False
      object ViewProDocIDSKL: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1052#1061
        DataBinding.FieldName = 'IDSKL'
        Width = 41
      end
      object ViewProDocNAMEMH: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'NAMEMH'
        Width = 183
      end
      object ViewProDocIDATEDOC: TcxGridDBColumn
        DataBinding.FieldName = 'IDATEDOC'
        Visible = False
        VisibleForCustomization = False
        VisibleForEditForm = bFalse
      end
      object ViewProDocID: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
        DataBinding.FieldName = 'ID'
        Width = 85
      end
      object ViewProDocDATEDOC: TcxGridDBColumn
        Caption = #1044#1072#1090#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
        DataBinding.FieldName = 'DATEDOC'
        Width = 111
      end
      object ViewProDocNUMDOC: TcxGridDBColumn
        Caption = #8470' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
        DataBinding.FieldName = 'NUMDOC'
      end
      object ViewProDocIDCLI: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
        DataBinding.FieldName = 'IDCLI'
      end
      object ViewProDocNAMECL: TcxGridDBColumn
        Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
        DataBinding.FieldName = 'NAMECL'
        Width = 201
      end
      object ViewProDocNUMSF: TcxGridDBColumn
        Caption = #1057#1063#1060#1050' '#1085#1086#1084#1077#1088
        DataBinding.FieldName = 'NUMSF'
        Width = 64
      end
      object ViewProDocDATESF: TcxGridDBColumn
        Caption = #1057#1063#1060#1050' '#1076#1072#1090#1072
        DataBinding.FieldName = 'DATESF'
        Width = 100
      end
      object ViewProDocIACTIVE: TcxGridDBColumn
        Caption = #1057#1090#1072#1090#1091#1089' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
        DataBinding.FieldName = 'IACTIVE'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.Items = <
          item
            Description = #1042' '#1088#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1085#1080#1080
            ImageIndex = 0
            Value = 0
          end
          item
            Description = #1054#1087#1088#1080#1093#1086#1076#1086#1074#1072#1085
            Value = 1
          end>
        Width = 106
      end
    end
    object ViewProSpec: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      DataController.DataSource = dsquProDocsIn_SP
      DataController.DetailKeyFieldNames = 'IDHEAD'
      DataController.MasterKeyFieldNames = 'ID'
      DataController.Options = [dcoAssignGroupingValues]
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'QUANT'
          Column = ViewProSpecQUANT
        end
        item
          Format = #1050#1086#1083'-'#1074#1086': ###'
          Kind = skCount
          FieldName = 'NUM'
          Column = ViewProSpecNUM
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.ExpandButtonsForEmptyDetails = False
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      object ViewProSpecIDHEAD: TcxGridDBColumn
        DataBinding.FieldName = 'IDHEAD'
        Visible = False
      end
      object ViewProSpecID: TcxGridDBColumn
        DataBinding.FieldName = 'ID'
        Visible = False
      end
      object ViewProSpecNUM: TcxGridDBColumn
        Caption = #8470' '#1087#1087
        DataBinding.FieldName = 'NUM'
      end
      object ViewProSpecIDCARD: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1082#1072#1088#1090#1086#1095#1082#1080
        DataBinding.FieldName = 'IDCARD'
      end
      object ViewProSpecNAME: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'NAME'
        Width = 220
      end
      object ViewProSpecQUANT: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086
        DataBinding.FieldName = 'QUANT'
        Styles.Content = dmR.stlBold
      end
      object ViewProSpecIDM: TcxGridDBColumn
        DataBinding.FieldName = 'IDM'
        Visible = False
      end
      object ViewProSpecNAMESHORT: TcxGridDBColumn
        Caption = #1077#1076'.'#1080#1079#1084'.'
        DataBinding.FieldName = 'NAMESHORT'
        Width = 150
      end
      object ViewProSpecKM: TcxGridDBColumn
        Caption = #1082#1086#1101#1092#1092'. '#1077#1076'. '#1080#1079#1084'.'
        DataBinding.FieldName = 'KM'
      end
      object ViewProSpecPRICEIN: TcxGridDBColumn
        Caption = #1062#1077#1085#1072' '#1079#1072#1082#1091#1087#1072' '#1089' '#1053#1044#1057
        DataBinding.FieldName = 'PRICEIN'
      end
      object ViewProSpecSUMIN: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1079#1072#1082#1091#1087#1072' '#1089' '#1053#1044#1057
        DataBinding.FieldName = 'SUMIN'
      end
      object ViewProSpecPRICEIN0: TcxGridDBColumn
        Caption = #1062#1077#1085#1072' '#1079#1072#1082#1091#1087#1072' '#1073#1077#1079' '#1053#1044#1057
        DataBinding.FieldName = 'PRICEIN0'
      end
      object ViewProSpecSUMIN0: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1079#1072#1082#1091#1087#1072' '#1073#1077#1079' '#1053#1044#1057
        DataBinding.FieldName = 'SUMIN0'
      end
    end
    object ViewRealDoc: TcxGridDBTableView
      PopupMenu = pmProDocsOut
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.InfoPanel.Visible = True
      Navigator.Visible = True
      DataController.DataSource = dsquProDocsOut_HD
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.MultiSelect = True
      OptionsView.ExpandButtonsForEmptyDetails = False
      OptionsView.GroupByBox = False
      object ViewRealDocIDSKL: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1052#1061
        DataBinding.FieldName = 'IDSKL'
        Width = 42
      end
      object ViewRealDocNAMEMH: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'NAMEMH'
        Width = 147
      end
      object ViewRealDocIDATEDOC: TcxGridDBColumn
        DataBinding.FieldName = 'IDATEDOC'
        Visible = False
        VisibleForCustomization = False
        VisibleForEditForm = bFalse
      end
      object ViewRealDocID: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
        DataBinding.FieldName = 'ID'
        Width = 85
      end
      object ViewRealDocDATEDOC: TcxGridDBColumn
        Caption = #1044#1072#1090#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
        DataBinding.FieldName = 'DATEDOC'
        Width = 111
      end
      object ViewRealDocWEEKNUM: TcxGridDBColumn
        Caption = #1053#1086#1084#1077#1088' '#1085#1077#1076#1077#1083#1080
        DataBinding.FieldName = 'WEEKNUM'
        Width = 64
      end
      object ViewRealDocNUMDOC: TcxGridDBColumn
        Caption = #8470' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
        DataBinding.FieldName = 'NUMDOC'
        Width = 104
      end
      object ViewRealDocIDCLI: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
        DataBinding.FieldName = 'IDCLI'
      end
      object ViewRealDocNAMECL: TcxGridDBColumn
        Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
        DataBinding.FieldName = 'NAMECL'
        Width = 201
      end
      object ViewRealDocNUMSF: TcxGridDBColumn
        Caption = #1057#1063#1060#1050' '#1085#1086#1084#1077#1088
        DataBinding.FieldName = 'NUMSF'
        Width = 64
      end
      object ViewRealDocDATESF: TcxGridDBColumn
        Caption = #1057#1063#1060#1050' '#1076#1072#1090#1072
        DataBinding.FieldName = 'DATESF'
        Width = 100
      end
      object ViewRealDocIACTIVE: TcxGridDBColumn
        Caption = #1057#1090#1072#1090#1091#1089' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
        DataBinding.FieldName = 'IACTIVE'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.Items = <
          item
            Description = #1042' '#1088#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1085#1080#1080
            ImageIndex = 0
            Value = 0
          end
          item
            Description = #1054#1087#1088#1080#1093#1086#1076#1086#1074#1072#1085
            Value = 1
          end>
        Width = 106
      end
      object ViewRealDocBZSTATUS: TcxGridDBColumn
        Caption = #1057#1090#1072#1090#1091#1089' '#1079#1072#1082#1072#1079#1072
        DataBinding.FieldName = 'BZSTATUS'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.Items = <
          item
            Description = #1057#1092#1086#1088#1084#1080#1088#1086#1074#1072#1085
            ImageIndex = 0
            Value = 0
          end
          item
            Description = #1055#1086#1076#1090#1074#1077#1088#1078#1076#1077#1085
            Value = 1
          end
          item
            Description = #1054#1090#1087#1088#1072#1074#1083#1077#1085
            Value = 2
          end
          item
            Description = #1055#1086#1083#1091#1095#1077#1085
            Value = 3
          end>
        Width = 106
      end
    end
    object ViewRealSpec: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      DataController.DataSource = dsquProDocsOut_SP
      DataController.DetailKeyFieldNames = 'IDHEAD'
      DataController.MasterKeyFieldNames = 'ID'
      DataController.Options = [dcoAssignGroupingValues]
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'QUANT'
          Column = ViewRealSpecQUANT
        end
        item
          Format = #1050#1086#1083'-'#1074#1086': ###'
          Kind = skCount
          FieldName = 'NUM'
          Column = ViewRealSpecNUM
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.ExpandButtonsForEmptyDetails = False
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      object ViewRealSpecIDHEAD: TcxGridDBColumn
        DataBinding.FieldName = 'IDHEAD'
        Visible = False
      end
      object ViewRealSpecID: TcxGridDBColumn
        DataBinding.FieldName = 'ID'
        Visible = False
      end
      object ViewRealSpecNUM: TcxGridDBColumn
        Caption = #8470' '#1087#1087
        DataBinding.FieldName = 'NUM'
        Width = 64
      end
      object ViewRealSpecIDCARD: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1082#1072#1088#1090#1086#1095#1082#1080
        DataBinding.FieldName = 'IDCARD'
      end
      object ViewRealSpecNAME: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'NAME'
        Width = 300
      end
      object ViewRealSpecQUANT: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086', '#1076#1077#1082#1072#1083#1080#1090#1088#1086#1074
        DataBinding.FieldName = 'QUANT'
        Width = 80
      end
      object ViewRealSpecIDM: TcxGridDBColumn
        DataBinding.FieldName = 'IDM'
        Visible = False
      end
      object ViewRealSpecNAMESHORT: TcxGridDBColumn
        Caption = #1077#1076'.'#1080#1079#1084'.'
        DataBinding.FieldName = 'NAMESHORT'
        Width = 150
      end
      object ViewRealSpecKM: TcxGridDBColumn
        Caption = #1082#1086#1101#1092#1092'. '#1077#1076'. '#1080#1079#1084'.'
        DataBinding.FieldName = 'KM'
      end
      object ViewRealSpecPRICER: TcxGridDBColumn
        Caption = #1062#1077#1085#1072' '#1087#1088#1086#1076#1072#1078#1080
        DataBinding.FieldName = 'PRICER'
      end
      object ViewRealSpecSUMR: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1087#1088#1086#1076#1072#1078#1080' '#1089' '#1053#1044#1057
        DataBinding.FieldName = 'SUMR'
      end
      object ViewRealSpecSumOut0: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1087#1088#1086#1076#1072#1078#1080' '#1073#1077#1079' '#1053#1044#1057
        DataBinding.FieldName = 'SumOut0'
      end
    end
    object LevelProDoc: TcxGridLevel
      Caption = #1042#1093#1086#1076#1103#1097#1080#1077' ('#1055#1088#1080#1093#1086#1076')'
      GridView = ViewProDoc
      Options.TabsForEmptyDetails = False
      object LevelProSpec: TcxGridLevel
        GridView = ViewProSpec
        Options.DetailTabsPosition = dtpLeft
        Options.TabsForEmptyDetails = False
      end
    end
    object LevelRealDoc: TcxGridLevel
      Caption = #1048#1089#1093#1086#1076#1103#1097#1080#1077' ('#1056#1077#1072#1083#1080#1079#1072#1094#1080#1103')'
      GridView = ViewRealDoc
      object LevelRealSpec: TcxGridLevel
        GridView = ViewRealSpec
      end
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      #1044#1077#1081#1089#1090#1074#1080#1103)
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmR.SmallImage
    ImageOptions.LargeImages = dmR.LargeImage
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 788
    Top = 52
    DockControlHeights = (
      0
      0
      0
      0)
    object bmApplyDate: TdxBar
      Caption = #1042#1099#1073#1086#1088' '#1087#1077#1088#1080#1086#1076#1072
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 778
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          UserDefine = [udWidth]
          UserWidth = 75
          Visible = True
          ItemName = 'deDateBeg'
        end
        item
          UserDefine = [udWidth]
          UserWidth = 74
          Visible = True
          ItemName = 'deDateEnd'
        end
        item
          Visible = True
          ItemName = 'btnDone'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object bmClose: TdxBar
      Caption = #1047#1072#1082#1088#1099#1090#1100
      CaptionButtons = <>
      DockedLeft = 198
      DockedTop = 0
      FloatLeft = 778
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'btnClose'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object bmD: TdxBar
      Caption = #1044#1077#1081#1089#1090#1074#1080#1103
      CaptionButtons = <>
      DockedDockingStyle = dsNone
      DockedLeft = 137
      DockedTop = 0
      DockingStyle = dsNone
      FloatLeft = 1318
      FloatTop = 255
      FloatClientWidth = 51
      FloatClientHeight = 24
      ItemLinks = <>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = False
      WholeRow = False
    end
    object btnDone: TdxBarLargeButton
      Action = acRefresh
      Category = 0
    end
    object btnClose: TdxBarLargeButton
      Action = acClose
      Category = 0
    end
    object deDateBeg: TdxBarDateCombo
      Caption = 'C  '
      Category = 0
      Hint = 'C  '
      Visible = ivAlways
      OnChange = deDateBegChange
      ImageIndex = 25
      ShowDayText = False
    end
    object deDateEnd: TdxBarDateCombo
      Caption = #1087#1086
      Category = 0
      Hint = #1087#1086
      Visible = ivAlways
      OnChange = deDateBegChange
      ImageIndex = 25
      ShowDayText = False
    end
    object cbItem1: TcxBarEditItem
      Caption = 'XMLNotePad'
      Category = 0
      Hint = 'XMLNotePad'
      Visible = ivAlways
      ShowCaption = True
      Width = 0
      PropertiesClassName = 'TcxCheckBoxProperties'
      Properties.ImmediatePost = True
      InternalEditValue = False
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = acCards
      Category = 0
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = 'New SubItem'
      Category = 0
      Visible = ivAlways
      ItemLinks = <>
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = acUTMExch
      Category = 0
    end
    object dxBarGroup1: TdxBarGroup
      Items = ()
    end
  end
  object ActionList1: TActionList
    Images = dmR.SmallImage
    Left = 560
    Top = 228
    object acRefresh: TAction
      Category = #1060#1086#1088#1084#1072
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100' (F5)'
      Hint = #1054#1073#1085#1086#1074#1080#1090#1100' (F5)'
      ImageIndex = 5
      ShortCut = 116
      OnExecute = acRefreshExecute
    end
    object acClose: TAction
      Category = #1060#1086#1088#1084#1072
      Caption = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 100
      OnExecute = acCloseExecute
    end
    object acSelectDate: TAction
      Category = #1060#1086#1088#1084#1072
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      Hint = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      ImageIndex = 1
      OnExecute = acSelectDateExecute
    end
    object acDecodeTovar: TAction
      Category = #1044#1077#1082#1086#1076#1080#1088#1086#1074#1072#1085#1080#1077
      Caption = #1044#1077#1082#1086#1076#1080#1088#1086#1074#1072#1090#1100' '#1082#1072#1082' '#1090#1086#1074#1072#1088
      ImageIndex = 38
    end
    object acSaveToFile: TAction
      Category = #1044#1077#1082#1086#1076#1080#1088#1086#1074#1072#1085#1080#1077
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1074' '#1092#1072#1081#1083
      ImageIndex = 7
    end
    object acDelList: TAction
      Category = #1044#1077#1082#1086#1076#1080#1088#1086#1074#1072#1085#1080#1077
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1074#1099#1076#1077#1083#1077#1085#1085#1099#1077' '#1089#1090#1088#1086#1082#1080
      ImageIndex = 4
      ShortCut = 16452
    end
    object acDecodeTTN: TAction
      Category = #1044#1077#1082#1086#1076#1080#1088#1086#1074#1072#1085#1080#1077
      Caption = #1044#1077#1082#1086#1076#1080#1088#1086#1074#1072#1090#1100' '#1082#1072#1082' '#1076#1086#1082#1091#1084#1077#1085#1090
      ImageIndex = 38
    end
    object acCards: TAction
      Category = #1060#1086#1088#1084#1072
      Caption = #1050#1072#1088#1090#1086#1095#1082#1080
      ImageIndex = 11
    end
    object acSendActR: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1057#1092#1086#1088#1084#1080#1088#1086#1074#1072#1090#1100' '#1072#1082#1090' '#1088#1072#1089#1093#1086#1078#1076#1077#1085#1080#1103
      ImageIndex = 49
    end
    object acDecodeWB: TAction
      Category = #1044#1077#1082#1086#1076#1080#1088#1086#1074#1072#1085#1080#1077
      Caption = 'acDecodeWB'
      ImageIndex = 38
    end
    object acUTMExch: TAction
      Category = #1060#1086#1088#1084#1072
      Caption = #1059#1058#1052' '#1086#1073#1084#1077#1085
      ImageIndex = 130
    end
    object acSendDocsOut: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1057#1092#1086#1088#1084#1080#1088#1086#1074#1072#1090#1100' '#1080#1089#1093#1086#1076#1103#1097#1080#1077' '#1085#1072#1082#1083#1072#1076#1085#1099#1077
      ImageIndex = 115
      OnExecute = acSendDocsOutExecute
    end
  end
  object quProDocsIn_HD: TFDQuery
    Connection = FDConnectPro
    SQL.Strings = (
      'declare @IDATEB int = :IDATEB'
      'declare @IDATEE int = :IDATEE'
      ''
      'SELECT hd.[IDSKL]'
      '      ,mh.NAMEMH '
      '      ,hd.[IDATEDOC]'
      '      ,hd.[ID]'
      '      ,hd.[DATEDOC]'
      '      ,hd.[NUMDOC]'
      '      ,hd.[DATESF]'
      '      ,hd.[NUMSF]'
      '      ,hd.[IDCLI]'
      #9'  ,cli.NAMECL'
      '      ,hd.[IACTIVE]'
      '  FROM [dbo].[OF_DOCIN_HD] hd'
      '  left join [dbo].[OF_MH] mh on mh.ID=hd.[IDSKL]'
      '  left join [dbo].[OF_CLIENTS] cli on cli.ID=hd.[IDCLI]'
      ''
      '  where hd.[IDATEDOC]>=@IDATEB'
      '  and hd.[IDATEDOC]<=@IDATEE'
      '  and hd.[IACTIVE]=1'
      '  and cli.EGAIS=1')
    Left = 64
    Top = 344
    ParamData = <
      item
        Name = 'IDATEB'
        DataType = ftInteger
        ParamType = ptInput
        Value = 42300
      end
      item
        Name = 'IDATEE'
        DataType = ftInteger
        ParamType = ptInput
        Value = 42400
      end>
    object quProDocsIn_HDIDSKL: TIntegerField
      FieldName = 'IDSKL'
      Origin = 'IDSKL'
      Required = True
    end
    object quProDocsIn_HDNAMEMH: TStringField
      FieldName = 'NAMEMH'
      Origin = 'NAMEMH'
      Size = 100
    end
    object quProDocsIn_HDIDATEDOC: TIntegerField
      FieldName = 'IDATEDOC'
      Origin = 'IDATEDOC'
      Required = True
    end
    object quProDocsIn_HDID: TLargeintField
      FieldName = 'ID'
      Origin = 'ID'
      Required = True
    end
    object quProDocsIn_HDDATEDOC: TSQLTimeStampField
      FieldName = 'DATEDOC'
      Origin = 'DATEDOC'
    end
    object quProDocsIn_HDNUMDOC: TStringField
      FieldName = 'NUMDOC'
      Origin = 'NUMDOC'
      Size = 15
    end
    object quProDocsIn_HDDATESF: TSQLTimeStampField
      FieldName = 'DATESF'
      Origin = 'DATESF'
    end
    object quProDocsIn_HDNUMSF: TStringField
      FieldName = 'NUMSF'
      Origin = 'NUMSF'
      Size = 15
    end
    object quProDocsIn_HDIDCLI: TIntegerField
      FieldName = 'IDCLI'
      Origin = 'IDCLI'
    end
    object quProDocsIn_HDNAMECL: TStringField
      FieldName = 'NAMECL'
      Origin = 'NAMECL'
      Size = 200
    end
    object quProDocsIn_HDIACTIVE: TSmallintField
      FieldName = 'IACTIVE'
      Origin = 'IACTIVE'
    end
  end
  object dsquProDocsIn_HD: TDataSource
    DataSet = quProDocsIn_HD
    Left = 64
    Top = 408
  end
  object pmProDocsIn: TPopupMenu
    Images = dmR.SmallImage
    Left = 656
    Top = 248
  end
  object quProDocsIn_SP: TFDQuery
    Connection = FDConnectPro
    SQL.Strings = (
      'declare @IDATEB int = :IDATEB'
      'declare @IDATEE int = :IDATEE'
      ''
      'SELECT sp.[IDHEAD]'
      '      ,sp.[ID]'
      '      ,sp.[NUM]'
      '      ,sp.[IDCARD]'
      #9'  ,ca.NAME'
      '      ,sp.[QUANT]'
      '      ,sp.[IDM]'
      #9'  ,me.NAMESHORT'
      '      ,sp.[KM]'
      '      ,sp.[PRICEIN]'
      '      ,sp.[SUMIN]'
      '      ,sp.[PRICEIN0]'
      '      ,sp.[SUMIN0]'
      '  FROM [dbo].[OF_DOCIN_SP] sp'
      '  left join [dbo].[OF_DOCIN_HD] hd on hd.ID=sp.[IDHEAD]'
      '  left join [dbo].[OF_CLIENTS] cli on cli.ID=hd.[IDCLI]'
      '  left join [dbo].[OF_CARDS] ca on ca.ID=sp.[IDCARD]'
      '  left join [dbo].[OF_MESSUR] me on me.ID=sp.[IDM]'
      ''
      '  where hd.[IDATEDOC]>=@IDATEB'
      '  and hd.[IDATEDOC]<=@IDATEE'
      '  and hd.[IACTIVE]=1'
      '  and cli.EGAIS=1')
    Left = 160
    Top = 344
    ParamData = <
      item
        Name = 'IDATEB'
        DataType = ftInteger
        ParamType = ptInput
        Value = 42300
      end
      item
        Name = 'IDATEE'
        DataType = ftInteger
        ParamType = ptInput
        Value = 42400
      end>
    object quProDocsIn_SPIDHEAD: TLargeintField
      FieldName = 'IDHEAD'
      Origin = 'IDHEAD'
      Required = True
    end
    object quProDocsIn_SPID: TLargeintField
      AutoGenerateValue = arAutoInc
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere]
      ReadOnly = True
    end
    object quProDocsIn_SPNUM: TIntegerField
      FieldName = 'NUM'
      Origin = 'NUM'
    end
    object quProDocsIn_SPIDCARD: TIntegerField
      FieldName = 'IDCARD'
      Origin = 'IDCARD'
    end
    object quProDocsIn_SPNAME: TStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      Size = 200
    end
    object quProDocsIn_SPQUANT: TFloatField
      FieldName = 'QUANT'
      Origin = 'QUANT'
      DisplayFormat = '0.000'
    end
    object quProDocsIn_SPIDM: TIntegerField
      FieldName = 'IDM'
      Origin = 'IDM'
    end
    object quProDocsIn_SPNAMESHORT: TStringField
      FieldName = 'NAMESHORT'
      Origin = 'NAMESHORT'
      Size = 50
    end
    object quProDocsIn_SPKM: TSingleField
      FieldName = 'KM'
      Origin = 'KM'
    end
    object quProDocsIn_SPPRICEIN: TFloatField
      FieldName = 'PRICEIN'
      Origin = 'PRICEIN'
      DisplayFormat = '0.00'
    end
    object quProDocsIn_SPSUMIN: TFloatField
      FieldName = 'SUMIN'
      Origin = 'SUMIN'
      DisplayFormat = '0.00'
    end
    object quProDocsIn_SPPRICEIN0: TFloatField
      FieldName = 'PRICEIN0'
      Origin = 'PRICEIN0'
      DisplayFormat = '0.00'
    end
    object quProDocsIn_SPSUMIN0: TFloatField
      FieldName = 'SUMIN0'
      Origin = 'SUMIN0'
      DisplayFormat = '0.00'
    end
  end
  object dsquProDocsIn_SP: TDataSource
    DataSet = quProDocsIn_SP
    Left = 160
    Top = 408
  end
  object FDConnectPro: TFDConnection
    Params.Strings = (
      'Server=192.168.0.76'
      'User_Name=sa'
      'Password=314159'
      'ApplicationName=RarEx'
      'Workstation=SYS-4'
      'Database=PROCB'
      'MARS=yes'
      'DriverID=MSSQL')
    FetchOptions.AssignedValues = [evMode, evRowsetSize]
    FetchOptions.Mode = fmAll
    FetchOptions.RowsetSize = 10000
    ResourceOptions.AssignedValues = [rvServerOutput, rvAutoReconnect]
    ResourceOptions.ServerOutput = True
    ResourceOptions.AutoReconnect = True
    ConnectedStoredUsage = []
    LoginPrompt = False
    Transaction = FDTrans
    UpdateTransaction = FDTransUpdate
    Left = 36
    Top = 204
  end
  object FDTrans: TFDTransaction
    Connection = FDConnectPro
    Left = 108
    Top = 204
  end
  object FDTransUpdate: TFDTransaction
    Connection = FDConnectPro
    Left = 180
    Top = 204
  end
  object FDPhysMSSQLDriverLink: TFDPhysMSSQLDriverLink
    Left = 292
    Top = 204
  end
  object FDGUIxWaitCursor: TFDGUIxWaitCursor
    Provider = 'Forms'
    Left = 416
    Top = 204
  end
  object dsquProDocsOut_HD: TDataSource
    DataSet = quProDocsOut_HD
    Left = 288
    Top = 408
  end
  object quProDocsOut_HD: TFDQuery
    Connection = FDConnectPro
    SQL.Strings = (
      'DECLARE @IDATEB int = :IDATEB'
      'declare @IDATEE int = :IDATEE'
      ''
      'SELECT hd.[IDSKL]'
      '      ,mh.NAMEMH '
      '      ,hd.[IDATEDOC]'
      '      ,hd.[ID]'
      '      ,hd.[DATEDOC]'
      '      ,DATENAME(ISO_WEEK, hd.[DATEDOC]) AS WEEKNUM'
      '      ,hd.[NUMDOC]'
      '      ,hd.[DATESF]'
      '      ,hd.[NUMSF]'
      '      ,hd.[IDCLI]'
      '      ,cli.NAMECL'
      '      ,hd.[IACTIVE]  '
      '      ,hd.BZSTATUS'
      ''
      '  FROM [dbo].[OF_DOCOUTR_HD] hd'
      '  left join [dbo].[OF_MH] mh on mh.ID=hd.[IDSKL]'
      '  left join [dbo].[OF_CLIENTS] cli on cli.ID=hd.[IDCLI]'
      '  where hd.[IDATEDOC] BETWEEN @IDATEB and @IDATEE'
      '  AND hd.IDFROM=375 --'#1060#1077#1093#1091
      '  AND hd.[IACTIVE]=1 --'#1054#1087#1088#1080#1093#1086#1076#1086#1074#1072#1085
      '  AND hd.BZSTATUS=3 --'#1057#1090#1072#1090#1091#1089' '#1079#1072#1082#1072#1079#1072' '#1087#1086#1083#1091#1095#1077#1085
      
        '  AND hd.ID IN (SELECT DISTINCT sp1.[IDHEAD]      --'#1058#1086#1083#1100#1082#1086' '#1079#1072#1082#1072#1079 +
        #1099' '#1089' '#1087#1080#1074#1086#1084
      '                  FROM [dbo].[OF_DOCOUTR_SP] sp1'
      
        '                  left join [dbo].[OF_DOCOUTR_HD] hd1 on hd1.ID=' +
        'sp1.[IDHEAD]'
      '                where hd1.[IDATEDOC] BETWEEN @IDATEB and @IDATEE'
      '                  AND hd1.IDFROM=375 --'#1060#1077#1093#1091
      '                  AND hd1.[IACTIVE]=1 --'#1054#1087#1088#1080#1093#1086#1076#1086#1074#1072#1085
      '                  AND hd.BZSTATUS=3 --'#1057#1090#1072#1090#1091#1089' '#1079#1072#1082#1072#1079#1072' '#1087#1086#1083#1091#1095#1077#1085
      '                  AND sp1.[QUANT]<>0'
      
        '                  AND sp1.[IDCARD] IN (7305, 7306,7307,7308)) --' +
        #1058#1086#1083#1100#1082#1086' '#1087#1080#1074#1086
      'ORDER BY '
      '       hd.[DATEDOC], hd.[ID]'
      '')
    Left = 288
    Top = 344
    ParamData = <
      item
        Name = 'IDATEB'
        DataType = ftInteger
        ParamType = ptInput
        Value = 42300
      end
      item
        Name = 'IDATEE'
        DataType = ftInteger
        ParamType = ptInput
        Value = 42400
      end>
    object quProDocsOut_HDIDSKL: TIntegerField
      FieldName = 'IDSKL'
      Origin = 'IDSKL'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quProDocsOut_HDNAMEMH: TStringField
      FieldName = 'NAMEMH'
      Origin = 'NAMEMH'
      Size = 100
    end
    object quProDocsOut_HDIDATEDOC: TIntegerField
      FieldName = 'IDATEDOC'
      Origin = 'IDATEDOC'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quProDocsOut_HDID: TLargeintField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quProDocsOut_HDDATEDOC: TSQLTimeStampField
      FieldName = 'DATEDOC'
      Origin = 'DATEDOC'
    end
    object quProDocsOut_HDWEEKNUM: TWideStringField
      FieldName = 'WEEKNUM'
      Origin = 'WEEKNUM'
      ReadOnly = True
      Size = 30
    end
    object quProDocsOut_HDNUMDOC: TStringField
      FieldName = 'NUMDOC'
      Origin = 'NUMDOC'
    end
    object quProDocsOut_HDDATESF: TSQLTimeStampField
      FieldName = 'DATESF'
      Origin = 'DATESF'
    end
    object quProDocsOut_HDNUMSF: TStringField
      FieldName = 'NUMSF'
      Origin = 'NUMSF'
    end
    object quProDocsOut_HDIDCLI: TIntegerField
      FieldName = 'IDCLI'
      Origin = 'IDCLI'
    end
    object quProDocsOut_HDNAMECL: TStringField
      FieldName = 'NAMECL'
      Origin = 'NAMECL'
      Size = 200
    end
    object quProDocsOut_HDIACTIVE: TSmallintField
      FieldName = 'IACTIVE'
      Origin = 'IACTIVE'
    end
    object quProDocsOut_HDBZSTATUS: TSmallintField
      FieldName = 'BZSTATUS'
      Origin = 'BZSTATUS'
    end
  end
  object pmProDocsOut: TPopupMenu
    Images = dmR.SmallImage
    Left = 744
    Top = 248
    object N1: TMenuItem
      Action = acSendDocsOut
    end
  end
  object quProDocsOut_SP: TFDQuery
    Connection = FDConnectPro
    SQL.Strings = (
      'declare @IDATEB int = :IDATEB'
      'declare @IDATEE int = :IDATEE'
      ''
      'SELECT sp.[IDHEAD]'
      '      ,sp.[ID]'
      
        '      ,ROW_NUMBER() OVER(PARTITION BY sp.[IDHEAD] ORDER BY sp.[I' +
        'D]) AS NUM'
      '      ,sp.[IDCARD]'
      '      ,ca.NAME'
      '      ,cast(sp.[QUANT] as real)/10 as QUANT'
      '      --,sp.[IDM]'
      '      ,116 as IDM'
      '      --,me.NAMESHORT'
      '      ,'#39#1076#1082#1083#39' AS NAMESHORT'
      '      ,sp.[KM]'
      '      ,sp.[PRICER]*10 as PRICER'
      '      ,sp.[SumOut0]*10 as SumOut0'
      '      ,sp.[SUMR]*10 as SUMR'
      '      ,ca.CODEZAK'
      '  FROM [dbo].[OF_DOCOUTR_SP] sp'
      '  left join [dbo].[OF_DOCOUTR_HD] hd on hd.ID=sp.[IDHEAD]'
      '  left join [dbo].[OF_CLIENTS] cli on cli.ID=hd.[IDCLI]'
      '  left join [dbo].[OF_CARDS] ca on ca.ID=sp.[IDCARD]'
      '  left join [dbo].[OF_MESSUR] me on me.ID=sp.[IDM]'
      ''
      '  where hd.[IDATEDOC] BETWEEN @IDATEB and @IDATEE'
      '  AND hd.IDFROM=375 --'#1060#1077#1093#1091
      '  AND hd.[IACTIVE]=1 --'#1054#1087#1088#1080#1093#1086#1076#1086#1074#1072#1085
      '  AND hd.BZSTATUS=3 --'#1057#1090#1072#1090#1091#1089' '#1079#1072#1082#1072#1079#1072' '#1087#1086#1083#1091#1095#1077#1085
      '  AND sp.[QUANT]<>0'
      '  AND sp.[IDCARD] IN (7305, 7306,7307,7308)  --'#1058#1086#1083#1100#1082#1086' '#1087#1080#1074#1086)
    Left = 400
    Top = 344
    ParamData = <
      item
        Name = 'IDATEB'
        DataType = ftInteger
        ParamType = ptInput
        Value = 42300
      end
      item
        Name = 'IDATEE'
        DataType = ftInteger
        ParamType = ptInput
        Value = 42400
      end>
    object quProDocsOut_SPIDHEAD: TLargeintField
      FieldName = 'IDHEAD'
      Origin = 'IDHEAD'
      Required = True
    end
    object quProDocsOut_SPID: TLargeintField
      AutoGenerateValue = arAutoInc
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere]
      ReadOnly = True
    end
    object quProDocsOut_SPNUM: TLargeintField
      FieldName = 'NUM'
      Origin = 'NUM'
      ReadOnly = True
    end
    object quProDocsOut_SPIDCARD: TIntegerField
      FieldName = 'IDCARD'
      Origin = 'IDCARD'
    end
    object quProDocsOut_SPNAME: TStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      Size = 200
    end
    object quProDocsOut_SPQUANT: TSingleField
      FieldName = 'QUANT'
      Origin = 'QUANT'
      DisplayFormat = '0.00'
    end
    object quProDocsOut_SPIDM: TIntegerField
      FieldName = 'IDM'
      Origin = 'IDM'
    end
    object quProDocsOut_SPNAMESHORT: TStringField
      FieldName = 'NAMESHORT'
      Origin = 'NAMESHORT'
      Size = 50
    end
    object quProDocsOut_SPKM: TFloatField
      FieldName = 'KM'
      Origin = 'KM'
    end
    object quProDocsOut_SPPRICER: TSingleField
      FieldName = 'PRICER'
      Origin = 'PRICER'
      ReadOnly = True
      DisplayFormat = '0.00'
    end
    object quProDocsOut_SPSumOut0: TFloatField
      FieldName = 'SumOut0'
      Origin = 'SumOut0'
      ReadOnly = True
      DisplayFormat = '0.00'
    end
    object quProDocsOut_SPSUMR: TFloatField
      FieldName = 'SUMR'
      Origin = 'SUMR'
      ReadOnly = True
      DisplayFormat = '0.00'
    end
    object quProDocsOut_SPCODEZAK: TIntegerField
      FieldName = 'CODEZAK'
      Origin = 'CODEZAK'
    end
  end
  object dsquProDocsOut_SP: TDataSource
    DataSet = quProDocsOut_SP
    Left = 400
    Top = 408
  end
  object quProDocsOut_SP_ByIDH: TFDQuery
    Connection = FDConnectPro
    SQL.Strings = (
      'declare @IDHEAD BIGINT = :IDH'
      ''
      'SELECT sp.[IDHEAD]'
      '      ,sp.[ID]'
      
        '      ,ROW_NUMBER() OVER(PARTITION BY sp.[IDHEAD] ORDER BY sp.[I' +
        'D]) AS NUM'
      '      ,sp.[IDCARD]'
      '      ,ca.NAME'
      '      ,cast(sp.[QUANT] as real)/10 as QUANT'
      '      --,sp.[IDM]'
      '      ,116 as IDM'
      '      --,me.NAMESHORT'
      '      ,'#39#1076#1082#1083#39' AS NAMESHORT'
      '      ,sp.[KM]'
      '      ,sp.[PRICER]*10 as PRICER'
      '      ,sp.[SumOut0]*10 as SumOut0'
      '      ,sp.[SUMR]*10 as SUMR'
      '      ,ca.CODEZAK'
      
        '--      ,(SELECT TOP 1 Id FROM ECrystalR.dbo.EgaisGoods WHERE Id' +
        'Goods=ca.CODEZAK ORDER BY Id Desc) as AlcCode'
      '  FROM [dbo].[OF_DOCOUTR_SP] sp'
      '  left join [dbo].[OF_DOCOUTR_HD] hd on hd.ID=sp.[IDHEAD]'
      '  left join [dbo].[OF_CLIENTS] cli on cli.ID=hd.[IDCLI]'
      '  left join [dbo].[OF_CARDS] ca on ca.ID=sp.[IDCARD]'
      '  left join [dbo].[OF_MESSUR] me on me.ID=sp.[IDM]'
      ''
      '  where sp.IDHEAD=@IDHEAD'
      '  AND hd.IDFROM=375 --'#1060#1077#1093#1091
      '  AND hd.[IACTIVE]=1 --'#1054#1087#1088#1080#1093#1086#1076#1086#1074#1072#1085
      '  AND hd.BZSTATUS=3 --'#1057#1090#1072#1090#1091#1089' '#1079#1072#1082#1072#1079#1072' '#1087#1086#1083#1091#1095#1077#1085
      '  AND sp.[QUANT]<>0'
      '  AND sp.[IDCARD] IN (7305, 7306,7307,7308)  --'#1058#1086#1083#1100#1082#1086' '#1087#1080#1074#1086)
    Left = 472
    Top = 360
    ParamData = <
      item
        Name = 'IDH'
        DataType = ftLargeint
        ParamType = ptInput
        Value = Null
      end>
    object quProDocsOut_SP_ByIDHIDHEAD: TLargeintField
      FieldName = 'IDHEAD'
      Origin = 'IDHEAD'
      Required = True
    end
    object quProDocsOut_SP_ByIDHID: TLargeintField
      AutoGenerateValue = arAutoInc
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere]
      ReadOnly = True
    end
    object quProDocsOut_SP_ByIDHNUM: TLargeintField
      FieldName = 'NUM'
      Origin = 'NUM'
      ReadOnly = True
    end
    object quProDocsOut_SP_ByIDHIDCARD: TIntegerField
      FieldName = 'IDCARD'
      Origin = 'IDCARD'
    end
    object quProDocsOut_SP_ByIDHNAME: TStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      Size = 200
    end
    object quProDocsOut_SP_ByIDHQUANT: TSingleField
      FieldName = 'QUANT'
      Origin = 'QUANT'
      ReadOnly = True
    end
    object quProDocsOut_SP_ByIDHIDM: TIntegerField
      FieldName = 'IDM'
      Origin = 'IDM'
      ReadOnly = True
      Required = True
    end
    object quProDocsOut_SP_ByIDHNAMESHORT: TStringField
      FieldName = 'NAMESHORT'
      Origin = 'NAMESHORT'
      ReadOnly = True
      Required = True
      Size = 3
    end
    object quProDocsOut_SP_ByIDHKM: TFloatField
      FieldName = 'KM'
      Origin = 'KM'
    end
    object quProDocsOut_SP_ByIDHPRICER: TSingleField
      FieldName = 'PRICER'
      Origin = 'PRICER'
      ReadOnly = True
    end
    object quProDocsOut_SP_ByIDHSumOut0: TFloatField
      FieldName = 'SumOut0'
      Origin = 'SumOut0'
      ReadOnly = True
    end
    object quProDocsOut_SP_ByIDHSUMR: TFloatField
      FieldName = 'SUMR'
      Origin = 'SUMR'
      ReadOnly = True
    end
    object quProDocsOut_SP_ByIDHCODEZAK: TIntegerField
      FieldName = 'CODEZAK'
      Origin = 'CODEZAK'
    end
  end
end
