unit ViewSpecVn;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  CustomChildForm, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer,
  cxTextEdit, cxCalendar, cxCheckBox, cxDBLookupComboBox, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error,
  FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Vcl.Menus, cxClasses, FireDAC.Comp.Client,
  FireDAC.Comp.DataSet, System.Actions, Vcl.ActnList, dxBar, cxBarEditItem, cxMemo, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  System.StrUtils,
  cxGridDBTableView, cxGridCustomView, cxGrid, dxRibbon, cxCalc;

type
  TfmDocSpecVn = class(TfmCustomChildForm)
    dxBarManager1: TdxBarManager;
    dxBarGroup1: TdxBarGroup;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    dxBarManager1Bar1: TdxBar;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    ActionList1: TActionList;
    acClose: TAction;
    acSave: TAction;
    quSpecVn: TFDQuery;
    UpdSQL1: TFDUpdateSQL;
    dsquSpecVn: TDataSource;
    ViewSpecVn: TcxGridDBTableView;
    LevelSpec: TcxGridLevel;
    GridSpec: TcxGrid;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    PopupMenu1: TPopupMenu;
    cxStyle2: TcxStyle;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    cxBarEditItem2: TcxBarEditItem;
    quE: TFDQuery;
    acTestSpec: TAction;
    dxBarLargeButton4: TdxBarLargeButton;
    acDelPos: TAction;
    N1: TMenuItem;
    dxBarManager1Bar2: TdxBar;
    quSpecVnFSRARID: TStringField;
    quSpecVnIDATE: TIntegerField;
    quSpecVnSIDHD: TStringField;
    quSpecVnID: TLargeintField;
    quSpecVnNUM: TIntegerField;
    quSpecVnALCCODE: TStringField;
    quSpecVnQUANT: TFloatField;
    quSpecVnINFO_B: TStringField;
    quSpecVnNAME: TMemoField;
    quSpecVnAVID: TIntegerField;
    quSpecVnVOL: TSingleField;
    quSpecVnKREP: TSingleField;
    ViewSpecVnNUM: TcxGridDBColumn;
    ViewSpecVnALCCODE: TcxGridDBColumn;
    ViewSpecVnQUANT: TcxGridDBColumn;
    ViewSpecVnINFO_B: TcxGridDBColumn;
    ViewSpecVnNAME: TcxGridDBColumn;
    ViewSpecVnAVID: TcxGridDBColumn;
    ViewSpecVnVOL: TcxGridDBColumn;
    ViewSpecVnKREP: TcxGridDBColumn;
    acCreateFromShop: TAction;
    N2: TMenuItem;
    N3: TMenuItem;
    procedure acCloseExecute(Sender: TObject);
    procedure acSaveExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acDelPosExecute(Sender: TObject);
    procedure acCreateFromShopExecute(Sender: TObject);
  private
    { Private declarations }
    FRARID:string;
    FIDATE: Integer;
    FSID:string;
    FIEDIT: Integer;
    FNUMBER:string;
    FSIDIN:string;

    procedure Init;

  public
    { Public declarations }
  end;

// var
//  fmDocSpec: TfmDocSpec;

procedure ShowSpecViewVn(RARID,SID,FNUMBER:string;IDATE,IEDIT:Integer;SIDIN:String);

implementation

{$R *.dfm}

uses ViewRar, dmRar, EgaisDecode;

procedure ShowSpecViewVn(RARID,SID,FNUMBER:string;IDATE,IEDIT:Integer;SIDIN:String);
var F:TfmDocSpecVn;
begin
  F:=TfmDocSpecVn.Create(Application);
//  ShowMessage(Xmlstr);
  F.FRARID:=RARID;
  F.FIDATE:=IDATE;
  F.FSID:=SID;
  F.FIEDIT:=IEDIT;

  F.FNUMBER:=FNUMBER;
  F.FSIDIN:=Trim(SIDIN);

  F.Init;
  F.Show;
end;

procedure TfmDocSpecVn.acCloseExecute(Sender: TObject);
begin
//  quSpecIn.Active:=False;
  Close;
end;

procedure TfmDocSpecVn.acCreateFromShopExecute(Sender: TObject);
var Rec:TcxCustomGridRecord;
    i,j,iC,iDate:Integer;
    SKAP,SPR_B:string;
    SID: string;
    a: TGUID;
    iNUM:Integer;
    rQuant:Real;

begin
  //������������ �������� ����������� �� �����
  if ViewSpecVn.Controller.SelectedRecordCount>0 then
  begin
    ClearMessageLogLocal;
    ShowMessageLogLocal('����� ... ���� ������������ ���������.');

    CreateGUID(a);
    SID:=GUIDTostring(a);
    SID:=AnsiReplaceStr(SID,'{','');
    SID:=AnsiReplaceStr(SID,'}','');

    iDate:=Trunc(Date);
    iNum:=fGetId(4);

    with dmR do
    begin
      //��������� ���������
      try
        quS.Active:=False;
        quS.SQL.Clear;
        quS.SQL.Add('');
        quS.SQL.Add('INSERT INTO [dbo].[ADOCSVN_HD] ([FSRARID],[IDATE],[SID],[NUMBER],[SDATE],[STYPE],[IACTIVE],[READYSEND],[WBREGID],[FIXNUMBER],[FIXDATE],[TICK1],[TICK2],[SIDIN])');
        quS.SQL.Add('VALUES ('''+CommonSet.FSRAR_ID+''','+its(iDate)+','''+SID+''','+its(iNum)+',getdate(),''FromShop'',0,1,'''','''','''',0,0,'''')');
        quS.ExecSQL;

        ShowMessageLogLocal('  ����������� ��������� ��������� ('+SID+')');

        iC:=1;
        for i:=0 to ViewSpecVn.Controller.SelectedRecordCount-1 do
        begin
          Rec:=ViewSpecVn.Controller.SelectedRecords[i];

          SKAP:=''; SPR_B:=''; rQuant:=0;

          for j:=0 to Rec.ValueCount-1 do
          begin
            if ViewSpecVn.Columns[j].Name='ViewSpecVnALCCODE' then begin SKAP:=Rec.Values[j]; end;
            if ViewSpecVn.Columns[j].Name='ViewSpecVnINFO_B' then begin SPR_B:=Rec.Values[j]; end;
            if ViewSpecVn.Columns[j].Name='ViewSpecVnQUANT' then begin rQuant:=Rec.Values[j]; end;
          end;

          if (SKAP>'')and(SPR_B>'') then
          begin
            quS.Active:=False;
            quS.SQL.Clear;
            quS.SQL.Add('');
            quS.SQL.Add('INSERT INTO [dbo].[ADOCSVN_SP] ([FSRARID],[IDATE],[SIDHD],[NUM],[ALCCODE],[QUANT],[INFO_B])');
            quS.SQL.Add('VALUES ('''+CommonSet.FSRAR_ID+''','+its(iDate)+','''+SID+''','+its(iC)+','''+SKAP+''','+fts(rQuant)+','''+SPR_B+''')');
            quS.ExecSQL;

            Inc(iC);
          end;
        end;

        Init;
      except
      end;

      ShowMessageLogLocal('  ���������� '+its(iC)+' �����.');
    end;

    ShowMessageLogLocal('������� ��������.');
  end;
end;

procedure TfmDocSpecVn.acDelPosExecute(Sender: TObject);
begin
  if FIEDIT=1 then
  begin
    quSpecVn.Delete;
  end;
end;

procedure TfmDocSpecVn.acSaveExecute(Sender: TObject);
var iErrors:Integer;
begin
  //��������� ������������
  quSpecVn.UpdateTransaction.StartTransaction;
  iErrors := quSpecVn.ApplyUpdates;
  if iErrors = 0 then begin
    try
      quSpecVn.CommitUpdates;
      quSpecVn.UpdateTransaction.Commit;

      quE.Active:=False;
      quE.SQL.Clear;
      quE.SQL.Add('UPDATE [dbo].[ADOCSVN_HD]');
      quE.SQL.Add('Set');
      quE.SQL.Add('[IDATE] ='+its(Trunc(cxBarEditItem2.EditValue)));
      quE.SQL.Add(',[NUMBER]='''+Trim(cxBarEditItem1.EditValue)+'''');
      quE.SQL.Add('WHERE');
      quE.SQL.Add('[FSRARID] = '''+FRARID+'''');
      quE.SQL.Add('and [IDATE] ='+its(FIDATE));
      quE.SQL.Add('and [SID] ='''+FSID+'''');
      quE.ExecSQL;

      dmR.quDocsVnHd.Refresh;
    except
      ShowMessage('������ ����������..');
    end;
  end else
  begin
    ShowMessage('������ ����������.');
    quSpecVn.UpdateTransaction.Rollback;
  end;
end;

procedure TfmDocSpecVn.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
end;

procedure TfmDocSpecVn.Init;
begin
  Caption:='������������ ����������� ����� ����������  '+ds1(FIDATE)+', '+FNUMBER+', '+FSID+' ( ��. ���. '+ FSIDIN+')';

  cxBarEditItem1.EditValue:=FNUMBER;
  cxBarEditItem2.EditValue:=FIDATE;

  ViewSpecVn.BeginUpdate;
  quSpecVn.Active:=False;

  quSpecVn.ParamByName('RARID').AsString:=FRARID;
  quSpecVn.ParamByName('IDATE').AsInteger:=FIDATE;
  quSpecVn.ParamByName('SID').AsString:=FSID;

  quSpecVn.Active:=True;
  ViewSpecVn.EndUpdate;

  if FIEDIT=1 then
  begin
    acSave.Enabled:=True;
    ViewSpecVnQUANT.Options.Editing:=True;
    cxBarEditItem1.Properties.ReadOnly:=False;
    cxBarEditItem2.Properties.ReadOnly:=False;
  end else
  begin
    acSave.Enabled:=False;
    ViewSpecVnQUANT.Options.Editing:=False;
    cxBarEditItem1.Properties.ReadOnly:=True;
    cxBarEditItem2.Properties.ReadOnly:=True;
  end;
end;

end.
