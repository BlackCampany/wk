object fmQBarCodeS: TfmQBarCodeS
  Left = 0
  Top = 0
  Caption = #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1103' '#1079#1072#1087#1088#1086#1089#1072' '#1085#1077#1095#1080#1090#1072#1077#1084#1099#1093' '#1096#1090#1088#1080#1093#1082#1086#1076#1086#1074
  ClientHeight = 570
  ClientWidth = 1030
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object grQBarCodeS: TcxGrid
    Left = 0
    Top = 127
    Width = 1030
    Height = 361
    Align = alClient
    TabOrder = 0
    LookAndFeel.Kind = lfOffice11
    LookAndFeel.NativeStyle = False
    object vwQBarCodeS: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.InfoPanel.Visible = True
      Navigator.Visible = True
      DataController.DataSource = dsDocSpec
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
      OptionsView.GroupByBox = False
      object vwQBarCodeSIDH: TcxGridDBColumn
        DataBinding.FieldName = 'IDH'
        Visible = False
        VisibleForCustomization = False
        VisibleForEditForm = bFalse
      end
      object vwQBarCodeSID: TcxGridDBColumn
        DataBinding.FieldName = 'ID'
        Visible = False
        VisibleForCustomization = False
        VisibleForEditForm = bFalse
      end
      object vwQBarCodeSIDENT: TcxGridDBColumn
        Caption = #1053#1086#1084#1077#1088' '#1087#1086' '#1087#1086#1088#1103#1076#1082#1091
        DataBinding.FieldName = 'IDENT'
        Width = 112
      end
      object vwQBarCodeSTYPE: TcxGridDBColumn
        DataBinding.FieldName = 'TYPE'
      end
      object vwQBarCodeSRANK: TcxGridDBColumn
        DataBinding.FieldName = 'RANK'
      end
      object vwQBarCodeSNUMBER: TcxGridDBColumn
        DataBinding.FieldName = 'NUMBER'
      end
      object vwQBarCodeSBARCODE: TcxGridDBColumn
        DataBinding.FieldName = 'BARCODE'
        Options.Editing = False
        Width = 586
      end
    end
    object lvQBarCodeS: TcxGridLevel
      GridView = vwQBarCodeS
    end
  end
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1030
    Height = 127
    BarManager = dxBarManager1
    Style = rs2010
    ColorSchemeAccent = rcsaOrange
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 4
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1103' '#1079#1072#1087#1088#1086#1089#1072' '#1085#1077#1095#1080#1090#1072#1077#1084#1099#1093' '#1096#1090#1088#1080#1093#1082#1086#1076#1086#1074
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar3'
        end
        item
          ToolbarName = 'dxBarManager1Bar4'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end>
      Index = 0
    end
  end
  object PanelRejected: TPanel
    Left = 0
    Top = 488
    Width = 1030
    Height = 82
    Align = alBottom
    BevelOuter = bvNone
    ParentBackground = False
    TabOrder = 6
    object Label1: TLabel
      AlignWithMargins = True
      Left = 3
      Top = 3
      Width = 1024
      Height = 13
      Align = alTop
      Caption = #1050#1086#1084#1084#1077#1085#1090#1072#1088#1080#1081' '#1086#1090#1082#1072#1079#1072':'
      ExplicitWidth = 109
    end
    object cxDBMemo1: TcxDBMemo
      Left = 0
      Top = 19
      Align = alClient
      DataBinding.DataField = 'REJCOMMENT'
      DataBinding.DataSource = dsDocHead
      Properties.ReadOnly = True
      Properties.ScrollBars = ssVertical
      TabOrder = 0
      Height = 63
      Width = 1030
    end
  end
  object dsDocSpec: TDataSource
    DataSet = quDocSpec
    Left = 256
    Top = 304
  end
  object ActionManager1: TActionManager
    LargeImages = dmR.LargeImage
    Images = dmR.SmallImage
    Left = 168
    Top = 212
    StyleName = 'Platform Default'
    object acAdd: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1089#1090#1088#1086#1082#1091
      ImageIndex = 75
      OnExecute = acAddExecute
    end
    object acDelete: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1089#1090#1088#1086#1082#1091
      ImageIndex = 74
      OnExecute = acDeleteExecute
    end
    object acSaveDoc: TAction
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' Ctrl+S'
      ImageIndex = 7
      OnExecute = acSaveDocExecute
    end
    object acImport: TAction
      Caption = #1048#1084#1087#1086#1088#1090' '#1080#1079' Excel'
      ImageIndex = 77
      OnExecute = acImportExecute
    end
    object acClose: TAction
      Caption = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 100
      OnExecute = acCloseExecute
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      #1044#1077#1081#1089#1090#1074#1080#1103)
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmR.SmallImage
    ImageOptions.LargeImages = dmR.LargeImage
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 260
    Top = 212
    DockControlHeights = (
      0
      0
      0
      0)
    object dxBarManager1Bar2: TdxBar
      Caption = #1047#1072#1082#1088#1099#1090#1100
      CaptionButtons = <>
      DockedLeft = 508
      DockedTop = 0
      FloatLeft = 1064
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1059#1087#1088#1072#1074#1083#1077#1085#1080#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 1064
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          UserDefine = [udWidth]
          UserWidth = 134
          Visible = True
          ItemName = 'beiPoint'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1044#1077#1081#1089#1090#1074#1080#1103
      CaptionButtons = <>
      DockedLeft = 219
      DockedTop = 0
      FloatLeft = 1064
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton4'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = acClose
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = acSaveDoc
      Category = 0
    end
    object beiPoint: TcxBarEditItem
      Caption = #1058#1086#1095#1082#1072' '#1088#1077#1072#1083'.'
      Category = 0
      Hint = #1058#1086#1095#1082#1072' '#1088#1077#1072#1083'.'
      Visible = ivAlways
      PropertiesClassName = 'TcxLookupComboBoxProperties'
      Properties.DropDownListStyle = lsFixedList
      Properties.DropDownRows = 40
      Properties.DropDownSizeable = True
      Properties.DropDownWidth = 350
      Properties.KeyFieldNames = 'RARID'
      Properties.ListColumns = <
        item
          FieldName = 'Name'
        end
        item
          FieldName = 'ISHOP'
        end
        item
          FieldName = 'RARID'
        end>
      Properties.ListSource = dmR.dsquPoint
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = acAdd
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = acDelete
      Category = 0
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = acImport
      Category = 0
    end
    object dxBarGroup1: TdxBarGroup
      Items = ()
    end
  end
  object quDocSpec: TFDQuery
    OnNewRecord = quDocSpecNewRecord
    CachedUpdates = True
    BeforeApplyUpdates = quDocSpecBeforeApplyUpdates
    Connection = dmR.FDConnection
    Transaction = dmR.FDTrans
    UpdateTransaction = dmR.FDTransUpdate
    SQL.Strings = (
      'SELECT'
      '  IDH'
      ' ,ID'
      ' ,IDENT'
      ' ,TYPE'
      ' ,RANK'
      ' ,NUMBER'
      ' ,BARCODE'
      'FROM dbo.QBARCODE_SP'
      'WHERE '
      '  IDH=:IDH')
    Left = 168
    Top = 304
    ParamData = <
      item
        Name = 'IDH'
        DataType = ftInteger
        ParamType = ptInput
        Value = 1
      end>
    object quDocSpecIDH: TIntegerField
      FieldName = 'IDH'
      Origin = 'IDH'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quDocSpecID: TFDAutoIncField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object quDocSpecIDENT: TIntegerField
      FieldName = 'IDENT'
      Origin = 'IDENT'
    end
    object quDocSpecTYPE: TStringField
      FieldName = 'TYPE'
      Origin = 'TYPE'
      Size = 10
    end
    object quDocSpecRANK: TStringField
      FieldName = 'RANK'
      Origin = 'RANK'
      Size = 10
    end
    object quDocSpecNUMBER: TStringField
      FieldName = 'NUMBER'
      Origin = 'NUMBER'
    end
    object quDocSpecBARCODE: TStringField
      FieldName = 'BARCODE'
      Origin = 'BARCODE'
      Size = 250
    end
  end
  object quDocHead: TFDQuery
    CachedUpdates = True
    Connection = dmR.FDConnection
    Transaction = dmR.FDTrans
    UpdateTransaction = dmR.FDTransUpdate
    SQL.Strings = (
      'SELECT'
      '  ID'
      ' ,FSRARID'
      ' ,IDATE'
      ' ,INUMBER'
      ' ,ISTATUS'
      ' ,IDTORAR'
      ' ,REJCOMMENT'
      'FROM dbo.QBARCODE_HD'
      'WHERE ID=:IDH'
      '')
    Left = 168
    Top = 368
    ParamData = <
      item
        Name = 'IDH'
        DataType = ftInteger
        ParamType = ptInput
        Value = 0
      end>
    object quDocHeadID: TFDAutoIncField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object quDocHeadFSRARID: TStringField
      FieldName = 'FSRARID'
      Origin = 'FSRARID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 50
    end
    object quDocHeadIDATE: TIntegerField
      FieldName = 'IDATE'
      Origin = 'IDATE'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quDocHeadINUMBER: TIntegerField
      FieldName = 'INUMBER'
      Origin = 'INUMBER'
      Required = True
    end
    object quDocHeadISTATUS: TIntegerField
      FieldName = 'ISTATUS'
      Origin = 'ISTATUS'
      Required = True
    end
    object quDocHeadIDTORAR: TLargeintField
      FieldName = 'IDTORAR'
      Origin = 'IDTORAR'
    end
    object quDocHeadREJCOMMENT: TMemoField
      FieldName = 'REJCOMMENT'
      Origin = 'REJCOMMENT'
      BlobType = ftMemo
      Size = 2147483647
    end
  end
  object OpenDialog1: TOpenDialog
    Left = 404
    Top = 212
  end
  object dsDocHead: TDataSource
    DataSet = quDocHead
    Left = 256
    Top = 368
  end
end
