object fmShowXML: TfmShowXML
  Left = 0
  Top = 0
  Caption = 'XML'
  ClientHeight = 508
  ClientWidth = 891
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TcxMemo
    Left = 0
    Top = 127
    Align = alClient
    Lines.Strings = (
      'Memo1')
    PopupMenu = PopupMenu1
    Properties.ScrollBars = ssBoth
    Properties.WordWrap = False
    TabOrder = 0
    ExplicitTop = 0
    ExplicitHeight = 508
    Height = 381
    Width = 891
  end
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 891
    Height = 127
    BarManager = dxBarManager1
    Style = rs2010
    ColorSchemeAccent = rcsaOrange
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 5
    TabStop = False
    ExplicitLeft = -236
    ExplicitWidth = 1127
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = 'XML'
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar1'
        end>
      Index = 0
    end
  end
  object PopupMenu1: TPopupMenu
    Images = dmR.SmallImage
    Left = 56
    Top = 208
    object xml1: TMenuItem
      Action = acSaveToFile
    end
  end
  object ActionManager1: TActionManager
    LargeImages = dmR.LargeImage
    Images = dmR.SmallImage
    Left = 168
    Top = 208
    StyleName = 'Platform Default'
    object acSaveToFile: TAction
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1074' '#1092#1072#1081#1083' (*.xml)'
      ImageIndex = 7
      OnExecute = acSaveToFileExecute
    end
    object acClose: TAction
      Caption = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 100
      OnExecute = acCloseExecute
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      #1044#1077#1081#1089#1090#1074#1080#1103)
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmR.SmallImage
    ImageOptions.LargeImages = dmR.LargeImage
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 260
    Top = 212
    DockControlHeights = (
      0
      0
      0
      0)
    object dxBarManager1Bar1: TdxBar
      Caption = #1047#1072#1082#1088#1099#1090#1100
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 925
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = acClose
      Category = 0
    end
    object dxBarGroup1: TdxBarGroup
      Items = ()
    end
  end
end
