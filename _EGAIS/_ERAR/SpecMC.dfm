object fmSpecMC: TfmSpecMC
  Left = 0
  Top = 0
  Caption = #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1103' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
  ClientHeight = 512
  ClientWidth = 953
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object GridSpecMC: TcxGrid
    Left = 0
    Top = 127
    Width = 953
    Height = 385
    Align = alClient
    TabOrder = 0
    LookAndFeel.Kind = lfOffice11
    LookAndFeel.NativeStyle = False
    object ViewSpecMC: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.InfoPanel.Visible = True
      Navigator.Visible = True
      DataController.DataSource = dsquSpecMC
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'Kol'
          Column = ViewSpecMCKol
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.MultiSelect = True
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      object ViewSpecMCID: TcxGridDBColumn
        Caption = #8470' '#1087#1087
        DataBinding.FieldName = 'ID'
      end
      object ViewSpecMCCodeTovar: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1090#1086#1074#1072#1088#1072
        DataBinding.FieldName = 'CodeTovar'
        Styles.Content = dmR.stlBold
      end
      object ViewSpecMCCodeEdIzm: TcxGridDBColumn
        Caption = #1077#1076'.'#1080#1079#1084'.'
        DataBinding.FieldName = 'CodeEdIzm'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Items = <
          item
            Description = #1096#1090'.'
            ImageIndex = 0
            Value = 1
          end
          item
            Description = #1082#1075'.'
            Value = 2
          end>
      end
      object ViewSpecMCName: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'Name'
      end
      object ViewSpecMCKol: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086
        DataBinding.FieldName = 'Kol'
        Styles.Content = dmR.stlLightBlue
      end
      object ViewSpecMCCenaTovar: TcxGridDBColumn
        Caption = #1062#1077#1085#1072' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
        DataBinding.FieldName = 'CenaTovar'
      end
      object ViewSpecMCSumCenaTovarPost: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
        DataBinding.FieldName = 'SumCenaTovarPost'
      end
      object ViewSpecMCNewCenaTovar: TcxGridDBColumn
        Caption = #1062#1077#1085#1072' '#1084#1072#1075#1072#1079#1080#1085#1072
        DataBinding.FieldName = 'NewCenaTovar'
      end
      object ViewSpecMCSumCenaTovar: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1084#1072#1075#1072#1079#1080#1085#1072
        DataBinding.FieldName = 'SumCenaTovar'
      end
      object ViewSpecMCBarCode: TcxGridDBColumn
        Caption = #1064#1050
        DataBinding.FieldName = 'BarCode'
      end
      object ViewSpecMCFullName: TcxGridDBColumn
        Caption = #1055#1086#1083#1085#1086#1077' '#1085#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'FullName'
      end
    end
    object LevelSpecMC: TcxGridLevel
      GridView = ViewSpecMC
    end
  end
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 953
    Height = 127
    BarManager = dxBarManager1
    Style = rs2010
    ColorSchemeAccent = rcsaOrange
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 5
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1103' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar1'
        end>
      Index = 0
    end
  end
  object quSpecMC: TFDQuery
    Connection = dmR.FDConnection
    SQL.Strings = (
      'SELECT sp.[IdH]'
      '      ,sp.[ID]'
      '      ,row_number() OVER (ORDER BY sp.[IdH],sp.[ID]) '#39'IDSP'#39
      '      ,sp.[CodeTovar]'
      #9'  ,ca.Name'
      '      ,sp.[CodeEdIzm]'
      '      ,sp.[Kol]'
      '      ,sp.[CenaTovar]'
      '      ,sp.[SumCenaTovarPost]'
      '      ,sp.[NewCenaTovar]'
      '      ,sp.[SumCenaTovar]'
      #9'  ,ca.FullName'
      '      ,sp.[BarCode]'
      '  FROM [scrystal].[dbo].[A_TTNInLn] sp'
      '  left join [EcrystalR].[dbo].[Goods] ca on ca.Id=sp.[CodeTovar]'
      '  where sp.[IdH]=:IDH'
      '  order by sp.[ID]')
    Left = 192
    Top = 336
    ParamData = <
      item
        Name = 'IDH'
        DataType = ftString
        ParamType = ptInput
        Value = '168727'
      end>
    object IntegerField1: TIntegerField
      FieldName = 'IdH'
      Origin = 'IdH'
      Required = True
    end
    object IntegerField2: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      Required = True
    end
    object LargeintField1: TLargeintField
      FieldName = 'IDSP'
      Origin = 'IDSP'
      ReadOnly = True
    end
    object IntegerField3: TIntegerField
      FieldName = 'CodeTovar'
      Origin = 'CodeTovar'
    end
    object SmallintField1: TSmallintField
      FieldName = 'CodeEdIzm'
      Origin = 'CodeEdIzm'
    end
    object FloatField1: TFloatField
      FieldName = 'Kol'
      Origin = 'Kol'
      DisplayFormat = '0.000'
    end
    object FloatField2: TFloatField
      FieldName = 'CenaTovar'
      Origin = 'CenaTovar'
      DisplayFormat = '0.00'
    end
    object FloatField3: TFloatField
      FieldName = 'SumCenaTovarPost'
      Origin = 'SumCenaTovarPost'
      DisplayFormat = '0.00'
    end
    object FloatField4: TFloatField
      FieldName = 'NewCenaTovar'
      Origin = 'NewCenaTovar'
      DisplayFormat = '0.00'
    end
    object FloatField5: TFloatField
      FieldName = 'SumCenaTovar'
      Origin = 'SumCenaTovar'
      DisplayFormat = '0.00'
    end
    object StringField1: TStringField
      FieldName = 'BarCode'
      Origin = 'BarCode'
      Size = 13
    end
    object quSpecMCName: TStringField
      FieldName = 'Name'
      Origin = 'Name'
      Size = 30
    end
    object quSpecMCFullName: TStringField
      FieldName = 'FullName'
      Origin = 'FullName'
      Size = 60
    end
  end
  object dsquSpecMC: TDataSource
    DataSet = quSpecMC
    Left = 192
    Top = 400
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      #1044#1077#1081#1089#1090#1074#1080#1103)
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmR.SmallImage
    ImageOptions.LargeImages = dmR.LargeImage
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 260
    Top = 212
    DockControlHeights = (
      0
      0
      0
      0)
    object dxBarManager1Bar1: TdxBar
      Caption = #1047#1072#1082#1088#1099#1090#1100
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 925
      FloatTop = 8
      FloatClientWidth = 92
      FloatClientHeight = 54
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = acClose
      Category = 0
    end
    object dxBarGroup1: TdxBarGroup
      Items = ()
    end
  end
  object ActionManager1: TActionManager
    LargeImages = dmR.LargeImage
    Images = dmR.SmallImage
    Left = 168
    Top = 208
    StyleName = 'Platform Default'
    object acClose: TAction
      Caption = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 100
      OnExecute = acCloseExecute
    end
  end
end
