object fmDocCash: TfmDocCash
  Left = 0
  Top = 0
  Caption = #1055#1088#1086#1076#1072#1078#1080' '#1087#1086' '#1082#1072#1089#1089#1077
  ClientHeight = 558
  ClientWidth = 1525
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1525
    Height = 127
    ApplicationButton.Visible = False
    BarManager = dxBarManager1
    Style = rs2010
    ColorSchemeAccent = rcsaOrange
    ColorSchemeName = 'Blue'
    SupportNonClientDrawing = True
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1055#1088#1086#1076#1072#1078#1080' '#1087#1086' '#1082#1072#1089#1089#1077
      Groups = <
        item
          ToolbarName = 'bmApplyDate'
        end
        item
          ToolbarName = 'bmClose'
        end>
      Index = 0
    end
  end
  object GridDocCash: TcxGrid
    Left = 0
    Top = 127
    Width = 1525
    Height = 431
    Align = alClient
    TabOrder = 5
    LevelTabs.Style = 8
    LookAndFeel.Kind = lfOffice11
    LookAndFeel.NativeStyle = False
    object ViewDocCash: TcxGridDBTableView
      PopupMenu = pmDocVn
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.First.Visible = False
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.InfoPanel.Visible = True
      Navigator.Visible = True
      DataController.DataSource = dsquCashSail
      DataController.Summary.DefaultGroupSummaryItems = <
        item
          Format = '0.000'
          Kind = skSum
          Position = spFooter
          FieldName = 'QUANT'
          Column = ViewDocCashQUANT
          DisplayText = #1050#1086#1083'-'#1074#1086
        end
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'SUMR'
          Column = ViewDocCashSUMR
          DisplayText = #1057#1091#1084#1084#1072
        end
        item
          Format = '0.000'
          Kind = skSum
          Position = spFooter
          FieldName = 'VOL'
          Column = ViewDocCashVOL
          DisplayText = #1054#1073#1098#1077#1084
        end>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'QUANT'
          Column = ViewDocCashQUANT
          DisplayText = #1050#1086#1083'-'#1074#1086
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SUMR'
          Column = ViewDocCashSUMR
          DisplayText = #1057#1091#1084#1084#1072
        end
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'VOL'
          Column = ViewDocCashVOL
          DisplayText = #1054#1073#1098#1077#1084
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.MultiSelect = True
      OptionsView.Footer = True
      OptionsView.GroupFooters = gfAlwaysVisible
      object ViewDocCashFSRARID: TcxGridDBColumn
        Caption = 'RARID'
        DataBinding.FieldName = 'FSRARID'
        Width = 75
      end
      object ViewDocCashIDATE: TcxGridDBColumn
        Caption = #1044#1072#1090#1072' '#1094#1077#1083#1086#1077
        DataBinding.FieldName = 'IDATE'
        Visible = False
        Width = 53
      end
      object ViewDocCashZDATE: TcxGridDBColumn
        Caption = #1044#1072#1090#1072
        DataBinding.FieldName = 'ZDATE'
        Width = 107
      end
      object ViewDocCashISHOP: TcxGridDBColumn
        Caption = #1052#1072#1075#1072#1079#1080#1085' '#8470
        DataBinding.FieldName = 'ISHOP'
      end
      object ViewDocCashCASHNUM: TcxGridDBColumn
        Caption = #1050#1072#1089#1089#1072' '#8470
        DataBinding.FieldName = 'CASHNUM'
      end
      object ViewDocCashZNUM: TcxGridDBColumn
        Caption = #1057#1084#1077#1085#1072
        DataBinding.FieldName = 'ZNUM'
      end
      object ViewDocCashCHNUM: TcxGridDBColumn
        Caption = #1063#1077#1082' '#8470
        DataBinding.FieldName = 'CHNUM'
      end
      object ViewDocCashPOSID: TcxGridDBColumn
        Caption = #8470' '#1074' '#1095#1077#1082#1077
        DataBinding.FieldName = 'POSID'
      end
      object ViewDocCashID: TcxGridDBColumn
        DataBinding.FieldName = 'ID'
        Visible = False
      end
      object ViewDocCashDEPART: TcxGridDBColumn
        Caption = #1054#1090#1076#1077#1083
        DataBinding.FieldName = 'DEPART'
      end
      object ViewDocCashICODE: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1090#1086#1074#1072#1088#1072
        DataBinding.FieldName = 'ICODE'
      end
      object ViewDocCashBARCODE: TcxGridDBColumn
        Caption = #1064#1090#1088#1080#1093#1082#1086#1076
        DataBinding.FieldName = 'BARCODE'
      end
      object ViewDocCashKAP: TcxGridDBColumn
        Caption = #1050#1040#1055
        DataBinding.FieldName = 'KAP'
        Width = 157
      end
      object ViewDocCashCHDATE: TcxGridDBColumn
        Caption = #1044#1072#1090#1072' '#1095#1077#1082#1072
        DataBinding.FieldName = 'CHDATE'
        Width = 111
      end
      object ViewDocCashNAMEC: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'NAMEC'
        Width = 205
      end
      object ViewDocCashAVID: TcxGridDBColumn
        Caption = #1042#1080#1076' '#1040#1055
        DataBinding.FieldName = 'AVID'
      end
      object ViewDocCashKREP: TcxGridDBColumn
        Caption = #1050#1088#1077#1087#1086#1089#1090#1100
        DataBinding.FieldName = 'KREP'
      end
      object ViewDocCashVOL: TcxGridDBColumn
        Caption = #1054#1073#1098#1077#1084
        DataBinding.FieldName = 'VOL'
      end
      object ViewDocCashQUANT: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086
        DataBinding.FieldName = 'QUANT'
      end
      object ViewDocCashPRICER: TcxGridDBColumn
        Caption = #1062#1077#1085#1072
        DataBinding.FieldName = 'PRICER'
      end
      object ViewDocCashSUMR: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072
        DataBinding.FieldName = 'SUMR'
      end
      object ViewDocCashPRODID: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1087#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1103
        DataBinding.FieldName = 'PRODID'
        Width = 113
      end
      object ViewDocCashIMPORTERID: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1080#1084#1087#1086#1088#1090#1077#1088#1072
        DataBinding.FieldName = 'IMPORTERID'
        Visible = False
      end
      object ViewDocCashNAMECLI: TcxGridDBColumn
        Caption = #1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1100
        DataBinding.FieldName = 'NAMECLI'
      end
      object ViewDocCashCLIENTINN: TcxGridDBColumn
        Caption = #1048#1053#1053
        DataBinding.FieldName = 'CLIENTINN'
      end
      object ViewDocCashCLIENTKPP: TcxGridDBColumn
        Caption = #1050#1055#1055
        DataBinding.FieldName = 'CLIENTKPP'
      end
      object ViewDocCashAMARK: TcxGridDBColumn
        Caption = #1040#1082#1094#1080#1079#1085#1072#1103' '#1084#1072#1088#1082#1072
        DataBinding.FieldName = 'AMARK'
        Width = 246
      end
      object ViewDocCashCLITYPE: TcxGridDBColumn
        Caption = #1058#1080#1087' '#1087#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1103
        DataBinding.FieldName = 'CLITYPE'
      end
    end
    object LevelDocCash: TcxGridLevel
      Caption = #1053#1077#1086#1073#1088#1072#1073#1086#1090#1072#1085#1085#1099#1077' '#1058#1058#1053
      GridView = ViewDocCash
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      #1044#1077#1081#1089#1090#1074#1080#1103)
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmR.SmallImage
    ImageOptions.LargeImages = dmR.LargeImage
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 708
    Top = 204
    DockControlHeights = (
      0
      0
      0
      0)
    object bmApplyDate: TdxBar
      Caption = #1044#1077#1081#1089#1090#1074#1080#1103
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 778
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          UserDefine = [udWidth]
          UserWidth = 75
          Visible = True
          ItemName = 'deDateBeg'
        end
        item
          UserDefine = [udWidth]
          UserWidth = 74
          Visible = True
          ItemName = 'deDateEnd'
        end
        item
          Visible = True
          ItemName = 'btnDone'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object bmClose: TdxBar
      Caption = #1047#1072#1082#1088#1099#1090#1100
      CaptionButtons = <>
      DockedLeft = 282
      DockedTop = 0
      FloatLeft = 778
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'btnClose'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object deDateBeg: TdxBarDateCombo
      Caption = 'C  '
      Category = 0
      Hint = 'C  '
      Visible = ivAlways
      OnChange = deDateBegChange
      ImageIndex = 25
      ShowDayText = False
    end
    object deDateEnd: TdxBarDateCombo
      Caption = #1087#1086
      Category = 0
      Hint = #1087#1086
      Visible = ivAlways
      OnChange = deDateBegChange
      ImageIndex = 25
      ShowDayText = False
    end
    object btnDone: TdxBarLargeButton
      Action = acRefresh
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Caption = #1059#1058#1052' '#1086#1073#1084#1077#1085
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 38
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Caption = #1059#1058#1052' '#1080#1089#1090#1086#1088#1080#1103
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 130
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Caption = #1059#1058#1052' '#1072#1088#1093#1080#1074
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 21
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = #1048#1053#1053' '
      Category = 0
      Hint = #1048#1053#1053' '
      Visible = ivAlways
      PropertiesClassName = 'TcxTextEditProperties'
      BarStyleDropDownButton = False
      Properties.AutoSelect = False
      Properties.IncrementalSearch = False
      Properties.ValidationOptions = [evoAllowLoseFocus]
      InternalEditValue = ''
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Caption = #1047#1072#1087#1088#1086#1089' '#1072#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090#1072' '#1087#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1103
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 16
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Caption = #1056#1072#1079#1088#1077#1096#1080#1090#1100' '#1086#1090#1087#1088#1072#1074#1082#1091'  '
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 1
    end
    object beiPoint: TcxBarEditItem
      Caption = #1058#1086#1095#1082#1072' '#1088#1077#1072#1083'.'
      Category = 0
      Hint = #1058#1086#1095#1082#1072' '#1088#1077#1072#1083'.'
      Visible = ivAlways
      PropertiesClassName = 'TcxLookupComboBoxProperties'
      Properties.DropDownListStyle = lsFixedList
      Properties.DropDownRows = 40
      Properties.DropDownSizeable = True
      Properties.DropDownWidth = 350
      Properties.KeyFieldNames = 'ISHOP'
      Properties.ListColumns = <
        item
          Caption = #1052#1072#1075#1072#1079#1080#1085
          FieldName = 'Name'
        end
        item
          Caption = #1052#1072#1075'.'#8470
          FieldName = 'ISHOP'
        end
        item
          FieldName = 'RARID'
        end>
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Caption = #1052#1050#1088#1080#1089#1090#1072#1083
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 161
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Caption = #1055#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1086
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 162
    end
    object beiGetTTN: TcxBarEditItem
      Caption = #8470' TTN'
      Category = 0
      Hint = #8470' TTN'
      Visible = ivAlways
      PropertiesClassName = 'TcxTextEditProperties'
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1047#1072#1087#1088#1086#1089#1080#1090#1100' '#1076#1086#1082#1091#1084#1077#1085#1090' '#1074' '#1045#1043#1040#1048#1057
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 50
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = #1047#1072#1087#1088#1086#1089#1080#1090#1100' '#1085#1077#1086#1073#1088#1072#1073#1086#1090#1072#1085#1085#1099#1077' '#1058#1058#1053
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 164
    end
    object btnClose: TdxBarLargeButton
      Action = acClose
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Caption = #1047#1072#1087#1088#1086#1089' '#1089#1087#1088#1072#1074#1082#1080' 1'
      Category = 0
      Visible = ivNever
      LargeImageIndex = 164
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Caption = #1047#1072#1087#1088#1086#1089' '#1086#1089#1090#1072#1090#1082#1086#1074
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 164
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Caption = #1047#1072#1087#1088#1086#1089' '#1085#1077#1095#1080#1090#1072#1077#1084#1099#1093' '#1096#1090#1088#1080#1093#1082#1086#1076#1086#1074
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 34
      ShortCut = 49218
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Caption = #1044#1086#1082#1091#1084#1077#1085#1090#1099' '#1062#1041
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 167
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = #1047#1072#1087#1088#1086#1089#1080#1090#1100' '#1085#1077#1086#1073#1088#1072#1073#1086#1090#1072#1085#1085#1099#1077' '#1058#1058#1053
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 164
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = acGetCashSail
      Category = 0
    end
    object dxBarGroup1: TdxBarGroup
      Items = ()
    end
  end
  object ActionList1: TActionList
    Images = dmR.SmallImage
    Left = 560
    Top = 228
    object acRefresh: TAction
      Category = #1060#1086#1088#1084#1072
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100' (F5)'
      Hint = #1054#1073#1085#1086#1074#1080#1090#1100' (F5)'
      ImageIndex = 5
      ShortCut = 116
      OnExecute = acRefreshExecute
    end
    object acClose: TAction
      Category = #1060#1086#1088#1084#1072
      Caption = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 100
      OnExecute = acCloseExecute
    end
    object acSelectDate: TAction
      Category = #1060#1086#1088#1084#1072
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      Hint = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      ImageIndex = 1
      OnExecute = acSelectDateExecute
    end
    object acExpExcel: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1069#1082#1089#1087#1086#1088#1090' '#1074' Excel'
      ImageIndex = 76
      OnExecute = acExpExcelExecute
    end
    object acGetCashSail: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1055#1088#1080#1085#1103#1090#1100' '#1088#1077#1072#1083#1080#1079#1072#1094#1080#1102
      ImageIndex = 95
      OnExecute = acGetCashSailExecute
    end
    object acDelDocVn: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1076#1086#1082#1091#1084#1077#1085#1090
      ImageIndex = 100
    end
  end
  object pmDocVn: TPopupMenu
    Images = dmR.SmallImage
    Left = 488
    Top = 304
    object N1: TMenuItem
      Action = acGetCashSail
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Excel1: TMenuItem
      Action = acExpExcel
    end
  end
  object OpenDialog1: TOpenDialog
    Options = [ofHideReadOnly, ofAllowMultiSelect, ofEnableSizing]
    Left = 104
    Top = 224
  end
  object quDelZ: TFDQuery
    Connection = dmR.FDConnection
    SQL.Strings = (
      'DECLARE @ISHOP int'
      'DECLARE @CASHNUM int'
      'DECLARE @ZNUM int'
      ''
      'EXECUTE [dbo].[prDelZ] @ISHOP,@CASHNUM,@ZNUM')
    Left = 80
    Top = 304
  end
  object quS: TFDQuery
    Connection = dmR.FDConnection
    SQL.Strings = (
      'SELECT TOP 1 [RARID] FROM [dbo].[EGAISID] where [ISHOP]=1')
    Left = 80
    Top = 368
  end
  object quA: TFDQuery
    Connection = dmR.FDConnection
    SQL.Strings = (
      '')
    Left = 152
    Top = 368
  end
  object quCashSail: TFDQuery
    Connection = dmR.FDConnection
    SQL.Strings = (
      
        'SELECT cs.[FSRARID],cs.[IDATE],[dbo].i2d([IDATE]) as ZDATE,cs.[C' +
        'ASHNUM],cs.[ZNUM],cs.[CHNUM],cs.[POSID],cs.[ID],cs.[ISHOP],cs.[D' +
        'EPART],cs.[ICODE],cs.[BARCODE]'
      
        '  ,cast(ca.NAME as Varchar) as NAMEC,ca.AVID,ca.KREP,ca.VOL,ca.P' +
        'RODID,ca.IMPORTERID'
      
        '  ,cast(cli.NAME as Varchar) as NAMECLI,cli.[PRODINN] as CLIENTI' +
        'NN,cli.[PRODKPP] as CLIENTKPP, cli.CLITYPE '
      
        '  ,cs.[QUANT],cs.[PRICER],cs.[SUMR],cs.[AMARK],cs.[KAP],cs.[CHDA' +
        'TE]'
      '  FROM [RAR].[dbo].[ACASHSAIL] cs'
      '  left join [dbo].[ACARDS] ca on ca.KAP=cs.[KAP]'
      '  left join [dbo].[APRODS] cli on cli.[PRODID]=ca.PRODID'
      '  where FSRARID=:RARID'
      '  and IDATE>=:IDATEB'
      '  and IDATE<=:IDATEE')
    Left = 264
    Top = 368
    ParamData = <
      item
        Name = 'RARID'
        DataType = ftString
        ParamType = ptInput
        Value = '020000079738'
      end
      item
        Name = 'IDATEB'
        DataType = ftInteger
        ParamType = ptInput
        Value = 42600
      end
      item
        Name = 'IDATEE'
        DataType = ftInteger
        ParamType = ptInput
        Value = 42700
      end>
    object quCashSailFSRARID: TStringField
      FieldName = 'FSRARID'
      Origin = 'FSRARID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 50
    end
    object quCashSailIDATE: TIntegerField
      FieldName = 'IDATE'
      Origin = 'IDATE'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quCashSailZDATE: TSQLTimeStampField
      FieldName = 'ZDATE'
      Origin = 'ZDATE'
      ReadOnly = True
    end
    object quCashSailCASHNUM: TIntegerField
      FieldName = 'CASHNUM'
      Origin = 'CASHNUM'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quCashSailZNUM: TIntegerField
      FieldName = 'ZNUM'
      Origin = 'ZNUM'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quCashSailCHNUM: TIntegerField
      FieldName = 'CHNUM'
      Origin = 'CHNUM'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quCashSailPOSID: TIntegerField
      FieldName = 'POSID'
      Origin = 'POSID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quCashSailID: TLargeintField
      AutoGenerateValue = arAutoInc
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object quCashSailISHOP: TIntegerField
      FieldName = 'ISHOP'
      Origin = 'ISHOP'
    end
    object quCashSailDEPART: TIntegerField
      FieldName = 'DEPART'
      Origin = 'DEPART'
    end
    object quCashSailICODE: TIntegerField
      FieldName = 'ICODE'
      Origin = 'ICODE'
    end
    object quCashSailBARCODE: TStringField
      FieldName = 'BARCODE'
      Origin = 'BARCODE'
      Size = 15
    end
    object quCashSailNAMEC: TStringField
      FieldName = 'NAMEC'
      Origin = 'NAMEC'
      ReadOnly = True
      Size = 30
    end
    object quCashSailAVID: TIntegerField
      FieldName = 'AVID'
      Origin = 'AVID'
    end
    object quCashSailKREP: TSingleField
      FieldName = 'KREP'
      Origin = 'KREP'
      DisplayFormat = '0.0'
    end
    object quCashSailVOL: TSingleField
      FieldName = 'VOL'
      Origin = 'VOL'
      DisplayFormat = '0.000'
    end
    object quCashSailPRODID: TStringField
      FieldName = 'PRODID'
      Origin = 'PRODID'
      Size = 50
    end
    object quCashSailIMPORTERID: TStringField
      FieldName = 'IMPORTERID'
      Origin = 'IMPORTERID'
      Size = 50
    end
    object quCashSailNAMECLI: TStringField
      FieldName = 'NAMECLI'
      Origin = 'NAMECLI'
      ReadOnly = True
      Size = 30
    end
    object quCashSailCLIENTINN: TStringField
      FieldName = 'CLIENTINN'
      Origin = 'CLIENTINN'
    end
    object quCashSailCLIENTKPP: TStringField
      FieldName = 'CLIENTKPP'
      Origin = 'CLIENTKPP'
    end
    object quCashSailQUANT: TSingleField
      FieldName = 'QUANT'
      Origin = 'QUANT'
      DisplayFormat = '0.000'
    end
    object quCashSailPRICER: TSingleField
      FieldName = 'PRICER'
      Origin = 'PRICER'
      DisplayFormat = '0.00'
    end
    object quCashSailSUMR: TSingleField
      FieldName = 'SUMR'
      Origin = 'SUMR'
      DisplayFormat = '0.00'
    end
    object quCashSailAMARK: TStringField
      FieldName = 'AMARK'
      Origin = 'AMARK'
      Size = 100
    end
    object quCashSailKAP: TStringField
      FieldName = 'KAP'
      Origin = 'KAP'
      Size = 50
    end
    object quCashSailCHDATE: TSQLTimeStampField
      FieldName = 'CHDATE'
      Origin = 'CHDATE'
    end
    object quCashSailCLITYPE: TStringField
      FieldName = 'CLITYPE'
      Origin = 'CLITYPE'
      Size = 5
    end
  end
  object dsquCashSail: TDataSource
    DataSet = quCashSail
    Left = 264
    Top = 424
  end
end
