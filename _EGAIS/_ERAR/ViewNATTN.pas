unit ViewNATTN;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  CustomChildForm, System.UITypes, ShellApi, httpsend, synautil, nativexml, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxRibbonSkins, dxRibbonCustomizationForm, cxContainer, cxEdit, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator,
  Data.DB, cxDBData, cxImageComboBox, cxTextEdit, cxDBLookupComboBox, cxCheckBox, cxCalendar, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.Menus, System.Actions, Vcl.ActnList, dxBar, cxBarEditItem, dxBarExtItems, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid, cxMemo, Vcl.ExtCtrls, dxRibbon;

type
  TfmNATTN = class(TfmCustomChildForm)
    dxRibbon1: TdxRibbon;
    dxRibbon1Tab1: TdxRibbonTab;
    dxBarManager1: TdxBarManager;
    bmApplyDate: TdxBar;
    bmClose: TdxBar;
    btnDone: TdxBarLargeButton;
    btnClose: TdxBarLargeButton;
    deDateBeg: TdxBarDateCombo;
    deDateEnd: TdxBarDateCombo;
    dxBarGroup1: TdxBarGroup;
    ActionList1: TActionList;
    acRefresh: TAction;
    acClose: TAction;
    acSelectDate: TAction;
    GridNATTN: TcxGrid;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    beiPoint: TcxBarEditItem;
    acExpExcel: TAction;
    dxBarLargeButton9: TdxBarLargeButton;
    dxBarLargeButton10: TdxBarLargeButton;
    beiGetTTN: TcxBarEditItem;
    dxBarLargeButton14: TdxBarLargeButton;
    dxBarLargeButton15: TdxBarLargeButton;
    acGetRawTTNList: TAction;
    dxBarLargeButton16: TdxBarLargeButton;
    LevelNATTN: TcxGridLevel;
    ViewNATTN: TcxGridDBTableView;
    ViewNATTNFSRARID: TcxGridDBColumn;
    ViewNATTNDATE_REPLY: TcxGridDBColumn;
    ViewNATTNWBREGID: TcxGridDBColumn;
    ViewNATTNNUMBER: TcxGridDBColumn;
    ViewNATTNSHIPDATE: TcxGridDBColumn;
    ViewNATTNCLIFROM: TcxGridDBColumn;
    ViewNATTNIACTIVE: TcxGridDBColumn;
    ViewNATTNFULLNAME: TcxGridDBColumn;
    ViewNATTNNAME: TcxGridDBColumn;
    ViewNATTNCLIENTINN: TcxGridDBColumn;
    ViewNATTNCLIENTKPP: TcxGridDBColumn;
    pmNATTN: TPopupMenu;
    acGetTTNPopup: TAction;
    N17: TMenuItem;
    ViewNATTNISHIPDATE: TcxGridDBColumn;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton11: TdxBarLargeButton;
    acGetTTN: TAction;
    procedure acRefreshExecute(Sender: TObject);
    procedure acCloseExecute(Sender: TObject);
    procedure acSelectDateExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure deDateBegChange(Sender: TObject);
    procedure acExpExcelExecute(Sender: TObject);
    procedure acGetRawTTNListExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acGetTTNPopupExecute(Sender: TObject);
    procedure acGetTTNExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }

    procedure Init;
  end;

var
  fmNATTN: TfmNATTN;

procedure ShowFormNATTN;

implementation

{$R *.dfm}

uses ViewXML, EgaisDecode, ViewCards, dmRar, UTMExch, ViewSpecOut, MCrystDocs, ProDocs, History, UTMArh, UTMCheck, DocHeaderCB,
  ViewSpec, Main;

procedure ShowFormNATTN;
begin
  if Assigned(fmNATTN)=False then //����� �� ����������, � ����� �������
    fmNATTN:=TfmNATTN.Create(Application);
  if fmNATTN.WindowState=wsMinimized then //���� ���� ��������, �� ������������� ���
    fmNATTN.WindowState:=wsNormal;

  fmNATTN.deDateBeg.Date:=Date-7;
  fmNATTN.deDateEnd.Date:=Date;
  fmNATTN.Init;
  fmNATTN.Show;
end;

procedure TfmNATTN.Init;
var
  FocusedRow, TopRow: Integer;
  flag: boolean;
begin
  with dmR do
  begin
    try
      //<--Refresh � ��������������� ������� ������� � �������
      flag:=quNATTN.Active;  //���������� ���� �� ������� �������, ��� �������������� ������� �������
      TopRow := ViewNATTN.Controller.TopRowIndex;
      FocusedRow := ViewNATTN.DataController.FocusedRowIndex;
      //-->
      ViewNATTN.BeginUpdate;
      quNATTN.Active:=False;
      quNATTN.ParamByName('IDATEB').AsInteger:=Trunc(deDateBeg.Date);
      quNATTN.ParamByName('IDATEE').AsInteger:=Trunc(deDateEnd.Date);
      quNATTN.ParamByName('RARID').AsString:=CommonSet.FSRAR_ID;
      quNATTN.Active:=True;
      quNATTN.First;

      //<--������ ��������� ����������� � �����
      quNATTN.First;
      while not quNATTN.Eof do begin
        if quNATTNTT2.AsInteger=1 then begin
          quProc.Active:=False;
          quProc.SQL.Clear;
          quProc.SQL.Add('DECLARE @FSRARID varchar(50) = '''+quNATTNFSRARID.AsString+'''');
          quProc.SQL.Add('DECLARE @WBREGID varchar(50) = '''+quNATTNWBREGID.AsString+'''');
          quProc.SQL.Add('DELETE FROM dbo.ANATTN');
          quProc.SQL.Add('WHERE FSRARID = @FSRARID');
          quProc.SQL.Add('  AND WBREGID = @WBREGID');
          quProc.ExecSQL;
        end;
        quNATTN.Next;
      end;
      //-->

      //<--���� ���� ��������, �� ���� ������ ������� �������
      quNATTN.Active:=False;
      quNATTN.ParamByName('IDATEB').AsInteger:=Trunc(deDateBeg.Date);
      quNATTN.ParamByName('IDATEE').AsInteger:=Trunc(deDateEnd.Date);
      quNATTN.ParamByName('RARID').AsString:=CommonSet.FSRAR_ID;
      quNATTN.Active:=True;
      quNATTN.First;
      //-->
    finally
      ViewNATTN.EndUpdate;
      //<--��������������� �������
      if flag then begin
        ViewNATTN.DataController.FocusedRowIndex := FocusedRow;
        ViewNATTN.Controller.TopRowIndex := TopRow;
      end;
      //-->
    end;
  end;

//  FSRAR_ID:=CommonSet.FSRAR_ID;
end;


procedure TfmNATTN.acCloseExecute(Sender: TObject);
begin
  //�������
  Close;
//  fmMain.Close;
end;

procedure TfmNATTN.acExpExcelExecute(Sender: TObject);
begin
  ExportGridToFile(formatdatetime('ddmmyyyy',deDateBeg.Date)+'_'+formatdatetime('ddmmyyyy',deDateEnd.Date)+'_'+formatdatetime('ddmmyyyy_hh_nn_ss',now)+'.xls', GridNATTN);
end;

procedure TfmNATTN.acGetRawTTNListExecute(Sender: TObject);
Const
  CR = #$0d;
  LF = #$0a;
  CRLF = CR + LF;
  Boundary = 'END_OF_PART';

var  AskXml: TNativeXml;
     IdF:Integer;
     RetXml: TNativeXml;
     RetId:string;

     httpsend: THTTPSend;
     s: AnsiString;
     FS: TMemoryStream;
begin
  // ��������� �������������� TTN
  if MessageDlg('��������� �������������� ��� � ����� ? ( ���������� - '+CommonSet.FSRAR_ID+')',mtConfirmation, [mbYes, mbNo], 0, mbYes) <> mrYes then Exit;
  if dmR.FDConnection.Connected=False then Exit;

  with dmR do
  begin
    try
      ClearMessageLogLocal;
      ShowMessageLogLocal('���� ��');

      IdF:=fGetId(1);
      ShowMessageLogLocal('  ������ '+its(IdF));

      FS := TMemoryStream.Create;
      httpsend := THTTPSend.Create;

      //��������� ���� �������
      AskXml := TNativeXml.Create(nil);
      RetXml := TNativeXml.Create(nil);

      AskXml.XmlFormat := xfReadable;
      AskXml.CreateName('ns:Documents');
      AskXml.Root.WriteAttributeString('Version', '1.0');
      AskXml.Root.WriteAttributeString('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
      AskXml.Root.WriteAttributeString('xmlns:ns', 'http://fsrar.ru/WEGAIS/WB_DOC_SINGLE_01');
      AskXml.Root.WriteAttributeString('xmlns:qp', 'http://fsrar.ru/WEGAIS/QueryParameters');
      AskXml.Root.NodeNew('ns:Owner');
      AskXml.Root.NodeByName('ns:Owner').NodeNew('ns:FSRAR_ID').Value:=CommonSet.FSRAR_ID;

      AskXml.Root.NodeNew('ns:Document');
      AskXml.Root.NodeByName('ns:Document').NodeNew('ns:QueryNATTN');
      AskXml.Root.NodeByName('ns:Document').NodeByName('ns:QueryNATTN').NodeNew('qp:Parameters');
      AskXml.Root.NodeByName('ns:Document').NodeByName('ns:QueryNATTN').NodeByName('qp:Parameters').NodeNew('qp:Parameter');

      AskXml.Root.NodeByName('ns:Document').NodeByName('ns:QueryNATTN').NodeByName('qp:Parameters').NodeByName('qp:Parameter').NodeNew('qp:Name').Value:=UTF8Encode('���');
      AskXml.Root.NodeByName('ns:Document').NodeByName('ns:QueryNATTN').NodeByName('qp:Parameters').NodeByName('qp:Parameter').NodeNew('qp:Value').Value:=CommonSet.FSRAR_ID;

      //AskXml.SaveToFile('c:\QueryNATTN.xml');
      AskXml.SaveToStream(FS);

      //����� � ����
      quToRar.Active:=False;
      quToRar.ParamByName('ID').AsInteger:=IdF;
      quToRar.ParamByName('RARID').AsString:=CommonSet.FSRAR_ID;
      quToRar.Active:=True;

      quToRar.Append;
      quToRarFSRARID.AsString:=CommonSet.FSRAR_ID;
      quToRarID.AsInteger:=IdF;
      quToRarDATEQU.AsDateTime:=Now;
      quToRarITYPEQU.AsInteger:=1;
      quToRarISTATUS.AsInteger:=1;
      //quToRarSENDFILE.LoadFromFile(NameF);
      quToRarSENDFILE.LoadFromStream(FS);
      quToRar.Post;

      httpsend.MimeType := 'multipart/form-data; boundary='+Boundary;  //���������� Contetn-Type �������
      // ���������� Mime-��� � ������ �� �����
      s:='--'+Boundary+CRLF+'Content-Disposition: form-data; name="xml_file"; filename="'+IdToName(IdF)+'"'+CRLF+'Content-Type: application/xml'+CRLF+CRLF;
      httpsend.Document.Write(PAnsiChar(s)^, Length(s));
      FS.Position := 0;
      httpsend.Document.CopyFrom(FS, FS.Size);  //���������� ����� � ���� ���������
      //S:=CRLF+CRLF+'--'+Boundary+CRLF;  //��������� ���� �������
      S:=CRLF+CRLF+'--'+Boundary+'--'+CRLF;  //��������� ���� �������
      httpsend.Document.Write(PAnsiChar(s)^, Length(s)); // ��������� ���� ���������

      httpsend.KeepAlive:=False;
      httpsend.Status100:=True;
      httpsend.Headers.Add('Accept: */*');

      // ���������� ������
      if httpsend.HTTPMethod('POST','http://'+CommonSet.IPUTM+':8080/opt/in/QueryNATTN') then
      begin
        ShowMessageLogLocal('TRUE');
        ShowMessageLogLocal('ResultCode='+IntToStr(httpsend.ResultCode));
        ShowMessageLogLocal(httpsend.ResultString);
        //MemoLogLocal.Lines.LoadFromStream(httpsend.Document, TEncoding.UTF8);

        httpsend.Document.Position:=0;
        RetXml.LoadFromStream(httpsend.Document);
        RetXml.XmlFormat := xfReadable;
        //RetXml.SaveToFile('C:\1110.xml');

        //RetId:='';
        //nodeRoot := RetXml.Root;
        //if assigned(nodeRoot) then  RetId:=nodeRoot.NodeByName('url').Value;

        RetId:=RetXml.Root.NodeByName('url').Value;

        if RetId>'' then
        begin
          httpsend.Document.Position:=0;

          quToRar.Edit;
          quToRarISTATUS.AsInteger:=2;
          quToRarRECEIVE_ID.AsString:=RetId;
          quToRarRECEIVE_FILE.LoadFromStream(httpsend.Document);
          quToRar.Post;
        end;

      end else begin
        ShowMessageLogLocal('FALSE');
        ShowMessageLogLocal('ResultCode='+IntToStr(httpsend.ResultCode));
        //ShowMessageLogLocal(httpsend.ResultString);

        quToRar.Edit;
        quToRarISTATUS.AsInteger:=100;
        quToRar.Post;
      end;

    finally
      FS.Free;
      AskXml.Free;
      RetXml.Free;
      quToRar.Active:=False;
    end;

    ShowMessageLogLocal('������� ��������.');
  end;
end;

procedure TfmNATTN.acGetTTNExecute(Sender: TObject);
Const
  CR = #$0d;
  LF = #$0a;
  CRLF = CR + LF;
  Boundary = 'END_OF_PART';

var  AskXml: TNativeXml;
     IdF:Integer;
     RetXml: TNativeXml;
     RetId:string;

     httpsend: THTTPSend;
     s: AnsiString;
     FS: TMemoryStream;
begin
  // ��������� �������� �� TTN
  if MessageDlg('��������� �������� '+beiGetTTN.EditValue+' � ����� ? ( ���������� - '+CommonSet.FSRAR_ID+')',mtConfirmation, [mbYes, mbNo], 0, mbYes) = mrYes then
  begin
    with dmR do
    begin
      if (FDConnection.Connected)and(beiGetTTN.EditValue>'') then
      begin
        try
          ClearMessageLogLocal;
          ShowMessageLogLocal('���� ��');

          IdF:=fGetId(1);
          ShowMessageLogLocal('  ������ '+its(IdF));

          FS := TMemoryStream.Create;
          httpsend := THTTPSend.Create;

          //��������� ���� �������
          AskXml := TNativeXml.Create(nil);
          RetXml := TNativeXml.Create(nil);

          AskXml.XmlFormat := xfReadable;
          AskXml.CreateName('ns:Documents');
          AskXml.Root.WriteAttributeString('Version', '1.0');
          AskXml.Root.WriteAttributeString('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
          AskXml.Root.WriteAttributeString('xmlns:ns', 'http://fsrar.ru/WEGAIS/WB_DOC_SINGLE_01');
          AskXml.Root.WriteAttributeString('xmlns:qp', 'http://fsrar.ru/WEGAIS/QueryParameters');
          AskXml.Root.NodeNew('ns:Owner');
          AskXml.Root.NodeByName('ns:Owner').NodeNew('ns:FSRAR_ID').Value:=CommonSet.FSRAR_ID;

          AskXml.Root.NodeNew('ns:Document');
          AskXml.Root.NodeByName('ns:Document').NodeNew('ns:QueryResendDoc');
          AskXml.Root.NodeByName('ns:Document').NodeByName('ns:QueryResendDoc').NodeNew('qp:Parameters');
          AskXml.Root.NodeByName('ns:Document').NodeByName('ns:QueryResendDoc').NodeByName('qp:Parameters').NodeNew('qp:Parameter');

          AskXml.Root.NodeByName('ns:Document').NodeByName('ns:QueryResendDoc').NodeByName('qp:Parameters').NodeByName('qp:Parameter').NodeNew('qp:Name').Value:='WBREGID';
          AskXml.Root.NodeByName('ns:Document').NodeByName('ns:QueryResendDoc').NodeByName('qp:Parameters').NodeByName('qp:Parameter').NodeNew('qp:Value').Value:=beiGetTTN.EditValue;

  //        AskXml.SaveToFile(NameF);
          AskXml.SaveToStream(FS);

          //����� � ����
          quToRar.Active:=False;
          quToRar.ParamByName('ID').AsInteger:=IdF;
          quToRar.ParamByName('RARID').AsString:=CommonSet.FSRAR_ID;
          quToRar.Active:=True;

          quToRar.Append;
          quToRarFSRARID.AsString:=CommonSet.FSRAR_ID;
          quToRarID.AsInteger:=IdF;
          quToRarDATEQU.AsDateTime:=Now;
          quToRarITYPEQU.AsInteger:=1;
          quToRarISTATUS.AsInteger:=1;
  //        quToRarSENDFILE.LoadFromFile(NameF);
          quToRarSENDFILE.LoadFromStream(FS);
          quToRar.Post;

          httpsend.MimeType := 'multipart/form-data; boundary='+Boundary;  //���������� Contetn-Type �������
          // ���������� Mime-��� � ������ �� �����
          s:='--'+Boundary+CRLF+'Content-Disposition: form-data; name="xml_file"; filename="'+IdToName(IdF)+'"'+CRLF+'Content-Type: application/xml'+CRLF+CRLF;
          httpsend.Document.Write(PAnsiChar(s)^, Length(s));
          FS.Position := 0;
          httpsend.Document.CopyFrom(FS, FS.Size);  //���������� ����� � ���� ���������
  //        S:=CRLF+CRLF+'--'+Boundary+CRLF;  //��������� ���� �������
          S:=CRLF+CRLF+'--'+Boundary+'--'+CRLF;  //��������� ���� �������
          httpsend.Document.Write(PAnsiChar(s)^, Length(s)); // ��������� ���� ���������

          httpsend.KeepAlive:=False;
          httpsend.Status100:=True;
          httpsend.Headers.Add('Accept: */*');

          // ���������� ������
          if httpsend.HTTPMethod('POST','http://'+CommonSet.IPUTM+':8080/opt/in/QueryResendDoc') then
          begin
            ShowMessageLogLocal('TRUE');
            ShowMessageLogLocal('ResultCode='+IntToStr(httpsend.ResultCode));
            ShowMessageLogLocal(httpsend.ResultString);
  //          MemoLogLocal.Lines.LoadFromStream(httpsend.Document, TEncoding.UTF8);

            httpsend.Document.Position:=0;
            RetXml.LoadFromStream(httpsend.Document);
            RetXml.XmlFormat := xfReadable;
  //          RetXml.SaveToFile('C:\1110.xml');

  //          RetId:='';
  //          nodeRoot := RetXml.Root;
  //          if assigned(nodeRoot) then  RetId:=nodeRoot.NodeByName('url').Value;

            RetId:=RetXml.Root.NodeByName('url').Value;

            if RetId>'' then
            begin
              httpsend.Document.Position:=0;

              quToRar.Edit;
              quToRarISTATUS.AsInteger:=2;
              quToRarRECEIVE_ID.AsString:=RetId;
              quToRarRECEIVE_FILE.LoadFromStream(httpsend.Document);
              quToRar.Post;
            end;

          end else begin
            ShowMessageLogLocal('FALSE');
            ShowMessageLogLocal('ResultCode='+IntToStr(httpsend.ResultCode));
  //          ShowMessageLogLocal(httpsend.ResultString);

            quToRar.Edit;
            quToRarISTATUS.AsInteger:=100;
            quToRar.Post;
          end;

        finally
          FS.Free;
          AskXml.Free;
          RetXml.Free;
          quToRar.Active:=False;
        end;

        ShowMessageLogLocal('������� ��������.');
      end;
    end;
  end;
end;

procedure TfmNATTN.acGetTTNPopupExecute(Sender: TObject);
begin
  //��������� ���������� �������� � �����
  beiGetTTN.EditValue:=dmR.quNATTNWBREGID.AsString;
  acGetTTN.Execute;
end;

procedure TfmNATTN.acRefreshExecute(Sender: TObject);
begin
  //������
  Init;
end;

procedure TfmNATTN.acSelectDateExecute(Sender: TObject);
begin
  //��������
  Init;
end;

procedure TfmNATTN.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
  fmNATTN:=Nil;
end;

procedure TfmNATTN.FormCreate(Sender: TObject);
begin
  ClearMessageLogLocal;
end;

procedure TfmNATTN.deDateBegChange(Sender: TObject);
begin
  if dmR.FDConnection.Connected then Init;
end;

end.
