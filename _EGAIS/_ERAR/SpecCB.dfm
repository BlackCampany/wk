object fmSpecCB: TfmSpecCB
  Left = 0
  Top = 0
  Caption = #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1103' '#1076#1086#1082#1091#1084#1077#1085#1090#1072' '#1062#1041
  ClientHeight = 493
  ClientWidth = 923
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object GridSpecCB: TcxGrid
    Left = 0
    Top = 127
    Width = 923
    Height = 366
    Align = alClient
    TabOrder = 0
    LookAndFeel.Kind = lfOffice11
    LookAndFeel.NativeStyle = False
    ExplicitTop = 133
    object ViewSpecCB: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.InfoPanel.Visible = True
      Navigator.Visible = True
      DataController.DataSource = dsquSpecCB
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'Quant'
          Column = ViewSpecCBQuant
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SumIn'
          Column = ViewSpecCBSumIn
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SumOut'
          Column = ViewSpecCBSumOut
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.MultiSelect = True
      OptionsView.ColumnAutoWidth = True
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      object ViewSpecCBId: TcxGridDBColumn
        Caption = #8470' '#1087#1087
        DataBinding.FieldName = 'Id'
      end
      object ViewSpecCBArticul: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1090#1086#1074#1072#1088#1072
        DataBinding.FieldName = 'Articul'
        Styles.Content = dmR.stlBold
      end
      object ViewSpecCBEdIzm: TcxGridDBColumn
        Caption = #1077#1076'.'#1080#1079#1084'.'
        DataBinding.FieldName = 'EdIzm'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Items = <
          item
            Description = #1096#1090'.'
            ImageIndex = 0
            Value = 1
          end
          item
            Description = #1082#1075'.'
            Value = 2
          end>
      end
      object ViewSpecCBName: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'Name'
        Width = 155
      end
      object ViewSpecCBQuant: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086
        DataBinding.FieldName = 'Quant'
        Styles.Content = dmR.stlLightBlue
      end
      object ViewSpecCBPriceIn: TcxGridDBColumn
        Caption = #1062#1077#1085#1072' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
        DataBinding.FieldName = 'PriceIn'
      end
      object ViewSpecCBSumIn: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
        DataBinding.FieldName = 'SumIn'
      end
      object ViewSpecCBPriceOut: TcxGridDBColumn
        Caption = #1062#1077#1085#1072' '#1084#1072#1075#1072#1079#1080#1085#1072
        DataBinding.FieldName = 'PriceOut'
      end
      object ViewSpecCBSumOut: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1084#1072#1075#1072#1079#1080#1085#1072
        DataBinding.FieldName = 'SumOut'
      end
      object ViewSpecCBFullName: TcxGridDBColumn
        Caption = #1055#1086#1083#1085#1086#1077' '#1085#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'FullName'
        Width = 195
      end
    end
    object LevelSpecCB: TcxGridLevel
      GridView = ViewSpecCB
    end
  end
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 923
    Height = 127
    BarManager = dxBarManager1
    Style = rs2010
    ColorSchemeAccent = rcsaOrange
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 5
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1103' '#1076#1086#1082#1091#1084#1077#1085#1090#1072' '#1062#1041
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar1'
        end>
      Index = 0
    end
  end
  object quSpecCB: TFDQuery
    Connection = dmR.FDConnection
    SQL.Strings = (
      'SELECT Id_TTN=sp.[IDHEAD]'
      '      ,Id=sp.[NUM]'
      '      ,Articul=sp.[IDCARD]'
      #9'  ,gd.[Name]'
      #9'  ,gd.[FullName]'
      '      ,gd.EdIzm'
      '      ,sp.[Quant]'
      '      ,sp.[PriceIn]'
      '      ,PriceOut=sp.[PRICEUCH]'
      '      ,sp.[SumIn]'
      '      ,SumOut=sp.[SUMUCH]'
      '  FROM [MCRYSTAL].[dbo].[DOCIN_SPEC] sp'
      #9'left join [MCRYSTAL].[dbo].[CARDS] gd on gd.Id=sp.[IDCARD]'
      '  where sp.[IDHEAD]=:IDH'
      '  order by sp.[Id]')
    Left = 112
    Top = 208
    ParamData = <
      item
        Name = 'IDH'
        DataType = ftString
        ParamType = ptInput
        Value = '168727'
      end>
    object quSpecCBId_TTN: TLargeintField
      FieldName = 'Id_TTN'
      Origin = 'Id_TTN'
      Required = True
    end
    object quSpecCBId: TIntegerField
      FieldName = 'Id'
      Origin = 'Id'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object quSpecCBArticul: TIntegerField
      FieldName = 'Articul'
      Origin = 'Articul'
    end
    object quSpecCBName: TStringField
      FieldName = 'Name'
      Origin = 'Name'
      Size = 200
    end
    object quSpecCBFullName: TStringField
      FieldName = 'FullName'
      Origin = 'FullName'
      Size = 200
    end
    object quSpecCBEdIzm: TSmallintField
      FieldName = 'EdIzm'
      Origin = 'EdIzm'
    end
    object quSpecCBQuant: TSingleField
      FieldName = 'Quant'
      Origin = 'Quant'
      DisplayFormat = '0'
    end
    object quSpecCBPriceIn: TFloatField
      FieldName = 'PriceIn'
      Origin = 'PriceIn'
      DisplayFormat = '0.00'
    end
    object quSpecCBPriceOut: TFloatField
      FieldName = 'PriceOut'
      Origin = 'PriceOut'
      DisplayFormat = '0.00'
    end
    object quSpecCBSumIn: TSingleField
      FieldName = 'SumIn'
      Origin = 'SumIn'
      DisplayFormat = '0.00'
    end
    object quSpecCBSumOut: TSingleField
      FieldName = 'SumOut'
      Origin = 'SumOut'
      DisplayFormat = '0.00'
    end
  end
  object dsquSpecCB: TDataSource
    DataSet = quSpecCB
    Left = 112
    Top = 272
  end
  object ActionManager1: TActionManager
    LargeImages = dmR.LargeImage
    Images = dmR.SmallImage
    Left = 224
    Top = 168
    StyleName = 'Platform Default'
    object acClose: TAction
      Caption = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 100
      OnExecute = acCloseExecute
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      #1044#1077#1081#1089#1090#1074#1080#1103)
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmR.SmallImage
    ImageOptions.LargeImages = dmR.LargeImage
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 316
    Top = 172
    DockControlHeights = (
      0
      0
      0
      0)
    object dxBarManager1Bar1: TdxBar
      Caption = #1047#1072#1082#1088#1099#1090#1100
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 925
      FloatTop = 8
      FloatClientWidth = 92
      FloatClientHeight = 54
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = acClose
      Category = 0
    end
    object dxBarGroup1: TdxBarGroup
      Items = ()
    end
  end
end
