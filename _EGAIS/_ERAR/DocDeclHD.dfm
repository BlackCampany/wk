object fmDocDecl: TfmDocDecl
  Left = 0
  Top = 0
  Caption = #1056#1077#1077#1089#1090#1088' '#1076#1077#1082#1083#1072#1088#1072#1094#1080#1081
  ClientHeight = 558
  ClientWidth = 1272
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1272
    Height = 127
    ApplicationButton.Visible = False
    BarManager = dxBarManager1
    Style = rs2010
    ColorSchemeAccent = rcsaOrange
    ColorSchemeName = 'Blue'
    SupportNonClientDrawing = True
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1056#1077#1077#1089#1090#1088' '#1076#1077#1082#1083#1072#1088#1072#1094#1080#1081
      Groups = <
        item
          ToolbarName = 'bmApplyDate'
        end
        item
          Caption = #1054#1087#1077#1088#1072#1094#1080#1080
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'bmClose'
        end>
      Index = 0
    end
  end
  object GridDocDecl: TcxGrid
    Left = 0
    Top = 127
    Width = 1272
    Height = 431
    Align = alClient
    TabOrder = 5
    LevelTabs.Style = 8
    LookAndFeel.Kind = lfOffice11
    LookAndFeel.NativeStyle = False
    object ViewDocDecl: TcxGridDBTableView
      PopupMenu = pmDocVn
      OnDblClick = ViewDocDeclDblClick
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.First.Visible = False
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.InfoPanel.Visible = True
      Navigator.Visible = True
      DataController.DataSource = dsquDocsDecl
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.MultiSelect = True
      object ViewDocDeclMASTERORG: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1080
        DataBinding.FieldName = 'MASTERORG'
        Width = 39
      end
      object ViewDocDeclNAME: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'NAME'
        Width = 129
      end
      object ViewDocDeclINN: TcxGridDBColumn
        Caption = #1048#1053#1053
        DataBinding.FieldName = 'INN'
        Width = 107
      end
      object ViewDocDeclKPP: TcxGridDBColumn
        Caption = #1050#1055#1055
        DataBinding.FieldName = 'KPP'
        Width = 105
      end
      object ViewDocDeclID: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
        DataBinding.FieldName = 'ID'
        Width = 45
      end
      object ViewDocDeclDDATEB: TcxGridDBColumn
        Caption = #1044#1072#1090#1072' '#1085#1072#1095#1072#1083#1072
        DataBinding.FieldName = 'DDATEB'
        Width = 94
      end
      object ViewDocDeclDDATEE: TcxGridDBColumn
        Caption = #1044#1072#1090#1072' '#1082#1086#1085#1094#1072
        DataBinding.FieldName = 'DDATEE'
        Width = 98
      end
      object ViewDocDeclIT1: TcxGridDBColumn
        Caption = #1058#1080#1087
        DataBinding.FieldName = 'IT1'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Items = <
          item
            Description = #1055#1086' '#1072#1083#1082#1086#1075#1086#1083#1102
            ImageIndex = 0
            Value = 0
          end
          item
            Description = #1055#1086' '#1087#1080#1074#1091
            Value = 1
          end>
        Width = 70
      end
      object ViewDocDeclIT2: TcxGridDBColumn
        Caption = #1057#1090#1072#1090#1091#1089
        DataBinding.FieldName = 'IT2'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Items = <
          item
            Description = #1055#1077#1088#1074#1080#1095#1085#1072#1103
            ImageIndex = 0
            Value = 0
          end
          item
            Description = #1050#1086#1088#1088#1077#1082#1090#1080#1088#1091#1102#1097#1072#1103
            Value = 1
          end>
      end
      object ViewDocDeclIACTIVE: TcxGridDBColumn
        Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077
        DataBinding.FieldName = 'IACTIVE'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Images = dmR.SmallImage
        Properties.Items = <
          item
            Description = #1054#1090#1087#1088#1072#1074#1083#1077#1085#1072
            ImageIndex = 78
            Value = 1
          end
          item
            Description = #1074' '#1088#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1085#1080#1080
            ImageIndex = 80
            Value = 0
          end>
        Width = 71
      end
      object ViewDocDeclIDATEB: TcxGridDBColumn
        Caption = #1044#1072#1090#1072' '#1085#1072#1095#1072#1083#1072' ('#1094#1077#1083#1086#1077')'
        DataBinding.FieldName = 'IDATEB'
        Visible = False
      end
      object ViewDocDeclIDATEE: TcxGridDBColumn
        Caption = #1044#1072#1090#1072' '#1082#1086#1085#1094#1072' ('#1094#1077#1083#1086#1077')'
        DataBinding.FieldName = 'IDATEE'
        Visible = False
      end
    end
    object LevelDocDecl: TcxGridLevel
      Caption = #1053#1077#1086#1073#1088#1072#1073#1086#1090#1072#1085#1085#1099#1077' '#1058#1058#1053
      GridView = ViewDocDecl
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      #1044#1077#1081#1089#1090#1074#1080#1103)
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmR.SmallImage
    ImageOptions.LargeImages = dmR.LargeImage
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 708
    Top = 204
    DockControlHeights = (
      0
      0
      0
      0)
    object bmApplyDate: TdxBar
      Caption = #1044#1077#1081#1089#1090#1074#1080#1103
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 778
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          UserDefine = [udWidth]
          UserWidth = 75
          Visible = True
          ItemName = 'deDateBeg'
        end
        item
          UserDefine = [udWidth]
          UserWidth = 74
          Visible = True
          ItemName = 'deDateEnd'
        end
        item
          Visible = True
          ItemName = 'btnDone'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton17'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton18'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton19'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object bmClose: TdxBar
      Caption = #1047#1072#1082#1088#1099#1090#1100
      CaptionButtons = <>
      DockedLeft = 805
      DockedTop = 0
      FloatLeft = 778
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'btnClose'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar1: TdxBar
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedLeft = 503
      DockedTop = 0
      FloatLeft = 1306
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton22'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton23'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton20'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton21'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object deDateBeg: TdxBarDateCombo
      Caption = 'C  '
      Category = 0
      Hint = 'C  '
      Visible = ivAlways
      OnChange = deDateBegChange
      ImageIndex = 25
      ShowDayText = False
    end
    object deDateEnd: TdxBarDateCombo
      Caption = #1087#1086
      Category = 0
      Hint = #1087#1086
      Visible = ivAlways
      OnChange = deDateBegChange
      ImageIndex = 25
      ShowDayText = False
    end
    object btnDone: TdxBarLargeButton
      Action = acRefresh
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Caption = #1059#1058#1052' '#1086#1073#1084#1077#1085
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 38
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Caption = #1059#1058#1052' '#1080#1089#1090#1086#1088#1080#1103
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 130
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Caption = #1059#1058#1052' '#1072#1088#1093#1080#1074
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 21
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = #1048#1053#1053' '
      Category = 0
      Hint = #1048#1053#1053' '
      Visible = ivAlways
      PropertiesClassName = 'TcxTextEditProperties'
      BarStyleDropDownButton = False
      Properties.AutoSelect = False
      Properties.IncrementalSearch = False
      Properties.ValidationOptions = [evoAllowLoseFocus]
      InternalEditValue = ''
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Caption = #1047#1072#1087#1088#1086#1089' '#1072#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090#1072' '#1087#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1103
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 16
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Caption = #1056#1072#1079#1088#1077#1096#1080#1090#1100' '#1086#1090#1087#1088#1072#1074#1082#1091'  '
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 1
    end
    object beiPoint: TcxBarEditItem
      Caption = #1058#1086#1095#1082#1072' '#1088#1077#1072#1083'.'
      Category = 0
      Hint = #1058#1086#1095#1082#1072' '#1088#1077#1072#1083'.'
      Visible = ivAlways
      PropertiesClassName = 'TcxLookupComboBoxProperties'
      Properties.DropDownListStyle = lsFixedList
      Properties.DropDownRows = 40
      Properties.DropDownSizeable = True
      Properties.DropDownWidth = 350
      Properties.KeyFieldNames = 'ISHOP'
      Properties.ListColumns = <
        item
          Caption = #1052#1072#1075#1072#1079#1080#1085
          FieldName = 'Name'
        end
        item
          Caption = #1052#1072#1075'.'#8470
          FieldName = 'ISHOP'
        end
        item
          FieldName = 'RARID'
        end>
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Caption = #1052#1050#1088#1080#1089#1090#1072#1083
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 161
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Caption = #1055#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1086
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 162
    end
    object beiGetTTN: TcxBarEditItem
      Caption = #8470' TTN'
      Category = 0
      Hint = #8470' TTN'
      Visible = ivAlways
      PropertiesClassName = 'TcxTextEditProperties'
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1047#1072#1087#1088#1086#1089#1080#1090#1100' '#1076#1086#1082#1091#1084#1077#1085#1090' '#1074' '#1045#1043#1040#1048#1057
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 50
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = #1047#1072#1087#1088#1086#1089#1080#1090#1100' '#1085#1077#1086#1073#1088#1072#1073#1086#1090#1072#1085#1085#1099#1077' '#1058#1058#1053
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 164
    end
    object btnClose: TdxBarLargeButton
      Action = acClose
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Caption = #1047#1072#1087#1088#1086#1089' '#1089#1087#1088#1072#1074#1082#1080' 1'
      Category = 0
      Visible = ivNever
      LargeImageIndex = 164
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Caption = #1047#1072#1087#1088#1086#1089' '#1086#1089#1090#1072#1090#1082#1086#1074
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 164
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Caption = #1047#1072#1087#1088#1086#1089' '#1085#1077#1095#1080#1090#1072#1077#1084#1099#1093' '#1096#1090#1088#1080#1093#1082#1086#1076#1086#1074
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 34
      ShortCut = 49218
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Caption = #1044#1086#1082#1091#1084#1077#1085#1090#1099' '#1062#1041
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 167
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = #1047#1072#1087#1088#1086#1089#1080#1090#1100' '#1085#1077#1086#1073#1088#1072#1073#1086#1090#1072#1085#1085#1099#1077' '#1058#1058#1053
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 164
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = acSendCorr
      Category = 0
    end
    object dxBarButton1: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = acAddDoc
      Category = 0
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Action = acEditDoc
      Category = 0
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Action = acViewDoc
      Category = 0
    end
    object dxBarLargeButton19: TdxBarLargeButton
      Action = acDelDoc
      Category = 0
    end
    object dxBarLargeButton20: TdxBarLargeButton
      Action = acOn
      Category = 0
    end
    object dxBarLargeButton21: TdxBarLargeButton
      Action = acOff
      Category = 0
    end
    object dxBarLargeButton22: TdxBarLargeButton
      Action = acCalcDecl
      Category = 0
    end
    object dxBarLargeButton23: TdxBarLargeButton
      Action = acExpXML
      Category = 0
    end
    object dxBarGroup1: TdxBarGroup
      Items = ()
    end
  end
  object ActionList1: TActionList
    Images = dmR.SmallImage
    Left = 560
    Top = 228
    object acRefresh: TAction
      Category = #1060#1086#1088#1084#1072
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100' (F5)'
      Hint = #1054#1073#1085#1086#1074#1080#1090#1100' (F5)'
      ImageIndex = 5
      ShortCut = 116
      OnExecute = acRefreshExecute
    end
    object acClose: TAction
      Category = #1060#1086#1088#1084#1072
      Caption = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 100
      OnExecute = acCloseExecute
    end
    object acSelectDate: TAction
      Category = #1060#1086#1088#1084#1072
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      Hint = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      ImageIndex = 1
      OnExecute = acSelectDateExecute
    end
    object acExpExcel: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1069#1082#1089#1087#1086#1088#1090' '#1074' Excel'
      ImageIndex = 76
      OnExecute = acExpExcelExecute
    end
    object acSendCorr: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100' '#1089#1090#1072#1090#1091#1089' '#1085#1072' '#1086#1090#1087#1088#1072#1074#1083#1077#1085#1085#1099#1081
      ImageIndex = 27
    end
    object acResetStatus: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1055#1077#1088#1077#1074#1077#1089#1090#1080' '#1074' '#1085#1077#1086#1090#1087#1088#1072#1074#1083#1077#1085#1085#1099#1077
      ImageIndex = 154
      OnExecute = acResetStatusExecute
    end
    object acTestRemn: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1055#1088#1086#1074#1077#1088#1080#1090#1100' '#1086#1089#1090#1072#1090#1082#1080' '#1087#1086' '#1080#1085#1074'.2'
      ImageIndex = 29
      Visible = False
    end
    object acAddDoc: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1076#1086#1082#1091#1084#1077#1085#1090
      ImageIndex = 75
      OnExecute = acAddDocExecute
    end
    object acDelDoc: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1076#1086#1082#1091#1084#1077#1085#1090
      ImageIndex = 74
      OnExecute = acDelDocExecute
    end
    object acEditDoc: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100
      ImageIndex = 73
      OnExecute = acEditDocExecute
    end
    object acViewDoc: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1055#1088#1086#1089#1084#1086#1090#1088
      ImageIndex = 39
      OnExecute = acViewDocExecute
    end
    object acOn: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1054#1087#1088#1080#1093#1086#1076#1086#1074#1072#1090#1100
      ImageIndex = 81
      OnExecute = acOnExecute
    end
    object acOff: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1054#1090#1082#1072#1090#1080#1090#1100
      ImageIndex = 80
      OnExecute = acOffExecute
    end
    object acCalcDecl: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1056#1072#1089#1095#1080#1090#1072#1090#1100
      ImageIndex = 38
      OnExecute = acCalcDeclExecute
    end
    object acImportRemn: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = 'acImportRemn'
      ShortCut = 49225
      OnExecute = acImportRemnExecute
    end
    object acExpXML: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1069#1082#1089#1087#1086#1088#1090' '#1074' XML'
      ImageIndex = 70
      OnExecute = acExpXMLExecute
    end
    object acCalcDecl1: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = 'acCalcDecl1'
      ShortCut = 49219
      OnExecute = acCalcDecl1Execute
    end
  end
  object pmDocVn: TPopupMenu
    Images = dmR.SmallImage
    Left = 856
    Top = 272
    object N2: TMenuItem
      Action = acCalcDecl
    end
    object N3: TMenuItem
      Action = acAddDoc
    end
    object N5: TMenuItem
      Action = acDelDoc
    end
    object XML1: TMenuItem
      Action = acExpXML
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object N17: TMenuItem
      Action = acSendCorr
      Visible = False
    end
    object N1: TMenuItem
      Action = acResetStatus
      Visible = False
    end
  end
  object quDocsDecl: TFDQuery
    Connection = dmR.FDConnection
    SQL.Strings = (
      'declare @IDATEB int = :IDATEB'
      'declare @IDATEE int = :IDATEE'
      ''
      'SELECT hd.[MASTERORG]'
      '      ,org.[NAME]'
      #9'  ,org.INN'
      #9'  ,org.KPP '
      '      ,hd.[ID]'
      '      ,hd.[IDATEB]'
      '      ,hd.[IDATEE]'
      '      ,hd.[DDATEB]'
      '      ,hd.[DDATEE]'
      '      ,hd.[IACTIVE]'
      '      ,hd.[IT1]'
      '      ,hd.[IT2]'
      '      ,hd.[INUMCORR]'
      '  FROM [dbo].[ADECL_HD] hd'
      '  left join [dbo].[DECLORG] org on org.ID=hd.[MASTERORG]'
      '  where hd.[IDATEB]>=@IDATEB'
      '  and hd.[IDATEE]<=@IDATEE ')
    Left = 96
    Top = 216
    ParamData = <
      item
        Name = 'IDATEB'
        DataType = ftInteger
        ParamType = ptInput
        Value = 42000
      end
      item
        Name = 'IDATEE'
        DataType = ftInteger
        ParamType = ptInput
        Value = 42400
      end>
    object quDocsDeclMASTERORG: TIntegerField
      FieldName = 'MASTERORG'
      Origin = 'MASTERORG'
      Required = True
    end
    object quDocsDeclNAME: TStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      Size = 200
    end
    object quDocsDeclINN: TStringField
      FieldName = 'INN'
      Origin = 'INN'
    end
    object quDocsDeclKPP: TStringField
      FieldName = 'KPP'
      Origin = 'KPP'
    end
    object quDocsDeclID: TLargeintField
      FieldName = 'ID'
      Origin = 'ID'
      Required = True
    end
    object quDocsDeclIDATEB: TIntegerField
      FieldName = 'IDATEB'
      Origin = 'IDATEB'
    end
    object quDocsDeclIDATEE: TIntegerField
      FieldName = 'IDATEE'
      Origin = 'IDATEE'
    end
    object quDocsDeclDDATEB: TSQLTimeStampField
      FieldName = 'DDATEB'
      Origin = 'DDATEB'
    end
    object quDocsDeclDDATEE: TSQLTimeStampField
      FieldName = 'DDATEE'
      Origin = 'DDATEE'
    end
    object quDocsDeclIACTIVE: TSmallintField
      FieldName = 'IACTIVE'
      Origin = 'IACTIVE'
    end
    object quDocsDeclIT1: TSmallintField
      FieldName = 'IT1'
      Origin = 'IT1'
    end
    object quDocsDeclIT2: TSmallintField
      FieldName = 'IT2'
      Origin = 'IT2'
    end
    object quDocsDeclINUMCORR: TSmallintField
      FieldName = 'INUMCORR'
      Origin = 'INUMCORR'
    end
  end
  object dsquDocsDecl: TDataSource
    DataSet = quDocsDecl
    Left = 96
    Top = 270
  end
  object FileSaveDialog: TFileSaveDialog
    FavoriteLinks = <>
    FileTypes = <>
    Options = []
    Left = 235
    Top = 250
  end
  object quSelProd: TFDQuery
    Connection = dmR.FDConnection
    SQL.Strings = (
      'SELECT sp.[PRODID]'
      ',cast(pr.NAME as Varchar(200)) as NAME'
      ',cast(pr.FULLNAME  as Varchar(200)) as FULLNAME'
      ',pr.PRODINN,pr.PRODKPP,pr.[COUNTRY],pr.REGCODE'
      ',cast(pr.ADDR as Varchar(300)) as ADDR'
      ',pr.CLITYPE'
      '  FROM [dbo].[ADECL_SP1] sp'
      '  left join [dbo].[APRODS] pr on pr.PRODID=sp.[PRODID]'
      '  where sp.[IDHEAD]=:IDH'
      
        '  group by sp.[PRODID],pr.NAME,pr.FULLNAME,pr.PRODINN,pr.PRODKPP' +
        ',pr.[COUNTRY],pr.REGCODE,pr.ADDR,pr.CLITYPE'
      '  order by sp.[PRODID]')
    Left = 360
    Top = 224
    ParamData = <
      item
        Name = 'IDH'
        DataType = ftInteger
        ParamType = ptInput
        Value = 13
      end>
    object quSelProdPRODID: TStringField
      FieldName = 'PRODID'
      Origin = 'PRODID'
      Size = 50
    end
    object quSelProdNAME: TStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      ReadOnly = True
      Size = 200
    end
    object quSelProdFULLNAME: TStringField
      FieldName = 'FULLNAME'
      Origin = 'FULLNAME'
      ReadOnly = True
      Size = 200
    end
    object quSelProdPRODINN: TStringField
      FieldName = 'PRODINN'
      Origin = 'PRODINN'
    end
    object quSelProdPRODKPP: TStringField
      FieldName = 'PRODKPP'
      Origin = 'PRODKPP'
    end
    object quSelProdCOUNTRY: TStringField
      FieldName = 'COUNTRY'
      Origin = 'COUNTRY'
      Size = 10
    end
    object quSelProdREGCODE: TStringField
      FieldName = 'REGCODE'
      Origin = 'REGCODE'
      Size = 10
    end
    object quSelProdADDR: TStringField
      FieldName = 'ADDR'
      Origin = 'ADDR'
      ReadOnly = True
      Size = 300
    end
    object quSelProdCLITYPE: TStringField
      FieldName = 'CLITYPE'
      Origin = 'CLITYPE'
      Size = 5
    end
  end
  object quSelCliDecl: TFDQuery
    Connection = dmR.FDConnection
    SQL.Strings = (
      '/****** '#1057#1082#1088#1080#1087#1090' '#1076#1083#1103' '#1082#1086#1084#1072#1085#1076#1099' SelectTopNRows '#1080#1079' '#1089#1088#1077#1076#1099' SSMS  ******/'
      'SELECT sp.[CLIENTID],cli.[CLIENTINN],cli.[CLIENTKPP]'
      ',cast(cli.[FULLNAME] as Varchar(200)) as FULLNAME'
      ',cast(cli.[NAME] as Varchar(200)) as NAME'
      ',cli.[COUNTRY],cli.[REGCODE]'
      ',cast(cli.[ADDR] as Varchar(500)) as ADDR'
      ' FROM [dbo].[ADECL_SP2] sp'
      ' left join [dbo].[ACLIENTS] cli on cli.CLIENTID=sp.[CLIENTID]'
      ' where sp.[IDHEAD]=:IDHEAD'
      
        ' group by sp.[CLIENTID],cli.[CLIENTINN],cli.[CLIENTKPP],cli.[FUL' +
        'LNAME],cli.[NAME],cli.[COUNTRY],cli.[REGCODE],cli.[ADDR]')
    Left = 360
    Top = 288
    ParamData = <
      item
        Name = 'IDHEAD'
        DataType = ftInteger
        ParamType = ptInput
        Value = 13
      end>
    object quSelCliDeclCLIENTID: TStringField
      FieldName = 'CLIENTID'
      Origin = 'CLIENTID'
      Size = 50
    end
    object quSelCliDeclCLIENTINN: TStringField
      FieldName = 'CLIENTINN'
      Origin = 'CLIENTINN'
    end
    object quSelCliDeclCLIENTKPP: TStringField
      FieldName = 'CLIENTKPP'
      Origin = 'CLIENTKPP'
    end
    object quSelCliDeclFULLNAME: TStringField
      FieldName = 'FULLNAME'
      Origin = 'FULLNAME'
      ReadOnly = True
      Size = 200
    end
    object quSelCliDeclNAME: TStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      ReadOnly = True
      Size = 200
    end
    object quSelCliDeclCOUNTRY: TStringField
      FieldName = 'COUNTRY'
      Origin = 'COUNTRY'
      Size = 10
    end
    object quSelCliDeclREGCODE: TStringField
      FieldName = 'REGCODE'
      Origin = 'REGCODE'
      Size = 10
    end
    object quSelCliDeclADDR: TStringField
      FieldName = 'ADDR'
      Origin = 'ADDR'
      ReadOnly = True
      Size = 500
    end
  end
  object quSelLic: TFDQuery
    Connection = dmR.FDConnection
    SQL.Strings = (
      'SELECT [IDCLI]'
      '      ,[IDL]'
      '      ,[LICNUM]'
      '      ,[IDATEB]'
      '      ,[IDATEE]'
      '      ,[ORGAN]'
      '  FROM [dbo].[ALIC]'
      '  where [IDCLI]=:IDCLI'
      '  order by [IDATEE] desc, [IDATEB] desc')
    Left = 432
    Top = 288
    ParamData = <
      item
        Name = 'IDCLI'
        DataType = ftString
        ParamType = ptInput
        Value = '010000006633'
      end>
    object quSelLicIDCLI: TStringField
      FieldName = 'IDCLI'
      Origin = 'IDCLI'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 50
    end
    object quSelLicIDL: TFDAutoIncField
      FieldName = 'IDL'
      Origin = 'IDL'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object quSelLicLICNUM: TStringField
      FieldName = 'LICNUM'
      Origin = 'LICNUM'
    end
    object quSelLicIDATEB: TIntegerField
      FieldName = 'IDATEB'
      Origin = 'IDATEB'
    end
    object quSelLicIDATEE: TIntegerField
      FieldName = 'IDATEE'
      Origin = 'IDATEE'
    end
    object quSelLicORGAN: TStringField
      FieldName = 'ORGAN'
      Origin = 'ORGAN'
      Size = 250
    end
  end
  object quSelOrgDecl: TFDQuery
    Connection = dmR.FDConnection
    SQL.Strings = (
      'SELECT [ID]'
      '      ,[MAIN]'
      '      ,[FSRARID]'
      '      ,[NAME]'
      '      ,[PHONE]'
      '      ,[EMAILORG]'
      '      ,[INN]'
      '      ,[KPP]'
      '      ,[DIR1]'
      '      ,[DIR2]'
      '      ,[DIR3]'
      '      ,[GB1]'
      '      ,[GB2]'
      '      ,[GB3]'
      '      ,[ADDR_CC]'
      '      ,[ADDR_IND]'
      '      ,[ADDR_REG]'
      '      ,[ADDR_RN]'
      '      ,[ADDR_CITY]'
      '      ,[ADDR_NP]'
      '      ,[ADDR_STR]'
      '      ,[ADDR_D]'
      '      ,[ADDR_KORP]'
      '      ,[ADDR_L]'
      '      ,[ADDR_KV]'
      '  FROM [RAR].[dbo].[DECLORG]'
      '  where ID=:IDORG')
    Left = 360
    Top = 352
    ParamData = <
      item
        Name = 'IDORG'
        DataType = ftInteger
        ParamType = ptInput
        Value = 1
      end>
    object quSelOrgDeclID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quSelOrgDeclMAIN: TIntegerField
      FieldName = 'MAIN'
      Origin = 'MAIN'
    end
    object quSelOrgDeclFSRARID: TStringField
      FieldName = 'FSRARID'
      Origin = 'FSRARID'
      Size = 50
    end
    object quSelOrgDeclNAME: TStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      Size = 200
    end
    object quSelOrgDeclPHONE: TStringField
      FieldName = 'PHONE'
      Origin = 'PHONE'
      Size = 50
    end
    object quSelOrgDeclEMAILORG: TStringField
      FieldName = 'EMAILORG'
      Origin = 'EMAILORG'
      Size = 100
    end
    object quSelOrgDeclINN: TStringField
      FieldName = 'INN'
      Origin = 'INN'
    end
    object quSelOrgDeclKPP: TStringField
      FieldName = 'KPP'
      Origin = 'KPP'
    end
    object quSelOrgDeclDIR1: TStringField
      FieldName = 'DIR1'
      Origin = 'DIR1'
      Size = 100
    end
    object quSelOrgDeclDIR2: TStringField
      FieldName = 'DIR2'
      Origin = 'DIR2'
      Size = 100
    end
    object quSelOrgDeclDIR3: TStringField
      FieldName = 'DIR3'
      Origin = 'DIR3'
      Size = 100
    end
    object quSelOrgDeclGB1: TStringField
      FieldName = 'GB1'
      Origin = 'GB1'
      Size = 100
    end
    object quSelOrgDeclGB2: TStringField
      FieldName = 'GB2'
      Origin = 'GB2'
      Size = 100
    end
    object quSelOrgDeclGB3: TStringField
      FieldName = 'GB3'
      Origin = 'GB3'
      Size = 100
    end
    object quSelOrgDeclADDR_CC: TStringField
      FieldName = 'ADDR_CC'
      Origin = 'ADDR_CC'
      Size = 10
    end
    object quSelOrgDeclADDR_IND: TStringField
      FieldName = 'ADDR_IND'
      Origin = 'ADDR_IND'
      Size = 10
    end
    object quSelOrgDeclADDR_REG: TStringField
      FieldName = 'ADDR_REG'
      Origin = 'ADDR_REG'
      Size = 5
    end
    object quSelOrgDeclADDR_RN: TStringField
      FieldName = 'ADDR_RN'
      Origin = 'ADDR_RN'
      Size = 50
    end
    object quSelOrgDeclADDR_CITY: TStringField
      FieldName = 'ADDR_CITY'
      Origin = 'ADDR_CITY'
      Size = 50
    end
    object quSelOrgDeclADDR_NP: TStringField
      FieldName = 'ADDR_NP'
      Origin = 'ADDR_NP'
      Size = 50
    end
    object quSelOrgDeclADDR_STR: TStringField
      FieldName = 'ADDR_STR'
      Origin = 'ADDR_STR'
      Size = 100
    end
    object quSelOrgDeclADDR_D: TStringField
      FieldName = 'ADDR_D'
      Origin = 'ADDR_D'
      Size = 10
    end
    object quSelOrgDeclADDR_KORP: TStringField
      FieldName = 'ADDR_KORP'
      Origin = 'ADDR_KORP'
      Size = 10
    end
    object quSelOrgDeclADDR_L: TStringField
      FieldName = 'ADDR_L'
      Origin = 'ADDR_L'
      Size = 10
    end
    object quSelOrgDeclADDR_KV: TStringField
      FieldName = 'ADDR_KV'
      Origin = 'ADDR_KV'
      Size = 10
    end
  end
  object quSelLicOrg: TFDQuery
    Connection = dmR.FDConnection
    SQL.Strings = (
      'SELECT [IDORG]'
      '      ,[ID]'
      '      ,[VIDD]'
      '      ,[SERNUM]'
      '      ,[DATEB]'
      '      ,[DATEE]'
      '  FROM [RAR].[dbo].[DECLORGLIC]'
      '  where [IDORG]=:IDORG'
      '  order by DATEE desc')
    Left = 432
    Top = 352
    ParamData = <
      item
        Name = 'IDORG'
        DataType = ftInteger
        ParamType = ptInput
        Value = 1
      end>
    object quSelLicOrgIDORG: TIntegerField
      FieldName = 'IDORG'
      Origin = 'IDORG'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quSelLicOrgID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quSelLicOrgVIDD: TStringField
      FieldName = 'VIDD'
      Origin = 'VIDD'
      Size = 10
    end
    object quSelLicOrgSERNUM: TStringField
      FieldName = 'SERNUM'
      Origin = 'SERNUM'
      Size = 50
    end
    object quSelLicOrgDATEB: TSQLTimeStampField
      FieldName = 'DATEB'
      Origin = 'DATEB'
    end
    object quSelLicOrgDATEE: TSQLTimeStampField
      FieldName = 'DATEE'
      Origin = 'DATEE'
    end
  end
  object quSelOrgsOb: TFDQuery
    Connection = dmR.FDConnection
    SQL.Strings = (
      'declare @IDHEAD int = :IDH'
      ''
      'SELECT org.[ID]'
      '      ,org.[MAIN]'
      '      ,org.[FSRARID]'
      '      ,org.[NAME]'
      '      ,org.[PHONE]'
      '      ,org.[EMAILORG]'
      '      ,org.[INN]'
      '      ,org.[KPP]'
      '      ,org.[DIR1]'
      '      ,org.[DIR2]'
      '      ,org.[DIR3]'
      '      ,org.[GB1]'
      '      ,org.[GB2]'
      '      ,org.[GB3]'
      '      ,org.[ADDR_CC]'
      '      ,org.[ADDR_IND]'
      '      ,org.[ADDR_REG]'
      '      ,org.[ADDR_RN]'
      '      ,org.[ADDR_CITY]'
      '      ,org.[ADDR_NP]'
      '      ,org.[ADDR_STR]'
      '      ,org.[ADDR_D]'
      '      ,org.[ADDR_KORP]'
      '      ,org.[ADDR_L]'
      '      ,org.[ADDR_KV]'
      
        #9'  ,(Select count(*) from [dbo].[ADECL_SP1] where [IDHEAD]=@IDHE' +
        'AD and [IDORG]=org.[ID] and ([REMNB]+[QIN1]+[QIN2]+[QIN3]+[QIN4]' +
        '+[QIN5]+[QOUT1]+[QOUT2]+[QOUT3]+[QOUT4]+[REMNE])>0.01) as QUANTR' +
        'EC'
      '  FROM [dbo].[DECLORG] org'
      
        '  where org.[INN] = (Select [INN] from [dbo].[DECLORG] where ID ' +
        '= (Select [MASTERORG] from [dbo].[ADECL_HD] where ID=@IDHEAD))'
      '  order by ID')
    Left = 360
    Top = 424
    ParamData = <
      item
        Name = 'IDH'
        DataType = ftInteger
        ParamType = ptInput
        Value = 13
      end>
    object quSelOrgsObID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      Required = True
    end
    object quSelOrgsObMAIN: TIntegerField
      FieldName = 'MAIN'
      Origin = 'MAIN'
    end
    object quSelOrgsObFSRARID: TStringField
      FieldName = 'FSRARID'
      Origin = 'FSRARID'
      Size = 50
    end
    object quSelOrgsObNAME: TStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      Size = 200
    end
    object quSelOrgsObPHONE: TStringField
      FieldName = 'PHONE'
      Origin = 'PHONE'
      Size = 50
    end
    object quSelOrgsObEMAILORG: TStringField
      FieldName = 'EMAILORG'
      Origin = 'EMAILORG'
      Size = 100
    end
    object quSelOrgsObINN: TStringField
      FieldName = 'INN'
      Origin = 'INN'
    end
    object quSelOrgsObKPP: TStringField
      FieldName = 'KPP'
      Origin = 'KPP'
    end
    object quSelOrgsObDIR1: TStringField
      FieldName = 'DIR1'
      Origin = 'DIR1'
      Size = 100
    end
    object quSelOrgsObDIR2: TStringField
      FieldName = 'DIR2'
      Origin = 'DIR2'
      Size = 100
    end
    object quSelOrgsObDIR3: TStringField
      FieldName = 'DIR3'
      Origin = 'DIR3'
      Size = 100
    end
    object quSelOrgsObGB1: TStringField
      FieldName = 'GB1'
      Origin = 'GB1'
      Size = 100
    end
    object quSelOrgsObGB2: TStringField
      FieldName = 'GB2'
      Origin = 'GB2'
      Size = 100
    end
    object quSelOrgsObGB3: TStringField
      FieldName = 'GB3'
      Origin = 'GB3'
      Size = 100
    end
    object quSelOrgsObADDR_CC: TStringField
      FieldName = 'ADDR_CC'
      Origin = 'ADDR_CC'
      Size = 10
    end
    object quSelOrgsObADDR_IND: TStringField
      FieldName = 'ADDR_IND'
      Origin = 'ADDR_IND'
      Size = 10
    end
    object quSelOrgsObADDR_REG: TStringField
      FieldName = 'ADDR_REG'
      Origin = 'ADDR_REG'
      Size = 5
    end
    object quSelOrgsObADDR_RN: TStringField
      FieldName = 'ADDR_RN'
      Origin = 'ADDR_RN'
      Size = 50
    end
    object quSelOrgsObADDR_CITY: TStringField
      FieldName = 'ADDR_CITY'
      Origin = 'ADDR_CITY'
      Size = 50
    end
    object quSelOrgsObADDR_NP: TStringField
      FieldName = 'ADDR_NP'
      Origin = 'ADDR_NP'
      Size = 50
    end
    object quSelOrgsObADDR_STR: TStringField
      FieldName = 'ADDR_STR'
      Origin = 'ADDR_STR'
      Size = 100
    end
    object quSelOrgsObADDR_D: TStringField
      FieldName = 'ADDR_D'
      Origin = 'ADDR_D'
      Size = 10
    end
    object quSelOrgsObADDR_KORP: TStringField
      FieldName = 'ADDR_KORP'
      Origin = 'ADDR_KORP'
      Size = 10
    end
    object quSelOrgsObADDR_L: TStringField
      FieldName = 'ADDR_L'
      Origin = 'ADDR_L'
      Size = 10
    end
    object quSelOrgsObADDR_KV: TStringField
      FieldName = 'ADDR_KV'
      Origin = 'ADDR_KV'
      Size = 10
    end
    object quSelOrgsObQUANTREC: TIntegerField
      FieldName = 'QUANTREC'
      Origin = 'QUANTREC'
      ReadOnly = True
    end
  end
  object quSelVidsOb: TFDQuery
    Connection = dmR.FDConnection
    SQL.Strings = (
      'SELECT [AVID]'
      '  FROM [RAR].[dbo].[ADECL_SP1]'
      
        '  where [IDHEAD]=:IDH and [IDORG]=:IDORG and ([REMNB]+[QIN1]+[QI' +
        'N2]+[QIN3]+[QIN4]+[QIN5]+[QOUT1]+[QOUT2]+[QOUT3]+[QOUT4]+[REMNE]' +
        ')>0.01 '
      '  group by [AVID]'
      '  order by [AVID] ')
    Left = 440
    Top = 424
    ParamData = <
      item
        Name = 'IDH'
        DataType = ftInteger
        ParamType = ptInput
        Value = 13
      end
      item
        Name = 'IDORG'
        DataType = ftInteger
        ParamType = ptInput
        Value = 1
      end>
    object quSelVidsObAVID: TIntegerField
      FieldName = 'AVID'
      Origin = 'AVID'
    end
  end
  object quSelProdsOb: TFDQuery
    Connection = dmR.FDConnection
    SQL.Strings = (
      'SELECT [PRODID]'
      '      ,[REMNB]'
      '      ,[QIN1]'
      '      ,[QIN2]'
      '      ,[QIN3]'
      '      ,([QIN1]+[QIN2]+[QIN3]) as QINIT1'
      '      ,[QIN4]'
      '      ,[QIN5]'
      '      ,[QIN6]'
      '      ,([QIN1]+[QIN2]+[QIN3]+[QIN4]+[QIN5]+[QIN6]) as QINIT2'
      '      ,[QOUT1]'
      '      ,[QOUT2]'
      '      ,[QOUT3]'
      '      ,[QOUT4]'
      '      ,([QOUT1]+[QOUT2]+[QOUT3]+[QOUT4]) as QOUTIT1'
      '      ,[REMNE]'
      '  FROM [dbo].[ADECL_SP1]'
      
        '  where [IDHEAD]=:IDH and [IDORG]=:IDORG and ([REMNB]+[QIN1]+[QI' +
        'N2]+[QIN3]+[QIN4]+[QIN5]+[QOUT1]+[QOUT2]+[QOUT3]+[QOUT4]+[REMNE]' +
        ')>0.01 '
      '  and [AVID]=:AVID'
      '  order by [PRODID]')
    Left = 520
    Top = 424
    ParamData = <
      item
        Name = 'IDH'
        DataType = ftInteger
        ParamType = ptInput
        Value = 13
      end
      item
        Name = 'IDORG'
        DataType = ftInteger
        ParamType = ptInput
        Value = 1
      end
      item
        Name = 'AVID'
        DataType = ftInteger
        ParamType = ptInput
        Value = 200
      end>
    object quSelProdsObPRODID: TStringField
      FieldName = 'PRODID'
      Origin = 'PRODID'
      Size = 50
    end
    object quSelProdsObREMNB: TSingleField
      FieldName = 'REMNB'
      Origin = 'REMNB'
    end
    object quSelProdsObQIN1: TSingleField
      FieldName = 'QIN1'
      Origin = 'QIN1'
    end
    object quSelProdsObQIN2: TSingleField
      FieldName = 'QIN2'
      Origin = 'QIN2'
    end
    object quSelProdsObQIN3: TSingleField
      FieldName = 'QIN3'
      Origin = 'QIN3'
    end
    object quSelProdsObQIN4: TSingleField
      FieldName = 'QIN4'
      Origin = 'QIN4'
    end
    object quSelProdsObQIN5: TSingleField
      FieldName = 'QIN5'
      Origin = 'QIN5'
    end
    object quSelProdsObQIN6: TSingleField
      FieldName = 'QIN6'
      Origin = 'QIN6'
    end
    object quSelProdsObQOUT1: TSingleField
      FieldName = 'QOUT1'
      Origin = 'QOUT1'
    end
    object quSelProdsObQOUT2: TSingleField
      FieldName = 'QOUT2'
      Origin = 'QOUT2'
    end
    object quSelProdsObQOUT3: TSingleField
      FieldName = 'QOUT3'
      Origin = 'QOUT3'
    end
    object quSelProdsObQOUT4: TSingleField
      FieldName = 'QOUT4'
      Origin = 'QOUT4'
    end
    object quSelProdsObREMNE: TSingleField
      FieldName = 'REMNE'
      Origin = 'REMNE'
    end
    object quSelProdsObQINIT1: TSingleField
      FieldName = 'QINIT1'
      Origin = 'QINIT1'
      ReadOnly = True
    end
    object quSelProdsObQINIT2: TSingleField
      FieldName = 'QINIT2'
      Origin = 'QINIT2'
      ReadOnly = True
    end
    object quSelProdsObQOUTIT1: TSingleField
      FieldName = 'QOUTIT1'
      Origin = 'QOUTIT1'
      ReadOnly = True
    end
  end
  object quSelPostOb: TFDQuery
    Connection = dmR.FDConnection
    SQL.Strings = (
      'Select [CLIENTID]'
      
        '       ,(Select top 1 [IDL] from [dbo].[ALIC] where [IDCLI]=t1.[' +
        'CLIENTID] and [IDATEB]<=t1.DOCDATE and [IDATEE]>=t1.DOCDATE orde' +
        'r by [IDATEE] desc) as IDLIC'
      'from'
      '  (SELECT sp2.[CLIENTID] as CLIENTID'
      '  , min([DOCDATE]) as DOCDATE'
      '  FROM [dbo].[ADECL_SP2] sp2'
      
        '  where sp2.[IDHEAD]=:IDH and sp2.[IDORG]=:IDORG and sp2.[AVID]=' +
        ':AVID and sp2.[PRODID]=:IDPROD and sp2.[QIN]>0'
      '  group by sp2.[CLIENTID]) as t1')
    Left = 608
    Top = 424
    ParamData = <
      item
        Name = 'IDH'
        DataType = ftInteger
        ParamType = ptInput
        Value = 13
      end
      item
        Name = 'IDORG'
        DataType = ftInteger
        ParamType = ptInput
        Value = 3
      end
      item
        Name = 'AVID'
        DataType = ftInteger
        ParamType = ptInput
        Value = 200
      end
      item
        Name = 'IDPROD'
        DataType = ftString
        ParamType = ptInput
        Value = '010000000325'
      end>
    object quSelPostObCLIENTID: TStringField
      FieldName = 'CLIENTID'
      Origin = 'CLIENTID'
      Size = 50
    end
    object quSelPostObIDLIC: TIntegerField
      FieldName = 'IDLIC'
      Origin = 'IDLIC'
      ReadOnly = True
    end
  end
  object quSelDocOb: TFDQuery
    Connection = dmR.FDConnection
    SQL.Strings = (
      'SELECT sp2.[DOCDATE]'
      '      ,sp2.[DOCNUM]'
      '      ,sp2.[GTD]'
      '      ,sp2.[QIN]'
      '  FROM [dbo].[ADECL_SP2] sp2'
      
        '  where sp2.[IDHEAD]=:IDH and sp2.[IDORG]=:IDORG and sp2.[AVID]=' +
        ':AVID and sp2.[PRODID]=:IDPROD and sp2.[QIN]<>0 and sp2.[CLIENTI' +
        'D]=:IDCLI'
      '  order by sp2.[DOCDATE]')
    Left = 680
    Top = 424
    ParamData = <
      item
        Name = 'IDH'
        DataType = ftInteger
        ParamType = ptInput
        Value = 13
      end
      item
        Name = 'IDORG'
        DataType = ftInteger
        ParamType = ptInput
        Value = 3
      end
      item
        Name = 'AVID'
        DataType = ftInteger
        ParamType = ptInput
        Value = 200
      end
      item
        Name = 'IDPROD'
        DataType = ftString
        ParamType = ptInput
        Value = '010000000325'
      end
      item
        Name = 'IDCLI'
        DataType = ftString
        ParamType = ptInput
        Value = '010000006633'
      end>
    object quSelDocObDOCDATE: TSQLTimeStampField
      FieldName = 'DOCDATE'
      Origin = 'DOCDATE'
    end
    object quSelDocObDOCNUM: TStringField
      FieldName = 'DOCNUM'
      Origin = 'DOCNUM'
      Size = 50
    end
    object quSelDocObGTD: TStringField
      FieldName = 'GTD'
      Origin = 'GTD'
      Size = 200
    end
    object quSelDocObQIN: TSingleField
      FieldName = 'QIN'
      Origin = 'QIN'
    end
  end
end
