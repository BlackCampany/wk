object fmDocSpecVn: TfmDocSpecVn
  Left = 0
  Top = 0
  Caption = #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1103' '#1087#1077#1088#1077#1084#1077#1097#1077#1085#1080#1103
  ClientHeight = 555
  ClientWidth = 1127
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1127
    Height = 127
    BarManager = dxBarManager1
    Style = rs2010
    ColorSchemeAccent = rcsaOrange
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1103' '#1087#1077#1088#1077#1084#1077#1097#1077#1085#1080#1103
      Groups = <
        item
          Caption = 
            '         '#1059#1087#1088#1072#1074#1083#1077#1085#1080#1077'                                             ' +
            '         '
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end>
      Index = 0
    end
  end
  object GridSpec: TcxGrid
    Left = 0
    Top = 127
    Width = 1127
    Height = 428
    Align = alClient
    TabOrder = 1
    LookAndFeel.Kind = lfOffice11
    LookAndFeel.NativeStyle = False
    object ViewSpecVn: TcxGridDBTableView
      PopupMenu = PopupMenu1
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.InfoPanel.Visible = True
      Navigator.Visible = True
      DataController.DataSource = dsquSpecVn
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'QUANT'
        end
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'QUANTF'
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SUMF'
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Inserting = False
      OptionsSelection.MultiSelect = True
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      object ViewSpecVnNUM: TcxGridDBColumn
        Caption = #8470' '#1087#1087
        DataBinding.FieldName = 'NUM'
        Options.Editing = False
        Width = 54
      end
      object ViewSpecVnALCCODE: TcxGridDBColumn
        Caption = #1050#1040#1055
        DataBinding.FieldName = 'ALCCODE'
        Options.Editing = False
        Width = 124
      end
      object ViewSpecVnNAME: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'NAME'
        Options.Editing = False
        Width = 365
      end
      object ViewSpecVnQUANT: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086
        DataBinding.FieldName = 'QUANT'
        PropertiesClassName = 'TcxCalcEditProperties'
      end
      object ViewSpecVnINFO_B: TcxGridDBColumn
        Caption = #1057#1087#1088#1072#1074#1082#1072' '#1041
        DataBinding.FieldName = 'INFO_B'
        Options.Editing = False
        Width = 149
      end
      object ViewSpecVnAVID: TcxGridDBColumn
        Caption = #1042#1080#1076' '#1040#1055
        DataBinding.FieldName = 'AVID'
        Options.Editing = False
        Width = 50
      end
      object ViewSpecVnVOL: TcxGridDBColumn
        Caption = #1054#1073#1098#1077#1084' '#1083'.'
        DataBinding.FieldName = 'VOL'
        Options.Editing = False
        Width = 53
      end
      object ViewSpecVnKREP: TcxGridDBColumn
        Caption = '% '#1082#1088#1077#1087'.'
        DataBinding.FieldName = 'KREP'
        Options.Editing = False
        Width = 53
      end
    end
    object LevelSpec: TcxGridLevel
      GridView = ViewSpecVn
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      #1044#1077#1081#1089#1090#1074#1080#1103)
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmR.SmallImage
    ImageOptions.LargeImages = dmR.LargeImage
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 540
    Top = 164
    DockControlHeights = (
      0
      0
      0
      0)
    object dxBarManager1Bar1: TdxBar
      Caption = #1059#1087#1088#1072#1074#1083#1077#1085#1080#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 1001
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          BeginGroup = True
          UserDefine = [udWidth]
          UserWidth = 128
          Visible = True
          ItemName = 'cxBarEditItem1'
        end
        item
          UserDefine = [udWidth]
          UserWidth = 141
          Visible = True
          ItemName = 'cxBarEditItem2'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1047#1072#1082#1088#1099#1090#1100
      CaptionButtons = <>
      DockedLeft = 462
      DockedTop = 0
      FloatLeft = 1161
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = acSave
      Category = 0
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Caption = 
        '                                                                ' +
        '                '
      Category = 0
      Hint = 
        '                                                                ' +
        '                '
      Visible = ivAlways
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = acClose
      Category = 0
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = #1053#1086#1084#1077#1088' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
      Category = 0
      Hint = #1053#1086#1084#1077#1088' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
      Visible = ivAlways
      PropertiesClassName = 'TcxTextEditProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = 'New SubItem'
      Category = 0
      Visible = ivAlways
      ItemLinks = <>
    end
    object cxBarEditItem2: TcxBarEditItem
      Caption = #1044#1072#1090#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
      Category = 0
      Hint = #1044#1072#1090#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
      Visible = ivAlways
      PropertiesClassName = 'TcxDateEditProperties'
      InternalEditValue = 42667d
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = acTestSpec
      Category = 0
    end
    object dxBarGroup1: TdxBarGroup
      Items = ()
    end
  end
  object ActionList1: TActionList
    Images = dmR.SmallImage
    Left = 648
    Top = 168
    object acClose: TAction
      Caption = #1047#1072#1082#1088#1099#1090#1100
      ImageIndex = 100
      OnExecute = acCloseExecute
    end
    object acSave: TAction
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' (Ctrl + S)'
      ImageIndex = 7
      ShortCut = 16467
      OnExecute = acSaveExecute
    end
    object acTestSpec: TAction
      Caption = #1055#1088#1086#1074#1077#1088#1082#1072' '#1089#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1080
      ImageIndex = 38
    end
    object acDelPos: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1087#1086#1079#1080#1094#1080#1102
      ImageIndex = 4
      OnExecute = acDelPosExecute
    end
    object acCreateFromShop: TAction
      Caption = #1057#1092#1086#1088#1084#1080#1088#1086#1074#1072#1090#1100' '#1076#1086#1082#1091#1084#1077#1085#1090' ('#1085#1072' '#1089#1082#1083#1072#1076') '#1087#1086' '#1074#1099#1076#1077#1083#1077#1085#1085#1099#1084' '#1087#1086#1079#1080#1094#1080#1103#1084'.'
      ImageIndex = 49
      OnExecute = acCreateFromShopExecute
    end
  end
  object quSpecVn: TFDQuery
    CachedUpdates = True
    Connection = dmR.FDConnection
    Transaction = dmR.FDTrans
    UpdateTransaction = dmR.FDTransUpdate
    UpdateObject = UpdSQL1
    SQL.Strings = (
      'declare @FSRARID varchar(50) = :RARID'
      'declare @IDATE int = :IDATE'
      'declare @SID varchar(50) = :SID'
      ''
      'SELECT sp.[FSRARID]'
      '      ,sp.[IDATE]'
      '      ,sp.[SIDHD]'
      '      ,sp.[ID]'
      '      ,sp.[NUM]'
      '      ,sp.[ALCCODE]'
      '      ,sp.[QUANT]'
      '      ,sp.[INFO_B]'
      #9'  ,ca.NAME'
      #9'  ,ca.AVID'
      #9'  ,ca.VOL'
      #9'  ,ca.KREP'
      '  FROM [dbo].[ADOCSVN_SP] sp'
      '  left join [dbo].[ACARDS] ca on ca.KAP=sp.[ALCCODE]'
      '  where sp.[FSRARID]=@FSRARID'
      '  and sp.[IDATE]=@IDATE'
      '  and sp.[SIDHD]=@SID')
    Left = 80
    Top = 352
    ParamData = <
      item
        Name = 'RARID'
        DataType = ftString
        ParamType = ptInput
        Value = '020000021354'
      end
      item
        Name = 'IDATE'
        DataType = ftInteger
        ParamType = ptInput
        Value = 42666
      end
      item
        Name = 'SID'
        DataType = ftString
        ParamType = ptInput
        Value = 'B23BABFE-4170-43B7-979D-78DA50BD88E8'
      end>
    object quSpecVnFSRARID: TStringField
      FieldName = 'FSRARID'
      Origin = 'FSRARID'
      Required = True
      Size = 50
    end
    object quSpecVnIDATE: TIntegerField
      FieldName = 'IDATE'
      Origin = 'IDATE'
      Required = True
    end
    object quSpecVnSIDHD: TStringField
      FieldName = 'SIDHD'
      Origin = 'SIDHD'
      Required = True
      Size = 50
    end
    object quSpecVnID: TLargeintField
      AutoGenerateValue = arAutoInc
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere]
      ReadOnly = True
    end
    object quSpecVnNUM: TIntegerField
      FieldName = 'NUM'
      Origin = 'NUM'
    end
    object quSpecVnALCCODE: TStringField
      FieldName = 'ALCCODE'
      Origin = 'ALCCODE'
      Size = 50
    end
    object quSpecVnQUANT: TFloatField
      FieldName = 'QUANT'
      Origin = 'QUANT'
      DisplayFormat = '0.000'
    end
    object quSpecVnINFO_B: TStringField
      FieldName = 'INFO_B'
      Origin = 'INFO_B'
      Size = 50
    end
    object quSpecVnNAME: TMemoField
      FieldName = 'NAME'
      Origin = 'NAME'
      BlobType = ftMemo
      Size = 2147483647
    end
    object quSpecVnAVID: TIntegerField
      FieldName = 'AVID'
      Origin = 'AVID'
    end
    object quSpecVnVOL: TSingleField
      FieldName = 'VOL'
      Origin = 'VOL'
      DisplayFormat = '0.000'
    end
    object quSpecVnKREP: TSingleField
      FieldName = 'KREP'
      Origin = 'KREP'
      DisplayFormat = '0.0'
    end
  end
  object UpdSQL1: TFDUpdateSQL
    Connection = dmR.FDConnection
    InsertSQL.Strings = (
      'INSERT INTO RAR.dbo.ADOCSVN_SP'
      '(QUANT)'
      'VALUES (:NEW_QUANT);'
      'SELECT SCOPE_IDENTITY() AS ID, QUANT'
      'FROM RAR.dbo.ADOCSVN_SP'
      
        'WHERE FSRARID = :NEW_FSRARID AND IDATE = :NEW_IDATE AND SIDHD = ' +
        ':NEW_SIDHD AND '
      '  ID = SCOPE_IDENTITY()')
    ModifySQL.Strings = (
      'UPDATE RAR.dbo.ADOCSVN_SP'
      'SET QUANT = :NEW_QUANT'
      
        'WHERE FSRARID = :OLD_FSRARID AND IDATE = :OLD_IDATE AND SIDHD = ' +
        ':OLD_SIDHD AND '
      '  ID = :OLD_ID;'
      'SELECT QUANT'
      'FROM RAR.dbo.ADOCSVN_SP'
      
        'WHERE FSRARID = :NEW_FSRARID AND IDATE = :NEW_IDATE AND SIDHD = ' +
        ':NEW_SIDHD AND '
      '  ID = :NEW_ID')
    DeleteSQL.Strings = (
      'DELETE FROM RAR.dbo.ADOCSVN_SP'
      
        'WHERE FSRARID = :OLD_FSRARID AND IDATE = :OLD_IDATE AND SIDHD = ' +
        ':OLD_SIDHD AND '
      '  ID = :OLD_ID')
    FetchRowSQL.Strings = (
      
        'SELECT FSRARID, IDATE, SIDHD, SCOPE_IDENTITY() AS ID, NUM, ALCCO' +
        'DE, '
      '  QUANT, INFO_B'
      'FROM RAR.dbo.ADOCSVN_SP'
      
        'WHERE FSRARID = :FSRARID AND IDATE = :IDATE AND SIDHD = :SIDHD A' +
        'ND '
      '  ID = :ID')
    Left = 152
    Top = 352
  end
  object dsquSpecVn: TDataSource
    DataSet = quSpecVn
    Left = 80
    Top = 416
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 232
    Top = 200
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svFont]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
    end
    object cxStyle2: TcxStyle
    end
  end
  object PopupMenu1: TPopupMenu
    Images = dmR.SmallImage
    Left = 432
    Top = 272
    object N1: TMenuItem
      Action = acDelPos
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object N3: TMenuItem
      Action = acCreateFromShop
    end
  end
  object quE: TFDQuery
    Connection = dmR.FDConnection
    Left = 80
    Top = 280
  end
end
