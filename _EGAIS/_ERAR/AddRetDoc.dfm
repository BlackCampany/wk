object fmAddRetDoc: TfmAddRetDoc
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1076#1086#1082#1091#1084#1077#1085#1090' '#1074#1086#1079#1074#1088#1072#1090#1072
  ClientHeight = 208
  ClientWidth = 602
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object cxLabel1: TcxLabel
    Left = 16
    Top = 16
    Caption = #1053#1086#1084#1077#1088' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
  end
  object cxLabel2: TcxLabel
    Left = 16
    Top = 48
    Caption = #1044#1072#1090#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
  end
  object cxLabel3: TcxLabel
    Left = 16
    Top = 79
    Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
  end
  object Panel1: TPanel
    Left = 0
    Top = 124
    Width = 602
    Height = 84
    Align = alBottom
    BevelKind = bkFlat
    BevelOuter = bvLowered
    TabOrder = 3
    object Button1: TButton
      Left = 136
      Top = 20
      Width = 113
      Height = 41
      Caption = #1054#1082
      ModalResult = 1
      TabOrder = 0
    end
    object Button2: TButton
      Left = 352
      Top = 20
      Width = 113
      Height = 41
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
    end
  end
  object cxTextEdit1: TcxTextEdit
    Left = 144
    Top = 14
    TabOrder = 4
    Text = 'cxTextEdit1'
    Width = 169
  end
  object cxDateEdit1: TcxDateEdit
    Left = 144
    Top = 46
    TabOrder = 5
    Width = 169
  end
  object cxLookupComboBox1: TcxLookupComboBox
    Left = 144
    Top = 77
    Properties.DropDownAutoSize = True
    Properties.DropDownSizeable = True
    Properties.DropDownWidth = 500
    Properties.KeyFieldNames = 'CLIFROM'
    Properties.ListColumns = <
      item
        Caption = #1055#1086#1083#1085#1086#1077' '#1085#1072#1079#1074#1072#1085#1080#1077
        Width = 400
        FieldName = 'FULLNAME'
      end
      item
        Caption = #1050#1086#1088#1086#1090#1082#1086#1077' '#1085#1072#1079#1074#1072#1085#1080#1077
        Width = 250
        FieldName = 'NAME'
      end
      item
        Caption = #1050#1086#1076
        Width = 80
        FieldName = 'CLIFROM'
      end
      item
        Caption = #1048#1053#1053
        Width = 80
        FieldName = 'CLIENTINN'
      end
      item
        Caption = #1050#1055#1055
        Width = 80
        FieldName = 'CLIENTKPP'
      end>
    Properties.ListFieldIndex = 1
    Properties.ListSource = dsquListCli
    Properties.OEMConvert = True
    TabOrder = 6
    Width = 377
  end
  object quListCli: TFDQuery
    Connection = dmR.FDConnection
    SQL.Strings = (
      
        'SELECT hd.[CLIFROM], cast(cli.FULLNAME as Varchar(200)) as FULLN' +
        'AME, cast(cli.NAME as Varchar(200)) as NAME , cli.CLIENTINN, cli' +
        '.CLIENTKPP  '
      'FROM [dbo].[ADOCSIN_HD] hd'
      'left join [dbo].[ACLIENTS] cli on cli.[CLIENTID]=hd.CLIFROM'
      
        'group by hd.[CLIFROM], cli.FULLNAME, cli.NAME, cli.CLIENTINN, cl' +
        'i.CLIENTKPP  ')
    Left = 424
    Top = 12
    object quListCliCLIFROM: TStringField
      FieldName = 'CLIFROM'
      Origin = 'CLIFROM'
      Size = 50
    end
    object quListCliFULLNAME: TStringField
      FieldName = 'FULLNAME'
      Origin = 'FULLNAME'
      ReadOnly = True
      Size = 200
    end
    object quListCliNAME: TStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      ReadOnly = True
      Size = 200
    end
    object quListCliCLIENTINN: TStringField
      FieldName = 'CLIENTINN'
      Origin = 'CLIENTINN'
    end
    object quListCliCLIENTKPP: TStringField
      FieldName = 'CLIENTKPP'
      Origin = 'CLIENTKPP'
    end
  end
  object dsquListCli: TDataSource
    DataSet = quListCli
    Left = 488
    Top = 16
  end
end
