object fmDocInv: TfmDocInv
  Left = 0
  Top = 0
  Caption = #1044#1086#1082#1091#1084#1077#1085#1090#1099' '#1080#1085#1074#1077#1085#1090#1072#1088#1080#1079#1072#1094#1080#1080
  ClientHeight = 558
  ClientWidth = 1272
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1272
    Height = 127
    ApplicationButton.Visible = False
    BarManager = dxBarManager1
    Style = rs2010
    ColorSchemeAccent = rcsaOrange
    ColorSchemeName = 'Blue'
    SupportNonClientDrawing = True
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1044#1086#1082#1091#1084#1077#1085#1090#1099' '#1080#1085#1074#1077#1085#1090#1072#1088#1080#1079#1072#1094#1080#1080
      Groups = <
        item
          ToolbarName = 'bmApplyDate'
        end
        item
          ToolbarName = 'bmClose'
        end>
      Index = 0
    end
  end
  object GridDocInv: TcxGrid
    Left = 0
    Top = 127
    Width = 1272
    Height = 431
    Align = alClient
    TabOrder = 5
    LevelTabs.Style = 8
    LookAndFeel.Kind = lfOffice11
    LookAndFeel.NativeStyle = False
    object ViewDocInv: TcxGridDBTableView
      PopupMenu = pmDocInv
      OnDblClick = ViewDocInvDblClick
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.First.Visible = False
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.InfoPanel.Visible = True
      Navigator.Visible = True
      DataController.DataSource = dmR.dsquDocsInv
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.MultiSelect = True
      object ViewDocInvFSRARID: TcxGridDBColumn
        DataBinding.FieldName = 'FSRARID'
        Width = 121
      end
      object ViewDocInvDOCDATE: TcxGridDBColumn
        Caption = #1044#1072#1090#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
        DataBinding.FieldName = 'DOCDATE'
        Width = 164
      end
      object ViewDocInvID: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
        DataBinding.FieldName = 'ID'
      end
      object ViewDocInvSTYPE: TcxGridDBColumn
        Caption = #1058#1080#1087
        DataBinding.FieldName = 'STYPE'
        Width = 96
      end
      object ViewDocInvFIXDATE: TcxGridDBColumn
        Caption = #1044#1072#1090#1072' '#1086#1089#1090#1072#1090#1082#1086#1074
        DataBinding.FieldName = 'FIXDATE'
        Width = 213
      end
      object ViewDocInvXML: TcxGridDBColumn
        DataBinding.FieldName = 'XML'
        Width = 60
      end
      object ViewDocInvIDREPLY: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1086#1090#1074#1077#1090#1072' (IDREPLY)'
        DataBinding.FieldName = 'IDREPLY'
        Width = 88
      end
    end
    object LevelDocInv: TcxGridLevel
      Caption = #1053#1077#1086#1073#1088#1072#1073#1086#1090#1072#1085#1085#1099#1077' '#1058#1058#1053
      GridView = ViewDocInv
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      #1044#1077#1081#1089#1090#1074#1080#1103)
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmR.SmallImage
    ImageOptions.LargeImages = dmR.LargeImage
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 708
    Top = 204
    DockControlHeights = (
      0
      0
      0
      0)
    object bmApplyDate: TdxBar
      Caption = #1044#1077#1081#1089#1090#1074#1080#1103
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 778
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          UserDefine = [udWidth]
          UserWidth = 75
          Visible = True
          ItemName = 'deDateBeg'
        end
        item
          UserDefine = [udWidth]
          UserWidth = 74
          Visible = True
          ItemName = 'deDateEnd'
        end
        item
          Visible = True
          ItemName = 'btnDone'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton11'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object bmClose: TdxBar
      Caption = #1047#1072#1082#1088#1099#1090#1100
      CaptionButtons = <>
      DockedLeft = 424
      DockedTop = 0
      FloatLeft = 778
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'btnClose'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object deDateBeg: TdxBarDateCombo
      Caption = 'C  '
      Category = 0
      Hint = 'C  '
      Visible = ivAlways
      OnChange = deDateBegChange
      ImageIndex = 25
      ShowDayText = False
    end
    object deDateEnd: TdxBarDateCombo
      Caption = #1087#1086
      Category = 0
      Hint = #1087#1086
      Visible = ivAlways
      OnChange = deDateBegChange
      ImageIndex = 25
      ShowDayText = False
    end
    object btnDone: TdxBarLargeButton
      Action = acRefresh
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Caption = #1059#1058#1052' '#1086#1073#1084#1077#1085
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 38
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Caption = #1059#1058#1052' '#1080#1089#1090#1086#1088#1080#1103
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 130
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Caption = #1059#1058#1052' '#1072#1088#1093#1080#1074
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 21
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = #1048#1053#1053' '
      Category = 0
      Hint = #1048#1053#1053' '
      Visible = ivAlways
      PropertiesClassName = 'TcxTextEditProperties'
      BarStyleDropDownButton = False
      Properties.AutoSelect = False
      Properties.IncrementalSearch = False
      Properties.ValidationOptions = [evoAllowLoseFocus]
      InternalEditValue = ''
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Caption = #1047#1072#1087#1088#1086#1089' '#1072#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090#1072' '#1087#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1103
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 16
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Caption = #1056#1072#1079#1088#1077#1096#1080#1090#1100' '#1086#1090#1087#1088#1072#1074#1082#1091'  '
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 1
    end
    object beiPoint: TcxBarEditItem
      Caption = #1058#1086#1095#1082#1072' '#1088#1077#1072#1083'.'
      Category = 0
      Hint = #1058#1086#1095#1082#1072' '#1088#1077#1072#1083'.'
      Visible = ivAlways
      PropertiesClassName = 'TcxLookupComboBoxProperties'
      Properties.DropDownListStyle = lsFixedList
      Properties.DropDownRows = 40
      Properties.DropDownSizeable = True
      Properties.DropDownWidth = 350
      Properties.KeyFieldNames = 'ISHOP'
      Properties.ListColumns = <
        item
          Caption = #1052#1072#1075#1072#1079#1080#1085
          FieldName = 'Name'
        end
        item
          Caption = #1052#1072#1075'.'#8470
          FieldName = 'ISHOP'
        end
        item
          FieldName = 'RARID'
        end>
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Caption = #1052#1050#1088#1080#1089#1090#1072#1083
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 161
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Caption = #1055#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1086
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 162
    end
    object beiGetTTN: TcxBarEditItem
      Caption = #8470' TTN'
      Category = 0
      Hint = #8470' TTN'
      Visible = ivAlways
      PropertiesClassName = 'TcxTextEditProperties'
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1047#1072#1087#1088#1086#1089#1080#1090#1100' '#1076#1086#1082#1091#1084#1077#1085#1090' '#1074' '#1045#1043#1040#1048#1057
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 50
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = #1047#1072#1087#1088#1086#1089#1080#1090#1100' '#1085#1077#1086#1073#1088#1072#1073#1086#1090#1072#1085#1085#1099#1077' '#1058#1058#1053
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 164
    end
    object btnClose: TdxBarLargeButton
      Action = acClose
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Caption = #1047#1072#1087#1088#1086#1089' '#1089#1087#1088#1072#1074#1082#1080' 1'
      Category = 0
      Visible = ivNever
      LargeImageIndex = 164
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Caption = #1047#1072#1087#1088#1086#1089' '#1086#1089#1090#1072#1090#1082#1086#1074
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 164
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Caption = #1047#1072#1087#1088#1086#1089' '#1085#1077#1095#1080#1090#1072#1077#1084#1099#1093' '#1096#1090#1088#1080#1093#1082#1086#1076#1086#1074
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 34
      ShortCut = 49218
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Caption = #1044#1086#1082#1091#1084#1077#1085#1090#1099' '#1062#1041
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 167
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Action = acGetRemReg1
      Category = 0
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = acGetRemReg2
      Category = 0
    end
    object dxBarGroup1: TdxBarGroup
      Items = ()
    end
  end
  object ActionList1: TActionList
    Images = dmR.SmallImage
    Left = 560
    Top = 228
    object acRefresh: TAction
      Category = #1060#1086#1088#1084#1072
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100' (F5)'
      Hint = #1054#1073#1085#1086#1074#1080#1090#1100' (F5)'
      ImageIndex = 5
      ShortCut = 116
      OnExecute = acRefreshExecute
    end
    object acClose: TAction
      Category = #1060#1086#1088#1084#1072
      Caption = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 100
      OnExecute = acCloseExecute
    end
    object acSelectDate: TAction
      Category = #1060#1086#1088#1084#1072
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      Hint = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      ImageIndex = 1
      OnExecute = acSelectDateExecute
    end
    object acExpExcel: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1069#1082#1089#1087#1086#1088#1090' '#1074' Excel'
      ImageIndex = 76
      OnExecute = acExpExcelExecute
    end
    object acGetRemReg1: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1047#1072#1087#1088#1086#1089#1080#1090#1100' '#1086#1089#1090#1072#1090#1082#1080' 1 '#1088#1077#1075#1080#1089#1090#1088
      ImageIndex = 164
      OnExecute = acGetRemReg1Execute
    end
    object acGetRemReg2: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1047#1072#1087#1088#1086#1089#1080#1090#1100' '#1086#1089#1090#1072#1090#1082#1080' 2 '#1088#1077#1075#1080#1089#1090#1088
      ImageIndex = 164
      OnExecute = acGetRemReg2Execute
    end
    object acCreateVnDoc: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1057#1086#1079#1076#1072#1090#1100' '#1076#1086#1082#1091#1084#1077#1085#1090' '#1085#1072' '#1087#1077#1088#1077#1084#1077#1097#1077#1085#1080#1077' (1->2)'
      ImageIndex = 130
      OnExecute = acCreateVnDocExecute
    end
    object acSetFactQuant: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1047#1072#1087#1086#1083#1085#1080#1090#1100' '#1092#1072#1082#1090#1080#1095#1077#1089#1082#1086#1077' '#1082#1086#1083#1080#1095#1077#1089#1090#1074#1086
      ImageIndex = 60
      OnExecute = acSetFactQuantExecute
    end
    object acCreateCorr: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1057#1092#1086#1088#1084#1080#1088#1086#1074#1072#1090#1100' '#1082#1086#1088#1088#1077#1082#1090#1080#1088#1091#1102#1097#1080#1077' '#1076#1086#1082#1091#1084#1077#1085#1090#1099
      ImageIndex = 70
      OnExecute = acCreateCorrExecute
    end
    object acCreateCopyWithOutBeer: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1057#1086#1079#1076#1072#1090#1100' '#1082#1086#1087#1080#1102' '#1073#1077#1079' '#1087#1080#1074#1072
      ImageIndex = 21
      OnExecute = acCreateCopyWithOutBeerExecute
    end
  end
  object pmDocInv: TPopupMenu
    Images = dmR.SmallImage
    Left = 856
    Top = 272
    object N1: TMenuItem
      Action = acCreateVnDoc
    end
    object N2: TMenuItem
      Action = acSetFactQuant
    end
    object N3: TMenuItem
      Action = acCreateCorr
    end
    object N4: TMenuItem
      Action = acCreateCopyWithOutBeer
    end
  end
end
