object fmDocSpecCorr: TfmDocSpecCorr
  Left = 0
  Top = 0
  Caption = #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1103' '#1076#1086#1082#1091#1084#1077#1085#1090#1072' '#1082#1086#1088#1088#1077#1082#1094#1080#1080' '#1086#1089#1090#1072#1090#1082#1086#1074
  ClientHeight = 641
  ClientWidth = 1277
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1277
    Height = 127
    BarManager = dxBarManager1
    Style = rs2010
    ColorSchemeAccent = rcsaOrange
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1103' '#1082#1086#1088#1088#1077#1082#1094#1080#1080
      Groups = <
        item
          Caption = 
            '         '#1059#1087#1088#1072#1074#1083#1077#1085#1080#1077'                                             ' +
            '         '
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end>
      Index = 0
    end
  end
  object GridSpec: TcxGrid
    Left = 0
    Top = 127
    Width = 1277
    Height = 514
    Align = alClient
    TabOrder = 1
    LookAndFeel.Kind = lfOffice11
    LookAndFeel.NativeStyle = False
    object ViewSpecCorr: TcxGridDBTableView
      PopupMenu = PopupMenu1
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.InfoPanel.Visible = True
      Navigator.Visible = True
      DataController.DataSource = dsquSpecCorr
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'QUANT'
        end
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'QUANTF'
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SUMF'
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Inserting = False
      OptionsSelection.MultiSelect = True
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      object ViewSpecCorrFSRARID: TcxGridDBColumn
        DataBinding.FieldName = 'FSRARID'
        Visible = False
        Options.Editing = False
        Width = 130
      end
      object ViewSpecCorrIDATE: TcxGridDBColumn
        Caption = #1044#1072#1090#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
        DataBinding.FieldName = 'IDATE'
        PropertiesClassName = 'TcxDateEditProperties'
        Visible = False
        Options.Editing = False
        Width = 101
      end
      object ViewSpecCorrSIDHD: TcxGridDBColumn
        DataBinding.FieldName = 'SIDHD'
        Visible = False
        Options.Editing = False
        Width = 139
      end
      object ViewSpecCorrID: TcxGridDBColumn
        DataBinding.FieldName = 'ID'
        Visible = False
        Options.Editing = False
        Width = 42
      end
      object ViewSpecCorrNUM: TcxGridDBColumn
        Caption = #8470' '#1087#1087
        DataBinding.FieldName = 'NUM'
        Options.Editing = False
        Width = 42
      end
      object ViewSpecCorrALCCODE: TcxGridDBColumn
        Caption = #1050#1040#1055
        DataBinding.FieldName = 'ALCCODE'
        Options.Editing = False
        Width = 165
      end
      object ViewSpecCorrQUANT: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086
        DataBinding.FieldName = 'QUANT'
        Width = 75
      end
      object ViewSpecCorrNAME: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'NAME'
        Options.Editing = False
        Width = 413
      end
      object ViewSpecCorrVOL: TcxGridDBColumn
        Caption = #1054#1073#1098#1077#1084
        DataBinding.FieldName = 'VOL'
        Options.Editing = False
      end
      object ViewSpecCorrKREP: TcxGridDBColumn
        Caption = #1050#1088#1077#1087#1086#1089#1090#1100
        DataBinding.FieldName = 'KREP'
        Options.Editing = False
      end
      object ViewSpecCorrAVID: TcxGridDBColumn
        Caption = #1042#1080#1076' '#1040#1055
        DataBinding.FieldName = 'AVID'
        Options.Editing = False
      end
      object ViewSpecCorrPRODID: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1087#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1103
        DataBinding.FieldName = 'PRODID'
        Options.Editing = False
        Width = 109
      end
      object ViewSpecCorrFULLNAMECLI: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077' '#1087#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1103
        DataBinding.FieldName = 'FULLNAMECLI'
        Options.Editing = False
        Width = 130
      end
      object ViewSpecCorrPRODINN: TcxGridDBColumn
        Caption = #1048#1053#1053
        DataBinding.FieldName = 'PRODINN'
        Options.Editing = False
      end
      object ViewSpecCorrPRODKPP: TcxGridDBColumn
        Caption = #1050#1055#1055
        DataBinding.FieldName = 'PRODKPP'
        Options.Editing = False
      end
      object ViewSpecCorrCLITYPE: TcxGridDBColumn
        Caption = #1058#1080#1087' '#1087#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1103
        DataBinding.FieldName = 'CLITYPE'
        Options.Editing = False
        Width = 60
      end
    end
    object LevelSpec: TcxGridLevel
      GridView = ViewSpecCorr
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      #1044#1077#1081#1089#1090#1074#1080#1103)
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmR.SmallImage
    ImageOptions.LargeImages = dmR.LargeImage
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 540
    Top = 164
    DockControlHeights = (
      0
      0
      0
      0)
    object dxBarManager1Bar1: TdxBar
      Caption = #1059#1087#1088#1072#1074#1083#1077#1085#1080#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 1001
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          BeginGroup = True
          UserDefine = [udWidth]
          UserWidth = 293
          Visible = True
          ItemName = 'cxBarEditItem1'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1047#1072#1082#1088#1099#1090#1100
      CaptionButtons = <>
      DockedLeft = 606
      DockedTop = 0
      FloatLeft = 1161
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = acSave
      Category = 0
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Caption = 
        '                                                                ' +
        '                '
      Category = 0
      Hint = 
        '                                                                ' +
        '                '
      Visible = ivAlways
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = acClose
      Category = 0
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = #1055#1088#1080#1095#1080#1085#1072
      Category = 0
      Hint = #1055#1088#1080#1095#1080#1085#1072
      Visible = ivAlways
      PropertiesClassName = 'TcxTextEditProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = 'New SubItem'
      Category = 0
      Visible = ivAlways
      ItemLinks = <>
    end
    object cxBarEditItem2: TcxBarEditItem
      Caption = #1044#1072#1090#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
      Category = 0
      Hint = #1044#1072#1090#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
      Visible = ivAlways
      PropertiesClassName = 'TcxDateEditProperties'
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = acTestSpec
      Category = 0
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = acGetInfoSel
      Category = 0
    end
    object dxBarGroup1: TdxBarGroup
      Items = ()
    end
  end
  object ActionList1: TActionList
    Images = dmR.SmallImage
    Left = 648
    Top = 168
    object acClose: TAction
      Caption = #1047#1072#1082#1088#1099#1090#1100
      ImageIndex = 100
      OnExecute = acCloseExecute
    end
    object acSave: TAction
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' (Ctrl + S)'
      ImageIndex = 7
      ShortCut = 16467
      OnExecute = acSaveExecute
    end
    object acTestSpec: TAction
      Caption = #1055#1088#1086#1074#1077#1088#1082#1072' '#1089#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1080
      ImageIndex = 38
    end
    object acDelPos: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1087#1086#1079#1080#1094#1080#1102
      ImageIndex = 4
      OnExecute = acDelPosExecute
    end
    object acGetInfoSel: TAction
      Caption = #1047#1072#1087#1088#1086#1089#1080#1090#1100' '#1080#1085#1092#1086#1088#1084#1072#1094#1080#1102' '#1087#1086' '#1074#1099#1076#1077#1083#1077#1085#1085#1099#1084
      ImageIndex = 50
      OnExecute = acGetInfoSelExecute
    end
    object acAddPos: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1087#1086#1079#1080#1094#1080#1102
      ImageIndex = 3
      OnExecute = acAddPosExecute
    end
  end
  object quSpecCorr: TFDQuery
    CachedUpdates = True
    Connection = dmR.FDConnection
    Transaction = dmR.FDTrans
    UpdateTransaction = dmR.FDTransUpdate
    UpdateObject = UpdSQL1
    SQL.Strings = (
      'declare @FSRARID varchar(50) = :RARID'
      'declare @IDATE int = :IDATE'
      'declare @SID varchar(50) = :SID'
      ''
      'SELECT sp.[FSRARID]'
      '      ,sp.[IDATE]'
      '      ,sp.[SIDHD]'
      '      ,sp.[ID]'
      '      ,sp.[NUM]'
      '      ,sp.[ALCCODE]'
      '      ,sp.[QUANT]'
      '      ,ca.[NAME]'
      '      ,ca.[VOL]'
      '      ,ca.[KREP]'
      '      ,ca.[AVID]'
      '      ,ca.[PRODID]'
      #9'  ,prod.PRODINN'
      #9'  ,prod.PRODKPP'
      #9'  ,prod.NAME as NAMECLI'
      #9'  ,prod.FULLNAME as FULLNAMECLI'
      #9'  ,prod.ADDR'
      #9'  ,prod.COUNTRY'
      #9'  ,prod.REGCODE'
      '          ,prod.[CLITYPE]'
      '  FROM [RAR].[dbo].[ADOCSCORR_SP] sp'
      '  left join [dbo].[ACARDS] ca on ca.KAP=sp.[ALCCODE]'
      '  left join [dbo].[APRODS] prod on prod.[PRODID]=ca.[PRODID]'
      ' '
      '  where sp.[FSRARID]=@FSRARID'
      '  and sp.[IDATE]=@IDATE'
      '  and sp.[SIDHD]=@SID')
    Left = 88
    Top = 360
    ParamData = <
      item
        Name = 'RARID'
        DataType = ftString
        ParamType = ptInput
        Value = '020000021354'
      end
      item
        Name = 'IDATE'
        DataType = ftInteger
        ParamType = ptInput
        Value = 42416
      end
      item
        Name = 'SID'
        DataType = ftString
        ParamType = ptInput
        Value = '04F3A6D5-970B-4C30-A927-AA50AA999EF2'
      end>
    object quSpecCorrFSRARID: TStringField
      FieldName = 'FSRARID'
      Origin = 'FSRARID'
      Required = True
      Size = 50
    end
    object quSpecCorrIDATE: TIntegerField
      FieldName = 'IDATE'
      Origin = 'IDATE'
      Required = True
    end
    object quSpecCorrSIDHD: TStringField
      FieldName = 'SIDHD'
      Origin = 'SIDHD'
      Required = True
      Size = 50
    end
    object quSpecCorrID: TLargeintField
      AutoGenerateValue = arAutoInc
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere]
      ReadOnly = True
    end
    object quSpecCorrNUM: TIntegerField
      FieldName = 'NUM'
      Origin = 'NUM'
    end
    object quSpecCorrALCCODE: TStringField
      FieldName = 'ALCCODE'
      Origin = 'ALCCODE'
      Size = 50
    end
    object quSpecCorrQUANT: TFloatField
      FieldName = 'QUANT'
      Origin = 'QUANT'
      DisplayFormat = '0.000'
    end
    object quSpecCorrNAME: TMemoField
      FieldName = 'NAME'
      Origin = 'NAME'
      BlobType = ftMemo
      Size = 2147483647
    end
    object quSpecCorrVOL: TSingleField
      FieldName = 'VOL'
      Origin = 'VOL'
      DisplayFormat = '0.000'
    end
    object quSpecCorrKREP: TSingleField
      FieldName = 'KREP'
      Origin = 'KREP'
      DisplayFormat = '0.0'
    end
    object quSpecCorrAVID: TIntegerField
      FieldName = 'AVID'
      Origin = 'AVID'
    end
    object quSpecCorrPRODID: TStringField
      FieldName = 'PRODID'
      Origin = 'PRODID'
      Size = 50
    end
    object quSpecCorrPRODINN: TStringField
      FieldName = 'PRODINN'
      Origin = 'PRODINN'
    end
    object quSpecCorrPRODKPP: TStringField
      FieldName = 'PRODKPP'
      Origin = 'PRODKPP'
    end
    object quSpecCorrNAMECLI: TMemoField
      FieldName = 'NAMECLI'
      Origin = 'NAMECLI'
      BlobType = ftMemo
      Size = 2147483647
    end
    object quSpecCorrFULLNAMECLI: TMemoField
      FieldName = 'FULLNAMECLI'
      Origin = 'FULLNAMECLI'
      BlobType = ftMemo
      Size = 2147483647
    end
    object quSpecCorrADDR: TMemoField
      FieldName = 'ADDR'
      Origin = 'ADDR'
      BlobType = ftMemo
      Size = 2147483647
    end
    object quSpecCorrCOUNTRY: TStringField
      FieldName = 'COUNTRY'
      Origin = 'COUNTRY'
      Size = 10
    end
    object quSpecCorrREGCODE: TStringField
      FieldName = 'REGCODE'
      Origin = 'REGCODE'
      Size = 10
    end
    object quSpecCorrCLITYPE: TStringField
      FieldName = 'CLITYPE'
      Origin = 'CLITYPE'
      Size = 5
    end
  end
  object UpdSQL1: TFDUpdateSQL
    Connection = dmR.FDConnection
    InsertSQL.Strings = (
      'INSERT INTO RAR.dbo.ADOCSCORR_SP'
      '(FSRARID, IDATE, SIDHD, NUM, ALCCODE, '
      '  QUANT)'
      
        'VALUES (:NEW_FSRARID, :NEW_IDATE, :NEW_SIDHD, :NEW_NUM, :NEW_ALC' +
        'CODE, '
      '  :NEW_QUANT);'
      
        'SELECT FSRARID, IDATE, SIDHD, SCOPE_IDENTITY() AS ID, NUM, ALCCO' +
        'DE, '
      '  QUANT'
      'FROM RAR.dbo.ADOCSCORR_SP'
      'WHERE ID = SCOPE_IDENTITY()')
    ModifySQL.Strings = (
      'UPDATE RAR.dbo.ADOCSCORR_SP'
      
        'SET FSRARID = :NEW_FSRARID, IDATE = :NEW_IDATE, SIDHD = :NEW_SID' +
        'HD, '
      '  NUM = :NEW_NUM, ALCCODE = :NEW_ALCCODE, QUANT = :NEW_QUANT'
      
        'WHERE FSRARID = :OLD_FSRARID AND IDATE = :OLD_IDATE AND SIDHD = ' +
        ':OLD_SIDHD AND '
      '  ID = :OLD_ID;'
      'SELECT FSRARID, IDATE, SIDHD, ID, NUM, ALCCODE, QUANT'
      'FROM RAR.dbo.ADOCSCORR_SP'
      
        'WHERE FSRARID = :NEW_FSRARID AND IDATE = :NEW_IDATE AND SIDHD = ' +
        ':NEW_SIDHD AND '
      '  ID = :NEW_ID')
    DeleteSQL.Strings = (
      'DELETE FROM RAR.dbo.ADOCSCORR_SP'
      
        'WHERE FSRARID = :OLD_FSRARID AND IDATE = :OLD_IDATE AND SIDHD = ' +
        ':OLD_SIDHD AND '
      '  ID = :OLD_ID')
    FetchRowSQL.Strings = (
      
        'SELECT FSRARID, IDATE, SIDHD, SCOPE_IDENTITY() AS ID, NUM, ALCCO' +
        'DE, '
      '  QUANT'
      'FROM RAR.dbo.ADOCSCORR_SP'
      
        'WHERE FSRARID = :FSRARID AND IDATE = :IDATE AND SIDHD = :SIDHD A' +
        'ND '
      '  ID = :ID')
    Left = 176
    Top = 360
  end
  object dsquSpecCorr: TDataSource
    DataSet = quSpecCorr
    Left = 88
    Top = 424
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 232
    Top = 200
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svFont]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
    end
    object cxStyle2: TcxStyle
    end
  end
  object PopupMenu1: TPopupMenu
    Images = dmR.SmallImage
    Left = 432
    Top = 272
    object N4: TMenuItem
      Action = acAddPos
    end
    object N1: TMenuItem
      Action = acDelPos
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object N3: TMenuItem
      Action = acGetInfoSel
    end
  end
  object quE: TFDQuery
    Connection = dmR.FDConnection
    Left = 80
    Top = 280
  end
end
