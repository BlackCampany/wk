unit dmRar;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.MSSQL,
  FireDAC.Phys.MSSQLDef, FireDAC.Comp.Client, Data.DB, FireDAC.Stan.Param,
  FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt, FireDAC.Comp.DataSet,
  Winapi.Windows,Vcl.Forms,
  EgaisDecode,
  cxMemo,cxGrid,cxGridExportLink,

  httpsend, synautil, nativexml,
  FireDAC.VCLUI.Wait, FireDAC.Comp.UI, FireDAC.Phys.ODBCBase, Vcl.Dialogs,
  cxStyles, cxClasses, Vcl.ImgList, Vcl.Controls, cxGraphics, frxExportXLS, frxClass, frxExportRTF;

type
  TdmR = class(TDataModule)
    FDConnection: TFDConnection;
    FDTrans: TFDTransaction;
    FDTransUpdate: TFDTransaction;
    quGetId: TFDQuery;
    FDPhysMSSQLDriverLink: TFDPhysMSSQLDriverLink;
    FDGUIxWaitCursor: TFDGUIxWaitCursor;
    quGetIdretnum: TIntegerField;
    quToRar: TFDQuery;
    quToRarID: TLargeintField;
    quToRarDATEQU: TSQLTimeStampField;
    quToRarITYPEQU: TSmallintField;
    quToRarISTATUS: TSmallintField;
    quToRarSENDFILE: TMemoField;
    quToRarRECEIVE_ID: TStringField;
    quToRarRECEIVE_FILE: TMemoField;
    quAddToList: TFDQuery;
    quReplyList: TFDQuery;
    quAddToListReply: TFDQuery;
    quQuestListToDel_NoNeed: TFDQuery;
    quQuestListToDel_NoNeedRECEIVE_ID: TStringField;
    quQuestListToDel_NoNeedRETVAL: TStringField;
    quQuestListToDel_NoNeedIACTIVE: TSmallintField;
    quQuestListToDel_NoNeedIDEL: TSmallintField;
    quQuestListToDel_NoNeedDateSend: TSQLTimeStampField;
    quReplyListToDel_NoNeed: TFDQuery;
    quReplyListID: TLargeintField;
    quReplyListReplyId: TStringField;
    quReplyListReplyPath: TStringField;
    quReplyListReplyFile: TMemoField;
    quReplyListIACTIVE: TSmallintField;
    quReplyListIDEL: TSmallintField;
    quReplyListDateReply: TSQLTimeStampField;
    quReplyListToDel_NoNeedID: TLargeintField;
    quReplyListToDel_NoNeedReplyId: TStringField;
    quReplyListToDel_NoNeedReplyPath: TStringField;
    quReplyListToDel_NoNeedReplyFile: TMemoField;
    quReplyListToDel_NoNeedIACTIVE: TSmallintField;
    quReplyListToDel_NoNeedIDEL: TSmallintField;
    quReplyListToDel_NoNeedDateReply: TSQLTimeStampField;
    quQuestProdsList: TFDQuery;
    quQuestProdsListINN: TStringField;
    quQuestProdsListISTATUS: TSmallintField;
    quDocInRec: TFDQuery;
    quDocInRecFSRARID: TStringField;
    quDocInRecIDATE: TIntegerField;
    quDocInRecSID: TStringField;
    quDocInRecNUMBER: TStringField;
    quDocInRecSDATE: TStringField;
    quDocInRecSTYPE: TStringField;
    quDocInRecUNITTYPE: TStringField;
    quDocInRecSHIPDATE: TStringField;
    quDocInRecCLIFROM: TStringField;
    quDocInRecIACTIVE: TSmallintField;
    quDocInRecWBREGID: TStringField;
    quDocInRecFIXNUMBER: TStringField;
    quDocInRecFIXDATE: TStringField;
    quSpecInSel: TFDQuery;
    quSpecInSelFSRARID: TStringField;
    quSpecInSelIDATE: TIntegerField;
    quSpecInSelSIDHD: TStringField;
    quSpecInSelID: TLargeintField;
    quSpecInSelNUM: TIntegerField;
    quSpecInSelALCCODE: TStringField;
    quSpecInSelQUANT: TFloatField;
    quSpecInSelPRICE: TFloatField;
    quSpecInSelQUANTF: TFloatField;
    quSpecInSelPRICEF: TFloatField;
    quSpecInSelPRISEF0: TFloatField;
    quSpecInSelWBREGID: TStringField;
    quDocInHd: TFDQuery;
    quDocInHdFSRARID: TStringField;
    quDocInHdIDATE: TIntegerField;
    quDocInHdSID: TStringField;
    quDocInHdNUMBER: TStringField;
    quDocInHdSDATE: TStringField;
    quDocInHdSTYPE: TStringField;
    quDocInHdUNITTYPE: TStringField;
    quDocInHdSHIPDATE: TStringField;
    quDocInHdCLIFROM: TStringField;
    quDocInHdIACTIVE: TSmallintField;
    quDocInHdFULLNAME: TMemoField;
    quDocInHdNAME: TMemoField;
    quDocInHdCLIENTINN: TStringField;
    quDocInHdCLIENTKPP: TStringField;
    quDocInHdWBREGID: TStringField;
    quDocInHdFIXNUMBER: TStringField;
    quDocInHdFIXDATE: TStringField;
    dsquDocInHd: TDataSource;
    quS: TFDQuery;
    quUpdSpec: TFDQuery;
    quProc: TFDQuery;
    quDocInHdCOMPARE_STATUS: TSmallintField;
    quDocInHdCOMPARE_COMMENT: TStringField;
    quDocInHdREADYSEND: TSmallintField;
    quDocInHdIDHEAD: TLargeintField;
    quDocInHdCOMPARE_QUANT_STATUS: TSmallintField;
    quDocInHdDATE_INPUT: TSQLTimeStampField;
    quDocInHdDATE_OUTPUT: TSQLTimeStampField;
    quDocInHdXMLFILE: TStringField;
    quDocInRecIDHEAD: TLargeintField;
    quDocInRecCOMPARE_STATUS: TSmallintField;
    quDocInRecCOMPARE_QUANT_STATUS: TSmallintField;
    quDocInRecCOMPARE_COMMENT: TStringField;
    quDocInRecDATE_INPUT: TSQLTimeStampField;
    quDocInRecDATE_OUTPUT: TSQLTimeStampField;
    quDocInRecSENDXML: TMemoField;
    quDocInToCompare: TFDQuery;
    quDocInToCompareFSRARID: TStringField;
    quDocInToCompareIDATE: TIntegerField;
    quDocInToCompareSID: TStringField;
    quDocInToCompareNUMBER: TStringField;
    quDocInToCompareSDATE: TStringField;
    quDocInToCompareSTYPE: TStringField;
    quDocInToCompareUNITTYPE: TStringField;
    quDocInToCompareSHIPDATE: TStringField;
    quDocInToCompareCLIFROM: TStringField;
    quDocInToCompareIACTIVE: TSmallintField;
    quDocInToCompareWBREGID: TStringField;
    quDocInToCompareFIXNUMBER: TStringField;
    quDocInToCompareFIXDATE: TStringField;
    quDocInToCompareCOMPARE_STATUS: TSmallintField;
    quDocInToCompareCOMPARE_COMMENT: TStringField;
    quDocInToCompareREADYSEND: TSmallintField;
    quDocInToCompareIDHEAD: TLargeintField;
    quDocInToCompareCOMPARE_QUANT_STATUS: TSmallintField;
    quDocInToCompareDATE_INPUT: TSQLTimeStampField;
    quDocInToCompareDATE_OUTPUT: TSQLTimeStampField;
    quDocInToSend: TFDQuery;
    quDocInToSendFSRARID: TStringField;
    quDocInToSendIDATE: TIntegerField;
    quDocInToSendSID: TStringField;
    quDocInToSendNUMBER: TStringField;
    quDocInToSendSDATE: TStringField;
    quDocInToSendSTYPE: TStringField;
    quDocInToSendUNITTYPE: TStringField;
    quDocInToSendSHIPDATE: TStringField;
    quDocInToSendCLIFROM: TStringField;
    quDocInToSendIACTIVE: TSmallintField;
    quDocInToSendWBREGID: TStringField;
    quDocInToSendFIXNUMBER: TStringField;
    quDocInToSendFIXDATE: TStringField;
    quDocInToSendCOMPARE_STATUS: TSmallintField;
    quDocInToSendCOMPARE_COMMENT: TStringField;
    quDocInToSendREADYSEND: TSmallintField;
    quDocInToSendIDHEAD: TLargeintField;
    quDocInToSendCOMPARE_QUANT_STATUS: TSmallintField;
    quDocInToSendDATE_INPUT: TSQLTimeStampField;
    quDocInToSendDATE_OUTPUT: TSQLTimeStampField;
    SaveDialog: TSaveDialog;
    quDocOutHd: TFDQuery;
    dsquDocsOut: TDataSource;
    quDocOutHdFSRARID: TStringField;
    quDocOutHdIDATE: TIntegerField;
    quDocOutHdSID: TStringField;
    quDocOutHdNUMBER: TStringField;
    quDocOutHdSDATE: TStringField;
    quDocOutHdSTYPE: TStringField;
    quDocOutHdUNITTYPE: TStringField;
    quDocOutHdSHIPDATE: TStringField;
    quDocOutHdCLIFTO: TStringField;
    quDocOutHdIACTIVE: TSmallintField;
    quDocOutHdREADYSEND: TSmallintField;
    quDocOutHdWBREGID: TStringField;
    quDocOutHdFIXNUMBER: TStringField;
    quDocOutHdFIXDATE: TStringField;
    quDocOutHdIDHEAD: TLargeintField;
    quDocOutHdDATE_INPUT: TSQLTimeStampField;
    quDocOutHdDATE_OUTPUT: TSQLTimeStampField;
    quDocOutHdXMLF: TStringField;
    quDocOutHdFULLNAME: TMemoField;
    quDocOutHdNAME: TMemoField;
    quDocOutHdCLIENTINN: TStringField;
    quDocOutHdCLIENTKPP: TStringField;
    quDocOutHdDDATE: TSQLTimeStampField;
    quDocOutRec: TFDQuery;
    quDocOutRecFSRARID: TStringField;
    quDocOutRecIDATE: TIntegerField;
    quDocOutRecSID: TStringField;
    quDocOutRecNUMBER: TStringField;
    quDocOutRecSDATE: TStringField;
    quDocOutRecSTYPE: TStringField;
    quDocOutRecUNITTYPE: TStringField;
    quDocOutRecSHIPDATE: TStringField;
    quDocOutRecCLIFTO: TStringField;
    quDocOutRecIACTIVE: TSmallintField;
    quDocOutRecREADYSEND: TSmallintField;
    quDocOutRecWBREGID: TStringField;
    quDocOutRecFIXNUMBER: TStringField;
    quDocOutRecFIXDATE: TStringField;
    quDocOutRecIDHEAD: TLargeintField;
    quDocOutRecDATE_INPUT: TSQLTimeStampField;
    quDocOutRecDATE_OUTPUT: TSQLTimeStampField;
    quDocOutRecSENDXML: TMemoField;
    stlRepColors: TcxStyleRepository;
    stlLightBlue: TcxStyle;
    stlLightYellow: TcxStyle;
    stlBold: TcxStyle;
    quSpecOutSel: TFDQuery;
    quSpecOutSelFSRARID: TStringField;
    quSpecOutSelIDATE: TIntegerField;
    quSpecOutSelSIDHD: TStringField;
    quSpecOutSelID: TLargeintField;
    quSpecOutSelNUM: TIntegerField;
    quSpecOutSelALCCODE: TStringField;
    quSpecOutSelQUANT: TFloatField;
    quSpecOutSelQUANTREMN: TFloatField;
    quSpecOutSelQUANTF: TFloatField;
    quSpecOutSelPRICE: TFloatField;
    quSpecOutSelPRICEF: TFloatField;
    quSpecOutSelNAME: TMemoField;
    quSpecOutSelVOL: TSingleField;
    quSpecOutSelKREP: TSingleField;
    quSpecOutSelPRODNAME: TMemoField;
    quSpecOutSelWBREGID: TStringField;
    quSpecOutSelCODECB: TIntegerField;
    quDocOutHdDATEDOC: TSQLTimeStampField;
    quDocOutRecDATEDOC: TSQLTimeStampField;
    quSelCli: TFDQuery;
    quSelCliCLIENTID: TStringField;
    quSelCliCLIENTINN: TStringField;
    quSelCliCLIENTKPP: TStringField;
    quSelCliFULLNAME: TMemoField;
    quSelCliNAME: TMemoField;
    quSelCliCOUNTRY: TStringField;
    quSelCliREGCODE: TStringField;
    quSelCliADDR: TMemoField;
    quSpecOutSelINFO_A: TStringField;
    quSpecOutSelINFO_B: TStringField;
    quSpecOutSelAVID: TIntegerField;
    quSpecOutSelPRODID: TStringField;
    quSpecOutSelPRODINN: TStringField;
    quSpecOutSelPRODKPP: TStringField;
    quSpecOutSelPRODFULLNAME: TMemoField;
    quSpecOutSelPRODCOUNTRY: TStringField;
    quSpecOutSelPRODREGCODE: TStringField;
    quSpecOutSelPRODADDR: TMemoField;
    quReplyRec: TFDQuery;
    quReplyRecID: TLargeintField;
    quReplyRecReplyId: TStringField;
    quReplyRecReplyPath: TStringField;
    quReplyRecReplyFile: TMemoField;
    quReplyRecIACTIVE: TSmallintField;
    quReplyRecIDEL: TSmallintField;
    quReplyRecDateReply: TSQLTimeStampField;
    quDocInHdTICK1: TLargeintField;
    quDocInHdTICK2: TLargeintField;
    quDocInHdTT1: TIntegerField;
    quDocInHdTT2: TIntegerField;
    quDocOutHdTICK1: TLargeintField;
    quDocOutHdTICK2: TLargeintField;
    quDocOutHdTT1: TIntegerField;
    quDocOutHdTT2: TIntegerField;
    quSpecInSelSNUM: TStringField;
    quSelInHD: TFDQuery;
    quSelInHDFSRARID: TStringField;
    quSelInHDIDATE: TIntegerField;
    quSelInHDSID: TStringField;
    quSelHistory: TFDQuery;
    quSelHistoryLOG: TStringField;
    SmallImage: TcxImageList;
    LargeImage: TcxImageList;
    quNATTN: TFDQuery;
    dsquNATTN: TDataSource;
    quNATTNFSRARID: TStringField;
    quNATTNWBREGID: TStringField;
    quNATTNNUMBER: TStringField;
    quNATTNSHIPDATE: TStringField;
    quNATTNCLIFROM: TStringField;
    quNATTNIACTIVE: TSmallintField;
    quNATTNFULLNAME: TMemoField;
    quNATTNNAME: TMemoField;
    quNATTNCLIENTINN: TStringField;
    quNATTNCLIENTKPP: TStringField;
    quNATTNDATE_REPLY: TStringField;
    upNATTN: TFDUpdateSQL;
    quNATTNISHIPDATE: TStringField;
    quNATTNTT2: TIntegerField;
    quGetKAP: TFDQuery;
    quGetKAPId: TStringField;
    quGetKAPIdGoods: TIntegerField;
    quGetKAPIdProducer: TStringField;
    quGetKAPName: TStringField;
    quGetKAPFullName: TStringField;
    quSelOutHD: TFDQuery;
    quSelOutHDFSRARID: TStringField;
    quSelOutHDIDATE: TIntegerField;
    quSelOutHDSID: TStringField;
    quQBARCODE_SP: TFDQuery;
    quToRarFSRARID: TStringField;
    quQuestListToDel_NoNeedFSRARID: TStringField;
    quQBARCODE_SPID: TFDAutoIncField;
    quQBARCODE_SPFSRARID: TStringField;
    quQBARCODE_SPIDATE: TIntegerField;
    quQBARCODE_SPINUMBER: TIntegerField;
    quQBARCODE_SPIDENT: TIntegerField;
    quQBARCODE_SPTYPE: TStringField;
    quQBARCODE_SPRANK: TStringField;
    quQBARCODE_SPNUMBER: TStringField;
    quPoint: TFDQuery;
    quPointRARID: TStringField;
    quPointISHOP: TIntegerField;
    quPointName: TStringField;
    quPointIPUTM: TStringField;
    quPointIPDB: TStringField;
    quPointDBNAME: TStringField;
    quPointPASSW: TStringField;
    quPointINN: TStringField;
    quPointKPP: TStringField;
    quPointSHORTNAME: TStringField;
    quPointFULLNAME: TStringField;
    quPointCOUNTRY: TStringField;
    quPointDESCR: TStringField;
    quPointISS: TSmallintField;
    quPointISPRODUCTION: TSmallintField;
    quPointSDEP: TStringField;
    dsquPoint: TDataSource;
    quSelOrg: TFDQuery;
    quSelOrgRARID: TStringField;
    quSelOrgISHOP: TIntegerField;
    quSelOrgIPUTM: TStringField;
    quSelOrgIPDB: TStringField;
    quSelOrgDBNAME: TStringField;
    quSelOrgPASSW: TStringField;
    quSelOrgINN: TStringField;
    quSelOrgKPP: TStringField;
    quSelOrgSHORTNAME: TStringField;
    quSelOrgFULLNAME: TStringField;
    quSelOrgCOUNTRY: TStringField;
    quSelOrgDESCR: TStringField;
    quSelDocVn: TFDQuery;
    quSelDocVnFSRARID: TStringField;
    quSelDocVnIDATE: TIntegerField;
    quSelDocVnSID: TStringField;
    quSelDocVnNUMBER: TStringField;
    quSelDocVnIACTIVE: TSmallintField;
    quSelDocVnREADYSEND: TSmallintField;
    quSelDocVnWBREGID: TStringField;
    quSelDocVnFIXNUMBER: TStringField;
    quSelDocVnFIXDATE: TStringField;
    quSelDocVnTICK1: TLargeintField;
    quSelDocVnTICK2: TLargeintField;
    quSelDocVnSIDIN: TStringField;
    quSelDocVnSENDXML: TMemoField;
    quSelDocVnSp: TFDQuery;
    quSelDocVnSpFSRARID: TStringField;
    quSelDocVnSpIDATE: TIntegerField;
    quSelDocVnSpSIDHD: TStringField;
    quSelDocVnSpID: TLargeintField;
    quSelDocVnSpNUM: TIntegerField;
    quSelDocVnSpALCCODE: TStringField;
    quSelDocVnSpQUANT: TFloatField;
    quSelDocVnSpINFO_B: TStringField;
    quDocsVnHD: TFDQuery;
    dsquDocsVnHD: TDataSource;
    quSelDocVnSDATE: TSQLTimeStampField;
    quSelDocVnSTYPE: TStringField;
    quSelDocVn1: TFDQuery;
    quSelDocVn1FSRARID: TStringField;
    quSelDocVn1IDATE: TIntegerField;
    quSelDocVn1SID: TStringField;
    quSelDocVn1NUMBER: TStringField;
    quSelDocVn1SDATE: TSQLTimeStampField;
    quSelDocVn1STYPE: TStringField;
    quSelDocVn1IACTIVE: TSmallintField;
    quSelDocVn1READYSEND: TSmallintField;
    quSelDocVn1WBREGID: TStringField;
    quSelDocVn1FIXNUMBER: TStringField;
    quSelDocVn1FIXDATE: TStringField;
    quSelDocVn1TICK1: TLargeintField;
    quSelDocVn1TICK2: TLargeintField;
    quSelDocVn1SIDIN: TStringField;
    quSelDocVn1SENDXML: TMemoField;
    quDocsVnHDFSRARID: TStringField;
    quDocsVnHDIDATE: TIntegerField;
    quDocsVnHDSID: TStringField;
    quDocsVnHDNUMBER: TStringField;
    quDocsVnHDSDATE: TSQLTimeStampField;
    quDocsVnHDSTYPE: TStringField;
    quDocsVnHDIACTIVE: TSmallintField;
    quDocsVnHDREADYSEND: TSmallintField;
    quDocsVnHDWBREGID: TStringField;
    quDocsVnHDFIXNUMBER: TStringField;
    quDocsVnHDFIXDATE: TStringField;
    quDocsVnHDTICK1: TLargeintField;
    quDocsVnHDTICK2: TLargeintField;
    quDocsVnHDSIDIN: TStringField;
    quDocsVnHDSENDXML: TStringField;
    quDocsVnHDTT1: TIntegerField;
    quDocsVnHDTT2: TIntegerField;
    quDocsInv: TFDQuery;
    dsquDocsInv: TDataSource;
    quDocsInvFSRARID: TStringField;
    quDocsInvIDATE: TIntegerField;
    quDocsInvDOCDATE: TSQLTimeStampField;
    quDocsInvID: TLargeintField;
    quDocsInvSTYPE: TStringField;
    quDocsInvFIXDATE: TStringField;
    quDocsInvIDREPLY: TLargeintField;
    quDocsInvXML: TStringField;
    quDocsCorr: TFDQuery;
    dsquDocsCorr: TDataSource;
    quDocsCorrFSRARID: TStringField;
    quDocsCorrIDATE: TIntegerField;
    quDocsCorrSID: TStringField;
    quDocsCorrNUMBER: TStringField;
    quDocsCorrSDATE: TSQLTimeStampField;
    quDocsCorrSTYPE: TStringField;
    quDocsCorrDESCR: TStringField;
    quDocsCorrIACTIVE: TSmallintField;
    quDocsCorrFIXNUMBER: TStringField;
    quDocsCorrFIXDATE: TStringField;
    quDocsCorrTICK1: TLargeintField;
    quDocsCorrTICK2: TLargeintField;
    quDocsCorrSENDXML: TStringField;
    quDocsCorrTT1: TIntegerField;
    quDocsCorrTT2: TIntegerField;
    quDocsCorrRec: TFDQuery;
    quDocsCorrRecFSRARID: TStringField;
    quDocsCorrRecIDATE: TIntegerField;
    quDocsCorrRecSID: TStringField;
    quDocsCorrRecNUMBER: TStringField;
    quDocsCorrRecSDATE: TSQLTimeStampField;
    quDocsCorrRecSTYPE: TStringField;
    quDocsCorrRecDESCR: TStringField;
    quDocsCorrRecIACTIVE: TSmallintField;
    quDocsCorrRecFIXNUMBER: TStringField;
    quDocsCorrRecFIXDATE: TStringField;
    quDocsCorrRecTICK1: TLargeintField;
    quDocsCorrRecTICK2: TLargeintField;
    quDocsCorrRecSENDXML: TMemoField;
    quDocsCorrRecSP: TFDQuery;
    quDocsCorrRecSPFSRARID: TStringField;
    quDocsCorrRecSPIDATE: TIntegerField;
    quDocsCorrRecSPSIDHD: TStringField;
    quDocsCorrRecSPID: TLargeintField;
    quDocsCorrRecSPNUM: TIntegerField;
    quDocsCorrRecSPALCCODE: TStringField;
    quDocsCorrRecSPQUANT: TFloatField;
    quDocsCorrRecSPNAME: TMemoField;
    quDocsCorrRecSPVOL: TSingleField;
    quDocsCorrRecSPPRODID: TStringField;
    quDocsCorrRecSPKREP: TSingleField;
    quDocsCorrRecSPPRODINN: TStringField;
    quDocsCorrRecSPPRODKPP: TStringField;
    quDocsCorrRecSPNAMECLI: TMemoField;
    quDocsCorrRecSPFULLNAMECLI: TMemoField;
    quDocsCorrRecSPADDR: TMemoField;
    quDocsCorrRecSPCOUNTRY: TStringField;
    quDocsCorrRecSPREGCODE: TStringField;
    quDocsCorrRecSPCLITYPE: TStringField;
    quTestRemnInv2: TFDQuery;
    dsquTestRemnInv2: TDataSource;
    quTestRemnInv2NUM: TIntegerField;
    quTestRemnInv2ALCCODE: TStringField;
    quTestRemnInv2QUANT: TFloatField;
    quTestRemnInv2QUANTR: TSingleField;
    quTestRemnInv2QUANTD: TFloatField;
    quQBARCODE_SPIPUTM: TStringField;
    frxRTFExport1: TfrxRTFExport;
    frxXLSExport1: TfrxXLSExport;
    FDTransUpdate1: TFDTransaction;
    procedure DataModuleCreate(Sender: TObject);
    procedure quDocInHdCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    function DBConnect(Server,Database,User_Name,Password:String): boolean;
    function DBConnectUDL: boolean;
  end;

type
     TSupportedExportType = (exHTML, exXML, exExcel97, exExcel, exPDF, exText);

var
  dmR: TdmR;


function fGetId(iT:Integer):Integer;
procedure Delay(MSecs: Longint);
function IdToName(Id:Integer):string;
procedure prGetListAsk(Memo1:tcxMemo);
procedure prGetListAsw(Memo1:tcxMemo);
procedure prGetFilesAsw(Memo1:tcxMemo);
procedure prDecodeFilesAsw(Memo1:tcxMemo);
procedure WriteHistoryTC(Strwk_: string);

procedure prWMemo(Memo1:tcxMemo; StrWr:string);
procedure ExportGridToFile(afilename: string; cxGrid: TcxGrid);
procedure prGetAtrCli(vCli:TCli;var rCli:TCli);

procedure WriteHistoryIn(RARID,SID:String;IDATE:Integer;sComment:String);
procedure WriteHistoryOut(RARID,SID:String;IDATE:Integer;sComment:String);

function GetKAP(IdGoods: Integer):String;
procedure prGetAtr(vCli:TCli;var rCli:TCli);
function prGetVidName(AVid:Integer):string;
procedure prGetParProd(PRODID:string;var PRODNAME,PRODINN,PRODKPP:String);
procedure prGetParProd1(PRODINN,PRODKPP:string;var PRODID,PRODNAME:String);


implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

// uses MainRar;

{$R *.dfm}

procedure prGetParProd1(PRODINN,PRODKPP:string;var PRODID,PRODNAME:String);
begin
  with dmR do
  begin
    try
      PRODNAME:='';
      PRODID:='';

      if PRODINN>'' then
      begin
        quS.Active:=False;
        quS.SQL.Clear;
        quS.SQL.Add('');
        quS.SQL.Add('SELECT TOP 1 [PRODID],[PRODINN],[PRODKPP],[FULLNAME],[NAME],[COUNTRY],[REGCODE],[ADDR],[CLITYPE]');
        quS.SQL.Add('  FROM [dbo].[APRODS]');
        quS.SQL.Add('  where [PRODINN]='''+TRIM(PRODINN)+'''');
        quS.SQL.Add('  and [PRODKPP]='''+TRIM(PRODKPP)+'''');
        quS.Active:=True;
        if quS.RecordCount>0 then
        begin
          PRODNAME:=quS.FieldByName('FULLNAME').AsString;
          PRODID:=quS.FieldByName('PRODID').AsString;
        end;
        quS.Active:=False;
      end;
    except
    end;
  end;
end;


procedure prGetParProd(PRODID:string;var PRODNAME,PRODINN,PRODKPP:String);
begin
  with dmR do
  begin
    try
      while length(PRODID)<12 do PRODID:='0'+PRODID;

      PRODNAME:='';
      PRODINN:='';
      PRODKPP:='';

      quS.Active:=False;
      quS.SQL.Clear;
      quS.SQL.Add('');
      quS.SQL.Add('SELECT TOP 1 [PRODID],[PRODINN],[PRODKPP],[FULLNAME],[NAME],[COUNTRY],[REGCODE],[ADDR],[CLITYPE]');
      quS.SQL.Add('  FROM [dbo].[APRODS]');
      quS.SQL.Add('  where [PRODID]='''+TRIM(PRODID)+'''');
      quS.Active:=True;
      if quS.RecordCount>0 then
      begin
        PRODNAME:=quS.FieldByName('FULLNAME').AsString;
        PRODINN:=quS.FieldByName('PRODINN').AsString;
        PRODKPP:=quS.FieldByName('PRODKPP').AsString;
      end;
      quS.Active:=False;
    except
    end;
  end;
end;


function prGetVidName(AVid:Integer):string;
begin
  Result:='';
  with dmR do
  begin
    try
      quS.Active:=False;
      quS.SQL.Clear;
      quS.SQL.Add('');
      quS.SQL.Add('SELECT [ID],[SID],[NAMEV],[IFORM] FROM [dbo].[AVID]');
      quS.SQL.Add('where ID='+its(AVid));
      quS.Active:=True;
      if quS.RecordCount>0 then Result:=quS.FieldByName('NAMEV').AsString;
      quS.Active:=False;
    except
    end;
  end;
end;

procedure prGetAtr(vCli:TCli;var rCli:TCli);
begin
  with dmR do
  begin
    rCli.ClientRegId:=vCli.ClientRegId;

    if vCli.ClientRegId>'' then
    begin
      quSelOrg.Active:=False;
      quSelOrg.ParamByName('FSRARID').Value:=vCli.ClientRegId;
      quSelOrg.Active:=True;
      if quSelOrg.RecordCount>0 then
      begin
        rCli.FullName:=quSelOrgFULLNAME.AsString;
        rCli.ShortName:=quSelOrgSHORTNAME.AsString;
        rCli.INN:=quSelOrgINN.AsString;
        rCli.KPP:=quSelOrgKPP.AsString;
        rCli.Country:=quSelOrgCOUNTRY.AsString;
        rCli.description:=quSelOrgDESCR.AsString;
      end;
      quSelOrg.Active:=False;
    end;
  end;
end;

procedure WriteHistoryOut(RARID,SID:String;IDATE:Integer;sComment:String);
begin
  with dmR do
  begin
    try
      quS.Active:=False;
      quS.SQL.Clear;
      quS.SQL.Add('');
      quS.SQL.Add('DECLARE @FSRARID varchar(50) = '''+Trim(RARID)+'''');
      quS.SQL.Add('DECLARE @IDATE int = '+IntToStr(iDate));
      quS.SQL.Add('DECLARE @SID varchar(50) = '''+Trim(SID)+'''');
      quS.SQL.Add('DECLARE @COMMENT varchar(500) ='''+Trim(sComment)+'''');

      quS.SQL.Add('EXECUTE [dbo].[prAddHistoryOut] @FSRARID ,@IDATE ,@SID ,@COMMENT');
      quS.ExecSQL;
    except
    end;
  end;
end;


procedure WriteHistoryIn(RARID,SID:String;IDATE:Integer;sComment:String);
begin
  with dmR do
  begin
    try
      quS.Active:=False;
      quS.SQL.Clear;
      quS.SQL.Add('');
      quS.SQL.Add('DECLARE @FSRARID varchar(50) = '''+Trim(RARID)+'''');
      quS.SQL.Add('DECLARE @IDATE int = '+IntToStr(iDate));
      quS.SQL.Add('DECLARE @SID varchar(50) = '''+Trim(SID)+'''');
      quS.SQL.Add('DECLARE @COMMENT varchar(500) ='''+Trim(sComment)+'''');

      quS.SQL.Add('EXECUTE [dbo].[prAddHistoryIn] @FSRARID ,@IDATE ,@SID ,@COMMENT');
      quS.ExecSQL;
    except
    end;
  end;
end;

procedure prGetAtrCli(vCli:TCli;var rCli:TCli);
begin
  with dmR do
  begin
    rCli.ClientRegId:=vCli.ClientRegId;
    rCli.FullName:='';
    rCli.ShortName:='';
    rCli.INN:='';
    rCli.KPP:='';
    rCli.Country:='';
    rCli.description:='';

    if vCli.ClientRegId>'' then
    begin
      quSelCli.Active:=False;
      quSelCli.ParamByName('IDCLI').Value:=rCli.ClientRegId;
      quSelCli.Active:=True;
      if quSelCli.RecordCount>0 then
      begin
        rCli.FullName:=quSelCliFULLNAME.AsString;
        rCli.ShortName:=quSelCliNAME.AsString;
        rCli.INN:=quSelCliCLIENTINN.AsString;
        rCli.KPP:=quSelCliCLIENTKPP.AsString;
        rCli.Country:=quSelCliCOUNTRY.AsString;
        rCli.description:=quSelCliADDR.AsString;
      end;
      quSelCli.Active:=False;
    end;
  end;
end;

function  TdmR.DBConnectUDL: boolean;
var UDLData: TStrings;
    Params: TStrings;
    CurDir : string;
    Database, Server, User_Name, Password: string;
begin
  Result:=False;
  //����� �� ��������� ����� ���� � ����������� ���������� MSSQL � FireDAC
  //c������ ������ ������������ ����� udl �� ADO
  CurDir := ExtractFilePath(ParamStr(0));
  if FileExists(CurDir+'Rar.udl')=False then Exit;

  UDLData := TStringList.Create;
  Params := TStringList.Create;
  try
    UDLData.LoadFromFile(CurDir+'Rar.udl');

    //<--������ ��������� ����������� ;
    Params.Delimiter:=';';
    Params.StrictDelimiter := True;
    Params.DelimitedText:=UDLData.Text;
    Server:=trim(Params.Values['Data Source']);
    Database:=trim(Params.Values['Initial Catalog']);
    User_Name:=trim(Params.Values['User ID']);
    Password:=trim(Params.Values['Password']);
    //-->

    //<--��������� ��������� ����������
    FDConnection.Close;
    FDConnection.Params.Values['SERVER']:=Server;
    FDConnection.Params.Values['DATABASE']:=Database;
    FDConnection.Params.Values['User_Name']:=User_Name;
    FDConnection.Params.Values['Password']:=Password;
    FDConnection.Params.Values['ApplicationName']:=CommonSet.AppName;
    FDConnection.Params.Values['Workstation']:='';
    //FDConnection.Params.Values['MARS']:='yes';
    FDConnection.Params.Values['DriverID']:='MSSQL';
    FDConnection.Connected:=True;
    //-->

  finally
    UDLData.Free;
    Params.Free;
  end;

  Sleep(100);
  Result:=FDConnection.Connected;
end;



procedure ExportGridToFile(afilename: string; cxGrid: TcxGrid);
var AExportType: TSupportedExportType;
    Ext:String;
begin
  AExportType := exExcel97;
  dmR.SaveDialog.FileName := afilename;
  if dmR.SaveDialog.Execute then begin
    case dmR.SaveDialog.FilterIndex of
      1: begin AExportType := exExcel97; Ext := 'xls'; end;
      2: begin AExportType := exText; Ext := 'csv'; end;
    end;

    if UpperCase(Copy(dmR.SaveDialog.FileName,Length(dmR.SaveDialog.FileName)-2,3)) <> UpperCase(Ext) then
      dmR.SaveDialog.FileName := dmR.SaveDialog.FileName + '.' + Ext;

    case AExportType of
      exHTML:
        ExportGridToHTML(dmR.SaveDialog.FileName, cxGrid);
      exXML:
        ExportGridToXML(dmR.SaveDialog.FileName, cxGrid);
      exExcel97:
        ExportGridToExcel(dmR.SaveDialog.FileName, cxGrid);
      exExcel:
        ExportGridToXLSX(dmR.SaveDialog.FileName, cxGrid);
      exText:
        ExportGridToText(dmR.SaveDialog.FileName, cxGrid);
    end;
  end;
end;


procedure WriteHistoryTC(Strwk_: string);
var F: TextFile;
    Strwk1:String;
    FileN:String;
begin
  if CommonSetTr.writelog=1 then
  begin
    try
      strwk1:='ServiceRAR_'+FormatDateTime('yyyy_mm_dd',Date);
      Strwk1:=StrWk1+'.txt';
      Application.ProcessMessages;
  //  FileN:=CurDir+strwk1;
      FileN:=CommonSetTr.PathHistory+strwk1;
      AssignFile(F, FileN);
      if Not FileExists(FileN) then Rewrite(F)
      else                           Append(F);
      Write(F, FormatDateTime('dd HH:NN:SS  ', Now));
      WriteLn(F, Strwk_);
      Flush(F);
    finally
      CloseFile(F);
    end;
  end;
end;


procedure prWMemo(Memo1:tcxMemo; StrWr:string);
begin
  if Memo1<>nil then Memo1.Lines.Add(StrWr);
end;


procedure prDecodeFilesAsw(Memo1:tcxMemo);
begin
  if dmR.FDConnection.Connected then
    prDecodeFilesAswPrivate(Memo1,dmR.FDConnection,CommonSet.IPUTM,CommonSet.FSRAR_ID);
end;

{
procedure prDecodeFilesAsw(Memo1:tcxMemo);
var
  xmlstr:string;
  iC,iCAll:Integer;
begin
  with dmR do
  begin
    if FDConnection.Connected then
    begin
      try
        iC:=0; iCAll:=0;

        quReplyList.Active:=False;
        quReplyList.ParamByName('RARID').AsString:=CommonSet.FSRAR_ID;
        quReplyList.Active:=True;
        quReplyList.First;
        while not quReplyList.Eof do
        begin
          xmlstr:=UTF8ToString(quReplyListReplyFile.AsString);
          if Pos('ReplyAP',xmlstr)>0 then //��� ������
          begin
            prWMemo(Memo1,'  '+quReplyListID.AsString+' - ���� ��������� �� �������.');
            if prDecodeCards(UTF8ToString(quReplyListReplyFile.AsString)) then
            begin
              inc(iC);
              quReplyList.Edit;
              quReplyListIACTIVE.AsInteger:=3;
              quReplyList.Post;
            end;
          end;
          if (Pos('ns:WayBillAct',xmlstr)>0)or(Pos('ns:WAYBILLACT',xmlstr)>0) then //��� ��������
          begin
            prWMemo(Memo1,'  '+quReplyListID.AsString+' - ���� ��������� ����.');
            if prDecodeAct(UTF8ToString(quReplyListReplyFile.AsString),quReplyListID.AsInteger) then
            begin
              inc(iC);
              quReplyList.Edit;
              quReplyListIACTIVE.AsInteger:=3;
              quReplyList.Post;
            end;
          end;
          if ((Pos('ns:WayBill',xmlstr)>0)or(Pos('ns:WAYBILL',xmlstr)>0))and(Pos('ns:WayBillAct',xmlstr)=0) then //��� ��������
          begin
            prWMemo(Memo1,'  '+quReplyListID.AsString+' - ���� ��������� ���������.');
            if prDecodeTTN(UTF8ToString(quReplyListReplyFile.AsString),quReplyListID.AsInteger) then
            begin
              inc(iC);
              quReplyList.Edit;
              quReplyListIACTIVE.AsInteger:=3;
              quReplyList.Post;
            end;
          end;
          if Pos('ns:TTNInformBReg',xmlstr)>0 then //��� �������������� ���� � ���������
          begin
            prWMemo(Memo1,'  '+quReplyListID.AsString+' - ���� ��������� ��������������� �����.');
            if prDecodeWB(UTF8ToString(quReplyListReplyFile.AsString),quReplyListID.AsInteger) then
            begin
              inc(iC);

              quReplyList.Edit;
              quReplyListIACTIVE.AsInteger:=3;
              quReplyList.Post;
            end;
          end;
          if Pos('ns:Ticket',xmlstr)>0 then //��� �������������� ���� � ���������
          begin
            prWMemo(Memo1,'  '+quReplyListID.AsString+' - ���� ��������� ��������������� �����.');
            if prDecodeTicket(UTF8ToString(quReplyListReplyFile.AsString),quReplyListID.AsInteger) then
            begin
              inc(iC);

              quReplyList.Edit;
              quReplyListIACTIVE.AsInteger:=3;
              quReplyList.Post;
            end;
          end;
          if Pos('ns:ReplyNoAnswerTTN',xmlstr)>0 then //��� ������ �������������� ���
          begin
            prWMemo(Memo1,'  '+quReplyListID.AsString+' - ���� ��������� �������������� ���.');
            if prDecodeNoAnswerTTN(UTF8ToString(quReplyListReplyFile.AsString),quReplyListID.AsInteger) then
            begin
              inc(iC);

              quReplyList.Edit;
              quReplyListIACTIVE.AsInteger:=3;
              quReplyList.Post;
            end;
          end;
          if Pos('ns:ReplyBarcode',xmlstr)>0 then //��� ������ ���������� ����������
          begin
            prWMemo(Memo1,'  '+quReplyListID.AsString+' - ���� ��������� ���������� ����������.');
            if prDecodeQueryBarCode(UTF8ToString(quReplyListReplyFile.AsString),quReplyListID.AsInteger) then
            begin
              inc(iC);

              quReplyList.Edit;
              quReplyListIACTIVE.AsInteger:=3;
              quReplyList.Post;
            end;
          end;

          inc(iCAll);
          quReplyList.Next;
        end;
      finally
        prWMemo(Memo1,' ����� ������������ '+its(iC)+' �� '+its(iCAll));
      end;

    end;
  end;
end;
}

procedure prGetFilesAsw(Memo1:tcxMemo);
begin
  if dmR.FDConnection.Connected then
    prGetFilesAswPrivate(Memo1,dmR.FDConnection,CommonSet.IPUTM,CommonSet.FSRAR_ID);
end;


{
procedure prGetFilesAsw(Memo1:tcxMemo);
var
  httpsend: THTTPSend;
  RetXml: TNativeXml;
  nodeRoot,nodeURL: TXmlNode;
  I:Integer;
  RetId,RetVal:String;
  ReplyPath:string;
  iBs:Integer;

begin
  with dmR do
  begin
    if FDConnection.Connected then
    begin
      try
        prWMemo(Memo1,'�������� ����� �������.');

        quReplyList.Active:=False;
        quReplyList.ParamByName('RARID').AsString:=CommonSet.FSRAR_ID;
        quReplyList.Active:=True;
        quReplyList.First;
        while not quReplyList.Eof do
        begin
          try
            httpsend:=THTTPSend.Create;
            httpsend.Protocol:='1.1';
            httpsend.Status100:=True;

            ReplyPath:=quReplyListReplyPath.AsString;
            if Pos('localhost',ReplyPath)>0 then
            begin
              iBs:=Pos('localhost',ReplyPath);
              delete(ReplyPath,iBs,9);
              Insert(CommonSet.IPUTM,ReplyPath,iBs);
            end;

            if httpsend.HTTPMethod('get',ReplyPath) then
            begin
              if httpsend.ResultCode=200 then
              begin
                prWMemo(Memo1,'    TRUE '+'ResultCode='+IntToStr(httpsend.ResultCode)+' '+httpsend.ResultString);

                httpsend.Document.Position:=0;

                quReplyList.Edit;
                quReplyListReplyFile.AsString:='';
                quReplyListReplyFile.LoadFromStream(httpsend.Document);
                Delay(100);

                quReplyList.Post;
              end else prWMemo(Memo1,'    FALSE '+'ResultCode='+IntToStr(httpsend.ResultCode)+' '+httpsend.ResultString);
            end else
            begin
              prWMemo(Memo1,'    FALSE '+'ResultCode='+IntToStr(httpsend.ResultCode)+' '+httpsend.ResultString);
            end;
          finally
            httpsend.Free;
          end;

          quReplyList.Next;
        end;
      finally
      end;
    end;
  end;
end;
}

procedure prGetListAsk(Memo1:tcxMemo);
begin
  if dmR.FDConnection.Connected then
    prGetListAskPrivate(Memo1,dmR.FDConnection,CommonSet.IPUTM,CommonSet.FSRAR_ID);
end;

{
procedure prGetListAsk(Memo1:tcxMemo);  //OLD
var
  httpsend: THTTPSend;
  RetXml: TNativeXml;
  nodeRoot,nodeURL: TXmlNode;
  I:Integer;
  RetId,RetVal:String;

begin
  with dmR do
  begin
    if FDConnection.Connected then
    begin
      try
        httpsend:=THTTPSend.Create;
        RetXml := TNativeXml.Create(nil);

        if httpsend.HTTPMethod('get','http://'+CommonSet.IPUTM+':8080/opt/in') then
        begin
          prWMemo(Memo1,'    TRUE '+'ResultCode='+IntToStr(httpsend.ResultCode));

          httpsend.Document.Position:=0;
          RetXml.LoadFromStream(httpsend.Document);
          RetXml.XmlFormat := xfReadable;

          nodeRoot := RetXml.Root;
          if assigned(nodeRoot) then
          begin
            prWMemo(Memo1,'    �������� ���� "'+nodeRoot.Name+'" ���-�� ��������� '+inttostr(nodeRoot.AttributeCount)+', ���-�� ����� '+inttostr(nodeRoot.NodeCount));
            for I := 0 to nodeRoot.NodeCount - 1 do
            begin
              nodeURL:=nodeRoot.Nodes[I];
//              prWMemo(Memo1,'        �������� ���� "'+nodeURL.Name+'" ���-�� ��������� '+inttostr(nodeURL.AttributeCount)+', ���-�� ����� '+inttostr(nodeURL.NodeCount)+',�������� '+nodeURL.Value);
              RetId:=nodeURL.ReadAttributeString('replyId','');
              RetVal:=nodeURL.Value;
//              prWMemo(Memo1,RetId+'  '+RetVal);
              if RetId>'' then
              begin
                quAddToList.Active:=False;
                quAddToList.ParamByName('RETID').AsString:=RetId;
                quAddToList.ParamByName('RETVAL').AsString:=RetVal;
                quAddToList.ParamByName('RARID').AsString:=CommonSet.FSRAR_ID;
                quAddToList.ExecSQL;
              end;
            end;
          end;
        end else
        begin
          prWMemo(Memo1,'    FALSE '+'ResultCode='+IntToStr(httpsend.ResultCode));
//          prWMemo(Memo1,'ResultCode='+IntToStr(httpsend.ResultCode));
          prWMemo(Memo1,httpsend.ResultString);
        end;

      finally
        httpsend.Free;
        RetXml.Free;
      end;
    end;
  end;
end;
}

procedure prGetListAsw(Memo1:tcxMemo);
begin
  if dmR.FDConnection.Connected then
    prGetListAswPrivate(Memo1,dmR.FDConnection,CommonSet.IPUTM,CommonSet.FSRAR_ID);
end;

{
procedure prGetListAsw(Memo1:tcxMemo);
var
  httpsend: THTTPSend;
  RetXml: TNativeXml;
  nodeRoot,nodeURL: TXmlNode;
  I:Integer;
  RetId,RetVal:String;

begin
  with dmR do
  begin
    if FDConnection.Connected then
    begin
      try
        httpsend:=THTTPSend.Create;
        RetXml := TNativeXml.Create(nil);

        if httpsend.HTTPMethod('get','http://'+CommonSet.IPUTM+':8080/opt/out?refresh=true') then
        begin
          prWMemo(Memo1,'    TRUE '+'ResultCode='+IntToStr(httpsend.ResultCode));
//          prWMemo(Memo1,'ResultCode='+IntToStr(httpsend.ResultCode));
//          prWMemo(Memo1,httpsend.ResultString);
//          Memo1.Lines.LoadFromStream(httpsend.Document, TEncoding.UTF8);

          httpsend.Document.Position:=0;
          RetXml.LoadFromStream(httpsend.Document);
          RetXml.XmlFormat := xfReadable;

          nodeRoot := RetXml.Root;
          if assigned(nodeRoot) then
          begin
            prWMemo(Memo1,'    �������� ���� "'+nodeRoot.Name+'" ���-�� ��������� '+inttostr(nodeRoot.AttributeCount)+', ���-�� ����� '+inttostr(nodeRoot.NodeCount));
            for I := 0 to nodeRoot.NodeCount - 1 do
            begin
              nodeURL:=nodeRoot.Nodes[I];
//              prWMemo(Memo1,'        �������� ���� "'+nodeURL.Name+'" ���-�� ��������� '+inttostr(nodeURL.AttributeCount)+', ���-�� ����� '+inttostr(nodeURL.NodeCount)+',�������� '+nodeURL.Value);
              if Pos('url',nodeURL.Name)>0 then
              begin
                RetId:=Trim(nodeURL.ReadAttributeString('replyId',''));
                RetVal:=Trim(nodeURL.Value);
                prWMemo(Memo1,RetId+'  '+RetVal);
                if (RetId>'')or(RetVal>'') then
                begin
                  quAddToListReply.Active:=False;
                  quAddToListReply.ParamByName('RETID').AsString:=RetId;
                  quAddToListReply.ParamByName('RETVAL').AsString:=RetVal;
                  quAddToListReply.ParamByName('RARID').AsString:=CommonSet.FSRAR_ID;
                  quAddToListReply.ExecSQL;
                end;
              end;
            end;
          end;
        end else
        begin
          prWMemo(Memo1,'    FALSE '+'ResultCode='+IntToStr(httpsend.ResultCode));
//          prWMemo(Memo1,'ResultCode='+IntToStr(httpsend.ResultCode));
//          prWMemo(Memo1,httpsend.ResultString);
        end;

      finally
        httpsend.Free;
        RetXml.Free;
      end;
    end;
  end;
end;
}

function IdToName(Id:Integer):string;
begin
  Result:=IntToStr(Id);
  while Length(Result)<10 do Result:='0'+Result;
  Result:=Result+'.xml';
end;


procedure Delay(MSecs: Longint);
var
  FirstTickCount, Now: Longint;
begin
  FirstTickCount := GetTickCount;
  repeat
    Application.ProcessMessages;
    { allowing access to other controls, etc. }
    Now := GetTickCount;
  until (Now - FirstTickCount >= MSecs) or (Now < FirstTickCount);
end;


function fGetId(iT:Integer):Integer;
begin
 with dmR do
 begin
//  Result:=0;
  try
    quGetId.Active:=False;
    quGetId.ParamByName('ITN').Value:=iT;
    quGetId.Active:=True;
    Result:=quGetIdretnum.AsInteger;
    quGetId.Active:=False;
  except
    Result:=0;
  end;
 end;
end;


procedure TdmR.DataModuleCreate(Sender: TObject);
begin
//  DBConnect;
end;

function  TdmR.DBConnect(Server,Database,User_Name,Password:String): boolean;
var CurDir : string;
begin
  Result:=False;
  //����� �� ��������� ����� ���� � ����������� ���������� MSSQL � FireDAC
  //c������ ������ ������������ ����� udl �� ADO
  CurDir := ExtractFilePath(ParamStr(0));
  try

    FDConnection.Close;
    FDConnection.Params.Values['SERVER']:=Server;
    FDConnection.Params.Values['DATABASE']:=Database;
    FDConnection.Params.Values['User_Name']:=User_Name;
    FDConnection.Params.Values['Password']:=Password;
    FDConnection.Params.Values['ApplicationName']:=CommonSet.AppName;
    FDConnection.Params.Values['Workstation']:='';
    //FDConnection.Params.Values['MARS']:='yes';
    FDConnection.Params.Values['DriverID']:='MSSQL';
    FDConnection.Connected:=True;
   finally
  end;

  Sleep(100);
  Result:=FDConnection.Connected;
end;



procedure TdmR.quDocInHdCalcFields(DataSet: TDataSet);
begin
  quDocInHdXMLFILE.AsString:='XML �����';
end;

function GetKAP(IdGoods: Integer):String;
begin
  Result:='';
  with dmR do
  begin
    quGetKAP.Active:=False;
    quGetKAP.ParamByName('IdGoods').AsInteger:=IdGoods;
    quGetKAP.Active:=True;
    if quGetKAP.RecordCount>0 then
      Result:=quGetKAPId.AsString;
    quGetKAP.Active:=False;
  end;
end;

end.
