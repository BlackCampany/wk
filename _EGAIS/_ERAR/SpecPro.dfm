object fmSpecPro: TfmSpecPro
  Left = 0
  Top = 0
  Caption = #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1103' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
  ClientHeight = 512
  ClientWidth = 953
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object GridSpecPro: TcxGrid
    Left = 0
    Top = 127
    Width = 953
    Height = 385
    Align = alClient
    TabOrder = 0
    LookAndFeel.Kind = lfOffice11
    LookAndFeel.NativeStyle = False
    object ViewSpecPro: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.InfoPanel.Visible = True
      Navigator.Visible = True
      DataController.DataSource = dsquSpecPro
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'QUANT'
          Column = ViewSpecProQUANT
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.MultiSelect = True
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      object ViewSpecProIDHEAD: TcxGridDBColumn
        DataBinding.FieldName = 'IDHEAD'
        Visible = False
      end
      object ViewSpecProID: TcxGridDBColumn
        DataBinding.FieldName = 'ID'
        Width = 35
      end
      object ViewSpecProNUM: TcxGridDBColumn
        Caption = #8470' '#1087#1087
        DataBinding.FieldName = 'NUM'
        Width = 49
      end
      object ViewSpecProIDCARD: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1090#1086#1074#1072#1088#1072' '#1074' '#1087#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1077
        DataBinding.FieldName = 'IDCARD'
      end
      object ViewSpecProNAME: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'NAME'
        Width = 238
      end
      object ViewSpecProQUANT: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086
        DataBinding.FieldName = 'QUANT'
        Styles.Content = dmR.stlBold
        Styles.Footer = dmR.stlBold
      end
      object ViewSpecProIDM: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1077#1076'.'#1080#1079#1084'.'
        DataBinding.FieldName = 'IDM'
        Visible = False
      end
      object ViewSpecProNAMESHORT: TcxGridDBColumn
        Caption = #1045#1076'.'#1080#1079#1084'.'
        DataBinding.FieldName = 'NAMESHORT'
        Width = 67
      end
      object ViewSpecProKM: TcxGridDBColumn
        Caption = #1082#1086#1101#1092#1092'. '#1077#1076'. '#1080#1079#1084'.'
        DataBinding.FieldName = 'KM'
      end
      object ViewSpecProPRICEIN: TcxGridDBColumn
        Caption = #1062#1077#1085#1072' '#1079#1072#1082#1091#1087#1072' '#1089' '#1053#1044#1057
        DataBinding.FieldName = 'PRICEIN'
      end
      object ViewSpecProSUMIN: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1079#1072#1082#1091#1087#1072' '#1089' '#1053#1044#1057
        DataBinding.FieldName = 'SUMIN'
      end
      object ViewSpecProPRICEIN0: TcxGridDBColumn
        Caption = #1062#1077#1085#1072' '#1079#1072#1082#1091#1087#1072' '#1073#1077#1079' '#1053#1044#1057
        DataBinding.FieldName = 'PRICEIN0'
      end
      object ViewSpecProSUMIN0: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1079#1072#1082#1091#1087#1072' '#1073#1077#1079' '#1053#1044#1057
        DataBinding.FieldName = 'SUMIN0'
      end
    end
    object LevelSpecPro: TcxGridLevel
      GridView = ViewSpecPro
    end
  end
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 953
    Height = 127
    BarManager = dxBarManager1
    Style = rs2010
    ColorSchemeAccent = rcsaOrange
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 5
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1103' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar1'
        end>
      Index = 0
    end
  end
  object quSpecPro: TFDQuery
    Connection = fmProDoc.FDConnectPro
    SQL.Strings = (
      'SELECT sp.[IDHEAD]'
      '      ,sp.[ID]'
      '      ,sp.[NUM]'
      '      ,sp.[IDCARD]'
      #9'  ,ca.NAME'
      '      ,sp.[QUANT]'
      '      ,sp.[IDM]'
      #9'  ,me.NAMESHORT'
      '      ,sp.[KM]'
      '      ,sp.[PRICEIN]'
      '      ,sp.[SUMIN]'
      '      ,sp.[PRICEIN0]'
      '      ,sp.[SUMIN0]'
      '  FROM [dbo].[OF_DOCIN_SP] sp'
      '  left join [dbo].[OF_CARDS] ca on ca.ID=sp.[IDCARD]'
      '  left join [dbo].[OF_MESSUR] me on me.ID=sp.[IDM]'
      '  where sp.[IDHEAD]=:IDH'
      '  order by sp.[ID]')
    Left = 192
    Top = 336
    ParamData = <
      item
        Name = 'IDH'
        DataType = ftString
        ParamType = ptInput
        Value = '168727'
      end>
    object quSpecProIDHEAD: TLargeintField
      FieldName = 'IDHEAD'
      Origin = 'IDHEAD'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quSpecProID: TLargeintField
      AutoGenerateValue = arAutoInc
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object quSpecProNUM: TIntegerField
      FieldName = 'NUM'
      Origin = 'NUM'
    end
    object quSpecProIDCARD: TIntegerField
      FieldName = 'IDCARD'
      Origin = 'IDCARD'
    end
    object quSpecProNAME: TStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      Size = 200
    end
    object quSpecProQUANT: TFloatField
      FieldName = 'QUANT'
      Origin = 'QUANT'
      DisplayFormat = '0.000'
    end
    object quSpecProIDM: TIntegerField
      FieldName = 'IDM'
      Origin = 'IDM'
    end
    object quSpecProNAMESHORT: TStringField
      FieldName = 'NAMESHORT'
      Origin = 'NAMESHORT'
      Size = 50
    end
    object quSpecProKM: TSingleField
      FieldName = 'KM'
      Origin = 'KM'
    end
    object quSpecProPRICEIN: TFloatField
      FieldName = 'PRICEIN'
      Origin = 'PRICEIN'
      DisplayFormat = '0.00'
    end
    object quSpecProSUMIN: TFloatField
      FieldName = 'SUMIN'
      Origin = 'SUMIN'
      DisplayFormat = '0.00'
    end
    object quSpecProPRICEIN0: TFloatField
      FieldName = 'PRICEIN0'
      Origin = 'PRICEIN0'
      DisplayFormat = '0.00'
    end
    object quSpecProSUMIN0: TFloatField
      FieldName = 'SUMIN0'
      Origin = 'SUMIN0'
      DisplayFormat = '0.00'
    end
  end
  object dsquSpecPro: TDataSource
    DataSet = quSpecPro
    Left = 192
    Top = 400
  end
  object ActionManager1: TActionManager
    LargeImages = dmR.LargeImage
    Images = dmR.SmallImage
    Left = 168
    Top = 208
    StyleName = 'Platform Default'
    object acClose: TAction
      Caption = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 100
      OnExecute = acCloseExecute
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      #1044#1077#1081#1089#1090#1074#1080#1103)
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmR.SmallImage
    ImageOptions.LargeImages = dmR.LargeImage
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 260
    Top = 212
    DockControlHeights = (
      0
      0
      0
      0)
    object dxBarManager1Bar1: TdxBar
      Caption = #1047#1072#1082#1088#1099#1090#1100
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 925
      FloatTop = 8
      FloatClientWidth = 92
      FloatClientHeight = 54
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = acClose
      Category = 0
    end
    object dxBarGroup1: TdxBarGroup
      Items = ()
    end
  end
end
