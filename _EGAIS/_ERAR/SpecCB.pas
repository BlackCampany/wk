unit SpecCB;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  CustomChildForm, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  Data.DB, cxDBData, cxImageComboBox, cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, dxRibbonSkins, dxRibbonCustomizationForm, dxRibbon, dxBar, System.Actions, Vcl.ActnList,
  Vcl.PlatformDefaultStyleActnCtrls, Vcl.ActnMan;

type
  TfmSpecCB = class(TfmCustomChildForm)
    quSpecCB: TFDQuery;
    dsquSpecCB: TDataSource;
    GridSpecCB: TcxGrid;
    ViewSpecCB: TcxGridDBTableView;
    ViewSpecCBId: TcxGridDBColumn;
    ViewSpecCBArticul: TcxGridDBColumn;
    ViewSpecCBEdIzm: TcxGridDBColumn;
    ViewSpecCBName: TcxGridDBColumn;
    ViewSpecCBQuant: TcxGridDBColumn;
    ViewSpecCBPriceIn: TcxGridDBColumn;
    ViewSpecCBSumIn: TcxGridDBColumn;
    ViewSpecCBPriceOut: TcxGridDBColumn;
    ViewSpecCBSumOut: TcxGridDBColumn;
    ViewSpecCBFullName: TcxGridDBColumn;
    LevelSpecCB: TcxGridLevel;
    ActionManager1: TActionManager;
    acClose: TAction;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarGroup1: TdxBarGroup;
    dxRibbon1: TdxRibbon;
    dxRibbon1Tab1: TdxRibbonTab;
    quSpecCBId_TTN: TLargeintField;
    quSpecCBId: TIntegerField;
    quSpecCBArticul: TIntegerField;
    quSpecCBName: TStringField;
    quSpecCBFullName: TStringField;
    quSpecCBEdIzm: TSmallintField;
    quSpecCBQuant: TSingleField;
    quSpecCBPriceIn: TFloatField;
    quSpecCBPriceOut: TFloatField;
    quSpecCBSumIn: TSingleField;
    quSpecCBSumOut: TSingleField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acCloseExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

//var
//  fmSpecCB: TfmSpecCB;

implementation

uses
  dmRar;

{$R *.dfm}

procedure TfmSpecCB.acCloseExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmSpecCB.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
end;

end.
