unit ViewSpecOut;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  CustomChildForm, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer,
  cxTextEdit, cxCalendar, cxCheckBox, cxDBLookupComboBox, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error,
  FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Vcl.Menus, cxClasses, FireDAC.Comp.Client,
  FireDAC.Comp.DataSet, System.Actions, Vcl.ActnList, dxBar, cxBarEditItem, cxMemo, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridCustomView, cxGrid, dxRibbon;

type
  TfmDocSpecOut = class(TfmCustomChildForm)
    dxBarManager1: TdxBarManager;
    dxBarGroup1: TdxBarGroup;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    dxBarManager1Bar1: TdxBar;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    ActionList1: TActionList;
    acClose: TAction;
    acSave: TAction;
    quSpecOut: TFDQuery;
    UpdSQL1: TFDUpdateSQL;
    dsquSpecOut: TDataSource;
    ViewSpecOut: TcxGridDBTableView;
    LevelSpec: TcxGridLevel;
    GridSpec: TcxGrid;
    ViewSpecOutNUM: TcxGridDBColumn;
    ViewSpecOutALCCODE: TcxGridDBColumn;
    ViewSpecOutQUANT: TcxGridDBColumn;
    ViewSpecOutPRICE: TcxGridDBColumn;
    ViewSpecOutQUANTF: TcxGridDBColumn;
    ViewSpecOutNAME: TcxGridDBColumn;
    ViewSpecOutVOL: TcxGridDBColumn;
    ViewSpecOutKREP: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    ViewSpecOutWBREGID: TcxGridDBColumn;
    ViewSpecOutCODECB: TcxGridDBColumn;
    PopupMenu1: TPopupMenu;
    cxStyle2: TcxStyle;
    ViewSpecOutQUANTREMN: TcxGridDBColumn;
    quSpecOutFSRARID: TStringField;
    quSpecOutIDATE: TIntegerField;
    quSpecOutSIDHD: TStringField;
    quSpecOutID: TLargeintField;
    quSpecOutNUM: TIntegerField;
    quSpecOutALCCODE: TStringField;
    quSpecOutQUANT: TFloatField;
    quSpecOutQUANTREMN: TFloatField;
    quSpecOutQUANTF: TFloatField;
    quSpecOutPRICE: TFloatField;
    quSpecOutPRICEF: TFloatField;
    quSpecOutNAME: TMemoField;
    quSpecOutVOL: TSingleField;
    quSpecOutKREP: TSingleField;
    quSpecOutPRODNAME: TMemoField;
    quSpecOutWBREGID: TStringField;
    quSpecOutCODECB: TIntegerField;
    quSpecOutINFO_A: TStringField;
    quSpecOutINFO_B: TStringField;
    ViewSpecOutINFO_A: TcxGridDBColumn;
    ViewSpecOutINFO_B: TcxGridDBColumn;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    cxBarEditItem2: TcxBarEditItem;
    quE: TFDQuery;
    quSpecOutPRISEF0: TFloatField;
    acTestSpec: TAction;
    dxBarLargeButton4: TdxBarLargeButton;
    acDelPos: TAction;
    N1: TMenuItem;
    quSpecOutSUMF: TFloatField;
    ViewSpecOutSUMF: TcxGridDBColumn;
    dxBarManager1Bar2: TdxBar;
    acAddPosSpecOut: TAction;
    N2: TMenuItem;
    procedure acCloseExecute(Sender: TObject);
    procedure acSaveExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acTestSpecExecute(Sender: TObject);
    procedure acDelPosExecute(Sender: TObject);
    procedure quSpecOutCalcFields(DataSet: TDataSet);
    procedure acAddPosSpecOutExecute(Sender: TObject);
  private
    { Private declarations }
    FRARID:string;
    FIDATE: Integer;
    FSID:string;
    FIEDIT: Integer;
    FSDATE:string;
    FNUMBER:string;
    SCLI:string;
    FDATEDOC:TDateTime;
    FIDHEAD:Integer;
    FSDEP:string;

    procedure Init;

  public
    { Public declarations }
  end;

// var
//  fmDocSpec: TfmDocSpec;

procedure ShowSpecViewOut(RARID,SID,FSDATE,FNUMBER,SCLI:string;IDATE,IEDIT:Integer;DATEDOC:TDateTime;IDHEAD:Integer;SDEP:String);

implementation

{$R *.dfm}

uses ViewRar, dmRar, EgaisDecode, AddPosOutSpec;

procedure ShowSpecViewOut(RARID,SID,FSDATE,FNUMBER,SCLI:string;IDATE,IEDIT:Integer;DATEDOC:TDateTime;IDHEAD:Integer;SDEP:String);
var F:TfmDocSpecOut;
begin
  F:=TfmDocSpecOut.Create(Application);
//  ShowMessage(Xmlstr);
  F.FRARID:=RARID;
  F.FIDATE:=IDATE;
  F.FSID:=SID;
  F.FIEDIT:=IEDIT;

  F.FSDATE:=FSDATE;
  F.FNUMBER:=FNUMBER;
  F.SCLI:=SCLI;
  F.FDATEDOC:=DATEDOC;
  F.FIDHEAD:=IDHEAD;
  F.FSDEP:=Trim(SDEP);

  F.Init;
  F.Show;
end;

procedure TfmDocSpecOut.acAddPosSpecOutExecute(Sender: TObject);
begin
  //�������� �������
  if FIEDIT=1 then
  begin
    if not Assigned(fmAddPosOut) then fmAddPosOut:=TfmAddPosOut.Create(Application,TFormStyle.fsNormal);

    fmAddPosOut.cxTextEdit1.Text:='';
    fmAddPosOut.cxTextEdit2.Text:='';
    fmAddPosOut.cxTextEdit3.Text:='';

    fmAddPosOut.cxCalcEdit1.Value:=0;
    fmAddPosOut.Label5.Caption:='';
    fmAddPosOut.ShowModal;
    if fmAddPosOut.ModalResult=mrOk then
    begin //�������� �������
      with fmAddPosOut do
      with dmR do
      begin
        quSelCard.Active:=False;
        quSelCard.ParamByName('CODE').AsString:=fmAddPosOut.cxTextEdit1.Text;
        quSelCard.Active:=True;

        if quSelCard.RecordCount=1 then
        begin
          quSpecOut.Append;
          quSpecOutFSRARID.asstring:=quDocOutHdFSRARID.AsString;
          quSpecOutIDATE.AsInteger:=quDocOutHdIDATE.AsInteger;
          quSpecOutSIDHD.asstring:=quDocOutHdSID.AsString;
          quSpecOutNUM.AsInteger:=quSpecOut.RecordCount+1;
          quSpecOutALCCODE.asstring:=fmAddPosOut.cxTextEdit1.Text;
//          quSpecOutQUANT.AsFloat:=0;
//          quSpecOutQUANTREMN.AsFloat:=0;
          quSpecOutQUANTF.AsFloat:=fmAddPosOut.cxCalcEdit1.Value;
          quSpecOutPRICE.AsFloat:=fmAddPosOut.cxCurrencyEdit1.Value;
          quSpecOutPRICEF.AsFloat:=fmAddPosOut.cxCurrencyEdit1.Value;
          quSpecOutNAME.AsString:=quSelCardNAME.AsString;
          quSpecOutVOL.AsFloat:=quSelCardVOL.AsFloat;
          quSpecOutKREP.AsFloat:=quSelCardKREP.AsFloat;
          quSpecOutPRODNAME.AsString:='';
          quSpecOutWBREGID.asstring:=fmAddPosOut.cxTextEdit3.Text;
          quSpecOutCODECB.AsInteger:=0;
          quSpecOutINFO_A.asstring:=fmAddPosOut.cxTextEdit2.Text;
          quSpecOutINFO_B.asstring:='';
          quSpecOutPRISEF0.AsFloat:=fmAddPosOut.cxCurrencyEdit1.Value;
          quSpecOut.Post;
        end;

        quSelCard.Active:=False;
      end;
    end;
  end;
end;

procedure TfmDocSpecOut.acCloseExecute(Sender: TObject);
begin
//  quSpecIn.Active:=False;
  Close;
end;

procedure TfmDocSpecOut.acDelPosExecute(Sender: TObject);
begin
  if FIEDIT=1 then
  begin
    quSpecOut.Delete;
  end;
end;

procedure TfmDocSpecOut.acSaveExecute(Sender: TObject);
var iErrors:Integer;
begin
  //��������� ������������
  quSpecOut.UpdateTransaction.StartTransaction;
  iErrors := quSpecOut.ApplyUpdates;
  if iErrors = 0 then begin
    try
      quSpecOut.CommitUpdates;
      quSpecOut.UpdateTransaction.Commit;

      quE.Active:=False;
      quE.SQL.Clear;
      quE.SQL.Add('UPDATE [dbo].[ADOCSOUT_HD]');
      quE.SQL.Add('Set');
      quE.SQL.Add('[DATEDOC] ='''+dssql(cxBarEditItem2.EditValue)+'''');
      quE.SQL.Add(',[NUMBER]='''+Trim(cxBarEditItem1.EditValue)+'''');
      quE.SQL.Add('WHERE');
      quE.SQL.Add('[FSRARID] = '''+FRARID+'''');
      quE.SQL.Add('and [IDATE] ='+its(FIDATE));
      quE.SQL.Add('and [SID] ='''+FSID+'''');
      quE.ExecSQL;

      dmR.quDocOutHd.Refresh;
    except
      ShowMessage('������ ����������..');
    end;
  end else
  begin
    ShowMessage('������ ����������.');
    quSpecOut.UpdateTransaction.Rollback;
  end;
end;

procedure TfmDocSpecOut.acTestSpecExecute(Sender: TObject);
var rQIn,rQOut:Real;
    bErr:Boolean;
begin
  with dmR do
  begin
    bErr:=False;
    ClearMessageLogLocal;
    ShowMessageLogLocal('����� .. ���� �������� ������������ �� ��������.');
    quSpecOut.First;
    while not quSpecOut.Eof do
    begin
      ShowMessageLogLocal('  ���. '+quSpecOutNUM.AsString+'  '+quSpecOutALCCODE.AsString+'  '+quSpecOutNAME.AsString+'  � - '+quSpecOutINFO_A.AsString+'  � - '+quSpecOutWBREGID.AsString);

      rQIn:=0;
      //��� ������ �� ������
      try
        quS.Active:=False;
        quS.SQL.Clear;
        quS.SQL.Add('');

        quS.SQL.Add('SELECT isnull(SUM([QUANT]),0) as QUANT');
        quS.SQL.Add('  FROM [dbo].[ADOCSIN_SP]');
        quS.SQL.Add('  where [ALCCODE]='''+quSpecOutALCCODE.AsString+'''');
        quS.SQL.Add('  and [WBREGID]='''+quSpecOutWBREGID.AsString+'''');
        quS.SQL.Add('  and [INFO_A]='''+quSpecOutINFO_A.AsString+'''');
        quS.Active:=True;
        if quS.RecordCount>0 then rQIn:=quS.FieldByName('QUANT').AsFloat;
        quS.Active:=False;
      except
      end;

      rQOut:=0;

      //��� ��� ��������� ������ �� ������
      try
        quS.Active:=False;
        quS.SQL.Clear;
        quS.SQL.Add('');
        quS.SQL.Add('SELECT isnull(SUM([QUANTF]),0) as QUANT');
        quS.SQL.Add('  FROM [dbo].[ADOCSOUT_SP]');
        quS.SQL.Add('  where [ALCCODE]='''+quSpecOutALCCODE.AsString+'''');
        quS.SQL.Add('  and [WBREGID]='''+quSpecOutWBREGID.AsString+'''');
        quS.SQL.Add('  and [INFO_A]='''+quSpecOutINFO_A.AsString+'''');
        quS.SQL.Add('  and [SIDHD]<>'''+quSpecOutSIDHD.AsString+'''');
        quS.Active:=True;
        if quS.RecordCount>0 then rQOut:=quS.FieldByName('QUANT').AsFloat;
        quS.Active:=False;

      except
      end;

      //����� ������
      if (rQIn-rQOut-quSpecOutQUANTF.AsFloat)>=0 then
      begin
        ShowMessageLogLocal('      ��. ������ = '+fts(rQIn)+'   ������ = '+fts(rQOut)+'  ������� � �������� = '+fts(rQIn-rQOut)+'   � �������� = '+fts(quSpecOutQUANTF.AsFloat)+'. ��');
      end
      else
      begin
        bErr:=True;
        ShowMessageLogLocal('      ������. ������ = '+fts(rQIn)+'   ������ = '+fts(rQOut)+'  ������� � �������� = '+fts(rQIn-rQOut)+'   � �������� = '+fts(quSpecOutQUANTF.AsFloat)+'. ������� �� ��������.');
      end;


      quSpecOut.Next;
    end;
    if bErr then ShowMessageLogLocal('������� �������� c ��������. ������� ����������� ����������.')
    else ShowMessageLogLocal('������� �������� ��� ������.');
  end;
end;

procedure TfmDocSpecOut.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
end;

procedure TfmDocSpecOut.Init;
begin
  Caption:='������������ �� ������� '+FSDATE+', '+FNUMBER+', '+SCLI+' ('+ FSID+')';

  cxBarEditItem1.EditValue:=FNUMBER;
  cxBarEditItem2.EditValue:=FDATEDOC;

  ViewSpecOut.BeginUpdate;
  quSpecOut.Active:=False;
  quSpecOut.SQL.Clear;
  quSpecOut.SQL.Add('');
  quSpecOut.SQL.Add('declare @FSRARID varchar(50) = '''+FRARID+'''');
  quSpecOut.SQL.Add('declare @IDATE int = '+its(FIDATE));
  quSpecOut.SQL.Add('declare @SID varchar(50) = '''+FSID+'''');
  quSpecOut.SQL.Add('declare @IDHEADIN int = '+its(FIDHEAD));
  quSpecOut.SQL.Add('SELECT sp.[FSRARID],sp.[IDATE],sp.[SIDHD],sp.[ID],sp.[NUM],sp.[ALCCODE],sp.[INFO_A],sp.[INFO_B]');
  quSpecOut.SQL.Add('       ,sp.[QUANT]');
  quSpecOut.SQL.Add('  	   ,(sp.[QUANT]-isnull(spremn.QUANTREMN,0)) as QUANTREMN');
  quSpecOut.SQL.Add('	   ,sp.[QUANTF]');
  quSpecOut.SQL.Add('	   ,sp.[PRICE],sp.[PRICEF],sp.[PRISEF0]');
  quSpecOut.SQL.Add('       ,ca.NAME, ca.VOL, ca.KREP, prod.NAME as PRODNAME');
  quSpecOut.SQL.Add('	   ,sp.[WBREGID]');
  quSpecOut.SQL.Add('    ,0 as [CODECB]');
  quSpecOut.SQL.Add('  FROM [dbo].[ADOCSOUT_SP] sp');
  quSpecOut.SQL.Add('  left join [dbo].[ACARDS] ca on ca.KAP=sp.[ALCCODE]');
  quSpecOut.SQL.Add('  left join [dbo].[APRODS] prod on prod.PRODID=ca.PRODID');
  quSpecOut.SQL.Add('  left join ( SELECT spo.[ALCCODE]');
  quSpecOut.SQL.Add('				     ,spo.[WBREGID]');
  quSpecOut.SQL.Add('					 ,isnull(SUM(spo.[QUANTF]),0) as QUANTREMN');
  quSpecOut.SQL.Add('				FROM [dbo].[ADOCSOUT_SP] spo');
  quSpecOut.SQL.Add('				left join [dbo].[ADOCSOUT_HD] hdo on hdo.[FSRARID]=spo.[FSRARID] and hdo.[IDATE]=spo.[IDATE] and hdo.[SID]=spo.[SIDHD]');
  quSpecOut.SQL.Add('				where hdo.[IDHEAD]=@IDHEADIN and (hdo.IACTIVE=1 or hdo.READYSEND=1)');
  quSpecOut.SQL.Add('				group by spo.[ALCCODE],spo.[WBREGID]) spremn on spremn.ALCCODE=sp.[ALCCODE] and spremn.WBREGID=sp.[WBREGID]');
  quSpecOut.SQL.Add('  where sp.[FSRARID]=@FSRARID');
  quSpecOut.SQL.Add('  and sp.[IDATE]=@IDATE');
  quSpecOut.SQL.Add('  and sp.[SIDHD]=@SID');

  {
    F.FIDHEAD:=IDHEAD;
  F.FSDEP:=SDEP;

  quSpecOut.ParamByName('RARID').AsString:=FRARID;
  quSpecOut.ParamByName('IDATE').AsInteger:=FIDATE;
  quSpecOut.ParamByName('SID').AsString:=FSID;
   }

  quSpecOut.Active:=True;
  ViewSpecOut.EndUpdate;

  if FIEDIT=1 then
  begin
    acSave.Enabled:=True;
    ViewSpecOut.OptionsData.Editing:=True;
//    ViewSpecOutQUANTF.Options.Editing:=True;
    cxBarEditItem1.Properties.ReadOnly:=False;
    cxBarEditItem2.Properties.ReadOnly:=False;
  end else
  begin
    acSave.Enabled:=False;
    ViewSpecOut.OptionsData.Editing:=False;
    ViewSpecOutQUANTF.Options.Editing:=False;
    cxBarEditItem1.Properties.ReadOnly:=True;
    cxBarEditItem2.Properties.ReadOnly:=True;
  end;
end;

procedure TfmDocSpecOut.quSpecOutCalcFields(DataSet: TDataSet);
begin
  DataSet.FieldByName('SUMF').AsFloat:=DataSet.FieldByName('QUANTF').AsFloat*DataSet.FieldByName('PRICE').AsFloat;
end;

end.
