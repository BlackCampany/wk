object fmViewDocRet: TfmViewDocRet
  Left = 0
  Top = 0
  Caption = #1044#1086#1082#1091#1084#1077#1085#1090#1099' '#1042#1086#1079#1074#1088#1072#1090#1072' '#1090#1086#1074#1072#1088#1072
  ClientHeight = 558
  ClientWidth = 1272
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1272
    Height = 127
    ApplicationButton.Visible = False
    BarManager = dxBarManager1
    Style = rs2010
    ColorSchemeAccent = rcsaOrange
    ColorSchemeName = 'Blue'
    SupportNonClientDrawing = True
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1044#1086#1082#1091#1084#1077#1085#1090#1099' '#1074#1086#1079#1074#1088#1072#1090#1072' '#1090#1086#1074#1072#1088#1072
      Groups = <
        item
          ToolbarName = 'bmApplyDate'
        end
        item
          ToolbarName = 'bmClose'
        end>
      Index = 0
    end
  end
  object GridDocRet: TcxGrid
    Left = 0
    Top = 127
    Width = 1272
    Height = 431
    Align = alClient
    TabOrder = 5
    LevelTabs.Style = 8
    LookAndFeel.Kind = lfOffice11
    LookAndFeel.NativeStyle = False
    OnFocusedViewChanged = GridDocRetFocusedViewChanged
    object ViewDocsOut: TcxGridDBTableView
      PopupMenu = pmDocsOut
      OnDblClick = ViewDocsOutDblClick
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.InfoPanel.Visible = True
      Navigator.Visible = True
      DataController.DataSource = dmR.dsquDocsOut
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      object ViewDocsOutFSRARID: TcxGridDBColumn
        DataBinding.FieldName = 'FSRARID'
        Options.Editing = False
        Width = 86
      end
      object ViewDocsOutSID: TcxGridDBColumn
        DataBinding.FieldName = 'SID'
        Options.Editing = False
        Width = 81
      end
      object ViewDocsOutCLIFTO: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
        DataBinding.FieldName = 'CLIFTO'
        Options.Editing = False
        Width = 94
      end
      object ViewDocsOutIDATE: TcxGridDBColumn
        DataBinding.FieldName = 'IDATE'
        Visible = False
        Options.Editing = False
      end
      object ViewDocsOutDDATE: TcxGridDBColumn
        Caption = #1044#1072#1090#1072' '#1092#1086#1088#1084#1080#1088#1086#1074#1072#1085#1080#1103
        DataBinding.FieldName = 'DDATE'
        Options.Editing = False
        Width = 103
      end
      object ViewDocsOutDATE_OUTPUT: TcxGridDBColumn
        Caption = #1044#1072#1090#1072' '#1086#1090#1087#1088#1072#1074#1082#1080
        DataBinding.FieldName = 'DATE_OUTPUT'
        Options.Editing = False
        Width = 84
      end
      object ViewDocsOutDATEDOC: TcxGridDBColumn
        Caption = #1044#1072#1090#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
        DataBinding.FieldName = 'DATEDOC'
        Width = 113
      end
      object ViewDocsOutNUMBER: TcxGridDBColumn
        Caption = #1053#1086#1084#1077#1088' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
        DataBinding.FieldName = 'NUMBER'
        Width = 99
      end
      object ViewDocsOutNAME: TcxGridDBColumn
        Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
        DataBinding.FieldName = 'NAME'
        Options.Editing = False
        Width = 122
      end
      object ViewDocsOutIACTIVE: TcxGridDBColumn
        Caption = #1057#1090#1072#1090#1091#1089' '#1087#1077#1088#1077#1076#1072#1095#1080' '#1074' '#1045#1043#1040#1048#1057
        DataBinding.FieldName = 'IACTIVE'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Images = dmR.SmallImage
        Properties.Items = <
          item
            Description = #1074' '#1086#1073#1088#1072#1073#1086#1090#1082#1077
            ImageIndex = 74
            Value = '0'
          end
          item
            Description = #1054#1090#1087#1088#1072#1074#1083#1077#1085
            ImageIndex = 75
            Value = '1'
          end>
        Options.Editing = False
      end
      object ViewDocsOutREADYSEND: TcxGridDBColumn
        Caption = #1057#1090#1072#1090#1091#1089' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
        DataBinding.FieldName = 'READYSEND'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Images = dmR.SmallImage
        Properties.Items = <
          item
            Description = #1075#1086#1090#1086#1074' '#1082' '#1087#1077#1088#1077#1076#1072#1095#1077
            ImageIndex = 1
            Value = 1
          end
          item
            Description = #1074' '#1088#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1085#1080#1080
            ImageIndex = 26
            Value = 0
          end>
        Options.Editing = False
      end
      object ViewDocsOutTT1: TcxGridDBColumn
        Caption = #1042#1086#1079#1074#1088#1072#1090' '#1087#1088#1080#1085#1103#1090' '#1074' '#1045#1043#1040#1048#1057
        DataBinding.FieldName = 'TT1'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Images = dmR.SmallImage
        Properties.Items = <
          item
            ImageIndex = 154
            Value = 0
          end
          item
            ImageIndex = 156
            Value = 1
          end>
      end
      object ViewDocsOutTT2: TcxGridDBColumn
        Caption = #1044#1086#1082#1091#1084#1077#1085#1090' '#1087#1088#1086#1074#1077#1076#1077#1085' '#1074' '#1045#1043#1040#1048#1057
        DataBinding.FieldName = 'TT2'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Images = dmR.SmallImage
        Properties.Items = <
          item
            ImageIndex = 154
            Value = 0
          end
          item
            ImageIndex = 156
            Value = 1
          end>
      end
      object ViewDocsOutWBREGID: TcxGridDBColumn
        DataBinding.FieldName = 'WBREGID'
        Options.Editing = False
        Width = 75
      end
      object ViewDocsOutFIXNUMBER: TcxGridDBColumn
        DataBinding.FieldName = 'FIXNUMBER'
        Options.Editing = False
        Width = 84
      end
      object ViewDocsOutFIXDATE: TcxGridDBColumn
        DataBinding.FieldName = 'FIXDATE'
        Options.Editing = False
        Width = 61
      end
      object ViewDocsOutIDHEAD: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1087#1088#1080#1093'. '#1076#1086#1082#1091#1084#1077#1085#1090#1072' '#1052#1050#1088#1080#1089#1090#1072#1083
        DataBinding.FieldName = 'IDHEAD'
        Options.Editing = False
        Width = 87
      end
      object ViewDocsOutFULLNAME: TcxGridDBColumn
        Caption = #1055#1086#1083#1085#1086#1077' '#1085#1072#1079#1074#1072#1085#1080#1077' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
        DataBinding.FieldName = 'FULLNAME'
        Options.Editing = False
        Width = 168
      end
      object ViewDocsOutCLIENTINN: TcxGridDBColumn
        Caption = #1048#1053#1053' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
        DataBinding.FieldName = 'CLIENTINN'
        Options.Editing = False
        Width = 93
      end
      object ViewDocsOutCLIENTKPP: TcxGridDBColumn
        Caption = #1050#1055#1055' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
        DataBinding.FieldName = 'CLIENTKPP'
        Options.Editing = False
        Width = 95
      end
      object ViewDocsOutXMLF: TcxGridDBColumn
        Caption = 'XML'
        DataBinding.FieldName = 'XMLF'
        Options.Editing = False
        Width = 46
      end
    end
    object LevelDocsOut: TcxGridLevel
      Caption = #1042#1086#1079#1074#1088#1072#1090#1085#1099#1077' '#1076#1086#1082#1091#1084#1077#1085#1090#1099
      GridView = ViewDocsOut
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      #1044#1077#1081#1089#1090#1074#1080#1103)
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmR.SmallImage
    ImageOptions.LargeImages = dmR.LargeImage
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 708
    Top = 204
    DockControlHeights = (
      0
      0
      0
      0)
    object bmApplyDate: TdxBar
      Caption = #1044#1077#1081#1089#1090#1074#1080#1103
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 778
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          UserDefine = [udWidth]
          UserWidth = 75
          Visible = True
          ItemName = 'deDateBeg'
        end
        item
          UserDefine = [udWidth]
          UserWidth = 74
          Visible = True
          ItemName = 'deDateEnd'
        end
        item
          Visible = True
          ItemName = 'btnDone'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end
        item
          Visible = True
          ItemName = 'dxBarButton3'
        end
        item
          Visible = True
          ItemName = 'dxBarButton1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object bmClose: TdxBar
      Caption = #1047#1072#1082#1088#1099#1090#1100
      CaptionButtons = <>
      DockedLeft = 583
      DockedTop = 0
      FloatLeft = 778
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'btnClose'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object deDateBeg: TdxBarDateCombo
      Caption = 'C  '
      Category = 0
      Hint = 'C  '
      Visible = ivAlways
      OnChange = deDateBegChange
      ImageIndex = 25
      ShowDayText = False
    end
    object deDateEnd: TdxBarDateCombo
      Caption = #1087#1086
      Category = 0
      Hint = #1087#1086
      Visible = ivAlways
      OnChange = deDateBegChange
      ImageIndex = 25
      ShowDayText = False
    end
    object btnDone: TdxBarLargeButton
      Action = acRefresh
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = acUTMProc
      Category = 0
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = acUTMExch
      Category = 0
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = acUTMArh
      Category = 0
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = #1048#1053#1053' '
      Category = 0
      Hint = #1048#1053#1053' '
      Visible = ivAlways
      PropertiesClassName = 'TcxTextEditProperties'
      BarStyleDropDownButton = False
      Properties.AutoSelect = False
      Properties.IncrementalSearch = False
      Properties.ValidationOptions = [evoAllowLoseFocus]
      InternalEditValue = ''
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = acProd
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = acSendStatus
      Caption = #1056#1072#1079#1088#1077#1096#1080#1090#1100' '#1086#1090#1087#1088#1072#1074#1082#1091'  '
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = acMCrystalDocs
      Category = 0
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = acProDocs
      Category = 0
    end
    object beiGetTTN: TcxBarEditItem
      Caption = #8470' TTN'
      Category = 0
      Hint = #8470' TTN'
      Visible = ivAlways
      PropertiesClassName = 'TcxTextEditProperties'
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Action = acGetTTN
      Category = 0
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Action = acGetRawTTNList
      Category = 0
    end
    object btnClose: TdxBarLargeButton
      Action = acClose
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = acQueryFormF1
      Caption = #1047#1072#1087#1088#1086#1089' '#1089#1087#1088#1072#1074#1082#1080' 1'
      Category = 0
      Visible = ivNever
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = acQueryRests
      Category = 0
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = acQueryBarcode
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = acCBDoc
      Category = 0
    end
    object dxBarButton1: TdxBarButton
      Action = acSendRet
      Category = 0
    end
    object dxBarButton2: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarButton3: TdxBarButton
      Action = acGetRetDocs
      Category = 0
    end
    object dxBarGroup1: TdxBarGroup
      Items = ()
    end
  end
  object ActionList1: TActionList
    Images = dmR.SmallImage
    Left = 536
    Top = 236
    object acRefresh: TAction
      Category = #1060#1086#1088#1084#1072
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100' (F5)'
      Hint = #1054#1073#1085#1086#1074#1080#1090#1100' (F5)'
      ImageIndex = 5
      ShortCut = 116
      OnExecute = acRefreshExecute
    end
    object acClose: TAction
      Category = #1060#1086#1088#1084#1072
      Caption = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 100
      OnExecute = acCloseExecute
    end
    object acSelectDate: TAction
      Category = #1060#1086#1088#1084#1072
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      Hint = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      ImageIndex = 1
      OnExecute = acSelectDateExecute
    end
    object acDecodeTovar: TAction
      Category = #1044#1077#1082#1086#1076#1080#1088#1086#1074#1072#1085#1080#1077
      Caption = #1044#1077#1082#1086#1076#1080#1088#1086#1074#1072#1090#1100' '#1082#1072#1082' '#1090#1086#1074#1072#1088
      ImageIndex = 38
    end
    object acSaveToFile: TAction
      Category = #1044#1077#1082#1086#1076#1080#1088#1086#1074#1072#1085#1080#1077
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1074' '#1092#1072#1081#1083
      ImageIndex = 7
    end
    object acDelList: TAction
      Category = #1044#1077#1082#1086#1076#1080#1088#1086#1074#1072#1085#1080#1077
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1074#1099#1076#1077#1083#1077#1085#1085#1099#1077' '#1089#1090#1088#1086#1082#1080
      ImageIndex = 4
      ShortCut = 16452
    end
    object acDecodeTTN: TAction
      Category = #1044#1077#1082#1086#1076#1080#1088#1086#1074#1072#1085#1080#1077
      Caption = #1044#1077#1082#1086#1076#1080#1088#1086#1074#1072#1090#1100' '#1082#1072#1082' '#1076#1086#1082#1091#1084#1077#1085#1090
      ImageIndex = 38
    end
    object acCards: TAction
      Category = #1060#1086#1088#1084#1072
      Caption = #1050#1072#1088#1090#1086#1095#1082#1080
      ImageIndex = 11
    end
    object acDecodeWB: TAction
      Category = #1044#1077#1082#1086#1076#1080#1088#1086#1074#1072#1085#1080#1077
      Caption = 'acDecodeWB'
      ImageIndex = 38
    end
    object acUTMExch: TAction
      Category = #1060#1086#1088#1084#1072
      Caption = #1059#1058#1052' '#1080#1089#1090#1086#1088#1080#1103
      ImageIndex = 130
    end
    object acUTMProc: TAction
      Category = #1060#1086#1088#1084#1072
      Caption = #1059#1058#1052' '#1086#1073#1084#1077#1085
      ImageIndex = 38
    end
    object acProd: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1047#1072#1087#1088#1086#1089' '#1072#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090#1072' '#1087#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1103
      ImageIndex = 16
    end
    object acSendActPrih: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1057#1092#1086#1088#1084#1080#1088#1086#1074#1072#1090#1100' '#1072#1082#1090' '#1087#1086#1076#1090#1074#1077#1088#1078#1076#1077#1085#1080#1103
      Enabled = False
      ImageIndex = 1
      Visible = False
    end
    object acSendStatus: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1056#1072#1079#1088#1077#1096#1080#1090#1100' '#1086#1090#1087#1088#1072#1074#1082#1091' '#1085#1072' '#1074#1099#1076'. '
      ImageIndex = 1
    end
    object acSendActR2: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1055#1086#1074#1090#1086#1088#1085#1072#1103' '#1086#1090#1087#1088#1072#1074#1082#1072' '#1040#1082#1090#1072' '#1088#1072#1089#1093#1086#1078#1076#1077#1085#1080#1103
      ShortCut = 49235
    end
    object acAuto: TAction
      Category = #1060#1086#1088#1084#1072
      Caption = #1040#1074#1090#1086
      ShortCut = 49217
    end
    object acExpExcel: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1069#1082#1089#1087#1086#1088#1090' '#1074' Excel'
      ImageIndex = 76
      OnExecute = acExpExcelExecute
    end
    object acCreateReturn: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1057#1092#1086#1088#1084#1080#1088#1086#1074#1072#1090#1100' '#1074#1086#1079#1074#1088#1072#1090#1085#1099#1081' '#1076#1086#1082#1091#1084#1077#1085#1090
      ImageIndex = 12
      OnExecute = acCreateReturnExecute
    end
    object acSendRet: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1054#1090#1087#1088#1072#1074#1080#1090#1100' '#1074#1086#1079#1074#1088#1072#1090' '#1074' '#1045#1043#1040#1048#1057'.'
      ImageIndex = 70
      OnExecute = acSendRetExecute
    end
    object acMCrystalDocs: TAction
      Category = #1060#1086#1088#1084#1072
      Caption = #1052#1050#1088#1080#1089#1090#1072#1083
      Enabled = False
      ImageIndex = 161
      Visible = False
    end
    object acProDocs: TAction
      Category = #1060#1086#1088#1084#1072
      Caption = #1055#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1086
      Enabled = False
      ImageIndex = 162
      Visible = False
      OnExecute = acProDocsExecute
    end
    object acInfoAB: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1057#1087#1088#1072#1074#1082#1080' '#1040#1080#1041
      ShortCut = 16450
    end
    object acSendActR_HK: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1055#1086#1074#1090#1086#1088#1085#1072#1103' '#1086#1090#1087#1088#1072#1074#1082#1072' '#1040#1082#1090#1072' '#1088#1072#1089#1093#1086#1078#1076#1077#1085#1080#1081
      ShortCut = 49235
    end
    object acGetRetDocs: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1055#1086#1083#1091#1095#1080#1090#1100' '#1074#1086#1079#1074#1088#1072#1090#1085#1099#1077' '#1076#1086#1082#1091#1084#1077#1085#1090#1099
      Enabled = False
      ImageIndex = 49
      Visible = False
      OnExecute = acGetRetDocsExecute
    end
    object acDelDocOut: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1076#1086#1082#1091#1084#1077#1085#1090
      OnExecute = acDelDocOutExecute
    end
    object acGetTTN: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1047#1072#1087#1088#1086#1089#1080#1090#1100' '#1076#1086#1082#1091#1084#1077#1085#1090' '#1074' '#1045#1043#1040#1048#1057
      ImageIndex = 50
    end
    object acGetTTNPopup: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1047#1072#1087#1088#1086#1089#1080#1090#1100' '#1074#1099#1076#1077#1083#1077#1085#1085#1099#1081' '#1076#1086#1082#1091#1084#1077#1085#1090' '#1074' '#1045#1043#1040#1048#1057
      ImageIndex = 50
    end
    object acGetRawTTNList: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1047#1072#1087#1088#1086#1089#1080#1090#1100' '#1085#1077#1086#1073#1088#1072#1073#1086#1090#1072#1085#1085#1099#1077' '#1058#1058#1053
      ImageIndex = 164
    end
    object acDelDocIn: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1076#1086#1082#1091#1084#1077#1085#1090
      ImageIndex = 2
    end
    object acHistoryDocIn: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1048#1089#1090#1086#1088#1080#1103' '#1087#1086' '#1076#1086#1082#1091#1084#1077#1085#1090#1091
      ImageIndex = 8
    end
    object acInEdit: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1055#1077#1088#1077#1074#1077#1089#1090#1080' '#1076#1086#1082#1091#1084#1077#1085#1090' '#1074' '#1088#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1085#1080#1077
      ImageIndex = 74
    end
    object acInEditRet: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1055#1077#1088#1077#1074#1077#1089#1090#1080' '#1074#1086#1079#1074#1088#1072#1090#1085#1099#1081' '#1076#1086#1082#1091#1084#1077#1085#1090' '#1074' '#1088#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1085#1080#1077
      ImageIndex = 74
      OnExecute = acInEditRetExecute
    end
    object acUTMArh: TAction
      Category = #1060#1086#1088#1084#1072
      Caption = #1059#1058#1052' '#1072#1088#1093#1080#1074
      ImageIndex = 21
    end
    object acCreateFileSale: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = 'acCreateFileSale'
      ShortCut = 24659
    end
    object acQueryFormF1: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1047#1072#1087#1088#1086#1089' '#1089#1087#1088#1072#1074#1082#1080' '#1040
      ImageIndex = 164
    end
    object acQueryRests: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1047#1072#1087#1088#1086#1089' '#1086#1089#1090#1072#1090#1082#1086#1074
      ImageIndex = 164
    end
    object acQueryBarcode: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1047#1072#1087#1088#1086#1089' '#1085#1077#1095#1080#1090#1072#1077#1084#1099#1093' '#1096#1090#1088#1080#1093#1082#1086#1076#1086#1074
      ImageIndex = 34
      ShortCut = 49218
    end
    object acCBDoc: TAction
      Category = #1060#1086#1088#1084#1072
      Caption = #1044#1086#1082#1091#1084#1077#1085#1090#1099' '#1062#1041
      Enabled = False
      ImageIndex = 167
      Visible = False
      OnExecute = acCBDocExecute
    end
    object acNewRet: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1057#1086#1079#1076#1072#1090#1100' '#1076#1086#1082#1091#1084#1077#1085#1090' '#1074#1086#1079#1074#1088#1072#1090#1072' '#1072#1083#1082#1086#1075#1086#1083#1100#1085#1086#1081' '#1087#1088#1086#1076#1091#1082#1094#1080#1080
      OnExecute = acNewRetExecute
    end
  end
  object quSpecIn1: TFDQuery
    CachedUpdates = True
    SQL.Strings = (
      
        'SELECT sp.[FSRARID],sp.[IDATE],sp.[SIDHD],sp.[ID],sp.[NUM],sp.[A' +
        'LCCODE],sp.[QUANT],sp.[PRICE],sp.[QUANTF],sp.[PRICEF],sp.[PRISEF' +
        '0]'
      '  FROM [dbo].[ADOCSIN_SP] sp'
      '  where sp.[FSRARID]=:RARID'
      '  and sp.[IDATE]=:IDATE'
      '  and sp.[SIDHD]=:SID')
    Left = 480
    Top = 336
    ParamData = <
      item
        Name = 'RARID'
        DataType = ftString
        ParamType = ptInput
        Value = '020000021353'
      end
      item
        Name = 'IDATE'
        DataType = ftInteger
        ParamType = ptInput
        Value = 42333
      end
      item
        Name = 'SID'
        DataType = ftString
        ParamType = ptInput
        Value = '166178000003500451'
      end>
    object quSpecIn1FSRARID: TStringField
      FieldName = 'FSRARID'
      Origin = 'FSRARID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 50
    end
    object quSpecIn1IDATE: TIntegerField
      FieldName = 'IDATE'
      Origin = 'IDATE'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quSpecIn1SIDHD: TStringField
      FieldName = 'SIDHD'
      Origin = 'SIDHD'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 50
    end
    object quSpecIn1ID: TLargeintField
      AutoGenerateValue = arAutoInc
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object quSpecIn1NUM: TIntegerField
      FieldName = 'NUM'
      Origin = 'NUM'
    end
    object quSpecIn1ALCCODE: TStringField
      FieldName = 'ALCCODE'
      Origin = 'ALCCODE'
      Size = 50
    end
    object quSpecIn1QUANT: TFloatField
      FieldName = 'QUANT'
      Origin = 'QUANT'
    end
    object quSpecIn1PRICE: TFloatField
      FieldName = 'PRICE'
      Origin = 'PRICE'
    end
    object quSpecIn1QUANTF: TFloatField
      FieldName = 'QUANTF'
      Origin = 'QUANTF'
    end
    object quSpecIn1PRICEF: TFloatField
      FieldName = 'PRICEF'
      Origin = 'PRICEF'
    end
    object quSpecIn1PRISEF0: TFloatField
      FieldName = 'PRISEF0'
      Origin = 'PRISEF0'
    end
  end
  object pmDocsOut: TPopupMenu
    Images = dmR.SmallImage
    Left = 784
    Top = 272
    object N1: TMenuItem
      Action = acNewRet
    end
    object N20: TMenuItem
      Action = acSendRet
    end
    object N22: TMenuItem
      Caption = '-'
    end
    object N9: TMenuItem
      Action = acGetRetDocs
    end
    object N15: TMenuItem
      Action = acInEditRet
    end
    object N10: TMenuItem
      Action = acDelDocOut
    end
    object N8: TMenuItem
      Caption = '-'
    end
    object Excel2: TMenuItem
      Action = acExpExcel
    end
  end
end
