unit SpecMC;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  CustomChildForm, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxImageComboBox, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, dxRibbonSkins, dxRibbonCustomizationForm, dxRibbon, System.Actions, Vcl.ActnList,
  Vcl.PlatformDefaultStyleActnCtrls, Vcl.ActnMan, dxBar;

type
  TfmSpecMC = class(TfmCustomChildForm)
    GridSpecMC: TcxGrid;
    ViewSpecMC: TcxGridDBTableView;
    LevelSpecMC: TcxGridLevel;
    quSpecMC: TFDQuery;
    IntegerField1: TIntegerField;
    IntegerField2: TIntegerField;
    LargeintField1: TLargeintField;
    IntegerField3: TIntegerField;
    SmallintField1: TSmallintField;
    FloatField1: TFloatField;
    FloatField2: TFloatField;
    FloatField3: TFloatField;
    FloatField4: TFloatField;
    FloatField5: TFloatField;
    StringField1: TStringField;
    dsquSpecMC: TDataSource;
    quSpecMCName: TStringField;
    quSpecMCFullName: TStringField;
    ViewSpecMCID: TcxGridDBColumn;
    ViewSpecMCCodeTovar: TcxGridDBColumn;
    ViewSpecMCCodeEdIzm: TcxGridDBColumn;
    ViewSpecMCKol: TcxGridDBColumn;
    ViewSpecMCCenaTovar: TcxGridDBColumn;
    ViewSpecMCSumCenaTovarPost: TcxGridDBColumn;
    ViewSpecMCNewCenaTovar: TcxGridDBColumn;
    ViewSpecMCSumCenaTovar: TcxGridDBColumn;
    ViewSpecMCBarCode: TcxGridDBColumn;
    ViewSpecMCName: TcxGridDBColumn;
    ViewSpecMCFullName: TcxGridDBColumn;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarGroup1: TdxBarGroup;
    ActionManager1: TActionManager;
    acClose: TAction;
    dxRibbon1: TdxRibbon;
    dxRibbon1Tab1: TdxRibbonTab;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acCloseExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

//var
//  fmSpecMC: TfmSpecMC;

implementation

{$R *.dfm}

uses dmRar, MCrystDocs;

procedure TfmSpecMC.acCloseExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmSpecMC.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
end;

end.
