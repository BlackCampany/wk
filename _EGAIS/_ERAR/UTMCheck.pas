unit UTMCheck;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  CustomChildForm, System.Threading, Generics.Collections, httpsend, synautil, nativexml, System.StrUtils, System.DateUtils,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, cxClasses, dxRibbon, dxBar, System.Actions, Vcl.ActnList, cxContainer, cxEdit, cxTextEdit, cxMemo, Vcl.ExtCtrls,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator, Data.DB, cxDBData, cxImageComboBox, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridCustomView, cxGrid, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TfmUTMCheck = class(TfmCustomChildForm)
    ActionList1: TActionList;
    dxBarManager1: TdxBarManager;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    GridCheck: TcxGrid;
    ViewCheck: TcxGridDBTableView;
    LevelCheck: TcxGridLevel;
    meUTM: TFDMemTable;
    meUTMName: TStringField;
    meUTMIsLinked: TIntegerField;
    dsUTM: TDataSource;
    meUTMVersion: TStringField;
    meUTMDateCreateDB: TStringField;
    meUTMBufferAge: TStringField;
    meUTMIPUTM: TStringField;
    meUTMIShop: TIntegerField;
    meUTMRARID: TStringField;
    GridCheckDBTableView1: TcxGridDBTableView;
    GridCheckDBTableView1RARID: TcxGridDBColumn;
    GridCheckDBTableView1IPUTM: TcxGridDBColumn;
    GridCheckDBTableView1Name: TcxGridDBColumn;
    GridCheckDBTableView1IShop: TcxGridDBColumn;
    GridCheckDBTableView1IsLinked: TcxGridDBColumn;
    GridCheckDBTableView1Version: TcxGridDBColumn;
    GridCheckDBTableView1DateCreateDB: TcxGridDBColumn;
    GridCheckDBTableView1BufferAge: TcxGridDBColumn;
    ViewCheckRARID: TcxGridDBColumn;
    ViewCheckIPUTM: TcxGridDBColumn;
    ViewCheckName: TcxGridDBColumn;
    ViewCheckIShop: TcxGridDBColumn;
    ViewCheckIsLinked: TcxGridDBColumn;
    ViewCheckVersion: TcxGridDBColumn;
    ViewCheckDateCreateDB: TcxGridDBColumn;
    ViewCheckBufferAge: TcxGridDBColumn;
    TimerUTM: TTimer;
    acRefresh: TAction;
    dxBarManager1Bar1: TdxBar;
    dxBarLargeButton1: TdxBarLargeButton;
    acClose: TAction;
    dxBarManager1Bar2: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    ViewCheckResultCode: TcxGridDBColumn;
    ViewCheckResultString: TcxGridDBColumn;
    meUTMResultCode: TIntegerField;
    meUTMResultString: TStringField;
    meUTMDBufferAge: TDateTimeField;
    ViewCheckDBufferAge: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure TimerUTMTimer(Sender: TObject);
    procedure acRefreshExecute(Sender: TObject);
    procedure acCloseExecute(Sender: TObject);
    procedure ViewCheckCustomDrawCell(Sender: TcxCustomGridTableView; ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
  private
    { Private declarations }
    procedure Init;
  public
    { Public declarations }
  end;

  //��� ��� ������� UTM
  TTestUTM = record
    IPUTM    : String;    //IP ����� UTM
    IsLinked: Integer;    //������ 0-��� �����, 1-���� ����, 2-������ �����
    Version: String;      //������ UTM
    DateCreateDB: String; //���� �������� ���� ������
    BufferAge: String;    //���� ������ ������� ��������������� ���������� ���������
    ResultCode: Integer;  //��� ������ �������
    ResultString: String; //������ ������ �������
  end;

  //��������� ����� ��� ���������� �������� � UTM
  TTestUTMThread = class(TThread)
  private
    FQueueUTM: TThreadedQueue<TTestUTM>;  //������� ��� ��������� UTM
    FIPUTM:string;              //IP ����� UTM
    FIsLinked:integer;          //������ 0-��� �����, 1-���� ����, 2-������ �����
    FVersion: string;           //������ UTM
    FDateCreateDB: String;      //���� �������� ���� ������
    FBufferAge: String;         //���� ������ ������� ��������������� ���������� ���������
    FResultCode: Integer;       //��� ������ �������
    FResultString: String;      //������ ������ �������
    FThreadCounter: PInteger;   //��������� �� ������� �������
    FContent: TStringList;
    procedure ReturnServerStatus;
    procedure IncThreadCounter;
    procedure DecThreadCounter;
    procedure ParseContent;
  protected
    procedure Execute; override;
  public
    constructor Create(aIPUTM: String; aQueueUTM: TThreadedQueue<TTestUTM>; aThreadCounter: PInteger); overload;
    destructor Destroy; override;
  end;

var
  fmUTMCheck: TfmUTMCheck;

procedure ShowFormUTMCheck;

implementation

{$R *.dfm}

uses Shared_Functions, dmRar, EgaisDecode;

var
  QueueUTM: TThreadedQueue<TTestUTM>;  //������� ��� ��������� UTM
  ThreadCounter: Integer = 0;   //������� �������

procedure ShowFormUTMCheck;
begin
  if Assigned(fmUTMCheck)=False then begin //����� �� ����������, � ����� �������
    fmUTMCheck:=TfmUTMCheck.Create(Application);
  end;
  if fmUTMCheck.WindowState=wsMinimized then //���� ���� ��������, �� ������������� ���
    fmUTMCheck.WindowState:=wsNormal;

  fmUTMCheck.Show;
  Application.ProcessMessages;
  fmUTMCheck.Init;
end;

procedure TfmUTMCheck.Init;
var TestUTMThread: TTestUTMThread;
  ListUTM: TList<String>;
  IP: String;
  FocusedRow, TopRow: Integer;
  View: TcxGridTableView;
  DataController: TcxGridDataController;
  flag: boolean;
begin
  if ThreadCounter>0 then Exit; //���� ������ ��� ��������, �� ��� ��� �� �� ���������

  flag:=meUTM.Active;  //���������� ���� �� ������� �������, ��� �������������� ������� �������

  View := GridCheck.FocusedView as TcxGridTableView;
  DataController := View.DataController;
  TopRow := View.Controller.TopRowIndex;
  FocusedRow := DataController.FocusedRowIndex;

  meUTM.DisableControls;
  ListUTM:= TList<String>.Create;
  try
    dmR.quPoint.Active:=False;
    dmR.quPoint.Active:=True;
    meUTM.Active:=False;
    meUTM.Active:=True;

    //<--��������� ��������� ������� ������� UTM
    dmR.quPoint.First;
    while not dmR.quPoint.Eof do begin
      ListUTM.Add(dmR.quPointIPUTM.AsString);
      meUTM.Append;
      meUTMRARID.AsString:=dmR.quPointRARID.AsString;
      meUTMIPUTM.AsString:=dmR.quPointIPUTM.AsString;
      meUTMName.AsString:=dmR.quPointName.AsString;
      meUTMIShop.AsInteger:=dmR.quPointISHOP.AsInteger;
      meUTMIsLinked.AsInteger:=2;
      meUTMVersion.AsString:='';
      meUTMBufferAge.AsString:='';
      meUTMDateCreateDB.AsString:='';
      meUTM.Post;
      dmR.quPoint.Next;
    end;
    //-->

    Delay(100);
    //<--��������� UTM � �������
    //���������� ������, ����� ������ �� ������� �� ����� ������ ���
    for IP in ListUTM do begin
      TestUTMThread:=TTestUTMThread.Create(IP, QueueUTM, Addr(ThreadCounter));  //������� ����� � ����������� ����������� � �������� ���
    end;
    //-->

    Delay(100);
    TimerUTM.Enabled:=True;  //�������� ������
  finally
    ListUTM.Free;
    meUTM.First;
    meUTM.EnableControls;
    if flag then begin
      //��������������� �������
      DataController.FocusedRowIndex := FocusedRow;
      View.Controller.TopRowIndex := TopRow;
    end;
    Application.ProcessMessages;
  end;
end;

procedure TfmUTMCheck.TimerUTMTimer(Sender: TObject);
var
  TestUTM: TTestUTM;
  FocusedRow, TopRow: Integer;
  View: TcxGridTableView;
  DataController: TcxGridDataController;
begin
  //������ ������� �� �������, �� ���������� ������
  //��������� ������� � ������� �� ���� ������������ �������
  TimerUTM.Enabled:=False;

  //���������� ���.�������
  View := ViewCheck as TcxGridTableView;
  DataController := View.DataController;
  TopRow := View.Controller.TopRowIndex;
  FocusedRow := DataController.FocusedRowIndex;

  ViewCheck.BeginUpdate;
  meUTM.DisableControls;
  //������ ��������� �� �������, ���� ��� ��� ����.
  while QueueUTM.QueueSize>0 do begin
    //������ ������ ��������� � ������� � ������������ �������� ��� ������.
    TestUTM:= QueueUTM.PopItem;
    //����� ��������� ������ ���������� ����� ��� ���������.
    //MemoLogLocal.Lines.Add( FormatDateTime('hh:nn:ss.zzz', Time)+' �������� IP '+TestUTM.IPUTM+' - ������ '+IntToStr(TestUTM.IsLinked));  //�������

    if meUTM.Locate('IPUTM',TestUTM.IPUTM,[]) then begin
      meUTM.Edit;
      meUTMIsLinked.AsInteger:=TestUTM.IsLinked;
      meUTMVersion.AsString:=TestUTM.Version;
      meUTMDateCreateDB.AsString:=TestUTM.DateCreateDB;
      meUTMBufferAge.AsString:=TestUTM.BufferAge;
      meUTMResultCode.AsInteger:=TestUTM.ResultCode;
      meUTMResultString.AsString:=TestUTM.ResultString;
      meUTMDBufferAge.AsDateTime:=dtsfromrar(TestUTM.BufferAge);
      meUTM.Post;
    end;
  end;
  ViewCheck.EndUpdate;
  meUTM.EnableControls;

  //��������������� �������
  DataController.FocusedRowIndex := FocusedRow;
  View.Controller.TopRowIndex := TopRow;

  TimerUTM.Enabled:=ThreadCounter>0;  //�������� ������, ���� �������� ��� ������
end;

procedure TfmUTMCheck.ViewCheckCustomDrawCell(Sender: TcxCustomGridTableView; ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);
Var DBufferAge:TDateTime;
    colorLightRed: TColor;
begin
  colorLightRed:= $009393FF;   //������-�������

  if VarIsNull(AViewInfo.GridRecord.Values[ViewCheckDBufferAge.Index]) then
    DBufferAge:=0
  else
    DBufferAge:=AViewInfo.GridRecord.Values[ViewCheckDBufferAge.Index];

  if (Trunc(DBufferAge)>0)and(DBufferAge<IncHour(Now,-1)) then begin
    ACanvas.Canvas.Brush.Color := colorLightRed;
    ACanvas.Canvas.Font.Color:=clBlack;
  end;
end;

//<--TTestLinkedServerThread
constructor TTestUTMThread.Create(aIPUTM: String; aQueueUTM: TThreadedQueue<TTestUTM>; aThreadCounter: PInteger);
begin
  inherited Create(False);  //False - ��������� ����� ��������
  FreeOnTerminate:=True;
  Priority:=tpLower;
  FIPUTM:=aIPUTM;                  //IP ����� UTM ������
  FQueueUTM:=aQueueUTM;            //��������� �� �������
  FThreadCounter:=aThreadCounter;  //��������� �� ������� �������
  FContent:=TStringList.Create;
end;

destructor TTestUTMThread.Destroy;
begin
  FContent.Free;
  inherited Destroy;
end;

procedure TTestUTMThread.ReturnServerStatus;
var
  TestUTM: TTestUTM;
begin
  TestUTM.IPUTM:=FIPUTM;
  TestUTM.IsLinked:=FIsLinked;
  TestUTM.Version:=FVersion;
  TestUTM.DateCreateDB:=FDateCreateDB;
  TestUTM.BufferAge:=FBufferAge;
  TestUTM.ResultCode:=FResultCode;
  TestUTM.ResultString:=FResultString;
  FQueueUTM.PushItem(TestUTM);  //��������� ��������� � ����� �������
end;

procedure TTestUTMThread.IncThreadCounter;
begin
  inc(PInteger(FThreadCounter)^);
end;

procedure TTestUTMThread.DecThreadCounter;
begin
  dec(PInteger(FThreadCounter)^);
end;

procedure TTestUTMThread.ParseContent;
var StrWk: String;
  function parseBetween(startPhrase, endPhrase, text: string): string;
  var len, first,last: integer;
  begin
    len := length(startPhrase);
    first:=pos(startPhrase, text);
    last:=pos(endPhrase, text, first + len);
    if (first>0) and (last>0) then
      Result := Copy(text, first + len, last-first-len)
    else
      Result:='';
  end;
begin
  StrWk:=FContent.Text;
  StrWk:=AnsiReplaceStr(StrWk,#$D#$A,' '); //�������� ������� �������� ������ ��������

  //<--������ ����� ����� ����� �������, �� �������� ��� ������, �� ������� �������� ������� ��������
  FDateCreateDB:=parseBetween('<h3>���� �������� ���� ������:</h3> <pre>','</pre>',StrWk);
  FVersion:=parseBetween('<h3>������ ������������� ������:</h3> <pre>','</pre>',StrWk);
  FBufferAge:=parseBetween('<h3>���� ������ ������� ��������������� ���������� ���������:</h3> <pre>','</pre>',StrWk);
  //-->
end;

procedure TTestUTMThread.Execute;
var
  httpsend: THTTPSend;
begin
  //�������� ������� ������� �� �������
  Synchronize(IncThreadCounter);

  try
    FContent.Clear;
    httpsend:=THTTPSend.Create;
    try
      if httpsend.HTTPMethod('get','http://'+FIPUTM+':8080') then begin
        FIsLinked:=1;
        FResultCode:=httpsend.ResultCode;     //��� ������ �������
        FResultString:=httpsend.ResultString; //������ ������ �������
        FContent.LoadFromStream(httpsend.Document, TEncoding.UTF8);
        //FContent.SaveToFile('c:\'+FIPUTM+'.html'); //�������
        ParseContent; //������ �������
      end else begin
        FIsLinked:=0;
        FResultCode:=httpsend.ResultCode;     //��� ������ �������
        FResultString:=httpsend.ResultString; //������ ������ �������
      end;
    finally
      httpsend.Free;
    end;

  finally
    //��������� ������������ ������ �����
    Synchronize(ReturnServerStatus);
    //�������� ������� ������� �� �������
    Synchronize(DecThreadCounter);
  end;
end;
//-->

procedure TfmUTMCheck.acCloseExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmUTMCheck.acRefreshExecute(Sender: TObject);
begin
  Init;
end;

procedure TfmUTMCheck.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
  fmUTMCheck:=Nil;
end;

procedure TfmUTMCheck.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  if ThreadCounter>0 then begin
    ShowMsg('���� �������� �������� ('+IntToStr(ThreadCounter)+'). ���������� ������� ��������� �����.');
    CanClose:=False;
  end;
end;

procedure TfmUTMCheck.FormCreate(Sender: TObject);
begin
  ClearMessageLogLocal;
  dxRibbon1.ShowTabHeaders:=False;
end;

initialization
  //������ �������. ����� ���������� ������ ������ ������� � ����� �������� �� ���������� � ������� (10 ���.)
  QueueUTM:=TThreadedQueue<TTestUTM>.Create(100, 10000);
finalization
  QueueUTM.Free;
end.
