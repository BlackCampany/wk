unit ViewDocRet;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  CustomChildForm, System.UITypes, ShellApi, httpsend, synautil, nativexml, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxRibbonSkins, dxRibbonCustomizationForm, cxContainer, cxEdit, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator,
  Data.DB, cxDBData, cxImageComboBox, cxTextEdit, cxDBLookupComboBox, cxCheckBox, cxCalendar, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.Menus, System.Actions, Vcl.ActnList, dxBar, cxBarEditItem, dxBarExtItems, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid, cxMemo, Vcl.ExtCtrls, dxRibbon;

type
  TfmViewDocRet = class(TfmCustomChildForm)
    dxRibbon1: TdxRibbon;
    dxRibbon1Tab1: TdxRibbonTab;
    dxBarManager1: TdxBarManager;
    bmApplyDate: TdxBar;
    bmClose: TdxBar;
    btnDone: TdxBarLargeButton;
    btnClose: TdxBarLargeButton;
    deDateBeg: TdxBarDateCombo;
    deDateEnd: TdxBarDateCombo;
    dxBarGroup1: TdxBarGroup;
    ActionList1: TActionList;
    acRefresh: TAction;
    acClose: TAction;
    acSelectDate: TAction;
    acDecodeTovar: TAction;
    acSaveToFile: TAction;
    acDelList: TAction;
    acDecodeTTN: TAction;
    acCards: TAction;
    acDecodeWB: TAction;
    GridDocRet: TcxGrid;
    quSpecIn1: TFDQuery;
    quSpecIn1FSRARID: TStringField;
    quSpecIn1IDATE: TIntegerField;
    quSpecIn1SIDHD: TStringField;
    quSpecIn1ID: TLargeintField;
    quSpecIn1NUM: TIntegerField;
    quSpecIn1ALCCODE: TStringField;
    quSpecIn1QUANT: TFloatField;
    quSpecIn1PRICE: TFloatField;
    quSpecIn1QUANTF: TFloatField;
    quSpecIn1PRICEF: TFloatField;
    quSpecIn1PRISEF0: TFloatField;
    acUTMExch: TAction;
    dxBarLargeButton2: TdxBarLargeButton;
    acUTMProc: TAction;
    dxBarLargeButton3: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarLargeButton4: TdxBarLargeButton;
    acProd: TAction;
    acSendActPrih: TAction;
    dxBarLargeButton7: TdxBarLargeButton;
    acSendStatus: TAction;
    acSendActR2: TAction;
    acAuto: TAction;
    acExpExcel: TAction;
    acCreateReturn: TAction;
    LevelDocsOut: TcxGridLevel;
    ViewDocsOut: TcxGridDBTableView;
    ViewDocsOutFSRARID: TcxGridDBColumn;
    ViewDocsOutIDATE: TcxGridDBColumn;
    ViewDocsOutSID: TcxGridDBColumn;
    ViewDocsOutNUMBER: TcxGridDBColumn;
    ViewDocsOutCLIFTO: TcxGridDBColumn;
    ViewDocsOutIACTIVE: TcxGridDBColumn;
    ViewDocsOutREADYSEND: TcxGridDBColumn;
    ViewDocsOutWBREGID: TcxGridDBColumn;
    ViewDocsOutFIXNUMBER: TcxGridDBColumn;
    ViewDocsOutFIXDATE: TcxGridDBColumn;
    ViewDocsOutIDHEAD: TcxGridDBColumn;
    ViewDocsOutDATE_OUTPUT: TcxGridDBColumn;
    ViewDocsOutXMLF: TcxGridDBColumn;
    ViewDocsOutFULLNAME: TcxGridDBColumn;
    ViewDocsOutNAME: TcxGridDBColumn;
    ViewDocsOutCLIENTINN: TcxGridDBColumn;
    ViewDocsOutCLIENTKPP: TcxGridDBColumn;
    ViewDocsOutDDATE: TcxGridDBColumn;
    pmDocsOut: TPopupMenu;
    N8: TMenuItem;
    Excel2: TMenuItem;
    acSendRet: TAction;
    acMCrystalDocs: TAction;
    dxBarLargeButton9: TdxBarLargeButton;
    ViewDocsOutDATEDOC: TcxGridDBColumn;
    acProDocs: TAction;
    dxBarLargeButton10: TdxBarLargeButton;
    acInfoAB: TAction;
    acSendActR_HK: TAction;
    acGetRetDocs: TAction;
    N9: TMenuItem;
    acDelDocOut: TAction;
    N10: TMenuItem;
    ViewDocsOutTT1: TcxGridDBColumn;
    ViewDocsOutTT2: TcxGridDBColumn;
    acGetTTN: TAction;
    beiGetTTN: TcxBarEditItem;
    dxBarLargeButton14: TdxBarLargeButton;
    acDelDocIn: TAction;
    acHistoryDocIn: TAction;
    acInEdit: TAction;
    acInEditRet: TAction;
    N15: TMenuItem;
    acUTMArh: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    acCreateFileSale: TAction;
    acGetRawTTNList: TAction;
    dxBarLargeButton16: TdxBarLargeButton;
    acGetTTNPopup: TAction;
    N20: TMenuItem;
    N22: TMenuItem;
    acQueryFormF1: TAction;
    dxBarLargeButton1: TdxBarLargeButton;
    acQueryRests: TAction;
    dxBarLargeButton5: TdxBarLargeButton;
    acQueryBarcode: TAction;
    dxBarLargeButton6: TdxBarLargeButton;
    acCBDoc: TAction;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarButton1: TdxBarButton;
    dxBarButton2: TdxBarButton;
    dxBarButton3: TdxBarButton;
    acNewRet: TAction;
    N1: TMenuItem;
    procedure acRefreshExecute(Sender: TObject);
    procedure acCloseExecute(Sender: TObject);
    procedure acSelectDateExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure deDateBegChange(Sender: TObject);
    procedure acExpExcelExecute(Sender: TObject);
    procedure acCreateReturnExecute(Sender: TObject);
    procedure ViewDocsOutDblClick(Sender: TObject);
    procedure GridDocRetFocusedViewChanged(Sender: TcxCustomGrid; APrevFocusedView,
      AFocusedView: TcxCustomGridView);
    procedure acSendRetExecute(Sender: TObject);
    procedure acProDocsExecute(Sender: TObject);
    procedure acGetRetDocsExecute(Sender: TObject);
    procedure acDelDocOutExecute(Sender: TObject);
    procedure acInEditRetExecute(Sender: TObject);
    procedure acCBDocExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acNewRetExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }

    procedure Init;
  end;

var
  fmViewDocRet: TfmViewDocRet;

procedure ShowFormViewDocRet;

implementation

{$R *.dfm}

uses ViewXML, EgaisDecode, ViewCards, dmRar, UTMExch, ViewSpecOut, MCrystDocs, ProDocs, History, UTMArh, UTMCheck, DocHeaderCB,
  ViewSpec, Main, AddRetDoc;

procedure ShowFormViewDocRet;
begin
  if Assigned(fmViewDocRet)=False then //����� �� ����������, � ����� �������
    fmViewDocRet:=TfmViewDocRet.Create(Application);
  if fmViewDocRet.WindowState=wsMinimized then //���� ���� ��������, �� ������������� ���
    fmViewDocRet.WindowState:=wsNormal;

  fmViewDocRet.deDateBeg.Date:=Date-7;
  fmViewDocRet.deDateEnd.Date:=Date;

  fmViewDocRet.Init;
  fmViewDocRet.Show;
end;

procedure TfmViewDocRet.Init;
var
  FocusedRow, TopRow: Integer;
  flag: boolean;
begin
  with dmR do
  begin
    try
      //<--Refresh � ��������������� ������� ������� � �������
      flag:=quDocOutHd.Active;  //���������� ���� �� ������� �������, ��� �������������� ������� �������
      TopRow := ViewDocsOut.Controller.TopRowIndex;
      FocusedRow := ViewDocsOut.DataController.FocusedRowIndex;
      //-->
      ViewDocsOut.BeginUpdate;
      quDocOutHd.Active:=False;
      quDocOutHd.ParamByName('IDATEB').AsInteger:=Trunc(deDateBeg.Date);
      quDocOutHd.ParamByName('IDATEE').AsInteger:=Trunc(deDateEnd.Date);
      quDocOutHd.ParamByName('RARID').AsString:=CommonSet.FSRAR_ID;
      quDocOutHd.Active:=True;
      quDocOutHd.First;
    finally
      ViewDocsOut.EndUpdate;
      //<--��������������� �������
      if flag then begin
        ViewDocsOut.DataController.FocusedRowIndex := FocusedRow;
        ViewDocsOut.Controller.TopRowIndex := TopRow;
      end;
      //-->
    end;
  end;
end;


procedure TfmViewDocRet.ViewDocsOutDblClick(Sender: TObject);
begin
  // ������������
  with dmR do
  begin
    if quDocOutHd.RecordCount>0 then
    begin
      if (ViewDocsOut.Controller.FocusedColumn.Name='ViewDocsOutXMLF') then
      begin
        quDocOutRec.Active:=False;
        quDocOutRec.ParamByName('RARID').AsString:=quDocOutHdFSRARID.AsString;
        quDocOutRec.ParamByName('IDATE').AsInteger:=quDocOutHdIDATE.AsInteger;
        quDocOutRec.ParamByName('SID').AsString:=quDocOutHdSID.AsString;
        quDocOutRec.Active:=True;
        if quDocOutRec.RecordCount>0 then
        begin
          ShowXMLView(quDocOutRecIDHEAD.AsInteger,quDocOutRecSENDXML.AsString);
        end;

      end else
      begin
        if quDocOutHdIACTIVE.AsInteger=0 then
           ShowSpecViewOut(quDocOutHdFSRARID.AsString,quDocOutHdSID.AsString,quDocOutHdSHIPDATE.AsString,quDocOutHdNUMBER.AsString,quDocOutHdNAME.AsString,quDocOutHdIDATE.AsInteger,1,quDocOutHdDATEDOC.AsDateTime,quDocOutHdIDHEAD.AsInteger,CommonSet.SDEP)
        else
           ShowSpecViewOut(quDocOutHdFSRARID.AsString,quDocOutHdSID.AsString,quDocOutHdSHIPDATE.AsString,quDocOutHdNUMBER.AsString,quDocOutHdNAME.AsString,quDocOutHdIDATE.AsInteger,0,quDocOutHdDATEDOC.AsDateTime,quDocOutHdIDHEAD.AsInteger,CommonSet.SDEP);
      end;
    end;
  end;
end;

procedure TfmViewDocRet.acCBDocExecute(Sender: TObject);
begin
  // ��������� ��
  with dmR do
  begin
    if (FDConnection.Connected) then
    begin
      try
        if quPoint.Locate('ISHOP',fmMain.beiPointMF.EditValue,[]) then
        begin
          prShowCBDocs(quPointISHOP.AsInteger, quPointName.AsString,deDateBeg.Date,deDateEnd.Date,quPointSDEP.AsString);
//            prShowMCDocs(quPointISHOP.AsInteger, quPointName.AsString,deDateBeg.Date,deDateEnd.Date,quPointIPDB.AsString,quPointDBNAME.AsString,'sa',quPointPASSW.AsString);
        end;
      finally
      end;
    end;
  end;
end;

procedure TfmViewDocRet.acCloseExecute(Sender: TObject);
begin
  //�������
  Close;
end;

procedure TfmViewDocRet.acCreateReturnExecute(Sender: TObject);
begin
  //������� �������� �� �������
  with dmR do
  begin
    if quDocInHd.RecordCount>0 then
    begin
      if (quDocInHdIACTIVE.AsInteger=1) then
      begin
        // ����� ����������� �������
        if (Trim(quDocInHdWBREGID.AsString)>'') then
        begin
           ClearMessageLogLocal;
           ShowMessageLogLocal('�����.. ���� ������������ ��������� �� �������.');

           if prCreateRet(quDocInHdFSRARID.AsString,quDocInHdSID.AsString,quDocInHdIDATE.AsInteger,MemoLogLocal)
           then
           begin
             ShowMessageLogLocal('�������� ��������.');
           end
           else ShowMessageLogLocal('������ ����������.');

        end else ShowMessage('������������ �������� ����������, �.�. �� �������� �� ��� � �����.');
      end else ShowMessage('�������� ������.');
    end;
  end;
end;


procedure TfmViewDocRet.acDelDocOutExecute(Sender: TObject);
begin
  //������� �������� �������
  with dmR do
  begin
    if quDocOutHd.RecordCount>0 then
    begin
      if MessageDlg('������� �������� ������� '+quDocOutHdNUMBER.AsString+' �� '+ds1(quDocOutHdIDATE.AsInteger)+'?',mtConfirmation, [mbYes, mbNo], 0, mbYes) = mrYes then
      begin
        ClearMessageLogLocal;
        ShowMessageLogLocal('�����.. ���� �������� ���������');

        if quDocOutHdIACTIVE.AsInteger=0 then
        begin
          try
            quS.Active:=False;
            quS.SQL.Clear;
            quS.SQL.Add('');

            quS.SQL.Add('delete from dbo.ADOCSOUT_HD');
            quS.SQL.Add('WHERE');
            quS.SQL.Add('  FSRARID = '''+quDocOutHdFSRARID.AsString+'''');
            quS.SQL.Add('  AND IDATE = '+its(quDocOutHdIDATE.AsInteger));
            quS.SQL.Add('  AND SID = '''+quDocOutHdSID.AsString+'''');
            quS.ExecSQL;

            Init;
          except
          end;
        end else ShowMessageLogLocal('  �������� ������ ���������. �������� ����������.');


        ShowMessageLogLocal('������� ��������.');
      end;
    end;
  end;
end;

procedure TfmViewDocRet.acExpExcelExecute(Sender: TObject);
begin
  ExportGridToFile(formatdatetime('ddmmyyyy',deDateBeg.Date)+'_'+formatdatetime('ddmmyyyy',deDateEnd.Date)+'_'+formatdatetime('ddmmyyyy_hh_nn_ss',now)+'.xls', GridDocRet);
end;

procedure TfmViewDocRet.acGetRetDocsExecute(Sender: TObject);
begin
  //�������� ���������� ���������
  with dmR do
  begin
    ClearMessageLogLocal;
    ShowMessageLogLocal('�����.. ���� ��������� ���������� ���������� �� ������ � '+ds1(deDateBeg.Date)+' �� '+ds1(deDateEnd.Date));
    try
      quS.Active:=False;
      quS.SQL.Clear;
      quS.SQL.Add('');

      quS.SQL.Add('DECLARE @IDATEB int = '+its(Trunc(deDateBeg.Date)));
      quS.SQL.Add('DECLARE @IDATEE int = '+its(Trunc(deDateEnd.Date)));
      quS.SQL.Add('DECLARE @FSRARID varchar(50) = '''+CommonSet.FSRAR_ID+'''');

      if CommonSet.SDEP>'' then
      begin
        quS.SQL.Add('DECLARE @SDEP varchar(200) = '''+CommonSet.SDEP+'''');
        quS.SQL.Add('EXECUTE [dbo].[prImportRetDocs] @IDATEB,@IDATEE,@FSRARID,@SDEP');
     end else
      begin
        quS.SQL.Add('EXECUTE [dbo].[prImportRetDocs] @IDATEB,@IDATEE,@FSRARID');
      end;
      quS.ExecSQL;

      Init;
    except
    end;

    ShowMessageLogLocal('������� ��������.');
  end;
end;

procedure TfmViewDocRet.acInEditRetExecute(Sender: TObject);
begin
   //��������� �������� � ��������������
  with dmR do
  begin
    if quDocOutHd.RecordCount>0 then
    begin
      if MessageDlg('��������� �������� '+quDocOutHdNUMBER.AsString+' �� '+ds1(quDocOutHdIDATE.AsInteger)+' � ��������������?',mtConfirmation, [mbYes, mbNo], 0, mbYes) = mrYes then
      begin
        WriteHistoryOut(quDocOutHdFSRARID.AsString,quDocOutHdSID.AsString,quDocOutHdIDATE.AsInteger,' ��������� � ����� ��������������. ������� ������: Active '+its(quDocOutHdIACTIVE.AsInteger)+'  READYSEND '+its(quDocOutHdREADYSEND.AsInteger));

        ClearMessageLogLocal;
        ShowMessageLogLocal('�����..');

        quS.Active:=False;
        quS.SQL.Clear;
        quS.SQL.Add('');

        quS.SQL.Add('UPDATE dbo.ADOCSOUT_HD');
        quS.SQL.Add('SET IACTIVE = 0 ,READYSEND = 0');
        quS.SQL.Add('WHERE');
        quS.SQL.Add('  FSRARID = '''+quDocOutHdFSRARID.AsString+'''');
        quS.SQL.Add('  AND IDATE = '+its(quDocOutHdIDATE.AsInteger));
        quS.SQL.Add('  AND SID = '''+quDocOutHdSID.AsString+'''');
        quS.ExecSQL;

        Init;

        ShowMessageLogLocal('������� ��������.');
      end;
    end;
  end;
end;

procedure TfmViewDocRet.acNewRetExecute(Sender: TObject);
begin

// ������� ���������� ��������
  if not Assigned(fmAddRetDoc) then fmAddRetDoc:=TfmAddRetDoc.Create(Application,TFormStyle.fsNormal);

  fmAddRetDoc.cxTextEdit1.Text:='';
  fmAddRetDoc.cxDateEdit1.Date:=Date;
  fmAddRetDoc.cxLookupComboBox1.EditValue:=0;

  fmAddRetDoc.quListCli.Active:=True;

  fmAddRetDoc.ShowModal;
  if fmAddRetDoc.ModalResult=mrOk then
  begin
//    ShowMessage('������ ��������� ���������� ���������  '+fmAddRetDoc.cxLookupComboBox1.EditValue);
    ClearMessageLogLocal;

    if (Trim(fmAddRetDoc.cxTextEdit1.Text)>'') and (Trim(fmAddRetDoc.cxLookupComboBox1.EditValue)>'') and (fmAddRetDoc.cxDateEdit1.Date>(date-100))
    then
    begin
      try
        with dmR do
        begin
          quS.Active:=False;
          quS.SQL.Clear;
          quS.SQL.Add('');

          quS.SQL.Add('  declare @FSRARID VARCHAR(50) = '''+CommonSet.FSRAR_ID+'''');
          quS.SQL.Add('  DECLARE @IDATERET INT = '+its(Trunc(fmAddRetDoc.cxDateEdit1.Date)));
          quS.SQL.Add('  DECLARE @sCliTo VARCHAR(50) = '''+fmAddRetDoc.cxLookupComboBox1.EditValue+'''');
          quS.SQL.Add('  DECLARE @SNUM VARCHAR(50) = '''+fmAddRetDoc.cxTextEdit1.Text+'''');
          quS.SQL.Add('  DECLARE @SIDOUT VARCHAR(50)');

          quS.SQL.Add('  SET @SIDOUT = NEWID()');

          quS.SQL.Add('  INSERT INTO [dbo].[ADOCSOUT_HD] ([FSRARID], [IDATE], [SID], [NUMBER], [CLIFTO], [IACTIVE], [READYSEND], [WBREGID], [FIXNUMBER], [FIXDATE], [IDHEAD], [DATEDOC])');
          quS.SQL.Add('  VALUES (@FSRARID, @IDATERET, @SIDOUT, @SNUM, @sCliTo, 0, 0, '''', '''', '''', 0, [dbo].i2d(@IDATERET))');

          quS.ExecSQL;
        end;
        Init;
        ShowMessageLogLocal('�������� �� ������� ��������.')
      except
      end;
    end else ShowMessageLogLocal('����������� ��������� ������ ���������. ���������� ����������.');
  end;
  fmAddRetDoc.quListCli.Active:=False;

end;

procedure TfmViewDocRet.acProDocsExecute(Sender: TObject);
begin
  //��������� ������������
  if dmR.quPoint.Locate('ISHOP',fmMain.beiPointMF.EditValue,[]) then
    prShowProDocs(deDateBeg.Date,deDateEnd.Date, dmR.quPointRARID.AsString, dmR.quPointISPRODUCTION.AsInteger=1);
end;


procedure TfmViewDocRet.acRefreshExecute(Sender: TObject);
begin
  //������
  Init;
end;

procedure TfmViewDocRet.acSelectDateExecute(Sender: TObject);
begin
  //��������
  Init;
end;

procedure TfmViewDocRet.acSendRetExecute(Sender: TObject);
begin
  //������������ ��� ��������
  ClearMessageLogLocal;
  ShowMessageLogLocal('�����.. ���� ������������ ��������� ��������.');

  with dmR do
  begin
    if quDocOutHd.RecordCount>0 then
    begin
      if (quDocOutHdIACTIVE.AsInteger=0) then
      begin
        // ����� ����������� ��� �����������


         if prSendTTN(quDocOutHdFSRARID.AsString,quDocOutHdSID.AsString,quDocOutHdIDATE.AsInteger,MemoLogLocal) then
         begin
            try
              quS.Active:=False;
              quS.SQL.Clear;
              quS.SQL.Add('');

              quS.SQL.Add('UPDATE dbo.ADOCSOUT_HD');
              quS.SQL.Add('SET IACTIVE = 1');
              quS.SQL.Add('WHERE');
              quS.SQL.Add('  FSRARID = '''+quDocOutHdFSRARID.AsString+'''');
              quS.SQL.Add('  AND IDATE = '+its(quDocOutHdIDATE.AsInteger));
              quS.SQL.Add('  AND SID = '''+quDocOutHdSID.AsString+'''');
              quS.ExecSQL;

              Init;
            except
            end;
         end;

         ShowMessageLogLocal('������� ��������.');
      end else ShowMessage('�������� ������.');
    end;
  end;
  ShowMessageLogLocal('������� ��������.');
end;

procedure TfmViewDocRet.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
  fmViewDocRet:=Nil;
end;

procedure TfmViewDocRet.FormCreate(Sender: TObject);
begin
  ClearMessageLogLocal;
end;

procedure TfmViewDocRet.GridDocRetFocusedViewChanged(Sender: TcxCustomGrid;
  APrevFocusedView, AFocusedView: TcxCustomGridView);
begin
  acCreateReturn.Enabled:=False;
  acSendRet.Enabled:=True;
end;

procedure TfmViewDocRet.deDateBegChange(Sender: TObject);
begin
  if dmR.FDConnection.Connected then Init;
end;

end.
