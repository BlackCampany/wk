unit ViewSpecInv;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  CustomChildForm, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer,
  cxTextEdit, cxCalendar, cxCheckBox, cxDBLookupComboBox, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error,
  FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Vcl.Menus, cxClasses, FireDAC.Comp.Client,
  FireDAC.Comp.DataSet, System.Actions, Vcl.ActnList, dxBar, cxBarEditItem, cxMemo, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridCustomView, cxGrid, dxRibbon;

type
  TfmDocSpecInv = class(TfmCustomChildForm)
    dxBarManager1: TdxBarManager;
    dxBarGroup1: TdxBarGroup;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    dxBarManager1Bar1: TdxBar;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    ActionList1: TActionList;
    acClose: TAction;
    acSave: TAction;
    quSpecInv: TFDQuery;
    UpdSQL1: TFDUpdateSQL;
    dsquSpecInv: TDataSource;
    ViewSpecInv: TcxGridDBTableView;
    LevelSpec: TcxGridLevel;
    GridSpec: TcxGrid;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    PopupMenu1: TPopupMenu;
    cxStyle2: TcxStyle;
    dxBarSubItem1: TdxBarSubItem;
    quE: TFDQuery;
    acTestSpec: TAction;
    dxBarLargeButton4: TdxBarLargeButton;
    acDelPos: TAction;
    N1: TMenuItem;
    dxBarManager1Bar2: TdxBar;
    quSpecInvIDH: TLargeintField;
    quSpecInvID: TLargeintField;
    quSpecInvCAP: TStringField;
    quSpecInvNAME: TMemoField;
    quSpecInvAVID: TIntegerField;
    quSpecInvKREP: TSingleField;
    quSpecInvVOL: TSingleField;
    quSpecInvQUANTR: TSingleField;
    quSpecInvQUANTF: TSingleField;
    quSpecInvINFO_A: TStringField;
    quSpecInvINFO_B: TStringField;
    ViewSpecInvID: TcxGridDBColumn;
    ViewSpecInvCAP: TcxGridDBColumn;
    ViewSpecInvNAME: TcxGridDBColumn;
    ViewSpecInvAVID: TcxGridDBColumn;
    ViewSpecInvKREP: TcxGridDBColumn;
    ViewSpecInvVOL: TcxGridDBColumn;
    ViewSpecInvQUANTR: TcxGridDBColumn;
    ViewSpecInvQUANTF: TcxGridDBColumn;
    ViewSpecInvINFO_A: TcxGridDBColumn;
    ViewSpecInvINFO_B: TcxGridDBColumn;
    quSpecInvQUANTD: TSingleField;
    ViewSpecInvQUANTD: TcxGridDBColumn;
    procedure acCloseExecute(Sender: TObject);
    procedure acSaveExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acDelPosExecute(Sender: TObject);
  private
    { Private declarations }
    FRARID:string;
    FIDATE: Integer;
    FIEDIT: Integer;
    FDATEDOC:TDateTime;
    FIDHEAD:Integer;

    procedure Init;

  public
    { Public declarations }
  end;

// var
//  fmDocSpec: TfmDocSpec;

procedure ShowSpecViewInv(RARID:string;IDATE,IDHEAD,IEDIT:Integer;DATEDOC:TDateTime);

implementation

{$R *.dfm}

uses dmRar, EgaisDecode;

procedure ShowSpecViewInv(RARID:string;IDATE,IDHEAD,IEDIT:Integer;DATEDOC:TDateTime);
var F:TfmDocSpecInv;
begin
  F:=TfmDocSpecInv.Create(Application);

  F.FRARID:=RARID;
  F.FIDATE:=IDATE;
  F.FIEDIT:=IEDIT;
  F.FDATEDOC:=DATEDOC;
  F.FIDHEAD:=IDHEAD;

  F.Init;
  F.Show;
end;

procedure TfmDocSpecInv.acCloseExecute(Sender: TObject);
begin
//  quSpecIn.Active:=False;
  Close;
end;

procedure TfmDocSpecInv.acDelPosExecute(Sender: TObject);
begin
  if FIEDIT=1 then
  begin
    quSpecInv.Delete;
  end;
end;

procedure TfmDocSpecInv.acSaveExecute(Sender: TObject);
var iErrors:Integer;
begin
  //��������� ������������
  quSpecInv.UpdateTransaction.StartTransaction;
  iErrors := quSpecInv.ApplyUpdates;
  if iErrors = 0 then begin
  end else
  begin
    ShowMessage('������ ����������.');
    quSpecInv.UpdateTransaction.Rollback;
  end;
end;

procedure TfmDocSpecInv.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
end;

procedure TfmDocSpecInv.Init;
begin
  Caption:='������������  '+ds1(FDATEDOC)+', '+its(FIDHEAD);

  ViewSpecInv.BeginUpdate;

  quSpecInv.Active:=False;
  quSpecInv.ParamByName('IDH').AsInteger:=FIDHEAD;
  quSpecInv.Active:=True;
  ViewSpecInv.EndUpdate;

  if FIEDIT=1 then
  begin
    acSave.Enabled:=True;
    ViewSpecInvQUANTF.Options.Editing:=True;
  end else
  begin
    acSave.Enabled:=False;
    ViewSpecInvQUANTF.Options.Editing:=False;
  end;
end;

end.
