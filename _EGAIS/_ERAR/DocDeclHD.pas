unit DocDeclHD;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  CustomChildForm, System.UITypes, ShellApi, httpsend, synautil, nativexml, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxRibbonSkins, dxRibbonCustomizationForm, cxContainer, cxEdit, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator,
  Data.DB, cxDBData, cxImageComboBox, cxTextEdit, cxDBLookupComboBox, cxCheckBox, cxCalendar, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.Menus, System.Actions, Vcl.ActnList, dxBar, cxBarEditItem, dxBarExtItems, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid, cxMemo, Vcl.ExtCtrls, dxRibbon, cxDropDownEdit, cxCalc;

type
  TfmDocDecl = class(TfmCustomChildForm)
    dxRibbon1: TdxRibbon;
    dxRibbon1Tab1: TdxRibbonTab;
    dxBarManager1: TdxBarManager;
    bmApplyDate: TdxBar;
    bmClose: TdxBar;
    btnDone: TdxBarLargeButton;
    btnClose: TdxBarLargeButton;
    deDateBeg: TdxBarDateCombo;
    deDateEnd: TdxBarDateCombo;
    dxBarGroup1: TdxBarGroup;
    ActionList1: TActionList;
    acRefresh: TAction;
    acClose: TAction;
    acSelectDate: TAction;
    GridDocDecl: TcxGrid;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    beiPoint: TcxBarEditItem;
    acExpExcel: TAction;
    dxBarLargeButton9: TdxBarLargeButton;
    dxBarLargeButton10: TdxBarLargeButton;
    beiGetTTN: TcxBarEditItem;
    dxBarLargeButton14: TdxBarLargeButton;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    LevelDocDecl: TcxGridLevel;
    ViewDocDecl: TcxGridDBTableView;
    pmDocVn: TPopupMenu;
    N17: TMenuItem;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton11: TdxBarLargeButton;
    acSendCorr: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    acResetStatus: TAction;
    N1: TMenuItem;
    acTestRemn: TAction;
    acAddDoc: TAction;
    N3: TMenuItem;
    N4: TMenuItem;
    acDelDoc: TAction;
    N5: TMenuItem;
    quDocsDecl: TFDQuery;
    dsquDocsDecl: TDataSource;
    ViewDocDeclINN: TcxGridDBColumn;
    ViewDocDeclKPP: TcxGridDBColumn;
    ViewDocDeclID: TcxGridDBColumn;
    ViewDocDeclDDATEB: TcxGridDBColumn;
    ViewDocDeclDDATEE: TcxGridDBColumn;
    ViewDocDeclIACTIVE: TcxGridDBColumn;
    ViewDocDeclIT1: TcxGridDBColumn;
    ViewDocDeclIT2: TcxGridDBColumn;
    ViewDocDeclIDATEB: TcxGridDBColumn;
    ViewDocDeclIDATEE: TcxGridDBColumn;
    dxBarButton1: TdxBarButton;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    dxBarLargeButton18: TdxBarLargeButton;
    dxBarLargeButton19: TdxBarLargeButton;
    dxBarLargeButton20: TdxBarLargeButton;
    dxBarLargeButton21: TdxBarLargeButton;
    acEditDoc: TAction;
    acViewDoc: TAction;
    acOn: TAction;
    acOff: TAction;
    acCalcDecl: TAction;
    dxBarManager1Bar1: TdxBar;
    dxBarLargeButton22: TdxBarLargeButton;
    acImportRemn: TAction;
    quDocsDeclMASTERORG: TIntegerField;
    quDocsDeclNAME: TStringField;
    quDocsDeclINN: TStringField;
    quDocsDeclKPP: TStringField;
    quDocsDeclID: TLargeintField;
    quDocsDeclIDATEB: TIntegerField;
    quDocsDeclIDATEE: TIntegerField;
    quDocsDeclDDATEB: TSQLTimeStampField;
    quDocsDeclDDATEE: TSQLTimeStampField;
    quDocsDeclIACTIVE: TSmallintField;
    quDocsDeclIT1: TSmallintField;
    quDocsDeclIT2: TSmallintField;
    quDocsDeclINUMCORR: TSmallintField;
    ViewDocDeclMASTERORG: TcxGridDBColumn;
    ViewDocDeclNAME: TcxGridDBColumn;
    N2: TMenuItem;
    acExpXML: TAction;
    dxBarLargeButton23: TdxBarLargeButton;
    FileSaveDialog: TFileSaveDialog;
    XML1: TMenuItem;
    quSelProd: TFDQuery;
    quSelProdPRODID: TStringField;
    quSelProdNAME: TStringField;
    quSelProdFULLNAME: TStringField;
    quSelProdPRODINN: TStringField;
    quSelProdPRODKPP: TStringField;
    quSelProdCOUNTRY: TStringField;
    quSelProdREGCODE: TStringField;
    quSelProdADDR: TStringField;
    quSelProdCLITYPE: TStringField;
    quSelCliDecl: TFDQuery;
    quSelCliDeclCLIENTID: TStringField;
    quSelCliDeclCLIENTINN: TStringField;
    quSelCliDeclCLIENTKPP: TStringField;
    quSelCliDeclFULLNAME: TStringField;
    quSelCliDeclNAME: TStringField;
    quSelCliDeclCOUNTRY: TStringField;
    quSelCliDeclREGCODE: TStringField;
    quSelCliDeclADDR: TStringField;
    quSelLic: TFDQuery;
    quSelLicIDCLI: TStringField;
    quSelLicIDL: TFDAutoIncField;
    quSelLicLICNUM: TStringField;
    quSelLicIDATEB: TIntegerField;
    quSelLicIDATEE: TIntegerField;
    quSelLicORGAN: TStringField;
    quSelOrgDecl: TFDQuery;
    quSelOrgDeclID: TIntegerField;
    quSelOrgDeclMAIN: TIntegerField;
    quSelOrgDeclFSRARID: TStringField;
    quSelOrgDeclNAME: TStringField;
    quSelOrgDeclPHONE: TStringField;
    quSelOrgDeclEMAILORG: TStringField;
    quSelOrgDeclINN: TStringField;
    quSelOrgDeclKPP: TStringField;
    quSelOrgDeclDIR1: TStringField;
    quSelOrgDeclDIR2: TStringField;
    quSelOrgDeclDIR3: TStringField;
    quSelOrgDeclGB1: TStringField;
    quSelOrgDeclGB2: TStringField;
    quSelOrgDeclGB3: TStringField;
    quSelOrgDeclADDR_CC: TStringField;
    quSelOrgDeclADDR_IND: TStringField;
    quSelOrgDeclADDR_REG: TStringField;
    quSelOrgDeclADDR_RN: TStringField;
    quSelOrgDeclADDR_CITY: TStringField;
    quSelOrgDeclADDR_NP: TStringField;
    quSelOrgDeclADDR_STR: TStringField;
    quSelOrgDeclADDR_D: TStringField;
    quSelOrgDeclADDR_KORP: TStringField;
    quSelOrgDeclADDR_L: TStringField;
    quSelOrgDeclADDR_KV: TStringField;
    quSelLicOrg: TFDQuery;
    quSelLicOrgIDORG: TIntegerField;
    quSelLicOrgID: TIntegerField;
    quSelLicOrgVIDD: TStringField;
    quSelLicOrgSERNUM: TStringField;
    quSelLicOrgDATEB: TSQLTimeStampField;
    quSelLicOrgDATEE: TSQLTimeStampField;
    quSelOrgsOb: TFDQuery;
    quSelOrgsObID: TIntegerField;
    quSelOrgsObMAIN: TIntegerField;
    quSelOrgsObFSRARID: TStringField;
    quSelOrgsObNAME: TStringField;
    quSelOrgsObPHONE: TStringField;
    quSelOrgsObEMAILORG: TStringField;
    quSelOrgsObINN: TStringField;
    quSelOrgsObKPP: TStringField;
    quSelOrgsObDIR1: TStringField;
    quSelOrgsObDIR2: TStringField;
    quSelOrgsObDIR3: TStringField;
    quSelOrgsObGB1: TStringField;
    quSelOrgsObGB2: TStringField;
    quSelOrgsObGB3: TStringField;
    quSelOrgsObADDR_CC: TStringField;
    quSelOrgsObADDR_IND: TStringField;
    quSelOrgsObADDR_REG: TStringField;
    quSelOrgsObADDR_RN: TStringField;
    quSelOrgsObADDR_CITY: TStringField;
    quSelOrgsObADDR_NP: TStringField;
    quSelOrgsObADDR_STR: TStringField;
    quSelOrgsObADDR_D: TStringField;
    quSelOrgsObADDR_KORP: TStringField;
    quSelOrgsObADDR_L: TStringField;
    quSelOrgsObADDR_KV: TStringField;
    quSelOrgsObQUANTREC: TIntegerField;
    quSelVidsOb: TFDQuery;
    quSelVidsObAVID: TIntegerField;
    quSelProdsOb: TFDQuery;
    quSelProdsObPRODID: TStringField;
    quSelProdsObREMNB: TSingleField;
    quSelProdsObQIN1: TSingleField;
    quSelProdsObQIN2: TSingleField;
    quSelProdsObQIN3: TSingleField;
    quSelProdsObQIN4: TSingleField;
    quSelProdsObQIN5: TSingleField;
    quSelProdsObQIN6: TSingleField;
    quSelProdsObQOUT1: TSingleField;
    quSelProdsObQOUT2: TSingleField;
    quSelProdsObQOUT3: TSingleField;
    quSelProdsObQOUT4: TSingleField;
    quSelProdsObREMNE: TSingleField;
    quSelProdsObQINIT1: TSingleField;
    quSelProdsObQINIT2: TSingleField;
    quSelProdsObQOUTIT1: TSingleField;
    quSelPostOb: TFDQuery;
    quSelPostObCLIENTID: TStringField;
    quSelPostObIDLIC: TIntegerField;
    quSelDocOb: TFDQuery;
    quSelDocObDOCDATE: TSQLTimeStampField;
    quSelDocObDOCNUM: TStringField;
    quSelDocObGTD: TStringField;
    quSelDocObQIN: TSingleField;
    acCalcDecl1: TAction;
    procedure acRefreshExecute(Sender: TObject);
    procedure acCloseExecute(Sender: TObject);
    procedure acSelectDateExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure deDateBegChange(Sender: TObject);
    procedure acExpExcelExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ViewDocDeclDblClick(Sender: TObject);
    procedure acResetStatusExecute(Sender: TObject);
    procedure acAddDocExecute(Sender: TObject);
    procedure acDelDocExecute(Sender: TObject);
    procedure acEditDocExecute(Sender: TObject);
    procedure acViewDocExecute(Sender: TObject);
    procedure acOnExecute(Sender: TObject);
    procedure acOffExecute(Sender: TObject);
    procedure acCalcDeclExecute(Sender: TObject);
    procedure acImportRemnExecute(Sender: TObject);
    procedure acExpXMLExecute(Sender: TObject);
    procedure acCalcDecl1Execute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }

    procedure Init;
  end;

var
  fmDocDecl: TfmDocDecl;

procedure ShowFormDecl;

implementation

{$R *.dfm}

uses ViewXML, EgaisDecode, ViewCards, dmRar, UTMExch, ViewSpecOut, MCrystDocs, ProDocs, History, UTMArh, UTMCheck, DocHeaderCB,
  ViewSpec, Main, ViewSpecCorr, TestRemnInv2, DocDeclSP, ImpRemnDecl, UnitFunction;

procedure ShowFormDecl;
begin
 if Assigned(fmDocDecl)=False then //����� �� ����������, � ����� �������
    fmDocDecl:=TfmDocDecl.Create(Application);
  if fmDocDecl.WindowState=wsMinimized then //���� ���� ��������, �� ������������� ���
    fmDocDecl.WindowState:=wsNormal;

  fmDocDecl.deDateBeg.Date:=Date-180;
  fmDocDecl.deDateEnd.Date:=Date;
  fmDocDecl.Init;
  fmDocDecl.Show;
end;

procedure TfmDocDecl.Init;
var
  FocusedRow, TopRow: Integer;
  flag: boolean;
begin
  with dmR do
  begin
    try
      //<--Refresh � ��������������� ������� ������� � �������
      flag:=quDocsDecl.Active;  //���������� ���� �� ������� �������, ��� �������������� ������� �������
      TopRow := ViewDocDecl.Controller.TopRowIndex;
      FocusedRow := ViewDocDecl.DataController.FocusedRowIndex;
      //-->
      ViewDocDecl.BeginUpdate;

      quDocsDecl.Active:=False;
      quDocsDecl.ParamByName('IDATEB').AsInteger:=Trunc(deDateBeg.Date);
      quDocsDecl.ParamByName('IDATEE').AsInteger:=Trunc(deDateEnd.Date);
      quDocsDecl.Active:=True;
      quDocsDecl.First;

      //-->
    finally
      ViewDocDecl.EndUpdate;
      //<--��������������� �������
      if flag then begin
        ViewDocDecl.DataController.FocusedRowIndex := FocusedRow;
        ViewDocDecl.Controller.TopRowIndex := TopRow;
      end;
      //-->
    end;
  end;
end;


procedure TfmDocDecl.ViewDocDeclDblClick(Sender: TObject);
begin
  //�������������
  if quDocsDecl.RecordCount>0 then
  begin
    if quDocsDeclIACTIVE.AsInteger=0 then ShowSpecViewADecl(quDocsDeclID.AsInteger,1,0)
    else ShowSpecViewADecl(quDocsDeclID.AsInteger,0,0);
  end;
end;

procedure TfmDocDecl.acAddDocExecute(Sender: TObject);
Var IDH:Integer;

begin
  //�������� ��������
  IDH:=fGetId(10);
  ShowSpecViewADecl(IDH,1,1);
end;

procedure TfmDocDecl.acCalcDecl1Execute(Sender: TObject);
begin
 //
  with dmR do
  begin
    if quDocsDecl.RecordCount>0 then
    begin
      if MessageDlg('�������� ��� ������� ��� ������ ����� ����������� (1). ��������� �������� '+quDocsDeclNAME.AsString+' ( '+quDocsDeclID.AsString+') c '+ds1(quDocsDeclDDATEB.AsDateTime)+'  ��  '+ds1(quDocsDeclDDATEE.AsDateTime)+'?',mtConfirmation, [mbYes, mbNo], 0, mbYes) = mrYes then
      begin
        ClearMessageLogLocal;
        ShowMessageLogLocal('�����.. ���� ������');
        if quDocsDeclIACTIVE.AsInteger=0 then
        begin
          try

            quS.Active:=False;
            quS.SQL.Clear;
            quS.SQL.Add('');
            quS.SQL.Add('DECLARE @IDH bigint = '+its(quDocsDeclID.AsInteger));
            quS.SQL.Add('EXECUTE [dbo].[prCalcADecl1] @IDH');
            quS.ExecSQL;
            Init;
          except
          end;
        end else ShowMessageLogLocal('  �������� ������ ���������. ������ ����������.');
        ShowMessageLogLocal('������� ��������.');
      end;
    end;
  end;
end;

procedure TfmDocDecl.acCalcDeclExecute(Sender: TObject);
begin
 //
  with dmR do
  begin
    if quDocsDecl.RecordCount>0 then
    begin
      if MessageDlg('�������� ��� ������� ��� ������ ����� �����������. ��������� �������� '+quDocsDeclNAME.AsString+' ( '+quDocsDeclID.AsString+') c '+ds1(quDocsDeclDDATEB.AsDateTime)+'  ��  '+ds1(quDocsDeclDDATEE.AsDateTime)+'?',mtConfirmation, [mbYes, mbNo], 0, mbYes) = mrYes then
      begin
        ClearMessageLogLocal;
        ShowMessageLogLocal('�����.. ���� ������');
        if quDocsDeclIACTIVE.AsInteger=0 then
        begin
          try

            quS.Active:=False;
            quS.SQL.Clear;
            quS.SQL.Add('');

            if quDocsDeclIT1.AsInteger=0 then
            begin
              quS.SQL.Add('DECLARE @IDH bigint = '+its(quDocsDeclID.AsInteger));
              quS.SQL.Add('EXECUTE [dbo].[prCalcADecl] @IDH');
            end else
            begin
              quS.SQL.Add('DECLARE @IDH bigint = '+its(quDocsDeclID.AsInteger));
              quS.SQL.Add('EXECUTE [dbo].[prCalcADecl2] @IDH');
            end;
            quS.ExecSQL;
            Init;
          except
          end;
        end else ShowMessageLogLocal('  �������� ������ ���������. ������ ����������.');
        ShowMessageLogLocal('������� ��������.');
      end;
    end;
  end;
end;

procedure TfmDocDecl.acCloseExecute(Sender: TObject);
begin
  //�������
  Close;
end;

procedure TfmDocDecl.acDelDocExecute(Sender: TObject);
begin
  //������� ��������
  with dmR do
  begin
    if quDocsDecl.RecordCount>0 then
    begin
      if MessageDlg('������� �������� '+quDocsDeclNAME.AsString+' ('+quDocsDeclID.AsString+') c '+ds1(quDocsDeclDDATEB.AsDateTime)+'  ��  '+ds1(quDocsDeclDDATEE.AsDateTime)+'?',mtConfirmation, [mbYes, mbNo], 0, mbYes) = mrYes then
      begin
        ClearMessageLogLocal;
        ShowMessageLogLocal('�����.. ���� �������� ���������');

        if quDocsDeclIACTIVE.AsInteger=0 then
        begin
          try
            quS.Active:=False;
            quS.SQL.Clear;
            quS.SQL.Add('');
            quS.SQL.Add('delete from [dbo].[ADECL_HD]');
            quS.SQL.Add('WHERE');
            quS.SQL.Add(' ID = '+its(quDocsDeclID.AsInteger));
            quS.ExecSQL;

            Init;
          except
          end;
        end else ShowMessageLogLocal('  �������� ������ ���������. �������� ����������.');


        ShowMessageLogLocal('������� ��������.');
      end;
    end;
  end;
end;

procedure TfmDocDecl.acEditDocExecute(Sender: TObject);
begin
  //�������������
  if quDocsDecl.RecordCount>0 then
  begin
    if quDocsDeclIACTIVE.AsInteger=0 then ShowSpecViewADecl(quDocsDeclID.AsInteger,1,0)
    else ShowSpecViewADecl(quDocsDeclID.AsInteger,0,0);
  end;
end;

procedure TfmDocDecl.acExpExcelExecute(Sender: TObject);
begin
  ExportGridToFile(formatdatetime('ddmmyyyy',deDateBeg.Date)+'_'+formatdatetime('ddmmyyyy',deDateEnd.Date)+'_'+formatdatetime('ddmmyyyy_hh_nn_ss',now)+'.xls', GridDocDecl);
end;

procedure TfmDocDecl.acExpXMLExecute(Sender: TObject);
var iM,iKv:Integer;
    sKv,sY,StrGuid,StrN,sInn,fName,sNumForm:string;
    a:TGuid;
    IDH:Integer;
    SendXml: TNativeXml;
    nodePos,nodePos1,nodePos2,nodePos3,nodePos4: TXmlNode;
    Id1,Id2,Id3:Integer;
    sResStr:string;
    iPos:Integer;
    F:TextFile;
begin
   //������� � XML
  with dmR do
  begin
    ClearMessageLogLocal;
    ShowMessageLogLocal('���������� ��������'); delay(10);

    if quDocsDecl.RecordCount>0 then
    begin
      IDH:=quDocsDeclID.AsInteger;
      ShowMessageLogLocal('  ��� ��������� - '+its(IDH)); delay(10);

      iM:=fDateToKvartal(quDocsDeclIDATEE.AsInteger);
      iKv:=0;
      if (iM>=1) and (iM<=3) then iKv:=3;
      if (iM>=4) and (iM<=6) then iKv:=6;
      if (iM>=7) and (iM<=9) then iKv:=9;
      if (iM>=10) and (iM<=12) then iKv:=0;

      sKv:=Its(iKv); sKv:='0'+sKv;
      sY:=FormatDateTime('yyyy',quDocsDeclIDATEE.AsInteger);
      while length(sY)>1 do delete(sY,1,1);

      ShowMessageLogLocal('   '+sY+' '+sKv); delay(10);

      CreateGUID(a);
      StrGuid:=GUIDTostring(a);

      ShowMessageLogLocal('   '+StrGuid); delay(10);

      if quDocsDeclIT1.AsInteger=0 then StrN:='R1_' else StrN:='R2_';
      if quDocsDeclIT1.AsInteger=0 then sNumForm:='11' else sNumForm:='12';

      ShowMessageLogLocal('   '+sNumForm); delay(10);

      sInn:=quDocsDeclINN.AsString;
      StrN:=StrN+sInn+'_'+sKv+sY+'_'+formatdatetime('ddmmyyyy',date)+'_'+StrGuid+'.xml';
      while pos('{',StrN)>0 do delete(StrN,pos('{',StrN),1);
      while pos('}',StrN)>0 do delete(StrN,pos('}',StrN),1);

      fName:=CurDir+StrN;

      ShowMessageLogLocal('   '+fName); delay(10);

{
      FileSaveDialog.DefaultFolder:=CurDir;
      FileSaveDialog.fileName:=StrN;
      if FileSaveDialog.Execute then
      begin

}
      if fName>'' then  //�������� ������� - �� ����� �� �������� - �������� ���� ��� ������
      begin
        try
//          fName:=FileSaveDialog.fileName;
          ShowMessageLogLocal('������� � XML � ���� - '+fName); delay(10);

          SendXml := TNativeXml.Create(nil);
          SendXml.XmlFormat := xfReadable;
//          SendXml.Charset :='windows-1251';
//          SendXml.Charset := 'Windows-1251';
//          SendXml.ExternalCodepage:= 1251;
//          SendXml.ExternalEncoding := seAnsi;

          SendXml.CreateName('����');
          SendXml.Root.WriteAttributeString('�������', dts(date));
          SendXml.Root.WriteAttributeString('��������', '4.31');
          SendXml.Root.WriteAttributeString('��������', '��������������.������');

          SendXml.Root.NodeNew('��������');
          SendXml.Root.NodeByName('��������').WriteAttributeString('�������', sNumForm);
          SendXml.Root.NodeByName('��������').WriteAttributeString('�������������',its(iKv));
          SendXml.Root.NodeByName('��������').WriteAttributeString('������������',FormatDateTime('yyyy',quDocsDeclIDATEE.AsInteger));
          SendXml.Root.NodeByName('��������').NodeNew('��������������');
          SendXml.Root.NodeByName('��������').NodeByName('��������������').WriteAttributeString('���������', its(quDocsDeclINUMCORR.AsInteger));

          ShowMessageLogLocal('       �������������.'); delay(10);
          SendXml.Root.NodeNew('�����������');

          quSelProd.Active:=False;
          quSelProd.ParamByName('IDH').AsInteger:=IDH;
          quSelProd.Active:=True;

          quSelProd.First;
          while not quSelProd.Eof do
          begin
            nodePos:=SendXml.Root.NodeByName('�����������').NodeNew('����������������������');

            nodePos.WriteAttributeString('�����������', quSelProdPRODID.asstring);
            nodePos.WriteAttributeString('�000000000004', quSelProdNAME.asstring);

            if sNumForm='11' then
            begin
              nodePos.WriteAttributeString('�000000000005', quSelProdPRODINN.asstring);
              if Trim(quSelProdPRODKPP.AsString)>'' then nodePos.WriteAttributeString('�000000000006', quSelProdPRODKPP.AsString);
            end;
            if sNumForm='12' then
            begin
              if quSelProdCLITYPE.AsString='FL' then
              begin
                nodePos.NodeNew('��');
                nodePos.NodeByName('��').WriteAttributeString('�000000000005', quSelProdPRODINN.asstring);
              end else
              begin
                nodePos.NodeNew('��');
                nodePos.NodeByName('��').WriteAttributeString('�000000000005', quSelProdPRODINN.asstring);
                if Trim(quSelProdPRODKPP.AsString)>'' then nodePos.NodeByName('��').WriteAttributeString('�000000000006', quSelProdPRODKPP.AsString);
              end;
            end;

            quSelProd.Next;
          end;
          quSelProd.Active:=False;

          ShowMessageLogLocal('       ����������.'); delay(10);

          quSelCliDecl.Active:=False;
          quSelCliDecl.ParamByName('IDHEAD').AsInteger:=IDH;
          quSelCliDecl.Active:=True;

          quSelCliDecl.First;
          while not quSelCliDecl.Eof do
          begin
            nodePos:=SendXml.Root.NodeByName('�����������').NodeNew('����������');

            nodePos.WriteAttributeString('��������', quSelCliDeclCLIENTID.asstring);
            nodePos.WriteAttributeString('�000000000007', quSelCliDeclFULLNAME.asstring);

            //��������

            quSelLic.Active:=False;
            quSelLic.ParamByName('IDCLI').AsString:=quSelCliDeclCLIENTID.asstring;
            quSelLic.Active:=True;

            quSelLic.First;
            while not quSelLic.Eof do
            begin
              nodePos1:=nodePos.NodeNew('��������');
              nodePos1.NodeNew('��������');
              nodePos1.NodeByName('��������').WriteAttributeString('����������', quSelLicIDL.asstring);
              nodePos1.NodeByName('��������').WriteAttributeString('�000000000011', quSelLicLICNUM.asstring);
              nodePos1.NodeByName('��������').WriteAttributeString('�000000000012', dts(quSelLicIDATEB.AsInteger));
              nodePos1.NodeByName('��������').WriteAttributeString('�000000000013', dts(quSelLicIDATEE.AsInteger));
              nodePos1.NodeByName('��������').WriteAttributeString('�000000000014', quSelLicORGAN.asstring);

              quSelLic.Next;
            end;
            quSelLic.Active:=False;

            nodePos.NodeNew('��');
            nodePos.NodeByName('��').WriteAttributeString('�000000000009', quSelCliDeclCLIENTINN.asstring);
            if Trim(quSelCliDeclCLIENTKPP.AsString)>'' then nodePos.NodeByName('��').WriteAttributeString('�000000000010', quSelCliDeclCLIENTKPP.AsString);

            quSelCliDecl.Next;
          end;
          quSelCliDecl.Active:=False;

          ShowMessageLogLocal('       ��������.'); delay(10);
          SendXml.Root.NodeNew('��������');
          nodePos:=SendXml.Root.NodeByName('��������').NodeNew('�����������');

          nodePos.NodeNew('���������');

          quSelOrgDecl.Active:=False;
          quSelOrgDecl.ParamByName('IDORG').AsInteger:=quDocsDeclMASTERORG.AsInteger;
          quSelOrgDecl.Active:=True;

          nodePos.NodeByName('���������').WriteAttributeString('����', quSelOrgDeclNAME.asstring);
          nodePos.NodeByName('���������').WriteAttributeString('������', quSelOrgDeclPHONE.asstring);
          nodePos.NodeByName('���������').WriteAttributeString('Email����', quSelOrgDeclEMAILORG.asstring);

          nodePos.NodeByName('���������').NodeNew('������');
          nodePos.NodeByName('���������').NodeByName('������').NodeNew('���������').Value:=quSelOrgDeclADDR_CC.AsString;
          nodePos.NodeByName('���������').NodeByName('������').NodeNew('������').Value:=quSelOrgDeclADDR_IND.AsString;
          nodePos.NodeByName('���������').NodeByName('������').NodeNew('���������').Value:=quSelOrgDeclADDR_REG.AsString;
          nodePos.NodeByName('���������').NodeByName('������').NodeNew('�����').Value:=quSelOrgDeclADDR_RN.AsString;
          nodePos.NodeByName('���������').NodeByName('������').NodeNew('�����').Value:=quSelOrgDeclADDR_CITY.AsString;
          nodePos.NodeByName('���������').NodeByName('������').NodeNew('����������').Value:=quSelOrgDeclADDR_NP.AsString;
          nodePos.NodeByName('���������').NodeByName('������').NodeNew('�����').Value:=quSelOrgDeclADDR_STR.AsString;
          nodePos.NodeByName('���������').NodeByName('������').NodeNew('���').Value:=quSelOrgDeclADDR_D.AsString;
          nodePos.NodeByName('���������').NodeByName('������').NodeNew('������').Value:=quSelOrgDeclADDR_KORP.AsString;
          nodePos.NodeByName('���������').NodeByName('������').NodeNew('������').Value:=quSelOrgDeclADDR_L.AsString;
          nodePos.NodeByName('���������').NodeByName('������').NodeNew('�����').Value:=quSelOrgDeclADDR_KV.AsString;

          nodePos.NodeByName('���������').NodeNew('��');
          nodePos.NodeByName('���������').NodeByName('��').WriteAttributeString('�����', quSelOrgDeclINN.asstring);
          nodePos.NodeByName('���������').NodeByName('��').WriteAttributeString('�����', quSelOrgDeclKPP.asstring);

          nodePos.NodeNew('���������');
          nodePos.NodeByName('���������').NodeNew('������������');
          nodePos.NodeByName('���������').NodeByName('������������').NodeNew('�������').Value:=quSelOrgDeclDIR1.AsString;
          nodePos.NodeByName('���������').NodeByName('������������').NodeNew('���').Value:=quSelOrgDeclDIR2.AsString;
          nodePos.NodeByName('���������').NodeByName('������������').NodeNew('��������').Value:=quSelOrgDeclDIR3.AsString;

          nodePos.NodeByName('���������').NodeNew('�������');
          nodePos.NodeByName('���������').NodeByName('�������').NodeNew('�������').Value:=quSelOrgDeclGB1.AsString;
          nodePos.NodeByName('���������').NodeByName('�������').NodeNew('���').Value:=quSelOrgDeclGB2.AsString;
          nodePos.NodeByName('���������').NodeByName('�������').NodeNew('��������').Value:=quSelOrgDeclGB3.AsString;

          quSelOrgDecl.Active:=False;

          if sNumForm='11' then
          begin
            nodePos.NodeNew('������������');
            nodePos.NodeByName('������������').NodeNew('�������������');

            quSelLicOrg.Active:=False;
            quSelLicOrg.ParamByName('IDORG').AsInteger:=quDocsDeclMASTERORG.AsInteger;
            quSelLicOrg.Active:=True;
            quSelLicOrg.First;

            while not quSelLicOrg.Eof do
            begin
              nodePos1:=nodePos.NodeByName('������������').NodeByName('�������������').NodeNew('��������');
              nodePos1.WriteAttributeString('�������',quSelLicOrgVIDD.asstring);
              nodePos1.WriteAttributeString('���������',quSelLicOrgSERNUM.asstring);
              nodePos1.WriteAttributeString('����������',dts(quSelLicOrgDATEB.AsDateTime));
              nodePos1.WriteAttributeString('�����������',dts(quSelLicOrgDATEE.AsDateTime));

              quSelLicOrg.Next;
            end;
            quSelLicOrg.Active:=False;
          end;

          //������� ������
          //�����������

          quSelOrgsOb.Active:=False;
          quSelOrgsOb.ParamByName('IDH').AsInteger:=IDH;
          quSelOrgsOb.Active:=True;
          quSelOrgsOb.First;
          while not quSelOrgsOb.Eof do
          begin
            nodePos:=SendXml.Root.NodeByName('��������').NodeNew('������������');
            nodePos.WriteAttributeString('����',quSelOrgsObNAME.asstring);
            nodePos.WriteAttributeString('�����',quSelOrgsObKPP.asstring);
            if quSelOrgsObQUANTREC.AsFloat>0.001 then nodePos.WriteAttributeString('��������������','true') else nodePos.WriteAttributeString('��������������','false');

            nodePos.NodeNew('������');
            nodePos.NodeByName('������').NodeNew('���������').Value:=quSelOrgsObADDR_CC.AsString;
            nodePos.NodeByName('������').NodeNew('������').Value:=quSelOrgsObADDR_IND.AsString;
            nodePos.NodeByName('������').NodeNew('���������').Value:=quSelOrgsObADDR_REG.AsString;
            nodePos.NodeByName('������').NodeNew('�����').Value:=quSelOrgsObADDR_RN.AsString;
            nodePos.NodeByName('������').NodeNew('�����').Value:=quSelOrgsObADDR_CITY.AsString;
            nodePos.NodeByName('������').NodeNew('����������').Value:=quSelOrgsObADDR_NP.AsString;
            nodePos.NodeByName('������').NodeNew('�����').Value:=quSelOrgsObADDR_STR.AsString;
            nodePos.NodeByName('������').NodeNew('���').Value:=quSelOrgsObADDR_D.AsString;
            nodePos.NodeByName('������').NodeNew('������').Value:=quSelOrgsObADDR_KORP.AsString;
            nodePos.NodeByName('������').NodeNew('������').Value:=quSelOrgsObADDR_L.AsString;
            nodePos.NodeByName('������').NodeNew('�����').Value:=quSelOrgsObADDR_KV.AsString;

            if quSelOrgsObQUANTREC.AsFloat>0.001 then
            begin    //������ ����
              quSelVidsOb.Active:=False;
              quSelVidsOb.ParamByName('IDH').AsInteger:=IDH;
              quSelVidsOb.ParamByName('IDORG').AsInteger:=quSelOrgsObID.AsInteger;
              quSelVidsOb.Active:=True;
              quSelVidsOb.First;
              Id1:=1;
              while not quSelVidsOb.Eof do
              begin
                nodePos1:=nodePos.NodeNew('������');
                nodePos1.WriteAttributeString('�N',its(Id1));
                nodePos1.WriteAttributeString('�000000000003',quSelVidsObAVID.AsString);

                Id2:=1;
                quSelProdsOb.Active:=False;
                quSelProdsOb.ParamByName('IDH').AsInteger:=IDH;
                quSelProdsOb.ParamByName('IDORG').AsInteger:=quSelOrgsObID.AsInteger;
                quSelProdsOb.ParamByName('AVID').AsInteger:=quSelVidsObAVID.AsInteger;
                quSelProdsOb.Active:=True;
                quSelProdsOb.First;
                while not quSelProdsOb.Eof do
                begin
                  nodePos2:=nodePos1.NodeNew('����������������');
                  nodePos2.WriteAttributeString('�N',its(Id2));
                  nodePos2.WriteAttributeString('�����������',quSelProdsObPRODID.AsString);

                  quSelPostOb.Active:=False;
                  quSelPostOb.ParamByName('IDH').AsInteger:=IDH;
                  quSelPostOb.ParamByName('IDORG').AsInteger:=quSelOrgsObID.AsInteger;
                  quSelPostOb.ParamByName('AVID').AsInteger:=quSelVidsObAVID.AsInteger;
                  quSelPostOb.ParamByName('IDPROD').AsString:=quSelProdsObPRODID.AsString;
                  quSelPostOb.Active:=True;

                  if quSelPostOb.RecordCount>0 then
                  begin
                    quSelPostOb.First;  Id3:=1;
                    while not quSelPostOb.Eof do
                    begin
                      nodePos3:=nodePos2.NodeNew('���������');

                      nodePos3.WriteAttributeString('�N',its(Id3));
                      nodePos3.WriteAttributeString('������������',quSelPostObCLIENTID.AsString);
                      if sNumForm='11' then nodePos3.WriteAttributeString('����������',quSelPostObIDLIC.AsString);

                      //���������
                      quSelDocOb.Active:=False;
                      quSelDocOb.ParamByName('IDH').AsInteger:=IDH;
                      quSelDocOb.ParamByName('IDORG').AsInteger:=quSelOrgsObID.AsInteger;
                      quSelDocOb.ParamByName('AVID').AsInteger:=quSelVidsObAVID.AsInteger;
                      quSelDocOb.ParamByName('IDPROD').AsString:=quSelProdsObPRODID.AsString;
                      quSelDocOb.ParamByName('IDCLI').AsString:=quSelPostObCLIENTID.AsString;
                      quSelDocOb.Active:=True;
                      quSelDocOb.First;
                      while not quSelDocOb.Eof do
                      begin
                        nodePos4:=nodePos3.NodeNew('���������');
                        nodePos4.WriteAttributeString('�200000000013',dts(quSelDocObDOCDATE.AsDateTime));
                        nodePos4.WriteAttributeString('�200000000014',quSelDocObDOCNUM.AsString);
                        nodePos4.WriteAttributeString('�200000000015',Trim(quSelDocObGTD.AsString));
                        nodePos4.WriteAttributeString('�200000000016',rfmt(quSelDocObQIN.AsFloat));


                        quSelDocOb.Next;
                      end;
                      quSelDocOb.Active:=False;

                      quSelPostOb.Next; inc(Id3);
                    end;
                  end;
                  quSelPostOb.Active:=False;

                  nodePos2.NodeNew('��������');
                  if sNumForm='11' then
                  begin
                    nodePos2.NodeByName('��������').WriteAttributeString('�N','1');
                    nodePos2.NodeByName('��������').WriteAttributeString('�100000000006',rfmt(quSelProdsObREMNB.AsFloat));
                    nodePos2.NodeByName('��������').WriteAttributeString('�100000000007',rfmt(quSelProdsObQIN1.AsFloat));
                    nodePos2.NodeByName('��������').WriteAttributeString('�100000000008',rfmt(quSelProdsObQIN2.AsFloat));
                    nodePos2.NodeByName('��������').WriteAttributeString('�100000000009',rfmt(quSelProdsObQIN3.AsFloat));
                    nodePos2.NodeByName('��������').WriteAttributeString('�100000000010',rfmt(quSelProdsObQINIT1.AsFloat));
                    nodePos2.NodeByName('��������').WriteAttributeString('�100000000011',rfmt(quSelProdsObQIN4.AsFloat));
                    nodePos2.NodeByName('��������').WriteAttributeString('�100000000012',rfmt(quSelProdsObQIN5.AsFloat));
                    nodePos2.NodeByName('��������').WriteAttributeString('�100000000013',rfmt(quSelProdsObQIN6.AsFloat));
                    nodePos2.NodeByName('��������').WriteAttributeString('�100000000014',rfmt(quSelProdsObQINIT2.AsFloat));
                    nodePos2.NodeByName('��������').WriteAttributeString('�100000000015',rfmt(quSelProdsObQOUT1.AsFloat));
                    nodePos2.NodeByName('��������').WriteAttributeString('�100000000016',rfmt(quSelProdsObQOUT2.AsFloat));
                    nodePos2.NodeByName('��������').WriteAttributeString('�100000000017',rfmt(quSelProdsObQOUT3.AsFloat));
                    nodePos2.NodeByName('��������').WriteAttributeString('�100000000018',rfmt(quSelProdsObQOUT4.AsFloat));
                    nodePos2.NodeByName('��������').WriteAttributeString('�100000000019',rfmt(quSelProdsObQOUTIT1.AsFloat));
                    nodePos2.NodeByName('��������').WriteAttributeString('�100000000020',rfmt(quSelProdsObREMNE.AsFloat));
                    nodePos2.NodeByName('��������').WriteAttributeString('�100000000021',rfmt(0));
                  end;

                  if sNumForm='12' then
                  begin
                    nodePos2.NodeByName('��������').WriteAttributeString('�N','1');
                    nodePos2.NodeByName('��������').WriteAttributeString('�100000000006',rfmt(quSelProdsObREMNB.AsFloat));
                    nodePos2.NodeByName('��������').WriteAttributeString('�100000000007',rfmt(quSelProdsObQIN1.AsFloat));
                    nodePos2.NodeByName('��������').WriteAttributeString('�100000000008',rfmt(quSelProdsObQIN2.AsFloat));
                    nodePos2.NodeByName('��������').WriteAttributeString('�100000000009',rfmt(quSelProdsObQIN3.AsFloat));
                    nodePos2.NodeByName('��������').WriteAttributeString('�100000000010',rfmt(quSelProdsObQINIT1.AsFloat));
                    nodePos2.NodeByName('��������').WriteAttributeString('�100000000011',rfmt(quSelProdsObQIN4.AsFloat));
                    nodePos2.NodeByName('��������').WriteAttributeString('�100000000012',rfmt(quSelProdsObQIN5.AsFloat));
                    nodePos2.NodeByName('��������').WriteAttributeString('�100000000013',rfmt(quSelProdsObQINIT2.AsFloat));
                    nodePos2.NodeByName('��������').WriteAttributeString('�100000000014',rfmt(quSelProdsObQOUT1.AsFloat));
                    nodePos2.NodeByName('��������').WriteAttributeString('�100000000015',rfmt(quSelProdsObQOUT2.AsFloat));
                    nodePos2.NodeByName('��������').WriteAttributeString('�100000000016',rfmt(quSelProdsObQOUT3.AsFloat));
                    nodePos2.NodeByName('��������').WriteAttributeString('�100000000017',rfmt(quSelProdsObQOUTIT1.AsFloat));
                    nodePos2.NodeByName('��������').WriteAttributeString('�100000000018',rfmt(quSelProdsObREMNE.AsFloat));
                  end;

                  quSelProdsOb.Next; Inc(Id2);
                end;
                quSelProdsOb.Active:=False;

                quSelVidsOb.Next; Inc(Id1);
              end;
            end;

            quSelOrgsOb.Next;
          end;
          quSelOrgsOb.Active:=False;

//          SendXml.SaveToFile(fName);  // � UTF
          sResStr:=SendXml.WriteToString;    //��� ���� � win ���������
//          sResStr:=UTF8ToString(sResStr);
//          sResStr:=UTF8ToString(GetXmlReadable(SendXml.WriteToString));
          iPos:=Pos('UTF-8',sResStr);
          if iPos>0 then                //windows-1251
          begin
            Delete(sResStr,iPos,5);
            insert('windows-1251',sResStr,iPos);
          end;

          try
            AssignFile(F,fName);
            Rewrite(F);
            Writeln(F,sResStr);
            CloseFile(F);
          except
            ShowMessageLogLocal('  ������ ���������� �����.'); delay(10);
          end;

          ShowMessageLogLocal('������� ��������.'); delay(10);
        finally
          SendXml.Free;
        end;
      end;
    end;
  end;
end;

procedure TfmDocDecl.acImportRemnExecute(Sender: TObject);
begin
   //��������� ������� �� ��������� ������� �� XML �����
  try
    fmImportRemnDecl:=TfmImportRemnDecl.Create(Application);
    fmImportRemnDecl.ShowModal;
  finally
    fmImportRemnDecl.Free;
  end;
end;

procedure TfmDocDecl.acOffExecute(Sender: TObject);
begin
  //��������
  with dmR do
  begin
    if quDocsDecl.RecordCount>0 then
    begin
      if quDocsDeclIACTIVE.AsInteger=1 then
      begin
        quS.Active:=False;
        quS.SQL.Clear;
        quS.SQL.Add('');
        quS.SQL.Add('UPDATE [dbo].[ADECL_HD]');
        quS.SQL.Add('SET IACTIVE = 0');
        quS.SQL.Add('WHERE');
        quS.SQL.Add('  ID = '+its(quDocsDeclID.AsInteger));
        quS.ExecSQL;

        Init;
      end;
    end;
  end;
end;

procedure TfmDocDecl.acOnExecute(Sender: TObject);
begin
  //������������
  with dmR do
  begin
    if quDocsDecl.RecordCount>0 then
    begin
      if quDocsDeclIACTIVE.AsInteger=0 then
      begin
        quS.Active:=False;
        quS.SQL.Clear;
        quS.SQL.Add('');
        quS.SQL.Add('UPDATE [dbo].[ADECL_HD]');
        quS.SQL.Add('SET IACTIVE = 1');
        quS.SQL.Add('WHERE');
        quS.SQL.Add('  ID = '+its(quDocsDeclID.AsInteger));
        quS.ExecSQL;

        Init;
      end;
    end;
  end;
end;

procedure TfmDocDecl.acRefreshExecute(Sender: TObject);
begin
  //������
  Init;
end;

procedure TfmDocDecl.acResetStatusExecute(Sender: TObject);
begin
  //
  with dmR do
  begin
    {
    if quDocsCorr.RecordCount>0 then
    begin
      quS.Active:=False;
      quS.SQL.Clear;
      quS.SQL.Add('');

      quS.SQL.Add('UPDATE dbo.ADOCSCORR_HD');
      quS.SQL.Add('SET IACTIVE = 0');
      quS.SQL.Add('WHERE');
      quS.SQL.Add('  FSRARID = '''+quDocsCorrFSRARID.AsString+'''');
      quS.SQL.Add('  AND IDATE = '+its(quDocsCorrIDATE.AsInteger));
      quS.SQL.Add('  AND SID = '''+quDocsCorrSID.AsString+'''');

      quS.ExecSQL;

      Init;
    end;
    }
  end;
end;

procedure TfmDocDecl.acSelectDateExecute(Sender: TObject);
begin
  //��������
  Init;
end;

procedure TfmDocDecl.acViewDocExecute(Sender: TObject);
begin
  //��������
  if quDocsDecl.RecordCount>0 then ShowSpecViewADecl(quDocsDeclID.AsInteger,0,0);
end;

procedure TfmDocDecl.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
  fmDocDecl:=Nil;
end;

procedure TfmDocDecl.FormCreate(Sender: TObject);
begin
  ClearMessageLogLocal;
end;

procedure TfmDocDecl.deDateBegChange(Sender: TObject);
begin
  if dmR.FDConnection.Connected then Init;
end;

end.
