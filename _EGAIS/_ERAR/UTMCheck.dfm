object fmUTMCheck: TfmUTMCheck
  Left = 0
  Top = 0
  Caption = #1059#1058#1052' '#1087#1088#1086#1074#1077#1088#1082#1072
  ClientHeight = 528
  ClientWidth = 1174
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1174
    Height = 127
    BarManager = dxBarManager1
    Style = rs2010
    ColorSchemeName = 'Blue'
    SupportNonClientDrawing = True
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1059#1058#1052' '#1087#1088#1086#1074#1077#1088#1082#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end>
      Index = 0
    end
  end
  object GridCheck: TcxGrid
    Left = 0
    Top = 127
    Width = 1174
    Height = 401
    Align = alClient
    TabOrder = 5
    LevelTabs.Style = 8
    LookAndFeel.Kind = lfOffice11
    LookAndFeel.NativeStyle = False
    ExplicitHeight = 319
    object ViewCheck: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.InfoPanel.Visible = True
      Navigator.Visible = True
      OnCustomDrawCell = ViewCheckCustomDrawCell
      DataController.DataSource = dsUTM
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      Images = dmR.SmallImage
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.MultiSelect = True
      OptionsView.GroupByBox = False
      OptionsView.HeaderAutoHeight = True
      object ViewCheckRARID: TcxGridDBColumn
        DataBinding.FieldName = 'RARID'
        Visible = False
        Width = 109
      end
      object ViewCheckIPUTM: TcxGridDBColumn
        Caption = 'IP '#1072#1076#1088#1077#1089' '#1059#1058#1052
        DataBinding.FieldName = 'IPUTM'
        Width = 105
      end
      object ViewCheckName: TcxGridDBColumn
        DataBinding.FieldName = 'Name'
        Width = 177
      end
      object ViewCheckIShop: TcxGridDBColumn
        Caption = #1053#1086#1084#1077#1088' '#1084#1072#1075#1072#1079#1080#1085#1072
        DataBinding.FieldName = 'IShop'
      end
      object ViewCheckIsLinked: TcxGridDBColumn
        Caption = #1057#1074#1103#1079#1100' '#1089' '#1059#1058#1052
        DataBinding.FieldName = 'IsLinked'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.Images = dmR.SmallImage
        Properties.Items = <
          item
            Description = #1053#1077#1090' '#1089#1074#1103#1079#1080
            ImageIndex = 154
            Value = 0
          end
          item
            Description = #1057#1074#1103#1079#1100' '#1077#1089#1090#1100
            ImageIndex = 156
            Value = 1
          end
          item
            Description = #1055#1088#1086#1074#1077#1088#1082#1072
            ImageIndex = 155
            Value = 2
          end>
        Width = 101
      end
      object ViewCheckVersion: TcxGridDBColumn
        Caption = #1042#1077#1088#1089#1080#1103' '#1059#1058#1052
        DataBinding.FieldName = 'Version'
        Width = 325
      end
      object ViewCheckBufferAge: TcxGridDBColumn
        Caption = #1044#1072#1090#1072' '#1089#1072#1084#1086#1075#1086' '#1089#1090#1072#1088#1086#1075#1086' '#1085#1077#1086#1090#1087#1088#1072#1074#1083#1077#1085#1085#1086#1075#1086' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
        DataBinding.FieldName = 'BufferAge'
        Width = 320
      end
      object ViewCheckDBufferAge: TcxGridDBColumn
        Caption = #1044#1072#1090#1072' '#1089#1072#1084#1086#1075#1086' '#1089#1090#1072#1088#1086#1075#1086' '#1085#1077#1086#1090#1087#1088#1072#1074#1083#1077#1085#1085#1086#1075#1086' '#1076#1086#1082#1091#1084#1077#1085#1090#1072' ('#1044#1072#1090#1072' '#1080' '#1074#1088#1077#1084#1103')'
        DataBinding.FieldName = 'DBufferAge'
        Visible = False
        Width = 173
      end
      object ViewCheckDateCreateDB: TcxGridDBColumn
        Caption = #1044#1072#1090#1072' '#1089#1086#1079#1076#1072#1085#1080#1103' '#1073#1072#1079#1099' '#1076#1072#1085#1085#1099#1093
        DataBinding.FieldName = 'DateCreateDB'
        Visible = False
        Width = 139
      end
      object ViewCheckResultCode: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1086#1090#1074#1077#1090#1072' '#1059#1058#1052
        DataBinding.FieldName = 'ResultCode'
        Width = 71
      end
      object ViewCheckResultString: TcxGridDBColumn
        Caption = #1057#1090#1088#1086#1082#1072' '#1086#1090#1074#1077#1090#1072' '#1059#1058#1052
        DataBinding.FieldName = 'ResultString'
        Width = 200
      end
    end
    object GridCheckDBTableView1: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.First.Visible = True
      Navigator.Buttons.PriorPage.Visible = True
      Navigator.Buttons.Prior.Visible = True
      Navigator.Buttons.Next.Visible = True
      Navigator.Buttons.NextPage.Visible = True
      Navigator.Buttons.Last.Visible = True
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Append.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.Visible = True
      DataController.DataSource = dsUTM
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      object GridCheckDBTableView1RARID: TcxGridDBColumn
        DataBinding.FieldName = 'RARID'
      end
      object GridCheckDBTableView1IPUTM: TcxGridDBColumn
        DataBinding.FieldName = 'IPUTM'
      end
      object GridCheckDBTableView1Name: TcxGridDBColumn
        DataBinding.FieldName = 'Name'
      end
      object GridCheckDBTableView1IShop: TcxGridDBColumn
        DataBinding.FieldName = 'IShop'
      end
      object GridCheckDBTableView1IsLinked: TcxGridDBColumn
        DataBinding.FieldName = 'IsLinked'
      end
      object GridCheckDBTableView1Version: TcxGridDBColumn
        DataBinding.FieldName = 'Version'
      end
      object GridCheckDBTableView1DateCreateDB: TcxGridDBColumn
        DataBinding.FieldName = 'DateCreateDB'
      end
      object GridCheckDBTableView1BufferAge: TcxGridDBColumn
        DataBinding.FieldName = 'BufferAge'
      end
    end
    object LevelCheck: TcxGridLevel
      Caption = #1042#1093#1086#1076#1103#1097#1080#1077
      GridView = ViewCheck
    end
  end
  object ActionList1: TActionList
    Images = dmR.SmallImage
    Left = 344
    Top = 152
    object acRefresh: TAction
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100' (F5)'
      ImageIndex = 5
      OnExecute = acRefreshExecute
    end
    object acClose: TAction
      Caption = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 100
      OnExecute = acCloseExecute
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmR.SmallImage
    ImageOptions.LargeImages = dmR.LargeImage
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 432
    Top = 152
    DockControlHeights = (
      0
      0
      0
      0)
    object dxBarManager1Bar1: TdxBar
      Caption = #1044#1077#1081#1089#1090#1074#1080#1103
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 941
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1047#1072#1082#1088#1099#1090#1100
      CaptionButtons = <>
      DockedLeft = 81
      DockedTop = 0
      FloatLeft = 941
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = acRefresh
      Category = 0
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = acClose
      Category = 0
    end
  end
  object meUTM: TFDMemTable
    FieldDefs = <>
    IndexDefs = <>
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    AutoCommitUpdates = False
    StoreDefs = True
    Left = 104
    Top = 204
    object meUTMRARID: TStringField
      FieldName = 'RARID'
      Size = 50
    end
    object meUTMIPUTM: TStringField
      FieldName = 'IPUTM'
      Size = 50
    end
    object meUTMName: TStringField
      DisplayLabel = #1053#1072#1079#1074#1072#1085#1080#1077
      FieldName = 'Name'
      Size = 50
    end
    object meUTMIShop: TIntegerField
      FieldName = 'IShop'
    end
    object meUTMIsLinked: TIntegerField
      DisplayLabel = #1051#1080#1085#1082
      FieldName = 'IsLinked'
    end
    object meUTMVersion: TStringField
      FieldName = 'Version'
      Size = 100
    end
    object meUTMDateCreateDB: TStringField
      FieldName = 'DateCreateDB'
      Size = 30
    end
    object meUTMBufferAge: TStringField
      FieldName = 'BufferAge'
      Size = 50
    end
    object meUTMDBufferAge: TDateTimeField
      FieldName = 'DBufferAge'
    end
    object meUTMResultCode: TIntegerField
      FieldName = 'ResultCode'
    end
    object meUTMResultString: TStringField
      FieldName = 'ResultString'
      Size = 250
    end
  end
  object dsUTM: TDataSource
    DataSet = meUTM
    Left = 164
    Top = 204
  end
  object TimerUTM: TTimer
    Enabled = False
    Interval = 500
    OnTimer = TimerUTMTimer
    Left = 104
    Top = 260
  end
end
