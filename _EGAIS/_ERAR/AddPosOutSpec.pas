unit AddPosOutSpec;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic,
  dxSkinSharp, dxSkinSharpPlus, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint, dxSkinXmas2008Blue,
  cxMaskEdit, cxDropDownEdit, cxCalc, cxTextEdit
  ,CustomChildForm
  , Vcl.StdCtrls, Vcl.ExtCtrls, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client, cxCurrencyEdit;

type
  TfmAddPosOut = class(TfmCustomChildForm)
    Panel1: TPanel;
    Button1: TButton;
    Button2: TButton;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    cxTextEdit1: TcxTextEdit;
    cxTextEdit2: TcxTextEdit;
    cxTextEdit3: TcxTextEdit;
    cxCalcEdit1: TcxCalcEdit;
    Label5: TLabel;
    quSelCard: TFDQuery;
    quSelCardKAP: TStringField;
    quSelCardNAME: TStringField;
    quSelCardVOL: TSingleField;
    quSelCardKREP: TSingleField;
    quSelCardAVID: TIntegerField;
    quSelCardPRODID: TStringField;
    quSelCardIMPORTERID: TStringField;
    Label6: TLabel;
    cxCurrencyEdit1: TcxCurrencyEdit;
    procedure cxTextEdit1Exit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAddPosOut: TfmAddPosOut;

implementation

uses dmRar;

{$R *.dfm}

procedure TfmAddPosOut.cxTextEdit1Exit(Sender: TObject);
begin
  //
    quSelCard.Active:=False;
    quSelCard.ParamByName('CODE').AsString:=cxTextEdit1.Text;
    quSelCard.Active:=True;

    if quSelCard.RecordCount=1 then Label5.Caption:=quSelCardNAME.AsString+'   '+quSelCardVOL.AsString+'   '+quSelCardKREP.AsString+'   '+quSelCardAVID.AsString
    else Label5.Caption:='';

    quSelCard.Active:=False;
end;

end.
