object fmDocSpecDecl: TfmDocSpecDecl
  Left = 0
  Top = 0
  Caption = #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1103' '#1076#1086#1082#1091#1084#1077#1085#1090#1072' '#1072#1083#1082#1086#1075#1086#1083#1100#1085#1086#1081' '#1076#1077#1082#1083#1072#1088#1072#1094#1080#1080
  ClientHeight = 716
  ClientWidth = 1432
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1432
    Height = 127
    BarManager = dxBarManager1
    Style = rs2010
    ColorSchemeAccent = rcsaOrange
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1103' '#1076#1077#1082#1083#1072#1088#1072#1094#1080#1080
      Groups = <
        item
          Caption = 
            '         '#1059#1087#1088#1072#1074#1083#1077#1085#1080#1077'                                             ' +
            '         '
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end>
      Index = 0
    end
  end
  object GridSpecDecl: TcxGrid
    Left = 0
    Top = 127
    Width = 1432
    Height = 589
    Align = alClient
    TabOrder = 1
    LookAndFeel.Kind = lfOffice11
    LookAndFeel.NativeStyle = False
    RootLevelOptions.DetailTabsPosition = dtpTop
    ExplicitHeight = 585
    object ViewSpecDecl: TcxGridDBTableView
      PopupMenu = PopupMenu1
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.InfoPanel.Visible = True
      Navigator.Visible = True
      DataController.DataSource = dsquSpecOb
      DataController.Summary.DefaultGroupSummaryItems = <
        item
          Format = '0.0000'
          Kind = skSum
          Position = spFooter
          FieldName = 'QIN1'
          Column = ViewSpecDeclQIN1
        end
        item
          Format = '0.0000'
          Kind = skSum
          Position = spFooter
          FieldName = 'QIN2'
          Column = ViewSpecDeclQIN2
        end
        item
          Format = '0.0000'
          Kind = skSum
          Position = spFooter
          FieldName = 'QIN3'
          Column = ViewSpecDeclQIN3
        end
        item
          Format = '0.0000'
          Kind = skSum
          Position = spFooter
          FieldName = 'QIN4'
          Column = ViewSpecDeclQIN4
        end
        item
          Format = '0.0000'
          Kind = skSum
          Position = spFooter
          FieldName = 'QIN5'
          Column = ViewSpecDeclQIN5
        end
        item
          Format = '0.0000'
          Kind = skSum
          Position = spFooter
          FieldName = 'QIN6'
          Column = ViewSpecDeclQIN6
        end
        item
          Format = '0.0000'
          Kind = skSum
          Position = spFooter
          FieldName = 'QOUT1'
          Column = ViewSpecDeclQOUT1
        end
        item
          Format = '0.0000'
          Kind = skSum
          Position = spFooter
          FieldName = 'QOUT2'
          Column = ViewSpecDeclQOUT2
        end
        item
          Format = '0.0000'
          Kind = skSum
          Position = spFooter
          FieldName = 'QOUT3'
          Column = ViewSpecDeclQOUT3
        end
        item
          Format = '0.0000'
          Kind = skSum
          Position = spFooter
          FieldName = 'QOUT4'
          Column = ViewSpecDeclQOUT4
        end
        item
          Format = '0.0000'
          Kind = skSum
          Position = spFooter
          FieldName = 'QUANT_IN'
          Column = ViewSpecDeclQUANT_IN
        end
        item
          Format = '0.0000'
          Kind = skSum
          Position = spFooter
          FieldName = 'QUANT_IN_ITOG'
          Column = ViewSpecDeclQUANT_IN_ITOG
        end
        item
          Format = '0.0000'
          Kind = skSum
          Position = spFooter
          FieldName = 'QUANT_OUT_ITOG'
          Column = ViewSpecDeclQUANT_OUT_ITOG
        end
        item
          Format = '0.0000'
          Kind = skSum
          Position = spFooter
          FieldName = 'REMNB'
          Column = ViewSpecDeclREMNB
        end
        item
          Format = '0.0000'
          Kind = skSum
          Position = spFooter
          FieldName = 'REMNE'
          Column = ViewSpecDeclREMNE
        end>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0.0000'
          Kind = skSum
          FieldName = 'QIN1'
          Column = ViewSpecDeclQIN1
        end
        item
          Format = '0.0000'
          Kind = skSum
          FieldName = 'QIN2'
          Column = ViewSpecDeclQIN2
        end
        item
          Format = '0.0000'
          Kind = skSum
          FieldName = 'QIN3'
          Column = ViewSpecDeclQIN3
        end
        item
          Format = '0.0000'
          Kind = skSum
          FieldName = 'QIN4'
          Column = ViewSpecDeclQIN4
        end
        item
          Format = '0.0000'
          Kind = skSum
          FieldName = 'QIN5'
          Column = ViewSpecDeclQIN5
        end
        item
          Format = '0.0000'
          Kind = skSum
          FieldName = 'QIN6'
          Column = ViewSpecDeclQIN6
        end
        item
          Format = '0.0000'
          Kind = skSum
          FieldName = 'QOUT1'
          Column = ViewSpecDeclQOUT1
        end
        item
          Format = '0.0000'
          Kind = skSum
          FieldName = 'QOUT2'
          Column = ViewSpecDeclQOUT2
        end
        item
          Format = '0.0000'
          Kind = skSum
          FieldName = 'QIN3'
          Column = ViewSpecDeclQOUT3
        end
        item
          Format = '0.0000'
          Kind = skSum
          FieldName = 'QOUT4'
          Column = ViewSpecDeclQOUT4
        end
        item
          Format = '0.0000'
          Kind = skSum
          FieldName = 'QUANT_IN'
          Column = ViewSpecDeclQUANT_IN
        end
        item
          Format = '0.0000'
          Kind = skSum
          FieldName = 'QUANT_IN_ITOG'
          Column = ViewSpecDeclQUANT_IN_ITOG
        end
        item
          Format = '0.0000'
          Kind = skSum
          FieldName = 'QUANT_OUT_ITOG'
          Column = ViewSpecDeclQUANT_OUT_ITOG
        end
        item
          Format = '0.0000'
          Kind = skSum
          FieldName = 'REMNB'
          Column = ViewSpecDeclREMNB
        end
        item
          Format = '0.0000'
          Kind = skSum
          FieldName = 'REMNE'
          Column = ViewSpecDeclREMNE
        end>
      DataController.Summary.SummaryGroups = <>
      Images = dmR.SmallImage
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Inserting = False
      OptionsSelection.MultiSelect = True
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      OptionsView.HeaderHeight = 60
      object ViewSpecDeclIDORG: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1080
        DataBinding.FieldName = 'IDORG'
        Options.Editing = False
      end
      object ViewSpecDeclNAMEORG: TcxGridDBColumn
        Caption = #1054#1088#1075#1072#1085#1080#1079#1072#1094#1080#1103
        DataBinding.FieldName = 'NAMEORG'
        Options.Editing = False
        Width = 206
      end
      object ViewSpecDeclAVID: TcxGridDBColumn
        Caption = #1042#1080#1076' '#1087#1088#1086#1076#1091#1082#1094#1080#1080
        DataBinding.FieldName = 'AVID'
        Options.Editing = False
        Width = 47
      end
      object ViewSpecDeclNAMEV: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077' '#1074#1080#1076#1072' '#1087#1088#1086#1076#1091#1082#1094#1080#1080
        DataBinding.FieldName = 'NAMEV'
        Options.Editing = False
        Width = 140
      end
      object ViewSpecDeclNAME: TcxGridDBColumn
        Caption = #1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1100
        DataBinding.FieldName = 'NAME'
        Options.Editing = False
        Width = 236
      end
      object ViewSpecDeclPRODID: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1087#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1103
        DataBinding.FieldName = 'PRODID'
        Visible = False
        Options.Editing = False
      end
      object ViewSpecDeclPRODINN: TcxGridDBColumn
        Caption = #1048#1053#1053' '#1087#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1103
        DataBinding.FieldName = 'PRODINN'
        Options.Editing = False
      end
      object ViewSpecDeclPRODKPP: TcxGridDBColumn
        Caption = #1050#1055#1055' '#1087#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1103
        DataBinding.FieldName = 'PRODKPP'
        Options.Editing = False
      end
      object ViewSpecDeclREMNB: TcxGridDBColumn
        Caption = #1054#1089#1090#1072#1090#1086#1082' '#1085#1072' '#1085#1072#1095#1072#1083#1086
        DataBinding.FieldName = 'REMNB'
        Width = 100
      end
      object ViewSpecDeclQIN1: TcxGridDBColumn
        Caption = #1055#1088#1080#1093#1086#1076' '#1086#1090' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1081' '#1087#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1077#1081
        DataBinding.FieldName = 'QIN1'
        Width = 100
      end
      object ViewSpecDeclQIN2: TcxGridDBColumn
        Caption = #1055#1088#1080#1093#1086#1076' '#1086#1090' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1081' '#1086#1087#1090#1086#1074#1086#1081' '#1090#1086#1088#1075#1086#1074#1083#1080
        DataBinding.FieldName = 'QIN2'
        Width = 100
      end
      object ViewSpecDeclQIN3: TcxGridDBColumn
        Caption = #1055#1088#1080#1093#1086#1076' '#1087#1086' '#1080#1084#1087#1086#1088#1090#1091
        DataBinding.FieldName = 'QIN3'
        Width = 100
      end
      object ViewSpecDeclQUANT_IN: TcxGridDBColumn
        Caption = #1047#1072#1082#1091#1087#1082#1080' '#1080#1090#1086#1075#1086
        DataBinding.FieldName = 'QUANT_IN'
        Options.Editing = False
        Width = 100
      end
      object ViewSpecDeclQIN4: TcxGridDBColumn
        Caption = #1042#1086#1079#1074#1088#1072#1090' '#1086#1090' '#1087#1086#1082#1091#1087#1072#1090#1077#1083#1103
        DataBinding.FieldName = 'QIN4'
        Width = 100
      end
      object ViewSpecDeclQIN5: TcxGridDBColumn
        Caption = #1055#1088#1086#1095#1077#1077' '#1087#1086#1089#1090#1091#1087#1083#1077#1085#1080#1077
        DataBinding.FieldName = 'QIN5'
        Width = 100
      end
      object ViewSpecDeclQIN6: TcxGridDBColumn
        Caption = #1055#1077#1088#1077#1084#1077#1097#1077#1085#1080#1077' '#1074#1085#1091#1090#1088#1080' '#1086#1076#1085#1086#1081' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1080
        DataBinding.FieldName = 'QIN6'
        Width = 100
      end
      object ViewSpecDeclQUANT_IN_ITOG: TcxGridDBColumn
        Caption = #1055#1088#1080#1093#1086#1076' '#1048#1058#1054#1043#1054
        DataBinding.FieldName = 'QUANT_IN_ITOG'
        Options.Editing = False
        Width = 100
      end
      object ViewSpecDeclQOUT1: TcxGridDBColumn
        Caption = #1056#1072#1089#1093#1086#1076' '#1086#1073#1098#1077#1084' '#1088#1086#1079#1085#1080#1095#1085#1086#1081' '#1087#1088#1086#1076#1072#1078#1080
        DataBinding.FieldName = 'QOUT1'
        Width = 100
      end
      object ViewSpecDeclQOUT2: TcxGridDBColumn
        Caption = #1055#1088#1086#1095#1080#1081' '#1088#1072#1089#1093#1086#1076
        DataBinding.FieldName = 'QOUT2'
        Width = 100
      end
      object ViewSpecDeclQOUT3: TcxGridDBColumn
        Caption = #1042#1086#1079#1074#1088#1072#1090' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1091
        DataBinding.FieldName = 'QOUT3'
        Width = 100
      end
      object ViewSpecDeclQOUT4: TcxGridDBColumn
        Caption = #1056#1072#1089#1093#1086#1076' '#1087#1077#1088#1077#1084#1077#1097#1077#1085#1080#1077' '#1074#1085#1091#1090#1088#1080' '#1086#1076#1085#1086#1081' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1080
        DataBinding.FieldName = 'QOUT4'
        Width = 100
      end
      object ViewSpecDeclQUANT_OUT_ITOG: TcxGridDBColumn
        Caption = #1056#1072#1089#1093#1086#1076' '#1048#1058#1054#1043#1054
        DataBinding.FieldName = 'QUANT_OUT_ITOG'
        Options.Editing = False
        Width = 100
      end
      object ViewSpecDeclREMNE: TcxGridDBColumn
        Caption = #1054#1089#1090#1072#1090#1086#1082' '#1085#1072' '#1082#1086#1085#1077#1094
        DataBinding.FieldName = 'REMNE'
        Options.Editing = False
        Width = 100
      end
    end
    object ViewSpecDecl1: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = dsquSpecIn
      DataController.Summary.DefaultGroupSummaryItems = <
        item
          Format = '0.0000'
          Kind = skSum
          Position = spFooter
          FieldName = 'QIN'
          Column = ViewSpecDecl1QIN
        end>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0.0000'
          Kind = skSum
          FieldName = 'QIN'
          Column = ViewSpecDecl1QIN
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Inserting = False
      OptionsView.Footer = True
      OptionsView.GroupFooters = gfAlwaysVisible
      OptionsView.HeaderHeight = 60
      object ViewSpecDecl1IDORG: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1080
        DataBinding.FieldName = 'IDORG'
        Options.Editing = False
        Width = 56
      end
      object ViewSpecDecl1NAMEORG: TcxGridDBColumn
        Caption = #1054#1088#1075#1072#1085#1080#1079#1072#1094#1080#1103
        DataBinding.FieldName = 'NAMEORG'
        Options.Editing = False
        Width = 206
      end
      object ViewSpecDecl1AVID: TcxGridDBColumn
        Caption = #1042#1080#1076' '#1087#1088#1086#1076#1091#1082#1094#1080#1080
        DataBinding.FieldName = 'AVID'
        Options.Editing = False
        Width = 67
      end
      object ViewSpecDecl1NAMEV: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077' '#1074#1080#1076#1072' '#1087#1088#1086#1076#1091#1082#1094#1080#1080
        DataBinding.FieldName = 'NAMEV'
        Options.Editing = False
        Width = 185
      end
      object ViewSpecDecl1PRODID: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1087#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1103
        DataBinding.FieldName = 'PRODID'
        Visible = False
        Options.Editing = False
      end
      object ViewSpecDecl1NAMEP: TcxGridDBColumn
        Caption = #1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1100
        DataBinding.FieldName = 'NAMEP'
        Options.Editing = False
      end
      object ViewSpecDecl1PRODINN: TcxGridDBColumn
        Caption = #1048#1053#1053' '#1087#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1103
        DataBinding.FieldName = 'PRODINN'
        Options.Editing = False
      end
      object ViewSpecDecl1PRODKPP: TcxGridDBColumn
        Caption = #1050#1055#1055' '#1087#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1103
        DataBinding.FieldName = 'PRODKPP'
        Options.Editing = False
      end
      object ViewSpecDecl1CLIENTID: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
        DataBinding.FieldName = 'CLIENTID'
        Visible = False
        Options.Editing = False
        Width = 105
      end
      object ViewSpecDecl1NAMECLI: TcxGridDBColumn
        Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
        DataBinding.FieldName = 'NAMECLI'
        Options.Editing = False
        Width = 257
      end
      object ViewSpecDecl1CLIENTINN: TcxGridDBColumn
        Caption = #1048#1053#1053' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
        DataBinding.FieldName = 'CLIENTINN'
        Options.Editing = False
        Width = 98
      end
      object ViewSpecDecl1CLIENTKPP: TcxGridDBColumn
        Caption = #1050#1055#1055' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
        DataBinding.FieldName = 'CLIENTKPP'
        Options.Editing = False
        Width = 100
      end
      object ViewSpecDecl1LICCODE: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1083#1080#1094#1077#1085#1079#1080#1080
        DataBinding.FieldName = 'LICCODE'
        Options.Editing = False
        Width = 64
      end
      object ViewSpecDecl1LICSERNUM: TcxGridDBColumn
        Caption = #1051#1080#1094#1077#1085#1079#1080#1103' '#1089#1077#1088'. '#1085#1086#1084#1077#1088
        DataBinding.FieldName = 'LICSERNUM'
        Options.Editing = False
        Width = 81
      end
      object ViewSpecDecl1LICDATEB: TcxGridDBColumn
        Caption = #1044#1072#1090#1072' '#1074#1099#1076#1072#1095#1080
        DataBinding.FieldName = 'LICDATEB'
        Options.Editing = False
        Width = 73
      end
      object ViewSpecDecl1LICDATEE: TcxGridDBColumn
        Caption = #1044#1072#1090#1072' '#1086#1082#1086#1085#1095#1072#1085#1080#1103
        DataBinding.FieldName = 'LICDATEE'
        Options.Editing = False
        Width = 72
      end
      object ViewSpecDecl1LICORGAN: TcxGridDBColumn
        Caption = #1050#1077#1084' '#1074#1099#1076#1072#1085#1072' '#1083#1080#1094#1077#1085#1079#1080#1103
        DataBinding.FieldName = 'LICORGAN'
        Options.Editing = False
        Width = 141
      end
      object ViewSpecDecl1DOCDATE: TcxGridDBColumn
        Caption = #1044#1072#1090#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
        DataBinding.FieldName = 'DOCDATE'
        Options.Editing = False
        Width = 95
      end
      object ViewSpecDecl1DOCNUM: TcxGridDBColumn
        Caption = #1053#1086#1084#1077#1088' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
        DataBinding.FieldName = 'DOCNUM'
        Options.Editing = False
        Width = 99
      end
      object ViewSpecDecl1GTD: TcxGridDBColumn
        Caption = #1043#1058#1044
        DataBinding.FieldName = 'GTD'
        Options.Editing = False
        Width = 90
      end
      object ViewSpecDecl1QIN: TcxGridDBColumn
        Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086
        DataBinding.FieldName = 'QIN'
        Width = 74
      end
    end
    object LevelSpecDecl: TcxGridLevel
      Caption = #1054#1073#1086#1088#1086#1090#1099
      GridView = ViewSpecDecl
    end
    object LevelSpecDecl1: TcxGridLevel
      Caption = #1055#1088#1080#1093#1086#1076
      GridView = ViewSpecDecl1
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      #1044#1077#1081#1089#1090#1074#1080#1103)
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmR.SmallImage
    ImageOptions.LargeImages = dmR.LargeImage
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 540
    Top = 164
    DockControlHeights = (
      0
      0
      0
      0)
    object dxBarManager1Bar1: TdxBar
      Caption = #1059#1087#1088#1072#1074#1083#1077#1085#1080#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 1001
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          BeginGroup = True
          UserDefine = [udWidth]
          UserWidth = 196
          Visible = True
          ItemName = 'LookupComboBox1'
        end
        item
          UserDefine = [udWidth]
          UserWidth = 226
          Visible = True
          ItemName = 'Combo1'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'DateEdit1'
        end
        item
          Visible = True
          ItemName = 'DateEdit2'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'Combo2'
        end
        item
          UserDefine = [udWidth]
          UserWidth = 63
          Visible = True
          ItemName = 'SpinEdit1'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1047#1072#1082#1088#1099#1090#1100
      CaptionButtons = <>
      DockedLeft = 739
      DockedTop = 0
      FloatLeft = 1161
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = acSave
      Category = 0
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Caption = 
        '                                                                ' +
        '                '
      Category = 0
      Hint = 
        '                                                                ' +
        '                '
      Visible = ivAlways
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = acClose
      Category = 0
    end
    object LookupComboBox1: TcxBarEditItem
      Caption = #1054#1088#1075#1072#1085#1080#1079#1072#1094#1080#1103
      Category = 0
      Hint = #1054#1088#1075#1072#1085#1080#1079#1072#1094#1080#1103
      Visible = ivAlways
      PropertiesClassName = 'TcxLookupComboBoxProperties'
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Caption = #1053#1072#1079#1074#1072#1085#1080#1077
          FieldName = 'NAME'
        end
        item
          Caption = #1050#1086#1076
          FieldName = 'ID'
        end>
      Properties.ListSource = dsquOrg
      Properties.MaxLength = 150
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Caption = '                        '
      Category = 0
      Hint = '                        '
      Visible = ivAlways
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Caption = #1057#1092#1086#1088#1084#1080#1088#1086#1074#1072#1090#1100
      Category = 0
      Hint = #1057#1092#1086#1088#1084#1080#1088#1086#1074#1072#1090#1100
      Visible = ivAlways
      LargeImageIndex = 85
    end
    object SpinEdit1: TdxBarSpinEdit
      Caption = #8470' '#1082#1086#1088#1088#1077#1082#1094#1080#1080
      Category = 0
      Hint = #8470' '#1082#1086#1088#1088#1077#1082#1094#1080#1080
      Visible = ivAlways
    end
    object Combo2: TdxBarCombo
      Caption = #1057#1090#1072#1090#1091#1089
      Category = 0
      Hint = #1057#1090#1072#1090#1091#1089
      Visible = ivAlways
      Text = #1055#1077#1088#1074#1080#1095#1085#1072#1103
      Items.Strings = (
        #1055#1077#1088#1074#1080#1095#1085#1072#1103
        #1050#1086#1088#1088#1077#1082#1090#1080#1088#1091#1102#1097#1072#1103)
      ItemIndex = 0
    end
    object DateEdit1: TdxBarDateCombo
      Caption = #1057'    '
      Category = 0
      Hint = #1057'    '
      Visible = ivAlways
      ImageIndex = 25
      ShowDayText = False
    end
    object DateEdit2: TdxBarDateCombo
      Caption = #1087#1086'  '
      Category = 0
      Hint = #1087#1086'  '
      Visible = ivAlways
      ImageIndex = 25
      ShowDayText = False
    end
    object Combo1: TdxBarCombo
      Caption = #1058#1080#1087' '
      Category = 0
      Hint = #1058#1080#1087' '
      Visible = ivAlways
      Text = #1055#1086' '#1072#1083#1082#1086#1075#1086#1083#1102
      Items.Strings = (
        #1055#1086' '#1072#1083#1082#1086#1075#1086#1083#1102
        #1055#1086' '#1087#1080#1074#1091)
      ItemIndex = 0
    end
    object dxBarGroup1: TdxBarGroup
      Items = ()
    end
  end
  object ActionList1: TActionList
    Images = dmR.SmallImage
    Left = 648
    Top = 168
    object acClose: TAction
      Caption = #1047#1072#1082#1088#1099#1090#1100
      ImageIndex = 100
      OnExecute = acCloseExecute
    end
    object acSave: TAction
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' (Ctrl + S)'
      ImageIndex = 7
      ShortCut = 16467
      OnExecute = acSaveExecute
    end
    object acTestSpec: TAction
      Caption = #1055#1088#1086#1074#1077#1088#1082#1072' '#1089#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1080
      ImageIndex = 38
    end
    object acDelPos: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1087#1086#1079#1080#1094#1080#1102
      ImageIndex = 4
      OnExecute = acDelPosExecute
    end
    object acGetInfoSel: TAction
      Caption = #1047#1072#1087#1088#1086#1089#1080#1090#1100' '#1080#1085#1092#1086#1088#1084#1072#1094#1080#1102' '#1087#1086' '#1074#1099#1076#1077#1083#1077#1085#1085#1099#1084
      ImageIndex = 50
    end
    object acAddPos: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1087#1086#1079#1080#1094#1080#1102
      ImageIndex = 3
      OnExecute = acAddPosExecute
    end
    object acInputRemn: TAction
      Caption = #1055#1088#1080#1085#1103#1090#1100' '#1086#1089#1090#1072#1090#1082#1080
      ImageIndex = 76
      OnExecute = acInputRemnExecute
    end
  end
  object quSpecOb: TFDQuery
    BeforePost = quSpecObBeforePost
    OnCalcFields = quSpecObCalcFields
    CachedUpdates = True
    Connection = dmR.FDConnection
    Transaction = dmR.FDTrans
    UpdateTransaction = dmR.FDTransUpdate
    UpdateObject = UpdSQL1
    SQL.Strings = (
      'SELECT sp1.[IDHEAD]'
      '      ,sp1.[ID]'
      '      ,sp1.[IDORG]'
      #9'  ,org.NAME as NAMEORG'
      '      ,sp1.[AVID]'
      #9'  ,avid.[NAMEV]'
      '      ,sp1.[PRODID]'
      #9'  ,pr.NAME'
      #9'  ,pr.PRODINN'
      #9'  ,pr.PRODKPP'
      '      ,sp1.[REMNB] as REMNB'
      '      ,sp1.[QIN1] as QIN1'
      '      ,sp1.[QIN2] as QIN2'
      '      ,sp1.[QIN3] as QIN3'
      
        '--      ,(isnull(sp1.[QIN1],0)+isnull(sp1.[QIN2],0)+isnull(sp1.[' +
        'QIN3],0)) as QUANT_IN'
      '      ,sp1.[QIN4] as QIN4'
      '      ,sp1.[QIN5] as QIN5'
      '      ,sp1.[QIN6] as QIN6'
      
        '--      ,(isnull(sp1.[QIN1],0)+isnull(sp1.[QIN2],0)+isnull(sp1.[' +
        'QIN3],0)+isnull(sp1.[QIN4],0)+isnull(sp1.[QIN5],0)+isnull(sp1.[Q' +
        'IN6],0)) as QUANT_IN_ITOG'
      '      ,sp1.[QOUT1] as QOUT1'
      '      ,sp1.[QOUT2] as QOUT2'
      '      ,sp1.[QOUT3] as QOUT3'
      '      ,sp1.[QOUT4] as QOUT4'
      
        '--      ,(isnull(sp1.[QOUT1],0)+isnull(sp1.[QOUT2],0)+isnull(sp1' +
        '.[QOUT3],0)+isnull(sp1.[QOUT4],0)) as QUANT_OUT_ITOG'
      '      ,sp1.[REMNE] as REMNE'
      
        '--      ,(isnull(sp1.[QIN1],0)+isnull(sp1.[QIN2],0)+isnull(sp1.[' +
        'QIN3],0)+isnull(sp1.[QIN4],0)+isnull(sp1.[QIN5],0)+isnull(sp1.[Q' +
        'IN6],0))-((isnull(sp1.[QOUT1],0)+isnull(sp1.[QOUT2],0)+isnull(sp' +
        '1.[QOUT3],0)+isnull(sp1.[QOUT4],0))) as REMNE'
      '  FROM [dbo].[ADECL_SP1] sp1'
      '  left join [dbo].[AVID] avid on avid.ID=sp1.[AVID]'
      '  left join [dbo].[APRODS] pr on pr.PRODID=sp1.[PRODID]'
      '  left join [dbo].[DECLORG] org on org.[ID]=sp1.IDORG'
      '  where [IDHEAD]=:IDH'
      'order by sp1.[IDORG],sp1.[AVID],pr.NAME')
    Left = 80
    Top = 344
    ParamData = <
      item
        Name = 'IDH'
        DataType = ftInteger
        ParamType = ptInput
        Value = 4
      end>
    object quSpecObIDHEAD: TLargeintField
      FieldName = 'IDHEAD'
      Origin = 'IDHEAD'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quSpecObID: TLargeintField
      AutoGenerateValue = arAutoInc
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object quSpecObAVID: TIntegerField
      FieldName = 'AVID'
      Origin = 'AVID'
    end
    object quSpecObNAMEV: TStringField
      FieldName = 'NAMEV'
      Origin = 'NAMEV'
      Size = 200
    end
    object quSpecObPRODID: TStringField
      FieldName = 'PRODID'
      Origin = 'PRODID'
      Size = 50
    end
    object quSpecObNAME: TMemoField
      FieldName = 'NAME'
      Origin = 'NAME'
      BlobType = ftMemo
      Size = 2147483647
    end
    object quSpecObPRODINN: TStringField
      FieldName = 'PRODINN'
      Origin = 'PRODINN'
    end
    object quSpecObPRODKPP: TStringField
      FieldName = 'PRODKPP'
      Origin = 'PRODKPP'
    end
    object quSpecObREMNB: TSingleField
      FieldName = 'REMNB'
      Origin = 'REMNB'
      DisplayFormat = '0.0000'
    end
    object quSpecObQIN1: TSingleField
      FieldName = 'QIN1'
      Origin = 'QIN1'
      DisplayFormat = '0.0000'
    end
    object quSpecObQIN2: TSingleField
      FieldName = 'QIN2'
      Origin = 'QIN2'
      DisplayFormat = '0.0000'
    end
    object quSpecObQIN3: TSingleField
      FieldName = 'QIN3'
      Origin = 'QIN3'
      DisplayFormat = '0.0000'
    end
    object quSpecObQIN4: TSingleField
      FieldName = 'QIN4'
      Origin = 'QIN4'
      DisplayFormat = '0.0000'
    end
    object quSpecObQIN5: TSingleField
      FieldName = 'QIN5'
      Origin = 'QIN5'
      DisplayFormat = '0.0000'
    end
    object quSpecObQIN6: TSingleField
      FieldName = 'QIN6'
      Origin = 'QIN6'
      DisplayFormat = '0.0000'
    end
    object quSpecObQOUT1: TSingleField
      FieldName = 'QOUT1'
      Origin = 'QOUT1'
      DisplayFormat = '0.0000'
    end
    object quSpecObQOUT2: TSingleField
      FieldName = 'QOUT2'
      Origin = 'QOUT2'
      DisplayFormat = '0.0000'
    end
    object quSpecObQOUT3: TSingleField
      FieldName = 'QOUT3'
      Origin = 'QOUT3'
      DisplayFormat = '0.0000'
    end
    object quSpecObQOUT4: TSingleField
      FieldName = 'QOUT4'
      Origin = 'QOUT4'
      DisplayFormat = '0.0000'
    end
    object quSpecObREMNE: TSingleField
      FieldName = 'REMNE'
      Origin = 'REMNE'
      DisplayFormat = '0.0000'
    end
    object quSpecObQUANT_IN: TFloatField
      FieldKind = fkCalculated
      FieldName = 'QUANT_IN'
      DisplayFormat = '0.0000'
      Calculated = True
    end
    object quSpecObQUANT_IN_ITOG: TFloatField
      FieldKind = fkCalculated
      FieldName = 'QUANT_IN_ITOG'
      DisplayFormat = '0.0000'
      Calculated = True
    end
    object quSpecObQUANT_OUT_ITOG: TFloatField
      FieldKind = fkCalculated
      FieldName = 'QUANT_OUT_ITOG'
      DisplayFormat = '0.0000'
      Calculated = True
    end
    object quSpecObIDORG: TIntegerField
      FieldName = 'IDORG'
      Origin = 'IDORG'
    end
    object quSpecObNAMEORG: TStringField
      FieldName = 'NAMEORG'
      Origin = 'NAMEORG'
      Size = 200
    end
  end
  object UpdSQL1: TFDUpdateSQL
    Connection = dmR.FDConnection
    InsertSQL.Strings = (
      'INSERT INTO RAR.dbo.ADECL_SP1'
      '(IDHEAD, AVID, PRODID, REMNB, QIN1, '
      '  QIN2, QIN3, QIN4, QIN5, QIN6, QOUT1, '
      '  QOUT2, QOUT3, QOUT4, REMNE)'
      
        'VALUES (:NEW_IDHEAD, :NEW_AVID, :NEW_PRODID, :NEW_REMNB, :NEW_QI' +
        'N1, '
      
        '  :NEW_QIN2, :NEW_QIN3, :NEW_QIN4, :NEW_QIN5, :NEW_QIN6, :NEW_QO' +
        'UT1, '
      '  :NEW_QOUT2, :NEW_QOUT3, :NEW_QOUT4, :NEW_REMNE);'
      'SELECT SCOPE_IDENTITY() AS ID')
    ModifySQL.Strings = (
      'UPDATE RAR.dbo.ADECL_SP1'
      
        'SET IDHEAD = :NEW_IDHEAD, AVID = :NEW_AVID, PRODID = :NEW_PRODID' +
        ', '
      '  REMNB = :NEW_REMNB, QIN1 = :NEW_QIN1, QIN2 = :NEW_QIN2, '
      
        '  QIN3 = :NEW_QIN3, QIN4 = :NEW_QIN4, QIN5 = :NEW_QIN5, QIN6 = :' +
        'NEW_QIN6, '
      '  QOUT1 = :NEW_QOUT1, QOUT2 = :NEW_QOUT2, QOUT3 = :NEW_QOUT3, '
      '  QOUT4 = :NEW_QOUT4, REMNE = :NEW_REMNE'
      'WHERE IDHEAD = :OLD_IDHEAD AND ID = :OLD_ID;'
      'SELECT ID'
      'FROM RAR.dbo.ADECL_SP1'
      'WHERE IDHEAD = :NEW_IDHEAD AND ID = :NEW_ID')
    DeleteSQL.Strings = (
      'DELETE FROM RAR.dbo.ADECL_SP1'
      'WHERE IDHEAD = :OLD_IDHEAD AND ID = :OLD_ID')
    FetchRowSQL.Strings = (
      
        'SELECT IDHEAD, SCOPE_IDENTITY() AS ID, AVID, PRODID, REMNB, QIN1' +
        ', QIN2, '
      '  QIN3, QIN4, QIN5, QIN6, QOUT1, QOUT2, QOUT3, QOUT4, REMNE'
      'FROM RAR.dbo.ADECL_SP1'
      'WHERE IDHEAD = :IDHEAD AND ID = :ID')
    Left = 152
    Top = 344
  end
  object dsquSpecOb: TDataSource
    DataSet = quSpecOb
    Left = 80
    Top = 400
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 224
    Top = 256
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svFont]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
    end
    object cxStyle2: TcxStyle
    end
  end
  object PopupMenu1: TPopupMenu
    Images = dmR.SmallImage
    Left = 608
    Top = 272
    object N1: TMenuItem
      Action = acInputRemn
    end
  end
  object quE: TFDQuery
    Connection = dmR.FDConnection
    Left = 80
    Top = 280
  end
  object quS: TFDQuery
    Connection = dmR.FDConnection
    Left = 160
    Top = 280
  end
  object quOrg: TFDQuery
    Connection = dmR.FDConnection
    SQL.Strings = (
      'SELECT [ID]'
      '      ,[MAIN]'
      '      ,[FSRARID]'
      '      ,[NAME]'
      '      ,[PHONE]'
      '      ,[EMAILORG]'
      '      ,[INN]'
      '      ,[KPP]'
      '      ,[DIR1]'
      '      ,[DIR2]'
      '      ,[DIR3]'
      '      ,[GB1]'
      '      ,[GB2]'
      '      ,[GB3]'
      '      ,[ADDR_CC]'
      '      ,[ADDR_IND]'
      '      ,[ADDR_REG]'
      '      ,[ADDR_RN]'
      '      ,[ADDR_CITY]'
      '      ,[ADDR_NP]'
      '      ,[ADDR_STR]'
      '      ,[ADDR_D]'
      '      ,[ADDR_KORP]'
      '      ,[ADDR_L]'
      '      ,[ADDR_KV]'
      '  FROM [RAR].[dbo].[DECLORG]'
      '  where MAIN=1')
    Left = 328
    Top = 280
    object quOrgID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quOrgMAIN: TIntegerField
      FieldName = 'MAIN'
      Origin = 'MAIN'
    end
    object quOrgFSRARID: TStringField
      FieldName = 'FSRARID'
      Origin = 'FSRARID'
      Size = 50
    end
    object quOrgNAME: TStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      Size = 200
    end
    object quOrgPHONE: TStringField
      FieldName = 'PHONE'
      Origin = 'PHONE'
      Size = 50
    end
    object quOrgEMAILORG: TStringField
      FieldName = 'EMAILORG'
      Origin = 'EMAILORG'
      Size = 100
    end
    object quOrgINN: TStringField
      FieldName = 'INN'
      Origin = 'INN'
    end
    object quOrgKPP: TStringField
      FieldName = 'KPP'
      Origin = 'KPP'
    end
    object quOrgDIR1: TStringField
      FieldName = 'DIR1'
      Origin = 'DIR1'
      Size = 100
    end
    object quOrgDIR2: TStringField
      FieldName = 'DIR2'
      Origin = 'DIR2'
      Size = 100
    end
    object quOrgDIR3: TStringField
      FieldName = 'DIR3'
      Origin = 'DIR3'
      Size = 100
    end
    object quOrgGB1: TStringField
      FieldName = 'GB1'
      Origin = 'GB1'
      Size = 100
    end
    object quOrgGB2: TStringField
      FieldName = 'GB2'
      Origin = 'GB2'
      Size = 100
    end
    object quOrgGB3: TStringField
      FieldName = 'GB3'
      Origin = 'GB3'
      Size = 100
    end
    object quOrgADDR_CC: TStringField
      FieldName = 'ADDR_CC'
      Origin = 'ADDR_CC'
      Size = 10
    end
    object quOrgADDR_IND: TStringField
      FieldName = 'ADDR_IND'
      Origin = 'ADDR_IND'
      Size = 10
    end
    object quOrgADDR_REG: TStringField
      FieldName = 'ADDR_REG'
      Origin = 'ADDR_REG'
      Size = 5
    end
    object quOrgADDR_RN: TStringField
      FieldName = 'ADDR_RN'
      Origin = 'ADDR_RN'
      Size = 50
    end
    object quOrgADDR_CITY: TStringField
      FieldName = 'ADDR_CITY'
      Origin = 'ADDR_CITY'
      Size = 50
    end
    object quOrgADDR_NP: TStringField
      FieldName = 'ADDR_NP'
      Origin = 'ADDR_NP'
      Size = 50
    end
    object quOrgADDR_STR: TStringField
      FieldName = 'ADDR_STR'
      Origin = 'ADDR_STR'
      Size = 100
    end
    object quOrgADDR_D: TStringField
      FieldName = 'ADDR_D'
      Origin = 'ADDR_D'
      Size = 10
    end
    object quOrgADDR_KORP: TStringField
      FieldName = 'ADDR_KORP'
      Origin = 'ADDR_KORP'
      Size = 10
    end
    object quOrgADDR_L: TStringField
      FieldName = 'ADDR_L'
      Origin = 'ADDR_L'
      Size = 10
    end
    object quOrgADDR_KV: TStringField
      FieldName = 'ADDR_KV'
      Origin = 'ADDR_KV'
      Size = 10
    end
  end
  object dsquOrg: TDataSource
    DataSet = quOrg
    Left = 328
    Top = 336
  end
  object quSpecIn: TFDQuery
    CachedUpdates = True
    Connection = dmR.FDConnection
    Transaction = dmR.FDTrans
    UpdateTransaction = dmR.FDTransUpdate
    UpdateObject = UpdSQL2
    SQL.Strings = (
      'Declare  @IDATEB int = :IDATEB'
      'Declare  @IDATEE int = :IDATEE'
      ''
      'SELECT sp2.[IDHEAD]'
      '      ,sp2.[ID]'
      '      ,sp2.[IDORG]'
      #9'  ,org.NAME as NAMEORG'
      '      ,sp2.[AVID]'
      #9'  ,avid.[NAMEV]'
      '      ,sp2.[PRODID]'
      #9'  ,pr.NAME as NAMEP'
      #9'  ,pr.PRODINN'
      #9'  ,pr.PRODKPP'
      '      ,sp2.[CLIENTID]'
      #9'  ,cli.NAME as NAMECLI'
      #9'  ,cli.CLIENTINN'
      #9'  ,cli.CLIENTKPP'
      
        #9'  ,(Select TOP 1 [IDL] from [dbo].[ALIC] where [IDCLI]=sp2.[CLI' +
        'ENTID] and [IDATEB]<=@IDATEB and [IDATEE]>=@IDATEE) as LICCODE'
      
        #9'  ,(Select TOP 1 [LICNUM] from [dbo].[ALIC] where [IDCLI]=sp2.[' +
        'CLIENTID] and [IDATEB]<=@IDATEB and [IDATEE]>=@IDATEE) as LICSER' +
        'NUM'
      
        #9'  ,([dbo].i2d((Select TOP 1 [IDATEB] from [dbo].[ALIC] where [I' +
        'DCLI]=sp2.[CLIENTID] and [IDATEB]<=@IDATEB and [IDATEE]>=@IDATEE' +
        '))) as LICDATEB'
      
        #9'  ,([dbo].i2d((Select TOP 1 [IDATEE] from [dbo].[ALIC] where [I' +
        'DCLI]=sp2.[CLIENTID] and [IDATEB]<=@IDATEB and [IDATEE]>=@IDATEE' +
        '))) as LICDATEE'
      
        #9'  ,(Select TOP 1 [ORGAN] from [dbo].[ALIC] where [IDCLI]=sp2.[C' +
        'LIENTID] and [IDATEB]<=@IDATEB and [IDATEE]>=@IDATEE) as LICORGA' +
        'N'
      '      ,sp2.[DOCDATE]'
      '      ,sp2.[DOCNUM]'
      '      ,sp2.[GTD]'
      '      ,sp2.[QIN]'
      '  FROM [dbo].[ADECL_SP2] sp2'
      '  left join [dbo].[AVID] avid on avid.ID=sp2.[AVID]'
      '  left join [dbo].[APRODS] pr on pr.PRODID=sp2.[PRODID]'
      '  left join [dbo].[ACLIENTS] cli on cli.CLIENTID=sp2.[CLIENTID]'
      '  left join [dbo].[DECLORG] org on org.[ID]=sp2.IDORG'
      ''
      '  where sp2.[IDHEAD]=:IDH')
    Left = 80
    Top = 464
    ParamData = <
      item
        Name = 'IDATEB'
        DataType = ftInteger
        ParamType = ptInput
        Value = 42644
      end
      item
        Name = 'IDATEE'
        DataType = ftInteger
        ParamType = ptInput
        Value = 42680
      end
      item
        Name = 'IDH'
        DataType = ftInteger
        ParamType = ptInput
        Value = 0
      end>
    object quSpecInIDHEAD: TLargeintField
      FieldName = 'IDHEAD'
      Origin = 'IDHEAD'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quSpecInID: TLargeintField
      AutoGenerateValue = arAutoInc
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object quSpecInAVID: TIntegerField
      FieldName = 'AVID'
      Origin = 'AVID'
    end
    object quSpecInNAMEV: TStringField
      FieldName = 'NAMEV'
      Origin = 'NAMEV'
      Size = 200
    end
    object quSpecInPRODID: TStringField
      FieldName = 'PRODID'
      Origin = 'PRODID'
      Size = 50
    end
    object quSpecInNAMEP: TMemoField
      FieldName = 'NAMEP'
      Origin = 'NAMEP'
      BlobType = ftMemo
      Size = 2147483647
    end
    object quSpecInPRODINN: TStringField
      FieldName = 'PRODINN'
      Origin = 'PRODINN'
    end
    object quSpecInPRODKPP: TStringField
      FieldName = 'PRODKPP'
      Origin = 'PRODKPP'
    end
    object quSpecInCLIENTID: TStringField
      FieldName = 'CLIENTID'
      Origin = 'CLIENTID'
      Size = 50
    end
    object quSpecInNAMECLI: TMemoField
      FieldName = 'NAMECLI'
      Origin = 'NAMECLI'
      BlobType = ftMemo
      Size = 2147483647
    end
    object quSpecInCLIENTINN: TStringField
      FieldName = 'CLIENTINN'
      Origin = 'CLIENTINN'
    end
    object quSpecInCLIENTKPP: TStringField
      FieldName = 'CLIENTKPP'
      Origin = 'CLIENTKPP'
    end
    object quSpecInDOCDATE: TSQLTimeStampField
      FieldName = 'DOCDATE'
      Origin = 'DOCDATE'
    end
    object quSpecInDOCNUM: TStringField
      FieldName = 'DOCNUM'
      Origin = 'DOCNUM'
      Size = 50
    end
    object quSpecInGTD: TStringField
      FieldName = 'GTD'
      Origin = 'GTD'
      Size = 200
    end
    object quSpecInQIN: TSingleField
      FieldName = 'QIN'
      Origin = 'QIN'
      DisplayFormat = '0.0000'
    end
    object quSpecInLICCODE: TIntegerField
      FieldName = 'LICCODE'
      Origin = 'LICCODE'
      ReadOnly = True
    end
    object quSpecInLICSERNUM: TStringField
      FieldName = 'LICSERNUM'
      Origin = 'LICSERNUM'
      ReadOnly = True
    end
    object quSpecInLICDATEB: TSQLTimeStampField
      FieldName = 'LICDATEB'
      Origin = 'LICDATEB'
      ReadOnly = True
    end
    object quSpecInLICDATEE: TSQLTimeStampField
      FieldName = 'LICDATEE'
      Origin = 'LICDATEE'
      ReadOnly = True
    end
    object quSpecInLICORGAN: TStringField
      FieldName = 'LICORGAN'
      Origin = 'LICORGAN'
      ReadOnly = True
      Size = 250
    end
    object quSpecInIDORG: TIntegerField
      FieldName = 'IDORG'
      Origin = 'IDORG'
    end
    object quSpecInNAMEORG: TStringField
      FieldName = 'NAMEORG'
      Origin = 'NAMEORG'
      Size = 200
    end
  end
  object dsquSpecIn: TDataSource
    DataSet = quSpecIn
    Left = 80
    Top = 528
  end
  object UpdSQL2: TFDUpdateSQL
    Connection = dmR.FDConnection
    InsertSQL.Strings = (
      'INSERT INTO RAR.dbo.ADECL_SP2'
      '(IDHEAD, AVID, PRODID, CLIENTID, DOCDATE, '
      '  DOCNUM, GTD, QIN)'
      
        'VALUES (:NEW_IDHEAD, :NEW_AVID, :NEW_PRODID, :NEW_CLIENTID, :NEW' +
        '_DOCDATE, '
      '  :NEW_DOCNUM, :NEW_GTD, :NEW_QIN);'
      'SELECT SCOPE_IDENTITY() AS ID')
    ModifySQL.Strings = (
      'UPDATE RAR.dbo.ADECL_SP2'
      
        'SET IDHEAD = :NEW_IDHEAD, AVID = :NEW_AVID, PRODID = :NEW_PRODID' +
        ', '
      
        '  CLIENTID = :NEW_CLIENTID, DOCDATE = :NEW_DOCDATE, DOCNUM = :NE' +
        'W_DOCNUM, '
      '  GTD = :NEW_GTD, QIN = :NEW_QIN'
      'WHERE IDHEAD = :OLD_IDHEAD AND ID = :OLD_ID;'
      'SELECT ID'
      'FROM RAR.dbo.ADECL_SP2'
      'WHERE IDHEAD = :NEW_IDHEAD AND ID = :NEW_ID')
    DeleteSQL.Strings = (
      'DELETE FROM RAR.dbo.ADECL_SP2'
      'WHERE IDHEAD = :OLD_IDHEAD AND ID = :OLD_ID')
    FetchRowSQL.Strings = (
      
        'SELECT IDHEAD, SCOPE_IDENTITY() AS ID, AVID, PRODID, CLIENTID, D' +
        'OCDATE, '
      '  DOCNUM, GTD, QIN'
      'FROM RAR.dbo.ADECL_SP2'
      'WHERE IDHEAD = :IDHEAD AND ID = :ID')
    Left = 152
    Top = 464
  end
end
