object fmCliLic: TfmCliLic
  Left = 293
  Top = 111
  Caption = #1051#1080#1094#1077#1085#1079#1080#1080' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
  ClientHeight = 493
  ClientWidth = 786
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDesigned
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 786
    Height = 127
    BarManager = dxBarManager1
    Style = rs2010
    ColorSchemeAccent = rcsaOrange
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    ExplicitWidth = 1277
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1051#1080#1094#1077#1085#1079#1080#1080' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
      Groups = <
        item
          Caption = #1044#1077#1081#1089#1090#1074#1080#1103
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end>
      Index = 0
    end
  end
  object GridCliLIc: TcxGrid
    Left = 0
    Top = 127
    Width = 786
    Height = 366
    Align = alClient
    TabOrder = 1
    LookAndFeel.Kind = lfOffice11
    LookAndFeel.NativeStyle = False
    ExplicitTop = 119
    object ViewCliLic: TcxGridDBTableView
      PopupMenu = PopupMenu1
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.InfoPanel.Visible = True
      Navigator.Visible = True
      DataController.DataSource = dsquCliLic
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'QUANT'
        end
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'QUANTF'
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SUMF'
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.MultiSelect = True
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      object ViewCliLicIDL: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1083#1080#1094#1077#1085#1079#1080#1080
        DataBinding.FieldName = 'IDL'
        Visible = False
      end
      object ViewCliLicLICNUM: TcxGridDBColumn
        Caption = #1053#1086#1084#1077#1088' '
        DataBinding.FieldName = 'LICNUM'
        Width = 108
      end
      object ViewCliLicIDATEB: TcxGridDBColumn
        Caption = #1044#1072#1090#1072' '#1074#1099#1076#1072#1095#1080
        DataBinding.FieldName = 'IDATEB'
        PropertiesClassName = 'TcxDateEditProperties'
        Width = 85
      end
      object ViewCliLicIDATEE: TcxGridDBColumn
        Caption = #1044#1072#1090#1072' '#1086#1082#1086#1085#1095#1072#1085#1080#1103
        DataBinding.FieldName = 'IDATEE'
        PropertiesClassName = 'TcxDateEditProperties'
        Width = 96
      end
      object ViewCliLicORGAN: TcxGridDBColumn
        Caption = #1054#1088#1075#1072#1085' '#1074#1099#1076#1072#1074#1096#1080#1081' '#1083#1080#1094#1077#1085#1079#1080#1102
        DataBinding.FieldName = 'ORGAN'
        Width = 433
      end
    end
    object LevelCliLic: TcxGridLevel
      GridView = ViewCliLic
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      #1044#1077#1081#1089#1090#1074#1080#1103)
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmR.SmallImage
    ImageOptions.LargeImages = dmR.LargeImage
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 540
    Top = 164
    DockControlHeights = (
      0
      0
      0
      0)
    object dxBarManager1Bar2: TdxBar
      Caption = #1047#1072#1082#1088#1099#1090#1100
      CaptionButtons = <>
      DockedLeft = 263
      DockedTop = 0
      FloatLeft = 1161
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end>
      OneOnRow = True
      Row = 1
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar1: TdxBar
      Caption = #1044#1077#1081#1089#1090#1074#1080#1103
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 820
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Caption = 
        '                                                                ' +
        '                '
      Category = 0
      Hint = 
        '                                                                ' +
        '                '
      Visible = ivAlways
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = acClose
      Category = 0
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = 'New SubItem'
      Category = 0
      Visible = ivAlways
      ItemLinks = <>
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = acAddPos
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = acDelPos
      Category = 0
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = acAddPos
      Category = 0
    end
    object dxBarButton1: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = acDelPos
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Caption = 
        '                                                                ' +
        '          '
      Category = 0
      Hint = 
        '                                                                ' +
        '          '
      Visible = ivAlways
    end
    object dxBarGroup1: TdxBarGroup
      Items = ()
    end
  end
  object ActionList1: TActionList
    Images = dmR.SmallImage
    Left = 648
    Top = 168
    object acClose: TAction
      Caption = #1047#1072#1082#1088#1099#1090#1100
      ImageIndex = 100
      OnExecute = acCloseExecute
    end
    object acDelPos: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ImageIndex = 4
      OnExecute = acDelPosExecute
    end
    object acAddPos: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ImageIndex = 3
      OnExecute = acAddPosExecute
    end
  end
  object quCliLic: TFDQuery
    Connection = dmR.FDConnection
    Transaction = dmR.FDTrans
    UpdateTransaction = dmR.FDTransUpdate
    SQL.Strings = (
      'declare @IDCLI varchar(50) = :IDCLI'
      ''
      'SELECT [IDCLI],[IDL],[LICNUM],[IDATEB],[IDATEE],[ORGAN]'
      '  FROM [dbo].[ALIC]'
      '  where [IDCLI]=@IDCLI')
    Left = 136
    Top = 280
    ParamData = <
      item
        Name = 'IDCLI'
        DataType = ftString
        ParamType = ptInput
        Value = '1111'
      end>
    object quCliLicIDCLI: TStringField
      FieldName = 'IDCLI'
      Origin = 'IDCLI'
      Required = True
      Size = 50
    end
    object quCliLicIDL: TFDAutoIncField
      FieldName = 'IDL'
      Origin = 'IDL'
      ReadOnly = True
    end
    object quCliLicLICNUM: TStringField
      FieldName = 'LICNUM'
      Origin = 'LICNUM'
    end
    object quCliLicIDATEB: TIntegerField
      FieldName = 'IDATEB'
      Origin = 'IDATEB'
    end
    object quCliLicIDATEE: TIntegerField
      FieldName = 'IDATEE'
      Origin = 'IDATEE'
    end
    object quCliLicORGAN: TStringField
      FieldName = 'ORGAN'
      Origin = 'ORGAN'
      Size = 250
    end
  end
  object dsquCliLic: TDataSource
    DataSet = quCliLic
    Left = 200
    Top = 280
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 232
    Top = 200
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svFont]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
    end
    object cxStyle2: TcxStyle
    end
  end
  object PopupMenu1: TPopupMenu
    Images = dmR.SmallImage
    Left = 432
    Top = 272
    object N4: TMenuItem
      Action = acAddPos
    end
    object N1: TMenuItem
      Action = acDelPos
    end
  end
  object quE: TFDQuery
    Connection = dmR.FDConnection
    Left = 80
    Top = 280
  end
end
