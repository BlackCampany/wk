object fmACard: TfmACard
  Left = 0
  Top = 0
  Caption = #1050#1072#1088#1090#1086#1095#1082#1080' '#1090#1086#1074#1072#1088#1072' '#1045#1043#1040#1048#1057
  ClientHeight = 576
  ClientWidth = 981
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object GridCards: TcxGrid
    Left = 0
    Top = 127
    Width = 981
    Height = 449
    Align = alClient
    TabOrder = 0
    LookAndFeel.Kind = lfOffice11
    LookAndFeel.NativeStyle = False
    object ViewCards: TcxGridDBTableView
      PopupMenu = PopupMenu1
      OnDblClick = ViewCardsDblClick
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.InfoPanel.Visible = True
      Navigator.Visible = True
      DataController.DataSource = dsquCards
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.IncSearch = True
      OptionsCustomize.ColumnMoving = False
      OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.Indicator = True
      object ViewCardsKAP: TcxGridDBColumn
        Caption = #1050#1040#1055
        DataBinding.FieldName = 'KAP'
        Width = 94
      end
      object ViewCardsIdGoods: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1062#1041
        DataBinding.FieldName = 'IdGoods'
      end
      object ViewCardsNAME: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'NAME'
      end
      object ViewCardsVOL: TcxGridDBColumn
        Caption = #1054#1073#1098#1077#1084
        DataBinding.FieldName = 'VOL'
      end
      object ViewCardsKREP: TcxGridDBColumn
        Caption = #1050#1088#1077#1087#1086#1089#1090#1100
        DataBinding.FieldName = 'KREP'
      end
      object ViewCardsAVID: TcxGridDBColumn
        Caption = #1042#1080#1076' '#1040#1055
        DataBinding.FieldName = 'AVID'
      end
      object ViewCardsPRODID: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1087#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1103
        DataBinding.FieldName = 'PRODID'
        Width = 53
      end
      object ViewCardsIMPORTERID: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1080#1084#1087#1086#1088#1090#1077#1088#1072
        DataBinding.FieldName = 'IMPORTERID'
        Width = 51
      end
      object ViewCardsPRONAME: TcxGridDBColumn
        Caption = #1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1100
        DataBinding.FieldName = 'PRONAME'
        Width = 147
      end
      object ViewCardsPRODINN: TcxGridDBColumn
        Caption = #1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1100' ('#1048#1053#1053')'
        DataBinding.FieldName = 'PRODINN'
        Width = 117
      end
      object ViewCardsPRODKPP: TcxGridDBColumn
        Caption = #1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1100' ('#1050#1055#1055')'
        DataBinding.FieldName = 'PRODKPP'
        Width = 117
      end
      object ViewCardsIMPNAME: TcxGridDBColumn
        Caption = #1048#1084#1087#1086#1088#1090#1077#1088
        DataBinding.FieldName = 'IMPNAME'
        Width = 142
      end
      object ViewCardsIMPORTERINN: TcxGridDBColumn
        Caption = #1048#1084#1087#1086#1088#1090#1077#1088' ('#1048#1053#1053')'
        DataBinding.FieldName = 'IMPORTERINN'
        Width = 100
      end
      object ViewCardsIMPORTERKPP: TcxGridDBColumn
        Caption = #1048#1084#1087#1086#1088#1090#1077#1088' ('#1050#1055#1055')'
        DataBinding.FieldName = 'IMPORTERKPP'
        Width = 102
      end
    end
    object LevelCards: TcxGridLevel
      GridView = ViewCards
    end
  end
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 981
    Height = 127
    BarManager = dxBarManager1
    Style = rs2010
    ColorSchemeAccent = rcsaOrange
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 5
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1050#1072#1088#1090#1086#1095#1082#1080' '#1090#1086#1074#1072#1088#1072' '#1045#1043#1040#1048#1057
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar1'
        end>
      Index = 0
    end
  end
  object quCards: TFDQuery
    Connection = dmR.FDConnection
    SQL.Strings = (
      'SELECT ca.[KAP]'
      '      ,ca.ICODECB as IdGoods'
      '      ,ca.[NAME]'
      '      ,ca.[VOL]'
      '      ,ca.[KREP]'
      '      ,ca.[AVID]'
      '      ,ca.[PRODID]'
      '      ,ca.[IMPORTERID]'
      #9'  ,prod.FULLNAME+'#39' ('#39'+prod.NAME+'#39')'#39'  as PRONAME'
      #9'  ,prod.PRODINN'
      #9'  ,prod.PRODKPP'
      #9'  ,imp.FULLNAME+'#39' ('#39'+imp.NAME+'#39')'#39' as IMPNAME'
      #9'  ,imp.IMPORTERINN'
      #9'  ,imp.IMPORTERKPP'
      '  FROM [dbo].[ACARDS] ca'
      '  left join [dbo].[APRODS] as prod on prod.PRODID=ca.[PRODID]'
      
        '  left join [dbo].[AIMPORTERS] as imp on imp.[IMPORTERID]=ca.[IM' +
        'PORTERID]'
      '')
    Left = 104
    Top = 201
    object quCardsKAP: TStringField
      FieldName = 'KAP'
      Origin = 'KAP'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 50
    end
    object quCardsIdGoods: TIntegerField
      FieldName = 'IdGoods'
      Origin = 'IdGoods'
    end
    object quCardsNAME: TMemoField
      FieldName = 'NAME'
      Origin = 'NAME'
      BlobType = ftMemo
      Size = 2147483647
    end
    object quCardsVOL: TSingleField
      FieldName = 'VOL'
      Origin = 'VOL'
      DisplayFormat = '0.000'
    end
    object quCardsKREP: TSingleField
      FieldName = 'KREP'
      Origin = 'KREP'
      DisplayFormat = '0.00'
    end
    object quCardsAVID: TIntegerField
      FieldName = 'AVID'
      Origin = 'AVID'
    end
    object quCardsPRODID: TStringField
      FieldName = 'PRODID'
      Origin = 'PRODID'
      Size = 50
    end
    object quCardsIMPORTERID: TStringField
      FieldName = 'IMPORTERID'
      Origin = 'IMPORTERID'
      Size = 50
    end
    object quCardsPRONAME: TMemoField
      FieldName = 'PRONAME'
      Origin = 'PRONAME'
      BlobType = ftMemo
      Size = 2147483647
    end
    object quCardsPRODINN: TStringField
      FieldName = 'PRODINN'
      Origin = 'PRODINN'
    end
    object quCardsPRODKPP: TStringField
      FieldName = 'PRODKPP'
      Origin = 'PRODKPP'
    end
    object quCardsIMPNAME: TMemoField
      FieldName = 'IMPNAME'
      Origin = 'IMPNAME'
      BlobType = ftMemo
      Size = 2147483647
    end
    object quCardsIMPORTERINN: TStringField
      FieldName = 'IMPORTERINN'
      Origin = 'IMPORTERINN'
    end
    object quCardsIMPORTERKPP: TStringField
      FieldName = 'IMPORTERKPP'
      Origin = 'IMPORTERKPP'
    end
  end
  object dsquCards: TDataSource
    DataSet = quCards
    Left = 104
    Top = 265
  end
  object ActionManager1: TActionManager
    LargeImages = dmR.LargeImage
    Images = dmR.SmallImage
    Left = 200
    Top = 200
    StyleName = 'Platform Default'
    object acClose: TAction
      Caption = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 100
      OnExecute = acCloseExecute
    end
    object acRefresh: TAction
      Caption = 'acRefresh'
      ShortCut = 116
      OnExecute = acRefreshExecute
    end
    object acGetKAPInfo: TAction
      Caption = #1047#1072#1087#1088#1086#1089#1080#1090#1100' '#1080#1085#1092#1086#1088#1084#1072#1094#1080#1102' '#1087#1086' '#1050#1040#1055
      ImageIndex = 11
      OnExecute = acGetKAPInfoExecute
    end
    object acRecalcKap: TAction
      Caption = #1056#1072#1089#1095#1080#1090#1072#1090#1100' '#1050#1040#1055' '#1087#1086' '#1040#1052
      OnExecute = acRecalcKapExecute
    end
    object acSetCodeCB: TAction
      Caption = #1057#1086#1087#1086#1089#1090#1072#1074#1080#1090#1100' '#1082#1086#1076' '#1062#1041
      ImageIndex = 9
      OnExecute = acSetCodeCBExecute
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      #1044#1077#1081#1089#1090#1074#1080#1103)
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmR.SmallImage
    ImageOptions.LargeImages = dmR.LargeImage
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 292
    Top = 196
    DockControlHeights = (
      0
      0
      0
      0)
    object dxBarManager1Bar1: TdxBar
      Caption = #1047#1072#1082#1088#1099#1090#1100
      CaptionButtons = <>
      DockedLeft = 305
      DockedTop = 0
      FloatLeft = 925
      FloatTop = 8
      FloatClientWidth = 92
      FloatClientHeight = 54
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1044#1077#1081#1089#1090#1074#1080#1103
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 1015
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          UserDefine = [udWidth]
          UserWidth = 159
          Visible = True
          ItemName = 'cxBarEditItem1'
        end
        item
          Visible = True
          ItemName = 'dxBarButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = acClose
      Category = 0
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = #1050#1040#1055
      Category = 0
      Hint = #1050#1040#1055
      Visible = ivAlways
      PropertiesClassName = 'TcxTextEditProperties'
    end
    object dxBarButton1: TdxBarButton
      Action = acGetKAPInfo
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = acSetCodeCB
      Category = 0
    end
    object dxBarGroup1: TdxBarGroup
      Items = ()
    end
  end
  object PopupMenu1: TPopupMenu
    Images = dmR.SmallImage
    Left = 432
    Top = 272
    object N1: TMenuItem
      Action = acSetCodeCB
    end
  end
end
