unit ViewSpec;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  CustomChildForm, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Vcl.Menus, cxClasses, FireDAC.Comp.Client, FireDAC.Comp.DataSet, System.Actions, Vcl.ActnList, dxBar,
  cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridCustomView, cxGrid, dxRibbon;

type
  TfmDocSpec = class(TfmCustomChildForm)
    dxBarManager1: TdxBarManager;
    dxBarGroup1: TdxBarGroup;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    dxBarManager1Bar1: TdxBar;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    ActionList1: TActionList;
    acClose: TAction;
    acSave: TAction;
    quSpecIn: TFDQuery;
    UpdSQL1: TFDUpdateSQL;
    quSpecInFSRARID: TStringField;
    quSpecInIDATE: TIntegerField;
    quSpecInSIDHD: TStringField;
    quSpecInID: TLargeintField;
    quSpecInNUM: TIntegerField;
    quSpecInALCCODE: TStringField;
    quSpecInQUANT: TFloatField;
    quSpecInPRICE: TFloatField;
    quSpecInQUANTF: TFloatField;
    quSpecInPRICEF: TFloatField;
    quSpecInNAME: TMemoField;
    dsquSpecIn: TDataSource;
    ViewSpec: TcxGridDBTableView;
    LevelSpec: TcxGridLevel;
    GridSpec: TcxGrid;
    quSpecInPRISEF0: TFloatField;
    quSpecInVOL: TSingleField;
    quSpecInKREP: TSingleField;
    quSpecInPRODNAME: TMemoField;
    ViewSpecNUM: TcxGridDBColumn;
    ViewSpecALCCODE: TcxGridDBColumn;
    ViewSpecQUANT: TcxGridDBColumn;
    ViewSpecPRICE: TcxGridDBColumn;
    ViewSpecQUANTF: TcxGridDBColumn;
    ViewSpecPRICEF: TcxGridDBColumn;
    ViewSpecNAME: TcxGridDBColumn;
    ViewSpecVOL: TcxGridDBColumn;
    ViewSpecKREP: TcxGridDBColumn;
    ViewSpecPRODNAME: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    quSpecInWBREGID: TStringField;
    ViewSpecWBREGID: TcxGridDBColumn;
    quSpecInCODECB: TIntegerField;
    quSpecInCODECBNUM: TIntegerField;
    quSpecInNAMECBNUM: TStringField;
    ViewSpecCODECB: TcxGridDBColumn;
    ViewSpecCODECBNUM: TcxGridDBColumn;
    ViewSpecNAMECBNUM: TcxGridDBColumn;
    acResetCode: TAction;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    quSpecInQUANTDOC: TFloatField;
    quSpecInPRICEDOC: TFloatField;
    ViewSpecQUANTDOC: TcxGridDBColumn;
    ViewSpecPRICEDOC: TcxGridDBColumn;
    cxStyle2: TcxStyle;
    quSpecInINFO_A: TStringField;
    quSpecInINFO_B: TStringField;
    quSpecInPARTY: TStringField;
    ViewSpecINFO_A: TcxGridDBColumn;
    ViewSpecINFO_B: TcxGridDBColumn;
    ViewSpecPARTY: TcxGridDBColumn;
    quSpecInSNUM: TStringField;
    ViewSpecSNUM: TcxGridDBColumn;
    dxBarManager1Bar2: TdxBar;
    quSpecInADDR: TMemoField;
    ViewSpecADDR: TcxGridDBColumn;
    procedure acCloseExecute(Sender: TObject);
    procedure acSaveExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acResetCodeExecute(Sender: TObject);
    procedure ViewSpecCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
  private
    { Private declarations }
    FRARID:string;
    FIDATE: Integer;
    FSID:string;
    FIEDIT: Integer;
    FSDATE:string;
    FNUMBER:string;
    SCLI:string;

    procedure Init;

  public
    { Public declarations }
  end;

// var
//  fmDocSpec: TfmDocSpec;

procedure ShowSpecView(RARID,SID,FSDATE,FNUMBER,SCLI:string;IDATE,IEDIT:Integer);

implementation

{$R *.dfm}

uses ViewRar, dmRar;

procedure ShowSpecView(RARID,SID,FSDATE,FNUMBER,SCLI:string;IDATE,IEDIT:Integer);
var F:TfmDocSpec;
begin
  F:=TfmDocSpec.Create(Application);
//  ShowMessage(Xmlstr);
  F.FRARID:=RARID;
  F.FIDATE:=IDATE;
  F.FSID:=SID;
  F.FIEDIT:=IEDIT;

  F.FSDATE:=FSDATE;
  F.FNUMBER:=FNUMBER;
  F.SCLI:=SCLI;

  F.Init;
  F.Show;
end;

procedure TfmDocSpec.acCloseExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmDocSpec.acResetCodeExecute(Sender: TObject);
var Rec:TcxCustomGridRecord;
    i,j:Integer;
    ID:Integer;
begin
  // ����������� ���
  if ViewSpec.Controller.SelectedRecordCount>0 then
  begin
    for i:=0 to ViewSpec.Controller.SelectedRecordCount-1 do
    begin
      Rec:=ViewSpec.Controller.SelectedRecords[i];

      ID:=0;

      for j:=0 to Rec.ValueCount-1 do
      begin
        if ViewSpec.Columns[j].Name='ViewSpecNUM' then begin ID:=Rec.Values[j]; Break; end;
      end;
      if ID>0 then
      begin
        if quSpecIn.Locate('NUM',ID,[]) then
        begin
          quSpecIn.Edit;
          quSpecInCODECB.AsInteger:=quSpecInCODECBNUM.AsInteger;
          quSpecIn.Post;
        end;
      end;
    end;

    acSave.Execute;
    init;
  end;
end;

procedure TfmDocSpec.acSaveExecute(Sender: TObject);
var iErrors:Integer;
begin
  //��������� ������������
  quSpecIn.UpdateTransaction.StartTransaction;
  iErrors := quSpecIn.ApplyUpdates;
  if iErrors = 0 then begin
    try
      quSpecIn.CommitUpdates;
      quSpecIn.UpdateTransaction.Commit;
    except
      ShowMessage('������ ����������..');
    end;
  end else
  begin
    ShowMessage('������ ����������.');
    quSpecIn.UpdateTransaction.Rollback;
  end;
end;

procedure TfmDocSpec.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
end;

procedure TfmDocSpec.Init;
begin
  Caption:='������������ '+FSDATE+', '+FNUMBER+', '+SCLI+' ('+ FSID+')';

  ViewSpec.BeginUpdate;
  quSpecIn.Active:=False;

  quSpecIn.ParamByName('RARID').AsString:=FRARID;
  quSpecIn.ParamByName('IDATE').AsInteger:=FIDATE;
  quSpecIn.ParamByName('SID').AsString:=FSID;
  quSpecIn.Active:=True;
  ViewSpec.EndUpdate;

  if FIEDIT=1 then
  begin
    acSave.Enabled:=True;
    ViewSpecQUANTF.Options.Editing:=True;
    ViewSpecPRICEF.Options.Editing:=True;
  end else
  begin
    acSave.Enabled:=False;
    ViewSpecQUANTF.Options.Editing:=False;
    ViewSpecPRICEF.Options.Editing:=False;
  end;

end;

procedure TfmDocSpec.ViewSpecCustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);
var i:Integer;
    rQ:Real;
    sQ:string;
begin
 //
  for i:=0 to ViewSpec.ColumnCount-1 do
  begin
    if ViewSpec.Columns[i].Name='ViewSpecQUANTF' then
    begin
      sQ:=Trim(AViewInfo.GridRecord.DisplayTexts[i]);
      break;
    end;
  end;

//  while Pos(',',sQ)>0 do sQ[Pos(',',sQ)]:='.';

  rQ:=StrToFloatDef(sQ,-1);

  if Abs(rQ)<0.001 then  ACanvas.Canvas.Brush.Color := $00CACAFF;

end;

end.
