unit ViewSpecCorr;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  CustomChildForm, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer,
  cxTextEdit, cxCalendar, cxCheckBox, cxDBLookupComboBox, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error,
  FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Vcl.Menus, cxClasses, FireDAC.Comp.Client,
  FireDAC.Comp.DataSet, System.Actions, Vcl.ActnList, dxBar, cxBarEditItem, cxMemo, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridCustomView, cxGrid, dxRibbon;

type
  TfmDocSpecCorr = class(TfmCustomChildForm)
    dxBarManager1: TdxBarManager;
    dxBarGroup1: TdxBarGroup;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    dxBarManager1Bar1: TdxBar;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    ActionList1: TActionList;
    acClose: TAction;
    acSave: TAction;
    quSpecCorr: TFDQuery;
    UpdSQL1: TFDUpdateSQL;
    dsquSpecCorr: TDataSource;
    ViewSpecCorr: TcxGridDBTableView;
    LevelSpec: TcxGridLevel;
    GridSpec: TcxGrid;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    PopupMenu1: TPopupMenu;
    cxStyle2: TcxStyle;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    cxBarEditItem2: TcxBarEditItem;
    quE: TFDQuery;
    acTestSpec: TAction;
    dxBarLargeButton4: TdxBarLargeButton;
    acDelPos: TAction;
    N1: TMenuItem;
    dxBarManager1Bar2: TdxBar;
    quSpecCorrFSRARID: TStringField;
    quSpecCorrIDATE: TIntegerField;
    quSpecCorrSIDHD: TStringField;
    quSpecCorrID: TLargeintField;
    quSpecCorrNUM: TIntegerField;
    quSpecCorrALCCODE: TStringField;
    quSpecCorrQUANT: TFloatField;
    quSpecCorrNAME: TMemoField;
    quSpecCorrVOL: TSingleField;
    quSpecCorrKREP: TSingleField;
    quSpecCorrAVID: TIntegerField;
    ViewSpecCorrFSRARID: TcxGridDBColumn;
    ViewSpecCorrIDATE: TcxGridDBColumn;
    ViewSpecCorrSIDHD: TcxGridDBColumn;
    ViewSpecCorrID: TcxGridDBColumn;
    ViewSpecCorrNUM: TcxGridDBColumn;
    ViewSpecCorrALCCODE: TcxGridDBColumn;
    ViewSpecCorrQUANT: TcxGridDBColumn;
    ViewSpecCorrNAME: TcxGridDBColumn;
    ViewSpecCorrVOL: TcxGridDBColumn;
    ViewSpecCorrKREP: TcxGridDBColumn;
    ViewSpecCorrAVID: TcxGridDBColumn;
    quSpecCorrPRODID: TStringField;
    quSpecCorrPRODINN: TStringField;
    quSpecCorrPRODKPP: TStringField;
    quSpecCorrNAMECLI: TMemoField;
    quSpecCorrFULLNAMECLI: TMemoField;
    quSpecCorrADDR: TMemoField;
    quSpecCorrCOUNTRY: TStringField;
    quSpecCorrREGCODE: TStringField;
    ViewSpecCorrPRODID: TcxGridDBColumn;
    ViewSpecCorrFULLNAMECLI: TcxGridDBColumn;
    acGetInfoSel: TAction;
    dxBarLargeButton5: TdxBarLargeButton;
    N2: TMenuItem;
    N3: TMenuItem;
    ViewSpecCorrPRODINN: TcxGridDBColumn;
    ViewSpecCorrPRODKPP: TcxGridDBColumn;
    quSpecCorrCLITYPE: TStringField;
    ViewSpecCorrCLITYPE: TcxGridDBColumn;
    acAddPos: TAction;
    N4: TMenuItem;
    procedure acCloseExecute(Sender: TObject);
    procedure acSaveExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acDelPosExecute(Sender: TObject);
    procedure acGetInfoSelExecute(Sender: TObject);
    procedure acAddPosExecute(Sender: TObject);
  private
    { Private declarations }
    FRARID:string;
    FIDATE: Integer;
    FSID:string;
    FIEDIT: Integer;
    FNUMBER:string;
    FDESCR:string;
    FIADD:Integer;

    procedure Init;

  public
    { Public declarations }
  end;

// var
//  fmDocSpec: TfmDocSpec;

procedure ShowSpecViewCorr(RARID,SID,FNUMBER,FDESCR:string;IDATE,IEDIT,IADD:Integer);

implementation

{$R *.dfm}

uses ViewRar, dmRar, EgaisDecode, ViewDocCorr, AddPosCorrSpec, ViewCards;

procedure ShowSpecViewCorr(RARID,SID,FNUMBER,FDESCR:string;IDATE,IEDIT,IADD:Integer);
var F:TfmDocSpecCorr;
begin
  F:=TfmDocSpecCorr.Create(Application);
// ShowMessage(Xmlstr);
  F.FRARID:=RARID;
  F.FIDATE:=IDATE;
  F.FSID:=SID;
  F.FIEDIT:=IEDIT;
  F.FDESCR:=FDESCR;
  F.FIADD:=IADD;
  F.FNUMBER:=FNUMBER;

  F.Init;
  F.Show;
end;

procedure TfmDocSpecCorr.acAddPosExecute(Sender: TObject);
begin
  //�������� �������
  //�������� �������
  if FIEDIT=1 then
  begin
    //�������� �������
    if Assigned(fmACard)=False then
    begin //����� �� ����������, � ����� �������
      fmACard:=TfmACard.Create(Application);
      fmACard.Init;
      fmACard.FRARID:=FRARID;
      fmACard.FIDATE:=FIDATE;
      fmACard.FSID:=FSID;
      fmACard.STYPE:='CORR';
      fmACard.quSpec:=quSpecCorr;
      fmACard.Show;
    end else
    begin
      fmACard.FRARID:=FRARID;
      fmACard.FIDATE:=FIDATE;
      fmACard.FSID:=FSID;
      fmACard.STYPE:='CORR';
      fmACard.quSpec:=quSpecCorr;
      fmACard.Show;
    end;

    {
    if not Assigned(fmAddPosCorr) then fmAddPosCorr:=TfmAddPosCorr.Create(Application,TFormStyle.fsNormal);

    fmAddPosCorr.ShowModal;
    if fmAddPosCorr.ModalResult=mrOk then
    begin //�������� �������
      with fmAddPosCorr do
      with dmR do
      begin
        quSelCard.Active:=False;
        quSelCard.ParamByName('CODE').AsString:=fmAddPosCorr.cxButtonEdit1.Text;
        quSelCard.Active:=True;

        if quSelCard.RecordCount=1 then
        begin
        end;

        quSelCard.Active:=False;
      end;
    end;}
  end;

end;

procedure TfmDocSpecCorr.acCloseExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmDocSpecCorr.acDelPosExecute(Sender: TObject);
begin
  if FIEDIT=1 then
  begin
    quSpecCorr.Delete;
  end;
end;

procedure TfmDocSpecCorr.acGetInfoSelExecute(Sender: TObject);
var Rec:TcxCustomGridRecord;
    i,j,iC:Integer;
    SKAP:string;
begin
  //��������� � ����� ���������� �� ��������� ����� �� ���������� ������� - ��������� ��������������
  if ViewSpecCorr.Controller.SelectedRecordCount>0 then
  begin
    ClearMessageLogLocal;
    ShowMessageLogLocal('����� ... ���� ��������� ����������.');
    iC:=0;

    for i:=0 to ViewSpecCorr.Controller.SelectedRecordCount-1 do
    begin
      Rec:=ViewSpecCorr.Controller.SelectedRecords[i];

      SKAP:='';

      for j:=0 to Rec.ValueCount-1 do
      begin
        if ViewSpecCorr.Columns[j].Name='ViewSpecCorrALCCODE' then begin SKAP:=Rec.Values[j]; Break; end;
      end;
      if (SKAP>'') then
      begin
        ShowMessageLogLocal('    ���  '+SKAP);
        prSendKAP(CommonSet.FSRAR_ID,sKap,MemoLogLocal);
        inc(iC);
      end;
    end;

    ShowMessageLogLocal('���������� '+its(iC)+' ����������.');
    ShowMessageLogLocal('������� ��������.');
  end;
end;

procedure TfmDocSpecCorr.acSaveExecute(Sender: TObject);
var iErrors:Integer;
begin
  //��������� ������������
  if FIADD=1 then
  begin
    try
      with dmR do
      begin
        quS.Active:=False;
        quS.SQL.Clear;
        quS.SQL.Add('');
        quS.SQL.Add('  declare @FSRARID VARCHAR(50) = '''+CommonSet.FSRAR_ID+'''');
        quS.SQL.Add('  DECLARE @IDATEVN INT = '+its(FIDATE));
        quS.SQL.Add('  DECLARE @SID VARCHAR(50) = '''+FSID+'''');
        quS.SQL.Add('  DECLARE @SNUM VARCHAR(50) = '''+FNUMBER+'''');
        quS.SQL.Add('  DECLARE @STYPE VARCHAR(10) = ''---''');
        quS.SQL.Add('  INSERT INTO [dbo].[ADOCSCORR_HD] ([FSRARID],[IDATE],[SID],[NUMBER],[STYPE],[IACTIVE])');
        quS.SQL.Add('  VALUES (@FSRARID,@IDATEVN,@SID,@SNUM,@STYPE,0)');
        quS.ExecSQL;

        FIADD:=0;
      end;
    except
    end;
  end;

  if FIADD=0 then
  begin
    quSpecCorr.UpdateTransaction.StartTransaction;
    iErrors := quSpecCorr.ApplyUpdates;
    if iErrors = 0 then begin
      try
        quSpecCorr.CommitUpdates;
        quSpecCorr.UpdateTransaction.Commit;

        quE.Active:=False;
        quE.SQL.Clear;
        quE.SQL.Add('UPDATE [dbo].[ADOCSCORR_HD]');
        quE.SQL.Add('Set');
        quE.SQL.Add('[DESCR]='''+Trim(cxBarEditItem1.EditValue)+'''');
        quE.SQL.Add('WHERE');
        quE.SQL.Add('[FSRARID] = '''+FRARID+'''');
        quE.SQL.Add('and [IDATE] ='+its(FIDATE));
        quE.SQL.Add('and [SID] ='''+FSID+'''');
        quE.ExecSQL;

        if Assigned(fmDocCorr) then
          if fmDocCorr.Showing then fmDocCorr.Init;
      except
        ShowMessage('������ ����������..');
      end;
    end else
    begin
      ShowMessage('������ ����������.');
      quSpecCorr.UpdateTransaction.Rollback;
    end;
  end;
end;

procedure TfmDocSpecCorr.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
end;

procedure TfmDocSpecCorr.Init;
begin

  Caption:='������������ ��������� ��������� �������� '+FNUMBER+', '+ FSID;

  cxBarEditItem1.EditValue:=FDESCR;

  ViewSpecCorr.BeginUpdate;
  quSpecCorr.Active:=False;
  quSpecCorr.ParamByName('RARID').AsString:=FRARID;
  quSpecCorr.ParamByName('IDATE').AsInteger:=FIDATE;
  quSpecCorr.ParamByName('SID').AsString:=FSID;
  quSpecCorr.Active:=True;
  ViewSpecCorr.EndUpdate;


  if FIEDIT=1 then
  begin
    acSave.Enabled:=True;
    ViewSpecCorrQUANT.Options.Editing:=True;
    cxBarEditItem1.Properties.ReadOnly:=False;
  end else
  begin
    acSave.Enabled:=False;
    ViewSpecCorrQUANT.Options.Editing:=False;
    cxBarEditItem1.Properties.ReadOnly:=True;
  end;
end;

end.
