unit Clients;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  CustomChildForm, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid,
  System.Actions, Vcl.ActnList, Vcl.PlatformDefaultStyleActnCtrls, Vcl.ActnMan, dxRibbonSkins, dxRibbonCustomizationForm, dxRibbon, dxBar, cxTextEdit, cxBarEditItem,
  cxDBLookupComboBox, Vcl.Menus;

type
  TfmClients = class(TfmCustomChildForm)
    ViewClients: TcxGridDBTableView;
    LevelClients: TcxGridLevel;
    GridClients: TcxGrid;
    quClients: TFDQuery;
    dsquClients: TDataSource;
    ActionManager1: TActionManager;
    acClose: TAction;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarGroup1: TdxBarGroup;
    dxRibbon1: TdxRibbon;
    dxRibbon1Tab1: TdxRibbonTab;
    acRefresh: TAction;
    dxBarManager1Bar2: TdxBar;
    PopupMenu1: TPopupMenu;
    quClientsCLIENTID: TStringField;
    quClientsCLIENTINN: TStringField;
    quClientsCLIENTKPP: TStringField;
    quClientsFULLNAME: TMemoField;
    quClientsNAME: TMemoField;
    quClientsCOUNTRY: TStringField;
    quClientsREGCODE: TStringField;
    quClientsADDR: TMemoField;
    ViewClientsCLIENTID: TcxGridDBColumn;
    ViewClientsCLIENTINN: TcxGridDBColumn;
    ViewClientsCLIENTKPP: TcxGridDBColumn;
    ViewClientsFULLNAME: TcxGridDBColumn;
    ViewClientsNAME: TcxGridDBColumn;
    ViewClientsCOUNTRY: TcxGridDBColumn;
    ViewClientsREGCODE: TcxGridDBColumn;
    ViewClientsADDR: TcxGridDBColumn;
    acLic: TAction;
    dxBarLargeButton1: TdxBarLargeButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acCloseExecute(Sender: TObject);
    procedure acRefreshExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure acLicExecute(Sender: TObject);
    procedure ViewClientsDblClick(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
    procedure Init;
  end;

var
  fmClients: TfmClients;

implementation

{$R *.dfm}

uses ViewRar, dmRar, UnitFunction, EgaisDecode, SetCodeCB, CliLic;

procedure TfmClients.acCloseExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmClients.acLicExecute(Sender: TObject);
begin
  if quClients.RecordCount>0 then ShowCliLic(quClientsCLIENTID.AsString,quClientsNAME.AsString);
end;

procedure TfmClients.acRefreshExecute(Sender: TObject);
begin
  Init;
end;

procedure TfmClients.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewClients.StoreToIniFile(GetGridIniFile,False,[gsoUseSummary,gsoUseFilter]);
  Action:=caFree;
  fmClients:=Nil;
end;

procedure TfmClients.FormCreate(Sender: TObject);
begin
  ViewClients.RestoreFromIniFile(GetGridIniFile);
end;

procedure TfmClients.Init;
var
  FocusedRow, TopRow: Integer;
  flag: boolean;
begin
  if dmR.FDConnection.Connected then
  begin
    try
      ViewClients.BeginUpdate;
          //<--Refresh � ��������������� ������� ������� � �������
      flag:=quClients.Active;  //���������� ���� �� ������� �������, ��� �������������� ������� �������
      TopRow := ViewClients.Controller.TopRowIndex;
      FocusedRow := ViewClients.DataController.FocusedRowIndex;

      quClients.Active:=False;
      quClients.Active:=True;

    finally
      ViewClients.EndUpdate;
      //<--��������������� �������
      if flag then begin
        ViewClients.DataController.FocusedRowIndex := FocusedRow;
        ViewClients.Controller.TopRowIndex := TopRow;
      end;
    end;
  end;
end;


procedure TfmClients.ViewClientsDblClick(Sender: TObject);
begin
  if quClients.RecordCount>0 then ShowCliLic(quClientsCLIENTID.AsString,quClientsNAME.AsString);
end;

end.
