unit QBarCodeS;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  CustomChildForm, nativexml, httpsend, Shared_Types, System.Win.ComObj, Excel2010, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async,
  FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client, cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, dxRibbonSkins, dxRibbonCustomizationForm, dxRibbon, dxBar, System.Actions, Vcl.ActnList, Vcl.PlatformDefaultStyleActnCtrls,
  Vcl.ActnMan, dxBarExtItems, cxCalendar, cxImageComboBox, cxDBLookupComboBox, cxBarEditItem, dxmdaset, cxContainer, cxTextEdit, cxMemo,
  cxDBEdit, Vcl.StdCtrls, Vcl.ExtCtrls;

type
  TfmQBarCodeS = class(TfmCustomChildForm)
    grQBarCodeS: TcxGrid;
    vwQBarCodeS: TcxGridDBTableView;
    lvQBarCodeS: TcxGridLevel;
    dsDocSpec: TDataSource;
    ActionManager1: TActionManager;
    acClose: TAction;
    dxBarManager1: TdxBarManager;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarGroup1: TdxBarGroup;
    dxRibbon1: TdxRibbon;
    dxRibbon1Tab1: TdxRibbonTab;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarManager1Bar4: TdxBar;
    acSaveDoc: TAction;
    dxBarLargeButton1: TdxBarLargeButton;
    beiPoint: TcxBarEditItem;
    quDocSpec: TFDQuery;
    quDocSpecIDH: TIntegerField;
    quDocSpecID: TFDAutoIncField;
    quDocSpecIDENT: TIntegerField;
    quDocSpecTYPE: TStringField;
    quDocSpecRANK: TStringField;
    quDocSpecNUMBER: TStringField;
    vwQBarCodeSIDH: TcxGridDBColumn;
    vwQBarCodeSID: TcxGridDBColumn;
    vwQBarCodeSIDENT: TcxGridDBColumn;
    vwQBarCodeSTYPE: TcxGridDBColumn;
    vwQBarCodeSRANK: TcxGridDBColumn;
    vwQBarCodeSNUMBER: TcxGridDBColumn;
    quDocHead: TFDQuery;
    quDocHeadID: TFDAutoIncField;
    quDocHeadFSRARID: TStringField;
    quDocHeadIDATE: TIntegerField;
    quDocHeadINUMBER: TIntegerField;
    quDocHeadISTATUS: TIntegerField;
    acAdd: TAction;
    acDelete: TAction;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    acImport: TAction;
    dxBarLargeButton5: TdxBarLargeButton;
    OpenDialog1: TOpenDialog;
    quDocHeadIDTORAR: TLargeintField;
    quDocHeadREJCOMMENT: TMemoField;
    PanelRejected: TPanel;
    Label1: TLabel;
    cxDBMemo1: TcxDBMemo;
    dsDocHead: TDataSource;
    quDocSpecBARCODE: TStringField;
    vwQBarCodeSBARCODE: TcxGridDBColumn;
    procedure acCloseExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acSaveDocExecute(Sender: TObject);
    procedure quDocSpecBeforeApplyUpdates(DataSet: TFDDataSet);
    procedure quDocSpecNewRecord(DataSet: TDataSet);
    procedure acAddExecute(Sender: TObject);
    procedure acDeleteExecute(Sender: TObject);
    procedure acImportExecute(Sender: TObject);
  private
    { Private declarations }
    FIDH: Integer;
    FFSRARID: string;
    FState: TState;
    procedure Init;
  public
    { Public declarations }
  end;

//var
//  fmQBarCodeS: TfmQBarCodeS;

procedure ShowFormQBarCodeS(aIDH: Integer; aFSRARID: string; aState: TState);

implementation

{$R *.dfm}

uses dmRar, EgaisDecode, Shared_Functions, QBarCodeH;

procedure ShowFormQBarCodeS(aIDH: Integer; aFSRARID: string; aState: TState);
var F: TfmQBarCodeS;
begin
  F:=TfmQBarCodeS.Create(Application);
  if F.WindowState=wsMinimized then //���� ���� ��������, �� ������������� ���
    F.WindowState:=wsNormal;

  F.FIDH:=aIDH;
  F.FFSRARID:=aFSRARID;
  F.FState:=aState;
  F.Init;
  F.Show;
end;

procedure TfmQBarCodeS.Init;
begin
  Application.ProcessMessages;

  //<--����������� ������� ���
  case FState of
    sView: begin
             beiPoint.Enabled:=False;
             acSaveDoc.Enabled:=False;
             vwQBarCodeS.OptionsData.Editing:=False;
             vwQBarCodeS.OptionsData.Deleting:=False;
           end;
  end;
  //-->

  //<--���������� ������
  dmR.quPoint.Active:=False;
  dmR.quPoint.Active:=True;
  case FState of
    sAdd : begin
             beiPoint.EditValue:=dmR.quPointRARID.AsString;
             quDocHead.Active:=False;
             quDocHead.ParamByName('IDH').AsInteger:=0;
             quDocHead.Active:=True;
             quDocSpec.Active:=False;
             quDocSpec.ParamByName('IDH').AsInteger:=0;
             quDocSpec.Active:=True;
           end;
    sEdit,
    sView: begin
             dmR.quPoint.Locate('RARID',FFSRARID,[]);
             beiPoint.EditValue:=dmR.quPointRARID.AsString;

             quDocHead.Active:=False;
             quDocHead.ParamByName('IDH').AsInteger:=FIDH;
             quDocHead.Active:=True;
             quDocSpec.Active:=False;
             quDocSpec.ParamByName('IDH').AsInteger:=FIDH;
             quDocSpec.Active:=True;
           end;
  end;
  //-->
end;

procedure TfmQBarCodeS.quDocSpecBeforeApplyUpdates(DataSet: TFDDataSet);
var i: Integer;
begin
  quDocSpec.First;
  i:=1;
  while not quDocSpec.Eof do
  begin
    quDocSpec.Edit;
    quDocSpecIDH.AsInteger:=FIDH;
    quDocSpecIDENT.AsInteger:=i;
    quDocSpec.Post;
    i:=i+1;
    quDocSpec.Next;
  end;
end;

procedure TfmQBarCodeS.quDocSpecNewRecord(DataSet: TDataSet);
begin
  quDocSpecIDH.AsInteger:=FIDH;
  quDocSpecIDENT.AsInteger:=quDocSpec.RecNo;
end;

procedure TfmQBarCodeS.acAddExecute(Sender: TObject);
begin
  if vwQBarCodeS.OptionsData.Editing=False then Exit;

  quDocSpec.Append;
end;

procedure TfmQBarCodeS.acCloseExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmQBarCodeS.acDeleteExecute(Sender: TObject);
begin
  if vwQBarCodeS.OptionsData.Editing=False then Exit;

  quDocSpec.Delete;
end;

procedure TfmQBarCodeS.acImportExecute(Sender: TObject);
Var fName:String;
    ExcelApp, Workbook, ISheet: Variant;
    i:INteger;
    s:string;
    tType, tRank, tNumber: string;
begin
  if OpenDialog1.Execute then
    fName:=OpenDialog1.fileName
  else
    Exit;
  if FileExists(fName)=false then begin
    showmessage('���� �� ������');
    Exit;
  end;

  //�������
  quDocSpec.First;
  While not quDocSpec.Eof do
  begin
    quDocSpec.Delete;
  end;

  //����� ���������
  ClearMessageLogLocal;
  ShowMessageLogLocal('�����, ���� ������ ������..'); delay(10);

  ExcelApp := CreateOleObject('Excel.Application');
  ExcelApp.Application.EnableEvents := false;
  Workbook := ExcelApp.WorkBooks.Add(fName);
  ISheet := Workbook.Worksheets.Item[1];

  i:=1;
  s:='';
  While String(ISheet.Cells.Item[i, 1].Value)<>'' do
  begin
    tType:=String(ISheet.Cells.Item[i, 1].Value);
    tRank:=String(ISheet.Cells.Item[i, 2].Value);
    tNumber:=String(ISheet.Cells.Item[i, 3].Value);
    if (tType<>'') or (tRank<>'') or (tNumber<>'') then
    begin
      ShowMessageLogLocal('    - '+tType+', '+tRank+', '+tNumber); delay(10);

      quDocSpec.Append;
      quDocSpecTYPE.AsString:=tType;
      quDocSpecRANK.AsString:=tRank;
      quDocSpecNUMBER.AsString:=tNumber;
      quDocSpec.Post;
    end;

    inc(i);
  end;

  ExcelApp.DisplayAlerts:=False;
  ExcelApp.Quit;
  ExcelApp:=Unassigned;

  ShowMessageLogLocal('������� ��������.'); delay(10);
end;

procedure TfmQBarCodeS.acSaveDocExecute(Sender: TObject);
var iErrors: Integer;
begin
  if vwQBarCodeS.OptionsData.Editing=False then Exit;

  //����������
  if FIDH=0 then begin
    quDocHead.Append;
    quDocHeadFSRARID.AsString:=beiPoint.EditValue;
    quDocHeadIDATE.AsInteger:=Trunc(Date);
    quDocHeadINUMBER.AsInteger:=0;
    quDocHeadISTATUS.AsInteger:=0; //0-�����������
    quDocHead.Post;
  end else begin
    quDocHead.Edit;
    quDocHeadFSRARID.AsString:=beiPoint.EditValue;
    quDocHeadIDATE.AsInteger:=Trunc(Date);
    //quDocHeadINUMBER.AsInteger:=0;
    quDocHeadISTATUS.AsInteger:=0; //0-�����������
    quDocHead.Post;
  end;

  quDocHead.UpdateTransaction.StartTransaction;
  iErrors := quDocHead.ApplyUpdates;
  if iErrors = 0 then
  begin
    quDocHead.CommitUpdates;
    ShowMessageLogLocal(quDocHeadID.AsString);
    if FIDH=0 then
      FIDH:=quDocHeadID.AsInteger;

    iErrors := quDocSpec.ApplyUpdates;
    if iErrors = 0 then begin

      quDocSpec.CommitUpdates;
      quDocSpec.UpdateTransaction.Commit;

      ShowMessageLogLocal('���������� ��');
      Delay(10);

      //<--��������� ������ ����������
      if Assigned(fmQBarCodeH) then
        fmQBarCodeH.Init;
      //-->
    end else
      quDocSpec.UpdateTransaction.Rollback;
  end else
    quDocHead.UpdateTransaction.Rollback;
end;

procedure TfmQBarCodeS.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
end;

end.
