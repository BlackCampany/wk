object fmViewDocIn: TfmViewDocIn
  Left = 0
  Top = 0
  Caption = #1055#1088#1080#1093#1086#1076#1085#1099#1077' '#1076#1086#1082#1091#1084#1077#1085#1090#1099
  ClientHeight = 558
  ClientWidth = 1272
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1272
    Height = 127
    ApplicationButton.Visible = False
    BarManager = dxBarManager1
    Style = rs2010
    ColorSchemeAccent = rcsaOrange
    ColorSchemeName = 'Blue'
    SupportNonClientDrawing = True
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1055#1088#1080#1093#1086#1076#1099
      Groups = <
        item
          ToolbarName = 'bmApplyDate'
        end
        item
          ToolbarName = 'bmClose'
        end>
      Index = 0
    end
  end
  object GridDocIn: TcxGrid
    Left = 0
    Top = 127
    Width = 1272
    Height = 431
    Align = alClient
    TabOrder = 5
    LevelTabs.Style = 8
    LookAndFeel.Kind = lfOffice11
    LookAndFeel.NativeStyle = False
    OnFocusedViewChanged = GridDocInFocusedViewChanged
    object ViewDocIn: TcxGridDBTableView
      PopupMenu = pmDocs
      OnDblClick = ViewDocInDblClick
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.First.Visible = True
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.InfoPanel.Visible = True
      Navigator.Visible = True
      DataController.DataSource = dmR.dsquDocInHd
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.IncSearch = True
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.MultiSelect = True
      object ViewDocInFSRARID: TcxGridDBColumn
        Caption = 'FSRAR ID'
        DataBinding.FieldName = 'FSRARID'
        Width = 59
      end
      object ViewDocInIDATE: TcxGridDBColumn
        Caption = #1044#1072#1090#1072' ('#1094#1077#1083#1086#1077')'
        DataBinding.FieldName = 'IDATE'
        Visible = False
      end
      object ViewDocInSID: TcxGridDBColumn
        DataBinding.FieldName = 'SID'
        Width = 37
      end
      object ViewDocInIDHEAD: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1076#1086#1082#1091#1084#1077#1085#1090#1072' '#1052#1050#1088#1080#1089#1090#1072#1083
        DataBinding.FieldName = 'IDHEAD'
        Width = 77
      end
      object ViewDocInNUMBER: TcxGridDBColumn
        Caption = #8470' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
        DataBinding.FieldName = 'NUMBER'
        Width = 92
      end
      object ViewDocInDATE_OUTPUT: TcxGridDBColumn
        Caption = #1044#1086#1082'. '#1086#1090#1087#1088#1072#1074#1083#1077#1085
        DataBinding.FieldName = 'DATE_OUTPUT'
        Width = 132
      end
      object ViewDocInSHIPDATE: TcxGridDBColumn
        Caption = #1044#1072#1090#1072' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
        DataBinding.FieldName = 'SHIPDATE'
        Width = 101
      end
      object ViewDocInIACTIVE: TcxGridDBColumn
        Caption = #1057#1090#1072#1090#1091#1089' '#1087#1077#1088#1077#1076#1072#1095#1080' '#1074' '#1045#1043#1040#1048#1057
        DataBinding.FieldName = 'IACTIVE'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Images = dmR.SmallImage
        Properties.Items = <
          item
            Description = #1074' '#1086#1073#1088#1072#1073#1086#1090#1082#1077
            ImageIndex = 74
            Value = 0
          end
          item
            Description = #1054#1090#1087#1088#1072#1074#1083#1077#1085
            ImageIndex = 75
            Value = 1
          end>
      end
      object ViewDocInREADYSEND: TcxGridDBColumn
        Caption = #1057#1090#1072#1090#1091#1089' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
        DataBinding.FieldName = 'READYSEND'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Images = dmR.SmallImage
        Properties.Items = <
          item
            Description = #1075#1086#1090#1086#1074' '#1082' '#1087#1077#1088#1077#1076#1072#1095#1077
            ImageIndex = 1
            Value = 1
          end
          item
            Description = #1074' '#1088#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1085#1080#1080
            ImageIndex = 26
            Value = 0
          end>
      end
      object ViewDocInCOMPARE_STATUS: TcxGridDBColumn
        Caption = #1050#1040#1055' '#1089#1090#1072#1090#1091#1089' '#1089#1088#1072#1074#1085#1077#1085#1080#1103' '
        DataBinding.FieldName = 'COMPARE_STATUS'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Images = dmR.SmallImage
        Properties.Items = <
          item
            Description = #1085#1077#1090' '#1089#1086#1086#1090#1074#1077#1090#1089#1090#1074#1080#1103
            ImageIndex = 154
            Value = 0
          end
          item
            Description = #1054#1082
            ImageIndex = 156
            Value = 1
          end>
      end
      object ViewDocInCOMPARE_QUANT_STATUS: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086' '#1089#1090#1072#1090#1091#1089' '#1089#1088#1072#1074#1085#1077#1085#1080#1103
        DataBinding.FieldName = 'COMPARE_QUANT_STATUS'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Images = dmR.SmallImage
        Properties.Items = <
          item
            Description = #1045#1089#1090#1100' '#1088#1072#1089#1093#1086#1078#1076#1077#1085#1080#1103' '#1074' '#1082#1086#1083'-'#1074#1077
            ImageIndex = 154
            Value = 0
          end
          item
            Description = #1050#1086#1083'-'#1074#1086' '#1054#1050
            ImageIndex = 156
            Value = 1
          end>
      end
      object ViewDocInXMLFILE: TcxGridDBColumn
        Caption = 'XML'
        DataBinding.FieldName = 'XMLFILE'
        Width = 30
      end
      object ViewDocInTT1: TcxGridDBColumn
        Caption = #1040#1082#1090' '#1088#1072#1089#1093#1086#1078#1076#1077#1085#1080#1103' '#1087#1088#1080#1085#1103#1090' '#1074' '#1045#1043#1040#1048#1057
        DataBinding.FieldName = 'TT1'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Images = dmR.SmallImage
        Properties.Items = <
          item
            ImageIndex = 154
            Value = 0
          end
          item
            ImageIndex = 156
            Value = 1
          end
          item
            ImageIndex = 155
            Value = 2
          end>
        Options.Editing = False
      end
      object ViewDocInTT2: TcxGridDBColumn
        Caption = #1044#1086#1082#1091#1084#1077#1085#1090' '#1087#1088#1086#1074#1077#1076#1077#1085' '#1074' '#1045#1043#1040#1048#1057
        DataBinding.FieldName = 'TT2'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Images = dmR.SmallImage
        Properties.Items = <
          item
            ImageIndex = 154
            Value = 0
          end
          item
            ImageIndex = 156
            Value = 1
          end
          item
            ImageIndex = 155
            Value = 2
          end>
      end
      object ViewDocInCOMPARE_COMMENT: TcxGridDBColumn
        Caption = #1050#1086#1084#1084#1077#1085#1090#1072#1088#1080#1081' '#1089#1088#1072#1074#1085#1077#1085#1080#1103
        DataBinding.FieldName = 'COMPARE_COMMENT'
        Width = 184
      end
      object ViewDocInNAME: TcxGridDBColumn
        Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
        DataBinding.FieldName = 'NAME'
        Width = 133
      end
      object ViewDocInWBREGID: TcxGridDBColumn
        DataBinding.FieldName = 'WBREGID'
        Width = 144
      end
      object ViewDocInCLIENTINN: TcxGridDBColumn
        Caption = #1048#1053#1053' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
        DataBinding.FieldName = 'CLIENTINN'
      end
      object ViewDocInCLIENTKPP: TcxGridDBColumn
        Caption = #1050#1055#1055' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
        DataBinding.FieldName = 'CLIENTKPP'
      end
      object ViewDocInFULLNAME: TcxGridDBColumn
        Caption = #1055#1086#1083#1085#1086#1077' '#1085#1072#1079#1074#1072#1085#1080#1077' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
        DataBinding.FieldName = 'FULLNAME'
      end
      object ViewDocInFIXNUMBER: TcxGridDBColumn
        DataBinding.FieldName = 'FIXNUMBER'
        Width = 150
      end
      object ViewDocInSDATE: TcxGridDBColumn
        Caption = #1044#1072#1090#1072
        DataBinding.FieldName = 'SDATE'
        Width = 94
      end
      object ViewDocInDATE_INPUT: TcxGridDBColumn
        Caption = #1044#1086#1082'. '#1087#1086#1083#1091#1095#1077#1085
        DataBinding.FieldName = 'DATE_INPUT'
        Width = 132
      end
      object ViewDocInFIXDATE: TcxGridDBColumn
        DataBinding.FieldName = 'FIXDATE'
        Width = 90
      end
      object ViewDocInSTYPE: TcxGridDBColumn
        Caption = #1058#1080#1087
        DataBinding.FieldName = 'STYPE'
        Width = 69
      end
      object ViewDocInUNITTYPE: TcxGridDBColumn
        DataBinding.FieldName = 'UNITTYPE'
        Visible = False
      end
      object ViewDocInCLIFROM: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
        DataBinding.FieldName = 'CLIFROM'
        Width = 97
      end
    end
    object LevelDocIn: TcxGridLevel
      Caption = #1042#1093#1086#1076#1103#1097#1080#1077' '#1076#1086#1082#1091#1084#1077#1085#1090#1099
      GridView = ViewDocIn
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      #1044#1077#1081#1089#1090#1074#1080#1103)
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmR.SmallImage
    ImageOptions.LargeImages = dmR.LargeImage
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 708
    Top = 204
    DockControlHeights = (
      0
      0
      0
      0)
    object bmApplyDate: TdxBar
      Caption = #1044#1077#1081#1089#1090#1074#1080#1103
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 778
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          UserDefine = [udWidth]
          UserWidth = 75
          Visible = True
          ItemName = 'deDateBeg'
        end
        item
          UserDefine = [udWidth]
          UserWidth = 74
          Visible = True
          ItemName = 'deDateEnd'
        end
        item
          Visible = True
          ItemName = 'btnDone'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end
        item
          Visible = True
          ItemName = 'dxBarButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarButton3'
        end
        item
          BeginGroup = True
          UserDefine = [udWidth]
          UserWidth = 161
          Visible = True
          ItemName = 'beiGetTTN'
        end
        item
          ViewLevels = [ivlLargeControlOnly, ivlSmallIconWithText, ivlSmallIcon, ivlControlOnly]
          Visible = True
          ItemName = 'dxBarLargeButton14'
        end
        item
          ViewLevels = [ivlLargeControlOnly, ivlSmallIconWithText, ivlSmallIcon, ivlControlOnly]
          Visible = True
          ItemName = 'dxBarLargeButton16'
        end
        item
          ViewLevels = [ivlLargeControlOnly, ivlSmallIconWithText, ivlSmallIcon, ivlControlOnly]
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          ViewLevels = [ivlLargeControlOnly, ivlSmallIconWithText, ivlSmallIcon, ivlControlOnly]
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          ViewLevels = [ivlLargeControlOnly, ivlSmallIconWithText, ivlSmallIcon, ivlControlOnly]
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object bmClose: TdxBar
      Caption = #1047#1072#1082#1088#1099#1090#1100
      CaptionButtons = <>
      DockedLeft = 967
      DockedTop = 0
      FloatLeft = 778
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'btnClose'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object deDateBeg: TdxBarDateCombo
      Caption = 'C  '
      Category = 0
      Hint = 'C  '
      Visible = ivAlways
      OnChange = deDateBegChange
      ImageIndex = 25
      ShowDayText = False
    end
    object deDateEnd: TdxBarDateCombo
      Caption = #1087#1086
      Category = 0
      Hint = #1087#1086
      Visible = ivAlways
      OnChange = deDateBegChange
      ImageIndex = 25
      ShowDayText = False
    end
    object btnDone: TdxBarLargeButton
      Action = acRefresh
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = acUTMProc
      Category = 0
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = acUTMExch
      Category = 0
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = acUTMArh
      Category = 0
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = #1048#1053#1053' '
      Category = 0
      Hint = #1048#1053#1053' '
      Visible = ivAlways
      PropertiesClassName = 'TcxTextEditProperties'
      BarStyleDropDownButton = False
      Properties.AutoSelect = False
      Properties.IncrementalSearch = False
      Properties.ValidationOptions = [evoAllowLoseFocus]
      InternalEditValue = ''
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = acProd
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = acSendStatus
      Caption = #1056#1072#1079#1088#1077#1096#1080#1090#1100' '#1086#1090#1087#1088#1072#1074#1082#1091'  '
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = acMCrystalDocs
      Category = 0
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = acProDocs
      Category = 0
    end
    object beiGetTTN: TcxBarEditItem
      Caption = #8470' TTN'
      Category = 0
      Hint = #8470' TTN'
      Visible = ivAlways
      PropertiesClassName = 'TcxTextEditProperties'
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Action = acGetTTN
      Category = 0
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Action = acGetRawTTNList
      Category = 0
    end
    object btnClose: TdxBarLargeButton
      Action = acClose
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = acQueryFormF1
      Caption = #1047#1072#1087#1088#1086#1089' '#1089#1087#1088#1072#1074#1082#1080' 1'
      Category = 0
      Visible = ivNever
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = acQueryRests
      Category = 0
      Visible = ivNever
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = acQueryBarcode
      Category = 0
      Visible = ivNever
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = acCBDoc
      Category = 0
    end
    object dxBarButton1: TdxBarButton
      Action = acCompare
      Category = 0
    end
    object dxBarButton2: TdxBarButton
      Action = acSendStatus
      Category = 0
    end
    object dxBarButton3: TdxBarButton
      Action = acSendActR
      Category = 0
    end
    object dxBarGroup1: TdxBarGroup
      Items = ()
    end
  end
  object ActionList1: TActionList
    Images = dmR.SmallImage
    Left = 560
    Top = 228
    object acRefresh: TAction
      Category = #1060#1086#1088#1084#1072
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100' (F5)'
      Hint = #1054#1073#1085#1086#1074#1080#1090#1100' (F5)'
      ImageIndex = 5
      ShortCut = 116
      OnExecute = acRefreshExecute
    end
    object acClose: TAction
      Category = #1060#1086#1088#1084#1072
      Caption = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 100
      OnExecute = acCloseExecute
    end
    object acSelectDate: TAction
      Category = #1060#1086#1088#1084#1072
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      Hint = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      ImageIndex = 1
      OnExecute = acSelectDateExecute
    end
    object acDecodeTovar: TAction
      Category = #1044#1077#1082#1086#1076#1080#1088#1086#1074#1072#1085#1080#1077
      Caption = #1044#1077#1082#1086#1076#1080#1088#1086#1074#1072#1090#1100' '#1082#1072#1082' '#1090#1086#1074#1072#1088
      ImageIndex = 38
    end
    object acSaveToFile: TAction
      Category = #1044#1077#1082#1086#1076#1080#1088#1086#1074#1072#1085#1080#1077
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1074' '#1092#1072#1081#1083
      ImageIndex = 7
    end
    object acDelList: TAction
      Category = #1044#1077#1082#1086#1076#1080#1088#1086#1074#1072#1085#1080#1077
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1074#1099#1076#1077#1083#1077#1085#1085#1099#1077' '#1089#1090#1088#1086#1082#1080
      ImageIndex = 4
      ShortCut = 16452
    end
    object acDecodeTTN: TAction
      Category = #1044#1077#1082#1086#1076#1080#1088#1086#1074#1072#1085#1080#1077
      Caption = #1044#1077#1082#1086#1076#1080#1088#1086#1074#1072#1090#1100' '#1082#1072#1082' '#1076#1086#1082#1091#1084#1077#1085#1090
      ImageIndex = 38
    end
    object acCards: TAction
      Category = #1060#1086#1088#1084#1072
      Caption = #1050#1072#1088#1090#1086#1095#1082#1080
      ImageIndex = 11
    end
    object acSendActR: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1057#1092#1086#1088#1084'. '#1072#1082#1090' '#1088#1072#1089#1093#1086#1078#1076#1077#1085#1080#1103
      ImageIndex = 49
      OnExecute = acSendActRExecute
    end
    object acDecodeWB: TAction
      Category = #1044#1077#1082#1086#1076#1080#1088#1086#1074#1072#1085#1080#1077
      Caption = 'acDecodeWB'
      ImageIndex = 38
    end
    object acUTMExch: TAction
      Category = #1060#1086#1088#1084#1072
      Caption = #1059#1058#1052' '#1080#1089#1090#1086#1088#1080#1103
      ImageIndex = 130
    end
    object acUTMProc: TAction
      Category = #1060#1086#1088#1084#1072
      Caption = #1059#1058#1052' '#1086#1073#1084#1077#1085
      ImageIndex = 38
    end
    object acProd: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1047#1072#1087#1088#1086#1089' '#1072#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090#1072' '#1087#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1103
      ImageIndex = 16
      OnExecute = acProdExecute
    end
    object acSendActPrih: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1057#1092#1086#1088#1084#1080#1088#1086#1074#1072#1090#1100' '#1072#1082#1090' '#1087#1086#1076#1090#1074#1077#1088#1078#1076#1077#1085#1080#1103
      Enabled = False
      ImageIndex = 1
      Visible = False
      OnExecute = acSendActPrihExecute
    end
    object acCompare: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1057#1086#1087#1086#1089#1090#1072#1074#1080#1090#1100' '#1074#1099#1076'. '#1076#1086#1082#1091#1084#1077#1085#1090#1099
      ImageIndex = 92
      OnExecute = acCompareExecute
    end
    object acUnCompare: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1057#1085#1103#1090#1100' '#1089#1086#1087#1086#1089#1090#1072#1074#1083#1077#1085#1080#1077' '#1074#1099#1076'.'#1076#1086#1082#1091#1084#1077#1085#1090#1086#1074
      ImageIndex = 163
      OnExecute = acUnCompareExecute
    end
    object acCompareHand: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1057#1086#1087#1086#1089#1090#1072#1074#1080#1090#1100' '#1076#1086#1082#1091#1084#1077#1085#1090' '#1074#1088#1091#1095#1085#1091#1102
      ImageIndex = 165
      OnExecute = acCompareHandExecute
    end
    object acCompareHand2: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1057#1086#1087#1086#1089#1090#1072#1074#1080#1090#1100' '#1076#1086#1082#1091#1084#1077#1085#1090' '#1074#1088#1091#1095#1085#1091#1102' ('#1090#1086#1083#1100#1082#1086' '#1082#1086#1076' '#1076#1086#1082#1091#1084#1077#1085#1090#1072')'
      ImageIndex = 165
      OnExecute = acCompareHand2Execute
    end
    object acSendStatus: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1056#1072#1079#1088#1077#1096#1080#1090#1100' '#1086#1090#1087#1088#1072#1074#1082#1091' '#1085#1072' '#1074#1099#1076'. '
      ImageIndex = 1
      OnExecute = acSendStatusExecute
    end
    object acSendActR2: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1055#1086#1074#1090#1086#1088#1085#1072#1103' '#1086#1090#1087#1088#1072#1074#1082#1072' '#1040#1082#1090#1072' '#1088#1072#1089#1093#1086#1078#1076#1077#1085#1080#1103
      ShortCut = 49235
      OnExecute = acSendActR2Execute
    end
    object acAuto: TAction
      Category = #1060#1086#1088#1084#1072
      Caption = #1040#1074#1090#1086
      ShortCut = 49217
    end
    object acExpExcel: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1069#1082#1089#1087#1086#1088#1090' '#1074' Excel'
      ImageIndex = 76
      OnExecute = acExpExcelExecute
    end
    object acCreateReturn: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1057#1092#1086#1088#1084#1080#1088#1086#1074#1072#1090#1100' '#1074#1086#1079#1074#1088#1072#1090#1085#1099#1081' '#1076#1086#1082#1091#1084#1077#1085#1090
      ImageIndex = 12
      OnExecute = acCreateReturnExecute
    end
    object acSendRet: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1054#1090#1087#1088#1072#1074#1080#1090#1100' '#1074#1086#1079#1074#1088#1072#1090' '#1074' '#1045#1043#1040#1048#1057
      Enabled = False
      ImageIndex = 70
      OnExecute = acSendRetExecute
    end
    object acMCrystalDocs: TAction
      Category = #1060#1086#1088#1084#1072
      Caption = #1052#1050#1088#1080#1089#1090#1072#1083
      ImageIndex = 161
    end
    object acProDocs: TAction
      Category = #1060#1086#1088#1084#1072
      Caption = #1055#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1086
      Enabled = False
      ImageIndex = 162
      Visible = False
      OnExecute = acProDocsExecute
    end
    object acInfoAB: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1057#1087#1088#1072#1074#1082#1080' '#1040#1080#1041
      ShortCut = 16450
      OnExecute = acInfoABExecute
    end
    object acSendActR_HK: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1055#1086#1074#1090#1086#1088#1085#1072#1103' '#1086#1090#1087#1088#1072#1074#1082#1072' '#1040#1082#1090#1072' '#1088#1072#1089#1093#1086#1078#1076#1077#1085#1080#1081
      ShortCut = 49235
      OnExecute = acSendActR_HKExecute
    end
    object acGetRetDocs: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1055#1086#1083#1091#1095#1080#1090#1100' '#1074#1086#1079#1074#1088#1072#1090#1085#1099#1077' '#1076#1086#1082#1091#1084#1077#1085#1090#1099
      OnExecute = acGetRetDocsExecute
    end
    object acDelDocOut: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1076#1086#1082#1091#1084#1077#1085#1090
      OnExecute = acDelDocOutExecute
    end
    object acGetTTN: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1047#1072#1087#1088#1086#1089#1080#1090#1100' '#1076#1086#1082#1091#1084#1077#1085#1090' '#1074' '#1045#1043#1040#1048#1057
      ImageIndex = 50
      OnExecute = acGetTTNExecute
    end
    object acGetTTNPopup: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1047#1072#1087#1088#1086#1089#1080#1090#1100' '#1074#1099#1076#1077#1083#1077#1085#1085#1099#1081' '#1076#1086#1082#1091#1084#1077#1085#1090' '#1074' '#1045#1043#1040#1048#1057
      Enabled = False
      ImageIndex = 50
      Visible = False
      OnExecute = acGetTTNPopupExecute
    end
    object acGetRawTTNList: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1047#1072#1087#1088#1086#1089#1080#1090#1100' '#1085#1077#1086#1073#1088#1072#1073#1086#1090#1072#1085#1085#1099#1077' '#1058#1058#1053
      Enabled = False
      ImageIndex = 164
      Visible = False
      OnExecute = acGetRawTTNListExecute
    end
    object acDelDocIn: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1076#1086#1082#1091#1084#1077#1085#1090
      ImageIndex = 2
      OnExecute = acDelDocInExecute
    end
    object acHistoryDocIn: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1048#1089#1090#1086#1088#1080#1103' '#1087#1086' '#1076#1086#1082#1091#1084#1077#1085#1090#1091
      ImageIndex = 8
      OnExecute = acHistoryDocInExecute
    end
    object acInEdit: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1055#1077#1088#1077#1074#1077#1089#1090#1080' '#1076#1086#1082#1091#1084#1077#1085#1090' '#1074' '#1088#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1085#1080#1077
      ImageIndex = 74
      OnExecute = acInEditExecute
    end
    object acInEditRet: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1055#1077#1088#1077#1074#1077#1089#1090#1080' '#1074#1086#1079#1074#1088#1072#1090#1085#1099#1081' '#1076#1086#1082#1091#1084#1077#1085#1090' '#1074' '#1088#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1085#1080#1077
      ImageIndex = 74
      OnExecute = acInEditRetExecute
    end
    object acUTMArh: TAction
      Category = #1060#1086#1088#1084#1072
      Caption = #1059#1058#1052' '#1072#1088#1093#1080#1074
      ImageIndex = 21
    end
    object acCreateFileSale: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = 'acCreateFileSale'
      ShortCut = 24659
      OnExecute = acCreateFileSaleExecute
    end
    object acQueryFormF1: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1047#1072#1087#1088#1086#1089' '#1089#1087#1088#1072#1074#1082#1080' '#1040
      ImageIndex = 164
      OnExecute = acQueryFormF1Execute
    end
    object acQueryRests: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1047#1072#1087#1088#1086#1089' '#1086#1089#1090#1072#1090#1082#1086#1074
      ImageIndex = 164
      OnExecute = acQueryRestsExecute
    end
    object acQueryBarcode: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1047#1072#1087#1088#1086#1089' '#1085#1077#1095#1080#1090#1072#1077#1084#1099#1093' '#1096#1090#1088#1080#1093#1082#1086#1076#1086#1074
      ImageIndex = 34
      ShortCut = 49218
    end
    object acCBDoc: TAction
      Category = #1060#1086#1088#1084#1072
      Caption = 'MCrystal'
      ImageIndex = 116
      OnExecute = acCBDocExecute
    end
    object acCreateDocVn: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1057#1086#1079#1076#1072#1090#1100' '#1087#1077#1088#1077#1084#1077#1097#1077#1085#1080#1077' '#1085#1072' '#1084#1072#1075#1072#1079#1080#1085' '#1087#1086' '#1074#1099#1076'.'
      ImageIndex = 13
      OnExecute = acCreateDocVnExecute
    end
    object acCompare1: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1057#1086#1087#1086#1089#1090#1072#1074#1080#1090#1100' '#1074#1099#1076'. '#1076#1086#1082#1091#1084#1077#1085#1090#1099' '#1073#1077#1079' '#1080#1079#1084#1077#1085#1077#1080#1081' '#1089#1090#1072#1090#1091#1089#1072
      ImageIndex = 92
      ShortCut = 16503
      OnExecute = acCompare1Execute
    end
  end
  object pmDocs: TPopupMenu
    Images = dmR.SmallImage
    Left = 712
    Top = 272
    object N13: TMenuItem
      Action = acHistoryDocIn
    end
    object N1: TMenuItem
      Action = acCompare
    end
    object N18: TMenuItem
      Action = acCompareHand
    end
    object N21: TMenuItem
      Action = acCompareHand2
    end
    object N16: TMenuItem
      Action = acUnCompare
    end
    object N9: TMenuItem
      Action = acCompare1
    end
    object N19: TMenuItem
      Caption = '-'
    end
    object N2: TMenuItem
      Action = acSendStatus
    end
    object N4: TMenuItem
      Action = acCreateReturn
    end
    object N7: TMenuItem
      Caption = '-'
    end
    object N5: TMenuItem
      Action = acSendActR
    end
    object N11: TMenuItem
      Action = acSendActR_HK
    end
    object N8: TMenuItem
      Action = acCreateDocVn
    end
    object N6: TMenuItem
      Action = acSendActPrih
    end
    object N14: TMenuItem
      Action = acInEdit
    end
    object N12: TMenuItem
      Action = acDelDocIn
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object Excel1: TMenuItem
      Action = acExpExcel
    end
  end
  object quSpecIn1: TFDQuery
    CachedUpdates = True
    SQL.Strings = (
      
        'SELECT sp.[FSRARID],sp.[IDATE],sp.[SIDHD],sp.[ID],sp.[NUM],sp.[A' +
        'LCCODE],sp.[QUANT],sp.[PRICE],sp.[QUANTF],sp.[PRICEF],sp.[PRISEF' +
        '0]'
      '  FROM [dbo].[ADOCSIN_SP] sp'
      '  where sp.[FSRARID]=:RARID'
      '  and sp.[IDATE]=:IDATE'
      '  and sp.[SIDHD]=:SID')
    Left = 432
    Top = 360
    ParamData = <
      item
        Name = 'RARID'
        DataType = ftString
        ParamType = ptInput
        Value = '020000021353'
      end
      item
        Name = 'IDATE'
        DataType = ftInteger
        ParamType = ptInput
        Value = 42333
      end
      item
        Name = 'SID'
        DataType = ftString
        ParamType = ptInput
        Value = '166178000003500451'
      end>
    object quSpecIn1FSRARID: TStringField
      FieldName = 'FSRARID'
      Origin = 'FSRARID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 50
    end
    object quSpecIn1IDATE: TIntegerField
      FieldName = 'IDATE'
      Origin = 'IDATE'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quSpecIn1SIDHD: TStringField
      FieldName = 'SIDHD'
      Origin = 'SIDHD'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 50
    end
    object quSpecIn1ID: TLargeintField
      AutoGenerateValue = arAutoInc
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object quSpecIn1NUM: TIntegerField
      FieldName = 'NUM'
      Origin = 'NUM'
    end
    object quSpecIn1ALCCODE: TStringField
      FieldName = 'ALCCODE'
      Origin = 'ALCCODE'
      Size = 50
    end
    object quSpecIn1QUANT: TFloatField
      FieldName = 'QUANT'
      Origin = 'QUANT'
    end
    object quSpecIn1PRICE: TFloatField
      FieldName = 'PRICE'
      Origin = 'PRICE'
    end
    object quSpecIn1QUANTF: TFloatField
      FieldName = 'QUANTF'
      Origin = 'QUANTF'
    end
    object quSpecIn1PRICEF: TFloatField
      FieldName = 'PRICEF'
      Origin = 'PRICEF'
    end
    object quSpecIn1PRISEF0: TFloatField
      FieldName = 'PRISEF0'
      Origin = 'PRISEF0'
    end
  end
  object pmNATTN: TPopupMenu
    Images = dmR.SmallImage
    Left = 856
    Top = 272
    object N17: TMenuItem
      Action = acGetTTNPopup
    end
  end
end
