unit DocDeclSP;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  CustomChildForm, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer,
  cxTextEdit, cxCalendar, cxCheckBox, cxDBLookupComboBox, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error,
  FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Vcl.Menus, cxClasses, FireDAC.Comp.Client,
  FireDAC.Comp.DataSet, System.Actions, Vcl.ActnList, dxBar, cxBarEditItem, cxMemo, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  Excel2000, OleServer, ExcelXP, ComObj,
  cxGridDBTableView, cxGridCustomView, cxGrid, dxRibbon, cxDropDownEdit, cxCalc, dxBarExtItems;

type
  TfmDocSpecDecl = class(TfmCustomChildForm)
    dxBarManager1: TdxBarManager;
    dxBarGroup1: TdxBarGroup;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    dxBarManager1Bar1: TdxBar;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    ActionList1: TActionList;
    acClose: TAction;
    acSave: TAction;
    quSpecOb: TFDQuery;
    UpdSQL1: TFDUpdateSQL;
    dsquSpecOb: TDataSource;
    ViewSpecDecl: TcxGridDBTableView;
    LevelSpecDecl: TcxGridLevel;
    GridSpecDecl: TcxGrid;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    PopupMenu1: TPopupMenu;
    cxStyle2: TcxStyle;
    quE: TFDQuery;
    acTestSpec: TAction;
    acDelPos: TAction;
    dxBarManager1Bar2: TdxBar;
    acGetInfoSel: TAction;
    acAddPos: TAction;
    LookupComboBox1: TcxBarEditItem;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    LevelSpecDecl1: TcxGridLevel;
    ViewSpecDecl1: TcxGridDBTableView;
    quS: TFDQuery;
    SpinEdit1: TdxBarSpinEdit;
    Combo2: TdxBarCombo;
    DateEdit1: TdxBarDateCombo;
    DateEdit2: TdxBarDateCombo;
    Combo1: TdxBarCombo;
    quOrg: TFDQuery;
    dsquOrg: TDataSource;
    ViewSpecDeclAVID: TcxGridDBColumn;
    ViewSpecDeclNAMEV: TcxGridDBColumn;
    ViewSpecDeclNAME: TcxGridDBColumn;
    ViewSpecDeclPRODINN: TcxGridDBColumn;
    ViewSpecDeclPRODKPP: TcxGridDBColumn;
    ViewSpecDeclREMNB: TcxGridDBColumn;
    ViewSpecDeclQIN1: TcxGridDBColumn;
    ViewSpecDeclQIN2: TcxGridDBColumn;
    ViewSpecDeclQIN3: TcxGridDBColumn;
    ViewSpecDeclQUANT_IN: TcxGridDBColumn;
    ViewSpecDeclQIN4: TcxGridDBColumn;
    ViewSpecDeclQIN5: TcxGridDBColumn;
    ViewSpecDeclQIN6: TcxGridDBColumn;
    ViewSpecDeclQUANT_IN_ITOG: TcxGridDBColumn;
    ViewSpecDeclQOUT1: TcxGridDBColumn;
    ViewSpecDeclQOUT2: TcxGridDBColumn;
    ViewSpecDeclQOUT3: TcxGridDBColumn;
    ViewSpecDeclQOUT4: TcxGridDBColumn;
    ViewSpecDeclQUANT_OUT_ITOG: TcxGridDBColumn;
    ViewSpecDeclREMNE: TcxGridDBColumn;
    ViewSpecDeclPRODID: TcxGridDBColumn;
    quSpecIn: TFDQuery;
    dsquSpecIn: TDataSource;
    quSpecInIDHEAD: TLargeintField;
    quSpecInID: TLargeintField;
    quSpecInAVID: TIntegerField;
    quSpecInNAMEV: TStringField;
    quSpecInPRODID: TStringField;
    quSpecInNAMEP: TMemoField;
    quSpecInPRODINN: TStringField;
    quSpecInPRODKPP: TStringField;
    quSpecInCLIENTID: TStringField;
    quSpecInNAMECLI: TMemoField;
    quSpecInCLIENTINN: TStringField;
    quSpecInCLIENTKPP: TStringField;
    quSpecInDOCDATE: TSQLTimeStampField;
    quSpecInDOCNUM: TStringField;
    quSpecInGTD: TStringField;
    quSpecInQIN: TSingleField;
    ViewSpecDecl1AVID: TcxGridDBColumn;
    ViewSpecDecl1NAMEV: TcxGridDBColumn;
    ViewSpecDecl1PRODID: TcxGridDBColumn;
    ViewSpecDecl1NAMEP: TcxGridDBColumn;
    ViewSpecDecl1PRODINN: TcxGridDBColumn;
    ViewSpecDecl1PRODKPP: TcxGridDBColumn;
    ViewSpecDecl1CLIENTID: TcxGridDBColumn;
    ViewSpecDecl1NAMECLI: TcxGridDBColumn;
    ViewSpecDecl1CLIENTINN: TcxGridDBColumn;
    ViewSpecDecl1CLIENTKPP: TcxGridDBColumn;
    ViewSpecDecl1LICCODE: TcxGridDBColumn;
    ViewSpecDecl1LICSERNUM: TcxGridDBColumn;
    ViewSpecDecl1LICDATEB: TcxGridDBColumn;
    ViewSpecDecl1LICDATEE: TcxGridDBColumn;
    ViewSpecDecl1LICORGAN: TcxGridDBColumn;
    ViewSpecDecl1DOCDATE: TcxGridDBColumn;
    ViewSpecDecl1DOCNUM: TcxGridDBColumn;
    ViewSpecDecl1GTD: TcxGridDBColumn;
    ViewSpecDecl1QIN: TcxGridDBColumn;
    UpdSQL2: TFDUpdateSQL;
    acInputRemn: TAction;
    N1: TMenuItem;
    quSpecObIDHEAD: TLargeintField;
    quSpecObID: TLargeintField;
    quSpecObAVID: TIntegerField;
    quSpecObNAMEV: TStringField;
    quSpecObPRODID: TStringField;
    quSpecObNAME: TMemoField;
    quSpecObPRODINN: TStringField;
    quSpecObPRODKPP: TStringField;
    quSpecObREMNB: TSingleField;
    quSpecObQIN1: TSingleField;
    quSpecObQIN2: TSingleField;
    quSpecObQIN3: TSingleField;
    quSpecObQIN4: TSingleField;
    quSpecObQIN5: TSingleField;
    quSpecObQIN6: TSingleField;
    quSpecObQOUT1: TSingleField;
    quSpecObQOUT2: TSingleField;
    quSpecObQOUT3: TSingleField;
    quSpecObQOUT4: TSingleField;
    quSpecObREMNE: TSingleField;
    quSpecObQUANT_IN: TFloatField;
    quSpecObQUANT_IN_ITOG: TFloatField;
    quSpecObQUANT_OUT_ITOG: TFloatField;
    quSpecInLICCODE: TIntegerField;
    quSpecInLICSERNUM: TStringField;
    quSpecInLICDATEB: TSQLTimeStampField;
    quSpecInLICDATEE: TSQLTimeStampField;
    quSpecInLICORGAN: TStringField;
    quOrgID: TIntegerField;
    quOrgMAIN: TIntegerField;
    quOrgFSRARID: TStringField;
    quOrgNAME: TStringField;
    quOrgPHONE: TStringField;
    quOrgEMAILORG: TStringField;
    quOrgINN: TStringField;
    quOrgKPP: TStringField;
    quOrgDIR1: TStringField;
    quOrgDIR2: TStringField;
    quOrgDIR3: TStringField;
    quOrgGB1: TStringField;
    quOrgGB2: TStringField;
    quOrgGB3: TStringField;
    quOrgADDR_CC: TStringField;
    quOrgADDR_IND: TStringField;
    quOrgADDR_REG: TStringField;
    quOrgADDR_RN: TStringField;
    quOrgADDR_CITY: TStringField;
    quOrgADDR_NP: TStringField;
    quOrgADDR_STR: TStringField;
    quOrgADDR_D: TStringField;
    quOrgADDR_KORP: TStringField;
    quOrgADDR_L: TStringField;
    quOrgADDR_KV: TStringField;
    ViewSpecDeclIDORG: TcxGridDBColumn;
    ViewSpecDeclNAMEORG: TcxGridDBColumn;
    quSpecObIDORG: TIntegerField;
    quSpecObNAMEORG: TStringField;
    quSpecInIDORG: TIntegerField;
    quSpecInNAMEORG: TStringField;
    ViewSpecDecl1IDORG: TcxGridDBColumn;
    ViewSpecDecl1NAMEORG: TcxGridDBColumn;
    procedure acCloseExecute(Sender: TObject);
    procedure acSaveExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acDelPosExecute(Sender: TObject);
    procedure acAddPosExecute(Sender: TObject);
    procedure acInputRemnExecute(Sender: TObject);
    procedure quSpecObCalcFields(DataSet: TDataSet);
    procedure quSpecObBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
    FRARID:string;
//    FIDATE: Integer;
//    FSID:string;
    FIEDIT: Integer;
//    FNUMBER:string;
//    FDESCR:string;
    FIADD:Integer;
    FIDH:Integer;

    procedure Init;

  public
    { Public declarations }
  end;

// var
//  fmDocSpec: TfmDocSpec;

procedure ShowSpecViewADecl(IDH,IEDIT,IADD:Integer);

implementation

{$R *.dfm}

uses ViewRar, dmRar, EgaisDecode, ViewDocCorr, AddPosCorrSpec, ViewCards, DocDeclHD, InputFromFile;

procedure ShowSpecViewADecl(IDH,IEDIT,IADD:Integer);
var F:TfmDocSpecDecl;
begin
  F:=TfmDocSpecDecl.Create(Application);
  F.FRARID:='';
  F.FIEDIT:=IEDIT;
  F.FIADD:=IADD;
  F.FIDH:=IDH;

  F.Init;
  F.Show;
end;

procedure TfmDocSpecDecl.acAddPosExecute(Sender: TObject);
begin
  //�������� �������
  //�������� �������
  if FIEDIT=1 then
  begin
    {
    //�������� �������
    if Assigned(fmACard)=False then
    begin //����� �� ����������, � ����� �������
      fmACard:=TfmACard.Create(Application);
      fmACard.Init;
      fmACard.FRARID:=FRARID;
      fmACard.FIDATE:=FIDATE;
      fmACard.FSID:=FSID;
      fmACard.STYPE:='CORR';
      fmACard.quSpec:=quSpecCorr;
      fmACard.Show;
    end else
    begin
      fmACard.FRARID:=FRARID;
      fmACard.FIDATE:=FIDATE;
      fmACard.FSID:=FSID;
      fmACard.STYPE:='CORR';
      fmACard.quSpec:=quSpecCorr;
      fmACard.Show;
    end;
    }
    {
    if not Assigned(fmAddPosCorr) then fmAddPosCorr:=TfmAddPosCorr.Create(Application,TFormStyle.fsNormal);

    fmAddPosCorr.ShowModal;
    if fmAddPosCorr.ModalResult=mrOk then
    begin //�������� �������
      with fmAddPosCorr do
      with dmR do
      begin
        quSelCard.Active:=False;
        quSelCard.ParamByName('CODE').AsString:=fmAddPosCorr.cxButtonEdit1.Text;
        quSelCard.Active:=True;

        if quSelCard.RecordCount=1 then
        begin
        end;

        quSelCard.Active:=False;
      end;
    end;}
  end;

end;

procedure TfmDocSpecDecl.acCloseExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmDocSpecDecl.acDelPosExecute(Sender: TObject);
begin
  if FIEDIT=1 then
  begin
//    quSpecCorr.Delete;
  end;
end;

procedure TfmDocSpecDecl.acInputRemnExecute(Sender: TObject);
var NameF:string;
    ExcelApp, Workbook, ISheet: Variant;
    i,AVid:Integer;
    PCode,PName,PInn,PKpp,AVidName:string;
    bRemn:Real;
    bAdd:Boolean;
begin
  //������� ������� �� ������
  if FIEDIT=1 then
  begin
    NameF:='';
    fmInputFile:=tfmInputFile.Create(Application);
    fmInputFile.cxButtonEdit1.Text:='';
    fmInputFile.ShowModal;
    if fmInputFile.ModalResult=mrOk then NameF:=fmInputFile.cxButtonEdit1.Text;
    fmInputFile.Free;

    if NameF>'' then
    begin
      ClearMessageLogLocal;
      ShowMessageLogLocal('�����... ���� ���������.');
      if FileExists(NameF) then
      begin
        ShowMessageLogLocal('  ���� - '+NameF+'  ������.');
        ShowMessageLogLocal('  ������ ������..');

        try
          ViewSpecDecl.BeginUpdate;

          ExcelApp := CreateOleObject('Excel.Application');
          ExcelApp.Application.EnableEvents := false;
          Workbook := ExcelApp.WorkBooks.Add(NameF);
          ISheet := Workbook.Worksheets.Item[1];

          i:=2;
          While String(ISheet.Cells.Item[i, 1].Value)<>'' do
          begin
//            ShowMessageLogLocal('  '+String(ISheet.Cells.Item[i, 1].Value)+'  '+String(ISheet.Cells.Item[i, 3].Value)+'  '+String(ISheet.Cells.Item[i, 4].Value)+' '+String(ISheet.Cells.Item[i, 5].Value)+' '+String(ISheet.Cells.Item[i, 6].Value)+' '+String(ISheet.Cells.Item[i, 7].Value));
            AVid:=StrToIntDef(String(ISheet.Cells.Item[i,1].Value),0);
            PCode:=String(ISheet.Cells.Item[i,4].Value);

            if PCode>'0' then  //���-�� ����
              while Length(PCode)<12 do PCode:='0'+PCode;

            PName:=String(ISheet.Cells.Item[i,3].Value);
            PInn:=String(ISheet.Cells.Item[i,5].Value);
            PKpp:=String(ISheet.Cells.Item[i,6].Value);
            bRemn:=StrToFloatDef(String(ISheet.Cells.Item[i,7].Value),0);

            if AVid>0 then
            begin
              if Trim(PCode)>'' then  //��� ������������� ��������� � ��� ���������
              begin
                //������� �������� ���� � �������� ������������� �� ����
                prGetParProd(PCode,PName,PInn,PKpp);
              end else
              begin
                prGetParProd1(PInn,PKpp,PCode,PName);
              end;
              if (PCode>'') and (PName>'') then
              begin
                //��� ������� �������������
                AVidName:=prGetVidName(AVid);

                bAdd:=True;

                quSpecOb.First;
                while not quSpecOb.Eof do
                begin
                  if (quSpecObAVID.AsInteger=AVid) and (quSpecObPRODID.AsString=Trim(PCode)) then
                  begin
                    quSpecOb.Edit;
                    quSpecObREMNB.AsFloat:=bRemn;
                    quSpecOb.Post;

                    bAdd:=False; //�����
                    Break;
                  end;
                  quSpecOb.Next;
                end;

                if bAdd then //�� ����� - ����� ��������� ������
                begin
                  quSpecOb.Append;
                  quSpecObIDHEAD.AsInteger:=FIDH;
                  quSpecObAVID.AsInteger:=AVid;
                  quSpecObNAMEV.AsString:=AVidName;
                  quSpecObPRODID.AsString:=PCode;
                  quSpecObNAME.AsString:=PName;
                  quSpecObPRODINN.AsString:=PInn;
                  quSpecObPRODKPP.AsString:=PKpp;
                  quSpecObREMNB.AsFloat:=bRemn;
                  quSpecObQIN1.AsFloat:=0;
                  quSpecObQIN2.AsFloat:=0;
                  quSpecObQIN3.AsFloat:=0;
                  quSpecObQIN4.AsFloat:=0;
                  quSpecObQIN5.AsFloat:=0;
                  quSpecObQIN6.AsFloat:=0;
                  quSpecObQOUT1.AsFloat:=0;
                  quSpecObQOUT2.AsFloat:=0;
                  quSpecObQOUT3.AsFloat:=0;
                  quSpecObQOUT4.AsFloat:=0;
                  quSpecObREMNE.AsFloat:=bRemn;
                  quSpecOb.Post;
                end;
              end else ShowMessageLogLocal('  ������. ������������� � ����������� �� ������ ('+PName+','+PInn+','+PKpp+')');
            end;
            inc(i);
          end;
        finally
          ViewSpecDecl.EndUpdate;
          ExcelApp.Quit;
          ExcelApp:=Unassigned;
        end;
      end;
      ShowMessageLogLocal('������� ��������.');
    end;
  end;
end;

procedure TfmDocSpecDecl.acSaveExecute(Sender: TObject);
var iErr1,iErr2:Integer;
begin
  //��������� ������������
  if FIADD=1 then
  begin
    try
      with dmR do
      begin
        quS.Active:=False;
        quS.SQL.Clear;
        quS.SQL.Add('');
        quS.SQL.Add('declare @MASTERORG int = '+LookupComboBox1.EditValue);
        quS.SQL.Add('declare @IDH int = '+its(FIDH));
        quS.SQL.Add('declare @IDATEB int = '+its(Trunc(DateEdit1.Date)));
        quS.SQL.Add('declare @IDATEE int = '+its(Trunc(DateEdit2.Date)));
        quS.SQL.Add('declare @DDATEB datetime = '''+dssql(DateEdit1.Date)+'''');
        quS.SQL.Add('declare @DDATEE datetime = '''+dssql(DateEdit2.Date)+'''');
        quS.SQL.Add('declare @IT1 smallint = '+its(Combo1.ItemIndex));
        quS.SQL.Add('declare @IT2 smallint  ='+its(Combo2.ItemIndex));
        quS.SQL.Add('declare @INUMCORR smallint = '+its(SpinEdit1.IntValue));
        quS.SQL.Add('INSERT INTO [dbo].[ADECL_HD] ([MASTERORG],[ID],[IDATEB],[IDATEE],[DDATEB],[DDATEE],[IACTIVE],[IT1],[IT2],[INUMCORR])');
        quS.SQL.Add('VALUES (@MASTERORG,@IDH,@IDATEB,@IDATEE,@DDATEB,@DDATEE,0,@IT1,@IT2,@INUMCORR)');
        quS.ExecSQL;

        FIADD:=0;
      end;
    except
    end;
  end;

  if FIADD=0 then
  begin
    quSpecOb.UpdateTransaction.StartTransaction;
    iErr1 := quSpecOb.ApplyUpdates;
    if iErr1 = 0 then begin
      try
        quSpecOb.CommitUpdates;
        quSpecOb.UpdateTransaction.Commit;
      except
        ShowMessage('������ ����������..');
      end;
    end else
    begin
      ShowMessage('������ ���������� (�������).');
      quSpecOb.UpdateTransaction.Rollback;
    end;

    quSpecIn.UpdateTransaction.StartTransaction;
    iErr2 := quSpecIn.ApplyUpdates;
    if iErr2 = 0 then begin
      try
        quSpecIn.CommitUpdates;
        quSpecIn.UpdateTransaction.Commit;
      except
        ShowMessage('������ ����������..');
      end;
    end else
    begin
      ShowMessage('������ ���������� (�������).');
      quSpecIn.UpdateTransaction.Rollback;
    end;

    if (iErr1=0) and (iErr2=0) then
    begin
      quE.Active:=False;
      quE.SQL.Clear;
      quE.SQL.Add('UPDATE [dbo].[ADECL_HD]');
      quE.SQL.Add('      SET [IDATEB] = '+its(Trunc(DateEdit1.Date)));
      quE.SQL.Add('      ,[IDATEE] = '+its(Trunc(DateEdit2.Date)));
      quE.SQL.Add('      ,[DDATEB] = '''+dssql(DateEdit1.Date)+'''');
      quE.SQL.Add('      ,[DDATEE] = '''+dssql(DateEdit2.Date)+'''');
      quE.SQL.Add('      ,[IT1] = '+its(Combo1.ItemIndex));
      quE.SQL.Add('      ,[IT2] = '+its(Combo2.ItemIndex));
      quE.SQL.Add('      ,[INUMCORR] = '+its(SpinEdit1.IntValue));
      quE.SQL.Add('      ,[MASTERORG] = '+its(LookupComboBox1.EditValue));
      quE.SQL.Add(' WHERE [ID] = '+its(FIDH));
      quE.ExecSQL;

      if Assigned(fmDocDecl) then
        if fmDocDecl.Showing then fmDocDecl.Init;
    end;
  end;
end;

procedure TfmDocSpecDecl.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  quOrg.Active:=False;
  Action:=caFree;
end;

procedure TfmDocSpecDecl.Init;
var iDateB,iDateE:Integer;
begin
  quOrg.Active:=False;
  quOrg.Active:=True;
  quOrg.First;

  LookupComboBox1.EditValue:=quOrgID.AsString;

  if FIADD=1 then
  begin
    Caption:='���������� ����������� ����������.';

    DateEdit1.Date:=Date;
    DateEdit2.Date:=Date;
    Combo1.ItemIndex:=0;
    Combo2.ItemIndex:=0;
    SpinEdit1.IntValue:=0;
  end;

  if FIADD=0 then
  begin
    Caption:='������������ ��������� ����������� ���������� ('+its(FIDH)+')';

    quS.Active:=False;
    quS.SQL.Clear;
    quS.SQL.Add('');
    quS.SQL.Add('SELECT TOP 1 [MASTERORG],[ID],[IDATEB],[IDATEE],[DDATEB],[DDATEE],[IACTIVE],[IT1],[IT2],[INUMCORR]');
    quS.SQL.Add('  FROM [dbo].[ADECL_HD]');
    quS.SQL.Add('  where [ID]='+its(FIDH));
    quS.Active:=True;

    LookupComboBox1.EditValue:=quS.FieldByName('MASTERORG').AsINteger;

    DateEdit1.Date:=quS.FieldByName('DDATEB').AsDateTime;
    DateEdit2.Date:=quS.FieldByName('DDATEE').AsDateTime;

    Combo1.ItemIndex:=quS.FieldByName('IT1').AsInteger;
    Combo2.ItemIndex:=quS.FieldByName('IT2').AsInteger;
    SpinEdit1.IntValue:=quS.FieldByName('INUMCORR').AsInteger;

    iDateB:=quS.FieldByName('IDATEB').AsInteger;
    iDateE:=quS.FieldByName('IDATEE').AsInteger;

    quS.Active:=False;
  end;


  ViewSpecDecl.BeginUpdate;
  quSpecOb.Active:=False;
  quSpecOb.ParamByName('IDH').AsInteger:=FIDH;
  quSpecOb.Active:=True;
  ViewSpecDecl.EndUpdate;

  ViewSpecDecl1.BeginUpdate;
  quSpecIn.Active:=False;
  quSpecIn.ParamByName('IDATEB').AsInteger:=iDateB;
  quSpecIn.ParamByName('IDATEE').AsInteger:=iDateE;
  quSpecIn.ParamByName('IDH').AsInteger:=FIDH;
  quSpecIn.Active:=True;
  ViewSpecDecl1.EndUpdate;

  if FIEDIT=1 then
  begin
    acSave.Enabled:=True;
    ViewSpecDecl.OptionsData.Editing:=True;
    ViewSpecDecl1.OptionsData.Editing:=True;
  end else
  begin
    acSave.Enabled:=False;
    acInputRemn.Enabled:=False;

    ViewSpecDecl.OptionsData.Editing:=False;
    ViewSpecDecl1.OptionsData.Editing:=False;

//    ViewSpecCorrQUANT.Options.Editing:=False;
//    cxBarEditItem1.Properties.ReadOnly:=True;
  end;
end;

procedure TfmDocSpecDecl.quSpecObBeforePost(DataSet: TDataSet);
begin
  quSpecObREMNE.AsFloat:=quSpecObREMNB.AsFloat+quSpecObQIN1.AsFloat+quSpecObQIN2.AsFloat+quSpecObQIN3.AsFloat+quSpecObQIN4.AsFloat+quSpecObQIN5.AsFloat+quSpecObQIN6.AsFloat-(quSpecObQOUT1.AsFloat+quSpecObQOUT2.AsFloat+quSpecObQOUT3.AsFloat+quSpecObQOUT4.AsFloat);
end;

procedure TfmDocSpecDecl.quSpecObCalcFields(DataSet: TDataSet);
begin
  quSpecObQUANT_IN.AsFloat:=quSpecObQIN1.AsFloat+quSpecObQIN2.AsFloat+quSpecObQIN3.AsFloat;
  quSpecObQUANT_IN_ITOG.AsFloat:=quSpecObQIN1.AsFloat+quSpecObQIN2.AsFloat+quSpecObQIN3.AsFloat+quSpecObQIN4.AsFloat+quSpecObQIN5.AsFloat+quSpecObQIN6.AsFloat;
  quSpecObQUANT_OUT_ITOG.AsFloat:=quSpecObQOUT1.AsFloat+quSpecObQOUT2.AsFloat+quSpecObQOUT3.AsFloat+quSpecObQOUT4.AsFloat;
end;

end.
