object dmRA: TdmRA
  OldCreateOrder = False
  Height = 450
  Width = 827
  object FDConnection: TFDConnection
    Params.Strings = (
      'Server=192.168.0.72'
      'User_Name=sa'
      'Password=YKS15Le'
      'ApplicationName=ViewRarCorp'
      'Database=RAR'
      'MARS=yes'
      'DriverID=MSSQL')
    FetchOptions.AssignedValues = [evMode, evRowsetSize]
    FetchOptions.Mode = fmAll
    FetchOptions.RowsetSize = 10000
    ResourceOptions.AssignedValues = [rvServerOutput, rvAutoReconnect]
    ResourceOptions.ServerOutput = True
    ResourceOptions.AutoReconnect = True
    ConnectedStoredUsage = []
    LoginPrompt = False
    Transaction = FDTrans
    UpdateTransaction = FDTransUpdate
    Left = 28
    Top = 12
  end
  object FDTrans: TFDTransaction
    Connection = FDConnection
    Left = 100
    Top = 12
  end
  object FDPhysMSSQLDriverLink: TFDPhysMSSQLDriverLink
    Left = 284
    Top = 12
  end
  object FDTransUpdate: TFDTransaction
    Connection = FDConnection
    Left = 172
    Top = 12
  end
  object FDGUIxWaitCursor: TFDGUIxWaitCursor
    Provider = 'Forms'
    Left = 408
    Top = 12
  end
  object quPoint: TFDQuery
    Connection = FDConnection
    SQL.Strings = (
      'SELECT [RARID]'
      '      ,[ISHOP]'
      '      ,[IPUTM]'
      '      ,[IPDB]'
      '      ,[DBNAME]'
      '      ,[PASSW]'
      '      ,[INN]'
      '      ,[KPP]'
      '      ,[SHORTNAME]'
      '      ,[FULLNAME]'
      '      ,[COUNTRY]'
      '      ,[DESCR]'
      '      ,[NAMESHOP]'
      '      ,[ISS]'
      '      ,[ISPRODUCTION]'
      '      ,[SDEP]'
      '      ,[IACTIVE]'
      '      ,[WRITELOG]'
      '  FROM [RAR].[dbo].[EGAISID]'
      '  where IACTIVE=1'
      '  order by ISHOP')
    Left = 24
    Top = 72
    object quPointRARID: TStringField
      FieldName = 'RARID'
      Origin = 'RARID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 50
    end
    object quPointISHOP: TIntegerField
      FieldName = 'ISHOP'
      Origin = 'ISHOP'
    end
    object quPointIPUTM: TStringField
      FieldName = 'IPUTM'
      Origin = 'IPUTM'
      Size = 50
    end
    object quPointIPDB: TStringField
      FieldName = 'IPDB'
      Origin = 'IPDB'
      Size = 50
    end
    object quPointDBNAME: TStringField
      FieldName = 'DBNAME'
      Origin = 'DBNAME'
      Size = 50
    end
    object quPointPASSW: TStringField
      FieldName = 'PASSW'
      Origin = 'PASSW'
      Size = 50
    end
    object quPointINN: TStringField
      FieldName = 'INN'
      Origin = 'INN'
    end
    object quPointKPP: TStringField
      FieldName = 'KPP'
      Origin = 'KPP'
    end
    object quPointSHORTNAME: TStringField
      FieldName = 'SHORTNAME'
      Origin = 'SHORTNAME'
      Size = 200
    end
    object quPointFULLNAME: TStringField
      FieldName = 'FULLNAME'
      Origin = 'FULLNAME'
      Size = 250
    end
    object quPointCOUNTRY: TStringField
      FieldName = 'COUNTRY'
      Origin = 'COUNTRY'
    end
    object quPointDESCR: TStringField
      FieldName = 'DESCR'
      Origin = 'DESCR'
      Size = 500
    end
    object quPointNAMESHOP: TStringField
      FieldName = 'NAMESHOP'
      Origin = 'NAMESHOP'
      Size = 100
    end
    object quPointISS: TSmallintField
      FieldName = 'ISS'
      Origin = 'ISS'
    end
    object quPointISPRODUCTION: TSmallintField
      FieldName = 'ISPRODUCTION'
      Origin = 'ISPRODUCTION'
    end
    object quPointSDEP: TStringField
      FieldName = 'SDEP'
      Origin = 'SDEP'
      Size = 500
    end
    object quPointIACTIVE: TSmallintField
      FieldName = 'IACTIVE'
      Origin = 'IACTIVE'
    end
    object quPointWRITELOG: TSmallintField
      FieldName = 'WRITELOG'
      Origin = 'WRITELOG'
    end
  end
end
