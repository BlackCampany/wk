unit ViewRar;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  CustomChildForm, System.UITypes, ShellApi, httpsend, synautil, nativexml, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxRibbonSkins, dxRibbonCustomizationForm, cxContainer, cxEdit, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator,
  Data.DB, cxDBData, cxImageComboBox, cxTextEdit, cxDBLookupComboBox, cxCheckBox, cxCalendar, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.Menus, System.Actions, Vcl.ActnList, dxBar, cxBarEditItem, dxBarExtItems, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid, cxMemo, Vcl.ExtCtrls, dxRibbon;

type
  TfmViewRar = class(TfmCustomChildForm)
    dxRibbon1: TdxRibbon;
    dxRibbon1Tab1: TdxRibbonTab;
    dxBarManager1: TdxBarManager;
    bmApplyDate: TdxBar;
    bmClose: TdxBar;
    btnDone: TdxBarLargeButton;
    btnClose: TdxBarLargeButton;
    deDateBeg: TdxBarDateCombo;
    deDateEnd: TdxBarDateCombo;
    dxBarGroup1: TdxBarGroup;
    ActionList1: TActionList;
    acRefresh: TAction;
    acClose: TAction;
    acSelectDate: TAction;
    acDecodeTovar: TAction;
    acSaveToFile: TAction;
    acDelList: TAction;
    acDecodeTTN: TAction;
    acCards: TAction;
    acSendActR: TAction;
    pmDocs: TPopupMenu;
    N5: TMenuItem;
    acDecodeWB: TAction;
    GridRar: TcxGrid;
    ViewDocs: TcxGridDBTableView;
    ViewDocsFSRARID: TcxGridDBColumn;
    ViewDocsIDATE: TcxGridDBColumn;
    ViewDocsSID: TcxGridDBColumn;
    ViewDocsNUMBER: TcxGridDBColumn;
    ViewDocsSDATE: TcxGridDBColumn;
    ViewDocsSTYPE: TcxGridDBColumn;
    ViewDocsUNITTYPE: TcxGridDBColumn;
    ViewDocsSHIPDATE: TcxGridDBColumn;
    ViewDocsCLIFROM: TcxGridDBColumn;
    ViewDocsIACTIVE: TcxGridDBColumn;
    ViewDocsNAME: TcxGridDBColumn;
    ViewDocsCLIENTINN: TcxGridDBColumn;
    ViewDocsCLIENTKPP: TcxGridDBColumn;
    ViewDocsFULLNAME: TcxGridDBColumn;
    ViewDocsWBREGID: TcxGridDBColumn;
    ViewDocsFIXNUMBER: TcxGridDBColumn;
    ViewDocsFIXDATE: TcxGridDBColumn;
    LevelDocs: TcxGridLevel;
    quSpecIn1: TFDQuery;
    quSpecIn1FSRARID: TStringField;
    quSpecIn1IDATE: TIntegerField;
    quSpecIn1SIDHD: TStringField;
    quSpecIn1ID: TLargeintField;
    quSpecIn1NUM: TIntegerField;
    quSpecIn1ALCCODE: TStringField;
    quSpecIn1QUANT: TFloatField;
    quSpecIn1PRICE: TFloatField;
    quSpecIn1QUANTF: TFloatField;
    quSpecIn1PRICEF: TFloatField;
    quSpecIn1PRISEF0: TFloatField;
    acUTMExch: TAction;
    dxBarLargeButton2: TdxBarLargeButton;
    acUTMProc: TAction;
    dxBarLargeButton3: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarLargeButton4: TdxBarLargeButton;
    acProd: TAction;
    acSendActPrih: TAction;
    N6: TMenuItem;
    N7: TMenuItem;
    acCompare: TAction;
    N1: TMenuItem;
    ViewDocsCOMPARE_STATUS: TcxGridDBColumn;
    ViewDocsCOMPARE_COMMENT: TcxGridDBColumn;
    ViewDocsREADYSEND: TcxGridDBColumn;
    ViewDocsCOMPARE_QUANT_STATUS: TcxGridDBColumn;
    ViewDocsDATE_INPUT: TcxGridDBColumn;
    ViewDocsDATE_OUTPUT: TcxGridDBColumn;
    ViewDocsXMLFILE: TcxGridDBColumn;
    dxBarLargeButton7: TdxBarLargeButton;
    acSendStatus: TAction;
    N2: TMenuItem;
    acSendActR2: TAction;
    acAuto: TAction;
    beiPoint: TcxBarEditItem;
    acExpExcel: TAction;
    N3: TMenuItem;
    Excel1: TMenuItem;
    acCreateReturn: TAction;
    N4: TMenuItem;
    LevelDocsOut: TcxGridLevel;
    ViewDocsOut: TcxGridDBTableView;
    ViewDocsOutFSRARID: TcxGridDBColumn;
    ViewDocsOutIDATE: TcxGridDBColumn;
    ViewDocsOutSID: TcxGridDBColumn;
    ViewDocsOutNUMBER: TcxGridDBColumn;
    ViewDocsOutCLIFTO: TcxGridDBColumn;
    ViewDocsOutIACTIVE: TcxGridDBColumn;
    ViewDocsOutREADYSEND: TcxGridDBColumn;
    ViewDocsOutWBREGID: TcxGridDBColumn;
    ViewDocsOutFIXNUMBER: TcxGridDBColumn;
    ViewDocsOutFIXDATE: TcxGridDBColumn;
    ViewDocsOutIDHEAD: TcxGridDBColumn;
    ViewDocsOutDATE_OUTPUT: TcxGridDBColumn;
    ViewDocsOutXMLF: TcxGridDBColumn;
    ViewDocsOutFULLNAME: TcxGridDBColumn;
    ViewDocsOutNAME: TcxGridDBColumn;
    ViewDocsOutCLIENTINN: TcxGridDBColumn;
    ViewDocsOutCLIENTKPP: TcxGridDBColumn;
    ViewDocsOutDDATE: TcxGridDBColumn;
    pmDocsOut: TPopupMenu;
    N8: TMenuItem;
    Excel2: TMenuItem;
    ViewDocsIDHEAD: TcxGridDBColumn;
    acSendRet: TAction;
    acMCrystalDocs: TAction;
    dxBarLargeButton9: TdxBarLargeButton;
    ViewDocsOutDATEDOC: TcxGridDBColumn;
    acProDocs: TAction;
    dxBarLargeButton10: TdxBarLargeButton;
    acInfoAB: TAction;
    ViewDocsTT1: TcxGridDBColumn;
    ViewDocsTT2: TcxGridDBColumn;
    acSendActR_HK: TAction;
    acGetRetDocs: TAction;
    N9: TMenuItem;
    acDelDocOut: TAction;
    N10: TMenuItem;
    N11: TMenuItem;
    ViewDocsOutTT1: TcxGridDBColumn;
    ViewDocsOutTT2: TcxGridDBColumn;
    acGetTTN: TAction;
    beiGetTTN: TcxBarEditItem;
    dxBarLargeButton14: TdxBarLargeButton;
    acDelDocIn: TAction;
    N12: TMenuItem;
    acHistoryDocIn: TAction;
    N13: TMenuItem;
    acInEdit: TAction;
    N14: TMenuItem;
    acInEditRet: TAction;
    N15: TMenuItem;
    acUTMArh: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    acCreateFileSale: TAction;
    acUnCompare: TAction;
    N16: TMenuItem;
    acGetRawTTNList: TAction;
    dxBarLargeButton16: TdxBarLargeButton;
    LevelNATTN: TcxGridLevel;
    ViewNATTN: TcxGridDBTableView;
    ViewNATTNFSRARID: TcxGridDBColumn;
    ViewNATTNDATE_REPLY: TcxGridDBColumn;
    ViewNATTNWBREGID: TcxGridDBColumn;
    ViewNATTNNUMBER: TcxGridDBColumn;
    ViewNATTNSHIPDATE: TcxGridDBColumn;
    ViewNATTNCLIFROM: TcxGridDBColumn;
    ViewNATTNIACTIVE: TcxGridDBColumn;
    ViewNATTNFULLNAME: TcxGridDBColumn;
    ViewNATTNNAME: TcxGridDBColumn;
    ViewNATTNCLIENTINN: TcxGridDBColumn;
    ViewNATTNCLIENTKPP: TcxGridDBColumn;
    pmNATTN: TPopupMenu;
    acGetTTNPopup: TAction;
    N17: TMenuItem;
    acCompareHand: TAction;
    N18: TMenuItem;
    N19: TMenuItem;
    ViewNATTNISHIPDATE: TcxGridDBColumn;
    acCompareHand2: TAction;
    N21: TMenuItem;
    N20: TMenuItem;
    N22: TMenuItem;
    acQueryFormF1: TAction;
    dxBarLargeButton1: TdxBarLargeButton;
    acQueryRests: TAction;
    dxBarLargeButton5: TdxBarLargeButton;
    acQueryBarcode: TAction;
    dxBarLargeButton6: TdxBarLargeButton;
    acCBDoc: TAction;
    dxBarLargeButton8: TdxBarLargeButton;
    procedure acRefreshExecute(Sender: TObject);
    procedure acCloseExecute(Sender: TObject);
    procedure acSelectDateExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure deDateBegChange(Sender: TObject);
    procedure ViewDocsDblClick(Sender: TObject);
    procedure acSendActRExecute(Sender: TObject);
    procedure acUTMExchExecute(Sender: TObject);
    procedure acUTMProcExecute(Sender: TObject);
    procedure acProdExecute(Sender: TObject);
    procedure acSendActPrihExecute(Sender: TObject);
    procedure acCompareExecute(Sender: TObject);
    procedure acSendStatusExecute(Sender: TObject);
    procedure acSendActR2Execute(Sender: TObject);
    procedure acSetNumberExecute(Sender: TObject);
    procedure acAutoExecute(Sender: TObject);
    procedure beiPointChange(Sender: TObject);
    procedure acExpExcelExecute(Sender: TObject);
    procedure acCreateReturnExecute(Sender: TObject);
    procedure ViewDocsOutDblClick(Sender: TObject);
    procedure GridRarFocusedViewChanged(Sender: TcxCustomGrid; APrevFocusedView,
      AFocusedView: TcxCustomGridView);
    procedure acSendRetExecute(Sender: TObject);
    procedure acMCrystalDocsExecute(Sender: TObject);
    procedure acProDocsExecute(Sender: TObject);
    procedure acInfoABExecute(Sender: TObject);
    procedure acSendActR_HKExecute(Sender: TObject);
    procedure acGetRetDocsExecute(Sender: TObject);
    procedure acDelDocOutExecute(Sender: TObject);
    procedure acGetTTNExecute(Sender: TObject);
    procedure acDelDocInExecute(Sender: TObject);
    procedure acHistoryDocInExecute(Sender: TObject);
    procedure acInEditExecute(Sender: TObject);
    procedure acInEditRetExecute(Sender: TObject);
    procedure acUTMArhExecute(Sender: TObject);
    procedure acCreateFileSaleExecute(Sender: TObject);
    procedure acUnCompareExecute(Sender: TObject);
    procedure acGetRawTTNListExecute(Sender: TObject);
    procedure acGetTTNPopupExecute(Sender: TObject);
    procedure acCompareHandExecute(Sender: TObject);
    procedure acCompareHand2Execute(Sender: TObject);
    procedure acQueryFormF1Execute(Sender: TObject);
    procedure acQueryRestsExecute(Sender: TObject);
    procedure acCBDocExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }

    procedure Init;
  end;

var
  fmViewRar: TfmViewRar;

procedure ShowFormViewRar;

implementation

{$R *.dfm}

uses ViewXML, EgaisDecode, ViewCards, dmRar, UTMExch, ViewSpecOut, MCrystDocs, ProDocs, History, UTMArh, UTMCheck, DocHeaderCB,
  ViewSpec, Main;

procedure ShowFormViewRar;
begin
  if Assigned(fmViewRar)=False then //����� �� ����������, � ����� �������
    fmViewRar:=TfmViewRar.Create(Application);
  if fmViewRar.WindowState=wsMinimized then //���� ���� ��������, �� ������������� ���
    fmViewRar.WindowState:=wsNormal;

  fmViewRar.deDateBeg.Date:=Date-7;
  fmViewRar.deDateEnd.Date:=Date;
  {
  with dmCB do
  begin
    if DBCBConnect then
    begin
      fmViewRar.ShowMessageLogLocal('�� ��');
      quPoint.Active:=False;
      quPoint.Active:=True;
      quPoint.First;
      quPoint.Locate('ISHOP',29,[]);

      fmViewRar.beiPoint.EditValue:=quPointISHOP.AsInteger;

      Delay(100);
      if dmR.FDConnection.Connected then fmViewRar.Init;
    end else
    begin
      ShowMessage('������ �������� ��.');
    end;
  end;}

  fmViewRar.Show;
end;

procedure TfmViewRar.Init;
var
  FocusedRow, TopRow: Integer;
  flag: boolean;
begin
  with dmR do
  begin
    try
      //<--Refresh � ��������������� ������� ������� � �������
      flag:=quDocInHd.Active;  //���������� ���� �� ������� �������, ��� �������������� ������� �������
      TopRow := ViewDocs.Controller.TopRowIndex;
      FocusedRow := ViewDocs.DataController.FocusedRowIndex;
      //-->
      ViewDocs.BeginUpdate;
      quDocInHd.Active:=False;
      quDocInHd.ParamByName('IDATEB').AsInteger:=Trunc(deDateBeg.Date);
      quDocInHd.ParamByName('IDATEE').AsInteger:=Trunc(deDateEnd.Date);
      quDocInHd.ParamByName('RARID').AsString:=CommonSet.FSRAR_ID;
      quDocInHd.Active:=True;
      quDocInHd.First;
    finally
      ViewDocs.EndUpdate;
      //<--��������������� �������
      if flag then begin
        ViewDocs.DataController.FocusedRowIndex := FocusedRow;
        ViewDocs.Controller.TopRowIndex := TopRow;
      end;
      //-->
    end;

    try
      //<--Refresh � ��������������� ������� ������� � �������
      flag:=quDocOutHd.Active;  //���������� ���� �� ������� �������, ��� �������������� ������� �������
      TopRow := ViewDocsOut.Controller.TopRowIndex;
      FocusedRow := ViewDocsOut.DataController.FocusedRowIndex;
      //-->
      ViewDocsOut.BeginUpdate;
      quDocOutHd.Active:=False;
      quDocOutHd.ParamByName('IDATEB').AsInteger:=Trunc(deDateBeg.Date);
      quDocOutHd.ParamByName('IDATEE').AsInteger:=Trunc(deDateEnd.Date);
      quDocOutHd.ParamByName('RARID').AsString:=CommonSet.FSRAR_ID;
      quDocOutHd.Active:=True;
      quDocOutHd.First;
    finally
      ViewDocsOut.EndUpdate;
      //<--��������������� �������
      if flag then begin
        ViewDocsOut.DataController.FocusedRowIndex := FocusedRow;
        ViewDocsOut.Controller.TopRowIndex := TopRow;
      end;
      //-->
    end;

    try
      //<--Refresh � ��������������� ������� ������� � �������
      flag:=quNATTN.Active;  //���������� ���� �� ������� �������, ��� �������������� ������� �������
      TopRow := ViewNATTN.Controller.TopRowIndex;
      FocusedRow := ViewNATTN.DataController.FocusedRowIndex;
      //-->
      ViewNATTN.BeginUpdate;
      quNATTN.Active:=False;
      quNATTN.ParamByName('IDATEB').AsInteger:=Trunc(deDateBeg.Date);
      quNATTN.ParamByName('IDATEE').AsInteger:=Trunc(deDateEnd.Date);
      quNATTN.ParamByName('RARID').AsString:=CommonSet.FSRAR_ID;
      quNATTN.Active:=True;
      quNATTN.First;

      //<--������ ��������� ����������� � �����
      quNATTN.First;
      while not quNATTN.Eof do begin
        if quNATTNTT2.AsInteger=1 then begin
          quProc.Active:=False;
          quProc.SQL.Clear;
          quProc.SQL.Add('DECLARE @FSRARID varchar(50) = '''+quNATTNFSRARID.AsString+'''');
          quProc.SQL.Add('DECLARE @WBREGID varchar(50) = '''+quNATTNWBREGID.AsString+'''');
          quProc.SQL.Add('DELETE FROM dbo.ANATTN');
          quProc.SQL.Add('WHERE FSRARID = @FSRARID');
          quProc.SQL.Add('  AND WBREGID = @WBREGID');
          quProc.ExecSQL;
        end;
        quNATTN.Next;
      end;
      //-->

      //<--���� ���� ��������, �� ���� ������ ������� �������
      quNATTN.Active:=False;
      quNATTN.ParamByName('IDATEB').AsInteger:=Trunc(deDateBeg.Date);
      quNATTN.ParamByName('IDATEE').AsInteger:=Trunc(deDateEnd.Date);
      quNATTN.ParamByName('RARID').AsString:=CommonSet.FSRAR_ID;
      quNATTN.Active:=True;
      quNATTN.First;
      //-->
    finally
      ViewNATTN.EndUpdate;
      //<--��������������� �������
      if flag then begin
        ViewNATTN.DataController.FocusedRowIndex := FocusedRow;
        ViewNATTN.Controller.TopRowIndex := TopRow;
      end;
      //-->
    end;

  end;


//  FSRAR_ID:=CommonSet.FSRAR_ID;
end;


procedure TfmViewRar.ViewDocsDblClick(Sender: TObject);
begin
  // ������������
  with dmR do
  begin
    if quDocInHd.RecordCount>0 then
    begin
      if (ViewDocs.Controller.FocusedColumn.Name='ViewDocsXMLFILE') then
      begin
        quDocInRec.Active:=False;
        quDocInRec.ParamByName('RARID').AsString:=quDocInHdFSRARID.AsString;
        quDocInRec.ParamByName('IDATE').AsInteger:=quDocInHdIDATE.AsInteger;
        quDocInRec.ParamByName('SID').AsString:=quDocInHdSID.AsString;
        quDocInRec.Active:=True;
        if quDocInRec.RecordCount>0 then
        begin
          ShowXMLView(quDocInRecIDHEAD.AsInteger,quDocInRecSENDXML.AsString);
        end;
        Exit;
      end;

      if (ViewDocs.Controller.FocusedColumn.Name='ViewDocsTT1') then
      begin
        quReplyRec.Active:=False;
        quReplyRec.SQL.Clear;
        quReplyRec.SQL.Add('SELECT * FROM dbo.REPLYLIST');
        quReplyRec.SQL.Add('  WHERE ID='+its(Abs(quDocInHdTICK1.AsInteger)));
        quReplyRec.SQL.Add('  and FSRARID='''+quDocInHdFSRARID.AsString+'''');
        quReplyRec.Active:=True;

        if quReplyRec.RecordCount>0 then
          ShowXMLView(quDocInHdTICK1.AsInteger,quReplyRecReplyFile.AsString);

        Exit;
      end;

      if (ViewDocs.Controller.FocusedColumn.Name='ViewDocsTT2') then
      begin
        quReplyRec.Active:=False;
        quReplyRec.SQL.Clear;
        quReplyRec.SQL.Add('SELECT * FROM dbo.REPLYLIST');
        quReplyRec.SQL.Add('  WHERE ID='+its(Abs(quDocInHdTICK2.AsInteger)));
        quReplyRec.SQL.Add('  and FSRARID='''+quDocInHdFSRARID.AsString+'''');
        quReplyRec.Active:=True;

        if quReplyRec.RecordCount>0 then
          ShowXMLView(quDocInHdTICK1.AsInteger,quReplyRecReplyFile.AsString);

        Exit;
      end;


      if quDocInHdIACTIVE.AsInteger=0 then
         ShowSpecView(quDocInHdFSRARID.AsString,quDocInHdSID.AsString,quDocInHdSHIPDATE.AsString,quDocInHdNUMBER.AsString,quDocInHdNAME.AsString,quDocInHdIDATE.AsInteger,1)
      else
         ShowSpecView(quDocInHdFSRARID.AsString,quDocInHdSID.AsString,quDocInHdSHIPDATE.AsString,quDocInHdNUMBER.AsString,quDocInHdNAME.AsString,quDocInHdIDATE.AsInteger,0);
    end;
  end;
end;

procedure TfmViewRar.ViewDocsOutDblClick(Sender: TObject);
begin
  // ������������
  with dmR do
  begin
    if quDocOutHd.RecordCount>0 then
    begin
      if (ViewDocsOut.Controller.FocusedColumn.Name='ViewDocsOutXMLF') then
      begin
        quDocOutRec.Active:=False;
        quDocOutRec.ParamByName('RARID').AsString:=quDocOutHdFSRARID.AsString;
        quDocOutRec.ParamByName('IDATE').AsInteger:=quDocOutHdIDATE.AsInteger;
        quDocOutRec.ParamByName('SID').AsString:=quDocOutHdSID.AsString;
        quDocOutRec.Active:=True;
        if quDocOutRec.RecordCount>0 then
        begin
          ShowXMLView(quDocOutRecIDHEAD.AsInteger,quDocOutRecSENDXML.AsString);
        end;

      end else
      begin
        if quDocOutHdIACTIVE.AsInteger=0 then
           ShowSpecViewOut(quDocOutHdFSRARID.AsString,quDocOutHdSID.AsString,quDocOutHdSHIPDATE.AsString,quDocOutHdNUMBER.AsString,quDocOutHdNAME.AsString,quDocOutHdIDATE.AsInteger,1,quDocOutHdDATEDOC.AsDateTime,quDocOutHdIDHEAD.AsInteger,CommonSet.SDEP)
        else
           ShowSpecViewOut(quDocOutHdFSRARID.AsString,quDocOutHdSID.AsString,quDocOutHdSHIPDATE.AsString,quDocOutHdNUMBER.AsString,quDocOutHdNAME.AsString,quDocOutHdIDATE.AsInteger,0,quDocOutHdDATEDOC.AsDateTime,quDocOutHdIDHEAD.AsInteger,CommonSet.SDEP);
      end;
    end;
  end;
end;

procedure TfmViewRar.acAutoExecute(Sender: TObject);
begin
  WriteHistoryTC('������ ���������.');
  WriteHistoryTC('   �������� - '+its(CommonSetTr.PeriodSec)+'���.');
  WriteHistoryTC('   IPUTM - '+CommonSet.IPUTM);
  WriteHistoryTC('   FSRAR_ID - '+CommonSet.FSRAR_ID);

  with dmR do
  begin
    try
      if FDConnection.Connected then
      begin
        WriteHistoryTC('');
        WriteHistoryTC(' �� ��');

        WriteHistoryTC('   ��� �����.');
        WriteHistoryTC('     �������� ������ ��������...');
        prGetListAsk(MemoLogLocal);

        WriteHistoryTC('     �������� ������ �������...');
        prGetListAsw(MemoLogLocal);

        WriteHistoryTC('     �������� ����� �������...');
        prGetFilesAsw(MemoLogLocal);

        WriteHistoryTC('     ���������� ����� �������...');
        prDecodeFilesAsw(MemoLogLocal);

        WriteHistoryTC('     �������������...');

        quDocInToCompare.Active:=False;
        quDocInToCompare.ParamByName('RARID').AsString:=CommonSet.FSRAR_ID;
        quDocInToCompare.Active:=True;
        quDocInToCompare.First;
        while not quDocInToCompare.Eof do
        begin
          WriteHistoryTC('       - '+quDocInToCompareFSRARID.AsString+' '+quDocInToCompareIDATE.AsString+' '+quDocInToCompareSHIPDATE.AsString+' '+quDocInToCompareSID.AsString+' '+quDocInToCompareNUMBER.AsString);

          quProc.Active:=False;
          quProc.SQL.Clear;
          quProc.SQL.Add('');

          quProc.SQL.Add('DECLARE @FSRARID varchar(50) = '''+quDocInToCompareFSRARID.AsString+'''');
          quProc.SQL.Add('DECLARE @SID varchar(50) = '''+quDocInToCompareSID.AsString+'''');
          quProc.SQL.Add('DECLARE @IDATE int = '+its(quDocInToCompareIDATE.AsInteger));
          quProc.SQL.Add('EXECUTE [dbo].[prCompareDoc] @FSRARID,@SID,@IDATE');
          quProc.Active:=True;

          WriteHistoryTC('          '+quProc.FieldByName('COMMENTCOMPARE').AsString);
          WriteHistoryTC('');
          quProc.Active:=False;

          quDocInToCompare.Next;
        end;
        quDocInToCompare.Active:=False;

        WriteHistoryTC('     �������� ������� � �����...');

        quDocInToSend.Active:=False;
        quDocInToSend.ParamByName('RARID').AsString:=CommonSet.FSRAR_ID;
        quDocInToSend.Active:=True;
        quDocInToSend.First;
        while not quDocInToSend.Eof do
        begin
          WriteHistoryTC('       - '+quDocInToSendFSRARID.AsString+' '+quDocInToSendIDATE.AsString+' '+quDocInToSendSHIPDATE.AsString+' '+quDocInToSendSID.AsString+' '+quDocInToSendNUMBER.AsString);

          if prSendAct(quDocInToSendFSRARID.AsString,quDocInToSendSID.AsString,quDocInToSendIDATE.AsInteger,0,MemoLogLocal) then
          begin
            WriteHistoryTC('         ��');

            quProc.Active:=False;
            quProc.SQL.Clear;
            quProc.SQL.Add('');

            quProc.SQL.Add('DECLARE @FSRARID varchar(50) = '''+quDocInToSendFSRARID.AsString+'''');
            quProc.SQL.Add('DECLARE @SID varchar(50) = '''+quDocInToSendSID.AsString+'''');
            quProc.SQL.Add('DECLARE @IDATE int = '+its(quDocInToSendIDATE.AsInteger));
            quProc.SQL.Add('EXECUTE [dbo].[prSetSendStatusDoc1] @FSRARID,@SID,@IDATE');
            quProc.Active:=True;

            quProc.Active:=False;


          end else WriteHistoryTC('         ERROR');

          quDocInToSend.Next;
        end;
        quDocInToSend.Active:=False;

        Init;
      end else
      begin
        WriteHistoryTC(' ������ �������� ��');
      end;
    finally
    end;
  end;
  WriteHistoryTC('������� ��������.');
  WriteHistoryTC('');

end;

procedure TfmViewRar.acCBDocExecute(Sender: TObject);
begin
  // ��������� ��
  with dmR do
  begin
    if (FDConnection.Connected) then
    begin
      try
        with dmR do
        begin
          if quPoint.Locate('ISHOP',beiPoint.EditValue,[]) then
          begin
            prShowCBDocs(quPointISHOP.AsInteger, quPointName.AsString,deDateBeg.Date,deDateEnd.Date,quPointSDEP.AsString);
//            prShowMCDocs(quPointISHOP.AsInteger, quPointName.AsString,deDateBeg.Date,deDateEnd.Date,quPointIPDB.AsString,quPointDBNAME.AsString,'sa',quPointPASSW.AsString);
          end;
        end;
      finally
      end;
    end;
  end;
end;

procedure TfmViewRar.acCloseExecute(Sender: TObject);
begin
  //�������
  Close;
//  fmMain.Close;
end;

procedure TfmViewRar.acCompareExecute(Sender: TObject);
var Rec:TcxCustomGridRecord;
    i,j,iC:Integer;
    FSRARID,SID:string;
    IDATE,IACTIVE:Integer;
begin
  //�����������
  if (ViewDocs.Controller.SelectedRecordCount>0) and (dmR.FDConnection.Connected) then
  begin
    ClearMessageLogLocal;
    ShowMessageLogLocal('����� ... ���� ��������� ����������.');
    iC:=0;

    FSRARID:='';
    SID:='';
    IDATE:=0;

    for i:=0 to ViewDocs.Controller.SelectedRecordCount-1 do
    begin
      Rec:=ViewDocs.Controller.SelectedRecords[i];

      for j:=0 to Rec.ValueCount-1 do
      begin
        if ViewDocs.Columns[j].Name='ViewDocsFSRARID' then begin FSRARID:=Rec.Values[j]; end;
        if ViewDocs.Columns[j].Name='ViewDocsIDATE' then begin IDATE:=Rec.Values[j]; end;
        if ViewDocs.Columns[j].Name='ViewDocsSID' then begin SID:=Rec.Values[j]; end;
        if ViewDocs.Columns[j].Name='ViewDocsIACTIVE' then begin IACTIVE:=Rec.Values[j]; end;
      end;
      if IACTIVE=1 then begin
        ShowMessageLogLocal('    ���. '+FSRARID+','+ds1(iDate)+','+SID+' � ������� "���������", ��������� � ��� ���������.');
        Continue;
      end;
      if (FSRARID>'')and(IDATE>0)and(SID>'') then
      begin
        with dmR do
        begin
          ShowMessageLogLocal('    ���. '+FSRARID+','+ds1(iDate)+','+SID);
          quProc.Active:=False;
          quProc.SQL.Clear;
          quProc.SQL.Add('');

          quProc.SQL.Add('DECLARE @FSRARID varchar(50) = '''+FSRARID+'''');
          quProc.SQL.Add('DECLARE @SID varchar(50) = '''+SID+'''');
          quProc.SQL.Add('DECLARE @IDATE int = '+its(iDate));
          quProc.SQL.Add('EXECUTE [dbo].[prCompareDoc] @FSRARID,@SID,@IDATE');
          quProc.Active:=True;

          ShowMessageLogLocal('         ---- '+quProc.FieldByName('COMMENTCOMPARE').AsString);
          quProc.Active:=False;
          inc(iC);
        end;
      end;
    end;
    init;
    ShowMessageLogLocal('���������� '+its(iC)+' ����������.');
    ShowMessageLogLocal('������� ��������.');
  end;
end;

procedure TfmViewRar.acCompareHand2Execute(Sender: TObject);
var IDHEAD: Integer;
    FSRARID,SID:string;
    IDATE:Integer;
    SDEP:string;
    bDo:Boolean;
begin
  //����������� �������� �������
  if dmR.FDConnection.Connected=false then Exit;
  if dmR.quDocInHd.RecordCount=0 then Exit;
  if dmR.quDocInHdIACTIVE.AsInteger>0 then begin
    ShowMessageLogLocal('    ���. '+FSRARID+','+ds1(iDate)+','+SID+' � ������� "���������", ��������� � ��� ���������.');
    Exit;
  end;

  with dmR do
  begin
    if (FDConnection.Connected) then
    begin
      with dmR do
      begin
        if quPoint.Locate('ISHOP',beiPoint.EditValue,[]) then
        begin
          //�������� ��������
          bDo:=False;

          SDEP:=Trim(quPointSDEP.AsString);
          if SDEP='' then  //������ ������� � ������ ��������
            if prSelectMCDocs(quPointISHOP.AsInteger, quPointName.AsString,deDateBeg.Date,deDateEnd.Date,quPointIPDB.AsString,quPointDBNAME.AsString,'sa',quPointPASSW.AsString, IDHEAD) then bDo:=True
          else   // � ������ ��
            if prSelectCBDocs(quPointISHOP.AsInteger,quPointName.AsString,deDateBeg.Date,deDateEnd.Date,SDEP,IDHEAD) then bDo:=True;

          if bDo then
          begin
            //����� ��������� ������� � IDHEAD
            FSRARID:=quDocInHdFSRARID.AsString;
            SID:=quDocInHdSID.AsString;
            IDATE:=quDocInHdIDATE.AsInteger;

            if (FSRARID>'')and(IDATE>0)and(SID>'') then
            begin
              with dmR do
              begin
                quProc.Active:=False;
                quProc.SQL.Clear;
                quProc.SQL.Add('');

                quProc.SQL.Add('DECLARE @FSRARID varchar(50) = '''+FSRARID+'''');
                quProc.SQL.Add('DECLARE @SID varchar(50) = '''+SID+'''');
                quProc.SQL.Add('DECLARE @IDATE int = '+its(iDate));
                quProc.SQL.Add('DECLARE @IDHEAD int = '+its(IDHEAD));
                quProc.SQL.Add('UPDATE dbo.ADOCSIN_HD');
                quProc.SQL.Add('SET');
                quProc.SQL.Add('  IDHEAD = @IDHEAD');
                quProc.SQL.Add('WHERE');
                quProc.SQL.Add('  FSRARID = @FSRARID');
                quProc.SQL.Add('  AND IDATE = @IDATE');
                quProc.SQL.Add('  AND SID = @SID');

                quProc.ExecSQL;
                ShowMessageLogLocal('    ���. '+FSRARID+','+ds1(iDate)+','+SID+'  c���������� ����� '+its(IDHEAD));
              end;
            end;
          end;
        end;
      end;
    end;
  end;
  Init;
end;

procedure TfmViewRar.acCompareHandExecute(Sender: TObject);
var IDHEAD: Integer;
    FSRARID,SID:string;
    IDATE:Integer;
    SDEP:string;
    bDo:Boolean;
begin
  //����������� �������� �������
  if dmR.FDConnection.Connected=false then Exit;
  if dmR.quDocInHd.RecordCount=0 then Exit;
  if dmR.quDocInHdIACTIVE.AsInteger>0 then begin
    ShowMessageLogLocal('    ���. '+FSRARID+','+ds1(iDate)+','+SID+' � ������� "���������", ��������� � ��� ���������.');
    Exit;
  end;

  with dmR do
  begin
    if (FDConnection.Connected) then
    begin
      with dmR do
      begin
        if quPoint.Locate('ISHOP',beiPoint.EditValue,[]) then
        begin
          //�������� ��������
          SDEP:=Trim(quPointSDEP.AsString);
          bDo:=False;

          if SDEP='' then  //������ ������� � ������ ��������
            if prSelectMCDocs(quPointISHOP.AsInteger, quPointName.AsString,deDateBeg.Date,deDateEnd.Date,quPointIPDB.AsString,quPointDBNAME.AsString,'sa',quPointPASSW.AsString, IDHEAD) then bDo:=True
          else   // � ������ ��
            if prSelectCBDocs(quPointISHOP.AsInteger,quPointName.AsString,deDateBeg.Date,deDateEnd.Date,SDEP,IDHEAD) then bDo:=True;

          if bDo then
          begin
            //����� ��������� ������� � IDHEAD
            FSRARID:=quDocInHdFSRARID.AsString;
            SID:=quDocInHdSID.AsString;
            IDATE:=quDocInHdIDATE.AsInteger;

            if (FSRARID>'')and(IDATE>0)and(SID>'') then
            begin
              with dmR do
              begin
                quProc.Active:=False;
                quProc.SQL.Clear;
                quProc.SQL.Add('');
                quProc.SQL.Add('DECLARE @FSRARID varchar(50) = '''+FSRARID+'''');
                quProc.SQL.Add('DECLARE @SID varchar(50) = '''+SID+'''');
                quProc.SQL.Add('DECLARE @IDATE int = '+its(iDate));
                quProc.SQL.Add('DECLARE @IDHEADMANUAL int = '+its(IDHEAD));
                quProc.SQL.Add('EXECUTE [dbo].[prCompareDoc] @FSRARID,@SID,@IDATE,@IDHEADMANUAL');
                quProc.Active:=True;

                ShowMessageLogLocal('         ---- '+quProc.FieldByName('COMMENTCOMPARE').AsString);
                ShowMessageLogLocal('    ���. '+FSRARID+','+ds1(iDate)+','+SID+'  c���������� ����� '+its(IDHEAD));
                quProc.Active:=False;
              end;
            end;
          end;
        end;
      end;
    end;
  end;
  Init;
end;

procedure TfmViewRar.acCreateFileSaleExecute(Sender: TObject);
Const
  CR = #$0d;
  LF = #$0a;
  CRLF = CR + LF;
  Boundary = 'END_OF_PART';

  var  AskXml: TNativeXml;
     IdF:Integer;
     SendXml,RetXml: TNativeXml;
     RetId:string;

     httpsend: THTTPSend;
     s: AnsiString;
     FS: TMemoryStream;
     bNoQuant:Boolean;
     nodePos: TXmlNode;
     vCli,vCli1:TCli;
     iNum:Integer;
     StrMemo:string;
     RetVal:TStringList;

     bRes:Boolean;
begin
  //������������ ���� �������� ������ �� �����
  with dmR do
  begin
    try
      IdF:=fGetId(1);
      //��������� XML

      prWMemo(MemoLogLocal,'  ��������� ������...');
      SendXml := TNativeXml.Create(nil);
      RetXml  := TNativeXml.Create(nil);

      httpsend := THTTPSend.Create;

      FS := TMemoryStream.Create;

      SendXml.XmlFormat := xfReadable;

      SendXml.CreateName('Cheque');

      vCli.ClientRegId:=CommonSet.FSRAR_ID;
      prGetAtr(vCli,vCli);

      SendXml.Root.WriteAttributeString('inn',Trim(vCli.INN));
      SendXml.Root.WriteAttributeString('datetime',FormatDateTime('ddmmyyhhnn',now));
      SendXml.Root.WriteAttributeString('kpp',Trim(vCli.KPP));
      SendXml.Root.WriteAttributeString('kassa','ALK060200439');
      SendXml.Root.WriteAttributeString('address',Trim(vCli.description));
      SendXml.Root.WriteAttributeString('name',Trim('������� "������"'));
      SendXml.Root.WriteAttributeString('shift','1804');
      SendXml.Root.WriteAttributeString('number','49');

      SendXml.Root.NodeNew('Bottle');
      SendXml.Root.NodeByName('Bottle').WriteAttributeString('barcode','22N000003GIMS772PH009RA605060120304438NQE33CHXHJJB7E426I36XBY52RCC6Q');
      SendXml.Root.NodeByName('Bottle').WriteAttributeString('ean','4607002225682');
      SendXml.Root.NodeByName('Bottle').WriteAttributeString('price',fts00(169.90));

//      SendXml.SaveToFile(its(IdF)+'.xml');
      SendXml.SaveToStream(FS);

      prWMemo(MemoLogLocal,'  ��������� ������ ...');

      httpsend.MimeType := 'multipart/form-data; boundary='+Boundary;  //���������� Contetn-Type �������
      // ���������� Mime-��� � ������ �� �����
      s:='--'+Boundary+CRLF+'Content-Disposition: form-data; name="xml_file"; filename="'+IdToName(IdF)+'"'+CRLF+'Content-Type: application/xml'+CRLF+CRLF;
      httpsend.Document.Write(PAnsiChar(s)^, Length(s));
      FS.Position := 0;
      httpsend.Document.CopyFrom(FS, FS.Size);  //���������� ����� � ���� ���������
//        S:=CRLF+CRLF+'--'+Boundary+CRLF;  //��������� ���� �������
      S:=CRLF+CRLF+'--'+Boundary+'--'+CRLF;  //��������� ���� �������
      httpsend.Document.Write(PAnsiChar(s)^, Length(s)); // ��������� ���� ���������

      httpsend.KeepAlive:=False;
      //httpsend.Protocol:='1.1';  //��������
      httpsend.Status100:=True;
      httpsend.Headers.Add('Accept: */*');

      // ���������� ������
      if httpsend.HTTPMethod('POST','http://'+CommonSet.IPUTM+':8080/xml') then
      begin
        prWMemo(MemoLogLocal,'TRUE');
        prWMemo(MemoLogLocal,'ResultCode='+IntToStr(httpsend.ResultCode));
        prWMemo(MemoLogLocal,httpsend.ResultString);

        if httpsend.ResultCode<>200 then
        begin



        end;


        if httpsend.ResultCode<>200 then
        begin
          bRes:=False;
          try
            RetVal:=TStringList.Create;
            RetVal.LoadFromStream(httpsend.Document, TEncoding.UTF8);
            prWMemo(MemoLogLocal,RetVal.Text);
          finally
            RetVal.Free;
          end;
        end;

        httpsend.Document.Position:=0;
        RetXml.LoadFromStream(httpsend.Document);
        RetXml.XmlFormat := xfReadable;

        if Assigned(RetXml.Root.NodeByName('url')) then
        begin
          RetId:=RetXml.Root.NodeByName('url').Value;

          if RetId>'' then
          begin
            httpsend.Document.Position:=0;

            quToRar.Edit;
            quToRarISTATUS.AsInteger:=2;
            quToRarRECEIVE_ID.AsString:=RetId;
            quToRarRECEIVE_FILE.LoadFromStream(httpsend.Document);
            quToRar.Post;
          end;
        end;
      end else begin
        prWMemo(MemoLogLocal,'  ������ ���������� ������� - POST,http://'+CommonSet.IPUTM+':8080/opt/in/WayBill');
        prWMemo(MemoLogLocal,'ResultCode='+IntToStr(httpsend.ResultCode));
        prWMemo(MemoLogLocal,httpsend.ResultString);

        try
          RetVal:=TStringList.Create;
          RetVal.LoadFromStream(httpsend.Document, TEncoding.UTF8);
          prWMemo(MemoLogLocal,RetVal.Text);
        finally
          RetVal.Free;
        end;

        quToRar.Edit;
        quToRarISTATUS.AsInteger:=100;
        quToRar.Post;

        //Result:=False;
      end;

    finally
      SendXml.Free;
      httpsend.Free;
      FS.Free;
      RetXml.Free;
    end;
  end;
end;

procedure TfmViewRar.acCreateReturnExecute(Sender: TObject);
begin
  //������� �������� �� �������
  with dmR do
  begin
    if quDocInHd.RecordCount>0 then
    begin
      if (quDocInHdIACTIVE.AsInteger=1) then
      begin
        // ����� ����������� �������
        if (Trim(quDocInHdWBREGID.AsString)>'') then
        begin
           ClearMessageLogLocal;
           ShowMessageLogLocal('�����.. ���� ������������ ��������� �� �������.');

           if prCreateRet(quDocInHdFSRARID.AsString,quDocInHdSID.AsString,quDocInHdIDATE.AsInteger,MemoLogLocal)
           then
           begin
             ShowMessageLogLocal('�������� ��������.');
           end
           else ShowMessageLogLocal('������ ����������.');

        end else ShowMessage('������������ �������� ����������, �.�. �� �������� �� ��� � �����.');
      end else ShowMessage('�������� ������.');
    end;
  end;
end;


procedure TfmViewRar.acDelDocInExecute(Sender: TObject);
begin
  //������� �������� �������
  with dmR do
  begin
    if quDocInHd.RecordCount>0 then
    begin
      if MessageDlg('������� �������� '+quDocInHdNUMBER.AsString+' �� '+ds1(quDocInHdIDATE.AsInteger)+'?',mtConfirmation, [mbYes, mbNo], 0, mbYes) = mrYes then
      begin
        ClearMessageLogLocal;
        ShowMessageLogLocal('�����.. ���� �������� ���������');

        WriteHistoryIn(quDocInHdFSRARID.AsString,quDocInHdSID.AsString,quDocInHdIDATE.AsInteger,'DELETE');

        if quDocInHdIACTIVE.AsInteger=0 then
        begin
          try
            quS.Active:=False;
            quS.SQL.Clear;
            quS.SQL.Add('');

            quS.SQL.Add('delete from dbo.ADOCSIN_HD');
            quS.SQL.Add('WHERE');
            quS.SQL.Add('  FSRARID = '''+quDocInHdFSRARID.AsString+'''');
            quS.SQL.Add('  AND IDATE = '+its(quDocInHdIDATE.AsInteger));
            quS.SQL.Add('  AND SID = '''+quDocInHdSID.AsString+'''');
            quS.ExecSQL;

            Init;
          except
          end;
        end else ShowMessageLogLocal('  �������� ������ ���������. �������� ����������.');


        ShowMessageLogLocal('������� ��������.');
      end;
    end;
  end;

end;

procedure TfmViewRar.acDelDocOutExecute(Sender: TObject);
begin
  //������� �������� �������
  with dmR do
  begin
    if quDocOutHd.RecordCount>0 then
    begin
      if MessageDlg('������� �������� ������� '+quDocOutHdNUMBER.AsString+' �� '+ds1(quDocOutHdIDATE.AsInteger)+'?',mtConfirmation, [mbYes, mbNo], 0, mbYes) = mrYes then
      begin
        ClearMessageLogLocal;
        ShowMessageLogLocal('�����.. ���� �������� ���������');

        if quDocOutHdIACTIVE.AsInteger=0 then
        begin
          try
            quS.Active:=False;
            quS.SQL.Clear;
            quS.SQL.Add('');

            quS.SQL.Add('delete from dbo.ADOCSOUT_HD');
            quS.SQL.Add('WHERE');
            quS.SQL.Add('  FSRARID = '''+quDocOutHdFSRARID.AsString+'''');
            quS.SQL.Add('  AND IDATE = '+its(quDocOutHdIDATE.AsInteger));
            quS.SQL.Add('  AND SID = '''+quDocOutHdSID.AsString+'''');
            quS.ExecSQL;

            Init;
          except
          end;
        end else ShowMessageLogLocal('  �������� ������ ���������. �������� ����������.');


        ShowMessageLogLocal('������� ��������.');
      end;
    end;
  end;
end;

procedure TfmViewRar.acExpExcelExecute(Sender: TObject);
begin
  if LevelDocs.Active then
     ExportGridToFile(formatdatetime('ddmmyyyy',deDateBeg.Date)+'_'+formatdatetime('ddmmyyyy',deDateEnd.Date)+'_'+formatdatetime('ddmmyyyy_hh_nn_ss',now)+'.xls', GridRar)
  else ExportGridToFile(formatdatetime('ddmmyyyy',deDateBeg.Date)+'_'+formatdatetime('ddmmyyyy',deDateEnd.Date)+'_'+formatdatetime('ddmmyyyy_hh_nn_ss',now)+'.xls', GridRar);

end;

procedure TfmViewRar.acGetRawTTNListExecute(Sender: TObject);
Const
  CR = #$0d;
  LF = #$0a;
  CRLF = CR + LF;
  Boundary = 'END_OF_PART';

var  AskXml: TNativeXml;
     IdF:Integer;
     RetXml: TNativeXml;
     RetId:string;

     httpsend: THTTPSend;
     s: AnsiString;
     FS: TMemoryStream;
begin
  // ��������� �������������� TTN
  if MessageDlg('��������� �������������� ��� � ����� ? ( ���������� - '+CommonSet.FSRAR_ID+')',mtConfirmation, [mbYes, mbNo], 0, mbYes) <> mrYes then Exit;
  if dmR.FDConnection.Connected=False then Exit;

  with dmR do
  begin
    try
      ClearMessageLogLocal;
      ShowMessageLogLocal('���� ��');

      IdF:=fGetId(1);
      ShowMessageLogLocal('  ������ '+its(IdF));

      FS := TMemoryStream.Create;
      httpsend := THTTPSend.Create;

      //��������� ���� �������
      AskXml := TNativeXml.Create(nil);
      RetXml := TNativeXml.Create(nil);

      AskXml.XmlFormat := xfReadable;
      AskXml.CreateName('ns:Documents');
      AskXml.Root.WriteAttributeString('Version', '1.0');
      AskXml.Root.WriteAttributeString('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
      AskXml.Root.WriteAttributeString('xmlns:ns', 'http://fsrar.ru/WEGAIS/WB_DOC_SINGLE_01');
      AskXml.Root.WriteAttributeString('xmlns:qp', 'http://fsrar.ru/WEGAIS/QueryParameters');
      AskXml.Root.NodeNew('ns:Owner');
      AskXml.Root.NodeByName('ns:Owner').NodeNew('ns:FSRAR_ID').Value:=CommonSet.FSRAR_ID;

      AskXml.Root.NodeNew('ns:Document');
      AskXml.Root.NodeByName('ns:Document').NodeNew('ns:QueryNATTN');
      AskXml.Root.NodeByName('ns:Document').NodeByName('ns:QueryNATTN').NodeNew('qp:Parameters');
      AskXml.Root.NodeByName('ns:Document').NodeByName('ns:QueryNATTN').NodeByName('qp:Parameters').NodeNew('qp:Parameter');

      AskXml.Root.NodeByName('ns:Document').NodeByName('ns:QueryNATTN').NodeByName('qp:Parameters').NodeByName('qp:Parameter').NodeNew('qp:Name').Value:=UTF8Encode('���');
      AskXml.Root.NodeByName('ns:Document').NodeByName('ns:QueryNATTN').NodeByName('qp:Parameters').NodeByName('qp:Parameter').NodeNew('qp:Value').Value:=CommonSet.FSRAR_ID;

      //AskXml.SaveToFile('c:\QueryNATTN.xml');
      AskXml.SaveToStream(FS);

      //����� � ����
      quToRar.Active:=False;
      quToRar.ParamByName('ID').AsInteger:=IdF;
      quToRar.ParamByName('RARID').AsString:=CommonSet.FSRAR_ID;
      quToRar.Active:=True;

      quToRar.Append;
      quToRarFSRARID.AsString:=CommonSet.FSRAR_ID;
      quToRarID.AsInteger:=IdF;
      quToRarDATEQU.AsDateTime:=Now;
      quToRarITYPEQU.AsInteger:=1;
      quToRarISTATUS.AsInteger:=1;
      //quToRarSENDFILE.LoadFromFile(NameF);
      quToRarSENDFILE.LoadFromStream(FS);
      quToRar.Post;

      httpsend.MimeType := 'multipart/form-data; boundary='+Boundary;  //���������� Contetn-Type �������
      // ���������� Mime-��� � ������ �� �����
      s:='--'+Boundary+CRLF+'Content-Disposition: form-data; name="xml_file"; filename="'+IdToName(IdF)+'"'+CRLF+'Content-Type: application/xml'+CRLF+CRLF;
      httpsend.Document.Write(PAnsiChar(s)^, Length(s));
      FS.Position := 0;
      httpsend.Document.CopyFrom(FS, FS.Size);  //���������� ����� � ���� ���������
      //S:=CRLF+CRLF+'--'+Boundary+CRLF;  //��������� ���� �������
      S:=CRLF+CRLF+'--'+Boundary+'--'+CRLF;  //��������� ���� �������
      httpsend.Document.Write(PAnsiChar(s)^, Length(s)); // ��������� ���� ���������

      httpsend.KeepAlive:=False;
      httpsend.Status100:=True;
      httpsend.Headers.Add('Accept: */*');

      // ���������� ������
      if httpsend.HTTPMethod('POST','http://'+CommonSet.IPUTM+':8080/opt/in/QueryNATTN') then
      begin
        ShowMessageLogLocal('TRUE');
        ShowMessageLogLocal('ResultCode='+IntToStr(httpsend.ResultCode));
        ShowMessageLogLocal(httpsend.ResultString);
        //MemoLogLocal.Lines.LoadFromStream(httpsend.Document, TEncoding.UTF8);

        httpsend.Document.Position:=0;
        RetXml.LoadFromStream(httpsend.Document);
        RetXml.XmlFormat := xfReadable;
        //RetXml.SaveToFile('C:\1110.xml');

        //RetId:='';
        //nodeRoot := RetXml.Root;
        //if assigned(nodeRoot) then  RetId:=nodeRoot.NodeByName('url').Value;

        RetId:=RetXml.Root.NodeByName('url').Value;

        if RetId>'' then
        begin
          httpsend.Document.Position:=0;

          quToRar.Edit;
          quToRarISTATUS.AsInteger:=2;
          quToRarRECEIVE_ID.AsString:=RetId;
          quToRarRECEIVE_FILE.LoadFromStream(httpsend.Document);
          quToRar.Post;
        end;

      end else begin
        ShowMessageLogLocal('FALSE');
        ShowMessageLogLocal('ResultCode='+IntToStr(httpsend.ResultCode));
        //ShowMessageLogLocal(httpsend.ResultString);

        quToRar.Edit;
        quToRarISTATUS.AsInteger:=100;
        quToRar.Post;
      end;

    finally
      FS.Free;
      AskXml.Free;
      RetXml.Free;
      quToRar.Active:=False;
    end;

    ShowMessageLogLocal('������� ��������.');
  end;
end;

procedure TfmViewRar.acGetRetDocsExecute(Sender: TObject);
begin
  //�������� ���������� ���������
  with dmR do
  begin
    ClearMessageLogLocal;
    ShowMessageLogLocal('�����.. ���� ��������� ���������� ���������� �� ������ � '+ds1(deDateBeg.Date)+' �� '+ds1(deDateEnd.Date));
    try
      quS.Active:=False;
      quS.SQL.Clear;
      quS.SQL.Add('');

      quS.SQL.Add('DECLARE @IDATEB int = '+its(Trunc(deDateBeg.Date)));
      quS.SQL.Add('DECLARE @IDATEE int = '+its(Trunc(deDateEnd.Date)));
      quS.SQL.Add('DECLARE @FSRARID varchar(50) = '''+CommonSet.FSRAR_ID+'''');

      if CommonSet.SDEP>'' then
      begin
        quS.SQL.Add('DECLARE @SDEP varchar(200) = '''+CommonSet.SDEP+'''');
        quS.SQL.Add('EXECUTE [dbo].[prImportRetDocs] @IDATEB,@IDATEE,@FSRARID,@SDEP');
     end else
      begin
        quS.SQL.Add('EXECUTE [dbo].[prImportRetDocs] @IDATEB,@IDATEE,@FSRARID');
      end;
      quS.ExecSQL;

      Init;
    except
    end;

    ShowMessageLogLocal('������� ��������.');
  end;
end;

procedure TfmViewRar.acGetTTNExecute(Sender: TObject);
Const
  CR = #$0d;
  LF = #$0a;
  CRLF = CR + LF;
  Boundary = 'END_OF_PART';

var  AskXml: TNativeXml;
     IdF:Integer;
     RetXml: TNativeXml;
     RetId:string;

     httpsend: THTTPSend;
     s: AnsiString;
     FS: TMemoryStream;
begin
  // ��������� �������� �� TTN
  if MessageDlg('��������� �������� '+beiGetTTN.EditValue+' � ����� ? ( ���������� - '+CommonSet.FSRAR_ID+')',mtConfirmation, [mbYes, mbNo], 0, mbYes) = mrYes then
  begin
    with dmR do
    begin
      if (FDConnection.Connected)and(beiGetTTN.EditValue>'') then
      begin
        try
          ClearMessageLogLocal;
          ShowMessageLogLocal('���� ��');

          IdF:=fGetId(1);
          ShowMessageLogLocal('  ������ '+its(IdF));

          FS := TMemoryStream.Create;
          httpsend := THTTPSend.Create;

          //��������� ���� �������
          AskXml := TNativeXml.Create(nil);
          RetXml := TNativeXml.Create(nil);

          AskXml.XmlFormat := xfReadable;
          AskXml.CreateName('ns:Documents');
          AskXml.Root.WriteAttributeString('Version', '1.0');
          AskXml.Root.WriteAttributeString('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
          AskXml.Root.WriteAttributeString('xmlns:ns', 'http://fsrar.ru/WEGAIS/WB_DOC_SINGLE_01');
          AskXml.Root.WriteAttributeString('xmlns:qp', 'http://fsrar.ru/WEGAIS/QueryParameters');
          AskXml.Root.NodeNew('ns:Owner');
          AskXml.Root.NodeByName('ns:Owner').NodeNew('ns:FSRAR_ID').Value:=CommonSet.FSRAR_ID;

          AskXml.Root.NodeNew('ns:Document');
          AskXml.Root.NodeByName('ns:Document').NodeNew('ns:QueryResendDoc');
          AskXml.Root.NodeByName('ns:Document').NodeByName('ns:QueryResendDoc').NodeNew('qp:Parameters');
          AskXml.Root.NodeByName('ns:Document').NodeByName('ns:QueryResendDoc').NodeByName('qp:Parameters').NodeNew('qp:Parameter');

          AskXml.Root.NodeByName('ns:Document').NodeByName('ns:QueryResendDoc').NodeByName('qp:Parameters').NodeByName('qp:Parameter').NodeNew('qp:Name').Value:='WBREGID';
          AskXml.Root.NodeByName('ns:Document').NodeByName('ns:QueryResendDoc').NodeByName('qp:Parameters').NodeByName('qp:Parameter').NodeNew('qp:Value').Value:=beiGetTTN.EditValue;

  //        AskXml.SaveToFile(NameF);
          AskXml.SaveToStream(FS);

          //����� � ����
          quToRar.Active:=False;
          quToRar.ParamByName('ID').AsInteger:=IdF;
          quToRar.ParamByName('RARID').AsString:=CommonSet.FSRAR_ID;
          quToRar.Active:=True;

          quToRar.Append;
          quToRarFSRARID.AsString:=CommonSet.FSRAR_ID;
          quToRarID.AsInteger:=IdF;
          quToRarDATEQU.AsDateTime:=Now;
          quToRarITYPEQU.AsInteger:=1;
          quToRarISTATUS.AsInteger:=1;
  //        quToRarSENDFILE.LoadFromFile(NameF);
          quToRarSENDFILE.LoadFromStream(FS);
          quToRar.Post;

          httpsend.MimeType := 'multipart/form-data; boundary='+Boundary;  //���������� Contetn-Type �������
          // ���������� Mime-��� � ������ �� �����
          s:='--'+Boundary+CRLF+'Content-Disposition: form-data; name="xml_file"; filename="'+IdToName(IdF)+'"'+CRLF+'Content-Type: application/xml'+CRLF+CRLF;
          httpsend.Document.Write(PAnsiChar(s)^, Length(s));
          FS.Position := 0;
          httpsend.Document.CopyFrom(FS, FS.Size);  //���������� ����� � ���� ���������
  //        S:=CRLF+CRLF+'--'+Boundary+CRLF;  //��������� ���� �������
          S:=CRLF+CRLF+'--'+Boundary+'--'+CRLF;  //��������� ���� �������
          httpsend.Document.Write(PAnsiChar(s)^, Length(s)); // ��������� ���� ���������

          httpsend.KeepAlive:=False;
          httpsend.Status100:=True;
          httpsend.Headers.Add('Accept: */*');

          // ���������� ������
          if httpsend.HTTPMethod('POST','http://'+CommonSet.IPUTM+':8080/opt/in/QueryResendDoc') then
          begin
            ShowMessageLogLocal('TRUE');
            ShowMessageLogLocal('ResultCode='+IntToStr(httpsend.ResultCode));
            ShowMessageLogLocal(httpsend.ResultString);
  //          MemoLogLocal.Lines.LoadFromStream(httpsend.Document, TEncoding.UTF8);

            httpsend.Document.Position:=0;
            RetXml.LoadFromStream(httpsend.Document);
            RetXml.XmlFormat := xfReadable;
  //          RetXml.SaveToFile('C:\1110.xml');

  //          RetId:='';
  //          nodeRoot := RetXml.Root;
  //          if assigned(nodeRoot) then  RetId:=nodeRoot.NodeByName('url').Value;

            RetId:=RetXml.Root.NodeByName('url').Value;

            if RetId>'' then
            begin
              httpsend.Document.Position:=0;

              quToRar.Edit;
              quToRarISTATUS.AsInteger:=2;
              quToRarRECEIVE_ID.AsString:=RetId;
              quToRarRECEIVE_FILE.LoadFromStream(httpsend.Document);
              quToRar.Post;
            end;

          end else begin
            ShowMessageLogLocal('FALSE');
            ShowMessageLogLocal('ResultCode='+IntToStr(httpsend.ResultCode));
  //          ShowMessageLogLocal(httpsend.ResultString);

            quToRar.Edit;
            quToRarISTATUS.AsInteger:=100;
            quToRar.Post;
          end;

        finally
          FS.Free;
          AskXml.Free;
          RetXml.Free;
          quToRar.Active:=False;
        end;

        ShowMessageLogLocal('������� ��������.');
      end;
    end;
  end;
end;

procedure TfmViewRar.acGetTTNPopupExecute(Sender: TObject);
begin
  //��������� ���������� �������� � �����
  beiGetTTN.EditValue:=dmR.quNATTNWBREGID.AsString;
  acGetTTN.Execute;
end;

procedure TfmViewRar.acHistoryDocInExecute(Sender: TObject);
var F: TfmHistoryDoc;
begin
  with dmR do
  begin
    if quDocInHd.RecordCount<=0 then Exit;

    F:=TfmHistoryDoc.Create(Self);
    try
      F.ClearMessageLogLocal;

      quSelHistory.Active:=False;
      quSelHistory.SQL.Clear;
      quSelHistory.SQL.Add('');
      quSelHistory.SQL.Add('SELECT cast([LOG] as varchar(900)) as [LOG]');
      quSelHistory.SQL.Add('FROM dbo.ADOCS_HISTORY');
      quSelHistory.SQL.Add('where [ITYPE]=1');
      quSelHistory.SQL.Add('and [FSRARID]='''+quDocInHdFSRARID.AsString+'''');
      quSelHistory.SQL.Add('and [IDATE]='+its(quDocInHdIDATE.AsInteger));
      quSelHistory.SQL.Add('and [SID]='''+quDocInHdSID.AsString+'''');
      quSelHistory.SQL.Add('order by [ID]');
      quSelHistory.Active:=True;

      quSelHistory.First;
      while not quSelHistory.Eof do
      begin
        F.ShowMessageLogLocal(quSelHistoryLOG.AsString);
        quSelHistory.Next;
      end;
      quSelHistory.Active:=False;

      F.ShowModal;
    finally
      F.Free;
    end;
  end;
end;

procedure TfmViewRar.acInEditExecute(Sender: TObject);
begin
   //��������� �������� � ��������������
  with dmR do
  begin
    if quDocInHd.RecordCount>0 then
    begin
      if MessageDlg('��������� �������� '+quDocInHdNUMBER.AsString+' �� '+ds1(quDocInHdIDATE.AsInteger)+' � ��������������?',mtConfirmation, [mbYes, mbNo], 0, mbYes) = mrYes then
      begin
        WriteHistoryIn(quDocInHdFSRARID.AsString,quDocInHdSID.AsString,quDocInHdIDATE.AsInteger,' ��������� � ����� ��������������. ������� ������: Active '+its(quDocInHdIACTIVE.AsInteger)+'  READYSEND '+its(quDocInHdREADYSEND.AsInteger));

        ClearMessageLogLocal;
        ShowMessageLogLocal('�����..');

        quS.Active:=False;
        quS.SQL.Clear;
        quS.SQL.Add('');

        quS.SQL.Add('UPDATE dbo.ADOCSIN_HD');
        quS.SQL.Add('SET IACTIVE = 0 ,READYSEND = 0');
        quS.SQL.Add('WHERE');
        quS.SQL.Add('  FSRARID = '''+quDocInHdFSRARID.AsString+'''');
        quS.SQL.Add('  AND IDATE = '+its(quDocInHdIDATE.AsInteger));
        quS.SQL.Add('  AND SID = '''+quDocInHdSID.AsString+'''');
        quS.ExecSQL;

        Init;

        ShowMessageLogLocal('������� ��������.');
      end;
    end;
  end;
end;

procedure TfmViewRar.acInEditRetExecute(Sender: TObject);
begin
   //��������� �������� � ��������������
  with dmR do
  begin
    if quDocOutHd.RecordCount>0 then
    begin
      if MessageDlg('��������� �������� '+quDocOutHdNUMBER.AsString+' �� '+ds1(quDocOutHdIDATE.AsInteger)+' � ��������������?',mtConfirmation, [mbYes, mbNo], 0, mbYes) = mrYes then
      begin
        WriteHistoryOut(quDocOutHdFSRARID.AsString,quDocOutHdSID.AsString,quDocOutHdIDATE.AsInteger,' ��������� � ����� ��������������. ������� ������: Active '+its(quDocOutHdIACTIVE.AsInteger)+'  READYSEND '+its(quDocOutHdREADYSEND.AsInteger));

        ClearMessageLogLocal;
        ShowMessageLogLocal('�����..');

        quS.Active:=False;
        quS.SQL.Clear;
        quS.SQL.Add('');

        quS.SQL.Add('UPDATE dbo.ADOCSOUT_HD');
        quS.SQL.Add('SET IACTIVE = 0 ,READYSEND = 0');
        quS.SQL.Add('WHERE');
        quS.SQL.Add('  FSRARID = '''+quDocOutHdFSRARID.AsString+'''');
        quS.SQL.Add('  AND IDATE = '+its(quDocOutHdIDATE.AsInteger));
        quS.SQL.Add('  AND SID = '''+quDocOutHdSID.AsString+'''');
        quS.ExecSQL;

        Init;

        ShowMessageLogLocal('������� ��������.');
      end;
    end;
  end;
end;

procedure TfmViewRar.acInfoABExecute(Sender: TObject);
var SCLI:UTF8String;
    xmlstr,SID,CLIID,RARID:string;

    Rec:TcxCustomGridRecord;
    i,j,iC:Integer;
begin
  //�������� ������� � � � � ���������
  if (ViewDocs.Controller.SelectedRecordCount>0) and (dmR.FDConnection.Connected) then
  begin
    if MessageDlg('������������� ������� � � � �� ��������� ('+its(ViewDocs.Controller.SelectedRecordCount)+') ���������� ?',mtConfirmation, [mbYes, mbNo], 0, mbYes) = mrYes then
    begin
      ClearMessageLogLocal;
      ShowMessageLogLocal('����� ... ���� ��������� ����������.');
      iC:=0;

      SID:='';
      CLIID:='';
      RARID:='';


      for i:=0 to ViewDocs.Controller.SelectedRecordCount-1 do
      begin
        Rec:=ViewDocs.Controller.SelectedRecords[i];

        for j:=0 to Rec.ValueCount-1 do
        begin
          if ViewDocs.Columns[j].Name='ViewDocsCLIFROM' then begin CLIID:=Rec.Values[j]; end;
          if ViewDocs.Columns[j].Name='ViewDocsSID' then begin SID:=Rec.Values[j]; end;
          if ViewDocs.Columns[j].Name='ViewDocsFSRARID' then begin RARID:=Rec.Values[j]; end;
        end;
        if (CLIID>'')and(SID>'')and(RARID>'') then
        begin
          with dmR do
          begin
            ShowMessageLogLocal('    ���. '+SID+' ��������� - '+CLIID);

            SCLI:=UTF8Encode('<ns:FSRAR_ID>'+CLIID+'</ns:FSRAR_ID>');

            quReplyRec.Active:=False;
            quReplyRec.SQL.Clear;
            quReplyRec.SQL.Add('SELECT * FROM dbo.REPLYLIST');
            quReplyRec.SQL.Add('  WHERE ReplyFile LIKE ''%'+SCLI+'%''');
            quReplyRec.SQL.Add('  and FSRARID='''+RARID+'''');
            quReplyRec.Active:=True;

            if quReplyRec.RecordCount>0 then
            begin
              ShowMessageLogLocal(its(quReplyRec.RecordCount));
              ShowMessageLogLocal('   �����.. ���� ���������.');

              quReplyRec.First;
              while not quReplyRec.Eof do
              begin
                xmlstr:=UTF8ToString(quReplyRecReplyFile.AsString);
                if Pos('WayBill',xmlstr)>0 then //��� ��������
                begin
                  prDecodeTTN_AB(xmlstr,SID)
                end;

                quReplyRec.Next;
              end;

              ShowMessageLogLocal(' ������� ��������.');
            end;

            quReplyRec.Active:=False;

            inc(iC);
          end;
        end;
      end;
      init;
      ShowMessageLogLocal('���������� '+its(iC)+' ����������.');
      ShowMessageLogLocal('������� ��������.');
    end;
  end;
end;

procedure TfmViewRar.acMCrystalDocsExecute(Sender: TObject);
begin
  //��������� ���������
  with dmR do
  begin
    if (FDConnection.Connected) then
    begin
      try
        if quPoint.Locate('ISHOP',beiPoint.EditValue,[]) then
        begin
          prShowMCDocs(quPointISHOP.AsInteger, quPointName.AsString,deDateBeg.Date,deDateEnd.Date,quPointIPDB.AsString,quPointDBNAME.AsString,'sa',quPointPASSW.AsString);
        end;
      finally
      end;
    end;
  end;
end;

procedure TfmViewRar.acProdExecute(Sender: TObject);
Const
  CR = #$0d;
  LF = #$0a;
  CRLF = CR + LF;
  Boundary = 'END_OF_PART';

var  AskXml: TNativeXml;
     IdF:Integer;
     RetXml: TNativeXml;
     RetId:string;

     httpsend: THTTPSend;
     s: AnsiString;
     FS: TMemoryStream;


  function IdToName(Id:Integer):string;
  begin
    Result:=IntToStr(Id);
    while Length(Result)<10 do Result:='0'+Result;
    Result:=Result+'.xml';
  end;
begin
  //������ ����������� �� �������������
  with dmR do
  begin
    if (FDConnection.Connected)and(cxBarEditItem1.EditValue>'') then
    begin
      try
        ClearMessageLogLocal;
        ShowMessageLogLocal('���� ��');

        IdF:=fGetId(1);

        FS := TMemoryStream.Create;
        httpsend := THTTPSend.Create;

        //��������� ���� �������
        AskXml := TNativeXml.Create(nil);
        RetXml := TNativeXml.Create(nil);


        AskXml.XmlFormat := xfReadable;
        AskXml.CreateName('ns:Documents');
        AskXml.Root.WriteAttributeString('Version', '1.0');
        AskXml.Root.WriteAttributeString('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
        AskXml.Root.WriteAttributeString('xmlns:ns', 'http://fsrar.ru/WEGAIS/WB_DOC_SINGLE_01');
        AskXml.Root.WriteAttributeString('xmlns:qp', 'http://fsrar.ru/WEGAIS/QueryParameters');
        AskXml.Root.NodeNew('ns:Owner');
        AskXml.Root.NodeByName('ns:Owner').NodeNew('ns:FSRAR_ID').Value:=CommonSet.FSRAR_ID;
        AskXml.Root.NodeNew('ns:Document');
        AskXml.Root.NodeByName('ns:Document').NodeNew('ns:QueryAP');
        AskXml.Root.NodeByName('ns:Document').NodeByName('ns:QueryAP').NodeNew('qp:Parameters');
        AskXml.Root.NodeByName('ns:Document').NodeByName('ns:QueryAP').NodeByName('qp:Parameters').NodeNew('qp:Parameter');

        AskXml.Root.NodeByName('ns:Document').NodeByName('ns:QueryAP').NodeByName('qp:Parameters').NodeByName('qp:Parameter').NodeNew('qp:Name').Value:='���';
        AskXml.Root.NodeByName('ns:Document').NodeByName('ns:QueryAP').NodeByName('qp:Parameters').NodeByName('qp:Parameter').NodeNew('qp:Value').Value:=cxBarEditItem1.EditValue;

//        AskXml.SaveToFile(NameF);
        AskXml.SaveToStream(FS);


        //����� � ����
        quToRar.Active:=False;
        quToRar.ParamByName('ID').AsInteger:=IdF;
        quToRar.ParamByName('RARID').AsString:=CommonSet.FSRAR_ID;
        quToRar.Active:=True;

        quToRar.Append;
        quToRarFSRARID.AsString:=CommonSet.FSRAR_ID;
        quToRarID.AsInteger:=IdF;
        quToRarDATEQU.AsDateTime:=Now;
        quToRarITYPEQU.AsInteger:=1;
        quToRarISTATUS.AsInteger:=1;
//        quToRarSENDFILE.LoadFromFile(NameF);
        quToRarSENDFILE.LoadFromStream(FS);
        quToRar.Post;


        httpsend.MimeType := 'multipart/form-data; boundary='+Boundary;  //���������� Contetn-Type �������
        // ���������� Mime-��� � ������ �� �����
        s:='--'+Boundary+CRLF+'Content-Disposition: form-data; name="xml_file"; filename="'+IdToName(IdF)+'"'+CRLF+'Content-Type: application/xml'+CRLF+CRLF;
        httpsend.Document.Write(PAnsiChar(s)^, Length(s));
        FS.Position := 0;
        httpsend.Document.CopyFrom(FS, FS.Size);  //���������� ����� � ���� ���������
//        S:=CRLF+CRLF+'--'+Boundary+CRLF;  //��������� ���� �������
        S:=CRLF+CRLF+'--'+Boundary+'--'+CRLF;  //��������� ���� �������
        httpsend.Document.Write(PAnsiChar(s)^, Length(s)); // ��������� ���� ���������

        httpsend.KeepAlive:=False;
        httpsend.Status100:=True;
        httpsend.Headers.Add('Accept: */*');

        // ���������� ������
        if httpsend.HTTPMethod('POST','http://'+CommonSet.IPUTM+':8080/opt/in/QueryAP') then
        begin
          ShowMessageLogLocal('TRUE');
          ShowMessageLogLocal('ResultCode='+IntToStr(httpsend.ResultCode));
          ShowMessageLogLocal(httpsend.ResultString);
//          MemoLogLocal.Lines.LoadFromStream(httpsend.Document, TEncoding.UTF8);

          httpsend.Document.Position:=0;
          RetXml.LoadFromStream(httpsend.Document);
          RetXml.XmlFormat := xfReadable;
//          RetXml.SaveToFile('C:\1110.xml');

//          RetId:='';
//          nodeRoot := RetXml.Root;
//          if assigned(nodeRoot) then  RetId:=nodeRoot.NodeByName('url').Value;

          RetId:=RetXml.Root.NodeByName('url').Value;

          if RetId>'' then
          begin
            httpsend.Document.Position:=0;

            quToRar.Edit;
            quToRarISTATUS.AsInteger:=2;
            quToRarRECEIVE_ID.AsString:=RetId;
            quToRarRECEIVE_FILE.LoadFromStream(httpsend.Document);
            quToRar.Post;
          end;

        end else begin
          ShowMessageLogLocal('FALSE');
          ShowMessageLogLocal('ResultCode='+IntToStr(httpsend.ResultCode));
//          ShowMessageLogLocal(httpsend.ResultString);

          quToRar.Edit;
          quToRarISTATUS.AsInteger:=100;
          quToRar.Post;
        end;

      finally
        FS.Free;
        AskXml.Free;
        RetXml.Free;
        quToRar.Active:=False;
      end;

      ShowMessageLogLocal('������� ��������.');
    end;
  end;
end;

procedure TfmViewRar.acProDocsExecute(Sender: TObject);
begin
  //��������� ������������
  if dmR.quPoint.Locate('ISHOP',beiPoint.EditValue,[]) then
    prShowProDocs(deDateBeg.Date,deDateEnd.Date, dmR.quPointRARID.AsString, dmR.quPointISPRODUCTION.AsInteger=1);
end;


procedure TfmViewRar.acQueryFormF1Execute(Sender: TObject);
Const
  CR = #$0d;
  LF = #$0a;
  CRLF = CR + LF;
  Boundary = 'END_OF_PART';

var  AskXml: TNativeXml;
     IdF:Integer;
     RetXml: TNativeXml;
     RetId:string;

     httpsend: THTTPSend;
     s: AnsiString;
     FS: TMemoryStream;
     FormRegId:string;
     RetVal:TStringList;
begin
  //������� ������ ������� 1 (��� �� ������� �)

  if MessageDlg('��������� ������� 1 � ����� ? ( ���������� - '+CommonSet.FSRAR_ID+')',mtConfirmation, [mbYes, mbNo], 0, mbYes) <> mrYes then Exit;
  if dmR.FDConnection.Connected=False then Exit;

  with dmR do
  begin
    try
      ClearMessageLogLocal;
      ShowMessageLogLocal('���� ��');

      IdF:=fGetId(1);
      ShowMessageLogLocal('  ������ '+its(IdF));

      FS := TMemoryStream.Create;
      httpsend := THTTPSend.Create;

      //��������� ���� �������
      AskXml := TNativeXml.Create(nil);
      RetXml := TNativeXml.Create(nil);

      AskXml.XmlFormat := xfReadable;
      AskXml.CreateName('ns:Documents');
      AskXml.Root.WriteAttributeString('Version', '1.0');
      AskXml.Root.WriteAttributeString('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
      AskXml.Root.WriteAttributeString('xmlns:ns', 'http://fsrar.ru/WEGAIS/WB_DOC_SINGLE_01');
      AskXml.Root.WriteAttributeString('xmlns:qf', 'http://fsrar.ru/WEGAIS/QueryFormF1F2');
      AskXml.Root.NodeNew('ns:Owner');
      AskXml.Root.NodeByName('ns:Owner').NodeNew('ns:FSRAR_ID').Value:=CommonSet.FSRAR_ID;

      AskXml.Root.NodeNew('ns:Document');
      AskXml.Root.NodeByName('ns:Document').NodeNew('ns:QueryFormF1');
      AskXml.Root.NodeByName('ns:Document').NodeByName('ns:QueryFormF1').NodeNew('qf:FormRegId');
      //FormRegId:='FA-000000019991821';
      FormRegId:='*';
      AskXml.Root.NodeByName('ns:Document').NodeByName('ns:QueryFormF1').NodeByName('qf:FormRegId').Value:=UTF8Encode(FormRegId);

      //<?xml version="1.0" encoding="UTF-8"?>
      //<ns:Documents Version="1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ns="http://fsrar.ru/WEGAIS/WB_DOC_SINGLE_01" xmlns:qf="http://fsrar.ru/WEGAIS/QueryFormF1F2">
      //	<ns:Owner>
      //		<ns:FSRAR_ID>030000194005</ns:FSRAR_ID>
      //	</ns:Owner>
      //	<ns:Document>
      //		<ns:QueryFormF1>
      //			<qf:FormRegId>TEST-FA-000000005349030</qf:FormRegId>
      //		</ns:QueryFormF1>
      //	</ns:Document>
      //</ns:Documents>

      //AskXml.SaveToFile('c:\QueryForm1.xml');
      AskXml.SaveToStream(FS);

      //����� � ����
      quToRar.Active:=False;
      quToRar.ParamByName('ID').AsInteger:=IdF;
      quToRar.ParamByName('RARID').AsString:=CommonSet.FSRAR_ID;
      quToRar.Active:=True;

      quToRar.Append;
      quToRarFSRARID.AsString:=CommonSet.FSRAR_ID;
      quToRarID.AsInteger:=IdF;
      quToRarDATEQU.AsDateTime:=Now;
      quToRarITYPEQU.AsInteger:=1;
      quToRarISTATUS.AsInteger:=1;
      //quToRarSENDFILE.LoadFromFile(NameF);
      quToRarSENDFILE.LoadFromStream(FS);
      quToRar.Post;

      httpsend.MimeType := 'multipart/form-data; boundary='+Boundary;  //���������� Contetn-Type �������
      // ���������� Mime-��� � ������ �� �����
      s:='--'+Boundary+CRLF+'Content-Disposition: form-data; name="xml_file"; filename="'+IdToName(IdF)+'"'+CRLF+'Content-Type: application/xml'+CRLF+CRLF;
      httpsend.Document.Write(PAnsiChar(s)^, Length(s));
      FS.Position := 0;
      httpsend.Document.CopyFrom(FS, FS.Size);  //���������� ����� � ���� ���������
      //S:=CRLF+CRLF+'--'+Boundary+CRLF;  //��������� ���� �������
      S:=CRLF+CRLF+'--'+Boundary+'--'+CRLF;  //��������� ���� �������
      httpsend.Document.Write(PAnsiChar(s)^, Length(s)); // ��������� ���� ���������

      httpsend.KeepAlive:=False;
      httpsend.Status100:=True;
      httpsend.Headers.Add('Accept: */*');

      // ���������� ������
      if httpsend.HTTPMethod('POST','http://'+CommonSet.IPUTM+':8080/opt/in/QueryFormF1') then
      begin
        ShowMessageLogLocal('TRUE');
        ShowMessageLogLocal('ResultCode='+IntToStr(httpsend.ResultCode));
        ShowMessageLogLocal(httpsend.ResultString);
        //MemoLogLocal.Lines.LoadFromStream(httpsend.Document, TEncoding.UTF8);

        if httpsend.ResultCode<>200 then
        begin
          try
            RetVal:=TStringList.Create;
            RetVal.LoadFromStream(httpsend.Document, TEncoding.UTF8);
            prWMemo(MemoLogLocal,RetVal.Text);
          finally
            RetVal.Free;
          end;
        end;

        httpsend.Document.Position:=0;
        RetXml.LoadFromStream(httpsend.Document);
        RetXml.XmlFormat := xfReadable;
        //RetXml.SaveToFile('C:\1110.xml');

        //RetId:='';
        //nodeRoot := RetXml.Root;
        //if assigned(nodeRoot) then  RetId:=nodeRoot.NodeByName('url').Value;

        RetId:=RetXml.Root.NodeByName('url').Value;

        if RetId>'' then
        begin
          httpsend.Document.Position:=0;

          quToRar.Edit;
          quToRarISTATUS.AsInteger:=2;
          quToRarRECEIVE_ID.AsString:=RetId;
          quToRarRECEIVE_FILE.LoadFromStream(httpsend.Document);
          quToRar.Post;
        end;

      end else begin
        ShowMessageLogLocal('FALSE');
        ShowMessageLogLocal('ResultCode='+IntToStr(httpsend.ResultCode));
        //ShowMessageLogLocal(httpsend.ResultString);

        quToRar.Edit;
        quToRarISTATUS.AsInteger:=100;
        quToRar.Post;
      end;

    finally
      FS.Free;
      AskXml.Free;
      RetXml.Free;
      quToRar.Active:=False;
    end;

    ShowMessageLogLocal('������� ��������.');
  end;
end;

procedure TfmViewRar.acQueryRestsExecute(Sender: TObject);
Const
  CR = #$0d;
  LF = #$0a;
  CRLF = CR + LF;
  Boundary = 'END_OF_PART';

var  AskXml: TNativeXml;
     IdF:Integer;
     RetXml: TNativeXml;
     RetId:string;

     httpsend: THTTPSend;
     s: AnsiString;
     FS: TMemoryStream;
     RetVal:TStringList;
begin
  //������ ��������
  if MessageDlg('��������� ������� � ����� ? ( ���������� - '+CommonSet.FSRAR_ID+')',mtConfirmation, [mbYes, mbNo], 0, mbYes) <> mrYes then Exit;
  if dmR.FDConnection.Connected=False then Exit;

  with dmR do
  begin
    try
      ClearMessageLogLocal;
      ShowMessageLogLocal('���� ��');

      IdF:=fGetId(1);
      ShowMessageLogLocal('  ������ '+its(IdF));

      FS := TMemoryStream.Create;
      httpsend := THTTPSend.Create;

      //��������� ���� �������
      AskXml := TNativeXml.Create(nil);
      RetXml := TNativeXml.Create(nil);

      AskXml.XmlFormat := xfReadable;
      AskXml.CreateName('ns:Documents');
      AskXml.Root.WriteAttributeString('Version', '1.0');
      AskXml.Root.WriteAttributeString('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
      AskXml.Root.WriteAttributeString('xmlns:ns', 'http://fsrar.ru/WEGAIS/WB_DOC_SINGLE_01');
      AskXml.Root.WriteAttributeString('xmlns:qp', 'http://fsrar.ru/WEGAIS/QueryParameters');
      AskXml.Root.NodeNew('ns:Owner');
      AskXml.Root.NodeByName('ns:Owner').NodeNew('ns:FSRAR_ID').Value:=CommonSet.FSRAR_ID;

      AskXml.Root.NodeNew('ns:Document');
      AskXml.Root.NodeByName('ns:Document').NodeNew('ns:QueryRests');
      AskXml.Root.NodeByName('ns:Document').NodeByName('ns:QueryRests').Value:='';

      //<?xml version="1.0" encoding="UTF-8"?>
      //<ns:Documents Version="1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ns="http://fsrar.ru/WEGAIS/WB_DOC_SINGLE_01" xmlns:qp="http://fsrar.ru/WEGAIS/QueryParameters">
      //	<ns:Owner>
      //		<ns:FSRAR_ID>00040218</ns:FSRAR_ID>
      //	</ns:Owner>
      //	<ns:Document>
      //		<ns:QueryRests></ns:QueryRests>
      //	</ns:Document>
      //</ns:Documents>

      //AskXml.SaveToFile('c:\QueryRests.xml');
      AskXml.SaveToStream(FS);

      //����� � ����
      quToRar.Active:=False;
      quToRar.ParamByName('ID').AsInteger:=IdF;
      quToRar.ParamByName('RARID').AsString:=CommonSet.FSRAR_ID;
      quToRar.Active:=True;

      quToRar.Append;
      quToRarFSRARID.AsString:=CommonSet.FSRAR_ID;
      quToRarID.AsInteger:=IdF;
      quToRarDATEQU.AsDateTime:=Now;
      quToRarITYPEQU.AsInteger:=1;
      quToRarISTATUS.AsInteger:=1;
      //quToRarSENDFILE.LoadFromFile(NameF);
      quToRarSENDFILE.LoadFromStream(FS);
      quToRar.Post;

      httpsend.MimeType := 'multipart/form-data; boundary='+Boundary;  //���������� Contetn-Type �������
      // ���������� Mime-��� � ������ �� �����
      s:='--'+Boundary+CRLF+'Content-Disposition: form-data; name="xml_file"; filename="'+IdToName(IdF)+'"'+CRLF+'Content-Type: application/xml'+CRLF+CRLF;
      httpsend.Document.Write(PAnsiChar(s)^, Length(s));
      FS.Position := 0;
      httpsend.Document.CopyFrom(FS, FS.Size);  //���������� ����� � ���� ���������
      //S:=CRLF+CRLF+'--'+Boundary+CRLF;  //��������� ���� �������
      S:=CRLF+CRLF+'--'+Boundary+'--'+CRLF;  //��������� ���� �������
      httpsend.Document.Write(PAnsiChar(s)^, Length(s)); // ��������� ���� ���������

      httpsend.KeepAlive:=False;
      httpsend.Status100:=True;
      httpsend.Headers.Add('Accept: */*');

      // ���������� ������
      if httpsend.HTTPMethod('POST','http://'+CommonSet.IPUTM+':8080/opt/in/QueryRests') then
      begin
        ShowMessageLogLocal('TRUE');
        ShowMessageLogLocal('ResultCode='+IntToStr(httpsend.ResultCode));
        ShowMessageLogLocal(httpsend.ResultString);
        //MemoLogLocal.Lines.LoadFromStream(httpsend.Document, TEncoding.UTF8);

        if httpsend.ResultCode<>200 then
        begin
          try
            RetVal:=TStringList.Create;
            RetVal.LoadFromStream(httpsend.Document, TEncoding.UTF8);
            prWMemo(MemoLogLocal,RetVal.Text);
          finally
            RetVal.Free;
          end;
        end;

        httpsend.Document.Position:=0;
        RetXml.LoadFromStream(httpsend.Document);
        RetXml.XmlFormat := xfReadable;
        //RetXml.SaveToFile('C:\1110.xml');

        //RetId:='';
        //nodeRoot := RetXml.Root;
        //if assigned(nodeRoot) then  RetId:=nodeRoot.NodeByName('url').Value;

        RetId:=RetXml.Root.NodeByName('url').Value;

        if RetId>'' then
        begin
          httpsend.Document.Position:=0;

          quToRar.Edit;
          quToRarISTATUS.AsInteger:=2;
          quToRarRECEIVE_ID.AsString:=RetId;
          quToRarRECEIVE_FILE.LoadFromStream(httpsend.Document);
          quToRar.Post;
        end;

      end else begin
        ShowMessageLogLocal('FALSE');
        ShowMessageLogLocal('ResultCode='+IntToStr(httpsend.ResultCode));
        //ShowMessageLogLocal(httpsend.ResultString);

        quToRar.Edit;
        quToRarISTATUS.AsInteger:=100;
        quToRar.Post;
      end;

    finally
      FS.Free;
      AskXml.Free;
      RetXml.Free;
      quToRar.Active:=False;
    end;

    ShowMessageLogLocal('������� ��������.');
  end;
end;

procedure TfmViewRar.acRefreshExecute(Sender: TObject);
begin
  //������
  Init;
end;

procedure TfmViewRar.acSelectDateExecute(Sender: TObject);
begin
  //��������
  Init;
end;

procedure TfmViewRar.acSendActPrihExecute(Sender: TObject);
begin
  //����������� ���������
  with dmR do
  begin
    if quDocInHd.RecordCount>0 then
    begin
      if quDocInHdIACTIVE.AsInteger=0 then
      begin
        // ����� ����������� ��� �����������
        if Trim(quDocInHdWBREGID.AsString)>'' then
        begin
           ClearMessageLogLocal;
           ShowMessageLogLocal('�����.. ���� ������������ ����.');

           if prSendAct(quDocInHdFSRARID.AsString,quDocInHdSID.AsString,quDocInHdIDATE.AsInteger,1,MemoLogLocal) then
           begin
             quDocInHd.Edit;
             quDocInHdIACTIVE.AsInteger:=1;
             quDocInHd.Post;
           end;

           ShowMessageLogLocal('������� ��������.');
        end else ShowMessage('������������ ���� ����������, �.�. ����������� ������������� �������� �� ��� � �����.');
      end else ShowMessage('�������� ������.');
    end;
  end;
end;

procedure TfmViewRar.acSendActR2Execute(Sender: TObject);
begin
  //������������ ��� �����������
  with dmR do
  begin
    if quDocInHd.RecordCount>0 then
    begin
      if (quDocInHdREADYSEND.AsInteger=1) then
      begin
        // ����� ����������� ��� �����������
        if (Trim(quDocInHdWBREGID.AsString)>'') then
        begin
           ClearMessageLogLocal;
           ShowMessageLogLocal('�����.. ���� ������������ ����.');

           if prSendAct(quDocInHdFSRARID.AsString,quDocInHdSID.AsString,quDocInHdIDATE.AsInteger,0,MemoLogLocal) then
           begin
             quDocInHd.Edit;
             quDocInHdIACTIVE.AsInteger:=1;
             quDocInHdDATE_OUTPUT.AsDateTime:=Now;
             quDocInHd.Post;
           end;

           ShowMessageLogLocal('������� ��������.');
        end else ShowMessage('������������ ���� ����������, �.�. ����������� ������������� �������� �� ��� � �����.');
      end else ShowMessage('�������� ������.');
    end;
  end;
end;

procedure TfmViewRar.acSendActRExecute(Sender: TObject);
begin
  //������������ ��� �����������
  with dmR do
  begin
    if quDocInHd.RecordCount>0 then
    begin
      if (quDocInHdIACTIVE.AsInteger=0)and(quDocInHdREADYSEND.AsInteger=1) then
      begin
        // ����� ����������� ��� �����������
        if (Trim(quDocInHdWBREGID.AsString)>'') then
        begin
           ClearMessageLogLocal;
           ShowMessageLogLocal('�����.. ���� ������������ ����.');

           if prSendAct(quDocInHdFSRARID.AsString,quDocInHdSID.AsString,quDocInHdIDATE.AsInteger,0,MemoLogLocal) then
           begin
             quS.Active:=False;
             quS.SQL.Clear;
             quS.SQL.Add('');

             quS.SQL.Add('UPDATE dbo.ADOCSIN_HD');
             quS.SQL.Add('SET IACTIVE = 1');
             quS.SQL.Add(',DATE_OUTPUT = GETDATE()');
             quS.SQL.Add('WHERE');
             quS.SQL.Add('  FSRARID = '''+quDocInHdFSRARID.AsString+'''');
             quS.SQL.Add('  AND IDATE = '+its(quDocInHdIDATE.AsInteger));
             quS.SQL.Add('  AND SID = '''+quDocInHdSID.AsString+'''');
             quS.ExecSQL;

             Init;

{             quDocInHd.Edit;
             quDocInHdIACTIVE.AsInteger:=1;
             quDocInHdDATE_OUTPUT.AsDateTime:=Now;
             quDocInHd.Post;}
           end;

           ShowMessageLogLocal('������� ��������.');
        end else ShowMessage('������������ ���� ����������, �.�. ����������� ������������� �������� �� ��� � �����.');
      end else ShowMessage('�������� ������.');
    end;
  end;
end;

procedure TfmViewRar.acSendActR_HKExecute(Sender: TObject);
var Rec:TcxCustomGridRecord;
    i,j,iC:Integer;
    FSRARID,SID:string;
    IDATE:Integer;
begin
  //������������ ��� �����������  ���������� �� �������
  if (ViewDocs.Controller.SelectedRecordCount>0) and (dmR.FDConnection.Connected) then
  begin
    ClearMessageLogLocal;
    ShowMessageLogLocal('����� ... ���� ��������� ����������.');
    iC:=0;

    FSRARID:='';
    SID:='';
    IDATE:=0;

    for i:=0 to ViewDocs.Controller.SelectedRecordCount-1 do
    begin
      Rec:=ViewDocs.Controller.SelectedRecords[i];

      for j:=0 to Rec.ValueCount-1 do
      begin
        if ViewDocs.Columns[j].Name='ViewDocsFSRARID' then begin FSRARID:=Rec.Values[j]; end;
        if ViewDocs.Columns[j].Name='ViewDocsIDATE' then begin IDATE:=Rec.Values[j]; end;
        if ViewDocs.Columns[j].Name='ViewDocsSID' then begin SID:=Rec.Values[j]; end;
      end;
      if (FSRARID>'')and(IDATE>0)and(SID>'') then
      begin
        with dmR do
        begin
          ShowMessageLogLocal('    ���. '+FSRARID+','+ds1(iDate)+','+SID);

          WriteHistoryIn(FSRARID,SID,IDATE,'��������� ��������');

          if prSendAct(FSRARID,SID,IDATE,0,MemoLogLocal) then
          begin
            ShowMessageLogLocal('  Ok');
          end;

          inc(iC);
        end;
      end;
    end;
    init;
    ShowMessageLogLocal('���������� '+its(iC)+' ����������.');
    ShowMessageLogLocal('������� ��������.');
  end;
end;

procedure TfmViewRar.acSendRetExecute(Sender: TObject);
begin
  //������������ ��� ��������
  with dmR do
  begin
    if quDocOutHd.RecordCount>0 then
    begin
      if (quDocOutHdIACTIVE.AsInteger=0) then
      begin
        // ����� ����������� ��� �����������

         ClearMessageLogLocal;
         ShowMessageLogLocal('�����.. ���� ������������ ��������� ��������.');

         if prSendTTN(quDocOutHdFSRARID.AsString,quDocOutHdSID.AsString,quDocOutHdIDATE.AsInteger,MemoLogLocal) then
         begin
            try
              quS.Active:=False;
              quS.SQL.Clear;
              quS.SQL.Add('');

              quS.SQL.Add('UPDATE dbo.ADOCSOUT_HD');
              quS.SQL.Add('SET IACTIVE = 1');
              quS.SQL.Add('WHERE');
              quS.SQL.Add('  FSRARID = '''+quDocOutHdFSRARID.AsString+'''');
              quS.SQL.Add('  AND IDATE = '+its(quDocOutHdIDATE.AsInteger));
              quS.SQL.Add('  AND SID = '''+quDocOutHdSID.AsString+'''');
              quS.ExecSQL;

              Init;
            except
            end;
         end;

         ShowMessageLogLocal('������� ��������.');
      end else ShowMessage('�������� ������.');
    end;
  end;
end;

procedure TfmViewRar.acSendStatusExecute(Sender: TObject);
var Rec:TcxCustomGridRecord;
    i,j,iC:Integer;
    FSRARID,SID:string;
    IDATE:Integer;
begin
  //��������� ������ ����� � �������� �� ���������
  if ViewDocs.Controller.SelectedRecordCount>0 then
  begin
    ClearMessageLogLocal;
    ShowMessageLogLocal('����� ... ���� ��������� ����������.');
    iC:=0;

    FSRARID:='';
    SID:='';
    IDATE:=0;

    for i:=0 to ViewDocs.Controller.SelectedRecordCount-1 do
    begin
      Rec:=ViewDocs.Controller.SelectedRecords[i];

      for j:=0 to Rec.ValueCount-1 do
      begin
        if ViewDocs.Columns[j].Name='ViewDocsFSRARID' then begin FSRARID:=Rec.Values[j]; end;
        if ViewDocs.Columns[j].Name='ViewDocsIDATE' then begin IDATE:=Rec.Values[j]; end;
        if ViewDocs.Columns[j].Name='ViewDocsSID' then begin SID:=Rec.Values[j]; end;
      end;
      if (FSRARID>'')and(IDATE>0)and(SID>'') then
      begin
        with dmR do
        begin
          ShowMessageLogLocal('    ���. '+FSRARID+','+ds1(iDate)+','+SID);
          quProc.Active:=False;
          quProc.SQL.Clear;
          quProc.SQL.Add('');

          quProc.SQL.Add('DECLARE @FSRARID varchar(50) = '''+FSRARID+'''');
          quProc.SQL.Add('DECLARE @SID varchar(50) = '''+SID+'''');
          quProc.SQL.Add('DECLARE @IDATE int = '+its(iDate));
          quProc.SQL.Add('EXECUTE [dbo].[prSetSendStatusDoc] @FSRARID,@SID,@IDATE');
          quProc.Active:=True;

          ShowMessageLogLocal('         ---- ������ � �������� ');
          quProc.Active:=False;
          inc(iC);
        end;
      end;
    end;
    init;
    ShowMessageLogLocal('���������� '+its(iC)+' ����������.');
    ShowMessageLogLocal('������� ��������.');
  end;

end;

procedure TfmViewRar.acSetNumberExecute(Sender: TObject);
begin
//
end;

procedure TfmViewRar.acUnCompareExecute(Sender: TObject);
var Rec:TcxCustomGridRecord;
    i,j,iC:Integer;
    FSRARID,SID:string;
    IDATE, IACTIVE:Integer;
begin
  //�������������� ���.��������
  if (ViewDocs.Controller.SelectedRecordCount>0) and (dmR.FDConnection.Connected) then
  begin
    ClearMessageLogLocal;
    ShowMessageLogLocal('����� ... ���� ��������� ����������.');
    iC:=0;

    FSRARID:='';
    SID:='';
    IDATE:=0;

    for i:=0 to ViewDocs.Controller.SelectedRecordCount-1 do
    begin
      Rec:=ViewDocs.Controller.SelectedRecords[i];

      for j:=0 to Rec.ValueCount-1 do
      begin
        if ViewDocs.Columns[j].Name='ViewDocsFSRARID' then begin FSRARID:=Rec.Values[j]; end;
        if ViewDocs.Columns[j].Name='ViewDocsIDATE' then begin IDATE:=Rec.Values[j]; end;
        if ViewDocs.Columns[j].Name='ViewDocsSID' then begin SID:=Rec.Values[j]; end;
        if ViewDocs.Columns[j].Name='ViewDocsIACTIVE' then begin IACTIVE:=Rec.Values[j]; end;
      end;
      if IACTIVE=1 then begin
        ShowMessageLogLocal('    ���. '+FSRARID+','+ds1(iDate)+','+SID+' � ������� "���������", ��������� � ��� ���������.');
        Continue;
      end;
      if (FSRARID>'')and(IDATE>0)and(SID>'') then
      begin
        with dmR do
        begin
          ShowMessageLogLocal('    ���. '+FSRARID+','+ds1(iDate)+','+SID);

          quProc.Active:=False;
          quProc.SQL.Clear;
          quProc.SQL.Add('DECLARE @FSRARID varchar(50) = '''+FSRARID+'''');
          quProc.SQL.Add('DECLARE @SID varchar(50) = '''+SID+'''');
          quProc.SQL.Add('DECLARE @IDATE int = '+its(iDate));
          quProc.SQL.Add('UPDATE dbo.ADOCSIN_HD');
          quProc.SQL.Add('SET');
          quProc.SQL.Add('  IDHEAD = 0');
          quProc.SQL.Add('WHERE');
          quProc.SQL.Add('  FSRARID = @FSRARID');
          quProc.SQL.Add('  AND IDATE = @IDATE');
          quProc.SQL.Add('  AND SID = @SID');
          quProc.ExecSQL;

          quProc.Active:=False;
          quProc.SQL.Clear;
          quProc.SQL.Add('DECLARE @FSRARID varchar(50) = '''+FSRARID+'''');
          quProc.SQL.Add('DECLARE @SID varchar(50) = '''+SID+'''');
          quProc.SQL.Add('DECLARE @IDATE int = '+its(iDate));
          quProc.SQL.Add('UPDATE [dbo].[ADOCSIN_SP]');
          quProc.SQL.Add('SET [QUANTF] = 0,');
          quProc.SQL.Add('    [PRICEF] = 0,');
          quProc.SQL.Add('    [PRISEF0] = 0');
          quProc.SQL.Add('WHERE [FSRARID] = @FSRARID');
          quProc.SQL.Add('  AND [IDATE] = @IDATE');
          quProc.SQL.Add('  AND [SIDHD] = @SID');
          quProc.ExecSQL;

          inc(iC);
        end;
      end;
    end;
    init;
    ShowMessageLogLocal('���������� '+its(iC)+' ����������.');
    ShowMessageLogLocal('������� ��������.');
  end;
end;

procedure TfmViewRar.acUTMArhExecute(Sender: TObject);
begin
  //��� �����
  ShowFormReplyListArh;
end;

procedure TfmViewRar.acUTMExchExecute(Sender: TObject);
begin
  //��� �������
  ShowFormUTMExch;
end;

procedure TfmViewRar.acUTMProcExecute(Sender: TObject);
var
  xmlstr:string;
begin
  //�������� �����

  //������ ������ ��������
  //curl -X GET http://localhost:8080/opt/in >quest_list.xml
  ClearMessageLogLocal;

  with dmR do
  begin
    if FDConnection.Connected then
    begin
      ClearMessageLogLocal;
      ShowMessageLogLocal('�������� ������ ��������.');
      prGetListAsk(MemoLogLocal);

      ShowMessageLogLocal('�������� ������ �������.');
      prGetListAsw(MemoLogLocal);

      ShowMessageLogLocal('�������� ����� �������.');
      prGetFilesAsw(MemoLogLocal);

      ShowMessageLogLocal('���������� ����� �������.');
      prDecodeFilesAsw(MemoLogLocal);

      Init;

      ShowMessageLogLocal('������� ��������.');
    end;
  end;
end;

procedure TfmViewRar.beiPointChange(Sender: TObject);
begin
//  ShowMessageLogLocal(its(beiPoint.EditValue));
  ClearMessageLogLocal;
  with dmR do
  begin
//    ShowMessageLogLocal(quPointRARID.AsString+' '+its(quPointISHOP.AsInteger)+' '+quPointName.AsString+' '+quPointIPUTM.AsString+' '+quPointIPDB.AsString);
    if quPoint.Locate('ISHOP',beiPoint.EditValue,[]) then
    begin
//      ShowMessageLogLocal(quPointRARID.AsString+' '+its(quPointISHOP.AsInteger)+' '+quPointName.AsString+' '+quPointIPUTM.AsString+' '+quPointIPDB.AsString);
      if dmR.DBConnect(quPointIPDB.AsString,quPointDBNAME.AsString,'sa',quPointPASSW.AsString) then
      begin
        ShowMessageLogLocal('����� � ����� '+quPointIPDB.AsString+' ��');
        CommonSet.FSRAR_ID:=quPointRARID.AsString;
        CommonSet.IPUTM:=quPointIPUTM.AsString;
        CommonSet.ISS:=quPointISS.AsInteger;
        CommonSet.SDEP:=Trim(quPointSDEP.AsString);
      end else
      begin
        ShowMessageLogLocal('������ ����� � ����� '+quPointIPDB.AsString);
        dmR.FDConnection.Close;
      end;
    end;
  end;
end;

procedure TfmViewRar.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
  fmViewRar:=Nil;
end;

procedure TfmViewRar.FormCreate(Sender: TObject);
begin
  ClearMessageLogLocal;
end;

procedure TfmViewRar.GridRarFocusedViewChanged(Sender: TcxCustomGrid;
  APrevFocusedView, AFocusedView: TcxCustomGridView);
begin
  if LevelDocs.Active then
  begin
    acSendActR.Enabled:=True;
    acCompare.Enabled:=True;
    acCreateReturn.Enabled:=True;
    acAuto.Enabled:=True;
    acSendRet.Enabled:=False;
  end;
  if LevelDocsOut.Active then
  begin
    acSendActR.Enabled:=False;
    acCompare.Enabled:=False;
    acCreateReturn.Enabled:=False;
    acAuto.Enabled:=False;
    acSendRet.Enabled:=True;
  end;
end;

procedure TfmViewRar.deDateBegChange(Sender: TObject);
begin
  if dmR.FDConnection.Connected then Init;
end;

end.
