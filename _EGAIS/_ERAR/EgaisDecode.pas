unit EgaisDecode;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics,
  nativexml,Vcl.Dialogs,FireDAC.Comp.Client, StrUtils,
  httpsend,
  cxMemo,
  Data.DB,
  Controls;

function its(i:Integer):String;
function ds1(d:TDateTime):String;
function dssql(d:TDateTime):String;
function dssqltime(d:TDateTime):String;
function dsrar(d:TDateTime):String;
function dtsrar(d:TDateTime):String;
function dsfromrar(s:String):TDateTime;
function dtsfromrar(s:String):TDateTime;

function prDecodeCards(xmlstr:string; FDConn: TFDConnection):Boolean;

function prDecodeAct(xmlstr:string;IdRec:integer; FDConn: TFDConnection):Boolean;
function prDecodeTTN(xmlstr:string;IdRec:integer; FDConn: TFDConnection; RARID:String):Boolean;
function prDecodeTTN_AB(xmlstr,ssid:string):Boolean;
function prDecodeWB(xmlstr:string;IdRec:integer; FDConn: TFDConnection; RARID:string):Boolean;
function prDecodeTicket(xmlstr:string;IdRec:integer; FDConn: TFDConnection; RARID:String):Boolean;
function prDecodeNoAnswerTTN(xmlstr:string;IdRec:integer; FDConn: TFDConnection):Boolean;
function prDecodeQueryBarCode(xmlstr:string;IdRec:integer; FDConn: TFDConnection):Boolean;
function prDecodeActInv(xmlstr:string;IdRec:integer; FDConn: TFDConnection; RARID:String):Boolean;


function fGetId(iT:Integer):Integer;

function stf(sV:String):Real;
Function fts(rSum:Real):String;
Function fts00(rSum:Real):String;
Function fts0(rSum:Real):String;

function tf(testnode: TXmlNode):String;
function prSendAct(RARID,SID:String;IDATE,iPrih:Integer;Memo1:tcxMemo):Boolean;
function prCreateRet(RARID,SID:String;IDATE:Integer;Memo1:tcxMemo):Boolean;
function prSendTTN(RARID,SID:String;IDATE:Integer;Memo1:tcxMemo):Boolean;
function prSendDocVn(RARID,SID:String;IDATE:Integer;Memo1:tcxMemo):Boolean;
function prSendKAP(RARID,KAP:String;Memo1:tcxMemo):Boolean;
function prSendDocCorr(RARID,SID:String;IDATE:Integer;Memo1:tcxMemo):Boolean;


function R1000( X: Double ): Double;
function RoundEx( X: Double ): Integer;
function strtest(Str1:string):string;


//��������� ��������� ��� ������������� � �������

procedure prGetListAskPrivate(Memo1:tcxMemo; FDConn: TFDConnection; IPUTM,FSRAR_ID:String);
procedure prGetListAswPrivate(Memo1:tcxMemo; FDConn: TFDConnection; IPUTM,FSRAR_ID:String);
procedure prGetFilesAswPrivate(Memo1:tcxMemo; FDConn: TFDConnection; IPUTM,FSRAR_ID:String);
procedure prDecodeFilesAswPrivate(Memo1:tcxMemo; FDConn: TFDConnection; IPUTM,FSRAR_ID:String);
procedure WriteHistoryInPrivate(RARID,SID:String;IDATE:Integer;sComment:String;FDConn: TFDConnection);
procedure WriteHistoryOutPrivate(RARID,SID:String;IDATE:Integer;sComment:String;FDConn: TFDConnection);
function prSendActPrivate(RARID,SID:String;IDATE,iPrih:Integer;Memo1:tcxMemo; FDConn: TFDConnection; IPUTM:String):Boolean;
function fGetIdPrivate(iT:Integer;FDConn: TFDConnection):Integer;
function prGetRemn1Private(RARID:String; Memo1:tcxMemo; FDConn: TFDConnection; IPUTM:String):Boolean;
function prSendDocVnPrivate(RARID,SID:String;IDATE:Integer;Memo1:tcxMemo; FDConn: TFDConnection; IPUTM:String):Boolean;
function prSendDocVn1Private(RARID,SID:String;IDATE:Integer;Memo1:tcxMemo; FDConn: TFDConnection; IPUTM:String):Boolean;


type

   TCommonSet = record
     AppName: string;
     FSRAR_ID: String;
     TmpDir: String;
     ReportsDir: String;
     IPUTM:string;
     ISS:SmallInt;
     SDEP:string;
   end;

   TCli = record
              ClientRegId,FullName,ShortName,INN,KPP,Country,RegionCode,description:string;
          end;



type
   TCommonSetTR = record
                  PathHistory:String;
                  PeriodSec:Integer;
                  writelog:Integer;
                end;


Var
  CommonSet:TCommonSet;
  CommonSetTR:TCommonSetTR;


Const
  CR = #$0d;
  LF = #$0a;
  CRLF = CR + LF;
  Boundary = 'END_OF_PART';


implementation

uses dmRar;

function prGetRemn1Private(RARID:String; Memo1:tcxMemo; FDConn: TFDConnection; IPUTM:String):Boolean;
Const
  CR = #$0d;
  LF = #$0a;
  CRLF = CR + LF;
  Boundary = 'END_OF_PART';

var  AskXml: TNativeXml;
     IdF:Integer;
     RetXml: TNativeXml;
     RetId:string;

     httpsend: THTTPSend;
     s: AnsiString;
     FS: TMemoryStream;
     RetVal:TStringList;
     quToRar: TFDQuery;
begin
  if FDConn.Connected then
  begin
    try
      Result:=False;

      IdF:=fGetIdPrivate(1,FDConn);
      prWMemo(Memo1,'  ��������� ������ �� ��������� �������� �� 1 �������� ('+its(IdF)+')');

      FS := TMemoryStream.Create;
      httpsend := THTTPSend.Create;

      //��������� ���� �������
      AskXml := TNativeXml.Create(nil);
      RetXml := TNativeXml.Create(nil);

      AskXml.XmlFormat := xfReadable;
      AskXml.CreateName('ns:Documents');
      AskXml.Root.WriteAttributeString('Version', '1.0');
      AskXml.Root.WriteAttributeString('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
      AskXml.Root.WriteAttributeString('xmlns:ns', 'http://fsrar.ru/WEGAIS/WB_DOC_SINGLE_01');
      AskXml.Root.WriteAttributeString('xmlns:qp', 'http://fsrar.ru/WEGAIS/QueryParameters');
      AskXml.Root.NodeNew('ns:Owner');
      AskXml.Root.NodeByName('ns:Owner').NodeNew('ns:FSRAR_ID').Value:=RARID;

      AskXml.Root.NodeNew('ns:Document');
      AskXml.Root.NodeByName('ns:Document').NodeNew('ns:QueryRests');
      AskXml.Root.NodeByName('ns:Document').NodeByName('ns:QueryRests').Value:='';

      //AskXml.SaveToFile('c:\QueryRests.xml');
      AskXml.SaveToStream(FS);

      //����� � ����
      quToRar := TFDQuery.Create(nil);
      quToRar.Connection := FDConn;
      quToRar.ResourceOptions.SilentMode:=True;     //�� ���������� Wait cursor

      quToRar.Active:=False;
      quToRar.SQL.Clear;
      quToRar.SQL.Add('');
      quToRar.SQL.Add('SELECT [FSRARID],[ID],[DATEQU],[ITYPEQU],[ISTATUS],[SENDFILE],[RECEIVE_ID],[RECEIVE_FILE] FROM [dbo].[TORAR]');
      quToRar.SQL.Add('where [ID]='+its(IdF));
      quToRar.SQL.Add('and [FSRARID]='''+RARID+'''');
      quToRar.Active:=True;

      quToRar.Append;
      quToRar.FieldByName('FSRARID').AsString:=RARID;
      quToRar.FieldByName('ID').AsInteger:=IdF;
      quToRar.FieldByName('DATEQU').AsDateTime:=Now;
      quToRar.FieldByName('ITYPEQU').AsInteger:=1;
      quToRar.FieldByName('ISTATUS').AsInteger:=1;
//        quToRarSENDFILE.LoadFromFile(NameF);
      FS.Position := 0;
      quToRar.FieldByName('SENDFILE').AsString:='';
      TMemoField(quToRar.FieldByName('SENDFILE')).LoadFromStream(FS);
      quToRar.Post;

      httpsend.MimeType := 'multipart/form-data; boundary='+Boundary;  //���������� Contetn-Type �������
      // ���������� Mime-��� � ������ �� �����
      s:='--'+Boundary+CRLF+'Content-Disposition: form-data; name="xml_file"; filename="'+IdToName(IdF)+'"'+CRLF+'Content-Type: application/xml'+CRLF+CRLF;
      httpsend.Document.Write(PAnsiChar(s)^, Length(s));
      FS.Position := 0;
      httpsend.Document.CopyFrom(FS, FS.Size);  //���������� ����� � ���� ���������
      //S:=CRLF+CRLF+'--'+Boundary+CRLF;  //��������� ���� �������
      S:=CRLF+CRLF+'--'+Boundary+'--'+CRLF;  //��������� ���� �������
      httpsend.Document.Write(PAnsiChar(s)^, Length(s)); // ��������� ���� ���������

      httpsend.KeepAlive:=False;
      httpsend.Status100:=True;
      httpsend.Headers.Add('Accept: */*');

      // ���������� ������
      if httpsend.HTTPMethod('POST','http://'+IPUTM+':8080/opt/in/QueryRests') then
      begin
        prWMemo(Memo1,'������ �������� 1  ( ResultCode='+IntToStr(httpsend.ResultCode)+' )');
        prWMemo(Memo1,httpsend.ResultString);
        //MemoLogLocal.Lines.LoadFromStream(httpsend.Document, TEncoding.UTF8);

        if httpsend.ResultCode<>200 then
        begin
          try
            RetVal:=TStringList.Create;
            RetVal.LoadFromStream(httpsend.Document, TEncoding.UTF8);
            prWMemo(Memo1,RetVal.Text);
          finally
            RetVal.Free;
          end;
        end else Result:=True;

        httpsend.Document.Position:=0;
        RetXml.LoadFromStream(httpsend.Document);
        RetXml.XmlFormat := xfReadable;

        if Assigned(RetXml.Root.NodeByName('url')) then RetId:=RetXml.Root.NodeByName('url').Value;

        if RetId>'' then
        begin
          httpsend.Document.Position:=0;

          quToRar.Edit;
          quToRar.FieldByName('ISTATUS').AsInteger:=2;
          quToRar.FieldByName('RECEIVE_ID').AsString:=RetId;
          TMemoField(quToRar.FieldByName('RECEIVE_FILE')).LoadFromStream(httpsend.Document);
          quToRar.Post;
        end;

      end else begin
        prWMemo(Memo1,'FALSE');
        prWMemo(Memo1,'ResultCode='+IntToStr(httpsend.ResultCode));

        quToRar.Edit;
        quToRar.FieldByName('ISTATUS').AsInteger:=100;
        quToRar.Post;
      end;

    finally
      quToRar.Free;
      FS.Free;
      AskXml.Free;
      RetXml.Free;
      quToRar.Active:=False;
    end;
  end;
end;

procedure WriteHistoryOutPrivate(RARID,SID:String;IDATE:Integer;sComment:String;FDConn: TFDConnection);
var quS: TFDQuery;
begin
  try
    quS := TFDQuery.Create(nil);
    quS.Connection := FDConn;
    quS.ResourceOptions.SilentMode:=True;     //�� ���������� Wait cursor

    quS.Active:=False;
    quS.SQL.Clear;
    quS.SQL.Add('');
    quS.SQL.Add('DECLARE @FSRARID varchar(50) = '''+Trim(RARID)+'''');
    quS.SQL.Add('DECLARE @IDATE int = '+IntToStr(iDate));
    quS.SQL.Add('DECLARE @SID varchar(50) = '''+Trim(SID)+'''');
    quS.SQL.Add('DECLARE @COMMENT varchar(500) ='''+Trim(sComment)+'''');

    quS.SQL.Add('EXECUTE [dbo].[prAddHistoryOut] @FSRARID ,@IDATE ,@SID ,@COMMENT');
    quS.ExecSQL;
  finally
    quS.Free;
  end;
end;


procedure WriteHistoryInPrivate(RARID,SID:String;IDATE:Integer;sComment:String;FDConn: TFDConnection);
var quS: TFDQuery;
begin
  try
    quS := TFDQuery.Create(nil);
    quS.Connection := FDConn;
    quS.ResourceOptions.SilentMode:=True;     //�� ���������� Wait cursor

    quS.Active:=False;
    quS.SQL.Clear;
    quS.SQL.Add('');
    quS.SQL.Add('DECLARE @FSRARID varchar(50) = '''+Trim(RARID)+'''');
    quS.SQL.Add('DECLARE @IDATE int = '+IntToStr(iDate));
    quS.SQL.Add('DECLARE @SID varchar(50) = '''+Trim(SID)+'''');
    quS.SQL.Add('DECLARE @COMMENT varchar(500) ='''+Trim(sComment)+'''');

    quS.SQL.Add('EXECUTE [dbo].[prAddHistoryIn] @FSRARID ,@IDATE ,@SID ,@COMMENT');
    quS.ExecSQL;
  finally
    quS.Free;
  end;
end;


procedure prDecodeFilesAswPrivate(Memo1:tcxMemo; FDConn: TFDConnection; IPUTM,FSRAR_ID:String);
var
  xmlstr:string;
  iC,iCAll:Integer;
  tQuery: TFDQuery;

begin
  if FDConn.Connected then
  begin
    try
      iC:=0; iCAll:=0;

      tQuery := TFDQuery.Create(nil);
      tQuery.Connection := FDConn;
      tQuery.ResourceOptions.SilentMode:=True;     //�� ���������� Wait cursor

      tQuery.SQL.Clear;
      tQuery.SQL.Add('');
      tQuery.SQL.Add('SELECT [FSRARID],[ID],[ReplyId],[ReplyPath],[ReplyFile],[IACTIVE],[IDEL],[DateReply]');
      tQuery.SQL.Add('FROM [dbo].[REPLYLIST]');
      tQuery.SQL.Add('where IACTIVE=1 and IDEL=0 and [FSRARID]='''+FSRAR_ID+'''');
      tQuery.Active:=True;

      tQuery.First;
      while not tQuery.Eof do
      begin
        xmlstr:=UTF8ToString(tQuery.FieldByName('ReplyFile').AsString);
        if Pos('ReplyAP',xmlstr)>0 then //��� ������
        begin
          prWMemo(Memo1,'  Decode ReplyAP '+tQuery.FieldByName('ID').AsString+' - ���� ��������� �� �������.');
          if prDecodeCards(UTF8ToString(tQuery.FieldByName('ReplyFile').AsString),FDConn) then
          begin
            inc(iC);
            tQuery.Edit;
            tQuery.FieldByName('IACTIVE').AsInteger:=3;
            tQuery.Post;
          end;
        end;
        if (Pos('ns:WayBillAct',xmlstr)>0)or(Pos('ns:WAYBILLACT',xmlstr)>0) then //��� ��������
        begin
          prWMemo(Memo1,'  Decode WayBillAct '+tQuery.FieldByName('ID').AsString+' - ���� ��������� ����.');
          if prDecodeAct(UTF8ToString(tQuery.FieldByName('ReplyFile').AsString),tQuery.FieldByName('ID').AsInteger,FDConn) then
          begin
            inc(iC);
            tQuery.Edit;
            tQuery.FieldByName('IACTIVE').AsInteger:=3;
            tQuery.Post;
          end;
        end;
        if ((Pos('ns:WayBill',xmlstr)>0)or(Pos('ns:WAYBILL',xmlstr)>0))and(Pos('ns:WayBillAct',xmlstr)=0) then //��� ��������
        begin
          prWMemo(Memo1,'  Decode WayBill '+tQuery.FieldByName('ID').AsString+' - ���� ��������� ���������.');
          if prDecodeTTN(UTF8ToString(tQuery.FieldByName('ReplyFile').AsString),tQuery.FieldByName('ID').AsInteger,FDConn,FSRAR_ID) then
          begin
            inc(iC);
            tQuery.Edit;
            tQuery.FieldByName('IACTIVE').AsInteger:=3;
            tQuery.Post;
          end;
        end;
        if Pos('ns:TTNInformBReg',xmlstr)>0 then //��� �������������� ���� � ���������
        begin
          prWMemo(Memo1,'  Decode TTNInformBReg '+tQuery.FieldByName('ID').AsString+' - ���� ��������� ��������������� �����.');
          if prDecodeWB(UTF8ToString(tQuery.FieldByName('ReplyFile').AsString),tQuery.FieldByName('ID').AsInteger,FDConn,FSRAR_ID) then
          begin
            inc(iC);

            tQuery.Edit;
            tQuery.FieldByName('IACTIVE').AsInteger:=3;
            tQuery.Post;
          end;
        end;
        if Pos('ns:Ticket',xmlstr)>0 then //��� �������������� ���� � ���������
        begin
          prWMemo(Memo1,'  Decode Ticket '+tQuery.FieldByName('ID').AsString+' - ���� ��������� ��������������� �����.');
          if prDecodeTicket(UTF8ToString(tQuery.FieldByName('ReplyFile').AsString),tQuery.FieldByName('ID').AsInteger,FDConn,FSRAR_ID) then
          begin
            inc(iC);

            tQuery.Edit;
            tQuery.FieldByName('IACTIVE').AsInteger:=3;
            tQuery.Post;
          end;
        end;
        if Pos('ns:ReplyNoAnswerTTN',xmlstr)>0 then //��� ������ �������������� ���
        begin
          prWMemo(Memo1,'  Decode ReplyNoAnswerTTN '+tQuery.FieldByName('ID').AsString+' - ���� ��������� �������������� ���.');
          if prDecodeNoAnswerTTN(UTF8ToString(tQuery.FieldByName('ReplyFile').AsString),tQuery.FieldByName('ID').AsInteger,FDConn) then
          begin
            inc(iC);

            tQuery.Edit;
            tQuery.FieldByName('IACTIVE').AsInteger:=3;
            tQuery.Post;
          end;
        end;
        if Pos('ns:ReplyRests',xmlstr)>0 then //��� �������������� ���� � ���������
        begin
          prWMemo(Memo1,'  Decode ReplyRests '+tQuery.FieldByName('ID').AsString+' - ���� ��������� ��������������.');
          if prDecodeActInv(UTF8ToString(tQuery.FieldByName('ReplyFile').AsString),tQuery.FieldByName('ID').AsInteger,FDConn,FSRAR_ID) then
          begin
            inc(iC);

            tQuery.Edit;
            tQuery.FieldByName('IACTIVE').AsInteger:=3;
            tQuery.Post;
          end;
        end;

        inc(iCAll);
        tQuery.Next;
      end;
    finally
      tQuery.Free;
      prWMemo(Memo1,' ����� ������������ '+its(iC)+' �� '+its(iCAll));
    end;
  end;
end;


procedure prGetFilesAswPrivate(Memo1:tcxMemo; FDConn: TFDConnection; IPUTM,FSRAR_ID:String);
var
  httpsend: THTTPSend;
  RetXml: TNativeXml;
  nodeRoot,nodeURL: TXmlNode;
  I:Integer;
  RetId,RetVal:String;
  ReplyPath:string;
  iBs:Integer;
  tQuery: TFDQuery;
//  StrMemo:TcxMemo;

begin
  if FDConn.Connected then
  begin
    try
      prWMemo(Memo1,'�������� ����� �������.');

      tQuery := TFDQuery.Create(nil);
      tQuery.Connection := FDConn;
      tQuery.ResourceOptions.SilentMode:=True;     //�� ���������� Wait cursor

      tQuery.SQL.Clear;
      tQuery.SQL.Add('');
      tQuery.SQL.Add('SELECT [FSRARID],[ID],[ReplyId],[ReplyPath],[ReplyFile],[IACTIVE],[IDEL],[DateReply]');
      tQuery.SQL.Add('FROM [dbo].[REPLYLIST]');
      tQuery.SQL.Add('where IACTIVE=1 and IDEL=0 and [FSRARID]='''+FSRAR_ID+'''');
      tQuery.Active:=True;

      tQuery.First;
      while not tQuery.Eof do
      begin
        try
          httpsend:=THTTPSend.Create;
          httpsend.Protocol:='1.1';
          httpsend.Status100:=True;

          ReplyPath:=tQuery.FieldByName('ReplyPath').AsString;
          if Pos('localhost',ReplyPath)>0 then
          begin
            iBs:=Pos('localhost',ReplyPath);
            delete(ReplyPath,iBs,9);
            Insert(IPUTM,ReplyPath,iBs);
          end;

          if httpsend.HTTPMethod('get',ReplyPath) then
          begin
            if httpsend.ResultCode=200 then
            begin
              prWMemo(Memo1,'    Get file  TRUE '+'ResultCode='+IntToStr(httpsend.ResultCode)+' '+httpsend.ResultString);

              httpsend.Document.Position:=0;

              if (tQuery.FieldByName('ReplyFile') is TMemoField) then
              begin
                tQuery.Edit;
                tQuery.FieldByName('ReplyFile').AsString:='';
                TMemoField(tQuery.FieldByName('ReplyFile')).LoadFromStream(httpsend.Document);
                Delay(100);
                tQuery.Post;
              end;
            end else prWMemo(Memo1,'    Get file FALSE '+'ResultCode='+IntToStr(httpsend.ResultCode)+' '+httpsend.ResultString);
          end else
          begin
            prWMemo(Memo1,'    Get file FALSE '+'ResultCode='+IntToStr(httpsend.ResultCode)+' '+httpsend.ResultString);
          end;
        finally
          httpsend.Free;
        end;

        tQuery.Next;
      end;
    finally
      tQuery.Free;
    end;
  end;
end;


procedure prGetListAswPrivate(Memo1:tcxMemo; FDConn: TFDConnection; IPUTM,FSRAR_ID:String);
var
  httpsend: THTTPSend;
  RetXml: TNativeXml;
  nodeRoot,nodeURL: TXmlNode;
  I:Integer;
  RetId,RetVal:String;
  tQuery: TFDQuery;
begin
  if FDConn.Connected then
  begin
    try
      httpsend:=THTTPSend.Create;
      RetXml := TNativeXml.Create(nil);
      tQuery := TFDQuery.Create(nil);
      tQuery.Connection := FDConn;
      tQuery.ResourceOptions.SilentMode:=True;     //�� ���������� Wait cursor

      if httpsend.HTTPMethod('get','http://'+IPUTM+':8080/opt/out?refresh=true') then
      begin
        prWMemo(Memo1,'    TRUE '+'ResultCode='+IntToStr(httpsend.ResultCode));
  //          prWMemo(Memo1,'ResultCode='+IntToStr(httpsend.ResultCode));
  //          prWMemo(Memo1,httpsend.ResultString);
  //          Memo1.Lines.LoadFromStream(httpsend.Document, TEncoding.UTF8);

        httpsend.Document.Position:=0;
        RetXml.LoadFromStream(httpsend.Document);
        RetXml.XmlFormat := xfReadable;

        nodeRoot := RetXml.Root;
        if assigned(nodeRoot) then
        begin
          prWMemo(Memo1,'    �������� ���� "'+nodeRoot.Name+'" ���-�� ��������� '+inttostr(nodeRoot.AttributeCount)+', ���-�� ����� '+inttostr(nodeRoot.NodeCount));
          for I := 0 to nodeRoot.NodeCount - 1 do
          begin
            nodeURL:=nodeRoot.Nodes[I];
  //              prWMemo(Memo1,'        �������� ���� "'+nodeURL.Name+'" ���-�� ��������� '+inttostr(nodeURL.AttributeCount)+', ���-�� ����� '+inttostr(nodeURL.NodeCount)+',�������� '+nodeURL.Value);
            if Pos('url',nodeURL.Name)>0 then
            begin
              RetId:=Trim(nodeURL.ReadAttributeString('replyId',''));
              RetVal:=Trim(nodeURL.Value);
              prWMemo(Memo1,'    I = '+its(I)+'    '+RetId+'  '+RetVal);
              if (RetId>'')or(RetVal>'') then
              begin
                tQuery.SQL.Clear;
                tQuery.SQL.Add('');
                tQuery.SQL.Add('DECLARE @IdList varchar(100) = '''+RetId+'''');
                tQuery.SQL.Add('DECLARE @IdVal varchar(500) = '''+RetVal+'''');
                tQuery.SQL.Add('DECLARE @Rarid varchar(50) = '''+FSRAR_ID+'''');
                tQuery.SQL.Add('EXECUTE [dbo].[prAddPosReplyList] @IdList,@IdVal,@Rarid');
                tQuery.ExecSQL;
                prWMemo(Memo1,'    ��������.');
              end;
            end;
          end;
        end;
      end else
      begin
        prWMemo(Memo1,'    FALSE '+'ResultCode='+IntToStr(httpsend.ResultCode));
      end;

    finally
      tQuery.Free;
      httpsend.Free;
      RetXml.Free;
    end;
  end;
end;


procedure prGetListAskPrivate(Memo1:tcxMemo; FDConn: TFDConnection; IPUTM,FSRAR_ID:String);
var
  httpsend: THTTPSend;
  RetXml: TNativeXml;
  nodeRoot,nodeURL: TXmlNode;
  I:Integer;
  RetId,RetVal:String;
  tQuery: TFDQuery;
begin
  if FDConn.Connected then
  begin
    try
      httpsend:=THTTPSend.Create;
      RetXml := TNativeXml.Create(nil);

      tQuery := TFDQuery.Create(nil);
      tQuery.Connection := FDConn;
      tQuery.ResourceOptions.SilentMode:=True;     //�� ���������� Wait cursor


      if httpsend.HTTPMethod('get','http://'+IPUTM+':8080/opt/in') then
      begin
        prWMemo(Memo1,'    TRUE '+'ResultCode='+IntToStr(httpsend.ResultCode));

        httpsend.Document.Position:=0;
        RetXml.LoadFromStream(httpsend.Document);
        RetXml.XmlFormat := xfReadable;

        nodeRoot := RetXml.Root;
        if assigned(nodeRoot) then
        begin
          prWMemo(Memo1,'    �������� ���� "'+nodeRoot.Name+'" ���-�� ��������� '+inttostr(nodeRoot.AttributeCount)+', ���-�� ����� '+inttostr(nodeRoot.NodeCount));
          for I := 0 to nodeRoot.NodeCount - 1 do
          begin
            nodeURL:=nodeRoot.Nodes[I];
//              prWMemo(Memo1,'        �������� ���� "'+nodeURL.Name+'" ���-�� ��������� '+inttostr(nodeURL.AttributeCount)+', ���-�� ����� '+inttostr(nodeURL.NodeCount)+',�������� '+nodeURL.Value);
            RetId:=nodeURL.ReadAttributeString('replyId','');
            RetVal:=nodeURL.Value;
//              prWMemo(Memo1,RetId+'  '+RetVal);
            if RetId>'' then
            begin
              tQuery.SQL.Clear;
              tQuery.SQL.Add('');
              tQuery.SQL.Add('DECLARE @IdList varchar(100) = '''+RetId+'''');
              tQuery.SQL.Add('DECLARE @IdVal varchar(500) = '''+RetVal+'''');
              tQuery.SQL.Add('DECLARE @Rarid varchar(50) = '''+FSRAR_ID+'''');
              tQuery.SQL.Add('EXECUTE [dbo].[prAddPosQuList] @IdList,@IdVal,@Rarid');
              tQuery.ExecSQL;
            end;
          end;
        end;
      end else
      begin
        prWMemo(Memo1,'    FALSE '+'ResultCode='+IntToStr(httpsend.ResultCode));
        prWMemo(Memo1,httpsend.ResultString);
      end;

    finally
      tQuery.Free;
      httpsend.Free;
      RetXml.Free;
    end;
  end;
end;


function prSendKAP(RARID,KAP:String;Memo1:tcxMemo):Boolean;
var  AskXml: TNativeXml;
     IdF:Integer;
     SendXml,RetXml: TNativeXml;
     RetId:string;

     httpsend: THTTPSend;
     s: AnsiString;
     FS: TMemoryStream;
     bNoQuant:Boolean;
     nodePos: TXmlNode;
     vCli,vCli1:TCli;
     iNum:Integer;
     RetVal:TStringList;
begin
  Result:=True;
  with dmR do
  begin
    try
      IdF:=fGetId(1);

      httpsend := THTTPSend.Create;

      //�������� XML
      prWMemo(Memo1,'       - ��������� XML');

      SendXml := TNativeXml.Create(nil);
      RetXml  := TNativeXml.Create(nil);

      SendXml.XmlFormat := xfReadable;
      SendXml.CreateName('ns:Documents');
      SendXml.Root.WriteAttributeString('Version','1.0');
      SendXml.Root.WriteAttributeString('xmlns:xsi','http://www.w3.org/2001/XMLSchema-instance');
      SendXml.Root.WriteAttributeString('xmlns:ns','http://fsrar.ru/WEGAIS/WB_DOC_SINGLE_01');
      SendXml.Root.WriteAttributeString('xmlns:qp','http://fsrar.ru/WEGAIS/QueryParameters');

      SendXml.Root.NodeNew('ns:Owner');
      SendXml.Root.NodeByName('ns:Owner').NodeNew('ns:FSRAR_ID').Value:=RARID;

      SendXml.Root.NodeNew('ns:Document');
      SendXml.Root.NodeByName('ns:Document').NodeNew('ns:QueryAP_v2');
      SendXml.Root.NodeByName('ns:Document').NodeByName('ns:QueryAP_v2').NodeNew('qp:Parameters');
      SendXml.Root.NodeByName('ns:Document').NodeByName('ns:QueryAP_v2').NodeByName('qp:Parameters').NodeNew('qp:Parameter');
      SendXml.Root.NodeByName('ns:Document').NodeByName('ns:QueryAP_v2').NodeByName('qp:Parameters').NodeByName('qp:Parameter').NodeNew('qp:Name').Value:='���';
      SendXml.Root.NodeByName('ns:Document').NodeByName('ns:QueryAP_v2').NodeByName('qp:Parameters').NodeByName('qp:Parameter').NodeNew('qp:Value').Value:=KAP;

      //��������� � ����
      FS := TMemoryStream.Create;
      SendXml.SaveToStream(FS);

      prWMemo(Memo1,'       - ��������� XML � ����.');

      quToRar.Active:=False;
      quToRar.ParamByName('ID').AsInteger:=IdF;
      quToRar.ParamByName('RARID').AsString:=RARID;
      quToRar.Active:=True;

      quToRar.Append;
      quToRarFSRARID.AsString:=RARID;
      quToRarID.AsInteger:=IdF;
      quToRarDATEQU.AsDateTime:=Now;
      quToRarITYPEQU.AsInteger:=2;
      quToRarISTATUS.AsInteger:=1;
//        quToRarSENDFILE.LoadFromFile(NameF);
      FS.Position := 0;
      quToRarSENDFILE.LoadFromStream(FS);
      quToRar.Post;

      prWMemo(Memo1,'  ��������� ������ ...');

      httpsend.MimeType := 'multipart/form-data; boundary='+Boundary;  //���������� Contetn-Type �������
        // ���������� Mime-��� � ������ �� �����
      s:='--'+Boundary+CRLF+'Content-Disposition: form-data; name="xml_file"; filename="'+IdToName(IdF)+'"'+CRLF+'Content-Type: application/xml'+CRLF+CRLF;
      httpsend.Document.Write(PAnsiChar(s)^, Length(s));
      FS.Position := 0;
      httpsend.Document.CopyFrom(FS, FS.Size);  //���������� ����� � ���� ���������
//        S:=CRLF+CRLF+'--'+Boundary+CRLF;  //��������� ���� �������
      S:=CRLF+CRLF+'--'+Boundary+'--'+CRLF;  //��������� ���� �������
      httpsend.Document.Write(PAnsiChar(s)^, Length(s)); // ��������� ���� ���������

      httpsend.KeepAlive:=False;
        //httpsend.Protocol:='1.1';  //��������
      httpsend.Status100:=True;
      httpsend.Headers.Add('Accept: */*');

      // ���������� ������
      if httpsend.HTTPMethod('POST','http://'+CommonSet.IPUTM+':8080/opt/in/QueryAP_v2') then   //http://localhost:8080/opt/in/WayBillAct
      begin
        prWMemo(Memo1,'       �������� ��� �� ( ResultCode='+IntToStr(httpsend.ResultCode)+' )');
        prWMemo(Memo1,httpsend.ResultString);

        if httpsend.ResultCode<>200 then
        begin
          Result:=False;
          try
            RetVal:=TStringList.Create;
            RetVal.LoadFromStream(httpsend.Document, TEncoding.UTF8);
            prWMemo(Memo1,RetVal.Text);
          finally
            RetVal.Free;
          end;
        end;

        httpsend.Document.Position:=0;
        RetXml.LoadFromStream(httpsend.Document);
        RetXml.XmlFormat := xfReadable;

        if Assigned(RetXml.Root.NodeByName('url')) then
        begin
          RetId:=RetXml.Root.NodeByName('url').Value;

          if RetId>'' then
          begin
            httpsend.Document.Position:=0;

            quToRar.Edit;
            quToRarISTATUS.AsInteger:=2;
            quToRarRECEIVE_ID.AsString:=RetId;
            quToRarRECEIVE_FILE.LoadFromStream(httpsend.Document);
            quToRar.Post;
          end;
        end;
      end else begin
        prWMemo(Memo1,'       ������ ���������� ������� ��� - POST,http://'+CommonSet.IPUTM+':8080/opt/in/QueryAP_v2');
        prWMemo(Memo1,'       ResultCode='+IntToStr(httpsend.ResultCode));
        prWMemo(Memo1,httpsend.ResultString);

        try
          RetVal:=TStringList.Create;
          RetVal.LoadFromStream(httpsend.Document, TEncoding.UTF8);
          prWMemo(Memo1,RetVal.Text);
        finally
          RetVal.Free;
        end;

        quToRar.Edit;
        quToRarISTATUS.AsInteger:=100;
        quToRar.Post;

        Result:=False;
      end;
    finally
      httpsend.Free;
      FS.Free;
      SendXml.Free;
      RetXml.Free;
    end;
  end;
end;

function strtest(Str1:string):string;
var Str2:string;
begin
  Str2:=Str1;
  while Pos('''',Str2)>0 do Str2[Pos('''',Str2)]:='"';
  Result:=Str2;
end;

function RoundEx( X: Double ): Integer;
var  ScaledFractPart:Integer;
     Temp : Double;
begin
 ScaledFractPart := Trunc(X);
 Temp := Trunc(Frac(X)*1000000)+1;
 if Temp >=  500000 then ScaledFractPart := ScaledFractPart + 1;
 if Temp <= -500000 then ScaledFractPart := ScaledFractPart - 1;
{ if Temp >=  0.5 then ScaledFractPart := ScaledFractPart + 1;
 if Temp <= -0.5 then ScaledFractPart := ScaledFractPart - 1;}
 RoundEx := ScaledFractPart;
end;


function R1000( X: Double ): Double;
begin
  Result:=RoundEx(X*1000)/1000;
end;


function dsrar(d:TDateTime):String;
begin
  result:=FormatDateTime('yyyy-mm-dd',d);
end;

function dtsrar(d:TDateTime):String;
begin
  result:=FormatDateTime('yyyy-mm-dd_hh:nn:ss',d);
  result:=AnsiReplaceStr(result,'_','T');
end;

function dsfromrar(s:String):TDateTime;
var StrWk: string;
begin
  StrWk:=AnsiReplaceStr(s,'-','');
  StrWk:=Copy(StrWk,7,2)+'.'+Copy(StrWk,5,2)+'.'+Copy(StrWk,1,4);
  Result:=StrToDateTimeDef(StrWk,date);
end;

function dtsfromrar(s:String):TDateTime;
var StrWk: string;
    dt,tm: string;
begin
  StrWk:=AnsiReplaceStr(s,'T',' ');
  dt:=Copy(StrWk,1,pos(' ',StrWk)-1);
  tm:=Copy(StrWk,pos(' ',StrWk)+1,length(StrWk));
  dt:=Copy(dt,9,2)+'.'+Copy(dt,6,2)+'.'+Copy(dt,1,4);
  tm:=Copy(tm,1,8);
  Result:=StrToDateTimeDef(dt+' '+tm,0);
end;

function dssql(d:TDateTime):String;
begin
  result:=FormatDateTime('yyyymmdd',d);
end;

function dssqltime(d:TDateTime):String;
begin
  result:=FormatDateTime('yyyymmdd hh:nn:ss',d);
end;


function ds1(d:TDateTime):String;
begin
  result:=FormatDateTime('dd.mm.yyyy',d);
end;


function fGetIdPrivate(iT:Integer;FDConn: TFDConnection):Integer;
var quGetId:TFDQuery;
begin
  Result:=0;
  try
    quGetId := TFDQuery.Create(nil);
    quGetId.Connection := FDConn;
    quGetId.ResourceOptions.SilentMode:=True;     //�� ���������� Wait cursor

    quGetId.Active:=False;
    quGetId.SQL.Clear;
    quGetId.SQL.Add('EXECUTE [dbo].[fGetId]  '+its(iT));
    quGetId.Active:=True;
    Result:=quGetId.FieldByName('retnum').AsInteger;
    quGetId.Active:=False;
  finally
    quGetId.Free;
  end;
end;


function fGetId(iT:Integer):Integer;
begin
  Result:=0;
  try
    with dmR do
    begin
      quGetId.Active:=False;
      quGetId.ParamByName('ITN').Value:=iT;
      quGetId.Active:=True;
      Result:=quGetIdretnum.AsInteger;
      quGetId.Active:=False;
    end;
  except
    Result:=0;
  end;
end;


function prCreateRet(RARID,SID:String;IDATE:Integer;Memo1:tcxMemo):Boolean;
begin
  with dmR do
  begin
    Result:=True;
    try
      quS.Active:=False;
      quS.SQL.Clear;
      quS.SQL.Add('');
      quS.SQL.Add('DECLARE @FSRARID varchar(50) = '''+Trim(RARID)+'''');
      quS.SQL.Add('DECLARE @IDATE int = '+IntToStr(iDate));
      quS.SQL.Add('DECLARE @SID varchar(50) = '''+Trim(SID)+'''');
      quS.SQL.Add('EXECUTE [dbo].[prCreateRetDoc] @FSRARID,@SID,@IDATE');
      quS.ExecSQL;
    except
      Result:=False;
    end;
  end;
end;

function prSendDocCorr(RARID,SID:String;IDATE:Integer;Memo1:tcxMemo):Boolean;
var  AskXml: TNativeXml;
     IdF:Integer;
     SendXml,RetXml: TNativeXml;
     RetId:string;

     httpsend: THTTPSend;
     s: AnsiString;
     FS: TMemoryStream;
     bNoQuant:Boolean;
     nodePos: TXmlNode;
     vCli,vCli1:TCli;
     iNum:Integer;
     RetVal:TStringList;
     SendPath:string;
     CliType:String;
     rVol:Real;
begin
  Result:=True;
  with dmR do
  begin
    try
      IdF:=fGetId(1);
      SendPath:='';

      httpsend := THTTPSend.Create;

      quDocsCorrRec.Active:=False;
      quDocsCorrRec.ParamByName('RARID').AsString:=RARID;
      quDocsCorrRec.ParamByName('IDATE').AsInteger:=IDATE;
      quDocsCorrRec.ParamByName('SID').AsString:=SID;
      quDocsCorrRec.Active:=True;
      if quDocsCorrRec.RecordCount>0 then
      begin
        //�������� XML
        prWMemo(Memo1,'       - ��������� XML');

        SendXml := TNativeXml.Create(nil);
        RetXml  := TNativeXml.Create(nil);

        SendXml.XmlFormat := xfReadable;
        SendXml.CreateName('ns:Documents');
        SendXml.Root.WriteAttributeString('Version','1.0');
        SendXml.Root.WriteAttributeString('xmlns:xsi','http://www.w3.org/2001/XMLSchema-instance');
        SendXml.Root.WriteAttributeString('xmlns:ns','http://fsrar.ru/WEGAIS/WB_DOC_SINGLE_01');
        SendXml.Root.WriteAttributeString('xmlns:pref','http://fsrar.ru/WEGAIS/ProductRef_v2');
        SendXml.Root.WriteAttributeString('xmlns:oref','http://fsrar.ru/WEGAIS/ClientRef_v2');
        SendXml.Root.WriteAttributeString('xmlns:ce','http://fsrar.ru/WEGAIS/CommonEnum');

        if Pos('---',quDocsCorrRecSTYPE.AsString)>0 then //��������
        begin
          SendXml.Root.WriteAttributeString('xmlns:awr','http://fsrar.ru/WEGAIS/ActWriteOffShop_v2');

          SendXml.Root.NodeNew('ns:Owner');
          SendXml.Root.NodeByName('ns:Owner').NodeNew('ns:FSRAR_ID').Value:=RARID;

          SendXml.Root.NodeNew('ns:Document');
          SendXml.Root.NodeByName('ns:Document').NodeNew('ns:ActWriteOffShop_v2');
          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:ActWriteOffShop_v2').NodeNew('awr:Identity').Value:=SID;

          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:ActWriteOffShop_v2').NodeNew('awr:Header');
          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:ActWriteOffShop_v2').NodeByName('awr:Header').NodeNew('awr:ActNumber').Value:=quDocsCorrRecNUMBER.AsString;
          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:ActWriteOffShop_v2').NodeByName('awr:Header').NodeNew('awr:ActDate').Value:=FormatDateTime('yyyy-mm-dd',date);
          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:ActWriteOffShop_v2').NodeByName('awr:Header').NodeNew('awr:TypeWriteOff').Value:='����������';
          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:ActWriteOffShop_v2').NodeByName('awr:Header').NodeNew('awr:Note').Value:='�������������� '+quDocsCorrRecNUMBER.AsString;

          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:ActWriteOffShop_v2').NodeNew('awr:Content');

          quDocsCorrRecSP.Active:=False;
          quDocsCorrRecSP.ParamByName('RARID').AsString:=RARID;
          quDocsCorrRecSP.ParamByName('IDATE').AsInteger:=IDATE;
          quDocsCorrRecSP.ParamByName('SID').AsString:=SID;
          quDocsCorrRecSP.Active:=True;
          quDocsCorrRecSP.First;
          while not quDocsCorrRecSP.eof do
          begin
            nodePos:=SendXml.Root.NodeByName('ns:Document').NodeByName('ns:ActWriteOffShop_v2').NodeByName('awr:Content').NodeNew('awr:Position');
            nodePos.NodeNew('awr:Identity').Value:=quDocsCorrRecSPNUM.AsString;
            nodePos.NodeNew('awr:Product');

            if quDocsCorrRecSPVOL.AsFloat>0.001 then nodePos.NodeByName('awr:Product').NodeNew('pref:UnitType').Value:='Packed'
            else nodePos.NodeByName('awr:Product').NodeNew('pref:UnitType').Value:='Unpacked';

            rVol:=quDocsCorrRecSPVOL.AsFloat;
            if rVol=0 then rVol:=1;

            nodePos.NodeByName('awr:Product').NodeNew('pref:Type').Value:='��';
            nodePos.NodeByName('awr:Product').NodeNew('pref:FullName').Value:=quDocsCorrRecSPNAME.AsString;
            nodePos.NodeByName('awr:Product').NodeNew('pref:AlcCode').Value:=quDocsCorrRecSPALCCODE.AsString;
            nodePos.NodeByName('awr:Product').NodeNew('pref:Capacity').Value:=fts0(rVol);
            nodePos.NodeByName('awr:Product').NodeNew('pref:ShortName').Value:='';
            nodePos.NodeByName('awr:Product').NodeNew('pref:AlcVolume').Value:=fts0(quDocsCorrRecSPKREP.AsFloat);

            CliType:=quDocsCorrRecSPCLITYPE.AsString;

            nodePos.NodeByName('awr:Product').NodeNew('pref:Producer');
            nodePos.NodeByName('awr:Product').NodeByName('pref:Producer').NodeNew('oref:'+CliType);

            nodePos.NodeByName('awr:Product').NodeByName('pref:Producer').NodeByName('oref:'+CliType).NodeNew('oref:ClientRegId').Value:=quDocsCorrRecSPPRODID.AsString;
            nodePos.NodeByName('awr:Product').NodeByName('pref:Producer').NodeByName('oref:'+CliType).NodeNew('oref:FullName').Value:=quDocsCorrRecSPFULLNAMECLI.AsString;
            nodePos.NodeByName('awr:Product').NodeByName('pref:Producer').NodeByName('oref:'+CliType).NodeNew('oref:ShortName').Value:=quDocsCorrRecSPNAMECLI.AsString;

            if CliType='UL' then
            begin
              nodePos.NodeByName('awr:Product').NodeByName('pref:Producer').NodeByName('oref:'+CliType).NodeNew('oref:INN').Value:=quDocsCorrRecSPPRODINN.AsString;
              nodePos.NodeByName('awr:Product').NodeByName('pref:Producer').NodeByName('oref:'+CliType).NodeNew('oref:KPP').Value:=quDocsCorrRecSPPRODKPP.AsString;
              nodePos.NodeByName('awr:Product').NodeByName('pref:Producer').NodeByName('oref:'+CliType).NodeNew('oref:address');

              nodePos.NodeByName('awr:Product').NodeByName('pref:Producer').NodeByName('oref:'+CliType).NodeByName('oref:address').NodeNew('oref:Country').Value:=quDocsCorrRecSPCOUNTRY.AsString;
              nodePos.NodeByName('awr:Product').NodeByName('pref:Producer').NodeByName('oref:'+CliType).NodeByName('oref:address').NodeNew('oref:RegionCode').Value:=quDocsCorrRecSPREGCODE.AsString;
              nodePos.NodeByName('awr:Product').NodeByName('pref:Producer').NodeByName('oref:'+CliType).NodeByName('oref:address').NodeNew('oref:description').Value:=quDocsCorrRecSPADDR.AsString;
            end;

            if CliType='FL' then
            begin
              nodePos.NodeByName('awr:Product').NodeByName('pref:Producer').NodeByName('oref:'+CliType).NodeNew('oref:INN').Value:=quDocsCorrRecSPPRODINN.AsString;
              nodePos.NodeByName('awr:Product').NodeByName('pref:Producer').NodeByName('oref:'+CliType).NodeNew('oref:address');

              nodePos.NodeByName('awr:Product').NodeByName('pref:Producer').NodeByName('oref:'+CliType).NodeByName('oref:address').NodeNew('oref:Country').Value:=quDocsCorrRecSPCOUNTRY.AsString;
              nodePos.NodeByName('awr:Product').NodeByName('pref:Producer').NodeByName('oref:'+CliType).NodeByName('oref:address').NodeNew('oref:RegionCode').Value:=quDocsCorrRecSPREGCODE.AsString;
              nodePos.NodeByName('awr:Product').NodeByName('pref:Producer').NodeByName('oref:'+CliType).NodeByName('oref:address').NodeNew('oref:description').Value:=quDocsCorrRecSPADDR.AsString;
            end;

            if CliType='FO' then
            begin
              nodePos.NodeByName('awr:Product').NodeByName('pref:Producer').NodeByName('oref:'+CliType).NodeNew('oref:address');

              nodePos.NodeByName('awr:Product').NodeByName('pref:Producer').NodeByName('oref:'+CliType).NodeByName('oref:address').NodeNew('oref:Country').Value:=quDocsCorrRecSPCOUNTRY.AsString;
              nodePos.NodeByName('awr:Product').NodeByName('pref:Producer').NodeByName('oref:'+CliType).NodeByName('oref:address').NodeNew('oref:description').Value:=quDocsCorrRecSPADDR.AsString;
            end;

            if CliType='TS' then
            begin
              nodePos.NodeByName('awr:Product').NodeByName('pref:Producer').NodeByName('oref:'+CliType).NodeNew('oref:address');

              nodePos.NodeByName('awr:Product').NodeByName('pref:Producer').NodeByName('oref:'+CliType).NodeByName('oref:address').NodeNew('oref:Country').Value:=quDocsCorrRecSPCOUNTRY.AsString;
              nodePos.NodeByName('awr:Product').NodeByName('pref:Producer').NodeByName('oref:'+CliType).NodeByName('oref:address').NodeNew('oref:description').Value:=quDocsCorrRecSPADDR.AsString;
            end;

            nodePos.NodeByName('awr:Product').NodeNew('pref:ProductVCode').Value:='��';

            nodePos.NodeNew('awr:Quantity').Value:=quDocsCorrRecSPQUANT.AsString;

            quDocsCorrRecSP.Next;
          end;
          quDocsCorrRecSP.Active:=False;

          SendPath:=':8080/opt/in/ActWriteOffShop_v2';
        end;
        if Pos('+++',quDocsCorrRecSTYPE.AsString)>0 then //��� ���������� �� ����
        begin
          SendXml.Root.WriteAttributeString('xmlns:ainp','http://fsrar.ru/WEGAIS/ActChargeOnShop_v2');

          SendXml.Root.NodeNew('ns:Owner');
          SendXml.Root.NodeByName('ns:Owner').NodeNew('ns:FSRAR_ID').Value:=RARID;

          SendXml.Root.NodeNew('ns:Document');
          SendXml.Root.NodeByName('ns:Document').NodeNew('ns:ActChargeOnShop_v2');
          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:ActChargeOnShop_v2').NodeNew('ainp:Identity').Value:=SID;

          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:ActChargeOnShop_v2').NodeNew('ainp:Header');
          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:ActChargeOnShop_v2').NodeByName('ainp:Header').NodeNew('ainp:Number').Value:=quDocsCorrRecNUMBER.AsString;
          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:ActChargeOnShop_v2').NodeByName('ainp:Header').NodeNew('ainp:ActDate').Value:=FormatDateTime('yyyy-mm-dd',date);
          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:ActChargeOnShop_v2').NodeByName('ainp:Header').NodeNew('ainp:TypeChargeOn').Value:='���������, ���������� �� 01.01.2016';
          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:ActChargeOnShop_v2').NodeByName('ainp:Header').NodeNew('ainp:Note').Value:='�������������� '+quDocsCorrRecNUMBER.AsString;

          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:ActChargeOnShop_v2').NodeNew('ainp:Content');

          quDocsCorrRecSP.Active:=False;
          quDocsCorrRecSP.ParamByName('RARID').AsString:=RARID;
          quDocsCorrRecSP.ParamByName('IDATE').AsInteger:=IDATE;
          quDocsCorrRecSP.ParamByName('SID').AsString:=SID;
          quDocsCorrRecSP.Active:=True;
          quDocsCorrRecSP.First;
          while not quDocsCorrRecSP.eof do
          begin
            nodePos:=SendXml.Root.NodeByName('ns:Document').NodeByName('ns:ActChargeOnShop_v2').NodeByName('ainp:Content').NodeNew('ainp:Position');
            nodePos.NodeNew('ainp:Identity').Value:=quDocsCorrRecSPNUM.AsString;
            nodePos.NodeNew('ainp:Product');
            nodePos.NodeByName('ainp:Product').NodeNew('pref:UnitType').Value:='Packed';
            nodePos.NodeByName('ainp:Product').NodeNew('pref:Type').Value:='��';
            nodePos.NodeByName('ainp:Product').NodeNew('pref:FullName').Value:=quDocsCorrRecSPNAME.AsString;
            nodePos.NodeByName('ainp:Product').NodeNew('pref:AlcCode').Value:=quDocsCorrRecSPALCCODE.AsString;
            nodePos.NodeByName('ainp:Product').NodeNew('pref:Capacity').Value:=fts(quDocsCorrRecSPVOL.AsFloat);
            nodePos.NodeByName('ainp:Product').NodeNew('pref:ShortName').Value:='';
            nodePos.NodeByName('ainp:Product').NodeNew('pref:AlcVolume').Value:=fts(quDocsCorrRecSPKREP.AsFloat);

            CliType:=quDocsCorrRecSPCLITYPE.AsString;

            nodePos.NodeByName('ainp:Product').NodeNew('pref:Producer');
            nodePos.NodeByName('ainp:Product').NodeByName('pref:Producer').NodeNew('oref:'+CliType);

            nodePos.NodeByName('ainp:Product').NodeByName('pref:Producer').NodeByName('oref:'+CliType).NodeNew('oref:ClientRegId').Value:=quDocsCorrRecSPPRODID.AsString;
            nodePos.NodeByName('ainp:Product').NodeByName('pref:Producer').NodeByName('oref:'+CliType).NodeNew('oref:FullName').Value:=quDocsCorrRecSPFULLNAMECLI.AsString;
            nodePos.NodeByName('ainp:Product').NodeByName('pref:Producer').NodeByName('oref:'+CliType).NodeNew('oref:ShortName').Value:=quDocsCorrRecSPNAMECLI.AsString;

            if CliType='UL' then
            begin
              nodePos.NodeByName('ainp:Product').NodeByName('pref:Producer').NodeByName('oref:'+CliType).NodeNew('oref:INN').Value:=quDocsCorrRecSPPRODINN.AsString;
              nodePos.NodeByName('ainp:Product').NodeByName('pref:Producer').NodeByName('oref:'+CliType).NodeNew('oref:KPP').Value:=quDocsCorrRecSPPRODKPP.AsString;
              nodePos.NodeByName('ainp:Product').NodeByName('pref:Producer').NodeByName('oref:'+CliType).NodeNew('oref:address');

              nodePos.NodeByName('ainp:Product').NodeByName('pref:Producer').NodeByName('oref:'+CliType).NodeByName('oref:address').NodeNew('oref:Country').Value:=quDocsCorrRecSPCOUNTRY.AsString;
              nodePos.NodeByName('ainp:Product').NodeByName('pref:Producer').NodeByName('oref:'+CliType).NodeByName('oref:address').NodeNew('oref:RegionCode').Value:=quDocsCorrRecSPREGCODE.AsString;
              nodePos.NodeByName('ainp:Product').NodeByName('pref:Producer').NodeByName('oref:'+CliType).NodeByName('oref:address').NodeNew('oref:description').Value:=quDocsCorrRecSPADDR.AsString;
            end;

            if CliType='FL' then
            begin
              nodePos.NodeByName('ainp:Product').NodeByName('pref:Producer').NodeByName('oref:'+CliType).NodeNew('oref:INN').Value:=quDocsCorrRecSPPRODINN.AsString;
              nodePos.NodeByName('ainp:Product').NodeByName('pref:Producer').NodeByName('oref:'+CliType).NodeNew('oref:address');

              nodePos.NodeByName('ainp:Product').NodeByName('pref:Producer').NodeByName('oref:'+CliType).NodeByName('oref:address').NodeNew('oref:Country').Value:=quDocsCorrRecSPCOUNTRY.AsString;
              nodePos.NodeByName('ainp:Product').NodeByName('pref:Producer').NodeByName('oref:'+CliType).NodeByName('oref:address').NodeNew('oref:RegionCode').Value:=quDocsCorrRecSPREGCODE.AsString;
              nodePos.NodeByName('ainp:Product').NodeByName('pref:Producer').NodeByName('oref:'+CliType).NodeByName('oref:address').NodeNew('oref:description').Value:=quDocsCorrRecSPADDR.AsString;
            end;

            if CliType='FO' then
            begin
              nodePos.NodeByName('ainp:Product').NodeByName('pref:Producer').NodeByName('oref:'+CliType).NodeNew('oref:address');

              nodePos.NodeByName('ainp:Product').NodeByName('pref:Producer').NodeByName('oref:'+CliType).NodeByName('oref:address').NodeNew('oref:Country').Value:=quDocsCorrRecSPCOUNTRY.AsString;
              nodePos.NodeByName('ainp:Product').NodeByName('pref:Producer').NodeByName('oref:'+CliType).NodeByName('oref:address').NodeNew('oref:description').Value:=quDocsCorrRecSPADDR.AsString;
            end;

            if CliType='TS' then
            begin
              nodePos.NodeByName('ainp:Product').NodeByName('pref:Producer').NodeByName('oref:'+CliType).NodeNew('oref:address');

              nodePos.NodeByName('ainp:Product').NodeByName('pref:Producer').NodeByName('oref:'+CliType).NodeByName('oref:address').NodeNew('oref:Country').Value:=quDocsCorrRecSPCOUNTRY.AsString;
              nodePos.NodeByName('ainp:Product').NodeByName('pref:Producer').NodeByName('oref:'+CliType).NodeByName('oref:address').NodeNew('oref:description').Value:=quDocsCorrRecSPADDR.AsString;
            end;

            nodePos.NodeByName('ainp:Product').NodeNew('pref:ProductVCode').Value:='��';

            nodePos.NodeNew('ainp:Quantity').Value:=quDocsCorrRecSPQUANT.AsString;

            quDocsCorrRecSP.Next;
          end;
          quDocsCorrRecSP.Active:=False;
          SendPath:=':8080/opt/in/ActChargeOnShop_v2';
        end;

        //��������� � ����
        FS := TMemoryStream.Create;
        SendXml.SaveToStream(FS);

        prWMemo(Memo1,'       - ��������� XML � ����.');

        quDocsCorrRec.Edit;
        FS.Position := 0;
        quDocsCorrRecSENDXML.AsString:='';
        quDocsCorrRecSENDXML.LoadFromStream(FS);
        quDocsCorrRec.Post;


        quToRar.Active:=False;
        quToRar.ParamByName('ID').AsInteger:=IdF;
        quToRar.ParamByName('RARID').AsString:=RARID;
        quToRar.Active:=True;

        quToRar.Append;
        quToRarFSRARID.AsString:=RARID;
        quToRarID.AsInteger:=IdF;
        quToRarDATEQU.AsDateTime:=Now;
        quToRarITYPEQU.AsInteger:=2;
        quToRarISTATUS.AsInteger:=1;
//        quToRarSENDFILE.LoadFromFile(NameF);
        FS.Position := 0;
        quToRarSENDFILE.LoadFromStream(FS);
        quToRar.Post;


        prWMemo(Memo1,'  ��������� ������ ...');

        httpsend.MimeType := 'multipart/form-data; boundary='+Boundary;  //���������� Contetn-Type �������
          // ���������� Mime-��� � ������ �� �����
        s:='--'+Boundary+CRLF+'Content-Disposition: form-data; name="xml_file"; filename="'+IdToName(IdF)+'"'+CRLF+'Content-Type: application/xml'+CRLF+CRLF;
        httpsend.Document.Write(PAnsiChar(s)^, Length(s));
        FS.Position := 0;
        httpsend.Document.CopyFrom(FS, FS.Size);  //���������� ����� � ���� ���������
  //        S:=CRLF+CRLF+'--'+Boundary+CRLF;  //��������� ���� �������
        S:=CRLF+CRLF+'--'+Boundary+'--'+CRLF;  //��������� ���� �������
        httpsend.Document.Write(PAnsiChar(s)^, Length(s)); // ��������� ���� ���������

        httpsend.KeepAlive:=False;
          //httpsend.Protocol:='1.1';  //��������
        httpsend.Status100:=True;
        httpsend.Headers.Add('Accept: */*');

        // ���������� ������
        if httpsend.HTTPMethod('POST','http://'+CommonSet.IPUTM+SendPath) then   //http://localhost:8080/opt/in/WayBillAct
        begin
          prWMemo(Memo1,'       �������� '+quDocsCorrRecSTYPE.AsString+' �� ( ResultCode='+IntToStr(httpsend.ResultCode)+' )');
          prWMemo(Memo1,httpsend.ResultString);

          if httpsend.ResultCode<>200 then
          begin
            Result:=False;
            try
              RetVal:=TStringList.Create;
              RetVal.LoadFromStream(httpsend.Document, TEncoding.UTF8);
              prWMemo(Memo1,RetVal.Text);
            finally
              RetVal.Free;
            end;
          end;

          httpsend.Document.Position:=0;
          RetXml.LoadFromStream(httpsend.Document);
          RetXml.XmlFormat := xfReadable;

          if Assigned(RetXml.Root.NodeByName('url')) then
          begin
            RetId:=RetXml.Root.NodeByName('url').Value;

            if RetId>'' then
            begin
              httpsend.Document.Position:=0;

              quToRar.Edit;
              quToRarISTATUS.AsInteger:=2;
              quToRarRECEIVE_ID.AsString:=RetId;
              quToRarRECEIVE_FILE.LoadFromStream(httpsend.Document);
              quToRar.Post;
            end;
          end;
        end else begin
          prWMemo(Memo1,'       ������ ���������� ������� '+quDocsCorrRecSTYPE.AsString+' - POST,http://'+CommonSet.IPUTM+SendPath);
          prWMemo(Memo1,'       ResultCode='+IntToStr(httpsend.ResultCode));
          prWMemo(Memo1,httpsend.ResultString);

          try
            RetVal:=TStringList.Create;
            RetVal.LoadFromStream(httpsend.Document, TEncoding.UTF8);
            prWMemo(Memo1,RetVal.Text);
          finally
            RetVal.Free;
          end;

          quToRar.Edit;
          quToRarISTATUS.AsInteger:=100;
          quToRar.Post;

          Result:=False;
        end;
      end;
    finally
      quDocsCorrRec.Active:=False;
      httpsend.Free;
      FS.Free;
      SendXml.Free;
      RetXml.Free;
    end;
  end;
end;


function prSendDocVn(RARID,SID:String;IDATE:Integer;Memo1:tcxMemo):Boolean;
var  AskXml: TNativeXml;
     IdF:Integer;
     SendXml,RetXml: TNativeXml;
     RetId:string;

     httpsend: THTTPSend;
     s: AnsiString;
     FS: TMemoryStream;
     bNoQuant:Boolean;
     nodePos: TXmlNode;
     vCli,vCli1:TCli;
     iNum:Integer;
     RetVal:TStringList;
begin
  Result:=True;
  with dmR do
  begin
    try
      IdF:=fGetId(1);

      httpsend := THTTPSend.Create;

      quSelDocVn1.Active:=False;
      quSelDocVn1.ParamByName('RARID').AsString:=RARID;
      quSelDocVn1.ParamByName('IDATE').AsInteger:=IDATE;
      quSelDocVn1.ParamByName('SID').AsString:=SID;
      quSelDocVn1.Active:=True;
      if quSelDocVn1.RecordCount>0 then
      begin
        //�������� XML
        prWMemo(Memo1,'       - ��������� XML');

        SendXml := TNativeXml.Create(nil);
        RetXml  := TNativeXml.Create(nil);

        SendXml.XmlFormat := xfReadable;
        SendXml.CreateName('ns:Documents');
        SendXml.Root.WriteAttributeString('Version','1.0');
        SendXml.Root.WriteAttributeString('xmlns:ns','http://fsrar.ru/WEGAIS/WB_DOC_SINGLE_01');
        SendXml.Root.WriteAttributeString('xmlns:xs','http://www.w3.org/2001/XMLSchema');
        SendXml.Root.WriteAttributeString('xmlns:c','http://fsrar.ru/WEGAIS/Common');
        SendXml.Root.WriteAttributeString('xmlns:pref','http://fsrar.ru/WEGAIS/ProductRef_v2');
        SendXml.Root.WriteAttributeString('xmlns:tts','http://fsrar.ru/WEGAIS/TransferToShop');

        SendXml.Root.NodeNew('ns:Owner');
        SendXml.Root.NodeByName('ns:Owner').NodeNew('ns:FSRAR_ID').Value:=RARID;

        SendXml.Root.NodeNew('ns:Document');
        SendXml.Root.NodeByName('ns:Document').NodeNew('ns:TransferToShop');

        SendXml.Root.NodeByName('ns:Document').NodeByName('ns:TransferToShop').NodeNew('tts:Identity').Value:=quSelDocVn1SID.AsString;

        SendXml.Root.NodeByName('ns:Document').NodeByName('ns:TransferToShop').NodeNew('tts:Header');
        SendXml.Root.NodeByName('ns:Document').NodeByName('ns:TransferToShop').NodeByName('tts:Header').NodeNew('tts:TransferNumber').Value:=quSelDocVn1NUMBER.AsString;
        SendXml.Root.NodeByName('ns:Document').NodeByName('ns:TransferToShop').NodeByName('tts:Header').NodeNew('tts:TransferDate').Value:=FormatDateTime('yyyy-mm-dd',quSelDocVn1IDATE.AsInteger);

        SendXml.Root.NodeByName('ns:Document').NodeByName('ns:TransferToShop').NodeNew('tts:Content');

        quSelDocVnSp.Active:=False;
        quSelDocVnSp.ParamByName('RARID').AsString:=quSelDocVn1FSRARID.AsString;
        quSelDocVnSp.ParamByName('IDATE').AsInteger:=quSelDocVn1IDATE.AsInteger;
        quSelDocVnSp.ParamByName('SID').AsString:=quSelDocVn1SID.AsString;
        quSelDocVnSp.Active:=True;
        quSelDocVnSp.First;
        while not quSelDocVnSp.eof do
        begin
          nodePos:=SendXml.Root.NodeByName('ns:Document').NodeByName('ns:TransferToShop').NodeByName('tts:Content').NodeNew('tts:Position');
          nodePos.NodeNew('tts:Identity').Value:=quSelDocVnSpNUM.AsString;
          nodePos.NodeNew('tts:ProductCode').Value:=quSelDocVnSpALCCODE.AsString;
          nodePos.NodeNew('tts:Quantity').Value:=fts(R1000(quSelDocVnSpQUANT.AsFloat));
          nodePos.NodeNew('tts:InformF2');
          nodePos.NodeByName('tts:InformF2').NodeNew('pref:F2RegId').Value:=quSelDocVnSpINFO_B.AsString;
          quSelDocVnSp.Next;
        end;
        quSelDocVnSp.Active:=False;

        //��������� � ����
        FS := TMemoryStream.Create;
        SendXml.SaveToStream(FS);

        prWMemo(Memo1,'       - ��������� XML � ����.');

        quSelDocVn1.Edit;
        FS.Position := 0;
        quSelDocVn1SENDXML.AsString:='';
        quSelDocVn1SENDXML.LoadFromStream(FS);
        quSelDocVn1.Post;

        quToRar.Active:=False;
        quToRar.ParamByName('ID').AsInteger:=IdF;
        quToRar.ParamByName('RARID').AsString:=RARID;
        quToRar.Active:=True;

        quToRar.Append;
        quToRarFSRARID.AsString:=RARID;
        quToRarID.AsInteger:=IdF;
        quToRarDATEQU.AsDateTime:=Now;
        quToRarITYPEQU.AsInteger:=2;
        quToRarISTATUS.AsInteger:=1;
//        quToRarSENDFILE.LoadFromFile(NameF);
        FS.Position := 0;
        quToRarSENDFILE.LoadFromStream(FS);
        quToRar.Post;

        prWMemo(Memo1,'  ��������� ������ ...');

        httpsend.MimeType := 'multipart/form-data; boundary='+Boundary;  //���������� Contetn-Type �������
          // ���������� Mime-��� � ������ �� �����
        s:='--'+Boundary+CRLF+'Content-Disposition: form-data; name="xml_file"; filename="'+IdToName(IdF)+'"'+CRLF+'Content-Type: application/xml'+CRLF+CRLF;
        httpsend.Document.Write(PAnsiChar(s)^, Length(s));
        FS.Position := 0;
        httpsend.Document.CopyFrom(FS, FS.Size);  //���������� ����� � ���� ���������
  //        S:=CRLF+CRLF+'--'+Boundary+CRLF;  //��������� ���� �������
        S:=CRLF+CRLF+'--'+Boundary+'--'+CRLF;  //��������� ���� �������
        httpsend.Document.Write(PAnsiChar(s)^, Length(s)); // ��������� ���� ���������

        httpsend.KeepAlive:=False;
          //httpsend.Protocol:='1.1';  //��������
        httpsend.Status100:=True;
        httpsend.Headers.Add('Accept: */*');

        // ���������� ������
        if httpsend.HTTPMethod('POST','http://'+CommonSet.IPUTM+':8080/opt/in/TransferToShop') then   //http://localhost:8080/opt/in/WayBillAct
        begin
          prWMemo(Memo1,'       �������� TransferToShop �� ( ResultCode='+IntToStr(httpsend.ResultCode)+' )');
          prWMemo(Memo1,httpsend.ResultString);

          if httpsend.ResultCode<>200 then
          begin
            Result:=False;
            try
              RetVal:=TStringList.Create;
              RetVal.LoadFromStream(httpsend.Document, TEncoding.UTF8);
              prWMemo(Memo1,RetVal.Text);
            finally
              RetVal.Free;
            end;
          end;

          httpsend.Document.Position:=0;
          RetXml.LoadFromStream(httpsend.Document);
          RetXml.XmlFormat := xfReadable;

          if Assigned(RetXml.Root.NodeByName('url')) then
          begin
            RetId:=RetXml.Root.NodeByName('url').Value;

            if RetId>'' then
            begin
              httpsend.Document.Position:=0;

              quToRar.Edit;
              quToRarISTATUS.AsInteger:=2;
              quToRarRECEIVE_ID.AsString:=RetId;
              quToRarRECEIVE_FILE.LoadFromStream(httpsend.Document);
              quToRar.Post;
            end;
          end;
        end else begin
          prWMemo(Memo1,'       ������ ���������� ������� TransferToShop - POST,http://'+CommonSet.IPUTM+':8080/opt/in/TransferToShop');
          prWMemo(Memo1,'       ResultCode='+IntToStr(httpsend.ResultCode));
          prWMemo(Memo1,httpsend.ResultString);

          try
            RetVal:=TStringList.Create;
            RetVal.LoadFromStream(httpsend.Document, TEncoding.UTF8);
            prWMemo(Memo1,RetVal.Text);
          finally
            RetVal.Free;
          end;

          quToRar.Edit;
          quToRarISTATUS.AsInteger:=100;
          quToRar.Post;

          Result:=False;
        end;
      end;
    finally
      quSelDocVn1.Active:=False;
      httpsend.Free;
      FS.Free;
      SendXml.Free;
      RetXml.Free;
    end;
  end;
end;


function prSendDocVnPrivate(RARID,SID:String;IDATE:Integer;Memo1:tcxMemo; FDConn: TFDConnection; IPUTM:String):Boolean;
var  AskXml: TNativeXml;
     IdF:Integer;
     SendXml,RetXml: TNativeXml;
     RetId:string;

     httpsend: THTTPSend;
     s: AnsiString;
     FS: TMemoryStream;
     bNoQuant:Boolean;
     nodePos: TXmlNode;
     vCli,vCli1:TCli;
     iNum:Integer;
     RetVal:TStringList;

     quSelDocVn1,quSelDocVnSp,quToRar: TFDQuery;
begin
  Result:=True;
  try
    IdF:=fGetIdPrivate(1,FDConn);

    quSelDocVn1 := TFDQuery.Create(nil);
    quSelDocVn1.Connection := FDConn;
    quSelDocVn1.ResourceOptions.SilentMode:=True;     //�� ���������� Wait cursor

    quSelDocVnSp := TFDQuery.Create(nil);
    quSelDocVnSp.Connection := FDConn;
    quSelDocVnSp.ResourceOptions.SilentMode:=True;     //�� ���������� Wait cursor

    quToRar := TFDQuery.Create(nil);
    quToRar.Connection := FDConn;
    quToRar.ResourceOptions.SilentMode:=True;     //�� ���������� Wait cursor

    httpsend := THTTPSend.Create;

    quSelDocVn1.Active:=False;
    quSelDocVn1.SQL.Clear;
    quSelDocVn1.SQL.Add('');

    quSelDocVn1.SQL.Add('SELECT TOP 1 [FSRARID],[IDATE],[SID],[NUMBER],[SDATE],[STYPE],[IACTIVE],[READYSEND],[WBREGID],[FIXNUMBER],[FIXDATE],[TICK1],[TICK2],[SIDIN],[SENDXML] FROM dbo.ADOCSVN_HD');
    quSelDocVn1.SQL.Add('where [FSRARID]='''+RARID+'''');
    quSelDocVn1.SQL.Add('and [IDATE]='+its(IDATE));
    quSelDocVn1.SQL.Add('and [SID]='''+SID+'''');
    quSelDocVn1.Active:=True;
    if quSelDocVn1.RecordCount>0 then
    begin
      //�������� XML
      prWMemo(Memo1,'       - ��������� XML');

      SendXml := TNativeXml.Create(nil);
      RetXml  := TNativeXml.Create(nil);

      SendXml.XmlFormat := xfReadable;
      SendXml.CreateName('ns:Documents');
      SendXml.Root.WriteAttributeString('Version','1.0');
      SendXml.Root.WriteAttributeString('xmlns:ns','http://fsrar.ru/WEGAIS/WB_DOC_SINGLE_01');
      SendXml.Root.WriteAttributeString('xmlns:xs','http://www.w3.org/2001/XMLSchema');
      SendXml.Root.WriteAttributeString('xmlns:c','http://fsrar.ru/WEGAIS/Common');
      SendXml.Root.WriteAttributeString('xmlns:pref','http://fsrar.ru/WEGAIS/ProductRef_v2');
      SendXml.Root.WriteAttributeString('xmlns:tts','http://fsrar.ru/WEGAIS/TransferToShop');

      SendXml.Root.NodeNew('ns:Owner');
      SendXml.Root.NodeByName('ns:Owner').NodeNew('ns:FSRAR_ID').Value:=RARID;

      SendXml.Root.NodeNew('ns:Document');
      SendXml.Root.NodeByName('ns:Document').NodeNew('ns:TransferToShop');

      SendXml.Root.NodeByName('ns:Document').NodeByName('ns:TransferToShop').NodeNew('tts:Identity').Value:=quSelDocVn1.FieldByName('SID').AsString;

      SendXml.Root.NodeByName('ns:Document').NodeByName('ns:TransferToShop').NodeNew('tts:Header');
      SendXml.Root.NodeByName('ns:Document').NodeByName('ns:TransferToShop').NodeByName('tts:Header').NodeNew('tts:TransferNumber').Value:=quSelDocVn1.FieldByName('NUMBER').AsString;
      SendXml.Root.NodeByName('ns:Document').NodeByName('ns:TransferToShop').NodeByName('tts:Header').NodeNew('tts:TransferDate').Value:=FormatDateTime('yyyy-mm-dd',quSelDocVn1.FieldByName('IDATE').AsInteger);

      SendXml.Root.NodeByName('ns:Document').NodeByName('ns:TransferToShop').NodeNew('tts:Content');

      quSelDocVnSp.Active:=False;
      quSelDocVnSp.SQL.Clear;
      quSelDocVnSp.SQL.Add('');

      quSelDocVnSp.SQL.Add('SELECT [FSRARID],[IDATE],[SIDHD],[ID],[NUM],[ALCCODE],[QUANT],[INFO_B]');
      quSelDocVnSp.SQL.Add('FROM [dbo].[ADOCSVN_SP]');
      quSelDocVnSp.SQL.Add('where [FSRARID]='''+RARID+'''');
      quSelDocVnSp.SQL.Add('and [IDATE]='+its(IDATE));
      quSelDocVnSp.SQL.Add('and [SIDHD]='''+SID+'''');

      quSelDocVnSp.Active:=True;
      quSelDocVnSp.First;
      while not quSelDocVnSp.eof do
      begin
        nodePos:=SendXml.Root.NodeByName('ns:Document').NodeByName('ns:TransferToShop').NodeByName('tts:Content').NodeNew('tts:Position');
        nodePos.NodeNew('tts:Identity').Value:=quSelDocVnSp.FieldByName('NUM').AsString;
        nodePos.NodeNew('tts:ProductCode').Value:=quSelDocVnSp.FieldByName('ALCCODE').AsString;
        nodePos.NodeNew('tts:Quantity').Value:=fts(R1000(quSelDocVnSp.FieldByName('QUANT').AsFloat));
        nodePos.NodeNew('tts:InformF2');
        nodePos.NodeByName('tts:InformF2').NodeNew('pref:F2RegId').Value:=quSelDocVnSp.FieldByName('INFO_B').AsString;
        quSelDocVnSp.Next;
      end;
      quSelDocVnSp.Active:=False;

      //��������� � ����
      FS := TMemoryStream.Create;
      SendXml.SaveToStream(FS);

      prWMemo(Memo1,'       - ��������� XML � ����.');

      quSelDocVn1.Edit;
      FS.Position := 0;
      quSelDocVn1.FieldByName('SENDXML').AsString:='';
      TMemoField(quSelDocVn1.FieldByName('SENDXML')).LoadFromStream(FS);
      quSelDocVn1.Post;

      quToRar.Active:=False;
      quToRar.SQL.Clear;
      quToRar.SQL.Add('');
      quToRar.SQL.Add('SELECT [FSRARID],[ID],[DATEQU],[ITYPEQU],[ISTATUS],[SENDFILE],[RECEIVE_ID],[RECEIVE_FILE] FROM [dbo].[TORAR]');
      quToRar.SQL.Add('where [ID]='+its(IdF));
      quToRar.SQL.Add('and [FSRARID]='''+RARID+'''');
      quToRar.Active:=True;

      quToRar.Append;
      quToRar.FieldByName('FSRARID').AsString:=RARID;
      quToRar.FieldByName('ID').AsInteger:=IdF;
      quToRar.FieldByName('DATEQU').AsDateTime:=Now;
      quToRar.FieldByName('ITYPEQU').AsInteger:=2;
      quToRar.FieldByName('ISTATUS').AsInteger:=1;
//        quToRarSENDFILE.LoadFromFile(NameF);
      FS.Position := 0;
      quToRar.FieldByName('SENDFILE').AsString:='';
      TMemoField(quToRar.FieldByName('SENDFILE')).LoadFromStream(FS);
      quToRar.Post;

      prWMemo(Memo1,'  ��������� ������ ...');

      httpsend.MimeType := 'multipart/form-data; boundary='+Boundary;  //���������� Contetn-Type �������
        // ���������� Mime-��� � ������ �� �����
      s:='--'+Boundary+CRLF+'Content-Disposition: form-data; name="xml_file"; filename="'+IdToName(IdF)+'"'+CRLF+'Content-Type: application/xml'+CRLF+CRLF;
      httpsend.Document.Write(PAnsiChar(s)^, Length(s));
      FS.Position := 0;
      httpsend.Document.CopyFrom(FS, FS.Size);  //���������� ����� � ���� ���������
//        S:=CRLF+CRLF+'--'+Boundary+CRLF;  //��������� ���� �������
      S:=CRLF+CRLF+'--'+Boundary+'--'+CRLF;  //��������� ���� �������
      httpsend.Document.Write(PAnsiChar(s)^, Length(s)); // ��������� ���� ���������

      httpsend.KeepAlive:=False;
        //httpsend.Protocol:='1.1';  //��������
      httpsend.Status100:=True;
      httpsend.Headers.Add('Accept: */*');

      // ���������� ������
      if httpsend.HTTPMethod('POST','http://'+IPUTM+':8080/opt/in/TransferToShop') then   //http://localhost:8080/opt/in/WayBillAct
      begin
        prWMemo(Memo1,'       �������� TransferToShop �� ( ResultCode='+IntToStr(httpsend.ResultCode)+' )');
        prWMemo(Memo1,httpsend.ResultString);

        if httpsend.ResultCode<>200 then
        begin
          Result:=False;
          try
            RetVal:=TStringList.Create;
            RetVal.LoadFromStream(httpsend.Document, TEncoding.UTF8);
            prWMemo(Memo1,RetVal.Text);
          finally
            RetVal.Free;
          end;
        end;

        httpsend.Document.Position:=0;
        RetXml.LoadFromStream(httpsend.Document);
        RetXml.XmlFormat := xfReadable;

        if Assigned(RetXml.Root.NodeByName('url')) then
        begin
          RetId:=RetXml.Root.NodeByName('url').Value;

          if RetId>'' then
          begin
            httpsend.Document.Position:=0;

            quToRar.Edit;
            quToRar.FieldByName('ISTATUS').AsInteger:=2;
            quToRar.FieldByName('RECEIVE_ID').AsString:=RetId;
            TMemoField(quToRar.FieldByName('RECEIVE_FILE')).LoadFromStream(httpsend.Document);
            quToRar.Post;
          end;
        end;
      end else begin
        prWMemo(Memo1,'       ������ ���������� ������� TransferToShop - POST,http://'+IPUTM+':8080/opt/in/TransferToShop');
        prWMemo(Memo1,'       ResultCode='+IntToStr(httpsend.ResultCode));
        prWMemo(Memo1,httpsend.ResultString);

        try
          RetVal:=TStringList.Create;
          RetVal.LoadFromStream(httpsend.Document, TEncoding.UTF8);
          prWMemo(Memo1,RetVal.Text);
        finally
          RetVal.Free;
        end;

        quToRar.Edit;
        quToRar.FieldByName('ISTATUS').AsInteger:=100;
        quToRar.Post;

        Result:=False;
      end;
    end;
  finally
    quSelDocVnSp.Active:=False;
    quSelDocVnSp.Free;

    quSelDocVn1.Active:=False;
    quSelDocVn1.Free;

    quToRar.Active:=False;
    quToRar.Free;

    httpsend.Free;
    FS.Free;
    SendXml.Free;
    RetXml.Free;
  end;
end;

function prSendDocVn1Private(RARID,SID:String;IDATE:Integer;Memo1:tcxMemo; FDConn: TFDConnection; IPUTM:String):Boolean;
var  AskXml: TNativeXml;
     IdF:Integer;
     SendXml,RetXml: TNativeXml;
     RetId:string;

     httpsend: THTTPSend;
     s: AnsiString;
     FS: TMemoryStream;
     bNoQuant:Boolean;
     nodePos: TXmlNode;
     vCli,vCli1:TCli;
     iNum:Integer;
     RetVal:TStringList;

     quSelDocVn1,quSelDocVnSp,quToRar: TFDQuery;
begin
  Result:=True;
  try
    IdF:=fGetIdPrivate(1,FDConn);

    quSelDocVn1 := TFDQuery.Create(nil);
    quSelDocVn1.Connection := FDConn;
    quSelDocVn1.ResourceOptions.SilentMode:=True;     //�� ���������� Wait cursor

    quSelDocVnSp := TFDQuery.Create(nil);
    quSelDocVnSp.Connection := FDConn;
    quSelDocVnSp.ResourceOptions.SilentMode:=True;     //�� ���������� Wait cursor

    quToRar := TFDQuery.Create(nil);
    quToRar.Connection := FDConn;
    quToRar.ResourceOptions.SilentMode:=True;     //�� ���������� Wait cursor

    httpsend := THTTPSend.Create;

    quSelDocVn1.Active:=False;
    quSelDocVn1.SQL.Clear;
    quSelDocVn1.SQL.Add('');

    quSelDocVn1.SQL.Add('SELECT TOP 1 [FSRARID],[IDATE],[SID],[NUMBER],[SDATE],[STYPE],[IACTIVE],[READYSEND],[WBREGID],[FIXNUMBER],[FIXDATE],[TICK1],[TICK2],[SIDIN],[SENDXML] FROM dbo.ADOCSVN_HD');
    quSelDocVn1.SQL.Add('where [FSRARID]='''+RARID+'''');
    quSelDocVn1.SQL.Add('and [IDATE]='+its(IDATE));
    quSelDocVn1.SQL.Add('and [SID]='''+SID+'''');
    quSelDocVn1.Active:=True;
    if quSelDocVn1.RecordCount>0 then
    begin
      //�������� XML
      prWMemo(Memo1,'       - ��������� XML');

      SendXml := TNativeXml.Create(nil);
      RetXml  := TNativeXml.Create(nil);

      SendXml.XmlFormat := xfReadable;
      SendXml.CreateName('ns:Documents');
      SendXml.Root.WriteAttributeString('Version','1.0');
      SendXml.Root.WriteAttributeString('xmlns:ns','http://fsrar.ru/WEGAIS/WB_DOC_SINGLE_01');
      SendXml.Root.WriteAttributeString('xmlns:xs','http://www.w3.org/2001/XMLSchema');
      SendXml.Root.WriteAttributeString('xmlns:c','http://fsrar.ru/WEGAIS/Common');
      SendXml.Root.WriteAttributeString('xmlns:pref','http://fsrar.ru/WEGAIS/ProductRef_v2');
      SendXml.Root.WriteAttributeString('xmlns:tfs','http://fsrar.ru/WEGAIS/TransferFromShop');

      SendXml.Root.NodeNew('ns:Owner');
      SendXml.Root.NodeByName('ns:Owner').NodeNew('ns:FSRAR_ID').Value:=RARID;

      SendXml.Root.NodeNew('ns:Document');
      SendXml.Root.NodeByName('ns:Document').NodeNew('ns:TransferFromShop');

      SendXml.Root.NodeByName('ns:Document').NodeByName('ns:TransferFromShop').NodeNew('tfs:Identity').Value:=quSelDocVn1.FieldByName('SID').AsString;

      SendXml.Root.NodeByName('ns:Document').NodeByName('ns:TransferFromShop').NodeNew('tfs:Header');
      SendXml.Root.NodeByName('ns:Document').NodeByName('ns:TransferFromShop').NodeByName('tfs:Header').NodeNew('tfs:TransferNumber').Value:=quSelDocVn1.FieldByName('NUMBER').AsString;
      SendXml.Root.NodeByName('ns:Document').NodeByName('ns:TransferFromShop').NodeByName('tfs:Header').NodeNew('tfs:TransferDate').Value:=FormatDateTime('yyyy-mm-dd',quSelDocVn1.FieldByName('IDATE').AsInteger);

      SendXml.Root.NodeByName('ns:Document').NodeByName('ns:TransferFromShop').NodeNew('tfs:Content');

      quSelDocVnSp.Active:=False;
      quSelDocVnSp.SQL.Clear;
      quSelDocVnSp.SQL.Add('');

      quSelDocVnSp.SQL.Add('SELECT [FSRARID],[IDATE],[SIDHD],[ID],[NUM],[ALCCODE],[QUANT],[INFO_B]');
      quSelDocVnSp.SQL.Add('FROM [dbo].[ADOCSVN_SP]');
      quSelDocVnSp.SQL.Add('where [FSRARID]='''+RARID+'''');
      quSelDocVnSp.SQL.Add('and [IDATE]='+its(IDATE));
      quSelDocVnSp.SQL.Add('and [SIDHD]='''+SID+'''');

      quSelDocVnSp.Active:=True;
      quSelDocVnSp.First;
      while not quSelDocVnSp.eof do
      begin
        nodePos:=SendXml.Root.NodeByName('ns:Document').NodeByName('ns:TransferFromShop').NodeByName('tfs:Content').NodeNew('tfs:Position');
        nodePos.NodeNew('tfs:Identity').Value:=quSelDocVnSp.FieldByName('NUM').AsString;
        nodePos.NodeNew('tfs:ProductCode').Value:=quSelDocVnSp.FieldByName('ALCCODE').AsString;
        nodePos.NodeNew('tfs:Quantity').Value:=fts(R1000(quSelDocVnSp.FieldByName('QUANT').AsFloat));
        nodePos.NodeNew('tfs:InformF2');
        nodePos.NodeByName('tfs:InformF2').NodeNew('pref:F2RegId').Value:=quSelDocVnSp.FieldByName('INFO_B').AsString;
        quSelDocVnSp.Next;
      end;
      quSelDocVnSp.Active:=False;

      //��������� � ����
      FS := TMemoryStream.Create;
      SendXml.SaveToStream(FS);

      prWMemo(Memo1,'       - ��������� XML � ����.');

      quSelDocVn1.Edit;
      FS.Position := 0;
      quSelDocVn1.FieldByName('SENDXML').AsString:='';
      TMemoField(quSelDocVn1.FieldByName('SENDXML')).LoadFromStream(FS);
      quSelDocVn1.Post;

      quToRar.Active:=False;
      quToRar.SQL.Clear;
      quToRar.SQL.Add('');
      quToRar.SQL.Add('SELECT [FSRARID],[ID],[DATEQU],[ITYPEQU],[ISTATUS],[SENDFILE],[RECEIVE_ID],[RECEIVE_FILE] FROM [dbo].[TORAR]');
      quToRar.SQL.Add('where [ID]='+its(IdF));
      quToRar.SQL.Add('and [FSRARID]='''+RARID+'''');
      quToRar.Active:=True;

      quToRar.Append;
      quToRar.FieldByName('FSRARID').AsString:=RARID;
      quToRar.FieldByName('ID').AsInteger:=IdF;
      quToRar.FieldByName('DATEQU').AsDateTime:=Now;
      quToRar.FieldByName('ITYPEQU').AsInteger:=2;
      quToRar.FieldByName('ISTATUS').AsInteger:=1;
//        quToRarSENDFILE.LoadFromFile(NameF);
      FS.Position := 0;
      quToRar.FieldByName('SENDFILE').AsString:='';
      TMemoField(quToRar.FieldByName('SENDFILE')).LoadFromStream(FS);
      quToRar.Post;

      prWMemo(Memo1,'  ��������� ������ ...');

      httpsend.MimeType := 'multipart/form-data; boundary='+Boundary;  //���������� Contetn-Type �������
        // ���������� Mime-��� � ������ �� �����
      s:='--'+Boundary+CRLF+'Content-Disposition: form-data; name="xml_file"; filename="'+IdToName(IdF)+'"'+CRLF+'Content-Type: application/xml'+CRLF+CRLF;
      httpsend.Document.Write(PAnsiChar(s)^, Length(s));
      FS.Position := 0;
      httpsend.Document.CopyFrom(FS, FS.Size);  //���������� ����� � ���� ���������
//        S:=CRLF+CRLF+'--'+Boundary+CRLF;  //��������� ���� �������
      S:=CRLF+CRLF+'--'+Boundary+'--'+CRLF;  //��������� ���� �������
      httpsend.Document.Write(PAnsiChar(s)^, Length(s)); // ��������� ���� ���������

      httpsend.KeepAlive:=False;
        //httpsend.Protocol:='1.1';  //��������
      httpsend.Status100:=True;
      httpsend.Headers.Add('Accept: */*');

      // ���������� ������
      if httpsend.HTTPMethod('POST','http://'+IPUTM+':8080/opt/in/TransferFromShop') then   //http://localhost:8080/opt/in/WayBillAct
      begin
        prWMemo(Memo1,'       �������� TransferToShop �� ( ResultCode='+IntToStr(httpsend.ResultCode)+' )');
        prWMemo(Memo1,httpsend.ResultString);

        if httpsend.ResultCode<>200 then
        begin
          Result:=False;
          try
            RetVal:=TStringList.Create;
            RetVal.LoadFromStream(httpsend.Document, TEncoding.UTF8);
            prWMemo(Memo1,RetVal.Text);
          finally
            RetVal.Free;
          end;
        end;

        httpsend.Document.Position:=0;
        RetXml.LoadFromStream(httpsend.Document);
        RetXml.XmlFormat := xfReadable;

        if Assigned(RetXml.Root.NodeByName('url')) then
        begin
          RetId:=RetXml.Root.NodeByName('url').Value;

          if RetId>'' then
          begin
            httpsend.Document.Position:=0;

            quToRar.Edit;
            quToRar.FieldByName('ISTATUS').AsInteger:=2;
            quToRar.FieldByName('RECEIVE_ID').AsString:=RetId;
            TMemoField(quToRar.FieldByName('RECEIVE_FILE')).LoadFromStream(httpsend.Document);
            quToRar.Post;
          end;
        end;
      end else begin
        prWMemo(Memo1,'       ������ ���������� ������� TransferFromShop - POST,http://'+IPUTM+':8080/opt/in/TransferFromShop');
        prWMemo(Memo1,'       ResultCode='+IntToStr(httpsend.ResultCode));
        prWMemo(Memo1,httpsend.ResultString);

        try
          RetVal:=TStringList.Create;
          RetVal.LoadFromStream(httpsend.Document, TEncoding.UTF8);
          prWMemo(Memo1,RetVal.Text);
        finally
          RetVal.Free;
        end;

        quToRar.Edit;
        quToRar.FieldByName('ISTATUS').AsInteger:=100;
        quToRar.Post;

        Result:=False;
      end;
    end;
  finally
    quSelDocVnSp.Active:=False;
    quSelDocVnSp.Free;

    quSelDocVn1.Active:=False;
    quSelDocVn1.Free;

    quToRar.Active:=False;
    quToRar.Free;

    httpsend.Free;
    FS.Free;
    SendXml.Free;
    RetXml.Free;
  end;
end;


function prSendActPrivate(RARID,SID:String;IDATE,iPrih:Integer;Memo1:tcxMemo; FDConn: TFDConnection; IPUTM:String):Boolean;
Const
  CR = #$0d;
  LF = #$0a;
  CRLF = CR + LF;
  Boundary = 'END_OF_PART';

var  AskXml: TNativeXml;
     IdF:Integer;
     SendXml,RetXml: TNativeXml;
     RetId:string;

     httpsend: THTTPSend;
     s: AnsiString;
     FS: TMemoryStream;

     rQAll:Real;
     bDif:Boolean;

     nodePos: TXmlNode;
     sNum:string;
     quDocInRec, quSpecInSel, quToRar: TFDQuery;
     quS: TFDQuery;
begin
  Result:=True;
  if FDConn.Connected then
  begin
    try
      IdF:=fGetIdPrivate(1,FDConn);

      FS := TMemoryStream.Create;
      httpsend := THTTPSend.Create;

      quDocInRec := TFDQuery.Create(nil);
      quDocInRec.Connection := FDConn;
      quDocInRec.ResourceOptions.SilentMode:=True;     //�� ���������� Wait cursor

      quS := TFDQuery.Create(nil);
      quS.Connection := FDConn;
      quS.ResourceOptions.SilentMode:=True;     //�� ���������� Wait cursor

      quSpecInSel := TFDQuery.Create(nil);
      quSpecInSel.Connection := FDConn;
      quSpecInSel.ResourceOptions.SilentMode:=True;     //�� ���������� Wait cursor

      quToRar := TFDQuery.Create(nil);
      quToRar.Connection := FDConn;
      quToRar.ResourceOptions.SilentMode:=True;     //�� ���������� Wait cursor

      quDocInRec.Active:=False;
      quDocInRec.SQL.Clear;
      quDocInRec.SQL.Add('');
      quDocInRec.SQL.Add('SELECT [FSRARID],[IDATE],[SID],[NUMBER],[SDATE],[STYPE],[UNITTYPE],[SHIPDATE],[CLIFROM],[IACTIVE],[WBREGID],[FIXNUMBER],[FIXDATE],[IDHEAD],[COMPARE_STATUS]');
      quDocInRec.SQL.Add('      ,[COMPARE_QUANT_STATUS],[COMPARE_COMMENT],[DATE_INPUT],[DATE_OUTPUT],[SENDXML]');
      quDocInRec.SQL.Add('  FROM [dbo].[ADOCSIN_HD]');
      quDocInRec.SQL.Add('  where [FSRARID]='''+RARID+'''');
      quDocInRec.SQL.Add('  and [IDATE]='+its(IDATE));
      quDocInRec.SQL.Add('  and [SID]='''+SID+'''');
      quDocInRec.Active:=True;
      if (quDocInRec.RecordCount>0)and(quDocInRec.FieldByName('WBREGID').AsString>'') then
      begin
        rQAll:=0;
        bDif:=False;

        if iPrih=1 then  //������������ ����������
        begin
          bDif:=False;
          rQAll:=1;

          quS.Active:=False;
          quS.SQL.Clear;
          quS.SQL.Add('');
          quS.SQL.Add('UPDATE [dbo].[ADOCSIN_SP] SET [QUANTF] = [QUANT]');
          quS.SQL.Add('  where [FSRARID]='''+RARID+'''');
          quS.SQL.Add('  and [IDATE]='+its(IDATE));
          quS.SQL.Add('  and [SIDHD]='''+SID+'''');
          quS.ExecSQL;

        end else // ����� �� ������������
        begin
          quSpecInSel.Active:=False;
          quSpecInSel.SQL.Clear;
          quSpecInSel.SQL.Add('');
          quSpecInSel.SQL.Add('SELECT [FSRARID],[IDATE],[SIDHD],[ID],[NUM],[ALCCODE],[QUANT],[PRICE],[QUANTF],[PRICEF],[PRISEF0],[WBREGID]');
          quSpecInSel.SQL.Add('      ,isnull([SNUM],cast([NUM] as varchar)) as SNUM');
          quSpecInSel.SQL.Add('  FROM [dbo].[ADOCSIN_SP]');
          quSpecInSel.SQL.Add('  where [FSRARID]='''+RARID+'''');
          quSpecInSel.SQL.Add('  and [IDATE]='+its(IDATE));
          quSpecInSel.SQL.Add('  and [SIDHD]='''+SID+'''');
          quSpecInSel.Active:=True;
          quSpecInSel.First;
          while not quSpecInSel.Eof do
          begin
            rQAll:=rQAll+quSpecInSel.FieldByName('QUANTF').AsFloat;
            if abs(quSpecInSel.FieldByName('QUANT').AsFloat-quSpecInSel.FieldByName('QUANTF').AsFloat)>0.001 then bDif:=True;
            quSpecInSel.Next;
          end;
        end;

        //��������� XML

        SendXml := TNativeXml.Create(nil);
        RetXml  := TNativeXml.Create(nil);

        SendXml.XmlFormat := xfReadable;
        SendXml.CreateName('ns:Documents');
        SendXml.Root.WriteAttributeString('Version', '1.0');
        SendXml.Root.WriteAttributeString('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
        SendXml.Root.WriteAttributeString('xmlns:ns', 'http://fsrar.ru/WEGAIS/WB_DOC_SINGLE_01');
        SendXml.Root.WriteAttributeString('xmlns:oref', 'http://fsrar.ru/WEGAIS/ClientRef');
        SendXml.Root.WriteAttributeString('xmlns:pref', 'http://fsrar.ru/WEGAIS/ProductRef');
        SendXml.Root.WriteAttributeString('xmlns:wa', 'http://fsrar.ru/WEGAIS/ActTTNSingle');

        SendXml.Root.NodeNew('ns:Owner');
        SendXml.Root.NodeByName('ns:Owner').NodeNew('ns:FSRAR_ID').Value:=quDocInRec.FieldByName('FSRARID').AsString;

        SendXml.Root.NodeNew('ns:Document');
        SendXml.Root.NodeByName('ns:Document').NodeNew('ns:WayBillAct');
        SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBillAct').NodeNew('wa:Header');

        if rQAll>0 then SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBillAct').NodeByName('wa:Header').NodeNew('wa:IsAccept').Value:='Accepted'
        else SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBillAct').NodeByName('wa:Header').NodeNew('wa:IsAccept').Value:='Rejected';

        SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBillAct').NodeByName('wa:Header').NodeNew('wa:ACTNUMBER').Value:=quDocInRec.FieldByName('NUMBER').AsString;
        SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBillAct').NodeByName('wa:Header').NodeNew('wa:ActDate').Value:=FormatDateTime('yyyy-mm-dd',date);
        SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBillAct').NodeByName('wa:Header').NodeNew('wa:WBRegId').Value:=quDocInRec.FieldByName('WBREGID').Value;

        if rQAll>0 then SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBillAct').NodeByName('wa:Header').NodeNew('wa:Note').Value:='��������� ���������'
        else SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBillAct').NodeByName('wa:Header').NodeNew('wa:Note').Value:='�� ��������� ���������';

        SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBillAct').NodeNew('wa:Content');

        if (bDif)and(rQAll>0) then
        begin
          quSpecInSel.First;
          while not quSpecInSel.Eof do
          begin
            nodePos:=SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBillAct').NodeByName('wa:Content').NodeNew('wa:Position');

            sNum:=Trim(quSpecInSel.FieldByName('SNUM').AsString);
            if sNum='' then sNum:=its(quSpecInSel.FieldByName('NUM').AsInteger);

            nodePos.NodeNew('wa:Identity').Value:=sNum;
            nodePos.NodeNew('wa:InformBRegId').Value:=quSpecInSel.FieldByName('WBREGID').AsString;
            nodePos.NodeNew('wa:RealQuantity').Value:=quSpecInSel.FieldByName('QUANTF').AsString;

            quSpecInSel.Next;
          end;
        end;
        quSpecInSel.Active:=False;   //IdF

//        SendXml.SaveToFile(CommonSet.TmpDir+'Act'+its(IdF)+'.xml');
        SendXml.SaveToStream(FS);

        //����� � ����
        quToRar.Active:=False;
        quToRar.SQL.Clear;
        quToRar.SQL.Add('');
        quToRar.SQL.Add('SELECT [FSRARID],[ID],[DATEQU],[ITYPEQU],[ISTATUS],[SENDFILE],[RECEIVE_ID],[RECEIVE_FILE] FROM [dbo].[TORAR]');
        quToRar.SQL.Add('where [ID]='+its(IdF));
        quToRar.SQL.Add('and [FSRARID]='''+RARID+'''');
        quToRar.Active:=True;

        quToRar.Append;
        quToRar.FieldByName('FSRARID').AsString:=RARID;
        quToRar.FieldByName('ID').AsInteger:=IdF;
        quToRar.FieldByName('DATEQU').AsDateTime:=Now;
        quToRar.FieldByName('ITYPEQU').AsInteger:=2;
        quToRar.FieldByName('ISTATUS').AsInteger:=1;
//        quToRarSENDFILE.LoadFromFile(NameF);
        FS.Position := 0;
        quToRar.FieldByName('SENDFILE').AsString:='';
        TMemoField(quToRar.FieldByName('SENDFILE')).LoadFromStream(FS);
        quToRar.Post;

        quDocInRec.Edit;
        quDocInRec.FieldByName('DATE_OUTPUT').AsDateTime:=Now;
        FS.Position := 0;
        quDocInRec.FieldByName('SENDXML').AsString:='';
        TMemoField(quDocInRec.FieldByName('SENDXML')).LoadFromStream(FS);
        quDocInRec.Post;

        httpsend.MimeType := 'multipart/form-data; boundary='+Boundary;  //���������� Contetn-Type �������
        // ���������� Mime-��� � ������ �� �����
        s:='--'+Boundary+CRLF+'Content-Disposition: form-data; name="xml_file"; filename="'+IdToName(IdF)+'"'+CRLF+'Content-Type: application/xml'+CRLF+CRLF;
        httpsend.Document.Write(PAnsiChar(s)^, Length(s));
        FS.Position := 0;
        httpsend.Document.CopyFrom(FS, FS.Size);  //���������� ����� � ���� ���������
        S:=CRLF+CRLF+'--'+Boundary+'--'+CRLF;  //��������� ���� �������
        httpsend.Document.Write(PAnsiChar(s)^, Length(s)); // ��������� ���� ���������

        httpsend.KeepAlive:=False;
        httpsend.Status100:=True;
        httpsend.Headers.Add('Accept: */*');


        // ���������� ������
        if httpsend.HTTPMethod('POST','http://'+IPUTM+':8080/opt/in/WayBillAct') then   //http://localhost:8080/opt/in/WayBillAct
        begin
          prWMemo(Memo1,'TRUE');
          prWMemo(Memo1,'ResultCode='+IntToStr(httpsend.ResultCode));
          prWMemo(Memo1,httpsend.ResultString);

          WriteHistoryInPrivate(RARID,SID,iDate,'  �������� � UTM -  '+'TRUE  '+'ResultCode='+IntToStr(httpsend.ResultCode)+'  '+httpsend.ResultString,FDConn);

          if httpsend.ResultCode<>200 then Result:=False;

          httpsend.Document.Position:=0;
          RetXml.LoadFromStream(httpsend.Document);
          RetXml.XmlFormat := xfReadable;

          if Assigned(RetXml.Root.NodeByName('url')) then
          begin
            RetId:=RetXml.Root.NodeByName('url').Value;

            if RetId>'' then
            begin
              httpsend.Document.Position:=0;

              quToRar.Edit;
              quToRar.FieldByName('ISTATUS').AsInteger:=2;
              quToRar.FieldByName('RECEIVE_ID').AsString:=RetId;
              TMemoField(quToRar.FieldByName('RECEIVE_FILE')).LoadFromStream(httpsend.Document);
              quToRar.Post;
            end;
          end;
        end else begin
          WriteHistoryInPrivate(RARID,SID,iDate,'  ������ �������� � UTM. UTM �� ��������.',FDConn);

          quToRar.Edit;
          quToRar.FieldByName('ISTATUS').AsInteger:=100;
          quToRar.Post;

          Result:=False;
        end;
      end else
      begin
        prWMemo(Memo1,'WBREGID - �� ���������.');
        Result:=False;
      end;
    finally
      quSpecInSel.Active:=False;
      quSpecInSel.Free;

      quDocInRec.Active:=False;
      quDocInRec.Free;

      quS.Free;
      quToRar.Free;

      httpsend.Free;
      FS.Free;
      SendXml.Free;
      RetXml.Free;
    end;
  end;
end;



function prSendAct(RARID,SID:String;IDATE,iPrih:Integer;Memo1:tcxMemo):Boolean;
Const
  CR = #$0d;
  LF = #$0a;
  CRLF = CR + LF;
  Boundary = 'END_OF_PART';

var  AskXml: TNativeXml;
     IdF:Integer;
     SendXml,RetXml: TNativeXml;
     RetId:string;

     httpsend: THTTPSend;
     s: AnsiString;
     FS: TMemoryStream;

     rQAll:Real;
     bDif:Boolean;

     nodePos: TXmlNode;
     sNum:string;
begin
  Result:=True;
  with dmR do
  begin
    if dmR.FDConnection.Connected then
    begin
      try
        IdF:=fGetId(1);

        FS := TMemoryStream.Create;
        httpsend := THTTPSend.Create;

        quDocInRec.Active:=False;
        quDocInRec.ParamByName('RARID').AsString:=RARID;
        quDocInRec.ParamByName('IDATE').AsInteger:=IDATE;
        quDocInRec.ParamByName('SID').AsString:=SID;
        quDocInRec.Active:=True;
        if (quDocInRec.RecordCount>0)and(quDocInRecWBREGID.AsString>'') then
        begin
          rQAll:=0;
          bDif:=False;

          if iPrih=1 then  //������������ ����������
          begin
            bDif:=False;
            rQAll:=1;

            quUpdSpec.Active:=False;
            quUpdSpec.ParamByName('RARID').AsString:=quDocInRecFSRARID.AsString;
            quUpdSpec.ParamByName('IDATE').AsInteger:=quDocInRecIDATE.AsInteger;
            quUpdSpec.ParamByName('SID').AsString:=quDocInRecSID.AsString;
            quUpdSpec.ExecSQL;

          end else // ����� �� ������������
          begin
            quSpecInSel.Active:=False;
            quSpecInSel.ParamByName('RARID').AsString:=quDocInRecFSRARID.AsString;
            quSpecInSel.ParamByName('IDATE').AsInteger:=quDocInRecIDATE.AsInteger;
            quSpecInSel.ParamByName('SID').AsString:=quDocInRecSID.AsString;
            quSpecInSel.Active:=True;
            quSpecInSel.First;
            while not quSpecInSel.Eof do
            begin
              rQAll:=rQAll+quSpecInSelQUANTF.AsFloat;
              if abs(quSpecInSelQUANT.AsFloat-quSpecInSelQUANTF.AsFloat)>0.001 then bDif:=True;
              quSpecInSel.Next;
            end;
          end;

          //��������� XML
 
          SendXml := TNativeXml.Create(nil);
          RetXml  := TNativeXml.Create(nil);

          SendXml.XmlFormat := xfReadable;
          SendXml.CreateName('ns:Documents');
          SendXml.Root.WriteAttributeString('Version', '1.0');
          SendXml.Root.WriteAttributeString('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
          SendXml.Root.WriteAttributeString('xmlns:ns', 'http://fsrar.ru/WEGAIS/WB_DOC_SINGLE_01');
          SendXml.Root.WriteAttributeString('xmlns:oref', 'http://fsrar.ru/WEGAIS/ClientRef');
          SendXml.Root.WriteAttributeString('xmlns:pref', 'http://fsrar.ru/WEGAIS/ProductRef');
          SendXml.Root.WriteAttributeString('xmlns:wa', 'http://fsrar.ru/WEGAIS/ActTTNSingle');

          SendXml.Root.NodeNew('ns:Owner');
          SendXml.Root.NodeByName('ns:Owner').NodeNew('ns:FSRAR_ID').Value:=quDocInRecFSRARID.AsString;

          SendXml.Root.NodeNew('ns:Document');
          SendXml.Root.NodeByName('ns:Document').NodeNew('ns:WayBillAct');
          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBillAct').NodeNew('wa:Header');
        
          if rQAll>0 then SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBillAct').NodeByName('wa:Header').NodeNew('wa:IsAccept').Value:='Accepted'
          else SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBillAct').NodeByName('wa:Header').NodeNew('wa:IsAccept').Value:='Rejected';

          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBillAct').NodeByName('wa:Header').NodeNew('wa:ACTNUMBER').Value:=quDocInRecNUMBER.AsString;
          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBillAct').NodeByName('wa:Header').NodeNew('wa:ActDate').Value:=FormatDateTime('yyyy-mm-dd',date);
          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBillAct').NodeByName('wa:Header').NodeNew('wa:WBRegId').Value:=quDocInRecWBREGID.Value;

          if rQAll>0 then SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBillAct').NodeByName('wa:Header').NodeNew('wa:Note').Value:='��������� ���������'
          else SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBillAct').NodeByName('wa:Header').NodeNew('wa:Note').Value:='�� ��������� ���������';
              
          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBillAct').NodeNew('wa:Content');

          if (bDif)and(rQAll>0) then
          begin
            quSpecInSel.First;
            while not quSpecInSel.Eof do
            begin
              nodePos:=SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBillAct').NodeByName('wa:Content').NodeNew('wa:Position');

              sNum:=Trim(quSpecInSelSNUM.AsString);
              if sNum='' then sNum:=its(quSpecInSelNUM.AsInteger);

              nodePos.NodeNew('wa:Identity').Value:=sNum;
              nodePos.NodeNew('wa:InformBRegId').Value:=quSpecInSelWBREGID.AsString;
              nodePos.NodeNew('wa:RealQuantity').Value:=quSpecInSelQUANTF.AsString;

              quSpecInSel.Next;
            end;
          end;
          quSpecInSel.Active:=False;   //IdF

  //        SendXml.SaveToFile(CommonSet.TmpDir+'Act'+its(IdF)+'.xml');
          SendXml.SaveToStream(FS);


          //����� � ����
          quToRar.Active:=False;
          quToRar.ParamByName('ID').AsInteger:=IdF;
          quToRar.ParamByName('RARID').AsString:=CommonSet.FSRAR_ID;
          quToRar.Active:=True;

          quToRar.Append;
          quToRarFSRARID.AsString:=CommonSet.FSRAR_ID;
          quToRarID.AsInteger:=IdF;
          quToRarDATEQU.AsDateTime:=Now;
          quToRarITYPEQU.AsInteger:=2;
          quToRarISTATUS.AsInteger:=1;
  //        quToRarSENDFILE.LoadFromFile(NameF);
          FS.Position := 0;
          quToRarSENDFILE.AsString:='';
          quToRarSENDFILE.LoadFromStream(FS);
          quToRar.Post;

          quDocInRec.Edit;
          quDocInRecDATE_OUTPUT.AsDateTime:=Now;
          FS.Position := 0;
          quDocInRecSENDXML.AsString:='';
          quDocInRecSENDXML.LoadFromStream(FS);
          quDocInRec.Post;

          httpsend.MimeType := 'multipart/form-data; boundary='+Boundary;  //���������� Contetn-Type �������
          // ���������� Mime-��� � ������ �� �����
          s:='--'+Boundary+CRLF+'Content-Disposition: form-data; name="xml_file"; filename="'+IdToName(IdF)+'"'+CRLF+'Content-Type: application/xml'+CRLF+CRLF;
          httpsend.Document.Write(PAnsiChar(s)^, Length(s));
          FS.Position := 0;
          httpsend.Document.CopyFrom(FS, FS.Size);  //���������� ����� � ���� ���������
  //        S:=CRLF+CRLF+'--'+Boundary+CRLF;  //��������� ���� �������
          S:=CRLF+CRLF+'--'+Boundary+'--'+CRLF;  //��������� ���� �������
          httpsend.Document.Write(PAnsiChar(s)^, Length(s)); // ��������� ���� ���������

          httpsend.KeepAlive:=False;
          httpsend.Status100:=True;
          httpsend.Headers.Add('Accept: */*');


          // ���������� ������
          if httpsend.HTTPMethod('POST','http://'+CommonSet.IPUTM+':8080/opt/in/WayBillAct') then   //http://localhost:8080/opt/in/WayBillAct
          begin
            prWMemo(Memo1,'TRUE');
            prWMemo(Memo1,'ResultCode='+IntToStr(httpsend.ResultCode));
            prWMemo(Memo1,httpsend.ResultString);

            WriteHistoryIn(RARID,SID,iDate,'  �������� � UTM -  '+'TRUE  '+'ResultCode='+IntToStr(httpsend.ResultCode)+'  '+httpsend.ResultString);

            if httpsend.ResultCode<>200 then Result:=False;

            httpsend.Document.Position:=0;
            RetXml.LoadFromStream(httpsend.Document);
            RetXml.XmlFormat := xfReadable;

            if Assigned(RetXml.Root.NodeByName('url')) then
            begin
              RetId:=RetXml.Root.NodeByName('url').Value;

              if RetId>'' then
              begin
                httpsend.Document.Position:=0;

                quToRar.Edit;
                quToRarISTATUS.AsInteger:=2;
                quToRarRECEIVE_ID.AsString:=RetId;
                quToRarRECEIVE_FILE.LoadFromStream(httpsend.Document);
                quToRar.Post;
              end;
            end;
          end else begin
            WriteHistoryIn(RARID,SID,iDate,'  ������ �������� � UTM. UTM �� ��������.');

            quToRar.Edit;
            quToRarISTATUS.AsInteger:=100;
            quToRar.Post;

            Result:=False;
          end;
        end else
        begin
          prWMemo(Memo1,'WBREGID - �� ���������.');
          Result:=False;
        end;
      finally
        quSpecInSel.Active:=False;
        quDocInRec.Active:=False;
        httpsend.Free;
        FS.Free;
        SendXml.Free;
        RetXml.Free;
      end;
    end;
  end;
end;


function prSendTTN(RARID,SID:String;IDATE:Integer;Memo1:tcxMemo):Boolean;
Const
  CR = #$0d;
  LF = #$0a;
  CRLF = CR + LF;
  Boundary = 'END_OF_PART';

var  AskXml: TNativeXml;
     IdF:Integer;
     SendXml,RetXml: TNativeXml;
     RetId:string;

     httpsend: THTTPSend;
     s: AnsiString;
     FS: TMemoryStream;
     bNoQuant:Boolean;
     nodePos: TXmlNode;
     vCli,vCli1:TCli;
     iNum:Integer;
     RetVal:TStringList;

begin
  Result:=True;
  with dmR do
  begin
    try
      IdF:=fGetId(1);
      prWMemo(Memo1,'  IdF-'+its(IdF));

      FS := TMemoryStream.Create;
      httpsend := THTTPSend.Create;

      prWMemo(Memo1,'  ��������� ���������');
      quDocOutRec.Active:=False;
      quDocOutRec.ParamByName('RARID').AsString:=RARID;
      quDocOutRec.ParamByName('IDATE').AsInteger:=IDATE;
      quDocOutRec.ParamByName('SID').AsString:=SID;
      quDocOutRec.Active:=True;
      if quDocOutRec.RecordCount>0 then
      begin
        bNoQuant:=False;
        prWMemo(Memo1,'  ������������ ���������');

        quSpecOutSel.Active:=False;
        quSpecOutSel.SQL.Clear;
        quSpecOutSel.SQL.Add('');
        quSpecOutSel.SQL.Add('declare @FSRARID varchar(50) = '''+RARID+'''');
        quSpecOutSel.SQL.Add('declare @IDATE int = '+its(IDATE));
        quSpecOutSel.SQL.Add('declare @SID varchar(50) = '''+SID+'''');
        quSpecOutSel.SQL.Add('SELECT sp.[FSRARID],sp.[IDATE],sp.[SIDHD],sp.[ID],sp.[NUM],sp.[ALCCODE]');
        quSpecOutSel.SQL.Add('       ,sp.[QUANT]');
        quSpecOutSel.SQL.Add('  	   ,sp.[QUANT] as QUANTREMN');
        quSpecOutSel.SQL.Add('	   ,sp.[QUANTF]');
        quSpecOutSel.SQL.Add('	   ,sp.[PRICE],sp.[PRICEF]');
        quSpecOutSel.SQL.Add('       ,ca.NAME, ca.VOL, ca.KREP, ca.[AVID], ca.PRODID');
        quSpecOutSel.SQL.Add('       ,prod.NAME as PRODNAME');
        quSpecOutSel.SQL.Add('       ,prod.[PRODINN]');
        quSpecOutSel.SQL.Add('       ,prod.[PRODKPP]');
        quSpecOutSel.SQL.Add('       ,prod.[FULLNAME] as PRODFULLNAME');
        quSpecOutSel.SQL.Add('       ,prod.[COUNTRY] as PRODCOUNTRY');
        quSpecOutSel.SQL.Add('       ,prod.[REGCODE] as PRODREGCODE');
        quSpecOutSel.SQL.Add('       ,prod.[ADDR] as PRODADDR');
        quSpecOutSel.SQL.Add('       ,sp.[WBREGID]');
        quSpecOutSel.SQL.Add('       ,0 as [CODECB]');
        quSpecOutSel.SQL.Add('       ,sp.[INFO_A],sp.[INFO_B]');
        quSpecOutSel.SQL.Add('  FROM [dbo].[ADOCSOUT_SP] sp');
        quSpecOutSel.SQL.Add('  left join [dbo].[ACARDS] ca on ca.KAP=sp.[ALCCODE]');
        quSpecOutSel.SQL.Add('  left join [dbo].[APRODS] prod on prod.PRODID=ca.PRODID');

  //      if CommonSet.SDEP>'' then quSpecOutSel.SQL.Add('  left join [Ecrystal].[dbo].[EgaisGoods] ega on ega.Id=sp.[ALCCODE] collate Cyrillic_General_CS_AS_WS')
  //      else quSpecOutSel.SQL.Add('  left join [EcrystalR].[dbo].[EgaisGoods] ega on ega.Id=sp.[ALCCODE]');

        quSpecOutSel.SQL.Add('  where sp.[FSRARID]=@FSRARID');
        quSpecOutSel.SQL.Add('  and sp.[IDATE]=@IDATE');
        quSpecOutSel.SQL.Add('  and sp.[SIDHD]=@SID');

        {
        quSpecOutSel.ParamByName('RARID').AsString:=quDocOutRecFSRARID.AsString;
        quSpecOutSel.ParamByName('IDATE').AsInteger:=quDocOutRecIDATE.AsInteger;
        quSpecOutSel.ParamByName('SID').AsString:=quDocOutRecSID.AsString;
        }
        quSpecOutSel.Active:=True;
        quSpecOutSel.First;

        if bNoQuant then
        begin
          prWMemo(Memo1,'�������� ��������� ����������.');
          Result:=False;
        end else
        begin
          //��������� XML
          prWMemo(Memo1,'  ��������� ������...');
          SendXml := TNativeXml.Create(nil);
          RetXml  := TNativeXml.Create(nil);

          SendXml.XmlFormat := xfReadable;
          SendXml.CreateName('ns:Documents');
          SendXml.Root.WriteAttributeString('Version', '1.0');
          SendXml.Root.WriteAttributeString('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
          SendXml.Root.WriteAttributeString('xmlns:ns', 'http://fsrar.ru/WEGAIS/WB_DOC_SINGLE_01');
          SendXml.Root.WriteAttributeString('xmlns:c', 'http://fsrar.ru/WEGAIS/Common');
          SendXml.Root.WriteAttributeString('xmlns:oref', 'http://fsrar.ru/WEGAIS/ClientRef');
          SendXml.Root.WriteAttributeString('xmlns:pref', 'http://fsrar.ru/WEGAIS/ProductRef');
          SendXml.Root.WriteAttributeString('xmlns:wb', 'http://fsrar.ru/WEGAIS/TTNSingle');

          SendXml.Root.NodeNew('ns:Owner');
          SendXml.Root.NodeByName('ns:Owner').NodeNew('ns:FSRAR_ID').Value:=quDocOutRecFSRARID.AsString;

          SendXml.Root.NodeNew('ns:Document');
          SendXml.Root.NodeByName('ns:Document').NodeNew('ns:WayBill');
          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeNew('wb:Identity').Value:=Trim(quDocOutRecSID.AsString);

          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeNew('wb:Header');

          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeByName('wb:Header').NodeNew('wb:NUMBER').Value:=Trim(quDocOutRecNUMBER.AsString);
          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeByName('wb:Header').NodeNew('wb:Date').Value:=dsrar(date);
          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeByName('wb:Header').NodeNew('wb:ShippingDate').Value:=dsrar(quDocOutRecDATEDOC.AsDateTime);

          //���������� - ������������ ���
          if CommonSet.ISS=2 then SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeByName('wb:Header').NodeNew('wb:Type').Value:='WBInvoiceFromMe'
          else SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeByName('wb:Header').NodeNew('wb:Type').Value:='WBReturnFromMe';

          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeByName('wb:Header').NodeNew('wb:UnitType').Value:='Packed';

          vCli.ClientRegId:=quDocOutRecFSRARID.AsString;

          prGetAtr(vCli,vCli);
          vCli1:=vCli;

          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeByName('wb:Header').NodeNew('wb:Shipper');

          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeByName('wb:Header').NodeByName('wb:Shipper').NodeNew('oref:INN').Value:=vCli.INN;
          if Trim(vCli.KPP)>'' then  SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeByName('wb:Header').NodeByName('wb:Shipper').NodeNew('oref:KPP').Value:=vCli.KPP;
          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeByName('wb:Header').NodeByName('wb:Shipper').NodeNew('oref:ClientRegId').Value:=vCli.ClientRegId;
          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeByName('wb:Header').NodeByName('wb:Shipper').NodeNew('oref:FullName').Value:=vCli.FullName;
          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeByName('wb:Header').NodeByName('wb:Shipper').NodeNew('oref:ShortName').Value:=vCli.ShortName;
          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeByName('wb:Header').NodeByName('wb:Shipper').NodeNew('oref:address');
          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeByName('wb:Header').NodeByName('wb:Shipper').NodeByName('oref:address').NodeNew('oref:Country').Value:=vCli.Country;
          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeByName('wb:Header').NodeByName('wb:Shipper').NodeByName('oref:address').NodeNew('oref:description').Value:=vCli.description;

          vCli.ClientRegId:=quDocOutRecCLIFTO.AsString;

          prGetAtrCli(vCli,vCli);

          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeByName('wb:Header').NodeNew('wb:Consignee');

          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeByName('wb:Header').NodeByName('wb:Consignee').NodeNew('oref:INN').Value:=vCli.INN;
          if Trim(vCli.KPP)>'' then SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeByName('wb:Header').NodeByName('wb:Consignee').NodeNew('oref:KPP').Value:=vCli.KPP;
          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeByName('wb:Header').NodeByName('wb:Consignee').NodeNew('oref:ClientRegId').Value:=vCli.ClientRegId;
          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeByName('wb:Header').NodeByName('wb:Consignee').NodeNew('oref:FullName').Value:=vCli.FullName;
          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeByName('wb:Header').NodeByName('wb:Consignee').NodeNew('oref:ShortName').Value:=vCli.ShortName;
          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeByName('wb:Header').NodeByName('wb:Consignee').NodeNew('oref:address');
          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeByName('wb:Header').NodeByName('wb:Consignee').NodeByName('oref:address').NodeNew('oref:Country').Value:=vCli.Country;
          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeByName('wb:Header').NodeByName('wb:Consignee').NodeByName('oref:address').NodeNew('oref:description').Value:=vCli.description;

          vCli:=vCli1;

          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeByName('wb:Header').NodeNew('wb:Supplier');

          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeByName('wb:Header').NodeByName('wb:Supplier').NodeNew('oref:INN').Value:=vCli.INN;
          if Trim(vCli.KPP)>'' then SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeByName('wb:Header').NodeByName('wb:Supplier').NodeNew('oref:KPP').Value:=vCli.KPP;
          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeByName('wb:Header').NodeByName('wb:Supplier').NodeNew('oref:ClientRegId').Value:=vCli.ClientRegId;
          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeByName('wb:Header').NodeByName('wb:Supplier').NodeNew('oref:FullName').Value:=vCli.FullName;
          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeByName('wb:Header').NodeByName('wb:Supplier').NodeNew('oref:ShortName').Value:=vCli.ShortName;
          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeByName('wb:Header').NodeByName('wb:Supplier').NodeNew('oref:address');
          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeByName('wb:Header').NodeByName('wb:Supplier').NodeByName('oref:address').NodeNew('oref:Country').Value:=vCli.Country;
          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeByName('wb:Header').NodeByName('wb:Supplier').NodeByName('oref:address').NodeNew('oref:description').Value:=vCli.description;

          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeByName('wb:Header').NodeNew('wb:Transport');
          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeByName('wb:Header').NodeByName('wb:Transport').NodeNew('wb:TRAN_COMPANY').Value:='-';
          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeByName('wb:Header').NodeByName('wb:Transport').NodeNew('wb:TRAN_CAR').Value:='-';
          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeByName('wb:Header').NodeByName('wb:Transport').NodeNew('wb:TRAN_CUSTOMER').Value:='-';
          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeByName('wb:Header').NodeByName('wb:Transport').NodeNew('wb:TRAN_DRIVER').Value:='-';
          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeByName('wb:Header').NodeByName('wb:Transport').NodeNew('wb:TRAN_LOADPOINT').Value:='-';
          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeByName('wb:Header').NodeByName('wb:Transport').NodeNew('wb:TRAN_UNLOADPOINT').Value:='-';
          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeByName('wb:Header').NodeByName('wb:Transport').NodeNew('wb:TRAN_FORWARDER').Value:='-';

{
<wb:Transport>
					<wb:TRAN_COMPANY>��� "��������"</wb:TRAN_COMPANY>
					<wb:TRAN_CAR>���                                                                                                 X441OY174</wb:TRAN_CAR>
					<wb:TRAN_CUSTOMER>�������� � ������������ ����������������  "����"</wb:TRAN_CUSTOMER>
					<wb:TRAN_DRIVER>�������� ���� �������������</wb:TRAN_DRIVER>
					<wb:TRAN_LOADPOINT>,624092,������������ ���,,������� ����� �,������� �,�������������� ������,1,2,</wb:TRAN_LOADPOINT>
					<wb:TRAN_UNLOADPOINT>,620141,������������ ���,,������������ �,,������ ��,184,,</wb:TRAN_UNLOADPOINT>
					<wb:TRAN_FORWARDER>�������� ���� �������������</wb:TRAN_FORWARDER>
				</wb:Transport>
 }

          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeByName('wb:Header').NodeNew('wb:Base').Value:='������� ��������';
          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeByName('wb:Header').NodeNew('wb:Note').Value:='-';

          SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeNew('wb:Content');

          iNum:=1;
          quSpecOutSel.First;
          while not quSpecOutSel.Eof do
          begin
            if quSpecOutSelQUANTF.AsFloat>0 then
            begin
              nodePos:=SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeByName('wb:Content').NodeNew('wb:Position');

//            nodePos:=SendXml.Root.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeByName('wb:Content').NodeByName('wb:Position');

              nodePos.NodeNew('wb:Identity').Value:=its(iNum);
              nodePos.NodeNew('wb:Quantity').Value:=fts(r1000(quSpecOutSelQUANTF.AsFloat));
              nodePos.NodeNew('wb:Price').Value:=fts(r1000(quSpecOutSelPRICE.AsFloat));

              nodePos.NodeNew('wb:InformA');
              nodePos.NodeByName('wb:InformA').NodeNew('pref:RegId').Value:=quSpecOutSelINFO_A.AsString;

              nodePos.NodeNew('wb:InformB');
              nodePos.NodeByName('wb:InformB').NodeNew('pref:InformBItem');
              nodePos.NodeByName('wb:InformB').NodeByName('pref:InformBItem').NodeNew('pref:BRegId').Value:=quSpecOutSelWBREGID.AsString; //  quSpecOutSelINFO_B.AsString;

              nodePos.NodeNew('wb:Product');
              nodePos.NodeByName('wb:Product').NodeNew('pref:FullName').Value:=quSpecOutSelNAME.AsString;
              nodePos.NodeByName('wb:Product').NodeNew('pref:AlcCode').Value:=quSpecOutSelALCCODE.AsString;
              nodePos.NodeByName('wb:Product').NodeNew('pref:Capacity').Value:=fts(r1000(quSpecOutSelVOL.AsFloat));
              nodePos.NodeByName('wb:Product').NodeNew('pref:AlcVolume').Value:=fts(r1000(quSpecOutSelKREP.AsFloat));
              nodePos.NodeByName('wb:Product').NodeNew('pref:ProductVCode').Value:=quSpecOutSelAVID.AsString;

              if Trim(quSpecOutSelPRODID.Asstring)<>'' then begin
                nodePos.NodeByName('wb:Product').NodeNew('pref:Producer');
                nodePos.NodeByName('wb:Product').NodeByName('pref:Producer').NodeNew('oref:ClientRegId').Value:=quSpecOutSelPRODID.Asstring;
                nodePos.NodeByName('wb:Product').NodeByName('pref:Producer').NodeNew('oref:FullName').Value:=quSpecOutSelPRODFULLNAME.Asstring;
                nodePos.NodeByName('wb:Product').NodeByName('pref:Producer').NodeNew('oref:ShortName').Value:=quSpecOutSelPRODNAME.Asstring;
                nodePos.NodeByName('wb:Product').NodeByName('pref:Producer').NodeNew('oref:address');
                nodePos.NodeByName('wb:Product').NodeByName('pref:Producer').NodeByName('oref:address').NodeNew('oref:Country').Value:=quSpecOutSelPRODCOUNTRY.AsString;
                nodePos.NodeByName('wb:Product').NodeByName('pref:Producer').NodeByName('oref:address').NodeNew('oref:description').Value:=quSpecOutSelPRODADDR.AsString;
              end;

            end;

            quSpecOutSel.Next; inc(iNum);
          end;

          quSpecOutSel.Active:=False;   //IdF

          SendXml.SaveToStream(FS);

          quDocOutRec.Edit;
          quDocOutRecDATE_OUTPUT.AsDateTime:=Now;
          FS.Position := 0;
          quDocOutRecSENDXML.AsString:='';
          quDocOutRecSENDXML.LoadFromStream(FS);
          quDocOutRec.Post;


          //����� � ����
          prWMemo(Memo1,'  ��������� ������ � ���� ...');

          quToRar.Active:=False;
          quToRar.ParamByName('ID').AsInteger:=IdF;
          quToRar.ParamByName('RARID').AsString:=CommonSet.FSRAR_ID;
          quToRar.Active:=True;

          quToRar.Append;
          quToRarFSRARID.AsString:=CommonSet.FSRAR_ID;
          quToRarID.AsInteger:=IdF;
          quToRarDATEQU.AsDateTime:=Now;
          quToRarITYPEQU.AsInteger:=2;
          quToRarISTATUS.AsInteger:=1;
  //        quToRarSENDFILE.LoadFromFile(NameF);
          FS.Position := 0;
          quToRarSENDFILE.LoadFromStream(FS);
          quToRar.Post;

          prWMemo(Memo1,'  ��������� ������ ...');

          httpsend.MimeType := 'multipart/form-data; boundary='+Boundary;  //���������� Contetn-Type �������
          // ���������� Mime-��� � ������ �� �����
          s:='--'+Boundary+CRLF+'Content-Disposition: form-data; name="xml_file"; filename="'+IdToName(IdF)+'"'+CRLF+'Content-Type: application/xml'+CRLF+CRLF;
          httpsend.Document.Write(PAnsiChar(s)^, Length(s));
          FS.Position := 0;
          httpsend.Document.CopyFrom(FS, FS.Size);  //���������� ����� � ���� ���������
  //        S:=CRLF+CRLF+'--'+Boundary+CRLF;  //��������� ���� �������
          S:=CRLF+CRLF+'--'+Boundary+'--'+CRLF;  //��������� ���� �������
          httpsend.Document.Write(PAnsiChar(s)^, Length(s)); // ��������� ���� ���������

          httpsend.KeepAlive:=False;
          //httpsend.Protocol:='1.1';  //��������
          httpsend.Status100:=True;
          httpsend.Headers.Add('Accept: */*');

          // ���������� ������
          if httpsend.HTTPMethod('POST','http://'+CommonSet.IPUTM+':8080/opt/in/WayBill') then   //http://localhost:8080/opt/in/WayBillAct
          begin
            prWMemo(Memo1,'TRUE');
            prWMemo(Memo1,'ResultCode='+IntToStr(httpsend.ResultCode));
            prWMemo(Memo1,httpsend.ResultString);

            if httpsend.ResultCode<>200 then
            begin
              Result:=False;
              try
                RetVal:=TStringList.Create;
                RetVal.LoadFromStream(httpsend.Document, TEncoding.UTF8);
                prWMemo(Memo1,RetVal.Text);
              finally
                RetVal.Free;
              end;
            end;

            httpsend.Document.Position:=0;
            RetXml.LoadFromStream(httpsend.Document);
            RetXml.XmlFormat := xfReadable;

            if Assigned(RetXml.Root.NodeByName('url')) then
            begin
              RetId:=RetXml.Root.NodeByName('url').Value;

              if RetId>'' then
              begin
                httpsend.Document.Position:=0;

                quToRar.Edit;
                quToRarISTATUS.AsInteger:=2;
                quToRarRECEIVE_ID.AsString:=RetId;
                quToRarRECEIVE_FILE.LoadFromStream(httpsend.Document);
                quToRar.Post;
              end;
            end;
          end else begin
            prWMemo(Memo1,'  ������ ���������� ������� - POST,http://'+CommonSet.IPUTM+':8080/opt/in/WayBill');
            prWMemo(Memo1,'ResultCode='+IntToStr(httpsend.ResultCode));
            prWMemo(Memo1,httpsend.ResultString);

            try
              RetVal:=TStringList.Create;
              RetVal.LoadFromStream(httpsend.Document, TEncoding.UTF8);
              prWMemo(Memo1,RetVal.Text);
            finally
              RetVal.Free;
            end;

            quToRar.Edit;
            quToRarISTATUS.AsInteger:=100;
            quToRar.Post;

            Result:=False;
          end;
        end;
      end;
    finally
//        quSpecOutSel.Active:=False;
      quDocOutRec.Active:=False;
      httpsend.Free;
      FS.Free;
      SendXml.Free;
      RetXml.Free;
    end;
  end;
end;



function tf(testnode: TXmlNode):String;
begin
  if Assigned(testnode) then
  begin
    Result:=testnode.Value
  end else
  begin
    Result:='';
  end;
end;


function stf(sV:String):Real;
var sV1:string;
begin
  sV1:=Trim(sV);
  while Pos('.',sV1)>0 do sV1[Pos('.',sV1)]:=',';
  Result:=StrToFloatDef(sV1,0);
end;


Function fts(rSum:Real):String;
Var s:String;
begin
  s:=FloatToStr(rSum);
  while pos(',',s)>0 do s[pos(',',s)]:='.';
  Result:=s;
end;

Function fts00(rSum:Real):String;
Var s:String;
    iRub,iKop:INteger;
    rKop:Real;
begin
  iRub:=Trunc(rSum);
  rKop:=frac(rSum)*100;
  iKop:=RoundEx(rKop);
  s:=its(iKop);
  if length(s)<2 then s:='0'+s;
  s:=its(iRub)+'.'+s;
  Result:=s;
end;

Function fts0(rSum:Real):String;
Var s:String;
    iRub,iKop:INteger;
    rKop:Real;
begin
  iRub:=Trunc(rSum);
  rKop:=frac(rSum)*1000;
  iKop:=RoundEx(rKop);
  s:=its(iKop);
  if length(s)<1 then s:='0'+s;
  s:=its(iRub)+'.'+s;
  Result:=s;
end;


function prDecodeWB(xmlstr:string;IdRec:integer; FDConn: TFDConnection; RARID:string):Boolean;
type

     TCli = record
              ClientRegId,FullName,ShortName,INN,KPP,Country,RegionCode,description:string;
            end;

     TDocInfo = record
              sIDH:string; //ID ���������
              WBRegId,EGAISFixNumber,EGAISFixDate:string;
              CliFrom:TCli;
              CliTo:TCli; //��� ������ ����
            end;

     TPos = record
              Num:Integer;
              InformBRegId:string;
            end;



var
  Xml: TNativeXml;
  nodeRoot,nodeHead,nodeSpec,nodePos: TXmlNode;
  I: integer;
  StrWk:string;
  iDate:Integer;
  vDocInfo:TDocInfo;
  vPos:TPos;
  quS,quSelD: TFDQuery;

  procedure ClearDoc;
  begin
    vDocInfo.sIDH:=''; vDocInfo.WBRegId:=''; vDocInfo.EGAISFixNumber:=''; vDocInfo.EGAISFixDate:='';
    vDocInfo.CliTo.ClientRegId:='';
    vDocInfo.CliFrom.ClientRegId:=''; vDocInfo.CliFrom.INN:=''; vDocInfo.CliFrom.KPP:=''; vDocInfo.CliFrom.FullName:=''; vDocInfo.CliFrom.ShortName:=''; vDocInfo.CliFrom.Country:=''; vDocInfo.CliFrom.RegionCode:=''; vDocInfo.CliFrom.description:='';
  end;

  procedure ClearPos;
  begin
    vPos.Num:=0;
    vPos.InformBRegId:='';
  end;

begin
  Result:=False;

  if Pos('TTNInformBReg',xmlstr)>0 then    //�������������� ���� ����
  begin
    try
      quS := TFDQuery.Create(nil);
      quS.Connection := FDConn;
      quS.ResourceOptions.SilentMode:=True;     //�� ���������� Wait cursor

      quSelD := TFDQuery.Create(nil);
      quSelD.Connection := FDConn;
      quSelD.ResourceOptions.SilentMode:=True;     //�� ���������� Wait cursor

      Xml:= TNativeXml.Create(nil);
      Xml.XmlFormat := xfReadable;
      while Pos('''',xmlstr)>0 do xmlstr[Pos('''',xmlstr)]:=' ';

      ClearDoc;

      Xml.ReadFromString(xmlstr);

      nodeRoot:=Xml.Root;
      if Assigned(nodeRoot.NodeByName('ns:Document')) then
      begin
        if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:TTNInformBReg')) then
        begin
          if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:TTNInformBReg').NodeByName('wbr:Header')) then
          begin
            nodeHead:=nodeRoot.NodeByName('ns:Document').NodeByName('ns:TTNInformBReg').NodeByName('wbr:Header');

            if Assigned(nodeHead.NodeByName('wbr:Identity')) then vDocInfo.sIDH:=nodeHead.NodeByName('wbr:Identity').Value;

            if vDocInfo.sIDH>='' then    //��������
            begin
              if Assigned(nodeHead.NodeByName('wbr:WBRegId')) then vDocInfo.WBRegId:=nodeHead.NodeByName('wbr:WBRegId').Value;
              if Assigned(nodeHead.NodeByName('wbr:EGAISFixNumber')) then vDocInfo.EGAISFixNumber:=nodeHead.NodeByName('wbr:EGAISFixNumber').Value;
              if Assigned(nodeHead.NodeByName('wbr:EGAISFixDate')) then vDocInfo.EGAISFixDate:=nodeHead.NodeByName('wbr:EGAISFixDate').Value;

              if Assigned(nodeHead.NodeByName('wbr:WBDate')) then StrWk:=nodeHead.NodeByName('wbr:WBDate').Value;

              if Length(vDocInfo.sIDH)<3 then
                if Assigned(nodeHead.NodeByName('wbr:WBNUMBER')) then vDocInfo.sIDH:=nodeHead.NodeByName('wbr:WBNUMBER').Value;


              while Pos('-',StrWk)>0 do Delete(StrWk,Pos('-',StrWk),1);
              StrWk:=Copy(StrWk,7,2)+'.'+Copy(StrWk,5,2)+'.'+Copy(StrWk,1,4);
              IDate:=Trunc(StrToDateTimeDef(StrWk,date));

              try
                quSelD.Active:=False;
                quSelD.SQL.Clear;
                quSelD.SQL.Add('');
                quSelD.SQL.Add('Select * from [dbo].[ADOCSIN_HD]');
                quSelD.SQL.Add('WHERE [FSRARID]= '''+Trim(RARID)+'''');
                quSelD.SQL.Add('and [IDATE]='+its(IDate));
                quSelD.SQL.Add('and [SID]='''+Trim(vDocInfo.sIDH)+'''');
                quSelD.Active:=True;

                if quSelD.RecordCount>0 then
                begin
                  quS.Active:=False;
                  quS.SQL.Clear;
                  quS.SQL.Add('');
                  quS.SQL.Add('UPDATE [dbo].[ADOCSIN_HD]');
                  quS.SQL.Add('SET');
                  quS.SQL.Add('      [WBREGID] = '''+vDocInfo.WBRegId+'''');
                  quS.SQL.Add('      ,[FIXNUMBER] = '''+vDocInfo.EGAISFixNumber+'''');
                  quS.SQL.Add('      ,[FIXDATE] = '''+vDocInfo.EGAISFixDate+'''');
                  quS.SQL.Add('WHERE [FSRARID]= '''+Trim(RARID)+'''');
                  quS.SQL.Add('and [IDATE]='+its(IDate));
                  quS.SQL.Add('and [SID]='''+Trim(vDocInfo.sIDH)+'''');
                  quS.ExecSQL;

                  if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:TTNInformBReg').NodeByName('wbr:Content')) then
                  begin
                    nodeSpec:=nodeRoot.NodeByName('ns:Document').NodeByName('ns:TTNInformBReg').NodeByName('wbr:Content');

                    for i:=0 to nodeSpec.NodeCount-1 do
                    begin
                      nodePos:=nodeSpec[i];

                      ClearPos;

                      if Pos('wbr:Position',nodePos.Name)>0 then  //��� ������� ���������
                      begin
                        if Assigned(nodePos.NodeByName('wbr:Identity')) then vPos.Num:=StrToIntDef(nodePos.NodeByName('wbr:Identity').Value,0);
                        if Assigned(nodePos.NodeByName('wbr:InformBRegId')) then vPos.InformBRegId:=nodePos.NodeByName('wbr:InformBRegId').Value;

                        if (vPos.Num>0)and(vPos.InformBRegId>'') then
                        begin
                          quS.Active:=False;
                          quS.SQL.Clear;
                          quS.SQL.Add('');
                          quS.SQL.Add('UPDATE [dbo].[ADOCSIN_SP]');
                          quS.SQL.Add('SET  [WBREGID] = '''+vPos.InformBRegId+'''');
                          quS.SQL.Add('WHERE [FSRARID]= '''+Trim(RARID)+'''');
                          quS.SQL.Add('and [IDATE]='+its(IDate));
                          quS.SQL.Add('and [SIDHD]='''+Trim(vDocInfo.sIDH)+'''');
                          quS.SQL.Add('and [NUM]='+its(vPos.Num));
                          quS.ExecSQL;
                        end;
                      end;
                    end;
                  end;

                  WriteHistoryInPrivate(RARID,vDocInfo.sIDH,iDate,'������� ������� "B" (WBREGID '+vDocInfo.WBRegId+'). (ID '+its(IdRec)+')',FDConn);
                  Result:=True;
                end else Result:=False;
                quSelD.Active:=False;
              except
                Result:=False;
              end;
            end;
          end;
        end;
      end;
    finally
      quSelD.Active:=False;
      quSelD.Free;
      quS.Free;
      Xml.Free;
    end;
  end;
end;

function prDecodeActInv(xmlstr:string;IdRec:integer; FDConn: TFDConnection; RARID:String):Boolean;
type

     TCli = record
              ClientRegId,FullName,ShortName,INN,KPP,Country,RegionCode,description:string;
            end;

     TCard = record
               FullName,AlcCode:string;
               Capacity,AlcVolume:Real;
               ProductVCode:Integer;
               Prod:TCli;
               Impr:TCli;
             end;

     TPos = record
              Num:Integer;
              Quant,Price:Real;
              Card:TCard;
              info_a,info_b,party:string;
              sNum:string;
            end;

var  iC:Integer;
     Xml: TNativeXml;
     nodeRoot,nodePos,nodeSpec: TXmlNode;
     IDH,iDateInv,i:integer;
     SFIXDATE:string;
     vPos:TPos;
     quS,quSelD: TFDQuery;

  procedure ClearPos;
  begin
    vPos.Num:=0;
    vPos.Quant:=0; vPos.Price:=0;
    vPos.Card.FullName:=''; vPos.Card.AlcCode:=''; vPos.Card.Capacity:=0; vPos.Card.AlcVolume:=0; vPos.Card.ProductVCode:=0; vPos.info_a:=''; vPos.info_b:=''; vPos.party:='';
    vPos.Card.Prod.ClientRegId:=''; vPos.Card.Prod.FullName:=''; vPos.Card.Prod.ShortName:=' '; vPos.Card.Prod.INN:=' '; vPos.Card.Prod.INN:=' '; vPos.Card.Prod.KPP:=' '; vPos.Card.Prod.Country:=' '; vPos.Card.Prod.RegionCode:=' '; vPos.Card.Prod.description:=' ';
    vPos.Card.Impr.ClientRegId:=''; vPos.Card.Impr.FullName:=''; vPos.Card.Impr.ShortName:=' '; vPos.Card.Impr.INN:=' '; vPos.Card.Impr.INN:=' '; vPos.Card.Impr.KPP:=' '; vPos.Card.Impr.Country:=' '; vPos.Card.Impr.RegionCode:=' '; vPos.Card.Impr.description:=' ';
    vPos.sNum:='';
  end;

begin
  Result:=False;
  if (Pos('ns:ReplyRests',xmlstr)>0)or(Pos('ns:ReplyRestsShop_v2',xmlstr)>0) then  // ��� �������
  begin
    //������� ������������
    try
//      ClearMessageLogLocal;
//      ShowMessageLogLocal('����� ... ���� ������������ ���������.');

      Xml:= TNativeXml.Create(nil);
      Xml.XmlFormat := xfReadable;
//          while Pos('''',xmlstr)>0 do xmlstr[Pos('''',xmlstr)]:='"';

      Xml.ReadFromString(xmlstr);

      quS := TFDQuery.Create(nil);
      quS.Connection := FDConn;
      quS.ResourceOptions.SilentMode:=True;     //�� ���������� Wait cursor

      quSelD := TFDQuery.Create(nil);
      quSelD.Connection := FDConn;
      quSelD.ResourceOptions.SilentMode:=True;     //�� ���������� Wait cursor

      quSelD.Active:=False;
      quSelD.SQL.Clear;
      quSelD.SQL.Add('');
      quSelD.SQL.Add('Select * from [dbo].[ADOCSINV_HD]');
      quSelD.SQL.Add('WHERE [FSRARID]= '''+Trim(RARID)+'''');
      quSelD.SQL.Add('and [IDREPLY]='+its(IdRec));
      quSelD.Active:=True;

      if quSelD.RecordCount=0 then  //�������������� ��� ���
      begin
        nodeRoot:=Xml.Root;
        IDH:=fGetIdPrivate(6,FDConn);
        iDateInv:=Trunc(Date);

        if Assigned(nodeRoot.NodeByName('ns:Document')) then
        begin
          if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:ReplyRests')) then
          begin  // �����  (1 �������)
            SFIXDATE:='';
            if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:ReplyRests').NodeByName('rst:RestsDate')) then
              SFIXDATE:=nodeRoot.NodeByName('ns:Document').NodeByName('ns:ReplyRests').NodeByName('rst:RestsDate').Value;

            //��������� ���������
  //          ShowMessageLogLocal('  - ��������� ��������� ��������� ('+its(IDH)+')');

            quS.Active:=False;
            quS.SQL.Clear;
            quS.SQL.Add('');
            quS.SQL.Add('INSERT INTO [dbo].[ADOCSINV_HD] ([FSRARID],[IDATE],[ID],[STYPE],[FIXDATE],[IDREPLY])');
            quS.SQL.Add('VALUES ('''+RARID+'''');
            quS.SQL.Add('           ,'+its(iDateInv));
            quS.SQL.Add('           ,'+its(IDH));
            quS.SQL.Add('           ,''STOCK ( 1-reg )''');
            quS.SQL.Add('           ,'''+SFIXDATE+'''');
            quS.SQL.Add('           ,'+its(IdRec)+')');
            quS.ExecSQL;

            if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:ReplyRests').NodeByName('rst:Products')) then //������������
            begin
              nodeSpec:=nodeRoot.NodeByName('ns:Document').NodeByName('ns:ReplyRests').NodeByName('rst:Products');

  //            ShowMessageLogLocal('  - ��������� ������������ ( '+its(nodeSpec.NodeCount)+' ������� )');

              for i:=0 to nodeSpec.NodeCount-1 do
              begin
                nodePos:=nodeSpec[i];

  //              if i mod 100 = 0 then  ShowMessageLogLocal('     - ���������� '+its(iC)+' �������');

                ClearPos;

                if Pos('rst:StockPosition',nodePos.Name)>0 then  //��� �������
                begin
                  if Assigned(nodePos.NodeByName('rst:Quantity')) then vPos.Quant:=stf(nodePos.NodeByName('rst:Quantity').Value);
                  if Assigned(nodePos.NodeByName('rst:InformARegId')) then vPos.info_a:=nodePos.NodeByName('rst:InformARegId').Value;
                  if Assigned(nodePos.NodeByName('rst:InformBRegId')) then vPos.info_b:=nodePos.NodeByName('rst:InformBRegId').Value;
                  if Assigned(nodePos.NodeByName('rst:Product')) then
                    if Assigned(nodePos.NodeByName('rst:Product').NodeByName('pref:AlcCode')) then vPos.Card.AlcCode:=nodePos.NodeByName('rst:Product').NodeByName('pref:AlcCode').Value;

                  if vPos.Card.AlcCode>'' then  //������� � �������� ���������� - ��������
                  begin
                    try
                      quS.Active:=False;
                      quS.SQL.Clear;
                      quS.SQL.Add('');
                      quS.SQL.Add('INSERT INTO [dbo].[ADOCSINV_SP] ([IDH],[ID],[CAP],[QUANTR],[QUANTF],[INFO_A],[INFO_B])');
                      quS.SQL.Add('     VALUES');
                      quS.SQL.Add('           ('+its(IDH));
                      quS.SQL.Add('           ,'+its(iC+1));
                      quS.SQL.Add('           ,'''+vPos.Card.AlcCode+'''');
                      quS.SQL.Add('           ,'+fts(vPos.Quant));
                      quS.SQL.Add('           ,0');
                      quS.SQL.Add('           ,'''+vPos.info_a+'''');
                      quS.SQL.Add('           ,'''+vPos.info_b+''')');
                      quS.ExecSQL;
                    except
  //                    ShowMessageLogLocal('     ������ ���������� ('+its(iC+1)+','+vPos.Card.AlcCode+','+fts(vPos.Quant)+')');
                    end;
                    inc(iC);
                  end;
                end;
              end;
  //            ShowMessageLogLocal('     - ���������� ����� '+its(iC)+' �������');
            end;
          end;
          if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:ReplyRestsShop_v2')) then
          begin  // ������� (2 �������)
            SFIXDATE:='';
            if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:ReplyRestsShop_v2').NodeByName('rst:RestsDate')) then
              SFIXDATE:=nodeRoot.NodeByName('ns:Document').NodeByName('ns:ReplyRestsShop_v2').NodeByName('rst:RestsDate').Value;

            //��������� ���������
            quS.Active:=False;
            quS.SQL.Clear;
            quS.SQL.Add('');
            quS.SQL.Add('INSERT INTO [dbo].[ADOCSINV_HD] ([FSRARID],[IDATE],[ID],[STYPE],[FIXDATE],[IDREPLY])');
            quS.SQL.Add('VALUES ('''+RARID+'''');
            quS.SQL.Add('           ,'+its(iDateInv));
            quS.SQL.Add('           ,'+its(IDH));
            quS.SQL.Add('           ,''SHOP ( 2-reg )''');
            quS.SQL.Add('           ,'''+SFIXDATE+'''');
            quS.SQL.Add('           ,'+its(IdRec)+')');
            quS.ExecSQL;

            if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:ReplyRestsShop_v2').NodeByName('rst:Products')) then //������������
            begin
              nodeSpec:=nodeRoot.NodeByName('ns:Document').NodeByName('ns:ReplyRestsShop_v2').NodeByName('rst:Products');

  //            ShowMessageLogLocal('  - ��������� ������������ ( '+its(nodeSpec.NodeCount)+' ������� )');

              for i:=0 to nodeSpec.NodeCount-1 do
              begin
                nodePos:=nodeSpec[i];

  //              if i mod 100 = 0 then begin ShowMessageLogLocal('     - ���������� '+its(iC)+' �������'); delay(10); end;

                ClearPos;

                if Pos('rst:ShopPosition',nodePos.Name)>0 then  //��� �������
                begin
                  if Assigned(nodePos.NodeByName('rst:Quantity')) then vPos.Quant:=stf(nodePos.NodeByName('rst:Quantity').Value);
  //                    if Assigned(nodePos.NodeByName('rst:InformARegId')) then vPos.info_a:=nodePos.NodeByName('rst:InformARegId').Value;
  //                    if Assigned(nodePos.NodeByName('rst:InformBRegId')) then vPos.info_b:=nodePos.NodeByName('rst:InformBRegId').Value;
                  if Assigned(nodePos.NodeByName('rst:Product')) then
                    if Assigned(nodePos.NodeByName('rst:Product').NodeByName('pref:AlcCode')) then vPos.Card.AlcCode:=nodePos.NodeByName('rst:Product').NodeByName('pref:AlcCode').Value;

                  if vPos.Card.AlcCode>'' then  //������� � �������� ���������� - ��������
                  begin
                    try
                      quS.Active:=False;
                      quS.SQL.Clear;
                      quS.SQL.Add('');
                      quS.SQL.Add('INSERT INTO [dbo].[ADOCSINV_SP] ([IDH],[ID],[CAP],[QUANTR],[QUANTF])');
                      quS.SQL.Add('     VALUES');
                      quS.SQL.Add('           ('+its(IDH));
                      quS.SQL.Add('           ,'+its(iC+1));
                      quS.SQL.Add('           ,'''+vPos.Card.AlcCode+'''');
                      quS.SQL.Add('           ,'+fts(vPos.Quant));
                      quS.SQL.Add('           ,0)');
                      quS.ExecSQL;
                    except
  //                    ShowMessageLogLocal('     ������ ���������� ('+its(iC+1)+','+vPos.Card.AlcCode+','+fts(vPos.Quant)+')');
                    end;
                    inc(iC);
                  end;
                end;
              end;
  //            ShowMessageLogLocal('     - ���������� ����� '+its(iC)+' �������');
            end;
          end;
        end;
      end;
      Result:=True;
    finally
      quSelD.Active:=False;
      quSelD.Free;
      quS.Free;
      Xml.Free;
    end;
  end;
end;

function prDecodeAct(xmlstr:string;IdRec:integer; FDConn: TFDConnection):Boolean;
var
  Xml: TNativeXml;
  nodeRoot: TXmlNode;
  sIdDoc_B:string;
  sResult:string;
  sComment:string;
  k:INteger;
  quS,quSelOutHD: TFDQuery;

begin
  Result:=False;

//  with dmR do
  if Pos('ns:WayBillAct',xmlstr)>0 then
  begin
    try
      quS := TFDQuery.Create(nil);
      quS.Connection := FDConn;
      quS.ResourceOptions.SilentMode:=True;     //�� ���������� Wait cursor

      quSelOutHD := TFDQuery.Create(nil);
      quSelOutHD.Connection := FDConn;
      quSelOutHD.ResourceOptions.SilentMode:=True;     //�� ���������� Wait cursor

      Xml:= TNativeXml.Create(nil);
      Xml.XmlFormat := xfReadable;
      while Pos('''',xmlstr)>0 do xmlstr[Pos('''',xmlstr)]:=' ';

      Xml.ReadFromString(xmlstr);

      nodeRoot:=Xml.Root;
      if Assigned(nodeRoot.NodeByName('ns:Document')) then
      begin
        if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:WayBillAct')) then
        begin
          sIdDoc_B:='';

          if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:WayBillAct').NodeByName('wa:Header').NodeByName('wa:WBRegId')) then
            sIdDoc_B:=nodeRoot.NodeByName('ns:Document').NodeByName('ns:WayBillAct').NodeByName('wa:Header').NodeByName('wa:WBRegId').Value;

          if sIdDoc_B>'' then
          begin
            if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:WayBillAct').NodeByName('wa:Header').NodeByName('wa:IsAccept')) then
               sResult:=nodeRoot.NodeByName('ns:Document').NodeByName('ns:WayBillAct').NodeByName('wa:Header').NodeByName('wa:IsAccept').Value;
            if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:WayBillAct').NodeByName('wa:Header').NodeByName('wa:Note')) then
               sComment:=nodeRoot.NodeByName('ns:Document').NodeByName('ns:WayBillAct').NodeByName('wa:Header').NodeByName('wa:Note').Value;

            //����� ������� �� ���������
            quSelOutHD.Active:=False;
            quSelOutHD.SQL.Clear;
            quSelOutHD.SQL.Add('');
            quSelOutHD.SQL.Add('SELECT TOP 1 [FSRARID],[IDATE],[SID] FROM dbo.ADOCSOUT_HD');
            quSelOutHD.SQL.Add('where [WBREGID] = '''+sIdDoc_B+'''');
            quSelOutHD.SQL.Add('and [FSRARID] = '''+CommonSet.FSRAR_ID+'''');
            quSelOutHD.Active:=True;
            if quSelOutHD.RecordCount>0 then
            begin
              WriteHistoryOut(quSelOutHD.FieldByName('FSRARID').AsString,quSelOutHD.FieldByName('SID').AsString,quSelOutHD.FieldByName('IDATE').AsInteger,'������� Ticket. (ID '+its(IdRec)+')');
              WriteHistoryOut(quSelOutHD.FieldByName('FSRARID').AsString,quSelOutHD.FieldByName('SID').AsString,quSelOutHD.FieldByName('IDATE').AsInteger,'   '+sResult+' '+sComment);

              if sResult='Accepted' then  k:=1 else k:=-1;

              quS.Active:=False;
              quS.SQL.Clear;
              quS.SQL.Add('');
              quS.SQL.Add('UPDATE dbo.ADOCSOUT_HD');
              quS.SQL.Add('SET TICK2 = '+its(IdRec*k));
              quS.SQL.Add('where WBREGID = '''+sIdDoc_B+'''');
              quS.SQL.Add('and [FSRARID] = '''+quSelOutHD.FieldByName('FSRARID').AsString+'''');
              quS.ExecSQL;
            end;
            quSelOutHD.Active:=False;

          end;
        end;
      end;

      Result:=True;
    finally
      quS.Free;
      quSelOutHD.Free;
      Xml.Free;
    end;
  end;
end;

function prDecodeTTN(xmlstr:string;IdRec:integer; FDConn: TFDConnection; RARID:String):Boolean;

type

     TCli = record
              ClientRegId,FullName,ShortName,INN,KPP,Country,RegionCode,description:string;
            end;

     TDoc = record
              sIDH:string; //ID ���������
              NUMBER,sDate,sType,sUnitType,sShippingDate:string;
              CliFrom:TCli;
              CliTo:TCli; //��� ������ ����
            end;

     TCard = record
               FullName,AlcCode:string;
               Capacity,AlcVolume:Real;
               ProductVCode:Integer;
               Prod:TCli;
               Impr:TCli;
             end;

     TPos = record
              Num:Integer;
              Quant,Price:Real;
              Card:TCard;
              info_a,info_b,party:string;
              sNum:string;
            end;

var
  Xml: TNativeXml;
  nodeRoot,nodeHead,nodeSpec,nodePos,nodeProd,nodeImp,nodeR: TXmlNode;
  vDoc:TDoc;
  vPos:TPos;
  I: integer;
  StrWk:string;
  iDate:Integer;
  a:TGuid;
  quS,quA: TFDQuery;

//  iD,iC:Integer;


  procedure ClearDoc;
  begin
    vDoc.NUMBER:=''; vDoc.sIDH:=''; vDoc.sDate:=''; vDoc.sType:=''; vDoc.sUnitType:=''; vDoc.sShippingDate:='';
    vDoc.CliTo.ClientRegId:='';
    vDoc.CliFrom.ClientRegId:=''; vDoc.CliFrom.INN:=''; vDoc.CliFrom.KPP:=''; vDoc.CliFrom.FullName:=''; vDoc.CliFrom.ShortName:=''; vDoc.CliFrom.Country:=''; vDoc.CliFrom.RegionCode:=''; vDoc.CliFrom.description:='';
  end;


  procedure ClearPos;
  begin
    vPos.Num:=0;
    vPos.Quant:=0; vPos.Price:=0;
    vPos.Card.FullName:=''; vPos.Card.AlcCode:=''; vPos.Card.Capacity:=0; vPos.Card.AlcVolume:=0; vPos.Card.ProductVCode:=0; vPos.info_a:=''; vPos.info_b:=''; vPos.party:='';
    vPos.Card.Prod.ClientRegId:=''; vPos.Card.Prod.FullName:=''; vPos.Card.Prod.ShortName:=' '; vPos.Card.Prod.INN:=' '; vPos.Card.Prod.INN:=' '; vPos.Card.Prod.KPP:=' '; vPos.Card.Prod.Country:=' '; vPos.Card.Prod.RegionCode:=' '; vPos.Card.Prod.description:=' ';
    vPos.Card.Impr.ClientRegId:=''; vPos.Card.Impr.FullName:=''; vPos.Card.Impr.ShortName:=' '; vPos.Card.Impr.INN:=' '; vPos.Card.Impr.INN:=' '; vPos.Card.Impr.KPP:=' '; vPos.Card.Impr.Country:=' '; vPos.Card.Impr.RegionCode:=' '; vPos.Card.Impr.description:=' ';
    vPos.sNum:='';
  end;


begin
  Result:=False;
//  with dmR do
  if (Pos('WayBill',xmlstr)>0)or(Pos('WAYBILL',xmlstr)>0) then
  begin
    try
      quS := TFDQuery.Create(nil);
      quS.Connection := FDConn;
      quS.ResourceOptions.SilentMode:=True;     //�� ���������� Wait cursor

      quA := TFDQuery.Create(nil);
      quA.Connection := FDConn;
      quA.ResourceOptions.SilentMode:=True;     //�� ���������� Wait cursor

      Xml:= TNativeXml.Create(nil);
      Xml.XmlFormat := xfReadable;
      while Pos('''',xmlstr)>0 do xmlstr[Pos('''',xmlstr)]:='"';

      Xml.ReadFromString(xmlstr);

      ClearDoc;
//      iD:=0;

//      nodeR:=Xml.FindFirst;

{
      nodeR := Xml.RootNodes. .FindFirst;
      while assigned(nodeR) do
      begin
        if Assigned(nodeR.NodeByName('ns:Documents')) then
        begin
          nodeRoot:=nodeR.NodeByName('ns:Documents');
          Inc(iD);
        end;
        nodeR:= Xml.RootNodes.FindNext(nodeR);
      end;

      for iC:=0 to Xml.RootNodes.Count-1 do
      begin
        nodeR := Xml.RootNodes.Items[iC];
        if Assigned(nodeR.NodeByName('ns:Document')) then begin Inc(iD); end;
      end;
}
      nodeRoot:=Xml.Root;

      if Assigned(nodeRoot.NodeByName('ns:Document')) then
      begin
        if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:WayBill')) then
        begin
          vDoc.sIDH:='';
          if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeByName('wb:Identity')) then
            vDoc.sIDH:=Trim(nodeRoot.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeByName('wb:Identity').Value);

          if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeByName('wb:Header')) then //������ ���������
          begin
            nodeHead:=nodeRoot.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeByName('wb:Header');

            if Assigned(nodeHead.NodeByName('wb:NUMBER')) then vDoc.NUMBER:=nodeHead.NodeByName('wb:NUMBER').Value;
            if Assigned(nodeHead.NodeByName('wb:Date')) then vDoc.sDate:=nodeHead.NodeByName('wb:Date').Value;
            if Assigned(nodeHead.NodeByName('wb:Type')) then vDoc.sType:=nodeHead.NodeByName('wb:Type').Value;
            if Assigned(nodeHead.NodeByName('wb:UnitType')) then vDoc.sUnitType:=nodeHead.NodeByName('wb:UnitType').Value;
            if Assigned(nodeHead.NodeByName('wb:ShippingDate')) then vDoc.sShippingDate:=nodeHead.NodeByName('wb:ShippingDate').Value;

            //���������

            if Assigned(nodeHead.NodeByName('wb:Shipper')) then
              if Assigned(nodeHead.NodeByName('wb:Shipper').NodeByName('oref:ClientRegId')) then  vDoc.CliFrom.ClientRegId:=nodeHead.NodeByName('wb:Shipper').NodeByName('oref:ClientRegId').Value;

            if Assigned(nodeHead.NodeByName('wb:Shipper')) then
              if Assigned(nodeHead.NodeByName('wb:Shipper').NodeByName('oref:INN')) then  vDoc.CliFrom.INN:=nodeHead.NodeByName('wb:Shipper').NodeByName('oref:INN').Value;

            if Assigned(nodeHead.NodeByName('wb:Shipper')) then
              if Assigned(nodeHead.NodeByName('wb:Shipper').NodeByName('oref:KPP')) then  vDoc.CliFrom.KPP:=nodeHead.NodeByName('wb:Shipper').NodeByName('oref:KPP').Value;

            if Assigned(nodeHead.NodeByName('wb:Shipper')) then
              if Assigned(nodeHead.NodeByName('wb:Shipper').NodeByName('oref:FullName')) then  vDoc.CliFrom.FullName:=nodeHead.NodeByName('wb:Shipper').NodeByName('oref:FullName').Value;

            if Assigned(nodeHead.NodeByName('wb:Shipper')) then
              if Assigned(nodeHead.NodeByName('wb:Shipper').NodeByName('oref:ShortName')) then  vDoc.CliFrom.ShortName:=nodeHead.NodeByName('wb:Shipper').NodeByName('oref:ShortName').Value;

            if Assigned(nodeHead.NodeByName('wb:Shipper')) then
              if Assigned(nodeHead.NodeByName('wb:Shipper').NodeByName('oref:address')) then
                if Assigned(nodeHead.NodeByName('wb:Shipper').NodeByName('oref:address').NodeByName('oref:Country')) then vDoc.CliFrom.Country:=nodeHead.NodeByName('wb:Shipper').NodeByName('oref:address').NodeByName('oref:Country').Value;

            if Assigned(nodeHead.NodeByName('wb:Shipper')) then
              if Assigned(nodeHead.NodeByName('wb:Shipper').NodeByName('oref:address')) then
                if Assigned(nodeHead.NodeByName('wb:Shipper').NodeByName('oref:address').NodeByName('oref:description')) then vDoc.CliFrom.description:=nodeHead.NodeByName('wb:Shipper').NodeByName('oref:address').NodeByName('oref:description').Value;

            if Assigned(nodeHead.NodeByName('wb:Shipper')) then
              if Assigned(nodeHead.NodeByName('wb:Shipper').NodeByName('oref:address')) then
                if Assigned(nodeHead.NodeByName('wb:Shipper').NodeByName('oref:address').NodeByName('oref:RegionCode')) then vDoc.CliFrom.RegionCode:=nodeHead.NodeByName('wb:Shipper').NodeByName('oref:address').NodeByName('oref:RegionCode').Value;

            if Assigned(nodeHead.NodeByName('wb:Consignee')) then
              if Assigned(nodeHead.NodeByName('wb:Consignee').NodeByName('oref:ClientRegId')) then  vDoc.CliTo.ClientRegId:=nodeHead.NodeByName('wb:Consignee').NodeByName('oref:ClientRegId').Value;


            if trim(vDoc.CliTo.ClientRegId)=RARID then  //�������� ������������ ��� - ������������ �������
            begin
              // �������� �����������

              if Trim(vDoc.CliFrom.ClientRegId)>'' then
              begin
                quS.Active:=False;
                quS.SQL.Clear;
                quS.SQL.Add('');
                quS.SQL.Add('DECLARE @CLIID varchar(50) = '''+Trim(vDoc.CliFrom.ClientRegId)+'''');
                quS.SQL.Add('DECLARE @CLIINN varchar(20) = '''+vDoc.CliFrom.INN+'''');
                quS.SQL.Add('DECLARE @CLIKPP varchar(20) = '''+vDoc.CliFrom.KPP+'''');
                quS.SQL.Add('DECLARE @FULLNAME varchar(max) = '''+vDoc.CliFrom.FullName+'''');
                quS.SQL.Add('DECLARE @NAME varchar(max) = '''+vDoc.CliFrom.ShortName+'''');
                quS.SQL.Add('DECLARE @COUNTRY varchar(10) = '''+vDoc.CliFrom.Country+'''');
                quS.SQL.Add('DECLARE @REGCODE varchar(10) = '''+vDoc.CliFrom.RegionCode+'''');
                quS.SQL.Add('DECLARE @ADDR varchar(max) = '''+vDoc.CliFrom.description+'''');
                quS.SQL.Add('EXECUTE [dbo].[prAddCli] @CLIID,@CLIINN,@CLIKPP,@FULLNAME,@NAME,@COUNTRY,@REGCODE,@ADDR');
                quS.ExecSQL;
              end;

              //�������� ��������� ���������

              StrWk:=vDoc.sDate;
              while Pos('-',StrWk)>0 do Delete(StrWk,Pos('-',StrWk),1);

              StrWk:=Copy(StrWk,7,2)+'.'+Copy(StrWk,5,2)+'.'+Copy(StrWk,1,4);

              IDate:=Trunc(StrToDateTimeDef(StrWk,date));

              if Length(vDoc.sIDH)<3 then
                if vDoc.NUMBER>'' then vDoc.sIDH:=vDoc.NUMBER
                else
                begin
                  CreateGUID(a);
                  vDoc.sIDH:=GUIDTostring(a);
                end;

              quS.Active:=False;
              quS.SQL.Clear;
              quS.SQL.Add('');
              quS.SQL.Add('DECLARE @FSRARID varchar(50) = '''+Trim(RARID)+'''');
              quS.SQL.Add('DECLARE @IDATE int = '+IntToStr(iDate));
              quS.SQL.Add('DECLARE @SID varchar(50) = '''+Trim(vDoc.sIDH)+'''');
              quS.SQL.Add('DECLARE @NUMBER varchar(100) = '''+Trim(vDoc.NUMBER)+'''');
              quS.SQL.Add('DECLARE @SDATE varchar(20) = '''+Trim(vDoc.sDate)+'''');
              quS.SQL.Add('DECLARE @STYPE varchar(20) = '''+Trim(vDoc.sType)+'''');
              quS.SQL.Add('DECLARE @UNITTYPE varchar(20) = '''+Trim(vDoc.sUnitType)+'''');
              quS.SQL.Add('DECLARE @SHIPDATE varchar(20) = '''+Trim(vDoc.sShippingDate)+'''');
              quS.SQL.Add('DECLARE @CLIFROM varchar(50) = '''+Trim(vDoc.CliFrom.ClientRegId)+'''');
              quS.SQL.Add('EXECUTE [dbo].[prAddDocInHead] @FSRARID,@IDATE,@SID,@NUMBER,@SDATE,@STYPE,@UNITTYPE,@SHIPDATE,@CLIFROM');
              quS.ExecSQL;

              WriteHistoryInPrivate(RARID,vDoc.sIDH,iDate,'������ ��������� ���������. (ID '+its(IdRec)+')',FDConn);

              if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeByName('wb:Content')) then //������������
              begin
                nodeSpec:=nodeRoot.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeByName('wb:Content');

                for i:=0 to nodeSpec.NodeCount-1 do
                begin
                  nodePos:=nodeSpec[i];

                  ClearPos;

                  if Pos('wb:Position',nodePos.Name)>0 then  //��� ������� ���������
                  begin
                    if Assigned(nodePos.NodeByName('wb:Identity')) then vPos.Num:=StrToIntDef(nodePos.NodeByName('wb:Identity').Value,0);
                    if Assigned(nodePos.NodeByName('wb:Identity')) then vPos.sNum:=nodePos.NodeByName('wb:Identity').Value;

                    if Assigned(nodePos.NodeByName('wb:Quantity')) then vPos.Quant:=stf(nodePos.NodeByName('wb:Quantity').Value);
                    if Assigned(nodePos.NodeByName('wb:Price')) then vPos.Price:=stf(nodePos.NodeByName('wb:Price').Value);
                    if Assigned(nodePos.NodeByName('wb:Party')) then vPos.party:=nodePos.NodeByName('wb:Party').Value;

                    if Assigned(nodePos.NodeByName('wb:InformA')) then
                      if Assigned(nodePos.NodeByName('wb:InformA').NodeByName('pref:RegId')) then vPos.info_a:=nodePos.NodeByName('wb:InformA').NodeByName('pref:RegId').Value;

                    if Assigned(nodePos.NodeByName('wb:InformB')) then
                      if Assigned(nodePos.NodeByName('wb:InformB').NodeByName('pref:InformBItem')) then
                        if Assigned(nodePos.NodeByName('wb:InformB').NodeByName('pref:InformBItem').NodeByName('pref:BRegId')) then
                          vPos.info_b:=nodePos.NodeByName('wb:InformB').NodeByName('pref:InformBItem').NodeByName('pref:BRegId').Value;

                    if Assigned(nodePos.NodeByName('wb:Product')) then
                    begin
                      if Assigned(nodePos.NodeByName('wb:Product').NodeByName('pref:FullName')) then vPos.Card.FullName:=nodePos.NodeByName('wb:Product').NodeByName('pref:FullName').Value;
                      if Assigned(nodePos.NodeByName('wb:Product').NodeByName('pref:AlcCode')) then vPos.Card.AlcCode:=nodePos.NodeByName('wb:Product').NodeByName('pref:AlcCode').Value;
                      if Assigned(nodePos.NodeByName('wb:Product').NodeByName('pref:Capacity')) then vPos.Card.Capacity:=stf(nodePos.NodeByName('wb:Product').NodeByName('pref:Capacity').Value);
                      if Assigned(nodePos.NodeByName('wb:Product').NodeByName('pref:AlcVolume')) then vPos.Card.AlcVolume:=stf(nodePos.NodeByName('wb:Product').NodeByName('pref:AlcVolume').Value);
                      if Assigned(nodePos.NodeByName('wb:Product').NodeByName('pref:ProductVCode')) then vPos.Card.ProductVCode:=StrToIntDef(nodePos.NodeByName('wb:Product').NodeByName('pref:ProductVCode').Value,0);

                      {
                      <wb:Position>
                                <wb:Identity>1</wb:Identity>
                                <wb:Product>
                                  <pref:Type>��</pref:Type>
                                  <pref:FullName>���� "��������� ������������" ������� ��������������� ���������� ������� 0,5 �</pref:FullName>
                                  <pref:AlcCode>0035543000001238278</pref:AlcCode>
                                  <pref:Capacity>0.5000</pref:Capacity>
                                  <pref:AlcVolume>5.400</pref:AlcVolume>
                                  <pref:ProductVCode>500</pref:ProductVCode>
                                  <pref:Producer>
                                    <oref:INN>5020037784</oref:INN>
                                    <oref:KPP>550702001</oref:KPP>
                                    <oref:ClientRegId>030000000008</oref:ClientRegId>
                                    <oref:FullName>����������� �������� "��� �����"</oref:FullName>
                                    <oref:ShortName>�� "��� �����"</oref:ShortName>
                                    <oref:address>
                                      <oref:Country>643</oref:Country>
                                      <oref:RegionCode>55</oref:RegionCode>
                                      <oref:description>643,644073,55,,���� �,,�.�.������� ��,2,,</oref:description>
                                    </oref:address>
                                  </pref:Producer>
                                </wb:Product>
                                <wb:Quantity>40</wb:Quantity>
                                <wb:Price>43.5400</wb:Price>
                                <wb:Party>FB-000000038877201</wb:Party>
                                <wb:InformA>
                                  <pref:RegId>FA-000000001859406</pref:RegId>
                                </wb:InformA>
                                <wb:InformB>
                                  <pref:InformBItem>
                                    <pref:BRegId>FB-000000038877201</pref:BRegId>
                                  </pref:InformBItem>
                                </wb:InformB>
                              </wb:Position>
                      }


                      if Assigned(nodePos.NodeByName('wb:Product').NodeByName('pref:Producer')) then
                      begin
                        nodeProd:=nodePos.NodeByName('wb:Product').NodeByName('pref:Producer');
                        if Assigned(nodeProd.NodeByName('oref:INN')) then  vPos.Card.Prod.INN:=nodeProd.NodeByName('oref:INN').Value;
                        if Assigned(nodeProd.NodeByName('oref:KPP')) then  vPos.Card.Prod.KPP:=nodeProd.NodeByName('oref:KPP').Value;
                        if Assigned(nodeProd.NodeByName('oref:ClientRegId')) then  vPos.Card.Prod.ClientRegId:=nodeProd.NodeByName('oref:ClientRegId').Value;
                        if Assigned(nodeProd.NodeByName('oref:FullName')) then  vPos.Card.Prod.FullName:=nodeProd.NodeByName('oref:FullName').Value;
                        if Assigned(nodeProd.NodeByName('oref:ShortName')) then  vPos.Card.Prod.ShortName:=nodeProd.NodeByName('oref:ShortName').Value;

                        if Assigned(nodeProd.NodeByName('oref:address').NodeByName('oref:Country')) then  vPos.Card.Prod.Country:=nodeProd.NodeByName('oref:address').NodeByName('oref:Country').Value;
                        if Assigned(nodeProd.NodeByName('oref:address').NodeByName('oref:RegionCode')) then  vPos.Card.Prod.RegionCode:=nodeProd.NodeByName('oref:address').NodeByName('oref:RegionCode').Value;
                        if Assigned(nodeProd.NodeByName('oref:address').NodeByName('oref:description')) then  vPos.Card.Prod.description:=nodeProd.NodeByName('oref:address').NodeByName('oref:description').Value;

                        if Trim(vPos.Card.Prod.ClientRegId)>'' then
                        begin
                          quS.Active:=False;
                          quS.SQL.Clear;
                          quS.SQL.Add('');
                          quS.SQL.Add('DECLARE @PRODID varchar(50) = '''+Trim(vPos.Card.Prod.ClientRegId)+'''');
                          quS.SQL.Add('DECLARE @PRODINN varchar(20) = '''+vPos.Card.Prod.INN+'''');
                          quS.SQL.Add('DECLARE @PRODKPP varchar(20) = '''+vPos.Card.Prod.KPP+'''');
                          quS.SQL.Add('DECLARE @FULLNAME varchar(max) = '''+vPos.Card.Prod.FullName+'''');
                          quS.SQL.Add('DECLARE @NAME varchar(max) = '''+vPos.Card.Prod.ShortName+'''');
                          quS.SQL.Add('DECLARE @COUNTRY varchar(10) = '''+vPos.Card.Prod.Country+'''');
                          quS.SQL.Add('DECLARE @REGCODE varchar(10) = '''+vPos.Card.Prod.RegionCode+'''');
                          quS.SQL.Add('DECLARE @ADDR varchar(max) = '''+vPos.Card.Prod.description+'''');
                          quS.SQL.Add('EXECUTE [dbo].[prAddProd] @PRODID,@PRODINN,@PRODKPP,@FULLNAME,@NAME,@COUNTRY,@REGCODE,@ADDR');
                          quS.ExecSQL;
                        end;
                      end;
                      if Assigned(nodePos.NodeByName('wb:Product').NodeByName('pref:Importer')) then
                      begin
                        nodeImp:=nodePos.NodeByName('wb:Product').NodeByName('pref:Importer');

                        if Assigned(nodeImp.NodeByName('oref:INN')) then  vPos.Card.Prod.INN:=nodeImp.NodeByName('oref:INN').Value;
                        if Assigned(nodeImp.NodeByName('oref:KPP')) then  vPos.Card.Prod.KPP:=nodeImp.NodeByName('oref:KPP').Value;
                        if Assigned(nodeImp.NodeByName('oref:ClientRegId')) then  vPos.Card.Impr.ClientRegId:=nodeImp.NodeByName('oref:ClientRegId').Value;
                        if Assigned(nodeImp.NodeByName('oref:FullName')) then  vPos.Card.Impr.FullName:=nodeImp.NodeByName('oref:FullName').Value;
                        if Assigned(nodeImp.NodeByName('oref:ShortName')) then  vPos.Card.Impr.ShortName:=nodeImp.NodeByName('oref:ShortName').Value;

                        if Assigned(nodeImp.NodeByName('oref:address').NodeByName('oref:Country')) then  vPos.Card.Impr.Country:=nodeImp.NodeByName('oref:address').NodeByName('oref:Country').Value;
                        if Assigned(nodeImp.NodeByName('oref:address').NodeByName('oref:RegionCode')) then  vPos.Card.Impr.RegionCode:=nodeImp.NodeByName('oref:address').NodeByName('oref:RegionCode').Value;
                        if Assigned(nodeImp.NodeByName('oref:address').NodeByName('oref:description')) then  vPos.Card.Impr.description:=nodeImp.NodeByName('oref:address').NodeByName('oref:description').Value;

                        if Trim(vPos.Card.Impr.ClientRegId)>'' then
                        begin
                          quS.Active:=False;
                          quS.SQL.Clear;
                          quS.SQL.Add('');
                          quS.SQL.Add('DECLARE @IMPORTERID varchar(50) = '''+Trim(vPos.Card.Impr.ClientRegId)+'''');
                          quS.SQL.Add('DECLARE @IMPORTERINN varchar(20) = '''+vPos.Card.Impr.INN+'''');
                          quS.SQL.Add('DECLARE @IMPORTERKPP varchar(20) = '''+vPos.Card.Impr.KPP+'''');
                          quS.SQL.Add('DECLARE @FULLNAME varchar(max) = '''+vPos.Card.Impr.FullName+'''');
                          quS.SQL.Add('DECLARE @NAME varchar(max) = '''+vPos.Card.Impr.ShortName+'''');
                          quS.SQL.Add('DECLARE @COUNTRY varchar(10) = '''+vPos.Card.Impr.Country+'''');
                          quS.SQL.Add('DECLARE @REGCODE varchar(10) = '''+vPos.Card.Impr.RegionCode+'''');
                          quS.SQL.Add('DECLARE @ADDR varchar(max) = '''+vPos.Card.Impr.description+'''');
                          quS.SQL.Add('EXECUTE [dbo].[prAddImporter] @IMPORTERID,@IMPORTERINN,@IMPORTERKPP,@FULLNAME,@NAME,@COUNTRY,@REGCODE,@ADDR');
                          quS.ExecSQL;
                        end;
                      end;

                      if Trim(vPos.Card.AlcCode)>'' then
                      begin
                        quS.Active:=False;
                        quS.SQL.Clear;
                        quS.SQL.Add('');
                        quS.SQL.Add('DECLARE @KAP varchar(50) = '''+Trim(vPos.Card.AlcCode)+'''');
                        quS.SQL.Add('DECLARE @NAME varchar(max) = '''+vPos.Card.FullName+'''');
                        quS.SQL.Add('DECLARE @VOL real = '+fts(vPos.Card.Capacity));
                        quS.SQL.Add('DECLARE @KREP real = '+fts(vPos.Card.AlcVolume));
                        quS.SQL.Add('DECLARE @AVID int = '+its(vPos.Card.ProductVCode));
                        quS.SQL.Add('DECLARE @PROID varchar(50) = '''+Trim(vPos.Card.Prod.ClientRegId)+'''');
                        quS.SQL.Add('DECLARE @IMPID varchar(50) = '''+Trim(vPos.Card.Impr.ClientRegId)+'''');
                        quS.SQL.Add('EXECUTE [dbo].[prAddCard] @KAP,@NAME,@VOL,@KREP,@AVID,@PROID,@IMPID');
                        quS.ExecSQL;


                        //����� ���� ������������
                        quA.Active:=False;
                        quA.SQL.Clear;
                        quA.SQL.Add('');
                        quA.SQL.Add('DECLARE @FSRARID varchar(50) = '''+Trim(RARID)+'''');
                        quA.SQL.Add('DECLARE @IDATE int = '+its(iDate));
                        quA.SQL.Add('DECLARE @SID varchar(50) = '''+Trim(vDoc.sIDH)+'''');
                        quA.SQL.Add('DECLARE @NUM int= '+its(vPos.Num));
                        quA.SQL.Add('DECLARE @ALCCODE varchar(50) = '''+Trim(vPos.Card.AlcCode)+'''');
                        quA.SQL.Add('DECLARE @QUANT float = '+fts(vPos.Quant));
                        quA.SQL.Add('DECLARE @PRICE float = '+fts(vPos.Price));
                        quA.SQL.Add('DECLARE @INFO_A varchar(50) = '''+Trim(vPos.info_a)+'''');
                        quA.SQL.Add('DECLARE @INFO_B varchar(50) = '''+Trim(vPos.info_b)+'''');
                        quA.SQL.Add('DECLARE @PARTY varchar(50) = '''+Trim(vPos.party)+'''');
                        quA.SQL.Add('DECLARE @SNUM varchar(50) = '''+Trim(vPos.sNum)+'''');
                        quA.SQL.Add('EXECUTE [dbo].[prAddDocInSpec02] @FSRARID,@IDATE,@SID,@NUM,@ALCCODE,@QUANT,@PRICE,@INFO_A,@INFO_B,@PARTY,@SNUM');
                        quA.ExecSQL;

                      end;
                    end;
                  end;
                end;
              end;
            end; //�������� ������������ ��� - ������������ �������
          end;
        end;
      end;

      Result:=True;
    finally
      quA.Free;
      quS.Free;
      Xml.Free;
    end;
  end;
end;


function prDecodeCards(xmlstr:string; FDConn: TFDConnection):Boolean;
var
  Xml: TNativeXml;
  nodeRoot,nodeCards,nodeCard: TXmlNode;
  I: integer;
  NameC,AlcCode:string;
  Capacity,AlcVolume:Real;
  ProductVCode:Integer;
  ProducerId,ProducerINN,ProducerKPP,ProducerFullName,ProducerName,ProducerAdrCountry,ProducerAdrRegCode,ProducerAdr,ProducerType:string;
  ImporterId,ImporterINN,ImporterKPP,ImporterFullName,ImporterName,ImporterAdrCountry,ImporterAdrRegCode,ImporterAdr:string;
  quS: TFDQuery;

begin
  Result:=False;

//  with dmR do
  if Pos('ReplyAP',xmlstr)>0 then
  begin
    try
      quS := TFDQuery.Create(nil);
      quS.Connection := FDConn;
      quS.ResourceOptions.SilentMode:=True;     //�� ���������� Wait cursor

      Xml:= TNativeXml.Create(nil);
      Xml.XmlFormat := xfReadable;
      while Pos('''',xmlstr)>0 do xmlstr[Pos('''',xmlstr)]:=' ';

      Xml.ReadFromString(xmlstr);

      nodeRoot:=Xml.Root;
      if Assigned(nodeRoot.NodeByName('ns:Document')) then
      if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:ReplyAP_v2')) then
      if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:ReplyAP_v2').NodeByName('rap:Products')) then
      begin
        nodeCards:=nodeRoot.NodeByName('ns:Document').NodeByName('ns:ReplyAP_v2').NodeByName('rap:Products');

        for i:=0 to nodeCards.NodeCount-1 do
        begin
          nodeCard:=nodeCards.Nodes[i];

          if nodeCard.Name='rap:Product' then
          begin
            AlcCode:=''; NameC:=''; Capacity:=0; AlcVolume:=0; ProductVCode:=0;
            ProducerId:=''; ProducerINN:=''; ProducerKPP:=''; ProducerFullName:=''; ProducerName:=''; ProducerAdrCountry:=''; ProducerAdrRegCode:=''; ProducerAdr:='';
            ImporterId:=''; ImporterINN:=''; ImporterKPP:=''; ImporterFullName:=''; ImporterName:=''; ImporterAdrCountry:=''; ImporterAdrRegCode:=''; ImporterAdr:='';

            try
              if Assigned(nodeCard.NodeByName('pref:FullName')) then NameC:=nodeCard.NodeByName('pref:FullName').Value;
              if Assigned(nodeCard.NodeByName('pref:AlcCode')) then AlcCode:=nodeCard.NodeByName('pref:AlcCode').Value;
              if Assigned(nodeCard.NodeByName('pref:Capacity')) then Capacity:=stf(nodeCard.NodeByName('pref:Capacity').Value);
              if Assigned(nodeCard.NodeByName('pref:AlcVolume')) then AlcVolume:=stf(nodeCard.NodeByName('pref:AlcVolume').Value);
              if Assigned(nodeCard.NodeByName('pref:ProductVCode')) then ProductVCode:=StrToIntDef(nodeCard.NodeByName('pref:ProductVCode').Value,0);

              //�������������
              {
              <pref:Producer>
						<oref:UL xmlns:pref="http://fsrar.ru/WEGAIS/ProductRef_v2" xmlns:oref="http://fsrar.ru/WEGAIS/ClientRef_v2" xmlns:rap="http://fsrar.ru/WEGAIS/ReplyAP_v2">
							<oref:ClientRegId>010000002036</oref:ClientRegId>
							<oref:INN>4720027123</oref:INN>
							<oref:KPP>352501001</oref:KPP>
							<oref:FullName>�������� � ������������ ���������������� "������� �����"</oref:FullName>
							<oref:ShortName>��� "������� �����"</oref:ShortName>
							<oref:address>
								<oref:Country>643</oref:Country>
								<oref:RegionCode>35</oref:RegionCode>
								<oref:description>������,,����������� ����, 1 ����, ������ �6, ���. � 1 (S=639,0 ��.�)</oref:description>
							</oref:address>
						</oref:UL>
					</pref:Producer>

           <oref:FO xmlns:pref="http://fsrar.ru/WEGAIS/ProductRef_v2" xmlns:oref="http://fsrar.ru/WEGAIS/ClientRef_v2" xmlns:rap="http://fsrar.ru/WEGAIS/ReplyAP_v2">
							<oref:ClientRegId>050000017526</oref:ClientRegId>
							<oref:FullName>"�.�� ����� ��� �. ���"</oref:FullName>
							<oref:ShortName>�.�� ����� ��� �. ���</oref:ShortName>
							<oref:address>
								<oref:Country>380</oref:Country>
								<oref:description>14053 �������, ������</oref:description>
							</oref:address>
						</oref:FO>
          }

              ProducerType:='';

              if Assigned(nodeCard.NodeByName('pref:Producer')) then
              begin
                if Assigned(nodeCard.NodeByName('pref:Producer').NodeByName('oref:UL')) then ProducerType:='UL';
                if Assigned(nodeCard.NodeByName('pref:Producer').NodeByName('oref:FO')) then ProducerType:='FO';
                if Assigned(nodeCard.NodeByName('pref:Producer').NodeByName('oref:TS')) then ProducerType:='TS';
                if Assigned(nodeCard.NodeByName('pref:Producer').NodeByName('oref:FL')) then ProducerType:='FL';

                if ProducerType>'' then
                begin
                  if Assigned(nodeCard.NodeByName('pref:Producer').NodeByName('oref:'+ProducerType).NodeByName('oref:ClientRegId')) then
                    ProducerId:=nodeCard.NodeByName('pref:Producer').NodeByName('oref:'+ProducerType).NodeByName('oref:ClientRegId').Value;

                  if Assigned(nodeCard.NodeByName('pref:Producer').NodeByName('oref:'+ProducerType).NodeByName('oref:INN')) then
                    ProducerINN:=nodeCard.NodeByName('pref:Producer').NodeByName('oref:'+ProducerType).NodeByName('oref:INN').Value;

                  if Assigned(nodeCard.NodeByName('pref:Producer').NodeByName('oref:'+ProducerType).NodeByName('oref:KPP')) then
                    ProducerKPP:=nodeCard.NodeByName('pref:Producer').NodeByName('oref:'+ProducerType).NodeByName('oref:KPP').Value;

                  if Assigned(nodeCard.NodeByName('pref:Producer').NodeByName('oref:'+ProducerType).NodeByName('oref:FullName')) then
                    ProducerFullName:=nodeCard.NodeByName('pref:Producer').NodeByName('oref:'+ProducerType).NodeByName('oref:FullName').Value;

                  if Assigned(nodeCard.NodeByName('pref:Producer').NodeByName('oref:'+ProducerType).NodeByName('oref:ShortName')) then
                    ProducerName:=nodeCard.NodeByName('pref:Producer').NodeByName('oref:'+ProducerType).NodeByName('oref:ShortName').Value;

                  if Assigned(nodeCard.NodeByName('pref:Producer').NodeByName('oref:'+ProducerType).NodeByName('oref:address')) then
                    if Assigned(nodeCard.NodeByName('pref:Producer').NodeByName('oref:'+ProducerType).NodeByName('oref:address').NodeByName('oref:Country')) then
                      ProducerAdrCountry:=nodeCard.NodeByName('pref:Producer').NodeByName('oref:'+ProducerType).NodeByName('oref:address').NodeByName('oref:Country').Value;

                  if Assigned(nodeCard.NodeByName('pref:Producer').NodeByName('oref:'+ProducerType).NodeByName('oref:address')) then
                    if Assigned(nodeCard.NodeByName('pref:Producer').NodeByName('oref:'+ProducerType).NodeByName('oref:address').NodeByName('oref:RegionCode')) then
                      ProducerAdrRegCode:=nodeCard.NodeByName('pref:Producer').NodeByName('oref:'+ProducerType).NodeByName('oref:address').NodeByName('oref:RegionCode').Value;

                  if Assigned(nodeCard.NodeByName('pref:Producer').NodeByName('oref:'+ProducerType).NodeByName('oref:address')) then
                    if Assigned(nodeCard.NodeByName('pref:Producer').NodeByName('oref:'+ProducerType).NodeByName('oref:address').NodeByName('oref:description')) then
                      ProducerAdr:=nodeCard.NodeByName('pref:Producer').NodeByName('oref:'+ProducerType).NodeByName('oref:address').NodeByName('oref:description').Value;
                end;
              end;

              // ��������

              if Assigned(nodeCard.NodeByName('pref:Importer')) then
                if Assigned(nodeCard.NodeByName('pref:Importer').NodeByName('oref:ClientRegId')) then
                   ImporterId:=nodeCard.NodeByName('pref:Importer').NodeByName('oref:ClientRegId').Value;

              if Assigned(nodeCard.NodeByName('pref:Importer')) then
                if Assigned(nodeCard.NodeByName('pref:Importer').NodeByName('oref:INN')) then
                   ImporterINN:=nodeCard.NodeByName('pref:Importer').NodeByName('oref:INN').Value;

              if Assigned(nodeCard.NodeByName('pref:Importer')) then
                if Assigned(nodeCard.NodeByName('pref:Importer').NodeByName('oref:KPP')) then
                  ImporterKPP:=nodeCard.NodeByName('pref:Importer').NodeByName('oref:KPP').Value;

              if Assigned(nodeCard.NodeByName('pref:Importer')) then
                if Assigned(nodeCard.NodeByName('pref:Importer').NodeByName('oref:FullName')) then
                  ImporterFullName:=nodeCard.NodeByName('pref:Importer').NodeByName('oref:FullName').Value;

              if Assigned(nodeCard.NodeByName('pref:Importer')) then
                if Assigned(nodeCard.NodeByName('pref:Importer').NodeByName('oref:ShortName')) then
                 ImporterName:=nodeCard.NodeByName('pref:Importer').NodeByName('oref:ShortName').Value;

              if Assigned(nodeCard.NodeByName('pref:Importer')) then
                if Assigned(nodeCard.NodeByName('pref:Importer').NodeByName('oref:address')) then
                  if Assigned(nodeCard.NodeByName('pref:Importer').NodeByName('oref:address').NodeByName('oref:Country')) then
                    ImporterAdrCountry:=nodeCard.NodeByName('pref:Importer').NodeByName('oref:address').NodeByName('oref:Country').Value;

              if Assigned(nodeCard.NodeByName('pref:Importer')) then
                if Assigned(nodeCard.NodeByName('pref:Importer').NodeByName('oref:address')) then
                  if Assigned(nodeCard.NodeByName('pref:Importer').NodeByName('oref:address').NodeByName('oref:RegionCode')) then
                    ImporterAdrRegCode:=nodeCard.NodeByName('pref:Importer').NodeByName('oref:address').NodeByName('oref:RegionCode').Value;

              if Assigned(nodeCard.NodeByName('pref:Importer')) then
                if Assigned(nodeCard.NodeByName('pref:Importer').NodeByName('oref:address')) then
                  if Assigned(nodeCard.NodeByName('pref:Importer').NodeByName('oref:address').NodeByName('oref:description')) then
                    ImporterAdr:=nodeCard.NodeByName('pref:Importer').NodeByName('oref:address').NodeByName('oref:description').Value;

            except
            end;

            if Trim(AlcCode)>'' then
            begin
              quS.Active:=False;
              quS.SQL.Clear;
              quS.SQL.Add('');
              quS.SQL.Add('DECLARE @KAP varchar(50) = '''+Trim(AlcCode)+'''');
              quS.SQL.Add('DECLARE @NAME varchar(max) = '''+NameC+'''');
              quS.SQL.Add('DECLARE @VOL real = '+fts(Capacity));
              quS.SQL.Add('DECLARE @KREP real = '+fts(AlcVolume));
              quS.SQL.Add('DECLARE @AVID int = '+its(ProductVCode));
              quS.SQL.Add('DECLARE @PROID varchar(50) = '''+Trim(ProducerId)+'''');
              quS.SQL.Add('DECLARE @IMPID varchar(50) = '''+Trim(ImporterId)+'''');
              quS.SQL.Add('EXECUTE [dbo].[prAddCard] @KAP,@NAME,@VOL,@KREP,@AVID,@PROID,@IMPID');
              quS.ExecSQL;
            end;

            if (Trim(ProducerId)>'') and (ProducerType>'') then
            begin
              quS.Active:=False;
              quS.SQL.Clear;
              quS.SQL.Add('');
              quS.SQL.Add('DECLARE @PRODID varchar(50) = '''+Trim(ProducerId)+'''');
              quS.SQL.Add('DECLARE @PRODINN varchar(20) = '''+ProducerINN+'''');
              quS.SQL.Add('DECLARE @PRODKPP varchar(20) = '''+ProducerKPP+'''');
              quS.SQL.Add('DECLARE @FULLNAME varchar(max) = '''+ProducerFullName+'''');
              quS.SQL.Add('DECLARE @NAME varchar(max) = '''+ProducerName+'''');
              quS.SQL.Add('DECLARE @COUNTRY varchar(10) = '''+ProducerAdrCountry+'''');
              quS.SQL.Add('DECLARE @REGCODE varchar(10) = '''+ProducerAdrRegCode+'''');
              quS.SQL.Add('DECLARE @ADDR varchar(max) = '''+ProducerAdr+'''');
              quS.SQL.Add('DECLARE @CLITYPE varchar(5) = '''+ProducerType+'''');
              quS.SQL.Add('EXECUTE [dbo].[prAddProd_v2] @PRODID,@PRODINN,@PRODKPP,@FULLNAME,@NAME,@COUNTRY,@REGCODE,@ADDR,@CLITYPE');
              quS.ExecSQL;
            end;

            if Trim(ImporterId)>'' then
            begin
              quS.Active:=False;
              quS.SQL.Clear;
              quS.SQL.Add('');
              quS.SQL.Add('DECLARE @IMPORTERID varchar(50) = '''+Trim(ImporterId)+'''');
              quS.SQL.Add('DECLARE @IMPORTERINN varchar(20) = '''+ImporterINN+'''');
              quS.SQL.Add('DECLARE @IMPORTERKPP varchar(20) = '''+ImporterKPP+'''');
              quS.SQL.Add('DECLARE @FULLNAME varchar(max) = '''+ImporterFullName+'''');
              quS.SQL.Add('DECLARE @NAME varchar(max) = '''+ImporterName+'''');
              quS.SQL.Add('DECLARE @COUNTRY varchar(10) = '''+ImporterAdrCountry+'''');
              quS.SQL.Add('DECLARE @REGCODE varchar(10) = '''+ImporterAdrRegCode+'''');
              quS.SQL.Add('DECLARE @ADDR varchar(max) = '''+ImporterAdr+'''');
              quS.SQL.Add('EXECUTE [dbo].[prAddImporter] @IMPORTERID,@IMPORTERINN,@IMPORTERKPP,@FULLNAME,@NAME,@COUNTRY,@REGCODE,@ADDR');
              quS.ExecSQL;
            end;


{
DECLARE @IMPORTERID varchar(50)
DECLARE @IMPORTERINN varchar(20)
DECLARE @IMPORTERKPP varchar(20)
DECLARE @FULLNAME varchar(max)
DECLARE @NAME varchar(max)
DECLARE @COUNTRY varchar(10)
DECLARE @REGCODE varchar(10)
DECLARE @ADDR varchar(max)

EXECUTE [dbo].[prAddImporter] @IMPORTERID,@IMPORTERINN,@IMPORTERKPP,@FULLNAME,@NAME,@COUNTRY,@REGCODE,@ADDR

}

          end;
        end;
      end;
      Result:=True;
    finally
      Xml.Free;
      quS.Free;
    end;
  end;
end;

function prDecodeTicket(xmlstr:string;IdRec:integer; FDConn: TFDConnection; RARID:String):Boolean;
var
  Xml: TNativeXml;
  nodeRoot: TXmlNode;
  sIdDoc_B:string;
  iType, iTypeVoz:Integer;
  sType:string;
  sResult:string;
  sComment:string;
  k:INteger;
  sIdentity: string;
  ITYPEQU: integer;
  sFixDate:string;
  tcDocType, tcConclusion, tcComments, tcTransportId: string;
  IdToRar: Int64;
  quS,quSelInHD,quProc,quSelOutHD: TFDQuery;

begin
  Result:=False;

//  with dmR do
  if Pos('ns:Ticket',xmlstr)>0 then
  begin
    try
      quS := TFDQuery.Create(nil);
      quS.Connection := FDConn;
      quS.ResourceOptions.SilentMode:=True;     //�� ���������� Wait cursor

      quSelInHD := TFDQuery.Create(nil);
      quSelInHD.Connection := FDConn;
      quSelInHD.ResourceOptions.SilentMode:=True;     //�� ���������� Wait cursor

      quProc := TFDQuery.Create(nil);
      quProc.Connection := FDConn;
      quProc.ResourceOptions.SilentMode:=True;     //�� ���������� Wait cursor

      quSelOutHD := TFDQuery.Create(nil);
      quSelOutHD.Connection := FDConn;
      quSelOutHD.ResourceOptions.SilentMode:=True;     //�� ���������� Wait cursor

      Xml:= TNativeXml.Create(nil);
      Xml.XmlFormat := xfReadable;
      while Pos('''',xmlstr)>0 do xmlstr[Pos('''',xmlstr)]:=' ';

      Xml.ReadFromString(xmlstr);

      nodeRoot:=Xml.Root;
      if Assigned(nodeRoot.NodeByName('ns:Document')) then
      begin
        if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket')) then
        begin
          sIdDoc_B:='';
          if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:RegID')) then
            sIdDoc_B:=nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:RegID').Value;

          tcDocType:='';
          if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:DocType')) then
            tcDocType:=nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:DocType').Value;  //QueryBarcode

          if sIdDoc_B>'' then
          begin
            sType:='';
            iType:=0;
            iTypeVoz:=0;
            if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:DocType')) then sType:=UpperCase(nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:DocType').Value);
            if sType>'' then
            begin
              if sType='WAYBILLACT' then iType:=1;
              if sType='WAYBILL' then iType:=2;
              if sType='TRANSFERTOSHOP' then iType:=3;
              if sType='TRANSFERFROMSHOP' then iType:=5;
              if sType='ACTCHARGEONSHOP_V2' then iType:=4;
              if sType='ACTWRITEOFFSHOP_V2' then iType:=4;

              if iType in [1,2] then
              begin
                if iType=1 then
                begin
                  if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:Result')) then
                  begin
                    if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:Result').NodeByName('tc:Conclusion')) then
                       sResult:=nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:Result').NodeByName('tc:Conclusion').Value;
                    if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:Result').NodeByName('tc:Comments')) then
                       sComment:=nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:Result').NodeByName('tc:Comments').Value;
                  end;
                end;

                if iType=2 then
                begin
                  if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:OperationResult')) then
                  begin
                    iTypeVoz:=1;
                    if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:OperationResult').NodeByName('tc:OperationResult')) then
                       sResult:=nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:OperationResult').NodeByName('tc:OperationResult').Value;
                    if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:OperationResult').NodeByName('tc:OperationComment')) then
                       sComment:=nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:OperationResult').NodeByName('tc:OperationComment').Value;
                  end else   //��� ����� �� ���������� ���������
                  begin
                    iTypeVoz:=0;
                    if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:Result').NodeByName('tc:Conclusion')) then
                       sResult:=nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:Result').NodeByName('tc:Conclusion').Value;
                    if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:Result').NodeByName('tc:Comments')) then
                       sComment:=nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:Result').NodeByName('tc:Comments').Value;
                  end;
                end;

                //<--��������� �������������
                if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:Identity')) then
                  sIdentity:=nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:Identity').Value;
                //-->
                //<--��������� � ������ ���� ��������� ��������� �����
                ITYPEQU:=1;  //�� ��������� ������� ��� ��������
                if Length(sIdentity)>5 then begin
                  quS.Active:=False;
                  quS.SQL.Clear;
                  quS.SQL.Add('DECLARE @Identity VARCHAR(50)='''+sIdentity+'''');
                  quS.SQL.Add('SELECT 1 AS ITYPEQU FROM dbo.ADOCSIN_HD');
                  quS.SQL.Add('  WHERE RTRIM(LTRIM(SID))=@Identity');
                  quS.SQL.Add('UNION ALL');
                  quS.SQL.Add('SELECT 2 AS ITYPEQU FROM dbo.ADOCSOUT_HD');
                  quS.SQL.Add('  WHERE RTRIM(LTRIM(SID))=@Identity');
                  quS.Active:=True;
                  if quS.RecordCount>0 then
                    ITYPEQU:=quS.FieldByName('ITYPEQU').AsInteger
                  else
                    ITYPEQU:=0;
                  quS.Active:=False;
                end;
                //-->

                //� ����������� �� ���� ��������� ������� ���������� �� ������
                case ITYPEQU of
                  1: begin //�������
                          //����� ������� �� ��������
                          quSelInHD.Active:=False;
                          quSelInHD.SQL.Clear;
                          quSelInHD.SQL.Add('');
                          quSelInHD.SQL.Add('SELECT TOP 1 [FSRARID],[IDATE],[SID] FROM dbo.ADOCSIN_HD');
                          quSelInHD.SQL.Add('where [WBREGID] = '''+sIdDoc_B+'''');
                          quSelInHD.SQL.Add('and [FSRARID] = '''+RARID+'''');
                          quSelInHD.Active:=True;
                          if quSelInHD.RecordCount>0 then
                          begin
                            WriteHistoryInPrivate(quSelInHD.FieldByName('FSRARID').AsString,quSelInHD.FieldByName('SID').AsString,quSelInHD.FieldByName('IDATE').AsInteger,'������� Ticket (��� '+its(iType)+'). (ID '+its(IdRec)+')',FDConn);
                            WriteHistoryInPrivate(quSelInHD.FieldByName('FSRARID').AsString,quSelInHD.FieldByName('SID').AsString,quSelInHD.FieldByName('IDATE').AsInteger,'   '+sResult+' '+sComment,FDConn);

                            if sResult='Accepted' then  k:=1 else k:=-1;

                            if iType=1 then
                            begin
                              quS.Active:=False;
                              quS.SQL.Clear;
                              quS.SQL.Add('');
                              quS.SQL.Add('UPDATE dbo.ADOCSIN_HD');
                              quS.SQL.Add('SET TICK1 = '+its(IdRec*k));
                              quS.SQL.Add('where WBREGID = '''+sIdDoc_B+'''');
                              quS.SQL.Add('and [FSRARID] = '''+quSelInHD.FieldByName('FSRARID').AsString+'''');
                              quS.ExecSQL;
                            end;
                            if iType=2 then
                            begin
                              quS.Active:=False;
                              quS.SQL.Clear;
                              quS.SQL.Add('');
                              quS.SQL.Add('UPDATE dbo.ADOCSIN_HD');
                              quS.SQL.Add('SET TICK2 = '+its(IdRec*k));
                              quS.SQL.Add('where WBREGID = '''+sIdDoc_B+'''');
                              quS.SQL.Add('and [FSRARID] = '''+quSelInHD.FieldByName('FSRARID').AsString+'''');
                              quS.ExecSQL;
                            end;
                          end;
                          quSelInHD.Active:=False;
                     end;
                  2: begin //��������
                          //<--����� ��������� WBREGID, ���� �� ���������
                          quS.Active:=False;
                          quS.SQL.Clear;
                          quS.SQL.Add('DECLARE @Identity VARCHAR(50)='''+sIdentity+'''');
                          quS.SQL.Add('SELECT FSRARID, IDATE, SID, WBREGID FROM dbo.ADOCSOUT_HD');
                          quS.SQL.Add('  WHERE RTRIM(LTRIM(SID))=@Identity');
                          quS.Active:=True;
                          if quS.RecordCount>0 then
                            if quS.FieldByName('WBREGID').AsString='' then begin
                              //WBREGID �� ��������, ��������
                              quProc.Active:=False;
                              quProc.SQL.Clear;
                              quProc.SQL.Add('DECLARE @FSRARID VARCHAR(50)='''+quS.FieldByName('FSRARID').AsString+'''');
                              quProc.SQL.Add('DECLARE @IDATE INT='+quS.FieldByName('IDATE').AsString);
                              quProc.SQL.Add('DECLARE @SID VARCHAR(50)='''+quS.FieldByName('SID').AsString+'''');
                              quProc.SQL.Add('DECLARE @WBREGID VARCHAR(50)='''+sIdDoc_B+'''');
                              quProc.SQL.Add('UPDATE dbo.ADOCSOUT_HD');
                              quProc.SQL.Add('SET');
                              quProc.SQL.Add('  WBREGID = @WBREGID');
                              quProc.SQL.Add('WHERE');
                              quProc.SQL.Add('  FSRARID = @FSRARID');
                              quProc.SQL.Add('  AND IDATE = @IDATE');
                              quProc.SQL.Add('  AND SID = @SID');
                              quProc.ExecSQL;
                            end;
                          quS.Active:=False;
                          //-->

                          //����� ������� �� ���������
                          quSelOutHD.Active:=False;
                          quSelOutHD.SQL.Clear;
                          quSelOutHD.SQL.Add('');
                          quSelOutHD.SQL.Add('SELECT TOP 1 [FSRARID],[IDATE],[SID] FROM dbo.ADOCSOUT_HD');
                          quSelOutHD.SQL.Add('where [WBREGID] = '''+sIdDoc_B+'''');
                          quSelOutHD.SQL.Add('and [FSRARID] = '''+RARID+'''');
                          quSelOutHD.Active:=True;
                          if quSelOutHD.RecordCount>0 then
                          begin
                            WriteHistoryOutPrivate(quSelOutHD.FieldByName('FSRARID').AsString,quSelOutHD.FieldByName('SID').AsString,quSelOutHD.FieldByName('IDATE').AsInteger,'������� Ticket (��� '+its(iType)+'). (ID '+its(IdRec)+')',FDConn);
                            WriteHistoryOutPrivate(quSelOutHD.FieldByName('FSRARID').AsString,quSelOutHD.FieldByName('SID').AsString,quSelOutHD.FieldByName('IDATE').AsInteger,'   '+sResult+' '+sComment,FDConn);

                            if sResult='Accepted' then  k:=1 else k:=-1;

                            case iTypeVoz of
                              0: begin
                                   //�� ������������ �����
                                 end;
                              1: begin
                                    quS.Active:=False;
                                    quS.SQL.Clear;
                                    quS.SQL.Add('');
                                    quS.SQL.Add('UPDATE dbo.ADOCSOUT_HD');
                                    quS.SQL.Add('SET TICK1 = '+its(IdRec*k));
                                    quS.SQL.Add('where WBREGID = '''+sIdDoc_B+'''');
                                    quS.SQL.Add('and [FSRARID] = '''+quSelOutHD.FieldByName('FSRARID').AsString+'''');
                                    quS.ExecSQL;
                                 end;
                              2: begin
                                    quS.Active:=False;
                                    quS.SQL.Clear;
                                    quS.SQL.Add('');
                                    quS.SQL.Add('UPDATE dbo.ADOCSOUT_HD');
                                    quS.SQL.Add('SET TICK2 = '+its(IdRec*k));
                                    quS.SQL.Add('where WBREGID = '''+sIdDoc_B+'''');
                                    quS.SQL.Add('and [FSRARID] = '''+quSelOutHD.FieldByName('FSRARID').AsString+'''');
                                    quS.ExecSQL;
                                 end;
                            end;
                          end;
                          quSelOutHD.Active:=False;
                     end;
                end;

              end;

              if iType in [3,5] then   // ������ �� ����������� ����� ����������
              begin
                //<--��������� �������������
                sIdentity:='';

                if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:Identity')) then
                  sIdentity:=nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:Identity').Value;
                if sIdentity>'' then
                begin
                  if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:Result')) then  //�������� ������� ������.
                  begin
                    if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:Result').NodeByName('tc:Conclusion')) then
                       sResult:=nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:Result').NodeByName('tc:Conclusion').Value;
                    if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:Result').NodeByName('tc:Comments')) then
                       sComment:=nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:Result').NodeByName('tc:Comments').Value;
                    if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:Result').NodeByName('tc:ConclusionDate')) then
                       sFixDate:=nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:Result').NodeByName('tc:ConclusionDate').Value;

                    if sResult='Accepted' then  k:=1 else k:=-1;

                    quS.Active:=False;
                    quS.SQL.Clear;
                    quS.SQL.Add('');
                    quS.SQL.Add('UPDATE [dbo].[ADOCSVN_HD]');
                    quS.SQL.Add('SET [TICK1] = '+its(IdRec*k));
                    quS.SQL.Add(',[WBREGID] = '''+sIdDoc_B+'''');
                    quS.SQL.Add(',[FIXNUMBER] = '''+sIdDoc_B+'''');
                    quS.SQL.Add(',[FIXDATE] = '''+sFixDate+'''');
                    quS.SQL.Add('where [FSRARID] = '''+RARID+'''');
                    quS.SQL.Add('and [SID] = '''+sIdentity+'''');
                    quS.SQL.Add('and [IDATE] >= '+its(Trunc(dsfromrar((sFixDate)))-5));
                    quS.ExecSQL;

                  end;
                  if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:OperationResult')) then     //��� �������� ��������� � �������� ��� �26 �� 23.10.2016 00:00:00 ��������
                  begin
                    if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:OperationResult').NodeByName('tc:OperationResult')) then
                       sResult:=nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:OperationResult').NodeByName('tc:OperationResult').Value;
                    if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:OperationResult').NodeByName('tc:OperationComment')) then
                       sComment:=nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:OperationResult').NodeByName('tc:OperationComment').Value;
                    if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:OperationResult').NodeByName('tc:OperationDate')) then
                       sFixDate:=nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:OperationResult').NodeByName('tc:OperationDate').Value;

                    if sResult='Accepted' then  k:=1 else k:=-1;

                    quS.Active:=False;
                    quS.SQL.Clear;
                    quS.SQL.Add('');
                    quS.SQL.Add('UPDATE [dbo].[ADOCSVN_HD]');
                    quS.SQL.Add('SET [TICK2] = '+its(IdRec*k));
                    quS.SQL.Add(',[WBREGID] = '''+sIdDoc_B+'''');
                    quS.SQL.Add(',[FIXNUMBER] = '''+sIdDoc_B+'''');
                    quS.SQL.Add(',[FIXDATE] = '''+sFixDate+'''');
                    quS.SQL.Add('where [FSRARID] = '''+RARID+'''');
                    quS.SQL.Add('and [SID] = '''+sIdentity+'''');
                    quS.SQL.Add('and [IDATE] >= '+its(Trunc(dsfromrar((sFixDate)))-5));
                    quS.ExecSQL;
                  end;
                end;
              end;

              if iType in [4] then   // ������ �� ��������������
              begin
                //<--��������� �������������
                sIdentity:='';

                if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:Identity')) then
                  sIdentity:=nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:Identity').Value;
                if sIdentity>'' then
                begin
                  if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:Result')) then  //�������� ������� ������.
                  begin
                    if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:Result').NodeByName('tc:Conclusion')) then
                       sResult:=nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:Result').NodeByName('tc:Conclusion').Value;
                    if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:Result').NodeByName('tc:Comments')) then
                       sComment:=nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:Result').NodeByName('tc:Comments').Value;
                    if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:Result').NodeByName('tc:ConclusionDate')) then
                       sFixDate:=nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:Result').NodeByName('tc:ConclusionDate').Value;

                    if sResult='Accepted' then  k:=1 else k:=-1;

                    quS.Active:=False;
                    quS.SQL.Clear;
                    quS.SQL.Add('');
                    quS.SQL.Add('UPDATE [dbo].[ADOCSCORR_HD]');
                    quS.SQL.Add('SET [TICK1] = '+its(IdRec*k));
                    quS.SQL.Add(',[FIXNUMBER] = '''+sIdDoc_B+'''');
                    quS.SQL.Add(',[FIXDATE] = '''+sFixDate+'''');
                    quS.SQL.Add('where [FSRARID] = '''+RARID+'''');
                    quS.SQL.Add('and [SID] = '''+sIdentity+'''');
                    quS.SQL.Add('and [IDATE] >= '+its(Trunc(dsfromrar((sFixDate)))-20));
                    quS.ExecSQL;
                  end;
                  if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:OperationResult')) then     //��� �������� ��������� � �������� ��� �26 �� 23.10.2016 00:00:00 ��������
                  begin
                    if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:OperationResult').NodeByName('tc:OperationResult')) then
                       sResult:=nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:OperationResult').NodeByName('tc:OperationResult').Value;
                    if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:OperationResult').NodeByName('tc:OperationComment')) then
                       sComment:=nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:OperationResult').NodeByName('tc:OperationComment').Value;
                    if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:OperationResult').NodeByName('tc:OperationDate')) then
                       sFixDate:=nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:OperationResult').NodeByName('tc:OperationDate').Value;

                    if sResult='Accepted' then  k:=1 else k:=-1;

                    quS.Active:=False;
                    quS.SQL.Clear;
                    quS.SQL.Add('');
                    quS.SQL.Add('UPDATE [dbo].[ADOCSCORR_HD]');
                    quS.SQL.Add('SET [TICK2] = '+its(IdRec*k));
                    quS.SQL.Add(',[FIXNUMBER] = '''+sIdDoc_B+'''');
                    quS.SQL.Add(',[FIXDATE] = '''+sFixDate+'''');
                    quS.SQL.Add('where [FSRARID] = '''+RARID+'''');
                    quS.SQL.Add('and [SID] = '''+sIdentity+'''');
                    quS.SQL.Add('and [IDATE] >= '+its(Trunc(dsfromrar((sFixDate)))-20));
                    quS.ExecSQL;
                  end;
                end;
              end;
            end;
          end;

          //<--����� �� ������ ���������� ����������
          if tcDocType='QueryBarcode' then begin
            tcTransportId:='';
            if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:TransportId')) then
              tcTransportId:=nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:TransportId').Value;
            tcConclusion:='';
            if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:Result').NodeByName('tc:Conclusion')) then
              tcConclusion:=nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:Result').NodeByName('tc:Conclusion').Value;
            tcComments:='';
            if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:Result').NodeByName('tc:Comments')) then
              tcComments:=nodeRoot.NodeByName('ns:Document').NodeByName('ns:Ticket').NodeByName('tc:Result').NodeByName('tc:Comments').Value;

            //<--������� ����� �������
            quS.Active:=False;
            quS.SQL.Clear;
            quS.SQL.Add('SELECT ID');
            quS.SQL.Add('FROM RAR.dbo.TORAR');
            quS.SQL.Add('WHERE RECEIVE_ID=:RECEIVE_ID');
            quS.ParamByName('RECEIVE_ID').Value:=tcTransportId;
            quS.Active:=True;
            IdToRar:=0;
            if quS.RecordCount>0 then
               IdToRar:=quS.FieldByName('ID').Value;
            quS.Active:=False;
            //-->
            //<--���������� ����� � ��������
            if IdToRar>0 then begin
              quS.Active:=False;
              quS.SQL.Clear;
              quS.SQL.Add('SELECT ID,FSRARID,IDATE,INUMBER,ISTATUS,IDTORAR,REJCOMMENT');
              quS.SQL.Add('FROM dbo.QBARCODE_HD');
              quS.SQL.Add('WHERE IDTORAR=:IDTORAR');
              quS.ParamByName('IDTORAR').Value:=IdToRar;
              quS.Active:=True;
              if quS.RecordCount>0 then begin
                quS.Edit;
                if tcConclusion='Rejected' then
                  quS.FieldByName('ISTATUS').Value:=2; //������ �������: 0-�����������;1-���������;2-�������;3-�������
                quS.FieldByName('REJCOMMENT').Value:=tcComments;
                quS.Post;
              end;
              quS.Active:=False;
            end;
            //-->
          end;
          //-->
        end;
      end;

      Result:=True;
    finally
      quS.Free;
      quSelInHD.Free;
      quProc.Free;
      quSelOutHD.Free;
      Xml.Free;
    end;
  end;
end;

function prDecodeNoAnswerTTN(xmlstr:string;IdRec:integer; FDConn: TFDConnection):Boolean;
var
  Xml: TNativeXml;
  nodeRoot, nodeTTNList, nodeNoAnswer: TXmlNode;
  i: Integer;
  sConsignee, sReplyDate: String;
  sWbRegID, sTTNNumber, sTTNDate, sShipper: String;
  iTTNDate: Integer;
  quS: TFDQuery;
begin
  Result:=False;

//  with dmR do
  if Pos('ns:ReplyNoAnswerTTN',xmlstr)>0 then
  begin
    try
      quS := TFDQuery.Create(nil);
      quS.Connection := FDConn;
      quS.ResourceOptions.SilentMode:=True;     //�� ���������� Wait cursor

      Xml:= TNativeXml.Create(nil);
      Xml.XmlFormat := xfReadable;
      while Pos('''',xmlstr)>0 do xmlstr[Pos('''',xmlstr)]:=' ';

      Xml.ReadFromString(xmlstr);

      nodeRoot:=Xml.Root;
      if Assigned(nodeRoot.NodeByName('ns:Document')) then
      begin
        if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:ReplyNoAnswerTTN')) then
        begin
          sConsignee:='';
          sReplyDate:='';

          if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:ReplyNoAnswerTTN').NodeByName('ttn:Consignee')) then
            sConsignee:=nodeRoot.NodeByName('ns:Document').NodeByName('ns:ReplyNoAnswerTTN').NodeByName('ttn:Consignee').Value;
          if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:ReplyNoAnswerTTN').NodeByName('ttn:ReplyDate')) then
            sReplyDate:=nodeRoot.NodeByName('ns:Document').NodeByName('ns:ReplyNoAnswerTTN').NodeByName('ttn:ReplyDate').Value;
            sReplyDate:=AnsiReplaceStr(sReplyDate, 'T', ' ');

          nodeTTNList:=nodeRoot.NodeByName('ns:Document').NodeByName('ns:ReplyNoAnswerTTN').NodeByName('ttn:ttnlist');
          if Assigned(nodeTTNList) then
          begin
            for i:=0 to nodeTTNList.NodeCount-1 do
            begin
              nodeNoAnswer:=nodeTTNList.Nodes[i];

              sWbRegID:='';
              sTTNNumber:='';
              sTTNDate:='';
              sShipper:='';

              if nodeNoAnswer.Name='ttn:NoAnswer' then
              begin
                if Assigned(nodeNoAnswer.NodeByName('ttn:WbRegID')) then sWbRegID:=nodeNoAnswer.NodeByName('ttn:WbRegID').Value;
                if Assigned(nodeNoAnswer.NodeByName('ttn:ttnNumber')) then sTTNNumber:=nodeNoAnswer.NodeByName('ttn:ttnNumber').Value;
                if Assigned(nodeNoAnswer.NodeByName('ttn:ttnDate')) then sTTNDate:=nodeNoAnswer.NodeByName('ttn:ttnDate').Value;
                if Assigned(nodeNoAnswer.NodeByName('ttn:Shipper')) then sShipper:=nodeNoAnswer.NodeByName('ttn:Shipper').Value;
                iTTNDate:=Trunc(dsfromrar(sTTNDate));
              end;

              if Trim(sWbRegID)>'' then
              begin
                quS.Active:=False;
                quS.SQL.Clear;
                quS.SQL.Add('DECLARE @FSRARID VARCHAR(50) = '''+Trim(sConsignee)+'''');
                quS.SQL.Add('DECLARE @DATE_REPLY VARCHAR(50) = '''+Trim(sReplyDate)+'''');
                quS.SQL.Add('DECLARE @WBREGID VARCHAR(50) = '''+Trim(sWbRegID)+'''');
                quS.SQL.Add('DECLARE @NUMBER VARCHAR(100) = '''+Trim(sTTNNumber)+'''');
                quS.SQL.Add('DECLARE @SHIPDATE VARCHAR(50) = '''+Trim(sTTNDate)+'''');
                quS.SQL.Add('DECLARE @ISHIPDATE VARCHAR(50) = '''+its(iTTNDate)+'''');
                quS.SQL.Add('DECLARE @CLIFROM VARCHAR(50) = '''+Trim(sShipper)+'''');
                quS.SQL.Add('EXECUTE [dbo].[prAddNATTN] @FSRARID,@DATE_REPLY,@WBREGID,@NUMBER,@SHIPDATE,@ISHIPDATE,@CLIFROM');
                quS.ExecSQL;
              end;

            end;
          end;

        end;
      end;

      Result:=True;
    finally
      quS.Free;
      Xml.Free;
    end;
  end;
end;

function prDecodeQueryBarCode(xmlstr:string;IdRec:integer; FDConn: TFDConnection):Boolean;
var
  Xml: TNativeXml;
  nodeRoot, nodeMarks, nodeMark: TXmlNode;
  i: Integer;
  nsFSRAR_ID: string;
  bkQueryNumber: string;
  bkIdentity: string;
  bkType: string;
  bkRank: string;
  bkNumber: string;
  bkBarcode: string;
  IDH: Integer;
begin
  Result:=False;

  with dmR do
  if Pos('ns:ReplyBarcode',xmlstr)>0 then
  begin
    try
      Xml:= TNativeXml.Create(nil);
      Xml.XmlFormat := xfReadable;
      while Pos('''',xmlstr)>0 do xmlstr[Pos('''',xmlstr)]:=' ';

      Xml.ReadFromString(xmlstr);

      nodeRoot:=Xml.Root;
      if Assigned(nodeRoot.NodeByName('ns:Document')) then
      begin
        if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:ReplyBarcode')) then
        begin
          nsFSRAR_ID:='';
          if Assigned(nodeRoot.NodeByName('ns:Owner').NodeByName('ns:FSRAR_ID')) then
            nsFSRAR_ID:=nodeRoot.NodeByName('ns:Owner').NodeByName('ns:FSRAR_ID').Value;
          bkQueryNumber:='';
          if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:ReplyBarcode').NodeByName('bk:QueryNumber')) then
            bkQueryNumber:=nodeRoot.NodeByName('ns:Document').NodeByName('ns:ReplyBarcode').NodeByName('bk:QueryNumber').Value;

          //<--������� �������� �������
          IDH:=0;
          if (nsFSRAR_ID<>'') and (StrToInt(bkQueryNumber)>0) then
          begin
            quS.Active:=False;
            quS.SQL.Clear;
            quS.SQL.Add('SELECT ID,FSRARID,INUMBER');
            quS.SQL.Add('FROM dbo.QBARCODE_HD');
            quS.SQL.Add('WHERE ISTATUS=1');  //������ �������: 0-�����������;1-���������;2-�������;3-�������
            quS.SQL.Add('  AND FSRARID=:FSRARID');
            quS.SQL.Add('  AND INUMBER=:INUMBER');
            quS.ParamByName('FSRARID').Value:=nsFSRAR_ID;
            quS.ParamByName('INUMBER').Value:=StrToInt(bkQueryNumber);
            quS.Active:=True;
            if quS.RecordCount>0 then
               IDH:=quS.FieldByName('ID').Value;
            quS.Active:=False;
          end;
          if IDH=0 then Exit(False);
          //-->

          nodeMarks:=nodeRoot.NodeByName('ns:Document').NodeByName('ns:ReplyBarcode').NodeByName('bk:Marks');
          if Assigned(nodeMarks) then
          begin
            for i:=0 to nodeMarks.NodeCount-1 do
            begin
              nodeMark:=nodeMarks.Nodes[i];

              bkIdentity:='';
              bkType:='';
              bkRank:='';
              bkNumber:='';
              bkBarcode:='';

              if nodeMark.Name='bk:Mark' then
              begin
                if Assigned(nodeMark.NodeByName('bk:Identity')) then bkIdentity:=nodeMark.NodeByName('bk:Identity').Value;
                if Assigned(nodeMark.NodeByName('bk:Type')) then bkType:=nodeMark.NodeByName('bk:Type').Value;
                if Assigned(nodeMark.NodeByName('bk:Rank')) then bkRank:=nodeMark.NodeByName('bk:Rank').Value;
                if Assigned(nodeMark.NodeByName('bk:Number')) then bkNumber:=nodeMark.NodeByName('bk:Number').Value;
                if Assigned(nodeMark.NodeByName('bk:Barcode')) then bkBarcode:=nodeMark.NodeByName('bk:Barcode').Value;

                //<--����� BarCode � ��������
                quS.Active:=False;
                quS.SQL.Clear;
                quS.SQL.Add('SELECT IDH,ID,IDENT,TYPE,RANK,NUMBER,BARCODE');
                quS.SQL.Add('FROM dbo.QBARCODE_SP');
                quS.SQL.Add('WHERE IDH=:IDH');
                quS.SQL.Add('  AND TYPE=:TYPE');
                quS.SQL.Add('  AND RANK=:RANK');
                quS.SQL.Add('  AND NUMBER=:NUMBER');
                quS.ParamByName('IDH').Value:=IDH;
                quS.ParamByName('TYPE').Value:=bkType;
                quS.ParamByName('RANK').Value:=bkRank;
                quS.ParamByName('NUMBER').Value:=bkNumber;
                quS.Active:=True;
                if quS.RecordCount>0 then begin
                   quS.Edit;
                   quS.FieldByName('BARCODE').Value:=bkBarcode;
                   quS.Post;
                end;
                quS.Active:=False;
                //-->
              end;
            end;
          end;

          //<--���������� � ��������
          quS.Active:=False;
          quS.SQL.Clear;
          quS.SQL.Add('SELECT ID,FSRARID,IDATE,INUMBER,ISTATUS,IDTORAR,REJCOMMENT');
          quS.SQL.Add('FROM dbo.QBARCODE_HD');
          quS.SQL.Add('WHERE ID=:IDH');
          quS.ParamByName('IDH').Value:=IDH;
          quS.Active:=True;
          if quS.RecordCount>0 then begin
            quS.Edit;
            quS.FieldByName('ISTATUS').Value:=3; //������ �������: 0-�����������;1-���������;2-�������;3-�������
            quS.FieldByName('REJCOMMENT').Value:='';
            quS.Post;
          end;
          quS.Active:=False;
          //-->
        end;
      end;

      Result:=True;
    finally
      Xml.Free;
    end;
  end;
end;

function its(i:Integer):String;
begin
  result:=IntToStr(i);
end;

function prDecodeTTN_AB(xmlstr,ssid:string):Boolean;

type

     TCli = record
              ClientRegId,FullName,ShortName,INN,KPP,Country,RegionCode,description:string;
            end;

     TDoc = record
              sIDH:string; //ID ���������
              NUMBER,sDate,sType,sUnitType,sShippingDate:string;
              CliFrom:TCli;
              CliTo:TCli; //��� ������ ����
            end;

     TCard = record
               FullName,AlcCode:string;
               Capacity,AlcVolume:Real;
               ProductVCode:Integer;
               Prod:TCli;
               Impr:TCli;
             end;

     TPos = record
              Num:Integer;
              Quant,Price:Real;
              Card:TCard;
              info_a,info_b,party:string;
            end;

var
  Xml: TNativeXml;
  nodeRoot,nodeHead,nodeSpec,nodePos,{nodeProd,}nodeImp,nodeR: TXmlNode;
  vDoc:TDoc;
  vPos:TPos;
  I: integer;
  StrWk:string;
  iDate:Integer;
//  iD,iC:Integer;


  procedure ClearDoc;
  begin
    vDoc.NUMBER:=''; vDoc.sIDH:=''; vDoc.sDate:=''; vDoc.sType:=''; vDoc.sUnitType:=''; vDoc.sShippingDate:='';
    vDoc.CliTo.ClientRegId:='';
    vDoc.CliFrom.ClientRegId:=''; vDoc.CliFrom.INN:=''; vDoc.CliFrom.KPP:=''; vDoc.CliFrom.FullName:=''; vDoc.CliFrom.ShortName:=''; vDoc.CliFrom.Country:=''; vDoc.CliFrom.RegionCode:=''; vDoc.CliFrom.description:='';
  end;


  procedure ClearPos;
  begin
    vPos.Num:=0;
    vPos.Quant:=0; vPos.Price:=0;
    vPos.Card.FullName:=''; vPos.Card.AlcCode:=''; vPos.Card.Capacity:=0; vPos.Card.AlcVolume:=0; vPos.Card.ProductVCode:=0; vPos.info_a:=''; vPos.info_b:=''; vPos.party:='';
    vPos.Card.Prod.ClientRegId:=''; vPos.Card.Prod.FullName:=''; vPos.Card.Prod.ShortName:=' '; vPos.Card.Prod.INN:=' '; vPos.Card.Prod.INN:=' '; vPos.Card.Prod.KPP:=' '; vPos.Card.Prod.Country:=' '; vPos.Card.Prod.RegionCode:=' '; vPos.Card.Prod.description:=' ';
    vPos.Card.Impr.ClientRegId:=''; vPos.Card.Impr.FullName:=''; vPos.Card.Impr.ShortName:=' '; vPos.Card.Impr.INN:=' '; vPos.Card.Impr.INN:=' '; vPos.Card.Impr.KPP:=' '; vPos.Card.Impr.Country:=' '; vPos.Card.Impr.RegionCode:=' '; vPos.Card.Impr.description:=' ';
  end;


begin
  Result:=False;
  with dmR do
  if Pos('WayBill',xmlstr)>0 then
  begin
    try
      Xml:= TNativeXml.Create(nil);
      Xml.XmlFormat := xfReadable;
      while Pos('''',xmlstr)>0 do xmlstr[Pos('''',xmlstr)]:='"';

      Xml.ReadFromString(xmlstr);

      ClearDoc;

      nodeRoot:=Xml.Root;

      if Assigned(nodeRoot.NodeByName('ns:Document')) then
      begin
        if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:WayBill')) then
        begin
          if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeByName('wb:Identity')) then
          begin
            vDoc.sIDH:=nodeRoot.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeByName('wb:Identity').Value;

            if vDoc.sIDH=ssid then  //ID ��������� ��������� � ��������� - ����������
            begin
              if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeByName('wb:Header')) then //������ ���������
              begin
                nodeHead:=nodeRoot.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeByName('wb:Header');

                if Assigned(nodeHead.NodeByName('wb:NUMBER')) then vDoc.NUMBER:=nodeHead.NodeByName('wb:NUMBER').Value;
                if Assigned(nodeHead.NodeByName('wb:Date')) then vDoc.sDate:=nodeHead.NodeByName('wb:Date').Value;
                if Assigned(nodeHead.NodeByName('wb:Type')) then vDoc.sType:=nodeHead.NodeByName('wb:Type').Value;
                if Assigned(nodeHead.NodeByName('wb:UnitType')) then vDoc.sUnitType:=nodeHead.NodeByName('wb:UnitType').Value;
                if Assigned(nodeHead.NodeByName('wb:ShippingDate')) then vDoc.sShippingDate:=nodeHead.NodeByName('wb:ShippingDate').Value;

                //�������� ��������� ���������

                StrWk:=vDoc.sDate;
                while Pos('-',StrWk)>0 do Delete(StrWk,Pos('-',StrWk),1);

                StrWk:=Copy(StrWk,7,2)+'.'+Copy(StrWk,5,2)+'.'+Copy(StrWk,1,4);

                IDate:=Trunc(StrToDateTimeDef(StrWk,date));

                if Assigned(nodeRoot.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeByName('wb:Content')) then //������������
                begin
                  nodeSpec:=nodeRoot.NodeByName('ns:Document').NodeByName('ns:WayBill').NodeByName('wb:Content');

                  for i:=0 to nodeSpec.NodeCount-1 do
                  begin
                    nodePos:=nodeSpec[i];

                    ClearPos;

                    if Pos('wb:Position',nodePos.Name)>0 then  //��� ������� ���������
                    begin
                      if Assigned(nodePos.NodeByName('wb:Identity')) then vPos.Num:=StrToIntDef(nodePos.NodeByName('wb:Identity').Value,0);
                      if Assigned(nodePos.NodeByName('wb:Quantity')) then vPos.Quant:=stf(nodePos.NodeByName('wb:Quantity').Value);
                      if Assigned(nodePos.NodeByName('wb:Price')) then vPos.Price:=stf(nodePos.NodeByName('wb:Price').Value);
                      if Assigned(nodePos.NodeByName('wb:Party')) then vPos.party:=nodePos.NodeByName('wb:Party').Value;

                      if Assigned(nodePos.NodeByName('wb:InformA')) then
                        if Assigned(nodePos.NodeByName('wb:InformA').NodeByName('pref:RegId')) then vPos.info_a:=nodePos.NodeByName('wb:InformA').NodeByName('pref:RegId').Value;

                      if Assigned(nodePos.NodeByName('wb:InformB')) then
                        if Assigned(nodePos.NodeByName('wb:InformB').NodeByName('pref:InformBItem')) then
                          if Assigned(nodePos.NodeByName('wb:InformB').NodeByName('pref:InformBItem').NodeByName('pref:BRegId')) then
                            vPos.info_b:=nodePos.NodeByName('wb:InformB').NodeByName('pref:InformBItem').NodeByName('pref:BRegId').Value;

                      if (vPos.info_a>'') or (vPos.info_b>'') then
                      begin  //� � � �����
                        //����� � ������������
                        quS.Active:=False;
                        quS.SQL.Clear;
                        quS.SQL.Add('');
                        quS.SQL.Add('DECLARE @FSRARID varchar(50) = '''+Trim(CommonSet.FSRAR_ID)+'''');
                        quS.SQL.Add('DECLARE @IDATE int = '+its(iDate));
                        quS.SQL.Add('DECLARE @SID varchar(50) = '''+Trim(vDoc.sIDH)+'''');
                        quS.SQL.Add('DECLARE @NUM int= '+its(vPos.Num));
                        quS.SQL.Add('DECLARE @INFO_A varchar(50) = '''+Trim(vPos.info_a)+'''');
                        quS.SQL.Add('DECLARE @INFO_B varchar(50) = '''+Trim(vPos.info_b)+'''');
                        quS.SQL.Add('EXECUTE dbo.prAddDocInSpec_AB @FSRARID,@IDATE,@SID,@NUM,@INFO_A,@INFO_B');
                        quS.ExecSQL;
                      end;
                    end;
                  end;
                end;
              end;
            end;
          end;
        end;
      end;

      Result:=True;
    finally
      Xml.Free;
    end;
  end;
end;



end.
