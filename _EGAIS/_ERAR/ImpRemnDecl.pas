unit ImpRemnDecl;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, Vcl.Menus, Vcl.StdCtrls, cxButtons, cxLabel, cxMaskEdit,
  nativexml,
  cxButtonEdit, cxTextEdit, cxMemo, Vcl.ExtCtrls;

type
  TfmImportRemnDecl = class(TForm)
    Panel1: TPanel;
    cxMemo1: TcxMemo;
    cxButtonEdit1: TcxButtonEdit;
    cxLabel1: TcxLabel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    OpenDialog1: TOpenDialog;
    procedure FormShow(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject; AButtonIndex: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmImportRemnDecl: TfmImportRemnDecl;

implementation

uses
  EgaisDecode,UnitFunction, dmRar, Shared_Functions;

{$R *.dfm}

procedure TfmImportRemnDecl.cxButton1Click(Sender: TObject);
var
  Xml: TNativeXml;
  nodeRoot,nodeSpr,nodePosSpr,nodePosLic,nodeDoc,nodeVol,nodeVid,nodeVidProd: TXmlNode;
  sDate,sNumF,sPeriod,sYear:string;
  i,k,j,o:Integer;
  sName,sInn,sKPP,sId,sIdRar:string;
  sIdLic,SerLic,sDate1,sDate2,sOrgan:string;
  OrgName,OrgKPP,OrgOb:string;
  iOrg,iObor:Integer;
  iNum,iVid:Integer;
  iNum1,iNum2,IdProd:Integer;
  ArSum:array[1..16] of Real;
begin
  //��������� ����
  cxMemo1.Clear;
  if FileExists(cxButtonEdit1.Text) then
  begin
    cxMemo1.Lines.Add('  ���� ������.'); delay(10);

    iOrg:=1;

    try
      Xml:= TNativeXml.Create(nil);
      Xml.XmlFormat := xfReadable;
      Xml.LoadFromFile(cxButtonEdit1.Text);
      nodeRoot:=Xml.Root;
      sDate:=Trim(nodeRoot.ReadAttributeString('�������',''));


      if sDate>'' then
      begin
        cxMemo1.Lines.Add('    - ���� '+sDate); delay(10);

        with dmR do
        begin
          cxMemo1.Lines.Add('    - ���������� �������'); delay(10);

          quS.Active:=False;
          quS.SQL.Clear;
          quS.SQL.Add('EXECUTE [dbo].[prClearImp]');
          quS.ExecSQL;

          if Assigned(nodeRoot.NodeByName('��������')) then
          begin
            sNumF:=Trim(nodeRoot.NodeByName('��������').ReadAttributeString('�������',''));
            sPeriod:=Trim(nodeRoot.NodeByName('��������').ReadAttributeString('�������������',''));
            sYear:=Trim(nodeRoot.NodeByName('��������').ReadAttributeString('������������',''));
          end;
          if Assigned(nodeRoot.NodeByName('�����������')) then
          begin
            nodeSpr:=nodeRoot.NodeByName('�����������');

            for i:=0 to nodeSpr.NodeCount-1 do
            begin
              nodePosSpr:=nodeSpr[i];

              if Pos('����������������������',nodePosSpr.Name)>0 then  //��� ����������������������
              begin
                sId:=Trim(nodePosSpr.ReadAttributeString('�����������',''));
                sName:=Trim(nodePosSpr.ReadAttributeString('�000000000004',''));
                sInn:=Trim(nodePosSpr.ReadAttributeString('�000000000005',''));
                sKPP:=Trim(nodePosSpr.ReadAttributeString('�000000000006',''));

                if sInn='' then
                begin
                  if Assigned(nodePosSpr.NodeByName('��')) then
                  begin
                    sInn:=Trim(nodePosSpr.NodeByName('��').ReadAttributeString('�000000000005',''));
                    sKPP:=Trim(nodePosSpr.NodeByName('��').ReadAttributeString('�000000000006',''));
                  end;

                  if Assigned(nodePosSpr.NodeByName('��')) then
                  begin
                    sInn:=Trim(nodePosSpr.NodeByName('��').ReadAttributeString('�000000000005',''));
                  end;
                end;

                if sId>'' then  //��������� ������������� �� ��������� �������
                begin
                  sIdRar:='';

                  quS.Active:=False;
                  quS.SQL.Clear;
                  quS.SQL.Add('');
                  quS.SQL.Add('INSERT INTO [dbo].[IMP_PROD] ([ID],[NAME],[INN],[KPP],[IDRAR])');
                  quS.SQL.Add('     VALUES');
                  quS.SQL.Add('           ('+sId);
                  quS.SQL.Add('           ,'''+sName+'''');
                  quS.SQL.Add('           ,'''+sInn+'''');
                  quS.SQL.Add('           ,'''+sKPP+'''');
                  quS.SQL.Add('           ,'''+sIdRar+''')');

                  quS.ExecSQL;
                end;
              end;

              if Pos('����������',nodePosSpr.Name)>0 then  //��� ����������
              begin
                sId:=Trim(nodePosSpr.ReadAttributeString('��������',''));
                sName:=Trim(nodePosSpr.ReadAttributeString('�000000000007',''));
                if Assigned(nodePosSpr.NodeByName('��')) then
                begin
                  sInn:=Trim(nodePosSpr.NodeByName('��').ReadAttributeString('�000000000009',''));
                  sKPP:=Trim(nodePosSpr.NodeByName('��').ReadAttributeString('�000000000010',''));
                end;

                if sId>'' then  //��������� ���������� �� ��������� �������
                begin
                  sIdRar:='';

                  quS.Active:=False;
                  quS.SQL.Clear;
                  quS.SQL.Add('');
                  quS.SQL.Add('INSERT INTO [dbo].[IMP_CLI] ([ID],[NAME],[INN],[KPP],[IDRAR])');
                  quS.SQL.Add('     VALUES');
                  quS.SQL.Add('           ('+sId);
                  quS.SQL.Add('           ,'''+sName+'''');
                  quS.SQL.Add('           ,'''+sInn+'''');
                  quS.SQL.Add('           ,'''+sKPP+'''');
                  quS.SQL.Add('           ,'''+sIdRar+''')');

                  quS.ExecSQL;
                end;

                for k:=0 to nodePosSpr.NodeCount-1 do
                begin
                  nodePosLic:=nodePosSpr[k];
                  if Pos('��������',nodePosLic.Name)>0 then  //��� ��������
                  begin
                    if Assigned(nodePosLic.NodeByName('��������')) then
                    begin
                      sIdLic:=Trim(nodePosLic.NodeByName('��������').ReadAttributeString('����������',''));
                      SerLic:=Trim(nodePosLic.NodeByName('��������').ReadAttributeString('�000000000011',''));
                      sDate1:=Trim(nodePosLic.NodeByName('��������').ReadAttributeString('�000000000012',''));
                      sDate2:=Trim(nodePosLic.NodeByName('��������').ReadAttributeString('�000000000013',''));
                      sOrgan:=Trim(nodePosLic.NodeByName('��������').ReadAttributeString('�000000000014',''));

                      if sIdLic>'' then
                      begin
                        if sId>'' then  //��������� ���������� �� ��������� �������
                        begin
                          quS.Active:=False;
                          quS.SQL.Clear;
                          quS.SQL.Add('');
                          quS.SQL.Add('INSERT INTO [dbo].[IMP_CLI_LIC] ([IDCLI],[ID],[SERNUM],[SDATE1],[SDATE2],[ORGAN],[DATE1],[DATE2])');
                          quS.SQL.Add('     VALUES');
                          quS.SQL.Add('           ('+sId);
                          quS.SQL.Add('           ,'+sIdLic);
                          quS.SQL.Add('           ,'''+SerLic+'''');
                          quS.SQL.Add('           ,'''+sDate1+'''');
                          quS.SQL.Add('           ,'''+sDate2+'''');
                          quS.SQL.Add('           ,'''+sOrgan+'''');
                          quS.SQL.Add('           ,'''+sDate1+'''');
                          quS.SQL.Add('           ,'''+sDate2+''')');

                          quS.ExecSQL;
                        end;
                      end;
                    end;
                  end;
                end;
              end;
            end;
          end;

          if Assigned(nodeRoot.NodeByName('��������')) then
          begin
            nodeDoc:=nodeRoot.NodeByName('��������');
            for i:=0 to nodeDoc.NodeCount-1 do
            begin
              nodeVol:=nodeDoc[i];
              if Pos('������������',nodeVol.Name)>0 then  //��� ������������
              begin
                OrgName:=Trim(nodeVol.ReadAttributeString('����',''));
                OrgKPP:=Trim(nodeVol.ReadAttributeString('�����',''));
                OrgOb:=Trim(nodeVol.ReadAttributeString('��������������',''));
                if OrgKPP>'' then
                begin
                  if OrgOb='true' then iObor:=1 else iObor:=0;

                  quS.Active:=False;
                  quS.SQL.Clear;
                  quS.SQL.Add('');
                  quS.SQL.Add('INSERT INTO [dbo].[IMP_ORG] ([ID],[NAME],[KPP],[OBOR],[RARID])');
                  quS.SQL.Add('     VALUES');
                  quS.SQL.Add('           ('+its(iOrg));
                  quS.SQL.Add('           ,'''+OrgName+'''');
                  quS.SQL.Add('           ,'''+OrgKPP+'''');
                  quS.SQL.Add('           ,'+its(iObor));
                  quS.SQL.Add('           ,'''')');

                  quS.ExecSQL;
                end;

                if OrgOb='true' then
                begin
                  for k:=0 to nodeVol.NodeCount-1 do
                  begin
                    nodeVid:=nodeVol[k];
                    if Pos('������',nodeVid.Name)>0 then  //��� ������
                    begin
                      iNum:=StrToIntDef(Trim(nodeVid.ReadAttributeString('�N','')),0);
                      iVid:=StrToIntDef(Trim(nodeVid.ReadAttributeString('�000000000003','')),0);
                      if (iNum>0) and (iVid>0) then
                      begin
                        for j:=0 to nodeVid.NodeCount-1 do
                        begin
                          nodeVidProd:=nodeVid[j];
                          if Pos('����������������',nodeVidProd.Name)>0 then  //��� ������ �� �������������
                          begin
                            for o:=1 to 16 do ArSum[o]:=0;
                            iNum1:=StrToIntDef(Trim(nodeVidProd.ReadAttributeString('�N','')),0);
                            IdProd:=StrToIntDef(Trim(nodeVidProd.ReadAttributeString('�����������','')),0);
                            if Assigned(nodeVidProd.NodeByName('��������')) then
                            begin
                              iNum2:=StrToIntDef(Trim(nodeVidProd.NodeByName('��������').ReadAttributeString('�N','')),0);
                              ArSum[1]:=s2f(nodeVidProd.NodeByName('��������').ReadAttributeString('�100000000006',''));
                              ArSum[2]:=s2f(nodeVidProd.NodeByName('��������').ReadAttributeString('�100000000007',''));
                              ArSum[3]:=s2f(nodeVidProd.NodeByName('��������').ReadAttributeString('�100000000008',''));
                              ArSum[4]:=s2f(nodeVidProd.NodeByName('��������').ReadAttributeString('�100000000009',''));
                              ArSum[5]:=s2f(nodeVidProd.NodeByName('��������').ReadAttributeString('�100000000010',''));
                              ArSum[6]:=s2f(nodeVidProd.NodeByName('��������').ReadAttributeString('�100000000011',''));
                              ArSum[7]:=s2f(nodeVidProd.NodeByName('��������').ReadAttributeString('�100000000012',''));
                              ArSum[8]:=s2f(nodeVidProd.NodeByName('��������').ReadAttributeString('�100000000013',''));
                              ArSum[9]:=s2f(nodeVidProd.NodeByName('��������').ReadAttributeString('�100000000014',''));
                              ArSum[10]:=s2f(nodeVidProd.NodeByName('��������').ReadAttributeString('�100000000015',''));
                              ArSum[11]:=s2f(nodeVidProd.NodeByName('��������').ReadAttributeString('�100000000016',''));
                              ArSum[12]:=s2f(nodeVidProd.NodeByName('��������').ReadAttributeString('�100000000017',''));
                              ArSum[13]:=s2f(nodeVidProd.NodeByName('��������').ReadAttributeString('�100000000018',''));
                              ArSum[14]:=s2f(nodeVidProd.NodeByName('��������').ReadAttributeString('�100000000019',''));
                              ArSum[15]:=s2f(nodeVidProd.NodeByName('��������').ReadAttributeString('�100000000020',''));
                              ArSum[16]:=s2f(nodeVidProd.NodeByName('��������').ReadAttributeString('�100000000021',''));

                              if iNum2>0 then
                              begin
                                quS.Active:=False;
                                quS.SQL.Clear;
                                quS.SQL.Add('');
                                quS.SQL.Add('INSERT INTO [dbo].[IMP_DECL] ([IORG],[ID1],[AVID],[ID2],[IDPROD],[ID3],[S01],[S02],[S03],[S04],[S05],[S06],[S07],[S08],[S09],[S10],[S11],[S12],[S13],[S14],[S15],[S16])');
                                quS.SQL.Add('     VALUES');
                                quS.SQL.Add('           ('+its(iOrg));
                                quS.SQL.Add('           ,'+its(iNum));
                                quS.SQL.Add('           ,'+its(iVid));
                                quS.SQL.Add('           ,'+its(iNum1));
                                quS.SQL.Add('           ,'+its(IdProd));
                                quS.SQL.Add('           ,'+its(iNum2));
                                quS.SQL.Add('           ,'+fs(ArSum[1]));
                                quS.SQL.Add('           ,'+fs(ArSum[2]));
                                quS.SQL.Add('           ,'+fs(ArSum[3]));
                                quS.SQL.Add('           ,'+fs(ArSum[4]));
                                quS.SQL.Add('           ,'+fs(ArSum[5]));
                                quS.SQL.Add('           ,'+fs(ArSum[6]));
                                quS.SQL.Add('           ,'+fs(ArSum[7]));
                                quS.SQL.Add('           ,'+fs(ArSum[8]));
                                quS.SQL.Add('           ,'+fs(ArSum[9]));
                                quS.SQL.Add('           ,'+fs(ArSum[10]));
                                quS.SQL.Add('           ,'+fs(ArSum[11]));
                                quS.SQL.Add('           ,'+fs(ArSum[12]));
                                quS.SQL.Add('           ,'+fs(ArSum[13]));
                                quS.SQL.Add('           ,'+fs(ArSum[14]));
                                quS.SQL.Add('           ,'+fs(ArSum[15]));
                                quS.SQL.Add('           ,'+fs(ArSum[16])+')');

                                quS.ExecSQL;
                              end;
                            end;
                          end;
                        end;
                      end;
                    end;
                  end;
                end;


                inc(iOrg);
              end;
            end;
          end;
        end;
      end;
    finally
      Xml.Free;
    end;
  end;
  cxMemo1.Lines.Add('������� ��������.'); delay(10);
end;

procedure TfmImportRemnDecl.cxButtonEdit1PropertiesButtonClick(Sender: TObject; AButtonIndex: Integer);
begin
  if OpenDialog1.Execute then cxButtonEdit1.Text:=OpenDialog1.fileName;
end;

procedure TfmImportRemnDecl.FormShow(Sender: TObject);
begin
 cxMemo1.Clear;
end;

end.
