object fmDocSpecInv: TfmDocSpecInv
  Left = 0
  Top = 0
  Caption = #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1103' '#1080#1085#1074#1077#1085#1090#1072#1088#1080#1079#1072#1094#1080#1080
  ClientHeight = 616
  ClientWidth = 1074
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1074
    Height = 127
    BarManager = dxBarManager1
    Style = rs2010
    ColorSchemeAccent = rcsaOrange
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1103' '#1080#1085#1074#1077#1085#1090#1072#1088#1080#1079#1072#1094#1080#1080' '
      Groups = <
        item
          Caption = 
            '         '#1059#1087#1088#1072#1074#1083#1077#1085#1080#1077'                                             ' +
            '         '
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end>
      Index = 0
    end
  end
  object GridSpec: TcxGrid
    Left = 0
    Top = 127
    Width = 1074
    Height = 489
    Align = alClient
    TabOrder = 1
    LookAndFeel.Kind = lfOffice11
    LookAndFeel.NativeStyle = False
    object ViewSpecInv: TcxGridDBTableView
      PopupMenu = PopupMenu1
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.InfoPanel.Visible = True
      Navigator.Visible = True
      DataController.DataSource = dsquSpecInv
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'QUANTF'
          Column = ViewSpecInvQUANTF
        end
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'QUANTR'
          Column = ViewSpecInvQUANTR
        end
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'QUANTD'
          Column = ViewSpecInvQUANTD
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Inserting = False
      OptionsSelection.MultiSelect = True
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      object ViewSpecInvID: TcxGridDBColumn
        Caption = #8470' '#1087#1087
        DataBinding.FieldName = 'ID'
        Options.Editing = False
        Width = 53
      end
      object ViewSpecInvCAP: TcxGridDBColumn
        Caption = #1050#1040#1055
        DataBinding.FieldName = 'CAP'
        Options.Editing = False
        Width = 143
      end
      object ViewSpecInvNAME: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'NAME'
        Options.Editing = False
        Width = 374
      end
      object ViewSpecInvAVID: TcxGridDBColumn
        Caption = #1074#1080#1076' '#1040#1055
        DataBinding.FieldName = 'AVID'
        Options.Editing = False
        Width = 48
      end
      object ViewSpecInvKREP: TcxGridDBColumn
        Caption = #1050#1088#1077#1087'.'
        DataBinding.FieldName = 'KREP'
        Options.Editing = False
        Width = 44
      end
      object ViewSpecInvVOL: TcxGridDBColumn
        Caption = #1054#1073#1098#1077#1084
        DataBinding.FieldName = 'VOL'
        Options.Editing = False
        Width = 48
      end
      object ViewSpecInvQUANTR: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086' '#1088#1072#1089#1095'.'
        DataBinding.FieldName = 'QUANTR'
        Options.Editing = False
        Styles.Content = dmR.stlBold
      end
      object ViewSpecInvQUANTF: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086' '#1092#1072#1082#1090'.'
        DataBinding.FieldName = 'QUANTF'
        Styles.Content = dmR.stlBold
      end
      object ViewSpecInvQUANTD: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086' '#1088#1072#1079#1085#1080#1094#1072
        DataBinding.FieldName = 'QUANTD'
        Options.Editing = False
        Styles.Content = dmR.stlBold
      end
      object ViewSpecInvINFO_A: TcxGridDBColumn
        Caption = #1057#1087#1088#1072#1074#1082#1072' '#1040
        DataBinding.FieldName = 'INFO_A'
        Options.Editing = False
        Width = 70
      end
      object ViewSpecInvINFO_B: TcxGridDBColumn
        Caption = #1057#1087#1088#1072#1074#1082#1072' '#1041
        DataBinding.FieldName = 'INFO_B'
        Options.Editing = False
        Width = 77
      end
    end
    object LevelSpec: TcxGridLevel
      GridView = ViewSpecInv
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      #1044#1077#1081#1089#1090#1074#1080#1103)
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmR.SmallImage
    ImageOptions.LargeImages = dmR.LargeImage
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 540
    Top = 164
    DockControlHeights = (
      0
      0
      0
      0)
    object dxBarManager1Bar1: TdxBar
      Caption = #1059#1087#1088#1072#1074#1083#1077#1085#1080#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 1001
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1047#1072#1082#1088#1099#1090#1100
      CaptionButtons = <>
      DockedLeft = 266
      DockedTop = 0
      FloatLeft = 1161
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = acSave
      Category = 0
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Caption = 
        '                                                                ' +
        '                '
      Category = 0
      Hint = 
        '                                                                ' +
        '                '
      Visible = ivAlways
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = acClose
      Category = 0
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = #1053#1086#1084#1077#1088' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
      Category = 0
      Hint = #1053#1086#1084#1077#1088' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
      Visible = ivAlways
      PropertiesClassName = 'TcxTextEditProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = 'New SubItem'
      Category = 0
      Visible = ivAlways
      ItemLinks = <>
    end
    object cxBarEditItem2: TcxBarEditItem
      Caption = #1044#1072#1090#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
      Category = 0
      Hint = #1044#1072#1090#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
      Visible = ivAlways
      PropertiesClassName = 'TcxDateEditProperties'
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = acTestSpec
      Category = 0
    end
    object dxBarGroup1: TdxBarGroup
      Items = ()
    end
  end
  object ActionList1: TActionList
    Images = dmR.SmallImage
    Left = 648
    Top = 168
    object acClose: TAction
      Caption = #1047#1072#1082#1088#1099#1090#1100
      ImageIndex = 100
      OnExecute = acCloseExecute
    end
    object acSave: TAction
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' (Ctrl + S)'
      ImageIndex = 7
      ShortCut = 16467
      OnExecute = acSaveExecute
    end
    object acTestSpec: TAction
      Caption = #1055#1088#1086#1074#1077#1088#1082#1072' '#1089#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1080
      ImageIndex = 38
    end
    object acDelPos: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1087#1086#1079#1080#1094#1080#1102
      ImageIndex = 4
      OnExecute = acDelPosExecute
    end
  end
  object quSpecInv: TFDQuery
    CachedUpdates = True
    Connection = dmR.FDConnection
    Transaction = dmR.FDTrans
    UpdateTransaction = dmR.FDTransUpdate
    UpdateObject = UpdSQL1
    SQL.Strings = (
      'SELECT sp.[IDH]'
      '      ,sp.[ID]'
      '      ,sp.[CAP]'
      #9'  ,ca.NAME'
      #9'  ,ca.AVID'
      #9'  ,ca.KREP'
      #9'  ,ca.VOL'
      '      ,sp.[QUANTR]'
      '      ,sp.[QUANTF]'
      '      ,sp.[QUANTF]-sp.[QUANTR] as QUANTD'
      '      ,sp.[INFO_A]'
      '      ,sp.[INFO_B]'
      ''
      '  FROM [dbo].[ADOCSINV_SP] sp'
      '  left join [dbo].[ACARDS] ca on ca.KAP=sp.[CAP]'
      '  where IDH=:IDH')
    Left = 88
    Top = 392
    ParamData = <
      item
        Name = 'IDH'
        DataType = ftInteger
        ParamType = ptInput
        Value = 10
      end>
    object quSpecInvIDH: TLargeintField
      FieldName = 'IDH'
      Origin = 'IDH'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quSpecInvID: TLargeintField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quSpecInvCAP: TStringField
      FieldName = 'CAP'
      Origin = 'CAP'
    end
    object quSpecInvNAME: TMemoField
      FieldName = 'NAME'
      Origin = 'NAME'
      BlobType = ftMemo
      Size = 2147483647
    end
    object quSpecInvAVID: TIntegerField
      FieldName = 'AVID'
      Origin = 'AVID'
    end
    object quSpecInvKREP: TSingleField
      FieldName = 'KREP'
      Origin = 'KREP'
      DisplayFormat = '0.0'
    end
    object quSpecInvVOL: TSingleField
      FieldName = 'VOL'
      Origin = 'VOL'
      DisplayFormat = '0.000'
    end
    object quSpecInvQUANTR: TSingleField
      FieldName = 'QUANTR'
      Origin = 'QUANTR'
      DisplayFormat = '0.000'
    end
    object quSpecInvQUANTF: TSingleField
      FieldName = 'QUANTF'
      Origin = 'QUANTF'
      DisplayFormat = '0.000'
    end
    object quSpecInvQUANTD: TSingleField
      FieldName = 'QUANTD'
      Origin = 'QUANTD'
      ReadOnly = True
      DisplayFormat = '0.000'
    end
    object quSpecInvINFO_A: TStringField
      FieldName = 'INFO_A'
      Origin = 'INFO_A'
      Size = 50
    end
    object quSpecInvINFO_B: TStringField
      FieldName = 'INFO_B'
      Origin = 'INFO_B'
      Size = 50
    end
  end
  object UpdSQL1: TFDUpdateSQL
    Connection = dmR.FDConnection
    InsertSQL.Strings = (
      'INSERT INTO RAR.dbo.ADOCSINV_SP'
      '(QUANTF)'
      'VALUES (:NEW_QUANTF);'
      'SELECT QUANTF'
      'FROM RAR.dbo.ADOCSINV_SP'
      'WHERE IDH = :NEW_IDH AND ID = :NEW_ID')
    ModifySQL.Strings = (
      'UPDATE RAR.dbo.ADOCSINV_SP'
      'SET QUANTF = :NEW_QUANTF'
      'WHERE IDH = :OLD_IDH AND ID = :OLD_ID;'
      'SELECT QUANTF'
      'FROM RAR.dbo.ADOCSINV_SP'
      'WHERE IDH = :NEW_IDH AND ID = :NEW_ID')
    DeleteSQL.Strings = (
      'DELETE FROM RAR.dbo.ADOCSINV_SP'
      'WHERE IDH = :OLD_IDH AND ID = :OLD_ID')
    FetchRowSQL.Strings = (
      'SELECT IDH, ID, CAP, QUANTR, QUANTF, INFO_A, INFO_B'
      'FROM RAR.dbo.ADOCSINV_SP'
      'WHERE IDH = :IDH AND ID = :ID')
    Left = 176
    Top = 360
  end
  object dsquSpecInv: TDataSource
    DataSet = quSpecInv
    Left = 88
    Top = 456
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 232
    Top = 200
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svFont]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
    end
    object cxStyle2: TcxStyle
    end
  end
  object PopupMenu1: TPopupMenu
    Images = dmR.SmallImage
    Left = 432
    Top = 272
    object N1: TMenuItem
      Action = acDelPos
    end
  end
  object quE: TFDQuery
    Connection = dmR.FDConnection
    Left = 80
    Top = 280
  end
end
