unit MCrystDocs;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  CustomChildForm, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  dxRibbonForm,ShellApi,
  httpsend, synautil, nativexml, IniFiles,
  cxLookAndFeelPainters, dxRibbonSkins, dxRibbonCustomizationForm, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, Data.DB, cxDBData, cxImageComboBox, cxCheckBox, cxTextEdit, cxDBLookupComboBox, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async,
  FireDAC.DApt, FireDAC.UI.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Phys, FireDAC.Phys.MSSQL, FireDAC.Phys.MSSQLDef,
  FireDAC.VCLUI.Wait, FireDAC.Comp.UI, FireDAC.Phys.ODBCBase, FireDAC.Comp.Client, Vcl.Menus, FireDAC.Comp.DataSet, Vcl.ImgList,
  System.Actions, Vcl.ActnList, dxBar, cxBarEditItem, dxBarExtItems, cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxClasses, cxGridCustomView, cxGrid, dxRibbon;

type
  TfmMCrystDoc = class(TfmCustomChildForm)
    dxRibbon1: TdxRibbon;
    dxRibbon1Tab1: TdxRibbonTab;
    dxBarManager1: TdxBarManager;
    bmApplyDate: TdxBar;
    bmClose: TdxBar;
    btnDone: TdxBarLargeButton;
    btnClose: TdxBarLargeButton;
    deDateBeg: TdxBarDateCombo;
    deDateEnd: TdxBarDateCombo;
    dxBarGroup1: TdxBarGroup;
    ActionList1: TActionList;
    acRefresh: TAction;
    acClose: TAction;
    acSelectDate: TAction;
    quMCDocsIn_HD: TFDQuery;
    dsquMCDocsIn_HD: TDataSource;
    cbItem1: TcxBarEditItem;
    acDecodeTovar: TAction;
    PopupMenu1: TPopupMenu;
    acSaveToFile: TAction;
    acDelList: TAction;
    acDecodeTTN: TAction;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarSubItem1: TdxBarSubItem;
    acCards: TAction;
    acSendActR: TAction;
    acDecodeWB: TAction;
    GridMCrystalDoc: TcxGrid;
    ViewMCrystalDoc: TcxGridDBTableView;
    LevelMCrystalDoc: TcxGridLevel;
    acUTMExch: TAction;
    dxBarLargeButton2: TdxBarLargeButton;
    quMCDocsIn_HDID: TIntegerField;
    quMCDocsIn_HDIdActP: TStringField;
    quMCDocsIn_HDOrderNumber: TIntegerField;
    quMCDocsIn_HDDepart: TSmallintField;
    quMCDocsIn_HDNumber: TStringField;
    quMCDocsIn_HDDateInvoice: TSQLTimeStampField;
    quMCDocsIn_HDName: TStringField;
    quMCDocsIn_HDIACTIVE: TSmallintField;
    quMCDocsIn_HDSCFNumber: TStringField;
    quMCDocsIn_HDSummaTovarPost: TFloatField;
    quMCDocsIn_HDSummaTovar: TFloatField;
    quMCDocsIn_HDSummaTara: TFloatField;
    quMCDocsIn_HDAcStatus: TByteField;
    quMCDocsIn_HDProvodType: TByteField;
    quMCDocsIn_HDICLICB: TIntegerField;
    quMCDocsIn_HDFullName: TStringField;
    quMCDocsIn_HDEgais: TSmallintField;
    ViewMCrystalDocID: TcxGridDBColumn;
    ViewMCrystalDocIdActP: TcxGridDBColumn;
    ViewMCrystalDocOrderNumber: TcxGridDBColumn;
    ViewMCrystalDocDepart: TcxGridDBColumn;
    ViewMCrystalDocNumber: TcxGridDBColumn;
    ViewMCrystalDocDateInvoice: TcxGridDBColumn;
    ViewMCrystalDocName: TcxGridDBColumn;
    ViewMCrystalDocIACTIVE: TcxGridDBColumn;
    ViewMCrystalDocSCFNumber: TcxGridDBColumn;
    ViewMCrystalDocSummaTovarPost: TcxGridDBColumn;
    ViewMCrystalDocSummaTovar: TcxGridDBColumn;
    ViewMCrystalDocSummaTara: TcxGridDBColumn;
    ViewMCrystalDocAcStatus: TcxGridDBColumn;
    ViewMCrystalDocProvodType: TcxGridDBColumn;
    ViewMCrystalDocICLICB: TcxGridDBColumn;
    ViewMCrystalDocFullName: TcxGridDBColumn;
    ViewMCrystalDocEgais: TcxGridDBColumn;
    quMCDocsIn_HDDepName: TStringField;
    ViewMCrystalDocDepName: TcxGridDBColumn;
    LevelMCrystalSpec: TcxGridLevel;
    ViewMCrystalSpec: TcxGridDBTableView;
    quMCDocsIn_SP: TFDQuery;
    dsquMCDocsIn_SP: TDataSource;
    quMCDocsIn_SPIdH: TIntegerField;
    quMCDocsIn_SPID: TIntegerField;
    quMCDocsIn_SPCodeTovar: TIntegerField;
    quMCDocsIn_SPCodeEdIzm: TSmallintField;
    quMCDocsIn_SPKol: TFloatField;
    quMCDocsIn_SPCenaTovar: TFloatField;
    quMCDocsIn_SPSumCenaTovarPost: TFloatField;
    quMCDocsIn_SPNewCenaTovar: TFloatField;
    quMCDocsIn_SPSumCenaTovar: TFloatField;
    quMCDocsIn_SPBarCode: TStringField;
    ViewMCrystalSpecID: TcxGridDBColumn;
    ViewMCrystalSpecCodeTovar: TcxGridDBColumn;
    quMCDocsIn_SPIDSP: TLargeintField;
    FDConnectShop: TFDConnection;
    FDTrans: TFDTransaction;
    FDTransUpdate: TFDTransaction;
    FDPhysMSSQLDriverLink: TFDPhysMSSQLDriverLink;
    FDGUIxWaitCursor: TFDGUIxWaitCursor;
    quMCDocsIn_SPName: TStringField;
    quMCDocsIn_SPFullName: TStringField;
    ViewMCrystalSpecIdH: TcxGridDBColumn;
    ViewMCrystalSpecName: TcxGridDBColumn;
    ViewMCrystalSpecCodeEdIzm: TcxGridDBColumn;
    ViewMCrystalSpecKol: TcxGridDBColumn;
    ViewMCrystalSpecCenaTovar: TcxGridDBColumn;
    ViewMCrystalSpecSumCenaTovarPost: TcxGridDBColumn;
    ViewMCrystalSpecNewCenaTovar: TcxGridDBColumn;
    ViewMCrystalSpecSumCenaTovar: TcxGridDBColumn;
    ViewMCrystalSpecFullName: TcxGridDBColumn;
    ViewMCrystalSpecBarCode: TcxGridDBColumn;
    quMCDocsIn_HDNUMBERRAR: TStringField;
    quMCDocsIn_HDSID: TStringField;
    quMCDocsIn_HDSHIPDATE: TStringField;
    ViewMCrystalDocNUMBERRAR: TcxGridDBColumn;
    ViewMCrystalDocSID: TcxGridDBColumn;
    ViewMCrystalDocSHIPDATE: TcxGridDBColumn;
    brSelect: TdxBar;
    dxBarLargeButton3: TdxBarLargeButton;
    acSelect: TAction;
    procedure acRefreshExecute(Sender: TObject);
    procedure acCloseExecute(Sender: TObject);
    procedure acSelectDateExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure deDateBegChange(Sender: TObject);
    procedure ViewMCrystalDocDblClick(Sender: TObject);
    procedure acSelectExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    FISHOP:Integer;
    FNAMESHOP:string;
    FDATEB,FDATEE: TDateTime;
  public
    { Public declarations }
    function DBConnect(Server,Database,User_Name,Password:String): boolean;
    procedure Init;
  end;


procedure prShowMCDocs(ISHOP:Integer; NAMESHOP:string; DateB,DateE:TDateTime;Server,Database,User_Name,Password:String);
function prSelectMCDocs(ISHOP:Integer; NAMESHOP:string; DateB,DateE:TDateTime;Server,Database,User_Name,Password:String; var IDHEAD: Integer):Boolean;

//  var  fmMCrystDoc: TfmMCrystDoc;

implementation

{$R *.dfm}

uses ViewXML, EgaisDecode, ViewCards, dmRar, ViewRar, SpecMC;

procedure prShowMCDocs(ISHOP:Integer; NAMESHOP:string; DateB,DateE:TDateTime;Server,Database,User_Name,Password:String);
var F:TfmMCrystDoc;
begin
  F:=TfmMCrystDoc.Create(Application);
//  ShowMessage(Xmlstr);
  F.brSelect.Visible:=False;
  F.FISHOP:=ISHOP;
  F.FNAMESHOP:=NAMESHOP;
  F.FDATEB:=DateB;
  F.FDATEE:=DateE;

  F.deDateBeg.Date:=DateB;
  F.deDateEnd.Date:=DateE;

  if F.DBConnect(Server,Database,User_Name,Password) then
  begin
    F.Init;
    F.Show;
  end;
end;

function prSelectMCDocs(ISHOP:Integer; NAMESHOP:string; DateB,DateE:TDateTime;Server,Database,User_Name,Password:String; var IDHEAD: Integer):Boolean;
var F:TfmMCrystDoc;
begin
  Result:=False;
  IDHEAD:=0;
  F:=TfmMCrystDoc.Create(Application);
  try
    F.brSelect.Visible:=True;
    F.FISHOP:=ISHOP;
    F.FNAMESHOP:=NAMESHOP;
    F.FDATEB:=DateB;
    F.FDATEE:=DateE;

    F.deDateBeg.Date:=DateB;
    F.deDateEnd.Date:=DateE;

    if F.DBConnect(Server,Database,User_Name,Password) then
    begin
      F.Init;
      //��������� ������
      F.ViewMCrystalDoc.DataController.Filter.BeginUpdate;
        F.ViewMCrystalDoc.DataController.Filter.Root.Clear;
      F.ViewMCrystalDoc.DataController.Filter.Root.BoolOperatorKind := fboOr;
      F.ViewMCrystalDoc.DataController.Filter.Root.AddItem(F.ViewMCrystalDocNUMBERRAR, foEqual, null, 'Null' );
      F.ViewMCrystalDoc.DataController.Filter.Root.AddItem(F.ViewMCrystalDocNUMBERRAR, foEqual, '0', '0' );
      F.ViewMCrystalDoc.DataController.Filter.Active:=True;
      F.ViewMCrystalDoc.DataController.Filter.EndUpdate;

      if F.ShowModal=mrOk then begin
        IDHEAD:=F.quMCDocsIn_HDID.AsInteger;
        Result:=True;
      end;
    end;
  finally
    F.Free;
  end;
end;

function  TfmMCrystDoc.DBConnect(Server,Database,User_Name,Password:String): boolean;
var CurDir : string;
begin
  Result:=False;
  //����� �� ��������� ����� ���� � ����������� ���������� MSSQL � FireDAC
  //c������ ������ ������������ ����� udl �� ADO
  CurDir := ExtractFilePath(ParamStr(0));
  try

    FDConnectShop.Close;
    FDConnectShop.Params.Values['SERVER']:=Server;
    FDConnectShop.Params.Values['DATABASE']:=Database;
    FDConnectShop.Params.Values['User_Name']:=User_Name;
    FDConnectShop.Params.Values['Password']:=Password;
    FDConnectShop.Params.Values['ApplicationName']:=CommonSet.AppName;
    FDConnectShop.Params.Values['Workstation']:='';
    //FDConnection.Params.Values['MARS']:='yes';
    FDConnectShop.Params.Values['DriverID']:='MSSQL';
    FDConnectShop.Connected:=True;
   finally
  end;

  Sleep(100);
  Result:=FDConnectShop.Connected;
end;


procedure TfmMCrystDoc.Init;
var
  FocusedRow, TopRow: Integer;
  flag: boolean;
begin
  if FDConnectShop.Connected then
  begin
    Caption:=FNAMESHOP+' ( '+its(FISHOP)+' ) c '+ds1(deDateBeg.Date)+' �� '+ds1(deDateEnd.Date);

    try
      //<--Refresh � ��������������� ������� ������� � �������
      flag:=quMCDocsIn_HD.Active;  //���������� ���� �� ������� �������, ��� �������������� ������� �������
      TopRow := ViewMCrystalDoc.Controller.TopRowIndex;
      FocusedRow := ViewMCrystalDoc.DataController.FocusedRowIndex;
      //-->
      ViewMCrystalDoc.BeginUpdate;
      ViewMCrystalSpec.BeginUpdate;

      quMCDocsIn_HD.Active:=False;
      quMCDocsIn_HD.ParamByName('DATEB').AsDateTime:=Trunc(deDateBeg.Date);
      quMCDocsIn_HD.ParamByName('DATEE').AsDateTime:=Trunc(deDateEnd.Date);
      quMCDocsIn_HD.Active:=True;
      quMCDocsIn_HD.First;

      quMCDocsIn_SP.Active:=False;
      quMCDocsIn_SP.ParamByName('DATEB').AsDateTime:=Trunc(deDateBeg.Date);
      quMCDocsIn_SP.ParamByName('DATEE').AsDateTime:=Trunc(deDateEnd.Date);
      quMCDocsIn_SP.Active:=True;
    finally
      ViewMCrystalSpec.EndUpdate;
      ViewMCrystalDoc.EndUpdate;
      //<--��������������� �������
      if flag then begin
        ViewMCrystalDoc.DataController.FocusedRowIndex := FocusedRow;
        ViewMCrystalDoc.Controller.TopRowIndex := TopRow;
      end;
      //-->
    end;
  end;

  {
  if dmR.FDConnection.Connected then
  begin
    Caption:=FNAMESHOP+' ( '+its(FISHOP)+' ) c '+ds1(deDateBeg.Date)+' �� '+ds1(deDateEnd.Date);

    ViewMCrystalDoc.BeginUpdate;
    ViewMCrystalSpec.BeginUpdate;

    quMCDocsIn_HD.Active:=False;
    quMCDocsIn_HD.ParamByName('DATEB').AsDateTime:=Trunc(deDateBeg.Date);
    quMCDocsIn_HD.ParamByName('DATEE').AsDateTime:=Trunc(deDateEnd.Date);
    quMCDocsIn_HD.Active:=True;
    quMCDocsIn_HD.First;

    quMCDocsIn_SP.Active:=False;
    quMCDocsIn_SP.ParamByName('DATEB').AsDateTime:=Trunc(deDateBeg.Date);
    quMCDocsIn_SP.ParamByName('DATEE').AsDateTime:=Trunc(deDateEnd.Date);
    quMCDocsIn_SP.Active:=True;

    ViewMCrystalSpec.EndUpdate;
    ViewMCrystalDoc.EndUpdate;
  end;}
end;


procedure TfmMCrystDoc.ViewMCrystalDocDblClick(Sender: TObject);
var F: TfmSpecMC;
begin
  if dmR.FDConnection.Connected then
  begin
    if quMCDocsIn_HD.RecordCount>0 then
    begin
      F:=TfmSpecMC.Create(Self);
      F.quSpecMC.Active:=False;
      F.quSpecMC.ParamByName('IDH').Value:=quMCDocsIn_HDID.AsInteger;
      F.quSpecMC.Active:=True;
      F.Show;
    end;
  end;
end;

procedure TfmMCrystDoc.acCloseExecute(Sender: TObject);
begin
  //�������
  Close;
end;

procedure TfmMCrystDoc.acRefreshExecute(Sender: TObject);
begin
  //������
  Init;
end;

procedure TfmMCrystDoc.acSelectDateExecute(Sender: TObject);
begin
  //��������
  Init;
end;

procedure TfmMCrystDoc.acSelectExecute(Sender: TObject);
begin
  ModalResult:=mrOk;
end;

procedure TfmMCrystDoc.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
end;

procedure TfmMCrystDoc.FormCreate(Sender: TObject);
begin
  deDateBeg.Date:=Date-1;
  deDateEnd.Date:=Date;
end;

procedure TfmMCrystDoc.FormShow(Sender: TObject);
begin
  dxRibbon1.ShowTabHeaders:=False;
  with dmR do
  begin
    if FDConnection.Connected then
    begin
  //    ShowMessage('���� ��');
      Init;
    end else
    begin
      ShowMessage('������ �������� ��');
      Close;
    end;
  end;
end;


procedure TfmMCrystDoc.deDateBegChange(Sender: TObject);
begin
  Init;
end;

end.
