object fmAddPosCorr: TfmAddPosCorr
  Left = 0
  Top = 0
  Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1089#1090#1088#1086#1082#1091
  ClientHeight = 266
  ClientWidth = 516
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 19
    Width = 30
    Height = 13
    Caption = #1058#1086#1074#1072#1088
  end
  object Label4: TLabel
    Left = 18
    Top = 149
    Width = 60
    Height = 13
    Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086
  end
  object Panel1: TPanel
    Left = 0
    Top = 190
    Width = 516
    Height = 76
    Align = alBottom
    BevelInner = bvLowered
    BevelKind = bkFlat
    TabOrder = 0
    ExplicitTop = 236
    object Button1: TButton
      Left = 88
      Top = 16
      Width = 113
      Height = 41
      Caption = #1054#1082
      ModalResult = 1
      TabOrder = 0
    end
    object Button2: TButton
      Left = 296
      Top = 16
      Width = 113
      Height = 41
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
    end
  end
  object cxCalcEdit1: TcxCalcEdit
    Left = 106
    Top = 145
    EditValue = 0
    TabOrder = 1
    Width = 145
  end
  object cxButtonEdit1: TcxButtonEdit
    Left = 72
    Top = 16
    Properties.Buttons = <
      item
        Default = True
        Kind = bkEllipsis
      end>
    TabOrder = 2
    Text = 'cxButtonEdit1'
    Width = 409
  end
  object cxMemo1: TcxMemo
    Left = 16
    Top = 48
    Lines.Strings = (
      'cxMemo1')
    TabOrder = 3
    Height = 73
    Width = 465
  end
  object quSelCard: TFDQuery
    Connection = dmR.FDConnection
    SQL.Strings = (
      'SELECT [KAP]'
      '      ,cast([NAME] as varchar(200)) as NAME'
      '      ,[VOL]'
      '      ,[KREP]'
      '      ,[AVID]'
      '      ,[PRODID]'
      '      ,[IMPORTERID]'
      '  FROM [dbo].[ACARDS]'
      '  where [KAP]=:CODE')
    Left = 384
    Top = 91
    ParamData = <
      item
        Name = 'CODE'
        DataType = ftString
        ParamType = ptInput
        Size = 20
        Value = '0003477000001836431'
      end>
    object quSelCardKAP: TStringField
      FieldName = 'KAP'
      Origin = 'KAP'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 50
    end
    object quSelCardNAME: TStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      ReadOnly = True
      Size = 200
    end
    object quSelCardVOL: TSingleField
      FieldName = 'VOL'
      Origin = 'VOL'
    end
    object quSelCardKREP: TSingleField
      FieldName = 'KREP'
      Origin = 'KREP'
    end
    object quSelCardAVID: TIntegerField
      FieldName = 'AVID'
      Origin = 'AVID'
    end
    object quSelCardPRODID: TStringField
      FieldName = 'PRODID'
      Origin = 'PRODID'
      Size = 50
    end
    object quSelCardIMPORTERID: TStringField
      FieldName = 'IMPORTERID'
      Origin = 'IMPORTERID'
      Size = 50
    end
  end
end
