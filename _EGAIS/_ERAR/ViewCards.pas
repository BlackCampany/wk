unit ViewCards;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  CustomChildForm, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid,
  System.Actions, Vcl.ActnList, Vcl.PlatformDefaultStyleActnCtrls, Vcl.ActnMan, dxRibbonSkins, dxRibbonCustomizationForm, dxRibbon, dxBar, cxTextEdit, cxBarEditItem,
  cxDBLookupComboBox, Vcl.Menus;

type
  TfmACard = class(TfmCustomChildForm)
    ViewCards: TcxGridDBTableView;
    LevelCards: TcxGridLevel;
    GridCards: TcxGrid;
    quCards: TFDQuery;
    dsquCards: TDataSource;
    quCardsKAP: TStringField;
    quCardsNAME: TMemoField;
    quCardsVOL: TSingleField;
    quCardsKREP: TSingleField;
    quCardsAVID: TIntegerField;
    quCardsPRODID: TStringField;
    quCardsIMPORTERID: TStringField;
    quCardsPRONAME: TMemoField;
    quCardsPRODINN: TStringField;
    quCardsPRODKPP: TStringField;
    quCardsIMPNAME: TMemoField;
    quCardsIMPORTERINN: TStringField;
    quCardsIMPORTERKPP: TStringField;
    ViewCardsKAP: TcxGridDBColumn;
    ViewCardsNAME: TcxGridDBColumn;
    ViewCardsVOL: TcxGridDBColumn;
    ViewCardsKREP: TcxGridDBColumn;
    ViewCardsAVID: TcxGridDBColumn;
    ViewCardsPRODID: TcxGridDBColumn;
    ViewCardsIMPORTERID: TcxGridDBColumn;
    ViewCardsPRONAME: TcxGridDBColumn;
    ViewCardsPRODINN: TcxGridDBColumn;
    ViewCardsPRODKPP: TcxGridDBColumn;
    ViewCardsIMPNAME: TcxGridDBColumn;
    ViewCardsIMPORTERINN: TcxGridDBColumn;
    ViewCardsIMPORTERKPP: TcxGridDBColumn;
    ActionManager1: TActionManager;
    acClose: TAction;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarGroup1: TdxBarGroup;
    dxRibbon1: TdxRibbon;
    dxRibbon1Tab1: TdxRibbonTab;
    quCardsIdGoods: TIntegerField;
    ViewCardsIdGoods: TcxGridDBColumn;
    acRefresh: TAction;
    dxBarManager1Bar2: TdxBar;
    cxBarEditItem1: TcxBarEditItem;
    dxBarButton1: TdxBarButton;
    acGetKAPInfo: TAction;
    acRecalcKap: TAction;
    acSetCodeCB: TAction;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton2: TdxBarLargeButton;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acCloseExecute(Sender: TObject);
    procedure acRefreshExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure acGetKAPInfoExecute(Sender: TObject);
    procedure acRecalcKapExecute(Sender: TObject);
    procedure acSetCodeCBExecute(Sender: TObject);
    procedure ViewCardsDblClick(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
    STYPE:string;
    FRARID:string;
    FIDATE: Integer;
    FSID:string;
    quSpec:TFDQuery;
    procedure Init;
  end;

var
  fmACard: TfmACard;

implementation

{$R *.dfm}

uses ViewRar, dmRar, UnitFunction, EgaisDecode, SetCodeCB;

procedure TfmACard.acCloseExecute(Sender: TObject);
begin
  FRARID:='';
  FIDATE:=0;
  FSID:='';
  STYPE:='';
  quSpec:=nil;

  Close;
end;

procedure TfmACard.acGetKAPInfoExecute(Sender: TObject);
var sKap:string;
begin
  //��������� ���������� �� ���
  sKap:=Trim(cxBarEditItem1.EditValue);
  if length(sKap)>10 then
  begin
    ClearMessageLogLocal;
    ShowMessageLogLocal('����� ... ���� ������������ �������.');
    prSendKAP(CommonSet.FSRAR_ID,sKap,MemoLogLocal);
    ShowMessageLogLocal('������� ��������.');
  end;
end;

procedure TfmACard.acRecalcKapExecute(Sender: TObject);
begin
  with dmR do
  begin


  end;
end;

procedure TfmACard.acRefreshExecute(Sender: TObject);
begin
  Init;
end;

procedure TfmACard.acSetCodeCBExecute(Sender: TObject);
begin
  //����������� ��� ��
  with dmR do
  begin
    if quCards.RecordCount>0 then
    begin
      try
        fmSetCodeCB:=tfmSetCodeCB.Create(application);
        fmSetCodeCB.Label4.Caption:=quCardsKAP.AsString;
        fmSetCodeCB.Label5.Caption:=quCardsNAME.AsString;
        fmSetCodeCB.cxCalcEdit1.Value:=quCardsIdGoods.AsInteger;
        fmSetCodeCB.ShowModal;
        if fmSetCodeCB.ModalResult=mrOk then
        begin
          quS.Active:=False;
          quS.SQL.Clear;
          quS.SQL.Add('');
          quS.SQL.Add('UPDATE [dbo].[ACARDS] SET [ICODECB] = '+its(Trunc(fmSetCodeCB.cxCalcEdit1.Value))) ;
          quS.SQL.Add('WHERE [KAP] = '''+quCardsKAP.AsString+'''');
          quS.ExecSQL;

          Delay(100);
          Init;
        end;
      finally
        fmSetCodeCB.Free;
      end;
    end;
  end;
end;

procedure TfmACard.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewCards.StoreToIniFile(GetGridIniFile,False,[gsoUseSummary,gsoUseFilter]);
  Action:=caFree;
  fmACard:=Nil;
end;

procedure TfmACard.FormCreate(Sender: TObject);
begin
  ViewCards.RestoreFromIniFile(GetGridIniFile);
end;

procedure TfmACard.Init;
var
  FocusedRow, TopRow: Integer;
  flag: boolean;
begin
  if dmR.FDConnection.Connected then
  begin
    try
      ViewCards.BeginUpdate;
          //<--Refresh � ��������������� ������� ������� � �������
      flag:=quCards.Active;  //���������� ���� �� ������� �������, ��� �������������� ������� �������
      TopRow := ViewCards.Controller.TopRowIndex;
      FocusedRow := ViewCards.DataController.FocusedRowIndex;

      quCards.Active:=False;
      quCards.Active:=True;

    finally
      ViewCards.EndUpdate;
      //<--��������������� �������
      if flag then begin
        ViewCards.DataController.FocusedRowIndex := FocusedRow;
        ViewCards.Controller.TopRowIndex := TopRow;
      end;
    end;
  end;
end;


procedure TfmACard.ViewCardsDblClick(Sender: TObject);
begin
  if STYPE='CORR' then
  begin
    if Assigned(quSpec) then
    begin
      if quSpec.Active then
      begin
        quSpec.Append;
        quSpec.FieldByName('FSRARID').AsString:=FRARID;
        quSpec.FieldByName('IDATE').AsInteger:=FIDATE;
        quSpec.FieldByName('SIDHD').AsString:=FSID;
        quSpec.FieldByName('NUM').AsInteger:=quSpec.RecordCount+1;
        quSpec.FieldByName('ALCCODE').AsString:=quCardsKAP.AsString;
        quSpec.FieldByName('QUANT').AsFloat:=0;
        quSpec.FieldByName('NAME').AsString:=quCardsNAME.AsString;
        quSpec.FieldByName('VOL').AsFloat:=quCardsVOL.AsFloat;
        quSpec.FieldByName('KREP').AsFloat:=quCardsKREP.AsFloat;
        quSpec.FieldByName('AVID').AsInteger:=quCardsAVID.AsInteger;
        quSpec.Post;
      end;
    end;
  end;
end;

end.
