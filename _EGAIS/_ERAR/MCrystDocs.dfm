object fmMCrystDoc: TfmMCrystDoc
  Left = 0
  Top = 0
  Caption = #1044#1086#1082#1091#1084#1077#1085#1090#1099' '#1052#1050#1088#1080#1089#1090#1072#1083#1072
  ClientHeight = 549
  ClientWidth = 1112
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1112
    Height = 127
    ApplicationButton.Visible = False
    BarManager = dxBarManager1
    Style = rs2010
    ColorSchemeAccent = rcsaOrange
    ColorSchemeName = 'Blue'
    SupportNonClientDrawing = True
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1044#1086#1082#1091#1084#1077#1085#1090#1099' '#1052#1050#1088#1080#1089#1090#1072#1083#1072
      Groups = <
        item
          Caption = #1042#1099#1073#1088#1072#1090#1100
          ToolbarName = 'brSelect'
        end
        item
          ToolbarName = 'bmApplyDate'
        end
        item
          ToolbarName = 'bmClose'
        end>
      Index = 0
    end
  end
  object GridMCrystalDoc: TcxGrid
    Left = 0
    Top = 127
    Width = 1112
    Height = 422
    Align = alClient
    TabOrder = 5
    LevelTabs.Style = 8
    LookAndFeel.Kind = lfOffice11
    LookAndFeel.NativeStyle = False
    RootLevelOptions.DetailTabsPosition = dtpTop
    object ViewMCrystalDoc: TcxGridDBTableView
      PopupMenu = PopupMenu1
      OnDblClick = ViewMCrystalDocDblClick
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.Last.Visible = True
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.InfoPanel.Visible = True
      Navigator.Visible = True
      DataController.DataSource = dsquMCDocsIn_HD
      DataController.KeyFieldNames = 'ID'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      Images = dmR.SmallImage
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.MultiSelect = True
      OptionsView.ExpandButtonsForEmptyDetails = False
      OptionsView.GroupByBox = False
      object ViewMCrystalDocID: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1076#1086#1082#1091#1084#1077#1085#1090#1072' '#1052#1050#1088#1080#1089#1090#1072#1083
        DataBinding.FieldName = 'ID'
      end
      object ViewMCrystalDocSID: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1076#1086#1082#1091#1084#1077#1085#1090#1072' '#1045#1043#1040#1048#1057' (SID)'
        DataBinding.FieldName = 'SID'
        Width = 171
      end
      object ViewMCrystalDocIdActP: TcxGridDBColumn
        Caption = #1059#1085#1080#1082#1072#1083#1100#1085#1099#1081' '#1082#1086#1076' '#1076#1086#1082#1091#1084#1077#1085#1090#1072' '#1074' '#1089#1077#1090#1080
        DataBinding.FieldName = 'IdActP'
        Width = 60
      end
      object ViewMCrystalDocOrderNumber: TcxGridDBColumn
        Caption = #1053#1086#1084#1077#1088' '#1079#1072#1082#1072#1079#1072
        DataBinding.FieldName = 'OrderNumber'
      end
      object ViewMCrystalDocDepart: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1086#1090#1076#1077#1083#1072
        DataBinding.FieldName = 'Depart'
      end
      object ViewMCrystalDocDepName: TcxGridDBColumn
        Caption = #1054#1090#1076#1077#1083
        DataBinding.FieldName = 'DepName'
        Width = 123
      end
      object ViewMCrystalDocNumber: TcxGridDBColumn
        Caption = #1053#1086#1084#1077#1088' '#1076#1086#1082'.'
        DataBinding.FieldName = 'Number'
        Width = 76
      end
      object ViewMCrystalDocNUMBERRAR: TcxGridDBColumn
        Caption = #1053#1086#1084#1077#1088' '#1074' '#1045#1043#1040#1048#1057
        DataBinding.FieldName = 'NUMBERRAR'
        Width = 143
      end
      object ViewMCrystalDocDateInvoice: TcxGridDBColumn
        Caption = #1044#1072#1090#1072' '#1076#1086#1082'.'
        DataBinding.FieldName = 'DateInvoice'
        Width = 92
      end
      object ViewMCrystalDocSHIPDATE: TcxGridDBColumn
        Caption = #1044#1072#1090#1072' '#1087#1086#1089#1090#1072#1074#1082#1080' '#1045#1043#1040#1048#1057
        DataBinding.FieldName = 'SHIPDATE'
      end
      object ViewMCrystalDocName: TcxGridDBColumn
        Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082' '
        DataBinding.FieldName = 'Name'
      end
      object ViewMCrystalDocIACTIVE: TcxGridDBColumn
        Caption = #1057#1090#1072#1090#1091#1089' '#1086#1090#1087#1088#1072#1074#1082#1080' '#1076#1086#1082'. '#1074' '#1056#1040#1056
        DataBinding.FieldName = 'IACTIVE'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Images = dmR.SmallImage
        Properties.Items = <
          item
            Description = #1074' '#1088#1077#1076#1072#1082#1090'.'
            ImageIndex = 74
            Value = 0
          end
          item
            Description = #1054#1090#1087#1088#1072#1074#1083#1077#1085
            ImageIndex = 75
            Value = 1
          end
          item
            Description = #1053#1045' '#1055#1054#1051#1059#1063#1045#1053
            ImageIndex = 4
            Value = 99
          end>
      end
      object ViewMCrystalDocSCFNumber: TcxGridDBColumn
        Caption = #1053#1086#1084#1077#1088' '#1057#1063#1060#1050
        DataBinding.FieldName = 'SCFNumber'
      end
      object ViewMCrystalDocSummaTovarPost: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
        DataBinding.FieldName = 'SummaTovarPost'
      end
      object ViewMCrystalDocSummaTovar: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1084#1072#1075#1072#1079#1080#1085#1072
        DataBinding.FieldName = 'SummaTovar'
      end
      object ViewMCrystalDocSummaTara: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1087#1086' '#1090#1072#1088#1077
        DataBinding.FieldName = 'SummaTara'
      end
      object ViewMCrystalDocAcStatus: TcxGridDBColumn
        Caption = #1057#1090#1072#1090#1091#1089' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
        DataBinding.FieldName = 'AcStatus'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Images = dmR.SmallImage
        Properties.Items = <
          item
            ImageIndex = 154
            Value = 0
          end
          item
            ImageIndex = 1
            Value = 3
          end>
      end
      object ViewMCrystalDocProvodType: TcxGridDBColumn
        Caption = #1058#1080#1087' '#1087#1088#1086#1074#1086#1076#1082#1080
        DataBinding.FieldName = 'ProvodType'
      end
      object ViewMCrystalDocICLICB: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
        DataBinding.FieldName = 'ICLICB'
      end
      object ViewMCrystalDocFullName: TcxGridDBColumn
        Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082' '#1087#1086#1083#1085#1086#1077' '#1085#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'FullName'
        Width = 229
      end
      object ViewMCrystalDocEgais: TcxGridDBColumn
        DataBinding.FieldName = 'Egais'
        Visible = False
      end
    end
    object ViewMCrystalSpec: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      DataController.DataSource = dsquMCDocsIn_SP
      DataController.DetailKeyFieldNames = 'IdH'
      DataController.MasterKeyFieldNames = 'ID'
      DataController.Options = [dcoAssignGroupingValues]
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0.000'
          Kind = skSum
          FieldName = 'Kol'
          Column = ViewMCrystalSpecKol
        end
        item
          Format = #1050#1086#1083'-'#1074#1086': ###'
          Kind = skCount
          FieldName = 'ID'
          Column = ViewMCrystalSpecID
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.ExpandButtonsForEmptyDetails = False
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      object ViewMCrystalSpecID: TcxGridDBColumn
        Caption = #8470' '#1087#1087
        DataBinding.FieldName = 'ID'
      end
      object ViewMCrystalSpecCodeTovar: TcxGridDBColumn
        Caption = #1050#1086#1076' '#1090#1086#1074#1072#1088#1072
        DataBinding.FieldName = 'CodeTovar'
      end
      object ViewMCrystalSpecName: TcxGridDBColumn
        Caption = #1058#1086#1074#1072#1088' '#1085#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'Name'
      end
      object ViewMCrystalSpecCodeEdIzm: TcxGridDBColumn
        Caption = #1077#1076'.'#1080#1079#1084'.'
        DataBinding.FieldName = 'CodeEdIzm'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Items = <
          item
            Description = #1096#1090'.'
            ImageIndex = 0
            Value = 1
          end
          item
            Description = #1082#1075'.'
            Value = 2
          end>
      end
      object ViewMCrystalSpecKol: TcxGridDBColumn
        Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086
        DataBinding.FieldName = 'Kol'
        Styles.Content = dmR.stlBold
      end
      object ViewMCrystalSpecCenaTovar: TcxGridDBColumn
        Caption = #1062#1077#1085#1072' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
        DataBinding.FieldName = 'CenaTovar'
      end
      object ViewMCrystalSpecSumCenaTovarPost: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
        DataBinding.FieldName = 'SumCenaTovarPost'
      end
      object ViewMCrystalSpecNewCenaTovar: TcxGridDBColumn
        Caption = #1062#1077#1085#1072' '#1084#1072#1075#1072#1079#1080#1085#1072
        DataBinding.FieldName = 'NewCenaTovar'
      end
      object ViewMCrystalSpecSumCenaTovar: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1084#1072#1075#1072#1079#1080#1085#1072
        DataBinding.FieldName = 'SumCenaTovar'
      end
      object ViewMCrystalSpecFullName: TcxGridDBColumn
        Caption = #1058#1086#1074#1072#1088' '#1087#1086#1083#1085#1086#1077' '#1085#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'FullName'
      end
      object ViewMCrystalSpecBarCode: TcxGridDBColumn
        Caption = #1064#1058#1056#1048#1061#1050#1054#1044
        DataBinding.FieldName = 'BarCode'
      end
      object ViewMCrystalSpecIdH: TcxGridDBColumn
        DataBinding.FieldName = 'IdH'
        Visible = False
      end
    end
    object LevelMCrystalDoc: TcxGridLevel
      Caption = #1042#1093#1086#1076#1103#1097#1080#1077
      GridView = ViewMCrystalDoc
      Options.TabsForEmptyDetails = False
      object LevelMCrystalSpec: TcxGridLevel
        GridView = ViewMCrystalSpec
        Options.DetailTabsPosition = dtpLeft
        Options.TabsForEmptyDetails = False
      end
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      #1044#1077#1081#1089#1090#1074#1080#1103)
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmR.SmallImage
    ImageOptions.LargeImages = dmR.LargeImage
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 788
    Top = 52
    DockControlHeights = (
      0
      0
      0
      0)
    object bmApplyDate: TdxBar
      Caption = #1042#1099#1073#1086#1088' '#1087#1077#1088#1080#1086#1076#1072
      CaptionButtons = <>
      DockedLeft = 79
      DockedTop = 0
      FloatLeft = 778
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          UserDefine = [udWidth]
          UserWidth = 75
          Visible = True
          ItemName = 'deDateBeg'
        end
        item
          UserDefine = [udWidth]
          UserWidth = 74
          Visible = True
          ItemName = 'deDateEnd'
        end
        item
          Visible = True
          ItemName = 'btnDone'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object bmClose: TdxBar
      Caption = #1047#1072#1082#1088#1099#1090#1100
      CaptionButtons = <>
      DockedLeft = 277
      DockedTop = 0
      FloatLeft = 778
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'btnClose'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object brSelect: TdxBar
      Caption = #1042#1099#1073#1088#1072#1090#1100
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 1146
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object btnDone: TdxBarLargeButton
      Action = acRefresh
      Category = 0
    end
    object btnClose: TdxBarLargeButton
      Action = acClose
      Category = 0
    end
    object deDateBeg: TdxBarDateCombo
      Caption = 'C  '
      Category = 0
      Hint = 'C  '
      Visible = ivAlways
      OnChange = deDateBegChange
      ImageIndex = 25
      ShowDayText = False
    end
    object deDateEnd: TdxBarDateCombo
      Caption = #1087#1086
      Category = 0
      Hint = #1087#1086
      Visible = ivAlways
      OnChange = deDateBegChange
      ImageIndex = 25
      ShowDayText = False
    end
    object cbItem1: TcxBarEditItem
      Caption = 'XMLNotePad'
      Category = 0
      Hint = 'XMLNotePad'
      Visible = ivAlways
      ShowCaption = True
      Width = 0
      PropertiesClassName = 'TcxCheckBoxProperties'
      Properties.ImmediatePost = True
      InternalEditValue = False
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = acCards
      Category = 0
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = 'New SubItem'
      Category = 0
      Visible = ivAlways
      ItemLinks = <>
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = acUTMExch
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = acSelect
      Category = 0
    end
    object dxBarGroup1: TdxBarGroup
      Items = ()
    end
  end
  object ActionList1: TActionList
    Images = dmR.SmallImage
    Left = 560
    Top = 228
    object acRefresh: TAction
      Category = #1060#1086#1088#1084#1072
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100' (F5)'
      Hint = #1054#1073#1085#1086#1074#1080#1090#1100' (F5)'
      ImageIndex = 5
      ShortCut = 116
      OnExecute = acRefreshExecute
    end
    object acClose: TAction
      Category = #1060#1086#1088#1084#1072
      Caption = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 100
      OnExecute = acCloseExecute
    end
    object acSelectDate: TAction
      Category = #1060#1086#1088#1084#1072
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      Hint = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      ImageIndex = 1
      OnExecute = acSelectDateExecute
    end
    object acDecodeTovar: TAction
      Category = #1044#1077#1082#1086#1076#1080#1088#1086#1074#1072#1085#1080#1077
      Caption = #1044#1077#1082#1086#1076#1080#1088#1086#1074#1072#1090#1100' '#1082#1072#1082' '#1090#1086#1074#1072#1088
      ImageIndex = 38
    end
    object acSaveToFile: TAction
      Category = #1044#1077#1082#1086#1076#1080#1088#1086#1074#1072#1085#1080#1077
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1074' '#1092#1072#1081#1083
      ImageIndex = 7
    end
    object acDelList: TAction
      Category = #1044#1077#1082#1086#1076#1080#1088#1086#1074#1072#1085#1080#1077
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1074#1099#1076#1077#1083#1077#1085#1085#1099#1077' '#1089#1090#1088#1086#1082#1080
      ImageIndex = 4
      ShortCut = 16452
    end
    object acDecodeTTN: TAction
      Category = #1044#1077#1082#1086#1076#1080#1088#1086#1074#1072#1085#1080#1077
      Caption = #1044#1077#1082#1086#1076#1080#1088#1086#1074#1072#1090#1100' '#1082#1072#1082' '#1076#1086#1082#1091#1084#1077#1085#1090
      ImageIndex = 38
    end
    object acCards: TAction
      Category = #1060#1086#1088#1084#1072
      Caption = #1050#1072#1088#1090#1086#1095#1082#1080
      ImageIndex = 11
    end
    object acSendActR: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1057#1092#1086#1088#1084#1080#1088#1086#1074#1072#1090#1100' '#1072#1082#1090' '#1088#1072#1089#1093#1086#1078#1076#1077#1085#1080#1103
      ImageIndex = 49
    end
    object acDecodeWB: TAction
      Category = #1044#1077#1082#1086#1076#1080#1088#1086#1074#1072#1085#1080#1077
      Caption = 'acDecodeWB'
      ImageIndex = 38
    end
    object acUTMExch: TAction
      Category = #1060#1086#1088#1084#1072
      Caption = #1059#1058#1052' '#1086#1073#1084#1077#1085
      ImageIndex = 130
    end
    object acSelect: TAction
      Category = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1042#1099#1073#1088#1072#1090#1100' '#1076#1086#1082#1091#1084#1077#1085#1090
      ImageIndex = 1
      OnExecute = acSelectExecute
    end
  end
  object quMCDocsIn_HD: TFDQuery
    Connection = FDConnectShop
    SQL.Strings = (
      'declare @DATEB datetime = :DATEB'
      'declare @DATEE datetime = :DATEE'
      ''
      'SELECT hd.[ID]'
      '      ,hd.[IdActP]'
      '      ,hd.[OrderNumber]'
      '      ,hd.[Depart]'
      
        #9'  ,(dep.Name+'#39' ( '#1084#1072#1075'. '#39'+cast(dep.Id_Shop as varchar)+'#39' )'#39') as D' +
        'epName'
      '      ,hd.[Number]'
      '      ,hd.[DateInvoice]'
      #9'  ,cli.Name'
      #9'  ,isnull(rarhd.IACTIVE,99) as IACTIVE'
      '      ,rarhd.NUMBER as NUMBERRAR'
      '      ,rarhd.SID'
      '      ,rarhd.SHIPDATE'
      '      ,hd.[SCFNumber]'
      '      ,hd.[SummaTovarPost]'
      '      ,hd.[SummaTovar]'
      '      ,hd.[SummaTara]'
      '      ,hd.[AcStatus]'
      '      ,hd.[ProvodType]'
      '      ,hd.[ICLICB]'
      #9'  ,cli.FullName'
      #9'  ,cli.Egais'
      '  FROM [scrystal].[dbo].[A_TTNIn] hd'
      
        '  left join [EcrystalR].[dbo].[Clients] cli on cli.Id=hd.[ICLICB' +
        ']'
      '  left join [dbo].[ADOCSIN_HD] rarhd on rarhd.IDHEAD=hd.[ID]'
      
        '  left join [EcrystalR].[dbo].[Departs] dep on dep.Id=hd.[Depart' +
        '] and dep.IStatus=1'
      '  where hd.DateInvoice>=@DATEB'
      '  and hd.DateInvoice<(@DATEE+1)'
      '  and isnull(cli.Egais,0)>0'
      '  order by cli.Name,hd.[DateInvoice],hd.[ID]')
    Left = 64
    Top = 344
    ParamData = <
      item
        Name = 'DATEB'
        DataType = ftDateTime
        ParamType = ptInput
        Value = 42401d
      end
      item
        Name = 'DATEE'
        DataType = ftDateTime
        ParamType = ptInput
        Value = 42415d
      end>
    object quMCDocsIn_HDID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      Required = True
    end
    object quMCDocsIn_HDIdActP: TStringField
      FieldName = 'IdActP'
      Origin = 'IdActP'
      Required = True
    end
    object quMCDocsIn_HDOrderNumber: TIntegerField
      FieldName = 'OrderNumber'
      Origin = 'OrderNumber'
    end
    object quMCDocsIn_HDDepart: TSmallintField
      FieldName = 'Depart'
      Origin = 'Depart'
    end
    object quMCDocsIn_HDNumber: TStringField
      FieldName = 'Number'
      Origin = 'Number'
      Size = 10
    end
    object quMCDocsIn_HDDateInvoice: TSQLTimeStampField
      FieldName = 'DateInvoice'
      Origin = 'DateInvoice'
    end
    object quMCDocsIn_HDName: TStringField
      FieldName = 'Name'
      Origin = 'Name'
      Size = 30
    end
    object quMCDocsIn_HDIACTIVE: TSmallintField
      FieldName = 'IACTIVE'
      Origin = 'IACTIVE'
      ReadOnly = True
      Required = True
    end
    object quMCDocsIn_HDSCFNumber: TStringField
      FieldName = 'SCFNumber'
      Origin = 'SCFNumber'
      Size = 10
    end
    object quMCDocsIn_HDSummaTovarPost: TFloatField
      FieldName = 'SummaTovarPost'
      Origin = 'SummaTovarPost'
      DisplayFormat = '0.00'
    end
    object quMCDocsIn_HDSummaTovar: TFloatField
      FieldName = 'SummaTovar'
      Origin = 'SummaTovar'
      DisplayFormat = '0.00'
    end
    object quMCDocsIn_HDSummaTara: TFloatField
      FieldName = 'SummaTara'
      Origin = 'SummaTara'
      DisplayFormat = '0.00'
    end
    object quMCDocsIn_HDAcStatus: TByteField
      FieldName = 'AcStatus'
      Origin = 'AcStatus'
    end
    object quMCDocsIn_HDProvodType: TByteField
      FieldName = 'ProvodType'
      Origin = 'ProvodType'
    end
    object quMCDocsIn_HDICLICB: TIntegerField
      FieldName = 'ICLICB'
      Origin = 'ICLICB'
    end
    object quMCDocsIn_HDFullName: TStringField
      FieldName = 'FullName'
      Origin = 'FullName'
      Size = 100
    end
    object quMCDocsIn_HDEgais: TSmallintField
      FieldName = 'Egais'
      Origin = 'Egais'
    end
    object quMCDocsIn_HDDepName: TStringField
      FieldName = 'DepName'
      Origin = 'DepName'
      ReadOnly = True
      Size = 140
    end
    object quMCDocsIn_HDNUMBERRAR: TStringField
      FieldName = 'NUMBERRAR'
      Origin = 'NUMBERRAR'
      Size = 100
    end
    object quMCDocsIn_HDSID: TStringField
      FieldName = 'SID'
      Origin = 'SID'
      Size = 50
    end
    object quMCDocsIn_HDSHIPDATE: TStringField
      FieldName = 'SHIPDATE'
      Origin = 'SHIPDATE'
    end
  end
  object dsquMCDocsIn_HD: TDataSource
    DataSet = quMCDocsIn_HD
    Left = 64
    Top = 408
  end
  object PopupMenu1: TPopupMenu
    Images = dmR.SmallImage
    Left = 656
    Top = 248
  end
  object quMCDocsIn_SP: TFDQuery
    Connection = FDConnectShop
    SQL.Strings = (
      'declare @DATEB datetime = :DATEB'
      'declare @DATEE datetime = :DATEE'
      ''
      'SELECT sp.[IdH]'
      '      ,sp.[ID]'
      '      ,row_number() OVER (ORDER BY sp.[IdH],sp.[ID]) '#39'IDSP'#39
      '      ,sp.[CodeTovar]'
      '      ,ca.Name'
      '      ,sp.[CodeEdIzm]'
      '      ,sp.[Kol]'
      '      ,sp.[CenaTovar]'
      '      ,sp.[SumCenaTovarPost]'
      '      ,sp.[NewCenaTovar]'
      '      ,sp.[SumCenaTovar]'
      '      ,ca.FullName'
      '      ,sp.[BarCode]'
      '  FROM [scrystal].[dbo].[A_TTNInLn] sp'
      '  left join [scrystal].[dbo].[A_TTNIn] hd on hd.ID=sp.[IdH]'
      
        '  left join [EcrystalR].[dbo].[Clients] cli on cli.Id=hd.[ICLICB' +
        ']'
      '  left join [EcrystalR].[dbo].[Goods] ca on ca.Id=sp.[CodeTovar]'
      '  where hd.DateInvoice>=@DATEB'
      '  and hd.DateInvoice<(@DATEE+1)'
      '  and isnull(cli.Egais,0)>0'
      '  order by sp.[IdH],sp.[ID]')
    Left = 200
    Top = 344
    ParamData = <
      item
        Name = 'DATEB'
        DataType = ftDateTime
        ParamType = ptInput
        Value = 42401d
      end
      item
        Name = 'DATEE'
        DataType = ftDateTime
        ParamType = ptInput
        Value = 42415d
      end>
    object quMCDocsIn_SPIdH: TIntegerField
      FieldName = 'IdH'
      Origin = 'IdH'
      Required = True
    end
    object quMCDocsIn_SPID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      Required = True
    end
    object quMCDocsIn_SPIDSP: TLargeintField
      FieldName = 'IDSP'
      Origin = 'IDSP'
      ReadOnly = True
    end
    object quMCDocsIn_SPCodeTovar: TIntegerField
      FieldName = 'CodeTovar'
      Origin = 'CodeTovar'
    end
    object quMCDocsIn_SPName: TStringField
      FieldName = 'Name'
      Origin = 'Name'
      Size = 30
    end
    object quMCDocsIn_SPCodeEdIzm: TSmallintField
      FieldName = 'CodeEdIzm'
      Origin = 'CodeEdIzm'
    end
    object quMCDocsIn_SPKol: TFloatField
      FieldName = 'Kol'
      Origin = 'Kol'
      DisplayFormat = '0.000'
    end
    object quMCDocsIn_SPCenaTovar: TFloatField
      FieldName = 'CenaTovar'
      Origin = 'CenaTovar'
      DisplayFormat = '0.00'
    end
    object quMCDocsIn_SPSumCenaTovarPost: TFloatField
      FieldName = 'SumCenaTovarPost'
      Origin = 'SumCenaTovarPost'
      DisplayFormat = '0.00'
    end
    object quMCDocsIn_SPNewCenaTovar: TFloatField
      FieldName = 'NewCenaTovar'
      Origin = 'NewCenaTovar'
      DisplayFormat = '0.00'
    end
    object quMCDocsIn_SPSumCenaTovar: TFloatField
      FieldName = 'SumCenaTovar'
      Origin = 'SumCenaTovar'
      DisplayFormat = '0.00'
    end
    object quMCDocsIn_SPFullName: TStringField
      FieldName = 'FullName'
      Origin = 'FullName'
      Size = 60
    end
    object quMCDocsIn_SPBarCode: TStringField
      FieldName = 'BarCode'
      Origin = 'BarCode'
      Size = 13
    end
  end
  object dsquMCDocsIn_SP: TDataSource
    DataSet = quMCDocsIn_SP
    Left = 200
    Top = 408
  end
  object FDConnectShop: TFDConnection
    Params.Strings = (
      'Server=192.168.1.150'
      'User_Name=sa'
      'Password=YKS15Le'
      'ApplicationName=RarEx'
      'Workstation=SYS-4'
      'Database=RAR'
      'MARS=yes'
      'DriverID=MSSQL')
    FetchOptions.AssignedValues = [evMode, evRowsetSize]
    FetchOptions.Mode = fmAll
    FetchOptions.RowsetSize = 10000
    ResourceOptions.AssignedValues = [rvServerOutput, rvAutoReconnect]
    ResourceOptions.ServerOutput = True
    ResourceOptions.AutoReconnect = True
    ConnectedStoredUsage = []
    LoginPrompt = False
    Transaction = FDTrans
    UpdateTransaction = FDTransUpdate
    Left = 36
    Top = 204
  end
  object FDTrans: TFDTransaction
    Connection = FDConnectShop
    Left = 108
    Top = 204
  end
  object FDTransUpdate: TFDTransaction
    Connection = FDConnectShop
    Left = 180
    Top = 204
  end
  object FDPhysMSSQLDriverLink: TFDPhysMSSQLDriverLink
    Left = 292
    Top = 204
  end
  object FDGUIxWaitCursor: TFDGUIxWaitCursor
    Provider = 'Forms'
    Left = 416
    Top = 204
  end
end
