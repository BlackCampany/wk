unit ServiceAuto;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.SvcMgr, Vcl.Dialogs
  ,Winapi.ActiveX,FireDAC.Comp.Client,Vcl.Forms
  ,Vcl.ExtCtrls;

type
  TERAR_Auto_Service = class(TService)
    Timer1: TTimer;
    procedure ServiceCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { Private declarations }
  public
    function GetServiceController: TServiceController; override;
    { Public declarations }
  end;

  TUtm = class(TThread)
    private
       NUM:integer; //� �����
       RARID:String; //FSRARID
       IPUTM:String; //IP ����� UTM
    protected
       procedure Execute; override;
   end;

procedure WriteHistoryTC(Strwk_: string);
procedure WriteHistoryNum(Num: Integer; Strwk_: string);
function fmt:string;
procedure Delay(MSecs: Longint);

var
  ERAR_Auto_Service: TERAR_Auto_Service;
  CurDir:String;

implementation

{$R *.dfm}

uses dmRarA, Shared_Logs, Shared_Ping, EgaisDecode;


procedure Delay(MSecs: Longint);
var
  FirstTickCount, Now: Longint;
begin
  FirstTickCount := GetTickCount;
  repeat
    Application.ProcessMessages;
    { allowing access to other controls, etc. }
    Now := GetTickCount;
  until (Now - FirstTickCount >= MSecs) or (Now < FirstTickCount);
end;


procedure TUtm.Execute;
var _Log1:TLogWriter;
    tConn: TFDConnection;
    quDocInToCompare,quProc,quDocInToSend,quPlan,quSelDocInv,quSelDocInvSp,quSelDocVn: TFDQuery;
    TimeProc:TDateTime;
    iStatusPlanInv,iCurDate:Integer;

begin
  try
    CoInitialize(nil);
    //������� ������� � �������� ����
    WriteHistoryNum(NUM,'����� ����');
    tConn := TFDConnection.Create(nil);
    tConn.ConnectionDefName := ConstConnectionDef;  //���������� ���������� ����� Pool

    WriteHistoryNum(NUM,'������� �������');
    tConn.Connected:=True;

    if tConn.Connected then
    begin
      WriteHistoryNum(NUM,'������� � ����� ����');

      iCurDate:=Trunc(date);

      _Log1:=TLogWriter.Create;
      //<--
      //��������� ����������, ����� ����� ���� ������ ���� � �� ELog
      _Log1.Connection:=tConn;
      _Log1.Exemplar:=NUM;
      _Log1.CacheLogEnabled:=False;   //����� ������ ���� � ���
      _Log1.CacheLogMaxCount:=10000; //����.����� ����� � ����, ������ �����, ����� ���� ���� ����
      _Log1.TimesInterval:=5000;     //�������� ������ ���� ���� � �� �� �������
      _Log1.TimesEnabled:=False;     //������ ���� �� ������� ���������
      //-->

      _Log1.WriteMessageToLog(fmt+'������ �������� ������.');

      _Log1.WriteMessageToLog(fmt+'   ��� �����.');
      _Log1.WriteMessageToLog(fmt+'     �������� ������ ��������...');
      prGetListAskPrivate(nil,tConn,IPUTM,RARID);

      _Log1.WriteMessageToLog(fmt+'     �������� ������ �������...');
      prGetListAswPrivate(nil,tConn,IPUTM,RARID);  //Memo1:tcxMemo; FDConn: TFDConnection; IPUTM,FSRAR_ID:String

      _Log1.WriteMessageToLog(fmt+'     �������� ����� �������...');
      prGetFilesAswPrivate(nil,tConn,IPUTM,RARID);

      _Log1.WriteMessageToLog(fmt+'     ���������� ����� �������...');
      prDecodeFilesAswPrivate(nil,tConn,IPUTM,RARID);

      _Log1.WriteMessageToLog(fmt+'     �������������...');


      quDocInToCompare := TFDQuery.Create(nil);
      quDocInToCompare.Connection := tConn;
      quDocInToCompare.ResourceOptions.SilentMode:=True;     //�� ���������� Wait cursor

      quProc := TFDQuery.Create(nil);
      quProc.Connection := tConn;
      quProc.ResourceOptions.SilentMode:=True;     //�� ���������� Wait cursor

      quDocInToSend := TFDQuery.Create(nil);
      quDocInToSend.Connection := tConn;
      quDocInToSend.ResourceOptions.SilentMode:=True;     //�� ���������� Wait cursor

      quDocInToCompare.Active:=False;
      quDocInToCompare.SQL.Clear;
      quDocInToCompare.SQL.Add('');

      quDocInToCompare.SQL.Add('SELECT hd.[FSRARID],hd.[IDATE],hd.[SID],hd.[NUMBER],hd.[SDATE],hd.[STYPE],hd.[UNITTYPE],hd.[SHIPDATE],hd.[CLIFROM],hd.[IACTIVE],hd.[WBREGID],hd.[FIXNUMBER]');
      quDocInToCompare.SQL.Add('      ,hd.[FIXDATE],hd.[COMPARE_STATUS],hd.[COMPARE_COMMENT],hd.[READYSEND],hd.[IDHEAD],hd.[COMPARE_QUANT_STATUS],hd.[DATE_INPUT],hd.[DATE_OUTPUT]');
      quDocInToCompare.SQL.Add('FROM [dbo].[ADOCSIN_HD] hd');
      quDocInToCompare.SQL.Add('where hd.[IACTIVE]=0 and hd.[READYSEND]=0 and hd.[FSRARID]='''+RARID+'''');

      quDocInToCompare.Active:=True;
      quDocInToCompare.First;
      while not quDocInToCompare.Eof do
      begin
        _Log1.WriteMessageToLog(fmt+'       - '+quDocInToCompare.FieldByName('FSRARID').AsString+' '+quDocInToCompare.FieldByName('SHIPDATE').AsString+' '+quDocInToCompare.FieldByName('SID').AsString+' '+quDocInToCompare.FieldByName('NUMBER').AsString);

        quProc.Active:=False;
        quProc.SQL.Clear;
        quProc.SQL.Add('');

        quProc.SQL.Add('DECLARE @FSRARID varchar(50) = '''+quDocInToCompare.FieldByName('FSRARID').AsString+'''');
        quProc.SQL.Add('DECLARE @SID varchar(50) = '''+quDocInToCompare.FieldByName('SID').AsString+'''');
        quProc.SQL.Add('DECLARE @IDATE int = '+its(quDocInToCompare.FieldByName('IDATE').AsInteger));
        quProc.SQL.Add('DECLARE @IHEADMANUAL bigint = '+its(quDocInToCompare.FieldByName('IDHEAD').AsInteger));
        quProc.SQL.Add('EXECUTE [RAR].[dbo].[prCompareDoc] @FSRARID,@SID,@IDATE,@IHEADMANUAL');
        quProc.Active:=True;

        _Log1.WriteMessageToLog(fmt+'          '+quProc.FieldByName('COMMENTCOMPARE').AsString);
        _Log1.WriteMessageToLog(fmt+'');
        quProc.Active:=False;

        quDocInToCompare.Next;
      end;
      quDocInToCompare.Active:=False;
      _Log1.WriteMessageToLog(fmt+'     ������������� ���������.');
      _Log1.WriteMessageToLog(fmt+'    ');
      _Log1.WriteMessageToLog(fmt+'     �������� ������� � �����...');

      quDocInToSend.Active:=False;
      quDocInToSend.SQL.Clear;
      quDocInToSend.SQL.Add('EXECUTE [RAR].[dbo].[prSendLIST] '''+RARID+'''');
      quDocInToSend.Active:=True;
      quDocInToSend.First;
      while not quDocInToSend.Eof do
      begin
        _Log1.WriteMessageToLog(fmt+'       - '+quDocInToSend.FieldByName('FSRARID').AsString+' '+quDocInToSend.FieldByName('SHIPDATE').AsString+' '+quDocInToSend.FieldByName('SID').AsString+' '+quDocInToSend.FieldByName('NUMBER').AsString);

        if prSendActPrivate(RARID,quDocInToSend.FieldByName('SID').AsString,quDocInToSend.FieldByName('IDATE').AsInteger,0,nil,tConn,IPUTM) then
        begin
          _Log1.WriteMessageToLog(fmt+'         ��');

          quProc.Active:=False;
          quProc.SQL.Clear;
          quProc.SQL.Add('');

          quProc.SQL.Add('DECLARE @FSRARID varchar(50) = '''+quDocInToSend.FieldByName('FSRARID').AsString+'''');
          quProc.SQL.Add('DECLARE @SID varchar(50) = '''+quDocInToSend.FieldByName('SID').AsString+'''');
          quProc.SQL.Add('DECLARE @IDATE int = '+its(quDocInToSend.FieldByName('IDATE').AsInteger));
          quProc.SQL.Add('EXECUTE [RAR].[dbo].[prSetSendStatusDoc1] @FSRARID,@SID,@IDATE');

          quProc.ExecSQL;

//          quProc.Active:=True;
//          quProc.Active:=False;

        end else _Log1.WriteMessageToLog(fmt+'         ERROR');

        quDocInToSend.Next;
      end;
      quDocInToSend.Active:=False;

      _Log1.WriteMessageToLog(fmt+'     �������� ������� � ����� ���������');
      _Log1.WriteMessageToLog(fmt+'   ');

      //��������� ������ �� �������������� ���� �����
      TimeProc:=frac(Now);
//      if (TimeProc>=0.04167) and (TimeProc<=0.25) then  //������� ����� >= 1:00 � <= 6:00
//      if NUM=1 then
//      if (TimeProc>=0.04167) then  //������� ����� >= 1:00 � <= 6:00
      if (TimeProc>=0.0833333) and (TimeProc<=0.25) then  //������� ����� >= 1:00 � <= 6:00
      begin
        try
          quPlan := TFDQuery.Create(nil);
          quPlan.Connection := tConn;
          quPlan.ResourceOptions.SilentMode:=True;     //�� ���������� Wait cursor

          quPlan.Active:=False;
          quPlan.SQL.Clear;
          quPlan.SQL.Add('EXECUTE [dbo].[prAddPlanProc] 1,'''+RARID+''',0');
          quPlan.Active:=True;
          iStatusPlanInv:=quPlan.FieldByName('RETNUM').AsInteger;
          quPlan.Active:=False;

          if iStatusPlanInv=0 then  //������� ������ �� �������������� ��� �� ����������
          begin
            _Log1.WriteMessageToLog(fmt+'     ��������� ������ �������� �� ������ ��������.');
            if prGetRemn1Private(RARID,nil,tConn,IPUTM) then
            begin
              quProc.Active:=False;
              quProc.SQL.Clear;
              quProc.SQL.Add('');

              quProc.SQL.Add('UPDATE [dbo].[PLANPROC] SET [STATUS] = 1');
              quProc.SQL.Add('where [FSRARID]='''+RARID+'''');
              quProc.SQL.Add('and [TYPEPROC]=1');
              quProc.SQL.Add('and [IDATE]='+its(iCurDate));
              quProc.ExecSQL;

            end;
          end;
        finally
          quPlan.Active:=False;
          quPlan.Free;
        end;

        //���� �������� �������������� ���� ����� ��������� ���� �� �������� ����������� - ���� ��� �� �������
        _Log1.WriteMessageToLog(fmt+'     �������� ������� ��������� �������������� �� 1 ��������.');

        try
          quSelDocInv := TFDQuery.Create(nil);
          quSelDocInv.Connection := tConn;
          quSelDocInv.ResourceOptions.SilentMode:=True;     //�� ���������� Wait cursor

          quSelDocInvSp := TFDQuery.Create(nil);
          quSelDocInvSp.Connection := tConn;
          quSelDocInvSp.ResourceOptions.SilentMode:=True;     //�� ���������� Wait cursor

          quSelDocVn := TFDQuery.Create(nil);
          quSelDocVn.Connection := tConn;
          quSelDocVn.ResourceOptions.SilentMode:=True;     //�� ���������� Wait cursor

          quSelDocInv.Active:=False;
          quSelDocInv.SQL.Clear;
          quSelDocInv.SQL.Add('');
          quSelDocInv.SQL.Add('SELECT TOP 1 [FSRARID],[IDATE],[ID],[STYPE],[FIXDATE],[IDREPLY]');
          quSelDocInv.SQL.Add('FROM [dbo].[ADOCSINV_HD]');
          quSelDocInv.SQL.Add('where [FSRARID]='''+RARID+'''');
          quSelDocInv.SQL.Add('and [IDATE]='+its(iCurDate));
          quSelDocInv.SQL.Add('and STYPE=''STOCK ( 1-reg )''');

          quSelDocInv.Active:=True;
          if quSelDocInv.RecordCount>0 then  //�������������� ��������
          begin
            //��������� �� ������ �� ��
            _Log1.WriteMessageToLog(fmt+'     �������������� �� 1 �������� ��������.');

            quSelDocInvSp.Active:=False;
            quSelDocInvSp.SQL.Clear;
            quSelDocInvSp.SQL.Add('');
            quSelDocInvSp.SQL.Add('SELECT TOP 10 spi.[IDH],spi.[ID],spi.[CAP],spi.[QUANTR],spi.[QUANTF],spi.[INFO_A],spi.[INFO_B]');
            quSelDocInvSp.SQL.Add('  FROM [dbo].[ADOCSINV_SP] spi');
            quSelDocInvSp.SQL.Add('  left join [dbo].[ACARDS] ca on ca.KAP=spi.CAP');
            quSelDocInvSp.SQL.Add('  where spi.IDH='+its(quSelDocInv.FieldByName('ID').AsInteger));
            quSelDocInvSp.SQL.Add('  and ca.[AVID]<500');
            quSelDocInvSp.Active:=True;

            if quSelDocInvSp.RecordCount>0 then  //�������������� �� ������ �� �������� ��������
            begin
              //��������� ����������� �� �������� ����������� �� 2-�� �������
//              _Log1.WriteMessageToLog(fmt+'     �������������� �� 1 �������� �� ������.');
              quSelDocVn.Active:=False;
              quSelDocVn.SQL.Clear;
              quSelDocVn.SQL.Add('');

              quSelDocVn.SQL.Add('SELECT TOP 1 [FSRARID],[IDATE],[SID],[NUMBER],[SDATE],[STYPE],[IACTIVE],[READYSEND],[WBREGID],[TICK1],[TICK2] ,[SIDIN]');
              quSelDocVn.SQL.Add('  FROM [RAR].[dbo].[ADOCSVN_HD]');
              quSelDocVn.SQL.Add(' where [FSRARID]='''+RARID+'''');
              quSelDocVn.SQL.Add(' and [IDATE]='+its(iCurDate));
              quSelDocVn.SQL.Add(' and STYPE=''ToShop''');

              quSelDocVn.Active:=True;
              if quSelDocVn.RecordCount=0 then
              begin //�������� ����������� ��� �� ����������� - ����� �����������
                _Log1.WriteMessageToLog(fmt+'     �������� �������� ����������� �� 2 �������.');

                quProc.Active:=False;
                quProc.SQL.Clear;
                quProc.SQL.Add('DECLARE @IDH bigint = '+its(quSelDocInv.FieldByName('ID').AsInteger));
                quProc.SQL.Add('EXECUTE [dbo].[prCreateVnDoc12fromInv] @IDH');
                quProc.ExecSQL;
                Delay(1000);

              end;

              quSelDocVn.Active:=False;
              quSelDocVn.SQL.Clear;
              quSelDocVn.SQL.Add('');

              quSelDocVn.SQL.Add('SELECT TOP 1 [FSRARID],[IDATE],[SID],[NUMBER],[SDATE],[STYPE],[IACTIVE],[READYSEND],[WBREGID],[TICK1],[TICK2] ,[SIDIN]');
              quSelDocVn.SQL.Add('  FROM [RAR].[dbo].[ADOCSVN_HD]');
              quSelDocVn.SQL.Add(' where [FSRARID]='''+RARID+'''');
              quSelDocVn.SQL.Add(' and [IDATE]='+its(iCurDate));
              quSelDocVn.SQL.Add(' and STYPE=''ToShop''');

              quSelDocVn.Active:=True;

              if quSelDocVn.RecordCount>0 then
              begin //�������� ����������� ����������� - ����� ��������
                if quSelDocVn.FieldByName('IACTIVE').AsInteger=0 then //��� �� ��������
                begin
                   _Log1.WriteMessageToLog(fmt+'     ������� �������� ����������� �� 2 ������� �����.');

                   if prSendDocVnPrivate(RARID,quSelDocVn.FieldByName('SID').AsString,iCurDate,nil,tConn,IPUTM) then
                   begin
                     _Log1.WriteMessageToLog(fmt+'       �������� ��.');

                     quProc.Active:=False;
                     quProc.SQL.Clear;
                     quProc.SQL.Add('');
                     quProc.SQL.Add('UPDATE dbo.ADOCSVN_HD');
                     quProc.SQL.Add('SET IACTIVE = 1');
                     quProc.SQL.Add(',SDATE = GETDATE()');
                     quProc.SQL.Add('WHERE');
                     quProc.SQL.Add('  FSRARID = '''+RARID+'''');
                     quProc.SQL.Add('  AND IDATE = '+its(iCurDate));
                     quProc.SQL.Add('  AND SID = '''+quSelDocVn.FieldByName('SID').AsString+'''');
                     quProc.ExecSQL;
                   end else
                   begin
                     _Log1.WriteMessageToLog(fmt+'       �������� ������ � ��������.');
                   end;
                end;
              end;
            end;
          end else _Log1.WriteMessageToLog(fmt+'     �������������� �� 1 �������� ���.');

        finally
          quSelDocVn.Active:=False;
          quSelDocVn.Free;

          quSelDocInvSp.Active:=False;
          quSelDocInvSp.Free;

          quSelDocInv.Active:=False;
          quSelDocInv.Free;
        end;
      end;
      _Log1.WriteMessageToLog(fmt+'������� ������ ��������.');
       WriteHistoryNum(NUM,'������� ������ ��������.');
    end;
  finally
    quDocInToCompare.Free;
    quProc.Free;
    quDocInToSend.Free;
    FreeAndNil(_Log1);
    tConn.Free;
    CoUnInitialize;
    terminate;
  end;
end;


function fmt:string;
begin
  Result:= FormatDateTime('dd HH:NN:SS  ', Now);
end;


procedure WriteHistoryNum(Num: Integer; Strwk_: string);
var F: TextFile;
    Strwk1:String;
    FileN:String;
begin
  try
    strwk1:='service_erar_'+inttostr(Num)+'_'+FormatDateTime('yyyy_mm_dd',Date);
    Strwk1:=StrWk1+'.txt';
//    Application.ProcessMessages;
//  FileN:=CurDir+strwk1;
    FileN:=curdir+strwk1;
    AssignFile(F, FileN);
    if Not FileExists(FileN) then Rewrite(F)
    else                           Append(F);
    Write(F, FormatDateTime('dd HH:NN:SS  ', Now));
    WriteLn(F, Strwk_);
    Flush(F);
  finally
    CloseFile(F);
  end;
end;

procedure WriteHistoryTC(Strwk_: string);
var F: TextFile;
    Strwk1:String;
    FileN:String;
begin
  try
    strwk1:='service_erar_'+FormatDateTime('yyyy_mm_dd',Date);
    Strwk1:=StrWk1+'.txt';
//    Application.ProcessMessages;
//  FileN:=CurDir+strwk1;
    FileN:=curdir+strwk1;
    AssignFile(F, FileN);
    if Not FileExists(FileN) then Rewrite(F)
    else                           Append(F);
    Write(F, FormatDateTime('dd HH:NN:SS  ', Now));
    WriteLn(F, Strwk_);
    Flush(F);
  finally
    CloseFile(F);
  end;
end;

procedure ServiceController(CtrlCode: DWord); stdcall;
begin
  ERAR_Auto_Service.Controller(CtrlCode);
end;

function TERAR_Auto_Service.GetServiceController: TServiceController;
begin
  Result := ServiceController;
end;

procedure TERAR_Auto_Service.ServiceCreate(Sender: TObject);
begin
  CurDir := ExtractFilePath(ParamStr(0));
  Timer1.Enabled:=False;
//  Timer1.Interval:=300000;     //5 ��� ����
//  Timer1.Interval:=30000;     //30 ���
  Timer1.Interval:=900000;     //15 ���
  Timer1.Enabled:=True;
end;

procedure TERAR_Auto_Service.Timer1Timer(Sender: TObject);
var _LOG0:TLogWriter;
    sWr:string;
    Utms:array[1..100] of TUtm;
    n:Integer;
    CurTime:TDateTime;
begin
  with dmRA do
  begin
    //��������� ��������� ������ �������
    CurTime:=Frac(now);
    if CurTime<0.04167 then Exit;  //00:00 �� 1:00
    if CurTime>0.84375 then Exit;  //20:45 �� 24:00  - ���� ���������� � ���������

    Timer1.Enabled:=False;

    WriteHistoryTC(' ������ '+IntToStr(Timer1.Interval)+' �� ');
    try
      _Log0:=TLogWriter.Create;
      //<--
      //��������� ����������, ����� ����� ���� ������ ���� � �� ELog
      _Log0.Connection:=dmRA.FDConnection;
      _Log0.CacheLogEnabled:=False;   //����� ������ ���� � ���
      _Log0.CacheLogMaxCount:=10000; //����.����� ����� � ����, ������ �����, ����� ���� ���� ����
      _Log0.TimesInterval:=5000;     //�������� ������ ���� ���� � �� �� �������
      _Log0.TimesEnabled:=False;     //������ ���� �� ������� ���������
      _LOG0.Exemplar:=0;
      //-->
      _LOG0.WriteMessageToLog(fmt+'������ ��������.');

//      if DBConnectUDL then

      if DBConnectPool then
      begin
        WriteHistoryTC(' ���� ��');
        _LOG0.WriteMessageToLog(fmt+' ���� ��');

        quPoint.Active:=False;
        quPoint.Active:=True;

        n:=1;
        quPoint.First;
        while not quPoint.Eof do
        begin
          _LOG0.WriteMessageToLog(fmt+'  ��� '+quPointNAMESHOP.AsString+' ('+quPointISHOP.AsString+') '+quPointSHORTNAME.AsString+' '+quPointIPUTM.AsString+' '+quPointRARID.AsString);
          if PingTest(quPointIPUTM.AsString,100,sWr) then
          begin
            _LOG0.WriteMessageToLog(fmt+'  Ping Ok.');

            Utms[n]:=TUtm.Create(True);
            Utms[n].Priority:=tpLower;
            Utms[n].NUM:=quPointISHOP.AsInteger;
            Utms[n].RARID:=quPointRARID.AsString;
            Utms[n].IPUTM:=quPointIPUTM.AsString;

            Utms[n].FreeOnTerminate:=True;
            Utms[n].Resume;

          end else _LOG0.WriteMessageToLog(fmt+'��������� ����������. Ping �����������. '+sWr);

          quPoint.Next;
          inc(n); if n>100 then Break;
        end;
        quPoint.Active:=False;

      end else
      begin
        WriteHistoryTC(' ������ �������� ��');
        _LOG0.WriteMessageToLog(fmt+' ������ �������� ��');
      end;

      _LOG0.WriteMessageToLog(fmt+'������� ��������.');
    finally
        FreeAndNil(_Log0);
    end;
    Timer1.Enabled:=True;
  end;
end;

end.
