unit QBarCodeH;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  CustomChildForm, nativexml, httpsend, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid,
  dxRibbonSkins, dxRibbonCustomizationForm, dxRibbon, dxBar, System.Actions, Vcl.ActnList, Vcl.PlatformDefaultStyleActnCtrls, Vcl.ActnMan,
  dxBarExtItems, cxCalendar, cxImageComboBox, frxClass, frxDBSet, frxBarcode;

type
  TfmQBarCodeH = class(TfmCustomChildForm)
    grQBarCodeH: TcxGrid;
    vwQBarCodeH: TcxGridDBTableView;
    lvQBarCodeH: TcxGridLevel;
    quQBarCodeH: TFDQuery;
    dsQBarCodeH: TDataSource;
    ActionManager1: TActionManager;
    acClose: TAction;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarGroup1: TdxBarGroup;
    dxRibbon1: TdxRibbon;
    dxRibbon1Tab1: TdxRibbonTab;
    vwQBarCodeHFSRARID: TcxGridDBColumn;
    vwQBarCodeHIDATE: TcxGridDBColumn;
    vwQBarCodeHID: TcxGridDBColumn;
    acSendQueryBarcode: TAction;
    vwQBarCodeHISHOP: TcxGridDBColumn;
    deDateBeg: TdxBarDateCombo;
    deDateEnd: TdxBarDateCombo;
    acRefresh: TAction;
    dxBarManager1Bar2: TdxBar;
    dxBarLargeButton1: TdxBarLargeButton;
    acAdd: TAction;
    dxBarManager1Bar3: TdxBar;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    acEdit: TAction;
    acView: TAction;
    acDelete: TAction;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    vwQBarCodeHISTATUS: TcxGridDBColumn;
    quQBarCodeHID: TFDAutoIncField;
    quQBarCodeHFSRARID: TStringField;
    quQBarCodeHIDATE: TIntegerField;
    quQBarCodeHINUMBER: TIntegerField;
    quQBarCodeHISTATUS: TIntegerField;
    quQBarCodeHISHOP: TIntegerField;
    vwQBarCodeHINUMBER: TcxGridDBColumn;
    quQBarCodeHNAMESHOP: TStringField;
    vwQBarCodeHNAMESHOP: TcxGridDBColumn;
    quQBarCodeHIDTORAR: TLargeintField;
    quQBarCodeHREJCOMMENT: TMemoField;
    dxBarLargeButton8: TdxBarLargeButton;
    acPrint: TAction;
    quDocSpecPrint: TFDQuery;
    quDocSpecPrintIDH: TIntegerField;
    quDocSpecPrintID: TFDAutoIncField;
    quDocSpecPrintIDENT: TIntegerField;
    quDocSpecPrintTYPE: TStringField;
    quDocSpecPrintRANK: TStringField;
    quDocSpecPrintNUMBER: TStringField;
    quDocSpecPrintBARCODE: TStringField;
    frxReport: TfrxReport;
    frxdsDocSpecPrint: TfrxDBDataset;
    frxBarCodeObject1: TfrxBarCodeObject;
    frxdsDocHeadPrint: TfrxDBDataset;
    procedure acCloseExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acSendQueryBarcodeExecute(Sender: TObject);
    procedure deDateBegChange(Sender: TObject);
    procedure acRefreshExecute(Sender: TObject);
    procedure acEditExecute(Sender: TObject);
    procedure acAddExecute(Sender: TObject);
    procedure acViewExecute(Sender: TObject);
    procedure acDeleteExecute(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Init;
  end;

var
  fmQBarCodeH: TfmQBarCodeH;

procedure ShowFormQBarCodeH;

implementation

{$R *.dfm}

uses dmRar, EgaisDecode, Shared_Types, QBarCodeS;

procedure ShowFormQBarCodeH;
begin
  if Assigned(fmQBarCodeH)=False then //����� �� ����������, � ����� �������
    fmQBarCodeH:=TfmQBarCodeH.Create(Application);
  if fmQBarCodeH.WindowState=wsMinimized then //���� ���� ��������, �� ������������� ���
    fmQBarCodeH.WindowState:=wsNormal;

  fmQBarCodeH.deDateBeg.Date:=Date-30;
  fmQBarCodeH.deDateEnd.Date:=Date;
  fmQBarCodeH.Init;
  fmQBarCodeH.Show;
end;

procedure TfmQBarCodeH.Init;
var
  FocusedRow, TopRow: Integer;
  flag: boolean;
begin
  Application.ProcessMessages;

  try
    //<--Refresh � ��������������� ������� ������� � �������
    flag:=quQBarCodeH.Active;  //���������� ���� �� ������� �������, ��� �������������� ������� �������
    TopRow := vwQBarCodeH.Controller.TopRowIndex;
    FocusedRow := vwQBarCodeH.DataController.FocusedRowIndex;
    //-->
    vwQBarCodeH.BeginUpdate;
    quQBarCodeH.Active:=False;
    quQBarCodeH.ParamByName('IDATEB').AsInteger:=Trunc(deDateBeg.Date);
    quQBarCodeH.ParamByName('IDATEE').AsInteger:=Trunc(deDateEnd.Date);
    quQBarCodeH.Active:=True;
    quQBarCodeH.First;
  finally
    vwQBarCodeH.EndUpdate;
    //<--��������������� �������
    if flag then begin
      vwQBarCodeH.DataController.FocusedRowIndex := FocusedRow;
      vwQBarCodeH.Controller.TopRowIndex := TopRow;
    end;
    //-->
  end;
end;

procedure TfmQBarCodeH.acAddExecute(Sender: TObject);
begin
  //��������
  ShowFormQBarCodeS(0, '', TState.sAdd);
end;

procedure TfmQBarCodeH.acCloseExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmQBarCodeH.acDeleteExecute(Sender: TObject);
begin
  //��������
  if quQBarCodeH.RecordCount=0 then
  begin
    showmessage('�������� ������ ��� ��������.');
    Exit;
  end;
  if not (quQBarCodeHISTATUS.AsInteger in [0,2])  then
  begin
    showmessage('������� ����� ������� ������ � ������� ����������� ��� �������.');
    Exit;
  end;

  //������� ��������
end;

procedure TfmQBarCodeH.acEditExecute(Sender: TObject);
begin
  //�������������
  if quQBarCodeH.RecordCount=0 then
  begin
    showmessage('�������� ������ ��� ��������������.');
    Exit;
  end;

  if quQBarCodeHISTATUS.AsInteger=1  then
  begin
    showmessage('������������� ������������ ������ ������.');
    Exit;
  end;
  if quQBarCodeHISTATUS.AsInteger=3  then
  begin
    showmessage('������������� ���������� ������ ������.');
    Exit;
  end;

  ShowFormQBarCodeS(quQBarCodeHID.AsInteger, quQBarCodeHFSRARID.AsString, TState.sEdit);
end;

procedure TfmQBarCodeH.acPrintExecute(Sender: TObject);
begin
  quDocSpecPrint.Active:=False;
  quDocSpecPrint.ParamByName('IDH').Value:=quQBarCodeHID.Value;
  quDocSpecPrint.Active:=True;

  frxReport.LoadFromFile(CommonSet.ReportsDir + 'QBarCode.fr3');
//  frxReport.Variables['Num']:=''''+dmOS.quDocsOutRSelNUMDOC.AsString+'/1'+'''';
//  frxReport.Variables['sDate']:=''''+FormatDateTime('dd.mm.yyyy',dmOS.quDocsOutRSelDATEDOC.AsDateTime)+'''';
//  frxReport.Variables['NameMH']:=''''+dmOS.quDocsOutRSelNAMEMH.AsString+'''';
  frxReport.PrepareReport;
  frxReport.ShowPreparedReport;



  quDocSpecPrint.Active:=False;
end;

procedure TfmQBarCodeH.acRefreshExecute(Sender: TObject);
begin
  //��������
  Init;
end;

procedure TfmQBarCodeH.acSendQueryBarcodeExecute(Sender: TObject);
Const
  CR = #$0d;
  LF = #$0a;
  CRLF = CR + LF;
  Boundary = 'END_OF_PART';

var  AskXml: TNativeXml;
     IdF:Integer;
     RetXml: TNativeXml;
     RetId:string;

     httpsend: THTTPSend;
     s: AnsiString;
     FS: TMemoryStream;
     RetVal:TStringList;
     nodeMark: TXmlNode;
begin
  //������ ������ ���������� ����������
  //������ ��� �������� ����� ������������ ��� � ����� �� ����� �����!
  if quQBarCodeHISTATUS.AsInteger<>0 then begin
    ShowMessage('��������� ������ � ����� ����� ������ � ������� �����������!');
    Exit;
  end;

  if MessageDlg('��������� ��������� � ����� ? ( ���������� - '+quQBarCodeHFSRARID.AsString+')',mtConfirmation, [mbYes, mbNo], 0, mbYes) <> mrYes then Exit;
  if dmR.FDConnection.Connected=False then Exit;

  with dmR do
  begin
    try
      ClearMessageLogLocal;
      ShowMessageLogLocal('���� ��');

      quQBARCODE_SP.Active:=False;
      quQBARCODE_SP.ParamByName('IDH').AsInteger:=quQBarCodeHID.AsInteger;
      quQBARCODE_SP.Active:=True;
      if quQBARCODE_SP.RecordCount=0 then Exit;

      IdF:=fGetId(1);
      ShowMessageLogLocal('  ������ '+IntToStr(IdF));

      FS := TMemoryStream.Create;
      httpsend := THTTPSend.Create;

      //��������� ���� �������
      AskXml := TNativeXml.Create(nil);
      RetXml := TNativeXml.Create(nil);

      AskXml.XmlFormat := xfReadable;
      AskXml.CreateName('ns:Documents');
      AskXml.Root.WriteAttributeString('Version', '1.0');
      AskXml.Root.WriteAttributeString('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
      AskXml.Root.WriteAttributeString('xmlns:ns', 'http://fsrar.ru/WEGAIS/WB_DOC_SINGLE_01');
      AskXml.Root.WriteAttributeString('xmlns:bk', 'http://fsrar.ru/WEGAIS/QueryBarcode');
      AskXml.Root.WriteAttributeString('xmlns:ce', 'http://fsrar.ru/WEGAIS/CommonEnum');

      AskXml.Root.NodeNew('ns:Owner');
      AskXml.Root.NodeByName('ns:Owner').NodeNew('ns:FSRAR_ID').Value:=quQBarCodeHFSRARID.AsString;

      AskXml.Root.NodeNew('ns:Document');
      AskXml.Root.NodeByName('ns:Document').NodeNew('ns:QueryBarcode');
      AskXml.Root.NodeByName('ns:Document').NodeByName('ns:QueryBarcode').NodeNew('bk:QueryNumber').Value:=dmR.quQBARCODE_SPINUMBER.AsString;
      AskXml.Root.NodeByName('ns:Document').NodeByName('ns:QueryBarcode').NodeNew('bk:Date').Value:=dtsrar(Now);
      AskXml.Root.NodeByName('ns:Document').NodeByName('ns:QueryBarcode').NodeNew('bk:Marks');

      quQBARCODE_SP.First;
      while not quQBARCODE_SP.Eof do
      begin
        nodeMark:=AskXml.Root.NodeByName('ns:Document').NodeByName('ns:QueryBarcode').NodeByName('bk:Marks').NodeNew('bk:Mark');
        nodeMark.NodeNew('bk:Identity').Value:=quQBARCODE_SPIDENT.AsString;
        nodeMark.NodeNew('bk:Type').Value:=quQBARCODE_SPTYPE.AsString;
        nodeMark.NodeNew('bk:Rank').Value:=quQBARCODE_SPRANK.AsString;
        nodeMark.NodeNew('bk:Number').Value:=quQBARCODE_SPNUMBER.AsString;

        quQBARCODE_SP.Next;
      end;

      //AskXml.SaveToFile('c:\QueryBarcode.xml');
      AskXml.SaveToStream(FS);

      //����� � ����
      quToRar.Active:=False;
      quToRar.ParamByName('ID').AsInteger:=IdF;
      quToRar.ParamByName('RARID').AsString:=quQBarCodeHFSRARID.AsString;
      quToRar.Active:=True;

      quToRar.Append;
      quToRarFSRARID.AsString:=quQBarCodeHFSRARID.AsString;
      quToRarID.AsInteger:=IdF;
      quToRarDATEQU.AsDateTime:=Now;
      quToRarITYPEQU.AsInteger:=1;
      quToRarISTATUS.AsInteger:=1;
      quToRarSENDFILE.LoadFromStream(FS);
      quToRar.Post;

      httpsend.MimeType := 'multipart/form-data; boundary='+Boundary;  //���������� Contetn-Type �������
      // ���������� Mime-��� � ������ �� �����
      s:='--'+Boundary+CRLF+'Content-Disposition: form-data; name="xml_file"; filename="'+IdToName(IdF)+'"'+CRLF+'Content-Type: application/xml'+CRLF+CRLF;
      httpsend.Document.Write(PAnsiChar(s)^, Length(s));
      FS.Position := 0;
      httpsend.Document.CopyFrom(FS, FS.Size);  //���������� ����� � ���� ���������
      S:=CRLF+CRLF+'--'+Boundary+'--'+CRLF;     //��������� ���� �������
      httpsend.Document.Write(PAnsiChar(s)^, Length(s)); // ��������� ���� ���������

      httpsend.KeepAlive:=False;
      httpsend.Status100:=True;
      httpsend.Headers.Add('Accept: */*');

      // ���������� ������
      if httpsend.HTTPMethod('POST','http://'+quQBARCODE_SPIPUTM.AsString+':8080/opt/in/QueryBarcode') then
      begin
        ShowMessageLogLocal('TRUE');
        ShowMessageLogLocal('ResultCode='+IntToStr(httpsend.ResultCode));
        ShowMessageLogLocal(httpsend.ResultString);
        //MemoLogLocal.Lines.LoadFromStream(httpsend.Document, TEncoding.UTF8);

        if httpsend.ResultCode<>200 then
        begin
          try
            RetVal:=TStringList.Create;
            RetVal.LoadFromStream(httpsend.Document, TEncoding.UTF8);
            prWMemo(MemoLogLocal,RetVal.Text);
          finally
            RetVal.Free;
          end;
        end;

        httpsend.Document.Position:=0;
        RetXml.LoadFromStream(httpsend.Document);
        RetXml.XmlFormat := xfReadable;
        //RetXml.SaveToFile('C:\1110.xml');

        //RetId:='';
        //nodeRoot := RetXml.Root;
        //if assigned(nodeRoot) then  RetId:=nodeRoot.NodeByName('url').Value;

        RetId:=RetXml.Root.NodeByName('url').Value;

        if RetId>'' then
        begin
          httpsend.Document.Position:=0;

          quToRar.Edit;
          quToRarISTATUS.AsInteger:=2;
          quToRarRECEIVE_ID.AsString:=RetId;
          quToRarRECEIVE_FILE.LoadFromStream(httpsend.Document);
          quToRar.Post;

          //<--�������� ������ �������
          quQBarCodeH.Edit;
          quQBarCodeHIDATE.AsInteger:=Trunc(Date);
          quQBarCodeHINUMBER.AsInteger:=quQBARCODE_SPINUMBER.AsInteger;
          quQBarCodeHISTATUS.AsInteger:=1; //1-���������
          quQBarCodeHIDTORAR.Value:=IdF;   //����� �������
          quQBarCodeH.Post;
          //-->
        end;

      end else begin
        ShowMessageLogLocal('FALSE');
        ShowMessageLogLocal('ResultCode='+IntToStr(httpsend.ResultCode));
        //ShowMessageLogLocal(httpsend.ResultString);

        quToRar.Edit;
        quToRarISTATUS.AsInteger:=100;
        quToRar.Post;
      end;

    finally
      FS.Free;
      AskXml.Free;
      RetXml.Free;
      quToRar.Active:=False;
      quQBARCODE_SP.Active:=False;
    end;

    ShowMessageLogLocal('������� ��������.');
  end;
end;

procedure TfmQBarCodeH.acViewExecute(Sender: TObject);
begin
  //��������
  if quQBarCodeH.RecordCount=0 then
  begin
    showmessage('�������� ������ ��� ���������.');
    Exit;
  end;

  ShowFormQBarCodeS(quQBarCodeHID.AsInteger, quQBarCodeHFSRARID.AsString, TState.sView);
end;

procedure TfmQBarCodeH.deDateBegChange(Sender: TObject);
begin
  if dmR.FDConnection.Connected then Init;
end;

procedure TfmQBarCodeH.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
  fmQBarCodeH:=Nil;
end;

end.
