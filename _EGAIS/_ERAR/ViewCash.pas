unit ViewCash;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  CustomChildForm, System.UITypes, ShellApi, httpsend, synautil, nativexml, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxRibbonSkins, dxRibbonCustomizationForm, cxContainer, cxEdit, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator,
  Data.DB, cxDBData, cxImageComboBox, cxTextEdit, cxDBLookupComboBox, cxCheckBox, cxCalendar, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.Menus, System.Actions, Vcl.ActnList, dxBar, cxBarEditItem, dxBarExtItems, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid, cxMemo, Vcl.ExtCtrls, dxRibbon;

type
  TfmDocCash = class(TfmCustomChildForm)
    dxRibbon1: TdxRibbon;
    dxRibbon1Tab1: TdxRibbonTab;
    dxBarManager1: TdxBarManager;
    bmApplyDate: TdxBar;
    bmClose: TdxBar;
    btnDone: TdxBarLargeButton;
    btnClose: TdxBarLargeButton;
    deDateBeg: TdxBarDateCombo;
    deDateEnd: TdxBarDateCombo;
    dxBarGroup1: TdxBarGroup;
    ActionList1: TActionList;
    acRefresh: TAction;
    acClose: TAction;
    acSelectDate: TAction;
    GridDocCash: TcxGrid;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    beiPoint: TcxBarEditItem;
    acExpExcel: TAction;
    dxBarLargeButton9: TdxBarLargeButton;
    dxBarLargeButton10: TdxBarLargeButton;
    beiGetTTN: TcxBarEditItem;
    dxBarLargeButton14: TdxBarLargeButton;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    LevelDocCash: TcxGridLevel;
    ViewDocCash: TcxGridDBTableView;
    pmDocVn: TPopupMenu;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton11: TdxBarLargeButton;
    acGetCashSail: TAction;
    N1: TMenuItem;
    dxBarLargeButton12: TdxBarLargeButton;
    N2: TMenuItem;
    Excel1: TMenuItem;
    acDelDocVn: TAction;
    OpenDialog1: TOpenDialog;
    quDelZ: TFDQuery;
    quS: TFDQuery;
    quA: TFDQuery;
    quCashSail: TFDQuery;
    quCashSailFSRARID: TStringField;
    quCashSailIDATE: TIntegerField;
    quCashSailZDATE: TSQLTimeStampField;
    quCashSailCASHNUM: TIntegerField;
    quCashSailZNUM: TIntegerField;
    quCashSailCHNUM: TIntegerField;
    quCashSailPOSID: TIntegerField;
    quCashSailID: TLargeintField;
    quCashSailISHOP: TIntegerField;
    quCashSailDEPART: TIntegerField;
    quCashSailICODE: TIntegerField;
    quCashSailBARCODE: TStringField;
    quCashSailNAMEC: TStringField;
    quCashSailAVID: TIntegerField;
    quCashSailKREP: TSingleField;
    quCashSailVOL: TSingleField;
    quCashSailPRODID: TStringField;
    quCashSailIMPORTERID: TStringField;
    quCashSailNAMECLI: TStringField;
    quCashSailCLIENTINN: TStringField;
    quCashSailCLIENTKPP: TStringField;
    quCashSailQUANT: TSingleField;
    quCashSailPRICER: TSingleField;
    quCashSailSUMR: TSingleField;
    quCashSailAMARK: TStringField;
    quCashSailKAP: TStringField;
    quCashSailCHDATE: TSQLTimeStampField;
    dsquCashSail: TDataSource;
    ViewDocCashFSRARID: TcxGridDBColumn;
    ViewDocCashIDATE: TcxGridDBColumn;
    ViewDocCashZDATE: TcxGridDBColumn;
    ViewDocCashCASHNUM: TcxGridDBColumn;
    ViewDocCashZNUM: TcxGridDBColumn;
    ViewDocCashCHNUM: TcxGridDBColumn;
    ViewDocCashPOSID: TcxGridDBColumn;
    ViewDocCashID: TcxGridDBColumn;
    ViewDocCashISHOP: TcxGridDBColumn;
    ViewDocCashDEPART: TcxGridDBColumn;
    ViewDocCashICODE: TcxGridDBColumn;
    ViewDocCashBARCODE: TcxGridDBColumn;
    ViewDocCashNAMEC: TcxGridDBColumn;
    ViewDocCashAVID: TcxGridDBColumn;
    ViewDocCashKREP: TcxGridDBColumn;
    ViewDocCashVOL: TcxGridDBColumn;
    ViewDocCashPRODID: TcxGridDBColumn;
    ViewDocCashIMPORTERID: TcxGridDBColumn;
    ViewDocCashNAMECLI: TcxGridDBColumn;
    ViewDocCashCLIENTINN: TcxGridDBColumn;
    ViewDocCashCLIENTKPP: TcxGridDBColumn;
    ViewDocCashQUANT: TcxGridDBColumn;
    ViewDocCashPRICER: TcxGridDBColumn;
    ViewDocCashSUMR: TcxGridDBColumn;
    ViewDocCashAMARK: TcxGridDBColumn;
    ViewDocCashKAP: TcxGridDBColumn;
    ViewDocCashCHDATE: TcxGridDBColumn;
    quCashSailCLITYPE: TStringField;
    ViewDocCashCLITYPE: TcxGridDBColumn;
    procedure acRefreshExecute(Sender: TObject);
    procedure acCloseExecute(Sender: TObject);
    procedure acSelectDateExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure deDateBegChange(Sender: TObject);
    procedure acExpExcelExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acGetCashSailExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }

    procedure Init;
  end;

var
  fmDocCash: TfmDocCash;

procedure ShowFormDocCash;

implementation

{$R *.dfm}

uses ViewXML, EgaisDecode, ViewCards, dmRar, UTMExch, ViewSpecOut, MCrystDocs, ProDocs, History, UTMArh, UTMCheck, DocHeaderCB,
  ViewSpec, Main, ViewSpecVn;

procedure ShowFormDocCash;
begin
  if Assigned(fmDocCash)=False then //����� �� ����������, � ����� �������
    fmDocCash:=TfmDocCash.Create(Application);
  if fmDocCash.WindowState=wsMinimized then //���� ���� ��������, �� ������������� ���
    fmDocCash.WindowState:=wsNormal;

  fmDocCash.deDateBeg.Date:=Date-7;
  fmDocCash.deDateEnd.Date:=Date;
  fmDocCash.Init;
  fmDocCash.Show;
end;

procedure TfmDocCash.Init;
var
  FocusedRow, TopRow: Integer;
  flag: boolean;
begin
  with dmR do
  begin
    try
      //<--Refresh � ��������������� ������� ������� � �������
      flag:=quDocsVnHD.Active;  //���������� ���� �� ������� �������, ��� �������������� ������� �������
      TopRow := ViewDocCash.Controller.TopRowIndex;
      FocusedRow := ViewDocCash.DataController.FocusedRowIndex;
      //-->
      ViewDocCash.BeginUpdate;

      quCashSail.Active:=False;
      quCashSail.ParamByName('IDATEB').AsInteger:=Trunc(deDateBeg.Date);
      quCashSail.ParamByName('IDATEE').AsInteger:=Trunc(deDateEnd.Date);
      quCashSail.ParamByName('RARID').AsString:=CommonSet.FSRAR_ID;
      quCashSail.Active:=True;
      quCashSail.First;

      //-->
    finally
      ViewDocCash.EndUpdate;
      //<--��������������� �������
      if flag then begin
        ViewDocCash.DataController.FocusedRowIndex := FocusedRow;
        ViewDocCash.Controller.TopRowIndex := TopRow;
      end;
      //-->
    end;
  end;

//  FSRAR_ID:=CommonSet.FSRAR_ID;
end;


procedure TfmDocCash.acCloseExecute(Sender: TObject);
begin
  //�������
  Close;
//  fmMain.Close;
end;

procedure TfmDocCash.acExpExcelExecute(Sender: TObject);
begin
  ExportGridToFile(formatdatetime('ddmmyyyy',deDateBeg.Date)+'_'+formatdatetime('ddmmyyyy',deDateEnd.Date)+'_'+formatdatetime('ddmmyyyy_hh_nn_ss',now)+'.xls', GridDocCash);
end;

procedure TfmDocCash.acRefreshExecute(Sender: TObject);
begin
  //������
  Init;
end;

procedure TfmDocCash.acSelectDateExecute(Sender: TObject);
begin
  //��������
  Init;
end;

procedure TfmDocCash.acGetCashSailExecute(Sender: TObject);
var i,n,l:Integer;
  Xml: TNativeXml;
  XMLText:string;
  nodeRoot,nodeCh,nodePos: TXmlNode;

  IShop,CashNum,ZNum,iDate:Integer;
  dDate:TDateTime;
  StrWk:string;

  FName:string;

  iDep,ChNum,iOper,k:Integer;
  AMark:string;
  iCode,NumPos:Integer;
  BarCode,RARID:string;
  PriceR,SumR,Quant:Real;
  CHDate:TDateTime;
begin
  //������� ����������
  ClearMessageLogLocal;
  OpenDialog1.Execute;
  ShowMessageLogLocal('����� ... ���� ��������� ����������.');

  for i:=0 to OpenDialog1.Files.Count-1 do
  begin
    ShowMessageLogLocal('   '+OpenDialog1.Files.Strings[i]);
    try
      FName:=OpenDialog1.Files.Strings[i];

      Xml:= TNativeXml.Create(nil);
      Xml.XmlFormat := xfReadable;
      Xml.LoadFromFile(FName);

      //���������
      nodeRoot:=Xml.Root;
      iShop:=0; CashNum:=0; ZNum:=0;

      IShop:=StrToIntDef(nodeRoot.ReadAttributeString('storeId',''),0);
      CashNum:=StrToIntDef(nodeRoot.ReadAttributeString('posNum',''),0);
      ZNum:=StrToIntDef(nodeRoot.ReadAttributeString('shiftNum',''),0);

      if (IShop>0)and(CashNum>0)and(ZNum>0) then
      begin
        //������� ����� �� ��������
        quDelZ.Active:=False;

        quDelZ.SQL.Clear;
        quDelZ.SQL.Add('');
        quDelZ.SQL.Add('DECLARE @ISHOP int = '+its(iShop));
        quDelZ.SQL.Add('DECLARE @CASHNUM int = '+its(CashNum));
        quDelZ.SQL.Add('DECLARE @ZNUM int = '+its(ZNum));
        quDelZ.SQL.Add('EXECUTE [dbo].[prDelZ] @ISHOP,@CASHNUM,@ZNUM');

        quDelZ.ExecSQL;


        quS.Active:=False;
        quS.SQL.Clear;
        quS.SQL.Add('SELECT TOP 1 [RARID] FROM [dbo].[EGAISID] where [ISHOP]='+its(iShop));
        quS.Active:=True;
        quS.First;
        RARID:=quS.FieldByName('RARID').AsString;
        quS.Active:=False;

        //������� ������������� ������
        StrWk:='';
        if Assigned(nodeRoot.NodeByName('dateClose')) then StrWk:=nodeRoot.NodeByName('dateClose').Value;
        dDate:=dsfromrar(StrWk);     //dtsfromrar  ���� + �����
        iDate:=Trunc(dDate);

        if (RARID>'')and(iDate>0) then
        begin
          for n:=0 to nodeRoot.NodeCount-1 do
          begin
            nodeCh:=nodeRoot[n];
            if Pos('receipt',nodeCh.Name)>0 then  //��� ���
            begin
              ChNum:=StrToIntDef(nodeCh.ReadAttributeString('receiptNum',''),0);

              CHDate:=dDate;
              if Assigned(nodeCh.NodeByName('receiptDateTime')) then CHDate:=dtsfromrar(nodeCh.NodeByName('receiptDateTime').Value);

              iOper:=0;
              if Assigned(nodeCh.NodeByName('type')) then iOper:=StrToIntDef(nodeCh.NodeByName('type').Value,0);
              if iOper=0 then k:=1 else k:=-1; //����� ��� ��������� ��� �������� ��������

              NumPos:=0;

              for l:=0 to nodeCh.NodeCount-1 do
              begin
                nodePos:=nodeCh[l];
                if Pos('item',nodePos.Name)>0 then  //��� ������� ����
                begin
                  inc(NumPos);
                  AMark:='';
                  if Assigned(nodePos.NodeByName('egaisBarcode')) then AMark:=nodePos.NodeByName('egaisBarcode').Value;
                  if AMark>'' then
                  begin
                    iCode:=0; BarCode:=''; PriceR:=0; SumR:=0; Quant:=0;
                    if Assigned(nodePos.NodeByName('article')) then iCode:=StrToIntDef(nodePos.NodeByName('article').Value,0);
                    if Assigned(nodePos.NodeByName('barcode')) then BarCode:=nodePos.NodeByName('barcode').Value;
                    if Assigned(nodePos.NodeByName('quantity')) then Quant:=stf(nodePos.NodeByName('quantity').Value);
                    if Assigned(nodePos.NodeByName('price')) then PriceR:=stf(nodePos.NodeByName('price').Value);
                    if Assigned(nodePos.NodeByName('total')) then SumR:=stf(nodePos.NodeByName('total').Value);
                    if Assigned(nodePos.NodeByName('stockId')) then iDep:=StrToIntDef(nodePos.NodeByName('stockId').Value,0);

//                    ShowMessageLogLocal('    '+RARID+' '+its(iShop)+' '+its(CashNum)+' '+its(ZNum)+' '+its(ChNum)+' '+its(NumPos)+' '+its(iCode)+' '+AMark);
                    try
                      quA.Active:=False;
                      quA.SQL.Clear;
                      quA.SQL.Add('');

                      quA.SQL.Add('INSERT INTO [dbo].[ACASHSAIL]');
                      quA.SQL.Add('           ([FSRARID],[IDATE],[CASHNUM],[ZNUM],[CHNUM],[POSID],[ISHOP],[DEPART],[ICODE],[BARCODE],[QUANT],[PRICER],[SUMR],[AMARK],[KAP],[CHDATE])');
                      quA.SQL.Add('     VALUES');
                      quA.SQL.Add('           ('''+RARID+'''');
                      quA.SQL.Add('           ,'+its(iDate));
                      quA.SQL.Add('           ,'+its(CashNum));
                      quA.SQL.Add('           ,'+its(ZNum));
                      quA.SQL.Add('           ,'+its(ChNum));
                      quA.SQL.Add('           ,'+its(NumPos));
                      quA.SQL.Add('           ,'+its(iShop));
                      quA.SQL.Add('           ,'+its(iDep));
                      quA.SQL.Add('           ,'+its(iCode));
                      quA.SQL.Add('           ,'''+BarCode+'''');
                      quA.SQL.Add('           ,'+fts(Quant*k));
                      quA.SQL.Add('           ,'+fts(PriceR));
                      quA.SQL.Add('           ,'+fts(SumR*k));
                      quA.SQL.Add('           ,'''+AMark+'''');
                      quA.SQL.Add('           ,[dbo].[AkcizToKAP]('''+AMark+''')');
                      quA.SQL.Add('           ,'''+dssqltime(CHDate)+''')');
                      quA.ExecSQL;

                    except

                    end;

                  end;
                end;
              end;
            end;
          end;
        end;
      end;
    finally
      Xml.Free;
    end;
  end;
  ShowMessageLogLocal('������� ��������  .');


  {
  if ViewDocVn.Controller.SelectedRecordCount>0 then
  begin
    ClearMessageLogLocal;
    ShowMessageLogLocal('����� ... ���� ��������� ����������.');
    iC:=0;

    FSRARID:='';
    SID:='';
    IDATE:=0;
    STYPE:='';

    for i:=0 to ViewDocVn.Controller.SelectedRecordCount-1 do
    begin
      Rec:=ViewDocVn.Controller.SelectedRecords[i];

      for j:=0 to Rec.ValueCount-1 do
      begin
        if ViewDocVn.Columns[j].Name='ViewDocVnFSRARID' then begin FSRARID:=Rec.Values[j]; end;
        if ViewDocVn.Columns[j].Name='ViewDocVnIDATE' then begin IDATE:=Rec.Values[j]; end;
        if ViewDocVn.Columns[j].Name='ViewDocVnSID' then begin SID:=Rec.Values[j]; end;
        if ViewDocVn.Columns[j].Name='ViewDocVnIACTIVE' then begin IACTIVE:=Rec.Values[j]; end;
        if ViewDocVn.Columns[j].Name='ViewDocVnSTYPE' then begin STYPE:=Rec.Values[j]; end;

      end;
      if (FSRARID>'')and(IDATE>0)and(SID>'')and(IACTIVE=0)  then  //��� ����������� �� 2-�� �������
      begin
        with dmR do
        begin
          ShowMessageLogLocal('    ���. '+FSRARID+','+ds1(iDate)+','+SID);
          if STYPE='ToShop' then
          begin
            if prSendDocVnPrivate(FSRARID,SID,IDATE,MemoLogLocal,dmR.FDConnection,CommonSet.IPUTM) then
            begin
              quS.Active:=False;
              quS.SQL.Clear;
              quS.SQL.Add('');

              quS.SQL.Add('UPDATE dbo.ADOCSVN_HD');
              quS.SQL.Add('SET IACTIVE = 1');
              quS.SQL.Add(',SDATE = GETDATE()');
              quS.SQL.Add('WHERE');
              quS.SQL.Add('  FSRARID = '''+FSRARID+'''');
              quS.SQL.Add('  AND IDATE = '+its(IDATE));
              quS.SQL.Add('  AND SID = '''+SID+'''');
              quS.ExecSQL;
            end;
            inc(iC);
          end;
          if STYPE='FromShop' then
          begin
            if prSendDocVn1Private(FSRARID,SID,IDATE,MemoLogLocal,dmR.FDConnection,CommonSet.IPUTM) then
            begin
              quS.Active:=False;
              quS.SQL.Clear;
              quS.SQL.Add('');

              quS.SQL.Add('UPDATE dbo.ADOCSVN_HD');
              quS.SQL.Add('SET IACTIVE = 1');
              quS.SQL.Add(',SDATE = GETDATE()');
              quS.SQL.Add('WHERE');
              quS.SQL.Add('  FSRARID = '''+FSRARID+'''');
              quS.SQL.Add('  AND IDATE = '+its(IDATE));
              quS.SQL.Add('  AND SID = '''+SID+'''');
              quS.ExecSQL;
            end;
            inc(iC);
          end;
        end;
      end;
    end;
    init;
    ShowMessageLogLocal('���������� '+its(iC)+' ����������.');
    ShowMessageLogLocal('������� ��������.');
  end;
  }
end;

procedure TfmDocCash.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
  fmDocCash:=Nil;
end;

procedure TfmDocCash.FormCreate(Sender: TObject);
begin
  ClearMessageLogLocal;
end;

procedure TfmDocCash.deDateBegChange(Sender: TObject);
begin
  if dmR.FDConnection.Connected then Init;
end;

end.
