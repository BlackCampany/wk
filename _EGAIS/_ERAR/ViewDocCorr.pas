unit ViewDocCorr;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  CustomChildForm, System.UITypes, ShellApi, httpsend, synautil, nativexml, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxRibbonSkins, dxRibbonCustomizationForm, cxContainer, cxEdit, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator,
  Data.DB, cxDBData, cxImageComboBox, cxTextEdit, cxDBLookupComboBox, cxCheckBox, cxCalendar, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.Menus, System.Actions, Vcl.ActnList, dxBar, cxBarEditItem, dxBarExtItems, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid, cxMemo, Vcl.ExtCtrls, dxRibbon;

type
  TfmDocCorr = class(TfmCustomChildForm)
    dxRibbon1: TdxRibbon;
    dxRibbon1Tab1: TdxRibbonTab;
    dxBarManager1: TdxBarManager;
    bmApplyDate: TdxBar;
    bmClose: TdxBar;
    btnDone: TdxBarLargeButton;
    btnClose: TdxBarLargeButton;
    deDateBeg: TdxBarDateCombo;
    deDateEnd: TdxBarDateCombo;
    dxBarGroup1: TdxBarGroup;
    ActionList1: TActionList;
    acRefresh: TAction;
    acClose: TAction;
    acSelectDate: TAction;
    GridDocCorr: TcxGrid;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    beiPoint: TcxBarEditItem;
    acExpExcel: TAction;
    dxBarLargeButton9: TdxBarLargeButton;
    dxBarLargeButton10: TdxBarLargeButton;
    beiGetTTN: TcxBarEditItem;
    dxBarLargeButton14: TdxBarLargeButton;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    LevelDocCorr: TcxGridLevel;
    ViewDocCorr: TcxGridDBTableView;
    ViewDocCorrFSRARID: TcxGridDBColumn;
    ViewDocCorrNUMBER: TcxGridDBColumn;
    ViewDocCorrIACTIVE: TcxGridDBColumn;
    pmDocVn: TPopupMenu;
    N17: TMenuItem;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton11: TdxBarLargeButton;
    ViewDocCorrSID: TcxGridDBColumn;
    ViewDocCorrSDATE: TcxGridDBColumn;
    ViewDocCorrSTYPE: TcxGridDBColumn;
    ViewDocCorrDESCR: TcxGridDBColumn;
    ViewDocCorrFIXNUMBER: TcxGridDBColumn;
    ViewDocCorrFIXDATE: TcxGridDBColumn;
    ViewDocCorrSENDXML: TcxGridDBColumn;
    ViewDocCorrTT1: TcxGridDBColumn;
    ViewDocCorrTT2: TcxGridDBColumn;
    ViewDocCorrIDATE: TcxGridDBColumn;
    acSendCorr: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    acResetStatus: TAction;
    N1: TMenuItem;
    acTestRemn: TAction;
    N2: TMenuItem;
    acAddDoc: TAction;
    N3: TMenuItem;
    N4: TMenuItem;
    acDelDoc: TAction;
    N5: TMenuItem;
    procedure acRefreshExecute(Sender: TObject);
    procedure acCloseExecute(Sender: TObject);
    procedure acSelectDateExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure deDateBegChange(Sender: TObject);
    procedure acExpExcelExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ViewDocCorrDblClick(Sender: TObject);
    procedure acSendCorrExecute(Sender: TObject);
    procedure acResetStatusExecute(Sender: TObject);
    procedure acTestRemnExecute(Sender: TObject);
    procedure acAddDocExecute(Sender: TObject);
    procedure acDelDocExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }

    procedure Init;
  end;

var
  fmDocCorr: TfmDocCorr;

procedure ShowFormCorr;

implementation

{$R *.dfm}

uses ViewXML, EgaisDecode, ViewCards, dmRar, UTMExch, ViewSpecOut, MCrystDocs, ProDocs, History, UTMArh, UTMCheck, DocHeaderCB,
  ViewSpec, Main, ViewSpecCorr, TestRemnInv2;

procedure ShowFormCorr;
begin
  if Assigned(fmDocCorr)=False then //����� �� ����������, � ����� �������
    fmDocCorr:=TfmDocCorr.Create(Application);
  if fmDocCorr.WindowState=wsMinimized then //���� ���� ��������, �� ������������� ���
    fmDocCorr.WindowState:=wsNormal;

  fmDocCorr.deDateBeg.Date:=Date-7;
  fmDocCorr.deDateEnd.Date:=Date;
  fmDocCorr.Init;
  fmDocCorr.Show;
end;

procedure TfmDocCorr.Init;
var
  FocusedRow, TopRow: Integer;
  flag: boolean;
begin
  with dmR do
  begin
    try
      //<--Refresh � ��������������� ������� ������� � �������
      flag:=quDocsCorr.Active;  //���������� ���� �� ������� �������, ��� �������������� ������� �������
      TopRow := ViewDocCorr.Controller.TopRowIndex;
      FocusedRow := ViewDocCorr.DataController.FocusedRowIndex;
      //-->
      ViewDocCorr.BeginUpdate;
      quDocsCorr.Active:=False;
      quDocsCorr.ParamByName('IDATEB').AsInteger:=Trunc(deDateBeg.Date);
      quDocsCorr.ParamByName('IDATEE').AsInteger:=Trunc(deDateEnd.Date);
      quDocsCorr.ParamByName('RARID').AsString:=CommonSet.FSRAR_ID;
      quDocsCorr.Active:=True;
      quDocsCorr.First;
      //-->
    finally
      ViewDocCorr.EndUpdate;
      //<--��������������� �������
      if flag then begin
        ViewDocCorr.DataController.FocusedRowIndex := FocusedRow;
        ViewDocCorr.Controller.TopRowIndex := TopRow;
      end;
      //-->
    end;
  end;
end;


procedure TfmDocCorr.ViewDocCorrDblClick(Sender: TObject);
begin
//  ShowSpecViewOut(RARID,SID,FSDATE,FNUMBER,FDESCR:string;IDATE,IEDIT:Integer);
  // ������������
  with dmR do
  begin
    if quDocsCorr.RecordCount>0 then
    begin
      if (ViewDocCorr.Controller.FocusedColumn.Name='ViewDocCorrSENDXML') then
      begin
        quDocsCorrRec.Active:=False;
        quDocsCorrRec.ParamByName('RARID').AsString:=quDocsCorrFSRARID.AsString;
        quDocsCorrRec.ParamByName('IDATE').AsInteger:=quDocsCorrIDATE.AsInteger;
        quDocsCorrRec.ParamByName('SID').AsString:=quDocsCorrSID.AsString;
        quDocsCorrRec.Active:=True;

        if quDocsCorrRec.RecordCount>0 then
            ShowXMLView(Abs(quDocsCorrRecIDATE.AsInteger),quDocsCorrRecSENDXML.AsString);
        Exit;
      end;

      if (ViewDocCorr.Controller.FocusedColumn.Name='ViewDocCorrTT1') then
      begin
        if (Abs(quDocsCorrTICK1.AsInteger)>0) then
        begin
          quReplyRec.Active:=False;
          quReplyRec.SQL.Clear;
          quReplyRec.SQL.Add('SELECT * FROM dbo.REPLYLIST');
          quReplyRec.SQL.Add('  WHERE ID='+its(Abs(quDocsCorrTICK1.AsInteger)));
          quReplyRec.SQL.Add('  and FSRARID='''+quDocsCorrFSRARID.AsString+'''');
          quReplyRec.Active:=True;

          if quReplyRec.RecordCount>0 then
            ShowXMLView(quDocsCorrTICK1.AsInteger,quReplyRecReplyFile.AsString);
        end;
        Exit;
      end;

      if (ViewDocCorr.Controller.FocusedColumn.Name='ViewDocCorrTT2') then
      begin
        if Abs(quDocsCorrTICK2.AsInteger)>0 then
        begin
          quReplyRec.Active:=False;
          quReplyRec.SQL.Clear;
          quReplyRec.SQL.Add('SELECT * FROM dbo.REPLYLIST');
          quReplyRec.SQL.Add('  WHERE ID='+its(Abs(quDocsCorrTICK2.AsInteger)));
          quReplyRec.SQL.Add('  and FSRARID='''+quDocsCorrFSRARID.AsString+'''');
          quReplyRec.Active:=True;

          if quReplyRec.RecordCount>0 then
            ShowXMLView(quDocsCorrTICK2.AsInteger,quReplyRecReplyFile.AsString);
        end;
        Exit;
      end;

      if quDocsCorrIACTIVE.AsInteger=0 then
         ShowSpecViewCorr(quDocsCorrFSRARID.AsString,quDocsCorrSID.AsString,quDocsCorrNUMBER.AsString,quDocsCorrDESCR.AsString,quDocsCorrIDATE.AsInteger,1,0)
      else
         ShowSpecViewCorr(quDocsCorrFSRARID.AsString,quDocsCorrSID.AsString,quDocsCorrNUMBER.AsString,quDocsCorrDESCR.AsString,quDocsCorrIDATE.AsInteger,0,0);
    end;
  end;
end;

procedure TfmDocCorr.acAddDocExecute(Sender: TObject);
Var     a: TGUID;
    sSID,sNum:string;
begin
  //�������� �������� �������� � ��������� ����
  CreateGUID(a);
  sSID:=GUIDTostring(a);
  sNum:=its(fGetId(7));

  ShowSpecViewCorr(CommonSet.FSRAR_ID,sSID,sNum,'����������',Trunc(Date),1,1);
end;

procedure TfmDocCorr.acCloseExecute(Sender: TObject);
begin
  //�������
  Close;
//  fmMain.Close;
end;

procedure TfmDocCorr.acDelDocExecute(Sender: TObject);
begin
  //������� ��������
  with dmR do
  begin
    if quDocsCorr.RecordCount>0 then
    begin
      if MessageDlg('������� �������� ������� '+quDocsCorrNUMBER.AsString+' �� '+ds1(quDocsCorrIDATE.AsInteger)+'?',mtConfirmation, [mbYes, mbNo], 0, mbYes) = mrYes then
      begin
        ClearMessageLogLocal;
        ShowMessageLogLocal('�����.. ���� �������� ���������');

        if quDocsCorrIACTIVE.AsInteger=0 then
        begin
          try
            quS.Active:=False;
            quS.SQL.Clear;
            quS.SQL.Add('');

            quS.SQL.Add('delete from ADOCSCORR_HD');
            quS.SQL.Add('WHERE');
            quS.SQL.Add('  FSRARID = '''+quDocsCorrFSRARID.AsString+'''');
            quS.SQL.Add('  AND IDATE = '+its(quDocsCorrIDATE.AsInteger));
            quS.SQL.Add('  AND SID = '''+quDocsCorrSID.AsString+'''');
            quS.ExecSQL;

            Init;
          except
          end;
        end else ShowMessageLogLocal('  �������� ������ ���������. �������� ����������.');


        ShowMessageLogLocal('������� ��������.');
      end;
    end;
  end;
end;

procedure TfmDocCorr.acExpExcelExecute(Sender: TObject);
begin
  ExportGridToFile(formatdatetime('ddmmyyyy',deDateBeg.Date)+'_'+formatdatetime('ddmmyyyy',deDateEnd.Date)+'_'+formatdatetime('ddmmyyyy_hh_nn_ss',now)+'.xls', GridDocCorr);
end;

procedure TfmDocCorr.acRefreshExecute(Sender: TObject);
begin
  //������
  Init;
end;

procedure TfmDocCorr.acResetStatusExecute(Sender: TObject);
begin
  //
  with dmR do
  begin
    if quDocsCorr.RecordCount>0 then
    begin
      quS.Active:=False;
      quS.SQL.Clear;
      quS.SQL.Add('');

      quS.SQL.Add('UPDATE dbo.ADOCSCORR_HD');
      quS.SQL.Add('SET IACTIVE = 0');
      quS.SQL.Add('WHERE');
      quS.SQL.Add('  FSRARID = '''+quDocsCorrFSRARID.AsString+'''');
      quS.SQL.Add('  AND IDATE = '+its(quDocsCorrIDATE.AsInteger));
      quS.SQL.Add('  AND SID = '''+quDocsCorrSID.AsString+'''');

      quS.ExecSQL;

      Init;
    end;
  end;
end;

procedure TfmDocCorr.acSelectDateExecute(Sender: TObject);
begin
  //��������
  Init;
end;

procedure TfmDocCorr.acSendCorrExecute(Sender: TObject);
var Rec:TcxCustomGridRecord;
    i,j,iC:Integer;
    FSRARID,SID:string;
    IDATE,IACTIVE:Integer;
begin
  //��������� � ����� ����������
  if ViewDocCorr.Controller.SelectedRecordCount>0 then
  begin
    ClearMessageLogLocal;
    ShowMessageLogLocal('����� ... ���� ��������� ����������.');
    iC:=0;

    FSRARID:='';
    SID:='';
    IDATE:=0;

    for i:=0 to ViewDocCorr.Controller.SelectedRecordCount-1 do
    begin
      Rec:=ViewDocCorr.Controller.SelectedRecords[i];

      for j:=0 to Rec.ValueCount-1 do
      begin
        if ViewDocCorr.Columns[j].Name='ViewDocCorrFSRARID' then begin FSRARID:=Rec.Values[j]; end;
        if ViewDocCorr.Columns[j].Name='ViewDocCorrIDATE' then begin IDATE:=Rec.Values[j]; end;
        if ViewDocCorr.Columns[j].Name='ViewDocCorrSID' then begin SID:=Rec.Values[j]; end;
        if ViewDocCorr.Columns[j].Name='ViewDocCorrIACTIVE' then begin IACTIVE:=Rec.Values[j]; end;
      end;
      if (FSRARID>'')and(IDATE>0)and(SID>'')and(IACTIVE=0) then
      begin
        with dmR do
        begin
          ShowMessageLogLocal('    ���. '+FSRARID+','+ds1(iDate)+','+SID);

           if prSendDocCorr(FSRARID,SID,IDATE,MemoLogLocal) then
           begin
             quS.Active:=False;
             quS.SQL.Clear;
             quS.SQL.Add('');

             quS.SQL.Add('UPDATE dbo.ADOCSCORR_HD');
             quS.SQL.Add('SET IACTIVE = 1');
             quS.SQL.Add(',SDATE = GETDATE()');
             quS.SQL.Add('WHERE');
             quS.SQL.Add('  FSRARID = '''+FSRARID+'''');
             quS.SQL.Add('  AND IDATE = '+its(IDATE));
             quS.SQL.Add('  AND SID = '''+SID+'''');

             quS.ExecSQL;
           end;
          inc(iC);
        end;
      end;
    end;
    init;
    ShowMessageLogLocal('���������� '+its(iC)+' ����������.');
    ShowMessageLogLocal('������� ��������.');
  end;
end;

procedure TfmDocCorr.acTestRemnExecute(Sender: TObject);
begin
  //��������� �������
  with dmR do
  begin
    if quDocsCorr.RecordCount>0 then
    begin
      quTestRemnInv2.Active:=False;
      quTestRemnInv2.ParamByName('RARID').AsString:=quDocsCorrFSRARID.AsString;
      quTestRemnInv2.ParamByName('SID').AsString:=quDocsCorrSID.AsString;
      quTestRemnInv2.ParamByName('IDATE').AsInteger:=quDocsCorrIDATE.AsInteger;
      quTestRemnInv2.Active:=True;

      if Assigned(fmTestRemnInv2)=False then //����� �� ����������, � ����� �������
        fmTestRemnInv2:=TfmTestRemnInv2.Create(Application);
      if fmTestRemnInv2.WindowState=wsMinimized then //���� ���� ��������, �� ������������� ���
        fmTestRemnInv2.WindowState:=wsNormal;

      fmTestRemnInv2.Show;
    end;
  end;
end;

procedure TfmDocCorr.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
  fmDocCorr:=Nil;
end;

procedure TfmDocCorr.FormCreate(Sender: TObject);
begin
  ClearMessageLogLocal;
end;

procedure TfmDocCorr.deDateBegChange(Sender: TObject);
begin
  if dmR.FDConnection.Connected then Init;
end;

end.
