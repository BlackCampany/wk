unit AddCards;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ComCtrls, cxControls, cxContainer, cxEdit,
  cxRadioGroup, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalc, StdCtrls,
  cxLookAndFeelPainters, cxButtons, cxButtonEdit, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, DB, cxDBData, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, SpeedBar, ActnList, XPStyleActnCtrls, ActnMan,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, Menus, cxGroupBox,
  cxCheckBox, cxCheckListBox, cxSpinEdit, ADODB;

type
  TfmAddCard = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    RadioGroup1: TcxRadioGroup;
    RadioGroup2: TcxRadioGroup;
    Label1: TLabel;
    Label2: TLabel;
    MaskEdit1: TcxMaskEdit;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    MaskEdit2: TcxMaskEdit;
    Label3: TLabel;
    Label4: TLabel;
    TextEdit1: TcxTextEdit;
    TextEdit2: TcxTextEdit;
    Label5: TLabel;
    Label6: TLabel;
    ButtonEdit1: TcxButtonEdit;
    amCards: TActionManager;
    acBarRead: TAction;
    acTextEdit2: TAction;
    acClear: TAction;
    Label8: TLabel;
    cxButton4: TcxButton;
    Label14: TLabel;
    cxLookupComboBox3: TcxLookupComboBox;
    Label16: TLabel;
    cxCalcEdit3: TcxCalcEdit;
    cxCalcEdit4: TcxCalcEdit;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label26: TLabel;
    cxSpinEdit1: TcxSpinEdit;
    Label27: TLabel;
    cxComboBox1: TcxComboBox;
    GroupBox1: TGroupBox;
    cxCalcEdit7: TcxCalcEdit;
    Label28: TLabel;
    Label29: TLabel;
    Label31: TLabel;
    Label33: TLabel;
    cxCalcEdit2: TcxCalcEdit;
    Label37: TLabel;
    cxCalcEdit6: TcxCalcEdit;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    mSostav: TMemo;
    mGost: TMemo;
    Label11: TLabel;
    Label17: TLabel;
    GroupBox4: TGroupBox;
    Label24: TLabel;
    Label32: TLabel;
    cxButtonEdit1: TcxButtonEdit;
    Label7: TLabel;
    cxCalcEdit13: TcxCalcEdit;
    cxCalcEdit5: TcxCalcEdit;
    cxCalcEdit12: TcxCalcEdit;
    cxCheckBox2: TcxCheckBox;
    cxCheckBox1: TcxCheckBox;
    cxLookupComboBox1: TcxLookupComboBox;
    cxLookupComboBox2: TcxLookupComboBox;
    cxLookupComboBox4: TcxLookupComboBox;
    Label9: TLabel;
    cxLookupComboBox5: TcxLookupComboBox;
    cxComboBox2: TcxComboBox;
    cxSpinEdit2: TcxSpinEdit;
    cxSpinEdit3: TcxSpinEdit;
    cxSpinEdit4: TcxSpinEdit;
    quCateg: TADOQuery;
    dsquCateg: TDataSource;
    quCategID: TIntegerField;
    quCategSID: TStringField;
    quCategCOMMENT: TStringField;
    quCountry: TADOQuery;
    dsquCountry: TDataSource;
    quCountryIDPARENT: TIntegerField;
    quCountryID: TIntegerField;
    quCountryNAME: TStringField;
    quCountryKOD: TIntegerField;
    quCatMan: TADOQuery;
    dsquCatMan: TDataSource;
    quCatManID: TIntegerField;
    quCatManID_PARENT: TIntegerField;
    quCatManNAME: TStringField;
    quCatManUVOLNEN: TSmallintField;
    quCatManPASSW: TStringField;
    quCatManMODUL1: TSmallintField;
    quCatManMODUL2: TSmallintField;
    quCatManMODUL3: TSmallintField;
    quCatManMODUL4: TSmallintField;
    quCatManMODUL5: TSmallintField;
    quCatManMODUL6: TSmallintField;
    quCatManBARCODE: TStringField;
    quCatManPATHEXP: TStringField;
    quCatManHOSTFTP: TStringField;
    quCatManLOGINFTP: TStringField;
    quCatManPASSWFTP: TStringField;
    quCatManSCLI: TStringField;
    quCatManALLSHOPS: TSmallintField;
    quCatManCATMAN: TSmallintField;
    quCatManPOSTACCESS: TSmallintField;
    quBrands: TADOQuery;
    dsquBrands: TDataSource;
    quBrandsID: TIntegerField;
    quBrandsNAMEBRAND: TStringField;
    quAVid: TADOQuery;
    dsquAVid: TDataSource;
    quAVidID: TIntegerField;
    quAVidNAME: TStringField;
    quAVidAFM: TSmallintField;
    cxCalcEdit1: TcxCalcEdit;
    Label10: TLabel;
    cxComboBox3: TcxComboBox;
    Label12: TLabel;
    procedure RadioGroup1PropertiesChange(Sender: TObject);
    procedure RadioGroup2PropertiesChange(Sender: TObject);
    procedure acBarReadExecute(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure acTextEdit2Execute(Sender: TObject);
    procedure acClearExecute(Sender: TObject);
    procedure ButtonEdit2PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure MaskEdit2PropertiesEditValueChanged(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure prSetEditCard(iT:SmallInt);

  end;

var
  fmAddCard: TfmAddCard;

implementation

uses Dm, Un1, Cards, ClasSel, Makers;

{$R *.dfm}

procedure TfmAddCard.prSetEditCard(iT:SmallInt);
Var sM:String;
    iItem:Integer;
begin
  quCateg.Active:=False; quCateg.Active:=True; quCateg.First;
  quCountry.Active:=False; quCountry.Active:=True; quCountry.First; quCountry.Locate('NAME','������',[loCaseInsensitive]);
  quCatMan.Active:=False; quCatMan.Parameters.ParamByName('ICATMAN').Value:=Commonset.CatManGroup; quCatMan.Active:=True; quCatMan.First;
  quBrands.Active:=False; quBrands.Active:=True; quBrands.First;
  quAVid.Active:=False; quAVid.Active:=True; quAVid.First;

  if iT=0 then
  begin
    fmAddCard.Tag:=0;
    RadioGroup1.Enabled:=True;
    RadioGroup2.Enabled:=True;
    MaskEdit1.Text:=its(prFindNewArt(False,sM)); MaskEdit1.Properties.ReadOnly:=False; //��� ������
    MaskEdit2.Text:=''; MaskEdit2.Properties.ReadOnly:=False; //�� ������
    cxButton2.Enabled:=True;
    TextEdit1.Text:=''; TextEdit1.Properties.ReadOnly:=False; //��������
    TextEdit2.Text:=''; TextEdit2.Properties.ReadOnly:=False; //������ ��������
    ButtonEdit1.Text:=fmCards.CardsTree.Selected.Text; ButtonEdit1.Tag:=Integer(fmCards.CardsTree.Selected.Data); ButtonEdit1.Properties.ReadOnly:=False; ButtonEdit1.Enabled:=True;
    cxButton4.Enabled:=True; //��������� ������ ����
    cxComboBox2.ItemIndex:=0; cxComboBox2.Properties.ReadOnly:=False; //���
    cxComboBox1.ItemIndex:=0; cxComboBox2.Properties.ReadOnly:=False; //������ �������� � �����
    cxComboBox3.ItemIndex:=0; cxComboBox2.Properties.ReadOnly:=False; //��� �����-����

    cxCalcEdit1.Value:=0; cxCalcEdit1.Properties.ReadOnly:=False; //�������
    cxCalcEdit2.Value:=0; cxCalcEdit2.Properties.ReadOnly:=False; //�����
    cxCalcEdit3.Value:=0; cxCalcEdit3.Properties.ReadOnly:=False; //�����
    cxCalcEdit4.Value:=0; cxCalcEdit4.Properties.ReadOnly:=False; //������
    cxCalcEdit5.Value:=0; cxCalcEdit5.Properties.ReadOnly:=False; //������
    cxCalcEdit6.Value:=0; cxCalcEdit6.Properties.ReadOnly:=False; //���

    cxSpinEdit1.Value:=0; cxSpinEdit1.Properties.ReadOnly:=False;  //������ � �����

    cxCalcEdit7.Value:=0; cxCalcEdit7.Properties.ReadOnly:=False;  //���������� ����������� ��� ������
    cxSpinEdit2.Value:=0; cxSpinEdit2.Properties.ReadOnly:=False;  //��������� ����� ����
    cxSpinEdit3.Value:=0; cxSpinEdit3.Properties.ReadOnly:=False;  //���-�� ���� ��� �������
    cxSpinEdit4.Value:=0; cxSpinEdit4.Properties.ReadOnly:=False;  //���� ����������
    cxCalcEdit12.Value:=0; cxCalcEdit12.Properties.ReadOnly:=False;  //����� � 1-�� ������ �����

    mSostav.Clear; mSostav.ReadOnly:=False;
    mGost.Clear;  mGost.ReadOnly:=False;
    cxButtonEdit1.Text:=''; cxButtonEdit1.Tag:=0; cxButtonEdit1.Properties.ReadOnly:=False; cxButtonEdit1.Enabled:=True; //������������� ����
    cxCalcEdit13.Value:=0; cxCalcEdit13.Properties.ReadOnly:=False; //��������

    cxCheckBox1.Checked:=False; cxCheckBox1.Properties.ReadOnly:=False; // ������
    cxCheckBox2.Checked:=False; cxCheckBox2.Properties.ReadOnly:=False; // ���

    cxLookupComboBox1.Properties.ReadOnly:=False; cxLookupComboBox1.EditValue:=25; // cxLookupComboBox1.Text:='������'; // ������
    cxLookupComboBox2.Properties.ReadOnly:=False; cxLookupComboBox2.Text:=quBrandsNAMEBRAND.AsString; cxLookupComboBox2.EditValue:=quBrandsID.AsInteger; // �����
    cxLookupComboBox3.Properties.ReadOnly:=False; cxLookupComboBox3.Text:=quCatManNAME.AsString; cxLookupComboBox3.EditValue:=quCatManID.AsINteger; // ���. ��������
    cxLookupComboBox4.Properties.ReadOnly:=False; cxLookupComboBox4.Text:=quAVidNAME.AsString; cxLookupComboBox4.EditValue:=quAVidID.AsInteger; // ��� ���������
    cxLookupComboBox5.Properties.ReadOnly:=False; cxLookupComboBox5.Text:=quCategSID.AsString; cxLookupComboBox5.EditValue:=quCategID.AsInteger; // ������ ������
  end;

  if iT=1 then
  begin
    with dmMCS do
    begin
      iItem:=quCardsID.AsInteger;
      fmAddCard.Tag:=iItem;

      quFC.Active:=False;
      quFC.SQL.Clear;
      quFC.SQL.Add('SELECT * FROM [dbo].[CARDS] ca');
      quFC.SQL.Add('where ca.[ID]='+its(iItem));
      quFC.Active:=True;

      if quFC.RecordCount=1 then
      begin
        RadioGroup1.Enabled:=True;
        RadioGroup2.Enabled:=True;

        RadioGroup1.ItemIndex:=quFCEDIZM.AsInteger-1;
        if RadioGroup1.ItemIndex=1 then RadioGroup2.Enabled:=False;

        MaskEdit1.Text:=its(iItem); MaskEdit1.Properties.ReadOnly:=True; //��� ������
        MaskEdit2.Text:=quFCBARCODE.AsString; MaskEdit2.Properties.ReadOnly:=False; //�� ������
        cxButton2.Enabled:=True; //���������
        TextEdit1.Text:=quFCNAME.AsString; TextEdit1.Properties.ReadOnly:=False; //��������
        TextEdit2.Text:=quFCFULLNAME.AsString; TextEdit2.Properties.ReadOnly:=False; //������ ��������

      //SELECT [ID],[IDPARENT],[NAME] FROM [MCRYSTAL].[dbo].[CLASSIF] where [ID]=5
        quF.Active:=False;
        quF.SQL.Clear;
        quF.SQL.Add('SELECT [ID],[IDPARENT],[NAME] FROM [dbo].[CLASSIF] where [ID]='+its(quFCIDCLASSIF.AsInteger));
        quF.Active:=True;
        if quF.RecordCount=1 then ButtonEdit1.Text:=quF.FieldByName('NAME').Value else ButtonEdit1.Text:='';
        quF.Active:=False;
        ButtonEdit1.Tag:=quFCIDCLASSIF.AsInteger; ButtonEdit1.Properties.ReadOnly:=False; ButtonEdit1.Enabled:=True; // �������������

        cxButton4.Enabled:=False; //��������� ������ ����
        if quFCNDS.AsFloat<1 then cxComboBox2.ItemIndex:=2 else
          if quFCNDS.AsFloat>17 then cxComboBox2.ItemIndex:=0 else cxComboBox2.ItemIndex:=1;
        cxComboBox2.Properties.ReadOnly:=False; //���
        cxComboBox1.ItemIndex:=quFCVESFORMAT.AsInteger; cxComboBox2.Properties.ReadOnly:=False; //������ �������� � �����
        cxComboBox3.ItemIndex:=quFCTTOVAR.AsInteger; cxComboBox2.Properties.ReadOnly:=False; //��� �����-����

        cxCalcEdit1.Value:=quFCRNAC.AsFloat; cxCalcEdit1.Properties.ReadOnly:=False; //�������
        cxCalcEdit2.Value:=quFCVOL.AsFloat; cxCalcEdit2.Properties.ReadOnly:=False; //�����
        cxCalcEdit3.Value:=quFCLL.AsInteger/10; cxCalcEdit3.Properties.ReadOnly:=False; //�����
        cxCalcEdit4.Value:=quFCWW.AsInteger/10; cxCalcEdit4.Properties.ReadOnly:=False; //������
        cxCalcEdit5.Value:=quFCHH.AsInteger/10; cxCalcEdit5.Properties.ReadOnly:=False; //������
        cxCalcEdit6.Value:=quFCVES.AsFloat; cxCalcEdit6.Properties.ReadOnly:=False; //���

        cxSpinEdit1.Value:=quFCVESBUTTON.AsInteger; cxSpinEdit1.Properties.ReadOnly:=False;  //������ � �����

        cxCalcEdit7.Value:=quFCAZKOEF.AsFloat; cxCalcEdit7.Properties.ReadOnly:=False;  //���������� ����������� ��� ������
        cxSpinEdit2.Value:=roundex(quFCAZSTRAHR.AsFloat); cxSpinEdit2.Properties.ReadOnly:=False;  //��������� ����� ����
        cxSpinEdit3.Value:=quFCAZDAYSPROC.AsInteger; cxSpinEdit3.Properties.ReadOnly:=False;  //���-�� ���� ��� �������
        cxSpinEdit4.Value:=quFCAZSROKREAL.AsInteger; cxSpinEdit4.Properties.ReadOnly:=False;  //���� ����������
        cxCalcEdit12.Value:=quFCQUANTPACK.AsFloat; cxCalcEdit12.Properties.ReadOnly:=False;  //����� � 1-�� ������ �����

        mSostav.Text:=quFCSOSTAV.AsString; mSostav.ReadOnly:=False;
        mGost.Text:=quFCGOST.AsString;  mGost.ReadOnly:=False;
        cxCalcEdit13.Value:=quFCKREP.AsFloat; cxCalcEdit13.Properties.ReadOnly:=False; //��������

        if quFCISTENDER.AsInteger=1 then cxCheckBox1.Checked:=True else cxCheckBox1.Checked:=False; cxCheckBox1.Properties.ReadOnly:=False; // ������
        if quFCISTOP.AsInteger=1 then cxCheckBox2.Checked:=True else cxCheckBox2.Checked:=False; cxCheckBox2.Properties.ReadOnly:=False; // ������


        quF.Active:=False;
        quF.SQL.Clear;
        quF.SQL.Add('SELECT [ID],[NAME],[INN],[KPP]FROM [dbo].[AMAKER]where [ID]='+its(quFCMAKER.AsInteger));
        quF.Active:=True;
        if quF.RecordCount=1 then cxButtonEdit1.Text:=quF.FieldByName('NAME').Value else cxButtonEdit1.Text:='';
        quF.Active:=False;
        cxButtonEdit1.Tag:=quFCMAKER.AsInteger; cxButtonEdit1.Properties.ReadOnly:=False; cxButtonEdit1.Enabled:=True;//������������� ����

        cxLookupComboBox1.Properties.ReadOnly:=False; cxLookupComboBox1.EditValue:=quFCCOUNTRY.AsInteger; // cxLookupComboBox1.Text:='������'; // ������
        cxLookupComboBox2.Properties.ReadOnly:=False; cxLookupComboBox2.EditValue:=quFCBRAND.AsInteger; // �����
        cxLookupComboBox3.Properties.ReadOnly:=False; cxLookupComboBox3.EditValue:=quFCMANCAT.AsINteger; // ���. ��������
        cxLookupComboBox4.Properties.ReadOnly:=False; cxLookupComboBox4.EditValue:=quFCAVID.AsInteger; // ��� ���������
        cxLookupComboBox5.Properties.ReadOnly:=False; cxLookupComboBox5.EditValue:=quFCCATEG.AsInteger; // ������ ������
      end;
      quFC.Active:=False;
    end;
  end;

  if iT=2 then
  begin
    with dmMCS do
    begin
      iItem:=quCardsID.AsInteger;
      fmAddCard.Tag:=iItem;

      quFC.Active:=False;
      quFC.SQL.Clear;
      quFC.SQL.Add('SELECT * FROM [dbo].[CARDS] ca');
      quFC.SQL.Add('where ca.[ID]='+its(iItem));
      quFC.Active:=True;

      if quFC.RecordCount=1 then
      begin
        RadioGroup1.Enabled:=False;
        RadioGroup2.Enabled:=False;

        RadioGroup1.ItemIndex:=quFCEDIZM.AsInteger-1;
        if RadioGroup1.ItemIndex=1 then RadioGroup2.Enabled:=False;

        MaskEdit1.Text:=its(iItem); MaskEdit1.Properties.ReadOnly:=True; //��� ������
        MaskEdit2.Text:=quFCBARCODE.AsString; MaskEdit2.Properties.ReadOnly:=True; //�� ������
        cxButton2.Enabled:=False; //���������
        TextEdit1.Text:=quFCNAME.AsString; TextEdit1.Properties.ReadOnly:=True; //��������
        TextEdit2.Text:=quFCFULLNAME.AsString; TextEdit2.Properties.ReadOnly:=True; //������ ��������

        quF.Active:=False;
        quF.SQL.Clear;
        quF.SQL.Add('SELECT [ID],[IDPARENT],[NAME] FROM [dbo].[CLASSIF] where [ID]='+its(quFCIDCLASSIF.AsInteger));
        quF.Active:=True;
        if quF.RecordCount=1 then ButtonEdit1.Text:=quF.FieldByName('NAME').Value else ButtonEdit1.Text:='';
        quF.Active:=False;
        ButtonEdit1.Tag:=quFCIDCLASSIF.AsInteger; ButtonEdit1.Properties.ReadOnly:=False; ButtonEdit1.Enabled:=True; // �������������

        cxButton4.Enabled:=False; //��������� ������ ����
        if quFCNDS.AsFloat<1 then cxComboBox2.ItemIndex:=2 else
          if quFCNDS.AsFloat>17 then cxComboBox2.ItemIndex:=0 else cxComboBox2.ItemIndex:=1;
        cxComboBox2.Properties.ReadOnly:=True; //���
        cxComboBox1.ItemIndex:=quFCVESFORMAT.AsInteger; cxComboBox2.Properties.ReadOnly:=True; //������ �������� � �����
        cxComboBox3.ItemIndex:=quFCTTOVAR.AsInteger; cxComboBox2.Properties.ReadOnly:=True; //��� �����-����

        cxCalcEdit1.Value:=quFCRNAC.AsFloat; cxCalcEdit1.Properties.ReadOnly:=True; //�������
        cxCalcEdit2.Value:=quFCVOL.AsFloat; cxCalcEdit2.Properties.ReadOnly:=True; //�����
        cxCalcEdit3.Value:=quFCLL.AsInteger/10; cxCalcEdit3.Properties.ReadOnly:=True; //�����
        cxCalcEdit4.Value:=quFCWW.AsInteger/10; cxCalcEdit4.Properties.ReadOnly:=True; //������
        cxCalcEdit5.Value:=quFCHH.AsInteger/10; cxCalcEdit5.Properties.ReadOnly:=True; //������
        cxCalcEdit6.Value:=quFCVES.AsFloat; cxCalcEdit6.Properties.ReadOnly:=True; //���

        cxSpinEdit1.Value:=quFCVESBUTTON.AsInteger; cxSpinEdit1.Properties.ReadOnly:=True;  //������ � �����

        cxCalcEdit7.Value:=quFCAZKOEF.AsFloat; cxCalcEdit7.Properties.ReadOnly:=True;  //���������� ����������� ��� ������
        cxSpinEdit2.Value:=roundex(quFCAZSTRAHR.AsFloat); cxSpinEdit2.Properties.ReadOnly:=True;  //��������� ����� ����
        cxSpinEdit3.Value:=quFCAZDAYSPROC.AsInteger; cxSpinEdit3.Properties.ReadOnly:=True;  //���-�� ���� ��� �������
        cxSpinEdit4.Value:=quFCAZSROKREAL.AsInteger; cxSpinEdit4.Properties.ReadOnly:=True;  //���� ����������
        cxCalcEdit12.Value:=quFCQUANTPACK.AsFloat; cxCalcEdit12.Properties.ReadOnly:=True;  //����� � 1-�� ������ �����

        mSostav.Text:=quFCSOSTAV.AsString; mSostav.ReadOnly:=True;
        mGost.Text:=quFCGOST.AsString;  mGost.ReadOnly:=True;
        cxCalcEdit13.Value:=quFCKREP.AsFloat; cxCalcEdit13.Properties.ReadOnly:=True; //��������

        if quFCISTENDER.AsInteger=1 then cxCheckBox1.Checked:=True else cxCheckBox1.Checked:=False; cxCheckBox1.Properties.ReadOnly:=True; // ������
        if quFCISTOP.AsInteger=1 then cxCheckBox2.Checked:=True else cxCheckBox2.Checked:=False; cxCheckBox2.Properties.ReadOnly:=True; // ������


        quF.Active:=False;
        quF.SQL.Clear;
        quF.SQL.Add('SELECT [ID],[NAME],[INN],[KPP]FROM [dbo].[AMAKER]where [ID]='+its(quFCMAKER.AsInteger));
        quF.Active:=True;
        if quF.RecordCount=1 then cxButtonEdit1.Text:=quF.FieldByName('NAME').Value else cxButtonEdit1.Text:='';
        quF.Active:=False;
        cxButtonEdit1.Tag:=quFCMAKER.AsInteger; cxButtonEdit1.Properties.ReadOnly:=True; cxButtonEdit1.Enabled:=False; //������������� ����

        cxLookupComboBox1.Properties.ReadOnly:=True; cxLookupComboBox1.EditValue:=quFCCOUNTRY.AsInteger; // cxLookupComboBox1.Text:='������'; // ������
        cxLookupComboBox2.Properties.ReadOnly:=True; cxLookupComboBox2.EditValue:=quFCBRAND.AsInteger; // �����
        cxLookupComboBox3.Properties.ReadOnly:=True; cxLookupComboBox3.EditValue:=quFCMANCAT.AsINteger; // ���. ��������
        cxLookupComboBox4.Properties.ReadOnly:=True; cxLookupComboBox4.EditValue:=quFCAVID.AsInteger; // ��� ���������
        cxLookupComboBox5.Properties.ReadOnly:=True; cxLookupComboBox5.EditValue:=quFCCATEG.AsInteger; // ������ ������
      end;
      quFC.Active:=False;
    end;
  end;
end;

procedure TfmAddCard.RadioGroup1PropertiesChange(Sender: TObject);
Var iItem:Integer;
    sBar,StrWk:String;
begin
  iItem:=StrToIntDef(MaskEdit1.Text,0);
  
  if RadioGroup1.ItemIndex=1 then  //������� �����
  begin
    RadioGroup2.ItemIndex:=1;
    RadioGroup2.Enabled:=False;

    StrWk:=IntToStr(iItem);
    while Length(StrWk)<5 do StrWk:='0'+StrWk;
    sBar:=CommonSet.PrefixVes+StrWk;
  end
  else  //������� �����
  begin
    RadioGroup2.ItemIndex:=0;
    RadioGroup2.Enabled:=True;
    sBar:='';
  end;

  MaskEdit2.Text:=sBar;
end;

procedure TfmAddCard.RadioGroup2PropertiesChange(Sender: TObject);
Var iItem:Integer;
    sBar,StrWk:String;
begin
  with dmMCS do
  begin
    iItem:=StrToIntDef(MaskEdit1.Text,0);
   //��������� ����
    if RadioGroup1.ItemIndex=0 then
    begin //������� �����
      if RadioGroup2.ItemIndex=0 then
      begin
        sBar:='';
      end;
      if RadioGroup2.ItemIndex=1 then
      begin
        sBar:=its(iItem);
        while length(sBar)<10 do sBar:='0'+sBar;
        sBar:=CommonSet.Prefix+sBar;
        sBar:=ToStandart(sBar);
      end;
      if RadioGroup2.ItemIndex=2 then
      begin
        sBar:=IntToStr(iItem);
      end;
    end
    else
    begin //������� �����
      StrWk:=IntToStr(iItem);
      while Length(StrWk)<5 do StrWk:='0'+StrWk;
      sBar:=CommonSet.PrefixVes+StrWk;
    end;
    MaskEdit2.Text:=sBar;
  end;
end;

procedure TfmAddCard.acBarReadExecute(Sender: TObject);
begin
  if bAddC then MaskEdit2.SetFocus;
end;

procedure TfmAddCard.cxButton1Click(Sender: TObject);
Var StrWk:String;
begin
  if not bViewC then
  begin
    StrWk:=TextEdit1.Text;
    if Length(StrWk)>200 then StrWk:=Copy(StrWk,1,200);
    TextEdit1.Text:=StrWk;

    StrWk:=TextEdit2.Text;
    if Length(StrWk)>200 then StrWk:=Copy(StrWk,1,200);
    TextEdit2.Text:=StrWk;
  end;
end;

procedure TfmAddCard.acTextEdit2Execute(Sender: TObject);
Var StrWk:String;
begin
  if not bViewC then
  begin
    StrWk:=TextEdit1.Text;
    if Length(StrWk)>200 then StrWk:=Copy(StrWk,1,200);
    TextEdit1.Text:=StrWk;

    StrWk:=TextEdit2.Text;
    if Length(StrWk)>200 then StrWk:=Copy(StrWk,1,200);
    TextEdit2.Text:=StrWk;

    TextEdit2.Text:=TextEdit1.Text;
  end;
end;

procedure TfmAddCard.acClearExecute(Sender: TObject);
begin
//  if bAddC then SetGoodsDef;
end;

procedure TfmAddCard.ButtonEdit2PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  with dmMCS do
  begin
    fmClassSel:=TfmClassSel.Create(Application);

    PosP.Id:=0;
    PosP.Name:='';

    fmClassSel.Tag:=ButtonEdit1.Tag;
    fmClassSel.ShowModal;

    if PosP.Id>0 then
    begin
      ButtonEdit1.Tag:=PosP.Id;
      ButtonEdit1.Text:=PosP.Name;
    end;

    fmClassSel.Release;
  end;
end;

procedure TfmAddCard.MaskEdit2PropertiesEditValueChanged(Sender: TObject);
Var iNum:INteger;
begin
  if fmAddCard.Tag=0 then
  begin
    if prFindBar(trim(MaskEdit2.Text),-1,iNum) then
    begin
      ShowMessage('�������� "'+Trim(MaskEdit2.Text)+'" ��� ���� � ������� ('+IntToStr(iNum)+'). ������������ ���������.');
      fmAddCard.MaskEdit2.EditText:='';
      fmAddCard.MaskEdit2.Text:='';
    end;
  end;
end;

procedure TfmAddCard.cxButton2Click(Sender: TObject);
Var iItem,iCe:Integer;
    bErr:Boolean;
    sBar:String;
begin
  if trim(MaskEdit2.EditText)='' then
  begin
    ShowMessage('�� �������� �����-��� !!!');
    abort;
  end;

  if ButtonEdit1.tag<1 then
  begin
    ShowMessage('���������� ������ !!!');
    abort;
  end;

  if fmAddCard.Tag>0 then
  begin
    fmAddCard.ModalResult:=mrOk;
  end else
  begin
    with dmMCS do
    begin
      bErr:=False;
      iItem:=StrToIntDef(fmAddCard.MaskEdit1.Text,0);
      if (iItem>0)and(MaskEdit2.Text>'')  then
      begin
        sBar:=trim(MaskEdit2.EditText);
        if prFindBar(sBar,-1,iCe)=False then
        begin
          quFC.Active:=False;
          quFC.SQL.Clear;
          quFC.SQL.Add('SELECT * FROM [dbo].[CARDS] ca');
          quFC.SQL.Add('where ca.[ID]='+its(iItem));
          quFC.Active:=True;

          if quFC.RecordCount>0 then bErr:=True;

          quFC.Active:=False;

          if bErr then
          begin
           showmessage('������ ��� ��� �����, �������� "�������� ����".');
            cxButton4.SetFocus;
          end else
          begin
            fmAddCard.ModalResult:=mrOk;
          end;
        end else showmessage('������ �������� ��� ���� � �������, ���������� ����������.');
      end;
    end;
  end;
end;

procedure TfmAddCard.cxButton4Click(Sender: TObject);
Var iItem:Integer;
    sBar,StrWk,sM:String;
begin
  with dmMCS do
  begin
    if RadioGroup1.ItemIndex=0 then
    begin //������� �����
      iItem:=prFindNewArt(False,sM);
      //������ ��������
      if RadioGroup2.ItemIndex=0 then
      begin
        sBar:='';
      end;
      if RadioGroup2.ItemIndex=1 then
      begin
        sBar:=its(iItem);
        while length(sBar)<10 do sBar:='0'+sBar;
        sBar:=CommonSet.Prefix+sBar;
        sBar:=ToStandart(sBar);
      end;
      if RadioGroup2.ItemIndex=2 then
      begin
        sBar:=IntToStr(iItem);
      end;
    end
    else
    begin //������� �����
      iItem:=prFindNewArt(True,sM);

      //������ ��������
      StrWk:=IntToStr(iItem);
      while Length(StrWk)<5 do StrWk:='0'+StrWk;
      sBar:=CommonSet.PrefixVes+StrWk;
    end;

    if iItem=0 then
    begin
      Showmessage(sM);
      Showmessage('���������� ����������.');
    end else
    begin
      MaskEdit1.Text:=IntToStr(iItem);
      MaskEdit2.Text:=sBar;
    end;
  end;
  cxButton2.SetFocus;
end;

procedure TfmAddCard.cxButtonEdit1PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  PosP.Id:=0;;
  PosP.Name:='';
  if fmMakers.Showing=False then
  begin
    if fmMakers.quMakers.Active=False then
    begin
      fmMakers.ViewMakers.BeginUpdate;
      fmMakers.quMakers.Active:=False;
      fmMakers.quMakers.Active:=True;

      if cxButtonEdit1.Tag>0 then fmMakers.quMakers.Locate('ID',cxButtonEdit1.Tag,[]);

      fmMakers.ViewMakers.EndUpdate;
    end;
    fmMakers.Show;
  end;
end;

procedure TfmAddCard.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  quCateg.Active:=False;
  quCountry.Active:=False;
  quCatMan.Active:=False;
  quBrands.Active:=False;
  quAVid.Active:=False;
end;

end.
