unit Dm;

interface

uses
  SysUtils, Classes, DB, ADODB, ImgList, Controls, BseMain, BtrMain,
  cxStyles,Dialogs, frexpimg, frRtfExp, frTXTExp, frXMLExl, frOLEExl,
  frHTMExp, FR_E_HTML2, FR_E_HTM, FR_E_CSV, FR_E_RTF, FR_Class, FR_E_TXT,
  DBClient, IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient,
  IdFTP,cxMemo, dxmdaset,cxGridDBTableView,ComObj,ActiveX, Excel2000, OleServer, ExcelXP,
  IdMessage,Variants, IdIntercept, IdLogBase, IdLogFile, IdMessageClient,
  IdSMTP, FR_DSet, FR_DBSet, FR_Chart,ComCtrls, FR_BarC,Windows;

type
  TdmMCS = class(TDataModule)
    msConnection: TADOConnection;
    ImageList1: TImageList;
    quPassw: TADOQuery;
    dsPassw: TDataSource;
    quPasswID: TIntegerField;
    quPasswID_PARENT: TIntegerField;
    quPasswNAME: TStringField;
    quPasswUVOLNEN: TSmallintField;
    quPasswPASSW: TStringField;
    quPasswMODUL1: TSmallintField;
    quPasswMODUL2: TSmallintField;
    quPasswMODUL3: TSmallintField;
    quPasswMODUL4: TSmallintField;
    quPasswMODUL5: TSmallintField;
    quPasswMODUL6: TSmallintField;
    quPasswBARCODE: TStringField;
    quPersonal: TADOQuery;
    quPersonalID: TIntegerField;
    quPersonalID_PARENT: TIntegerField;
    quPersonalNAME: TStringField;
    quPersonalUVOLNEN: TSmallintField;
    quPersonalPASSW: TStringField;
    quPersonalMODUL1: TSmallintField;
    quPersonalMODUL2: TSmallintField;
    quPersonalMODUL3: TSmallintField;
    quPersonalMODUL4: TSmallintField;
    quPersonalMODUL5: TSmallintField;
    quPersonalMODUL6: TSmallintField;
    quPersonalBARCODE: TStringField;
    taPersonal: TADOTable;
    taPersonalID: TIntegerField;
    taPersonalID_PARENT: TIntegerField;
    taPersonalNAME: TStringField;
    taPersonalUVOLNEN: TSmallintField;
    taPersonalPASSW: TStringField;
    taPersonalMODUL1: TSmallintField;
    taPersonalMODUL2: TSmallintField;
    taPersonalMODUL3: TSmallintField;
    taPersonalMODUL4: TSmallintField;
    taPersonalMODUL5: TSmallintField;
    taPersonalMODUL6: TSmallintField;
    taPersonalBARCODE: TStringField;
    quFuncList: TADOQuery;
    dsFuncList: TDataSource;
    quFuncListID_PERSONAL: TIntegerField;
    quFuncListNAME: TStringField;
    quFuncListPREXEC: TSmallintField;
    quFuncListCOMMENT: TStringField;
    prChangeRFunction: TADOStoredProc;
    prDelPersonal: TADOStoredProc;
    prExistPersonal: TADOStoredProc;
    quGetId: TADOQuery;
    quGetIdretnum: TIntegerField;
    quCanDo: TADOQuery;
    quCanDoID_PERSONAL: TIntegerField;
    quCanDoNAME: TStringField;
    quCanDoPREXEC: TSmallintField;
    ColorDialog1: TColorDialog;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    cxStyle14: TcxStyle;
    cxStyle15: TcxStyle;
    cxStyle16: TcxStyle;
    cxStyle17: TcxStyle;
    cxStyle18: TcxStyle;
    cxStyle19: TcxStyle;
    cxStyle20: TcxStyle;
    cxStyle21: TcxStyle;
    cxStyle22: TcxStyle;
    cxStyle23: TcxStyle;
    cxStyle24: TcxStyle;
    cxStyle25: TcxStyle;
    cxStyle26: TcxStyle;
    imState: TImageList;
    IdSMTP: TIdSMTP;
    IdMessage: TIdMessage;
    quPersonZ: TADOQuery;
    quPersonZID: TIntegerField;
    quPersonZZSTART: TDateTimeField;
    quPersonZZEND: TDateTimeField;
    frRtfAdvExport1: TfrRtfAdvExport;
    frJPEGExport1: TfrJPEGExport;
    frTIFFExport1: TfrTIFFExport;
    frBMPExport1: TfrBMPExport;
    frTextAdvExport1: TfrTextAdvExport;
    frTextExport1: TfrTextExport;
    frRTFExport1: TfrRTFExport;
    frCSVExport1: TfrCSVExport;
    frHTMLTableExport1: TfrHTMLTableExport;
    frOLEExcelExport1: TfrOLEExcelExport;
    frXMLExcelExport1: TfrXMLExcelExport;
    frHTML2Export1: TfrHTML2Export;
    frChartObject1: TfrChartObject;
    quRCLassif: TADOQuery;
    quRCLassifID_CLASSIF: TIntegerField;
    quRCLassifRIGHTS: TIntegerField;
    quRCLassifID_PARENT: TIntegerField;
    quE: TADOQuery;
    quRCLassifName: TStringField;
    taPersonalPATHEXP: TStringField;
    taPersonalHOSTFTP: TStringField;
    taPersonalLOGINFTP: TStringField;
    taPersonalPASSWFTP: TStringField;
    taPersonalSCLI: TStringField;
    taPersonalALLSHOPS: TSmallintField;
    taPersonalCATMAN: TSmallintField;
    taPersonalPOSTACCESS: TSmallintField;
    quT: TADOQuery;
    teClass: TdxMemData;
    teClassId: TIntegerField;
    teClassIdParent: TIntegerField;
    teClassNameClass: TStringField;
    teClassrNac: TFloatField;
    quClassif: TADOQuery;
    quClassifID: TIntegerField;
    quClassifIDPARENT: TIntegerField;
    quClassifNAME: TStringField;
    quClassifNAC: TFloatField;
    quClassifR1: TFloatField;
    quClassifR2: TFloatField;
    quClassifR3: TFloatField;
    quClassifI1: TIntegerField;
    quClassifI2: TIntegerField;
    quClassifI3: TIntegerField;
    quClassifS1: TStringField;
    quClassifS2: TStringField;
    quClassifS3: TStringField;
    quA: TADOQuery;
    quCards: TADOQuery;
    quCountry: TADOQuery;
    quCountryIDPARENT: TIntegerField;
    quCountryID: TIntegerField;
    quCountryNAME: TStringField;
    quCountryKOD: TIntegerField;
    teClassKod: TIntegerField;
    quD: TADOQuery;
    quDeps: TADOQuery;
    quCardsIDCLASSIF: TIntegerField;
    quCardsID: TIntegerField;
    quCardsNAME: TStringField;
    quCardsFULLNAME: TStringField;
    quCardsBARCODE: TStringField;
    quCardsEDIZM: TSmallintField;
    quCardsNDS: TFloatField;
    quCardsISTATUS: TSmallintField;
    quCardsMANCAT: TIntegerField;
    quCardsCOUNTRY: TIntegerField;
    quCardsBRAND: TIntegerField;
    quCardsISTENDER: TSmallintField;
    quCardsISTOP: TSmallintField;
    quCardsISNOV: TSmallintField;
    quCardsCATEG: TIntegerField;
    quCardsKREP: TFloatField;
    quCardsAVID: TIntegerField;
    quCardsRNAC: TFloatField;
    quCardsPRICE: TFloatField;
    quCardsNAMECU: TStringField;
    quCardsNAMEPERS: TStringField;
    quCardsSID: TStringField;
    dsquCards: TDataSource;
    quCardsVOL: TFloatField;
    quFindC: TADOQuery;
    quFindCIDCLASSIF: TIntegerField;
    quFindCID: TIntegerField;
    quFindCNAME: TStringField;
    quFindCFULLNAME: TStringField;
    quFindCBARCODE: TStringField;
    quFindCTTOVAR: TSmallintField;
    quFindCEDIZM: TSmallintField;
    quFindCNDS: TFloatField;
    quFindCOKDP: TFloatField;
    quFindCISTATUS: TSmallintField;
    quFindCPRSISION: TFloatField;
    quFindCMANCAT: TIntegerField;
    quFindCCOUNTRY: TIntegerField;
    quFindCBRAND: TIntegerField;
    quFindCISTENDER: TSmallintField;
    quFindCISTOP: TSmallintField;
    quFindCISNOV: TSmallintField;
    quFindCCATEG: TIntegerField;
    quFindCSROKGODN: TIntegerField;
    quFindCQUANTPACK: TFloatField;
    quFindCVESBUTTON: TIntegerField;
    quFindCVESFORMAT: TSmallintField;
    quFindCVES: TFloatField;
    quFindCVOL: TFloatField;
    quFindCWW: TIntegerField;
    quFindCHH: TIntegerField;
    quFindCLL: TIntegerField;
    quFindCKREP: TFloatField;
    quFindCAVID: TIntegerField;
    quFindCMAKER: TIntegerField;
    quFindCDATECREATE: TIntegerField;
    quFindCAZKOEF: TFloatField;
    quFindCAZSTRAHR: TFloatField;
    quFindCAZSROKREAL: TIntegerField;
    quFindCAZDAYSPROC: TIntegerField;
    quFindCAZQUANTMAX: TFloatField;
    quFindCRNAC: TFloatField;
    dsquFindC: TDataSource;
    quFindCGOODSID: TIntegerField;
    quFindCIDSKL: TIntegerField;
    quFindCPRICE: TFloatField;
    quFindBar: TADOQuery;
    quFindBarBar: TStringField;
    quFindBarGoodsId: TIntegerField;
    quFindBarQuant: TFloatField;
    quFindBarBarFormat: TSmallintField;
    quFindBarBarStatus: TSmallintField;
    quFindBarCost: TFloatField;
    quFindBarStatus: TSmallintField;
    quFindBarId_Producer: TIntegerField;
    quFindCBar: TADOQuery;
    quFindCBarID: TIntegerField;
    quFC: TADOQuery;
    quFCIDCLASSIF: TIntegerField;
    quFCID: TIntegerField;
    quFCNAME: TStringField;
    quFCFULLNAME: TStringField;
    quFCBARCODE: TStringField;
    quFCTTOVAR: TSmallintField;
    quFCEDIZM: TSmallintField;
    quFCNDS: TFloatField;
    quFCOKDP: TFloatField;
    quFCISTATUS: TSmallintField;
    quFCPRSISION: TFloatField;
    quFCMANCAT: TIntegerField;
    quFCCOUNTRY: TIntegerField;
    quFCBRAND: TIntegerField;
    quFCISTENDER: TSmallintField;
    quFCISTOP: TSmallintField;
    quFCISNOV: TSmallintField;
    quFCCATEG: TIntegerField;
    quFCSROKGODN: TIntegerField;
    quFCQUANTPACK: TFloatField;
    quFCVESBUTTON: TIntegerField;
    quFCVESFORMAT: TSmallintField;
    quFCVES: TFloatField;
    quFCVOL: TFloatField;
    quFCWW: TIntegerField;
    quFCHH: TIntegerField;
    quFCLL: TIntegerField;
    quFCKREP: TFloatField;
    quFCAVID: TIntegerField;
    quFCMAKER: TIntegerField;
    quFCDATECREATE: TIntegerField;
    quFCAZKOEF: TFloatField;
    quFCAZSTRAHR: TFloatField;
    quFCAZSROKREAL: TIntegerField;
    quFCAZDAYSPROC: TIntegerField;
    quFCAZQUANTMAX: TFloatField;
    quFCRNAC: TFloatField;
    quSel: TADOQuery;
    quCardsNAMEBRAND: TStringField;
    quFCSOSTAV: TStringField;
    quFCGOST: TStringField;
    quF: TADOQuery;
    quBars: TADOQuery;
    quBarsBar: TStringField;
    quBarsGoodsId: TIntegerField;
    quBarsQuant: TFloatField;
    quBarsBarFormat: TSmallintField;
    quBarsBarStatus: TSmallintField;
    quBarsCost: TFloatField;
    quBarsStatus: TSmallintField;
    quBarsId_Producer: TIntegerField;
    quCardsMAKER: TIntegerField;
    teClassCli: TdxMemData;
    teClassCliId: TIntegerField;
    teClassCliIdParent: TIntegerField;
    teClassCliNameClass: TStringField;
    teClassClirNac: TFloatField;
    teClassCliKod: TIntegerField;
    quClassifCli: TADOQuery;
    quClassifCliID: TIntegerField;
    quClassifCliIDPARENT: TIntegerField;
    quClassifCliNAME: TStringField;
    quClients: TADOQuery;
    dsquClients: TDataSource;
    quClientsId: TIntegerField;
    quClientsIdParent: TIntegerField;
    quClientsINN: TStringField;
    quClientsName: TStringField;
    quClientsFullName: TStringField;
    quClientsIType: TSmallintField;
    quClientsIActive: TSmallintField;
    quClientsPostIndex: TStringField;
    quClientsGorod: TStringField;
    quClientsStreet: TStringField;
    quClientsHouse: TStringField;
    quClientsKPP: TStringField;
    quClientsNAMEOTP: TStringField;
    quClientsADROTPR: TStringField;
    quClientsRSch: TStringField;
    quClientsKSch: TStringField;
    quClientsBank: TStringField;
    quClientsBik: TStringField;
    quClientsDogNum: TStringField;
    quClientsDogDate: TDateTimeField;
    quClientsEmail: TStringField;
    quClientsPhone: TStringField;
    quClientsMol: TStringField;
    quClientsZakazT: TIntegerField;
    quClientsGln: TStringField;
    quClientsPayNDS: TSmallintField;
    quClientsITypeCli: TSmallintField;
    quClientsEDIProvider: TIntegerField;
    quClientsMinSumZak: TFloatField;
    quClientsManyStore: TSmallintField;
    quClientsEmailVoz: TStringField;
    quClientsPhoneVoz: TStringField;
    quClientsMolVoz: TStringField;
    quClientsTypeVoz: TIntegerField;
    quClientsNoNaclZ: TSmallintField;
    quClientsIDPERS: TIntegerField;
    quClientsPretOrg: TStringField;
    quClientsEmailPret: TStringField;
    quClientsZakAccess: TSmallintField;
    quClientsNoSpecif: TSmallintField;
    quClientsEmailSpecif: TStringField;
    quClientsNoRecadv: TSmallintField;
    quTTNIn: TADOQuery;
    dsquTTNIn: TDataSource;
    quTTNInIDSKL: TIntegerField;
    quTTNInIDATEDOC: TIntegerField;
    quTTNInID: TLargeintField;
    quTTNInDATEDOC: TDateTimeField;
    quTTNInNUMDOC: TStringField;
    quTTNInDATESF: TDateTimeField;
    quTTNInNUMSF: TStringField;
    quTTNInIDCLI: TIntegerField;
    quTTNInSUMIN: TFloatField;
    quTTNInSUMUCH: TFloatField;
    quTTNInSUMTAR: TFloatField;
    quTTNInIACTIVE: TSmallintField;
    quTTNInOPRIZN: TSmallintField;
    quTTNInNAMECLI: TStringField;
    quTTNInNAMESKL: TStringField;
    quFCli: TADOQuery;
    quFindCli: TADOQuery;
    dsquFindCli: TDataSource;
    quFindCliId: TIntegerField;
    quFindCliIdParent: TIntegerField;
    quFindCliINN: TStringField;
    quFindCliName: TStringField;
    quFindCliFullName: TStringField;
    quFindCliIType: TSmallintField;
    quFindCliIActive: TSmallintField;
    quFindCliPostIndex: TStringField;
    quFindCliGorod: TStringField;
    quFindCliStreet: TStringField;
    quFindCliHouse: TStringField;
    quFindCliKPP: TStringField;
    quFindCliNAMEOTP: TStringField;
    quFindCliADROTPR: TStringField;
    quFindCliRSch: TStringField;
    quFindCliKSch: TStringField;
    quFindCliBank: TStringField;
    quFindCliBik: TStringField;
    quFindCliDogNum: TStringField;
    quFindCliDogDate: TDateTimeField;
    quFindCliEmail: TStringField;
    quFindCliPhone: TStringField;
    quFindCliMol: TStringField;
    quFindCliZakazT: TIntegerField;
    quFindCliGln: TStringField;
    quFindCliPayNDS: TSmallintField;
    quFindCliITypeCli: TSmallintField;
    quFindCliEDIProvider: TIntegerField;
    quFindCliMinSumZak: TFloatField;
    quFindCliManyStore: TSmallintField;
    quFindCliEmailVoz: TStringField;
    quFindCliPhoneVoz: TStringField;
    quFindCliMolVoz: TStringField;
    quFindCliTypeVoz: TIntegerField;
    quFindCliNoNaclZ: TSmallintField;
    quFindCliIDPERS: TIntegerField;
    quFindCliPretOrg: TStringField;
    quFindCliEmailPret: TStringField;
    quFindCliZakAccess: TSmallintField;
    quFindCliNoSpecif: TSmallintField;
    quFindCliEmailSpecif: TStringField;
    quFindCliNoRecadv: TSmallintField;
    quLabs: TADOQuery;
    quLabsID: TAutoIncField;
    quLabsName: TStringField;
    quLabsNameF: TStringField;
    dsquLabs: TDataSource;
    RepCenn: TfrReport;
    frtaCen: TfrDBDataSet;
    taCen: TdxMemData;
    taCenIdCard: TIntegerField;
    taCenFullName: TStringField;
    taCenCountry: TStringField;
    taCenPrice1: TFloatField;
    taCenPrice2: TFloatField;
    taCenDiscount: TFloatField;
    taCenBarCode: TStringField;
    taCenEdIzm: TSmallintField;
    taCenQuant: TIntegerField;
    taCenPluScale: TStringField;
    taCenReceipt: TStringField;
    taCenQuanrR: TFloatField;
    taCenDepName: TStringField;
    taCensDisc: TStringField;
    taCenScaleKey: TIntegerField;
    taCensDate: TStringField;
    taCenV02: TIntegerField;
    taCenV12: TIntegerField;
    taCenMaker: TStringField;
    dstaCen: TDataSource;
    quFindC4: TADOQuery;
    quFindC4IDCLASSIF: TIntegerField;
    quFindC4ID: TIntegerField;
    quFindC4NAME: TStringField;
    quFindC4FULLNAME: TStringField;
    quFindC4BARCODE: TStringField;
    quFindC4EDIZM: TSmallintField;
    quFindC4NDS: TFloatField;
    quFindC4VOL: TFloatField;
    quFindC4ISTATUS: TSmallintField;
    quFindC4MANCAT: TIntegerField;
    quFindC4COUNTRY: TIntegerField;
    quFindC4BRAND: TIntegerField;
    quFindC4ISTENDER: TSmallintField;
    quFindC4ISTOP: TSmallintField;
    quFindC4ISNOV: TSmallintField;
    quFindC4CATEG: TIntegerField;
    quFindC4KREP: TFloatField;
    quFindC4AVID: TIntegerField;
    quFindC4RNAC: TFloatField;
    quFindC4PRICE: TFloatField;
    quFindC4MAKER: TIntegerField;
    quFindC4NAMECU: TStringField;
    quFindC4NAMEPERS: TStringField;
    quFindC4NAMEBRAND: TStringField;
    quFindC4SID: TStringField;
    quFindC4GOST: TStringField;
    quFindC4SOSTAV: TStringField;
    quFindC4VESBUTTON: TIntegerField;
    quFindPlu: TADOQuery;
    quFindPluPLU: TIntegerField;
    quFindPluGOODSITEM: TIntegerField;
    quFindPluNAME1: TStringField;
    quFindPluNAME2: TStringField;
    quFindPluPRICE: TFloatField;
    quFindPluShelfLife: TSmallintField;
    quFindPluTareWeight: TSmallintField;
    quFindPluGroupCode: TSmallintField;
    quFindPluMessageNo: TSmallintField;
    quFindPluStatus: TSmallintField;
    frBarCodeObject1: TfrBarCodeObject;
    quDepartsSt: TADOQuery;
    dsquDepartsSt: TDataSource;
    quTTNInIDZ: TIntegerField;
    quTTNInSNUMZ: TStringField;
    quTTNInPayNDS: TSmallintField;
    quSpecIn: TADOQuery;
    quTTnId: TADOQuery;
    quTTnIdIDSKL: TIntegerField;
    quTTnIdIDATEDOC: TIntegerField;
    quTTnIdID: TLargeintField;
    quTTnIdDATEDOC: TDateTimeField;
    quTTnIdNUMDOC: TStringField;
    quTTnIdDATESF: TDateTimeField;
    quTTnIdNUMSF: TStringField;
    quTTnIdIDCLI: TIntegerField;
    quTTnIdSUMIN: TFloatField;
    quTTnIdSUMUCH: TFloatField;
    quTTnIdSUMTAR: TFloatField;
    quTTnIdIACTIVE: TSmallintField;
    quTTnIdOPRIZN: TSmallintField;
    quTTnIdIDZ: TIntegerField;
    quTTnIdSNUMZ: TStringField;
    quSpecInRec: TADOQuery;
    quChecks: TADOQuery;
    dsquChecks: TDataSource;
    quChecksIdHead: TLargeintField;
    quChecksId: TLargeintField;
    quChecksNum: TWordField;
    quChecksCode: TIntegerField;
    quChecksBarCode: TStringField;
    quChecksQuant: TFloatField;
    quChecksPrice: TFloatField;
    quChecksSumma: TFloatField;
    quChecksProcessingTime: TFloatField;
    quChecksDProc: TFloatField;
    quChecksDSum: TFloatField;
    quChecksId_Depart: TIntegerField;
    quChecksIDate: TIntegerField;
    quChecksDateOperation: TDateTimeField;
    quChecksOperation: TSmallintField;
    quChecksCk_Number: TIntegerField;
    quChecksCassir: TIntegerField;
    quChecksCash_Code: TIntegerField;
    quChecksNSmena: TIntegerField;
    quChecksCardNumber: TStringField;
    quChecksPaymentType: TSmallintField;
    quChecksNAME: TStringField;
    quTTNOut: TADOQuery;
    dsquTTNOut: TDataSource;
    quTTNOutIDSKL: TIntegerField;
    quTTNOutIDATEDOC: TIntegerField;
    quTTNOutID: TLargeintField;
    quTTNOutDATEDOC: TDateTimeField;
    quTTNOutNUMDOC: TStringField;
    quTTNOutDATESF: TDateTimeField;
    quTTNOutNUMSF: TStringField;
    quTTNOutIDCLI: TIntegerField;
    quTTNOutSUMIN: TFloatField;
    quTTNOutSUMUCH: TFloatField;
    quTTNOutSUMTAR: TFloatField;
    quTTNOutIACTIVE: TSmallintField;
    quTTNOutOPRIZN: TSmallintField;
    quTTNOutNAMECLI: TStringField;
    quTTNOutNAMESKL: TStringField;
    quTTNOutPayNDS: TSmallintField;
    quSpecInIDHEAD: TLargeintField;
    quSpecInNUM: TIntegerField;
    quSpecInIDCARD: TIntegerField;
    quSpecInSBAR: TStringField;
    quSpecInIDM: TIntegerField;
    quSpecInQUANT: TFloatField;
    quSpecInPRICEIN: TFloatField;
    quSpecInSUMIN: TFloatField;
    quSpecInPRICEIN0: TFloatField;
    quSpecInSUMIN0: TFloatField;
    quSpecInPRICEUCH: TFloatField;
    quSpecInSUMUCH: TFloatField;
    quSpecInNDSPROC: TFloatField;
    quSpecInSUMNDS: TFloatField;
    quSpecInGTD: TStringField;
    quSpecInSNUMHEAD: TStringField;
    quSpecInNAME: TStringField;
    quSpecInFULLNAME: TStringField;
    quSpecInBARCODE: TStringField;
    quSpecInNDS: TFloatField;
    quSpecInVOL: TFloatField;
    quSpecInAVID: TIntegerField;
    quSpecInMAKER: TIntegerField;
    quSpecInRNAC: TFloatField;
    quSpecInPRICE: TFloatField;
    quSpecInRecSUMIN0: TFloatField;
    quSpecInRecPRICEUCH: TFloatField;
    quSpecInRecSUMUCH: TFloatField;
    quSpecInRecNDSPROC: TFloatField;
    quSpecInRecSUMNDS: TFloatField;
    quSpecInRecGTD: TStringField;
    quSpecInRecSNUMHEAD: TStringField;
    quFindC5: TADOQuery;
    quCash: TADOQuery;
    quFindC5IDCLASSIF: TIntegerField;
    quFindC5ID: TIntegerField;
    quFindC5NAME: TStringField;
    quFindC5FULLNAME: TStringField;
    quFindC5BARCODE: TStringField;
    quFindC5TTOVAR: TSmallintField;
    quFindC5EDIZM: TSmallintField;
    quFindC5NDS: TFloatField;
    quFindC5OKDP: TFloatField;
    quFindC5ISTATUS: TSmallintField;
    quFindC5PRSISION: TFloatField;
    quFindC5MANCAT: TIntegerField;
    quFindC5COUNTRY: TIntegerField;
    quFindC5BRAND: TIntegerField;
    quFindC5ISTENDER: TSmallintField;
    quFindC5ISTOP: TSmallintField;
    quFindC5ISNOV: TSmallintField;
    quFindC5CATEG: TIntegerField;
    quFindC5SROKGODN: TIntegerField;
    quFindC5QUANTPACK: TFloatField;
    quFindC5RNAC: TFloatField;
    quFindC5PRICE: TFloatField;
    quCashIDEP: TIntegerField;
    quCashID: TSmallintField;
    quCashNAME: TStringField;
    quCashITYPE: TSmallintField;
    quCashIACTIVE: TSmallintField;
    quCashITLOAD: TSmallintField;
    quCashITCLOSE: TSmallintField;
    quCashITOPSV: TSmallintField;
    quCashSPATH: TStringField;
    quFCP: TADOQuery;
    quFCPIDCLASSIF: TIntegerField;
    quFCPID: TIntegerField;
    quFCPNAME: TStringField;
    quFCPFULLNAME: TStringField;
    quFCPBARCODE: TStringField;
    quFCPTTOVAR: TSmallintField;
    quFCPEDIZM: TSmallintField;
    quFCPNDS: TFloatField;
    quFCPOKDP: TFloatField;
    quFCPISTATUS: TSmallintField;
    quFCPPRSISION: TFloatField;
    quFCPMANCAT: TIntegerField;
    quFCPCOUNTRY: TIntegerField;
    quFCPBRAND: TIntegerField;
    quFCPISTENDER: TSmallintField;
    quFCPISTOP: TSmallintField;
    quFCPISNOV: TSmallintField;
    quFCPCATEG: TIntegerField;
    quFCPSROKGODN: TIntegerField;
    quFCPQUANTPACK: TFloatField;
    quFCPVESBUTTON: TIntegerField;
    quFCPVESFORMAT: TSmallintField;
    quFCPVES: TFloatField;
    quFCPVOL: TFloatField;
    quFCPWW: TIntegerField;
    quFCPHH: TIntegerField;
    quFCPLL: TIntegerField;
    quFCPKREP: TFloatField;
    quFCPAVID: TIntegerField;
    quFCPMAKER: TIntegerField;
    quFCPDATECREATE: TIntegerField;
    quFCPAZKOEF: TFloatField;
    quFCPAZSTRAHR: TFloatField;
    quFCPAZSROKREAL: TIntegerField;
    quFCPAZDAYSPROC: TIntegerField;
    quFCPAZQUANTMAX: TFloatField;
    quFCPRNAC: TFloatField;
    quFCPSOSTAV: TStringField;
    quFCPGOST: TStringField;
    quFCPGOODSID: TIntegerField;
    quFCPIDSKL: TIntegerField;
    quFCPPRICE: TFloatField;
    quPosts: TADOQuery;
    dsquPosts: TDataSource;
    quCLHist: TADOQuery;
    quCLHistCode: TIntegerField;
    quCLHistISkl: TIntegerField;
    quCLHistIDate: TIntegerField;
    quCLHistId: TLargeintField;
    quCLHistDateLoad: TDateTimeField;
    quCLHistCType: TStringField;
    quCLHistPrice: TFloatField;
    quCLHistDisc: TFloatField;
    quCLHistPerson: TStringField;
    quCLHistList: TADOQuery;
    quCLHistListCode: TIntegerField;
    quCLHistListISkl: TIntegerField;
    quCLHistListIDate: TIntegerField;
    quCLHistListId: TLargeintField;
    quCLHistListDateLoad: TDateTimeField;
    quCLHistListCType: TStringField;
    quCLHistListPrice: TFloatField;
    quCLHistListDisc: TFloatField;
    quCLHistListPerson: TStringField;
    dsquCLHistList: TDataSource;
    quCloseSel: TADOQuery;
    quCloseSelISKL: TIntegerField;
    quCloseSelID: TLargeintField;
    quCloseSelIDATE: TIntegerField;
    quCloseSelDDATE: TDateTimeField;
    quCloseSelIPERS: TIntegerField;
    quCloseSelDDATEEDIT: TDateTimeField;
    quSelLasInv: TADOQuery;
    quProc: TADOQuery;
    IntegerField3: TIntegerField;
    quBuffPr: TADOQuery;
    dsquBuffPr: TDataSource;
    quBuffPrCLINAME1: TStringField;
    quBuffPrNUMDOC1: TStringField;
    quPosts1: TADOQuery;
    dsquPosts1: TDataSource;
    quPosts1IDHEAD: TLargeintField;
    quPosts1NUM: TIntegerField;
    quPosts1IDCARD: TIntegerField;
    quPosts1SBAR: TStringField;
    quPosts1IDM: TIntegerField;
    quPosts1QUANT: TFloatField;
    quPosts1PRICEIN: TFloatField;
    quPosts1SUMIN: TFloatField;
    quPosts1PRICEIN0: TFloatField;
    quPosts1SUMIN0: TFloatField;
    quPosts1PRICEUCH: TFloatField;
    quPosts1SUMUCH: TFloatField;
    quPosts1NDSPROC: TFloatField;
    quPosts1SUMNDS: TFloatField;
    quPosts1GTD: TStringField;
    quPosts1SNUMHEAD: TStringField;
    quPosts1DATEDOC: TDateTimeField;
    quPosts1NUMDOC: TStringField;
    quPosts1IDCLI: TIntegerField;
    quPosts1IDSKL: TIntegerField;
    quPosts1Name: TStringField;
    quPosts1INN: TStringField;
    quPosts1PayNDS: TSmallintField;
    quScaleList: TADOQuery;
    quScaleListDEPART: TIntegerField;
    quScaleListID: TIntegerField;
    quScaleListNAME: TStringField;
    quScaleListSCALETYPE: TStringField;
    quScaleListIP1: TStringField;
    quScaleListIP2: TStringField;
    quScaleListIP3: TStringField;
    quScaleListIP4: TStringField;
    quScaleListIP5: TStringField;
    dsquScaleList: TDataSource;
    quScaleListIDAI: TAutoIncField;
    quScaleItems: TADOQuery;
    dsquScaleItems: TDataSource;
    quScaleItemsSCALENUM: TIntegerField;
    quScaleItemsPLU: TIntegerField;
    quScaleItemsGOODSITEM: TIntegerField;
    quScaleItemsNAME1: TStringField;
    quScaleItemsNAME2: TStringField;
    quScaleItemsPRICE: TFloatField;
    quScaleItemsShelfLife: TSmallintField;
    quScaleItemsTareWeight: TSmallintField;
    quScaleItemsGroupCode: TSmallintField;
    quScaleItemsMessageNo: TSmallintField;
    quScaleItemsStatus: TSmallintField;
    quFindPluSCALENUM: TIntegerField;
    taToScale: TdxMemData;
    taToScaleId: TIntegerField;
    taToScaleBarcode: TStringField;
    taToScaleName: TStringField;
    taToScaleSrok: TIntegerField;
    taToScalePrice: TFloatField;
    taToScaleNumF: TSmallintField;
    quPluId: TADOQuery;
    quPluIdSCALENUM: TIntegerField;
    quPluIdPLU: TIntegerField;
    quPluIdGOODSITEM: TIntegerField;
    quPluIdNAME1: TStringField;
    quPluIdNAME2: TStringField;
    quPluIdPRICE: TFloatField;
    quPluIdShelfLife: TSmallintField;
    quPluIdTareWeight: TSmallintField;
    quPluIdGroupCode: TSmallintField;
    quPluIdMessageNo: TSmallintField;
    quPluIdStatus: TSmallintField;
    quScales: TADOQuery;
    quScalesDEPART: TIntegerField;
    quScalesID: TIntegerField;
    quScalesIDAI: TAutoIncField;
    quScalesNAME: TStringField;
    quScalesSCALETYPE: TStringField;
    quScalesIP1: TStringField;
    quScalesIP2: TStringField;
    quScalesIP3: TStringField;
    quScalesIP4: TStringField;
    quScalesIP5: TStringField;
    dsquScales: TDataSource;
    quTTnInv: TADOQuery;
    dsquTTnInv: TDataSource;
    quTTnInvIDSKL: TIntegerField;
    quTTnInvIDATEDOC: TIntegerField;
    quTTnInvDATEDOC: TDateTimeField;
    quTTnInvNUMDOC: TStringField;
    quTTnInvSUMINR: TFloatField;
    quTTnInvSUMINF: TFloatField;
    quTTnInvSUMINRTO: TFloatField;
    quTTnInvRSUMR: TFloatField;
    quTTnInvRSUMF: TFloatField;
    quTTnInvRSUMTO: TFloatField;
    quTTnInvSUMTARR: TFloatField;
    quTTnInvSUMTARF: TFloatField;
    quTTnInvIACTIVE: TSmallintField;
    quTTnInvIDETAIL: TSmallintField;
    quTTnInvNAMESKL: TStringField;
    dsquDepartsSt1: TDataSource;
    quPostsIDHEAD: TLargeintField;
    quPostsNUM: TIntegerField;
    quPostsIDCARD: TIntegerField;
    quPostsSBAR: TStringField;
    quPostsIDM: TIntegerField;
    quPostsQUANT: TFloatField;
    quPostsPRICEIN: TFloatField;
    quPostsSUMIN: TFloatField;
    quPostsPRICEIN0: TFloatField;
    quPostsSUMIN0: TFloatField;
    quPostsPRICEUCH: TFloatField;
    quPostsSUMUCH: TFloatField;
    quPostsNDSPROC: TFloatField;
    quPostsSUMNDS: TFloatField;
    quPostsGTD: TStringField;
    quPostsSNUMHEAD: TStringField;
    quPostsDATEDOC: TDateTimeField;
    quPostsNUMDOC: TStringField;
    quPostsIDCLI: TIntegerField;
    quPostsIDSKL: TIntegerField;
    quPostsName: TStringField;
    quPostsINN: TStringField;
    quPostsPayNDS: TSmallintField;
    quPostsID: TLargeintField;
    quPosts1ID: TLargeintField;
    quPostsPROCN: TFloatField;
    quInvHRec: TADOQuery;
    quInvHRecIDSKL: TIntegerField;
    quInvHRecIDATEDOC: TIntegerField;
    quInvHRecDATEDOC: TDateTimeField;
    quInvHRecNUMDOC: TStringField;
    quInvHRecSUMINR: TFloatField;
    quInvHRecSUMINF: TFloatField;
    quInvHRecSUMINRTO: TFloatField;
    quInvHRecRSUMR: TFloatField;
    quInvHRecRSUMF: TFloatField;
    quInvHRecRSUMTO: TFloatField;
    quInvHRecSUMTARR: TFloatField;
    quInvHRecSUMTARF: TFloatField;
    quInvHRecIACTIVE: TSmallintField;
    quInvHRecIDETAIL: TSmallintField;
    quInvHRecIN_TO: TSmallintField;
    quSpecInvRec: TADOQuery;
    quSpecInvRecNUM: TIntegerField;
    quSpecInvRecIDCARD: TIntegerField;
    quSpecInvRecSBAR: TStringField;
    quSpecInvRecQUANTR: TFloatField;
    quSpecInvRecQUANTF: TFloatField;
    quSpecInvRecPRICER: TFloatField;
    quSpecInvRecSUMR: TFloatField;
    quSpecInvRecSUMIN0: TFloatField;
    quSpecInvRecSUMIN: TFloatField;
    quSpecInvRecSUMRF: TFloatField;
    quSpecInvRecSUMIN0F: TFloatField;
    quSpecInvRecSUMINF: TFloatField;
    quSpecInvRecQUANTC: TFloatField;
    quSpecInvRecSUMC: TFloatField;
    quSpecInvRecSUMRSC: TFloatField;
    quRemn: TADOQuery;
    quRemnQUANTR: TFloatField;
    quSpecInv: TADOQuery;
    quSpecInvNUM: TIntegerField;
    quSpecInvIDCARD: TIntegerField;
    quSpecInvSBAR: TStringField;
    quSpecInvQUANTR: TFloatField;
    quSpecInvQUANTF: TFloatField;
    quSpecInvPRICER: TFloatField;
    quSpecInvSUMR: TFloatField;
    quSpecInvSUMIN0: TFloatField;
    quSpecInvSUMIN: TFloatField;
    quSpecInvSUMRF: TFloatField;
    quSpecInvSUMIN0F: TFloatField;
    quSpecInvSUMINF: TFloatField;
    quSpecInvQUANTC: TFloatField;
    quSpecInvSUMC: TFloatField;
    quSpecInvSUMRSC: TFloatField;
    quSpecInvNAME: TStringField;
    quSpecInvTTOVAR: TSmallintField;
    quSpecInvVOL: TFloatField;
    quSpecInvAVID: TIntegerField;
    quSpecInvMAKER: TIntegerField;
    quSpecInvEDIZM: TSmallintField;
    quSpecInvNDS: TFloatField;
    quSpecInvSumRTO: TFloatField;
    quSpecInvRecSumRTO: TFloatField;
    dsquDepartsSt3: TDataSource;
    quSpecOut: TADOQuery;
    quTTNOutRec: TADOQuery;
    quSpecOutRec: TADOQuery;
    quTTNOutRecIDSKL: TIntegerField;
    quTTNOutRecIDATEDOC: TIntegerField;
    quTTNOutRecID: TLargeintField;
    quTTNOutRecDATEDOC: TDateTimeField;
    quTTNOutRecNUMDOC: TStringField;
    quTTNOutRecDATESF: TDateTimeField;
    quTTNOutRecNUMSF: TStringField;
    quTTNOutRecIDCLI: TIntegerField;
    quTTNOutRecSUMIN: TFloatField;
    quTTNOutRecSUMUCH: TFloatField;
    quTTNOutRecSUMTAR: TFloatField;
    quTTNOutRecIACTIVE: TSmallintField;
    quTTNOutRecOPRIZN: TSmallintField;
    quTTNOutRecSNUMZ: TStringField;
    quSpecOutIDHEAD: TLargeintField;
    quSpecOutID: TLargeintField;
    quSpecOutNUM: TIntegerField;
    quSpecOutIDCARD: TIntegerField;
    quSpecOutSBAR: TStringField;
    quSpecOutIDM: TIntegerField;
    quSpecOutQUANT: TFloatField;
    quSpecOutPRICEIN: TFloatField;
    quSpecOutSUMIN: TFloatField;
    quSpecOutPRICEIN0: TFloatField;
    quSpecOutSUMIN0: TFloatField;
    quSpecOutPRICER: TFloatField;
    quSpecOutSUMR: TFloatField;
    quSpecOutPRICER0: TFloatField;
    quSpecOutSUMR0: TFloatField;
    quSpecOutNDSPROC: TFloatField;
    quSpecOutSUMNDSIN: TFloatField;
    quSpecOutSUMNDSOUT: TFloatField;
    quSpecOutSNUMHEAD: TStringField;
    quSpecOutNAME: TStringField;
    quSpecOutFULLNAME: TStringField;
    quSpecOutBARCODE: TStringField;
    quSpecOutNDS: TFloatField;
    quSpecOutVOL: TFloatField;
    quSpecOutAVID: TIntegerField;
    quSpecOutMAKER: TIntegerField;
    quSpecOutRNAC: TFloatField;
    quSpecOutPRICE: TFloatField;
    quSpecOutRecIDHEAD: TLargeintField;
    quSpecOutRecID: TLargeintField;
    quSpecOutRecNUM: TIntegerField;
    quSpecOutRecIDCARD: TIntegerField;
    quSpecOutRecSBAR: TStringField;
    quSpecOutRecIDM: TIntegerField;
    quSpecOutRecQUANT: TFloatField;
    quSpecOutRecPRICEIN: TFloatField;
    quSpecOutRecSUMIN: TFloatField;
    quSpecOutRecPRICEIN0: TFloatField;
    quSpecOutRecSUMIN0: TFloatField;
    quSpecOutRecPRICER: TFloatField;
    quSpecOutRecSUMR: TFloatField;
    quSpecOutRecPRICER0: TFloatField;
    quSpecOutRecSUMR0: TFloatField;
    quSpecOutRecNDSPROC: TFloatField;
    quSpecOutRecSUMNDSIN: TFloatField;
    quSpecOutRecSUMNDSOUT: TFloatField;
    quSpecOutRecSNUMHEAD: TStringField;
    quSpecOutTTOVAR: TSmallintField;
    quSpecInID: TLargeintField;
    quSpecInTTOVAR: TSmallintField;
    dsquPosts2: TDataSource;
    quPosts2: TADOQuery;
    quPosts2IDHEAD: TLargeintField;
    quPosts2ID: TLargeintField;
    quPosts2NUM: TIntegerField;
    quPosts2IDCARD: TIntegerField;
    quPosts2SBAR: TStringField;
    quPosts2IDM: TIntegerField;
    quPosts2QUANT: TFloatField;
    quPosts2PRICEIN: TFloatField;
    quPosts2SUMIN: TFloatField;
    quPosts2PRICEIN0: TFloatField;
    quPosts2SUMIN0: TFloatField;
    quPosts2PRICEUCH: TFloatField;
    quPosts2SUMUCH: TFloatField;
    quPosts2NDSPROC: TFloatField;
    quPosts2SUMNDS: TFloatField;
    quPosts2GTD: TStringField;
    quPosts2SNUMHEAD: TStringField;
    quPosts2DATEDOC: TDateTimeField;
    quPosts2NUMDOC: TStringField;
    quPosts2IDCLI: TIntegerField;
    quPosts2IDSKL: TIntegerField;
    quPosts2Name: TStringField;
    quPosts2INN: TStringField;
    quPosts2PayNDS: TSmallintField;
    quPosts2PROCN: TFloatField;
    quCardsRemn: TFloatField;
    quMove: TADOQuery;
    quMoveISKL: TIntegerField;
    quMoveICODE: TIntegerField;
    quMoveIDATE: TIntegerField;
    quMoveITYPED: TIntegerField;
    quMoveIDDOC: TLargeintField;
    quMoveIDPOS: TLargeintField;
    quMoveQUANT: TFloatField;
    quMoveQUANTF: TFloatField;
    quMoveID: TLargeintField;
    quMoveREMN: TFloatField;
    dsquMove: TDataSource;
    quSpecInRecIDHEAD: TLargeintField;
    quSpecInRecID: TLargeintField;
    quSpecInRecNUM: TIntegerField;
    quSpecInRecIDCARD: TIntegerField;
    quSpecInRecSBAR: TStringField;
    quSpecInRecIDM: TIntegerField;
    quSpecInRecQUANT: TFloatField;
    quSpecInRecPRICEIN: TFloatField;
    quSpecInRecSUMIN: TFloatField;
    quSpecInRecPRICEIN0: TFloatField;
    quBuffPrISKL: TIntegerField;
    quBuffPrIDATE: TIntegerField;
    quBuffPrID: TLargeintField;
    quBuffPrIDCARD: TIntegerField;
    quBuffPrIDDOC: TIntegerField;
    quBuffPrTYPEDOC: TSmallintField;
    quBuffPrOLDPR: TFloatField;
    quBuffPrNEWPR: TFloatField;
    quBuffPrSTATUS: TSmallintField;
    quBuffPrSPERS: TStringField;
    quBuffPrNAMEMH: TStringField;
    quBuffPrIDCLI: TIntegerField;
    quBuffPrNUMDOC: TStringField;
    quBuffPrDATEDOC: TDateTimeField;
    quBuffPrNAMECLI: TStringField;
    quBuffPrNAMEC: TStringField;
    quBuffPrPRICE: TFloatField;
    quAcH: TADOQuery;
    dsquAcH: TDataSource;
    quSpecAc: TADOQuery;
    quAcRec: TADOQuery;
    quSpecAcRec: TADOQuery;
    quAcHIDATEDOC: TIntegerField;
    quAcHID: TLargeintField;
    quAcHDATEDOC: TDateTimeField;
    quAcHNUMDOC: TStringField;
    quAcHSUM1: TFloatField;
    quAcHSUM2: TFloatField;
    quAcHIACTIVE: TSmallintField;
    quAcRecIDSKL: TIntegerField;
    quAcRecIDATEDOC: TIntegerField;
    quAcRecID: TLargeintField;
    quAcRecDATEDOC: TDateTimeField;
    quAcRecNUMDOC: TStringField;
    quAcRecSUM1: TFloatField;
    quAcRecSUM2: TFloatField;
    quAcRecIACTIVE: TSmallintField;
    quSpecAcRecIDHEAD: TLargeintField;
    quSpecAcRecID: TLargeintField;
    quSpecAcRecNUM: TIntegerField;
    quSpecAcRecIDCARD: TIntegerField;
    quSpecAcRecSBAR: TStringField;
    quSpecAcRecQUANT: TFloatField;
    quSpecAcRecPRICER: TFloatField;
    quSpecAcRecPRICEN: TFloatField;
    quSpecAcRecSUMR: TFloatField;
    quSpecAcRecSUMN: TFloatField;
    quSpecAcIDHEAD: TLargeintField;
    quSpecAcID: TLargeintField;
    quSpecAcNUM: TIntegerField;
    quSpecAcIDCARD: TIntegerField;
    quSpecAcSBAR: TStringField;
    quSpecAcQUANT: TFloatField;
    quSpecAcPRICER: TFloatField;
    quSpecAcPRICEN: TFloatField;
    quSpecAcSUMR: TFloatField;
    quSpecAcSUMN: TFloatField;
    quSpecAcNAME: TStringField;
    quSpecAcTTOVAR: TSmallintField;
    quSpecAcEDIZM: TSmallintField;
    quAcHSum3: TFloatField;
    dsquDepartsStAc: TDataSource;
    quCardsMoveList: TADOQuery;
    quCardsMoveListICODE: TIntegerField;
    quSpecAcPRICE: TFloatField;
    quCardsTTOVAR: TSmallintField;
    quCashRep: TADOQuery;
    dsquCashRep: TDataSource;
    quCashRepIDS: TIntegerField;
    quCashRepNAME: TStringField;
    quCashRepId_Depart: TIntegerField;
    quCashRepIDate: TIntegerField;
    quCashRepCash_Code: TIntegerField;
    quCashRepNSmena: TIntegerField;
    quCashRepOperation: TSmallintField;
    quCashRepPaymentType: TSmallintField;
    quCashRepQCHECK: TIntegerField;
    quCashRepSUMR: TFloatField;
    quCashRepSUMD: TFloatField;
    quCashRepAVSUM: TFloatField;
    quTORep: TADOQuery;
    dsquTORep: TDataSource;
    quTORepISKL: TIntegerField;
    quTORepNAME: TStringField;
    quTORepIDATE: TIntegerField;
    quTORepSumIn: TFloatField;
    quTORepSumIn0: TFloatField;
    quTORepSumInR: TFloatField;
    quTORepSumInVn: TFloatField;
    quTORepSumInVnR: TFloatField;
    quTORepSumInv: TFloatField;
    quTORepSumOutIn: TFloatField;
    quTORepSumOutIn0: TFloatField;
    quTORepSumOutR: TFloatField;
    quTORepSumOutVn: TFloatField;
    quTORepSumOutVnR: TFloatField;
    quTORepSumAC: TFloatField;
    quTORepCashNal: TFloatField;
    quTORepCashDNal: TFloatField;
    quTORepCashBn: TFloatField;
    quTORepCashDBn: TFloatField;
    quTORepCashSS: TFloatField;
    quTORepCashSS10: TFloatField;
    quTORepCashSS20: TFloatField;
    quTORepSumInT: TFloatField;
    quTORepSumInVnT: TFloatField;
    quTORepSumOutT: TFloatField;
    quTORepSumOutVnT: TFloatField;
    quTORepSumInvT: TFloatField;
    quTORepSumBeg: TFloatField;
    quTORepSumEnd: TFloatField;
    quTORepSumTBeg: TFloatField;
    quTORepSumTEnd: TFloatField;
    quTOPrint: TADOQuery;
    quTOPrintRAZD: TIntegerField;
    quTOPrintTD: TIntegerField;
    quTOPrintID: TLargeintField;
    quTOPrintIDATEDOC: TIntegerField;
    quTOPrintNUMDOC: TStringField;
    quTOPrintIDCLI: TIntegerField;
    quTOPrintName: TStringField;
    quTOPrintSumR: TFloatField;
    quTOPrintSumIn: TFloatField;
    quTOPrintSumIn0: TFloatField;
    quTOPrintNAMERAZD: TStringField;
    quTOPrintNAMETD: TStringField;
    quTORepFULLNAME: TStringField;
    quCardsqOpSv: TFloatField;
    quOpSv: TADOQuery;
    quOpSvQuant: TFloatField;
    quSpecInvPrint1: TADOQuery;
    quSpecInvPrint1NUM: TIntegerField;
    quSpecInvPrint1IDCARD: TIntegerField;
    quSpecInvPrint1SBAR: TStringField;
    quSpecInvPrint1QUANTR: TFloatField;
    quSpecInvPrint1QUANTF: TFloatField;
    quSpecInvPrint1PRICER: TFloatField;
    quSpecInvPrint1SUMR: TFloatField;
    quSpecInvPrint1SUMIN0: TFloatField;
    quSpecInvPrint1SUMIN: TFloatField;
    quSpecInvPrint1SUMRF: TFloatField;
    quSpecInvPrint1SUMIN0F: TFloatField;
    quSpecInvPrint1SUMINF: TFloatField;
    quSpecInvPrint1QUANTC: TFloatField;
    quSpecInvPrint1SUMC: TFloatField;
    quSpecInvPrint1SUMRSC: TFloatField;
    quSpecInvPrint1SumRTO: TFloatField;
    quSpecInvPrint1FULLNAME: TStringField;
    quSpecInvPrint1EDIZM: TSmallintField;
    quSpecInvPrint1IDCLASSIF: TIntegerField;
    quSpecInvPrint1iGr: TIntegerField;
    quSpecInvPrint1sGr: TStringField;
    quSpecInvPrint1iSGr: TIntegerField;
    quSpecInvPrint1sSGr: TStringField;
    quSpecInvPrint1iSSGr: TIntegerField;
    quSpecInvPrint1sSSGr: TStringField;
    quVnH: TADOQuery;
    dsquVnH: TDataSource;
    quVnHIDSKLFROM: TIntegerField;
    quVnHIDSKLTO: TIntegerField;
    quVnHIDATEDOC: TIntegerField;
    quVnHID: TLargeintField;
    quVnHDATEDOC: TDateTimeField;
    quVnHNUMDOC: TStringField;
    quVnHSUMIN: TFloatField;
    quVnHSUMIN0: TFloatField;
    quVnHSUMR: TFloatField;
    quVnHIACTIVE: TSmallintField;
    quVnHSumD: TFloatField;
    quVnHNAMESKLFROM: TStringField;
    quVnHNAMESKLTO: TStringField;
    quSpecVn: TADOQuery;
    quSpecVnIDHEAD: TLargeintField;
    quSpecVnID: TLargeintField;
    quSpecVnNUM: TIntegerField;
    quSpecVnIDCARD: TIntegerField;
    quSpecVnIDM: TIntegerField;
    quSpecVnSBAR: TStringField;
    quSpecVnQUANT: TFloatField;
    quSpecVnPRICEIN: TFloatField;
    quSpecVnPRICEIN0: TFloatField;
    quSpecVnPRICER: TFloatField;
    quSpecVnSUMIN: TFloatField;
    quSpecVnSUMIN0: TFloatField;
    quSpecVnSUMR: TFloatField;
    quSpecVnNAME: TStringField;
    quSpecVnTTOVAR: TSmallintField;
    quSpecVnEDIZM: TSmallintField;
    quSpecVnPRICE: TFloatField;
    quVnRec: TADOQuery;
    quVnRecIDSKLFROM: TIntegerField;
    quVnRecIDSKLTO: TIntegerField;
    quVnRecIDATEDOC: TIntegerField;
    quVnRecID: TLargeintField;
    quVnRecDATEDOC: TDateTimeField;
    quVnRecNUMDOC: TStringField;
    quVnRecSUMIN: TFloatField;
    quVnRecSUMIN0: TFloatField;
    quVnRecSUMR: TFloatField;
    quVnRecIACTIVE: TSmallintField;
    quVnRecSumD: TFloatField;
    quSpecVnRec: TADOQuery;
    quSpecVnRecIDHEAD: TLargeintField;
    quSpecVnRecID: TLargeintField;
    quSpecVnRecNUM: TIntegerField;
    quSpecVnRecITYPEPOS: TSmallintField;
    quSpecVnRecIDCARD: TIntegerField;
    quSpecVnRecSBAR: TStringField;
    quSpecVnRecIDM: TIntegerField;
    quSpecVnRecQUANT: TFloatField;
    quSpecVnRecPRICEIN: TFloatField;
    quSpecVnRecPRICEIN0: TFloatField;
    quSpecVnRecPRICER: TFloatField;
    quSpecVnRecSUMIN: TFloatField;
    quSpecVnRecSUMIN0: TFloatField;
    quSpecVnRecSUMR: TFloatField;
    quAcHIDSKL: TIntegerField;
    quAcHNAMESKL: TStringField;
    quFLastMove: TADOQuery;
    quFLastMoveISKL: TIntegerField;
    quFLastMoveICODE: TIntegerField;
    quFLastMoveIDATE: TIntegerField;
    quFLastMoveITYPED: TIntegerField;
    quFLastMoveIDDOC: TLargeintField;
    quFLastMoveIDPOS: TLargeintField;
    quFLastMoveQUANT: TFloatField;
    quFLastMoveQUANTF: TFloatField;
    quShops: TADOQuery;
    dsquShops: TDataSource;
    quShopsID: TIntegerField;
    quShopsNAME: TStringField;
    quDepsRep: TADOQuery;
    dsquDepsRep: TDataSource;
    quDepsRepIDS: TIntegerField;
    quDepsRepID: TIntegerField;
    quDepsRepNAME: TStringField;
    quDepsRepFULLNAME: TStringField;
    quCliAss: TADOQuery;
    quCliAssIDCARD: TIntegerField;
    quRep1Data: TADOQuery;
    quRep1DataIDCLASSIF: TIntegerField;
    quRep1DataID: TIntegerField;
    quRep1DataNAME: TStringField;
    quRep1DataFULLNAME: TStringField;
    quRep1DataBARCODE: TStringField;
    quRep1DataEDIZM: TSmallintField;
    quRep1DataRNAC: TFloatField;
    quRep1DatarQB: TFloatField;
    quRep1DatarQIn: TFloatField;
    quRep1DatarQOut: TFloatField;
    quRep1DatasClass: TStringField;
    dsquRep1Data: TDataSource;
    quRep1DatarQE: TFloatField;
    quPosts3: TADOQuery;
    dsquPost3: TDataSource;
    quPosts3IDHEAD: TLargeintField;
    quPosts3ID: TLargeintField;
    quPosts3NUM: TIntegerField;
    quPosts3IDCARD: TIntegerField;
    quPosts3SBAR: TStringField;
    quPosts3IDM: TIntegerField;
    quPosts3QUANT: TFloatField;
    quPosts3PRICEIN: TFloatField;
    quPosts3SUMIN: TFloatField;
    quPosts3PRICEIN0: TFloatField;
    quPosts3SUMIN0: TFloatField;
    quPosts3PRICEUCH: TFloatField;
    quPosts3SUMUCH: TFloatField;
    quPosts3NDSPROC: TFloatField;
    quPosts3SUMNDS: TFloatField;
    quPosts3GTD: TStringField;
    quPosts3SNUMHEAD: TStringField;
    quPosts3DATEDOC: TDateTimeField;
    quPosts3NUMDOC: TStringField;
    quPosts3IDCLI: TIntegerField;
    quPosts3IDSKL: TIntegerField;
    quPosts3Name: TStringField;
    quPosts3INN: TStringField;
    quPosts3PayNDS: TSmallintField;
    teL: TdxMemData;
    teLiCode: TIntegerField;
    quDepsIDS: TIntegerField;
    quDepsID: TIntegerField;
    quDepsNAME: TStringField;
    quDepsFULLNAME: TStringField;
    quDepartsStIDS: TIntegerField;
    quDepartsStID: TIntegerField;
    quDepartsStNAME: TStringField;
    quDepartsStFULLNAME: TStringField;
    quDepartsStISTATUS: TSmallintField;
    quDepartsStIDORG: TIntegerField;
    quDepartsStGLN: TStringField;
    quDepartsStISS: TSmallintField;
    quDepartsStPRIOR: TSmallintField;
    quDepartsStPLATNDS: TSmallintField;
    quDepartsStINN: TStringField;
    quDepartsStKPP: TStringField;
    quDepartsStCITY: TStringField;
    quDepartsStSTREET: TStringField;
    quDepartsStHOUSE: TStringField;
    quDepartsStKORP: TStringField;
    quDepartsStPOSTINDEX: TStringField;
    quDepartsStPHONE: TStringField;
    quDepartsStSERLIC: TStringField;
    quDepartsStNUMLIC: TStringField;
    quDepartsStORGAN: TStringField;
    quDepartsStDATEB: TDateTimeField;
    quDepartsStDATEE: TDateTimeField;
    quDepartsStIP: TSmallintField;
    quDepartsStIPREG: TStringField;
    quDepartsStNAMEOTPR: TStringField;
    quDepartsStADROPR: TStringField;
    quDepartsStRSCH: TStringField;
    quDepartsStKSCH: TStringField;
    quDepartsStBIK: TStringField;
    quDepartsStBANK: TStringField;
    quGetNumDoc: TADOQuery;
    quGetNumDocretnum: TIntegerField;
    quGetNumDocretpre: TStringField;
    quCardsInv: TADOQuery;
    quCardsInvIDCARD: TIntegerField;
    quPrice: TADOQuery;
    quPriceGOODSID: TIntegerField;
    quPriceIDSKL: TIntegerField;
    quPricePRICE: TFloatField;
    quDocs: TADOQuery;
    quDocsID: TLargeintField;
    quRepPribData: TADOQuery;
    dsquRepPribData: TDataSource;
    quTTnInvID: TLargeintField;
    quSpecInvIDHEAD: TLargeintField;
    quSpecInvID: TLargeintField;
    quInvHRecID: TLargeintField;
    quSpecInvRecIDHEAD: TLargeintField;
    quSpecInvRecID: TLargeintField;
    quSpecInvPrint1IDHEAD: TLargeintField;
    quSpecInvPrint1ID: TLargeintField;
    quRepPribDataIDCLASSIF: TIntegerField;
    quRepPribDataID: TIntegerField;
    quRepPribDataNAME: TStringField;
    quRepPribDataFULLNAME: TStringField;
    quRepPribDataBARCODE: TStringField;
    quRepPribDataEDIZM: TSmallintField;
    quRepPribDataRNAC: TFloatField;
    quRepPribDatasClass: TStringField;
    quRepPribDatarQBIn: TFloatField;
    quRepPribDatarSBIn: TFloatField;
    quRepPribDatarSBIn0: TFloatField;
    quRepPribDatarSBInR: TFloatField;
    quRepPribDatarQBOut: TFloatField;
    quRepPribDatarSBOut: TFloatField;
    quRepPribDatarSBOut0: TFloatField;
    quRepPribDatarSBOutR: TFloatField;
    quRepPribDatarQIn: TFloatField;
    quRepPribDatarSIn: TFloatField;
    quRepPribDatarSIn0: TFloatField;
    quRepPribDatarSInR: TFloatField;
    quRepPribDatarQRetOut: TFloatField;
    quRepPribDatarSRetOut: TFloatField;
    quRepPribDatarSRetOut0: TFloatField;
    quRepPribDatarSRetOutR: TFloatField;
    quRepPribDatarQSpisOut: TFloatField;
    quRepPribDatarSSpisOut: TFloatField;
    quRepPribDatarSSpisOut0: TFloatField;
    quRepPribDatarSSpisOutR: TFloatField;
    quRepPribDatarQRealOut: TFloatField;
    quRepPribDatarSRealOut: TFloatField;
    quRepPribDatarSRealOut0: TFloatField;
    quRepPribDatarSRealOutR: TFloatField;
    quRepPribDatarQInvOut: TFloatField;
    quRepPribDatarSInvOut: TFloatField;
    quRepPribDatarSInvOut0: TFloatField;
    quRepPribDatarSInvOutR: TFloatField;
    quRepPribDatarQBeg: TFloatField;
    quRepPribDatarSBeg: TFloatField;
    quRepPribDatarSBeg0: TFloatField;
    quRepPribDatarQEnd: TFloatField;
    quRepPribDatarSEnd: TFloatField;
    quRepPribDatarSEnd0: TFloatField;
    quRepPribDatarSEndR: TFloatField;
    quRepPribDatarSBegR: TFloatField;
    quRepPribDatarSRealPrib: TFloatField;
    cxStyle27: TcxStyle;
    quRepPostRemn: TADOQuery;
    dsquRepPostRemn: TDataSource;
    quRepPostRemnIDSKL: TIntegerField;
    quRepPostRemnIDHEAD: TLargeintField;
    quRepPostRemnIDCARD: TIntegerField;
    quRepPostRemnQPART: TFloatField;
    quRepPostRemnQREMN: TFloatField;
    quRepPostRemnPRICEIN: TFloatField;
    quRepPostRemnPRICEIN0: TFloatField;
    quRepPostRemnPRICER: TFloatField;
    quRepPostRemnSumInRemn: TFloatField;
    quRepPostRemnSumIn0Remn: TFloatField;
    quRepPostRemnSumRRemn: TFloatField;
    quRepPostRemnIDATEDOC: TIntegerField;
    quRepPostRemnNUMDOC: TStringField;
    quRepPostRemnNUMSF: TStringField;
    quRepPostRemnIDCLI: TIntegerField;
    quRepPostRemnSUMIN: TFloatField;
    quRepPostRemnCLINAME: TStringField;
    quRepPostRemnCLIFULLNAME: TStringField;
    quRepPostRemnNAME: TStringField;
    quRepPostRemnFULLNAME: TStringField;
    quRepPostRemnBARCODE: TStringField;
    quRepPostRemnEDIZM: TSmallintField;
    quRepPostRemnRNAC: TFloatField;
    quRepPostRemnsClass: TStringField;
    quRepPostRemnDEPNAME: TStringField;
    quRepReal: TADOQuery;
    dsquRepReal: TDataSource;
    quRepRealId_Depart: TIntegerField;
    quRepRealCode: TIntegerField;
    quRepRealIDS: TIntegerField;
    quRepRealDEPNAME: TStringField;
    quRepRealNAME: TStringField;
    quRepRealBARCODE: TStringField;
    quRepRealEDIZM: TSmallintField;
    quRepRealRNAC: TFloatField;
    quRepRealQuant: TFloatField;
    quRepRealRSum: TFloatField;
    quRepRealPriceR: TFloatField;
    quRepRealDSUM: TFloatField;
    quRepRealNameCl: TStringField;
    quFindC5AVID: TIntegerField;
    quTTnInvDifR: TFloatField;
    quRepRD: TADOQuery;
    quRepRDICODE: TIntegerField;
    quRepRDISKL: TIntegerField;
    quRepRDIDCLASSIF: TIntegerField;
    quRepRDNAME: TStringField;
    quRepRDBARCODE: TStringField;
    quRepRDEDIZM: TSmallintField;
    quRepRDRNAC: TFloatField;
    quRepRDsDep: TStringField;
    quRepRDsClass: TStringField;
    quRepRDrQRemn: TFloatField;
    quGetSpeed: TADOQuery;
    quGetSpeedvspeed: TFloatField;
    quRepRDVSPEED: TFloatField;
    quRepPribDataIDS: TIntegerField;
    quRepPribDataIDDEP: TIntegerField;
    quRepPribDataNAMEDEP: TStringField;
    quScaleItemsCh: TADOQuery;
    quScaleItemsChSCALENUM: TIntegerField;
    quScaleItemsChPLU: TIntegerField;
    quScaleItemsChGOODSITEM: TIntegerField;
    quScaleItemsChNAME1: TStringField;
    quScaleItemsChNAME2: TStringField;
    quScaleItemsChPRICE: TFloatField;
    quScaleItemsChShelfLife: TSmallintField;
    quScaleItemsChTareWeight: TSmallintField;
    quScaleItemsChGroupCode: TSmallintField;
    quScaleItemsChMessageNo: TSmallintField;
    quScaleItemsChStatus: TSmallintField;
    quSelLasInvIDSKL: TIntegerField;
    quSelLasInvIDATEDOC: TIntegerField;
    quSelLasInvID: TLargeintField;
    quSelLasInvDATEDOC: TDateTimeField;
    quSelLasInvNUMDOC: TStringField;
    quSelLasInvSUMINR: TFloatField;
    quSelLasInvSUMINF: TFloatField;
    quSelLasInvSUMINRTO: TFloatField;
    quSelLasInvRSUMR: TFloatField;
    quSelLasInvRSUMF: TFloatField;
    quSelLasInvRSUMTO: TFloatField;
    quSelLasInvSUMTARR: TFloatField;
    quSelLasInvSUMTARF: TFloatField;
    quSelLasInvIACTIVE: TSmallintField;
    quSelLasInvIDETAIL: TSmallintField;
    quGetLog: TADOQuery;
    dsquGetLog: TDataSource;
    quGetLogIDATE: TIntegerField;
    quGetLogITYPED: TSmallintField;
    quGetLogIDH: TLargeintField;
    quGetLogID: TLargeintField;
    quGetLogISKL: TIntegerField;
    quGetLogIOPER: TSmallintField;
    quGetLogSPERS: TStringField;
    quGetLogDDATE: TDateTimeField;
    quGetLogDOCTYPE: TStringField;
    quGetLogNAMEDEP: TStringField;
    quRepRemnSum: TADOQuery;
    dsquRepRemnSum: TDataSource;
    quParts: TADOQuery;
    dsquParts: TDataSource;
    quPartsIDSKL: TIntegerField;
    quPartsIDATEDOC: TIntegerField;
    quPartsITYPED: TSmallintField;
    quPartsIDHEAD: TLargeintField;
    quPartsIDCARD: TIntegerField;
    quPartsIDPOS: TLargeintField;
    quPartsID: TLargeintField;
    quPartsPARTIN: TLargeintField;
    quPartsQPART: TFloatField;
    quPartsQREMN: TFloatField;
    quPartsPRICEIN: TFloatField;
    quPartsPRICEIN0: TFloatField;
    quPartsPRICER: TFloatField;
    quPartsNAME: TStringField;
    quCardsABC1: TStringField;
    quCardsABC2: TStringField;
    quCardsABC3: TStringField;
    quTTnInvCOMMENT: TStringField;
    quInvHRecCOMMENT: TStringField;
    quRepPribDatarQInvIn: TFloatField;
    quRepPribDatarSInvIn: TFloatField;
    quRepPribDatarSInvInR: TFloatField;
    quRepPribDatarSInvIn0: TFloatField;
    quRepPribDatarQInvItog: TFloatField;
    quRepPribDatarSInvItog: TFloatField;
    quRepPribDatarSInv0Itog: TFloatField;
    quRepPribDatarSInvRItog: TFloatField;
    quTORepSumInvR: TFloatField;
    quRepEconom: TADOQuery;
    dsquRepEconom: TDataSource;
    quRepEconomID: TIntegerField;
    quRepEconomNAME: TStringField;
    quRepEconomREMNR: TFloatField;
    quRepEconomREMNIN: TFloatField;
    quRepEconomREALR: TFloatField;
    quRepEconomREALIN: TFloatField;
    quRepEconomPRIB: TFloatField;
    quRepPribDatasClassGr: TStringField;
    quVnHCOMMENT: TStringField;
    quVnRecCOMMENT: TStringField;
    quTTnInvDifIn: TFloatField;
    quSpecInvPRICEUCH: TFloatField;
    quSpecInvRecPRICEUCH: TFloatField;
    teRP: TdxMemData;
    teRPIDCLASSIF: TIntegerField;
    teRPID: TIntegerField;
    teRPNAME: TStringField;
    teRPFULLNAME: TStringField;
    teRPBARCODE: TStringField;
    teRPEDIZM: TIntegerField;
    teRPRNAC: TFloatField;
    teRPsClass: TStringField;
    teRPrQBIn: TFloatField;
    teRPrSBIn: TFloatField;
    teRPrSBIn0: TFloatField;
    teRPrSBInR: TFloatField;
    teRPrQBOut: TFloatField;
    teRPrSBOut: TFloatField;
    teRPrSBOut0: TFloatField;
    teRPrSBOutR: TFloatField;
    teRPrQIn: TFloatField;
    teRPrSIn: TFloatField;
    teRPrSIn0: TFloatField;
    teRPrSInR: TFloatField;
    teRPrQRetOut: TFloatField;
    teRPrSRetOut: TFloatField;
    teRPrSRetOut0: TFloatField;
    teRPrSRetOutR: TFloatField;
    teRPrQSpisOut: TFloatField;
    teRPrSSpisOut: TFloatField;
    teRPrSSpisOut0: TFloatField;
    teRPrSSpisOutR: TFloatField;
    teRPrQRealOut: TFloatField;
    teRPrSRealOut: TFloatField;
    teRPrSRealOut0: TFloatField;
    teRPrSRealOutR: TFloatField;
    teRPrQInvOut: TFloatField;
    teRPrSInvOut: TFloatField;
    teRPrSInvOut0: TFloatField;
    teRPrSInvOutR: TFloatField;
    teRPrQBeg: TFloatField;
    teRPrSBeg: TFloatField;
    teRPrSBeg0: TFloatField;
    teRPrSBegR: TFloatField;
    teRPrQEnd: TFloatField;
    teRPrSEnd: TFloatField;
    teRPrSEnd0: TFloatField;
    teRPrSRealPrib: TFloatField;
    teRPIDS: TIntegerField;
    teRPIDDEP: TIntegerField;
    teRPNAMEDEP: TStringField;
    teRPrQInvIn: TFloatField;
    teRPrSInvIn: TFloatField;
    teRPrSInvIn0: TFloatField;
    teRPrSInvInR: TFloatField;
    teRPrQInvItog: TFloatField;
    teRPrSInvItog: TFloatField;
    teRPrSInv0Itog: TFloatField;
    teRPrSInvRItog: TFloatField;
    teRPsClassGr: TStringField;
    teRPrSEndR: TFloatField;
    quRepPribData1: TADOQuery;
    quRepPribData1ID: TIntegerField;
    quRepPribData1IDCLASSIF: TIntegerField;
    quRepPribData1NAME: TStringField;
    quRepPribData1FULLNAME: TStringField;
    quRepPribData1BARCODE: TStringField;
    quRepPribData1EDIZM: TSmallintField;
    quRepPribData1RNAC: TFloatField;
    quRepPribData1sClass: TStringField;
    quRepPribData1sClassGr: TStringField;
    quRepPribData1QUANT_B: TFloatField;
    quRepPribData1RSUMIN0_B: TFloatField;
    quRepPribData1RSUMIN_B: TFloatField;
    quRepPribData1RSUMR_B: TFloatField;
    quRepPribData1QUANTIN: TFloatField;
    quRepPribData1SUMIN0: TFloatField;
    quRepPribData1SUMIN: TFloatField;
    quRepPribData1SUMRIN: TFloatField;
    quRepPribData1QUANTOUT: TFloatField;
    quRepPribData1SUMIN0OUT: TFloatField;
    quRepPribData1SUMINOUT: TFloatField;
    quRepPribData1SUMROUT: TFloatField;
    quRepPribData1QUANTOUTVN: TFloatField;
    quRepPribData1SUMIN0OUTVN: TFloatField;
    quRepPribData1SUMINOUTVN: TFloatField;
    quRepPribData1SUMROUTVN: TFloatField;
    quRepPribData1QUANTINV: TFloatField;
    quRepPribData1SUMIN0INV: TFloatField;
    quRepPribData1SUMININV: TFloatField;
    quRepPribData1SUMRINV: TFloatField;
    quRepPribData1QUANTREAL: TFloatField;
    quRepPribData1SUMIN0REAL: TFloatField;
    quRepPribData1SUMINREAL: TFloatField;
    quRepPribData1SUMRREAL: TFloatField;
    teRR: TdxMemData;
    teRRIDCLASSIF: TIntegerField;
    teRRID: TIntegerField;
    teRRNAME: TStringField;
    teRRFULLNAME: TStringField;
    teRRBARCODE: TStringField;
    teRREDIZM: TIntegerField;
    teRRRNAC: TFloatField;
    teRRsClass: TStringField;
    teRRrQBeg: TFloatField;
    teRRrSBeg: TFloatField;
    teRRrSBeg0: TFloatField;
    teRRrSBegR: TFloatField;
    teRRIDS: TIntegerField;
    teRRIDDEP: TIntegerField;
    teRRNAMEDEP: TStringField;
    teRRsClassGr: TStringField;
    quRepRemnSumID: TIntegerField;
    quRepRemnSumIDCLASSIF: TIntegerField;
    quRepRemnSumNAME: TStringField;
    quRepRemnSumFULLNAME: TStringField;
    quRepRemnSumBARCODE: TStringField;
    quRepRemnSumEDIZM: TSmallintField;
    quRepRemnSumRNAC: TFloatField;
    quRepRemnSumsClass: TStringField;
    quRepRemnSumsClassGr: TStringField;
    quRepRemnSumQUANT_B: TFloatField;
    quRepRemnSumRSUMIN0_B: TFloatField;
    quRepRemnSumRSUMIN_B: TFloatField;
    quRepRemnSumRSUMR_B: TFloatField;
    quRepEconomIDS: TIntegerField;
    quCardsabcq: TSmallintField;
    quCardsabcp: TSmallintField;
    quRep1DatasClass2: TStringField;
    quRepPribData1sClass2: TStringField;
    teRPsClass2: TStringField;
    quRepPribData1abcq: TSmallintField;
    quRepPribData1abcp: TSmallintField;
    teRPABCP: TSmallintField;
    teRPABCQ: TSmallintField;
    teRRsClass2: TStringField;
    teRRABCP: TSmallintField;
    teRRABCQ: TSmallintField;
    quRepRemnSumsClass2: TStringField;
    quRepRemnSumabcq: TSmallintField;
    quRepRemnSumabcp: TSmallintField;
    quRepRDIGR2: TIntegerField;
    quRepRDAGR: TSmallintField;
    quRepRDAGR_1: TSmallintField;
    quRepRDsClass2: TStringField;
    quRepRDabcq: TSmallintField;
    quRepRDabcp: TSmallintField;
    quRepRealIGR2: TIntegerField;
    quRepRealAGR: TSmallintField;
    quRepRealAGR_1: TSmallintField;
    quRepRealsClass2: TStringField;
    quRepRealabcq: TSmallintField;
    quRepRealabcp: TSmallintField;
    quClientsDayZ: TIntegerField;
    quRepRDIDCLI: TIntegerField;
    quRepRDNameCli: TStringField;
    quRepRDDayZ: TIntegerField;
    quRepRDAZKOEF: TFloatField;
    quRepRDAZSTRAHR: TFloatField;
    quTTNInSUMOPL: TFloatField;
    quDepsDep: TADOQuery;
    quDepsDepID: TIntegerField;
    quRepSaleGr: TADOQuery;
    dsquRepSaleGr: TDataSource;
    quRepSaleGrISKL: TIntegerField;
    quRepSaleGrNAME: TStringField;
    quRepSaleGrIGR2: TIntegerField;
    quRepSaleGrNAMEGR: TStringField;
    quRepSaleGrIGR: TIntegerField;
    quRepSaleGrNAMESGR: TStringField;
    quRepSaleGrREALQUANT: TFloatField;
    quRepSaleGrREALSUM: TFloatField;
    quRepSaleGrQUANTDEP: TFloatField;
    quRepSaleGrSUMDEP: TFloatField;
    quRepSaleGrPROCQUANT: TFloatField;
    quRepSaleGrPROCSUM: TFloatField;
    quRepSaleAP: TADOQuery;
    dsquRepSaleAP: TDataSource;
    quRepSaleAPId_Depart: TIntegerField;
    quRepSaleAPIDate: TIntegerField;
    quRepSaleAPOperation: TSmallintField;
    quRepSaleAPRowNum: TLargeintField;
    quRepSaleAPDateOperation: TDateTimeField;
    quRepSaleAPIdHead: TLargeintField;
    quRepSaleAPCode: TIntegerField;
    quRepSaleAPBARCODE: TStringField;
    quRepSaleAPFULLNAME: TStringField;
    quRepSaleAPVOL: TFloatField;
    quRepSaleAPAVID: TIntegerField;
    quRepSaleAPQuant: TFloatField;
    quFDep: TADOQuery;
    quFDepIDS: TIntegerField;
    quFDepID: TIntegerField;
    quFDepNAME: TStringField;
    quFDepFULLNAME: TStringField;
    quFDepISTATUS: TSmallintField;
    quFDepIDORG: TIntegerField;
    quFDepGLN: TStringField;
    quFDepISS: TSmallintField;
    quFDepPRIOR: TSmallintField;
    quFDepPLATNDS: TSmallintField;
    quFDepINN: TStringField;
    quFDepKPP: TStringField;
    quFDepDIR1: TStringField;
    quFDepDIR2: TStringField;
    quFDepDIR3: TStringField;
    quFDepGB1: TStringField;
    quFDepGB2: TStringField;
    quFDepGB3: TStringField;
    quFDepCITY: TStringField;
    quFDepSTREET: TStringField;
    quFDepHOUSE: TStringField;
    quFDepKORP: TStringField;
    quFDepPOSTINDEX: TStringField;
    quFDepPHONE: TStringField;
    quFDepSERLIC: TStringField;
    quFDepNUMLIC: TStringField;
    quFDepORGAN: TStringField;
    quFDepDATEB: TDateTimeField;
    quFDepDATEE: TDateTimeField;
    quFDepIP: TSmallintField;
    quFDepIPREG: TStringField;
    quFDepNAMEOTPR: TStringField;
    quFDepADROPR: TStringField;
    quFDepRSCH: TStringField;
    quFDepKSCH: TStringField;
    quFDepBIK: TStringField;
    quFDepBANK: TStringField;
    quRepSaleAPItog: TADOQuery;
    quRepSaleAPItogCode: TIntegerField;
    quRepSaleAPItogBARCODE: TStringField;
    quRepSaleAPItogFULLNAME: TStringField;
    quRepSaleAPItogVOL: TFloatField;
    quRepSaleAPItogAVID: TIntegerField;
    quRepSaleAPItogQUANTITOG: TFloatField;
    teClassAlco: TStringField;
    quFAvid: TADOQuery;
    quFAvidID: TIntegerField;
    quFAvidNAME: TStringField;
    quFAvidAFM: TSmallintField;
    quRepSaleAPQuantDAL: TFloatField;
    quRepSaleAPMAKER: TIntegerField;
    quRepSaleAPNAME: TStringField;
    quRepSaleAPINN: TStringField;
    quRepSaleAPKPP: TStringField;
    procedure DataModuleCreate(Sender: TObject);
    procedure quPostsCalcFields(DataSet: TDataSet);
    procedure quBuffPrCalcFields(DataSet: TDataSet);
    procedure quScaleListBeforePost(DataSet: TDataSet);
    procedure quCashRepCalcFields(DataSet: TDataSet);
    procedure quRep1DataCalcFields(DataSet: TDataSet);
    procedure quRepPribDataCalcFields(DataSet: TDataSet);
    procedure quTTnInvCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

type TArrCli = array[1..20] of string;


function fGetID(iT:INteger):Integer;
function fGetNumDoc(iSkl,iT:INteger):string;
Function CanDo(Name_F:String):Boolean;
procedure RefreshQu(quQ:TADOQuery;iId:INteger);
function fTestDel(iT,iVal:INteger):Boolean;

procedure prReadClass;
procedure prReadClassCli;

procedure prReadCu;

Procedure ClassExpNewT( Node : TTreeNode; Tree:TTreeView);
Procedure ClassExpNewTCu( Node : TTreeNode; Tree:TTreeView);
Procedure ClassExpNewTCli( Node : TTreeNode; Tree:TTreeView);

function fCanDel(iT:INteger):Boolean;
function fGetSlave(Id:INteger):String;  //������ ������ ������� ���� ������� � ��������������
function fGetSlaveCli(Id:INteger):String;  //������ ������ ������� ���� ������� � ��������������

procedure prRefreshCards(iGr,iSkl:Integer;bNoActive,bRemn,bOS:Boolean);
procedure prRefreshCli(iGr:Integer;bNoActive:Boolean);

function prFindBar(sBar:String;iC:Integer;Var IDC:INteger):Boolean;
Function prFindNewArt(bVes:Boolean; Var sM:String):Integer;
function prFindPlu(iC,iDep:INteger):String;
Function prSDisc(iC,iDep:Integer):String;
function prFindMaker(iM:INteger):String;
function fSS(iSkl:INteger):INteger;
function prFLP(iCode,iSkl:Integer; Var rPrIn0,rPrIn,rPrR:Real):Real; //rPrIn
function fShopDep(iSkl:INteger):INteger;
procedure prAddCashLoadHist(iC,iSkl,iT:INteger;rPr,rD:Real);

function prOpenDate(iSkl:INteger):Integer;
function fTestInv(iSkl,iDate:INteger):Boolean;
Function prFindNewPlu(iScale:INteger):Integer;
procedure prAddToScale(iSc:Integer);
function fDepScale(iScale:INteger):Integer;

function prFindRemnDepD(iCode,iSkl,iDate:Integer):Real;
function prFindPrice(iCode,iSkl:INteger):Real;
procedure prFCli(iCli:INteger; Var sCli:TArrCli);
function prCashLoadFromDoc(IDH,iSkl,iTd:INteger; Memo1:TcxMemo):Boolean;

function fFullName(iSkl:INteger):String;

function prFindOPSVDep(iCode,iDep:INteger):Real;
function prFindLastMove(iC,iDep:INteger):Integer;
procedure prRecalcSS(iSkl,DateB,DateE:INteger; Memo1:tcxMemo);
procedure prRecalcPartIn(iSkl,DateB,DateE:INteger; Memo1:tcxMemo);
function prTestDepAlco(iDep:INteger):Boolean;

procedure prDelDoc(IDH,iDate,iDep,iType:INteger);
function prCalcSpeed(ICODE,iDep:INteger):Real;
procedure prWrLog(iDep,iTypeD,iOper,IDH:INteger;sComm:String);

function fDepsDep(iSkl:INteger):String;
function fAMark(AVid:INteger):String;  //���� ����� ������� �� �� ���������� ���� �������� , ���� �� ����� ������, ���� �� �������� ������


var
  dmMCS: TdmMCS;

implementation

uses Un1, u2fdk;

{$R *.dfm}

function fAMark(AVid:INteger):String;  //���� ����� ������� �� �� ���������� ���� �������� , ���� �� ����� ������, ���� �� �������� ������
begin
  Result:='0';
  if (AVid>0) and (AVid<1000) then
  begin
    try
      with dmMCS do
      begin
        quFAvid.Active:=False;
        quFAvid.Parameters.ParamByName('IDA').Value:=AVid;
        quFAvid.Active:=True;
        if quFAvid.RecordCount>0 then
          if quFAvidAFM.AsInteger=1 then Result:='5' else Result:='10';
        quFAvid.Active:=False;
      end;
    except
    end;  
  end;
end;


function fDepsDep(iSkl:INteger):String;
begin
  with dmMCS do
  begin
    Result:='';
    quDepsDep.Active:=False;
    quDepsDep.Parameters.ParamByName('IDEP').Value:=iSkl;
    quDepsDep.Active:=True;
    quDepsDep.First;
    while not quDepsDep.Eof do
    begin
      Result:=Result+','+its(quDepsDepID.AsInteger);
      quDepsDep.Next;
    end;
    quDepsDep.Active:=False;

    if Result>'' then Delete(Result,1,1);  
  end;
end;

procedure prWrLog(iDep,iTypeD,iOper,IDH:INteger;sComm:String);
begin
  with dmMCS do
  begin
    try
      quProc.SQL.Clear;
      quProc.SQL.Add('DECLARE @TYPED int = '+its(iTypeD));
      quProc.SQL.Add('DECLARE @ISKL int = '+its(iDep));
      quProc.SQL.Add('DECLARE @IOPER int = '+its(iOper));
      quProc.SQL.Add('DECLARE @IDHEAD int = '+its(IDH));
      quProc.SQL.Add('DECLARE @SPERS varchar(150) = '''+Person.Name+' ('+Person.WinName+') '+sComm+'''');
      quProc.SQL.Add('EXECUTE [dbo].[prInsLog]  @TYPED,@ISKL,@IOPER,@IDHEAD,@SPERS');
      quProc.ExecSQL;
    except
    end;
  end;
end;

function prCalcSpeed(ICODE,iDep:INteger):Real;
begin
  with dmMCS do
  begin
    try
      quGetSpeed.Active:=False;
      quGetSpeed.SQL.Clear;
      quGetSpeed.SQL.Add('DECLARE @ICODE int = '+its(ICODE));
      quGetSpeed.SQL.Add('DECLARE @ISKL int = '+its(iDep));
      quGetSpeed.SQL.Add('EXECUTE [dbo].[prCalcSpeedRealCard] @ISKL,@ICODE');
      quGetSpeed.Active:=True;

      Result:=quGetSpeedvspeed.AsFloat;
    except
      Result:=0;
    end;
    quGetSpeed.Active:=False;
  end;
end;


procedure prDelDoc(IDH,iDate,iDep,iType:INteger);
begin
  with dmMCS do
  begin
    quProc.SQL.Clear;
    quProc.SQL.Add('DECLARE @IDHEAD int = '+its(IDH));
    quProc.SQL.Add('DECLARE @IDATE int = '+its(iDate));
    quProc.SQL.Add('DECLARE @ISKL int = '+its(iDep));
    quProc.SQL.Add('DECLARE @ITYPED int = '+its(iType));
    quProc.SQL.Add('EXECUTE [dbo].[prDelDoc] @IDHEAD,@IDATE,@ISKL,@ITYPED');
    try
      quProc.ExecSQL;
    except
    end;
    
  end;
end;

function prTestDepAlco(iDep:INteger):Boolean;
Var StrWk:String;
begin
  with dmMCS do
  begin
    StrWk:='';

    quSel.Active:=False;
    quSel.SQL.Clear;
    quSel.SQL.Add('SELECT [SERLIC],[NUMLIC]');
    quSel.SQL.Add('FROM [dbo].[DEPARTS]    ');
    quSel.SQL.Add('where [ID]='+its(CurDep));
    quSel.Active:=True;
    if quSel.RecordCount>0 then  StrWk:=quSel.Fields.FieldByName('SERLIC').Value+quSel.Fields.FieldByName('NUMLIC').Value;
    quSel.Active:=False;

    StrWk:=Trim(StrWk);
    if StrWk>'' then Result:=True else Result:=False; //���� ����� �������� - ����� ������� ��������
  end;
end;

procedure prRecalcPartIn(iSkl,DateB,DateE:INteger; Memo1:tcxMemo);

  procedure wrmemo(StrPrint:String);
  begin
    if Memo1<>nil then Memo1.Lines.Add(StrPrint);
    WriteHistory(StrPrint);
    delay(10);
  end;

begin
  with dmMCS do
  begin
    wrmemo(fmt+'  ������ ...');

    wrmemo(fmt+'    - ������');
    quD.Active:=False;
    quD.SQL.Clear;
    quD.SQL.Add('DELETE FROM [dbo].[PARTIN] WHERE IDSKL='+its(iSkl)+' and IDATEDOC>='+its(DateB)+' and IDATEDOC<='+its(DateE));
    quD.ExecSQL;

    wrmemo(fmt+'    - ������');

    quProc.SQL.Clear;
    quProc.SQL.Add('DECLARE @ISKL int = '+its(iSkl));
    quProc.SQL.Add('DECLARE @IDATEB int = '+its(DateB));
    quProc.SQL.Add('declare @IDATEE int = '+its(DateE));
    quProc.SQL.Add('EXECUTE [dbo].[prDelPartOut] @ISKL,@IDATEB,@IDATEE');
    try
      quProc.ExecSQL;
    except
      wrmemo(fmt+'    ������');
    end;

    wrmemo(fmt+'  ����� .. ���� ������ ...');

    quProc.SQL.Clear;
    quProc.SQL.Add('DECLARE @ISKL int = '+its(iSkl));
    quProc.SQL.Add('DECLARE @IDATEB int = '+its(DateB));
    quProc.SQL.Add('declare @IDATEE int = '+its(DateE));
    quProc.SQL.Add('EXECUTE [dbo].[prCreatePartIn] @ISKL,@IDATEB,@IDATEE');
    try
      quProc.ExecSQL;
    except
      wrmemo(fmt+'    ������');
    end;
  end;
end;


procedure prRecalcSS(iSkl,DateB,DateE:INteger; Memo1:tcxMemo);
Var iCurD:INteger;

procedure wrmemo(StrPrint:String);
begin
  if Memo1<>nil then Memo1.Lines.Add(StrPrint);
  WriteHistory(StrPrint);
  delay(10);
end;

begin
  with dmMCS do
  begin
    wrmemo(fmt+'  ������ ...');

    wrmemo(fmt+'    - ������');
    quD.Active:=False;
    quD.SQL.Clear;
    quD.SQL.Add('DELETE FROM [dbo].[PARTIN] WHERE IDSKL='+its(iSkl)+' and IDATEDOC>='+its(DateB)+' and IDATEDOC<='+its(DateE));
    quD.ExecSQL;

    delay(100);

    wrmemo(fmt+'    - ������');

    quProc.SQL.Clear;
    quProc.SQL.Add('DECLARE @ISKL int = '+its(iSkl));
    quProc.SQL.Add('DECLARE @IDATEB int = '+its(DateB));
    quProc.SQL.Add('declare @IDATEE int = '+its(DateE));
    quProc.SQL.Add('EXECUTE [dbo].[prDelPartOut] @ISKL,@IDATEB,@IDATEE');
    try
      quProc.ExecSQL;
    except
      wrmemo(fmt+'    ������');
    end;

    delay(100);

    wrmemo(fmt+'  ������ ...');
    wrmemo(fmt+'    ������ ');

    quProc.SQL.Clear;
    quProc.SQL.Add('DECLARE @ISKL int = '+its(iSkl));
    quProc.SQL.Add('DECLARE @IDATEB int = '+its(DateB));
    quProc.SQL.Add('declare @IDATEE int = '+its(DateE));
    quProc.SQL.Add('EXECUTE [dbo].[prCreatePartIn] @ISKL,@IDATEB,@IDATEE');
    try
      quProc.ExecSQL;
    except
      wrmemo(fmt+'    ������');
    end;
    delay(100);

    wrmemo(fmt+'    ������ ');

    for iCurD:=DateB to DateE do
    begin
      try
        wrmemo(fmt+'      '+ds1(iCurD));

        wrmemo(fmt+'         - ��. ���������');

        quDocs.Active:=False;
        quDocs.SQL.Clear;
        quDocs.SQL.Add('SELECT [ID] FROM [dbo].[DOCVN_HEAD]');
        quDocs.SQL.Add('where [IDATEDOC]='+its(iCurD));
        quDocs.SQL.Add('and [IDSKLFROM]='+its(iSkl));
        quDocs.SQL.Add('and [IACTIVE]=3');
        quDocs.Active:=True;
        quDocs.First;
        while not quDocs.Eof do
        begin
          wrmemo(fmt+'            - '+its(quDocsID.AsInteger));

          quProc.SQL.Clear;
          quProc.SQL.Add('DECLARE @ITYPED int = 3');
          quProc.SQL.Add('DECLARE @IDH bigint = '+its(quDocsID.AsInteger));
          quProc.SQL.Add('DECLARE @IDEP int = 0');
          quProc.SQL.Add('DECLARE @IDATE int = 0');
          quProc.SQL.Add('EXECUTE [dbo].[prCalcPartOutDoc] @ITYPED,@IDH,@IDEP,@IDATE');
          quProc.ExecSQL;

          quDocs.Next;
        end;
        quDocs.Active:=False;
        delay(100);

        wrmemo(fmt+'         - �������');

        quDocs.Active:=False;
        quDocs.SQL.Clear;
        quDocs.SQL.Add('SELECT [ID] FROM [dbo].[DOCOUT_HEAD]');
        quDocs.SQL.Add('where [IDATEDOC]='+its(iCurD));
        quDocs.SQL.Add('and [IDSKL]='+its(iSkl));
        quDocs.SQL.Add('and [IACTIVE]=3');
        quDocs.Active:=True;
        quDocs.First;
        while not quDocs.Eof do
        begin
          wrmemo(fmt+'            - '+its(quDocsID.AsInteger));

          quProc.SQL.Clear;
          quProc.SQL.Add('DECLARE @ITYPED int = 2');
          quProc.SQL.Add('DECLARE @IDH bigint = '+its(quDocsID.AsInteger));
          quProc.SQL.Add('DECLARE @IDEP int = 0');
          quProc.SQL.Add('DECLARE @IDATE int = 0');
          quProc.SQL.Add('EXECUTE [dbo].[prCalcPartOutDoc] @ITYPED,@IDH,@IDEP,@IDATE');
          quProc.ExecSQL;

          quDocs.Next;
        end;
        quDocs.Active:=False;
        delay(100);

        wrmemo(fmt+'         - ����������');

        quProc.SQL.Clear;
        quProc.SQL.Add('DECLARE @ITYPED int = 7');
        quProc.SQL.Add('DECLARE @IDH bigint = 0');
        quProc.SQL.Add('DECLARE @IDEP int = '+its(iSkl));
        quProc.SQL.Add('DECLARE @IDATE int = '+its(iCurD));
        quProc.SQL.Add('EXECUTE [dbo].[prCalcPartOutDoc] @ITYPED,@IDH,@IDEP,@IDATE');
        quProc.ExecSQL;
        delay(100);

        wrmemo(fmt+'         - ��������������');

        quDocs.Active:=False;
        quDocs.SQL.Clear;
        quDocs.SQL.Add('SELECT [ID] FROM [dbo].[DOCINV_HEAD]');
        quDocs.SQL.Add('where [IDATEDOC]='+its(iCurD));
        quDocs.SQL.Add('and [IDSKL]='+its(iSkl));
        quDocs.SQL.Add('and [IACTIVE]=3');
        quDocs.Active:=True;
        quDocs.First;
        while not quDocs.Eof do
        begin
          wrmemo(fmt+'            - '+its(quDocsID.AsInteger));

          quProc.SQL.Clear;
          quProc.SQL.Add('DECLARE @ITYPED int = 20');
          quProc.SQL.Add('DECLARE @IDH bigint = '+its(quDocsID.AsInteger));
          quProc.SQL.Add('DECLARE @IDEP int = '+its(iSkl));
          quProc.SQL.Add('DECLARE @IDATE int = '+its(iCurD));
          quProc.SQL.Add('EXECUTE [dbo].[prCalcPartOutDoc] @ITYPED,@IDH,@IDEP,@IDATE');
          quProc.ExecSQL;

          quDocs.Next;
        end;
        quDocs.Active:=False;
        delay(100);

        quProc.SQL.Clear;
        quProc.SQL.Add('EXECUTE [dbo].[fReSetTA] '+its(iCurD)+','+its(iSkl));
        quProc.ExecSQL;

        quProc.SQL.Clear;
        quProc.SQL.Add('EXECUTE [dbo].[prCalcTO] '+its(iCurD)+','+its(iSkl));
        quProc.ExecSQL;

      except
        wrmemo(fmt+'    ������');
      end;
    end;

  end;
end;

function prFindLastMove(iC,iDep:INteger):Integer;
begin
  with dmMCS do
  begin
    Result:=0;
    quFLastMove.Active:=False;
    quFLastMove.Parameters.ParamByName('ISKL').Value:=iDep;
    quFLastMove.Parameters.ParamByName('ICODE').Value:=iC;
    quFLastMove.Active:=True;
    if quFLastMove.RecordCount>0 then result:=quFLastMoveIDATE.AsInteger;
    quFLastMove.Active:=False;
  end;
end;


function prFindOPSVDep(iCode,iDep:INteger):Real;
begin
  with dmMCS do
  begin
    Result:=0;
    quOpSv.Active:=False;
    quOpSv.Parameters.ParamByName('IDEP').Value:=iDep;
    quOpSv.Parameters.ParamByName('ICODE').Value:=iCode;
    quOpSv.Parameters.ParamByName('IDATE').Value:=Trunc(date);
    quOpSv.Active:=True;
    if quOpSv.RecordCount>0 then result:=quOpSvQuant.AsFloat;
    quOpSv.Active:=False;
  end;
end;

function prCashLoadFromDoc(IDH,iSkl,iTd:INteger; Memo1:TcxMemo):Boolean;
Var sPath,sCashLdd,sMessur,sCSz,sName:String;
    iNum:INteger;
    rPr:Real;
    fc,fb,fupd:TextFile;
    sPos,sB,sM,sr,sDel:String;
    iShop:INteger;
    bOk:Boolean;
    bAlco,bL:Boolean;
begin
  with dmMCS do
  begin
    bOk:=False;
    if CommonSet.UKM4=1 then
    begin
      try
        Memo1.Lines.Add('  ������������ ���� ������� �� ���������.');
        Memo1.Lines.Add('     - ��������� ����� ��������.'); delay(10);

        assignfile(fc,CurDir+Person.Name+'\'+'PLUCASH.DAT');
        assignfile(fb,CurDir+Person.Name+'\'+'BAR.DAT');

        Memo1.Lines.Add('     - ��������� ������.'); delay(10);
        sr:=',';

        //������������ ������
        rewrite(fc);
        rewrite(fb);
        CloseTe(teL);

        if iTd=1 then //�� ��������
        begin
          quSpecIn.Active:=False;
          quSpecIn.Parameters.ParamByName('IDH').Value:=quTTnInID.AsInteger;
          quSpecIn.Parameters.ParamByName('IDSKL').Value:=quTTNInIDSKL.AsInteger;  //��� ���������� ���
          quSpecIn.Active:=True;

          bAlco:=prTestDepAlco(quTTNInIDSKL.AsInteger);
          if bAlco then Memo1.Lines.Add('  �������� �������� ���������.') else Memo1.Lines.Add('  �������� �������� ���������.');

          quSpecIn.First;
          while not quSpecIn.Eof do
          begin
            iNum:=quSpecInIDCARD.AsInteger;
            rPr:=rv(quSpecInPRICEUCH.AsFloat);

            if teL.Locate('iCode',iNum,[])=False then
            begin

              teL.Append;
              teLiCode.AsInteger:=iNum;
              teL.Post;


              if abs(quSpecInPRICE.AsFloat-rPr)>0.001 then
              begin
                quProc.SQL.Clear;
                quProc.SQL.Add('EXECUTE [dbo].[fSetPRICE] '+its(iNum)+','+its(iSkl)+','+fs(rPr));
                quProc.ExecSQL;
                delay(33);
              end;

              quFCP.Active:=False;
              quFCP.SQL.Clear;
              quFCP.SQL.Add('SELECT * FROM [dbo].[CARDS] ca');
              quFCP.SQL.Add('left join [dbo].[CARDSPRICE] pr on pr.[GOODSID]=ca.[ID] and pr.[IDSKL]='+its(iSkl));
              quFCP.SQL.Add('where ca.[ID]='+its(iNum));
              quFCP.SQL.Add('and [TTOVAR]=0');
              quFCP.Active:=True;

              if (quFCP.RecordCount=1)and(rPr>0.01) then
              begin
                bL:=True;
                if quFCPAVID.AsInteger>0 then if bAlco=False then bL:=False;

                if bL then
                begin
                  quBars.Active:=False;
                  quBars.Parameters.ParamByName('IDC').Value:=iNum;
                  quBars.Active:=True;

                  if quFCPEDIZM.AsInteger=1 then sMessur:='��.' else sMessur:='��.';
                  sName:=quFCPFULLNAME.AsString;

                  sPos:='';

                  if (pos('��',sMessur)>0)or(pos('��',sMessur)>0) then sM:='0.001' else sM:='1';
                  if (quFCPISTATUS.AsInteger<100)and(rPr>0.001) then  sDel:='1' else sDel:='0';
                  sPos:=s1(quFCPID.AsString)+sr+s1(sName)+sr+s1(sMessur)+sr+sM+sr+sr+sr+sr+'0'+sr+'0'+sr+'0'+sr+'NOSIZE'+sr+fAMark(quFCPAVID.AsInteger)+sr+'0'+sr+'0'+sr+'0'+sr+'0'+sr+fs(rv(rPr))+sr+'0'+sr+its(iSkl)+sr+sr+sDel+sr+sr+sr+sr;
                  writeln(fc,AnsiToOemConvert(sPos));
                  Flush(fc);

                  quBars.First;
                  while not quBars.Eof do
                  begin
                    if quBarsBarStatus.AsInteger=0 then sCSz:='NOSIZE'
                    else sCSz:='QUANTITY';

                    sB:=quBarsBar.AsString+sr+quBarsGoodsID.AsString+sr+sCSz+sr+fs(quBarsQuant.AsFloat)+sr;
                    writeln(fb,AnsiToOemConvert(sB));
                    Flush(fb);

                    quBars.Next;
                  end;
                  quBars.Active:=False;

                  prAddCashLoadHist(iNum,iSkl,1,rPr,0);
                end;
              end;
              quFCP.Active:=False;

            end;
            quSpecIn.Next;
          end;
          quSpecIn.Active:=False;
        end;

        if iTd=3 then //��
        begin
          quSpecAc.Active:=False;
          quSpecAc.Parameters.ParamByName('IDH').Value:=quAcHID.AsInteger;
          quSpecAc.Parameters.ParamByName('ISKL').Value:=quAcHIDSKL.AsInteger;
          quSpecAc.Active:=True;

          bAlco:=prTestDepAlco(quAcHIDSKL.AsInteger);

          if bAlco then Memo1.Lines.Add('  �������� �������� ���������.') else Memo1.Lines.Add('  �������� �������� ���������.');

          quSpecAc.First;
          while not quSpecAc.Eof do
          begin
            iNum:=quSpecAcIDCARD.AsInteger;
//            rPr:=rv(quSpecAcPRICEN.AsFloat);

            if teL.Locate('iCode',iNum,[])=False then
            begin
              iNum:=quSpecAcIDCARD.AsInteger;
              rPr:=rv(quSpecAcPRICEN.AsFloat);

              teL.Append;
              teLiCode.AsInteger:=iNum;
              teL.Post;

              quProc.SQL.Clear;
              quProc.SQL.Add('EXECUTE [dbo].[fSetPRICE] '+its(iNum)+','+its(iSkl)+','+fs(rPr));
              quProc.ExecSQL;
              delay(33);

              quFCP.Active:=False;
              quFCP.SQL.Clear;
              quFCP.SQL.Add('SELECT * FROM [dbo].[CARDS] ca');
              quFCP.SQL.Add('left join [dbo].[CARDSPRICE] pr on pr.[GOODSID]=ca.[ID] and pr.[IDSKL]='+its(iSkl));
              quFCP.SQL.Add('where ca.[ID]='+its(iNum));
              quFCP.SQL.Add('and [TTOVAR]=0');
              quFCP.Active:=True;

              if (quFCP.RecordCount=1)and(rPr>0.01) then
              begin
                bL:=True;
                if quFCPAVID.AsInteger>0 then if bAlco=False then bL:=False;

                if bL then
                begin
                  quBars.Active:=False;
                  quBars.Parameters.ParamByName('IDC').Value:=iNum;
                  quBars.Active:=True;

                  if quFCPEDIZM.AsInteger=1 then sMessur:='��.' else sMessur:='��.';
                  sName:=quFCPFULLNAME.AsString;

                  sPos:='';

                  if (pos('��',sMessur)>0)or(pos('��',sMessur)>0) then sM:='0.001' else sM:='1';
                  if (quFCPISTATUS.AsInteger<100)and(rPr>0.001) then  sDel:='1' else sDel:='0';
                  sPos:=s1(quFCPID.AsString)+sr+s1(sName)+sr+s1(sMessur)+sr+sM+sr+sr+sr+sr+'0'+sr+'0'+sr+'0'+sr+'NOSIZE'+sr+fAMark(quFCPAVID.AsInteger)+sr+'0'+sr+'0'+sr+'0'+sr+'0'+sr+fs(rv(rPr))+sr+'0'+sr+its(iSkl)+sr+sr+sDel+sr+sr+sr+sr;
                  writeln(fc,AnsiToOemConvert(sPos));
                  Flush(fc);

                  quBars.First;
                  while not quBars.Eof do
                  begin
                    if quBarsBarStatus.AsInteger=0 then sCSz:='NOSIZE'
                    else sCSz:='QUANTITY';

                    sB:=quBarsBar.AsString+sr+quBarsGoodsID.AsString+sr+sCSz+sr+fs(quBarsQuant.AsFloat)+sr;
                    writeln(fb,AnsiToOemConvert(sB));
                    Flush(fb);

                    quBars.Next;
                  end;
                  quBars.Active:=False;
                  prAddCashLoadHist(iNum,iSkl,1,rPr,0);
                end;
              end;
              quFCP.Active:=False;
            end;
            quSpecAc.Next;
          end;
          quSpecAc.Active:=False;
        end;

        closefile(fc);
        closefile(fb);
        delay(100);

        teL.Active:=False;

        Memo1.Lines.Add('     - ������ Ok.'); delay(10);

        Memo1.Lines.Add('     - �������� ������ �� �����.'); delay(10);
        //����� ������� - ����� ������� � ���������� �����
        iShop:=fShopDep(CurDep);

        if DirectoryExists(CommonSet.POSLoad+its(iShop)) then
        begin
          sPath:=CommonSet.POSLoad+its(iShop)+'\';

          if Fileexists(sPath+'PLUCASH.DAT') or Fileexists(sPath+'BAR.DAT') then
          begin
            Memo1.Lines.Add('');
            Memo1.Lines.Add('  ����� ��� �� ���������� ���������� ������...');
            Memo1.Lines.Add('  �������� ����������!!!');
            Memo1.Lines.Add('  ���������� �����... ����� ����� ��������� ���');
            Memo1.Lines.Add('');
          end else
          begin
            if Fileexists(sPath+CashNon) or Fileexists(sPath+sCashLdd) then
            begin
              Memo1.Lines.Add('');
              Memo1.Lines.Add('  ����� ������������ ������.');
              Memo1.Lines.Add('  �������� ����������!!!');
              Memo1.Lines.Add('  ���������� �����... ����� ����� ��������� ���');
              Memo1.Lines.Add('');
            end else
            begin //��� ����� ���������� �������� �����
              MoveFile(PChar(CurDir+Person.Name+'\'+'PLUCASH.DAT'),PChar(sPath+'PLUCASH.DAT'));
              MoveFile(PChar(CurDir+Person.Name+'\'+'BAR.DAT'),PChar(sPath+'BAR.DAT'));
              assignfile(fupd,sPath+CashUpd);
              rewrite(fupd);
              closefile(fupd);

              bOk:=True;
              Memo1.Lines.Add('  �������� ��������� ��.');
            end;
          end;
        end else Memo1.Lines.Add('������ �������� . ��� ����� �������� - '+CommonSet.POSLoad+its(iShop));
      except
      end;
    end;

    if bOk then Result:=True else result:=False;
  end;
end;

procedure prFCli(iCli:INteger; Var sCli:TArrCli);
Var i:INteger;
begin
  with dmMCS do
  begin
    for i:=1 to 20 do  sCli[i]:='';
    quFCli.Active:=False;
    quFCli.SQL.Clear;
    quFCli.SQL.Add('SELECT * FROM [dbo].[CLIENTS] cli');
    quFCli.SQL.Add('where cli.[Id]='+its(iCli));
    quFCli.Active:=True;
    if quFCli.RecordCount=1 then
    begin
      sCli[1]:=quFCli.FieldByName('Name').AsString;
      sCli[2]:=quFCli.FieldByName('FullName').AsString;
      sCli[3]:=quFCli.FieldByName('INN').AsString;
      sCli[4]:=quFCli.FieldByName('KPP').AsString;
      sCli[5]:=quFCli.FieldByName('Gln').AsString;
      sCli[6]:=quFCli.FieldByName('NAMEOTP').AsString;
      sCli[7]:=quFCli.FieldByName('DogNum').AsString;
      sCli[8]:=quFCli.FieldByName('ADROTPR').AsString;
      sCli[9]:=quFCli.FieldByName('RSch').AsString;
      sCli[10]:=quFCli.FieldByName('KSch').AsString;
      sCli[11]:=quFCli.FieldByName('Bank').AsString;
      sCli[12]:=quFCli.FieldByName('Bik').AsString;
      sCli[13]:=quFCli.FieldByName('PostIndex').AsString;
      sCli[14]:=quFCli.FieldByName('Gorod').AsString;
      sCli[15]:=quFCli.FieldByName('Street').AsString;
      sCli[16]:=quFCli.FieldByName('House').AsString;
    end;
    quFCli.Active:=False;
  end;
end;

function prFindPrice(iCode,iSkl:INteger):Real;
begin
  with dmMCS do
  begin
    Result:=0;
    quFCP.Active:=False;
    quFCP.SQL.Clear;
    quFCP.SQL.Add('SELECT * FROM [dbo].[CARDS] ca');
    quFCP.SQL.Add('left join [dbo].[CARDSPRICE] pr on pr.[GOODSID]=ca.[ID] and pr.[IDSKL]='+its(iSkl));
    quFCP.SQL.Add('where ca.[ID]='+its(iCode));
//    quFCP.SQL.Add('and [TTOVAR]=0');
    quFCP.Active:=True;

    if quFCP.RecordCount=1 then
    begin
      Result:=quFCPPRICE.AsFloat;
    end;
    quFCP.Active:=False;
  end;
end;


function prFindRemnDepD(iCode,iSkl,iDate:Integer):Real;
begin
  with dmMCS do
  begin
    quRemn.Active:=False;
    quRemn.Parameters.ParamByName('ISKL').Value:=iSkl;
    quRemn.Parameters.ParamByName('ICODE').Value:=iCode;
    quRemn.Parameters.ParamByName('IDATE').Value:=iDate;
    quRemn.Active:=True;
    result:=quRemnQUANTR.AsFloat;
    quRemn.Active:=False;
  end;
end;

procedure prAddToScale(iSc:Integer);
Var sN1,sN2,s:String;
    iPlu,l:Integer;
    NumF:Integer;
begin
  with dmMCS do
  begin
    if taToScale.Active then
    begin
      taToScale.First;
      while not taToScale.Eof do
      begin
        sN1:=Copy(taToScaleName.AsString,1,29);
        l:=28;
        if (Pos(' ',sN1)>0) and(length(taToScaleName.AsString)>28) then
        begin
          s:=sN1[l];     //��� ������ ������ � ����� � ������ �������� - ������ �������������� � ���� ��������
          while s<>' ' do
          begin
            dec(l);
            s:=sN1[l];
          end;
        end;

        sN1:=Copy(taToScaleName.AsString,1,l);
        if Length(taToScaleName.AsString)>l then sN2:=Copy(taToScaleName.AsString,(l+1),28)
                                            else sN2:='';
        NumF:=0; //����� ��������

        quPluId.Active:=False;
        quPluId.Parameters.ParamByName('INUM').Value:=iSc;
        quPluId.Parameters.ParamByName('ICODE').Value:=taToScaleId.AsInteger;
        quPluId.Active:=True;
        if quPluId.RecordCount>0 then //����������
        begin
          quPluId.Edit;
          quPluIdNAME1.AsString:=sN1;
          quPluIdNAME2.AsString:=sN2;
          quPluIdPRICE.AsFloat:=rv(taToScalePrice.AsFloat);
          quPluIdShelfLife.AsInteger:=taToScaleSrok.AsInteger;
          quPluIdMessageNo.AsInteger:=NumF;
          quPluIdStatus.AsInteger:=0;
          quPluId.Post;
        end else  //���������
        begin
          //���� ��������� ���
          iPlu:=prFindNewPlu(iSc);
          if iPlu>0 then
          begin
            quPluId.Append;
            quPluIdSCALENUM.AsInteger:=iSc;
            quPluIdGOODSITEM.AsInteger:=taToScaleId.AsInteger;
            quPluIdPLU.AsInteger:=iPlu;
            quPluIdNAME1.AsString:=sN1;
            quPluIdNAME2.AsString:=sN2;
            quPluIdPRICE.AsFloat:=rv(taToScalePrice.AsFloat);
            quPluIdShelfLife.AsInteger:=taToScaleSrok.AsInteger;
            quPluIdMessageNo.AsInteger:=NumF;
            quPluIdGroupCode.AsInteger:=22;
            quPluIdStatus.AsInteger:=0;
            quPluId.Post;
          end;
        end;
        quPluId.Active:=False;

        taToScale.Next;
      end;
    end;
  end;
end;


function fTestInv(iSkl,iDate:INteger):Boolean;
begin
  result:=True;
  with dmMCS do
  begin
    try
      quSelLasInv.Active:=False;
      quSelLasInv.Parameters.ParamByName('ISKL').Value:=iSkl;
      quSelLasInv.Parameters.ParamByName('IDATE').Value:=iDate;
      quSelLasInv.Active:=True;
      if quSelLasInv.RecordCount>0 then Result:=False;
      quSelLasInv.Active:=False;
    except
    end;
  end;
end;

function prOpenDate(iSkl:INteger):Integer;
begin
  result:=0;
  with dmMCS do
  begin
    try
      quCloseSel.Active:=False;
      quCloseSel.Parameters.ParamByName('ISKL').Value:=iSkl;
      quCloseSel.Active:=True;
      if quCloseSel.RecordCount>0 then
      begin
        quCloseSel.First;
        Result:=quCloseSelIDATE.AsInteger+1;
      end;
      quCloseSel.Active:=False;
    except
    end;
  end;
end;

procedure prAddCashLoadHist(iC,iSkl,iT:INteger;rPr,rD:Real);
begin
  with dmMCS do
  begin
    try
      quCLHist.Active:=False;
      quCLHist.SQL.Clear;
      quCLHist.SQL.Add('SELECT [Code],[ISkl],[IDate],[Id],[DateLoad],[CType],[Price],[Disc],[Person]');
      quCLHist.SQL.Add('FROM [dbo].[CASHLOAD]');
      quCLHist.SQL.Add('where [Code]='+its(iC));
      quCLHist.SQL.Add('and [ISkl]='+its(iSkl));
      quCLHist.SQL.Add('and [IDate]='+its(Trunc(date)));
      quCLHist.Active:=True;

      quCLHist.Append;
      quCLHistCode.AsInteger:=iC;
      quCLHistISkl.AsInteger:=iSkl;
      quCLHistIDate.AsInteger:=Trunc(date);
      quCLHistDateLoad.AsDateTime:=now;
      if iT=1 then quCLHistCType.AsString:='C';
      if iT=2 then quCLHistCType.AsString:='D';
      if iT=3 then quCLHistCType.AsString:='P';
      if iT=4 then quCLHistCType.AsString:='K';
      if iT=5 then quCLHistCType.AsString:='�';
      quCLHistPrice.AsFloat:=rPr;
      quCLHistDisc.AsFloat:=rD;
      quCLHistPerson.AsString:=Person.WinName+' ('+Person.Name+')';
      quCLHist.Post;

      quCLHist.Active:=False;
    except
    end;
  end;
end;

function fDepScale(iScale:INteger):Integer;
begin
  with dmMCS do
  begin
    Result:=0;
    if iScale>0 then
    begin
      quF.Active:=False;
      quF.SQL.Clear;
      quF.SQL.Add('SELECT [DEPART] FROM [dbo].[SCALES]');
      quF.SQL.Add('where [IDAI]='+its(iScale));
      quF.Active:=True;
      quF.First;
      if quF.RecordCount>0 then Result:=quF.FieldByName('DEPART').Value;
      quF.Active:=False;
    end;
  end;
end;


function fShopDep(iSkl:INteger):Integer;
begin
  with dmMCS do
  begin
    Result:=0;
    if iSkl>0 then
    begin
      quF.Active:=False;
      quF.SQL.Clear;
      quF.SQL.Add('SELECT [IDS] FROM [dbo].[DEPARTS]');
      quF.SQL.Add('where [ID]='+its(iSkl));
      quF.Active:=True;
      quF.First;
      if quF.RecordCount>0 then Result:=quF.FieldByName('IDS').Value;
      quF.Active:=False;
    end;
  end;
end;

function prFLP(iCode,iSkl:Integer; Var rPrIn0,rPrIn,rPrR:Real):Real; //rPrIn
begin
  with dmMCS do
  begin
    rPrIn0:=0;
    rPrIn:=0;
    rPrR:=0;
    quF.Active:=False;
    quF.SQL.Clear;
    quF.SQL.Add('SELECT TOP 1  sp.[IDHEAD]');
    quF.SQL.Add(',sp.[ID],sp.[NUM],sp.[IDCARD]');
    quF.SQL.Add(',sp.[SBAR],sp.[IDM],sp.[QUANT],sp.[PRICEIN],sp.[SUMIN],sp.[PRICEIN0],sp.[SUMIN0]');
    quF.SQL.Add(',sp.[PRICEUCH],sp.[SUMUCH],sp.[NDSPROC],sp.[SUMNDS],sp.[GTD],sp.[SNUMHEAD],dh.[DATEDOC]');
    quF.SQL.Add(',dh.[IDATEDOC],dh.[IDSKL],dh.[IDCLI],dh.[IACTIVE]');
    quF.SQL.Add('FROM [dbo].[DOCIN_SPEC] sp');
    quF.SQL.Add('left join [dbo].[DOCIN_HEAD] dh on dh.[ID]=sp.[IDHEAD]');
    quF.SQL.Add('where [IDCARD]='+its(iCode));
    quF.SQL.Add('and dh.[IDSKL]='+its(iSkl));
    quF.SQL.Add('and dh.[IACTIVE]=3');
    quF.SQL.Add('order by dh.[DATEDOC] desc');

    quF.Active:=True;
    quF.First;
    if quF.RecordCount>0 then
    begin
      rPrIn0:=rv(quF.FieldByName('PRICEIN0').Value);
      rPrIn:=rv(quF.FieldByName('PRICEIN').Value);
      rPrR:=rv(quF.FieldByName('PRICEUCH').Value);
    end;
    quF.Active:=False;
    Result:=rPrIn;
  end;
end;


function fFullName(iSkl:INteger):String;
begin
  with dmMCS do
  begin
    Result:='';
    if iSkl>0 then
    begin
      quF.Active:=False;
      quF.SQL.Clear;
      quF.SQL.Add('SELECT [FULLNAME] FROM [dbo].[DEPARTS]');
      quF.SQL.Add('where [ID]='+its(iSkl));
      quF.Active:=True;
      quF.First;
      if quF.RecordCount>0 then Result:=quF.FieldByName('FULLNAME').Value;
      quF.Active:=False;
    end;
  end;
end;

function fSS(iSkl:INteger):INteger;
begin
  with dmMCS do
  begin
    Result:=1;
    if iSkl>0 then
    begin
      quF.Active:=False;
      quF.SQL.Clear;
      quF.SQL.Add('SELECT [ISS] FROM [dbo].[DEPARTS]');
      quF.SQL.Add('where [ID]='+its(iSkl));
      quF.Active:=True;
      quF.First;
      if quF.RecordCount>0 then Result:=quF.FieldByName('ISS').Value;
      quF.Active:=False;
    end;
  end;
end;


function prFindMaker(iM:INteger):String;
begin
  with dmMCS do
  begin
    Result:='';
    if iM>0 then
    begin
      quF.Active:=False;
      quF.SQL.Clear;
      quF.SQL.Add('SELECT TOP 1 [ID]');
      quF.SQL.Add('      ,[NAME]');
      quF.SQL.Add('      ,[INN]');
      quF.SQL.Add('      ,[KPP]');
      quF.SQL.Add('FROM [dbo].[AMAKER]');
      quF.SQL.Add('where [ID]='+its(iM));
      quF.Active:=True;
      quF.First;
      if quF.RecordCount>0 then Result:=quF.FieldByName('NAME').Value;
      quF.Active:=False;
    end;
  end;
end;

Function prSDisc(iC,iDep:Integer):String;
begin
  with dmMCS do
  begin
    Result:='';
    quF.Active:=False;
    quF.SQL.Clear;
    quF.SQL.Add('SELECT [DEPART]');
    quF.SQL.Add('      ,[ICARD]');
    quF.SQL.Add('      ,[MAXDISC]');
    quF.SQL.Add('FROM [dbo].[DISCSTOP]');
    quF.SQL.Add('where [DEPART]='+its(iDep)+' and [ICARD]='+its(iC));
    quF.Active:=True;
    if quF.RecordCount>0 then
    begin
      if quF.FieldByName('[MAXDISC]').Value<0.01 then Result:='������ �� ���������������';
    end;

    quF.Active:=False;
  end;
end;

function prFindPlu(iC,iDep:INteger):String;
Var Str1,StrSt:String;
begin
  Result:='';
  with dmMCS do
  begin
    quFindPlu.Active:=False;
    quFindPlu.Parameters.ParamByName('IDC').Value:=iC;
    quFindPlu.Parameters.ParamByName('IDEP').Value:=iDep;
    quFindPlu.Active:=True;
    Str1:='';
    if quFindPlu.RecordCount>0 then
    begin
      quFindPlu.First;
      while not quFindPlu.Eof do
      begin
        StrSt:='';
//        if quFindPluStatus.AsInteger=1 then StrSt:=' (����.)' else StrSt:=' (�� ����.)';
        Str1:=Str1+quFindPluSCALENUM.AsString+'-'+quFindPluPLU.AsString+StrSt+' , ';
        quFindPlu.Next;
      end;
      Result:=Copy(Str1,1,length(Str1)-2);
    end;
    quFindPlu.Active:=False;
  end;
end;


Function prFindNewPlu(iScale:INteger):Integer;
Var i:Integer;
begin
  with dmMCS do
  begin
    quSel.Active:=False;
    quSel.SQL.Clear;
    quSel.SQL.Add('Select num = dbo.getNewPlu ('+its(iScale)+')');
    quSel.Active:=True;
    i:=quSel.fieldbyName('num').AsInteger;
    quSel.Active:=False;
    Result:=i;
  end;
end;


Function prFindNewArt(bVes:Boolean; Var sM:String):Integer;
Var i:Integer;
begin
//  Result:=0;
  sM:='';
  with dmMCS do
  begin
    quSel.Active:=False;
    quSel.SQL.Clear;
    quSel.SQL.Add('Select dbo.getNewArt(');

    if bVes then
    begin
      quSel.SQL.Add(INtToStr(CommonSet.iMinVes)+','+INtToStr(CommonSet.iMaxVes)+')');
    end else
    begin
      quSel.SQL.Add(INtToStr(CommonSet.iMin)+','+INtToStr(CommonSet.iMax)+')');
    end;

    quSel.SQL.Add(' num');
    quSel.Active:=True;
    i:=quSel.fieldbyName('num').AsInteger;
    quSel.Active:=False;
    Result:=i;
    if Result=0 then
    begin
      if bVes then sM:='���������� �������� � ��������� �� '+INtToStr(CommonSet.iMinVes)+' �� '+INtToStr(CommonSet.iMaxVes)+' ���.'
      else sM:='���������� �������� � ��������� �� '+INtToStr(CommonSet.iMin)+' �� '+INtToStr(CommonSet.iMax)+' ���.';
    end;
  end;
end;


function prFindBar(sBar:String;iC:Integer;Var IDC:INteger):Boolean;
begin
  with dmMCS do
  begin
    IDC:=0;
    Result:=False;
    quFindBar.Close;
    if sBar='' then exit;
    quFindBar.Parameters.ParamByName('SBAR').Value:=sBar;
    quFindBar.Parameters.ParamByName('IC').Value:=iC;
    quFindBar.Open;
    if quFindBar.RecordCount=1 then
    begin
      result:=True;
      IDC:=quFindBarGoodsID.AsInteger;
    end;
    quFindBar.Close;

    if IDC=0 then
    begin
      quFindCBar.Active:=False;
      quFindCBar.Parameters.ParamByName('SBAR').Value:=sBar;
      quFindCBar.Active:=True;
      if quFindCBar.RecordCount>0 then
      begin
        quFindCBar.First;
        IDC:=quFindCBarID.AsInteger;
        if IDC<>iC then result:=True;
      end;
      quFindCBar.Active:=False;
    end;
  end;
end;


function fGetSlave(Id:INteger):String;  //������ ������ ������� ���� ������� � ��������������
Var StrWk:String;
    quSlave:TADOQuery;
begin
  with dmMCS do
  begin
    quSlave:=TADOQuery.Create(nil);
    quSlave.Active:=False;
    quSlave.Connection:=msConnection;
    quSlave.CommandTimeout:=500;
    quSlave.SQL.Clear;
    quSlave.SQL.Add('Select [ID] from [dbo].[CLASSIF]');
    quSlave.SQL.Add('where [IDPARENT]='+IntToStr(Id));
    quSlave.Active:=True;

    quSlave.First;
    while not quSlave.Eof do
    begin
      StrWk:=StrWk+INtToStr(quSlave.FieldByName('ID').AsInteger)+','+fGetSlave(quSlave.FieldByName('ID').AsInteger);
      quSlave.Next;
    end;
    quSlave.Active:=False;
    quSlave.Free;
  end;
  Result:=StrWk;
end;

function fGetSlaveCli(Id:INteger):String;  //������ ������ ������� ���� ������� � ��������������
Var StrWk:String;
    quSlave:TADOQuery;
begin
  with dmMCS do
  begin
    quSlave:=TADOQuery.Create(nil);
    quSlave.Active:=False;
    quSlave.Connection:=msConnection;
    quSlave.CommandTimeout:=500;
    quSlave.SQL.Clear;
    quSlave.SQL.Add('Select [ID] from [dbo].[CLASSIFCLI]');
    quSlave.SQL.Add('where [IDPARENT]='+IntToStr(Id));
    quSlave.Active:=True;

    quSlave.First;
    while not quSlave.Eof do
    begin
      StrWk:=StrWk+INtToStr(quSlave.FieldByName('ID').AsInteger)+','+fGetSlaveCli(quSlave.FieldByName('ID').AsInteger);
      quSlave.Next;
    end;
    quSlave.Active:=False;
    quSlave.Free;
  end;
  Result:=StrWk;
end;


procedure prRefreshCards(iGr,iSkl:Integer;bNoActive,bRemn,bOS:Boolean);
Var StrCl:String;
begin
  with dmMCS do
  begin
    dsquCards.DataSet:=nil;
    StrCl:=fGetSlave(iGr)+INtToStr(iGr);

    quCards.Active:=False;
    quCards.SQL.Clear;

    if bRemn=True then
    begin
      quCards.SQL.Add('SELECT ca.[IDCLASSIF],ca.[ID],ca.[NAME],ca.[FULLNAME],ca.[BARCODE],ca.[EDIZM],ca.[VOL],ca.[NDS],ca.[ISTATUS]');
      quCards.SQL.Add(',ca.[MANCAT],ca.[COUNTRY],ca.[BRAND],ca.[ISTENDER],ca.[ISTOP],ca.[ISNOV],ca.[CATEG],ca.[KREP]');
      quCards.SQL.Add(',ca.[AVID],ca.[RNAC],ca.[MAKER],ca.[TTOVAR],pr.[PRICE],cu.[NAME] as NAMECU,pers.[NAME] as NAMEPERS,br.[NAMEBRAND],cat.[SID]');

      if bOS then quCards.SQL.Add(',opsv.[Quant] as qOpSv') else quCards.SQL.Add(',ca.[RNAC]*0 as qOpSv');

      quCards.SQL.Add(',ca.[ABC1],ca.[ABC2],ca.[ABC3]');
      quCards.SQL.Add(',isnull(abcq.[AGR],4) as abcq');
      quCards.SQL.Add(',isnull(abcp.[AGR],4) as abcp');


      quCards.SQL.Add(',SUM(mv.[QUANT]) as Remn');
      quCards.SQL.Add('FROM [dbo].[CARDS] ca');
      quCards.SQL.Add('left join [dbo].[CARDSPRICE] pr on pr.[GOODSID]=ca.[ID] and pr.[IDSKL]='+its(iSkl));
      quCards.SQL.Add('left join [dbo].[COUNTRY] cu on cu.[ID]=ca.[COUNTRY]');
      quCards.SQL.Add('left join [dbo].[RPERSONAL] pers on pers.[ID]=ca.[MANCAT]');
      quCards.SQL.Add('left join [dbo].[BRANDS] br on br.[ID]=ca.[BRAND]');
      quCards.SQL.Add('left join [dbo].[CATEG] cat on cat.[ID]=isnull(ca.[CATEG],0)');
      quCards.SQL.Add('left join [dbo].[GDSMOVE] mv on mv.[ISKL]='+its(iSkl)+' and mv.[ICODE]=ca.[ID]');
      quCards.SQL.Add('left join [dbo].[ABC_Q] abcq on abcq.[ICODE]=ca.[ID] and abcq.[IDSKL]='+its(iSkl));
      quCards.SQL.Add('left join [dbo].[ABC_P] abcp on abcp.[ICODE]=ca.[ID] and abcp.[IDSKL]='+its(iSkl));


      if bOS then quCards.SQL.Add('left join [dbo].[vOperSv] opsv on (opsv.[Id_Depart]='+its(iSkl)+' and opsv.[IDate]='+its(Trunc(date))+' and opsv.[Code]=ca.[ID])');

      quCards.SQL.Add('where ca.[IDCLASSIF] in ('+StrCl+')');
//      quCards.SQL.Add('and ca.[TTOVAR]=0');
      if bNoActive then quCards.SQL.Add('and ca.[ISTATUS]>100')
      else quCards.SQL.Add('and ca.[ISTATUS]<=100');
      quCards.SQL.Add('group by ca.[IDCLASSIF],ca.[ID],ca.[NAME],ca.[FULLNAME],ca.[BARCODE],ca.[EDIZM],ca.[VOL],ca.[NDS],ca.[ISTATUS]');
      quCards.SQL.Add(',ca.[MANCAT],ca.[COUNTRY],ca.[BRAND],ca.[ISTENDER],ca.[ISTOP],ca.[ISNOV],ca.[CATEG],ca.[KREP]');
      quCards.SQL.Add(',ca.[AVID],ca.[RNAC],ca.[MAKER],ca.[TTOVAR],pr.[PRICE],cu.[NAME],pers.[NAME],br.[NAMEBRAND],cat.[SID]');

      if bOS then quCards.SQL.Add(',opsv.[Quant]');

      quCards.SQL.Add(',ca.[ABC1],ca.[ABC2],ca.[ABC3],abcq.[AGR],abcp.[AGR]');


      // as qOpSv

    end else
    begin
      quCards.SQL.Add('SELECT ca.[IDCLASSIF],ca.[ID],ca.[NAME],ca.[FULLNAME],ca.[BARCODE],ca.[EDIZM],ca.[VOL],ca.[NDS],ca.[ISTATUS]');
      quCards.SQL.Add(',ca.[MANCAT],ca.[COUNTRY],ca.[BRAND],ca.[ISTENDER],ca.[ISTOP],ca.[ISNOV],ca.[CATEG],ca.[KREP]');
      quCards.SQL.Add(',ca.[AVID],ca.[RNAC],ca.[MAKER],ca.[TTOVAR],pr.[PRICE],cu.[NAME] as NAMECU,pers.[NAME] as NAMEPERS,br.[NAMEBRAND],cat.[SID]');
      quCards.SQL.Add(',ca.[RNAC]*0 as Remn');

      if bOS then quCards.SQL.Add(',opsv.[Quant] as qOpSv') else quCards.SQL.Add(',ca.[RNAC]*0 as qOpSv');

      quCards.SQL.Add(',ca.[ABC1],ca.[ABC2],ca.[ABC3]');
      quCards.SQL.Add(',isnull(abcq.[AGR],4) as abcq');
      quCards.SQL.Add(',isnull(abcp.[AGR],4) as abcp');

      quCards.SQL.Add('FROM [dbo].[CARDS] ca');
      quCards.SQL.Add('left join [dbo].[CARDSPRICE] pr on pr.[GOODSID]=ca.[ID] and pr.[IDSKL]='+its(iSkl));
      quCards.SQL.Add('left join [dbo].[COUNTRY] cu on cu.[ID]=ca.[COUNTRY]');
      quCards.SQL.Add('left join [dbo].[RPERSONAL] pers on pers.[ID]=ca.[MANCAT]');
      quCards.SQL.Add('left join [dbo].[BRANDS] br on br.[ID]=ca.[BRAND]');
      quCards.SQL.Add('left join [dbo].[CATEG] cat on cat.[ID]=isnull(ca.[CATEG],0)');
      quCards.SQL.Add('left join [dbo].[ABC_Q] abcq on abcq.[ICODE]=ca.[ID] and abcq.[IDSKL]='+its(iSkl));
      quCards.SQL.Add('left join [dbo].[ABC_P] abcp on abcp.[ICODE]=ca.[ID] and abcp.[IDSKL]='+its(iSkl));

      if bOS then quCards.SQL.Add('left join [dbo].[vOperSv] opsv on (opsv.[Id_Depart]='+its(iSkl)+' and opsv.[IDate]='+its(Trunc(date))+' and opsv.[Code]=ca.[ID])');

      quCards.SQL.Add('where ca.[IDCLASSIF] in ('+StrCl+')');
//      quCards.SQL.Add('and ca.[TTOVAR]=0');
      if bNoActive then quCards.SQL.Add('and ca.[ISTATUS]>100')
      else quCards.SQL.Add('and ca.[ISTATUS]<=100');
    end;

    quCards.Active:=True;
    dsquCards.DataSet:=quCards;

{
SELECT ca.[IDCLASSIF],ca.[ID],ca.[NAME],ca.[FULLNAME],ca.[BARCODE],ca.[EDIZM],ca.[NDS],ca.[ISTATUS]
      ,ca.[MANCAT],ca.[COUNTRY],ca.[BRAND],ca.[ISTENDER],ca.[ISTOP],ca.[ISNOV],ca.[CATEG],ca.[KREP]
      ,ca.[AVID],ca.[RNAC],pr.[PRICE],cu.[NAME] as NAMECU,pers.[NAME] as NAMEPERS,br.[NAMEBRAND],cat.[SID]
  FROM [dbo].[CARDS] ca
  left join [dbo].[CARDSPRICE] pr on pr.[GOODSID]=ca.[ID] and pr.[IDSKL]=6
  left join [dbo].[COUNTRY] cu on cu.[ID]=ca.[COUNTRY]
  left join [dbo].[RPERSONAL] pers on pers.[ID]=ca.[MANCAT]
  left join [dbo].[BRANDS] br on br.[ID]=ca.[BRAND]
  left join [dbo].[CATEG] cat on cat.[ID]=isnull(ca.[CATEG],0)
  where ca.[IDCLASSIF]=4
  and ca.[TTOVAR]=0}

  end;
end;

procedure prRefreshCli(iGr:Integer;bNoActive:Boolean);
Var StrCl:String;
begin
  with dmMCS do
  begin
    StrCl:=fGetSlaveCli(iGr)+INtToStr(iGr);

    quClients.Active:=False;
    quClients.SQL.Clear;

    quClients.SQL.Add('SELECT * FROM [dbo].[CLIENTS] where [IdParent] in ('+StrCl+')');

    if bNoActive=False then quClients.SQL.Add('and [IActive]>0')
    else quClients.SQL.Add('and [IActive]=0');

    quClients.Active:=True;
  end;
end;


function fCanDel(iT:INteger):Boolean;
begin
  Result:=False;
  if iT=3 then //�������������
  begin
    if iDel>0 then
    begin
      with dmMCS do
      begin
        quSel.Active:=False;
        quSel.SQL.Clear;
        quSel.SQL.Add('SELECT TOP 2 [IDCLASSIF],[ID]');
        quSel.SQL.Add('FROM [dbo].[CARDS]');
        quSel.SQL.Add('where [IDCLASSIF]='+its(iDel));
        quSel.Active:=True;
        if quSel.RecordCount=0 then Result:=True else Result:=False;
        quSel.Active:=False;
      end;
    end;
  end;
  if iT=4 then //������������� �����������
  begin


    Result:=True;
  end;
end;

Procedure ClassExpNewT( Node : TTreeNode; Tree:TTreeView);
Var ID : Integer;
    TreeNode : TTreeNode;
Begin
// ��� ������ �������� ������ ������� ������ ���,
// ��� �� ����� ���������.
  if Node = nil then ID:=0
  else ID:=Integer(Node.Data);

  with dmMCS do
  begin
    while teClass.Locate('IdParent',ID,[]) do
    begin
      TreeNode:=Tree.Items.AddChildObject(Node,teClassNameClass.AsString+' ('+fs(teClassrNac.AsFloat)+'%)', Pointer(teClassId.AsInteger));
      TreeNode.ImageIndex:=12;
      TreeNode.SelectedIndex:=11;

      teClass.Delete;

      ClassExpNewT(TreeNode,Tree);
    end;
  end;
end;

Procedure ClassExpNewTCli( Node : TTreeNode; Tree:TTreeView);
Var ID : Integer;
    TreeNode : TTreeNode;
Begin
// ��� ������ �������� ������ ������� ������ ���,
// ��� �� ����� ���������.
  if Node = nil then ID:=0
  else ID:=Integer(Node.Data);

  with dmMCS do
  begin
    while teClassCli.Locate('IdParent',ID,[]) do
    begin
      TreeNode:=Tree.Items.AddChildObject(Node,teClassCliNameClass.AsString, Pointer(teClassCliId.AsInteger));
      TreeNode.ImageIndex:=12;
      TreeNode.SelectedIndex:=11;

      teClassCli.Delete;

      ClassExpNewTCli(TreeNode,Tree);
    end;
  end;
end;


Procedure ClassExpNewTCu( Node : TTreeNode; Tree:TTreeView);
Var ID : Integer;
    TreeNode : TTreeNode;
Begin
// ��� ������ �������� ������ ������� ������ ���,
// ��� �� ����� ���������.
  if Node = nil then ID:=0
  else ID:=Integer(Node.Data);

  with dmMCS do
  begin
    while teClass.Locate('IdParent',ID,[]) do
    begin
      TreeNode:=Tree.Items.AddChildObject(Node,teClassNameClass.AsString, Pointer(teClassId.AsInteger));
      TreeNode.ImageIndex:=12;
      TreeNode.SelectedIndex:=11;

      teClass.Delete;

      ClassExpNewTCu(TreeNode,Tree);
    end;
  end;
end;

procedure prReadCu;
begin
  with dmMCS do
  begin
    CloseTe(teClass);
    quCountry.Active:=False;
    quCountry.Active:=True;
    quCountry.First;
    while not quCountry.Eof do
    begin
      teClass.Append;
      teClassId.AsInteger:=quCountryID.AsInteger;
      teClassIdParent.AsInteger:=quCountryIDPARENT.AsInteger;
      teClassNameClass.AsString:=quCountryNAME.AsString;
      teClassKod.AsInteger:=quCountryKOD.AsInteger;
      teClass.Post;

      quCountry.next;
    end;
    quCountry.Active:=False;
  end;
end;


procedure prReadClass;
begin
  with dmMCS do
  begin
    CloseTe(teClass);
    quClassif.Active:=False;
    quClassif.Active:=True;
    quClassif.First;
    while not quClassif.Eof do
    begin
      teClass.Append;
      teClassId.AsInteger:=quClassifID.AsInteger;
      teClassIdParent.AsInteger:=quClassifIDPARENT.AsInteger;
      teClassNameClass.AsString:=quClassifNAME.AsString;
      teClassrNac.AsFloat:=quClassifNAC.AsFloat;
//      teClassAlco.AsString:=quClassifS1.AsString;
      teClass.Post;

      quClassif.next;
    end;
    quClassif.Active:=False;
  end;
end;

procedure prReadClassCli;
begin
  with dmMCS do
  begin
    CloseTe(teClassCli);
    quClassifCli.Active:=False;
    quClassifCli.Active:=True;
    quClassifCli.First;
    while not quClassifCli.Eof do
    begin
      teClassCli.Append;
      teClassCliId.AsInteger:=quClassifCliID.AsInteger;
      teClassCliIdParent.AsInteger:=quClassifCliIDPARENT.AsInteger;
      teClassCliNameClass.AsString:=quClassifCliNAME.AsString;
      teClassClirNac.AsFloat:=0;
      teClassCli.Post;

      quClassifCli.next;
    end;
    quClassifCli.Active:=False;
  end;
end;


function fTestDel(iT,iVal:INteger):Boolean;
begin
  result:=True;
  with dmMCS do
  begin
    if iT=1 then  //�������� ��������
    begin
      quT.Active:=False;
      quT.SQL.Clear;
      quT.SQL.Add('SELECT Count(*) as CountRec');
      quT.SQL.Add('FROM [dbo].[DEPARTS]');
      quT.SQL.Add('where [IDS]='+its(iVal));
      quT.Active:=True;
      if quT.FieldByName('CountRec').Value>0 then Result:=False;
      quT.Active:=False;
    end;
  end;
end;

procedure RefreshQu(quQ:TADOQuery;iId:INteger);
begin
  try
    quQ.Active:=False;
    quQ.Active:=True;
    if iID>0 then quQ.Locate('ID',IID,[]);
  except
  end;
end;

Function CanDo(Name_F:String):Boolean;
begin
  result:=True;
  with dmMCS do
  begin
    quCanDo.Active:=False;
    quCanDo.Parameters.ParamByName('Person_id').Value:=Person.Id;
    quCanDo.Parameters.ParamByName('Name_F').Value:=Name_F;
    quCanDo.Active:=True;
    if quCanDoPREXEC.AsInteger<>0 then Result:=False;
  end;
end;

function fGetNumDoc(iSkl,iT:INteger):string;
Var Strwk:String;
    iYear:INteger;
begin
  with dmMCS do
  begin
    StrWk:=FormatDateTime('YYYY',date);
    iYear:=StrToIntDef(StrWk,2014);

    quGetNumDoc.Active:=False;
    quGetNumDoc.Parameters.ParamByName('ISKL').Value:=iSkl;
    quGetNumDoc.Parameters.ParamByName('ITN').Value:=iT;
    quGetNumDoc.Parameters.ParamByName('IYEAR').Value:=iYear;
    quGetNumDoc.Active:=True;
    StrWk:=its(quGetNumDocretnum.AsInteger);
    while length(strwk)<5 do strwk:='0'+StrWk;
    if quGetNumDocretpre.AsString>'' then StrWk:=quGetNumDocretpre.AsString+'-'+StrWk;
    Result:=Strwk;
    quGetNumDoc.Active:=False;
  end;
end;


function fGetID(iT:INteger):Integer;
begin
  with dmMCS do
  begin
    quGetId.Active:=False;
    quGetId.Parameters.ParamByName('ITN').Value:=iT;
    quGetId.Active:=True;
    Result:=quGetIdretnum.AsInteger;
    quGetId.Active:=False;
  end;
end;

procedure TdmMCS.DataModuleCreate(Sender: TObject);
Var StrWk:String;
begin
  delay(10);
  with dmMCS do
  begin
    msConnection.Connected:=False;
    Strwk:='FILE NAME='+CurDir+'mcr.udl';
    msConnection.ConnectionString:=Strwk;
    delay(10);
    msConnection.Connected:=True;
    delay(10);
    if msConnection.Connected then
    begin
      taPersonal.Active:=True;

    end;
  end;
end;

procedure TdmMCS.quPostsCalcFields(DataSet: TDataSet);
begin
  quPostsPROCN.AsFloat:=0;
  if quPostsPRICEIN.AsFloat>0 then quPostsPROCN.AsFloat:=rv((quPostsPRICEUCH.AsFloat-quPostsPRICEIN.AsFloat)/quPostsPRICEIN.AsFloat*100);
end;

procedure TdmMCS.quBuffPrCalcFields(DataSet: TDataSet);
begin
  if quBuffPrTYPEDOC.AsInteger=1 then
  begin
    quBuffPrCLINAME1.AsString:=quBuffPrNAMECLI.AsString;
    quBuffPrNUMDOC1.AsString:=quBuffPrNUMDOC.AsString;
  end;
end;

procedure TdmMCS.quScaleListBeforePost(DataSet: TDataSet);
begin
  //fmScaleList.Tag
  quScaleListDEPART.AsInteger:=CurDep;
end;

procedure TdmMCS.quCashRepCalcFields(DataSet: TDataSet);
begin
  if quCashRepQCHECK.AsInteger>0 then quCashRepAVSUM.AsFloat:=rv(quCashRepSUMR.AsFloat/quCashRepQCHECK.AsInteger) else quCashRepAVSUM.AsFloat:=0;
end;

procedure TdmMCS.quRep1DataCalcFields(DataSet: TDataSet);
begin
  quRep1DatarQE.AsFloat:=quRep1DatarQB.AsFloat+quRep1DatarQIn.AsFloat+quRep1DatarQOut.AsFloat;
end;

procedure TdmMCS.quRepPribDataCalcFields(DataSet: TDataSet);
Var rQ:Real;
begin
  quRepPribDatarQBeg.AsFloat:=quRepPribDatarQBIn.AsFloat-quRepPribDatarQBOut.AsFloat;

  rQ:=RoundEx(quRepPribDatarQBeg.AsFloat);
  if abs(rQ)>=0.001 then
  begin
    quRepPribDatarSBeg.AsFloat:=quRepPribDatarSBIn.AsFloat-quRepPribDatarSBOut.AsFloat;
    quRepPribDatarSBeg0.AsFloat:=quRepPribDatarSBIn0.AsFloat-quRepPribDatarSBOut0.AsFloat;
    quRepPribDatarSBegR.AsFloat:=quRepPribDatarSBInR.AsFloat-quRepPribDatarSBOutR.AsFloat;
  end else
  begin
    quRepPribDatarSBeg.AsFloat:=0;
    quRepPribDatarSBeg0.AsFloat:=0;
    quRepPribDatarSBegR.AsFloat:=0;
  end;

  quRepPribDatarQEnd.AsFloat:=quRepPribDatarQBIn.AsFloat-quRepPribDatarQBOut.AsFloat+quRepPribDatarQIn.AsFloat-quRepPribDatarQRetOut.AsFloat-quRepPribDatarQSpisOut.AsFloat-quRepPribDatarQRealOut.AsFloat-quRepPribDatarQInvOut.AsFloat+quRepPribDatarQInvIn.AsFloat;
  quRepPribDatarSEnd.AsFloat:=quRepPribDatarSBIn.AsFloat-quRepPribDatarSBOut.AsFloat+quRepPribDatarSIn.AsFloat-quRepPribDatarSRetOut.AsFloat-quRepPribDatarSSpisOut.AsFloat-quRepPribDatarSRealOut.AsFloat-quRepPribDatarSInvOut.AsFloat+quRepPribDatarSInvIn.AsFloat;
  quRepPribDatarSEnd0.AsFloat:=quRepPribDatarSBIn0.AsFloat-quRepPribDatarSBOut0.AsFloat+quRepPribDatarSIn0.AsFloat-quRepPribDatarSRetOut0.AsFloat-quRepPribDatarSSpisOut0.AsFloat-quRepPribDatarSRealOut0.AsFloat-quRepPribDatarSInvOut0.AsFloat+quRepPribDatarSInvIn0.AsFloat;
  quRepPribDatarSEndR.AsFloat:=quRepPribDatarSBInR.AsFloat-quRepPribDatarSBOutR.AsFloat+quRepPribDatarSInR.AsFloat-quRepPribDatarSRetOutR.AsFloat-quRepPribDatarSSpisOutR.AsFloat-quRepPribDatarSRealOutR.AsFloat-quRepPribDatarSInvOutR.AsFloat+quRepPribDatarSInvInR.AsFloat;

  quRepPribDatarQInvItog.AsFloat:=quRepPribDatarQInvIn.AsFloat-quRepPribDatarQInvOut.AsFloat;
  quRepPribDatarSInvItog.AsFloat:=quRepPribDatarSInvIn.AsFloat-quRepPribDatarSInvOut.AsFloat;
  quRepPribDatarSInv0Itog.AsFloat:=quRepPribDatarSInvIn0.AsFloat-quRepPribDatarSInvOut0.AsFloat;
  quRepPribDatarSInvRItog.AsFloat:=quRepPribDatarSInvInR.AsFloat-quRepPribDatarSInvOutR.AsFloat;

  quRepPribDatarSRealPrib.AsFloat:=quRepPribDatarSRealOutR.AsFloat-quRepPribDatarSRealOut.AsFloat;
end;

procedure TdmMCS.quTTnInvCalcFields(DataSet: TDataSet);
begin
  quTTnInvDifR.AsFloat:=quTTnInvRSUMF.AsFloat-quTTnInvRSUMR.AsFloat;
  quTTnInvDifIn.AsFloat:=quTTnInvSUMINF.AsFloat-quTTnInvSUMINR.AsFloat;
end;

end.
