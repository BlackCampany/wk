object fmOplata: TfmOplata
  Left = 820
  Top = 263
  BorderStyle = bsDialog
  Caption = #1054#1087#1083#1072#1090#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
  ClientHeight = 297
  ClientWidth = 557
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 208
    Width = 557
    Height = 89
    Align = alBottom
    BevelInner = bvLowered
    Color = 16768443
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 20
      Width = 87
      Height = 13
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1086#1087#1083#1072#1090#1091
    end
    object Label2: TLabel
      Left = 16
      Top = 56
      Width = 70
      Height = 13
      Caption = #1050#1086#1084#1084#1077#1085#1090#1072#1088#1080#1081
    end
    object Label3: TLabel
      Left = 216
      Top = 20
      Width = 23
      Height = 13
      Caption = #1076#1072#1090#1072
    end
    object cxCalcEdit1: TcxCalcEdit
      Left = 112
      Top = 16
      EditValue = 0.000000000000000000
      TabOrder = 0
      Width = 89
    end
    object cxTextEdit1: TcxTextEdit
      Left = 112
      Top = 52
      TabOrder = 1
      Text = 'cxTextEdit1'
      Width = 241
    end
    object cxButton1: TcxButton
      Left = 368
      Top = 16
      Width = 65
      Height = 49
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      TabOrder = 2
      OnClick = cxButton1Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxDateEdit1: TcxDateEdit
      Left = 248
      Top = 16
      TabOrder = 3
      Width = 105
    end
    object cxButton2: TcxButton
      Left = 452
      Top = 16
      Width = 85
      Height = 49
      Caption = #1047#1072#1082#1088#1099#1090#1100
      Default = True
      TabOrder = 4
      OnClick = cxButton2Click
      LookAndFeel.Kind = lfOffice11
    end
  end
  object GrigOpl: TcxGrid
    Left = 8
    Top = 8
    Width = 513
    Height = 184
    TabOrder = 1
    LookAndFeel.Kind = lfOffice11
    object ViewOpl: TcxGridDBTableView
      PopupMenu = PopupMenu1
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dsquOplata
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'SUMPL'
          Column = ViewOplSUMPL
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      object ViewOplDATEPL: TcxGridDBColumn
        Caption = #1044#1072#1090#1072
        DataBinding.FieldName = 'DATEPL'
        Width = 76
      end
      object ViewOplSUMPL: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1086#1087#1083#1072#1090#1099
        DataBinding.FieldName = 'SUMPL'
        Width = 86
      end
      object ViewOplCOMMENT: TcxGridDBColumn
        Caption = #1050#1086#1084#1084#1077#1085#1090#1072#1088#1080#1081
        DataBinding.FieldName = 'COMMENT'
        Width = 324
      end
    end
    object LevelOpl: TcxGridLevel
      GridView = ViewOpl
    end
  end
  object quOplata: TADOQuery
    Connection = dmMCS.msConnection
    Parameters = <>
    SQL.Strings = (
      
        'SELECT [IDSKL],[IDDOC],[ID],[ITYPED],[IDATEPL],[DATEPL],[SUMPL],' +
        '[COMMENT]'
      '  FROM [MCRYSTAL].[dbo].[OPLATA]'
      '  where [IDSKL]=8'
      '  and [IDDOC]=444'
      '  and [ITYPED]=1'
      '  order by [IDATEPL]')
    Left = 88
    Top = 56
    object quOplataIDSKL: TIntegerField
      FieldName = 'IDSKL'
    end
    object quOplataIDDOC: TLargeintField
      FieldName = 'IDDOC'
    end
    object quOplataID: TLargeintField
      FieldName = 'ID'
      ReadOnly = True
    end
    object quOplataITYPED: TIntegerField
      FieldName = 'ITYPED'
    end
    object quOplataIDATEPL: TIntegerField
      FieldName = 'IDATEPL'
    end
    object quOplataDATEPL: TWideStringField
      FieldName = 'DATEPL'
      FixedChar = True
      Size = 10
    end
    object quOplataSUMPL: TFloatField
      FieldName = 'SUMPL'
      DisplayFormat = '0.00'
    end
    object quOplataCOMMENT: TStringField
      FieldName = 'COMMENT'
      Size = 200
    end
  end
  object dsquOplata: TDataSource
    DataSet = quOplata
    Left = 88
    Top = 112
  end
  object PopupMenu1: TPopupMenu
    Left = 176
    Top = 56
    object N1: TMenuItem
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1086#1087#1083#1072#1090#1091
      OnClick = N1Click
    end
  end
end
