with CM([ISKL],[ICODE],[IDATE],[ITYPED],[IDDOC],[IDPOS],[QUANT],[QUANTF],[KINV],[ID]) 
as ( SELECT cm.[ISKL],cm.[ICODE],cm.[IDATE],cm.[ITYPED],cm.[IDDOC],cm.[IDPOS],cm.[QUANT],cm.[QUANTF],cm.[KINV],row_number() over(order by cm.[ISKL],cm.[ICODE],cm.[IDATE],cm.[ITYPED],cm.[IDDOC],cm.[IDPOS]) 'ID'
     FROM [dbo].[GDSMOVE] cm where cm.[ISKL]=2 and cm.[ICODE]=100)
select t1.[ISKL],t1.[ICODE],t1.[IDATE],t1.[ITYPED],t1.[IDDOC],t1.[IDPOS],t1.[QUANT],t1.[QUANTF],t1.[KINV],t1.[ID],REMN=(isnull((Select SUM([QUANT]) from CM where [ID]<t1.[ID]),0)+t1.[QUANT]) from CM t1

