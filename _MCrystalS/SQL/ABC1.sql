/****** Сценарий для команды SelectTopNRows среды SSMS  ******/
declare @IDATEB int
declare @IDATEE int

declare @I_A int
declare @I_B int
declare @I_C int

Set @I_A=(SElect RPROC from dbo.ABCLIST where ID=1)
Set @I_B=(SElect RPROC from dbo.ABCLIST where ID=2)
Set @I_C=(SElect RPROC from dbo.ABCLIST where ID=3)

Set @IDATEB=[dbo].d2i('20150701')
Set @IDATEE=[dbo].d2i('20150731')

declare @tsum table(ISKL int, RVAL float, RVAL_A float, RVAL_B float,  RVAL_C float)

insert into @tsum (ISKL,RVAL,RVAL_A,RVAL_B,RVAL_C)
(SELECT [IDSKL]
       ,SUM([PRICER]*[QUANT]-[PRICEIN]*[QUANT])
       ,SUM([PRICER]*[QUANT]-[PRICEIN]*[QUANT])/100*@I_A
       ,SUM([PRICER]*[QUANT]-[PRICEIN]*[QUANT])/100*@I_B
       ,SUM([PRICER]*[QUANT]-[PRICEIN]*[QUANT])/100*@I_C
         
  FROM [MCRYSTAL].[dbo].[PARTOUT]
  where [IDATEDOC]>=@IDATEB
  and [IDATEDOC]<=@IDATEE 
  and [ITYPED]=7
  and [PRICEIN]<=[PRICER]
  group by  [IDSKL]
  )
  
  Select * from @tsum

 declare @tsumr table(ISKL int, ICODE int, RSUM float)

 insert into @tsumr (ISKL,ICODE,RSUM)
 (SELECT [IDSKL]
        ,[IDCARD]
        ,SUM([PRICER]*[QUANT]-[PRICEIN]*[QUANT])
  FROM [MCRYSTAL].[dbo].[PARTOUT] po
  where [IDATEDOC]>=@IDATEB
  and [IDATEDOC]<=@IDATEE 
  and [ITYPED]=7
  and [PRICEIN]<=[PRICER]
  group by  [IDSKL],[IDCARD]
) ;
  
  Select sr.ISKL,sr.ICODE,sr.RSUM,ca.IDCLASSIF,cl.[IGR2] from @tsumr sr
  left join [dbo].[CARDS] ca on ca.ID=sr.ICODE
  left join [dbo].[vClassGr2] cl on cl.ID=ca.IDCLASSIF
  where sr.ISKL=1 and cl.IGR2=222
  order by sr.ISKL,sr.RSUM desc

   SELECT   SUM(sr.RSUM)
		   ,SUM(sr.RSUM)/100*@I_A
		   ,SUM(sr.RSUM)/100*@I_B
		   ,SUM(sr.RSUM)/100*@I_C
   FROM  @tsumr sr
	  left join [dbo].[CARDS] ca on ca.ID=sr.ICODE
	  left join [dbo].[vClassGr2] cl on cl.ID=ca.IDCLASSIF
	  where sr.ISKL=1 and cl.IGR2=222

  
  declare @tsumr_n table(ISKL int, RSUM float, ICODE int, ID int, RSUMN float);
 
  with SR([ISKL],[RSUM],[ICODE],[ID]) 
  as (SELECT sr.[ISKL],sr.[RSUM],sr.[ICODE],row_number() over(order by sr.[ISKL],sr.[RSUM] desc,sr.[ICODE]) 'ID'
      FROM @tsumr sr 
      left join [dbo].[CARDS] ca on ca.ID=sr.ICODE
	  left join [dbo].[vClassGr2] cl on cl.ID=ca.IDCLASSIF
	  where sr.ISKL=1 and cl.IGR2=222)
  insert into @tsumr_n (ISKL,RSUM,ICODE,ID,RSUMN)
  (
  select t1.[ISKL],t1.[RSUM],t1.[ICODE],t1.[ID],RSUM_NAR=(isnull((Select SUM([RSUM]) from SR where [ID]<t1.[ID]),0)+t1.[RSUM]) 
  from SR t1
  --order by t1.[ISKL],t1.[RSUM] desc,t1.[ICODE]
  )

  Select * from @tsumr_n -- where RSUMN<=71263 order by ID
  --order by [ISKL],[RSUM] desc

/* 
SELECT [IDSKL]
      ,[IDCARD]
      ,ts.RVAL_A
      ,ts.RVAL_B
      ,ts.RVAL_C
      ,SUM([PRICER]*[QUANT]-[PRICEIN]*[QUANT])
      
         
  FROM [MCRYSTAL].[dbo].[PARTOUT] po
  left join @tsum ts on [IDSKL]=[ISKL]
  where [IDATEDOC]>=@IDATEB
  and [IDATEDOC]<=@IDATEE 
  and [ITYPED]=7
  group by  [IDSKL],[IDCARD],ts.RVAL_A,ts.RVAL_B,ts.RVAL_C
  order by [IDSKL],SUM([PRICER]*[QUANT]-[PRICEIN]*[QUANT]) desc
  */

