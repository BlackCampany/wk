/*
   2 февраля 2015 г.12:39:00
   Пользователь: sa
   Сервер: BEB-HO-SYS4
   База данных: MCRYSTAL
   Приложение: 
*/

/* Чтобы предотвратить возможность потери данных, необходимо внимательно просмотреть этот скрипт, прежде чем запускать его вне контекста конструктора баз данных.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.CARDS ADD
	ABC1 varchar(1) NULL,
	ABC2 varchar(1) NULL,
	ABC3 varchar(1) NULL
GO
ALTER TABLE dbo.CARDS SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
