unit Departs;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxImageComboBox, SpeedBar, ExtCtrls,
  cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxClasses, cxControls, cxGridCustomView, cxGrid, Placemnt, ADODB,
  cxContainer, cxTextEdit, cxMemo, ActnList, XPStyleActnCtrls, ActnMan;

type
  TfmDeparts = class(TForm)
    GridDeps: TcxGrid;
    ViewDeps: TcxGridDBTableView;
    LevelDeps: TcxGridLevel;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    FormPlacement1: TFormPlacement;
    quDeps: TADOQuery;
    dsquDeps: TDataSource;
    quDepsIDS: TIntegerField;
    quDepsID: TIntegerField;
    quDepsNAME: TStringField;
    quDepsFULLNAME: TStringField;
    quDepsISTATUS: TSmallintField;
    quDepsIDORG: TIntegerField;
    quDepsGLN: TStringField;
    quDepsISS: TSmallintField;
    quDepsPRIOR: TSmallintField;
    quDepsINN: TStringField;
    quDepsKPP: TStringField;
    quDepsDIR1: TStringField;
    quDepsDIR2: TStringField;
    quDepsDIR3: TStringField;
    quDepsGB1: TStringField;
    quDepsGB2: TStringField;
    quDepsGB3: TStringField;
    quDepsCITY: TStringField;
    quDepsSTREET: TStringField;
    quDepsHOUSE: TStringField;
    quDepsKORP: TStringField;
    quDepsPOSTINDEX: TStringField;
    quDepsPHONE: TStringField;
    quDepsSERLIC: TStringField;
    quDepsNUMLIC: TStringField;
    quDepsORGAN: TStringField;
    quDepsDATEB: TDateTimeField;
    quDepsDATEE: TDateTimeField;
    quDepsIP: TSmallintField;
    quDepsIPREG: TStringField;
    quDepsNAMESH: TStringField;
    ViewDepsID: TcxGridDBColumn;
    ViewDepsNAME: TcxGridDBColumn;
    ViewDepsFULLNAME: TcxGridDBColumn;
    ViewDepsISTATUS: TcxGridDBColumn;
    ViewDepsGLN: TcxGridDBColumn;
    ViewDepsISS: TcxGridDBColumn;
    ViewDepsIP: TcxGridDBColumn;
    ViewDepsNAMESH: TcxGridDBColumn;
    Memo1: TcxMemo;
    amDeps: TActionManager;
    acAdd: TAction;
    acEdit: TAction;
    acDel: TAction;
    acExit: TAction;
    quDepId: TADOQuery;
    quDepIdIDS: TIntegerField;
    quDepIdID: TIntegerField;
    quDepIdNAME: TStringField;
    quDepIdFULLNAME: TStringField;
    quDepIdISTATUS: TSmallintField;
    quDepIdIDORG: TIntegerField;
    quDepIdGLN: TStringField;
    quDepIdISS: TSmallintField;
    quDepIdPRIOR: TSmallintField;
    quDepIdINN: TStringField;
    quDepIdKPP: TStringField;
    quDepIdDIR1: TStringField;
    quDepIdDIR2: TStringField;
    quDepIdDIR3: TStringField;
    quDepIdGB1: TStringField;
    quDepIdGB2: TStringField;
    quDepIdGB3: TStringField;
    quDepIdCITY: TStringField;
    quDepIdSTREET: TStringField;
    quDepIdHOUSE: TStringField;
    quDepIdKORP: TStringField;
    quDepIdPOSTINDEX: TStringField;
    quDepIdPHONE: TStringField;
    quDepIdSERLIC: TStringField;
    quDepIdNUMLIC: TStringField;
    quDepIdORGAN: TStringField;
    quDepIdDATEB: TDateTimeField;
    quDepIdDATEE: TDateTimeField;
    quDepIdIP: TSmallintField;
    quDepIdIPREG: TStringField;
    acView: TAction;
    SpeedItem5: TSpeedItem;
    quDepsPLATNDS: TSmallintField;
    quDepsNAMEOTPR: TStringField;
    quDepsADROPR: TStringField;
    quDepsRSCH: TStringField;
    quDepsKSCH: TStringField;
    quDepsBIK: TStringField;
    quDepsBANK: TStringField;
    quDepIdNAMEOTPR: TStringField;
    quDepIdADROPR: TStringField;
    quDepIdRSCH: TStringField;
    quDepIdKSCH: TStringField;
    quDepIdBIK: TStringField;
    quDepIdBANK: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure acAddExecute(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure acEditExecute(Sender: TObject);
    procedure acViewExecute(Sender: TObject);
    procedure ViewDepsDblClick(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmDeparts: TfmDeparts;

implementation

uses Un1, Dm, AddMH;

{$R *.dfm}

procedure TfmDeparts.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=sFormIni;
  FormPlacement1.Active:=True;
  GridDeps.Align:=AlClient;
end;

procedure TfmDeparts.acAddExecute(Sender: TObject);
Var IdMH:INteger;
begin
  //��������
  if not CanDo('prAddDepart') then begin Showmessage('��� ����.'); exit; end;
  try
    fmAddMH:=tfmAddMH.Create(Application);
    fmAddMH.cxButton1.Enabled:=True;
    with fmAddMH do
    begin
      quShops.Active:=False; quShops.Active:=True; quShops.First;
      if quShops.RecordCount>0 then
      begin
        cxLookupComboBox2.EditValue:=quShopsID.AsInteger;
        Label4.Caption:='���������� ������ (����� ��������)';
        cxTextEdit1.Text:='����� �����';
        cxTextEdit2.Text:='';
        cxTextEdit3.Text:='';
        cxTextEdit4.Text:='';
        cxTextEdit5.Text:='����� �����';
        cxTextEdit6.Text:='';
        cxTextEdit7.Text:='';
        cxTextEdit8.Text:='';
        cxTextEdit9.Text:='';
        cxTextEdit10.Text:='';
        cxTextEdit11.Text:='';
        cxTextEdit12.Text:='';
        cxTextEdit13.Text:='';
        cxTextEdit14.Text:='';
        cxTextEdit15.Text:='';
        cxTextEdit16.Text:='';
        cxTextEdit17.Text:='';
        cxTextEdit18.Text:='';
        cxTextEdit19.Text:='';
        cxTextEdit20.Text:='';
        cxTextEdit21.Text:='';
        cxTextEdit22.Text:='';
        cxTextEdit23.Text:='';
        cxTextEdit24.Text:='';
        cxTextEdit25.Text:='';
        cxTextEdit26.Text:='';
        cxTextEdit27.Text:='';
        cxDateEdit1.Date:=date;
        cxDateEdit2.Date:=date;
        cxComboBox3.ItemIndex:=0;
      end else Showmessage('�������� �������.');
    end;
    fmAddMH.ShowModal;
    if fmAddMH.ModalResult=mrOk then
    begin //��������� �����
      IdMH:=fGetID(2);
      quDepId.Active:=False;
      quDepId.Parameters.ParamByName('IDMH').Value:=IdMH;
      quDepId.Active:=True;
      if quDepId.RecordCount=0 then
      begin
        quDepId.Append;
        quDepIdIDS.AsInteger:=fmAddMH.cxLookupComboBox2.EditValue;
        quDepIdID.AsInteger:=IdMH;
        quDepIdNAME.AsString:=fmAddMH.cxTextEdit1.Text;
        quDepIdFULLNAME.AsString:=fmAddMH.cxTextEdit5.Text;
        quDepIdISTATUS.AsInteger:=1;
        quDepIdIDORG.AsInteger:=0;
        quDepIdGLN.AsString:=fmAddMH.cxTextEdit2.Text;
        quDepIdISS.AsInteger:=fmAddMH.cxComboBox1.ItemIndex;
        quDepIdPRIOR.AsInteger:=fmAddMH.cxComboBox2.ItemIndex;
        quDepIdINN.AsString:=fmAddMH.cxTextEdit3.Text;
        quDepIdKPP.AsString:=fmAddMH.cxTextEdit4.Text;;
        quDepIdDIR1.AsString:=fmAddMH.cxTextEdit12.Text;;
        quDepIdDIR2.AsString:=fmAddMH.cxTextEdit13.Text;;
        quDepIdDIR3.AsString:=fmAddMH.cxTextEdit13.Text;;
        quDepIdGB1.AsString:=fmAddMH.cxTextEdit15.Text;;
        quDepIdGB2.AsString:=fmAddMH.cxTextEdit16.Text;;
        quDepIdGB3.AsString:=fmAddMH.cxTextEdit17.Text;;
        quDepIdCITY.AsString:=fmAddMH.cxTextEdit6.Text;;
        quDepIdSTREET.AsString:=fmAddMH.cxTextEdit7.Text;
        quDepIdHOUSE.AsString:=fmAddMH.cxTextEdit8.Text;
        quDepIdKORP.AsString:=fmAddMH.cxTextEdit9.Text;
        quDepIdPOSTINDEX.AsString:=fmAddMH.cxTextEdit10.Text;
        quDepIdPHONE.AsString:=fmAddMH.cxTextEdit11.Text;
        quDepIdSERLIC.AsString:=fmAddMH.cxTextEdit18.Text;
        quDepIdNUMLIC.AsString:=fmAddMH.cxTextEdit19.Text;
        quDepIdORGAN.AsString:=fmAddMH.cxTextEdit20.Text;
        quDepIdDATEB.AsDateTime:=fmAddMH.cxDateEdit1.Date;
        quDepIdDATEE.AsDateTime:=fmAddMH.cxDateEdit2.Date;
        quDepIdIP.AsInteger:=fmAddMH.cxComboBox3.ItemIndex;
        quDepIdIPREG.AsString:=fmAddMH.cxTextEdit21.Text;
        quDepIdNAMEOTPR.AsString:=fmAddMH.cxTextEdit22.Text;
        quDepIdADROPR.AsString:=fmAddMH.cxTextEdit23.Text;
        quDepIdRSCH.AsString:=fmAddMH.cxTextEdit24.Text;
        quDepIdKSCH.AsString:=fmAddMH.cxTextEdit25.Text;
        quDepIdBIK.AsString:=fmAddMH.cxTextEdit26.Text;
        quDepIdBANK.AsString:=fmAddMH.cxTextEdit27.Text;
        quDepId.Post;

        quDeps.Requery();
        quDeps.Locate('ID',IdMH,[]);

      end else showmessage('�������� �� ����. ���������� ����������.');
      quDepId.Active:=False;
    end;
  finally
    fmAddMH.quShops.Active:=False;
    fmAddMH.Release;
  end;
end;

procedure TfmDeparts.acExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmDeparts.acEditExecute(Sender: TObject);
Var IdMH:INteger;
begin
  //�������������
  if not CanDo('prEditDepart') then begin Showmessage('��� ����.'); exit; end;
  if quDeps.RecordCount>0 then
  begin
    IdMH:=quDepsID.AsInteger;
    try
      fmAddMH:=tfmAddMH.Create(Application);
      fmAddMH.Tag:=IdMH;
      fmAddMH.cxButton1.Enabled:=True;
      with fmAddMH do
      begin
        quShops.Active:=False; quShops.Active:=True; quShops.First;
        cxLookupComboBox2.EditValue:=quDepsIDS.AsInteger;
        Label4.Caption:='�������������� ������ (����� ��������)';
        cxTextEdit1.Text:=quDepsNAME.AsString;
        cxTextEdit2.Text:=quDepsGLN.AsString;
        cxTextEdit3.Text:=quDepsINN.AsString;
        cxTextEdit4.Text:=quDepsKPP.AsString;
        cxTextEdit5.Text:=quDepsFULLNAME.AsString;
        cxTextEdit6.Text:=quDepsCITY.AsString;
        cxTextEdit7.Text:=quDepsSTREET.AsString;
        cxTextEdit8.Text:=quDepsHOUSE.AsString;
        cxTextEdit9.Text:=quDepsKORP.AsString;
        cxTextEdit10.Text:=quDepsPOSTINDEX.AsString;
        cxTextEdit11.Text:=quDepsPHONE.AsString;
        cxTextEdit12.Text:=quDepsDIR1.AsString;
        cxTextEdit13.Text:=quDepsDIR2.AsString;
        cxTextEdit14.Text:=quDepsDIR3.AsString;
        cxTextEdit15.Text:=quDepsGB1.AsString;
        cxTextEdit16.Text:=quDepsGB2.AsString;
        cxTextEdit17.Text:=quDepsGB3.AsString;
        cxTextEdit18.Text:=quDepsSERLIC.AsString;
        cxTextEdit19.Text:=quDepsNUMLIC.AsString;
        cxTextEdit20.Text:=quDepsORGAN.AsString;
        cxTextEdit21.Text:=quDepsIPREG.AsString;
        cxTextEdit22.Text:=quDepsNAMEOTPR.AsString;
        cxTextEdit23.Text:=quDepsADROPR.AsString;
        cxTextEdit24.Text:=quDepsRSCH.AsString;
        cxTextEdit25.Text:=quDepsKSCH.AsString;
        cxTextEdit26.Text:=quDepsBIK.AsString;
        cxTextEdit27.Text:=quDepsBANK.AsString;

        cxDateEdit1.Date:=quDepsDATEB.AsDateTime;
        cxDateEdit2.Date:=quDepsDATEE.AsDateTime;
        cxComboBox1.ItemIndex:=quDepsISS.AsInteger;
        cxComboBox2.ItemIndex:=quDepsPRIOR.AsInteger;
        cxComboBox3.ItemIndex:=quDepsIP.AsInteger;
      end;
      fmAddMH.ShowModal;
      if fmAddMH.ModalResult=mrOk then
      begin //��������� �����
        quDepId.Active:=False;
        quDepId.Parameters.ParamByName('IDMH').Value:=IdMH;
        quDepId.Active:=True;
        if quDepId.RecordCount=1 then
        begin
          quDepId.Edit;
          quDepIdIDS.AsInteger:=fmAddMH.cxLookupComboBox2.EditValue;
//        quDepIdID.AsInteger:=IdMH;
          quDepIdNAME.AsString:=fmAddMH.cxTextEdit1.Text;
          quDepIdFULLNAME.AsString:=fmAddMH.cxTextEdit5.Text;
          quDepIdISTATUS.AsInteger:=1;
          quDepIdIDORG.AsInteger:=0;
          quDepIdGLN.AsString:=fmAddMH.cxTextEdit2.Text;
          quDepIdISS.AsInteger:=fmAddMH.cxComboBox1.ItemIndex;
          quDepIdPRIOR.AsInteger:=fmAddMH.cxComboBox2.ItemIndex;
          quDepIdINN.AsString:=fmAddMH.cxTextEdit3.Text;
          quDepIdKPP.AsString:=fmAddMH.cxTextEdit4.Text;;
          quDepIdDIR1.AsString:=fmAddMH.cxTextEdit12.Text;;
          quDepIdDIR2.AsString:=fmAddMH.cxTextEdit13.Text;;
          quDepIdDIR3.AsString:=fmAddMH.cxTextEdit13.Text;;
          quDepIdGB1.AsString:=fmAddMH.cxTextEdit15.Text;;
          quDepIdGB2.AsString:=fmAddMH.cxTextEdit16.Text;;
          quDepIdGB3.AsString:=fmAddMH.cxTextEdit17.Text;;
          quDepIdCITY.AsString:=fmAddMH.cxTextEdit6.Text;;
          quDepIdSTREET.AsString:=fmAddMH.cxTextEdit7.Text;
          quDepIdHOUSE.AsString:=fmAddMH.cxTextEdit8.Text;
          quDepIdKORP.AsString:=fmAddMH.cxTextEdit9.Text;
          quDepIdPOSTINDEX.AsString:=fmAddMH.cxTextEdit10.Text;
          quDepIdPHONE.AsString:=fmAddMH.cxTextEdit11.Text;
          quDepIdSERLIC.AsString:=fmAddMH.cxTextEdit18.Text;
          quDepIdNUMLIC.AsString:=fmAddMH.cxTextEdit19.Text;
          quDepIdORGAN.AsString:=fmAddMH.cxTextEdit20.Text;
          quDepIdDATEB.AsDateTime:=fmAddMH.cxDateEdit1.Date;
          quDepIdDATEE.AsDateTime:=fmAddMH.cxDateEdit2.Date;
          quDepIdIP.AsInteger:=fmAddMH.cxComboBox3.ItemIndex;
          quDepIdIPREG.AsString:=fmAddMH.cxTextEdit21.Text;
          quDepIdNAMEOTPR.AsString:=fmAddMH.cxTextEdit22.Text;
          quDepIdADROPR.AsString:=fmAddMH.cxTextEdit23.Text;
          quDepIdRSCH.AsString:=fmAddMH.cxTextEdit24.Text;
          quDepIdKSCH.AsString:=fmAddMH.cxTextEdit25.Text;
          quDepIdBIK.AsString:=fmAddMH.cxTextEdit26.Text;
          quDepIdBANK.AsString:=fmAddMH.cxTextEdit27.Text;
          quDepId.Post;

          quDeps.Requery();
          quDeps.Locate('ID',IdMH,[]);

        end else showmessage('�������� �� ����. ���������� ����������.');
        quDepId.Active:=False;
      end;
    finally
      fmAddMH.quShops.Active:=False;
      fmAddMH.Release;
    end;
  end else showmessage('�������� ������ ��� ��������������');
end;

procedure TfmDeparts.acViewExecute(Sender: TObject);
Var IdMH:INteger;
begin
  //��������
  if not CanDo('prViewDepart') then begin Showmessage('��� ����.'); exit; end;
  if quDeps.RecordCount>0 then
  begin
    IdMH:=quDepsID.AsInteger;
    try
      fmAddMH:=tfmAddMH.Create(Application);
      fmAddMH.Tag:=IdMH;
      fmAddMH.cxButton1.Enabled:=False;

      with fmAddMH do
      begin
        quShops.Active:=False; quShops.Active:=True; quShops.First;
        cxLookupComboBox2.EditValue:=quDepsIDS.AsInteger;
        Label4.Caption:='�������� ���������� ������ (����� ��������). ���������� ����������.';
        cxTextEdit1.Text:=quDepsNAME.AsString;
        cxTextEdit2.Text:=quDepsGLN.AsString;
        cxTextEdit3.Text:=quDepsINN.AsString;
        cxTextEdit4.Text:=quDepsKPP.AsString;
        cxTextEdit5.Text:=quDepsFULLNAME.AsString;
        cxTextEdit6.Text:=quDepsCITY.AsString;
        cxTextEdit7.Text:=quDepsSTREET.AsString;
        cxTextEdit8.Text:=quDepsHOUSE.AsString;
        cxTextEdit9.Text:=quDepsKORP.AsString;
        cxTextEdit10.Text:=quDepsPOSTINDEX.AsString;
        cxTextEdit11.Text:=quDepsPHONE.AsString;
        cxTextEdit12.Text:=quDepsDIR1.AsString;
        cxTextEdit13.Text:=quDepsDIR2.AsString;
        cxTextEdit14.Text:=quDepsDIR3.AsString;
        cxTextEdit15.Text:=quDepsGB1.AsString;
        cxTextEdit16.Text:=quDepsGB2.AsString;
        cxTextEdit17.Text:=quDepsGB3.AsString;
        cxTextEdit18.Text:=quDepsSERLIC.AsString;
        cxTextEdit19.Text:=quDepsNUMLIC.AsString;
        cxTextEdit20.Text:=quDepsORGAN.AsString;
        cxTextEdit21.Text:=quDepsIPREG.AsString;
        cxTextEdit22.Text:=quDepsNAMEOTPR.AsString;
        cxTextEdit23.Text:=quDepsADROPR.AsString;
        cxTextEdit24.Text:=quDepsRSCH.AsString;
        cxTextEdit25.Text:=quDepsKSCH.AsString;
        cxTextEdit26.Text:=quDepsBIK.AsString;
        cxTextEdit27.Text:=quDepsBANK.AsString;

        cxDateEdit1.Date:=quDepsDATEB.AsDateTime;
        cxDateEdit2.Date:=quDepsDATEE.AsDateTime;
        cxComboBox1.ItemIndex:=quDepsISS.AsInteger;
        cxComboBox2.ItemIndex:=quDepsPRIOR.AsInteger;
        cxComboBox3.ItemIndex:=quDepsIP.AsInteger;
      end;
      fmAddMH.ShowModal;
    finally
      fmAddMH.quShops.Active:=False;
      fmAddMH.Release;
    end;

  end else showmessage('�������� ������ ��� ���������');
end;

procedure TfmDeparts.ViewDepsDblClick(Sender: TObject);
begin
  acView.Execute;
end;

procedure TfmDeparts.acDelExecute(Sender: TObject);
Var IdMH:INteger;
begin
 //�������
 //��������
  if not CanDo('prDelDepart') then begin Showmessage('��� ����.'); exit; end;
  
  if quDeps.RecordCount>0 then
  begin
    IdMH:=quDepsID.AsInteger;
    if MessageDlg('�� ������������� ������ ������� ����� '+quDepsNAME.AsString+' ('+quDepsID.AsString+')', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      if MessageDlg('����������� ��������', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        try
          quDepId.Active:=False;
          quDepId.Parameters.ParamByName('IDMH').Value:=IdMH;
          quDepId.Active:=True;
          if quDepId.RecordCount=1 then quDepId.Delete;

          quDeps.Requery();
        except
          ShowMessage('������ ��������');
        end;
      end;
    end;
  end else showmessage('�������� ������ ��� ��������');
end;

end.
