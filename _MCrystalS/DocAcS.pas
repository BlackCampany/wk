unit DocAcS;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, cxGraphics, cxDropDownEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxCheckBox, StdCtrls,
  cxButtons, cxMaskEdit, cxCalendar, cxControls, cxContainer, cxEdit,
  cxTextEdit, ExtCtrls, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, DB, cxDBData, cxImageComboBox, Placemnt, ActnList,
  XPStyleActnCtrls, ActnMan, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid,
  dxmdaset, cxLabel, cxMemo, ComCtrls, FR_DSet, FR_DBSet, FR_Class;

type
  TfmDocsAcS = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    Label5: TLabel;
    Label12: TLabel;
    cxTextEdit1: TcxTextEdit;
    cxDateEdit1: TcxDateEdit;
    cxButton3: TcxButton;
    cxCheckBox1: TcxCheckBox;
    cxLookupComboBox1: TcxLookupComboBox;
    StatusBar1: TStatusBar;
    Panel2: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton8: TcxButton;
    Memo1: TcxMemo;
    Panel3: TPanel;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    cxLabel3: TcxLabel;
    cxLabel5: TcxLabel;
    cxLabel6: TcxLabel;
    taSpecAc: TdxMemData;
    taSpecAcNum: TIntegerField;
    taSpecAcCodeTovar: TIntegerField;
    taSpecAcName: TStringField;
    taSpecAcEdIzm: TSmallintField;
    dstaSpecAc: TDataSource;
    GridDoc3: TcxGrid;
    ViewDoc3: TcxGridDBTableView;
    LevelDoc3: TcxGridLevel;
    amAc: TActionManager;
    acAddPos: TAction;
    acAddList: TAction;
    acDelPos: TAction;
    acDelall: TAction;
    acSaveDoc: TAction;
    acReadBar: TAction;
    acMove: TAction;
    FormPlacementAc: TFormPlacement;
    taSpecAcQuant: TFloatField;
    taSpecAcPriceR: TFloatField;
    taSpecAcPriceN: TFloatField;
    taSpecAcSumR: TFloatField;
    taSpecAcSumN: TFloatField;
    taSpecAcSumD: TFloatField;
    ViewDoc3Num: TcxGridDBColumn;
    ViewDoc3CodeTovar: TcxGridDBColumn;
    ViewDoc3Name: TcxGridDBColumn;
    ViewDoc3EdIzm: TcxGridDBColumn;
    ViewDoc3Quant: TcxGridDBColumn;
    ViewDoc3PriceR: TcxGridDBColumn;
    ViewDoc3PriceN: TcxGridDBColumn;
    ViewDoc3SumR: TcxGridDBColumn;
    ViewDoc3SumN: TcxGridDBColumn;
    ViewDoc3SumD: TcxGridDBColumn;
    taSpecAcBarCode: TStringField;
    ViewDoc3BarCode: TcxGridDBColumn;
    Edit1: TEdit;
    taSpecAcQuantRemn: TFloatField;
    ViewDoc3Price: TcxGridDBColumn;
    ViewDoc3QuantRemn: TcxGridDBColumn;
    taSpecAcPrice: TFloatField;
    acRecalcRemn: TAction;
    frRepDAC: TfrReport;
    frtaSpecAc: TfrDBDataSet;
    cxLabel14: TcxLabel;
    acPrintCen: TAction;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cxButton2Click(Sender: TObject);
    procedure acAddPosExecute(Sender: TObject);
    procedure cxLabel2Click(Sender: TObject);
    procedure acDelallExecute(Sender: TObject);
    procedure acAddListExecute(Sender: TObject);
    procedure cxLabel5Click(Sender: TObject);
    procedure cxLabel1Click(Sender: TObject);
    procedure acDelPosExecute(Sender: TObject);
    procedure cxLabel6Click(Sender: TObject);
    procedure ViewDoc3EditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure ViewDoc3EditKeyPress(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
    procedure ViewDoc3Editing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure taSpecAcQuantChange(Sender: TField);
    procedure taSpecAcPriceNChange(Sender: TField);
    procedure acSaveDocExecute(Sender: TObject);
    procedure acReadBarExecute(Sender: TObject);
    procedure Edit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure acRecalcRemnExecute(Sender: TObject);
    procedure cxLabel3Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure acPrintCenExecute(Sender: TObject);
    procedure cxLabel14Click(Sender: TObject);
    procedure cxLookupComboBox1PropertiesChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

Function SaveDocsAc(var IdH:Integer):boolean;

var
  fmDocsAcS: TfmDocsAcS;
  iCol:INteger;

implementation

uses Un1, Cards, Dm, mFind, DocAcH, BarcodeNo, sumprops, SelParPrintCen,
  MainMC;

{$R *.dfm}

Function SaveDocsAc(var IdH:Integer):boolean;
Var rS:array[1..10] of Real;
    i:INteger;
    bErr:Boolean;
begin
  with dmMCS do
  with fmDocsAcS do
  begin
    Result:=False;
    if cxLookupComboBox1.EditValue<1 then exit;

    bErr:=False;
    quAcRec.Active:=False;
    IDH:=fmDocsAcS.Tag;
    try
      ViewDoc3.BeginUpdate;

      if IDH=0 then //����������
      begin
        IDH:=fGetId(14); //��������� ����� ����������

        quAcRec.Parameters.ParamByName('IDH').Value:=IDH;
        quAcRec.Active:=True;

        quAcRec.Append;
        quAcRecIDSKL.AsInteger:=cxLookupComboBox1.EditValue;
        quAcRecIDATEDOC.AsInteger:=Trunc(cxDateEdit1.Date);
        quAcRecID.AsInteger:=IDH;
        quAcRecDATEDOC.AsDateTime:=Trunc(cxDateEdit1.Date);
        quAcRecNUMDOC.AsString:=Trim(cxTextEdit1.Text);
        quAcRecSUM1.AsFloat:=0;
        quAcRecSUM2.AsFloat:=0;
        quAcRecIACTIVE.AsInteger:=1;
        quAcRec.Post;

        fmDocsAcS.Tag:=IDH;
      end else
      begin
        quAcRec.Parameters.ParamByName('IDH').Value:=IDH;
        quAcRec.Active:=True;
        try
          quAcRec.Edit;
          quAcRecIDSKL.AsInteger:=cxLookupComboBox1.EditValue;
          quAcRecIDATEDOC.AsInteger:=Trunc(cxDateEdit1.Date);
          quAcRecID.AsInteger:=IDH;
          quAcRecDATEDOC.AsDateTime:=Trunc(cxDateEdit1.Date);
          quAcRecNUMDOC.AsString:=Trim(cxTextEdit1.Text);
          quAcRecSUM1.AsFloat:=0;
          quAcRecSUM2.AsFloat:=0;
          quAcRecIACTIVE.AsInteger:=1;
          quAcRec.Post;
        except
        end;
      end;

      quSpecAcRec.Active:=False;
      quSpecAcRec.Parameters.ParamByName('IDH').Value:=IDH;
      quSpecAcRec.Active:=True;

      quSpecAcRec.First;
      while not quSpecAcRec.Eof do quSpecAcRec.Delete; //���� �������

      for i:=1 to 10 do rS[i]:=0;

      iCol:=0;

      taSpecAc.First;
      while not taSpecAc.Eof do
      begin

        taSpecAc.Edit;
        taSpecAcPriceN.AsFloat:=rv(taSpecAcPriceN.AsFloat);
        taSpecAcSumR.AsFloat:=rv(taSpecAcSumR.AsFloat);
        taSpecAcSumN.AsFloat:=rv(taSpecAcSumN.AsFloat);
        taSpecAc.Post;

        rS[1]:=rS[1]+taSpecAcSumR.AsFloat;
        rS[2]:=rS[2]+taSpecAcSumN.AsFloat;

        quSpecAcRec.Append;
        quSpecAcRecIDHEAD.AsInteger:=IDH;
        quSpecAcRecNUM.AsInteger:=taSpecAcNum.AsInteger;
        quSpecAcRecIDCARD.AsInteger:=taSpecAcCodeTovar.AsInteger;
        quSpecAcRecSBAR.AsString:=taSpecAcBarCode.AsString;
        quSpecAcRecQUANT.AsFloat:=taSpecAcQuant.AsFloat;
        quSpecAcRecPRICER.AsFloat:=taSpecAcPriceR.AsFloat;
        quSpecAcRecPRICEN.AsFloat:=taSpecAcPriceN.AsFloat;
        quSpecAcRecSUMR.AsFloat:=taSpecAcSumR.AsFloat;
        quSpecAcRecSUMN.AsFloat:=taSpecAcSumN.AsFloat;
        quSpecAcRec.Post;

        taSpecAc.Next;
      end;
      quSpecAcRec.Active:=False;

      quAcRec.Edit;
      quAcRecSUM1.AsFloat:=rS[1];
      quAcRecSUM2.AsFloat:=rS[2];
      quAcRec.Post;

      bErr:=True;
    finally
      ViewDoc3.EndUpdate;
      if bErr then Result:=True;
    end;
  end;
end;


procedure TfmDocsAcS.FormCreate(Sender: TObject);
begin
  FormPlacementAc.IniFileName:=sFormIni;
  FormPlacementAc.Active:=True;
  GridDoc3.Align:=alClient;
  ViewDoc3.RestoreFromIniFile(sGridIni);
end;

procedure TfmDocsAcS.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewDoc3.StoreToIniFile(sGridIni,False);
end;

procedure TfmDocsAcS.cxButton2Click(Sender: TObject);
begin
  close;
end;

procedure TfmDocsAcS.acAddPosExecute(Sender: TObject);
Var iMax:Integer;
begin
  iMax:=taSpecAc.RecordCount+1;

  if taSpecAc.Locate('CodeTovar',0,[])=False then
  begin
    taSpecAc.Append;
    taSpecAcCodeTovar.AsInteger:=0;
    taSpecAcName.AsString:='';
    taSpecAcEdIzm.AsInteger:=1;
    taSpecAcQuant.AsFloat:=0;
    taSpecAcPriceR.AsFloat:=0;
    taSpecAcPrice.AsFloat:=0;
    taSpecAcPriceN.AsFloat:=0;
    taSpecAcSumR.AsFloat:=0;
    taSpecAcSumN.AsFloat:=0;
    taSpecAcSumD.AsFloat:=0;
    taSpecAcBarCode.AsString:='';

    taSpecAcNum.AsInteger:=iMax;
    taSpecAc.Post;
  end;

  GridDoc3.SetFocus;

  ViewDoc3CodeTovar.Options.Editing:=True;
  ViewDoc3Name.Options.Editing:=True;

  ViewDoc3Name.Focused:=True;
  ViewDoc3.Controller.FocusRecord(ViewDoc3.DataController.FocusedRowIndex,True);
end;

procedure TfmDocsAcS.cxLabel2Click(Sender: TObject);
begin
  acDelPos.Execute;
end;

procedure TfmDocsAcS.acDelallExecute(Sender: TObject);
begin
  if taSpecAc.RecordCount>0 then
  begin
    if acSaveDoc.Enabled then
    begin
      if MessageDlg('�� ������������� ������ �������� ������������ �������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        ViewDoc3.BeginUpdate;
        taSpecAc.First; while not taSpecAc.Eof do taSpecAc.Delete;
        ViewDoc3.EndUpdate;
      end;
    end;
  end;
end;

procedure TfmDocsAcS.acAddListExecute(Sender: TObject);
begin
  iDirect:=4;
  if cxLookupComboBox1.EditValue<>fmMainMC.Label3.Tag then
  begin
    fmMainMC.Label3.Tag:=cxLookupComboBox1.EditValue;
    CurDep:=fmMainMC.Label3.Tag;
    fmMainMC.Label3.Caption:=cxLookupComboBox1.Text;
    try
      fmCards.CardsView.BeginUpdate;
      prRefreshCards(Integer(fmCards.CardsTree.Selected.Data),cxLookupComboBox1.EditValue,fmCards.cxCheckBox1.Checked,fmCards.cxCheckBox2.Checked,fmCards.cxCheckBox3.Checked);
    finally
      fmCards.CardsView.EndUpdate;
    end;
  end;
  fmCards.Show;
end;

procedure TfmDocsAcS.cxLabel5Click(Sender: TObject);
begin
  acAddPos.Execute;
end;

procedure TfmDocsAcS.cxLabel1Click(Sender: TObject);
begin
  acAddList.Execute;
end;

procedure TfmDocsAcS.acDelPosExecute(Sender: TObject);
begin
  //������� �������
  if acSaveDoc.Enabled then
    if taSpecAc.RecordCount>0 then
    begin
      taSpecAc.Delete;
    end;
end;

procedure TfmDocsAcS.cxLabel6Click(Sender: TObject);
begin
  if acSaveDoc.Enabled then acDelall.Execute;
end;

procedure TfmDocsAcS.ViewDoc3EditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
Var iCode:Integer;
    sName:String;
begin
  with dmMCS do
  begin
    if (Key=$0D) then
    begin
      if ViewDoc3.Controller.FocusedColumn.Name='ViewDoc3CodeTovar' then
      begin
        iCode:=VarAsType(AEdit.EditingValue, varInteger);

        quFCP.Active:=False;
        quFCP.SQL.Clear;
        quFCP.SQL.Add('SELECT * FROM [dbo].[CARDS] ca');
        quFCP.SQL.Add('left join [dbo].[CARDSPRICE] pr on pr.[GOODSID]=ca.[ID] and pr.[IDSKL]='+its(cxLookupComboBox1.EditValue));
        quFCP.SQL.Add('where ca.[ID]='+its(iCode));
//        quFCP.SQL.Add('where Upper(ca.[NAME]) like ''%'+AnsiUpperCase(s1)+'%''');
//        quFCP.SQL.Add('and [TTOVAR]=0');
        quFCP.Active:=True;

        if quFCP.RecordCount=1 then
        begin
          taSpecAc.Edit;
          taSpecAcCodeTovar.AsInteger:=iCode;
          taSpecAcName.AsString:=quFCPNAME.AsString;
          taSpecAcEdIzm.AsInteger:=quFCPEDIZM.AsInteger;
          taSpecAcPriceR.AsFloat:=quFCPPRICE.AsFloat;
          taSpecAcPrice.AsFloat:=quFCPPRICE.AsFloat;
          taSpecAcPriceN.AsFloat:=quFCPPRICE.AsFloat;
          taSpecAcQuant.AsFloat:=0;
          taSpecAcSumR.AsFloat:=0;
          taSpecAcSumN.AsFloat:=0;
          taSpecAcSumD.AsFloat:=0;
          taSpecAcBarCode.AsString:=quFCPBARCODE.AsString;
          taSpecAcNum.AsInteger:=taSpecAc.RecordCount+1;
          taSpecAcQuantRemn.AsFloat:=0;
          taSpecAc.Post;
        end;
        quFCP.Active:=False;
      end;
      if ViewDoc3.Controller.FocusedColumn.Name='ViewDoc3Name' then
      begin
        bAdd:=True;

        sName:=VarAsType(AEdit.EditingValue, varString);
        if Length(sName)>3 then
        begin
          fmFindC.ViewFind.BeginUpdate;
          dsquFindC.DataSet:=nil;
          try
            quFindC.Active:=False;
            quFindC.SQL.Clear;
            quFindC.SQL.Add('SELECT * FROM [dbo].[CARDS] ca');
            quFindC.SQL.Add('left join [dbo].[CARDSPRICE] pr on pr.[GOODSID]=ca.[ID] and pr.[IDSKL]='+its(cxLookupComboBox1.EditValue));
            quFindC.SQL.Add('where Upper(ca.[NAME]) like ''%'+AnsiUpperCase(sName)+'%''');
//            quFindC.SQL.Add('and [TTOVAR]=0');
            quFindC.Active:=True;
          finally
            dsquFindC.DataSet:=quFindC;
            fmFindC.ViewFind.EndUpdate;
          end;
          fmFindC.ShowModal;

          if fmFindC.ModalResult=mrOk then
          begin
            if quFindC.RecordCount>0 then
            begin
              taSpecAc.Edit;
              taSpecAcCodeTovar.AsInteger:=quFindCID.AsInteger;
              taSpecAcName.AsString:=quFindCNAME.AsString;
              taSpecAcEdIzm.AsInteger:=quFindCEDIZM.AsInteger;
              taSpecAcPriceR.AsFloat:=quFindCPRICE.AsFloat;
              taSpecAcPriceN.AsFloat:=quFindCPRICE.AsFloat;
              taSpecAcPrice.AsFloat:=quFindCPRICE.AsFloat;
              taSpecAcQuant.AsFloat:=0;
              taSpecAcSumR.AsFloat:=0;
              taSpecAcSumN.AsFloat:=0;
              taSpecAcSumD.AsFloat:=0;
              taSpecAcBarCode.AsString:=quFindCBARCODE.AsString;
              taSpecAcNum.AsInteger:=taSpecAc.RecordCount+1;
              taSpecAcQuantRemn.AsFloat:=0;
              taSpecAc.Post;

              AEdit.SelectAll;
            end;
          end;
          quFindC.Active:=False;
        end;

        bAdd:=False;
      end;

   //   ViewDoc3KolWithMest.Focused:=True;

    end else
      if ViewDoc3.Controller.FocusedColumn.Name='ViewDoc3CodeTovar' then
        if fTestKey(Key)=False then
          if taSpecAc.State in [dsEdit,dsInsert] then taSpecAc.Cancel;
  end;
end;

procedure TfmDocsAcS.ViewDoc3EditKeyPress(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
Var sName:String;
begin
  if bAdd then exit;
  with dmMCS do
  begin
    if ViewDoc3.Controller.FocusedColumn.Name='ViewDoc3Name' then
    begin
      sName:=VarAsType(AEdit.EditingValue, varString);
      if Length(sName)>3 then
      begin
        quFCP.Active:=False;
        quFCP.SQL.Clear;
        quFCP.SQL.Add('SELECT TOP 2 * FROM [dbo].[CARDS] ca');
        quFCP.SQL.Add('left join [dbo].[CARDSPRICE] pr on pr.[GOODSID]=ca.[ID] and pr.[IDSKL]='+its(cxLookupComboBox1.EditValue));
        quFCP.SQL.Add('where Upper(ca.[NAME]) like ''%'+AnsiUpperCase(sName)+'%''');
//        quFCP.SQL.Add('and [TTOVAR]=0');
        quFCP.Active:=True;

        if quFCP.RecordCount=1 then
        begin
          ViewDoc3.BeginUpdate;

          taSpecAc.Edit;
          taSpecAcCodeTovar.AsInteger:=quFCPID.AsInteger;
          taSpecAcName.AsString:=quFCPNAME.AsString;
          taSpecAcEdIzm.AsInteger:=quFCPEDIZM.AsInteger;
          taSpecAcPriceR.AsFloat:=quFCPPRICE.AsFloat;
          taSpecAcPrice.AsFloat:=quFCPPRICE.AsFloat;
          taSpecAcPriceN.AsFloat:=0;

          taSpecAcQuant.AsFloat:=0;

          taSpecAcSumR.AsFloat:=0;
          taSpecAcSumN.AsFloat:=0;
          taSpecAcSumD.AsFloat:=0;

          taSpecAcBarCode.AsString:=quFCPBARCODE.AsString;
          taSpecAcNum.AsInteger:=taSpecAc.RecordCount+1;
          taSpecAc.Post;

          ViewDoc3.EndUpdate;

          AEdit.SelectAll;
          ViewDoc3Name.Options.Editing:=False;
          ViewDoc3Name.Focused:=True;
          Key:=#0;
        end;
        quFCP.Active:=False;
      end;
    end;
  end;
end;

procedure TfmDocsAcS.ViewDoc3Editing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  iCol:=0;
  if ViewDoc3.Controller.FocusedColumn.Name='ViewDoc3Quant' then iCol:=1;
  if ViewDoc3.Controller.FocusedColumn.Name='ViewDoc3PriceN' then iCol:=2;
end;

procedure TfmDocsAcS.taSpecAcQuantChange(Sender: TField);
Var rQ:Real;
begin
  if iCol=1 then   //
  begin
    rQ:=taSpecAcQuant.AsFloat;
    taSpecAcSumR.AsFloat:=rv(rQ*taSpecAcPriceR.AsFloat);
    taSpecAcSumN.AsFloat:=rv(rQ*taSpecAcPriceN.AsFloat);
    taSpecAcSumD.AsFloat:=taSpecAcSumN.AsFloat-taSpecAcSumR.AsFloat;
  end;
end;

procedure TfmDocsAcS.taSpecAcPriceNChange(Sender: TField);
Var rQ:Real;
begin
  if iCol=2 then   //
  begin
    rQ:=taSpecAcQuant.AsFloat;
    taSpecAcSumR.AsFloat:=rv(rQ*taSpecAcPriceR.AsFloat);
    taSpecAcSumN.AsFloat:=rv(rQ*taSpecAcPriceN.AsFloat);
    taSpecAcSumD.AsFloat:=taSpecAcSumN.AsFloat-taSpecAcSumR.AsFloat;
  end;
end;

procedure TfmDocsAcS.acSaveDocExecute(Sender: TObject);
Var IDH:INteger;
begin
  Memo1.Clear;
  Memo1.Lines.Add('����� ... ���� ���������� ���������.'); delay(10);
  if SaveDocsAc(IdH) then
  begin
    with dmMCS do
    begin
      fmDocsAcH.ViewDocsAc.BeginUpdate;
      quAcH.Requery();
      quAcH.Locate('ID',IDH,[]);
      fmDocsAcH.ViewDocsAc.EndUpdate;
      fmDocsAcH.ViewDocsAc.Controller.FocusRecord(fmDocsAcH.ViewDocsAc.DataController.FocusedRowIndex,True);

      Memo1.Lines.Add('���������� ��.'); delay(10);
    end;
  end else begin Memo1.Lines.Add('������.'); delay(10); end;
end;

procedure TfmDocsAcS.acReadBarExecute(Sender: TObject);
begin
  Edit1.SetFocus;
  Edit1.SelectAll;
end;

procedure TfmDocsAcS.Edit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
Var sBar:String;
    IC:INteger;
begin
  if (Key=$0D) then
  begin

    if cxButton1.Enabled=False then exit;

    sBar:=Edit1.Text;
    if (sBar[1]='2')and((sBar[2]='1')or(sBar[2]='2')) then  sBar:=copy(sBar,1,7); //������� �����
    if length(sBar)>=12 then if pos('00002',sBar)=1 then sBar:=Copy(sBar,5,7);
    with dmMCS do
    begin
      if prFindBar(sBar,0,iC) then
      begin
        quFCP.Active:=False;
        quFCP.SQL.Clear;
        quFCP.SQL.Add('SELECT * FROM [dbo].[CARDS] ca');
        quFCP.SQL.Add('left join [dbo].[CARDSPRICE] pr on pr.[GOODSID]=ca.[ID] and pr.[IDSKL]='+its(cxLookupComboBox1.EditValue));
        quFCP.SQL.Add('where ca.[ID]='+its(iC));
//        quFCP.SQL.Add('and [TTOVAR]=0');
        quFCP.Active:=True;

        if quFCP.RecordCount>0 then
        begin
          if quFCPISTATUS.AsInteger>100 then
          begin
            Showmessage('�������� ����� ��������� � ��������������. ���������� ����������.');
          end else
          begin
            if taSpecAc.RecordCount>0 then
              if taSpecAc.Locate('CodeTovar',iC,[]) then
              begin
                ViewDoc3.Controller.FocusRecord(ViewDoc3.DataController.FocusedRowIndex,True);
                GridDoc3.SetFocus;
                exit;
              end;

            taSpecAc.Append;
            taSpecAcCodeTovar.AsInteger:=quFCPID.asinteger;
            taSpecAcName.AsString:=quFCPNAME.AsString;
            taSpecAcEdIzm.AsInteger:=quFCPEDIZM.AsInteger;
            taSpecAcPriceR.AsFloat:=quFCPPRICE.AsFloat;
            taSpecAcPrice.AsFloat:=quFCPPRICE.AsFloat;
            taSpecAcPriceN.AsFloat:=quFCPPRICE.AsFloat;
            taSpecAcQuant.AsFloat:=0;

            taSpecAcSumR.AsFloat:=0;
            taSpecAcSumN.AsFloat:=0;
            taSpecAcSumD.AsFloat:=0;

            taSpecAcBarCode.AsString:=quFCPBARCODE.AsString;
            taSpecAcNum.AsInteger:=taSpecAc.RecordCount+1;

            taSpecAcQuantRemn.AsFloat:=0;
            taSpecAc.Post;

            ViewDoc3.Controller.FocusRecord(ViewDoc3.DataController.FocusedRowIndex,True);
            GridDoc3.SetFocus;
          end;
        end;
        quFCP.Active:=False;
      end else
      begin
        fmBarMessage:=TfmBarMessage.Create(Application);
        fmBarMessage.ShowModal;
        fmBarMessage.Release;
      end;
    end;
  end;
end;

procedure TfmDocsAcS.cxButton1Click(Sender: TObject);
begin
  acSaveDoc.Execute;
end;

procedure TfmDocsAcS.FormShow(Sender: TObject);
begin
  iCol:=0;
  Memo1.Clear;
end;

procedure TfmDocsAcS.acRecalcRemnExecute(Sender: TObject);
Var rQ:Real;
begin
  //
  ViewDoc3.BeginUpdate;
  iCol:=1;
  taSpecAc.First;
  while not taSpecAc.Eof do
  begin

    rQ:=prFindRemnDepD(taSpecAcCodeTovar.AsInteger,cxLookupComboBox1.EditValue,Trunc(cxDateEdit1.date));

    taSpecAc.Edit;
    if acSaveDoc.Enabled then taSpecAcQuant.AsFloat:=rQ;
    taSpecAcQuantRemn.AsFloat:=rQ;
    taSpecAc.Post;

    taSpecAc.Next;
  end;
  iCol:=0;
  ViewDoc3.EndUpdate;
end;

procedure TfmDocsAcS.cxLabel3Click(Sender: TObject);
begin
  acRecalcRemn.Execute;
end;

procedure TfmDocsAcS.cxButton3Click(Sender: TObject);
Var vPP:TfrPrintPages;
    rSum1,rSum2,rSum3:Real;
begin
  try
    ViewDoc3.BeginUpdate;

    rSum1:=0; rSum2:=0; rSum3:=0;
    taSpecAc.First;
    while not taSpecAc.Eof do
    begin
      rSum1:=rSum1+rv(taSpecAcSumR.AsFloat);
      rSum2:=rSum2+rv(taSpecAcSumN.AsFloat);
      rSum3:=rSum3+rv(taSpecAcSumD.AsFloat);

      taSpecAc.Next;
    end;

    frRepDAC.LoadFromFile(CommonSet.PathReport + 'ActP.frf');

    frVariables.Variable['DocNum']:=cxTextEdit1.Text;
    frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date);
    frVariables.Variable['Depart']:=cxLookupComboBox1.Text;
    frVariables.Variable['SumOld']:=rSum1;
    frVariables.Variable['SumNew']:=rSum2;
    frVariables.Variable['SumNac']:=rSum3;
    frVariables.Variable['SSumOld']:=MoneyToString(abs(rSum1),True,False);
    frVariables.Variable['SSumNew']:=MoneyToString(abs(rSum2),True,False);
    frVariables.Variable['SSumNac']:=MoneyToString(abs(rSum3),True,False);

    frRepDAC.ReportName:='��� ����������.';
    frRepDAC.PrepareReport;
    vPP:=frAll;
    if cxCheckBox1.Checked then frRepDAC.ShowPreparedReport
    else frRepDAC.PrintPreparedReport('',1,False,vPP);
  finally
    ViewDoc3.EndUpdate;
  end;
end;


procedure TfmDocsAcS.acPrintCenExecute(Sender: TObject);
Var i,j,iNum:INteger;
    Rec:TcxCustomGridRecord;
    rPrice,rPriceC:Real;
begin
  //������ ��������
  with dmMCS do
  begin
    if cxLookupComboBox1.EditValue<1 then begin showmessage('�������� ����� ��������..'); exit; end;

    fmSelParCen.ShowModal;
    if fmSelParCen.ModalResult=mrOk then
    begin
      if (fmSelParCen.cxRadioGroup1.ItemIndex=1)or(fmSelParCen.cxRadioGroup1.ItemIndex=2) then ViewDoc3.Controller.SelectAll;

      try
        dstaCen.DataSet:=nil;
        CloseTe(taCen);
        for i:=0 to ViewDoc3.Controller.SelectedRecordCount-1 do
        begin
          Rec:=ViewDoc3.Controller.SelectedRecords[i];

          iNum:=0; rPrice:=0;
          for j:=0 to Rec.ValueCount-1 do
          begin
            if ViewDoc3.Columns[j].Name='ViewDoc3CodeTovar' then  begin iNum:=Rec.Values[j]; end;
            if ViewDoc3.Columns[j].Name='ViewDoc3PriceN' then  begin rPrice:=Rec.Values[j]; end;
            if (iNum>0)and(rPrice>0) then break;
          end;

          if iNum>0 then
          begin
            quFindC4.Active:=False;
            quFindC4.Parameters.ParamByName('IDEP').Value:=cxLookupComboBox1.EditValue;
            quFindC4.Parameters.ParamByName('IDC').Value:=iNum;
            quFindC4.Active:=True;
            if quFindC4.RecordCount>0 then
            begin
              rPriceC:=quFindC4PRICE.AsFloat;

              if (fmSelParCen.cxRadioGroup1.ItemIndex=2) then rPriceC:=0;

              if abs(rPrice-rPriceC)>0.001 then
              begin
                taCen.Append;
                taCenIdCard.AsInteger:=iNum;
                taCenFullName.AsString:=quFindC4FullName.AsString;
                taCenCountry.AsString:=quFindC4NameCu.AsString;
                taCenPrice1.AsFloat:=RV(rPrice);
                taCenPrice2.AsFloat:=0;
                taCenDiscount.AsFloat:=0;
                taCenBarCode.AsString:=quFindC4BARCODE.AsString;
                taCenEdIzm.AsInteger:=quFindC4EDIZM.AsInteger;
                if quFindC4EdIzm.AsInteger=2 then  taCenPluScale.AsString:=prFindPlu(iNum,cxLookupComboBox1.EditValue) else taCenPluScale.AsString:='';
                taCenReceipt.AsString:=quFindC4SOSTAV.AsString;
                taCenDepName.AsString:=fFullName(cxLookupComboBox1.EditValue);
                taCensDisc.AsString:=prSDisc(iNum,cxLookupComboBox1.EditValue);
                taCenScaleKey.AsInteger:=quFindC4VESBUTTON.AsInteger;
                taCensDate.AsString:=ds1(fmMainMC.cxDEdit1.Date);
//              taCenV02.AsInteger:=quFindC4V02.AsInteger;
//              taCenV12.AsInteger:=quFindC4V12.AsInteger;
//              taCenMaker.AsString:=quFindC4NAMEM.AsString;
                taCen.Post;
              end;
            end;

            quFindC4.Active:=False;
          end;
        end;

        if quLabs.Locate('ID',fmMainMC.cxLookupComboBox1.EditValue,[]) then
        begin
          if FileExists(CommonSet.PathReport+quLabsNameF.AsString) then
          begin
            RepCenn.LoadFromFile(CommonSet.PathReport+quLabsNameF.AsString);
            RepCenn.ReportName:='�������.';
            RepCenn.PrepareReport;
            RepCenn.ShowPreparedReport;
//        vPP:=frAll;
//        RepCenn.PrintPreparedReport('',1,False,vPP);
          end else showmessage('���� �� ������ : '+CommonSet.PathReport+quLabsNameF.AsString);

        end;
        CloseTe(taCen);
        dstaCen.DataSet:=taCen;
        fmMainMC.cxDEdit1.Date:=Date;
      finally
      end;

    end;
  end;
end;

procedure TfmDocsAcS.cxLabel14Click(Sender: TObject);
begin
  acPrintCen.Execute;
end;

procedure TfmDocsAcS.cxLookupComboBox1PropertiesChange(Sender: TObject);
begin
  if fmDocsAcS.Showing then
    if fmDocsAcS.Tag=0 then cxTextEdit1.Text:=fGetNumDoc(cxLookupComboBox1.EditValue,4);
end;

end.
