object dmPx: TdmPx
  OldCreateOrder = False
  Left = 1484
  Top = 476
  Height = 344
  Width = 432
  object taCard: TTable
    SessionName = 'Ses1'
    TableName = '_710OE74.DB'
    Left = 28
    Top = 24
    object taCardArticul: TStringField
      FieldName = 'Articul'
      Size = 30
    end
    object taCardName: TStringField
      FieldName = 'Name'
      Size = 80
      Transliterate = False
    end
    object taCardMesuriment: TStringField
      FieldName = 'Mesuriment'
      Size = 10
      Transliterate = False
    end
    object taCardMesPresision: TFloatField
      FieldName = 'MesPresision'
    end
    object taCardAdd1: TStringField
      FieldName = 'Add1'
    end
    object taCardAdd2: TStringField
      FieldName = 'Add2'
    end
    object taCardAdd3: TStringField
      FieldName = 'Add3'
    end
    object taCardAddNum1: TFloatField
      FieldName = 'AddNum1'
    end
    object taCardAddNum2: TFloatField
      FieldName = 'AddNum2'
    end
    object taCardAddNum3: TFloatField
      FieldName = 'AddNum3'
    end
    object taCardScale: TStringField
      FieldName = 'Scale'
      Size = 10
    end
    object taCardGroop1: TSmallintField
      FieldName = 'Groop1'
    end
    object taCardGroop2: TSmallintField
      FieldName = 'Groop2'
    end
    object taCardGroop3: TSmallintField
      FieldName = 'Groop3'
    end
    object taCardGroop4: TSmallintField
      FieldName = 'Groop4'
    end
    object taCardGroop5: TSmallintField
      FieldName = 'Groop5'
    end
    object taCardPriceRub: TCurrencyField
      FieldName = 'PriceRub'
    end
    object taCardPriceCur: TCurrencyField
      FieldName = 'PriceCur'
    end
    object taCardClientIndex: TSmallintField
      FieldName = 'ClientIndex'
    end
    object taCardCommentary: TStringField
      FieldName = 'Commentary'
      Size = 80
    end
    object taCardDeleted: TSmallintField
      FieldName = 'Deleted'
    end
    object taCardModDate: TDateField
      FieldName = 'ModDate'
    end
    object taCardModTime: TSmallintField
      FieldName = 'ModTime'
    end
    object taCardModPersonIndex: TSmallintField
      FieldName = 'ModPersonIndex'
    end
  end
  object taBar: TTable
    DatabaseName = 'DB_M'
    SessionName = 'Ses1'
    TableName = 'bars.DB'
    Left = 76
    Top = 24
    object taBarBarCode: TStringField
      FieldName = 'BarCode'
      Size = 15
    end
    object taBarCardArticul: TStringField
      FieldName = 'CardArticul'
      Size = 30
    end
    object taBarCardSize: TStringField
      FieldName = 'CardSize'
      Size = 10
    end
    object taBarQuantity: TFloatField
      FieldName = 'Quantity'
    end
  end
  object tbTax: TTable
    DatabaseName = 'C:\Projects\Casher\FB\Bin\Import\'
    SessionName = 'Ses1'
    TableName = '_710OE7F.DB'
    Left = 124
    Top = 24
    object tbTaxCardArticul: TStringField
      FieldName = 'CardArticul'
      Size = 30
    end
    object tbTaxTaxIndex: TSmallintField
      FieldName = 'TaxIndex'
    end
    object tbTaxTax: TFloatField
      FieldName = 'Tax'
    end
    object tbTaxTaxSumRub: TFloatField
      FieldName = 'TaxSumRub'
    end
    object tbTaxTaxSumCur: TFloatField
      FieldName = 'TaxSumCur'
    end
  end
  object taList: TTable
    AutoRefresh = True
    DatabaseName = 'C:\Projects\Casher\FB\Bin\Import\'
    SessionName = 'Ses1'
    TableName = 'CASH001.DB'
    Left = 352
    Top = 24
    object taListTName: TStringField
      FieldName = 'TName'
      Size = 8
    end
    object taListDataType: TSmallintField
      FieldName = 'DataType'
    end
    object taListOperation: TSmallintField
      FieldName = 'Operation'
    end
  end
  object taClass: TTable
    DatabaseName = 'C:\Projects\Casher\FB\Bin\Import\'
    SessionName = 'Ses1'
    TableName = '_710Q4AW.DB'
    Left = 172
    Top = 24
    object taClassGroop1: TSmallintField
      FieldName = 'Groop1'
    end
    object taClassGroop2: TSmallintField
      FieldName = 'Groop2'
    end
    object taClassGroop3: TSmallintField
      FieldName = 'Groop3'
    end
    object taClassGroop4: TSmallintField
      FieldName = 'Groop4'
    end
    object taClassGroop5: TSmallintField
      FieldName = 'Groop5'
    end
    object taClassName: TStringField
      FieldName = 'Name'
      Size = 80
      Transliterate = False
    end
    object taClassGr1: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'Gr1'
      Calculated = True
    end
    object taClassGr2: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'Gr2'
      Calculated = True
    end
    object taClassGr3: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'Gr3'
      Calculated = True
    end
    object taClassGr4: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'Gr4'
      Calculated = True
    end
    object taClassGr5: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'Gr5'
      Calculated = True
    end
  end
  object tbTaxT: TTable
    DatabaseName = 'C:\Projects\Casher\FB\Bin\Import\'
    SessionName = 'Ses1'
    TableName = '_710OWQ5.DB'
    Left = 220
    Top = 24
    object tbTaxTID: TSmallintField
      FieldName = 'ID'
    end
    object tbTaxTPriority: TSmallintField
      FieldName = 'Priority'
    end
    object tbTaxTName: TStringField
      FieldName = 'Name'
      Size = 40
      Transliterate = False
    end
  end
  object tbCashers: TTable
    DatabaseName = 'C:\Crystal\POS\1\'
    SessionName = 'Ses1'
    TableName = '_710OXC4.DB'
    Left = 28
    Top = 88
    object tbCashersIdent: TSmallintField
      FieldName = 'Ident'
    end
    object tbCashersName: TStringField
      FieldName = 'Name'
      Size = 40
      Transliterate = False
    end
    object tbCashersPassw: TStringField
      FieldName = 'Passw'
      Size = 15
      Transliterate = False
    end
    object tbCashersOfficialIndex: TSmallintField
      FieldName = 'OfficialIndex'
    end
  end
  object tbDeparts: TTable
    DatabaseName = 'C:\Crystal\POS\1\'
    SessionName = 'Ses1'
    TableName = '_710OY2Z.DB'
    Left = 76
    Top = 88
    object tbDepartsID: TSmallintField
      FieldName = 'ID'
    end
    object tbDepartsName: TStringField
      FieldName = 'Name'
      Size = 40
      Transliterate = False
    end
  end
  object tbDiscSum: TTable
    DatabaseName = 'C:\Crystal\POS\1\'
    SessionName = 'Ses1'
    TableName = '_716VZ1M.DB'
    Left = 135
    Top = 88
    object tbDiscSumPriceIndex: TSmallintField
      FieldName = 'PriceIndex'
    end
    object tbDiscSumTime: TSmallintField
      FieldName = 'Time'
    end
    object tbDiscSumSumma: TCurrencyField
      FieldName = 'Summa'
    end
    object tbDiscSumDiscount: TFloatField
      FieldName = 'Discount'
    end
  end
  object tbDiscQ: TTable
    DatabaseName = 'C:\Crystal\POS\1\'
    SessionName = 'Ses1'
    TableName = '_7160X8B.DB'
    Left = 188
    Top = 88
    object tbDiscQCardArticul: TStringField
      FieldName = 'CardArticul'
      Size = 30
    end
    object tbDiscQQuantity: TFloatField
      FieldName = 'Quantity'
    end
    object tbDiscQDiscount: TCurrencyField
      FieldName = 'Discount'
    end
  end
  object tbDiscCard: TTable
    DatabaseName = 'C:\Crystal\POS\1\'
    SessionName = 'Ses1'
    TableName = '_71695H6.DB'
    Left = 28
    Top = 152
    object tbDiscCardBarCode: TStringField
      FieldName = 'BarCode'
      Size = 22
    end
    object tbDiscCardName: TStringField
      FieldName = 'Name'
      Size = 40
      Transliterate = False
    end
    object tbDiscCardPercent: TFloatField
      FieldName = 'Percent'
    end
    object tbDiscCardClientIndex: TSmallintField
      FieldName = 'ClientIndex'
    end
  end
  object tbDiscStop: TTable
    DatabaseName = 'C:\Crystal\POS\1\'
    SessionName = 'Ses1'
    TableName = '_716KOXZ.DB'
    Left = 100
    Top = 152
    object tbDiscStopCodeStart: TStringField
      FieldName = 'CodeStart'
      Size = 22
      Transliterate = False
    end
    object tbDiscStopCodeEnd: TStringField
      FieldName = 'CodeEnd'
      Size = 22
      Transliterate = False
    end
  end
  object tbAdvDisc: TTable
    DatabaseName = 'C:\Crystal\POS\1\'
    SessionName = 'Ses1'
    TableName = '_716R1KK.DB'
    Left = 164
    Top = 152
    object tbAdvDiscGroop1: TSmallintField
      FieldName = 'Groop1'
    end
    object tbAdvDiscGroop2: TSmallintField
      FieldName = 'Groop2'
    end
    object tbAdvDiscGroop3: TSmallintField
      FieldName = 'Groop3'
    end
    object tbAdvDiscGroop4: TSmallintField
      FieldName = 'Groop4'
    end
    object tbAdvDiscGroop5: TSmallintField
      FieldName = 'Groop5'
    end
    object tbAdvDiscSumma: TCurrencyField
      FieldName = 'Summa'
    end
    object tbAdvDiscBarCode: TStringField
      FieldName = 'BarCode'
      Size = 22
      Transliterate = False
    end
    object tbAdvDiscPercent: TFloatField
      FieldName = 'Percent'
    end
  end
  object tbCredCard: TTable
    DatabaseName = 'C:\Crystal\POS\1\'
    SessionName = 'Ses1'
    TableName = '_716TVR7.DB'
    Left = 28
    Top = 208
    object tbCredCardID: TSmallintField
      FieldName = 'ID'
    end
    object tbCredCardName: TStringField
      FieldName = 'Name'
      Size = 80
      Transliterate = False
    end
    object tbCredCardClientIndex: TSmallintField
      FieldName = 'ClientIndex'
    end
    object tbCredCardLimitSum: TCurrencyField
      FieldName = 'LimitSum'
    end
    object tbCredCardCanReturn: TSmallintField
      FieldName = 'CanReturn'
    end
  end
  object tbCredPref: TTable
    DatabaseName = 'C:\Crystal\POS\1\'
    SessionName = 'Ses1'
    TableName = '_716TW7U.DB'
    Left = 100
    Top = 208
    object tbCredPrefPrefix: TStringField
      FieldName = 'Prefix'
      Size = 19
    end
    object tbCredPrefCredCardIndex: TSmallintField
      FieldName = 'CredCardIndex'
    end
  end
  object Session1: TSession
    Active = True
    NetFileDir = 'D:\KASSA\MCRYSTAL'
    SessionName = 'Ses1'
    Left = 281
    Top = 26
  end
  object taCS: TTable
    DatabaseName = 'C:\_MCrystal\Bin\TRF'
    SessionName = 'Ses1'
    TableName = 'CASHSAIL.DB'
    Left = 300
    Top = 88
    object taCSShopIndex: TSmallintField
      FieldName = 'ShopIndex'
    end
    object taCSCashNumber: TSmallintField
      FieldName = 'CashNumber'
    end
    object taCSZNumber: TSmallintField
      FieldName = 'ZNumber'
    end
    object taCSCheckNumber: TSmallintField
      FieldName = 'CheckNumber'
    end
    object taCSID: TSmallintField
      FieldName = 'ID'
    end
    object taCSDate: TDateField
      FieldName = 'Date'
    end
    object taCSTime: TSmallintField
      FieldName = 'Time'
    end
    object taCSCardArticul: TStringField
      FieldName = 'CardArticul'
      Size = 30
    end
    object taCSCardSize: TStringField
      FieldName = 'CardSize'
      Size = 10
    end
    object taCSPriceRub: TCurrencyField
      FieldName = 'PriceRub'
    end
    object taCSQuantity: TFloatField
      FieldName = 'Quantity'
    end
    object taCSPriceCur: TCurrencyField
      FieldName = 'PriceCur'
    end
    object taCSTotalRub: TCurrencyField
      FieldName = 'TotalRub'
    end
    object taCSTotalCur: TCurrencyField
      FieldName = 'TotalCur'
    end
    object taCSDepartment: TSmallintField
      FieldName = 'Department'
    end
    object taCSCasher: TSmallintField
      FieldName = 'Casher'
    end
    object taCSUsingIndex: TSmallintField
      FieldName = 'UsingIndex'
    end
    object taCSReplace: TSmallintField
      FieldName = 'Replace'
    end
    object taCSOperation: TSmallintField
      FieldName = 'Operation'
    end
    object taCSCredCardIndex: TSmallintField
      FieldName = 'CredCardIndex'
    end
    object taCSDiscCliIndex: TSmallintField
      FieldName = 'DiscCliIndex'
    end
    object taCSLinked: TSmallintField
      FieldName = 'Linked'
    end
  end
  object dstaCS: TDataSource
    DataSet = taCS
    Left = 300
    Top = 144
  end
  object taDC: TTable
    DatabaseName = 'POS3'
    SessionName = 'Ses1'
    IndexFieldNames = 'ShopIndex;CashNumber;ZNumber;CheckNumber;ID'
    MasterFields = 'ShopIndex;CashNumber;ZNumber;CheckNumber;ID'
    MasterSource = dstaCS
    TableName = 'CASHDISC.DB'
    Left = 352
    Top = 88
    object taDCShopIndex: TSmallintField
      FieldName = 'ShopIndex'
    end
    object taDCCashNumber: TSmallintField
      FieldName = 'CashNumber'
    end
    object taDCZNumber: TSmallintField
      FieldName = 'ZNumber'
    end
    object taDCCheckNumber: TSmallintField
      FieldName = 'CheckNumber'
    end
    object taDCID: TSmallintField
      FieldName = 'ID'
    end
    object taDCDiscountIndex: TSmallintField
      FieldName = 'DiscountIndex'
    end
    object taDCDiscountProc: TFloatField
      FieldName = 'DiscountProc'
    end
    object taDCDiscountRub: TCurrencyField
      FieldName = 'DiscountRub'
    end
    object taDCDiscountCur: TCurrencyField
      FieldName = 'DiscountCur'
    end
  end
  object taDStop: TTable
    DatabaseName = 'C:\_MCrystal\Bin\TRF'
    SessionName = 'Ses1'
    TableName = 'DSTOP.DB'
    Left = 352
    Top = 208
    object taDStopCardArticul: TStringField
      FieldName = 'CardArticul'
      Size = 30
    end
    object taDStopPercent: TFloatField
      FieldName = 'Percent'
    end
  end
  object taDCRD: TTable
    DatabaseName = 'C:\POS\1\Output'
    SessionName = 'Ses1'
    IndexFieldNames = 'ShopIndex;CashNumber;ZNumber;CheckNumber;CardType;CardNumber'
    TableName = 'CASHDCRD.DB'
    Left = 284
    Top = 204
    object taDCRDShopIndex: TSmallintField
      FieldName = 'ShopIndex'
    end
    object taDCRDCashNumber: TSmallintField
      FieldName = 'CashNumber'
    end
    object taDCRDZNumber: TSmallintField
      FieldName = 'ZNumber'
    end
    object taDCRDCheckNumber: TSmallintField
      FieldName = 'CheckNumber'
    end
    object taDCRDCardType: TSmallintField
      FieldName = 'CardType'
    end
    object taDCRDCardNumber: TStringField
      FieldName = 'CardNumber'
      Size = 22
    end
    object taDCRDDiscountRub: TCurrencyField
      FieldName = 'DiscountRub'
    end
    object taDCRDDiscountCur: TCurrencyField
      FieldName = 'DiscountCur'
    end
  end
end
