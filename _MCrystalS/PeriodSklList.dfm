object fmPerSklList: TfmPerSklList
  Left = 689
  Top = 479
  Width = 406
  Height = 249
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label4: TLabel
    Left = 16
    Top = 24
    Width = 50
    Height = 13
    Caption = #1055#1077#1088#1080#1086#1076' '#1089' '
  end
  object Label5: TLabel
    Left = 228
    Top = 24
    Width = 12
    Height = 13
    Caption = #1087#1086
  end
  object lbl2: TLabel
    Left = 8
    Top = 72
    Width = 82
    Height = 13
    Caption = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103
    Transparent = True
  end
  object Panel1: TPanel
    Left = 0
    Top = 144
    Width = 390
    Height = 67
    Align = alBottom
    BevelInner = bvLowered
    Color = 16765864
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 80
      Top = 16
      Width = 85
      Height = 33
      Caption = 'Ok'
      ModalResult = 1
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 236
      Top = 16
      Width = 81
      Height = 33
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      LookAndFeel.Kind = lfOffice11
    end
  end
  object cxDateEdit1: TcxDateEdit
    Left = 96
    Top = 20
    Style.ButtonStyle = btsOffice11
    TabOrder = 1
    Width = 121
  end
  object cxDateEdit2: TcxDateEdit
    Left = 248
    Top = 20
    Style.ButtonStyle = btsOffice11
    TabOrder = 2
    Width = 121
  end
  object cxCheckBox2: TcxCheckBox
    Left = 119
    Top = 68
    Caption = #1074#1099#1073#1088#1072#1090#1100' '#1074#1089#1077
    Properties.OnChange = cxCheckBox2PropertiesChange
    TabOrder = 3
    Transparent = True
    Width = 90
  end
  object cxCheckComboBox1: TcxCheckComboBox
    Left = 8
    Top = 96
    Properties.Delimiter = ','
    Properties.EmptySelectionText = #1053#1077' '#1074#1099#1073#1088#1072#1085#1086
    Properties.DropDownRows = 15
    Properties.EditValueFormat = cvfCaptions
    Properties.Items = <
      item
      end>
    Properties.ReadOnly = False
    Style.LookAndFeel.Kind = lfOffice11
    Style.LookAndFeel.NativeStyle = False
    Style.Shadow = False
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleDisabled.LookAndFeel.NativeStyle = False
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.NativeStyle = False
    StyleHot.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.NativeStyle = False
    TabOrder = 4
    Width = 369
  end
end
