unit Oplata;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, ADODB,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  StdCtrls, cxButtons, cxTextEdit, cxContainer, cxMaskEdit, cxDropDownEdit,
  cxCalc, ExtCtrls, cxCalendar;

type
  TfmOplata = class(TForm)
    Panel1: TPanel;
    cxCalcEdit1: TcxCalcEdit;
    cxTextEdit1: TcxTextEdit;
    Label1: TLabel;
    Label2: TLabel;
    cxButton1: TcxButton;
    ViewOpl: TcxGridDBTableView;
    LevelOpl: TcxGridLevel;
    GrigOpl: TcxGrid;
    quOplata: TADOQuery;
    dsquOplata: TDataSource;
    quOplataIDSKL: TIntegerField;
    quOplataIDDOC: TLargeintField;
    quOplataID: TLargeintField;
    quOplataITYPED: TIntegerField;
    quOplataIDATEPL: TIntegerField;
    quOplataDATEPL: TWideStringField;
    quOplataSUMPL: TFloatField;
    quOplataCOMMENT: TStringField;
    ViewOplDATEPL: TcxGridDBColumn;
    ViewOplSUMPL: TcxGridDBColumn;
    ViewOplCOMMENT: TcxGridDBColumn;
    cxDateEdit1: TcxDateEdit;
    Label3: TLabel;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    cxButton2: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FrSumD:Real;
    FiSkl:Integer;
    FiDDoc:Integer;
    FIDateDoc:Integer;
  end;

var
  fmOplata: TfmOplata;

implementation

uses Dm, Un1;

{$R *.dfm}

procedure TfmOplata.FormCreate(Sender: TObject);
begin
  GrigOpl.Align:=alClient;
end;

procedure TfmOplata.cxButton1Click(Sender: TObject);
var rSum:Real;
begin
  //�������� ������
  if not CanDo('prEditOplata') then begin ShowMessage('��� ����.'); exit; end;

  ViewOpl.BeginUpdate;
  rSum:=0;
  quOplata.First;
  while not quOplata.eof do
  begin
    rSum:=rSum+rv(quOplataSUMPL.AsFloat);
    quOplata.Next;
  end;

  if (rv(cxCalcEdit1.Value)<=(FrSumD-rSum)+0.01) and (cxCalcEdit1.Value>=0.01) then
  begin
    quOplata.Append;
    quOplataIDSKL.AsInteger:=FiSkl;
    quOplataIDDOC.AsInteger:=FiDDoc;
    quOplataITYPED.AsInteger:=1;
    quOplataIDATEPL.AsInteger:=trunc(cxDateEdit1.date);
    quOplataDATEPL.AsDateTime:=cxDateEdit1.date;
    quOplataSUMPL.AsFloat:=cxCalcEdit1.Value;
    quOplataCOMMENT.AsString:=cxTextEdit1.Text;
    quOplata.Post;
  end;

  ViewOpl.EndUpdate;

  cxCalcEdit1.Value:=0;
end;

procedure TfmOplata.N1Click(Sender: TObject);
begin
  if not CanDo('prEditOplata') then begin ShowMessage('��� ����.'); exit; end;

  if quOplata.RecordCount>0 then quOplata.Delete;
end;

procedure TfmOplata.cxButton2Click(Sender: TObject);
begin
  Close;
end;

end.
