unit DocSInv;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, Menus, cxLookAndFeelPainters, cxDropDownEdit,
  cxCheckBox, StdCtrls, cxButtons, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox, cxMaskEdit, cxCalendar, cxControls, cxContainer,
  cxEdit, cxTextEdit, ExtCtrls, Placemnt, cxLabel, cxMemo, ComCtrls,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, DB, cxDBData,
  cxImageComboBox, dxmdaset, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid,
  DBClient, XPStyleActnCtrls, ActnList, ActnMan, FR_DSet, FR_DBSet,
  FR_Class;

type
  TfmDocsInvS = class(TForm)
    FormPlacementInv: TFormPlacement;
    Panel1: TPanel;
    Label1: TLabel;
    Label5: TLabel;
    Label12: TLabel;
    Label6: TLabel;
    cxTextEdit1: TcxTextEdit;
    cxDateEdit1: TcxDateEdit;
    cxButton3: TcxButton;
    cxCheckBox1: TcxCheckBox;
    cxComboBox4: TcxComboBox;
    cxCheckBox3: TcxCheckBox;
    cxCheckBox4: TcxCheckBox;
    cxCheckBox5: TcxCheckBox;
    cxCheckBox6: TcxCheckBox;
    cxCheckBox7: TcxCheckBox;
    cxButton6: TcxButton;
    StatusBar1: TStatusBar;
    Panel2: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton8: TcxButton;
    Memo1: TcxMemo;
    Panel3: TPanel;
    Label7: TLabel;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    cxLabel3: TcxLabel;
    cxLabel5: TcxLabel;
    cxLabel6: TcxLabel;
    cxLabel4: TcxLabel;
    cxTextEdit2: TcxTextEdit;
    cxButton5: TcxButton;
    cxCheckBox2: TcxCheckBox;
    GridDoc5: TcxGrid;
    ViewDoc5: TcxGridDBTableView;
    ViewDoc5Num: TcxGridDBColumn;
    ViewDoc5CodeTovar: TcxGridDBColumn;
    ViewDoc5Name: TcxGridDBColumn;
    ViewDoc5EdIzm: TcxGridDBColumn;
    ViewDoc5Price: TcxGridDBColumn;
    ViewDoc5QuantR: TcxGridDBColumn;
    ViewDoc5SumR: TcxGridDBColumn;
    ViewDoc5QuantF: TcxGridDBColumn;
    ViewDoc5SumF: TcxGridDBColumn;
    ViewDoc5QuantD: TcxGridDBColumn;
    ViewDoc5SumD: TcxGridDBColumn;
    ViewDoc5IdGr: TcxGridDBColumn;
    ViewDoc5SGr: TcxGridDBColumn;
    ViewDoc5IdSGr: TcxGridDBColumn;
    ViewDoc5SSGr: TcxGridDBColumn;
    ViewDoc5IdSSgr: TcxGridDBColumn;
    ViewDoc5SSSGr: TcxGridDBColumn;
    ViewDoc5PriceSS: TcxGridDBColumn;
    ViewDoc5SumRSS: TcxGridDBColumn;
    ViewDoc5SumFSS: TcxGridDBColumn;
    ViewDoc5SumDSS: TcxGridDBColumn;
    ViewDoc5SumRTO: TcxGridDBColumn;
    ViewDoc5SumDTO: TcxGridDBColumn;
    ViewDoc5QuantC: TcxGridDBColumn;
    ViewDoc5SumC: TcxGridDBColumn;
    ViewDoc5SumRSC: TcxGridDBColumn;
    ViewDoc5SumRI: TcxGridDBColumn;
    ViewDoc5SumRSSI: TcxGridDBColumn;
    ViewDoc5iVid: TcxGridDBColumn;
    ViewDoc5sMaker: TcxGridDBColumn;
    ViewDoc5Vol: TcxGridDBColumn;
    ViewDoc5VolInv: TcxGridDBColumn;
    ViewDoc5SumDTOSS: TcxGridDBColumn;
    LevelDoc5: TcxGridLevel;
    taSpecInv: TdxMemData;
    dstaSpecInv: TDataSource;
    taSpecInvCodeTovar: TIntegerField;
    taSpecInvName: TStringField;
    taSpecInvEdIzm: TSmallintField;
    taSpecInvNDSProc: TFloatField;
    taSpecInvPrice: TFloatField;
    taSpecInvQuantR: TFloatField;
    taSpecInvQuantF: TFloatField;
    taSpecInvQuantD: TFloatField;
    taSpecInvQuantC: TFloatField;
    taSpecInvQuantRI: TFloatField;
    taSpecInvSumR: TFloatField;
    taSpecInvSumF: TFloatField;
    taSpecInvSumC: TFloatField;
    taSpecInvSumD: TFloatField;
    taSpecInvNum: TIntegerField;
    taSpecInvPriceSS: TFloatField;
    taSpecInvSumRSS: TFloatField;
    taSpecInvSumFSS: TFloatField;
    taSpecInvSumRSC: TFloatField;
    taSpecInvSumRSSI: TFloatField;
    taSpecInvSumDSS: TFloatField;
    taSpecInvSumRTO: TFloatField;
    taSpecInvSumDTO: TFloatField;
    taSpecInvBarCode: TStringField;
    taSpecInviVid: TIntegerField;
    taSpecInviMaker: TIntegerField;
    taSpecInvsMaker: TStringField;
    taSpecInvVol: TFloatField;
    taSpecInvVolInv: TFloatField;
    taSpecInvSumDTOSS: TFloatField;
    taSpecInvIdGr: TIntegerField;
    taSpecInvIdSGr: TIntegerField;
    taSpecInvIdSSGr: TIntegerField;
    taSpecInvSGr: TStringField;
    taSpecInvSSGr: TStringField;
    taSpecInvSSSGr: TStringField;
    taSpecInvCType: TSmallintField;
    ViewDoc5CType: TcxGridDBColumn;
    ViewDoc5QuantRI: TcxGridDBColumn;
    amInv: TActionManager;
    cxLookupComboBox1: TcxLookupComboBox;
    acAddPos: TAction;
    acAddList: TAction;
    acDelPos: TAction;
    acDelall: TAction;
    taSpecInvSumRI: TFloatField;
    acSaveDoc: TAction;
    Edit1: TEdit;
    acReadBar: TAction;
    taSp: TdxMemData;
    taSpNum: TIntegerField;
    taSpCodeTovar: TIntegerField;
    taSpName: TStringField;
    taSpEdIzm: TSmallintField;
    taSpNDSProc: TFloatField;
    taSpPrice: TFloatField;
    taSpPriceSS: TFloatField;
    taSpQuantR: TFloatField;
    taSpQuantF: TFloatField;
    taSpQuantC: TFloatField;
    taSpSumR: TFloatField;
    taSpSumF: TFloatField;
    taSpSumC: TFloatField;
    taSpSumRSS: TFloatField;
    taSpSumFSS: TFloatField;
    taSpSumRSC: TFloatField;
    taSpSumRTO: TFloatField;
    taSpSumDTO: TFloatField;
    taSpSumDTOSS: TFloatField;
    taSpBarCode: TStringField;
    taSpCType: TSmallintField;
    PopupMenu1: TPopupMenu;
    acEQIALQ: TAction;
    acMove: TAction;
    acAddAllMove: TAction;
    taSpecInvqOpSv: TFloatField;
    ViewDoc5qOpSv: TcxGridDBColumn;
    acGen: TAction;
    acTestPrice: TAction;
    ViewDoc5BarCode: TcxGridDBColumn;
    cxTextEdit3: TcxTextEdit;
    Label2: TLabel;
    taSpecInvPriceUch: TFloatField;
    ViewDoc5PriceUch: TcxGridDBColumn;
    taSpPriceUch: TFloatField;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure cxLabel5Click(Sender: TObject);
    procedure cxLabel1Click(Sender: TObject);
    procedure cxLabel2Click(Sender: TObject);
    procedure cxLabel6Click(Sender: TObject);
    procedure acDelallExecute(Sender: TObject);
    procedure acDelPosExecute(Sender: TObject);
    procedure acAddPosExecute(Sender: TObject);
    procedure taSpecInvCalcFields(DataSet: TDataSet);
    procedure acAddListExecute(Sender: TObject);
    procedure ViewDoc5EditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure ViewDoc5EditKeyPress(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
    procedure acSaveDocExecute(Sender: TObject);
    procedure ViewDoc5Editing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure taSpecInvQuantFChange(Sender: TField);
    procedure taSpecInvPriceChange(Sender: TField);
    procedure acReadBarExecute(Sender: TObject);
    procedure Edit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxLabel3Click(Sender: TObject);
    procedure acEQIALQExecute(Sender: TObject);
    procedure acMoveExecute(Sender: TObject);
    procedure acAddAllMoveExecute(Sender: TObject);
    procedure cxLookupComboBox1PropertiesChange(Sender: TObject);
    procedure acGenExecute(Sender: TObject);
    procedure cxLabel4Click(Sender: TObject);
    procedure acTestPriceExecute(Sender: TObject);
  private
    { Private declarations }
    procedure prButton(bSt:Boolean);
  public
    { Public declarations }
  end;

var
  fmDocsInvS: TfmDocsInvS;
  iCol:SmallINt;

implementation

uses Un1, Dm, Cards, MainMC, mFind, BarcodeNo, DocHInv, Move;

{$R *.dfm}

procedure TfmDocsInvS.FormCreate(Sender: TObject);
begin
  FormPlacementInv.IniFileName:=sFormIni;
  FormPlacementInv.Active:=True;
  GridDoc5.Align:=alClient;
  ViewDoc5.RestoreFromIniFile(sGridIni);
end;

procedure TfmDocsInvS.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if cxButton1.Enabled then
  begin
    if MessageDlg('����� ?',mtConfirmation, [mbYes, mbNo], 0)=mrYes then
    begin
      ViewDoc5.StoreToIniFile(sGridIni,False);
    end else
    begin
      Action:= caNone;
    end;
  end else
  begin
    ViewDoc5.StoreToIniFile(sGridIni,False);
  end;

end;

procedure TfmDocsInvS.FormShow(Sender: TObject);
begin
  iCol:=0;
  if cxTextEdit1.Text='' then
  begin
    cxTextEdit1.SetFocus;
    cxTextEdit1.SelectAll;
  end else GridDoc5.SetFocus;

  cxTextEdit2.Text:='';

  Memo1.Clear;
end;

procedure TfmDocsInvS.cxLabel5Click(Sender: TObject);
begin
  acAddPos.Execute;
end;

procedure TfmDocsInvS.cxLabel1Click(Sender: TObject);
begin
  acAddList.Execute;
end;

procedure TfmDocsInvS.cxLabel2Click(Sender: TObject);
begin
  acDelPos.Execute;
end;

procedure TfmDocsInvS.cxLabel6Click(Sender: TObject);
begin
  acDelall.Execute;
end;

procedure TfmDocsInvS.acDelallExecute(Sender: TObject);
begin
  if taSpecInv.RecordCount>0 then
  begin
    if MessageDlg('�� ������������� ������ �������� ������������ �������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      ViewDoc5.BeginUpdate;
      taSpecInv.First; while not taSpecInv.Eof do taSpecInv.Delete;
      ViewDoc5.EndUpdate;
    end;
  end;
end;

procedure TfmDocsInvS.acDelPosExecute(Sender: TObject);
begin
  //������� �������
  if taSpecInv.RecordCount>0 then
  begin
    taSpecInv.Delete;
  end;
end;

procedure TfmDocsInvS.acAddPosExecute(Sender: TObject);
Var iMax:Integer;
begin
  iMax:=taSpecInv.RecordCount+1;

  if taSpecInv.Locate('CodeTovar',0,[])=False then
  begin
    taSpecInv.Append;
    taSpecInvCodeTovar.AsInteger:=0;
    taSpecInvBarCode.AsString:='';
    taSpecInvName.AsString:='';
    taSpecInvEdIzm.AsInteger:=1;
    taSpecInvNDSProc.AsFloat:=0;
    taSpecInvPrice.AsFloat:=0;
    taSpecInvPriceSS.AsFloat:=0;
    taSpecInvQuantR.AsFloat:=0;
    taSpecInvQuantF.AsFloat:=0;
    taSpecInvQuantC.AsFloat:=0;
    taSpecInvqOpSv.AsFloat:=0;

    taSpecInvSumR.AsFloat:=0;
    taSpecInvSumF.AsFloat:=0;
    taSpecInvSumC.AsFloat:=0;

    taSpecInvSumRSS.AsFloat:=0;
    taSpecInvSumFSS.AsFloat:=0;
    taSpecInvSumRSC.AsFloat:=0;

    taSpecInvSumRTO.AsFloat:=0;
    taSpecInvSumDTO.AsFloat:=0;
    taSpecInvSumDTOSS.AsFloat:=0;

    taSpecInvBarCode.AsString:='';

    taSpecInvNum.AsInteger:=iMax;
    taSpecInv.Post;
  end;

  GridDoc5.SetFocus;

  ViewDoc5CodeTovar.Options.Editing:=True;
  ViewDoc5Name.Options.Editing:=True;

  ViewDoc5Name.Focused:=True;
  ViewDoc5.Controller.FocusRecord(ViewDoc5.DataController.FocusedRowIndex,True);
end;

procedure TfmDocsInvS.taSpecInvCalcFields(DataSet: TDataSet);
begin
  taSpecInvQuantRI.AsFloat:=taSpecInvQuantR.AsFloat+taSpecInvQuantC.AsFloat;
  taSpecInvQuantD.AsFloat:=taSpecInvQuantF.AsFloat-(taSpecInvQuantR.AsFloat+taSpecInvQuantC.AsFloat);
  taSpecInvSumRI.AsFloat:=rv(taSpecInvSumR.AsFloat+taSpecInvSumC.AsFloat);
  taSpecInvSumD.AsFloat:=rv(taSpecInvSumF.AsFloat-(taSpecInvSumR.AsFloat+taSpecInvSumC.AsFloat));
  taSpecInvSumRSSI.AsFloat:=rv(taSpecInvSumRSS.AsFloat+taSpecInvSumRSC.AsFloat);
  taSpecInvSumDSS.AsFloat:=rv(taSpecInvSumFSS.AsFloat-(taSpecInvSumRSS.AsFloat+taSpecInvSumRSC.AsFloat));
end;

procedure TfmDocsInvS.acAddListExecute(Sender: TObject);
begin
  iDirect:=5;
  if cxLookupComboBox1.EditValue<>fmMainMC.Label3.Tag then
  begin
    fmMainMC.Label3.Tag:=cxLookupComboBox1.EditValue;
    CurDep:=fmMainMC.Label3.Tag;
    fmMainMC.Label3.Caption:=cxLookupComboBox1.Text;
    try
      fmCards.CardsView.BeginUpdate;
      prRefreshCards(Integer(fmCards.CardsTree.Selected.Data),cxLookupComboBox1.EditValue,fmCards.cxCheckBox1.Checked,fmCards.cxCheckBox2.Checked,fmCards.cxCheckBox3.Checked);
    finally
      fmCards.CardsView.EndUpdate;
    end;
  end;
  fmCards.Show;
end;

procedure TfmDocsInvS.ViewDoc5EditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
Var iCode:Integer;
    sName:String;
    kNDS:Real;
    iSS:INteger;
begin
  with dmMCS do
  begin
    if (Key=$0D) then
    begin
      kNDS:=0;
      iSS:=fSS(cxLookupComboBox1.EditValue);
      if (iSS=2) then kNDS:=1;

      if ViewDoc5.Controller.FocusedColumn.Name='ViewDoc5CodeTovar' then
      begin
        iCode:=VarAsType(AEdit.EditingValue, varInteger);

        quFCP.Active:=False;
        quFCP.SQL.Clear;
        quFCP.SQL.Add('SELECT * FROM [dbo].[CARDS] ca');
        quFCP.SQL.Add('left join [dbo].[CARDSPRICE] pr on pr.[GOODSID]=ca.[ID] and pr.[IDSKL]='+its(cxLookupComboBox1.EditValue));
        quFCP.SQL.Add('where ca.[ID]='+its(iCode));
//        quFCP.SQL.Add('where Upper(ca.[NAME]) like ''%'+AnsiUpperCase(s1)+'%''');
//        quFCP.SQL.Add('and [TTOVAR]=0');
        quFCP.Active:=True;

        if quFCP.RecordCount=1 then
        begin
          taSpecInv.Edit;
          taSpecInvCodeTovar.AsInteger:=iCode;
          taSpecInvBarCode.AsString:=quFCPBarCode.AsString;
          taSpecInvName.AsString:=quFCPNAME.AsString;
          taSpecInvEdIzm.AsInteger:=quFCPEDIZM.AsInteger;
          taSpecInvNDSProc.AsFloat:=kNDS*quFCPNDS.AsFloat;
          taSpecInvPrice.AsFloat:=quFCPPRICE.AsFloat;
          taSpecInvPriceSS.AsFloat:=0;
          taSpecInvQuantR.AsFloat:=0;
          taSpecInvQuantF.AsFloat:=0;
          taSpecInvQuantC.AsFloat:=0;
          taSpecInvqOpSv.AsFloat:=0;

          taSpecInvSumR.AsFloat:=0;
          taSpecInvSumF.AsFloat:=0;
          taSpecInvSumC.AsFloat:=0;

          taSpecInvSumRSS.AsFloat:=0;
          taSpecInvSumFSS.AsFloat:=0;
          taSpecInvSumRSC.AsFloat:=0;

          taSpecInvSumRTO.AsFloat:=0;
          taSpecInvSumDTO.AsFloat:=0;
          taSpecInvSumDTOSS.AsFloat:=0;
          taSpecInvBarCode.AsString:=quFCPBARCODE.AsString;
          taSpecInvNum.AsInteger:=taSpecInv.RecordCount+1;
          taSpecInv.Post;
        end;
        quFCP.Active:=False;
      end;
      if ViewDoc5.Controller.FocusedColumn.Name='ViewDoc5Name' then
      begin
        bAdd:=True;

        sName:=VarAsType(AEdit.EditingValue, varString);
        if Length(sName)>3 then
        begin
          fmFindC.ViewFind.BeginUpdate;
          dsquFindC.DataSet:=nil;
          try
            quFindC.Active:=False;
            quFindC.SQL.Clear;
            quFindC.SQL.Add('SELECT * FROM [dbo].[CARDS] ca');
            quFindC.SQL.Add('left join [dbo].[CARDSPRICE] pr on pr.[GOODSID]=ca.[ID] and pr.[IDSKL]='+its(cxLookupComboBox1.EditValue));
            quFindC.SQL.Add('where Upper(ca.[NAME]) like ''%'+AnsiUpperCase(sName)+'%''');
//            quFindC.SQL.Add('and [TTOVAR]=0');
            quFindC.Active:=True;
          finally
            dsquFindC.DataSet:=quFindC;
            fmFindC.ViewFind.EndUpdate;
          end;
          fmFindC.ShowModal;

          if fmFindC.ModalResult=mrOk then
          begin
            if quFindC.RecordCount>0 then
            begin
              taSpecInv.Edit;
              taSpecInvCodeTovar.AsInteger:=quFindCID.AsInteger;
              taSpecInvBarCode.AsString:=quFindCBarCode.AsString;
              taSpecInvName.AsString:=quFindCNAME.AsString;
              taSpecInvEdIzm.AsInteger:=quFindCEDIZM.AsInteger;
              taSpecInvNDSProc.AsFloat:=kNDS*quFindCNDS.AsFloat;
              taSpecInvPrice.AsFloat:=quFindCPRICE.AsFloat;
              taSpecInvPriceSS.AsFloat:=0;
              taSpecInvQuantR.AsFloat:=0;
              taSpecInvQuantF.AsFloat:=0;
              taSpecInvQuantC.AsFloat:=0;
              taSpecInvqOpSv.AsFloat:=0;

              taSpecInvSumR.AsFloat:=0;
              taSpecInvSumF.AsFloat:=0;
              taSpecInvSumC.AsFloat:=0;

              taSpecInvSumRSS.AsFloat:=0;
              taSpecInvSumFSS.AsFloat:=0;
              taSpecInvSumRSC.AsFloat:=0;

              taSpecInvSumRTO.AsFloat:=0;
              taSpecInvSumDTO.AsFloat:=0;
              taSpecInvSumDTOSS.AsFloat:=0;
              taSpecInvBarCode.AsString:=quFindCBARCODE.AsString;
              taSpecInvNum.AsInteger:=taSpecInv.RecordCount+1;
              taSpecInv.Post;

              AEdit.SelectAll;
            end;
          end;
          quFindC.Active:=False;
        end;

        bAdd:=False;
      end;

   //   ViewDoc5KolWithMest.Focused:=True;

    end else
      if ViewDoc5.Controller.FocusedColumn.Name='ViewDoc5CodeTovar' then
        if fTestKey(Key)=False then
          if taSpecInv.State in [dsEdit,dsInsert] then taSpecInv.Cancel;
  end;
end;

procedure TfmDocsInvS.ViewDoc5EditKeyPress(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Char);
Var sName:String;
    kNDS:Real;
    iSS:INteger;
begin
  if bAdd then exit;
  with dmMCS do
  begin
    if ViewDoc5.Controller.FocusedColumn.Name='ViewDoc5Name' then
    begin
      sName:=VarAsType(AEdit.EditingValue, varString);
      if Length(sName)>3 then
      begin
        quFCP.Active:=False;
        quFCP.SQL.Clear;
        quFCP.SQL.Add('SELECT TOP 2 * FROM [dbo].[CARDS] ca');
        quFCP.SQL.Add('left join [dbo].[CARDSPRICE] pr on pr.[GOODSID]=ca.[ID] and pr.[IDSKL]='+its(cxLookupComboBox1.EditValue));
        quFCP.SQL.Add('where Upper(ca.[NAME]) like ''%'+AnsiUpperCase(sName)+'%''');
//        quFCP.SQL.Add('and [TTOVAR]=0');
        quFCP.Active:=True;

        if quFCP.RecordCount=1 then
        begin
          kNDS:=0;
          iSS:=fSS(cxLookupComboBox1.EditValue);
          if (iSS=2) then kNDS:=1;

          ViewDoc5.BeginUpdate;

          taSpecInv.Edit;
          taSpecInvCodeTovar.AsInteger:=quFCPID.AsInteger;
          taSpecInvBarCode.AsString:=quFCPBarCode.AsString;
          taSpecInvName.AsString:=quFCPNAME.AsString;
          taSpecInvEdIzm.AsInteger:=quFCPEDIZM.AsInteger;
          taSpecInvNDSProc.AsFloat:=kNDS*quFCPNDS.AsFloat;
          taSpecInvPrice.AsFloat:=quFCPPRICE.AsFloat;
          taSpecInvPriceSS.AsFloat:=0;
          taSpecInvQuantR.AsFloat:=0;
          taSpecInvQuantF.AsFloat:=0;
          taSpecInvQuantC.AsFloat:=0;
          taSpecInvqOpSv.AsFloat:=0;

          taSpecInvSumR.AsFloat:=0;
          taSpecInvSumF.AsFloat:=0;
          taSpecInvSumC.AsFloat:=0;

          taSpecInvSumRSS.AsFloat:=0;
          taSpecInvSumFSS.AsFloat:=0;
          taSpecInvSumRSC.AsFloat:=0;

          taSpecInvSumRTO.AsFloat:=0;
          taSpecInvSumDTO.AsFloat:=0;
          taSpecInvSumDTOSS.AsFloat:=0;
          taSpecInvBarCode.AsString:=quFCPBARCODE.AsString;
          taSpecInvNum.AsInteger:=taSpecInv.RecordCount+1;
          taSpecInv.Post;

          ViewDoc5.EndUpdate;

          AEdit.SelectAll;
          ViewDoc5Name.Options.Editing:=False;
          ViewDoc5Name.Focused:=True;
          Key:=#0;
        end;
        quFCP.Active:=False;
      end;
    end;
  end;
end;

procedure TfmDocsInvS.prButton(bSt:Boolean);
begin
  cxTextEdit1.Properties.ReadOnly:=not bSt;
  cxDateEdit1.Properties.ReadOnly:=not bSt;
  cxLookupComboBox1.Properties.ReadOnly:=not bSt;

  cxLabel1.Enabled:=bSt;
  cxLabel2.Enabled:=bSt;
  cxLabel3.Enabled:=bSt;
  cxLabel4.Enabled:=bSt;
  cxLabel5.Enabled:=bSt;
  cxLabel6.Enabled:=bSt;

  cxButton1.Enabled:=bSt;
  cxButton2.Enabled:=bSt;
  
  ViewDoc5.OptionsData.Editing:=bSt;
  ViewDoc5.OptionsData.Deleting:=bSt;
end;


procedure TfmDocsInvS.acSaveDocExecute(Sender: TObject);
Var IDH:INteger;
    iSS:INteger;
    rS:Array[1..20] of Real;
    i,iC:INteger;
begin
  //���������
  StatusBar1.Panels[0].Text:='';
  StatusBar1.Panels[1].Text:='';
  Memo1.Clear;

  if cxDateEdit1.Date<3000 then  cxDateEdit1.Date:=Date;

  if cxDateEdit1.Date<prOpenDate(cxLookupComboBox1.EditValue) then
  begin
    ShowMessage('������ ������.');
    Memo1.Lines.Add('������ ������.');
    exit;
  end;

  try
    prButton(False);
    ViewDoc5.BeginUpdate;

    Memo1.Lines.Add('����� ���� ���������� ���������.'); delay(10);
    Memo1.Lines.Add('  ����� ����� - '+its(taSpecInv.RecordCount)); delay(10);

    IDH:=fmDocsInvS.Tag;
    if taSpecInv.State in [dsEdit,dsInsert] then taSpecInv.Post;
    iSS:=fSS(cxLookupComboBox1.EditValue);
    with dmMCS do
    begin
      if IDH=0 then //����������
      begin
        IDH:=fGetId(12);
        fmDocsInvS.Tag:=IDH;

        quInvHRec.Active:=False;
        quInvHRec.Parameters.ParamByName('IDH').Value:=IDH;
        quInvHRec.Active:=True;

        quInvHRec.Append;
        quInvHRecIDSKL.AsInteger:=cxLookupComboBox1.EditValue;
        quInvHRecIDATEDOC.AsInteger:=Trunc(cxDateEdit1.Date);
        quInvHRecID.AsInteger:=IDH;
        quInvHRecDATEDOC.AsDateTime:=Trunc(cxDateEdit1.Date);
        quInvHRecNUMDOC.AsString:=Trim(cxTextEdit1.Text);

        quInvHRecSUMINR.AsFloat:=0;
        quInvHRecSUMINF.AsFloat:=0;
        quInvHRecSUMINRTO.AsFloat:=0;

        quInvHRecRSUMR.AsFloat:=0;
        quInvHRecRSUMF.AsFloat:=0;
        quInvHRecRSUMTO.AsFloat:=0;
        quInvHRecSUMTARR.AsFloat:=0;
        quInvHRecSUMTARF.AsFloat:=0;

        quInvHRecIACTIVE.AsInteger:=1;
        quInvHRecIDETAIL.AsInteger:=cxComboBox4.ItemIndex;
        quInvHRecCOMMENT.AsString:=Trim(cxTextEdit3.Text);
        quInvHRec.Post;
      end else
      begin
        quInvHRec.Active:=False;
        quInvHRec.Parameters.ParamByName('IDH').Value:=IDH;
        quInvHRec.Active:=True;

        quInvHRec.Edit;
        quInvHRecIDSKL.AsInteger:=cxLookupComboBox1.EditValue;
        quInvHRecIDATEDOC.AsInteger:=Trunc(cxDateEdit1.Date);
        quInvHRecID.AsInteger:=IDH;
        quInvHRecDATEDOC.AsDateTime:=Trunc(cxDateEdit1.Date);
        quInvHRecNUMDOC.AsString:=Trim(cxTextEdit1.Text);

        quInvHRecSUMINR.AsFloat:=0;
        quInvHRecSUMINF.AsFloat:=0;
        quInvHRecSUMINRTO.AsFloat:=0;

        quInvHRecRSUMR.AsFloat:=0;
        quInvHRecRSUMF.AsFloat:=0;
        quInvHRecRSUMTO.AsFloat:=0;
        quInvHRecSUMTARR.AsFloat:=0;
        quInvHRecSUMTARF.AsFloat:=0;

        quInvHRecIACTIVE.AsInteger:=1;
        quInvHRecIDETAIL.AsInteger:=cxComboBox4.ItemIndex;
        quInvHRecCOMMENT.AsString:=Trim(cxTextEdit3.Text);
        quInvHRec.Post;
      end;

      //������������
      //�������� ��� ������� �� ��������� ��������
      Memo1.Lines.Add('  - ������'); delay(10);

      for i:=1 to 20 do rS[i]:=0;

      dstaSpecInv.DataSet:=nil;
      CloseTe(taSp);
      taSpecInv.First;
      while not taSpecInv.Eof do
      begin
        taSpecInv.Edit;
        taSpecInvSumR.AsFloat:=rv(taSpecInvSumR.AsFloat);
        taSpecInvSumF.AsFloat:=rv(taSpecInvSumF.AsFloat);
        taSpecInvSumC.AsFloat:=rv(taSpecInvSumC.AsFloat);
        taSpecInvSumD.AsFloat:=rv(taSpecInvSumD.AsFloat);
        taSpecInvSumRSS.AsFloat:=rv(taSpecInvSumRSS.AsFloat);
        taSpecInvSumFSS.AsFloat:=rv(taSpecInvSumFSS.AsFloat);
        taSpecInvSumRSC.AsFloat:=rv(taSpecInvSumRSC.AsFloat);
        taSpecInvSumRSSI.AsFloat:=rv(taSpecInvSumRSSI.AsFloat);
        taSpecInvSumDSS.AsFloat:=rv(taSpecInvSumDSS.AsFloat);
        taSpecInvSumRTO.AsFloat:=rv(taSpecInvSumRTO.AsFloat);
        taSpecInv.Post;

        taSp.Append;
        taSpNum.AsInteger:=taSp.RecordCount+1;
        taSpCodeTovar.AsInteger:=taSpecInvCodeTovar.AsInteger;
        taSpPrice.AsFloat:=taSpecInvPrice.AsFloat;
        taSpPriceSS.AsFloat:=taSpecInvPriceSS.AsFloat;
        taSpQuantR.AsFloat:=taSpecInvQuantR.AsFloat;
        taSpQuantF.AsFloat:=taSpecInvQuantF.AsFloat;
        taSpQuantC.AsFloat:=taSpecInvQuantC.AsFloat;
        taSpSumR.AsFloat:=taSpecInvSumR.AsFloat;
        taSpSumF.AsFloat:=taSpecInvSumF.AsFloat;
        taSpSumC.AsFloat:=taSpecInvSumC.AsFloat;
        taSpSumRSS.AsFloat:=taSpecInvSumRSS.AsFloat;
        taSpSumFSS.AsFloat:=taSpecInvSumFSS.AsFloat;
        taSpSumRSC.AsFloat:=taSpecInvSumRSC.AsFloat;
        taSpSumRTO.AsFloat:=taSpecInvSumRTO.AsFloat;
        taSpSumDTO.AsFloat:=taSpecInvSumDTO.AsFloat;
        taSpSumDTOSS.AsFloat:=taSpecInvSumDTOSS.AsFloat;
        taSpBarCode.AsString:=taSpecInvBarCode.AsString;
        taSpPriceUch.AsFloat:=taSpecInvPriceUch.AsFloat;
        taSp.Post;

        rS[1]:=rS[1]+taSpecInvQuantR.AsFloat;
        rS[2]:=rS[2]+taSpecInvQuantF.AsFloat;
        rS[3]:=rS[3]+taSpecInvQuantC.AsFloat;
        rS[4]:=rS[4]+taSpecInvSumR.AsFloat;
        rS[5]:=rS[5]+taSpecInvSumF.AsFloat;
        rS[6]:=rS[6]+taSpecInvSumC.AsFloat;
        rS[7]:=rS[7]+taSpecInvSumRSS.AsFloat;
        rS[8]:=rS[8]+taSpecInvSumFSS.AsFloat;
        rS[9]:=rS[9]+taSpecInvSumRSC.AsFloat;
        rS[10]:=rS[10]+taSpecInvSumRTO.AsFloat;
        rS[11]:=rS[11]+taSpecInvSumDTO.AsFloat;
        rS[12]:=rS[12]+taSpecInvSumDTOSS.AsFloat;

        taSpecInv.Next;
      end;
      dstaSpecInv.DataSet:=taSpecInv;

      Memo1.Lines.Add('  - ���������'); delay(10);

      quSpecInvRec.Active:=False;
      quSpecInvRec.Parameters.ParamByName('IDH').Value:=IDH;
      quSpecInvRec.Active:=True;

      iC:=0;

      quSpecInvRec.First;
      while not quSpecInvRec.Eof do
      begin
        if taSp.Locate('CodeTovar',quSpecInvRecIDCARD.AsInteger,[]) then
        begin
          quSpecInvRec.Edit;
          quSpecInvRecIDHEAD.AsInteger:=IDH;
          quSpecInvRecNUM.AsInteger:=taSpNum.AsInteger;
          quSpecInvRecIDCARD.AsInteger:=taSpCodeTovar.AsInteger;
          quSpecInvRecSBAR.AsString:=taSpBarCode.AsString;
          quSpecInvRecQUANTR.AsFloat:=taSpQuantR.AsFloat;
          quSpecInvRecQUANTF.AsFloat:=taSpQuantF.AsFloat;
          quSpecInvRecPRICER.AsFloat:=taSpPrice.AsFloat;
          quSpecInvRecSUMR.AsFloat:=taSpSumR.AsFloat;
          quSpecInvRecSUMIN0.AsFloat:=taSpSumRSS.AsFloat;
          quSpecInvRecSUMIN.AsFloat:=taSpSumRSS.AsFloat;
          quSpecInvRecSUMRF.AsFloat:=taSpSumF.AsFloat;
          quSpecInvRecSUMIN0F.AsFloat:=taSpSumFSS.AsFloat;
          quSpecInvRecSUMINF.AsFloat:=taSpSumFSS.AsFloat;
          quSpecInvRecQUANTC.AsFloat:=taSpQuantC.AsFloat;
          quSpecInvRecSUMC.AsFloat:=taSpSumC.AsFloat;
          quSpecInvRecSUMRSC.AsFloat:=taSpSumRSC.AsFloat;
          quSpecInvRecSumRTO.AsFloat:=taSpSumRTO.AsFloat;
          quSpecInvRecPRICEUCH.AsFloat:=taSpPriceUch.AsFloat;
          quSpecInvRec.Post;

          taSp.Delete;
          quSpecInvRec.Next;
        end else quSpecInvRec.Delete;

        inc(iC);
        if iC mod 100 =0 then
        begin
          StatusBar1.Panels[0].Text:=its(iC);
          Memo1.Lines.Add('    .. '+its(iC)); delay(10);
        end;
      end;
      Memo1.Lines.Add('    .. '+its(iC)); delay(10);

      Memo1.Lines.Add('  - ����������'); delay(10);

      iC:=0;
      taSp.First;
      while not taSp.Eof do
      begin
        quSpecInvRec.Append;
        quSpecInvRecIDHEAD.AsInteger:=IDH;
        quSpecInvRecNUM.AsInteger:=taSpNum.AsInteger;
        quSpecInvRecIDCARD.AsInteger:=taSpCodeTovar.AsInteger;
        quSpecInvRecSBAR.AsString:=taSpBarCode.AsString;
        quSpecInvRecQUANTR.AsFloat:=taSpQuantR.AsFloat;
        quSpecInvRecQUANTF.AsFloat:=taSpQuantF.AsFloat;
        quSpecInvRecPRICER.AsFloat:=taSpPrice.AsFloat;
        quSpecInvRecSUMR.AsFloat:=taSpSumR.AsFloat;
        quSpecInvRecSUMIN0.AsFloat:=taSpSumRSS.AsFloat;
        quSpecInvRecSUMIN.AsFloat:=taSpSumRSS.AsFloat;
        quSpecInvRecSUMRF.AsFloat:=taSpSumF.AsFloat;
        quSpecInvRecSUMIN0F.AsFloat:=taSpSumFSS.AsFloat;
        quSpecInvRecSUMINF.AsFloat:=taSpSumFSS.AsFloat;
        quSpecInvRecQUANTC.AsFloat:=taSpQuantC.AsFloat;
        quSpecInvRecSUMC.AsFloat:=taSpSumC.AsFloat;
        quSpecInvRecSUMRSC.AsFloat:=taSpSumRSC.AsFloat;
        quSpecInvRecSumRTO.AsFloat:=taSpSumRTO.AsFloat;
        quSpecInvRecPRICEUCH.AsFloat:=taSpPriceUch.AsFloat;
        quSpecInvRec.Post;

        taSp.Delete;

        inc(iC);
        if iC mod 100 =0 then
        begin
          StatusBar1.Panels[0].Text:=its(iC);
          Memo1.Lines.Add('    .. '+its(iC)); delay(10);
        end;
      end;
      Memo1.Lines.Add('    .. '+its(iC)); delay(10);

      quSpecInvRec.Active:=False;

      quInvHRec.Edit;
      if iSS=1 then
      begin
        quInvHRecSUMINR.AsFloat:=rS[7];
        quInvHRecSUMINF.AsFloat:=rS[8];
        quInvHRecSUMINRTO.AsFloat:=rS[10];

        quInvHRecRSUMR.AsFloat:=rS[4];
        quInvHRecRSUMF.AsFloat:=rS[5];
      end;

      if iSS=2 then
      begin
        quInvHRecRSUMR.AsFloat:=rS[4];
        quInvHRecRSUMF.AsFloat:=rS[5];
      end;

      quInvHRecRSUMTO.AsFloat:=0;
      quInvHRecSUMTARR.AsFloat:=0;
      quInvHRecSUMTARF.AsFloat:=0;

      quInvHRecIACTIVE.AsInteger:=1;
      quInvHRecIDETAIL.AsInteger:=cxComboBox4.ItemIndex;
      quInvHRec.Post;

      fmDocsInvH.ViewDocsInv.BeginUpdate;
      quTTNInv.Requery();
      quTTNInv.Locate('ID',IDH,[]);
      fmDocsInvH.ViewDocsInv.EndUpdate;
      fmDocsInvH.ViewDocsInv.Controller.FocusRecord(fmDocsInvH.ViewDocsInv.DataController.FocusedRowIndex,True);
    end;
  finally
    ViewDoc5.EndUpdate;
    prButton(True);
  end;

  Memo1.Lines.Add('������� ��������.'); delay(10);
end;

procedure TfmDocsInvS.ViewDoc5Editing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  iCol:=0;
  if ViewDoc5.Controller.FocusedColumn.Name='ViewDoc5QuantF' then iCol:=1;
  if ViewDoc5.Controller.FocusedColumn.Name='ViewDoc5Price' then iCol:=2;
end;

procedure TfmDocsInvS.cxButton2Click(Sender: TObject);
begin
  Close;
end;

procedure TfmDocsInvS.cxButton5Click(Sender: TObject);
Var iCode:Integer;
begin
  try
    iCode:=StrToIntDef(cxTextEdit2.Text,0);
    if iCode>0 then
    begin
      taSpecInv.Locate('CodeTovar',iCode,[]);
      ViewDoc5.Controller.FocusRecord(ViewDoc5.DataController.FocusedRowIndex,True);
    end;
  except
  end;
end;

procedure TfmDocsInvS.cxButton1Click(Sender: TObject);
begin
  if acSaveDoc.Enabled then acSaveDoc.Execute;
end;

procedure TfmDocsInvS.taSpecInvQuantFChange(Sender: TField);
Var rQ:Real;
begin
  if iCol=1 then   //
  begin
    rQ:=taSpecInvQuantF.AsFloat;
    taSpecInvSumF.AsFloat:=rv(rQ*taSpecInvPrice.AsFloat);
  end;
end;

procedure TfmDocsInvS.taSpecInvPriceChange(Sender: TField);
Var rQ:Real;
begin
  if iCol=2 then   //
  begin
    rQ:=taSpecInvQuantF.AsFloat;
    taSpecInvSumF.AsFloat:=rv(rQ*taSpecInvPrice.AsFloat);
  end;
end;

procedure TfmDocsInvS.acReadBarExecute(Sender: TObject);
begin
  Edit1.SetFocus;
  Edit1.SelectAll;
end;

procedure TfmDocsInvS.Edit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
Var sBar:String;
    IC,kNDS:INteger;
    iSS:INteger;
begin
  if (Key=$0D) then
  begin

    if cxButton1.Enabled=False then exit;

    sBar:=Edit1.Text;
    if (sBar[1]='2')and((sBar[2]='1')or(sBar[2]='2')) then  sBar:=copy(sBar,1,7); //������� �����
    if length(sBar)>=12 then if pos('00002',sBar)=1 then sBar:=Copy(sBar,5,7);
    with dmMCS do
    begin
      if prFindBar(sBar,0,iC) then
      begin
        quFCP.Active:=False;
        quFCP.SQL.Clear;
        quFCP.SQL.Add('SELECT * FROM [dbo].[CARDS] ca');
        quFCP.SQL.Add('left join [dbo].[CARDSPRICE] pr on pr.[GOODSID]=ca.[ID] and pr.[IDSKL]='+its(cxLookupComboBox1.EditValue));
        quFCP.SQL.Add('where ca.[ID]='+its(iC));
//        quFCP.SQL.Add('and [TTOVAR]=0');
        quFCP.Active:=True;

        if quFCP.RecordCount>0 then
        begin
          if quFCPISTATUS.AsInteger>100 then
          begin
            Showmessage('�������� ����� ��������� � ��������������. ���������� ����������.');
          end else
          begin
            if taSpecInv.RecordCount>0 then
              if taSpecInv.Locate('CodeTovar',iC,[]) then
              begin
                ViewDoc5.Controller.FocusRecord(ViewDoc5.DataController.FocusedRowIndex,True);
                GridDoc5.SetFocus;
                exit;
              end;

            kNDS:=0;
            iSS:=fSS(cxLookupComboBox1.EditValue);
            if (iSS=2) then kNDS:=1;

            taSpecInv.Append;
            taSpecInvCodeTovar.AsInteger:=quFCPID.asinteger;
            taSpecInvName.AsString:=quFCPNAME.AsString;
            taSpecInvEdIzm.AsInteger:=quFCPEDIZM.AsInteger;
            taSpecInvNDSProc.AsFloat:=kNDS*quFCPNDS.AsFloat;
            taSpecInvPrice.AsFloat:=quFCPPRICE.AsFloat;
            taSpecInvPriceSS.AsFloat:=0;
            taSpecInvQuantR.AsFloat:=0;
            taSpecInvQuantF.AsFloat:=0;
            taSpecInvQuantC.AsFloat:=0;
            taSpecInvqOpSv.AsFloat:=0;

            taSpecInvSumR.AsFloat:=0;
            taSpecInvSumF.AsFloat:=0;
            taSpecInvSumC.AsFloat:=0;

            taSpecInvSumRSS.AsFloat:=0;
            taSpecInvSumFSS.AsFloat:=0;
            taSpecInvSumRSC.AsFloat:=0;

            taSpecInvSumRTO.AsFloat:=0;
            taSpecInvSumDTO.AsFloat:=0;
            taSpecInvSumDTOSS.AsFloat:=0;
            taSpecInvBarCode.AsString:=quFCPBARCODE.AsString;
            taSpecInvNum.AsInteger:=taSpecInv.RecordCount+1;

            taSpecInv.Post;

            ViewDoc5.Controller.FocusRecord(ViewDoc5.DataController.FocusedRowIndex,True);
            GridDoc5.SetFocus;
          end;
        end;
        quFCP.Active:=False;
      end else
      begin
        fmBarMessage:=TfmBarMessage.Create(Application);
        fmBarMessage.ShowModal;
        fmBarMessage.Release;
      end;
    end;
  end;
end;

procedure TfmDocsInvS.cxLabel3Click(Sender: TObject);
Var iDep:INteger;
    rPriceM:Real;
    rRemn:Real;
    iC:Integer;
    rOpSv:REal;
begin
  //����������� �������
  iCol:=0;
//    Memo1.Clear;
  Memo1.Lines.Add('����� ... ���� ������ ��������.'); delay(10);
  Memo1.Lines.Add('  - ����� '+its(taSpecInv.RecordCount)); delay(10);

  try
    prButton(False);
    ViewDoc5.BeginUpdate;
    dstaSpecInv.DataSet:=nil;
    with dmMCS do
    begin
      iDep:=0;
      if cxLookupComboBox1.EditValue>0 then iDep:=cxLookupComboBox1.EditValue;
      if iDep>0 then
      begin
        iC:=0;
        taSpecInv.First;
        while not taSpecInv.Eof do
        begin
          rOpSv:=0;
          rPriceM:=taSpecInvPrice.AsFloat;
          if rPriceM=0 then rPriceM:=prFindPrice(taSpecInvCodeTovar.AsInteger,iDep);

          if (taSpecInvCodeTovar.AsInteger>0) then
          begin
            rRemn:=prFindRemnDepD(taSpecInvCodeTovar.AsInteger,iDep,Trunc(cxDateEdit1.Date));
            rOpSv:=prFindOPSVDep(taSpecInvCodeTovar.AsInteger,iDep);
          end else rRemn:=0;

//          rQuantC:=0;
//          rSumC:=0;
//          rSumRSC:=0;

          taSpecInv.Edit;
          taSpecInvPrice.AsFloat:=rPriceM;
          taSpecInvQuantR.AsFloat:=rRemn;
          taSpecInvSumR.AsFloat:=rv(rPriceM*rRemn);
          taSpecInvqOpSv.AsFloat:=rOpSv;
          taSpecInv.Post;

          taSpecInv.Next;

          inc(iC);
          if iC mod 100 =0 then
          begin
            StatusBar1.Panels[0].Text:=its(iC);
            Memo1.Lines.Add('    .. '+its(iC)); delay(10);
          end;
        end;
        Memo1.Lines.Add('    .. '+its(iC)); delay(10);
      end;
    end;
  finally
    dstaSpecInv.DataSet:=taSpecInv;
    ViewDoc5.EndUpdate;
    prButton(True);
    Memo1.Lines.Add('������� ��������.'); delay(10);
  end;
end;

procedure TfmDocsInvS.acEQIALQExecute(Sender: TObject);
Var iC:Integer;
begin
  if cxButton1.Enabled then
  begin
     if MessageDlg('�������� ����=����?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
     begin
       iCol:=0;
       Memo1.Lines.Add('����� ... '); delay(10);
       Memo1.Lines.Add('  - ����� '+its(taSpecInv.RecordCount)); delay(10);

       try
         prButton(False);
         ViewDoc5.BeginUpdate;
         dstaSpecInv.DataSet:=nil;
         with dmMCS do
         begin
           iC:=0;
           taSpecInv.First;
           while not taSpecInv.Eof do
           begin
             iCol:=1;
             taSpecInv.Edit;
             taSpecInvQuantF.AsFloat:=taSpecInvQuantR.AsFloat;
             taSpecInv.Post;

             taSpecInv.Next;

             inc(iC);
             if iC mod 100 =0 then
             begin
               StatusBar1.Panels[0].Text:=its(iC);
               Memo1.Lines.Add('    .. '+its(iC)); delay(10);
             end;
           end;
           Memo1.Lines.Add('    .. '+its(iC)); delay(10);
         end;
       finally
         dstaSpecInv.DataSet:=taSpecInv;
         ViewDoc5.EndUpdate;
         prButton(True);
         Memo1.Lines.Add('������� ��������.'); delay(10);
       end;
     end;
  end;
end;

procedure TfmDocsInvS.acMoveExecute(Sender: TObject);
begin
  //�������� ������ - ����� �� ��������� �� �����
  with dmMCS do
  begin
    if taSpecInv.RecordCount>0 then
    begin
      prFormMove(cxLookupComboBox1.EditValue,taSpecInvCodeTovar.AsInteger,Trunc(CommonSet.DateBeg),Trunc(CommonSet.DateEnd),taSpecInvName.AsString);
      fmMove.ShowModal;
    end;
  end;
end;

procedure TfmDocsInvS.acAddAllMoveExecute(Sender: TObject);
Var iC,iAdd:Integer;
begin
  //�������� ��� ������ � ���������
  with dmMCS do
  begin
    if MessageDlg('�������� ��������� ������ ?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      Memo1.Clear;
      Memo1.Lines.Add('����� ���� ������������ ������.');
      quCardsMoveList.Active:=False;
      quCardsMoveList.SQL.Clear;
      quCardsMoveList.SQL.Add('SELECT [ICODE] FROM [MCRYSTAL].[dbo].[GDSMOVE]');
      quCardsMoveList.SQL.Add('where [ISKL]='+its(cxLookupComboBox1.EditValue));
      quCardsMoveList.SQL.Add('group by [ICODE]');
      quCardsMoveList.Active:=True;
      Memo1.Lines.Add('����� ���� ���������� �������. ����� - '+its(quCardsMoveList.RecordCount));
      iC:=0;   iAdd:=0;

      ViewDoc5.BeginUpdate;
      dstaSpecInv.DataSet:=nil;

      quCardsMoveList.First;
      while not quCardsMoveList.Eof do
      begin
        if taSpecInv.Locate('CodeTovar',quCardsMoveListICODE.AsInteger,[])=False then
        begin
          taSpecInv.Append;
          taSpecInvCodeTovar.AsInteger:=quCardsMoveListICODE.AsInteger;
          taSpecInvName.AsString:='';
          taSpecInvEdIzm.AsInteger:=1;
          taSpecInvNDSProc.AsFloat:=0;
          taSpecInvPrice.AsFloat:=0;
          taSpecInvPriceSS.AsFloat:=0;
          taSpecInvQuantR.AsFloat:=0;
          taSpecInvQuantF.AsFloat:=0;
          taSpecInvQuantC.AsFloat:=0;
          taSpecInvqOpSv.AsFloat:=0;
          taSpecInvSumR.AsFloat:=0;
          taSpecInvSumF.AsFloat:=0;
          taSpecInvSumC.AsFloat:=0;
          taSpecInvSumRSS.AsFloat:=0;
          taSpecInvSumFSS.AsFloat:=0;
          taSpecInvSumRSC.AsFloat:=0;
          taSpecInvSumRTO.AsFloat:=0;
          taSpecInvSumDTO.AsFloat:=0;
          taSpecInvSumDTOSS.AsFloat:=0;
          taSpecInvBarCode.AsString:='';
          taSpecInvNum.AsInteger:=taSpecInv.RecordCount+1;
          taSpecInv.Post;

          inc(iAdd);
        end;

        quCardsMoveList.Next; inc(iC);
        if iC mod 100 = 0 then
        begin
          Memo1.Lines.Add('  �������� - '+its(iC)+'  ��������� - '+its(iAdd));
          delay(10);
        end;
      end;
      Memo1.Lines.Add('  �������� - '+its(iC)+'  ��������� - '+its(iAdd)); delay(10);

      quCardsMoveList.Active:=False;
      Memo1.Lines.Add('������� ��������.');

      dstaSpecInv.DataSet:=taSpecInv;
      ViewDoc5.EndUpdate;
    end;
  end;
end;

procedure TfmDocsInvS.cxLookupComboBox1PropertiesChange(Sender: TObject);
begin
  if fmDocsInvS.Showing then
    if fmDocsInvS.Tag=0 then cxTextEdit1.Text:=fGetNumDoc(cxLookupComboBox1.EditValue,20);
end;

procedure TfmDocsInvS.acGenExecute(Sender: TObject);
Var iC,iAdd:Integer;
begin
  //������������
  if MessageDlg('��������� ������������ ���� ������������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
    with dmMCS do
    begin
      Memo1.Clear;
      Memo1.Lines.Add('����� ���� ������������ ������.');
      quCardsMoveList.Active:=False;
      quCardsMoveList.SQL.Clear;
      quCardsMoveList.SQL.Add('SELECT [ICODE] FROM [dbo].[GDSMOVE]');
      quCardsMoveList.SQL.Add('where [ISKL]='+its(cxLookupComboBox1.EditValue));
      quCardsMoveList.SQL.Add('group by [ICODE]');
      quCardsMoveList.Active:=True;

      quCardsInv.Active:=False;
      quCardsInv.SQL.Clear;
      quCardsInv.SQL.Add('SELECT spec.[IDCARD]');
      quCardsInv.SQL.Add('FROM [dbo].[DOCINV_SPEC] spec');
      quCardsInv.SQL.Add('left join dbo.DOCINV_HEAD head on head.ID=spec.[IDHEAD]');
      quCardsInv.SQL.Add('where head.IDSKL='+its(cxLookupComboBox1.EditValue));
      quCardsInv.SQL.Add('and IDATEDOC='+its(trunc(cxDateEdit1.Date)));
      quCardsInv.SQL.Add('group by spec.[IDCARD]');
      quCardsInv.Active:=True;

      Memo1.Lines.Add('����� ���� ���������� �������. ����� - '+its(quCardsMoveList.RecordCount));
      iC:=0;   iAdd:=0;

      ViewDoc5.BeginUpdate;
      dstaSpecInv.DataSet:=nil;

      quCardsMoveList.First;
      while not quCardsMoveList.Eof do
      begin
        if taSpecInv.Locate('CodeTovar',quCardsMoveListICODE.AsInteger,[])=False then
        begin
          //��� � ������������ - ����� ��� ���� ������ � ������ ���������������
          if quCardsInv.Locate('IDCARD',quCardsMoveListICODE.AsInteger,[])=False then
          begin
            taSpecInv.Append;
            taSpecInvCodeTovar.AsInteger:=quCardsMoveListICODE.AsInteger;
            taSpecInvName.AsString:='';
            taSpecInvEdIzm.AsInteger:=1;
            taSpecInvNDSProc.AsFloat:=0;
            taSpecInvPrice.AsFloat:=0;
            taSpecInvPriceSS.AsFloat:=0;
            taSpecInvQuantR.AsFloat:=0;
            taSpecInvQuantF.AsFloat:=0;
            taSpecInvQuantC.AsFloat:=0;
            taSpecInvqOpSv.AsFloat:=0;
            taSpecInvSumR.AsFloat:=0;
            taSpecInvSumF.AsFloat:=0;
            taSpecInvSumC.AsFloat:=0;
            taSpecInvSumRSS.AsFloat:=0;
            taSpecInvSumFSS.AsFloat:=0;
            taSpecInvSumRSC.AsFloat:=0;
            taSpecInvSumRTO.AsFloat:=0;
            taSpecInvSumDTO.AsFloat:=0;
            taSpecInvSumDTOSS.AsFloat:=0;
            taSpecInvBarCode.AsString:='';
            taSpecInvNum.AsInteger:=taSpecInv.RecordCount+1;
            taSpecInv.Post;

            inc(iAdd);
          end;
        end;

        quCardsMoveList.Next; inc(iC);
        if iC mod 100 = 0 then
        begin
          Memo1.Lines.Add('  �������� - '+its(iC)+'  ��������� - '+its(iAdd));
          delay(10);
        end;
      end;
      Memo1.Lines.Add('  �������� - '+its(iC)+'  ��������� - '+its(iAdd)); delay(10);

      quCardsInv.Active:=False;
      quCardsMoveList.Active:=False;
      Memo1.Lines.Add('������� ��������.');

      dstaSpecInv.DataSet:=taSpecInv;
      ViewDoc5.EndUpdate;
    end;
  end;
end;

procedure TfmDocsInvS.cxLabel4Click(Sender: TObject);
begin
  if acSaveDoc.Enabled then acGen.Execute;
end;

procedure TfmDocsInvS.acTestPriceExecute(Sender: TObject);
Var iCode:INteger;
    rPr:Real;
begin
  //�������� ��� �� ������
  if MessageDlg('�������� ������ ������� ��������� ��� �� ������������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
    with dmMCS do
    begin
      Memo1.Clear;
      Memo1.Lines.Add('������.');
      // ��������� ������

      quPrice.Active:=False;
      quPrice.Parameters.ParamByName('ISKL').Value:=cxLookupComboBox1.EditValue;
      quPrice.Active:=True;

      taSpecInv.First;
      while not taSpecInv.Eof do
      begin
        iCode:=taSpecInvCodeTovar.AsInteger;
        rPr:=rv(taSpecInvPrice.AsFloat);
        try
          if quPrice.Locate('GOODSID',iCode,[]) then
          begin
            if rv(quPricePRICE.AsFloat)<rPr then
            begin
              Memo1.Lines.Add('  -- �������� ���� c '+fts(quPricePRICE.AsFloat)+' �� '+fts(rPr)+' �� ��� '+its(iCode));
              quPrice.Edit;
//            quPriceGOODSID.AsInteger:=iCode;
//            quPriceIDSKL.AsInteger:=iDep;
              quPricePRICE.AsFloat:=rPr;
              quPrice.Post;
            end;
          end else
          begin
            Memo1.Lines.Add('  -- ���������� ���� '+fts(rPr)+' �� ��� '+its(iCode));

            quPrice.Append;
            quPriceGOODSID.AsInteger:=iCode;
            quPriceIDSKL.AsInteger:=cxLookupComboBox1.EditValue;
            quPricePRICE.AsFloat:=rPr;
            quPrice.Post;

          end;
        except
          quPrice.Cancel;
        end;

        taSpecInv.Next;
      end;

      quPrice.Active:=False;

      Memo1.Lines.Add('������� ��������.');
    end;
  end;
end;

end.
