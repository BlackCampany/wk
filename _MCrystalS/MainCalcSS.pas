unit MainCalcSS;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, cxTextEdit, cxMemo, ComCtrls,
  cxControls, cxContainer, cxEdit, cxCheckBox, StdCtrls, cxButtons,
  ExtCtrls, ActnList, XPStyleActnCtrls, ActnMan, DB, ADODB;

type
  TfmMainCalcSS = class(TForm)
    cxButton1: TcxButton;
    cxCheckBox1: TcxCheckBox;
    StatusBar1: TStatusBar;
    Memo1: TcxMemo;
    Timer1: TTimer;
    am1: TActionManager;
    acStart: TAction;
    quShopsList: TADOQuery;
    quShopsListIDS: TIntegerField;
    quDepsShop: TADOQuery;
    quDepsShopIDS: TIntegerField;
    quDepsShopID: TIntegerField;
    quDepsShopNAME: TStringField;
    quDepsShopFULLNAME: TStringField;
    quDepsShopISTATUS: TSmallintField;
    quDepsShopIDORG: TIntegerField;
    quDepsShopGLN: TStringField;
    quDepsShopISS: TSmallintField;
    quDepsShopPRIOR: TSmallintField;
    quDepsShopPLATNDS: TSmallintField;
    quDepsShopINN: TStringField;
    quDepsShopKPP: TStringField;
    quDepsShopDIR1: TStringField;
    quDepsShopDIR2: TStringField;
    quDepsShopDIR3: TStringField;
    quDepsShopGB1: TStringField;
    quDepsShopGB2: TStringField;
    quDepsShopGB3: TStringField;
    quDepsShopCITY: TStringField;
    quDepsShopSTREET: TStringField;
    quDepsShopHOUSE: TStringField;
    quDepsShopKORP: TStringField;
    quDepsShopPOSTINDEX: TStringField;
    quDepsShopPHONE: TStringField;
    quDepsShopSERLIC: TStringField;
    quDepsShopNUMLIC: TStringField;
    quDepsShopORGAN: TStringField;
    quDepsShopDATEB: TDateTimeField;
    quDepsShopDATEE: TDateTimeField;
    quDepsShopIP: TSmallintField;
    quDepsShopIPREG: TStringField;
    quDepsShopNAMEOTPR: TStringField;
    quDepsShopADROPR: TStringField;
    quDepsShopRSCH: TStringField;
    quDepsShopKSCH: TStringField;
    quDepsShopBIK: TStringField;
    quDepsShopBANK: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure acStartExecute(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

Procedure WrMess(S:String);

var
  fmMainCalcSS: TfmMainCalcSS;

implementation

uses Un1, Dm;

{$R *.dfm}

Procedure WrMess(S:String);
Var StrWk:String;
begin
  try
    if fmMainCalcSS.Memo1.Lines.Count > 2000 then fmMainCalcSS.Memo1.Clear;
    delay(10);
    if S>'' then StrWk:=FormatDateTime('dd.mm hh:nn:ss:zzz',now)+'  '+S
    else StrWk:=S;
    WriteHistory(StrWk);
    fmMainCalcSS.Memo1.Lines.Add(StrWk);
    delay(10);
  except
    try
      WriteHistory(StrWk);
      StrWk:=FormatDateTime('dd.mm hh:nn:ss:zzz',now)+'  ���� ������ �������.';
      WriteHistory(StrWk);
    except
    end;
  end;
end;


procedure TfmMainCalcSS.FormCreate(Sender: TObject);
begin
  CurDir := ExtractFilePath(ParamStr(0));

  Memo1.Clear;

  ReadIni;
  Person.Id:=0;
  Person.Name:='Auto';

//  if (Frac(time)>=0.041666666)and(Frac(time)<=0.208333333) then  // c 1:00 �� 5:00

  cxCheckBox1.Checked:=True;
  Timer1.Enabled:=True;
end;

procedure TfmMainCalcSS.Timer1Timer(Sender: TObject);
Var n:Integer;
begin
  Timer1.Enabled:=False;

  for n:=0 to 10 do
  begin
    cxCheckBox1.Caption:='�� ������� '+INtToStr(10-n)+' ���.';
    delay(1000); //�������� 10 ���
    if cxCheckBox1.Checked=False then Break;
  end;

  if cxCheckBox1.Checked then
  begin
    acStart.Execute;
    Close;
  end;
end;

procedure TfmMainCalcSS.acStartExecute(Sender: TObject);
Var StrWk:String;
    iShop:INteger;
    iPointA,iPointACalc:Integer;
begin
  Memo1.Clear;
  WrMess('������.');
  with dmMCS do
  begin
    msConnection.Connected:=False;
    Strwk:='FILE NAME='+CurDir+'mcr.udl';
    msConnection.ConnectionString:=Strwk;
    delay(10);
    msConnection.Connected:=True;
    delay(10);
    if msConnection.Connected then
    begin
      WrMess('  ���� ��');
      quShopsList.Active:=False;
      quShopsList.Active:=True;
      quShopsList.First;
      while not quShopsList.Eof do
      begin
        iShop:=quShopsListIDS.asInteger;
        WrMess('    ������� � '+its(iShop));

        quDepsShop.Active:=False;
        quDepsShop.Parameters.ParamByName('IDS').Value:=iShop;
        quDepsShop.Active:=True;
        quDepsShop.First;
        while not quDepsShop.Eof do
        begin
          WrMess('       - ����� '+quDepsShopNAME.AsString);
          iPointACalc:=Trunc(Date-8);
          iPointA:=Trunc(Date-8);

          quSel.Active:=False;
          quSel.SQL.Clear;
          quSel.SQL.Add('SELECT [ISKL],[IDATE] FROM [dbo].[SS_TA] where [ISKL]='+its(quDepsShopID.AsInteger));
          quSel.Active:=True;
          if quSel.RecordCount>0 then  iPointA:=quSel.Fields.FieldByName('IDATE').Value; // ���������� ��������
          quSel.Active:=False;

          WrMess('          ����� ������������ - '+ds1(iPointA)+' ('+its(iPointA)+')');

          if iPointA>iPointACalc then iPointA:=iPointACalc;

          WrMess('          ������ � - '+ds1(iPointA)+' ('+its(iPointA)+')');
          prWrLog(quDepsShopID.AsInteger,31,3,0,' ������ ������ � '+ds1(iPointA)+'('+its(iPointA)+') �� '+ds1(Trunc(date))+'('+its(Trunc(date))+')');

          if iPointA<prOpenDate(quDepsShopID.AsInteger) then
          begin
            WrMess('          ������ ������.');
            prWrLog(quDepsShopID.AsInteger,31,3,0,' ������ ������ '+ds1(iPointA)+'('+its(iPointA)+')');
          end
          else
          begin
             WrMess('           - ������ ..');  delay(10);
             prRecalcSS(quDepsShopID.AsInteger,iPointA,Trunc(date),Memo1);
             prWrLog(quDepsShopID.AsInteger,31,1,0,' ������������ �� ������ � '+ds1(iPointA)+'('+its(iPointA)+') �� '+ds1(Trunc(date))+'('+its(Trunc(date))+')');
             WrMess('           - ������ ��');
          end;

          //��������� ������
          WrMess('        ��������� ������ �� '+ds1(Trunc(Date-8))+' ������������.');  delay(10);
          quA.Active:=False;
          quA.SQL.Clear;
          quA.SQL.Add('');
          quA.SQL.Add('INSERT INTO [dbo].[CLOSEHIST]');
          quA.SQL.Add('           ([ISKL]           ');
          quA.SQL.Add('           ,[IDATE]          ');
          quA.SQL.Add('           ,[DDATE]          ');
          quA.SQL.Add('           ,[IPERS]          ');
          quA.SQL.Add('           ,[DDATEEDIT])     ');
          quA.SQL.Add('     VALUES                  ');
          quA.SQL.Add('           ('+its(quDepsShopID.AsInteger));//<ISKL, int,>
          quA.SQL.Add('           ,'+its(Trunc(Date-8)));//<IDATE, int,>
          quA.SQL.Add('           ,'''+ds1(Trunc(Date-8))+'''');//<DDATE, datetime,>
          quA.SQL.Add('           ,0');//<IPERS, int,>
          quA.SQL.Add('           ,'''+ds1(now)+' '+formatdatetime('hh:nn:ss',now)+''')');//<DDATEEDIT, datetime,>

          quA.ExecSQL;

          quDepsShop.Next;
        end;
        quDepsShop.Active:=False;

        quShopsList.Next;
      end;
      quShopsList.Active:=False;
      msConnection.Connected:=False;
    end else  WrMess('  ������ ��.');
  end;
  WrMess('������� ��������.');
end;

procedure TfmMainCalcSS.cxButton1Click(Sender: TObject);
begin
  Timer1.Enabled:=False;
  cxCheckBox1.Checked:=False;
  acStart.Execute;
end;

end.
