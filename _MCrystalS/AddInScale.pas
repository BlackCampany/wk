unit AddInScale;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  StdCtrls, cxButtons;

type
  TfmAddInScale = class(TForm)
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    ViAddInSc: TcxGridDBTableView;
    LeAddInSc: TcxGridLevel;
    GrAddInSc: TcxGrid;
    ViAddInScID: TcxGridDBColumn;
    ViAddInScNAME: TcxGridDBColumn;
    Label1: TLabel;
    procedure ViAddInScDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAddInScale: TfmAddInScale;

implementation

uses Dm;

{$R *.dfm}

procedure TfmAddInScale.ViAddInScDblClick(Sender: TObject);
begin
  modalresult:=mrOk;
end;

end.
