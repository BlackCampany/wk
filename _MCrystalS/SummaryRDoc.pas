unit SummaryRDoc;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons, ExtCtrls,
  ComCtrls, cxClasses, cxGraphics, cxCustomData, cxStyles, cxEdit,
  cxControls, cxCustomPivotGrid, cxDBPivotGrid, dxPSGlbl, dxPSUtl,
  dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
  dxPScxPivotGrid2Lnk, ActnList, XPStyleActnCtrls, ActnMan, cxImageComboBox,
  cxDBLookupComboBox;

type
  TfmSummary1 = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    PivotGrid1: TcxDBPivotGrid;
    PrintExp3: TdxComponentPrinter;
    PrintExp3Link1: TcxPivotGridReportLink;
    am1: TActionManager;
    acPrintTab: TAction;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    PrintExp3Link2: TcxPivotGridReportLink;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    PivotGrid2: TcxDBPivotGrid;
    PivotGrid1Field1: TcxDBPivotGridField;
    PivotGrid1Field2: TcxDBPivotGridField;
    PivotGrid1Field3: TcxDBPivotGridField;
    PivotGrid1Field4: TcxDBPivotGridField;
    PivotGrid1Field5: TcxDBPivotGridField;
    PivotGrid1Field6: TcxDBPivotGridField;
    PivotGrid1Field7: TcxDBPivotGridField;
    PivotGrid1Field8: TcxDBPivotGridField;
    PivotGrid1Field9: TcxDBPivotGridField;
    PivotGrid1Field10: TcxDBPivotGridField;
    PivotGrid1Field11: TcxDBPivotGridField;
    PivotGrid1Field12: TcxDBPivotGridField;
    PivotGrid1Field13: TcxDBPivotGridField;
    PivotGrid1Field14: TcxDBPivotGridField;
    PivotGrid1Field15: TcxDBPivotGridField;
    PivotGrid1Field16: TcxDBPivotGridField;
    PivotGrid1Field17: TcxDBPivotGridField;
    PivotGrid1Field18: TcxDBPivotGridField;
    PivotGrid1Field19: TcxDBPivotGridField;
    PivotGrid1Field20: TcxDBPivotGridField;
    PivotGrid1Field21: TcxDBPivotGridField;
    PivotGrid1Field22: TcxDBPivotGridField;
    PivotGrid1Field23: TcxDBPivotGridField;
    PivotGrid1Field24: TcxDBPivotGridField;
    PivotGrid1Field25: TcxDBPivotGridField;
    PivotGrid1Field26: TcxDBPivotGridField;
    PivotGrid2Field1: TcxDBPivotGridField;
    PivotGrid2Field2: TcxDBPivotGridField;
    PivotGrid2Field3: TcxDBPivotGridField;
    PivotGrid2Field4: TcxDBPivotGridField;
    PivotGrid2Field5: TcxDBPivotGridField;
    PivotGrid2Field6: TcxDBPivotGridField;
    PivotGrid2Field7: TcxDBPivotGridField;
    PivotGrid2Field8: TcxDBPivotGridField;
    PivotGrid2Field9: TcxDBPivotGridField;
    PivotGrid2Field10: TcxDBPivotGridField;
    PivotGrid2Field11: TcxDBPivotGridField;
    PivotGrid2Field12: TcxDBPivotGridField;
    PivotGrid1Field27: TcxDBPivotGridField;
    PivotGrid1Field28: TcxDBPivotGridField;
    PivotGrid1Field29: TcxDBPivotGridField;
    procedure FormCreate(Sender: TObject);
    procedure acPrintTabExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmSummary1: TfmSummary1;

implementation

uses Un1, RepPrib, SortPar, ExcelList, DB, Dm;

{$R *.dfm}

procedure TfmSummary1.FormCreate(Sender: TObject);
begin
  PivotGrid1.Align:=alClient;
  PivotGrid2.Align:=alClient;
  PivotGrid1.Visible:=True;
  PivotGrid2.Visible:=False;
  PivotGrid1.RestoreFromIniFile(sGridIni);
  PivotGrid2.RestoreFromIniFile(sGridIni);
end;

procedure TfmSummary1.acPrintTabExecute(Sender: TObject);
begin
  if PivotGrid1.Visible then
  begin
    PrintExp3.CurrentLink:=PrintExp3Link1;
    //������ �������
    PrintExp3Link1.ReportTitle.Text:=fmSummary1.Caption;
    PrintExp3.Preview(True,nil);
  end;
  if PivotGrid2.Visible then
  begin
    PrintExp3.CurrentLink:=PrintExp3Link2;
    PrintExp3Link2.ReportTitle.Text:=fmSummary1.Caption;

    PrintExp3.Preview(True,nil);
  end;
end;

procedure TfmSummary1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  PivotGrid1.StoreToIniFile(sGridIni,False);
  PivotGrid2.StoreToIniFile(sGridIni,False);
end;

procedure TfmSummary1.cxButton3Click(Sender: TObject);
Var i:INteger;
    StrWk:String;
    fField:TcxPivotGridField;
begin
  if PivotGrid1.Visible then
  begin
    try
      fmSortF:=tfmSortF.Create(Application);
      with fmSortF do
      begin
        CloseTe(taRowF);
        CloseTe(taColVal);

        for i:=0 to PivotGrid1.FieldCount-1 do
        begin
          if PivotGrid1.Fields[i].Area=faRow then
          begin
            taRowF.Append;
            taRowFId.AsInteger:=i;
            taRowFCapt.AsString:=PivotGrid1.Fields[i].Caption;
            StrWk:=PivotGrid1.Fields[i].Name;
//          Delete(StrWk,1,10);
            taRowFNameF.AsString:=StrWk;
            taRowF.Post;
          end;

          if PivotGrid1.Fields[i].Area=faData then
          begin
            taColVal.Append;
            taColValId.AsInteger:=i;
            taColValCapt.AsString:=PivotGrid1.Fields[i].Caption;
            StrWk:=PivotGrid1.Fields[i].Name;
//          Delete(StrWk,1,10);
            taColValNameF.AsString:=StrWk;
            taColVal.Post;
          end;
        end;

        taRowF.Last;
        if taRowF.RecordCount>0 then
        begin
          cxLookupComboBox1.EditValue:=taRowFNameF.AsString;
          cxLookupComboBox1.Text:=taRowFCapt.AsString;
          cxLookupComboBox1.Enabled:=True;
        end else
        begin
          cxLookupComboBox1.EditValue:='';
          cxLookupComboBox1.Text:='��� ��������';
          cxLookupComboBox1.Enabled:=False;
        end;


        taColVal.First;
        if taColVal.RecordCount>0 then
        begin
          cxLookupComboBox2.EditValue:=taColValNameF.AsString;
          cxLookupComboBox2.Text:=taColValCapt.AsString;
          cxLookupComboBox2.Enabled:=True;
        end else
        begin
          cxLookupComboBox2.EditValue:='';
          cxLookupComboBox2.Text:='��� ��������';
          cxLookupComboBox2.Enabled:=False;
        end;
      end;
      fmSortF.ShowModal;
      if fmSortF.ModalResult=mrOk then
      begin
        // sync settings with selected field
        PivotGrid1.BeginUpdate;
        fField:=PivotGrid1.GetFieldByName(fmSortF.cxLookupComboBox1.Text);
        if fmSortF.cxRadioButton1.Checked then fField.SortOrder:=soDescending;
        if fmSortF.cxRadioButton2.Checked then fField.SortOrder:=soAscending;
        fField.SortBySummaryInfo.Field := PivotGrid1.GetFieldByName(fmSortF.cxLookupComboBox2.Text);

        if fmSortF.cxCheckBox1.Checked then
        begin
          fField.TopValueCount:=0;
          fField.TopValueShowOthers:=True;
        end
        else
        begin
          fField.TopValueCount:=fmSortF.cxSpinEdit1.EditValue;
          fField.TopValueShowOthers:=fmSortF.CheckBox1.Checked;
        end;

        PivotGrid1.EndUpdate;
      end;
    finally
      fmSortF.Release;
    end;
  end;
  if PivotGrid2.Visible then
  begin
    try
      fmSortF:=tfmSortF.Create(Application);
      with fmSortF do
      begin
        CloseTe(taRowF);
        CloseTe(taColVal);

        for i:=0 to PivotGrid2.FieldCount-1 do
        begin
          if PivotGrid2.Fields[i].Area=faRow then
          begin
            taRowF.Append;
            taRowFId.AsInteger:=i;
            taRowFCapt.AsString:=PivotGrid2.Fields[i].Caption;
            StrWk:=PivotGrid2.Fields[i].Name;
//          Delete(StrWk,1,10);
            taRowFNameF.AsString:=StrWk;
            taRowF.Post;
          end;

          if PivotGrid2.Fields[i].Area=faData then
          begin
            taColVal.Append;
            taColValId.AsInteger:=i;
            taColValCapt.AsString:=PivotGrid2.Fields[i].Caption;
            StrWk:=PivotGrid2.Fields[i].Name;
//          Delete(StrWk,1,10);
            taColValNameF.AsString:=StrWk;
            taColVal.Post;
          end;
        end;

        taRowF.Last;
        if taRowF.RecordCount>0 then
        begin
          cxLookupComboBox1.EditValue:=taRowFNameF.AsString;
          cxLookupComboBox1.Text:=taRowFCapt.AsString;
          cxLookupComboBox1.Enabled:=True;
        end else
        begin
          cxLookupComboBox1.EditValue:='';
          cxLookupComboBox1.Text:='��� ��������';
          cxLookupComboBox1.Enabled:=False;
        end;


        taColVal.First;
        if taColVal.RecordCount>0 then
        begin
          cxLookupComboBox2.EditValue:=taColValNameF.AsString;
          cxLookupComboBox2.Text:=taColValCapt.AsString;
          cxLookupComboBox2.Enabled:=True;
        end else
        begin
          cxLookupComboBox2.EditValue:='';
          cxLookupComboBox2.Text:='��� ��������';
          cxLookupComboBox2.Enabled:=False;
        end;
      end;
      fmSortF.ShowModal;
      if fmSortF.ModalResult=mrOk then
      begin
        // sync settings with selected field
        PivotGrid2.BeginUpdate;
        fField:=PivotGrid2.GetFieldByName(fmSortF.cxLookupComboBox1.Text);
        if fmSortF.cxRadioButton1.Checked then fField.SortOrder:=soDescending;
        if fmSortF.cxRadioButton2.Checked then fField.SortOrder:=soAscending;
        fField.SortBySummaryInfo.Field := PivotGrid2.GetFieldByName(fmSortF.cxLookupComboBox2.Text);

        if fmSortF.cxCheckBox1.Checked then
        begin
          fField.TopValueCount:=0;
          fField.TopValueShowOthers:=True;
        end
        else
        begin
          fField.TopValueCount:=fmSortF.cxSpinEdit1.EditValue;
          fField.TopValueShowOthers:=fmSortF.CheckBox1.Checked;
        end;

        PivotGrid2.EndUpdate;
      end;
    finally
      fmSortF.Release;
    end;
  end;
end;

procedure TfmSummary1.cxButton4Click(Sender: TObject);
var
  PG:TcxDBPivotGrid;
begin
  //������� � ������
  fmExcelList:=tfmExcelList.Create(Application);
  with fmExcelList do
  begin
    GridEx.Align:=AlClient;
    ViewEx.BeginUpdate;

    if PivotGrid1.Visible then
    begin
      dsSummary.PivotGrid:=PivotGrid1;
      PG:=PivotGrid1;
    end;
    if PivotGrid2.Visible then
    begin
      dsSummary.PivotGrid:=PivotGrid2;
      PG:=PivotGrid2;
    end;

    dsSummary.CreateData;

    try
      ViewEx.ClearItems;
      ViewEx.DataController.CreateAllItems;
      CreateFooterSummary(ViewEx,PG);
    finally
      ViewEx.EndUpdate;
    end;
  end;
  fmExcelList.ShowModal;
  fmExcelList.dsSummary.PivotGrid:=nil;
  fmExcelList.Release;

end;

end.
