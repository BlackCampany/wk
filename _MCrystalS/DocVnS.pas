unit DocVnS;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, cxGraphics, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxImageComboBox, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxControls, cxGridCustomView, cxGrid,
  cxMemo, ComCtrls, cxLabel, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox, cxCheckBox, StdCtrls, cxButtons, cxMaskEdit,
  cxCalendar, cxContainer, cxTextEdit, ExtCtrls, dxmdaset, ActnList,
  XPStyleActnCtrls, ActnMan, Placemnt, FR_DSet, FR_DBSet, FR_Class;

type
  TfmDocsVnS = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    Label5: TLabel;
    Label12: TLabel;
    cxTextEdit1: TcxTextEdit;
    cxDateEdit1: TcxDateEdit;
    cxButton3: TcxButton;
    cxCheckBox1: TcxCheckBox;
    cxLookupComboBox1: TcxLookupComboBox;
    Panel3: TPanel;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    cxLabel3: TcxLabel;
    cxLabel5: TcxLabel;
    cxLabel6: TcxLabel;
    StatusBar1: TStatusBar;
    GridDoc4: TcxGrid;
    ViewDoc4: TcxGridDBTableView;
    LevelDoc4: TcxGridLevel;
    Panel2: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton8: TcxButton;
    taSpecVn: TdxMemData;
    taSpecVnNum: TIntegerField;
    taSpecVnCodeTovar: TIntegerField;
    taSpecVnName: TStringField;
    taSpecVnEdIzm: TSmallintField;
    taSpecVnQuant: TFloatField;
    taSpecVnPriceR: TFloatField;
    taSpecVnPriceIn: TFloatField;
    taSpecVnSumR: TFloatField;
    taSpecVnSumIn: TFloatField;
    taSpecVnSumD: TFloatField;
    taSpecVnBarCode: TStringField;
    taSpecVnQuantRemn: TFloatField;
    taSpecVnPrice: TFloatField;
    dstaSpecVn: TDataSource;
    amAddDocVn: TActionManager;
    acSaveDoc: TAction;
    Panel5: TPanel;
    Memo1: TcxMemo;
    Gr1: TcxGrid;
    Vi1: TcxGridDBTableView;
    Vi1DATEDOC: TcxGridDBColumn;
    Vi1Name: TcxGridDBColumn;
    Vi1QUANT: TcxGridDBColumn;
    Vi1PRICEIN: TcxGridDBColumn;
    Vi1PROCN: TcxGridDBColumn;
    Vi1PRICEUCH: TcxGridDBColumn;
    Vi1SUMIN: TcxGridDBColumn;
    Vi1SUMUCH: TcxGridDBColumn;
    Vi1NUMDOC: TcxGridDBColumn;
    ViCenn: TcxGridDBTableView;
    ViCennRecId: TcxGridDBColumn;
    ViCennIdCard: TcxGridDBColumn;
    ViCennFullName: TcxGridDBColumn;
    ViCennEdIzm: TcxGridDBColumn;
    ViCennPrice1: TcxGridDBColumn;
    ViCennCountry: TcxGridDBColumn;
    ViCennBarCode: TcxGridDBColumn;
    ViCennQuant: TcxGridDBColumn;
    ViCennQuanrR: TcxGridDBColumn;
    le1: TcxGridLevel;
    Le2: TcxGridLevel;
    taSpecVnPriceIn0: TFloatField;
    taSpecVnSumIn0: TFloatField;
    ViewDoc4Num: TcxGridDBColumn;
    ViewDoc4CodeTovar: TcxGridDBColumn;
    ViewDoc4Name: TcxGridDBColumn;
    ViewDoc4BarCode: TcxGridDBColumn;
    ViewDoc4EdIzm: TcxGridDBColumn;
    ViewDoc4Price: TcxGridDBColumn;
    ViewDoc4Quant: TcxGridDBColumn;
    ViewDoc4PriceR: TcxGridDBColumn;
    ViewDoc4PriceIn: TcxGridDBColumn;
    ViewDoc4PriceIn0: TcxGridDBColumn;
    ViewDoc4SumR: TcxGridDBColumn;
    ViewDoc4SumIn: TcxGridDBColumn;
    ViewDoc4SumIn0: TcxGridDBColumn;
    ViewDoc4SumD: TcxGridDBColumn;
    ViewDoc4QuantRemn: TcxGridDBColumn;
    FormPlacementVn: TFormPlacement;
    acAddPos: TAction;
    acAddList: TAction;
    acSetPriceRemn: TAction;
    frRepDVn: TfrReport;
    frtaSpecVn: TfrDBDataSet;
    acDelPos: TAction;
    acDelAll: TAction;
    Label2: TLabel;
    cxTextEdit2: TcxTextEdit;
    procedure acSaveDocExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acAddPosExecute(Sender: TObject);
    procedure cxLabel5Click(Sender: TObject);
    procedure acAddListExecute(Sender: TObject);
    procedure cxLabel1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure ViewDoc4Editing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure taSpecVnQuantChange(Sender: TField);
    procedure taSpecVnPriceRChange(Sender: TField);
    procedure taSpecVnPriceInChange(Sender: TField);
    procedure taSpecVnPriceIn0Change(Sender: TField);
    procedure taSpecVnSumRChange(Sender: TField);
    procedure taSpecVnSumInChange(Sender: TField);
    procedure taSpecVnSumIn0Change(Sender: TField);
    procedure cxButton2Click(Sender: TObject);
    procedure acSetPriceRemnExecute(Sender: TObject);
    procedure cxLabel3Click(Sender: TObject);
    procedure ViewDoc4SelectionChanged(Sender: TcxCustomGridTableView);
    procedure Vi1DblClick(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxLabel2Click(Sender: TObject);
    procedure acDelPosExecute(Sender: TObject);
    procedure acDelAllExecute(Sender: TObject);
    procedure cxLookupComboBox1PropertiesChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

Function SaveDocsVn(var IdH:Integer):boolean;

var
  fmDocsVnS: TfmDocsVnS;
  iCol:INteger;

implementation

uses Dm, Un1, MainMC, Cards, DocVnH, sumprops;

{$R *.dfm}

Function SaveDocsVn(var IdH:Integer):boolean;
Var rS:array[1..10] of Real;
    i:INteger;
    bErr:Boolean;
begin
  with dmMCS do
  with fmDocsVnS do
  begin
    Result:=False;
    if cxLookupComboBox1.EditValue<1 then exit;

    bErr:=False;
    quVnRec.Active:=False;
    IDH:=fmDocsVnS.Tag;
    try
      ViewDoc4.BeginUpdate;

      if IDH=0 then //����������
      begin
        IDH:=fGetId(16); //��������� ���������� ����������

        quVnRec.Parameters.ParamByName('IDH').Value:=IDH;
        quVnRec.Active:=True;

        quVnRec.Append;
        quVnRecIDSKLFROM.AsInteger:=cxLookupComboBox1.EditValue;
        quVnRecIDSKLTO.AsInteger:=100;
        quVnRecIDATEDOC.AsInteger:=Trunc(cxDateEdit1.Date);
        quVnRecID.AsInteger:=IDH;
        quVnRecDATEDOC.AsDateTime:=Trunc(cxDateEdit1.Date);
        quVnRecNUMDOC.AsString:=Trim(cxTextEdit1.Text);
        quVnRecSUMIN.AsFloat:=0;
        quVnRecSUMIN0.AsFloat:=0;
        quVnRecSUMR.AsFloat:=0;
        quVnRecIACTIVE.AsInteger:=1;
        quVnRecCOMMENT.AsString:=Trim(cxTextEdit2.Text);
        quVnRec.Post;

        fmDocsVnS.Tag:=IDH;
      end else
      begin
        quVnRec.Parameters.ParamByName('IDH').Value:=IDH;
        quVnRec.Active:=True;
        try
          quVnRec.Edit;
          quVnRecIDSKLFROM.AsInteger:=cxLookupComboBox1.EditValue;
          quVnRecIDSKLTO.AsInteger:=100;
          quVnRecIDATEDOC.AsInteger:=Trunc(cxDateEdit1.Date);
          quVnRecID.AsInteger:=IDH;
          quVnRecDATEDOC.AsDateTime:=Trunc(cxDateEdit1.Date);
          quVnRecNUMDOC.AsString:=Trim(cxTextEdit1.Text);
          quVnRecSUMIN.AsFloat:=0;
          quVnRecSUMIN0.AsFloat:=0;
          quVnRecSUMR.AsFloat:=0;
          quVnRecIACTIVE.AsInteger:=1;
          quVnRecCOMMENT.AsString:=Trim(cxTextEdit2.Text);
          quVnRec.Post;
        except
        end;
      end;

      quSpecVnRec.Active:=False;
      quSpecVnRec.Parameters.ParamByName('IDH').Value:=IDH;
      quSpecVnRec.Active:=True;

      quSpecVnRec.First;
      while not quSpecVnRec.Eof do quSpecVnRec.Delete; //���� �������

      for i:=1 to 10 do rS[i]:=0;

      iCol:=0;

      taSpecVn.First;
      while not taSpecVn.Eof do
      begin

        taSpecVn.Edit;
        taSpecVnSumR.AsFloat:=rv(taSpecVnSumR.AsFloat);
        taSpecVnSumIn.AsFloat:=rv(taSpecVnSumIn.AsFloat);
        taSpecVnSumIn0.AsFloat:=rv(taSpecVnSumIn0.AsFloat);
        taSpecVn.Post;

        rS[1]:=rS[1]+taSpecVnSumR.AsFloat;
        rS[2]:=rS[2]+taSpecVnSumIn.AsFloat;
        rS[3]:=rS[3]+taSpecVnSumIn0.AsFloat;

        quSpecVnRec.Append;
        quSpecVnRecIDHEAD.AsInteger:=IDH;
        quSpecVnRecNUM.AsInteger:=taSpecVnNum.AsInteger;
        quSpecVnRecIDCARD.AsInteger:=taSpecVnCodeTovar.AsInteger;
        quSpecVnRecSBAR.AsString:=taSpecVnBarCode.AsString;

        quSpecVnRecITYPEPOS.AsInteger:=1;
        quSpecVnRecIDM.AsInteger:=taSpecVnEdIzm.AsInteger;
        quSpecVnRecQUANT.AsFloat:=taSpecVnQuant.AsFloat;
        quSpecVnRecPRICEIN.AsFloat:=taSpecVnPriceIn.AsFloat;
        quSpecVnRecPRICEIN0.AsFloat:=taSpecVnPriceIn0.AsFloat;
        quSpecVnRecPRICER.AsFloat:=taSpecVnPriceR.AsFloat;
        quSpecVnRecSUMIN.AsFloat:=taSpecVnSumIn.AsFloat;
        quSpecVnRecSUMIN0.AsFloat:=taSpecVnSumIn0.AsFloat;
        quSpecVnRecSUMR.AsFloat:=taSpecVnSumR.AsFloat;
        quSpecVnRec.Post;

        taSpecVn.Next;
      end;
      quSpecVnRec.Active:=False;

      quVnRec.Edit;
      quVnRecSUMIN.AsFloat:=rS[2];
      quVnRecSUMIN0.AsFloat:=rS[3];
      quVnRecSUMR.AsFloat:=rS[1];
      quVnRec.Post;

      bErr:=True;
    finally
      ViewDoc4.EndUpdate;
      if bErr then Result:=True;
    end;
  end;
end;


procedure TfmDocsVnS.acSaveDocExecute(Sender: TObject);
 //���������� ���������
Var IDH:INteger;
begin
  Memo1.Clear;
  Memo1.Lines.Add('����� ... ���� ���������� ���������.'); delay(10);
  if SaveDocsVn(IdH) then
  begin
    with dmMCS do
    begin
      fmDocsVnH.ViewDocsVn.BeginUpdate;
      quVnH.Requery();
      quVnH.Locate('ID',IDH,[]);
      fmDocsVnH.ViewDocsVn.EndUpdate;
      fmDocsVnH.ViewDocsVn.Controller.FocusRecord(fmDocsVnH.ViewDocsVn.DataController.FocusedRowIndex,True);

      Memo1.Lines.Add('���������� ��.'); delay(10);
    end;
  end else begin Memo1.Lines.Add('������.'); delay(10); end;
end;

procedure TfmDocsVnS.FormCreate(Sender: TObject);
begin
  FormPlacementVn.IniFileName:=sFormIni;
  FormPlacementVn.Active:=True;
  GridDoc4.Align:=alClient;
  ViewDoc4.RestoreFromIniFile(sGridIni);
end;

procedure TfmDocsVnS.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewDoc4.StoreToIniFile(sGridIni,False);
end;

procedure TfmDocsVnS.acAddPosExecute(Sender: TObject);
Var iMax:Integer;
begin
  iMax:=taSpecVn.RecordCount+1;

  if taSpecVn.Locate('CodeTovar',0,[])=False then
  begin
    taSpecVn.Append;
    taSpecVnNum.AsInteger:=iMax;
    taSpecVnCodeTovar.AsInteger:=0;
    taSpecVnName.AsString:='';
    taSpecVnEdIzm.AsInteger:=1;
    taSpecVnQuant.AsFloat:=0;
    taSpecVnBarCode.AsString:='';
    taSpecVnPrice.AsFloat:=0;
    taSpecVnQuant.AsFloat:=0;
    taSpecVnPriceR.AsFloat:=0;
    taSpecVnPriceIn.AsFloat:=0;
    taSpecVnPriceIn0.AsFloat:=0;
    taSpecVnSumR.AsFloat:=0;
    taSpecVnSumIn.AsFloat:=0;
    taSpecVnSumIn0.AsFloat:=0;
    taSpecVnSumD.AsFloat:=0;
    taSpecVnQuantRemn.AsFloat:=0;
    taSpecVn.Post;
  end;

  GridDoc4.SetFocus;

  ViewDoc4CodeTovar.Options.Editing:=True;
  ViewDoc4Name.Options.Editing:=True;

  ViewDoc4Name.Focused:=True;
  ViewDoc4.Controller.FocusRecord(ViewDoc4.DataController.FocusedRowIndex,True);
end;

procedure TfmDocsVnS.cxLabel5Click(Sender: TObject);
begin
  acAddPos.Execute;
end;

procedure TfmDocsVnS.acAddListExecute(Sender: TObject);
begin
  iDirect:=6;
  if cxLookupComboBox1.EditValue<>fmMainMC.Label3.Tag then
  begin
    fmMainMC.Label3.Tag:=cxLookupComboBox1.EditValue;
    CurDep:=fmMainMC.Label3.Tag;
    fmMainMC.Label3.Caption:=cxLookupComboBox1.Text;
    try
      fmCards.CardsView.BeginUpdate;
      prRefreshCards(Integer(fmCards.CardsTree.Selected.Data),cxLookupComboBox1.EditValue,fmCards.cxCheckBox1.Checked,fmCards.cxCheckBox2.Checked,fmCards.cxCheckBox3.Checked);
    finally
      fmCards.CardsView.EndUpdate;
    end;
  end;
  fmCards.Show;
end;

procedure TfmDocsVnS.cxLabel1Click(Sender: TObject);
begin
  acAddList.Execute;
end;

procedure TfmDocsVnS.FormShow(Sender: TObject);
begin
  iCol:=0;
  Memo1.Clear;
end;

procedure TfmDocsVnS.cxButton1Click(Sender: TObject);
begin
  acSaveDoc.Execute;
end;

procedure TfmDocsVnS.ViewDoc4Editing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  iCol:=0;
  if ViewDoc4.Controller.FocusedColumn.Name='ViewDoc4Quant' then iCol:=1;
  if ViewDoc4.Controller.FocusedColumn.Name='ViewDoc4PriceR' then iCol:=2;
  if ViewDoc4.Controller.FocusedColumn.Name='ViewDoc4PriceIn' then iCol:=3;
  if ViewDoc4.Controller.FocusedColumn.Name='ViewDoc4PriceIn0' then iCol:=4;
  if ViewDoc4.Controller.FocusedColumn.Name='ViewDoc4SumR' then iCol:=5;
  if ViewDoc4.Controller.FocusedColumn.Name='ViewDoc4SumIn' then iCol:=6;
  if ViewDoc4.Controller.FocusedColumn.Name='ViewDoc4SumIn0' then iCol:=7;
end;

procedure TfmDocsVnS.taSpecVnQuantChange(Sender: TField);
Var rQ:Real;
begin
  if iCol=1 then   //
  begin
    rQ:=taSpecVnQuant.AsFloat;
    taSpecVnSumR.AsFloat:=rv(rQ*taSpecVnPriceR.AsFloat);
    taSpecVnSumIn.AsFloat:=rv(rQ*taSpecVnPriceIn.AsFloat);
    taSpecVnSumIn0.AsFloat:=rv(rQ*taSpecVnPriceIn0.AsFloat);
    taSpecVnSumD.AsFloat:=taSpecVnSumR.AsFloat-taSpecVnSumIn.AsFloat;
  end;
end;

procedure TfmDocsVnS.taSpecVnPriceRChange(Sender: TField);
Var rQ:Real;
begin
  if iCol=2 then   //
  begin
    rQ:=taSpecVnQuant.AsFloat;
    taSpecVnSumR.AsFloat:=rv(rQ*taSpecVnPriceR.AsFloat);
    taSpecVnSumD.AsFloat:=taSpecVnSumR.AsFloat-taSpecVnSumIn.AsFloat;
  end;
end;

procedure TfmDocsVnS.taSpecVnPriceInChange(Sender: TField);
Var rQ:Real;
begin
  if iCol=3 then   //
  begin
    rQ:=taSpecVnQuant.AsFloat;
    taSpecVnSumIn.AsFloat:=rv(rQ*taSpecVnPriceIn.AsFloat);
    taSpecVnSumD.AsFloat:=taSpecVnSumR.AsFloat-taSpecVnSumIn.AsFloat;
  end;
end;

procedure TfmDocsVnS.taSpecVnPriceIn0Change(Sender: TField);
Var rQ:Real;
begin
  if iCol=4 then   //
  begin
    rQ:=taSpecVnQuant.AsFloat;
    taSpecVnSumIn0.AsFloat:=rv(rQ*taSpecVnPriceIn0.AsFloat);
  end;
end;

procedure TfmDocsVnS.taSpecVnSumRChange(Sender: TField);
Var rQ:Real;
begin
  if iCol=5 then   //
  begin
    rQ:=taSpecVnQuant.AsFloat;
    if rQ<>0 then
    begin
      taSpecVnPriceR.AsFloat:=taSpecVnSumR.AsFloat/rQ;
      taSpecVnSumD.AsFloat:=taSpecVnSumR.AsFloat-taSpecVnSumIn.AsFloat;
    end else
    begin
      iCol:=0;
      taSpecVnSumR.AsFloat:=0;
      taSpecVnSumIn.AsFloat:=0;
      taSpecVnSumIn0.AsFloat:=0;
      taSpecVnSumD.AsFloat:=0;
    end;
  end;
end;

procedure TfmDocsVnS.taSpecVnSumInChange(Sender: TField);
Var rQ:Real;
begin
  if iCol=6 then   //
  begin
    rQ:=taSpecVnQuant.AsFloat;
    if rQ<>0 then
    begin
      taSpecVnPriceIn.AsFloat:=taSpecVnSumIn.AsFloat/rQ;
      taSpecVnSumD.AsFloat:=taSpecVnSumR.AsFloat-taSpecVnSumIn.AsFloat;
    end else
    begin
      iCol:=0;
      taSpecVnSumR.AsFloat:=0;
      taSpecVnSumIn.AsFloat:=0;
      taSpecVnSumIn0.AsFloat:=0;
      taSpecVnSumD.AsFloat:=0;
    end;
  end;
end;

procedure TfmDocsVnS.taSpecVnSumIn0Change(Sender: TField);
Var rQ:Real;
begin
  if iCol=7 then   //
  begin
    rQ:=taSpecVnQuant.AsFloat;
    if rQ<>0 then
    begin
      taSpecVnPriceIn0.AsFloat:=taSpecVnSumIn0.AsFloat/rQ;
    end else
    begin
      iCol:=0;
      taSpecVnSumR.AsFloat:=0;
      taSpecVnSumIn.AsFloat:=0;
      taSpecVnSumIn0.AsFloat:=0;
      taSpecVnSumD.AsFloat:=0;
    end;
  end;
end;

procedure TfmDocsVnS.cxButton2Click(Sender: TObject);
begin
  Close;
end;

procedure TfmDocsVnS.acSetPriceRemnExecute(Sender: TObject);
Var rPrIn0,rPrIn,rPrR:Real;
    rRemn:Real;
begin
  //��������� � ����� ���������� �������
  if acSaveDoc.Enabled then
  begin
    if cxLookupComboBox1.EditValue>0 then
    begin
      Memo1.Clear;
      Memo1.Lines.Add('���������� � ����� ��'); delay(10);
      ViewDoc4.BeginUpdate;
      taSpecVn.First;
      while not taSpecVn.Eof do
      begin
        iCol:=0; //

        rPrIn:=prFLP(taSpecVnCodeTovar.AsInteger,cxLookupComboBox1.EditValue,rPrIn0,rPrIn,rPrR);

        rRemn:=prFindRemnDepD(taSpecVnCodeTovar.AsInteger,cxLookupComboBox1.EditValue,Trunc(date));

        taSpecVn.Edit;
        taSpecVnPriceIn.AsFloat:=rPrIn;
        taSpecVnPriceIn0.AsFloat:=rPrIn0;
        taSpecVnPriceR.AsFloat:=taSpecVnPrice.AsFloat;

        taSpecVnSumIn.AsFloat:=rv(taSpecVnQuant.AsFloat*rPrIn);
        taSpecVnSumIn0.AsFloat:=rv(taSpecVnQuant.AsFloat*rPrIn0);
        taSpecVnSumR.AsFloat:=rv(taSpecVnQuant.AsFloat*taSpecVnPriceR.AsFloat);

        taSpecVnSumD.AsFloat:=taSpecVnSumR.AsFloat-taSpecVnSumIn.AsFloat;
        taSpecVnQuantRemn.AsFloat:=rREmn;
        taSpecVn.Post;

        taSpecVn.Next;
        delay(10);
      end;
      iCol:=0;
      taSpecVn.First;
      ViewDoc4.EndUpdate;
      Memo1.Lines.Add('������� ��������.'); delay(10);
    //prSetNac;
    end else Showmessage('���������� ��');
  end;
end;

procedure TfmDocsVnS.cxLabel3Click(Sender: TObject);
begin
  acSetPriceRemn.Execute;
end;

procedure TfmDocsVnS.ViewDoc4SelectionChanged(
  Sender: TcxCustomGridTableView);
begin
  if ViewDoc4.Controller.SelectedRecordCount>1 then exit;
  with dmMCS do
  begin
    Vi1.BeginUpdate;
    quPosts2.Active:=False;
    quPosts2.Parameters.ParamByName('ICODE').Value:=taSpecVnCodeTovar.AsInteger;
    quPosts2.Parameters.ParamByName('ISKL').Value:=cxLookupComboBox1.EditValue;
    quPosts2.Active:=True;
    Vi1.EndUpdate;
  end;
end;

procedure TfmDocsVnS.Vi1DblClick(Sender: TObject);
begin
  if acSaveDoc.Enabled then
  begin
    with dmMCS do
    begin
      if quPosts2.RecordCount>0 then
      begin
        if taSpecVn.RecordCount>0 then
        begin
          iCol:=0; //PriceIn
          taSpecVn.Edit;
          taSpecVnPriceIn.AsFloat:=quPosts2PRICEIN.AsFloat;
          taSpecVnPriceIn0.AsFloat:=quPosts2PRICEIN0.AsFloat;

          taSpecVnSumIn.AsFloat:=rv(taSpecVnQuant.AsFloat*taSpecVnPriceIn.AsFloat);
          taSpecVnSumIn0.AsFloat:=rv(taSpecVnQuant.AsFloat*taSpecVnPriceIn0.AsFloat);
          taSpecVnSumR.AsFloat:=rv(taSpecVnQuant.AsFloat*taSpecVnPriceR.AsFloat);

          taSpecVnSumD.AsFloat:=taSpecVnSumR.AsFloat-taSpecVnSumIn.AsFloat;

          taSpecVn.Post;

          ViewDoc4EDIZM.Focused:=True;
        end;
      end;
    end;
  end;
end;

procedure TfmDocsVnS.cxButton3Click(Sender: TObject);
Var vPP:TfrPrintPages;
    rSum1,rSum2:Real;  //,rSum3
//    iSS:INteger;
    sStr:String;
begin
  try
    ViewDoc4.BeginUpdate;

    rSum1:=0; rSum2:=0;
//    iSS:=fSS(cxLookupComboBox1.EditValue);

    taSpecVn.First;
    while not taSpecVn.Eof do
    begin
      rSum1:=rSum1+taSpecVnSumIn.AsFloat;
      rSum2:=rSum2+taSpecVnSumR.AsFloat;
      taSpecVn.Next;
    end;

      sStr:='��� ��������  ';

//      if iSS=2 then frRepDVN.LoadFromFile(CommonSet.PathReport + 'ActSpis.frf');
      frRepDVN.LoadFromFile(CommonSet.PathReport + 'ActSpis.frf');

      frVariables.Variable['DocType']:=sStr;
      frVariables.Variable['DocNum']:=cxTextEdit1.Text;
      frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',cxDateEdit1.Date);
      frVariables.Variable['Depart']:=cxLookupComboBox1.Text;
      frVariables.Variable['SumOld']:=rSum1;
      frVariables.Variable['SumNew']:=rSum2;
      frVariables.Variable['SSumNew']:=MoneyToString(abs(rSum1),True,False);
      frVariables.Variable['SSumMag']:=MoneyToString(abs(rSum2),True,False);
      frVariables.Variable['SSumNDS']:=MoneyToString(0,True,False);

      frRepDVN.ReportName:='��� ��������.';

    frRepDVN.PrepareReport;
    delay(10);
    vPP:=frAll;
    if cxCheckBox1.Checked then frRepDVN.ShowPreparedReport
    else frRepDVN.PrintPreparedReport('',1,False,vPP);
    delay(10);
  finally
    delay(33);
    ViewDoc4.EndUpdate;
  end;
end;

procedure TfmDocsVnS.cxLabel2Click(Sender: TObject);
begin
  acDelPos.Execute;
end;

procedure TfmDocsVnS.acDelPosExecute(Sender: TObject);
begin
  //������� �������
  if acSaveDoc.Enabled then
    if taSpecVn.RecordCount>0 then
    begin
      taSpecVn.Delete;
    end;

end;

procedure TfmDocsVnS.acDelAllExecute(Sender: TObject);
begin
  if taSpecVn.RecordCount>0 then
  begin
    if acSaveDoc.Enabled then
    begin
      if MessageDlg('�� ������������� ������ �������� ������������ �������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        ViewDoc4.BeginUpdate;
        taSpecVn.First; while not taSpecVn.Eof do taSpecVn.Delete;
        ViewDoc4.EndUpdate;
      end;  
    end;
  end;
end;

procedure TfmDocsVnS.cxLookupComboBox1PropertiesChange(Sender: TObject);
begin
  if fmDocsVnS.Showing then
    if fmDocsVnS.Tag=0 then cxTextEdit1.Text:=fGetNumDoc(cxLookupComboBox1.EditValue,3);
end;

end.
