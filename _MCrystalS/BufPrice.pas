unit BufPrice;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ExtCtrls, cxStyles, cxCustomData, cxGraphics,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, Menus, cxLookAndFeelPainters,
  StdCtrls, cxButtons, cxImageComboBox, Placemnt, cxContainer, cxTextEdit,
  cxMemo, cxLabel, ActnList, XPStyleActnCtrls, ActnMan, cxMaskEdit,
  cxDropDownEdit, cxCalendar, cxCheckBox, dxmdaset, FR_DSet, FR_DBSet,
  FR_Class, FR_BarC;

type
  TfmBufPrice = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    ViewBufPr: TcxGridDBTableView;
    LevelBufPr: TcxGridLevel;
    GridBufPr: TcxGrid;
    ViewBufPrIDCARD: TcxGridDBColumn;
    ViewBufPrIDDOC: TcxGridDBColumn;
    ViewBufPrTYPEDOC: TcxGridDBColumn;
    ViewBufPrNEWP: TcxGridDBColumn;
    ViewBufPrSTATUS: TcxGridDBColumn;
    ViewBufPrNUMDOC: TcxGridDBColumn;
    ViewBufPrDATEDOC: TcxGridDBColumn;
    ViewBufPrNAMEC: TcxGridDBColumn;
    ViewBufPrPRICE: TcxGridDBColumn;
    FormPlacement1: TFormPlacement;
    cxLabel2: TcxLabel;
    acBufPrice: TActionManager;
    acExit: TAction;
    Memo1: TcxMemo;
    acLoadCash: TAction;
    cxDateEdit1: TcxDateEdit;
    Label1: TLabel;
    Label2: TLabel;
    cxDateEdit2: TcxDateEdit;
    cxButton2: TcxButton;
    acPeriod: TAction;
    cxCheckBox1: TcxCheckBox;
    ViewBufPrOLDP: TcxGridDBColumn;
    taLoad: TdxMemData;
    taLoadIdCard: TStringField;
    acPrintCenn: TAction;
    cxLabel3: TcxLabel;
    cxCheckBox2: TcxCheckBox;
    frBarCodeObject1: TfrBarCodeObject;
    cxLabel4: TcxLabel;
    PopupMenu1: TPopupMenu;
    acSetNo: TAction;
    acSeOtloj: TAction;
    N1: TMenuItem;
    N3: TMenuItem;
    acSetOk: TAction;
    N2: TMenuItem;
    acMoveT: TAction;
    N4: TMenuItem;
    N5: TMenuItem;
    acRemnT: TAction;
    N6: TMenuItem;
    Gr1: TcxGrid;
    Vi1: TcxGridDBTableView;
    Vi1IDSKL: TcxGridDBColumn;
    Vi1Name: TcxGridDBColumn;
    Vi1DATEDOC: TcxGridDBColumn;
    Vi1NUMDOC: TcxGridDBColumn;
    Vi1QUANT: TcxGridDBColumn;
    Vi1PRICEIN: TcxGridDBColumn;
    Vi1PRICEUCH: TcxGridDBColumn;
    Vi1SUMIN: TcxGridDBColumn;
    Vi1SUMUCH: TcxGridDBColumn;
    ViCenn: TcxGridDBTableView;
    ViCennRecId: TcxGridDBColumn;
    ViCennIdCard: TcxGridDBColumn;
    ViCennFullName: TcxGridDBColumn;
    ViCennEdIzm: TcxGridDBColumn;
    ViCennPrice1: TcxGridDBColumn;
    ViCennCountry: TcxGridDBColumn;
    ViCennBarCode: TcxGridDBColumn;
    ViCennQuant: TcxGridDBColumn;
    le1: TcxGridLevel;
    Le2: TcxGridLevel;
    cxButton1: TcxButton;
    ViewBufPrReserv1: TcxGridDBColumn;
    acPrintCen: TAction;
    N7: TMenuItem;
    N8: TMenuItem;
    cxLabel5: TcxLabel;
    acPost: TAction;
    acAddToScale: TAction;
    N9: TMenuItem;
    acFindPluNum: TAction;
    N10: TMenuItem;
    ViewBufPrCLINAME1: TcxGridDBColumn;
    ViewBufPrIDATE: TcxGridDBColumn;
    ViewBufPrISKL: TcxGridDBColumn;
    ViewBufPrID: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acExitExecute(Sender: TObject);
    procedure acLoadCashExecute(Sender: TObject);
    procedure cxLabel2Click(Sender: TObject);
    procedure acPeriodExecute(Sender: TObject);
    procedure acPrintCennExecute(Sender: TObject);
    procedure cxLabel3Click(Sender: TObject);
    procedure cxLabel4Click(Sender: TObject);
    procedure acSetNoExecute(Sender: TObject);
    procedure acSetOkExecute(Sender: TObject);
    procedure acSeOtlojExecute(Sender: TObject);
    procedure acMoveTExecute(Sender: TObject);
    procedure acRemnTExecute(Sender: TObject);
    procedure ViewBufPrSelectionChanged(Sender: TcxCustomGridTableView);
    procedure cxButton1Click(Sender: TObject);
    procedure acPrintCenExecute(Sender: TObject);
    procedure cxLabel5Click(Sender: TObject);
    procedure acPostExecute(Sender: TObject);
    procedure acAddToScaleExecute(Sender: TObject);
    procedure acFindPluNumExecute(Sender: TObject);
    procedure ViewBufPrCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmBufPrice: TfmBufPrice;

implementation

uses Dm, Un1, PXDB, u2fdk, Cards, MFB, MainMC, Move;

{$R *.dfm}

procedure TfmBufPrice.FormCreate(Sender: TObject);
begin
  FormPlacement1.IniFileName:=sFormIni;
  FormPlacement1.Active:=True;
  GridBufPr.Align:=AlClient;
  ViewBufPr.RestoreFromIniFile(sGridIni);
  Memo1.Clear;
  cxDateEdit1.Date:=Date;
  cxDateEdit2.Date:=Date;
  cxCheckBox1.Checked:=False;
end;

procedure TfmBufPrice.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewBufPr.StoreToIniFile(sGridIni,False);
end;

procedure TfmBufPrice.acExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmBufPrice.acLoadCashExecute(Sender: TObject);
Var sPath,sCashLdd,sMessur,sCSz,sName:String;
    i,j,iNum:INteger;
    Rec:TcxCustomGridRecord;
    rPr:Real;
    fc,fb,fupd:TextFile;
    sPos,sB,sM,sr,sDel:String;
    iShop:INteger;
    iSkl,iDate,Id:INteger;
    bOk:Boolean;
    bAlco,bL:Boolean;
begin
  //�������� ����
  if cxButton1.Enabled then
  begin
    with dmMCS do
    begin
      try
        cxButton1.Enabled:=False;
        cxButton2.Enabled:=False;

        if ViewBufPr.Controller.SelectedRecordCount=0 then
        begin
          cxButton1.Enabled:=True;
          cxButton2.Enabled:=True;
          exit;
        end;

        Memo1.Clear;
        Memo1.Lines.Add('���� �������� ����..');

        bOk:=False;
        sr:=',';

        if CommonSet.UKM4=1 then
        begin
          try
            Memo1.Clear;
            Memo1.Lines.Add('  ����� ���� ������������ ������ ..');

            bAlco:=prTestDepAlco(fmMainMC.Label3.Tag);
            if bAlco then Memo1.Lines.Add('  �������� �������� ���������.') else Memo1.Lines.Add('  �������� �������� ���������.');

            assignfile(fc,CurDir+Person.Name+'\'+'PLUCASH.DAT');
            assignfile(fb,CurDir+Person.Name+'\'+'BAR.DAT');

            //������������ ������
            rewrite(fc);
            rewrite(fb);

            for i:=0 to ViewBufPr.Controller.SelectedRecordCount-1 do
            begin
              Rec:=ViewBufPr.Controller.SelectedRecords[i];

              iNum:=0;
              rPr:=0;
              iSkl:=fmMainMC.Label3.tag;

              for j:=0 to Rec.ValueCount-1 do
              begin
                if ViewBufPr.Columns[j].Name='ViewBufPrIDCARD' then iNum:=Rec.Values[j];
                if ViewBufPr.Columns[j].Name='ViewBufPrNEWP' then rPr:=Rec.Values[j];
                if ViewBufPr.Columns[j].Name='ViewBufPrISKL' then iSkl:=Rec.Values[j];
              end;

              quFCP.Active:=False;
              quFCP.SQL.Clear;
              quFCP.SQL.Add('SELECT * FROM [dbo].[CARDS] ca');
              quFCP.SQL.Add('left join [dbo].[CARDSPRICE] pr on pr.[GOODSID]=ca.[ID] and pr.[IDSKL]='+its(iSkl));
              quFCP.SQL.Add('where ca.[ID]='+its(iNum));
              quFCP.SQL.Add('and [TTOVAR]=0');
              quFCP.Active:=True;

              if (quFCP.RecordCount=1)and(rPr>0.01) then
              begin
                bL:=True;
                if quFCPAVID.AsInteger>0 then if bAlco=False then bL:=False;

                if bL then
                begin
                  quBars.Active:=False;
                  quBars.Parameters.ParamByName('IDC').Value:=iNum;
                  quBars.Active:=True;

                  if quFCPEDIZM.AsInteger=1 then sMessur:='��.' else sMessur:='��.';
                  sName:=quFCPFULLNAME.AsString;

                  sPos:='';

                  if (pos('��',sMessur)>0)or(pos('��',sMessur)>0) then sM:='0.001' else sM:='1';
                  if (quFCPISTATUS.AsInteger<100)and(rPr>0.001) then  sDel:='1' else sDel:='0';
                  sPos:=s1(quFCPID.AsString)+sr+s1(sName)+sr+s1(sMessur)+sr+sM+sr+sr+sr+sr+'0'+sr+'0'+sr+'0'+sr+'NOSIZE'+sr+fAMark(quFCPAVID.AsInteger)+sr+'0'+sr+'0'+sr+'0'+sr+'0'+sr+fs(rv(rPr))+sr+'0'+sr+its(iSkl)+sr+sr+sDel+sr+sr+sr+sr;
                  writeln(fc,AnsiToOemConvert(sPos));
                  Flush(fc);

                  quBars.First;
                  while not quBars.Eof do
                  begin
                    if quBarsBarStatus.AsInteger=0 then sCSz:='NOSIZE'
                    else sCSz:='QUANTITY';

                    sB:=quBarsBar.AsString+sr+quBarsGoodsID.AsString+sr+sCSz+sr+fs(quBarsQuant.AsFloat)+sr;
                    writeln(fb,AnsiToOemConvert(sB));
                    Flush(fb);

                    quBars.Next;
                  end;
                  quBars.Active:=False;

                  prAddCashLoadHist(iNum,iSkl,1,rPr,0);
                end;
              end;

              quFCP.Active:=False;
            end;

            closefile(fc);
            closefile(fb);
            delay(100);

            //����� ������� - ����� ������� � ���������� �����
            iShop:=fShopDep(fmMainMC.Label3.Tag);

            if DirectoryExists(CommonSet.POSLoad+its(iShop)) then
            begin
              sPath:=CommonSet.POSLoad+its(iShop)+'\';

              if Fileexists(sPath+'PLUCASH.DAT') or Fileexists(sPath+'BAR.DAT') then
              begin
                Memo1.Lines.Add('');
                Memo1.Lines.Add('  ����� ��� �� ���������� ���������� ������...');
                Memo1.Lines.Add('  �������� ����������!!!');
                Memo1.Lines.Add('  ���������� �����...');
                Memo1.Lines.Add('');
              end else
              begin
                if Fileexists(sPath+CashNon) or Fileexists(sPath+sCashLdd) then
                begin
                  Memo1.Lines.Add('');
                  Memo1.Lines.Add('  ����� ������������ ������.');
                  Memo1.Lines.Add('  �������� ����������!!!');
                  Memo1.Lines.Add('  ���������� �����...');
                  Memo1.Lines.Add('');
                end else
                begin //��� ����� ���������� �������� �����
                  MoveFile(PChar(CurDir+Person.Name+'\'+'PLUCASH.DAT'),PChar(sPath+'PLUCASH.DAT'));
                  MoveFile(PChar(CurDir+Person.Name+'\'+'BAR.DAT'),PChar(sPath+'BAR.DAT'));
                  assignfile(fupd,sPath+CashUpd);
                  rewrite(fupd);
                  closefile(fupd);

                  bOk:=True;
//                  Memo1.Lines.Add('�������� ��������� ��.');
                end;
              end;
            end else Memo1.Lines.Add('������ �������� . ��� ����� �������� - '+CommonSet.POSLoad+its(iShop));
          except
            Memo1.Lines.Add('������ ��� ��������. ��������� �����.');
          end;
        end;

        if bOk then
        begin
          //��������� - ����� �������
          for i:=0 to ViewBufPr.Controller.SelectedRecordCount-1 do
          begin
            Rec:=ViewBufPr.Controller.SelectedRecords[i];

            iSkl:=fmMainMC.Label3.tag;
            iDate:=0;
            Id:=0;
            rPr:=0;
            iNum:=0;

            for j:=0 to Rec.ValueCount-1 do
            begin
              if ViewBufPr.Columns[j].Name='ViewBufPrIDATE' then iDate:=Rec.Values[j];
              if ViewBufPr.Columns[j].Name='ViewBufPrNEWP' then rPr:=Rec.Values[j];
              if ViewBufPr.Columns[j].Name='ViewBufPrISKL' then iSkl:=Rec.Values[j];
              if ViewBufPr.Columns[j].Name='ViewBufPrID' then iD:=Rec.Values[j];
              if ViewBufPr.Columns[j].Name='ViewBufPrIDCARD' then iNum:=Rec.Values[j];
            end;


            //���������� ��� ����� � ��������
            quE.Active:=False;
            quE.SQL.Clear;
            quE.SQL.Add('Update [dbo].[BUFPRICE] Set [STATUS]=1');
            quE.SQL.Add('where [ISKL]='+its(iSkl));
            quE.SQL.Add('and [IDATE]='+its(iDate));
            quE.SQL.Add('and [ID]='+its(Id));
            quE.ExecSQL;

            //��������
            quProc.SQL.Clear;
            quProc.SQL.Add('EXECUTE [dbo].[fSetPRICE] '+its(iNum)+','+its(iSkl)+','+fs(rv(rPr)));
            quProc.ExecSQL;
            delay(33);
            
           {
            quE.Active:=False;
            quE.SQL.Clear;
            quE.SQL.Add('UPDATE [dbo].[CARDSPRICE]');
            quE.SQL.Add('SET [PRICE]='+fs(rv(rPr)));
            quE.SQL.Add('WHERE  [GOODSID]='+its(iNum));
            quE.SQL.Add('and [IDSKL]='+its(iSkl));
            quE.ExecSQL;


            //����� ����� � �����
            quE.Active:=False;
            quE.SQL.Clear;
            quE.SQL.Add('Update "ScalePLU" Set ');
            quE.SQL.Add('Price='+its(RoundEx(rNPr*100))+',');
            quE.SQL.Add('Status=0');
            quE.SQL.Add('where GoodsItem='+its(iNum));
            quE.ExecSQL;
             }
          end;
          quBuffPr.Requery();

          ViewBufPr.Controller.ClearSelection;
        end;
        Memo1.Lines.Add('�������� ��������� ��.');
      finally
        cxButton1.Enabled:=True;
        cxButton2.Enabled:=True;
      end;
    end;
  end else
  begin
    showmessage('��������� ���������� ����������� ��������...');
  end;
end;

procedure TfmBufPrice.cxLabel2Click(Sender: TObject);
begin
  acLoadCash.Execute;
end;

procedure TfmBufPrice.acPeriodExecute(Sender: TObject);
begin
  //����� ��������� ���
  with dmMCS do
  begin
    fmBufPrice.ViewBufPr.BeginUpdate;
    quBuffPr.Active:=False;
    quBuffPr.Parameters.ParamByName('ISKL').Value:=fmMainMC.Label3.Tag;
    quBuffPr.Parameters.ParamByName('DATEB').Value:=Trunc(cxDateEdit1.Date);
    quBuffPr.Parameters.ParamByName('DATEE').Value:=Trunc(cxDateEdit2.Date);
    if cxCheckBox1.Checked then
      quBuffPr.Parameters.ParamByName('IST').Value:=2 //���������� ���
    else
      quBuffPr.Parameters.ParamByName('IST').Value:=0; //������ �������������
      
    quBuffPr.Active:=True;
    fmBufPrice.ViewBufPr.EndUpdate;
  end;
end;

procedure TfmBufPrice.acPrintCennExecute(Sender: TObject);
Var
    i,j,iNum:INteger;
    Rec:TcxCustomGridRecord;
    vPP:TfrPrintPages;
    rNPr:Real;
    iSkl:INteger;
begin
  //������ ��������
  if cxButton1.Enabled then
  begin
    with dmMCS do
    begin
      try
        cxButton1.Enabled:=False;
        cxButton2.Enabled:=False;

//      �������� ������

        if ViewBufPr.Controller.SelectedRecordCount=0 then
        begin
          cxButton1.Enabled:=True;
          cxButton2.Enabled:=True;
          exit;
        end;

        Memo1.Clear;
        Memo1.Lines.Add('�����, ���� ������������ ..');

        fmCards.ViCenn.BeginUpdate;
        CloseTe(taCen);

        for i:=0 to ViewBufPr.Controller.SelectedRecordCount-1 do
        begin
          Rec:=ViewBufPr.Controller.SelectedRecords[i];

          iNum:=0; rNPr:=0;  iSkl:=fmMainMC.Label3.tag;

          for j:=0 to Rec.ValueCount-1 do
          begin
            if ViewBufPr.Columns[j].Name='ViewBufPrIDCARD' then iNum:=Rec.Values[j];
            if ViewBufPr.Columns[j].Name='ViewBufPrNEWP' then rNPr:=Rec.Values[j];
            if ViewBufPr.Columns[j].Name='ViewBufPrISKL' then iSkl:=Rec.Values[j];
          end;

          if iNum>0 then
          begin
            quFindC4.Active:=False;
            quFindC4.Parameters.ParamByName('IDEP').Value:=iSkl;
            quFindC4.Parameters.ParamByName('IDC').Value:=iNum;
            quFindC4.Active:=True;
            if quFindC4.RecordCount>0 then
            begin
              taCen.Append;
              taCenIdCard.AsInteger:=iNum;
              taCenFullName.AsString:=quFindC4FullName.AsString;
              taCenCountry.AsString:=quFindC4NameCu.AsString;
              taCenPrice1.AsFloat:=RV(rNPr);
              taCenPrice2.AsFloat:=0;
              taCenDiscount.AsFloat:=0;
              taCenBarCode.AsString:=quFindC4BARCODE.AsString;
              taCenEdIzm.AsInteger:=quFindC4EDIZM.AsInteger;
              if quFindC4EdIzm.AsInteger=2 then  taCenPluScale.AsString:=prFindPlu(iNum,fmMainMC.Label3.tag) else taCenPluScale.AsString:='';
              taCenReceipt.AsString:=quFindC4SOSTAV.AsString;
              taCenDepName.AsString:=fmMainMC.Label3.Caption;
              taCensDisc.AsString:=prSDisc(iNum,fmMainMC.Label3.tag);
              taCenScaleKey.AsInteger:=quFindC4VESBUTTON.AsInteger;
              taCensDate.AsString:=ds1(fmMainMC.cxDEdit1.Date);
//            taCenV02.AsInteger:=quFindC4V02.AsInteger;
//            taCenV12.AsInteger:=quFindC4V12.AsInteger;
//            taCenMaker.AsString:=quFindC4NAMEM.AsString;
              taCen.Post;
            end;
            quFindC4.Active:=False;
          end;

        end;
        Memo1.Lines.Add('������������ ��.');
        Memo1.Lines.Add('������ ...');

        if quLabs.Locate('ID',fmMainMC.cxLookupComboBox1.EditValue,[]) then
        begin
          if FileExists(CommonSet.PathReport+quLabsNameF.AsString) then
          begin
            RepCenn.LoadFromFile(CommonSet.PathReport+quLabsNameF.AsString);
            RepCenn.ReportName:='�������.';
            RepCenn.PrepareReport;

            vPP:=frAll;
            if cxCheckBox2.Checked then RepCenn.ShowPreparedReport
            else RepCenn.PrintPreparedReport('',1,False,vPP);

          end else showmessage('���� �� ������ : '+CommonSet.PathReport+quLabsNameF.AsString);
        end;
        CloseTe(taCen);
        fmMainMC.cxDEdit1.Date:=Date;
      finally
        cxButton1.Enabled:=True;
        cxButton2.Enabled:=True;
        fmCards.ViCenn.EndUpdate;
      end;
    end;
  end else
  begin
    showmessage('��������� ���������� ����������� ��������...');
  end;
end;

procedure TfmBufPrice.cxLabel3Click(Sender: TObject);
begin
  acPrintCenn.Execute;
end;

procedure TfmBufPrice.cxLabel4Click(Sender: TObject);
begin
  //������� � Excel
  prNExportExel6(ViewBufPr);
end;

procedure TfmBufPrice.acSetNoExecute(Sender: TObject);
Var
    sStatus:String;
    i,j,iNum:INteger;
    Rec:TcxCustomGridRecord;
    iSkl,iDate,Id:INteger;
//    rPr:Real;
    rOPr:Real;

begin
  //���������� - �����������
  if not CanDo('prBufSetNo') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;

  with dmMCS do
  begin
    try
      for i:=0 to ViewBufPr.Controller.SelectedRecordCount-1 do
      begin
        Rec:=ViewBufPr.Controller.SelectedRecords[i];

        iSkl:=fmMainMC.Label3.tag;
        iDate:=0;
        Id:=0;
//        rPr:=0;
        rOPr:=0;
        iNum:=0;
        sStatus:='';

        for j:=0 to Rec.ValueCount-1 do
        begin
          if ViewBufPr.Columns[j].Name='ViewBufPrIDATE' then iDate:=Rec.Values[j];
//          if ViewBufPr.Columns[j].Name='ViewBufPrNEWP' then rPr:=Rec.Values[j];
          if ViewBufPr.Columns[j].Name='ViewBufPrOLDP' then rOPr:=Rec.Values[j];
          if ViewBufPr.Columns[j].Name='ViewBufPrISKL' then iSkl:=Rec.Values[j];
          if ViewBufPr.Columns[j].Name='ViewBufPrID' then iD:=Rec.Values[j];
          if ViewBufPr.Columns[j].Name='ViewBufPrIDCARD' then iNum:=Rec.Values[j];
          if ViewBufPr.Columns[j].Name='ViewBufPrSTATUS' then sStatus:=Rec.Values[j];
        end;

        if iNum>0 then
        begin
          //���������� ��� �����
          quE.Active:=False;
          quE.SQL.Clear;
          quE.SQL.Add('Update [dbo].[BUFPRICE] Set [STATUS]=0');
          quE.SQL.Add('where [ISKL]='+its(iSkl));
          quE.SQL.Add('and [IDATE]='+its(iDate));
          quE.SQL.Add('and [ID]='+its(Id));
          quE.ExecSQL;

          if pos('1',sStatus)>0 then
          begin
             //��������     - �� ������ ����
            quE.Active:=False;
            quE.SQL.Clear;
            quE.SQL.Add('UPDATE [dbo].[CARDSPRICE]');
            quE.SQL.Add('SET [PRICE]='+fs(rv(rOPr)));
            quE.SQL.Add('WHERE  [GOODSID]='+its(iNum));
            quE.SQL.Add('and [IDSKL]='+its(iSkl));
            quE.ExecSQL;
          end;
        end;
      end;
      ViewBufPr.Controller.ClearSelection;
      quBuffPr.Requery();
    finally
    end;
  end;
end;

procedure TfmBufPrice.acSetOkExecute(Sender: TObject);
Var
    i,j,iNum:INteger;
    Rec:TcxCustomGridRecord;
    iSkl,iDate,Id:INteger;
begin
  //���������� - ���������
  if not CanDo('prBufSetOk') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;

  with dmMCS do
  begin
    try
      for i:=0 to ViewBufPr.Controller.SelectedRecordCount-1 do
      begin
        Rec:=ViewBufPr.Controller.SelectedRecords[i];

        iSkl:=fmMainMC.Label3.tag;
        iDate:=0;
        Id:=0;
        iNum:=0;

        for j:=0 to Rec.ValueCount-1 do
        begin
          if ViewBufPr.Columns[j].Name='ViewBufPrIDATE' then iDate:=Rec.Values[j];
          if ViewBufPr.Columns[j].Name='ViewBufPrISKL' then iSkl:=Rec.Values[j];
          if ViewBufPr.Columns[j].Name='ViewBufPrID' then iD:=Rec.Values[j];
          if ViewBufPr.Columns[j].Name='ViewBufPrIDCARD' then iNum:=Rec.Values[j];
        end;

        if iNum>0 then
        begin
          //���������� ��� �����
          quE.Active:=False;
          quE.SQL.Clear;
          quE.SQL.Add('Update [dbo].[BUFPRICE] Set [STATUS]=1');
          quE.SQL.Add('where [ISKL]='+its(iSkl));
          quE.SQL.Add('and [IDATE]='+its(iDate));
          quE.SQL.Add('and [ID]='+its(Id));
          quE.ExecSQL;
        end;
      end;

      ViewBufPr.Controller.ClearSelection;
      quBuffPr.Requery();
    finally
    end;
  end;
end;

procedure TfmBufPrice.acSeOtlojExecute(Sender: TObject);
Var
    i,j,iNum:INteger;
    Rec:TcxCustomGridRecord;
    iSkl,iDate,Id:INteger;

begin
  //���������� - �������
  if not CanDo('prBufSetOtl') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;

  with dmMCS do
  begin
    try
      for i:=0 to ViewBufPr.Controller.SelectedRecordCount-1 do
      begin
        Rec:=ViewBufPr.Controller.SelectedRecords[i];

        iSkl:=fmMainMC.Label3.tag;
        iDate:=0;
        Id:=0;
        iNum:=0;

        for j:=0 to Rec.ValueCount-1 do
        begin
          if ViewBufPr.Columns[j].Name='ViewBufPrIDATE' then iDate:=Rec.Values[j];
          if ViewBufPr.Columns[j].Name='ViewBufPrISKL' then iSkl:=Rec.Values[j];
          if ViewBufPr.Columns[j].Name='ViewBufPrID' then iD:=Rec.Values[j];
          if ViewBufPr.Columns[j].Name='ViewBufPrIDCARD' then iNum:=Rec.Values[j];
        end;

        if iNum>0 then
        begin
          //���������� ��� �����
          quE.Active:=False;
          quE.SQL.Clear;
          quE.SQL.Add('Update [dbo].[BUFPRICE] Set [STATUS]=2');
          quE.SQL.Add('where [ISKL]='+its(iSkl));
          quE.SQL.Add('and [IDATE]='+its(iDate));
          quE.SQL.Add('and [ID]='+its(Id));
          quE.ExecSQL;
        end;
      end;

      ViewBufPr.Controller.ClearSelection;
      quBuffPr.Requery();
    finally
    end;
  end;
end;

procedure TfmBufPrice.acMoveTExecute(Sender: TObject);
begin
//��������������
  if not CanDo('prViewMoveCard') then begin StatusBar1.Panels[0].Text:='��� ����.';  exit end;
    //�������� ������ - ����� �� ��������� �� �����
  with dmMCS do
  begin
    if quBuffPr.RecordCount>0 then
    begin
      prFormMove(quBuffPrISKL.AsInteger,quBuffPrIDCARD.AsInteger,Trunc(CommonSet.DateBeg),Trunc(CommonSet.DateEnd),quBuffPrNAMEC.AsString);
      fmMove.ShowModal;
    end;
  end;
end;

procedure TfmBufPrice.acRemnTExecute(Sender: TObject);
begin
  with dmMCS do
  begin
    if quBuffPr.RecordCount>0 then
    begin
      {
      fmRemn.cxButton1.Enabled:=False;
      quRemn.Active:=False;
      delay(20);
      fmRemn.ViewR.BeginUpdate;
      quRemn.ParamByName('IDCARD').AsInteger:=quBuffPrIDCARD.AsInteger;
      quRemn.Active:=True;
      fmRemn.ViewR.EndUpdate;

      fmRemn.Caption:='�������: '+quBuffPrIDCARD.AsString+'  '+quBuffPrName.AsString;
      fmRemn.cxButton1.Enabled:=True;
      fmRemn.Show;}
    end;  
  end;
end;

procedure TfmBufPrice.ViewBufPrSelectionChanged(
  Sender: TcxCustomGridTableView);
begin
  if ViewBufPr.Controller.SelectedRecordCount>1 then exit;
  with dmMCS do
  begin
    Vi1.BeginUpdate;
    quPosts1.Active:=False;
    quPosts1.Parameters.ParamByName('ICODE').Value:=quBuffPrIDCARD.AsInteger;
    quPosts1.Parameters.ParamByName('ISKL').Value:=quBuffPrISKL.AsInteger;
    quPosts1.Active:=True;
    Vi1.EndUpdate;
  end;
end;

procedure TfmBufPrice.cxButton1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmBufPrice.acPrintCenExecute(Sender: TObject);
Var j,iNum:INteger;
    Rec:TcxCustomGridRecord;
    iSkl:INteger;
    sBar,sCountry,sName,sTehno:String;
begin
  if ViewBufPr.Controller.SelectedRecordCount=0 then exit;
  try
   //���� ������� ���� , ������� �� ���������
    with dmMCS do
    begin
      Rec:=ViewBufPr.Controller.SelectedRecords[0];

      iNum:=0;
      iSkl:=fmMainMC.Label3.tag;

      for j:=0 to Rec.ValueCount-1 do
      begin
        if ViewBufPr.Columns[j].Name='ViewBufPrIDCARD' then iNum:=Rec.Values[j];
        if ViewBufPr.Columns[j].Name='ViewBufPrISKL' then iSkl:=Rec.Values[j];
      end;

      if (iNum>0)and(iNum<>CommonSet.CutTailCode) then //�������� �� ������� ������ ����� �� �������
      begin
        quFindC4.Active:=False;
        quFindC4.Parameters.ParamByName('IDEP').Value:=iSkl;
        quFindC4.Parameters.ParamByName('IDC').Value:=iNum;
        quFindC4.Active:=True;
        if quFindC4.RecordCount>0 then
        begin
          sBar:=quFindC4BarCode.AsString;
          if pos('22',sBar)=1 then //��� ������� �����
          begin
            while length(sBar)<12 do sBar:=sBar+'0';
            sBar:=ToStandart(sBar);
          end;

          sName:=quFindC4FullName.AsString;
          sTehno:=its(iNum);
          sCountry:=quFindC4NameCu.AsString;

          if FileExists(CurDir + 'LabelBar.frf') then
          begin
            RepCenn.LoadFromFile(CurDir + 'LabelBar.frf');

            frVariables.Variable['SNAME']:=sName;
            frVariables.Variable['SBAR']:=sBar;
            frVariables.Variable['STEHNO']:=sTehno;
            frVariables.Variable['COUNTRY']:=sCountry;

            RepCenn.ReportName:='������ ��������.';
            RepCenn.PrepareReport;
            RepCenn.ShowPreparedReport;
          end else Showmessage('���� ������� �������� : '+CurDir + 'LabelBar.frf'+' �� ������.');
        end;
        quFindC4.Active:=False;
      end;
    end;
  finally
  end;
end;

procedure TfmBufPrice.cxLabel5Click(Sender: TObject);
begin
  acPrintCen.Execute;
end;

procedure TfmBufPrice.acPostExecute(Sender: TObject);
begin
  //���������� ������
  if not CanDo('prViewPostCard') then begin StatusBar1.Panels[0].Text:='��� ����.';  exit end;
  with dmMCS do
  begin
    if not quBuffPr.Eof then
    begin
      {
      fmMove.ViewP.BeginUpdate;
      fmMove.GridM.Tag:=quBuffPrIDCARD.AsInteger;
      quPost.Active:=False;
      quPost.ParamByName('IDCARD').Value:=quBuffPrIDCARD.AsInteger;
      quPost.ParamByName('DATEB').Value:=CommonSet.DateBeg;
      quPost.ParamByName('DATEE').Value:=CommonSet.DateEnd;
      quPost.Active:=True;
      quPost.First;

      fmMove.ViewP.EndUpdate;

      fmMove.Caption:=quBuffPrName.AsString+': ��������� �� ������ � '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateBeg)+' �� '+FormatDateTime('dd.mm.yyyy ',CommonSet.DateEnd);

      fmMove.LevelP.Visible:=True;
      fmMove.LevelM.Visible:=False;

      fmMove.Show;}
    end;
  end;
end;

procedure TfmBufPrice.acAddToScaleExecute(Sender: TObject);
{Var
    i,j,iNum:INteger;
    Rec:TcxCustomGridRecord;
    rPr:Real;}
begin
  //�������� ����� � ����
  {
  fmAddInScale:=TfmAddInScale.Create(Application);
  with dmMCS do
  begin
    if ViewBufPr.Controller.SelectedRecordCount=0 then exit;

    CloseTe(taToScale);

    for i:=0 to ViewBufPr.Controller.SelectedRecordCount-1 do
    begin
      Rec:=ViewBufPr.Controller.SelectedRecords[i];

      iNum:=0;
      rPr:=0;

      for j:=0 to Rec.ValueCount-1 do
      begin
        if ViewBufPr.Columns[j].Name='ViewBufPrIDCARD' then iNum:=Rec.Values[j];
        if ViewBufPr.Columns[j].Name='ViewBufPrNEWP' then rPr:=Rec.Values[j];
      end;

      if (iNum>0) then
      begin
        quFindC4.Active:=False;
        quFindC4.ParamByName('IDC').AsInteger:=iNum;
        quFindC4.Active:=True;
        if quFindC4.RecordCount>0 then
        begin
          if quFindC4TovarType.AsInteger=1 then   //������� �����
          begin
            taToScale.Append;
            taToScaleId.AsInteger:=iNum;
            taToScaleBarcode.AsString:=quFindC4BarCode.AsString;
            taToScaleName.AsString:=quFindC4FullName.AsString;
            taToScaleSrok.AsInteger:=quFindC4SrokReal.AsInteger;
            taToScalePrice.AsFloat:=rPr;
            taToScale.Post;
          end;
        end;
        quFindC4.Active:=False;
      end;
    end;
    quScSpr.Active:=False;
    quScSpr.Active:=True;
    fmAddInScale.Label1.Caption:='�������� - ' +its(taToScale.RecordCount)+' ������� ��������.';

    fmAddInScale.ShowModal;
    if fmAddInScale.ModalResult=mrOk then  prAddToScale(quScSprNumber.AsString);

    quScSpr.Active:=False;
    taToScale.Active:=False;
    ViewBufPr.Controller.ClearSelection;
  end;
  fmAddInScale.Release;}
end;

procedure TfmBufPrice.acFindPluNumExecute(Sender: TObject);
{Var iNum,j:INteger;
    Rec:TcxCustomGridRecord;
    Str1:String;}
begin
 //� �����
  {
  if ViewBufPr.Controller.SelectedRecordCount>0 then
  begin
    Rec:=ViewBufPr.Controller.SelectedRecords[0];

    iNum:=0;
     for j:=0 to Rec.ValueCount-1 do
     begin
       if ViewBufPr.Columns[j].Name='ViewBufPrIDCARD' then begin iNum:=Rec.Values[j]; break; end;
     end;

     Str1:=prFindPlu(iNum);

     showmessage('��� ������ '+INtToStr(iNum)+' � �����: '+Str1);
  end;
  }
end;

procedure TfmBufPrice.ViewBufPrCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
Var rPrM,rPrN:Real;
    i:SmallINt;
begin
  rPrM:=0;
  rPrN:=0;

  for i:=0 to ViewBufPr.ColumnCount-1 do
  begin
    if ViewBufPr.Columns[i].Name='ViewBufPrPRICE' then
      rPrM:=VarAsType(AViewInfo.GridRecord.DisplayTexts[i],varDouble);
    if ViewBufPr.Columns[i].Name='ViewBufPrNEWP' then
      rPrN:=VarAsType(AViewInfo.GridRecord.DisplayTexts[i],varDouble);
  end;

  if (rPrM-rPrN)>0.01 then
  begin
    ACanvas.Canvas.Brush.Color := $00BFFFBF; //��� �����������
    ACanvas.Font.Color:=clGreen;
  end;
end;

end.
