unit Period1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, dxfBackGround, ComCtrls, ExtCtrls, Menus, cxLookAndFeelPainters,
  StdCtrls, cxButtons, cxControls, cxContainer, cxEdit, cxTextEdit,
  cxMaskEdit, cxDropDownEdit, cxCalendar;

type
  TfmPeriod1 = class(TForm)
    StatusBar1: TStatusBar;
    dxfBackGround1: TdxfBackGround;
    Panel1: TPanel;
    cxDateEdit1: TcxDateEdit;
    cxDateEdit2: TcxDateEdit;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    procedure cxDateEdit1PropertiesChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmPeriod1: TfmPeriod1;

implementation

{$R *.dfm}

procedure TfmPeriod1.cxDateEdit1PropertiesChange(Sender: TObject);
begin
  if cxDateEdit1.Date>cxDateEdit2.Date then cxDateEdit2.Date:=cxDateEdit1.Date; 
end;

end.
