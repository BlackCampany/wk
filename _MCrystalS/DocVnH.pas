unit DocVnH;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxCalendar, cxImageComboBox,
  Placemnt, SpeedBar, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxControls, cxGridCustomView, cxGrid,
  cxContainer, cxTextEdit, cxMemo, ExtCtrls, ComCtrls, ActnList,
  XPStyleActnCtrls, ActnMan;

type
  TfmDocsVnH = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Memo1: TcxMemo;
    Gr1: TcxGrid;
    Vi1: TcxGridDBTableView;
    Vi1Name: TcxGridDBColumn;
    Vi1NameCli: TcxGridDBColumn;
    Vi1DateInvoice: TcxGridDBColumn;
    Vi1Number: TcxGridDBColumn;
    Vi1Kol: TcxGridDBColumn;
    Vi1CenaTovar: TcxGridDBColumn;
    Vi1Procent: TcxGridDBColumn;
    Vi1NewCenaTovar: TcxGridDBColumn;
    Vi1SumCenaTovarPost: TcxGridDBColumn;
    Vi1SumCenaTovar: TcxGridDBColumn;
    le1: TcxGridLevel;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    SpeedItem6: TSpeedItem;
    SpeedItem7: TSpeedItem;
    SpeedItem8: TSpeedItem;
    SpeedItem9: TSpeedItem;
    SpeedItem10: TSpeedItem;
    fpfmDocsVnH: TFormPlacement;
    GridDocsVn: TcxGrid;
    ViewDocsVn: TcxGridDBTableView;
    LevelDocsVn: TcxGridLevel;
    ViewDocsVnIDSKLFROM: TcxGridDBColumn;
    ViewDocsVnIDSKLTO: TcxGridDBColumn;
    ViewDocsVnIDATEDOC: TcxGridDBColumn;
    ViewDocsVnID: TcxGridDBColumn;
    ViewDocsVnNUMDOC: TcxGridDBColumn;
    ViewDocsVnSUMIN: TcxGridDBColumn;
    ViewDocsVnSUMIN0: TcxGridDBColumn;
    ViewDocsVnSUMR: TcxGridDBColumn;
    ViewDocsVnIACTIVE: TcxGridDBColumn;
    ViewDocsVnSumD: TcxGridDBColumn;
    ViewDocsVnNAMESKLFROM: TcxGridDBColumn;
    ViewDocsVnNAMESKLTO: TcxGridDBColumn;
    amVnDocs: TActionManager;
    acPeriod: TAction;
    acAddDoc: TAction;
    acEditDoc: TAction;
    acViewDoc: TAction;
    acDelDoc: TAction;
    acOnVn: TAction;
    acOffVn: TAction;
    ViewDocsVnCOMMENT: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acPeriodExecute(Sender: TObject);
    procedure SpeedItem10Click(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure acAddDocExecute(Sender: TObject);
    procedure acEditDocExecute(Sender: TObject);
    procedure acViewDocExecute(Sender: TObject);
    procedure ViewDocsVnCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure acDelDocExecute(Sender: TObject);
    procedure ViewDocsVnDblClick(Sender: TObject);
    procedure acOnVnExecute(Sender: TObject);
    procedure acOffVnExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure prSetValsAddDocVn(iT:SmallINt);
  end;

Function OprDocsVn(IDH:integer; Memo1:TcxMemo):Boolean;
Function OtkatDocsVn(IDH:integer; Memo1:TcxMemo):Boolean;

var
  fmDocsVnH: TfmDocsVnH;

implementation

uses Un1, Dm, Period1, MainMC, DocVnS, DocAcH;

{$R *.dfm}

Function OtkatDocsVn(IDH:integer; Memo1:TcxMemo):Boolean;
Var bOpr:Boolean;
begin
  Result:=False;

  Memo1.Lines.Add('');
  Memo1.Lines.Add(' ����� ... ���� ����� ���������.');

  if CanDo('prOffDocVn') then
  begin
    with dmMCS do
    begin
      bOpr:=True;

      //��� �� ���� �������� �� ��������

      if quVnH.Locate('ID',IDH,[])=False then bOpr:=False;

      if bOpr then
      begin
        quProc.SQL.Clear;
        quProc.SQL.Add('declare @IDHEAD int = '+its(quVnHID.AsInteger));
        quProc.SQL.Add('declare @IDATE int = '+its(quVnHIDATEDOC.AsInteger));
        quProc.SQL.Add('declare @ISKL int = '+its(quVnHIDSKLFROM.AsInteger));
        quProc.SQL.Add('declare @IST int = 1');
        quProc.SQL.Add('EXECUTE [dbo].[prSetStatusVn] @IDHEAD,@IDATE,@ISKL,@IST');
        quProc.ExecSQL;
        delay(33);
        Result:=True;
        Memo1.Lines.Add(' ������� ��������.');
        delay(10);
      end;
    end;
  end else Memo1.Lines.Add('��� ����');
end;


Function OprDocsVn(IDH:integer; Memo1:TcxMemo):Boolean;
Var bOpr:Boolean;
begin
  Result:=False;

  Memo1.Lines.Add('');
  Memo1.Lines.Add(' ����� ... ���� ������������� ���������.');

  if CanDo('prOnDocVn') then
  begin
    with dmMCS do
    begin
      bOpr:=True;

      //��� �� ���� �������� �� ��������

      if quVnH.Locate('ID',IDH,[])=False then bOpr:=False;

      if bOpr then
      begin
        quProc.SQL.Clear;
        quProc.SQL.Add('declare @IDHEAD int = '+its(quVnHID.AsInteger));
        quProc.SQL.Add('declare @IDATE int = '+its(quVnHIDATEDOC.AsInteger));
        quProc.SQL.Add('declare @ISKL int = '+its(quVnHIDSKLFROM.AsInteger));
        quProc.SQL.Add('declare @IST int = 3');
        quProc.SQL.Add('EXECUTE [dbo].[prSetStatusVn] @IDHEAD,@IDATE,@ISKL,@IST');
        quProc.ExecSQL;
        delay(33);
        Result:=True;
        Memo1.Lines.Add(' ������� ��������.');
        delay(10);
      end;
    end;
  end else Memo1.Lines.Add('��� ����');
end;


procedure TfmDocsVnH.prSetValsAddDocVn(iT:SmallINt); //0-����������, 1-��������������, 2-��������
// var user:string;
begin
  with dmMCS do
  begin
    if iT=0 then
    begin
      fmDocsVnS.Caption:='���������: ����������.';

      fmDocsVnS.Tag:=0;

      fmDocsVnS.cxTextEdit1.Text:=fGetNumDoc(fmMainMC.Label3.tag,3);  fmDocsVnS.cxTextEdit1.Properties.ReadOnly:=True;
      fmDocsVnS.cxTextEdit2.Text:='';  fmDocsVnS.cxTextEdit2.Properties.ReadOnly:=False;
      fmDocsVnS.cxDateEdit1.Date:=date;  fmDocsVnS.cxDateEdit1.Properties.ReadOnly:=False;

      quDepartsSt.Active:=False; quDepartsSt.Parameters.ParamByName('IPERS').Value:=Person.Id; quDepartsSt.Active:=True; quDepartsSt.Locate('ID',fmMainMC.Label3.tag,[]);

      fmDocsVnS.cxLookupComboBox1.EditValue:=quDepartsStID.AsInteger;
      fmDocsVnS.cxLookupComboBox1.Text:=quDepartsStName.AsString;
      fmDocsVnS.cxLookupComboBox1.Properties.ReadOnly:=False;

      fmDocsVnS.cxLabel5.Enabled:=True;
      fmDocsVnS.cxLabel1.Enabled:=True;
      fmDocsVnS.cxLabel2.Enabled:=True;
      fmDocsVnS.cxLabel6.Enabled:=True;
      fmDocsVnS.cxLabel3.Enabled:=True;

      fmDocsVnS.acSaveDoc.Enabled:=True;
      fmDocsVnS.cxButton1.Enabled:=True;

      fmDocsVnS.ViewDoc4.OptionsData.Editing:=True;
      fmDocsVnS.ViewDoc4.OptionsData.Deleting:=True;
    end;
    if iT=1 then
    begin
      fmDocsVnS.Caption:='���������: ��������������.';

      fmDocsVnS.Tag:=quVnHID.AsInteger;

      fmDocsVnS.cxTextEdit1.Text:=quVnHNUMDOC.AsString;  fmDocsVnS.cxTextEdit1.Properties.ReadOnly:=True;
      fmDocsVnS.cxTextEdit2.Text:=quVnHCOMMENT.AsString;  fmDocsVnS.cxTextEdit2.Properties.ReadOnly:=False;
      fmDocsVnS.cxDateEdit1.Date:=quVnHDATEDOC.AsDateTime;  fmDocsVnS.cxDateEdit1.Properties.ReadOnly:=False;

      quDepartsSt.Active:=False; quDepartsSt.Parameters.ParamByName('IPERS').Value:=Person.Id; quDepartsSt.Active:=True; quDepartsSt.Locate('ID',quVnHIDSKLFROM.AsInteger,[]);

      fmDocsVnS.cxLookupComboBox1.EditValue:=quDepartsStID.AsInteger;
      fmDocsVnS.cxLookupComboBox1.Text:=quDepartsStName.AsString;
      fmDocsVnS.cxLookupComboBox1.Properties.ReadOnly:=False;

      fmDocsVnS.cxLabel5.Enabled:=True;
      fmDocsVnS.cxLabel1.Enabled:=True;
      fmDocsVnS.cxLabel2.Enabled:=True;
      fmDocsVnS.cxLabel6.Enabled:=True;
      fmDocsVnS.cxLabel3.Enabled:=True;

      fmDocsVnS.acSaveDoc.Enabled:=True;
      fmDocsVnS.cxButton1.Enabled:=True;

      fmDocsVnS.ViewDoc4.OptionsData.Editing:=True;
      fmDocsVnS.ViewDoc4.OptionsData.Deleting:=True;
    end;

    if iT=2 then
    begin
      fmDocsVnS.Caption:='���������: ��������.';

      fmDocsVnS.Tag:=quVnHID.AsInteger;

      fmDocsVnS.cxTextEdit1.Text:=quVnHNUMDOC.AsString;  fmDocsVnS.cxTextEdit1.Properties.ReadOnly:=True;
      fmDocsVnS.cxTextEdit2.Text:=quVnHCOMMENT.AsString;  fmDocsVnS.cxTextEdit2.Properties.ReadOnly:=True;
      fmDocsVnS.cxDateEdit1.Date:=quVnHDATEDOC.AsDateTime;  fmDocsVnS.cxDateEdit1.Properties.ReadOnly:=True;

      quDepartsSt.Active:=False; quDepartsSt.Parameters.ParamByName('IPERS').Value:=Person.Id; quDepartsSt.Active:=True; quDepartsSt.Locate('ID',quVnHIDSKLFROM.AsInteger,[]);

      fmDocsVnS.cxLookupComboBox1.EditValue:=quDepartsStID.AsInteger;
      fmDocsVnS.cxLookupComboBox1.Text:=quDepartsStName.AsString;
      fmDocsVnS.cxLookupComboBox1.Properties.ReadOnly:=True;

      fmDocsVnS.cxLabel5.Enabled:=False;
      fmDocsVnS.cxLabel1.Enabled:=False;
      fmDocsVnS.cxLabel2.Enabled:=False;
      fmDocsVnS.cxLabel6.Enabled:=False;
      fmDocsVnS.cxLabel3.Enabled:=False;

      fmDocsVnS.acSaveDoc.Enabled:=False;
      fmDocsVnS.cxButton1.Enabled:=False;

      fmDocsVnS.ViewDoc4.OptionsData.Editing:=False;
      fmDocsVnS.ViewDoc4.OptionsData.Deleting:=False;
    end;
  end;
end;


procedure TfmDocsVnH.FormCreate(Sender: TObject);
begin
  fpfmDocsVnH.IniFileName:=sFormIni;
  fpfmDocsVnH.Active:=True; delay(10);
//  Timer1.Enabled:=True;
  GridDocsVn.Align:=AlClient;
  ViewDocsVn.RestoreFromIniFile(sGridIni); delay(10);
  StatusBar1.Color:=$00FFCACA;
end;

procedure TfmDocsVnH.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  ViewDocsVn.StoreToIniFile(sGridIni,False); delay(10);
end;

procedure TfmDocsVnH.acPeriodExecute(Sender: TObject);
begin
  //���������� ���������
  fmPeriod1.cxDateEdit1.Date:=CommonSet.DateBeg;
  fmPeriod1.cxDateEdit2.Date:=CommonSet.DateEnd;
  fmPeriod1.Label3.Caption:='';
  fmPeriod1.ShowModal;
  if fmPeriod1.ModalResult=mrOk then
  begin
    CommonSet.DateBeg:=Trunc(fmPeriod1.cxDateEdit1.Date);
    CommonSet.DateEnd:=Trunc(fmPeriod1.cxDateEdit2.Date);
    with dmMCS do
    begin
      fmDocsVnH.ViewDocsVn.BeginUpdate;
      try
        quVnH.Active:=False;
        quVnH.Parameters.ParamByName('IDATEB').Value:=Trunc(CommonSet.DateBeg);
        quVnH.Parameters.ParamByName('IDATEE').Value:=Trunc(CommonSet.DateEnd);
        quVnH.Parameters.ParamByName('ISKL').Value:=fmMainMC.Label3.tag;
        quVnH.Active:=True;
      finally
        fmDocsVnH.ViewDocsVn.EndUpdate;
      end;
    end;
    fmDocsVnH.Caption:='���������� ��������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd);
  end;  
end;

procedure TfmDocsVnH.SpeedItem10Click(Sender: TObject);
begin
  prNExportExel6(ViewDocsVn);
end;

procedure TfmDocsVnH.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmDocsVnH.acAddDocExecute(Sender: TObject);
begin
  //�������� ��������
  if not CanDo('prAddDocVn') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmMCS do
  begin
    prSetValsAddDocVn(0); //0-����������, 1-��������������, 2-��������

    CloseTe(fmDocsVnS.taSpecVn);
    fmDocsVnS.acSaveDoc.Enabled:=True;
    fmDocsVnS.Show;
  end;
end;

procedure TfmDocsVnH.acEditDocExecute(Sender: TObject);
begin
  //�������������
  if not CanDo('prEditDocVn') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmMCS do
  begin
    if quVnH.RecordCount>0 then //���� ��� �������������
    begin
      if quVnHIACTIVE.AsInteger<3 then
      begin
        prSetValsAddDocVn(1); //0-����������, 1-��������������, 2-��������

        CloseTe(fmDocsVnS.taSpecVn);

        fmDocsVnS.ViewDoc4.BeginUpdate;
        quSpecVn.Active:=False;
        quSpecVn.Parameters.ParamByName('IDH').Value:=quVnHID.AsInteger;
        quSpecVn.Parameters.ParamByName('ISKL').Value:=quVnHIDSKLFROM.AsInteger;
        quSpecVn.Active:=True;

        quSpecVn.First;
        while not quSpecVn.Eof do
        begin
          with fmDocsVnS do
          begin
            taSpecVn.Append;
            taSpecVnNum.AsInteger:=quSpecVnNUM.AsInteger;
            taSpecVnCodeTovar.AsInteger:=quSpecVnIDCARD.asinteger;
            taSpecVnName.AsString:=quSpecVnNAME.AsString;
            taSpecVnEdIzm.AsInteger:=quSpecVnEDIZM.AsInteger;
            taSpecVnBarCode.AsString:=quSpecVnSBAR.AsString;
            taSpecVnPrice.AsFloat:=quSpecVnPRICE.AsFloat;
            taSpecVnQuant.AsFloat:=quSpecVnQUANT.AsFloat;
            taSpecVnPriceR.AsFloat:=quSpecVnPRICER.AsFloat;
            taSpecVnPriceIn.AsFloat:=quSpecVnPRICEIN.AsFloat;
            taSpecVnPriceIn0.AsFloat:=quSpecVnPRICEIN0.AsFloat;
            taSpecVnSumR.AsFloat:=quSpecVnSUMR.AsFloat;
            taSpecVnSumIn.AsFloat:=quSpecVnSUMIN.AsFloat;
            taSpecVnSumIn0.AsFloat:=quSpecVnSUMIN0.AsFloat;
            taSpecVnSumD.AsFloat:=quSpecVnSUMR.AsFloat-quSpecVnSUMIN.AsFloat;
            taSpecVnQuantRemn.AsFloat:=0;
            taSpecVn.Post;
          end;
          quSpecVn.Next;
        end;
        quSpecVn.Active:=False;
        fmDocsVnS.ViewDoc4.EndUpdate;

        fmDocsVnS.acSaveDoc.Enabled:=True;

        fmDocsVnS.Show;
      end else  showmessage('� ������ ������� ������������� �������� ���������.');
    end else
    begin
      showmessage('�������� �������� ��� ���������.');
    end;
  end;

end;

procedure TfmDocsVnH.acViewDocExecute(Sender: TObject);
begin
  //�������������
  if not CanDo('prViewDocVn') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmMCS do
  begin
    if quVnH.RecordCount>0 then //���� ��� �������������
    begin
      if quVnHIACTIVE.AsInteger<10 then  //��������
      begin
        prSetValsAddDocVn(2); //0-����������, 1-��������������, 2-��������

        CloseTe(fmDocsVnS.taSpecVn);

        fmDocsVnS.ViewDoc4.BeginUpdate;
        quSpecVn.Active:=False;
        quSpecVn.Parameters.ParamByName('IDH').Value:=quVnHID.AsInteger;
        quSpecVn.Parameters.ParamByName('ISKL').Value:=quVnHIDSKLFROM.AsInteger;
        quSpecVn.Active:=True;

        quSpecVn.First;
        while not quSpecVn.Eof do
        begin
          with fmDocsVnS do
          begin
            taSpecVn.Append;
            taSpecVnNum.AsInteger:=quSpecVnNUM.AsInteger;
            taSpecVnCodeTovar.AsInteger:=quSpecVnIDCARD.asinteger;
            taSpecVnName.AsString:=quSpecVnNAME.AsString;
            taSpecVnEdIzm.AsInteger:=quSpecVnEDIZM.AsInteger;
            taSpecVnBarCode.AsString:=quSpecVnSBAR.AsString;
            taSpecVnPrice.AsFloat:=quSpecVnPRICE.AsFloat;
            taSpecVnQuant.AsFloat:=quSpecVnQUANT.AsFloat;
            taSpecVnPriceR.AsFloat:=quSpecVnPRICER.AsFloat;
            taSpecVnPriceIn.AsFloat:=quSpecVnPRICEIN.AsFloat;
            taSpecVnPriceIn0.AsFloat:=quSpecVnPRICEIN0.AsFloat;
            taSpecVnSumR.AsFloat:=quSpecVnSUMR.AsFloat;
            taSpecVnSumIn.AsFloat:=quSpecVnSUMIN.AsFloat;
            taSpecVnSumIn0.AsFloat:=quSpecVnSUMIN0.AsFloat;
            taSpecVnSumD.AsFloat:=quSpecVnSUMR.AsFloat-quSpecVnSUMIN.AsFloat;
            taSpecVnQuantRemn.AsFloat:=0;
            taSpecVn.Post;
          end;
          quSpecVn.Next;
        end;
        quSpecVn.Active:=False;
        fmDocsVnS.ViewDoc4.EndUpdate;

        fmDocsVnS.acSaveDoc.Enabled:=False;

        fmDocsVnS.Show;
      end else  showmessage('� ������ ������� ������������� �������� ���������.');
    end else
    begin
      showmessage('�������� �������� ��� ���������.');
    end;
  end;

end;

procedure TfmDocsVnH.ViewDocsVnCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
Var i:Integer;
    sA:String;
begin
  sA:=' ';
  for i:=0 to ViewDocsVn.ColumnCount-1 do
  begin
    if ViewDocsVn.Columns[i].Name='ViewDocsVnIACTIVE' then
    begin
      sA:=AViewInfo.GridRecord.DisplayTexts[i];
    //  break;
    end;
  end;

//  if pos('�����',sA)=0  then  ACanvas.Canvas.Brush.Color := $00ECD9FF;
  if pos('�',sA)=0  then  ACanvas.Canvas.Brush.Color := $00B3B3FF;  //�����������
  if pos('�',sA)>0  then  ACanvas.Canvas.Brush.Color := $00E6F3DE; //����������� - ������������

// $00E6E6CA ���,  $00F4F4EA �������

end;

procedure TfmDocsVnH.acDelDocExecute(Sender: TObject);
begin
  if not CanDo('prDelDocVn') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  with dmMCS do
  begin
    if quVnH.RecordCount>0 then //���� ��� �������
    begin
      if quVnHIACTIVE.AsInteger<3 then
      begin
        if MessageDlg('�� ������������� ������ ������� �������� �'+quVnHNUMDOC.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
//          prAddHist(2,quVnHID.AsInteger,trunc(quVnHDateInvoice.AsDateTime),quVnHNumber.AsString,'���.',0);

          quD.SQL.Clear;
          quD.SQL.Add('DELETE FROM [dbo].[DOCVN_HEAD]');
          quD.SQL.Add('where IDSKLFROM='+its(quVnHIDSKLFROM.AsInteger));
          quD.SQL.Add('and IDATEDOC='+its(quVnHIDATEDOC.AsInteger));
          quD.SQL.Add('and ID='+its(quVnHID.AsInteger));
          quD.ExecSQL;
          delay(100);

          quVnH.Requery();
        end;
      end else
      begin
        showmessage('������� �������������� �������� ������.');
      end;
    end else
    begin
      showmessage('�������� �������� ��� ��������.');
    end;
  end;
end;

procedure TfmDocsVnH.ViewDocsVnDblClick(Sender: TObject);
begin
  //������� �������
  with dmMCS do
  begin
    if quVnH.RecordCount>0 then
    begin
      if quVnHIACTIVE.AsInteger<3 then acEditDoc.Execute //��������������
      else acViewDoc.Execute; //��������}
    end;
  end;
end;

procedure TfmDocsVnH.acOnVnExecute(Sender: TObject);
Var IDH:INteger;
begin
  //������������
  with dmMCS do
  begin
    if not CanDo('prOnDocVn') then begin StatusBar1.Panels[0].Text:='��� ����.'; SpeedItem7.Enabled:=True; exit; end;

    if quVnH.RecordCount>0 then //���� ��� ������������
    begin
      if quVnHIACTIVE.AsInteger<=1 then
      begin
        if quVnHDATEDOC.AsDateTime<prOpenDate(quVnHIDSKLFROM.AsInteger) then begin Memo1.Lines.Add('������ ������.'); StatusBar1.Panels[0].Text:='������ ������.'; exit; end;

        if fTestInv(quVnHIDSKLFROM.AsInteger,quVnHIDATEDOC.AsInteger)=False then begin StatusBar1.Panels[0].Text:='������ ������ (��������������).'; exit; end;
        if quVnHIDSKLFROM.AsInteger<1 then
        begin
          ShowMessage('�������� �����.');
          Memo1.Lines.Add('����� �������� �� ����������, ������������� ����������!!!');
          exit;
        end;

        if quVnHIDATEDOC.AsInteger<Trunc(Date) then
        begin
          StrMess:='�������� ! �������� ����� ����� ����������. ���������� ?';
          if MessageDlg(StrMess,mtConfirmation, [mbYes, mbNo], 0) <> mrYes then Exit;
        end;

        if MessageDlg('������������ �������� �'+quVnHNUMDOC.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          IDH:=quVnHID.AsInteger;
          if OprDocsVn(IDH,fmDocsVnH.Memo1) then
          begin
            ViewDocsVn.BeginUpdate;
            quVnH.Requery();
            quVnH.Locate('ID',IDH,[]);
            ViewDocsVn.EndUpdate;

            prWrLog(quVnHIDSKLFROM.AsInteger,3,1,IDH,'');

            GridDocsVn.SetFocus;
            ViewDocsVn.Controller.FocusRecord(ViewDocsVn.DataController.FocusedRowIndex,True);
          end;  
        end;
      end;
    end;
  end;
end;


procedure TfmDocsVnH.acOffVnExecute(Sender: TObject);
Var IDH:INteger;
begin
//��������
  with dmMCS do
  begin
    if not CanDo('prOffDocVn') then begin StatusBar1.Panels[0].Text:='��� ����.'; SpeedItem8.Enabled:=True; exit; end;

    if quVnH.RecordCount>0 then //���� ��� ������������
    begin
      if quVnHIACTIVE.AsInteger=3 then
      begin
        if quVnHDATEDOC.AsDateTime<prOpenDate(quVnHIDSKLFROM.AsInteger) then begin Memo1.Lines.Add('������ ������.'); StatusBar1.Panels[0].Text:='������ ������.'; exit; end;

        if fTestInv(quVnHIDSKLFROM.AsInteger,quVnHIDATEDOC.AsInteger)=False then begin StatusBar1.Panels[0].Text:='������ ������ (��������������).'; exit; end;
        if quVnHIDSKLFROM.AsInteger<1 then
        begin
          ShowMessage('�������� �����.');
          Memo1.Lines.Add('����� �������� �� ����������, �������� ����������!!!');
          exit;
        end;

        if quVnHIDATEDOC.AsInteger<Trunc(Date) then
        begin
          StrMess:='�������� ! �������� ����� ����� ����������. ���������� ?';
          if MessageDlg(StrMess,mtConfirmation, [mbYes, mbNo], 0) <> mrYes then Exit;
        end;
        
        if MessageDlg('�������� �������� �'+quVnHNUMDOC.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          IDH:=quVnHID.AsInteger;
          if OtkatDocsVn(IDH,fmDocsVnH.Memo1) then
          begin
            ViewDocsVn.BeginUpdate;
            quVnH.Requery();
            quVnH.Locate('ID',IDH,[]);
            ViewDocsVn.EndUpdate;

            prWrLog(quVnHIDSKLFROM.AsInteger,3,0,IDH,'');

            GridDocsVn.SetFocus;
            ViewDocsVn.Controller.FocusRecord(ViewDocsVn.DataController.FocusedRowIndex,True);
          end;
        end;
      end;
    end;


  end;
end;

end.
