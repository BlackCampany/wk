unit PXDB;

interface

uses
  SysUtils, Classes, DBTables, DB;

type
  TdmPx = class(TDataModule)
    taCard: TTable;
    taCardArticul: TStringField;
    taCardName: TStringField;
    taCardMesuriment: TStringField;
    taCardMesPresision: TFloatField;
    taCardAdd1: TStringField;
    taCardAdd2: TStringField;
    taCardAdd3: TStringField;
    taCardAddNum1: TFloatField;
    taCardAddNum2: TFloatField;
    taCardAddNum3: TFloatField;
    taCardScale: TStringField;
    taCardGroop1: TSmallintField;
    taCardGroop2: TSmallintField;
    taCardGroop3: TSmallintField;
    taCardGroop4: TSmallintField;
    taCardGroop5: TSmallintField;
    taCardPriceRub: TCurrencyField;
    taCardPriceCur: TCurrencyField;
    taCardClientIndex: TSmallintField;
    taCardCommentary: TStringField;
    taCardDeleted: TSmallintField;
    taCardModDate: TDateField;
    taCardModTime: TSmallintField;
    taCardModPersonIndex: TSmallintField;
    taBar: TTable;
    taBarBarCode: TStringField;
    taBarCardArticul: TStringField;
    taBarCardSize: TStringField;
    taBarQuantity: TFloatField;
    tbTax: TTable;
    tbTaxCardArticul: TStringField;
    tbTaxTaxIndex: TSmallintField;
    tbTaxTax: TFloatField;
    tbTaxTaxSumRub: TFloatField;
    tbTaxTaxSumCur: TFloatField;
    taList: TTable;
    taListTName: TStringField;
    taListDataType: TSmallintField;
    taListOperation: TSmallintField;
    taClass: TTable;
    taClassGroop1: TSmallintField;
    taClassGroop2: TSmallintField;
    taClassGroop3: TSmallintField;
    taClassGroop4: TSmallintField;
    taClassGroop5: TSmallintField;
    taClassName: TStringField;
    taClassGr1: TIntegerField;
    taClassGr2: TIntegerField;
    taClassGr3: TIntegerField;
    taClassGr4: TIntegerField;
    taClassGr5: TIntegerField;
    tbTaxT: TTable;
    tbTaxTID: TSmallintField;
    tbTaxTPriority: TSmallintField;
    tbTaxTName: TStringField;
    tbCashers: TTable;
    tbCashersIdent: TSmallintField;
    tbCashersName: TStringField;
    tbCashersPassw: TStringField;
    tbCashersOfficialIndex: TSmallintField;
    tbDeparts: TTable;
    tbDepartsID: TSmallintField;
    tbDepartsName: TStringField;
    tbDiscSum: TTable;
    tbDiscSumPriceIndex: TSmallintField;
    tbDiscSumTime: TSmallintField;
    tbDiscSumSumma: TCurrencyField;
    tbDiscSumDiscount: TFloatField;
    tbDiscQ: TTable;
    tbDiscQCardArticul: TStringField;
    tbDiscQQuantity: TFloatField;
    tbDiscQDiscount: TCurrencyField;
    tbDiscCard: TTable;
    tbDiscCardBarCode: TStringField;
    tbDiscCardName: TStringField;
    tbDiscCardPercent: TFloatField;
    tbDiscCardClientIndex: TSmallintField;
    tbDiscStop: TTable;
    tbDiscStopCodeStart: TStringField;
    tbDiscStopCodeEnd: TStringField;
    tbAdvDisc: TTable;
    tbAdvDiscGroop1: TSmallintField;
    tbAdvDiscGroop2: TSmallintField;
    tbAdvDiscGroop3: TSmallintField;
    tbAdvDiscGroop4: TSmallintField;
    tbAdvDiscGroop5: TSmallintField;
    tbAdvDiscSumma: TCurrencyField;
    tbAdvDiscBarCode: TStringField;
    tbAdvDiscPercent: TFloatField;
    tbCredCard: TTable;
    tbCredCardID: TSmallintField;
    tbCredCardName: TStringField;
    tbCredCardClientIndex: TSmallintField;
    tbCredCardLimitSum: TCurrencyField;
    tbCredCardCanReturn: TSmallintField;
    tbCredPref: TTable;
    tbCredPrefPrefix: TStringField;
    tbCredPrefCredCardIndex: TSmallintField;
    Session1: TSession;
    taCS: TTable;
    taCSShopIndex: TSmallintField;
    taCSCashNumber: TSmallintField;
    taCSZNumber: TSmallintField;
    taCSCheckNumber: TSmallintField;
    taCSID: TSmallintField;
    taCSDate: TDateField;
    taCSTime: TSmallintField;
    taCSCardArticul: TStringField;
    taCSCardSize: TStringField;
    taCSQuantity: TFloatField;
    taCSPriceRub: TCurrencyField;
    taCSPriceCur: TCurrencyField;
    taCSTotalRub: TCurrencyField;
    taCSTotalCur: TCurrencyField;
    taCSDepartment: TSmallintField;
    taCSCasher: TSmallintField;
    taCSUsingIndex: TSmallintField;
    taCSReplace: TSmallintField;
    taCSOperation: TSmallintField;
    taCSCredCardIndex: TSmallintField;
    taCSDiscCliIndex: TSmallintField;
    taCSLinked: TSmallintField;
    dstaCS: TDataSource;
    taDC: TTable;
    taDCShopIndex: TSmallintField;
    taDCCashNumber: TSmallintField;
    taDCZNumber: TSmallintField;
    taDCCheckNumber: TSmallintField;
    taDCID: TSmallintField;
    taDCDiscountIndex: TSmallintField;
    taDCDiscountProc: TFloatField;
    taDCDiscountRub: TCurrencyField;
    taDCDiscountCur: TCurrencyField;
    taDStop: TTable;
    taDStopCardArticul: TStringField;
    taDStopPercent: TFloatField;
    taDCRD: TTable;
    taDCRDShopIndex: TSmallintField;
    taDCRDCashNumber: TSmallintField;
    taDCRDZNumber: TSmallintField;
    taDCRDCheckNumber: TSmallintField;
    taDCRDCardType: TSmallintField;
    taDCRDCardNumber: TStringField;
    taDCRDDiscountRub: TCurrencyField;
    taDCRDDiscountCur: TCurrencyField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmPx: TdmPx;

implementation

{$R *.dfm}

end.
