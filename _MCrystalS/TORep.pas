unit TORep;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxControls, cxContainer, cxEdit, cxTextEdit, cxMemo, cxStyles,
  cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, DB, cxDBData,
  cxCalendar, cxImageComboBox, Placemnt, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, SpeedBar, ExtCtrls, ComCtrls, XPStyleActnCtrls,
  ActnList, ActnMan, FR_DSet, FR_DBSet, FR_Class;

type
  TfmTO = class(TForm)
    Memo1: TcxMemo;
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    SpeedItem6: TSpeedItem;
    GridTO: TcxGrid;
    ViewTO: TcxGridDBTableView;
    LevelTO: TcxGridLevel;
    fpfmTORep: TFormPlacement;
    ViewTOISKL: TcxGridDBColumn;
    ViewTONAME: TcxGridDBColumn;
    ViewTOIDATE: TcxGridDBColumn;
    ViewTOSumIn: TcxGridDBColumn;
    ViewTOSumIn0: TcxGridDBColumn;
    ViewTOSumInR: TcxGridDBColumn;
    ViewTOSumInVn: TcxGridDBColumn;
    ViewTOSumInVnR: TcxGridDBColumn;
    ViewTOSumInv: TcxGridDBColumn;
    ViewTOSumOutIn: TcxGridDBColumn;
    ViewTOSumOutIn0: TcxGridDBColumn;
    ViewTOSumOutR: TcxGridDBColumn;
    ViewTOSumOutVn: TcxGridDBColumn;
    ViewTOSumOutVnR: TcxGridDBColumn;
    ViewTOSumAC: TcxGridDBColumn;
    ViewTOCashNal: TcxGridDBColumn;
    ViewTOCashDNal: TcxGridDBColumn;
    ViewTOCashBn: TcxGridDBColumn;
    ViewTOCashDBn: TcxGridDBColumn;
    ViewTOCashSS: TcxGridDBColumn;
    ViewTOSumInT: TcxGridDBColumn;
    ViewTOSumInVnT: TcxGridDBColumn;
    ViewTOSumOutT: TcxGridDBColumn;
    ViewTOSumOutVnT: TcxGridDBColumn;
    ViewTOSumInvT: TcxGridDBColumn;
    ViewTOSumBeg: TcxGridDBColumn;
    ViewTOSumEnd: TcxGridDBColumn;
    ViewTOSumTBeg: TcxGridDBColumn;
    ViewTOSumTEnd: TcxGridDBColumn;
    amTO: TActionManager;
    acCalcTO: TAction;
    acPrintTO: TAction;
    RepTO: TfrReport;
    frtaTO: TfrDBDataSet;
    ViewTOSumInvR: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SpeedItem5Click(Sender: TObject);
    procedure SpeedItem4Click(Sender: TObject);
    procedure SpeedItem6Click(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
    procedure acCalcTOExecute(Sender: TObject);
    procedure acPrintTOExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmTO: TfmTO;

implementation

uses Un1, Dm, Period1, MainMC;

{$R *.dfm}

procedure TfmTO.FormCreate(Sender: TObject);
begin
  fpfmToRep.IniFileName:=sFormIni;
  fpfmToRep.Active:=True; delay(10);
  GridTo.Align:=AlClient;
  ViewTo.RestoreFromIniFile(sGridIni); delay(10);
end;

procedure TfmTO.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewTO.StoreToIniFile(sGridIni,False); delay(10);
end;

procedure TfmTO.SpeedItem5Click(Sender: TObject);
Var iDateE,iDateB:INteger;
begin
  with dmMCS do
  begin
    fmPeriod1.cxDateEdit1.Date:=Trunc(CommonSet.DateBeg);
    fmPeriod1.cxDateEdit2.Date:=Trunc(CommonSet.DateEnd);
    fmPeriod1.Label3.Caption:='';
    fmPeriod1.ShowModal;
    if fmPeriod1.ModalResult=mrOk then
    begin
      iDateB:=Trunc(fmPeriod1.cxDateEdit1.Date);
      iDateE:=Trunc(fmPeriod1.cxDateEdit2.Date);
      CommonSet.DateBeg:=Trunc(fmPeriod1.cxDateEdit1.Date);
      CommonSet.DateEnd:=Trunc(fmPeriod1.cxDateEdit2.Date);

      fmTO.Memo1.Clear; delay(10);
      fmTO.Caption:='�������� ������ � '+FormatDateTiMe('dd.mm.yyyy',iDateB)+' �� '+FormatDateTiMe('dd.mm.yyyy',iDateE);
      fmTO.Memo1.Lines.Add('����� .. ���� ������������ ������.'); delay(10);
      fmTO.ViewTO.BeginUpdate;
      try
        quTORep.Active:=False;
        quTORep.SQL.Clear;
        quTORep.SQL.Add('EXECUTE [dbo].[prGetTO] '+its(fmMainMC.Label3.tag)+','+its(iDateB)+','+its(iDateE));
        quTORep.Active:=True;
      finally
        fmTO.ViewTO.EndUpdate;
      end;
      fmTO.Memo1.Lines.Add('������� ��������.'); delay(10);
    end;
  end;
end;

procedure TfmTO.SpeedItem4Click(Sender: TObject);
begin
  close;
end;

procedure TfmTO.SpeedItem6Click(Sender: TObject);
begin
  prNExportExel6(ViewTO);
end;

procedure TfmTO.SpeedItem2Click(Sender: TObject);
begin
  //������

end;

procedure TfmTO.acCalcTOExecute(Sender: TObject);
Var iDateCur,iDateB,iDateE:INteger;
begin
  if not CanDo('prCalcTO') then begin StatusBar1.Panels[0].Text:='��� ����.'; end else
  with dmMCS do
  begin
    fmPeriod1.cxDateEdit1.Date:=Trunc(CommonSet.DateBeg);
    fmPeriod1.cxDateEdit2.Date:=Trunc(CommonSet.DateEnd);
    fmPeriod1.Label3.Caption:='';
    fmPeriod1.ShowModal;
    if fmPeriod1.ModalResult=mrOk then
    begin
      iDateB:=Trunc(fmPeriod1.cxDateEdit1.Date);
      iDateE:=Trunc(fmPeriod1.cxDateEdit2.Date);
      CommonSet.DateBeg:=Trunc(fmPeriod1.cxDateEdit1.Date);
      CommonSet.DateEnd:=Trunc(fmPeriod1.cxDateEdit2.Date);

      if MessageDlg('���������� ������ ������ �� �� � '+FormatDateTiMe('dd.mm.yyyy',iDateB)+' �� '+FormatDateTiMe('dd.mm.yyyy',iDateE), mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        fmTO.Memo1.Clear; delay(10);
        fmTO.Memo1.Lines.Add('����� ... ���� ������������ ������ �� � '+FormatDateTiMe('dd.mm.yyyy',iDateB)+' �� '+FormatDateTiMe('dd.mm.yyyy',iDateE));
        fmTO.ViewTO.BeginUpdate;
        try
          prWrLog(fmMainMC.Label3.tag,30,1,0,' ������������ �� ������ � '+ds1(iDateB)+'('+its(iDateB)+') �� '+ds1(iDateE)+'('+its(iDateE)+')');

          for iDateCur:=iDateB to iDateE do
          begin
            if iDateCur<prOpenDate(fmMainMC.Label3.tag) then
            begin
              fmTO.Memo1.Lines.Add('  - '+ds1(iDateCur)+'  ������ ������.');
              prWrLog(fmMainMC.Label3.tag,30,1,0,' ������ ������ '+ds1(iDateCur)+'('+its(iDateCur)+')');
              delay(10);
            end else
            begin
              fmTO.Memo1.Lines.Add('  - '+ds1(iDateCur)); delay(10);


              quProc.SQL.Clear;
              quProc.SQL.Add('DECLARE @IDATE int = '+its(iDateCur));
              quProc.SQL.Add('DECLARE @ISKL int = '+its(fmMainMC.Label3.tag));
              quProc.SQL.Add('EXECUTE [dbo].[prCalcTO] @IDATE,@ISKL');
              quProc.ExecSQL;

              prWrLog(fmMainMC.Label3.tag,30,1,0,' ������ �� '+ds1(iDateCur)+'('+its(iDateCur)+')');
            end;
          end;

          quTORep.Active:=False;
          quTORep.SQL.Clear;
          quTORep.SQL.Add('EXECUTE [dbo].[prGetTO] '+its(fmMainMC.Label3.tag)+','+its(iDateB)+','+its(iDateE));
          quTORep.Active:=True;
        finally
          fmTO.ViewTO.EndUpdate;
        end;
        fmTO.Memo1.Lines.Add('������� ��������.'); delay(10);
      end;
    end;
  end;
end;

procedure TfmTO.acPrintTOExecute(Sender: TObject);
Var iSS:INteger;
begin
  //������ TO
  with dmMCS do
  begin
    if quTORep.RecordCount>0 then
    begin
      iSS:=fSS(quTORepISKL.AsInteger);

      if iSS>=0 then //���� ��� ���� ���������
      begin
        quTOPrint.Active:=False;
        quTOPrint.SQL.Clear;
        quTOPrint.SQL.Add('EXECUTE [dbo].[prSelTO] '+its(quTORepISKL.AsInteger)+','+its(quTORepIDATE.AsInteger));
        quTOPrint.Active:=True;

        RepTO.LoadFromFile(CommonSet.PathReport + 'RepTO1.frf');

        frVariables.Variable['DocDate']:=ds1(quTORepIDATE.AsInteger);
        frVariables.Variable['Depart']:=quTORepFULLNAME.AsString;
        frVariables.Variable['SumB']:=quTORepSumBeg.AsFloat;
        frVariables.Variable['SumBT']:=0;
        frVariables.Variable['SumInv']:=0;
        frVariables.Variable['SumInT']:=0;
        frVariables.Variable['SumOut']:=0;
        frVariables.Variable['SumOutT']:=0;
        frVariables.Variable['CountDocIn']:=5;
        frVariables.Variable['CountDocOut']:=5;
        frVariables.Variable['SumSale']:=quTORepCashNal.AsFloat;
        frVariables.Variable['SumSaleBn']:=quTORepCashBn.AsFloat;
        frVariables.Variable['SumDisc']:=quTORepCashDNal.AsFloat;
        frVariables.Variable['SumDiscBn']:=quTORepCashDBn.AsFloat;
        frVariables.Variable['SumAC']:=quTORepSumAC.AsFloat;
        frVariables.Variable['SumE']:=quTORepSumEnd.AsFloat;
        frVariables.Variable['SumET']:=0;
        frVariables.Variable['SumInOutNDS']:=0;
        frVariables.Variable['NDS10']:=0;
        frVariables.Variable['NDS20']:=0;
        frVariables.Variable['SumSaleSS']:=quTORepCashSS.AsFloat;
        frVariables.Variable['SumSaleSSOutNDS']:=0;
        frVariables.Variable['SumSaleNDS10']:=0;
        frVariables.Variable['SumSaleNDS20']:=0;
        frVariables.Variable['SumCash']:=quTORepCashNal.AsFloat+quTORepCashBn.AsFloat;

        RepTO.ReportName:='�������� �����.';
        RepTO.PrepareReport;
        RepTO.ShowPreparedReport;
      end;
    end;
  end;
end;

procedure TfmTO.FormShow(Sender: TObject);
Var iSS:INteger;
begin
  iSS:=fSS(fmMainMC.Label3.tag);
  if iSS=2 then
  begin
    ViewTOSumIn.Visible:=False;
    ViewTOSumIn0.Visible:=False;
    ViewTOSumInVn.Visible:=False;
    ViewTOSumOutIn.Visible:=False;
    ViewTOSumOutIn0.Visible:=False;
    ViewTOSumOutVn.Visible:=False;
    ViewTOCashSS.Visible:=False;
  end;
  if iSS=0 then
  begin
    ViewTOSumIn.Visible:=True;
    ViewTOSumIn0.Visible:=False;
    ViewTOSumInVn.Visible:=True;
    ViewTOSumOutIn.Visible:=True;
    ViewTOSumOutIn0.Visible:=False;
    ViewTOSumOutVn.Visible:=True;
    ViewTOCashSS.Visible:=True;
  end;
end;

end.
