unit Rep1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, Placemnt, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxControls, cxGridCustomView, cxGrid, cxContainer, cxTextEdit, cxMemo,
  SpeedBar, ExtCtrls, dxmdaset, ActnList, XPStyleActnCtrls, ActnMan,
  cxImageComboBox, Menus;

type
  TfmRep1 = class(TForm)
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    SpeedItem6: TSpeedItem;
    GridRep1: TcxGrid;
    ViewRep1: TcxGridDBTableView;
    LevelRep1: TcxGridLevel;
    fmplRep1: TFormPlacement;
    ViewRep1RecId: TcxGridDBColumn;
    ViewRep1iCode: TcxGridDBColumn;
    ViewRep1NAME: TcxGridDBColumn;
    ViewRep1EDIZM: TcxGridDBColumn;
    ViewRep1rQB: TcxGridDBColumn;
    ViewRep1rQIn: TcxGridDBColumn;
    ViewRep1rQOut: TcxGridDBColumn;
    ViewRep1rQE: TcxGridDBColumn;
    ViewRep1sClass: TcxGridDBColumn;
    amRep1: TActionManager;
    acMoveRep1: TAction;
    Panel5: TPanel;
    Memo1: TcxMemo;
    Gr1: TcxGrid;
    Vi1: TcxGridDBTableView;
    Vi1DATEDOC: TcxGridDBColumn;
    Vi1Name: TcxGridDBColumn;
    Vi1QUANT: TcxGridDBColumn;
    Vi1PRICEIN: TcxGridDBColumn;
    Vi1PROCN: TcxGridDBColumn;
    Vi1PRICEUCH: TcxGridDBColumn;
    Vi1SUMIN: TcxGridDBColumn;
    Vi1SUMUCH: TcxGridDBColumn;
    Vi1NUMDOC: TcxGridDBColumn;
    le1: TcxGridLevel;
    PopupMenu1: TPopupMenu;
    acMoveRep11: TMenuItem;
    ViewRep1sClass2: TcxGridDBColumn;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SpeedItem4Click(Sender: TObject);
    procedure SpeedItem6Click(Sender: TObject);
    procedure acMoveRep1Execute(Sender: TObject);
    procedure ViewRep1SelectionChanged(Sender: TcxCustomGridTableView);
    procedure N2Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmRep1: TfmRep1;

implementation

uses Un1, Dm, MainMC, Move;

{$R *.dfm}

procedure TfmRep1.FormCreate(Sender: TObject);
begin
  fmplRep1.IniFileName:=sFormIni;
  fmplRep1.Active:=True; delay(10);

  GridRep1.Align:=AlClient;
  ViewRep1.RestoreFromIniFile(sGridIni); delay(10);
end;

procedure TfmRep1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewRep1.StoreToIniFile(sGridIni,False); delay(10);
end;

procedure TfmRep1.SpeedItem4Click(Sender: TObject);
begin
  Close;
end;

procedure TfmRep1.SpeedItem6Click(Sender: TObject);
begin
  prNExportExel6(ViewRep1);
end;

procedure TfmRep1.acMoveRep1Execute(Sender: TObject);
begin
  with dmMCS do
  begin
    if quRep1Data.RecordCount>0 then
    begin
      prFormMove(fmMainMC.Label3.tag,quRep1DataID.AsInteger,Trunc(CommonSet.DateBeg),Trunc(CommonSet.DateEnd),quRep1DataNAME.AsString);
      fmMove.ShowModal;
    end;
  end;
end;

procedure TfmRep1.ViewRep1SelectionChanged(Sender: TcxCustomGridTableView);
begin
  if ViewRep1.Controller.SelectedRecordCount>1 then exit;
  with dmMCS do
  begin
    Vi1.BeginUpdate;
    quPosts3.Active:=False;
    quPosts3.Parameters.ParamByName('ICODE').Value:=quRep1DataID.AsInteger;
    quPosts3.Parameters.ParamByName('ISKL').Value:=fmMainMC.Label3.tag;
    quPosts3.Active:=True;
    Vi1.EndUpdate;
  end;
end;

procedure TfmRep1.N2Click(Sender: TObject);
begin
  prExpandVi(ViewRep1,True);
end;

procedure TfmRep1.N3Click(Sender: TObject);
begin
  prExpandVi(ViewRep1,False);
end;

end.
