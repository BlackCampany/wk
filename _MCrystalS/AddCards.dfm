object fmAddCard: TfmAddCard
  Left = 751
  Top = 167
  BorderStyle = bsDialog
  Caption = #1050#1072#1088#1090#1086#1095#1082#1072' '#1090#1086#1074#1072#1088#1072
  ClientHeight = 717
  ClientWidth = 581
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 98
    Width = 57
    Height = 13
    Caption = #1050#1086#1076' '#1090#1086#1074#1072#1088#1072
    Transparent = True
  end
  object Label2: TLabel
    Left = 192
    Top = 96
    Width = 52
    Height = 13
    Caption = #1064#1090#1088#1080#1093'-'#1082#1086#1076
    Transparent = True
  end
  object Label3: TLabel
    Left = 8
    Top = 132
    Width = 76
    Height = 13
    Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object Label4: TLabel
    Left = 8
    Top = 160
    Width = 70
    Height = 13
    Caption = #1055#1086#1083#1085#1086#1077' '#1085#1072#1080#1084'.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object Label5: TLabel
    Left = 8
    Top = 190
    Width = 36
    Height = 13
    Caption = #1057#1090#1088#1072#1085#1072
    Transparent = True
  end
  object Label6: TLabel
    Left = 8
    Top = 218
    Width = 35
    Height = 13
    Caption = #1043#1088#1091#1087#1087#1072
    Transparent = True
  end
  object Label8: TLabel
    Left = 415
    Top = 190
    Width = 24
    Height = 13
    Caption = #1053#1044#1057
    Transparent = True
  end
  object Label14: TLabel
    Left = 8
    Top = 246
    Width = 76
    Height = 13
    Caption = #1050#1072#1090'. '#1084#1077#1085#1077#1076#1078#1077#1088
    Transparent = True
  end
  object Label16: TLabel
    Left = 8
    Top = 274
    Width = 31
    Height = 13
    Caption = #1041#1088#1101#1085#1076
    Transparent = True
  end
  object Label18: TLabel
    Left = 415
    Top = 246
    Width = 53
    Height = 13
    Caption = #1044#1083#1080#1085#1072', '#1089#1084
    Transparent = True
  end
  object Label19: TLabel
    Left = 415
    Top = 274
    Width = 59
    Height = 13
    Caption = #1064#1080#1088#1080#1085#1072', '#1089#1084
    Transparent = True
  end
  object Label20: TLabel
    Left = 415
    Top = 302
    Width = 58
    Height = 13
    Caption = #1042#1099#1089#1086#1090#1072', '#1089#1084
    Transparent = True
  end
  object Label26: TLabel
    Left = 12
    Top = 308
    Width = 78
    Height = 13
    Caption = #1050#1085#1086#1087#1082#1072' '#1074' '#1074#1077#1089#1072#1093
    Transparent = True
  end
  object Label27: TLabel
    Left = 176
    Top = 308
    Width = 132
    Height = 13
    Caption = #1060#1086#1088#1084#1072#1090' '#1101#1090#1080#1082#1077#1090#1082#1080' '#1074' '#1074#1077#1089#1072#1093
    Transparent = True
  end
  object Label33: TLabel
    Left = 415
    Top = 218
    Width = 35
    Height = 13
    Caption = #1054#1073#1098#1077#1084
    Transparent = True
  end
  object Label37: TLabel
    Left = 416
    Top = 328
    Width = 50
    Height = 13
    Caption = #1042#1077#1089' Brutto'
  end
  object Label9: TLabel
    Left = 444
    Top = 60
    Width = 34
    Height = 13
    Caption = #1057#1090#1072#1090#1091#1089
  end
  object Label10: TLabel
    Left = 279
    Top = 62
    Width = 44
    Height = 13
    Caption = #1053#1072#1094#1077#1085#1082#1072
    Transparent = True
  end
  object Label12: TLabel
    Left = 279
    Top = 22
    Width = 19
    Height = 13
    Caption = #1058#1080#1087
    Transparent = True
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 698
    Width = 581
    Height = 19
    Panels = <
      item
        Width = 400
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 645
    Width = 581
    Height = 53
    Align = alBottom
    BevelInner = bvLowered
    Color = 16765348
    TabOrder = 4
    object cxButton2: TcxButton
      Left = 168
      Top = 12
      Width = 97
      Height = 33
      Caption = 'Ok'
      Default = True
      TabOrder = 0
      OnClick = cxButton2Click
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton3: TcxButton
      Left = 328
      Top = 12
      Width = 97
      Height = 33
      Caption = #1042#1099#1093#1086#1076
      ModalResult = 2
      TabOrder = 1
      Glyph.Data = {
        5E040000424D5E04000000000000360000002800000012000000130000000100
        18000000000028040000C40E0000C40E00000000000000000000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D684868484868484868484868484868484868484
        8684848684848684848684CED3D6C6C7C6848684C6C7C6CED3D6C6C7C6CED3D6
        0000CED3D6848684848684848684848684848684848684848684848684848684
        848684848684FFFFFF848684FFFFFF848684FFFFFFCED3D60000CED3D6A57500
        7B59007B59007B59004A494A4A494A848684848684848684FFFFFFFFFFFFFFFF
        FF7B59007B59007B5900A57500CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A575004A494A4A494A848684FFFFFFFFFFFFFFFFFF7B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFFFFFFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFFFF
        FFFFFFFFFFFFFFFFFF7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D67B5900A57500A57500A575004A494AFFFFDEFFFF84F7EF73FFFF
        007B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B59
        00A57500A57500A575004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A5
        75004A494AFFFF84FFFF84FFFF84FFFF847B5900CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D67B5900A57500A57500A575004A494AFFFF00
        FFFF84FFFF84FFFFDE7B5900CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6A575007B59007B59007B59007B59007B59007B59007B59007B59
        00A57500CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D66B6D6B4A
        494A4A494A4A494A4A494A6B6D6BCED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D64A494A00DF0000DF0000DF00
        00DF004A494ACED3D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6
        CED3D6CED3D6CED3D6CED3D66B6D6B4A494A4A494A4A494A4A494A6B6D6BCED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3
        D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D60000CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CE
        D3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6CED3D6
        0000}
      LookAndFeel.Kind = lfOffice11
    end
  end
  object RadioGroup1: TcxRadioGroup
    Left = 8
    Top = 8
    Caption = #1058#1080#1087' '#1090#1086#1074#1072#1088#1072' '
    Properties.Items = <
      item
        Caption = #1064#1090#1091#1095#1085#1099#1081
        Value = 0
      end
      item
        Caption = #1042#1077#1089#1086#1074#1086#1081
        Value = 1
      end>
    Properties.OnChange = RadioGroup1PropertiesChange
    ItemIndex = 0
    Style.BorderStyle = ebsFlat
    TabOrder = 0
    Height = 57
    Width = 109
  end
  object RadioGroup2: TcxRadioGroup
    Left = 132
    Top = 8
    Caption = #1058#1080#1087' '#1096#1090#1088#1080#1093'-'#1082#1086#1076#1072' '
    Properties.Items = <
      item
        Caption = #1057#1090#1072#1085#1076#1072#1088#1090#1085#1099#1081
        Value = 0
      end
      item
        Caption = #1060#1086#1088#1084#1080#1088#1091#1077#1084#1099#1081
        Value = 1
      end
      item
        Caption = #1050#1086#1088#1086#1090#1082#1080#1081
        Value = 2
      end>
    Properties.OnChange = RadioGroup2PropertiesChange
    ItemIndex = 0
    Style.BorderStyle = ebsFlat
    TabOrder = 1
    Height = 73
    Width = 129
  end
  object MaskEdit1: TcxMaskEdit
    Left = 88
    Top = 92
    ParentFont = False
    Properties.Alignment.Horz = taRightJustify
    Properties.MaskKind = emkRegExpr
    Properties.EditMask = '\d+'
    Properties.MaxLength = 0
    Style.BorderStyle = ebsFlat
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -11
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = [fsBold]
    Style.LookAndFeel.Kind = lfFlat
    Style.Shadow = True
    Style.IsFontAssigned = True
    StyleDisabled.LookAndFeel.Kind = lfFlat
    StyleFocused.LookAndFeel.Kind = lfFlat
    StyleHot.LookAndFeel.Kind = lfFlat
    TabOrder = 2
    Text = '1'
    Width = 73
  end
  object MaskEdit2: TcxMaskEdit
    Left = 256
    Top = 92
    BeepOnEnter = False
    Properties.MaskKind = emkRegExpr
    Properties.EditMask = '\d+'
    Properties.MaxLength = 0
    Properties.OnEditValueChanged = MaskEdit2PropertiesEditValueChanged
    Style.BorderStyle = ebsFlat
    Style.Shadow = True
    TabOrder = 3
    Width = 137
  end
  object TextEdit1: TcxTextEdit
    Left = 100
    Top = 124
    Properties.MaxLength = 200
    Style.LookAndFeel.Kind = lfFlat
    Style.Shadow = True
    StyleDisabled.LookAndFeel.Kind = lfFlat
    StyleFocused.LookAndFeel.Kind = lfFlat
    StyleHot.LookAndFeel.Kind = lfFlat
    TabOrder = 6
    Text = '123456789012345678901234567890'
    Width = 457
  end
  object TextEdit2: TcxTextEdit
    Left = 100
    Top = 152
    Properties.MaxLength = 200
    Style.LookAndFeel.Kind = lfFlat
    Style.Shadow = True
    StyleDisabled.LookAndFeel.Kind = lfFlat
    StyleFocused.LookAndFeel.Kind = lfFlat
    StyleHot.LookAndFeel.Kind = lfFlat
    TabOrder = 7
    Text = '123456789012345678901234567890123456789012345678901234567890'
    Width = 457
  end
  object ButtonEdit1: TcxButtonEdit
    Left = 72
    Top = 212
    ParentFont = False
    Properties.Buttons = <
      item
        Default = True
        Kind = bkEllipsis
      end>
    Properties.OnButtonClick = ButtonEdit2PropertiesButtonClick
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = 4204565
    Style.Font.Height = -11
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = [fsBold]
    Style.LookAndFeel.Kind = lfFlat
    Style.Shadow = True
    Style.IsFontAssigned = True
    StyleDisabled.LookAndFeel.Kind = lfFlat
    StyleFocused.LookAndFeel.Kind = lfFlat
    StyleHot.LookAndFeel.Kind = lfFlat
    TabOrder = 8
    Text = 'ButtonEdit1'
    Width = 251
  end
  object cxButton4: TcxButton
    Left = 408
    Top = 92
    Width = 145
    Height = 25
    Caption = #1043#1077#1085#1077#1088#1072#1094#1080#1103' '#1085#1086#1074#1086#1075#1086' '#1082#1086#1076#1072
    TabOrder = 10
    TabStop = False
    OnClick = cxButton4Click
    LookAndFeel.Kind = lfOffice11
  end
  object cxLookupComboBox3: TcxLookupComboBox
    Left = 96
    Top = 240
    Properties.ClearKey = 8
    Properties.KeyFieldNames = 'ID'
    Properties.ListColumns = <
      item
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        FieldName = 'NAME'
      end>
    Properties.ListOptions.ColumnSorting = False
    Properties.ListSource = dsquCatMan
    EditValue = 0
    Style.BorderStyle = ebsFlat
    Style.Shadow = True
    TabOrder = 9
    Width = 227
  end
  object cxCalcEdit3: TcxCalcEdit
    Left = 487
    Top = 240
    EditValue = 0.000000000000000000
    Properties.DisplayFormat = '0.0'
    Style.BorderStyle = ebsFlat
    Style.Shadow = True
    TabOrder = 11
    Width = 69
  end
  object cxCalcEdit4: TcxCalcEdit
    Left = 487
    Top = 268
    EditValue = 0.000000000000000000
    Properties.DisplayFormat = '0.0'
    Style.BorderStyle = ebsFlat
    Style.Shadow = True
    TabOrder = 12
    Width = 69
  end
  object cxSpinEdit1: TcxSpinEdit
    Left = 96
    Top = 304
    Style.LookAndFeel.Kind = lfFlat
    Style.Shadow = True
    StyleDisabled.LookAndFeel.Kind = lfFlat
    StyleFocused.LookAndFeel.Kind = lfFlat
    StyleHot.LookAndFeel.Kind = lfFlat
    TabOrder = 13
    Width = 65
  end
  object cxComboBox1: TcxComboBox
    Left = 316
    Top = 304
    Properties.Items.Strings = (
      '1'
      '2'
      '3')
    Style.BorderStyle = ebsFlat
    Style.Shadow = True
    TabOrder = 14
    Text = '1'
    Width = 69
  end
  object GroupBox1: TGroupBox
    Left = 8
    Top = 444
    Width = 281
    Height = 193
    Caption = #1040#1074#1090#1086#1079#1072#1082#1072#1079
    TabOrder = 15
    object Label28: TLabel
      Left = 12
      Top = 23
      Width = 136
      Height = 13
      Caption = #1044#1086#1073#1072#1074#1086#1095#1085#1099#1081' '#1082#1086#1101#1092#1092#1080#1094#1080#1077#1085#1090
      Transparent = True
    end
    object Label29: TLabel
      Left = 12
      Top = 51
      Width = 119
      Height = 13
      Caption = #1057#1090#1088#1072#1093#1086#1074#1086#1081' '#1079#1072#1087#1072#1089' ('#1076#1085#1077#1081')'
      Transparent = True
    end
    object Label31: TLabel
      Left = 12
      Top = 77
      Width = 127
      Height = 13
      Caption = #1050#1086#1083'-'#1074#1086' '#1076#1085#1077#1081' '#1076#1083#1103' '#1072#1085#1072#1083#1080#1079#1072
    end
    object Label11: TLabel
      Left = 10
      Top = 104
      Width = 111
      Height = 13
      Hint = 
        #1055#1088#1080' '#1079#1085#1072#1095#1077#1085#1080#1080' '#1084#1077#1085#1077#1077' 14 '#1076#1085#1077#1081' '#1089#1095#1080#1090#1072#1077#1090#1089#1103' '#1082#1072#1082' '#1089#1082#1086#1088#1086#1087#1086#1088#1090', '#1087#1088#1080' '#13#10#1079#1085#1072#1095#1077#1085 +
        #1080#1080' 1 '#1076#1077#1085#1100' '#1073#1077#1088#1077#1090#1089#1103' '#1084#1072#1082#1089#1080#1084#1072#1083#1100#1085#1072#1103' '#1088#1077#1072#1083#1080#1079#1072#1094#1080#1103' '#13#10#1079#1072' '#1087#1077#1088#1080#1086#1076' ('#1044#1085#1077#1081' '#1072#1085#1072#1083 +
        #1080#1079#1072')'
      Caption = #1057#1088#1086#1082' '#1088#1077#1072#1083#1080#1079#1072#1094#1080#1080' ('#1040#1047')'
      Transparent = True
    end
    object Label17: TLabel
      Left = 12
      Top = 140
      Width = 141
      Height = 13
      Caption = #1050#1086#1083'-'#1074#1086' '#1074' '#1086#1076#1085#1086#1084' '#1090#1072#1088#1085'. '#1084#1077#1089#1090#1077
      Transparent = True
    end
    object cxCalcEdit7: TcxCalcEdit
      Left = 163
      Top = 17
      EditValue = 0.000000000000000000
      Properties.Alignment.Horz = taRightJustify
      Properties.DisplayFormat = '0.000'
      Style.BorderStyle = ebsFlat
      Style.Shadow = True
      TabOrder = 0
      Width = 106
    end
    object cxCalcEdit12: TcxCalcEdit
      Left = 192
      Top = 133
      EditValue = 0.000000000000000000
      Properties.Alignment.Horz = taRightJustify
      Properties.DisplayFormat = '0.000'
      Style.BorderStyle = ebsFlat
      Style.Shadow = True
      TabOrder = 1
      Width = 77
    end
    object cxCheckBox1: TcxCheckBox
      Left = 13
      Top = 164
      Caption = #1058#1077#1085#1076#1077#1088#1085#1099#1081' '#1090#1086#1074#1072#1088
      Style.BorderStyle = ebsFlat
      TabOrder = 2
      Width = 140
    end
    object cxSpinEdit2: TcxSpinEdit
      Left = 164
      Top = 45
      Style.LookAndFeel.Kind = lfFlat
      Style.Shadow = True
      StyleDisabled.LookAndFeel.Kind = lfFlat
      StyleFocused.LookAndFeel.Kind = lfFlat
      StyleHot.LookAndFeel.Kind = lfFlat
      TabOrder = 3
      Width = 105
    end
  end
  object cxCalcEdit2: TcxCalcEdit
    Left = 487
    Top = 212
    EditValue = 0.000000000000000000
    Properties.DisplayFormat = '0.000'
    Style.BorderStyle = ebsFlat
    Style.Shadow = True
    TabOrder = 16
    Width = 69
  end
  object cxCalcEdit6: TcxCalcEdit
    Left = 488
    Top = 325
    EditValue = 0.000000000000000000
    Properties.Alignment.Horz = taRightJustify
    Properties.DisplayFormat = '0.000'
    Style.BorderStyle = ebsFlat
    Style.Shadow = True
    TabOrder = 17
    Width = 69
  end
  object GroupBox2: TGroupBox
    Left = 8
    Top = 356
    Width = 281
    Height = 83
    Caption = #1057#1086#1089#1090#1072#1074
    TabOrder = 18
    object mSostav: TMemo
      Left = 2
      Top = 15
      Width = 277
      Height = 66
      Align = alClient
      BorderStyle = bsNone
      TabOrder = 0
    end
  end
  object GroupBox3: TGroupBox
    Left = 300
    Top = 356
    Width = 257
    Height = 83
    Caption = #1043#1054#1057#1058
    TabOrder = 19
    object mGost: TMemo
      Left = 2
      Top = 15
      Width = 253
      Height = 66
      Align = alClient
      BorderStyle = bsNone
      TabOrder = 0
    end
  end
  object GroupBox4: TGroupBox
    Left = 300
    Top = 444
    Width = 257
    Height = 121
    Caption = #1040#1083#1082#1086#1075#1086#1083#1100
    TabOrder = 20
    object Label24: TLabel
      Left = 12
      Top = 28
      Width = 57
      Height = 13
      Caption = #1042#1080#1076' '#1090#1086#1074#1072#1088#1072
      Transparent = True
    end
    object Label32: TLabel
      Left = 12
      Top = 56
      Width = 79
      Height = 13
      Caption = #1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1100
    end
    object Label7: TLabel
      Left = 12
      Top = 88
      Width = 48
      Height = 13
      Caption = #1050#1088#1077#1087#1086#1089#1090#1100
    end
    object cxButtonEdit1: TcxButtonEdit
      Left = 100
      Top = 52
      ParentFont = False
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = 4204565
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsBold]
      Style.LookAndFeel.Kind = lfFlat
      Style.Shadow = True
      Style.IsFontAssigned = True
      StyleDisabled.LookAndFeel.Kind = lfFlat
      StyleFocused.LookAndFeel.Kind = lfFlat
      StyleHot.LookAndFeel.Kind = lfFlat
      TabOrder = 0
      Width = 145
    end
    object cxCalcEdit13: TcxCalcEdit
      Left = 128
      Top = 84
      EditValue = 0.000000000000000000
      Properties.DisplayFormat = '0.0'
      Style.BorderStyle = ebsFlat
      Style.LookAndFeel.Kind = lfFlat
      Style.Shadow = True
      StyleDisabled.LookAndFeel.Kind = lfFlat
      StyleFocused.LookAndFeel.Kind = lfFlat
      StyleHot.LookAndFeel.Kind = lfFlat
      TabOrder = 1
      Width = 117
    end
  end
  object cxCalcEdit5: TcxCalcEdit
    Left = 487
    Top = 296
    EditValue = 0.000000000000000000
    Properties.DisplayFormat = '0.000'
    Style.BorderStyle = ebsFlat
    Style.Shadow = True
    TabOrder = 21
    Width = 69
  end
  object cxCheckBox2: TcxCheckBox
    Left = 489
    Top = 16
    Caption = #1058#1054#1055
    Style.BorderStyle = ebsFlat
    TabOrder = 22
    Width = 72
  end
  object cxLookupComboBox1: TcxLookupComboBox
    Left = 96
    Top = 184
    Properties.ClearKey = 8
    Properties.KeyFieldNames = 'ID'
    Properties.ListColumns = <
      item
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        FieldName = 'NAME'
      end>
    Properties.ListOptions.ColumnSorting = False
    Properties.ListSource = dsquCountry
    EditValue = 0
    Style.BorderStyle = ebsFlat
    Style.Shadow = True
    TabOrder = 23
    Width = 227
  end
  object cxLookupComboBox2: TcxLookupComboBox
    Left = 96
    Top = 268
    Properties.ClearKey = 8
    Properties.KeyFieldNames = 'ID'
    Properties.ListColumns = <
      item
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        FieldName = 'NAMEBRAND'
      end>
    Properties.ListOptions.ColumnSorting = False
    Properties.ListSource = dsquBrands
    EditValue = 0
    Style.BorderStyle = ebsFlat
    Style.Shadow = True
    TabOrder = 24
    Width = 227
  end
  object cxLookupComboBox4: TcxLookupComboBox
    Left = 380
    Top = 468
    Properties.ClearKey = 8
    Properties.DropDownAutoSize = True
    Properties.KeyFieldNames = 'ID'
    Properties.ListColumns = <
      item
        Caption = #1050#1086#1076
        MinWidth = 30
        Width = 40
        FieldName = 'ID'
      end
      item
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        Width = 300
        FieldName = 'NAME'
      end>
    Properties.ListOptions.ColumnSorting = False
    Properties.ListSource = dsquAVid
    EditValue = 0
    Style.BorderStyle = ebsFlat
    Style.Shadow = True
    TabOrder = 25
    Width = 165
  end
  object cxLookupComboBox5: TcxLookupComboBox
    Left = 488
    Top = 56
    Properties.ClearKey = 8
    Properties.KeyFieldNames = 'ID'
    Properties.ListColumns = <
      item
        Caption = #1054#1073#1086#1079#1085#1072#1095#1077#1085#1080#1077
        FieldName = 'SID'
      end
      item
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        FieldName = 'COMMENT'
      end>
    Properties.ListOptions.ColumnSorting = False
    Properties.ListSource = dsquCateg
    EditValue = 0
    Style.BorderStyle = ebsFlat
    Style.Shadow = True
    TabOrder = 26
    Width = 69
  end
  object cxComboBox2: TcxComboBox
    Left = 468
    Top = 184
    Properties.Items.Strings = (
      #1053#1044#1057' 18%'
      #1053#1044#1057' 10%'
      #1073#1077#1079' '#1053#1044#1057)
    Style.BorderStyle = ebsFlat
    Style.Shadow = True
    TabOrder = 27
    Text = #1053#1044#1057' 18%'
    Width = 85
  end
  object cxSpinEdit3: TcxSpinEdit
    Left = 172
    Top = 517
    Style.LookAndFeel.Kind = lfFlat
    Style.Shadow = True
    StyleDisabled.LookAndFeel.Kind = lfFlat
    StyleFocused.LookAndFeel.Kind = lfFlat
    StyleHot.LookAndFeel.Kind = lfFlat
    TabOrder = 28
    Width = 105
  end
  object cxSpinEdit4: TcxSpinEdit
    Left = 172
    Top = 545
    Style.LookAndFeel.Kind = lfFlat
    Style.Shadow = True
    StyleDisabled.LookAndFeel.Kind = lfFlat
    StyleFocused.LookAndFeel.Kind = lfFlat
    StyleHot.LookAndFeel.Kind = lfFlat
    TabOrder = 29
    Width = 105
  end
  object cxCalcEdit1: TcxCalcEdit
    Left = 356
    Top = 56
    EditValue = 0.000000000000000000
    Properties.DisplayFormat = '0.0'
    Style.BorderStyle = ebsFlat
    Style.Shadow = True
    TabOrder = 30
    Width = 69
  end
  object cxComboBox3: TcxComboBox
    Left = 320
    Top = 16
    Properties.Items.Strings = (
      #1058#1086#1074#1072#1088
      #1058#1072#1088#1072)
    Style.BorderStyle = ebsFlat
    Style.Shadow = True
    TabOrder = 31
    Text = #1058#1086#1074#1072#1088
    Width = 105
  end
  object amCards: TActionManager
    Left = 344
    Top = 220
    StyleName = 'XP Style'
    object acBarRead: TAction
      Caption = 'acBarRead'
      ShortCut = 123
      OnExecute = acBarReadExecute
    end
    object acTextEdit2: TAction
      Caption = 'acTextEdit2'
      ShortCut = 8237
      OnExecute = acTextEdit2Execute
    end
    object acClear: TAction
      Caption = 'acClear'
      ShortCut = 117
      OnExecute = acClearExecute
    end
  end
  object quCateg: TADOQuery
    Connection = dmMCS.msConnection
    CommandTimeout = 500
    Parameters = <>
    SQL.Strings = (
      'SELECT [ID]'
      '      ,[SID]'
      '      ,[COMMENT]'
      '  FROM [dbo].[CATEG]')
    Left = 308
    Top = 564
    object quCategID: TIntegerField
      FieldName = 'ID'
    end
    object quCategSID: TStringField
      FieldName = 'SID'
      FixedChar = True
      Size = 2
    end
    object quCategCOMMENT: TStringField
      FieldName = 'COMMENT'
      Size = 100
    end
  end
  object dsquCateg: TDataSource
    DataSet = quCateg
    Left = 312
    Top = 616
  end
  object quCountry: TADOQuery
    Connection = dmMCS.msConnection
    CommandTimeout = 500
    Parameters = <>
    SQL.Strings = (
      'SELECT [IDPARENT]'
      '      ,[ID]'
      '      ,[NAME]'
      '      ,[KOD]'
      '  FROM [dbo].[COUNTRY]'
      'order by [NAME]'
      '')
    Left = 380
    Top = 568
    object quCountryIDPARENT: TIntegerField
      FieldName = 'IDPARENT'
    end
    object quCountryID: TIntegerField
      FieldName = 'ID'
    end
    object quCountryNAME: TStringField
      FieldName = 'NAME'
      Size = 50
    end
    object quCountryKOD: TIntegerField
      FieldName = 'KOD'
    end
  end
  object dsquCountry: TDataSource
    DataSet = quCountry
    Left = 384
    Top = 620
  end
  object quCatMan: TADOQuery
    Connection = dmMCS.msConnection
    CommandTimeout = 500
    Parameters = <
      item
        Name = 'ICATMAN'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT [ID]'
      '      ,[ID_PARENT]'
      '      ,[NAME]'
      '      ,[UVOLNEN]'
      '      ,[PASSW]'
      '      ,[MODUL1]'
      '      ,[MODUL2]'
      '      ,[MODUL3]'
      '      ,[MODUL4]'
      '      ,[MODUL5]'
      '      ,[MODUL6]'
      '      ,[BARCODE]'
      '      ,[PATHEXP]'
      '      ,[HOSTFTP]'
      '      ,[LOGINFTP]'
      '      ,[PASSWFTP]'
      '      ,[SCLI]'
      '      ,[ALLSHOPS]'
      '      ,[CATMAN]'
      '      ,[POSTACCESS]'
      '  FROM [dbo].[RPERSONAL]'
      '  where [CATMAN]=1 or [ID_PARENT]=:ICATMAN'
      'order by [NAME]')
    Left = 444
    Top = 568
    object quCatManID: TIntegerField
      FieldName = 'ID'
    end
    object quCatManID_PARENT: TIntegerField
      FieldName = 'ID_PARENT'
    end
    object quCatManNAME: TStringField
      FieldName = 'NAME'
      Size = 200
    end
    object quCatManUVOLNEN: TSmallintField
      FieldName = 'UVOLNEN'
    end
    object quCatManPASSW: TStringField
      FieldName = 'PASSW'
    end
    object quCatManMODUL1: TSmallintField
      FieldName = 'MODUL1'
    end
    object quCatManMODUL2: TSmallintField
      FieldName = 'MODUL2'
    end
    object quCatManMODUL3: TSmallintField
      FieldName = 'MODUL3'
    end
    object quCatManMODUL4: TSmallintField
      FieldName = 'MODUL4'
    end
    object quCatManMODUL5: TSmallintField
      FieldName = 'MODUL5'
    end
    object quCatManMODUL6: TSmallintField
      FieldName = 'MODUL6'
    end
    object quCatManBARCODE: TStringField
      FieldName = 'BARCODE'
    end
    object quCatManPATHEXP: TStringField
      FieldName = 'PATHEXP'
      Size = 100
    end
    object quCatManHOSTFTP: TStringField
      FieldName = 'HOSTFTP'
      Size = 100
    end
    object quCatManLOGINFTP: TStringField
      FieldName = 'LOGINFTP'
      Size = 100
    end
    object quCatManPASSWFTP: TStringField
      FieldName = 'PASSWFTP'
      Size = 100
    end
    object quCatManSCLI: TStringField
      FieldName = 'SCLI'
      Size = 200
    end
    object quCatManALLSHOPS: TSmallintField
      FieldName = 'ALLSHOPS'
    end
    object quCatManCATMAN: TSmallintField
      FieldName = 'CATMAN'
    end
    object quCatManPOSTACCESS: TSmallintField
      FieldName = 'POSTACCESS'
    end
  end
  object dsquCatMan: TDataSource
    DataSet = quCatMan
    Left = 444
    Top = 620
  end
  object quBrands: TADOQuery
    Connection = dmMCS.msConnection
    CommandTimeout = 500
    Parameters = <>
    SQL.Strings = (
      'SELECT [ID]'
      '      ,[NAMEBRAND]'
      '  FROM [dbo].[BRANDS]')
    Left = 508
    Top = 572
    object quBrandsID: TIntegerField
      FieldName = 'ID'
    end
    object quBrandsNAMEBRAND: TStringField
      FieldName = 'NAMEBRAND'
      Size = 50
    end
  end
  object dsquBrands: TDataSource
    DataSet = quBrands
    Left = 512
    Top = 624
  end
  object quAVid: TADOQuery
    Connection = dmMCS.msConnection
    CommandTimeout = 500
    Parameters = <>
    SQL.Strings = (
      'SELECT [ID]'
      '      ,[NAME]'
      '      ,[AFM]'
      '  FROM [dbo].[AVID]')
    Left = 144
    Top = 352
    object quAVidID: TIntegerField
      FieldName = 'ID'
    end
    object quAVidNAME: TStringField
      FieldName = 'NAME'
      Size = 150
    end
    object quAVidAFM: TSmallintField
      FieldName = 'AFM'
    end
  end
  object dsquAVid: TDataSource
    DataSet = quAVid
    Left = 144
    Top = 400
  end
end
