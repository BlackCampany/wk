unit PaswClosePer;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxLookAndFeelPainters, StdCtrls, cxButtons, ExtCtrls, ComCtrls,
  DB, ADODB, cxControls, cxContainer, cxEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxImageComboBox, cxDBEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, Menus, cxGraphics;

type
  TfmPerA_ClosePer = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Button1: TcxButton;
    Button2: TcxButton;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    ComboBox1: TcxLookupComboBox;
    Edit1: TcxTextEdit;
    procedure FormCreate(Sender: TObject);
    procedure Edit1Enter(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ComboBox1PropertiesChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmPerA_ClosePer: TfmPerA_ClosePer;
  CountEnter:Integer;
  iLastP:Integer;

implementation

uses Un1, ClosePer;

{$R *.dfm}

procedure TfmPerA_ClosePer.FormCreate(Sender: TObject);
begin
  Left:=1;
  Top:=1;
  CountEnter:=0;
  iLastP:=Person.Id;
  Person.Id:=0;
  Person.AllShop:=0;
  Person.Name:='';

  with fmClosePer do
  begin
    if msConn.Connected then
    begin
      quPassw.SQL.Add('SELECT [ID],[ID_PARENT],[NAME],[UVOLNEN],[PASSW],[MODUL1],[MODUL2],[MODUL3],[MODUL4],[MODUL5],[MODUL6],[BARCODE],[ALLSHOPS]');
      quPassw.SQL.Add('FROM [dbo].[RPERSONAL]');
      quPassw.SQL.Add('where Uvolnen=1 and Modul3=0');
      quPassw.SQL.Add('order by NAME');

      quPassw.Active:=True;
      ComboBox1.EditValue:=iLastP;
    end;
  end;
end;

procedure TfmPerA_ClosePer.Edit1Enter(Sender: TObject);
begin
  Text:='';
end;

procedure TfmPerA_ClosePer.Button1Click(Sender: TObject);
begin
  with fmClosePer do
  begin
    if quPassw.Locate('ID',ComboBox1.EditValue,[]) then
    begin
      if quPasswPASSW.AsString=Edit1.Text then
      begin //������� ������
        Person.Id:=quPasswID.AsInteger;
        Person.Name:=quPasswNAME.AsString;
        Person.AllShop:=quPasswALLSHOPS.AsInteger;
        quPassw.Active:=False;
        fmPerA_ClosePer.ModalResult:=mrOk;
      end
      else
      begin
        inc(CountEnter);
        if CountEnter>2 then close;
        showmessage('������ ������������. �������� '+IntToStr(3-CountEnter)+' �������.');
        Edit1.Text:='';
        Edit1.SetFocus;
      end;
    end;
  end;
end;

procedure TfmPerA_ClosePer.Button2Click(Sender: TObject);
begin
  fmClosePer.quPassw.Active:=False;
  Close;
end;

procedure TfmPerA_ClosePer.FormShow(Sender: TObject);
begin
  Edit1.SetFocus;
  Edit1.SelectAll;
end;

procedure TfmPerA_ClosePer.ComboBox1PropertiesChange(Sender: TObject);
begin
  with fmClosePer do
  begin
//    Label2.Caption:=quPasswID.AsString;
//    Label2.Caption:=ComboBox1.EditValue;
  end;
end;

end.
