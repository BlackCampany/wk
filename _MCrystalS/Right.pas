unit Right;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ExtCtrls, ImgList, StdCtrls,
  Buttons, cxLookAndFeelPainters, cxButtons, cxControls, cxContainer,
  cxEdit, cxTextEdit, cxCheckBox, Grids, DBGrids, Menus, cxStyles,
  cxCustomData, cxGraphics, cxFilter, cxData, DB, cxDBData, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxDataStorage, cxImageComboBox, ADODB,
  cxMaskEdit, cxDropDownEdit, XPStyleActnCtrls, ActnList, ActnMan;

type
  TfmRight = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Splitter1: TSplitter;
    Panel2: TPanel;
    ImageList1: TImageList;
    TreePersonal: TTreeView;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet3: TTabSheet;
    Panel3: TPanel;
    Bevel1: TBevel;
    Label1: TLabel;
    Edit1: TcxTextEdit;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    Edit2: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    Edit3: TEdit;
    GroupBox1: TGroupBox;
    CheckBox1: TcxCheckBox;
    CheckBox2: TcxCheckBox;
    CheckBox3: TcxCheckBox;
    CheckBox4: TcxCheckBox;
    ViewFuncList: TcxGridDBTableView;
    LevelFuncList: TcxGridLevel;
    GridFuncList: TcxGrid;
    Button1: TButton;
    CheckBox5: TcxCheckBox;
    CheckBox6: TcxCheckBox;
    CheckBox7: TcxCheckBox;
    CheckBox8: TcxCheckBox;
    TabSheet4: TTabSheet;
    Panel4: TPanel;
    cxCheckBox1: TcxCheckBox;
    Panel5: TPanel;
    ViShop: TcxGridDBTableView;
    LeShop: TcxGridLevel;
    GridShop: TcxGrid;
    ViShopAll: TcxGridDBTableView;
    LeShopAll: TcxGridLevel;
    GridShopAll: TcxGrid;
    Panel6: TPanel;
    Panel7: TPanel;
    Panel8: TPanel;
    Label4: TLabel;
    Label5: TLabel;
    quShopsToSel: TADOQuery;
    dsquShopsToSel: TDataSource;
    ViShopAllId: TcxGridDBColumn;
    ViShopAllName: TcxGridDBColumn;
    quPersonalSh: TADOQuery;
    dsquPersonalSh: TDataSource;
    quPersonalShIdP: TIntegerField;
    quPersonalShiShop: TIntegerField;
    quPersonalShsShop: TStringField;
    ViShopiShop: TcxGridDBColumn;
    ViShopsShop: TcxGridDBColumn;
    cxCheckBox2: TcxCheckBox;
    Panel9: TPanel;
    cxButton4: TcxButton;
    cxButton5: TcxButton;
    acPersShop: TActionManager;
    acAddSh: TAction;
    acDelSh: TAction;
    quShopsToSelId: TIntegerField;
    quShopsToSelName: TStringField;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    ViewFuncListID_PERSONAL: TcxGridDBColumn;
    ViewFuncListNAME: TcxGridDBColumn;
    ViewFuncListPREXEC: TcxGridDBColumn;
    ViewFuncListCOMMENT: TcxGridDBColumn;
    procedure TreePersonalExpanding(Sender: TObject; Node: TTreeNode;
      var AllowExpansion: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure CheckBox4Enter(Sender: TObject);
    procedure TreePersonalChanging(Sender: TObject; Node: TTreeNode;
      var AllowChange: Boolean);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure TreePersonalChange(Sender: TObject; Node: TTreeNode);
    procedure TreeClassifExpanding(Sender: TObject; Node: TTreeNode;
      var AllowExpansion: Boolean);
    procedure TreeClassifChange(Sender: TObject; Node: TTreeNode);
    procedure PageControl1Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure PageControl1Change(Sender: TObject);
    procedure cxGrid1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ViewFuncListMouseDown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure Button1Click(Sender: TObject);
    procedure cxCheckBox1PropertiesChange(Sender: TObject);
    procedure cxCheckBox1Enter(Sender: TObject);
    procedure acAddShExecute(Sender: TObject);
    procedure acDelShExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmRight: TfmRight;

implementation

uses Dm, MainAdm, Un1, Passw;

{$R *.dfm}

procedure TfmRight.TreePersonalExpanding(Sender: TObject; Node: TTreeNode;
  var AllowExpansion: Boolean);
begin
  if Node.getFirstChild.Data = nil then
  begin
    Node.DeleteChildren;
    ExpandLevel(Node,TreePersonal,dmMCS.quPersonal);
  end;
end;

procedure TfmRight.FormCreate(Sender: TObject);
begin
  //������ ��� ����
  Edit1.Text:=''; Edit2.Text:=''; Edit3.Text:='';
  CheckBox1.Checked:=False; CheckBox2.Checked:=False; CheckBox3.Checked:=False; CheckBox4.Checked:=False;
  CheckBox8.Checked:=False;
  //�������� ������� �������
  ExpandLevel(nil,TreePersonal,dmMCS.quPersonal);
  PageControl1.ActivePageIndex:=0;
end;

procedure TfmRight.CheckBox4Enter(Sender: TObject);
begin
  cxButton2.Enabled:=True;
end;

procedure TfmRight.TreePersonalChanging(Sender: TObject; Node: TTreeNode;
  var AllowChange: Boolean);
begin
  if cxButton2.Enabled then
  begin
    showmessage('��������� �� ���������. ������� ������ "���������" ��� ����������, ��� ������ "������" ��� ������ ���������.');
    AllowChange:=False;
  end;
end;

procedure TfmRight.cxButton2Click(Sender: TObject);
Var StrWk:String;
begin
  //�������� ���������
  if Edit2.Text<>Edit3.Text then
  begin
    showmessage('������ ������ �����������, ���������� ��� ���.');
    exit;
  end;
  with dmMCS do
  begin
    try
      taPersonal.Edit;
      taPersonalName.AsString:=Edit1.Text;
      if CheckBox1.Checked then taPersonalModul1.AsInteger:=0 else taPersonalModul1.AsInteger:=1;
      if CheckBox2.Checked then taPersonalModul2.AsInteger:=0 else taPersonalModul2.AsInteger:=1;
      if CheckBox3.Checked then taPersonalModul3.AsInteger:=0 else taPersonalModul3.AsInteger:=1;
      if CheckBox5.Checked then taPersonalModul4.AsInteger:=0 else taPersonalModul4.AsInteger:=1;
      if CheckBox6.Checked then taPersonalModul5.AsInteger:=0 else taPersonalModul5.AsInteger:=1;
      if CheckBox7.Checked then taPersonalModul6.AsInteger:=0 else taPersonalModul6.AsInteger:=1;

      taPersonalCatMan.AsInteger:=CheckBox8.EditValue;
      taPersonalUVOLNEN.AsInteger:=CheckBox4.EditValue;

      strwk:=Edit2.Text;
      while pos(' ',strwk)>0 do delete(Strwk,pos(' ',strwk),1);
      taPersonalPassw.AsString:=StrWk;

      taPersonalAllShops.AsInteger:=cxCheckBox1.EditValue;    //1 �� ���������
      taPersonalPostAccess.AsInteger:=cxCheckBox2.EditValue; //0 �� ���������

      taPersonal.Post;
//��������� � ���� ������� ���� ������ ��������
      TreePersonal.Items.BeginUpdate;
      TreePersonal.Selected.Text:=Edit1.Text;
      TreePersonal.Items.EndUpdate;
      TreePersonal.Refresh;

    except
    end;
  end;
  cxButton2.Enabled:=False;
end;

procedure TfmRight.cxButton1Click(Sender: TObject);
Var StrWk:String;
begin
  if Edit2.Text<>Edit3.Text then
  begin
    showmessage('������ ������ �����������, ���������� ��� ���.');
    exit;
  end;
  //�������� ���������
  with dmMCS do
  begin
    try
      taPersonal.Edit;
      taPersonalName.AsString:=Edit1.Text;
      if CheckBox1.Checked then taPersonalModul1.AsInteger:=0 else taPersonalModul1.AsInteger:=1;
      if CheckBox2.Checked then taPersonalModul2.AsInteger:=0 else taPersonalModul2.AsInteger:=1;
      if CheckBox3.Checked then taPersonalModul3.AsInteger:=0 else taPersonalModul3.AsInteger:=1;
      if CheckBox5.Checked then taPersonalModul4.AsInteger:=0 else taPersonalModul4.AsInteger:=1;
      if CheckBox6.Checked then taPersonalModul5.AsInteger:=0 else taPersonalModul5.AsInteger:=1;
      if CheckBox7.Checked then taPersonalModul6.AsInteger:=0 else taPersonalModul6.AsInteger:=1;

      taPersonalCatMan.AsInteger:=CheckBox8.EditValue;

      taPersonalUVOLNEN.AsInteger:=CheckBox4.EditValue;

      strwk:=Edit2.Text;
      while pos(' ',strwk)>0 do delete(Strwk,pos(' ',strwk),1);
      taPersonalPassw.AsString:=StrWk;
      taPersonal.Post;
    except
    end;
  end;
  cxButton2.Enabled:=False;
  PageControl1.ActivePageIndex:=1;
end;

procedure TfmRight.cxButton3Click(Sender: TObject);
begin
  cxButton2.Enabled:=False;
  Close;
end;

procedure TfmRight.TreePersonalChange(Sender: TObject; Node: TTreeNode);
Var Id:Integer;
begin
  id:=Integer(TreePersonal.Selected.Data);
  with dmMCS do
  begin
    taPersonal.Locate('ID',ID,[]);
    if taPersonalId_Parent.AsInteger=0 then
    begin  //������ �������������
      Edit1.Text:=taPersonalName.AsString;

      Edit2.Text:='';
      Edit3.Text:='';
      CheckBox1.Checked:=False; CheckBox2.Checked:=False; CheckBox3.Checked:=False; CheckBox4.Checked:=False;CheckBox5.Checked:=False;CheckBox6.Checked:=False; CheckBox7.Checked:=False; CheckBox8.Checked:=False;
      Edit2.Enabled:=False; Edit3.Enabled:=False;
      CheckBox1.Enabled:=False; CheckBox2.Enabled:=False; CheckBox3.Enabled:=False; CheckBox4.Enabled:=False;  CheckBox5.Enabled:=False;
      CheckBox5.Enabled:=False; CheckBox6.Enabled:=False; CheckBox7.Enabled:=False;
      CheckBox8.Enabled:=False;

      cxCheckBox1.Checked:=True;
      Panel5.Visible:=False;
      quPersonalSh.Active:=False;

    end
    else //���������� ������������
    begin
      Edit2.Enabled:=True; Edit3.Enabled:=True;
      CheckBox1.Enabled:=True; CheckBox2.Enabled:=True; CheckBox3.Enabled:=True; CheckBox4.Enabled:=True;  CheckBox5.Enabled:=True;
      CheckBox5.Enabled:=True; CheckBox6.Enabled:=True; CheckBox7.Enabled:=True;
      CheckBox8.Enabled:=True;
      Edit1.Text:=taPersonalName.AsString;
      Edit2.Text:=taPersonalPassw.AsString;
      while Length(Edit2.Text)<15 do Edit2.Text:=Edit2.Text+' ';
      Edit3.Text:=Edit2.Text;

      if taPersonalModul1.AsInteger=0 then CheckBox1.Checked:=True else CheckBox1.Checked:=False;
      if taPersonalModul2.AsInteger=0 then CheckBox2.Checked:=True else CheckBox2.Checked:=False;
      if taPersonalModul3.AsInteger=0 then CheckBox3.Checked:=True else CheckBox3.Checked:=False;
      if taPersonalModul4.AsInteger=0 then CheckBox5.Checked:=True else CheckBox5.Checked:=False;
      if taPersonalModul5.AsInteger=0 then CheckBox6.Checked:=True else CheckBox6.Checked:=False;
      if taPersonalModul6.AsInteger=0 then CheckBox7.Checked:=True else CheckBox7.Checked:=False;


      CheckBox4.EditValue:=taPersonalUVOLNEN.AsInteger;

      CheckBox8.EditValue:=taPersonalCatMan.AsInteger;

      cxCheckBox1.EditValue:=taPersonalAllShops.AsInteger;
      if taPersonalAllShops.AsInteger=1 then
      begin
        Panel5.Visible:=False;
        quPersonalSh.Active:=False;
      end else
      begin
        quPersonalSh.Active:=False;
        quPersonalSh.SQL.Clear;
        quPersonalSh.SQL.Add('SELECT [IdP]');
        quPersonalSh.SQL.Add(',[iShop]');
        quPersonalSh.SQL.Add(',sShop=(Select [Name] from [dbo].[Shops] where [Id]=[iShop])');
        quPersonalSh.SQL.Add('FROM [dbo].[RPersonalShop]');
        quPersonalSh.SQL.Add('where [IdP]='+its(taPersonalID.AsInteger));
        quPersonalSh.SQL.Add('order by [iShop]');
        quPersonalSh.Active:=True;

        Panel5.Visible:=True;
      end;

      cxCheckBox2.EditValue:=taPersonalPostAccess.AsInteger;
    end;

    quFuncList.Active:=False;
    quFuncList.Parameters.ParamByName('PERSONAL').Value:=ID;
    quFuncList.Active:=True;
    delay(10);
  end;
end;

procedure TfmRight.TreeClassifExpanding(Sender: TObject; Node: TTreeNode;
  var AllowExpansion: Boolean);
begin
  if Node.getFirstChild.Data = nil then
  begin
//    Node.DeleteChildren;
//    RExpandLevel(Node,TreeClassif,dmC.quClassif,dmC.taPersonalID.AsInteger);
  end;
end;

procedure TfmRight.TreeClassifChange(Sender: TObject; Node: TTreeNode);
begin
//  dmC.taClassif.Locate('ID',Integer(TreeClassif.Selected.Data),[]);
end;

procedure TfmRight.PageControl1Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
  if cxButton2.Enabled then
  begin
    showmessage('��������� �� ���������. ������� ������ "���������" ��� ����������, ��� ������ "������" ��� ������ ���������.');
    AllowChange:=False;
  end;
end;

procedure TfmRight.PageControl1Change(Sender: TObject);
begin
  if PageControl1.ActivePageIndex=0 then
  begin
    cxButton1.Enabled:=True;
    cxButton3.Enabled:=True;
  end
  else
  begin
    cxButton1.Enabled:=False;
    cxButton3.Enabled:=False;
  end;
end;

procedure TfmRight.cxGrid1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Button=mbRight then
  begin
    showmessage('22');
  end;
end;

procedure TfmRight.ViewFuncListMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbRight then
  begin
    with dmMCS do
    begin
      delay(10);
      quFuncList.Edit;
      if quFuncListprExec.AsString='F' then quFuncListprExec.AsString:='T'
      else quFuncListprExec.AsString:='F';
      quFuncList.Post;
      delay(10);
      GridFuncList.Refresh;
    end;
  end;
end;

procedure TfmRight.Button1Click(Sender: TObject);
begin
{ fmPassw.ShowModal;
  if fmPassw.ModalResult=mrOk then
  begin
    showmessage('Ok');
  end
  else
  begin
    showmessage('Bad');
  end;}
end;

procedure TfmRight.cxCheckBox1PropertiesChange(Sender: TObject);
begin
  if cxCheckBox1.Checked then Panel5.Visible:=False else
  begin
    quPersonalSh.Active:=False;
    quPersonalSh.SQL.Clear;
    quPersonalSh.SQL.Add('SELECT [IdP]');
    quPersonalSh.SQL.Add(',[iShop]');
    quPersonalSh.SQL.Add(',sShop=(Select [Name] from [dbo].[Shops] where [Id]=[iShop])');
    quPersonalSh.SQL.Add('FROM [dbo].[RPersonalShop]');
    quPersonalSh.SQL.Add('where [IdP]='+its(dmMCS.taPersonalID.AsInteger));
    quPersonalSh.SQL.Add('order by [iShop]');
    quPersonalSh.Active:=True;

    Panel5.Visible:=True;
  end;
end;

procedure TfmRight.cxCheckBox1Enter(Sender: TObject);
begin
  cxButton2.Enabled:=True;
end;

procedure TfmRight.acAddShExecute(Sender: TObject);
Var iSh:INteger;
begin
  with dmMCS do
  begin
    iSh:=quShopsToSelId.AsInteger;
    if quPersonalSh.Locate('iShop',iSh,[])=False then
    begin
      quPersonalSh.Append;
      quPersonalShIdP.AsInteger:=dmMCS.taPersonalID.AsInteger;
      quPersonalShiShop.AsInteger:=iSh;
      quPersonalSh.Post;

      quPersonalSh.Active:=False;
      quPersonalSh.SQL.Clear;
      quPersonalSh.SQL.Add('SELECT [IdP]');
      quPersonalSh.SQL.Add(',[iShop]');
      quPersonalSh.SQL.Add(',sShop=(Select [Name] from [dbo].[Shops] where [Id]=[iShop])');
      quPersonalSh.SQL.Add('FROM [dbo].[RPersonalShop]');
      quPersonalSh.SQL.Add('where [IdP]='+its(dmMCS.taPersonalID.AsInteger));
      quPersonalSh.SQL.Add('order by [iShop]');
      quPersonalSh.Active:=True;

    end;
  end;
end;

procedure TfmRight.acDelShExecute(Sender: TObject);
begin
  if quPersonalSh.RecordCount>0 then quPersonalSh.Delete;
end;

end.
