unit Un1;

interface
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  IniFiles, ADODB, Variants, IdGlobal, ComCtrls, EasyCompression,
  cxGridDBTableView,cxGridDBBandedTableView,DB,DBClient,ComObj, ActiveX, Excel2000, OleServer, ExcelXP,
  cxCustomData, dxmdaset, StrUtils, cxGridCustomTableView, cxMemo;

{  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  IniFiles, ADODB, Variants, IdGlobal, ComCtrls, EasyCompression,
  cxGridDBTableView,DB,DBClient,ComObj, ActiveX, Excel2000, OleServer, ExcelXP,
  cxCustomData, dxmdaset, pvtables, pvsqltables, btvtables, sqldataset, cxMemo,WinSpool,
  cxGridCustomTableView,cxGridDBBandedTableView;
}

procedure Delay(MSecs: Longint);
Procedure CheckNodeOn(Node:TTreeNode;bOn:Boolean);
Procedure ExpandLevel( Node : TTreeNode; Tree:TTreeView; quTree:TADOQuery);
Procedure RExpandLevel( Node : TTreeNode; Tree:TTreeView; quTree:TADOQuery;PersonalId:Integer);
Procedure RefreshTree(Tree:TTreeView; quTree:TADOQuery;PersonalId:Integer);
procedure WriteHistory(Strwk_: string);
procedure WriteLog(Strwk_: string);

Procedure ReadIni;
Procedure WriteIni;

function RoundEx( X: Double ): Integer;
function RoundVal( X: Double ): Double;
function RV( X: Double ): Double;
function R10000( X: Double ): Double;

procedure prExportExel1(Vi:tcxGridDBTableView;dsT:tDataSource;taT:tClientDataSet);
procedure prExportExel2(Vi:tcxGridDBTableView;dsT:tDataSource;taT:tADOQuery);

procedure prNExportExel3(Vi:tcxGridDBTableView);
procedure prNExportExel4(Vi:tcxGridDBTableView);
procedure prNExportExel5(Vi:TcxGridDBBandedTableView);
procedure prNExportExel6(Vi:tcxGridDBTableView);
procedure prNExportExel7(Vi:tcxGridDBTableView);

procedure prNExportExel15(Vi:tcxGridDBTableView); //���� �� ��������� prNExportExel5 

Procedure prCreateVi(ta:TClientDataSet;V:TcxGridDBTableView);

function R1000( X: Double ): Double;

function IsOLEObjectInstalled(Name: String): boolean;

Procedure CardsExpandLevel( Node : TTreeNode; Tree:TTreeView; quTree:TADOQuery);
procedure CloseTa(taT:tClientDataSet);
procedure CloseTe(taT:tdxMemData);

procedure prCalcSumNac(i1,i2,i3:Integer; V:TcxGridDBTableView);
procedure CalcAvg1(i1,i2,i3:Integer; V:TcxGridDBBandedTableView);

procedure prCalcDefGroup(i1,i2,i3,iParent:Integer; V:TcxGridDBTableView);
procedure prCalcOborach(i1,i2,i3,i4,iParent:Integer; V:TcxGridDBTableView);
//procedure prCalcDefGroup2(i1,i2,i3,iParent:Integer; V:TcxGridDBBandedTableView);
function IsGroupingRow(ARowInfo: TcxRowInfo; ADataController: TcxCustomDataController): Boolean;
Function fs(rSum:Real):String;
Function its(i:Integer):String;
function GetFileVersion(const FileName: string): string;
function DeleteSpaces(Str: string): string;
function DelP(S:String):String;
Function TrimStr(StrIn:String):String;
function ds(d:TDateTime):String;
function ds1(d:TDateTime):String;
function dst(d:TDateTime):String;
function FindDayWeek(iDate:INteger):INteger;
Function fts(rSum:Real):String;
function prRoundPack(rQ,rQPack:Real; iPack:INteger):Real;
procedure prWH(Strwk_: string;Memo1:TcxMemo);
function RoundHi( X: Double ): Integer;
function fmdt: String;
procedure prCalcSumAv(i1,i2,i3:Integer; V:TcxGridDBTableView);
procedure prCalcDefGroupAv(i1,i2,i3,iParent:Integer; V:TcxGridDBTableView);
function fDateToKvartal(iCurD:INteger):Integer;
Function DToYY(tD:TDateTime):INteger;
procedure prWriteStart(Strwk_: string);
function GetCurrentUserName: string;
function fDWeek(iDay:INteger):Integer;
function sdtoint(s:String):INteger;
procedure prRowFocus(Vi:tcxGridDBTableView);
function fmt:String;
procedure prExpandVi(Vi:tcxGridDBTableView;bExpand:Boolean);



procedure WriteHistorySMS(Strwk_: string);
Procedure Readcfg;
Function SOnlyDigit(S:String):String;
Procedure WriteColor;
procedure prWriteLog(Strwk_: string);
Procedure WriteCheckNum;
Function ToStandart(S:String):String;
function s1(S:String):String;
Function fTestKey(Key:Word):Boolean;


type TPerson = record
     Id:Integer;
     Name:String;
     Modul:String;
     WinName:String;
     CatMan:SmallInt;
     AllShop:SmallINt;
     end;

     TSMSSet = record
     iSend,Port,TimeSec,TimeSecMain:Integer;
     ToEml:String;
     Host,UserName,Passw,From:String;
     end;

     TCommonSet = record
     PathExport:String;
     PathExportBuh:String;
     FileExp:String;
     PathImport:String;
     FtpImport:String;
     PathHistory:String;
     PathReport:String;
     PathArh:String;
     NetPath:String;
     TrfPath:String;
//     CashPath:String;
     POSLoad,POSReport,POSOper:String;
     UKM4:INteger;
     h12:INteger;
     AutoLoadCash:INteger;

     PeriodPing:INteger;
     CountStart:INteger;
     DateBeg,DateEnd:TDateTime;
     TrDocAutoStart:String;
     TrDocDay:Integer;
     DBName:String;
     Depart,DepartFil:INteger;
     BonusPr:INteger;
     WorkerName:String;
     ZTimeShift:String;

     FtpExport:String;
     HostIp:String;
     HostUser:String;
     HostPassw:string;

     NoFis:Integer;
     CashNum:Integer;
     ComDelay:INteger;
     CashChNum,iCashPort,CashZ:INteger;
     PortCash,TypeFis:String;
     SpecChar:Integer;

     UseCashBase:INteger;
     iP:String;
     TmpPath:String;
     CatManGroup:Integer;
     iMin,iMax,iMinVes,iMaxVes:Integer;
     Prefix,PrefixVes:String;
     CutTailCode:INteger;
     DaysTestMove:INteger;
     CBIP:string;
     end;

     TClient = Record
     Bar:String;
     ID:Integer;
     ZType:ShortInt;
     DProc:Real;
     end;

     TTab = record
     Id_Personal:Integer;
     Name:String;
     OpenTime:TDateTime;
     NumTable:String[20];
     Quests:Integer;
     iStatus:Integer; //0 - ��������������    1- �������
     Id:Integer;
     DBar,DBar1:String;
     DPercent:Real;
     DName:String;
     Summa:Real;
     PBar:String;
     PName:String;
     iNumZ:INteger;
     SaleT:SmallINt;
     DType:SmallINt;
     end;



     TGm = Record
     DateM:TDateTime;
     QIn,QOut,QVnIn,QVnOut,QReal,QInv,QRemn:Real;
     rPr,rSum:Currency;
     end;

     TUserColor = Record
     Color1,Color2,Color3:TColor;
     Main:TColor;
     end;

     TPos = Record
     Id:INteger;
     IdGr:INteger;
     Name:String;
     end;

     TCurVal = record
     IdShop,IdDep:Integer;
     end;



Var CurDir:String;
    Person:TPerson;
    sFormatDate:String = 'mm.dd.yyyy';
    CommonSet:TCommonSet;
    bAdd:Boolean = False;
    MaxNum:INteger;
    bAddC:Boolean = False;
    bAddB:Boolean = False;
    bEditC:Boolean = False;
    bViewC:Boolean = False;
    FindVal:Integer = 1; //�� ��������
    bTransp:Boolean = False;
    Ftr:TextFile;
    bFtp:Boolean = False;
    bNotPr:Boolean = False;
    bFtpOk:Boolean = True;
    iCountSec:INteger;
    bClearStatusBar:Boolean = False;
    bRefreshCard:Boolean = False;
    bRefreshCli:Boolean = False;
    Client:TClient;
    bDr:Boolean = False;
    bAddTCard:Boolean = False;
    bAddTCource:Boolean = False;
    bAddOplCurce:Boolean = False;
    bAddZ:Boolean = False;
    UserColor:TUserColor;
    Tab:TTab;
    Operation:INteger;
    iCh,iOpCh:INteger;
    SMSSet:TSMSSet;
    PosP:TPos;
    iDirect:INteger; //��������� ���� ����� �� �������� ���������
    sGridIni:String;
    sFormIni:String;

    DrvScaleCas: OleVariant;
    ScaleCas:Boolean = False;

    CurDep:INteger;
    iDel:INteger;

    CurVal:TCurVal;
    StrMess:string;


Const CurIni:String = 'Profiles.ini';
      GridIni:String = 'ProfilesGr.ini';
      FormIni:String = 'ProfilesF.ini';
      CashNon:String = 'Cash01.non';
      CashLdd:String = 'Cash01.ldd';
      CashUpd:String = 'Cash01.upd';
      CashCng:String = 'Cash01.cng';
      CashBsv:String = 'Cash01.bsv';
      R:String = ';';
      TrExt:String = '.cr~';
      levelValue: array [0..9] of TECLCompressionLevel =
              (eclNone, zlibFastest, zlibNormal, zlibMax, bzipFastest,
               bzipNormal, bzipMax, ppmFastest, ppmNormal, ppmMax);
      odInac:String = 'odNoFocusRect_';
      WarnMess:String = '����� �������� ��������� �������.';
      VerMess:String = 'Ver for Sion (pr. Ivanchenko)';
      CaptionMain:String = '������� ����� � ������������ ������� �����.';


implementation


procedure prExpandVi(Vi:tcxGridDBTableView;bExpand:Boolean);
begin
  if bExpand then
  begin
    Vi.BeginUpdate;
    Vi.DataController.Groups.FullExpand;
    Vi.EndUpdate;
  end else  //Collapse
  begin
    Vi.BeginUpdate;
    Vi.DataController.Groups.FullCollapse;
    Vi.EndUpdate;
  end;
end;



//------------------------------------------------------------------------------
//   ��������� ������������� � ������� i3 ������� ������� ����� ��������� i1 � i2
//------------------------------------------------------------------------------
procedure prNExportExel15(Vi:tcxGridDBTableView);
Var ExcelApp, Workbook, Range, Cell1, Cell2, ArrayData  : Variant;
    iCol,iRow,i:Integer;
    RowInf:TcxRowInfo;
    iRowV,J,k,iGCount,iLev,iItem:Integer;
    StrWk:String;
    arCol:array[1..20] of integer;
    iCountDG,ll,cc,ff:INteger;
    sField,sColumn,sSum:String;
    SumGr:tcxGridDBTableSummaryItem;
    Rec:TcxCustomGridRecord;
    rVal:Real;

    StarOffice,StarDesktop,Document,Sheets,Sheet,ooRange,ooArrayData:Variant;
    ii,jj:integer;
    destSoft:String;


function IsGroupingRow(ARowInfo: TcxRowInfo; ADataController: TcxCustomDataController): Boolean;
begin
  with ADataController do
    Result := ARowInfo.Level <> Groups.GroupingItemCount;
end;

begin
//������� � ������
//  Vi.BeginUpdate;

  destSoft:='non'; //������� �� �����

  if IsOLEObjectInstalled('Excel.Application')
    then
      begin
        ExcelApp := CreateOleObject('Excel.Application');
        ExcelApp.Application.EnableEvents := false;
        //� ������� �����
        Workbook := ExcelApp.WorkBooks.Add;
        destSoft := 'mse';  //excell
      end
    else destSoft:='non';

  //destSoft:='non';  // ����� ��� ����� ���� �����, "����" ������ ��� -- ��� ������ ������ ���� ���������

  if ((IsOLEObjectInstalled('com.sun.star.ServiceManager')) and (destSoft='non'))
    then
      begin
        StarOffice := CreateOleObject('com.sun.star.ServiceManager');
        StarDesktop := StarOffice.createInstance('com.sun.star.frame.Desktop');

        Document := StarDesktop.LoadComponentFromURL(
                    'private:factory/scalc', '_blank', 0,
                    VarArrayCreate([-1, -1], varVariant));
        Document.getCurrentController.getFrame.getContainerWindow.setVisible(False);
        Sheets := Document.getSheets;
        //Sheet := Sheets.getByName('����1');
        Sheet := Sheets.getByIndex(0);
        destSoft:='ooc';
      end;

  if destSoft='non' then exit;


  for i:=1 to 20 do arCol[i]:=-1; //������� ��� �������

  iCol:=Vi.DataController.Groups.GroupingItemCount; //����� ����� �����
  for i:=0 to iCol-1 do
  begin
    arCol[i+1]:=Vi.DataController.Groups.GroupingItemIndex[i];
  end;

  if Vi.Controller.SelectedRowCount>1 then //������������ ������ ���������
  begin
    iGCount:=0;
    iCol:=Vi.VisibleColumnCount;

    iRow:=Vi.Controller.SelectedRowCount+1;

    iRowV:=Vi.Controller.SelectedRowCount;

    ArrayData := VarArrayCreate([1,iRow, 1, iCol+iGCount], varVariant);
    for i:=iGCount+1 to iCol+iGCount do
    begin
      ArrayData[1,i] := Vi.VisibleColumns[i-1-iGCount].Caption;
    end;

    iRow:=2;

    for i:=0 to Vi.Controller.SelectedRecordCount-1 do
    begin
      Rec:=Vi.Controller.SelectedRecords[i];
      k:=1;
      for j:=0 to Vi.ColumnCount-1 do
      begin
        if Vi.Columns[j].Visible then
        begin
          try
            StrWk:=Rec.Values[j]
          except
            StrWk:='';
          end;
          if pos('�',StrWk)=length(StrWk) then delete(StrWk,length(StrWk),1);
          if pos('%',StrWk)=length(StrWk) then delete(StrWk,length(StrWk),1);

          rVal:=StrToFloatDef(StrWk,1000000);
          if rVal<>1000000 then  ArrayData[iRow+i,k]:=rVal
          else ArrayData[iRow+i,k]:=StrWk;
          inc(k);
        end;
      end;
    end;
  end else
  begin       //��� ������

    iGCount:=Vi.DataController.Groups.GroupingItemCount;
    iCol:=Vi.VisibleColumnCount;

    iRow:=Vi.DataController.RowCount+1;

    iRowV:=Vi.DataController.RowCount;
//  iJ:=Vi.DataController.ItemCount;
//  iJ:=Vi.VisibleColumnCount;

    ArrayData := VarArrayCreate([1,iRow, 1, iCol+iGCount], varVariant);
    for i:=iGCount+1 to iCol+iGCount do
    begin
      ArrayData[1,i] := Vi.VisibleColumns[i-1-iGCount].Caption;
    end;

    iRow:=2;

    for i:=0 to iRowV-1 do
    begin
      with Vi.DataController do
      begin
        RowInf:=GetRowInfo(i);
        if IsGroupingRow(RowInf,Vi.DataController)=False then
        begin
          k:=1+iGCount;
          for j:=0 to Vi.ColumnCount-1 do
          begin
            if Vi.Columns[j].Visible then
            begin
              StrWk:=GetRowDisplayText(RowInf,j);
              if pos('�',StrWk)=length(StrWk) then delete(StrWk,length(StrWk),1);
              if pos('%',StrWk)=length(StrWk) then delete(StrWk,length(StrWk),1);

              rVal:=StrToFloatDef(StrWk,1000000);
              if rVal<>1000000 then  ArrayData[iRow,k]:=rVal
              else ArrayData[iRow,k]:=StrWk;

              inc(k);
            end;
          end
        end else
        begin
          iLev:=RowInf.Level;
          iItem:=Groups.GroupingItemIndex[iLev];

          ArrayData[iRow,iLev+1]:=Vi.Columns[iItem].Caption+': '+GetRowDisplayText(RowInf,iItem);

//        StrWk:=Summary.GroupSummaryText[i];

//        prStrToArr(StrWk); //��� �������������� ��� �� �����


          iCountDG:=Summary.DefaultGroupSummaryItems.Count;
          for ll:=0 to iCountDG-1 do
          begin
            SumGr:=(Summary.DefaultGroupSummaryItems.Items[ll] as tcxGridDBTableSummaryItem);
            if SumGr.Position=spFooter then
            begin
              sField:=SumGr.FieldName;
              sSum:=Summary.GroupFooterSummaryTexts[i,iLev,ll];

              for cc:=iGCount+1 to iCol+iGCount do
              begin
                sColumn:=Vi.VisibleColumns[cc-1-iGCount].Name;
                delete(sColumn,1,Length(Vi.Name)); //������ �� �������� ������� �������� View
                if sColumn=sField then //����� �������
                begin
                  ff:=cc;
                  rVal:=StrToFloatDef(sSum,1000000);
                  if rVal<>1000000 then  ArrayData[iRow,ff]:=rVal
                  else ArrayData[iRow,ff]:=sSum;
//                  ArrayData[iRow,ff]:=sSum;
                end;
              end;
            end;
          end;


        end;
        inc(iRow);
      end;
    end;
  end;

  if destSoft='mse' then
    begin
      Cell1 := WorkBook.WorkSheets[1].Cells[1,1];
      Cell2 := WorkBook.WorkSheets[1].Cells[1+iRowV,iCol+iGCount];
      Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
      Range.Value := ArrayData;

      ExcelApp.Visible := true;
    end;


  if destSoft='ooc' then
    begin
      {for ii:=0 to iRow-2 do      //iRow-10
        for jj:=0 to iCol-1 do    //iCol
          begin
            cell := Sheet.getCellByPosition(jj,ii); //������
            if StrToFloatDef(ArrayData[ii+1,jj+1],1000000) = 1000000
              then Cell.SetString(ArrayData[ii+1,jj+1])
              else Cell.SetValue(ArrayData[ii+1,jj+1]);
          end;}

      ooArrayData := VarArrayCreate([1,iCol+iGCount, 1, iRow-1], varVariant);

      for jj:=1 to iRow-1 do
        for ii:=1 to iCol+iGCount do ooArrayData[ii,jj]:=ArrayData[jj,ii];

      ooRange:=Sheet.getCellRangeByPosition(0,0, iCol+iGCount-1, iRow-2);
      ooRange.setDataArray(ooArrayData);
      Document.getCurrentController.getFrame.getContainerWindow.setVisible(true);
      StarOffice := Unassigned;
    end;

end;


procedure prNExportExel7(Vi:tcxGridDBTableView);
Var ExcelApp, Workbook, Range, Cell1, Cell2, ArrayData  : Variant;
    iCol,iRow,i:Integer;
    RowInf:TcxRowInfo;
    iRowV,J,k,iGCount,iLev,iItem:Integer;
    StrWk:String;
    arCol:array[1..20] of integer;
    iCountDG,ll,cc,ff:INteger;
    sField,sColumn,sSum:String;
    SumGr:tcxGridDBTableSummaryItem;
    Rec:TcxCustomGridRecord;
    rVal:Real;


function IsGroupingRow(ARowInfo: TcxRowInfo; ADataController: TcxCustomDataController): Boolean;
begin
  with ADataController do
    Result := ARowInfo.Level <> Groups.GroupingItemCount;
end;

begin
//������� � ������
//  Vi.BeginUpdate;

  if not IsOLEObjectInstalled('Excel.Application') then exit;
  ExcelApp := CreateOleObject('Excel.Application');
  ExcelApp.Application.EnableEvents := false;
  //� ������� �����
  Workbook := ExcelApp.WorkBooks.Add;

  for i:=1 to 20 do arCol[i]:=-1; //������� ��� �������

  iCol:=Vi.DataController.Groups.GroupingItemCount; //����� ����� �����
  for i:=0 to iCol-1 do
  begin
    arCol[i+1]:=Vi.DataController.Groups.GroupingItemIndex[i];
  end;

  if Vi.Controller.SelectedRowCount>1 then //������������ ������ ���������
  begin
    iGCount:=0;
    iCol:=Vi.VisibleColumnCount;

    iRow:=Vi.Controller.SelectedRowCount+1;

    iRowV:=Vi.Controller.SelectedRowCount;

    ArrayData := VarArrayCreate([1,iRow, 1, iCol+iGCount], varVariant);
    for i:=iGCount+1 to iCol+iGCount do
    begin
      ArrayData[1,i] := Vi.VisibleColumns[i-1-iGCount].Caption;
    end;

    iRow:=2;

    for i:=0 to Vi.Controller.SelectedRecordCount-1 do
    begin
      Rec:=Vi.Controller.SelectedRecords[i];
      k:=1;
      for j:=0 to Vi.ColumnCount-1 do
      begin
        if Vi.Columns[j].Visible then
        begin
          try
            StrWk:=Rec.Values[j]
          except
            StrWk:='';
          end;
          if pos('�',StrWk)=length(StrWk) then delete(StrWk,length(StrWk),1);
          if pos('%',StrWk)=length(StrWk) then delete(StrWk,length(StrWk),1);

          rVal:=StrToFloatDef(StrWk,1000000);
          if rVal<>1000000 then  ArrayData[iRow+i,k]:=rVal
          else ArrayData[iRow+i,k]:=StrWk;
          inc(k);
        end;
      end;
    end;
  end else
  begin       //��� ������

    iGCount:=Vi.DataController.Groups.GroupingItemCount;
    iCol:=Vi.VisibleColumnCount;

    iRow:=Vi.DataController.RowCount+1;

    iRowV:=Vi.DataController.RowCount;
//  iJ:=Vi.DataController.ItemCount;
//  iJ:=Vi.VisibleColumnCount;

    ArrayData := VarArrayCreate([1,iRow, 1, iCol+iGCount], varVariant);
    for i:=iGCount+1 to iCol+iGCount do
    begin
      ArrayData[1,i] := Vi.VisibleColumns[i-1-iGCount].Caption;
    end;

    iRow:=2;

    for i:=0 to iRowV-1 do
    begin
      with Vi.DataController do
      begin
        RowInf:=GetRowInfo(i);
        if IsGroupingRow(RowInf,Vi.DataController)=False then
        begin
          k:=1+iGCount;
          for j:=0 to Vi.ColumnCount-1 do
          begin
            if Vi.Columns[j].Visible then
            begin
              StrWk:=GetRowDisplayText(RowInf,j);
              if pos('�',StrWk)=length(StrWk) then delete(StrWk,length(StrWk),1);
              if pos('%',StrWk)=length(StrWk) then delete(StrWk,length(StrWk),1);

              rVal:=StrToFloatDef(StrWk,1000000);
              if rVal<>1000000 then  ArrayData[iRow,k]:=rVal
              else ArrayData[iRow,k]:=StrWk;

              inc(k);
            end;
          end
        end else
        begin
          iLev:=RowInf.Level;
          iItem:=Groups.GroupingItemIndex[iLev];

          ArrayData[iRow,iLev+1]:=Vi.Columns[iItem].Caption+': '+GetRowDisplayText(RowInf,iItem);

//        StrWk:=Summary.GroupSummaryText[i];

//        prStrToArr(StrWk); //��� �������������� ��� �� �����

{        for mm:=1 to 20 do
        begin
          if arSum[mm]<1000000000 then
          begin
            ArrayData[iRow,iLev+2+mm]:=arSum[mm];
          end;
        end;

GroupFooterSummaryTexts[RowIndex, Level, Index: Integer]
}
          iCountDG:=Summary.DefaultGroupSummaryItems.Count;
          for ll:=0 to iCountDG-1 do
          begin
            SumGr:=(Summary.DefaultGroupSummaryItems.Items[ll] as tcxGridDBTableSummaryItem);
            if SumGr.Position=spFooter then
            begin
              sField:=SumGr.FieldName;
              sSum:=Summary.GroupFooterSummaryTexts[i,iLev,ll];

              for cc:=iGCount+1 to iCol+iGCount do
              begin
                sColumn:=Vi.VisibleColumns[cc-1-iGCount].Name;
                delete(sColumn,1,Length(Vi.Name)); //������ �� �������� ������� �������� View
                if sColumn=sField then //����� �������
                begin
                  ff:=cc;
                  ArrayData[iRow,ff]:=sSum;
                end;
              end;
            end;
          end;


        end;
        inc(iRow);
      end;
    end;
  end;

  Cell1 := WorkBook.WorkSheets[1].Cells[1,1];
  Cell2 := WorkBook.WorkSheets[1].Cells[1+iRowV,iCol+iGCount];
  Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
  Range.Value := ArrayData;

//  Vi.EndUpdate;

  ExcelApp.Visible := true;
end;


Function fTestKey(Key:Word):Boolean;
begin
  Result:=False;
  if (Key=$30)
  or (Key=$31)
  or (Key=$32)
  or (Key=$33)
  or (Key=$34)
  or (Key=$35)
  or (Key=$36)
  or (Key=$37)
  or (Key=$38)
  or (Key=$39)
  or (Key=$60)
  or (Key=$61)
  or (Key=$62)
  or (Key=$63)
  or (Key=$64)
  or (Key=$65)
  or (Key=$66)
  or (Key=$67)
  or (Key=$68)
  or (Key=$69)
  then Result:=True;
end;


function s1(S:String):String;
Var Str1:String;
begin
  Str1:=S;
  while pos(',',Str1)>0 do Str1[pos(',',Str1)]:='.';
  while pos('"',Str1)>0 do Str1[pos('"',Str1)]:=' ';
  while pos('''',Str1)>0 do Str1[pos('''',Str1)]:=' ';
  Result:=Str1;
end;


Procedure WriteCheckNum;
Var f:TIniFile;
begin
  f:=TIniFile.create(CurDir+CurIni);
  f.WriteInteger('Config','CashChNum',CommonSet.CashChNum);
  f.WriteInteger('Config','CashZ',CommonSet.CashZ);
  f.Free;
end;

Function ToStandart(S:String):String;
var StrWk,Str1:String;
    iBar: array[1..13] of integer;
    n,c,n1:Integer;
begin
  StrWk:=S;
  while Length(StrWk)<12 do StrWk:=StrWk+'0';
  for n:=1 to 12 do
  begin
    str1:=Copy(StrWk,n,1);
    iBar[n]:=StrToIntDef(Str1,0);
  end;
  //220123401000C
  c:=0;
  n:=(iBar[2]+iBar[4]+iBar[6]+iBar[8]+iBar[10]+iBar[12])*3+(iBar[1]+iBar[3]+iBar[5]+iBar[7]+iBar[9]+iBar[11]);
  for n1:=0 to 9 do
  begin
    if ((n+n1) mod 10)=0 then
    begin
      c:=n1;
      break;
    end;
  end;
  iBar[13]:=c;
  Str1:='';
  for n:=1 to 13 do str1:=Str1+IntToStr(iBar[n]);

  strwk:=Str1;
  Result:=StrWk;
end;


procedure prWriteLog(Strwk_: string);
var F: TextFile;
    Strwk1:String;
    FileN:String;
begin
  try
    if not DirectoryExists(CommonSet.PathArh) then
    begin
      exit;
    end;

    strwk1:=FormatDateTime('yyyy_mm_dd',Date);
    Strwk1:=StrWk1+'.log';
    Application.ProcessMessages;

    FileN:=CommonSet.PathArh+strwk1;

    AssignFile(F, FileN);
    if Not FileExists(FileN) then Rewrite(F)
    else                           Append(F);
    Write(F, FormatDateTime('HH:NN:SS ;', Now));
    WriteLn(F, Strwk_);
    Flush(F);
  finally
    CloseFile(F);
  end;
end;

Procedure WriteColor;
Var f:TIniFile;
begin
  f:=TIniFile.create(CurDir+CurIni);
  f.WriteInteger('Colors_','Main',UserColor.Main);
  f.Free;
end;

Function SOnlyDigit(S:String):String;
Var i:Integer;
begin
  result:='';
  for i:=1 to length(S) do
  begin
    if s[i] in ['0','1','2','3','4','5','6','7','8','9'] then
    begin
      Result:=Result+s[i];
    end;
  end;
  if result='' then result:='0';
end;


Procedure Readcfg;
Var f:TIniFile;
begin
  f:=TIniFile.create(CurDir+'ToShop.ini');
  CommonSet.FtpExport:=f.ReadString('Config','FtpExport','');
  CommonSet.HostIp:=f.ReadString('Config','HostIp','');
  CommonSet.HostUser:=f.ReadString('Config','HostUser','');
  CommonSet.HostPassw:=f.ReadString('Config','HostPassw','');
  CommonSet.DBName:=f.ReadString('Config','DBName','DBName');

  f.WriteString('Config','FtpExport',CommonSet.FtpExport);
  f.WriteString('Config','HostIp',CommonSet.HostIp);
  f.WriteString('Config','HostUser',CommonSet.HostUser);
  f.WriteString('Config','HostPassw',CommonSet.HostPassw);
  f.WriteString('Config','DBName',CommonSet.DBName);

  f.Free;
end;


procedure WriteHistorySMS(Strwk_: string);
var F: TextFile;
    Strwk1:String;
    FileN:String;
begin
  try
    strwk1:='sms_'+FormatDateTime('yyyy_mm_dd',Date);
    Strwk1:=StrWk1+'.txt';
    Application.ProcessMessages;
//    FileN:=CurDir+strwk1;
    FileN:=CommonSet.PathHistory+strwk1;
    AssignFile(F, FileN);
    if Not FileExists(FileN) then Rewrite(F)
    else                           Append(F);
    Write(F, FormatDateTime('dd HH:NN:SS  ', Now));
    WriteLn(F, Strwk_);
    Flush(F);
  finally
    CloseFile(F);
  end;
end;



function R10000( X: Double ): Double;
begin
  Result:=RoundEx(X*10000)/10000;
end;


procedure prRowFocus(Vi:tcxGridDBTableView);
begin
  Vi.Controller.FocusRecord(Vi.DataController.FocusedRowIndex,True);
end;


function sdtoint(s:String):INteger;
Var StrWk:String;
    DDate:TDateTime;
begin //�������������� ������ YYYYMMDD � ����� ���
  StrWk:=Copy(S,7,2)+'.'+Copy(S,5,2)+'.'+Copy(S,1,4); //�������� ������ ����
  DDate:=StrToDateDef(StrWk,date);
  Result:=Trunc(DDate);
end;


function fDWeek(iDay:INteger):Integer;
Var iWeek:integer;
begin
  iWeek:=DayOfWeek(iDay);
  if iWeek>1 then iWeek:=iWeek-1 else iWeek:=7;
  result:=iWeek;
end;


function GetCurrentUserName: string;
const
 cnMaxUserNameLen = 254;
var
  sUserName: string;
  dwUserNameLen: DWORD;
begin
  dwUserNameLen := cnMaxUserNameLen - 1;
  SetLength(sUserName, cnMaxUserNameLen);
  GetUserName(PChar(sUserName), dwUserNameLen);
  SetLength(sUserName, dwUserNameLen-1);

  Result := sUserName;
end;


Function DToYY(tD:TDateTime):INteger;
begin
  Result:=StrToINtDef(FormatDateTime('yyyy',tD),0);
end;


procedure prWriteStart(Strwk_: string);
var F: TextFile;
    Strwk1:String;
    FileN:String;
begin
  try
    Strwk1:='Start_'+Person.WinName+'.log';
    Application.ProcessMessages;

    FileN:=CurDir+strwk1;

    AssignFile(F, FileN);
    if Not FileExists(FileN) then Rewrite(F)
    else                           Append(F);
//    Write(F, FormatDateTime('HH:NN:SS ;', Now));
    WriteLn(F, Strwk_);
    Flush(F);
  finally
    CloseFile(F);
  end;
end;


function fDateToKvartal(iCurD:INteger):Integer;
Var StrM:String;
begin
  StrM:=FormatDateTime('m',iCurD);
  Result:=StrToINtDef(StrM,0);
end;

procedure prCalcDefGroupAv(i1,i2,i3,iParent:Integer; V:TcxGridDBTableView);
var
  AChildDataGroupsCount: Integer;
  AChildDataGroupIndex: TcxDataGroupIndex;
  AChildPosition: Integer;
  AVarSum, AVarSum1, AVarNac: Variant;
//  RowInf:TcxRowInfo;

begin
  with V.DataController.Groups do
  begin
    AChildDataGroupsCount := ChildCount[iParent];

    for AChildPosition := 0 to AChildDataGroupsCount - 1 do
    begin
      //data group index of a child
      AChildDataGroupIndex := ChildDataGroupIndex[iParent, AChildPosition];

      if AChildDataGroupIndex>=0 then
      begin
        prCalcDefGroup(i1,i2,i3,AChildDataGroupIndex,V);

        with V.DataController.Summary do
        begin
          AVarSum := GroupSummaryValues[AChildDataGroupIndex, i1];
          AVarSum1 := GroupSummaryValues[AChildDataGroupIndex, i2];
          if not (VarIsNull(AVarSum) or VarIsNull(AVarSum1)) then
          begin
//            AVarNac := (AVarSum1 - AVarSum) / AVarSum*100;
            try
              AVarNac := AVarSum/AVarSum1;
            except
              AVarNac := 0;
            end;
            GroupSummaryValues[AChildDataGroupIndex, i3] := Format('%7.2f', [Double(AVarNac)]);
//          GroupSummaryValues[AChildDataGroupIndex, 2] := Double(AVarNac);
//          Memo1.Lines.Add('In '+FloatToStr(Double(AVarSum))+' Out '+FloatToStr(Double(AVarSum1))+' Nac '+FloatToStr(Double(AVarNac))+' AChildDataGroupIndex '+INtToStr(AChildDataGroupIndex));
            delay(10);
          end;
        end;
      end;
    end;
  end;
end;


procedure prCalcSumAv(i1,i2,i3:Integer; V:TcxGridDBTableView);
Var rSumR,rSumQ,rAv:Real;
    StrWk:String;
    vSum:Variant;
begin
  rSumR:=0; rSumQ:=0;
  vSum:=V.DataController.Summary.FooterSummaryValues[i1];
  if vSum<>Null then rSumR:=vSum;

  vSum:=V.DataController.Summary.FooterSummaryValues[i2];
  if vSum<>Null then rSumQ:=vSum;

  rAv:=0;
  if rSumQ<>0 then
    rAv:=rSumR/rSumQ;
  str(rAv:7:2,StrWk);
//  StrWk:=FloatToStr(rv(rAv));
  V.DataController.Summary.FooterSummaryValues[i3]:=StrWk;
end;

function fmdt: String;
begin
  Result:=FormatDateTime('hh:nn:ss ',now);
end;

procedure prWH(Strwk_: string;Memo1:TcxMemo);
begin
  WriteHistory(Strwk_);
  if Memo1<>nil then Memo1.Lines.Add(Strwk_);
  Delay(10);
end;


function prRoundPack(rQ,rQPack:Real; iPack:INteger):Real;
Var k:INteger;
begin
  Result:=rQ;
  rQPack:=RoundEx(rQPack*1000)/1000;
  if iPack=1 then //��������� �� �����
  begin
    if rQPack>0 then
    begin
      if rQ>=rQPack then
      begin
//         Result:=rQPack*(RoundEx(rQ/rQPack)) //��� �� 0.5 ������� �����
// ����� �� 0,4 ����� - ������ ����
        if frac(rQ/rQPack)>=0.4 then k:=1 else k:=0;

        Result:=rQPack*Trunc(rQ/rQPack)+k*rQPack;
      end
      else if rQ>0.010 then Result:=rQPack
           else Result:=0;
    end;
  end;
end;

function FindDayWeek(iDate:INteger):INteger;
Var iDate0:INteger;
    IDW,IDW2,IDW1:INteger;
    DW1:Real;
begin
  iDate0:=40455;
  IDW:=iDate-iDate0;
  IDW2:=IDW div 7;
  DW1:=IDW/7-IDW2;
  IDW1:=0;

  if (DW1<0.14) then
  begin
    IDW1:=1;
  end;
  if (DW1>0.14) then
  begin
    if (DW1<0.28) then IDW1:=2;
  end;
  if (DW1>0.28) then
  begin
    if (DW1<0.42) then IDW1:=3;
  end;
  if (DW1>0.42) then
  begin
    if (DW1<0.57) then IDW1:=4;
  end;
  if (DW1>0.57) then
  begin
    if (DW1<0.71) then IDW1:=5;
  end;
  if (DW1>0.71) then
  begin
    if (DW1<0.85) then IDW1:=6;
  end;
  if (DW1>0.85) then
  begin
    IDW1:=7;
  end;

  Result:=IDW1;

{

DECLARE VARIABLE DATECUR DATE;
DECLARE VARIABLE DATE0 DATE;
DECLARE VARIABLE IDW INTEGER;
DECLARE VARIABLE DW DOUBLE PRECISION;
DECLARE VARIABLE IDW2 INTEGER;
DECLARE VARIABLE DW1 DOUBLE PRECISION;
DECLARE VARIABLE DW2 DOUBLE PRECISION;
DECLARE VARIABLE IDW1 INTEGER;

  DATE0 = cast('04.10.2010' as date); /* ����������� */

  IDW=:DATECUR-:DATE0;
  DW=:IDW;

  IDW2=:IDW/7;

  DW2=:DW/7;
  DW1=:DW / 7- IDW2;

  IDW=0;

  if (:DW1<0.14) then
  begin
    IDW1=1;
  end
  if (:DW1>0.14) then
  begin
    if (:DW1<0.28) then IDW1=2;
  end
  if (:DW1>0.28) then
  begin
    if (:DW1<0.42) then IDW1=3;
  end
  if (:DW1>0.42) then
  begin
    if (:DW1<0.57) then IDW1=4;
  end
  if (:DW1>0.57) then
  begin
    if (:DW1<0.71) then IDW1=5;
  end
  if (:DW1>0.71) then
  begin
    if (:DW1<0.85) then IDW1=6;
  end
  if (:DW1>0.85) then
  begin
    IDW1=7;
  end

  DWEEK=:IDW1;
  }

end;


function ds(d:TDateTime):String;
begin
  result:=FormatDateTime('yyyymmdd',d);
end;

function ds1(d:TDateTime):String;
begin
  result:=FormatDateTime('dd.mm.yyyy',d);
end;

function dst(d:TDateTime):String;
begin
  result:=FormatDateTime('yyyymmdd hh:nn:ss',d);
end;


Function TrimStr(StrIn:String):String;
begin
  delete(StrIn,Pos('//',StrIn),(Length(StrIn)-Pos('//',StrIn)+1)); //������ ���������
  while Pos(' ',StrIn)>0 do delete(StrIn,Pos(' ',StrIn),1);        //������ ������v
  Result:=StrIn;
end;


procedure prNExportExel6(Vi:tcxGridDBTableView);
Var ExcelApp, Workbook, Range, Cell1, Cell2, ArrayData  : Variant;
    iCol,iRow,i:Integer;
    RowInf:TcxRowInfo;
    iRowV,J,k,iGCount,iLev,iItem:Integer;
    StrWk:String;
    arCol:array[1..20] of integer;
    iCountDG,ll,cc,ff:INteger;
    sField,sColumn,sSum:String;
    SumGr:tcxGridDBTableSummaryItem;
    Rec:TcxCustomGridRecord;
    rVal:Real;

    StarOffice,StarDesktop,Document,Sheets,Sheet,ooRange,ooArrayData:Variant;
    ii,jj:integer;
    destSoft:String;


function IsGroupingRow(ARowInfo: TcxRowInfo; ADataController: TcxCustomDataController): Boolean;
begin
  with ADataController do
    Result := ARowInfo.Level <> Groups.GroupingItemCount;
end;

begin
//������� � ������
//  Vi.BeginUpdate;

  destSoft:='non'; //������� �� �����

  if IsOLEObjectInstalled('Excel.Application')
    then
      begin
        ExcelApp := CreateOleObject('Excel.Application');
        ExcelApp.Application.EnableEvents := false;
        //� ������� �����
        Workbook := ExcelApp.WorkBooks.Add;
        destSoft := 'mse';  //excell
      end
    else destSoft:='non';

  //destSoft:='non';  // ����� ��� ����� ���� �����, "����" ������ ��� -- ��� ������ ������ ���� ���������

  if ((IsOLEObjectInstalled('com.sun.star.ServiceManager')) and (destSoft='non'))
    then
      begin
        StarOffice := CreateOleObject('com.sun.star.ServiceManager');
        StarDesktop := StarOffice.createInstance('com.sun.star.frame.Desktop');

        Document := StarDesktop.LoadComponentFromURL(
                    'private:factory/scalc', '_blank', 0,
                    VarArrayCreate([-1, -1], varVariant));
        Document.getCurrentController.getFrame.getContainerWindow.setVisible(False);
        Sheets := Document.getSheets;
        //Sheet := Sheets.getByName('����1');
        Sheet := Sheets.getByIndex(0);
        destSoft:='ooc';
      end;

  if destSoft='non' then exit;


  for i:=1 to 20 do arCol[i]:=-1; //������� ��� �������

  iCol:=Vi.DataController.Groups.GroupingItemCount; //����� ����� �����
  for i:=0 to iCol-1 do
  begin
    arCol[i+1]:=Vi.DataController.Groups.GroupingItemIndex[i];
  end;

  if Vi.Controller.SelectedRowCount>1 then //������������ ������ ���������
  begin
    iGCount:=0;
    iCol:=Vi.VisibleColumnCount;

    iRow:=Vi.Controller.SelectedRowCount+1;

    iRowV:=Vi.Controller.SelectedRowCount;

    ArrayData := VarArrayCreate([1,iRow, 1, iCol+iGCount], varVariant);
    for i:=iGCount+1 to iCol+iGCount do
    begin
      ArrayData[1,i] := Vi.VisibleColumns[i-1-iGCount].Caption;
    end;

    iRow:=2;

    for i:=0 to Vi.Controller.SelectedRecordCount-1 do
    begin
      Rec:=Vi.Controller.SelectedRecords[i];
      k:=1;
      for j:=0 to Vi.ColumnCount-1 do
      begin
        if Vi.Columns[j].Visible then
        begin
          try
            StrWk:=Rec.Values[j]
          except
            StrWk:='';
          end;  
          if pos('�',StrWk)=length(StrWk) then delete(StrWk,length(StrWk),1);
          if pos('%',StrWk)=length(StrWk) then delete(StrWk,length(StrWk),1);

          rVal:=StrToFloatDef(StrWk,1000000);
          if rVal<>1000000 then  ArrayData[iRow+i,k]:=rVal
          else ArrayData[iRow+i,k]:=StrWk;
          inc(k);
        end;
      end;
    end;
  end else
  begin       //��� ������

    iGCount:=Vi.DataController.Groups.GroupingItemCount;
    iCol:=Vi.VisibleColumnCount;

    iRow:=Vi.DataController.RowCount+1;

    iRowV:=Vi.DataController.RowCount;
//  iJ:=Vi.DataController.ItemCount;
//  iJ:=Vi.VisibleColumnCount;

    ArrayData := VarArrayCreate([1,iRow, 1, iCol+iGCount], varVariant);
    for i:=iGCount+1 to iCol+iGCount do
    begin
      ArrayData[1,i] := Vi.VisibleColumns[i-1-iGCount].Caption;
    end;

    iRow:=2;

    for i:=0 to iRowV-1 do
    begin
      with Vi.DataController do
      begin
        RowInf:=GetRowInfo(i);
        if IsGroupingRow(RowInf,Vi.DataController)=False then
        begin
          k:=1+iGCount;
          for j:=0 to Vi.ColumnCount-1 do
          begin
            if Vi.Columns[j].Visible then
            begin
              StrWk:=GetRowDisplayText(RowInf,j);
              if pos('�',StrWk)=length(StrWk) then delete(StrWk,length(StrWk),1);
              if pos('%',StrWk)=length(StrWk) then delete(StrWk,length(StrWk),1);

              rVal:=StrToFloatDef(StrWk,1000000);
              if rVal<>1000000 then  ArrayData[iRow,k]:=rVal
              else ArrayData[iRow,k]:=StrWk;

              inc(k);
            end;
          end
        end else
        begin
          iLev:=RowInf.Level;
          iItem:=Groups.GroupingItemIndex[iLev];

          ArrayData[iRow,iLev+1]:=Vi.Columns[iItem].Caption+': '+GetRowDisplayText(RowInf,iItem);

//        StrWk:=Summary.GroupSummaryText[i];

//        prStrToArr(StrWk); //��� �������������� ��� �� �����

{        for mm:=1 to 20 do
        begin
          if arSum[mm]<1000000000 then
          begin
            ArrayData[iRow,iLev+2+mm]:=arSum[mm];
          end;
        end;

GroupFooterSummaryTexts[RowIndex, Level, Index: Integer]
}
          iCountDG:=Summary.DefaultGroupSummaryItems.Count;
          for ll:=0 to iCountDG-1 do
          begin
            SumGr:=(Summary.DefaultGroupSummaryItems.Items[ll] as tcxGridDBTableSummaryItem);
            if SumGr.Position=spFooter then
            begin
              sField:=SumGr.FieldName;
              sSum:=Summary.GroupFooterSummaryTexts[i,iLev,ll];

              for cc:=iGCount+1 to iCol+iGCount do
              begin
                sColumn:=Vi.VisibleColumns[cc-1-iGCount].Name;
                delete(sColumn,1,Length(Vi.Name)); //������ �� �������� ������� �������� View
                if sColumn=sField then //����� �������
                begin
                  ff:=cc;

                  if StrToFloatDef(StrWk,1000000)<>1000000 then  ArrayData[iRow,ff]:=StrToFloatDef(StrWk,1000000)
                  else ArrayData[iRow,ff]:=StrWk;

                end;
              end;
            end;
          end;


        end;
        inc(iRow);
      end;
    end;
  end;

  if destSoft='mse' then
    begin
      Cell1 := WorkBook.WorkSheets[1].Cells[1,1];
      Cell2 := WorkBook.WorkSheets[1].Cells[1+iRowV,iCol+iGCount];
      Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
      Range.Value := ArrayData;

      ExcelApp.Visible := true;
    end;


  if destSoft='ooc' then
    begin
      {for ii:=0 to iRow-2 do      //iRow-10
        for jj:=0 to iCol-1 do    //iCol
          begin
            cell := Sheet.getCellByPosition(jj,ii); //������
            if StrToFloatDef(ArrayData[ii+1,jj+1],1000000) = 1000000
              then Cell.SetString(ArrayData[ii+1,jj+1])
              else Cell.SetValue(ArrayData[ii+1,jj+1]);
          end;}

      ooArrayData := VarArrayCreate([1,iCol+iGCount, 1, iRow-1], varVariant);

      for jj:=1 to iRow-1 do
        for ii:=1 to iCol+iGCount do ooArrayData[ii,jj]:=ArrayData[jj,ii];

      ooRange:=Sheet.getCellRangeByPosition(0,0, iCol+iGCount-1, iRow-2);
      ooRange.setDataArray(ooArrayData);
      Document.getCurrentController.getFrame.getContainerWindow.setVisible(true);
      StarOffice := Unassigned;
    end;

end;


Function its(i:Integer):String;
begin
  result:=IntToStr(i);
end;


function DelP(S:String):String;
Var Str1:String;
begin
  Str1:=S;
  while pos('.',Str1)>0 do delete(Str1,pos('.',Str1),1);
  while pos(' ',Str1)>0 do delete(Str1,pos(' ',Str1),1);
  Result:=Str1;
end;


Function fs(rSum:Real):String;
Var s:String;
begin
  s:=FloatToStr(rSum);
  while pos(',',s)>0 do s[pos(',',s)]:='.';
  Result:=s;
end;

Function fts(rSum:Real):String;
Var s:String;
begin
  s:=FloatToStr(rSum);
  while pos(',',s)>0 do s[pos(',',s)]:='.';
  Result:=s;
end;


function IsGroupingRow(ARowInfo: TcxRowInfo; ADataController: TcxCustomDataController): Boolean;
begin
  with ADataController do
    Result := ARowInfo.Level <> Groups.GroupingItemCount;
end;


procedure prCalcDefGroup(i1,i2,i3,iParent:Integer; V:TcxGridDBTableView);
var
  AChildDataGroupsCount: Integer;
  AChildDataGroupIndex: TcxDataGroupIndex;
  AChildPosition: Integer;
  AVarSum, AVarSum1, AVarNac: Variant;
  //RowInf:TcxRowInfo;

begin
  with V.DataController.Groups do
  begin
    AChildDataGroupsCount := ChildCount[iParent];

    for AChildPosition := 0 to AChildDataGroupsCount - 1 do
    begin
      //data group index of a child
      AChildDataGroupIndex := ChildDataGroupIndex[iParent, AChildPosition];

      if AChildDataGroupIndex>=0 then     //AChildDataGroupIndex>=0
      begin
        prCalcDefGroup(i1,i2,i3,AChildDataGroupIndex,V);

        with V.DataController.Summary do
        begin
          AVarSum := GroupSummaryValues[AChildDataGroupIndex, i1];
          AVarSum1 := GroupSummaryValues[AChildDataGroupIndex, i2];

          if ((not (VarIsNull(AVarSum) or VarIsNull(AVarSum1))) and (AVarSum<>0)) then
          begin
            AVarNac := (AVarSum1 - AVarSum) / AVarSum*100;
            GroupSummaryValues[AChildDataGroupIndex, i3] := Format('%4.1f', [Double(AVarNac)])+'%';

//          GroupSummaryValues[AChildDataGroupIndex, 2] := Double(AVarNac);
//          Memo1.Lines.Add('In '+FloatToStr(Double(AVarSum))+' Out '+FloatToStr(Double(AVarSum1))+' Nac '+FloatToStr(Double(AVarNac))+' AChildDataGroupIndex '+INtToStr(AChildDataGroupIndex));
            delay(10);
          end;

        end;
      end;
    end;
  end;
end;
         {
procedure prCalcDefGroup2(i1,i2,i3,iParent:Integer; V:TcxGridDBBandedTableView);
var
  AChildDataGroupsCount: Integer;
  AChildDataGroupIndex: TcxDataGroupIndex;
  AChildPosition: Integer;
  AVarSum, AVarSum1, AVarNac: Variant;
  RowInf:TcxRowInfo;

begin
  with V.DataController.Groups do
  begin
    AChildDataGroupsCount := ChildCount[iParent];

    for AChildPosition := 0 to AChildDataGroupsCount - 1 do
    begin
      //data group index of a child
      AChildDataGroupIndex := ChildDataGroupIndex[iParent, AChildPosition];

      if AChildDataGroupIndex>=0 then
      begin
        prCalcDefGroup2(i1,i2,i3,AChildDataGroupIndex,V);

        with V.DataController.Summary do
        begin
          AVarSum := GroupSummaryValues[AChildDataGroupIndex, i1];
          AVarSum1 := GroupSummaryValues[AChildDataGroupIndex, i2];
          if not (VarIsNull(AVarSum) or VarIsNull(AVarSum1)) then
          begin
            AVarNac := (AVarSum1 - AVarSum) / AVarSum*100;
            GroupSummaryValues[AChildDataGroupIndex, i3] := Format('%4.1f', [Double(AVarNac)])+'%';
//          GroupSummaryValues[AChildDataGroupIndex, 2] := Double(AVarNac);
//          Memo1.Lines.Add('In '+FloatToStr(Double(AVarSum))+' Out '+FloatToStr(Double(AVarSum1))+' Nac '+FloatToStr(Double(AVarNac))+' AChildDataGroupIndex '+INtToStr(AChildDataGroupIndex));
            delay(10);
          end;
        end;
      end;
    end;
  end;
end;

                    }


procedure prCalcOborach(i1,i2,i3,i4,iParent:Integer; V:TcxGridDBTableView);
var
  AChildDataGroupsCount: Integer;
  AChildDataGroupIndex: TcxDataGroupIndex;
  AChildPosition: Integer;
  GoodsStock, SumOut, DaysCount,Percent: Variant;
  //RowInf:TcxRowInfo;

begin
  with V.DataController.Groups do
  begin
    AChildDataGroupsCount := ChildCount[iParent];

    for AChildPosition := 0 to AChildDataGroupsCount - 1 do
    begin
      //data group index of a child
      AChildDataGroupIndex := ChildDataGroupIndex[iParent, AChildPosition];

      if AChildDataGroupIndex>=0 then
      begin
        prCalcOborach(i1,i2,i3,i4,AChildDataGroupIndex,V);

        with V.DataController.Summary do
        begin
          GoodsStock := GroupSummaryValues[AChildDataGroupIndex, i1];
          SumOut     := GroupSummaryValues[AChildDataGroupIndex, i2];
          DaysCount  := GroupSummaryValues[AChildDataGroupIndex, i3];
          if ((not (VarIsNull(SumOut) or VarIsNull(SumOut))) and (SumOut<>0)) then
          begin
            Percent := (GoodsStock * DaysCount) / SumOut;
            GroupSummaryValues[AChildDataGroupIndex, i4] := Double(Percent);
            delay(10);
          end;
        end;
      end;
    end;
  end;
end;

procedure prNExportExel4(Vi:tcxGridDBTableView);
Var ExcelApp, Workbook, Range, Cell1, Cell2, ArrayData  : Variant;
    iCol,iRow,i:Integer;
    RowInf:TcxRowInfo;
    iRowV,J,k,iGCount,iLev,iItem:Integer;
    StrWk:String;
    arCol:array[1..20] of integer;
    iCountDG,ll,cc,ff:INteger;
    sField,sColumn,sSum:String;
    SumGr:tcxGridDBTableSummaryItem;

    StarOffice,StarDesktop,Document,Sheets,Sheet,ooRange,ooArrayData:Variant;
    ii,jj:integer;
    destSoft:String;

function IsGroupingRow(ARowInfo: TcxRowInfo; ADataController: TcxCustomDataController): Boolean;
begin
  with ADataController do
    Result := ARowInfo.Level <> Groups.GroupingItemCount;
end;

begin
//������� � ������
//  Vi.BeginUpdate;

  destSoft:='non'; //������� �� �����

  if IsOLEObjectInstalled('Excel.Application')
    then
      begin
        ExcelApp := CreateOleObject('Excel.Application');
        ExcelApp.Application.EnableEvents := false;
        //� ������� �����
        Workbook := ExcelApp.WorkBooks.Add;
        destSoft := 'mse';  //excell
      end
    else destSoft:='non';

  //destSoft:='non';  // ����� ��� ����� ���� �����, "����" ������ ��� -- ��� ��������� ����� �.�. ���������

  if ((IsOLEObjectInstalled('com.sun.star.ServiceManager')) and (destSoft='non'))
    then
      begin
        StarOffice := CreateOleObject('com.sun.star.ServiceManager');
        StarDesktop := StarOffice.createInstance('com.sun.star.frame.Desktop');

        Document := StarDesktop.LoadComponentFromURL(
                    'private:factory/scalc', '_blank', 0,
                    VarArrayCreate([-1, -1], varVariant));
        Document.getCurrentController.getFrame.getContainerWindow.setVisible(False);
        Sheets := Document.getSheets;
        //Sheet := Sheets.getByName('����1');
        Sheet := Sheets.getByIndex(0);
        destSoft:='ooc';
      end;

  if destSoft='non' then exit;

  for i:=1 to 20 do arCol[i]:=-1; //������� ��� �������

  iCol:=Vi.DataController.Groups.GroupingItemCount; //����� ����� �����
  for i:=0 to iCol-1 do
  begin
    arCol[i+1]:=Vi.DataController.Groups.GroupingItemIndex[i];
  end;

  iGCount:=Vi.DataController.Groups.GroupingItemCount;
  iCol:=Vi.VisibleColumnCount;

  iRow:=Vi.DataController.RowCount+1;

  iRowV:=Vi.DataController.RowCount;

  ArrayData := VarArrayCreate([1,iRow, 1, iCol+iGCount], varVariant);

  for i:=iGCount+1 to iCol+iGCount do
  begin
    ArrayData[1,i] := Vi.VisibleColumns[i-1-iGCount].Caption;
  end;


  iRow:=2;

  for i:=0 to iRowV-1 do
  begin
    with Vi.DataController do
    begin
      RowInf:=GetRowInfo(i);
      if IsGroupingRow(RowInf,Vi.DataController)=False then
      begin
        k:=1+iGCount;
        for j:=0 to Vi.ColumnCount-1 do
        begin
          if Vi.Columns[j].Visible then
          begin
            StrWk:=GetRowDisplayText(RowInf,j);
            //StrWk:=AnsiReplaceStr(StrWk,',','.');

            if pos('�',StrWk)=length(StrWk) then delete(StrWk,length(StrWk),1);

            if StrToFloatDef(StrWk,1000000)<>1000000
              then ArrayData[iRow,k]:=StrToFloatDef(StrWk,1000000)
              else ArrayData[iRow,k]:=StrWk;

            if ArrayData[iRow,k]=null then ArrayData[iRow,k]:=''; //��� �� �� ���� #�/�

            inc(k);
          end;
        end
      end else
      begin
        iLev:=RowInf.Level;
        iItem:=Groups.GroupingItemIndex[iLev];

        ArrayData[iRow,iLev+1]:=Vi.Columns[iItem].Caption+': '+GetRowDisplayText(RowInf,iItem);

        iCountDG:=Summary.DefaultGroupSummaryItems.Count;
        for ll:=0 to iCountDG-1 do
        begin
          SumGr:=(Summary.DefaultGroupSummaryItems.Items[ll] as tcxGridDBTableSummaryItem);
          if SumGr.Position=spFooter then
          begin
            sField:=SumGr.FieldName;
            sSum:=Summary.GroupFooterSummaryTexts[i,iLev,ll];

            for cc:=iGCount+1 to iCol+iGCount do
            begin
              sColumn:=Vi.VisibleColumns[cc-1-iGCount].Name;
              delete(sColumn,1,Length(Vi.Name)); //������ �� �������� ������� �������� View
              if sColumn=sField then //����� �������
              begin
                ff:=cc;
                try ArrayData[iRow,ff]:=StrToFloat(sSum);
                except ArrayData[iRow,ff]:=sSum; end;
                //ArrayData[iRow,ff]:=sSum;
              end;
            end;
          end;
        end;


      end;
      inc(iRow);
    end;
  end;

  if destSoft='mse' then
    begin
      Cell1 := WorkBook.WorkSheets[1].Cells[1,1];
      Cell2 := WorkBook.WorkSheets[1].Cells[1+iRowV,iCol+iGCount];
      Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
      Range.Value := ArrayData;

      ExcelApp.Visible := true;
    end;


  if destSoft='ooc' then
    begin
      {for ii:=0 to iRow-2 do      //iRow-10
        for jj:=0 to iCol-1 do    //iCol
          begin
            cell := Sheet.getCellByPosition(jj,ii); //������
            if StrToFloatDef(ArrayData[ii+1,jj+1],1000000) = 1000000
              then Cell.SetString(ArrayData[ii+1,jj+1])
              else Cell.SetValue(ArrayData[ii+1,jj+1]);
          end;}

      ooArrayData := VarArrayCreate([1,iCol+iGCount, 1, iRow-1], varVariant);

      for jj:=1 to iRow-1 do
        for ii:=1 to iCol+iGCount do ooArrayData[ii,jj]:=ArrayData[jj,ii];

      ooRange:=Sheet.getCellRangeByPosition(0,0, iCol+iGCount-1, iRow-2);
      ooRange.setDataArray(ooArrayData);
      Document.getCurrentController.getFrame.getContainerWindow.setVisible(true);
      StarOffice := Unassigned;
    end;

end;



procedure prCalcSumNac(i1,i2,i3:Integer; V:TcxGridDBTableView);
Var rSumIn,rSumOut,rNac:Real;
    StrWk:String;
    vSum:Variant;
begin
  rSumIn:=0; rSumOut:=0;
  vSum:=V.DataController.Summary.FooterSummaryValues[i1];
  if vSum<>Null then rSumIn:=vSum;

  vSum:=V.DataController.Summary.FooterSummaryValues[i2];
  if vSum<>Null then rSumOut:=vSum;

  rNac:=0;
  if rSumIn<>0 then
    rNac:=(rSumOut-rSumIn)/rSumIn*100;
  str(rNac:2:1,StrWk);
  StrWk:=StrWk+'%';
  V.DataController.Summary.FooterSummaryValues[i3]:=StrWk;
end;

procedure CalcAvg1(i1,i2,i3:Integer; V:TcxGridDBBandedTableView);
Var rSumIn,rSumOut,rNac:Real;
    StrWk:String;
    vSum:Variant;
begin
  rSumIn:=0; rSumOut:=0;
  vSum:=V.DataController.Summary.FooterSummaryValues[i1];
  if vSum<>Null then rSumIn:=vSum;

  vSum:=V.DataController.Summary.FooterSummaryValues[i2];
  if vSum<>Null then rSumOut:=vSum;

  rNac:=0;
  if ((rSumOut<>0) and (rSumIn<>0)) then
    rNac:=rSumIn/rSumOut;
  str(rNac:2:1,StrWk);
  StrWk:=StrWk;
  V.DataController.Summary.FooterSummaryValues[i3]:=StrWk;
end;



procedure CloseTe(taT:tdxMemData);
begin
  taT.Close;
  if taT.Active then
  begin
    taT.First;
    while not taT.Eof do taT.Delete;
    taT.Active:=False;
  end;
//  taT.Free;
//  taT.Create(Application);
  taT.Open;
  if taT.Active then
  begin
    taT.First;
    while not taT.Eof do taT.Delete;
  end;  
end;

Procedure prCreateVi(ta:TClientDataSet;V:TcxGridDBTableView);
Var i:Integer;
    CurCol: TcxGridDBColumn;
    vS1:TcxDataSummaryItem;
begin
  v.ClearItems;
  v.DataController.Summary.FooterSummaryItems.Clear;
  v.DataController.Summary.DefaultGroupSummaryItems.Clear;

  for i:=0 to ta.FieldDefs.Count-1 do
  begin
    CurCol:=v.CreateColumn;
    CurCol.Name:=V.Name+ta.FieldDefs.Items[i].DisplayName;
    CurCol.DataBinding.FieldName:=ta.FieldDefs.Items[i].Name;
    CurCol.HeaderAlignmentHorz:=taCenter;
    CurCol.Caption:=ta.FieldDefs.Items[i].DisplayName;
    CurCol.Width:=100;
    CurCol.Options.Editing:=False;

  end;
  for i:=0 to v.ColumnCount-1 do
  begin
    if  Pos('QU',v.Columns[i].Name)>0 then
    begin
      vS1:=v.DataController.Summary.DefaultGroupSummaryItems.Add;
      vS1.ItemLink:=v.Columns[i];
      vS1.Format:='0.000';
      vs1.Position:=spGroup;
//      vs1.Position:=spFooter;
      vs1.Kind:=skSum;      delay(10);
    end;
    if  (Pos('SI',v.Columns[i].Name)>0)or(Pos('SO',v.Columns[i].Name)>0)or(Pos('SN',v.Columns[i].Name)>0) then
    begin
      vS1:=v.DataController.Summary.DefaultGroupSummaryItems.Add;
      vS1.ItemLink:=v.Columns[i];
      vS1.Format:='0.00';
      vs1.Position:=spGroup;
//      vs1.Position:=spFooter;
      vs1.Kind:=skSum;      delay(10);
    end;
    if  Pos('NA',v.Columns[i].Name)>0 then
    begin
      vS1:=v.DataController.Summary.DefaultGroupSummaryItems.Add;
      vS1.ItemLink:=v.Columns[i];
      vS1.Format:='0.0'+'%';
      vs1.Position:=spGroup;
//      vs1.Position:=spFooter;
      vs1.Kind:=skAverage;      delay(10);
    end;

  end;
  for i:=0 to v.ColumnCount-1 do
  begin
    if  Pos('QU',v.Columns[i].Name)>0 then
    begin
      vS1:=v.DataController.Summary.DefaultGroupSummaryItems.Add;
      vS1.ItemLink:=v.Columns[i];
      vS1.Format:='0.000';
      vs1.Position:=spFooter;
//      vs1.Position:=spFooter;
      vs1.Kind:=skSum;      delay(10);
    end;
    if  (Pos('SI',v.Columns[i].Name)>0)or(Pos('SO',v.Columns[i].Name)>0)or(Pos('SN',v.Columns[i].Name)>0) then
    begin
      vS1:=v.DataController.Summary.DefaultGroupSummaryItems.Add;
      vS1.ItemLink:=v.Columns[i];
      vS1.Format:='0.00';
      vs1.Position:=spFooter;
//      vs1.Position:=spFooter;
      vs1.Kind:=skSum;      delay(10);
    end;
    if  Pos('NA',v.Columns[i].Name)>0 then
    begin
      vS1:=v.DataController.Summary.DefaultGroupSummaryItems.Add;
      vS1.ItemLink:=v.Columns[i];
      vS1.Format:='0.0'+'%';
      vs1.Position:=spFooter;
//      vs1.Position:=spFooter;
      vs1.Kind:=skAverage;      delay(10);
    end;

  end;
  for i:=0 to v.ColumnCount-1 do
  begin
    if  Pos('QU',v.Columns[i].Name)>0 then
    begin
      vS1:=v.DataController.Summary.FooterSummaryItems.Add;
      vS1.ItemLink:=v.Columns[i];
      vS1.Format:='0.000';
      vs1.Position:=spFooter;
//      vs1.Position:=spFooter;
      vs1.Kind:=skSum;      delay(10);
    end;
    if  (Pos('SI',v.Columns[i].Name)>0)or(Pos('SO',v.Columns[i].Name)>0)or(Pos('SN',v.Columns[i].Name)>0) then
    begin
      vS1:=v.DataController.Summary.FooterSummaryItems.Add;
      vS1.ItemLink:=v.Columns[i];
      vS1.Format:='0.00';
      vs1.Position:=spFooter;
//      vs1.Position:=spFooter;
      vs1.Kind:=skSum;      delay(10);
    end;
    if  Pos('NA',v.Columns[i].Name)>0 then
    begin
      vS1:=v.DataController.Summary.FooterSummaryItems.Add;
      vS1.ItemLink:=v.Columns[i];
      vS1.Format:='0.0'+'%';
      vs1.Position:=spFooter;
//      vs1.Position:=spFooter;
      vs1.Kind:=skAverage;      delay(10);
    end;
  end;
  //  v.DataController.KeyFieldNames:='Id';
end;


procedure prNExportExel3(Vi:tcxGridDBTableView);
Var ExcelApp, Workbook, Range, Cell1, Cell2, ArrayData  : Variant;
    iCol,iRow,i:Integer;
    RowInf:TcxRowInfo;
    iRowV,J,k,iGCount,iLev,iItem:Integer;
    StrWk:String;
    arCol:array[1..20] of integer;
    arSum:Array[1..20] of Real;
    iCountDG,ll,cc,ff:INteger;
    sField,sColumn:String;
    SumGr:tcxGridDBTableSummaryItem;

procedure prStrToArr(S:String);
Var n:Integer;
    s2:String;
begin
  for n:=1 to 20 do arSum[n]:=1000000000; //������� ��� �������
  while pos('(',S)>0 do delete(S,pos('(',S),1);
  while pos(')',S)>0 do delete(S,pos(')',S),1);
  while pos('�',S)>0 do delete(S,pos('�',S),1);
  while pos('.',S)>0 do delete(S,pos('.',S),1);
  while pos(',',S)>0 do
  begin
    if pos(',',S)>0 then S[pos(',',S)]:='&';
    if pos(',',S)>0 then S[pos(',',S)]:='^';
  end;
  while pos('&',S)>0 do S[pos('&',S)]:=',';
  n:=0;
  while S>'' do
  begin
    if Pos('^',S)>0 then
    begin
      S2:=Copy(S,1,Pos('^',S)-1);
      delete(S,1,pos('^',S));
    end else
    begin
      S2:=S;
      S:='';
    end;
    inc(n);
    arSum[n]:=StrToFloatDef(S2,0);
  end;

end;

function IsGroupingRow(ARowInfo: TcxRowInfo; ADataController: TcxCustomDataController): Boolean;
begin
  with ADataController do
    Result := ARowInfo.Level <> Groups.GroupingItemCount;
end;

begin
//������� � ������
//  Vi.BeginUpdate;

  if not IsOLEObjectInstalled('Excel.Application') then exit;
  ExcelApp := CreateOleObject('Excel.Application');
  ExcelApp.Application.EnableEvents := false;
  //� ������� �����
  Workbook := ExcelApp.WorkBooks.Add;

  for i:=1 to 20 do arCol[i]:=-1; //������� ��� �������

  iCol:=Vi.DataController.Groups.GroupingItemCount; //����� ����� �����
  for i:=0 to iCol-1 do
  begin
    arCol[i+1]:=Vi.DataController.Groups.GroupingItemIndex[i];
  end;

  iGCount:=Vi.DataController.Groups.GroupingItemCount;
  iCol:=Vi.VisibleColumnCount;

  iRow:=Vi.DataController.RowCount+1;

  iRowV:=Vi.DataController.RowCount;
//  iJ:=Vi.DataController.ItemCount;
//  iJ:=Vi.VisibleColumnCount;

  ArrayData := VarArrayCreate([1,iRow, 1, iCol+iGCount], varVariant);
  for i:=iGCount+1 to iCol+iGCount do
  begin
    ArrayData[1,i] := Vi.VisibleColumns[i-1-iGCount].Caption;
  end;

  iRow:=2;

  for i:=0 to iRowV-1 do
  begin
    with Vi.DataController do
    begin
      RowInf:=GetRowInfo(i);
      if IsGroupingRow(RowInf,Vi.DataController)=False then
      begin
        k:=1+iGCount;
        for j:=0 to Vi.ColumnCount-1 do
        begin
          if Vi.Columns[j].Visible then
          begin
            StrWk:=GetRowDisplayText(RowInf,j);
            if pos('�',StrWk)=length(StrWk) then delete(StrWk,length(StrWk),1);
            ArrayData[iRow,k] :=StrWk;
            inc(k);
          end;
        end
      end else
      begin
        iLev:=RowInf.Level;
        iItem:=Groups.GroupingItemIndex[iLev];

        ArrayData[iRow,iLev+1]:=Vi.Columns[iItem].Caption+': '+GetRowDisplayText(RowInf,iItem);
        StrWk:=Summary.GroupSummaryText[i];

        prStrToArr(StrWk); //��� �������������� ��� �� �����

{        for mm:=1 to 20 do
        begin
          if arSum[mm]<1000000000 then
          begin
            ArrayData[iRow,iLev+2+mm]:=arSum[mm];
          end;
        end;
}
        iCountDG:=Summary.DefaultGroupSummaryItems.Count;
        for ll:=0 to iCountDG-1 do
        begin
           SumGr:=(Summary.DefaultGroupSummaryItems.Items[ll] as tcxGridDBTableSummaryItem);
          if SumGr.Position=spGroup then
          begin
            sField:=SumGr.FieldName;
//            ArrayData[iRow,iLev+5+ll]:=sField;

            for cc:=iGCount+1 to iCol+iGCount do
            begin
              sColumn:=Vi.VisibleColumns[cc-1-iGCount].Name;
              delete(sColumn,1,Length(Vi.Name)); //������ �� �������� ������� �������� View
              if sColumn=sField then //����� �������
              begin
                ff:=cc;
                if arSum[ll+1]<1000000000 then
                begin
                  ArrayData[iRow,ff]:=arSum[ll+1];
                end;

              end;
            end;
          end;
        end;


      end;
      inc(iRow);
    end;
  end;

  Cell1 := WorkBook.WorkSheets[1].Cells[1,1];
  Cell2 := WorkBook.WorkSheets[1].Cells[1+iRowV,iCol+iGCount];
  Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
  Range.Value := ArrayData;

//  Vi.EndUpdate;

  ExcelApp.Visible := true;
end;

function RV( X: Double ): Double;
var  ScaledFractPart:Integer;
     Temp : Double;

begin

 ScaledFractPart := Trunc(X*100);
 if X>=0 then Temp := Trunc(Frac(X*100)*1000000)+1 else  Temp := Trunc(Frac(X*100)*1000000)-1;
 if Temp >=  500000 then ScaledFractPart := ScaledFractPart + 1;
 if Temp <= -500000 then ScaledFractPart := ScaledFractPart - 1;
{ if Temp >=  0.5 then ScaledFractPart := ScaledFractPart + 1;
 if Temp <= -0.5 then ScaledFractPart := ScaledFractPart - 1;}
 RV:= ScaledFractPart/100;
end;


function RoundVal( X: Double ): Double;
var  ScaledFractPart:Integer;
     Temp : Double;

begin
 ScaledFractPart := Trunc(X*100);
 if X>=0 then Temp := Trunc(Frac(X*100)*1000000)+1 else  Temp := Trunc(Frac(X*100)*1000000)-1;

 if Temp >=  500000 then ScaledFractPart := ScaledFractPart + 1;
 if Temp <= -500000 then ScaledFractPart := ScaledFractPart - 1;
{ if Temp >=  0.5 then ScaledFractPart := ScaledFractPart + 1;
 if Temp <= -0.5 then ScaledFractPart := ScaledFractPart - 1;}
 RoundVal := ScaledFractPart/100;
end;


function R1000( X: Double ): Double;
begin
  Result:=RoundEx(X*1000)/1000;
end;

procedure CloseTa(taT:tClientDataSet);
begin
 if taT.Active then
 begin
   taT.Close;
//   taT.Free;
   Delay(50);
   taT.CreateDataSet;
 end else taT.CreateDataSet;
end;


function IsOLEObjectInstalled(Name: String): boolean;
var
  ClassID: TCLSID;
  Rez : HRESULT;
begin

// L�� CLSID OLE-������
  Rez := CLSIDFromProgID(PWideChar(WideString(Name)), ClassID);
  if Rez = S_OK then
 // +���� ������
    Result := true
  else
    Result := false;
end;

procedure prExportExel2(Vi:tcxGridDBTableView;dsT:tDataSource;taT:tADOQuery);
Var ExcelApp, Workbook, Range, Cell1, Cell2, ArrayData  : Variant;
    iCol,iRow,i:Integer;
    NameF:String;
begin
//������� � ������
  Vi.BeginUpdate;

  dsT.DataSet:=nil;
  try
    taT.Filter:=Vi.DataController.Filter.FilterText;
    taT.Filtered:=True;

      //����� ������ ������
    if not IsOLEObjectInstalled('Excel.Application') then exit;
    ExcelApp := CreateOleObject('Excel.Application');
    ExcelApp.Application.EnableEvents := false;
    //� ������� �����
    Workbook := ExcelApp.WorkBooks.Add;

    iCol:=Vi.VisibleColumnCount;
    iRow:=taT.RecordCount+1;

    ArrayData := VarArrayCreate([1,iRow, 1, iCol], varVariant);
    for i:=1 to iCol do
    begin
      ArrayData[1,i] := Vi.VisibleColumns[i-1].Caption;
    end;

    taT.First;
    iRow:=2;
    While not taT.eof do
    begin
      for i:=1 to iCol do
      begin
        NameF:=Vi.VisibleColumns[i-1].Name;
        delete(NameF,1,Length(Vi.Name)); //������ �� �������� ������� �������� View
        ArrayData[iRow,i] := taT.Fields.FieldByName(NameF).Value;
      end;
      taT.Next; //Delay(10);
      inc(iRow);
    end;

    Cell1 := WorkBook.WorkSheets[1].Cells[1,1];
    Cell2 := WorkBook.WorkSheets[1].Cells[1+taT.RecordCount,iCol];
    Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
    Range.Value := ArrayData;

    taT.Filter:='';
    taT.Filtered:=False;

    taT.First;

    dsT.DataSet:=taT;

    Vi.EndUpdate;

    ExcelApp.Visible := true;
  except
    showmessage('������ ��� ������������... ���������� � ������� ���������.')
  end;
end;

procedure prExportExel1(Vi:tcxGridDBTableView;dsT:tDataSource;taT:tClientDataSet);
Var ExcelApp, Workbook, Range, Cell1, Cell2, ArrayData  : Variant;
    iCol,iRow,i:Integer;
    NameF:String;
begin
//������� � ������
  Vi.BeginUpdate;

  dsT.DataSet:=nil;
  try
    taT.Filter:=Vi.DataController.Filter.FilterText;
    taT.Filtered:=True;

      //����� ������ ������
    if not IsOLEObjectInstalled('Excel.Application') then exit;
    ExcelApp := CreateOleObject('Excel.Application');
    ExcelApp.Application.EnableEvents := false;
    //� ������� �����
    Workbook := ExcelApp.WorkBooks.Add;

    iCol:=Vi.VisibleColumnCount;
    iRow:=taT.RecordCount+1;

    ArrayData := VarArrayCreate([1,iRow, 1, iCol], varVariant);
    for i:=1 to iCol do
    begin
      ArrayData[1,i] := Vi.VisibleColumns[i-1].Caption;
    end;

    taT.First;
    iRow:=2;
    While not taT.eof do
    begin
      for i:=1 to iCol do
      begin
        NameF:=Vi.VisibleColumns[i-1].Name;
        delete(NameF,1,Length(Vi.Name)); //������ �� �������� ������� �������� View
        ArrayData[iRow,i]:=taT.Fields.FieldByName(NameF).Value;
      end;
      taT.Next; //Delay(10);
      inc(iRow);
    end;

    Cell1 := WorkBook.WorkSheets[1].Cells[1,1];
    Cell2 := WorkBook.WorkSheets[1].Cells[1+taT.RecordCount,iCol];
    Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
    Range.Value := ArrayData;

    taT.Filter:='';
    taT.Filtered:=False;

    taT.First;

    dsT.DataSet:=taT;

    Vi.EndUpdate;

    ExcelApp.Visible := true;
  except
    showmessage('������ ��� ������������... ���������� � ������� ���������.')
  end;
end;



function RoundEx( X: Double ): Integer;
var  ScaledFractPart:Integer;
     Temp : Double;
begin
 ScaledFractPart := Trunc(X);
 Temp := Trunc(Frac(X)*1000000)+1;
 if Temp >=  500000 then ScaledFractPart := ScaledFractPart + 1;
 if Temp <= -500000 then ScaledFractPart := ScaledFractPart - 1;
{ if Temp >=  0.5 then ScaledFractPart := ScaledFractPart + 1;
 if Temp <= -0.5 then ScaledFractPart := ScaledFractPart - 1;}
 RoundEx := ScaledFractPart;
end;

function RoundHi( X: Double ): Integer;
var  ScaledFractPart:Integer;
begin
 ScaledFractPart := Trunc(X);
 if Frac(X)>0.0001 then ScaledFractPart := ScaledFractPart + 1;
 RoundHi:=ScaledFractPart;
end;


procedure WriteLog(Strwk_: string);
var F: TextFile;
    Strwk1:String;
    FileN:String;
begin
  try
    Strwk1:='Log.txt';
    Application.ProcessMessages;
    FileN:=CommonSet.PathHistory+strwk1;
    AssignFile(F, FileN);
    if Not FileExists(FileN) then Rewrite(F)
    else                           Append(F);
    WriteLn(F,Strwk_);
    Flush(F);
  finally
    CloseFile(F);
  end;
end;



//uses MainSync;

{
Function StrToClassif(StrIn:String; Var ClRec:TClassifRec):Boolean;
Var strwk:String;
    n:Integer;
begin
  result:=True;
  for n:=1 to 4 do
  begin
    Delete(StrIn,1,pos(r,strin));
    if pos(r,strin)>0 then strwk:=Copy(StrIn,1,pos(r,strin)-1);
    Case n of
    1: begin
         ClRec.TYPE_CLASSIF:=StrToIntDef(StrWk,-1);
         if ClRec.TYPE_CLASSIF<0 then result:=False;
       end;
    2: begin
         ClRec.ID:=StrToIntDef(StrWk,-1);
         if ClRec.Id<0 then result:=False;
       end;
    3: begin
         ClRec.Id_Parent:=StrToIntDef(StrWk,-1);
         if ClRec.Id_Parent<0 then result:=False;
       end;
    4: begin
         ClRec.Name:=StrIn;
       end;
    end;
  end;
end;

}


Procedure CardsExpandLevel( Node : TTreeNode; Tree:TTreeView; quTree:TADOQuery);
Var ID , i   : Integer;
    TreeNode : TTreeNode;
Begin
// ��� ������ �������� ������ ������� ������ ���,
// ��� �� ����� ���������.
  if Node = nil then ID:=0
  else ID:=Integer(Node.Data);
  quTree.Close;
  quTree.Parameters.ParamByName('ParentID').Value:=ID;
  quTree.Parameters.ParamByName('PersonalID').Value:=Person.Id;
  quTree.Open;
  //showmessage(inttostr(ID));

  Tree.Items.BeginUpdate;
  // ��� ������ ������ �� ����������� ������ ������
  // ��������� ����� � TreeView, ��� �������� ����� � ���,
  // ������� �� ������ ��� "��������"
  for i:=1 to quTree.RecordCount do
  begin    // ������� � ���� Data ����� �� ����������������� �����(ID) � �������
    TreeNode:=Tree.Items.AddChildObject(Node, quTree.FieldByName('Name').AsString, Pointer(quTree.FieldByName('Id').AsInteger));
    TreeNode.ImageIndex:=12;
    TreeNode.SelectedIndex:=11;
    // ������� ��������� (������) �������� ����� ������ ��� ����,
    // ����� ��� ��������� [+] �� ����� � �� ����� ���� �� ��������

    if quTree.FieldByName('cnt').AsInteger>0 then Tree.Items.AddChildObject(TreeNode,'', nil);
    quTree.Next;
  end;
  Tree.Items.EndUpdate;
end;



Procedure ReadIni;
Var f:TIniFile;
begin
  f:=TIniFile.create(CurDir+CurIni);
  Person.Id:=f.ReadInteger('Config','PersonId',0);

  Commonset.CatManGroup:=f.ReadInteger('Config','CatManGroup',1007);
  f.WriteInteger('Config','CatManGroup',Commonset.CatManGroup);

  CommonSet.iMin:=f.ReadInteger('Config','iMin',0);
  f.WriteInteger('Config','iMin',CommonSet.iMin);

  CommonSet.iMax:=f.ReadInteger('Config','iMax',50000);
  f.WriteInteger('Config','iMax',CommonSet.iMax);

  CommonSet.iMinVes:=f.ReadInteger('Config','iMinVes',50000);
  f.WriteInteger('Config','iMinVes',CommonSet.iMinVes);

  CommonSet.iMaxVes:=f.ReadInteger('Config','iMaxVes',60000);
  f.WriteInteger('Config','iMaxVes',CommonSet.iMaxVes);

  CommonSet.Prefix:=f.ReadString('Config','Prefix','26');
  f.WriteString('Config','Prefix',CommonSet.Prefix);

  CommonSet.PrefixVes:=f.ReadString('Config','PrefixVes','22');
  f.WriteString('Config','PrefixVes',CommonSet.PrefixVes);

  CommonSet.PathReport:=f.ReadString('Config','PathReport',CurDir+'Report\');
  if CommonSet.PathReport[Length(CommonSet.PathReport)]<>'\' then CommonSet.PathReport:=CommonSet.PathReport+'\';
  f.WriteString('Config','PathReport',CommonSet.PathReport);

  CommonSet.NetPath:=f.ReadString('Config','NetPath',CurDir);
  if CommonSet.NetPath[Length(CommonSet.NetPath)]<>'\' then CommonSet.NetPath:=CommonSet.NetPath+'\';
  f.WriteString('Config','NetPath',CommonSet.NetPath);

  CommonSet.TrfPath:=f.ReadString('Config','TrfPath',CurDir+'TRF\');
  if CommonSet.TrfPath[Length(CommonSet.TrfPath)]<>'\' then CommonSet.TrfPath:=CommonSet.TrfPath+'\';
  f.WriteString('Config','TrfPath',CommonSet.TrfPath);

  CommonSet.POSLoad:=f.ReadString('Config','POSLoad',CurDir+'pos\cashload\');
  if CommonSet.POSLoad[Length(CommonSet.POSLoad)]<>'\' then CommonSet.POSLoad:=CommonSet.POSLoad+'\';
  f.WriteString('Config','POSLoad',CommonSet.POSLoad);

  CommonSet.POSReport:=f.ReadString('Config','POSReport',CurDir+'pos\cashrep\');
  if CommonSet.POSReport[Length(CommonSet.POSReport)]<>'\' then CommonSet.POSReport:=CommonSet.POSReport+'\';
  f.WriteString('Config','POSReport',CommonSet.POSReport);

  CommonSet.POSOper:=f.ReadString('Config','POSOper',CurDir+'pos\cashoper\');
  if CommonSet.POSOper[Length(CommonSet.POSOper)]<>'\' then CommonSet.POSOper:=CommonSet.POSOper+'\';
  f.WriteString('Config','POSOper',CommonSet.POSOper);

  CommonSet.UKM4:=f.ReadInteger('Config','UKM4',0);
  f.WriteInteger('Config','UKM4',CommonSet.UKM4);

  Person.Id:=f.ReadInteger('Config','PersInId',0);
  f.WriteInteger('Config','PersInId',Person.Id);

  CommonSet.h12:=f.ReadInteger('Config','h12',1); //���� =1 �� �������� ���� ������������� ����� � 12 ���� , ����� �� ���������
  f.WriteInteger('Config','h12',CommonSet.h12);

  CommonSet.AutoLoadCash:=f.ReadInteger('Config','AutoLoadCash',0);
  f.WriteInteger('Config','AutoLoadCash',CommonSet.AutoLoadCash);

  CommonSet.DaysTestMove:=f.ReadInteger('Config','DaysTestMove',30);
  f.WriteInteger('Config','DaysTestMove',CommonSet.DaysTestMove);

  CommonSet.PathExportBuh:=f.ReadString('Config','PathExportBuh',CurDir+'ExportBuh\');
  f.WriteString('Config','PathExportBuh',CommonSet.PathExportBuh);

  CommonSet.FileExp:=f.ReadString('Config','FileExp','export.txt');
  f.WriteString('Config','FileExp',CommonSet.FileExp);

  CommonSet.CBIP:=f.ReadString('Config','CBIP','192.168.0.72');
  f.WriteString('Config','CBIP',CommonSet.CBIP);


  {
  CommonSet.PathImport:=f.ReadString('Config','PathImport',CurDir+'Import\');
  CommonSet.PathExport:=f.ReadString('Config','PathExport',CurDir+'Export\');
  CommonSet.FtpImport:=f.ReadString('Config','FtpImport',CurDir+'temp\04\');
  CommonSet.FtpExport:=f.ReadString('Config','FtpExport',CurDir+'temp\Office\;temp\office1\');
  CommonSet.PathHistory:=f.ReadString('Config','PathHistory',CurDir+'History\');
  CommonSet.PathArh:=f.ReadString('Config','PathArh',CurDir+'Arh\');
  CommonSet.PeriodPing:=f.ReadInteger('Config','PeriodPing',120);
  CommonSet.CountStart:=f.ReadInteger('Config','CountStart',0);
  CommonSet.TrDocAutoStart:=f.ReadString('Config','TrDocAutoStart','No');
  CommonSet.TrDocDay:=f.ReadInteger('Config','TrDocDay',1);
  sFormatDate:=f.ReadString('Config','FormatDate','mm.dd.yyyy');
  CommonSet.HostUser:=f.ReadString('Config','HostUser','admin');
  CommonSet.HostPassw:=f.ReadString('Config','HostPassw','314159314159');
  CommonSet.HostIp:=f.ReadString('Config','HostIp','80.75.80.206');
  CommonSet.DBName:=f.ReadString('Config','DBName','DBName');
  CommonSet.Depart:=f.ReadInteger('Config','Depart',1);
  CommonSet.BonusPr:=f.ReadInteger('Config','BonusPr',300); //� �������� ���� 3%
  CommonSet.WorkerName:=f.ReadString('Config','WorkerName','������');
  CommonSet.ZTimeShift:=f.ReadString('Config','ZTimeShift','06:00');
  CommonSet.NoFis:=f.ReadInteger('Config','NoFis',1);
  Commonset.CashNum:=f.ReadInteger('Config','CashNum',0);
  Commonset.ComDelay:=f.ReadInteger('Config','ComDelay',100);
  Commonset.CashChNum:=f.ReadInteger('Config','CashChNum',0);
  Commonset.iCashPort:=f.ReadInteger('Config','iCashPort',0);
  Commonset.CashZ:=f.ReadInteger('Config','CashZ',0);
  Commonset.UseCashBase:=f.ReadInteger('Config','UseCashBase',0);

  UserColor.Main:=f.ReadINteger('Colors_','Main',$00FBFBFB);

  SMSSet.iSend:=f.ReadInteger('SMS','SendSMS',0);
  SMSSet.Port:=f.ReadInteger('SMS','Port',2525);
  SMSSet.ToEml:=f.ReadString('SMS','ToEml','@sms.hyper-tech.org');
  SMSSet.Host:=f.ReadString('SMS','Host','sms.hyper-tech.org');
  SMSSet.UserName:=f.ReadString('SMS','UserName','27175.1');
  SMSSet.Passw:=f.ReadString('SMS','Passw','84487355');
  SMSSet.From:=f.ReadString('SMS','From','default@siam-rus.com');
  SMSSet.TimeSec:=f.ReadInteger('SMS','TimeSec',1500);
  SMSSet.TimeSecMain:=f.ReadInteger('SMS','TimeSecMain',60);

//  CommonSet.NameDopDisc:=f.ReadString('Config','NameDopDisc','����������� ������.');

  if CommonSet.PathExport[Length(CommonSet.PathExport)]<>'\' then CommonSet.PathExport:=CommonSet.PathExport+'\';
  if CommonSet.PathImport[Length(CommonSet.PathImport)]<>'\' then CommonSet.PathImport:=CommonSet.PathImport+'\';
  if CommonSet.PathArh[Length(CommonSet.PathArh)]<>'\' then CommonSet.PathArh:=CommonSet.PathArh+'\';
  if CommonSet.PathHistory[Length(CommonSet.PathHistory)]<>'\' then CommonSet.PathHistory:=CommonSet.PathHistory+'\';


  f.WriteString('Config','ZTimeShift',CommonSet.ZTimeShift); //����� �� ������� ��� �������� ����������
  f.WriteInteger('Config','PersonId',Person.Id);
  f.WriteString('Config','PathExport',CommonSet.PathExport);
  f.WriteString('Config','PathImport',CommonSet.PathImport);
  f.WriteString('Config','FtpExport',CommonSet.FtpExport);
  f.WriteString('Config','FtpImport',CommonSet.FtpImport);
  f.WriteString('Config','PathHistory',CommonSet.PathHistory);
  f.WriteString('Config','PathArh',CommonSet.PathArh);
  f.WriteInteger('Config','PeriodPing',CommonSet.PeriodPing);
  f.WriteInteger('Config','CountStart',CommonSet.CountStart);
  f.WriteString('Config','HostIp',CommonSet.HostIp);
  f.WriteString('Config','TrDocAutoStart',CommonSet.TrDocAutoStart);
  f.WriteInteger('Config','TrDocDay',CommonSet.TrDocDay);
  f.WriteString('Config','FormatDate',sFormatDate);
  f.WriteString('Config','HostUser',CommonSet.HostUser);
  f.WriteString('Config','HostPassw',CommonSet.HostPassw);
  f.WriteString('Config','DBName',CommonSet.DBName);
  f.WriteInteger('Config','Depart',CommonSet.Depart);
  f.WriteInteger('Config','BonusPr',CommonSet.BonusPr); //� �������� ����
  f.WriteString('Config','WorkerName',CommonSet.WorkerName);
  f.WriteInteger('Config','NoFis',CommonSet.NoFis);
  f.WriteInteger('Config','CashNum',CommonSet.CashNum);
  f.WriteInteger('Config','ComDelay',Commonset.ComDelay);
  f.WriteInteger('Config','CashChNum',Commonset.CashChNum);
  f.WriteInteger('Config','iCashPort',Commonset.iCashPort);
  f.WriteInteger('Config','CashZ',Commonset.CashZ);
  f.WriteInteger('Config','UseCashBase',Commonset.UseCashBase);

  f.WriteINteger('Colors_','Main',UserColor.Main);

  f.WriteInteger('SMS','SendSMS',SMSSet.iSend);
  f.WriteString('SMS','ToEml',SMSSet.ToEml);
  f.WriteString('SMS','Host',SMSSet.Host);
  f.WriteString('SMS','UserName',SMSSet.UserName);
  f.WriteString('SMS','Passw',SMSSet.Passw);
  f.WriteInteger('SMS','Port',SMSSet.Port);
  f.WriteInteger('SMS','TimeSec',SMSSet.TimeSec);
  f.WriteInteger('SMS','TimeSecMain',SMSSet.TimeSecMain);

//  f.WriteString('Config','NameDopDisc',CommonSet.NameDopDisc);
  }
  f.Free;
end;

Procedure WriteIni;
Var f:TIniFile;
begin
  f:=TIniFile.create(CurDir+CurIni);

  f.WriteInteger('Config','PersInId',Person.Id);
{  f.WriteString('Config','PathExport',CommonSet.PathExport);
  f.WriteString('Config','PathImport',CommonSet.PathImport);
  f.WriteString('Config','FtpExport',CommonSet.FtpExport);
  f.WriteString('Config','FtpImport',CommonSet.FtpImport);
  f.WriteString('Config','PathHistory',CommonSet.PathHistory);
  f.WriteString('Config','PathArh',CommonSet.PathArh);
  f.WriteInteger('Config','PeriodPing',CommonSet.PeriodPing);
  f.WriteString('Config','HostIp',CommonSet.HostIp);
  f.WriteString('Config','TrDocAutoStart',CommonSet.TrDocAutoStart);
  f.WriteInteger('Config','TrDocDay',CommonSet.TrDocDay);
  f.WriteString('Config','HostUser',CommonSet.HostUser);
  f.WriteString('Config','HostPassw',CommonSet.HostPassw);
  f.WriteString('Config','DBName',CommonSet.DBName);
  f.WriteInteger('Config','BonusPr',CommonSet.BonusPr); //� �������� ����
  }
  f.Free;
end;


Procedure RefreshTree(Tree:TTreeView; quTree:TADOQuery;PersonalId:Integer);
begin
  while tree.Items.Count>0 do tree.Items[0].Delete;
  RExpandLevel( Nil,Tree,quTree,PersonalId);
//  tree.Items[0].Expand(True);
  delay(10);
end;

Procedure RExpandLevel( Node : TTreeNode; Tree:TTreeView; quTree:TADOQuery;PersonalId:Integer);
Var ID , i   : Integer;
    TreeNode : TTreeNode;
Begin
// ��� ������ �������� ������ ������� ������ ���,
// ��� �� ����� ���������.
  if Node = nil then ID:=0
  else ID:=Integer(Node.Data);
  quTree.Close;
  quTree.Parameters.ParamByName('ParentID').Value:=ID;
  quTree.Parameters.ParamByName('PersonalID').Value:=PersonalId;
  quTree.Open;
  Tree.Items.BeginUpdate;
  for i:=1 to quTree.RecordCount do
  begin    // ������� � ���� Data ����� �� ����������������� �����(ID) � �������
    TreeNode:=Tree.Items.AddChildObject(Node, quTree.FieldByName('Name').AsString, Pointer(quTree.FieldByName('ID_Classif').AsInteger));
    if quTree.FieldByName('Rights').AsInteger=1 then //��� �������
    begin
      TreeNode.ImageIndex:=0;
      TreeNode.SelectedIndex:=2;
    end;
    if quTree.FieldByName('Rights').AsInteger=0 then  //���� ������
    begin
      TreeNode.ImageIndex:=1;
      TreeNode.SelectedIndex:=3;
    end;
    // ������� ��������� (������) �������� ����� ������ ��� ����,
    // ����� ��� ��������� [+] �� ����� � �� ����� ���� �� ��������
    Tree.Items.AddChildObject(TreeNode,'', nil);
    quTree.Next;
  end;
  Tree.Items.EndUpdate;
end;



Procedure ExpandLevel( Node : TTreeNode; Tree:TTreeView; quTree:TADOQuery);
Var ID , i   : Integer;
    TreeNode : TTreeNode;
Begin
// ��� ������ �������� ������ ������� ������ ���,
// ��� �� ����� ���������.
  if Node = nil then ID:=0
  else ID:=Integer(Node.Data);
  quTree.Close;
  quTree.Parameters.ParamByName('PARENTID').Value:=ID;
  quTree.Open;
  Tree.Items.BeginUpdate;
  // ��� ������ ������ �� ����������� ������ ������
  // ��������� ����� � TreeView, ��� �������� ����� � ���,
  // ������� �� ������ ��� "��������"
  for i:=1 to quTree.RecordCount do
  begin    // ������� � ���� Data ����� �� ����������������� �����(ID) � �������
    TreeNode:=Tree.Items.AddChildObject(Node, quTree.FieldByName('Name').AsString, Pointer(quTree.FieldByName('ID').AsInteger));

    TreeNode.ImageIndex:=8;
    TreeNode.SelectedIndex:=7;

    if quTree.FieldByName('ID_PARENT').AsInteger=0 then
    begin
      TreeNode.ImageIndex:=0;
      TreeNode.SelectedIndex:=2;
    end;
    if quTree.FieldByName('ID_PARENT').AsInteger>0 then
    begin
      TreeNode.ImageIndex:=1;
      TreeNode.SelectedIndex:=3;
    end;
    
    // ������� ��������� (������) �������� ����� ������ ��� ����,
    // ����� ��� ��������� [+] �� ����� � �� ����� ���� �� ��������
    Tree.Items.AddChildObject(TreeNode,'', nil);
    quTree.Next;
  end;
  Tree.Items.EndUpdate;
end;



Procedure CheckNodeOn(Node:TTreeNode;bOn:Boolean);
Var ChildNode,CurNode:TTreeNode;
begin
  ChildNode:=Node.getFirstChild;
  while ChildNode<>Nil do
  begin
    if bOn then ChildNode.ImageIndex:=2
    else ChildNode.ImageIndex:=0;
    CurNode:=ChildNode;
    CheckNodeOn(CurNode,bOn);
    ChildNode:=CurNode.getNextChild(CurNode);
  end;
end;


procedure Delay(MSecs: Longint);
var
  FirstTickCount, Now: Longint;
begin
  FirstTickCount := GetTickCount;
  repeat
    Application.ProcessMessages;
    { allowing access to other controls, etc. }
    Now := GetTickCount;
  until (Now - FirstTickCount >= MSecs) or (Now < FirstTickCount);
end;


procedure WriteHistory(Strwk_: string);
var F: TextFile;
    Strwk1:String;
    FileN:String;
begin

  try
    strwk1:=FormatDateTime('yyyy_mm',Date);
    Strwk1:=StrWk1+'.txt';
    Application.ProcessMessages;
    FileN:=CommonSet.PathHistory+strwk1;
    AssignFile(F, FileN);
    if Not FileExists(FileN) then Rewrite(F)
    else                           Append(F);
    StrWk1:=FormatDateTime('dd hh:nn',now)+' ';
    WriteLn(F,StrWk1+Strwk_);
    Flush(F);
  finally
    CloseFile(F);
  end;

end;

//function Search()


procedure prNExportExel5(Vi:TcxGridDBBandedTableView);
Var ExcelApp, Workbook, Range, Cell1, Cell2, ArrayData  : Variant;
    iCol,iRow,i:Integer;
    RowInf:TcxRowInfo;
    iRowV,J,k,iGCount,iLev,iItem:Integer;
    StrWk:String;
    arCol:array[1..20] of integer;
    iCountDG,ll,cc,ff:INteger;
    sField,sColumn,sSum:String;
    SumGr:tcxGridDBTableSummaryItem;

    StarOffice,StarDesktop,Document,Sheets,Sheet,ooRange,ooArrayData:Variant;
    ii,jj:integer;
    destSoft:String;

function IsGroupingRow(ARowInfo: TcxRowInfo; ADataController: TcxCustomDataController): Boolean;
begin
  with ADataController do
    Result := ARowInfo.Level <> Groups.GroupingItemCount;
end;

begin
//������� � ������
//  Vi.BeginUpdate;

  destSoft:='non'; //������� �� �����

  if IsOLEObjectInstalled('Excel.Application')
    then
      begin
        ExcelApp := CreateOleObject('Excel.Application');
        ExcelApp.Application.EnableEvents := false;
        //� ������� �����
        Workbook := ExcelApp.WorkBooks.Add;
        destSoft := 'mse';  //excell
      end
    else destSoft:='non';

  //destSoft:='non';  // ����� ��� ����� ���� �����, "����" ������ ��� -- ��� ������ ������ ���� ���������

  if ((IsOLEObjectInstalled('com.sun.star.ServiceManager')) and (destSoft='non'))
    then
      begin
        StarOffice := CreateOleObject('com.sun.star.ServiceManager');
        StarDesktop := StarOffice.createInstance('com.sun.star.frame.Desktop');

        Document := StarDesktop.LoadComponentFromURL(
                    'private:factory/scalc', '_blank', 0,
                    VarArrayCreate([-1, -1], varVariant));
        Document.getCurrentController.getFrame.getContainerWindow.setVisible(False);
        Sheets := Document.getSheets;
        //Sheet := Sheets.getByName('����1');
        Sheet := Sheets.getByIndex(0);
        destSoft:='ooc';
      end;

  if destSoft='non' then exit;

  for i:=1 to 20 do arCol[i]:=-1; //������� ��� �������

  iCol:=Vi.DataController.Groups.GroupingItemCount; //����� ����� �����
  for i:=0 to iCol-1 do
  begin
    arCol[i+1]:=Vi.DataController.Groups.GroupingItemIndex[i];
  end;

  iGCount:=Vi.DataController.Groups.GroupingItemCount;
  iCol:=Vi.VisibleColumnCount;

  iRow:=Vi.DataController.RowCount+1;

  iRowV:=Vi.DataController.RowCount;
//  iJ:=Vi.DataController.ItemCount;
//  iJ:=Vi.VisibleColumnCount;

  ArrayData := VarArrayCreate([1,iRow, 1, iCol+iGCount], varVariant);
  for i:=iGCount+1 to iCol+iGCount do
  begin
    ArrayData[1,i] := Vi.VisibleColumns[i-1-iGCount].Caption;
  end;

  iRow:=2;

  for i:=0 to iRowV-1 do
  begin
    with Vi.DataController do
    begin
      RowInf:=GetRowInfo(i);
      if IsGroupingRow(RowInf,Vi.DataController)=False then
      begin
        k:=1+iGCount;
        for j:=0 to Vi.ColumnCount-1 do
        begin
          if Vi.Columns[j].Visible then
          begin
            StrWk:=GetRowDisplayText(RowInf,j);
            if pos('�',StrWk)=length(StrWk) then delete(StrWk,length(StrWk),1);
            //ArrayData[iRow,k] :=StrWk;
            if StrToFloatDef(StrWk,1000000)<>1000000
              then ArrayData[iRow,k]:=StrToFloatDef(StrWk,1000000)
              else ArrayData[iRow,k]:=StrWk;

            if ArrayData[iRow,k]=null then ArrayData[iRow,k]:=''; //��� �� �� ���� #�/�

            inc(k);
          end;
        end
      end else
      begin
        iLev:=RowInf.Level;
        iItem:=Groups.GroupingItemIndex[iLev];

        ArrayData[iRow,iLev+1]:=Vi.Columns[iItem].Caption+': '+GetRowDisplayText(RowInf,iItem);

//        StrWk:=Summary.GroupSummaryText[i];

//        prStrToArr(StrWk); //��� �������������� ��� �� �����

{        for mm:=1 to 20 do
        begin
          if arSum[mm]<1000000000 then
          begin
            ArrayData[iRow,iLev+2+mm]:=arSum[mm];
          end;
        end;

GroupFooterSummaryTexts[RowIndex, Level, Index: Integer]
}
        iCountDG:=Summary.DefaultGroupSummaryItems.Count;
        for ll:=0 to iCountDG-1 do
        begin
          SumGr:=(Summary.DefaultGroupSummaryItems.Items[ll] as tcxGridDBTableSummaryItem);
          if SumGr.Position=spFooter then
          begin
            sField:=SumGr.FieldName;
            sSum:=Summary.GroupFooterSummaryTexts[i,iLev,ll];

            for cc:=iGCount+1 to iCol+iGCount do
            begin
              sColumn:=Vi.VisibleColumns[cc-1-iGCount].Name;
              delete(sColumn,1,Length(Vi.Name)); //������ �� �������� ������� �������� View
              if sColumn=sField then //����� �������
              begin
                ff:=cc;
                ArrayData[iRow,ff]:=sSum;
              end;
            end;
          end;
        end;


      end;
      inc(iRow);
    end;
  end;

  if destSoft='mse' then
    begin
      Cell1 := WorkBook.WorkSheets[1].Cells[1,1];
      Cell2 := WorkBook.WorkSheets[1].Cells[1+iRowV,iCol+iGCount];
      Range := WorkBook.WorkSheets[1].Range[Cell1, Cell2];
      Range.Value := ArrayData;

      ExcelApp.Visible := true;
    end;


  if destSoft='ooc' then
    begin
      {for ii:=0 to iRow-2 do      //iRow-10
        for jj:=0 to iCol-1 do    //iCol
          begin
            cell := Sheet.getCellByPosition(jj,ii); //������
            if StrToFloatDef(ArrayData[ii+1,jj+1],1000000) = 1000000
              then Cell.SetString(ArrayData[ii+1,jj+1])
              else Cell.SetValue(ArrayData[ii+1,jj+1]);
          end;}

      ooArrayData := VarArrayCreate([1,iCol+iGCount, 1, iRow-1], varVariant);

      for jj:=1 to iRow-1 do
        for ii:=1 to iCol+iGCount do ooArrayData[ii,jj]:=ArrayData[jj,ii];

      ooRange:=Sheet.getCellRangeByPosition(0,0, iCol+iGCount-1, iRow-2);
      ooRange.setDataArray(ooArrayData);
      Document.getCurrentController.getFrame.getContainerWindow.setVisible(true);
      StarOffice := Unassigned;
    end;

end;

function GetFileVersion(const FileName: string): string;
type
  PDWORD = ^DWORD;
  PLangAndCodePage = ^TLangAndCodePage;
  TLangAndCodePage = packed record
    wLanguage: WORD;
    wCodePage: WORD;
  end;
  PLangAndCodePageArray = ^TLangAndCodePageArray;
  TLangAndCodePageArray = array[0..0] of TLangAndCodePage;
var
  loc_InfoBufSize: DWORD;
  loc_InfoBuf: PChar;
  loc_VerBufSize: DWORD;
  loc_VerBuf: PChar;
  cbTranslate: DWORD;
  lpTranslate: PDWORD;
  i: DWORD;
begin
  Result := '';
  if (Length(FileName) = 0) or (not Fileexists(FileName)) then
    Exit;
  loc_InfoBufSize := GetFileVersionInfoSize(PChar(FileName), loc_InfoBufSize);
  if loc_InfoBufSize > 0 then
  begin
    loc_VerBuf := nil;
    loc_InfoBuf := AllocMem(loc_InfoBufSize);
    try
      if not GetFileVersionInfo(PChar(FileName), 0, loc_InfoBufSize, loc_InfoBuf)
        then
        exit;
      if not VerQueryValue(loc_InfoBuf, '\\VarFileInfo\\Translation',
        Pointer(lpTranslate), DWORD(cbTranslate)) then
        exit;
      for i := 0 to (cbTranslate div SizeOf(TLangAndCodePage)) - 1 do
      begin
        if VerQueryValue(
          loc_InfoBuf,
          PChar(Format(
          'StringFileInfo\0%x0%x\FileVersion', [
          PLangAndCodePageArray(lpTranslate)[i].wLanguage,
            PLangAndCodePageArray(lpTranslate)[i].wCodePage])),
            Pointer(loc_VerBuf),
          DWORD(loc_VerBufSize)
          ) then
        begin
          Result := loc_VerBuf;
          Break;
        end;
      end;
    finally
      FreeMem(loc_InfoBuf, loc_InfoBufSize);
    end;
  end;
end;

function DeleteSpaces(Str: string): string;
var
  i: Integer;
begin
  i:=1;
  while i<=Length(Str) do
    if not (Str[i] in ['0'..'9',',','.','-']) then Delete(Str, i, 1)
    else Inc(i);
  Result:=Str;
end;

function fmt:String;
begin
  Result:=FormatDateTime('hh:nn:ss ',time);
end;


end.
