unit WriteSale;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxContainer, cxEdit, cxTextEdit,
  cxMaskEdit, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox, StdCtrls, Menus, cxLookAndFeelPainters, cxCalc,
  cxButtons, ExtCtrls, cxSpinEdit, cxCalendar;

type
  TfmWriteSale = class(TForm)
    Label1: TLabel;
    cxLookupComboBox2: TcxLookupComboBox;
    Label2: TLabel;
    cxDateEdit1: TcxDateEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    cxSpinEdit1: TcxSpinEdit;
    cxSpinEdit2: TcxSpinEdit;
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxCalcEdit1: TcxCalcEdit;
    cxCalcEdit2: TcxCalcEdit;
    cxCalcEdit3: TcxCalcEdit;
    cxCalcEdit4: TcxCalcEdit;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmWriteSale: TfmWriteSale;

implementation

{$R *.dfm}

end.
