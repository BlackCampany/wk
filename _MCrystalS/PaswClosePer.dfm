object fmPerA_ClosePer: TfmPerA_ClosePer
  Left = 701
  Top = 112
  BorderStyle = bsSingle
  Caption = #1048#1076#1077#1085#1090#1080#1092#1080#1082#1072#1094#1080#1103
  ClientHeight = 168
  ClientWidth = 298
  Color = clSilver
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 149
    Width = 298
    Height = 19
    Color = clWhite
    Panels = <
      item
        Width = 300
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 96
    Width = 298
    Height = 53
    Align = alBottom
    BevelInner = bvLowered
    Color = clWhite
    TabOrder = 1
    object Button1: TcxButton
      Left = 88
      Top = 16
      Width = 89
      Height = 25
      Caption = #1054#1082
      Default = True
      TabOrder = 0
      OnClick = Button1Click
      Colors.Default = 16776176
      Colors.Normal = 16776176
      LookAndFeel.Kind = lfOffice11
    end
    object Button2: TcxButton
      Left = 192
      Top = 16
      Width = 83
      Height = 25
      Caption = #1054#1090#1084#1077#1085#1072
      TabOrder = 1
      OnClick = Button2Click
      Colors.Default = 16776176
      Colors.Normal = 16776176
      LookAndFeel.Kind = lfOffice11
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 298
    Height = 96
    Align = alClient
    Caption = #1042#1099#1073#1077#1088#1080#1090#1077' '#1087#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1103' '#1080' '#1074#1074#1077#1076#1080#1090#1077' '#1087#1072#1088#1086#1083#1100
    Color = 16765348
    ParentColor = False
    TabOrder = 2
    object Label1: TLabel
      Left = 32
      Top = 64
      Width = 38
      Height = 13
      Caption = #1055#1072#1088#1086#1083#1100
    end
    object ComboBox1: TcxLookupComboBox
      Left = 48
      Top = 24
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Caption = #1048#1084#1103
          FieldName = 'NAME'
        end>
      Properties.ListSource = fmClosePer.dsPassw
      Properties.OnChange = ComboBox1PropertiesChange
      Style.BorderColor = clMenuHighlight
      Style.Color = 16776176
      Style.LookAndFeel.Kind = lfOffice11
      Style.Shadow = True
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 0
      Width = 217
    end
    object Edit1: TcxTextEdit
      Left = 88
      Top = 56
      Properties.EchoMode = eemPassword
      Style.Color = 16776176
      Style.Shadow = True
      TabOrder = 1
      Text = '111111111111'
      Width = 177
    end
  end
end
