object fmMainCalcSS: TfmMainCalcSS
  Left = 440
  Top = 113
  Width = 462
  Height = 576
  Caption = #1056#1072#1089#1095#1077#1090' '#1089#1077#1073#1077#1089#1090#1086#1080#1084#1086#1089#1090#1080
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxButton1: TcxButton
    Left = 264
    Top = 16
    Width = 97
    Height = 25
    Caption = #1055#1091#1089#1082
    TabOrder = 0
    OnClick = cxButton1Click
    LookAndFeel.Kind = lfOffice11
  end
  object cxCheckBox1: TcxCheckBox
    Left = 56
    Top = 20
    Caption = #1040#1074#1090#1086#1089#1090#1072#1088#1090
    State = cbsChecked
    TabOrder = 1
    Width = 121
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 519
    Width = 446
    Height = 19
    Panels = <
      item
        Width = 300
      end
      item
        Text = '14.01'
        Width = 300
      end>
  end
  object Memo1: TcxMemo
    Left = 0
    Top = 52
    Align = alBottom
    Lines.Strings = (
      'Memo1')
    TabOrder = 3
    Height = 467
    Width = 446
  end
  object Timer1: TTimer
    Enabled = False
    OnTimer = Timer1Timer
    Left = 40
    Top = 104
  end
  object am1: TActionManager
    Left = 96
    Top = 104
    StyleName = 'XP Style'
    object acStart: TAction
      Caption = 'acStart'
      OnExecute = acStartExecute
    end
  end
  object quShopsList: TADOQuery
    Connection = dmMCS.msConnection
    CommandTimeout = 500
    Parameters = <>
    SQL.Strings = (
      'SELECT [IDS] FROM [dbo].[DEPARTS]'
      'where [IDS] >0'
      'group by [IDS]'
      'order by [IDS] ')
    Left = 112
    Top = 172
    object quShopsListIDS: TIntegerField
      FieldName = 'IDS'
    end
  end
  object quDepsShop: TADOQuery
    Connection = dmMCS.msConnection
    CursorType = ctStatic
    CommandTimeout = 1800
    Parameters = <
      item
        Name = 'IDS'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT [IDS]'
      '      ,[ID]'
      '      ,[NAME]'
      '      ,[FULLNAME]'
      '      ,[ISTATUS]'
      '      ,[IDORG]'
      '      ,[GLN]'
      '      ,[ISS]'
      '      ,[PRIOR]'
      '      ,[PLATNDS]'
      '      ,[INN]'
      '      ,[KPP]'
      '      ,[DIR1]'
      '      ,[DIR2]'
      '      ,[DIR3]'
      '      ,[GB1]'
      '      ,[GB2]'
      '      ,[GB3]'
      '      ,[CITY]'
      '      ,[STREET]'
      '      ,[HOUSE]'
      '      ,[KORP]'
      '      ,[POSTINDEX]'
      '      ,[PHONE]'
      '      ,[SERLIC]'
      '      ,[NUMLIC]'
      '      ,[ORGAN]'
      '      ,[DATEB]'
      '      ,[DATEE]'
      '      ,[IP]'
      '      ,[IPREG]'
      '      ,[NAMEOTPR]'
      '      ,[ADROPR]'
      '      ,[RSCH]'
      '      ,[KSCH]'
      '      ,[BIK]'
      '      ,[BANK]'
      '  FROM [MCRYSTAL].[dbo].[DEPARTS]'
      '  where IDS=:IDS'
      'and [ISTATUS]=1'
      '')
    Left = 188
    Top = 169
    object quDepsShopIDS: TIntegerField
      FieldName = 'IDS'
    end
    object quDepsShopID: TIntegerField
      FieldName = 'ID'
    end
    object quDepsShopNAME: TStringField
      FieldName = 'NAME'
      Size = 100
    end
    object quDepsShopFULLNAME: TStringField
      FieldName = 'FULLNAME'
      Size = 150
    end
    object quDepsShopISTATUS: TSmallintField
      FieldName = 'ISTATUS'
    end
    object quDepsShopIDORG: TIntegerField
      FieldName = 'IDORG'
    end
    object quDepsShopGLN: TStringField
      FieldName = 'GLN'
      Size = 15
    end
    object quDepsShopISS: TSmallintField
      FieldName = 'ISS'
    end
    object quDepsShopPRIOR: TSmallintField
      FieldName = 'PRIOR'
    end
    object quDepsShopPLATNDS: TSmallintField
      FieldName = 'PLATNDS'
    end
    object quDepsShopINN: TStringField
      FieldName = 'INN'
      Size = 15
    end
    object quDepsShopKPP: TStringField
      FieldName = 'KPP'
      Size = 15
    end
    object quDepsShopDIR1: TStringField
      FieldName = 'DIR1'
      Size = 50
    end
    object quDepsShopDIR2: TStringField
      FieldName = 'DIR2'
      Size = 50
    end
    object quDepsShopDIR3: TStringField
      FieldName = 'DIR3'
      Size = 50
    end
    object quDepsShopGB1: TStringField
      FieldName = 'GB1'
      Size = 50
    end
    object quDepsShopGB2: TStringField
      FieldName = 'GB2'
      Size = 50
    end
    object quDepsShopGB3: TStringField
      FieldName = 'GB3'
      Size = 50
    end
    object quDepsShopCITY: TStringField
      FieldName = 'CITY'
      Size = 50
    end
    object quDepsShopSTREET: TStringField
      FieldName = 'STREET'
      Size = 50
    end
    object quDepsShopHOUSE: TStringField
      FieldName = 'HOUSE'
      Size = 10
    end
    object quDepsShopKORP: TStringField
      FieldName = 'KORP'
      Size = 5
    end
    object quDepsShopPOSTINDEX: TStringField
      FieldName = 'POSTINDEX'
      Size = 10
    end
    object quDepsShopPHONE: TStringField
      FieldName = 'PHONE'
    end
    object quDepsShopSERLIC: TStringField
      FieldName = 'SERLIC'
      Size = 50
    end
    object quDepsShopNUMLIC: TStringField
      FieldName = 'NUMLIC'
      Size = 50
    end
    object quDepsShopORGAN: TStringField
      FieldName = 'ORGAN'
      Size = 200
    end
    object quDepsShopDATEB: TDateTimeField
      FieldName = 'DATEB'
    end
    object quDepsShopDATEE: TDateTimeField
      FieldName = 'DATEE'
    end
    object quDepsShopIP: TSmallintField
      FieldName = 'IP'
    end
    object quDepsShopIPREG: TStringField
      FieldName = 'IPREG'
      Size = 200
    end
    object quDepsShopNAMEOTPR: TStringField
      FieldName = 'NAMEOTPR'
      Size = 200
    end
    object quDepsShopADROPR: TStringField
      FieldName = 'ADROPR'
      Size = 200
    end
    object quDepsShopRSCH: TStringField
      FieldName = 'RSCH'
      Size = 30
    end
    object quDepsShopKSCH: TStringField
      FieldName = 'KSCH'
      Size = 30
    end
    object quDepsShopBIK: TStringField
      FieldName = 'BIK'
      Size = 30
    end
    object quDepsShopBANK: TStringField
      FieldName = 'BANK'
      Size = 200
    end
  end
end
