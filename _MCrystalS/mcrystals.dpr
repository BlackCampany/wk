// JCL_DEBUG_EXPERT_GENERATEJDBG OFF
// JCL_DEBUG_EXPERT_INSERTJDBG OFF
// JCL_DEBUG_EXPERT_DELETEMAPFILE OFF
program mcrystals;

uses
  Forms,
  MainMC in 'MainMC.pas' {fmMainMC},
  Dm in 'DM.pas' {dmMCS: TDataModule},
  Passw in 'PasswAdmin\Passw.pas' {fmPerA},
  Un1 in 'Un1.pas',
  uTermElisey in 'uTermElisey.pas' {Service1: TService},
  Shops in 'Shops.pas' {fmShops},
  AddShop in 'AddShop.pas' {fmAddShop},
  Departs in 'Departs.pas' {fmDeparts},
  AddMH in 'AddMH.pas' {fmAddMH},
  Cards in 'Cards.pas' {fmCards},
  AddClassif in 'AddClassif.pas' {fmAddClassif},
  AddAlgClass in 'AddAlgClass.pas' {fmAddAlgClass},
  ImpExcel in 'ImpExcel.pas' {fmImpExcel},
  ClassAlg in 'ClassAlg.pas' {fmAlgClass},
  Makers in 'Makers.pas' {fmMakers},
  AddMaker in 'AddMaker.pas' {fmAddMaker},
  Brands in 'Brands.pas' {fmBrands},
  AddName in 'AddName.pas' {fmAddName},
  Country in 'Country.pas' {fmCountry},
  AddCountry in 'AddCountry.pas' {fmAddCountry},
  DepSel in 'DepSel.pas' {fmDepSel},
  mFind in 'mFind.pas' {fmFindC},
  BarcodeNo in 'BarcodeNo.pas' {fmBarMessage},
  Bar in 'Bar.pas' {fmBar},
  AddCards in 'AddCards.pas' {fmAddCard},
  ClasSel in 'ClasSel.pas' {fmClassSel},
  SetCardsPars in 'SetCardsPars.pas' {fmSetCardsParams},
  Clients in 'Clients.pas' {fmClients},
  DocHIn in 'DocHIn.pas' {fmDocsInH},
  Period1 in 'Period1.pas' {fmPeriod1},
  AddCli in 'AddCli.pas' {fmAddCli},
  mFindCli in 'mFindCli.pas' {fmFindCli},
  Labels in 'Labels.pas' {fmLabels},
  DocSIn in 'DocSIn.pas' {fmDocsInS},
  sumprops in 'sumprops.pas',
  Checks in 'Checks.pas' {fmChecks},
  DocHOut in 'DocHOut.pas' {fmDocsOutH},
  Status in 'Status.pas' {fmSt},
  PXDB in 'PXDB.pas' {dmPx: TDataModule},
  MFB in 'MFB.pas' {dmFB: TDataModule},
  u2fdk in 'U2FDK.PAS',
  SelParPrintCen in 'SelParPrintCen.pas' {fmSelParCen},
  GdsLoad in 'GdsLoad.pas' {fmGdsLoad},
  BufPrice in 'BufPrice.pas' {fmBufPrice},
  ScalesList in 'ScalesList.pas' {fmScaleList},
  Scales in 'Scales.pas' {fmScale},
  EditPlu in 'EditPlu.pas' {fmEditPlu},
  AddInScale in 'AddInScale.pas' {fmAddInScale},
  DocHInv in 'DocHInv.pas' {fmDocsInvH},
  DocSInv in 'DocSInv.pas' {fmDocsInvS},
  DocSOut in 'DocSOut.pas' {fmDocSOut},
  Move in 'Move.pas' {fmMove},
  DocAcH in 'DocAcH.pas' {fmDocsAcH},
  DocAcS in 'DocAcS.pas' {fmDocsAcS},
  CashReps in 'CashReps.pas' {fmCashRep},
  TORep in 'TORep.pas' {fmTO},
  SelTypeVnDoc in 'SelTypeVnDoc.pas' {fmSelTypeVnDoc},
  DocVnH in 'DocVnH.pas' {fmDocsVnH},
  DocVnS in 'DocVnS.pas' {fmDocsVnS},
  Period2 in 'Period2.pas' {fmPer1},
  Rep1 in 'Rep1.pas' {fmRep1},
  MessFullCashLoad in 'MessFullCashLoad.pas' {fmMessFullCashLoad},
  PeriodSingleDate in 'PeriodSingleDate.pas' {fmPeriodSingleDate},
  RepPrib in 'RepPrib.pas' {fmRepPrib},
  SummaryRDoc in 'SummaryRDoc.pas' {fmSummary1},
  SortPar in 'SortPar.pas' {fmSortF},
  ExcelList in 'ExcelList.pas' {fmExcelList},
  Period3 in 'Period3.pas' {fmPer2},
  RepPostRemn in 'RepPostRemn.pas' {fmRepPostRemn},
  RepReal in 'RepReal.pas' {fmRepReal},
  RemnsDay in 'RemnsDay.pas' {fmRemnsDay},
  RepLog in 'RepLog.pas' {fmRepLog},
  RepRemnS in 'RepRemnS.pas' {fmRepRemnSum},
  Period3Short in 'Period3Short.pas' {fmPer3},
  RepEconom in 'RepEconom.pas' {fmRepEconom},
  AddCardsParams in 'AddCardsParams.pas' {fmAddParams},
  Oplata in 'Oplata.pas' {fmOplata},
  RepPostRemnDay in 'RepPostRemnDay.pas' {fmRepPostRemnDay},
  RepSaleGr in 'RepSaleGr.pas' {fmRepSaleGr},
  RepSaleAP in 'RepSaleAP.pas' {fmRepSaleAP},
  PeriodSklList in 'PeriodSklList.pas' {fmPerSklList},
  WriteSale in 'WriteSale.pas' {fmWriteSale};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfmMainMC, fmMainMC);
  Application.CreateForm(TdmMCS, dmMCS);
  Application.CreateForm(TfmMakers, fmMakers);
  Application.CreateForm(TfmBrands, fmBrands);
  Application.CreateForm(TfmDepSel, fmDepSel);
  Application.CreateForm(TfmFindC, fmFindC);
  Application.CreateForm(TfmFindCli, fmFindCli);
  Application.CreateForm(TfmBar, fmBar);
  Application.CreateForm(TfmAddCard, fmAddCard);
  Application.CreateForm(TfmPeriod1, fmPeriod1);
  Application.CreateForm(TfmAddCli, fmAddCli);
  Application.CreateForm(TfmChecks, fmChecks);
  Application.CreateForm(TfmSt, fmSt);
  Application.CreateForm(TdmPx, dmPx);
  Application.CreateForm(TdmFB, dmFB);
  Application.CreateForm(TfmSelParCen, fmSelParCen);
  Application.CreateForm(TfmBufPrice, fmBufPrice);
  Application.CreateForm(TfmScale, fmScale);
  Application.CreateForm(TfmMove, fmMove);
  Application.CreateForm(TfmSelTypeVnDoc, fmSelTypeVnDoc);
  Application.CreateForm(TfmPer1, fmPer1);
  Application.CreateForm(TfmRepPrib, fmRepPrib);
  Application.CreateForm(TfmPer2, fmPer2);
  Application.CreateForm(TfmRepPostRemn, fmRepPostRemn);
  Application.CreateForm(TfmRepReal, fmRepReal);
  Application.CreateForm(TfmRemnsDay, fmRemnsDay);
  Application.CreateForm(TfmRepRemnSum, fmRepRemnSum);
  Application.CreateForm(TfmPer3, fmPer3);
  Application.CreateForm(TfmRepEconom, fmRepEconom);
  Application.CreateForm(TfmAddParams, fmAddParams);
  Application.CreateForm(TfmRepPostRemnDay, fmRepPostRemnDay);
  Application.CreateForm(TfmRepSaleGr, fmRepSaleGr);
  Application.CreateForm(TfmRepSaleAP, fmRepSaleAP);
  Application.CreateForm(TfmWriteSale, fmWriteSale);
  Application.Run;
end.
