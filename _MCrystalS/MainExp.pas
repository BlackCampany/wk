unit MainExp;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons, cxCheckBox,
  cxMaskEdit, cxDropDownEdit, cxCalendar, cxControls, cxContainer, cxEdit,
  cxTextEdit, cxMemo, ComCtrls, ActnList, XPStyleActnCtrls, ActnMan,
  ExtCtrls, DB, ADODB;

type
  TfmMainEXp = class(TForm)
    StatusBar1: TStatusBar;
    Memo1: TcxMemo;
    Label1: TLabel;
    DateEdit1: TcxDateEdit;
    Label2: TLabel;
    DateEdit2: TcxDateEdit;
    cxCheckBox1: TcxCheckBox;
    cxButton1: TcxButton;
    Timer1: TTimer;
    am1: TActionManager;
    acStart: TAction;
    msConnection: TADOConnection;
    quShops: TADOQuery;
    quShopsIDS: TIntegerField;
    quSelData: TADOQuery;
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure acStartExecute(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

Procedure WrMess(S:String);
procedure WriteHistory(Strwk_: string);

var
  fmMainEXp: TfmMainEXp;

implementation

uses Un1;

{$R *.dfm}

procedure WriteHistory(Strwk_: string);
var F: TextFile;
    Strwk1:String;
    FileN:String;
begin
  try
    strwk1:=FormatDateTime('yyyy_mm',Date);
    Strwk1:=StrWk1+'.txt';
    Application.ProcessMessages;
    FileN:=CurDir+strwk1;
    AssignFile(F, FileN);
    if Not FileExists(FileN) then Rewrite(F)
    else                           Append(F);
    WriteLn(F,Strwk_);
    Flush(F);
  finally
    CloseFile(F);
  end;
end;


Procedure WrMess(S:String);
Var StrWk:String;
begin
  try
    if fmMainEXp.Memo1.Lines.Count > 2000 then fmMainEXp.Memo1.Clear;
    delay(10);
    if S>'' then StrWk:=FormatDateTime('dd.mm hh:nn:ss:zzz',now)+'  '+S
    else StrWk:=S;
    WriteHistory(StrWk);
    fmMainEXp.Memo1.Lines.Add(StrWk);
    delay(10);
  except
    try
      WriteHistory(StrWk);
      StrWk:=FormatDateTime('dd.mm hh:nn:ss:zzz',now)+'  ���� ������ �������.';
      WriteHistory(StrWk);
    except
    end;
  end;
end;


procedure TfmMainEXp.FormCreate(Sender: TObject);
begin
  CurDir := ExtractFilePath(ParamStr(0));

  Memo1.Clear;

  DateEdit1.Date:=Date-2;
  DateEdit2.Date:=Date-1;

  ReadIni;

//  if (Frac(time)>=0.041666666)and(Frac(time)<=0.208333333) then  // c 1:00 �� 5:00

  cxCheckBox1.Checked:=True;
  Timer1.Enabled:=True;
end;

procedure TfmMainEXp.Timer1Timer(Sender: TObject);
Var n:Integer;
begin
  Timer1.Enabled:=False;

  for n:=0 to 10 do
  begin
    cxCheckBox1.Caption:='�� ������� '+INtToStr(10-n)+' ���.';
    delay(1000); //�������� 10 ���
    if cxCheckBox1.Checked=False then Break;
  end;

  if cxCheckBox1.Checked then
  begin
    acStart.Execute;
    Close;
  end;
end;


procedure TfmMainEXp.acStartExecute(Sender: TObject);
Var StrWk:String;
    sPath:String;
    iShop:INteger;
    fexp,fexp1:TextFile;
    iType:Integer;
    iDateB,iDateE:INteger;
    Strwk1:String;
    i:INteger;
begin
  Memo1.Clear;
  WrMess('������.');
  WrMess('  ���� �������� - '+CommonSet.PathExportBuh);

  msConnection.Connected:=False;
  Strwk:='FILE NAME='+CurDir+'mcr.udl';
  msConnection.ConnectionString:=Strwk;
  delay(10);
  msConnection.Connected:=True;
  delay(10);
  if msConnection.Connected then
  begin
    WrMess('  ���� ��');
    sPath:=CommonSet.PathExportBuh;

    try
      assignfile(fexp1,sPath+CommonSet.FileExp); //��� �����
      rewrite(fexp1);
    except
    end;

    quShops.Active:=False;
    quShops.Active:=True;
    quShops.First;
    while not quShops.Eof do
    begin
      iShop:=quShopsIDS.asInteger;
      sPath:=CommonSet.PathExportBuh+its(iShop)+'\';

      iDateB:=Trunc(DateEdit1.Date);
      iDateE:=Trunc(DateEdit2.Date);

      WrMess('    ������� � '+its(iShop));
      WrMess('      ���� - '+sPath);
      try
        assignfile(fexp,sPath+CommonSet.FileExp);
        rewrite(fexp);

        //������
        for iType:=1 to 30 do
        begin
          quSelData.Active:=False;
          quSelData.SQL.Clear;
          quSelData.SQL.Add('DECLARE @IDATEB int     ');
          quSelData.SQL.Add('DECLARE @IDATEE int     ');
          quSelData.SQL.Add('DECLARE @ISHOP int      ');
          quSelData.SQL.Add('DECLARE @ITYPED int = '+its(iType));
          quSelData.SQL.Add('Set @IDATEB='+its(iDateB));
          quSelData.SQL.Add('Set @IDATEE='+its(iDateE));
          quSelData.SQL.Add('Set @ISHOP= '+its(iShop));
//          quSelData.SQL.Add('Set @ISHOP= 2');
          quSelData.SQL.Add('EXECUTE [dbo].[prExp1C] @IDATEB,@IDATEE,@ISHOP,@ITYPED ');
          try
            quSelData.Active:=True;
            if quSelData.RecordCount>0 then
            begin
              quSelData.First;
              while not quSelData.Eof do
              begin
                Strwk1:='';
                if quSelData.Fields.Count>0 then
                begin
                  for i:=0 to quSelData.Fields.Count-1 do
                  begin
                    StrWk1:=StrWk1+';'+trim(quSelData.Fields[i].Text);
//                StrWk1:=StrWk1+';'+quSelData.Fields[i].FieldName;
                  end;
                end;
                if Length(StrWk1)>0 then
                begin
                  delete(StrWk1,1,1);
                  while pos(',',StrWk)>0 do StrWk[pos(',',StrWk)]:='.';
                  writeln(fexp,StrWk1);
                  try writeln(fexp1,StrWk1); except end;
                  WrMess(StrWk1);
                end;
                quSelData.Next;
              end;
            end;
          except
            quSelData.Active:=False;
          end;
        end;

      finally
        CloseFile(fexp);
      end;
      quShops.Next;
    end;
    quShops.Active:=False;
    try CloseFile(fexp1); except end;

    msConnection.Connected:=False;
  end else  WrMess('  ������ ��.');

  WrMess('������� ��������.');
end;

procedure TfmMainEXp.cxButton1Click(Sender: TObject);
begin
  Timer1.Enabled:=False;
  cxCheckBox1.Checked:=False;
  acStart.Execute;
end;

end.
