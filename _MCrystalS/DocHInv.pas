unit DocHInv;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SpeedBar, ExtCtrls, ComCtrls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Placemnt, cxImageComboBox, XPStyleActnCtrls, ActnList, ActnMan, Menus,
  FR_DSet, FR_DBSet, FR_Class, cxContainer, cxTextEdit, cxMemo, dxmdaset,
  cxDBLookupComboBox, cxCalendar, cxProgressBar, ADODB;

type
  TfmDocsInvH = class(TForm)
    StatusBar1: TStatusBar;
    SpeedBar1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    Timer1: TTimer;
    GridDocsInv: TcxGrid;
    ViewDocsInv: TcxGridDBTableView;
    LevelDocsInv: TcxGridLevel;
    amDocsInv: TActionManager;
    acPeriod: TAction;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    SpeedItem6: TSpeedItem;
    acAddDoc1: TAction;
    acEditDoc1: TAction;
    acViewDoc1: TAction;
    acDelDoc1: TAction;
    acOnDoc1: TAction;
    acOffDoc1: TAction;
    SpeedItem7: TSpeedItem;
    SpeedItem8: TSpeedItem;
    acVid: TAction;
    SpeedItem9: TSpeedItem;
    PopupMenu1: TPopupMenu;
    acPrint1: TAction;
    acCopy: TAction;
    acInsertD: TAction;
    SpeedItem10: TSpeedItem;
    acExpExcel: TAction;
    acEdit1: TAction;
    acTestMoveDoc: TAction;
    acOprih: TAction;
    acRazrub: TAction;
    acCheckBuh: TAction;
    teInLn: TdxMemData;
    teInLnDepart: TIntegerField;
    teInLnDateInvoice: TIntegerField;
    teInLnNumber: TStringField;
    teInLnCliName: TStringField;
    teInLnCode: TIntegerField;
    teInLniM: TIntegerField;
    teInLnNdsSum: TFloatField;
    teInLnNdsProc: TFloatField;
    teInLnKol: TFloatField;
    teInLnKolMest: TFloatField;
    teInLnKolWithMest: TFloatField;
    teInLnCenaTovar: TFloatField;
    teInLnNewCenaTovar: TFloatField;
    teInLnProc: TFloatField;
    teInLnSumCenaTovar: TFloatField;
    teInLnSumNewCenaTovar: TFloatField;
    teInLnCodeTara: TIntegerField;
    teInLnCenaTara: TFloatField;
    teInLnSumCenaTara: TFloatField;
    teInLnDepName: TStringField;
    teInLnCardName: TStringField;
    teInLniCat: TIntegerField;
    teInLniTop: TIntegerField;
    teInLniNov: TIntegerField;
    dsteInLn: TDataSource;
    Panel1: TPanel;
    Memo1: TcxMemo;
    acChangePost: TAction;
    acChangeTypePol: TAction;
    acPrintAddDocs: TAction;
    Gr1: TcxGrid;
    Vi1: TcxGridDBTableView;
    Vi1Name: TcxGridDBColumn;
    Vi1NameCli: TcxGridDBColumn;
    Vi1DateInvoice: TcxGridDBColumn;
    Vi1Number: TcxGridDBColumn;
    Vi1Kol: TcxGridDBColumn;
    Vi1CenaTovar: TcxGridDBColumn;
    Vi1Procent: TcxGridDBColumn;
    Vi1NewCenaTovar: TcxGridDBColumn;
    Vi1SumCenaTovarPost: TcxGridDBColumn;
    Vi1SumCenaTovar: TcxGridDBColumn;
    le1: TcxGridLevel;
    teInLniMaker: TIntegerField;
    teInLnsMaker: TStringField;
    PopupMenu2: TPopupMenu;
    acGroupChange: TAction;
    N13: TMenuItem;
    acMoveCard: TAction;
    teBuffLn: TdxMemData;
    teBuffLniDep: TIntegerField;
    teBuffLnNumDoc: TStringField;
    teBuffLnCode: TIntegerField;
    teInLnV11: TIntegerField;
    teBuffLniMaker: TIntegerField;
    teBuffLnsMaker: TStringField;
    teBuffLniDateDoc: TIntegerField;
    teInLnVolIn: TFloatField;
    teInLnVol: TFloatField;
    teInLniMakerCard: TIntegerField;
    teInLnsMakerCard: TStringField;
    fpfmDocsInvH: TFormPlacement;
    ViewDocsInvIDSKL: TcxGridDBColumn;
    ViewDocsInvID: TcxGridDBColumn;
    ViewDocsInvDATEDOC: TcxGridDBColumn;
    ViewDocsInvNUMDOC: TcxGridDBColumn;
    ViewDocsInvSUMINR: TcxGridDBColumn;
    ViewDocsInvSUMINF: TcxGridDBColumn;
    ViewDocsInvSUMINRTO: TcxGridDBColumn;
    ViewDocsInvRSUMR: TcxGridDBColumn;
    ViewDocsInvRSUMF: TcxGridDBColumn;
    ViewDocsInvRSUMTO: TcxGridDBColumn;
    ViewDocsInvSUMTARR: TcxGridDBColumn;
    ViewDocsInvSUMTARF: TcxGridDBColumn;
    ViewDocsInvIACTIVE: TcxGridDBColumn;
    ViewDocsInvIDETAIL: TcxGridDBColumn;
    ViewDocsInvNAMESKL: TcxGridDBColumn;
    acPrintF1: TAction;
    N11: TMenuItem;
    frRepDINV: TfrReport;
    frtaSpecInv: TfrDBDataSet;
    acPrintF2: TAction;
    N21: TMenuItem;
    quSpecInvPrint2: TADOQuery;
    quSpecInvPrint2NUM: TIntegerField;
    quSpecInvPrint2IDCARD: TIntegerField;
    quSpecInvPrint2SBAR: TStringField;
    quSpecInvPrint2QUANTR: TFloatField;
    quSpecInvPrint2QUANTF: TFloatField;
    quSpecInvPrint2PRICER: TFloatField;
    quSpecInvPrint2SUMR: TFloatField;
    quSpecInvPrint2SUMIN0: TFloatField;
    quSpecInvPrint2SUMIN: TFloatField;
    quSpecInvPrint2SUMRF: TFloatField;
    quSpecInvPrint2SUMIN0F: TFloatField;
    quSpecInvPrint2SUMINF: TFloatField;
    quSpecInvPrint2QUANTC: TFloatField;
    quSpecInvPrint2SUMC: TFloatField;
    quSpecInvPrint2SUMRSC: TFloatField;
    quSpecInvPrint2SumRTO: TFloatField;
    quSpecInvPrint2FULLNAME: TStringField;
    quSpecInvPrint2EDIZM: TSmallintField;
    quSpecInvPrint2IDCLASSIF: TIntegerField;
    quSpecInvPrint2IDHEAD: TLargeintField;
    quSpecInvPrint2ID: TLargeintField;
    ViewDocsInvDifR: TcxGridDBColumn;
    ViewDocsInvCOMMENT: TcxGridDBColumn;
    acCalcInv: TAction;
    N1: TMenuItem;
    N2: TMenuItem;
    acRecalcInvActive: TAction;
    ViewDocsInvDifIn: TcxGridDBColumn;
    procedure SpeedItem1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acPeriodExecute(Sender: TObject);
    procedure acAddDoc1Execute(Sender: TObject);
    procedure acEditDoc1Execute(Sender: TObject);
    procedure acViewDoc1Execute(Sender: TObject);
    procedure ViewDocsInvDblClick(Sender: TObject);
    procedure acDelDoc1Execute(Sender: TObject);
    procedure acOnDoc1Execute(Sender: TObject);
    procedure acOffDoc1Execute(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure acVidExecute(Sender: TObject);
    procedure SpeedItem1Click0(Sender: TObject);
    procedure acPrint1Execute(Sender: TObject);
    procedure acCopyExecute(Sender: TObject);
    procedure acInsertDExecute(Sender: TObject);
    procedure acExpExcelExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Excel1Click(Sender: TObject);
    procedure acTestMoveDocExecute(Sender: TObject);
    procedure ViewDocsInvCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure acOprihExecute(Sender: TObject);
    procedure acRazrubExecute(Sender: TObject);
    procedure acCheckBuhExecute(Sender: TObject);
    procedure acChangePostExecute(Sender: TObject);
    procedure acChangeTypePolExecute(Sender: TObject);
    procedure acPrintAddDocsExecute(Sender: TObject);
    procedure acPrintF1Execute(Sender: TObject);
    procedure acPrintF2Execute(Sender: TObject);
    procedure acCalcInvExecute(Sender: TObject);
    procedure acRecalcInvActiveExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure prButtonSet(bSet:Boolean);
    procedure prSetValsAddDoc5(iT:SmallINt); //0-����������, 1-��������������, 2-��������

  end;

Function OprDocsInv(IDH:integer; Memo1:TcxMemo):Boolean;
Function OtkatDocsInv(IDH:integer; Memo1:TcxMemo):Boolean;

var
  fmDocsInvH: TfmDocsInvH;
  bClearDocIn:Boolean = false;
  bOpenSpIn:Boolean = False;

implementation

uses Un1, DM, Period1, MainMC, DocSInv, sumprops;

{$R *.dfm}

Function OtkatDocsInv(IDH:integer; Memo1:TcxMemo):Boolean;
Var bOpr:Boolean;
begin
  Result:=False;

  Memo1.Lines.Add('');
  Memo1.Lines.Add(' ����� ... ���� ����� ���������.');

  if CanDo('prOffDocInv') then
  begin
    with dmMCS do
    begin
      bOpr:=True;

      //��� �� ���� �������� �� ��������

      if quTTnInv.Locate('ID',IDH,[])=False then bOpr:=False;

      if bOpr then
      begin
        quProc.SQL.Clear;
        quProc.SQL.Add('declare @IDHEAD int = '+its(quTTNInvID.AsInteger));
        quProc.SQL.Add('declare @IDATE int = '+its(quTTNInvIDATEDOC.AsInteger));
        quProc.SQL.Add('declare @ISKL int = '+its(quTTNInvIDSKL.AsInteger));
        quProc.SQL.Add('declare @IST int = 1');
        quProc.SQL.Add('EXECUTE [dbo].[prInvSetStatus] @IDHEAD,@IDATE,@ISKL,@IST');
        quProc.ExecSQL;
        delay(33);
        Result:=True;
        Memo1.Lines.Add(' ������� ��������.');
        delay(10);
      end;
    end;
  end else Memo1.Lines.Add('��� ����');
end;


Function OprDocsInv(IDH:integer; Memo1:TcxMemo):Boolean;
Var bOpr:Boolean;
begin
  Result:=False;

  Memo1.Lines.Add('');
  Memo1.Lines.Add(' ����� ... ���� ������������� ���������.');

  if CanDo('prOnDocInv') then
  begin
    with dmMCS do
    begin
      bOpr:=True;

      //��� �� ���� �������� �� ��������

      if quTTnInv.Locate('ID',IDH,[])=False then bOpr:=False;

      if bOpr then
      begin
        Memo1.Lines.Add('   ������ ��������� ..');
        Delay(10);
        quProc.SQL.Clear;
        quProc.SQL.Add('EXECUTE [dbo].[prCalcInvSum] '+its(IDH));
        quProc.ExecSQL;
        Memo1.Lines.Add('   ������ ��.');
        Delay(33);

        Memo1.Lines.Add('   ������������� ..');
        Delay(10);
        quProc.SQL.Clear;
        quProc.SQL.Add('declare @IDHEAD int = '+its(quTTNInvID.AsInteger));
        quProc.SQL.Add('declare @IDATE int = '+its(quTTNInvIDATEDOC.AsInteger));
        quProc.SQL.Add('declare @ISKL int = '+its(quTTNInvIDSKL.AsInteger));
        quProc.SQL.Add('declare @IST int = 3');
        quProc.SQL.Add('EXECUTE [dbo].[prInvSetStatus] @IDHEAD,@IDATE,@ISKL,@IST');
        quProc.ExecSQL;
        delay(33);
        Result:=True;
        Memo1.Lines.Add(' ������� ��������.');
        delay(10);
      end;
    end;
  end else Memo1.Lines.Add('��� ����');
end;


procedure TfmDocsInvH.prSetValsAddDoc5(iT:SmallINt); //0-����������, 1-��������������, 2-��������
// var user:string;
begin
  with dmMCS do
  begin
    bOpenSpIn:=True;
    if iT=0 then
    begin
      fmDocsInvS.Caption:='���������: ����������.';

      fmDocsInvS.Tag:=0;

      fmDocsInvS.cxTextEdit1.Text:=fGetNumDoc(fmMainMC.Label3.tag,20);  fmDocsInvS.cxTextEdit1.Properties.ReadOnly:=False;
      fmDocsInvS.cxTextEdit2.Text:='';  fmDocsInvS.cxTextEdit2.Properties.ReadOnly:=False;
      fmDocsInvS.cxTextEdit3.Text:='';  fmDocsInvS.cxTextEdit3.Properties.ReadOnly:=False;

      fmDocsInvS.cxDateEdit1.Date:=date;  fmDocsInvS.cxDateEdit1.Properties.ReadOnly:=False;

      quDepartsSt.Active:=False; quDepartsSt.Parameters.ParamByName('IPERS').Value:=Person.Id; quDepartsSt.Active:=True; quDepartsSt.Locate('ID',fmMainMC.Label3.tag,[]);

      fmDocsInvS.cxLookupComboBox1.EditValue:=quDepartsStID.AsInteger;
      fmDocsInvS.cxLookupComboBox1.Text:=quDepartsStName.AsString;
      fmDocsInvS.cxLookupComboBox1.Properties.ReadOnly:=False;

      fmDocsInvS.cxComboBox4.ItemIndex:=1;
      fmDocsInvS.cxComboBox4.Properties.ReadOnly:=False;

      fmDocsInvS.cxLabel5.Enabled:=True;
      fmDocsInvS.cxLabel1.Enabled:=True;
      fmDocsInvS.cxLabel2.Enabled:=True;
      fmDocsInvS.cxLabel6.Enabled:=True;
      fmDocsInvS.cxLabel3.Enabled:=True;
      fmDocsInvS.cxLabel4.Enabled:=True;

      fmDocsInvS.acSaveDoc.Enabled:=True;
      fmDocsInvS.cxButton1.Enabled:=True;

      fmDocsInvS.ViewDoc5.OptionsData.Editing:=True;
      fmDocsInvS.ViewDoc5.OptionsData.Deleting:=True;
    end;
    if iT=1 then
    begin
      fmDocsInvS.Caption:='���������: ��������������.';

      fmDocsInvS.Tag:=quTTNInvID.AsInteger;

      fmDocsInvS.cxTextEdit1.Text:=quTTNInvNUMDOC.AsString;  fmDocsInvS.cxTextEdit1.Properties.ReadOnly:=False;
      fmDocsInvS.cxTextEdit2.Text:='';
      fmDocsInvS.cxTextEdit3.Text:=quTTnInvCOMMENT.AsString;  fmDocsInvS.cxTextEdit3.Properties.ReadOnly:=False;

      fmDocsInvS.cxDateEdit1.Date:=quTTNInvDATEDOC.AsDateTime;  fmDocsInvS.cxDateEdit1.Properties.ReadOnly:=False;

      quDepartsSt.Active:=False; quDepartsSt.Parameters.ParamByName('IPERS').Value:=Person.Id; quDepartsSt.Active:=True; quDepartsSt.Locate('ID',quTTNInvIDSKL.AsInteger,[]);

      fmDocsInvS.cxLookupComboBox1.EditValue:=quDepartsStID.AsInteger;
      fmDocsInvS.cxLookupComboBox1.Text:=quDepartsStName.AsString;
      fmDocsInvS.cxLookupComboBox1.Properties.ReadOnly:=False;

      fmDocsInvS.cxComboBox4.ItemIndex:=quTTnInvIDETAIL.AsInteger;
      fmDocsInvS.cxComboBox4.Properties.ReadOnly:=False;

      fmDocsInvS.cxLabel5.Enabled:=True;
      fmDocsInvS.cxLabel1.Enabled:=True;
      fmDocsInvS.cxLabel2.Enabled:=True;
      fmDocsInvS.cxLabel6.Enabled:=True;
      fmDocsInvS.cxLabel3.Enabled:=True;
      fmDocsInvS.cxLabel4.Enabled:=True;

      fmDocsInvS.acSaveDoc.Enabled:=True;
      fmDocsInvS.cxButton1.Enabled:=True;

      fmDocsInvS.ViewDoc5.OptionsData.Editing:=True;
      fmDocsInvS.ViewDoc5.OptionsData.Deleting:=True;
    end;

    if iT=2 then
    begin
      fmDocsInvS.Caption:='���������: ��������.';

      fmDocsInvS.Tag:=quTTNInvID.AsInteger;

      fmDocsInvS.cxTextEdit1.Text:=quTTNInvNUMDOC.AsString;  fmDocsInvS.cxTextEdit1.Properties.ReadOnly:=True;
      fmDocsInvS.cxTextEdit2.Text:='';
      fmDocsInvS.cxTextEdit3.Text:=quTTnInvCOMMENT.AsString;  fmDocsInvS.cxTextEdit3.Properties.ReadOnly:=True;

      fmDocsInvS.cxDateEdit1.Date:=quTTNInvDATEDOC.AsDateTime;  fmDocsInvS.cxDateEdit1.Properties.ReadOnly:=True;

      quDepartsSt.Active:=False; quDepartsSt.Parameters.ParamByName('IPERS').Value:=Person.Id; quDepartsSt.Active:=True; quDepartsSt.Locate('ID',quTTNInvIDSKL.AsInteger,[]);

      fmDocsInvS.cxLookupComboBox1.EditValue:=quDepartsStID.AsInteger;
      fmDocsInvS.cxLookupComboBox1.Text:=quDepartsStName.AsString;
      fmDocsInvS.cxLookupComboBox1.Properties.ReadOnly:=True;

      fmDocsInvS.cxComboBox4.ItemIndex:=quTTnInvIDETAIL.AsInteger;
      fmDocsInvS.cxComboBox4.Properties.ReadOnly:=True;

      fmDocsInvS.cxLabel5.Enabled:=False;
      fmDocsInvS.cxLabel1.Enabled:=False;
      fmDocsInvS.cxLabel2.Enabled:=False;
      fmDocsInvS.cxLabel3.Enabled:=False;
      fmDocsInvS.cxLabel6.Enabled:=False;
      fmDocsInvS.cxLabel4.Enabled:=False;

      fmDocsInvS.acSaveDoc.Enabled:=False;
      fmDocsInvS.cxButton1.Enabled:=False;

      fmDocsInvS.ViewDoc5.OptionsData.Editing:=False;
      fmDocsInvS.ViewDoc5.OptionsData.Deleting:=False;
    end;
  end;
end;

procedure TfmDocsInvH.prButtonSet(bSet:Boolean);
begin
  SpeedItem1.Enabled:=bSet;
  SpeedItem2.Enabled:=bSet;
  SpeedItem3.Enabled:=bSet;
  SpeedItem4.Enabled:=bSet;
  SpeedItem5.Enabled:=bSet;
  SpeedItem6.Enabled:=bSet;
  SpeedItem7.Enabled:=bSet;
  SpeedItem8.Enabled:=bSet;
  SpeedItem9.Enabled:=bSet;
  SpeedItem10.Enabled:=bSet;
  delay(100);
end;

procedure TfmDocsInvH.SpeedItem1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmDocsInvH.FormCreate(Sender: TObject);
begin
  fpfmDocsInvH.IniFileName:=sFormIni;
  fpfmDocsInvH.Active:=True; delay(10);
  Timer1.Enabled:=True;
  GridDocsInv.Align:=AlClient;
  ViewDocsInv.RestoreFromIniFile(sGridIni); delay(10);
  StatusBar1.Color:=$00FFCACA;
end;

procedure TfmDocsInvH.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ViewDocsInv.StoreToIniFile(sGridIni,False); delay(10);
end;

procedure TfmDocsInvH.acPeriodExecute(Sender: TObject);
begin
//  ������
  fmPeriod1.cxDateEdit1.Date:=CommonSet.DateBeg;
  fmPeriod1.cxDateEdit2.Date:=CommonSet.DateEnd;
  fmPeriod1.Label3.Caption:='';
  fmPeriod1.ShowModal;
  if fmPeriod1.ModalResult=mrOk then
  begin
    CommonSet.DateBeg:=Trunc(fmPeriod1.cxDateEdit1.Date);
    CommonSet.DateEnd:=Trunc(fmPeriod1.cxDateEdit2.Date);

    with dmMCS do
    begin
      fmDocsInvH.Caption:='��������� �������������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd);

      fmDocsInvH.ViewDocsInv.BeginUpdate;
      try
        quTTNInv.Active:=False;
        quTTNInv.Parameters.ParamByName('IDATEB').Value:=Trunc(CommonSet.DateBeg);
        quTTNInv.Parameters.ParamByName('IDATEE').Value:=Trunc(CommonSet.DateEnd);
        quTTNInv.Parameters.ParamByName('ISKL').Value:=fmMainMC.Label3.tag;
        quTTNInv.Active:=True;
      finally
        fmDocsInvH.ViewDocsInv.EndUpdate;
      end;
    end;
  end;
end;

procedure TfmDocsInvH.acAddDoc1Execute(Sender: TObject);
//Var IDH:INteger;
//    rSum1,rSum2:Real;
begin
  //�������� ��������

  if not CanDo('prAddDocInv') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmMCS do
  begin
    prSetValsAddDoc5(0); //0-����������, 1-��������������, 2-��������

    CloseTe(fmDocsInvS.taSpecInv);
    fmDocsInvS.acSaveDoc.Enabled:=True;
    fmDocsInvS.Show;
  end;
end;

procedure TfmDocsInvH.acEditDoc1Execute(Sender: TObject);
Var iC:INteger;

//IDH:INteger;
//    rSum1,rSum2:Real;
//    iC:INteger;
//    StrWk:String;
//    rn1,rn2,rn3,rPr,rPrA:Real;
begin
  //�������������
  if not CanDo('prEditDocInv') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmMCS do
  begin
    if (quTTNInv.RecordCount>0) then //���� ��� �������������
    begin
      if (quTTnInvIACTIVE.AsInteger=1) then
      begin
        prSetValsAddDoc5(1); //0-����������, 1-��������������, 2-��������

        fmDocsInvS.Memo1.Clear; delay(20);
        fmDocsInvS.Show;  delay(20);
        fmDocsInvS.Memo1.Lines.Add('�����... ���� �������� ���������.'); delay(20);

        CloseTe(fmDocsInvS.taSpecInv);

        fmDocsInvS.ViewDoc5.BeginUpdate;
        fmDocsInvS.dstaSpecInv.DataSet:=nil;

        fmDocsInvS.Memo1.Lines.Add('  ���� ������������ ������'); delay(20);

        quSpecInv.Active:=False;
        quSpecInv.Parameters.ParamByName('IDH').Value:=quTTNInvID.AsInteger;
        quSpecInv.Active:=True;

        iC:=0;
        fmDocsInvS.Memo1.Lines.Add('  ���� �������� ������ - ����� '+its(quSpecInv.RecordCount)); delay(20);

        quSpecInv.First;
        while not quSpecInv.Eof do
        begin
          with fmDocsInvS do
          begin
            taSpecInv.Append;
            taSpecInvNum.AsInteger:=quSpecInvNUM.AsInteger;
            taSpecInvCodeTovar.AsInteger:=quSpecInvIDCARD.asinteger;
            taSpecInvBarCode.AsString:=quSpecInvSBAR.AsString;
            taSpecInvName.AsString:=quSpecInvNAME.AsString;
            taSpecInvEdIzm.AsInteger:=quSpecInvEDIZM.AsInteger;
            taSpecInvNDSProc.AsFloat:=quSpecInvNDS.AsFloat;
            taSpecInvPrice.AsFloat:=quSpecInvPRICER.AsFloat;

            try
              taSpecInvPriceSS.AsFloat:=0;
              if abs(quSpecInvQUANTR.AsFloat)>0.0001 then taSpecInvPriceSS.AsFloat:=RoundEx((quSpecInvSUMIN.AsFloat/quSpecInvQUANTR.AsFloat)*1000)/1000
              else if abs(quSpecInvQUANTF.AsFloat)>0.0001 then taSpecInvPriceSS.AsFloat:=RoundEx((quSpecInvSUMINF.AsFloat/quSpecInvQUANTF.AsFloat)*1000)/1000
            except
              taSpecInvPriceSS.AsFloat:=0;
            end;

            taSpecInvQuantR.AsFloat:=quSpecInvQUANTR.AsFloat;
            taSpecInvQuantF.AsFloat:=quSpecInvQUANTF.AsFloat;
            taSpecInvQuantC.AsFloat:=quSpecInvQUANTC.AsFloat;
            taSpecInvqOpSv.AsFloat:=0;

            taSpecInvSumR.AsFloat:=quSpecInvSUMR.AsFloat;
            taSpecInvSumF.AsFloat:=quSpecInvSUMRF.AsFloat;
            taSpecInvSumC.AsFloat:=quSpecInvSUMC.AsFloat;

            taSpecInvSumRSS.AsFloat:=quSpecInvSUMIN.AsFloat;
            taSpecInvSumFSS.AsFloat:=quSpecInvSUMINF.AsFloat;
            taSpecInvSumRSC.AsFloat:=quSpecInvSUMRSC.AsFloat;

            taSpecInvSumRTO.AsFloat:=quSpecInvSumRTO.AsFloat;

            taSpecInvCType.AsInteger:=quSpecInvTTOVAR.AsInteger;

            taSpecInviVid.AsInteger:=quSpecInvAVID.AsInteger;
            taSpecInviMaker.AsInteger:=quSpecInvMAKER.AsInteger;
  //          taSpecInvsMaker.AsString:=prFindMaker(quSpecInvMAKER.AsInteger);
            taSpecInvVol.AsFloat:=quSpecInvVOL.AsFloat;
            taSpecInvVolInv.AsFloat:=rv(quSpecInvVOL.AsFloat*quSpecInvQUANTF.AsFloat);

            taSpecInvPRICEUCH.AsFloat:=quSpecInvPRICEUCH.AsFloat;
            taSpecInv.Post;
          end;
          quSpecInv.Next; inc(iC);

          if iC mod 100 = 0 then
          begin
            fmDocsInvS.Memo1.Lines.Add('  ��������� ����� - '+its(iC)); delay(10);
          end;

        end;
        quSpecInv.Active:=False;
        fmDocsInvS.dstaSpecInv.DataSet:=fmDocsInvS.taSpecInv;
        fmDocsInvS.ViewDoc5.EndUpdate;

        fmDocsInvS.Memo1.Lines.Add('  ��������� ����� - '+its(iC)); delay(10);

        fmDocsInvS.Memo1.Lines.Add('�������� ��������.');
      end else ShowMessage('�������������� ��������������� ��������� ���������.');
    end else
    begin
      showmessage('�������� �������� ��� ��������������.');
    end;
  end;
end;

procedure TfmDocsInvH.acViewDoc1Execute(Sender: TObject);
Var iC:INteger;
//    StrWk:String;
//    rn1,rn2,rn3,rPr,rPrA:Real;
begin
  //��������
  if not CanDo('prViewDocInv') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  //��� ���������
  with dmMCS do
  begin
    if quTTNInv.RecordCount>0 then //���� ��� �������������
    begin
      prSetValsAddDoc5(2); //0-����������, 1-��������������, 2-��������

      fmDocsInvS.Memo1.Clear; delay(20);
      fmDocsInvS.Show;  delay(20);
      fmDocsInvS.Memo1.Lines.Add('�����... ���� �������� ���������.'); delay(20);

      CloseTe(fmDocsInvS.taSpecInv); delay(20);

      fmDocsInvS.ViewDoc5.BeginUpdate;

      fmDocsInvS.Memo1.Lines.Add('  ���� ������������ ������'); delay(20);

      quSpecInv.Active:=False;
      quSpecInv.Parameters.ParamByName('IDH').Value:=quTTNInvID.AsInteger;
      quSpecInv.Active:=True;

      iC:=0;
      fmDocsInvS.Memo1.Lines.Add('  ���� �������� ������ - ����� '+its(quSpecInv.RecordCount)); delay(20);
      fmDocsInvS.dstaSpecInv.DataSet:=nil;

      quSpecInv.First;
      while not quSpecInv.Eof do
      begin
        with fmDocsInvS do
        begin
          taSpecInv.Append;
          taSpecInvNum.AsInteger:=quSpecInvNUM.AsInteger;
          taSpecInvCodeTovar.AsInteger:=quSpecInvIDCARD.asinteger;
          taSpecInvBarCode.AsString:=quSpecInvSBAR.AsString;
          taSpecInvName.AsString:=quSpecInvNAME.AsString;
          taSpecInvEdIzm.AsInteger:=quSpecInvEDIZM.AsInteger;
          taSpecInvNDSProc.AsFloat:=quSpecInvNDS.AsFloat;
          taSpecInvPrice.AsFloat:=quSpecInvPRICER.AsFloat;

          try
            taSpecInvPriceSS.AsFloat:=0;
            if abs(quSpecInvQUANTR.AsFloat)>0.0001 then taSpecInvPriceSS.AsFloat:=RoundEx((quSpecInvSUMIN.AsFloat/quSpecInvQUANTR.AsFloat)*1000)/1000
            else if abs(quSpecInvQUANTF.AsFloat)>0.0001 then taSpecInvPriceSS.AsFloat:=RoundEx((quSpecInvSUMINF.AsFloat/quSpecInvQUANTF.AsFloat)*1000)/1000
          except
            taSpecInvPriceSS.AsFloat:=0;
          end;

          taSpecInvQuantR.AsFloat:=quSpecInvQUANTR.AsFloat;
          taSpecInvQuantF.AsFloat:=quSpecInvQUANTF.AsFloat;
          taSpecInvQuantC.AsFloat:=quSpecInvQUANTC.AsFloat;
          taSpecInvqOpSv.AsFloat:=0;

          taSpecInvSumR.AsFloat:=quSpecInvSUMR.AsFloat;
          taSpecInvSumF.AsFloat:=quSpecInvSUMRF.AsFloat;
          taSpecInvSumC.AsFloat:=quSpecInvSUMC.AsFloat;

          taSpecInvSumRSS.AsFloat:=quSpecInvSUMIN.AsFloat;
          taSpecInvSumFSS.AsFloat:=quSpecInvSUMINF.AsFloat;
          taSpecInvSumRSC.AsFloat:=quSpecInvSUMRSC.AsFloat;

          taSpecInvSumRTO.AsFloat:=quSpecInvSumRTO.AsFloat;

          taSpecInvCType.AsInteger:=quSpecInvTTOVAR.AsInteger;

          taSpecInviVid.AsInteger:=quSpecInvAVID.AsInteger;
          taSpecInviMaker.AsInteger:=quSpecInvMAKER.AsInteger;
//          taSpecInvsMaker.AsString:=prFindMaker(quSpecInvMAKER.AsInteger);
          taSpecInvVol.AsFloat:=quSpecInvVOL.AsFloat;
          taSpecInvVolInv.AsFloat:=rv(quSpecInvVOL.AsFloat*quSpecInvQUANTF.AsFloat);

          taSpecInvPRICEUCH.AsFloat:=quSpecInvPRICEUCH.AsFloat;

          taSpecInv.Post;  inc(iC);

          if iC mod 100 = 0 then
          begin
            fmDocsInvS.Memo1.Lines.Add('  ��������� ����� - '+its(iC)); delay(10);
          end;
        end;
        quSpecInv.Next;
      end;
      quSpecInv.Active:=False;

      fmDocsInvS.dstaSpecInv.DataSet:=fmDocsInvS.taSpecInv;
      fmDocsInvS.ViewDoc5.EndUpdate;
      fmDocsInvS.Memo1.Lines.Add('  ��������� ����� - '+its(iC)); delay(10);

      fmDocsInvS.Memo1.Lines.Add('�������� ��������.');

    end else
    begin
      showmessage('�������� �������� ��� ���������.');
    end;
  end;
end;

procedure TfmDocsInvH.ViewDocsInvDblClick(Sender: TObject);
begin
  //������� �������
  with dmMCS do
  begin
    if quTTNInv.RecordCount>0 then
    begin
      if quTTNInvIACTIVE.AsInteger<3 then acEditDoc1.Execute //��������������
      else acViewDoc1.Execute; //��������}
    end;  
  end;
end;

procedure TfmDocsInvH.acDelDoc1Execute(Sender: TObject);
begin
  if not CanDo('prDelDocInv') then begin StatusBar1.Panels[0].Text:='��� ����.'; exit; end;
  with dmMCS do
  begin
    if quTTNInv.RecordCount=0 then exit;

    prButtonSet(False);

    if quTTnInvIACTIVE.AsInteger<>3 then
    begin
      if MessageDlg('�� ������������� ������ ������� �������� �'+quTTnInvNUMDOC.AsString+' �� '+ds1(quTTnInvDATEDOC.AsDateTime),mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
//        prAddHist(20,quTTNInvID.AsInteger,quTTnInvIDATEDOC.AsInteger,quTTnInvNUMDOC.AsString,'���.',0);

//        quTTNInv.Delete;
        quD.Active:=False;
        quD.SQL.Clear;
        quD.SQL.Add('DELETE FROM [dbo].[DOCINV_HEAD]');
        quD.SQL.Add('WHERE [IDSKL]='+its(quTTnInvIDSKL.AsInteger));
        quD.SQL.Add('and [IDATEDOC]='+its(quTTnInvIDATEDOC.AsInteger));
        quD.SQL.Add('and [ID]='+its(quTTnInvID.AsInteger));
        quD.ExecSQL;
        delay(100);

        quTTNInv.Requery();

        showmessage('�������� ��');
      end;
    end else Showmessage('� ������ ������� �������� ��������� ���������.');
  end;
  prButtonSet(True);
end;

procedure TfmDocsInvH.acOnDoc1Execute(Sender: TObject);
Var IDH:INteger;
begin
  //������������
  with dmMCS do
  begin
    if not CanDo('prOnDocInv') then begin StatusBar1.Panels[0].Text:='��� ����.'; SpeedItem7.Enabled:=True; exit; end;

    if quTTNInv.RecordCount>0 then //���� ��� ������������
    begin
      if quTTNInvIACTIVE.AsInteger<=1 then
      begin
        if quTTNInvDATEDOC.AsDateTime<prOpenDate(quTTNInvIDSKL.AsInteger) then begin Memo1.Lines.Add('������ ������.'); StatusBar1.Panels[0].Text:='������ ������.'; exit; end;

        if fTestInv(quTTNInvIDSKL.AsInteger,quTTNInvIDATEDOC.AsInteger)=False then begin StatusBar1.Panels[0].Text:='������ ������ (��������������).'; exit; end;
        if quTTNInvIDSKL.AsInteger<1 then
        begin
          ShowMessage('�������� �����.');
          Memo1.Lines.Add('����� �������� �� ����������, ������������� ����������!!!');
          exit;
        end;

        if quTTNInvIDATEDOC.AsInteger<Trunc(Date) then
        begin
          StrMess:='�������� ! �������� ����� ����� ����������. ���������� ?';
          if MessageDlg(StrMess,mtConfirmation, [mbYes, mbNo], 0) <> mrYes then Exit;
        end;

        if MessageDlg('������������ �������� �'+quTTNInvNUMDOC.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          IDH:=quTTNInvID.AsInteger;
          if OprDocsInv(IDH,fmDocsInvH.Memo1) then
          begin
            ViewDocsInv.BeginUpdate;
            quTTNInv.Requery();
            quTTNInv.Locate('ID',IDH,[]);
            ViewDocsInv.EndUpdate;

            prWrLog(quTTnInvIDSKL.AsInteger,20,1,IDH,'');

            GridDocsInv.SetFocus;
            ViewDocsInv.Controller.FocusRecord(ViewDocsInv.DataController.FocusedRowIndex,True);
          end;  
        end;
      end;
    end;
  end;
end;

procedure TfmDocsInvH.acOffDoc1Execute(Sender: TObject);
Var IDH:INteger;
begin
//��������
  with dmMCS do
  begin
    if not CanDo('prOffDocInv') then begin StatusBar1.Panels[0].Text:='��� ����.'; SpeedItem8.Enabled:=True; exit; end;

    if quTTNInv.RecordCount>0 then //���� ��� ������������
    begin
      if quTTNInvIACTIVE.AsInteger=3 then
      begin
        if quTTNInvDATEDOC.AsDateTime<prOpenDate(quTTNInvIDSKL.AsInteger) then begin Memo1.Lines.Add('������ ������.'); StatusBar1.Panels[0].Text:='������ ������.'; exit; end;

//        if fTestInv(quTTNInvIDSKL.AsInteger,quTTNInvIDATEDOC.AsInteger)=False then begin StatusBar1.Panels[0].Text:='������ ������ (��������������).'; exit; end;

        if quTTNInvIDATEDOC.AsInteger<Trunc(Date) then
        begin
          StrMess:='�������� ! �������� ����� ����� ����������. ���������� ?';
          if MessageDlg(StrMess,mtConfirmation, [mbYes, mbNo], 0) <> mrYes then Exit;
        end;

        if MessageDlg('�������� �������� �'+quTTNInvNUMDOC.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          IDH:=quTTNInvID.AsInteger;
          if OtkatDocsInv(IDH,Memo1) then
          begin
            ViewDocsInv.BeginUpdate;
            quTTNInv.Requery();
            quTTNInv.Locate('ID',IDH,[]);
            ViewDocsInv.EndUpdate;

            prWrLog(quTTnInvIDSKL.AsInteger,20,0,IDH,'');

            GridDocsInv.SetFocus;
            ViewDocsInv.Controller.FocusRecord(ViewDocsInv.DataController.FocusedRowIndex,True);
          end;
        end;
      end;
    end;


  end;
end;

procedure TfmDocsInvH.Timer1Timer(Sender: TObject);
begin
  if bClearDocIn=True then begin StatusBar1.Panels[0].Text:=''; bClearDocIn:=False; end;
  if StatusBar1.Panels[0].Text>'' then bClearDocIn:=True;
end;

procedure TfmDocsInvH.acVidExecute(Sender: TObject);
//Var iC,iCliCB:INteger;
//    Sinn:String;
begin
  //���
  with dmMCS do
  begin
    {
    if LevelDocsInv.Visible then
    begin
      LevelDocsInv.Visible:=False;
      LevelCards.Visible:=True;
      Gr1.Visible:=True;

      fmDocsInvH.Caption:='������� �� ������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd-1);

      ViewCards.BeginUpdate;
      dsteInLn.DataSet:=nil;

      CloseTe(teInLn);
      if ptInLn.Active=False then ptINLn.Active:=True;

      Memo1.Clear;
      Memo1.Lines.Add('����� ���� ������������ ...');

      ptDep.First;
      while not ptDep.Eof do
      begin
        if ptDepStatus1.AsInteger=9 then
        begin
          Memo1.Lines.Add('   '+OemToAnsiConvert(ptDepName.AsString)); delay(10);
          ptInLn.CancelRange;
          ptInLn.SetRange([ptDepID.AsInteger,CommonSet.DateBeg],[ptDepID.AsInteger,CommonSet.DateEnd]);
          ptInLn.First;

          iC:=0;
          while not ptInLn.Eof do
          begin
            if taCards.FindKey([ptInLnCodeTovar.AsInteger]) then
            begin
              teInLn.Append;
              teInLnDepart.AsInteger:=ptInLnDepart.AsInteger;
              teInLnDepName.AsString:=OemToAnsiConvert(ptDepName.AsString);
              teInLnDateInvoice.AsInteger:=Trunc(ptInLnDateInvoice.AsDateTime);
              teInLnNumber.AsString:=OemToAnsiConvert(ptInLnNumber.AsString);
              teInLnCliName.AsString:=prFindCliName1(ptInLnIndexPost.AsInteger,ptInLnCodePost.AsInteger,Sinn,iCliCB);
              teInLnCode.AsInteger:=ptInLnCodeTovar.AsInteger;
              teInLnCardName.AsString:=OemToAnsiConvert(taCardsName.AsString);
              teInLniM.AsInteger:=ptInLnCodeEdIzm.AsInteger;
              teInLnKol.AsFloat:=ptInLnKol.AsFloat;
              teInLnKolMest.AsFloat:=ptInLnKolMest.AsFloat;
              teInLnKolWithMest.AsFloat:=ptInLnKolWithMest.AsFloat;
              teInLnCenaTovar.AsFloat:=ptInLnCenaTovar.AsFloat;
              teInLnNewCenaTovar.AsFloat:=ptInLnNewCenaTovar.AsFloat;
              teInLnSumCenaTovar.AsFloat:=ptInLnSumCenaTovarPost.AsFloat;
              teInLnSumNewCenaTovar.AsFloat:=ptInLnSumCenaTovar.AsFloat;
              teInLnProc.AsFloat:=ptInLnProcent.AsFloat;
              teInLnNdsProc.AsFloat:=ptInLnNDSProc.AsFloat;
              teInLnNdsSum.AsFloat:=ptInLnNDSSum.AsFloat;
              teInLnCodeTara.AsInteger:=ptInLnCodeTara.AsInteger;
              teInLnCenaTara.AsFloat:=ptInLnCenaTara.AsFloat;
              teInLnSumCenaTara.AsFloat:=ptInLnSumCenaTara.AsFloat;
              teInLniCat.AsInteger:=taCardsV02.AsInteger;
              teInLniTop.AsInteger:=taCardsV04.AsInteger;
              teInLniNov.AsInteger:=taCardsV05.AsInteger;
              teInLniMaker.AsInteger:=Trunc(ptInLnSumVesTara.AsFloat);
              teInLnsMaker.AsString:=prFindMakerName(Trunc(ptInLnSumVesTara.AsFloat));
              teInLnV11.AsInteger:=taCardsV11.AsInteger;
              teInLnVol.AsFloat:=taCardsV10.AsInteger/1000;
              teInLnVolIn.AsFloat:=(taCardsV10.AsInteger/1000)*ptInLnKol.AsFloat;
              teInLniMakerCard.AsInteger:=taCardsV12.AsInteger;
              teInLnsMakerCard.AsString:=prFindMakerName(taCardsV12.AsInteger);
              teInLn.Post;
            end;

            ptInLn.Next;
            inc(iC);
            if (iC mod 100 = 0) then
            begin
              StatusBar1.Panels[0].Text:=its(iC); delay(10);
            end;
          end;
        end;
        ptDep.Next;
      end;

      dsteInLn.DataSet:=teInLn;

      ViewCards.EndUpdate;

      SpeedItem3.Visible:=False;
      SpeedItem4.Visible:=False;
      SpeedItem5.Visible:=False;
      SpeedItem6.Visible:=False;
      SpeedItem7.Visible:=False;
      SpeedItem8.Visible:=False;

      Memo1.Lines.Add('������������ ��.');

    end else
    begin
      LevelDocsInv.Visible:=True;
      LevelCards.Visible:=False;
      Gr1.Visible:=False;

      fmDocsInvH.Caption:='��������� ��������� �� ������ � '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateBeg)+' �� '+FormatDateTiMe('dd.mm.yyyy',CommonSet.DateEnd);

      fmDocsInvH.ViewDocsIn.BeginUpdate;
      try
        quTTNInv.Active:=False;
        quTTNInv.ParamByName('DATEB').AsDate:=Trunc(CommonSet.DateBeg);
        quTTNInv.ParamByName('DATEE').AsDate:=Trunc(CommonSet.DateEnd);  // ��� <=
        quTTNInv.Active:=True;
      finally
        fmDocsInvH.ViewDocsIn.EndUpdate;
      end;

      SpeedItem3.Visible:=True;
      SpeedItem4.Visible:=True;
      SpeedItem5.Visible:=True;
      SpeedItem6.Visible:=True;
      SpeedItem7.Visible:=True;
      SpeedItem8.Visible:=True;
    end;}
  end;
end;

procedure TfmDocsInvH.SpeedItem1Click0(Sender: TObject);
begin
  Close;
end;

procedure TfmDocsInvH.acPrint1Execute(Sender: TObject);
begin
//������ �������
 { if LevelDocsInv.Visible=False then exit;
  with dmMCS do
  begin
    if quTTNInv.RecordCount>0 then //���� ��� �������������
    begin
      quSpecIn.Active:=False;
      quSpecIn.ParamByName('IDHD').AsInteger:=quTTNInvID.AsInteger;
      quSpecIn.Active:=True;

      frRepDocsIn.LoadFromFile(CurDir + 'DocInReestr.frf');

      frVariables.Variable['CliName']:=quTTNInvNAMECL.AsString;
      frVariables.Variable['DocNum']:=quTTNInvNUMDOC.AsString;
      frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',quTTNInvDATEDOC.AsDateTime);
      frVariables.Variable['DocStore']:=quTTNInvNAMEMH.AsString;
      frVariables.Variable['Depart']:=CommonSet.DepartName;

      frRepDocsIn.ReportName:='������ �� ��������� ���������.';
      frRepDocsIn.PrepareReport;
      frRepDocsIn.ShowPreparedReport;

      quSpecIn.Active:=False;
    end else
    begin
      showmessage('�������� �������� ��� ������.');
    end;
  end;}
end;

procedure TfmDocsInvH.acCopyExecute(Sender: TObject);
//var Par:Variant;
//    iC:INteger;
begin
  //����������
  with dmMCS do
  begin
    {
    if ViewDocsIn.Controller.SelectedRowCount=0 then exit;

    taHeadDoc.Active:=False;
    taHeadDoc.FileName:=CurDir+'HeadDoc.cds';
    if FileExists(CurDir+'HeadDoc.cds') then taHeadDoc.Active:=True
    else taHeadDoc.CreateDataSet;

    taSpecDoc.Active:=False;
    taSpecDoc.FileName:=CurDir+'SpecDoc.cds';
    if FileExists(CurDir+'SpecDoc.cds') then taSpecDoc.Active:=True
    else taSpecDoc.CreateDataSet;

    par := VarArrayCreate([0,1], varInteger);
    par[0]:=1;
    par[1]:=quTTNInvID.AsInteger;
    if taHeadDoc.Locate('IType;Id',par,[])=False then
    begin
      taHeadDoc.Append;
      taHeadDocIType.AsInteger:=1;
      taHeadDocId.AsInteger:=quTTNInvID.AsInteger;
      taHeadDocDateDoc.AsInteger:=Trunc(quTTNInvDATEDOC.AsDateTime);
      taHeadDocNumDoc.AsString:=quTTNInvNumber.AsString;
      taHeadDocIdCli.AsInteger:=quTTNInvCodePost.AsInteger;
      taHeadDocNameCli.AsString:=Copy(quTTNInvNameCli.AsString,1,70);
      taHeadDocIdSkl.AsInteger:=quTTNInvDepart.AsInteger;
      taHeadDocNameSkl.AsString:=Copy(quTTNInvNameDep.AsString,1,70);
      taHeadDocSumIN.AsFloat:=quTTNInvSummaTovarPost.AsFloat;
      taHeadDocSumUch.AsFloat:=quTTNInvSummaTovar.AsFloat;
      taHeadDociTypeCli.AsInteger:=quTTNInvIndexPost.AsInteger;
      taHeadDocSumTara.AsFloat:=quTTNInvSummaTara.AsFloat;
      taHeadDoc.Post;


      quSpecIn.Active:=False;
      quSpecIn.ParamByName('IDEP').AsInteger:=quTTNInvDepart.AsInteger;
      quSpecIn.ParamByName('SDATE').AsDate:=Trunc(quTTNInvDATEDOC.AsDateTime);
      quSpecIn.ParamByName('SNUM').AsString:=quTTNInvNumber.AsString;
      quSpecIn.Active:=True;

      quSpecIn.First;
      iC:=1;
      while not quSpecIn.Eof do
      begin
        taSpecDoc.Append;
        taSpecDocIType.AsInteger:=1;
        taSpecDocIdHead.AsInteger:=quTTNInvID.AsInteger;
        taSpecDocNum.AsInteger:=iC;
        taSpecDocIdCard.AsInteger:=quSpecInCodeTovar.AsInteger;
        taSpecDocQuantM.AsFloat:=quSpecInKolMest.AsFloat;
        taSpecDocQuant.AsFloat:=quSpecInKolWithMest.AsFloat;
        taSpecDocPriceIn.AsFloat:=quSpecInCenaTovar.AsFloat;
        taSpecDocSumIn.AsFloat:=quSpecInSumCenaTovarPost.AsFloat;
        taSpecDocPriceUch.AsFloat:=quSpecInNewCenaTovar.AsFloat;
        taSpecDocSumUch.AsFloat:=quSpecInSumCenaTovar.AsFloat;
        taSpecDocIdNds.AsInteger:=RoundEx(quSpecInNDSProc.AsFloat);
        taSpecDocSumNds.AsFloat:=quSpecInNDSSum.AsFloat;
        taSpecDocNameC.AsString:=Copy(quSpecInName.AsString,1,30);
        taSpecDocSm.AsString:='';
        taSpecDocIdM.AsInteger:=quSpecInCodeEdIzm.AsInteger;
        taSpecDocKm.AsFloat:=0;
        taSpecDocPriceUch1.AsFloat:=0;
        taSpecDocSumUch1.AsFloat:=0;
        taSpecDocProcPrice.AsFloat:=quSpecInProcent.AsFloat;
        taSpecDocIdTara.AsInteger:=quSpecInCodeTara.AsInteger;
        taSpecDocQuantT.AsFloat:=quSpecInKolMest.AsFloat;
        taSpecDocNameT.AsString:='';
        taSpecDocPriceT.AsFloat:=quSpecInCenaTara.AsFloat;
        taSpecDocSumTara.AsFloat:=quSpecInSumCenaTara.AsFloat;
        taSpecDocBarCode.AsString:=quSpecInBarCode.AsString;
        taSpecDoc.Post;

        quSpecIn.Next;   inc(iC);
      end;
      quSpecIn.Active:=False;
      showmessage('�������� ������� � �����.');
    end else
    begin
      showmessage('�������� ��� ���� � ������.');
    end;
    taHeadDoc.Active:=False;
    taSpecDoc.Active:=False;}
  end;
end;

procedure TfmDocsInvH.acInsertDExecute(Sender: TObject);
//Var StrWk:String;
//    rn1,rn2,rn3,rPr:Real;
begin
  // ��������
  with dmMCS do
  begin
    {
    taHeadDoc.Active:=False;
    taHeadDoc.FileName:=CurDir+'HeadDoc.cds';
    if FileExists(CurDir+'HeadDoc.cds') then taHeadDoc.Active:=True
    else taHeadDoc.CreateDataSet;

    taSpecDoc.Active:=False;
    taSpecDoc.FileName:=CurDir+'SpecDoc.cds';
    if FileExists(CurDir+'SpecDoc.cds') then taSpecDoc.Active:=True
    else taSpecDoc.CreateDataSet;

    fmTBuff:=TfmTBuff.Create(Application);

    fmTBuff.LevelD.Visible:=True;
    fmTBuff.LevelDSpec.Visible:=True;

    fmTBuff.ShowModal;
    if fmTBuff.ModalResult=mrOk then
    begin //���������
      fmTBuff.Release;
      if taHeadDoc.RecordCount>0 then
      begin
        if CanDo('prAddDocIn') then
        begin
          prSetValsAddDoc5(0); //0-����������, 1-��������������, 2-��������

          bOpen:=True; //���������� �� ������������

          fmDocsInvS.cxCurrencyEdit1.EditValue:=taHeadDocSumIN.AsFloat;

          fmDocsInvS.Label4.Tag:=taHeadDociTypeCli.AsInteger;

          if prCliNDS(taHeadDociTypeCli.AsInteger,taHeadDocIdCli.AsInteger,0)=1 then
          begin
            fmDocsInvS.Label17.Caption:='���������� ���';
            fmDocsInvS.Label17.Tag:=1;
          end else
          begin
            fmDocsInvS.Label17.Caption:='�� ���������� ���';
            fmDocsInvS.Label17.Tag:=0;
          end;

          fmDocsInvS.cxButtonEdit1.Tag:=taHeadDocIdCli.AsInteger;
          fmDocsInvS.cxButtonEdit1.Text:=taHeadDocNameCli.AsString;
          fmDocsInvS.cxButtonEdit1.Properties.ReadOnly:=False;

          quDepartsSt.Active:=False; quDepartsSt.Parameters.ParamByName('IPERS').Value:=Person.Id; quDepartsSt.Active:=True;

          fmDocsInvS.cxLookupComboBox1.EditValue:=taHeadDocIdSkl.AsInteger;
          fmDocsInvS.cxLookupComboBox1.Text:=taHeadDocNameSkl.AsString;
          fmDocsInvS.cxLookupComboBox1.Properties.ReadOnly:=False;

          fmDocsInvS.cxLabel1.Enabled:=True;
          fmDocsInvS.cxLabel2.Enabled:=True;
          fmDocsInvS.cxLabel3.Enabled:=True;
          fmDocsInvS.cxLabel4.Enabled:=True;
          fmDocsInvS.cxLabel5.Enabled:=True;
          fmDocsInvS.cxLabel6.Enabled:=True;
          fmDocsInvS.cxButton1.Enabled:=True;

          fmDocsInvS.ViewDoc1.OptionsData.Editing:=True;
          fmDocsInvS.ViewDoc1.OptionsData.Deleting:=True;
          fmDocsInvS.Panel4.Visible:=True;

          CloseTe(fmDocsInvS.taSpecIn);

          if taG.Active=False then taG.Active:=True;

          fmDocsInvS.ViewDoc1.BeginUpdate;
          taSpecDoc.First;
          while not taSpecDoc.Eof do
          begin
            if (taSpecDocIType.AsInteger=taHeadDocIType.AsInteger)and(taSpecDocIdHead.AsInteger=taHeadDocId.AsInteger) then
            begin
              with fmDocsInvS do
              with dmMt do
              begin
                if taCards.FindKey([taSpecDocIdCard.AsInteger]) then
                begin
                  prFindNacGr(taSpecDocIdCard.AsInteger,rn1,rn2,rn3,rPr);

                  taSpecIn.Append;
                  taSpecInNum.AsInteger:=taSpecDocNum.AsInteger;

                  taSpecInCodeTovar.AsInteger:=taSpecDocIdCard.AsInteger;
                  taSpecInCodeEdIzm.AsInteger:=taSpecDocIdM.AsInteger;
                  taSpecInBarCode.AsString:=taSpecDocBarCode.AsString;
                  taSpecInNDSProc.AsFloat:=taSpecDocIdNds.AsINteger;
                  taSpecInNDSSum.AsFloat:=taSpecDocSumNds.AsFloat;

                  taSpecInOutNDSSum.AsFloat:=taSpecDocSumIn.AsFloat-taSpecDocSumNds.AsFloat;
                  taSpecInBestBefore.AsDateTime:=Date;
                  taSpecInKolMest.AsInteger:=RoundEx(taSpecDocQuantM.AsFloat);
                  taSpecInKolEdMest.AsFloat:=0;
                  taSpecInKolWithMest.AsFloat:=taSpecDocQuant.AsFloat;
                  taSpecInKolWithNecond.AsFloat:=0;
                  taSpecInKol.AsFloat:=taSpecDocQuantM.AsFloat*taSpecDocQuant.AsFloat;
                  taSpecInCenaTovar.AsFloat:=taSpecDocPriceIn.AsFloat;
                  taSpecInProcent.AsFloat:=taSpecDocProcPrice.AsFloat;

                  taSpecInNewCenaTovar.AsFloat:=taSpecDocPriceUch.AsFloat;
                  taSpecInSumCenaTovarPost.AsFloat:=taSpecDocSumIn.AsFloat;
                  taSpecInSumCenaTovar.AsFloat:=taSpecDocSumUch.AsFloat;
                  taSpecInProcentN.AsFloat:=0;
                  taSpecInNecond.AsFloat:=0;
                  taSpecInCenaNecond.AsFloat:=0;
                  taSpecInSumNecond.AsFloat:=0;
                  taSpecInProcentZ.AsFloat:=0;
                  taSpecInZemlia.AsFloat:=0;
                  taSpecInProcentO.AsFloat:=0;
                  taSpecInOthodi.AsFloat:=0;
                  taSpecInName.AsString:=taSpecDocNameC.AsString;
                  taSpecInCodeTara.AsInteger:=taSpecDocIdTara.AsInteger;

                  StrWk:='';
                  if taSpecDocIdTara.AsInteger>0 then fTara(taSpecDocIdTara.AsInteger,StrWk);
                  taSpecInNameTara.AsString:=StrWk;

                  taSpecInVesTara.AsFloat:=0;
                  taSpecInCenaTara.AsFloat:=taSpecDocPriceT.AsFloat;
                  taSpecInSumCenaTara.AsFloat:=taSpecDocSumTara.AsFloat;
//                  taSpecInTovarType.AsInteger:=0;

                  taSpecInRealPrice.AsFloat:=taCardsCena.AsFloat;
                  taSpecInRemn.AsFloat:=taCardsReserv1.AsFloat;
                  taSpecInNac1.AsFloat:=rn1;
                  taSpecInNac2.AsFloat:=rn2;
                  taSpecInNac3.AsFloat:=rn3;
                  taSpecIniCat.AsINteger:=taCardsV02.AsInteger;
                  taSpecInPriceNac.AsFloat:=taCardsReserv5.AsFloat;
                  if taCardsV04.AsInteger=0 then taSpecInsTop.AsString:=' ' else taSpecInsTop.AsString:='T';
                  if taCardsV05.AsInteger=0 then taSpecInsNov.AsString:=' ' else taSpecInsNov.AsString:='N';

                  if taCardsV12.AsInteger>0 then
                  begin
                    taSpecInSumVesTara.AsFloat:=taCardsV12.AsInteger;
                    taSpecInsMaker.AsString:=prFindMakerName(taCardsV12.AsInteger);
                  end else
                  begin
                    taSpecInSumVesTara.AsFloat:=0;
                    taSpecInsMaker.AsString:='';
                  end;
                  taSpecIn.Post;

                end;
              end;
            end;
            taSpecDoc.Next;
          end;

          taHeadDoc.Active:=False;
          taSpecDoc.Active:=False;

          fmDocsInvS.ViewDoc1.EndUpdate;

          fmDocsInvS.acSaveDoc.Enabled:=True;

          bOpen:=False; //���������� �� ������������

          fmDocsInvS.Show;
        end else showmessage('��� ����.');
      end;
    end else
    begin
      fmTBuff.Release;
      taHeadDoc.Active:=False;
      taSpecDoc.Active:=False;
    end;}
  end;
end;

procedure TfmDocsInvH.acExpExcelExecute(Sender: TObject);
begin
  //������� � ������
  prNExportExel6(ViewDocsInv);
end;

procedure TfmDocsInvH.FormShow(Sender: TObject);
begin
  if (Pos('����',Person.Name)=0)and(Pos('OPER CB',Person.Name)=0)and(Pos('OPERZAK',Person.Name)=0) then
  begin
    acChangePost.Enabled:=False;
  end else
  begin
    acChangePost.Enabled:=True;
  end;
  Memo1.Clear;
end;

procedure TfmDocsInvH.Excel1Click(Sender: TObject);
begin
  prNExportExel6(ViewDocsInv);
end;

procedure TfmDocsInvH.acTestMoveDocExecute(Sender: TObject);
begin
  with dmMCS do
  begin
    if not CanDo('prTestMoveDocInv') then begin StatusBar1.Panels[0].Text:='��� ����.'; SpeedItem7.Enabled:=True; exit; end;
    //��� ���������
    {
    if quTTNInv.RecordCount>0 then //���� ��� ���������
    begin
      if quTTNInvIACTIVE.AsInteger=3 then
      begin
        if MessageDlg('��������� �������������� �� ��������� �'+quTTNInvNumber.AsString+' �� '+quTTNInvNameCli.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          prButtonSet(False);

          Memo1.Clear;
          prWH('����� ... ���� ��������.',Memo1);
         // 1 - �������� ��������������
         // 2 - � ����� ��� �������������
         // 3 - �������� ������

          quSpecIn1.Active:=False;
          quSpecIn1.ParamByName('IDEP').AsInteger:=quTTNInvDepart.AsInteger;
          quSpecIn1.ParamByName('SDATE').AsDate:=Trunc(quTTNInvDATEDOC.AsDateTime);
          quSpecIn1.ParamByName('SNUM').AsString:=quTTNInvNumber.AsString;
          quSpecIn1.Active:=True;

          quSpecIn1.First;  //��������� ����� ���  � ��������� ��������������
          while not quSpecIn1.Eof do
          begin
//            prDelTMoveId(quTTNInvDepart.AsInteger,quSpecIn1CodeTovar.AsInteger,Trunc(quTTNInvDATEDOC.AsDateTime),quTTNInvID.AsInteger,1,quSpecIn1Kol.AsFloat);
            prWh('  -- ��� '+its(quSpecIn1CodeTovar.AsInteger),Memo1); delay(10);

            //������ ��� �������� �� ���� �� ���� ���������� � ���������� ��������
            prDelTMoveDate(quTTNInvDepart.AsInteger,quSpecIn1CodeTovar.AsInteger,Trunc(quTTNInvDATEDOC.AsDateTime),1);

            //�������� ��� ������� ������ �� ���� � �������� �������� �� ����
            quSpecInTest.Active:=False;
            quSpecInTest.SQL.Clear;
            quSpecInTest.SQL.Add('select hd.ID,SUM(Ln.Kol) as Kol from "TTNInLn" ln');
            quSpecInTest.SQL.Add('left join "TTNIn" hd on (Ln.Depart=hd.Depart) and (Ln.DateInvoice=hd.DateInvoice) and(Ln.Number=hd.Number)');
            quSpecInTest.SQL.Add('where ln.DateInvoice='''+ds(quTTNInvDATEDOC.AsDateTime)+'''');
            quSpecInTest.SQL.Add('and ln.CodeTovar='+its(quSpecIn1CodeTovar.AsInteger));
            quSpecInTest.SQL.Add('and ln.Depart='+its(quTTNInvDepart.AsInteger));
            quSpecInTest.SQL.Add('and hd.AcStatus=3');
            quSpecInTest.SQL.Add('group by hd.ID');
            quSpecInTest.Active:=True;
            quSpecInTest.First;
            while not quSpecInTest.Eof do
            begin
              prAddTMoveId(quTTNInvDepart.AsInteger,quSpecIn1CodeTovar.AsInteger,Trunc(quTTNInvDATEDOC.AsDateTime),quSpecInTestID.AsInteger,1,quSpecInTestKol.AsFloat);
              delay(20);
              quSpecInTest.Next;
            end;
            quSpecInTest.Active:=False;

            quSpecIn1.Next; delay(20);
          end;
          quSpecIn1.Active:=False;

         // 4 - �������� ������

          prWh('�������� ��.',Memo1);
          prButtonSet(True);
        end;
      end;
    end;}
  end;
end;

procedure TfmDocsInvH.ViewDocsInvCustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);
Var i:Integer;
    sA:String;
begin
  sA:=' ';
  for i:=0 to ViewDocsInv.ColumnCount-1 do
  begin
    if ViewDocsInv.Columns[i].Name='ViewDocsInvIACTIVE' then
    begin
      sA:=AViewInfo.GridRecord.DisplayTexts[i];
    //  break;
    end;
  end;

//  if pos('�����',sA)=0  then  ACanvas.Canvas.Brush.Color := $00ECD9FF;
  if pos('�',sA)=0  then  ACanvas.Canvas.Brush.Color := $00B3B3FF;  //�����������
  if pos('�',sA)>0  then  ACanvas.Canvas.Brush.Color := $00E6F3DE; //����������� - ������������

// $00E6E6CA ���,  $00F4F4EA �������

end;

procedure TfmDocsInvH.acOprihExecute(Sender: TObject);
//Var bOpr:Boolean;
//    iTestZ:INteger;
//    sNumZ:String;
begin
//������������
  with dmMCS do
  begin
    if not CanDo('prOnDocIn') then begin StatusBar1.Panels[0].Text:='��� ����.'; SpeedItem7.Enabled:=True; exit; end;

    {
    if quTTNInv.RecordCount>0 then //���� ��� ������������
    begin
      if quTTNInvIACTIVE.AsInteger=0 then
      begin
        if quTTNInvDATEDOC.AsDateTime<=prOpenDate then begin StatusBar1.Panels[0].Text:='������ ������.'; exit; end;
        if fTestInv(quTTNInvDepart.AsInteger,Trunc(quTTNInvDATEDOC.AsDateTime))=False then begin StatusBar1.Panels[0].Text:='������ ������ (��������������).'; exit; end;

        if MessageDlg('������������ �������� �'+quTTNInvNumber.AsString+' �� '+quTTNInvNameCli.AsString,mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          prButtonSet(False);

          bOpr:=True;

          iTestZ:=0;
          if (quTTNInvRaion.AsInteger>=1)and(quTTNInvRaion.AsInteger<>6) then
          begin
            //��� ������
            sNumZ:=its(quTTNInvORDERNUMBER.AsInteger);
            while length(sNUmZ)<6 do sNUmZ:='0'+sNUmZ;

            quZakHNum.Active:=False;
            quZakHNum.ParamByName('SNUM').AsString:=sNumZ;
            quZakHNum.ParamByName('ICLI').AsInteger:=quTTNInvCodePost.AsInteger;
            quZakHNum.Active:=True;
            quZakHNum.First;
            if quZakHNum.RecordCount>0 then iTestZ:=quZakHNumID.AsInteger;
            quZakHNum.Active:=False;
          end;

          if bOpr = False then
          begin
            prWH('�������� !!! ������� �� ������������� �������������� �������. ������������� ����������...',Memo1);
            Showmessage('�������� !!! ������� �� ������������� �������������� �������. ������������� ����������...');
          end else
          begin
            prWH('����� ... ���� ������.',Memo1);

            prAddHist(1,quTTNInvID.AsInteger,trunc(quTTNInvDATEDOC.AsDateTime),quTTNInvNumber.AsString,'�����.',1);

           // 1 - �������� ��������������
           // 2 - � ����� ��� �������������
           // 3 - �������� ������

            quSpecIn1.Active:=False;
            quSpecIn1.ParamByName('IDEP').AsInteger:=quTTNInvDepart.AsInteger;
            quSpecIn1.ParamByName('SDATE').AsDate:=Trunc(quTTNInvDATEDOC.AsDateTime);
            quSpecIn1.ParamByName('SNUM').AsString:=quTTNInvNumber.AsString;
            quSpecIn1.Active:=True;

//            prClearBuf(quTTNInvID.AsInteger,1,Trunc(quTTNInvDATEDOC.AsDateTime),1);

            bOpr:=True;
            if NeedPost(trunc(quTTNInvDATEDOC.AsDateTime)) then
            begin
              iNumPost:=PostNum(quTTNInvID.AsInteger)*100+1;
              prWH('  ������������ ��������� ��� �� ('+its(iNumPost)+')',Memo1);
              try
                assignfile(fPost,CommonSet.TmpPath+sNumPost(iNumPost));
                rewrite(fPost);

                writeln(fPost,'TTNIN;ON;'+its(quTTNInvDepart.AsInteger)+r+its(quTTNInvID.AsInteger)+r+ds(quTTNInvDATEDOC.AsDateTime)+r+quTTNInvNumber.AsString+r+quTTNInvINN.AsString+r+fs(quTTNInvSummaTovarPost.AsFloat)+r+fs(quTTNInvSummaTovar.AsFloat)+r+fs(quTTNInvSummaTara.AsFloat)+r+fs(quTTNInvNDS10.AsFloat)+r+fs(quTTNInvNDS20.AsFloat)+r);

                quSpecIn1.First;
                while not quSpecIn1.Eof do
                begin
                  StrPost:='TTNINLN;ON;'+its(quTTNInvDepart.AsInteger)+r+its(quTTNInvID.AsInteger);
                  StrPost:=StrPost+r+its(quSpecIn1CodeTovar.asINteger)+r+fs(quSpecIn1Kol.asfloat)+r+fs(quSpecIn1CenaTovar.asfloat)+r+fs(quSpecIn1NewCenaTovar.asfloat)+r+fs(quSpecIn1SumCenaTovarPost.asfloat)+r+fs(quSpecIn1SumCenaTovar.asfloat)+r+fs(quSpecIn1Cena.asfloat)+r+fs(quSpecIn1NDSSum.asfloat)+r;
                  writeln(fPost,StrPost);
                  quSpecIn1.Next;
                end;
              finally
                closefile(fPost);
              end;

              //������ ����� - ���� ��������
              bOpr:=prTr(sNumPost(iNumPost),Memo1);
            end;

            if bOpr then
            begin
              quSpecIn1.First;  //��������� ����� ���  � ��������� ��������������
              while not quSpecIn1.Eof do
              begin
                if abs(quSpecIn1NewCenaTovar.AsFloat-quSpecIn1Cena.AsFloat)>=0.01 then //
                  prTPriceBuf(quSpecIn1CodeTovar.AsInteger,Trunc(quTTNInvDATEDOC.AsDateTime),quTTNInvID.AsInteger,1,quTTNInvNumber.AsString,quSpecIn1Cena.AsFloat,quSpecIn1NewCenaTovar.AsFloat);

                prDelTMoveId(quTTNInvDepart.AsInteger,quSpecIn1CodeTovar.AsInteger,Trunc(quTTNInvDATEDOC.AsDateTime),quTTNInvID.AsInteger,1,quSpecIn1Kol.AsFloat);
                prAddTMoveId(quTTNInvDepart.AsInteger,quSpecIn1CodeTovar.AsInteger,Trunc(quTTNInvDATEDOC.AsDateTime),quTTNInvID.AsInteger,1,quSpecIn1Kol.AsFloat);

                quSpecIn1.Next;
              end;
              quSpecIn1.Active:=False;


              if quTTNInvSummaTara.AsFloat<>0 then
              begin //�������� �� ����
                quSpecInT.Active:=False;
                quSpecInT.ParamByName('IDEP').AsInteger:=quTTNInvDepart.AsInteger;
                quSpecInT.ParamByName('SDATE').AsDate:=Trunc(quTTNInvDATEDOC.AsDateTime);
                quSpecInT.ParamByName('SNUM').AsString:=quTTNInvNumber.AsString;
                quSpecInT.Active:=True;

                quSpecInT.First;  // ��������������
                while not quSpecInT.Eof do
                begin
// ��� ������� �� ���� ��� ������������� ��� ��� ������� ���������  prDelTaraMoveId(quTTNInvDepart.AsInteger,quSpecInTCodeTara.AsInteger,Trunc(quTTNInvDATEDOC.AsDateTime),quTTNInvID.AsInteger,1,quSpecInTKolMest.AsFloat);
                  prAddTaraMoveId(quTTNInvDepart.AsInteger,quSpecInTCodeTara.AsInteger,Trunc(quTTNInvDATEDOC.AsDateTime),quTTNInvID.AsInteger,1,quSpecInTKolMest.AsFloat,quSpecInTCenaTara.AsFloat);

                  quSpecInT.Next;
                end;
                quSpecInT.Active:=False;
              end;

           // 4 - �������� ������
              quE.Active:=False;
              quE.SQL.Clear;
              quE.SQL.Add('Update "TTNIn" Set AcStatus=3');
              quE.SQL.Add('where Depart='+its(quTTNInvDepart.AsInteger));
              quE.SQL.Add('and DateInvoice='''+ds(quTTNInvDATEDOC.AsDateTime)+'''');
              quE.SQL.Add('and Number='''+quTTNInvNumber.AsString+'''');
              quE.ExecSQL;

              //��������� ������ ������
              if iTestZ>0 then
              begin
                quE.Active:=False;
                quE.SQL.Clear;
                quE.SQL.Add('Update "A_DOCZHEAD" Set IACTIVE=11');
                quE.SQL.Add('where ID='+its(iTestZ));
                quE.ExecSQL;
              end;

              prRefrID(quTTNInv,ViewDocsIn,0);

              //�������� ��
              prWh('  �������� ��.',Memo1);
              if (trunc(Date)-trunc(quTTNInvDATEDOC.AsDateTime))>=1 then prRecalcTO(trunc(quTTNInvDATEDOC.AsDateTime),trunc(Date)-1,nil,1,0);

              prWh('������ ��.',Memo1);
            end else prWh(' �������� �������� (��� �����). ���������� �����.',Memo1);
          end;

          prButtonSet(True);
        end;
      end;
    end;}
  end;
end;

procedure TfmDocsInvH.acRazrubExecute(Sender: TObject);
//Var iC:INteger;
//    StrWk:String;
//    rSumP,rQ,rNac,rSumNDS,rSumOutNDS:Real;
begin
// ������������ �������� �� ������
  with dmMCS do
  begin
    if not CanDo('prRazrIn') then begin StatusBar1.Panels[0].Text:='��� ����.'; SpeedItem7.Enabled:=True; exit; end;
    //��� ���������
    {
    if quTTNInv.RecordCount>0 then //���� ��� ������������
    begin
      if quTTNInvIACTIVE.AsInteger<3 then
      begin
        if quTTNInvDATEDOC.AsDateTime<=prOpenDate then begin StatusBar1.Panels[0].Text:='������ ������.'; exit; end;
        if fTestInv(quTTNInvDepart.AsInteger,Trunc(quTTNInvDATEDOC.AsDateTime))=False then begin StatusBar1.Panels[0].Text:='������ ������ (��������������).'; exit; end;

        if MessageDlg('������������ �������� ������������?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          // ��������� �� 2010-06-16----- ������
          prButtonSet(False);

          prWH('����� ... ���� ������������.',Memo1);

          // 4 - �������� ������
          quE.Active:=False;
          quE.SQL.Clear;
          quE.SQL.Add('Update "TTNIn" Set AcStatus=2');
          quE.SQL.Add('where Depart='+its(quTTNInvDepart.AsInteger));
          quE.SQL.Add('and DateInvoice='''+ds(quTTNInvDATEDOC.AsDateTime)+'''');
          quE.SQL.Add('and Number='''+quTTNInvNumber.AsString+'''');
          quE.ExecSQL;

          prRefrID(quTTNInv,ViewDocsIn,0);

          prWh('������ ��.',Memo1);

          prButtonSet(True);


          prSetValsAddDoc5(0); //0-����������, 1-��������������, 2-��������

          bOpen:=True; //���������� �� ������������

          fmDocsInvS.cxCurrencyEdit1.EditValue:=quTTNInvSummaTovarPost.AsFloat;
          fmDocsInvS.cxTextEdit1.Text:=quTTNInvNumber.AsString+'rzr';
          fmDocsInvS.Label4.Tag:=quTTNInvIndexPost.AsInteger;

          if prCliNDS(quTTNInvIndexPost.AsInteger,quTTNInvCodePost.AsInteger,0)=1 then
          begin
            fmDocsInvS.Label17.Caption:='���������� ���';
            fmDocsInvS.Label17.Tag:=1;
          end else
          begin
            fmDocsInvS.Label17.Caption:='�� ���������� ���';
            fmDocsInvS.Label17.Tag:=0;
          end;

          fmDocsInvS.cxButtonEdit1.Tag:=quTTNInvCodePost.AsInteger;
          fmDocsInvS.cxButtonEdit1.Text:=quTTNInvNameCli.AsString;
          fmDocsInvS.cxButtonEdit1.Properties.ReadOnly:=False;

          quDepartsSt.Active:=False; quDepartsSt.Parameters.ParamByName('IPERS').Value:=Person.Id; quDepartsSt.Active:=True;

          fmDocsInvS.cxLookupComboBox1.EditValue:=quTTNInvDepart.AsInteger;
          fmDocsInvS.cxLookupComboBox1.Text:=quTTNInvNameDep.AsString;
          fmDocsInvS.cxLookupComboBox1.Properties.ReadOnly:=False;

          fmDocsInvS.cxLabel1.Enabled:=True;
          fmDocsInvS.cxLabel2.Enabled:=True;
          fmDocsInvS.cxLabel3.Enabled:=True;
          fmDocsInvS.cxLabel4.Enabled:=True;
          fmDocsInvS.cxLabel5.Enabled:=True;
          fmDocsInvS.cxLabel6.Enabled:=True;
          fmDocsInvS.cxButton1.Enabled:=True;

          fmDocsInvS.ViewDoc1.OptionsData.Editing:=True;
          fmDocsInvS.ViewDoc1.OptionsData.Deleting:=True;
          fmDocsInvS.Panel4.Visible:=True;

          CloseTe(fmDocsInvS.taSpecIn);

          if taG.Active=False  then taG.Active:=True;
          if ptTTK.Active=False  then ptTTK.Active:=True;

          fmDocsInvS.ViewDoc1.BeginUpdate;

          quSpecIn.Active:=False;
          quSpecIn.ParamByName('IDEP').AsInteger:=quTTNInvDepart.AsInteger;
          quSpecIn.ParamByName('SDATE').AsDate:=Trunc(quTTNInvDATEDOC.AsDateTime);
          quSpecIn.ParamByName('SNUM').AsString:=quTTNInvNumber.AsString;
          quSpecIn.Active:=True;

          quSpecIn.First;
          iC:=1;
          while not quSpecIn.Eof do
          begin
            with fmDocsInvS do
            begin
              if ptTTK.Active=False then ptTTK.Active:=True;
              ptTTK.CancelRange;
              if ptTTK.locate('ID',quSpecInCodeTovar.AsInteger,[]) then
              begin
                rSumP:=rv(quSpecInSumCenaTovarPost.AsFloat);
                rSumNDS:=rv(quSpecInNDSSum.AsFloat);
                rSumOutNDS:=rv(quSpecInOutNDSSum.AsFloat);

                ptTTK.CancelRange;
                ptTTK.SetRange([quSpecInCodeTovar.AsInteger,0],[quSpecInCodeTovar.AsInteger,100]);
                ptTTK.First;
                while not ptTTK.Eof do
                begin
                  rNac:=ptTTKPROC2.AsFloat;
                  if rNac<1 then rNac:=rNac*100-100;

                  if dmMT.taG.FindKey([ptTTKIDC.AsInteger]) then
                  begin
                    rQ:=R1000((quSpecInKolWithMest.AsFloat*quSpecInKolMest.AsInteger)*ptTTKPROC1.AsFloat/100);

                    taSpecIn.Append;
                    taSpecInNum.AsInteger:=iC;
                    taSpecInCodeTovar.AsInteger:=ptTTKIDC.AsInteger;
                    taSpecInCodeEdIzm.AsInteger:=dmMt.taGEdIzm.AsInteger;
                    taSpecInBarCode.AsString:=dmMt.taGBarCode.AsString;
                    taSpecInNDSProc.AsFloat:=dmMt.taGNDS.AsFloat;
                    taSpecInNDSSum.AsFloat:=rv(rQ*quSpecInCenaTovar.AsFloat*dmMt.taGNDS.AsFloat/(100+dmMt.taGNDS.AsFloat));
                    taSpecInOutNDSSum.AsFloat:=rv(rQ*quSpecInCenaTovar.AsFloat)-rv(rQ*quSpecInCenaTovar.AsFloat*dmMt.taGNDS.AsFloat/(100+dmMt.taGNDS.AsFloat));
                    taSpecInBestBefore.AsDateTime:=date+dmMt.taGSrokReal.AsInteger;
                    taSpecInKolMest.AsInteger:=1;
                    taSpecInKolEdMest.AsFloat:=0;
                    taSpecInKolWithMest.AsFloat:=rQ;
                    taSpecInKolWithNecond.AsFloat:=rQ;
                    taSpecInKol.AsFloat:=rQ;
                    taSpecInCenaTovar.AsFloat:=quSpecInCenaTovar.AsFloat;
                    taSpecInProcent.AsFloat:=rNac;
                    taSpecInNewCenaTovar.AsFloat:=rv(quSpecInCenaTovar.AsFloat+quSpecInCenaTovar.AsFloat*rNac/100);
                    taSpecInSumCenaTovarPost.AsFloat:=rv(rQ*quSpecInCenaTovar.AsFloat);
                    taSpecInSumCenaTovar.AsFloat:=rQ*rv(quSpecInCenaTovar.AsFloat+quSpecInCenaTovar.AsFloat*rNac/100);
                    taSpecInProcentN.AsFloat:=0;
                    taSpecInNecond.AsFloat:=0;
                    taSpecInCenaNecond.AsFloat:=0;
                    taSpecInSumNecond.AsFloat:=0;
                    taSpecInProcentZ.AsFloat:=0;
                    taSpecInZemlia.AsFloat:=0;
                    taSpecInProcentO.AsFloat:=0;
                    taSpecInOthodi.AsFloat:=0;
                    taSpecInName.AsString:=OemToAnsiConvert(dmMt.taGName.AsString);
                    taSpecInCodeTara.AsInteger:=0;
                    taSpecInNameTara.AsString:='';

                    taSpecInVesTara.AsFloat:=0;
                    taSpecInCenaTara.AsFloat:=0;
                    taSpecInSumVesTara.AsFloat:=0;
                    taSpecInSumCenaTara.AsFloat:=0;
                    taSpecInTovarType.AsInteger:=dmMt.taGTovarType.AsInteger;
                    taSpecInRealPrice.AsFloat:=dmMt.taGCena.AsFloat;
                    taSpecIniCat.AsInteger:=dmMt.taGV02.AsInteger;
                    if taGV04.AsInteger=0 then taSpecInsTop.AsString:=' ' else taSpecInsTop.AsString:='T';
                    if taGV05.AsInteger=0 then taSpecInsNov.AsString:=' ' else taSpecInsNov.AsString:='N';

                    taSpecInsMaker.AsString:='';
                    taSpecIn.Post;

                    rSumP:=rSumP-rv(rQ*quSpecInCenaTovar.AsFloat);
                    rSumNDS:=rSumNDS-rv(rQ*quSpecInCenaTovar.AsFloat*dmMt.taGNDS.AsFloat/(100+dmMt.taGNDS.AsFloat));
                    rSumOutNDS:=rSumOutNDS-(rv(rQ*quSpecInCenaTovar.AsFloat)-rv(rQ*quSpecInCenaTovar.AsFloat*dmMt.taGNDS.AsFloat/(100+dmMt.taGNDS.AsFloat)));
                  end;

                  ptTTK.Next; inc(iC);
                end;
                //��������� ������� ��������� ���� ����
                if abs(rSumP)>0.000001 then
                begin
                  taSpecIn.Edit;
                  taSpecInSumCenaTovarPost.AsFloat:=taSpecInSumCenaTovarPost.AsFloat+rSumP;
                  taSpecIn.Post;
                end;
                if abs(rSumNDS)>0.000001 then
                begin
                  taSpecIn.Edit;
                  taSpecInNDSSum.AsFloat:=taSpecInNDSSum.AsFloat+rSumNDS;
                  taSpecIn.Post;
                end;
                if abs(rSumOutNDS)>0.000001 then
                begin
                  taSpecIn.Edit;
                  taSpecInOutNDSSum.AsFloat:=taSpecInOutNDSSum.AsFloat+rSumOutNDS;
                  taSpecIn.Post;
                end;

              end else
              begin
                taSpecIn.Append;
                taSpecInNum.AsInteger:=iC;
                taSpecInCodeTovar.AsInteger:=quSpecInCodeTovar.AsInteger;
                taSpecInCodeEdIzm.AsInteger:=quSpecInCodeEdIzm.AsInteger;
                taSpecInBarCode.AsString:=quSpecInBarCode.AsString;
                taSpecInNDSProc.AsFloat:=quSpecInNDSProc.AsFloat;
                taSpecInNDSSum.AsFloat:=rv(quSpecInNDSSum.AsFloat);
                taSpecInOutNDSSum.AsFloat:=rv(quSpecInOutNDSSum.AsFloat);
                taSpecInBestBefore.AsDateTime:=quSpecInBestBefore.AsDateTime;
                taSpecInKolMest.AsInteger:=quSpecInKolMest.AsInteger;
                taSpecInKolEdMest.AsFloat:=0;
                taSpecInKolWithMest.AsFloat:=quSpecInKolWithMest.AsFloat;
                taSpecInKolWithNecond.AsFloat:=quSpecInKolWithNecond.AsFloat;
                taSpecInKol.AsFloat:=quSpecInKol.AsFloat;
                taSpecInCenaTovar.AsFloat:=quSpecInCenaTovar.AsFloat;
                taSpecInProcent.AsFloat:=quSpecInProcent.AsFloat;
                taSpecInNewCenaTovar.AsFloat:=quSpecInNewCenaTovar.AsFloat;
                taSpecInSumCenaTovarPost.AsFloat:=rv(quSpecInSumCenaTovarPost.AsFloat);
                taSpecInSumCenaTovar.AsFloat:=rv(quSpecInSumCenaTovar.AsFloat);
                taSpecInProcentN.AsFloat:=quSpecInProcentN.AsFloat;
                taSpecInNecond.AsFloat:=quSpecInNecond.AsFloat;
                taSpecInCenaNecond.AsFloat:=quSpecInCenaNecond.AsFloat;
                taSpecInSumNecond.AsFloat:=quSpecInSumNecond.AsFloat;
                taSpecInProcentZ.AsFloat:=quSpecInProcentZ.AsFloat;
                taSpecInZemlia.AsFloat:=quSpecInZemlia.AsFloat;
                taSpecInProcentO.AsFloat:=quSpecInProcentO.AsFloat;
                taSpecInOthodi.AsFloat:=quSpecInOthodi.AsFloat;
                taSpecInName.AsString:=quSpecInName.AsString;
                taSpecInCodeTara.AsInteger:=quSpecInCodeTara.AsInteger;

                StrWk:='';
                if quSpecInCodeTara.AsInteger>0 then fTara(quSpecInCodeTara.AsInteger,StrWk);
                taSpecInNameTara.AsString:=StrWk;

                taSpecInVesTara.AsFloat:=quSpecInVesTara.AsFloat;
                taSpecInCenaTara.AsFloat:=quSpecInCenaTara.AsFloat;
                taSpecInSumCenaTara.AsFloat:=rv(quSpecInSumCenaTara.AsFloat);
                taSpecInTovarType.AsInteger:=quSpecInTovarType.AsInteger;
                taSpecInRealPrice.AsFloat:=quSpecInCena.AsFloat;
                taSpecIniCat.AsInteger:=quSpecInV02.AsInteger;
                if quSpecInV04.AsInteger=0 then taSpecInsTop.AsString:=' ' else taSpecInsTop.AsString:='T';
                if quSpecInV05.AsInteger=0 then taSpecInsNov.AsString:=' ' else taSpecInsNov.AsString:='N';

                taSpecInSumVesTara.AsFloat:=quSpecInSumVesTara.AsFloat;
                if quSpecInSumVesTara.AsFloat>0.1 then  taSpecInsMaker.AsString:=prFindMakerName(RoundEx(quSpecInSumVesTara.AsFloat)) else taSpecInsMaker.AsString:='';

                taSpecIn.Post;

                inc(iC);
              end;
            end;
            quSpecIn.Next;
          end;

          ptTTK.Active:=False;

          fmDocsInvS.ViewDoc1.EndUpdate;

          fmDocsInvS.acSaveDoc.Enabled:=True;

          bOpen:=False; //���������� �� ������������

          fmDocsInvS.Show;
        end;
      end else
      begin
        showmessage('�������� ��������� �.�. �������� �����������.');
      end;
    end;}
  end;
end;

procedure TfmDocsInvH.acCheckBuhExecute(Sender: TObject);
//Var iSt:INteger;
begin
//
  with dmMCS do
  begin
    {
    if quTTNInv.RecordCount>0 then
    begin
      if quTTNInvChekBuh.AsInteger=0 then fmChangeB.cxRadioButton1.Checked:=True;
      if quTTNInvChekBuh.AsInteger=1 then fmChangeB.cxRadioButton2.Checked:=True;
      if quTTNInvChekBuh.AsInteger=2 then fmChangeB.cxRadioButton3.Checked:=True;

      fmChangeB.ShowModal;
      if fmChangeB.ModalResult=mrOk then
      begin
        iSt:=0;
        if fmChangeB.cxRadioButton2.Checked then iSt:=1;
        if fmChangeB.cxRadioButton3.Checked then iSt:=2;

        quE.Active:=False;
        quE.SQL.Clear;
        quE.SQL.Add('Update "TTNIn" Set ChekBuh='+its(iSt));
        quE.SQL.Add('where Depart='+its(quTTNInvDepart.AsInteger));
        quE.SQL.Add('and DateInvoice='''+ds(quTTNInvDATEDOC.AsDateTime)+'''');
        quE.SQL.Add('and Number='''+quTTNInvNumber.AsString+'''');
        quE.ExecSQL;

        prRefrID(quTTNInv,ViewDocsIn,0);

        showmessage('��.');
      end;
    end else
      showmessage('�������� ��������');}
  end;
end;

procedure TfmDocsInvH.acChangePostExecute(Sender: TObject);
begin
// �������� ����������
  with dmMCS do
  begin
    {
    if quTTNInv.RecordCount>0 then //���� ��� �������
    begin
      try
        fmChangePost:=tfmChangePost.Create(Application);
        fmChangePost.Label1.Caption:='�������� ���������� � ��������� � '+quTTNInvNumber.AsString;
        fmChangePost.Label3.Caption:=quTTNInvNameCli.AsString;
        fmChangePost.cxButtonEdit1.Text:='';
        fmChangePost.cxButtonEdit1.Tag:=0;
        fmChangePost.Label4.Tag:=0;

        fmDocsInvS.Label17.Caption:='';
        fmDocsInvS.Label17.Tag:=0;

        fmChangePost.ShowModal;
      finally
        fmChangePost.Release;
      end;
    end;}
  end;
end;

procedure TfmDocsInvH.acChangeTypePolExecute(Sender: TObject);
begin
  //�������� ����������
  with dmMCS do
  begin
    {
    if quTTNInv.RecordCount>0 then
    begin
      fmChangeT1.Label1.Caption:='�������� � '+quTTNInvNumber.AsString+' �� '+DateToStr(quTTNInvDATEDOC.AsDateTime)+' ������ ��� '+its(quTTNInvProvodType.AsInteger);
      if quTTNInvProvodType.AsInteger=0 then fmChangeT1.cxComboBox1.ItemIndex:=1 else fmChangeT1.cxComboBox1.ItemIndex:=0;
      fmChangeT1.ShowModal;
      if fmChangeT1.ModalResult=mrOk then
      begin
//        showmessage('�����');
        quE.Active:=False;
        quE.SQL.Clear;
        quE.SQL.Add('Update "TTNIn" Set ProvodType='+its(fmChangeT1.cxComboBox1.ItemIndex));
        quE.SQL.Add('where Depart='+its(quTTNInvDepart.AsInteger));
        quE.SQL.Add('and DateInvoice='''+ds(quTTNInvDATEDOC.AsDateTime)+'''');
        quE.SQL.Add('and Number='''+quTTNInvNumber.AsString+'''');
        quE.ExecSQL;

        prRefrID(quTTNInv,ViewDocsIn,0);

        showmessage('��� ���������� �������.');
      end;
    end else
      showmessage('�������� ��������');}
  end;
end;

procedure TfmDocsInvH.acPrintAddDocsExecute(Sender: TObject);
begin
  // ������ ��������� ���������� ���� + ����
  with dmMCS do
  begin
{    if ViewDocsIn.Controller.SelectedRecordCount>0 then
    begin
      try
        fmPrintAdd:=tfmPrintAdd.Create(Application);
        fmPrintAdd.Label1.Caption:='�������� '+its(ViewDocsIn.Controller.SelectedRecordCount)+' ����������.';
        fmPrintAdd.Tag:=1;
        fmPrintAdd.Memo1.Clear;
        fmPrintAdd.ShowModal;
      except
        fmPrintAdd.Release;
      end;
    end;}
  end;
end;

procedure TfmDocsInvH.acPrintF1Execute(Sender: TObject);
begin
  //������ ������� �����
  with dmMCS do
  begin
    if quTTnInv.RecordCount>0 then
    begin
      Memo1.Clear;
      Memo1.Lines.Add('����� ... ���� ������������ ������.'); delay(10);
      frtaSpecInv.DataSet:=dmMCS.quSpecInvPrint1;

      quSpecInvPrint1.Active:=False;
      quSpecInvPrint1.Parameters.ParamByName('IDH').Value:=quTTnInvID.AsInteger;
      quSpecInvPrint1.Active:=True;
      Memo1.Lines.Add('������� ��������.'); delay(10);
      try
        frRepDINV.LoadFromFile(CommonSet.PathReport + 'Inventry01.frf');

        frVariables.Variable['DocNum']:=quTTnInvNUMDOC.AsString;
        frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',quTTnInvDATEDOC.AsDateTime);
        frVariables.Variable['Depart']:=quTTnInvNAMESKL.AsString;
        frVariables.Variable['SumR']:=quTTnInvRSUMR.AsFloat;
        frVariables.Variable['SumF']:=quTTnInvRSUMF.AsFloat;
        frVariables.Variable['SumD']:=quTTnInvRSUMF.AsFloat-quTTnInvRSUMR.AsFloat;
        frVariables.Variable['SSumR']:=MoneyToString(abs(rv(quTTnInvRSUMR.AsFloat)),True,False);
        frVariables.Variable['SSumF']:=MoneyToString(abs(rv(quTTnInvRSUMF.AsFloat)),True,False);
        frVariables.Variable['SSumD']:=MoneyToString(abs(rv(quTTnInvRSUMF.AsFloat-quTTnInvRSUMR.AsFloat)),True,False);
        frVariables.Variable['Filial']:='';

        frRepDINV.ReportName:='������������������ ��������� (� ��������� �����).';
        frRepDINV.PrepareReport;
        frRepDINV.ShowPreparedReport;
      except
        Memo1.Lines.Add('������ ������.'); delay(10);
      end;
      quSpecInvPrint1.Active:=False;
    end;
  end;
end;

procedure TfmDocsInvH.acPrintF2Execute(Sender: TObject);
begin
  //������ ������� �����
  with dmMCS do
  begin
    if quTTnInv.RecordCount>0 then
    begin
      Memo1.Clear;
      Memo1.Lines.Add('����� ... ���� ������������ ������.'); delay(10);
      frtaSpecInv.DataSet:=quSpecInvPrint2;

      quSpecInvPrint2.Active:=False;
      quSpecInvPrint2.Parameters.ParamByName('IDH').Value:=quTTnInvID.AsInteger;
      quSpecInvPrint2.Active:=True;
      Memo1.Lines.Add('������� ��������.'); delay(10);
      try
        frRepDINV.LoadFromFile(CommonSet.PathReport + 'Inventry02.frf');

        frVariables.Variable['DocNum']:=quTTnInvNUMDOC.AsString;
        frVariables.Variable['DocDate']:=FormatDateTime('dd.mm.yyyy',quTTnInvDATEDOC.AsDateTime);
        frVariables.Variable['Depart']:=quTTnInvNAMESKL.AsString;
        frVariables.Variable['SumR']:=quTTnInvRSUMR.AsFloat;
        frVariables.Variable['SumF']:=quTTnInvRSUMF.AsFloat;
        frVariables.Variable['SumD']:=quTTnInvRSUMF.AsFloat-quTTnInvRSUMR.AsFloat;
        frVariables.Variable['SSumR']:=MoneyToString(abs(rv(quTTnInvRSUMR.AsFloat)),True,False);
        frVariables.Variable['SSumF']:=MoneyToString(abs(rv(quTTnInvRSUMF.AsFloat)),True,False);
        frVariables.Variable['SSumD']:=MoneyToString(abs(rv(quTTnInvRSUMF.AsFloat-quTTnInvRSUMR.AsFloat)),True,False);
        frVariables.Variable['Filial']:='';

        frRepDINV.ReportName:='������������������ ��������� (� ��������� �����).';
        frRepDINV.PrepareReport;
        frRepDINV.ShowPreparedReport;
      except
        Memo1.Lines.Add('������ ������.'); delay(10);
      end;
      quSpecInvPrint2.Active:=False;
    end;
  end;
end;

procedure TfmDocsInvH.acCalcInvExecute(Sender: TObject);
var IDH:Integer;
    Rec:TcxCustomGridRecord;
    i,j:Integer;
begin
  with dmMCS do
  begin
    if quTTnInv.RecordCount>0 then
    begin
      Memo1.Clear;
      if MessageDlg('���������� ������ ���-�� � ���� �� ���������� ���������� ( '+its(ViewDocsInv.Controller.SelectedRecordCount)+' ���. ) ?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        Memo1.Lines.Add('�����... ���� ������.');

        IDH:=quTTnInvID.AsInteger;

        for i:=0 to ViewDocsInv.Controller.SelectedRecordCount-1 do
        begin
          Rec:=ViewDocsInv.Controller.SelectedRecords[i];

          IDH:=0;

          for j:=0 to Rec.ValueCount-1 do
          begin
            if ViewDocsInv.Columns[j].Name='ViewDocsInvID' then begin IDH:=Rec.Values[j]; Break; end;
          end;

          if (IDH>0)and(quTTnInv.Locate('ID',IDH,[])=True) then
          begin
            Memo1.Lines.Add('   ������ ��������� �'+quTTnInvNUMDOC.AsString+' �� '+quTTnInvDATEDOC.AsString);
            if quTTnInvIACTIVE.AsInteger<>3 then
            begin
              quProc.SQL.Clear;
              quProc.SQL.Add('EXECUTE [dbo].[prCalcInvSum] '+its(IDH));
              quProc.ExecSQL;

              Memo1.Lines.Add('   ������ ��.');
            end else
            begin
              Memo1.Lines.Add('   ������ ��������. �������� ������ ���������.');
            end;
          end;
        end;
        quTTnInv.Requery();
        quTTnInv.Locate('ID',IDH,[]);
        Memo1.Lines.Add('������� ��������.');
      end;
    end;
  end;
end;

procedure TfmDocsInvH.acRecalcInvActiveExecute(Sender: TObject);
var IDH:Integer;
    Rec:TcxCustomGridRecord;
    i,j:Integer;
begin
  with dmMCS do
  begin
    if quTTnInv.RecordCount>0 then
    begin
      Memo1.Clear;
      if MessageDlg('���������� ������ ���-�� � ���� �� ���������� ���������� ( '+its(ViewDocsInv.Controller.SelectedRecordCount)+' ���. ) ?',mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        Memo1.Lines.Add('�����... ���� ������.');

        IDH:=quTTnInvID.AsInteger;

        for i:=0 to ViewDocsInv.Controller.SelectedRecordCount-1 do
        begin
          Rec:=ViewDocsInv.Controller.SelectedRecords[i];

          IDH:=0;

          for j:=0 to Rec.ValueCount-1 do
          begin
            if ViewDocsInv.Columns[j].Name='ViewDocsInvID' then begin IDH:=Rec.Values[j]; Break; end;
          end;

          if (IDH>0)and(quTTnInv.Locate('ID',IDH,[])=True) then
          begin
            Memo1.Lines.Add('   ������ ��������� �'+quTTnInvNUMDOC.AsString+' �� '+quTTnInvDATEDOC.AsString);
            if quTTnInvIACTIVE.AsInteger<>999 then //��������
            begin
              quProc.SQL.Clear;
              quProc.SQL.Add('EXECUTE [dbo].[prCalcInvSum] '+its(IDH));
              quProc.ExecSQL;

              Memo1.Lines.Add('   ������ ��.');
            end else
            begin
              Memo1.Lines.Add('   ������ ��������. �������� ������ ���������.');
            end;
          end;
        end;
        quTTnInv.Requery();
        quTTnInv.Locate('ID',IDH,[]);
        Memo1.Lines.Add('������� ��������.');
      end;
    end;
  end;
end;

end.
