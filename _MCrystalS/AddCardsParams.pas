unit AddCardsParams;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons, ExtCtrls,
  cxTextEdit, cxControls, cxContainer, cxEdit, cxMemo;

type
  TfmAddParams = class(TForm)
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Label1: TLabel;
    cxMemo1: TcxMemo;
    Label2: TLabel;
    Label3: TLabel;
    cxTextEdit1: TcxTextEdit;
    Label4: TLabel;
    cxTextEdit2: TcxTextEdit;
    Label5: TLabel;
    cxMemo2: TcxMemo;
    Label6: TLabel;
    cxMemo3: TcxMemo;
    Label7: TLabel;
    cxTextEdit3: TcxTextEdit;
    Label8: TLabel;
    cxTextEdit4: TcxTextEdit;
    Label9: TLabel;
    cxMemo4: TcxMemo;
    Label10: TLabel;
    cxTextEdit5: TcxTextEdit;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAddParams: TfmAddParams;

implementation

{$R *.dfm}

end.
