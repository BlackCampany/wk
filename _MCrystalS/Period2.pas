unit Period2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons,
  cxGraphics, cxButtonEdit, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox, cxControls, cxContainer, cxEdit, cxTextEdit,
  cxMaskEdit, cxCalendar, cxCheckBox, DB, ADODB;

type
  TfmPer1 = class(TForm)
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    cxDateEdit1: TcxDateEdit;
    cxDateEdit2: TcxDateEdit;
    cxLookupComboBox1: TcxLookupComboBox;
    cxLookupComboBox2: TcxLookupComboBox;
    cxButtonEdit1: TcxButtonEdit;
    cxCheckBox1: TcxCheckBox;
    cxCheckBox2: TcxCheckBox;
    cxCheckBox3: TcxCheckBox;
    Label6: TLabel;
    cxCheckBox4: TcxCheckBox;
    cxButtonEdit2: TcxButtonEdit;
    procedure FormCreate(Sender: TObject);
    procedure cxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxButtonEdit2PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxCheckBox1PropertiesChange(Sender: TObject);
    procedure cxCheckBox2PropertiesChange(Sender: TObject);
    procedure cxCheckBox3PropertiesChange(Sender: TObject);
    procedure cxCheckBox4PropertiesChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmPer1: TfmPer1;

implementation

uses Dm, Clients, Un1, ClasSel;

{$R *.dfm}

procedure TfmPer1.FormCreate(Sender: TObject);
begin
  cxCheckBox1.Checked:=False;
  cxCheckBox2.Checked:=True;
  cxCheckBox3.Checked:=False;
  cxCheckBox4.Checked:=True;

  cxDateEdit1.Date:=Trunc(Date-30);
  cxDateEdit2.Date:=Date;

  cxButtonEdit1.Tag:=0; cxButtonEdit1.Text:='';
  cxButtonEdit2.Tag:=0; cxButtonEdit2.Text:='';
end;

procedure TfmPer1.cxButtonEdit1PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
Var i:Integer;
begin
  with dmMCS do
  with fmClients do
  begin
    iDirect:=101; //0 - ������ , 1- �������

    fmClients.Show;

    if cxButtonEdit1.Tag>0 then  //�����-�� ��������� - ���� �� ���� ���������
    begin
      quFCli.Active:=False;
      quFCli.SQL.Clear;
      quFCli.SQL.Add('SELECT * FROM [dbo].[CLIENTS] cli');
      quFCli.SQL.Add('where cli.[Id]='+its(cxButtonEdit1.Tag));
      quFCli.Active:=True;

      bRefreshCli:=False;
      for i:=0 to CliTree.Items.Count-1 Do
      begin
        if Integer(CliTree.Items[i].Data) = quFCli.FieldByName('IdParent').AsInteger then
        begin
          CliTree.Items[i].Expand(False);
          CliTree.Repaint;
          CliTree.Items[i].Selected:=True;
          try
            CliView.BeginUpdate;
            prRefreshCli(quFCli.FieldByName('IdParent').AsInteger,cxCheckBox1.Checked);
            quClients.Locate('ID',quFCli.FieldByName('Id').AsInteger,[]);
          finally
            CliView.EndUpdate;
            CliGr.SetFocus;
            CliView.Controller.FocusRecord(CliView.DataController.FocusedRowIndex,True);
          end;
          Break;
        End;
      end;
      bRefreshCli:=True;
      quFCli.Active:=False;
    end;

  end;
end;

procedure TfmPer1.cxButtonEdit2PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  with dmMCS do
  begin
    fmClassSel:=TfmClassSel.Create(Application);

    PosP.Id:=0;
    PosP.Name:='';

    fmClassSel.Tag:=cxButtonEdit2.Tag;
    fmClassSel.ShowModal;

    if PosP.Id>0 then
    begin
      cxButtonEdit2.Tag:=PosP.Id;
      cxButtonEdit2.Text:=PosP.Name;
    end;

    fmClassSel.Release;
  end;
end;

procedure TfmPer1.cxCheckBox1PropertiesChange(Sender: TObject);
begin
  if cxCheckBox1.Checked then
  begin
    cxLookupComboBox1.Enabled:=False;
  end else
  begin
    cxLookupComboBox1.Enabled:=True;
  end;
end;

procedure TfmPer1.cxCheckBox2PropertiesChange(Sender: TObject);
begin
  if cxCheckBox2.Checked then
  begin
    cxLookupComboBox2.Enabled:=False;
  end else
  begin
    cxLookupComboBox2.Enabled:=True;
  end;
end;

procedure TfmPer1.cxCheckBox3PropertiesChange(Sender: TObject);
begin
  if cxCheckBox3.Checked then
  begin
    cxButtonEdit1.Enabled:=False;
  end else
  begin
    cxButtonEdit1.Enabled:=True;
  end;
end;

procedure TfmPer1.cxCheckBox4PropertiesChange(Sender: TObject);
begin
  if cxCheckBox4.Checked then
  begin
    cxButtonEdit2.Enabled:=False;
  end else
  begin
    cxButtonEdit2.Enabled:=True;
  end;
end;

end.
