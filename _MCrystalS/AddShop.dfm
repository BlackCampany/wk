object fmAddShop: TfmAddShop
  Left = 290
  Top = 583
  BorderStyle = bsDialog
  ClientHeight = 173
  ClientWidth = 374
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 20
    Top = 48
    Width = 50
    Height = 13
    Caption = #1053#1072#1079#1074#1072#1085#1080#1077
    Transparent = True
  end
  object Label2: TLabel
    Left = 28
    Top = 80
    Width = 34
    Height = 13
    Caption = #1057#1090#1072#1090#1091#1089
    Transparent = True
  end
  object Label3: TLabel
    Left = 28
    Top = 12
    Width = 34
    Height = 13
    Caption = #1053#1086#1084#1077#1088
    Transparent = True
  end
  object Panel1: TPanel
    Left = 0
    Top = 119
    Width = 374
    Height = 54
    Align = alBottom
    BevelInner = bvLowered
    Color = 16763541
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 60
      Top = 12
      Width = 85
      Height = 33
      Caption = #1054#1082
      ModalResult = 1
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
    end
    object cxButton2: TcxButton
      Left = 228
      Top = 12
      Width = 85
      Height = 33
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      LookAndFeel.Kind = lfOffice11
    end
  end
  object cxTextEdit1: TcxTextEdit
    Left = 84
    Top = 44
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 1
    Text = 'cxTextEdit1'
    Width = 261
  end
  object cxComboBox1: TcxComboBox
    Left = 84
    Top = 76
    Properties.Items.Strings = (
      #1053#1077#1072#1082#1090#1080#1074#1085#1099#1081
      #1040#1082#1090#1080#1074#1085#1099#1081)
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 2
    Text = 'cxComboBox1'
    Width = 121
  end
  object cxSpinEdit1: TcxSpinEdit
    Left = 84
    Top = 12
    Style.BorderStyle = ebsOffice11
    Style.Shadow = True
    TabOrder = 3
    Width = 73
  end
end
