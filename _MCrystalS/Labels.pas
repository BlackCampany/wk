unit Labels;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, Menus, cxLookAndFeelPainters, dxmdaset, StdCtrls, cxButtons, ExtCtrls, ADODB;

type
  TfmLabels = class(TForm)
    ViewLabels: TcxGridDBTableView;
    LevelLabels: TcxGridLevel;
    GrLabels: TcxGrid;
    dsquLabels: TDataSource;
    Panel1: TPanel;
    cxButton1: TcxButton;
    ViewLabelsID: TcxGridDBColumn;
    ViewLabelsName: TcxGridDBColumn;
    ViewLabelsNameF: TcxGridDBColumn;
    quLabels: TADOQuery;
    quLabelsID: TAutoIncField;
    quLabelsName: TStringField;
    quLabelsNameF: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmLabels: TfmLabels;

implementation

uses Dm, MainMC;

{$R *.dfm}

procedure TfmLabels.FormCreate(Sender: TObject);
begin
  GrLabels.Align:=AlClient;
end;

procedure TfmLabels.cxButton1Click(Sender: TObject);
begin
  with dmMCS do
  begin
    quLabs.Active:=False;
    quLabs.Active:=True;
    quLabs.First;

    fmMainMC.cxLookupComboBox1.EditValue:=quLabsID.AsInteger;
  end;

  Close;
end;

end.
