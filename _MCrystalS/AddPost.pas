unit AddPost;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, ComCtrls, StdCtrls, cxButtons,
  ExtCtrls, cxControls, cxContainer, cxEdit, cxTextEdit, cxGraphics,
  cxMaskEdit, cxDropDownEdit, cxImageComboBox, cxCheckBox, cxCalendar,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxCalc, cxListView,
  cxGroupBox;

type
  TfmAddPost = class(TForm)
    Panel1: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    StatusBar1: TStatusBar;
    cxTextEdit1: TcxTextEdit;
    cxTextEdit2: TcxTextEdit;
    cxTextEdit3: TcxTextEdit;
    cxImageComboBox1: TcxImageComboBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    cxTextEdit4: TcxTextEdit;
    Label6: TLabel;
    cxTextEdit5: TcxTextEdit;
    Label7: TLabel;
    cxTextEdit6: TcxTextEdit;
    Label8: TLabel;
    cxTextEdit7: TcxTextEdit;
    Label9: TLabel;
    cxTextEdit8: TcxTextEdit;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    cxTextEdit9: TcxTextEdit;
    cxTextEdit10: TcxTextEdit;
    cxTextEdit11: TcxTextEdit;
    cxTextEdit12: TcxTextEdit;
    cxTextEdit13: TcxTextEdit;
    cxTextEdit14: TcxTextEdit;
    Label16: TLabel;
    Label17: TLabel;
    cxTextEdit15: TcxTextEdit;
    cxCheckBox1: TcxCheckBox;
    cxDateEdit1: TcxDateEdit;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    GroupBox1: TGroupBox;
    cxTextEdit16: TcxTextEdit;
    cxTextEdit17: TcxTextEdit;
    cxImageComboBox2: TcxImageComboBox;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    cxTextEdit18: TcxTextEdit;
    cxTextEdit19: TcxTextEdit;
    cxCheckBox2: TcxCheckBox;
    cxCheckBox3: TcxCheckBox;
    Label24: TLabel;
    cxLookupComboBox1: TcxLookupComboBox;
    cxCalcEdit1: TcxCalcEdit;
    Label25: TLabel;
    cxCheckBox4: TcxCheckBox;
    GroupBox2: TGroupBox;
    cxTextEdit20: TcxTextEdit;
    Label26: TLabel;
    Label27: TLabel;
    cxTextEdit21: TcxTextEdit;
    cxTextEdit22: TcxTextEdit;
    Label28: TLabel;
    Label29: TLabel;
    cxLookupComboBox2: TcxLookupComboBox;
    cxCheckBox5: TcxCheckBox;
    Label30: TLabel;
    cxTextEdit23: TcxTextEdit;
    Label31: TLabel;
    cxComboBox1: TcxComboBox;
    cxCheckBox6: TcxCheckBox;
    cxTextEdit24: TcxTextEdit;
    Label32: TLabel;
    cxCheckBox7: TcxCheckBox;
    procedure cxButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAddPost: TfmAddPost;

implementation

uses Dm, Un1;

{$R *.dfm}

procedure TfmAddPost.cxButton1Click(Sender: TObject);
Var bExit:Boolean;
begin
  bExit:=True;
  if trim(cxTextEdit3.Text)='' then
  begin
   bExit:=False;
   ShowMessage('��������� ���. ���������� ����������!');
   abort;
  end;
  if cxImageComboBox2.EditValue=1 then
  begin
    if Length(cxTextEdit18.Text)<13 then
    begin
      bExit:=False;
      ShowMessage('��������� ���. ���������� ����������!');
      abort;
    end;
   ModalResult:=mrOk;
  end;

  if cxImageComboBox2.EditValue=2 then
  begin
    if pos('@',cxTextEdit19.Text)=0 then
    begin
      bExit:=False;
      ShowMessage('��������� E-Mail. ���������� ����������!');
    end;
  end;
  if cxTextEdit22.Text>'' then
  begin
    if pos('@',cxTextEdit22.Text)=0 then
    begin
      bExit:=False;
      ShowMessage('����������� ����� E-Mail ��������. ���������� ����������!');
    end;
  end;


  if bExit then  ModalResult:=mrOk else ModalResult:=mrNone; 
end;

end.
